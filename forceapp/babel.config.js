module.exports = {
  presets: [
    '@babel/preset-react',
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'usage',
        'corejs': 2,
      },
    ],
    '@babel/preset-flow',
  ],
  plugins: [
    'webpack-chunkname',
    '@babel/plugin-transform-flow-strip-types',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-transform-react-inline-elements',
  ],
  env: {
    test: {
      plugins: ['babel-plugin-rewire', 'require-context-hook'],
    },
  },
};
