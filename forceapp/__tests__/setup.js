import 'jest-styled-components';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import areIntlLocalesSupported from 'intl-locales-supported';

Enzyme.configure({ adapter: new Adapter() });

// Simulate Intl on NodeJs
const localesMyAppSupports = [
  /* list locales here */
  'ja',
  'ja-JP',
  'en',
  'en-US',
];

/* eslint-disable no-undef */

if (global.Intl) {
  // Determine if the built-in `Intl` has the locale data we need.
  if (!areIntlLocalesSupported(localesMyAppSupports)) {
    // `Intl` exists, but it doesn't have the data we need, so load the
    // polyfill and replace the constructors we need with the polyfill's.
    require('intl');
    Intl.NumberFormat = IntlPolyfill.NumberFormat;
    Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;
  }
} else {
  // No `Intl`, so use and load the polyfill.
  global.Intl = require('intl');
}

/* eslint-enable no-undef */
