export class ErrorResponse {
  /**
   * @param {Object} error APIエラー時のレスポンスオブジェクト相当
   * @param {String} [error.errorCode] エラーコード
   * @param {String} [error.message] エラーメッセージ
   * @param {String} [error.stackTrace] Apexのスタックトレース
   * @see https://teamspiritdev.atlassian.net/wiki/spaces/GENIE/pages/10949907/API
   */
  constructor(error) {
    this.error = error;
  }
}

class ApiMock {
  constructor() {
    this.pathSettings = [];
  }

  /**
   * Api.invoke実行時のレスポンス内容（ダミーデータ）を登録する
   * @param {String} path
   * @param {Object} param
   * @param {Object|ErrorResponse} response
   */
  setDummyResponse(path, param, response) {
    this.pathSettings[path] = this.pathSettings[path] || [];
    this.pathSettings[path][JSON.stringify(param)] = response;
  }

  /**
   * API呼び出し処理のモック
   * - 返却値のPromiseは、即時にresolve/rejectされる。
   * - この時にthen/catchに渡される値は、
   *   あらかじめsetDummyResponseで設定された内容から
   *   引数path, paramにもとづいて引き当てられる
   * @param {String} path
   * @param {Object} param
   * @return {Promise}
   */
  invoke({ path, param }) {
    return new Promise((resolve, reject) => {
      const result = this.pathSettings[path][JSON.stringify(param)];
      if (result instanceof ErrorResponse) {
        reject(result.error);
      } else {
        resolve(result);
      }
    });
  }
}

export default new ApiMock();
