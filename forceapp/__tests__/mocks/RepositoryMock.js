// @flow

const repositoryMock = {
  search: jest.fn(),
  fetch: jest.fn(),
  update: jest.fn(),
  create: jest.fn(),
  delete: jest.fn(),
};

export default Object.assign(repositoryMock, {
  mockClearAll: () => {
    repositoryMock.search.mockClear();
    repositoryMock.fetch.mockClear();
    repositoryMock.update.mockClear();
    repositoryMock.create.mockClear();
    repositoryMock.delete.mockClear();
  },
  mockResetAll: () => {
    repositoryMock.search.mockReset();
    repositoryMock.fetch.mockReset();
    repositoryMock.update.mockReset();
    repositoryMock.create.mockReset();
    repositoryMock.delete.mockReset();
  },
  mockRestoreAll: () => {
    repositoryMock.search.mockRestore();
    repositoryMock.fetch.mockRestore();
    repositoryMock.update.mockRestore();
    repositoryMock.create.mockRestore();
    repositoryMock.delete.mockRestore();
  },
});
