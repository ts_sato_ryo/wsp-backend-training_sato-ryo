export default class DispatcherMock {
  constructor() {
    this.logged = [];
    this.dispatch = this.dispatch.bind(this);
  }

  // NOTE: Jest.fn()のモックで置き換えても良いかもしれない
  dispatch(action) {
    this.logged.push(action);

    // NOTE: redux-thunkの挙動をエミュレート
    if (typeof action === 'function') {
      return action(this.dispatch);
    }
    return undefined;
  }
}
