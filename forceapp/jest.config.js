/* eslint-disable import/no-extraneous-dependencies */
require('@babel/register');

module.exports = {
  rootDir: '.',
  testMatch: ['**/__tests__/**/*.test.js'],
  unmockedModulePathPatterns: ['node_modules/'],
  moduleNameMapper: {
    '(commons|^..)/api$': '<rootDir>/__tests__/mocks/ApiMock.js',
    '(repositories)/(.*)?(\\.js)$':
      '<rootDir>/__test__/mocks/RepositoryMock.js',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__tests__/mocks/fileMock.js',
    '\\.(css|scss)$': '<rootDir>/__tests__/mocks/styleMock.js',
  },
  transformIgnorePatterns: [
    "(node_modules)(?!/@salesforce/)"
  ],
  testEnvironment: 'jest-environment-jsdom-global',
  setupFilesAfterEnv: ['<rootDir>/__tests__/setup.js'],
  setupFiles: ['<rootDir>/__tests__/register-context.js']
};
