import HtmlWebpackPlugin from 'html-webpack-plugin';
import HtmlWebpackHarddiskPlugin from 'html-webpack-harddisk-plugin';
import path from 'path';

import createWebpackConfig from './webpack.config.babel';

const webpack = createWebpackConfig('local');

const entries = Object.keys(webpack.entry);
const htmlWebpackPlugins = entries.map((entry) => {
  const dist = entry.replace(/_/g, '-');

  const links = [];
  if (entry === 'test_api') {
    links.push(
      'https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/themes/prism-tomorrow.min.css'
    );
  }

  return new HtmlWebpackPlugin({
    inject: false,
    alwaysWriteToDisk: true,
    title: entry,
    chunks: [entry],
    filename: path.resolve(__dirname, 'build', dist, 'index.html'),
    template: path.resolve(__dirname, 'templates/index.development.ejs'),
    links,
  });
});

const devConfig = {
  ...webpack,

  plugins: [
    ...webpack.plugins,
    ...htmlWebpackPlugins,
    new HtmlWebpackHarddiskPlugin(),
  ],
};

const debug = (config) => {
  const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
  const smp = new SpeedMeasurePlugin();
  return smp.wrap(config);
}


export default (process.env.DEBUG)
  ? debug(devConfig)
  : devConfig;
