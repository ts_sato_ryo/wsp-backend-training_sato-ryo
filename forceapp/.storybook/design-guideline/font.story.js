import React from 'react';
/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */
import withReadme from 'storybook-readme/with-readme';

import readme from './font.story.md';

import './design-guideline.story.scss';
import './font.story.scss';

const ROOT = 'ts-design-guildeline-font';

storiesOf('Design Guideline', module)
  .add(
    'Font',
    withReadme(
      readme,
      () => {
        return (
          <section className="ts-design-guideline">
            <h1>Sample</h1>

            <section>
              <h2>Font Size</h2>

              <section>
                <h3>$font-size-default</h3>

                <p className={`${ROOT}__size-default`}>
                  基本文字サイズ
                </p>
              </section>

              <section>
                <h3>$font-size-small</h3>

                <p className={`${ROOT}__size-small`}>
                  サイズ小
                </p>
              </section>

              <section>
                <h3>$font-size-large</h3>

                <p className={`${ROOT}__size-large`}>
                  サイズ大
                </p>
              </section>

              <section>
                <h3>$font-size-x-large</h3>

                <p className={`${ROOT}__size-x-large`}>
                  サイズ特大
                </p>
              </section>
            </section>
          </section>
        );
      }
    )
  );
