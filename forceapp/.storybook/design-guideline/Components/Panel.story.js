import React from 'react';
/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */
import withReadme from 'storybook-readme/with-readme';

import readme from './Panel.story.md';

import '../design-guideline.story.scss';
import './Panel.story.scss';

const ROOT = 'ts-design-guildeline-components-panel';

storiesOf('Design Guideline', module)
  .add(
    'Components/Panel',
    withReadme(
      readme,
      () => {
        return (
          <section className="ts-design-guideline">
            <h1>Sample</h1>

            <div className={`${ROOT}`}>
              <div className={`${ROOT}__first`}>
                <header className={`${ROOT}__first-header`}>$color-bg-pane-header-first /<br />$color-text-pane-header-first</header>

                <p className={`${ROOT}__first-body`}>$color-bg-pane-first</p>
              </div>

              <div className={`${ROOT}__second`}>
                <header className={`${ROOT}__second-header`}>$color-bg-pane-header-second /<br />$color-text-pane-header-second</header>

                <p className={`${ROOT}__second-body`}>$color-bg-pane-second</p>

                <div className={`${ROOT}__sub`}>
                  <header className={`${ROOT}__sub-header`}>$color-bg-pane-header-sub</header>
                  <p>Text</p>
                </div>
              </div>

              <div className={`${ROOT}__third`}>
                <header className={`${ROOT}__third-header`}>$color-bg-pane-header-third /<br />$color-text-pane-header-third</header>

                <p className={`${ROOT}__third-body`}>$color-bg-pane-third</p>

                <div className={`${ROOT}__sub`}>
                  <header className={`${ROOT}__sub-header`}>$color-bg-pane-header-sub</header>
                  <p>Text</p>
                </div>
              </div>
            </div>
          </section>
        );
      }
    )
  );
