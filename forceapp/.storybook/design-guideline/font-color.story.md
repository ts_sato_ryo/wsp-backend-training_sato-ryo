# Design Guideline

## Font Color

Font ColorはすべてSass変数により定義されている。  
変数はすべてのSCSSファイルへWebpackを通じて自動的にimportされる。

### Color

#### $color-text-default

基本色、bodyに定義されている。  
コンポーネント側へは継承されるため意識しなくて良い

#### $color-text-modest

modest = 控え目な

テーブルヘッダー見出しやラベル、付随情報など、優先度をすこし下げたい箇所に使われる
 
#### $color-text-emphasis

emphasis = 強調

金額表示など、強調したい文字に使われる

#### $color-text-anchor

リンクに使われる

#### $color-text-inverse

濃い紺などに使われる反転色

### 定義されていないColor

定義された色を個別に調整する必要がある場合も、Sass関数の `lighten()` や `darken()` などを使い、動的に調整することを検討すること。

__SAMPLE__

```
.module {
  // やや基本色からやや明るめに調整
  color: lighten( $color-text-base, 10% );
}
```

参考

* [lighten](http://sass-lang.com/documentation/Sass/Script/Functions.html#lighten-instance_method)
* [darken](http://sass-lang.com/documentation/Sass/Script/Functions.html#darken-instance_method)
