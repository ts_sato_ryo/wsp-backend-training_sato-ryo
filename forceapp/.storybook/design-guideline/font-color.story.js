import React from 'react';
/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */
import withReadme from 'storybook-readme/with-readme';

import readme from './font-color.story.md';

import './design-guideline.story.scss';
import './font-color.story.scss';

const ROOT = 'ts-design-guildeline-font-color';

storiesOf('Design Guideline', module)
  .add(
    'Font Color',
    withReadme(
      readme,
      () => {
        return (
          <section className="ts-design-guideline">
            <h1>Sample</h1>

            <section>
              <h2>Font Color</h2>

              <section>
                <h3>$color-text-default</h3>

                <p className={`${ROOT}__base`}>
                  基本色
                </p>
              </section>

              <section>
                <h3>$color-text-modest</h3>

                <p className={`${ROOT}__mod`}>
                  テーブルヘッダー見出しや、付随情報などに用いる弱めの色
                </p>

                <table className={`${ROOT}__modest-sample-table`}>
                  <thead>
                    <tr>
                      <th>サンプル1</th>
                      <th>サンプル2</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>値1</td>
                      <td>値2</td>
                    </tr>
                  </tbody>
                </table>
              </section>

              <section>
                <h3>$color-text-emphasis</h3>

                <p className={`${ROOT}__em`}>
                  強調色 ※サンプルではboldも付与
                </p>
              </section>

              <section>
                <h3>$color-text-anchor</h3>

                <p className={`${ROOT}__anchor`}>
                  リンク色
                </p>
              </section>

              <section>
                <h3>$color-text-inverse</h3>

                <p className={`${ROOT}__inverse-container`}>
                  <span className={`${ROOT}__inverse`}>濃い背景の場合の反転色</span>
                </p>
              </section>
            </section>
          </section>
        );
      }
    )
  );
