// @flow
/* eslint-disable import/no-extraneous-dependencies */

import * as React from 'react';
import { Provider } from 'react-redux';

type Props = $ReadOnly<{|
  children: React.Node,
  store: Object
|}>

const ProviderWrapper = ({ children, store }: Props) => (
  <Provider store={store}>
    { children }
  </Provider>
)

// eslint-disable-next-line import/prefer-default-export
export const withProvider = (store: Object) => (story: () => React.Element<any>) => (
  <ProviderWrapper store={store}>
    { story() }
  </ProviderWrapper>
)
