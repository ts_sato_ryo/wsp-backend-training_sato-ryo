import React from 'react';
import PropTypes from 'prop-types';

import './Dialog.scss';

export default class Class extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node.isRequired,
    };
  }

  render() {
    return (
      <div className="storybook-decorator-dialog">
        {this.props.children}
      </div>
    );
  }
}
