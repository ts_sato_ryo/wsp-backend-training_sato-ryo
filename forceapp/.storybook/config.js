/* eslint-disable import/no-extraneous-dependencies */
import { configure } from '@storybook/react';

import '../apps/commons/styles/base.scss';

function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

function loadStories() {
  requireAll(require.context('../apps/core', true, /.story.js$/));
  requireAll(require.context('../apps/commons/styles', true, /.story.js$/));
  requireAll(require.context('./design-guideline', true, /.story.js$/));
  requireAll(require.context('../apps/commons/stories', true, /.story.js$/));
  requireAll(
    require.context('../apps/approvals-pc/stories', true, /.story.js$/)
  );
  requireAll(require.context('../apps/admin-pc/stories', true, /.story.js$/));
  requireAll(require.context('../apps/planner-pc', true, /.story.js$/));
  requireAll(require.context('../apps/team-pc/stories', true, /.story.js$/));
  requireAll(
    require.context('../apps/timesheet-pc/stories', true, /.story.js$/)
  );
  requireAll(require.context('../apps/time-tracking', true, /.story.js$/));
  requireAll(require.context('../widgets/stories', true, /.story.js$/));
}

configure(loadStories, module);
