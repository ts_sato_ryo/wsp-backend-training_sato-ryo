/* eslint-disable import/no-extraneous-dependencies */
require('@babel/register');

const webpack = require('../webpack.config.babel.js').default();

// eslint-disable-next-line func-names, no-unused-vars
module.exports = async function ({ config, mode }) {
  config.module.rules = webpack.module.rules.map((item) => {
    const idx = item.use.indexOf('style-lodaer');
    if (idx >= 0) {
      item[idx] = {
        loader: 'style-lodaer',
        options: {
          insertAt: 'top',
        },
      };
    }

    return item;
  });

  // Readme plugin用、markdown loader
  config.module.rules.push({
    test: /\.md$/,
    use: [
      { loader: 'html-loader' },
      {
        loader: 'markdown-loader',
      },
    ],
  });

  return config;
};
