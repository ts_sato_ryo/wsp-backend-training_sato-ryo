#!/bin/sh
SECONDS=0

# Common variables
COLOR_REST="$(tput sgr0)"

COLOR_ALERT="$(tput setaf 1)"

DIR_PREFIX='forceapp/sfdx/'

COMMON_FRONT_MODULES=(domain commons repositories)

FRONT_MODULES=(admin-pc approvals-pc expenses-pc expenses-pc-print finance-approval-pc mobile-app oauth-result-view planner-pc requests-pc team-pc test-api timesheet-pc timesheet-pc-leave timesheet-pc-summary tracking-pc)

GULP_PATH='./node_modules/gulp/bin/gulp.js'

# Track the SHA history of previous deployed commits
lastDeployedCommit=`cat git-history.txt`
lastLocalCommit=`git log -n 1 --pretty=format:"%H"`

for arg in "$@"
do
	case ${1} in
		-u)
		USER_NAME="$2"
		shift
		;;
		--all)
		TYPE="--all"
		;;
		--with-local)
		TYPE="--with-local"
		;;
    esac
    shift
	done

if [[ $USER_NAME == '' ]]; then
	cat <<EOF

Usage:
  $(basename ${0}) -u [<TARGET_USER_NAME>] [DeployTarget]

  -u: required

  DeployTarget:
		default
    --with-local    To include local changes while deploying
    --all           Deploy all of your workspace.
EOF

  exit 1
fi

cd `dirname $0`

if [[ $lastDeployedCommit == '' || $TYPE == '--all' ]]; then

	printf '%s%s%s\n' $COLOR_ALERT 'Building and deploying whole WSP' $COLOR_REST
	(cd .. && ${GULP_PATH} build --color)
	result=`(cd ../../forceapp/sfdx/ && sfdx force:source:deploy -x ./manifest/package.xml -u ${USER_NAME})`

else
	# Check all the diffs that needs deployment
	filesToDeploy=($(git diff $lastDeployedCommit $lastLocalCommit --name-only))

	echo $TYPE
	if [[ $TYPE ==  "--with-local" ]]; then
		localChangedFiles=($(git diff --name-only --diff-filter=M))
		filesToDeploy=( "${filesToDeploy[@]}" "${localChangedFiles[@]}" )
	fi

	printf '%s%s%s\n' $COLOR_ALERT 'Changes to be Deployed -- ' $COLOR_REST

	jsFileCount=0
	buildAll=0
	jsModules=''
	sfdxFiles=''

	# Parse the changes for deployment
	# Add staticresources for frontend changes
	for file in "${filesToDeploy[@]}"
	do
		if [[ $file == $'forceapp/yarn.lock' || $file == $'forceapp/sfdx/assets'* || $file == $'forceapp/sfdx/config'* || $file == $'forceapp/sfdx/manifest'* ]]; then
			continue
		elif [[ $file == $'forceapp/translation'* || $file == $'forceapp/apps'*  ]]; then
			filePathArray=($(echo "$file" | tr '\/' '\n'))
			if [[ "${COMMON_FRONT_MODULES[*]}" == *"${filePathArray[2]}"* || $file == $'forceapp/translation'* ]]; then
				buildAll=1
				sfdxFiles="$sfdxFiles,force-app/main/default/staticresources"
				#break
			elif [[ "${FRONT_MODULES[*]}" == *"${filePathArray[2]}"* && "$jsModules" != *"${filePathArray[2]//[-]/_}"* ]]; then
				formattedModule=${filePathArray[2]//[-]/_}
				jsModules="$jsModules,${formattedModule}"
				sfdxFiles="$sfdxFiles,force-app/main/default/staticresources/${formattedModule}"
			fi
			echo " --FE-- $file"
			((jsFileCount++))
		elif [[ $file == $'forceapp/sfdx'* ]]; then
			echo " --BE-- $file"
			sfdxFiles="$sfdxFiles,${file#"$DIR_PREFIX"}"
		fi
	done

	# Build specific or all frontend changes as per the diff
	jsModules="${jsModules#","}"
	if [[ "$jsFileCount" -gt 0 ]]; then
		printf '%s%s%s\n' $COLOR_ALERT 'Running gulp build for frontend changes' $COLOR_REST
		if [[ "$buildAll" -gt 0 ]]; then
			(cd .. && ${GULP_PATH} build --color)
		else
			(cd .. && ${GULP_PATH} build --component ${jsModules//[-]/_} --color)
		fi
	else
		printf '%s%s%s\n' $COLOR_ALERT 'Gulp build not needed' $COLOR_REST
	fi

	# Deploy all relevant changes to SF environment
	sfdxFiles="${sfdxFiles#","}"
	echo "$sfdxFiles"
	if [[ $sfdxFiles == '' ]]; then
		printf '%s%s%s\n' $COLOR_ALERT 'Deployment not needed' $COLOR_REST
	else
		printf '%s%s%s\n' $COLOR_ALERT 'Deploying to Salesforce' $COLOR_REST
		result=`(cd ../../forceapp/sfdx/ && sfdx force:source:deploy -p $sfdxFiles -u ${USER_NAME})`
	fi
fi

if [[ $result == $"=== Deployed Source"* ]]; then
	# Save the newly deployed commit SHA to git-history
	echo "$lastLocalCommit" > 'git-history.txt'
	`git update-index --skip-worktree git-history.txt`
	echo "Successful Deploy"
	echo "$result"
else
	echo "PARTIAL DEPLOY FAILED, PLEASE DEPLOY ALL"
	echo "$result"
fi

# Log time
duration=$SECONDS
printf '%sTotal time taken = %.2f minutes %s\n' $COLOR_ALERT $(echo "$duration/60" | bc -l ) $COLOR_REST
