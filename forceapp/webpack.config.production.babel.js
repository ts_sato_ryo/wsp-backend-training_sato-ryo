import HtmlWebpackPlugin from 'html-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import path from 'path';
import _ from 'lodash';

import createWebpackConfig from './webpack.config.babel';

const env = process.env.SF_ENV || 'vfp';
process.env.NODE_ENV = 'production';

const SFDX_BASE_PATH = 'sfdx/force-app/main/default';
const componentList =
  (process.argv[3] === '--component' && process.argv[4].split(',')) || [];

const webpack = createWebpackConfig(env, componentList);

const apps = [
  // PC
  {
    entry: 'admin_pc',
    name: 'Admin',
    pageTitle: 'Admin',
  },
  {
    entry: 'approvals_pc',
    name: 'Approval',
    pageTitle: 'Approval',
  },
  {
    entry: 'expenses_pc',
    name: 'Expenses',
    pageTitle: 'Expenses',
  },
  {
    entry: 'finance_approval_pc',
    name: 'FinanceApproval',
    pageTitle: 'FinanceApproval',
  },
  {
    entry: 'oauth_result_view',
    name: 'OAuthResultView',
    pageTitle: 'OAuthResultView', // Set only those without tabs individually
  },
  {
    entry: 'planner_pc',
    name: 'Planner',
    pageTitle: 'Planner',
  },
  {
    entry: 'requests_pc',
    name: 'Requests',
    pageTitle: 'Requests',
  },
  {
    entry: 'team_pc',
    name: 'Team',
    pageTitle: 'Team',
  },
  {
    entry: 'timesheet_pc',
    name: 'TimeAttendance',
    label: 'Time Attendance',
    pageTitle: 'TimeAttendance',
  },
  {
    entry: 'timesheet_pc_leave',
    name: 'TimesheetLeave',
    label: 'Timesheet Leave',
    pageTitle: 'TimeAttendance',
  },
  {
    entry: 'timesheet_pc_summary',
    name: 'TimesheetSummary',
    label: 'Timesheet Summary',
    pageTitle: 'TimeAttendance',
  },
  {
    entry: 'tracking_pc',
    name: 'Tracking',
    pageTitle: 'Tracking',
  },
  {
    entry: 'expenses_pc_print',
    name: 'ExpenseReportPrint',
    pageTitle: 'Expenses',
  },

  // Mobile
  {
    name: 'ApprovalMobile',
    entry: 'mobile_app',
    entryPath: 'approval/list',
    mobile: true,
    pageTitle: 'Approval',// same as PC not show in Both Mobile App. and Mobile Browser
  },
  {
    name: 'ExpenseMobile',
    entry: 'mobile_app',
    entryPath: 'expense/route/new',
    mobile: true,
    pageTitle: 'Expenses',
  },
  {
    name: 'ExpenseRecord',
    entry: 'mobile_app',
    entryPath: 'expense/record/new/general',
    mobile: true,
    pageTitle: 'Expenses',
  },
  {
    name: 'ExpenseReport',
    entry: 'mobile_app',
    entryPath: 'expense/report/list',
    mobile: true,
    pageTitle: 'Expenses',
  },
  {
    name: 'TimesheetDailyMobile',
    entry: 'mobile_app',
    entryPath: 'attendance/timesheet-daily',
    mobile: true,
    pageTitle: 'TimeAttendance',
  },
  {
    name: 'TimesheetMonthlyMobile',
    entry: 'mobile_app',
    entryPath: 'attendance/timesheet-monthly',
    mobile: true,
    pageTitle: 'TimeAttendance',
  },
  {
    name: 'TimestampMobile',
    entry: 'mobile_app',
    entryPath: 'attendance/timestamp',
    mobile: true,
    pageTitle: 'TimeAttendance',
  },
  {
    name: 'TrackingDailyMobile',
    entry: 'mobile_app',
    entryPath: 'tracking/tracking-daily',
    mobile: true,
    pageTitle: 'Tracking',
  },
  {
    name: 'UploadReceipt',
    entry: 'mobile_app',
    entryPath: 'expense/receipt/upload',
    mobile: true,
    pageTitle: 'Expenses',
  },
];

const prodConfig = {
  mode: 'production',
  ...webpack,

  entry: {
    // Omit dev tools
    ..._(webpack.entry)
      .omit('test_api')
      .omit('subapps_debug')
      .value(),
  },

  optimization: {
    splitChunks: _.isEmpty(componentList)
      ? {
          chunks: 'all',
          name: true,
          cacheGroups: {
            react: {
              test: /[\\/]node_modules[\\/](react|react-dom|redux|reduex-logger|redux-thunk|reselect)[\\/]/,
              name: 'react',
              chunks: 'all',
            },
            vendor: {
              test: /[\\/]node_modules[\\/](lodash|moment|moment-timezone)[\\/]/,
              name: 'vendor',
              chunks: 'all',
            },
          },
        }
      : {},
    minimizer: [
      new TerserPlugin({
        extractComments: true,
        sourceMap: false,
        parallel:
          // On CircleCI, We have to set number of CPUs to parallel in order to
          // avoid crushing build job.
          //
          // The container of CircleCI appears to have a massive number of CPUs
          // from Node.js.
          // This causes a job failure due to spawning a lager number of processes.
          process.env.CIRCLECI ? 2 : true,
        cache: true,
        terserOptions: {
          warnings: false,
          ie8: false,
          safari10: false,
        },
      }),
    ],
  },

  output: {
    ...webpack.output,
    filename: '[name]/js/[name].js',
    path: path.resolve(__dirname, SFDX_BASE_PATH, 'staticresources'),
    publicPath: '__SET_PUBLIC_PATH_IN_VISUAL_FORCE_PAGE_ON_THE_FLY__',
  },

  plugins: [
    ...webpack.plugins,

    ..._(apps)
      .flatMap((app) => [
        new HtmlWebpackPlugin({
          inject: false,
          minify: false,
          chunks: [app.entry],
          filename: path.resolve(
            __dirname,
            SFDX_BASE_PATH,
            'pages',
            `${app.name}.page`
          ),
          template: path.resolve(__dirname, 'templates/index.production.ejs'),
          ...app,
        }),
        new HtmlWebpackPlugin({
          inject: false,
          minify: false,
          filename: path.resolve(
            __dirname,
            SFDX_BASE_PATH,
            'pages',
            `${app.name}.page-meta.xml`
          ),
          template: path.resolve(__dirname, 'templates/index-meta.xml.ejs'),
          ...app,
        }),
        new HtmlWebpackPlugin({
          inject: false,
          minify: false,
          filename: path.resolve(
            __dirname,
            SFDX_BASE_PATH,
            'staticresources',
            `${app.entry}.resource-meta.xml`
          ),
          template: path.resolve(
            __dirname,
            'templates/static-resource-meta.xml.ejs'
          ),
          ...app,
        }),
      ])
      .value(),
  ],
};

const debug = (config) => {
  const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
  const smp = new SpeedMeasurePlugin();
  return smp.wrap(config);
};

export default (process.env.DEBUG ? debug(prodConfig) : prodConfig);
