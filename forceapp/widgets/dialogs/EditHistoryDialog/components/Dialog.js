// @flow

import React from 'react';

import { type EditHistoryItem } from '../../../../apps/domain/models/exp/FinanceApproval';
import DialogFrame from '../../../../apps/commons/components/dialogs/DialogFrame';
import EditHistoryTable from '../../../../apps/commons/components/exp/History';
import Button from '../../../../apps/commons/components/buttons/Button';
import msg from '../../../../apps/commons/languages';

import './Dialog.scss';

const ROOT = 'widgets-edit-history-dialog';

type Props = {
  modificationList: EditHistoryItem[],
  onHide: Function,
};

export default class Dialog extends React.Component<Props> {
  render() {
    return (
      <DialogFrame
        className={ROOT}
        title={msg().Exp_Lbl_EditHistory}
        hide={this.props.onHide}
        footer={
          <DialogFrame.Footer>
            <Button onClick={this.props.onHide}>{msg().Com_Btn_Close}</Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__table`}>
          <EditHistoryTable modificationList={this.props.modificationList} />
        </div>
      </DialogFrame>
    );
  }
}
