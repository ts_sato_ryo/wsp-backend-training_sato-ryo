// @flow

import * as React from 'react';

import msg from '../../../../apps/commons/languages';
import { type ApproverShowObj } from '../models/SwitchApprover';

import FixedHeaderTable, {
  HeaderRow,
  HeaderCell,
  BodyRow,
  BodyCell,
} from '../../../../apps/commons/components/FixedHeaderTable';

import './SwitchApproverTable.scss';

const ROOT = 'ts-expenses-switch-approver-table';
const CELL_CLASS = `${ROOT}__cell ${ROOT}__column`;

type Props = {
  originalApprovers: ApproverShowObj[],
  onClickApprover: (ApproverShowObj) => void,
};

export default class SwitchApproverTable extends React.Component<Props> {
  renderRow() {
    const items = this.props.originalApprovers;
    const rows = items.map<React.Element<typeof BodyRow>>((item, idx) => {
      return (
        <BodyRow key={idx} onClick={() => this.props.onClickApprover(item)}>
          <BodyCell className={`${CELL_CLASS}-emp-photo-url`}>
            <img
              className={`${ROOT}__icon`}
              src={item.employeePhotoUrl}
              alt="Employee"
            />
            <div className={`${ROOT}__count`}>{item.totalRequestCount}</div>
          </BodyCell>
          <BodyCell className={`${CELL_CLASS}-emp-name`}>
            {item.employeeName}
          </BodyCell>
          <BodyCell className={`${CELL_CLASS}-emp-code`}>
            {item.employeeCode}
          </BodyCell>
          <BodyCell className={`${CELL_CLASS}-dep-name`}>
            {item.departmentName}
          </BodyCell>
          <BodyCell className={`${CELL_CLASS}-dep-code`}>
            {item.departmentCode}
          </BodyCell>
          <BodyCell className={`${CELL_CLASS}-title`}>{item.title}</BodyCell>
        </BodyRow>
      );
    });
    return rows;
  }

  render() {
    return (
      <div className={ROOT}>
        <FixedHeaderTable scrollableClass={`${ROOT}__scrollable`}>
          <HeaderRow>
            <HeaderCell className={`${CELL_CLASS}-emp-photo-url`}> </HeaderCell>
            <HeaderCell className={`${CELL_CLASS}-emp-name`}>
              {msg().Com_Lbl_EmployeeName}
            </HeaderCell>
            <HeaderCell className={`${CELL_CLASS}-emp-code`}>
              {msg().Com_Lbl_EmployeeCode}
            </HeaderCell>
            <HeaderCell className={`${CELL_CLASS}-dep-name`}>
              {msg().Com_Lbl_DepartmentName}
            </HeaderCell>
            <HeaderCell className={`${CELL_CLASS}-dep-code`}>
              {msg().Com_Lbl_DepartmentCode}
            </HeaderCell>
            <HeaderCell className={`${CELL_CLASS}-title`}>
              {msg().Com_Lbl_Title}
            </HeaderCell>
          </HeaderRow>
          {this.renderRow()}
        </FixedHeaderTable>
      </div>
    );
  }
}
