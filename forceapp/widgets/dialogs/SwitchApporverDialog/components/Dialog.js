// @flow

import React from 'react';

import { type ApproverShowObj } from '../models/SwitchApprover';
import DialogFrame from '../../../../apps/commons/components/dialogs/DialogFrame';
import Button from '../../../../apps/commons/components/buttons/Button';
import msg from '../../../../apps/commons/languages';
import SwitchApproverTable from './SwitchApproverTable';
import './Dialog.scss';

const ROOT = 'widgets-switch-approver-dialog';

type Props = {
  originalApprovers: ApproverShowObj[],
  onHide: () => void,
  onClickApprover: (ApproverShowObj) => void,
};

export default class SwitchApproverDialog extends React.Component<Props> {
  render() {
    return (
      <DialogFrame
        className={ROOT}
        title={msg().Com_Lbl_SwitchEmployee}
        hide={this.props.onHide}
        footer={
          <DialogFrame.Footer>
            <Button onClick={this.props.onHide}>{msg().Com_Btn_Cancel}</Button>
          </DialogFrame.Footer>
        }
      >
        <SwitchApproverTable
          originalApprovers={this.props.originalApprovers}
          onClickApprover={this.props.onClickApprover}
        />
      </DialogFrame>
    );
  }
}
