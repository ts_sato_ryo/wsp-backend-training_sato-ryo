// @flow
import { type Dispatch } from 'redux';

export const ACTIONS = {
  SHOW: 'WIDGETS/DIALOGS/SWITCH_APPROVER_DIALOG/UI/SHOW',
  SELECT: 'WIDGETS/DIALOGS/SWITCH_APPROVER_DIALOG/UI/EMPLOYEE/SELECT',
  HIDE: 'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/HIDE',
};

type State = { isVisible: boolean };

type ShowProxyDialog = {|
  type: 'WIDGETS/DIALOGS/SWITCH_APPROVER_DIALOG/UI/SHOW',
|};

type HideProxyDialog = {|
  type: 'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/HIDE',
|};

type SelectEmployee = {|
  type: 'WIDGETS/DIALOGS/SWITCH_APPROVER_DIALOG/UI/EMPLOYEE/SELECT',
|};

type Actions = ShowProxyDialog | HideProxyDialog | SelectEmployee;

export const actions = {
  show: () => (dispatch: Dispatch<any>) => {
    dispatch({
      type: ACTIONS.SHOW,
    });
  },

  hide: () => ({
    type: ACTIONS.HIDE,
  }),

  selectEmployee: () => ({
    type: ACTIONS.SELECT,
  }),
};

const initialState = {
  isVisible: false,
};

export default (state: State = initialState, action: Actions) => {
  switch (action.type) {
    case ACTIONS.SHOW:
      return {
        ...state,
        isVisible: true,
      };
    case ACTIONS.SELECT:
      return {
        ...state,
      };
    case ACTIONS.HIDE:
      return initialState;
    default:
      return state;
  }
};
