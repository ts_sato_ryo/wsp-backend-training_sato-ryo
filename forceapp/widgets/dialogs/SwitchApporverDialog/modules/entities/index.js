// @flow
import { type Dispatch } from 'redux';
import {
  withLoading,
  catchApiError,
} from '../../../../../apps/commons/actions/app';

import {
  type ApproverShowObj,
  getOriginalApproverList,
} from '../../models/SwitchApprover';

type State = {
  originalApprovers: ApproverShowObj[],
  pendingRequestCount: number,
};
const ACTIONS = {
  LIST: 'WIDGETS/DIALOGS/SWITCH_APPROVER_DIALOG/ENITITES/APPROVERS/LIST',
};

const listSuccess = (result) => ({
  type: ACTIONS.LIST,
  payload: result,
});

type ListOriginalApprovers = {|
  type: 'WIDGETS/DIALOGS/SWITCH_APPROVER_DIALOG/ENITITES/APPROVERS/LIST',
  payload: ApproverShowObj[],
|};

type Actions = ListOriginalApprovers;

export const actions = {
  list: (empId: string) => (dispatch: Dispatch<any>) =>
    dispatch(
      withLoading(() =>
        getOriginalApproverList(empId).then((result: ApproverShowObj[]) => {
          dispatch(listSuccess(result));
        })
      )
    ).catch((err) => dispatch(catchApiError(err, { isContinuable: true }))),
};

const initialState = {
  originalApprovers: [],
  pendingRequestCount: 0,
};

export default (state: State = initialState, action: Actions) => {
  switch (action.type) {
    case ACTIONS.LIST:
      const originalApprovers = action.payload;
      const pendingRequestCount = originalApprovers.reduce(
        (x, y) => x + y.totalRequestCount,
        0
      );
      return {
        originalApprovers,
        pendingRequestCount,
      };
    default:
      return state;
  }
};
