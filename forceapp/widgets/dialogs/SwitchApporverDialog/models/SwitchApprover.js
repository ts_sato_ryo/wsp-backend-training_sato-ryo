// @flow
import Api from '../../../../apps/commons/api';

export type ApproverShowObj = {
  employeeBaseId: string,
  employeeCode: string,
  employeeName: string,
  departmentCode: string,
  departmentName: string,
  employeePhotoUrl: string,
  title: string,
  expPreApprovalRequestCount: number,
  expReportRequestCount: number,
  totalRequestCount: number,
};

// eslint-disable-next-line import/prefer-default-export
export const getOriginalApproverList = (
  empId: string
): Promise<ApproverShowObj[]> => {
  return Api.invoke({
    path: '/approval/request/original-approver/list',
    param: {
      empId,
    },
  }).then((result: { originalApproverList: Array<ApproverShowObj> }) => {
    return result.originalApproverList;
  });
};
