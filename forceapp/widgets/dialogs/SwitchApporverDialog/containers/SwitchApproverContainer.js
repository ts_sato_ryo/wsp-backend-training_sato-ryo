import { connect } from 'react-redux';
import { actions as proxyEmployeeInfoActions } from '../../../../apps/commons/modules/proxyEmployeeInfo';
import { actions as switchApproverDialogUIActions } from '../modules/ui';
import { actions as advSearchStatusActions } from '../../../../apps/approvals-pc/modules/ui/expenses/list/advSearch/statusList';
import { actions as advSearchEmployeeActions } from '../../../../apps/approvals-pc/modules/ui/expenses/list/advSearch/empBaseIdList';
import { actions as advSearchRequestDateActions } from '../../../../apps/approvals-pc/modules/ui/expenses/list/advSearch/requestDateRange';

import SwitchApproverDialog from '../components/Dialog';

const mapStateToProps = (state) => ({
  originalApprovers:
    state.widgets.SwitchApproverDialog.entities.originalApprovers,
  selectedTab: state.ui.tabs.selected,
});

const mapDispatchToProps = {
  setProxyEmployee: proxyEmployeeInfoActions.set,
  onClickCloseButton: switchApproverDialogUIActions.hide,
  selectEmployee: switchApproverDialogUIActions.selectEmployee,
  clearStatus: advSearchStatusActions.clear,
  clearEmployee: advSearchEmployeeActions.clear,
  clearSubmitDate: advSearchRequestDateActions.clear,
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickApprover: (proxyUser) => {
    dispatchProps.onClickCloseButton();
    dispatchProps.setProxyEmployee({
      id: proxyUser.employeeBaseId,
      employeeCode: proxyUser.employeeCode,
      employeeName: proxyUser.employeeName,
      departmentCode: proxyUser.departmentCode,
      departmentName: proxyUser.departmentName,
      employeePhotoUrl: proxyUser.employeePhotoUrl,
      expPreApprovalRequestCount: proxyUser.expPreApprovalRequestCount || 0,
      expReportRequestCount: proxyUser.expReportRequestCount || 0,
      title: proxyUser.title,
    });
    dispatchProps.selectEmployee();
    dispatchProps.clearStatus();
    dispatchProps.clearEmployee();
    dispatchProps.clearSubmitDate();
  },

  onHide: () => {
    dispatchProps.onClickCloseButton();
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(SwitchApproverDialog);
