// @flow

import type { ApprovalHistory } from '../../../../../../apps/domain/models/approval/request/History';

import * as constants from './constants';

type Set = {
  type: $PropertyType<constants.ActionType, 'Set'>,
  payload: ApprovalHistory[],
};

type Unset = {
  type: $PropertyType<constants.ActionType, 'Unset'>,
};

export type Action = Set | Unset;

const set = (list: ApprovalHistory[]): Set => {
  return {
    type: constants.SET,
    payload: list,
  };
};

const unset = (): Unset => {
  return { type: constants.UNSET };
};

export { set, unset };
