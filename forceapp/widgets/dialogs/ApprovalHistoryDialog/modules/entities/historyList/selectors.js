// @flow

import { createSelector } from 'reselect';
import type { ApprovalHistory } from '../../../../../../apps/domain/models/approval/request/History';

type State = $Shape<{
  widgets: {
    ApprovalHistoryDialog: {
      entities: {
        historyList: {
          allIds: string[],
          byId: { [string]: ApprovalHistory },
        },
      },
    },
  },
}>;

// $FlowFixMe
const historyListSelector = createSelector(
  (state: State): string[] => {
    return state.widgets.ApprovalHistoryDialog.entities.historyList.allIds;
  },
  (state: State): { [string]: ApprovalHistory } => {
    return state.widgets.ApprovalHistoryDialog.entities.historyList.byId;
  },
  (allIds: string[], byId: { [string]: ApprovalHistory }) => {
    return allIds.map((id) => byId[id]);
  }
);

// eslint-disable-next-line import/prefer-default-export
export { historyListSelector };
