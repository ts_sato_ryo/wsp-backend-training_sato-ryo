// @flow

export type ActionType = {
  Set: 'WIDGETS/DIALOGS/APPROVAL_HISTORY_DIALOG/SET',
  Unset: 'WIDGETS/DIALOGS/APPROVAL_HISTORY_DIALOG/UNSET',
};

const SET: $PropertyType<ActionType, 'Set'> =
  'WIDGETS/DIALOGS/APPROVAL_HISTORY_DIALOG/SET';
const UNSET: $PropertyType<ActionType, 'Unset'> =
  'WIDGETS/DIALOGS/APPROVAL_HISTORY_DIALOG/UNSET';

export { SET, UNSET };
