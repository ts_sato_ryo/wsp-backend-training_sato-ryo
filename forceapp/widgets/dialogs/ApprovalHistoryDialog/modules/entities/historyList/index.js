// @flow

import type { ApprovalHistory } from '../../../../../../apps/domain/models/approval/request/History';

import * as constants from './constants';
import * as actions from './actions';
import * as selectors from './selectors';

type State = {
  byId: { [string]: ApprovalHistory },
  allIds: string[],
};

const extractHistoryEntitiy = (historyList): { [string]: ApprovalHistory } => {
  const obj = {};

  historyList.forEach((history) => {
    obj[history.id] = {
      id: history.id || '',
      actorName: history.actorName || '',
      approveTime: history.approveTime || '',
      actorPhotoUrl: history.actorPhotoUrl || '',
      approverName: history.approverName || '',
      comment: history.comment || '',
      status: history.status || '',
      statusLabel: history.statusLabel || '',
      stepName: history.stepName || '',
      isDelegated: history.isDelegated,
    };
  });

  return obj;
};

const extractHistoryIds = (historyList: ApprovalHistory[]): string[] => {
  return historyList.map((history) => {
    return history.id;
  });
};

const createEntity = (list: ApprovalHistory[]): State => {
  return {
    byId: extractHistoryEntitiy(list),
    allIds: extractHistoryIds(list),
  };
};

const initialState: State = createEntity([]);

export default (state: State = initialState, action: actions.Action) => {
  switch (action.type) {
    case constants.SET:
      return createEntity(action.payload);
    case constants.UNSET:
      return initialState;
    default:
      return state;
  }
};

export { constants, actions, selectors };
