import { combineReducers } from 'redux';

import historyList from './historyList';

export default combineReducers({
  historyList,
});
