// @flow

import React from 'react';

import type { ApprovalHistory } from '../../../../apps/domain/models/approval/request/History';
import type { ExpApprovalHistory } from '../../../../apps/domain/models/exp/approval/request/History';
import DialogFrame from '../../../../apps/commons/components/dialogs/DialogFrame';
import HistoryTable from '../../../../apps/commons/components/tables/HistoryTable';
import Button from '../../../../apps/commons/components/buttons/Button';
import msg from '../../../../apps/commons/languages';

import imgIconApprovalHistory from '../../../../apps/commons/images/iconApprovalHistory.png';

import './Dialog.scss';

const ROOT = 'widgets-approval-history-dialog';

type Props = {
  historyList: Array<ApprovalHistory> | Array<ExpApprovalHistory>,
  onHide: Function,
};

export default class Dialog extends React.Component<Props> {
  render() {
    return (
      <DialogFrame
        className={ROOT}
        titleIcon={imgIconApprovalHistory}
        title={msg().Com_Lbl_ApprovalHistory}
        hide={this.props.onHide}
        footer={
          <DialogFrame.Footer>
            <Button onClick={this.props.onHide}>{msg().Com_Btn_Close}</Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__table`}>
          <HistoryTable historyList={this.props.historyList} />
        </div>
      </DialogFrame>
    );
  }
}
