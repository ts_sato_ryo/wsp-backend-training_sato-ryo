import reducer, {
  constants,
  actions,
  selectors,
} from '../modules/entities/historyList';

import * as mocks from './mocks';

describe('actions/set', () => {
  describe('set', () => {
    test('return correct type', () => {
      expect(actions.set().type).toBe(constants.SET);
    });

    test('pass arg to palyload', () => {
      const arg = [];

      expect(actions.set(arg).payload).toBe(arg);
    });
  });

  describe('unset', () => {
    test('return correct type', () => {
      expect(actions.unset().type).toBe(constants.UNSET);
    });
  });
});

describe('actions/unset', () => {
  test('return correct type', () => {
    expect(actions.unset().type).toBe(constants.UNSET);
  });
});

describe('reducer', () => {
  test('return correct initial value', () => {
    expect(reducer(undefined, { type: 'DUMMY' })).toEqual(
      mocks.dummyPlentyHistoyList
    );
  });

  describe('SET action', () => {
    test('when pass empty object, return empty entities', () => {
      const result = reducer(undefined, {
        type: constants.SET,
        payload: mocks.onlyIdList,
      });
      expect(result).toEqual(mocks.onlyIdResult);
    });

    test('when pass two object, return two entities', () => {
      const result = reducer(undefined, {
        type: constants.SET,
        payload: mocks.twoList,
      });
      expect(result).toEqual(mocks.twoResult);
    });
  });

  test('no change, when pass unrelated action', () => {
    const state = reducer(undefined, {
      type: constants.SET,
      payload: mocks.twoList,
    });
    const result = reducer(state, { type: 'DUMMY' });
    expect(result).toEqual(mocks.twoResult);
  });
});

/**
 * { allIds: [...], byId: {...} }のオブジェクトをstate treeに配置された構造に配置して返す
 */
const convertToStateStructure = (entities) => {
  return {
    widgets: {
      ApprovalHistoryDialog: {
        entities: {
          historyList: {
            ...entities,
          },
        },
      },
    },
  };
};

describe('selectors', () => {
  describe('historyListSelector', () => {
    test('has two entities, return array list', () => {
      const result = selectors.historyListSelector(
        convertToStateStructure(mocks.twoResult)
      );
      expect(result).toEqual(mocks.twoList);
    });

    test('state is empty, return empty array', () => {
      const result = selectors.historyListSelector(
        convertToStateStructure(mocks.dummyPlentyHistoyList)
      );
      expect(result).toEqual([]);
    });
  });
});
