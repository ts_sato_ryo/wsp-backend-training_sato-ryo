export const dummyPlentyHistoyList = {
  allIds: [],
  byId: {},
};

export const onlyIdList = [{ id: 'hoge' }];

export const onlyIdResult = {
  allIds: ['hoge'],
  byId: {
    hoge: {
      id: 'hoge',
      actorName: '',
      actorPhotoUrl: '',
      approveTime: '',
      approverName: '',
      comment: '',
      status: '',
      statusLabel: '',
      stepName: '',
    },
  },
};

export const twoList = [
  {
    id: 'hoge1',
    actorName: 'actorName1',
    actorPhotoUrl: 'actorPhotoUrl1',
    approveTime: '2004-04-01T12:00Z',
    approverName: 'approverName1',
    comment: 'comment1',
    status: 'status1',
    statusLabel: 'statusLabel1',
    stepName: 'stepName1',
  },
  {
    id: 'hoge2',
    actorName: 'actorName2',
    actorPhotoUrl: 'actorPhotoUrl2',
    approveTime: '2005-04-01T12:00Z',
    approverName: 'approverName2',
    comment: 'comment2',
    status: 'status2',
    statusLabel: 'statusLabel2',
    stepName: 'stepName2',
  },
];

export const twoResult = {
  allIds: ['hoge1', 'hoge2'],
  byId: {
    hoge1: {
      id: 'hoge1',
      actorName: 'actorName1',
      actorPhotoUrl: 'actorPhotoUrl1',
      approveTime: '2004-04-01T12:00Z',
      approverName: 'approverName1',
      comment: 'comment1',
      status: 'status1',
      statusLabel: 'statusLabel1',
      stepName: 'stepName1',
    },
    hoge2: {
      id: 'hoge2',
      actorName: 'actorName2',
      actorPhotoUrl: 'actorPhotoUrl2',
      approveTime: '2005-04-01T12:00Z',
      approverName: 'approverName2',
      comment: 'comment2',
      status: 'status2',
      statusLabel: 'statusLabel2',
      stepName: 'stepName2',
    },
  },
};
