import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ProxyEmployeeSelectDialog from '../components/ProxyEmployeeSelectDialog';

import { selectors as entitiesSelectors } from '../modules/entities';
import { actions as uiActions } from '../modules/ui';

const mapStateToProps = (state) => ({
  companyId: state.common.userSetting.companyId,
  departmentId: state.common.userSetting.departmentId,
  isVisible: state.widgets.ProxyEmployeeSelectDialog.ui.isVisible,
  searchStrategy: state.widgets.ProxyEmployeeSelectDialog.ui.searchStrategy,
  isSearchByQueriesExecuted:
    state.widgets.ProxyEmployeeSelectDialog.ui.isSearchByQueriesExecuted,
  departmentCodeQuery:
    state.widgets.ProxyEmployeeSelectDialog.ui.departmentCodeQuery,
  departmentNameQuery:
    state.widgets.ProxyEmployeeSelectDialog.ui.departmentNameQuery,
  employeeCodeQuery:
    state.widgets.ProxyEmployeeSelectDialog.ui.employeeCodeQuery,
  employeeNameQuery:
    state.widgets.ProxyEmployeeSelectDialog.ui.employeeNameQuery,
  titleQuery: state.widgets.ProxyEmployeeSelectDialog.ui.titleQuery,
  employees: entitiesSelectors.employeesButUsersSelector(state),
  selectedEmployeeId:
    state.widgets.ProxyEmployeeSelectDialog.ui.selectedEmployeeId,
  selectedEmployee: entitiesSelectors.selectedEmployeeSelector(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onSwitchSearchStrategy: uiActions.switchSearchStrategy,
      onEditDepartmentCodeQuery: uiActions.editDepartmentCodeQuery,
      onEditDepartmentNameQuery: uiActions.editDepartmentNameQuery,
      onEditEmployeeCodeQuery: uiActions.editEmployeeCodeQuery,
      onEditEmployeeNameQuery: uiActions.editEmployeeNameQuery,
      onEditTitleQuery: uiActions.editTitleQuery,
      onSearchByQueries: uiActions.searchByQueries,
      onSelectEmployee: uiActions.selectEmployee,
      onDecide: uiActions.decide,
      onCancel: uiActions.hide,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onSwitchSearchStrategy: (newSearchStrategy) => {
    dispatchProps.onSwitchSearchStrategy(
      newSearchStrategy,
      stateProps.companyId,
      stateProps.departmentId
    );
  },
  onSearchByQueries: () => {
    dispatchProps.onSearchByQueries(stateProps.companyId, {
      departmentCodeQuery: stateProps.departmentCodeQuery,
      departmentNameQuery: stateProps.departmentNameQuery,
      employeeCodeQuery: stateProps.employeeCodeQuery,
      employeeNameQuery: stateProps.employeeNameQuery,
      titleQuery: stateProps.titleQuery,
    });
  },
  onDecide: () => {
    ownProps.onDecide(stateProps.selectedEmployee).then((isSuccess) => {
      if (isSuccess) {
        dispatchProps.onDecide(stateProps.selectedEmployee);
      }
    });
  },
  onCancel: () => {
    dispatchProps.onCancel();
    ownProps.onCancel();
  },
});

const ProxyEmployeeSelectDialogContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ProxyEmployeeSelectDialog);

ProxyEmployeeSelectDialogContainer.defaultProps = {
  onDecide: () => {},
  onCancel: () => {},
};

export default ProxyEmployeeSelectDialogContainer;
