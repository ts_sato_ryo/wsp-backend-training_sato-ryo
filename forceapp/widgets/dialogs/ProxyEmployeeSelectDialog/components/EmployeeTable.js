import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { IconSettings, Icon } from '@salesforce/design-system-react';

import Employee from '../models/Employee';
import SearchStrategy from '../models/SearchStrategy';

import msg from '../../../../apps/commons/languages';

import './EmployeeTable.scss';

const ROOT = 'widgets-proxy-employee-select-dialog-employee-table';

const Table = (props) => (
  <table
    role="grid"
    className={`slds-table slds-table--bordered slds-table--cell-buffer slds-no-row-hover ${ROOT}__table`}
  >
    {props.header}
    {props.children}
  </table>
);

Table.propTypes = {
  header: PropTypes.node,
  children: PropTypes.node.isRequired,
};

const TableCaptions = (props) => (
  <thead className={`${ROOT}__head`}>
    <tr className="slds-line-height--reset">{props.children}</tr>
  </thead>
);

TableCaptions.propTypes = {
  children: PropTypes.node.isRequired,
};

const TableCaption = (props) => (
  <th
    scope="col"
    aria-label={props.label}
    className="slds-text-title--caps"
    style={{ width: props.width }}
  >
    <span title={props.label}>{props.label}</span>
  </th>
);

TableCaption.propTypes = {
  label: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
};

const TextTableCell = (props) => (
  <td>
    {props.textTruncation ? (
      <span title={props.text} className="slds-truncate">
        {props.text}
      </span>
    ) : (
      props.text
    )}
  </td>
);

TextTableCell.propTypes = {
  text: PropTypes.string.isRequired,
  textTruncation: PropTypes.bool,
};

TextTableCell.defaultProps = {
  textTruncation: false,
};

const CustomTableCell = (props) => <td>{props.children}</td>;

CustomTableCell.propTypes = {
  children: PropTypes.node.isRequired,
};

// TODO Stop setting interactions to table elements (and table itself)
/* eslint-disable jsx-a11y/no-static-element-interactions */
const SelectableEmployeeRow = (props) => (
  <tr
    tabIndex="0"
    className={classNames(`${ROOT}__row`, {
      [`${ROOT}__row--active`]: props.isSelected,
    })}
    onClick={!props.isSelected ? props.onClickEmployeeRow : null}
    onKeyPress={!props.isSelected ? props.onKeyPressOnEmployeeRow : null}
    data-employee-id={props.employee.id}
  >
    <CustomTableCell>
      <div className="slds-grid slds-grid--vertical-align-center">
        <div className="slds-shrink slds-p-right--small">
          <img
            role="presentation"
            src={props.employee.employeePhotoUrl}
            width="32"
            height="32"
            className={`${ROOT}__emp-photo`}
          />
        </div>
        <div className="slds-grow">{props.employee.employeeName}</div>
      </div>
    </CustomTableCell>

    <TextTableCell text={props.employee.employeeCode} />
    <TextTableCell text={props.employee.departmentName} />
    <TextTableCell text={props.employee.departmentCode} />
    <TextTableCell text={props.employee.title} />
  </tr>
);
/* eslint-enable jsx-a11y/no-static-element-interactions */

SelectableEmployeeRow.propTypes = {
  employee: PropTypes.instanceOf(Employee).isRequired,
  isSelected: PropTypes.bool.isRequired,
  onClickEmployeeRow: PropTypes.func.isRequired,
  onKeyPressOnEmployeeRow: PropTypes.func.isRequired,
};

const EmployeeRows = (props) => (
  <tbody>
    {props.employees.map((employee) => (
      <SelectableEmployeeRow
        key={employee.id}
        employee={employee}
        isSelected={employee.id === props.selectedEmployeeId}
        onClickEmployeeRow={props.onClickEmployeeRow}
        onKeyPressOnEmployeeRow={props.onKeyPressOnEmployeeRow}
      />
    ))}
  </tbody>
);

// Note: ESLint warns some props are unused. It seems an ESLint bug.
EmployeeRows.propTypes = {
  employees: PropTypes.arrayOf(PropTypes.instanceOf(Employee).isRequired)
    .isRequired,
  /* eslint-disable react/no-unused-prop-types */
  selectedEmployeeId: PropTypes.string,
  onClickEmployeeRow: PropTypes.func.isRequired,
  /* eslint-enable react/no-unused-prop-types */
  onKeyPressOnEmployeeRow: PropTypes.func.isRequired,
};

EmployeeRows.defaultProps = {
  selectedEmployeeId: null,
};

const MessageBoardContainer = (props) => (
  <tbody>
    <tr>
      <td colSpan="5">
        <div className="slds-align--absolute-center slds-grid slds-grid--vertical slds-p-vertical--medium">
          {props.children}
        </div>
      </td>
    </tr>
  </tbody>
);

MessageBoardContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const SearchGuideMessageBoard = () => (
  <MessageBoardContainer>
    <div className="slds-icon--container slds-icon-utility-search slds-p-bottom--x-small">
      <Icon
        category="utility"
        name="search"
        colorVariant="default"
        size="large"
      />
    </div>
    <p className="slds-text-color--weak">{msg().Com_Msg_SearchGuideMessage}</p>
  </MessageBoardContainer>
);

const NotFoundMessageBoard = () => (
  <MessageBoardContainer>
    <div className="slds-icon--container slds-icon-utility-search slds-p-bottom--x-small">
      <Icon
        category="utility"
        name="search"
        colorVariant="default"
        size="large"
      />
    </div>
    <p className="slds-text-color--weak">{msg().Com_Err_NoEmployeesFound}</p>
  </MessageBoardContainer>
);

const EmptyMessageBoard = (props) =>
  props.searchStrategy !== SearchStrategy.SEARCH_BY_QUERIES ||
  props.isSearchByQueriesExecuted ? (
    <NotFoundMessageBoard />
  ) : (
    <SearchGuideMessageBoard />
  );

EmptyMessageBoard.propTypes = {
  searchStrategy: PropTypes.number.isRequired,
  isSearchByQueriesExecuted: PropTypes.bool.isRequired,
};

export default class EmployeeTable extends React.Component {
  static get propTypes() {
    return {
      searchStrategy: PropTypes.number.isRequired,
      isSearchByQueriesExecuted: PropTypes.bool.isRequired,
      employees: PropTypes.arrayOf(PropTypes.instanceOf(Employee).isRequired)
        .isRequired,
      selectedEmployeeId: PropTypes.string,
      onSelectEmployee: PropTypes.func.isRequired,
    };
  }

  constructor() {
    super();
    this.onClickEmployeeRow = this.onClickEmployeeRow.bind(this);
    this.onKeyPressOnEmployeeRow = this.onKeyPressOnEmployeeRow.bind(this);
  }

  onClickEmployeeRow(event) {
    if (!event || event.defaultPrevented) {
      return;
    }

    const targetEmployeeId =
      event.currentTarget &&
      event.currentTarget.getAttribute('data-employee-id');
    if (!targetEmployeeId) {
      return;
    }

    event.preventDefault();
    this.props.onSelectEmployee(targetEmployeeId);
  }

  onKeyPressOnEmployeeRow(event) {
    if (!event || event.defaultPrevented) {
      return;
    }

    if (event.key === ' ' || event.key === 'Enter') {
      const targetEmployeeId =
        event.currentTarget &&
        event.currentTarget.getAttribute('data-employee-id');
      if (!targetEmployeeId) {
        return;
      }

      event.preventDefault();
      this.props.onSelectEmployee(targetEmployeeId);
    }
  }

  render() {
    const noEmployeesFound = this.props.employees.length === 0;

    return (
      <div className="slds-scrollable">
        <IconSettings iconPath={window.__icon_path__}>
          <Table
            header={
              <TableCaptions>
                <TableCaption label={msg().Com_Lbl_EmployeeName} width="30%" />
                <TableCaption label={msg().Com_Lbl_EmployeeCode} width="10%" />
                <TableCaption
                  label={msg().Com_Lbl_DepartmentName}
                  width="30%"
                />
                <TableCaption
                  label={msg().Com_Lbl_DepartmentCode}
                  width="10%"
                />
                <TableCaption label={msg().Com_Lbl_Title} width="20%" />
              </TableCaptions>
            }
            rowHover={!noEmployeesFound}
          >
            {!noEmployeesFound ? (
              <EmployeeRows
                employees={this.props.employees}
                selectedEmployeeId={this.props.selectedEmployeeId}
                onClickEmployeeRow={this.onClickEmployeeRow}
                onKeyPressOnEmployeeRow={this.onKeyPressOnEmployeeRow}
              />
            ) : (
              <EmptyMessageBoard
                searchStrategy={this.props.searchStrategy}
                isSearchByQueriesExecuted={this.props.isSearchByQueriesExecuted}
              />
            )}
          </Table>
        </IconSettings>
      </div>
    );
  }
}
