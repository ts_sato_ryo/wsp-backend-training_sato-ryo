import PropTypes from 'prop-types';
import React from 'react';

import EmployeeTable from './EmployeeTable';
import SearchForm from './SearchForm';

import Employee from '../models/Employee';

const ROOT = 'widgets-proxy-employee-select-dialog-dialog-content';

export default class DialogContent extends React.Component {
  static get propTypes() {
    return {
      searchStrategy: PropTypes.number.isRequired,
      isSearchByQueriesExecuted: PropTypes.bool.isRequired,
      departmentCodeQuery: PropTypes.string.isRequired,
      departmentNameQuery: PropTypes.string.isRequired,
      employeeCodeQuery: PropTypes.string.isRequired,
      employeeNameQuery: PropTypes.string.isRequired,
      titleQuery: PropTypes.string.isRequired,
      onSwitchSearchStrategy: PropTypes.func.isRequired,
      onEditDepartmentCodeQuery: PropTypes.func.isRequired,
      onEditDepartmentNameQuery: PropTypes.func.isRequired,
      onEditEmployeeCodeQuery: PropTypes.func.isRequired,
      onEditEmployeeNameQuery: PropTypes.func.isRequired,
      onEditTitleQuery: PropTypes.func.isRequired,
      employees: PropTypes.arrayOf(PropTypes.instanceOf(Employee).isRequired)
        .isRequired,
      selectedEmployeeId: PropTypes.string,
      onSearchByQueries: PropTypes.func.isRequired,
      onSelectEmployee: PropTypes.func.isRequired,
    };
  }

  render() {
    return (
      <main
        className={`slds-modal__content slds-grid slds-grid--vertical ${ROOT}`}
      >
        <SearchForm
          searchStrategy={this.props.searchStrategy}
          departmentCodeQuery={this.props.departmentCodeQuery}
          departmentNameQuery={this.props.departmentNameQuery}
          employeeCodeQuery={this.props.employeeCodeQuery}
          employeeNameQuery={this.props.employeeNameQuery}
          titleQuery={this.props.titleQuery}
          onSwitchSearchStrategy={this.props.onSwitchSearchStrategy}
          onEditDepartmentCodeQuery={this.props.onEditDepartmentCodeQuery}
          onEditDepartmentNameQuery={this.props.onEditDepartmentNameQuery}
          onEditEmployeeCodeQuery={this.props.onEditEmployeeCodeQuery}
          onEditEmployeeNameQuery={this.props.onEditEmployeeNameQuery}
          onEditTitleQuery={this.props.onEditTitleQuery}
          onSearchByQueries={this.props.onSearchByQueries}
        />
        <EmployeeTable
          searchStrategy={this.props.searchStrategy}
          isSearchByQueriesExecuted={this.props.isSearchByQueriesExecuted}
          employees={this.props.employees}
          selectedEmployeeId={this.props.selectedEmployeeId}
          onSelectEmployee={this.props.onSelectEmployee}
        />
      </main>
    );
  }
}
