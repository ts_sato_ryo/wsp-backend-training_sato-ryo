import React from 'react';
import PropTypes from 'prop-types';

import msg from '../../../../apps/commons/languages';

const ROOT = 'widgets-proxy-employee-select-dialog-dialog-footer';

export default class DialogFooter extends React.Component {
  static get propTypes() {
    return {
      isDecideButtonEnabled: PropTypes.bool.isRequired,
      onClickDecideButton: PropTypes.func.isRequired,
      onClickCancelButton: PropTypes.func.isRequired,
    };
  }

  render() {
    return (
      <footer className={`slds-modal__footer ${ROOT}`}>
        <button
          type="button"
          className="slds-button slds-button--neutral"
          onClick={this.props.onClickCancelButton}
        >
          {msg().Com_Btn_Cancel}
        </button>
        <button
          type="button"
          disabled={!this.props.isDecideButtonEnabled}
          className="slds-button slds-button--brand"
          onClick={this.props.onClickDecideButton}
        >
          {msg().Com_Btn_Decide}
        </button>
      </footer>
    );
  }
}
