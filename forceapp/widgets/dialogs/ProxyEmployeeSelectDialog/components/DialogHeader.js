import PropTypes from 'prop-types';
import React from 'react';
import { IconSettings, Button } from '@salesforce/design-system-react';

import msg from '../../../../apps/commons/languages';

import './DialogHeader.scss';

const ROOT = 'widgets-proxy-employee-select-dialog-dialog-header';

const CloseButton = (props) => (
  <Button
    className="slds-modal__close"
    assistiveText={{ icon: msg().Com_Btn_Close }}
    iconClassName="slds-button__icon--large"
    iconCategory="utility"
    iconName="close"
    iconSize="large"
    iconVariant="default"
    inverse
    variant="icon"
    onClick={props.onClick}
    title={msg().Com_Btn_Close}
  />
);

CloseButton.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default class DialogHeader extends React.Component {
  static get propTypes() {
    return {
      title: PropTypes.string,
      onClickCloseButton: PropTypes.func.isRequired,
    };
  }

  render() {
    return (
      <header className={`slds-modal__header ${ROOT}`}>
        <IconSettings iconPath={window.__icon_path__}>
          <CloseButton onClick={this.props.onClickCloseButton} />
          <h2
            className={`slds-text-heading slds-text-heading--medium ${ROOT}__title`}
          >
            {this.props.title || msg().Com_Lbl_SwitchEmployee}
          </h2>
        </IconSettings>
      </header>
    );
  }
}
