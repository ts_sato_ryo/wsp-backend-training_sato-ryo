import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import uuidV4 from 'uuid/v4';

import msg from '../../../../apps/commons/languages';

import SearchStrategy from '../models/SearchStrategy';

import './SearchForm.scss';

const ROOT = 'widgets-proxy-employee-select-dialog-search-form';

const RadioGroup = (props) => (
  <fieldset className="slds-form-element">
    <legend className="slds-form-element__legend slds-form-element__label">
      {props.label}
    </legend>
    <div className="slds-form-element__control slds-grid">{props.children}</div>
  </fieldset>
);

RadioGroup.propTypes = {
  label: PropTypes.string.isRequired,
  children: PropTypes.node,
};

const Radio = (props) => (
  <span className="slds-radio">
    <input
      id={props.id}
      type="radio"
      name={props.name}
      value={props.value}
      checked={!!props.isChecked}
      onChange={props.onChange}
    />
    <label htmlFor={props.id} className="slds-radio__label">
      <span className="slds-radio--faux" />
      <span className="slds-form-element__label">{props.label}</span>
    </label>
  </span>
);

Radio.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  isChecked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

const InputColumn = (props) => (
  <div
    className={classNames(`${ROOT}__input-column`, props.className)}
    style={{
      flexGrow: props.flexGrow,
      flexShrink: props.flexShrink,
      flexBasis: props.flexBasis,
    }}
  >
    <label htmlFor={props.id} className="slds-form-element__label">
      {props.label}
    </label>
    <input
      id={props.id}
      type="search"
      value={props.inputValue}
      className="slds-input"
      style={{ width: 'calc(100% + 0.5rem)' }}
      onChange={(event) => props.onChange(event.currentTarget.value, event)}
    />
  </div>
);

InputColumn.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  inputValue: PropTypes.string.isRequired,
  className: PropTypes.string,
  flexGrow: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    .isRequired,
  flexShrink: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    .isRequired,
  flexBasis: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    .isRequired,
  onChange: PropTypes.func.isRequired,
};

InputColumn.defaultProps = {
  className: '',
  flexGrow: 0,
  flexShrink: 0,
};

const SearchButton = (props) => (
  <button
    type="button"
    disabled={!props.isEnabled}
    className="slds-button slds-button--neutral"
    onClick={props.onClick}
  >
    {msg().Com_Btn_Search}
  </button>
);

SearchButton.propTypes = {
  isEnabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default class SearchForm extends React.Component {
  static get propTypes() {
    return {
      searchStrategy: PropTypes.number.isRequired,
      departmentCodeQuery: PropTypes.string.isRequired,
      departmentNameQuery: PropTypes.string.isRequired,
      employeeCodeQuery: PropTypes.string.isRequired,
      employeeNameQuery: PropTypes.string.isRequired,
      titleQuery: PropTypes.string.isRequired,
      onSwitchSearchStrategy: PropTypes.func.isRequired,
      onEditDepartmentCodeQuery: PropTypes.func.isRequired,
      onEditDepartmentNameQuery: PropTypes.func.isRequired,
      onEditEmployeeCodeQuery: PropTypes.func.isRequired,
      onEditEmployeeNameQuery: PropTypes.func.isRequired,
      onEditTitleQuery: PropTypes.func.isRequired,
      onSearchByQueries: PropTypes.func.isRequired,
    };
  }

  constructor() {
    super();
    this.radioGroupName = uuidV4();
    this.radioIds = [uuidV4(), uuidV4()];
    this.onSwitchSearchStrategy = this.onSwitchSearchStrategy.bind(this);
  }

  onSwitchSearchStrategy(event) {
    if (!event || event.defaultPrevented) {
      return;
    }

    const eventTarget = event.currentTarget;
    if (!eventTarget) {
      return;
    }

    const stringValue = eventTarget.getAttribute('value');
    if (stringValue === null || stringValue === undefined) {
      return;
    }

    const newSearchStrategy = Number(stringValue);
    if (newSearchStrategy === this.props.searchStrategy) {
      return;
    }

    this.props.onSwitchSearchStrategy(newSearchStrategy);
  }

  render() {
    return (
      <div className={ROOT}>
        <section
          className={`slds-grid slds-p-vertical--x-small slds-p-horizontal--large ${ROOT}__form-row`}
        >
          <RadioGroup label={msg().Com_Lbl_SearchEmployee}>
            <Radio
              id={this.radioIds[0]}
              label={msg().Com_Lbl_ShowEmployeesInSameDepartment}
              name={this.radioGroupName}
              value={SearchStrategy.SHOW_EMPLOYEES_IN_SAME_DEPARTMENT}
              isChecked={
                this.props.searchStrategy ===
                SearchStrategy.SHOW_EMPLOYEES_IN_SAME_DEPARTMENT
              }
              onChange={this.onSwitchSearchStrategy}
            />
            <Radio
              id={this.radioIds[1]}
              label={msg().Com_Lbl_Search}
              name={this.radioGroupName}
              value={SearchStrategy.SEARCH_BY_QUERIES}
              isChecked={
                this.props.searchStrategy === SearchStrategy.SEARCH_BY_QUERIES
              }
              onChange={this.onSwitchSearchStrategy}
            />
          </RadioGroup>
        </section>
        {this.props.searchStrategy === SearchStrategy.SEARCH_BY_QUERIES ? (
          <section
            className={`slds-grid slds-grid--vertical-align-end slds-p-bottom--x-small ${ROOT}__form-row`}
          >
            <InputColumn
              id={uuidV4()}
              label={msg().Com_Lbl_EmployeeName}
              inputValue={this.props.employeeNameQuery}
              className="slds-p-left--large slds-p-right--x-small"
              flexBasis="30%"
              onChange={this.props.onEditEmployeeNameQuery}
            />
            <InputColumn
              id={uuidV4()}
              label={msg().Com_Lbl_EmployeeCode}
              inputValue={this.props.employeeCodeQuery}
              className="slds-p-horizontal--x-small"
              flexBasis="10%"
              onChange={this.props.onEditEmployeeCodeQuery}
            />
            <InputColumn
              id={uuidV4()}
              label={msg().Com_Lbl_DepartmentName}
              inputValue={this.props.departmentNameQuery}
              className="slds-p-horizontal--x-small"
              flexBasis="30%"
              onChange={this.props.onEditDepartmentNameQuery}
            />
            <InputColumn
              id={uuidV4()}
              label={msg().Com_Lbl_DepartmentCode}
              inputValue={this.props.departmentCodeQuery}
              className="slds-p-horizontal--x-small"
              flexBasis="10%"
              onChange={this.props.onEditDepartmentCodeQuery}
            />
            <InputColumn
              id={uuidV4()}
              label={msg().Com_Lbl_Title}
              inputValue={this.props.titleQuery}
              className="slds-p-horizontal--x-small"
              flexBasis="0"
              flexGrow="1"
              onChange={this.props.onEditTitleQuery}
            />
            <div className="slds-shrink slds-p-left--x-small slds-p-right--medium">
              <SearchButton
                isEnabled={
                  this.props.employeeNameQuery !== '' ||
                  this.props.employeeCodeQuery !== '' ||
                  this.props.departmentNameQuery !== '' ||
                  this.props.departmentCodeQuery !== '' ||
                  this.props.titleQuery !== ''
                }
                onClick={this.props.onSearchByQueries}
              />
            </div>
          </section>
        ) : null}
      </div>
    );
  }
}
