import React from 'react';
import PropTypes from 'prop-types';

import Header from './DialogHeader';
import Content from './DialogContent';
import Footer from './DialogFooter';

import Employee from '../models/Employee';

import { Z_INDEX_DEFAULT } from '../../../../apps/commons/components/dialogs/DialogFrame';

const ROOT = 'widgets-proxy-employee-select-dialog';

export default class ProxyEmployeeSelectDialog extends React.Component {
  static get defaultProps() {
    return {
      zIndex: Z_INDEX_DEFAULT,
    };
  }

  static get propTypes() {
    return {
      isVisible: PropTypes.bool.isRequired,
      searchStrategy: PropTypes.number.isRequired,
      isSearchByQueriesExecuted: PropTypes.bool.isRequired,
      departmentCodeQuery: PropTypes.string.isRequired,
      departmentNameQuery: PropTypes.string.isRequired,
      employeeCodeQuery: PropTypes.string.isRequired,
      employeeNameQuery: PropTypes.string.isRequired,
      titleQuery: PropTypes.string.isRequired,
      employees: PropTypes.arrayOf(PropTypes.instanceOf(Employee).isRequired)
        .isRequired,
      selectedEmployeeId: PropTypes.string,
      onSwitchSearchStrategy: PropTypes.func.isRequired,
      onEditDepartmentCodeQuery: PropTypes.func.isRequired,
      onEditDepartmentNameQuery: PropTypes.func.isRequired,
      onEditEmployeeCodeQuery: PropTypes.func.isRequired,
      onEditEmployeeNameQuery: PropTypes.func.isRequired,
      onEditTitleQuery: PropTypes.func.isRequired,
      onSearchByQueries: PropTypes.func.isRequired,
      onSelectEmployee: PropTypes.func.isRequired,
      onDecide: PropTypes.func.isRequired,
      onCancel: PropTypes.func.isRequired,
      zIndex: PropTypes.number,
      title: PropTypes.string,
    };
  }

  // TODO Use design-system-react
  render() {
    return this.props.isVisible ? (
      // FIXME DialogFrame を使うようにしてほしいです。
      <div
        className="commons-dialog-frame__wrapper"
        style={{ zIndex: this.props.zIndex }}
      >
        <section
          role="dialog"
          className={`slds-modal slds-modal--large slds-fade-in-open ${ROOT}`}
        >
          <div className={`${ROOT}__container slds-modal__container`}>
            <Header
              title={this.props.title}
              onClickCloseButton={this.props.onCancel}
            />

            <Content
              searchStrategy={this.props.searchStrategy}
              isSearchByQueriesExecuted={this.props.isSearchByQueriesExecuted}
              departmentCodeQuery={this.props.departmentCodeQuery}
              departmentNameQuery={this.props.departmentNameQuery}
              employeeCodeQuery={this.props.employeeCodeQuery}
              employeeNameQuery={this.props.employeeNameQuery}
              titleQuery={this.props.titleQuery}
              employees={this.props.employees}
              selectedEmployeeId={this.props.selectedEmployeeId}
              onSwitchSearchStrategy={this.props.onSwitchSearchStrategy}
              onEditDepartmentCodeQuery={this.props.onEditDepartmentCodeQuery}
              onEditDepartmentNameQuery={this.props.onEditDepartmentNameQuery}
              onEditEmployeeCodeQuery={this.props.onEditEmployeeCodeQuery}
              onEditEmployeeNameQuery={this.props.onEditEmployeeNameQuery}
              onEditTitleQuery={this.props.onEditTitleQuery}
              onSearchByQueries={this.props.onSearchByQueries}
              onSelectEmployee={this.props.onSelectEmployee}
            />

            <Footer
              isDecideButtonEnabled={this.props.selectedEmployeeId !== null}
              onClickDecideButton={this.props.onDecide}
              onClickCancelButton={this.props.onCancel}
            />
          </div>
        </section>

        <div className="slds-backdrop slds-backdrop--open" />
      </div>
    ) : null;
  }
}
