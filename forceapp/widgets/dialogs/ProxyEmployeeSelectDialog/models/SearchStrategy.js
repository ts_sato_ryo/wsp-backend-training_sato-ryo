// @flow
/**
 * @readonly
 * @enum {number}
 */

export type SearchStrategyType = 1 | 2;

export default ({
  SHOW_EMPLOYEES_IN_SAME_DEPARTMENT: 1,
  SEARCH_BY_QUERIES: 2,
}: { [string]: SearchStrategyType });
