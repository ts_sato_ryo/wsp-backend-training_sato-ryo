export default class Employee {
  /**
   * Create an employee
   * @param {Object} object
   * @param {String} object.id The ID of the object
   * @param {String} object.departmentCode The code of employee's department
   * @param {String} object.departmentName The name of employee's department
   * @param {String} object.employeeCode Employee code
   * @param {String} object.employeeName Employee's name
   * @param {String} object.employeePhotoUrl The URL of avatar of employee's user
   * @param {String} object.title The name of employee's title
   */
  constructor(object) {
    this.id = object.id;
    this.departmentCode = object.departmentCode;
    this.departmentName = object.departmentName;
    this.employeeCode = object.employeeCode;
    this.employeeName = object.employeeName;
    this.employeePhotoUrl = object.employeePhotoUrl;
    this.title = object.title;
  }
}
