import {
  constants as entitiesConstants,
  actions as entitiesActions,
} from '../entities';

import SearchStrategy from '../../models/SearchStrategy';

const SHOW = Symbol('WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/SHOW');

const SWITCH_SEARCH_STRATEGY = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/SWITCH_SEARCH_STRATEGY'
);

const EDIT_DEPARTMENT_CODE_QUERY = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/EDIT_DEPARTMENT_CODE_QUERY'
);

const EDIT_DEPARTMENT_NAME_QUERY = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/EDIT_DEPARTMENT_NAME_QUERY'
);

const EDIT_EMPLOYEE_CODE_QUERY = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/EDIT_EMPLOYEE_CODE_QUERY'
);

const EDIT_EMPLOYEE_NAME_QUERY = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/EDIT_EMPLOYEE_NAME_QUERY'
);

const EDIT_TITLE_QUERY = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/EDIT_TITLE_QUERY'
);

const SELECT_EMPLOYEE = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/SELECT_EMPLOYEE'
);

const DECIDE = Symbol('WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/DECIDE');

const HIDE = Symbol('WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/UI/HIDE');

export const constants = {
  SHOW,
  HIDE,
  SWITCH_SEARCH_STRATEGY,
  EDIT_DEPARTMENT_CODE_QUERY,
  EDIT_DEPARTMENT_NAME_QUERY,
  EDIT_EMPLOYEE_CODE_QUERY,
  EDIT_EMPLOYEE_NAME_QUERY,
  EDIT_TITLE_QUERY,
  SELECT_EMPLOYEE,
};

/**
 * Create an event dispatcher to show the dialog
 * @param {String} targetCompanyId The ID of employee's company
 * @param {String} targetDepartmentId The ID of department where the user's employee belongs
 */
const show = (targetCompanyId, targetDepartmentId) => (dispatch) => {
  // TODO Call search action inside component automatically.
  // This action shuold be refactored according to LoD.
  dispatch({
    type: SHOW,
  });
  dispatch(
    entitiesActions.searchEmployeesInSameDepartment(
      targetCompanyId,
      targetDepartmentId
    )
  );
};

/**
 * @param {SearchStrategy} searchStrategy A search strategy to switch to
 * @param {String} [targetCompanyId=null] The ID of employee's company
 * @param {String} [targetDepartmentId=null] The ID of department of user's employee
 */
const switchSearchStrategy = (
  searchStrategy,
  targetCompanyId = null,
  targetDepartmentId = null
) => (dispatch) => {
  dispatch({
    type: SWITCH_SEARCH_STRATEGY,
    payload: searchStrategy,
  });

  if (searchStrategy === SearchStrategy.SHOW_EMPLOYEES_IN_SAME_DEPARTMENT) {
    dispatch(
      entitiesActions.searchEmployeesInSameDepartment(
        targetCompanyId,
        targetDepartmentId
      )
    );
  }
};

/**
 * @param {String} value The content of the textbox after edit
 * @return {Object} A redux action
 */
const editDepartmentCodeQuery = (value) => ({
  type: EDIT_DEPARTMENT_CODE_QUERY,
  payload: value,
});

/**
 * @param {String} value The content of the textbox after edit
 * @return {Object} A redux action
 */
const editDepartmentNameQuery = (value) => ({
  type: EDIT_DEPARTMENT_NAME_QUERY,
  payload: value,
});

/**
 * @param {String} value The content of the textbox after edit
 * @return {Object} A redux action
 */
const editEmployeeCodeQuery = (value) => ({
  type: EDIT_EMPLOYEE_CODE_QUERY,
  payload: value,
});

/**
 * @param {String} value The content of the textbox after edit
 * @return {Object} A redux action
 */
const editEmployeeNameQuery = (value) => ({
  type: EDIT_EMPLOYEE_NAME_QUERY,
  payload: value,
});

/**
 * @param {String} value The content of the textbox after edit
 * @return {Object} A redux action
 */
const editTitleQuery = (value) => ({
  type: EDIT_TITLE_QUERY,
  payload: value,
});

/**
 * Search employees and update the list by input queries
 */
const searchByQueries = (targetCompanyId, searchQueries) =>
  entitiesActions.searchByQueries(targetCompanyId, searchQueries);

/**
 * Create an action to make the employee selected
 * @param {String} targetEmployeeId The ID of the selected employee
 */
const selectEmployee = (targetEmployeeId) => ({
  type: SELECT_EMPLOYEE,
  payload: targetEmployeeId,
});

/**
 * Create an action to decide selection and close the dialog
 * @param {Object} targetEmployee The selected employee
 */
const decide = (targetEmployee) => ({
  type: DECIDE,
  payload: targetEmployee,
});

/**
 * Create an event to hide the dialog
 * @return {Object} A redux action
 */
const hide = () => ({
  type: HIDE,
});

export const actions = {
  show,
  switchSearchStrategy,
  editDepartmentCodeQuery,
  editDepartmentNameQuery,
  editEmployeeCodeQuery,
  editEmployeeNameQuery,
  editTitleQuery,
  searchByQueries,
  selectEmployee,
  decide,
  hide,
};

const initialState = {
  isVisible: false,
  searchStrategy: SearchStrategy.SHOW_EMPLOYEES_IN_SAME_DEPARTMENT,
  isSearchByQueriesExecuted: false,
  departmentCodeQuery: '',
  departmentNameQuery: '',
  employeeCodeQuery: '',
  employeeNameQuery: '',
  titleQuery: '',
  selectedEmployeeId: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SHOW:
      return {
        ...state,
        isVisible: true,
      };

    case SWITCH_SEARCH_STRATEGY:
      return {
        ...state,
        searchStrategy: action.payload,
        selectedEmployeeId: null,
        isSearchByQueriesExecuted: false,
        departmentCodeQuery: '',
        departmentNameQuery: '',
        employeeCodeQuery: '',
        employeeNameQuery: '',
        titleQuery: '',
      };

    case EDIT_DEPARTMENT_CODE_QUERY:
      return {
        ...state,
        departmentCodeQuery: action.payload,
      };
    case EDIT_DEPARTMENT_NAME_QUERY:
      return {
        ...state,
        departmentNameQuery: action.payload,
      };
    case EDIT_EMPLOYEE_CODE_QUERY:
      return {
        ...state,
        employeeCodeQuery: action.payload,
      };
    case EDIT_EMPLOYEE_NAME_QUERY:
      return {
        ...state,
        employeeNameQuery: action.payload,
      };
    case EDIT_TITLE_QUERY:
      return {
        ...state,
        titleQuery: action.payload,
      };

    case SELECT_EMPLOYEE:
      return {
        ...state,
        selectedEmployeeId: action.payload,
      };

    case DECIDE:
    case HIDE:
      return initialState;

    case entitiesConstants.SEARCH_EMPLOYEES_IN_SAME_DEPARTMENT_SUCCESS:
      return {
        ...state,
        selectedEmployeeId:
          state.selectedEmployeeId !== null &&
          action.payload.byId[state.selectedEmployeeId]
            ? state.selectedEmployeeId
            : null,
      };
    case entitiesConstants.SEARCH_BY_QUERIES_SUCCESS:
      return {
        ...state,
        isSearchByQueriesExecuted: true,
        selectedEmployeeId:
          state.selectedEmployeeId !== null &&
          action.payload.byId[state.selectedEmployeeId]
            ? state.selectedEmployeeId
            : null,
      };

    default:
      return state;
  }
};
