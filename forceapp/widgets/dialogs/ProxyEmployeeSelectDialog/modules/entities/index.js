import { createSelector } from 'reselect';

import {
  catchApiError,
  loadingEnd,
  loadingStart,
} from '../../../../../apps/commons/actions/app';
import Api from '../../../../../apps/commons/api';

import { constants as uiConstants } from '../ui';

import Employee from '../../models/Employee';

const SEARCH_BY_QUERIES_SUCCESS = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/ENTITIES/SEARCH_BY_QUERIES_SUCCESS'
);
const SEARCH_EMPLOYEES_IN_SAME_DEPARTMENT_SUCCESS = Symbol(
  'WIDGETS/DIALOGS/PROXY_EMPLOYEE_SELECT_DIALOG/ENTITIES/SEARCH_EMPLOYEES_IN_SAME_DEPARTMENT_SUCCESS'
);

export const constants = {
  SEARCH_EMPLOYEES_IN_SAME_DEPARTMENT_SUCCESS,
  SEARCH_BY_QUERIES_SUCCESS,
};

const convertEmployee = (record) =>
  new Employee({
    id: record.id,
    employeeCode: record.code || '',
    employeeName: record.name || '',
    employeePhotoUrl: record.user.photoUrl,
    departmentCode: (record.department && record.department.code) || '',
    departmentName: (record.department && record.department.name) || '',
    title: record.title || '',
  });

/**
 * Convert an array of records into normalized list of employees
 * @param {Object[]} records
 * @return
 */
const convertEmployees = (records) => ({
  allIds: records.map((item) => item.id),
  byId: Object.assign(
    {},
    ...records.map((item) => ({ [item.id]: convertEmployee(item) }))
  ),
});

export const converters = { convertEmployees };

const searchEmployeesInSameDepartmentSuccess = (result) => ({
  type: SEARCH_EMPLOYEES_IN_SAME_DEPARTMENT_SUCCESS,
  payload: convertEmployees(result.records),
});

const searchByQueriesSuccess = (result) => ({
  type: SEARCH_BY_QUERIES_SUCCESS,
  payload: convertEmployees(result.records),
});

/**
 * Search employees and the list under user's descendant departments
 * @return {Promise}
 */
const searchEmployeesInSameDepartment = (
  targetCompanyId,
  targetDepartmentId
) => (dispatch) => {
  const req = {
    path: '/employee/search',
    param: {
      companyId: targetCompanyId,
      departmentId: targetDepartmentId,
    },
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then((res) => dispatch(searchEmployeesInSameDepartmentSuccess(res)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

/**
 * Search employees and update the list by queries
 * @param {Object} searchQueries
 * @param {String} searchQueries.departmentCode
 * @param {String} searchQueries.departmentName
 * @param {String} searchQueries.employeeCode
 * @param {String} searchQueries.employeeName
 * @param {String} searchQueries.title
 * @return {Promise}
 */
const searchByQueries = (targetCompanyId, searchQueries) => (dispatch) => {
  const req = {
    path: '/employee/search',
    param: {
      companyId: targetCompanyId,
      departmentCode: searchQueries.departmentCodeQuery,
      departmentName: searchQueries.departmentNameQuery,
      code: searchQueries.employeeCodeQuery,
      name: searchQueries.employeeNameQuery,
      title: searchQueries.titleQuery,
    },
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then((result) => dispatch(searchByQueriesSuccess(result)))
    .catch((error) => dispatch(catchApiError(error, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const actions = {
  searchEmployeesInSameDepartment,
  searchByQueries,
};

/**
 * Select employees but the user's
 */
// $FlowFixMe
const employeesButUsersSelector = createSelector(
  (state) => state.common.userSetting.employeeId,
  (state) => state.widgets.ProxyEmployeeSelectDialog.entities.employees.allIds,
  (state) => state.widgets.ProxyEmployeeSelectDialog.entities.employees.byId,
  (userEmployeeId, allIds, byId) =>
    allIds.filter((id) => id !== userEmployeeId).map((id) => byId[id])
);

/**
 * Select full data of the selected employee
 */
// $FlowFixMe
const selectedEmployeeSelector = createSelector(
  (state) => state.widgets.ProxyEmployeeSelectDialog.ui.selectedEmployeeId,
  (state) => state.widgets.ProxyEmployeeSelectDialog.entities.employees.byId,
  (selectedEmployeeId, byId) => {
    return selectedEmployeeId !== null ? byId[selectedEmployeeId] : null;
  }
);

export const selectors = {
  employeesButUsersSelector,
  selectedEmployeeSelector,
};

const initialState = {
  employees: {
    allIds: [],
    byId: {},
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_EMPLOYEES_IN_SAME_DEPARTMENT_SUCCESS:
      return {
        ...state,
        employees: action.payload,
      };
    case SEARCH_BY_QUERIES_SUCCESS:
      return {
        ...state,
        employees: action.payload,
      };

    case uiConstants.SWITCH_SEARCH_STRATEGY:
    case uiConstants.DECIDE:
    case uiConstants.HIDE:
      return initialState;

    default:
      return state;
  }
};
