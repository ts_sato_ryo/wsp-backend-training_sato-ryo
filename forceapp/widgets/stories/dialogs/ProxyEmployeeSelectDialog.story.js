import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import ProxyEmployeeSelectDialog from '../../dialogs/ProxyEmployeeSelectDialog/components/ProxyEmployeeSelectDialog';

import Employee from '../../dialogs/ProxyEmployeeSelectDialog/models/Employee';
import SearchStrategy from '../../dialogs/ProxyEmployeeSelectDialog/models/SearchStrategy';

import dummyEmployeePhoto from '../../../apps/commons/images/photo_Blank.png';

const dummyEmployees = [
  new Employee({
    id: 'AnonymousEmployee0',
    departmentCode: 'Test',
    departmentName: 'テスト部署',
    employeeCode: '000001',
    employeeName: '田中 太郎',
    employeePhotoUrl: dummyEmployeePhoto,
    managerName: '鈴木 二子',
    title: '役職',
  }),
  new Employee({
    id: 'AnonymousEmployee1',
    departmentCode: 'Test',
    departmentName: 'テスト部署',
    employeeCode: '000002',
    employeeName: '鈴木 二子',
    employeePhotoUrl: dummyEmployeePhoto,
    managerName: '',
    title: '役職',
  }),
];

storiesOf('widgets/dialogs', module)
  .add(
    'ProxyEmployeeSelectDialog - show employees in same department',
    withInfo({
      propTables: [ProxyEmployeeSelectDialog],
      inline: false,
      source: true,
    })(() => (
      <ProxyEmployeeSelectDialog
        isVisible
        searchStrategy={SearchStrategy.SHOW_EMPLOYEES_IN_SAME_DEPARTMENT}
        isSearchByQueriesExecuted={false}
        departmentCodeQuery=""
        departmentNameQuery=""
        employeeCodeQuery=""
        employeeNameQuery=""
        titleQuery=""
        employees={dummyEmployees}
        selectedEmployeeId={null}
        selectedEmployee={null}
        onSwitchSearchStrategy={action('switchStrategy')}
        onEditDepartmentCodeQuery={action('editDepartmentCodeQuery')}
        onEditDepartmentNameQuery={action('editDepartmentNameQuery')}
        onEditEmployeeCodeQuery={action('editEmployeeCodeQuery')}
        onEditEmployeeNameQuery={action('editEmployeeNameQuery')}
        onEditTitleQuery={action('editTitleQuery')}
        onSearchByQueries={action('searchByQueries')}
        onSelectEmployee={action('selectEmployee')}
        onDecide={action('decide')}
        onCancel={action('cancel')}
      />
    ))
  )
  .add(
    'ProxyEmployeeSelectDialog - custom search beginning',
    withInfo({
      propTables: [ProxyEmployeeSelectDialog],
      inline: false,
      source: true,
    })(() => (
      <ProxyEmployeeSelectDialog
        isVisible
        searchStrategy={SearchStrategy.SEARCH_BY_QUERIES}
        isSearchByQueriesExecuted={false}
        departmentCodeQuery=""
        departmentNameQuery=""
        employeeCodeQuery=""
        employeeNameQuery=""
        titleQuery=""
        employees={[]}
        selectedEmployeeId={null}
        selectedEmployee={null}
        onSwitchSearchStrategy={action('switchStrategy')}
        onEditDepartmentCodeQuery={action('editDepartmentCodeQuery')}
        onEditDepartmentNameQuery={action('editDepartmentNameQuery')}
        onEditEmployeeCodeQuery={action('editEmployeeCodeQuery')}
        onEditEmployeeNameQuery={action('editEmployeeNameQuery')}
        onEditTitleQuery={action('editTitleQuery')}
        onSearchByQueries={action('searchByQueries')}
        onSelectEmployee={action('selectEmployee')}
        onDecide={action('decide')}
        onCancel={action('cancel')}
      />
    ))
  )
  .add(
    'ProxyEmployeeSelectDialog - with some search results and selection',
    withInfo({
      propTables: [ProxyEmployeeSelectDialog],
      inline: false,
      source: true,
    })(() => (
      <ProxyEmployeeSelectDialog
        isVisible
        searchStrategy={SearchStrategy.SEARCH_BY_QUERIES}
        isSearchByQueriesExecuted
        departmentCodeQuery=""
        departmentNameQuery=""
        employeeCodeQuery=""
        employeeNameQuery="人事 一徹"
        titleQuery=""
        employees={dummyEmployees}
        selectedEmployeeId="AnonymousEmployee0"
        selectedEmployee={dummyEmployees[0]}
        onSwitchSearchStrategy={action('switchStrategy')}
        onEditDepartmentCodeQuery={action('editDepartmentCodeQuery')}
        onEditDepartmentNameQuery={action('editDepartmentNameQuery')}
        onEditEmployeeCodeQuery={action('editEmployeeCodeQuery')}
        onEditEmployeeNameQuery={action('editEmployeeNameQuery')}
        onEditTitleQuery={action('editTitleQuery')}
        onSearchByQueries={action('searchByQueries')}
        onSelectEmployee={action('selectEmployee')}
        onDecide={action('decide')}
        onCancel={action('cancel')}
      />
    ))
  )
  .add(
    'ProxyEmployeeSelectDialog - with no search results',
    withInfo({
      propTables: [ProxyEmployeeSelectDialog],
      inline: false,
      source: true,
    })(() => (
      <ProxyEmployeeSelectDialog
        isVisible
        searchStrategy={SearchStrategy.SEARCH_BY_QUERIES}
        isSearchByQueriesExecuted
        departmentCodeQuery=""
        departmentNameQuery=""
        employeeCodeQuery=""
        employeeNameQuery="人事 一徹"
        titleQuery=""
        employees={[]}
        selectedEmployeeId={null}
        selectedEmployee={null}
        onSwitchSearchStrategy={action('switchStrategy')}
        onEditDepartmentCodeQuery={action('editDepartmentCodeQuery')}
        onEditDepartmentNameQuery={action('editDepartmentNameQuery')}
        onEditEmployeeCodeQuery={action('editEmployeeCodeQuery')}
        onEditEmployeeNameQuery={action('editEmployeeNameQuery')}
        onEditTitleQuery={action('editTitleQuery')}
        onSearchByQueries={action('searchByQueries')}
        onSelectEmployee={action('selectEmployee')}
        onDecide={action('decide')}
        onCancel={action('cancel')}
      />
    ))
  );
