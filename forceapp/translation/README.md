# 文字列翻訳変換ツール

## ツール説明

このツールでは画面表示文言やクラス内での固定文言をcsvファイルから各JSファイルやClsファイル等に変換します。

## 使い方

forceappフォルダからgulp実行する事によりツールが動作します。

```
gulp build
```

ツール自体の直接起動は下記となります。

```
gulp build-language
```

## 成果物

* JSファイル

	Reactにて使用される文字列定義JSファイルが出力されます。

* 出力先

```
forceapp
  └ apps
     └ commons
        └ languages
           └ Msg_*.js .............. （"*"には言語コードが格納されます）
```

* Clsファイル

	Apex側にて使用される文字列定義Clsファイルが出力されます

* 出力先

```
forceapp
  └src
    └classes
      └ComMsg_*.js .............. （"*"には言語コードが格納されます）
```

## 使い方

* React

```
import msg from '../../commons/languages';

render() {
  return (
    {msg().Com_Sample_Test1}
  );
}
```

* Apex

```
// 単純に使用する場合
String msg = ComMessage.msg().Com_Sample_Test1;
// パラメータによる置き換えしたい場合
String msg = ComMessage.msg().Com_Sample_Test1(new List<String>{ 'test' });
```

## フォルダ構成

```
forceapp
└translsation
  ├*.csv ................................. 固定文言ファイル（"*"には言語コードが格納されます）
  ├README.md ............................. このファイル
  └messageRes
    ├templates .......................... テンプレートファイル
    │ └apex .............................. Apex用
    ├index.js
    ├TranslationMapping.js ............... CSVファイルを解析しマッピングするファイル
    ├JSTranslation.js .................... JSファイルを作成するファイル
    ├ApexTranslation.js .................. Clsファイルを作成するファイル
    └ObjectTranslation.js ................ オブジェクト翻訳ファイルを作成するファイル
```

## 言語追加方法

1. \*.csvファイル（"\*"には言語コードが格納されます）を追加する
1. 「forceapp/src/classes/ComMessage.cls」に追加する言語の条件分岐を追加する。
1. 「forceapp/apps/commons/languages/index.js」に追加する言語の条件分岐を追加する。

## 参考文献

csvファイルの記載ルール：[日英対応表作成ルール](https://labo.teamspirit.co.jp/projects/document/wiki/%E6%97%A5%E8%8B%B1%E5%AF%BE%E5%BF%9C%E8%A1%A8%E4%BD%9C%E6%88%90%E3%83%AB%E3%83%BC%E3%83%AB)

Salesforce言語コード一覧：[Salesforce がサポートする言語は?（要ログイン）](https://help.salesforce.com/HTViewHelpDoc?id=faq_getstart_what_languages_does.htm&language=ja)