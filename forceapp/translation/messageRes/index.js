const through = require('through2');
const gutil = require('gulp-util');
const fs = require('fs');
const csvParser = require('csv-parser');
const mapping = require('./TranslationMapping.js');
const jsTrans = require('./JSTranslation.js');
const apexTrans = require('./ApexTranslation.js');
const customLabelTrans = require('./CustomLabelTranslation.js');

// メッセージリソース生成
class MessageRes {
  execute(options) {
    const isMessageEmpty = (data) =>
      !Object.keys(data).every((key) => key === 'error' || data[key]);

    const langTypes = [];
    langTypes.push('ja');
    const transform = function(file, enc, callback) {
      // +1 because file.base ends with '/'
      // fileName has not to have '/'
      const fileName = file.path.substring(file.base.length + 1);
      gutil.log(`読込:${fileName}`);
      const langType = fileName.split(/(?=\.[^.]+$)/)[0];
      langTypes.push(langType);
      const stream = fs
        .createReadStream(file.path)
        .pipe(
          csvParser({
            mapHeaders: ({ header }) => header || 'error',
          })
        )
        .on('error', callback)
        .on('data', (data) => {
          if (data.error || isMessageEmpty(data)) {
            stream.destroy(
              new Error(`
          The CSV Row of Message ID ${data.Id} is wrong. Verify that the number of columns is correct and messages including commas are properly escaped with double quotes.`)
            );
            return;
          }
          mapping.addLine(data, langType);
        })
        .on('end', () => {
          callback();
        });
    };

    const flush = function(callback) {
      jsTrans.output(langTypes, options.js, options.template, mapping);
      apexTrans.output(langTypes, options.src, options.template, mapping);
      customLabelTrans.output(
        langTypes,
        options.src,
        options.template,
        mapping
      );
      callback();
    };

    return through.obj(transform, flush);
  }
}

module.exports = function(options) {
  const msg = new MessageRes();
  return msg.execute(options);
};
