// JS側文字列定義ファイル生成モジュール
const fs = require('fs');
const gutil = require('gulp-util');

class JSTranslation {
  output(langTypes, jsPath, templatePath, mapping) {
    // 成形
    const jsMap = mapping.langMap.J;
    for (let index = 0; index < langTypes.length; index++) {
      const langType = langTypes[index];
      const langName = langType;
      const fileName = `Msg_${langName}`;
      const outputMap = new Array();
      if (jsMap) {
        const langMap = jsMap[langType];
        for (const id in langMap) {
          const message = langMap[id]
            .replace(/\n/g, '\\n')
            .replace(/'/g, "\\'");
          outputMap.push(`\
  /**
   * @description [${langType}] ${message}
   */
  ${id}: '${message}'\
`);
        }
      }
      // 出力先へファイル出力
      const outputFile = `${jsPath}/${fileName}.js`;
      const data = `\
/* @flow */

const ${langName} = {
${outputMap.join(',\n\n')},
  ['']: '',
};

export default ${langName};
`;
      fs.writeFileSync(outputFile, data);
      gutil.log(`出力:${outputFile}`);
    }
  }
}

module.exports = new JSTranslation();
