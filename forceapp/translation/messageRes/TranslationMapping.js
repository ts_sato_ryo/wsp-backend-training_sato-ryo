// メッセージリソース生成モジュール
// 日本語、ID と英語メッセージのマッピングテーブル生成
class TranslationMapping {
  constructor() {
    this.langMap = {
      /**
       * Translation map for Frontend
       */
      J: {
        ja: {},
        en_US: {},
      },
      /**
       * Translation map fo Backend
       */
      C: {
        ja: {},
        en_US: {},
      },
    }; // [Location] => [Language] => Words
  }

  /**
   * Add translated word to dictionary
   * @param  {object}       translation object representing translated word
   * @param  {string}       language representing language. e.g. ja|en_US
   * @param  {string}       location taking J or C.
   */
  addTranslatedWord(translation, language, location) {
    if (
      translation.Id === undefined ||
      translation.Id === null ||
      translation.Id === ''
    ) {
      return;
    }

    this.langMap[location].ja[translation.Id] = translation.originalWord;

    this.langMap[location][language][translation.Id] =
      translation.translatedWord || translation.originalWord;
  }

  /**
   * Add a row of CSV to translation dictionary
   * @param  {object}       line object representing a row of CSV.
   * @param  {string}       language representing language. e.g. ja|en_US
   */
  addLine(line, language) {
    const translation = {
      Id: line.Id,
      originalWord: line['原文'] || '',
      translatedWord: line['翻訳結果'] || '',
      location: line['使用箇所'] || '',
    };

    switch (translation.location) {
      case 'JC':
        this.addTranslatedWord(translation, language, 'J');
        this.addTranslatedWord(translation, language, 'C');
        break;
      case 'J':
        this.addTranslatedWord(translation, language, 'J');
        break;
      case 'C':
        this.addTranslatedWord(translation, language, 'C');
        break;
      default:
        // Skip unknown location
        break;
    }
  }
}

module.exports = new TranslationMapping();
