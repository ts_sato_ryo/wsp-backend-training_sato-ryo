// カスタムラベル定義生成モジュール
const fs = require('fs');
const gutil = require('gulp-util');

class CustomLabelTranslation {
  output(langTypes, srcPath, templatePath, mapping) {
    // カスタムラベルはC、J両方で同じラベルを定義することを前提としているため、Cを固定で参照する
    const messages = mapping.langMap.C;
    const customLabels = new Array();
    for (const id in messages.ja) {
      // カスタムラベルのみ出力対象とする
      if (!id.startsWith('$')) {
        continue;
      }
      customLabels.push(
        {
          id:id,
          Ja_Default:messages.ja[id],
          US_Default:messages.en_US[id]
        }
      );
    }

    // テンプレートを読み込み
    const templateContent = fs.readFileSync(
      `${templatePath}/meta/ComCustomLabel.md-meta.xml`,
      'utf-8'
    );
    customLabels.forEach(label => {
      const labelId = label.id.replace('$', '');
      // 中身置き換え
      const outputContent = templateContent
        .replace('[label]', label.US_Default)
        .replace('[Ja_Default]', label.Ja_Default)
        .replace('[US_Default]', label.US_Default);

      // 出力先へファイル出力
      const outputFile = `${srcPath}/customMetadata/ComCustomLabel.${labelId}.md-meta.xml`;
      fs.writeFileSync(outputFile, outputContent);
      gutil.log(`出力:${outputFile}`);
    })
  }
}

module.exports = new CustomLabelTranslation();