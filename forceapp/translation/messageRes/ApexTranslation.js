// Apex文字列定義クラス生成モジュール
const fs = require('fs');
const gutil = require('gulp-util');

class ApexTranslation {
  output(langTypes, srcPath, templatePath, mapping) {
    // 言語毎の固定文言設定ファイルを成形
    const jsMap = mapping.langMap.C;
    const idMap = new Array();
    for (var index = 0; index < langTypes.length; index++) {
      const langType = langTypes[index];
      const langName = langType;
      const className = `ComMsg_${langName}`;
      const outputMap = new Array();
      if (jsMap) {
        outputMap.push(`public ${className}() {this.language = AppLanguage.valueOf('${langName}');}`);
        const langMap = jsMap[langType];
        for (const id in langMap) {
          // カスタムラベルは出力対象外
          if (!id.startsWith('$')) {
            outputMap.push(
              `protected override String msg_${id}() { return '${langMap[id]
                .replace(/\n/g, '\\n')
                .replace(/'/g, "\\'")}'; }`
            );
          }
          if (idMap.indexOf(id) < 0) {
            idMap.push(id);
          }
        }
      }
      // const className = `ComMsg_${langName}`;
      // 出力定義
      const head = `public with sharing class ${className} extends ComMsgBase {\n`;
      const contents = `\t${outputMap.join('\n\t')}\n`;
      const foot = '}';
      // 出力先へファイル出力
      var outputFile = `${srcPath}/classes/${className}.cls`;
      fs.writeFileSync(outputFile, head + contents + foot);
      gutil.log(`出力:${outputFile}`);
      // meta.xmlの作成
      var xmlContent = fs.readFileSync(
        `${srcPath}/classes/ComMessage.cls-meta.xml`,
        'utf-8'
      ); // ComMesssage.cls-meta.xml を読みこむ
      var outputXmlFile = `${srcPath}/classes/${className}.cls-meta.xml`;
      fs.writeFileSync(outputXmlFile, xmlContent);
    }
    // ComMsgBase.clsファイルの生成
    // 各要素をgetterとvalue属性に変更
    const outputList = new Array();
    var messageId;
    for (var index = 0; index < idMap.length; index++) {
      if (idMap[index].startsWith('$')) {
        // カスタムラベルの場合
        // メッセージID先頭の'$'を除外する
        messageId = idMap[index].substr(1);
        outputList.push(
          `protected virtual String msg_${messageId}() { return getCustomLabel('${messageId}', this.language); }`
        );
        // convertしない(Not supported Place Holder)
        outputList.push(
          `public String ${messageId} { get{ return msg_${messageId}(); } }`
        );
      } else {
        // 非カスタムラベルの場合
        messageId = idMap[index];
        outputList.push(
          `protected virtual String msg_${messageId}() { return ''; }`
        );
        outputList.push(
          `public String ${messageId} { get{ return convert(msg_${
            messageId
          }()); } }`
        );
        outputList.push(
          `public String ${messageId}(List<String> cl) { return convert(msg_${
            messageId
          }(), cl); }`
        );
      }
    }
    // テンプレートを読み込み
    let templateContent = fs.readFileSync(
      `${templatePath}/apex/ComMsgBase.cls`,
      'utf-8'
    ); // ComMsgBase.cls を読みこむ
    // 中身置き換え
    const outputContent = `\t${outputList.join('\n\t')}`;
    templateContent = templateContent.replace('[contents]', outputContent);
    // 出力先へファイル出力
    var outputFile = `${srcPath}/classes/ComMsgBase.cls`;
    fs.writeFileSync(outputFile, templateContent);
    gutil.log(`出力:${outputFile}`);
    // meta.xmlの作成
    var xmlContent = fs.readFileSync(
      `${srcPath}/classes/ComMessage.cls-meta.xml`,
      'utf-8'
    ); // ComMesssage.cls-meta.xml を読みこむ
    var outputXmlFile = `${srcPath}/classes/ComMsgBase.cls-meta.xml`;
    fs.writeFileSync(outputXmlFile, xmlContent);
  }
}

module.exports = new ApexTranslation();
