/**
 * 固定文言テンプレートのベースクラス
 * ※自動生成クラスであるため、直接編集はしないで下さい
 * FIXME: 使用する文字数が多いので短縮化したい
 */
public abstract with sharing class ComMsgBase {

[contents]

	/** @description 翻訳対象言語 */
	protected AppLanguage language;

	/** @description 翻訳対象言語 */
	protected ComCustomLabelService customLabelService = new ComCustomLabelService();

	/**
	 * @description カスタムラベルを取得する。
	 * @param messageId メッセージID
	 * @param lang 取得対象言語
	 * @return カスタムラベル。ただし単体テストの場合はデフォルトラベルを返却する
	 */
	protected String getCustomLabel(String messageId, AppLanguage lang) {
		if (Test.isRunningTest()) {
			// テストの場合は常にデフォルトラベルを返却する
			return customLabelService.getDefaultLabel(messageId, lang);
		}
		return customLabelService.getCustomLabel(messageId, lang);
	}

	/**
	 * 変換実行（List<String>）
	 * @param value 変換前文字列
	 * @param paramList 変換リスト
	 * @return 変換後文字列
	 */
	@testVisible
	protected String convert(String value, List<String> paramList) {
		Integer index = 1;
		String convertValue = value;
		if (paramList != null) {
			for (; index <= paramList.size() ; index++) {
				String convertCode = generateConvertCode(index);
				// テスト時に変換対象コードが存在しない場合はエラーとする
				if (value.indexOf(convertCode) == -1) {
					ComUtility.throwTestException(new ComTestException('変換対象コード(' + convertCode + ')が含まれていません: ' + value ));
				}
				String replaceStr = paramList[index - 1];
				if (replaceStr == null) {
					replaceStr = 'null';
				}
				convertValue = convertValue.replace(convertCode, replaceStr);
			}
		}
		// テスト時に変換対象が余っている場合はエラーとする
		String convertCode = generateConvertCode(index);
		if (convertValue.indexOf(convertCode) != -1) {
			ComUtility.throwTestException(new ComTestException('変換対象コード(' + convertCode + ')が使用されていません: ' + value ));
		}

		// メッセージに埋め込まれているカスタムラベルを変換する
		final String customPrefix = '{$';
		final String customSuffix = '}';
		final String placeHolderPrefix = '\\{\\$';
		final String placeHolderSuffix = '\\}';
		if (convertValue.indexOf(customPrefix) >= 0) {
			Integer baseIndex = 0;
			for (Integer i = 0; i < convertValue.countMatches(customPrefix); i++) {
				Integer startIndex = convertValue.indexOf(customPrefix, baseIndex);
				Integer endIndex = convertValue.indexOf(customSuffix, startIndex);
				if (endIndex < 0) {
					throw new App.IllegalStateException('Invalid message format: ' + value);
				}
				String labelId = convertValue.subString(startIndex + customPrefix.length(), endIndex);
				String label = customLabelService.getCustomLabel(labelId, this.language);
				if (label == null) {
					throw new App.RecordNotFoundException('Custom label not found: ' + value);
				}
				String placeHolder = placeHolderPrefix + labelId + placeHolderSuffix;
				convertValue = convertValue.replaceFirst(placeHolder, label);
				baseIndex = endIndex + 1;
			}
		}

		return convertValue;
	}

	/**
	 * 変換リストを格納（なし）主にチェック用
	 * @param value 変換前文字列
	 * @return 変換後文字列
	 */
	@testVisible
	protected String convert(String value) {
		return convert(value, new List<String>{});
	}

	/**
	 * 変換対象コードを生成
	 * @param index 変換対象インデックス
	 * @return 変換対象コード
	 */
	private String generateConvertCode(Integer index) {
		return '[%' + index + ']';
	}

}
