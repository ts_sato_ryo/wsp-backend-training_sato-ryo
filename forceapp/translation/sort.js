/**
 * 翻訳ファイルソートスクリプト
 * npm run translation:sort
 */
const fs = require('fs');

const sort = (err, buf) => {
  let sorted = [];

  const text = buf.toString();
  const ary = text.split('\n');

  // 見出しを一旦除去
  sorted.push(ary.shift());
  sorted.push(...ary.sort());
  sorted = sorted.filter((line) => { return line.trim() !== ''; });

  // 末尾改行
  sorted.push('');

  fs.writeFile('./translation/en_US.csv', sorted.join('\n'), (error) => {
    if (error) {
      throw error;
    }
    console.log('Sorting en_US.csv succeeded!');
  });
};

fs.readFile('./translation/en_US.csv', sort);
