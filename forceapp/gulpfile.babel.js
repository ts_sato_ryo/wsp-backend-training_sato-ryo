/* eslint-disable import/no-extraneous-dependencies */
import { exec } from 'child_process';

import errorhandler from 'errorhandler';
import log from 'fancy-log';
import gulp from 'gulp';
import zip from 'gulp-zip';
import notify from 'gulp-notify';
import rename from 'gulp-rename';
import replace from 'gulp-replace';
import imagemin from 'gulp-imagemin';
import sass from 'gulp-sass';

import pngquant from 'imagemin-pngquant';
import del from 'del';
import runSequence from 'gulp4-run-sequence';
import express from 'express';
import extract from 'gulp-extract';
import fs from 'fs';
import http from 'http';
import jsforceAjaxProxy from 'jsforce-ajax-proxy';
import webpackStream from 'webpack-stream';
import webpackConfigProduction from './webpack.config.production.babel';

import messageRes from './translation/messageRes';

const SFDX_BASE_PATH = `sfdx/force-app/main/default`;
const SFDX_APP_PATH = `force-app/main/default`;

const TEN_MEGA_BYTE = 1024 * 1024 * 10;

let env = process.env.SF_ENV || 'vfp';

gulp.task(
  'build-api',
  gulp.series(() => {
    const hash = {};
    return gulp
      .src(`./${SFDX_BASE_PATH}/classes/RemoteApiRouteMap.cls`)
      .pipe(extract(hash))
      .on('end', () => {
        // extract only contents between single quotes ''
        const str = hash.RemoteApiRouteMap;
        const regex = /'(.*?)'/g;
        const result = [];
        let current = regex.exec(str);

        while (current) {
          result.push(current.pop());
          current = regex.exec(str);
        }

        const content = result.length > 0 ? result : [str];
        fs.writeFileSync(
          './apps/test-api/api/api-list.json',
          JSON.stringify(content, null, 4)
        );
      });
  })
);

gulp.task(
  'clean',
  gulp.series((cb) => {
    return del(
      [
        'build',
        `${SFDX_BASE_PATH}/pages/**/*`,
        `${SFDX_BASE_PATH}/staticresources/**/js`,
        `${SFDX_BASE_PATH}/staticresources/**/*.resource`,
      ],
      cb
    );
  })
);

gulp.task(
  'build-language',
  gulp.series(() => {
    return gulp.src('./translation/*.csv').pipe(
      messageRes({
        js: './apps/commons/languages',
        src: `./${SFDX_BASE_PATH}`,
        template: './translation/messageRes/templates',
      })
    );
  })
);

// Example: gulp create-config --name record
// Example: gulp create-config --name expenseType
gulp.task(
  'create-config',
  gulp.series((done) => {
    const ROOT = './apps/admin-pc';
    const configName = process.argv[4];

    if (!configName) {
      // eslint-disable-next-line no-console
      console.log('\ngulp create-config --name <newConfigName>\n');
      done();
      return null;
    }

    const camelCasedName =
      configName.charAt(0).toUpperCase() + configName.slice(1);
    const upperCasedName = configName.toUpperCase();
    const lowerCasedName = configName.toLowerCase();

    return gulp
      .src([
        `${ROOT}/**/company.js`,
        `${ROOT}/*/CompanyContainer.js`,
        `${ROOT}/*/Company/index.js`,
        `${ROOT}/*/searchCompany.js`,
      ])
      .pipe(replace('Company', camelCasedName))
      .pipe(replace('COMPANY', upperCasedName))
      .pipe(replace('company', lowerCasedName))
      .pipe(
        rename((path) => {
          path.basename = path.basename
            .replace('company', lowerCasedName)
            .replace('Company', camelCasedName);
          path.dirname = path.dirname.replace('Company', camelCasedName);
        })
      )
      .pipe(gulp.dest(ROOT))
      .pipe(
        notify({
          onLast: true,
          message: `Success: New config create!`,
        })
      );
  })
);

gulp.task(
  'optimize-images',
  gulp.series(() => {
    if (process.env.NODE_ENV === 'production') {
      return gulp
        .src('apps/**/images-pre-optimized/**/*')
        .pipe(
          imagemin({
            use: [
              pngquant({
                quality: 65 - 80,
                speed: 1,
              }),
            ],
          })
        )
        .pipe(
          rename((path) => {
            path.dirname = path.dirname.replace(
              'images-pre-optimized',
              'images'
            );
          })
        )
        .pipe(gulp.dest('apps'));
    } else {
      // production以外では最適化しない
      return gulp
        .src('apps/**/images-pre-optimized/**/*')
        .pipe(
          rename((path) => {
            path.dirname = path.dirname.replace(
              'images-pre-optimized',
              'images'
            );
          })
        )
        .pipe(gulp.dest('apps'));
    }
  })
);

gulp.task(
  'build-js',
  gulp.series(() => {
    log('SF_ENV=', env);
    return webpackStream({
      ...webpackConfigProduction,
      mode: 'production',
    }).pipe(gulp.dest(`${SFDX_BASE_PATH}/staticresources`));
  })
);

// classic.scss を静的リソースとして書き出す
gulp.task(
  'zip-classic-css',
  gulp.series(() => {
    return gulp
      .src('apps/commons/styles/classic.scss')
      .pipe(sass())
      .pipe(gulp.dest('build/resource/classic/css/'))
      .pipe(zip('classic.resource'))
      .pipe(gulp.dest(`${SFDX_BASE_PATH}/staticresources/`));
  })
);

gulp.task(
  'copy-html',
  gulp.series(() => {
    return gulp.src('apps/**/*.html').pipe(gulp.dest('build/'));
  })
);

/** SLDS を build/ 以下にコピーする */
gulp.task(
  'copy-slds',
  gulp.series(() => {
    return gulp
      .src(
        [
          'node_modules/@salesforce-ux/design-system/assets/fonts/**/*',
          'node_modules/@salesforce-ux/design-system/assets/icons/*-sprite/**/*',
          'node_modules/@salesforce-ux/design-system/assets/images/**/*',
          'node_modules/@salesforce-ux/design-system/assets/styles/*.min.css',
        ],
        {
          base: 'node_modules/@salesforce-ux/design-system/assets/',
        }
      )
      .pipe(gulp.dest('build/resource/slds/assets'));
  })
);

/** SLDS stylesheets to staticresources */
gulp.task(
  'zip-slds',
  gulp.series(() => {
    return gulp
      .src(`build/resource/slds/assets/**/*`, {
        base: 'build/resource/slds',
      })
      .pipe(zip(`slds.resource`))
      .pipe(gulp.dest(`${SFDX_BASE_PATH}/staticresources/`));
  })
);

/**
 * ローカル環境向けビルド
 */
gulp.task(
  'build-local',
  gulp.series('clean', (cb) => {
    // FIXME: webpack-dev-server と共存するための暫定措置。
    // NODE_ENV=development のままだと react-hmre が有効になりエラーとなる。
    process.env.NODE_ENV = 'local';

    env = 'local';
    return runSequence(
      ['copy-slds', 'copy-html', 'build-language', 'optimize-images'],
      cb
    );
  })
);

/**
 * vfp 環境向けビルド
 *
 * NODE_ENV=production でのビルドがデフォルトで、
 * デバッグ時など production build をしたくない場合は NODE_ENV を明示的に指定する
 *
 * Different options available:
 * gulp build --nojs
 * => build without js resources. Good for BE.
 *
 *
 */
gulp.task(
  'build',
  gulp.series('clean', (cb) => {
    const isJSBuildDisabled = process.argv[3] === '--nojs';

    if (!process.env.NODE_ENV) {
      process.env.NODE_ENV = 'production';
    }
    log('NODE_ENV=', process.env.NODE_ENV);

    if (isJSBuildDisabled) {
      // Ultra Fast. ~3 seconds
      // no js build. Used for development
      return runSequence(
        'copy-slds',
        'zip-slds',
        ['build-language', 'optimize-images', 'zip-classic-css'],
        cb
      );
    } else {
      // Slow. ~ 4 minutes
      // default build
      return runSequence(
        'copy-slds',
        'zip-slds',
        ['build-language', 'optimize-images', 'zip-classic-css'],
        'build-js',
        cb
      );
    }
  })
);

gulp.task(
  'deploy:front',
  gulp.series((cb) => {
    const frontApps = [
      'admin_pc',
      'approvals_pc',
      'expenses_pc',
      'expenses_pc_print',
      'classic',
      'oauth_result_view',
      'planner_pc',
      'requests_pc',
      'finance_approval_pc',
      'slds',
      'team_pc',
      'timesheet_pc',
      'timesheet_pc_leave',
      'timesheet_pc_summary',
      'tracking_pc',
      'mobile_app',
    ];

    const defaultMsg =
      'Deploying all frontend, you can specify: --front mobile_app';
    const component = process.argv[4];
    const isComponentSpecified = frontApps.includes(component);

    if (isComponentSpecified) {
      log('Full deployment is preferred in case of changes affecting backend');
    }

    const { target, msg } = isComponentSpecified
      ? {
          target: `${SFDX_APP_PATH}/staticresources/${component}.resource-meta.xml`,
          msg: `Selected Component = ${component}`,
        }
      : { target: `${SFDX_APP_PATH}/staticresources`, msg: defaultMsg };

    log.info(msg);

    return exec(
      `sfdx force:source:deploy -p ${target}`,
      { maxBuffer: TEN_MEGA_BYTE, cwd: 'sfdx' },
      (err, stdout, stderr) => {
        notify('Deploy Completed');
        log.info(stdout);
        log.error(stderr);
        cb(err);
      }
    );
  })
);

gulp.task(
  'deploy-only',
  gulp.series((cb) => {
    return exec(
      `sfdx force:source:deploy -x manifest/package.xml`,
      { maxBuffer: TEN_MEGA_BYTE, cwd: 'sfdx' },
      (err, stdout, stderr) => {
        notify('Deploy Completed');
        log.info(stdout);
        log.error(stderr);
        cb(err);
      }
    );
  })
);

/**
 * Salesforce 組織にデプロイする
 *
 * SF_USERNAME, SF_PASSWORD はコマンド実行時に必須
 * また NAMESPACE を指定すると、JavaScript Remoting コントローラーに名前空間を付与する
 * （パッケージ作成組織ではこれを指定する）
 */
gulp.task(
  'deploy',
  gulp.series((cb) => {
    const isDeployFront = process.argv[3] === '--front';
    if (isDeployFront) {
      return runSequence('deploy:front', cb);
    }
    return runSequence('deploy-only', cb);
  })
);

gulp.task(
  'jsforce-ajax-proxy',
  gulp.series(() => {
    const proxy = express();
    proxy.set('port', 3123);
    proxy.use(errorhandler());
    proxy.all(
      '/proxy/?*',
      jsforceAjaxProxy({
        enableCORS: true,
      })
    );
    proxy.get('/', (req, res) => {
      return res.send('JSforce AJAX Proxy');
    });
    return http.createServer(proxy).listen(proxy.get('port'), () => {
      return log.info(
        `jsforce-ajax-proxy is listening on ${proxy.get('port')}`
      );
    });
  })
);

gulp.task(
  'deploy:apex',
  gulp.series((cb) => {
    return exec(
      `sfdx force:source:deploy -p ${SFDX_APP_PATH}/classes`,
      { maxBuffer: TEN_MEGA_BYTE, cwd: 'sfdx' },
      (err, stdout, stderr) => {
        notify('Deploy Completed');
        log.info(stdout);
        log.error(stderr);
        cb(err);
      }
    );
  })
);

gulp.task(
  'watch:translation',
  gulp.series('build-language', (_done) => {
    gulp.watch('./translation/*.csv', gulp.series('build-language'));
  })
);

gulp.task(
  'deploy-diff',
  gulp.series((cb) => {
    exec(
      `(cd deploy-script && sh deploy-diff.sh ${process.argv.join(' ')})`
    ).stdout.pipe(process.stdout);
    cb();
  })
);

gulp.task('default', gulp.series('build'));

/**
 * Run PMD static checker against staged APEX files (cls or trigger files)
 * The rules from {@link pmd-apex-ruleset.xml} is used for checking
 */
gulp.task(
  'pmd-apex-staged',
  gulp.series(() => {
    return exec(
      `git diff --name-only --staged '*.cls' '*.trigger' |sed 's/forceapp/./' | tee apex-lint-target.csv`,
      { maxBuffer: TEN_MEGA_BYTE },
      (gitErr, gitStdout, _gitStderr) => {
        if (gitStdout !== null && gitStdout.length !== 0) {
          exec(
            `run.sh pmd -filelist apex-lint-target.csv -R ./pmd-apex-ruleset.xml`,
            { maxBuffer: TEN_MEGA_BYTE },
            (err, stdout, _stdErr) => {
              notify('APEX PMD Completed');
              log.error(stdout);
              // log.error(err.code);
            }
          );
        }
      }
    );
  })
);
