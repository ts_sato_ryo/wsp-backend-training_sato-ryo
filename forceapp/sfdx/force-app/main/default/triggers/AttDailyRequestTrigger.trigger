/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠日次申請のトリガー
 */
trigger AttDailyRequestTrigger on AttDailyRequest__c (before update, after update) {

	if (Trigger.isUpdate) {
		// 承認済みや却下となる承認申請を取得
		AttDailyRequestTriggerHandler handler = new AttDailyRequestTriggerHandler(Trigger.oldMap, Trigger.newMap);
		if (handler.approvedRequestIds.isEmpty() && handler.disabledRequestIds.isEmpty() && handler.removeRequestIds.isEmpty() && handler.reapplyingToDisabledRequestIds.isEmpty()) {
			return;
		}
		if (Trigger.isBefore) {
			// 更新前トリガーの場合、申請データに承認プロセス情報を適用
			handler.applyProcessInfo();

			// 承認内容変更申請が承認されたら、元の申請を無効化する
			// 休暇申請で元申請の消費データを削除してから、新たに消費データを作成する必要があるため、isBeforeで更新する
			List<AttDailyRequest__c> originalRequestList = getOriginalRequestList(handler.approvedRequestIds, Trigger.newMap);
			updateOriginalRequest(originalRequestList, handler);
		}
		else {
			// 承認済みの申請のみ明細への反映処理を行う
			if (!handler.approvedRequestIds.isEmpty()) {
				List<AttRecord__c> attRecordList = getAttRecordList(handler.approvedRequestIds);
				handler.applyAttRecordInfo(attRecordList);
				updateSObject(handler.updatedAttRecordList);
				updateSObject(handler.updatedAttSummaryList);

				// 年次有休・日数管理休暇の休暇申請を取得する
				List<AttDailyRequest__c> managedLeaveRequestList = getManagedLeaveRequest(handler.approvedRequestIds);
				if (!managedLeaveRequestList.isEmpty()) {
					// 休暇消費を作成する
					List<AttManagedLeaveSummary__c> leaveSummaryList = getLeaveSummaryList(managedLeaveRequestList);
					List<AttManagedLeaveTake__c> takenList = handler.createManagedLeaveTakenList(managedLeaveRequestList, leaveSummaryList);
					insertSObject(takenList);

					// 休暇サマリ、休暇付与サマリ、休暇付与を再計算して保存する
					recalcManagedLeaveInfoSave(handler, managedLeaveRequestList);

					// 元申請の休暇消費の申請IDに承認内容変更申請IDを設定する
					List<AttManagedLeaveTake__c> leaveTakeList = updateLeaveRequestId(managedLeaveRequestList);
					updateSObject(leaveTakeList);
				}

				List<AttDailyRequest__c> childRequestList = getChildRequestList(handler.approvedRequestIds);
				for (AttDailyRequest__c childRequest : childRequestList) {
					AttDailyRequest__c parentRequest = Trigger.newMap.get(childRequest.ParentRequestId__c);
					childRequest.Status__c = parentRequest.Status__c;
				}
				if (!childRequestList.isEmpty()) {
					updateSObject(childRequestList); // 子申請の承認処理を実行
				}
			}

			// 却下、取消済み申請の勤怠明細への反映処理
			if (!handler.disabledRequestIds.isEmpty()) {
				List<AttRecord__c> attRecordList = getApprovedAttRecordList(handler.disabledRequestIds);
				handler.unapplyAttRecordInfo(attRecordList);
				updateSObject(handler.updatedAttRecordList);
				updateSObject(handler.updatedAttSummaryList);

				List<AttDailyRequest__c> childRequestList = getChildRequestList(handler.disabledRequestIds);
				for (AttDailyRequest__c childRequest : childRequestList) {
					AttDailyRequest__c parentRequest = Trigger.newMap.get(childRequest.ParentRequestId__c);
					childRequest.Status__c = parentRequest.Status__c;
					childRequest.CancelType__c = parentRequest.CancelType__c;
				}
				if (!childRequestList.isEmpty()) {
					updateSObject(childRequestList); // 子申請の却下、承認取消処理を実行
				}
			}

			// 削除済み(取下)申請の勤怠明細への反映処理
			if (!handler.removeRequestIds.isEmpty()) {
				// 子申請と紐づく勤怠明細も取得する
				Set<Id> removeRequestIdsAll = new Set<Id>(handler.removeRequestIds);
				for (AttDailyRequest__c childRequest : getChildRequestList(handler.removeRequestIds)) {
					removeRequestIdsAll.add(childRequest.id);
				}
				List<AttRecord__c> attRecordList = getAttRecordList(removeRequestIdsAll);
				handler.removeAttRecordInfo(attRecordList);
				updateSObject(handler.updatedAttRecordList);
			}

			// 承認内容変更申請の元申請が無効になった時の処理
			if (!handler.reapplyingToDisabledRequestIds.isEmpty()) {
				// 申請に紐づく、勤務確定済みではない休暇消費を取得する
				// 勤務確定済みの休暇消費は変更申請時には作成されないため、ここでは勤務確定済みの休暇消費は削除しない
				List<AttManagedLeaveTake__c> leaveTakeList = getTakenListByRequestId(handler.reapplyingToDisabledRequestIds, false);
				// 休暇消費を削除する（変更申請の休暇消費を作成時に日数計算をおこなうため、ここでは再計算しない）
				handler.deleteManagedLeaveTakenList(leaveTakeList);
			}
		}
	}

	/**
	 * 休暇サマリ、休暇付与サマリ、休暇付与を再計算して保存する
	 * @param managedLeaveRequestList 年次有休・日数管理休暇の休暇申請
	 */
	private static void recalcManagedLeaveInfoSave(AttDailyRequestTriggerHandler handler, List<AttDailyRequest__c> managedLeaveRequestList) {

		// 休暇サマリ
		List<AttManagedLeaveSummary__c> leaveSummaryList = getLeaveSummaryList(managedLeaveRequestList);
		// 休暇消費
		List<AttManagedLeaveTake__c> takenListAll = getTakenListBySummary(leaveSummaryList);
		// 休暇付与
		List<AttManagedLeaveGrant__c> grantListAll = getGrantedListBySummary(leaveSummaryList);
		System.debug(LoggingLevel.DEBUG, '---takenListAll is ' + takenListAll);
		System.debug(LoggingLevel.DEBUG, '---grantListAll is ' + grantListAll);
		List<AttManagedLeaveSummary__c> summaryListRecalced = new List<AttManagedLeaveSummary__c>();
		List<AttManagedLeaveGrantSummary__c> grantSummaryListRecalced = new List<AttManagedLeaveGrantSummary__c>();

		// 休暇サマリ、休暇付与サマリの再計算
		handler.recalcManagedLeaveSummary(leaveSummaryList, takenListAll, grantListAll, summaryListRecalced, grantSummaryListRecalced);

		// 休暇サマリの保存
		updateSObject(summaryListRecalced);
		// 再計算前の休暇付与サマリの削除
		deleteSObject([SELECT Id FROM AttManagedLeaveGrantSummary__c WHERE LeaveSummaryId__c = :summaryListRecalced]);
		// 再計算後の休暇付与サマリの保存
		insertSObject(grantSummaryListRecalced);

		// 休暇付与の再計算
		List<AttManagedLeaveGrant__c> grantListRecalced = getGrantedListWithSummary(grantListAll);
		for (AttManagedLeaveGrant__c grant : grantListRecalced) {
			Decimal daysTakenTotal = 0;
			for (AttManagedLeaveGrantSummary__c grantSummary : grant.LeaveGrantSummaries__r) {
				daysTakenTotal += grantSummary.DaysTaken__c;
			}
			grant.DaysLeft__c = grant.DaysGranted__c - daysTakenTotal;
		}
		// 休暇付与の保存
		updateSObject(grantListRecalced);
	}

	/**
	 * 承認内容変更申請の元申請のステータスを更新し、勤怠明細に反映させる。
	 * 更新値
	 *   ステータス：変更承認待ち → 無効
	 *   取消種別：なし → 変更承認済み
	 *
	 * @param originalRequestList 承認済みになった申請の元申請
	 * @param handler TODO クラスフィールドに出来るか検討する
	 */
	private static void updateOriginalRequest(List<AttDailyRequest__c> originalRequestList, AttDailyRequestTriggerHandler handler) {
		if (originalRequestList.isEmpty()) {
			return;
		}
		Set<Id> originalRequestIds = new Set<Id>();
		for(AttDailyRequest__c originalRequest : originalRequestList) {
			originalRequest.Status__c = AppRequestStatus.DISABLED.value;
			originalRequest.CancelType__c = AppCancelType.REAPPLIED.value;
			originalRequestIds.add(originalRequest.Id);
		}
		// 子申請も無効化する
		List<AttDailyRequest__c> childOriginalRequestList = getChildRequestList(originalRequestIds);
		for (AttDailyRequest__c childRequest : childOriginalRequestList) {
			childRequest.Status__c = AppRequestStatus.DISABLED.value;
			childRequest.CancelType__c = AppCancelType.REAPPLIED.value;
		}
		List<AttDailyRequest__c> reapplyingRequestList = new List<AttDailyRequest__c>();
		reapplyingRequestList.addAll(originalRequestList);
		reapplyingRequestList.addAll(childOriginalRequestList);

		updateSObject(reapplyingRequestList);

		// 元申請の無効化を勤怠明細に反映させる
		Set<Id> disableIds = new Set<Id>();
		for (AttDailyRequest__c parent : originalRequestList) {
			disableIds.add(parent.Id);
		}
		for (AttDailyRequest__c child : childOriginalRequestList) {
			disableIds.add(child.Id);
		}
		handler.reapplyAttRecordInfo(disableIds, getApprovedAttRecordList(disableIds));
		updateSObject(handler.updatedAttRecordList);
		updateSObject(handler.updatedAttSummaryList);
	}

	// 承認内容変更申請の元申請を取得する
	private static List<AttDailyRequest__c> getOriginalRequestList(Set<Id> requestIds, Map<Id, AttDailyRequest__c> newMap) {
		List<AttDailyRequest__c> originalRequestList = new List<AttDailyRequest__c>();
		for(Id requestId : requestIds) {
			AttDailyRequest__c request = newMap.get(requestId);
			if(request != null && request.OriginalRequestId__c != null) {
				originalRequestList.add(new AttDailyRequest__c(Id = request.OriginalRequestId__c));
			}
		}
		return originalRequestList;
	}
	// 子申請を取得する
	private static List<AttDailyRequest__c> getChildRequestList(Set<Id> requestIds) {
		return [SELECT Id, ParentRequestId__c FROM AttDailyRequest__c WHERE ParentRequestId__c IN :requestIds];
	}
	// 勤怠明細を取得する
	private static List<AttRecord__c> getAttRecordList(Set<Id> requestIds) {
		// 現在、休暇、休日出勤、時間外勤務、欠勤、直行直帰申請、勤務時間変更に対応
		return [
				SELECT Id, SummaryId__c,
					ReqRequestingLeave1RequestId__c, ReqRequestingLeave1RequestId__r.ParentRequestId__c,
					ReqRequestingLeave2RequestId__c, ReqRequestingLeave2RequestId__r.ParentRequestId__c,
					ReqRequestingHolidayWorkRequestId__c,
					ReqRequestingEarlyStartWorkRequestId__c, ReqRequestingOvertimeWorkRequestId__c,
					ReqRequestingAbsenceRequestId__c,
					InpRequestingDirectRequestId__c,
					ReqRequestingPatternRequestId__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest1StartTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest1EndTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest2StartTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest2EndTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest3StartTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest3EndTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest4StartTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest4EndTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest5StartTime__c,
					SummaryId__r.WorkingTypeHistoryId__r.Rest5EndTime__c,
					InpLastUpdateTime__c
				FROM AttRecord__c
				WHERE ReqRequestingLeave1RequestId__c in :requestIds
					OR ReqRequestingLeave2RequestId__c in :requestIds
					OR ReqRequestingHolidayWorkRequestId__c in :requestIds
					OR ReqRequestingEarlyStartWorkRequestId__c in :requestIds
					OR ReqRequestingOvertimeWorkRequestId__c in :requestIds
					OR ReqRequestingAbsenceRequestId__c in :requestIds
					OR InpRequestingDirectRequestId__c in :requestIds
					OR ReqRequestingPatternRequestId__c in :requestIds];
	}
	// 承認済みの勤怠申請を持つ勤怠明細を取得する
	private static List<AttRecord__c> getApprovedAttRecordList(Set<Id> requestIds) {
		// 現在、休暇、休日出勤、時間外勤務、欠勤、直行直帰申請、勤務時間変更申請に対応
		return [
				SELECT Id, SummaryId__c,
					ReqLeave1RequestId__c,
					ReqLeave2RequestId__c,
					ReqHolidayWorkRequestId__c,
					ReqEarlyStartWorkRequestId__c,
					ReqOvertimeWorkRequestId__c,
					ReqAbsenceRequestId__c,
					InpDirectRequestId__c,
					ReqPatternRequestId__c
				FROM AttRecord__c
				WHERE ReqLeave1RequestId__c in :requestIds
					OR ReqLeave2RequestId__c in :requestIds
					OR ReqHolidayWorkRequestId__c in :requestIds
					OR ReqEarlyStartWorkRequestId__c in :requestIds
					OR ReqOvertimeWorkRequestId__c in :requestIds
					OR ReqAbsenceRequestId__c in :requestIds
					OR InpDirectRequestId__c in :requestIds
					OR ReqPatternRequestId__c in :requestIds];
	}
	/**
	 * 申請一覧のうち、年次有休・日数管理休暇の休暇申請を取得する
	 * @param requestId 取得対象の申請ID
	 * @return 年次有休・日数管理休暇の休暇申請のリスト
	 */
	private static List<AttDailyRequest__c> getManagedLeaveRequest(Set<Id> requestId) {
		return [SELECT Id, AttLeaveId__c, RequestType__c, EmployeeHistoryId__r.BaseId__c, StartDate__c, EndDate__c, AttLeaveRange__c, ExcludingDate__c, LeaveDays__c, OriginalRequestId__c
				FROM AttDailyRequest__c
				WHERE Id in :requestId
					AND RequestType__c = :AttRequestType.LEAVE.value
					AND (AttLeaveId__r.Type__c = :AttLeaveType.ANNUAL.value
						OR AttLeaveId__r.DaysManaged__c = true)];
	}
	/**
	 * 申請情報の休暇に紐づく休暇サマリのうち、勤務確定していない月の情報を取得する
	 * @param requestList 日数管理する休暇申請のリスト
	 * @return 勤務確定していない月の休暇サマリ
	 */
	private static List<AttManagedLeaveSummary__c> getLeaveSummaryList(List<AttDailyRequest__c> requestList) {
		Set<Id> empIds = new Set<Id>();
		Set<Id> leaveIds = new Set<Id>();

		for (AttDailyRequest__c request : requestList) {
			empIds.add(request.EmployeeHistoryId__r.BaseId__c);
			leaveIds.add(request.AttLeaveId__c);
		}
		List<AttManagedLeaveSummary__c> retList = new List<AttManagedLeaveSummary__c>();

		for (AttManagedLeaveSummary__c leaveSummary :
				[SELECT Id, AttSummaryId__c, LeaveId__c, AttSummaryId__r.StartDate__c, AttSummaryId__r.EndDate__c,
					AttSummaryId__r.EmployeeHistoryId__r.BaseId__c
				FROM AttManagedLeaveSummary__c
				WHERE AttSummaryId__r.EmployeeHistoryId__r.BaseId__c IN :empIds
					AND LeaveId__c IN :leaveIds
					AND AttSummaryId__r.Locked__c = false]) {
			Id empId = leaveSummary.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c;
			Id leaveId = leaveSummary.LeaveId__c;
			for (AttDailyRequest__c request : requestList) {
				if (empId == request.EmployeeHistoryId__r.BaseId__c
						&& leaveId == request.AttLeaveId__c) {
					retList.add(leaveSummary);
					break;
				}
			}
		}
		return retList;
	}
	/**
	 * 指定した休暇サマリの全消費データを取得する
	 * @param summaryList
	 * @return 休暇消費データのリスト
	 */
	private static List<AttManagedLeaveTake__c> getTakenListBySummary(List<AttManagedLeaveSummary__c> summaryList) {
		Set<Id> summaryIds = new Set<Id>();
		for (AttManagedLeaveSummary__c leaveSummary : summaryList) {
			summaryIds.add(leaveSummary.id);
		}
		return [SELECT Id, LeaveSummaryId__c, LeaveRequestId__c, StartDate__c, EndDate__c, DaysTaken__c,
						LeaveRequestId__r.AttLeaveId__c,
						LeaveSummaryId__r.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c
					FROM AttManagedLeaveTake__c
					WHERE LeaveSummaryId__c IN :summaryIds];
	}
	/**
	 * TODO
	 * 指定休暇サマリの有効な休暇付与を取得する
	 * 休暇付与の残日数を計算している？
	 */
	private static List<AttManagedLeaveGrant__c> getGrantedListBySummary(List<AttManagedLeaveSummary__c> summaryList) {
		Set<Id> empIds = new Set<Id>();
		Set<Id> leaveIds = new Set<Id>();

		for (AttManagedLeaveSummary__c leaveSummary : summaryList) {
			empIds.add(leaveSummary.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c);
			leaveIds.add(leaveSummary.LeaveId__c);
		}
		List<AttManagedLeaveGrant__c> retList = new List<AttManagedLeaveGrant__c>();
		for (AttManagedLeaveGrant__c leaveGrant :
				[SELECT Id, LeaveId__c, EmployeeBaseId__c, ValidFrom__c, ValidTo__c, OriginalDaysGranted__c, DaysGranted__c, DaysLeft__c
					,(SELECT DaysTaken__c
					FROM LeaveGrantSummaries__r
					WHERE LeaveSummaryId__r.AttSummaryId__r.Locked__c = true)
				FROM AttManagedLeaveGrant__c
				WHERE LeaveId__c IN :leaveIds
					AND EmployeeBaseId__c IN :empIds
					AND Removed__c = false]) {
			Id empId = leaveGrant.EmployeeBaseId__c;
			Id leaveId = leaveGrant.LeaveId__c;
			for (AttManagedLeaveSummary__c leaveSummary : summaryList) {
				if (empId == leaveSummary.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c
						&& leaveId == leaveSummary.LeaveId__c) {
					Decimal daysTakenTotal = 0;
					for (AttManagedLeaveGrantSummary__c grantSummary : leaveGrant.LeaveGrantSummaries__r) {
						daysTakenTotal += grantSummary.DaysTaken__c;
					}
					leaveGrant.DaysLeft__c = leaveGrant.DaysGranted__c - daysTakenTotal;
					retList.add(leaveGrant);
					break;
				}
			}
		}
		return retList;
	}
	// 指定休暇サマリの有効付与を取得する
	private static List<AttManagedLeaveGrant__c> getGrantedListWithSummary(List<AttManagedLeaveGrant__c> grantList) {
		return [SELECT Id, OriginalDaysGranted__c, DaysGranted__c
					,(SELECT DaysTaken__c FROM LeaveGrantSummaries__r)
				FROM AttManagedLeaveGrant__c
				WHERE Id IN :grantList AND Removed__c = false];
	}

	/**
	 * 年次有休・日数管理休暇の休暇消費を取得する
	 * @param requestIds 取得対象の申請ID
	 * @param summaryLock 勤務確定済み:true / 勤務確定済ではない:false
	 * @return 休暇消費のリスト
	 */
	private static List<AttManagedLeaveTake__c> getTakenListByRequestId(Set<Id> requestIds, Boolean summaryLock) {
		return [SELECT Id, LeaveRequestId__c
				FROM AttManagedLeaveTake__c
				WHERE LeaveRequestId__c in :requestIds
				AND LeaveSummaryId__r.AttSummaryId__r.Locked__c = :summaryLock];
	}

	/**
	 * 元申請の休暇消費の申請IDに承認内容変更申請IDを設定する
	 * @param managedLeaveRequestList 年次有休・日数管理休暇の休暇申請
	 * @return 承認内容変更申請IDを設定した休暇消費
	 */
	private static List<AttManagedLeaveTake__c> updateLeaveRequestId(List<AttDailyRequest__c> managedLeaveRequestList) {
		// 休暇申請の元申請IDを取得する
		Set<Id> originalRequestIds = new Set<Id>();
		for (AttDailyRequest__c request : managedLeaveRequestList) {
			originalRequestIds.add(request.OriginalRequestId__c);
		}

		// 元申請IDの休暇消費を取得する（勤務確定済みの休暇消費）
		List<AttManagedLeaveTake__c> leaveTakeList = getTakenListByRequestId(originalRequestIds, true);

		// 休暇消費の申請IDを承認内容変更申請IDに付け替える
		for (AttDailyRequest__c request : managedLeaveRequestList) {
			for (AttManagedLeaveTake__c leaveTake : leaveTakeList) {
				if (leaveTake.LeaveRequestId__c == request.OriginalRequestId__c) {
					leaveTake.LeaveRequestId__c = request.Id;
				}
			}
		}
		return leaveTakeList;
	}

	/**
	 * @description insert
	 * ※入力規則でエラーが発生した場合、APIが返すエラーメッセージを入力規則側で指定したものにする
	 * @param SObject to insert
	 * @return void
	 */
	private static void insertSObject(List<SObject> objList) {
		try {
			// トリガではFLSチェック不要
			insert objList;
		} catch (DMLException e) {
			Trigger.New[0].addError(e.getDmlMessage(0));
		}
	}

	/**
	 * @description update
	 * ※入力規則でエラーが発生した場合、APIが返すエラーメッセージを入力規則側で指定したものにする
	 * @param SObject to update
	 * @return void
	 */
	private static void updateSObject(List<SObject> objList) {
		try {
			// トリガではFLSチェック不要
			update objList;
		} catch (DMLException e) {
			Trigger.New[0].addError(e.getDmlMessage(0));
		}
	}

	/**
	 * @description delete
	 * ※入力規則でエラーが発生した場合、APIが返すエラーメッセージを入力規則側で指定したものにする
	 * @param SObject to delete
	 * @return void
	 */
	private static void deleteSObject(List<SObject> objList) {
		try {
			// トリガではFLSチェック不要
			delete objList;
		} catch (DMLException e) {
			Trigger.New[0].addError(e.getDmlMessage(0));
		}
	}
}