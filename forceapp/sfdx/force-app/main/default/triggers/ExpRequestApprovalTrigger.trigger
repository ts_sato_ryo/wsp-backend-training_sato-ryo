/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Trigger of Expense Request Approval
 */
trigger ExpRequestApprovalTrigger on ExpRequestApproval__c (before update, after insert) {

	if (Trigger.isInsert) {
		insertSharingRecords();
	} else {
		updateRequestRecord();
	}

	/*
	 * Insert sharing record
	 */
	private void insertSharingRecords() {
		// Create a map of original approvers (* This map's key is "userId").
		Map<Id, EmployeeBaseEntity> approverMap = createApproverMap(Trigger.new);
		// Retrieve every settings of the original approvers
		Map<Id, List<DelegatedApproverSettingEntity>> settingMap = retrieveSettingsOfApprovers(approverMap.values());

		if ((settingMap == null) || (settingMap.isEmpty())) {
			return ;
		}

		List<ExpRequestApproval__Share> sharingList = new List<ExpRequestApproval__Share>();
		for (ExpRequestApproval__c request : Trigger.new) {
			Id originalApproverBaseId = approverMap.get(request.Approver01Id__c).id;
			for (DelegatedApproverSettingEntity setting : settingMap.get(originalApproverBaseId)) {
				if (setting.canApproveExpenseRequestByDelegate) {
					ExpRequestApproval__Share share = new ExpRequestApproval__Share();
					share.ParentId = request.Id;
					share.UserOrGroupId = setting.delegatedApprover.userId;
					share.AccessLevel = 'Edit';
					share.RowCause = ExpRequestApprovalRepository.SHARE_ROW_CAUSE;
					sharingList.add(share);
				}
			}
		}
		insert sharingList;
	}

	/*
	 * Create a employee base map of approvers in created requests.
	 * @param requestList Request Id created
	 * @return Employee base map of approvers
	 */
	private Map<Id, EmployeeBaseEntity> createApproverMap(List<ExpRequestApproval__c> requestList) {
		Set<Id> appUserIdList = new Set<Id>();
		for (ExpRequestApproval__c request : requestList) {
			appUserIdList.add(request.Approver01Id__c);
		}
		return new EmployeeService().getBaseMapByUserIds(appUserIdList);
	}

	/*
	 * Retrieve every settings of the original approvers.
	 * @param List of target employee base to retrieve the settings
	 * @return List of Delegated Approver setting
	 */
	private Map<Id, List<DelegatedApproverSettingEntity>> retrieveSettingsOfApprovers(List<EmployeeBaseEntity> approverBaseList) {
		Set<Id> empBaseIds = new Set<Id>();
		for (EmployeeBaseEntity base : approverBaseList) {
			empBaseIds.add(base.id);
		}
		return new DelegatedApproverSettingService().getSettingMap(empBaseIds);
	}

	/*
	 * Update request record along with approval status.
	 */
	private void updateRequestRecord() {
		ExpRequestApprovalTriggerHandler handler = new ExpRequestApprovalTriggerHandler(Trigger.oldMap, Trigger.newMap);

		// Get requests which will be approved or discarded
		if (handler.approvedRequestIds.isEmpty() && handler.disabledRequestIds.isEmpty()) {
			return;
		}
		// Update request records
		handler.applyProcessInfo();
	}
}