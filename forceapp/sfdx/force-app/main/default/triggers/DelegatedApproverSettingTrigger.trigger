/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Trigger of Delegated Approver setting
 */
trigger DelegatedApproverSettingTrigger on ComDelegatedApproverSetting__c (after insert, before delete) {

	if (Trigger.isInsert) {
		createSharingRecords();
	} else if (Trigger.isDelete) {
		deleteSharingRecords();
	}

	/*
	 * Create sharing record based on delegated approver setting added.
	 */
	private void createSharingRecords() {
		// Get the original approver
		Id originalEmployeeId = Trigger.new.get(0).EmployeeBaseId__c;
		EmployeeBaseEntity oriApproverBase = new EmployeeService().getEmployee(originalEmployeeId);

		// Create delegated approvers map
		// Not to retrieve employee data many times, retrieve it here and pass it down to each methods.
		Map<Id, EmployeeBaseEntity> approverMap = createDelegatedApproverMap(Trigger.new);

		// Insert sharing record for each approval requests
		insertReportSharingRecords(oriApproverBase, approverMap);
		insertRequestSharingRecords(oriApproverBase, approverMap);
	}

	/*
	 * Create delegated approver employee base map from the setting list given.
	 * @param settingList Delegated approver setting list which get approver Ids from
	 * @return Employee base map
	 */
	private Map<Id, EmployeeBaseEntity> createDelegatedApproverMap(List<ComDelegatedApproverSetting__c> settingList) {
		// Extract approver employee base Ids
		List<Id> approverIdList = new List<Id>();
		for (ComDelegatedApproverSetting__c setting : settingList) {
			approverIdList.add(setting.DelegatedApproverBaseId__c);
		}

		return new EmployeeService().getBaseMap(approverIdList);
	}

	/*
	 * Insert sharing records for Expense Report Request.
	 * @param oriApproverBase Original approver employee base
	 * @param approverMap Delegated approver employee base map to be used to convert employee Id to user Id
	 */
	private void insertReportSharingRecords(EmployeeBaseEntity oriApproverBase, Map<Id, EmployeeBaseEntity> approverMap) {

		List<ExpReportRequestEntity> reportList = getPendingReportListOfApprover(oriApproverBase.userId);

		List<ExpReportRequest__Share> reportSharingList = new List<ExpReportRequest__Share>();

		for (ComDelegatedApproverSetting__c setting : Trigger.new) {
			// Only when ApproveExpenseReportByDelegate__c is true, create a sharing record
			if (setting.ApproveExpenseReportByDelegate__c == true) {
				for (ExpReportRequestEntity report : reportList) {
					ExpReportRequest__Share share = new ExpReportRequest__Share();
					share.ParentId = report.Id;
					share.UserOrGroupId = approverMap.get(setting.DelegatedApproverBaseId__c).userId;
					share.AccessLevel = 'Edit';
					share.RowCause = ExpReportRequestRepository.SHARE_ROW_CAUSE;
					reportSharingList.add(share);
				}
			}
		}

		insert reportSharingList;
	}

	/*
	 * Insert sharing records for Expense Request Approval.
	 * @param oriApproverBase Original approver employee base
	 * @param approverMap Delegated approver employee base map to be used to convert employee ID to user ID
	 */
	private void insertRequestSharingRecords(EmployeeBaseEntity oriApproverBase, Map<Id, EmployeeBaseEntity> approverMap) {

		List<ExpRequestApprovalEntity> requestList = getPendingRequestListOfApprover(oriApproverBase.userId);

		List<ExpRequestApproval__Share> requestSharingList = new List<ExpRequestApproval__Share>();
		for (ComDelegatedApproverSetting__c setting : Trigger.new) {
			// Only when ApproveExpenseReportByDelegate__c is true, create a sharing record
			if (setting.ApproveExpenseRequestByDelegate__c == true) {
				for (ExpRequestApprovalEntity request : requestList) {
					ExpRequestApproval__Share share = new ExpRequestApproval__Share();
					share.ParentId = request.Id;
					share.UserOrGroupId = approverMap.get(setting.DelegatedApproverBaseId__c).userId;
					share.AccessLevel = 'Edit';
					share.RowCause = ExpRequestApprovalRepository.SHARE_ROW_CAUSE;
					requestSharingList.add(share);
				}
			}
		}

		insert requestSharingList;
	}


	/*
	 * Get pending ExpReportRequest list by specified user id of approver
	 * @param appUserId Approver user ID
	 * @return Pending ExpReportRequest entity list
	 */
	private List<ExpReportRequestEntity> getPendingReportListOfApprover(Id appUserId) {
		return new ExpReportRequestRepository().getEntityListByApprover01Id(appUserId, AppRequestStatus.PENDING);
	}

	/*
	 * Get pending ExpenseRequestApproval list by specified user id of approver
	 * @param appUserId Approver user ID
	 * @return Pending ExpenseRequestApproval list
	 */
	private List<ExpRequestApprovalEntity> getPendingRequestListOfApprover(Id appUserId) {
		return new ExpRequestApprovalRepository().getEntityListByApprover01Id(appUserId, AppRequestStatus.PENDING);
	}

	/*
	 * Delete sharing records based on delegated approver setting deleted.
	 */
	private void deleteSharingRecords() {
		// Get the original approver
		Id originalEmployeeId = Trigger.old.get(0).EmployeeBaseId__c;
		EmployeeBaseEntity oriApproverBase = new EmployeeService().getEmployee(originalEmployeeId);

		// Create delegated approvers map
		Map<Id, EmployeeBaseEntity> approverMap = createDelegatedApproverMap(Trigger.old);
		// Get delegated approver ID list
		List<Id> approverIds = getDeletedApproverIds(approverMap);

		// Delete sharing record for each approval requests
		deleteReportSharingRecords(oriApproverBase, approverIds);
		deleteRequestSharingRecords(oriApproverBase, approverIds);
	}

	/*
	 * Delete sharing records of Expense Report Request
	 * @param oriApproverBase Original approver employee base
	 * @param approverIds Approver user Ids set in the deleted settings
	 */
	private void deleteReportSharingRecords(EmployeeBaseEntity oriApproverBase, List<Id> approverIds) {
		List<ExpReportRequest__Share> shareReportList = getReportSharingRecordToDelete(oriApproverBase, approverIds);
		delete shareReportList;
	}

	/*
	 * Delete sharing records of Expense Request Approval
	 * @param oriApproverBase Original approver employee base
	 * @param approverIds Approver user Ids set in the deleted settings
	 */
	private void deleteRequestSharingRecords(EmployeeBaseEntity oriApproverBase, List<Id> approverIds) {
		List<ExpRequestApproval__Share> shareReportList = getRequestSharingRecordToDelete(oriApproverBase, approverIds);
		delete shareReportList;
	}

	/*
	 * Get ExpReportRequest sharing records to delete.
	 * @param Original approver employee base entity
	 * @param approverIds Target delegated approver ids to delete
	 * @return ExpReportRequest__Share list to delete
	 */
	private List<ExpReportRequest__Share> getReportSharingRecordToDelete(EmployeeBaseEntity oriApproverBase, List<Id> approverIds) {
		// Get report Id list of the original approver
		List<Id> reportIds = getPendingReportIds(oriApproverBase.userId);

		return [SELECT Id FROM ExpReportRequest__Share WHERE RowCause = 'DelegatedApprover__c' AND ParentId IN :reportIds AND UserOrGroupId IN :approverIds];
	}

	/*
	 * Get ExpRequestApproval sharing records to delete.
	 * @param Original approver employee base entity
	 * @param approverIds Target delegated approver ids to delete
	 * @return ExpRequestApproval__Share list to delete
	 */
	private List<ExpRequestApproval__Share> getRequestSharingRecordToDelete(EmployeeBaseEntity oriApproverBase, List<Id> approverIds) {
		// Get request Id list of the original approver
		List<Id> requestIds = getPendingRequestIds(oriApproverBase.userId);

		return [SELECT Id FROM ExpRequestApproval__Share WHERE RowCause = 'DelegatedApprover__c' AND ParentId IN :requestIds AND UserOrGroupId IN :approverIds];
	}

	/*
	 * Get ID list of pending Expense Report Request by specified approver.
	 * @param appUserId Approval user id
	 * @return ID list of pending Expense Report Request
	 */
	private List<Id> getPendingReportIds(Id appUserId) {
		List<ExpReportRequestEntity> reportList = getPendingReportListOfApprover(appUserId);
		return makeIdList(reportList);
	}

	/*
	 * Get ID list of pending Expense Request Approver by specified approver.
	 * @param appUserId Approval user id
	 * @return ID list of pending Expense Request Approver
	 */
	private List<Id> getPendingRequestIds(Id appUserId) {
		List<ExpRequestApprovalEntity> reportList = getPendingRequestListOfApprover(appUserId);
		return makeIdList(reportList);
	}

	/*
	 * Get delegated approver user ID list in deleted settings.
	 * @param approverMap Entity map of delegated approver employee base
	 * @return Delegated approver user ID list in deleted settings.
	 */
	private List<Id> getDeletedApproverIds(Map<Id, EmployeeBaseEntity> approverMap) {
		List<Id> userIdList = new List<Id>();
		for (ComDelegatedApproverSetting__c setting : Trigger.old) {
			userIdList.add(approverMap.get(setting.DelegatedApproverBaseId__c).userId);
		}
		return userIdList;
	}

	/*
	 * Make Id list from the given entity list
	 * @param entity list to extract IDs
	 * @return ID list
	 */
	private List<Id> makeIdList(List<Entity> entityList) {
		List<Id> idList = new List<Id>();
		for (Entity entity : entityList) {
			idList.add(entity.id);
		}
		return idList;
	}

}