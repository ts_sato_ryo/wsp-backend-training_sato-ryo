/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Trigger on ComExtendedItemCustom__c Before Insert or Before Update (Up
 *
 * @group Expense
 */
trigger ExtendedItemCustomTrigger on ComExtendedItemCustom__c (before insert, before update) {

	ExtendedItemCustomTriggerHandler triggerHandler = new ExtendedItemCustomTriggerHandler(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
	// NOTE: UPSERT operation will be simply becomes Insert, followed by Update
	if (Trigger.isInsert) {
		triggerHandler.validateInsert();
	} else if (Trigger.isUpdate) {
		triggerHandler.validateUpdate();
	}
}