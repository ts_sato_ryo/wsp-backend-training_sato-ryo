/**
 * @group 工数
 *
 * 工数確定申請のトリガー
 */
trigger TimeRequestTrigger on TimeRequest__c (before update) {
	// 承認済みや却下となる承認申請を取得
	TimeRequestTriggerHandler handler = new TimeRequestTriggerHandler(Trigger.oldMap, Trigger.newMap);
	if (handler.approvedRequestIds.isEmpty() && handler.disabledRequestIds.isEmpty())
	{
		return;
	}
	// 却下・取消の申請のみの処理
	if (! handler.disabledRequestIds.isEmpty()) {
		// 工数サマリーを更新する
		updateSObject(handler.updatedTimeSummaryList);
	}
	// 申請レコード更新
	handler.applyProcessInfo();

	/**
	 * @description update
	 * ※入力規則でエラーが発生した場合、APIが返すエラーメッセージを入力規則側で指定したものにする
	 * @param objList List of SObject to update
	 * @return void
	 */
	private static void updateSObject(List<SObject> objList) {
		try {
			update objList;
		} catch(DMLException e) {
			Trigger.New[0].addError(e.getDmlMessage(0));
		}
	}
}