/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 汎用稟議申請のトリガー
 */
trigger ComGeneralRequestTrigger on ComGeneralRequest__c (before insert, before update) {

	if (Trigger.isInsert) {
		for (ComGeneralRequest__c generalRequest : Trigger.new) {
			// 申請作成時のステータスはNULLにする（デフォルト値に無効が設定されているため、置き換える必要がある）
			generalRequest.Status__c = null;
		}
	}
	if (Trigger.isUpdate) {
		// 承認待ち、承認済み、却下、取消の承認申請を取得する
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(Trigger.oldMap, Trigger.newMap);
		if (Trigger.isBefore) {
			// 申請データに、画面からは設定できない申請情報を設定する
			if (handler.pendingRequest != null) {
				try {
					handler.applyGeneralRequestInfo();
				} catch (App.IllegalStateException e) {
					Trigger.New[0].addError(e.getMessage());
				}
			}
			// 申請データに、承認プロセス情報を設定する
			if (!handler.approvedRequestIds.isEmpty() || !handler.disabledRequestIds.isEmpty()) {
				handler.applyProcessInfo();
			}
		}
	}
}