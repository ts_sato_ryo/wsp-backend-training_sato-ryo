/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠申請のトリガー
 */
trigger AttRequestTrigger on AttRequest__c (after update, before update) {
	if (Trigger.isUpdate) {
		// 承認済みや却下となる承認申請を取得
		AttRequestTriggerHandler handler = new AttRequestTriggerHandler(Trigger.oldMap, Trigger.newMap);
		if (handler.approvedRequestIds.isEmpty() && handler.disabledRequestIds.isEmpty()) {
			return;
		}
		if (Trigger.isBefore) {
			handler.applyProcessInfo();
		}
		else {
			// 却下・取消の申請のみ処理を行う
			if (handler.disabledRequestIds.isEmpty()) {
				return;
			}

			// 勤怠サマリーを更新する
			updateSObject(handler.updatedAttSummaryList);

			// 勤怠明細を更新する
			Set<Id> summaryIds = new Set<Id>();
			for (AttSummary__c summary : handler.updatedAttSummaryList) {
				summaryIds.add(summary.Id);
			}
			List<AttRecord__c> attRecordList = getAttRecordList(summaryIds);
			handler.applyAttRecordInfo(attRecordList);
			updateSObject(handler.updatedAttRecordList);
		}
	}
	// 勤怠明細を取得する
	private static List<AttRecord__c> getAttRecordList(Set<Id> summaryIds) {
		return [
				SELECT Id, OutWorkAbsenceDays__c
				FROM AttRecord__c
				WHERE SummaryId__c in :summaryIds];
	}

	/**
		* @description update
			※入力規則でエラーが発生した場合、APIが返すエラーメッセージを入力規則側で指定したものにする
		* @param SObject to update
		* @return void
		*/
	private static void updateSObject(List<SObject> objList) {
		try {
			update objList;
		} catch(DMLException e) {
			Trigger.New[0].addError(e.getDmlMessage(0));
		}
	}
}