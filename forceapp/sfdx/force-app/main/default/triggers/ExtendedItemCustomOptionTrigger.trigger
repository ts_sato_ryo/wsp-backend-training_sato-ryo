/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Trigger on ComExtendedItemCustomOption__c Before Insert or Before Update or Before Delete
 *
 * @group Expense
 */
trigger ExtendedItemCustomOptionTrigger on ComExtendedItemCustomOption__c (before insert, before update, before delete) {

	ExtItemCustomOptionTriggerHandler triggerHandler = new ExtItemCustomOptionTriggerHandler(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
	if (Trigger.isInsert) {
		triggerHandler.validateInsert();
	} else if (Trigger.isUpdate) {
		triggerHandler.validateUpdate();
	} else if (Trigger.isDelete) {
		triggerHandler.validateDelete();
	}
}