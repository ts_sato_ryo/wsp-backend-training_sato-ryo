/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * Salesforceユーザのリポジトリ
 */
@isTest
private class UserRepositoryTest {

	/**
	 * 指定したIDのユーザが取得できることを確認する
	 */
	@isTest static void getEntityTest() {
		List<User> testRecords = ComTestDataUtility.createStandardUsers('testユーザ', 3);
		User targetRecord = testRecords[1];

		Test.startTest();
			UserRepository repo = new UserRepository();
			UserEntity resEntity = repo.getEntity(targetRecord.id);
		Test.stopTest();

		// 検証する
		assertEntity(targetRecord, resEntity);
	}

	/**
	 * 指定した複数IDのユーザが取得できることを確認する
	 */
	@isTest static void getEntityListTestSpecifiedIds() {
		List<User> testRecords = ComTestDataUtility.createStandardUsers('testユーザ', 5);
		Map<Id, User> targetRecordMap = new Map<Id, User> {
			testRecords[1].Id => testRecords[1],
			testRecords[2].Id => testRecords[2]};
		List<Id> targetIds = new List<Id>(targetRecordMap.keySet());

		Test.startTest();
			UserRepository repo = new UserRepository();
			List<UserEntity> resEntities = repo.getEntityList(targetIds);
		Test.stopTest();

		// 検証する
		System.assertEquals(targetIds.size(), resEntities.size());

		Map<Id, UserEntity> resEntityMap = new Map<Id, UserEntity>();
		for (UserEntity resEntity : resEntities) {
			resEntityMap.put(resEntity.id, resEntity);
		}
		for (Id targetId : targetIds) {
			UserEntity resEntity = resEntityMap.get(targetId);
			System.assertNotEquals(null, resEntity, '取得対象のエンティティが含まれていませんでした。');
			assertEntity(targetRecordMap.get(targetId), resEntity);
		}
	}

	/**
	 * IDリストがnullの場合は全てのユーザが取得できることを確認する
	 */
	@isTest static void getEntityListTestAll() {
		List<User> testRecords = ComTestDataUtility.createStandardUsers('testユーザ', 5);

		Test.startTest();
			UserRepository repo = new UserRepository();
			List<UserEntity> resEntities = repo.getEntityList(null);
		Test.stopTest();


		// 検証する
		List<User> allUserRecords = [SELECT
				Id,
				Name,
				UserName,
				ProfileId,
				Profile.Name
			FROM User
			ORDER BY Name
		];

		System.assertEquals(allUserRecords.size(), resEntities.size());

		Map<Id, UserEntity> resEntityMap = new Map<Id, UserEntity>();
		for (UserEntity resEntity : resEntities) {
			resEntityMap.put(resEntity.id, resEntity);
		}
		for (User allUserRecord : allUserRecords) {
			// 各項目の値の検証は getEntityListTestSpecifiedIds で実施
			UserEntity resEntity = resEntityMap.get(allUserRecord.Id);
			System.assertNotEquals(null, resEntity, '取得対象のエンティティが含まれていませんでした。');
		}
	}

	/**
	 * エンティティに想定していたユーザの情報が設定されていることを確認する。
	 * @param expRecord 期待値
	 * @param actEntity 実際に取得したエンティティ
	 */
	private static void assertEntity(User expRecord, UserEntity actEntity) {

		System.assertEquals(expRecord == null, actEntity == null);
		System.assertEquals(expRecord.Id, actEntity.id);
		System.assertEquals(expRecord.UserName, actEntity.userName);
		System.assertEquals(expRecord.LanguageLocaleKey, actEntity.language);
		System.assertEquals(expRecord.ProfileId, actEntity.profileId);

		// プロファイルの確認
		if (expRecord.ProfileId != null) {
			User expProfileUser = [SELECT profile.Name From User WHERE Id = :expRecord.Id];
			System.assertEquals(expProfileUser.Profile.Name, actEntity.profile.name);
		}
	}

}
