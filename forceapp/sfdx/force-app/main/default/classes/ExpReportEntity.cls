/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * 経費精算のエンティティ Entity class for ExpReport
 */
public with sharing class ExpReportEntity extends ExpReport {

	/** 申請情報の値オブジェクト Object of approval request info */
	public class Request extends ExpReport.Request {
		/** コンストラクタ Constructor */
		public Request(AppRequestStatus status, AppCancelType cancelType, Boolean isConfirmationRequired, AppDatetime requestTime) {
			this.status = status;
			this.cancelType = cancelType;
			this.isConfirmationRequired = isConfirmationRequired;
			this.requestTime = requestTime;
		}
	}

	/** 項目の定義(変更管理で使用する) Fields definition */
	public enum Field {
		ACCOUNTING_DATE,
		ACCOUNTING_PERIOD_ID,
		EXP_PRE_REQUEST_ID,
		EXP_REQUEST_ID,
		MOBILE_INCOMPATIBLE_COUNT,
		RECORD_LIST,
		REMARKS
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** 値を変更した項目のリスト Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;


	/**
	 * コンストラクタ Constructor
	 */
	public ExpReportEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** Accounting Date */
	public AppDate accountingDate {
		get;
		set {
			accountingDate = value;
			setChanged(Field.ACCOUNTING_DATE);
		}
	}

	/** Accounting Period ID */
	public Id accountingPeriodId {
		get;
		set {
			accountingPeriodId = value;
			setChanged(Field.ACCOUNTING_PERIOD_ID);
		}
	}

	/** Company ID (for reference only) */
	public Id companyId {get; set;}

	/** Department Code */
	public String departmentCode {get; set;}

	/** Department Name (for reference only) */
	public String departmentName {
		get {
			return departmentNameL != null ? departmentNameL.getValue(): null;
		}
	}
	public AppMultiString departmentNameL {get; set;}

	/** Pre-Request ID */
	public Id expPreRequestId {
		get;
		set {
			expPreRequestId = value;
			setChanged(Field.EXP_PRE_REQUEST_ID);
		}
	}

	/** Expense Request */
	public ExpRequestEntity expPreRequest {get; set;}

	/** Expense Report Request ID */
	public Id expRequestId {
		get;
		set {
			expRequestId = value;
			setChanged(Field.EXP_REQUEST_ID);
		}
	}

	/** Approval Request */
	public ExpReportEntity.Request expRequest {
		get;
		set;
	}

	/** Last Modified Data */
	public AppDatetime lastModifiedDate {get; set;}

	/** Number of Incompatibility the Report has (such as Vendor in Report, Hotel Fee, File Attachment etc   */
	public Decimal mobileIncompatibleCount {
		get;
		set {
			mobileIncompatibleCount = value;
			setChanged(Field.MOBILE_INCOMPATIBLE_COUNT);
		}
	}

	/** Remarks */
	public String remarks {
		get;
		set {
			remarks = value;
			setChanged(Field.REMARKS);
		}
	}

	/** Report No. (for reference only) */
	public String reportNo {get; set;}

	/** Vendor Used In (for reference only) */
	public ExpVendorUsedIn vendorUsedIn {get; set;}

	/** Vendor Required For (for reference only) */
	public ExpVendorRequiredFor vendorRequiredFor {get; set;}

	/** Expense Records */
	public List<ExpRecordEntity> recordList {get; private set;}


	/**
	 * 経費明細リストをリプレースする（リストをコピーする） Replace recordList (copy)
	 * @param recordList リプレース対象の経費明細リスト List of ExpRecordEntity to replace
	 */
	public void replaceRecordList(List<ExpRecordEntity> recordList) {
		this.recordList = new List<ExpRecordEntity>(recordList);
		setChanged(Field.RECORD_LIST);
	}

	/**
	 * 項目の値を変更済みに設定する Mark field as changed
	 * @param Field 変更した項目 Changed field
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する Checks if the value of the field has been changed
	 * @param field 取得対象の項目 Target field
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする Resets field changed info
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
		// 経費明細リストの変更もリセットする Resets ExpRecord changes
		if (recordList != null) {
			for (ExpRecordEntity entity : recordList) {
				entity.resetChanged();
			}
		}
	}

	/**
	 * 申請の詳細ステータスを取得する Retrieves detailed status of ExpReportRequest
	 */
	public ExpReportRequestEntity.DetailStatus getRequestDetailStatus() {
		ExpReportRequestEntity requestEntity = new ExpReportRequestEntity();

		requestEntity.setId(this.expRequestId);
		if (this.expRequest != null) {
			requestEntity.status = this.expRequest.status;
			requestEntity.cancelType = this.expRequest.cancelType;
		}

		return requestEntity.getDetailStatus();
	}

	/*
	 * Return the field value
	 * @param name Target field name
	 */
	public override Object getValue(String name) {
		Field field = ExpReportEntity.FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

		return super.getValue(name);
	}

	/**
	 * Return the field value
	 * @param field Target field to get the value
	 */
	private Object getValue(ExpReportEntity.Field field) {
		if (field == ExpReportEntity.Field.EXP_REQUEST_ID) {
			return expRequestId;
		} else if (field == ExpReportEntity.Field.EXP_PRE_REQUEST_ID) {
			return expPreRequestId;
		} else if (field == ExpReportEntity.Field.ACCOUNTING_DATE) {
			return accountingDate;
		} else if (field == ExpReportEntity.Field.ACCOUNTING_PERIOD_ID) {
			return accountingPeriodId;
		} else if (field == ExpReportEntity.Field.REMARKS) {
			return remarks;
		} else if (field == ExpReportEntity.Field.MOBILE_INCOMPATIBLE_COUNT) {
			return mobileIncompatibleCount;
		} else if (field == ExpReportEntity.Field.RECORD_LIST) {
			return recordList;
		}
		
		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}