/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 勤務体系履歴切り替えバッチ
 */
public with sharing class AttWorkingTypeCurrentUpdateBatch extends CurrentHistoryUpdateBatchBase implements Schedulable {

	/**
	 * コンストラクタ
	 */
	public AttWorkingTypeCurrentUpdateBatch() {
		// ベース・履歴オブジェクトのAPI参照名を渡す
		super(AttWorkingTypeBase__c.getSObjectType(), AttWorkingTypeHistory__c.getSObjectType());
	}

	/**
	 * Apex実行スケジュールを登録する
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc){
		AttWorkingTypeCurrentUpdateBatch b = new AttWorkingTypeCurrentUpdateBatch();
		Id jobId = Database.executeBatch(b);
	}

	/**
	 * バッチ開始処理
	 * @param  bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public override Database.QueryLocator start(Database.BatchableContext bc) {
		// ベースオブジェクトを全取得する
		return super.start(bc);
	}

	/**
	 * バッチ実行処理
	 * @param  bc    バッチコンテキスト
	 * @param  scope
	 */
	public override void execute(Database.BatchableContext bc, List<Sobject> scope) {
		super.execute(bc, scope);
	}

	/**
	 * バッチ終了処理
	 * @param  bc    バッチコンテキスト
	 */
	public override void finish(Database.BatchableContext bc) {
		super.finish(bc);
	}

}
