/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 経費申請タイプのサービス Service class for Expense Report Type
 */
public with sharing class ExpReportTypeService {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * Create a new expense report type
	 * @param newEntity Target entity
	 * @param expTypeIdList List of Expense Type linked to Report Type
	 * @return Id of the created record
	 */
	public Id createNewExpReportType(ExpReportTypeEntity newEntity, List<String> expTypeIdList) {
		// create a new entity
		ExpReportTypeEntity entity = newEntity.copy();

		// save
		Id savedReportId = saveExpReportType(entity);

		if (expTypeIdList != null && !expTypeIdList.isEmpty()) {
			saveExpReportTypeExpTypeLink(savedReportId, expTypeIdList);
		}

		return savedReportId;
	}

	/**
	 * Update an expense report type
	 * @param updateEntity Target entity(Id must be set)
	 * @param expTypeIdList List of Expense Type linked to Report Type
	 */
	public void updateExpReportType(ExpReportTypeEntity updateEntity, List<Id> expTypeIdList) {
		// 対象レコードが存在するかを確認 Check if the target record exists
		ExpReportTypeEntity entity = (new ExpReportTypeRepository()).getEntity(updateEntity.id);
		if (entity == null) {
			// 存在しない場合はエラー The target record can not be found
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
		}

		// 拡張項目が変更されている場合 When the extended item, CostCenter, Job, Vendor and UseFileAttachment has been changed
		if (isChangedExtendedItem(entity, updateEntity) || isChangedCostCenterUsed(entity, updateEntity) ||
				isChangedJobUsed(entity, updateEntity) || isChangedVendorUsed(entity, updateEntity) ||
				isChangedUseFileAttachment(entity, updateEntity)) {
			// 関連するトランザクションレコードが存在する場合はエラー If related transaction records exist, exception is thrown
			if (existsRelatedTransactionRecords(updateEntity.id)) {
				throw new App.IllegalStateException(MESSAGE.Com_Err_CannotChangeReference);
			}
		}

		// 値を更新用のエンティティにセットする Set the value to entity from the parameter entity
		setUpdateValueToEntity(entity, updateEntity);
		// 更新実行 save
		saveExpReportType(entity);
		// Update Expense Report Type Expense Type Link
		updateExpReportTypeExpTypeLink(updateEntity.id, expTypeIdList);
	}

	/**
	 * Delete an expense report type group
	 * @param expReportTypeId Target entity(Id must be set)
	 */
	public void deleteExpReportType (Id expReportTypeId) {

		// 関連するトランザクションレコードが存在する場合はエラー If related transaction records exist, exception is thrown
		if (existsRelatedTransactionRecords(expReportTypeId)) {
			throw new App.IllegalStateException(MESSAGE.Com_Err_CannotDeleteReference);
		}

		try {
			// 削除 / delete
			(new ExpReportTypeRepository()).deleteEntity(expReportTypeId);
		} catch (DmlException e) {
			// if the record is already deleted(System.StatusCode.ENTITY_IS_DELETED), process is regarded as successful
			if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
				// TODO Output a debug log tentatively
				System.debug(e);

				// TODO Ultimately, it needs to check whether there are records referring to the target record
				// Message：Failed to delete. It may be referenced from other records.
				e.setMessage(MESSAGE.Com_Err_FaildDeleteReference);
				throw e;
			}
		}
	}

	/**
	 * Search Expense report type with expense type attached
	 * @param searchFilter ExpeneReportTypeRepository SearchFilter
	 * @param usedIn REPORT or REQUEST
	 * @param targetDate Used to get EmployeeGroup in the given time; when null is set to today
	 * @param startDate Accounting Period start date
	 * @param endDate Accounting Period end date
	 * @param withExpTypeName Include Expense Type Name in report type search
	 * @param employeeId  Employee Base Id, null is considered as do not filter Report Type Based on EmployeeGroup
	 * @return ExpReportTypeWithExpTypeLink List of ExpenseReportType with Expense Type Detail
	 */
	public List<ExpReportTypeWithExpTypeLink> searchExpReportTypeWithExpType(ExpReportTypeRepository.SearchFilter searchFilter,
			String usedIn, AppDate targetDate, AppDate startDate, AppDate endDate, Boolean withExpTypeName, Id employeeId) {

		ExpService.CallType callType;
		List<Id> filteredReportIdByGroup;

		Boolean originalTargetDateNull = false;
		if (targetDate == null) {
			originalTargetDateNull = true;
 			// The cases where employee belong to two employee group within an accounting period case will be handled later
			targetDate = startDate == null ? AppDate.today() : startDate;
		}

		callType = ExpService.CALL_TYPE_MAP.get(usedIn);
		if (employeeId != null) {
			// When called from Admin Page & FA page, the 'employeeId' will be null, and available Report Types are not filtered by Employee Group
			filteredReportIdByGroup = getAvailableReportTypeInUserEmpGroup(targetDate, false, employeeId);
			if (filteredReportIdByGroup != null) { // TODO Remove it When handling EXP-2385 Handling Corner Cases @ZinZin
				searchFilter.ids = new Set<Id>(filteredReportIdByGroup);
			}
		}
		// perform search
		List<ExpReportTypeEntity> entityList = (new ExpReportTypeRepository()).searchEntity(searchFilter);

		if (callType != null && filteredReportIdByGroup != null) {
			entityList = sortReportTypeList(entityList, filteredReportIdByGroup);
		}


		// Filter out the Extended Item from ExpReportType if usedIn is specified as `Request`
		if (callType == ExpService.CallType.REQUEST) {
			filterOutUsedInReportOnlyExtendedItems(entityList);
		}

		setExtendedItemInfo(entityList, callType);
		// targetDate is set to today date for employee gorup retrieving but for report type if targetdate is not specified return all the expense type avaliable
		if (!originalTargetDateNull) {
			targetDate = startDate != null ? null : targetDate;
		} else {
			targetDate = null;
		}

		return getExpReportTypeWithExpTypeLink(entityList, targetDate, startDate, endDate, withExpTypeName);
	}

	/*
	 * Get the Expense Report Type by ID
	 * @param reportTypeId ID of the Report Type to retrieve
	 * @param targetDate
	 * @param isFilteredByEmployeeGroup To indicate to filter Report Type by employee group
	 */
	public ExpReportTypeEntity getExpReportTypeById(Id reportTypeId, AppDate targetDate, Boolean isFilteredByEmployeeGroup) {
		if (isFilteredByEmployeeGroup == null) {
			isFilteredByEmployeeGroup = true;
		}
		ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();
		if (isFilteredByEmployeeGroup) {
			List<Id> filteredReportIdByGroup = getAvailableReportTypeInUserEmpGroup(targetDate, false, null);
			if (!filteredReportIdByGroup.isEmpty()) {
				Set<Id> avaliableReportTypeFromEmpGroup = new Set<Id>(filteredReportIdByGroup);
				if (avaliableReportTypeFromEmpGroup.contains(reportTypeId)) { // Search only if the reportTypeId is belong to current Employee Group
					filter.ids = new Set<Id>{reportTypeId};
				} else {
					throw new App.RecordNotFoundException(App.ERR_CODE_NOT_FOUND_REPORT_TYPE, MESSAGE.Exp_Msg_CannotUseExpenseReportType);
				}
			} else {
				return null;
			}
		} else {
			filter.ids = new Set<Id>{reportTypeId};
		}

		// perform search
		List<ExpReportTypeEntity> entityList = (new ExpReportTypeRepository()).searchEntity(filter);
		setExtendedItemInfo(entityList, null);
		// Set Expense Report Type Expense Type Link
		getExpReportTypeWithExpTypeLink(entityList, targetDate, null , null, false);
		// Check Request need ExpReportType or not with exp type range filtering (Will be in supporting expense date range for PC ticket)
		return entityList.isEmpty() ? null : entityList.get(0);
	}

	/*
	 * Remove the Extended Items which can only be used in Reports from the Entity
	 * @param expReportTypeList List of Report Types
	 */
	public void filterOutUsedInReportOnlyExtendedItems(List<ExpReportTypeEntity> expReportTypeList) {
		for (ExpReportTypeEntity expReportType : expReportTypeList) {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				if (expReportType.getExtendedItemTextUsedIn(i) == ComExtendedItemUsedIn.EXPENSE_REPORT) {
					expReportType.setExtendedItemTextUsedIn(i, null);
					expReportType.setExtendedItemTextRequiredFor(i, null);
					expReportType.setExtendedItemTextId(i, null);
				}

				if (expReportType.getExtendedItemPicklistUsedIn(i) == ComExtendedItemUsedIn.EXPENSE_REPORT) {
					expReportType.setExtendedItemPicklistUsedIn(i, null);
					expReportType.setExtendedItemPicklistRequiredFor(i, null);
					expReportType.setExtendedItemPicklistId(i, null);
				}

				if (expReportType.getExtendedItemLookupUsedIn(i) == ComExtendedItemUsedIn.EXPENSE_REPORT) {
					expReportType.setExtendedItemLookupUsedIn(i, null);
					expReportType.setExtendedItemLookupRequiredFor(i, null);
					expReportType.setExtendedItemLookupId(i, null);
				}

				if (expReportType.getExtendedItemDateUsedIn(i) == ComExtendedItemUsedIn.EXPENSE_REPORT) {
					expReportType.setExtendedItemDateUsedIn(i, null);
					expReportType.setExtendedItemDateRequiredFor(i, null);
					expReportType.setExtendedItemDateId(i, null);
				}
			}
		}
	}

	/**
	 * 拡張項目情報を取得し、費目エンティティに設定する Get Extended Item info and set to ExpTypeEntity
	 * @param expTypeList 費目エンティティ List of ExpTypeEntity
	 * @param callType if NOT null, then based on the type, the ExtendedItemInformation isRequired field will be set accordingly
	 */
	public void setExtendedItemInfo(List<ExpReportTypeEntity> expReportTypeList, ExpService.CallType callType) {
		Set<Id> extendedItemIdSet = new Set<Id>();

		for (ExpReportTypeEntity expReportType : expReportTypeList) {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				extendedItemIdSet.add(expReportType.getExtendedItemTextId(i));
				extendedItemIdSet.add(expReportType.getExtendedItemPicklistId(i));
				extendedItemIdSet.add(expReportType.getExtendedItemLookupId(i));
				extendedItemIdSet.add(expReportType.getExtendedItemDateId(i));
			}
		}

		Map<Id, ExtendedItemService.ExtendedItemInfo> extendedItemMap = new ExtendedItemService().getExtendedItemInfo(new List<Id>(extendedItemIdSet));

		for (ExpReportTypeEntity expReportType : expReportTypeList) {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				expReportType.setExtendedItemTextInfo(i, ExtendedItemService.shallowCloneExtendedItemInfo(extendedItemMap.get(expReportType.getExtendedItemTextId(i))));
				expReportType.setExtendedItemPicklistInfo(i, ExtendedItemService.shallowCloneExtendedItemInfo(extendedItemMap.get(expReportType.getExtendedItemPicklistId(i))));
				expReportType.setExtendedItemLookupInfo(i, ExtendedItemService.shallowCloneExtendedItemInfo(extendedItemMap.get(expReportType.getExtendedItemLookupId(i))));
				expReportType.setExtendedItemDateInfo(i, ExtendedItemService.shallowCloneExtendedItemInfo(extendedItemMap.get(expReportType.getExtendedItemDateId(i))));
			}

			if (callType != null) {
				// The ExtendedItemInfo is a logical representation of data and isn't stored.
				// So depending on the use case (as of now, it's only used in FE), it may not be necessary to process this information.
				for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
					setRequired(expReportType.getExtendedItemTextInfo(i), expReportType.getExtendedItemTextRequiredFor(i), callType);
					setRequired(expReportType.getExtendedItemPicklistInfo(i), expReportType.getExtendedItemPicklistRequiredFor(i), callType);
					setRequired(expReportType.getExtendedItemLookupInfo(i), expReportType.getExtendedItemLookupRequiredFor(i), callType);
					setRequired(expReportType.getExtendedItemDateInfo(i), expReportType.getExtendedItemDateRequiredFor(i), callType);
				}
			}
		}
	}

	/**
	 * ExpReportTypeEntity with ExpReportTypeExpTypeLinkEntity list
	 */
	public class ExpReportTypeWithExpTypeLink {
		public ExpReportTypeEntity expReportType;
		public List<Id> expTypeIdList;
		public List<ExpTypeIdName> expTypeList;

		public ExpReportTypeWithExpTypeLink(ExpReportTypeEntity entity) {
			expReportType = entity;
			expTypeIdList = new List<Id>();
			expTypeList = new List<ExpTypeIdName>();
		}

		public void addExpTypeId(Id expType) {
			expTypeIdList.add(expType);
		}
	}

	/**
	 * ExpTypeId with ExpTypeName
	 */
	public class ExpTypeIdName {
		public String expTypeId;
		public String expTypeName;

		public ExpTypeIdName(String expTypeId, String expTypeName) {
			this.expTypeId = expTypeId;
			this.expTypeName = expTypeName;
		}
	}

	/*
	 * If both Info and callType are not NULL, then set the isRequired field of the info based on the callType and requiredFor.
	 * @param info Extended Item Info
	 * @param requiredFor RequiredFor information
	 * @param callType specifies whether it's for Report or Request
	 */
	private static void setRequired(ExtendedItemService.ExtendedItemInfo info, ComExtendedItemRequiredFor requiredFor, ExpService.CallType callType) {
		if (info == null || callType == null) {
			return;
		}

		if (requiredFor == null) {
			info.isRequired = false;
		} else if (callType == ExpService.CallType.REQUEST) {
			info.isRequired = (requiredFor == ComExtendedItemRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT);
		} else {
			// If not NULL and is not REQUEST, only possible left is ComExtendedItemRequiredFor.EXPENSE_REPORT
			info.isRequired = true;
		}
	}

	/**
	 * Create or update an expense report type
	 * If the code is changed, the unique key also updated
	 * @param entity Target entity
	 * @return Id of the saved record
	 */
	private Id saveExpReportType(ExpReportTypeEntity entity) {
		// update unique key of the entity
		updateUniqKey(entity);
		// validate the entity
		validateEntity(entity);

		// check if the code is duplicated
		if (isDuplicatedCode(entity)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		// save to object
		Repository.SaveResult saveResult = (new ExpReportTypeRepository()).saveEntity(entity);
		return saveResult.details[0].id;
	}

	/**
	 * Set the update value to the entity
	 * @param toEntity The value in updateEntity will be set to this entity
	 * @param updateEntity Entity that have the update values
	 */
	private void setUpdateValueToEntity (ExpReportTypeEntity toEntity, ExpReportTypeEntity updateEntity) {
		// set only fields whose value have been changed
		for (ExpReportTypeEntity.Field field : ExpReportTypeEntity.Field.values()) {
			if (updateEntity.isChanged(field)) {
				toEntity.setFieldValue(field, updateEntity.getFieldValue(field));
			}
		}

		// Set Extended Items
		for (ExpReportTypeEntity.ExtendedItemConfigField field: ExpReportTypeEntity.ExtendedItemConfigField.values()) {
			if (updateEntity.isChanged(field)) {
				for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
					toEntity.setExtendedItemValue(field, i, updateEntity.getExtendedItemValue(field, i));
				}
			}
		}
	}

	/**
	 * Update unique key of the entity(Do not update to Object)
	 * @param entity Target entity(code and companyId must be set)
	 */
	private void updateUniqKey(ExpReportTypeEntity entity) {
		// get company record
		CompanyEntity company = getCompany(entity.companyId);
		if (company == null) {
			// the company cannot be found
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		entity.uniqKey = entity.createUniqKey(company.code);
	}

	/**
	 * Check if the code is duplicated in the company
	 * @param entity Target entity(code and companyId must be set)
	 * @return Returns true if the same code already exists in the compnay. otherwise, returns false.
	 */
	private Boolean isDuplicatedCode(ExpReportTypeEntity entity) {
		ExpReportTypeEntity codeEntity = (new ExpReportTypeRepository()).getEntityByCode(entity.code, entity.companyId);

		Boolean isDuplicated;
		if ((codeEntity != null) && (codeEntity.id != entity.id)) {
			return true;
		}
		return false;
	}

	/**
	 * Validate the entity
	 * Throws exception if the entity has invalid value
	 * @param entity Target entity
	 */
	private void validateEntity(ExpReportTypeEntity entity) {
		Validator.Result result = (new ExpConfigValidator.ExpReportTypeValidator(entity)).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * 拡張項目の設定が変更されているかチェックする Check if extended item setting is changed
	 * @param entity 変更前のエンティティ Entity before changed
	 * @param updateEntity 変更後のエンティティ Entity after changed
	 * @return 変更されている場合はtrue、変更されていない場合はfalse If changed return true, otherwise return false
	 */
	private Boolean isChangedExtendedItem(ExpReportTypeEntity entity, ExpReportTypeEntity updateEntity) {
		for (ExpReportTypeEntity.ExtendedItemConfigField field: ExpReportTypeEntity.ExtendedItemConfigField.values()) {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				if (updateEntity.getExtendedItemValue(field, i) != entity.getExtendedItemValue(field, i)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Check if the Cost Center setting is changed
	 * @param entity Entity before changed
	 * @param updateEntity Entity after changed
	 * @return If changed return true, otherwise return false
	 */
	private Boolean isChangedCostCenterUsed(ExpReportTypeEntity entity, ExpReportTypeEntity updateEntity) {
		return (updateEntity.costCenterUsedIn != entity.costCenterUsedIn || updateEntity.costCenterRequiredFor != entity.costCenterRequiredFor);
	}

	/**
	 * Check if the Job setting is changed
	 * @param entity Entity before changed
	 * @param updateEntity Entity after changed
	 * @return If changed return true, otherwise return false
	 */
	private Boolean isChangedJobUsed(ExpReportTypeEntity entity, ExpReportTypeEntity updateEntity) {
		return (updateEntity.jobUsedIn != entity.jobUsedIn || updateEntity.jobRequiredFor != entity.jobRequiredFor);
	}

	/**
	 * Check if the vendor setting is changed
	 * @param entity Entity before changed
	 * @param updateEntity Entity after changed
	 * @return If changed return true, otherwise return false
	 */
	private Boolean isChangedVendorUsed(ExpReportTypeEntity entity, ExpReportTypeEntity updateEntity) {
		return (updateEntity.vendorUsedIn != entity.vendorUsedIn || updateEntity.vendorRequiredFor != entity.vendorRequiredFor);
	}

	/**
	 * Check if the useFileAttachment is changed
	 * @param entity Entity before update
	 * @param updateEntity Entity to update
	 * @return If changed return true, otherwise return false
	 */
	private Boolean isChangedUseFileAttachment(ExpReportTypeEntity entity, ExpReportTypeEntity updateEntity) {
		return (updateEntity.useFileAttachment != entity.useFileAttachment);
	}

	/**
	 * 関連するトランザクションレコードが存在するかチェックする Check if related transaction records exist
	 * @param expReportTypeId チェック対象の申請タイプID report type ID
	 * @return 関連するレコードが存在する場合はtrue、存在しない場合はfalse If exists return true, otherwise return false
	 */
	private Boolean existsRelatedTransactionRecords(Id expReportTypeId) {

		// 対象の申請タイプが紐づいている経費精算を検索 / search for expense report item whose expense report type is associated
		List<ExpReportEntity> reportList = new ExpReportRepository().getEntityListByExpReportTypeId(expReportTypeId);

		// 経費精算が存在する場合はtrue If related records exist, return true
		if (reportList != null && !reportList.isEmpty()) {
			return true;
		}

		// 対象の申請タイプが紐づいている事前申請を検索 / search for expense request item whose expense report type is associated
		List<ExpRequestEntity> requestList = new ExpRequestRepository().getEntityListByExpReportTypeId(expReportTypeId);

		// 事前申請が存在する場合はtrue If related records exist, return true
		if (requestList != null && !requestList.isEmpty()) {
			return true;
		}

		return false;
	}

	/** Cache data of company */
	private Map<Id, CompanyEntity> companyCache = new Map<Id, CompanyEntity>();
	/**
	 * Get company entity
	 * @param companyId Id of the target company
	 * @return Company entity
	 * @throws App.RecordNotFoundException Throws if the company can not be found
	 */
	private CompanyEntity getCompany(Id companyId) {
		CompanyEntity company = companyCache.get(companyId);
		if (company == null) {
			company = new CompanyRepository().getEntity(companyId);
			if (company == null) {
				// the company can not be found
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
			}
			companyCache.put(companyId, company);
		}
		return company;
	}

	/*
	 * Save Expense Report Type and Expense Type Link
	 * @param reportTypeId Report Type Id
	 * @param expTypeIdList Expense Type Id list
	 */
	private void saveExpReportTypeExpTypeLink(Id reportTypeId, List<Id> expTypeIdList) {
		// Create List of Report Type and Expense Type Link Entity
		List<ExpReportTypeExpTypeLinkEntity> entityList = new List<ExpReportTypeExpTypeLinkEntity>();
		for (Id expTypeId : expTypeIdList) {
			ExpReportTypeExpTypeLinkEntity entity = new ExpReportTypeExpTypeLinkEntity();
			entity.expReportTypeId = reportTypeId;
			entity.expTypeId = expTypeId;

			entityList.add(entity);
		}
		if (!new ExpReportTypeExpTypeLinkRepository().saveEntityList(entityList).isSuccessAll) {
			throw new App.IllegalStateException('EXPTYPELINKERROR', MESSAGE.Com_Err_NoObjectInsert(new List<String>{MESSAGE.Exp_Lbl_ExpenseReportTypeExpenseTypeLink}));
		}
	}

	/**
	 * Set Expense Type Ids for Search Return
	 * @param entityList Expense Report Type Entity List
	 * @param targetDate Used to get EmployeeGroup in the given time; when null is set to today date
	 * @param startDate Accounting Period start Date
	 * @param endDate Accounting Period end Date
	 * @param withExpTypeName Include Expense Type name in Report Type search
	 * @return Expense Report Type With Linked Expense Type Id List
	 */
	private List<ExpReportTypeWithExpTypeLink> getExpReportTypeWithExpTypeLink(List<ExpReportTypeEntity> entityList, AppDate targetDate, AppDate startDate, AppDate endDate, Boolean withExpTypeName) {
		Boolean getExpTypeName = withExpTypeName != null ? withExpTypeName : false;
		// Return Expense Report Type With Expense Type Link Object
		List<ExpReportTypeWithExpTypeLink> expReportTypeWithExpTypeEntityList = new List<ExpReportTypeWithExpTypeLink>();

		// Link Search Filter
		ExpReportTypeExpTypeLinkRepository.SearchFilter searchFilter = new ExpReportTypeExpTypeLinkRepository.SearchFilter();
		searchFilter.endDate = endDate;
		searchFilter.expReportTypeIds = new Set<Id>();
		searchFilter.startDate = startDate;
		searchFilter.targetDate = targetDate;

		// Link Search Repo
		ExpReportTypeExpTypeLinkRepository repo = new ExpReportTypeExpTypeLinkRepository();

		// Add Ids in link search filter
		for (ExpReportTypeEntity entity : entityList) {
			searchFilter.expReportTypeIds.add(entity.id);
		}

		// Search link Entity
		Set<ExpReportTypeExpTypeLinkRepository.SelectDetail> selectDetail = getExpTypeName ?
				new Set<ExpReportTypeExpTypeLinkRepository.SelectDetail>{ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_TYPE_FIELD_NAME_LIST}
				: new Set<ExpReportTypeExpTypeLinkRepository.SelectDetail>{ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_REPORT_TYPE_FIELD_NAME_LIST};
		ExpReportTypeExpTypeLinkRepository.ExpReportTypeExpTypeOrderBy orderBy = getExpTypeName ?
				ExpReportTypeExpTypeLinkRepository.ExpReportTypeExpTypeOrderBy.EXP_TYPE_NAME : null;
		List<ExpReportTypeExpTypeLinkEntity> linkEntityList = repo.searchEntityList(searchFilter, selectDetail, orderBy);

		Map<Id, List<ExpTypeIdName>> linkExpTypeIdNameMap;
		Map<Id, List<Id>> linkEntityMap;

		if (getExpTypeName) {
			// Group Expense Type Id and name by Report Type in a Map
			linkExpTypeIdNameMap = new Map<Id, List<ExpTypeIdName>>();
			for (ExpReportTypeExpTypeLinkEntity entity : linkEntityList) {
				Id reportId = entity.expReportTypeId;

				ExpTypeIdName tempIdName = new ExpTypeIdName(entity.expTypeId, entity.expTypeNameL.getValue());
				if (!linkExpTypeIdNameMap.containsKey(reportId)) {
					linkExpTypeIdNameMap.put(reportId, new List<ExpTypeIdName>{tempIdName});
				} else {
					linkExpTypeIdNameMap.get(reportId).add(tempIdName);
				}

			}
		} else {
			// Group Expense Type Id by Report Type in a Map
			linkEntityMap = new Map<Id, List<Id>>();
			for (ExpReportTypeExpTypeLinkEntity entity : linkEntityList) {
				Id reportId = entity.expReportTypeId;
				if (!linkEntityMap.containsKey(reportId)) {
					linkEntityMap.put(reportId, new List<Id>{entity.expTypeId});
				} else {
					linkEntityMap.get(reportId).add(entity.expTypeId);
				}

			}
		}
		// Create Expense Report Type linked with Expense Type Id Object for search return
		for (ExpReportTypeEntity entity : entityList) {
			ExpReportTypeWithExpTypeLink linkedObject = new ExpReportTypeWithExpTypeLink(entity);
			if (getExpTypeName) {
				linkedObject.expTypeList = linkExpTypeIdNameMap.get(entity.id);
			} else {
				linkedObject.expTypeIdList = linkEntityMap.get(entity.id);
			}
			expReportTypeWithExpTypeEntityList.add(linkedObject);
		}

		return expReportTypeWithExpTypeEntityList;
	}

	/**
	 * Update Expense Report Type Expense Type Link
	 * @param reportTypeId Expense Report Type Id
	 * @param expTypeIdList Expense Type Id List linked to Expense Report Type
	 * @return true if save and delete successfully finished
	 */
	private void updateExpReportTypeExpTypeLink(Id reportTypeId, List<Id> expTypeIdList) {
		ExpReportTypeExpTypeLinkRepository linkRepo = new ExpReportTypeExpTypeLinkRepository();
		// Get current Link Entity related to report type id
		List<ExpReportTypeExpTypeLinkEntity> expTypeLinkEntityList = linkRepo.getEntityListByExpReportTypeId(new Set<Id>{reportTypeId});
		Map<Id, ExpReportTypeExpTypeLinkEntity> expReportTypeExpTypeLinkEntityMap = new Map<Id, ExpReportTypeExpTypeLinkEntity>();

		// Remove the unchanged Expense Type Id and add in removed Expense Type Link Entity to map
		for (ExpReportTypeExpTypeLinkEntity entity: expTypeLinkEntityList) {
			expReportTypeExpTypeLinkEntityMap.put(entity.expTypeId, entity);
			for (Integer i = 0; i < expTypeIdList.size(); i++) {
				Id expTypeId = expTypeIdList[i];
				if (expTypeId == entity.expTypeId) {
					expReportTypeExpTypeLinkEntityMap.remove(expTypeId);
					expTypeIdList.remove(i);
					break;
				}
			}
		}

		// Add Newly Added Expense Type to Report

		if (expTypeIdList != null && !expTypeIdList.isEmpty()) {
			saveExpReportTypeExpTypeLink(reportTypeId, expTypeIdList);
		}

		// Delete Removed Expense Type
		Boolean isDeleteSuccessful = true;
		if (!expReportTypeExpTypeLinkEntityMap.isEmpty()) {
			isDeleteSuccessful = linkRepo.deleteEntityList(expReportTypeExpTypeLinkEntityMap.values()).isSuccessAll;
		}

		if (!isDeleteSuccessful) {
			throw new App.IllegalStateException(APP.ERR_CODE_EXP_TYPE_LINK, MESSAGE.Com_Err_FaildDeleteReference(new List<String>{MESSAGE.Exp_Lbl_ExpenseReportTypeExpenseTypeLink}));
		}
	}

	/**
	 * Get Available Report Type Id Based on Current Employee Group
	 *
	 * @param targetDate Target Date for the Employee
	 * @param onlyActiveReportType specify whether to Filter Out non-Active Report Types
	 * @param employeeId Employee Base Id, when employeeId is not present is considered as current login user
	 * @return List of Available Report Type Id filtered by employee group
	 */
	public List<Id> getAvailableReportTypeInUserEmpGroup(AppDate targetDate, Boolean onlyActiveReportType, Id employeeId) {
		return this.getAvailableReportTypeInUserEmpGroup(targetDate, onlyActiveReportType, false, employeeId);
	}

	/**
	 * Get Available Report Type Id Based on Current Employee Group
	 *
	 * @param targetDate Target Date for the Employee
	 * @param onlyActiveReportType specify whether to Filter Out non-Active Report Types
	 * @param reportTypesWithoutVendorOnly specify whether to filter out Report Types with Vendor Used In value is NOT null
	 * @param employeeId Employee Base Id, when employeeId is not present is considered as current login user
	 * @return List of Available Report Type Id filtered by employee group
	 */
	public List<Id> getAvailableReportTypeInUserEmpGroup(AppDate targetDate, Boolean onlyActiveReportType, Boolean reportTypesWithoutVendorOnly, Id employeeId) {
		List<Id> reportTypeIdList = null;
		EmployeeBaseEntity empBase;
		if (employeeId != null) {
			empBase = ExpCommonUtil.getEmployeeBaseEntity(employeeId);
		} else {
			empBase = ExpCommonUtil.getEmployeeByUserId(UserInfo.getUserId(), targetDate);
		}

		// Target Date is defined and first employee history is the valid history
		EmployeeHistoryEntity empHistory = empBase.getHistory(0);
		if (empHistory != null && empHistory.expEmployeeGroupId != null) {
			reportTypeIdList = new List<Id>();

			ExpEmpGroupExpReportTypeLinkRepository.SearchFilter filter = new ExpEmpGroupExpReportTypeLinkRepository.SearchFilter();
			filter.empGroupIdSet = new Set<Id>{ empHistory.expEmployeeGroupId };
			if (onlyActiveReportType == true) {
				filter.reportTypeActive = true;
			}
			if (reportTypesWithoutVendorOnly == true) {
				filter.reportTypeWithoutVendorOnly = true;
			}
			List<ExpEmpGroupExpReportTypeLinkEntity> expEmpGroupExpReportTypeLinkList = new ExpEmpGroupExpReportTypeLinkRepository().searchEntityList(filter);
			if (!expEmpGroupExpReportTypeLinkList.isEmpty()) {
				for (ExpEmpGroupExpReportTypeLinkEntity entity : expEmpGroupExpReportTypeLinkList) {
					reportTypeIdList.add(entity.expReportTypeId);
				}
			} else {
				// TODO: EXP-2385 Handling Corner Cases @ZinZin
				// throw error saying no report type is here
			}
		} else {
			if (empHistory == null) {
				throw new App.RecordNotFoundException(APP.ERR_CODE_NOT_FOUND_EMP, MESSAGE.Com_Err_SpecifiedNotFound);
			}
			if (empHistory.expEmployeeGroupId == null) {
				throw new App.NoPermissionException(APP.ERR_CODE_CANNOT_USE_EXPENSE,
						MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Exp_Lbl_EmployeeGroup}));
			}
		}

		// TODO :EXP-2385 Handling Corner Cases @ZinZin
		// not only to check in here when the returned report type are not active also throw error
		return reportTypeIdList;
	}

	/**
	 * Sort Report Type According to the Employee Group pre-defined report type order
	 * @param entityList List of ExpReportTypeEntity to sort
	 * @param filteredReportIdByGroup Order of the report type id to sort
	 * @return Sorted ExpReportTypeEntity list
	 */
	private List<ExpReportTypeEntity> sortReportTypeList(List<ExpReportTypeEntity> entityList, List<Id> filteredReportIdByGroup) {
		List<ExpReportTypeEntity> sortedEntity = new List<ExpReportTypeEntity>();
		Map<Id,ExpReportTypeEntity> entityMap = new Map<Id, ExpReportTypeEntity>();
		// EntityList to Map
		for (ExpReportTypeEntity entity : entityList) {
			entityMap.put(entity.id, entity);
		}
		// Loop filteredReportId
		for (Id reportId : filteredReportIdByGroup) {
			if (entityMap.containsKey(reportId)) { //Checked this not to add null value in list
				sortedEntity.add(entityMap.get(reportId));
			}
		}
		return sortedEntity;
	}

}