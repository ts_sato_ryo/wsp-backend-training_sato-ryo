/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 取消種別
 */
public with sharing class AppCancelType extends ValueObjectType {

	/** なし */
	public static final AppCancelType NONE = new AppCancelType('None');
	/** 却下 */
	public static final AppCancelType REJECTED = new AppCancelType('Rejected');
	/** 申請取消 */
	public static final AppCancelType REMOVED = new AppCancelType('Removed');
	/** 承認取消 */
	public static final AppCancelType CANCELED = new AppCancelType('Canceled');
	/** 変更承認済み */
	public static final AppCancelType REAPPLIED = new AppCancelType('Reapplied');

	/** 取消種別のリスト（種別が追加されら本リストにも追加してください） */
	public static final List<AppCancelType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AppCancelType> {
			NONE,
			REJECTED,
			REMOVED,
			CANCELED,
			REAPPLIED
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AppCancelType(String value) {
		super(value);
	}

	/**
	 * 値からAppCancelTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAppCancelType、ただし該当するAppCancelTypeが存在しない場合はnull
	 */
	public static AppCancelType valueOf(String value) {
		AppCancelType retType = null;
		if (String.isNotBlank(value)) {
			for (AppCancelType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AppCancelType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AppCancelType) {
			// 値が同じであればtrue
			if (this.value == ((AppCancelType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}