/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 工数
 *
 * TimeRecordItemSnapshotRepositoryのテスト
 */
@isTest
private class TimeRecordItemSnapshotRepositoryTest {

	private static TimeRecordItemSnapshotRepository repository = new TimeRecordItemSnapshotRepository();

	private class TestData {

		/** 共通の工数テストデータ */
		private TimeTestData testData;

		/** スナップショットを作成する対象の工数明細 */
		private TimeRecord__c timeRecord;

		/**
		 * コンストラクタ
		 * @param targetDate マスタの有効開始日
		 */
		private TestData(AppDate targetDate) {
			testData = new TimeTestData(targetDate);
			TimeSummaryEntity summary = testData.createSummaryEntity();
			timeRecord = createTimeRecord(summary)[0];
		}

		/**
		 * 工数サマリーに紐づく工数明細を作成する
		 * @param summary 工数サマリー
		 * @return 工数内訳のリスト
		 * TODO TimeTestDataに移行する
		 */
		private List<TimeRecord__c> createTimeRecord(TimeSummaryEntity summary) {
			List<TimeRecord__c> records = new List<TimeRecord__c>();
			for (AppDate targetDate : new AppDateRange(summary.startDate, summary.endDate).values()) {
				records.add(new TimeRecord__c(
					UniqKey__c = summary.Id + targetDate.format(),
					TimeSummaryId__c = summary.Id,
					Date__c = targetDate.getDate(),
					Note__c = 'Note',
					Output__c = 'Output'
				));
			}
			insert records;
			return records;
		}

		/**
		 * 工数内訳を作成する
		 * @param size 作成する件数
		 * @return レコードのリスト
		 */
		private List<TimeRecordItem__c> createTimeRecordItem(Integer size) {
			List<TimeRecordItem__c> items = new List<TimeRecordItem__c>();
			for (Integer i = 0; i < size; i++) {
				items.add(new TimeRecordItem__c(
					Name = 'RecordItem_' + String.valueOf(i),
					TimeRecordId__c = timeRecord.Id,
					JobId__c = testData.jobList[0].Id,
					Time__c = i,
					TaskNote__c = String.valueOf(i)
				));
			}
			insert items;
			return items;
		}

		/**
		 * 工数内訳スナップショットレコードを作成する。
		 * 内部的には主従関係となる工数内訳も作成する。
		 * 作成するスナップショットには必須項目以外は設定されていないので、テストに必要な情報は呼び出し元で設定すること。
		 * 
		 * @param size 作成する件数
		 * @return レコードのリスト
		 */
		private List<TimeRecordItemSnapshot__c> createSnapshot(Integer size) {
			// 工数内訳を作成
			List<TimeRecordItem__c> items = createTimeRecordItem(size);

			// 工数内訳スナップショットを作成
			List<TimeRecordItemSnapshot__c> snapshots = new List<TimeRecordItemSnapshot__c>();
			for (TimeRecordItem__c item : items) {
				snapshots.add(new TimeRecordItemSnapshot__c(
					Name = 'RecordItemSnapshot_' + item.TaskNote__c,
					TimeRecordItemId__c = item.Id
				));
			}
			insert snapshots;
			return snapshots;
		}

		/**
		 * 指定した件数のジョブを作成する
		 * @param size レコード件数
		 * @return 作成したジョブ（DB保存済み）
		 */
		private List<ComJob__c> createJobs(Integer size) {
			return ComTestDataUtility.createJobs('Job', testData.company.Id, testData.jobType.Id, size);
		}

		/**
		 * 指定した件数の作業分類を作成する
		 * @param size レコード件数
		 * @return 作成した作業分類（DB保存済み）
		 */
		private List<TimeWorkCategory__c> createWorkCategories(Integer size) {
			return ComTestDataUtility.createWorkCategories('WorkCategory', testData.company.Id, size);
		}
	}

	/**
	 * 検索条件を指定しない場合、全てのレコードを取得できる事を検証する
	 */
	@isTest
	static void searchAllTest() {
		// データ作成
		TestData testData = new TestData(AppDate.newInstance(2019, 4, 1));
		List<TimeRecordItemSnapshot__c> snapshots = testData.createSnapshot(3);

		// 検索
		TimeRecordItemSnapshotRepository.SearchFilter filter = new TimeRecordItemSnapshotRepository.SearchFilter();
		List<TimeRecordItemSnapshotEntity> entities = repository.searchEntityList(filter);

		// 検証
		System.assertEquals(3, entities.size());
		
		// IDを比較する
		List<Id> expected = new List<Id>{snapshots[0].Id, snapshots[1].Id, snapshots[2].Id};
		expected.sort();
		List<Id> actual = new List<Id>{entities[0].id, entities[1].id, entities[2].id};
		actual.sort();
		System.assertEquals(expected, actual);
	}

	/**
	 * 工数内訳IDを条件に検索できる事を検証する
	 */
	@isTest
	static void searchByRecordItemIdTest() {
		// データ作成
		TestData testData = new TestData(AppDate.newInstance(2019, 4, 1));
		List<TimeRecordItemSnapshot__c> snapshots = testData.createSnapshot(5);

		// 検索
		TimeRecordItemSnapshotRepository.SearchFilter filter = new TimeRecordItemSnapshotRepository.SearchFilter();
		filter.recordItemIds = new List<Id>();
		filter.recordItemIds.add(snapshots[0].TimeRecordItemId__c);
		filter.recordItemIds.add(snapshots[4].TimeRecordItemId__c);
		List<TimeRecordItemSnapshotEntity> entities = repository.searchEntityList(filter);

		// 検証
		System.assertEquals(2, entities.size());
		
		// IDを比較する
		List<Id> expected = new List<Id>{snapshots[0].Id, snapshots[4].Id};
		expected.sort();
		List<Id> actual = new List<Id>{entities[0].id, entities[1].id};
		actual.sort();
		System.assertEquals(expected, actual);
	}

	/*
	 * 工数明細IDを条件に検索できる事を検証する
	 */
	@isTest
	static void searchByRecordIdTest() {
		// データ作成
		TestData testData = new TestData(AppDate.newInstance(2019, 4, 1));
		List<TimeRecordItemSnapshot__c> snapshots = testData.createSnapshot(3);

		// 検索
		TimeRecordItemSnapshotRepository.SearchFilter filter = new TimeRecordItemSnapshotRepository.SearchFilter();
		filter.recordIds = new List<Id>{testData.timeRecord.Id};
		List<TimeRecordItemSnapshotEntity> entities = repository.searchEntityList(filter);

		// 検証
		System.assertEquals(3, entities.size());
	}
	
	/*
	 * 検索条件の工数明細IDに一致しない場合、結果を返さない事を検証する
	 */
	@isTest
	static void searchByRecordIdNoResultTest() {
		// データ作成
		TestData testData = new TestData(AppDate.newInstance(2019, 4, 1));
		List<TimeRecordItemSnapshot__c> snapshots = testData.createSnapshot(3);

		// 検索
		TimeRecordItemSnapshotRepository.SearchFilter filter = new TimeRecordItemSnapshotRepository.SearchFilter();
		filter.recordIds = new List<Id>{snapshots[0].Id};
		List<TimeRecordItemSnapshotEntity> entities = repository.searchEntityList(filter);

		// 検証
		System.assertEquals(0, entities.size());
	}
	
	/**
	 * エンティティから参照する項目を全て取得している事を検証する
	 */
	@isTest
	static void selectAllColumnTest() {
		// データ作成
		TestData testData = new TestData(AppDate.newInstance(2019, 4, 1));
		TimeRecordItemSnapshot__c snapshot = testData.createSnapshot(1)[0];

		// 検索
		TimeRecordItemSnapshotRepository.SearchFilter filter = new TimeRecordItemSnapshotRepository.SearchFilter();
		TimeRecordItemSnapshotEntity entity = repository.searchEntityList(filter)[0];
		
		// 全ての項目を参照する（取得できていない場合、SObjectExceptionが発生する）
		Id id = entity.id;
		String name = entity.name;
		Id timeRecordItemId = entity.timeRecordItemId;
		Id jobIdLevel1 = entity.jobIdLevel1;
		String jobCodeLevel1 = entity.jobCodeLevel1;
		String jobNameLevel1L0 = entity.jobNameLevel1L0;
		String jobNameLevel1L1 = entity.jobNameLevel1L1;
		String jobNameLevel1L2 = entity.jobNameLevel1L2;
		Id jobIdLevel2 = entity.jobIdLevel2;
		String jobCodeLevel2 = entity.jobCodeLevel2;
		String jobNameLevel2L0 = entity.jobNameLevel2L0;
		String jobNameLevel2L1 = entity.jobNameLevel2L1;
		String jobNameLevel2L2 = entity.jobNameLevel2L2;
		Id jobIdLevel3 = entity.jobIdLevel3;
		String jobCodeLevel3 = entity.jobCodeLevel3;
		String jobNameLevel3L0 = entity.jobNameLevel3L0;
		String jobNameLevel3L1 = entity.jobNameLevel3L1;
		String jobNameLevel3L2 = entity.jobNameLevel3L2;
		Id jobIdLevel4 = entity.jobIdLevel4;
		String jobCodeLevel4 = entity.jobCodeLevel4;
		String jobNameLevel4L0 = entity.jobNameLevel4L0;
		String jobNameLevel4L1 = entity.jobNameLevel4L1;
		String jobNameLevel4L2 = entity.jobNameLevel4L2;
		Id jobIdLevel5 = entity.jobIdLevel5;
		String jobCodeLevel5 = entity.jobCodeLevel5;
		String jobNameLevel5L0 = entity.jobNameLevel5L0;
		String jobNameLevel5L1 = entity.jobNameLevel5L1;
		String jobNameLevel5L2 = entity.jobNameLevel5L2;
		Id jobIdLevel6 = entity.jobIdLevel6;
		String jobCodeLevel6 = entity.jobCodeLevel6;
		String jobNameLevel6L0 = entity.jobNameLevel6L0;
		String jobNameLevel6L1 = entity.jobNameLevel6L1;
		String jobNameLevel6L2 = entity.jobNameLevel6L2;
		Id jobIdLevel7 = entity.jobIdLevel7;
		String jobCodeLevel7 = entity.jobCodeLevel7;
		String jobNameLevel7L0 = entity.jobNameLevel7L0;
		String jobNameLevel7L1 = entity.jobNameLevel7L1;
		String jobNameLevel7L2 = entity.jobNameLevel7L2;
		Id jobIdLevel8 = entity.jobIdLevel8;
		String jobCodeLevel8 = entity.jobCodeLevel8;
		String jobNameLevel8L0 = entity.jobNameLevel8L0;
		String jobNameLevel8L1 = entity.jobNameLevel8L1;
		String jobNameLevel8L2 = entity.jobNameLevel8L2;
		Id jobIdLevel9 = entity.jobIdLevel9;
		String jobCodeLevel9 = entity.jobCodeLevel9;
		String jobNameLevel9L0 = entity.jobNameLevel9L0;
		String jobNameLevel9L1 = entity.jobNameLevel9L1;
		String jobNameLevel9L2 = entity.jobNameLevel9L2;
		Id jobIdLevel10 = entity.jobIdLevel10;
		String jobCodeLevel10 = entity.jobCodeLevel1;
		String jobNameLevel10L0 = entity.jobNameLevel10L0;
		String jobNameLevel10L1 = entity.jobNameLevel10L1;
		String jobNameLevel10L2 = entity.jobNameLevel10L2;
		Id workCategoryId = entity.workCategoryId;
		String workCategoryCode = entity.workCategoryCode;
		String workCategoryNameL0 = entity.workCategoryNameL0;
		String workCategoryNameL1 = entity.workCategoryNameL1;
		String workCategoryNameL2 = entity.workCategoryNameL2;
	}

	/**
	 * エンティティの値を正しく保存出来る事を検証する
	 */
	@isTest
	static void saveEntityTest() {
		// テストデータを作成
		TestData testData = new TestData(AppDate.newInstance(2019, 4, 1));
		List<ComJob__c> jobs = testData.createJobs(10);
		TimeWorkCategory__c workCategory = testData.createWorkCategories(3)[1];
		TimeRecordItem__c recordItem = testData.createTimeRecordItem(1)[0];

		// エンティティを作成
		TimeRecordItemSnapshotEntity entity = new TimeRecordItemSnapshotEntity();
		entity.name = 'SnapshotName';
		entity.timeRecordItemId = recordItem.Id;
		entity.jobIdLevel1 = jobs[1].Id;
		entity.jobCodeLevel1 = jobs[1].Code__c;
		entity.jobNameLevel1L0 = jobs[1].Name_L0__c;
		entity.jobNameLevel1L1 = jobs[1].Name_L1__c;
		entity.jobNameLevel1L2 = jobs[1].Name_L2__c;
		entity.jobIdLevel2 = jobs[2].Id;
		entity.jobCodeLevel2 = jobs[2].Code__c;
		entity.jobNameLevel2L0 = jobs[2].Name_L0__c;
		entity.jobNameLevel2L1 = jobs[2].Name_L1__c;
		entity.jobNameLevel2L2 = jobs[2].Name_L2__c;
		entity.jobIdLevel3 = jobs[3].Id;
		entity.jobCodeLevel3 = jobs[3].Code__c;
		entity.jobNameLevel3L0 = jobs[3].Name_L0__c;
		entity.jobNameLevel3L1 = jobs[3].Name_L1__c;
		entity.jobNameLevel3L2 = jobs[3].Name_L2__c;
		entity.jobIdLevel4 = jobs[4].Id;
		entity.jobCodeLevel4 = jobs[4].Code__c;
		entity.jobNameLevel4L0 = jobs[4].Name_L0__c;
		entity.jobNameLevel4L1 = jobs[4].Name_L1__c;
		entity.jobNameLevel4L2 = jobs[4].Name_L2__c;
		entity.jobIdLevel5 = jobs[5].Id;
		entity.jobCodeLevel5 = jobs[5].Code__c;
		entity.jobNameLevel5L0 = jobs[5].Name_L0__c;
		entity.jobNameLevel5L1 = jobs[5].Name_L1__c;
		entity.jobNameLevel5L2 = jobs[5].Name_L2__c;
		entity.jobIdLevel6 = jobs[6].Id;
		entity.jobCodeLevel6 = jobs[6].Code__c;
		entity.jobNameLevel6L0 = jobs[6].Name_L0__c;
		entity.jobNameLevel6L1 = jobs[6].Name_L1__c;
		entity.jobNameLevel6L2 = jobs[6].Name_L2__c;
		entity.jobIdLevel7 = jobs[7].Id;
		entity.jobCodeLevel7 = jobs[7].Code__c;
		entity.jobNameLevel7L0 = jobs[7].Name_L0__c;
		entity.jobNameLevel7L1 = jobs[7].Name_L1__c;
		entity.jobNameLevel7L2 = jobs[7].Name_L2__c;
		entity.jobIdLevel8 = jobs[8].Id;
		entity.jobCodeLevel8 = jobs[8].Code__c;
		entity.jobNameLevel8L0 = jobs[8].Name_L0__c;
		entity.jobNameLevel8L1 = jobs[8].Name_L1__c;
		entity.jobNameLevel8L2 = jobs[8].Name_L2__c;
		entity.jobIdLevel9 = jobs[9].Id;
		entity.jobCodeLevel9 = jobs[9].Code__c;
		entity.jobNameLevel9L0 = jobs[9].Name_L0__c;
		entity.jobNameLevel9L1 = jobs[9].Name_L1__c;
		entity.jobNameLevel9L2 = jobs[9].Name_L2__c;
		entity.jobIdLevel10 = jobs[0].Id;
		entity.jobCodeLevel10 = jobs[0].Code__c;
		entity.jobNameLevel10L0 = jobs[0].Name_L0__c;
		entity.jobNameLevel10L1 = jobs[0].Name_L1__c;
		entity.jobNameLevel10L2 = jobs[0].Name_L2__c;
		entity.workCategoryId = workCategory.Id;
		entity.workCategoryCode = workCategory.Code__c;
		entity.workCategoryNameL0 = workCategory.Name_L0__c;
		entity.workCategoryNameL1 = workCategory.Name_L1__c;
		entity.workCategoryNameL2 = workCategory.Name_L2__c;
		repository.saveEntity(entity);

		// 検証
		List<TimeRecordItemSnapshot__c> actual = selectSnapshots();
		System.assertEquals(1, actual.size());
		System.assertEquals(entity.name, actual[0].Name);
		System.assertEquals(recordItem.Id, actual[0].TimeRecordItemId__c);
		System.assertEquals(jobs[1].Id, actual[0].JobIdLevel1__c);
		System.assertEquals(jobs[1].Code__c, actual[0].JobCodeLevel1__c);
		System.assertEquals(jobs[1].Name_L0__c, actual[0].JobNameLevel1_L0__c);
		System.assertEquals(jobs[1].Name_L1__c, actual[0].JobNameLevel1_L1__c);
		System.assertEquals(jobs[1].Name_L2__c, actual[0].JobNameLevel1_L2__c);
		System.assertEquals(jobs[2].Id, actual[0].JobIdLevel2__c);
		System.assertEquals(jobs[2].Code__c, actual[0].JobCodeLevel2__c);
		System.assertEquals(jobs[2].Name_L0__c, actual[0].JobNameLevel2_L0__c);
		System.assertEquals(jobs[2].Name_L1__c, actual[0].JobNameLevel2_L1__c);
		System.assertEquals(jobs[2].Name_L2__c, actual[0].JobNameLevel2_L2__c);
		System.assertEquals(jobs[3].Id, actual[0].JobIdLevel3__c);
		System.assertEquals(jobs[3].Code__c, actual[0].JobCodeLevel3__c);
		System.assertEquals(jobs[3].Name_L0__c, actual[0].JobNameLevel3_L0__c);
		System.assertEquals(jobs[3].Name_L1__c, actual[0].JobNameLevel3_L1__c);
		System.assertEquals(jobs[3].Name_L2__c, actual[0].JobNameLevel3_L2__c);
		System.assertEquals(jobs[4].Id, actual[0].JobIdLevel4__c);
		System.assertEquals(jobs[4].Code__c, actual[0].JobCodeLevel4__c);
		System.assertEquals(jobs[4].Name_L0__c, actual[0].JobNameLevel4_L0__c);
		System.assertEquals(jobs[4].Name_L1__c, actual[0].JobNameLevel4_L1__c);
		System.assertEquals(jobs[4].Name_L2__c, actual[0].JobNameLevel4_L2__c);
		System.assertEquals(jobs[5].Id, actual[0].JobIdLevel5__c);
		System.assertEquals(jobs[5].Code__c, actual[0].JobCodeLevel5__c);
		System.assertEquals(jobs[5].Name_L0__c, actual[0].JobNameLevel5_L0__c);
		System.assertEquals(jobs[5].Name_L1__c, actual[0].JobNameLevel5_L1__c);
		System.assertEquals(jobs[5].Name_L2__c, actual[0].JobNameLevel5_L2__c);
		System.assertEquals(jobs[6].Id, actual[0].JobIdLevel6__c);
		System.assertEquals(jobs[6].Code__c, actual[0].JobCodeLevel6__c);
		System.assertEquals(jobs[6].Name_L0__c, actual[0].JobNameLevel6_L0__c);
		System.assertEquals(jobs[6].Name_L1__c, actual[0].JobNameLevel6_L1__c);
		System.assertEquals(jobs[6].Name_L2__c, actual[0].JobNameLevel6_L2__c);
		System.assertEquals(jobs[7].Id, actual[0].JobIdLevel7__c);
		System.assertEquals(jobs[7].Code__c, actual[0].JobCodeLevel7__c);
		System.assertEquals(jobs[7].Name_L0__c, actual[0].JobNameLevel7_L0__c);
		System.assertEquals(jobs[7].Name_L1__c, actual[0].JobNameLevel7_L1__c);
		System.assertEquals(jobs[7].Name_L2__c, actual[0].JobNameLevel7_L2__c);
		System.assertEquals(jobs[8].Id, actual[0].JobIdLevel8__c);
		System.assertEquals(jobs[8].Code__c, actual[0].JobCodeLevel8__c);
		System.assertEquals(jobs[8].Name_L0__c, actual[0].JobNameLevel8_L0__c);
		System.assertEquals(jobs[8].Name_L1__c, actual[0].JobNameLevel8_L1__c);
		System.assertEquals(jobs[8].Name_L2__c, actual[0].JobNameLevel8_L2__c);
		System.assertEquals(jobs[9].Id, actual[0].JobIdLevel9__c);
		System.assertEquals(jobs[9].Code__c, actual[0].JobCodeLevel9__c);
		System.assertEquals(jobs[9].Name_L0__c, actual[0].JobNameLevel9_L0__c);
		System.assertEquals(jobs[9].Name_L1__c, actual[0].JobNameLevel9_L1__c);
		System.assertEquals(jobs[9].Name_L2__c, actual[0].JobNameLevel9_L2__c);
		System.assertEquals(jobs[0].Id, actual[0].JobIdLevel10__c);
		System.assertEquals(jobs[0].Code__c, actual[0].JobCodeLevel10__c);
		System.assertEquals(jobs[0].Name_L0__c, actual[0].JobNameLevel10_L0__c);
		System.assertEquals(jobs[0].Name_L1__c, actual[0].JobNameLevel10_L1__c);
		System.assertEquals(jobs[0].Name_L2__c, actual[0].JobNameLevel10_L2__c);
		System.assertEquals(workCategory.Id, actual[0].WorkCategoryId__c);
		System.assertEquals(workCategory.Code__c, actual[0].WorkCategoryCode__c);
		System.assertEquals(workCategory.Name_L0__c, actual[0].WorkCategoryName_L0__c);
		System.assertEquals(workCategory.Name_L1__c, actual[0].WorkCategoryName_L1__c);
		System.assertEquals(workCategory.Name_L2__c, actual[0].WorkCategoryName_L2__c);
	}

	/**
	 * エンティティ保存後に、登録したIDがエンティティに設定されることを検証する
	 */
	@isTest
	static void saveEntityIdTest() {
		// テストデータを作成
		TestData testData = new TestData(AppDate.newInstance(2019, 4, 1));
		List<ComJob__c> jobs = testData.createJobs(10);
		TimeWorkCategory__c workCategory = testData.createWorkCategories(3)[1];
		TimeRecordItem__c recordItem = testData.createTimeRecordItem(1)[0];

		// エンティティを作成
		TimeRecordItemSnapshotEntity entity1 = new TimeRecordItemSnapshotEntity();
		entity1.timeRecordItemId = recordItem.Id;
		TimeRecordItemSnapshotEntity entity2 = new TimeRecordItemSnapshotEntity();
		entity2.timeRecordItemId = recordItem.Id;
		TimeRecordItemSnapshotEntity entity3 = new TimeRecordItemSnapshotEntity();
		entity3.timeRecordItemId = recordItem.Id;

		// 保存
		repository.saveEntityList(new List<TimeRecordItemSnapshotEntity>{entity1, entity2, entity3});

		// 検証
		List<TimeRecordItemSnapshot__c> selectSnapshots = selectSnapshots();
		List<Id> expectedIds = new List<Id>{selectSnapshots[0].Id, selectSnapshots[1].Id, selectSnapshots[2].Id};
		expectedIds.sort();
		List<Id> actualIds = new List<Id>{entity1.id, entity2.id, entity3.id};
		actualIds.sort();
		System.assertEquals(expectedIds, actualIds);
	}

	/**
	 * 工数内訳スナップショットを全件取得する
	 * @return 検索結果
	 */
	private static List<TimeRecordItemSnapshot__c> selectSnapshots() {
		return [SELECT Id, TimeRecordItemId__c, Name, 
						JobIdLevel1__c, JobCodeLevel1__c, JobNameLevel1_L0__c, JobNameLevel1_L1__c, JobNameLevel1_L2__c, 
						JobIdLevel2__c, JobCodeLevel2__c, JobNameLevel2_L0__c, JobNameLevel2_L1__c, JobNameLevel2_L2__c, 
						JobIdLevel3__c, JobCodeLevel3__c, JobNameLevel3_L0__c, JobNameLevel3_L1__c, JobNameLevel3_L2__c, 
						JobIdLevel4__c, JobCodeLevel4__c, JobNameLevel4_L0__c, JobNameLevel4_L1__c, JobNameLevel4_L2__c, 
						JobIdLevel5__c, JobCodeLevel5__c, JobNameLevel5_L0__c, JobNameLevel5_L1__c, JobNameLevel5_L2__c, 
						JobIdLevel6__c, JobCodeLevel6__c, JobNameLevel6_L0__c, JobNameLevel6_L1__c, JobNameLevel6_L2__c, 
						JobIdLevel7__c, JobCodeLevel7__c, JobNameLevel7_L0__c, JobNameLevel7_L1__c, JobNameLevel7_L2__c, 
						JobIdLevel8__c, JobCodeLevel8__c, JobNameLevel8_L0__c, JobNameLevel8_L1__c, JobNameLevel8_L2__c, 
						JobIdLevel9__c, JobCodeLevel9__c, JobNameLevel9_L0__c, JobNameLevel9_L1__c, JobNameLevel9_L2__c, 
						JobIdLevel10__c, JobCodeLevel10__c, JobNameLevel10_L0__c, JobNameLevel10_L1__c, JobNameLevel10_L2__c, 
						WorkCategoryId__c, WorkCategoryCode__c, WorkCategoryName_L0__c, WorkCategoryName_L1__c, WorkCategoryName_L2__c
				From TimeRecordItemSnapshot__c];
	}
}