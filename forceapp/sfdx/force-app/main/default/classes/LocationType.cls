/**
 * @group 共通
 *
 * 打刻種別
 */
public with sharing class LocationType extends ValueObjectType {

	// 勤怠打刻
	public static final LocationType ATT_STAMP = new LocationType('AttStamp');
	// 勤怠種別のリスト
	public static final List<LocationType> TYPE_LIST;
	static {
		TYPE_LIST = new List<LocationType> {
			ATT_STAMP
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private LocationType(String value) {
		super(value);
	}


	/**
	 * 値からLocationTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つLocationType、ただし該当するLocationTypeが存在しない場合はnull
	 */
	public static LocationType valueOf(String value) {
		LocationType retType = null;
		if (String.isNotBlank(value)) {
			for (LocationType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// LocationType以外の場合はfalse
		Boolean eq;
		if (compare instanceof LocationType) {
			// 値が同じであればtrue
			if (this.value == ((LocationType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}
