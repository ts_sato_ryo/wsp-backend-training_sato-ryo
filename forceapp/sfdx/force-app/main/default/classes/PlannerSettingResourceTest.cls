/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description PlannerSettingResourceのテストクラス
*/
@isTest
private class PlannerSettingResourceTest {

	/**
	 * 認証コード保存APIのテスト
	 * 認証コードを会社別非公開設定（カスタム設定）に保存できること、更新できることを検証する
	 */
	@isTest
	static void saveOAuthCodeApiTestSuccess() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		// API実行（保存できることを確認）
		PlannerSettingResource.SaveOAuthCodeParam paramBefore = new PlannerSettingResource.SaveOAuthCodeParam();
		PlannerSettingResource.SaveOAuthCodeApi api = new PlannerSettingResource.SaveOAuthCodeApi();

		paramBefore.companyId = company.Id;
		paramBefore.authCode = 'testAuthCodeBefore';
		paramBefore.state = new PlannerSettingService().generateState(company.Id);

		System.assert(api.execute(paramBefore) == null);

		// 検証
		List<CompanyPrivateSetting__c> resultBefore = [SELECT Id, Name, CompanyId__c, CryptoKey__c, Office365AuthCode__c FROM CompanyPrivateSetting__c];
		System.assertEquals(1, resultBefore.size());
		System.assertEquals(comPrivateSetting.CompanyId__c, resultBefore[0].Name);
		System.assertEquals(comPrivateSetting.CompanyId__c, resultBefore[0].CompanyId__c);
		System.assertEquals('testAuthCodeBefore', resultBefore[0].Office365AuthCode__c);
		System.debug(resultBefore[0].CryptoKey__c);

		// API実行（更新できることを確認）
		PlannerSettingResource.SaveOAuthCodeParam paramAfter = new PlannerSettingResource.SaveOAuthCodeParam();

		paramAfter.companyId = resultBefore[0].CompanyId__c;
		paramAfter.authCode = 'testAuthCodeAfter';
		paramAfter.state = new PlannerSettingService().generateState(resultBefore[0].CompanyId__c);

		System.assert(api.execute(paramAfter) == null);

		// 検証
		List<CompanyPrivateSetting__c> resultAfter = [SELECT Id, Name, CompanyId__c, CryptoKey__c, Office365AuthCode__c FROM CompanyPrivateSetting__c];
		System.assertEquals(1, resultAfter.size());
		System.assertEquals(comPrivateSetting.CompanyId__c, resultAfter[0].Name);
		System.assertEquals(comPrivateSetting.CompanyId__c, resultAfter[0].CompanyId__c);
		System.assertEquals('testAuthCodeAfter', resultAfter[0].Office365AuthCode__c);
		System.assertEquals(resultBefore[0].CryptoKey__c, resultAfter[0].CryptoKey__c);
	}

	/**
	 * 認証コード保存APIのテスト
	 * API実行権限を持っていない場合、異常終了することを検証する
	 */
	@isTest
	static void saveOAuthCodeApiTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// プランナー機能設定権限を無効に設定する
		testData.permission.isManagePlannerSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		PlannerSettingResource.SaveOAuthCodeParam param = new PlannerSettingResource.SaveOAuthCodeParam();
		param.companyId = testData.company.Id;
		param.authCode = 'testAuthCodeBefore';
		param.state = 'testState';

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				PlannerSettingResource.SaveOAuthCodeApi api = new PlannerSettingResource.SaveOAuthCodeApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 認証コード保存APIのテスト
	 * パラメータの会社レコードIDが設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void saveOAuthCodeApiTestCompanyIdNull() {

		// API実行
		PlannerSettingResource.SaveOAuthCodeParam param = new PlannerSettingResource.SaveOAuthCodeParam();
		PlannerSettingResource.SaveOAuthCodeApi api = new PlannerSettingResource.SaveOAuthCodeApi();

		param.authCode = 'testAuthCode';
		param.state = 'testState';

		try {
			api.execute(param);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'companyId'}), e.getMessage());
			System.debug(e.getMessage());
		}
	}

	/**
	 * 認証コード保存APIのテスト
	 * パラメータの会社レコードIDの値が不正な場合、異常終了することを検証する
	 */
	@isTest
	static void saveOAuthCodeApiTestCompanyIdInvalid() {

		// API実行
		PlannerSettingResource.SaveOAuthCodeParam param = new PlannerSettingResource.SaveOAuthCodeParam();
		PlannerSettingResource.SaveOAuthCodeApi api = new PlannerSettingResource.SaveOAuthCodeApi();

		param.companyId = 'invalid';
		param.authCode = 'testAuthCode';
		param.state = 'testState';

		try {
			api.execute(param);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'companyId', String.valueOf(param.companyId)}), e.getMessage());
			System.debug(e.getMessage());
		}
	}

	/**
	 * 認証コード保存APIのテスト
	 * パラメータの認証コードが設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void saveOAuthCodeApiTestAuthCodeNull() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		// リクエストパラメータ作成
		PlannerSettingResource.SaveOAuthCodeParam param = new PlannerSettingResource.SaveOAuthCodeParam();
		param.companyId = company.Id;
		param.state = 'testState';

		// API実行
		PlannerSettingResource.SaveOAuthCodeApi api = new PlannerSettingResource.SaveOAuthCodeApi();

		try {
			api.execute(param);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'authCode'}), e.getMessage());
			System.debug(e.getMessage());
		}
	}

	/**
	 * 認証コード保存APIのテスト
	 * パラメータのステート情報が設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void saveOAuthCodeApiTestStateNull() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		// API実行
		PlannerSettingResource.SaveOAuthCodeParam param = new PlannerSettingResource.SaveOAuthCodeParam();
		PlannerSettingResource.SaveOAuthCodeApi api = new PlannerSettingResource.SaveOAuthCodeApi();

		param.companyId = company.Id;
		param.authCode = 'testAuthCode';

		try {
			api.execute(param);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'state'}), e.getMessage());
			System.debug(e.getMessage());
		}
	}

	/**
	 * 認証コード保存APIのテスト
	 * 保存対象のレコードが存在しない場合、異常終了することを検証する
	 */
	@isTest
	static void saveOAuthCodeApiTestEntityNull() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);

		// API実行
		PlannerSettingResource.SaveOAuthCodeParam param = new PlannerSettingResource.SaveOAuthCodeParam();
		PlannerSettingResource.SaveOAuthCodeApi api = new PlannerSettingResource.SaveOAuthCodeApi();

		param.companyId = company.Id;
		param.authCode = 'testAuthCode';
		param.state = 'testState';

		try {
			api.execute(param);
			System.assert(false);
		} catch (App.RecordNotFoundException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, e.getMessage());
			System.debug(e.getMessage());
		}
	}

	/**
	 * OAuth認証URL取得APIのテスト
	 * URLが取得できることを確認する
	 */
	@isTest
	static void getOAuthUrlApiTestNormal() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		// リクエストパラメータ作成
		PlannerSettingResource.GetOAuthUrlRequest param = new PlannerSettingResource.GetOAuthUrlRequest();
		param.companyId = company.Id;

		// API実行
		PlannerSettingResource.GetOAuthUrlApi api = new PlannerSettingResource.GetOAuthUrlApi();
		PlannerSettingResource.GetOAuthUrlResponse response
				= (PlannerSettingResource.GetOAuthUrlResponse)api.execute(param);

		// 検証
		// URL形式であることだけを確認しておく
		System.assertEquals(true, response.authUrl.startsWith('https://'));
	}

	/**
	 * OAuth認証URL取得APIのテスト
	 * リクエストパラメータの値が不正な場合に想定されている例外が発生することを確認する
	 */
	@isTest
	static void getOAuthUrlApiTestInvalidParam() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		PlannerSettingResource.GetOAuthUrlApi api = new PlannerSettingResource.GetOAuthUrlApi();
		PlannerSettingResource.GetOAuthUrlRequest param;
		App.ParameterException resEx;
		String expMessage;

		// Test1: 会社IDが未設定
		try {
			resEx = null;

			param = new PlannerSettingResource.GetOAuthUrlRequest();
			param.companyId = '';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'companyId'});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test2: 会社IDがID以外の文字列
		try {
			resEx = null;

			param = new PlannerSettingResource.GetOAuthUrlRequest();
			param.companyId = 'aaa';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'companyId', param.companyId});
		System.assertEquals(expMessage, resEx.getMessage());
	}

	/**
	 * プランナー機能設定保存APIのテスト
	 * 設定が保存できることを確認する
	 */
	@isTest
	static void saveApiTestNormal() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);

		// リクエストパラメータ作成
		PlannerSettingResource.SaveRequest param = new PlannerSettingResource.SaveRequest();
		param.companyId = company.Id;
		param.useCalendarAccess = true;
		param.calendarAccessService = 'Office365';

		// API実行
		PlannerSettingResource.SaveApi api = new PlannerSettingResource.SaveApi();
		api.execute(param);

		// 検証
		CompanyEntity savedCompany = new CompanyRepository().getEntity(company.Id);
		System.assertEquals(param.useCalendarAccess, savedCompany.useCalendarAccess);
		System.assertEquals(param.calendarAccessService, savedCompany.calendarAccessService.value);
	}

	/**
	 * プランナー機能設定保存APIのテスト
	 * API実行権限を持っていない場合、異常終了することを検証する
	 */
	@isTest
	static void saveApiTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// プランナー機能設定権限を無効に設定する
		testData.permission.isManagePlannerSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ作成
		PlannerSettingResource.SaveRequest param = new PlannerSettingResource.SaveRequest();
		param.companyId = testData.company.Id;
		param.useCalendarAccess = true;
		param.calendarAccessService = 'Office365';

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				PlannerSettingResource.SaveApi api = new PlannerSettingResource.SaveApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * プランナー機能設定保存APIのテスト
	 * リクエストパラメータの会社IDの値が不正な場合に想定されている例外が発生することを確認する
	 */
	@isTest
	static void saveApiTestInvalidParamCompany() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		// CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		PlannerSettingResource.SaveApi api = new PlannerSettingResource.SaveApi();
		PlannerSettingResource.SaveRequest param;
		App.ParameterException resEx;
		String expMessage;

		// Test1: 会社IDが未設定
		try {
			resEx = null;

			param = new PlannerSettingResource.SaveRequest();
			param.companyId = '';
			param.useCalendarAccess = true;
			param.calendarAccessService = 'Office365';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx =e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'companyId'});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test2: 会社IDがID以外の文字列
		try {
			resEx = null;

			param = new PlannerSettingResource.SaveRequest();
			param.companyId = 'aaa';
			param.useCalendarAccess = true;
			param.calendarAccessService = 'Office365';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'companyId', param.companyId});
		System.assertEquals(expMessage, resEx.getMessage());
	}

	/**
	 * プランナー機能設定保存APIのテスト
	 * カレンダーのリクエストパラメータの値が不正な場合に想定されている例外が発生することを確認する
	 */
	@isTest
	static void saveApiTestInvalidParamCalendar() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		// CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		PlannerSettingResource.SaveApi api = new PlannerSettingResource.SaveApi();
		PlannerSettingResource.SaveRequest param;
		App.ParameterException resEx;
		String expMessage;

		// Test1: 外部カレンダー連携の有効無効フラグが未設定の場合
		try {
			resEx = null;

			param = new PlannerSettingResource.SaveRequest();
			param.companyId = company.Id;
			param.useCalendarAccess = null;
			param.calendarAccessService = 'Office365';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'useCalendarAccess'});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test2: 外部カレンダー連携が有効で、連携対象の外部カレンダーが未設定の場合
		try {
			resEx = null;

			param = new PlannerSettingResource.SaveRequest();
			param.companyId = company.Id;
			param.useCalendarAccess = true;
			param.calendarAccessService = null;
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx =e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'calendarAccessService'});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test3: 外部カレンダー連携が有効で、連携対象の外部カレンダーが不正な場合
		try {
			resEx = null;

			param = new PlannerSettingResource.SaveRequest();
			param.companyId = company.Id;
			param.useCalendarAccess = true;
			param.calendarAccessService = 'Office';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx =e;
		}
		TestUtil.assertNotNull(resEx, 'Test3: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'calendarAccessService', param.calendarAccessService});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test4: 外部カレンダー連携が無効の場合は、連携対象の外部カレンダーは未設定でもOK
		try {
			resEx = null;

			param = new PlannerSettingResource.SaveRequest();
			param.companyId = company.Id;
			param.useCalendarAccess = false;
			param.calendarAccessService = '';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx =e;
		}
		System.assertEquals(null, resEx, 'Test4: 例外が発生しました。' + resEx);
	}

	/**
	 * プランナー機能設定取得APIのテスト
	 * カレンダー連携機能が無効に設定されている場合、設定が取得できることを確認する
	 */
	@isTest
	static void getApiTestDisableCalendar() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		// 連携有効で設定しておく
		company.UseCalendarAccess__c = false;
		company.CalendarAccessService__c = 'Office365';
		update company;

		// リクエストパラメータ作成
		PlannerSettingResource.GetRequest param = new PlannerSettingResource.GetRequest();
		param.companyId = company.Id;

		// API実行
		PlannerSettingResource.GetApi api = new PlannerSettingResource.GetApi();
		PlannerSettingResource.GetResponse response
				= (PlannerSettingResource.GetResponse)api.execute(param);

		// 検証
		System.assertEquals(company.UseCalendarAccess__c, response.useCalendarAccess);
		System.assertEquals(company.CalendarAccessService__c, response.calendarAccessService);
		System.assertEquals(null, response.authStatus);
	}

	/**
	 * プランナー機能設定取得APIのテスト
	 * カレンダー連携機能が有効に設定されている場合、設定が取得できることを確認する
	 */
	@isTest static void getApiTestEnableCalendar() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		// 連携有効で設定しておく
		company.UseCalendarAccess__c = true;
		company.CalendarAccessService__c = 'Office365';
		update company;

		// リクエストパラメータ作成
		PlannerSettingResource.GetRequest param = new PlannerSettingResource.GetRequest();
		param.companyId = company.Id;

		// API実行
		PlannerSettingResource.GetApi api = new PlannerSettingResource.GetApi();
		PlannerSettingResource.GetResponse response
				= (PlannerSettingResource.GetResponse)api.execute(param);

		// 検証
		System.assertEquals(company.UseCalendarAccess__c, response.useCalendarAccess);
		System.assertEquals(company.CalendarAccessService__c, response.calendarAccessService);
		System.assertEquals('UNAUTHORIZED', response.authStatus);
	}

	/**
	 * プランナー機能設定取得APIのテスト
	 * リクエストパラメータの値が不正な場合に想定されている例外が発生することを確認する
	 */
	@isTest
	static void getApiTestInvalidParam() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		PlannerSettingResource.GetApi api = new PlannerSettingResource.GetApi();
		PlannerSettingResource.GetRequest param;
		App.ParameterException resEx;
		String expMessage;

		// Test1: 会社IDが未設定
		try {
			resEx = null;

			param = new PlannerSettingResource.GetRequest();
			param.companyId = '';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx =e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'companyId'});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test2: 会社IDがID以外の文字列
		try {
			resEx = null;

			param = new PlannerSettingResource.GetRequest();
			param.companyId = 'aaa';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'companyId', param.companyId});
		System.assertEquals(expMessage, resEx.getMessage());
	}

	/**
	 * プランナー機能設定取得APIのテスト
	 * 存在しない会社を指定した場合に想定されている例外が発生することを確認する
	 */
	@isTest
	static void getApiTestCompanyNotExist() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		CompanyPrivateSetting__c comPrivateSetting = ComTestDataUtility.createCompanyPrivateSetting(company);

		PlannerSettingResource.GetApi api = new PlannerSettingResource.GetApi();
		PlannerSettingResource.GetRequest param;
		App.RecordNotFoundException resEx;

		// 会社を削除しておく
		delete company;

		// API実行
		try {
			resEx = null;

			param = new PlannerSettingResource.GetRequest();
			param.companyId = company.Id;
			api.execute(param);
		} catch (App.RecordNotFoundException e) {
			resEx =e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
	}
}