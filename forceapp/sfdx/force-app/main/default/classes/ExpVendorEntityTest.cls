/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpVendorEntity
 */
@isTest
private class ExpVendorEntityTest {

	private static ExpVendorEntityTest.TestData testData = new TestData();

	/**
	 * Test Data Class
	 */
	private class TestData extends TestData.TestDataEntity {
		/** Accounting Period Data */
		public List<ExpVendorEntity> vendors = new List<ExpVendorEntity>();

		/**
		 * Constructor
		 */
		public TestData() {
			super();
			// Create entity
			ComTestDataUtility.createExpVendors('TestCode', this.company.id, 3);
			this.vendors = (new ExpVendorRepository()).getEntityList(null);
		}

		/**
		 * Create Accounting Period object
		 * @code Accounting Period Code
		 * @size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpVendorEntity> createExpVendors(String code, Integer size) {
			return createExpVendors(code, this.company.id, size);
		}

		/**
		 * Create Accounting Period object
		 * @code Accounting Period Code
		 * @companyId Company ID
		 * @size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpVendorEntity> createExpVendors(String code, Id companyId, Integer size) {
			ComTestDataUtility.createExpVendors(code, companyId, size);
			return (new ExpVendorRepository()).getEntityList(null);
		}
	}

	/**
	 * Confirm unique key is created properly
	 */
	@isTest static void createUniqKeyTest() {
		ExpVendorEntity entity = new ExpVendorEntity();

		// When rate code and company code are not empty, key would be created.
		entity.code = 'TestCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// When rate code is empty, exception would occur.
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occurred');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// When company code is empty, exception would occur.
		entity.code = 'TestCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occurred');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/**
	 * Test for field changed status
	 * Confirm status is changed when field value is changed
	 */
	@isTest static void isChangedTest() {
		ExpVendorEntity entity = testData.createExpVendors('Test', 1).get(0);

		entity.resetChanged();

		ExpVendorEntity.Field targetField = ExpVendorEntity.Field.BANK_ACCOUNT_NUMBER;
		System.assertEquals(false, entity.isChanged(targetField));
		entity.bankAccountNumber = 'Changed';
		System.assertEquals(true, entity.isChanged(targetField));
	}

	/** Test for getFieldValue method */
	@isTest static void getFieldValueTest() {
		ExpVendorEntity entity = testData.createExpVendors('Test2', 1).get(0);

		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.NAME_L0), entity.nameL0);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.NAME_L1), entity.nameL1);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.NAME_L2), entity.nameL2);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.CODE), entity.code);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.ADDRESS), entity.address);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.BANK_ACCOUNT_NUMBER), entity.bankAccountNumber);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.BANK_CODE), entity.bankCode);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.BANK_NAME), entity.bankName);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.BRANCH_ADDRESS), entity.branchAddress);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.BRANCH_CODE), entity.branchCode);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.BRANCH_NAME), entity.branchName);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.CORRESPONDENT_BANK_ADDRESS), entity.correspondentBankAddress);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.CORRESPONDENT_BANK_NAME), entity.correspondentBankName);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.CORRESPONDENT_BRANCH_NAME), entity.correspondentBranchName);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.CORRESPONDENT_SWIFT_CODE), entity.correspondentSwiftCode);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.COUNTRY), entity.country);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.PAYEE_NAME), entity.payeeName);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.PAYMENT_TERM), entity.paymentTerm);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.SWIFT_CODE), entity.swiftCode);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.ZIP_CODE), entity.zipCode);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.ACTIVE), entity.active);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.BANK_ACCOUNT_TYPE), entity.bankAccountType);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.COMPANY_ID), entity.companyId);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.CURRENCY_CODE), entity.currencyCode);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.IS_WITHHOLDING_TAX), entity.isWithholdingTax);
		System.assertEquals(entity.getFieldValue(ExpVendorEntity.Field.UNIQUE_KEY), entity.uniqKey);
	}

	/** Test for setFieldValue method */
	@isTest static void setFieldValueTest() {
		ExpVendorEntity entity = testData.createExpVendors('Test2', 1).get(0);

		for (ExpVendorEntity.Field field : ExpVendorEntity.Field.values()) {
			ExpVendorEntity testEntity = new ExpVendorEntity();

			Object value = entity.getFieldValue(field);
			testEntity.setFieldValue(field, value);
			System.assertEquals(value, entity.getFieldValue(field));
		}
	}
}