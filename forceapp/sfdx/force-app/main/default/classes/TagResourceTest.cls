/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description TagResourceのテストクラス
*/
@isTest
private class TagResourceTest {

	/**
	 * タグ検索APIのテスト
	 * タグIDで検索する
	 */
	@isTest
	static void searchApiTestById() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		List<ComTag__c> tagObjes = new List<ComTag__c>{
			ComTestDataUtility.createTag('tag3', TagType.SHORTEN_WORK_REASON, companyObj.Id, 3, false),
			ComTestDataUtility.createTag('tag1', TagType.SHORTEN_WORK_REASON, companyObj.Id, 1, false),
			ComTestDataUtility.createTag('tag2', TagType.JOB_ASSIGN_GROUP, companyObj.Id, 2, false)
		};

		// API実行
		Test.startTest();
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => tagObjes[0].Id
		};
		TagResource.SearchApi api = new TagResource.SearchApi();
		TagResource.SearchResult result = (TagResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		// 検証
		System.assertEquals(1, result.records.size());
		System.assertEquals('tag3', result.records[0].code);
	}

	/**
	 * タグ検索APIのテスト
	 * 会社とタグタイプで検索する
	 */
	@isTest
	static void searchApiTestByCompanyAndTagType() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		List<ComTag__c> tagObjes = new List<ComTag__c>{
			ComTestDataUtility.createTag('tag4', TagType.SHORTEN_WORK_REASON, companyObj.Id, 3, false),
			ComTestDataUtility.createTag('tag3', TagType.SHORTEN_WORK_REASON, companyObj.Id, 3, false),
			ComTestDataUtility.createTag('tag1', TagType.SHORTEN_WORK_REASON, companyObj.Id, 1, false),
			ComTestDataUtility.createTag('tag2', TagType.JOB_ASSIGN_GROUP, companyObj.Id, 2, false)
		};

		// API実行
		Test.startTest();
		Map<String, Object> paramMap = new Map<String, Object> {
			'companyId' => companyObj.Id,
			'tagType' => TagType.SHORTEN_WORK_REASON.value
		};
		TagResource.SearchApi api = new TagResource.SearchApi();
		TagResource.SearchResult result = (TagResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		System.assertEquals(3, result.records.size());
		System.assertEquals('tag1', result.records[0].code);
		System.assertEquals('tag3', result.records[1].code);
		System.assertEquals('tag4', result.records[2].code);
	}

	/**
	 * タグ検索APIのテスト
	 * 検索結果が存在しない場合のレスポンスを検証する
	 */
	@isTest
	static void searchApiTestNoResult() {
		// API実行
		Map<String, Object> paramMap = new Map<String, Object> {
			'tagType' => 'NO_RESULT'
		};
		Test.startTest();
		TagResource.SearchApi api = new TagResource.SearchApi();
		TagResource.SearchResult result = (TagResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		// 結果がnullではないこと確認
		System.assertEquals(0, result.records.size());
	}

	/*
	 * エンティティをレスポンスクラスに変換するテスト
	 */
	@isTest static void toResponseTest() {
		// 変換対象のエンティティ
		TagEntity entity = new TagEntity();
		entity.setId(ComTestDataUtility.getDummyId(entity.createSObject().getSObjectType())); // 適当なID
		entity.code = 'code';
		entity.tagTypeValue = TagType.JOB_ASSIGN_GROUP;
		entity.name = 'name';
		entity.nameL0 = 'nameL0';
		entity.nameL1 = 'nameL1';
		entity.nameL2 = 'nameL2';
		entity.companyId = Id.valueOf('001xa000010DIlo');
		entity.order = 10;
		entity.isRemoved = false;

		// 変換後のレスポンス
		TagResource.Tag response = new TagResource.SearchApi().toResponse(entity);
		System.assertEquals(entity.id, response.id);
		System.assertEquals('code', entity.code);
		System.assertEquals('JobAssignGroup', entity.tagTypeValue.value);
		System.assertEquals('name', entity.name);
		System.assertEquals('nameL0', entity.nameL0);
		System.assertEquals('nameL1', entity.nameL1);
		System.assertEquals('nameL2', entity.nameL2);
		System.assertEquals('001xa000010DIlo', entity.companyId);
		System.assertEquals(10, entity.order);
	}

	/**
	 * 短時間勤務の理由タグレコードを1件作成する(正常系)
	 * DBに正しく保存されていることを確認する
	 */
	@isTest
	static void createTestSaveWithSW() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagResource.Tag param = new TagResource.Tag();
		param.companyId = testData.company.id;
		param.code = '001';
		param.tagType = TagType.SHORTEN_WORK_REASON.value;
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.order = 1;

		Test.startTest();
		TagResource.CreateApi api = new TagResource.CreateApi();
		TagResource.SaveResult res = (TagResource.SaveResult)api.execute(param);
		Test.stopTest();

		System.assertNotEquals(null, res.id);

		// レコードが作成されている
		TagEntity entity = new TagRepository().getEntity(res.id);
		System.assertNotEquals(null, entity);

		// レコード値の検証
		System.assertEquals(param.code, entity.code);
		System.assertEquals(param.companyId, entity.companyId);
		System.assertEquals(param.tagType, entity.tagTypeValue.value);
		System.assertEquals(param.name_L0, entity.name);
		System.assertEquals(param.name_L0, entity.nameL0);
		System.assertEquals(param.name_L1, entity.nameL1);
		System.assertEquals(param.name_L2, entity.nameL2);
		System.assertEquals(param.order, entity.order);
	}

	/**
	 * ジョブアサイングループのタグレコードを1件作成する(正常系)
	 * DBに正しく保存されていることを確認する
	 * FIXME: 現状、このタグを保存する機能がないので省略します。
	 * 必要になったら実装してください。
	 */
	@isTest
	static void createTestSaveWithJA() {
	}

	/**
	 * タグレコードを1件作成する(正常系)
	 * パラメータにデフォルト値が正しく適用されることを確認する
	 */
	@isTest
	static void createTestDefaultValue() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagResource.Tag param = new TagResource.Tag();
		param.companyId = testData.company.id;
		param.code = '001';
		param.tagType = TagType.SHORTEN_WORK_REASON.value;
		param.name_L0 = 'テスト設定_L0';

		Test.startTest();
		TagResource.CreateApi api = new TagResource.CreateApi();
		TagResource.SaveResult res = (TagResource.SaveResult)api.execute(param);
		Test.stopTest();

		System.assertNotEquals(null, res.id);

		// レコードが作成されている
		TagEntity entity = new TagRepository().getEntity(res.id);
		System.assertNotEquals(null, entity);

		// レコード値の検証
		System.assertEquals(param.code, entity.code);
		System.assertEquals(param.companyId, entity.companyId);
		System.assertEquals(param.tagType, entity.tagTypeValue.value);
		System.assertEquals(param.name_L0, entity.name);
		System.assertEquals(param.name_L0, entity.nameL0);
		System.assertEquals(null, entity.nameL1);
		System.assertEquals(null, entity.nameL2);
		System.assertEquals(null, entity.order);
	}

	/**
	 * タグレコードを1件作成する(異常系)
	 * パラメータ（name）が不足している場合にエラーが発生することを確認します。
	 * companyId, code, tagType が不足している場合もエラーが発生しますが、
	 * このテストとは異なる分岐であり、そのテストは TagServiceTest で行っているのでここでは省略します。
	 */
	@isTest
	static void createNegativeTestWithHasNotRequred() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagResource.Tag param = new TagResource.Tag();
		param.companyId = testData.company.id;
		param.code = '001';
		param.tagType = TagType.SHORTEN_WORK_REASON.value;

		App.ParameterException ex;
		Test.startTest();
		try {
			TagResource.CreateApi api = new TagResource.CreateApi();
			TagResource.SaveResult res = (TagResource.SaveResult)api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('NOT_SET_VALUE', ex.getErrorCode());
	}

	/**
	 * タグレコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		TagResource.Tag param = new TagResource.Tag();
		param.companyId = testData.company.id;
		param.code = '001';
		param.tagType = TagType.SHORTEN_WORK_REASON.value;
		param.name_L0 = 'テスト設定_L0';

		App.NoPermissionException ex;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TagResource.CreateApi api = new TagResource.CreateApi();
				TagResource.SaveResult res = (TagResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				ex = e;
			}
		}

		// 検証
		System.assertNotEquals(null, ex, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, ex.getMessage());
	}

	/**
	 * タグレコードを1件更新する(正常系)
	 * DBに正しく保存されていることを確認する
	 */
	@isTest
	static void updateTestSave() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity baseTagEntity = testData.createTag('TestTag', TagType.SHORTEN_WORK_REASON);

		TagResource.Tag param = new TagResource.Tag();
		param.id = baseTagEntity.id;
		param.companyId = testData.company.id;
		param.code = '001';
		param.tagType = TagType.SHORTEN_WORK_REASON.value;
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.order = 1;

		Test.startTest();
		TagResource.UpdateApi api = new TagResource.UpdateApi();
		api.execute(param);
		Test.stopTest();

		// レコード値の検証
		TagEntity entity = new TagRepository().getEntity(baseTagEntity.id);
		System.assertEquals(param.id, entity.id);
		System.assertEquals(param.code, entity.code);
		System.assertEquals(param.companyId, entity.companyId);
		System.assertEquals(param.tagType, entity.tagTypeValue.value);
		System.assertEquals(param.name_L0, entity.name);
		System.assertEquals(param.name_L0, entity.nameL0);
		System.assertEquals(param.name_L1, entity.nameL1);
		System.assertEquals(param.name_L2, entity.nameL2);
		System.assertEquals(param.order, entity.order);
	}

	/**
	 * タグレコードを1件更新する(異常系)
	 * 更新対象のレコードが存在しない場合はエラーが発生することを確認します。
	 */
	@isTest
	static void updateNegativeTestSave() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity baseTagEntity = testData.createTag('TestTag', TagType.SHORTEN_WORK_REASON);

		TagResource.Tag param = new TagResource.Tag();
		param.id = baseTagEntity.id;
		param.companyId = testData.company.id;
		param.code = '001';
		param.tagType = TagType.SHORTEN_WORK_REASON.value;
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.order = 1;

		App.RecordNotFoundException ex;
		Test.startTest();
		try {
			new TagRepository().deleteEntity(baseTagEntity.id);
			TagResource.UpdateApi api = new TagResource.UpdateApi();
			api.execute(param);
		} catch (App.RecordNotFoundException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('RECORD_NOT_FOUND', ex.getErrorCode());
	}

	/**
	 * タグレコードを1件更新する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		TagEntity baseTagEntity =  new TestData.TestDataEntity().createTag('TestTag', TagType.SHORTEN_WORK_REASON);

		TagResource.Tag param = new TagResource.Tag();
		param.id = baseTagEntity.id;
		param.companyId = testData.company.id;
		param.code = '001';
		param.tagType = TagType.SHORTEN_WORK_REASON.value;
		param.name_L0 = 'テスト設定_L0';

		App.NoPermissionException ex;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TagResource.UpdateApi api = new TagResource.UpdateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				ex = e;
			}
		}

		// 検証
		System.assertNotEquals(null, ex, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, ex.getMessage());
	}

	/**
	 * タグレコードを1件削除する(正常系)
	 * DBに正しく削除されていることを確認する
	 */
	@isTest
	static void deleteTestSave() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity baseTagEntity = testData.createTag('TestTag', TagType.SHORTEN_WORK_REASON);

		TagResource.Tag param = new TagResource.Tag();
		param.id = baseTagEntity.id;

		Test.startTest();
		TagResource.DeleteApi api = new TagResource.DeleteApi();
		api.execute(param);
		Test.stopTest();

		// レコード値の検証
		TagEntity entity = new TagRepository().getEntity(baseTagEntity.id);
		System.assertEquals(null, entity);
	}

	/**
	 * タグレコードを1件削除する(異常系)
	 */
	@isTest
	static void deleteNegativeTestIdIsNull() {
		TagResource.Tag param = new TagResource.Tag();
		param.id = '';

		App.ParameterException ex;
		Test.startTest();
		try {
			TagResource.DeleteApi api = new TagResource.DeleteApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * タグレコードを1件削除する(異常系)
	 * 削除対象のレコードが存在しない場合はエラーが発生することを確認します。
	 */
	@isTest
	static void deleteNegativeTestNotFound() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity baseTagEntity = testData.createTag('TestTag', TagType.SHORTEN_WORK_REASON);

		TagResource.Tag param = new TagResource.Tag();
		param.id = baseTagEntity.id;

		App.RecordNotFoundException ex;
		Test.startTest();
		try {
			new TagRepository().deleteEntity(baseTagEntity.id);
			TagResource.DeleteApi api = new TagResource.DeleteApi();
			api.execute(param);
		} catch (App.RecordNotFoundException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('RECORD_NOT_FOUND', ex.getErrorCode());
	}

	/**
	 * タグレコードを1件削除する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		TagEntity baseTagEntity =  new TestData.TestDataEntity().createTag('TestTag', TagType.SHORTEN_WORK_REASON);

		TagResource.Tag param = new TagResource.Tag();
		param.id = baseTagEntity.id;

		App.NoPermissionException ex;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TagResource.DeleteApi api = new TagResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				ex = e;
			}
		}

		// 検証
		System.assertNotEquals(null, ex, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, ex.getMessage());
	}
}