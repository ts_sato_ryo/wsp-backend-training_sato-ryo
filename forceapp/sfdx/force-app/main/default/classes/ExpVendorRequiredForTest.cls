/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Test class for ExpVendorRequiredFor
 */
@isTest
private class ExpVendorRequiredForTest {

	/**
	 * Check if it compare properly
	 */
	@isTest static void equalsTest() {

		// Equal
		System.assertEquals(ExpVendorRequiredFor.EXPENSE_REPORT, ExpVendorRequiredFor.valueOf('ExpenseReport'));
		System.assertEquals(ExpVendorRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ExpVendorRequiredFor.valueOf('ExpenseRequestAndExpenseReport'));

		// Values are different
		System.assertEquals(false, ExpVendorRequiredFor.EXPENSE_REPORT.equals(ExpVendorRequiredFor.valueOf('ExpenseRequestAndExpenseReport')));

		// Class type is different
		System.assertEquals(false, ExpVendorRequiredFor.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// Value is null
		System.assertEquals(false, ExpVendorRequiredFor.EXPENSE_REPORT.equals(null));
	}
}