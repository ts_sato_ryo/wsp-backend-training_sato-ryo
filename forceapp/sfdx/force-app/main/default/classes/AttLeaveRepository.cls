/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 休暇マスタのリポジトリ
 */
public with sharing class AttLeaveRepository extends ValidPeriodRepository {

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無 */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** キャッシュ */
	private static final Repository.Cache CACHE = new Repository.Cache();

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_LEAVE_FIELD_SET;
	static {
		GET_LEAVE_FIELD_SET = new Set<Schema.SObjectField> {
				AttLeave__c.Id,
				AttLeave__c.CompanyId__c,
				AttLeave__c.Name,
				AttLeave__c.Name_L0__c,
				AttLeave__c.Name_L1__c,
				AttLeave__c.Name_L2__c,
				AttLeave__c.Code__c,
				AttLeave__c.UniqKey__c,
				AttLeave__c.Type__c,
				AttLeave__c.DaysManaged__c,
				AttLeave__c.Range__c,
				AttLeave__c.RequireReason__c,
				AttLeave__c.Order__c,
				AttLeave__c.SummaryItemNo__c,
				AttLeave__c.CountType__c,
				AttLeave__c.ValidFrom__c,
				AttLeave__c.ValidTo__c
		};
	}

	/**
	 * コンストラクタ
	 * キャッシュを利用しないインスタンスを生成する
	 */
	public AttLeaveRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public AttLeaveRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}


	/** 検索フィルタ */
	@TestVisible
	private class LeaveFilter {
		public Set<Id> leaveIds;
		public Id companyId;
		public List<String> codes;
		public boolean daysManaged;
		public AttLeaveType leaveType;
		public AttLeaveType exceptLeaveType;
		/** 対象日(nullの場合は全レコードが対象) */
		public AppDate targetDate;
	}

	/** 検索フィルタ (検索API実行用) */
	public class SearchFilter {
		public String id;
		public String companyId;
		public boolean daysManaged;
		public AttLeaveType leaveType;
		public AttLeaveType exceptLeaveType;
	}

	/**
	 * 指定会社の全休暇マスタを取得する
	 * @param companyId 指定する会社Id
	 * @return 指定会社の全休暇マスタ一覧(指定順)
	 */
	public List<AttLeaveEntity> getLeaveListByCompanyId(Id companyId) {
		LeaveFilter filter = new LeaveFilter();
		filter.companyId = companyId;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定したコードの休暇マスタを取得する
	 * @param code 取得対象の休暇コード
	 * @param companyId 会社ID
	 * @return 休暇マスタ、存在しない場合はnull
	 */
	public AttLeaveEntity getLeaveByCode(String code, Id companyId) {
		LeaveFilter filter = new LeaveFilter();
		filter.codes = new List<String>{code};
		filter.companyId = companyId;

		List<AttLeaveEntity> leaves = searchEntityList(filter, false);
		if (leaves.isEmpty()) {
			return null;
		}
		return leaves[0];
	}

	/**
	 * 指定したidの休暇マスタを取得する
	 * @param leaveId 取得対象の休暇id
	 * @return 休暇マスタ
	 */
	public AttLeaveEntity getLeaveById(Id leaveId) {
		LeaveFilter filter = new LeaveFilter();
		filter.leaveIds = new Set<Id>{leaveId};

		for (AttLeaveEntity leaveData : searchEntityList(filter, false)) {
			return leaveData;
		}
		return null;
	}
	/**
	 * 指定したidの休暇マスタを取得する
	 * @param leaveId 取得対象の休暇id
	 * @return 休暇マスタ
	 */
	public List<AttLeaveEntity> getLeaveListByIds(Set<Id> leaveIds) {
		LeaveFilter filter = new LeaveFilter();
		filter.leaveIds = leaveIds;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定したコードの休暇マスタを取得する
	 * @param code 取得対象の休暇コード
	 * @return 休暇マスタ一覧、Order__c順
	 */
	public List<AttLeaveEntity> getLeaveByCodes(List<String> codes) {
		LeaveFilter filter = new LeaveFilter();
		filter.codes = codes;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定したコードの休暇マスタを取得する
	 * @param code 取得対象の休暇コード
	 * @return 対象日に有効な休暇マスタ一覧、Order__c順
	 */
	public List<AttLeaveEntity> getLeaveByCodes(List<String> codes, Id companyId) {
		return getLeaveByCodes(codes, companyId, null);
	}

	/**
	 * 指定したコードの休暇マスタを取得する
	 * @param code 取得対象の休暇コード
	 * @param targetDate 対象日、nullの場合は全ての日が対象となる
	 * @return 対象日に有効な休暇マスタ一覧、Order__c順
	 */
	public List<AttLeaveEntity> getLeaveByCodes(List<String> codes, Id companyId, AppDate targetDate) {
		LeaveFilter filter = new LeaveFilter();
		filter.codes = codes;
		filter.targetDate = targetDate;
		filter.companyId = companyId;

		return searchEntityList(filter, false);
	}

	/**
	 * 休暇を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した勤怠休暇一覧(指定順)
	 */
	@TestVisible
	private List<AttLeaveEntity> searchEntityList(
			LeaveFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchEntityList ReturnCache key:' + cacheKey);
			List<AttLeaveEntity> entities = new List<AttLeaveEntity>();
			for (AttLeave__c sObj : (List<AttLeave__c>)CACHE.get(cacheKey)) {
				entities.add(new AttLeaveEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchEntityList NoCache key:' + cacheKey);

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_LEAVE_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// TODO フィルタ条件からWHERE句の条件リストを作成してください
		List<String> condList = new List<String>();
		
		final Id companyId = filter.companyId;
		final Set<Id> leaveIds = filter.leaveIds;
		final List<String> codes = filter.codes;
		final Boolean daysManaged = filter.daysManaged;

		if (companyId != null) {
			condList.add(getFieldName(AttLeave__c.CompanyId__c) + ' = : companyId');
		}
		if (leaveIds != null) {
			condList.add(getFieldName(AttLeave__c.Id) + ' IN : leaveIds');
		}
		if (codes != null) {
			condList.add(getFieldName(AttLeave__c.Code__c) + ' IN : codes');
		}
		if (daysManaged != null) {
			condList.add(getFieldName(AttLeave__c.DaysManaged__c) + ' = : daysManaged');
		}

		AttLeaveType pLeaveType = filter.leaveType;
		String leaveType;
		if (pleaveType != null) {
			leaveType = pLeaveType.value;
			condList.add(getFieldName(AttLeave__c.Type__c) + ' = : leaveType');
		}

		AttLeaveType pExceptLeaveType = filter.exceptLeaveType;
		String exceptLeaveType;
		if (pexceptLeaveType != null) {
			exceptLeaveType = pExceptLeaveType.value;
			condList.add(getFieldName(AttLeave__c.Type__c) + ' != : exceptLeaveType');
		}
		Date targetDate;
		if (filter.targetDate != null) {
			targetDate = filter.targetDate.getDate();
			condList.add(getFieldName(AttAgreementAlertSetting__c.ValidFrom__c) + ' <= :targetDate');
			condList.add(getFieldName(AttAgreementAlertSetting__c.ValidTo__c) + ' > :targetDate');
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + AttLeave__c.SObjectType.getDescribe().getName()
				+ buildWhereString(condList);

		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(AttLeave__c.Order__c) + ' Asc,'
				+ getFieldName(AttLeave__c.Code__c) + ' Asc';
		}
		// クエリ実行
		//System.debug('AttLeaveRepository.searchEntityList: SOQL==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttLeave__c> sObjList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttLeave__c.SObjectType);

		// 結果からエンティティを作成する
		List<AttLeaveEntity> entityList = new List<AttLeaveEntity>();
		for (AttLeave__c attLeave : sObjList) {
			entityList.add(new AttLeaveEntity(attLeave));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			CACHE.put(cacheKey, sObjList);
		}
		return entityList;
	}

	/**
	 * 休暇を検索する (検索API用)
	 * @param filter 検索条件
	 * @return 条件に一致した勤怠休暇一覧(指定順)
	 */
	public List<AttLeaveEntity> searchEntityList(SearchFilter filter){
		LeaveFilter f = new LeaveFilter();
		if (filter.id != null) {
			f.leaveIds = new Set<Id>{ filter.id };
		}
		if (filter.companyId != null) {
			f.companyId = filter.companyId;
		}
		if (filter.leaveType != null) {
			f.leaveType = filter.leaveType;
		}
		if (filter.daysManaged != null) {
			f.daysManaged = filter.daysManaged;
		}
		if (filter.exceptLeaveType != null) {
			f.exceptLeaveType = filter.exceptLeaveType;
		}
		return searchEntityList(f, false);
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntity> entityList) {
		List<AttLeave__c> objectList = new List<AttLeave__c>();
		for (ValidPeriodEntity entity : entityList) {
			objectList.add(((AttLeaveEntity)entity).createSObject());
		}
		return objectList;
	}
}