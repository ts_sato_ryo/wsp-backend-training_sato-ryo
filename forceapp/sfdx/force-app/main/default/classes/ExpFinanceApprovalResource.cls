/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description 精算確定のAPIを提供するリソースクラス Resource class for finance approval function
 */
public with sharing class ExpFinanceApprovalResource {

	/** 一覧のソートキー */
	private static final Map<String, ExpReportRequestRepository.SortKey> LIST_SORT_KEY_MAP;
	static {
		LIST_SORT_KEY_MAP = new Map<String, ExpReportRequestRepository.SortKey> {
			'ReportNo' => ExpReportRequestRepository.SortKey.REPORT_NO,
			'Status' => ExpReportRequestRepository.SortKey.STATUS,
			'RequestDate' => ExpReportRequestRepository.SortKey.REQUEST_TIME,
			'Subject' => ExpReportRequestRepository.SortKey.SUBJECT,
			'TotalAmount' => ExpReportRequestRepository.SortKey.TOTAL_AMOUNT,
			'EmployeeCode' => ExpReportRequestRepository.SortKey.EMPLOYEE_CODE,
			'EmployeeName' => ExpReportRequestRepository.SortKey.EMPLOYEE_NAME,
			'DepartmentCode' => ExpReportRequestRepository.SortKey.DEPARTMENT_CODE,
			'DepartmentName' => ExpReportRequestRepository.SortKey.DEPARTMENT_NAME
		};
	}

	/** 一覧のソート順 */
	private static final Map<String, ExpReportRequestRepository.SortOrder> LIST_SORT_ORDER_MAP;
	static {
		LIST_SORT_ORDER_MAP = new Map<String, ExpReportRequestRepository.SortOrder> {
			'Asc' => ExpReportRequestRepository.SortOrder.ORDER_ASC,
			'Desc' => ExpReportRequestRepository.SortOrder.ORDER_DESC
		};
	}


	/** 経費申請の詳細ステータスとレスポンスパラメータ値のMap */
	private static final Map<ExpReportRequestEntity.DetailStatus, String> EXP_REQUEST_DETAIL_STATUS_MAP;
	static {
		EXP_REQUEST_DETAIL_STATUS_MAP = new Map<ExpReportRequestEntity.DetailStatus, String> {
			ExpReportRequestEntity.DetailStatus.APPROVED => 'Approved',
			ExpReportRequestEntity.DetailStatus.FULLY_PAID => 'Fully Paid',
			ExpReportRequestEntity.DetailStatus.ACCOUNTING_AUTHORIZED => 'AccountingAuthorized',
			ExpReportRequestEntity.DetailStatus.ACCOUNTING_REJECTED => 'AccountingRejected',
			ExpReportRequestEntity.DetailStatus.JOURNAL_CREATED => 'JournalCreated'
		};
	}

	/**
	 * @description 申請ID一覧データ取得条件を格納するクラス Parameter for getting request ID list
	 */
	public virtual class GetExpReportRequestIdListParam implements RemoteApi.RequestParam {
		/** ソートキー */
		public String sortBy;
		/** ソート順 */
		public String order;
		/**  search filter **/
		public ExpFinanceApprovalService.SearchFilter SearchFilter;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public virtual void validate() {
			// ソートキー
			if (String.isNotBlank(sortBy)) {
				if (!LIST_SORT_KEY_MAP.containsKey(sortBy)) {
					throw new App.ParameterException('sortBy', sortBy);
				}
			}
			// ソート順
			if (String.isNotBlank(order)) {
				if (!LIST_SORT_ORDER_MAP.containsKey(order)) {
					throw new App.ParameterException('order', order);
				}
			}
			// 検索条件のレポートId
			// if (String.isNotBlank(SearchFilter.reportId)) {
			// 	try {
			// 		Id.ValueOf(SearchFilter.reportId);
			// 	} catch (Exception e) {
			// 		throw new App.ParameterException('reportId', SearchFilter.reportId);
			// 	}
			// }
		}
	}

	/**
	 * @description 経費申請ID一覧データ取得結果を格納するクラス Result of getting request ID list
	 */
	public class GetExpReportRequestIdListResult implements RemoteApi.ResponseParam {
		/** 取得件数 */
		public Integer totalSize;
		/** 経費申請IDのリスト */
		public List<String> requestIdList;

		public GetExpReportRequestIdListResult() {
			totalSize = 0;
			requestIdList = new List<String>();
		}
	}

	/**
	 * @description 経費申請ID一覧データ取得APIを実装するクラス API to get request ID list
	 */
	public with sharing class GetExpReportRequestIdListApi extends RemoteApi.ResourceBase {

		/**
		 * @description 経費申請ID一覧データを取得する Get request ID list
		 * @param req リクエストパラメータ(GetExpReportRequestIdListParam)
		 * @return レスポンス(GetExpReportRequestIdListResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			GetExpReportRequestIdListParam param = (GetExpReportRequestIdListParam)req.getParam(GetExpReportRequestIdListParam.class);

			// パラメータ値の検証
			param.validate();

			// 社員情報を取得 Get employee
			EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());

			// 経費精算機能が有効か確認
			ExpCommonUtil.canUseExpense(employee);

			// 経費申請データを取得 Get ExpReportRequest list
			ExpReportRequestRepository.SortKey sortKey = LIST_SORT_KEY_MAP.get(param.sortBy);
			ExpReportRequestRepository.SortOrder sortOrder = LIST_SORT_ORDER_MAP.get(param.order);
			ExpFinanceApprovalService.RequestIdListResult result = new ExpFinanceApprovalService().getRequestIdList(employee.companyId, sortKey, sortOrder, param.SearchFilter);

			// レスポンスをセットする Create response
			GetExpReportRequestIdListResult res = new GetExpReportRequestIdListResult();
			if (result.requestIdList != null && !result.requestIdList.isEmpty()) {
				res.totalSize = result.totalSize;
				for (Id requestId : result.requestIdList) {
					res.requestIdList.add(requestId);
				}
			}

			return res;
		}
	}

	/**
	 * @description 申請一覧データ取得条件を格納するクラス Parameter for getting ExpReportRequest list
	 */
	public virtual class GetExpReportRequestListParam implements RemoteApi.RequestParam {
		/** 申請IDのリスト */
		public List<String> requestIds;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public virtual void validate() {
			// 申請IDのリスト
			ExpCommonUtil.validateIdList('requestIds', requestIds, true);
		}
	}

	/**
	 * @description 経費申請一覧データ取得結果を格納するクラス Result of getting ExpReportRequest list
	 */
	public class GetExpReportRequestListResult implements RemoteApi.ResponseParam {
		/** 経費申請のリスト */
		public List<ExpReportRequest> requestList;

		public GetExpReportRequestListResult() {
			requestList = new List<ExpReportRequest>();
		}
	}

	/**
	 * @description 経費申請一覧データ（申請情報）取得結果を格納するクラス
	 */
	public class ExpReportRequest {
		/** 申請ID */
		public String requestId;
		/** 申請番号 */
		public String reportNo;
		/* ステータス */
		public String status;
		/** 申請日 */
		public String requestDate;
		/** 件名 */
		public String subject;
		/** 金額 */
		public Decimal totalAmount;
		/** 社員コード */
		public String employeeCode;
		/** 社員名 */
		public String employeeName;
		/** 顔写真URL */
		public String photoUrl;
		/** 部署コード */
		public String departmentCode;
		/** 部署名 */
		public String departmentName;
		/** 領収書添付あり */
		public Boolean hasReceipts;

		/**
		 * コンストラクタ（ExpReportRequestEntityから作成) Constructor(create from entity)
		 */
		public ExpReportRequest(ExpReportRequestEntity entity) {
			requestId = entity.id;
			reportNo = entity.reportNo;
			status = EXP_REQUEST_DETAIL_STATUS_MAP.get(entity.getDetailStatus());
			requestDate = new AppDate(entity.requestTime.getDate()).format();
			subject = entity.subject;
			totalAmount = entity.totalAmount;
			employeeCode = entity.employee.code;
			employeeName = entity.employee.fullName.getFullName();
			photoUrl = entity.employee.photoUrl;
			departmentCode = entity.department.code;
			departmentName = AppMultiString.stringValue(entity.department.name);
			hasReceipts = entity.hasReceipts;
		}
	}

	/**
	 * @description 経費申請一覧データ取得APIを実装するクラス API to get ExpReportRequest list
	 */
	public with sharing class GetExpReportRequestListApi extends RemoteApi.ResourceBase {

		/**
		 * @description 経費申請一覧データを取得する Get ExpReportRequest list
		 * @param req リクエストパラメータ(GetExpReportRequestListParam)
		 * @return レスポンス(GetExpReportRequestListResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			GetExpReportRequestListParam param = (GetExpReportRequestListParam)req.getParam(GetExpReportRequestListParam.class);

			// パラメータ値の検証
			param.validate();

			// 経費精算機能が有効か確認
			ExpCommonUtil.canUseExpense();

			// 経費申請データを取得 Get ExpReportRequest list
			List<ExpReportRequestEntity> requestList = new ExpFinanceApprovalService().getRequestList(param.requestIds);

			// レスポンスをセットする Create response
			GetExpReportRequestListResult res = new GetExpReportRequestListResult();
			if (requestList != null && !requestList.isEmpty()) {
				res.requestList = new List<ExpReportRequest>();
				for (ExpReportRequestEntity request : requestList) {
					res.requestList.add(new ExpReportRequest(request));
				}
			}

			return res;
		}
	}

	/*
	 * API for saving the ExpReport
	 */
	public with sharing class SaveExpReportApi extends RemoteApi.ResourceBase {

		/**
		 * @description Save Expense Report data (without the Record) and the modification history
		 * @param  req Request Parameter (SaveExpReportParam)
		 * @return Response (SaveExpReportResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			Map<String, Object> paramMap = req.getParamMap();
			paramMap.put('isFinanceApproval', true);
			RemoteApi.Request wrappedReq = new RemoteApi.Request(req.getPath(), paramMap);

			ExpResource.SaveExpReportApi api = new ExpResource.SaveExpReportApi();
			ExpResource.SaveExpReportResult res = (ExpResource.SaveExpReportResult)api.execute(wrappedReq);

			return res;
		}
	}

	/*
	 * API for saving the Exp Record
	 */
	public with sharing class SaveExpRecordApi extends RemoteApi.ResourceBase {

		/**
		 * @description Save Expense Report Record data and the modification history
		 * @param req Request Parameter (SaveExpRecordParam)
		 * @return Response (SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			Map<String, Object> paramMap = req.getParamMap();
			paramMap.put('isFinanceApproval', true);
			RemoteApi.Request wrappedReq = new RemoteApi.Request(req.getPath(), paramMap);

			ExpRecordResource.SaveExpRecordApi api = new ExpRecordResource.SaveExpRecordApi();
			ExpRecordResource.SaveResult res = (ExpRecordResource.SaveResult) api.execute(wrappedReq);

			return res;
		}
	}


	/**
	 * @description 経費申請詳細データ取得条件を格納するクラス  Parameter for getting ExpReportRequest detail
	 */
	public class GetExpReportRequestParam implements RemoteApi.RequestParam {
		/** 申請ID */
		public String requestId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 申請ID
			ExpCommonUtil.validateId('requestId', requestId, true);
		}
	}

	/**
	 * @description 経費申請詳細データ取得APIを実装するクラス API to get ExpReportRequest detail
	 */
	public with sharing class GetExpReportRequestApi extends RemoteApi.ResourceBase {

		/**
		 * @description 経費申請詳細データを取得する Get ExpReportRequest detail
		 * @param req リクエストパラメータ(GetExpReportRequestParam)
		 * @return レスポンス(ExpResource.GetExpReportResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			GetExpReportRequestParam param = (GetExpReportRequestParam)req.getParam(GetExpReportRequestParam.class);

			// パラメータ値の検証
			param.validate();

			//経費精算機能が有効か確認
			ExpCommonUtil.canUseExpense();

			ExpFinanceApprovalService service = new ExpFinanceApprovalService();
			// 経費精算データを取得 Get ExpReportRequest detail
			ExpReportEntity report = service.getExpReport(param.requestId);
			// 申請ステータスを取得 Get request status
			ExpReportRequestEntity.DetailStatus status = service.getRequestStatus(param.requestId);

			// レスポンスをセットする Create response
			ExpResource.GetExpReportResult res = ExpResource.createGetResponseFromEntity(report);
			res.status = EXP_REQUEST_DETAIL_STATUS_MAP.get(status);

			return res;
		}
	}

	/*
	 * Request parameter class for the modification history GET API
	 */
	public class GetExpModificationHistoryParam implements RemoteApi.RequestParam {
		public Id requestId;
	}

	/*
	 * Modification history object class
	 */
	public class ExpModificationHistory {
		/** Request ID */
		public String requestId;
		/** Report ID */
		public String reportId;
		/* Record ID */
		public String recordId;
		/** Record Item ID */
		public String recordItemId;
		/** Record Summary */
		public String recordSummary;
		/** Field Name */
		public String fieldName;
		/** Old Value */
		public String oldValue;
		/** New Value */
		public String newValue;
		/** Employee Name */
		public String modifiedByEmployeeName;
		/** Modified Date Time */
		public String modifiedDateTime;

	}

	/**
   * Response parameter class for the modification history GET API
   */
	public class GetExpModificationHistoryResult implements RemoteApi.ResponseParam {
		/* Modification history */
		public List<ExpModificationHistory> modificationList;

		public GetExpModificationHistoryResult(List<ExpModificationHistoryEntity> entityList) {
			modificationList = new List<ExpModificationHistory>();
			modificationList = convertEntityToResponse(entityList);
		}

		/*
		 * Convert modification history entity to response
		 * @param entityList Entity list to convert
		 * @return List of response objects
		 */
		public List<ExpModificationHistory> convertEntityToResponse(List<ExpModificationHistoryEntity> entityList) {
			List<ExpModificationHistory> result = new List<ExpModificationHistory>();

			for (ExpModificationHistoryEntity entity : entityList) {
				ExpModificationHistory response = new ExpModificationHistory();

				response.requestId = entity.expRequestId;
				response.reportId = entity.expReportId;
				response.recordId = entity.expRecordId;
				response.recordItemId = entity.expRecordItemId;
				response.recordSummary = entity.recordSummary;
				response.fieldName = entity.fieldLabel;
				response.oldValue = entity.oldValue;
				response.newValue = entity.newValue;
				response.modifiedByEmployeeName = entity.modifiedByEmployeeName.getFullName();
				response.modifiedDateTime = entity.modifiedDateTime.getDatetime().format('YYYY-MM-dd\' \'HH:mm');

				result.add(response);
			}
			return result;
		}
	}

	/*
	 * API to retrieve modification history of expense report
	 */
	public with sharing class GetExpReportModificationHistoryApi extends RemoteApi.ResourceBase {

		/*
		 * @param req (GetExpModificationHistoryParam) Expense request ID
		 * @return (GetExpModificationHistoryResult) List of modification history of the report
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			GetExpModificationHistoryParam param = (GetExpModificationHistoryParam)req.getParam(GetExpModificationHistoryParam.class);

			// Check if Expense module is available
			ExpCommonUtil.canUseExpense();

			// Retrieve modification history
			List<ExpModificationHistoryEntity> modificationList = new ExpFinanceApprovalService().getModificationHistory(param.requestId);
			// Create response from the entity list
			GetExpModificationHistoryResult res = new GetExpModificationHistoryResult(modificationList);

			return res;
		}
	}


	/**
	 * @description 承認用のパラメータを格納するクラス Parameter for approval
	 */
	public class ApproveRequestParam implements RemoteApi.RequestParam {
		/** 申請IDのリスト */
		public List<String> requestIds;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 申請IDのリスト
			ExpCommonUtil.validateIdList('requestIds', requestIds, true);
		}
	}

	/**
	 * @description 承認APIを実装するクラス API to approve ExpReportRequest
	 */
	public with sharing class ApproveRequestApi extends RemoteApi.ResourceBase {

		/**
		 * @description 申請を承認する Approve ExpReportRequest
		 * @param req リクエストパラメータ(ApproveRequestParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			ApproveRequestParam param = (ApproveRequestParam)req.getParam(ApproveRequestParam.class);

			// パラメータ値の検証
			param.validate();

			// 承認実行
			ExpFinanceApprovalService service = new ExpFinanceApprovalService();
			service.approve(param.requestIds);

			return null;
		}
	}


	/**
	 * @description 却下用のパラメータを格納するクラス Parameter for reject
	 */
	public class RejectRequestParam implements RemoteApi.RequestParam {
		/** 申請IDのリスト */
		public List<String> requestIds;
		/** コメント */
		public String comment;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 申請IDのリスト
			ExpCommonUtil.validateIdList('requestIds', requestIds, true);
		}
	}

	/**
	 * @description 却下APIを実装するクラス API to approve ExpReportRequest
	 */
	public with sharing class RejectRequestApi extends RemoteApi.ResourceBase {

		/**
		 * @description 申請を却下する Reject ExpReportRequest
		 * @param req リクエストパラメータ(RejectRequestParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			RejectRequestParam param = (RejectRequestParam)req.getParam(RejectRequestParam.class);

			// パラメータ値の検証
			param.validate();

			// 却下実行
			ExpFinanceApprovalService service = new ExpFinanceApprovalService();
			service.reject(param.requestIds, param.comment);

			return null;
		}
	}
}