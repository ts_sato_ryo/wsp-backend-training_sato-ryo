/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description 外部カレンダー連携のテスト用モッククラス
*/
@isTest
public class Office365CalloutMock implements HttpCalloutMock {

	public List<Map<String, String>> responseList;

	// コンストラクタ
	public Office365CalloutMock(List<Map<String, String>> responseList) {
		this.responseList = responseList;
	}

	public HTTPResponse respond(HTTPRequest req) {

		String endpoint = req.getEndpoint();
		String method = req.getMethod();

		for (Map<String, String> resMap : this.responseList) {

			// HTTPメソッドとURLが一致する場合のみ、レスポンス作成
			if (method.equalsIgnoreCase(resMap.get('method'))
			 && endpoint.startsWithIgnoreCase(resMap.get('urlprefix'))) {

				// 例外作成
				if (resMap.get('exception') != null) {
					String errorMessage = (String)resMap.get('exception');
					System.CalloutException ce = new System.CalloutException();
					ce.setMessage(errorMessage);
					throw ce;
				}

				// レスポンス作成
				HttpResponse res = new HttpResponse();
				res.setHeader('Content-Type', 'application/json');
				res.setBody(resMap.get('body'));
				res.setStatus(resMap.get('status'));
				res.setStatusCode(Integer.valueOf(resMap.get('statuscode')));

				System.debug(res);
				return res;
			}
		}
		return null;
	}
}