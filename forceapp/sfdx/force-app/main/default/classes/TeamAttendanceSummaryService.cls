/**
 * @group 共通
 *
 * @description チーム機能で利用する勤怠サマリーのサービスを統合するサービスクラス。
 */
public with sharing class TeamAttendanceSummaryService implements Service {
	// TODO: 共通化する
	/** 勤務確定申請ステータスとレスポンスパラメータ値のMap */
	@TestVisible
	private static final Map<AttRequestEntity.DetailStatus, String> FIX_REQUEST_STATUS_MAP;
	static {
		FIX_REQUEST_STATUS_MAP = new Map<AttRequestEntity.DetailStatus, String> {
		AttRequestEntity.DetailStatus.NOT_REQUESTED => 'NotRequested',
		AttRequestEntity.DetailStatus.PENDING => 'Pending',
		AttRequestEntity.DetailStatus.APPROVED => 'Approved',
		AttRequestEntity.DetailStatus.REJECTED => 'Rejected',
		AttRequestEntity.DetailStatus.REMOVED => 'Removed',
		AttRequestEntity.DetailStatus.CANCELED => 'Canceled'};
	}
	/** 勤務確定申請ステータスと配列値のMap */
	// 本当はSummaryRequestRecordのサブクラスにstaticで持たせるべきだがサブクラス内はstaticにできないのでここに書く
	@TestVisible
	private static final Map<AttRequestEntity.DetailStatus, Integer> FIX_REQUEST_STATUS_ORDER_MAP;
	static {
		FIX_REQUEST_STATUS_ORDER_MAP = new Map<AttRequestEntity.DetailStatus, Integer> {
		AttRequestEntity.DetailStatus.NOT_REQUESTED => 1,
		AttRequestEntity.DetailStatus.REMOVED => 2,
		AttRequestEntity.DetailStatus.REJECTED => 3,
		AttRequestEntity.DetailStatus.CANCELED => 4,
		AttRequestEntity.DetailStatus.PENDING => 5,
		AttRequestEntity.DetailStatus.APPROVED => 6};
	}

	/**
	 * @description 各社員の月度勤務確定状況データを検索する
	 * @param searchFilter 検索条件
	 * @return 各社員の月度勤務確定状況データ
	 */
	public List<SummaryRequestRecord> searchSummaryRequestRecord(SummaryRequestSearchFilter searchFilter) {
		// 1. 初期化する
		List<SummaryRequestRecord> returnSummaryList = new List<SummaryRequestRecord>();
		AttSummaryRepository summaryRep = new AttSummaryRepository();

		// 2. 対象年・対象月度から最大対象期間を取得する
		AppDate monthlyBaseDate = AppDate.newInstance(
				searchFilter.targetYear,
				searchFilter.targetMonthly,
				1);
		AppDate targetTermStartDate = monthlyBaseDate.addMonths(2).addDays(-1);
		AppDate targetTermEndDate = monthlyBaseDate.addMonths(-1);

		// 3. 社員をキーにした勤怠サマリリストのマップを初期化
		Map<Id, List<SummaryRequestRecord>> empSummaryMap = new Map<Id, List<SummaryRequestRecord>>();

		// 4. 部署の履歴IDと最大対象期間から勤怠サマリーを取得する
		List<Id> departmentIdList;
		if (searchFilter.isSearchEmployeeWithoutDepartment) {
			departmentIdList = new List<Id>{null};
		} else {
			departmentIdList = searchFilter.departmentIdList;
		}
		List<AttSummaryEntity> summaryList = summaryRep.searchEntity(targetTermStartDate, targetTermEndDate, departmentIdList);

		// 5. 対象月度確定に必要な勤務体系ベースIDと社員IDを取得する
		Set<Id> workingTypeIdSet = new Set<Id>();
		Set<Id> empBaseIdSet= new Set<Id>();
		for (AttSummaryEntity summary : summaryList) {
			workingTypeIdSet.add(summary.workingTypeBaseId);
			empBaseIdSet.add(summary.employeeBaseId);
		}

		// 6. 対象部署にいる社員を取得する
		Map<Id, EmployeeBaseEntity> empMap = new Map<Id, EmployeeBaseEntity>();
		empMap.putAll(getEmployeeMap(empBaseIdSet, null, null, false));

		// 7.社員の個人設定を取得する
		Map<Id, PersonalSettingEntity> personalSettingMap = getPersonalSettingMap(new List<Id>(empMap.keySet()));

		// 8. 対象社員から勤務体系を（できれば集約して）取得する
		Map<Id, AttWorkingTypeBaseEntity> workingTypeMap = getWorkingTypeMap(workingTypeIdSet, false);

		// 9. 会社はログインユーザーから取得する
		Map<Id, EmployeeBaseEntity> loginUserEmpMap = new Map<Id, EmployeeBaseEntity>();
		loginUserEmpMap.putAll(getUserEmployeeMap(null, null, new Set<Id>{userInfo.getUserId()}, true));
		Id loginEmpCompanyId = loginUserEmpMap.get(userInfo.getUserId()) == null ? null : loginUserEmpMap.get(userInfo.getUserId()).companyId;

		// 10.各社員の月度勤務確定状況データに必要なマスタ情報をセットする
		Set<Id> approverEmpIdSet = new Set<Id>();
		Set<Id> approverUserIdSet = new Set<Id>();
		Set<Id> workingRequestIdSet = new Set<Id>();
		Set<Id> processedRequestIdSet = new Set<Id>();
		for (AttSummaryEntity summary : summaryList) {
			if (empSummaryMap.get(summary.employeeBaseId) == null) {
				empSummaryMap.put(summary.employeeBaseId, new List<SummaryRequestRecord>());
			}
			SummaryRequestRecord tmpSummaryReq =new SummaryRequestRecord(
					summary,
					empMap.get(summary.employeeBaseId),
					workingTypeMap.get(summary.workingTypeBaseId),
					personalSettingMap.get(summary.employeeBaseId));
			// 11. 対象月度リスト取得 -  勤務体系の表記設定から対象月度を判定し、対象リストを取得する
			if (! tmpSummaryReq.isExpiredEmployee(searchFilter.targetYear, searchFilter.targetMonthly)) {
				if (tmpSummaryReq.employee.companyId == loginEmpCompanyId) {
					if (tmpSummaryReq.isTargetMonthly(searchFilter.targetYear, searchFilter.targetMonthly)) {
						empSummaryMap.get(summary.employeeBaseId).add(tmpSummaryReq);
					}
				}
			}

			// 12. 承認者情報取得に必要な情報を取得する
			// 承認者情報取得に必要な情報を申請ステータスで判定
			// 未申請・申請取消の場合はマスタにセットされた承認者IDから承認者情報を取得する
			if (tmpSummaryReq.statusValue == AttRequestEntity.DetailStatus.NOT_REQUESTED ||
				 tmpSummaryReq.statusValue == AttRequestEntity.DetailStatus.REMOVED) {
				// 両方取得しないと申請者選択時と同じ挙動にならない
				EmployeeHistoryEntity empHistory = tmpSummaryReq.employee.getHistoryByDate(summary.endDate);
				if (empHistory != null) {
					approverEmpIdSet.add(empHistory.approverBase01Id);
				}
				if (tmpSummaryReq.pSetting != null) {
					approverEmpIdSet.add(tmpSummaryReq.pSetting.approverBase01Id);
				}
			// 申請中の場合は申請中のプロセスインスタンスの申請IDから承認者情報を取得する
			} else if (tmpSummaryReq.statusValue == AttRequestEntity.DetailStatus.PENDING) {
				workingRequestIdSet.add(tmpSummaryReq.summary.requestId);
			// 却下・承認済・承認取消の場合は申請プロセスが終了しているプロセスインスタンスの申請IDから承認者情報を取得する
			} else {
				processedRequestIdSet.add(tmpSummaryReq.summary.requestId);
			}
		}

		// 13. SFの承認プロセスから承認者を取得する
		Map<Id, ApprovalProcessWorkitemEntity> workItemMap = getApprovalWorkItemMap(workingRequestIdSet);
		Map<Id, ApprovalProcessEntity> apprMap = getApprovalMap(processedRequestIdSet);

		// 14. 承認済・却下したアクターはuserIdしか取得していないかつNameがSFの名称のでユーザーIdをセットして社員マスタを取得する
		for (Id id : workItemMap.keySet()) {
			approverUserIdSet.add(workItemMap.get(id).actorId);
		}
		for (Id id : apprMap.keySet()) {
			approverUserIdSet.add(apprMap.get(id).lastActorId);
		}

		// 15. 次の承認者を取得する
		empMap.putAll(getEmployeeMap(approverEmpIdSet, null, null, false));
		Map<Id, EmployeeBaseEntity> userEmpMap = new Map<Id, EmployeeBaseEntity>();
		userEmpMap.putAll(getUserEmployeeMap(null, null, approverUserIdSet, false));

		for (Id empBaseId : empSummaryMap.keySet()) {
			List<SummaryRequestRecord> sumReqList = empSummaryMap.get(empBaseId);
			System.debug(LoggingLevel.DEBUG, 'sumReqList=' + sumReqList.size());
			for (SummaryRequestRecord summaryReq : sumReqList) {
				// 16. 承認プロセス取得 - 勤怠サマリーのリクエストIdから次の承認者名を取得する
				summaryReq.approverName = summaryReq.getNextApproverName(empMap, userEmpMap, workItemMap, apprMap);

				System.debug(LoggingLevel.DEBUG, 'returnSummaryListTarget=' + summaryReq);
				returnSummaryList.add(summaryReq);
			}
		}

		// 17. 取得対象リストソート - 勤務確定申請ステータス＞締め日＞社員コード
		returnSummaryList.sort();
		return returnSummaryList;
	}

	/**
	 * @description 勤務体系ベースIDをキーにした勤務体系マスタのMapを取得します
	 *
	 * @param  workingTypeBaseIdSet 検索条件となる勤務体系ベースIDのリスト
	 * @param  includeRemoved trueの場合削除済の履歴を含める
	 * @return 勤務体系ベースIDをキーにした勤務体系マスタのMap
	 */
	@TestVisible
	private Map<Id, AttWorkingTypeBaseEntity> getWorkingTypeMap(Set<Id> workingTypeBaseIdSet, Boolean includeRemoved) {
		// 初期化
		Map<Id, AttWorkingTypeBaseEntity> returnWorkingTypeMap = new Map<Id, AttWorkingTypeBaseEntity>();
		AttWorkingTypeRepository workingTypeRep = new AttWorkingTypeRepository();

		// 検索条件設定
		AttWorkingTypeRepository.SearchFilter workingTypeFilter = new AttWorkingTypeRepository.SearchFilter();
		workingTypeFilter.includeRemoved = includeRemoved;
		if (workingTypeBaseIdSet != null) {
			workingTypeFilter.baseIds = workingTypeBaseIdSet;
		}

		// 検索実行
		List<AttWorkingTypeBaseEntity> workingTypeList = workingTypeRep.searchEntity(workingTypeFilter);

		// 検索結果をMapにセットする
		if (workingTypeList != null && !workingTypeList.isEmpty()){
			for (AttWorkingTypeBaseEntity workingType : workingTypeList) {
				returnWorkingTypeMap.put(workingType.Id, workingType);
			}
		}
		return returnWorkingTypeMap;
	}

	/**
	 * @description 社員ベースIDをキーにした社員マスタのMapを取得します
	 *
	 * @param  empBaseIdSet 検索条件となる社員ベースIDのリスト
	 * @param  deptBaseIdSet 検索条件となる部署ベースIDのリスト
	 * @param  includeRemoved trueの場合削除済の履歴を含める
	 * @return 社員ベースIDをキーにした社員マスタのMap
	 */
	private Map<Id, EmployeeBaseEntity> getEmployeeMap(Set<Id> empBaseIdSet, Set<Id> deptBaseIdSet, Set<Id> userIdSet, Boolean includeRemoved) {
		// 初期化
		Map<Id, EmployeeBaseEntity> returnEmpMap = new Map<Id, EmployeeBaseEntity>();
		EmployeeRepository empRep = new EmployeeRepository();
		// 検索条件設定
		EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
		empFilter.includeRemoved = includeRemoved;
		if (empBaseIdSet != null && !empBaseIdSet.isEmpty()) {
			empFilter.baseIds = empBaseIdSet;
		}
		if (deptBaseIdSet != null && !deptBaseIdSet.isEmpty()) {
			empFilter.departmentIds = deptBaseIdSet;
		}
		if (userIdSet != null && !userIdSet.isEmpty()) {
			empFilter.userIds = userIdSet;
		}

		// 検索実行
		List<EmployeeBaseEntity> empList = empRep.searchEntity(empFilter);

		// 検索結果をMapにセットする
		if (empList != null && !empList.isEmpty()) {
			for (EmployeeBaseEntity emp : empList) {
				returnEmpMap.put(emp.Id, emp);
			}
		}
		return returnEmpMap;
	}

	/**
	 * @description ユーザーIDをキーにした社員マスタのMapを取得します
	 *
	 * @param  empBaseIdSet 検索条件となる社員ベースIDのリスト
	 * @param  deptBaseIdSet 検索条件となる部署ベースIDのリスト
	 * @param  includeRemoved trueの場合削除済の履歴を含める
	 */
	private Map<Id, EmployeeBaseEntity> getUserEmployeeMap(Set<Id> empBaseIdSet, Set<Id> deptBaseIdSet, Set<Id> userIdSet, Boolean includeRemoved) {
		// 初期化
		Map<Id, EmployeeBaseEntity> returnEmpMap = new Map<Id, EmployeeBaseEntity>();
		EmployeeRepository empRep = new EmployeeRepository();
		// 検索条件設定
		EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
		empFilter.includeRemoved = includeRemoved;
		if (empBaseIdSet != null && !empBaseIdSet.isEmpty()) {
			empFilter.baseIds = empBaseIdSet;
		}
		if (deptBaseIdSet != null && !deptBaseIdSet.isEmpty()) {
			empFilter.departmentIds = deptBaseIdSet;
		}
		if (userIdSet != null && !userIdSet.isEmpty()) {
			empFilter.userIds = userIdSet;
		}

		// 検索実行
		List<EmployeeBaseEntity> empList = empRep.searchEntity(empFilter);

		// 検索結果をMapにセットする
		if (empList != null && !empList.isEmpty()) {
			for (EmployeeBaseEntity emp : empList) {
				returnEmpMap.put(emp.userId, emp);
			}
		}
		return returnEmpMap;
	}

	/**
	 * @description 勤務体系ベースIDをキーにした社員マスタのMapを取得します
	 *
	 * @param  empBaseIdSet 検索条件となる社員ベースIDのリスト
	 */
	private Map<Id, PersonalSettingEntity> getPersonalSettingMap(List<Id> empBaseIdList) {
		// 初期化
		Map<Id, PersonalSettingEntity> returnPSMap = new Map<Id, PersonalSettingEntity>();
		PersonalSettingRepository personalStgRepo = new PersonalSettingRepository();

		// 検索実行
		List<PersonalSettingEntity> psList = personalStgRepo.getPersonalSettingList(empBaseIdList);

		// 検索結果をMapにセットする
		if (psList != null && !psList.isEmpty()) {
			for (PersonalSettingEntity ps : psList) {
				returnPSMap.put(ps.employeeBaseId, ps);
			}
		}
		return returnPSMap;
	}
	/**
	 * @description 申請IDをキーにしたプロセスインスタンス履歴のエンティティのMapを取得します
	 * @param  requestIdSet 申請中承認のリスト
	 * @return 申請IDをキーにしたプロセスインスタンス履歴のエンティティのMap
	 */
	private Map<Id, ApprovalProcessWorkitemEntity> getApprovalWorkItemMap(Set<Id> requestIdSet) {
		// 初期化
		Map<Id, ApprovalProcessWorkitemEntity> returnWorkItemMap = new Map<Id, ApprovalProcessWorkitemEntity>();
		ApprovalProcessWorkitemRepository apprWorkRepo = new ApprovalProcessWorkitemRepository();
		List<ApprovalProcessWorkitemEntity> apprWorkList = apprWorkRepo.getEntityListByRequestId(new List<Id>(requestIdSet));
		// 検索結果をMapにセットする
		if (apprWorkList != null && !apprWorkList.isEmpty()) {
			for (ApprovalProcessWorkitemEntity apprWork : apprWorkList) {
				returnWorkItemMap.put(apprWork.targetObjectId, apprWork);
			}
		}
		return returnWorkItemMap;
	}

	/**
	 * @description 申請IDをキーにしたプロセスインスタンス履歴のエンティティのMapを取得します
	 * @param  requestIdSet 申請中承認のリスト
	 * @return 申請IDをキーにしたプロセスインスタンス履歴のエンティティのMap
	 */
	private Map<Id, ApprovalProcessEntity> getApprovalMap(Set<Id> requestIdSet) {
		// 初期化
		ApprovalProcessRepository apprRepo = new ApprovalProcessRepository();
		Map<Id, ApprovalProcessEntity> returnMap = new Map<Id, ApprovalProcessEntity>();
		List<ApprovalProcessEntity> apprList = apprRepo.getRequestList(new List<Id>(requestIdSet));

		// 検索結果をMapにセットする
		if (apprList != null && !apprList.isEmpty()) {
			for (ApprovalProcessEntity appr : apprList) {
				// 作成日時の降順で取得するので、最新の申請情報のみ格納する
				if (!returnMap.containsKey(appr.targetObjectId)) {
					returnMap.put(appr.targetObjectId, appr);
				}
			}
		}
		return returnMap;
	}

	/**
	 * @description 各社員の月度勤務確定状況データを検索する条件
	 */
	public class SummaryRequestSearchFilter {
		/** 対象年 */
		public Integer targetYear;
		/** 対象年度 */
		public Integer targetMonthly;
		/** 部署ベースIDリスト */
		public List<Id> departmentIdList;
		/** trueの場合、部門なしの社員の検索をする departmentIdListは無効*/
		public Boolean isSearchEmployeeWithoutDepartment;
	}

	/**
	 * @description 各社員の月度勤務確定状況データを表すオブジェクト
	 */
	public class SummaryRequestRecord implements Comparable {
		/** 勤怠サマリー */
		public AttSummaryEntity summary {get; private set;}
		/** 勤務社員 */
		public EmployeeBaseEntity employee {get; private set;}
		/** 勤務表社員の個人設定 */
		public PersonalSettingEntity pSetting {get; set;}
		/** 勤務社員の勤務体系マスタ */
		public AttWorkingTypeBaseEntity employeeWorkingType {get; private set;}
		/** (参照用)月度の表記 */
		public AttWorkingTypeBaseGeneratedEntity.MarkType monthMark {
			get {
				return employeeWorkingType.monthMark;
			}
			private set;
		}
		/** 次の承認者 */
		public EmployeeBaseEntity nextApprover {get; set;}
		/** (参照用)社員ID */
		public Id employeeId {
			get {
				return summary.employeeBaseId;
			}
			private set;
		}
		/** (参照用)社員コード */
		public String employeeCode {
			get {
				return summary.employeeCode;
			}
			private set;
		}
		/** (参照用)社員名 */
		public String employeeName {
			get {
				return summary.employeeName;
			}
			private set;
		}
		/** (参照用)社員の顔写真URL */
		public String photoUrl {
			get {
				return employee.user.photoUrl;
			}
			private set;
		}
		/** (参照用)勤務体系ID */
		public Id workingTypeBaseId {
			get {
				return summary.workingTypeBaseId;
			}
			private set;
		}
		/** (参照用)勤務体系名 */
		public String workingTypeName {
			get {
				return this.workingTypeHistory != null ? workingTypeHistory.nameL.getValue() : '';
			}
			private set;
		}
		/** (参照用)月度開始日 */
		public AppDate startDate {
			get {
				return summary.startDate;
			}
			private set;
		}
		/** (参照用)月度終了日 */
		public AppDate endDate {
			get {
				return summary.endDate;
			}
			private set;
		}
		/** (参照用)勤務確定申請のステータス名 *月度のみ対応。申請前の場合は未申請のステータスが戻り値 */
		public String status {
			get {
				return FIX_REQUEST_STATUS_MAP.get(this.statusValue);
			}
			private set;
		}
		/** (参照用)勤務確定申請のステータス *月度のみ対応。申請前の場合は未申請のステータスが戻り値 */
		public AttRequestEntity.DetailStatus statusValue {
			get {
				//ステータスがない場合は未申請を返す
				return summary.getRequestDetailStatus();
			}
			private set;
		}
		/** 勤怠サマリー終了日時点で有効な勤務体系履歴 */
		private AttWorkingTypeHistoryEntity workingTypeHistory {
			get {
				return this.employeeWorkingType.getHistoryByDate(summary.endDate);
			}
		}
		/** 勤怠サマリー終了日時点で有効な社員履歴 */
		private EmployeeHistoryEntity employeeHistory {
			get {
				return this.employee.getHistoryByDate(summary.endDate);
			}
		}

		/** (参照用)勤務確定申請の承認者名 *月度のみ対応。今のところ要件がないので承認者IDは保持しない */
		public String approverName { get; set; }
		/**
		 * インスタンス
		 * トランザクションのAttSummaryEntity優位で生成される
		 * @param  summary 勤怠サマリー
		 * @param  employee 勤務社員
		 * @param  employeeWorkingType　勤務社員の勤務体系
		 * @param  pSetting 勤務社員の個人設定
		 * @throws App.RecordNotFoundException // 勤怠サマリで社員マスタ・勤務体系マスタが必須なので、マスタが存在しなかった場合はインスタンス生成時エラー
		 */
		public SummaryRequestRecord(AttSummaryEntity summary, EmployeeBaseEntity employee,
				AttWorkingTypeBaseEntity employeeWorkingType, PersonalSettingEntity pSetting) {
			if (summary == null || employee == null || employeeWorkingType == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			this.summary = summary;
			this.employee = employee;
			this.employeeWorkingType = employeeWorkingType;
			this.pSetting = pSetting;
		}
		/**
		* インスタンスとパラメータの比較結果をIntegerで返却する
		* ステータス昇順＞締め日昇順＞社員コード昇順
		* @param 比較対象
		* @return 比較結果（このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満）
		*/
		public Integer compareTo(Object compareTo) {
			// 申請ステータスでの判定
			Integer diffStatusOrder = FIX_REQUEST_STATUS_ORDER_MAP.get(this.statusValue)
					- FIX_REQUEST_STATUS_ORDER_MAP.get(((SummaryRequestRecord)compareTo).statusValue);
			if (diffStatusOrder < 0) {// 比較対象よりもステータスの優先度が高い
				return App.COMPARE_BEFORE;
			} else if (diffStatusOrder > 0) {// 比較対象よりもステータスの優先度が低い
				return App.COMPARE_AFTER;
			}

			// 締め日での判定
			Integer cpEndResult = this.endDate.compareTo(((SummaryRequestRecord)compareTo).endDate);
			if (cpEndResult != App.COMPARE_EQUAL) {
				return cpEndResult;
			}

			// 社員コードでの判定
			Integer cpEmpCodeResult = this.employeeCode.compareTo(((SummaryRequestRecord)compareTo).employeeCode);
			return cpEmpCodeResult;
		}

		/**
		 * @description 勤務体系の設定から月度を判定する基準日を取得し、対象月度と同月であれば、対象とする
		 * @param  targetYear 対象年。年度ではない。
		 * @param  targetMonthly 対象月度
		 * @return 以下の場合にtrueになる
		 * 1. 勤務体系の月度の表記が開始日基準かつ勤怠サマリーの開始日が対象年・対象月度と一致する
		 * 2. 勤務体系の月度の表記が終了日基準かつ勤怠サマリーの終了日が対象年・対象月度と一致する
		 */
		public Boolean isTargetMonthly(Integer targetYear, Integer targetMonthly) {
			Boolean isTargetYear = false;
			Boolean isTargetMonthly = false;
			if (this.monthMark != null) {
				// 同月度判定
				// 勤務体系の月度の表記から月度を判定する基準日を取得
				AppDate monthlyStandardDate;
				if (this.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase) {
					monthlyStandardDate = this.startDate;
				} else if (this.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase) {
					monthlyStandardDate = this.endDate;
				}
				// 対象月度と同月であれば、対象とする
				isTargetMonthly = (monthlyStandardDate.month() == targetMonthly);

				// 同年判定
				// 年度ではなく年なので新しく基準日を取得する必要はない
				isTargetYear = (monthlyStandardDate.year() == targetYear);
			}
			return isTargetYear && isTargetMonthly;
		}

		/**
		 * @description 社員が退職済しているかどうか
		 * @param  targetYear 対象年。年度ではない。
		 * @param  targetMonthly 対象月度
		 * @return 以下の場合にtrueになる
		 * 該当月以前に退職済である
		 */
		public Boolean isExpiredEmployee(Integer targetYear, Integer targetMonthly) {
			AppDate expiredStandardDate;
			if (this.monthMark != null) {
				// 同月度判定
				// 勤務体系の月度の表記から月度を判定する基準日を取得
				if (this.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase) {
					expiredStandardDate = this.startDate;
				} else if (this.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase) {
					expiredStandardDate = this.endDate;
					expiredStandardDate = expiredStandardDate.addMonths(-1).addDays(1);
				}
			}
			AppDate expiredDate = this.employee.getLatestSuperHistory().validTo.addDays(-1);
			return expiredDate.isBefore(expiredStandardDate);
		}

		/**
		 * @description 承認者名を取得する
		 * @param  empMap 社員IDをキーにした社員マスタ
		 * @param  userEmpMap ユーザーIDをキーにした社員マスタ
		 * @param  workItemMap 申請IDをキーにした申請中申請のプロセスインスタンス
		 * @param  apprMap 申請IDをキーにした承認済・却下申請のプロセスインスタンス
		 * @return 承認者名、ただし承認者が見つからなかった場合は空文字を返却
		 */
		public String getNextApproverName(Map<Id, EmployeeBaseEntity> empMap, Map<Id, EmployeeBaseEntity> userEmpMap,
				Map<Id, ApprovalProcessWorkitemEntity> workItemMap, Map<Id, ApprovalProcessEntity> apprMap) {
			AttRequestEntity.DetailStatus requestDetailStatus = summary.getRequestDetailStatus();

			// 未申請・申請取消の場合
			if (requestDetailStatus == AttRequestEntity.DetailStatus.NOT_REQUESTED
					|| requestDetailStatus == AttRequestEntity.DetailStatus.REMOVED) {

				// 承認者選択が有効で、個人設定に有効な承認者が設定されている場合は個人設定の承認者を返却
				if (this.workingTypeHistory != null && this.workingTypeHistory.allowToChangeApproverSelf
						&& pSetting != null && pSetting.approverBase01Id != null) {
					EmployeeBaseEntity approveEmpBase = empMap.get(pSetting.approverBase01Id);
					if (approveEmpBase.isValid(summary.endDate)) {
						return approveEmpBase.fullNameL.getFullName();
					}
				}

				// 社員に有効な承認者が設定されている場合は、社員の承認者を返却
				if (this.employeeHistory != null && this.employeeHistory.ApproverBase01Id != null) {
					EmployeeBaseEntity approveEmpBase = empMap.get(this.employeeHistory.approverBase01Id);
					if (approveEmpBase.isValid(summary.endDate)) {
						return approveEmpBase.fullNameL.getFullName();
					}
				}

			// 申請中の場合
			} else if (requestDetailStatus == AttRequestEntity.DetailStatus.PENDING) {
				// 承認プロセスの承認者を返却
				Id actorId = workItemMap.containsKey(summary.requestId) ? workItemMap.get(summary.requestId).actorId : null;
				if (actorId != null && userEmpMap.containsKey(actorId)) {
					return userEmpMap.get(actorId).fullNameL.getFullName();
				}

			// 却下・承認済・承認取消の場合
			} else {
				Id actorId = apprMap.containsKey(summary.requestId) ? apprMap.get(summary.requestId).lastActorId : null;
				if (actorId != null && userEmpMap.containsKey(actorId)) {
					return userEmpMap.get(actorId).fullNameL.getFullName();
				}
			}

			// 承認者が見つからなかった場合は空文字を返却
			return '';
 		}
	}
}