/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description Parent entity class for ExpRecordItemEntity and ExpRequestRecordItemEntity
 */
public abstract with sharing class ExpRecordItem extends ExpExtendedItemEntity {

	/** Fields definition */
	public enum RecordItemBaseField {
		AMOUNT,
		COST_CENTER_HISTORY_ID,
		CURRENCY_ID,
		EXCHANGE_RATE,
		EXCHANGE_RATE_MANUAL,
		EXP_TYPE_ID,
		FIXED_ALLOWANCE_OPTION_ID,
		GST_VAT,
		ITEM_DATE,
		JOB_ID,
		LOCAL_AMOUNT,
		NAME,
		ORDER,
		ORIGINAL_EXCHANGE_RATE,
		REMARKS,
		TAX_MANUAL,
		TAX_TYPE_HISTORY_ID,
		USE_FIXED_FOREIGN_CURRENCY,
		USE_FOREIGN_CURRENCY,
		WITHOUT_TAX
	}

	/** Map of Fields with Name as the Key and the RecordItemBaseField enum as the value */
	public static final Map<String, RecordItemBaseField> RECORD_ITEM_BASE_FIELD_MAP;
	static {
		final Map<String, RecordItemBaseField> fieldMap = new Map<String, RecordItemBaseField>();
		for (RecordItemBaseField f : RecordItemBaseField.values()) {
			fieldMap.put(f.name(), f);
		}
		RECORD_ITEM_BASE_FIELD_MAP = fieldMap;
	}

	/** Class of Currency lookup fields (for reference only) */
	public class CurrencyInfo {
		/** Currency Name */
		public AppMultiString nameL;
		/** Currency Code */
		public String code;
		/** Currency Symbol */
		public String symbol;
		/** Currency Decimal Places */
		public Integer decimalPlaces;
	}

	/** Set of fields whose value has changed */
	private Set<RecordItemBaseField> isChangedRecordItemBaseFieldSet;


	/** Constructor */
	protected ExpRecordItem() {
		super();
		isChangedRecordItemBaseFieldSet = new Set<RecordItemBaseField>();
	}

	/** amount including tax */
	public Decimal amount {
		get;
		set {
			amount = value;
			setChanged(RecordItemBaseField.AMOUNT);
		}
	}

	/** ID of Cost Center History */
	public Id costCenterHistoryId {
		get;
		set {
			costCenterHistoryId = value;
			setChanged(RecordItemBaseField.COST_CENTER_HISTORY_ID);
		}
	}

	/** Cost Center Name (History Specific) */
	public String costCenterName {get {return AppMultiString.stringValue(costCenterNameL);}}
	public AppMultiString costCenterNameL {get; set;}

	/** Cost Center Code (for reference only) */
	public String costCenterCode {get; set;}

	/** Currency ID */
	public Id currencyId {
		get;
		set {
			currencyId = value;
			setChanged(RecordItemBaseField.CURRENCY_ID);
		}
	}
	/** Currency information (for reference only) */
	public ExpRecordItem.CurrencyInfo currencyInfo;

	/** Exchange Rate */
	public Decimal exchangeRate {
		get;
		set {
			exchangeRate = value;
			setChanged(RecordItemBaseField.EXCHANGE_RATE);
		}
	}

	/** Exchange Rate Manual */
	public Boolean exchangeRateManual {
		get;
		set {
			exchangeRateManual = value;
			setChanged(RecordItemBaseField.EXCHANGE_RATE_MANUAL);
		}
	}

	/** Fixed Allowance Option ID */
	public Id fixedAllowanceOptionId {
		get;
		set {
			fixedAllowanceOptionId = value;
			setChanged(RecordItemBaseField.FIXED_ALLOWANCE_OPTION_ID);
		}
	}

	/** Fixed Allowance Option Name (for reference only) */
	public String fixedAllowanceOptionLabel {get {return AppMultiString.stringValue(fixedAllowanceOptionLabelL);} private set;}
	public AppMultiString fixedAllowanceOptionLabelL {get; set;}

	/** Expense type ID */
	public Id expTypeId {
		get;
		set {
			expTypeId = value;
			setChanged(RecordItemBaseField.EXP_TYPE_ID);
		}
	}

	/** Expense type name */
	public String expTypeName {get {return AppMultiString.stringValue(expTypeNameL);}}
	public AppMultiString expTypeNameL {get; set;}

	/** GST/VAT */
	public Decimal gstVat {
		get;
		set {
			gstVat = value;
			setChanged(RecordItemBaseField.GST_VAT);
		}
	}

	/** Date */
	public AppDate itemDate {
		get;
		set {
			itemDate = value;
			setChanged(RecordItemBaseField.ITEM_DATE);
		}
	}

	/** Job ID */
	public String jobId {
		get;
		set {
			jobId = value;
			setChanged(RecordItemBaseField.JOB_ID);
		}
	}

	/** Job Name (for reference only) */
	public String jobName {get {return AppMultiString.stringValue(jobNameL);}}
	public AppMultiString jobNameL {get; set;}

	/** Job Code (for reference only) */
	public String jobCode {get; set;}
	
	/** Local Amount */
	public Decimal localAmount {
		get;
		set {
			localAmount = value;
			setChanged(RecordItemBaseField.LOCAL_AMOUNT);
		}
	}

	/** Name of record */
	public String name {
		get;
		set {
			name = value;
			setChanged(RecordItemBaseField.NAME);
		}
	}

	/** Order */
	public Integer order {
		get;
		set {
			order = value;
			setChanged(RecordItemBaseField.ORDER);
		}
	}

	/** Original Exchange Rate */
	public Decimal originalExchangeRate {
		get;
		set {
			originalExchangeRate = value;
			setChanged(RecordItemBaseField.ORIGINAL_EXCHANGE_RATE);
		}
	}

	/** Remarks */
	public String remarks {
		get;
		set {
			remarks = value;
			setChanged(RecordItemBaseField.REMARKS);
		}
	}

	/** Tax Auto */
	public Boolean taxManual {
		get;
		set {
			taxManual = value;
			setChanged(RecordItemBaseField.TAX_MANUAL);
		}
	}

	/** Tax Type */
	public Id taxTypeHistoryId {
		get;
		set {
			taxTypeHistoryId = value;
			setChanged(RecordItemBaseField.TAX_TYPE_HISTORY_ID);
		}
	}

	/*
	 * The Tax rate specified for the taxTypeHistoryId (for reference only)
	 */
	public Decimal taxRate {get; set;}

	/** Tax Type Base Id (for reference only) */
	public Id taxTypeBaseId {get; set;}

	/** Tax Type Name (for reference only) */
	public String taxTypeName {get {return AppMultiString.stringValue(taxTypeNameL);}}
	public AppMultiString taxTypeNameL {get; set;}

	/** Flag to indicate whether it's using Fixed Foreign Currency or Flexible Foreign Currency */
	public Boolean useFixedForeignCurrency {
		get;
		set {
			useFixedForeignCurrency = value;
			setChanged(RecordItemBaseField.USE_FIXED_FOREIGN_CURRENCY);
		}
	}

	/** Use Foreign Currency */
	public Boolean useForeignCurrency {
		get;
		set {
			useForeignCurrency = value;
			setChanged(RecordItemBaseField.USE_FOREIGN_CURRENCY);
		}
	}

	/** Amount excluding tax */
	public Decimal withoutTax {
		get;
		set {
			withoutTax = value;
			setChanged(RecordItemBaseField.WITHOUT_TAX);
		}
	}
	

	/**
	 * Mark field as changed
	 * @param field Changed field
	 */
	private void setChanged(RecordItemBaseField field) {
		this.isChangedRecordItemBaseFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(RecordItemBaseField field) {
		return isChangedRecordItemBaseFieldSet.contains(field);
	}

	public virtual override void resetChanged() {
		super.resetChanged();
		this.isChangedRecordItemBaseFieldSet.clear();
	}

	/*
	 * To allows child classes to determine if a field is an ExpRecordItem field
	 * @param fieldName the representative field name in String (e.g. expTypeId)
	 *
	 * @return true if the specified string is an ExpRecordItem field. Otherwise, false.
	 */
	protected Boolean isExpRecordItemField(String fieldName) {
		return ExpRecordItem.RECORD_ITEM_BASE_FIELD_MAP.get(name) == null ? false : true;
	}

	/*
	 * Return the field value
	 * @param name Target field name
	 */
	public virtual Object getValue(String name) {
		RecordItemBaseField field = ExpRecordItem.RECORD_ITEM_BASE_FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

		if (super.isExtendedItemField(name)) {
			return getExtendedItemValue(name);
		}

        throw new App.ParameterException('Unknown field : ' + name);
	}

	/**
	 * Return the field value
	 * @param field Target field to get the value
	 */
	public Object getValue(ExpRecordItem.RecordItemBaseField field) {
		if (field == ExpRecordItem.RecordItemBaseField.AMOUNT) {
			return amount;
		} else if (field == ExpRecordItem.RecordItemBaseField.COST_CENTER_HISTORY_ID) {
			return costCenterHistoryId;
		} else if (field == ExpRecordItem.RecordItemBaseField.CURRENCY_ID) {
			return currencyId;
		} else if (field == ExpRecordItem.RecordItemBaseField.EXCHANGE_RATE) {
			return exchangeRate;
		} else if (field == ExpRecordItem.RecordItemBaseField.EXCHANGE_RATE_MANUAL) {
			return exchangeRateManual;
		} else if (field == ExpRecordItem.RecordItemBaseField.EXP_TYPE_ID) {
			return expTypeId;
		} else if (field == ExpRecordItem.RecordItemBaseField.FIXED_ALLOWANCE_OPTION_ID) {
			return fixedAllowanceOptionId;
		} else if (field == ExpRecordItem.RecordItemBaseField.GST_VAT) {
			return gstVat;
		} else if (field == ExpRecordItem.RecordItemBaseField.ITEM_DATE) {
			return itemDate;
		} else if (field == ExpRecordItem.RecordItemBaseField.JOB_ID) {
			return jobId;
		} else if (field == ExpRecordItem.RecordItemBaseField.LOCAL_AMOUNT) {
			return localAmount;
		} else if (field == ExpRecordItem.RecordItemBaseField.NAME) {
			return name;
		} else if (field == ExpRecordItem.RecordItemBaseField.ORDER) {
			return order;
		} else if (field == ExpRecordItem.RecordItemBaseField.ORIGINAL_EXCHANGE_RATE) {
			return originalExchangeRate;
		} else if (field == ExpRecordItem.RecordItemBaseField.REMARKS) {
			return remarks;
		} else if (field == ExpRecordItem.RecordItemBaseField.TAX_MANUAL) {
			return taxManual;
		} else if (field == ExpRecordItem.RecordItemBaseField.TAX_TYPE_HISTORY_ID) {
			return taxTypeHistoryId;
		} else if (field == ExpRecordItem.RecordItemBaseField.USE_FOREIGN_CURRENCY) {
			return useForeignCurrency;
		} else if (field == ExpRecordItem.RecordItemBaseField.WITHOUT_TAX) {
			return withoutTax;
		}

		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}
