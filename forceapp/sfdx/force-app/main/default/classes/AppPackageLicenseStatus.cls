/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group アプリ
 *
 * パッケージライセンスのステータス
 */
public with sharing class AppPackageLicenseStatus extends ValueObjectType {

	/** トライアル */
	public static final AppPackageLicenseStatus TRIAL = new AppPackageLicenseStatus('Trial');
	/** アクティブ */
	public static final AppPackageLicenseStatus ACTIVE = new AppPackageLicenseStatus('Active');

	/** ステータスのリスト（ステータスが追加されら本リストにも追加してください） */
	public static final List<AppPackageLicenseStatus> STATUS_LIST;
	static {
		STATUS_LIST = new List<AppPackageLicenseStatus> {
			TRIAL,
			ACTIVE
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AppPackageLicenseStatus(String value) {
		super(value);
	}

	/**
	 * 値からAppPackageLicenseStatusを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAppPackageLicenseStatus、ただし該当するAppPackageLicenseStatusが存在しない場合はnull
	 */
	public static AppPackageLicenseStatus valueOf(String value) {
		AppPackageLicenseStatus retStatus = null;
		if (String.isNotBlank(value)) {
			for (AppPackageLicenseStatus status : STATUS_LIST) {
				if (value == status.value) {
					retStatus = status;
				}
			}
		}
		return retStatus;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AppPackageLicenseStatus以外の場合はfalse
		Boolean eq;
		if (compare instanceof AppPackageLicenseStatus) {
			// 値が同じであればtrue
			if (this.value == ((AppPackageLicenseStatus)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}