/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Entity class for Vendor master
 */
public with sharing class ExpVendorEntity extends Entity {

	/** Max length of code fields */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** Max length of short code fields */
	public static final Integer SHORT_CODE_MAX_LENGTH = 3;
	/** Max length of bank code fields */
	public static final Integer BANK_CODE_MAX_LENGTH = 7;
	/** Max length of swift code fields */
	public static final Integer SWIFT_CODE_MAX_LENGTH = 11;
	/** Max length of the zip code field */
	public static final Integer ZIP_CODE_MAX_LENGTH = 9;
	/** Max length of name fields */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** Max length of the bank name field */
	public static final Integer BANK_NAME_MAX_LENGTH = 60;
	/** Max length of the payee name field */
	public static final Integer PAYEE_NAME_MAX_LENGTH = 50;
	/** Max length of the branch name field */
	public static final Integer BRANCH_NAME_MAX_LENGTH = 40;
	/** Max length of the bank account number field */
	public static final Integer BANK_ACCOUNT_NUM_MAX_LENGTH = 38;
	/** Max length of long text fields */
	public static final Integer LONG_TEXT_MAX_LENGTH = 255;

	public enum Field {
		ACTIVE,
		ADDRESS,
		BANK_ACCOUNT_TYPE,
		BANK_ACCOUNT_NUMBER,
		BANK_CODE,
		BANK_NAME,
		BRANCH_ADDRESS,
		BRANCH_CODE,
		BRANCH_NAME,
		CODE,
		COMPANY_ID,
		COUNTRY,
		CURRENCY_CODE,
		CORRESPONDENT_BANK_ADDRESS,
		CORRESPONDENT_BANK_NAME,
		CORRESPONDENT_BRANCH_NAME,
		CORRESPONDENT_SWIFT_CODE,
		IS_WITHHOLDING_TAX,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		PAYEE_NAME,
		PAYMENT_DUE_DATE_USAGE,
		PAYMENT_TERM,
		PAYMENT_TERM_CODE,
		SWIFT_CODE,
		UNIQUE_KEY,
		ZIP_CODE
	}

	/** Set of the fields whose value has been changed */
	protected Set<Field> changedFieldSet = new Set<Field>();

	public Boolean active {
		get;
		set {
			active = value;
			setChanged(Field.ACTIVE);
		}
	}

	public String address {
		get;
		set {
			address = value;
			setChanged(Field.ADDRESS);
		}
	}

	public ExpBankAccountType bankAccountType {
		get;
		set {
			bankAccountType = value;
			setChanged(Field.BANK_ACCOUNT_TYPE);
		}
	}

	public String bankAccountNumber {
		get;
		set {
			bankAccountNumber = value;
			setChanged(Field.BANK_ACCOUNT_NUMBER);
		}
	}

	public String bankCode {
		get;
		set {
			bankCode = value;
			setChanged(Field.BANK_CODE);
		}
	}

	public String bankName {
		get;
		set {
			bankName = value;
			setChanged(Field.BANK_NAME);
		}
	}

	public String branchAddress {
		get;
		set {
			branchAddress = value;
			setChanged(Field.BRANCH_ADDRESS);
		}
	}

	public String branchCode {
		get;
		set {
			branchCode = value;
			setChanged(Field.BRANCH_CODE);
		}
	}

	public String branchName {
		get;
		set {
			branchName = value;
			setChanged(Field.BRANCH_NAME);
		}
	}

	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	public String country {
		get;
		set {
			country = value;
			setChanged(Field.COUNTRY);
		}
	}

	public ComIsoCurrencyCode currencyCode {
		get;
		set {
			currencyCode = value;
			setChanged(Field.CURRENCY_CODE);
		}
	}

	public String correspondentBankAddress {
		get;
		set {
			correspondentBankAddress = value;
			setChanged(Field.CORRESPONDENT_BANK_ADDRESS);
		}
	}

	public String correspondentBankName {
		get;
		set {
			correspondentBankName = value;
			setChanged(Field.CORRESPONDENT_BANK_NAME);
		}
	}

	public String correspondentBranchName {
		get;
		set {
			correspondentBranchName = value;
			setChanged(Field.CORRESPONDENT_BRANCH_NAME);
		}
	}

	public String correspondentSwiftCode {
		get;
		set {
			correspondentSwiftCode = value;
			setChanged(Field.CORRESPONDENT_SWIFT_CODE);
		}
	}

	public Boolean isWithholdingTax {
		get;
		set {
			isWithholdingTax = value;
			setChanged(Field.IS_WITHHOLDING_TAX);
		}
	}

	/** Whether Payment Due Date will be Required/Optional/NotUsed */
	public ExpVendorPaymentDueDateUsage paymentDueDateUsage {
		get;
		set {
			paymentDueDateUsage = value;
			setChanged(Field.PAYMENT_DUE_DATE_USAGE);
		}
	}

	/** Name(Multilingual) */
	public AppMultiString nameL {
		get {return new AppMultiString(nameL0, nameL1, nameL2);}
	}

	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	public String payeeName {
		get;
		set {
			payeeName = value;
			setChanged(Field.PAYEE_NAME);
		}
	}

	public String paymentTerm {
		get;
		set {
			paymentTerm = value;
			setChanged(Field.PAYMENT_TERM);
		}
	}

	public String paymentTermCode {
		get;
		set {
			paymentTermCode = value;
			setChanged(Field.PAYMENT_TERM_CODE);
		}
	}

	public String swiftCode {
		get;
		set {
			swiftCode = value;
			setChanged(Field.SWIFT_CODE);
		}
	}

	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQUE_KEY);
		}
	}

	public String zipCode {
		get;
		set {
			zipCode = value;
			setChanged(Field.ZIP_CODE);
		}
	}

	/**
	 * Set field as changed
	 * @param field Field to be set
	 */
	private void setChanged(Field field) {
		this.changedFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public void resetChanged() {
		this.changedFieldSet.clear();
	}

	/**
	* Create unique key
	* Need to be set the value of instance variable 'code'
	* @param companyCode Code of company
	* @return Unique key created
	*/
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// Only for debugging, no need to translate
			throw new App.ParameterException('[Server Error]Failed to create unique key of "Exchange Rate". companyCode is empty.');
		}
		if (String.isBlank(this.code)) {
			// Only for debugging, no need to translate
			throw new App.IllegalStateException('[Server Error][Server Error]Failed to create unique key of "Exchange Rate". code is empty.');
		}

		String key = companyCode + '-' + this.code;
		this.uniqKey = key;

		// company code + '-' + exchange rate code
		return key;
	}

	/**
	 * Get Field enum value by name
	 * @param fieldName This should be equal to one of the enum value of name().(e.g. CODE.name()
	 * @return Field enum value match to
	 */
	private Field getFieldByName(String fieldName) {
		for (Field fld : Field.values()) {
			if (fld.name().equals(fieldName)) {
				return fld;
			}
		}
		throw new App.UnsupportedException('ExpVendorEntity.getFieldByName: Unexpected field. fieldName=' + fieldName);
	}

	/**
	 * Return the value of the specified field name
	 * @param targetField Target Field name
	 * @return Field enum value matched the specified field name
	 */
	public Object getFieldValue(String fieldName) {
		return getFieldValue(getFieldByName(fieldName));
	}

	/**
	 * Return the value of the specified field
	 * @param targetField Target Field
	 * @return Field enum value matched the specified field
	 */
	public Object getFieldValue(Field targetField) {
		if (targetField == Field.ACTIVE) {
			return this.active;
		}
		if (targetField == Field.ADDRESS) {
			return this.address;
		}
		if (targetField == Field.BANK_ACCOUNT_TYPE) {
			return this.bankAccountType;
		}
		if (targetField == Field.BANK_ACCOUNT_NUMBER) {
			return this.bankAccountNumber;
		}
		if (targetField == Field.BANK_CODE) {
			return this.bankCode;
		}
		if (targetField == Field.BANK_NAME) {
			return this.bankName;
		}
		if (targetField == Field.BRANCH_ADDRESS) {
			return this.branchAddress;
		}
		if (targetField == Field.BRANCH_CODE) {
			return this.branchCode;
		}
		if (targetField == Field.BRANCH_NAME) {
			return this.branchName;
		}
		if (targetField == Field.CODE) {
			return this.code;
		}
		if (targetField == Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == Field.COUNTRY) {
			return this.country;
		}
		if (targetField == Field.CURRENCY_CODE) {
			return this.currencyCode;
		}
		if (targetField == Field.CORRESPONDENT_BANK_ADDRESS) {
			return this.correspondentBankAddress;
		}
		if (targetField == Field.CORRESPONDENT_BANK_NAME) {
			return this.correspondentBankName;
		}
		if (targetField == Field.CORRESPONDENT_BRANCH_NAME) {
			return this.correspondentBranchName;
		}
		if (targetField == Field.CORRESPONDENT_SWIFT_CODE) {
			return this.correspondentSwiftCode;
		}
		if (targetField == Field.IS_WITHHOLDING_TAX) {
			return this.isWithholdingTax;
		}
		if (targetField == Field.NAME_L0) {
			return this.nameL0;
		}
		if (targetField == Field.NAME_L1) {
			return this.nameL1;
		}
		if (targetField == Field.NAME_L2) {
			return this.nameL2;
		}
		if (targetField == Field.PAYEE_NAME) {
			return this.payeeName;
		}
		if (targetField == Field.PAYMENT_DUE_DATE_USAGE) {
			return this.paymentDueDateUsage;
		}
		if (targetField == Field.PAYMENT_TERM) {
			return this.paymentTerm;
		}
		if (targetField == Field.PAYMENT_TERM_CODE) {
			return this.paymentTermCode;
		}
		if (targetField == Field.SWIFT_CODE) {
			return this.swiftCode;
		}
		if (targetField == Field.UNIQUE_KEY) {
			return this.uniqKey;
		}
		if (targetField == Field.ZIP_CODE) {
			return this.zipCode;
		}
		throw new App.UnsupportedException('ExpVendorEntity.getFieldValue: Unexpected field. targetField='+ targetField);
	}

	/**
	 * Set the value to the specified field name
	 * @param targetField Target field to set the value
	 * @pram value Value to be set
	 */
	public void setFieldValue(String fieldName, Object value) {
		setFieldValue(getFieldByName(fieldName), value);
	}

	/**
	 * Set the value to the specified field
	 * @param targetField Target field to set the value
	 * @pram value Value to be set
	 */
	public void setFieldValue(Field targetField, Object value) {
		if (targetField == Field.ACTIVE) {
			active = (Boolean)value;
		} else if (targetField == Field.ADDRESS) {
			address = (String)value;
		} else if (targetField == Field.BANK_ACCOUNT_NUMBER) {
			bankAccountNumber = (String)value;
		} else if (targetField == Field.BANK_ACCOUNT_TYPE) {
			bankAccountType = (ExpBankAccountType)value;
		} else if (targetField == Field.BANK_CODE) {
			bankCode = (String)value;
		} else if (targetField == Field.BANK_NAME) {
			bankName = (String)value;
		} else if (targetField == Field.BRANCH_ADDRESS) {
			branchAddress = (String)value;
		} else if (targetField == Field.BRANCH_CODE) {
			branchCode = (String)value;
		} else if (targetField == Field.BRANCH_NAME) {
			branchName = (String)value;
		} else if (targetField == Field.CODE) {
			code = (String)value;
		} else if (targetField == Field.COMPANY_ID) {
			companyId = (Id)value;
		} else if (targetField == Field.CORRESPONDENT_BANK_ADDRESS) {
			correspondentBankAddress = (String)value;
		} else if (targetField == Field.CORRESPONDENT_BANK_NAME) {
			correspondentBankName = (String)value;
		} else if (targetField == Field.CORRESPONDENT_BRANCH_NAME) {
			correspondentBranchName = (String)value;
		} else if (targetField == Field.CORRESPONDENT_SWIFT_CODE) {
			correspondentSwiftCode = (String)value;
		} else if (targetField == Field.COUNTRY) {
			country = (String)value;
		} else if (targetField == Field.CURRENCY_CODE) {
			currencyCode = (ComIsoCurrencyCode)value;
		} else if (targetField == Field.IS_WITHHOLDING_TAX) {
			isWithholdingTax = (Boolean)value;
		} else if (targetField == Field.NAME_L0) {
			nameL0 = (String)value;
		} else if (targetField == Field.NAME_L1) {
			nameL1 = (String)value;
		} else if (targetField == Field.NAME_L2) {
			nameL2 = (String)value;
		} else if (targetField == Field.PAYEE_NAME) {
			payeeName = (String)value;
		} else if (targetField == Field.PAYMENT_DUE_DATE_USAGE) {
			paymentDueDateUsage = (ExpVendorPaymentDueDateUsage) value;
		} else if (targetField == Field.PAYMENT_TERM) {
			paymentTerm = (String)value;
		} else if (targetField == Field.PAYMENT_TERM_CODE) {
			paymentTermCode = (String) value;
		} else if (targetField == Field.SWIFT_CODE) {
			swiftCode = (String)value;
		} else if (targetField == Field.UNIQUE_KEY) {
			uniqKey = (String)value;
		} else if (targetField == Field.ZIP_CODE) {
			zipCode = (String)value;
		} else {
			throw new App.UnsupportedException('ExpVendorEntity.setFieldValue: Unexpected field. targetField=' + targetField);
		}
	}
}