/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブリポジトリ
 */
public with sharing class JobRepository extends ValidPeriodRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** Pattern for detecting characters to unescape */
	private static final Pattern SPECIAL_CHARACTERS_PATTERN = Pattern.compile('[%\\_\\\\]');

	/**
	 * 取得対象の項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ComJob__c.Id,
				ComJob__c.Name,
				ComJob__c.ValidFrom__c,
				ComJob__c.ValidTo__c,
				ComJob__c.Code__c,
				ComJob__c.UniqKey__c,
				ComJob__c.CompanyId__c,
				ComJob__c.Name_L0__c,
				ComJob__c.Name_L1__c,
				ComJob__c.Name_L2__c,
				ComJob__c.DepartmentBaseId__c,
				ComJob__c.ParentId__c,
				ComJob__c.JobOwnerBaseId__c,
				ComJob__c.DirectCharged__c,
				ComJob__c.JobTypeId__c,
				ComJob__c.SelectabledExpense__c,
				ComJob__c.SelectabledTimeTrack__c,
				ComJob__c.ScopedAssignment__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** 親ジョブのリレーション名 */
	private static final String PARENT_R = ComJob__c.ParentId__c.getDescribe().getRelationshipName();
	/**
	 * 親ジョブのオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_PARENT_FIELD_NAME_LIST;
	static {

		// 取得対象の項目定義(ジョブ)
		final Set<Schema.SObjectField> jobFieldList = new Set<Schema.SObjectField> {
				ComJob__c.Name_L0__c,
				ComJob__c.Name_L1__c,
				ComJob__c.Name_L2__c};

		GET_PARENT_FIELD_NAME_LIST = Repository.generateFieldNameList(PARENT_R, jobFieldList);
	}

	/** ジョブオーナーのリレーション名 */
	private static final String JOB_OWNER_R = ComJob__c.JobOwnerBaseId__c.getDescribe().getRelationshipName();
	/**
	 * 取得対象のジョブオーナーのオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_JOB_OWNER_FIELD_NAME_LIST;
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> employeeFieldList = new Set<Schema.SObjectField> {
				ComEmpBase__c.FirstName_L0__c, ComEmpBase__c.LastName_L0__c,
				ComEmpBase__c.FirstName_L1__c, ComEmpBase__c.LastName_L1__c,
				ComEmpBase__c.FirstName_L2__c, ComEmpBase__c.LastName_L2__c};

		GET_JOB_OWNER_FIELD_NAME_LIST = Repository.generateFieldNameList(JOB_OWNER_R, employeeFieldList);
	}

	/** ジョブタイプのリレーション名 */
	private static final String JOB_TYPE_R = ComJob__c.JobTypeId__c.getDescribe().getRelationshipName();
	/**
	 * ジョブタイプのオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_JOB_TYPE_FIELD_NAME_LIST;
	static {

		// 取得対象の項目定義(ジョブ)
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ComJobType__c.Name_L0__c,
				ComJobType__c.Name_L1__c,
				ComJobType__c.Name_L2__c};

		GET_JOB_TYPE_FIELD_NAME_LIST = Repository.generateFieldNameList(JOB_TYPE_R, fieldList);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** ジョブID */
		public Set<Id> ids;
		/** 会社ID */
		public Set<Id> companyIds;
		/** コード(完全一致) */
		public Set<String> codes;
		/** 部署ベースID */
		public Set<Id> departmentIds;
		/** 親ジョブID */
		public Set<Id> parentIds;
		/** 取得対象日 */
		public AppDate targetDate;
		/**
		 * 取得対象日範囲
		 * 開始日から終了日のうち、1日でも有効なジョブがあれば取得対象とする。
		 * 終了日に有効なジョブも含む。
		 */
		public AppDateRange dateRange;
		/** 経費選択可能フラグ */
		public Boolean isSelectableExpense;
		/** 工数選択可能フラグ */
		public Boolean isSelectableTimeTrack;
		/** 上階層ジョブ取得フラグ */
		public Boolean includeHigherParents;
		/** アサイン個別指定フラグ */
		public Boolean isScopedAssignment;
		/** Search query for Code and Name */
		public String query;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param jobId ジョブID
	 * @return ベースエンティティの検索結果
	 */
	public JobEntity getEntity(Id jobId) {
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.ids = new Set<Id>{jobId};
		List<JobEntity> entities = searchEntity(filter, NOT_FOR_UPDATE);

		if (entities.isEmpty()) {
			return null;
		} else {
			return entities[0];
		}
	}

	/**
	 * 指定したコードと会社IDを持つエンティティを取得する
	 * @param code 取得対象のコード
	 * @param companyId 取得対象の会社ID
	 * @return ベースエンティティの検索結果
	 */
	public JobEntity getEntityByCode(String code, Id companyId) {
		List<JobEntity> entities = getEntityListByCode(new Set<String>{code}, companyId);

		if (entities.isEmpty()) {
			return null;
		} else {
			return entities[0];
		}
	}

	/**
	 * 指定したコードと会社IDを持つエンティティを取得する
	 * @param codes 取得対象のコード
	 * @param companyId 取得対象の会社ID
	 * @return ベースエンティティの検索結果
	 */
	public List<JobEntity> getEntityListByCode(Set<String> codes, Id companyId) {
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.codes = codes;
		filter.companyIds = new Set<Id>{companyId};
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	public List<JobEntity> searchEntity(JobRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	public List<JobEntity> searchEntity(JobRepository.SearchFilter filter, Boolean forUpdate) {

		// WHERE句
		List<String> whereList = new List<String>();
		// ジョブIDで検索
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ComJob__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ComJob__c.Code__c) + ' IN :pCodes');
		}
		// 部署ベースIDで検索
		Set<Id> pDepartmentIds;
		if (filter.departmentIds != null) {
			pDepartmentIds = filter.departmentIds;
			whereList.add(getFieldName(ComJob__c.DepartmentBaseId__c) + ' IN :pDepartmentIds');
		}
		// 親ジョブIDで検索
		Set<Id> pParentIds;
		if (filter.parentIds != null) {
			pParentIds = filter.parentIds;
			if (filter.includeHigherParents != true) {
				whereList.add(getFieldName(ComJob__c.ParentId__c) + ' IN :pParentIds');
			} else {
				List<String> parentWhereList = new List<String>();
				parentWhereList.add(getFieldName(ComJob__c.ParentId__c) + ' IN :pParentIds');
				parentWhereList.add(getFieldName(PARENT_R, ComJob__c.ParentId__c) + ' IN :pParentIds');
				parentWhereList.add(getFieldName(PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c) + ' IN :pParentIds');
				parentWhereList.add(getFieldName(PARENT_R + '.' + PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c) + ' IN :pParentIds');
				parentWhereList.add(getFieldName(PARENT_R + '.' + PARENT_R + '.' + PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c) + ' IN :pParentIds');
				parentWhereList.add(getFieldName(PARENT_R + '.' + PARENT_R + '.' + PARENT_R + '.' + PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c)+ ' IN :pParentIds');
				whereList.add('(' + String.join(parentWhereList, ' OR ') + ')');
			}
		}
		// Query for Code or Name
		String pQuery;
		if (filter.query != null) {
			pQuery = '%' + escapeSpecialCharacters(filter.query) + '%';
			String s = getFieldName(ComJob__c.Code__c) + ' LIKE :pQuery OR '
						+ getFieldName(ComJob__c.Name_L0__c) + ' LIKE :pQuery OR '
						+ getFieldName(ComJob__c.Name_L1__c) + ' LIKE :pQuery OR '
						+ getFieldName(ComJob__c.Name_L2__c) + ' LIKE :pQuery ';
			whereList.add(s);
		}
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ComJob__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ComJob__c.ValidTo__c) + ' > :pTargetDate');
		}
		// 対象日範囲
		Date pTargetDateRangeFrom;
		Date pTargetDateRangeTo;
		if (filter.dateRange != null) {
			pTargetDateRangeFrom = filter.dateRange.startDate.getDate();
			pTargetDateRangeTo = filter.dateRange.endDate.getDate();
			whereList.add(getFieldName(ComJob__c.ValidTo__c) + ' > :pTargetDateRangeFrom');
			whereList.add(getFieldName(ComJob__c.ValidFrom__c) + ' <= :pTargetDateRangeTo');
		}
		// 経費選択可能フラグ
		Boolean pIsSelectableExpense;
		if (filter.isSelectableExpense != null) {
			pIsSelectableExpense = filter.isSelectableExpense;
			whereList.add(getFieldName(ComJob__c.SelectabledExpense__c) + ' = :pIsSelectableExpense');
		}
		// 工数選択可能フラグ
		Boolean pIsSelectableTimeTrack;
		if (filter.isSelectableTimeTrack != null) {
			pIsSelectableTimeTrack = filter.isSelectableTimeTrack;
			whereList.add(getFieldName(ComJob__c.SelectabledTimeTrack__c) + ' = :pIsSelectableTimeTrack');
		}
		// アサイン個別指定
		Boolean pIsScopedAssignment;
		if (filter.isScopedAssignment != null) {
			pIsScopedAssignment = filter.isScopedAssignment;
			whereList.add(getFieldName(ComJob__c.ScopedAssignment__c) + ' = :pIsScopedAssignment');
		}

		String whereString = buildWhereString(whereList);

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 親ジョブ
		selectFieldList.addAll(GET_PARENT_FIELD_NAME_LIST);
		// 上位階層親ジョブ(SF制限上、2階層から6階層まで)
		if (filter.includeHigherParents == true) {
			selectFieldList.add(getFieldName(PARENT_R, ComJob__c.ParentId__c));
			selectFieldList.add(getFieldName(PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c));
			selectFieldList.add(getFieldName(PARENT_R + '.' + PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c));
			selectFieldList.add(getFieldName(PARENT_R + '.' + PARENT_R + '.' + PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c));
			selectFieldList.add(getFieldName(PARENT_R + '.' + PARENT_R + '.' + PARENT_R + '.' + PARENT_R + '.' + PARENT_R, ComJob__c.ParentId__c));
		}
		// ジョブオーナー
		selectFieldList.addAll(GET_JOB_OWNER_FIELD_NAME_LIST);
		// ジョブタイプ
		selectFieldList.addAll(GET_JOB_TYPE_FIELD_NAME_LIST);

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ComJob__c.SObjectType.getDescribe().getName()
				+ whereString
				+ ' ORDER BY ' + getFieldName(ComJob__c.Code__c) + ' Asc NULLS LAST'
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('JobRepository: SOQL=' + soql);

		// クエリ実行
		List<ComJob__c> sObjs = (List<ComJob__c>)Database.query(soql);

		// SObjectからエンティティを作成
		List<JobEntity> entities = new List<JobEntity>();
		for (ComJob__c sObj : sObjs) {
			entities.add(new JobEntity(sObj));
		}

		return entities;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	@TestVisible
	protected override List<SObject> createObjectList(List<ValidPeriodEntity> entityList) {

		List<ComJob__c> objectList = new List<ComJob__c>();
		for (ValidPeriodEntity entity : entityList) {
			objectList.add(((JobEntity)entity).createSObject());
		}

		return objectList;
	}

	/*
	 * Return an escaped version of the input.
	 * @param unEscapedString String to check
	 * @return escaped version of the string
	 */
	private static String escapeSpecialCharacters(String unEscapedString) {
		String[] chars = unEscapedString.split('');
		for (Integer i = 0; i < chars.size(); i++) {
			if (SPECIAL_CHARACTERS_PATTERN.matcher(chars[i]).matches()) {
				chars[i] = '\\' + chars[i];
			}
		}
		return String.join(chars, '');
	}
}