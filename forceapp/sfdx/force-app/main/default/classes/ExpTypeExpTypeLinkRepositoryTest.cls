/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description 費目別費目リポジトリのテスト Test class for ExpTypeExpTypeLinkRepository
 */
@isTest
private class ExpTypeExpTypeLinkRepositoryTest {
	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする Rollback transaction if there is a failure upon saving of record */
	private final static Boolean ALL_SAVE = true;

	/** テストデータ Test Data */
	private class RepoTestData {
		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') Organization Object */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト Country Object */
		public ComCountry__c countryObj;
		/** 会社オブジェクト Company Object */
		public ComCompany__c companyObj;
		/** 費目(親)オブジェクトリスト List of Parent ExpType Objects */
		public List<ExpType__c> parentExpTypeObjs;
		/** 費目(子)オブジェクトリスト List of Child ExpType Objects */
		public List<ExpType__c> childExpTypeObjs;
		/** 費目別費目オブジェクト List of ExpTypeExpTypeLink Objects */
		public List<ExpTypeExpTypeLink__c> expTypeExpTypeLinkObjs;

		/** コンストラクタ Constructor */
		public RepoTestData(Integer parentExpTypeSize, Integer childExpTypeSize) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
			parentExpTypeObjs = ComTestDataUtility.createExpTypes('Test PET', ExpRecordType.HOTEL_FEE, ExpFileAttachment.NOT_USED, companyObj.Id, parentExpTypeSize);
			childExpTypeObjs = ComTestDataUtility.createExpTypes('Test CET', ExpRecordType.GENERAL, ExpFileAttachment.NOT_USED, companyObj.Id, childExpTypeSize);

			expTypeExpTypeLinkObjs = new List<ExpTypeExpTypeLink__c>();
			for (ExpType__c parentExpTypeObj : parentExpTypeObjs) {
				for (ExpType__c childExpTypeObj : childExpTypeObjs) {
					ExpTypeExpTypeLink__c obj = new ExpTypeExpTypeLink__c(
							ExpTypeParentId__c = parentExpTypeObj.Id, 
							ExpTypeChildId__c = childExpTypeObj.Id);
					expTypeExpTypeLinkObjs.add(obj);
				}
			}
			insert expTypeExpTypeLinkObjs;
		}
	}

	/**
	 * getEntityのテスト getEntity Test
	 * 指定したIDで取得できることを確認する Verify specified ID can be retrieved
	 */
	@isTest static void getEntityTest() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);
		
		ExpTypeExpTypeLink__c target = getSObject(testData.expTypeExpTypeLinkObjs[4].Id);
		ExpTypeExpTypeLinkEntity entity = new ExpTypeExpTypeLinkRepository().getEntity(target.Id);

		// 各項目の値が正しく取得できているかのチェックはsearchEntityListのテストで行う Execute searchEntityList and validate entity retrieved
		TestUtil.assertNotNull(entity);
		assertEntity(target, entity);
	}

	/**
	 * getEntityのテスト getEntity Test
	 * 指定したIDのレコードが存在しない場合、nullが返却されることを確認する Verify null is returned if specified ID does not exists
	 */
	@isTest static void getEntityTestNotFound() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		Id targetId = UserInfo.getUserId();
		ExpTypeExpTypeLinkEntity entity = new ExpTypeExpTypeLinkRepository().getEntity(targetId);

		System.assertEquals(null, entity);
	}

	/**
	 * getEntityListのテスト getEntityList Test
	 * 指定したIDで取得できることを確認する Verify specified IDs can be retrieved
	 */
	@isTest static void getEntityListTest() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		ExpTypeExpTypeLink__c target1 = getSObject(testData.expTypeExpTypeLinkObjs[1].Id);
		ExpTypeExpTypeLink__c target2 = getSObject(testData.expTypeExpTypeLinkObjs[5].Id);
		List<ExpTypeExpTypeLinkEntity> entities =
				new ExpTypeExpTypeLinkRepository().getEntityList(new List<Id>{target2.Id, target1.Id});

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * getEntityListByParentExpTypeIdのテスト getEntityListByParentExpTypeId Test
	 * 指定したIDで取得できることを確認する Verify specified Parent ExpType ID can be retrieved
	 */
	@isTest static void getEntityListByParentExpTypeTest() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		ExpTypeExpTypeLink__c target1 = getSObject(testData.expTypeExpTypeLinkObjs[0].Id);
		ExpTypeExpTypeLink__c target2 = getSObject(testData.expTypeExpTypeLinkObjs[1].Id);
		List<ExpTypeExpTypeLinkEntity> entities =
				new ExpTypeExpTypeLinkRepository().getEntityListByParentExpTypeId(target1.ExpTypeParentId__c);

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * getEntityListByChildExpTypeIdのテスト getEntityListByChildExpTypeId Test
	 * 指定したIDで取得できることを確認する Verify specified Child ExpType ID can be retrieved
	 */
	@isTest static void getEntityListByChildExpTypeTest() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		ExpTypeExpTypeLink__c target1 = getSObject(testData.expTypeExpTypeLinkObjs[1].Id);
		ExpTypeExpTypeLink__c target2 = getSObject(testData.expTypeExpTypeLinkObjs[3].Id);
		ExpTypeExpTypeLink__c target3 = getSObject(testData.expTypeExpTypeLinkObjs[5].Id);
		List<ExpTypeExpTypeLinkEntity> entities =
				new ExpTypeExpTypeLinkRepository().getEntityListByChildExpTypeId(target1.ExpTypeChildId__c);

		System.assertEquals(3, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
		assertEntity(target3, entities[2]);
	}

	/**
	 * searchEntityListのテスト searchEntityList Test
	 * 指定した条件で取得できることを確認する Verify entities can be retrieved correctly based on filter criteria
	 */
	@isTest static void searchEntityListTestFilter() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		// 同じ費目(親) Same Parent ExpType
		ExpTypeExpTypeLink__c targetObj1 = getSObject(testData.expTypeExpTypeLinkObjs[0].Id);
		ExpTypeExpTypeLink__c targetObj2 = getSObject(testData.expTypeExpTypeLinkObjs[1].Id);

		ExpTypeExpTypeLinkRepository.SearchFilter filter;
		ExpTypeExpTypeLinkRepository repo = new ExpTypeExpTypeLinkRepository();
		List<ExpTypeExpTypeLinkEntity> resEntities;

		// TEST1: IDで検索 Search by Id
		filter = new ExpTypeExpTypeLinkRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj2.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// TEST2: 費目(親)IDで検索 Search by Parent ExpType Id
		ExpType__c expType1 = testData.childExpTypeObjs[1];
		ExpType__c expType2 = testData.childExpTypeObjs[0];
		targetObj1.ExpTypeChildId__c = expType2.Id;
		targetObj2.ExpTypeChildId__c = expType1.Id;
		update new List<ExpTypeExpTypeLink__c>{targetObj1, targetObj2};
		targetObj1.ExpTypeChildId__c = expType2.Id;
		filter = new ExpTypeExpTypeLinkRepository.SearchFilter();
		filter.expTypeParentIds = new Set<Id>{targetObj1.ExpTypeParentId__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(2, resEntities.size());
		System.assertEquals(targetObj2.ExpTypeParentId__c, resEntities[0].expTypeParentId);
		System.assertEquals(targetObj1.ExpTypeParentId__c, resEntities[1].expTypeParentId);

		// TEST3: 費目(子)IDで検索 Search by Child ExpType Id
		filter = new ExpTypeExpTypeLinkRepository.SearchFilter();
		filter.expTypeChildIds = new Set<Id>{targetObj1.ExpTypeChildId__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(3, resEntities.size());
		
		// TEST4: 取得対象日で検索 Search by TargetDate
		expType1.ValidFrom__c = expType1.ValidTo__c;
		expType1.ValidTo__c = expType1.ValidFrom__c.addYears(1);
		update expType1;
		filter = new ExpTypeExpTypeLinkRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(expType1.ValidFrom__c);
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(3, resEntities.size());
	}

	/**
	 * saveEntityのテスト saveEntity Test
	 * 新規保存できることを確認する Verify creation of new record
	 */
	@isTest static void saveEntityTestNew() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		// テストデータの費目別費目を削除しておく Delete test data of ExpTypeExpTypeLink
		delete testData.expTypeExpTypeLinkObjs;

		ExpTypeExpTypeLinkEntity entity = new ExpTypeExpTypeLinkEntity();
		entity.expTypeParentId = testData.parentExpTypeObjs[2].Id;
		entity.expTypeChildId = testData.childExpTypeObjs[1].Id;

		Repository.SaveResult result = new ExpTypeExpTypeLinkRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ExpTypeExpTypeLink__c resObj = getSObject(result.details[0].id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityのテスト saveEntity Test
	 * 既存のエンティティを保存できることを確認する Verify update of existing records
	 */
	@isTest static void saveEntityTestUpdate() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		ExpTypeExpTypeLink__c target = testData.expTypeExpTypeLinkObjs[0];
		ExpTypeExpTypeLinkEntity entity = new ExpTypeExpTypeLinkEntity();
		entity.setId(target.Id);
		entity.expTypeParentId = testData.parentExpTypeObjs[2].Id;
		entity.expTypeChildId = testData.childExpTypeObjs[1].Id;

		Repository.SaveResult result = new ExpTypeExpTypeLinkRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ExpTypeExpTypeLink__c resObj = getSObject(target.Id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityListのテスト saveEntityList Test
	 * 新規、既存のエンティティが保存できることを確認する Verify upsert of new and existing records
	 */
	@isTest static void saveEntityListTest() {
		final Integer parentExpTypeSize = 3;
		final Integer childExpTypeSize = 2;
		RepoTestData testData = new RepoTestData(parentExpTypeSize, childExpTypeSize);

		// 既存 Existing record
		ExpTypeExpTypeLink__c orgObj = testData.expTypeExpTypeLinkObjs[0];
		ExpTypeExpTypeLinkEntity entityUd = new ExpTypeExpTypeLinkEntity();
		entityUd.setId(orgObj.Id);
		entityUd.expTypeParentId = testData.parentExpTypeObjs[2].Id;
		entityUd.expTypeChildId = testData.childExpTypeObjs[1].Id;

		// 新規 New record
		ExpTypeExpTypeLinkEntity entityNew = new ExpTypeExpTypeLinkEntity();
		entityNew.expTypeParentId = testData.parentExpTypeObjs[2].Id;
		entityNew.expTypeChildId = testData.childExpTypeObjs[1].Id;

		Repository.SaveResult result =
				new ExpTypeExpTypeLinkRepository().saveEntityList(new List<ExpTypeExpTypeLinkEntity>{entityUd, entityNew});

		System.assertEquals(true, result.isSuccessAll);
		ExpTypeExpTypeLink__c resObjUd = getSObject(orgObj.Id);
		assertSObject(entityUd, resObjUd);
		ExpTypeExpTypeLink__c resObjNew = getSObject(result.details[1].id);
		assertSObject(entityNew, resObjNew);
	}


	/**
	 * 取得したエンティティが期待値通りであることを確認する Verify entity details
	 * @param expObj Object to compare
	 * @param actEntity エンティティ Target entity to verify
	 */
	private static void assertEntity(ExpTypeExpTypeLink__c expObj, ExpTypeExpTypeLinkEntity actEntity) {

		// assert base entity details are retrieved
		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.ExpTypeParentId__c, actEntity.expTypeParentId);
		System.assertEquals(expObj.ExpTypeChildId__c, actEntity.expTypeChildId);
	}

	/**
	 * 保存したSObjectが期待値通りであることを確認する Verify saved sObject details
	 * @param expEntity エンティティ Entity to compare
	 * @param actObj Target object to verify
	 */
	private static void assertSObject(ExpTypeExpTypeLinkEntity expEntity, ExpTypeExpTypeLink__c actObj) {
		System.assertEquals(expEntity.expTypeParentId, actObj.ExpTypeParentId__c);
		System.assertEquals(expEntity.expTypeChildId, actObj.ExpTypeChildId__c);
	}
	
	/**
	 * 指定したIDのExpTypeExpTypeLink__cを取得する Retrieve ExpTypeExpTypeLink from the specified Id
	 */
	private static ExpTypeExpTypeLink__c getSObject(Id id) {
		return [
				SELECT 
					Id, 
					Name, 
					ExpTypeParentId__c, 
					ExpTypeChildId__c
				FROM ExpTypeExpTypeLink__c
				WHERE Id = :id];
	}
}
