/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group プランナー
 *
 * @description デイリーサマリー操作APIを実装するクラス
 */
public with sharing class DailySummaryResource {

	/**
	 * @description タスク１件分の情報を格納するクラス
	 */
	public class Task {
		public String jobId;							// ジョブID
		public String jobCode;							// ジョブコード
		public String jobName;							// ジョブ名
		public Boolean isDirectCharged;					// 直課 or 非直課
		public String workCategoryId;					// 作業分類ID
		public String workCategoryCode;					// 作業分類コード
		public String workCategoryName;					// 作業分類名
		public Boolean isDirectInput;					// 時間入力 or 割合入力
		public Integer volume;							// ボリューム
		public Integer ratio;							// 比率
		public Integer taskTime;						// 工数（分）
		public String taskNote;							// タスクの作業報告
		public List<WorkCategory> workCategoryList;		// 紐づく作業分類のリスト
		public List<Event> eventList;					// 紐づくイベントのリスト

		public Task() {
			workCategoryList = new List<WorkCategory>();
			eventList = new List<Event>();
		}

		/**
		 * サービスクラスのタスクからインスタンスを生成する
		 */
		public Task(TimeService.Task task) {
			this.jobId = task.jobId;
			this.jobCode = task.jobCode;
			this.jobName = task.jobName;
			this.isDirectCharged = task.isDirectCharged;
			this.workCategoryId = task.workCategoryId;
			this.workCategoryCode = task.workCategoryCode;
			this.workCategoryName = task.workCategoryName;
			this.isDirectInput = task.isDirectInput;
			this.volume = task.volume;
			this.ratio = task.ratio;
			this.taskTime = task.taskTime;
			this.taskNote = task.taskNote;
			this.eventList = new List<Event>();
			this.workCategoryList = new List<WorkCategory>();
			for (PlanEventEntity planEvent : task.events) {
				eventList.add(new Event(planEvent));
			}
		}

		/**
		 * APIパラメータのタスクから、サービスクラスのタスクへ変換する
		 */
		public TimeService.Task convert() {
			TimeService.Task task = new TimeService.Task(this.jobId, this.workCategoryId);
			task.jobCode = this.jobCode;
			task.jobName = this.jobName;
			task.isDirectCharged = this.isDirectCharged;
			task.workCategoryCode = this.workCategoryCode;
			task.workCategoryName = this.workCategoryName;
			task.isDirectInput = this.isDirectInput;
			task.volume = this.volume;
			task.ratio = this.ratio;
			task.taskTime = this.taskTime;
			task.taskNote = this.taskNote;
			return task;
		}

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// ジョブID
			if (String.isBlank(jobId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('jobId');
				throw e;
			}
			try {
				Id.ValueOf(jobId);
			} catch (Exception e) {
				throw new App.ParameterException('jobId', jobId);
			}
			// 時間入力 or 割合入力
			if (isDirectInput == null) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('isDirectInput');
				throw e;
			}
		}
	}

	/**
	 * @description 作業分類１件の情報を格納するクラス
	 */
	public class WorkCategory {
		public String id;		// 作業分類ID
		public String code;		// 作業分類コード
		public String name;		// 作業分類名

		public WorkCategory(WorkCategoryEntity workCategory) {
			id = workCategory.Id;
			code = workCategory.code;
			name = workCategory.nameL.getValue();
		}
	}

	/**
	 * @description 予定情報１件の情報を格納するクラス
	 */
	public class Event {
		public String id;				// イベントID
		public String subject;			// 件名
		public String startDateTime;	// 開始日時
		public String endDateTime;		// 終了日時

		public Event(PlanEventEntity planEvent) {
			this.id = planEvent.id;
			this.subject = planEvent.subject;
			this.startDateTime = planEvent.startDateTime.toString();
			this.endDateTime = planEvent.endDateTime.toString();
		}
	}

	/**
	 * @description デイリーサマリーデータ取得条件を格納するクラス
	 */
	public class GetParam implements RemoteApi.RequestParam {
		/** 社員ID */
		public String empId;
		/** 日付 */
		public String targetDate;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 社員ID
			if (String.isNotBlank(empId)) {
				try {
					Id.ValueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', empId);
				}
			}
			// 日付
			if (String.isBlank(targetDate)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('targetDate');
				throw e;
			}
			try {
				AppDate.parse(targetDate);
			} catch (Exception e) {
				throw new App.ParameterException('targetDate', targetDate);
			}
		}
	}

	/**
	 * @description デイリーサマリーデータ取得結果を格納するクラス
	 */
	public class GetResult implements RemoteApi.ResponseParam {
		public String targetDate;					// 日付
		public String status;						// 申請ステータス
		public String note;							// 日次の作業報告
		public String output;						// アウトプット
		public Integer realWorkTime;				// 勤怠の実労働時間（分）
		public Boolean isTemporaryWorkTime;			// 実労働時間暫定フラグ
		public List<Task> taskList;					// タスクのリスト

		public GetResult() {
			taskList = new List<Task>();
		}
	}

	/**
	 * @description デイリーサマリーデータ取得APIを実装するクラス
	 */
	public with sharing class GetApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		final private List<TimeResourcePermission.Permission> requiredPermissionList =
				new List<TimeResourcePermission.Permission>{TimeResourcePermission.Permission.VIEW_TIME_TRACK_BY_DELEGATE};

		@testVisible
		private TimeService service = new TimeService();

		@testVisible
		private TimeSummaryRepository repository = new TimeSummaryRepository();

		/**
		 * @description デイリーサマリーデータを取得する
		 * @param  req リクエストパラメータ(GetParam)
		 * @return レスポンス(GetResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// TODO 廃止する
			TimeResource.Util util = new TimeResource.Util();

			// リクエストパラメータを取得
			GetParam param = (GetParam)req.getParam(GetParam.class);
			param.validate();

			// API実行権限チェック
			new TimeResourcePermission().hasExecutePermission(requiredPermissionList, param.empId);

			// 社員情報を取得
			EmployeeBaseEntity employee;
			EmployeeService employeeService = new EmployeeService();
			if (String.isBlank(param.empId)) {
				// 社員IDが設定されていない場合、ログインユーザから社員情報を取得する
				employee = employeeService.getEmployeeByUserIdFromCache(UserInfo.getUserId(), AppDate.today());
			} else {
				employee = employeeService.getEmployeeFromCache(param.empId, AppDate.today());
			}

			// 対象日
			AppDate targetDate = new Appdate(param.targetDate);

			// タスクの一覧を取得
			GetResult response = new GetResult();
			response.targetDate = param.targetDate;
			List<TimeService.Task> taskList = service.getTaskList(employee, targetDate);
			for (TimeService.Task task : taskList) {
				response.taskList.add(new Task(task));
			}

			// 対象日の内訳データを取得
			TimeRecordEntity timeRecord = repository.getRecord(employee.id, targetDate);
			if (timeRecord != null) {
				TimeSummaryEntity summary = repository.getSummary(timeRecord.timeSummaryId);
				response.status = TimeRequestEntity.getStatusLabel(summary.status, summary.cancelType);
				response.note = timeRecord.note;
				response.output = timeRecord.output;
			}

			// 対象日の実労働時間を取得
			TimeService.workTime workTime = service.getWorkTime(employee.id, targetDate);
			if (workTime != null) {
				response.realWorkTime = workTime.value;
				response.isTemporaryWorkTime = workTime.isTemporary;
			}

			// 作業分類の選択候補を設定
			Map<JobEntity, List<WorkCategoryEntity>> activeTasks = service.getActiveTasks(employee, targetDate);
			for (Task task : response.taskList) {
				for (WorkCategoryEntity workCategory : getWorkCategories(activeTasks, task.jobId)) {
					task.workCategoryList.add(new WorkCategory(workCategory));
				}
			}
			return response;
		}

		/*
		 * タスクの一覧から、ジョブに紐づく作業分類の一覧を取得する
		 * @param activeTasks 有効なタスクの一覧
		 * @param jobId 抽出対象のジョブID
		 * @return ジョブに紐づく作業分類の一覧
		 */
		private List<WorkCategoryEntity> getWorkCategories(Map<JobEntity, List<WorkCategoryEntity>> activeTasks, String jobId) {
			for (JobEntity job : activeTasks.keySet()) {
				if (job.id.equals(jobId)) {
					return activeTasks.get(job);
				}
			}
			return new List<WorkCategoryEntity>();
		}
	}


	/**
	 * @description デイリーサマリーデータ保存内容を格納するクラス
	 */
	public class SaveParam implements RemoteApi.RequestParam {
		/** 社員ID */
		public String empId;
		/** 日付 */
		public String targetDate;
		/** 日次の作業報告 */
		public String note;
		/** アウトプット */
		public String output;
		/** タスク(ジョブ・作業分類・作業時間)のリスト */
		public List<Task> taskList;

		public SaveParam() {
			taskList = new List<Task>();
		}
		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 社員ID
			if (String.isNotBlank(empId)) {
				try {
					Id.ValueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', empId);
				}
			}
			// 日付
			if (String.isBlank(targetDate)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('targetDate');
				throw e;
			}
			try {
				AppDate.parse(targetDate);
			} catch (Exception e) {
				throw new App.ParameterException('targetDate', targetDate);
			}
			// タスクのリスト(IDと入力方法)
			for (Task task : taskList) {
				task.validate();
			}
		}
	}

	/**
	 * @description デイリーサマリーデータ保存APIを実装するクラス
	 */
	public with sharing class SaveApi extends RemoteApi.ResourceBase {

		private TimeService service = new TimeService();

		/**
		 * @description デイリーサマリーデータを保存する
		 * @param  req リクエストパラメータ(SaveParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			TimeResource.Util util = new TimeResource.Util();
			EmployeeService employeeService = new EmployeeService();

			// リクエストパラメータを取得
			SaveParam param = (SaveParam)req.getParam(SaveParam.class);
			param.validate();

			// 社員情報を取得
			EmployeeBaseEntity employee;
			if (String.isBlank(param.empId)) {
				// 社員IDが設定されていない場合、ログインユーザから社員情報を取得する
				employee = employeeService.getEmployeeByUserIdFromCache(UserInfo.getUserId(), AppDate.today());
			} else {
				employee = employeeService.getEmployeeFromCache(param.empId, AppDate.today());
			}
			// デイリーサマリー入力日と現在日が異なる場合に対応するために全ての履歴を取得する
			employee = employeeService.getEmployeeFromCache(employee.id, null);

			// 対象日
			AppDate targetDate = new Appdate(param.targetDate);

			//工数設定の有無を検証
			TimeSettingService settingService = new TimeSettingService();
			settingService.validateTimeSetting(employee.id, targetDate);

			// 工数明細を保存
			TimeRecordEntity timeRecord = service.getTimeRecord(employee, targetDate);
			timeRecord.note = param.note;
			timeRecord.output = param.output;
			service.saveTimeRecord(timeRecord);

			// 工数内訳を保存
			List<TimeService.Task> tasks = convertTasks(param.taskList);
			service.saveTasks(employee, targetDate, tasks);

			// 個人情報にタスクの履歴を保存
			service.saveTaskHistory(employee, targetDate, tasks);
			return null;
		}

		private List<TimeService.Task> convertTasks(List<Task> tasks) {
			List<TimeService.Task> result = new List<TimeService.Task>();
			for (Task task : tasks) {
				result.add(task.convert());
			}
			return result;
		}
	}
}