/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test for repository of Vendor
 */
@isTest
private class ExpVendorRepositoryTest {

	/** Test Data */
	private class RepoTestData {

		/** Country object */
		public ComCountry__c countryObj;
		/** Company object */
		public ComCompany__c companyObj;

		/** Constructor */
		public RepoTestData() {
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
		}

		/**
		 * Create Accounting Period objects
		 */
		public List<ExpVendor__c> createExpVendors(String code, Integer size) {
			return ComTestDataUtility.createExpVendors(code, this.companyObj.Id, size);
		}

		/**
		 * Create Company object
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}
	}

	/**
	 * Test for getEntity method
	 * Confirm being enable to get entity by specified ID
	 */
	@isTest
	static void getEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpVendor__c> testCurrencyRate = testData.createExpVendors('SearchTest', recordSize);
		ExpVendor__c targetObj = testCurrencyRate[1];

		ExpVendorRepository repo = new ExpVendorRepository();
		ExpVendorEntity resEntity;

		// Check existing ID
		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);

		// Check not existing ID
		delete targetObj;
		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * Test for searchEntity method
	 * Confirm enable to search data properly
	 */
	@isTest
	static void searchEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		testData.createCompany('Search Test Co.');
		List<ExpVendor__c> testGroups = testData.createExpVendors('SearchTest', recordSize);
		ExpVendor__c targetObj = testGroups[1];


		ExpVendorRepository repo = new ExpVendorRepository();
		ExpVendorRepository.SearchFilter filter;
		List<ExpVendorEntity> resEntities;

		// Search by ID
		filter = new ExpVendorRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
		// Check fields value
		assertFieldValue(targetObj, resEntities[0]);
	}

	/**
	 * Check fields value
	 * @param sObj Expected value
	 * @param actEntity Actual value
	 */
	private static void assertFieldValue(ExpVendor__c sObj, ExpVendorEntity entity) {
		System.assertEquals(sObj.Id, entity.id);
		System.assertEquals(sObj.Active__c, entity.active);
		System.assertEquals(sObj.Address__c, entity.address);
		System.assertEquals(sObj.BankAccountType__c, entity.bankAccountType.value);
		System.assertEquals(sObj.BankAccountNumber__c, entity.bankAccountNumber);
		System.assertEquals(sObj.BankCode__c, entity.bankCode);
		System.assertEquals(sObj.BankName__c, entity.bankName);
		System.assertEquals(sObj.BranchAddress__c, entity.branchAddress);
		System.assertEquals(sObj.BranchCode__c, entity.branchCode);
		System.assertEquals(sObj.BranchName__c, entity.branchName);
		System.assertEquals(sObj.Code__c, entity.code);
		System.assertEquals(sObj.CorrespondentBankAddress__c, entity.correspondentBankAddress);
		System.assertEquals(sObj.CorrespondentBankName__c, entity.correspondentBankName);
		System.assertEquals(sObj.CorrespondentBranchName__c, entity.correspondentBranchName);
		System.assertEquals(sObj.CorrespondentSwiftCode__c, entity.correspondentSwiftCode);
		System.assertEquals(sObj.CompanyId__c, entity.companyId);
		System.assertEquals(sObj.Country__c, entity.country);
		System.assertEquals(sObj.CurrencyCode__c, entity.currencyCode.value);
		System.assertEquals(sObj.IsWithholdingTax__c, entity.isWithholdingTax);
		System.assertEquals(sObj.Name_L0__c, entity.nameL0);
		System.assertEquals(sObj.Name_L1__c, entity.nameL1);
		System.assertEquals(sObj.Name_L2__c, entity.nameL2);
		System.assertEquals(sObj.PayeeName__c, entity.payeeName);
		System.assertEquals(sObj.PaymentTerm__c, entity.paymentTerm);
		System.assertEquals(sObj.SwiftCode__c, entity.swiftCode);
		System.assertEquals(sObj.ZipCode__c, entity.zipCode);
		System.assertEquals(sObj.UniqKey__c, entity.uniqKey);
	}

}