/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 工数
  *
  * 工数のテストデータクラス
  */
@isTest
@TestVisible
private class TimeTestData {

	/** 各データの有効開始日 */
	private final AppDate validFrom;

	/** ユーザ */
	public User user {get; private set;}
	/** ユーザ（マネージャー）@Deprecated 廃止予定 */
	public User manager {get; private set;}
	/** ユーザ（承認者） */
	public User approver {get; private set;}
	/** 会社 */
	public ComCompany__c company {get; private set;}
	/** 部署 */
	public ComDeptBase__c dept {get; private set;}
	/** 部署履歴 */
	public ComDeptHistory__c deptHistory {get; private set;}
	/** 権限 */
	public ComPermission__c permission {get; private set;}
	/** 社員 */
	public ComEmpBase__c emp {get; private set;}
	/** 社員履歴 */
	public ComEmpHistory__c empHistory {get; private set;}
	/** 工数設定ベース */
	public TimeSettingBase__c timeSetting {get; private set;}
	/** 工数設定履歴 */
	public TimeSettingHistory__c timeSettingHistory {get; private set;}
	/** 上長 @Deprecated 廃止予定 */
	public ComEmpBase__c empManager {get; private set;}
	/** 承認者 */
	public ComEmpBase__c empApprover {get; private set;}
	/** ジョブタイプ */
	public ComJobType__c jobType {get; private set;}
	/** ジョブ */
	public List<ComJob__c> jobList {get; private set;}
	/** 作業分類 */
	public List<TimeWorkCategory__c> workCategoryList {get; private set;}
	/** ジョブタイプ別作業分類 */
	public List<TimeJobTypeWorkCategory__c> jobTypeWorkCategoryList {get; private set;}

	/**
	 * コンストラクタ
	 */
	public TimeTestData() {
		this(AppDate.newInstance(2016, 1, 1));
	}

	/**
	 * コンストラクタ
	 * マスタデータの有効期間を指定してデータを生成する
	 * @param validFrom 有効開始日 この日より1年間を有効期限とする
	 *                  この値を使用して以下のマスタの有効期限を設定する
	 *                  ・ジョブ
	 *                  ・作業分類
	 */
	public TimeTestData(AppDate validFrom) {
		this.validFrom = validFrom;
		user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		manager = ComTestDataUtility.createUser('manager', 'ja', 'ja_JP');
		approver = ComTestDataUtility.createUser('approver', 'ja', 'ja_JP');
		company = ComTestDataUtility.createTestCompany();
		dept = ComTestDataUtility.createDepartmentWithHistory('test', company.Id);
		permission = ComTestDataUtility.createPermission('test', company.Id);
		emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, dept.Id, user.Id, permission.Id);
		empManager = ComTestDataUtility.createEmployeeWithHistory('manager', company.Id, dept.Id, manager.Id, permission.Id);
		empApprover = ComTestDataUtility.createEmployeeWithHistory('approver', company.Id, dept.Id, approver.Id, permission.Id);
		jobType = ComTestDataUtility.createJobType('test', company.Id);
		jobList = createJobList(validFrom);
		workCategoryList = createWorkCategoryList(validFrom);
		jobTypeWorkCategoryList = createJobTypeWorkCategory();

		// 組織設定を作成
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);

		// 会社のデフォルト言語を設定
		company.Language__c = 'ja';
		update company;
		// 社員の上長・承認者・有効期間を設定
		empHistory = [SELECT Id, BaseId__c, UniqKey__c, PermissionId__c, ManagerBaseId__c, ValidFrom__c, ValidTo__c, DepartmentBaseId__c FROM ComEmpHistory__c WHERE BaseId__c = :emp.Id];
		empHistory.ManagerBaseId__c = empManager.Id;
		empHistory.ApproverBase01Id__c = empApprover.Id;
		empHistory.ValidFrom__c = ParentChildHistoryEntity.VALID_FROM_MIN.getDate();
		empHistory.ValidTo__c = ParentChildHistoryEntity.VALID_TO_MAX.getDate();
		update empHistory;
		// 上長の有効期間を設定
		ComEmpHistory__c empManagerHistory = [SELECT ValidFrom__c, ValidTo__c FROM ComEmpHistory__c WHERE BaseId__c = :empManager.Id];
		empManagerHistory.ValidFrom__c = ParentChildHistoryEntity.VALID_FROM_MIN.getDate();
		empManagerHistory.ValidTo__c = ParentChildHistoryEntity.VALID_TO_MAX.getDate();
		update empManagerHistory;
		// 承認者の有効期間を設定
		ComEmpHistory__c empApproverHistory = [SELECT ValidFrom__c, ValidTo__c FROM ComEmpHistory__c WHERE BaseId__c = :empApprover.Id];
		empApproverHistory.ValidFrom__c = validFrom.getDate();
		empApproverHistory.ValidTo__c = ParentChildHistoryEntity.VALID_TO_MAX.getDate();
		update empApproverHistory;
		// 部署履歴の期間を変更
		deptHistory = [SELECT Id, Name_L0__c, BaseId__c, ValidFrom__c, ValidTo__c FROM ComDeptHistory__c WHERE Id = :dept.CurrentHistoryId__c];
		deptHistory.ValidFrom__c = ParentChildHistoryEntity.VALID_FROM_MIN.getDate();
		deptHistory.ValidTo__c = ParentChildHistoryEntity.VALID_TO_MAX.getDate();
		update deptHistory;

		// デフォルトで工数設定を作成する
		timeSetting = createSettingBase();
		timeSettingHistory = createSettingHistory(timeSetting);
	}

	/**
	 * 工数設定（ベース）を作成し社員に紐づける
	 * @return 工数設定（ベース）オブジェクト
	 */
	public TimeSettingBase__c createSettingBase() {
		TimeSettingBase__c settingBase = new TimeSettingBase__c(
			Code__c = '0001',
			UniqKey__c = company.Code__c + '-0001',
			CompanyId__c = company.Id,
			SummaryPeriod__c = 'Month',
			StartDateOfMonth__c = 1,
			MonthMark__c = 'BeginBase',
			UseRequest__c = true
		);
		insert settingBase;

		// 社員に紐付ける
		empHistory.TimeSettingBaseId__c = settingBase.Id;
		upsert empHistory;

		return settingBase;
	}

	/**
	 * 工数設定（履歴）を作成する
	 * @param 工数設定（ベース）オブジェクト
	 * @return 工数設定（履歴）オブジェクト
	 */
	public TimeSettingHistory__c createSettingHistory(TimeSettingBase__c settingBase) {
		TimeSettingHistory__c settingHistory = new TimeSettingHistory__c(
			BaseId__c = settingBase.Id,
			Name_L0__c='工数設定',
			UniqKey__c = '00012017010120171231',
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = validFrom.getDate().addYears(1),
			Removed__c = false
		);
		insert settingHistory;

		return settingHistory;
	}

	/**
	 * 標準ユーザと社員エンティティを作成して登録する
	 * @param code 社員コード
	 * @return 標準ユーザの社員
	 */
	public ComEmpBase__c createEmployeeStandardUser(String code) {
		AppDateRange validRange = new AppDateRange(this.validFrom, ParentChildHistoryEntity.VALID_TO_MAX);
		User standardUser = ComTestDataUtility.createStandardUser(code);
		return ComTestDataUtility.createEmployeeWithHistory(code, this.company.id, this.dept.id, standardUser.Id, this.permission.id, validRange);
	}

	/**
	 * 社員からSystem.runAs(User)用のユーザを作成する
	 */
	public User createRunAsUserFromEmployee(ComEmpBase__c employee) {
		return new User(Id = employee.UserId__c);
	}

	/**
	 * 工数サマリーエンティティを作成する
	 * @return 工数サマリーエンティティ
	 * TODO 戻り値にIDが設定されていない。理由もわからないため当メソッドは廃止する
	 */
	public TimeSummaryEntity createSummaryEntity() {
		return createSummaryEntity(AppDate.newInstance(2017, 7, 1));
	}
	/**
	 * 工数サマリーエンティティを作成する
	 * @return 工数サマリーエンティティ
	 * TODO 戻り値にIDが設定されていない。理由もわからないため当メソッドは廃止する
	 */
	public TimeSummaryEntity createSummaryEntity(AppDate startDate) {
		// 工数サマリーを作成
		TimeSummary__c summary = new TimeSummary__c(
			Name = 'test',
			UniqKey__c = emp.Code__c + startDate.format(AppDate.FormatType.YYYYMM) + '1',
			EmployeeHistoryId__c = emp.CurrentHistoryId__c,
			TimeSettingHistoryId__c = timeSettingHistory.Id,
			SummaryName__c = startDate.format(AppDate.FormatType.YYYYMM),
			SubNo__c = 1,
			StartDate__c = startDate.getDate(),
			EndDate__c = startDate.getDate().addMonths(1).addDays(-1)
		);
		insert summary;

		TimeSummaryRepository summaryRep = new TimeSummaryRepository();
		return summaryRep.createSummaryEntity(summary);
	}

	/**
	 * 月次の工数サマリーを作成する
	 * @param name Name項目
	 * @param startDate サマリー開始日
	 * @return DB登録済みの工数サマリー
	 */
	public TimeSummary__c createMonthlySummary(AppDate startDate) {
		return createSummary(startDate, startDate.addMonths(1).minusDays(1));
	}

	/**
	 * 工数サマリーを作成する
	 * @param name Name項目
	 * @param startDate サマリー開始日
	 * @param endDate サマリー終了日
	 * @return DB登録済みの工数サマリー
	 */
	public TimeSummary__c createSummary(AppDate startDate, AppDate endDate) {
		TimeSummary__c summary = new TimeSummary__c(
				Name = startDate.formatYYYYMMDD() + '-' + endDate.formatYYYYMMDD(),
				UniqKey__c = emp.Code__c + startDate.format() + ':' + endDate.format(),
				EmployeeHistoryId__c = emp.CurrentHistoryId__c,
				TimeSettingHistoryId__c = timeSettingHistory.Id,
				SummaryName__c = startDate.formatYYYYMMDD() + '-' + endDate.formatYYYYMMDD(),
				SubNo__c = 1,
				StartDate__c = startDate.getDate(),
				EndDate__c = endDate.getDate()
		);
		insert summary;
		return summary;
	}

	/**
	 * 工数サマリーに紐づく工数明細を作成する
	 * @param summary 対処の工数サマリー
	 * @return 作成した工数明細（サマリーの開始日〜終了日の全てのレコードを作成する）
	 */
	public List<TimeRecord__c> createRecordList(TimeSummary__c summary) {
		List<TimeRecord__c> records = new List<TimeRecord__c>();
		AppDateRange range = new AppDateRange(AppDate.valueOf(summary.StartDate__c), AppDate.valueOf(summary.EndDate__c));
		for (AppDate d : range.values()) {
			records.add(new TimeRecord__c(
					Name = d.formatYYYYMMDD(),
					UniqKey__c = summary.id + d.formatYYYYMMDD(),
					TimeSummaryId__c = summary.id,
					Date__c = d.getDate())
			);
		}
		insert records;
		return records;
	}

	/**
	 * 工数明細に紐づく工数内訳を作成する
	 * このメソッドではジョブのみ設定した工数内訳を作成します。
	 * 他の項目をテストで使用する場合は、呼び出し元で設定してください。
	 * @param record 工数明細
	 * @param job ジョブ
	 * @return 作成した工数内訳
	 */
	public TimeRecordItem__c createRecordItem(TimeRecord__c record, ComJob__c job) {
		return createRecordItem(record, job, null);
	}

	/**
	 * 工数明細に紐づく工数内訳を作成する
	 * このメソッドではジョブと作業分類のみ設定した工数内訳を作成します。
	 * 他の項目をテストで使用する場合は、呼び出し元で設定してください。
	 * @param record 工数明細
	 * @param job ジョブ
	 * @param workCategory 作業分類
	 * @return 作成した工数内訳
	 */
	public TimeRecordItem__c createRecordItem(TimeRecord__c record, ComJob__c job, TimeWorkCategory__c workCategory) {
		TimeRecordItem__c item = new TimeRecordItem__c(
				TimeRecordId__c = record.Id,
				JobId__c = job.Id,
				WorkCategoryId__c = workCategory == null ? null : workCategory.Id
		);
		insert item;
		return item;
	}

	/**
	 * 工数明細に紐づく工数内訳を作成する
	 * @param recordList 工数明細リスト
	 * @param itemList コピー対象の内訳
	 * @return 作成した工数内訳のリスト
	 */
	public List<TimeRecordItem__c> copyItemToMultiRecord(List<TimeRecord__c> recordList,
			List<TimeRecordItem__c> itemList) {
		List<TimeRecordItem__c> recordItemList = new List<TimeRecordItem__c>();
		for (TimeRecord__c record : recordList) {
			for (TimeRecordItem__c item : itemList) {
				TimeRecordItem__c recordItem = item.clone(false, true);
				recordItem.TimeRecordId__c = record.Id;
				recordItemList.add(recordItem);
			}
		}
		insert recordItemList;
		return recordItemList;
	}

	/**
	 * 工数内訳に紐づく工数内訳スナップショットを作成する
	 * @param item 工数内訳
	 * @param job ジョブ階層1
	 * @return 作成した工数内訳スナップショット
	 */
	public TimeRecordItemSnapshot__c createSnapshot(TimeRecordItem__c item, ComJob__c job, TimeWorkCategory__c workCategory) {
		TimeRecordItemSnapshot__c snapshot = new TimeRecordItemSnapshot__c(
				TimeRecordItemId__c = item.Id,
				JobIdLevel1__c = job.Id,
				JobCodeLevel1__c = job.Code__c,
				JobNameLevel1_L0__c = job.Name_L0__c,
				JobNameLevel1_L1__c = job.Name_L1__c,
				JobNameLevel1_L2__c = job.Name_L2__c
		);
		if (workCategory != null) {
			snapshot.WorkCategoryId__c = workCategory.Id;
			snapshot.WorkCategoryCode__c = workCategory.Code__c;
			snapshot.WorkCategoryName_L0__c = workCategory.Name_L0__c;
			snapshot.WorkCategoryName_L1__c = workCategory.Name_L1__c;
			snapshot.WorkCategoryName_L2__c = workCategory.Name_L2__c;
		}
		insert snapshot;
		return snapshot;
	}

	/**
	 * 工数確定申請オブジェクトを作成する（ステータスは申請中）
	 * @param summary 工数サマリー
	 * @return 登録した工数確定申請（呼び出し元で工数サマリーと紐付けること）
	 */
	public TimeRequest__c createRequest(TimeSummary__c summary) {
		TimeRequest__c request = new TimeRequest__c(
				Name = 'Request ' + summary.Name,
				TimeSummaryId__c = summary.id,
				Status__c = AppRequestStatus.PENDING.value
		);
		insert request;
		return request;
	}

	/**
	 * @deprecated 廃止予定
	 * 工数確定申請エンティティを作成する
	 * @param 工数サマリーエンティティ
	 * @return 工数確定申請エンティティ
	 */
	public TimeRequestEntity createRequestEntity(TimeSummaryEntity summary) {
		// 部署履歴の期間を変更
		ComDeptHistory__c deptHistory = [
			SELECT
				Id,
				ValidFrom__c,
				ValidTo__c
			FROM ComDeptHistory__c
			WHERE Id = :dept.CurrentHistoryId__c][0];
		deptHistory.ValidFrom__c = AppDate.convertDate(summary.startDate.addDays(-30));
		deptHistory.ValidTo__c = AppDate.convertDate(summary.endDate.addDays(30));
		update deptHistory;

		TimeRequest__c request = createRequest(summary.createSObject(), manager.Id);

		// 作成した申請をサマリーに紐づけ
		summary.requestId = request.Id;

		return new TimeRequestEntity(request);
	}

	/**
	 * 工数確定申請エンティティを作成する
	 * 必要があれば引数に項目を追加してください
	 * @param 工数サマリーオブジェクト
	 * @param 承認者のユーザーID
	 * @return 工数確定申請オブジェクト
	 */
	public TimeRequest__c createRequest(TimeSummary__c summary, Id approver01Id) {

		// 工数確定申請を作成
		TimeRequest__c request = new TimeRequest__c(
			Name = 'test',
			TimeSummaryId__c = summary.Id,
			Status__c = 'Pending',
			ConfirmationRequired__c = false,
			CancelType__c = null,
			RequestTime__c = System.now(),
			Comment__c = 'コメント',
			EmployeeHistoryId__c = emp.CurrentHistoryId__c,
			DepartmentHistoryId__c = dept.CurrentHistoryId__c,
			ActorHistoryId__c = emp.CurrentHistoryId__c,
			Approver01Id__c = approver01Id,
			OwnerId = emp.UserId__c
		);
		insert request;

		// 作成した申請をサマリーに紐づけ
		summary.RequestId__c = request.Id;
		update summary;

		return request;
	}

	/**
	 * テスト用のジョブを作成する
	 * @param 会社ID
	 * @return ジョブのリスト
	 */
	private List<ComJob__c> createJobList(AppDate validFrom) {
		List<ComJob__c> jobList = new List<ComJob__c>();
		jobList.add(new ComJob__c(
			Name = 'ジョブ1',
			Name_L0__c = 'ジョブ1',
			Name_L1__c = 'ジョブ1',
			Name_L2__c = 'ジョブ1',
			Code__c = '0001',
			UniqKey__c = '0001',
			CompanyId__c = company.Id,
			JobTypeId__c = this.jobType.Id,
			ParentId__c = null,
			DirectCharged__c = true,
			SelectabledTimeTrack__c = true,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate()
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ2',
			Name_L0__c = 'ジョブ2',
			Name_L1__c = 'ジョブ2',
			Name_L2__c = 'ジョブ2',
			Code__c = '0002',
			UniqKey__c = '0002',
			CompanyId__c = company.Id,
			JobTypeId__c = this.jobType.Id,
			ParentId__c = null,
			DirectCharged__c = true,
			SelectabledTimeTrack__c = true,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate()
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ3',
			Name_L0__c = 'ジョブ3',
			Name_L1__c = 'ジョブ3',
			Name_L2__c = 'ジョブ3',
			Code__c = '0003',
			UniqKey__c = '0003',
			CompanyId__c = company.Id,
			JobTypeId__c = this.jobType.Id,
			ParentId__c = null,
			DirectCharged__c = true,
			SelectabledTimeTrack__c = true,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = validFrom.getDate().addYears(1)
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ4',
			Name_L0__c = 'ジョブ4',
			Name_L1__c = 'ジョブ4',
			Name_L2__c = 'ジョブ4',
			Code__c = '0004',
			UniqKey__c = '0004',
			CompanyId__c = company.Id,
			JobTypeId__c = this.jobType.Id,
			ParentId__c = null,
			DirectCharged__c = true,
			SelectabledTimeTrack__c = true,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate()
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ5',
			Name_L0__c = 'ジョブ5',
			Name_L1__c = 'ジョブ5',
			Name_L2__c = 'ジョブ5',
			Code__c = '0005',
			UniqKey__c = '0005',
			CompanyId__c = company.Id,
			JobTypeId__c = this.jobType.Id,
			ParentId__c = null,
			DirectCharged__c = true,
			SelectabledTimeTrack__c = true,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate()
		));
		insert jobList;

		insert new ComJob__c(
			Name = '有効期間外:当日失効',
			Name_L0__c = 'ジョブ6',
			Name_L1__c = 'ジョブ6',
			Name_L2__c = 'ジョブ6',
			Code__c = '0006',
			UniqKey__c = '0006',
			CompanyId__c = company.Id,
			JobTypeId__c = this.jobType.Id,
			ParentId__c = null,
			DirectCharged__c = true,
			SelectabledTimeTrack__c = true,
			ValidFrom__c = validFrom.getDate().addYears(-1),
			ValidTo__c = validFrom.getDate()
		);

		return jobList;
	}

	/**
	 * テスト用の作業分類を作成する
	 * @param 会社ID
	 * @return 作業分類のリスト
	 */
	private List<TimeWorkCategory__c> createWorkCategoryList(AppDate validFrom) {
		List<TimeWorkCategory__c> workCategoryList = new List<TimeWorkCategory__c>();
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類1-1',
			Name_L0__c='作業分類1-1',
			Name_L1__c='作業分類1-1',
			Name_L2__c='作業分類1-1',
			Code__c='01-001',
			UniqKey__c='01-001',
			CompanyId__c = company.Id,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate(),
			Order__c = 1
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類1-2',
			Name_L0__c='作業分類1-2',
			Name_L1__c='作業分類1-2',
			Name_L2__c='作業分類1-2',
			Code__c='01-002',
			UniqKey__c='01-002',
			CompanyId__c = company.Id,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate(),
			Order__c = 2
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類1-3',
			Name_L0__c='作業分類1-3',
			Name_L1__c='作業分類1-3',
			Name_L2__c='作業分類1-3',
			Code__c='01-003',
			UniqKey__c='01-003',
			CompanyId__c = company.Id,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate(),
			Order__c = 3
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類2-1',
			Name_L0__c='作業分類2-1',
			Name_L1__c='作業分類2-1',
			Name_L2__c='作業分類2-1',
			Code__c='02-001',
			UniqKey__c='02-001',
			CompanyId__c = company.Id,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate(),
			Order__c = 4
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類2-2',
			Name_L0__c='作業分類2-2',
			Name_L1__c='作業分類2-2',
			Name_L2__c='作業分類2-2',
			Code__c='02-002',
			UniqKey__c='02-002',
			CompanyId__c = company.Id,
			ValidFrom__c = validFrom.getDate(),
			ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate(),
			Order__c = 5
		));
		insert workCategoryList;

		return workCategoryList;
	}

	private List<TimeJobTypeWorkCategory__c> createJobTypeWorkCategory() {
		List<TimeJobTypeWorkCategory__c> jobTypeWorkCategoryList = new List<TimeJobTypeWorkCategory__c>();
		for (TimeWorkCategory__c workCategory : workCategoryList) {
			jobTypeWorkCategoryList.add(new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategory.Id));
		}
		insert jobTypeWorkCategoryList;
		return jobTypeWorkCategoryList;
	}
}