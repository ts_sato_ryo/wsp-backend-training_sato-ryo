/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Repository of Expense Request
 */
public with sharing class ExpRequestRepository extends Repository.BaseRepository {

	/** Max record number to fetch */
	private static Integer MAX_SEARCH_RECORDS_NUMBER = 10000;

	/** Number of Extended Items for each type */
	private static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/** String representation of Extended Item Field of the sObject */
	private static final String EXTENDED_ITEM_TEXT = ExpRequest__c.ExtendedItemText01Value__c.getDescribe().getName().substringBefore('01');
	private static final String EXTENDED_ITEM_PICKLIST = ExpRequest__c.ExtendedItemPicklist01Value__c.getDescribe().getName().substringBefore('01');
	private static final String EXTENDED_ITEM_LOOKUP = ExpRequest__c.ExtendedItemLookup01Value__c.getDescribe().getName().substringBefore('01');
	private static final String EXTENDED_ITEM_DATE = ExpRequest__c.ExtendedItemDate01Value__c.getDescribe().getName().substringBefore('01');

	private static final String REPORT_TYPE_EXTENDED_ITEM_TEXT = ExpReportType__c.ExtendedItemText01TextId__c.getDescribe().getName().substringBefore('01');
	private static final String REPORT_TYPE_EXTENDED_ITEM_PICKLIST = ExpReportType__c.ExtendedItemPicklist01TextId__c.getDescribe().getName().substringBefore('01');
	private static final String REPORT_TYPE_EXTENDED_ITEM_LOOKUP = ExpReportType__c.ExtendedItemLookup01TextId__c.getDescribe().getName().substringBefore('01');
	private static final String REPORT_TYPE_EXTENDED_ITEM_DATE = ExpReportType__c.ExtendedItemDate01TextId__c.getDescribe().getName().substringBefore('01');

	private static final String EXTENDED_ITEM_USED_IN = 'UsedIn__c';
	private static final String EXTENDED_ITEM_REQUIRED_FOR = 'RequiredFor__c';
	private static final String EXTENDED_ITEM_LOOKUP_ID = 'Id__c';
	private static final String EXTENDED_ITEM_ID = 'TextId__c';
	private static final String EXTENDED_ITEM_Value = 'Value__c';

	/** Do not execute query with 'FOR UPDATE' */
	private final static Boolean NOT_FOR_UPDATE = false;

	private static final String COST_CENTER_HISTORY_R = ExpReport__c.CostCenterHistoryId__c.getDescribe().getRelationshipName();
	private static final String COST_CENTER_BASE_R = ComCostCenterHistory__c.BaseId__c.getDescribe().getRelationshipName();

	/** ジョブのリレーション名 */
	private static final String JOB_R = ExpRequest__c.JobId__c.getDescribe().getRelationshipName();

	/** 経費申請タイプのリレーション名  Relation (Expense Report Type) */
	private static final String EXP_REPORT_TYPE_R = ExpRequest__c.ExpReportTypeId__c.getDescribe().getRelationshipName();

	/** Relation (Vendor) */
	private static final String VENDOR_R = ExpRequest__c.VendorId__c.getDescribe().getRelationshipName();

	/** Target fields to fetch data */
	private static final List<String> SELECT_FIELD_SET;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpRequest__c.Id,
			ExpRequest__c.Name,
			ExpRequest__c.expRequestApprovalId__c,
			ExpRequest__c.RecordingDate__c,
			ExpRequest__c.ScheduledDate__c,
			ExpRequest__c.TotalAmount__c,
			ExpRequest__c.RequestNo__c,
			ExpRequest__c.Prefix__c,
			ExpRequest__c.SequenceNo__c,
			ExpRequest__c.DepartmentHistoryId__c,
			ExpRequest__c.EmployeeHistoryId__c,
			ExpRequest__c.CostCenterHistoryId__c,
			ExpRequest__c.JobId__c,
			ExpRequest__c.ExpReportTypeId__c,
			ExpRequest__c.VendorId__c,
			ExpRequest__c.PaymentDueDate__c,
			ExpRequest__c.ExtendedItemDate01Value__c,
			ExpRequest__c.ExtendedItemDate02Value__c,
			ExpRequest__c.ExtendedItemDate03Value__c,
			ExpRequest__c.ExtendedItemDate04Value__c,
			ExpRequest__c.ExtendedItemDate05Value__c,
			ExpRequest__c.ExtendedItemDate06Value__c,
			ExpRequest__c.ExtendedItemDate07Value__c,
			ExpRequest__c.ExtendedItemDate08Value__c,
			ExpRequest__c.ExtendedItemDate09Value__c,
			ExpRequest__c.ExtendedItemDate10Value__c,
			ExpRequest__c.ExtendedItemLookup01Id__c,
			ExpRequest__c.ExtendedItemLookup01Value__c,
			ExpRequest__c.ExtendedItemLookup02Id__c,
			ExpRequest__c.ExtendedItemLookup02Value__c,
			ExpRequest__c.ExtendedItemLookup03Id__c,
			ExpRequest__c.ExtendedItemLookup03Value__c,
			ExpRequest__c.ExtendedItemLookup04Id__c,
			ExpRequest__c.ExtendedItemLookup04Value__c,
			ExpRequest__c.ExtendedItemLookup05Id__c,
			ExpRequest__c.ExtendedItemLookup05Value__c,
			ExpRequest__c.ExtendedItemLookup06Id__c,
			ExpRequest__c.ExtendedItemLookup06Value__c,
			ExpRequest__c.ExtendedItemLookup07Id__c,
			ExpRequest__c.ExtendedItemLookup07Value__c,
			ExpRequest__c.ExtendedItemLookup08Id__c,
			ExpRequest__c.ExtendedItemLookup08Value__c,
			ExpRequest__c.ExtendedItemLookup09Id__c,
			ExpRequest__c.ExtendedItemLookup09Value__c,
			ExpRequest__c.ExtendedItemLookup10Id__c,
			ExpRequest__c.ExtendedItemLookup10Value__c,
			ExpRequest__c.ExtendedItemText01Value__c,
			ExpRequest__c.ExtendedItemText02Value__c,
			ExpRequest__c.ExtendedItemText03Value__c,
			ExpRequest__c.ExtendedItemText04Value__c,
			ExpRequest__c.ExtendedItemText05Value__c,
			ExpRequest__c.ExtendedItemText06Value__c,
			ExpRequest__c.ExtendedItemText07Value__c,
			ExpRequest__c.ExtendedItemText08Value__c,
			ExpRequest__c.ExtendedItemText09Value__c,
			ExpRequest__c.ExtendedItemText10Value__c,
			ExpRequest__c.ExtendedItemPicklist01Value__c,
			ExpRequest__c.ExtendedItemPicklist02Value__c,
			ExpRequest__c.ExtendedItemPicklist03Value__c,
			ExpRequest__c.ExtendedItemPicklist04Value__c,
			ExpRequest__c.ExtendedItemPicklist05Value__c,
			ExpRequest__c.ExtendedItemPicklist06Value__c,
			ExpRequest__c.ExtendedItemPicklist07Value__c,
			ExpRequest__c.ExtendedItemPicklist08Value__c,
			ExpRequest__c.ExtendedItemPicklist09Value__c,
			ExpRequest__c.ExtendedItemPicklist10Value__c,
			ExpRequest__c.Subject__c,
			ExpRequest__c.Purpose__c
		};
		SELECT_FIELD_SET = Repository.generateFieldNameList(fields);
	}

	/** Relation of Request Approval */
	private static final String REQUEST_R = ExpRequest__c.expRequestApprovalId__c.getDescribe().getRelationshipName();
	/** Fields of Request Approval */
	private static final List<String> SELECT_REQUEST_FIELD_SET;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpRequestApproval__c.Status__c,
			ExpRequestApproval__c.Claimed__c,
			ExpRequestApproval__c.CancelType__c,
			ExpRequestApproval__c.ConfirmationRequired__c,
			ExpRequestApproval__c.RequestTime__c
			};
		SELECT_REQUEST_FIELD_SET = Repository.generateFieldNameList(REQUEST_R, fields);
	}

	/** Relation of Employee */
	private static final String EMP_HISTORY_R = ExpRequest__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	private static final String EMP_BASE_R = ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/** Fields of Employee */
	private final String SELECT_EMP_HIST_FIELD = EMP_HISTORY_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c);
	private static final List<String> SELECT_EMP_BASE_FIELD_SET;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ComEmpBase__c.FirstName_L0__c,
			ComEmpBase__c.FirstName_L1__c,
			ComEmpBase__c.FirstName_L2__c,
			ComEmpBase__c.LastName_L0__c,
			ComEmpBase__c.LastName_L1__c,
			ComEmpBase__c.LastName_L2__c,
			ComEmpBase__c.Code__c
		};
		SELECT_EMP_BASE_FIELD_SET = Repository.generateFieldNameList(EMP_HISTORY_R + '.' + EMP_BASE_R, fields);
	}

	/**
	 * Get entity by specified ID
	 * @param reportId ID of target entity
	 * @return Entity of specified ID. If no data is found, return null.
	 */
	public ExpRequestEntity getEntity(Id id) {
		List<ExpRequestEntity> entityList = getEntityList(new List<Id>{ id });
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * Get entity list by specified IDs
	 * @param ids ID list of target entity
	 * @return Entity list of specified IDs.  If no data is found, return null.
	 */
	public List<ExpRequestEntity> getEntityList(List<Id> ids) {
		SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);
		filter.claimed = null;

		return searchEntity(filter);
	}

	/**
   * Get entity list by employee ID
   * @param empIds ID list of target employees
   * @return Entity list of specified employee IDs.  If no data is found, return null.
   */
	public List<ExpRequestEntity> getEntityListByEmpId(Set<Id> empIds, Set<String> status, AppDate targetDate, Boolean includeClaimed) {
		SearchFilter filter = new SearchFilter();
		filter.empIds = empIds;
		filter.status = status;
		filter.targetDate = targetDate;
		filter.claimed = includeClaimed ? null : false;

		return searchEntity(filter);
	}

	/**
	 * Get Entity List By ReportIdList
	 * @param empIds
	 * @param status
	 * @param targetDate
	 * @param includeClaimed
	 * @param reportIdList
	 * @return search result entity
	 */
	public List<ExpRequestEntity> getEntityListByReportIdList(List<Id> reportIdList) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>(reportIdList);

		return searchEntity(filter);
	}

	/**
	 * getEntityIdListByEmpId
	 * @param empIds
	 * @param status
	 * @param targetDate
	 * @param includeClaimed
	 * @return search result entity
	 */
	public List<Id> getEntityIdListByEmpId(Set<Id> empIds, Set<String> status, AppDate targetDate, Boolean includeClaimed) {
		SearchFilter filter = new SearchFilter();
		filter.empIds = empIds;
		filter.status = status;
		filter.targetDate = targetDate;
		filter.claimed = includeClaimed ? null : false;

		return searchIdList(filter, NOT_FOR_UPDATE, MAX_SEARCH_RECORDS_NUMBER);
	}

	/*
	 * Get Entity list by Company IDs.
	 * @param companyIds list of companyIds
	 * @return List of Entity using the specified companys Ids. Empty List if no data found.
	 */
	public List<ExpRequestEntity> getEntityListByCompanyId(Set<Id> companyIds) {
		SearchFilter filter = new SearchFilter();
		filter.companyIds = companyIds;
		return searchEntity(filter);
	}

	/**
	 * Get Entity list by Expense Report Type ID.
	 * @param reportTypeId 経費申請タイプID
	 * @return List of Entity using the specified Expense Report Type Id. Empty List if no data found.
	 */
	public List<ExpRequestEntity> getEntityListByExpReportTypeId(Id reportTypeId) {
		SearchFilter filter = new SearchFilter();
		filter.expReportTypeIds = new Set<Id>{reportTypeId};
		return searchEntity(filter, NOT_FOR_UPDATE, MAX_SEARCH_RECORDS_NUMBER);
	}

	/**
	 * Get Entity list by Extended Item ID.
	 * @param extendedItemId  Extended item ID
	 * @return List of expense request entities searched by the condition
	 */
	public List<ExpRequestEntity> getEntityListByExtendedItemId(Id extendedItemId) {
		SearchFilter filter = new SearchFilter();
		filter.extendedItemId = extendedItemId;
		return searchEntity(filter, NOT_FOR_UPDATE, MAX_SEARCH_RECORDS_NUMBER);
	}

	/**
	 * Save a entity
	 * @param entity Entity to be saved
	 * @return Saved result
	 */
	public Repository.SaveResult saveEntity(ExpRequestEntity entity) {
		return saveEntityList(new List<ExpRequestEntity>{ entity });
	}

	/**
	 * Save entity list
	 * @param entityList Entity list to be saved
	 * @return Saved result
	 */
	public Repository.SaveResult saveEntityList(List<ExpRequestEntity> entityList) {
		// Convert entity to sObject
		List<ExpRequest__c> sObjList = createObjectList(entityList);

		// Save to DB
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sObjList);

		// Make and return result
		return resultFactory.createSaveResult(recResList);
	}

	/** Search Filters */
	private class SearchFilter {
		/** Record ID */
		public Set<Id> ids;
		/** Employee ID */
		public Set<Id> empIds;
		/** Company ID */
		public Set<Id> companyIds;
		/** Target Date */
		public AppDate targetDate;
		/** Status */
		public Set<String> status;
		/** Claimed */
		public Boolean claimed;
		/** 経費申請タイプID */
		public Set<Id> expReportTypeIds;
		/** Extended item ID */
		public Id extendedItemId;
	}

	/**
	 * Search expense request
	 * @param filter Search filter
	 * @return Search result
	 */
	private List<ExpRequestEntity> searchEntity(SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE, MAX_SEARCH_RECORDS_NUMBER);
	}

	/**
	 * Search expense request
	 * @param filter Search filter
	 * @param forUpdate When call this method for update, specify as true.
	 * @param limitNumber Limit of records to fetch for one time
	 * @return Search result
	 */
	private List<ExpRequestEntity> searchEntity(SearchFilter filter, Boolean forUpdate, Integer limitNumber) {

		// SELECT Target fields
		List<String> selectFieldList = createSelectFieldList();

		// WHERE statement
		List<String> whereList = createWhereList(filter);

		// Create SOQL
		String soql = createSQOL(selectFieldList, whereList, filter, forUpdate);

		// Bind variables
		Set<Id> ids = filter.ids;
		Set<Id> empIds = filter.empIds;
		Set<Id> companyIds = filter.companyIds;
		Date targetDate = filter.targetDate != null ? filter.targetDate.getDate() : null;
		Set<String> status = filter.status;
		Boolean claimed = filter.claimed;
		Set<Id> expReportTypeIds = filter.expReportTypeIds;
		Id extendedItemId = filter.extendedItemId;

		// Execute query
		System.debug(LoggingLevel.INFO, 'ExpRequest.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// Create entity list from result
		return createEntityList(sObjList);
	}

	/**
	 * Search Entity and return request id List
	 * @param filter
	 * @param forUpdate
	 * @param limitNumber
	 * @return id list of search result
	 */
	private List<Id> searchIdList(SearchFilter filter, Boolean forUpdate, Integer limitNumber) {

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		selectFieldList.add(getFieldName(ExpRequest__c.Id));

		// WHERE statement
		List<String> whereList = createWhereList(filter);

		// Create SOQL
		String soql = createSQOL(selectFieldList, whereList, filter, forUpdate);

		Set<Id> ids = filter.ids;
		Set<Id> empIds = filter.empIds;
		Set<Id> companyIds = filter.companyIds;
		Date targetDate = filter.targetDate != null ? filter.targetDate.getDate() : null;
		Set<String> status = filter.status;
		Boolean claimed = filter.claimed;
		Set<Id> expReportTypeIds = filter.expReportTypeIds;
		Id extendedItemId = filter.extendedItemId;

		// Execute query
		System.debug('ExpRequest.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// Create entity list from result
		return createIdList(sObjList);
	}

	/**
	 * create request id List from objest
	 * @param sObjList
	 * @return request id List
	 */
	private List<Id> createIdList(List<ExpRequest__c> sObjList) {
		List<Id> idList = new List<Id>();
		for (ExpRequest__c sobj : sObjList) {
			idList.add(sobj.Id);
		}
		return idList;
	}

	/*
	 * Create filed list to be fetched
	 */
	private List<String> createSelectFieldList() {
		List<String> fieldList = new List<String>();
		// Regular fields
		fieldList.addAll(SELECT_FIELD_SET);
		// Relation fields (Request Approval)
		fieldList.addAll(SELECT_REQUEST_FIELD_SET);
		// Relation fields (Employee history)
		fieldList.add(SELECT_EMP_HIST_FIELD);
		// Relation fields (Employee base)
		fieldList.addAll(SELECT_EMP_BASE_FIELD_SET);

		fieldList.add(getFieldName(COST_CENTER_HISTORY_R, ComCostCenterHistory__c.Name_L0__c));
		fieldList.add(getFieldName(COST_CENTER_HISTORY_R, ComCostCenterHistory__c.Name_L1__c));
		fieldList.add(getFieldName(COST_CENTER_HISTORY_R, ComCostCenterHistory__c.Name_L2__c));
		fieldList.add(getFieldName(COST_CENTER_HISTORY_R, ComCostCenterHistory__c.LinkageCode__c));
		fieldList.add(COST_CENTER_HISTORY_R + '.' + COST_CENTER_BASE_R + '.' + getFieldName(ComCostCenterBase__c.Code__c));

		// 関連項目（ジョブ）
		fieldList.add(getFieldName(JOB_R, ComJob__c.Name_L0__c));
		fieldList.add(getFieldName(JOB_R, ComJob__c.Name_L1__c));
		fieldList.add(getFieldName(JOB_R, ComJob__c.Name_L2__c));
		fieldList.add(getFieldName(JOB_R, ComJob__c.Code__c));

		// 経費申請タイプ
		fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.Name_L0__c));
		fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.Name_L1__c));
		fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.Name_L2__c));
		fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.Code__c));
		fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.UseFileAttachment__c));

		// Get Extended Item Information from Report Type
		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			// Extended Item Id for Text and Picklist, and Date are retrieved from Report Type
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					REPORT_TYPE_EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID)));
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					REPORT_TYPE_EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID)));
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					REPORT_TYPE_EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID)));

			// Extended Item Required For
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR)));
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR)));
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR)));
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR)));

			// Extended Item Used In: Only Text and Picklist, and Date are needed as Lookup ID is retrieved directly from ExpRequest Obj
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_USED_IN)));
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_USED_IN)));
			fieldList.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_USED_IN)));
		}

		// Vendor
		fieldList.add(getFieldName(VENDOR_R, ExpVendor__c.Name_L0__c));
		fieldList.add(getFieldName(VENDOR_R, ExpVendor__c.Name_L1__c));
		fieldList.add(getFieldName(VENDOR_R, ExpVendor__c.Name_L2__c));
		fieldList.add(getFieldName(VENDOR_R, ExpVendor__c.Code__c));
		fieldList.add(getFieldName(VENDOR_R, ExpVendor__c.PaymentDueDateUsage__c));

		return fieldList;
	}

	/*
	 * Create where statement list
	 */
	private List<String> createWhereList(SearchFilter filter) {
		List<String> whereList = new List<String>();
		// Record ID
		if (filter.ids != null) {
			whereList.add('Id IN :ids');
		}
		// Employee
		if (filter.empIds != null) {
			whereList.add(getFieldName(EMP_HISTORY_R, EMP_BASE_R, ComEmpBase__c.Id) + ' IN :empIds');
		}
		// Company
		if (filter.companyIds != null) {
			whereList.add(getFieldName(EMP_HISTORY_R, EMP_BASE_R, ComEmpBase__c.CompanyId__c) + ' IN :companyIds');
		}
		// Target Date
		if (filter.targetDate != null) {
			whereList.add(getFieldName(ExpRequest__c.ScheduledDate__c) + '<= :targetDate' );
		}
		// Status
		if (filter.status != null) {
			whereList.add(getFieldName(REQUEST_R, ExpRequestApproval__c.Status__c) + ' IN :status');
		}
		// Claimed
		if (filter.claimed != null) {
			whereList.add(getFieldName(REQUEST_R, ExpRequestApproval__c.Claimed__c) + ' = :claimed');
		}
		// Expense Report Type ID
		if (filter.expReportTypeIds != null) {
			whereList.add(getFieldName(ExpRequest__c.ExpReportTypeId__c) + ' IN :expReportTypeIds');
		}
		// Extended Item ID
		if (filter.extendedItemId != null) {
			whereList.add('(' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate01TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate02TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate03TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate04TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate05TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate06TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate07TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate08TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate09TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate10TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText01TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText02TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText03TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText04TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText05TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText06TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText07TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText08TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText09TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText10TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup01Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup02Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup03Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup04Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup05Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup06Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup07Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup08Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup09Id__c) + ' = :extendedItemId OR ' +
				getFieldName(ExpRequest__c.ExtendedItemLookup10Id__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist01TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist02TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist03TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist04TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist05TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist06TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist07TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist08TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist09TextId__c) + ' = :extendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist10TextId__c) + ' = :extendedItemId)');
		}

		return whereList;
	}

	/*
	 * Create SOQL
	 * @param fieldList Field list to be fetched
	 * @param whereList Where statement list
	 */
	private String createSQOL(List<String> fieldList, List<String> whereList, SearchFilter filter, Boolean forUpdate) {
		String fieldStr = String.join(fieldList, ',');
		String whereStr = (whereList.isEmpty()) ? '' : ' WHERE (' + String.join(whereList, ') AND (') + ')';
		String objectName = ExpRequest__c.SObjectType.getDescribe().getName();
		String orderStr = ' ORDER BY ' + getFieldName(ExpRequest__c.SequenceNo__c) + ' Desc, CreatedDate Desc';
		String forUpdateStr = forUpdate ? ' FOR UPDATE' : '';

		return 'SELECT ' +
			fieldStr +
			' FROM ' +
			objectName +
			whereStr +
			orderStr +
			' LIMIT :limitNumber' +
			forUpdateStr;
	}

	/**
   * Create entity list from SObject list
   * @param sObjList Original SObject list
   * @return Created entity list
   */
	private List<ExpRequestEntity> createEntityList(List<ExpRequest__c> sObjList) {
		List<ExpRequestEntity> entityList = new List<ExpRequestEntity>();
		for (ExpRequest__c sobj : sObjList) {
			entityList.add(createEntity(sobj));
		}
		return entityList;
	}

	/**
	 * Create entity from SObject
	 * @param obj Original SObject
	 * @return Created entity
	 */
	private ExpRequestEntity createEntity(ExpRequest__c obj) {
		ExpRequestEntity entity = new ExpRequestEntity();

		entity.setId(obj.Id);
		entity.name = obj.Name;
		entity.expRequestApprovalId = obj.expRequestApprovalId__c;
		entity.expRequestApproval = new ExpRequestEntity.Request(
				AppRequestStatus.valueOf(obj.expRequestApprovalId__r.Status__c),
				obj.expRequestApprovalId__r.Claimed__c,
				AppCancelType.valueOf(obj.expRequestApprovalId__r.CancelType__c),
				obj.expRequestApprovalId__r.ConfirmationRequired__c,
				AppDatetime.valueOf(obj.expRequestApprovalId__r.RequestTime__c));
		entity.recordingDate = AppDate.valueOf(obj.RecordingDate__c);
		entity.scheduledDate = AppDate.valueOf(obj.ScheduledDate__c);
		entity.totalAmount = obj.TotalAmount__c;
		entity.requestNo = obj.RequestNo__c;
		entity.prefix = obj.Prefix__c;
		entity.sequenceNo = Integer.valueOf(obj.SequenceNo__c);
		entity.departmentHistoryId = obj.DepartmentHistoryId__c;
		entity.employeeHistoryId = obj.EmployeeHistoryId__c;
		entity.jobId = obj.JobId__c;
		entity.costCenterHistoryId = obj.CostCenterHistoryId__c;
		entity.costCenterNameL = new AppMultiString(
				obj.CostCenterHistoryId__r.Name_L0__c,
				obj.CostCenterHistoryId__r.Name_L1__c,
				obj.CostCenterHistoryId__r.Name_L2__c);
		entity.costCenterLinkageCode = obj.CostCenterHistoryId__r.LinkageCode__c;
		entity.costCenterCode = obj.CostCenterHistoryId__r.BaseId__r.Code__c;
		entity.employeeBaseId = obj.EmployeeHistoryId__r.BaseId__c;
		entity.employeeNameL = new AppPersonName(
			obj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c, obj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
			obj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c, obj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
			obj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c, obj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c);
		entity.employeeCode = obj.EmployeeHistoryId__r.BaseId__r.Code__c;
		entity.subject = obj.Subject__c;
		entity.purpose = obj.Purpose__c;
		entity.jobNameL = new AppMultiString(
				obj.JobId__r.Name_L0__c,
				obj.JobId__r.Name_L1__c,
				obj.JobId__r.Name_L2__c);
		entity.jobCode = obj.JobId__r.Code__c;
		entity.expReportTypeId = obj.ExpReportTypeId__c;
		entity.expReportTypeNameL = new AppMultiString(
				obj.ExpReportTypeId__r.Name_L0__c,
				obj.ExpReportTypeId__r.Name_L1__c,
				obj.ExpReportTypeId__r.Name_L2__c);
		entity.expReportTypeCode = obj.ExpReportTypeId__r.Code__c;
		entity.vendorId = obj.VendorId__c;
		entity.vendorNameL = new AppMultiString(
			obj.VendorId__r.Name_L0__c,
			obj.VendorId__r.Name_L1__c,
			obj.VendorId__r.Name_L2__c);
		entity.vendorCode = obj.VendorId__r.Code__c;
		entity.vendorPaymentDueDateUsage = ExpVendorPaymentDueDateUsage.valueOf(obj.VendorId__r.PaymentDueDateUsage__c);
		entity.paymentDueDate = AppDate.valueOf(obj.PaymentDueDate__c);
		entity.useFileAttachment = obj.ExpReportTypeId__r.UseFileAttachment__c;

		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			// Id
			if (ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT == ComExtendedItemUsedIn.valueOf((String) obj.ExpReportTypeId__r.get(
				REPORT_TYPE_EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_USED_IN))) {
				entity.setExtendedItemDateId(i, (Id) obj.ExpReportTypeId__r.get(REPORT_TYPE_EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID));
			}
			if (ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT == ComExtendedItemUsedIn.valueOf((String) obj.ExpReportTypeId__r.get(
				REPORT_TYPE_EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_USED_IN))) {
				entity.setExtendedItemTextId(i, (Id) obj.ExpReportTypeId__r.get(REPORT_TYPE_EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID));
			}
			if (ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT == ComExtendedItemUsedIn.valueOf((String) obj.ExpReportTypeId__r.get(
				REPORT_TYPE_EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_USED_IN))) {
				entity.setExtendedItemPicklistId(i, (Id) obj.ExpReportTypeId__r.get(REPORT_TYPE_EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID));
			}
			entity.setExtendedItemLookupId(i, (Id) obj.get(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_LOOKUP_ID));

			// Value
			entity.setExtendedItemDateValue(i, (Date) obj.get(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_Value));
			entity.setExtendedItemTextValue(i, (String) obj.get(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_Value));
			entity.setExtendedItemLookupValue(i, (String) obj.get(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_Value));
			entity.setExtendedItemPicklistValue(i, (String) obj.get(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_Value));

			// Required For
			entity.setExtendedItemDateRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
					REPORT_TYPE_EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR)));
			entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
					REPORT_TYPE_EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR)));
			entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
					REPORT_TYPE_EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR)));
			entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
					REPORT_TYPE_EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR)));
		}

		entity.resetChanged();
		return entity;
	}

	/**
	 * Create object from entity to save
	 * @param entityList Target entity list to save
	 * @return Object list to be saved
	 */
	private List<SObject> createObjectList(List<ExpRequestEntity> entityList) {
		List<ExpRequest__c> objectList = new List<ExpRequest__c>();
		for (ExpRequestEntity entity : entityList) {
			objectList.add(createObject(entity));
		}
		return objectList;
	}

	/**
	 * Create object from entity to save
	 * @param entity Original entity
	 * @return SObject
	 */
	private ExpRequest__c createObject(ExpRequestEntity entity) {
		ExpRequest__c retObj = new ExpRequest__c();

		retObj.Id = entity.id;
		if (entity.isChanged(ExpReport.ReportBaseField.NAME)) {
			retObj.Name = entity.name;
		}
		if (entity.isChanged(ExpRequestEntity.Field.EXP_REQUEST_APPROVAL_ID)) {
			retObj.expRequestApprovalId__c = entity.expRequestApprovalId;
		}
		if (entity.isChanged(ExpRequestEntity.Field.RECORDING_DATE)) {
			retObj.RecordingDate__c = AppDate.convertDate(entity.recordingDate);
		}
		if (entity.isChanged(ExpRequestEntity.Field.SCHEDULED_DATE)) {
			retObj.ScheduledDate__c = AppDate.convertDate(entity.scheduledDate);
		}
		if (entity.isChanged(ExpReport.ReportBaseField.TOTAL_AMOUNT)) {
			retObj.TotalAmount__c = entity.totalAmount;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.PREFIX)) {
			retObj.Prefix__c = entity.prefix;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.SEQUENCE_NO)) {
			retObj.SequenceNo__c = entity.sequenceNo;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.DEPARTMENT_HISTORY_ID)) {
			retObj.DepartmentHistoryId__c = entity.departmentHistoryId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.EMPLOYEE_HISTORY_ID)) {
			retObj.EmployeeHistoryId__c = entity.employeeHistoryId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.COST_CENTER_HISTORY_ID)) {
			retObj.CostCenterHistoryId__c = entity.costCenterHistoryId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.JOB_ID)) {
			retObj.JobId__c = entity.jobId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.EXP_REPORT_TYPE_ID)) {
			retObj.ExpReportTypeId__c = entity.expReportTypeId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.VENDOR_ID)) {
			retObj.VendorId__c = entity.vendorId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.PAYMENT_DUE_DATE)) {
			retObj.PaymentDueDate__c = AppDate.convertDate(entity.paymentDueDate);
		}
		if (entity.isChanged(ExpReport.ReportBaseField.SUBJECT)) {
			retObj.Subject__c = entity.subject;
		}
		if (entity.isChanged(ExpRequestEntity.Field.PURPOSE)) {
			retObj.Purpose__c = entity.purpose;
		}

		Boolean extendedItemDateValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemDateValueList);
		Boolean extendedItemLookupIdChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemLookupIdList);
		Boolean extendedItemLookupValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemLookupValueList);
		Boolean extendedItemTextValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemTextValueList);
		Boolean extendedItemPicklistValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemPicklistValueList);

		//Extended Item Lookup: ID & Value fields
		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			if (extendedItemDateValueChanged) {
				retObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_Value, entity.getExtendedItemDateValueAsDate(i));
			}

			if (extendedItemLookupIdChanged) {
				retObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_LOOKUP_ID, entity.getExtendedItemLookupId(i));
			}
			if (extendedItemLookupValueChanged) {
				retObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_Value, entity.getExtendedItemLookupValue(i));
			}

			if (extendedItemTextValueChanged) {
				retObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_Value, entity.getExtendedItemTextValue(i));
			}

			if (extendedItemPicklistValueChanged) {
				retObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_Value, entity.getExtendedItemPicklistValue(i));
			}
		}
		return retObj;
	}

	/*
	 * Get current max sequence number
	 */
	public Integer getMaxSequence() {
		AggregateResult[] result = [SELECT Max(SequenceNo__c) max FROM ExpRequest__c];
		Decimal maxNum = (Decimal) result[0].get('max');
		return maxNum == null ? 0 : maxNum.intValue();
	}
}
