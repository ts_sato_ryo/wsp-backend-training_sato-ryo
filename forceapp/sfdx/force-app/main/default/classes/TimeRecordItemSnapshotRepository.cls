/**
 * @group 工数
 *
 * 工数内訳スナップショットのリポジトリ
 * このクラスは以下の理由により、工数サマリーの集約とはしていません。
 * ・工数内訳スナップショットは、工数明細や工数内訳と異なるタイミングでデータが保存される
 * ・工数内訳スナップショットはレポートで出力するためのオブジェクトである
 */
public with sharing class TimeRecordItemSnapshotRepository extends Repository.BaseRepository {

	/** 工数内訳のリレーション名 */
	private static final String TIME_RECORD_ITEM_R = TimeRecordItemSnapshot__c.TimeRecordItemId__c.getDescribe().getRelationshipName();

	/** 取得対象の項目 */
	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldSet = new Set<Schema.SObjectField> {
			TimeRecordItemSnapshot__c.Id,
			TimeRecordItemSnapshot__c.TimeRecordItemId__c,
			TimeRecordItemSnapshot__c.Name,
			TimeRecordItemSnapshot__c.JobIdLevel1__c,
			TimeRecordItemSnapshot__c.JobCodeLevel1__c,
			TimeRecordItemSnapshot__c.JobNameLevel1_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel1_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel1_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel2__c,
			TimeRecordItemSnapshot__c.JobCodeLevel2__c,
			TimeRecordItemSnapshot__c.JobNameLevel2_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel2_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel2_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel3__c,
			TimeRecordItemSnapshot__c.JobCodeLevel3__c,
			TimeRecordItemSnapshot__c.JobNameLevel3_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel3_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel3_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel4__c,
			TimeRecordItemSnapshot__c.JobCodeLevel4__c,
			TimeRecordItemSnapshot__c.JobNameLevel4_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel4_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel4_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel5__c,
			TimeRecordItemSnapshot__c.JobCodeLevel5__c,
			TimeRecordItemSnapshot__c.JobNameLevel5_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel5_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel5_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel6__c,
			TimeRecordItemSnapshot__c.JobCodeLevel6__c,
			TimeRecordItemSnapshot__c.JobNameLevel6_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel6_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel6_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel7__c,
			TimeRecordItemSnapshot__c.JobCodeLevel7__c,
			TimeRecordItemSnapshot__c.JobNameLevel7_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel7_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel7_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel8__c,
			TimeRecordItemSnapshot__c.JobCodeLevel8__c,
			TimeRecordItemSnapshot__c.JobNameLevel8_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel8_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel8_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel9__c,
			TimeRecordItemSnapshot__c.JobCodeLevel9__c,
			TimeRecordItemSnapshot__c.JobNameLevel9_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel9_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel9_L2__c,
			TimeRecordItemSnapshot__c.JobIdLevel10__c,
			TimeRecordItemSnapshot__c.JobCodeLevel10__c,
			TimeRecordItemSnapshot__c.JobNameLevel10_L0__c,
			TimeRecordItemSnapshot__c.JobNameLevel10_L1__c,
			TimeRecordItemSnapshot__c.JobNameLevel10_L2__c,
			TimeRecordItemSnapshot__c.WorkCategoryId__c,
			TimeRecordItemSnapshot__c.WorkCategoryCode__c,
			TimeRecordItemSnapshot__c.WorkCategoryName_L0__c,
			TimeRecordItemSnapshot__c.WorkCategoryName_L1__c,
			TimeRecordItemSnapshot__c.WorkCategoryName_L2__c
		};
		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldSet);
	}
	
	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/** 検索フィルタ */
	public class SearchFilter {
		/** 工数明細ID */
		public List<Id> recordIds;

		/** 工数内訳ID */
		public List<Id> recordItemIds;
	}

	/**
	 * 検索条件に該当する工数内訳スナップショットを、レコードロックせずに取得する
	 * @param filter 検索条件
	 * @return 工数内訳スナップショットのリスト（該当しない場合は空のリスト）
	 */
	public List<TimeRecordItemSnapshotEntity> searchEntityList(SearchFilter filter) {
		return searchEntityList(filter, false);
	}

	/**
	 * 工数内訳スナップショットを1件保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntity(TimeRecordItemSnapshotEntity entity) {
		return saveEntityList(new List<TimeRecordItemSnapshotEntity>{entity}, ALL_SAVE);
	}
	
	/**
	 * 工数内訳スナップショットを複数件保存する
	 * 1件でもエラーが発生した場合は異常終了する。
	 * @param entityList 保存対象のエンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntityList(List<TimeRecordItemSnapshotEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	}

	/**
	 * 検索条件に該当する工数内訳スナップショットをを取得する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 工数内訳スナップショットのリスト（該当しない場合は空のリスト）
	 */
	 private List<TimeRecordItemSnapshotEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> conditions = new List<String>();
		final List<Id> recordIds = filter.recordIds;
		if (recordIds != null) {
			conditions.add(getFieldName(TIME_RECORD_ITEM_R, TimeRecordItem__c.TimeRecordId__c) + ' IN :recordIds');
		}
		final List<Id> recordItemIds = filter.recordItemIds;
		if (recordItemIds != null) {
			conditions.add(getFieldName(TimeRecordItemSnapshot__c.TimeRecordItemId__c) + ' IN :recordItemIds');
		}

		// SOQLを作成する
		String soql =
				'SELECT ' + String.join(GET_FIELD_NAME_LIST, ', ')
				+ ' FROM ' + TimeRecordItemSnapshot__c.SObjectType.getDescribe().getName()
				+ buildWhereString(conditions);
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('TimeRecordItemSnapshotRepository.searchEntity: SOQL==>' + soql);
		List<TimeRecordItemSnapshot__c> sobjs = (List<TimeRecordItemSnapshot__c>)Database.query(soql);

		// 結果からエンティティを作成する
		List<TimeRecordItemSnapshotEntity> entities = new List<TimeRecordItemSnapshotEntity>();
		for (TimeRecordItemSnapshot__c sobj : sobjs) {
			entities.add(new TimeRecordItemSnapshotEntity(sobj));
		}
		return entities;
	 }

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果
	 */
	private virtual Repository.SaveResult saveEntityList(List<TimeRecordItemSnapshotEntity> entityList, Boolean allOrNone) {
		// エンティティからSObjectを作成する
		List<TimeRecordItemSnapshot__c> sobjs = new List<TimeRecordItemSnapshot__c>();
		for (TimeRecordItemSnapshotEntity entity : entityList) {
			sobjs.add(entity.createSObject());
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sobjs, allOrNone);

		// 結果作成
		Repository.SaveResult result = resultFactory.createSaveResult(recResList);

		// 保存したIdを設定する
		for (Integer i = 0; i < entityList.size(); i++) {
			entityList[i].setId(result.details[i].id);
		}
		return result;
	}
}
