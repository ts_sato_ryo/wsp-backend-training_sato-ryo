/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * ジョブタイプのリポジトリ
 */
public with sharing class JobTypeRepository extends LogicalDeleteRepository {

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/** オブジェクト名 */
	private static final String BASE_OBJECT_NAME = ComJobType__c.SObjectType.getDescribe().getName();

	/**
	 * 取得対象の項目名
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ComJobType__c.Id,
				ComJobType__c.Name,
				ComJobType__c.Code__c,
				ComJobType__c.UniqKey__c,
				ComJobType__c.CompanyId__c,
				ComJobType__c.Name_L0__c,
				ComJobType__c.Name_L1__c,
				ComJobType__c.Name_L2__c,
				ComJobType__c.Removed__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** ジョブタイプIDセット */
		public Set<Id> ids;
		/** ジョブタイプコードセット */
		public Set<String> codes;
		/** 会社ID */
		public Set<Id> companyIds;
		/** 論理削除済みのレコードも取得対象とする場合はtrue */
		public Boolean includeRemoved;
	}

	/**
	 * 指定したIDを持つジョブタイプを取得する
	 * 論理削除済みのレコードは取得対象外。
	 * @param jobTypeId 取得対象のID
	 * @return 指定したIDを持つジョブタイプ、ただし該当するジョブタイプが存在しない場合はnull
	 */
	public JobTypeEntity getEntity(Id jobTypeId) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>{jobTypeId};
		List<JobTypeEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 指定したIDを持つジョブタイプを取得する
	 * 論理削除済みのレコードは取得対象外。
	 * @param jobTypeIdList 取得対象のID
	 * @return 指定したIDを持つジョブタイプ、該当するジョブタイプが存在しない場合は空のリストを返却する
	 */
	public List<JobTypeEntity> getEntityList(List<Id> jobTypeIdList) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>(jobTypeIdList);
		return searchEntityList(filter);
	}

	/**
	 * 会社IDとコードを条件にジョブタイプを検索する。
	 * 論理削除済みのレコードは取得対象外。
	 * @param companyId 会社ID
	 * @param code コード
	 * @return 検索結果 該当するマスタが存在しない場合はnull
	 */
	public JobTypeEntity getEntityByCode(Id companyId, String code) {
		SearchFilter filter = new SearchFilter();
		filter.companyIds = new Set<Id>{companyId};
		filter.codes = new Set<String>{code};
		List<JobTypeEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 検索フィルタの条件に一致するジョブタイプをコードの昇順で取得する。
	 * 当メソッドではレコードをロックしない。(forUpdateではない)
	 * @param filter 検索フィルタ
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<JobTypeEntity> searchEntityList(SearchFilter filter) {
		return searchEntityList(filter, false);
	}

	/**
	 * 検索フィルタの条件に一致するジョブタイプをコードの昇順で取得する。
	 * @param filter 検索フィルタ
	 * @param forUpdate 更新用で取得する場合はtrue(ロックする)
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<JobTypeEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);

		// WHERE句を生成
		List<String> conditions = new List<String>();
		final Set<Id> ids = filter.ids;
		if (ids != null) {
			conditions.add(createInExpression(ComJobType__c.Id, 'ids'));
		}
		final Set<Id> companyIds = filter.companyIds;
		if (companyIds != null) {
			conditions.add(createInExpression(ComJobType__c.CompanyId__c, 'companyIds'));
		}
		final Set<String> codes = filter.codes;
		if (codes != null) {
			conditions.add(createInExpression(ComJobType__c.Code__c, 'codes'));
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			conditions.add(createEqExpression(ComJobType__c.Removed__c, false));
		}

		// SOQLを生成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + BASE_OBJECT_NAME
				+ buildWhereString(conditions)
				+ ' ORDER BY ' + getFieldName(ComJobType__c.Code__c) + ' Asc';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		System.debug('JobTypeRepository: SOQL=' + soql);

		// クエリ実行
		List<ComJobType__c> sObjList = Database.query(soql);
		List<JobTypeEntity> entityList = new List<JobTypeEntity>();
		for (ComJobType__c sobj : sObjList) {
			entityList.add(createEntity(sobj));
		}
		return entityList;
	}

	/**
	 * ジョブタイプを保存する
	 * @param entity 保存対象のジョブタイプ
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntity(JobTypeEntity entity) {
		 return saveEntityList(new List<JobTypeEntity>{entity});
	 }

	/**
	 * ジョブタイプを保存する
	 * @param entityList 保存対象のジョブタイプ
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntityList(List<JobTypeEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	 }

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public virtual Repository.SaveResult saveEntityList(List<JobTypeEntity> entityList, Boolean allOrNone) {

		// エンティティをSObjectに変換する
		List<ComJobType__c> sObjList = new List<ComJobType__c>();
		for (JobTypeEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}

		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/**
	 * エンティティからSObjectを作成する
	 * @param entity 作成元のエンティティ（nullは許容しない）
	 * @return 作成したsObject
	 */
	private ComJobType__c createSObject(JobTypeEntity entity) {

		ComJobType__c sobj = new ComJobType__c(Id = entity.Id);
		if (entity.isChanged(JobTypeEntity.Field.NAME)) {
			sobj.Name = entity.name;
		}

		if (entity.isChanged(JobTypeEntity.Field.COMPANY_ID)) {
			sobj.CompanyId__c = entity.companyId;
		}

		if (entity.isChanged(JobTypeEntity.Field.CODE)) {
			sobj.Code__c = entity.code;
		}

		if (entity.isChanged(JobTypeEntity.Field.UNIQ_KEY)) {
			sobj.UniqKey__c = entity.uniqKey;
		}

		if (entity.isChanged(JobTypeEntity.Field.NAME_L0)) {
			sobj.Name_L0__c = entity.nameL0;
		}

		if (entity.isChanged(JobTypeEntity.Field.NAME_L1)) {
			sobj.Name_L1__c = entity.nameL1;
		}

		if (entity.isChanged(JobTypeEntity.Field.NAME_L2)) {
			sobj.Name_L2__c = entity.nameL2;
		}

		if (entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED)) {
			sobj.Removed__c = entity.isRemoved;
		}
		return sobj;
	}

	/**
	 * JobType__cオブジェクトからエンティティを作成する
	 * @param sObj 作成元のSObjct
	 * @return 作成したエンティティ
	 */
	private JobTypeEntity createEntity(ComJobType__c sobj) {
		JobTypeEntity entity = new JobTypeEntity();
		entity.setId(sobj.Id);
		entity.companyId = sobj.CompanyId__c;
		entity.code = sobj.Code__c;
		entity.uniqKey = sobj.UniqKey__c;
		entity.name = sobj.Name;
		entity.nameL0 = sobj.Name_L0__c;
		entity.nameL1 = sobj.Name_L1__c;
		entity.nameL2 = sobj.Name_L2__c;
		entity.isRemoved = sobj.Removed__c;

		// 変更情報をリセットする
		entity.resetChanged();

		return entity;
	}
}