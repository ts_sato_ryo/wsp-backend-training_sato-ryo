/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 36協定アラート設定マスタのリポジトリのテスト
 */
@isTest
private class AttAgreementAlertSettingRepositoryTest {

	/** テストデータ */
	private class RepoTestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;

		/** コンストラクタ　*/
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
		}

		/**
		 * アラート設定オブジェクトを作成する
		 */
		public List<AttAgreementAlertSetting__c> createAgreementAlertSetting(String name, Integer size) {
			return ComTestDataUtility.createAgreementAlertSettings(name, this.companyObj.Id, size);
		}

		/**
		 * アラート設定エンティティを作成する(DB保存なし)
		 */
		public AttAgreementAlertSettingEntity createAgreementAlertSettingEntity(String name) {
			AttAgreementAlertSettingEntity entity = new AttAgreementAlertSettingEntity();

			entity.name = name;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.code = name;
			entity.uniqKey = this.companyObj.Code__c + '-' + entity.code;
			entity.companyId = this.companyObj.Id;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);
			entity.monthlyAgreementHourWarning1 = 30 * 60;
			entity.monthlyAgreementHourWarning2 = 40 * 60;
			entity.monthlyAgreementHourLimit = 45 * 60;
			entity.monthlyAgreementHourWarningSpecial1 = 50 * 60;
			entity.monthlyAgreementHourWarningSpecial2 = 55 * 60;
			entity.monthlyAgreementHourLimitSpecial = 60 * 60;

			return entity;
		}

		/**
		 * 会社オブジェクトを作成する
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのエンティティが取得できることを確認する
	 * 各項目の値が取得できているかの確認はsearchEntityTestで実施
	 */
	@isTest static void getEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<AttAgreementAlertSetting__c> testSettings = testData.createAgreementAlertSetting('検索テスト', recordSize);
		AttAgreementAlertSetting__c targetObj = testSettings[1];

		AttAgreementAlertSettingRepository repo = new AttAgreementAlertSettingRepository();
		AttAgreementAlertSettingEntity resEntity;

		// 存在するIDを指定する
		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);

		// 存在しないIDを指定する
		delete targetObj;
		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * 正しく検索できることを確認する
	 * 有効期間での検索はsearchEntityTestValidDateで実施
	 */
	@isTest static void searchEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ComCompany__c testCompanyObj = testData.createCompany('検索テスト会社');
		List<AttAgreementAlertSetting__c> testSettings = testData.createAgreementAlertSetting('検索テスト', recordSize);
		AttAgreementAlertSetting__c targetObj = testSettings[1];

		AttAgreementAlertSettingRepository repo = new AttAgreementAlertSettingRepository();
		AttAgreementAlertSettingRepository.SearchFilter filter;
		List<AttAgreementAlertSettingEntity> resEntities;

		// 36協定アラート設定IDで検索
		filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
		// 項目値の検証
		assertFieldValue(targetObj, resEntities[0]);

		// 会社IDで検索
		targetObj.CompanyId__c = testCompanyObj.Id;
		update targetObj;
		filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.companyIds = new Set<Id>{targetObj.CompanyId__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

		// コードで検索
		filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.codes = new Set<String>{targetObj.Code__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

	}

	/**
	 * エンティティ取得のテスト
	 * 指定した対象日に有効なエンティティが取得できることを確認する
	 */
	@isTest static void searchEntityTestValidDate() {

		// テストデータ作成
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ComCompany__c testCompanyObj = testData.createCompany('検索テスト会社');
		List<AttAgreementAlertSetting__c> targetObjs = testData.createAgreementAlertSetting('検索テスト', recordSize);
		AttAgreementAlertSetting__c targetObj = targetObjs[0];
		AttAgreementAlertSetting__c targetObj2 = targetObjs[1];
		AttAgreementAlertSetting__c targetObj3 = targetObjs[2];
		targetObj2.ValidFrom__c = targetObj.ValidTo__c;
		targetObj2.ValidTo__c = targetObj2.ValidFrom__c.addYears(1);
		targetObj3.ValidFrom__c = targetObj2.ValidTo__c;
		targetObj3.ValidTo__c = targetObj3.ValidFrom__c.addYears(1);
		update targetObjs;

		AttAgreementAlertSettingRepository repo = new AttAgreementAlertSettingRepository();
		AttAgreementAlertSettingRepository.SearchFilter filter;
		List<AttAgreementAlertSettingEntity> resEntities;

		// 取得対象日に履歴の有効期間開始日を指定
		filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj2.ValidFrom__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c.addDays(-1));
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj3.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日を指定
		// 取得できないはず
		filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());

	}

	/**
	 * saveEntityのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		AttAgreementAlertSettingEntity setting1 = testData.createAgreementAlertSettingEntity('アラート設定1');

		// テスト実行
		AttAgreementAlertSettingRepository repo = new AttAgreementAlertSettingRepository();
		Repository.SaveResult result = repo.saveEntity(setting1);

		// 確認
		AttAgreementAlertSetting__c resObj1 = getSettingObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(setting1, resObj1);
	}

	/**
	 * saveEntityListのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		AttAgreementAlertSettingEntity setting1 = testData.createAgreementAlertSettingEntity('アラート設定1');
		AttAgreementAlertSettingEntity setting2 = testData.createAgreementAlertSettingEntity('アラート設定2');
		setting2.validFrom = AppDate.today();
		setting2.validTo = setting2.validFrom.addMonths(12);
		setting2.monthlyAgreementHourWarning1 = 30 - 1;
		setting2.monthlyAgreementHourWarning2 = 40 - 1;
		setting2.monthlyAgreementHourLimit = 45 - 1;
		setting2.monthlyAgreementHourWarningSpecial1 = 50 - 1;
		setting2.monthlyAgreementHourWarningSpecial2 = 55 - 1;
		setting2.monthlyAgreementHourLimitSpecial = 60 - 1;

		// テスト実行
		AttAgreementAlertSettingRepository repo = new AttAgreementAlertSettingRepository();
		Repository.SaveResult result = repo.saveEntityList(new List<AttAgreementAlertSettingEntity>{setting1, setting2});

		// 確認
		AttAgreementAlertSetting__c resObj1 = getSettingObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(setting1, resObj1);
		AttAgreementAlertSetting__c resObj2 = getSettingObj(result.details[1].id);
		System.assertNotEquals(null, resObj2);
		assertFieldValue(setting2, resObj2);

	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		RepoTestData testData = new RepoTestData();
		AttAgreementAlertSetting__c alertSettingObj = testData.createAgreementAlertSetting('Test', 1)[0];

		// キャッシュをonでインスタンスを生成
		AttAgreementAlertSettingRepository repository = new AttAgreementAlertSettingRepository(true);

		// 検索（DBから取得）
		AttAgreementAlertSettingRepository.SearchFilter filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.ids = new Set<Id>{alertSettingObj.Id};
		AttAgreementAlertSettingEntity alertSetting = repository.searchEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		alertSettingObj.Code__c = 'NotCache';
		upsert alertSettingObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttAgreementAlertSettingEntity cachedAlertSetting = repository.searchEntity(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(alertSetting.id, cachedAlertSetting.id);
		System.assertNotEquals('NotCache', cachedAlertSetting.code);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		RepoTestData testData = new RepoTestData();
		AttAgreementAlertSetting__c alertSettingObj = testData.createAgreementAlertSetting('Test', 1)[0];

		// 検索（DBから取得）
		AttAgreementAlertSettingRepository.SearchFilter filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.ids = new Set<Id>{alertSettingObj.Id};
		AttAgreementAlertSettingEntity alertSetting = new AttAgreementAlertSettingRepository(true).searchEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		alertSettingObj.Code__c = 'NotCache';
		upsert alertSettingObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttAgreementAlertSettingEntity cachedAlertSetting = new AttAgreementAlertSettingRepository(true).searchEntity(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(alertSetting.id, cachedAlertSetting.id);
		System.assertNotEquals('NotCache', cachedAlertSetting.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheDifferentCondition() {
		RepoTestData testData = new RepoTestData();
		AttAgreementAlertSetting__c alertSettingObj = testData.createAgreementAlertSetting('Test', 1)[0];

		// キャッシュをonでインスタンスを生成
		AttAgreementAlertSettingRepository repository = new AttAgreementAlertSettingRepository(true);

		// 検索（DBから取得）
		AttAgreementAlertSettingRepository.SearchFilter filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.ids = new Set<Id>{alertSettingObj.Id};
		AttAgreementAlertSettingEntity alertSetting = repository.searchEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		alertSettingObj.Code__c = 'NotCache';
		upsert alertSettingObj;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.ids.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		AttAgreementAlertSettingEntity cachedAlertSetting = repository.searchEntity(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(alertSetting.id, cachedAlertSetting.id);
		System.assertEquals('NotCache', cachedAlertSetting.code);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		RepoTestData testData = new RepoTestData();
		AttAgreementAlertSetting__c alertSettingObj = testData.createAgreementAlertSetting('Test', 1)[0];

		// キャッシュをonでインスタンスを生成
		AttAgreementAlertSettingRepository repository = new AttAgreementAlertSettingRepository(true);

		// 検索（DBから取得）
		AttAgreementAlertSettingRepository.SearchFilter filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.ids = new Set<Id>{alertSettingObj.Id};
		AttAgreementAlertSettingEntity alertSetting = repository.searchEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		alertSettingObj.Code__c = 'NotCache';
		upsert alertSettingObj;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttAgreementAlertSettingEntity cachedAlertSetting = repository.searchEntity(filter, true)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(alertSetting.id, cachedAlertSetting.id);
		System.assertEquals('NotCache', cachedAlertSetting.code);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		RepoTestData testData = new RepoTestData();
		AttAgreementAlertSetting__c alertSettingObj = testData.createAgreementAlertSetting('Test', 1)[0];

		// キャッシュをoffでインスタンスを生成
		AttAgreementAlertSettingRepository repository = new AttAgreementAlertSettingRepository();

		// 検索（DBから取得）
		AttAgreementAlertSettingRepository.SearchFilter filter = new AttAgreementAlertSettingRepository.SearchFilter();
		filter.ids = new Set<Id>{alertSettingObj.Id};
		AttAgreementAlertSettingEntity alertSetting = repository.searchEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		alertSettingObj.Code__c = 'NotCache';
		upsert alertSettingObj;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttAgreementAlertSettingEntity cachedAlertSetting = repository.searchEntity(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(alertSetting.id, cachedAlertSetting.id);
		System.assertEquals('NotCache', cachedAlertSetting.code);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		AttAgreementAlertSettingRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new AttAgreementAlertSettingRepository().isEnabledCache);
	}

	/**
	 * アラート設定オブジェクトを取得する
	 * @param settingId 取得対象のアラート設定ID
	 * @return アラート設定オブジェクト
	 */
	private static AttAgreementAlertSetting__c getSettingObj(Id settingId) {
		return [
				SELECT
					Id, Name, ValidFrom__c, ValidTo__c, Name_L0__c, Name_L1__c, Name_L2__c,
					Code__c, UniqKey__c, CompanyId__c,
					MonthlyAgreementHourWarning1__c, MonthlyAgreementHourWarning2__c,
					MonthlyAgreementHourLimit__c,
					MonthlyAgreementHourWarningSpecial1__c, MonthlyAgreementHourWarningSpecial2__c,
					MonthlyAgreementHourLimitSpecial__c
				FROM
					AttAgreementAlertSetting__c
				WHERE
					Id = :settingId
		];
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(AttAgreementAlertSettingEntity expEntity, AttAgreementAlertSetting__c actObj) {

		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.validFrom, AppDate.valueOf(actObj.ValidFrom__c));
		System.assertEquals(expEntity.validTo, AppDate.valueOf(actObj.ValidTo__c));
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
		System.assertEquals(expEntity.monthlyAgreementHourWarning1, actObj.MonthlyAgreementHourWarning1__c);
		System.assertEquals(expEntity.monthlyAgreementHourWarning2, actObj.MonthlyAgreementHourWarning2__c);
		System.assertEquals(expEntity.monthlyAgreementHourLimit, actObj.MonthlyAgreementHourLimit__c);
		System.assertEquals(expEntity.monthlyAgreementHourWarningSpecial1, actObj.MonthlyAgreementHourWarningSpecial1__c);
		System.assertEquals(expEntity.monthlyAgreementHourWarningSpecial2, actObj.MonthlyAgreementHourWarningSpecial2__c);
		System.assertEquals(expEntity.monthlyAgreementHourLimitSpecial, actObj.MonthlyAgreementHourLimitSpecial__c);

	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(AttAgreementAlertSetting__c expObj, AttAgreementAlertSettingEntity actEntity) {

		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(AppDate.valueOf(expObj.ValidFrom__c), actEntity.validFrom);
		System.assertEquals(AppDate.valueOf(expObj.ValidTo__c), actEntity.validTo);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.MonthlyAgreementHourWarning1__c, actEntity.monthlyAgreementHourWarning1);
		System.assertEquals(expObj.MonthlyAgreementHourWarning2__c, actEntity.monthlyAgreementHourWarning2);
		System.assertEquals(expObj.MonthlyAgreementHourLimit__c, actEntity.monthlyAgreementHourLimit);
		System.assertEquals(expObj.MonthlyAgreementHourWarningSpecial1__c, actEntity.monthlyAgreementHourWarningSpecial1);
		System.assertEquals(expObj.MonthlyAgreementHourWarningSpecial2__c, actEntity.monthlyAgreementHourWarningSpecial2);
		System.assertEquals(expObj.MonthlyAgreementHourLimitSpecial__c, actEntity.monthlyAgreementHourLimitSpecial);

	}

}
