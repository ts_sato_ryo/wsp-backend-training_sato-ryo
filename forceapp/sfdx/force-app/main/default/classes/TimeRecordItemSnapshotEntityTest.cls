/**
 * @group 工数
 *
 * @description TimeRecordItemSnapshotEntityのテストクラス
 */
@isTest
private class TimeRecordItemSnapshotEntityTest {

	/**
	 * 追加したジョブが実行回数に合わせて適切なプロパティに設定されることを検証する
	 */
	@isTest
	static void addJobTest() {
		TimeRecordItemSnapshotEntity entity = new TimeRecordItemSnapshotEntity();
		// 1件目
		JobEntity job = new JobEntity();
		job.setId(createId(ComJob__c.SObjectType));
		job.code = 'job1';
		job.nameL0 = 'name1L0';
		job.nameL1 = 'name1L1';
		job.nameL2 = 'name1L2';
		entity.addJob(job);
		System.assertEquals(job.id, entity.jobIdLevel1);
		System.assertEquals('job1', entity.jobCodeLevel1);
		System.assertEquals('name1L0', entity.jobNameLevel1L0);
		System.assertEquals('name1L1', entity.jobNameLevel1L1);
		System.assertEquals('name1L2', entity.jobNameLevel1L2);

		entity.addJob(job); // 2件目
		entity.addJob(job); // 3件目
		entity.addJob(job); // 4件目

		// 5件目
		job.setId(createId(ComJob__c.SObjectType));
		job.code = 'job5';
		job.nameL0 = 'name5L0';
		job.nameL1 = 'name5L1';
		job.nameL2 = 'name5L2';
		entity.addJob(job);
		System.assertEquals(job.id, entity.jobIdLevel5);
		System.assertEquals('job5', entity.jobCodeLevel5);
		System.assertEquals('name5L0', entity.jobNameLevel5L0);
		System.assertEquals('name5L1', entity.jobNameLevel5L1);
		System.assertEquals('name5L2', entity.jobNameLevel5L2);

		entity.addJob(job); // 6件目
		entity.addJob(job); // 7件目
		entity.addJob(job); // 8件目
		entity.addJob(job); // 9件目

		// 10件目
		job.setId(createId(ComJob__c.SObjectType));
		job.code = 'job10';
		job.nameL0 = 'name10L0';
		job.nameL1 = 'name10L1';
		job.nameL2 = 'name10L2';
		entity.addJob(job);
		System.assertEquals(job.id, entity.jobIdLevel10);
		System.assertEquals('job10', entity.jobCodeLevel10);
		System.assertEquals('name10L0', entity.jobNameLevel10L0);
		System.assertEquals('name10L1', entity.jobNameLevel10L1);
		System.assertEquals('name10L2', entity.jobNameLevel10L2);
	}

	/**
	 * 内部で保持しているカウンターと、ジョブの設定階層が一致することを検証する
	 */
	@isTest
	static void jobCounterTest() {
		// ジョブを未設定の場合
		TimeRecordItemSnapshotEntity entity = new TimeRecordItemSnapshotEntity();
		System.assertEquals(0, entity.jobCount);
		
		// 1件
		JobEntity job = new JobEntity();
		job.setId(createId(ComJob__c.SObjectType));
		entity.addJob(job);
		System.assertEquals(1, entity.jobCount);

		// 10件（上限値）
		for (Integer i = 1; i < 10; i++) {
			entity.addJob(job);
		}
		System.assertEquals(10, entity.jobCount);

		// 上限値を超えた場合
		entity.addJob(job);
		System.assertEquals(10, entity.jobCount);
	}

	/**
	 * エンティティに作業分類を正しく設定できる事を検証する
	 */
	@isTest
	static void setWorkCategoryTest() {
		WorkCategoryEntity workCategory = new WorkCategoryEntity();
		workCategory.setId(createId(TimeWorkCategory__c.SObjectType));
		workCategory.code = 'workCategory';
		workCategory.nameL0 = 'nameL0';
		workCategory.nameL1 = 'nameL1';
		workCategory.nameL2 = 'nameL2';

		TimeRecordItemSnapshotEntity entity = new TimeRecordItemSnapshotEntity();
		entity.setWorkCategory(workCategory);
		System.assertEquals(workCategory.id, entity.workCategoryId);
		System.assertEquals('workCategory', entity.workCategoryCode);
		System.assertEquals('nameL0', entity.workCategoryNameL0);
		System.assertEquals('nameL1', entity.workCategoryNameL1);
		System.assertEquals('nameL2', entity.workCategoryNameL2);
	}

	/** テスト用のIDのカウンタ */
	private static Integer idSeq = 0;

	/**
	 * 指定したオブジェクトのダミーのIDを作成する
	 * テスト用にエンティティの値を設定したい場合のみ使用すること
	 * @param objType
	 * @return 生成したダミーのID
	 */
	private static Id createId(Schema.SObjectType objType) {
		String cnt = String.valueOf(idSeq++);
		String suffix = '0'.repeat(15 - cnt.length()) + cnt;
		return objType.getDescribe().getKeyPrefix() + suffix;
	}
}
