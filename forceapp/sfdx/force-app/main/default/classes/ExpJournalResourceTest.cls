/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Test class for Exp Journal Resource
 *
 * @group Expense
 */
@isTest
private class ExpJournalResourceTest {

	/*
	 * Test that the extractDate can be updated successfully.
	 */
	@isTest static void updateExtractDateApiPositiveTest() {
		createJournalTestData();

		Test.startTest();
		ExpJournalResource.JournalExtractDateParam param = new ExpJournalResource.JournalExtractDateParam();
		String journal1No = '000000000001';
		String journal2No = '000000000002';
		AppDate extractDate = AppDate.today().addDays(2);
		param.journalNos = new Set<String>{journal1No, journal2No};
		param.extractDate = extractDate.toString();

		ExpJournalResource.UpdateExtractDateApi api = new ExpJournalResource.UpdateExtractDateApi();
		ExpJournalResource.ExtractDateUpdateResult result = (ExpJournalResource.ExtractDateUpdateResult) api.execute(param);
		Test.stopTest();

		System.assertEquals(2, result.journalNos.size());
		ExpJournalRepository journalRepo = new ExpJournalRepository();
		ExpJournalRepository.SearchFilter filter = new ExpJournalRepository.SearchFilter();
		List<ExpJournalEntity> retJournals = journalRepo.searchEntity(filter, false, ExpJournalRepository.SortOrder.JOURNAL_NO_ASC);

		System.assertEquals(journal1No, retJournals[0].journalNo);
		System.assertEquals(journal2No, retJournals[1].journalNo);

		System.assertEquals(extractDate.getDate(), retJournals[0].extractDate.getDate());
		System.assertEquals(extractDate.getDate(), retJournals[1].extractDate.getDate());
		System.assertEquals(null, retJournals[2].extractDate);  // Since only Journal 01 and 02 are updated with extract date
	}

	/*
	 * Test that extractDateUpdateApi will throw correct corresponding errors.
	 */
	@isTest static void updateExtractDateApiNegativeTest() {
		createJournalTestData();

		Test.startTest();
		//Extract Date Earlier than ExportedTime
		ExpJournalResource.JournalExtractDateParam earlierThanExportedParam = new ExpJournalResource.JournalExtractDateParam();
		String journal1No = '000000000001';
		String journal2No = '000000000002';
		earlierThanExportedParam.journalNos = new Set<String>{journal1No, journal2No};
		earlierThanExportedParam.extractDate = AppDate.today().addDays(-2).toString();  //Earlier than exported

		ExpJournalResource.UpdateExtractDateApi api = new ExpJournalResource.UpdateExtractDateApi();

		Exception earlierThanExportedException;
		try {
			api.execute(earlierThanExportedParam);
		} catch (Exception e) {
			earlierThanExportedException = e;
		}

		 //Report Number doesn't exist
		ExpJournalResource.JournalExtractDateParam reportNotExistParam = new ExpJournalResource.JournalExtractDateParam();
		String invalidJournalNo = '000000000009';
		reportNotExistParam.journalNos = new Set<String>{journal1No, invalidJournalNo};
		reportNotExistParam.extractDate = AppDate.today().addDays(1).toString();

		Exception reportNotExistException;
		try {
			api.execute(reportNotExistParam);
		} catch (Exception e) {
			reportNotExistException = e;
		}
		Test.stopTest();

		System.assertNotEquals(null, earlierThanExportedException);
		System.assertEquals(true, earlierThanExportedException instanceof App.IllegalStateException);
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValueLater(new List<String>{
				ComMessage.msg().Exp_Lbl_ExtractDate, ComMessage.msg().Exp_Lbl_ExportedTime }),
				earlierThanExportedException.getMessage());

		System.assertNotEquals(null, reportNotExistException);
		System.assertEquals(true, reportNotExistException instanceof App.RecordNotFoundException);
		System.assertEquals(ComMessage.msg().Exp_Err_NotFoundRecord,
				reportNotExistException.getMessage());
	}


	/*
	 * Test that the Payment Date can be updated successfully.
	 */
	@isTest static void paymentDatePositiveTest() {
		createJournalTestData();

		String reportId = [SELECT Id, ExpReportId__c From ExpJournal__c][0].ExpReportId__c;

		Test.startTest();
		ExpJournalResource.JournalPaymentDateParam createParam = new ExpJournalResource.JournalPaymentDateParam();

		ExpJournalResource.ExpReport expReport = new ExpJournalResource.ExpReport();
		AppDate initialPaymentDate = AppDate.today();
		expReport.reportId = reportId;
		expReport.paymentDate = initialPaymentDate.toString();
		createParam.reports = new List<ExpJournalResource.ExpReport>{expReport};
		ExpJournalResource.CreatePaymentDateApi createApi = new ExpJournalResource.CreatePaymentDateApi();
		ExpJournalResource.PaymentDateUpdateResult createResult = (ExpJournalResource.PaymentDateUpdateResult) createApi.execute(createParam);
		List<ExpJournal__c> journalsAfterCreate = [SELECT Id, PaymentDate__c FROM ExpJournal__c];
		List<ExpReportRequest__c> reportReqAfterCreate = [SELECT Id, PaymentDate__c, AccountingStatus__c FROM ExpReportRequest__c];

		ExpJournalResource.JournalPaymentDateParam updateParam = new ExpJournalResource.JournalPaymentDateParam();
		AppDate updatedPaymentDate = AppDate.today().addDays(2);
		expReport.reportId = reportId;
		expReport.paymentDate = updatedPaymentDate.toString();
		updateParam.reports = new List<ExpJournalResource.ExpReport>{expReport};
		ExpJournalResource.UpdatePaymentDateApi updateApi = new ExpJournalResource.UpdatePaymentDateApi();
		ExpJournalResource.PaymentDateUpdateResult updateResult = (ExpJournalResource.PaymentDateUpdateResult) updateApi.execute(createParam);
		List<ExpJournal__c> journalsAfterUpdate = [SELECT Id, PaymentDate__c FROM ExpJournal__c];
		List<ExpReportRequest__c> reportReqAfterUpdate = [SELECT Id, PaymentDate__c, AccountingStatus__c FROM ExpReportRequest__c];
		Test.stopTest();

		System.assertEquals(1, createResult.reportIds.size());
		System.assertEquals(reportId, createResult.reportIds[0]);

		System.assertEquals(1, updateResult.reportIds.size());
		System.assertEquals(reportId, updateResult.reportIds[0]);

		for (ExpJournal__c journal: journalsAfterCreate) {
			System.assertEquals(initialPaymentDate.getDate(), journal.PaymentDate__c);
		}
		for (ExpJournal__c journal: journalsAfterUpdate) {
			System.assertEquals(updatedPaymentDate.getDate(), journal.PaymentDate__c);
		}

		for (ExpReportRequest__c reportReq : reportReqAfterCreate) {
			System.assertEquals(initialPaymentDate.getDate(), reportReq.PaymentDate__c);
			System.assertEquals(ExpAccountingStatus.FULLY_PAID.value, reportReq.AccountingStatus__c);
		}
		for (ExpReportRequest__c reportReq : reportReqAfterUpdate) {
			System.assertEquals(updatedPaymentDate.getDate(), reportReq.PaymentDate__c);
			System.assertEquals(ExpAccountingStatus.FULLY_PAID.value, reportReq.AccountingStatus__c);
		}
	}

	/*
	 * Test that the Payment Date setting/update will throw correct corresponding errors.
	 */
	@isTest static void paymentDateApiNegativeTest() {
		createJournalTestData();

		String reportId = [SELECT Id, ExpReportId__c From ExpJournal__c][0].ExpReportId__c;

		ExpJournalResource.JournalPaymentDateParam param = new ExpJournalResource.JournalPaymentDateParam();

		ExpJournalResource.ExpReport expReport = new ExpJournalResource.ExpReport();
		AppDate initialPaymentDate = AppDate.today();
		expReport.reportId = reportId;
		expReport.paymentDate = initialPaymentDate.toString();
		param.reports = new List<ExpJournalResource.ExpReport>{expReport};
		ExpJournalResource.CreatePaymentDateApi createApi = new ExpJournalResource.CreatePaymentDateApi();
		ExpJournalResource.UpdatePaymentDateApi updateApi = new ExpJournalResource.UpdatePaymentDateApi();

		// 1. Create not set paymentDate via Update API -> NG (must use Create API for first time creation)
		Exception createUsingUpdateApiException;
		try {
			updateApi.execute(param);
		} catch (Exception e) {
			createUsingUpdateApiException = e;
		}

		System.assertNotEquals(null, createUsingUpdateApiException);
		System.assertEquals(true, createUsingUpdateApiException instanceof App.IllegalStateException);
		System.assertEquals(ComMessage.msg().Exp_Err_NoPreviousPaymentDate,
				createUsingUpdateApiException.getMessage());

		// 2. Payment Date is before the Authorized Time -> NG
		expReport.paymentDate = AppDate.today().addDays(-1).toString();    //Set the date before the Authorized Time
		Exception payingBeforeAuthorizedDateException;
		try {
			createApi.execute(param);
		} catch (Exception e) {
			payingBeforeAuthorizedDateException = e;
		}

		System.assertNotEquals(null, createUsingUpdateApiException);
		System.assertEquals(true, createUsingUpdateApiException instanceof App.IllegalStateException);
		System.assertEquals(ComMessage.msg().Exp_Err_NoPreviousPaymentDate,
				createUsingUpdateApiException.getMessage());

		// 3. Update Existing paymentDate via Create API -> NG
		expReport.paymentDate = initialPaymentDate.toString();    //Set the correct date
		createApi.execute(param);       //Set the PaymentDate
		expReport.paymentDate = initialPaymentDate.addDays(2).toString();
		Exception updateExistingUsingCreateApiException;
		try {
			createApi.execute(param);
		} catch (Exception e) {
			updateExistingUsingCreateApiException = e;
		}
		System.assertNotEquals(null, updateExistingUsingCreateApiException);
		System.assertEquals(true, updateExistingUsingCreateApiException instanceof App.IllegalStateException);
		System.assertEquals(ComMessage.msg().Exp_Err_AlreadySetPaymentDate,
				updateExistingUsingCreateApiException.getMessage());

		// 4. Same Report Id (duplicate Report Ids), but different dates
		ExpJournalResource.ExpReport duplicateReport = new ExpJournalResource.ExpReport();
		duplicateReport.reportId = reportId;
		duplicateReport.paymentDate = initialPaymentDate.toString();
		param.reports.add(duplicateReport);
		Exception duplicateReportException;
		try {
			updateApi.execute(param);
		} catch (Exception e) {
			duplicateReportException = e;
		}
		System.assertNotEquals(null, duplicateReportException);
		System.assertEquals(true, duplicateReportException instanceof App.ParameterException);
		System.assertEquals(ComMessage.msg().Com_Err_DuplicateValue(new List<String>{'reportId'}),
				duplicateReportException.getMessage());
	}

	/*
	 * Test the rollback function of UpdatePaymentDateApi
	 */
	@isTest static void updatePaymentDateApiTestRollback() {
		createJournalTestData();

		String reportId = [SELECT Id, ExpReportId__c From ExpJournal__c][0].ExpReportId__c;

		Test.startTest();
		ExpJournalResource.JournalPaymentDateParam createParam = new ExpJournalResource.JournalPaymentDateParam();

		// set Payment Date
		ExpJournalResource.ExpReport expReport = new ExpJournalResource.ExpReport();
		AppDate initialPaymentDate = AppDate.today();
		expReport.reportId = reportId;
		expReport.paymentDate = initialPaymentDate.toString();
		createParam.reports = new List<ExpJournalResource.ExpReport>{expReport};
		ExpJournalResource.CreatePaymentDateApi createApi = new ExpJournalResource.CreatePaymentDateApi();
		ExpJournalResource.PaymentDateUpdateResult createResult = (ExpJournalResource.PaymentDateUpdateResult) createApi.execute(createParam);
		ExpJournal__c afterCreateExpJournal = [SELECT Id, ExpReportId__c, PaymentDate__c From ExpJournal__c][0];
		ExpReportRequest__c afterCreateExpReportRequest = [SELECT Id, ExpReportId__c, PaymentDate__c, AccountingStatus__c
														FROM ExpReportRequest__c WHERE ExpReportId__c =: reportId];

		// update Payment Date
		ExpJournalResource.JournalPaymentDateParam updateParam = new ExpJournalResource.JournalPaymentDateParam();
		AppDate updatedPaymentDate = AppDate.today().addDays(2);
		expReport.reportId = reportId;
		expReport.paymentDate = updatedPaymentDate.toString();
		updateParam.reports = new List<ExpJournalResource.ExpReport>{expReport};

		try {
			ExpJournalResource.UpdatePaymentDateApi updateApi = new ExpJournalResource.UpdatePaymentDateApi();
			updateApi.isRollbackTest = true;
			updateApi.execute(createParam);

			System.assert(false, 'No exception occurred');
		} catch (Exception e) {
			ExpJournal__c resExpJournal = [SELECT Id, ExpReportId__c, PaymentDate__c From ExpJournal__c][0];
			ExpReportRequest__c resExpReportRequest = [SELECT Id, ExpReportId__c, PaymentDate__c, AccountingStatus__c
														FROM ExpReportRequest__c WHERE ExpReportId__c =: reportId];
			System.assertEquals(afterCreateExpJournal.PaymentDate__c, resExpJournal.PaymentDate__c);
			System.assertEquals(afterCreateExpReportRequest.PaymentDate__c, resExpReportRequest.PaymentDate__c);
			System.assertEquals(afterCreateExpReportRequest.AccountingStatus__c, resExpReportRequest.AccountingStatus__c);
		}

		Test.stopTest();
	}

	/*
	 * Test the rollback function of CreatePaymentDateApi
	 */
	@isTest static void createPaymentDateApiTestRollback() {
		createJournalTestData();

		String reportId = [SELECT Id, ExpReportId__c From ExpJournal__c][0].ExpReportId__c;

		Test.startTest();
		ExpJournalResource.JournalPaymentDateParam createParam = new ExpJournalResource.JournalPaymentDateParam();

		ExpJournalResource.ExpReport expReport = new ExpJournalResource.ExpReport();
		AppDate initialPaymentDate = AppDate.today();
		expReport.reportId = reportId;
		expReport.paymentDate = initialPaymentDate.toString();
		createParam.reports = new List<ExpJournalResource.ExpReport>{expReport};

		try {
			ExpJournalResource.CreatePaymentDateApi createApi = new ExpJournalResource.CreatePaymentDateApi();
			createApi.isRollbackTest = true;
			createApi.execute(createParam);

			System.assert(false, 'No exception occurred');
		} catch (Exception e) {
			ExpJournal__c resExpJournal = [SELECT Id, ExpReportId__c, PaymentDate__c From ExpJournal__c][0];
			ExpReportRequest__c resExpReportRequest = [SELECT Id, ExpReportId__c, PaymentDate__c, AccountingStatus__c
														FROM ExpReportRequest__c WHERE ExpReportId__c =: reportId];
			System.assertEquals(null, resExpJournal.PaymentDate__c);
			System.assertEquals(null, resExpReportRequest.PaymentDate__c);
			System.assertNotEquals(ExpAccountingStatus.FULLY_PAID.value, resExpReportRequest.AccountingStatus__c);
		}

		Test.stopTest();
	}

	/*
	 * Helper method which create a report with 3 record, and then submit, approve, and export to Journal object.
	 */
	private static void createJournalTestData() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity reportEntity =
		testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job,
				testData.costCenter.CurrentHistoryId__c, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportEntity.id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c, 3);

		setVendorDetails(testData.company, reportEntity);

		// Set Manager
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpReportRequestService service = new ExpReportRequestService();

		//Submit Report
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportEntity.id, '');

		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();
		ExpReportEntity retrievedReportEntity = new ExpReportRepository().getEntity(reportEntity.id);

		// Set the Authorized Date and Last Approved Time
		ExpReportRequestEntity requestEntity = requestRepo.getEntity(retrievedReportEntity.expRequestId);
		requestEntity.authorizedTime = AppDatetime.now();
		requestEntity.lastApproveTime = AppDatetime.now();
		requestRepo.saveEntity(requestEntity);

		ExpJournalService journalService = new ExpJournalService();
		journalService.exportConfirmedReportsToJournal(new List<Id>{reportEntity.id});
	}

	/*
	 * Helper method to create ExpVendor information and set it to ExpReport
	 */
	private static void setVendorDetails(ComCompany__c companyObj, ExpReportEntity reportEntity) {
		// create ExpVendor object
		ExpVendor__c vendorObj = new ExpVendor__c(
			Active__c = true,
			Address__c = 'address',
			BankAccountType__c = 'Checking',
			BankAccountNumber__c = 'bankAccountNumber',
			BankCode__c = 'bankCd',
			BankName__c = 'bankName',
			BranchAddress__c = 'branchAddress',
			BranchCode__c = 'XYZ',
			BranchName__c = 'branchName',
			Code__c = 'code',
			CorrespondentBankAddress__c = 'correspondentBankAddress',
			CorrespondentBankName__c = 'correspondentBankName',
			CorrespondentBranchName__c = 'correspondentBranchName',
			CorrespondentSwiftCode__c = 'cSwiftCode',
			CompanyId__c = companyObj.Id,
			Country__c = 'JPN',
			CurrencyCode__c = 'JPY',
			IsWithholdingTax__c = true,
			Name = 'nameL0',
			Name_L0__c = 'nameL0',
			Name_L1__c = 'nameL1',
			Name_L2__c = 'nameL2',
			PayeeName__c = 'payeeName',
			PaymentTerm__c = 'paymentTerm',
			SwiftCode__c = 'swiftCode',
			ZipCode__c = 'zipCode',
			UniqKey__c = 'uniqKey'
		);
		insert vendorObj;

		// set Vendor details in ExpReport object
		ExpReport__c reportObj = [SELECT Id, PaymentDueDate__c, VendorId__c FROM ExpReport__c WHERE Id =: reportEntity.Id];
		reportObj.PaymentDueDate__c = Date.today() + 90;
		reportObj.VendorId__c = vendorObj.Id;
		update reportObj;

		// set Vendor details in ExpReport entity
		reportEntity.paymentDueDate = AppDate.valueOf(Date.today() + 90);
		reportEntity.vendorId = vendorObj.Id;
		reportEntity.vendorCode = vendorObj.Code__c;
	}
}