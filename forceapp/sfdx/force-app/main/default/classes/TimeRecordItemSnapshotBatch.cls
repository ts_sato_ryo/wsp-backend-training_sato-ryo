/**
 * @group 工数
 *
 * 工数内訳スナップショットを作成するバッチ
 */
public with sharing class TimeRecordItemSnapshotBatch implements Database.Batchable<sObject>, Database.Stateful {

	/**
	 * scopeの上限値
	 * 処理速度が問題にならない限り、ガバナ制限に違反しないように「1」固定とする
	 */
	public static final Integer SCOPE_LIMIT = 1;

	/**
	 * @description バッチ開始処理
	 * @param bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {
		// 工数確定していない全ての工数サマリーを対象とする
		String query = 'SELECT Id, Name, EmployeeHistoryId__r.BaseId__r.CompanyId__c, EmployeeHistoryId__r.BaseId__r.CompanyId__r.Name'
				+ ' FROM TimeSummary__c'
				+ ' WHERE RequestId__c = null'
				+ ' ORDER BY EmployeeHistoryId__r.BaseId__r.CompanyId__c';
		return Database.getQueryLocator(query);
	}

	/**
	 * @description バッチ実行処理
	 * 工数確定していない工数内訳に対してスナップショットレコードを1件作成する。
	 * @param bc バッチコンテキスト
	 * @param scope バッチ処理対象のレコード
	 */
	public void execute(Database.BatchableContext bc, List<TimeSummary__c> scope) {
		// 上限を超えた場合は異常終了とする
		if (SCOPE_LIMIT < scope.size()) {
			App.IllegalStateException e = new App.IllegalStateException();
			e.setMessage('Scope size exceeds upper limit[' + scope.size() + ']'); // プログラムバグなので多言語対応はしない
			throw e;
		}

		TimeRecordItemSnapshotService service;
		for (TimeSummary__c summary : scope) {
			System.debug('TimeRecordItemSnapshotBatch.execute START: ' + summary.name);
			try {
				List<TimeRecordEntity> timeRecords = new TimeSummaryRepository().getRecordListBySummaryId(summary.Id);
				Id companyId = summary.EmployeeHistoryId__r.BaseId__r.CompanyId__c;
				
				// 会社単位でServiceクラスを再利用する
				if (service == null || companyId != service.companyId) {
					service = new TimeRecordItemSnapshotService(companyId);
				}
				service.save(timeRecords);
			} catch (Exception e) {
				// エラーメッセージに会社名、サマリー名を追加しておく
				String msg = summary.EmployeeHistoryId__r.BaseId__r.CompanyId__r.Name + ' '
						+ summary.name + ':'
						+ e.getMessage();
				e.setMessage(msg);
				throw e;
			}
		}
	}

	/**
	 * @description バッチ終了処理
	 * @param bc バッチコンテキスト
	 */
	public void finish(Database.BatchableContext bc) {
	}
}
