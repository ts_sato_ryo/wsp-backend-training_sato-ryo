/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group アプリ
 *
 * 言語の選択リスト値
 */
public with sharing class AppLanguage extends ValueObjectType {

	/** 組織の言語キー */
	public enum LangKey {
		L0, L1, L2
	}

	/** ユーザのデフォルト言語(en_US) */
	@testVisible
	private static final AppLanguage USER_DEFAULT_LANG;

	/** 英語（アメリカ合衆国） */
	public static final AppLanguage EN_US;
	/** 日本語 */
	public static final AppLanguage JA;
	/** 値のリスト */
	public static final List<AppLanguage> TYPE_LIST;
	/** 値のMap(キーは言語の値) */
	public static final Map<String, AppLanguage> TYPE_MAP;
	static {
		TYPE_LIST = new List<AppLanguage>();
		TYPE_MAP = new Map<String, AppLanguage>();

		// ラベルと値を動的に取得する
		List<Schema.PicklistEntry> pickList =
				ComCompany__c.Language__c.getDescribe().getPicklistValues();
		for (Schema.PicklistEntry entry : pickList) {
			AppLanguage lang = new AppLanguage(entry.getValue(), entry.getLabel());
			if (entry.getValue() == 'en_US') {
				EN_US = lang;
				USER_DEFAULT_LANG = lang;
			} else if (entry.getValue() == 'ja') {
				JA = lang;
			}
			TYPE_LIST.add(lang);
			TYPE_MAP.put(lang.value, lang);
		}
	}

	/** 組織の言語設定(キーは言語名(value)) */
	@TestVisible
	private static Map<String, AppLanguage.LangKey> langKeyMap;

	/**
	 * コンストラクタ
	 * @param value 値
	 * @param label ラベル
	 */
	private AppLanguage(String value, String label) {
		super(value, label);
	}

	/**
	 * 指定した文字列に該当するLanguageを返却する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つLanguage
	 */
	public static AppLanguage valueOf(String value) {
		return TYPE_MAP.get(value);
	}

	/**
	 * 実行ユーザの言語を取得する。
	 * ただし、該当するAppLanguageが存在しない場合はユーザのデフォルト言語を返却する。
	 * @param 取得対象のインスタンスが持つ値
	 * @return 実行ユーザの言語。ただし、AppLanguageに存在していない言語の場合はユーザのデフォルト言語。
	 */
	public static AppLanguage getUserLang() {
		AppLanguage retType = valueOf(UserInfo.getLanguage());
		if (retType == null) {
			retType = USER_DEFAULT_LANG;
		}
		return retType;
	}

	/**
	 * 組織設定での対応する言語キー(L0, L1, L2)を取得する
	 * @return 言語キー
	 */
	public AppLanguage.LangKey getLangkey() {
		if (AppLanguage.langKeyMap == null) {
			AppLanguage.langKeyMap = getLangKeyMap();
		}
		return langKeyMap.get(this.value);
	}

	/**
	 * 指定された言語キーに対応する言語を取得する
	 * @param langKey 取得対象の言語キー
	 * @return 対応する言語。設定されていない場合はnull
	 */
	public static AppLanguage getLangByLangKey(AppLanguage.LangKey langKey) {
		if (AppLanguage.langKeyMap == null) {
			AppLanguage.langKeyMap = getLangKeyMap();
		}

		AppLanguage retLang;
		for (String key : langKeyMap.keyset()) {
			if (langKeyMap.get(key) == langKey) {
				retLang = AppLanguage.valueOf(key);
				break;
			}
		}
		return retLang;
	}

	/**
	 * 組織設定を取得する
	 */
	private static Map<String, AppLanguage.LangKey> getLangKeyMap() {

		// 本来はエンティティ＆リポジトリで取得するべきですが、
		// 組織設定エンティティに言語項目が存在し、クラス間を相互参照してしまうため
		// SObjectから直接取得します。
		List<ComOrgSetting__c> orgSettingObjs = [
				SELECT Id, Name, Language_0__c, Language_1__c, Language_2__c
				FROM ComOrgSetting__c
				LIMIT 1];

		// 組織設定が存在しない場合、空のMapを返却する
		if (orgSettingObjs.isEmpty()) {
			return new Map<String, AppLanguage.LangKey>();
		}

		ComOrgSetting__c orgSettingObj = orgSettingObjs[0];
		AppLanguage lang0 = AppLanguage.valueOf(orgSettingObj.Language_0__c);
		AppLanguage	lang1 = AppLanguage.valueOf(orgSettingObj.Language_1__c);
		AppLanguage	lang2 = AppLanguage.valueOf(orgSettingObj.Language_2__c);

		// 優先順位: L0(主言語) > L1 > L2
		Map<String, AppLanguage.LangKey> langKeyMap = new Map<String, AppLanguage.LangKey>();
		if (lang2 != null) {
			langKeyMap.put(lang2.value, AppLanguage.LangKey.L2);
		}
		if (lang1 != null) {
			langKeyMap.put(lang1.value, AppLanguage.LangKey.L1);
		}
		if (lang0 != null) {
			langKeyMap.put(lang0.value, AppLanguage.LangKey.L0);
		}

		return langKeyMap;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// Language以外の場合はfalse
		Boolean eq;
		if (compare instanceof AppLanguage) {
			// 値が同じであればtrue
			if (this.value == ((AppLanguage)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}
