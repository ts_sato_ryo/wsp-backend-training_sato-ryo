/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 工数
*
* @description TimeRequestTriggerHandlerのテストクラス
*/
@isTest
private class TimeRequestTriggerHandlerTest {

	/**
	 * @description 申請レコード更新処理（承認済み）のテスト
	 * - 承認時に申請レコード更新する場合に要確認フラグ・取消種別が正しい状態であることを確認する
	 * - 承認時に申請レコード更新する場合に工数サマリーがロックされた状態のままであることを確認する
	 */
	@isTest
	static void updateRequestApproveTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summaryEntity = testData.createSummaryEntity();
		summaryEntity.isLocked = true;// 申請時点でtrueになる
		TimeSummaryRepository summaryRepo = new TimeSummaryRepository();
		summaryRepo.saveSummaryEntity(summaryEntity, false);
		TimeRequestEntity requestEntity = testData.createRequestEntity(summaryEntity);

		Test.startTest();

		TimeRequestRepository repo = new TimeRequestRepository();
		requestEntity.status = AppRequestStatus.APPROVED;
		repo.saveEntity(requestEntity);

		Test.stopTest();

		// 申請データを取得
		TimeRequest__c request = [
				SELECT
					Id,
					ConfirmationRequired__c,
					CancelType__c,
					CancelComment__c
				FROM TimeRequest__c
				WHERE Id = :requestEntity.id];

		System.assertEquals(false, request.ConfirmationRequired__c);
		System.assertEquals(null, request.CancelType__c);

		// サマリーデータを取得
		TimeSummary__c summary = [
				SELECT
					Id,
					Locked__c
				FROM TimeSummary__c
				WHERE Id = :summaryEntity.id];
		System.assertEquals(true, summary.Locked__c);
	}

	/**
	 * @description 申請レコード更新処理（却下）のテスト
	 * - 却下時に申請レコード更新する場合に要確認フラグが正しい状態であることを確認する
	 * - 却下時に申請レコード更新する場合に工数サマリーのロックが解除されることを確認する
	 */
	@isTest
	static void updateRequestRejectTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summaryEntity = testData.createSummaryEntity();
		TimeRequestEntity requestEntity = testData.createRequestEntity(summaryEntity);

		Test.startTest();

		TimeRequestRepository repo = new TimeRequestRepository();
		requestEntity.status = AppRequestStatus.DISABLED;
		requestEntity.cancelType = AppCancelType.REJECTED;
		repo.saveEntity(requestEntity);

		Test.stopTest();

		// 申請データを取得
		TimeRequest__c request = [
				SELECT
					Id,
					ConfirmationRequired__c,
					CancelType__c,
					CancelComment__c
				FROM TimeRequest__c
				WHERE Id = :requestEntity.id];

		System.assertEquals(true, request.ConfirmationRequired__c);

		// サマリーデータを取得
		TimeSummary__c summary = [
				SELECT
					Id,
					Locked__c
				FROM TimeSummary__c
				WHERE Id = :summaryEntity.id];
		System.assertEquals(false, summary.Locked__c);
	}

	/**
	 * @description 申請レコード更新処理（申請取消）のテスト
	 * - 申請取消時に申請レコード更新する場合に要確認フラグが正しい状態であることを確認する
	 * - 申請取消時に申請レコード更新する場合に工数サマリーのロックが解除されることを確認する
	 */
	@isTest
	static void updateRequestRemoveTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summaryEntity = testData.createSummaryEntity();
		TimeRequestEntity requestEntity = testData.createRequestEntity(summaryEntity);

		Test.startTest();

		TimeRequestRepository repo = new TimeRequestRepository();
		requestEntity.status = AppRequestStatus.DISABLED;
		requestEntity.cancelType = AppCancelType.REMOVED;
		repo.saveEntity(requestEntity);

		Test.stopTest();

		// 申請データを取得
		TimeRequest__c request = [
				SELECT
					Id,
					ConfirmationRequired__c,
					CancelType__c,
					CancelComment__c
				FROM TimeRequest__c
				WHERE Id = :requestEntity.id];

		System.assertEquals(true, request.ConfirmationRequired__c);

		// サマリーデータを取得
		TimeSummary__c summary = [
				SELECT
					Id,
					Locked__c
				FROM TimeSummary__c
				WHERE Id = :summaryEntity.id];
		System.assertEquals(false, summary.Locked__c);
	}
}