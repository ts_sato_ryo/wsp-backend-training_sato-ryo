/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブ割当リポジトリ
 */
public with sharing class JobAssignRepository extends Repository.BaseRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * 取得対象の項目名ß
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ComJobAssign__c.Id,
				ComJobAssign__c.Name,
				ComJobAssign__c.JobId__c,
				ComJobAssign__c.EmployeeBaseId__c,
				ComJobAssign__c.ValidFrom__c,
				ComJobAssign__c.ValidTo__c,
				ComJobAssign__c.Order__c};
		FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** ジョブのリレーション名 */
	private static final String JOB_R = ComJobAssign__c.JobId__c.getDescribe().getRelationshipName();

	/** ジョブの取得項目名 */
	private static final List<String> JOB_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> jobFieldList = new Set<Schema.SObjectField> {
				ComJob__c.Name_L0__c,
				ComJob__c.Name_L1__c,
				ComJob__c.Name_L2__c};
		JOB_FIELD_NAME_LIST = Repository.generateFieldNameList(JOB_R, jobFieldList);
	}

	/** 社員ベースのリレーション名 */
	private static final String EMPLOYEE_R = ComJobAssign__c.EmployeeBaseId__c.getDescribe().getRelationshipName();

	/** 社員の取得項目名 */
	private static final List<String> EMPLOYEE_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> employeeFieldList = new Set<Schema.SObjectField> {
			ComEmpBase__c.Code__c,
			ComEmpBase__c.FirstName_L0__c,
			ComEmpBase__c.LastName_L0__c,
			ComEmpBase__c.FirstName_L1__c,
			ComEmpBase__c.LastName_L1__c,
			ComEmpBase__c.FirstName_L2__c,
			ComEmpBase__c.LastName_L2__c};
		EMPLOYEE_FIELD_NAME_LIST = Repository.generateFieldNameList(EMPLOYEE_R, employeeFieldList);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** ジョブ割当ID */
		public Set<Id> ids = new Set<Id>();
		/** ジョブID */
		public Set<Id> jobIds = new Set<Id>();
		/** 社員ベースID */
		public Set<Id> employeeIds = new Set<Id>();
		/** 対象日 */
		public AppDate targetDate;
		/**
		 * 取得対象日範囲
		 * 開始日から終了日のうち、1日でも有効なジョブがあれば取得対象とする。
		 * 終了日に有効なジョブも含む。
		 */
		public AppDateRange dateRange;
	}

	/**
	 * IDを条件にエンティティを検索する。
	 * @param id ジョブ割当ID
	 * @return 検索結果
	 */
	public JobAssignEntity getEntity(Id id) {
		SearchFilter filter = new SearchFilter();
		filter.ids.add(id);
		List<JobAssignEntity> entities = searchEntity(filter, NOT_FOR_UPDATE);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 検索条件に該当するエンティティは検索する。
	 * @param filter 検索条件
	 * @return 検索結果（存在しない場合は空のリスト）
	 */
	public List<JobAssignEntity> searchEntity(SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	public List<JobAssignEntity> searchEntity(SearchFilter filter, Boolean forUpdate) {
		List<String> conditions = new List<String>();

		// ID
		Set<Id> ids;
		if (!filter.ids.isEmpty()) {
			ids = filter.ids;
			conditions.add('Id IN :ids');
		}
		// ジョブID
		Set<Id> jobIds;
		if (!filter.jobIds.isEmpty()) {
			jobIds = filter.jobIds;
			conditions.add(getFieldName(ComJobAssign__c.JobId__c) + ' IN :jobIds');
		}
		// 社員ID
		Set<Id> employeeIds;
		if (!filter.employeeIds.isEmpty()) {
			employeeIds = filter.employeeIds;
			conditions.add(getFieldName(ComJobAssign__c.EmployeeBaseId__c) + ' IN :employeeIds');
		}
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date targetDate;
		if (filter.targetDate != null){
			targetDate = filter.targetDate.getDate();
			conditions.add(getFieldName(ComJob__c.ValidFrom__c) + ' <= :targetDate');
			conditions.add(getFieldName(ComJob__c.ValidTo__c) + ' > :targetDate');
		}

		// 対象日範囲
		Date pStartDate;
		Date pEndDate;
		if (filter.dateRange != null) {
			pStartDate = filter.dateRange.startDate.getDate();
			pEndDate = filter.dateRange.endDate.getDate();
			conditions.add(getFieldName(ComJobAssign__c.ValidTo__c) + ' > :pStartDate');
			conditions.add(getFieldName(ComJobAssign__c.ValidFrom__c) + ' <= :pEndDate');
		}

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		selectFieldList.addAll(FIELD_NAME_LIST);			// ジョブ割当
		selectFieldList.addAll(JOB_FIELD_NAME_LIST);		// ジョブ
		selectFieldList.addAll(EMPLOYEE_FIELD_NAME_LIST);	// 社員

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ComJobAssign__c.SObjectType.getDescribe().getName()
				+ buildWhereString(conditions)
				+ ' ORDER BY '
				+ getFieldName(EMPLOYEE_R, ComEmpBase__c.Code__c) + ' Asc, '
				+ getFieldName(ComJobAssign__c.ValidFrom__c) + ' Desc '
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug(LoggingLevel.DEBUG, 'JobAssignRepository: SOQL=' + soql);

		// クエリ実行
		List<ComJobAssign__c> sObjs = (List<ComJobAssign__c>)Database.query(soql);

		// SObjectからエンティティを作成
		List<JobAssignEntity> entities = new List<JobAssignEntity>();
		for (ComJobAssign__c sObj : sObjs) {
			entities.add(new JobAssignEntity(sObj));
		}
		return entities;
	}

	/**
	 * ジョブ割当を1件保存する
	 * @param entity 保存対象のジョブ割当
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntity(JobAssignEntity entity) {
		 return saveEntityList(new List<JobAssignEntity>{entity});
	 }

	/**
	 * ジョブ割当を保存する
	 * @param entityList 保存対象のジョブ割当
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntityList(List<JobAssignEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	 }

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveEntityList(List<JobAssignEntity> entityList, Boolean allOrNone) {
		List<ComJobAssign__c> assigns = new List<ComJobAssign__c>();
		for (JobAssignEntity entity : entityList) {
			assigns.add(entity.createSObject());
		}
		List<Database.UpsertResult> results = AppDatabase.doUpsert(assigns, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(results);
		for (Integer i = 0; i < entityList.size(); i++) {
			entityList[i].setId(result.details[i].id);
		}
		return result;
	}
}