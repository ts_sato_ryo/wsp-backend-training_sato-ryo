/**
 * FIXME：V5既存ソースコードをリプレース
 * Range は、Rangesの補助クラスで、１つの開始終了時刻を持つ時間の範囲を表します。
 * Range 同士の and, or 演算とある時刻を基準にした前後を切り取る演算をサポートします。
 * 演算の結果は、空の場合を表すため、Optional<Range>になります。
 * Rangeは不変のクラスで、変更する場合はコピーが発生します。
 * Rangeは比較可能です。
 */
public with sharing class AttCalcRangeDto{
	public Integer st { get; set; }
	public Integer et { get; set; }
	public Integer st0 { get { return st == null?0:st; } }
	public Integer et0 { get { return et == null?(24*60*10):et;} }
	public String id { get; set; }
	public Integer flag { get; set; }
	public AttCalcRangeDto(Integer s, Integer e) { st = s; et = e; flag = 0; }
	public AttCalcRangeDto(Integer s, Integer e, Integer f) { st = s; et = e; flag = f; }
	public AttCalcRangeDto(AttCalcRangeDto r) { st = r.st; et = r.et; flag = r.flag; }
	public Integer width() { return (et != null && st!= null) ? et - st : 0; }
	public AttCalcRangeDto cutBefore(Integer t) {
		return et <= t ? this : (t <= st ? null : new AttCalcRangeDto(st, t, flag));
	}
	public AttCalcRangeDto cutAfter(Integer t) {
		return t <= st ? this : (et <= t ? null : new AttCalcRangeDto(t, et, flag));
	}
	public AttCalcRangeDto cutBetween(Integer st0, Integer et0) {
		return (st0 <= st && et <= et0)
					? this
					: (et0 <= st || et <= st0)
						? null
						: new AttCalcRangeDto(st0 < st ? st :st0, et0 > et ? et : et0, flag)
				;
	}
	public Integer widthBetween(Integer st0, Integer et0) {
		if(et0 <= st || et <= st0) return 0;
		if(st0 < st) st0 = st;
		if(et0 > et) et0 = et;
		return et0 - st0;
	}
	public boolean exclude(AttCalcRangesDto rs, AttCalcRangeDto r) {
		Integer s = rs.size();
		if(r.et <= st || et <= r.st)
			rs.add(this);
		else {
			if(st < r.st) rs.add(new AttCalcRangeDto(st, r.st, flag));
			if(r.et < et) rs.add(new AttCalcRangeDto(r.et, et, flag));
		}
		return s < rs.size();
	}
	public boolean equals(AttCalcRangeDto r) { return (this == r) || (r != null && st == r.st && et == r.et && flag == r.flag && id == r.id); }
	public String toStr() { return '{st:' + st + ',et:' + et  + ',f:' + flag + ((id != null) ? ',id:' + id : '') + '}'; }

	// 以下、新ロジック
	/**
	 * 範囲の中に入るように時刻 t を調整して返す
	 *
	 * 範囲の開始時刻より小さい場合は開始時刻、終了時刻よより大きい場合は終了時刻を返す
	 * @param t 調整対象の時刻
	 * @return 調整後の時刻
	 */
	public Integer constrain(Integer t) { return Math.min(Math.max(t, st), et); }

	/**
	 * 範囲の時間を返す。範囲が逆転している場合は0を返す。
	 * @return 範囲の時間
	 */
	public Integer time() {
		return Math.max(et - st, 0);
	}
	/**
	 * 範囲の開始時刻を返す
	 * @return 開始時刻
	 */
	public Integer getStartTime() {
		return st;
	}
	/**
	 * 範囲の終了時刻を返す
	 * @return 終了時刻
	 */
	public Integer getEndTime() {
		return et;
	}
	public Integer compareTo(AttCalcRangeDto r) {
			if(st < r.st)
				return -1;
			if(st > r.st)
				return 1;
			if(et < r.et)
				return -1;
			if(et > r.et)
				return 1;
			return 0;
		}
	/**
	 * ふたつのRangeが接続して１つのRangeに融合可能かどうか判定する。isOverlapと異なり重なってなくても間がなければTrueを返す。
	 * orメソッド は isJoinableの時のみ結果を返す
	 * @param r1 判定対象のRange 1
	 * @param r2 判定対象のRange 2
	 * @return true 接続可能, false 接続できない
	 */
	public static boolean isJoinable(AttCalcRangeDto r1, AttCalcRangeDto r2) {
		if(r1.et < r2.st) return false;
		if(r2.et < r1.st) return false;
		return true;
	}
	/**
	 * ふたつのRangeが重なっていればTrueを返す。
	 * andメソッド は isOverlapの時のみ結果を返す
	 * @param r1 判定対象のRange 1
	 * @param r2 判定対象のRange 2
	 * @return true 重なっている, false 重なっていない
	 */
	public static boolean isOverlap(AttCalcRangeDto r1, AttCalcRangeDto r2) {
		if(r1.et <= r2.st) return false;
		if(r2.et <= r1.st) return false;
		return true;
	}
	/**
	 * ふたつのRangeをandした時間帯を返す。
	 * @param r1 andするRange 1
	 * @param r2 andするRange 2
	 * @return 重なっている時間帯。ただし、2つが重なっていない場合はempty()を返す
	 */
	public static AttCalcRangeDto andRange(AttCalcRangeDto r1, AttCalcRangeDto r2) {
		if(isOverlap(r1, r2)) {
			Integer st = Math.max(r1.st, r2.st);
			Integer et = Math.min(r1.et, r2.et);
			if(st < et) {
				return new AttCalcRangeDto(st, et);
			}
		}
		return null;
	}
	/**
	 * ふたつのRangeをorした時間帯を返す。
	 * @param r1 orするRange 1
	 * @param r2 orするRange 2
	 * @return orした時間帯。ただし、2つがisJoinableでない場合はempty()を返す
	 */
	public static AttCalcRangeDto orRange(AttCalcRangeDto r1, AttCalcRangeDto r2) {
		if(isJoinable(r1, r2)) {
			Integer st = Math.min(r1.st, r2.st);
			Integer et = Math.max(r1.et, r2.et);
			if(st < et) {
				return new AttCalcRangeDto(st, et);
			}
		}
		return null;
	}
	public override String toString() {
		return st + '-' + et;
	}
}