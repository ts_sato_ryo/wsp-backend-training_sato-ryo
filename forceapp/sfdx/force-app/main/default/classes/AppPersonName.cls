/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * 人の名前を表す値オブジェクト
 */
public with sharing class AppPersonName extends ValueObject {

	/** 名(L0) */
	public String firstNameL0 {get; private set;}
	/** 名(L1) */
	public String firstNameL1 {get; private set;}
	/** 名(L2) */
	public String firstNameL2 {get; private set;}

	/** 姓(L0) */
	public String lastNameL0 {get; private set;}
	/** 姓(L1) */
	public String lastNameL1 {get; private set;}
	/** 姓(L2) */
	public String lastNameL2 {get; private set;}


	/**
	 * コンストラクタ
	 * @param firstNameL0 名(L0)
	 * @param lastNameL0 姓(L0)
	 * @param firstNameL1 名(L1)
	 * @param lastNameL1 姓(L1)
	 * @param firstNameL2 名(L2)
	 * @param lastNameL2 姓(L2)
	 */
	public AppPersonName(
			String firstNameL0, String lastNameL0,
			String firstNameL1, String lastNameL1,
			String firstNameL2, String lastNameL2) {

		this.lastNameL0 = lastNameL0;
		this.firstNameL0 = firstNameL0;
		this.lastNameL1 = lastNameL1;
		this.firstNameL1 = firstNameL1;
		this.lastNameL2 = lastNameL2;
		this.firstNameL2 = firstNameL2;
	}

	/**
	 * 実行ユーザの言語を考慮した氏名を返却する
	 * @return 氏名
	*/
	public String getFullName() {
		return getFullName(AppLanguage.getUserLang());
	}

	/**
	 * 指定した言語の氏名を返却する
	 * @param 言語キー
	 * @return 氏名
	 */
	public String getFullName(AppLanguage lang) {

		String fullName;
		if (lang != null) {
			fullName = createFullName(lang);
		}
		// 値が取得できなかった場合、
		// 　→ 取得対象の言語キーがL0以外で(L0は必須項目なので必ず値が設定されているはず)、
		// 　  → en_USのキーが存在し、値がnullでなければ英語で返却
		// 　  → そうでない場合は、主言語L0で返却
		if (String.isBlank(fullName)) {
			if ((lang != null) && (lang.getLangkey() != AppLanguage.LangKey.L0)) {
				final AppLanguage.LangKey enKey = AppLanguage.EN_US.getLangkey();

				if (enKey != null) {
					fullName = createFullName(AppLanguage.EN_US);
				}
				if (String.isBlank(fullName)) {
					// L0で返却。L0の言語が不明(組織設定が未登録)の場合は、英語の表記で返却しておく
					AppLanguage langL0 = AppLanguage.getLangByLangKey(AppLanguage.LangKey.L0);
					if (langL0 == null) {
						langL0 = AppLanguage.EN_US;
					}
					fullName = createFullName(langL0, this.lastNameL0, this.firstNameL0);
				}
			}
		}

		return fullName;
	}

	/**
	 * 指定された言語の氏名を作成する
	 */
	private String createFullName(AppLanguage lang) {
		final AppLanguage.LangKey langKey = lang.getLangkey();
		String fullName;
		if (langKey == AppLanguage.LangKey.L0) {
			fullName = createFullName(lang, this.lastNameL0, this.firstNameL0);
		} else if (langKey == AppLanguage.LangKey.L1) {
			fullName = createFullName(lang, this.lastNameL1, this.firstNameL1);
		} else if (langKey == AppLanguage.LangKey.L2) {
			fullName = createFullName(lang, this.lastNameL2, this.firstNameL2);
		}

		return fullName;
	}

	/**
	 * 姓と名、及び言語を指定して名前を取得
	 * @param language 表示言語
	 * @param lastName 姓
	 * @param firstName 名
	 * @return 名前（姓と名の間に半角スペースを入れて返す、姓と名が空白の場合空白を返す）
	 */
	private String createFullName(AppLanguage language, String lastName, String firstName) {
		// 成型
		lastName = String.isBlank(lastName) ? '' : lastName.trim();
		firstName = String.isBlank(firstName) ? '' : firstName.trim();
		// 姓と名が入力されているか
		if (String.isBlank(lastName) && String.isBlank(firstName)) {
			return '';
		}
		// 姓名表記であるか
		if (isLastFirstName(language)) {
			return (lastName + ' ' + firstName).trim();
		}
		return (firstName + ' ' + lastName).trim();
	}

	/**
	 * 指定された言語は姓名表記であるか
	 * @param lang 表示言語
	 * @return 姓名表記である
	 */
	private Boolean isLastFirstName(AppLanguage lang) {
		return LAST_FIRST_NAME_LANG.contains(lang);
	}

	/** 姓名表記の言語の一覧 (本来言語はロケールで決まるものですが。。)*/
	private static final Set<AppLanguage> LAST_FIRST_NAME_LANG;
	static {
		LAST_FIRST_NAME_LANG = new Set<AppLanguage> {
			AppLanguage.JA};
	}

	/**
	 * オブジェクトの値が等しいかどうか判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {
		Boolean eq;
		if (compare instanceof AppPersonName) {
			AppPersonName compareObj = (AppPersonName)compare;
			if ((compareObj.firstNameL0 == this.firstNameL0)
					&& (compareObj.lastNameL0 == this.lastNameL0)
					&& (compareObj.firstNameL1 == this.firstNameL1)
					&& (compareObj.lastNameL1 == this.lastNameL1)
					&& (compareObj.firstNameL2 == this.firstNameL2)
					&& (compareObj.lastNameL2 == this.lastNameL2)) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}
		return eq;
	}
}
