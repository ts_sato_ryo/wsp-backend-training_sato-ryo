/**
  * @group 共通
  *
  * 位置情報エンティティ
  */
public with sharing class ComLocationEntity extends ComLocationGeneratedEntity {
	/**
	 * コンストラクタ
	 */
	public ComLocationEntity() {
		super(new ComLocation__c());
	}

	/**
	 * コンストラクタ
	 */
	public ComLocationEntity(ComLocation__c sobj) {
		super(sobj);
	}

	/* 種別 */
	public static final Schema.SObjectField FIELD_TYPE = ComLocation__c.Type__c;
	public LocationType rcdType {
		get {
			String value = (String)getFieldValue(FIELD_TYPE);
			return LocationType.valueOf(value);
		}
		set {
			setFieldValue(FIELD_TYPE, AppConverter.stringValue(value));
		}
	}

	/** 位置情報 */
	public static final Schema.SObjectField FIELD_LOCATION_LATITUDE = ComLocation__c.Location__Latitude__s;
	public static final Schema.SObjectField FIELD_LOCATION_LONGITUDE = ComLocation__c.Location__Longitude__s;
	public Location rcdLocation {
		get {
				Double longitude = (Double)getFieldValue(FIELD_LOCATION_LONGITUDE);
				Double latitude = (Double)getFieldValue(FIELD_LOCATION_LATITUDE);
				return Location.newInstance(latitude, longitude);
		}
		set {
			// 緯度、経度を別々に保存する
			if (value != null) {
				setFieldValue(FIELD_LOCATION_LATITUDE, value.latitude);
				setFieldValue(FIELD_LOCATION_LONGITUDE, value.longitude);
			} else {
				setFieldValue(FIELD_LOCATION_LATITUDE, null);
				setFieldValue(FIELD_LOCATION_LONGITUDE, null);
			}
		}
	}

	/* 打刻種別 */
	public static final Schema.SObjectField FIELD_ATT_STAMP_TYPE = ComLocation__c.AttStampType__c;
	public AttStampType rcdAttStampType {
		get {
			String value = (String)getFieldValue(FIELD_ATT_STAMP_TYPE);
			return AttStampType.valueOf(value);
		}
		set {
			setFieldValue(FIELD_ATT_STAMP_TYPE, AppConverter.stringValue(value));
		}
	}


	/* 記録元 */
	public static final Schema.SObjectField FIELD_SOURCE = ComLocation__c.Source__c;
	public LocationSource source {
		get {
			String value = (String)getFieldValue(FIELD_SOURCE);
			return LocationSource.valueOf(value);
		}
		set {
				setFieldValue(FIELD_SOURCE, AppConverter.stringValue(value));
		}
	}

	/**
	 * 更新対象のSObjectを生成する。
	 */
	public override ComLocation__c createSObject() {
		ComLocation__c sobj = super.createSObject();
		return sobj;
	}
}