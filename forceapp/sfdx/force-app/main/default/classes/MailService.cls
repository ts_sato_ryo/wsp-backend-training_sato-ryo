/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * メール送信サービス
  */
public with sharing class MailService {

  /** 送信用メールリスト */
  private List<Messaging.SingleEmailMessage> mails;

  public MailService() {
    mails = new List<Messaging.SingleEmailMessage>();
  }

	/**
	 * メール内容を格納するクラス
	 */
	public class mailParam {
		/** 送信者名 */
		public String senderDisplayName;
		/** タイトル */
		public String subject;
		/** 本文 */
		public String body;
	}

  /**
   * @description メールを作成する
   * @param mailMessage メールの送信者名（senderDisplayName）、タイトル（subject）、本文（body） が格納されたMAP
   * @param targetId 送信先のユーザID
   */
  public void createMail(mailParam mailMessage, Id targetId) {

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setSaveAsActivity(false);
		mail.setTargetObjectId(targetId);
		mail.setSenderDisplayName(mailMessage.senderDisplayName);
		mail.setSubject(mailMessage.subject);
		mail.setPlainTextBody(mailMessage.body);
		mails.add(mail);
  }

  /**
   * @description メールを送信する
   */
  public void send() {
    if (mails != null && mails.size() > 0) {
      Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
			if (!results[0].success) {
				System.debug('承認取消時のメール送信が失敗しました');
			}
    }
  }
}