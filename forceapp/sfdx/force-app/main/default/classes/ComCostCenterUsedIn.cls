/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Cost Center UsedIn picklist
 */
public with sharing class ComCostCenterUsedIn extends ValueObjectType {
    
    /** Expense Report Only */
	public static final ComCostCenterUsedIn EXPENSE_REPORT = new ComCostCenterUsedIn('ExpenseReport');
	/** Expense Report and Request */
	public static final ComCostCenterUsedIn EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ComCostCenterUsedIn('ExpenseRequestAndExpenseReport');

	/** Entries */
	public static final List<ComCostCenterUsedIn> TYPE_LIST;
	static {
		TYPE_LIST = new List<ComCostCenterUsedIn> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}

	/**
	 * Constructor
	 * @param value
	 */
	private ComCostCenterUsedIn(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
	 * @param The key value to get the instance
	 * @return The instance having the value specified
	 */
	public static ComCostCenterUsedIn valueOf(String value) {
		ComCostCenterUsedIn retType = null;
		if (String.isNotBlank(value)) {
			for (ComCostCenterUsedIn type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {

		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ComCostCenterUsedIn) {
			if (this.value == ((ComCostCenterUsedIn)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}
}
