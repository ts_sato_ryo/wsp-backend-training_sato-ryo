/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 権限エンティティ
 */
public with sharing class PermissionEntity extends PermissionGeneratedEntity {

	/** コードの最大文字列長さ */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 名前の最大文字列長さ */
	public static final Integer NAME_MAX_LENGTH = 80;

	/**
	 * コンストラクタ
	 */
	public PermissionEntity() {
		super(new ComPermission__c());
	}

	/**
	 * コンストラクタ
	 */
	public PermissionEntity(ComPermission__c sobj) {
		super(sobj);
	}

	/** 権限名(多言語対応。参照専用) */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	/**
	 * ユニークキーを作成する
	 * インスタンス変数のcodeを指定しておくこと。
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]アクセス権限設定のユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]アクセス権限設定のユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + コード
		return companyCode + '-' + this.code;
	}
}