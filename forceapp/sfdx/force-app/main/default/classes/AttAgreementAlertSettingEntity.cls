/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 36協定アラート設定マスタのエンティティ
 */
public with sharing class AttAgreementAlertSettingEntity extends AttAgreementAlertSettingGeneratedEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 36協定アラート設定名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 残業時間の最小値 */
	public static final Integer HOUR_MIN = 0;
	/** 残業時間の最大値(DBに保存できる最大値) */
	public static final Integer HOUR_MAX = 99999;

	/**
	 * コンストラクタ
	 */
	public AttAgreementAlertSettingEntity() {
		super(new AttAgreementAlertSetting__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttAgreementAlertSettingEntity(AttAgreementAlertSetting__c sobj) {
		super(sobj);
	}

	/** 会社(参照専用) */
	public ValidPeriodCompanyEntity.Company company {
		get {
			if (company == null) {
				company = new ValidPeriodCompanyEntity.Company(sobj.CompanyId__r.Code__c);
			}
			return company;
		}
		private set;
 	}

	/** 36協定アラート設定名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}

	/**
	 * エンティティの複製を作成する(IDはコピーされません)
	 */
	public AttAgreementAlertSettingEntity copy() {
		AttAgreementAlertSettingEntity copyEntity = new AttAgreementAlertSettingEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}
}
