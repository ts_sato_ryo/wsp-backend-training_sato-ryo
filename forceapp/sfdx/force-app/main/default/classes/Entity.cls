/**
 * エンティティ基底クラス
 * Winter'19よりエンティティの設計変更により、BaseEntityを基底クラスとして移行しています。
 * 移行が完了したらこのクラスは削除する予定です。
 */
public abstract with sharing class Entity {

	/** ID(取得用) 設定はsetterを利用してください。*/
	public Id id {get; private set;}

	/** IDを設定する */
	public void setId(String id) {
		try {
			this.id = id;
		} catch (Exception e) {
			//
			throw e;
		}
	}

	/** IDを設定する */
	public void setId(Id id) {
		this.id = id;
	}

}
