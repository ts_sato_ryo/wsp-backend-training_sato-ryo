/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数設定のベースエンティティ
 */
public with sharing class TimeSettingBaseEntity extends TimeSettingBaseGeneratedEntity {

	/** コードの最大文字列長さ */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 月度の開始日の最小値 */
	public static final Integer START_DATE_OF_MONTH_MIN = 1;
	/** 月度の開始日の最大値 */
	public static final Integer START_DATE_OF_MONTH_MAX = 28;

	/** サマリー期間マップ */
	private final static Map<String, SummaryPeriodType> SUMMARY_PERIOD_TYPE_MAP;
	static {
		SUMMARY_PERIOD_TYPE_MAP = new Map<String, SummaryPeriodType>();
		for (SummaryPeriodType type : SummaryPeriodType.values()) {
			SUMMARY_PERIOD_TYPE_MAP.put(type.name(), type);
		}
	}

	/** 月度の表記マップ */
	private final static Map<String, MonthMarkType> MONTH_MARK_TYPE_MAP;
	static {
		MONTH_MARK_TYPE_MAP = new Map<String, MonthMarkType>();
		for (MonthMarkType type : MonthMarkType.values()) {
			MONTH_MARK_TYPE_MAP.put(type.name(), type);
		}
	}

	/** 起算曜日マップ */
	private final static Map<String, StartDayOfWeekType> START_DAY_OF_WEEK_MAP;
	static {
		START_DAY_OF_WEEK_MAP = new Map<String, StartDayOfWeekType>();
		for (StartDayOfWeekType type : StartDayOfWeekType.values()) {
			START_DAY_OF_WEEK_MAP.put(type.name(), type);
		}
	}
	/**
	 * 曜日の判定で使用するマップ(起算曜日からAppDate.DayOfWeekの曜日を返す)
	 * FIXME: AppDate.DayOfWeekのordinalで対応するかも検討したが、複雑かな？と思い数字で実装
	 */
	private static final Map<StartDayOfWeekType, Integer> APP_DAY_OF_WEEK_MAP;
	static {
		APP_DAY_OF_WEEK_MAP = new Map<StartDayOfWeekType, Integer> {
				StartDayOfWeekType.Sunday => 0,
				StartDayOfWeekType.Monday => 1,
				StartDayOfWeekType.Tuesday => 2,
				StartDayOfWeekType.Wednesday => 3,
				StartDayOfWeekType.Thursday => 4,
				StartDayOfWeekType.Friday => 5,
				StartDayOfWeekType.Saturday => 6
			};
	}

	/**
	 * デフォルトコンストラクタ
	 */
	public TimeSettingBaseEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public TimeSettingBaseEntity(TimeSettingBase__c sobj) {
		super(sobj);
	}

	/** 履歴エンティティのリスト */
	@testVisible
	protected List<TimeSettingHistoryEntity> historyList;

	/** 会社(参照専用) */
	public ParentChildBaseCompanyEntity.Company company {
		get {
			if (company == null) {
				company = new ParentChildBaseCompanyEntity.Company(sobj.CompanyId__r.Code__c);
			}
			return company;
		}
		private set;
	}

	/** サマリー期間を設定する */
	public void setSummaryPeriod(String value) {

		if (! String.isBlank(value) && ! SUMMARY_PERIOD_TYPE_MAP.containsKey(value)) {
			throw new App.ParameterException('指定された値のSummaryPeriodTypeが存在しません。(value=' + value + ')');
		}
		this.summaryPeriod = SUMMARY_PERIOD_TYPE_MAP.get(value);
	}

	/** 月度の表記を設定する */
	public void setMonthMark(String value) {

		if (! String.isBlank(value) && ! MONTH_MARK_TYPE_MAP.containsKey(value)) {
			throw new App.ParameterException('指定された値のMonthMarkTypeが存在しません。(value=' + value + ')');
		}

		this.monthMark = MONTH_MARK_TYPE_MAP.get(value);
	}

	/** 起算曜日を設定する */
	public void setStartDayOfWeek(String value) {

		if (! String.isBlank(value) && ! START_DAY_OF_WEEK_MAP.containsKey(value)) {
			throw new App.ParameterException('指定された値のStartDayOfWeekTypeが存在しません。(value=' + value + ')');
		}

		this.startDayOfWeek = toStartDayOfWeekType(value);
	}

	/**
	 * エンティティの起算曜日をAppDate.DayOfWeekの数値に変換する
	 */
	public Integer convertAppDateDayOfWeek() {
		if (this.startDayOfWeek == null) {
			return null;
		}
		return APP_DAY_OF_WEEK_MAP.get(this.startDayOfWeek);
	}

	/**
	 * 履歴の基底クラスに変換する
	 */
	protected override List<ParentChildHistoryEntity> convertSuperHistoryList() {
		return (List<ParentChildHistoryEntity>)historyList;
	}

	/**
	 * 履歴エンティティリストを取得する
	 * @return 履歴エンティティリスト
	 */
	public List<TimeSettingHistoryEntity> getHistoryList() {
		return historyList;
	}

	/**
	 * 履歴エンティティをリストに追加する
	 * @param history 追加する履歴エンティティ
	 */
	 public void addHistory(TimeSettingHistoryEntity history) {
		 if (historyList == null) {
			historyList = new List<TimeSettingHistoryEntity>();
		 }
		 historyList.add(history);
	 }

	 /**
	  * 部署履歴リストを取得する
	  * @return 部署名。履歴が存在しない場合はnull
	  */
	 public TimeSettingHistoryEntity getHistory(Integer index) {
		 if (historyList == null) {
			 return null;
		 }
		 return historyList[index];
	 }

	/**
	 * 指定した日に有効な履歴を取得する
	 * 履歴リストに論理削除されている履歴が含まれている場合は利用しないでください
	 * @param targetDate 対象日
	 * @return 有効な履歴、ただし有効な履歴が存在しない場合はnull
	 */
	public TimeSettingHistoryEntity getHistoryByDate(AppDate targetDate) {
		return (TimeSettingHistoryEntity)getSuperHistoryByDate(targetDate);
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public TimeSettingBaseEntity copy() {
		TimeSettingBaseEntity copyEntity = new TimeSettingBaseEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildBaseEntity copySuperEntity() {
		return copy();
	}
}
