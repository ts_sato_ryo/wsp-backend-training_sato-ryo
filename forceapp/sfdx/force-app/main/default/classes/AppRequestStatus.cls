/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 申請ステータス
 */
public with sharing class AppRequestStatus extends ValueObjectType {

	/** 無効 */
	public static final AppRequestStatus DISABLED = new AppRequestStatus('Disabled');
	/** 申請中 */
	public static final AppRequestStatus PENDING = new AppRequestStatus('Pending');
	/** 承認済み */
	public static final AppRequestStatus APPROVED = new AppRequestStatus('Approved');
	/** 変更承認待ち */
	public static final AppRequestStatus REAPPLYING = new AppRequestStatus('Reapplying');

	/** 申請ステータスのリスト（ステータスが追加されら本リストにも追加してください） */
	public static final List<AppRequestStatus> STATUS_LIST;
	static {
		STATUS_LIST = new List<AppRequestStatus> {
			DISABLED,
			PENDING,
			APPROVED,
			REAPPLYING
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AppRequestStatus(String value) {
		super(value);
	}

	/**
	 * 値からAppRequestStatusを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAppRequestStatus
	 */
	public static AppRequestStatus valueOf(String value) {
		AppRequestStatus retStatus = null;
		if (String.isNotBlank(value)) {
			for (AppRequestStatus status : STATUS_LIST) {
				if (value == status.value) {
					retStatus = status;
				}
			}
		}
		return retStatus;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AppRequestStatus以外の場合はfalse
		Boolean eq;
		if (compare instanceof AppRequestStatus) {
			// 値が同じであればtrue
			if (this.value == ((AppRequestStatus)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}