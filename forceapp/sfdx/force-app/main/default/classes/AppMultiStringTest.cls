/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group アプリ
 *
 * 多言語文字列の値オブジェクトのテスト
 */
@isTest
private class AppMultiStringTest {

	/**
	 * コンストラクタのテスト
	 * 設定した値が取得できることを確認する
	 */
	@isTest private static void constructorTest() {
		String l0 = 'TestLang0';
		String l1 = 'TestLang1';
		String l2 = 'TestLang2';

		AppMultiString multi = new AppMultiString(l0, l1, l2);

		System.assertEquals(l0, multi.valueL0);
		System.assertEquals(l1, multi.valueL1);
		System.assertEquals(l2, multi.valueL2);
	}

	/**
	 * ユーザの言語で文字列が取得できることを確認する
	 */
	@isTest private static void getValueTest() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		User userEn = ComTestDataUtility.createUser('Multi0', 'en_US', UserInfo.getLocale());
		User userJa = ComTestDataUtility.createUser('Multi1', 'ja', UserInfo.getLocale());
		User userOther = ComTestDataUtility.createUser('Multi2', 'de', UserInfo.getLocale());

		String l0 = 'TestLang0';
		String l1 = 'TestLang1';
		String l2 = 'TestLang2';
		AppMultiString multi = new AppMultiString(l0, l1, l2);

		// L0言語を取得
		System.runAs(userJa) {
			System.assertEquals(l0, multi.getValue());
		}

		// L1の言語を取得
		System.runAs(userEn) {
			System.assertEquals(l1, multi.getValue());
		}

		// L2の言語を取得
		updateOrgLang('ja', null, 'en_US');
		AppLanguage.langKeyMap = null;
		System.runAs(userEn) {
			System.assertEquals(l2, multi.getValue());
		}

		// ユーザの言語がAppLanguage存在しない言語の場合、英語の値が返却される
		updateOrgLang('ja', 'en_US', 'ja');
		AppLanguage.langKeyMap = null;
		System.runAs(userOther) {
			System.assertEquals(l1, multi.getValue());
		}

		// 対象の項目がnullで英語項目が存在しない場合はL0の値が返却される
		updateOrgLang('en_US', 'ja', null);
		AppLanguage.langKeyMap = null;
		multi = new AppMultiString(l0, null, l2);
		System.runAs(userJa) {
			System.assertEquals(l0, multi.getValue());
		}

//		// TODO ３カ国以上サポートするようになったらテストを実施すること
//		// 対象の項目がnullで英語項目が存在する場合は英語の値が返却される
//		updateOrgLang('ja', 'en_US', 'zh_CN');
//		AppLanguage.langKeyMap = null;
//		multi = new AppMultiString(null, l1, l2);
//		System.runAs(userCn) { // TODO userCn作成
//			System.assertEquals(l1, multi.getValue());
//		}

	}

	/**
	 * 言語設定を更新する
	 */
	private static void updateOrgLang(String lang0, String lang1, String lang2) {
		ComOrgSetting__c org = [SELECT Id, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c LIMIT 1];
		org.Language_0__c = lang0;
		org.Language_1__c = lang1;
		org.Language_2__c = lang2;
		update org;
	}

	/**
	 * stringValue()のテスト
	 */
	@isTest private static void stringValueTest() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('en_US', 'ja', null);
		User userL0 = ComTestDataUtility.createUser('TestLang0', 'en_US', UserInfo.getLocale());

		String l0 = 'TestLang0';
		String l1 = 'TestLang1';
		String l2 = 'TestLang2';
		AppMultiString multi;

		System.runAs(userL0) {

			// インスタンスがnullでない場合
			// 実行ユーザの言語の文字列が取得できる
			multi = new AppMultiString(l0, l1, l2);
			System.assertEquals(l0, AppMultiString.stringValue(multi));

			// インスタンスがnullの場合
			// nullが返却される
			multi = null;
			System.assertEquals(null, AppMultiString.stringValue(multi));
		}
	}

	/**
	 * equalsのテスト
	 */
	@isTest private static void equalsTest() {
		AppMultiString str = new AppMultiString('Test_L0', 'Test_L1', 'Test_L2');
		AppMultiString compare = new AppMultiString('Test_L0', 'Test_L1', 'Test_L2');

		// 等しい
		System.assertEquals(str, compare);

		// 値が等しくない
		compare = new AppMultiString('Diff_Test_L0', 'Test_L1', 'Test_L2');
		System.assertNotEquals(str, compare);
		compare = new AppMultiString('Test_L0', 'Diff_Test_L1', 'Test_L2');
		System.assertNotEquals(str, compare);
		compare = new AppMultiString('Test_L0', 'Test_L1', 'Diff_Test_L2');
		System.assertNotEquals(str, compare);

		// 型が異なる
		System.assertEquals(false, str.equals('Test_L0'));

		// null
		System.assertEquals(false, str.equals(null));
	}

}
