/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description 作業分類レコードおよび関連情報を操作するAPIを実装するクラス
*/
public with sharing class WorkCategoryResource {

	/**
	 * @description 作業分類レコードを表すクラス
	 */
	public class WorkCategory implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 作業分類名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 作業分類名(言語0) */
		public String name_L0;
		/** 作業分類名(言語1) */
		public String name_L1;
		/** 作業分類名(言語2) */
		public String name_L2;
		/** 作業分類コード */
		public String code;
		/** 関連する会社レコードID */
		public String companyId;
		/** 有効期間の開始日(yyyy-MM-dd) */
		public Date validDateFrom;
		/** 有効期間の終了日(yyyy-MM-dd) */
		public Date validDateTo;
		/** 表示の並び順 */
		public Integer order;
	}

	/**
	 * @description 作業分類レコード作成結果
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成されたレコードID */
		public String id;
	}

	/**
	 * @description 作業分類レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_WORK_CATEGORY;

		/**
		 * @description 作業分類レコードを1件作成する
		 * @param req リクエストパラメータ (WorkCategoryResource.WorkCategory)
		 * @return レスポンス (WorkCategoryResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			WorkCategory param = (WorkCategory)req.getParam(WorkCategory.class);

			// 権限チェック
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			Id recordId = WorkCategoryResource.saveRecord(param, req.getParamMap(), false);

			SaveResult res = new SaveResult();
			res.id = recordId;
			return res;
		}

	}

	/**
	 * @description 作業分類レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_WORK_CATEGORY;

		/**
		 * @description 作業分類レコードを1件更新する
		 * @param req リクエストパラメータ (WorkCategoryResource.WorkCategory)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			WorkCategory param = (WorkCategory)req.getParam(WorkCategory.class);

			// 権限チェック
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			WorkCategoryResource.saveRecord(param, req.getParamMap(), true);

			return null;
		}

	}

	/**
	 * @description 作業分類レコード削除処理のパラメータ
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
	}

	/**
	 * @description 作業分類レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_WORK_CATEGORY;

		/**
		 * @description 作業分類レコードを1件更新する
		 * @param req リクエストパラメータ (WorkCategoryResource.DeleteOption)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// IDがブランクの場合はエラー
			if (String.isBlank(param.id)) {
				throw new App.ParameterException('パラメータ "id" を指定して下さい');
			}

			// 権限チェック
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				AppDatabase.doDelete(param.Id);
			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

					// TODO 暫定的にデバッグログを出力しておきます
					System.debug(e);

					// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
					//      暫定対応として下記のようなエラーメッセージで対応します。
					// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
					e.setMessage(ComMessage.msg().Com_Err_FaildDeleteReference);
					throw e;
				}
			}

			return null;
		}

	}

	/**
	 * @description 作業分類レコード検索処理のパラメータクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** 作業分類レコードID */
		public String id;
		/** 関連する会社レコードID */
		public String companyId;
		/** 検索結果レコードの並び順 */
		public SortOrder[] sortOrder;
	}

	/**
	 * @description レコード検索時の並び順を格納するクラス
	 */
	public class SortOrder {
		/** ソート対象項目名 */
		public String field;
		/** ソート順(昇順or降順) */
		public String orderBy;
	}

	/**
	 * @description 作業分類レコード検索処理のレスポンスクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public WorkCategory[] records;
	}

	/**
	 * @description 作業分類レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 作業分類レコードを検索する
		 * @param  req リクエストパラメータ (WorkCategoryResource.SearchCondition)
		 * @return レスポンス (TimeWorkCategoryResource.SearchResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);
			Map<String, Object> paramMap = req.getParamMap();

			// ユーザの言語キー(L0〜L2)を取得
			Integer userLangKey = ComUtility.getUserLanguageKey();

			// ユーザ言語に対応するフィールドを取得
			String nameField = (new List<String> {
				'Name_L0__c',
				'Name_L1__c',
				'Name_L2__c'
			})[userLangKey];

			// 検索項目名を取得
			final List<String> FIELD_NAMES = new List<String> {
				'Id',
				'Name',
				'Name_L0__c',
				'Name_L1__c',
				'Name_L2__c',
				'Code__c',
				'UniqKey__c',
				'CompanyId__c',
				'ValidFrom__c',
				'ValidTo__c',
				'Order__c'
			};

			String soql = 'SELECT ' + String.join(FIELD_NAMES, ',') + ' FROM TimeWorkCategory__c';

			// 検索条件を設定
			List<String> whereStrings = new List<String>();
			String pId;
			String pCompanyId;

			// パラメータ id が指定されている場合は、レコードIDで検索
			if(String.isNotBlank(param.Id)){
				// ID値を直接SOQL文に組み込むと
				// ID以外の値が指定された時にSOQLエラーが発生するため、変数をバインドする
				pId = param.Id;
				whereStrings.add('Id =:pId');
			}

			// パラメータが指定されている場合は、会社レコードIDで検索
			if(paramMap.containsKey('companyId')){
				pCompanyId = param.companyId;
				whereStrings.add('CompanyId__c =:pCompanyId');
			}

			// 検索条件をANDで連結
			if(whereStrings.size() > 0){
				soql += ' WHERE ' + String.join(whereStrings, ' AND ');
			}

			List<String> orderByStrings = new List<String>();

			// 並び順指定がない場合は、デフォルトで作業分類コードの昇順でソート
			if(param.sortOrder == null || param.sortOrder.size() == 0){
				SortOrder so = new SortOrder();
				so.field = 'Code__c';
				so.orderBy = 'asc';
				param.sortOrder = new List<SortOrder>{ so };
			}

			// レコードの並び順指定
			for (SortOrder so:param.sortOrder) {
				String orderBy = '';
				if(so.orderBy == 'desc'){
					orderBy = ' DESC';
				}
				if(String.isNotBlank(so.field)){
					orderByStrings.add(so.field + orderBy + ' NULLS LAST');
				}
			}

			soql += ' ORDER BY ' + String.join(orderByStrings, ',');

			// レコードを検索
			List<TimeWorkCategory__c> searchRecords = (List<TimeWorkCategory__c>) Database.query(soql);

			// 結果からエンティティを作成する
			List<WorkCategoryEntity> entityList = new List<WorkCategoryEntity>();
			for (SObject sobj : searchRecords) {
				entityList.add(createEntity((TimeWorkCategory__c)sobj));
			}

			// レスポンスクラスに変換
			List<WorkCategory> curList = new List<WorkCategory>();
			for (WorkCategoryEntity entity : entityList) {
				WorkCategory c = new WorkCategory();
				c.id = entity.Id;
				c.name = entity.nameL.getValue();
				c.name_L0 = entity.nameL0;
				c.name_L1 = entity.nameL1;
				c.name_L2 = entity.nameL2;
				c.code = entity.code;
				c.companyId = entity.companyId;
				c.validDateFrom = AppDate.convertDate(entity.ValidFrom);
				c.validDateTo = AppDate.convertDate(entity.validTo);
				c.order = entity.order;

				curList.add(c);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = curList;
			return res;
		}
	}

	/**
	 * @description 作業分類レコードを1件作成または更新する<br/>
	 *    ※他のクラスから参照しないで下さい<br/>
	 *    TODO:DAOクラスに移動
	 * @param  param レコード値を含むパラメータ
	 * @param  paramMap レコード値を含むパラメータのマップ
	 * @param  isUpdate レコード更新時はtrue
	 * @return 作成または更新した作業分類レコード
	 */
	public static Id saveRecord (WorkCategory param, Map<String, Object> paramMap, Boolean isUpdate) {
		Set<String> paramKeys = paramMap.keySet();

		// レコード更新時はID必須
		if (isUpdate && String.isBlank(param.id)) {
			throw new App.ParameterException('パラメータ "id" を指定してください');
		}

		WorkCategoryEntity entity = new WorkCategoryEntity();

		if (isUpdate) {
			entity.setId(param.Id);
		}
		if (paramKeys.contains('name_L0')) {
			entity.nameL0 = param.name_L0;

			// TODO nameは仮置きでnameL0を設定
			entity.name = param.name_L0;
		}
		if (paramKeys.contains('name_L1')) {
			entity.nameL1 = param.name_L1;
		}
		if (paramKeys.contains('name_L2')) {
			entity.nameL2 = param.name_L2;
		}
		if (paramKeys.contains('code')) {
			entity.code = param.code;
		}
		if (paramKeys.contains('companyId')) {
			entity.companyId = param.companyId;
		}
		if (paramKeys.contains('validDateFrom')) {
			entity.validFrom = AppDate.valueOf(param.validDateFrom);
		}
		// 有効開始日のデフォルトは実行日
		if (entity.validFrom == null) {
			entity.validFrom = AppDate.today();
		}
		if (paramKeys.contains('validDateTo')) {
			entity.validTo = AppDate.valueOf(param.validDateTo);
		}
		if (entity.validTo == null) {
			entity.validTo = ValidPeriodEntity.VALID_TO_MAX;
		}
		if (paramKeys.contains('order')) {
			entity.order = param.order;
		}

		Repository.SaveResult result = (new WorkCategoryService()).saveWorkCategory(entity);
		return result.details[0].id;
	}

	// TODO: リポジトリクラスを作成し、そちらに移動する
	/**
	 * SObjectからエンティティを作成する
	 * @param sObjs 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private static WorkCategoryEntity createEntity(TimeWorkCategory__c sObjs) {

		WorkCategoryEntity entity = new WorkCategoryEntity();

		// 有効期間型共通の項目を設定する
		setValidPeriodObjectValueToEntity(sObjs, entity);

		entity.nameL0 = sObjs.Name_L0__c;
		entity.nameL1 = sObjs.Name_L1__c;
		entity.nameL2 = sObjs.Name_L2__c;
		entity.code = sObjs.Code__c;
		entity.uniqKey = sObjs.UniqKey__c;
		entity.companyId = sObjs.CompanyId__c;
		entity.order = Integer.valueOf(sObjs.Order__c);

		return entity;
	}

	// TODO: createEntity()をリポジトリに移動したら削除する
	private static void setValidPeriodObjectValueToEntity(SObject obj, ValidPeriodEntity toEntity) {

		toEntity.setId((Id)obj.get('Id'));
		toEntity.name = (String)obj.get('Name');
		toEntity.validFrom = AppDate.valueOf((Date)obj.get('ValidFrom__c'));
		toEntity.validTo = AppDate.valueOf((Date)obj.get('ValidTo__c'));

		return;
	}
}