/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Repository class for CostCenterBase and CostCenter History records.
 */
public with sharing class CostCenterRepository extends ParentChildRepository {
	private static final Boolean NOT_FOR_UPDATE = false;
	/** Pattern for detecting characters to unescape */
	private static final Pattern SPECIAL_CHARACTERS_PATTERN = Pattern.compile('[%\\_\\\\]');
	/** CostCenterBase Entity to CostCenterHistory Entity Relationship name */
	private static final String HISTORY_R = ComCostCenterBase__c.CurrentHistoryId__c.getDescribe().getRelationshipName();

	public enum HISTORY_SORT_ORDER {
		VALID_ASC, VALID_DESC
	}

	private static final String HISTORY_BASE_R = ComCostCenterHistory__c.BaseId__c.getDescribe().getRelationshipName();

	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> baseFieldList = new Set<SObjectField> {
			ComCostCenterBase__c.Id,
			ComCostCenterBase__c.Name,
			ComCostCenterBase__c.Code__c,
			ComCostCenterBase__c.UniqKey__c,
			ComCostCenterBase__c.CurrentHistoryId__c,
			ComCostCenterBase__c.CompanyId__c
		};
		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(baseFieldList);
	}

	private static final String BASE_COMPANY_R = ComCostCenterBase__c.CompanyId__c.getDescribe().getRelationshipName();
	private static final List<String> GET_BASE_COMPANY_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> companyFieldList = new Set<Schema.SObjectField> {
			ComCompany__c.Code__c
		};
		GET_BASE_COMPANY_FIELD_NAME_LIST = Repository.generateFieldNameList(BASE_COMPANY_R, companyFieldList);
	}

	private static final List<String> GET_HISTORY_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> historyFieldSet = new Set<SObjectField> {
			ComCostCenterHistory__c.Id,
			ComCostCenterHistory__c.Name,
			ComCostCenterHistory__c.BaseId__c,
			ComCostCenterHistory__c.UniqKey__c,
			ComCostCenterHistory__c.Name_L0__c,
			ComCostCenterHistory__c.Name_L1__c,
			ComCostCenterHistory__c.Name_L2__c,
			ComCostCenterHistory__c.LinkageCode__c,
			ComCostCenterHistory__c.ParentBaseId__c,
			ComCostCenterHistory__c.HistoryComment__c,
			ComCostCenterHistory__c.ValidFrom__c,
			ComCostCenterHistory__c.ValidTo__c,
			ComCostCenterHistory__c.Removed__c
		};
		GET_HISTORY_FIELD_NAME_LIST = Repository.generateFieldNameList(historyFieldSet);
	}

	public CostCenterBaseEntity getEntity(Id baseId) {
		return getEntity(baseId, null);
	}

	public CostCenterBaseEntity getEntity(Id baseId, AppDate targetDate) {
		CostCenterRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};
		filter.targetDate = targetDate;

		List<CostCenterBaseEntity> entities = searchEntityWithHistory(filter, NOT_FOR_UPDATE, HISTORY_SORT_ORDER.VALID_ASC);
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	public List<CostCenterBaseEntity> getEntityList(List<Id> baseIds, AppDate targetDate) {
		CostCenterRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = baseIds;
		filter.targetDate = targetDate;
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HISTORY_SORT_ORDER.VALID_ASC);
	}

	public CostCenterBaseEntity getEntityByHistoryId(Id historyId) {
		CostCenterRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new List<Id>{historyId};
		List<CostCenterBaseEntity> entities = searchEntityWithHistory(filter, NOT_FOR_UPDATE, HISTORY_SORT_ORDER.VALID_ASC);
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	public CostCenterBaseEntity getBaseEntity(Id baseId) {
		return getBaseEntity(baseId, NOT_FOR_UPDATE);
	}

	public CostCenterBaseEntity getBaseEntity(Id baseId, Boolean forUpdate) {
		List<CostCenterBaseEntity> bases = getBaseEntityList(new List<Id>{baseId}, forUpdate);
		if (bases.isEmpty()) {
			return null;
		}
		return bases[0];
	}

	public List<CostCenterBaseEntity> getBaseEntityList(List<Id> baseIds, Boolean forUpdate) {
		CostCenterRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.baseIds = new List<Id>(baseIds);
		return searchBaseEntity(filter, forUpdate);
	}

	/*
	 * Search Cost Center History excluding the logically removed histories.
	 */
	public CostCenterHistoryEntity getHistoryEntity(Id historyId) {
		return getHistoryEntity(historyId, false);
	}

	/*
	 * Search Cost Center History.
	 * @param historyId
	 *      Id of the History Entity to search for
	 * @param includeLogicallyRemoved
	 *      If set to True, then the search will include those logically removed histories.
	 */
	public CostCenterHistoryEntity getHistoryEntity(Id historyId, Boolean includeLogicallyRemoved) {
		CostCenterRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new List<Id>{historyId};
		if (includeLogicallyRemoved == true) {
			filter.includeRemoved = true;
		}
		List<CostCenterHistoryEntity> entities = searchHistoryEntity(filter, NOT_FOR_UPDATE, HISTORY_SORT_ORDER.VALID_ASC);
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	public List<CostCenterHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		CostCenterRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = historyIds;
		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HISTORY_SORT_ORDER.VALID_ASC);
	}

	public List<CostCenterHistoryEntity> getHistoryEntityByBaseId(Id baseId, HISTORY_SORT_ORDER sortOrder) {
		CostCenterRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};
		return searchHistoryEntity(filter, NOT_FOR_UPDATE, sortOrder);
	}

	public List<CostCenterBaseEntity> searchEntity(SearchFilter filter) {
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HISTORY_SORT_ORDER.VALID_ASC);
	}


	public class SearchFilter {
		public List<Id> baseIds;
		public List<Id> historyIds;
		public List<Id> companyIds;
		public List<Id> parentIds;
		public AppDate targetDate;
		public List<AppDate> targetPeriod;
		public List<String> codes;
		public List<String> linkageCodes;
		public String nameL;
		public Boolean includeRemoved;
		public String query;
	}

	public List<CostCenterBaseEntity> searchEntityWithHistory(SearchFilter filter, Boolean forUpdate, HISTORY_SORT_ORDER sortOrder) {
		List<CostCenterHistoryEntity> histories = searchHistoryEntity(filter, forUpdate, sortOrder);

		Set<Id> targetBaseIds = new Set<Id>();
		for (CostCenterHistoryEntity history : histories) {
			targetBaseIds.add(history.baseId);
		}

		SearchBaseFilter baseFilter = new SearchBaseFilter();
		baseFilter.baseIds = new List<Id>(targetBaseIds);
		List<CostCenterBaseEntity> bases = searchBaseEntity(baseFilter, forUpdate);

		Map<Id, CostCenterBaseEntity> baseMap = new Map<Id, CostCenterBaseEntity>();
		for (CostCenterBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		for (CostCenterHistoryEntity history : histories) {
			CostCenterBaseEntity base = baseMap.get(history.baseId);
			base.addHistory(history);
		}
		return bases;
	}

	public class SearchBaseFilter {
		public List<Id> baseIds;
	}

	private List<CostCenterBaseEntity> searchBaseEntity(SearchBaseFilter filter, Boolean forUpdate) {
		List<String> baseWhereList = new List<String>();
		List<Id> pIds;
		if (filter.baseIds != null) {
			pIds = filter.baseIds;
			baseWhereList.add('Id IN :pIds');
		}

		List<String> selectFiledList = new List<String>();
		selectFiledList.addAll(GET_BASE_FIELD_NAME_LIST);
		selectFiledList.addAll(GET_BASE_COMPANY_FIELD_NAME_LIST);

		String soql = String.format('SELECT {0} FROM {1} {2} ORDER BY {3}',
			new String[] {
				String.join(selectFiledList, ','),
				ComCostCenterBase__c.SObjectType.getDescribe().getName(),
				buildWhereString(baseWhereList),
				getFieldName(ComCostCenterBase__c.Code__c)
			});
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		List<ComCostCenterBase__c> sObjs = (List<ComCostCenterBase__c>) Database.query(soql);

		List<CostCenterBaseEntity> entities = new List<CostCenterBaseEntity>();
		for (ComCostCenterBase__c sObj : sObjs) {
			entities.add(createBaseEntity(sObj));
		}
		return entities;
	}


	/*
	 * Search the existing records base on the filter.
	 * If the targetDate is specified in the filter, it will be used against both ValidFrom and ValidTo Dates.
	 * @param filter
	 * 		Used for building the "WHERE" condition.
	 * @param forUpdate
	 * 		Specifies whether the search is to be used in Update or not.
	 * @param sortOrder
	 * 		Controls how the returned record should be sorted
	 */
	public List<CostCenterHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, HISTORY_SORT_ORDER sortOrder) {
		return searchHistoryEntity(filter, forUpdate, true, sortOrder);
	}

	/*
	 * Search the existing records base on the filter.
	 * @param filter
	 * 		Used for building the "WHERE" condition
	 * @param forUpdate
	 * 		Specifies whether the search is to be used in Update or not.
	 * @param excludeFutureRecords
	 * 		If the targetDate is specified in the filter, this controls whether targetDate will be used against ValidFrom date.
	 * @param sortOrder
	 * 		Controls how the returned record should be sorted
	 */
	public List<CostCenterHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, Boolean excludeFutureRecords, HISTORY_SORT_ORDER sortOrder) {
		List<String> selectFieldList = new List<String>();
		selectFieldList.addAll(GET_HISTORY_FIELD_NAME_LIST);
		selectFieldList.add(getFieldName(HISTORY_BASE_R, ComCostCenterBase__c.Code__c));

		List<String> whereList = new List<String>();

		Date pTargetDate;
		if (filter.targetDate != null) {
			pTargetDate = filter.targetDate.getDate();
			if (excludeFutureRecords) {
				whereList.add(getFieldName(ComCostCenterHistory__c.ValidFrom__c) + ' <= :pTargetDate');
			}
			whereList.add(getFieldName(ComCostCenterHistory__c.ValidTo__c) + ' > :pTargetDate');
		}

		// Check record existing in the period
		Date pTargetPeriodFrom;
		Date pTargetPeriodTo;
		if (filter.targetPeriod != null) {
			pTargetPeriodFrom = filter.targetPeriod.get(0).getDate();
			pTargetPeriodTo = filter.targetPeriod.get(1).getDate();
			whereList.add(getFieldName(ComCostCenterHistory__c.ValidFrom__c) + ' <= :pTargetPeriodTo');
			whereList.add(getFieldName(ComCostCenterHistory__c.ValidTo__c) + ' > :pTargetPeriodFrom');
		}

		List<Id> pHistoryIds;
		if (filter.historyIds != null) {
			pHistoryIds = filter.historyIds;
			whereList.add(getFieldName(ComCostCenterHistory__c.Id) + ' IN :pHistoryIds');
		}

		List<Id> pParentIds;
		if (filter.parentIds != null) {
			pParentIds = filter.parentIds;
			whereList.add(getFieldName(ComCostCenterHistory__c.ParentBaseId__c) + ' IN :pParentIds');
		}

		String pNameL;
		if (String.isNotBlank(filter.nameL)) {
			pNameL = '%' + filter.nameL + '%';
			String s = getFieldName(ComCostCenterHistory__c.Name_L0__c) + ' LIKE :pNameL OR '
				+ getFieldName(ComCostCenterHistory__c.Name_L1__c) + ' LIKE :pNameL OR '
				+ getFieldName(ComCostCenterHistory__c.Name_L2__c) + ' LIKE :pNameL';

			whereList.add(s);
		}

		if (filter.includeRemoved != true) {
			whereList.add(getFieldName(ComCostCenterHistory__c.Removed__c) + ' = false');
		}

		List<Id> pBaseIds;
		if (filter.baseIds != null) {
			pBaseIds = filter.baseIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ComCostCenterBase__c.Id) + ' IN :pBaseIds');
		}

		List<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ComCostCenterBase__c.CompanyId__c) + ' IN :pCompanyIds');
		}

		List<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(HISTORY_BASE_R, ComCostCenterBase__c.Code__c) + ' IN :pCodes');
		}

		List<String> pLinkageCodes;
		if (filter.linkageCodes != null) {
			pLinkageCodes = filter.linkageCodes;
			whereList.add(getFieldName(ComCostCenterHistory__c.LinkageCode__c) + ' IN :pLinkageCodes');
		}

		String pQuery;
		if (String.isNotEmpty(filter.query)) {
			pQuery = '%' + escapeSpecialCharacters(filter.query) + '%';
			String s = getFieldName(HISTORY_BASE_R, ComCostCenterBase__c.Code__c) + ' LIKE :pQuery OR '
					+ getFieldName(ComCostCenterHistory__c.Name_L0__c) + ' LIKE :pQuery OR '
					+ getFieldName(ComCostCenterHistory__c.Name_L1__c) + ' LIKE :pQuery OR '
					+ getFieldName(ComCostCenterHistory__c.Name_L2__c) + ' LIKE :pQuery ';

			whereList.add(s);
		}

		String orderBy = ' ORDER BY ' + getFieldName(HISTORY_BASE_R, ComCostCenterBase__c.Code__c);

		if (sortOrder == HISTORY_SORT_ORDER.VALID_ASC) {
			orderBy += ', ' + getFieldName(ComCostCenterHistory__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == HISTORY_SORT_ORDER.VALID_DESC) {
			orderBy += ', ' + getFieldName(ComCostCenterHistory__c.ValidFrom__c) + ' DESC NULLS LAST';
		}

		String soql = 'SELECT ' + String.join(selectFieldList, ',')
			+ ' FROM ComCostCenterHistory__c' +
			+ buildWhereString(whereList)
			+ orderBy;

		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		List<ComCostCenterHistory__c> sObjs = (List<ComCostCenterHistory__c>) Database.query(soql);
		List<CostCenterHistoryEntity> entities = new List<CostCenterHistoryEntity>();
		for (ComCostCenterHistory__c sObj : sObjs) {
			entities.add(createHistoryEntity(sObj));
		}
		return entities;
	}

	@TestVisible
	private CostCenterBaseEntity createBaseEntity(ComCostCenterBase__c baseObj) {
		return new CostCenterBaseEntity(baseObj);
	}

	private CostCenterHistoryEntity createHistoryEntity(ComCostCenterHistory__c historyObj) {
		return new CostCenterHistoryEntity(historyObj);
	}

	protected override List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone) {
		List<ComCostCenterBase__c> costCenterList = new List<ComCostCenterBase__c>();
		for (SObject obj : baseObjList) {
			costCenterList.add((ComCostCenterBase__c) obj);
		}
		return AppDatabase.doUpsert(costCenterList, allOrNone);
	}

	protected override List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone) {
		List<ComCostCenterHistory__c> costCenterList = new List<ComCostCenterHistory__c>();
		for (SObject obj : historyObjList) {
			costCenterList.add((ComCostCenterHistory__c) obj);
		}
		return AppDatabase.doUpsert(costCenterList, allOrNone);
	}

	protected override SObject createBaseObj(ParentChildBaseEntity baseEntity) {
		return ((CostCenterBaseEntity)baseEntity).createSObject();
	}

	protected override SObject createHistoryObj(ParentChildHistoryEntity historyEntity) {
		return ((CostCenterHistoryEntity)historyEntity).createSObject();
	}

	/*
	 * Return an escaped version of the input.
	 * @param unEscapedString String to check
	 * @return escaped version of the string
	 */
	private static String escapeSpecialCharacters(String unEscapedString) {
		String[] chars = unEscapedString.split('');
		for (Integer i = 0; i < chars.size(); i++) {
			if (SPECIAL_CHARACTERS_PATTERN.matcher(chars[i]).matches()) {
				chars[i] = '\\' + chars[i];
			}
		}
		return String.join(chars, '');
	}
}