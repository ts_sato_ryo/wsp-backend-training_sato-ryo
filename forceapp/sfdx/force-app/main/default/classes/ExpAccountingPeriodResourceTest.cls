/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for ExpAccountingPeriodRepository
 */
@isTest
private class ExpAccountingPeriodResourceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);

	/**
	 * Create a new Accounting Period record(Positive case)
	 */
	@isTest
	static void createApiTest() {

		ExpAccountingPeriodResource.UpsertParam param = new ExpAccountingPeriodResource.UpsertParam();
		setTestParam(param);

		Test.startTest();

		ExpAccountingPeriodResource.createApi api = new ExpAccountingPeriodResource.createApi();
		ExpAccountingPeriodResource.SaveResult result = (ExpAccountingPeriodResource.SaveResult)api.execute(param);

		Test.stopTest();

		List<ExpAccountingPeriod__c> records =
			[SELECT
				Id,
				Code__c,
				CompanyId__c,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				ValidFrom__c,
				ValidTo__c,
				RecordingDate__c,
				Active__c
			FROM ExpAccountingPeriod__c
			WHERE Id =: result.id];

		//Confirm new record was created
		System.assertEquals(1, records.size());

		System.assertEquals(param.code, records[0].Code__c);
		System.assertEquals(param.companyId, records[0].CompanyId__c);
		System.assertEquals(param.name_L0, records[0].Name_L0__c);
		System.assertEquals(param.name_L1, records[0].Name_L1__c);
		System.assertEquals(param.name_L2, records[0].Name_L2__c);
		System.assertEquals(param.validDateFrom, records[0].ValidFrom__c);
		System.assertEquals(param.validDateTo, records[0].ValidTo__c);
		System.assertEquals(param.recordingDate, records[0].RecordingDate__c);
	}

	/**
   * Create a new Accounting Period record(Negative case: Mandatory params are not set)
   */
	@isTest
	static void createApiParamErrTest() {

		ExpAccountingPeriodResource.UpsertParam param = new ExpAccountingPeriodResource.UpsertParam();
		setTestParam(param);

		ExpAccountingPeriodResource.CreateApi api = new ExpAccountingPeriodResource.CreateApi();
		App.ParameterException ex1;
		App.ParameterException ex2;
		App.ParameterException ex3;
		App.ParameterException ex4;
		App.ParameterException ex5;
		App.ParameterException ex6;

		Test.startTest();

		// Set code blank
		setTestParam(param);
		param.code = '';
		try {
			// Parameter error should occur
			api.execute(param);
		} catch (App.ParameterException e1) {
			ex1 = e1;
		}

		// Set name blank
		setTestParam(param);
		param.name_L0 = '';
		try {
			// Parameter error should occur
			api.execute(param);
		} catch (App.ParameterException e2) {
			ex2 = e2;
		}

		// Set company id blank
		setTestParam(param);
		param.companyId = '';
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e3) {
			ex3 = e3;
		}

		// Set start date id blank
		setTestParam(param);
		param.validDateFrom = null;
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e4) {
			ex4 = e4;
		}

		// Set end date id blank
		setTestParam(param);
		param.validDateTo = null;
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e5) {
			ex5 = e5;
		}

		// Set recording date id blank
		setTestParam(param);
		param.recordingDate = null;
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e6) {
			ex6 = e6;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex1.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Com_Lbl_Code}), ex1.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex2.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_Name}), ex2.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex3.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'Company ID'}), ex3.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex4.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_StartDate}), ex4.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex4.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_EndDate}), ex5.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex4.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_RecordingDate}), ex6.getMessage());
	}

	/**
	* Update a Accounting Period record(Positive case)
	*/
	@isTest
	static void updateApiTest() {

		// Create a Accounting Period record first
		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		ExpAccountingPeriodResource.UpsertParam param = new ExpAccountingPeriodResource.UpsertParam();
		setTestParam(param);
		param.id = testRecord.Id;

		Test.startTest();

		ExpAccountingPeriodResource.UpdateApi api = new ExpAccountingPeriodResource.UpdateApi();
		api.execute(param);

		Test.stopTest();

		List<ExpAccountingPeriod__c> records =
		[SELECT
			Id,
			Code__c,
			CompanyId__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			ValidFrom__c,
			ValidTo__c,
			RecordingDate__c,
			Active__c
			FROM ExpAccountingPeriod__c
			WHERE Id =: testRecord.id];

		//Confirm the record was updated
		System.assertEquals(1, records.size());

		System.assertEquals(param.code, records[0].Code__c);
		System.assertEquals(param.companyId, records[0].CompanyId__c);
		System.assertEquals(param.name_L0, records[0].Name_L0__c);
		System.assertEquals(param.name_L1, records[0].Name_L1__c);
		System.assertEquals(param.name_L2, records[0].Name_L2__c);
		System.assertEquals(param.validDateFrom, records[0].ValidFrom__c);
		System.assertEquals(param.validDateTo, records[0].ValidTo__c);
		System.assertEquals(param.recordingDate, records[0].RecordingDate__c);
	}

	/**
   * Update a Accounting Period record(Negative case: Mandatory param is not set)
   */
	@isTest
	static void updateApiParamErrTest() {

		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		ExpAccountingPeriodResource.UpsertParam param = new ExpAccountingPeriodResource.UpsertParam();
		setTestParam(param);
		// Do not set ID

		Test.startTest();

		ExpAccountingPeriodResource.UpdateApi api = new ExpAccountingPeriodResource.UpdateApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), ex.getMessage());
	}

	/**
   * Delete a AccountingPeriod record(Positive case)
   */
	@isTest
	static void deleteApiTest() {

		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		ExpAccountingPeriodResource.DeleteParam param = new ExpAccountingPeriodResource.DeleteParam();
		param.id = testRecord.Id;

		Test.startTest();

		ExpAccountingPeriodResource.DeleteApi api = new ExpAccountingPeriodResource.DeleteApi();
		api.execute(param);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ExpAccountingPeriod__c WHERE Id =: param.id]);
	}

	/**
	 * Delete a AccountingPeriod record(Negative case)
	 */
	@isTest
	static void deleteApiParamErrTest() {

		ExpAccountingPeriodResource.DeleteParam param = new ExpAccountingPeriodResource.DeleteParam();

		Test.startTest();

		ExpAccountingPeriodResource.DeleteApi api = new ExpAccountingPeriodResource.DeleteApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), ex.getMessage());
	}

	/**
	 * Search a AccountingPeriod record by ID(Positive case)
	 */
	@isTest
	static void searchApiTest() {

		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		Test.startTest();

		ExpAccountingPeriodResource.SearchParam param = new ExpAccountingPeriodResource.SearchParam();
		param.companyId = testRecord.CompanyId__c;
		param.isNotMaster = true;

		ExpAccountingPeriodResource.SearchApi api = new ExpAccountingPeriodResource.SearchApi();
		ExpAccountingPeriodResource.SearchResult res = (ExpAccountingPeriodResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());

		ExpAccountingPeriodResource.ExpAccountingPeriodRecord result = res.records[0];
		System.assertEquals(testRecord.Id, result.id);
		System.assertEquals(testRecord.Code__c, result.code);
		System.assertEquals(testRecord.CompanyId__c, result.companyId);
		System.assertEquals(testRecord.Name_L0__c, result.name_L0);
		System.assertEquals(testRecord.Name_L1__c, result.name_L1);
		System.assertEquals(testRecord.Name_L2__c, result.name_L2);
		System.assertEquals(testRecord.ValidFrom__c, result.validDateFrom);
		System.assertEquals(testRecord.ValidTo__c.addDays(-1), result.validDateTo);
		System.assertEquals(testRecord.RecordingDate__c, result.recordingDate);
		System.assertEquals(testRecord.Active__c, result.active);
	}

	/**
	 * Search a AccountingPeriod record(Positive case: test for L1 name(multi lang))
	 */
	@isTest
	static void searchApiGetNameL1Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		Test.startTest();

		ExpAccountingPeriodResource.SearchParam param = new ExpAccountingPeriodResource.SearchParam();
		param.companyId = testRecord.CompanyId__c;
		param.isNotMaster = true;

		ExpAccountingPeriodResource.SearchApi api = new ExpAccountingPeriodResource.SearchApi();
		ExpAccountingPeriodResource.SearchResult res = (ExpAccountingPeriodResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(testRecord.Name_L1__c, res.records[0].name);
	}

	/**
	 * Search a AccountingPeriod record(Positive case: test for L2 name(multi lang))
	 */
	@isTest
	static void searchApiGetNameL2Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		Test.startTest();

		ExpAccountingPeriodResource.SearchParam param = new ExpAccountingPeriodResource.SearchParam();
		param.companyId = testRecord.CompanyId__c;
		param.isNotMaster = true;

		ExpAccountingPeriodResource.SearchApi api = new ExpAccountingPeriodResource.SearchApi();
		ExpAccountingPeriodResource.SearchResult res = (ExpAccountingPeriodResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(testRecord.Name_L2__c, res.records[0].name);
	}

	/**
	 * Set test parameter tp request parameter
	 * @param param Target object to set data
	 */
	static void setTestParam(ExpAccountingPeriodResource.UpsertParam param) {
		param.code = 'TEST';
		param.companyId = companyObj.Id;
		param.name_L0 = 'Period L0';
		param.name_L1 = 'Period L1';
		param.name_L2 = 'Period L2';
		param.validDateFrom = Date.today();
		param.validDateTo = Date.today().addMonths(1);
		param.recordingDate = param.validDateTo.addDays(-1);
		param.active = true;
	}

}
