/**
 * @group 工数
 *
 * 工数内訳スナップショットのサービスクラス
 * 
 * 会社ごとのマスタ情報を内部でキャッシュしています。
 * 同一の会社で複数回実行する場合はインスタンスを再利用できます。
 * TODO 会社単位でマスタを取得しているため、社員の会社が変更になった場合は変更前の工数内訳のスナップショットは作成できない（階層情報の作成時にエラーが発生する）
 */
public with sharing class TimeRecordItemSnapshotService {

	// スナップショットリポジトリ
	private TimeRecordItemSnapshotRepository repository = new TimeRecordItemSnapshotRepository();
	
	// 工数ジョブのキャッシュ
	private Map<Id, JobEntity> timeTrackJobs;
	
	// 作業分類のキャッシュ
	private Map<Id, WorkCategoryEntity> workCategories;

	/** 会社ID */
	public Id companyId {
		get {
			return companyId;
		}
		private set;
	}

	/**
	 * コンストラクタ
	 * インスタンス生成時に、会社に紐づくジョブと作業分類を検索する
	 * @param companyId 会社ID
	 */
	public TimeRecordItemSnapshotService(Id companyId) {
		this.companyId = companyId;

		// 工数のジョブを取得する
		JobRepository.SearchFilter jobFilter = new JobRepository.SearchFilter();
		jobFilter.companyIds = new Set<Id>{companyId};
		jobFilter.isSelectableTimeTrack = true;
		timeTrackJobs = new Map<Id, JobEntity>();
		for (JobEntity job : new JobRepository().searchEntity(jobFilter)) {
			timeTrackJobs.put(job.id, job);
		}

		// 作業分類を取得する
		workCategories = new Map<Id, WorkCategoryEntity>();
		WorkCategoryRepository.SearchFilter workCategoryFilter = new WorkCategoryRepository.SearchFilter();
		workCategoryFilter.companyIds = new Set<Id>{companyId};
		for (WorkCategoryEntity workCategory : new WorkCategoryRepository().searchEntity(workCategoryFilter)) {
			workCategories.put(workCategory.id, workCategory);
		}
	}

	/**
	 * 工数明細に紐づく全ての工数内訳に対して、スナップショットを保存する。
	 * 既存のスナップショットは削除し、新たにレコードを登録する。
	 * @param record 工数明細レコード（工数確定していない、かつ 工数内訳を内部で保持しているものに限る）
	 * @return 保存したスナップショットのリスト
	 */
	public List<TimeRecordItemSnapshotEntity> save(TimeRecordEntity record) {
		return save(new List<TimeRecordEntity>{record});
	}

	/**
	 * 工数明細に紐づく全ての工数内訳に対して、スナップショットを保存する。
	 * 既存のスナップショットは削除し、新たにレコードを登録する。
	 * 確定済みの明細はスナップショットの処理対象外とする。
	 * 
	 * @param record 工数明細レコードのリスト（工数内訳を内部で保持しているものに限る）
	 * @return 保存したスナップショットのリスト
	 */
	public List<TimeRecordItemSnapshotEntity> save(List<TimeRecordEntity> records) {
		// 確定済みの明細を処理対象外とする
		List<TimeRecordEntity> unlockedRecords = new List<TimeRecordEntity>(); 
		for (TimeRecordEntity record : records) {
			if (record.isSummaryLocked) {
				continue;
			}
			unlockedRecords.add(record);
		}

		// 既存のスナップショットが存在する場合は削除し、新たに登録する
		List<Id> oldSnapshotIds = new List<Id>();
		List<TimeRecordItemSnapshotEntity> newSnapshots = new List<TimeRecordItemSnapshotEntity>();
		for (TimeRecordEntity record : unlockedRecords) {
			// 削除対象
			oldSnapshotIds.add(record.id);
			// 登録対象
			for (TimeRecordItemEntity item : record.recordItemList) {
				newSnapshots.add(createSnapshotEntity(item));
			}
		}

		// 既存のスナップショットを削除する
		TimeRecordItemSnapshotRepository.SearchFilter filter = new TimeRecordItemSnapshotRepository.SearchFilter();
		filter.recordIds = oldSnapshotIds;
		List<TimeRecordItemSnapshotEntity> oldSnapshots = repository.searchEntityList(filter);
		repository.deleteEntityList(oldSnapshots);

		// スナップショットを登録する
		repository.saveEntityList(newSnapshots);
		return newSnapshots;
	}

	/**
	 * 工数内訳に対して、スナップショットを保存する。
	 * 工数サマリーが確定済みの場合は処理を行わない。
	 * @param recordItem 保存対象の工数サマリー
	 * @return 作成したスナップショット（DBに保存はしない）
	 */
	private TimeRecordItemSnapshotEntity createSnapshotEntity(TimeRecordItemEntity recordItem) {
		// ジョブの階層情報を取得する
		List<JobEntity> jobList = createJobTree(timeTrackJobs, recordItem.jobId);

		// エンティティを生成する
		TimeRecordItemSnapshotEntity entity = new TimeRecordItemSnapshotEntity();
		entity.timeRecordItemId = recordItem.Id;
		entity.name = recordItem.name; 
		entity.addJobList(jobList);
		
		// 作業分類を取得する
		if (recordItem.workCategoryId != null) {
			WorkCategoryEntity workCategory = workCategories.get(recordItem.workCategoryId);
			entity.setWorkCategory(workCategory);
		}
		return entity;
	}

	/**
	 * 階層構造を持つジョブのうち、最上位から指定したジョブまで階層順のリストを作成する
	 * @param jobs ジョブの一覧
	 * @param endJobId 取得対象のジョブId
	 * @return ジョブ階層のリスト
	 */
	@TestVisible
	private List<JobEntity> createJobTree(Map<Id, JobEntity> jobs, Id bottomJobId) {
		List<JobEntity> tree = new List<JobEntity>();
		createJobTree(jobs, tree, bottomJobId);
		return tree;
	}

	/**
	 * jobIdが最上位のジョブになるまで再帰的にリストに要素を追加する
	 * @param jobs ジョブの一覧
	 * @param tree 操作対象のリスト
	 * @param jobId 追加対象のジョブID
	 */
	private void createJobTree(Map<Id, JobEntity> jobs, List<JobEntity> tree, Id jobId) {
		JobEntity current = jobs.get(jobId);
		if (tree.isEmpty()) {
			tree.add(current); // 1件目で0番目にaddするとListExceptionが発生する
		} else {
			tree.add(0, current);
		}
		if (current.parentId == null) {
			return;
		}
		createJobTree(jobs, tree, current.parentId);
	}
}
