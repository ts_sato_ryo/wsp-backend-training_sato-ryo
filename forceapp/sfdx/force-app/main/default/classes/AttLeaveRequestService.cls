/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇申請のサービスクラス
 * 当クラスはAttAttDailyRequestServiceからのみ実行する事を前提としています。
 * Resourceクラスなどから直接実行しないで下さい。
 */
public with sharing class AttLeaveRequestService extends AttDailyRequestService.AbstractRequestProcessor {

	/** 申請可能な最大日数 */
	private static final Integer REQUEST_PERIOD_MAX = 31;

	/**
	 * 勤怠日次申請のリポジトリ
	 */
	private final AttDailyRequestRepository dailyRequestRepository = new AttDailyRequestRepository();
	/**
	 * 勤務体系サービス
	 */
	private AttWorkingTypeService workingTypeService = new AttWorkingTypeService();

	/**
	 * 申請・再申請パラメータ
	 */
	public class SubmitParam extends AttDailyRequestService.SubmitParam {
		/** 休暇コード */
		public String leaveCode;
		/** 休暇範囲 */
		public AttLeaveRange range;
		/** 開始日 */
		public AppDate startDate;
		/** 終了日 */
		public AppDate endDate;
		/** 開始時刻 */
		public AttTime startTime;
		/** 終了時刻 */
		public AttTime endTime;
		/** 備考 */
		public String remarks;
		/** 理由 */
		public String reason;
	}

	/**
	 * 休暇申請の申請/再申請を行う。
	 *
	 * @param param 申請情報
	 * @return 作成した申請情報
	 */
	public override AttDailyRequestEntity submit(AttDailyRequestService.SubmitParam submitParam) {
		// 再申請の場合は取下げを行う
		if (submitParam.isResubmittion()) {
			super.remove(dailyRequestRepository.getEntity(submitParam.requestId));
		}

		final Id originalRequestId = null; // 承認内容変更申請でないためnull
		return submit(submitParam, originalRequestId);

	}

	/**
	 * 休暇申請の申請を行う。
	 *
	 * @param param 申請情報
	 * @param originalRequestId 承認内容変更申請の場合、元の申請ID
	 * @return 作成した申請情報
	 */
	private AttDailyRequestEntity submit(AttDailyRequestService.SubmitParam submitParam, Id originalRequestId) {
		SubmitParam param = (SubmitParam)submitParam;

// TODO AttDailyRequestServeceのメソッドを呼び出している処理は、今後修正される予定
		AttDailyRequestService requestService = new AttDailyRequestService();

		// 社員を取得する
		EmployeeBaseEntity employee;
		try {
			if (param.endDate == null) {
				employee = new EmployeeService().getEmployee(param.empId, param.startDate);
			} else {
				employee = new EmployeeService().getEmployeeByRange(
						param.empId, new AppDateRange(param.startDate, param.endDate));
			}
		} catch (App.RecordNotFoundException e) {
			// 申請期間に社員が無効である期間が含まれている場合は、申請用のエラーメッセージに変更する
			if (e.getErrorCode() == App.ERR_CODE_INVALID_EMP) {
				e.setMessage(ComMessage.msg().Att_Err_InvalidLeaveRequestPeriodEmployee);
			}
			throw e;
		}

		// 休暇を取得する
		AttLeaveEntity leave = getLeaveByCode(param.leaveCode, employee.companyId, param.startDate, param.endDate);
		System.debug(LoggingLevel.DEBUG, 'submit1 QueryCount:' + Limits.getQueries());

		// 「理由を求める」が有効の場合は、理由が未入力だとエラー
		if (leave.requireReason && String.isBlank(param.reason)) {
			// メッセージ：この休暇種類での申請は、理由の入力が必要です。
			throw new App.ParameterException(ComMessage.msg().Att_Err_LeaveReasonRequired);
		}

		// 申請対象となる勤怠明細レコードとサマリーを取得する
		AttDailyRequestService.RecordsWithSummary recordsWithSummary =
				getRecordEntityList(param.empId, param.startDate, param.endDate);
		Map<Id, AttSummaryEntity> summaryMap = recordsWithSummary.summaryMap;
		List<AttRecordEntity> records = recordsWithSummary.records;

		// 申請対象の勤怠明細に適用済みの勤務パターンを全件取得する
		Set<Id> patternIdSet = new Set<Id>();
		for (AttRecordEntity record : records) {
			patternIdSet.add(record.outPatternId);
			if (record.reqRequestingPatternPatternId != null) {
				patternIdSet.add(record.reqRequestingPatternPatternId);
			}
		}
		Map<Id, AttPatternEntity> patternMap = getPatternMapById(patternIdSet);

		// 申請可能な勤務体系であることをチェック
		Map<Id, AttWorkingTypeBaseEntity> workingTypeHistoryIdMap = new Map<Id, AttWorkingTypeBaseEntity>(); // キー：勤務体系履歴ID
		for (AttSummaryEntity summary : summaryMap.values()) {
			AttWorkingTypeBaseEntity workingType = this.workingTypeService.getWorkingType(summary.workingTypeBaseId);
			workingTypeHistoryIdMap.put(summary.workingTypeId, workingType);
			validateWorkingTypeLeaveRequest(workingType, param.range);
		}
		System.debug(LoggingLevel.DEBUG, 'submit2 QueryCount:' + Limits.getQueries());
		// 各日の勤務体系、勤務パターンで許可されている休暇、休暇範囲であることを確認する
		Set<Id> isCheckedWokingTypeSet = new Set<Id>(); // チェック済みの勤務体系ID
		Set<Id> isCheckedPatternSet = new Set<Id>(); // チェック済みの勤務パターンID

		for (AttRecordEntity record : records) {
			AttSummaryEntity summary = summaryMap.get(record.summaryId);
			Id patternId = null;
			if (record.outPatternId != null) {
				patternId = record.outPatternId;
			} else if (record.reqRequestingPatternPatternId != null) {
				patternId = record.reqRequestingPatternPatternId;
			}
			AttPatternEntity pattern = patternMap.get(patternId);

			// 対象の勤務体系IDと勤務パターンIDが既にチェック済みの場合はスキップする
			if (! isCheckedWokingTypeSet.contains(summary.workingTypeId) || ! isCheckedPatternSet.contains(patternId)) {
				isCheckedWokingTypeSet.add(summary.workingTypeId);
				isCheckedPatternSet.add(patternId);

				AttWorkingTypeBaseEntity workingType = workingTypeHistoryIdMap.get(summary.workingTypeId);
				AttWorkingTypeHistoryEntity workingTypeHistory = workingType.getHistoryByDate(record.rcdDate);
				List<AttDailyRequestService.AvailableLeave> avLeaves = requestService.getLeaveList(workingType, workingTypeHistory, pattern);
				if (! canSubmitLeaveRequest(avLeaves, param.leaveCode, param.range)) {
					// エラーメッセージ：休暇種類または範囲が、申請期間内の勤務体系で許可されていません。
					throw new App.IllegalStateException(App.ERR_CODE_NOT_PERMITTID_REQUEST,
							ComMessage.msg().Att_Err_NotAllowedLeaveTypeOrRange);
				}
			}
		}
		System.debug(LoggingLevel.DEBUG, 'submit3 QueryCount:' + Limits.getQueries());
		// 申請エンティティを作成する
		AppLanguage companyLang = getCompanyLanguage(employee.companyId);
		AttDailyRequestEntity request = createLeaveRequest(
				param, originalRequestId, recordsWithSummary, employee, leave, companyLang, patternMap);
		validateAttDailiyRequestEntity(request);
		System.debug(LoggingLevel.DEBUG, 'submit4 QueryCount:' + Limits.getQueries());

		// 申請を保存する
		Repository.SaveResult saveRes = new AttDailyRequestRepository().saveEntity(request);
		request.setId(saveRes.details[0].id);
		Id requestId = saveRes.details[0].id;

		// 勤怠明細エンティティの申請IDを更新する
		Set<AppDate> excludingDateList = new Set<AppDate>(request.excludingDateList);
		final Integer leaveIndex1 = 1;
		final Integer leaveIndex2 = 2;
		for (AttRecordEntity record : records) {
			// 申請で除外日に設定されている場合は設定しない
			if (excludingDateList.contains(record.rcdDate)) {
				continue;
			}

			Integer leaveIndex;
			// 申請IDの設定項目を決定
			// 承認内容変更申請で元の申請が勤怠明細に存在する場合、元の申請と同じIndexに申請する
			if (originalRequestId != null) {
				if (record.reqLeave1RequestId == originalRequestId) {
					leaveIndex = leaveIndex1;
				} else if (record.reqLeave2RequestId == originalRequestId) {
					leaveIndex = leaveIndex2;
				}
			}
			// 新規申請の場合
			// 承認内容変更申請でも期間を延長した日の明細については新規申請と同様に設定する
			if (leaveIndex == null) {
				if (record.reqRequestingLeave1RequestId == null && record.reqLeave1RequestId == null) {
					leaveIndex = leaveIndex1;
				} else if (record.reqRequestingLeave2RequestId == null && record.reqLeave2RequestId == null) {
					leaveIndex = leaveIndex2;
				} else {
					// 休暇申請1も2も設定できないためエラー
					// メッセージ：既に休暇申請が存在するため、申請できません。
					throw new App.IllegalStateException(ComMessage.msg().Att_Err_LeaveRequestsAlreadyExist);
				}
			}
			if (leaveIndex == null) {
				// 予期せぬエラー(バグ)
				throw new App.IllegalStateException('勤怠明細の設定先のleaveIndexが不明です');
			}

			// 申請可能であることを検証
			validateSubmitLeaveRequest(record, request, leaveIndex);

			// 勤怠明細に申請ID設定
			if (leaveIndex == leaveIndex1) {
				record.reqRequestingLeave1RequestId = requestId;
			} else if (leaveIndex == leaveIndex2) {
				record.reqRequestingLeave2RequestId = requestId;
			}
		}
		new AttSummaryRepository().saveRecordEntityList(records);
		System.debug(LoggingLevel.DEBUG, 'submit5 QueryCount:' + Limits.getQueries());

		// 申請時も休暇サマリの作成を行います(サマリ作成後休暇利用変更の場合もあり)
		AttManagedLeaveService managedLeaveService = new AttManagedLeaveService();
		for (AttSummaryEntity attSummary : summaryMap.values()) {
			managedLeaveService.initLeaveSummary(attSummary, new List<AttLeaveEntity>{leave});
		}
		System.debug(LoggingLevel.DEBUG, 'submit6 QueryCount:' + Limits.getQueries());
		// 有休残日数チェック
		managedLeaveService.checkLeaveRequestDaysTaken(param.empId, request.leaveId);

		// 申請する(申請コメントなし)
		submitProcess(request, null);

		return request;
	}

	/**
	 * 承認内容変更申請・再申請パラメータ
	 */
	public class ReapplyParam extends AttDailyRequestService.ReapplyParam {
		/** 開始日 */
		public AppDate startDate;
		/** 終了日 */
		public AppDate endDate;
		/** 備考 */
		public String remarks;
		/** 理由 */
		public String reason;
	}

	/**
	 * 休日出勤申請の承認内容変更申請/再申請を行う。
	 *
	 * @param param 申請情報
	 * @return 作成した申請情報
	 */
	public override AttDailyRequestEntity reapply(AttDailyRequestService.ReapplyParam reapplyParam) {
		ReapplyParam param = (ReapplyParam)reapplyParam;

		// 再申請の場合は取下げを行う
		if (param.isResubmittion()) {
			super.remove(dailyRequestRepository.getEntity(param.requestId));
		}

		// 変更対象の申請を取得
		AttDailyRequestEntity originalRequest = dailyRequestRepository.getEntity(param.originalRequestId);
		if (originalRequest == null) {
			String msg = ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Att_Lbl_LeaveRequest});
			throw new App.RecordNotFoundException(msg);
		}

		// 承認内容元申請の状態をチェック
		if (! originalRequest.canReapply()) {
			throw new App.IllegalStateException(App.ERR_CODE_CANNOT_REAPPLY, ComMessage.msg().Att_Err_CannotProcessRequest);
		}

		// 申請内容に変更がない場合はエラーとする
		if (! isRequestChanged(originalRequest, param)) {
			App.ParameterException e = new App.ParameterException();
			e.setCode(App.ERR_CODE_ATT_INVALID_REQUEST_LEAVE);
			e.setMessage(ComMessage.msg().Att_Err_NoChangedRequest);
			throw e;
		}

		// 現在は全日休のみ変更可能であるため、元の申請が全日休以外の場合はエラーとする
		if (originalRequest.leaveRange != AttLeaveRange.RANGE_DAY) {
			throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_REQUEST_LEAVE, ComMessage.msg().Att_Slt_InvalidLeaveRangeReapply);
		}

		// 承認内容変更申請の申請可能なことをチェックする
		validateReapplyLeaveRequest(originalRequest, param);

		// 既存の申請のステータスを更新する
		originalRequest.status = AppRequestStatus.REAPPLYING;
		dailyRequestRepository.saveEntity(originalRequest);

		// 変更した内容で休暇を申請する
		// 現在は全日休のみの対応のためstartTime, endTimeは設定しない
		AttLeaveRequestService.SubmitParam submitParam = new AttLeaveRequestService.SubmitParam();
		submitParam.leaveCode = originalRequest.leaveCode;
		submitParam.range = originalRequest.leaveRange;
		submitParam.empId = originalRequest.employeeBaseId;
		submitParam.requestId = param.requestId;
		submitParam.startDate = param.startDate;
		submitParam.endDate = param.endDate;
		submitParam.remarks = param.remarks;
		submitParam.reason = param.reason;
		return submit(submitParam, param.originalRequestId);
	}

	/**
	 * 承認内容変更申請をする場合、申請内容に変更があるか判定する
	 * @param originalRequest 変更前の元の承認済みの休暇申請
	 * @param param 承認内容変更申請の申請パラメータ
	 * @return 変更がある場合はtrue, そうでない場合はfalse
	 */
	@TestVisible
	private Boolean isRequestChanged(AttDailyRequestEntity originalRequest, ReapplyParam param) {

		// 開始日
		if (originalRequest.startDate != param.startDate) {
			return true;
		}

		// 終了日
		if (originalRequest.endDate != param.endDate) {
			return true;
		}

		// 理由または備考（休暇マスタの理由必須オプションに応じて理由または備考の変更をチェックする）
		String originalReasonOrRemarks;
		String paramReasonOrRemarks;
		if (originalRequest.requireReason) {
			originalReasonOrRemarks = originalRequest.reason == null ? '' : originalRequest.reason;
			paramReasonOrRemarks = param.reason == null ? '' : param.reason;
		} else {
			originalReasonOrRemarks = originalRequest.remarks == null ? '' : originalRequest.remarks;
			paramReasonOrRemarks =  param.remarks == null ? '' : param.remarks;
		}

		if (!originalReasonOrRemarks.equals(paramReasonOrRemarks)) {
			return true;
		}

		return false;
	}


	/**
	 * 承認取消処理を行う
	 * @param request 対象の申請
	 * @throws App.IllegalStateException 申請ステータスが「承認済み」ではない場合
	 */
	public override void cancelApproval(AttDailyRequestEntity request) {
		super.cancelApproval(request);

		// 有休消費データの削除を行う
		EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserId(request.ownerId, request.startDate);
		new AttManagedLeaveService().deleteTakenByRequest(employee.id, request);
	}

	/**
	 * 休暇申請可能かどうかを確認する。
	 * @param record 勤怠明細(申請前の状態)
	 * @param request 日次申請(申請情報設定済み)
	 * @throws App.IllegalStateException 申請不可である場合
	 */
	private void validateSubmitLeaveRequest(AttRecordEntity record, AttDailyRequestEntity request, Integer index) {
		AttValidator.DailyLeaveRequestSubmitValidator validator =
				new AttValidator.DailyLeaveRequestSubmitValidator(record, request, index);
		Validator.Result result = validator.validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}
	}

	/**
	 * 休暇の承認内容変更申請が可能かどうかを確認する。
	 * @param originalRequest 元申請
	 * @param param 承認内容変更申請のパラメータ
	 * @throws App.IllegalStateException 変更対象の期間が勤務確定済みの場合
	 */
	@TestVisible
	private void validateReapplyLeaveRequest(AttDailyRequestEntity originalRequest, ReapplyParam param) {
		// 元申請と変更申請で最も広い範囲の勤怠明細を取得する
		AppDate minStartDate = AppDate.min(originalRequest.startDate, param.startDate);
		AppDate maxEndDate = AppDate.max(originalRequest.endDate, param.endDate);
		AttSummaryRepository attRepository = new AttSummaryRepository();
		List<AttRecordEntity> recordList = attRepository.searchRecordEntityList(originalRequest.employeeBaseId, minStartDate, maxEndDate);
		Map<AppDate, AttRecordEntity> recordMap = new Map<AppDate, AttRecordEntity>();
		for (AttRecordEntity record : recordList) {
			recordMap.put(record.rcdDate, record);
		}

		// 変更前と変更後の確定日が異なる場合はエラーとする
		// TODO 本来は勤怠サマリーが作成されていない場合も、勤怠明細の有無ではなくロック状態を判定すべきだが
		//      SOQLガバナでエラーなってしまうため、勤怠サマリーが存在しない場合は未ロックとみなす
		List<AppDate> beforeLocked = new List<AppDate>();
		for (AppDate rcdDate : new AppDateRange(originalRequest.startDate, originalRequest.endDate).values()) {
			if (recordMap.containsKey(rcdDate) && recordMap.get(rcdDate).isSummaryLocked) {
				beforeLocked.add(rcdDate);
			}
		}
		List<AppDate> afterLocked = new List<AppDate>();
		for (AppDate rcdDate : new AppDateRange(param.startDate, param.endDate).values()) {
			if (recordMap.containsKey(rcdDate) &&recordMap.get(rcdDate).isSummaryLocked) {
				afterLocked.add(rcdDate);
			}
		}
		if (beforeLocked != afterLocked) {
			throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_REQUEST_LEAVE, ComMessage.msg().Att_Slt_LeaveDateHasLock);
		}
	}

	/**
	 * @description 各種勤怠エンティティに不正な値が設定されていないことを確認する
	 * @param entity 検証対象のエンティティ
	 * @throws IllegalStateException 不正な値が設定されている場合
	 */
	private void validateAttDailiyRequestEntity(AttDailyRequestEntity request) {
		Validator.Result requestValidatorResult = new AttValidator.DailyRequestValidator(request).validate();
		if (! requestValidatorResult.isSuccess()) {
			Validator.Error error = requestValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}
	}

	/**
	 * 休暇申請可能かどうかを確認する。
	 * @param workingType 申請期間で有効な勤務体系(参照するのはベースのみ)
	 * @param targetRange 申請対象の休暇範囲
	 * @throws IllegalStateException 対象の勤務体系では申請不可能な場合
	 */
	private void validateWorkingTypeLeaveRequest(AttWorkingTypeBaseEntity workingType, AttLeaveRange targetRange) {

		// 裁量労働制の場合、時間単位休暇申請不可
		if (workingType.workSystem == AttWorkSystem.JP_Discretion) {
			if (AttLeaveRange.RANGE_TIME.equals(targetRange)) {
				throw new App.IllegalStateException(App.ERR_CODE_NOT_PERMITTID_REQUEST,
						ComMessage.msg().Att_Err_NotPermittedRequest);
				}
			}

		// コアなしフレックスの場合、全日休と半日休以外申請不可
		if (workingType.workSystem == AttWorkSystem.JP_Flex &&
				workingType.withoutCoreTime == true) {
			if (!AttLeaveRange.RANGE_DAY.equals(targetRange) && !AttLeaveRange.RANGE_HALF.equals(targetRange)) {
				throw new App.IllegalStateException(App.ERR_CODE_NOT_PERMITTID_REQUEST,
					ComMessage.msg().Att_Err_NotPermittedRequest);
			}
		}
	}


	/**
	 * 指定された休暇コードの休暇が申請可能かどうかをチェックする
	 * @param avLeaves 申請可能な休暇一覧
	 * @param leaveCode 申請対象の休暇コード
	 * @param leaveRange 申請対象の休暇範囲
	 * @return 申請可能な場合はtrue, そうでない場合はfalse
	 */
	private Boolean canSubmitLeaveRequest(
			List<AttDailyRequestService.AvailableLeave> avLeaves,
			String leaveCode,
			AttLeaveRange leaveRange) {

		// 休暇コードが含まれているか確認する
		// 休暇コードが含まれていなければ申請不可
		List<AttLeaveRange> avRanges = null;
		for (AttDailyRequestService.AvailableLeave avLeave : avLeaves) {
			if (avLeave.leave.code == leaveCode) {
				avRanges = avLeave.availableRanges;
				break;
			}
		}
		if (avRanges == null) {
			return false;
		}

		// 休暇範囲が含まれていることを確認する
		Boolean canSubmit = false;
		for (AttLeaveRange avRange : avRanges) {
			if (avRange == leaveRange) {
				canSubmit = true;
				break;
			}
		}

		return canSubmit;
	}

	/**
	 * 休暇申請用の勤怠日次申請エンティティを作成する
	 * @param param 休暇申請パラメータ
	 * @param recordsWithSummary 申請対象の勤怠明細
	 * @param employeeNameL 社員名
	 * @param lang 作成言語(申請名など)
	 * @param attPatternMap 申請対象の勤務パターン
	 * @return 作成した日時申請エンティティ
	 */
	private AttDailyRequestEntity createLeaveRequest(
			SubmitParam param, Id originalRequestId,
			AttDailyRequestService.RecordsWithSummary recordsWithSummary,
			EmployeeBaseEntity employee, AttLeaveEntity leave, AppLanguage lang, Map<Id, AttPatternEntity> attPatternMap) {

// TODO AttDailyRequestServeceのメソッドを呼び出している処理は、今後修正される予定
		AttDailyRequestService requestService = new AttDailyRequestService();

		List<AttRecordEntity> records = recordsWithSummary.records;
		AttDailyRequestEntity entity = new AttDailyRequestEntity();
		entity.setId(param.requestId);
		entity.name = createLeaveRequestName(param, employee, lang);
		// entity.name = createLeaveRequestName(param, recordsWithSummary.summaryMap.get(records[0].summaryId).employeeNameL, lang);
		entity.requestType = AttRequestType.LEAVE;
		entity.status = AppRequestStatus.PENDING;
		entity.requestTime = AppDatetime.now();
		// 「理由を求める」がtrueなら理由を、falseなら備考を保存する
		if (leave.requireReason) {
			entity.reason = param.reason;
		} else {
			entity.remarks = param.remarks;
		}
		entity.leaveId = leave.id;
		entity.leaveType = leave.leaveType;
		entity.leaveRange = param.range;
		entity.startDate = param.startDate;
		entity.endDate = param.endDate != null ? param.endDate : param.startDate; // 全日休以外は終了日=開始日
		entity.startTime = param.startTime;
		entity.endTime = param.endTime;
		entity.excludingDateList = getExcludingDateList(records);
		Integer leaveTotalTime = calcLeaveTotalTime(recordsWithSummary, entity.excludingDateList, param);
		entity.leaveTotalTime = leaveTotalTime;
		entity.cancelType = AppCancelType.NONE;

		// 承認内容変更申請用に申請元申請IDを設定する
		entity.originalRequestId = originalRequestId;

		// 午前・午後半休の場合、時間帯を設定する
		// 時間単位休暇の場合、消費日数の計算を行う
		// TODO：勤務時間変更申請時始業終業時間のチェック
		Boolean isAMLeave= AttLeaveRange.RANGE_AM.equals(entity.leaveRange);
		Boolean isPMLeave= AttLeaveRange.RANGE_PM.equals(entity.leaveRange);
		Boolean isTIMELeave= AttLeaveRange.RANGE_TIME.equals(entity.leaveRange);
		if (isAMLeave || isPMLeave || isTIMELeave) {
			Map<Id, AttSummaryEntity> summaryMap = recordsWithSummary.summaryMap;
			AttSummaryEntity summaryData = summaryMap.get(records[0].summaryId);
			AttWorkingTypeBaseEntity workingTypeData = this.workingTypeService.getWorkingType(summaryData.workingTypeBaseId, summaryData.endDate);
			AttWorkingTypeHistoryEntity workingTypeHistory = workingTypeData.getHistory(0);
			requestService.validateWorkingType(workingTypeData);

			// 実行勤務パターンがある場合は勤務体系から補完する
			// 午前半日休・午後半日休・時間単位休は複数日対応しない前提
			AttPatternEntity validPattern = null;
			if ((recordsWithSummary != null) && (! recordsWithSummary.records.isEmpty())
					&& (recordsWithSummary.records[0].outPatternId != null)) {
				validPattern = attPatternMap.get(recordsWithSummary.records[0].outPatternId);
			} else {
				validPattern = workingTypeHistory.createPatternEntity();
			}

			if(isAMLeave || isPMLeave) {
				entity.startTime = isAMLeave ? validPattern.startTime : validPattern.pmEndTime;
				entity.endTime = isPMLeave ? validPattern.endTime : validPattern.amStartTime;
			}
			else if(isTIMELeave){
				// FIXME: GENIE-10288 分母が勤務体系の所定労働時間だが、仕様上は年平均の労働時間をベースに消費時間を算出するのがただしい。
				// 上記チケットで対応するため、実効勤務パターンの対応を行わない
				entity.leaveDays = AttDays.getDaysByLeaveTime(leaveTotalTime, workingTypeData.getHistory(0).contractedWorkHours.getIntValue());
			}
		}

		setRequestCommInfo(entity, employee);

		return entity;
	}

	/**
	 * 休暇申請の申請名を作成する
	 * @param employee 社員
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @param lang 作成言語
	 * @return 申請名
	 */
	@testVisible
	private String createLeaveRequestName(
			SubmitParam param, EmployeeBaseEntity employee, AppLanguage lang) {

		final AppDate.FormatType dateFormat = AppDate.FormatType.YYYYMMDD; // YYYYMMDD

		// 全日休： {氏名} さん 休暇申請 {開始日}–{終了日}
		// 単日休、半日休： {氏名} さん 休暇申請 {YYYYMMDD}
		// 時間単位休、時間指定半休： {氏名} さん 休暇申請 {YYYYMMDD} {HH:mm}–{HH:mm}
		AppDate startDate = param.startDate;
		AppDate endDate = null;
		AttTime startTime = null;
		AttTime endTime = null;
		if (param.range == AttLeaveRange.RANGE_DAY) {
			// 全日休
			endDate = param.endDate;
		} else if ((param.range == AttLeaveRange.RANGE_HALF) || (param.range == AttLeaveRange.RANGE_TIME)) {
			// 時間指定半休、時間単位休
			startTime = param.startTime;
			endTime = param.endTime;
		}

		// 申請種類名
		ComMsgBase targetLang = ComMessage.msg(lang.value);
		String label = lang == AppLanguage.JA ?
			'さん ' + targetLang.Att_Lbl_RequestTypeLeave + targetLang.Att_Lbl_Request + ' ' :
			' ' + targetLang.Att_Lbl_RequestTypeLeave + ' ' + targetLang.Att_Lbl_Request + ' ';

		// 申請範囲
		String range = startDate.format(dateFormat);
		if ((endDate != null) && (endDate != startDate)) {
			range += '–' + endDate.format(dateFormat);
		}
		if (startTime != null) {
			range += ' ' + startTime.format();
		}
		if ((endTime != null) && (endTime != startTime)) {
			range += '–' + endTime.format();
		}

		return employee.displayNameL.getValue(lang) + label + range;
	}

	/**
	 * 休暇合計時間を計算する
	 * @return 合計時間。単位は「分」
	 */
	private Integer calcLeaveTotalTime(
			AttDailyRequestService.RecordsWithSummary recordsWithSummary,
			List<AppDate> excludingDateList,
			SubmitParam param) {

		// TODO AttDailyRequestServeceのメソッドを呼び出している処理は、今後修正される予定
		AttDailyRequestService requestService = new AttDailyRequestService();

		Map<Id, AttSummaryEntity> summaryMap = recordsWithSummary.summaryMap;
		List<AttRecordEntity> records = recordsWithSummary.records;
		Integer totalTime = 0; // 単位は「分」
		Set<AppDate> excludingDateSet = new Set<AppDate>(excludingDateList);
		for (AttRecordEntity record : records) {

			// 除外日は対象外
			if (excludingDateSet.contains(record.rcdDate)) {
				continue;
			}

			AttSummaryEntity summaryData = summaryMap.get(record.summaryId);
			AttWorkingTypeBaseEntity workingType = this.workingTypeService.getWorkingType(summaryData.workingTypeBaseId,summaryData.endDate);
			AttWorkingTypeHistoryEntity workingTypeHistory = workingType.getHistory(0);
			requestService.validateWorkingType(workingType);
			if (param.range == AttLeaveRange.RANGE_DAY) {
				// 全日休の場合、所定勤務時間
				totalTime += workingTypeHistory.contractedWorkHours.getIntValue();
			} else if ((param.range == AttLeaveRange.RANGE_AM)
					|| (param.range == AttLeaveRange.RANGE_PM)
					|| (param.range == AttLeaveRange.RANGE_HALF)) {
				// 半休の場合、所定勤務時間 * 0.5
				totalTime += (Integer)(workingTypeHistory.contractedWorkHours.getIntValue() * 0.5).round(System.RoundingMode.CEILING);
			} else if (param.range == AttLeaveRange.RANGE_TIME) {
				// 所定休憩時間範囲を取得する
				AttCalcRangesDto contractedRestRanges = AttCalcDto.calcRanges(
					workingTypeHistory.rest1StartTime, workingTypeHistory.rest1EndTime,
					workingTypeHistory.rest2StartTime, workingTypeHistory.rest2EndTime,
					workingTypeHistory.rest3StartTime, workingTypeHistory.rest3EndTime,
					workingTypeHistory.rest4StartTime, workingTypeHistory.rest4EndTime,
					workingTypeHistory.rest5StartTime, workingTypeHistory.rest5EndTime
					);
				AttCalcRangesDto timeLeaveRanges = AttCalcRangesDto.RS(param.startTime.getIntValue(), param.endTime.getIntValue());
				// 時間単位休 申請時間(休憩除く)
				totalTime += timeLeaveRanges.exclude(contractedRestRanges).total();
			} else {
				// ここにきたら実装ミス。とりあえずログに出しておく
				System.debug(LoggingLevel.ERROR, '対応していない休暇範囲です。range='+ param.range);
			}
		}
		return totalTime;
	}

	/**
	 * 休暇申請の除外日を取得する
	 * @param 申請対象日の勤怠明細リスト
	 * @return 除外日のリスト
	 */
	private List<AppDate> getExcludingDateList(List<AttRecordEntity> records) {
		// 勤務日以外は除外日とする
		List<AppDate> excludingDateList = new List<AppDate>();
		for (AttRecordEntity record : records) {
			if (record.outDayType != AttDayType.WORKDAY) {
				excludingDateList.add(record.rcdDate);
			}
		}
		return excludingDateList;
	}

	/**
	 * @description 休暇マスタを取得する
	 * @param leaveCode 休暇コード
	 * @param companyId 会社ID
	 * @param startDate 申請開始日
	 * @param endDate 申請終了日
	 * @return 休暇
	 * @throws IllegalStateException 指定されたコードの休暇が見つからない場合
	 * @throws ParameterException 申請期間で休暇が無効な期間が存在する場合
	 */
	private AttLeaveEntity getLeaveByCode(String leaveCode, Id companyId, AppDate startDate, AppDate endDate) {
		// 休暇が申請期間中に有効であることを確認する
		AttLeaveEntity leave = new AttLeaveRepository().getLeaveByCode(leaveCode, companyId);
		if (leave == null) {
			// メッセージ：指定された休暇が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_LEAVE,
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_Leave}));
		}
		if (! leave.isValid(startDate) ||
				(endDate != null && ! leave.isValid(endDate))) {
			// メッセージ：申請期間が休暇の有効期間外です。
			throw new App.ParameterException(ComMessage.msg().Att_Err_OutOfLeavePeriod);
		}

		return leave;
	}

	/**
	 * @description 勤務パターンをMapで取得する
	 * @param patternIdSet 取得対象の勤務パターンのSet
	 * @return 勤務パターンIDをキーとした勤務パターンMap
	 */
	private Map<Id, AttPatternEntity> getPatternMapById(Set<Id> patternIdSet) {
		List<AttPatternEntity> patternList = new AttPatternRepository2().getPatternList(patternIdSet);
		// 勤務パターンをMapに変換する
		Map<Id, AttPatternEntity> patternMap = new Map<Id, AttPatternEntity>();
		for (AttPatternEntity pattern : patternList) {
			patternMap.put(pattern.id, pattern);
		}
		return patternMap;
	}

	/**
	 * @description 承認取消時のメールを作成する
	 * @param request 申請
	 * @param applicant 申請者
	 * @param empLastModified 最終更新者
	 * @param targetLanguage 送信先ユーザの言語
	 * @return mailMessage メール内容
	 */
	public override MailService.MailParam createCancelMailMessage(AttDailyRequestEntity request, EmployeeBaseEntity applicant, EmployeeBaseEntity empLastModified, AppLanguage targetLanguage) {

		MailService.MailParam mailMessage = new MailService.MailParam();
		String language = targetLanguage.value;

		// 送信者名
		String senderDisplayName = empLastModified.fullNameL.getFullName(targetLanguage);
		mailMessage.senderDisplayName = senderDisplayName;

		// メールタイトル
		String mailSubject;
		// 申請種別を文字列に変換する
		String requestType = ComMessage.msg(language).Att_Lbl_RequestTypeLeave;
		mailSubject = ComMessage.msg(language).Att_Msg_CancelMailSubject(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType});
		mailMessage.subject = mailSubject;

		// メール本文
		String mailBody;
		// 申請日付
		String requestDate = request.requestTime.formatLocal().substring(0, 10).replace('-', '/');
		// 休暇種類
		String leaveName = request.leaveNameL.getValue(targetLanguage);
		// 休暇範囲
		String leaveRange = convertLeaveRange(request.leaveRange, language);
		// 開始日
		String startDate = String.valueOf(AppDate.convertDate(request.startDate)).replace('-', '/');
		// 終了日
		String endDate = String.valueOf(AppDate.convertDate(request.endDate)).replace('-', '/');
		// 開始時刻
		String startTime = request.startTime != null ? request.startTime.format() : null;
		// 終了時刻
		String endTime = request.endTime != null ? request.endTime.format() : null;
		// 備考
		String remarks = request.remarks;
		// メール本文を作成
		mailBody = ComMessage.msg(language).Att_Msg_CancelMailMessage(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType, empLastModified.fullNameL.getFullName(targetLanguage)});
		mailBody += '\r\n\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_RequestContents + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestDate + '：' + requestDate + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestType + '：' + requestType + '\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_LeaveType + '：' + leaveName + '\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_Range + '：' + leaveRange + '\r\n';
		// 期間での申請の場合は、開始日と終了日を追加
		if (startDate.equals(endDate)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate;
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate + '－' + endDate;
		}
		// 時間単位休の場合は、開始時刻と終了時刻を追加
		if (String.isNotBlank(startTime) && AttLeaveRange.RANGE_TIME.equals(request.leaveRange)) {
			mailBody += ' ' + startTime + '－' + endTime;
		}
		mailBody += '\r\n';
		if (String.isNotBlank(remarks)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + remarks + '\r\n';
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + '\r\n';
		}
		// 署名
		mailBody += '\r\n---\r\n';
		mailBody += ComMessage.msg(language).Att_Msg_TeamSpirit;
		mailMessage.body = mailBody;

		return mailMessage;
	}

	/**
	 * 休暇範囲
	 */
	private String convertLeaveRange(AttLeaveRange leaveRange, String targerLanguage) {

		String leaveRangeStr;
		if (AttLeaveRange.RANGE_DAY.equals(leaveRange)) {
			leaveRangeStr = ComMessage.msg(targerLanguage).Att_Lbl_FullDayLeave;
		} else if (AttLeaveRange.RANGE_AM.equals(leaveRange)) {
			leaveRangeStr = ComMessage.msg(targerLanguage).Att_Lbl_FirstHalfOfDayLeave;
		} else if (AttLeaveRange.RANGE_PM.equals(leaveRange)) {
			leaveRangeStr = ComMessage.msg(targerLanguage).Att_Lbl_SecondHalfOfDayLeave;
		} else if (AttLeaveRange.RANGE_HALF.equals(leaveRange)) {
			leaveRangeStr = ComMessage.msg(targerLanguage).Att_Lbl_HourlyDesignatedHalfDayLeave;
		} else if (AttLeaveRange.RANGE_TIME.equals(leaveRange)) {
			leaveRangeStr = ComMessage.msg(targerLanguage).Att_Lbl_HourlyLeave;
		}
		return leaveRangeStr;
	}
}