/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Repository of Expense Request Approval
 */
public with sharing class ExpRequestApprovalRepository extends Repository.BaseRepository {

	/** Max record number to fetch */
	private static Integer MAX_SEARCH_RECORDS_NUMBER = 10000;

	/** Do not execute query with 'FOR UPDATE' */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** Target fields to fetch data */
	private static final List<String> SELECT_FIELD_SET;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpRequestApproval__c.Id,
			ExpRequestApproval__c.Name,
			ExpRequestApproval__c.OwnerId,
			ExpRequestApproval__c.ExpRequestId__c,
			ExpRequestApproval__c.TotalAmount__c,
			ExpRequestApproval__c.RequestNo__c,
			ExpRequestApproval__c.Subject__c,
			ExpRequestApproval__c.Comment__c,
			ExpRequestApproval__c.ProcessComment__c,
			ExpRequestApproval__c.Status__c,
			ExpRequestApproval__c.Claimed__c,
			ExpRequestApproval__c.ConfirmationRequired__c,
			ExpRequestApproval__c.CancelType__c,
			ExpRequestApproval__c.EmployeeHistoryId__c,
			ExpRequestApproval__c.RequestTime__c,
			ExpRequestApproval__c.DepartmentHistoryId__c,
			ExpRequestApproval__c.ActorHistoryId__c,
			ExpRequestApproval__c.LastApproveTime__c,
			ExpRequestApproval__c.LastApproverId__c,
			ExpRequestApproval__c.Approver01Id__c
			};
		SELECT_FIELD_SET = Repository.generateFieldNameList(fields);
	}

	/** Relation of Employee */
	private static final String EMP_HISTORY_R = ExpRequestApproval__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	private static final String EMP_BASE_R = ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();
	private static final String USER_R = ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();

	/** Relation of Department */
	private static final String DEPT_HISTORY_R = ExpRequestApproval__c.DepartmentHistoryId__c.getDescribe().getRelationshipName();
	private static final List<String> SELECT_DEPT_FIELD_SET;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ComDeptHistory__c.Name_L0__c,
			ComDeptHistory__c.Name_L1__c,
			ComDeptHistory__c.Name_L2__c,
			ComDeptHistory__c.ValidFrom__c,
			ComDeptHistory__c.ValidTo__c,
			ComDeptHistory__c.Removed__c
		};
		SELECT_DEPT_FIELD_SET = Repository.generateFieldNameList(DEPT_HISTORY_R, fields);
	}

	/** Relation of Actor(Employee) */
	private static final String ACTOR_HISTORY_R = ExpRequestApproval__c.ActorHistoryId__c.getDescribe().getRelationshipName();

	/** Acces level */
	private static String SHARE_ACCESS_LEVEL = 'Edit';
	/** Sharing reason */
	public static String SHARE_ROW_CAUSE = Schema.ExpRequestApproval__Share.RowCause.DelegatedApprover__c;

	/** Target fields to fetch data(Sharing info) */
	private static final Set<Schema.SObjectField> SELECT_SHARE_FIELD_SET;
	static {
		SELECT_SHARE_FIELD_SET = new Set<Schema.SObjectField> {
			ExpRequestApproval__Share.Id,
			ExpRequestApproval__Share.ParentId,
			ExpRequestApproval__Share.UserOrGroupId,
			ExpRequestApproval__Share.AccessLevel,
			ExpRequestApproval__Share.RowCause
		};
	}

	/** Fields to be retrieved using UserRecordAccess */
	private static final List<String> GET_USER_RECORD_ACCESS_FIELD_NAME_LIST;
	/** UserRecordAccess FK (relation name cannot be retrieved) */
	private static final String USER_RECORD_ACCESS_R = 'UserRecordAccess';
	static {
		// Fields to be retrieved (UserRecordAccess)
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			UserRecordAccess.HasEditAccess};
		GET_USER_RECORD_ACCESS_FIELD_NAME_LIST = Repository.generateFieldNameList(USER_RECORD_ACCESS_R, fieldList);
	}

	/**
	 * Get entity by specified ID
	 * @param reportId ID of target entity
	 * @return Entity of specified ID. If no data is found, return null.
	 */
	public ExpRequestApprovalEntity getEntity(Id id) {
		List<ExpRequestApprovalEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * Get entity list by specified IDs
	 * @param ids ID list of target entity
	 * @return Entity list of specified IDs.  If no data is found, return null.
	 */
	public List<ExpRequestApprovalEntity> getEntityList(List<Id> ids) {
		SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter);
	}

	/*
	 * Get entity list by Request IDs
	 * @param requestIds ID list of Requests
	 * @return Entity list which has the specified Request IDs.
	 */
	public List<ExpRequestApprovalEntity> getEntityListFromRequestId(Set<Id> requestIds) {
		SearchFilter filter = new SearchFilter();
		filter.requestIds = requestIds;
		return searchEntity(filter);
	}

	/*
	 * Get entity list by employee ID
	 * @param approver01Id Approver01 User ID
	 * @param status Approval status
	 * @return Entity list which has the specified employee ID and status.
	 */
	public List<ExpRequestApprovalEntity> getEntityListByApprover01Id(Id approver01Id, AppRequestStatus status) {
		SearchFilter filter = new SearchFilter();
		filter.approver01Id = approver01Id;
		filter.statusList = new Set<AppRequestStatus>{status};
		return searchEntity(filter);
	}

	/*
 * Get entity list by employee ID
 * @param empHistoryId Employee history ID
 * @param status Approval status
 * @return Entity list which has the specified employee ID and status.
 */
	public List<ExpRequestApprovalEntity> getEntityListByEmpId(Id empHistoryId, AppRequestStatus status) {
		SearchFilter filter = new SearchFilter();
		filter.empHistoryIds = new Set<Id>{empHistoryId};
		filter.statusList = new Set<AppRequestStatus>{status};
		return searchEntity(filter);
	}

	/**
	 * Save a entity
	 * @param entity Entity to be saved
	 * @return Saved result
	 */
	public Repository.SaveResult saveEntity(ExpRequestApprovalEntity entity) {
		return saveEntityList(new List<ExpRequestApprovalEntity>{ entity });
	}

	/**
	 * Save entity list
	 * @param entityList Entity list to be saved
	 * @return Saved result
	 */
	public Repository.SaveResult saveEntityList(List<ExpRequestApprovalEntity> entityList) {
		// Convert entity to sObject
		List<ExpRequestApproval__c> sObjList = createObjectList(entityList);

		// Save to DB
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sObjList);

		// Make and return result
		return resultFactory.createSaveResult(recResList);
	}

	/** Search Filters */
	private class SearchFilter {
		/** Record ID */
		public Set<Id> ids;
		/** Request ID */
		public Set<Id> requestIds;
		/** Employee history ID */
		public Set<Id> empHistoryIds;
		/** Approver01 ID */
		public Id approver01Id;
		/** Approval status */
		public Set<AppRequestStatus> statusList;
	}

	/**
	 * Search expense request
	 * @param filter Search filter
	 * @return Search result
	 */
	private List<ExpRequestApprovalEntity> searchEntity(SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE, MAX_SEARCH_RECORDS_NUMBER);
	}

	/**
   * Search expense request
   * @param filter Search filter
   * @param forUpdate When call this method for update, specify as true.
   * @return Search result
   */
	private List<ExpRequestApprovalEntity> searchEntity(SearchFilter filter, Boolean forUpdate, Integer limitNumber) {

		// SELECT Target fields
		List<String> selectFieldList = createSelectFieldList();

		// WHERE statement
		List<String> whereList = createWhereList(filter);

		// Bind variables
		final Set<Id> ids = filter.ids;
		final Set<Id> requestIds = filter.requestIds;
		final Set<Id> empHistoryIds = filter.empHistoryIds;
		final Id approver01Id = filter.approver01Id;
		final Set<String> statusList = new Set<String>();
		if (filter.statusList != null && !filter.statusList.isEmpty()) {
			for (AppRequestStatus status : filter.statusList) {
				statusList.add(status.value);
			}
		}
		Integer limitNum = limitNumber;

		// Create SOQL
		String soql = createSQOL(selectFieldList, whereList, forUpdate, limitNumber);

		// Execute query
		System.debug('ExpRequest.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// Create entity list from result
		return createEntityList(sObjList);
	}

	/*
	 * Create filed list to be fetched
	 */
	private List<String> createSelectFieldList() {
		List<String> fieldList = new List<String>();
		// Regular fields
		fieldList.addAll(SELECT_FIELD_SET);
		// Relation fields (Employee)
		fieldList.addAll(createEmployeeFields(EMP_HISTORY_R));
		// Relation fields (Department)
		fieldList.addAll(SELECT_DEPT_FIELD_SET);
		// Relation fields (Actor(Employee))
		fieldList.addAll(createEmployeeFields(ACTOR_HISTORY_R));
		// UserRecordAccess
		fieldList.addAll(GET_USER_RECORD_ACCESS_FIELD_NAME_LIST);
		return fieldList;
	}

	/**
	 * Create field list of Employee
   * @param relationshipName Parent relationship name
   * @return Target field list
   */
	private List<String> createEmployeeFields(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.FirstName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.FirstName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.FirstName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.LastName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.LastName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.LastName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.UserId__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, USER_R, User.SmallPhotoUrl));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, USER_R, User.IsActive));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidFrom__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidTo__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.Removed__c));
		return selectFldList;
	}

	/*
   * Create where statement list
   */
	private List<String> createWhereList(SearchFilter filter) {
		List<String> whereList = new List<String>();
		// Record ID
		if (filter.ids != null) {
			whereList.add('Id IN :ids');
		}
		// Request ID
		if (filter.requestIds != null && !filter.requestIds.isEmpty()) {
			whereList.add(getFieldName(ExpRequestApproval__c.ExpRequestId__c) + ' IN :requestIds');
		}
		// Employee history ID
		if (filter.empHistoryIds != null && !filter.empHistoryIds.isEmpty()) {
			whereList.add(getFieldName(ExpRequestApproval__c.EmployeeHistoryId__c) + ' IN :empHistoryIds');
		}
		// Employee history ID
		if (filter.approver01Id != null) {
			whereList.add(getFieldName(ExpRequestApproval__c.Approver01Id__c) + ' = :approver01Id');
		}
		// Approval status
		if (filter.statusList != null && !filter.statusList.isEmpty()) {
			whereList.add(getFieldName(ExpRequestApproval__c.Status__c) + ' IN :statusList');
		}
		return whereList;
	}

	/*
   * Create SOQL
   * @param fieldList Field list to be fetched
   * @param whereList Where statement list
   */
	private String createSQOL(List<String> fieldList, List<String> whereList, Boolean forUpdate , Integer limitNum) {
		String fieldStr = String.join(fieldList, ',');
		String whereStr = (whereList.isEmpty()) ? '' : ' WHERE (' + String.join(whereList, ') AND (') + ')';
		String objectName = ExpRequestApproval__c.SObjectType.getDescribe().getName();
		String orderStr = ' ORDER BY ' + getFieldName(ExpRequestApproval__c.RequestNo__c) + ' Desc';
		String forUpdateStr = forUpdate ? ' FOR UPDATE' : '';

		return 'SELECT ' +
			fieldStr +
			' FROM ' +
			objectName +
			whereStr +
			orderStr +
			' LIMIT :limitNum' +
			forUpdateStr;
	}

	/**
   * Create entity list from SObject list
   * @param sObjList Original SObject list
   * @return Created entity list
   */
	private List<ExpRequestApprovalEntity> createEntityList(List<ExpRequestApproval__c> sObjList) {
		List<ExpRequestApprovalEntity> entityList = new List<ExpRequestApprovalEntity>();
		for (ExpRequestApproval__c sobj : sObjList) {
			entityList.add(createEntity(sobj));
		}
		return entityList;
	}

	/**
   * Create entity from SObject
   * @param obj Original SObject
   * @return Created entity
   */
	private ExpRequestApprovalEntity createEntity(ExpRequestApproval__c obj) {
		ExpRequestApprovalEntity entity = new ExpRequestApprovalEntity();

		entity.setId(obj.Id);
		entity.name = obj.Name;
		entity.ownerId = obj.OwnerId;
		entity.expRequestId = obj.ExpRequestId__c;
		entity.totalAmount = obj.TotalAmount__c;
		entity.requestNo = obj.RequestNo__c;
		entity.subject = obj.Subject__c;
		entity.comment = obj.Comment__c;
		entity.processComment = obj.ProcessComment__c;
		entity.status = AppRequestStatus.valueOf(obj.Status__c);
		entity.claimed = obj.Claimed__c;
		entity.isConfirmationRequired = obj.ConfirmationRequired__c;
		entity.cancelType = AppCancelType.valueOf(obj.CancelType__c);
		entity.employeeHistoryId = obj.EmployeeHistoryId__c;
		entity.employeeBaseId = obj.EmployeeHistoryId__r.BaseId__c;
		entity.employee = new AppLookup.Employee(
				null,
				new AppPersonName(
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c,
					obj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c,
					obj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c,
					obj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c),
				obj.EmployeeHistoryId__r.BaseId__r.UserId__c,
				obj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
				AppDate.valueOf(obj.EmployeeHistoryId__r.ValidFrom__c),
				AppDate.valueOf(obj.EmployeeHistoryId__r.ValidTo__c),
				obj.EmployeeHistoryId__r.Removed__c,
				obj.EmployeeHistoryId__r.BaseId__r.UserId__r.isActive);
		entity.requestTime = AppDatetime.valueOf(obj.RequestTime__c);
		entity.departmentHistoryId = obj.DepartmentHistoryId__c;
		entity.department = new AppLookup.Department(
				null,
				new AppMultiString(
					obj.DepartmentHistoryId__r.Name_L0__c,
					obj.DepartmentHistoryId__r.Name_L1__c,
					obj.DepartmentHistoryId__r.Name_L2__c),
				AppDate.valueOf(obj.DepartmentHistoryId__r.ValidFrom__c),
				AppDate.valueOf(obj.DepartmentHistoryId__r.ValidTo__c),
				obj.DepartmentHistoryId__r.Removed__c);
		entity.actorHistoryId = obj.ActorHistoryId__c;
		entity.actor = new AppLookup.Employee(
				null,
				new AppPersonName(
					obj.ActorHistoryId__r.BaseId__r.FirstName_L0__c,
					obj.ActorHistoryId__r.BaseId__r.LastName_L0__c,
					obj.ActorHistoryId__r.BaseId__r.FirstName_L1__c,
					obj.ActorHistoryId__r.BaseId__r.LastName_L1__c,
					obj.ActorHistoryId__r.BaseId__r.FirstName_L2__c,
					obj.ActorHistoryId__r.BaseId__r.LastName_L2__c),
				obj.ActorHistoryId__r.BaseId__r.UserId__c,
				obj.ActorHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
				AppDate.valueOf(obj.ActorHistoryId__r.ValidFrom__c),
				AppDate.valueOf(obj.ActorHistoryId__r.ValidTo__c),
				obj.ActorHistoryId__r.Removed__c,
				obj.ActorHistoryId__r.BaseId__r.UserId__r.isActive);
		entity.lastApproveTime = AppDatetime.valueOf(obj.LastApproveTime__c);
		entity.lastApproverId = obj.LastApproverId__c;
		entity.approver01Id = obj.Approver01Id__c;
		entity.hasEditAccess = obj.UserRecordAccess.HasEditAccess;

		entity.resetChanged();
		return entity;
	}

	/**
   * Create object from entity to save
   * @param entityList Target entity list to save
   * @return Object list to be saved
   */
	private List<SObject> createObjectList(List<ExpRequestApprovalEntity> entityList) {
		List<ExpRequestApproval__c> objectList = new List<ExpRequestApproval__c>();
		for (ExpRequestApprovalEntity entity : entityList) {
			objectList.add(createObject(entity));
		}
		return objectList;
	}

	/**
	 * Create object from entity to save
	 * @param entity Original entity
	 * @return SObject
	 */
	private ExpRequestApproval__c createObject(ExpRequestApprovalEntity entity) {
		ExpRequestApproval__c obj = new ExpRequestApproval__c();

		obj.Id = entity.id;
		if (entity.isChanged(ExpRequestApprovalEntity.Field.Name)) {
			obj.Name = entity.name;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.EXP_REQUEST_ID)) {
			obj.ExpRequestId__c = entity.expRequestId;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.TOTAL_AMOUNT)) {
			obj.TotalAmount__c = entity.totalAmount;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.REQUEST_NO)) {
			obj.RequestNo__c = entity.requestNo;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.SUBJECT)) {
			obj.Subject__c = entity.subject;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.COMMENT)) {
			obj.Comment__c = entity.comment;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.PROCESS_COMMENT)) {
			obj.ProcessComment__c = entity.processComment;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.STATUS)) {
			obj.Status__c = AppConverter.stringValue(entity.status);
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.IS_CLAIMED)) {
			obj.Claimed__c = entity.claimed;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.IS_CONFIRMATION_REQUIRED)) {
			obj.ConfirmationRequired__c = entity.isConfirmationRequired;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.CANCEL_TYPE)) {
			obj.CancelType__c = AppConverter.stringValue(entity.cancelType);
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.EMPLOYEE_HISTORY_ID)) {
			obj.EmployeeHistoryId__c = entity.employeeHistoryId;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.REQUEST_TIME)) {
			obj.RequestTime__c = AppDatetime.convertDatetime(entity.requestTime);
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.DEPARTMENT_HISTORY_ID)) {
			obj.DepartmentHistoryId__c = entity.departmentHistoryId;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.ACTOR_HISTORY_ID)) {
			obj.ActorHistoryId__c = entity.actorHistoryId;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.LAST_APPROVAL_TIME)) {
			obj.LastApproveTime__c = AppDatetime.convertDatetime(entity.lastApproveTime);
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.LAST_APPROVAL_ID)) {
			obj.LastApproverId__c = entity.lastApproverId;
		}
		if (entity.isChanged(ExpRequestApprovalEntity.Field.APPROVAL_01_ID)) {
			obj.Approver01Id__c = entity.approver01Id;
		}

		return obj;
	}

	/** Search Filters(Sharing info) */
	 private class  ShareFilter {
		/** Request ID */
		public List<Id> approvalIds;
	 }

	/**
	 * Get share entity list by request IDs
	 * @param requestIds ID list of target request
	 * @return Share entity list of specified requests
	 */
	public List<AppShareEntity> getShareEntityListByApprovalId(List<Id> approvalIds) {
		ShareFilter filter = new ShareFilter();
		filter.approvalIds = approvalIds;
		return searchShareEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * Search sharing info
	 * @param filter Search filter
	 * @param forUpdate When call this method for update, specify as true.
	 * @return Search result
	 */
	 private List<AppShareEntity> searchShareEntity(ShareFilter filter, Boolean forUpdate) {

		// SELECT Target fields
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_SHARE_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// WHERE statement
		List<String> condList = new List<String>();
		final List<Id> approvalIds = filter.approvalIds;
		if (approvalIds != null && !approvalIds.isEmpty()) {
			condList.add(getFieldName(ExpRequestApproval__Share.ParentId) + ' IN :approvalIds');
		}
		condList.add(getFieldName(ExpRequestApproval__Share.RowCause) + ' = :SHARE_ROW_CAUSE');

		String whereCond;
		if (!condList.isEmpty()) {
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// Create SOQL
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ExpRequestApproval__Share.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		soql += ' LIMIT :MAX_SEARCH_RECORDS_NUMBER';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// Execute query
		System.debug('ExpRequestApprovalRepository.searchShareEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// Create entity list from result
		List<AppShareEntity> entityList = new List<AppShareEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createShareEntity((ExpRequestApproval__Share)sobj));
		}

		return entityList;
	 }

	/**
	 * Create entity from SObject
	 * @param obj Original SObject
	 * @return Created entity
	 */
	private AppShareEntity createShareEntity(ExpRequestApproval__Share obj) {
		AppShareEntity entity = new AppShareEntity();

		entity.setId(obj.Id);
		entity.parentId = obj.ParentId;
		entity.userOrGroupId = obj.UserOrGroupId;

		entity.resetChanged();
		return entity;
	}

}