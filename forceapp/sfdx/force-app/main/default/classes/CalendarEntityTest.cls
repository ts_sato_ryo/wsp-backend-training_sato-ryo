/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description CalendarEntityのテスト
 */
@isTest
private class CalendarEntityTest {

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		CalendarEntity entity = new CalendarEntity();

		// 会社コードとカレンダーコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'CalendarCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// カレンダーコードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'CalendarCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}
}