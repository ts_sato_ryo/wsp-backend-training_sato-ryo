/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Expense Modification History Entity class
 */
public with sharing class ExpModificationHistoryEntity extends Entity {

	/** Expense Request ID */
	public Id expRequestId {
		get;
		set;
	}

	/** Expense Report ID */
	public Id expReportId {
		get;
		set;
	}

	/** Expense Record ID */
	public Id expRecordId {
		get;
		set;
	}

	/** Expense Record Item ID */
	public Id expRecordItemId {
		get;
		set;
	}

	/** Record Summary */
	public String recordSummary {
		get;
		set;
	}

	/** Extended Item ID */
	public Id extendedItemId {
		get;
		set;
	}

	/** Extended Item Name */
	public AppMultiString extendedItemName {
		get;
		set;
	}

	/** Field (sObject field name) */
	public String field {
		get;
		set;
	}

	/** Field label key */
	public String fieldLabel {
		get;
		set;
	}

	/** New Value */
	public String newValue {
		get;
		set;
	}

	/** Old Value */
	public String oldValue {
		get;
		set;
	}

	/** Comment */
	public String comment {
		get;
		set;
	}

	/** Modified By */
	public Id modifiedBy {
		get;
		set;
	}

	/** Modified By Employee Name */
	public AppPersonName modifiedByEmployeeName {
		get;
		set;
	}

	/** Modified Date Time */
	public AppDateTime modifiedDateTime {
		get;
		set;
	}
}