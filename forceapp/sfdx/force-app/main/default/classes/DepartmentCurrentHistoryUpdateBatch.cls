/**
 * @author TeamSpirit Inc.
 * @date 2017
 * 
 * @group 共通
 * 
 * @description 部署履歴切り替えバッチ
 */
public with sharing class DepartmentCurrentHistoryUpdateBatch extends CurrentHistoryUpdateBatchBase implements Schedulable {

	/**
	 * コンストラクタ
	 */
	public DepartmentCurrentHistoryUpdateBatch() {
		// ベース・履歴オブジェクトのAPI参照名を渡す
		super(ComDeptBase__c.getSObjectType(), ComDeptHistory__c.getSObjectType());
	}

	/**
	 * Apex実行スケジュールを登録する
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc){
		DepartmentCurrentHistoryUpdateBatch b = new DepartmentCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
	}

	/**
	 * バッチ開始処理
	 * @param  BC バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public override Database.QueryLocator start(Database.BatchableContext BC) {
		// ベースオブジェクトを全取得する
		return super.start(BC);
	}

	/**
	 * バッチ実行処理
	 * @param  BC    バッチコンテキスト
	 * @param  scope
	 */
	public override void execute(Database.BatchableContext BC, List<Sobject> scope) {
		super.execute(BC, scope);
	}

	/**
	 * バッチ終了処理
	 * @param  BC    バッチコンテキスト
	 */
	public override void finish(Database.BatchableContext BC) {
		super.finish(BC);
	}

}