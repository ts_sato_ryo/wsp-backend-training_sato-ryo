/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * @description ExpTypeEntityのテスト
 */
@isTest
private class ExpTypeEntityTest {

	/**
	 * テストデータクラス
	 */
	private class TestData extends TestData.TestDataEntity {
		/** 税区分データ */
		public List<ExpTaxTypeBaseEntity> taxTypes = new List<ExpTaxTypeBaseEntity>();
		/** 拡張項目データ */
		public List<ExtendedItemEntity> extendedItems = new List<ExtendedItemEntity>();
		/** 拡張項目(選択リスト)データ Extended item data(Picklist) */
		public List<ExtendedItemEntity> extendedItemsPicklist = new List<ExtendedItemEntity>();
		/** 拡張項目使用 */
		public ComExtendedItemUsedIn extendedItemUsedIn;
		/** 拡張項目必須 */
		public ComExtendedItemRequiredFor extendedItemRequiredFor;

		/**
		 * コンストラクタ
		 */
		public TestData() {
			super();
			// 税区分データ作成
			ComTestDataUtility.createTaxTypesWithHistory('Tax Type', null, this.company.id, 3);
			this.taxTypes = (new ExpTaxTypeRepository()).getEntityList(null, AppDate.today());
			// 拡張項目データ作成
			List<Id> extendedItemIdList = new List<Id>();
			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItems('Extended Item', this.company.id, 10)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItems = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);
			// 選択リスト Picklist
			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItems('Extended Item Picklist', this.company.id, ExtendedItemInputType.PICKLIST, 10)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItemsPicklist = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);
			// 拡張項目使用作成
			this.extendedItemUsedIn = ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			// 拡張項目必須作成
			this.extendedItemRequiredFor = ComExtendedItemRequiredFor.EXPENSE_REPORT;
		}

		/**
		 * 費目オブジェクトを作成する
		 */
		public List<ExpTypeEntity> createExpTypes(String name, Integer size) {
			return createExpTypes(name, this.company.id, size);
		}

		/**
		 * 費目オブジェクトを作成する
		 */
		public List<ExpTypeEntity> createExpTypes(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpTypes(name, companyId, size);
			return (new ExpTypeRepository()).getEntityList(null);
		}

		/**
		 * 費目エンティティを作成する(DB保存なし)
		 */
		public ExpTypeEntity createExpTypeEntity(String name) {
			ExpTypeEntity entity = new ExpTypeEntity();

			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.company.code + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.company.id;
			entity.parentGroupId = null;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.setUsage('Normal');
			entity.fileAttachment = ExpFileAttachment.OPTIONAL;
			entity.recordType = ExpRecordType.GENERAL;
			entity.taxTypeBase1Id = this.taxTypes[0].id;
			entity.taxTypeBase2Id = this.taxTypes[1].id;
			entity.taxTypeBase3Id = this.taxTypes[2].id;
			for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				entity.setExtendedItemTextId(i, this.extendedItems[i].id);
				entity.setExtendedItemTextUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemTextRequiredFor(i, this.extendedItemRequiredFor);

				entity.setExtendedItemPicklistId(i, this.extendedItemsPicklist[i].id);
				entity.setExtendedItemPicklistUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemPicklistRequiredFor(i, this.extendedItemRequiredFor);
			}
			entity.order = 1;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);
			entity.useForeignCurrency = false;

			return entity;
		}
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		ExpTypeEntity entity = new ExpTypeEntity();

		// 会社コードと費目グループコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'GroupCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// 費目グループコードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'ExpTypeCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/**
	 * copyのテスト
	 * エンティティが複製できることを確認する
	 */
	@isTest static void copyTest() {
		ExpTypeEntityTest.TestData testData = new TestData();
		ExpTypeEntity orgEntity = testData.createExpTypes('Test', 2)[1];

		ExpTypeEntity copyEntity = orgEntity.copy();

		System.assertEquals(null, copyEntity.id);  // IDはコピーされない
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.nameL0, copyEntity.nameL0);
		System.assertEquals(orgEntity.nameL1, copyEntity.nameL1);
		System.assertEquals(orgEntity.nameL2, copyEntity.nameL2);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.parentGroupId, copyEntity.parentGroupId);
		System.assertEquals(orgEntity.descriptionL0, copyEntity.descriptionL0);
		System.assertEquals(orgEntity.descriptionL1, copyEntity.descriptionL1);
		System.assertEquals(orgEntity.descriptionL2, copyEntity.descriptionL2);
		System.assertEquals(orgEntity.usage, copyEntity.usage);
		System.assertEquals(orgEntity.fileAttachment, copyEntity.fileAttachment);
		System.assertEquals(orgEntity.recordType, copyEntity.recordType);
		System.assertEquals(orgEntity.taxTypeBase1Id, copyEntity.taxTypeBase1Id);
		System.assertEquals(orgEntity.taxTypeBase2Id, copyEntity.taxTypeBase2Id);
		System.assertEquals(orgEntity.taxTypeBase3Id, copyEntity.taxTypeBase3Id);
		for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			System.assertEquals(orgEntity.getExtendedItemTextId(i), copyEntity.getExtendedItemTextId(i));
			System.assertEquals(orgEntity.getExtendedItemTextUsedIn(i), copyEntity.getExtendedItemTextUsedIn(i));
			System.assertEquals(orgEntity.getExtendedItemTextRequiredFor(i), copyEntity.getExtendedItemTextRequiredFor(i));

			System.assertEquals(orgEntity.getExtendedItemPicklistId(i), copyEntity.getExtendedItemPicklistId(i));
			System.assertEquals(orgEntity.getExtendedItemPicklistUsedIn(i), copyEntity.getExtendedItemPicklistUsedIn(i));
			System.assertEquals(orgEntity.getExtendedItemPicklistRequiredFor(i), copyEntity.getExtendedItemPicklistRequiredFor(i));
		}
		System.assertEquals(orgEntity.order, copyEntity.order);
		System.assertEquals(orgEntity.validFrom, copyEntity.validFrom);
		System.assertEquals(orgEntity.validTo, copyEntity.validTo);
		System.assertEquals(orgEntity.useForeignCurrency, copyEntity.useForeignCurrency);
	}
}