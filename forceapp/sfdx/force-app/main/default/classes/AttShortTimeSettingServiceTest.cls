/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttShortTimeSettingServiceのテスト
 */
@isTest
private class AttShortTimeSettingServiceTest {

	/**
	 * テストデータクラス
	 */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * 短時間勤務設定を作成する
		 */
		public List<AttShortTimeSettingBaseEntity> createShortSettingList(
				String name, Integer baseSize, Integer historySize) {
			List<AttShortTimeWorkSettingBase__c> settingObjList =
					ComTestDataUtility.createAttShortTimeSettingsWithHistories(
							name, this.company.id, this.tag.id, baseSize, historySize);

			List<Id> baseIds = new List<Id>();
			for (AttShortTimeWorkSettingBase__c settingObj : settingObjList) {
				baseIds.add(settingObj.Id);
			}

			return new AttShortTimeSettingRepository().getEntityList(baseIds, null);
		}
	}

	/**
	 * updateBaseのテスト
	 * ベースエンティティが更新できることを確認する
	 */
	@isTest static void updateBaseTest() {
		AttShortTimeSettingServiceTest.TestData testData = new AttShortTimeSettingServiceTest.TestData();
		final Integer baseNum = 1;
 		final Integer historyNum = 1;
		AttShortTimeSettingBaseEntity testSettingBase = testData.createShortSettingList('更新テスト', baseNum, historyNum)[0];
		TagEntity testTag = testData.createTag('更新テストタグ', TagType.SHORTEN_WORK_REASON);

		// 更新内容を設定する
		AttShortTimeSettingBaseEntity updateBase = new AttShortTimeSettingBaseEntity();
		updateBase.setId(testSettingBase.id);
		updateBase.reasonId = testTag.id;

		new AttShortTimeSettingService().updateBase(updateBase);

		// 更新されていることを確認する
		AttShortTimeSettingBaseEntity resBase = new AttShortTimeSettingRepository().getBaseEntity(updateBase.id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(updateBase.reasonId, resBase.reasonId);
	}

	/**
	 * updateBaseのテスト
	 * 更新後の値を設定しない項目は更新されないことを確認する
	 */
	@isTest static void updateBaseTestNotUpdate() {
		AttShortTimeSettingServiceTest.TestData testData = new AttShortTimeSettingServiceTest.TestData();
		final Integer baseNum = 1;
		final Integer historyNum = 1;
		AttShortTimeSettingBaseEntity testSettingBase = testData.createShortSettingList('更新テスト', baseNum, historyNum)[0];
		TagEntity testTag = testData.createTag('更新テストタグ', TagType.SHORTEN_WORK_REASON);

		// 更新内容を設定する
		// 何も更新しないのでIDのみ設定
		AttShortTimeSettingBaseEntity updateBase = new AttShortTimeSettingBaseEntity();
		updateBase.setId(testSettingBase.id);

		new AttShortTimeSettingService().updateBase(updateBase);

		// 更新されていることを確認する
		AttShortTimeSettingBaseEntity resBase = new AttShortTimeSettingRepository().getBaseEntity(updateBase.id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(testSettingBase.reasonId, resBase.reasonId);
	}

	/**
	 * 履歴が改定できることを確認する
	 * 改定内容が伝搬されることを確認する
	 */
	@isTest static void reviseHistoryTest() {
		AttShortTimeSettingRepository repo = new AttShortTimeSettingRepository();
		AttShortTimeSettingServiceTest.TestData testData = new AttShortTimeSettingServiceTest.TestData();

		final Integer baseNum = 1;
 		final Integer historyNum = 5;
		AttShortTimeSettingBaseEntity base = testData.createShortSettingList('伝搬テスト', baseNum, historyNum)[0];

		// 念のため有効期間を明示的に設定しておく
		AppDate validFrom = AppDate.today();
		AppDate validTo = validFrom.addMonths(1);
		for (Integer i = 0; i < historyNum; i++) {
			base.getHistory(i).nameL2 = '伝搬テスト'; // 伝搬されるように全て同じ名前
			base.getHistory(i).validFrom = validFrom;
			base.getHistory(i).validTo = validTo;
			validFrom = validTo;
			validTo = validFrom.addMonths(1);
		}
		repo.saveHistoryEntityList(base.getHistoryList());
		base = repo.getEntity(base.Id);

		// 改定前履歴
		AttShortTimeSettingHistoryEntity prevHistory = base.getHistory(1);

		validFrom = prevHistory.validFrom.addDays(10);
		validTo = validFrom.addDays(10);
		AttShortTimeSettingHistoryEntity targetHistory = new AttShortTimeSettingHistoryEntity();
		targetHistory.baseId = base.id;
		targetHistory.validFrom = validFrom;
		targetHistory.validTo = validTo;
		targetHistory.historyComment = '改定履歴コメント'; // 履歴管理項目は伝搬されないはず
		targetHistory.nameL0 = prevHistory.nameL0 + '_改定後';
		targetHistory.nameL2 = prevHistory.nameL2 + '_改定後';
		targetHistory.allowableTimeOfEarlyLeave = prevHistory.allowableTimeOfEarlyLeave + 1;

		// 末尾の履歴のName_L2を変更しておく
		base.getHistory(historyNum - 1).nameL2 = targetHistory.nameL2 + '_改定前に変更';
		repo.saveHistoryEntity(base.getHistory(historyNum - 1));

		// 改定実行
		AttShortTimeSettingService service = new AttShortTimeSettingService();
		service.reviseHistory(targetHistory);

		// resHistoryList[0] : 伝搬対象外
		// resHistoryList[1] : 改定前履歴
		// resHistoryList[2] : 改定後履歴
		// resHistoryList[3] : 伝搬対象
		// resHistoryList[4] : 伝搬対象 nameL2は変更しているため伝搬されない
		// resHistoryList[5] : 伝搬対象
		List<AttShortTimeSettingHistoryEntity> resHistoryList = repo.getEntity(base.id).getHistoryList();
		System.assertEquals(historyNum + 1, resHistoryList.size());
		// 改定されていることを確認
		System.assertEquals(validFrom, resHistoryList[1].validTo); // 改定前履歴
		System.assertEquals(validFrom, resHistoryList[2].validFrom); // 改定後履歴

		// デバッグ
		System.debug('>>> 改定後の履歴');
		for (AttShortTimeSettingHistoryEntity history : resHistoryList) {
			System.debug(history.name + '(' + history.validFrom + '-' + history.validTo + '): nameL0 = ' + history.nameL0 + ', allowableTimeOfEarlyLeave =' + history.allowableTimeOfEarlyLeave);
		}

		// 伝搬されていることを確認
		// 末尾の履歴のnameL2はすでに変更されているので伝搬対象外
		for (Integer i = 3; i < historyNum; i++) {
			// System.assertEquals(targetHistory.nameL0, resHistoryList[i].nameL0, 'resHistoryList[' + i + '].nameL0');
			System.assertEquals(targetHistory.nameL2, resHistoryList[i].nameL2, 'resHistoryList[' + i + '].nameL2');
			System.assertEquals(targetHistory.allowableTimeOfEarlyLeave, resHistoryList[i].allowableTimeOfEarlyLeave);

			// 履歴管理項目は伝搬されない
			System.assertEquals(base.getHistory(i - 1).historyComment, resHistoryList[i].historyComment);

		}

		// 改定前の履歴には伝搬されていない
		for (Integer i = 0; i <= 1; i++) {
			System.assertEquals(base.getHistory(i).nameL2, resHistoryList[i].nameL2);
			System.assertEquals(base.getHistory(i).allowableTimeOfEarlyLeave, resHistoryList[i].allowableTimeOfEarlyLeave);
		}

		// 末尾の履歴の短時間勤務設定名L2は既に変更されているので伝搬されない
		AttShortTimeSettingHistoryEntity resTailHistory = resHistoryList[resHistoryList.size() - 1];
		System.assertEquals(base.getHistory(historyNum - 1).nameL2, resTailHistory.nameL2);
		// allowableTimeOfEarlyLeaveは伝搬されている
		System.assertEquals(targetHistory.allowableTimeOfEarlyLeave, resTailHistory.allowableTimeOfEarlyLeave);
		// 短時間勤務設定名L1の値は変更されていない
		System.assertEquals(base.getHistory(historyNum - 1).nameL1, resTailHistory.nameL1);

		// 伝搬対象の履歴の元の履歴は論理削除されている
		List<AttShortTimeWorkSettingHistory__c> removedList = [
				SELECT Id, ValidFrom__c
				FROM AttShortTimeWorkSettingHistory__c
				WHERE BaseId__c = :base.Id AND Removed__c = true];
		System.assertEquals(historyNum - 2, removedList.size());

	}

	/**
	 * 短時間勤務設定ベースのコードの重複チェックが正しくできることを確認する
	 */
	@isTest static void isCodeDuplicatedTest() {
		AttShortTimeSettingServiceTest.TestData testData = new AttShortTimeSettingServiceTest.TestData();

		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<AttShortTimeSettingBaseEntity> bases = testData.createShortSettingList('テスト', baseNum, historyNum);
		AttShortTimeSettingBaseEntity base0 = bases[0];
		AttShortTimeSettingBaseEntity targetBase = bases[1];

		AttShortTimeSettingService service = new AttShortTimeSettingService();

		// 会社内で重複している → true
		targetBase.code = base0.code;
		System.assertEquals(true, service.isCodeDuplicated(targetBase));

		// 会社をまたいで重複している → false
		CompanyEntity ompany2 = testData.createCompany('Test2');
		targetBase.companyId = ompany2.id;
		targetBase.code = base0.code;
		System.assertEquals(false, service.isCodeDuplicated(targetBase));

		// 組織全体でも重複していない → false
		targetBase.code = targetBase.code + 'Test';
		System.assertEquals(false, service.isCodeDuplicated(targetBase));
	}
}
