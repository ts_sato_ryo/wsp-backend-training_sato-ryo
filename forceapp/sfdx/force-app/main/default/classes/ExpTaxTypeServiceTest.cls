/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分サービステスト Test class for ExpTaxTypeService
 */
@isTest
private class ExpTaxTypeServiceTest {

	/** メッセージ Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') Organization setting */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト Country */
		public ComCountry__c countryObj;
		/** 会社オブジェクト Companry */
		public ComCompany__c companyObj {get; private set;}

		/** コンストラクタ Constructor */
		public TestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		}

		/**
		 * テスト用の税区分ベースエンティティを作成する
		 */
		public ExpTaxTypeBaseEntity createTaxTypeEntity(String name, String code) {
			ExpTaxTypeBaseEntity baseEntity = new ExpTaxTypeBaseEntity();
			baseEntity.name = name;
			baseEntity.code = code;
			baseEntity.countryId = null;
			baseEntity.companyId = companyObj.id;
			baseEntity.addHistory(createTaxTypeHistoryEntity(name + '1', code, AppDate.newInstance(2017, 12, 15), AppDate.newInstance(2018, 4, 1)));
			baseEntity.addHistory(createTaxTypeHistoryEntity(name + '2', code, AppDate.newInstance(2018, 4, 1), AppDate.newInstance(2019, 1, 1)));

			return baseEntity;
		}

		/**
		 * テスト用の税区分履歴エンティティを作成する
		 */
		public ExpTaxTypeHistoryEntity createTaxTypeHistoryEntity(String name, String code, AppDate validFrom, AppDate validTo) {
			ExpTaxTypeHistoryEntity historyEntity = new ExpTaxTypeHistoryEntity();
			historyEntity.name = name;
			historyEntity.nameL0 = name + '_L0';
			historyEntity.nameL1 = name + '_L1';
			historyEntity.nameL2 = name + '_L2';
			historyEntity.rate = 8.0;
			historyEntity.historyComment = '改訂コメント';
			historyEntity.validFrom = validFrom;
			historyEntity.validTo = validTo;
			historyEntity.isRemoved = false;
			historyEntity.uniqKey = historyEntity.createUniqKey(code);

			return historyEntity;
		}

		/**
		 * 税区分ベースエンティティを作成し、登録する
		 */
		public List<ExpTaxTypeBaseEntity> createTaxTypeBaseEntityList(String name, Integer baseSize, Integer historySize) {
			List<ExpTaxTypeBase__c> taxTypeList = createTaxTypesWithHistory(name, baseSize, historySize);
			List<Id> baseIdList = new List<Id>();
			for (ExpTaxTypeBase__c taxType : taxTypeList) {
				baseIdList.add(taxType.Id);
			}

			return (new ExpTaxTypeRepository()).getEntityList(baseIdList, null);
		}

		/**
		 * 税区分のベースと履歴のオブジェクトを作成する
		 * @pame 税区分名
		 * @param baseSize 作成するベースの数
		 * @param historySize 各ベースに対して作成する履歴オブジェクトの数
		 */
		public List<ExpTaxTypeBase__c> createTaxTypesWithHistory(String name, Integer baseSize, Integer historySize) {
			List<ExpTaxTypeBase__c> taxTypeList =  ComTestDataUtility.createTaxTypesWithHistory(name, this.countryObj.Id, this.companyObj.Id, baseSize, historySize);

			return taxTypeList;
		}
	}

	/**
	 * 履歴が改定できることを確認する
	 * 改定内容が伝搬されることを確認する
	 */
	@isTest static void reviseHistoryTest() {
		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeServiceTest.TestData testData = new TestData();

		final Integer historyNum = 5;
		ExpTaxTypeBaseEntity base = testData.createTaxTypeBaseEntityList('Test', 1, historyNum).get(0);

		// 全履歴の税率を同じにしておく
		for (ExpTaxTypeHistoryEntity history : base.getHistoryList()) {
			history.rate = 8.0;
		}
		repo.saveEntity(base);

		AppDate validFrom = base.getHistoryList().get(1).validFrom.addDays(30);
		AppDate validTo = validFrom.addDays(10);
		ExpTaxTypeHistoryEntity targetHistory = testData.createTaxTypeHistoryEntity('改定テスト', base.code, validFrom, validTo);
		targetHistory.baseId = base.id;
		targetHistory.rate = 10.0;
		targetHistory.historyComment = '改定履歴コメント'; // 履歴管理項目は伝搬されないはず

		// 末尾の履歴の税率を変更しておく
		base.getHistoryList().get(historyNum - 1).rate = 12.0;
		repo.saveHistoryEntity(base.getHistoryList().get(historyNum - 1));

		Test.startTest();

		// 改定実行
		ExpTaxTypeService service = new ExpTaxTypeService();
		service.reviseHistory(targetHistory);

		Test.stopTest();

		List<ExpTaxTypeHistoryEntity> resHistoryList = repo.getHistoryEntityByBaseId(base.id);
		System.assertEquals(historyNum + 1, resHistoryList.size());
		// 改定されていることを確認
		System.assertEquals(validFrom, resHistoryList[1].validTo); // 改定前履歴
		System.assertEquals(validFrom, resHistoryList[2].validFrom); // 改定後履歴

		// 伝搬されていることを確認
		// 末尾の履歴の管理者はすでに変更されているので伝搬対象外
		for (Integer i = 3; i < historyNum; i++) {
			System.assertEquals(targetHistory.nameL0, resHistoryList[i].nameL0);
			System.assertEquals(targetHistory.nameL1, resHistoryList[i].nameL1);
			System.assertEquals(targetHistory.nameL2, resHistoryList[i].nameL2);
			System.assertEquals(targetHistory.rate, resHistoryList[i].rate);

			// 履歴管理項目は伝搬されない
			System.assertEquals(base.getHistoryList().get(i - 1).historyComment, resHistoryList[i].historyComment);
		}

		// 改定前の履歴には伝搬されていない
		for (Integer i = 0; i <= 1; i++) {
			System.assertEquals(base.getHistoryList().get(i).rate, resHistoryList[i].rate);
		}

		// 末尾の履歴の管理者はすでに変更されているので伝搬されない
		Integer tailIdx = resHistoryList.size() - 1;
		System.assertEquals(base.getHistoryList().get(base.getHistoryList().size() - 1).rate, resHistoryList[tailIdx].rate);
		// 名前は伝搬されている
		System.assertEquals(targetHistory.nameL0, resHistoryList[tailIdx].nameL0);
		System.assertEquals(targetHistory.nameL1, resHistoryList[tailIdx].nameL1);
		System.assertEquals(targetHistory.nameL2, resHistoryList[tailIdx].nameL2);

		// 伝搬対象の履歴の元の履歴は論理削除されている
		List<ExpTaxTypeHistory__c> removedList = [SELECT Id, ValidFrom__c FROM ExpTaxTypeHistory__c WHERE BaseId__c = :base.Id AND Removed__c = true];
		System.assertEquals(historyNum - 2, removedList.size());
	}

	/**
	 * 税区分ベースのコード重複チェックが正しくできることを確認する
	 */
	@isTest static void isCodeDuplicatedTest() {
		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeServiceTest.TestData testData = new TestData();

		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<ExpTaxTypeBaseEntity> bases = testData.createTaxTypeBaseEntityList('Test', baseNum, historyNum);
		ExpTaxTypeBaseEntity targetBase = bases[1];

		ExpTaxTypeService service = new ExpTaxTypeService();

		// 会社内で重複している → true
		targetBase.code = bases[0].code;
		System.assertEquals(true, service.isCodeDuplicated(targetBase));

		// 会社をまたいで重複している → false
		ComCompany__c ompanyObj2 = ComTestDataUtility.createCompany('Test2', testData.countryObj.Id);
		targetBase.companyId = ompanyObj2.Id;
		targetBase.code = bases[0].code;
		System.assertEquals(false, service.isCodeDuplicated(targetBase));

		// 組織全体でも重複していない → false
		targetBase.code = targetBase.code + 'Test';
		System.assertEquals(false, service.isCodeDuplicated(targetBase));
	}

	/**
	 * updateUniqKeyのテスト Test for updateUniqKey method
	 * 国の税区分の場合、ユニークキーが正しく設定されることを確認する
	 * In case of country tax type, check that valid unique key is set
	 */
	@isTest static void updateUniqKeyTestCountry() {
		final Integer testTypeSize = 3;
		ExpTaxTypeServiceTest.TestData testData = new TestData();

		ExpTaxTypeBaseEntity targetType = testData.createTaxTypeBaseEntityList('Test', testTypeSize, 1).get(0);

		// 更新対象のエンティティ entity for update
		ExpTaxTypeBaseEntity updateType = new ExpTaxTypeBaseEntity();
		updateType.setId(targetType.id);

		Test.startTest();

		(new ExpTaxTypeService()).updateUniqKey(updateType);

		Test.stopTest();

		// ユニークキーはレコードID Unique key is record ID
		System.assertEquals((String)targetType.id, updateType.uniqKey);
	}

	/**
	 * updateUniqKeyのテスト Test for updateUniqKey method
	 * 会社の税区分の場合、ユニークキーが正しく設定されることを確認する
	 * In case of company tax type, check that valid unique key is set
	 */
	@isTest static void updateUniqKeyTestCompany() {
		final Integer testTypeSize = 3;
		ExpTaxTypeServiceTest.TestData testData = new TestData();

		ExpTaxTypeBaseEntity targetType = testData.createTaxTypeBaseEntityList('Test', testTypeSize, 1).get(0);
		targetType.countryId = null;
		new ExpTaxTypeRepository().saveEntity(targetType);

		// 更新対象のエンティティ entity for update
		ExpTaxTypeBaseEntity updateType = new ExpTaxTypeBaseEntity();
		updateType.setId(targetType.id);
		updateType.code = targetType.code + 'Update';

		Test.startTest();

		(new ExpTaxTypeService()).updateUniqKey(updateType);

		Test.stopTest();

		// ユニークキーは会社コード＋税区分コード Uniqe key is company code and tax type code
		System.assertEquals(testData.companyObj.Code__c + '-' + updateType.code, updateType.uniqKey);
	}

}