/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * 欠勤申請のサービスクラス
  * 当クラスはAttAttDailyRequestServiceからのみ実行する事を前提としています。
  * Resourceクラスなどから直接実行しないで下さい。
  */
public with sharing class AttAbsenceRequestService extends AttDailyRequestService.AbstractRequestProcessor {

	/** 申請可能な最大日数 */
	private static final Integer REQUEST_PERIOD_MAX = 31;

	private final AttDailyRequestRepository dailyRequestRepository = new AttDailyRequestRepository();

	private final AttAttendanceService attService = new AttAttendanceService();

	/**
	 * 申請・再申請パラメータ
	 */
	public class SubmitParam extends AttDailyRequestService.SubmitParam {
		/** 開始日 */
		public AppDate startDate;
		/** 終了日 */
		public AppDate endDate;
		/** 理由 */
		public String reason;
	}

	/**
	 * 欠勤申請の申請/再申請を行う。
	 *
	 * @param param 申請情報
	 * @return 作成した申請情報
	 */
	public override AttDailyRequestEntity submit(AttDailyRequestService.SubmitParam submitParam) {
		SubmitParam param = (SubmitParam)submitParam;

		// 申請期間のチェック
		// 申請期間が長すぎるとSOQLガバナ例外やexcludingDateの文字数が長すぎてDML例外が発生してしまうため
		if (param.startDate != null && param.endDate != null) {
			if (param.startDate.daysBetween(param.endDate) >= REQUEST_PERIOD_MAX) {
				throw new App.ParameterException(
						ComMessage.msg().Att_Err_RequesePeriodTooLong(new List<String>{REQUEST_PERIOD_MAX.format()}));
			}
		}

		// 再申請の場合は削除する
		if (param.requestId != null) {
			super.remove(dailyRequestRepository.getEntity(param.requestId));
		}

		// 申請対象となる勤怠明細レコードとサマリーを取得する
		AttDailyRequestService.RecordsWithSummary recordsWithSummary = getRecordEntityList(param.empId, param.startDate, param.endDate);

		// 欠勤申請を使用可能か判定
		if (!isValidAbsenceRequest(recordsWithSummary, param.startDate, param.endDate)) {
			throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_ABSENCE_REQUEST, ComMessage.msg().Att_Err_NotPermittedAbsenceRequest);
		}

		// 申請エンティティを作成する
		AttDailyRequestEntity request = createAbsenceRequest(param, recordsWithSummary);
		AttValidator.DailyRequestValidator requestValidator = new AttValidator.DailyRequestValidator(request);
		Validator.Result requestValidatorResult = requestValidator.validate();
		if (!requestValidatorResult.isSuccess()) {
			Validator.Error error = requestValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 申請を保存する
		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();
		Repository.SaveResult saveRes = requestRepo.saveEntity(request);
		request.setId(saveRes.details[0].id);

		// 勤務日を含まない場合はエラー
		Boolean includeWorkday = getExceptWorkDay(recordsWithSummary.records).size() < recordsWithSummary.records.size();
		if (!includeWorkday) {
			throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_ABSENCE_REQUEST, ComMessage.msg().Att_Err_NotWorkDay);
		}

		// 勤怠明細の欠勤申請IDを更新する（勤務日のみ）
		for (AttRecordEntity record : recordsWithSummary.records) {
			if (request.excludingDateList.contains(record.rcdDate)) {
				continue;
			}
			// 勤怠明細のバリデーション
			AttValidator.AbsenceSubmitSubmitValidator validator = new AttValidator.AbsenceSubmitSubmitValidator(record, request);
			Validator.Result validatorResult = validator.validate();
			if (!validatorResult.isSuccess()) {
				Validator.Error error = validatorResult.getErrors()[0];
				throw new App.IllegalStateException(error.code, error.message);
			}
			record.reqRequestingAbsenceRequestId = request.id;
		}

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		summaryRepo.saveRecordEntityList(recordsWithSummary.records);

		// 申請する(申請コメントなし)
		submitProcess(request, null);
		return request;
	}

	/**
	 * 欠勤申請のエンティティを作成する
	 * @param param 申請情報
	 * @param recordsWithSummary 申請対象の勤怠明細
	 * @return エンティティ
	 */
	private AttDailyRequestEntity createAbsenceRequest(SubmitParam param, AttDailyRequestService.RecordsWithSummary recordsWithSummary) {
		// 社員を取得
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity employee = empService.getEmployee(param.empId, param.startDate);

		// エンティティを生成
		AttDailyRequestEntity entity = new AttDailyRequestEntity();
		entity.setId(param.requestId);
		entity.name = createRequestName(param, employee, getCompanyLanguage(employee.companyId));
		entity.requestType = AttRequestType.ABSENCE;
		entity.startDate = param.startDate;
		entity.endDate = param.endDate;
		entity.reason = param.reason;
		entity.status = AppRequestStatus.PENDING;
		entity.cancelType = AppCancelType.NONE;
		entity.isConfirmationRequired = false;
		entity.excludingDateList = getExceptWorkDay(recordsWithSummary.records);
		entity.requestTime = AppDatetime.now();
		setRequestCommInfo(entity, employee);
		return entity;
	}

	/**
	 * 会社の言語に合わせた欠勤申請名称を作成する。
	 * 日本語の場合：{氏名} さん 欠勤申請 {開始日}–{終了日}
	 * 日本語以外の場合：{氏名} Absence Request {開始日}–{終了日}
	 * @param param 申請情報
	 * @param employee 社員情報
	 * @return 欠勤申請名
	 */
	@TestVisible
	private String createRequestName(SubmitParam param, EmployeeBaseEntity employee, AppLanguage lang) {
		// 日付フォーマット
		final AppDate.FormatType dateFormat = AppDate.FormatType.YYYYMMDD;

		// バインド文字列
		List<String> args = new List<String>{
			employee.displayNameL.getValue(lang),
			param.startDate.format(dateFormat),
			param.endDate.format(dateFormat)};

		// 申請種類名
		ComMsgBase targetLang = ComMessage.msg(lang.value);
		return lang == AppLanguage.JA ?
			String.format('{0}さん ' + targetLang.Att_Lbl_RequestTypeAbsence + targetLang.Att_Lbl_Request + ' {1}-{2}', args) :
			String.format('{0} ' + targetLang.Att_Lbl_RequestTypeAbsence + ' ' + targetLang.Att_Lbl_Request + ' {1}-{2}', args);
	}

	/**
	 * 勤務日以外の日付を取得する
	 * @param 判定対象の勤怠明細リスト
	 * @return 勤務日以外の日付のリスト
	 */
	@TestVisible
	private List<AppDate> getExceptWorkDay(List<AttRecordEntity> records) {
		List<AppDate> exceptWorkDays = new List<AppDate>();
		for (AttRecordEntity record : records) {
			if (record.outDayType != AttDayType.WORKDAY) {
				exceptWorkDays.add(record.rcdDate);
			}
		}
		return exceptWorkDays;
	}

	/**
	 * 指定の期間で欠勤申請を使用可能か、勤務体系マスタから判定する。
	 * @param summaries 勤怠サマリ
	 * @return 判定結果 期間全てにおいて、欠勤申請が可能な場合true
	 */
	private Boolean isValidAbsenceRequest(AttDailyRequestService.RecordsWithSummary summaries, AppDate startDate, AppDate endDate) {
		// 勤務体系を取得
		AttWorkingTypeRepository.SearchFilter filter = new AttWorkingTypeRepository.SearchFilter();
		filter.baseIds = new Set<Id>();
		for (AttSummaryEntity summary : summaries.summaryMap.values()) {
			filter.baseIds.add(summary.workingTypeBaseId);
		}
		List<AttWorkingTypeBaseEntity> workingTypes = new AttWorkingTypeRepository().searchEntity(filter);

		// 申請日に紐づく勤務体系履歴を判定
		for (AttRecordEntity attRecord : summaries.records) {
			AttWorkingTypeHistoryEntity history = getWorkingTypeHistory(workingTypes, attRecord.rcdDate);
			if (history == null || !history.useAbsenceApply) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 指定した日付の勤務体系履歴を取得する。
	 * @param workingTypes 勤務体系ベース
	 * @param targetDate 対象日
	 * @param 指定日に対応する勤務体系履歴（存在しない場合はnull）
	 */
	private AttWorkingTypeHistoryEntity getWorkingTypeHistory(List<AttWorkingTypeBaseEntity> workingTypes, AppDate targetDate) {
		for (AttWorkingTypeBaseEntity base : workingTypes) {
			AttWorkingTypeHistoryEntity history = base.getHistoryByDate(targetDate);
			if (history != null) {
				return history;
			}
		}
		return null;
	}

	/**
	 * @description 承認取消時のメールを作成する
	 * @param request 申請
	 * @param applicant 申請者
	 * @param empLastModified 最終更新者
	 * @param targetLanguage 送信先ユーザの言語
	 * @return mailMessage メール内容
	 */
	public override MailService.MailParam createCancelMailMessage(AttDailyRequestEntity request, EmployeeBaseEntity applicant, EmployeeBaseEntity empLastModified, AppLanguage targetLanguage) {

		MailService.MailParam mailMessage = new MailService.MailParam();
		String language = targetLanguage.value;

		// 送信者名
		String senderDisplayName = empLastModified.fullNameL.getFullName(targetLanguage);
		mailMessage.senderDisplayName = senderDisplayName;

		// メールタイトル
		String mailSubject;
		// 申請種別を文字列に変換する
		String requestType = ComMessage.msg(language).Att_Lbl_RequestTypeAbsence;
		mailSubject = ComMessage.msg(language).Att_Msg_CancelMailSubject(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType});
		mailMessage.subject = mailSubject;

		// メール本文
		String mailBody;
		// 申請日付
		String requestDate = request.requestTime.formatLocal().substring(0, 10).replace('-', '/');
		// 開始日
		String startDate = String.valueOf(AppDate.convertDate(request.startDate)).replace('-', '/');
		// 終了日
		String endDate = String.valueOf(AppDate.convertDate(request.endDate)).replace('-', '/');
		// 理由
		String reason = request.reason;
		// メール本文を作成
		mailBody = ComMessage.msg(language).Att_Msg_CancelMailMessage(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType, empLastModified.fullNameL.getFullName(targetLanguage)});
		mailBody += '\r\n\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_RequestContents + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestDate + '：' + requestDate + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestType + '：' + requestType + '\r\n';
		// 期間での申請の場合は、開始日と終了日を追加
		if (startDate.equals(endDate)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate;
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate + '－' + endDate;
		}
		mailBody += '\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_Reason + '：' + reason + '\r\n';
		// 署名
		mailBody += '\r\n---\r\n';
		mailBody += ComMessage.msg(language).Att_Msg_TeamSpirit;
		mailMessage.body = mailBody;

		return mailMessage;
	}
}