/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description CountryResourceのテストクラス
 */
@isTest
private class CountryResourceTest {

	/**
	 * 国レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		Test.startTest();

		CountryResource.Country param = new CountryResource.Country();
		param.name_L0 = '国_L0';
		param.name_L1 = '国_L1';
		param.name_L2 = '国_L2';
		param.code = '001';

		CountryResource.CreateApi api = new CountryResource.CreateApi();
		CountryResource.SaveResult res = (CountryResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 国レコードが1件作成されること
		List<ComCountry__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c
			FROM ComCountry__c];

		System.assertEquals(1, newRecords.size());

		ComCountry__c newRecord = newRecords[0];
		System.assertEquals(newRecord.Id, res.id);
		System.assertEquals(param.name_L0, newRecord.Name);
		System.assertEquals(param.name_L0, newRecord.Name_L0__c);
		System.assertEquals(param.name_L1, newRecord.Name_L1__c);
		System.assertEquals(param.name_L2, newRecord.Name_L2__c);
		System.assertEquals(param.code, newRecord.Code__c);
	}

	/**
	 * 国レコードを1件作成する(異常系:必須パラメータ(コード)が未設定)
	 */
	@isTest
	static void creativeNegativeTestCodeNull() {

		Test.startTest();

		CountryResource.Country param = new CountryResource.Country();
		CountryResource.CreateApi api = new CountryResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{ComMessage.msg().Com_Lbl_Code}), ex.getMessage());
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 国レコードを1件作成する(異常系:App.NoPermissionException発生)
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void creativeNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		CountryResource.Country param = new CountryResource.Country();
		param.name_L0 = '国_L0';
		param.name_L1 = '国_L1';
		param.name_L2 = '国_L2';
		param.code = '001';

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				CountryResource.CreateApi api = new CountryResource.CreateApi();
				CountryResource.SaveResult res = (CountryResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 国レコードを1件作成する(異常系:必須パラメータ(国名)が未設定)
	 */
	@isTest
	static void creativeNegativeTestNameNull() {

		Test.startTest();

		CountryResource.Country param = new CountryResource.Country();
		CountryResource.CreateApi api = new CountryResource.CreateApi();
		App.ParameterException ex;

		param.code = 'Japan';
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{ComMessage.msg().Admin_Lbl_Name}), ex.getMessage());
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 国レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		ComCountry__c country = ComTestDataUtility.createCountry('テスト国');

		Test.startTest();

		// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
		// 全パラメータの検証は createPositiveTest() で行っている
		Map<String, Object> param = new Map<String, Object> {
			'id' => country.id,
			'code' => '002',
			'name_L0' => 'name'
		};

		CountryResource.UpdateApi api = new CountryResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// レコードが更新されていること
		List<ComCountry__c> wcList = [SELECT Code__c, Name_L0__c FROM ComCountry__c WHERE Id =:(String)param.get('id')];
		System.assertEquals(1, wcList.size());
		System.assertEquals((String)param.get('code'), wcList[0].Code__c);
		System.assertEquals((String)param.get('name_L0'), wcList[0].Name_L0__c);

	}

	/**
	 * 国レコードを1件更新する(異常系:App.NoPermissionException発生)
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		Map<String, Object> param = new Map<String, Object> {
			'id' => testData.country.id,
			'code' => '002',
			'name_L0' => 'name'
		};

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				CountryResource.UpdateApi api = new CountryResource.UpdateApi();
				Object res = api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 国レコードを1件更新する(異常系:必須パラメータ(ID)が未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		Test.startTest();

		CountryResource.Country param = new CountryResource.Country();

		App.ParameterException ex;
		try {
			Object res = (new CountryResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'id'}), ex.getMessage());
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 国レコードを1件更新する(異常系:必須パラメータ(ID)が不正)
	 */
	@isTest
	static void updateNegativeTestInvalidId() {
		Test.startTest();

		CountryResource.Country param = new CountryResource.Country();
		param.id = 'Invalid';

		App.ParameterException ex;
		try {
			Object res = (new CountryResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'id', String.valueOf(param.id)}), ex.getMessage());
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 国レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		ComCountry__c oldRecord = ComTestDataUtility.createCountry('テスト国');

		Test.startTest();

		CountryResource.DeleteOption param = new CountryResource.DeleteOption();
		param.id = oldRecord.id;

		Object res = (new CountryResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 国レコードが削除されること
		System.assertEquals(0, [SELECT Count() FROM ComCompany__c]);
	}

	/**
	 * 国レコードを1件削除する(異常系:App.NoPermissionException発生)
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		ComCountry__c oldRecord = ComTestDataUtility.createCountry('テスト国');

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		CountryResource.DeleteOption param = new CountryResource.DeleteOption();
		param.id = oldRecord.id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Object res = (new CountryResource.DeleteApi()).execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 国レコードを1件削除する(異常系:必須パラメータ(ID)が未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		CountryResource.DeleteOption param = new CountryResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new CountryResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'id'}), ex.getMessage());
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 国レコードを1件削除する(異常系:必須パラメータ(ID)が不正)
	 */
	@isTest
	static void deleteRequiredIdTestInvalidId() {

		Test.startTest();

		CountryResource.DeleteOption param = new CountryResource.DeleteOption();
		param.id = 'Invalid';

		App.ParameterException ex;
		try {
			Object res = (new CountryResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'id', String.valueOf(param.id)}), ex.getMessage());
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 国レコードを1件削除する(異常系:DMLException発生)
	 * TODO: 標準ユーザから国レコードが削除できてしまいテストが失敗するため(原因不明)、保留
	 */
	@isTest
	static void deleteNegativeTest() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		ComCountry__c oldRecord = ComTestDataUtility.createCountry('テスト国');

		Test.startTest();

		CountryResource.DeleteOption param = new CountryResource.DeleteOption();
		param.Id = oldRecord.Id;

		DmlException ex;

		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)){
			try {
				Object res = (new CountryResource.DeleteApi()).execute(param);
			} catch (DmlException e) {
				ex = e;
			}
		}

		Test.stopTest();

		System.assertEquals('INSUFFICIENT_ACCESS_OR_READONLY', ex.getDmlStatusCode(0));
	}

	/**
	 * 国レコードをレコードIDで検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {

		ComCountry__c testRecord = ComTestDataUtility.createCountry('テスト国');

		Test.startTest();

		CountryResource.SearchCondition param = new CountryResource.SearchCondition();
		param.id = testRecord.id;

		CountryResource.SearchApi api = new CountryResource.SearchApi();
		CountryResource.SearchResult res = (CountryResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 国レコードが取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());

		CountryResource.Country result = res.records[0];
		System.assertEquals(testRecord.Id, result.id);
		// ID以外の項目値の検証はsearchAllPositiveTest()で行っているため省略

	}

	/**
	 * 国レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCountry__c testRecord = ComTestDataUtility.createCountry('テスト国');

		Test.startTest();

		CountryResource.SearchCondition param = new CountryResource.SearchCondition();
		param.id = testRecord.id;

		CountryResource.SearchApi api = new CountryResource.SearchApi();
		CountryResource.SearchResult res = (CountryResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 国レコードが取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// 多言語対応項目の検証
		System.assertEquals(testRecord.Name_L1__c, res.records[0].name);
	}

	/**
	 * 国レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCountry__c testRecord = ComTestDataUtility.createCountry('テスト国');

		Test.startTest();

		CountryResource.SearchCondition param = new CountryResource.SearchCondition();
		param.id = testRecord.id;

		CountryResource.SearchApi api = new CountryResource.SearchApi();
		CountryResource.SearchResult res = (CountryResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 国レコードが取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// 多言語対応項目の検証
		System.assertEquals(testRecord.Name_L2__c, res.records[0].name);
	}

	/**
	 * 国レコードを全件検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		List<ComCountry__c> testRecords = ComTestDataUtility.createCountries('テスト国', 3);
		// 国コード順で再取得
		testRecords = [SELECT Id, Name_L0__c, Name_L1__c, Name_L2__c, Code__c
			FROM ComCountry__c
			ORDER BY Code__c NULLS LAST];

		Test.startTest();

		Map<String, Object> param = new Map<String, Object>();
		CountryResource.SearchApi api = new CountryResource.SearchApi();
		CountryResource.SearchResult res = (CountryResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 国レコードがすべて取得できること
		System.assertEquals(testRecords.size(), res.records.size());

		for (Integer i = 0, n = testRecords.size(); i < n; i++) {
			System.assertEquals(testRecords[i].Id, res.records[i].id);
			System.assertEquals(testRecords[i].Name_L0__c, res.records[i].name);
			System.assertEquals(testRecords[i].Name_L0__c, res.records[i].name_L0);
			System.assertEquals(testRecords[i].Name_L1__c, res.records[i].name_L1);
			System.assertEquals(testRecords[i].Name_L2__c, res.records[i].name_L2);
			System.assertEquals(testRecords[i].Code__c, res.records[i].code);
		}

	}

	/**
	 * コードを重複して登録する(作成時)
	 */
	@isTest
	static void isExistCodeTestForCreateTrue() {

		// データ登録
		List<ComCountry__c> testRecords = ComTestDataUtility.createCountries('テスト国', 3);

		// 実行
		Test.startTest();

		CountryEntity entity = new CountryEntity();
		entity.code = testRecords[2].Code__c;

		Boolean result = CountryResource.isExistCode(entity, false);

		Test.stopTest();

		// 検証
		System.assert(result);
	}

	/**
	 * コードを重複せずに登録する(作成時)
	 */
	@isTest
	static void isExistCodeTestForCreatefalse() {

		// データ登録
		List<ComCountry__c> testRecords = ComTestDataUtility.createCountries('テスト国', 3);

		// 実行
		Test.startTest();

		CountryEntity entity = new CountryEntity();
		entity.code = '001';

		Boolean result = CountryResource.isExistCode(entity, false);

		Test.stopTest();

		// 検証
		System.assert(!result);
	}

	/**
	 * コードを重複して登録する(更新時・IDは異なる)
	 */
	@isTest
	static void isExistCodeTestForUpdateTrue() {

		// データ登録
		List<ComCountry__c> testRecords = ComTestDataUtility.createCountries('テスト国', 3);

		// 実行
		Test.startTest();

		CountryEntity entity = new CountryEntity();
		entity.code = testRecords[2].Code__c;
		entity.setId(testRecords[1].Id);

		Boolean result = CountryResource.isExistCode(entity, true);

		Test.stopTest();

		// 検証
		System.assert(result);
	}

	/**
	 * コードを重複して登録する(更新時・IDは同じ)
	 */
	@isTest
	static void isExistCodeTestForUpdateTrueSameId() {

		// データ登録
		List<ComCountry__c> testRecords = ComTestDataUtility.createCountries('テスト国', 3);

		// 実行
		Test.startTest();

		CountryEntity entity = new CountryEntity();
		entity.code = testRecords[2].Code__c;
		entity.setId(testRecords[2].Id);

		Boolean result = CountryResource.isExistCode(entity, true);

		Test.stopTest();

		// 検証
		System.assert(!result);
	}

	/**
	 * コードを重複せずに登録する(更新時)
	 */
	@isTest
	static void isExistCodeTestForUpdatefalse() {

		// データ登録
		List<ComCountry__c> testRecords = ComTestDataUtility.createCountries('テスト国', 3);

		// 実行
		Test.startTest();

		CountryEntity entity = new CountryEntity();
		entity.code = '001';
		entity.setId(testRecords[1].Id);

		Boolean result = CountryResource.isExistCode(entity, true);

		Test.stopTest();

		// 検証
		System.assert(!result);
	}

	/**
	 * エンティティのバリデーションチェックを通ることを検証する
	 */
	@isTest
	static void passValidateCheck() {

		Test.startTest();

		App.ParameterException ex;
		CountryResource.Country param = new CountryResource.Country();
		param.name_L0 = '国_L0';
		param.code = '123456789123456789123';

		CountryResource.CreateApi api = new CountryResource.CreateApi();
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// 検証
		System.assertEquals(ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{ComMessage.msg().Com_Lbl_Code, CountryEntity.CODE_MAX_LENGTH.format()}), ex.getMessage());
		System.assertEquals('INVALID_VALUE', ex.getErrorCode());
	}
}
