/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description ExpReportTypeRepositoryのテスト Test class for ExpReportTypeRepository
 */
@isTest
private class ExpReportTypeRepositoryTest {

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/** テストデータ Test Data */
	private class RepoTestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;
		/** 拡張項目オブジェクト */
		public List<ComExtendedItem__c> extendedItemObjs;

		/** 拡張項目(選択リスト)オブジェクト */
		public List<ComExtendedItem__c> extendedItemPicklistObjs;
		public List<ComExtendedItem__c> extendedItemsLookupObjs;

		/** コンストラクタ Constructor */
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
			extendedItemObjs = ComTestDataUtility.createExtendedItems('Extended Item', this.companyObj.Id, 10);
			extendedItemPicklistObjs = ComTestDataUtility.createExtendedItems('Extended Item Picklist', this.companyObj.id, ExtendedItemInputType.PICKLIST, 10);
			List<ComExtendedItemCustom__c> extendedItemCustoms = ComTestDataUtility.createExtendedItemCustoms('Custom Obj', this.companyObj.id, 20);
			extendedItemsLookupObjs = ComTestDataUtility.createExtendedItemLookups('EILookup', this.companyObj.id, extendedItemCustoms);
		}

		/**
		 * 経費申請タイプオブジェクトを作成する Create Expense Report Type records
		 */
		public List<ExpReportType__c> createExpReportTypes(String name, Integer size) {
			return ComTestDataUtility.createExpReportTypes(name, this.companyObj.Id, size, true);
		}

		/**
		 * 経費申請タイプエンティティを作成する(DB保存なし) Create an Expense Report Type Entity (Do not update to DB)
		 */
		public ExpReportTypeEntity createExpReportTypeEntity(String name) {
			ExpReportTypeEntity entity = new ExpReportTypeEntity();

			entity.active = true;
			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.companyObj.Code__c + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.companyObj.Id;
			entity.costCenterRequiredFor = ComCostCenterRequiredFor.EXPENSE_REPORT;
			entity.costCenterUsedIn = ComCostCenterUsedIn.EXPENSE_REPORT;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.jobRequiredFor = ComJobRequiredFor.EXPENSE_REPORT;
			entity.jobUsedIn = ComJobUsedIn.EXPENSE_REPORT;
			entity.useFileAttachment = true;
			entity.vendorRequiredFor = ExpVendorRequiredFor.EXPENSE_REPORT;
			entity.vendorUsedIn = ExpVendorUsedIn.EXPENSE_REPORT;

			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				entity.setExtendedItemTextId(i, this.extendedItemObjs[i].Id);
				entity.setExtendedItemTextUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT);
				entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);

				entity.setExtendedItemPicklistId(i, this.extendedItemPicklistObjs[i].Id);
				entity.setExtendedItemPicklistUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT);
				entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);

				entity.setExtendedItemLookupId(i, this.extendedItemsLookupObjs[i].Id);
				entity.setExtendedItemLookupUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT);
				entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);
			}

			return entity;
		}

		/**
		 * 会社オブジェクトを作成する Create a Company record
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのエンティティが取得できることを確認する
	 * 各項目の値が取得できているかの確認はsearchEntityTestで実施
	 */
	@isTest static void getEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpReportType__c> testGroups = testData.createExpReportTypes('検索テスト', recordSize);
		ExpReportType__c targetObj = testGroups[1];

		ExpReportTypeRepository repo = new ExpReportTypeRepository();
		ExpReportTypeEntity resEntity;

		// 存在するIDを指定する
		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);

		// 存在しないIDを指定する
		delete targetObj;
		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * 正しく検索できることを確認する
	 */
	@isTest static void searchEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ComCompany__c testCompanyObj = testData.createCompany('検索テスト会社');
		List<ExpReportType__c> testGroups = testData.createExpReportTypes('検索テスト', recordSize);
		ExpReportType__c targetObj = testGroups[1];

		ExpReportTypeRepository repo = new ExpReportTypeRepository();
		ExpReportTypeRepository.SearchFilter filter;
		List<ExpReportTypeEntity> resEntities;

		// 費目グループIDで検索
		filter = new ExpReportTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
		// 項目値の検証
		assertFieldValue(targetObj, resEntities[0]);
	}

	/**
	 * saveEntityのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ExpReportTypeEntity expReportType1 = testData.createExpReportTypeEntity('経費申請タイプ1');

		// テスト実行
		ExpReportTypeRepository repo = new ExpReportTypeRepository();
		Repository.SaveResult result = repo.saveEntity(expReportType1);

		// 確認
		ExpReportType__c resObj1 = getExpReportTypeObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(expReportType1, resObj1);
	}

	/**
	 * saveEntityListのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ExpReportTypeEntity expReportType1 = testData.createExpReportTypeEntity('経費申請タイプ1');
		ExpReportTypeEntity expReportType2 = testData.createExpReportTypeEntity('経費申請タイプ2');

		// テスト実行
		ExpReportTypeRepository repo = new ExpReportTypeRepository();
		Repository.SaveResult result = repo.saveEntityList(new List<ExpReportTypeEntity>{expReportType1, expReportType2}, ALL_SAVE);

		// 確認
		ExpReportType__c resObj1 = getExpReportTypeObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(expReportType1, resObj1);
		ExpReportType__c resObj2 = getExpReportTypeObj(result.details[1].id);
		System.assertNotEquals(null, resObj2);
		assertFieldValue(expReportType2, resObj2);
	}


	/**
	 * 経費申請タイプオブジェクトを取得する
	 * @param groupId 取得対象の経費申請タイプID
	 * @return 経費申請タイプオブジェクト
	 */
	private static ExpReportType__c getExpReportTypeObj(Id expReportTypeId) {
		return [
				SELECT
					Id,
					Name,
					Active__c,
					Code__c,
					CompanyId__c,
					CostCenterRequiredFor__c,
					CostCenterUsedIn__c,
					JobRequiredFor__c,
					JobUsedIn__c,
					UseFileAttachment__c,
					VendorRequiredFor__c,
					VendorUsedIn__c,
					ExtendedItemText01TextId__c,
					ExtendedItemText01UsedIn__c,
					ExtendedItemText01RequiredFor__c,
					ExtendedItemText02TextId__c,
					ExtendedItemText02UsedIn__c,
					ExtendedItemText02RequiredFor__c,
					ExtendedItemText03TextId__c,
					ExtendedItemText03UsedIn__c,
					ExtendedItemText03RequiredFor__c,
					ExtendedItemText04TextId__c,
					ExtendedItemText04UsedIn__c,
					ExtendedItemText04RequiredFor__c,
					ExtendedItemText05TextId__c,
					ExtendedItemText05UsedIn__c,
					ExtendedItemText05RequiredFor__c,
					ExtendedItemText06TextId__c,
					ExtendedItemText06UsedIn__c,
					ExtendedItemText06RequiredFor__c,
					ExtendedItemText07TextId__c,
					ExtendedItemText07UsedIn__c,
					ExtendedItemText07RequiredFor__c,
					ExtendedItemText08TextId__c,
					ExtendedItemText08UsedIn__c,
					ExtendedItemText08RequiredFor__c,
					ExtendedItemText09TextId__c,
					ExtendedItemText09UsedIn__c,
					ExtendedItemText09RequiredFor__c,
					ExtendedItemText10TextId__c,
					ExtendedItemText10UsedIn__c,
					ExtendedItemText10RequiredFor__c,
					ExtendedItemPicklist01TextId__c,
					ExtendedItemPicklist01UsedIn__c,
					ExtendedItemPicklist01RequiredFor__c,
					ExtendedItemPicklist02TextId__c,
					ExtendedItemPicklist02UsedIn__c,
					ExtendedItemPicklist02RequiredFor__c,
					ExtendedItemPicklist03TextId__c,
					ExtendedItemPicklist03UsedIn__c,
					ExtendedItemPicklist03RequiredFor__c,
					ExtendedItemPicklist04TextId__c,
					ExtendedItemPicklist04UsedIn__c,
					ExtendedItemPicklist04RequiredFor__c,
					ExtendedItemPicklist05TextId__c,
					ExtendedItemPicklist05UsedIn__c,
					ExtendedItemPicklist05RequiredFor__c,
					ExtendedItemPicklist06TextId__c,
					ExtendedItemPicklist06UsedIn__c,
					ExtendedItemPicklist06RequiredFor__c,
					ExtendedItemPicklist07TextId__c,
					ExtendedItemPicklist07UsedIn__c,
					ExtendedItemPicklist07RequiredFor__c,
					ExtendedItemPicklist08TextId__c,
					ExtendedItemPicklist08UsedIn__c,
					ExtendedItemPicklist08RequiredFor__c,
					ExtendedItemPicklist09TextId__c,
					ExtendedItemPicklist09UsedIn__c,
					ExtendedItemPicklist09RequiredFor__c,
					ExtendedItemPicklist10TextId__c,
					ExtendedItemPicklist10UsedIn__c,
					ExtendedItemPicklist10RequiredFor__c,
					ExtendedItemLookup01TextId__c,
					ExtendedItemLookup01UsedIn__c,
					ExtendedItemLookup01RequiredFor__c,
					ExtendedItemLookup02TextId__c,
					ExtendedItemLookup02UsedIn__c,
					ExtendedItemLookup02RequiredFor__c,
					ExtendedItemLookup03TextId__c,
					ExtendedItemLookup03UsedIn__c,
					ExtendedItemLookup03RequiredFor__c,
					ExtendedItemLookup04TextId__c,
					ExtendedItemLookup04UsedIn__c,
					ExtendedItemLookup04RequiredFor__c,
					ExtendedItemLookup05TextId__c,
					ExtendedItemLookup05UsedIn__c,
					ExtendedItemLookup05RequiredFor__c,
					ExtendedItemLookup06TextId__c,
					ExtendedItemLookup06UsedIn__c,
					ExtendedItemLookup06RequiredFor__c,
					ExtendedItemLookup07TextId__c,
					ExtendedItemLookup07UsedIn__c,
					ExtendedItemLookup07RequiredFor__c,
					ExtendedItemLookup08TextId__c,
					ExtendedItemLookup08UsedIn__c,
					ExtendedItemLookup08RequiredFor__c,
					ExtendedItemLookup09TextId__c,
					ExtendedItemLookup09UsedIn__c,
					ExtendedItemLookup09RequiredFor__c,
					ExtendedItemLookup10TextId__c,
					ExtendedItemLookup10UsedIn__c,
					ExtendedItemLookup10RequiredFor__c,
					Description_L0__c,
					Description_L1__c,
					Description_L2__c,
					Name_L0__c,
					Name_L1__c,
					Name_L2__c,
					UniqKey__c
				FROM
					ExpReportType__c
				WHERE
					Id = :expReportTypeId
		];
	}

	/**
	 * 項目の値を検証する
	 * @param actObj 期待値
	 * @param expEntity 実際の値
	 */
	private static void assertFieldValue(ExpReportTypeEntity expEntity, ExpReportType__c actObj) {

		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.active, actObj.Active__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
		System.assertEquals(expEntity.costCenterRequiredFor.value, actObj.CostCenterRequiredFor__c);
		System.assertEquals(expEntity.costCenterUsedIn.value, actObj.CostCenterUsedIn__c);
		System.assertEquals(expEntity.descriptionL0, actObj.Description_L0__c);
		System.assertEquals(expEntity.descriptionL1, actObj.Description_L1__c);
		System.assertEquals(expEntity.descriptionL2, actObj.Description_L2__c);
		System.assertEquals(expEntity.jobRequiredFor.value, actObj.JobRequiredFor__c);
		System.assertEquals(expEntity.jobUsedIn.value, actObj.JobUsedIn__c);
		System.assertEquals(expEntity.useFileAttachment, actObj.UseFileAttachment__c);
		System.assertEquals(expEntity.vendorRequiredFor.value, actObj.VendorRequiredFor__c);
		System.assertEquals(expEntity.vendorUsedIn.value, actObj.VendorUsedIn__c);

		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			System.assertEquals(expEntity.getExtendedItemTextId(i),
					(Id) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_TEXT + index + ExpReportTypeRepository.EXTENDED_ITEM_ID));
			System.assertEquals(expEntity.getExtendedItemTextUsedIn(i).value,
					(String) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_TEXT + index + ExpReportTypeRepository.EXTENDED_ITEM_USED_IN));
			System.assertEquals(expEntity.getExtendedItemTextRequiredFor(i).value,
					(String) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_TEXT + index + ExpReportTypeRepository.EXTENDED_ITEM_REQUIRED_FOR));

			System.assertEquals(expEntity.getExtendedItemPicklistId(i),
					(Id) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpReportTypeRepository.EXTENDED_ITEM_ID));
			System.assertEquals(expEntity.getExtendedItemPicklistUsedIn(i).value,
					(String) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpReportTypeRepository.EXTENDED_ITEM_USED_IN));
			System.assertEquals(expEntity.getExtendedItemPicklistRequiredFor(i).value,
					(String) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpReportTypeRepository.EXTENDED_ITEM_REQUIRED_FOR));

			System.assertEquals(expEntity.getExtendedItemLookupId(i),
					(Id) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpReportTypeRepository.EXTENDED_ITEM_ID));
			System.assertEquals(expEntity.getExtendedItemLookupUsedIn(i).value,
					(String) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpReportTypeRepository.EXTENDED_ITEM_USED_IN));
			System.assertEquals(expEntity.getExtendedItemLookupRequiredFor(i).value,
					(String) actObj.get(ExpReportTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpReportTypeRepository.EXTENDED_ITEM_REQUIRED_FOR));
		}

		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(ExpReportType__c expObj, ExpReportTypeEntity actEntity) {

		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Active__c, actEntity.active);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.CostCenterRequiredFor__c, actEntity.costCenterRequiredFor.value);
		System.assertEquals(expObj.CostCenterUsedIn__c, actEntity.costCenterUsedIn.value);
		System.assertEquals(expObj.Description_L0__c, actEntity.descriptionL0);
		System.assertEquals(expObj.Description_L1__c, actEntity.descriptionL1);
		System.assertEquals(expObj.Description_L2__c, actEntity.descriptionL2);
		System.assertEquals(expObj.JobRequiredFor__c, actEntity.jobRequiredFor.value);
		System.assertEquals(expObj.JobUsedIn__c, actEntity.jobUsedIn.value);
		System.assertEquals(expObj.UseFileAttachment__c, actEntity.useFileAttachment);
		System.assertEquals(expObj.VendorRequiredFor__c, actEntity.vendorRequiredFor.value);
		System.assertEquals(expObj.VendorUsedIn__c, actEntity.vendorUsedIn.value);

		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			System.assertEquals(
				(Id) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_TEXT + index + ExpReportTypeRepository.EXTENDED_ITEM_ID), actEntity.getExtendedItemTextId(i));
			System.assertEquals(
				(String) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_TEXT + index + ExpReportTypeRepository.EXTENDED_ITEM_USED_IN), actEntity.getExtendedItemTextUsedIn(i).value);
			System.assertEquals(
				(String) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_TEXT + index + ExpReportTypeRepository.EXTENDED_ITEM_REQUIRED_FOR), actEntity.getExtendedItemTextRequiredFor(i).value);

			System.assertEquals(
				(Id) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpReportTypeRepository.EXTENDED_ITEM_ID), actEntity.getExtendedItemPicklistId(i));
			System.assertEquals(
				(String) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpReportTypeRepository.EXTENDED_ITEM_USED_IN), actEntity.getExtendedItemPicklistUsedIn(i).value);
			System.assertEquals(
				(String) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpReportTypeRepository.EXTENDED_ITEM_REQUIRED_FOR), actEntity.getExtendedItemPicklistRequiredFor(i).value);

			System.assertEquals(
				(Id) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpReportTypeRepository.EXTENDED_ITEM_ID), actEntity.getExtendedItemLookupId(i));
			System.assertEquals(
				(String) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpReportTypeRepository.EXTENDED_ITEM_USED_IN), actEntity.getExtendedItemLookupUsedIn(i).value);
			System.assertEquals(
				(String) expObj.get(ExpReportTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpReportTypeRepository.EXTENDED_ITEM_REQUIRED_FOR), actEntity.getExtendedItemLookupRequiredFor(i).value);
		}

		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
	}
}