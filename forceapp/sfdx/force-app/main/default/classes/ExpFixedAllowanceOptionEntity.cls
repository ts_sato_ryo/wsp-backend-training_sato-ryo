/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Entity class for Expense Fixed Allowance Option
 * 
 * @group Expense
 */
public class ExpFixedAllowanceOptionEntity extends Entity {

	/** Field definition */
	public enum Field {
		ALLOWANCE_AMOUNT,
		CURRENCY_ID,
		EXP_TYPE_ID,
		LABEL_L0,
		LABEL_L1,
		LABEL_L2,
		ORDER
	}

	/** Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;

	public ExpFixedAllowanceOptionEntity() {
		super();
		this.isChangedFieldSet = new Set<Field>();
	}

	/** Fixed Allowance Option Name (reference only) */
	public String name { get; set; }

	/** Fixed Allowance Amount */
	public Decimal allowanceAmount {
		get;
		set {
			allowanceAmount = value;
			setChanged(Field.ALLOWANCE_AMOUNT);
		}
	}

	/** Currency Id */
	public Id currencyId {
		get;
		set {
			currencyId = value;
			setChanged(Field.CURRENCY_ID);
		}
	}

	/** ID of Expense Type the Fixed Allowance Option belongs to */
	public Id expTypeId {
		get;
		set {
			expTypeId = value;
			setChanged(Field.EXP_TYPE_ID);
		}
	}

	/** Fixed Allowance Label (L0) */
	public String labelL0 {
		get;
		set {
			labelL0 = value;
			setChanged(Field.LABEL_L0);
		}
	}

	/** Fixed Allowance Label (L1) */
	public String labelL1 {
		get;
		set {
			labelL1 = value;
			setChanged(Field.LABEL_L1);
		}
	}

	/** Fixed Allowance Label (L2) */
	public String labelL2 {
		get;
		set {
			labelL2 = value;
			setChanged(Field.LABEL_L2);
		}
	}

	/** Fixed Allowance Label (Reference ONLY) */
	public AppMultiString labelL {
		get {return new AppMultiString(this.labelL0, this.labelL1, this.labelL2);}
	}

	/** Order of Fixed Allowance Option, 0 at the top */
	public Integer order {
		get;
		set {
			order = value;
			setChanged(Field.ORDER);
		}
	}

	/**
	 * Mark a field as changed
	 * @param field The field which has been changed
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field to check
	 * @return True if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status for all the fields
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}