/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブタイプ別作業分類エンティティ
 */
public with sharing class JobTypeWorkCategoryEntity extends JobTypeWorkCategoryGeneratedEntity {

	/**
	 * コンストラクタ
	 */
	public JobTypeWorkCategoryEntity() {
		super(new TimeJobTypeWorkCategory__c());
	}

	/**
	 * コンストラクタ
	 */
	public JobTypeWorkCategoryEntity(TimeJobTypeWorkCategory__c sobj) {
		super(sobj);
	}

	/**
	 * 作業分類情報
	 * TODO : 子エンティティの持ち方を変更する。GENIE-11249で対応予定。
	 */
	public class WorkCategory {
		/** 作業分類名 */
		public AppMultiString nameL;
		/** 並び順 */
		public Integer order;
		/** 有効開始日 */
		public AppDate validFrom;
		/** 失効日 */
		public AppDate validTo;
	}

	/**
	 * ジョブタイプ情報
	 * TODO : 子エンティティの持ち方を変更する。GENIE-11249で対応予定。
	 */
	public class JobType {
		/** ジョブタイプ名 */
		public AppMultiString nameL;
	}

	/** ジョブタイプ(参照専用) */
	public JobTypeWorkCategoryEntity.JobType jobType {
		get {
			if (this.jobTypeId == null) {
				return null;
			}
			if (jobType == null) {
				jobType = new JobTypeWorkCategoryEntity.JobType();
				jobType.nameL = new AppMultiString(
					sObj.JobTypeId__r.Name_L0__c,
					sObj.JobTypeId__r.Name_L1__c,
					sObj.JobTypeId__r.Name_L2__c);
			}
			return jobType;
		}
		private set;
	}


	/** 作業分類(参照専用) */
	public JobTypeWorkCategoryEntity.WorkCategory workCategory {
		get {
			if (this.workCategoryId == null) {
				return null;
			}
			if (workCategory == null) {
				workCategory = new JobTypeWorkCategoryEntity.workCategory();
				workCategory.nameL = new AppMultiString(
							sObj.WorkCategoryId__r.Name_L0__c,
							sObj.WorkCategoryId__r.Name_L1__c,
							sObj.WorkCategoryId__r.Name_L2__c);
				workCategory.order = (Integer)sObj.WorkCategoryId__r.Order__c;
				workCategory.validFrom = AppDate.valueOf(sObj.WorkCategoryId__r.ValidFrom__c);
				workCategory.validTo = AppDate.valueOf(sObj.WorkCategoryId__r.ValidTo__c);
			}
			return workCategory;
		}
		private set;
	}

}