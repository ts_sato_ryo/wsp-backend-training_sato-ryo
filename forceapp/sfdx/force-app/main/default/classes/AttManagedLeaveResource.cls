/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇管理系APIを提供するリソースクラス
 */
public with sharing class AttManagedLeaveResource {
	private static final Integer DAYS_GRANTED_MAX = 999;
	/**
	 * @desctiprion 年次有休対管理社員検索APIのリクエストパラメータ
	 */
	public class SearchEmpListRequest implements RemoteApi.RequestParam {
		/** 会社Id */
		public String companyId;
		/** 休暇Id */
		public String leaveId;
		/** 社員コード */
		public String empCode;
		/** 社員名 */
		public String empName;
		/** 所属部署名 */
		public String deptName;
		/** 勤務体系名 */
		public String workingTypeName;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 会社ID
			if (String.isNotBlank(companyId)) {
				try {
					Id.ValueOf(companyId);
				} catch (Exception e) {
					throw new App.ParameterException('companyId', companyId);
				}
			}
			else {
				throw new App.ParameterException('companyId', companyId);
			}
			// 休暇ID
			if (String.isNotBlank(leaveId)) {
				try {
					Id.ValueOf(leaveId);
				} catch (Exception e) {
					throw new App.ParameterException('leaveId', leaveId);
				}
			}
		}
	}
	/**
	 * @desctiprion 年次有休対管理社員検索APIのレスポンスパラメータ
	 */
	public class SearchEmpListResponse implements RemoteApi.ResponseParam {
		/** 社員一覧 */
		public List<EmpResponse> records = new List<EmpResponse>();
	}
	@testVisible
	private class EmpResponse {
		public String id;
		public String code;
		public String name;
		public String photoUrl;
		public String deptName;
		public String workingTypeName;

		public EmpResponse(EmployeeBaseEntity empData) {
			EmployeeHistoryEntity empHistory = empData.getHistory(0);
			this.id = empData.id;
			this.code = empData.code;
			this.name = empData.fullNameL.getFullName();
			this.photoUrl = empData.user == null ? null : empData.user.photoUrl;
			this.deptName = empHistory.department == null ? null : empHistory.department.nameL.getValue();
			this.workingTypeName = empHistory.workingType == null ? null : empHistory.workingType.nameL.getValue();
		}
	}

	/**
	 * @description 指定有休の対象社員一覧を取得するAPI
	 */
	public with sharing class SearchEmpListApi extends RemoteApi.ResourceBase {
		/**
		 * @description 申請可能な休暇マスタ一覧を取得する
		 * @param req リクエストパラメータ(SearchEmpListRequest)
		 * @return レスポンス(GetLeaveResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SearchEmpListResponse res = new SearchEmpListResponse();

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchEmpListRequest param =
					(SearchEmpListRequest)req.getParam(SearchEmpListRequest.class);

			// パラメータ値の検証
			param.validate();

			Id companyId = Id.valueOf(param.companyId);
			AttLeaveEntity leaveData = null;
			if (String.isBlank(param.leaveId)) {
				// 年次有休マスタを取得
				leaveData = new AttLeaveService().getAnnualLeave(companyId);
			}
			else {
				// 指定有休マスタを取得
				leaveData = new AttLeaveRepository().getLeaveById(param.leaveId);
			}
			if (leaveData == null) {
				return res;
			}
			String leaveCode = leaveData.code;
			Date dtToday = Date.today();

			// 勤務体系取得
			Set<Id> workingTypeIds = new Set<id>();
			AttWorkingTypeRepository.SearchFilter workingTypeParam = new AttWorkingTypeRepository.SearchFilter();
			workingTypeParam.companyIds = new Set<Id>{companyId};
			workingTypeParam.targetDate = AppDate.valueOf(dtToday);
			workingTypeParam.nameL = param.workingTypeName;
			for (AttWorkingTypeBaseEntity workingTypeData :
					new AttWorkingTypeRepository().searchEntity(workingTypeParam)) {
				AttWorkingTypeHistoryEntity workingTypeHistory = workingTypeData.getHistory(0);
				// 指定有休を利用する勤務体系Idを記録
				if (workingTypeHistory.leaveCodeList != null && !workingTypeHistory.leaveCodeList.isEmpty()) {
					if (new Set<String>(workingTypeHistory.leaveCodeList).contains(leaveCode)) {
						workingTypeIds.add(workingTypeData.id);
					}
				}
			}
			if (workingTypeIds.isEmpty()) {
				return res;
			}

			// 部署ベースIdを記録
			Set<Id> deptIds = null; // null:無条件
			if (String.isNotBlank(param.deptName)) {
				deptIds = new Set<Id>();
				DepartmentRepository.SearchFilter deptParam = new DepartmentRepository.SearchFilter();
				deptParam.companyIds = companyId == null ? null : new List<Id>{param.companyId};
				deptParam.targetDate = AppDate.valueOf(dtToday);
				deptparam.nameL = param.deptName;
				for (DepartmentBaseEntity deptData :
						new DepartmentRepository().searchEntity(deptParam)) {
					deptIds.add(deptData.id);
				}
				if (deptIds.isEmpty()) {
					return res;
				}
			}
			// 社員検索
			EmployeeRepository.SearchFilter empParam = new EmployeeRepository.SearchFilter();
			empParam.companyIds = companyId == null ? null : new Set<Id>{companyId};
			empParam.nameL = param.empName;
			empParam.code = param.empCode;
			empParam.targetDate = AppDate.valueOf(dtToday);
			empParam.departmentIds = deptIds;
			empParam.workingTypeIds = workingTypeIds;
			List<EmployeeBaseEntity> empDataList = new EmployeeService().searchEntity(empParam);
			if (empDataList.isEmpty()) {
				return res;
			}

			for (EmployeeBaseEntity empData :empDataList) {
				res.records.add(new EmpResponse(empData));
			}
			return res;
		}
	}
	/**
	 * @desctiprion 日数管理休暇一覧取得APIのリクエストパラメータ
	 */
	public class SearchManagedLeaveListRequest implements RemoteApi.RequestParam {
		/** 会社Id */
		public String companyId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 会社Id
			if (String.isNotBlank(companyId)) {
				try {
					Id.ValueOf(companyId);
				} catch (Exception e) {
					throw new App.ParameterException('companyId', companyId);
				}
			}
			else {
				throw new App.ParameterException('companyId', companyId);
			}
		}
	}
	/**
	 * @desctiprion 日数管理休暇一覧APIのレスポンスパラメータ
	 */
	public class SearchManagedLeaveListResponse implements RemoteApi.ResponseParam {
		/** 日数管理休暇一覧 */
		public List<SearchManagedLeaveResponse> records = new List<SearchManagedLeaveResponse>();
	}
	@testVisible
	private class SearchManagedLeaveResponse {
		public String id;
		public String name;

		public SearchManagedLeaveResponse(AttLeaveEntity managedLeaveData) {
			this.id = managedLeaveData.id;
			this.name = managedLeaveData.nameL.getValue();
		}
	}
	/**
	 * @description 日数管理休暇一覧取得API
	 */
	public with sharing class SearchManagedLeaveListApi extends RemoteApi.ResourceBase {
		/**
		 * @description 会社毎の日数管理休暇一覧を取得するAPI
		 * @param req リクエストパラメータ(SearchManagedLeaveListRequest)
		 * @return レスポンス(SearchManagedLeaveListResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchManagedLeaveListRequest param =
					(SearchManagedLeaveListRequest)req.getParam(SearchManagedLeaveListRequest.class);

			// パラメータ値の検証
			param.validate();

			List<SearchManagedLeaveResponse> managedLeaveList = new List<SearchManagedLeaveResponse>();
			for (AttLeaveEntity managedLeaveData: new AttLeaveService().
						getManagedLeaveList(param.companyId)) {
				managedLeaveList.add(new SearchManagedLeaveResponse(managedLeaveData));
			}
			SearchManagedLeaveListResponse res = new SearchManagedLeaveListResponse();
			res.records = managedLeaveList;
			return res;
		}
	}

	/**
	 * @desctiprion 有休付与APIのリクエストパラメータ
	 */
	public class CreateGrantRequest implements RemoteApi.RequestParam {
		/** 社員Id */
		public String empId;
		/** 休暇Id */
		public String leaveId;
		/** 付与日数 */
		public Decimal daysGranted;
		/** 利用開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** コメント */
		public String comment;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 社員Id
			if (String.isNotBlank(empId)) {
				try {
					Id.ValueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', empId);
				}
			}
			else {
				throw new App.ParameterException('empId', empId);
			}
			// 休暇ID
			if (String.isNotBlank(leaveId)) {
				try {
					Id.ValueOf(leaveId);
				} catch (Exception e) {
					throw new App.ParameterException('leaveId', leaveId);
				}
			}
			// 付与日数
			if (daysGranted == null) {
				throw new App.ParameterException('daysGranted', daysGranted);
			}
			else if (daysGranted < 1
					|| daysGranted > DAYS_GRANTED_MAX
					|| daysGranted > Integer.valueOf(daysGranted)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(
						new List<String>{ComMessage.msg().Admin_Lbl_DaysGranted}));
			}
			// 有効開始日
			if (validDateFrom == null) {
				throw new App.ParameterException('validDateFrom', validDateFrom);
			}
			// 失効日
			if (validDateTo == null) {
				throw new App.ParameterException('validDateTo', validDateTo);
			}
			// 失効日＞有効開始日
			if (validDateTo <= validDateFrom) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValueEarlier(
						new List<String>{
							ComMessage.msg().Admin_Lbl_LeaveGrantValidDateFrom,
							ComMessage.msg().Admin_Lbl_LeaveGrantValidDateTo}));
			}
			if (comment != null && comment.length() > 1000) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(
						new List<String>{ComMessage.msg().Att_Lbl_Comment}));
			}
		}
	}
	/**
	 * @desctiprion 有休付与APIのレスポンスパラメータ
	 */
	public class CreateGrantResponse implements RemoteApi.ResponseParam {
	}
	/**
	 * @description 有休付与API
	 */
	public with sharing class CreateGrantApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_GRANT;

		/**
		 * @description 対象社員に有休を付与するAPI
		 * @param req リクエストパラメータ(CreateGrantRequest)
		 * @return レスポンス(CreateGrantResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			CreateGrantRequest param =
					(CreateGrantRequest)req.getParam(CreateGrantRequest.class);

			// パラメータ値の検証
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			Id empId = Id.valueOf(param.empId);
			AttDays daysGranted = AttDays.valueOf(param.daysGranted);
			AppDate validDateFrom = AppDate.valueOf(param.validDateFrom);
			AppDate validDateTo = AppDate.valueOf(param.validDateTo);
			String comment = param.comment;

			// 有効開始日により社員を取得する
			EmployeeBaseEntity empData = new EmployeeService().getEmployee(empId, AppDate.today());
			AttLeaveEntity leaveData = null;
			if (String.isBlank(param.leaveId)) {
				// 年次有休マスタを取得
				leaveData = new AttLeaveService().getAnnualLeave(empData.companyId);
			}
			else {
				// 指定有休マスタを取得
				leaveData = new AttLeaveRepository().getLeaveById(param.leaveId);
			}
			if (leaveData == null) {
				throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_LEAVE,
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_Leave}));
			}

			Savepoint sp = Database.setSavepoint();
			try {
				new AttManagedLeaveService().createGrant(empId, leaveData.Id, daysGranted, validDateFrom, validDateTo, comment);
			}
			catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}
			return new CreateGrantResponse();
		}
	}
	/**
	 * @desctiprion 有休付与一覧取得APIのリクエストパラメータ
	 */
	public class GetGrantListRequest implements RemoteApi.RequestParam {
		/** 社員Id */
		public String empId;
		/** 休暇Id */
		public String leaveId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 社員Id
			if (String.isNotBlank(empId)) {
				try {
					Id.ValueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', empId);
				}
			}
			else {
				throw new App.ParameterException('empId', empId);
			}
			// 休暇ID
			if (String.isNotBlank(leaveId)) {
				try {
					Id.ValueOf(leaveId);
				} catch (Exception e) {
					throw new App.ParameterException('leaveId', leaveId);
				}
			}
		}
	}
	/**
	 * @desctiprion 有休付与APIのレスポンスパラメータ
	 */
	public class GetGrantListResponse implements RemoteApi.ResponseParam {
		/** 有休付与一覧 */
		public List<GrantResponse> records = new List<GrantResponse>();
	}
	@testVisible
	private class GrantResponse {
		public String id;
		public Decimal daysGranted;
		public Decimal daysLeft;
		public Integer hoursLeft;
		public Date validDateFrom;
		public Date validDateTo;
		public String comment;

		public GrantResponse(AttManagedLeaveGrantEntity grantData) {
			this.id = grantData.id;
			this.daysGranted = AppConverter.decValue(grantData.daysGranted);
			this.daysLeft = AppConverter.decValue(grantData.daysLeft);
			this.hoursLeft = grantData.hoursLeft;
			this.validDateFrom = AppConverter.dateValue(grantData.validDateFrom);
			this.validDateTo = AppConverter.dateValue(grantData.validDateTo);
			this.comment = grantData.comment;
		}
	}
	/**
	 * @description 有休付与一覧取得API
	 */
	public with sharing class GetGrantListApi extends RemoteApi.ResourceBase {
		/**
		 * @description 対象社員に有休付与一覧を取得するAPI
		 * @param req リクエストパラメータ(GetGrantListRequest)
		 * @return レスポンス(GetGrantListResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			GetGrantListRequest param =
					(GetGrantListRequest)req.getParam(GetGrantListRequest.class);

			// パラメータ値の検証
			param.validate();

			Id empId = Id.valueOf(param.empId);
			// 現在日付により社員を取得する
			EmployeeBaseEntity empData = new EmployeeService().getEmployee(empId, AppDate.today());
			AttLeaveEntity leaveData = null;
			if (String.isBlank(param.leaveId)) {
				// 年次有休マスタを取得
				leaveData = new AttLeaveService().getAnnualLeave(empData.companyId);
			}
			else {
				// 指定有休マスタを取得
				leaveData = new AttLeaveRepository().getLeaveById(param.leaveId);
			}
			if (leaveData == null) {
				throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_LEAVE,
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_Leave}));
			}
			// 勤務体系を取得
			AttWorkingTypeRepository workTypeRep = new AttWorkingTypeRepository();
			Date dtToday = Date.today();
			AttWorkingTypeBaseEntity workTypeData = workTypeRep.getEntity(empData.getHistory(0).workingTypeId, AppDate.valueOf(dtToday));
			if (workTypeData == null) {
				throw new App.IllegalStateException(
						ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_WorkingType}));
			}
			AttWorkingTypeHistoryEntity workTypeHistory = workTypeData.getHistory(0);
			List<GrantResponse> grantResList = new List<GrantResponse>();
			for (AttManagedLeaveGrantEntity grantData : new AttManagedLeaveService().
						getAllGrantedList(empId, leaveData.Id)) {
				AttDays.DaysAndHours daysAndHours = grantData.daysLeft.getDaysAndHours(workTypeHistory.contractedWorkHours);
				grantData.daysLeft = AttDays.valueOf(daysAndHours.days);
				grantData.hoursLeft = daysAndHours.hours;
				grantResList.add(new GrantResponse(grantData));
			}
			GetGrantListResponse res = new GetGrantListResponse();
			res.records = grantResList;
			return res;
		}
	}

	/**
	 * @desctiprion 有休付与調整APIのリクエストパラメータ
	 */
	public class AdjustGrantRequest implements RemoteApi.RequestParam {
		/** 付与Id */
		public String grantId;
		/** 付与日数(調整後) */
		public Decimal daysGranted;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// 付与Id
			if (String.isNotBlank(grantId)) {
				try {
					Id.ValueOf(grantId);
				} catch (Exception e) {
					throw new App.ParameterException('grantId', grantId);
				}
			}
			else {
				throw new App.ParameterException('grantId', grantId);
			}
			// 付与日数
			if (daysGranted == null) {
				throw new App.ParameterException('daysGranted', daysGranted);
			}
			else if (daysGranted < 0
					|| daysGranted > DAYS_GRANTED_MAX
					|| daysGranted > Integer.valueOf(daysGranted)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(
						new List<String>{ComMessage.msg().Admin_Lbl_DaysGranted}));
			}
		}
	}
	/**
	 * @desctiprion 有休付与APIのレスポンスパラメータ
	 */
	public class AdjustGrantResponse implements RemoteApi.ResponseParam {
	}
	/**
	 * @description 有休付与API
	 */
	public with sharing class AdjustGrantApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_GRANT;

		/**
		 * @description 有休付与を調整するAPI
		 * @param req リクエストパラメータ(AdjustGrantRequest)
		 * @return レスポンス(AdjustGrantResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			AdjustGrantRequest param =
					(AdjustGrantRequest)req.getParam(AdjustGrantRequest.class);

			// パラメータ値の検証
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			Id grantId = Id.valueOf(param.grantId);
			AttDays daysGranted = AttDays.valueOf(param.daysGranted);
			Savepoint sp = Database.setSavepoint();
			try {
				new AttManagedLeaveService().adjustGrant(grantId, daysGranted);
			}
			catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}
			return new AdjustGrantResponse();
		}
	}

	/**
	 * @description 有休付与削除APIのリクエストパラメータ
	 */
	public class DeleteGrantRequest implements RemoteApi.RequestParam {
		/** 付与Id */
		public String grantId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException
		 */
		public void validate() {
			// 付与Id
			if (String.isBlank(grantId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('grantId');
				throw e;
			}
			try {
				Id.valueOf(grantId);
			} catch (StringException e) {
				throw new App.TypeException('grantId', grantId);
			}
		}
	}

	/**
	 * @desctiprion 有休付与削除APIのレスポンスパラメータ
	 */
	public class DeleteGrantResponse implements RemoteApi.ResponseParam {
	}

	/**
	 * @description 有休付与削除API
	 */
	public with sharing class DeleteGrantApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_GRANT;

		/**
		 * @description 有休付与を削除するAPI
		 * @param req リクエストパラメータ(DeleteGrantRequest)
		 * @return レスポンス(DeleteGrantResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータを変換
			DeleteGrantRequest param = (DeleteGrantRequest)req.getParam(DeleteGrantRequest.class);
			// パラメータ値の検証
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 削除実行
			Id grantId = Id.valueOf(param.grantId);
			new AttManagedLeaveService().deleteGrant(grantId);

			return new DeleteGrantResponse();
		}
	}
}
