/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 *
 */
@isTest
private class AttTimePeriodTest {

	/*
	 * nullを含まない値のソートのテスト
	 */
	@isTest
    static void sortTest() {
		List<AttTimePeriod> result = AttTimePeriod.sortByStartTime(
			AttTime.newInstance(12, 0), AttTime.newInstance(12, 30),
			AttTime.newInstance(13, 0), AttTime.newInstance(13, 30),
			AttTime.newInstance(11, 0), AttTime.newInstance(11, 30),
			AttTime.newInstance(9, 0), AttTime.newInstance(10, 0),
			AttTime.newInstance(10, 0), AttTime.newInstance(9, 0)
		);

		System.assertEquals(5, result.size());
		System.assertEquals(AttTime.newInstance(9,  00), result[0].startTime);
		System.assertEquals(AttTime.newInstance(10, 00), result[0].endTime);
		System.assertEquals(AttTime.newInstance(10, 00), result[1].startTime);
		System.assertEquals(AttTime.newInstance(9,  00), result[1].endTime);
		System.assertEquals(AttTime.newInstance(11, 00), result[2].startTime);
		System.assertEquals(AttTime.newInstance(11, 30), result[2].endTime);
		System.assertEquals(AttTime.newInstance(12, 00), result[3].startTime);
		System.assertEquals(AttTime.newInstance(12, 30), result[3].endTime);
		System.assertEquals(AttTime.newInstance(13, 00), result[4].startTime);
		System.assertEquals(AttTime.newInstance(13, 30), result[4].endTime);
    }

	/*
	 * nullを含む値のソートのテスト
	 */
	@isTest
    static void sortTestWithNull() {
		List<AttTimePeriod> result = AttTimePeriod.sortByStartTime(
			null, AttTime.newInstance(9, 00),
			AttTime.newInstance(12, 00), null,
			null, AttTime.newInstance(10, 00),
			null, null,
			AttTime.newInstance(13, 00), AttTime.newInstance(14, 00)
		);

		System.debug(LoggingLevel.INFO, result);
		System.assertEquals(5, result.size());
		System.assertEquals(AttTime.newInstance(13, 0), result[0].startTime);
		System.assertEquals(AttTime.newInstance(14, 0), result[0].endTime);
		System.assertEquals(AttTime.newInstance(12, 0), result[1].startTime);
		System.assertEquals(null, result[1].endTime);
		System.assertEquals(null, result[2].startTime);
		System.assertEquals(AttTime.newInstance(9, 0), result[2].endTime);
		System.assertEquals(null, result[3].startTime);
		System.assertEquals(AttTime.newInstance(10, 0), result[3].endTime);
		System.assertEquals(null, result[4].startTime);
		System.assertEquals(null, result[4].endTime);
    }
}