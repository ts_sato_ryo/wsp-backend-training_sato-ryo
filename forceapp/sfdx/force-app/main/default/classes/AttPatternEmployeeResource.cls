/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 勤務パターン社員一括適用のAPIを実装するクラス
 */
public with sharing class AttPatternEmployeeResource {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @description 勤務パターン社員一括適用の1レコードを表すクラス
	 */
	public virtual class EmployeePattern {
		/** 社員コード */
		public String employeeCode;
		/** 勤務パターン適用日(yyyy-mm-dd) */
		public String targetDate;
		/** 勤務パターンコード */
		public String patternCode;
		/** 日タイプ */
		public String dayType;
	}

	/**
	 * @description 勤務パターン適用リクエストパラメータ
	 */
	public class BatchExecutionRequest implements RemoteApi.RequestParam {
		/** 会社ID */
		public String companyId;
		/** 勤務パターン適用一覧 */
		public EmployeePatternExecution[] records;
		/** コメント */
		public String comment;

		/**
		 * @description パラメータを検証する
		 */
		public void validate() {

			// 会社ID
			if (String.isBlank(this.companyId)) {
				throw new App.ParameterException(MESSAGE.Com_Err_NotSetValue(new List<String>{'companyId'}));
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'companyId'}));
			}

			// 勤務パターン適用一覧
			if (this.records == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_NotSetValue(new List<String>{'records'}));
			}

			// 勤務パターン適用一覧の上限チェック
			if (this.records.size() > AttPatternEmployeeBatch.LIMIT_RECORDS_COUNT) {
				throw new App.ParameterException(MESSAGE.Att_Err_PatternApplyOverRecordCount);
			}
		}

		/**
		 * @description 勤務パターン社員適用インポートエンティティを作成する
		 */
		public List<AttPatternApplyImportEntity> createImportEntity(Id importBatchId) {
			List<AttPatternApplyImportEntity> patternApplyList = new List<AttPatternApplyImportEntity>();
			for (Integer i = 0; i < this.records.size(); i++) {

				// インポート作成
				Integer importNo = i + 1;
				AttPatternApplyImportEntity patternApply = this.records[i].createImportEntity(importBatchId, importNo);
				patternApplyList.add(patternApply);
			}
			return patternApplyList;
		}
	}

	/**
	 * @description 勤務パターン社員適用情報
	 */
	public class EmployeePatternExecution extends AttPatternEmployeeResource.EmployeePattern {

		/**
		 * @description 勤務パターン適用インポートエンティティを作成する
		 * @param batchId インポートバッチID
		 * @param importNo インポート順
		 */
		public AttPatternApplyImportEntity createImportEntity(Id batchId, Integer importNo) {
			AttPatternApplyImportEntity entity = new AttPatternApplyImportEntity();
			entity.importBatchId = batchId;
			entity.shiftDate = this.targetDate;
			entity.employeeCode = this.employeeCode;
			entity.patternCode = this.patternCode;
			entity.dayType = this.dayType;
			entity.importNo = importNo;
			entity.status = ImportStatus.WAITING;

			return entity;
		}
	}

	/**
	 * @desctiprion 勤務パターン社員一括適用APIを実行する
	 */
	public with sharing class BatchExecutionApi extends RemoteApi.ResourceBase {

		/** テスト用：バッチを実行しない場合はtrueにする */
		@TestVisible
		private Boolean skipBatchForTest = Test.isRunningTest();

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_PATTERN_APPLY;

		/**
		 * @description 勤務パターンを社員に一括適用する
		 * @param  req リクエストパラメータ(BatchExecutionRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			BatchExecutionRequest param = (BatchExecutionRequest)req.getParam(BatchExecutionRequest.class);

			// パラメータのバリデーション
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// ログインユーザの社員を取得
			// 社員でない場合はAPIを実行できない
			EmployeeBaseEntity loginEmployee;
			try {
				loginEmployee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
			} catch (Exception e) {
				throw new App.IllegalStateException(MESSAGE.Com_Err_RequiredEmployeeToExcecute);
			}

			// インポートバッチを作成して保存する
			AttImportBatchEntity importBatch = createImportBatchEntity(param.companyId, loginEmployee, param.comment, param.records.size());
			Validator.Result batchResult = new AttConfigValidator.AttImportBatchValidator(importBatch).validate();
			if (! batchResult.isSuccess()) {
				String msg = batchResult.getErrors().get(0).message + ')';
				App.ParameterException ex = new App.ParameterException(msg);
				ex.setCode(batchResult.getErrors().get(0).code);
				throw ex;
			}
			Id importBatchId = new AttImportBatchRepository().saveEntity(importBatch).details[0].id;

			// 勤務パターン適用インポートエンティティを作成する
			List<AttPatternApplyImportEntity> patternApplyList = param.createImportEntity(importBatchId);
			// 検証する
			for (AttPatternApplyImportEntity patternApply : patternApplyList) {
				Validator.Result result = new AttConfigValidator.AttPatternApplyImportValidator(patternApply).validate();
				if (! result.isSuccess()) {
					// メッセージには行番号を含む
					String msg = MESSAGE.Com_Err_InvalidData + '(' + MESSAGE.Att_Lbl_RecordNumber + ' ' + patternApply.importNo + ': ' + result.getErrors().get(0).message + ')';
					App.ParameterException ex = new App.ParameterException(msg);
					ex.setCode(result.getErrors().get(0).code);
					throw ex;
				}
			}

			// 勤務パターン適用インポートを保存
			new AttPatternApplyImportRepository().saveEntityList(patternApplyList);

			// 適用バッチを実行
			if (! this.skipBatchForTest) {
				AttPatternEmployeeBatch batch = new AttPatternEmployeeBatch(importBatchId);
				Database.executeBatch(batch, AttPatternEmployeeBatch.BATCH_SCOPE);
			}

			// レスポンスなし
			return null;
		}

		/**
		 * @description インポートバッチエンティティを作成する
		 * @param companyId 会社ID
		 * @param actorEmployee 実行者
		 * @param comment コメント
		 * @param recordsCount レコード数
		 * @return レスポンス(null)
		 */
		private AttImportBatchEntity createImportBatchEntity(Id companyId, EmployeeBaseEntity actorEmployee, String comment, Integer recordsCount) {
			final AppDate today = AppDate.today();

			AttImportBatchEntity entity = new AttImportBatchEntity();
			entity.name = 'Batch Import ' + Datetime.valueOfGmt(String.valueOf(system.now()));
			entity.companyId = companyId;
			entity.importType = AttImportBatchType.ATT_PATTERN_APPLY;
			entity.importTime = AppDatetime.now();
			entity.status = ImportBatchStatus.WAITING;
			entity.comment = comment;
			entity.count = recordsCount;
			entity.successCount = 0;
			entity.failureCount = 0;

			EmployeeHistoryEntity actorEmployeeHistory = actorEmployee.getHistoryByDate(today);
			entity.actorHistoryId = actorEmployeeHistory.id;

			if (actorEmployeeHistory.departmentId != null) {
				// 今日時点の部署履歴IDを設定する
				DepartmentBaseEntity departmentBase = new DepartmentRepository().getEntity(actorEmployeeHistory.departmentId, today);
				if (departmentBase != null) {
					entity.departmentHistoryId = departmentBase.getHistory(0).id;
				}
			}

			return entity;
		}
	}

	/**
	 * @desctiprion 勤務パターン適用結果一覧取得APIの検索条件を格納するクラス
	 */
	public class BatchListRequest implements RemoteApi.RequestParam {
		/** 会社ID */
		public String companyId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 会社ID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}
		}
	}

	/**
	 * @description 勤務パターン適用結果一覧取得APIの検索結果を格納するクラス
	 */
	public class BatchListResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public BatchRecord[] records;
	}

	/**
	 * @description 勤怠インポートバッチレコードを表すクラス
	 */
	public class BatchRecord {
		/** レコードID */
		public String id;
		/** 実行日時 */
		public String importDateTime;
		/** ステータス */
		public String status;
		/** 実行者名（社員名） */
		public String actorName;
		/** 実行者の部署名 */
		public String departmentName;
		/** コメント */
		public String comment;
		/** 処理件数 */
		public Integer count;
		/** 成功件数 */
		public Integer successCount;
		/** エラー件数 */
		public Integer failureCount;

		/**
		 * コンストラクタ
		 * @param entity 検索結果(勤怠インポートバッチのエンティティ)
		 * @param empName 実行者名(社員名)
		 * @param deptName 実行者の部署名
		 */
		public BatchRecord(AttImportBatchEntity entity, String empName, String deptName) {
			this.id = entity.id;
			this.importDateTime = entity.importTime.format();
			this.status = entity.status.value;
			this.actorName = empName;
			this.departmentName = deptName;
			this.comment = entity.comment;
			this.count = entity.count != null ? entity.count : 0;
			this.successCount = entity.successCount != null ? entity.successCount : 0;
			this.failureCount = entity.failureCount != null ? entity.failureCount : 0;
		}
	}

	/**
	 * @description 勤務パターン適用結果一覧取得APIを実装するクラス
	 */
	public with sharing class BatchListApi extends RemoteApi.ResourceBase {

		/**
		 * @description 勤務パターン社員一括適用バッチの実行結果を取得する
		 * @param req リクエストパラメータ(BatchListRequest)
		 * @return レスポンス(BatchListResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			BatchListRequest param = (BatchListRequest)req.getParam(BatchListRequest.class);

			// パラメータのバリデーション
			param.validate();

			// 検索フィルタを作成(パラメータで指定されている検索条件を設定)
			AttImportBatchRepository.SearchFilter filter = new AttImportBatchRepository.SearchFilter();
			filter.companyId = param.companyId;

			// 検索
			List<AttImportBatchEntity> entityList = new AttImportBatchRepository().searchEntityList(filter);

			// 実行者IDのセット
			Set<Id> empHistoryIdSet = new Set<Id>();
			// 実行者の部署IDのセット
			Set<Id> deptHistoryIdSet = new Set<Id>();

			// 実行者ID、実行者の部署IDをセットに追加
			for (AttImportBatchEntity ent : entityList) {
				empHistoryIdSet.add(ent.actorHistoryId);
				deptHistoryIdSet.add(ent.departmentHistoryId);
			}

			// 社員履歴IDと社員ベースのMapを取得
			Map<Id, EmployeeBaseEntity> empMap = getEmployeeMap(empHistoryIdSet);
			// 部署履歴IDと部署ベースのMapを取得
			Map<Id, DepartmentBaseEntity> deptMap = getDepartmentMap(deptHistoryIdSet);
			// 会社IDと会社のMapを取得
			Map<Id, CompanyEntity> companyMap = getCompanyMap(deptMap.values());

			// レスポンスのレコードリストを作成
			List<BatchRecord> batchRecordList = createBatchRecordList(entityList, empMap, deptMap, companyMap);

			// 成功レスポンスをセット
			BatchListResponse res = new BatchListResponse();
			res.records = batchRecordList;
			return res;
		}

		/**
		 * @description 社員履歴IDと社員ベースエンティティのMapを返却する
		 * @param empHistoryIdSet 実行者IDのSet
		 * @return 社員履歴IDと社員ベースエンティティのMap
		 */
		@TestVisible
		private Map<Id, EmployeeBaseEntity> getEmployeeMap(Set<Id> empHistoryIdSet) {

			// 社員を検索(論理削除済みのレコードを含む)
			EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
			empfilter.historyIds = empHistoryIdSet;
			empfilter.includeRemoved = true;
			List<EmployeeBaseEntity> empBaseList = new EmployeeRepository().searchEntity(empfilter);

			// 社員履歴IDと社員ベースのMapを作成
			Map<Id, EmployeeBaseEntity> empMap = new Map<Id, EmployeeBaseEntity>();
			for (Id empHistoryId : empHistoryIdSet) {
				for (EmployeeBaseEntity empBase : empBaseList) {
					EmployeeHistoryEntity empHistory = (EmployeeHistoryEntity)empBase.getSuperHistoryById(empHistoryId);
					if (empHistory != null) {
						empMap.put(empHistory.id, empBase);
						break;
					}
				}
			}
			return empMap;
		}

		/**
		 * @description 部署履歴IDと部署ベースエンティティのMapを返却する
		 * @param deptHistoryIdSet 実行者の部署IDのSet
		 * @return 部署履歴IDと部署ベースエンティティのMap
		 */
		@TestVisible
		private Map<Id, DepartmentBaseEntity> getDepartmentMap(Set<Id> deptHistoryIdSet) {

			// 部署を検索(論理削除済みのレコードを含む)
			DepartmentRepository.SearchFilter deptFilter = new DepartmentRepository.SearchFilter();
			deptFilter.historyIds = new List<Id>(deptHistoryIdSet);
			deptFilter.includeRemoved = true;
			List<DepartmentBaseEntity> deptBaseList = new DepartmentRepository().searchEntity(deptFilter);

			// 部署履歴IDと部署ベースのMapを作成
			Map<Id, DepartmentBaseEntity> deptMap = new Map<Id, DepartmentBaseEntity>();
			for (Id deptHistoryId : deptHistoryIdSet) {
				for (DepartmentBaseEntity deptBase : deptBaseList) {
					DepartmentHistoryEntity deptHistory = (DepartmentHistoryEntity)deptBase.getSuperHistoryById(deptHistoryId);
					if (deptHistory != null) {
						deptMap.put(deptHistory.id, deptBase);
						break;
					}
				}
			}
			return deptMap;
		}

		/**
		 * @description 会社IDと会社エンティティのMapを返却する
		 * @param deptBaseList 部署ベースエンティティのList
		 * @return 会社IDと会社エンティティのMap
		 */
		@TestVisible
		private Map<Id, CompanyEntity> getCompanyMap(List<DepartmentBaseEntity> deptBaseList) {

			// 会社を検索
			Set<Id> companyIdSet = new Set<Id>();
			for (DepartmentBaseEntity deptBase : deptBaseList) {
				companyIdSet.add(deptBase.companyId);
			}
			List<CompanyEntity> companyList = new CompanyRepository().getEntityList(new List<Id>(companyIdSet));
			// 会社IDと会社のMapを作成
			Map<Id, CompanyEntity> companyMap = new Map<Id, CompanyEntity>();
			for (CompanyEntity company : companyList) {
				companyMap.put(company.id, company);
			}
			return companyMap;
		}

		/**
		 * @description BatchRecordのListを作成する
		 * @param entityList 勤怠インポートバッチエンティティのList
		 * @param empMap 社員履歴IDと社員ベースエンティティのMap
		 * @param deptMap 部署履歴IDと部署ベースエンティティのMap
		 * @param companyMap 会社IDと会社エンティティのMap
		 * @return レスポンスのレコードリスト(BatchRecordのList)
		 */
		@TestVisible
		private List<BatchRecord> createBatchRecordList(
					List<AttImportBatchEntity> entityList,
					Map<Id, EmployeeBaseEntity> empMap,
					Map<Id, DepartmentBaseEntity> deptMap,
					Map<Id, CompanyEntity> companyMap) {

			List<BatchRecord> batchRecordList = new List<BatchRecord>();
			for (AttImportBatchEntity ent : entityList) {
				// 実行者名を取得
				EmployeeBaseEntity empBase = empMap.get(ent.actorHistoryId);
				String empName = empBase != null ? empBase.fullNameL.getFullName() : null;

				// 実行者の部署名を取得
				String deptName;
				DepartmentBaseEntity deptBase = deptMap.get(ent.departmentHistoryId);
				if (deptBase != null) {
					DepartmentHistoryEntity deptHistory = (DepartmentHistoryEntity)deptBase.getSuperHistoryById(ent.departmentHistoryId);
					deptName = deptHistory != null ? deptHistory.nameL.getValue() : null;

					// 別会社の部署の場合、部署名 + (会社名)を設定
					if (ent.companyId != deptBase.companyId) {
						CompanyEntity company = companyMap.get(deptBase.companyId);
						deptName = company != null ? deptName + '(' + company.nameL.getValue() + ')' : deptName;
					}
				}

				// BatchRecordを作成
				BatchRecord record = new BatchRecord(ent, empName, deptName);
				batchRecordList.add(record);
			}
			return batchRecordList;
		}
	}

	/**
	 * @desctiprion 勤務パターン適用結果詳細取得APIの検索条件を格納するクラス
	 */
	public class BatchDetailRequest implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// レコードID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			} else {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('id');
				throw e;
			}
		}
	}

	/**
	 * @description 勤務パターン適用結果詳細取得APIの検索結果を格納するクラス
	 */
	public class BatchDetailResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public EmployeePatternResult[] records;
	}

	/**
	 * @description 勤務パターン社員一括適用結果の1レコードを表すクラス
	 */
	public class EmployeePatternResult extends AttPatternEmployeeResource.EmployeePattern {
		/** ステータス */
		public String status;
		/** エラー詳細 */
		public String errorDetail;

		/**
		 * コンストラクタ
		 * @param entity 勤務パターン適用インポートのエンティティ
		 */
		public EmployeePatternResult(AttPatternApplyImportEntity entity) {
			this.employeeCode = entity.employeeCode;
			this.targetDate = entity.shiftDate;
			this.patternCode = entity.patternCode;
			this.dayType = entity.dayType;
			this.status = entity.status.value;
			this.errorDetail = entity.error;
		}
	}

	/**
	 * @description 勤務パターン適用結果詳細取得APIを実装するクラス
	 */
	public with sharing class BatchDetailApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_PATTERN_APPLY;

		/**
		 * @description 勤務パターン社員一括適用バッチの実行結果の詳細を取得する
		 * @param req リクエストパラメータ(BatchDetailRequest)
		 * @return レスポンス(BatchDetailResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			BatchDetailRequest param = (BatchDetailRequest)req.getParam(BatchDetailRequest.class);

			// パラメータのバリデーション
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 検索フィルタを作成(パラメータで指定されている検索条件を設定)
			AttPatternApplyImportRepository.SearchFilter filter = new AttPatternApplyImportRepository.SearchFilter();
			filter.importBatchId = param.id;

			// 検索
			List<AttPatternApplyImportEntity> entityList = new AttPatternApplyImportRepository().searchEntityList(filter);

			// 検索結果が0件の場合は、エラーを出力する
			if (entityList.isEmpty()) {
				throw new App.RecordNotFoundException(MESSAGE.Att_Err_LogNotFound);
			}

			// レスポンスのレコードリストを作成
			List<EmployeePatternResult> employeePatternResultList = new List<EmployeePatternResult>();
			for (AttPatternApplyImportEntity entity : entityList) {
				// EmployeePatternResultを作成
				EmployeePatternResult record = new EmployeePatternResult(entity);
				employeePatternResultList.add(record);
			}

			// 成功レスポンスをセット
			BatchDetailResponse res = new BatchDetailResponse();
			res.records = employeePatternResultList;
			return res;
		}
	}
}