/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * AttAbsenceRequestServiceのテストクラス
 */
@isTest
private class AttAbsenceRequestServiceTest {

	private static AttAbsenceRequestService requestService = new AttAbsenceRequestService();
	private static AttDailyRequestRepository requestRepository = new AttDailyRequestRepository();
	private static AttAttendanceService attService = new AttAttendanceService();
	private static AttSummaryRepository attRepository = new AttSummaryRepository();

	/**
	 * テストに使用する共通のデータを定義する。
	 */
	private class TestData {

		/** 申請者 */
		public EmployeeBaseEntity employee;

		/** 承認者 */
		public EmployeeBaseEntity approver01;

		/** 勤務体系 */
		public AttWorkingTypeBaseEntity workingType;

		public TestData() {
			AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
			List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(2);
			employee = testData.employee;
			approver01 = testEmpList[1];
			workingType = testData.workingType;
			// 承認者01設定
			update new ComEmpHistory__c(Id = employee.getHistory(0).id, ApproverBase01Id__c = approver01.id);
			// 欠勤申請を使用する
			update new AttWorkingTypeHistory__c(Id = testData.workingType.getHistoryList()[0].id, UseAbsenceApply__c = true);

			// 振替休日を作成する
			AttLeaveEntity substituteLeave = testData.createLeave('x', '振替休暇', AttLeaveType.SUBSTITUTE, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
			testData.workingType.getHistory(0).leaveCodeList = new List<String>{substituteLeave.code};
			new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));
		}
	}

	/**
	 * 申請：正常系
	 * 勤務日に欠勤申請出来る事を検証する。
	 */
	@isTest static void submitDay() {
		TestData testData = new TestData();
		AppDate absenceDate = AppDate.newInstance(2018, 9, 3);
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = absenceDate;
		param.endDate = absenceDate;
		param.reason = '欠勤理由';

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.ABSENCE, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.endDate);
		System.assertEquals('欠勤理由', request.reason);
		System.assertEquals(AppRequestStatus.PENDING, request.status);
		System.assertEquals(AppCancelType.NONE, request.cancelType);
		System.assertEquals(false, request.isConfirmationRequired);

		// 勤怠明細を検証
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, absenceDate);
		System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
		System.assertEquals(null, attRecord.reqAbsenceRequestId);
	}

	/**
	 * 申請：正常系
	 * 複数の勤務日に欠勤申請出来る事を検証する。
	 */
	@isTest static void submitDays() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 5);
		param.reason = '欠勤理由';

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.ABSENCE, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 5), request.endDate);
		System.assertEquals(AppRequestStatus.PENDING, request.status);
		System.assertEquals(AppCancelType.NONE, request.cancelType);
		System.assertEquals(false, request.isConfirmationRequired);
		System.assertEquals(0, request.excludingDateList.size());

		// 勤怠明細を検証
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 3));
			System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 4));
			System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 5));
			System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
	}

	/**
	 * 申請：正常系
	 * 週跨ぎ（休日・法定休日を含む場合）の欠勤申請を行える事を検証する。
	 */
	@isTest static void submitOverWeekend() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 7);
		param.endDate = AppDate.newInstance(2018, 9, 10);
		param.reason = '欠勤理由';

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.ABSENCE, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 7), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 10), request.endDate);
		System.assertEquals(AppRequestStatus.PENDING, request.status);
		System.assertEquals(AppCancelType.NONE, request.cancelType);
		System.assertEquals(false, request.isConfirmationRequired);
		System.assertEquals(AppDate.newInstance(2018, 9, 8), request.excludingDateList[0]);
		System.assertEquals(AppDate.newInstance(2018, 9, 9), request.excludingDateList[1]);

		// 勤怠明細を検証
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 7));
			System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 所定休日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 8));
			System.assertEquals(null, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 法定休日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 9));
			System.assertEquals(null, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 10));
			System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
	}

	/**
	 * 申請：正常系
	 * 月跨ぎの欠勤申請を行える事を検証する。
	 */
	@isTest static void submitOverMonth() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 8, 31);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.ABSENCE, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 8, 31), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.endDate);
		System.assertEquals(AppRequestStatus.PENDING, request.status);
		System.assertEquals(AppCancelType.NONE, request.cancelType);
		System.assertEquals(false, request.isConfirmationRequired);
		System.assertEquals(AppDate.newInstance(2018, 9, 1), request.excludingDateList[0]);
		System.assertEquals(AppDate.newInstance(2018, 9, 2), request.excludingDateList[1]);

		// 勤怠明細を検証
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 8, 31));
			System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 所定休日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 1));
			System.assertEquals(null, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 法定休日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 2));
			System.assertEquals(null, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 3));
			System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
			System.assertEquals(null, attRecord.reqAbsenceRequestId);
		}
	}

	/**
	 * 申請：異常系
	 * 勤務体系.欠勤申請を使用するオプションがOffの場合、欠勤申請出来ないことを検証する。
	 */
	@isTest static void submitUnuseAbsenceOption() {
		// 欠勤申請を使用しない
		TestData testData = new TestData();
		update new AttWorkingTypeHistory__c(Id = testData.workingType.getHistoryList()[0].id, UseAbsenceApply__c = false);

		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_NotPermittedAbsenceRequest, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 欠勤開始日 > 欠勤終了日の場合、欠勤申請出来ないことを検証する。
	 */
	@isTest static void submitInvalidDate() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 4);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_InvalidStartEndDate, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 欠勤開始日〜終了日に勤務日が含まれない場合、欠勤申請出来ないことを検証する。
	 */
	@isTest static void submitNoWorkday() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 1);
		param.endDate = AppDate.newInstance(2018, 9, 2);
		param.reason = '欠勤理由';

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_NotWorkDay, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 欠勤開始日〜終了日に勤務確定した日が含まれている場合、欠勤申請出来ないことを検証する。
	 */
	@isTest static void submitLockedSummary() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 8, 31);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（ロック）
		attService.getSummaryList(param.empId, param.startDate, param.endDate);
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, param.startDate);
		attRecord.isLocked = true;
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_Locked(new List<String>{ComMessage.msg().Att_Lbl_TimeSheet}), e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 欠勤開始日〜終了日に出退勤打刻済みの場合、欠勤申請出来ないことを検証する。
	 */
	@isTest static void submitStamped() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 5);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（打刻）
		AttRecordEntity attRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		attRecord.inpStartTime = AttTime.newInstance(9, 0);
		attRecord.inpStartStampTime = AttTime.newInstance(9, 0);
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_NotAllowInpWorkTimeOnAbsent, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：正常系
	 * 半日休暇を1件取得時に、欠勤申請出来る事を検証する。
	 */
	@isTest static void submitWithHalfLeave() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（休暇申請-半日）
		AttDailyRequestEntity leaveRequest = createLeaveRequest(AttLeaveRange.RANGE_AM);
		AttRecordEntity leaveRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		leaveRecord.reqLeave1RequestId = leaveRequest.id;
		leaveRecord.reqLeave1Range = leaveRequest.leaveRange;
		attRepository.saveRecordEntity(leaveRecord);

		// 申請
		Test.startTest();
		AttDailyRequestEntity absenceRequest = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.ABSENCE, absenceRequest.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), absenceRequest.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), absenceRequest.endDate);
		System.assertEquals('欠勤理由', absenceRequest.reason);
		System.assertEquals(AppRequestStatus.PENDING, absenceRequest.status);
		System.assertEquals(AppCancelType.NONE, absenceRequest.cancelType);
		System.assertEquals(false, absenceRequest.isConfirmationRequired);

		// 勤怠明細を検証
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, param.startDate);
		System.assertEquals(absenceRequest.id, attRecord.reqRequestingAbsenceRequestId);
		System.assertEquals(null, attRecord.reqAbsenceRequestId);
		System.assertEquals(leaveRequest.id, attRecord.reqLeave1RequestId);
		System.assertEquals(null, attRecord.reqRequestingLeave1RequestId);
	}

	/**
	 * 申請：異常系
	 * 全日休暇取得時に、欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithDayLeave() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（休暇申請-全日）
		AttDailyRequestEntity leaveRequest = createLeaveRequest(AttLeaveRange.RANGE_DAY);
		AttRecordEntity leaveRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		leaveRecord.reqLeave1RequestId = leaveRequest.id;
		leaveRecord.reqLeave1Range = leaveRequest.leaveRange;
		attRepository.saveRecordEntity(leaveRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			String expected = ComMessage.msg().Att_Err_RequestedDayLeave;
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 申請：異常系
	 * 休暇を2件取得時に、欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithLeaves() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（休暇申請-半日2件）
		AttRecordEntity leaveRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		leaveRecord.reqLeave1RequestId = createLeaveRequest(AttLeaveRange.RANGE_AM).id;
		leaveRecord.reqLeave2RequestId = createLeaveRequest(AttLeaveRange.RANGE_PM).id;
		attRepository.saveRecordEntity(leaveRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_LeaveRequestsAlreadyExist, e.getMessage());
		}
	}

	/**
	 * 申請：異常系
	 * 振替休日なしの休日出勤日に、欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithHolidayWork() {
		// 休日出勤申請
		TestData testData = new TestData();
		AttHolidayWorkRequestService.SubmitParam holidayWorkParam = new AttHolidayWorkRequestService.SubmitParam();
		holidayWorkParam.empId = testData.employee.id;
		holidayWorkParam.targetDate = AppDate.newInstance(2018, 9, 1);
		holidayWorkParam.startTime = AttTime.newInstance(11, 00);
		holidayWorkParam.endTime = AttTime.newInstance(15, 00);
		AttDailyRequestEntity holidayWorkRequest = new AttDailyRequestService().submitRequest(holidayWorkParam, AttRequestType.HOLIDAYWORK);

		// 休日出勤申請承認
		holidayWorkRequest.status = AppRequestStatus.APPROVED;
		requestRepository.saveEntity(holidayWorkRequest);

		// 欠勤申請
		AttAbsenceRequestService.SubmitParam absenceParam = new AttAbsenceRequestService.SubmitParam();
		absenceParam.empId = testData.employee.id;
		absenceParam.startDate = AppDate.newInstance(2018, 9, 1);
		absenceParam.endDate = AppDate.newInstance(2018, 9, 1);
		absenceParam.reason = '欠勤理由';
		Test.startTest();
		try {
			requestService.submit(absenceParam);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_NotWorkDay, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：正常系
	 * 振替休日ありの休日出勤日に、欠勤申請出来る事を検証する。
	 */
	@isTest static void submitWithHolidayWorkSubstitute() {
		// 休日出勤申請
		TestData testData = new TestData();
		AttHolidayWorkRequestService.SubmitParam holidayWorkParam = new AttHolidayWorkRequestService.SubmitParam();
		holidayWorkParam.empId = testData.employee.id;
		holidayWorkParam.targetDate = AppDate.newInstance(2018, 9, 1); // 所定休日
		holidayWorkParam.startTime = AttTime.newInstance(11, 00);
		holidayWorkParam.endTime = AttTime.newInstance(15, 00);
		holidayWorkParam.substituteLeaveType = AttSubstituteLeaveType.SUBSTITUTE;
		holidayWorkParam.substituteDate = AppDate.newInstance(2018, 9, 3); // 勤務日
		AttDailyRequestEntity holidayWorkRequest = new AttDailyRequestService().submitRequest(holidayWorkParam, AttRequestType.HOLIDAYWORK);

		// 休日出勤申請承認
		holidayWorkRequest.status = AppRequestStatus.APPROVED;
		requestRepository.saveEntity(holidayWorkRequest);

		// 欠勤申請
		AttAbsenceRequestService.SubmitParam absenceParam = new AttAbsenceRequestService.SubmitParam();
		absenceParam.empId = testData.employee.id;
		absenceParam.startDate = AppDate.newInstance(2018, 9, 1);
		absenceParam.endDate = AppDate.newInstance(2018, 9, 1);
		absenceParam.reason = '欠勤理由';

		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(absenceParam);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.ABSENCE, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 1), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 1), request.endDate);
		System.assertEquals('欠勤理由', request.reason);
		System.assertEquals(AppRequestStatus.PENDING, request.status);
		System.assertEquals(AppCancelType.NONE, request.cancelType);
		System.assertEquals(false, request.isConfirmationRequired);

		// 勤怠明細を検証
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, absenceParam.startDate);
		System.assertEquals(request.id, attRecord.reqRequestingAbsenceRequestId);
		System.assertEquals(null, attRecord.reqAbsenceRequestId);
	}

	/**
	 * 申請：異常系
	 * 振替休日に欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithSubstitute() {
		// 休日出勤申請
		TestData testData = new TestData();
		AttHolidayWorkRequestService.SubmitParam holidayWorkParam = new AttHolidayWorkRequestService.SubmitParam();
		holidayWorkParam.empId = testData.employee.id;
		holidayWorkParam.targetDate = AppDate.newInstance(2018, 9, 1); // 所定休日
		holidayWorkParam.startTime = AttTime.newInstance(11, 00);
		holidayWorkParam.endTime = AttTime.newInstance(15, 00);
		holidayWorkParam.substituteLeaveType = AttSubstituteLeaveType.SUBSTITUTE;
		holidayWorkParam.substituteDate = AppDate.newInstance(2018, 9, 3); // 勤務日
		AttDailyRequestEntity holidayWorkRequest = new AttDailyRequestService().submitRequest(holidayWorkParam, AttRequestType.HOLIDAYWORK);

		// 休日出勤申請承認
		holidayWorkRequest.status = AppRequestStatus.APPROVED;
		requestRepository.saveEntity(holidayWorkRequest);

		// 欠勤申請
		AttAbsenceRequestService.SubmitParam absenceParam = new AttAbsenceRequestService.SubmitParam();
		absenceParam.empId = testData.employee.id;
		absenceParam.startDate = AppDate.newInstance(2018, 9, 3);
		absenceParam.endDate = AppDate.newInstance(2018, 9, 3);
		absenceParam.reason = '欠勤理由';
		Test.startTest();
		try {
			requestService.submit(absenceParam);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_NotWorkDay, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 早朝勤務日に欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithEarlyStartWork() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（早朝勤務申請）
		AttRecordEntity attRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		attRecord.reqEarlyStartWorkRequestId = createEarlyStartWorkRequest().id;
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_RequestedEarlyStartWork, e.getMessage());
		}
	}

	/**
	 * 申請：異常系
	 * 残業申請日に欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithOvertimeWork() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（残業申請）
		AttRecordEntity attRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		attRecord.reqRequestingOvertimeWorkRequestId = createOverTimeaWorkRequest().id;
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_RequestedOvertimeWork, e.getMessage());
		}
	}
	/**
	 * 申請：異常系
	 * 直行直帰申請日に欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithDirect() {
		TestData testData = new TestData();
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 4);
		param.reason = '欠勤理由';

		// 勤怠明細を更新（残業申請）
		AttRecordEntity attRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		attRecord.inpRequestingDirectRequestId = createOverTimeaWorkRequest().id; // ダミ
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_RequestedDirect, e.getMessage());
		}
	}
	/**
	 * 申請：異常系
	 * 欠勤申請日に欠勤申請出来ない事を検証する。
	 */
	@isTest static void submitWithAbsence() {
		// 欠勤申請
		TestData testData = new TestData();
		AppDate absenceDate = AppDate.newInstance(2018, 9, 3);
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = absenceDate;
		param.endDate = absenceDate;
		param.reason = '欠勤理由';
		requestService.submit(param);

		// 同日に申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_RequestedAbsence, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 申請期間が長すぎる場合は申請できないことを確認する
	 */
	@isTest static void submitWithLongPeriod() {
		// 欠勤申請
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.startDate = AppDate.newInstance(2018, 10, 1);
		param.endDate = AppDate.newInstance(2018, 11, 1);
		param.reason = '欠勤理由';

		// 申請期間は32日間で申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expMsg = ComMessage.msg().Att_Err_RequesePeriodTooLong(new List<String>{'31'});
			System.assertEquals(expMsg, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * createCancelMailMessageのテスト
	 * 承認取消メールが正常に作成できることを確認する
	 */
	@isTest static void createCancelMailMessageTest() {

		AttAbsenceRequestService service = new AttAbsenceRequestService();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Flex);
		EmployeeBaseEntity testEmp1 = testData.employee;
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// 欠勤申請のテスト
		AttDailyRequestEntity request = new AttDailyRequestEntity();
		request.requestTime = AppDatetime.valueOf(Datetime.newInstance(2018, 10, 1));
		request.startDate = AppDate.newInstance(2018, 10, 1);
		request.endDate = AppDate.newInstance(2018, 10, 3);
		request.reason = 'テスト理由';

		// 実行
		MailService.MailParam result = service.createCancelMailMessage(request, testEmp1, testEmp2, AppLanguage.JA);
		// 検証
		// 送信者名
		System.assertEquals('adminTest_L0 太郎_L0', result.senderDisplayName);
		// メールタイトル
		System.assertEquals('001_L0 太郎_L0さんの欠勤の承認が取消されました', result.subject);
		// メール本文
		System.assertEquals('001_L0 太郎_L0さんの欠勤の承認がadminTest_L0 太郎_L0さんによって取消されました'
				+ '\r\n\r\n'
				+ '【申請内容】\r\n'
				+ '申請日付：2018/10/01\r\n'
				+ '申請種別：欠勤\r\n'
				+ '期間：2018/10/01－2018/10/03\r\n'
				+ '理由：テスト理由\r\n'
				+ '\r\n---\r\n'
				+ 'チームスピリット', result.body);
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が無効の場合、アプリ例外が発生することを確認する
	 */
	@isTest
	static void selfApprovePermissionTestFailure() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 欠勤申請を使用する
		testData.workingType.getHistory(0).useAbsenceApply = true;
		new AttWorkingTypeRepository().saveEntity(testData.workingType);
		// 各種勤怠申請自己承認(本人)の権限を無効に設定する
		testData.permission.isApproveSelfAttDailyRequestByEmployee = false;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		AppDate absenceDate = AppDate.newInstance(2018, 9, 3);
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testEmp1.id;
		param.startDate = absenceDate;
		param.endDate = absenceDate;
		param.reason = '欠勤理由';

		// 実行
		System.runAs(AttTestData.getUser(testEmp1.userId)) {
			App.NoPermissionException actEx;
			try {
				AttDailyRequestEntity request = requestService.submit(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
			String expectMessage = ComMessage.msg().Att_Err_NotAllowSelfAttDailyRequest(new List<String>{testEmp1.fullNameL.getFullName()});
			System.assertEquals(expectMessage, actEx.getMessage());
		}
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が有効の場合、アプリ例外が発生しないことを確認する
	 */
	@isTest
	static void selfApprovePermissionTestSuccess() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 欠勤申請を使用する
		testData.workingType.getHistory(0).useAbsenceApply = true;
		new AttWorkingTypeRepository().saveEntity(testData.workingType);
		// 各種勤怠申請自己承認(本人)の権限を有効に設定する
		testData.permission.isApproveSelfAttDailyRequestByEmployee = true;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		AppDate absenceDate = AppDate.newInstance(2018, 9, 3);
		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();
		param.empId = testEmp1.id;
		param.startDate = absenceDate;
		param.endDate = absenceDate;
		param.reason = '欠勤理由';

		// 実行
		System.runAs(AttTestData.getUser(testEmp1.userId)) {
			App.NoPermissionException actEx;
			try {
				AttDailyRequestEntity request = requestService.submit(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertEquals(null, actEx);
		}
	}

	/**
	 * createRequestNameのテスト
	 * 申請名が正しく作成できることを確認する
	 */
	@isTest static void createRequestNameTest() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp = testData.employee;

		AttAbsenceRequestService.SubmitParam param = new AttAbsenceRequestService.SubmitParam();

		param.startDate = AppDate.newInstance(2018, 6, 5);
		param.endDate = AppDate.newInstance(2018, 6, 6);

		AttAbsenceRequestService service = new AttAbsenceRequestService();

		// 日本語の場合
		AppLanguage lang = AppLanguage.JA;
		// 実行
		String requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		System.assertEquals('001 太郎1_L0さん 欠勤申請 20180605-20180606', requestName);

		// 英語の場合
		lang = AppLanguage.EN_US;
		// 実行
		requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		// ComTestDataUtility.createEmployeesWithHistoryテストデータの表示名の作成メソッドが不正なので名＋半角スペース＋姓にならない
		System.assertEquals('001 太郎1_L1 Absence Request 20180605-20180606', requestName);
	}

	/**
	 * 申請取消：正常系
	 * 申請済みの欠勤申請を取消出来る事を検証する。
	 */
	@isTest static void cancelRequest() {}

	/**
	 * 再申請：正常系
	 * 申請取消済みの欠勤申請を再申請出来る事を検証する。
	 */
	@isTest static void submitAfterCancel() {}

	/**
	 * 申請削除：正常系
	 * 申請取消済みの欠勤申請を削除出来る事を検証する。
	 */
	@isTest static void removeRequest() {}

	/**
	 * 休暇申請を作成し保存する。
	 * @param 休暇範囲
	 * @return 登録したエンティティ（登録したIDを保持したエンティティ）
	 */
	private static AttDailyRequestEntity createLeaveRequest(AttLeaveRange range) {
		AttDailyRequestEntity request = new AttDailyRequestEntity();
		request.requestType = AttRequestType.LEAVE;
		request.leaveRange = range;
		Repository.SaveResult result = requestRepository.saveEntity(request);
		request.setId(result.details[0].id);
		return request;
	}

	/**
	 * 早朝勤務申請を作成し保存する。
	 * @return 登録したエンティティ（登録したIDを保持したエンティティ）
	 */
	private static AttDailyRequestEntity createEarlyStartWorkRequest() {
		AttDailyRequestEntity request = new AttDailyRequestEntity();
		request.requestType = AttRequestType.EARLY_START_WORK;
		request.startTime = AttTime.newInstance(7,00);
		request.endTime = AttTime.newInstance(8,00);
		Repository.SaveResult result = requestRepository.saveEntity(request);
		request.setId(result.details[0].id);
		return request;
	}

	/**
	 * 残業申請を作成し保存する。
	 * @return 登録したエンティティ（登録したIDを保持したエンティティ）
	 */
	private static AttDailyRequestEntity createOverTimeaWorkRequest() {
		AttDailyRequestEntity request = new AttDailyRequestEntity();
		request.requestType = AttRequestType.OVERTIME_WORK;
		request.startTime = AttTime.newInstance(20,00);
		request.endTime = AttTime.newInstance(21,00);
		Repository.SaveResult result = requestRepository.saveEntity(request);
		request.setId(result.details[0].id);
		return request;
	}
}