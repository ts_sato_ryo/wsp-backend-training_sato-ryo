/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * ComResourcePermissionクラスのテスト
 */
@isTest
private class ComResourcePermissionTest {

	/** 権限リポジトリ */
	private static PermissionRepository permissionRepo = new PermissionRepository();

	/** テストデータクラス */
	private class TestData extends TestData.TestDataEntity {
		private PermissionRepository permissionRepo = new PermissionRepository();

		public TestData() {
			super();
			// 明示的に社員データを作成しておく
			this.employee = this.employee;
		}
	}

	/**
	 * 未実装の権限が指定された場合、App.UnsupportedExceptionが発生することを確認する
	 */
	@isTest static void hasExecutePermissionTestInvalidPermission() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.UnsupportedException actEx;
			ComResourcePermission.Permission requiredPermission = null;

			actEx = null;
			testData.permission.isManageTimeWorkCategory = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.UnsupportedException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
		}
	}

	/**
	 * 全体設定の管理の権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageOverallSettingTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			List<ComResourcePermission.Permission> requiredPermissionList
					= new List<ComResourcePermission.Permission>();
			requiredPermissionList.add(ComResourcePermission.Permission.MANAGE_OVERALL_SETTING);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageOverallSetting = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermissionList);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageOverallSetting = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermissionList);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 部署の管理の権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageDepartmentTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			List<ComResourcePermission.Permission> requiredPermissionList
					= new List<ComResourcePermission.Permission>();
			requiredPermissionList.add(ComResourcePermission.Permission.MANAGE_DEPARTMENT);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageDepartment = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermissionList);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageDepartment = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermissionList);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 社員の管理の権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageEmployeeTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
					= ComResourcePermission.Permission.MANAGE_EMPLOYEE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageEmployee = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageEmployee = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * カレンダーの管理の権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageCalendarTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
					= ComResourcePermission.Permission.MANAGE_CALENDAR;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageCalendar = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageCalendar = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * ジョブタイプの管理の権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageJobTypeTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
					= ComResourcePermission.Permission.MANAGE_JOB_TYPE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageJobType = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageJobType = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * ジョブの管理の権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageJobTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
					= ComResourcePermission.Permission.MANAGE_JOB;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageJob = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageJob = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * モバイル機能設定の管理の権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageMobileSettingTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
					= ComResourcePermission.Permission.MANAGE_MOBILE_SETTING;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageMobileSetting = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageMobileSetting = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * アクセス権限の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManagePermissionTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
					= ComResourcePermission.Permission.MANAGE_PERMISSION;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManagePermission = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManagePermission = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageAccountingPeriodTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_ACCOUNTING_PERIOD;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpAccountingPeriod = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpAccountingPeriod = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageCostCenterTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_COST_CENTER;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpCostCenter = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpCostCenter = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageExchangeRateTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_EXCHANGE_RATE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpExchangeRate = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpExchangeRate = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageExtendedItemTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_EXTENDED_ITEM;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpExtendedItem = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpExtendedItem = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageReportTypeTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_REPORT_TYPE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpReportType = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpReportType = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageExpSettingTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_SETTING;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpSetting = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpSetting = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageTaxTypeTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_TAX_TYPE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpTaxType = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpTaxType = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageExpTypeGroupTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_TYPE_GROUP;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpTypeGroup = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpTypeGroup = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageExpenseTypeTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_TYPE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpType = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpType = false;
			testData.permission.isManageExpType = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageVendorTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_VENDOR;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpVendor = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpVendor = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageEmployeeGroupTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_EMPLOYEE_GROUP;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpEmployeeGroup = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpEmployeeGroup = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	@isTest static void hasManageCustomHintTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			ComResourcePermission.Permission requiredPermission
				= ComResourcePermission.Permission.MANAGE_EXP_CUSTOM_HINT;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageExpCustomHint = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageExpCustomHint = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new ComResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}
}