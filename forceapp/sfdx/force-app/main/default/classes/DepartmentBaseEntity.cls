/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署エンティティを表すクラス
 */
public with sharing class DepartmentBaseEntity extends DepartmentBaseGeneratedEntity {
	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;

	/**
	 * デフォルトコンストラクタ
	 */
	public DepartmentBaseEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public DepartmentBaseEntity(ComDeptBase__c sobj) {
		super(sobj);
	}

	/** 履歴エンティティのリスト */
	@testVisible
	protected List<DepartmentHistoryEntity> historyList;

	/** 会社(参照専用) */
	public ParentChildBaseCompanyEntity.Company company {
		get {
			if (company == null) {
				company = new ParentChildBaseCompanyEntity.Company(sobj.CompanyId__r.Code__c);
			}
			return company;
		}
		private set;
	}

	/**
	 * 履歴エンティティリストから指定したIDの履歴を取得する
	 * @return 履歴エンティティ、ただし存在しない場合はnull
	 */
	public DepartmentHistoryEntity getHistoryById(Id historyId) {
		return (DepartmentHistoryEntity)super.getSuperHistoryById(historyId);
	}

	/**
	 * 履歴エンティティリストを取得する
	 * @return 履歴エンティティリスト
	 */
	public List<DepartmentHistoryEntity> getHistoryList() {
		return historyList;
	}

	/**
	 * 履歴エンティティをリストに追加する
	 * @param history 追加する履歴エンティティ
	 */
	 public void addHistory(DepartmentHistoryEntity history) {
		 if (historyList == null) {
		 	historyList = new List<DepartmentHistoryEntity>();
		 }
		 historyList.add(history);
	 }

	 /**
	  * 部署履歴リストを取得する
	  * @return 部署名。履歴が存在しない場合はnull
	  */
	 public DepartmentHistoryEntity getHistory(Integer index) {
		 if (historyList == null) {
			 return null;
		 }
		 return historyList[index];
	 }

	 /**
	  * 履歴の基底クラスに変換する
	  */
	 protected override List<ParentChildHistoryEntity> convertSuperHistoryList() {
		 return (List<ParentChildHistoryEntity>)historyList;
	 }

	/**
	 * 有効期間終了日に相当する項目のラベルを取得する
	 * @return 有効期間終了日に相当する項目のラベル
	 */
	public override String getValidEndDateLabel() {
		return ComMessage.msg().Admin_Lbl_AbolishmentDate;
	}

	/**
	 * 指定した日に有効な履歴を取得する
	 * @param targetDate 対象日
	 * @return 有効な履歴、ただし有効な履歴が存在しない場合はnull
	 */
	public DepartmentHistoryEntity getHistoryByDate(AppDate targetDate) {
		return (DepartmentHistoryEntity)getSuperHistoryByDate(targetDate);
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public DepartmentBaseEntity copy() {
		DepartmentBaseEntity copyEntity = new DepartmentBaseEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildBaseEntity copySuperEntity() {
		return copy();
	}
}
