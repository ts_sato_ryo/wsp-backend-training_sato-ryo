/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group プランナー
 *
 * @description プランイベントのリポジトリ
 */
 public with sharing class PlanEventRepository extends Repository.BaseRepository {

	/** 取得対象の項目（イベント） */
	private static final Set<Schema.SObjectField> SELECT_FIELDS_PLAN_EVENT;
	/** 取得対象の項目（リレーション項目） */
	private static final Map<Schema.DescribeFieldResult, Set<Schema.SObjectField>> SELECT_FIELDS_RELATION;
	static {
		SELECT_FIELDS_PLAN_EVENT = new Set<Schema.SObjectField> {
				PlanEvent__c.Id,
				PlanEvent__c.OwnerId,
				PlanEvent__c.Subject__c,
				PlanEvent__c.StartDateTime__c,
				PlanEvent__c.EndDateTime__c,
				PlanEvent__c.IsAllDay__c,
				PlanEvent__c.IsOrganizer__c,
				PlanEvent__c.Location__c,
				PlanEvent__c.Description__c,
				PlanEvent__c.IsOuting__c,
				PlanEvent__c.CreatedServiceBy__c,
				PlanEvent__c.JobId__c,
				PlanEvent__c.WorkCategoryId__c,
				PlanEvent__c.ContactId__c
		};

		SELECT_FIELDS_RELATION = new Map<Schema.DescribeFieldResult, Set<Schema.SObjectField>> {
			// ジョブ
			PlanEvent__c.JobId__c.getDescribe() => new Set<Schema.SObjectField> {
				ComJob__c.Id,
				ComJob__c.Name,
				ComJob__c.ValidFrom__c,
				ComJob__c.ValidTo__c,
				ComJob__c.Code__c,
				ComJob__c.UniqKey__c,
				ComJob__c.CompanyId__c,
				ComJob__c.Name_L0__c,
				ComJob__c.Name_L1__c,
				ComJob__c.Name_L2__c,
				ComJob__c.DepartmentBaseId__c,
				ComJob__c.ParentId__c,
				ComJob__c.JobOwnerBaseId__c,
				ComJob__c.DirectCharged__c,
				ComJob__c.SelectabledTimeTrack__c
			},
			// 作業分類
			PlanEvent__c.WorkCategoryId__c.getDescribe() => new Set<Schema.SObjectField> {
				TimeWorkCategory__c.Id,
				TimeWorkCategory__c.Name,
				TimeWorkCategory__c.Code__c,
				TimeWorkCategory__c.UniqKey__c,
				TimeWorkCategory__c.Name_L0__c,
				TimeWorkCategory__c.Name_L1__c,
				TimeWorkCategory__c.Name_L2__c,
				TimeWorkCategory__c.CompanyId__c,
				TimeWorkCategory__c.Order__c,
				TimeWorkCategory__c.ValidFrom__c,
				TimeWorkCategory__c.ValidTo__c
			},
			// 取引先責任者
			PlanEvent__c.ContactId__c.getDescribe() => new Set<Schema.SObjectField> {
				Contact.Name
			}
		};
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** ID */
		public Set<Id> ids;
		/** 所有者 */
		public Set<Id> ownerIds;
		/** 開始日時 */
		public Datetime startDateTime;
		/** 終了日時 */
		public Datetime endDateTime;
		/** 開始時、終了時に有効なイベントを検索する場合true */
		public Boolean isOverwrap = false;
	}

	/** FOR UPDATE でクエリしない */
	private static final Boolean NOT_FOR_UPDATE = false;

	/**
	 * 指定したIDのエンティティを検索する
	 * @param eventId 取得対象のID
	 * @return 検索結果 該当するレコードが存在しない場合はnull
	 */
	public PlanEventEntity getEntity(Id eventId) {
		PlanEventRepository.SearchFilter filter = new PlanEventRepository.SearchFilter();
		filter.ids = new Set<Id>{eventid};
		List<PlanEventEntity> entityList = searchEntityList(filter, NOT_FOR_UPDATE);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したイベント一覧（開始日の昇順）
	 */
	public List<PlanEventEntity> searchEntityList(PlanEventRepository.SearchFilter filter) {
		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(PlanEventEntity entity) {
		return saveEntityList(new List<PlanEventEntity>{entity});
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<PlanEventEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<PlanEventEntity> entityList, Boolean allOrNone) {
		List<PlanEvent__c> events = new List<PlanEvent__c>();
		for (PlanEventEntity entity : entityList) {
			events.add(createObject(entity));
		}
		// 保存
		List<Database.UpsertResult> result = AppDatabase.doUpsert(events, allOrNone);
		return resultFactory.createSaveResult(result);
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したイベント一覧
	 */
	private List<PlanEventEntity> searchEntityList(PlanEventRepository.SearchFilter filter, Boolean forUpdate) {
		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELDS_PLAN_EVENT) {
			selectFldList.add(getFieldName(fld));
		}
		for (Schema.DescribeFieldResult relation : SELECT_FIELDS_RELATION.keySet()) {
			for (Schema.SObjectField fld : SELECT_FIELDS_RELATION.get(relation)) {
				selectFldList.add(getFieldName(relation.getRelationshipName(), fld));
			}
		}

		List<String> conditions = new List<String>();
		Set<Id> ids = filter.ids;
		if (ids != null) {
			conditions.add(getFieldName(PlanEvent__c.Id) + ' IN :ids');
		}
		Set<Id> ownerIds = filter.ownerIds;
		if (ownerIds != null) {
			conditions.add(getFieldName(PlanEvent__c.OwnerId) + ' IN :ownerIds');
		}
		// 開始日時
		Datetime startDateTime = filter.startDateTime;
		if (startDateTime != null) {
			if (filter.isOverwrap) {
				conditions.add(getFieldName(PlanEvent__c.EndDateTime__c) + ' >= :startDateTime');
			} else {
				conditions.add(getFieldName(PlanEvent__c.StartDateTime__c) + ' >= :startDateTime');
			}
		}
		// 終了日時
		Datetime endDateTime = filter.endDateTime;
		if (endDateTime != null) {
			if (filter.isOverwrap) {
				conditions.add(getFieldName(PlanEvent__c.StartDateTime__c) + ' <= :endDateTime');
			} else {
				conditions.add(getFieldName(PlanEvent__c.EndDateTime__c) + ' <= :endDateTime');
			}
		}

		// 以下は明確な要件が不明のため、動的な切替は行わない
		conditions.add(getFieldName(PlanEvent__c.IsRecurrence__c) + ' = false');
		conditions.add(getFieldName(PlanEvent__c.ResponseStatus__c) + ' = NULL OR ' +
						getFieldName(PlanEvent__c.ResponseStatus__c) + ' = \'Accepted\'');

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + PlanEvent__c.SObjectType.getDescribe().getName()
				+ buildWhereString(conditions)
				+ ' ORDER BY ' + getFieldName(PlanEvent__c.StartDateTime__c) + ' Asc';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('PlanEventRepository.searchEntityList: SOQL ==>' + soql);
		List<PlanEvent__c> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<PlanEventEntity> entityList = new List<PlanEventEntity>();
		for (PlanEvent__c sObj : sObjList) {
			entityList.add(createEntity(sObj));
		}
		return entityList;
	}

	/**
	 * PlanEvent__cからPlanEventEntityを作成する
	 * @param obj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	@TestVisible
	private PlanEventEntity createEntity(PlanEvent__c obj) {
		PlanEventEntity entity = new PlanEventEntity();
		entity.setId(obj.Id);
		entity.ownerId = obj.OwnerId;
		entity.subject = obj.Subject__c;
		entity.startDateTime = AppDatetime.valueOf(obj.StartDateTime__c);
		entity.endDateTime = AppDatetime.valueOf(obj.EndDateTime__c);
		entity.isAllDay = obj.IsAllDay__c;
		entity.isOrganizer = obj.IsOrganizer__c;
		entity.location = obj.Location__c;
		entity.description = obj.Description__c;
		entity.isOuting = obj.IsOuting__c;
		entity.createdServiceBy = entity.valueOfServiceType(obj.CreatedServiceBy__c);
		entity.contactId = obj.ContactId__c;
		entity.contactName = obj.ContactId__r.Name;

		// ジョブ
		if (obj.JobId__c != null) {
			entity.job = new JobEntity();
			entity.job.setId(obj.JobId__c);
			entity.job.code = obj.JobId__r.Code__c;
			entity.job.nameL0 = obj.JobId__r.Name_L0__c;
			entity.job.nameL1 = obj.JobId__r.Name_L1__c;
			entity.job.nameL2 = obj.JobId__r.Name_L2__c;
		}

		// 作業分類
		if (obj.WorkCategoryId__c != null) {
			entity.workCategory = new WorkCategoryEntity();
			entity.workCategory.setId(obj.WorkCategoryId__c);
			entity.workCategory.code = obj.WorkCategoryId__r.Code__c;
			entity.workCategory.nameL0 = obj.WorkCategoryId__r.Name_L0__c;
			entity.workCategory.nameL1 = obj.WorkCategoryId__r.Name_L1__c;
			entity.workCategory.nameL2 = obj.WorkCategoryId__r.Name_L2__c;
		}
		return entity;
	}

	/**
	 * PlanEventEntityからPlanEvent__cを作成する
	 * @param obj 作成元のエンティティ
	 * @return 作成したSObject
	 */
	@TestVisible
	private PlanEvent__c createObject(PlanEventEntity entity) {
		PlanEvent__c obj = new PlanEvent__c();
		obj.Id = entity.id;
		if (entity.isChanged(PlanEventEntity.Field.Subject)) {
			obj.Subject__c = entity.Subject;
		}
		if (entity.isChanged(PlanEventEntity.Field.StartDateTime)) {
			obj.StartDateTime__c = entity.startDateTime == null ? null : entity.startDateTime.getDatetime();
		}
		if (entity.isChanged(PlanEventEntity.Field.EndDateTime)) {
			obj.EndDateTime__c = entity.endDateTime == null ? null : entity.endDateTime.getDatetime();
		}
		if (entity.isChanged(PlanEventEntity.Field.IsAllDay)) {
			obj.IsAllDay__c = entity.isAllDay;
		}
		if (entity.isChanged(PlanEventEntity.Field.IsOrganizer)) {
			obj.IsOrganizer__c = entity.isOrganizer;
		}
		if (entity.isChanged(PlanEventEntity.Field.Location)) {
			obj.Location__c = entity.location;
		}
		if (entity.isChanged(PlanEventEntity.Field.Description)) {
			obj.Description__c = entity.description;
		}
		if (entity.isChanged(PlanEventEntity.Field.IsOuting)) {
			obj.IsOuting__c = entity.isOuting;
		}
		if (entity.isChanged(PlanEventEntity.Field.Job)) {
			obj.JobId__c = entity.job == null ? null : entity.job.id;
		}
		if (entity.isChanged(PlanEventEntity.Field.WorkCategory)) {
			obj.WorkCategoryId__c = entity.workCategory == null ? null : entity.workCategory.id;
		}
		if (entity.isChanged(PlanEventEntity.Field.CreatedServiceBy)) {
			obj.CreatedServiceBy__c = entity.createdServiceBy == null ? null : entity.createdServiceBy.name();
		}
		if (entity.isChanged(PlanEventEntity.Field.ContactId)) {
			obj.ContactId__c = entity.contactId;
		}
		if (entity.isChanged(PlanEventEntity.Field.LastModifiedService)) {
			obj.LastModifiedService__c = entity.lastModifiedService == null ? null : entity.lastModifiedService.name();
		}
		if (entity.isChanged(PlanEventEntity.Field.LastModifiedDateTime)) {
			obj.LastModifiedDateTime__c = entity.lastModifiedDateTime.getDatetime();
		}
		return obj;
	}
}