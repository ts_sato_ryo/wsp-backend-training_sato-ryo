/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 経費申請のトリガーハンドラー
 */
public with sharing class ExpReportRequestTriggerHandler extends RequestTriggerHandler {

	public override Set<Id> getApprovedRequestIds() {
		return approvedRequestMap.keySet();
	}

	public override Set<Id> getDisabledRequestIds() {
		return disabledRequestMap.keySet();
	}

	private Map<Id, ExpReportRequest__c> approvedRequestMap = new Map<Id, ExpReportRequest__c>();
	private Map<Id, ExpReportRequest__c> disabledRequestMap = new Map<Id, ExpReportRequest__c>();

	/**
	 * コンストラクタ
	 * @param oldMap
	 * @param newMap
	 */
	public ExpReportRequestTriggerHandler(Map<Id, ExpReportRequest__c> oldMap, Map<Id, ExpReportRequest__c> newMap) {
		super();
		this.init(oldMap, newMap);
	}

	/**
	 * 申請データを更新する
	 */
	public override void applyProcessInfo() {
		// 承認履歴を取得
		Set<Id> targetObjectIds = new Set<Id>();
		targetObjectIds.addAll(approvedRequestIds);
		targetObjectIds.addAll(disabledRequestIds);
		Map<Id, ProcessInstanceStep> processInfoMap = getProcessInfo(targetObjectIds);

		// 承認済みになった申請レコードを更新
		for (ExpReportRequest__c request : approvedRequestMap.values()) {
			if (!processInfoMap.containsKey(request.Id)) {
				continue;
			}

			ProcessInstanceStep pis = processInfoMap.get(request.Id);
			request.LastApproverId__c = pis.ActorId;
			request.LastApproveTime__c = pis.CreatedDate;
			request.ProcessComment__c = pis.Comments;
		}

		// 無効になった申請レコードを更新
		for (ExpReportRequest__c request : disabledRequestMap.values()) {
			request.ConfirmationRequired__c = true;

			if (!processInfoMap.containsKey(request.Id)) {
				continue;
			}

			ProcessInstanceStep pis = processInfoMap.get(request.Id);
			// 却下
			if (pis.StepStatus == STEP_STATUS_REJECTED) {
				request.CancelType__c = AppCancelType.REJECTED.value;
				request.ProcessComment__c = pis.Comments;
			}
			// 申請取消
			else if (pis.StepStatus == STEP_STATUS_REMOVED) {
				request.CancelType__c = AppCancelType.REMOVED.value;
				request.CancelComment__c = pis.Comments;
			}
		}
	}

	/**
	 * 承認済み・無効になった経費申請IDを取得する
	 * @param oldMap
	 * @param newMap
	 */
	private void init(Map<Id, ExpReportRequest__c> oldMap, Map<Id, ExpReportRequest__c> newMap) {
		for (Id requestId : newMap.keySet()) {
			ExpReportRequest__c requestNew = newMap.get(requestId);
			ExpReportRequest__c requestOld = oldMap.get(requestId);

			// 承認済み
			if (requestNew.Status__c == ApprovalService.Status.Approved.name() &&
				requestOld.Status__c != ApprovalService.Status.Approved.name())
			{
				approvedRequestMap.put(requestNew.Id, requestNew);
				continue;
			}
			// 無効
			if (requestNew.Status__c == ApprovalService.Status.Disabled.name() &&
				requestOld.Status__c != ApprovalService.Status.Disabled.name())
			{
				disabledRequestMap.put(requestNew.Id, requestNew);
				continue;
			}
		}
	}
}