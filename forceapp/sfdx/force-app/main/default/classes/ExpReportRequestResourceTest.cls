/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description ExpReportRequestResourceのテストクラス
 */
@isTest
private class ExpReportRequestResourceTest {
	
	/**
	 * 経費承認申請可否チェックAPIのテスト Test for CheckExpReportRequestApi
	 */
	@isTest static void checkExpReportRequestApiTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType, testData.employee, 3);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});

		Test.startTest();

		// リクエストパラメータ Request parameter
		ExpReportRequestResource.CheckExpReportRequestParam param = new ExpReportRequestResource.CheckExpReportRequestParam();
		param.reportId = report.id;

		// API実行 Execute API
		ExpReportRequestResource.CheckExpReportRequestApi api = new ExpReportRequestResource.CheckExpReportRequestApi();
		ExpReportRequestResource.CheckExpReportRequestResponse res = (ExpReportRequestResource.CheckExpReportRequestResponse)api.execute(param);

		Test.stopTest();

		System.assertEquals(0, res.confirmation.size());
	}

	/**
	 * 経費承認申請可否チェックAPIのテスト Test for CheckExpReportRequestApi
	 * バリデーションが正しく行われることを確認する Confirm validation is done correctly
	 */
	@isTest static void checkExpReportRequestApiTestValidation() {

		Test.startTest();

		// 経費精算IDが未指定 Report ID is not set
		ExpReportRequestResource.SubmitExpReportRequestParam param1 = new ExpReportRequestResource.SubmitExpReportRequestParam();
		try {
			param1.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, 'An unexpected exception occurred.' + e.getMessage());
		}

		// 経費精算IDが不正 Report ID is invalid
		ExpReportRequestResource.SubmitExpReportRequestParam param2 = new ExpReportRequestResource.SubmitExpReportRequestParam();
		param2.reportId = 'test';
		try {
			param2.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, 'An unexpected exception occurred.' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 経費承認申請可否チェックAPIのテスト Test for CheckExpReportRequestApi
	 * エラーが発生した場合は例外がスローされることを確認する Confirm that exeption is thrown, if an error occurs
	 */
	@isTest static void checkExpReportRequestApiTestError() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType, testData.employee, 3);

		recordList[0].recordType = ExpRecordType.TRANSIT_JORUDAN_JP;
		new ExpRecordRepository().saveEntityList(recordList);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});

		Test.startTest();

		// リクエストパラメータ Request parameter
		ExpReportRequestResource.CheckExpReportRequestParam param = new ExpReportRequestResource.CheckExpReportRequestParam();
		param.reportId = report.id;

		// API実行 Execute API
		try {
			ExpReportRequestResource.CheckExpReportRequestApi api = new ExpReportRequestResource.CheckExpReportRequestApi();
			api.execute(param);

			System.assert(false, 'Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NO_EVIDENCE, e.getErrorCode());
			System.assertEquals(ComMessage.msg().Exp_Err_NoEvidence, e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 経費承認申請APIのテスト
	 */
	@isTest static void submitExpReportRequestApiTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee,
				testData.job, testData.costCenter.CurrentHistoryId__c, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c, 3);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});
		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		Test.startTest();

		// リクエストパラメータ
		ExpReportRequestResource.SubmitExpReportRequestParam param = new ExpReportRequestResource.SubmitExpReportRequestParam();
		param.reportId = report.id;
		param.comment = 'コメント';

		// API実行
		ExpReportRequestResource.SubmitExpReportRequestApi api = new ExpReportRequestResource.SubmitExpReportRequestApi();
		api.execute(param);

		Test.stopTest();

		// 申請レコードを取得
		ExpReportRequest__c requestObj = [
				SELECT
					Id,
					Name,
					ExpReportId__c,
					TotalAmount__c,
					ReportNo__c,
					Subject__c,
					Comment__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					Approver01Id__c
				FROM ExpReportRequest__c
				WHERE ExpReportId__c = :report.id];

		System.assertEquals(report.id, requestObj.ExpReportId__c);
		System.assertEquals(report.totalAmount, requestObj.TotalAmount__c);
		System.assertEquals('exp00000001', requestObj.ReportNo__c);
		System.assertEquals(report.subject, requestObj.Subject__c);
		System.assertEquals(param.comment, requestObj.Comment__c);
		System.assertEquals(AppRequestStatus.PENDING.value, requestObj.Status__c);
		System.assertEquals(false, requestObj.ConfirmationRequired__c);
		System.assertEquals(null, requestObj.CancelType__c);
		System.assertEquals(report.employeeHistoryId, requestObj.EmployeeHistoryId__c);
		System.assertEquals(report.departmentHistoryId, requestObj.DepartmentHistoryId__c);
		System.assertEquals(testData.employee.getHistory(0).id, requestObj.ActorHistoryId__c);
		System.assertEquals(manager.userId, requestObj.Approver01Id__c);
	}

	/**
	 * 経費承認申請APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void submitExpReportRequestApiTestValidation() {

		Test.startTest();

		// 経費精算IDが未指定
		ExpReportRequestResource.SubmitExpReportRequestParam param1 = new ExpReportRequestResource.SubmitExpReportRequestParam();
		try {
			param1.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 経費精算IDが不正
		ExpReportRequestResource.SubmitExpReportRequestParam param2 = new ExpReportRequestResource.SubmitExpReportRequestParam();
		param2.reportId = 'test';
		try {
			param2.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 経費承認申請APIのテスト
	 * エラーが発生した場合はロールバックされることを確認する
	 */
	@isTest static void submitExpReportRequestApiTestRollback() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		Test.startTest();

		// リクエストパラメータ
		ExpReportRequestResource.SubmitExpReportRequestParam param = new ExpReportRequestResource.SubmitExpReportRequestParam();
		param.reportId = report.id;
		param.comment = 'コメント';

		// API実行
		try {
			ExpReportRequestResource.SubmitExpReportRequestApi api = new ExpReportRequestResource.SubmitExpReportRequestApi();
			api.isRollbackTest = true;
			api.execute(param);

			System.assert(false, '例外が発生しませんでした。');
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());

			// ロールバックされていることを確認する
			// 1件も経費申請データが作成されていないはず
			List<ExpReportRequest__c> requestList = [SELECT Id, Name FROM ExpReportRequest__c];
			System.assertEquals(true, requestList.isEmpty());
		}

		Test.stopTest();
	}
	
	/**
	 * 経費精算機能が利用できない場合にエラーが返ることを確認する - SubmitExpReportRequestApi
	 */
	@isTest static void SubmitExpReportRequestApiTestCanNotUse() {

		ExpTestData testData = new ExpTestData();
		//経費申請機能をOFFにする
		testData.changeUseExpense(false);
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		Test.startTest();

		// リクエストパラメータ
		ExpReportRequestResource.SubmitExpReportRequestParam param = new ExpReportRequestResource.SubmitExpReportRequestParam();
		param.reportId = report.id;
		param.comment = 'コメント';

		try {
			// API実行
			ExpReportRequestResource.SubmitExpReportRequestApi api = new ExpReportRequestResource.SubmitExpReportRequestApi();
			api.execute(param);

			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * @description 経費承認取消処理のテスト
	 */
	@isTest
	static void CancelExpReportRequestApiTest() {
		// テストデータ作成
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType, testData.employee, 3);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});

		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// リクエストパラメータ
		ExpReportRequestResource.SubmitExpReportRequestParam param = new ExpReportRequestResource.SubmitExpReportRequestParam();
		param.reportId = report.id;
		param.comment = 'コメント';

		// API実行
		ExpReportRequestResource.SubmitExpReportRequestApi api = new ExpReportRequestResource.SubmitExpReportRequestApi();
		api.execute(param);

		// 申請レコードを取得
		ExpReportRequest__c requestObj = [
				SELECT
					Id,
					Name,
					ExpReportId__c,
					TotalAmount__c,
					ReportNo__c,
					Subject__c,
					Comment__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					Approver01Id__c
				FROM ExpReportRequest__c
				WHERE ExpReportId__c = :report.id];

		Test.startTest();

		// リクエストパラメータ
		ExpReportRequestResource.CancelExpReportRequestParam cancelParam = new ExpReportRequestResource.CancelExpReportRequestParam();
		cancelParam.requestId = requestObj.Id;
		cancelParam.comment = '取消コメント';

		// 経費承認申請取り消しAPI実行
		ExpReportRequestResource.CancelExpReportRequestApi cancelApi = new ExpReportRequestResource.CancelExpReportRequestApi();
		cancelApi.execute(cancelParam);

		// ステータスを更新
		ExpReportRequestRepository repo = new ExpReportRequestRepository();
		ExpReportRequestEntity requestEntity = repo.getEntity(requestObj.Id);
		requestEntity.status = AppRequestStatus.DISABLED;
		requestEntity.cancelType = AppCancelType.REMOVED;
		repo.saveEntity(requestEntity);

		Test.stopTest();

		// 申請データを取得
		ExpReportRequest__c expReportReq = [
			SELECT
					Id,
					Status__c,
					CancelType__c,
					ConfirmationRequired__c,
					CancelComment__c
			FROM ExpReportRequest__c
			WHERE id = :requestObj.Id
			LIMIT 1];

		System.assertEquals(true, expReportReq.ConfirmationRequired__c);
	}
}