/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Service class for Currency
 */
public with sharing class CurrencyService {

	private static final Boolean CHECK_BY_ID = true;
	private static final Boolean CHECK_BY_CODE = false;

	/**
	 * Create a new Currency record
	 * @param entity Entity to be saved
	 * @return Repository.SaveResult
	 */
	public static Repository.SaveResult createCurrency(CurrencyEntity entity) {

		validateEntity(entity);

		// If target record exists, throw error.
		if (isExistRecord(entity, CHECK_BY_CODE)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		CurrencyRepository rep = new CurrencyRepository();
		return rep.saveEntity(entity);
	}

	/**
	 * Update a new Currency record
	 * @param entity Entity to be updated
	 * @return
	 */
	public static void updateCurrency(CurrencyEntity entity) {

		// Validate entity value
		validateEntity(entity);

		// If target record dose not exist, throw error.
		if (!isExistRecord(entity, CHECK_BY_ID)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_RecordNotFound);
		}

		// If target the code record exists, throw error.
		if (isExistRecord(entity, CHECK_BY_CODE)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		CurrencyRepository rep = new CurrencyRepository();
		rep.saveEntity(entity);
	}

	/**
   * Delete a new Currency record
   * @param entity Entity to be deleted
   * @return
   */
	public static void deleteCurrency(Id id) {

		CurrencyEntity entity = new CurrencyEntity();
		entity.setId(id);

		// If target record has been deleted, do nothing
		if (!isExistRecord(entity, CHECK_BY_ID)) return;

		if (isBaseCurrency(entity.id)) {
			throw new App.IllegalStateException(ComMessage.msg().Com_Err_CannotDeleteReference);
		}

		CurrencyRepository rep = new CurrencyRepository();
		try {
			rep.deleteEntity(id);
		} catch (DmlException e) {
			if (e.getDmlStatusCode(0) == StatusCode.DELETE_FAILED.name()) {
				throw new App.IllegalStateException(ComMessage.msg().Com_Err_CannotDeleteReference);
			} else {
				throw e;
			}
		}
	}

	/**
	 * Search a Currency record by record ID
	 * @param id Target record ID
	 * @return List of fetched Currency records
	 */
	public static List<CurrencyEntity> searchCurrency(Id id, Id companyId) {
		CurrencyRepository.SearchFilter filter = new CurrencyRepository.SearchFilter();
		filter.ids = String.isNotBlank(id) ? new Set<Id>{ id } : null;
		filter.companyId = String.isNotBlank(companyId) ? companyId  : null;

		return new CurrencyRepository().searchEntity(filter);
	}

	/**
	 * Validate entity
	 * @param entity Target entity to be validated
	 */
	@TestVisible
	private static void validateEntity(CurrencyEntity entity) {
		Validator.Result validResult = new ComConfigValidator.CurrencyValidator(entity).validate();
		if (!validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	@TestVisible
	private static Boolean isExistRecord(CurrencyEntity entity, Boolean isCheckById) {
		CurrencyRepository.SearchFilter filter = new CurrencyRepository.SearchFilter();
		if (isCheckById) {
			filter.ids = new Set<Id>{ entity.id };
			List<CurrencyEntity> result = new CurrencyRepository().searchEntity(filter);
			return (result.size() > 0);
		} else {
			CurrencyEntity result = new CurrencyRepository().getEntity(entity.code);
			return (result != null) && (entity.id <> result.id);
		}
	}

	/*
	 * Check if the target currency is used as a base currency.
	 * @param currencyId target currency ID
	 */
	@TestVisible
	private static Boolean isBaseCurrency(Id currencyId) {
		List<CompanyEntity> companyList = new CompanyRepository().searchEntityList(new CompanyRepository.SearchFilter());
		for (CompanyEntity company: companyList) {
			if (company.currencyId == currencyId) {
				return true;
			}
		}
		return false;
	}
}