/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * @description 勤務体系履歴エンティティ
 */
public with sharing class AttWorkingTypeHistoryEntity extends AttWorkingTypeHistoryGeneratedEntity {

	/**
	 * デフォルトコンストラクタ
	 */
	public AttWorkingTypeHistoryEntity() {
		super();
		// initFieldValue();
	}

	/**
	 * コンストラクタ
	 */
	public AttWorkingTypeHistoryEntity(AttWorkingTypeHistory__c sobj) {
		super(sobj);
		// initFieldValue();
	}

	// /**
	//  * 項目値を初期化する
	//  */
	// private void initFieldValue() {
	// 	// 休暇コードリスト
	// 	// setFieldValue(FIELD_LEAVE_CODE_LIST, convertCodeListValueToList((String)getFieldValue(FIELD_LEAVE_CODE_LIST)));
	// 	// // 勤務パターンコードリスト
	// 	// setFieldValue(FIELD_PATTERN_CODE_LIST, convertCodeListValueToList((String)getFieldValue(FIELD_PATTERN_CODE_LIST)));
	// 	// // 休日出勤の代わりにとる休暇
	// 	// setFieldValue(FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE, AttSubstituteLeaveType.valuesOf((String)getFieldValue(FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE)));
	// }


	/** 勤怠システム 参照のみ */
	public AttWorkSystem workSystem {
		get {
			if (workSystem == null) {
				workSystem = AttWorkSystem.valueOf(sobj.BaseId__r.WorkSystem__c);
			}
			return workSystem;
		}
		private set;
	}

	/**
	 * @description (テスト用)workSystemを設定する
	 */
	@testVisible
	private void setWorkSystem(AttWorkSystem value) {
		this.workSystem = value;
	}

	/** 勤務体系名(多言語) 参照のみ */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	// 管理画面では設定できない非公開項目

	//---------------------------------------
	// TODO divideWorkHoursAtDayBoundaryが文字列型に変更されたため勤務体系バリデータでチェックする
	//---------------------------------------
	/** 2暦日をまたぐ勤怠計算方法 */
	public void setDivideWorkHoursAtDayBoundary(AttCalcLogic.ClassificationNextDayWork type) {
		this.divideWorkHoursAtDayBoundary = type == null ? null : type.name();
	}

	/**
	 * 文字列に対応したDivideWorkHoursAtDayBoundaryを取得する
	 */
	private AttCalcLogic.ClassificationNextDayWork getDivideWorkHoursAtDayBoundary(String value) {
		if (value == null) {
			return null;
		}
		for (AttCalcLogic.ClassificationNextDayWork enm : AttCalcLogic.ClassificationNextDayWork.values()) {
			if (value == enm.name()) {
				return enm;
			}
		}
		return null;
	}


	//----------------------------------------
	// DBに保存されない項目
	//----------------------------------------
	/** 所定休憩時間(DBに保存されない) */
	public AttDailyTime restTimeWoStartEndTime {get; set;}
	/** 午前所定休憩時間(DBに保存されない) */
	public AttDailyTime amRestTimeWoStartEndTime {get; set;}
	/** 午後所定休憩時間(DBに保存されない) */
	public AttDailyTime pmRestTimeWoStartEndTime {get; set;}
	/** 所定休日の勤務も法定休日と同じ割増を払う(DBに保存されない) */
	public Boolean legalHolidayAllowanceForAll {get; set;}

// TODO:下記のオプション項目を管理機能で登録する
// // 所定時間まで申請無しで許可
// public Boolean permitOvertimeWorkUntilContructedHours {get; set;}
// // 法定休日の所定内勤務にも基本給を払う
// public Boolean basicSalaryForLegalHoliday {get; set;}
// // 所定休日の勤務は法定内も割増を払う
// public Boolean allowanceOnEveryHoliday {get; set;}
// // 所定休日の勤務も法定休日と同じ割増を払う
// public Boolean legalHolidayAllowanceForAll {get; set;}
// // 法定内残業も割増を払う
// public Boolean allowanceForOverContracted {get; set;}

	// 関連リスト系オプション

	/** 休暇コードリスト */
	public static final Schema.SObjectField FIELD_LEAVE_CODE_LIST = AttWorkingTypeHistory__c.LeaveCodeList__c;
	public List<String> leaveCodeList {
		get {
			if (isChanged(FIELD_LEAVE_CODE_LIST)) {
				return (List<String>)getFieldValue(FIELD_LEAVE_CODE_LIST);
			}
			List<String> valueList = convertCodeListValueToList(sobj.LeaveCodeList__c);
			setFieldValue(FIELD_LEAVE_CODE_LIST, valueList);
			return valueList;
		}
		set {
			setFieldValue(FIELD_LEAVE_CODE_LIST, value);
		}
	}

	/** 勤務パターンコードリスト */
	public static final Schema.SObjectField FIELD_PATTERN_CODE_LIST = AttWorkingTypeHistory__c.PatternCodeList__c;
	public List<String> patternCodeList {
		get {
			if (isChanged(FIELD_PATTERN_CODE_LIST)) {
				return (List<String>)getFieldValue(FIELD_PATTERN_CODE_LIST);
			}
			List<String> valueList = convertCodeListValueToList(sobj.PatternCodeList__c);
			setFieldValue(FIELD_PATTERN_CODE_LIST, valueList);
			return valueList;
		}
		set {
			setFieldValue(FIELD_PATTERN_CODE_LIST, value);
		}
	}

	/**
	 * @description SObjectに保存されているコードリストの値をList型に変換する
	 * @param value SObjedctの項目値
	 * @return 変換後のリスト。項目値がnullの場合は空のリストが返却される
	 * @throws App.TypeException 文字列がJSON形式でない場合
	 */
	private List<String> convertCodeListValueToList(String value) {
		if (value == null) {
			return new List<String>();
		}
		try {
			return (List<String>)JSON.deserializeStrict(value, List<String>.class);
		} catch (Exception e) {
			throw new App.TypeException(ComMessage.msg().Com_Err_InvalidFormat(new List<String>{ComMessage.msg().Admin_Lbl_WorkTimeLeaveList}));
		}
	}

	/**
	 * @description SObjectに保存されているコードリストの値をSObjectの項目値に変換する
	 * @param valueList コードのリスト
	 * @return 変換後のリスト
	 */
	private String convertCodeListValueToSObject(List<String> codeList) {
		if (codeList == null) {
			return null;
		}
		return JSON.serialize(codeList);
	}


	// 利用機能系オプション
	/** 休日出勤の代わりにとる休暇 */
	public static final Schema.SObjectField FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE = AttWorkingTypeHistory__c.AllocatedSubstituteLeaveType__c;
	public List<AttSubstituteLeaveType> allocatedSubstituteLeaveType {
		get {
			if (isChanged(FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE)) {
				return (List<AttSubstituteLeaveType>)getFieldValue(FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE);
			}
			List<AttSubstituteLeaveType> valueList = AttSubstituteLeaveType.valuesOf(sobj.AllocatedSubstituteLeaveType__c);
			// 空のリストの場合はnullを返却しているらしい
			valueList = (valueList == null || valueList.isEmpty()) ? null : valueList;
			setFieldValue(FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE, valueList);
			return (List<AttSubstituteLeaveType>)getFieldValue(FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE);
		}
		set {
			setFieldValue(FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE, value);
		}
	}

	/**
	 * @description 指定した勤務パターンが勤務体系で利用可能かどうかをチェックする
	 * @param patternCode チェック対象の勤務パターンコード
	 * @return 利用可能な勤務パターンの場合はtrue、そうでない場合はfalse
	 */
	public Boolean isAvailablePattern(String targetPatternCode) {
		if (this.patternCodeList == null) {
			return false;
		}
		for (String patternCode : this.patternCodeList) {
			if (targetPatternCode.equals(patternCode)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 勤務パターンエンティティを作成する
	 */
	public AttPatternEntity createPatternEntity() {
		AttPatternEntity pattern = new AttPatternEntity();
		pattern.startTime = this.startTime;
		pattern.endTime = this.endTime;
		pattern.contractedWorkHours = this.contractedWorkHours;
		pattern.rest1StartTime = this.rest1StartTime;
		pattern.rest1EndTime = this.rest1EndTime;
		pattern.rest2StartTime = this.rest2StartTime;
		pattern.rest2EndTime = this.rest2EndTime;
		pattern.rest3StartTime = this.rest3StartTime;
		pattern.rest3EndTime = this.rest3EndTime;
		pattern.rest4StartTime = this.rest4StartTime;
		pattern.rest4EndTime = this.rest4EndTime;
		pattern.rest5StartTime = this.rest5StartTime;
		pattern.rest5EndTime = this.rest5EndTime;
		// 午前半休
		pattern.useAMHalfDayLeave = this.useAMHalfDayLeave;
		pattern.amStartTime = this.amStartTime;
		pattern.amEndTime = this.amEndTime;
		pattern.amContractedWorkHours = this.amContractedWorkHours;
		pattern.amRest1StartTime = this.amRest1StartTime;
		pattern.amRest1EndTime = this.amRest1EndTime;
		pattern.amRest2StartTime = this.amRest2StartTime;
		pattern.amRest2EndTime = this.amRest2EndTime;
		pattern.amRest3StartTime = this.amRest3StartTime;
		pattern.amRest3EndTime = this.amRest3EndTime;
		pattern.amRest4StartTime = this.amRest4StartTime;
		pattern.amRest4EndTime = this.amRest4EndTime;
		pattern.amRest5StartTime = this.amRest5StartTime;
		pattern.amRest5EndTime = this.amRest5EndTime;
		// 午後半休
		pattern.usePMHalfDayLeave = this.usePMHalfDayLeave;
		pattern.pmStartTime = this.pmStartTime;
		pattern.pmEndTime = this.pmEndTime;
		pattern.pmContractedWorkHours = this.pmContractedWorkHours;
		pattern.pmRest1StartTime = this.pmRest1StartTime;
		pattern.pmRest1EndTime = this.pmRest1EndTime;
		pattern.pmRest2StartTime = this.pmRest2StartTime;
		pattern.pmRest2EndTime = this.pmRest2EndTime;
		pattern.pmRest3StartTime = this.pmRest3StartTime;
		pattern.pmRest3EndTime = this.pmRest3EndTime;
		pattern.pmRest4StartTime = this.pmRest4StartTime;
		pattern.pmRest4EndTime = this.pmRest4EndTime;
		pattern.pmRest5StartTime = this.pmRest5StartTime;
		pattern.pmRest5EndTime = this.pmRest5EndTime;
		//半休
		pattern.halfDayLeaveHours = this.halfDayLeaveHours;

		pattern.boundaryOfStartTime = this.boundaryOfStartTime;
		pattern.boundaryOfEndTime = this.boundaryOfEndTime;

		return pattern;
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public AttWorkingTypeHistoryEntity copy() {
		AttWorkingTypeHistoryEntity copyEntity = new AttWorkingTypeHistoryEntity();
		setAllFieldValues(copyEntity);

		// List型の項目をコピー
		copyEntity.leaveCodeList = this.leaveCodeList == null ? null : this.leaveCodeList.clone();
		copyEntity.patternCodeList = this.patternCodeList == null ? null : this.patternCodeList.clone();
		copyEntity.allocatedSubstituteLeaveType = this.allocatedSubstituteLeaveType == null ? null : this.allocatedSubstituteLeaveType.clone();

		// Entityで保持しているがにchangedValuesで管理されていない項目をコピー
		copyEntity.restTimeWoStartEndTime = this.restTimeWoStartEndTime;
		copyEntity.amRestTimeWoStartEndTime = this.amRestTimeWoStartEndTime;
		copyEntity.pmRestTimeWoStartEndTime = this.pmRestTimeWoStartEndTime;
		copyEntity.legalHolidayAllowanceForAll = this.legalHolidayAllowanceForAll;

		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildHistoryEntity copySuperEntity() {
		return copy();
	}

	/**
	 * 更新対象のSObjectを生成する。
	 * OutWorkTimeDetails__cは親クラスで定義していないので、更新時にJson文字列へ変換する。
	 */
	public override AttWorkingTypeHistory__c createSObject() {
		AttWorkingTypeHistory__c sobj = new AttWorkingTypeHistory__c(Id = super.id);
		for (Schema.SObjectField field : changedValues.keySet()) {
			if (field == FIELD_LEAVE_CODE_LIST
					|| field == FIELD_PATTERN_CODE_LIST
					|| field == FIELD_ALLOCATED_SUBSTITUTE_LEAVE_TYPE) {
				continue;
			}
			sobj.put(field, changedValues.get(field));
		}

		sobj.LeaveCodeList__c = convertCodeListValueToSObject(this.leaveCodeList);
		sobj.PatternCodeList__c = convertCodeListValueToSObject(this.patternCodeList);
		sobj.AllocatedSubstituteLeaveType__c = AppConverter.stringValue(this.allocatedSubstituteLeaveType);
		return sobj;
	}
}