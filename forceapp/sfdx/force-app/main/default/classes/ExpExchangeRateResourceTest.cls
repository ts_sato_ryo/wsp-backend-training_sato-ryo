/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for ExpExchangeRateRepository
 */
@isTest
private class ExpExchangeRateResourceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);
	static final ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('USD', 'US Dollar');

	/**
	 * Create a new ExchangeRate record(Positive case)
	 */
	@isTest
	static void createApiTest() {

		ExpExchangeRateResource.UpsertParam param = new ExpExchangeRateResource.UpsertParam();
		setTestParam(param);

		Test.startTest();

		ExpExchangeRateResource.createApi api = new ExpExchangeRateResource.createApi();
		ExpExchangeRateResource.SaveResult result = (ExpExchangeRateResource.SaveResult)api.execute(param);

		Test.stopTest();

		List<ExpExchangeRate__c> records =
			[SELECT
				Id,
				Code__c,
				CompanyId__c,
				CurrencyId__c,
				CurrencyPair__c,
				Rate__c,
				ReverseRate__c,
				ValidFrom__c,
				ValidTo__c
			FROM ExpExchangeRate__c
			WHERE Id =: result.id];

		//Confirm new record was created
		System.assertEquals(1, records.size());

		System.assertEquals(param.code, records[0].Code__c);
		System.assertEquals(param.companyId, records[0].CompanyId__c);
		System.assertEquals(param.currencyId, records[0].CurrencyId__c);
		System.assertEquals(param.currencyPair, records[0].CurrencyPair__c);
		System.assertEquals(param.rate, records[0].Rate__c);
		System.assertEquals(param.reverseRate, records[0].ReverseRate__c);
		System.assertEquals(param.validDateFrom, records[0].ValidFrom__c);
		System.assertEquals(param.validDateTo, records[0].ValidTo__c);
	}

	/**
   * Create a new ExchangeRate record(Negative case: Mandatory params are not set)
   */
	@isTest
	static void createApiParamErrTest() {

		ExpExchangeRateResource.UpsertParam param = new ExpExchangeRateResource.UpsertParam();
		setTestParam(param);

		ExpExchangeRateResource.CreateApi api = new ExpExchangeRateResource.CreateApi();
		App.ParameterException ex1;
		App.ParameterException ex2;
		App.ParameterException ex3;
		App.ParameterException ex4;

		Test.startTest();

		// Set code blank
		App.ParameterException ex;
		setTestParam(param);
		param.code = '';
		try {
			// Parameter error should occur
			api.execute(param);
		} catch (App.ParameterException e1) {
			ex1 = e1;
		}

		// Set company id blank
		setTestParam(param);
		param.companyId = '';
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e2) {
			ex2 = e2;
		}

		// Set currency id blank
		setTestParam(param);
		param.currencyId = '';
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e3) {
			ex3 = e3;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex1.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Com_Lbl_Code}), ex1.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex2.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'Company ID'}), ex2.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex3.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'Currency ID'}), ex3.getMessage());
	}

	/**
	   * Update a ExchangeRate record(Positive case)
	   */
	@isTest
	static void updateApiTest() {

		// Create a ExchangeRate record first
		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		ExpExchangeRateResource.UpsertParam param = new ExpExchangeRateResource.UpsertParam();
		setTestParam(param);
		param.id = testRecord.Id;

		Test.startTest();

		ExpExchangeRateResource.UpdateApi api = new ExpExchangeRateResource.UpdateApi();
		api.execute(param);

		Test.stopTest();

		List<ExpExchangeRate__c> records =
		[SELECT
			Id,
			Code__c,
			CompanyId__c,
			CurrencyId__c,
			CurrencyPair__c,
			Rate__c,
			ReverseRate__c,
			ValidFrom__c,
			ValidTo__c
			FROM ExpExchangeRate__c
			WHERE Id =: testRecord.id];

		//Confirm the record was updated
		System.assertEquals(1, records.size());

		System.assertEquals(param.code, records[0].Code__c);
		System.assertEquals(param.companyId, records[0].CompanyId__c);
		System.assertEquals(param.currencyId, records[0].CurrencyId__c);
		System.assertEquals(param.currencyPair, records[0].CurrencyPair__c);
		System.assertEquals(param.rate, records[0].Rate__c);
		System.assertEquals(param.reverseRate, records[0].ReverseRate__c);
		System.assertEquals(param.validDateFrom, records[0].ValidFrom__c);
		System.assertEquals(param.validDateTo, records[0].ValidTo__c);
	}

	/**
   * Update a ExchangeRate record(Negative case: Mandatory param is not set)
   */
	@isTest
	static void updateApiParamErrTest() {

		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		ExpExchangeRateResource.UpsertParam param = new ExpExchangeRateResource.UpsertParam();
		setTestParam(param);
		// Do not set ID

		Test.startTest();

		ExpExchangeRateResource.UpdateApi api = new ExpExchangeRateResource.UpdateApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), ex.getMessage());
	}

	/**
	 * Delete a ExchangeRate record(Positive case)
	 */
	@isTest
	static void deleteApiTest() {

		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		ExpExchangeRateResource.DeleteParam param = new ExpExchangeRateResource.DeleteParam();
		param.id = testRecord.Id;

		Test.startTest();

		ExpExchangeRateResource.DeleteApi api = new ExpExchangeRateResource.DeleteApi();
		api.execute(param);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ExpExchangeRate__c WHERE Id =: param.id]);
	}

	/**
	 * Delete a ExchangeRate record(Negative case)
	 */
	@isTest
	static void deleteApiParamErrTest() {

		ExpExchangeRateResource.DeleteParam param = new ExpExchangeRateResource.DeleteParam();

		Test.startTest();

		ExpExchangeRateResource.DeleteApi api = new ExpExchangeRateResource.DeleteApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), ex.getMessage());
	}

	/**
	 * Search a ExchangeRate record by ID(Positive case)
	 */
	@isTest
	static void searchApiTest() {

		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		Test.startTest();

		ExpExchangeRateResource.SearchParam param = new ExpExchangeRateResource.SearchParam();
		param.id = testRecord.id;

		ExpExchangeRateResource.SearchApi api = new ExpExchangeRateResource.SearchApi();
		ExpExchangeRateResource.SearchResult res = (ExpExchangeRateResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());

		ExpExchangeRateResource.ExpExchangeRateRecord result = res.records[0];
		System.assertEquals(testRecord.Id, result.id);

	}

	/**
	 */
	@isTest
	static void searchApiGetNameL1Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		Test.startTest();

		ExpExchangeRateResource.SearchParam param = new ExpExchangeRateResource.SearchParam();
		param.id = testRecord.id;

		ExpExchangeRateResource.SearchApi api = new ExpExchangeRateResource.SearchApi();
		ExpExchangeRateResource.SearchResult res = (ExpExchangeRateResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(currencyObj.Name_L1__c, res.records[0].currencyName);
	}

	/**
	 * Search a ExchangeRate record(Positive case: test for L1 name(multi lang))
	 */
	@isTest
	static void searchApiGetNameL2Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		Test.startTest();

		ExpExchangeRateResource.SearchParam param = new ExpExchangeRateResource.SearchParam();
		param.id = testRecord.id;

		ExpExchangeRateResource.SearchApi api = new ExpExchangeRateResource.SearchApi();
		ExpExchangeRateResource.SearchResult res = (ExpExchangeRateResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(currencyObj.Name_L2__c, res.records[0].currencyName);
	}

	/**
   * Get Currency pair list(Positive case)
   */
	@isTest
	static void searchIsoCurrencyCodeApiTest() {
		ExpExchangeRateResource.GetCurrencyPairApi api = new ExpExchangeRateResource.GetCurrencyPairApi();
		ExpExchangeRateResource.GetCurrencyPairResult res;

		res = (ExpExchangeRateResource.GetCurrencyPairResult)api.execute(new ExpExchangeRateResource.SearchParam());

		System.assert(res.records.size() > 0);
	}

	/**
	 * Set test parameter tp request parameter
	 * @param param Target object to set data
	 */
	static void setTestParam(ExpExchangeRateResource.UpsertParam param) {
		param.code = 'TEST';
		param.companyId = companyObj.Id;
		param.currencyId = currencyObj.Id;
		param.baseCurrencyCode = 'JPY';
		param.currencyCode = 'USD';
		param.currencyPair = 'USD_JPY';
		param.rate = 0.009091;
		param.reverseRate = (1 / param.rate).setScale(6);
		param.validDateFrom = Date.today();
		param.validDateTo = Date.today().addMonths(1);
		param.baseCurrencyCode='SGD';
	}

}
