/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 共通
  *
  * (廃止)チェックイン情報のエンティティ
  * このエンティティは廃止になりました。ComLocationEntity を使用してください。
  * This entity has been deprecated. Use ComLocationEntity instead.
  */
public with sharing class CheckInInfoEntity {
}