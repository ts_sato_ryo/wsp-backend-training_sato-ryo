/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * コンテンツドキュメントリンクエンティティ
 */
public with sharing class AppContentDocumentLinkEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		CONTENT_DOCUMENT_ID,
		LINKED_ENTITY_ID
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/** コンテンツドキュメントID */
	public Id contentDocumentId {
		get;
		set {
			contentDocumentId = value;
			setChanged(Field.CONTENT_DOCUMENT_ID);
		}
	}

	/** 関連先ID */
	public Id linkedEntityId {
		get;
		set {
			linkedEntityId = value;
			setChanged(Field.LINKED_ENTITY_ID);
		}
	}

	/** コンストラクタ */
	public AppContentDocumentLinkEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}