/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 工数
 *
 * @description 工数設定のサービスクラス
 */
public with sharing class TimeSettingService extends ParentChildService {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 工数設定リポジトリ */
	private TimeSettingRepository timeSettingRepo = new TimeSettingRepository();

	/** コンストラクタ */
	public TimeSettingService() {
		super(new TimeSettingRepository());
	}

	/**
	 * 全ての履歴を持った工数設定をMapで取得する。キーは工数設定のベースID
	 * @param baseIds 取得対象のベースID
	 */
	public Map<Id, TimeSettingBaseEntity> getBaseMap(List<Id> baseIds) {

		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAll = null;
		List<TimeSettingBaseEntity> bases = timeSettingRepo.getEntityList(baseIds, targetAll);

		// 検索しやすいようにMapに変換(キーはbaseId)
		Map<Id, TimeSettingBaseEntity> baseMap = new Map<Id, TimeSettingBaseEntity>();
		for (TimeSettingBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		return baseMap;
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntity(Id baseId) {
		// ベースに紐づく全ての履歴を取得する
		return timeSettingRepo.getEntity(baseId);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildBaseEntity> getEntityList(List<Id> baseIds) {
		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAllDate = null; // 全ての履歴が対象
		return timeSettingRepo.getEntityList(baseIds, targetAllDate);
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntityByCode(String code) {
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.codes = new Set<String>{code};
		List<TimeSettingBaseEntity> entityList = timeSettingRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildHistoryEntity getHistoryEntity(Id historyId) {
		// ベースに紐づく全ての履歴を取得する
		return timeSettingRepo.getHistoryEntity(historyId);
	}

	/**
	 * 指定した履歴IDの履歴エンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		// ベースに紐づく全ての履歴を取得する
		return timeSettingRepo.getHistoryEntityList(historyIds);
	}

	/**
	 * ベースエンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveBaseEntity(ParentChildBaseEntity base) {
		return (new TimeConfigValidator.TimeSettingBaseValidator((TimeSettingBaseEntity)base)).validate();
	}

	/**
	 * 履歴エンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveHistoryEntity(ParentChildBaseEntity base, ParentChildHistoryEntity history) {
		return (new TimeConfigValidator.TimeSettingHistoryValidator((TimeSettingBaseEntity)base, (TimeSettingHistoryEntity)history)).validate();
	}

	/**
	 * 履歴エンティティが持つnameL0を取得する
	 * @param history 取得対象の履歴エンティティ
	 * @return 履歴エンティティのnameL0、ただしnameL0を持っていない場合はnull
	 */
	protected override String getNameL0FromHistory(ParentChildHistoryEntity history) {
		return ((TimeSettingHistoryEntity)history).nameL0;
	}

	/**
	 * エンティティのコードが重複しているかどうかを確認する(会社単位での重複チェック)
	 * @param base チェック対象のベースエンティティ
	 * @param companyCode 会社コード
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	@TestVisible
	protected override Boolean isCodeDuplicated(ParentChildBaseEntity base) {
		TimeSettingBaseEntity empBase = (TimeSettingBaseEntity)base;
		TimeSettingBaseEntity duplicateBase = getEntityByCode(empBase.code, empBase.companyId);

		if (duplicateBase == null) {
			// 重複していない
			return false;
		}
		if (duplicateBase.id == empBase.id) {
			// 重複していない
			return false;
		}

		// ここまできたら重複している
		return true;
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	private TimeSettingBaseEntity getEntityByCode(String code, Id companyId) {
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.codes = new Set<String>{code};
		filter.companyIds = new Set<Id>{companyId};
		List<TimeSettingBaseEntity> entityList = timeSettingRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * エンティティのユニークキーを更新する
	 * (会社のコードが変更されている可能性があるため、常に更新を行う)
	 * @param base ユニークキーを作成するエンティティ
	 */
	public void updateUniqKey(TimeSettingBaseEntity base) {

		Id companyId = base.companyId;
		String code = base.code;

		// 既存のエンティティを更新する場合はエンティティに更新対象の項目以外は値が設定されていない可能性があるため
		// リポジトリから更新前のエンティティを取得する
		if ((base.id != null) && ((companyId == null) || (String.isBlank(code)))) {
			// 既存のエンティティを取得する
			TimeSettingBaseEntity orgBase = timeSettingRepo.getBaseEntity(base.id);
			if (orgBase == null) {
				// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			if (companyId == null) {
				companyId = orgBase.companyId;
			}
			if (String.isBlank(code)) {
				code = orgBase.code;
			}
		}

		if (companyId == null ) {
			// メッセージ：会社が設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Company}));
		}
		if (String.isBlank(code)) {
			// メッセージ：コードが設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}));
		}
		CompanyEntity company = getCompany(companyId);
		base.uniqKey = base.createUniqKey(company.code, code);
	}

	/**
	 * 対象日に社員が工数機能を利用できるか判定する。
	 * 利用できない場合は例外を投げる。
	 *
	 * @param employeeBaseId 判定対象の社員ID
	 * @param targetDate 対象日
	 * @IllegalStateException 対象日に有効な社員履歴が存在しない場合
	 *                        社員履歴に、工数設定が設定されていない場合
	 *                        対象日に有効な工数設定が存在しない場合
	 */
	public void validateTimeSetting(Id employeeBaseId, AppDate targetDate) {
		getTimeSetting(employeeBaseId, targetDate);
	}

	/**
	 * 対象日に社員が工数機能を利用できるか判定する。
	 * 利用できない場合は例外を投げる。
	 *
	 * @param employeeBaseId 判定対象の社員ID
	 * @param targetDateRange 対象期間
	 * @IllegalStateException 対象日に有効な社員履歴が存在しない場合
	 *                        社員履歴に、工数設定が設定されていない場合
	 *                        対象日に有効な工数設定が存在しない場合
	 */
	public void validateTimeSetting(Id employeeBaseId, AppDateRange targetDateRange) {
		getTimeSetting(employeeBaseId, targetDateRange);
	}

	/**
	 * 対象日の社員の工数設定を取得する。
	 * 取得出来ない場合は例外を投げる
	 *
	 * @param employeeBaseId 判定対象の社員ID
	 * @param targetDate 対象日
	 * @return 工数設定マスタ
	 * @IllegalStateException 対象日に有効な社員履歴が存在しない場合
	 *                        社員履歴に、工数設定が設定されていない場合
	 *                        対象日に有効な工数設定が存在しない場合
	 */
	 public TimeSettingBaseEntity getTimeSetting(Id employeeBaseId, AppDate targetDate) {
		return getTimeSetting(employeeBaseId, new AppDateRange(targetDate, targetDate))[0];
	}

	/**
	 * 履歴IDを条件に工数設定を取得する
	 * 論理削除済みの場合も取得対象とする
	 * @param historyId 工数設定履歴ID
	 * @return 指定した履歴を持つ工数設定
	 * @throws App.RecordNotFoundException 履歴IDに紐づく工数設定が存在しない場合
	 */
	public TimeSettingBaseEntity getTimeSettingByHistoryId(Id historyId) {
		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{historyId};
		filter.includeRemoved = true;
		List<TimeSettingBaseEntity> entities = repo.searchEntity(filter);

		// 取得できない場合はエラー
		if (entities.isEmpty()) {
			throw new App.RecordNotFoundException(
					App.ERR_CODE_RECORD_NOT_FOUND,
					ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Trac_Lbl_TimeTrackSetting}));
		}
		return entities[0];
	}

	/**
	 * 対象日の社員の工数設定を取得する。
	 * 取得出来ない場合は例外を投げる
	 *
	 * @param employeeBaseId 判定対象の社員ID
	 * @param targetDateRange 対象期間
	 * @return 対象期間の社員に設定された工数設定マスタ（有効な全履歴を含む）
	 *         対象期間内に社員に複数の工数設定が設定されている場合はその全てを返す
	 * @IllegalStateException 対象日に有効な社員履歴が存在しない場合
	 *                        社員履歴に、工数設定が設定されていない場合
	 *                        対象日に有効な工数設定が存在しない場合
	 */
	 @TestVisible
	private List<TimeSettingBaseEntity> getTimeSetting(Id employeeBaseId, AppDateRange targetDateRange) {
		//EmployeeServiceのキャッシュからとる
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity employee = empService.getEmployeeByRangeFromCache(employeeBaseId, targetDateRange);

		// 工数設定を取得
		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		Set<Id> idSet = new Set<Id>();
		for (EmployeeHistoryEntity entity : employee.getHistoryList()) {
			idSet.add(entity.timeSettingId);
		}
		filter.baseIds = idSet;
		Map<Id, TimeSettingBaseEntity> settingMap = repo.searchEntityMap(filter);

		// 検証
		for (AppDate targetDate : targetDateRange.values()) {
			// 社員の履歴を取得
			EmployeeHistoryEntity employeeHistory = employee.getHistoryByDate(targetDate);
			if (employeeHistory == null) {
				throw new App.IllegalStateException(
						App.ERR_CODE_NOT_FOUND_EMP,
						ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Com_Lbl_Employee}));
			}

			// 工数設定情報取得
			if (employeeHistory.timeSettingId == null) {
				// メッセージ：社員の工数設定が設定されていません。
				throw new App.IllegalStateException(
						App.ERR_CODE_NOT_SET_TIME_SETTING,
						ComMessage.msg().Trac_Err_FieldNotSet(new List<String>{
							ComMessage.msg().Com_Lbl_Employee,
							ComMessage.msg().Trac_Lbl_TimeTrackSetting}));
			}

			TimeSettingBaseEntity setting = settingMap.get(employeeHistory.timeSettingId);
			if (setting == null || setting.getSuperHistoryByDate(targetDate) == null) {
				// メッセージ：社員の工数設定が見つかりません。
				throw new App.IllegalStateException(
						App.ERR_CODE_NOT_FOUND_TIME_SETTING,
						ComMessage.msg().Trac_Err_NotFoundAOfB(new List<String>{
							ComMessage.msg().Com_Lbl_Employee,
							ComMessage.msg().Trac_Lbl_TimeTrackSetting}));
			}
		}
		return settingMap.values();
	}
}
