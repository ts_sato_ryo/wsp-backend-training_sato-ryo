/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Repository class for Custom Extended Item and Custom Extened Item Option
 * 
 * @group Expense
 */
public with sharing class ExtendedItemCustomRepository extends Repository.BaseRepository {
	/*
	 * Sorting Order
	 */
	public enum SortOrder {
		CODE_ASC, CODE_DESC
	}

	/*
	 * Sorting Order for ExtendedItemCustomOption
	 */
	public enum OptionSortOrder {
		CODE_ASC, CODE_DESC
	}

	private final static Boolean NOT_FOR_UPDATE = false;

	public static final Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** Pattern for detecting characters to unescape */
	private static final Pattern SPECIAL_CHARACTERS_PATTERN = Pattern.compile('[%\\_\\\\]');


	/*
	 * Fields of ComExtendedItemCustom__c to include in the SELECT query.
	 */
	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ComExtendedItemCustom__c.Id,
			ComExtendedItemCustom__c.Name,
			ComExtendedItemCustom__c.CompanyId__c,
			ComExtendedItemCustom__c.Code__c,
			ComExtendedItemCustom__c.Name_L0__c,
			ComExtendedItemCustom__c.Name_L1__c,
			ComExtendedItemCustom__c.Name_L2__c,
			ComExtendedItemCustom__c.UniqKey__c
		};
		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/*
	 * Criteria for searching the Custom Extended Items
	 */
	public class SearchFilter {
		/** Search ExtendedItemCustom by SF IDs */
		public Set<Id> ids;
		/** Search ExtendedItemCustom by CompanyIds*/
		public Set<Id> companyIds;
		/** Search ExtendedItemCustom by Code */
		public Set<String> codes;
	}

	/*
	 * Search ExtendedItemCustomEntity list. The order of the return records is not guaranteed and the records are not locked.
	 * Doesn't set the available Option values {@code ExtendedItemCustomOptionEntity} to the {@code ExtendedItemCustomEntity}
	 * @param filter SearchFilter containing the criteria.
	 *
	 * @return List<ExtendedItemCustomEntity> list of {@code ExtendedItemCustomEntity} without the Option values set.
	 */
	public List<ExtendedItemCustomEntity> searchEntity(ExtendedItemCustomRepository.SearchFilter filter) {
		return searchEntity(filter, null);
	}

	/*
	 * Search ExtendedItemCustomEntity list. The order of the return records is based on the specified order.
	 * The records are not locked.
	 * Doesn't set the available Option values {@code ExtendedItemCustomOptionEntity} to the {@code ExtendedItemCustomEntity}
	 *
	 * @param filter SearchFilter containing the criteria.
	 * @param order Sorting order for the returned entity list.
	 *
	 * @return List<ExtendedItemCustomEntity> list of {@code ExtendedItemCustomEntity} without the Option values.
	 */
	public List<ExtendedItemCustomEntity> searchEntity(ExtendedItemCustomRepository.SearchFilter filter, SortOrder order) {
		return searchEntity(filter, NOT_FOR_UPDATE, order);
	}

	/*
	 * Search ExtendedItemCustomEntity list.
	 *
	 * @param filter SearchFilter containing the criteria.
	 * @param forUpdate if True, the rows will be locked - cannot be used together with Order ({@code order} must be null}
	 * @param order Sorting order for the returned entity list - cannot be used together with forUpdate ({@code forUpdate} must be false
	 *
	 * @return List<ExtendedItemCustomEntity> list of {@code ExtendedItemCustomEntity} without the Option values.
	 */
	public List<ExtendedItemCustomEntity> searchEntity(ExtendedItemCustomRepository.SearchFilter filter, Boolean forUpdate, SortOrder order) {
		Set<Id> pIds;
		Set<Id> pCompanyIds;
		Set<String> pCodes;

		List<String> whereList = new List<String>();

		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ComExtendedItemCustom__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ComExtendedItemCustom__c.Code__c) + ' IN :pCodes');
		}

		String whereString = buildWhereString(whereList);

		String orderByString;
		if (order != null) {
			orderByString = ' ORDER BY ';
			if (order == SortOrder.CODE_ASC) {
				orderByString += getFieldName(ComExtendedItemCustom__c.Code__c) + ' ASC NULLS LAST';
			} else if (order == SortOrder.CODE_DESC) {
				orderByString += getFieldName(ComExtendedItemCustom__c.Code__c) + ' DESC NULLS LAST';
			}
		}

		List<String> selectFieldList = new List<String>();

		selectFieldList.addAll(GET_FIELD_NAME_LIST);

		String soql = 'SELECT ' + String.join(selectFieldList, ',')
			+ ' FROM ' + ComExtendedItemCustom__c.SObjectType.getDescribe().getName()
			+ whereString + orderByString;

		if (forUpdate == True) {
			soql += ' FOR UPDATE';
		}

		List<ComExtendedItemCustom__c> sObjs = (List<ComExtendedItemCustom__c>) Database.query(soql);

		List<ExtendedItemCustomEntity> entityList = new List<ExtendedItemCustomEntity>();
		for (ComExtendedItemCustom__c sObj : sObjs) {
			entityList.add(createEntity(sObj));
		}
		return entityList;
	}

	/*
	 * Convert the given {@code ComExtendedItemCustom__c} to {@code ExtendedItemCustomEntity}.
	 * @param sObject Source sObject
	 *
	 * @return ExtendedItemCustomEntity entity instance created from the sObject
	 */
	private ExtendedItemCustomEntity createEntity(ComExtendedItemCustom__c sObj) {
		ExtendedItemCustomEntity entity = new ExtendedItemCustomEntity();
		entity.setId(sObj.Id);
		entity.companyId = sObj.CompanyId__c;
		entity.name = sObj.Name;
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.code = sObj.Code__c;
		entity.uniqKey = sObj.UniqKey__c;

		entity.resetChanged();

		return entity;
	}

	private static final List<String> GET_OPTION_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ComExtendedItemCustomOption__c.Id,
			ComExtendedItemCustomOption__c.Name,
			ComExtendedItemCustomOption__c.ExtendedItemCustomId__c,
			ComExtendedItemCustomOption__c.Code__c,
			ComExtendedItemCustomOption__c.Name_L0__c,
			ComExtendedItemCustomOption__c.Name_L1__c,
			ComExtendedItemCustomOption__c.Name_L2__c,
			ComExtendedItemCustomOption__c.UniqKey__c
		};
		GET_OPTION_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/*
	 * Criteria for searching the Custom Extended Item Options
	 */
	public class OptionSearchFilter {
		/** Search ExtendedItemCustomOption by SF IDs */
		public Set<Id> ids;
		/** Search ExtendedItemCustomOption by extendedItemCustomIds */
		public Set<Id> extendedItemCustomIds;
		/** Search ExtendedItemCustomOption by Code */
		public Set<String> codes;
		/** Name or Code Search Query */
		public String nameOrCodeQuery;
	}

	/*
	 * Search ExtendedItemCustomOptionEntity list. The order of the return records is not guaranteed and the records are not locked.
	 * @param filter OptionSearchFilter containing the criteria.
	 *
	 * @return List<ExtendedItemCustomOptionEntity> list of {@code ExtendedItemCustomOptionEntity}
	 */
	public List<ExtendedItemCustomOptionEntity> searchOptionEntity(ExtendedItemCustomRepository.OptionSearchFilter filter) {
		return searchOptionEntity(filter, null, SEARCH_RECORDS_NUMBER_MAX);
	}

	/*
	 * Search ExtendedItemCustomOptionEntity list. The order of the return records is based on the specified order.
	 * The records are not locked.
	 *
	 * @param filter OptionSearchFilter containing the criteria.
	 * @param order Sorting order for the returned entity list.
	 * @param resultLimit controls the number of returned records if specified
	 *
	 * @return List<ExtendedItemCustomOptionEntity>
	 */
	public List<ExtendedItemCustomOptionEntity> searchOptionEntity(ExtendedItemCustomRepository.OptionSearchFilter filter, OptionSortOrder order, Integer resultLimit) {
		return searchOptionEntity(filter, NOT_FOR_UPDATE, order, resultLimit);
	}

	/*
	 * Search ExtendedItemCustomOptionEntity list.
	 *
	 * @param filter OptionSearchFilter containing the criteria.
	 * @param forUpdate if True, the rows will be locked - cannot be used together with Order ({@code order} must be null}
	 * @param order Sorting order for the returned entity list - cannot be used together with forUpdate ({@code forUpdate} must be false
	 * @param resultLimit controls the number of returned records if specified
	 *
	 * @return List<ExtendedItemCustomOptionEntity>
	 */
	public List<ExtendedItemCustomOptionEntity> searchOptionEntity(ExtendedItemCustomRepository.OptionSearchFilter filter, Boolean forUpdate, OptionSortOrder order, Integer resultLimit) {
		Set<Id> pIds;
		Set<Id> pExtendedItemCustomIds;
		Set<String> pCodes;
		String pNameOrCodeQuery;

		List<String> whereList = new List<String>();

		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		if (filter.extendedItemCustomIds != null) {
			pExtendedItemCustomIds = filter.extendedItemCustomIds;
			whereList.add(getFieldName(ComExtendedItemCustomOption__c.ExtendedItemCustomId__c) + ' IN :pExtendedItemCustomIds');
		}
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ComExtendedItemCustomOption__c.Code__c) + ' IN :pCodes');
		}
		if (String.isNotBlank(filter.nameOrCodeQuery)) {
			pNameOrCodeQuery = '%'+ escapeSpecialCharacters(filter.nameOrCodeQuery) + '%';
			whereList.add(
			getFieldName(ComExtendedItemCustomOption__c.Name_L0__c) + ' LIKE :pNameOrCodeQuery OR '
					+ getFieldName(ComExtendedItemCustomOption__c.Name_L1__c) + ' LIKE :pNameOrCodeQuery OR '
					+ getFieldName(ComExtendedItemCustomOption__c.Name_L2__c) + ' LIKE :pNameOrCodeQuery OR '
					+ getFieldName(ComExtendedItemCustomOption__c.Code__c) + ' LIKE :pNameOrCodeQuery');
		}

		String whereString = buildWhereString(whereList);

		String orderByString;
		if (order != null) {
			orderByString = ' ORDER BY ';
			if (order == OptionSortOrder.CODE_ASC) {
				orderByString += getFieldName(ComExtendedItemCustomOption__c.Code__c) + ' ASC NULLS LAST';
			} else if (order == OptionSortOrder.CODE_DESC) {
				orderByString += getFieldName(ComExtendedItemCustomOption__c.Code__c) + ' DESC NULLS LAST';
			}
		}

		String limitString = '';
		if (resultLimit != null && resultLimit > 0) {
			limitString = ' LIMIT :resultLimit';
		}

		List<String> selectFieldList = new List<String>();

		selectFieldList.addAll(GET_OPTION_FIELD_NAME_LIST);

		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ComExtendedItemCustomOption__c.SObjectType.getDescribe().getName()
				+ whereString + orderByString + limitString;

		if (forUpdate == True) {
			soql += ' FOR UPDATE';
		}

		List<ComExtendedItemCustomOption__c> sObjs = (List<ComExtendedItemCustomOption__c>) Database.query(soql);

		List<ExtendedItemCustomOptionEntity> entityList = new List<ExtendedItemCustomOptionEntity>();
		for (ComExtendedItemCustomOption__c sObj : sObjs) {
			entityList.add(createEntity(sObj));
		}
		return entityList;
	}

	/*
	 * Convert the given {@code ComExtendedItemCustomOption__c} to {@code ExtendedItemCustomOptionEntity}.
	 * @param sObject Source sObject
	 *
	 * @return ExtendedItemCustomOptionEntity entity instance created from the sObject
	 */
	private ExtendedItemCustomOptionEntity createEntity(ComExtendedItemCustomOption__c sObj) {
		ExtendedItemCustomOptionEntity entity = new ExtendedItemCustomOptionEntity();
		entity.setId(sObj.Id);
		entity.extendedItemCustomId = sObj.ExtendedItemCustomId__c;
		entity.name = sObj.Name;
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.code = sObj.Code__c;
		entity.uniqKey = sObj.UniqKey__c;

		entity.resetChanged();

		return entity;
	}

	/*
	 * Return an escaped version of the input.
	 * @param unEscapedString
	 *
	 * @retun escaped version of the string
	 */
	private static String escapeSpecialCharacters(String unEscapedString) {
		String[] chars = unEscapedString.split('');
		for (Integer i = 0; i < chars.size(); i++) {
			if (SPECIAL_CHARACTERS_PATTERN.matcher(chars[i]).matches()) {
				chars[i] = '\\' + chars[i];
			}
		}
		return String.join(chars, '');
	}
}