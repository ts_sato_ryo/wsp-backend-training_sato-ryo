/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group アプリ
 *
 * 言語の選択リスト値のテスト
 */
@isTest
private class AppLanguageTest {

	/**
	 * getUserLangのテスト
	 * ユーザの言語が取得できることを確認する
	 */
	@isTest static void getUserLangTest() {
		ComOrgSetting__c orgSetting = new ComOrgSetting__c(
				Language_0__c = 'ja',
				Language_1__c = 'en_US');
		insert orgSetting;

		// testUser2 は未対応の言語
		User testUser1 = ComTestDataUtility.createUser('Lang1', 'ja', UserInfo.getLocale());
		User testUser2 = ComTestDataUtility.createUser('Lang2', 'de', UserInfo.getLocale());

		System.runAs(testUser1) {
			System.assertEquals(AppLanguage.JA, AppLanguage.getUserLang());
		}

		System.runAs(testUser2) {
			System.assertEquals(AppLanguage.USER_DEFAULT_LANG, AppLanguage.getUserLang());
		}

	}

	/**
	 * getLangkeyのテスト
	 * 指定した言語のキーが取得できることを確認する
	 */
	@isTest static void getLangKeyTest() {
		ComOrgSetting__c orgSetting = new ComOrgSetting__c(
				Language_0__c = 'ja',
				Language_1__c = 'en_US');
		insert orgSetting;

		System.assertEquals(AppLanguage.LangKey.L0, AppLanguage.JA.getLangkey());
		System.assertEquals(AppLanguage.LangKey.L1, AppLanguage.EN_US.getLangkey());
	}

	/**
	 * getLangkeyのテスト
	 * 組織設定が登録されていない場合はnullが返却されることを確認する
	 */
	@isTest static void getLangKeyTestOrgNotFound() {

		// 組織設定を登録せずにキーを取得する
		System.assertEquals(null, AppLanguage.JA.getLangkey());
		
	}

	/**
	 * getLangByLangKeyのテスト
	 * 指定した言語キーの言語が取得できることを確認する
	 */
	@isTest static void getLangByLangKeyTest() {
		ComOrgSetting__c orgSetting = new ComOrgSetting__c(
				Language_0__c = 'ja',
				Language_1__c = 'en_US');
		insert orgSetting;

		System.assertEquals(AppLanguage.JA, AppLanguage.getLangByLangKey(AppLanguage.LangKey.L0));
		System.assertEquals(AppLanguage.EN_US, AppLanguage.getLangByLangKey(AppLanguage.LangKey.L1));
	}

	/**
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(AppLanguage.JA, AppLanguage.valueOf('ja'));
		System.assertEquals(AppLanguage.EN_US, AppLanguage.valueOf('en_US'));

		// 等しくない場合(値が異なる) → false
		System.assertEquals(false, AppLanguage.JA.equals(AppLanguage.EN_US));

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, AppLanguage.JA.equals(AttDayType.WORKDAY));

		// 等しくない場合(null)
		System.assertEquals(false, AppLanguage.JA.equals(null));
	}

}
