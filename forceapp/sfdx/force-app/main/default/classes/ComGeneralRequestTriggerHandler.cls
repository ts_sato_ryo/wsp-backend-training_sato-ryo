/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 汎用稟議申請のトリガーハンドラー
 */
public with sharing class ComGeneralRequestTriggerHandler extends RequestTriggerHandler {
	public override Set<Id> getApprovedRequestIds() {
		return approvedRequestMap.keySet();
	}
	public override Set<Id> getDisabledRequestIds() {
		return disabledRequestMap.keySet();
	}
	/** 承認待ちの申請（申請は1件ずつ対応する）*/
	public ComGeneralRequest__c pendingRequest;
	/** 承認済みの申請マップ*/
	private Map<Id, ComGeneralRequest__c> approvedRequestMap = new Map<id, ComGeneralRequest__c>();
	/** 無効の申請マップ*/
	private Map<Id, ComGeneralRequest__c> disabledRequestMap = new Map<Id, ComGeneralRequest__c>();

	public ComGeneralRequestTriggerHandler(Map<Id, ComGeneralRequest__c> oldMap, Map<Id, ComGeneralRequest__c> newMap) {
		super();
		this.init(oldMap, newMap);
	}

	/**
	 * 申請データに、画面からは設定できない申請情報を設定する
	 * 社員情報が取得できない場合は、エラーとする
	 * 部署情報が取得できない場合は、部署情報を設定しない
	 */
	public void applyGeneralRequestInfo() {
		if (pendingRequest == null) {
			// 該当する申請が見つかりません。（本来ありえないはずのエラー）
			throw new App.IllegalStateException(ComMessage.msg().Att_Err_NotFoundRequest);
		}
		AppDateTime targetDateTime = AppDateTime.now();
		pendingRequest.RequestTime__c = AppDateTime.convertDatetime(targetDateTime);
		pendingRequest.ActorId__c = UserInfo.getUserId();
		pendingRequest.CancelType__c = AppCancelType.NONE.value;

		// 社員情報を取得する
		List<ComEmpHistory__c> empHistoryList = getEmpHistory(pendingRequest.OwnerId, targetDateTime.getDate());
		if (empHistoryList.isEmpty()) {
			// エラー：申請者の社員が見つからないため申請できません。
			throw new App.IllegalStateException(ComMessage.msg().Com_Err_InvalidRequestForNoEmployeesFound);
		}
		// 社員情報を設定する
		ComEmpHistory__c empHistory = empHistoryList[0];
		AppLanguage lang = AppLanguage.valueOf(empHistory.BaseId__r.CompanyId__r.Language__c);
		pendingRequest.EmployeeHistoryId__c = empHistory.Id;
		pendingRequest.EmployeeName__c = getFullName(empHistory, lang);

		// 部署情報を取得する
		List<ComDeptHistory__c> deptHistoryList = getDeptHistory(empHistory.DepartmentBaseId__c, targetDateTime.getDate());
		if (deptHistoryList.isEmpty()) {
			return;
		}
		// 部署情報を設定する
		ComDeptHistory__c deptHistory = deptHistoryList[0];
		pendingRequest.DepartmentHistoryId__c = deptHistory.Id;
		pendingRequest.DepartmentName__c = getFullName(deptHistory, lang);
	}

	/**
	 * 申請データに、承認プロセス情報を設定する
	 */
	public override void applyProcessInfo() {
		Set<Id> targetObjectIds = new Set<Id>();
		targetObjectIds.addAll(approvedRequestIds);
		targetObjectIds.addAll(disabledRequestIds);
		Map<Id, ProcessInstanceStep> processInfoMap = getProcessInfo(targetObjectIds);

		// ステータス：承認済み
		for(ComGeneralRequest__c generalRequest : approvedRequestMap.values()) {
			if (!processInfoMap.containsKey(generalRequest.id)) {
				continue;
			}
			generalRequest.CancelType__c = AppCancelType.NONE.value;
			ProcessInstanceStep pis = processInfoMap.get(generalRequest.id);
			generalRequest.LastApproveTime__c = pis.CreatedDate;
			generalRequest.LastApproverId__c = pis.ActorId;
		}

		// ステータス：無効
		for(ComGeneralRequest__c generalRequest : disabledRequestMap.values()) {
			if (!processInfoMap.containsKey(generalRequest.id)) {
				continue;
			}
			ProcessInstanceStep pis = processInfoMap.get(generalRequest.id);
			// 却下
			if (pis.StepStatus == STEP_STATUS_REJECTED) {
				generalRequest.CancelType__c = AppCancelType.REJECTED.value;
			}
			// 申請取消
			else if (pis.StepStatus == STEP_STATUS_REMOVED) {
				generalRequest.CancelType__c = AppCancelType.REMOVED.value;
			}
			// 承認取消
			else if (pis.StepStatus == STEP_STATUS_APPROVED ) {
				generalRequest.CancelType__c = AppCancelType.CANCELED.value;
				generalRequest.LastApproveTime__c = null;
				generalRequest.LastApproverId__c = null;
			}
		}
	}

	/**
	 * 承認待ち、承認済み、却下、取消になった稟議申請IDを取得する
	 * @param oldMap
	 * @param newMap
	 */
	private void init(Map<Id, ComGeneralRequest__c> oldMap, Map<Id, ComGeneralRequest__c> newMap) {
		for (Id requestId : newMap.keySet()) {
			ComGeneralRequest__c generalRequestNew = newMap.get(requestId);
			ComGeneralRequest__c generalRequestOld = oldMap.get(requestId);

			// 申請判定：ステータス→承認待ち
			if (AppRequestStatus.PENDING.equals(AppRequestStatus.valueOf(generalRequestNew.Status__c))
						&& !AppRequestStatus.PENDING.equals(AppRequestStatus.valueOf(generalRequestOld.Status__c))) {
				pendingRequest = generalRequestNew;
			}
			// 承認判定：ステータス→承認済み
			else if (AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(generalRequestNew.Status__c))
						&& !AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(generalRequestOld.Status__c))) {
				approvedRequestMap.put(generalRequestNew.Id, generalRequestNew);
			}
			// 承認判定：ステータス→無効(却下、取消)
			else if (AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(generalRequestNew.Status__c))
						&& !AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(generalRequestOld.Status__c))) {
				disabledRequestMap.put(generalRequestNew.Id, generalRequestNew);
			}
		}
	}

	/**
	 * 社員履歴を取得する（検索条件はユーザIDと対象日）
	 * @param userId
	 * @param targetDate
	 * @return 社員履歴リスト
	 */
	private static List<ComEmpHistory__c> getEmpHistory(Id userId, Date targetDate) {
		return [
			SELECT Id, DepartmentBaseId__c,
				BaseId__r.UserId__c, BaseId__r.CompanyId__r.Language__c,
				BaseId__r.FirstName_L0__c, BaseId__r.FirstName_L1__c, BaseId__r.FirstName_L2__c,
				BaseId__r.LastName_L0__c, BaseId__r.LastName_L1__c, BaseId__r.LastName_L2__c
			FROM ComEmpHistory__c
			WHERE BaseId__r.UserId__c = :userId
				AND ValidFrom__c <= :targetDate
				AND ValidTo__c > :targetDate
				AND Removed__C = false];
	}

	/**
	 * 部署履歴を取得する（検索条件は部署ベースIDと対象日）
	 * @param baseId
	 * @param targetDate
	 * @return 部署履歴リスト
	 */
	private static List<ComDeptHistory__c> getDeptHistory(Id baseId, Date targetDate) {
		return [
			SELECT Id, Name_L0__c, Name_L1__c, Name_L2__c
			FROM ComDeptHistory__c
			WHERE BaseId__c = :baseId
				AND ValidFrom__c <= :targetDate
				AND ValidTo__c > :targetDate
				AND Removed__C = false];
	}

	/**
	 * 社員名を取得する
	 * @param empHistory 社員履歴
	 * @param lang 対象言語
	 */
	private String getFullName(ComEmpHistory__c empHistory, AppLanguage lang) {
		AppPersonName fullName = new AppPersonName(
				empHistory.BaseId__r.FirstName_L0__c, empHistory.BaseId__r.LastName_L0__c,
				empHistory.BaseId__r.FirstName_L1__c, empHistory.BaseId__r.LastName_L1__c,
				empHistory.BaseId__r.FirstName_L2__c, empHistory.BaseId__r.LastName_L2__c);
		return fullName.getFullName(lang);
	}

	/**
	 * 部署名を取得する
	 * @param deptHistory 部署履歴
	 * @param lang 対象言語
	 */
	private String getFullName(ComDeptHistory__c deptHistory, AppLanguage lang) {
		AppMultiString deptName = new AppMultiString(
					deptHistory.Name_L0__c, deptHistory.Name_L1__c, deptHistory.Name_L2__c);
		return deptName.getValue(lang);
	}
}