/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 承認
 *
 * @description プロセスインタンスのリポジトリ
 */
public with sharing class ApprovalProcessRepository extends Repository.BaseRepository {

	private static final String STEP_STATUS_NO_RESPONSE = 'NoResponse';

	/** 子リレーション名（プロセスインスタンス履歴）*/
	private static final String CHILD_RELATION_HISTORIES = 'StepsAndWorkitems';

	/** リレーション名（オブジェクト）*/
	private static final String TARGET_OBJECT_R =
			ProcessInstance.TargetObjectId.getDescribe().getRelationshipName();

	/** リレーション名（プロセスノード）*/
	private static final String PROCESS_NODE_R =
			ProcessInstanceHistory.ProcessNodeId.getDescribe().getRelationshipName();

	/** リレーション名（元のアクター）*/
	private static final String ORIGINAL_ACTOR_R =
			ProcessInstanceHistory.OriginalActorId.getDescribe().getRelationshipName();

	/** リレーション名（アクター）*/
	private static final String ACTOR_R =
			ProcessInstanceHistory.ActorId.getDescribe().getRelationshipName();

	/** リレーション名（プロセスインスタンス）*/
	private static final String PROCESS_INSTANCE_R =
			ProcessInstanceWorkitem.ProcessInstanceId.getDescribe().getRelationshipName();

	/** 取得対象の項目(プロセスインスタンス) */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ProcessInstance.Id,
			ProcessInstance.TargetObjectId,
			ProcessInstance.LastActorId
		};
	}

	/** 取得対象の項目(プロセスインスタンス履歴) */
	private static final Set<Schema.SObjectField> GET_HISTORY_FIELD_SET;
	static {
		GET_HISTORY_FIELD_SET = new Set<Schema.SObjectField> {
			ProcessInstanceHistory.Id,
			ProcessInstanceHistory.processNodeId,
			ProcessInstanceHistory.createdDate,
			ProcessInstanceHistory.stepStatus,
			ProcessInstanceHistory.originalActorId,
			ProcessInstanceHistory.actorId,
			ProcessInstanceHistory.comments
		};
	}

	/** 取得対象の項目(承認申請) */
	private static final Set<Schema.SObjectField> GET_WORK_ITEM_FIELD_SET;
	static {
		GET_WORK_ITEM_FIELD_SET = new Set<Schema.SObjectField> {
			ProcessInstanceWorkitem.Id,
			ProcessInstanceWorkitem.ActorId
		};
	}

	/** 検索フィルタ(プロセスインスタンス、プロセスインスタンス履歴) */
	private class Filter {
		// プロセスインスタンス
		/** 現在担当している承認者のユーザID */
		public Id userId;
		/** オブジェクト名 */
		public Set<String> objectNameSet;
		/** 申請ID */
		public List<Id> requestIdList;

		// プロセスインスタンス履歴
		/** 未承認 */
		public Boolean isPending;
		/** 状況 */
		public Set<String> stepStatusSet;
		/** 状況(除外対象) */
		public Set<String> stepStatusSetToBeExcluded;
	}

	/*
	 * Condition for advance search
	 */
	public class AdvancedSearchFilter {
		public Set<String> statusList;
		public Set<Id> empBaseIdList;
		public ExpReportRequestRepository.DateRange requestDateRange;

		public AdvancedSearchFilter() {
			statusList = new Set<String>();
			empBaseIdList = new Set<Id>();
			requestDateRange = new ExpReportRequestRepository.DateRange();
		}
	}

	/** 検索フィルタ(承認申請) */
	private class WorkItemFilter {
		/** 現在担当している承認者のユーザID */
		public Set<Id> userIdSet;
		/** 申請IDのリスト */
		public List<Id> requestIdList;
		/** オブジェクト名 */
		public Set<String> objectNameSet;
	}


	/**
	 * 指定ユーザの承認待ちになっている申請を取得する
	 * @param userId ユーザID
	 * @param objectNameList 対象オブジェクト名のリスト
	 * @return プロセスインスタンスエンティティのリスト
	 */
	public List<ApprovalProcessEntity> getRequestList(Id userId, Set<String> objectNameSet) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.userId = userId;
		filter.objectNameSet = objectNameSet;

		// 検索を実行する
		final Boolean withHistories = false;
		List<ApprovalProcessEntity> approvalProcessList = searchEntity(filter, withHistories);

		return ApprovalProcessList;
	}

	/**
	 * 指定申請IDの申請を取得する
	 * @param requestIdList 申請対象のリスト
	 * @return プロセスインスタンスエンティティのリスト
	 */
	public List<ApprovalProcessEntity> getRequestList(List<Id> requestIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.requestIdList = requestIdList;

		// 検索を実行する
		final Boolean withHistories = false;
		List<ApprovalProcessEntity> approvalProcessList = searchEntity(filter, withHistories);

		return ApprovalProcessList;
	}

	/**
	 * 指定ユーザの承認待ちになっている申請のIDを取得する
	 * @param userId ユーザID
	 * @param objectNameList 対象オブジェクト名のリスト
	 * @return 申請IDのリスト
	 */
	public List<Id> getRequestIdList(Id userId, String objectName) {
		List<Id> requestIdList = new List<Id>();

		// 検索条件を作成する
		Filter filter = new Filter();
		filter.userId = userId;
		filter.objectNameSet = new Set<String>{objectName};

		// 検索を実行する
		final Boolean withHistories = false;
		List<ApprovalProcessEntity> approvalProcessList = searchEntity(filter, withHistories);

		// 検索結果から申請IDを取得する
		for (ApprovalProcessEntity pi : ApprovalProcessList) {
			requestIdList.add(pi.targetObjectId);
		}

		return requestIdList;
	}

	/**
	 * @description Get request id list of Expense or Request by SELECT ProcessInstance. Only for status is Approved or Rejected.
	 * @param filter Search condition
	 * @param objectName Name of object. ExpReportRequest or ExpRequestApproval.
	 * @param approverUserId User id of approver
	 * @return List of request id
	 *
	 * 1. Get request id list from process instance by run SOQL (filtering is very rough)
	 * 2. Get process instance with history from process instance by run SOQL. Then filter precisely with for loop.
	 * The reason why run 2 SOQL is multiple approval process can exist for 1 request id. if the request is re-submitted, new process instance is created. if re-submitted, the previous process instance is ignored.
	 *
	 * TODO: Refactoring; Queries to group common functions into methods.
	 * NOTE: getExpApprovedRejectedRequestIdList and getExpPendingRequestIdList needs to be separated. 2 queries in those classes must be separate as they have inner queries that refer to different objects.
	 */
	public List<Id> getExpApprovedRejectedRequestIdList(AdvancedSearchFilter filter, String objectName, Id approverUserId) {

		// Items of process instance
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// Object
		selectFldList.add(TARGET_OBJECT_R + '.Type');

		// Items of process instance history
		List<String> historyFldList = new List<String>();
		for (Schema.SObjectField fld : GET_HISTORY_FIELD_SET) {
			historyFldList.add(getFieldName(fld));
		}
		// Process node
		historyFldList.add(PROCESS_NODE_R + '.' + getFieldName(ProcessNode.Name));
		// Original actor
		historyFldList.add(ORIGINAL_ACTOR_R + '.' + getFieldName(User.Name));
		// Actor
		historyFldList.add(ACTOR_R + '.' + getFieldName(User.Name));

		// History table
		selectFldList.add(
				'(SELECT ' + String.join(historyFldList, ',')
				+ ' FROM ' + CHILD_RELATION_HISTORIES
				+ ' ORDER BY CreatedDate ASC)');

		// Create WHERE list by conditions
		List<String> condList = new List<String>();
		Set<String> statusList = createStatusList(filter);
		final String statusSql = statusList != null ? ' AND StepStatus IN :statusList' : '';
		// By SELECT ProcessInstanceStep as below, you can get process instance which contains approved & rejected status.
		condList.add('Id IN (SELECT ProcessInstanceId FROM ProcessInstanceStep WHERE (ActorId = :approverUserId OR OriginalActorId = :approverUserId)' +statusSql+')');
		condList.add('TargetObject.Type = :objectName');
		final Date requestDateStart = (filter!=null && filter.requestDateRange!=null) ? filter.requestDateRange.startDate : null;
		final Date requestDateEnd = (filter!=null && filter.requestDateRange!=null && filter.requestDateRange.endDate!=null) ? filter.requestDateRange.endDate : null;
		if(requestDateStart!=null){
			Datetime startDateTime = Datetime.newInstance(requestDateStart.year(), requestDateStart.month(), requestDateStart.day(), 0, 0, 0);
			condList.add('CreatedDate>=:startDateTime');
		}
		if(requestDateEnd!=null){
			Datetime endDateTime = Datetime.newInstance(requestDateEnd.year(), requestDateEnd.month(), requestDateEnd.day(), 23, 59, 59);
			condList.add('CreatedDate<=:endDateTime');
		}

		String whereCond = buildWhereString(condList);

		// NOTE: Multiple ApprovalProcess for one TargetObjectId exist when re-submit
		String soql =
			'SELECT ' + String.join(selectFldList, ', ') +
			' FROM ' + ProcessInstance.SObjectType.getDescribe().getName() + whereCond;

		// Run query
		System.debug('ApprovalProcessRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// Create entity from query result
		List<ApprovalProcessEntity> entityList = new List<ApprovalProcessEntity>();
		for (SObject sObj : sObjList) {
			entityList.add(createEntity((ProcessInstance)sObj));
		}

		List<Id> requestIdList = new List<Id>();
		for (ApprovalProcessEntity entity: entityList){
			requestIdList.add(entity.targetObjectId);
		}

		String soql2 = 'SELECT ' + String.join(selectFldList, ', ') + ' FROM ' + ProcessInstance.SObjectType.getDescribe().getName()+ ' WHERE TargetObjectId IN :requestIdList ORDER BY TargetObjectId, createdDate DESC';
		// Run query
		System.debug('ApprovalProcessRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList2 = Database.query(soql2);

		// Create entity from query result
		List<ApprovalProcessEntity> entityList2 = new List<ApprovalProcessEntity>();
		for (SObject sObj : sObjList2) {
			entityList2.add(createEntity((ProcessInstance)sObj));
		}

		return filterEntityList(entityList2, filter, approverUserId);
	}

	/*
	 * @description filter ApprovalProcess by filter. And the older process instance is ignored.
	 * @param entityList Entity list of ApprovalProcessEntity
	 * @param filter Search condition
	 * @param approverUserId User id of approver
	 * @return requestIdList Filtered requestIdList
	 */
	private List<Id> filterEntityList(List<ApprovalProcessEntity> entityList, AdvancedSearchFilter filter, Id approverUserId) {
		List<Id> requestIdList = new List<Id>();
		for(Integer i=0; i<entityList.size(); i++){
			Boolean isNewestEntity = i==0 || entityList[i].targetObjectId != entityList[i-1].targetObjectId;
			if(isNewestEntity && isMeetFilterCondition(entityList[i], filter, approverUserId)){
				requestIdList.add(entityList[i].targetObjectId);
			}
		}
		return requestIdList;
	}

	/*
	 * @description filter ApprovalProcess by filter
	 * @param entity Entity of ApprovalProcessEntity
	 * @param filter Search condition.
	 * @param approverUserId User id of approver
	 * @return If meet condition return true, else return false.
	 */
	private Boolean isMeetFilterCondition(ApprovalProcessEntity entity, AdvancedSearchFilter filter, Id approverUserId) {
		Set<Id> userIdList = getUserIdFromEmpBaseId(filter.empBaseIdList);
		Boolean isContainSubmitter = filter.empBaseIdList==null || filter.empBaseIdList.isEmpty() || userIdList.contains(entity.historyList[0].actorId);
		Boolean isContainSubmitStartDate = filter.requestDateRange==null || filter.requestDateRange.startDate==null || entity.historyList[0].createdDate.compareTo(new AppDatetime(filter.requestDateRange.startDate))>=0;
		Boolean isContainSubmitEndDate = filter.requestDateRange==null || filter.requestDateRange.endDate==null || entity.historyList[0].createdDate.compareTo(new AppDatetime(filter.requestDateRange.endDate).addDays(1))<=0;
		for(ApprovalProcessHistoryEntity history: entity.historyList){
			Set<String> statusList = createStatusList(filter);
			Boolean isContainStatus = statusList==null || statusList.isEmpty() || statusList.contains(history.stepStatus);
			Boolean isContainApprover = approverUserId == history.actorId || approverUserId == history.originalActorId;
			if(isContainSubmitter && isContainSubmitStartDate && isContainSubmitEndDate && isContainStatus && isContainApprover){
				return true;
			}
		}
		return false;
	}

	/*
	 * @description filter ApprovalProcess by filter.empBaseIdList
	 * @param filter Search condition.
	 * @param entity Entity of ApprovalProcessEntity
	 * @return requestIdList requestIdList filtered in ApprovalProcess by filter.empBaseIdList
	 */
	private List<Id> filterEmpBaseIdList(List<ApprovalProcessEntity> entityList, AdvancedSearchFilter filter) {
		List<Id> requestIdList = new List<Id>();
		Set<Id> userIdList = new Set<Id>();
		if (filter != null && filter.empBaseIdList != null && !filter.empBaseIdList.isEmpty()) {
			userIdList = getUserIdFromEmpBaseId(filter.empBaseIdList); // userIdList.isEmpty() == empBaseId is specified but no data.
			if (userIdList.isEmpty()) {
				return requestIdList; // return empty list
			}
		}
		for (ApprovalProcessEntity entity : entityList) {
			// userIdList.isEmpty() == {(filter==null) or (filter.empBaseIdList==null) or (filter.empBaseIdList==[])} which mean empBaseId is not specified so return all ids.
			if (userIdList.isEmpty() || isContainUserList(userIdList, entity)) {
				requestIdList.add(entity.targetObjectId);
			}
		}
		return requestIdList;
	}

	/*
	 * @description create userIdList from empBaseIdList by searching empBaseEntity
	 * @param empBaseIdList list of employee base id
	 * @return userIdList associated with empBaseIdList
	 */
	private Set<Id> getUserIdFromEmpBaseId(Set<Id> empBaseIdList) {
		Set<Id> userIdList = new Set<Id>();
		EmployeeRepository empBaseRepo = new EmployeeRepository();
		List<EmployeeBaseEntity> empBaseEntityList = empBaseRepo.getBaseEntityList(new List<Id>(empBaseIdList), false);
		for (EmployeeBaseEntity empBaseEntity : empBaseEntityList) {
			userIdList.add(empBaseEntity.userId);
		}
		return userIdList;
	}

	/**
	 * @description Check if each actor id of entity is contained in empBaseIdList of filter
	 * @param filter Search condition.
	 * @param entity Entity of ApprovalProcessEntity
	 * @return If contain true, if not false.
	 */
	private Boolean isContainUserList(Set<Id> userIdList, ApprovalProcessEntity entity) {
		Integer historyLength = entity.historyList.size(); // entity.historyList[historyLength-1] means oldest history which have 'start' status.
		// 'entity.historyList[historyLength-1].ActorId' means submitter.
		// The entity always has history. so historyLength > 0.
		return userIdList.contains(entity.historyList[historyLength - 1].ActorId);
	}

	/**
	 * @description Create status list which contains only 'Approved' or 'Rejected' (exclude 'Pending')
	 * @param filter Search condition.
	 * @return Filtered status list
	 */
	private Set<String> createStatusList(AdvancedSearchFilter filter) {
		Set<String> statusList = new Set<String>();
		if (filter != null && filter.statusList != null && !filter.statusList.isEmpty()) {
			for(String status :filter.statusList){
				// add only when status is 'Approved' or 'Rejected'
				if (status == ApprovalService.ProcessInstanceStatus.Approved.name() || status == ApprovalService.ProcessInstanceStatus.Rejected.name()) {
					statusList.add(status);
				}
			}
		}
		return statusList;
	}

	/**
	 * @description Get Request Id List of Expense or Request by SELECT ProcessInstance. only for status is Pending
	 * @param Filter search condition.
	 * @param objectName Name of object. ExpReportRequest or ExpRequestApproval.
	 * @param approverUserId User id of approver
	 * @return List of request id
	 *
	 * TODO: Refactoring; Queries to group common functions into methods.
	 * NOTE: getExpApprovedRejectedRequestIdList and getExpPendingRequestIdList needs to be separated. 2 queries in those classes must be separate as they have inner queries that refer to different objects
	 */
	public List<Id> getExpPendingRequestIdList(AdvancedSearchFilter filter, String objectName, Id approverUserId) {

		// Items of process instance
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// Object
		selectFldList.add(TARGET_OBJECT_R + '.Type');

		// Items of process instance history
		List<String> historyFldList = new List<String>();
		for (Schema.SObjectField fld : GET_HISTORY_FIELD_SET) {
			historyFldList.add(getFieldName(fld));
		}
		// Process node
		historyFldList.add(PROCESS_NODE_R + '.' + getFieldName(ProcessNode.Name));
		// Original actor
		historyFldList.add(ORIGINAL_ACTOR_R + '.' + getFieldName(User.Name));
		// Actor
		historyFldList.add(ACTOR_R + '.' + getFieldName(User.Name));

		selectFldList.add(
				'(SELECT ' + String.join(historyFldList, ',')
				+ ' FROM ' + CHILD_RELATION_HISTORIES
				+ ' ORDER BY ' + getFieldName(ProcessInstanceHistory.CreatedDate) + ' Desc, '
				+ getFieldName(ProcessInstanceHistory.Id) + ' Desc)');

		// Create WHERE list by conditions
		List<String> condList = new List<String>();
		// By SELECT ProcessInstanceWorkItem as below, you can get process instance which contains pending status.
		condList.add('Id IN (SELECT ProcessInstanceId FROM ProcessInstanceWorkItem WHERE ActorId = :approverUserId)');
		condList.add('TargetObject.Type = :objectName');
		final Date requestDateStart = (filter != null && filter.requestDateRange != null) ? filter.requestDateRange.startDate : null;
		final Date requestDateEnd = (filter != null && filter.requestDateRange != null && filter.requestDateRange.endDate != null) ? filter.requestDateRange.endDate : null;
		if(requestDateStart!=null){
			Datetime startDateTime = Datetime.newInstance(requestDateStart.year(), requestDateStart.month(), requestDateStart.day(), 0, 0, 0);
			condList.add('CreatedDate>=:startDateTime');
		}
		if(requestDateEnd!=null){
			Datetime endDateTime = Datetime.newInstance(requestDateEnd.year(), requestDateEnd.month(), requestDateEnd.day(), 23, 59, 59);
			condList.add('CreatedDate<=:endDateTime');
		}

		String whereCond = buildWhereString(condList);

		// NOTE: Multiple ApprovalProcess for one TargetObjectId exist when re-submit.
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ProcessInstance.SObjectType.getDescribe().getName()
				+ whereCond
				+ ' ORDER BY ' + getFieldName(ProcessInstance.TargetObjectId) + ' Asc, '
				+ getFieldName(ProcessInstance.CreatedDate) + ' Desc';

		// Run query
		System.debug('ApprovalProcessRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// Create entity by query result
		List<ApprovalProcessEntity> entityList = new List<ApprovalProcessEntity>();
		for (SObject sObj : sObjList) {
			entityList.add(createEntity((ProcessInstance)sObj));
		}

		// Filter by empBaseIdList
		List<Id> requestIdList = filterEmpBaseIdList(entityList, filter);
		return requestIdList;
	}

	/**
	 * 申請に紐づく承認履歴を取得する
	 * @param requestId 申請ID
	 * @return プロセスインスタンス履歴のリスト
	 */
	public List<ApprovalProcessHistoryEntity> getHistoryList(Id requestId) {
		List<ApprovalProcessHistoryEntity> historyList = new List<ApprovalProcessHistoryEntity>();

		// 検索条件を作成する
		Filter filter = new Filter();
		filter.requestIdList = new List<Id>{requestId};
		filter.isPending = false;
		filter.stepStatusSetToBeExcluded = new Set<String>{STEP_STATUS_NO_RESPONSE};

		// 検索を実行する
		final Boolean withHistories = true;
		List<ApprovalProcessEntity> approvalProcessList = searchEntity(filter, withHistories);

		// 検索結果からプロセスインスタンス履歴を取得する
		if (approvalProcessList != null && !approvalProcessList.isEmpty()) {
			for (ApprovalProcessEntity pi : approvalProcessList) {
				for (ApprovalProcessHistoryEntity history : pi.historyList) {
					historyList.add(history);
				}
			}
		}

		return historyList;
	}

	/**
	 * 申請に紐づく承認履歴を取得する
	 * @param requestIdList 申請IDのリスト
	 * @return プロセスインスタンス履歴のリスト
	 */
	public List<ApprovalProcessEntity> getHistoryList(List<Id> requestIdList, Set<String> stepStatusSet) {
		List<ApprovalProcessHistoryEntity> historyList = new List<ApprovalProcessHistoryEntity>();

		// 検索条件を作成する
		Filter filter = new Filter();
		filter.requestIdList = requestIdList;
		filter.isPending = false;
		filter.stepStatusSet = stepStatusSet;

		// 検索を実行する
		final Boolean withHistories = true;
		return searchEntity(filter, withHistories);
	}

	/**
	 * 承認/却下対象の承認申請データを取得する
	 * @param requestIdList 申請IDのリスト
	 * @return 承認申請IDのリスト
	 */
	public List<Id> getWorkItemList(List<Id> requestIdList) {
		List<Id> workItemIdList = new List<Id>();

		// 検索条件を作成する
		WorkItemFilter filter = new WorkItemFilter();
		filter.requestIdList = requestIdList;

		// 検索を実行する
		workItemIdList = searchWorkItemEntity(filter).values();

		return workItemIdList;
	}

	/**
	 * 承認/却下対象の承認申請データを取得する
	 * @param requestIdList 申請IDのリスト
	 * @return 承認申請IDのマップ（key=申請ID、value=承認申請ID）
	 */
	public Map<Id, Id> getWorkItemMap(List<Id> requestIdList) {
		Map<Id, Id> workItemIdMap = new Map<Id, Id>();

		// 検索条件を作成する
		WorkItemFilter filter = new WorkItemFilter();
		filter.requestIdList = requestIdList;

		// 検索を実行する
		workItemIdMap = searchWorkItemEntity(filter);

		return workItemIdMap;
	}

	/**
	 * プロセスインスタンスを検索する
	 * @param filter 検索条件
	 * @param withRecords プロセスインスタンス履歴も一緒に取得する場合はtrue
	 * @return 条件に一致したプロセスインスタンスのリスト
	 */
	private List<ApprovalProcessEntity> searchEntity(Filter filter,  Boolean withHistories) {
		// SOQLを作成する
		// プロセスインスタンスの項目
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// オブジェクト
		selectFldList.add(TARGET_OBJECT_R + '.Type');

		// プロセスインスタンス履歴の項目
		if (withHistories == true) {
			List<String> historyFldList = new List<String>();
			for (Schema.SObjectField fld : GET_HISTORY_FIELD_SET) {
				historyFldList.add(getFieldName(fld));
			}
			// プロセスノード
			historyFldList.add(PROCESS_NODE_R + '.' + getFieldName(ProcessNode.Name));
			// 元のアクター
			historyFldList.add(ORIGINAL_ACTOR_R + '.' + getFieldName(User.Name));
			// アクター
			historyFldList.add(ACTOR_R + '.' + getFieldName(User.Name));

			// フィルタ条件からWHERE句の条件リストを作成する
			List<String> historyCondList = new List<String>();
			final Boolean isPending = filter.isPending;
			final Set<String> stepStatusSet = filter.stepStatusSet;
			final Set<String> stepStatusSetToBeExcluded = filter.stepStatusSetToBeExcluded;
			if (isPending != null) {
				historyCondList.add('IsPending = :isPending');
			}
			if (stepStatusSet != null) {
				historyCondList.add('StepStatus IN :stepStatusSet');
			}
			if (stepStatusSetToBeExcluded != null) {
				historyCondList.add('StepStatus NOT IN :stepStatusSetToBeExcluded');
			}

			String historyWhereCond;
			if (! historyCondList.isEmpty()) {
				// ANDで条件をつなぐ
				historyWhereCond = '(' + String.join(historyCondList, ') AND (') + ')';
			} else {
				historyWhereCond = '';
			}

			selectFldList.add(
					'(SELECT ' + String.join(historyFldList, ',')
					+ ' FROM ' + CHILD_RELATION_HISTORIES
					+ ' WHERE ' + historyWhereCond
					// CreatedDate が秒まで一致するステップが存在する場合を考慮 ref. GENIE-717
					+ ' ORDER BY ' + getFieldName(ProcessInstanceHistory.CreatedDate) + ' Desc, '
					+ getFieldName(ProcessInstanceHistory.Id) + ' Desc)');
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id userId = filter.userId;
		final Set<String> objectNameSet = filter.objectNameSet;
		final List<Id> requestIdList = filter.requestIdList;
		if (userId != null) {
			condList.add('Id IN (SELECT ProcessInstanceId FROM ProcessInstanceWorkItem WHERE ActorId = :userId)');
		}
		if (objectNameSet != null) {
			condList.add('TargetObject.Type IN :objectNameSet');
		}
		if (requestIdList != null) {
			condList.add('TargetObjectId IN :requestIdList');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// NOTE: 取消や却下を行った後に再申請すると、
		// 同じ TargetObjectId に対して複数の ApprovalProcess が存在する状態になる
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ProcessInstance.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond
				+ ' ORDER BY ' + getFieldName(ProcessInstance.TargetObjectId) + ' Asc, '
				+ getFieldName(ProcessInstance.CreatedDate) + ' Desc';

		// クエリ実行
		System.debug('ApprovalProcessRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<ApprovalProcessEntity> entityList = new List<ApprovalProcessEntity>();
		for (SObject sObj : sObjList) {
			entityList.add(createEntity((ProcessInstance)sObj));
		}

		return entityList;
	}

	/**
	 * ApprovalProcessオブジェクトからApprovalProcessEntityを作成する
	 * @param obj 作成元のApprovalProcessオブジェクト
	 * @return 作成したApprovalProcessEntity
	 */
	private ApprovalProcessEntity createEntity(ProcessInstance obj) {
		ApprovalProcessEntity entity = new ApprovalProcessEntity();
		entity.setId(obj.Id);
		entity.targetObjectId = obj.TargetObjectId;
		entity.targetObjectName = obj.TargetObject.Type;
		entity.LastActorId = obj.LastActorId;

		// プロセスインスタンス履歴
		List<ProcessInstanceHistory> historyObjList = obj.StepsAndWorkitems;
		if (historyObjList != null) {
			List<ApprovalProcessHistoryEntity> historyList = new List<ApprovalProcessHistoryEntity>();
			for (ProcessInstanceHistory historyObj : historyObjList) {
				historyList.add(createHistoryEntity(historyObj));
			}
			entity.replaceHistoryList(historyList);
		}

		return entity;
	}

	/**
	 * ApprovalProcessHistoryオブジェクトからApprovalProcessHistoryEntityを作成する
	 * @param obj ApprovalProcessHistoryオブジェクト
	 * @return 作成したApprovalProcessHistoryEntity
	 */
	private ApprovalProcessHistoryEntity createHistoryEntity(ProcessInstanceHistory obj) {
		ApprovalProcessHistoryEntity entity = new ApprovalProcessHistoryEntity();
		entity.setId(obj.Id);
		entity.processNodeId = obj.ProcessNodeId;
		entity.processNodeName = obj.ProcessNode.Name;
		entity.createdDate = AppDatetime.valueOf(obj.CreatedDate);
		entity.stepStatus = obj.StepStatus;
		entity.originalActorId = obj.OriginalActorId;
		entity.originalActorName = obj.OriginalActor.Name;
		entity.actorId = obj.ActorId;
		entity.actorName = obj.Actor.Name;
		entity.comments = obj.Comments;

		return entity;
	}

	/**
	 * 承認申請を検索する
	 * @param filter 検索条件
	 * @return 条件に一致した承認申請IDのマップ（key=申請レコードID、value=承認申請ID）
	 */
	private Map<Id, Id> searchWorkItemEntity(WorkItemFilter filter) {
		// 検索Query実行
		List<SObject> sObjList = queryWorkItem(filter);

		// 結果からエンティティを作成する
		Map<Id, Id> workItemIdMap = new Map<Id, Id>();
		for (SObject sObj : sObjList) {
			workItemIdMap.put(((ProcessInstanceWorkItem)sObj).ProcessInstance.TargetObjectId, sObj.Id);
		}
		return workItemIdMap;
	}

	/**
	 * 承認申請を検索する
	 * @param filter 検索条件
	 * @return 条件に一致した承認申請IDのマップ（key=申請レコードID、value=承認申請ID）
	 */
	public Map<Id, List<ApprovalProcessEntity>> getApprovalProcessListByUsers(Set<Id> userIdSet, Set<String> objectNameSet) {
		WorkItemFilter filter = new WorkItemFilter();
		filter.userIdSet = userIdSet;
		filter.objectNameSet = objectNameSet;
		// 検索Query実行
		List<SObject> sObjList = queryWorkItem(filter);

		// 結果からエンティティを作成する
		Map<Id, List<ApprovalProcessEntity>> appProcessListMap = new Map<Id, List<ApprovalProcessEntity>>();
		for (SObject sObj : sObjList) {
			ProcessInstanceWorkItem workItem = (ProcessInstanceWorkItem) sObj;

			List<ApprovalProcessEntity> appProcessList = new List<ApprovalProcessEntity>();
			if (appProcessListMap.containsKey(workItem.ActorId)) {
				appProcessList = appProcessListMap.get(workItem.ActorId);
			} else {
				appProcessList = new List<ApprovalProcessEntity>();
				appProcessListMap.put(workItem.ActorId, appProcessList);
			}

			// Add Object record to the list
			ApprovalProcessEntity appProcess = new ApprovalProcessEntity();
			appProcess.targetObjectId = workItem.ProcessInstance.TargetObjectId;
			appProcess.targetObjectName = workItem.ProcessInstance.TargetObject.Type;
			appProcess.lastActorId = workItem.ProcessInstance.LastActorId;
			appProcessList.add(appProcess);
		}
		return appProcessListMap;
	}

	private List<SObject> queryWorkItem(WorkItemFilter filter) {
		// SOQLを作成する
		// 承認申請の項目
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_WORK_ITEM_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// 申請レコード
		selectFldList.add(PROCESS_INSTANCE_R + '.' + getFieldName(ProcessInstance.TargetObjectId));
		selectFldList.add(PROCESS_INSTANCE_R + '.TargetObject.Type');
		selectFldList.add(PROCESS_INSTANCE_R + '.' + getFieldName(ProcessInstance.LastActorId));

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Set<Id> userIdSet = filter.userIdSet;
		if (userIdSet != null) {
			condList.add('ActorId IN :userIdSet');
		}
		final List<Id> requestIdList = filter.requestIdList;
		if (requestIdList != null) {
			condList.add('ProcessInstance.TargetObjectId IN :requestIdList');
		}
		final Set<String> objectNameSet = filter.objectNameSet;
		if (objectNameSet != null) {
			condList.add('ProcessInstance.TargetObject.Type IN :objectNameSet');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ProcessInstanceWorkItem.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond;

		// クエリ実行
		System.debug('ApprovalProcessRepository.searchWorkItemEntity: SOQL==>' + soql);
		return Database.query(soql);
	}
}
