/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブ割当エンティティ
 */
public with sharing class JobAssignEntity extends JobAssignGeneratedEntity {

	/**
	 * コンストラクタ
	 */
	public JobAssignEntity() {
		super(new ComJobAssign__c());
		this.sobj = (ComJobAssign__c)super.sobj;
	}

	/**
	 * コンストラクタ
	 */
	public JobAssignEntity(ComJobAssign__c sobj) {
		super(sobj);
		this.sobj = sobj;
	}

	/** 社員コード（参照のみ） */
	public String employeeCode {
		get {
			if (employeeCode == null) {
				employeeCode = sObj.EmployeeBaseId__r.Code__c;
			}
			return employeeCode;
		}
		private set;
	}
	@TestVisible
	private void setEmployeeCode(String value) {
		if (sObj.EmployeeBaseId__r == null) {
			sObj.EmployeeBaseId__r = new ComEmpBase__c();
		}
		sObj.EmployeeBaseId__r.Code__c = value;
	}

	/** 社員名（参照のみ） */
	public AppPersonName employeeNameL {
		get {
			if (employeeNameL == null) {
				employeeNameL = new AppPersonName(
						sObj.EmployeeBaseId__r.FirstName_L0__c, sObj.EmployeeBaseId__r.LastName_L0__c,
						sObj.EmployeeBaseId__r.FirstName_L1__c, sObj.EmployeeBaseId__r.LastName_L1__c,
						sObj.EmployeeBaseId__r.FirstName_L2__c, sObj.EmployeeBaseId__r.LastName_L2__c);
			}
			return employeeNameL;
		}
		private set;
	}
	@TestVisible
	private void setEmployeeNameL(AppPersonName value) {
		employeeNameL = value;
	}

	// TODO 親子型エンティティを継承したら削除する
	// 指定日付が期間範囲内かどうかをチェックする
	public Boolean isInPeriod(AppDate targetDate) {
		if (targetDate == null) {
			return false;
		}
		Date targetDt = targetDate.getDate();
		Date fromDt = validFrom.getDate();
		Date toDt = validTo.getDate();
		return fromDt <= targetDt && targetDt < toDt;
	}
}