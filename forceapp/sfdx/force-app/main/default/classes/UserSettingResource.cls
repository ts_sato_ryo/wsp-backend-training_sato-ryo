/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description ユーザ設定情報取得APIを実装するクラス
 */
public with sharing class UserSettingResource {

	/**
	 * Request parameter for GetApi for UserSetting
	 */
	public class GetApiParam implements RemoteApi.RequestParam {
		/*
		 * Specifies which extra Details to include. If null, all the details will be returned.
		 * If empty Set, then only the basic Information will be included.
		 */
		public Set<String> detailSelectors;
	}

	/**
	 * @desctiprion ユーザ設定情報を表すクラス
	 */
	public class UserSetting implements RemoteApi.ResponseParam {
		/** Company allow user to modify tax manually */
		public Boolean allowTaxAmountChange;
		/** ユーザレコードID */
		public String id;
		/** ユーザ名(SalesforceログインID) */
		public String userName;
		/** ユーザの氏名 */
		public String name;
		/** プロファイル写真のURL */
		public String photoUrl;
		/** ユーザの言語 */
		public String language;
		/** ユーザ(社員)の会社ID */
		public String companyId;
		/** ユーザ(社員)の会社名 */
		public String companyName;
		/** Cost Center History ID */
		public String costCenterHistoryId;
		/** Cost Center Code */
		public String costCenterCode;
		/** Cost Center Name */
		public String costCenterName;
		/** ユーザ(社員)の部署ID */
		public String departmentId;
		/** ユーザ(社員)の部署Code */
		public String departmentCode;
		/** ユーザ(社員)の部署名 */
		public String departmentName;
		/** ユーザ(社員)の社員ID */
		public String employeeId;
		/** ユーザ(社員)の社員Code */
		public String employeeCode;
		/** ユーザ(社員)の社員名 */
		public String employeeName;
		/** Base Currency ID / 基準通貨のID */
		public String currencyId;
		/** Base Currency Code / 基準通貨のCode */
		public String currencyCode;
		/** Base Currency Name / 基準通貨のName */
		public String currencyName;
		/** Base Currency Symbol / 基準通貨の通貨記号 */
		public String currencySymbol;
		/** Base Currency Decimal Places / 基準通貨の小数桁数 */
		public Integer currencyDecimalPlaces;
		/** 勤怠機能を利用する */
		public Boolean useAttendance;
		/** 経費精算機能を利用する */
		public Boolean useExpense;
		/** Use Expense Request function (advance expense application) */
		public Boolean useExpenseRequest;
		/** プランナー機能を利用する */
		public Boolean usePlanner;
		/** 工数管理機能を利用する */
		public Boolean useWorkTime;
		/** 上長(社員)の社員名 */
		public String managerName;
		/** 承認者選択機能を利用する */
		public Boolean allowToChangeApproverSelf;
		/** 承認者01名 */
		public String approver01Name;
		/** 打刻時の位置情報を送信する */
		public Boolean requireLocationAtMobileStamp;
	}

	/**
	 * @description ユーザ設定情報取得APIを実装するクラス
	 */
	public with sharing class GetApi extends RemoteApi.ResourceBase {

		/**
		 * @description 自身のユーザ設定情報を取得する
		 * @param  req リクエストパラメータ(なし)
		 * @return レスポンス(UserProfile)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			GetApiParam param = (GetApiParam) req.getParam(UserSettingResource.GetApiParam.class);

			EmployeeService empService = new EmployeeService();
			// ユーザ情報を取得
			Id userId = UserInfo.getUserId();
			UserEntity user = (new UserRepository()).getEntity(userId);

			Set<EmployeeService.EmployeeDetailSelector> detailSelectorSet = getEmployeeDetailSelector(param.detailSelectors);

			// ユーザに紐付く社員情報を取得
			EmployeeBaseEntity emp;
			EmployeeHistoryEntity empHistory;
			AppDate targetDate = AppDate.today();
			EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
			empFilter.userIds = new Set<Id>{userId};
			empFilter.targetDate = targetDate;
			List<EmployeeBaseEntity> empList = empService.searchEntity(empFilter, false,
				detailSelectorSet, EmployeeRepository.HistorySortOrder.VALID_FROM_ASC);
			if (empList != null && empList.size() > 0) {
				emp = empList[0];
				empHistory = emp.getHistory(0);
			}

			// 勤務体系履歴を取得
			Boolean allowToChangeApproverSelf;
			EmployeeBaseEntity approver01Data;
			if (empHistory != null &&
					detailSelectorSet.contains(EmployeeService.EmployeeDetailSelector.WORKING_TYPE)) {
				Id workingTypeBaseId = empHistory.workingTypeId;
				AttWorkingTypeBaseEntity workingTypeBaseData =
						new AttWorkingTypeRepository().getEntity(workingTypeBaseId, targetDate);
				allowToChangeApproverSelf =
						workingTypeBaseData != null && workingTypeBaseData.getHistory(0) != null
						? workingTypeBaseData.getHistory(0).allowToChangeApproverSelf
						: false;
				// 承認者01を取得
				approver01Data =
					empService
					.getApprover01(allowToChangeApproverSelf, emp.id, new List<AppDate>{targetDate})
					.get(targetDate);
			}

			// レスポンスをセット
			UserSetting userSetting = new UserSetting();
			userSetting.id = user.id;
			userSetting.name = user.name;
			userSetting.userName = user.userName;
			userSetting.photoUrl = user.smallPhotoUrl;
			userSetting.language = user.language;
			if (emp != null) {
				userSetting.companyId = emp.companyId;
				if (emp.company != null) {
					userSetting.companyName = emp.company.nameL.getValue();
				}
				if (emp.companyInfo != null) {
					userSetting.useAttendance = emp.companyInfo.useAttendance;
					userSetting.useExpense = emp.companyInfo.useExpense;
					userSetting.useExpenseRequest = emp.companyInfo.useExpenseRequest;
					userSetting.usePlanner = emp.companyInfo.usePlanner;
					userSetting.useWorkTime = emp.companyInfo.useWorkTime;
				}
				userSetting.departmentId = empHistory.departmentId;
				if (String.isNotBlank(empHistory.departmentId) &&
						detailSelectorSet.contains(EmployeeService.EmployeeDetailSelector.DEPARTMENT)) {
					DepartmentBaseEntity departmentBase = new DepartmentRepository().getEntity(empHistory.departmentId);
					userSetting.departmentCode = departmentBase.code;
				}
				if (empHistory.department != null) {
					userSetting.departmentName = empHistory.department.nameL.getValue();
				}
				userSetting.employeeId = emp.id;
				userSetting.employeeCode = emp.code;
				userSetting.employeeName = emp.fullNameL.getFullName();
				if (empHistory.manager != null) {
					userSetting.managerName = empHistory.manager.fullNameL.getFullName();
				}

				// userSetting.employeeName = new AppPersonName(
				// 		emp.FirstName_L0, emp.LastName_L0,
				// 		emp.FirstName_L1, emp.LastName_L1,
				// 		emp.FirstName_L2, emp.LastName_L2).getFullName(AppLanguage.getUserLang());
			}
			if (String.isNotBlank(userSetting.companyId)) {
				CompanyEntity company = new CompanyRepository().getEntity(userSetting.companyId);
				if (company != null) {
					userSetting.requireLocationAtMobileStamp = company.requireLocationAtMobileStamp;
					if (String.isNotBlank(company.currencyId)) {
						userSetting.allowTaxAmountChange = company.allowTaxAmountChange;
						userSetting.currencyId = company.currencyId;
						userSetting.currencyCode = company.currencyInfo.code;
						userSetting.currencyName = company.currencyInfo.nameL.getValue();
						userSetting.currencySymbol = company.currencyInfo.symbol;
						userSetting.currencyDecimalPlaces = company.currencyInfo.decimalPlaces;
					}
				}
			}
			userSetting.allowToChangeApproverSelf = allowToChangeApproverSelf;
			if (approver01Data != null) {
				userSetting.approver01Name = approver01Data.fullNameL.getFullName();
			}
			return userSetting;

		}

		/*
		 * Returns the Set of {@link EmployeeService.EmployeeDetailSelector} based on the given Detail Selector Set.
		 * @param detailSelectorStrSet Set of Strings representing the Details to be Included
		 * @returns Set of EmployeeDetailSelector. If detailSelectorStrSet is null, then it will returns the set of ALL the supported selectors.
		 * @throws ParameterException if the detailSelectors set contains unsupported selectors
		 */
		private Set<EmployeeService.EmployeeDetailSelector> getEmployeeDetailSelector(Set<String> detailSelectorStrSet) {
			Set<EmployeeService.EmployeeDetailSelector> detailSelectorSet = new Set<EmployeeService.EmployeeDetailSelector>();
			if (detailSelectorStrSet == null) {
				detailSelectorSet.addAll(EmployeeService.EMPLOYEE_DETAIL_SELECTOR_FIELD_MAP.values());
				return detailSelectorSet;
			}

			for (String detailSelectorStr : detailSelectorStrSet) {
				EmployeeService.EmployeeDetailSelector detailSelector = EmployeeService.EMPLOYEE_DETAIL_SELECTOR_FIELD_MAP.get(detailSelectorStr);
				if (detailSelector == null) {
					throw new App.ParameterException('detailSelectors', detailSelectorStr);
				}
				detailSelectorSet.add(detailSelector);
			}

			return detailSelectorSet;
		}
	}
}