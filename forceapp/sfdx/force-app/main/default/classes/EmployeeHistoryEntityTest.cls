/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 社員履歴エンティティのテスト
 */
@isTest
private class EmployeeHistoryEntityTest {

	private class EntityTestData extends TestData.TestDataEntity {


		/**
		 * 社員履歴エンティティを作成する(DB保存なし)
		 */
		public EmployeeHistoryEntity createEmployeeHistory(String name, AppDate validFrom, AppDate validTo) {
			EmployeeHistoryEntity history = new EmployeeHistoryEntity();
			history.name = name;
			history.historyComment = '履歴コメント';
			history.validFrom = validFrom;
			history.validTo = validTo;
			history.isRemoved = false;
			history.uniqKey = name + '_' ; validFrom.formatYYYYMMDD();
			history.departmentId = this.department.id;
			history.titleL0 = '役職_L0';
			history.titleL1 = '役職_L1';
			history.titleL2 = '役職_L2';
			history.gradeId = this.grade.Id;
			history.managerId = UserInfo.getUserId(); // ユーザIDで
			history.calendarId = this.calendar.id;
			history.workingTypeId = this.workingType.id;
			history.timeSettingId = this.timeSetting.Id;
			history.agreementAlertSettingId = this.agreementAlertSetting.id;
			history.permissionId = this.permission.id;
			history.expEmployeeGroupId = this.expEmployeeGroup.id;

			return history;
		}
	}

	/**
	 * エンティティを複製できることを確認する
	 */
	@isTest static void copyTest() {
		EntityTestData testData = new EntityTestData();
		EmployeeHistoryEntity history = testData.createEmployeeHistory('EntityTest', AppDate.today(), AppDate.today().addDays(30));

		EmployeeHistoryEntity copyHistory = history.copy();

		System.assertEquals(history.name, copyHistory.name);
		System.assertEquals(history.baseId, copyHistory.baseId);
		System.assertEquals(history.validFrom, copyHistory.validFrom);
		System.assertEquals(history.validTo, copyHistory.validTo);
		System.assertEquals(history.historyComment, copyHistory.historyComment);
		System.assertEquals(history.uniqKey, copyHistory.uniqKey);
		System.assertEquals(history.isRemoved, copyHistory.isRemoved);
		System.assertEquals(history.departmentId, copyHistory.departmentId);
		System.assertEquals(history.gradeId, copyHistory.gradeId);
		System.assertEquals(history.titleL0, copyHistory.titleL0);
		System.assertEquals(history.titleL1, copyHistory.titleL1);
		System.assertEquals(history.titleL2, copyHistory.titleL2);
		System.assertEquals(history.workingTypeId, copyHistory.workingTypeId);
		System.assertEquals(history.timeSettingId, copyHistory.timeSettingId);
		System.assertEquals(history.managerId, copyHistory.managerId);
		System.assertEquals(history.calendarId, copyHistory.calendarId);
		System.assertEquals(history.agreementAlertSettingId, copyHistory.agreementAlertSettingId);
		System.assertEquals(history.permissionId, copyHistory.permissionId);
		System.assertEquals(history.expEmployeeGroupId, copyHistory.expEmployeeGroupId);

	}
}
