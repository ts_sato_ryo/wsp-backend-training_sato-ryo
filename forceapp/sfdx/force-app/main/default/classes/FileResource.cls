/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description ファイルのAPIを提供するリソースクラス Resource class for File upload
 */
public with sharing class FileResource {
	private static final String PDF_EXTENSION = 'pdf';
	
	/**
	 * @description ファイルデータを格納するクラス Class for storing file data
	 */
	public class File {
		/** ファイルドキュメントID Content Document Id for the file */
		public Id contentDocumentId;
		/** ファイルID Content Version Id for the file */
		public Id contentVersionId;
		/** タイトル Title */
		public String title;
		/** ファイルタイプ Filetype */
		public String fileType;
		/** 日付 Created Date */
		public String createdDate;

		/**
		 * コンストラクタ Constructor
		 * @param entity コンテントドキュメントエンティティ AppContentDocument Entity
		 */
		public File(AppContentDocumentEntity entity) {
			contentDocumentId = entity.id;
			contentVersionId = entity.latestPublishedVersionId;
			title = entity.title;
			fileType = entity.fileType;
			createdDate = String.valueOf(entity.contentModifiedDate.getDate());
		}
	}
	
	/**
	 * @description ファイル保存内容を格納するクラス Request parameter for for SaveFileApi
	 */
	public class SaveFileParam implements RemoteApi.RequestParam {
		/** ファイル名 File name */
		public String fileName;
		/** ファイルボディ File body */
		public String fileBody;
		/** Type */
		public String type;

		/**
		 * パラメータを検証する Validation of parameter
		 * @throws App.ParameterException 不正な値が設定されている場合 throws App.ParameterException if invalid value is set
		 */
		public void validate() {
			// ファイル名 File name
			if (String.isBlank(fileName)) {
				throw new App.ParameterException('fileName', fileName);
			}
			// ファイルボディ File body
			if (String.isBlank(fileBody)) {
				throw new App.ParameterException('fileBody', fileBody);
			}
			// Type
			if (String.isBlank(type) || 
				(FileService.TYPE_MAP.get(this.type) != FileService.Type.Receipt && 
					FileService.TYPE_MAP.get(this.type) != FileService.Type.Quotation)) {
				throw new App.ParameterException('type', type);
			}
		}
	}

	/**
	 * @description ファイル保存結果を格納するクラス Request response for for SaveFileApi
	 */
	public class SaveFileResult implements RemoteApi.ResponseParam {
		/** 保存しファイルたのレコードID Content Document ID of stored file */
		public String contentDocumentId;
		/** 保存したファイルのレコードID Content Version ID of stored file */
		public String contentVersionId;
	}

	/**
	 * @description ファイル保存APIを実装するクラス Implementation class of save file API
	 */
	public with sharing class SaveFileApi extends RemoteApi.ResourceBase {

		/** ロールバックテストをする場合にtrue true if rollback test */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * @description ファイルを保存する Saves file into Salesforce Files
		 * @param req リクエストパラメータ(SaveFileParam) Request Parameter (SaveFileParam)
		 * @return レスポンス(SaveFileResult) Response (SaveFileResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する Capture request parameter
			SaveFileParam param = (SaveFileParam)req.getParam(FileResource.SaveFileParam.class);

			// パラメータ値の検証 Validate parameter
			param.validate();

			// 失敗時はDBをロールバックする Create DB save point to rollback if transaction fails
			Savepoint sp = Database.setSavepoint();

			AppContentVersionEntity savedEntity;
			try {
				// ファイルを保存 Save the file
				savedEntity = new FileService().saveFile(param.fileName, param.fileBody, param.type);

				// ロールバックテスト For rollback test
				// 強制的に例外を発生させてロールバックする Throw an exception and rollback
				if (isRollbackTest) {
					throw new App.IllegalStateException('SaveFileApi Rollback Test');
				}
			} catch (Exception e) {
				// ロールバック Rollback transaction
				Database.rollback(sp);
				throw e;
			}

			// レスポンスをセットする Set response
			FileResource.SaveFileResult res = new FileResource.SaveFileResult();
			res.contentDocumentId = savedEntity.contentDocumentId;
			res.contentVersionId = savedEntity.id;

			return res;
		}
	}
	
	/**
	 * @description ファイル一覧データ取得条件を格納するクラス Request parameter for for GetFileListApi
	 */
	public class GetFileListParam implements RemoteApi.RequestParam {
		/** Type */
		public String type;
		/** 社員ベースID Employee Base Id */
		public String empId;

		/**
		 * パラメータを検証する Validation of parameter
		 * @throws App.ParameterException 不正な値が設定されている場合 throws App.ParameterException if invalid value is set
		 */
		public void validate() {
			// タイプ Type
			if (FileService.TYPE_MAP.get(this.type) == null) {
				throw new App.ParameterException('type', this.type);
			}
			// 社員ID Employee ID
			if (String.isNotBlank(empId)) {
				try {
					Id.ValueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', empId);
				}
			}
		}
	}

	/**
	 * @description ファイル一覧データ取得結果を格納するクラス Request response for for GetFileListApi
	 */
	public class GetFileListResult implements RemoteApi.ResponseParam {
		/** ファイルのリスト List of Files */
		public List<File> files;
	}
	
	/**
	 * @description ファイル一覧データ取得APIを実装するクラス Implementation class of get file list from Employee Base Id API
	 */
	public with sharing class GetFileListApi extends RemoteApi.ResourceBase {

		/**
		 * @description ファイル一覧データを取得する Get list of files from Salesforce Files
		 * @param req リクエストパラメータ(GetFileListParam) Request Parameter (GetFileListParam)
		 * @return レスポンス(GetFileListResult) Response (GetFileListResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			FileResource.Util util = new FileResource.Util();

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する Capture request parameter
			GetFileListParam param = (GetFileListParam)req.getParam(FileResource.GetFileListParam.class);

			// パラメータ値の検証 Validate parameter
			param.validate();

			// 社員を取得 Retrieve EmployeeBaseEntity
			EmployeeBaseEntity empBase;
			if (String.isBlank(param.empId)) {
				// 社員IDが設定されていない場合、ログインユーザから社員情報を取得する If empId is not set, use login user information instead
				empBase = util.getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
			} else {
				empBase = util.getEmployee(param.empId, AppDate.today());
			}

			// ファイル一覧を取得 Retrieve list of files
			List<AppContentDocumentEntity> fileList = new FileService().getFileList(param.type, empBase.userId);

			// レスポンスをセットする Set response
			GetFileListResult res = new GetFileListResult();
			res.files = new List<File>();
			if (!fileList.isEmpty()) {
				for (AppContentDocumentEntity entity : fileList) {
					res.files.add(new File(entity));
				}
			}

			return res;
		}
	}
	
	/**
	 * @description ファイルデータ取得条件を格納するクラス Request parameter for for GetFileApi
	 */
	public class GetFileParam implements RemoteApi.RequestParam {
		/** ファイルID Content Version Id of the file */
		public String contentVersionId;

		/**
		 * パラメータを検証する Validation of parameter
		 * @throws App.ParameterException 不正な値が設定されている場合 throws App.ParameterException if invalid value is set
		 */
		public void validate() {
			// ファイルID Content Version ID
			if (String.isBlank(contentVersionId)) {
				throw new App.ParameterException('contentVersionId', contentVersionId);
			} else {
				try {
					Id.ValueOf(contentVersionId);
				} catch (Exception e) {
					throw new App.ParameterException('contentVersionId', contentVersionId);
				}
			}
		}
	}

	/**
	 * @description ファイルデータ取得結果を格納するクラス Request response for for GetFileApi
	 */
	public class GetFileResult implements RemoteApi.ResponseParam {
		/** ファイルドキュメントID Content Document Id of the file */
		public String contentDocumentId;
		/** タイトル Title */
		public String title;
		/** ファイルタイプ Filetype */
		public String fileType;
		/** ファイルボディ File body */
		public String fileBody;

		public String uploadedDate;

		public ExpAbbyyOCRService.OCRInfo ocrInfo;
	}

	/**
	 * @description ファイルデータ取得APIを実装するクラス Implementation class of get file API
	 */
	public with sharing class GetFileApi extends RemoteApi.ResourceBase {

		/**
		 * @description ファイルデータを取得する Get a file from Salesforce Files
		 * @param req リクエストパラメータ(GetFileParam) Request Parameter (GetFileParam)
		 * @return レスポンス(GetFileResult) Response (GetFileResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する Capture request parameter
			GetFileParam param = (GetFileParam)req.getParam(FileResource.GetFileParam.class);

			// パラメータ値の検証 Validate parameter
			param.validate();

			// ファイルデータを取得 Retrieve file
			AppContentVersionEntity file = new FileService().getFile(param.contentVersionId);
			ExpAbbyyOCRService.OCRInfo ocrInfo = new ExpAbbyyOCRService().searchEntityStatusByReceiptId(param.contentVersionId);

			// レスポンスをセットする Set response
			GetFileResult res = new GetFileResult();
			res.contentDocumentId = file.contentDocumentId;
			res.title = file.title;
			res.fileType = file.fileType;
			res.fileBody = file.versionDataText;
			res.uploadedDate = AppLanguage.getUserLang() == AppLanguage.JA ?
							   AppDateTime.valueOf(file.createdDate).formatLocal() :
							   AppDateTime.valueOf(file.createdDate).formatLocalDDMMYYYY() ;
			res.ocrInfo = ocrInfo;

			return res;
		}
	}
	
	/**
	 * @description ファイル削除条件を格納するクラス Request parameter for for DeleteFileApi
	 */
	public class DeleteFileParam implements RemoteApi.RequestParam {
		/** ファイルドキュメントIDのリスト List of Content Document Ids of the files to be deleted */
		public List<String> contentDocumentIds;

		/**
		 * パラメータを検証する Validation of parameter
		 * @throws App.ParameterException 不正な値が設定されている場合 throws App.ParameterException if invalid value is set
		 */
		public void validate() {
			// ファイルドキュメントID Content Document Ids
			if (contentDocumentIds == null || contentDocumentIds.isEmpty()) {
				throw new App.ParameterException('contentDocumentIds', contentDocumentIds);
			} else {
				for (String contentDocumentId : contentDocumentIds) {
					if (String.isNotBlank(contentDocumentId)) {
						try {
							Id.ValueOf(contentDocumentId);
						} catch (Exception e) {
							throw new App.ParameterException('contentDocumentId', contentDocumentId);
						}
					}
				}
			}
		}
	}

	/**
	 * @description ファイル削除APIを実装するクラス Implementation class of delete file API
	 */
	public with sharing class DeleteFileApi extends RemoteApi.ResourceBase {

		/** ロールバックテストをする場合にtrue true if rollback test */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * @description ファイルを削除する Delete links to Salesforce Files
		 * @param req リクエストパラメータ(DeleteFileParam) Request Parameter (DeleteFileParam)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する Capture request parameter
			DeleteFileParam param = (DeleteFileParam)req.getParam(FileResource.DeleteFileParam.class);

			// パラメータ値の検証 Validate parameter
			param.validate();

			// 失敗時はDBをロールバックする Create DB save point to rollback if transaction fails
			Savepoint sp = Database.setSavepoint();

			try {
				// ファイルを削除 Delete the files
				new FileService().deleteFile(param.contentDocumentIds);

				// ロールバックテスト For rollback test
				// 強制的に例外を発生させてロールバックする Throw an exception and rollback
				if (isRollbackTest) {
					throw new App.IllegalStateException('DeleteFileApi Rollback Test');
				}
			} catch (Exception e) {
				// ロールバック Rollback transaction
				Database.rollback(sp);
				throw e;
			}

			// 成功レスポンスをセットする Set response
			return null;
		}
	}


	public class ExecuteOCRParam implements RemoteApi.RequestParam {
		public String receiptId;

		public void validate() {
			ExpCommonUtil.validateId('receiptId', this.receiptId, true);
		}
	}

	public class ExecuteOCRResult implements RemoteApi.ResponseParam {
		/** ファイルドキュメントID Content Document Id of the file */
		public String taskId;
	}

	public with sharing class ExecuteOCRApi extends RemoteApi.NoRollbackResourceBase {

		/**
		 * @description ファイルを削除する Delete links to Salesforce Files
		 * @param req リクエストパラメータ(DeleteFileParam) Request Parameter (DeleteFileParam)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する Capture request parameter
			ExecuteOCRParam param = (ExecuteOCRParam)req.getParam(FileResource.ExecuteOCRParam.class);

			// パラメータ値の検証 Validate parameter
			param.validate();

			String taskId = new ExpAbbyyOCRService().executeOCR(param.receiptId);

			// Set response
			ExecuteOCRResult res = new ExecuteOCRResult();
			res.taskId = taskId;
			return res;
		}
	}

	public class GetOCRStatusParam implements RemoteApi.RequestParam {
		public String taskId;

		public void validate() {
			ExpCommonUtil.validateStringIsNotBlank('taskId', this.taskId);
		}
	}

	public class GetOCRStatusResult implements RemoteApi.ResponseParam {
		/** ファイルドキュメントID Content Document Id of the file */
		public ExpAbbyyOCRService.OCRInfo ocrInfo;
	}

	public with sharing class GetOCRStatusApi extends RemoteApi.NoRollbackResourceBase {

		/**
		 * @description ファイルを削除する Delete links to Salesforce Files
		 * @param req リクエストパラメータ(DeleteFileParam) Request Parameter (DeleteFileParam)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			GetOCRStatusParam param = (GetOCRStatusParam)req.getParam(FileResource.GetOCRStatusParam.class);

			param.validate();

			ExpAbbyyOCRService.OCRInfo ocrInfo = new ExpAbbyyOCRService().getOCRStatus(param.taskId);

			// Set response
			GetOCRStatusResult res = new GetOCRStatusResult();
			res.ocrInfo = ocrInfo;
			return res;
		}
	}
	
	
	/**
	 * ファイルAPIのユーティリティクラス Utility class for File API
	 */
	public class Util {

		/**
		 * 指定したユーザIDを持つ社員を取得する Retrieve employee details from specified User ID 
		 * @param userId ユーザID User ID
		 * @return 社員ベースエンティティ EmployeeBaseEntity
		 * @throws App.IllegalStateException 社員が見つからない場合、複数登録されている場合 
		 * Throws App.IllegalStateException if employee is not found or multiple employees with the same User ID 
		 */
		public EmployeeBaseEntity getEmployeeByUserId(Id userId, AppDate targetDate) {
			EmployeeService empService = new EmployeeService();
			return empService.getEmployeeByUserId(userId, targetDate);
		}

		/**
		 * 指定した社員IDの社員を取得する Retrieve employee details from specified Employee ID
		 * @param empId 社員ベースID Employee Base ID
		 * @return 社員ベースエンティティ EmployeeBaseEntity
		 * @throws App.IllegalStateException 社員が見つからない場合 Throws App.IllegalStateException if employee is not found
		 */
		public EmployeeBaseEntity getEmployee(Id empId, AppDate targetDate) {
			EmployeeRepository repo = new EmployeeRepository();
			EmployeeBaseEntity employee = repo.getEntity(empId, targetDate);
			if (employee == null) {
				// メッセージ：指定された社員が見つかりません。Employee not found
				throw new App.IllegalStateException(
						ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Employee})
						+ '(ID='+ empId + ')');
			}
			return employee;
		}
	}
}