/**
 * ある期間のカレンダーより労基法のルールに合わせて法定休日を自動割当するロジック
 * 
 * 一定の期間内の法定休日指定する日を決定する
 * ロジックは以下の優先順位で、法定休日指定する日を決める
 * 1. カレンダー上の法定休日と確定済みで法定休日指定済みの日
 * 2. (確定/未確定にかかわらず)休日出勤していない優先法定休日を期間の末尾から日数分
 * 3. (確定/未確定にかかわらず)休日出勤していない休日を期間の末尾から日数分
 * 4. 未確定で、休日出勤している優先法定休日を期間の末尾から日数分
 * 5. 未確定で、休日出勤している休日を期間の末尾から日数分
 * 6. 未確定の勤務日を末尾から日数分
 *
 * 未確定の日は、上の６つの条件のどれかに必ず当てはまるため、法定休日の日数より
 * 期間の未確定の日数が多ければ、必ず法定休日は割り当てられる。
 * @author arimotoyosuke
 */
public with sharing class AttCalcAutoHolidayLogic {
	/**
	 * 判定対象日の種別
	 * 法定休日 -- すでに法定休日と指定されている日。かならず法定休日として扱われる
	 * 優先法定休日 -- 法定休日を決めるとき、休日より優先して法定休日として扱われる日
	 *Holiday -- 通常の休日
	 * Workday --Holidayではない日
	 */
	public enum DayType { Workday,Holiday, PreferredLegalHoliday, LegalHoliday }
	/**
	 * 法定休日判定の対象の日
	 * 
	 * 法定休日自動判定のロジックで使用する日次の属性を返すためのインターフェース
	 */
	public interface iTargetDay {
		/**
		 * 対象日が確定済みか
		 * 
		 * 確定済みの場合、法定休日指定は変更されない
		 * @return 確定済みか
		 */
		Boolean isAdmit();
		/**
		 * 対象日の種別
		 * @return 種別
		 */
		DayType getDayType();
		/**
		 * 対象日に休日出勤をしているかどうかを返す。
		 *Holiday出勤が申請済みの日より、未申請の日が優先される。
		 * 
		 * このロジックのスコープ外の仕様ではあるが
		 * V5では勤務時間の有無を休日出勤の判定に使用していたが、
		 * Genieでは、休日出勤申請の有無を判定の基準にする
		 * 
		 * @return 休日出勤が申請済みか
		 */
		Boolean isHolidayWork();
		/**
		 * 現在、法定休日指定が既にされているかどうかを返す。
		 * 確定済みの日の場合はこの指定が最優先される
		 * @return 法定休日指定
		 */
		Boolean isLegalHoliday();
	}
	/**
	 * 法定休日自動割当の入力パラメタ
	 */
	public interface iInput {
		/**
		 * 期間に割り当てる法定休日の日数
		 * @return 日数
		 */
		Integer getLegalHolidayCount();
		/**
		 * 法定休日を割り当てる期間分の日の属性
		 * @return 期間の対象日の配列
		 */
		iTargetDay[] getTargetDayList();
	}
	/**
	 * 法定休日自動割当の出力パラメタ
	 */
	public interface iOutput {
		/**
		 * 期間の日数の法定休日割当結果を出力する
		 * @param days 期間の日数分の割当結果の配列。Input.期間の配列と同じ順番と数で、法定休日割当の結果を返す。
		 */
		void setLegalHoliday(Boolean[] days);
	}
	
	/**
	 * 法定休日自動割当のロジックを実行する
	 * @param Output 出力パラメタ
	 * @param Input 入力パラメタ
	 */
	public static void apply(iOutput output, iInput input) {
		List<Boolean> legalHolidayList = new List<Boolean>();
		for (Integer i = 0; i <= input.getTargetDayList().size() -1; i++) {
			legalHolidayList.add(false);
		}
		// 法定休日割当残日数のカウンタ
		Integer cnt = input.getLegalHolidayCount();
		// Step.1
		// 法定休日 あるいは 確定済みの日で法定休日指定済みのものはすべて法定休日とする
		// cnt はマイナスになる場合もある
		for(Integer i = 0; i < input.getTargetDayList().size(); i++) {
			iTargetDay d = input.getTargetDayList()[i];
			if(d.getDayType() == DayType.LegalHoliday) {
				legalHolidayList[i] = true;
				cnt --;
			}
			else if(d.isAdmit() && d.isLegalHoliday()) {
				legalHolidayList[i] = true;
				cnt --;
			}
		}
		// Step.2
		//休日出勤していない優先法定休日を、期間の後ろから法定休日にする
		for(Integer i = input.getTargetDayList().size() - 1; i >= 0 && cnt > 0 ; i--) {
			iTargetDay d = input.getTargetDayList()[i];
			// Step1でカウントしたものを取り除くため !(d.isAdmit() && d.isLegalHoliday()) が必要
			if(d.getDayType() == DayType.PreferredLegalHoliday && ! d.isHolidayWork() && ! (d.isAdmit() && d.isLegalHoliday())) {
				legalHolidayList[i] = true;
				cnt --;
			}
		}
		// Step.3
		//休日出勤していない休日を、期間の後ろから法定休日にする
		for(Integer i = input.getTargetDayList().size() - 1; i >= 0 && cnt > 0 ; i--) {
			iTargetDay d = input.getTargetDayList()[i];
			// Step1でカウントしたものを取り除くため !(d.isAdmit() && d.isLegalHoliday()) が必要
			if(d.getDayType() == DayType.Holiday && ! d.isHolidayWork() && ! (d.isAdmit() && d.isLegalHoliday())) {
				legalHolidayList[i] = true;
				cnt --;
			}
		}
		// Step.4
		// 未確定で休日出勤している優先法定休日を、期間の後ろから法定休日にする
		for(Integer i = input.getTargetDayList().size() - 1; i >= 0 && cnt > 0 ; i--) {
			 iTargetDay d = input.getTargetDayList()[i];
			if(! d.isAdmit() && d.getDayType() == DayType.PreferredLegalHoliday && d.isHolidayWork()) {
				legalHolidayList[i] = true;
				cnt --;
			}
		}
		// Step.5
		// 未確定で休日出勤している休日を、期間の後ろから法定休日にする
		for(Integer i = input.getTargetDayList().size() - 1; i >= 0 && cnt > 0 ; i--) {
			iTargetDay d = input.getTargetDayList()[i];
			if(! d.isAdmit() && d.getDayType() == DayType.Holiday && d.isHolidayWork()) {
				legalHolidayList[i] = true;
				cnt --;
			}
		}
		// Step.6
		// 未確定の勤務日を、期間の後ろから法定休日にする
		for(Integer i = input.getTargetDayList().size() - 1; i >= 0 && cnt > 0 ; i--) {
			iTargetDay d = input.getTargetDayList()[i];
			if(! d.isAdmit() && d.getDayType() == DayType.Workday) {
				legalHolidayList[i] = true;
				cnt --;
			}
		}
		output.setLegalHoliday(legalHolidayList);
	}
}