/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description 精算確定のサービスクラス Service class for finance approval function
 */
public with sharing class ExpFinanceApprovalService {

	/** Searchフィルタ */
	 public class SearchFilter {
		/** ステータス **/
		// public List<String> statusList;
		public List<String> financeStatusList;
		/** 部署 **/
		public List<Id> departmentBaseIds;
		/** 社員 **/
		public List<Id> empBaseIds;
		/** 申請日 **/
		public ExpReportRequestRepository.DateRange requestDateRange;
		/** 計上日 **/
		public ExpReportRequestRepository.DateRange accountingDateRange;
		/** レポートId **/
		public String reportNo;
		/** 金額 **/
		public ExpReportRequestRepository.AmountRange amountRange;
	 }

	/** Max size of list */
	private static final Integer MAX_LIST_SIZE = 1000;

	/** History type */
	public static final String HISTORY_TYPE_ACCOUNTING_AUTHORIZED = 'AccountingAuthorized';
	public static final String HISTORY_TYPE_ACCOUNTING_REJECTED = 'AccountingRejected';


	/** Result of getting request ID list */
	public class RequestIdListResult {
		/** Total size of list */
		public Integer totalSize {public get; private set;}
		/** Request ID list */
		public List<Id> requestIdList  {public get; private set;}

		public RequestIdListResult() {
			this.totalSize = 0;
		}
	}

	/**
	 * @description 経費申請ID一覧データを取得する Get request ID list
	 * @param companyId 会社ID
	 * @param sortKey ソートキー
	 * @param sortOrder ソート順
	 * @return 経費申請IDのリスト
	 */
	public RequestIdListResult getRequestIdList(Id companyId, ExpReportRequestRepository.SortKey sortKey, ExpReportRequestRepository.SortOrder sortOrder, SearchFilter SearchFilter) {
		RequestIdListResult result = new RequestIdListResult();

		ExpReportRequestRepository.Filter filter = new ExpReportRequestRepository.Filter();
		filter.companyId = companyId;

		if(SearchFilter != null){
			filter.financeStatusList = SearchFilter.financeStatusList;
			filter.departmentBaseIds = SearchFilter.departmentBaseIds;
			filter.empBaseIds = SearchFilter.empBaseIds;
			filter.requestDateRange = SearchFilter.requestDateRange;
			filter.accountingDateRange = SearchFilter.accountingDateRange;
			filter.reportNo = SearchFilter.reportNo;
			filter.amountRange = SearchFilter.amountRange;
		}

		// 検索を実行する
		List<ExpReportRequestEntity> requestList = new ExpReportRequestRepository().searchEntity(filter, sortKey, sortOrder);

		if (requestList == null || requestList.isEmpty()) {
			return result;
		}

		result.totalSize = requestList.size();
		Integer listSize = requestList.size() > MAX_LIST_SIZE ? MAX_LIST_SIZE : requestList.size();
		List<Id> requestIdList = new List<Id>();
		for (Integer i = 0; i < listSize; i++) {
			requestIdList.add(requestList.get(i).id);
		}
		result.requestIdList = requestIdList;

		return result;
	}

	/**
	 * @description 経費申請一覧データを取得する Get ExpReportRequest list
	 * @param requestIds 取得対象の申請IDのリスト
	 * @return 経費申請エンティティのリスト
	 */
	public List<ExpReportRequestEntity> getRequestList(List<Id> requestIds) {

		// 承認済みの経費申請データを取得 Get approved ExpReportRequest list
		List<ExpReportRequestEntity> requestList = new ExpReportRequestRepository().getEntityList(requestIds);
		if (requestList == null || requestList.isEmpty()) {
			return null;
		}

		Map<Id, ExpReportRequestEntity> requestEntityMapById = new Map<Id, ExpReportRequestEntity>();
		List<Id> reportIds = new List<Id>();
		for (ExpReportRequestEntity request : requestList) {
			requestEntityMapById.put(request.id, request);
			reportIds.add(request.expReportId);
		}

		List<ExpReportRequestEntity> result = new List<ExpReportRequestEntity>();
		for (Id requestId : requestIds) {
			if (requestEntityMapById.containsKey(requestId)) {
				result.add(requestEntityMapById.get(requestId));
			}
		}

		// 取得した経費申請に紐づく経費明細を取得 Get ExpRecord list
		List<ExpRecordEntity> recordList = new ExpRecordRepository().getEntityListByReportId(reportIds);

		Set<Id> reportIdsWithReceipt = new Set<Id>();
		if (recordList != null && !recordList.isEmpty()) {

			// get all ExpTypes used for the ExpRecords
			// Record's Record Type, and usage of File Attachement will be always retrieved from the recordItemList[0]
			// When Record Type is 'Hotel Fee', then recordItemList[0] contains the Record data. Otherwise, only 1 recordItem will be stored in the record.
			Set<Id> expTypeIdSet = new Set<Id>();
			for (ExpRecordEntity record : recordList) {
				expTypeIdSet.add(record.recordItemList[0].expTypeId);
			}

			// retrieve ExpType list
			ExpTypeRepository.SearchFilter filter = new ExpTypeRepository.SearchFilter();
			filter.ids = expTypeIdSet;
			List<ExpTypeEntity> expTypeList = new ExpTypeRepository().searchEntity(filter);

			// create ExpTypeId - ExpTypeEntity map
			Map<Id, ExpTypeEntity> expTypeIdMap = new Map<Id, ExpTypeEntity>();
			for (ExpTypeEntity expTypeEntity : expTypeList) {
				expTypeIdMap.put(expTypeEntity.id, expTypeEntity);
			}

			for (ExpRecordEntity record : recordList) {
				
				// Record's Record Type, and usage of File Attachement will be always retrieved from the recordItemList[0]
				// When Record Type is 'Hotel Fee', then recordItemList[0] contains the Record data. Otherwise, only 1 recordItem will be stored in the record.
				ExpTypeEntity recordExpType = expTypeIdMap.get(record.recordItemList[0].expTypeId);
				if (recordExpType.fileAttachment == ExpFileAttachment.REQUIRED || recordExpType.fileAttachment == ExpFileAttachment.OPTIONAL) {
					reportIdsWithReceipt.add(record.expReportId);
				}
			}
		}

		// 領収書有無のフラグをセット
		for (ExpReportRequestEntity request : result) {
			request.hasReceipts = reportIdsWithReceipt.contains(request.expReportId) ? true : false;
		}

		return result;
	}

	/**
	 * 経費精算詳細データを取得する Get ExpReport detail
	 * @param requestId 経費申請ID
	 * @return 経費精算エンティティ
	 */
	public ExpReportEntity getExpReport(Id requestId) {

		// 経費精算データを取得 Get ExpReport detail
		ExpReportEntity report = new ExpService().getExpReportByRequestId(requestId);

		if (report == null) {
			// メッセージ：該当する申請が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_EXP_REPORT,
					ComMessage.msg().Exp_Err_NotFoundRequest);
		}

		return report;
	}

	/**
	 * 経費申請のステータスを取得する Get request status
	 * @param requestId 経費申請ID
	 * @return ステータス
	 */
	public ExpReportRequestEntity.DetailStatus getRequestStatus(Id requestId) {

		// 経費申請データを取得 Get ExpReportRequest data
		ExpReportRequestEntity request = new ExpReportRequestRepository().getEntity(requestId);

		if (request == null) {
			// メッセージ：該当する申請が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_EXP_REPORT,
					ComMessage.msg().Exp_Err_NotFoundRequest);
		}

		// ステータスを取得 Get status
		ExpReportRequestEntity.DetailStatus status = request.getDetailStatus();

		return status;
	}

	/**
	 * @description 申請を承認する Approve ExpReportRequest
	 * @param requestIds 経費申請IDのリスト
	 */
	public void approve(List<Id> requestIds) {
		ExpReportRequestRepository repo = new ExpReportRequestRepository();

		// 承認対象の経費申請データを取得 Get ExpReportRequest list for approval
		List<ExpReportRequestEntity> requestList = repo.getEntityList(requestIds);
		if (requestList == null || requestList.isEmpty()) {
			return;
		}

		// 精算確定ステータスを更新 Update accounting status
		for (ExpReportRequestEntity request : requestList) {
			request.accountingStatus = ExpAccountingStatus.AUTHORIZED;
			request.authorizedTime = AppDatetime.now();
		}
		repo.saveEntityList(requestList);

		// 履歴データを作成 Create history
		List<ExpModificationHistoryEntity> historyList = new List<ExpModificationHistoryEntity>();
		for (ExpReportRequestEntity request : requestList) {
			ExpModificationHistoryEntity history = createHistory(request, HISTORY_TYPE_ACCOUNTING_AUTHORIZED, null);
			historyList.add(history);
		}
		ExpModificationHistoryRepository historyRepo = new ExpModificationHistoryRepository();
		historyRepo.saveEntityList(historyList);
	}

	/**
	 * @description 申請を却下する Reject ExpReportRequest
	 * @param requestIds 経費申請IDのリスト
	 * @param comment コメント
	 */
	public void reject(List<Id> requestIds, String comment) {
		ExpReportRequestRepository repo = new ExpReportRequestRepository();

		// 却下対象の経費申請データを取得 Get ExpReportRequest list for rejecting
		List<ExpReportRequestEntity> requestList = repo.getEntityList(requestIds);
		if (requestList == null || requestList.isEmpty()) {
			return;
		}

		// 申請レコードを更新 Update request records
		for (ExpReportRequestEntity request : requestList) {
			request.status = AppRequestStatus.DISABLED;
			request.cancelType = AppCancelType.CANCELED;
			request.isConfirmationRequired = true;
			request.ProcessComment = null;
			request.lastApproverId = null;
			request.lastApproveTime = null;
			request.cancelComment = comment;
			request.accountingStatus = ExpAccountingStatus.REJECTED;
			request.authorizedTime = null;
		}
		repo.saveEntityList(requestList);

		// 履歴データを作成 Create history
		List<ExpModificationHistoryEntity> historyList = new List<ExpModificationHistoryEntity>();
		for (ExpReportRequestEntity request : requestList) {
			ExpModificationHistoryEntity history = createHistory(request, HISTORY_TYPE_ACCOUNTING_REJECTED, comment);
			historyList.add(history);
		}
		ExpModificationHistoryRepository historyRepo = new ExpModificationHistoryRepository();
		historyRepo.saveEntityList(historyList);
	}

	/*
	 * Get modification history by the request ID given
	 * @param requestId Request ID of the target record
	 * @return List of modification history of the report
	 */
	public List<ExpModificationHistoryEntity> getModificationHistory(Id requestId) {
		ExpModificationHistoryRepository.SearchFilter filter = new ExpModificationHistoryRepository.SearchFilter();
		filter.requestId = requestId;
		filter.excludedFields = new List<String>{HISTORY_TYPE_ACCOUNTING_AUTHORIZED, HISTORY_TYPE_ACCOUNTING_REJECTED};
		List<ExpModificationHistoryEntity> resultList = new ExpModificationHistoryRepository().searchEntity(filter);

		// Set display name to the result
		for (ExpModificationHistoryEntity entity : resultList) {
			ExpModificationHistoryLogic logic = new ExpModificationHistoryLogic();
			logic.setDisplayLabel(entity);
		}
		return resultList;
	}

	/**
	 * @description 経理承認/却下時の履歴データを作成する Create authorize/reject history
	 * @param request 経費申請エンティティ
	 * @param type タイプ（承認 or 却下）
	 * @param comment コメント
	 */
	private ExpModificationHistoryEntity createHistory(ExpReportRequestEntity request, String type, String comment) {
		// 社員情報を取得 Get employee
		EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());

		// 履歴を作成 Create history
		ExpModificationHistoryEntity history = new ExpModificationHistoryEntity();
		history.expRequestId = request.id;
		history.expReportId = request.expReportId;
		history.field = type;
		history.comment = comment;
		history.modifiedBy = employee.id;
		history.modifiedDateTime = AppDatetime.now();

		return history;
	}
}