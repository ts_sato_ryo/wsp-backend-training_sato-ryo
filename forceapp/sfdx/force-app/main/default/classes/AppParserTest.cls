/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * AppParserのテスト
 */
@isTest
private class AppParserTest {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	@isTest static void validateParseIntegerTest() {
		final String fieldName = 'TestField';
		String stringValue;
		Integer maxValue;

		// 未設定 → OK
		stringValue = null;
		AppParser.validateParseInteger(fieldName, stringValue);

		// 整数値 → OK
		stringValue = '1234';
		AppParser.validateParseInteger(fieldName, stringValue);

		// 負の整数 → OK
		stringValue = '-1';
		AppParser.validateParseInteger(fieldName, stringValue);

		// 小数点以下の数値を含む → NG
		try {
			stringValue = '1.23';
			AppParser.validateParseInteger(fieldName, stringValue);
		} catch (App.TypeException e) {
			String msg = MESSAGE.Com_Err_InvalidValueInteger(new List<String>{fieldName});
			System.assertEquals(msg, e.getMessage());
		}

		// 桁数多い → NG
		try {
			stringValue = '12345678901234567890';
			AppParser.validateParseInteger(fieldName, stringValue);
		} catch (App.TypeException e) {
			String msg = MESSAGE.Com_Err_InvalidValue(new List<String>{fieldName});
			System.assertEquals(msg, e.getMessage());
		}

		// 桁数多い(最大値が設定されている) → NG
		try {
			stringValue = '12345678901234567890';
			maxValue = 255;
			AppParser.validateParseInteger(fieldName, stringValue, maxValue);
		} catch (App.TypeException e) {
			String msg = ComMessage.msg().Com_Err_MaxValue(new List<String>{fieldName, String.valueOf(MaxValue)});
			System.assertEquals(msg, e.getMessage());
		}

		// 数値ではない → NG
		try {
			stringValue = '1a';
			AppParser.validateParseInteger(fieldName, stringValue);
		} catch (App.TypeException e) {
			String msg = MESSAGE.Com_Err_InvalidValueInteger(new List<String>{fieldName});
			System.assertEquals(msg, e.getMessage());
		}
	}

}
