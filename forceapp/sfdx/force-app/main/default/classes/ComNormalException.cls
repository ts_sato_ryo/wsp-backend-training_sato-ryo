/*
 * 正常系Exception
 * 入力チェックや規定関連等、ユーザの入力情報によりエラーとなる場合に使用する
 */
public with sharing class ComNormalException extends Exception {

}