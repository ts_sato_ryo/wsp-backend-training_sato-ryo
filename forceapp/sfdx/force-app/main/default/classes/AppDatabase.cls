/**
 * データベースを操作する機能を提供するクラス(CUDUtil.clsから移行)
 * 本クラスのメソッドは原則リポジトリクラスからのみ実行するようにしてください。
 * (バッチ処理やトリガなどチューニングが必要な場合に限り、直接実行することを可能とします）
 */
public with sharing class AppDatabase {

	// CURDのunitTestは出来ないので強制的にエラーにするフラグを用意
	@TestVisible private static Boolean isTestObjectPermissionError = false;
	@TestVisible private static Boolean isTestFieldPermissionError = false;

	/**
	 * レコードを新規作成
	 * @param record 新規作成するレコード
	 * @return 作成結果
	 */
	public static Database.SaveResult doInsert(SObject record) {
		return doInsert(record, true);
	}

	/**
	 * レコードを新規作成
	 * @param record 新規作成するレコード
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 作成結果
	 */
	public static Database.SaveResult doInsert(SObject record, Boolean allOrNone) {
		if (record != null) {
			validateInsert(record);
			return Database.insert(record, allOrNone);
		}
		return null;
	}

	/**
	 * 複数レコードを新規作成
	 * @param recordList 新規作成するレコード一覧
	 * @return 作成結果
	 */
	public static List<Database.SaveResult> doInsert(List<Sobject> recordList) {
		return doInsert(recordList, true);
	}

	/**
	 * 複数レコードを新規作成
	 * @param recordList 新規作成するレコード一覧
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 作成結果
	 */
	public static List<Database.SaveResult> doInsert(List<Sobject> recordList, Boolean allOrNone) {
		if (recordList != null && !recordList.isEmpty()) {
			validateInsert(recordList[0]);
			return Database.insert(recordList, allOrNone);
		}
		return new List<Database.SaveResult>();
	}


	/**
	 * レコードを更新
	 * @param record 更新するレコード
	 * @return 更新結果
	 */
	public static Database.SaveResult doUpdate(SObject record) {
		return doUpdate(record, true);
	}

	/**
	 * レコードを更新
	 * @param record 更新するレコード
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 更新結果
	 */
	public static Database.SaveResult doUpdate(SObject record, Boolean allOrNone) {
		if (record != null) {
			return doUpdate(new List<SObject>{record}, allOrNone).get(0);
		}
		return null;
	}

	/**
	 * 複数レコードを更新
	 * @param recordList 更新するレコード一覧
	 * @return 更新結果
	 */
	public static List<Database.SaveResult> doUpdate(List<Sobject> recordList) {
		return doUpdate(recordList, true);
	}

	/**
	 * 複数レコードを更新
	 * @param recordList 更新するレコード一覧
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 更新結果
	 */
	public static List<Database.SaveResult> doUpdate(List<Sobject> recordList, Boolean allOrNone) {
		if (recordList != null && !recordList.isEmpty()) {
			validateUpdate(recordList[0]);
			return Database.update(recordList, allOrNone);
		}
		return new List<Database.SaveResult>();
	}


	/**
	 * レコードを新規作成/更新
	 * @param record 新規作成/更新するレコード
	 * @return 新規作成/更新結果
	 */
	public static Database.UpsertResult doUpsert(SObject record) {
		return doUpsert(record, true);
	}

	/**
	 * レコードを新規作成/更新
	 * @param record 新規作成/更新するレコード
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 新規作成/更新結果
	 */
	public static Database.UpsertResult doUpsert(SObject record, Boolean allOrNone) {
		if (record != null) {
			if (record.get('Id') == null) {
				validateInsert(record);
			}
			else {
				validateUpdate(record);
			}
			return Database.upsert(record, allOrNone);
		}
		return null;
	}

	/**
	 * 複数レコードを新規作成/更新
	 * @param recordList 新規作成/更新するレコード一覧
	 * @return 新規作成/更新結果
	 */
	public static List<Database.UpsertResult> doUpsert(List<Sobject> recordList) {
		return doUpsert(recordList, true);
	}

	/**
	 * 複数レコードを新規作成/更新
	 * @param recordList 新規作成/更新するレコード一覧
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 新規作成/更新結果
	 */
	public static List<Database.UpsertResult> doUpsert(List<Sobject> recordList, Boolean allOrNone) {
		if (recordList != null && !recordList.isEmpty()) {
			Boolean executeInsert = false;
			Boolean executeUpdate = false;
			for (SObject record : recordList) {
				if (record.get('Id') == null) {
					// 既にチェックされているか
					if (!executeInsert) {
						validateInsert(record);
						executeInsert = true;
					}
				}
				else {
					// 既にチェックされているか
					if (!executeUpdate) {
						validateUpdate(record);
						executeUpdate = true;
					}
				}
				// 両方チェックしたら終了
				if (executeInsert && executeUpdate) {
					break;
				}
			}
			return Database.upsert(recordList, allOrNone);
		}
		return new List<Database.UpsertResult>();
	}


	/**
	 * レコードを削除
	 * @param record 削除するレコード
	 * @return 削除結果
	 */
	public static Database.DeleteResult doDelete(SObject record) {
		return doDelete(record, true);
	}

	/**
	 * レコードを削除
	 * @param record 削除するレコード
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 削除結果
	 */
	public static Database.DeleteResult doDelete(SObject record, Boolean allOrNone) {
		if (record != null) {
			return doDelete(new List<SObject>{record}, allOrNone).get(0);
		}
		return null;
	}

	/**
	 * 複数レコードを削除
	 * @param recordList 削除するレコード一覧
	 * @return 削除結果
	 */
	public static List<Database.DeleteResult> doDelete(List<Sobject> recordList) {
		return doDelete(recordList, true);
	}

	/**
	 * 複数レコードを削除
	 * @param recordList 削除するレコード一覧
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 削除結果
	 */
	public static List<Database.DeleteResult> doDelete(List<Sobject> recordList, Boolean allOrNone) {
		if (recordList != null && !recordList.isEmpty()) {
			validateDelete(recordList[0].getSObjectType());
			return Database.delete(recordList, allOrNone);
		}
		return new List<Database.DeleteResult>();
	}


	/**
	 * レコードを削除
	 * @param recordId 削除するレコードId
	 * @return 削除結果
	 */
	public static Database.DeleteResult doDelete(Id recordId) {
		return doDelete(recordId, true);
	}

	/**
	 * レコードを削除
	 * @param recordId 削除するレコードId
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 削除結果
	 */
	public static Database.DeleteResult doDelete(Id recordId, Boolean allOrNone) {
		if (recordId != null) {
			return doDelete(new List<Id>{recordId}, allOrNone).get(0);
		}
		return null;
	}

	/**
	 * 複数レコードを削除
	 * @param recordIdList 削除するレコードId一覧
	 * @return 削除結果
	 */
	public static List<Database.DeleteResult> doDelete(List<Id> recordIdList) {
		return doDelete(recordIdList, true);
	}

	/**
	 * 複数レコードを削除
	 * @param recordIdList 削除するレコードId一覧
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 削除結果
	 */
	public static List<Database.DeleteResult> doDelete(List<Id> recordIdList, Boolean allOrNone) {
		if (recordIdList != null && !recordIdList.isEmpty()) {
			validateDelete(recordIdList[0].getSObjectType());
			return Database.delete(recordIdList, allOrNone);
		}
		return new List<Database.DeleteResult>();
	}

	/**
	 * 複数レコードを削除
	 * @param recordIdList 削除するレコードId一覧
	 * @return 削除結果
	 */
	public static List<Database.DeleteResult> doDelete(Set<Id> recordIdSet) {
		return doDelete(recordIdSet, true);
	}

	/**
	 * 複数レコードを削除
	 * @param recordIdList 削除するレコードId一覧
	 * @param allOrNone true：レコードへの処理が失敗した場合、Exceptionが発生する false：レコードへの処理が失敗した場合も正常に処理を行い、戻り値として結果を返す
	 * @return 削除結果
	 */
	public static List<Database.DeleteResult> doDelete(Set<Id> recordIdSet, Boolean allOrNone) {
		return doDelete(new List<Id>(recordIdSet), allOrNone);
	}


	/**
	 * Insert時の権限を確認する
	 * @param sObj 対象レコード
	 */
	private static void validateInsert(SObject sObj) {
		DescribeSObjectResult dsr = sObj.getSObjectType().getDescribe();
		// オブジェクトの作成権限があるか（テスト時は指定なしの場合、チェックしない）
		if ((!dsr.isCreateable() && !Test.isRunningTest()) || isTestObjectPermissionError) {
			throw new ComSemiNormalException(ComMessage.msg().Com_Err_NoObjectInsert(new List<String>{ dsr.getName() }));
		}
		// 使用（取得、格納）している項目のみチェックする
		Map<String, Schema.SObjectField> fieldMap = dsr.fields.getMap();
		for (String fieldName : sObj.getPopulatedFieldsAsMap().keyset()) {
			// リファレンス(__r)等存在しない項目がチェックしない
			if (fieldMap.containsKey(fieldName)) {
				DescribeFieldResult fr = fieldMap.get(fieldName).getDescribe();
				// チェック対象外項目である場合、検証しない
				if (isNoPermissionCheckField(fr)) {
					continue;
				}
				// 項目に作成権限があるか（テスト時は指定なしの場合、チェックしない）
				if ((!fr.isCreateable() && !Test.isRunningTest()) || isTestFieldPermissionError) {
					throw new ComSemiNormalException(ComMessage.msg().Com_Err_NoFieldInsert(new List<String>{ dsr.getName(), fieldName }));
				}
			}
		}
	}

	/**
	 * Update時の権限を確認する
	 * @param sObj 対象レコード
	 */
	private static void validateUpdate(SObject sObj) {
		DescribeSObjectResult dsr = sObj.getSObjectType().getDescribe();
		// オブジェクトの更新権限があるか（テスト時は指定なしの場合、チェックしない）
		if ((!dsr.isUpdateable() && !Test.isRunningTest()) || isTestObjectPermissionError) {
			throw new ComSemiNormalException(ComMessage.msg().Com_Err_NoObjectUpdate(new List<String>{ dsr.getName() }));
		}
		// 使用（取得、格納）している項目のみチェックする
		Map<String, Schema.SObjectField> fieldMap = dsr.fields.getMap();
		for (String fieldName : sObj.getPopulatedFieldsAsMap().keyset()) {
			// リファレンス(__r)等存在しない項目がチェックしない
			if (fieldMap.containsKey(fieldName)) {
				DescribeFieldResult fr = fieldMap.get(fieldName).getDescribe();
				// チェック対象外項目である場合、検証しない
				if (isNoPermissionCheckField(fr)) {
					continue;
				}
				// 項目に更新権限があるか（テスト時は指定なしの場合、チェックしない）
				if ((!fr.isUpdateable() && !Test.isRunningTest()) || isTestFieldPermissionError) {
					throw new ComSemiNormalException(ComMessage.msg().Com_Err_NoFieldUpdate(new List<String>{ dsr.getName(), fieldName }));
				}
			}
		}
	}

	/**
	 * Delete時の権限を確認する
	 * @param stype 対象オブジェクトタイプ
	 */
	private static void validateDelete(Schema.SObjectType stype) {
		DescribeSObjectResult dsr = stype.getDescribe();
		// オブジェクトの削除権限があるか（テスト時は指定なしの場合、チェックしない）
		if ((!dsr.isDeletable() && !Test.isRunningTest()) || isTestObjectPermissionError) {
			throw new ComSemiNormalException(ComMessage.msg().Com_Err_NoObjectDelete(new List<String>{ dsr.getName() }));
		}
	}

	/**
	 * 作成、更新権限チェック対象外項目であるか
	 * @param fr 対象項目
	 * @return チェック対象外項目の場合はtrue
	 */
	private static boolean isNoPermissionCheckField(DescribeFieldResult fr) {
			return !fr.isPermissionable() || // 権限設定ができない(IdやName等システム項目や必須項目)
			fr.isCalculated() || // 数式
			fr.isAutoNumber(); // 自動採番
	}
}
