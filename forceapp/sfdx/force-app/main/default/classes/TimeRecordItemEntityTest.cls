/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * @description TimeRecordItemEntityのテストクラス
 */
@isTest
private class TimeRecordItemEntityTest {

	/**
	 * 参照項目テスト
	 * 参照項目がない場合にエラーを発生しないことを確認する
	 * 参照項目がない場合にnull または 初期値が返ってくることを確認する
	 */
	 @isTest static void noReferenceItemTest() {
		// jobIdで紐付くもの
		// jobCode・jobName・isDirectChargedJob
		 {
			 try {
				 TimeRecordItemEntity entity = new TimeRecordItemEntity();
				 // Case-1 : jobIdの紐付けがない
				 System.assertEquals(null, entity.jobCode);
				 System.assertEquals(null, entity.jobNameL);
				 System.assertEquals(false, entity.isDirectChargedJob);

				 // Case-2 : jobIdの紐付けがある
				 entity.jobId = UserInfo.getUserId();// UserInfo.getUserId()はダミーコード
				 System.assertEquals(null, entity.jobCode);
				 System.assertEquals(null, entity.jobNameL.valueL0);
				 System.assertEquals(null, entity.jobNameL.valueL1);
				 System.assertEquals(null, entity.jobNameL.valueL2);
				 System.assertEquals(false, entity.isDirectChargedJob);

			 } catch (Exception e) {
				 TestUtil.fail('Unexpected Error occured ！' + e.getCause());
			 }
		 }
		// workCategoryIdで紐付くもの
		// workCategoryCode・workCategoryName
		 {
			 try {
				 TimeRecordItemEntity entity = new TimeRecordItemEntity();
				 // Case-1 : workCategoryIdの紐付けがない
				 System.assertEquals(null, entity.workCategoryCode);
				 System.assertEquals(null, entity.workCategoryNameL);
				 // Case-2 : workCategoryIdの紐付けがある
				 entity.workCategoryId = UserInfo.getUserId();// UserInfo.getUserId()はダミーコード
				 System.assertEquals(null, entity.workCategoryCode);
				 System.assertEquals(null, entity.workCategoryNameL.valueL0);
				 System.assertEquals(null, entity.workCategoryNameL.valueL1);
				 System.assertEquals(null, entity.workCategoryNameL.valueL2);
			 } catch (Exception e) {
				 TestUtil.fail('Unexpected Error occured ！' + e.getCause());
			 }
		 }
	 }


}