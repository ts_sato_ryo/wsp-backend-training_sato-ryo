/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Test class for ExtendedItemCustomService class
 *
 * @group Expense
 */
@IsTest
private class ExtendedItemCustomServiceTest {

	@TestSetup
	static void setup() {
		TestData.setupMaster();
	}

	@IsTest
	static void setExtendedItemCustomOptionsPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		Integer totalExtendedItemCustoms = 3;
		Integer totalExtendedItemCustomOptions = 8;
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, totalExtendedItemCustoms);
		List<ComExtendedItemCustomOption__c> customOptionList = ComTestDataUtility.createExtendedItemCustomOptions('EICO', customList, totalExtendedItemCustomOptions);

		List<ExtendedItemCustomEntity> responseCustomEntityList = new ExtendedItemCustomService()
				.getExtendedItemCustomEntityList(new ExtendedItemCustomRepository.SearchFilter(), true);

		for (Integer customIndex = 0; customIndex < totalExtendedItemCustoms; customIndex++) {
			ComExtendedItemCustom__c expectedCustom = customList.get(customIndex);
			ExtendedItemCustomEntity responseEntity = responseCustomEntityList.get(customIndex);

			System.assertEquals(expectedCustom.Id, responseEntity.id);
			System.assertEquals(expectedCustom.Name_L0__c, responseEntity.nameL0);
			System.assertEquals(expectedCustom.Name_L1__c, responseEntity.nameL1);
			System.assertEquals(expectedCustom.Name_L2__c, responseEntity.nameL2);
			System.assertEquals(expectedCustom.Code__c, responseEntity.code);
			System.assertEquals(expectedCustom.CompanyId__c, responseEntity.companyId);
			System.assertEquals(expectedCustom.UniqKey__c, responseEntity.uniqKey);

			for (Integer optionIndex = 0; optionIndex < totalExtendedItemCustomOptions; optionIndex++) {
				ComExtendedItemCustomOption__c expectedOption = customOptionList.get(optionIndex + (customIndex * totalExtendedItemCustomOptions));
				ExtendedItemCustomOptionEntity responseOptionEntity = responseEntity.extendedItemCustomOptionList.get(optionIndex);
				System.assertEquals(expectedOption.Id, responseOptionEntity.id);
				System.assertEquals(expectedOption.Code__c, responseOptionEntity.code);
				System.assertEquals(expectedOption.UniqKey__c, responseOptionEntity.uniqKey);
				System.assertEquals(expectedOption.Name_L0__c, responseOptionEntity.nameL0);
				System.assertEquals(expectedOption.Name_L1__c, responseOptionEntity.nameL1);
				System.assertEquals(expectedOption.Name_L2__c, responseOptionEntity.nameL2);
				System.assertEquals(expectedOption.ExtendedItemCustomId__c, responseOptionEntity.extendedItemCustomId);
			}
		}
	}
}