/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * Salesforceユーザのリポジトリ
 */
public with sharing class UserRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				User.Id,
				User.Name,
				User.UserName,
				User.Email,
				User.SmallPhotoUrl,
				User.LanguageLocaleKey,
				User.ProfileId};
	}

	/** プロファイルのリレーション名 */
	private static final String PROFILE_R = User.ProfileId.getDescribe().getRelationshipName();

	/** 検索フィルタ */
	private class SearchFilter {
		/** ユーザID */
		public List<Id> ids;
		/** ユーザ名（複数） */
		public List<String> userNames;
	}

	/** FOR UPDATE でクエリしない */
	private static final Boolean NOT_FOR_UPDATE = false;

	/**
	 * 指定したユーザIDのSalesforceユーザを取得する
	 * @param userId 取得対象のSalesforceユーザID
	 * @return ユーザ、ただし見つからなかった場合はnull
	 */
	public UserEntity getEntity(Id userId) {
		List<UserEntity> entities = getEntityList(new List<Id>{userId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したユーザIDのSalesforceユーザを取得する
	 * @param userIds 取得対象のユーザIDリスト、nullの場合は全件取得する
	 * @return ユーザリスト
	 */
	public List<UserEntity> getEntityList(List<Id> userIds) {
		UserRepository.SearchFilter filter = new SearchFilter();
		filter.ids = userIds;

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したユーザ名のSalesforceユーザを取得する
	 * @param userNames 取得対象のユーザ名リスト、nullの場合は全件取得する
	 * @return ユーザリスト
	 */
	public List<UserEntity> getEntityListByUserName(List<String> userNames) {
		UserRepository.SearchFilter filter = new SearchFilter();
		filter.userNames = userNames;

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した申請一覧
	 */
	private List<UserEntity> searchEntityList(
			UserRepository.SearchFilter filter, Boolean forUpdate) {

		// 取得対象項目
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		selectFldList.add(getFieldName(PROFILE_R, Profile.Name));

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> ids = filter.ids;
		final List<String> userNames = filter.userNames;
		if (ids != null) {
			condList.add(getFieldName(User.Id) + ' IN :ids');
		}
		if (userNames != null) {
			condList.add(getFieldName(User.UserName) + ' IN :userNames');
		}

		// Where句作成
		String whereClause;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereClause = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereClause = '';
		}

		String soql = 'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + User.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereClause)) {
			soql += ' WHERE ' + whereClause;
		}
		soql += ' ORDER BY Name';


		// クエリ実行
		System.debug('>>> UserRepository.searchEntityList: SOQL=' + soql);
		List<User> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<UserEntity> entityList = new List<UserEntity>();
		for (User sObj : sObjList) {
			entityList.add(createEntity(sObj));
		}

		return entityList;

	}

	/**
	 * UserオブジェクトからUserEntityを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private UserEntity createEntity(User sObj) {
		UserEntity entity = new UserEntity();
		entity.setId(sObj.Id);
		entity.name = sObj.Name;
		entity.userName = sObj.UserName;
		entity.email = sObj.Email;
		entity.smallPhotoUrl = sObj.SmallPhotoUrl;
		entity.language = sObj.LanguageLocaleKey;
		entity.profileId = sObj.ProfileId;
		entity.profile = new UserEntity.Profile();
		entity.profile.name = sObj.Profile.Name;
		// 変更情報をリセットしておく
		entity.resetChanged();

		return entity;
	}
}