/**
 * 勤務日の種別の定義
 */
public with sharing class AttDayType extends ValueObjectType {

	/** 勤務日 */
	public static final AttDayType WORKDAY = new AttDayType('Workday');
	/** 休日 */
	public static final AttDayType HOLIDAY = new AttDayType('Holiday');
	/** 法定優先休日 */
	public static final AttDayType PREFERRED_LEGAL_HOLIDAY = new AttDayType('PreferredLegalHoliday');
	/** 法定休日 */
	public static final AttDayType LEGAL_HOLIDAY = new AttDayType('LegalHoliday');

	/** 休暇種別のリスト（種別が追加されら本リストにも追加してください) */
	public static final List<AttDayType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttDayType> {
			WORKDAY,
			HOLIDAY,
			PREFERRED_LEGAL_HOLIDAY,
			LEGAL_HOLIDAY
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttDayType(String value) {
		super(value);
	}

	/**
	 * 値からAttDayTypeを取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttDayType、ただし該当するAttDayTypeが存在しない場合はnull
	 */
	public static AttDayType getType(String value) {
		AttDayType retType = null;
		if (String.isNotBlank(value)) {
			for (AttDayType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}
	
	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {
		
		// nullの場合はfalse
		if (compare == null) {
			return false;
		}
		
		// AttDayType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttDayType) {
			// 値が同じであればtrue
			if (this.value == ((AttDayType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}