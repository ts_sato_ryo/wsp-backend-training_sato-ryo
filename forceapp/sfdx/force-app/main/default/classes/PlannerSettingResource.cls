/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description プランナー設定の操作APIを実装するクラス
 */
public with sharing class PlannerSettingResource {

	/**
	 * @description 認証コード保存APIのリクエストクラス
	 */
	public class SaveOAuthCodeParam implements RemoteApi.RequestParam {

		/** 会社レコードID */
		public String companyId;
		/** 認証コード */
		public String authCode;
		/** ステート情報 */
		public String state;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 会社レコードIDの検証
			if (String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('companyId');
				throw e;
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch (StringException e) {
				throw new App.ParameterException('companyId', companyId);
			}
			// 認証コードの検証
			if (String.isBlank(this.authCode)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('authCode');
				throw e;
			}
			// ステート情報の検証
			if (String.isBlank(this.state)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('state');
				throw e;
			}
		}
	}

	public with sharing class SaveOAuthCodeApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final PlannerConfigResourcePermission.Permission requriedPermission =
				PlannerConfigResourcePermission.Permission.MANAGE_PLANNER_SETTING;

		/**
		 * @description 外部カレンダーの認証コードを保存する
		 * @param req リクエストパラメータ
		 * @return レスポンス（null）
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 入力値チェック
			SaveOAuthCodeParam param = (SaveOAuthCodeParam)req.getParam(SaveOAuthCodeParam.class);
			param.validate();

			// 権限チェック
			new PlannerConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 保存対象のレコードを検索
			CompanyPrivateSettingRepository repo = new CompanyPrivateSettingRepository();
			CompanyPrivateSettingEntity entity = repo.getEntity(param.companyId);
			if (entity == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}

			// エンティティ作成
			entity.office365AuthCode = param.authCode;

			// 保存
			PlannerSettingService service = new PlannerSettingService();
			service.saveOAuthCode(entity, param.state);

			// レスポンス作成
			return null;
		}
	}

	/**
	 * @description OAuth認証URLを取得するAPIのリクエストクラス
	 */
	public class GetOAuthUrlRequest implements RemoteApi.RequestParam {

		/** 会社レコードID */
		public String companyId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 会社レコードIDの検証
			if (String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('companyId');
				throw e;
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch (StringException e) {
				throw new App.ParameterException('companyId', this.companyId);
			}
		}
	}

	/**
	 * @description OAuth認証URLを取得するAPIのレスポンスクラス
	 */
	public class GetOAuthUrlResponse implements RemoteApi.ResponseParam {

		/** OAuth認証URL */
		public String authUrl;
	}

	/**
	 * OAuth認証URLを取得するAPI
	 */
	public with sharing class GetOAuthUrlApi extends RemoteApi.ResourceBase {

		/**
		 * @description OAuth認証URLを取得する
		 * @param req リクエストパラメータ(GetOAuthUrlRequest)
		 * @return レスポンスパラメータ(GetOAuthUrlResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 入力値チェック
			GetOAuthUrlRequest param = (GetOAuthUrlRequest)req.getParam(GetOAuthUrlRequest.class);
			param.validate();

			// OAuth認証URLを取得
			String authUrl = new PlannerSettingService().generateOAuthUrl(param.companyId);

			// レスポンス作成
			GetOAuthUrlResponse response = new GetOAuthUrlResponse();
			response.authUrl = authUrl;
			return response;
		}
	}

	/**
	 * @description プランナー機能設定保存APIのリクエストクラス
	 */
	public class SaveRequest implements RemoteApi.RequestParam {

		/** 会社レコードID */
		public String companyId;
		/** 外部カレンダー連携の有効無効フラグ */
		public Boolean useCalendarAccess;
		/** 連携対象の外部カレンダー */
		public String calendarAccessService;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 会社レコードIDの検証
			if (String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('companyId');
				throw e;
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch (StringException e) {
				throw new App.ParameterException('companyId', this.companyId);
			}

			// 外部カレンダー連携の有効無効フラグ
			if (this.useCalendarAccess == null) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('useCalendarAccess');
				throw e;
			}

			// 連携対象の外部カレンダーは外部カレンダー連携が有効な場合のみ必須
			if (this.useCalendarAccess == true) {
				if (String.isBlank(this.calendarAccessService)) {
					App.ParameterException e = new App.ParameterException();
					e.setMessageIsBlank('calendarAccessService');
					throw e;
				}
				if (CalendarAccessServiceName.valueOf(this.calendarAccessService) == null) {
					throw new App.ParameterException('calendarAccessService', this.calendarAccessService);
				}
			}
		}
	}

	/**
	 * プランナー機能設定保存API
	 */
	public with sharing class SaveApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final PlannerConfigResourcePermission.Permission requriedPermission =
				PlannerConfigResourcePermission.Permission.MANAGE_PLANNER_SETTING;

		/**
		 * @description プランナー機能設定を保存する
		 * @param req リクエストパラメータ(SaveRequest)
		 * @return レスポンス（null）
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 入力値チェック
			PlannerSettingResource.SaveRequest param = (SaveRequest)req.getParam(PlannerSettingResource.SaveRequest.class);
			param.validate();

			// 権限チェック
			new PlannerConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 保存する
			CompanyEntity company = createCompanyEntity(param);
			new PlannerSettingService().savePlannerSetting(company);

			// レスポンス作成
			return null;
		}

		/**
		 * リクエストパラメータから保存用の会社エンティティを作成する
		 * @param param 保存APIのリクエストパラメータ
		 * @return 会社エンティティ
		 */
		private CompanyEntity createCompanyEntity(SaveRequest param) {
			CompanyEntity company = new CompanyEntity();
			company.setId(param.companyId);
			company.useCalendarAccess = param.useCalendarAccess;
			company.calendarAccessService = CalendarAccessServiceName.valueOf(param.calendarAccessService);
			return company;
		}
	}

	/**
	 * @description プランナー機能設定取得APIのリクエストクラス
	 */
	public class GetRequest implements RemoteApi.RequestParam {

		/** 会社レコードID */
		public String companyId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 会社レコードIDの検証
			if (String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('companyId');
				throw e;
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch (StringException e) {
				throw new App.ParameterException('companyId', companyId);
			}
		}
	}

	/**
	 * @description プランナー機能設定取得APIのレスポンスクラス
	 */
	public class GetResponse implements RemoteApi.ResponseParam {

		/** 外部カレンダー連携の有効無効フラグ */
		public Boolean useCalendarAccess;
		/** 連携対象の外部カレンダー */
		public String calendarAccessService;
		/** 認証ステータス */
		public String authStatus;
	}

	/**
	 * プランナー機能設定取得API
	 */
	public with sharing class GetApi extends RemoteApi.NoRollbackResourceBase {

		/**
		 * @description プランナー機能設定を取得する
		 * @param req リクエストパラメータ(GetRequest)
		 * @return レスポンスパラメータ(GetRequest)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 入力値チェック
			GetRequest param = (GetRequest)req.getParam(GetRequest.class);
			param.validate();

			// 会社を取得する
			CompanyEntity company = new CompanyRepository().getEntity(param.companyId);
			if (company == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}

			// カレンダーが有効な場合、認証状態を返却する
			String authStatus;
			if (company.useCalendarAccess == true) {
				authStatus = new PlannerSettingService().checkAuth(param.companyId);
			}

			// レスポンス作成
			GetResponse response = new GetResponse();
			response.useCalendarAccess = company.useCalendarAccess;
			response.calendarAccessService = AppConverter.stringValue(company.calendarAccessService);
			response.authStatus = authStatus;

			return response;
		}
	}
}