/**
 * @group 工数
 *
 * 工数内訳スナップショットのエンティティ
 */
public with sharing class TimeRecordItemSnapshotEntity extends TimeRecordItemSnapshotGeneratedEntity {

	/**
	 * ジョブ階層情報の上限
	 * この値は工数内訳スナップショットで保存できる階層数であり、
	 * ジョブマスタに保存できる親子階層数の上限ではありません。
	 * Summer'19時点ではどちらも10階層ですが、今後異なる値になる可能性があります。
	 */
	private static final Integer JOB_LEVEL_LIMIT = 10;

	/** 
	 * 設定したジョブの階層数
	 * 1件もジョブを設定しない場合は「0」10階層設定した場合は「10」
	 * 検索した結果の階層数は保持しません
	 */
	@TestVisible
	private Integer jobCount = 0;

	/**
	 * コンストラクタ
	 */
	public TimeRecordItemSnapshotEntity() {
		super(new TimeRecordItemSnapshot__c());
	}

	/**
	 * コンストラクタ
	 */
	public TimeRecordItemSnapshotEntity(TimeRecordItemSnapshot__c sobj) {
		super(sobj);
	}
	
	/**
	 * 実行された回数に合わせて、ジョブの情報を設定する
	 * 階層数の上限である10階層を超過した場合は、エンティティに保存されない（エラーにはならない）
	 * @param job 保存対象のジョブ
	 */
	public void addJob(JobEntity job) {
		if (JOB_LEVEL_LIMIT <= jobCount) {
			return;
		}

		// 階層に合わせてジョブの情報を設定する
		Integer level = ++jobCount;
		List<Object> param = new List<Object>{getNameSpace(), level};
		setFieldValue(String.format('{0}JobIdLevel{1}__c', param), job.id);
		setFieldValue(String.format('{0}JobCodeLevel{1}__c', param), job.code);
		setFieldValue(String.format('{0}JobNameLevel{1}_L0__c', param), job.nameL0);
		setFieldValue(String.format('{0}JobNameLevel{1}_L1__c', param), job.nameL1);
		setFieldValue(String.format('{0}JobNameLevel{1}_L2__c', param), job.nameL2);
	}

	/**
	 * 複数のジョブの設定する
	 * @param jobs 保存対象のジョブ
	 */
	public void addJobList(List<JobEntity> jobs) {
		for (JobEntity job : jobs) {
			addJob(job);
		}
	}

	/**
	 * 作業分類を設定する
	 * @param workCategory
	 */
	public void setWorkCategory(WorkCategoryEntity workCategory) {
		workCategoryId = workCategory.id;
		workCategoryCode = workCategory.code;
		workCategoryNameL0 = workCategory.nameL0;
		workCategoryNameL1 = workCategory.nameL1;
		workCategoryNameL2 = workCategory.nameL2;
	}
}
