/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブタイプのサービスクラス
 */
public with sharing class JobTypeService implements Service {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** ジョブタイプと紐づけが可能な作業分類の上限数 */
	@TestVisible
	private static final Integer LIMIT_WORKCATEGORY_SIZE_EACH_JOBTYPE = 20;

	/** ジョブタイプのリポジトリ */
	private JobTypeRepository jobTypeRepo = new JobTypeRepository();

	/**
	 * ジョブタイプとジョブタイプに割り当てる作業分類を保存する
	 * @param jobType 保存するジョブタイプ
	 * @param workCategoryIdList ジョブタイプに紐付ける作業分類IDのリスト
	 * @return 保存したジョブタイプのID
	 */
	public Id saveJobTypeWithWorkCategoryList(
			JobTypeEntity jobType, List<Id> workCategoryIdList) {
		final Boolean isUpdate = jobType.id != null;

			// ジョブタイプのバリデーションを行う
			validateJobType(jobType);

			// エンティティにユニークキーを設定する
			setUniqKey(jobType);

			// ジョブタイプを保存する
			Repository.SaveResult saveResult = jobTypeRepo.saveEntity(jobType);

			// 更新の場合は登録済みのジョブタイプ別作業分類を削除する
			if (isUpdate) {
				deleteJobTypeWorkCategory(jobType.id);
			}

			// 作業分類をジョブタイプに紐付けて登録する
			Id savedJobTypeId = saveResult.details[0].id;
			registerJobTypeWorkCategory(savedJobTypeId, workCategoryIdList);

			return savedJobTypeId;
	}

	/**
	 * ジョブタイプのバリデーションを実行する
	 * @param jobType 検証対象のジョブタイプ
	 * @throws App.ParameterException 不正な値が設定されている場合
	 */
	private void validateJobType(JobTypeEntity jobType) {

		// ジョブタイプエンティティの検証
		Validator.Result validResult = new ComConfigValidator.JobTypeValidator(jobType).validate();
		if (!validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// コードが会社内で重複していないかを確認する
		JobTypeEntity duplicateJobType = jobTypeRepo.getEntityByCode(jobType.companyId, jobType.code);
		if (duplicateJobType != null) {
			if (duplicateJobType.id != jobType.id) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
			}
		}
	}

	/**
	 * ジョブタイプにユニークキーを設定する
	 * @param ユニークキーを設定するジョブタイプ
	 */
	private void setUniqKey(JobTypeEntity jobType) {
		CompanyEntity company = new CompanyRepository().getEntity(jobType.companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		jobType.uniqKey = jobType.createUniqKey(company.code);
	}

	/**
	 * 指定されたジョブタイプIDを持つジョブタイプ別作業分類を削除する
	 * @param jobTypeId 削除対象のジョブタイプID
	 */
	private void deleteJobTypeWorkCategory(Id jobTypeId) {
		JobTypeWorkCategoryRepository repo = new JobTypeWorkCategoryRepository();
		List<JobTypeWorkCategoryEntity> deleteEntity = repo.getEntityListByJobType(jobTypeId);
		if (! deleteEntity.isEmpty()) {
				repo.deleteEntityList(deleteEntity);
		}
	}

	/**
	 * ジョブタイプ別作業分類を登録する
	 * @param jobTypeId ジョブタイプID
	 * @param workCategoryIdList ジョブタイプに紐付ける作業分類IDのリスト
	 */
	private void registerJobTypeWorkCategory(Id jobTypeId, List<Id> workCategoryIdList) {

		// 作業分類リストがnullまたは空の場合は何もしない
		if (workCategoryIdList == null || workCategoryIdList.isEmpty()) {
			return;
		}

		// ジョブタイプ別作業分類リストを作成する
		List<JobTypeWorkCategoryEntity> entities = new List<JobTypeWorkCategoryEntity>();
		for (Id workCategoryId : workCategoryIdList) {
			JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryEntity();
			entity.jobTypeId = jobTypeId;
			entity.workCategoryId = workCategoryId;
			entities.add(entity);
		}

		// バリデーション
		validateJobTypeWorkCategory(entities);

		// 保存する
		new JobTypeWorkCategoryRepository().saveEntityList(entities);
	}

	/**
	 * ジョブタイプとジョブタイプに紐づく作業分類のセット
	 */
	public class JobTypeWithWorkCategory {
		public JobTypeEntity jobType;
		public List<JobTypeWorkCategoryEntity> workCategoryList;

		/** コンストラクタ */
		public JobTypeWithWorkCategory(JobTypeEntity jobType) {
			this.jobType = jobType;
			this.workCategoryList = new List<JobTypeWorkCategoryEntity>();
		}

		/** 作用分類を追加する */
		public void addWorkCategory(JobTypeWorkCategoryEntity workCategory) {
			this.workCategoryList.add(workCategory);
		}

	}

	/**
	 * ジョブタイプを検索し、ジョブタイプに紐づくジョブタイプ別作業分類と一緒に返却する
	 * @param companyId 会社ID、nullの場合は検索条件に含まない
	 * @param jobTypeId ジョブタイプID、nullの場合は検索条件に含まない
	 */
	public List<JobTypeWithWorkCategory> searchJobTypeListWithWorkCategory(Id companyId, Id jobTypeId) {

		// ジョブタイプを検索する
		JobTypeRepository.SearchFilter jobTypeFilter = new JobTypeRepository.SearchFilter();
		if (companyId != null) {
			jobTypeFilter.companyIds = new Set<Id>{companyId};
		}
		if (jobTypeId != null) {
			jobTypeFilter.ids = new Set<Id>{jobTypeId};
		}
		List<JobTypeEntity> jobTypeList = new JobTypeRepository().searchEntityList(jobTypeFilter);

		// ジョブタイプに紐づくジョブタイプ別作業分類を検索する
		Set<Id> jobTypeIdSet = new Set<Id>();
		for (JobTypeEntity jobType : jobTypeList) {
			jobTypeIdSet.add(jobType.id);
		}
		JobTypeWorkCategoryRepository.SearchFilter jobTypeWorkCategoryFilter
				= new JobTypeWorkCategoryRepository.SearchFilter();
		jobTypeWorkCategoryFilter.jobTypeIds = jobTypeIdSet;
		List<JobTypeWorkCategoryEntity> jobTypeWorkCategoryList =
				new JobTypeWorkCategoryRepository().searchEntityList(jobTypeWorkCategoryFilter);

		// 結果を作成する
		List<JobTypeWithWorkCategory> jobTypeWithWorkCategoryList = new List<JobTypeWithWorkCategory>();
		Map<Id, JobTypeWithWorkCategory> jobTypeWithWorkCategoryMap = new Map<Id, JobTypeWithWorkCategory>();
		for (JobTypeEntity jobType : jobTypeList) {
			JobTypeWithWorkCategory jw = new JobTypeWithWorkCategory(jobType);
			jobTypeWithWorkCategoryList.add(jw);
			jobTypeWithWorkCategoryMap.put(jobType.id, jw);
		}

		for (JobTypeWorkCategoryEntity jobTypeWorkCategory : jobTypeWorkCategoryList) {
			JobTypeWithWorkCategory jw = jobTypeWithWorkCategoryMap.get(jobTypeWorkCategory.jobTypeId);
			if (jw != null) {
				jw.addWorkCategory(jobTypeWorkCategory);
			}
		}

		return jobTypeWithWorkCategoryList;
	}

	/**
	 * ジョブタイプを削除する。
	 * NOTE:論理削除型のマスタは、レコードが参照されている場合は論理削除する方針だが、検討が十分ではないため現段階では実装せず、一律物理削除とする。
	 * @param entity 削除対象のエンティティ
	 * @throw DmlException 参照しているレコードが存在する場合、削除対象が既に削除されている場合(System.StatusCode.ENTITY_IS_DELETED)
	 */
	public void deleteJobType(Id jobTypeId) {
		new LogicalDeleteService(jobTypeRepo).deleteEntity(jobTypeId);
	}

	/**
	 * ジョブタイプ別作業分類のバリデーションを実行する
	 * @param jobTypeWorkCategoryList 検証対象のジョブタイプ別作業分類のリスト
	 * @throws App.ParameterException 検証対象のリストが作業分類の上限数を超過している場合
	 */
	@TestVisible
	private void validateJobTypeWorkCategory(List<JobTypeWorkCategoryEntity> jobTypeWorkCategoryList) {
		// 作業分類の上限を超過した場合はエラー
		if (LIMIT_WORKCATEGORY_SIZE_EACH_JOBTYPE < jobTypeWorkCategoryList.size()) {
			App.ParameterException e = new App.ParameterException();
			e.setMessage(MESSAGE.Time_Err_OverWorkCategoryLimitSize(new List<String>{String.valueOf(LIMIT_WORKCATEGORY_SIZE_EACH_JOBTYPE)}));
			throw e;
		}
	}
}