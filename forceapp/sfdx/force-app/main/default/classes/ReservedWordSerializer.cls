/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Serialize reserved word with suffix `_x`
 * https://salesforce.stackexchange.com/questions/2276/how-do-you-deserialize-json-properties-that-are-reserved-words-in-apex
 *
 * @group 経費 Expense
 */
public with sharing class ReservedWordSerializer {

	//true for pretty printing
	JsonGenerator g = Json.createGenerator(true);

	/**
	 * constructor
	 */
	public ReservedWordSerializer(Object obj) {
		if (obj == null) {
			g.writeNull();
		} else if (obj instanceof Map<String, Object>) {
			traverseMap((Map<String, Object>)obj);
		} else if (obj instanceof List<Object>) {
			traverseList((List<Object>)obj);
		} else {
			g.writeObject(obj);
		}
	}

	public String getAsString() {
		return g.getAsString();
	}

	/**
	 * @description replace key with _x
	 * @param obj: object
	 */
	private void traverseMap(Map<String, Object> obj) {
		List<String> keys = new List<String>(obj.keySet());
		keys.sort();

		g.writeStartObject();
		for (String key : keys) {
			Object value = obj.get(key);
			g.writeFieldName(key + '_x');

			if (value == null) {
				g.writeNull();
			} else if (value instanceof Map<String, Object>) {
				traverseMap((Map<String, Object>)value);
			} else if (value instanceof List<Object>) {
				traverseList((List<Object>)value);
			} else {
				g.writeObject(value);
			}
		}
		g.writeEndObject();
	}

	/**
	 * @description traverse
	 * @param obj: object
	 */
	private void traverseList(List<Object> objs) {
		g.writeStartArray();
		for (Object obj : objs) {
			if (obj == null) {
				g.writeNull();
			} else if (obj instanceof Map<String, Object>) {
				traverseMap((Map<String, Object>)obj);
			} else if (obj instanceof List<Object>) {
				traverseList((List<Object>)obj);
			} else {
				g.writeObject(obj);
			}
		}
		g.writeEndArray();
	}

}