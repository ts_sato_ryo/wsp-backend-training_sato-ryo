/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 共通
  *
  * 国マスタのエンティティ
  */
public with sharing class CountryEntity extends Entity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 国名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		CODE
	}

	/** 値を変更した項目のリスト */
	private Set<Field> changedFieldSet;

	/**
	 * コンストラクタ
	 */
	public CountryEntity() {
		changedFieldSet = new Set<Field>();
	}

	/** 国名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** 国名(多言語対応。参照専用) */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	/** 国名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	/** 国名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	/** 国名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/** コード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		changedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return changedFieldSet.contains(field);
	}
}