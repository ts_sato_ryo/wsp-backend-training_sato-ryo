/**
 * リポジトリの基底クラス
 * データベースへのアクセスは基本的に本クラスを継承したクラスからアクセスするようにしてください。
 */
public class Repository  {

	/**
	 * リポジトリの処理結果
	 */
	public virtual class BaseResult {
		/** すべてのエンティティに対する処理が成功した場合はtrue, そうでない場合はfalse */
		public Boolean isSuccessAll{get; set;}
		/** 処理結果の詳細 */
		public List<ResultDetail> details{get; set;}
	}

	/**
	 * 処理結果の詳細
	 */
	public class ResultDetail {
		/** 保存処理が成功したかどうか */
		public Boolean isSuccess{get; set;}
		/** 処理を実行したエンティティのID */
		public Id id{get; set;}
		/** 失敗した場合のエラーメッセージ */
		public String errorMessage{get; set;}

		/**
		 * コンストラクタ
		 */
		public ResultDetail(Boolean isSuccess, Id id, String errorMessage) {
			this.isSuccess = isSuccess;
			this.id = id;
			this.errorMessage = errorMessage;
		}
	}

	/**
	 * リポジトリへの保存結果
	 */
	public virtual class SaveResult extends BaseResult {
	}

	/**
	 * リポジトリへの保存結果（子レコードの保存結果も含む)
	 */
	public class SaveResultWithChildren extends Repository.SaveResult {
		/** 履歴の処理結果の詳細 */
		public List<ResultDetail> recordDetails{get; set;}
	}

	/**
	 * リポジトリからの削除結果
	 */
	public virtual class DeleteResult extends BaseResult {
	}

	/**
	 * リポジトリの処理結果を作成するためのクラス
	 */
	public class ResultFactory {

		/**
		 * Database.DeleteResultからRepository.DeleteResultを作成する
		 * @param dbRes DB実行結果
		 * @return 作成したRepository.DeleteResult
		 */
		public Repository.DeleteResult createDeleteResult(Database.DeleteResult dbRes) {
			Repository.DeleteResult repoRes = new Repository.DeleteResult();
			repoRes.details = new List<Repository.ResultDetail>();
			repoRes.details.add(
					new Repository.ResultDetail(dbRes.isSuccess(), dbRes.getId(), String.join(dbRes.getErrors(), ' ')));
			repoRes.isSuccessAll = dbRes.isSuccess();
			return repoRes;
		}

		/**
		 * Database.DeleteResultからRepository.DeleteResultを作成する
		 * @param dbResList DB実行結果のリスト
		 * @return 作成したRepository.DeleteResult
		 */
		public Repository.DeleteResult createDeleteResult(List<Database.DeleteResult> dbResList) {
			Repository.DeleteResult repoRes = new Repository.DeleteResult();
			repoRes.details = new List<Repository.ResultDetail>();
			Integer errCount = 0;
			for (Database.DeleteResult dbRes : dbResList) {
				repoRes.details.add(
						new Repository.ResultDetail(
						dbRes.isSuccess(), dbRes.getId(), String.join(dbRes.getErrors(), ' ')));
				if ( ! dbRes.isSuccess()) {
					errCount ++;
				}
			}
			repoRes.isSuccessAll = errCount == 0 ? true : false;
			return repoRes;
		}

		/**
		 * Database.UpsertResultからRepository.SaveResultを作成する
		 * @param dbResList DB実行結果のリスト
		 * @return 作成したRepository.DeleteResult
		 */
		public Repository.SaveResult createSaveResult(List<Database.UpsertResult> dbResList) {
			Repository.SaveResult repoRes = new Repository.SaveResult();
			repoRes.details = new List<Repository.ResultDetail>();
			Integer errCount = 0;
			for (Database.UpsertResult dbRes : dbResList) {
				repoRes.details.add(
						new Repository.ResultDetail(
						dbRes.isSuccess(), dbRes.getId(), String.join(dbRes.getErrors(), ' ')));
				if ( ! dbRes.isSuccess()) {
					errCount ++;
				}
			}
			repoRes.isSuccessAll = errCount == 0 ? true : false;
			return repoRes;
		}

		/**
		 * Database.SaveResultからRepository.SaveResultを作成する
		 * @param dbResList DB実行結果のリスト
		 * @return 作成したRepository.SaveResult
		 */
		public Repository.SaveResult createSaveResult(List<Database.SaveResult> dbResList) {
			Repository.SaveResult repoRes = new Repository.SaveResult();
			repoRes.details = new List<Repository.ResultDetail>();
			Integer errCount = 0;
			for (Database.SaveResult dbRes : dbResList) {
				repoRes.details.add(
						new Repository.ResultDetail(
						dbRes.isSuccess(), dbRes.getId(), String.join(dbRes.getErrors(), ' ')));
				if ( ! dbRes.isSuccess()) {
					errCount ++;
				}
			}
			repoRes.isSuccessAll = errCount == 0 ? true : false;
			return repoRes;
		}
	}

	/**
	 * リポジトリの基底クラス
	 */
	public abstract class BaseRepository {

		protected Repository.ResultFactory resultFactory = new Repository.ResultFactory();

		/**
		 * 項目のAPI名を取得する
		 */
		@TestVisible
		protected virtual String getFieldName(Schema.SObjectField field) {
			return field.getDescribe().getName();
		}

		/**
		 * SELECT句で使用する項目のAPI名を取得する(リレーション名付き)
		 * @param relationshipName リレーションシップ名
		 * @param field 項目
		 * @return API名
		 */
		protected virtual String getFieldName(String relationshipName, Schema.SObjectField field) {
			return relationshipName + '.' + field.getDescribe().getName();
		}

		/**
		 * SELECT句で使用する項目のAPI名を取得する(リレーション名付き)
		 * @param relationshipName1 リレーションシップ名
		 * @param relationshipName2 リレーションシップ名
		 * @param field 項目
		 * @return API名
		 */
		protected virtual String getFieldName(String relationshipName1, String relationshipName2, Schema.SObjectField field) {
			return relationshipName1 + '.' + relationshipName2 + '.' + field.getDescribe().getName();
		}

		/**
		 * SELECT句で使用する項目のAPI名を取得する(リレーション名付き)
		 * @param relationshipName1 リレーションシップ名
		 * @param relationshipName2 リレーションシップ名
		 * @param relationshipName3 リレーションシップ名
		 * @param field 項目
		 * @return API名
		 */
		protected virtual String getFieldName(String relationshipName1, String relationshipName2,
				String relationshipName3, Schema.SObjectField field) {
			return relationshipName1 + '.' + relationshipName2 + '.' + relationshipName3 + '.' + field.getDescribe().getName();
		}

		/**
		 * SELECT句で使用する項目のAPI名を取得する(リレーション名付き)
		 * @param relationshipName1 リレーションシップ名
		 * @param relationshipName2 リレーションシップ名
		 * @param relationshipName3 リレーションシップ名
		 * @param relationshipName4 リレーションシップ名
		 * @param field 項目
		 * @return API名
		 */
		protected virtual String getFieldName(String relationshipName1, String relationshipName2,
				String relationshipName3, String relationshipName4, Schema.SObjectField field) {
			return relationshipName1 + '.' + relationshipName2 + '.' + relationshipName3 + '.' + relationshipName4 + '.' + field.getDescribe().getName();
		}

		/**
		 * whereListをANDで連結してWHERE句を作成する(WHERE whereList[0] AND whereList[1] ...)
		 * @param whereList 条件
		 * @return WHERE句、ただしwhereListが空の場合は空文字
		 */
		protected String buildWhereString(List<String> whereList) {
			String whereString = '';
			if(! whereList.isEmpty()){
				whereString = ' WHERE (' + String.join(whereList, ') AND (') + ')';
			}
			return whereString;
		}

		/**
		 * WHERE句の条件式を作成する(IN)
		 * @param field 項目
		 * @param bindName バインドする変数名
		 * @return 条件式
		 */
		protected String createInExpression(Schema.SObjectField field, String bindName) {
			return getFieldName(field) + ' IN :' + bindName;
		}

		/**
		 * WHERE句の条件式を作成する(=)
		 * @param field 項目
		 * @param value 比較値
		 * @return 条件式
		 */
		protected String createEqExpression(Schema.SObjectField field, String value) {
			return getFieldName(field) + ' = ' + String.escapeSingleQuotes(value);
		}

		/**
		 * WHERE句の条件式を作成する(=)
		 * @param field 項目
		 * @param value 比較値
		 * @return 条件式
		 */
		protected String createEqExpression(Schema.SObjectField field, Boolean value) {
			return getFieldName(field) + ' = ' + value;
		}
		/**
		 * エンティティを削除する
		 * @param entity 削除対象のエンティティ
		 * @return 削除結果
		 */
		public virtual Repository.DeleteResult deleteEntity(Entity entity) {
			if (entity == null) {
				// TODO エラーメッセージ
				throw new App.ParameterException('entityがnullです');
			}
			return resultFactory.createDeleteResult(AppDatabase.doDelete(entity.id));
		}

		/**
		 * エンティティを削除する
		 * TODO 自動生成したエンティティに対応したリポジトリを実装後、削除する
		 * @param entity 削除対象のエンティティ
		 * @return 削除結果
		 */
		public virtual Repository.DeleteResult deleteEntity(BaseEntity entity) {
			return deleteEntity(entity.id);
		}

		/**
		 * エンティティを削除する
		 * @param entityList 削除対象のエンティティリスト
		 * @return 削除結果
		 */
		public virtual DeleteResult deleteEntityList(List<Entity> entityList) {
			if (entityList == null) {
				// TODO エラーメッセージ
				throw new App.ParameterException('entityListがnullです');
			}
			List<Id> idList = new List<Id>();
			for (Entity entity : entityList) {
				idList.add(entity.id);
			}

			return resultFactory.createDeleteResult(AppDatabase.doDelete(idList));
		}

		/**
		 * エンティティを削除する
		 * TODO 自動生成したエンティティに対応したリポジトリを実装後、削除する
		 * @param entityList 削除対象のエンティティリスト
		 * @return 削除結果
		 */
		public virtual DeleteResult deleteEntityList(List<BaseEntity> entityList) {
			if (entityList == null) {
				// TODO エラーメッセージ
				throw new App.ParameterException('entityListがnullです');
			}
			List<Id> idList = new List<Id>();
			for (BaseEntity entity : entityList) {
				idList.add(entity.id);
			}

			return resultFactory.createDeleteResult(AppDatabase.doDelete(idList));
		}

		/**
		 * エンティティを削除する
		 * @param id 削除対象のID
		 */
		public virtual Repository.DeleteResult deleteEntity(Id id) {
			return resultFactory.createDeleteResult(AppDatabase.doDelete(id));
		}
	}


	//---------------------------------------------------
	// 以下はリポジトリ共通で使用するメソッドの定義
	//---------------------------------------------------

	/**
	 * 項目名セットを生成する
	 * @param fieldSet 項目のセット
	 * @return 項目名のリスト
	 */
	public static List<String> generateFieldNameList(Set<Schema.SObjectField> fieldSet) {
		List<String> fieldNameList = new List<String>();
		for (Schema.SObjectField fld : fieldSet) {
			fieldNameList.add(fld.getDescribe().getName());
		}
		return fieldNameList;
	}

	/**
	 * 項目名セットを生成する
	 * @param relationName リレーション名
	 * @param fieldSet 項目のセット
	 * @return 項目名のリスト
	 */
	public static List<String> generateFieldNameList(String relationName, Set<Schema.SObjectField> fieldSet) {
		List<String> fieldNameList = new List<String>();
		for (Schema.SObjectField fld : fieldSet) {
			fieldNameList.add(relationName + '.' + fld.getDescribe().getName());
		}
		return fieldNameList;
	}

	/**
	 * SObjectの検索結果をキャッシュするクラス
	 */
	public class Cache {

		/** キャッシュ情報 */
		private final Map<CacheKey, List<SObject>> caches = new Map<CacheKey, List<SObject>>();

		/**
		 * keyに紐付くキャッシュが存在するか判定する
		 * @param key キャッシュキー
		 * @return キャッシュが存在する場合true
		 */
		public Boolean hasCache(CacheKey key) {
			return caches.containsKey(key);
		}

		/**
		 * keyに紐付くキャッシュを取得する
		 * @param key キャッシュキー
		 * @return キャッシュしたSObjectのリスト（キャッシュが存在しない場合はnull）
		 */
		public List<SObject> get(CacheKey key) {
			return caches.get(key);
		}

		/**
		 * キャッシュキーを指定して、キャッシュを保存する
		 * @param key キャッシュキー
		 * @param sobj 保存対象
		 */
		public void put(CacheKey key, SObject sobj) {
			caches.put(key, new List<SObject>{sobj});
		}

		/**
		 * キャッシュキーを指定して、キャッシュを保存する
		 * @param key キャッシュキー
		 * @param sobjs 保存対象
		 */
		public void put(CacheKey key, List<SObject> sobjs) {
			caches.put(key, sobjs);
		}

		/**
		 * 指定したキャッシュを削除する
		 * @param key 削除対象のキャッシュキー
		 */
		public void clear(CacheKey key) {
			caches.remove(key);
		}

		/**
		 * 全てのキャッシュをクリアする
		 */
		public void clearAll() {
			caches.clear();
		}
	}

	/**
	 * キャッシュのキー情報を管理するクラス
	 */
	public class CacheKey {

		/** キー情報のJson文字列 */
		private String value;
		/** キー情報のハッシュ値 */
		private Integer hashCode;

		/**
		 * 単一のオブジェクトからキー情報を作成する
		 */
		public CacheKey(Object value) {
			this(new List<Object>{value});
		}
		/**
		 * 複数のオブジェクトからキー情報を作成する
		 */
		public CacheKey(Object value1, Object value2) {
			this(new List<Object>{value1, value2});
		}

		/**
		 * 複数のオブジェクトからキー情報を作成する
		 * @param values
		 */
		public CacheKey(List<Object> values) {
			this.value = Json.serialize(values);
			this.hashCode = this.value.hashCode();
		}

		/**
		 * オブジェクトを比較する
		 * @param compare
		 * @return オブジェクトが等しい場合true
		 */
		public Boolean equals(Object compare) {
			if (compare == null) {
				return false;
			}
			if (! (compare instanceof CacheKey)) {
				return false;
			}
			return this.value == ((CacheKey)compare).value;
		}

		/**
		 * オブジェクトのハッシュ値を取得する
		 * @return ハッシュ値
		 */
		public Integer hashCode() {
			return this.hashCode;
		}
	}
}