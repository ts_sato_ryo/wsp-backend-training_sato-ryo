/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * AttConfigResourcePermissionクラスのテスト
 */
@isTest
private class AttConfigResourcePermissionTest {

	/** 権限リポジトリ */
	private static PermissionRepository permissionRepo = new PermissionRepository();

	/** テストデータクラス */
	private class TestData extends TestData.TestDataEntity {
		private PermissionRepository permissionRepo = new PermissionRepository();

		public TestData() {
			super();
			// 明示的に社員データを作成しておく
			this.employee = this.employee;
		}
	}

	/**
	 * 未実装の権限が指定された場合、App.UnsupportedExceptionが発生することを確認する
	 */
	@isTest static void hasExecutePermissionTestInvalidPermission() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.UnsupportedException actEx;
			AttConfigResourcePermission.Permission requiredPermission = null;

			actEx = null;
			testData.permission.isManageTimeWorkCategory = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.UnsupportedException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
		}
	}

	/**
	 * 休暇の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttLeaveTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttLeave = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttLeave = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 短時間勤務設定の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttShortTimeWorkSettingTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttShortTimeWorkSetting = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttShortTimeWorkSetting = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 休職・休業の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttLeaveOfAbsenceTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttLeaveOfAbsence = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttLeaveOfAbsence = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務体系の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttWorkingTypeTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_WORKING_TYPE;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttWorkingType = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttWorkingType = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務パターンの管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttPatternTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_PATTERN;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttPattern = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttPattern = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 残業警告設定の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttAgreementAlertSettingTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_AGREEMENT_ALERT_SETTING;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttAgreementAlertSetting = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttAgreementAlertSetting = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 *  休暇管理の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttLeaveGrantTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_GRANT;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttLeaveGrant = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttLeaveGrant = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 短時間勤務設定適用の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttShortTimeWorkSettingApplyTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttShortTimeWorkSettingApply = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttShortTimeWorkSettingApply = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 休職・休業適用の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttLeaveOfAbsenceApplyTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttLeaveOfAbsenceApply = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttLeaveOfAbsenceApply = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務パターン適用の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageAttPatternApplyTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			AttConfigResourcePermission.Permission requiredPermission
					= AttConfigResourcePermission.Permission.MANAGE_ATT_PATTERN_APPLY;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageAttPatternApply = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageAttPatternApply = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new AttConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}
}