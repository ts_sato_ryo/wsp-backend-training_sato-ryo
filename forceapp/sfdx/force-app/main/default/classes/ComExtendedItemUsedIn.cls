/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 拡張項目使用
 */
public with sharing class ComExtendedItemUsedIn extends ValueObjectType {
	
	/** 経費精算のみ */
	public static final ComExtendedItemUsedIn EXPENSE_REPORT = new ComExtendedItemUsedIn('ExpenseReport');
	/** 事前申請と経費精算 */
	public static final ComExtendedItemUsedIn EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ComExtendedItemUsedIn('ExpenseRequestAndExpenseReport');
	
	/** 拡張項目使用のリスト（明細タイプが追加されら本リストにも追加してください) */
	public static final List<ComExtendedItemUsedIn> TYPE_LIST;
	static {
		TYPE_LIST = new List<ComExtendedItemUsedIn> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}
	
	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ComExtendedItemUsedIn(String value) {
		super(value);
	}
	
	/**
	 * 値からComExtendedItemUsedInを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つComExtendedItemUsedIn、ただし該当するComExtendedItemUsedInが存在しない場合はnull
	 */
	public static ComExtendedItemUsedIn valueOf(String value) {
		ComExtendedItemUsedIn retType = null;
		if (String.isNotBlank(value)) {
			for (ComExtendedItemUsedIn type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/*
	 * Get the String value for the ComExtendedItemUsedIn
	 * @param usedIn ComExtendedItemUsedIn
	 * @return null if usedIn is null. Otherwise, return the String value.
	 */
	public static String getValue(ComExtendedItemUsedIn usedIn) {
		return usedIn == null ? null : usedIn.value;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ComExtendedItemUsedIn以外の場合はfalse
		Boolean eq;
		if (compare instanceof ComExtendedItemUsedIn) {
			// 値が同じであればtrue
			if (this.value == ((ComExtendedItemUsedIn)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}