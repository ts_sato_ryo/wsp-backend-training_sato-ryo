/**
 * @group 承認
 *
 * @description ProcessInstanceWorkitemのリポジトリ
 * 承認プロセスは単体テストからは利用できないため単体テストは実装しません。
 */
public with sharing class ApprovalProcessWorkitemRepository extends Repository.BaseRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_LIMIT_MAX = 10000;

	/** ProcessInstanceWorkitemの取得対象項目 */
	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		/** ProcessInstanceIdの取得対象項目の定義 */
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ProcessInstanceWorkitem.Id,
				ProcessInstanceWorkitem.ActorId,
				ProcessInstanceWorkitem.ProcessInstanceId};
		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** ActorIdのリレーション名 */
	private static final String Actor_R = ProcessInstanceWorkitem.ActorId.getDescribe().getRelationshipName();
	/** Actorの取得対象項目 */
	private static final List<String> GET_ACTOR_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				User.Name};
		GET_ACTOR_FIELD_NAME_LIST = Repository.generateFieldNameList(Actor_R, fieldList);
	}

	/** ProcessInstanceIdのリレーション名 */
	private static final String PROCESS_INSTANCE_R =
			ProcessInstanceWorkitem.ProcessInstanceId.getDescribe().getRelationshipName();
	/** ProcessInstanceIdの取得対象項目 */
	private static final List<String> GET_PROCESS_INSTANCE_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ProcessInstance.TargetObjectId};
		GET_PROCESS_INSTANCE_FIELD_NAME_LIST = Repository.generateFieldNameList(PROCESS_INSTANCE_R, fieldList);
	}

	/**
	 * @description 指定した申請の保留中になっている承認プロセスを取得する
	 * @param filter 検索条件
	 * @return 承認プロセス情報
	 */
	public List<ApprovalProcessWorkitemEntity> getEntityListByRequestId(List<Id> requestIdList) {
		SearchFilter filter = new SearchFilter();
		filter.requestIdList = requestIdList;
		return searchEntity(filter);
	}

	/** 検索フィルタ */
	private class SearchFilter {
		/** 申請ID */
		public List<Id> requestIdList;
	}

	/**
	 * @description 保留中(Pending)になっている承認プロセスを検索する
	 * @param filter 検索条件
	 * @return 条件に一致した承認申請情報
	 */
	private List<ApprovalProcessWorkitemEntity> searchEntity(SearchFilter filter) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		selectFldList.addAll(GET_FIELD_NAME_LIST);
		selectFldList.addAll(GET_ACTOR_FIELD_NAME_LIST);
		selectFldList.addAll(GET_PROCESS_INSTANCE_FIELD_NAME_LIST);

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<String> requestIdList = filter.requestIdList;
		if (requestIdList != null) {
			condList.add(getFieldName(PROCESS_INSTANCE_R, ProcessInstance.TargetObjectId) + ' = :requestIdList');
		}
		String whereStatement = buildWhereString(condList);

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ProcessInstanceWorkitem.SObjectType.getDescribe().getName()
				+ whereStatement
				+ ' ORDER BY ' + getFieldName(PROCESS_INSTANCE_R, ProcessInstance.CreatedDate) + ' Asc'
				+ ' LIMIT :SEARCH_RECORDS_LIMIT_MAX';

		// クエリ実行
		System.debug('ApprovalProcessWorkitemRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<ApprovalProcessWorkitemEntity> entityList = new List<ApprovalProcessWorkitemEntity>();
		for (SObject sObj : sObjList) {
			entityList.add(new ApprovalProcessWorkitemEntity((ProcessInstanceWorkitem)sObj));
		}

		return entityList;
	}
}