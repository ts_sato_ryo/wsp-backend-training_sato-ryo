/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署サービス
 */
public with sharing class DepartmentService extends ParentChildService {
	/** 部署階層最大数 */
	private static final Integer MAX_LEVEL = 10;
	/** 部署リポジトリ */
	private DepartmentRepository deptRepo = new DepartmentRepository();

	/**
	 * コンストラクタ
	 */
	public DepartmentService() {
		super(new DepartmentRepository());
	}

	/**
	 * 指定した部署が子部署を持っているかどうかをチェックする
	 * @param チェック対象の部署ベースID
	 * @param targetDate targetDate時点での子部署の存在有無をチェックする
	 * @return 子部署が存在する場合はtrue
	 */
	public Boolean hasChildren(Id baseId, AppDate targetDate) {
		Map<Id, Boolean> hasChildrenMap = hasChildren(new List<Id>{baseId}, targetDate);
		return hasChildrenMap.get(baseId);
	}

	/**
	 * 指定した部署が子部署を持っているかどうかをチェックする
	 * @param チェック対象の部署ベースID
	 * @param targetDate targetDate時点での子部署の存在有無をチェックする
	 * @return baseIdごとの子部署有無情報。子部署が存在する場合はtrue
	 */
	public Map<Id, Boolean> hasChildren(List<Id> baseIds, AppDate targetDate) {

		// 初期化
		final Boolean hasNotChildren = false;
		Map<Id, Boolean> hasChildrenMap = new Map<Id, Boolean>();
		for (Id baseId : baseIds) {
			hasChildrenMap.put(baseId, hasNotChildren);
		}

		// 履歴オブジェクトが存在すれば子部署あり
		final Boolean hasChildren = true;
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.parentIds = baseIds;
		filter.targetDate = targetDate;
		List<DepartmentBaseEntity> baseList = deptRepo.searchEntityWithHistory(
				filter, false, DepartmentRepository.HistorySortOrder.VALID_FROM_ASC);
		for (DepartmentBaseEntity base : baseList) {
			for (DepartmentHistoryEntity history : base.getHistoryList()) {
				hasChildrenMap.put(history.parentBaseId, hasChildren);
			}
		}

		return hasChildrenMap;
	}

	/**
	 * 指定した日に有効な部署の履歴をMapで取得する。キーは部署のベースID
	 * 親部署の取得に使用する
	 */
	public Map<Id, DepartmentHistoryEntity> getHistoryMap(List<Id> baseIds, AppDate targetDate) {
		List<DepartmentBaseEntity> bases = deptRepo.getEntityList(baseIds, targetDate);
		Map<Id, DepartmentHistoryEntity> historyMap = new Map<Id, DepartmentHistoryEntity>();
		for (DepartmentBaseEntity base : bases) {
			List<DepartmentHistoryEntity> histories = base.getHistoryList();
			if ((histories != null) && (! histories.isEmpty())) {
				historyMap.put(base.id, histories[0]);
			}
		}
		return historyMap;
	}

	/**
	 * 全ての履歴を持った部署をMapで取得する。キーは部署のベースID
	 * 親部署の取得に使用する。ベースにひもづく全ての履歴を返却する
	 * @param baseIds 取得対象のベースID
	 */
	public Map<Id, DepartmentBaseEntity> getBaseMap(List<Id> baseIds) {

		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAll = null;
		List<DepartmentBaseEntity> bases = deptRepo.getEntityList(baseIds, targetAll);

		// 検索しやすいようにMapに変換(キーはbaseId)
		Map<Id, DepartmentBaseEntity> baseMap = new Map<Id, DepartmentBaseEntity>();
		for (DepartmentBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		return baseMap;
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntity(Id baseId) {
		// ベースに紐づく全ての履歴を取得する
		return deptRepo.getEntity(baseId);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildBaseEntity> getEntityList(List<Id> baseIds) {
		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAllDate = null; // 全ての履歴が対象
		return deptRepo.getEntityList(baseIds, targetAllDate);
	}

	/**
	 * TODO 廃止予定 マスタは会社コード+マスタコードで一意となるため
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntityByCode(String code) {
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.codes = new List<String>{code};
		List<DepartmentBaseEntity> entityList = deptRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param codes 取得対象の部署コード
	 * @return ベースエンティティ
	 */
	public List<ParentChildBaseEntity> getEntityListByCode(List<String> codes) {
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.codes = codes;
		return deptRepo.searchEntity(filter);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildHistoryEntity getHistoryEntity(Id historyId) {
		// ベースに紐づく全ての履歴を取得する
		return deptRepo.getHistoryEntity(historyId);
	}

	/**
	 * 指定した履歴IDの履歴エンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		// ベースに紐づく全ての履歴を取得する
		return deptRepo.getHistoryEntityList(historyIds);
	}

	/**
	 * ベースエンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveBaseEntity(ParentChildBaseEntity base) {
		return (new ComConfigValidator.DepartmentBaseValidator((DepartmentBaseEntity)base)).validate();
	}

	/**
	 * 履歴エンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveHistoryEntity(ParentChildBaseEntity base, ParentChildHistoryEntity history) {
		return (new ComConfigValidator.DepartmentHistoryValidator((DepartmentBaseEntity)base, (DepartmentHistoryEntity)history)).validate();
	}

	/**
	 * 履歴エンティティが持つnameL0を取得する
	 * @param history 取得対象の履歴エンティティ
	 * @return 履歴エンティティのnameL0、ただしnameL0を持っていない場合はnull
	 */
	protected override String getNameL0FromHistory(ParentChildHistoryEntity history) {
		return ((DepartmentHistoryEntity)history).nameL0;
	}

	/**
	 * エンティティのコードが重複しているかどうかを確認する(会社単位での重複チェック)
	 * @param base チェック対象のベースエンティティ
	 * @param companyCode 会社コード
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	@TestVisible
	protected override Boolean isCodeDuplicated(ParentChildBaseEntity base) {
		DepartmentBaseEntity deptBase = (DepartmentBaseEntity)base;
		DepartmentBaseEntity duplicateBase = getEntityByCode(deptBase.code, deptBase.companyId);

		if (duplicateBase == null) {
			// 重複していない
			return false;
		}
		if (duplicateBase.id == deptBase.id) {
			// 重複していない
			return false;
		}

		// ここまできたら重複している
		return true;
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	private DepartmentBaseEntity getEntityByCode(String code, Id companyId) {
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.codes = new List<String>{code};
		filter.companyIds = new List<Id>{companyId};
		List<DepartmentBaseEntity> entityList = deptRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * Get a department with a valid history on the target date
	 * 指定した部署IDの対象日時点の部署を取得する
	 * @param departmentBaseId 部署のベースID
	 * @param targetDate 対象日、nullの場合は全履歴が取得対象
	 * @return 部署
	 * @throws App.RecordNotFoundException 部署が見つからない場合、対象日に有効な履歴が見つからない場合
	 */
	public DepartmentBaseEntity getDepartment(Id departmentBaseId, AppDate targetDate) {
		AppDateRange range = targetDate == null ? null : new AppDateRange(targetDate, targetDate);
		return getDepartmentByRange(departmentBaseId, range);
	}

	/**
	 * Get a department with valid history on the target period
	 * 指定した部署IDの部署を取得する。履歴は指定した期間に有効期間が1日以上含まれる履歴が返却対象となる
	 * @param departmentBaseId 部署ののベースID
	 * @param targetRange 取得対象の期間、nullの場合は全履歴が取得対象
	 *                    target period. if targetRange is null, return all valid history.
	 * @return 部署
	 * @throws App.RecordNotFoundException 部署が見つからない場合、対象期間で無効な期間が存在する場合
	 */
	public DepartmentBaseEntity getDepartmentByRange(Id departmentBaseId, AppDateRange targetRange) {
		// すべての有効な履歴も含めて対象の社員を取得する
		DepartmentBaseEntity department = deptRepo.getEntity(departmentBaseId);
		if (department == null) {
			// メッセージ：指定された部署が見つかりません。
			throw new App.RecordNotFoundException(
					ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Department}));
		}

		// 対象期間がnullの場合は全履歴を返却
		if (targetRange == null) {
			return department;
		}

		// 指定した期間でマスタが有効であることをチェック
		if (! department.isValid(targetRange.startDate) || ! department.isValid(targetRange.endDate)) {
			// メッセージ：指定された部署は有効期間外です
			throw new App.RecordNotFoundException(
					ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Department}));
		}

		// 対象日に有効な履歴を取得する
		List<ParentChildHistoryEntity> targetHistoryList = department.getSuperHistoryByDate(targetRange);

		// 対象日以外の履歴を削除する
		department.clearHistoryList();
		department.addHistoryAll(targetHistoryList);

		return department;
	}

	/**
	 * エンティティのユニークキーを更新する
	 * (会社のコードが変更されている可能性があるため、常に更新を行う)
	 * @param base ユニークキーを作成するエンティティ
	 */
	public void updateUniqKey(DepartmentBaseEntity base) {

		Id companyId = base.companyId;
		String code = base.code;

		// 既存のエンティティを更新する場合はエンティティに更新対象の項目以外は値が設定されていない可能性があるため
		// リポジトリから更新前のエンティティを取得する
		if ((base.id != null) && ((companyId == null) || (String.isBlank(code)))) {
			// 既存のエンティティを取得する
			DepartmentBaseEntity orgBase = deptRepo.getBaseEntity(base.id);
			if (orgBase == null) {
				// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			if (companyId == null) {
				companyId = orgBase.companyId;
			}
			if (String.isBlank(code)) {
				code = orgBase.code;
			}
		}

		if (companyId == null ) {
			// メッセージ：会社が設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Company}));
		}
		if (String.isBlank(code)) {
			// メッセージ：コードが設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}));
		}
		CompanyEntity company = getCompany(companyId);
		base.uniqKey = base.createUniqKey(company.code, code);
	}

	/*
	 * 部署の階層回帰チェックを行う
	 *@param base 部署のベースId
	 *@param historyList チェック対象の部署履歴Id
	 */
	protected override void validateCircular(ParentChildBaseEntity base, List<ParentChildHistoryEntity> historyList) {
		DepartmentBaseEntity deptBaseData = (DepartmentBaseEntity)base;
		Id companyId = deptBaseData.companyId;

		// 対象期間範囲を確定
		List<DepartmentHistoryEntity> targetChildList = new List<DepartmentHistoryEntity>();
		AppDate targetStartDate = null;
		AppDate targetEndDate = null;
		for (ParentChildHistoryEntity childData : historyList) {
			DepartmentHistoryEntity deptData = (DepartmentHistoryEntity)childData;
			if (deptData.parentBaseId != null) {
				targetChildList.add(deptData);
				if (targetStartDate == null || targetStartDate.isAfter(deptData.validFrom)) {
					targetStartDate = deptData.validFrom;
				}
				if (targetEndDate == null || targetEndDate.isBefore(deptData.validTo)) {
					targetEndDate = deptData.validTo;
				}
			}
		}
		if (targetChildList.isEmpty()) {
			return;
		}

		List<DepartmentHistoryEntity> allChildList = (List<DepartmentHistoryEntity>)deptRepo.getHistoryEntityListByCompany(companyId, targetStartDate, targetEndDate);
		CircularValidationService circularValidator = new CircularValidationService(MAX_LEVEL);
		try {
			List<DepartmentHistoryEntity> currentChildList = new List<DepartmentHistoryEntity>();
			currentChildList.addAll(targetChildList);
			while(!currentChildList.isEmpty()) {
				// 対象履歴と同じ期間の上位履歴を取得
				List<DepartmentHistoryEntity> upChildList = new List<DepartmentHistoryEntity>();
				for (DepartmentHistoryEntity currentChild : currentChildList) {
					Id upId = currentChild.parentBaseId;
					for(DepartmentHistoryEntity upChild : allChildList) {
						if (currentChild.id == upChild.id) {
							continue;
						}
						if (upId == upChild.baseId
								&& currentChild.validFrom.isBeforeEquals(upChild.validTo)
								&& currentChild.validTo.isAfterEquals(upChild.validFrom)) {
							// 階層登録
							circularValidator.appendChild(currentChild.baseId, upChild.baseId);
							upChildList.add(upChild);
						}
					}
				}
				currentChildList.clear();
				currentChildList.addAll(upChildList);
			}
		} catch (CircularValidationService.CircularException circulatEx) {
			String circularTargetId = circulatEx.circularTargetId;
			String circularTargetName = null;
			for (DepartmentHistoryEntity child : allChildList) {
				if (child.baseId == circularTargetId) {
					circularTargetName= child.nameL.getValue();
					break;
				}
			}
			throw new App.IllegalStateException(createCircularErrorMsg(circularTargetName));
		} catch (CircularValidationService.OverLevelException overLevelEx) {
			throw new App.IllegalStateException(createOverLevelErrorMsg(overLevelEx.levelLimit));
		}
	}
	// 階層の回帰チェックのエラーメッセージを作成する
	// 「データ名」の[上位階層名]の設定が循環参照になっております。
	private String createCircularErrorMsg(String targetName) {
		String upObjectLabel = ComMessage.msg().Admin_Lbl_ParentDepartName;
		return ComMessage.msg().Com_Err_CircularHierarchy(new List<String>{targetName, upObjectLabel});

	}
	// 階層の階層数オーバーのエラーメッセージを作成する
	// 「データ名」の階層が[階層数上限]を超えております。
	private String createOverLevelErrorMsg(Integer levelLimit) {
		return ComMessage.msg().Com_Err_MaxHierarchyOver(new List<String>{
				ComMessage.msg().Admin_Lbl_Department, String.valueOf(levelLimit)});
	}
}