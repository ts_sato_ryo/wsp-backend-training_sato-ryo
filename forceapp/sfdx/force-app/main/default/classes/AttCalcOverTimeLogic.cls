/**
 * 日次の勤務時間と残業の許可の内容から、残業時間を計算するロジック
 *
 * 残業時間は
 * 日次控除計算を行った後
 * ・所定勤務時間までは申請無しで残業認める設定なら、不足時間分の時間外勤務を認める処理を行い
 * ・控除と残業を相殺する設定なら、控除時間を不足時間に追加して計算を行い
 * ・補填時間を勤務時間に追加する設定なら、補填時間を勤務換算時間に追加した後で
 * <table>
 * <tr><th></th><th>&lt;=実行所定勤務時間</th><th>&gt;実行所定勤務時間</th></tr>
 * <tr><td>&lt;=法定勤務時間</td><td>所定内法定内勤務時間</td><td>>所定外法定内勤務時間</td></tr>
 * <tr><td>&gt;法定勤務時間</td><td>所定内法定外勤務時間</td><td>>所定外法定外勤務時間</td></tr>
 * </table>
 * に計算する
 * この数値を日種別(勤務日、休日、法定休日)に分類するのはこの外で行う
 * @author arimotoyosuke
 *
 */
public virtual with sharing class AttCalcOverTimeLogic {
	public enum OverTimeCategory { InContractInLegal, InContractOutLegal, OutContractInLegal, OutContractOutLegal}
	public virtual class Input extends AttCalcLostTimeLogic.Input {
		/**
		 * 半日有休/代休の換算時間を設定する。
		 *
		 * 半休などで勤務しなくても勤務したとみなす時間を設定する
		 * 勤務換算休憩と異なり、勤務から取り除く休憩時間は発生しない。
		 * 勤務換算時間との合計が所定勤務時間を越えてはならない。
		 * この時間を設定する場合、始業/終業時刻も調整を行うこと
		 * 例) 始業終業 9:00-18:00 所定休憩 12:00-13:00
		 *	午前半休時間帯: 9:00-13:00
		 *	午後半休時間帯: 13:00-18:00
		 * の場合、
		 * 午前半休を取ったら 始業時刻を 13:00にして 勤務換算時間 3:00 を設定する。
		 * 午後半休を取ったら 終業時刻を 13:00にして 勤務換算時間 5:00 を設定する
		 *
		 * 勤務換算休憩();と異なる点として、勤務換算休憩として午前半休を9:00-12:00として
		 * 与えると、11:30の出社ができなくなる
		 * 始業時刻を12:00に変えて、勤務換算時間に3:00を設定すると、11:30-12:00は、
		 * 通常の始業時刻前の勤務になり、勤務時間に追加して取り扱われる。
		 *
		 * @return 勤務時間に換算する時間を設定する
		 */
		public Integer convertedWorkTime = 0;
		/**
		 * @return 許可する時間外勤務時間 if {@code null} 時間外勤務を制限しない。
		 */
		public AttCalcRangesDto allowedWorkRanges = new AttCalcRangesDto();
		/**
		 * 法定勤務時間
		 * @return 法定勤務時間 if 法定勤務時間
		 */
		public Integer legalWorkTime = 0;
		/**
		 * 勤務日の勤怠計算かどうかを判定する。
		 * 勤務日でない場合、以下のパラメタは無効
		 *  startTime,所定勤務時刻
		 * @return 勤務日かどうか
		 */
		public Boolean isWorkDay = false;
		/**
		 * 法定休日かどうかを返す
		 * isWorkDay = true and isLegalHoliday = true もありあえるので注意
		 * @return 法定休日かどうか
		 */
		public Boolean isLegalHoliday = false;
		/**
		 * 控除と残業の相殺をするかどうかのオプション
		 * @return 控除と残業の相殺をしない
		 */
		public Boolean isNoOffsetOvertimeByUndertime = false;
		/**
		 * 所定時間まで申請無しで許可するかどうかのオプション
		 * @return 所定時間まで申請無しで許可
		 */
		public Boolean isPermitOvertimeWorkUntilNormalHours = false;
		/**
		 * 遅刻/早退/休憩補填時間を勤務換算時間として扱うかのオプション
		 * @return 遅刻/早退/休憩補填時間を勤務換算時間として扱う
		 */
		public Boolean isIncludeCompensationTimeToWorkHours = false;

		public Input clonea() {
			return this; // FIXME
		}
	}
	public class Output {
		/** 始業終業の間で実際に働いた時間 */
		public Integer realWorkTime = 0;
		/** 種別ごとの勤務時間を計算するマップ */
		public Map<OverTimeCategory, AttCalcRangesDto> workTimeCategoryMap = new Map<OverTimeCategory, AttCalcRangesDto>();
		/** 始業終業の間で実際に働いた時間に換算する実勤務外の時間(有休、補填) */
		public Integer convertedWorkTime = 0;
		/** 無給控除時間 */
		public Integer unpaidLeaveLostTime = 0;
		/** 有給休暇時間 */
		public Integer paidLeaveTime = 0;
		/** 無給休暇時間 */
		public Integer unpaidLeaveTime = 0;
		/** 早退した時間 */
		public Integer earlyLeaveTime = 0;
		/** 遅刻した時間 */
		public Integer lateArriveTime = 0;
		/** 取得した休憩時間 */
		public Integer breakTime = 0;
		/** 早退時間のうち、控除される時間 */
		public Integer earlyLeaveLostTime = 0;
		/** 遅刻時間のうち、控除される時間 */
		public Integer lateArriveLostTime = 0;
		/** 休憩時間のうち、控除される時間 */
		public Integer breakLostTime = 0;
		/** 早退のうち勤務とみなす時間 */
		public Integer authorizedEarlyLeaveTime = 0;
		/** 遅刻のうち勤務とみなす時間 */
		public Integer authorizedLateArriveTime = 0;
		/** 休憩のうち勤務とみなす時間 */
		public Integer authorizedBreakTime = 0;
		/** 不足勤務時間 */
		public Integer shortageWorkTime = 0;
		/** 未処理休憩時間 */
		public Integer unknowBreakTime = 0;
	}
	private AttCalcLostTimeLogic dlogic = new AttCalcLostTimeLogic();

	/**
	 * 時間 t までの勤務を表すRangesを計算する。
	 * まず in の時間帯を先頭から t 時間になるまでを取り出し
	 * 足りなければ before を末尾からtに達するまで取り出し
	 * それでも足りなければ after の先頭から tに達するまで取り出す
	 *
	 * 例)
	 *   in = [9-12,13-18], before=[7-9], after=[18-19,20-23]
	 *   指定時間までの勤務時間帯(7, in, before, after) -> [9-12,13-17]
	 *   指定時間までの勤務時間帯(9, in, before, after) -> [8-12,13-18]
	 *   指定時間までの勤務時間帯(11, in, before, after) -> [7-12,13-19]
	 *   指定時間までの勤務時間帯(12, in, before, after) -> [7-12,13-19,20-21]
	 *
	 * @param t 時間
	 * @param in 所定内勤務
	 * @param before 始業時刻より前の勤務
	 * @param after 終業時刻より後の勤務
	 * @return
	 */
	public AttCalcRangesDto getWorkRangesByStartTime(Integer t, AttCalcRangesDto inRanges, AttCalcRangesDto beforeRanges, AttCalcRangesDto afterRanges) {
		Integer t1 = inRanges.timeFromStart(t);
		AttCalcRangesDto r1 = inRanges.cutBefore(t1);
		Integer t2 = beforeRanges.timeFromEnd(t - r1.total());
		AttCalcRangesDto r2 = r1.insertAndMerge(beforeRanges.cutAfter(t2));
		Integer t3 = afterRanges.timeFromStart(t - r2.total());
		return r2.insertAndMerge(afterRanges.cutBefore(t3));
	}

	/**
	 * 時間 t1 から t2までの勤務を表すRangesを計算する
	 * t2までの"指定時間までの勤務時間帯"から t1までの"指定時間までの勤務時間帯"を取り除いたものになる
	 * 例)
	 * 	 in = [9-12,13-18], before=[7-9], after=[18-19,20-23]
	 *   指定時間までの勤務時間帯(7, 9, in, before, after) -> [8-9,17-18]
	 *
	 * @param t1 基準時間1
	 * @param t2 基準時間2
	 * @param in 所定内勤務
	 * @param before 始業時刻より前の勤務
	 * @param after 終業時刻より後の勤務
	 * @return
	 */
	public AttCalcRangesDto getWorkRangesByPeriod(Integer t1, Integer t2, AttCalcRangesDto inRanges, AttCalcRangesDto beforeRanges, AttCalcRangesDto afterRanges) {
		// この実装は効率がよくない仕様用Specialなので実装は再設計して欲しい
		AttCalcRangesDto r1 = getWorkRangesByStartTime(t1, inRanges, beforeRanges, afterRanges);
		AttCalcRangesDto r2 = getWorkRangesByStartTime(t2, inRanges, beforeRanges, afterRanges);
		return r2.exclude(r1);
	}

	/**
	 * 時間 t1 以降の勤務を表すRangesを計算する
	 * 全部の勤務時間を合わせたものから t1までの"指定時間までの勤務時間帯"を取り除いたものになる
	 * 例)
	 * 	 in = [9-12,13-18], before=[7-9], after=[18-19,20-23]
	 *   指定時間以降の勤務時間帯(9, in, before, after) -> [7-8,18-19,20-23]
	 *
	 * @param t1 基準時間
	 * @param in 所定内勤務
	 * @param before 始業時刻より前の勤務
	 * @param after 終業時刻より後の勤務
	 * @return
	 */
	public AttCalcRangesDto getWorkRangesByEndTime(Integer t1, AttCalcRangesDto inRanges, AttCalcRangesDto beforeRanges, AttCalcRangesDto afterRanges) {
		// この実装は効率がよくない仕様用Specialなので実装は再設計して欲しい
		AttCalcRangesDto r1 = getWorkRangesByStartTime(t1, inRanges, beforeRanges, afterRanges);
		AttCalcRangesDto r2 = inRanges.insertAndMerge(beforeRanges).insertAndMerge(afterRanges);
		return r2.exclude(r1);
	}

	/**
	 * 勤務日のルールで、残業時間を計算する
	 * 勤務日で法定休日でない場合(通常)は
	 * <ol>
	 * <li>所定勤務時間をもとに所定内/外の判定を行う</li>
	 * <li>法定勤務時間がもとに法定内/外の判定を行う</li>
	 * <li>始業終了時刻が決められていて、遅刻/早退/休憩の控除がある</li>
	 * </ol>
	 * 勤務日で法定休日の場合は
	 * <ol>
	 * <li>所定勤務時間をもとに所定内/外の判定を行う</li>
	 * <li>すべて法定外として扱う</li>
	 * <li>始業終了時刻が決められていて、遅刻/早退/休憩の控除がある</li>
	 * </ol>
	 *
	 * @param out
	 * @param in
	 */
	void applyWorkDay(Output output, Input input) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcOverTimeLogic.applyWorkDay start');
		// 出退勤してない場合も計算する項目
		// 勤務相当時間
		output.convertedWorkTime = input.convertedWorkTime;
		if (input.inputStartTime == null || input.inputEndTime == null) {
			System.debug(LoggingLevel.DEBUG, '---AttCalcOverTimeLogic.applyWorkDay return');
			return;
		}
		AttCalcLostTimeLogic.Output middle = new AttCalcLostTimeLogic.Output();
		// 全勤務時間
		// Javaロジックとの差異:
		// Javaロジックでは無給休暇休憩が取り除かれていないが、取り除かれるのが正しい
		AttCalcRangesDto workTimeRanges = AttCalcRangesDto.RS(input.inputStartTime, input.inputEndTime)
				.exclude(input.inputRestRanges)
				.exclude(input.convertdRestRanges)
				.exclude(input.unpaidLeaveRestRanges);
		// 所定時間内の勤務時間
		AttCalcRangesDto contractWorkRanges = workTimeRanges.cutBetween(input.startTime, input.endTime);
		// 始業時刻前の勤務を事前勤務とする
		AttCalcRangesDto earlyWorkRanges = workTimeRanges.cutBetween(input.inputStartTime, input.startTime);
		// 終業時刻後の勤務を事後勤務とする
		AttCalcRangesDto lateWorkRanges = workTimeRanges.cutBetween(input.endTime, input.inputEndTime);
		// 勤務内所定外の入力休暇
		AttCalcRangesDto inputRestRanges = input.inputRestRanges.exclude(AttCalcRangesDto.RS(input.startTime, input.endTime)).cutBetween(input.inputStartTime, input.inputEndTime);
		// 控除ロジックを呼び出す
		Input tmpInput = input.clone();
		tmpInput.inputRestTime = Math.min(input.inputRestTime, contractWorkRanges.total());
		Integer inputRestTimeOver = input.inputRestTime - tmpInput.inputRestTime;
		dlogic.apply(middle, tmpInput);
		middle.unknowBreakTime += inputRestTimeOver;
		// その日に働かなくてはいけない時間(実効所定勤務時間)を取得
		Integer realContractTime = middle.realContractTime; // input.contractWorkTime - middle.convertedWorkTime;

		// 所定勤務時間に達するまで申請不要のオプション用の不足時間を計算する。
		Integer shortageTime = Math.max(-middle.gapTime, 0);

		Integer convertedWorkTime = input.convertedWorkTime;

		// 補填時間は勤務時間に加える場合は勤務換算時間に足す
		if (input.isIncludeCompensationTimeToWorkHours) {
			convertedWorkTime += middle.authorizedBreakTime + middle.authorizedLateArriveTime + middle.authorizedEarlyLeaveTime;
		} else {
			// そうでない場合は、不足時間のほうに追加する
			shortageTime += middle.authorizedBreakTime + middle.authorizedLateArriveTime + middle.authorizedEarlyLeaveTime;
		}
		// 控除と残業を相殺するなら控除時間も不足時間に加える
		if (!input.isNoOffsetOvertimeByUndertime) {
			shortageTime += middle.lateArriveLostTime + middle.earlyLeaveLostTime + middle.breakLostTime;
		}
		else {
			realContractTime -= (middle.lateArriveLostTime + middle.earlyLeaveLostTime + middle.breakLostTime);
		}
		// 許可されてない勤務時間は認めない場合
		if (input.allowedWorkRanges.total() > 0) {
			AttCalcRangesDto allowedWorkRanges = input.allowedWorkRanges;
			// 所定時間まで申請無しで許可オプションの場合、不足分を残業許可時間に入れる

			if (input.isPermitOvertimeWorkUntilNormalHours && shortageTime > 0) {
				AttCalcRangesDto offsetedOverRanges = getWorkRangesByStartTime(shortageTime, AttCalcRangesDto.RS(), earlyWorkRanges, lateWorkRanges);
				allowedWorkRanges = allowedWorkRanges.insertAndMerge(offsetedOverRanges);
			}
			// 事前勤務,事後勤務を許可された時間だけにする。
			earlyWorkRanges = earlyWorkRanges.include(allowedWorkRanges);
			lateWorkRanges = lateWorkRanges.include(allowedWorkRanges);
			inputRestRanges = inputRestRanges.include(allowedWorkRanges);
		}

		// 許可する残業時間が決まったので、控除との相殺の計算を行う
		Integer offsetLateArriveLostTime = 0;
		Integer offsetEarlyLeaveLostTime = 0;
		Integer offsetBreakLostTime = 0;
		System.debug(LoggingLevel.DEBUG, '--- shortageTime is ' + shortageTime);
		if (shortageTime > 0 && ! input.isNoOffsetOvertimeByUndertime) {
			Integer offsetedOverTime = getWorkRangesByStartTime(shortageTime, AttCalcRangesDto.RS(), earlyWorkRanges, lateWorkRanges).total();
			shortageTime -= offsetedOverTime;
			offsetLateArriveLostTime = Math.min(offsetedOverTime, middle.lateArriveLostTime);
			offsetEarlyLeaveLostTime = Math.min(offsetedOverTime - offsetLateArriveLostTime, middle.earlyLeaveLostTime);
			offsetBreakLostTime = Math.min(offsetedOverTime - offsetLateArriveLostTime - offsetEarlyLeaveLostTime, middle.breakLostTime);
		}
		system.debug(LoggingLevel.DEBUG, '---offsetLateArriveLostTime is ' + offsetLateArriveLostTime);
		system.debug(LoggingLevel.DEBUG, '---offsetEarlyLeaveLostTime is ' + offsetEarlyLeaveLostTime);
		system.debug(LoggingLevel.DEBUG, '---offsetBreakLostTime is ' + offsetBreakLostTime);
		system.debug(LoggingLevel.DEBUG, '---realContractTime is ' + realContractTime);
		system.debug(LoggingLevel.DEBUG, '---contractWorkRanges is ' + contractWorkRanges);
		system.debug(LoggingLevel.DEBUG, '---earlyWorkRanges is ' + earlyWorkRanges);
		system.debug(LoggingLevel.DEBUG, '---lateWorkRanges is ' + lateWorkRanges);
		Integer restTimeInContract = Math.max(0, input.inputRestTime - middle.unknowBreakTime); // 所定内に取った追加休憩時間
		if (input.isLegalHoliday) { // 法定休日になった勤務日の場合
			AttCalcRangesDto inContractInLegalRanges = getWorkRangesByStartTime(realContractTime + restTimeInContract, contractWorkRanges, earlyWorkRanges, lateWorkRanges);
			AttCalcRangesDto outContractOutLegalRanges = getWorkRangesByEndTime(realContractTime + restTimeInContract, contractWorkRanges, earlyWorkRanges, lateWorkRanges);
			output.workTimeCategoryMap.put(OverTimeCategory.InContractOutLegal, inContractInLegalRanges);
			output.workTimeCategoryMap.put(OverTimeCategory.OutContractOutLegal, outContractOutLegalRanges);
			output.realWorkTime = inContractInLegalRanges.total() + outContractOutLegalRanges.total() - input.inputRestTime;
		} else {
			// 通常の勤務日の場合
			// 所定内に取った追加休憩時間分だけ、多めに所定内法定内時間を計算する
			AttCalcRangesDto inContractInLegalRanges = getWorkRangesByStartTime(Math.min(realContractTime, input.legalWorkTime) + restTimeInContract, contractWorkRanges, earlyWorkRanges, lateWorkRanges);
			// 以降のロジックを簡単にするため、所定内勤務の最初に所定内に取った追加休憩時間分の休憩を取ったものとみなす。
			contractWorkRanges = contractWorkRanges.cutAfter(contractWorkRanges.timeFromStart(restTimeInContract));
			AttCalcRangesDto outContractInLegalRanges = (realContractTime < input.legalWorkTime) ? getWorkRangesByPeriod(realContractTime, input.legalWorkTime, contractWorkRanges, earlyWorkRanges, lateWorkRanges) : AttCalcRangesDto.RS();
			AttCalcRangesDto inContractOutLegalRanges = (input.legalWorkTime < realContractTime) ? getWorkRangesByPeriod(input.legalWorkTime, realContractTime, contractWorkRanges, earlyWorkRanges, lateWorkRanges) : AttCalcRangesDto.RS();
			AttCalcRangesDto outContractOutLegalRanges = getWorkRangesByEndTime(Math.max(realContractTime, input.legalWorkTime), contractWorkRanges, earlyWorkRanges, lateWorkRanges);
			output.workTimeCategoryMap.put(OverTimeCategory.InContractInLegal, inContractInLegalRanges);
			output.workTimeCategoryMap.put(OverTimeCategory.InContractOutLegal, inContractOutLegalRanges);
			output.workTimeCategoryMap.put(OverTimeCategory.OutContractInLegal, outContractInLegalRanges);
			output.workTimeCategoryMap.put(OverTimeCategory.OutContractOutLegal, outContractOutLegalRanges);
			output.realWorkTime = inContractInLegalRanges.total() + outContractInLegalRanges.total() + inContractOutLegalRanges.total() + outContractOutLegalRanges.total() - input.inputRestTime;
		}
		output.convertedWorkTime = convertedWorkTime;
		output.unpaidLeaveLostTime = middle.unpaidLeaveLostTime;
		output.paidLeaveTime = middle.paidLeaveTime;
		output.unpaidLeaveTime = middle.unpaidLeaveTime;
		output.earlyLeaveTime = middle.earlyLeaveTime;
		output.lateArriveTime = middle.lateArriveTime;
		output.breakTime = middle.breakTime + inputRestRanges.total();
		output.earlyLeaveLostTime = middle.earlyLeaveLostTime - offsetEarlyLeaveLostTime;
		output.lateArriveLostTime = middle.lateArriveLostTime - offsetLateArriveLostTime;
		output.breakLostTime = middle.breakLostTime - offsetBreakLostTime;
		output.authorizedEarlyLeaveTime = middle.authorizedEarlyLeaveTime;
		output.authorizedLateArriveTime = middle.authorizedLateArriveTime;
		output.authorizedBreakTime = middle.authorizedBreakTime;
		output.shortageWorkTime = shortageTime;
		output.unknowBreakTime = middle.unknowBreakTime;
		if (output.realWorkTime < 0) {
			output.unknowBreakTime = Math.max(0, output.unknowBreakTime + output.realWorkTime);
			output.realWorkTime = 0;
		}
		System.debug(LoggingLevel.DEBUG, '---output is ' + output);
		System.debug(LoggingLevel.DEBUG, '---AttCalcOverTimeLogic.applyWorkDay end');
	}

	/**
	 * 所定休日のルールで残業時間を計算する
	 * 勤務日のルールとは
	 * <ol>
	 * <li>所定勤務時間はない。すべて所定外</li>
	 * <li>法定勤務時間がもとに法定内/外の判定を行う</li>
	 * <li>始業終了時刻がないので控除はない</li>
	 * </ol>
	 *
	 * @param out
	 * @param in
	 */
	void applyHoliday(Output output, Input input) {
		if (input.inputStartTime == null || input.inputEndTime == null) {
			return;
		}
		AttCalcRangesDto workRanges = AttCalcRangesDto.RS(input.inputStartTime, input.inputEndTime).exclude(input.inputRestRanges);
		AttCalcRangesDto permittedWorkRanges = workRanges.include(input.allowedWorkRanges);

		Integer t = permittedWorkRanges.timeFromStart(input.legalWorkTime);
		AttCalcRangesDto outContractInLegalRanges = permittedWorkRanges.cutBefore(t);
		AttCalcRangesDto outContractOutLegalRanges = permittedWorkRanges.cutAfter(t);
		output.workTimeCategoryMap.put(OverTimeCategory.OutContractInLegal, outContractInLegalRanges);
		output.workTimeCategoryMap.put(OverTimeCategory.OutContractOutLegal, outContractOutLegalRanges);

		Integer realWorkTime = outContractInLegalRanges.total() + outContractOutLegalRanges.total();
		output.realWorkTime = Math.max(0, realWorkTime - input.inputRestTime);
		output.convertedWorkTime = 0;
		output.unpaidLeaveLostTime = 0;
		output.paidLeaveTime = 0;
		output.unpaidLeaveTime = 0;
		output.earlyLeaveTime = 0;
		output.lateArriveTime = 0;
		output.breakTime = input.inputRestRanges.include(input.allowedWorkRanges).total();
		output.unknowBreakTime = Math.min(input.inputRestTime, realWorkTime); // その他休憩時間を呼び出す処理相殺する
		output.earlyLeaveLostTime = 0;
		output.lateArriveLostTime = 0;
		output.breakLostTime = 0;
		output.authorizedEarlyLeaveTime = 0;
		output.authorizedLateArriveTime = 0;
		output.authorizedBreakTime = 0;
		output.shortageWorkTime = 0;
	}

	/**
	 * 法定休日のルールで残業時間を計算する
	 * 勤務日のルールとは
	 * <ol>
	 * <li>所定勤務時間はない。すべて所定外</li>
	 * <li>法定休日の勤務はすべて法定外として扱う</li>
	 * <li>始業終了時刻がないので控除はない</li>
	 * </ol>
	 *
	 * @param out
	 * @param in
	 */
	void applyLegalHoliday(Output output, Input input) {
		if (input.inputStartTime == null || input.inputEndTime == null) {
			return;
		}
		AttCalcRangesDto workRanges = AttCalcRangesDto.RS(input.inputStartTime, input.inputEndTime).exclude(input.inputRestRanges);
		AttCalcRangesDto permittedWorkRanges = workRanges.include(input.allowedWorkRanges);

		// 法定休日の勤務は所定外/法定外のカウントに入れる。
		output.workTimeCategoryMap.put(OverTimeCategory.OutContractOutLegal, permittedWorkRanges);
		Integer realWorkTime = permittedWorkRanges.total();
		output.realWorkTime = Math.max(0, realWorkTime - input.inputRestTime);
		output.convertedWorkTime = 0;
		output.unpaidLeaveLostTime = 0;
		output.paidLeaveTime = 0;
		output.unpaidLeaveTime = 0;
		output.earlyLeaveTime = 0;
		output.lateArriveTime = 0;
		output.breakTime = input.inputRestRanges.include(input.allowedWorkRanges).total();
		output.unknowBreakTime = Math.min(input.inputRestTime, realWorkTime); // その他休憩時間を呼び出す処理相殺する
		output.earlyLeaveLostTime = 0;
		output.lateArriveLostTime = 0;
		output.breakLostTime = 0;
		output.authorizedEarlyLeaveTime = 0;
		output.authorizedLateArriveTime = 0;
		output.authorizedBreakTime = 0;
		output.shortageWorkTime = 0;
	}

	/**
	 * 残業時間を計算する
	 */
	public void apply(Output output, Input input) {
		system.debug(LoggingLevel.DEBUG, '---AttCalcOverTimeLogic apply start');
		system.debug(LoggingLevel.DEBUG, '---input is ' + input);
		if(input.isWorkDay) {
			// 法定休日で勤務日の場合の処理も中で行っている
			applyWorkDay(output, input);
		} else if (input.isLegalHoliday) {
			applyLegalHoliday(output, input);
		} else {
			applyHoliday(output, input);
		}
		system.debug(logginglevel.info, '---AttCalcOverTimeLogic apply end');
	}
}