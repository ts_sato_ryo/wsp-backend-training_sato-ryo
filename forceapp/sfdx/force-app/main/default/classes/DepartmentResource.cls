/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署レコード操作APIを実装するクラス
 */
public with sharing class DepartmentResource {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 部署レコードを表すクラス
	 */
	public class Department implements RemoteApi.RequestParam {
		/** 部署レコードベースID */
		public String id;
		/** 部署履歴レコードID */
		public String historyId;
		/** 部署名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 部署名(言語0) */
		public String name_L0;
		/** 部署名(言語1) */
		public String name_L1;
		/** 部署名(言語2) */
		public String name_L2;
		/** 部署コード */
		public String code;
		/** 会社レコードID */
		public String companyId;
		/** 部署管理者のユーザレコードID */
		public String managerId;
		/** 部署管理者 */
		public LookupField manager;
		/** 親部署ID */
		public String parentId;
		/** 親部署 */
		public LookupField parent;
		/** 備考 */
		public String remarks;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** コメント */
		public String comment;
		/** 親部署フラグ (取得専用) */
		public Boolean hasChildren;

		/**
		 * パラメータの値を設定したベースエンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public DepartmentBaseEntity createBaseEntity(Map<String, Object> paramMap) {

			DepartmentBaseEntity base = new DepartmentBaseEntity();
			base.setId(this.id);
			if(paramMap.containsKey('code')){
				base.code = this.code;
			}
			// 会社IDは主従関係の親かつ変更不可
			if((this.id == null) && paramMap.containsKey('companyId')){
				base.companyId = this.companyId;
			}

			// TODO 名前項目は暫定対応。とりあえず現状維持
			if(paramMap.containsKey('name_L0')){
				base.name = this.name_L0;
			}
			return base;
		}

		/**
		 * 部署エンティティを作成する
		 * @param paramMap パラメータのMap(キーが存在するパラメータのみ更新する)
		 */
		public DepartmentHistoryEntity createHistoryEntity(Map<String, Object> paramMap) {

			DepartmentHistoryEntity history = new DepartmentHistoryEntity();

			// 以下、レコード作成時のみ値をセット
			if(paramMap.containsKey('name_L0')){
				history.nameL0 = this.name_L0;

				// TODO 名前項目は暫定対応。とりあえず現状維持
				history.name = this.name_L0;
			}
			if(paramMap.containsKey('name_L1')){
				history.nameL1 = this.name_L1;
			}
			if(paramMap.containsKey('name_L2')){
				history.nameL2 = this.name_L2;
			}
			if(paramMap.containsKey('managerId')){
				history.managerId = this.managerId;
			}
			if(paramMap.containsKey('parentId')){
				history.parentBaseId = this.parentId;
			}
			if(paramMap.containsKey('remarks')){
				history.remarks = this.remarks;
			}
			if(paramMap.containsKey('validDateFrom')){
				history.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (history.validFrom == null) {
				// デフォルトは実行日
				history.validFrom = AppDate.today();
			}
			// レコード更新時は失効日を無視する (DepartmentRepository参照)
			if(paramMap.containsKey('validDateTo')){
				history.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (history.validTo == null) {
				history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}
			if(paramMap.containsKey('comment')){
				history.historyComment = this.comment;
			}

			return history;
		}

	}

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		public String name;
	}

	/**
	 * @description 部署レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したベースレコードID */
		public String id;
	}

	/**
	 * @description 部署レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_DEPARTMENT;

		/**
		 * @description 部署レコードを1件作成する
		 * @param  req リクエストパラメータ(Department)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			Department param = (Department)req.getParam(DepartmentResource.Department.class);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			DepartmentBaseEntity base = param.createBaseEntity(req.getParamMap());
			DepartmentHistoryEntity history = param.createHistoryEntity(req.getParamMap());
			base.addHistory(history);

			// 保存する
			DepartmentService service = new DepartmentService();
			service.updateUniqKey(base);
			Id resId = service.saveNewEntity(base);

			DepartmentResource.SaveResult res = new DepartmentResource.SaveResult();
			res.Id = resId;
			return res;

		}

	}

	/**
	 * @desctiprion 部署レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_DEPARTMENT;

		/**
		 * @description 部署レコードを1件更新する(ベースの項目のみ)
		 * @param  req リクエストパラメータ(Department)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 更新情報を設定したエンティティを作成する
			Department param = (Department)req.getParam(Department.class);

			// パラメータのバリデーション
			validateParam(param);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 更新する
			DepartmentBaseEntity base = param.createBaseEntity(req.getParamMap());
			DepartmentService service = new DepartmentService();
			service.updateUniqKey(base);
			service.updateBase(base);

			// 成功レスポンスをセットする
			return null;
		}

		/**
		 * ベース更新APIのバリデーションを実行
		 */
		private void validateParam(DepartmentResource.Department param) {
			// ID
			if (String.isBlank(param.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('ID');
				throw ex;
			}
		}
	}

	/**
	 * @description 部署レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}

	}

	/**
	 * 部署レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_DEPARTMENT;

		/**
		 * @description 部署レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				(new DepartmentService()).deleteEntity(param.id);
				// (new DepartmentRepository()).deleteEntity(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合は成功レスポンスを返す
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		}

	}


	/**
	 * @description 部署レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 関連する部署レコードID */
		public String companyId;
		/** 親部署レコードID */
		public String parentId;
		/** 有効期間の対象日 */
		public Date targetDate;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// companyId
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}

			// parentId
			if (String.isNotBlank(this.parentId)) {
				try {
					System.Id.valueOf(this.parentId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'parentId'}));
				}
			}
		}
	}

	/**
	 * @description 部署レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public Department[] records;
	}

	/**
	 * @description 部署レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 部署レコードを1件登録する
		 * @param  parameter リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);

			// パラメータのバリデーション
			param.validate();

			// 部署を検索
			AppDate targetDate = param.targetDate == null ? AppDate.today() : AppDate.valueOf(param.targetDate);
			DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
			filter.baseIds = String.isBlank(param.id) ? null : new List<Id>{param.id};
			filter.companyIds = String.isBlank(param.companyId) ? null : new List<Id>{param.companyId};
			filter.parentIds = String.isBlank(param.parentId) ? null : new List<Id>{param.parentId};
			filter.targetDate = targetDate;
			List<DepartmentBaseEntity> baseList = (new DepartmentRepository()).searchEntity(filter);

			DepartmentService service = new DepartmentService();

			// 子部署の有無を検索
			List<Id> baseIds = new List<Id>();
			for (DepartmentBaseEntity base : baseList) {
				baseIds.add(base.id);
			}
			Map<Id, Boolean> hasChildrenMap = service.hasChildren(baseIds, targetDate);

			// 親部署のエンティティを取得
			List<Id> parentIds = new List<Id>();
			for (DepartmentBaseEntity base : baseList) {
				// 対象日を指定しているため、各マスタの履歴は1件のはず
				DepartmentHistoryEntity history = base.getHistory(0);
				parentIds.add(history.parentBaseId);
			}
			Map<Id, DepartmentHistoryEntity> parentHistoryMap = service.getHistoryMap(parentIds, targetDate);

			// レスポンスクラスに変換
			List<Department> departmentList = new List<Department>();
			for (DepartmentBaseEntity base : baseList) {
				DepartmentHistoryEntity history = base.getHistoryList().get(0);
				Department d = new Department();
				d.id = base.id;
				d.code = base.code;
				d.companyId = base.companyId;
				d.historyId = history.id;
				d.name = history.nameL.getValue(); // ユーザ言語での部署名
				d.name_L0 = history.nameL.valueL0;
				d.name_L1 = history.nameL.valueL1;
				d.name_L2 = history.nameL.valueL2;
				d.validDateFrom = AppConverter.dateValue(history.validFrom);
				d.validDateTo = AppConverter.dateValue(history.validTo);
				d.remarks = history.remarks;

				d.parentId = history.parentBaseId;
				d.parent = new LookupField();
				if (history.parentBaseId != null && parentHistoryMap.containsKey(history.parentBaseId)) {
					d.parent.name = parentHistoryMap.get(history.parentBaseId).nameL.getValue();
				}

				d.managerId = history.managerId;
				d.manager = new LookupField();
				d.manager.name = history.manager != null ? history.manager.fullName.getFullName() : null;

				d.hasChildren = hasChildrenMap.get(base.id);

				departmentList.add(d);

			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = departmentList;
			return res;
		}

	}

}
