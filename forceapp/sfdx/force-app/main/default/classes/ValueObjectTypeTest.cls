/**
 * ValueObjectTypeのテスト
 */
@isTest
private class ValueObjectTypeTest {

	/**
	 * 値とラベルが正しく出力されることを確認する
	 */
	@isTest static void toStringTest() {

		// ラベルが設定されていない場合
		System.assertEquals(AttDayType.WORKDAY.value, AttDayType.WORKDAY.toString());

		// ラベルが設定されている場合
		System.assertEquals(AttRequestType.LEAVE.value + '(label:' + AttRequestType.LEAVE.label + ')',
				AttRequestType.LEAVE.toString());
	}

}
