/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 共通
  *
 * カレンダーのエンティティ
 */
public with sharing class CalendarEntity extends LogicalDeleteEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** ユニークキーの最大文字数 */
	public static final Integer UNIQ_KEY_MAX_LENGTH = 31;
	/** カレンダー名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 備考の最大文字数 */
	public static final Integer REMARKS_MAX_LENGTH = 255;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		COMPANY_ID,
		CODE,
		UNIQ_KEY,
		Name,
		Name_L,
		CAL_TYPE,
		REMARKS
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ
	 */
	public CalendarEntity() {
		isChangedFieldSet = new Set<Field>();
		recordList = new List<CalendarRecordEntity>();
	}
	/** カレンダーコード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}
	/** ユニークキー */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}
	/** カレンダー名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.Name);
		}
	}
	/** カレンダー名(L0-2) */
	public AppMultiString nameL {
		get;
		set {
			nameL = value;
			setChanged(Field.Name_L);
		}
	}
	/** 会社ID */
	public String companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}
	/** 会社コード（参照のみ。値を変更してもDBに保存されない。） */
	public String companyCode {get; set;}
	/** タイプ */
	public CalendarType calType {
		get;
		set {
			calType = value;
			setChanged(Field.CAL_TYPE);
		}
	}
	/** 備考 */
	public String remarks {
		get;
		set {
			remarks = value;
			setChanged(Field.REMARKS);
		}
	}

	/** カレンダー明細リスト */
	public List<CalendarRecordEntity> recordList {get; private set;}

	public void appendRecord(CalendarRecordEntity recordData) {
		this.recordList.add(recordData);
	}

	/**
	 * ユニークキーを作成する
	 * カレンダーコードを指定しておくこと。
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]ジョブのユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]ジョブのユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + カレンダーコード
		return companyCode + '-' + this.code;
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	protected override void resetChangedInSubClass() {
		isChangedFieldSet.clear();
	}

	/**
	 * 指定された項目の値を取得する
	 * @param targetField 対象の項目
	 * @return 項目値
	 */
	public Object getFieldValue(CalendarEntity.Field targetField) {

		if (targetField == Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == Field.CODE) {
			return this.code;
		}
		if (targetField == Field.UNIQ_KEY) {
			return this.uniqKey;
		}
		if (targetField == Field.Name) {
			return this.name;
		}
		if (targetField == Field.Name_L) {
			return this.nameL;
		}
		if (targetField == Field.CAL_TYPE) {
			return this.calType;
		}
		if (targetField == Field.REMARKS) {
			return this.remarks;
		}

		// ここにきたらバグ
		throw new App.UnsupportedException('CalendarEntity.getFieldValue: 未対応の項目です。targetField='+ targetField);
	}

	/**
	 * 指定された項目に値を設定する
	 * @param targetField 対象の項目
	 * @pram value 設定値
	 */
	public void setFieldValue(CalendarEntity.Field targetField, Object value) {

		if (targetField == Field.COMPANY_ID) {
			this.companyId = (Id)value;
		} else if (targetField == Field.CODE) {
			this.code = (String)value;
		} else if (targetField == Field.UNIQ_KEY) {
			this.uniqKey = (String)value;
		} else if (targetField == Field.Name) {
			this.name = (String)value;
		} else if (targetField == Field.Name_L) {
			this.nameL = (AppMultiString)value;
		} else if (targetField == Field.CAL_TYPE) {
			this.calType = (CalendarType)value;
		} else if (targetField == Field.REMARKS) {
			this.remarks = (String)value;
		} else {
			// ここにきたらバグ
			throw new App.UnsupportedException('CalendarEntity.setFieldValue: 未対応の項目です。targetField='+ targetField);
		}
	}
}