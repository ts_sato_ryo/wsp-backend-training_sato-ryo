/**
 * @group 共通
 *
 * 打刻種別
 */
public with sharing class AttStampType extends ValueObjectType {

	// 出勤打刻
	public static final AttStampType CLOCK_IN = new AttStampType('ClockIn');
	// 退勤打刻
	public static final AttStampType CLOCK_OUT = new AttStampType('ClockOut');
	// 休憩開始
	public static final AttStampType REST_START = new AttStampType('RestStart');
	// 休憩終了
	public static final AttStampType REST_END = new AttStampType('RestEnd');

	// 打刻種別のリスト
	public static final List<AttStampType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttStampType> {
			CLOCK_IN,
			CLOCK_OUT,
			REST_START,
			REST_END
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttStampType(String value) {
		super(value);
	}


	/**
	 * 値からAttStampTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttStampType、ただし該当するAttStampTypeが存在しない場合はnull
	 */
	public static AttStampType valueOf(String value) {
		AttStampType retType = null;
		if (String.isNotBlank(value)) {
			for (AttStampType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttStampType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttStampType) {
			// 値が同じであればtrue
			if (this.value == ((AttStampType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}
