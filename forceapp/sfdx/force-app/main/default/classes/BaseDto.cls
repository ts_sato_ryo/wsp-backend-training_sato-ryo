/**
 * Dto基本クラス
 */
public abstract with sharing class BaseDto {
	public Id id;
	public String name;
}