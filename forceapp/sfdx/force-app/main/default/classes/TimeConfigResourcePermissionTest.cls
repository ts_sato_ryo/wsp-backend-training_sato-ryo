/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 工数
 *
 * TimeConfigResourcePermissionクラスのテスト
 */
@isTest
private class TimeConfigResourcePermissionTest {

	/** 権限リポジトリ */
	private static PermissionRepository permissionRepo = new PermissionRepository();

	/** テストデータクラス */
	private class TestData extends TestData.TestDataEntity {
		private PermissionRepository permissionRepo = new PermissionRepository();

		public TestData() {
			super();
			// 明示的に社員データを作成しておく
			this.employee = this.employee;
		}
	}

	/**
	 * 未実装の権限が指定された場合、App.UnsupportedExceptionが発生することを確認する
	 */
	@isTest static void hasExecutePermissionTestInvalidPermission() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.UnsupportedException actEx;
			TimeConfigResourcePermission.Permission requiredPermission = null;

			actEx = null;
			testData.permission.isManageTimeWorkCategory = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new TimeConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.UnsupportedException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
		}
	}

	/**
	 * 工数設定の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageTimeSettingTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			TimeConfigResourcePermission.Permission requiredPermission
					= TimeConfigResourcePermission.Permission.MANAGE_TIME_SETTING;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageTimeSetting = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new TimeConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageTimeSetting = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new TimeConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 作業分類の管理権限チェックを正しく行うことができることを確認する
	 */
	@isTest static void hasManageTimeWorkCategoryTest() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Standard');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			TimeConfigResourcePermission.Permission requiredPermission
					= TimeConfigResourcePermission.Permission.MANAGE_TIME_WORK_CATEGORY;

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			testData.permission.isManageTimeWorkCategory = true;
			permissionRepo.saveEntity(testData.permission);
			try {
				new TimeConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isManageTimeWorkCategory = false;
			permissionRepo.saveEntity(testData.permission);
			try {
				new TimeConfigResourcePermission().hasExecutePermission(requiredPermission);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}
}