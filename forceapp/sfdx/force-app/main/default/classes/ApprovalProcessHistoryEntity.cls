/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 承認
 *
 * プロセスインスタンス履歴のエンティティ
 */
public with sharing class ApprovalProcessHistoryEntity extends Entity {

	/** プロセスノードID */
	public Id processNodeId {get; set;}

	/** プロセスノード名 */
	public String processNodeName {get; set;}

	/** 作成日 */
	public AppDatetime createdDate {get; set;}

	/** 状況 */
	public String stepStatus {get; set;}

	/** 元のアクターID */
	public Id originalActorId {get; set;}

	/** 元のアクター名 */
	public String originalActorName {get; set;}

	/** アクターID */
	public Id actorId {get; set;}

	/** アクター名 */
	public String actorName {get; set;}

	/** コメント */
	public String comments {get; set;}

}