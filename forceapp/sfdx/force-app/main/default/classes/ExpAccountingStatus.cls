/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費
 *
 * 精算確定ステータス
 */
public with sharing class ExpAccountingStatus extends ValueObjectType {

	/** なし */
	public static final ExpAccountingStatus NONE = new ExpAccountingStatus('None');
	/** 承認済み */
	public static final ExpAccountingStatus AUTHORIZED = new ExpAccountingStatus('Authorized');
	/** 支払い済み */
	public static final ExpAccountingStatus FULLY_PAID = new ExpAccountingStatus('Fully Paid');
	/** 却下 */
	public static final ExpAccountingStatus REJECTED = new ExpAccountingStatus('Rejected');

	/** 精算確定ステータスのリスト（ステータスが追加されら本リストにも追加してください) */
	public static final List<ExpAccountingStatus> STATUS_LIST;
	static {
		STATUS_LIST = new List<ExpAccountingStatus> {
			NONE,
			AUTHORIZED,
			FULLY_PAID,
			REJECTED
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ExpAccountingStatus(String value) {
		super(value);
	}

	/**
	 * 値からExpAccountingStatusを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つExpAccountingStatus、ただし該当するExpAccountingStatusが存在しない場合はnull
	 */
	public static ExpAccountingStatus valueOf(String value) {
		ExpAccountingStatus retType = null;
		if (String.isNotBlank(value)) {
			for (ExpAccountingStatus type : STATUS_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ExpAccountingStatus以外の場合はfalse
		Boolean eq;
		if (compare instanceof ExpAccountingStatus) {
			// 値が同じであればtrue
			if (this.value == ((ExpAccountingStatus)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}