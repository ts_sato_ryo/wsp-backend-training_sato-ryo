/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Service class for expense type group master mainenance
 */
public with sharing class ExpTypeGroupService {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * Create a new expense type group
	 * @param newEntity Target entity
	 * @return Id of the created record
	 */
	public Id createNewExpTypeGroup(ExpTypeGroupEntity newEntity) {

		// create a new entity
		ExpTypeGroupEntity entity = newEntity.copy();

		// save
		return saveExpTypeGroup(entity);
	}

	/**
	 * Update an expense type group
	 * @param updateEntity Target entity(Id must be set)
	 */
	public void updateExpTypeGroup(ExpTypeGroupEntity updateEntity) {

		// check if the target record exists
		ExpTypeGroupEntity entity = (new ExpTypeGroupRepository()).getEntity(updateEntity.id);
		if (entity == null) {
			// the target record can not be found
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
		}

		// set the value to entity from the pameter entithy
		setUpdateValueToEntity(entity, updateEntity);

		// save
		saveExpTypeGroup(entity);
	}

	/**
	 * Delete an expense type group
	 * @param updateEntity Target entity(Id must be set)
	 */
	public void deleteExpTypeGroup(Id groupId) {

		// if exists expense type group or expense type whose parent is the target record, exception is thrown
		if (hasChildren(groupId)) {
			throw new App.IllegalStateException(MESSAGE.Com_Err_CannotDeleteReference);
		}

		try {
			// delete
			(new ExpTypeGroupRepository()).deleteEntity(groupId);
		} catch (DmlException e) {
			// if the record is already deleted(System.StatusCode.ENTITY_IS_DELETED), process is regarded as successful
			if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
				// TODO Output a debug log tentatively
				System.debug(e);

				// TODO Ultimately, it needs to check whether there are records referring to the target record
				// Message：Failed to delete. It may be referenced from other records.
				e.setMessage(MESSAGE.Com_Err_FaildDeleteReference);
				throw e;
			}
		}
	}

	/**
	 * Create or update an expense type gruop
	 * If the code is changed, the unique key also updated
	 * @param entity Target entity
	 * @return Id of the saved record
	 */
	private Id saveExpTypeGroup(ExpTypeGroupEntity entity) {

		// update unique key of the entity
		updateUniqKey(entity);

		// validate the entity
		validateEntity(entity);

		// check if the code is duplicated
		if (isDuplicatedCode(entity)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		if(entity.parentId!=null && entity.parentId==entity.id){
			throw new App.IllegalStateException(MESSAGE.Exp_Err_CannotBeParentOfItself);
		}

		// check if the specified parent id is valid
		if (entity.parentId != null) {
			// the target entity already has child groups(hierarchy can not be 3 levels or more) * update only
			if (entity.id != null && hasChildGroups(entity.id)) {
				throw new App.IllegalStateException(
						MESSAGE.Com_Err_MaxHierarchyOver(new List<String>{
							MESSAGE.Com_Lbl_ExpenseTypeGroup,
							String.valueOf(ExpTypeGroupEntity.PARENT_MAX_HIERARCHY + 1)}));
			}

			// get the parent entity
			ExpTypeGroupEntity parent = (new ExpTypeGroupRepository()).getEntity(entity.parentId);
			// can not be found
			if (parent == null) {
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_ParentExpenseTypeGroup}));
			}
			// parent id of the parent entity is set(hierarchy can not be 3 levels or more)
			if (parent.parentId != null) {
				throw new App.IllegalStateException(
						MESSAGE.Com_Err_MaxHierarchyOver(new List<String>{
							MESSAGE.Com_Lbl_ExpenseTypeGroup,
							String.valueOf(ExpTypeGroupEntity.PARENT_MAX_HIERARCHY + 1)}));
			}
			// validFrom of parent > validFrom of the target entity
			if(parent.validFrom.compareTo(entity.validFrom) > 0) {
				throw new App.IllegalStateException(MESSAGE.Exp_Err_ParentNeedsToBeBeforeChild);
			}
		}

		// save to object
		Repository.SaveResult saveResult = (new ExpTypeGroupRepository()).saveEntity(entity);

		return saveResult.details[0].id;
	}

	/**
	 * Set the update value to the entity
	 * @param toEntity The value in updateEntity will be set to this entity
	 * @param updateEntity Entity that have the update values
	 */
	private void setUpdateValueToEntity(ExpTypeGroupEntity toEntity, ExpTypeGroupEntity updateEntity) {

		// set only fields whose value have been changed
		for (ExpTypeGroupEntity.Field field : ExpTypeGroupEntity.Field.values()) {
			if (updateEntity.isChanged(field)) {
				toEntity.setFieldValue(field, updateEntity.getFieldValue(field));
			}
		}
		for (ValidPeriodEntityOld.Field field : ValidPeriodEntityOld.Field.values()) {
			if (updateEntity.isChanged(field)) {
				toEntity.setFieldValue(field, updateEntity.getFieldValue(field));
			}
		}
	}

	/**
	 * Update unique key of the entity(Do not update to Object)
	 * @param entity Target entity(code and companyId must be set)
	 */
	private void updateUniqKey(ExpTypeGroupEntity entity) {
		// get company
		CompanyEntity company = getCompany(entity.companyId);
		if (company == null) {
			// the company can not be found
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		entity.uniqKey = entity.createUniqKey(company.code);
	}

	/**
	 * Validate the entity
	 * Throws exception if the entity has invalid value
	 */
	private void validateEntity(ExpTypeGroupEntity entity) {
		Validator.Result result = (new ExpConfigValidator.ExpTypeGroupValidator(entity)).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * Check if the code is duplicated in the company
	 * @param entity Target entity(code and companyId must be set)
	 * @return Returns true if the same code already exists in the compnay. otherwise, returns false.
	 */
	private Boolean isDuplicatedCode(ExpTypeGroupEntity entity) {
		ExpTypeGroupEntity codeEntity = (new ExpTypeGroupRepository()).getEntityByCode(entity.code, entity.companyId);

		Boolean isDuplicated;
		if ((codeEntity != null) && (codeEntity.id != entity.id)) {
			return true;
		}
		return false;
	}

	/**
	 * Check if exists expense type group or expense type whose parent is the target record
	 * @param groupId Id of the target expense type group record
	 * @return Returns true if the target record has children. otherwise, returns false.
	 */
	private Boolean hasChildren(Id groupId) {

		// search for expense type group whose parent id is the target record
		if (hasChildGroups(groupId)) {
			return true;
		}

		// search for expense type whose parent group id is the target record
		List<ExpTypeEntity> expTypeList = new ExpTypeRepository().getEntityListByParentGroupId(new List<Id>{groupId});
		if (expTypeList != null && !expTypeList.isEmpty()) {
			return true;
		}

		return false;
	}

	/**
	 * Check if exists expense type group whose parent is the target record
	 * @param groupId Id of the target expense type group record
	 * @return Returns true if the target record has children. otherwise, returns false.
	 */
	private Boolean hasChildGroups(Id groupId) {

		// search for expense type group whose parent id is the target record
		List<ExpTypeGroupEntity> groupList = new ExpTypeGroupRepository().getEntityListByParentId(new List<Id>{groupId});
		if (groupList != null && !groupList.isEmpty()) {
			return true;
		}

		return false;
	}

	/** Cache data of company */
	private Map<Id, CompanyEntity> companyCache = new Map<Id, CompanyEntity>();
	/**
	 * Get company entity
	 * @param companyId Id of the target company
	 * @return Company entity
	 * @throws App.RecordNotFoundException Throws if the company can not be found
	 */
	private CompanyEntity getCompany(Id companyId) {
		CompanyEntity company = companyCache.get(companyId);
		if (company == null) {
			company = new CompanyRepository().getEntity(companyId);
			if (company == null) {
				// the company can not be found
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
			}
			companyCache.put(companyId, company);
		}
		return company;
	}
}