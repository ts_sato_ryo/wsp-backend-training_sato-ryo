/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * CostCenter RequiredFor picklist
 */
public with sharing class ComCostCenterRequiredFor extends ValueObjectType {
    
    /** Expense Report Only */
	public static final ComCostCenterRequiredFor EXPENSE_REPORT = new ComCostCenterRequiredFor('ExpenseReport');
	/** Expense Report and Request */
	public static final ComCostCenterRequiredFor EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ComCostCenterRequiredFor('ExpenseRequestAndExpenseReport');

	/** Entries */
	public static final List<ComCostCenterRequiredFor> TYPE_LIST;
	static {
		TYPE_LIST = new List<ComCostCenterRequiredFor> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}

	/**
	 * Constructor
	 * @param value
	 */
	private ComCostCenterRequiredFor(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
	 * @param The key value to get the instance
	 * @return The instance having the value specified
	 */
	public static ComCostCenterRequiredFor valueOf(String value) {
		ComCostCenterRequiredFor retType = null;
		if (String.isNotBlank(value)) {
			for (ComCostCenterRequiredFor type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {

		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ComCostCenterRequiredFor) {
			// 値が同じであればtrue
			if (this.value == ((ComCostCenterRequiredFor)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}
}
