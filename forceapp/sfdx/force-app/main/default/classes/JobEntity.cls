/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブエンティティ
 */
public with sharing class JobEntity extends JobGeneratedEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** ジョブ名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** ユニークキーの最大文字数 */
	public static final Integer UNIQ_KEY_MAX_LENGTH = 31;

	/** 親ジョブのリレーション名 */
	private static final String PARENT_R = ComJob__c.ParentId__c.getDescribe().getRelationshipName();
	/** 親ジョブIDの取得フラグ（検索した場合はtrueが設定される） */
	private static Boolean isSearchdHigherParentId;

	/**
	 * コンストラクタ
	 */
	public JobEntity() {
		super(new ComJob__c());
		isSearchdHigherParentId = false;
	}

	/**
	 * コンストラクタ
	 */
	public JobEntity(ComJob__c sobj) {
		super(sobj);
		if (sobj.ParentId__r != null && sobj.ParentId__r.getPopulatedFieldsAsMap().containsKey(PARENT_R)) {
			isSearchdHigherParentId = true;
		} else {
			isSearchdHigherParentId = false;
		}
	}

	/**
	 * @desctiprion 社員のルックアップ項目を表すクラス
	 */
	public class Employee {
		/** レコードのName項目値 */
		public AppPersonName fullName;
		/** コンストラクタ */
		public Employee(AppPersonName fullName) {
			this.fullName = fullName;
		}
	}

	/** ジョブ名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}

	/** 親ジョブ(参照専用) */
	public LookupField parent {
		get {
			if (parent == null) {
				parent = new ValidPeriodEntity.LookupField(new AppMultiString(
						sObj.ParentId__r.Name_L0__c,
						sObj.ParentId__r.Name_L1__c,
						sObj.ParentId__r.Name_L2__c));
			}
			return parent;
		}
		set {
			parent = value;
		}
	}

	/** 2階層親ジョブID */
	public Id level2ParentId {
		get {
			if (level2ParentId == null && isSearchdHigherParentId) {
				level2ParentId = sObj.ParentId__r.ParentId__c;
			}
			return level2ParentId;
		}
		set {
			level2ParentId = value;
		}
	}

	/** 3階層親ジョブID */
	public Id level3ParentId {
		get {
			if (level3ParentId == null && isSearchdHigherParentId) {
				level3ParentId = sObj.ParentId__r.ParentId__r.ParentId__c;
			}
			return level3ParentId;
		}
		set {
			level3ParentId = value;
		}
	}

	/** 4階層親ジョブID */
	public Id level4ParentId {
		get {
			if (level4ParentId == null && isSearchdHigherParentId) {
				level4ParentId = sObj.ParentId__r.ParentId__r.ParentId__r.ParentId__c;
			}
			return level4ParentId;
		}
		set {
			level4ParentId = value;
		}
	}

	/** 5階層親ジョブID */
	public Id level5ParentId {
		get {
			if (level5ParentId == null && isSearchdHigherParentId) {
				level5ParentId = sObj.ParentId__r.ParentId__r.ParentId__r.ParentId__r.ParentId__c;
			}
			return level5ParentId;
		}
		set {
			level5ParentId = value;
		}
	}

	/** 6階層親ジョブID */
	public Id level6ParentId {
		get {
			if (level6ParentId == null && isSearchdHigherParentId) {
				level6ParentId = sObj.ParentId__r.ParentId__r.ParentId__r.ParentId__r.ParentId__r.ParentId__c;
			}
			return level6ParentId;
		}
		set {
			level6ParentId = value;
		}
	}

	/** ジョブオーナー(参照専用) */
	public Employee jobOwner {
		get {
			if (jobOwner == null) {
				jobOwner = new Employee(new AppPersonName(
						sobj.JobOwnerBaseId__r.FirstName_L0__c, sObj.JobOwnerBaseId__r.LastName_L0__c,
						sObj.JobOwnerBaseId__r.FirstName_L1__c, sObj.JobOwnerBaseId__r.LastName_L1__c,
						sObj.JobOwnerBaseId__r.FirstName_L2__c, sObj.JobOwnerBaseId__r.LastName_L2__c));
			}
			return jobOwner;
		}
		set {
			jobOwner = value;
		}
	}

	/** ジョブタイプ(参照専用) */
	public LookupField jobType {
		get {
			if (jobType == null) {
				jobType = new ValidPeriodEntity.LookupField(new AppMultiString(
						sObj.JobTypeId__r.Name_L0__c,
						sObj.JobTypeId__r.Name_L1__c,
						sObj.JobTypeId__r.Name_L2__c));
			}
			return jobType;
		}
		set {
			jobType = value;
		}
	}

	/**
	 * エンティティの複製を作成する(IDはコピーされません)
	 */
	public JobEntity copy() {
		JobEntity copyEntity = new JobEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}
}
