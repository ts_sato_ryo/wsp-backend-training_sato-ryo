/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description JobAssignResourceのテストクラス
*/
@isTest
private class JobAssignResourceTest {

	/**
	 * テストで使用するマスタデータを登録する
	 */
	@testSetup
	static void setup() {
		ComCompany__c company = ComTestDataUtility.createCompany('テスト会社', null);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		User user = ComTestDataUtility.createUser('user', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('テスト社員', company.Id, null, user.Id, permission.Id);
		ComJobType__c jobType = ComTestDataUtility.createJobType('テストジョブタイプ', company.Id);
		ComJob__c job = ComTestDataUtility.createJob('Job', company.id, jobType.Id);
	}
	/** テスト用のユーザ */
	private static User getUser() {
		return [SELECT Id From User ORDER BY CreatedDate DESC LIMIT 1];
	}
	/** 社員ID */
	private static Id employeeId() {
		return [SELECT Id FROM ComEmpBase__c LIMIT 1].Id;
	}
	/** 会社ID */
	private static Id companyId() {
		return [SELECT Id FROM ComCompany__c LIMIT 1].Id;
	}
	/** 権限ID */
	private static Id permissionId() {
		return [SELECT Id FROM ComPermission__c LIMIT 1].Id;
	}
	/** ジョブタイプID */
	private static Id jobTypeId() {
		return [SELECT Id FROM ComJobType__c LIMIT 1].Id;
	}
	/** ジョブID */
	private static Id jobId() {
		return [SELECT Id FROM ComJob__c LIMIT 1].Id;
	}

	/**
	 * 社員割当レコードを登録出来ることを検証する
	 */
	@isTest
	static void createRecordTest() {
		// パラメータ作成
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId()};

		// API実行
		JobAssignResource.CreateResponse response;
		System.runAs(getUser()) {
			Test.startTest();
			JobAssignResource.CreateApi api = new JobAssignResource.CreateApi();
			response = (JobAssignResource.CreateResponse)api.execute(request);
			Test.stopTest();
		}

		// 登録レコード取得
		JobAssignEntity entity = new JobAssignRepository().getEntity(response.ids[0]);
		System.assertEquals(1, response.ids.size());
		System.assertEquals(jobId(), entity.jobId);
		System.assertEquals(employeeId(), entity.employeeBaseId);
		System.assertEquals(AppDate.newInstance(2018, 4, 1), entity.validFrom);
		System.assertEquals(AppDate.newInstance(2019, 4, 1), entity.validTo);
	}

	/**
	 * ジョブの管理権限を持たない場合、アプリ例外が発生することを検証する
	 */
	@isTest
	static void createRecordTestNoPermission() {
		// 権限を無効に設定する
		Id permissionId = permissionId();
		ComPermission__c permission = new ComPermission__c(id=permissionId);
		permission.ManageJob__c = false;
		update permission;
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		User stdUser = ComTestDataUtility.createStandardUser('テスト標準ユーザ');
		ComEmpBase__c stdEmp = ComTestDataUtility.createEmployeeWithHistory(
					'標準テストユーザー', companyId(), null, stdUser.id, permissionId);

		// パラメータ作成
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId()};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(stdUser) {
			try {
				JobAssignResource.CreateApi api = new JobAssignResource.CreateApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 失効日のデフォルト値で社員割当レコードを登録出来ることを検証する
	 */
	@isTest
	static void createRecordTestDafaultValidTo() {
		// パラメータ作成
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = '2018-04-01';
		request.employeeIds = new List<String>{employeeId()};

		// API実行
		JobAssignResource.CreateResponse response;
		System.runAs(getUser()) {
			Test.startTest();
			JobAssignResource.CreateApi api = new JobAssignResource.CreateApi();
			response = (JobAssignResource.CreateResponse)api.execute(request);
			Test.stopTest();
		}

		// 登録レコード取得
		JobAssignEntity entity = new JobAssignRepository().getEntity(response.ids[0]);
		System.assertEquals(1, response.ids.size());
		System.assertEquals(jobId(), entity.jobId);
		System.assertEquals(employeeId(), entity.employeeBaseId);
		System.assertEquals(AppDate.newInstance(2018, 4, 1), entity.validFrom);
		System.assertEquals(AppDate.newInstance(2101, 1, 1), entity.validTo);
	}

	/**
	 * パラメータが正常な場合、検証処理が正常終了することを検証する
	 */
	@isTest
	static void createRequestValidateTestValid() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId()};
		request.validate();
	}

	/**
	 * パラメータのジョブIDが未設定の場合、エラーが発生することを検証する
	 */
	@isTest
	static void createRequestValidateTestUnsetJobId() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = null;
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId()};
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'jobId'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * パラメータのジョブIDが不正な場合、エラーが発生することを検証する
	 */
	@isTest
	static void createRequestValidateTestInvalidJobId() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = 'invalidId';
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId()};
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'jobId', 'invalidId'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * パラメータの有効開始日が未設定の場合、エラーが発生することを検証する
	 */
	@isTest
	static void createRequestValidateTestUnsetValidDateFrom() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = null;
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId()};
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'validDateFrom'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * パラメータの有効開始日が不正なフォーマットの場合、エラーが発生することを検証する
	 */
	@isTest
	static void createRequestValidateTestInvalidValidateFrom() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = 'invalidDate';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId()};
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'validDateFrom', 'invalidDate'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * パラメータの失効日が不正なフォーマットの場合、エラーが発生することを検証する
	 */
	@isTest
	static void createRequestValidateTestInvalidValidTo() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = '2018-04-01';
		request.validDateTo = 'invalidDate';
		request.employeeIds = new List<String>{employeeId()};
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'validDateTo', 'invalidDate'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * パラメータの割当対象社員が未設定の場合、エラーが発生することを検証する
	 */
	@isTest
	static void createRequestValidateTestEmptyEmployeeIds() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>();
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'employeeIds'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * パラメータの割当対象社員が不正なIDの場合、エラーが発生することを検証する
	 */
	@isTest
	static void createRequestValidateTestInvalidEmployeeIds() {
		JobAssignResource.CreateRequest request = new JobAssignResource.CreateRequest();
		request.jobId = jobId();
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-04-01';
		request.employeeIds = new List<String>{employeeId(), 'invalid', employeeId()};
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'employeeId', 'invalid'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 社員割当レコードを更新出来ることを検証する
	 */
	 @isTest
	static void updateRecordTest() {
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				jobId(),
				employeeId(),
				AppDate.newInstance(2015, 4, 1),
				AppDate.newInstance(2016, 4, 1));

		// パラメータ作成
		JobAssignResource.UpdateRequest request = new JobAssignResource.UpdateRequest();
		request.ids = new List<Id>{jobAssign.id};
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-03-28';

		// API実行
		JobAssignResource.UpdateResponse response;
		System.runAs(getUser()) {
			Test.startTest();
			JobAssignResource.UpdateApi api = new JobAssignResource.UpdateApi();
			response = (JobAssignResource.UpdateResponse)api.execute(request);
			Test.stopTest();
		}

		// 登録レコード取得
		JobAssignEntity entity = new JobAssignRepository().getEntity(response.ids[0]);
		System.assertEquals(1, response.ids.size());
		System.assertEquals(AppDate.newInstance(2018, 4, 1), entity.validFrom);
		System.assertEquals(AppDate.newInstance(2019, 3, 28), entity.validTo);
	}

	/**
	 * ジョブの管理権限を持たない場合、アプリ例外が発生することを検証する
	 */
	 @isTest
	static void updateRecordTestNoPermission() {
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				jobId(),
				employeeId(),
				AppDate.newInstance(2015, 4, 1),
				AppDate.newInstance(2016, 4, 1));

		// 権限を無効に設定する
		Id permissionId = permissionId();
		ComPermission__c permission = new ComPermission__c(id=permissionId);
		permission.ManageJob__c = false;
		update permission;
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		User stdUser = ComTestDataUtility.createStandardUser('テスト標準ユーザ');
		ComEmpBase__c stdEmp = ComTestDataUtility.createEmployeeWithHistory(
					'標準テストユーザー', companyId(), null, stdUser.id, permissionId);

		// パラメータ作成
		JobAssignResource.UpdateRequest request = new JobAssignResource.UpdateRequest();
		request.ids = new List<Id>{jobAssign.id};
		request.validDateFrom = '2018-04-01';
		request.validDateTo = '2019-03-28';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(stdUser) {
			try {
				JobAssignResource.UpdateApi api = new JobAssignResource.UpdateApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 有効開始日が未設定の場合、エラーが発生することを検証する
	 */
	@isTest
	static void updateRequestTestEmptyValidDateFrom() {
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				jobId(),
				employeeId(),
				AppDate.newInstance(2018, 4, 1),
				AppDate.newInstance(2019, 4, 1));

		// パラメータ作成
		JobAssignResource.UpdateRequest request = new JobAssignResource.UpdateRequest();
		List<Id> updateTargetIds = new List<Id>();
		updateTargetIds.add(jobAssign.id);
		request.ids = updateTargetIds;
		request.validDateTo = '2019-03-28';

		// 検証
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'validDateFrom'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 更新対象のジョブ割当IDのリストが未設定の場合、エラーが発生することを検証する
	 */
	@isTest
	static void updateRecordTestEmptyIds() {
		// パラメータ作成
		JobAssignResource.UpdateRequest request = new JobAssignResource.UpdateRequest();
		request.validDateFrom = '2019-01-01';
		request.validDateTo = '2019-01-28';

		// 検証
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'ids'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 更新対象のジョブ割当IDのリストが不正なIDである場合、エラーが発生することを検証する
	 */
	@isTest
	static void updateRecordTestInvalidIds() {
		// パラメータ作成
		JobAssignResource.UpdateRequest request = new JobAssignResource.UpdateRequest();
		request.ids = new List<String>{'invalid'};// 不正なジョブ割当ID
		request.validDateFrom = '2019-01-01';
		request.validDateTo = '2019-01-28';

		// 検証
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'id', 'invalid'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 検索パラメータのジョブIDが不正な場合、エラーが発生する事を検証する
	 */
	@isTest
	static void searchRequestValidateTestInvalidJobId() {
		JobAssignResource.SearchRequest request = new JobAssignResource.SearchRequest();
		request.jobId = 'invalid';
		request.employeeId = employeeId();
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'jobId', 'invalid'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 検索パラメータの社員IDが不正な場合、エラーが発生する事を検証する
	 */
	@isTest
	static void searchRequestValidateTestInvalidEmployeeId() {
		JobAssignResource.SearchRequest request = new JobAssignResource.SearchRequest();
		request.jobId = jobId();
		request.employeeId = 'invalid';
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'employeeId', 'invalid'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * ジョブIDを条件に正しく検索できることを検証する
	 */
	@isTest
	static void searchApiTest() {
		// テストデータ作成
		ComJob__c otherJob = ComTestDataUtility.createJob('対象外ジョブ', companyId(), jobTypeId());
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(otherJob.Id, employeeId(), AppDate.today(), AppDate.today().addMonths(1));
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(jobId(), employeeId(), AppDate.today(), AppDate.today().addMonths(1));
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(otherJob.Id, employeeId(), AppDate.today(), AppDate.today().addMonths(1));

		// パラメータ作成
		JobAssignResource.SearchRequest param = new JobAssignResource.SearchRequest();
		param.jobId = jobId();

		// API実行
		JobAssignResource.SearchApi api = new JobAssignResource.SearchApi();
		JobAssignResource.SearchResponse response = new JobAssignResource.SearchResponse();
		System.runAs(getUser()) {
			Test.startTest();
			response = (JobAssignResource.SearchResponse)api.execute(param);
			Test.stopTest();
		}

		// 検証
		System.assertEquals(1, response.records.size());
		System.assertEquals(assign2.Id, response.records[0].id);
	}

	/**
	 * ジョブ割当有効開始日時点に、有効な社員履歴が存在しない場合、正しくレスポンスを生成できることを検証する
	 */
	@isTest
	static void searchApiCreateJobAssignTestInvalidEmployee() {
		// 社員エンティティ作成
		EmployeeBaseEntity employee = new EmployeeBaseEntity();
		employee.setId(employeeId());
		EmployeeHistoryEntity empHistory = new EmployeeHistoryEntity();
		empHistory.validFrom = AppDate.newInstance(2018, 4, 1);
		empHistory.validTo = AppDate.newInstance(2018, 5, 1);
		employee.addHistory(empHistory);
		List<EmployeeBaseEntity> employees = new List<EmployeeBaseEntity>{employee};

		// 部署
		DepartmentBaseEntity department = new DepartmentBaseEntity();
		department.setId(createId(ComDeptBase__c.sObjectType));
		department.code = 'dept';
		DepartmentHistoryEntity deptHistory = new DepartmentHistoryEntity();
		deptHistory.nameL0 = '0';
		deptHistory.nameL1 = '1';
		deptHistory.nameL2 = '2';
		deptHistory.validFrom = AppDate.newInstance(2018, 5, 1);
		deptHistory.validTo = AppDate.newInstance(2019, 4, 1);
		List<DepartmentBaseEntity> departments = new List<DepartmentBaseEntity>{department};

		// ジョブ割当作成
		JobAssignEntity assign = new JobAssignEntity();
		assign.setId(createId(ComJobAssign__c.sObjectType));
		assign.employeeBaseId = employeeId();
		assign.setEmployeeCode('emp');
		assign.setEmployeeNameL(new AppPersonName('1', '2', '3', '4', '5', '6'));
		assign.validFrom = AppDate.newInstance(2018, 4, 1);
		assign.validTo = AppDate.newInstance(2019, 4, 1);
		List<JobAssignEntity> assigns = new List<JobAssignEntity>{assign};

		JobAssignResource.SearchApi api = new JobAssignResource.SearchApi();
		List<JobAssignResource.JobAssign> result = api.createJobAssign(assigns, employees, departments);
		System.assertEquals(1, result.size());
		System.assertEquals(assign.id, result[0].id);
		System.assertEquals(employee.id, result[0].employeeId);
		System.assertEquals('emp', result[0].employeeCode);
		System.assertEquals(assign.employeeNameL.getFullName(), result[0].employeeName);
		System.assertEquals(null, result[0].departmentId);
		System.assertEquals(null, result[0].departmentCode);
		System.assertEquals(null, result[0].departmentName);
		System.assertEquals(Date.newInstance(2018, 4, 1), result[0].validDateFrom);
		System.assertEquals(Date.newInstance(2019, 4, 1), result[0].validDateTo);
	}

	/**
	 * 社員履歴に部署が未登録の場合、正しくレスポンスを生成できることを検証する
	 */
	@isTest
	static void searchApiCreateJobAssignTestNoDepartmentBase() {
		// 社員エンティティ作成
		EmployeeBaseEntity employee = new EmployeeBaseEntity();
		employee.setId(employeeId());
		EmployeeHistoryEntity history = new EmployeeHistoryEntity();
		history.validFrom = AppDate.newInstance(2018, 4, 1);
		history.validTo = AppDate.newInstance(2019, 4, 1);
		employee.addHistory(history);
		List<EmployeeBaseEntity> employees = new List<EmployeeBaseEntity>{employee};

		// ジョブ割当作成
		JobAssignEntity assign = new JobAssignEntity();
		assign.setId(createId(ComJobAssign__c.sObjectType));
		assign.jobId = jobId();
		assign.employeeBaseId = employeeId();
		assign.setEmployeeCode('emp');
		assign.setEmployeeNameL(new AppPersonName('1', '2', '3', '4', '5', '6'));
		assign.validFrom = history.validFrom;
		assign.validTo =  history.validTo;
		List<JobAssignEntity> assigns = new List<JobAssignEntity>{assign};

		// 部署
		List<DepartmentBaseEntity> departments = new List<DepartmentBaseEntity>();

		JobAssignResource.SearchApi api = new JobAssignResource.SearchApi();
		List<JobAssignResource.JobAssign> result = api.createJobAssign(assigns, employees, departments);
		System.assertEquals(1, result.size());
		System.assertEquals(assign.id, result[0].id);
		System.assertEquals(employee.id, result[0].employeeId);
		System.assertEquals('emp', result[0].employeeCode);
		System.assertEquals(assign.employeeNameL.getFullName(), result[0].employeeName);
		System.assertEquals(null, result[0].departmentId);
		System.assertEquals(null, result[0].departmentCode);
		System.assertEquals(null, result[0].departmentName);
		System.assertEquals(Date.newInstance(2018, 4, 1), result[0].validDateFrom);
		System.assertEquals(Date.newInstance(2019, 4, 1), result[0].validDateTo);
	}

	/**
	 * ジョブ割当有効開始日時点の、部署履歴を正しく取得できることを検証する
	 */
	@isTest
	static void searchApiCreateJobAssignTestValidDepartment() {
		// テスト用の部署IDを作成
		Id deptId = createId(ComDeptBase__c.sObjectType);

		// 社員エンティティ作成
		EmployeeBaseEntity employee = new EmployeeBaseEntity();
		employee.setId(employeeId());
		EmployeeHistoryEntity history1 = new EmployeeHistoryEntity();
		history1.validFrom = AppDate.newInstance(2018, 4, 1);
		history1.validTo = AppDate.newInstance(2018, 5, 1);
		history1.departmentId = deptId;
		employee.addHistory(history1);
		EmployeeHistoryEntity history2 = new EmployeeHistoryEntity();
		history2.validFrom = AppDate.newInstance(2018, 5, 1);
		history2.validTo = AppDate.newInstance(2018, 6, 1);
		history2.departmentId = deptId;
		employee.addHistory(history2);
		List<EmployeeBaseEntity> employees = new List<EmployeeBaseEntity>{employee};

		// ジョブ割当作成
		JobAssignEntity assign1 = new JobAssignEntity();
		assign1.employeeBaseId = employeeId();
		assign1.validFrom = AppDate.newInstance(2018, 4, 15);
		assign1.validTo = AppDate.newInstance(2018, 5, 15);
		JobAssignEntity assign2 = new JobAssignEntity();
		assign2.employeeBaseId = employeeId();
		assign2.validFrom = AppDate.newInstance(2018, 5, 15);
		assign2.validTo = AppDate.newInstance(2101, 1, 1);
		List<JobAssignEntity> assigns = new List<JobAssignEntity>{assign1, assign2};

		// 部署
		DepartmentBaseEntity dept = new DepartmentBaseEntity();
		dept.setId(deptId);
		dept.code = 'dept';
		DepartmentHistoryEntity deptHistory1 = new DepartmentHistoryEntity();
		deptHistory1.validFrom = AppDate.newInstance(2018, 4, 15);
		deptHistory1.validTo = AppDate.newInstance(2018, 5, 15);
		deptHistory1.nameL0 = '1-0';
		deptHistory1.nameL1 = '1-1';
		deptHistory1.nameL2 = '1-2';
		dept.addHistory(deptHistory1);
		DepartmentHistoryEntity deptHistory2 = new DepartmentHistoryEntity();
		deptHistory2.validFrom = AppDate.newInstance(2018, 5, 15);
		deptHistory2.validTo = AppDate.newInstance(2101, 1, 11);
		deptHistory2.nameL0 = '2-0';
		deptHistory2.nameL1 = '2-1';
		deptHistory2.nameL2 = '2-2';
		dept.addHistory(deptHistory2);
		List<DepartmentBaseEntity> departments = new List<DepartmentBaseEntity>{dept};

		// 正しく履歴を取得したことを検証する
		JobAssignResource.SearchApi api = new JobAssignResource.SearchApi();
		List<JobAssignResource.JobAssign> result = api.createJobAssign(assigns, employees, departments);
		System.assertEquals(2, result.size());
		System.assertEquals(deptId, result[0].departmentId);
		System.assertEquals('dept', result[0].departmentCode);
		System.assertEquals(deptHistory1.nameL.getValue(), result[0].departmentName);
		System.assertEquals(deptId, result[1].departmentId);
		System.assertEquals('dept', result[1].departmentCode);
		System.assertEquals(deptHistory2.nameL.getValue(), result[1].departmentName);
	}

	/**
	 * 社員割当レコードを削除出来ることを検証する
	 */
	@isTest
	static void deleteRecordTest() {
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				jobId(),
				employeeId(),
				AppDate.newInstance(2015, 4, 1),
				AppDate.newInstance(2016, 4, 1));

		// パラメータ作成
		JobAssignResource.DeleteRequest request = new JobAssignResource.DeleteRequest();
		request.ids = new List<Id>{jobAssign.id};

		// API実行
		JobAssignResource.DeleteResponse response;
		System.runAs(getUser()) {
			Test.startTest();
			JobAssignResource.DeleteApi api = new JobAssignResource.DeleteApi();
			response = (JobAssignResource.DeleteResponse)api.execute(request);
			Test.stopTest();
		}

		// 登録レコード取得
		JobAssignEntity entity = new JobAssignRepository().getEntity(jobAssign.id);
		System.assertEquals(null, entity);
	}

	/**
	 * ジョブの管理権限を持たない場合、アプリ例外が発生することを検証する
	 */
	@isTest
	static void deleteRecordTestNoPermission() {
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				jobId(),
				employeeId(),
				AppDate.newInstance(2015, 4, 1),
				AppDate.newInstance(2016, 4, 1));

		// 権限を無効に設定する
		Id permissionId = permissionId();
		ComPermission__c permission = new ComPermission__c(id=permissionId);
		permission.ManageJob__c = false;
		update permission;
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		User stdUser = ComTestDataUtility.createStandardUser('テスト標準ユーザ');
		ComEmpBase__c stdEmp = ComTestDataUtility.createEmployeeWithHistory(
					'標準テストユーザー', companyId(), null, stdUser.id, permissionId);

		// パラメータ作成
		JobAssignResource.DeleteRequest request = new JobAssignResource.DeleteRequest();
		request.ids = new List<Id>{jobAssign.id};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(stdUser) {
			try {
				JobAssignResource.DeleteApi api = new JobAssignResource.DeleteApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 削除対象のジョブ割当IDのリストが未設定の場合、エラーが発生することを検証する
	 */
	@isTest
	static void deleteRequestTestEmptyIds() {
		// パラメータ作成
		JobAssignResource.DeleteRequest request = new JobAssignResource.DeleteRequest();

		// 検証
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'ids'});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 削除対象のジョブ割当IDのリストが不正なIDである場合、エラーが発生することを検証する
	 */
	@isTest
	static void deleteRequestTestInvalidIds() {
		// パラメータ作成
		JobAssignResource.DeleteRequest request = new JobAssignResource.DeleteRequest();
		request.ids = new List<String>{'invalid'};// 不正なジョブ割当ID

		// 検証
		try {
			request.validate();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'id', 'invalid'});
			System.assertEquals(expected, e.getMessage());
		}
	}


	/** テスト用のIDのカウンタ */
	private static Integer idSeq = 0;

	/**
	 * 指定したオブジェクトのダミーのIDを作成する
	 * テスト用にエンティティの値を設定したい場合のみ使用すること
	 * TODO チーム内で共有後、共通クラスへ移行する
	 * @param objType
	 * @return 生成したダミーのID
	 */
	private static Id createId(Schema.SObjectType objType) {
		String cnt = String.valueOf(idSeq++);
		String sufix = '0'.repeat(15 - cnt.length()) + cnt;
		return objType.getDescribe().getKeyPrefix() + sufix;
	}

}