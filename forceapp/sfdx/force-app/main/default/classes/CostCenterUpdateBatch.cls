/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通 Common
 *
 * @description 既存コストセンター更新バッチ処理 Existing Cost Center Update Batch Processing
 */
public with sharing class CostCenterUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {

	/** インポートバッチID */
	private Id importBatchId;

	/** 既存の会社データ */
	@TestVisible
	private Map<String, CompanyEntity> companyMap;
	/** 既存のコストセンターデータ */
	@TestVisible
	// キーは会社コード・コストセンターコード Company Code => Cost Center Code Key-Value pair
	private Map<List<String>, CostCenterBaseEntity> costCenterMap;

	/** 成功件数 */
	private Integer successCount = 0;
	/** 失敗件数 */
	private Integer errorCount = 0;
	/** 例外発生 */
	private Boolean exceptionOccurred = false;


	/**
	 * コンストラクタ Constructor
	 * @param importBatchId インポートバッチID
	 */
	public CostCenterUpdateBatch(Id importBatchId) {
		this.importBatchId = importBatchId;
	}

	/**
	 * バッチ開始処理 Start batch processing
	 * @param bc バッチコンテキスト Batch Context
	 * @return バッチ処理対象レコード Records to be processed in the batch
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {
		// コストセンターインポートデータを取得 Retrieve Cost Center Import data
		String query = 'SELECT Id, ' +
							'Name, ' +
							'ImportBatchId__c, ' +
							'RevisionDate__c, ' +
							'ValidTo__c, ' +
							'Code__c, ' +
							'CompanyCode__c, ' +
							'Name_L0__c, ' +
							'Name_L1__c, ' +
							'Name_L2__c, ' +
							'ParentBaseCode__c, ' +
							'HistoryComment__c, ' +
							'LinkageCode__c, ' +
							'Status__c, ' +
							'Error__c ' +
						'FROM ComCostCenterImport__c ' +
						'WHERE ImportBatchId__c = :importBatchId ' +
						'AND Status__c IN (\'Waiting\', \'Error\') ' +
						'ORDER BY Code__c, RevisionDate__c, ValidTo__c, CreatedDate';

		return Database.getQueryLocator(query);
	}

	/**
	 * バッチ実行処理 Execute batch processing
	 * @param bc バッチコンテキスト Batch Context
	 * @param scope バッチ処理対象レコードのリスト List of Cost Center Import records to be updated
	 */
	public void execute(Database.BatchableContext bc, List<ComCostCenterImport__c> scope) {
		this.companyMap = new Map<String, CompanyEntity>();
		this.costCenterMap = new Map<List<String>, CostCenterBaseEntity>();

		List<CostCenterImportEntity> costCenterImpList = new List<CostCenterImportEntity>();
		List<CostCenterImportEntity> costCenterImpValidPeriodList = new List<CostCenterImportEntity>();

		Set<String> companyCodeSet = new Set<String>();
		Set<String> costCenterCodeSet = new Set<String>();
		for (ComCostCenterImport__c obj : scope) {

			CostCenterImportEntity entity = this.createCostCenterImpEntity(obj);

			// 失効日が設定されている場合は失効日の更新を行う If ValidTo value is set, update the ValidTo field
			// 失効日と改定日が両方設定されている場合は失効日が優先される If existing ValidTo value is set, the ValidTo from CostCenterImport object takes precedence
			if (entity.validTo != null) {
				costCenterImpValidPeriodList.add(entity);
			} else {
				costCenterImpList.add(entity);
			}

			// 参照先のコードを取得する Retrieve all codes
			String code = ImportBatchService.convertValue(entity.code);
			if (String.isNotEmpty(code)) {
				costCenterCodeSet.add(code);
			}
			String companyCode = ImportBatchService.convertValue(entity.companyCode);
			if (String.isNotEmpty(companyCode)) {
				companyCodeSet.add(companyCode);
			}
			String parentBaseCode = ImportBatchService.convertValue(entity.parentBaseCode);
			if (String.isNotEmpty(parentBaseCode)) {
				costCenterCodeSet.add(parentBaseCode);
			}
		}

		// 参照先のデータを取得 Retreive reference data from code
		// 会社 Company
		if (companyCodeSet.size() > 0) {
			CompanyRepository companyRepo = new CompanyRepository();
			for (CompanyEntity company : companyRepo.getEntityList(new List<String>(companyCodeSet))) {
				this.companyMap.put(company.code, company);
			}
		}
		// コストセンター Cost Center
		CostCenterService costCenterService = new CostCenterService();
		if (costCenterCodeSet.size() > 0) {
			for (ParentChildBaseEntity costCenter : costCenterService.getEntityListByCode(new List<String>(costCenterCodeSet))) {
				List<String> key = new List<String>{((CostCenterBaseEntity)costCenter).company.code, ((CostCenterBaseEntity)costCenter).code};
				this.costCenterMap.put(key, (CostCenterBaseEntity)costCenter);
			}
		}

		// コストセンターデータの更新 Update Cost Center Records
		updateCostCenterList(costCenterImpList);

		// コストセンターの有効期間終了日の更新 Update ValidTo field of Cost Center records
		updateValidEndDateList(costCenterImpValidPeriodList);
	}

	/**
	 * バッチ終了処理 Finish batch processing
	 * @param bc バッチコンテキスト Batch Context
	 */
	public void finish(Database.BatchableContext bc) {
		// インポートバッチデータを更新 Update ImportBatch records
		ImportBatchRepository batchRepo = new ImportBatchRepository();
		ImportBatchEntity batch = batchRepo.getEntity(this.importBatchId);
		batch.status = this.exceptionOccurred ? ImportBatchStatus.FAILED : ImportBatchStatus.PROCESSING;
		batch.successCount += this.successCount;
		batch.failureCount += this.errorCount;
		batchRepo.saveEntity(batch);
	}

	//--------------------------------------------------------------------------------

	/**
	 * コストセンターインポートエンティティを作成する Create CostCenterImportEntity
	 * @param obj コストセンターインポートオブジェクトデータ Cost Center Import object record
	 * @return コストセンターインポートエンティティ CostCenterImportEntity
	 */
	private CostCenterImportEntity createCostCenterImpEntity(ComCostCenterImport__c obj) {
		CostCenterImportEntity entity = new CostCenterImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.validTo = AppDate.valueOf(obj.ValidTo__c);
		entity.code = obj.Code__c;
		entity.companyCode = obj.CompanyCode__c;
		entity.name_L0 = obj.Name_L0__c;
		entity.name_L1 = obj.Name_L1__c;
		entity.name_L2 = obj.Name_L2__c;
		entity.parentBaseCode = obj.ParentBaseCode__c;
		entity.historyComment = obj.HistoryComment__c;
		entity.linkageCode = obj.LinkageCode__c;
		entity.status = ImportStatus.valueOf(obj.Status__c);
		entity.error = obj.Error__c;

		entity.resetChanged();
		return entity;
	}

	/**
	 * バリデーションを行う(コストセンターデータ更新用) Validates CostCenterImportEntity for update
	 */
	@TestVisible
	private void validateCostCenterImpEntityForUpdate(CostCenterImportEntity costCenterImp) {
		// 既存データ Existing data
		List<String> costCenterMapKey = new List<String>{costCenterImp.companyCode, costCenterImp.code};
		CostCenterBaseEntity costCenter = this.costCenterMap.get(costCenterMapKey);

		// 改訂日が未設定 Checks if Revision Date value is set
		if (costCenterImp.revisionDate == null) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_RevisionDate});
			this.errorCount++;
			return;
		}

		// コストセンター名(L0)がクリアされている Cost Center Name(L0) is cleared
		String name_L0 = ImportBatchService.convertValue(costCenterImp.name_L0);
		if (String.isNotBlank(costCenterImp.name_L0) && String.isBlank(name_L0)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Batch_Err_CanNotClear(new List<String>{ComMessage.msg().Batch_Lbl_CostCenterName});
			this.errorCount++;
			return;
		}

		String companyCode = ImportBatchService.convertValue(costCenterImp.companyCode);

		// 親コストセンター Parent Cost Center
		String parentBaseCode = ImportBatchService.convertValue(costCenterImp.parentBaseCode);
		if (String.isNotBlank(parentBaseCode)) {
			List<String> key = new List<String>{companyCode, parentBaseCode};

			// 存在しない Does not exist
			if (!this.costCenterMap.containsKey(key)) {
				costCenterImp.status = ImportStatus.Error;
				costCenterImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_ParentCostCenterCode});
				return;
			}
			// 有効開始日 > 改訂日 Valid From > Revision Date
			if (this.costCenterMap.containsKey(key) && this.costCenterMap.get(key).getSuperHistoryByDate(costCenterImp.revisionDate) == null) {
				costCenterImp.status = ImportStatus.Error;
				costCenterImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_ParentCostCenterCode});
				return;
			}
		}
	}

	/**
	 * バリデーションを行う(有効期間更新用) Validates Valid From value
	 */
	@TestVisible
	private void validateCostCenterImpEntityForValidPeriod(CostCenterImportEntity costCenterImp) {

		// 会社コードが未設定 Company Code not set
		String companyCode = ImportBatchService.convertValue(costCenterImp.companyCode);
		if (String.isBlank(companyCode)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			return;
		}
		// 指定した会社が存在しない Company does not exist
		if (!this.companyMap.containsKey(companyCode)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			return;
		}

		// コストセンターコードが未設定 Cost Center Code not set
		String costCenterCode = ImportBatchService.convertValue(costCenterImp.code);
		if (String.isBlank(costCenterCode)) {
			costCenterImp.status = ImportStatus.Error;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CostCenterCode});
			return;
		}
		// 指定したコストセンターが存在しない Cost Center does not exist
		List<String> costCenterMapKey = new List<String>{companyCode, costCenterCode};
		if (!this.costCenterMap.containsKey(costCenterMapKey)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CostCenterCode});
			return;
		}

		// 改訂日が未設定 Checks if Revision Date value is set
		if (costCenterImp.revisionDate == null) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_RevisionDate});
			this.errorCount++;
			return;
		}
	}

	/**
	 * コストセンターエンティティを作成する（ベース）Creates Cost Center Entity (Base)
	 */
	private CostCenterBaseEntity createCostCenterBaseEntity(CostCenterImportEntity costCenterImp) {
		CostCenterBaseEntity base = new CostCenterBaseEntity();

		String companyCode = ImportBatchService.convertValue(costCenterImp.companyCode);
		String code = ImportBatchService.convertValue(costCenterImp.code);
		List<String> costCenterMapKey = new List<String>{companyCode, code};
		base.setId(this.costCenterMap.get(costCenterMapKey).id);
		base.code = code;
		base.uniqKey = base.createUniqKey(companyCode, base.code);
		base.companyId = this.companyMap.get(companyCode).id;
		if (String.isNotBlank(costCenterImp.name_L0)) {
			base.name = ImportBatchService.convertValue(costCenterImp.name_L0);
		}

		return base;
	}

	/**
	 * コストセンターエンティティを作成する（履歴）Creates Cost Center Entity (History)
	 */
	private CostCenterHistoryEntity createCostCenterHistoryEntity(CostCenterImportEntity costCenterImp, boolean updateValidTo) {
		CostCenterHistoryEntity history = new CostCenterHistoryEntity();

		String companyCode = ImportBatchService.convertValue(costCenterImp.companyCode);
		String code = ImportBatchService.convertValue(costCenterImp.code);
		List<String> costCenterMapKey = new List<String>{companyCode, code};
		history.baseId = this.costCenterMap.get(costCenterMapKey).id;
		if (String.isNotBlank(costCenterImp.name_L0)) {
			history.name = ImportBatchService.convertValue(costCenterImp.name_L0);
			history.nameL0 = ImportBatchService.convertValue(costCenterImp.name_L0);
		}
		if (String.isNotBlank(costCenterImp.name_L1)) {
			history.nameL1 = ImportBatchService.convertValue(costCenterImp.name_L1);
		}
		if (String.isNotBlank(costCenterImp.name_L2)) {
			history.nameL2 = ImportBatchService.convertValue(costCenterImp.name_L2);
		}
		if (String.isNotBlank(costCenterImp.parentBaseCode)) {
			String parentBaseCode = ImportBatchService.convertValue(costCenterImp.parentBaseCode);
			if (parentBaseCode != null) {
				costCenterMapKey = new List<String>{companyCode, parentBaseCode};
				history.parentBaseId = this.costCenterMap.get(costCenterMapKey).id;
			} else {
				history.parentBaseId = null;
			}
			history.parentBaseCode = parentBaseCode;
		}
		if (String.isNotBlank(costCenterImp.linkageCode)) {
			history.linkageCode = ImportBatchService.convertValue(costCenterImp.linkageCode);
		}

		// 項目に値が設定されたかをチェック Check if value is set
		Boolean isChanged = false;
		for (Schema.SObjectField field : history.getChangedFieldSet()) {
			if (! ParentChildHistoryEntity.HISTORY_MANAGEMENT_FIELD_SET.contains(field.getDescribe().getName())) {
				isChanged = true;
				break;
			}
		}
		// 値が全く設定されていない場合はnullを返却（改訂処理は行わない） If value is not set, record is not processed and return null
		if (!updateValidTo && !isChanged) {
			return null;
		}

		// 履歴コメントは必ず設定する History Comment is required
		history.historyComment = ImportBatchService.convertValue(costCenterImp.historyComment);
		history.validFrom = costCenterImp.revisionDate;

		// Sets the Valid To value to max if value is not defined by user
		if (costCenterImp.validTo == null) {
			history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
		} else {
			history.validTo = costCenterImp.validTo;
		}

		return history;
	}

	/**
	 * コストセンターマスタを更新する Updates Cost Center
	 * @param costCenterImpList 更新対象のコストセンターインポートデータ CostCenterImportEntity to be updated
	 */
	private void updateCostCenterList(List<CostCenterImportEntity> costCenterImpList) {
		Savepoint sp = Database.setSavepoint();

		// 既存データの更新 Update existing records
		CostCenterService costCenterService = new CostCenterService();
		for (CostCenterImportEntity costCenterImp : costCenterImpList) {
			try {
				// 既存データが存在しない or エラーがある場合は何もしない Do not do anything if existing data does not exist or there is an error
				String companyCode = ImportBatchService.convertValue(costCenterImp.companyCode);
				String costCenterCode = ImportBatchService.convertValue(costCenterImp.code);
				List<String> costCenterMapKey = new List<String>{companyCode, costCenterCode};
				if (!this.costCenterMap.containsKey(costCenterMapKey) || ImportStatus.Error.equals(costCenterImp.status)) {
					continue;
				}

				// バリデーション Validation
				this.validateCostCenterImpEntityForUpdate(costCenterImp);

				// エラーがある場合は何もしない Do not do anything if there is an error
				if (ImportStatus.Error.equals(costCenterImp.status)) {
					this.errorCount++;
					continue;
				}

				// 更新用のコストセンターエンティティを作成 Create CostCenterEntities for updating
				CostCenterBaseEntity costCenterBase = this.createCostCenterBaseEntity(costCenterImp);
				CostCenterHistoryEntity costCenterHistory = this.createCostCenterHistoryEntity(costCenterImp, false);

				// 更新 Perform update
				costCenterService.updateBase(costCenterBase);
				if (costCenterHistory != null) {
					costCenterService.saveCostCenterHistoryRecord(costCenterHistory);
				}

				costCenterImp.status = ImportStatus.SUCCESS;
				costCenterImp.error = null;
				this.successCount++;
			} catch (App.BaseException e) {
				costCenterImp.status = ImportStatus.ERROR;
				costCenterImp.error = e.getMessage();
				this.errorCount++;
			} catch (Exception e) {
				System.debug('*** ERROR e=' + e);
				System.debug('*** ERROR e=' + e.getStackTraceString());
				Database.rollback(sp);
				costCenterImp.status = ImportStatus.ERROR;
				costCenterImp.error = e.getMessage();
				this.errorCount++;
				this.exceptionOccurred = true;
				break;
			}
		}

		// 予期せぬ例外が発生した場合は成功したインポートデータのステータスを戻す If an exception occurs, revert the status of the CostCenterImport records
		if (this.exceptionOccurred) {
			for (CostCenterImportEntity costCenterImp : costCenterImpList) {
				if (ImportStatus.SUCCESS.equals(costCenterImp.status)) {
					costCenterImp.status = ImportStatus.WAITING;
					this.successCount--;
				}
			}
		}

		// コストセンターインポートデータを更新 Update CostCenterImport records
		CostCenterImportRepository costCenterImpRepo = new CostCenterImportRepository();
		costCenterImpRepo.saveEntityList(costCenterImpList);
	}

	/**
	 * コストセンターマスタの有効期間終了日を更新する Updates Valid To value of Cost Center record
	 * @param costCenterImpList 更新対象のコストセンターインポートデータ CostCenterImportEntity to be updated
	 */
	private void updateValidEndDateList(List<CostCenterImportEntity> costCenterImpList) {
		Savepoint sp = Database.setSavepoint();

		// 有効期間終了日の更新 Update Valid To value
		CostCenterService costCenterService = new CostCenterService();
		for (CostCenterImportEntity costCenterImp : costCenterImpList) {
			try {
				// バリデーション Validation
				this.validateCostCenterImpEntityForValidPeriod(costCenterImp);

				// バリデーションエラーがある場合は失敗件数をインクリメント If validation error occurs, increment errorCount
				if (ImportStatus.Error.equals(costCenterImp.status)) {
					this.errorCount++;
				}

				// 既存データが存在しない or エラーがある場合は何もしない Do not do anything if existing data does not exist or there is an error
				String companyCode = ImportBatchService.convertValue(costCenterImp.companyCode);
				String costCenterCode = ImportBatchService.convertValue(costCenterImp.code);
				List<String> costCenterMapKey = new List<String>{companyCode, costCenterCode};
				if (!this.costCenterMap.containsKey(costCenterMapKey) || ImportStatus.Error.equals(costCenterImp.status)) {
					continue;
				}
				// 有効期間終了日を更新 Update Valid To value by calling CostCenterService.saveCostCenterHistoryRecord
				CostCenterHistoryEntity costCenterHistory = this.createCostCenterHistoryEntity(costCenterImp, true);
				costCenterService.saveCostCenterHistoryRecord(costCenterHistory);

				costCenterImp.status = ImportStatus.SUCCESS;
				costCenterImp.error = null;
				this.successCount++;
			} catch (App.BaseException e) {
				costCenterImp.status = ImportStatus.ERROR;
				costCenterImp.error = e.getMessage();
				this.errorCount++;
			} catch (Exception e) {
				System.debug('*** ERROR e=' + e);
				System.debug('*** ERROR e=' + e.getStackTraceString());
				Database.rollback(sp);
				costCenterImp.status = ImportStatus.ERROR;
				costCenterImp.error = e.getMessage();
				this.errorCount++;
				this.exceptionOccurred = true;
				break;
			}
		}

		// 予期せぬ例外が発生した場合は成功したインポートデータのステータスを戻す If an exception occurs, revert the status of the CostCenterImport records
		if (this.exceptionOccurred) {
			for (CostCenterImportEntity costCenterImp : costCenterImpList) {
				if (ImportStatus.SUCCESS.equals(costCenterImp.status)) {
					costCenterImp.status = ImportStatus.WAITING;
					this.successCount--;
				}
			}
		}

		// コストセンターインポートデータを更新 Update CostCenterImport records
		CostCenterImportRepository costCenterImpRepo = new CostCenterImportRepository();
		costCenterImpRepo.saveEntityList(costCenterImpList);
	}
}