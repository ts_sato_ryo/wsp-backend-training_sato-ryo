/**
 * AppDateRangeのテストクラス
 */
@isTest
private class AppDateRangeTest {

	/**
	 * コンストラクタで指定した日付がnullの場合、例外が発生することを検証する
	 */
	@isTest
	static void invalidParameterNull() {
		try {
			new AppDateRange(null, AppDate.newInstance(2018, 1, 1));
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
		}
		try {
			new AppDateRange(AppDate.newInstance(2018, 1, 1), null);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
		}
	}

	/**
	 * コンストラクタで指定した日付の前後関係が逆の場合、例外が発生することを検証する
	 */
	@isTest
	static void invalidParameterInvalidDate() {
		try {
			new AppDateRange(AppDate.newInstance(2018, 1, 2), AppDate.newInstance(2018, 1, 1));
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
		}
	}

	/**
	 * オブジェクトの開始日、終了日を正しく取得できることを検証する
	 */
	@isTest
	static void getValuesTest() {
		AppDateRange range = new AppDateRange(AppDate.newInstance(2018, 1, 2), AppDate.newInstance(2019, 3, 4));
		System.assertEquals(AppDate.newInstance(2018, 1, 2), range.startDate);
		System.assertEquals(AppDate.newInstance(2019, 3, 4), range.endDate);
	}

	/**
	 * オブジェクトの比較を検証する
	 */
	@isTest
	static void equalsAndHashCodeTest() {
		// 同じ値のオブジェクト
		AppDateRange range1 = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 12, 31));
		AppDateRange range2 = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 12, 31));
		System.assert(range1.equals(range2));
		System.assert(range2.equals(range1));
		System.assertEquals(range1.hashCode(), range2.hashCode());

		// 開始日が異なるオブジェクト
		range1 = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 12, 31));
		range2 = new AppDateRange(AppDate.newInstance(2018, 1, 2), AppDate.newInstance(2019, 12, 31));
		System.assert(!range1.equals(range2));
		System.assert(!range2.equals(range1));
		System.assertNotEquals(range1.hashCode(), range2.hashCode());

		// 終了日が異なるオブジェクト
		range1 = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 12, 31));
		range2 = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 12, 30));
		System.assert(!range1.equals(range2));
		System.assert(!range2.equals(range1));
		System.assertNotEquals(range1.hashCode(), range2.hashCode());

		// 開始日と終了日が異なるオブジェクト
		range1 = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 12, 31));
		range2 = new AppDateRange(AppDate.newInstance(2018, 2, 2), AppDate.newInstance(2019, 11, 30));
		System.assert(!range1.equals(range2));
		System.assert(!range2.equals(range1));
		System.assertNotEquals(range1.hashCode(), range2.hashCode());
	}

	/**
	 * 開始日・終了日の範囲の値を取得できる事を検証する
	 */
	@isTest
	static void valuesTest() {
		AppDateRange range = new AppDateRange(AppDate.newInstance(2020, 1, 1), AppDate.newInstance(2020, 1, 5));
		AppDate date1 = AppDate.newInstance(2020, 1, 1);
		AppDate date2 = AppDate.newInstance(2020, 1, 2);
		AppDate date3 = AppDate.newInstance(2020, 1, 3);
		AppDate date4 = AppDate.newInstance(2020, 1, 4);
		AppDate date5 = AppDate.newInstance(2020, 1, 5);
		System.assertEquals(new List<AppDate>{date1, date2, date3, date4, date5}, range.values());
	}

	/**
	 * 開始日と終了日が同じ場合に、値を取得できる事を検証する
	 */
	@isTest
	static void valueTestOneDay() {
		AppDateRange range = new AppDateRange(AppDate.newInstance(2020, 1, 1), AppDate.newInstance(2020, 1, 1));
		System.assertEquals(new List<AppDate>{AppDate.newInstance(2020, 1, 1)}, range.values());
	}
}