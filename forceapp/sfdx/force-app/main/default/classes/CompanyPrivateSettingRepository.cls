/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 会社非公開設定のリポジトリ
 */
public with sharing class CompanyPrivateSettingRepository extends Repository.BaseRepository {
	/** 取得対象の項目(カレンダー) */
	private static final Set<Schema.SObjectField> GET_SETTING_FIELD_SET;
	static {
		GET_SETTING_FIELD_SET = new Set<Schema.SObjectField> {
			CompanyPrivateSetting__c.Id,
			CompanyPrivateSetting__c.Name,
			CompanyPrivateSetting__c.CompanyId__c,
			CompanyPrivateSetting__c.CryptoKey__c,
			CompanyPrivateSetting__c.Office365AuthCode__c
		};
	}
	
	/**
	 * 指定した会社Idの設定を取得する
	 * @param companyId 取得対象の会社ID
	 * @return 設定情報、見つからなかった場合はnull
	 */
	public CompanyPrivateSettingEntity getEntity(String companyId) {
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_SETTING_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		if (companyId != null) {
			condList.add(getFieldName(CompanyPrivateSetting__c.CompanyId__c) + ' = :companyId');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + CompanyPrivateSetting__c.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond;
		// クエリ実行
		System.debug('CompanyPrivateSettingRepository: SOQL==>' + soql);
		List<CompanyPrivateSetting__c> settingList = Database.query(soql);

		// 結果からエンティティを作成する
		for (CompanyPrivateSetting__c setting : settingList) {
			return createEntity(setting);
		}
		return null;
	}

		/**
	 * エンティティをレコードとして保存する
	 * @param entity 作成元のエンティティ
	 * @return レコード保存結果
	 */
	public SaveResult saveEntity(CompanyPrivateSettingEntity entity) {
		CompanyPrivateSetting__c sObj = createSObject(entity);
		
		// DBに保存する
		Database.UpsertResult recRes = AppDatabase.doUpsert(sObj);

		// 結果作成
		return resultFactory.createSaveResult(new List<Database.UpsertResult>{recRes});
	}

	/**
	 * CompanyPrivateSetting__cからCompanyPrivateSettingEntityを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private CompanyPrivateSettingEntity createEntity(CompanyPrivateSetting__c sObj) {
		CompanyPrivateSettingEntity entity = new CompanyPrivateSettingEntity();
		entity.setId(sObj.Id);
		entity.companyId = sObj.CompanyId__c;
		entity.cryptoKey = sObj.CryptoKey__c;
		entity.office365AuthCode = sObj.Office365AuthCode__c;

		entity.resetChanged();

		return entity;
	}

	/**
	 * CompanyPrivateSettingEntityからCompanyPrivateSetting__cを作成する
	 * @param entity 作成元のエンティティ
	 * @return 作成したSObject
	 */
	@TestVisible
	private CompanyPrivateSetting__c createSObject(CompanyPrivateSettingEntity entity) {
		Boolean isInsert = String.isBlank(entity.id);
		CompanyPrivateSetting__c sObj = new CompanyPrivateSetting__c(Id = entity.Id);

		if (isInsert) {
			sObj.CompanyId__c = entity.companyId;
			sObj.Name = entity.companyId;
		}
		
		if (entity.isChanged(CompanyPrivateSettingEntity.Field.CRYPTO_KEY)) {
			sObj.CryptoKey__c = entity.cryptoKey;
		}
		if (entity.isChanged(CompanyPrivateSettingEntity.Field.OFFICE365_AUTH_CODE)) {
			sObj.Office365AuthCode__c = entity.office365AuthCode;
		}
		return sObj;
	}
}