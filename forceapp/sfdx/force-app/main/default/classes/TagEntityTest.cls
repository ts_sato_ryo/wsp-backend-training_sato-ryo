/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description TagEntityのテストクラス
 */
@isTest
private class TagEntityTest {

	/**
	 * テストデータクラス
	 */
	private class EntityTestData extends TestData.TestDataEntity {
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		TagEntity entity = new TagEntity();

		// 会社コード、タグタイプ、タグコードが空でない場合
		// ユニークキーが作成される
		String companyCode = 'CompanyCode';
		entity.tagTypeValue = TagType.JOB_ASSIGN_GROUP;
		entity.code = 'TagCode';
		System.assertEquals('JA-CompanyCode-TagCode', entity.createUniqKey(companyCode));
	}

	/*
	 * ユニークキー作成テスト
	 * 会社コードが空の場合、例外が発生することを確認する
	 */
	@isTest static void createUniqKeyTestWhenCompanyCodeIsBrankThenThrough() {
		TagEntity entity = new TagEntity();
		String companyCode = '';
		entity.tagTypeValue = TagType.JOB_ASSIGN_GROUP;
		entity.code = 'TagCode';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}
	/*
	 * ユニークキー作成テスト
	 * タグタイプがnullの場合、例外が発生することを確認する
	 */
	@isTest static void createUniqKeyTestWhenTagTypeIsNullThenThrough() {
		TagEntity entity = new TagEntity();
		String companyCode = 'CompanyCode';
		entity.code = 'TagCode';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}
	}

	/*
	 * ユニークキー作成テスト
	 * タグコードが空の場合、例外が発生することを確認する
	 */
	@isTest static void createUniqKeyTestWhenTagCodeIsBrankThenThrough() {
		TagEntity entity = new TagEntity();
		String companyCode = 'CompanyCode';
		entity.tagTypeValue = TagType.JOB_ASSIGN_GROUP;
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}
	}

}