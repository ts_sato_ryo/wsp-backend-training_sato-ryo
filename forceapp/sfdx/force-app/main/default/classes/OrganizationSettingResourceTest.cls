/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description OrganizationSettingResourceのテストクラス
 */
@isTest
private class OrganizationSettingResourceTest {

	private class TestData extends TestData.TestDataEntity {

	}

	/**
	 * 組織設定レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		Test.startTest();

		OrganizationSettingResource.OrganizationSetting param = new OrganizationSettingResource.OrganizationSetting();
		param.language0 = 'ja';
		param.language1 = 'en_US';
//		param.language2 = 'de';	// TODO ２言語しか対応していないためコメントアウト。対応言語が増えたらコメントアウトを外してください

		OrganizationSettingResource.UpdateApi api = new OrganizationSettingResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		// レスポンスはnull
		System.assertEquals(null, res);

		// 組織設定レコードが更新されること
		List<ComOrgSetting__c> newRecords = [SELECT Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];
		System.assertEquals(1, newRecords.size());

		ComOrgSetting__c newRecord = newRecords[0];
		System.assertEquals(param.language0, newRecord.Language_0__c);
		System.assertEquals(param.language1, newRecord.Language_1__c);
//		System.assertEquals(param.language2, newRecord.Language_2__c);
	}

	/**
	 * 組織設定レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void createRequiredParameterTest() {
		Test.startTest();

		OrganizationSettingResource.OrganizationSetting param = new OrganizationSettingResource.OrganizationSetting();
		param.language0 = null;

		App.ParameterException ex;
		try {
			Object res = (new OrganizationSettingResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
//		System.assertEquals('REQUIRED_FIELD_MISSING', ex.getDmlStatusCode(0));
	}

	/**
	 * 組織設定レコードを1件作成する(異常系:不正なパラメータを設定)
	 */
	@isTest
	static void createInvalidParameterTest() {
		Test.startTest();

		OrganizationSettingResource.OrganizationSetting param = new OrganizationSettingResource.OrganizationSetting();
		param.language0 = 'dummy_language';

		App.ParameterException ex;
		try {
			Object res = (new OrganizationSettingResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
//		} catch (DmlException e) {
//			ex = e;
//		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
//		System.assertEquals('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST', ex.getDmlStatusCode(0));
	}

	/**
	 * 組織設定レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		ComOrgSetting__c testOrg = ComTestDataUtility.createOrgSetting();

		Test.startTest();

		OrganizationSettingResource.OrganizationSetting param = new OrganizationSettingResource.OrganizationSetting();
		param.language0 = 'en_US';
		param.language1 = 'ja';
//		param.language2 = 'de';	// TODO ２言語しか対応していないためコメントアウト。対応言語が増えたらコメントアウトを外してください

		OrganizationSettingResource.UpdateApi api = new OrganizationSettingResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		// レスポンスはnull
		System.assertEquals(null, res);

		// 組織設定レコードが更新されること
		List<ComOrgSetting__c> newRecords = [SELECT Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];
		System.assertEquals(1, newRecords.size());

		ComOrgSetting__c newRecord = newRecords[0];
		System.assertEquals(testOrg.Id, newRecord.Id);
		System.assertEquals(param.language0, newRecord.Language_0__c);
		System.assertEquals(param.language1, newRecord.Language_1__c);
//		System.assertEquals(param.language2, newRecord.Language_2__c);
	}

	/**
	 * 組織設定レコードを1件更新する(異常系)
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void updatePositiveTestNoPermission() {
		OrganizationSettingResourceTest.TestData testData = new TestData();
		OrganizationSettingEntity org = testData.org;
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('Test');

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			try {
				OrganizationSettingResource.OrganizationSetting param = new OrganizationSettingResource.OrganizationSetting();
				param.language0 = 'en_US';
				param.language1 = 'ja';

				OrganizationSettingResource.UpdateApi api = new OrganizationSettingResource.UpdateApi();
				Object res = api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	private class RequestParamTest implements RemoteApi.RequestParam {}

	/**
	 * 組織設定レコードを取得する(正常系: 組織レコードが存在する)
	 */
	@isTest
	static void getPositiveTest() {

		ComOrgSetting__c testOrg = ComTestDataUtility.createOrgSetting();
		testOrg.Language_0__c = 'ja';
		testOrg.Language_1__c = 'en_US';
//		param.language2 = 'de';	// TODO ２言語しか対応していないためコメントアウト。対応言語が増えたらコメントアウトを外してください
		update testOrg;

		Test.startTest();

		RequestParamTest req = new RequestParamTest();
		OrganizationSettingResource.GetApi api = new OrganizationSettingResource.GetApi();
		OrganizationSettingResource.OrganizationSetting res = (OrganizationSettingResource.OrganizationSetting)api.execute(req);

		Test.stopTest();

		// 組織設定レコードが取得できること
		System.assertEquals(testOrg.Language_0__c, res.language0);
		System.assertEquals(testOrg.Language_1__c, res.language1);
//		System.assertEquals(testOrg.Language_2__c, res.language2);
	}

	/**
	 * 組織設定レコードを取得する(正常系: 組織設定レコードが登録されていない)
	 */
	@isTest
	static void getPositiveTestRecordNotFound() {

		Test.startTest();

		RequestParamTest req = new RequestParamTest();
		OrganizationSettingResource.GetApi api = new OrganizationSettingResource.GetApi();
		OrganizationSettingResource.OrganizationSetting res = (OrganizationSettingResource.OrganizationSetting)api.execute(req);

		Test.stopTest();

		// 組織設定レコードが取得できること
		System.assertEquals(null, res.language0);
		System.assertEquals(null, res.language1);
		System.assertEquals(null, res.language2);
	}


	/**
	 * 言語選択リストを取得する(正常系)
	 */
	@isTest
	static void getLanguagePicklistTest() {

		ComOrgSetting__c.Language_0__c.getDescribe().getPicklistValues();

		Test.startTest();

		RequestParamTest req = new RequestParamTest();
		OrganizationSettingResource.GetLanguagePicklistApi api = new OrganizationSettingResource.GetLanguagePicklistApi();
		OrganizationSettingResource.Picklist res = (OrganizationSettingResource.Picklist)api.execute(req);

		Test.stopTest();

		// 言語選択リストが取得できること
		System.assertNotEquals(0, res.entries.size());

		List<Schema.PicklistEntry> pickEntries = ComOrgSetting__c.Language_0__c.getDescribe().getPicklistValues();
		for (Integer i = 0, n = pickEntries.size(); i < n; i++) {
			System.assertEquals(pickEntries[i].getValue(), res.entries[i].value);
			System.assertEquals(pickEntries[i].getLabel(), res.entries[i].label);
		}

	}

}
