/**
 * TeamSpirit コストセンター(ベース)のエンティティクラス。
 *
 * 当クラスはツールによって自動生成しているため、手動で修正しても上書きされる可能性があります。
 * 業務ロジックを追加する場合は、サブクラスを作成してください。
 *
 * Since this class is automatically generated by the tool, it may be overwritten even if it is corrected manually.
 * If you want to add business logic, please create a subclass.
 */
public abstract with sharing class CostCenterBaseGeneratedEntity extends ParentChildBaseCompanyEntity {

	/** フィールド名とフィールドのマップ */
	private static final Map<String, Schema.SObjectField> SOBJECT_FIELD_MAP = Schema.SObjectType.ComCostCenterBase__c.fields.getMap();

	/** TeamSpirit コストセンター(ベース) */
	protected ComCostCenterBase__c sobj;

	/**
	 * デフォルトコンストラクタ
	 */
	public CostCenterBaseGeneratedEntity() {
		super(new ComCostCenterBase__c(), SOBJECT_FIELD_MAP);
		this.sobj = (ComCostCenterBase__c)super.sobj;
	}

	/**
	 * コンストラクタ
	 */
	public CostCenterBaseGeneratedEntity(ComCostCenterBase__c sobj) {
		super(sobj, SOBJECT_FIELD_MAP);
		this.sobj = sobj;
	}

	/**
	 * Idと値を更新した項目を設定したSObjectを生成する。
	 * @return 生成したSObjecgt
	 */
	public virtual ComCostCenterBase__c createSObject() {
		ComCostCenterBase__c sobj = new ComCostCenterBase__c(Id = super.id);
		for (Schema.SObjectField field : changedValues.keySet()) {
			sobj.put(field, changedValues.get(field));
		}
		return sobj;
	}

}