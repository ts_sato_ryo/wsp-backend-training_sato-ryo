/**
 * フレックスタイム制の残業時間を計算するロジックテストクラス
 */
@isTest
private class AttCalcOverTimeFlexLogicTest {

	class CalcOverInput extends AttCalcOverTimeFlexLogic.Input {
		public CalcOverInput() {
			super();
			this.legalWorkTime = 8*60; // 法定８時間
			this.isWorkDay = true;
			this.startTime = 11*60; // 11:00〜16:00のコアタイム
			this.endTime = 16*60;
			this.flexStartTime = 7*60;
			this.flexEndTime = 21*60;
			this.contractWorkTime = 7*60 + 30; // 所定7.5時間
			this.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
			this.isIncludeHolidayWorkInPlainTime = true;
		}
	}

	// 残業計算入力データを作成
	static CalcOverInput createInput(Integer inputStartTime, Integer inputEndTime) {
		CalcOverInput input = new CalcOverInput();
		input.inputStartTime = inputStartTime;
		input.inputEndTime = inputEndTime;
		input.inputRestRanges = AttCalcRangesDto.RS(input.contractRestRanges);
		return input;
	}

	/**
	 * 所定休日の労働時間をフレックス対象労働時間に含めるかのオプションのテスト
	 */
	@isTest static void noOffsetLostAndOvertimeTest() {
		AttCalcOverTimeFlexLogic logic = new AttCalcOverTimeFlexLogic();
		{
			// 労働時間をフレックス対象労働時間に含める
			CalcOverInput input = createInput(8*60, 19*60);
			input.isWorkDay = false;
			input.isIncludeHolidayWorkInPlainTime = true;
			input.allowedWorkRanges = AttCalcRangesDto.RS(8*60, 19*60);
			AttCalcOverTimeFlexLogic.Output output = new AttCalcOverTimeFlexLogic.Output();
			logic.apply(output, input);

			AttCalcRangesDto cili = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.InContractInLegal);
			AttCalcRangesDto cilo = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.InContractOutLegal);
			AttCalcRangesDto coli = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.OutContractInLegal);
			AttCalcRangesDto colo = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.OutContractOutLegal);
			System.assertEquals(60*10, cili.total());
			System.assert(cilo == null || cilo.size() == 0);
			System.assert(coli == null || coli.size() == 0);
			System.assert(colo == null || colo.size() == 0);
		}
		{
			// 労働時間をフレックス対象労働時間に含めない
			CalcOverInput input = createInput(8*60, 19*60);
			input.isWorkDay = false;
			input.isIncludeHolidayWorkInPlainTime = false;
			input.allowedWorkRanges = AttCalcRangesDto.RS(8*60, 19*60);
			AttCalcOverTimeFlexLogic.Output output = new AttCalcOverTimeFlexLogic.Output();
			logic.apply(output, input);

			AttCalcRangesDto cili = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.InContractInLegal);
			AttCalcRangesDto cilo = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.InContractOutLegal);
			AttCalcRangesDto coli = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.OutContractInLegal);
			AttCalcRangesDto colo = output.workTimeCategoryMap.get(AttCalcOverTimeFlexLogic.OverTimeCategory.OutContractOutLegal);
			System.assert(cili == null || cili.size() == 0);
			System.assert(cilo == null || cilo.size() == 0);
			System.assert(coli == null || coli.size() == 0);
			System.assertEquals(60*10, colo.total());
		}
	}
	/**
	 * 許可勤務時間範囲テスト
	 */
	@isTest static void allowWorkRangesTest() {
		AttCalcOverTimeFlexLogic logic = new AttCalcOverTimeFlexLogic();
		// 労働時間をフレックス対象労働時間に含める
		CalcOverInput input = createInput(8*60, 22*60);
		input.inputRestRanges = input.inputRestRanges.insertAndMerge(AttCalcRangesDto.RS(20*60+30, 21*60+30)); // 30分休憩のみ認める
		input.isWorkDay = true;
		input.isIncludeHolidayWorkInPlainTime = true;
		input.allowedWorkRanges = AttCalcRangesDto.RS(13*60, 21*60);
		AttCalcOverTimeFlexLogic.Output output = new AttCalcOverTimeFlexLogic.Output();
		logic.apply(output, input);

		System.assertEquals(7*60 + 30, output.realWorkTime);
		System.assertEquals(30, output.breakTime);
	}
	/**
	 * その他休憩時間テスト
	 */
	@isTest static void inputRestHouresTest() {
		AttCalcOverTimeFlexLogic logic = new AttCalcOverTimeFlexLogic();
		// コアタイム外
		CalcOverInput input = createInput(8*60, 9*60);
		input.isWorkDay = true;
		input.isIncludeHolidayWorkInPlainTime = true;
		input.allowedWorkRanges = AttCalcRangesDto.RS(5*60, 21*60);
		input.inputRestTime = 30;
		AttCalcOverTimeFlexLogic.Output output = new AttCalcOverTimeFlexLogic.Output();
		logic.apply(output, input);

		System.assertEquals(30, output.realWorkTime);
		System.assertEquals(0, output.breakTime); // その他休憩は計算しない
	}
	/**
	 * 所定休日勤務はコアなしと見なすテスト
	 */
	@isTest static void holidayWorkAsNoCoreTimeTest() {
		AttCalcOverTimeFlexLogic logic = new AttCalcOverTimeFlexLogic();
		// コアタイム外
		CalcOverInput input = createInput(12*60, 14*60);
		input.startTime = 10*60; // テスト用、通常null
		input.endTime = 16*60; // テスト用、通常null
		input.isWorkDay = false;
		input.isLegalHoliday = false;
		input.isIncludeHolidayWorkInPlainTime = true;
		input.allowedWorkRanges = AttCalcRangesDto.RS(12*60, 14*60);
		AttCalcOverTimeFlexLogic.Output output = new AttCalcOverTimeFlexLogic.Output();
		logic.apply(output, input);

		System.assertEquals(0, output.lateArriveTime);
		System.assertEquals(0, output.earlyLeaveTime);
	}
	// 勤務換算休憩・無給休暇休憩が実労働時間から取り除かれること
	@isTest static void excludeRestsFromWorkTimeRangesTest() {
		CalcOverInput input = createInput(9*60, 18*60);
		input.isWorkDay = true;
		input.convertdRestRanges = AttCalcRangesDto.RS(11*60, 12*60); // 勤務換算時間(有給の時間単位休)を1時間取得
		input.unpaidLeaveRestRanges = AttCalcRangesDto.RS(15*60, 15*60 + 30); // 無給休暇休憩(無給の時間単位休)を30分取得
		AttCalcOverTimeFlexLogic.Output output = new AttCalcOverTimeFlexLogic.Output();

		AttCalcOverTimeFlexLogic logic = new AttCalcOverTimeFlexLogic();
		logic.apply(output, input);
		System.assertEquals(6*60 + 30, output.realWorkTime); // 実労働時間 = 労働時間 - 所定休憩 - 勤務換算休憩 - 無給休暇休憩
	}
}