/**
 * @group チーム
 *
 * @description チーム機能で利用する勤怠リソースのAPIを提供するクラス
 */
public with sharing class TeamAttendanceResource {
	/**
	 * @description 各社員の月度勤務確定状況データを表すクラス
	 */
	public class Summary implements RemoteApi.RequestParam, RemoteApi.ResponseParam {
		/** 社員ID */
		public String employeeId;
		/** 社員コード */
		public String employeeCode;
		/** 社員名 */
		public String employeeName;
		/** 社員の顔写真URL */
		public String photoUrl;
		/** 勤務体系名 */
		public String workingTypeName;
		/** 月度開始日 */
		public String startDate;
		/** 月度終了日 */
		public String endDate;
		/** (勤務確定申請の)ステータス *月度のみ対応。申請前の場合は未申請のステータスが戻り値 */
		public String status;
		/** (勤務確定申請の)承認者名 *月度のみ対応。*/
		public String approverName;

		public Summary(TeamAttendanceSummaryService.SummaryRequestRecord entity) {
			this.employeeId = entity.employeeId;
			this.employeeCode = entity.employeeCode;
			this.employeeName = entity.employeeName;
			this.photoUrl = entity.photoUrl;
			this.workingTypeName = entity.workingTypeName;
			this.startDate = entity.startDate == null ? null : entity.startDate.format();
			this.endDate = entity.endDate == null ? null : entity.endDate.format();
			this.status = entity.status;
			this.approverName = entity.approverName;
		}
	}

	/**
	 * @description 勤怠サマリー一覧データ検索処理のパラメータクラス
	 */
	public class SummarySearchCondition implements RemoteApi.RequestParam {
		/** 対象年 */
		public String targetYear;
		/** 対象月度 *対象月ではない */
		public String targetMonthly;
		/** 部署ID(ベース) */
		public String departmentId;
		/** API引数のバリデーションを行う */
		public void validate() {
			// 対象年
			try {
				Integer.valueOf(targetYear);
			} catch (Exception e) {
				throw new App.ParameterException('targetYear', targetYear);
			}
			// 対象月度
			try {
				Integer.valueOf(targetMonthly);
			} catch (Exception e) {
				throw new App.ParameterException('targetMonthly', targetMonthly);
			}
			// 対象年・対象月度を組み合わせて日付を取得できるか？
			if (String.isNotBlank(targetYear) && String.isNotBlank(targetMonthly)) {
				try {
					Date.newInstance(Integer.valueOf(targetYear), Integer.valueOf(targetMonthly), 1);
				} catch (Exception e) {
					String target = String.valueOf(targetYear + targetMonthly);
					throw new App.ParameterException('target', target);
				}
			}
			// 部署ID
			if (String.isNotBlank(departmentId)) {
				try {
					Id.valueOf(departmentId);
				} catch (Exception e) {
					throw new App.ParameterException('departmentId', departmentId);
				}
			}
		}
	}

	/**
	 * @description 勤怠サマリー一覧データ検索処理のレスポンスクラス
	 */
	public class SummarySearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public Summary[] records;
	}

	/**
	 * @description 勤怠サマリー一覧データ検索のAPIクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 勤怠サマリーを検索する
		 * @param  req リクエストパラメータ(SummarySearchCondition)
		 * @return レスポンス(SummarySearchResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SummarySearchCondition param = (SummarySearchCondition)req.getParam(SummarySearchCondition.class);
			System.debug(req.getParamMap());
			param.validate();

			// 初期化
			TeamAttendanceSummaryService teamAttService = new TeamAttendanceSummaryService();

			// 検索条件設定
			TeamAttendanceSummaryService.SummaryRequestSearchFilter filter
					= new TeamAttendanceSummaryService.SummaryRequestSearchFilter();
			if (String.isNotBlank(param.targetYear)) {
				filter.targetYear = Integer.valueOf(param.targetYear);
			}
			if (String.isNotBlank(param.targetMonthly)) {
				filter.targetMonthly = Integer.valueOf(param.targetMonthly);
			}
			if (String.isEmpty(param.departmentId)) {
				filter.isSearchEmployeeWithoutDepartment = true;
			} else {
				filter.isSearchEmployeeWithoutDepartment = false;
				filter.departmentIdList = new List<Id>{ param.departmentId };
			}

			//検索実行
			List<TeamAttendanceSummaryService.SummaryRequestRecord> searchResultList = teamAttService.searchSummaryRequestRecord(filter);

			// レスポンスクラスに変換
			List<Summary> records = new List<Summary>();
			for (TeamAttendanceSummaryService.SummaryRequestRecord record : searchResultList) {
				records.add(new Summary(record));
			}

			// 成功レスポンスをセットする
			SummarySearchResult res = new SummarySearchResult();
			res.records = records;
			return res;
		}
	}

	/**
	 * @description 集計期間情報
	 */
	public class SummaryPeriod {
		/** 期間名(yyyymm) */
		public String name;
		/** 期間表示名 */
		public String label;
		/** 年(年度ではない) */
		public String year;
		/** 月度 */
		public String monthly;

		/**
		 * @description コンストラクタ
		 * @param name 期間名
		 * @param label 表示名
		 */
		public SummaryPeriod(String name, String label) {
			this.name = name;
			this.label = label;
			this.year = name.subString(0, 4);
			this.monthly = String.valueOf(Integer.valueOf(name.subString(4, 6)));
		}
	}

	/**
	 * @description 勤怠集計期間一覧取得APIのリクエストパラメータクラス
	 */
	public class SummaryPeriodListRequest implements RemoteApi.RequestParam {
		/** 社員ベースID */
		public String empId;
		/** 対象期間名(SummaryPeriod.nameの値) */
		public String targetPeriod;

		/** パラメータを検証する */
		public void validate() {

			// 社員ベースID
			if (String.isNotBlank(this.empId)) {
				try {
					Id.valueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', this.empId);
				}
			}

			// 対象期間名
			if (String.isNotBlank(this.targetPeriod)) {
				try {
					// 年(yyyy)
					Integer.valueOf(targetPeriod.subString(0, 4));
					// 月度(mm)
					Integer.valueOf(targetPeriod.subString(4, 6));
				} catch (Exception e) {
					throw new App.ParameterException('targetPeriod', this.targetPeriod);
				}
			}
		}
	}

	/**
	 * @description 勤怠集計期間一覧取得APIのレスポンスパラメータクラス
	 */
	public class SummaryPeriodLisResult implements RemoteApi.ResponseParam {
		/** 集計期間のリスト */
		public SummaryPeriod[] periods;
		/** ログインユーザ社員の現在の集計期間(該当する集計期間のSummaryPeriod.nameの値) */
		public String currentPeriodName;
	}

	/**
	 * @description 勤怠集計期間一覧取得APIクラス
	 */
	public with sharing class SummaryPeriodListApi extends RemoteApi.ResourceBase {

		private AppDate today = AppDate.today();

		/** テスト用 */
		@testVisible
		private void setToday(AppDate today) {
			this.today = today;
		}

		/**
		 * @description 勤怠集計期間一覧を取得する
		 * @param req リクエストパラメータ(SummaryPeriodListRequest)
		 * @return レスポンス(SummaryPeriodLisResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SummaryPeriodListRequest param = (SummaryPeriodListRequest)req.getParam(SummaryPeriodListRequest.class);
			param.validate();

			EmployeeBaseEntity loginEmployee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
			EmployeeBaseEntity employee;
			if (String.isBlank(param.empId)) {
				// 社員ベースIDが設定されていない場合はログインユーザの社員を取得する
				employee = loginEmployee;
			} else {
				employee = new EmployeeService().getEmployeeFromCache(param.empId);
			}

			// 社員の会社の勤務体系を全て取得する（ベースのみ）
			AttWorkingTypeRepository.SearchFilter workingTypeFilter = new AttWorkingTypeRepository.SearchFilter();
			workingTypeFilter.companyIds = new Set<Id>{employee.companyId};
			List<AttWorkingTypeBaseEntity> workingTypeList = new AttWorkingTypeRepository().searchEntity(workingTypeFilter);

			// 月度のMapを作成する
			Map<String, SummaryPeriod> periodMap = createPeriodMap(workingTypeList, param.targetPeriod);

			// 月度リストを作成して降順にソートする
			List<String> monthlyList = new List<String>();
			for (String monthly : periodMap.keySet()) {
				monthlyList.add(monthly);
			}
			monthlyList = sortListDesc(monthlyList);

			// ログインユーザ社員の今日時点の勤務体系の現在月度を算出する
			String loginCurrentMonthly = null;
			if (loginEmployee.getHistory(0).workingTypeId != null) {
				Id loginWorkingTypeId = loginEmployee.getHistory(0).workingTypeId;
				AttWorkingTypeBaseEntity loginWorkingType = null;
				for (AttWorkingTypeBaseEntity workingType : workingTypeList) {
					if (workingType.id == loginWorkingTypeId) {
						loginWorkingType = workingType;
						continue;
					}
				}
				String monthly = loginWorkingType.getMonthlyByDate(this.today);
				// 月度一覧に含まれているのであればレスポンスで返却する
				if (periodMap.containsKey(monthly)) {
					loginCurrentMonthly = monthly;
				}
			}

			// レスポンス作成
			SummaryPeriodLisResult result = new SummaryPeriodLisResult();
			result.currentPeriodName = loginCurrentMonthly;
			result.periods = new List<SummaryPeriod>();
			for (String monthly : monthlyList) {
				result.periods.add(periodMap.get(monthly));
			}

			return result;
		}

		/**
		 * @description 勤怠集計期間Mapを取得する
		 */
		private Map<String, SummaryPeriod> createPeriodMap(List<AttWorkingTypeBaseEntity> workingTypeList, String targetPeriod) {
			final Integer futurePeriodSize = 2;
			final Integer pastPeriodSize = 12;
			final Integer totalPeriodSize = futurePeriodSize + pastPeriodSize + 1; // 1は対象月度分
			final String userLang = UserInfo.getLanguage();

			Map<String, SummaryPeriod> periodMap = new Map<String, SummaryPeriod>(); // キーはyyyymmの月度
			List<SummaryPeriod> periodList = new List<SummaryPeriod>();
			for (AttWorkingTypeBaseEntity workingType : workingTypeList) {
				// 各勤務体系の今日時点の月度を算出する → 最大月度になる
				String currentMontly = workingType.getMonthlyByDate(this.today);
				AppDate currentMonthlyStartDate = workingType.getStartDateOfMonthly(currentMontly);

				// 対象月度から対象月度の起算日を取得する
				AppDate targetStartDate;
				if (String.isBlank(targetPeriod)) {
					// 現在月度が対象月度となる
					targetStartDate = workingType.getStartDateOfMonthly(currentMontly);
				} else {
					targetStartDate = workingType.getStartDateOfMonthly(targetPeriod);
				}

				AppDate startDate = targetStartDate.addMonths(-pastPeriodSize);
				for (Integer i = 0; i < totalPeriodSize; i++) {
					// 対象の起算日が現在月度の起算日より未来の場合は一覧の対象外とする
					if (startDate.getDate() > currentMonthlyStartDate.getDate()) {
						continue;
					}

					String monthly = workingType.getMonthlyByDate(startDate);
					if (! periodMap.containsKey(monthly)) {
						String label = workingType.getMonthlyLabel(monthly, userLang);
						SummaryPeriod period = new SummaryPeriod(monthly, label);
						periodMap.put(monthly, period);
					}

					startDate = startDate.addMonths(1);
				}
			}

			return periodMap;
		}

		/**
		 * リストを降順にソートしたリストを取得する。
		 * メソッド引数のlistに対して変更を行わない
		 * @param list ソート対象のリスト
		 * @return 降順にソートされたリスト
		 */
		private List<String> sortListDesc(List<String> originalList) {
			List<String> sortList = originalList.clone();
			sortList.sort();
			List<String> descSortList = new List<String>();
			for (Integer i = sortList.size() - 1; i >= 0; i--) {
				descSortList.add(sortList[i]);
			}
			return descSortList;
		}
	}
}