/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * インポートバッチ種別
 */
public with sharing class ImportBatchType extends ValueObjectType {

	/** コストセンター */
	public static final ImportBatchType COST_CENTER = new ImportBatchType('CostCenter');
	/** 社員部署 */
	public static final ImportBatchType EMPLOYEE_DEPT = new ImportBatchType('EmployeeDept');
	/** ジョブ */
	public static final ImportBatchType JOB = new ImportBatchType('Job');

	/** インポートバッチ種別のリスト（種別が追加されら本リストにも追加してください） */
	public static final List<ImportBatchType> TYPE_LIST;
	static {
		TYPE_LIST = new List<ImportBatchType> {
			COST_CENTER,
			EMPLOYEE_DEPT,
			JOB
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ImportBatchType(String value) {
		super(value);
	}

	/**
	 * 値からImportBatchTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つImportBatchType、ただし該当するImportBatchTypeが存在しない場合はnull
	 */
	public static ImportBatchType valueOf(String value) {
		ImportBatchType retType = null;
		if (String.isNotBlank(value)) {
			for (ImportBatchType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ImportBatchType以外の場合はfalse
		Boolean eq;
		if (compare instanceof ImportBatchType) {
			// 値が同じであればtrue
			if (this.value == ((ImportBatchType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}