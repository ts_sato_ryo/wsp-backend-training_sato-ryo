/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Handler class for ComExtendedItemCustomTrigger
 * 
 * @group Expense
 */
public with sharing class ExtendedItemCustomTriggerHandler {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** CompanyId as the Key and the Set of all the ExtendedItemCustom__c.Code__c belonging to the company as Value*/
	private Map<Id, Set<String>> companyIdCustomCodeSetMap;
	/** Set contains all the ExtendedItemCustom__c.Code__c to process */
	private Set<String> allCodeSet;
	/** Set contains all the ExtendedItemCustom__c.CompanyId__c to process */
	private Set<Id> allCompanyIdSet;

	// Data From Trigger. Some of the field may be null base on the Trigger scenario.
	private List<ComExtendedItemCustom__c> oldList;
	private List<ComExtendedItemCustom__c> newList;
	private Map<Id, ComExtendedItemCustom__c> oldMap;
	private Map<Id, ComExtendedItemCustom__c> newMap;

	/**
	 * Constructor. It's designed as a general purpose constructor and depending on the trigger, some of the field may be null.
	 * */
	public ExtendedItemCustomTriggerHandler(List<ComExtendedItemCustom__c> oldList, Map<Id, ComExtendedItemCustom__c> oldMap,
			List<ComExtendedItemCustom__c> newList, Map<Id, ComExtendedItemCustom__c> newMap) {
		this.oldMap = oldMap;
		this.newMap = newMap;
		this.oldList = oldList;
		this.newList = newList;

		companyIdCustomCodeSetMap = new Map<Id, Set<String>>();
		allCodeSet = new Set<String>();
		allCompanyIdSet = new Set<Id>();
	}

	/*
	 * Validate the Data for Insert case
	 */
	public void validateInsert() {
		// Check Duplicate Among
		checkDuplicateWithinImport(this.newList);

		// Build Map of Existing
		Map<Id, Set<String>> existingCompanyIdCustomCodeSetMap = getExitingCompanyCodeMapForInsert();

		// Check against the codes which are already in the System
		checkDuplicateAgainstExisting(existingCompanyIdCustomCodeSetMap, this.newList);
	}

	/*
	 * Validate the Data for Update case
	 */
	public void validateUpdate() {

		Set<Id> reParentingIdSet = new Set<Id>();
		for (ComExtendedItemCustom__c extendedItemCustom : this.newList) {
			ComExtendedItemCustom__c previousCustom = oldMap.get(extendedItemCustom.Id);
			if (previousCustom.CompanyId__c != extendedItemCustom.CompanyId__c) {
				reParentingIdSet.add(extendedItemCustom.Id);
			}
		}
		Set<Id> alreadyReferenced = new Set<Id>();
		// If there are ExtendedItemCustoms which are changing parent, then find if they are already linked with an Extended Item
		if (!reParentingIdSet.isEmpty()) {
			for(ComExtendedItem__c extendedItem: [SELECT ID, ExtendedItemCustomId__c FROM ComExtendedItem__c WHERE ExtendedItemCustomId__c IN :reParentingIdSet]){
				if (extendedItem.ExtendedItemCustomId__c != null) {
					alreadyReferenced.add(extendedItem.ExtendedItemCustomId__c);
				}
			}
		}

		// Check Duplicate among records
		List<ComExtendedItemCustom__c> possibleDuplicateList = new List<ComExtendedItemCustom__c>();
		Set<String> possibleDuplicateCodeSet = new Set<String>();
		Set<String> possibleDuplicateCompanySet = new Set<String>();
		for (ComExtendedItemCustom__c extendedItemCustom : this.newList) {
			// If already referenced by an Extended Item, cannot change company. => Add error
			// But if it's just changing code, then it's not an issue as the code isn't reference anywhere at the moment.
			if (alreadyReferenced.contains(extendedItemCustom.Id)) {
				extendedItemCustom.CompanyId__c.addError(MESSAGE.Com_Err_CannotChangeReference);
			} else {
				// If code is changed or parent is changed, then need to check if new code is already in used or not.
				ComExtendedItemCustom__c previousCustom = oldMap.get(extendedItemCustom.Id);
				if (previousCustom.Code__c != extendedItemCustom.Code__c ||
								previousCustom.CompanyId__c != extendedItemCustom.CompanyId__c) {
					possibleDuplicateList.add(extendedItemCustom);
					possibleDuplicateCodeSet.add(extendedItemCustom.Code__c);
					possibleDuplicateCompanySet.add(extendedItemCustom.CompanyId__c);
				}
			}
		}

		checkDuplicateWithinImport(possibleDuplicateList);

		// Need to check only if there is Code changed.
		if (!possibleDuplicateList.isEmpty()) {
			// Build Map of Existing
			Map<Id, Set<String>> existingCompanyIdCustomCodeSetMap =
					getExitingCompanyCodeMapForUpdate(possibleDuplicateCodeSet, possibleDuplicateCompanySet);

			// Only need to check if there are some records found
			checkDuplicateAgainstExisting(existingCompanyIdCustomCodeSetMap, possibleDuplicateList);
		}
	}

	/*
	 * Check if the same Code is used in the records to be imported.
	 * This is intended for cases where the file user is using for data-loader contains
	 * duplicate code within same company. When 2 or more rows are using the same Code (for the same company),
	 * then all those rows will be marked as error.
	 */
	private void checkDuplicateWithinImport(List<ComExtendedItemCustom__c> newValueList) {
		/** Key: CompanyID + Code; Value: ComExtendedItemCustom__c */
		Map<String, ComExtendedItemCustom__c> companyIdCodeSObjMap = new Map<String, ComExtendedItemCustom__c>();
		Set<String> duplicateCompanyIdCustomCode = new Set<String>();
		for (ComExtendedItemCustom__c extendedItemCustom: newValueList) {
			String companyIdCustomCode = extendedItemCustom.CompanyId__c + extendedItemCustom.Code__c;
			ComExtendedItemCustom__c previouslyFound = companyIdCodeSObjMap.get(companyIdCustomCode);
			if (previouslyFound != null) {
				// Since there is already another record found using the same companyID and Code, both should be error.
				previouslyFound.Code__c.addError(MESSAGE.Com_Err_DuplicateCode);
				extendedItemCustom.Code__c.addError(MESSAGE.Com_Err_DuplicateCode);
				duplicateCompanyIdCustomCode.add(companyIdCustomCode);
			} else {
				companyIdCodeSObjMap.put(companyIdCustomCode, extendedItemCustom);
			}
		}

		for (ComExtendedItemCustom__c extendedItemCustom: newValueList) {
			// No need to consider the codes which will result in Error.
			String companyIdCustomCode = extendedItemCustom.CompanyId__c + extendedItemCustom.Code__c;
			if (!duplicateCompanyIdCustomCode.contains(companyIdCustomCode)) {
				Set<String> codeSet = companyIdCustomCodeSetMap.get(extendedItemCustom.CompanyId__c);
				allCompanyIdSet.add(extendedItemCustom.CompanyId__c);
				if (codeSet == null) {
					codeSet = new Set<String>();
					companyIdCustomCodeSetMap.put(extendedItemCustom.CompanyId__c, codeSet);
				}

				codeSet.add(extendedItemCustom.Code__c);
				allCodeSet.add(extendedItemCustom.Code__c);
			}
		}
	}

	/*
	 * Check if the Code is already in used in the system.
	 */
	private void checkDuplicateAgainstExisting(Map<Id, Set<String>> existingCompanyIdCustomCodeSetMap, List<ComExtendedItemCustom__c> validationTargetList) {
		// Only need to check if there is some records found
		if (!existingCompanyIdCustomCodeSetMap.keySet().isEmpty()) {
			for (ComExtendedItemCustom__c extendedItemCustom : validationTargetList) {
				Set<String> codeSet = existingCompanyIdCustomCodeSetMap.get(extendedItemCustom.CompanyId__c);
				if (codeSet != null) {
					if (codeSet.contains(extendedItemCustom.Code__c)) {
						extendedItemCustom.Code__c.addError(MESSAGE.Exp_Err_DuplicateExtendedItemCustomCode);
					}
				}
			}
		}
	}

	/*
	 * For Update case:
	 * Build the map with ExtendedItemCustom.CompanyId__c as the Key and the SET of all the ExtendedItemCustom.Code__c in the Company as value.
	 * When updating, if the code doesn't change, then no need to validate the code
	 *
	 * @param possibleDuplicateCodeSet Set of possible ExtendedItemCustom.Code__c
	 * @param possibleDuplicateCompanySet Set of possible ExtendedItemCustom.CompanyId__c
	 */
	private Map<Id, Set<String>> getExitingCompanyCodeMapForUpdate(Set<String> possibleDuplicateCodeSet, Set<String> possibleDuplicateCompanySet) {
		Map<Id, Set<String>> existingCompanyIdCustomCodeSetMap = new Map<Id, Set<String>>();
		for (ComExtendedItemCustom__c existing :
		[SELECT Id, Code__c, CompanyId__c, UniqKey__c FROM ComExtendedItemCustom__c WHERE Code__c in :possibleDuplicateCodeSet AND CompanyId__c in :possibleDuplicateCompanySet]) {
			populateExistingCompanyIdCustomCodeSetMap(existing, existingCompanyIdCustomCodeSetMap);
		}
		return existingCompanyIdCustomCodeSetMap;
	}

	/*
	 * For Insert case:
	 * Build the map with ExtendedItemCustom.CompanyId__c as the Key and the SET of all the ExtendedItemCustom.Code__c in the Company as value.
	 */
	private Map<Id, Set<String>> getExitingCompanyCodeMapForInsert() {
		Map<Id, Set<String>> existingCompanyIdCustomCodeSetMap = new Map<Id, Set<String>>();
		for (ComExtendedItemCustom__c existing :
		[SELECT Code__c, CompanyId__c FROM ComExtendedItemCustom__c WHERE Code__c in :allCodeSet AND CompanyId__c in :allCompanyIdSet]) {
			populateExistingCompanyIdCustomCodeSetMap(existing, existingCompanyIdCustomCodeSetMap);
		}
		return existingCompanyIdCustomCodeSetMap;
	}

	/*
	 * Populate the map with ExtendedItemCustom.CompanyId__c as the Key and the SET of all the ExtendedItemCustom.Code__c in the Company as value.
	 *
	 * @param existing ComExtendedItemCustom__c to be processed (depending on Insert or Update, the Items to process will be different)
	 * @param existingCompanyIdCustomCodeSetMap the map to populate
	 */
	private void populateExistingCompanyIdCustomCodeSetMap(ComExtendedItemCustom__c existing, Map<Id, Set<String>> existingCompanyIdCustomCodeSetMap) {
		Set<String> codeSet = existingCompanyIdCustomCodeSetMap.get(existing.CompanyId__c);
		if (codeSet == null) {
			codeSet = new Set<String>();
			existingCompanyIdCustomCodeSetMap.put(existing.CompanyId__c, codeSet);
		}
		codeSet.add(existing.Code__c);
	}
}