/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブタイプのリソースクラス
 */
public with sharing class JobTypeResource {

	/**
	 * ジョブタイプパラメータ
	 */
	public virtual class JobTypeParam implements RemoteApi.RequestParam {
		/** ジョブタイプID */
		public String id;
		/** ジョブタイプ名(翻訳) */
		public String name;
		/** ジョブタイプ名(L0) */
		public String name_L0;
		/** ジョブタイプ名(L1) */
		public String name_L1;
		/** ジョブタイプ名(L2) */
		public String name_L2;
		/** ジョブタイプコード */
		public String code;
		/** 会社ID */
		public String companyId;
		/** 作業分類リスト */
		public List<String> workCategoryIdList;

		/** パラメータを検証する */
		public virtual void validate() {
			// 会社ID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}

			// 作業分類ID
			if (this.workCategoryIdList != null) {
				for (String workCategoryId : this.workCategoryIdList) {
					try {
						System.Id.valueOf(workCategoryId);
					} catch (Exception e) {
						throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'workCategoryIdList'}));
					}
				}
			}
		}

		/**
		 * リクエストパラメータからジョブタイプエンティティを作成する
		 */
		public virtual JobTypeEntity createJobType(Map<String, Object> paramMap) {
			JobTypeEntity entity = new JobTypeEntity();
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('name_L0')) {
				entity.name = this.name_L0;
				entity.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			return entity;
		}

		/**
		 * パラメータに設定されている作業分類IDリストを返却する
		 */
		public List<Id> getWorkCategoryList(Map<String, Object> paramMap) {
			if (paramMap.containsKey('workCategoryIdList')) {
				return this.workCategoryIdList;
			}
			return null;
		}
	}

	/**
	 * 登録APIのリクエストクラス
	 */
	public class CreateRequest extends JobTypeParam implements RemoteApi.RequestParam {
	}

	/**
	 * 登録APIのレスポンスクラス
	 */
	public class CreateResponse implements RemoteApi.ResponseParam {
		/** ジョブタイプID */
		public String id;
	}

	/**
	 * ジョブタイプ登録API
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB_TYPE;

		/**
		 * @description ジョブタイプを1件登録する
		 * @param request リクエストパラメータ(CreateRequest)
		 * @return 登録結果(CreateResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// 入力値チェック
			CreateRequest param = (CreateRequest)request.getParam(CreateRequest.class);
			param.validate();
			Map<String, Object> paramMap = request.getParamMap();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 登録
			JobTypeEntity jobType = param.createJobType(paramMap);
	System.debug('JobTYPE >>>> ' + jobType);
			List<Id> workCategoryList = param.getWorkCategoryList(paramMap);
			Id resultId = new JobTypeService().saveJobTypeWithWorkCategoryList(jobType, workCategoryList);

			// レスポンス作成
			CreateResponse res = new CreateResponse();
			res.id = resultId;
			return res;
		}
	}

	/**
	 * 更新APIのリクエストクラス
	 */
	public class UpdateRequest extends JobTypeParam implements RemoteApi.RequestParam {
		/**
		 * パラメータを検証する
		 */
		public override void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}

			super.validate();
		}

		/**
		 * リクエストパラメータからジョブタイプエンティティを作成する
		 */
		public override JobTypeEntity createJobType(Map<String, Object> paramMap) {
			JobTypeEntity entity = super.createJobType(paramMap);
			if (paramMap.containsKey('id')) {
				entity.setId(this.id);
			}
			return entity;
		}
	}

	/**
	 * ジョブタイプ更新API
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB_TYPE;

		/**
		 * @description ジョブタイプを1件登録する
		 * @param request リクエストパラメータ(UpdateRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// 入力値チェック
			UpdateRequest param = (UpdateRequest)request.getParam(UpdateRequest.class);
			param.validate();
			Map<String, Object> paramMap = request.getParamMap();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 更新
			JobTypeEntity jobType = param.createJobType(paramMap);
			List<Id> workCategoryList = param.getWorkCategoryList(paramMap);
			Id resultId = new JobTypeService().saveJobTypeWithWorkCategoryList(jobType, workCategoryList);

			return null;
		}
	}

	/**
	 * @description 削除APIのリクエストパラメータ
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象ベースレコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}
	}

	/**
	 * ジョブタイプ削除API
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB_TYPE;

		/**
		 * @description ジョブタイプを1件登録する
		 * @param request リクエストパラメータ(DeleteRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// 入力値チェック
			DeleteRequest param = (DeleteRequest)request.getParam(DeleteRequest.class);
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new JobTypeService().deleteJobType(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合は成功レスポンスを返す
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					 throw e;
				}
			}
			return null;
		}
	}

	/**
	 * @description 検索APIのリクエストパラメータ
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** ジョブタイプID */
		public String id;
		/** 会社ID */
		public String companyId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// 会社ID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}
		}
	}

	/**
	 * @description 検索APIのリクエストパラメータ
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** ジョブタイプリスト */
		public List<JobTypeParam> records;
	}

	/**
	 * ジョブタイプ検索API
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description ジョブタイプを1件登録する
		 * @param request リクエストパラメータ(SearchRequest)
		 * @return レスポンス(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchRequest param = (SearchRequest)request.getParam(SearchRequest.class);
			Map<String, Object> paramMap = request.getParamMap();
			param.validate();

			// パラメータ id が指定されている場合は、idで検索
			Id jobTypeId;
			if(paramMap.containsKey('id')){
				jobTypeId = param.id;
			}
			// パラメータ companyId が指定されている場合は、会社IDで検索
			Id companyId;
			if(paramMap.containsKey('companyId')){
				companyId = param.companyId;
			}

			// 検索する
			List<JobTypeService.JobTypeWithWorkCategory> jobTypeWithWorkCategories =
				new JobTypeService().searchJobTypeListWithWorkCategory(companyId, jobTypeId);

			// レスポンス作成
			List<JobTypeParam> records = new List<JobTypeParam>();
			for (JobTypeService.JobTypeWithWorkCategory jobTypeWithWorkCategory : jobTypeWithWorkCategories) {
				records.add(craeteJobTypeParam(jobTypeWithWorkCategory));
			}
			SearchResponse res = new SearchResponse();
			res.records = records;

			return res;
		}

		/**
		 * 検索結果からレスポンスパラメータのジョブタイプを作成する
		 */
		private JobTypeParam craeteJobTypeParam(JobTypeService.JobTypeWithWorkCategory jobTypeWithWorkCategory) {
			JobTypeParam param = new JobTypeParam();

			JobTypeEntity jobType = jobTypeWithWorkCategory.jobType;
			param.id = jobType.id;
			param.name = jobType.nameL.getValue();
			param.name_L0 = jobType.nameL0;
			param.name_L1 = jobType.nameL1;
			param.name_L2 = jobType.nameL2;
			param.code = jobType.code;
			param.companyId = jobType.companyId;

			// 作業分類ID
			param.workCategoryIdList = new List<String>();
			for (JobTypeWorkCategoryEntity jobTypeWorkCategory : jobTypeWithWorkCategory.workCategoryList) {
				param.workCategoryIdList.add(jobTypeWorkCategory.workCategoryId);
			}

			return param;
		}
	}
}