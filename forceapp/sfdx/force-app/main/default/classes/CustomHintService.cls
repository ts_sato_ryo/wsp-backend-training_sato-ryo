/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description Service class of Custom Hint
 */
public with sharing class CustomHintService {
	/**
	 * @description save custom hints
	 * @param customHintList Custom Hint List
	 */
	public void saveCustomHintList(List<CustomHintEntity> customHintList) {
		System.debug('size:' + customHintList.size());
		if(customHintList != null && !customHintList.isEmpty()) {
			
			// Retrieve Existing Expense Custom Hints
			List<CustomHintEntity> existingList = getCustomHintList(customHintList[0].companyId, customHintList[0].moduleType.name());

			// Create Map <Field, Id>
			Map<String, Id> fieldIdMap = new Map<String, Id>();
			for (CustomHintEntity entity : existingList) {
				fieldIdMap.put(entity.fieldName, entity.id);
			}
			
			// Set Existing ID to Fields and Validate
			for (CustomHintEntity entity : customHintList) {
				entity.id = fieldIdMap.get(entity.fieldName);
				validateEntity(entity);
			}
			new CustomHintRepository().saveCustomHintEntityList(customHintList);
		}
	}

	/**
	 * Validate entity
	 * @param entity Target entity to be validated
	 */
	private static void validateEntity(CustomHintEntity entity) {
		Validator.Result result = new ComConfigValidator.CustomHintValidator(entity).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * @description get custom hints
	 * @param companyId Company
	 * @param moduleType Module Type Name
	 * @return customHintList Custom Hint List
	 */
	public List<CustomHintEntity> getCustomHintList(String companyId, String moduleType) {
		List<CustomHintEntity> customHintList = new CustomHintRepository().getCustomHintEntityList(companyId, moduleType);
		return customHintList;
	}
}