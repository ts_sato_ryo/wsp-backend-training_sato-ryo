/**
 */
@isTest
private class AttCalcRangesDtoTest {
	@isTest static void testRangeConstructor() {
		AttCalcRangeDto r = AttCalcRangesDto.R(1,2);
		System.assertEquals(1, r.getStartTime());
		System.assertEquals(2, r.getEndTime());
		AttCalcRangeDto rr = new AttCalcRangeDto(r);
		System.assertEquals(1, rr.getStartTime());
		System.assertEquals(2, rr.getEndTime());
	}
	
	@isTest static void testRangeEquals() {
		AttCalcRangeDto r = AttCalcRangesDto.R(1,2);
		AttCalcRangeDto r1 = AttCalcRangesDto.R(1, 3);
		AttCalcRangeDto r2 = AttCalcRangesDto.R(1, 2);
		AttCalcRangeDto r3 = AttCalcRangesDto.R(2, 3);
		AttCalcRangeDto r4 = AttCalcRangesDto.R(0, 3);
		System.assertEquals(true, r.equals(r));
		System.assertEquals(false, r.equals(r1));
		System.assertEquals(true, r.equals(r2));
		System.assertEquals(false, r.equals(r3));
		System.assertEquals(false, r.equals(r4));
		System.assertEquals(false, r.equals(null));
		System.assertEquals(false, r.equals(new AttCalcRangeDto(0, 0)));
	}
	
	@isTest static void testRangeCompareTo() {
		AttCalcRangeDto r12 = AttCalcRangesDto.R(1,2);
		AttCalcRangeDto r13 = AttCalcRangesDto.R(1,3);
		AttCalcRangeDto r14 = AttCalcRangesDto.R(1,4);
		AttCalcRangeDto r23 = AttCalcRangesDto.R(2,3);
		AttCalcRangeDto r24 = AttCalcRangesDto.R(2,4);
		AttCalcRangeDto r34 = AttCalcRangesDto.R(3,4);

		System.assertEquals(0, r12.compareTo(r12));
		System.assertEquals(-1, r12.compareTo(r13));
		System.assertEquals(1, r13.compareTo(r12));
		System.assertEquals(-1, r12.compareTo(r14));
		System.assertEquals(1, r14.compareTo(r12));
		System.assertEquals(-1, r12.compareTo(r23));
		System.assertEquals(1, r23.compareTo(r12));
		System.assertEquals(-1, r12.compareTo(r24));
		System.assertEquals(1, r24.compareTo(r12));
		System.assertEquals(-1, r12.compareTo(r34));
		System.assertEquals(1, r34.compareTo(r12));

		System.assertEquals(0, r13.compareTo(r13));
		System.assertEquals(-1, r13.compareTo(r14));
		System.assertEquals(1, r14.compareTo(r13));
		System.assertEquals(-1, r13.compareTo(r23));
		System.assertEquals(1, r23.compareTo(r13));
		System.assertEquals(-1, r13.compareTo(r24));
		System.assertEquals(1, r24.compareTo(r13));
		System.assertEquals(-1, r13.compareTo(r34));
		System.assertEquals(1, r34.compareTo(r13));

		System.assertEquals(0, r14.compareTo(r14));
		System.assertEquals(-1, r14.compareTo(r23));
		System.assertEquals(1, r23.compareTo(r14));
		System.assertEquals(-1, r14.compareTo(r24));
		System.assertEquals(1, r24.compareTo(r14));
		System.assertEquals(-1, r14.compareTo(r34));
		System.assertEquals(1, r34.compareTo(r14));

		System.assertEquals(0, r23.compareTo(r23));
		System.assertEquals(-1, r23.compareTo(r24));
		System.assertEquals(1, r24.compareTo(r23));
		System.assertEquals(-1, r23.compareTo(r34));
		System.assertEquals(1, r34.compareTo(r23));

		System.assertEquals(0, r24.compareTo(r24));
		System.assertEquals(-1, r24.compareTo(r34));
		System.assertEquals(1, r34.compareTo(r24));

		System.assertEquals(0, r34.compareTo(r34));
	}
	
	@isTest static void testRangeTime() {
		System.assertEquals(1, AttCalcRangesDto.R(60,61).time());
		System.assertEquals(0, AttCalcRangesDto.R(60,60).time());
		System.assertEquals(0, AttCalcRangesDto.R(60,59).time());
	}
	
	@isTest static void testRangeOr() {
		System.assertEquals(null,AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,59)));
		System.assertEquals(true, AttCalcRangesDto.R(50,120).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,60))));
		System.assertEquals(true, AttCalcRangesDto.R(50,120).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,61))));
		System.assertEquals(true, AttCalcRangesDto.R(50,120).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,120))));
		System.assertEquals(true, AttCalcRangesDto.R(50,121).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,121))));
		System.assertEquals(true, AttCalcRangesDto.R(50,121).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,121))));
		System.assertEquals(null, AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120),(AttCalcRangesDto.R(121,130))));
		System.assertEquals(true, AttCalcRangesDto.R(60,130).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(120,130))));
		System.assertEquals(true, AttCalcRangesDto.R(60,130).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(119,130))));
		System.assertEquals(true, AttCalcRangesDto.R(60,130).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(60,130))));
		System.assertEquals(true, AttCalcRangesDto.R(59,130).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(59,130))));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(60,60))));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(100,100))));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.orRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(120,120))));
	}
	
	@isTest static void testRangeAnd() {
		System.assertEquals(null,AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,59)));
		System.assertEquals(null,AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,60)));
		System.assertEquals(true, AttCalcRangesDto.R(60,61).equals(AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,61))));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,120))));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,121))));
		System.assertEquals(null,AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(121,130)));
		System.assertEquals(null,AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(120,130)));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(50,121))));
		System.assertEquals(true, AttCalcRangesDto.R(119,120).equals(AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(119,130))));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(60,130))));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(59,130))));
		System.assertEquals(null,AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(60,60)));
		System.assertEquals(null,AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(100,100)));
		System.assertEquals(null,AttCalcRangeDto.andRange(AttCalcRangesDto.R(60,120), AttCalcRangesDto.R(120,120)));
	}
	
	@isTest static void testRangeCutBefore() {
		System.assertEquals(null, AttCalcRangesDto.R(60,120).cutBefore(59));
		System.assertEquals(null, AttCalcRangesDto.R(60,120).cutBefore(60));
		System.assertEquals(true, AttCalcRangesDto.R(60,61).equals(AttCalcRangesDto.R(60,120).cutBefore(61)));
		System.assertEquals(true, AttCalcRangesDto.R(60,119).equals( AttCalcRangesDto.R(60,120).cutBefore(119)));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangesDto.R(60,120).cutBefore(120)));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangesDto.R(60,120).cutBefore(121)));
	}
	
	@isTest static void testRangeCutAfter() {
		System.assertEquals(null, AttCalcRangesDto.R(60,120).cutAfter(121));
		System.assertEquals(null, AttCalcRangesDto.R(60,120).cutAfter(120));
		System.assertEquals(true, AttCalcRangesDto.R(119,120).equals( AttCalcRangesDto.R(60,120).cutAfter(119)));
		System.assertEquals(true, AttCalcRangesDto.R(61,120).equals(AttCalcRangesDto.R(60,120).cutAfter(61)));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangesDto.R(60,120).cutAfter(60)));
		System.assertEquals(true, AttCalcRangesDto.R(60,120).equals(AttCalcRangesDto.R(60,120).cutAfter(59)));
	}

	
	@isTest static void testRangesMerge() {
		AttCalcRangesDto rs = AttCalcRangesDto.RS();
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(10,20));
		System.assertEquals('[10-20]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(30,40));
		System.assertEquals('[10-20,30-40]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(1,5));
		System.assertEquals('[1-5,10-20,30-40]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(22,25));
		System.assertEquals('[1-5,10-20,22-25,30-40]',rs.toString());
		rs = new AttCalcRangesDto(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(5, 10));
		System.assertEquals('[5-20,30-40,50-60]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(20, 25));
		System.assertEquals('[5-25,30-40,50-60]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(26, 30));
		System.assertEquals('[5-25,26-40,50-60]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(40, 45));
		System.assertEquals('[5-25,26-45,50-60]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(46, 50));
		System.assertEquals('[5-25,26-45,46-60]',rs.toString());
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(60, 65));
		System.assertEquals('[5-25,26-45,46-65]',rs.toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
		rs = rs.insertAndMerge(new AttCalcRangesDto(new List<AttCalcRangeDto>{AttCalcRangesDto.R(5,10),AttCalcRangesDto.R(20,25),AttCalcRangesDto.R(26,30),AttCalcRangesDto.R(40,45),AttCalcRangesDto.R(46,50),AttCalcRangesDto.R(60,65)}));
		System.assertEquals('[5-25,26-45,46-65]',rs.toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(20, 30));
		System.assertEquals('[10-40,50-60]',rs.toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(5, 30));
		System.assertEquals('[5-40,50-60]',rs.toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(15, 55));
		System.assertEquals('[10-60]',rs.toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(30, 65));
		System.assertEquals('[10-20,30-65]',rs.toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
		rs = rs.insertAndMerge(AttCalcRangesDto.RS(5, 65));
		System.assertEquals('[5-65]',rs.toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10,20), AttCalcRangesDto.R(30,40), AttCalcRangesDto.R(50,60)});
	}

	
	@isTest static void testRangesMask() {
		AttCalcRangesDto rs = new AttCalcRangesDto(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[]', rs.include(AttCalcRangesDto.RS(50,59)).toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[]', rs.include(AttCalcRangesDto.RS(50,60)).toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[60-61]', rs.include(AttCalcRangesDto.RS(50,61)).toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[100-110]', rs.include(new AttCalcRangesDto(new List<AttCalcRangeDto>{AttCalcRangesDto.R(50,60),AttCalcRangesDto.R(100,110),AttCalcRangesDto.R(120,130)})).toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,89),AttCalcRangesDto.R(90,120)});
		System.assertEquals('[80-89,90-110]', rs.include(AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(50,60),AttCalcRangesDto.R(80,110),AttCalcRangesDto.R(120,130)})).toString());
	}

	
	@isTest static void testRangesExclude() {
		AttCalcRangesDto rs = new AttCalcRangesDto(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[60-120]', rs.exclude(AttCalcRangesDto.RS(50,59)).toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[60-120]', rs.exclude(AttCalcRangesDto.RS(50,60)).toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[61-120]', rs.exclude(AttCalcRangesDto.RS(50,61)).toString());
		rs = AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(60,120)});
		System.assertEquals('[60-100,110-120]', rs.exclude(AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(50,60),AttCalcRangesDto.R(100,110),AttCalcRangesDto.R(120,130)})).toString());
	}
}