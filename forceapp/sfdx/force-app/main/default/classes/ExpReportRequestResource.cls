/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * @description 経費申請のAPIを提供するリソースクラス
 */
public with sharing class ExpReportRequestResource {
	
	/**
	 * @desctiprion 経費承認申請可否チェックのパラメータを格納するクラス
	 */
	public class CheckExpReportRequestParam implements RemoteApi.RequestParam {
		/** 経費精算ID */
		public String reportId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 経費精算ID
			ExpCommonUtil.validateId('reportId', reportId, true);
		}
	}

	/**
	 * @desctiprion 経費承認申請可否チェックの結果を格納するクラス
	 */
	public class CheckExpReportRequestResponse implements RemoteApi.ResponseParam {
		/** 確認メッセージ一覧 */
		public List<String> confirmation;
	}

	/**
	 * @description 経費承認申請可否チェックAPIを実装するクラス
	 */
	public with sharing class CheckExpReportRequestApi extends RemoteApi.ResourceBase {

		/**
		 * @description 経費承認申請可否チェックを行う
		 * @param req リクエストパラメータ(CheckExpReportRequestRequest)
		 * @return レスポンス(CheckExpReportRequestResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			CheckExpReportRequestParam param = (CheckExpReportRequestParam)req.getParam(ExpReportRequestResource.CheckExpReportRequestParam.class);

			// パラメータ値の検証
			param.validate();

			// 経費精算機能が有効か確認 Check if the expense module is available
			ExpCommonUtil.canUseExpense();

			// 経費承認申請可否のチェックを行う
			List<String> confirmationList = new ExpReportRequestService().checkExpReportRequest((Id)param.reportId);

			// レスポンスをセットする
			CheckExpReportRequestResponse res = new CheckExpReportRequestResponse();
			res.confirmation = confirmationList;

			return res;
		}
	}


	/**
	 * @description 経費承認申請のパラメータを格納するクラス
	 */
	public class SubmitExpReportRequestParam implements RemoteApi.RequestParam {
		/** 経費精算ID */
		public String reportId;
		/** 申請コメント */
		public String comment;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 経費精算ID
			ExpCommonUtil.validateId('reportId', reportId, true);
		}
	}
	
	/**
	 * @desctiprion 経費承認申請APIのレスポンスパラメータ
	 */
	public class SubmitExpReportRequestResponse implements RemoteApi.ResponseParam {
		// レスポンスパラメータなし
	}

	/**
	 * @description 経費承認申請APIを実装するクラス
	 */
	public with sharing class SubmitExpReportRequestApi extends RemoteApi.ResourceBase {

		/** ロールバックテストをする場合にtrue */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * @description 承認申請を実行する
		 * @param  req リクエストパラメータ(SubmitExpReportRequestParam)
		 * @return レスポンス
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SubmitExpReportRequestParam param = (SubmitExpReportRequestParam)req.getParam(ExpReportRequestResource.SubmitExpReportRequestParam.class);

			// パラメータ値の検証
			param.validate();

			//経費精算機能が有効か確認
			ExpCommonUtil.canUseExpense();

			// 失敗時はDBをロールバックする
			Savepoint sp = Database.setSavepoint();

			try {
				// 承認申請実行
				new ExpReportRequestService().submitRequest(param.reportId, param.comment);

				// ロールバックテスト
				// 強制的に例外を発生させてロールバックする
				if (isRollbackTest) {
					throw new App.IllegalStateException('SubmitExpReportRequestApi Rollback Test');
				}
				
				// レスポンスをセットする
				SubmitExpReportRequestResponse res = new SubmitExpReportRequestResponse();
				return res;
			} catch (Exception e) {
				// ロールバック
				Database.rollback(sp);
				throw e;
			}
		}
	}


	/**
	 * @description 経費承認申請取消のパラメータを格納するクラス
	 */
	public class CancelExpReportRequestParam implements RemoteApi.RequestParam {
		/** 申請ID */
		public String requestId;
		/** 申請コメント */
		public String comment;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 経費精算ID
			ExpCommonUtil.validateId('requestId', requestId, true);
		}
	}

	/**
	 * @desctiprion 経費承認申請取り消しAPIのレスポンスパラメータ
	 */
	public class CancelExpReportRequestResponse implements RemoteApi.ResponseParam {
		// レスポンスパラメータなし
	}

	/**
	 * @description 経費承認取消APIを実装するクラス
	 */
	public with sharing class CancelExpReportRequestApi extends RemoteApi.ResourceBase {

		/** ロールバックテストをする場合にtrue */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * @description 経費承認申請の取消を行う
		 * @param  req リクエストパラメータ(CancelExpReportRequest)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			CancelExpReportRequestParam param = (CancelExpReportRequestParam)req.getParam(ExpReportRequestResource.CancelExpReportRequestParam.class);

			// パラメータ値の検証
			param.validate();

			//経費精算機能が有効か確認
			ExpCommonUtil.canUseExpense();

			// 失敗時はDBをロールバックする
			Savepoint sp = Database.setSavepoint();

			try {
				// 取消実行
				new ExpReportRequestService().cancelRequest(param.requestId, param.comment);

				// ロールバックテスト
				// 強制的に例外を発生させてロールバックする
				if (isRollbackTest) {
					throw new App.IllegalStateException('CancelExpReportRequestApi Rollback Test');
				}
				
				// レスポンスをセットする
				CancelExpReportRequestResponse res = new CancelExpReportRequestResponse();
				return res;
			} catch (Exception e) {
				// ロールバック
				Database.rollback(sp);
				throw e;
			}
		}
	}
	
}