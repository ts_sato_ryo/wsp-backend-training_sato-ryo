/**
 * 勤務パターンのエンティティ
 */
public with sharing class AttPatternEntity extends AttPatternGeneratedEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 勤務パターン名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 表示順の最小値 */
	public static final Integer ORDER_MIN_VALUE = 1;
	/** 表示順の最大値 */
	public static final Integer ORDER_MAX_VALUE = 9999;

	/**
	 * コンストラクタ
	 */
	public AttPatternEntity() {
		super(new AttPattern__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttPatternEntity(AttPattern__c sobj) {
		super(sobj);
	}

	/** 勤務パターン名(多言語対応。参照専用) */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
		private set;
	}
}