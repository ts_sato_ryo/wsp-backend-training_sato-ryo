/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * @description 費目グループマスタのリポジトリのテスト
 */
@isTest
private class ExpTypeGroupRepositoryTest {

	/** テストデータ */
	private class RepoTestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;

		/** コンストラクタ */
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
		}

		/**
		 * 費目グループオブジェクトを作成する
		 */
		public List<ExpTypeGroup__c> createExpTypeGroups(String name, Integer size) {
			return ComTestDataUtility.createExpTypeGroups(name, this.companyObj.Id, size);
		}

		/**
		 * 費目グループエンティティを作成する(DB保存なし)
		 */
		public ExpTypeGroupEntity createExpTypeGroupEntity(String name) {
			ExpTypeGroupEntity entity = new ExpTypeGroupEntity();

			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.companyObj.Code__c + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.companyObj.Id;
			entity.parentId = null;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.order = 1;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);

			return entity;
		}

		/**
		 * 会社オブジェクトを作成する
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのエンティティが取得できることを確認する
	 * 各項目の値が取得できているかの確認はsearchEntityTestで実施
	 */
	@isTest static void getEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpTypeGroup__c> testGroups = testData.createExpTypeGroups('検索テスト', recordSize);
		ExpTypeGroup__c targetObj = testGroups[1];

		ExpTypeGroupRepository repo = new ExpTypeGroupRepository();
		ExpTypeGroupEntity resEntity;

		// 存在するIDを指定する
		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);

		// 存在しないIDを指定する
		delete targetObj;
		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * 正しく検索できることを確認する
	 * 有効期間での検索はsearchEntityTestValidDateで実施
	 */
	@isTest static void searchEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ComCompany__c testCompanyObj = testData.createCompany('検索テスト会社');
		List<ExpTypeGroup__c> testGroups = testData.createExpTypeGroups('検索テスト', recordSize);
		ExpTypeGroup__c targetObj = testGroups[1];

		ExpTypeGroupRepository repo = new ExpTypeGroupRepository();
		ExpTypeGroupRepository.SearchFilter filter;
		List<ExpTypeGroupEntity> resEntities;

		// 費目グループIDで検索
		filter = new ExpTypeGroupRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
		// 項目値の検証
		assertFieldValue(targetObj, resEntities[0]);
	}

	/**
	 * エンティティ取得のテスト
	 * 指定した対象日に有効なエンティティが取得できることを確認する
	 */
	@isTest static void searchEntityTestValidDate() {

		// テストデータ作成
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ComCompany__c testCompanyObj = testData.createCompany('検索テスト会社');
		List<ExpTypeGroup__c> targetObjs = testData.createExpTypeGroups('検索テスト', recordSize);
		ExpTypeGroup__c targetObj1 = targetObjs[0];
		ExpTypeGroup__c targetObj2 = targetObjs[1];
		ExpTypeGroup__c targetObj3 = targetObjs[2];
		targetObj2.ValidFrom__c = targetObj1.ValidTo__c;
		targetObj2.ValidTo__c = targetObj2.ValidFrom__c.addYears(1);
		targetObj3.ValidFrom__c = targetObj2.ValidTo__c;
		targetObj3.ValidTo__c = targetObj3.ValidFrom__c.addYears(1);
		update targetObjs;

		ExpTypeGroupRepository repo = new ExpTypeGroupRepository();
		ExpTypeGroupRepository.SearchFilter filter;
		List<ExpTypeGroupEntity> resEntities;

		// 取得対象日に履歴の有効期間開始日を指定
		filter = new ExpTypeGroupRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj2.ValidFrom__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		filter = new ExpTypeGroupRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c.addDays(-1));
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj3.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日を指定
		// 取得できないはず
		filter = new ExpTypeGroupRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());
	}

	/**
	 * saveEntityのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ExpTypeGroupEntity group1 = testData.createExpTypeGroupEntity('費目グループ1');

		// テスト実行
		ExpTypeGroupRepository repo = new ExpTypeGroupRepository();
		Repository.SaveResult result = repo.saveEntity(group1);

		// 確認
		ExpTypeGroup__c resObj1 = getGroupObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(group1, resObj1);
	}

	/**
	 * saveEntityListのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ExpTypeGroupEntity group1 = testData.createExpTypeGroupEntity('費目グループ1');
		ExpTypeGroupEntity group2 = testData.createExpTypeGroupEntity('費目グループ2');
		group2.validFrom = AppDate.today();
		group2.validTo = group2.validFrom.addMonths(12);
		group2.parentId = group1.id;

		// テスト実行
		ExpTypeGroupRepository repo = new ExpTypeGroupRepository();
		Repository.SaveResult result = repo.saveEntityList(new List<ExpTypeGroupEntity>{group1, group2});

		// 確認
		ExpTypeGroup__c resObj1 = getGroupObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(group1, resObj1);
		ExpTypeGroup__c resObj2 = getGroupObj(result.details[1].id);
		System.assertNotEquals(null, resObj2);
		assertFieldValue(group2, resObj2);
	}

	/**
	 * 費目グループオブジェクトを取得する
	 * @param groupId 取得対象の費目グループID
	 * @return 費目グループオブジェクト
	 */
	private static ExpTypeGroup__c getGroupObj(Id groupId) {
		return [
				SELECT
					Id,
					Name,
					Code__c,
					UniqKey__c,
					Name_L0__c,
					Name_L1__c,
					Name_L2__c,
					CompanyId__c,
					ParentId__c,
					Description_L0__c,
					Description_L1__c,
					Description_L2__c,
					Order__c,
					ValidFrom__c,
					ValidTo__c
				FROM
					ExpTypeGroup__c
				WHERE
					Id = :groupId
		];
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(ExpTypeGroupEntity expEntity, ExpTypeGroup__c actObj) {

		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
		System.assertEquals(expEntity.parentId, actObj.ParentId__c);
		System.assertEquals(expEntity.descriptionL0, actObj.Description_L0__c);
		System.assertEquals(expEntity.descriptionL1, actObj.Description_L1__c);
		System.assertEquals(expEntity.descriptionL2, actObj.Description_L2__c);
		System.assertEquals(expEntity.order, Integer.valueOf(actObj.Order__c));
		System.assertEquals(expEntity.validFrom, AppDate.valueOf(actObj.ValidFrom__c));
		System.assertEquals(expEntity.validTo, AppDate.valueOf(actObj.ValidTo__c));
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(ExpTypeGroup__c expObj, ExpTypeGroupEntity actEntity) {

		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.ParentId__c, actEntity.parentId);
		System.assertEquals(expObj.Description_L0__c, actEntity.descriptionL0);
		System.assertEquals(expObj.Description_L1__c, actEntity.descriptionL1);
		System.assertEquals(expObj.Description_L2__c, actEntity.descriptionL2);
		System.assertEquals(expObj.Order__c, actEntity.order);
		System.assertEquals(AppDate.valueOf(expObj.ValidFrom__c), actEntity.validFrom);
		System.assertEquals(AppDate.valueOf(expObj.ValidTo__c), actEntity.validTo);
	}

}