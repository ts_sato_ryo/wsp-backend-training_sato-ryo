/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group アプリ
 *
 * パッケージライセンスのリポジトリクラス
 */
public with sharing class AppPackageLicenseRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				PackageLicense.Id,
				PackageLicense.NamespacePrefix,
				PackageLicense.Status
		};
	}

	/**
	 * パッケージライセンスを取得する
	 * @param namespacePrefix 名前空間プレフィクス
	 */
	public AppPackageLicenseEntity getEntity(String namespacePrefix) {
		SearchFilter filter = new SearchFilter();
		filter.namespacePrefix = namespacePrefix;

		List<AppPackageLicenseEntity> licenseList = searchEntityList(filter);
		if (licenseList == null || licenseList.isEmpty()) {
			return null;
		}

		return licenseList[0];
	}

	/**
	 * 検索条件
	 */
	private class SearchFilter {
		/** 名前空間プレフィックス */
		String NamespacePrefix;
	}

	/**
	 * パッケージライセンスを検索する
	 * @param filter 検索条件
	 * @return 条件に一致したパッケージライセンス
	 */
	private List<AppPackageLicenseEntity> searchEntityList(SearchFilter filter) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final String pNamespacePrefix = filter.namespacePrefix;
		if (pNamespacePrefix != null) {
			condList.add(getFieldName(PackageLicense.NamespacePrefix) + ' = :pNamespacePrefix');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + PackageLicense.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond;

		// クエリ実行
		System.debug('AppPackageLicenseRepository.searchEntityList: SOQL ==>' + soql);
		List<PackageLicense> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<AppPackageLicenseEntity> entityList = new List<AppPackageLicenseEntity>();
		for (PackageLicense license : sObjList) {
			entityList.add(createEntity(license));
		}

		return entityList;
	}

	/**
	 * PackageLicenseからAppPackageLicenseEntityを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AppPackageLicenseEntity createEntity(PackageLicense sObj) {
		AppPackageLicenseEntity entity = new AppPackageLicenseEntity();
		entity.setId(sObj.Id);
		entity.namespacePrefix = sObj.NamespacePrefix;
		entity.status = AppPackageLicenseStatus.valueOf(sObj.Status);

		return entity;
	}
}