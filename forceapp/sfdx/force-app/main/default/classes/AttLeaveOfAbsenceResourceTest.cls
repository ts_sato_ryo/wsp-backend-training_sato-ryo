/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 勤怠
*
* @description AttLeaveOfAbsenceResourceのテストクラス
*/
@isTest
private class AttLeaveOfAbsenceResourceTest {

	/*
	 * 検索APIのテスト
	 * パラメータのIDが不正な場合、異常終了する事を検証する
	 */
	@isTest
	static void searchApiTestInvalidId() {
		AttLeaveOfAbsenceResource.SearchRequest request = new AttLeaveOfAbsenceResource.SearchRequest();
		request.id = 'invalid';

		try {
			Test.startTest();
			AttLeaveOfAbsenceResource.SearchApi api = new AttLeaveOfAbsenceResource.SearchApi();
			AttLeaveOfAbsenceResource.SearchResponse result = (AttLeaveOfAbsenceResource.SearchResponse)api.execute(request);
			Test.stopTest();
			System.assert(false);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), e.getMessage());
		}
	}

	/*
	 * 検索APIのテスト
	 * パラメータの会社IDが不正な場合、異常終了する事を検証する
	 */
	@isTest
	static void searchApiTestInvalidCompanyId() {
		AttLeaveOfAbsenceResource.SearchRequest request = new AttLeaveOfAbsenceResource.SearchRequest();
		request.companyId = 'invalid';

		try {
			Test.startTest();
			AttLeaveOfAbsenceResource.SearchApi api = new AttLeaveOfAbsenceResource.SearchApi();
			AttLeaveOfAbsenceResource.SearchResponse result = (AttLeaveOfAbsenceResource.SearchResponse)api.execute(request);
			Test.stopTest();
			System.assert(false);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}), e.getMessage());
		}
	}

	/*
	 * 検索APIのテスト
	 * エンティティからレスポンスに正しく変換する事を検証する
	 */
	@isTest
	static void searchApiTestConvertResponse() {
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.setId('001xa000003DIlo');
		entity.code = 'code';
		entity.companyId = 'companyId';
		entity.name = 'name';
		entity.nameL0 = 'nameL0';
		entity.nameL1 = 'nameL1';
		entity.nameL2 = 'nameL2';

		AttLeaveOfAbsenceResource.SearchApi api = new AttLeaveOfAbsenceResource.SearchApi();
		AttLeaveOfAbsenceResource.LeaveOfAbsence response = api.toResponse(entity);
		System.assertEquals(entity.id, response.id);
		System.assertEquals('code', response.code);
		System.assertEquals('companyId', response.companyId);
		System.assertEquals(entity.nameL.getValue(), response.name);
		System.assertEquals('nameL0', response.name_L0);
		System.assertEquals('nameL1', response.name_L1);
		System.assertEquals('nameL2', response.name_L2);
	}

	/*
	 * 検索APIのテスト
	 * idを条件に検索出来る事を検証する
	 */
	@isTest
	static void searchApiTestSearchById() {
		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'code', false);

		// API実行
		Test.startTest();
		AttLeaveOfAbsenceResource.SearchRequest request = new AttLeaveOfAbsenceResource.SearchRequest();
		request.id = leaveOfAbsence.id;
		AttLeaveOfAbsenceResource.SearchApi api = new AttLeaveOfAbsenceResource.SearchApi();
		AttLeaveOfAbsenceResource.SearchResponse result = (AttLeaveOfAbsenceResource.SearchResponse)api.execute(request);
		Test.stopTest();

		// 検証
		System.assertEquals(1, result.records.size());
		System.assertEquals(request.id, result.records[0].id);
	}

	/*
	 * 検索APIのテスト
	 * 会社IDを条件に検索出来る事、コードの昇順で取得する事を検証する
	 */
	@isTest
	static void searchApiTestSearchByCompanyId() {
		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		List<ComCompany__c> companies = new List<ComCompany__c> {
			ComTestDataUtility.createCompany('Company1', country.id),
			ComTestDataUtility.createCompany('Company2', country.id)
		};
		List<AttLeaveOfAbsence__c> leaveOfAbsences = new List<AttLeaveOfAbsence__c> {
			ComTestDataUtility.createLeaveOfAbsence(companies[1], 'code1', false),
			ComTestDataUtility.createLeaveOfAbsence(companies[0], 'code3', false),
			ComTestDataUtility.createLeaveOfAbsence(companies[0], 'code4', false),
			ComTestDataUtility.createLeaveOfAbsence(companies[0], 'code5', true),
			ComTestDataUtility.createLeaveOfAbsence(companies[0], 'code2', false)
		};

		// API実行
		Test.startTest();
		AttLeaveOfAbsenceResource.SearchRequest request = new AttLeaveOfAbsenceResource.SearchRequest();
		request.companyId = companies[0].id;
		AttLeaveOfAbsenceResource.SearchApi api = new AttLeaveOfAbsenceResource.SearchApi();
		AttLeaveOfAbsenceResource.SearchResponse result = (AttLeaveOfAbsenceResource.SearchResponse)api.execute(request);
		Test.stopTest();

		// 検証
		System.assertEquals(3, result.records.size());
		System.assertEquals(leaveOfAbsences[4].id, result.records[0].id);
		System.assertEquals(leaveOfAbsences[1].id, result.records[1].id);
		System.assertEquals(leaveOfAbsences[2].id, result.records[2].id);
	}

	/*
	 * 検索APIのテスト
	 * 検索結果が存在しない場合、レスポンスがnullではない事を検証する
	 */
	@isTest
	static void searchApiTestNoResult() {
		// API実行
		Test.startTest();
		AttLeaveOfAbsenceResource.SearchRequest request = new AttLeaveOfAbsenceResource.SearchRequest();
		AttLeaveOfAbsenceResource.SearchApi api = new AttLeaveOfAbsenceResource.SearchApi();
		AttLeaveOfAbsenceResource.SearchResponse result = (AttLeaveOfAbsenceResource.SearchResponse)api.execute(request);
		Test.stopTest();

		// 検証
		System.assert(result.records != null);
		System.assert(result.records.isEmpty());
	}

	/*
	 * 登録APIのテスト
	 * 休職休業マスタに登録できることを検証する
	 */
	@isTest
	static void createApiTestSaveEntity() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		request.companyId = ComTestDataUtility.createTestCompany().id;
		request.code = '001';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(request);

		// 検証
		List<AttLeaveOfAbsence__c> result = [SELECT Id, UniqKey__c, CompanyId__c, Code__c, Name, Name_L0__c, Name_L1__c, Name_L2__c, Removed__c  FROM AttLeaveOfAbsence__c];
		System.assertEquals(1, result.size());
		System.assertEquals(response.id, result[0].Id);
		System.assertEquals('testComp1-001', result[0].UniqKey__c);
		System.assertEquals(request.companyId, result[0].CompanyId__c);
		System.assertEquals('001', result[0].Code__c);
		System.assert(result[0].Name != null);
		System.assertEquals('leave1_L0', result[0].Name_L0__c);
		System.assertEquals('leave1_L1', result[0].Name_L1__c);
		System.assertEquals('leave1_L2', result[0].Name_L2__c);
		System.assert(!result[0].Removed__c);
	}

	/*
	 * 登録APIのテスト
	 * リクエストパラメータが必須項目のみの場合、休職休業マスタに登録できることを検証する
	 */
	@isTest
	static void createApiTestSaveEntityOnlyRequired() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		Id companyId = ComTestDataUtility.createTestCompany().id;
		Map<String, Object> requestParamMap = new Map<String, Object> {
			'companyId' => companyId,
			'code' => '001',
			'name_L0' => 'leave1_L0'
		};

		// API実行
		AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(requestParamMap);

		// 検証
		List<AttLeaveOfAbsence__c> result = [SELECT Id, UniqKey__c, CompanyId__c, Code__c, Name, Name_L0__c, Name_L1__c, Name_L2__c, Removed__c  FROM AttLeaveOfAbsence__c];
		System.assertEquals(1, result.size());
		System.assertEquals(response.id, result[0].Id);
		System.assertEquals('testComp1-001', result[0].UniqKey__c);
		System.assertEquals(companyId, result[0].CompanyId__c);
		System.assertEquals('001', result[0].Code__c);
		System.assert(result[0].Name != null);
		System.assertEquals('leave1_L0', result[0].Name_L0__c);
		System.assert(result[0].Name_L1__c == null);
		System.assert(result[0].Name_L2__c == null);
		System.assert(!result[0].Removed__c);
	}

	/*
	 * 登録APIのテスト
	 * リクエストからエンティティに正しく変換できることを検証する
	 */
	@isTest
	static void createApiTestConvertEntity() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();

		request.companyId = ComTestDataUtility.createTestCompany().id;
		request.code = '001';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		Map<String, Object> paramMap = new Map<String, Object> {
			'companyId' => request.companyId,
			'code' => request.code,
			'name_L0' => request.name_L0,
			'name_L1' => request.name_L1,
			'name_L2' => request.name_L2
		};

		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();
		AttLeaveOfAbsenceEntity entity = api.toEntity(request, paramMap);

		// 検証
		System.assertEquals(request.companyId, entity.companyId);
		System.assertEquals('001', entity.code);
		System.assertEquals('leave1_L0', entity.nameL0);
		System.assertEquals('leave1_L1', entity.nameL1);
		System.assertEquals('leave1_L2', entity.nameL2);
		System.assertEquals('testComp1-001', entity.uniqKey);
	}

	/**
	 * 登録APIのテスト
	 * API実行権限を持っていない場合、異常終了することを検証する
	 */
	@isTest
	static void createApiTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 休職・休業管理権限を無効に設定する
		testData.permission.isManageAttLeaveOfAbsence = false;
		new PermissionRepository().saveEntity(testData.permission);

		Map<String, Object> requestParamMap = new Map<String, Object> {
			'companyId' => testData.company.id,
			'code' => '001',
			'name_L0' => 'leave1_L0'
		};

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();
				api.execute(requestParamMap);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/*
	 * 登録APIのテスト
	 * パラメータの会社レコードIDが設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void createApiTestInvalidCompanyIdNull() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		request.code = '001';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		try {
			AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('companyIdが設定されていません', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/*
	 * 登録APIのテスト
	 * パラメータの会社レコードIDが不正な場合、異常終了することを検証する
	 */
	@isTest
	static void createApiTestInvalidCompanyId() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		request.companyId = 'invalid';
		request.code = '001';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		try {
			AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('companyIdの値が正しくありません。(companyId=invalid)', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/*
	 * 登録APIのテスト
	 * パラメータの休職休業コードが設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void createApiTestInvalidCodeNull() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		request.companyId = ComTestDataUtility.createTestCompany().id;
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		try {
			AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('codeが設定されていません', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/*
	 * 登録APIのテスト
	 * パラメータの休職休業名(L0)が設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void createApiTestInvalidNameL0Null() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		request.companyId = ComTestDataUtility.createTestCompany().id;
		request.code = '001';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		try {
			AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('name_L0が設定されていません', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/*
	 * 登録APIのテスト
	 * エンティティのバリデーションチェックを通ることを検証する
	 */
	@isTest
	static void createApiTestInvalidEntity() {

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		request.companyId = ComTestDataUtility.createTestCompany().id;
		request.code = '123456789123456789123';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		try {
			AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('コードは20文字以下にしてください。', e.getMessage());
			System.assertEquals('INVALID_VALUE', e.getErrorCode());
		}
	}

	/*
	* 登録APIのテスト
	* コードが重複している場合、異常終了することを検証する
	*/
	@isTest
	static void createApiTestDuplicateCode() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate', false);

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();
		AttLeaveOfAbsenceResource.CreateApi api = new AttLeaveOfAbsenceResource.CreateApi();

		request.companyId = company.Id;
		request.code = 'duplicate';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		try {
			AttLeaveOfAbsenceResource.CreateResponse response = (AttLeaveOfAbsenceResource.CreateResponse)api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('コードが重複しています。', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/*
	* 登録APIのテスト
	* コードの重複を判定できることを検証する(重複あり)
	*/
	@isTest
	static void createApiTestIsExistCodeTrue() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate', false);

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();

		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.companyId = company.Id;
		entity.code = 'duplicate';
		entity.nameL0 = 'leave1_L0';
		entity.nameL1 = 'leave1_L1';
		entity.nameL2 = 'leave1_L2';

		// API実行
		Boolean result = AttLeaveOfAbsenceResource.isExistCode(entity, false);

		// 検証
		System.assert(result);
	}

	/*
	* 登録APIのテスト
	* コードの重複を判定できることを検証する(重複なし)
	*/
	@isTest
	static void createApiTestIsExistCodeFalse() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate', false);

		AttLeaveOfAbsenceResource.CreateRequest request = new AttLeaveOfAbsenceResource.CreateRequest();

		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.companyId = company.Id;
		entity.code = 'not_duplicate';
		entity.nameL0 = 'leave1_L0';
		entity.nameL1 = 'leave1_L1';
		entity.nameL2 = 'leave1_L2';

		// API実行
		Boolean result = AttLeaveOfAbsenceResource.isExistCode(entity, false);

		// 検証
		System.assert(!result);
	}

	/*
	* 更新APIのテスト
	* 休職休業マスタを更新できることを検証する
	*/
	@isTest
	static void updateApiTestSaveEntity() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'code', false);

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();
		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		request.id = leaveOfAbsence.Id;
		request.code = '001';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		// API実行
		System.assert(api.execute(request) == null);

		// 検証
		List<AttLeaveOfAbsence__c> result = [SELECT Id, UniqKey__c, CompanyId__c, Code__c, Name, Name_L0__c, Name_L1__c, Name_L2__c, Removed__c  FROM AttLeaveOfAbsence__c];
		System.assertEquals(1, result.size());
		System.assertEquals('Company1-001', result[0].UniqKey__c);
		System.assertEquals(leaveOfAbsence.CompanyId__c, result[0].CompanyId__c);
		System.assertEquals('001', result[0].Code__c);
		System.assert(result[0].Name != null);
		System.assertEquals('leave1_L0', result[0].Name_L0__c);
		System.assertEquals('leave1_L1', result[0].Name_L1__c);
		System.assertEquals('leave1_L2', result[0].Name_L2__c);
		System.assert(!result[0].Removed__c);
	}

	/*
	* 更新APIのテスト
	* リクエストパラメータが必須項目のみの場合、休職休業マスタを更新できることを検証する
	*/
	@isTest
	static void updateApiTestSaveEntityOnlyRequired() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'code', false);

		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		Map<String, Object> requestParamMap = new Map<String, Object> {
			'Id' => leaveOfAbsence.Id,
			'code' => '001',
			'name_L0' => 'leave1_L0'
		};

		// API実行
		api.execute(requestParamMap);

		// 検証
		List<AttLeaveOfAbsence__c> result = [SELECT Id, UniqKey__c, CompanyId__c, Code__c, Name, Name_L0__c, Name_L1__c, Name_L2__c, Removed__c  FROM AttLeaveOfAbsence__c];
		System.assertEquals(1, result.size());
		System.assertEquals('Company1-001', result[0].UniqKey__c);
		System.assertEquals(leaveOfAbsence.CompanyId__c, result[0].CompanyId__c);
		System.assertEquals('001', result[0].Code__c);
		System.assert(result[0].Name != null);
		System.assertEquals('leave1_L0', result[0].Name_L0__c);
		System.assertEquals('code_L1', result[0].Name_L1__c);
		System.assertEquals('code_L2', result[0].Name_L2__c);
		System.assert(!result[0].Removed__c);
	}

	/*
	 * 更新APIのテスト
	 * リクエストからエンティティに正しく変換できることを検証する
	 */
	@isTest
	static void updateApiTestConvertEntity() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'code', false);

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();

		AttLeaveOfAbsenceRepository repo = new AttLeaveOfAbsenceRepository();
		AttLeaveOfAbsenceEntity entity = repo.searchbyId(leaveOfAbsence.Id);

		request.id = leaveOfAbsence.Id;
		request.code = '001';
		request.name_L0 = 'leave1_L0';
		request.name_L1 = 'leave1_L1';
		request.name_L2 = 'leave1_L2';

		Map<String, Object> paramMap = new Map<String, Object> {
			'Id' => request.id,
			'code' => request.code,
			'name_L0' => request.name_L0,
			'name_L1' => request.name_L1,
			'name_L2' => request.name_L2
		};

		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();
		api.setEntity(request, paramMap, entity);

		// 検証
		System.assertEquals(leaveOfAbsence.CompanyId__c, entity.companyId);
		System.assertEquals('001', entity.code);
		System.assertEquals('leave1_L0', entity.nameL0);
		System.assertEquals('leave1_L1', entity.nameL1);
		System.assertEquals('leave1_L2', entity.nameL2);
		System.assertEquals('Company1-001', entity.uniqKey);
	}

	/**
	 * 更新APIのテスト
	 * API実行権限を持っていない場合、異常終了することを検証する
	 */
	@isTest
	static void updateApiTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		AttLeaveOfAbsenceEntity leaveOfAbsence = testData.createLeaveOfAbsence('Test');

		// 休職・休業管理権限を無効に設定する
		testData.permission.isManageAttLeaveOfAbsence = false;
		new PermissionRepository().saveEntity(testData.permission);

		Map<String, Object> requestParamMap = new Map<String, Object> {
			'Id' => leaveOfAbsence.id,
			'code' => '001',
			'name_L0' => 'leave1_L0'
		};

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();
				api.execute(requestParamMap);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 更新APIのテスト
	 * 更新対象のレコードが存在しない場合、異常終了することを検証する
	 */
	@isTest
	static void updateApiTestNotExistRecord() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'code', false);

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();
		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		request.id = 'a0f7F0000016BbwQAE';
		request.code = '001';
		request.name_L0 = 'leave1_L0';

		// API実行
		try {
			api.execute(request);
			System.assert(false);
		} catch (App.RecordNotFoundException e) {
			// 検証
			System.assertEquals('対象データが見つかりませんでした。削除されている可能性があります。', e.getMessage());
			System.assertEquals('RECORD_NOT_FOUND', e.getErrorCode());
		}
	}

	/**
	 * 更新APIのテスト
	 * パラメータのIDが設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void updateApiTestInvalidIdNull() {

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();
		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		request.code = '001';
		request.name_L0 = 'leave1_L0';

		// API実行
		try {
			api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('idが設定されていません', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/**
	 * 更新APIのテスト
	 * パラメータのIDが不正な場合、異常終了することを検証する
	 */
	@isTest
	static void updateApiTestInvalidId() {

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();
		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		request.id = 'invalid';
		request.code = '001';
		request.name_L0 = 'leave1_L0';

		// API実行
		try {
			api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('idの値が正しくありません。(id=invalid)', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/**
	 * 更新APIのテスト
	 * エンティティのバリデーションチェックを通ることを検証する
	 */
	@isTest
	static void updateApiTestInvalidEntity() {

			// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'code', false);

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();
		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		request.id = leaveOfAbsence.Id;
		request.code = '123456789123456789123';
		request.name_L0 = 'leave1_L0';

		// API実行
		try {
			api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('コードは20文字以下にしてください。', e.getMessage());
			System.assertEquals('INVALID_VALUE', e.getErrorCode());
		}
	}

	/**
	 * 更新APIのテスト
	 * IDが同じでコードが重複している場合、レコードを更新できることを検証する
	 */
	@isTest
	static void updateApiTestDuplicateCodeSameId() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate', false);

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();
		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		request.id = leaveOfAbsence.Id;
		request.code = 'duplicate';
		request.name_L0 = 'leave1_L0';

		// API実行
		api.execute(request);

		// 検証
		List<AttLeaveOfAbsence__c> result = [SELECT Id, UniqKey__c, CompanyId__c, Code__c, Name, Name_L0__c, Name_L1__c, Name_L2__c, Removed__c FROM AttLeaveOfAbsence__c];
		System.assertEquals(1, result.size());
		System.assertEquals('Company1-duplicate', result[0].UniqKey__c);
		System.assertEquals('duplicate', result[0].Code__c);
		System.assertEquals('leave1_L0', result[0].Name_L0__c);
	}

	/**
	 * 更新APIのテスト
	 * IDが異なりコードが重複している場合、異常終了することを検証する
	 */
	@isTest
	static void updateApiTestDuplicateCodeNotSameId() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence1 = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate', false);
		AttLeaveOfAbsence__c leaveOfAbsence2 = ComTestDataUtility.createLeaveOfAbsence(company, 'not_duplicate', false);

		AttLeaveOfAbsenceResource.UpdateRequest request = new AttLeaveOfAbsenceResource.UpdateRequest();
		AttLeaveOfAbsenceResource.UpdateApi api = new AttLeaveOfAbsenceResource.UpdateApi();

		request.id = leaveOfAbsence2.Id;
		request.code = 'duplicate';
		request.name_L0 = 'leave1_L0';

		// API実行
		try {
			api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('コードが重複しています。', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/**
	 * 更新APIのテスト
	 * コードの重複を判定できることを検証する(重複あり)
	 */
	@isTest
	static void updateApiTestIsExistCodeTrue() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence1 = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate1', false);
		AttLeaveOfAbsence__c leaveOfAbsence2 = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate2', false);

		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.setId(leaveOfAbsence2.Id);
		entity.companyId = company.Id;
		entity.code = 'duplicate1';

		// API実行
		Boolean result = AttLeaveOfAbsenceResource.isExistCode(entity, true);

		// 検証
		System.assert(result);
	}

	/*
	 * 更新APIのテスト
	 * コードの重複を判定できることを検証する(重複なし)
	 */
	@isTest
	static void updateApiTestIsExistCodeFalse() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence1 = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate1', false);
		AttLeaveOfAbsence__c leaveOfAbsence2 = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate2', false);

		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.setId(leaveOfAbsence2.Id);
		entity.companyId = company.Id;
		entity.code = 'duplicate3';

		// API実行
		Boolean result = AttLeaveOfAbsenceResource.isExistCode(entity, true);

		// 検証
		System.assert(!result);
	}

	/*
	 * 更新APIのテスト
	 * コードの重複を判定できることを検証する(重複なし)
	 *	(コードは重複しているが、Idが同じため重複なしと判定する)
	 */
	@isTest
	static void updateApiTestIsExistCodeFalseSameId() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence1 = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate1', false);
		AttLeaveOfAbsence__c leaveOfAbsence2 = ComTestDataUtility.createLeaveOfAbsence(company, 'duplicate2', false);

		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.setId(leaveOfAbsence2.Id);
		entity.companyId = company.Id;
		entity.code = 'duplicate2';

		// API実行
		Boolean result = AttLeaveOfAbsenceResource.isExistCode(entity, true);

		// 検証
		System.assert(!result);
	}

	/*
	 * 削除APIのテスト
	 * 休職休業マスタからレコードを削除できることを検証する
	 */
	@isTest
	static void deleteApiTestDeleteEntity() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		AttLeaveOfAbsence__c leaveOfAbsence = ComTestDataUtility.createLeaveOfAbsence(company, 'code', false);

		AttLeaveOfAbsenceResource.DeleteRequest request = new AttLeaveOfAbsenceResource.DeleteRequest();
		AttLeaveOfAbsenceResource.DeleteApi api = new AttLeaveOfAbsenceResource.DeleteApi();

		List<AttLeaveOfAbsence__c> selectBefore = [SELECT Id FROM AttLeaveOfAbsence__c];
		System.assertEquals(leaveOfAbsence.Id, selectBefore[0].Id);
		request.id = selectBefore[0].Id;

		// API実行
		System.assert(api.execute(request) == null);
		List<AttLeaveOfAbsence__c> selectAfter = [SELECT Id FROM AttLeaveOfAbsence__c];
		System.assert(selectAfter.isEmpty());
	}

	/**
	 * 削除APIのテスト
	 * API実行権限を持っていない場合、異常終了することを検証する
	 */
	@isTest
	static void deleteApiTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		AttLeaveOfAbsenceEntity leaveOfAbsence = testData.createLeaveOfAbsence('Test');

		// 休職・休業管理権限を無効に設定する
		testData.permission.isManageAttLeaveOfAbsence = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttLeaveOfAbsenceResource.DeleteRequest request = new AttLeaveOfAbsenceResource.DeleteRequest();
		request.id = leaveOfAbsence.id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttLeaveOfAbsenceResource.DeleteApi api = new AttLeaveOfAbsenceResource.DeleteApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/*
	 * 削除APIのテスト
	 * パラメータのIDが設定されていない場合、異常終了することを検証する
	 */
	@isTest
	static void deleteApiTestInvalidIdNull() {

		AttLeaveOfAbsenceResource.DeleteRequest request = new AttLeaveOfAbsenceResource.DeleteRequest();
		AttLeaveOfAbsenceResource.DeleteApi api = new AttLeaveOfAbsenceResource.DeleteApi();

		// API実行
		try {
			api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('idが設定されていません', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/*
	 * 削除APIのテスト
	 * パラメータのIDが不正な場合、異常終了することを検証する
	 */
	@isTest
	static void deleteApiTestInvalidId() {

		AttLeaveOfAbsenceResource.DeleteRequest request = new AttLeaveOfAbsenceResource.DeleteRequest();
		AttLeaveOfAbsenceResource.DeleteApi api = new AttLeaveOfAbsenceResource.DeleteApi();

		request.id = 'invalid';

		// API実行
		try {
			api.execute(request);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals('idの値が正しくありません。(id=invalid)', e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}
}