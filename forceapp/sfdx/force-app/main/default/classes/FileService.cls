/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description ファイルのサービスクラス Service class for File upload
 */
public with sharing class FileService {
	private static final ComMsgBase MESSAGE = ComMessage.msg();
	
	/** 領収書ファイルタイトルのプレフィックス Prefix of receipt file title */
	@TestVisible
	private static final String EXP_RECEIPT_TITLE_PREFIX = 'Receipt';
	@TestVisible
	private static final String EXP_QUOTATION_TITLE_PREFIX = 'Quotation';
	
	/** タイプの定義(変更管理で使用する) Type definition */
	public enum Type {
		Expense,
		Quotation,
		Receipt
	}
	
	/** Map of Types with Name as the Key and the Type enum as the value */
	public static final Map<String, Type> TYPE_MAP;
	static {
		final Map<String, Type> typeMap = new Map<String, Type>();
		for (Type t : Type.values()) {
			typeMap.put(t.name(), t);
		}
		TYPE_MAP = typeMap;
	}
	
	/**
	 * ファイルを保存する Saves file into Salesforce Files
	 * @param fileName ファイル名 Filename
	 * @param body ファイルボディ Content of file
	 * @param type ファイルのタイプ Type of file
	 */
	public AppContentVersionEntity saveFile(String fileName, String fileBody, String type) {
		AppContentVersionRepository repo = new AppContentVersionRepository();

		AppContentVersionEntity entity = new AppContentVersionEntity();
		entity.title = (String)getFilePrefix(type) + AppDatetime.now().formatLocal();
		entity.pathOnClient = fileName;
		entity.versionData = EncodingUtil.base64Decode(fileBody);

		Repository.SaveResult result = repo.saveEntity(entity);

		AppContentVersionEntity savedEntity = repo.getEntity(result.details[0].id);

		return savedEntity;
	}
	
	/**
	 * ファイル一覧を取得する Retrieves list of files
	 * @param ownerId 所有者ID Owner ID
	 * @return 取得したエンティティのリスト List of AppContentDocumentEntities
	 */
	public List<AppContentDocumentEntity> getFileList(String type, Id ownerId) {
		List<AppContentDocumentEntity> fileList = new List<AppContentDocumentEntity>();

		// ファイルを取得 Retrieve files
		AppContentDocumentRepository repo = new AppContentDocumentRepository();
		List<AppContentDocumentEntity> documentList = repo.getEntityList((String)getFilePrefix(type), ownerId);

		if (documentList != null && !documentList.isEmpty()) {
			for (AppContentDocumentEntity document : documentList) {
				Boolean isLinked = false;

				// 既にレコードに紐づいているかどうかを確認 Check if there are any existing links with the files
				if (document.contentDocumentLinkList != null && !document.contentDocumentLinkList.isEmpty()) {
					for (AppContentDocumentLinkEntity link : document.contentDocumentLinkList) {
						if (link.linkedEntityId != ownerId) {
							isLinked = true;
							break;
						}
					}
				}

				// 既にレコードに紐づいているか場合は除外 Exclude record if link exists
				if (!isLinked) {
					fileList.add(document);
				}
			}
		}

		return fileList;
	}
	
	/**
	 * ファイルを取得する Retrieve file
	 * @param contentVersionId コンテンツバージョンID ContentVersion ID
	 * @return 取得したエンティティのリスト AppContentVersionEntity
	 */
	public AppContentVersionEntity getFile(Id contentVersionId) {
		return (new AppContentVersionRepository().getEntity(contentVersionId));
	}

	/**
	 * ファイルを削除する Delete file from Salesforce Files
	 * @param contentDocumentIdList コンテンツドキュメントIDのリスト List of ContentDocumentId
	 */
	public void deleteFile(List<Id> contentDocumentIdList) {
		AppContentDocumentRepository repo = new AppContentDocumentRepository();
		
		/*
		 * Deletion logic: If a document is not linked to any record, there will only be 1 Link Record which links the document to the user/owner
		 * If a document is linked to a record, an additional Link Record will be created which links the document to the record
		 * In general, if Number of Link Records > Number of Documents, there is at least 1 document linked to a record
		 */
		// if files are currently assoicated with other records, throw error
		if (repo.getNoOfLinksByContentDocumentId(contentDocumentIdList) != contentDocumentIdList.size()) {
			throw new App.ParameterException(MESSAGE.Com_Err_CannotDeleteReference);
		} else {
			// Retrieve AppContentDocumentEntities to be deleted
			List<AppContentDocumentEntity> contentList = new List<AppContentDocumentEntity>();
			for (Id contentDocumentId : contentDocumentIdList) {
				AppContentDocumentEntity content = new AppContentDocumentEntity();
				content.setId(contentDocumentId);
				contentList.add(content);
			}
	
			// ファイルを削除 Delete files
			(new AppContentDocumentRepository()).deleteEntityList(contentList);
		}
	}
	
	/**
	 * Retrieve List of ContentDocuments linked to the list of Salesforce IDs 
	 * @param linkedEntityIdList List of Salesforce IDs
	 * @return List of ContentDocument Entities
	 */
	public List<AppContentDocumentEntity> getContentDocumentEntityList(List<Id> linkedEntityIdList) {
		AppContentDocumentRepository repo = new AppContentDocumentRepository();

		// Retrieve ContentDocument IDs linked to the Salesforce IDs
		List<Id> contentDocumentIdList = new List<Id>();
		for (AppContentDocumentLinkEntity link : repo.getLinkEntityListByLinkedEntityId(linkedEntityIdList)) {
			contentDocumentIdList.add(link.contentDocumentId);
		}

		// Retrieve ContentDocument Entities
		List<AppContentDocumentEntity> contentDocumentEntityList = new List<AppContentDocumentEntity>();
		if (!contentDocumentIdList.isEmpty()) {
			contentDocumentEntityList = repo.getEntityList(contentDocumentIdList);
		}

		return contentDocumentEntityList;
	}
	
	
	/*
	 * Return the type value
	 * @param name Target type name
	 */
	private static Object getFilePrefix(String name) {
		Type type = FileService.TYPE_MAP.get(name);
		if (type == null) {
			throw new App.ParameterException('Unknown type : ' + name);
		}
		return getFilePrefix(type);
	}
	
	/**
	* Return the type value
	* @param type Target type to get the value
	* */
	private static Object getFilePrefix(FileService.Type type) {
		if (type == FileService.Type.Receipt) {
			return FileService.EXP_RECEIPT_TITLE_PREFIX;
		} else if (type == FileService.Type.Quotation) {
			return FileService.EXP_QUOTATION_TITLE_PREFIX;
		}
		return null;
	}
}