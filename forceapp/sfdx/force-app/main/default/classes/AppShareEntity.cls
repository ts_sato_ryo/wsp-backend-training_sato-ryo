/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通
 *
 * 共有オブジェクト用エンティティ Entity for share object
 */
public with sharing class AppShareEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		PARENT_ID,
		USER_OR_GROUP_ID
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;


	/** 対象レコードのID */
	public Id parentId {
		get;
		set {
			parentId = value;
			setChanged(Field.PARENT_ID);
		}
	}

	/** アクセス権を付与するユーザまたはグループのID */
	public Id userOrGroupId {
		get;
		set {
			userOrGroupId = value;
			setChanged(Field.USER_OR_GROUP_ID);
		}
	}

	/** コンストラクタ */
	public AppShareEntity() {
		isChangedFieldSet = new Set<AppShareEntity.Field>();
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}