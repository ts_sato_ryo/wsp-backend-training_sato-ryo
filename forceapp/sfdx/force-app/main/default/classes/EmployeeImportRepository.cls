/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 社員インポートのリポジトリ
 */
public with sharing class EmployeeImportRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ComEmployeeImport__c.Id,
			ComEmployeeImport__c.Name,
			ComEmployeeImport__c.ImportBatchId__c,
			ComEmployeeImport__c.RevisionDate__c,
			ComEmployeeImport__c.UserName__c,
			ComEmployeeImport__c.Code__c,
			ComEmployeeImport__c.CompanyCode__c,
			ComEmployeeImport__c.DisplayName_L0__c,
			ComEmployeeImport__c.DisplayName_L1__c,
			ComEmployeeImport__c.DisplayName_L2__c,
			ComEmployeeImport__c.FirstName_L0__c,
			ComEmployeeImport__c.FirstName_L1__c,
			ComEmployeeImport__c.FirstName_L2__c,
			ComEmployeeImport__c.LastName_L0__c,
			ComEmployeeImport__c.LastName_L1__c,
			ComEmployeeImport__c.LastName_L2__c,
			ComEmployeeImport__c.MiddleName_L0__c,
			ComEmployeeImport__c.MiddleName_L1__c,
			ComEmployeeImport__c.MiddleName_L2__c,
			ComEmployeeImport__c.HiredDate__c,
			ComEmployeeImport__c.ValidFrom__c,
			ComEmployeeImport__c.ValidTo__c,
			ComEmployeeImport__c.CostCenterBaseCode__c,
			ComEmployeeImport__c.DepartmentBaseCode__c,
			ComEmployeeImport__c.Title_L0__c,
			ComEmployeeImport__c.Title_L1__c,
			ComEmployeeImport__c.Title_L2__c,
			ComEmployeeImport__c.AdditionalDepartmentBaseCode__c,
			ComEmployeeImport__c.AdditionalTitle_L0__c,
			ComEmployeeImport__c.AdditionalTitle_L1__c,
			ComEmployeeImport__c.AdditionalTitle_L2__c,
			ComEmployeeImport__c.ManagerBaseCode__c,
			ComEmployeeImport__c.CalendarCode__c,
			ComEmployeeImport__c.WorkingTypeBaseCode__c,
			ComEmployeeImport__c.AgreementAlertSettingCode__c,
			ComEmployeeImport__c.TimeSettingBaseCode__c,
			ComEmployeeImport__c.HistoryComment__c,
			ComEmployeeImport__c.ApproverBase01Code__c,
			ComEmployeeImport__c.ApproverBase02Code__c,
			ComEmployeeImport__c.ApproverBase03Code__c,
			ComEmployeeImport__c.ApproverBase04Code__c,
			ComEmployeeImport__c.ApproverBase05Code__c,
			ComEmployeeImport__c.ApproverBase06Code__c,
			ComEmployeeImport__c.ApproverBase07Code__c,
			ComEmployeeImport__c.ApproverBase08Code__c,
			ComEmployeeImport__c.ApproverBase09Code__c,
			ComEmployeeImport__c.ApproverBase10Code__c,
			ComEmployeeImport__c.UserData01__c,
			ComEmployeeImport__c.UserData02__c,
			ComEmployeeImport__c.UserData03__c,
			ComEmployeeImport__c.UserData04__c,
			ComEmployeeImport__c.UserData05__c,
			ComEmployeeImport__c.UserData06__c,
			ComEmployeeImport__c.UserData07__c,
			ComEmployeeImport__c.UserData08__c,
			ComEmployeeImport__c.UserData09__c,
			ComEmployeeImport__c.UserData10__c,
			ComEmployeeImport__c.Status__c,
			ComEmployeeImport__c.Error__c
		};
	}

	/** 検索フィルタ */
	 private class Filter {
		/** インポートバッチID */
		public Id importBatchId;
		/** インポートID */
		public List<Id> importIdList;
		/** ステータス */
		public List<String> status;
	 }

	/**
	 * 社員インポートエンティティを取得する
	 * @param importId インポートID
	 * @return 社員インポートエンティティ
	 */
	public EmployeeImportEntity getEntity(Id importId) {
		// 検索実行
		List<EmployeeImportEntity> entityList = getEntityList(new List<Id>{importId});

		EmployeeImportEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * 社員インポートエンティティのリストを取得する
	 * @param importIdList 社員インポートIDのリスト
	 * @return 条件に一致した社員インポートエンティティのリスト
	 */
	public List<EmployeeImportEntity> getEntityList(List<Id> importIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.importIdList = importIdList;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, null, forUpdate);
	}

	/**
	 * 指定したインポートバッチIDで処理対象の社員インポートエンティティのリストを取得する
	 * @param importBatchId インポートバッチID
	 * @param status 社員インポートデータのステータス
	 * @param max 取得上限件数
	 * @return 条件に一致した社員インポートエンティティのリスト
	 */
	public List<EmployeeImportEntity> getEntityListByImportBatchId(Id importBatchId, List<String> status, Integer max) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.importBatchId = importBatchId;
		filter.status = status;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, max, forUpdate);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(EmployeeImportEntity entity) {
		return saveEntityList(new List<EmployeeImportEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<EmployeeImportEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<EmployeeImportEntity> entityList, Boolean allOrNone) {
		List<ComEmployeeImport__c> objList = new List<ComEmployeeImport__c>();

		// エンティティからSObjectを作成する
		for (EmployeeImportEntity entity : entityList) {
			ComEmployeeImport__c obj = createObj(entity);
			objList.add(obj);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(objList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 社員インポートを検索する
	 * @param filter 検索条件
	 * @param max 取得上限件数
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した社員インポートエンティティのリスト
	 */
	 private List<EmployeeImportEntity> searchEntity(Filter filter, Integer max, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id importBatchId = filter.importBatchId;
		final List<Id> importIds = filter.importIdList;
		final List<String> status = filter.status;
		condList.add(getFieldName(ComEmployeeImport__c.ImportBatchId__c) + ' = :importBatchId');
		if ((importIds != null) && (!importIds.isEmpty())) {
			condList.add(getFieldName(ComEmployeeImport__c.Id) + ' IN :importIds');
		}
		if ((status != null) && (!status.isEmpty())) {
			condList.add(getFieldName(ComEmployeeImport__c.Status__c) + ' IN :status');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComEmployeeImport__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (max != null) {
			soql += ' LIMIT ' + max;
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('EmployeeImportRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<EmployeeImportEntity> entityList = new List<EmployeeImportEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ComEmployeeImport__c)sobj));
		}

		return entityList;
	 }

	/**
	 * ComEmployeeImport__cオブジェクトからEmployeeImportEntityを作成する
	 * @param obj 作成元のComEmployeeImport__cオブジェクト
	 * @return 作成したEmployeeImportEntity
	 */
	private EmployeeImportEntity createEntity(ComEmployeeImport__c obj) {
		EmployeeImportEntity entity = new EmployeeImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.userName = obj.UserName__c;
		entity.code = obj.Code__c;
		entity.companyCode = obj.CompanyCode__c;
		entity.displayName_L0 = obj.DisplayName_L0__c;
		entity.displayName_L1 = obj.DisplayName_L1__c;
		entity.displayName_L2 = obj.DisplayName_L2__c;
		entity.firstName_L0 = obj.FirstName_L0__c;
		entity.firstName_L1 = obj.FirstName_L1__c;
		entity.firstName_L2 = obj.FirstName_L2__c;
		entity.lastName_L0 = obj.LastName_L0__c;
		entity.lastName_L1 = obj.LastName_L1__c;
		entity.lastName_L2 = obj.LastName_L2__c;
		entity.middleName_L0 = obj.MiddleName_L0__c;
		entity.middleName_L1 = obj.MiddleName_L1__c;
		entity.middleName_L2 = obj.MiddleName_L2__c;
		entity.hiredDate = AppDate.valueOf(obj.HiredDate__c);
		entity.validFrom = AppDate.valueOf(obj.ValidFrom__c);
		entity.validTo = AppDate.valueOf(obj.ValidTo__c);
		entity.costCenterBaseCode = obj.CostCenterBaseCode__c;
		entity.departmentBaseCode = obj.DepartmentBaseCode__c;
		entity.title_L0 = obj.Title_L0__c;
		entity.title_L1 = obj.Title_L1__c;
		entity.title_L2 = obj.Title_L2__c;
		entity.additionalDepartmentBaseCode = obj.AdditionalDepartmentBaseCode__c;
		entity.additionalTitle_L0 = obj.AdditionalTitle_L0__c;
		entity.additionalTitle_L1 = obj.AdditionalTitle_L1__c;
		entity.additionalTitle_L2 = obj.AdditionalTitle_L2__c;
		entity.managerBaseCode = obj.ManagerBaseCode__c;
		entity.calendarCode = obj.CalendarCode__c;
		entity.workingTypeBaseCode = obj.WorkingTypeBaseCode__c;
		entity.agreementAlertSettingCode = obj.AgreementAlertSettingCode__c;
		entity.timeSettingBaseCode = obj.TimeSettingBaseCode__c;
		entity.historyComment = obj.HistoryComment__c;
		entity.approverBase01Code = obj.ApproverBase01Code__c;
		entity.approverBase02Code = obj.ApproverBase02Code__c;
		entity.approverBase03Code = obj.ApproverBase03Code__c;
		entity.approverBase04Code = obj.ApproverBase04Code__c;
		entity.approverBase05Code = obj.ApproverBase05Code__c;
		entity.approverBase06Code = obj.ApproverBase06Code__c;
		entity.approverBase07Code = obj.ApproverBase07Code__c;
		entity.approverBase08Code = obj.ApproverBase08Code__c;
		entity.approverBase09Code = obj.ApproverBase09Code__c;
		entity.approverBase10Code = obj.ApproverBase10Code__c;
		entity.userData01 = obj.UserData01__c;
		entity.userData02 = obj.UserData02__c;
		entity.userData03 = obj.UserData03__c;
		entity.userData04 = obj.UserData04__c;
		entity.userData05 = obj.UserData05__c;
		entity.userData06 = obj.UserData06__c;
		entity.userData07 = obj.UserData07__c;
		entity.userData08 = obj.UserData08__c;
		entity.userData09 = obj.UserData09__c;
		entity.userData10 = obj.UserData10__c;
		entity.status = ImportStatus.valueOf(obj.Status__c);
		entity.error = obj.Error__c;

		entity.resetChanged();
		return entity;
	}

	/**
	 * EmployeeImportEntityからComEmployeeImport__cオブジェクトを作成する
	 * @param entity 作成元のEmployeeImportEntity
	 * @return 作成したComEmployeeImport__cオブジェクト
	 */
	private ComEmployeeImport__c createObj(EmployeeImportEntity entity) {
		ComEmployeeImport__c retObj = new ComEmployeeImport__c();

		retObj.Id = entity.id;
		if (entity.isChanged(EmployeeImportEntity.Field.ImportBatchId)) {
			retObj.ImportBatchId__c = entity.importBatchId;
		}
		if (entity.isChanged(EmployeeImportEntity.Field.Status)) {
			retObj.Status__c = entity.status.value;
		}
		if (entity.isChanged(EmployeeImportEntity.Field.Error)) {
			retObj.Error__c = entity.error;
		}

		return retObj;
	}
}