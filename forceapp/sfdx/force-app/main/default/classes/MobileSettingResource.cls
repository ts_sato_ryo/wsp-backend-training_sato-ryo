/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description モバイル設定APIを実装するクラス
 */
public with sharing class MobileSettingResource {

	/**
	 * @description モバイル機能設定保存APIのリクエストクラス
	 */
	public class SaveRequest implements RemoteApi.RequestParam {

		/** 会社レコードID */
		public String companyId;
		/** 打刻時の位置情報を送信する */
		public Boolean requireLocationAtMobileStamp;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 会社レコードIDの検証
			if (String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('companyId');
				throw e;
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch (StringException e) {
				throw new App.ParameterException('companyId', this.companyId);
			}

			// 打刻時の位置情報を送信するの検証
			if (this.requireLocationAtMobileStamp == null) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('requireLocationAtMobileStamp');
				throw e;
			}
		}
	}

	/**
	 * モバイル機能設定保存API
	 */
	public with sharing class SaveApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requiredPermission =
				ComResourcePermission.Permission.MANAGE_MOBILE_SETTING;

		/**
		 * @description モバイル機能設定を保存する
		 * @param req リクエストパラメータ(SaveRequest)
		 * @return レスポンス（null）
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 入力値チェック
			MobileSettingResource.SaveRequest param = (SaveRequest)req.getParam(MobileSettingResource.SaveRequest.class);
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requiredPermission);

			// 保存する
			CompanyEntity company = createCompanyEntity(param);
			new CompanyRepository().saveEntity(company);

			// レスポンス作成
			return null;
		}

		/**
		 * リクエストパラメータから保存用の会社エンティティを作成する
		 * @param param 保存APIのリクエストパラメータ
		 * @return 会社エンティティ
		 */
		private CompanyEntity createCompanyEntity(SaveRequest param) {
			CompanyEntity company = new CompanyEntity();
			company.setId(param.companyId);
			company.requireLocationAtMobileStamp = param.requireLocationAtMobileStamp;
			return company;
		}
	}

	/**
	 * @description モバイル機能設定取得APIのリクエストクラス
	 */
	public class GetRequest implements RemoteApi.RequestParam {

		/** 会社レコードID */
		public String companyId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 会社レコードIDの検証
			if (String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('companyId');
				throw e;
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch(StringException e) {
				throw new App.ParameterException('companyId', companyId);
			}
		}
	}

	/**
	 * @description モバイル機能設定取得APIのレスポンスクラス
	 */
	public class GetResponse implements RemoteApi.ResponseParam {

		/** 打刻時の位置情報を送信する */
		public Boolean requireLocationAtMobileStamp;
	}

	/**
	 * モバイル機能設定取得API
	 */
	public with sharing class GetApi extends RemoteApi.ResourceBase {

		/**
		 * @description モバイル機能設定を取得する
		 * @param req リクエストパラメータ(GetRequest)
		 * @return レスポンスパラメータ(GetResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 入力値チェック
			GetRequest param = (GetRequest)req.getParam(GetRequest.class);
			param.validate();

			// 会社を取得する
			CompanyEntity company = new CompanyRepository().getEntity(param.companyId);
			if (company == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}

			// レスポンス作成
			GetResponse response = new GetResponse();
			response.requireLocationAtMobileStamp = company.requireLocationAtMobileStamp;

			return response;
		}
	}
}