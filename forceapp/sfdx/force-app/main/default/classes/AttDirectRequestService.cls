/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * 直行直帰申請のサービスクラス
  * 当クラスはAttAttDailyRequestServiceからのみ実行する事を前提としています。
  * Resourceクラスなどから直接実行しないで下さい。
  */
public with sharing class AttDirectRequestService extends AttDailyRequestService.AbstractRequestProcessor {

	/** 申請可能な最大日数 */
	private static final Integer REQUEST_PERIOD_MAX = 31;

	private final AttDailyRequestRepository dailyRequestRepository = new AttDailyRequestRepository();

	private final AttAttendanceService attService = new AttAttendanceService();

	/**
	 * 申請・再申請パラメータ
	 */
	public class SubmitParam extends AttDailyRequestService.SubmitParam {
		/** 開始日 */
		public AppDate startDate;
		/** 終了日 */
		public AppDate endDate;
		/** 出勤時刻 */
		public AttTime startTime;
		/** 退勤時刻 */
		public AttTime endTime;
		/** 休憩1開始時刻 */
		public AttTime rest1StartTime;
		/** 休憩1終了時刻 */
		public AttTime rest1EndTime;
		/** 休憩2開始時刻 */
		public AttTime rest2StartTime;
		/** 休憩2終了時刻 */
		public AttTime rest2EndTime;
		/** 休憩3開始時刻 */
		public AttTime rest3StartTime;
		/** 休憩3終了時刻 */
		public AttTime rest3EndTime;
		/** 休憩4開始時刻 */
		public AttTime rest4StartTime;
		/** 休憩4終了時刻 */
		public AttTime rest4EndTime;
		/** 休憩5開始時刻 */
		public AttTime rest5StartTime;
		/** 休憩5終了時刻 */
		public AttTime rest5EndTime;
		/** 備考 */
		public String remarks;
	}

	/**
	 * 直行直帰申請の申請/再申請を行う。
	 *
	 * @param param 申請情報
	 * @return 作成した申請情報
	 */
	public override AttDailyRequestEntity submit(AttDailyRequestService.SubmitParam submitParam) {
		SubmitParam param = (SubmitParam)submitParam;

		// 申請期間のチェック
		// 申請期間が長すぎるとSOQLガバナ例外やexcludingDateの文字数が長すぎてDML例外が発生してしまうため
		if (param.startDate != null && param.endDate != null) {
			if (param.startDate.daysBetween(param.endDate) >= REQUEST_PERIOD_MAX) {
				throw new App.ParameterException(
						ComMessage.msg().Att_Err_RequesePeriodTooLong(new List<String>{REQUEST_PERIOD_MAX.format()}));
			}
		}

		// 再申請の場合は削除する
		if (param.requestId != null) {
			super.remove(dailyRequestRepository.getEntity(param.requestId));
		}

		// 申請対象となる勤怠明細レコードとサマリーを取得する
		AttDailyRequestService.RecordsWithSummary recordsWithSummary = getRecordEntityList(param.empId, param.startDate, param.endDate);

		// 直行直帰申請を使用可能か判定
		if (!isValidDirectRequest(recordsWithSummary, param.startDate, param.endDate)) {
			throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_DIRECT_REQUEST, ComMessage.msg().Att_Err_NotPermittedDirectRequest);
		}

		// 勤務日以外の日タイプを含む場合はエラー
		checkWorkDay(recordsWithSummary.records);

		// 申請エンティティを作成する
		AttDailyRequestEntity request = createDirectRequest(param, recordsWithSummary);
		// 申請エンティティのバリデーション
		AttValidator.DailyRequestValidator requestValidator = new AttValidator.DailyRequestValidator(request);
		Validator.Result requestValidatorResult = requestValidator.validate();
		if (!requestValidatorResult.isSuccess()) {
			Validator.Error error = requestValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 開始日の前日と終了日の翌日の勤怠明細を取得する
		AppDate dayBefore = param.startDate.addDays(-1);
		AppDate dayAfter = param.endDate.addDays(+1);
		AttSummaryRepository repo = new AttSummaryRepository();
		List<AttRecordEntity> records = repo.searchRecordEntityList(param.empId, dayBefore, dayAfter, false);
		AttRecordEntity recordBefore, recordAfter;
		for (AttRecordEntity record : records) {
			if (record.rcdDate == dayBefore) {
				recordBefore = record;
			} else if (record.rcdDate == dayAfter) {
				recordAfter = record;
			}
		}
		// 出退勤時刻、休憩時刻のバリデーション
		AttValidator.AttTimeValidator attTimeValidator = new AttValidator.AttTimeValidator(request, recordBefore, recordAfter);
		Validator.Result attTimeValidatorResult = attTimeValidator.validate();
		if (!attTimeValidatorResult.isSuccess()) {
			Validator.Error error = attTimeValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 1~5の休暇を時刻順でソートする
		List<AttTimePeriod> restPeriodList = AttTimePeriod.sortByStartTime(
				param.rest1StartTime, param.rest1EndTime,
				param.rest2StartTime, param.rest2EndTime,
				param.rest3StartTime, param.rest3EndTime,
				param.rest4StartTime, param.rest4EndTime,
				param.rest5StartTime, param.rest5EndTime
			);

		// 申請エンティティにソートした時刻を設定する
		if (restPeriodList.size() > 0) {
			request.rest1StartTime = restPeriodList[0].startTime;
			request.rest1EndTime = restPeriodList[0].endTime;
		}
		if (restPeriodList.size() > 1) {
			request.rest2StartTime = restPeriodList[1].startTime;
			request.rest2EndTime = restPeriodList[1].endTime;
		}
		if (restPeriodList.size() > 2) {
			request.rest3StartTime = restPeriodList[2].startTime;
			request.rest3EndTime = restPeriodList[2].endTime;
		}
		if (restPeriodList.size() > 3) {
			request.rest4StartTime = restPeriodList[3].startTime;
			request.rest4EndTime = restPeriodList[3].endTime;
		}
		if (restPeriodList.size() > 4) {
			request.rest5StartTime = restPeriodList[4].startTime;
			request.rest5EndTime = restPeriodList[4].endTime;
		}

		// 申請を保存する
		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();
		Repository.SaveResult saveRes = requestRepo.saveEntity(request);
		request.setId(saveRes.details[0].id);

		// 勤怠明細の直行直帰申請IDを更新する
		for (AttRecordEntity record : recordsWithSummary.records) {
			// 勤怠明細のバリデーション
			AttValidator.DirectSubmitValidator validator = new AttValidator.DirectSubmitValidator(record, request);
			Validator.Result validatorResult = validator.validate();
			if (!validatorResult.isSuccess()) {
				Validator.Error error = validatorResult.getErrors()[0];
				throw new App.IllegalStateException(error.code, error.message);
			}
			record.inpRequestingDirectRequestId = request.id;
		}

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		summaryRepo.saveRecordEntityList(recordsWithSummary.records);

		// 申請する(申請コメントなし)
		submitProcess(request, null);
		return request;
	}

	/**
	 * 直行直帰申請のエンティティを作成する
	 * @param param 申請情報
	 * @param recordsWithSummary 申請対象の勤怠明細
	 * @return エンティティ
	 */
	private AttDailyRequestEntity createDirectRequest(SubmitParam param, AttDailyRequestService.RecordsWithSummary recordsWithSummary) {
		// 社員を取得
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity employee = empService.getEmployee(param.empId, param.startDate);

		// エンティティを生成
		AttDailyRequestEntity entity = new AttDailyRequestEntity();
		entity.setId(param.requestId);
		entity.name = createRequestName(param, employee, getCompanyLanguage(employee.companyId));
		entity.requestType = AttRequestType.DIRECT;
		entity.startDate = param.startDate;
		entity.endDate = param.endDate;
		entity.startTime = param.startTime;
		entity.endTime = param.endTime;
		entity.rest1StartTime = param.rest1StartTime;
		entity.rest1EndTime = param.rest1EndTime;
		entity.rest2StartTime = param.rest2StartTime;
		entity.rest2EndTime = param.rest2EndTime;
		entity.rest3StartTime = param.rest3StartTime;
		entity.rest3EndTime = param.rest3EndTime;
		entity.rest4StartTime = param.rest4StartTime;
		entity.rest4EndTime = param.rest4EndTime;
		entity.rest5StartTime = param.rest5StartTime;
		entity.rest5EndTime = param.rest5EndTime;
		entity.remarks = param.remarks;
		entity.status = AppRequestStatus.PENDING;
		entity.cancelType = AppCancelType.NONE;
		entity.isConfirmationRequired = false;
		entity.requestTime = AppDatetime.now();
		setRequestCommInfo(entity, employee);
		return entity;
	}

	/**
	 * 会社の言語に合わせた直行直帰申請名称を作成する。
	 * 日本語の場合：{氏名} さん 直行直帰申請 {開始日}–{終了日}
	 * 日本語以外の場合：{氏名} Direct Request {開始日}–{終了日}
	 * @param param 申請情報
	 * @param employee 社員情報
	 * @return 直行直帰申請名
	 */
	@TestVisible
	private String createRequestName(SubmitParam param, EmployeeBaseEntity employee, AppLanguage lang) {
		// 日付フォーマット
		final AppDate.FormatType dateFormat = AppDate.FormatType.YYYYMMDD;

		// バインド文字列
		List<String> args = new List<String>{
			employee.displayNameL.getValue(lang),
			param.startDate.format(dateFormat),
			param.endDate.format(dateFormat)};

		// 申請種類名
		ComMsgBase targetLang = ComMessage.msg(lang.value);
		return lang == AppLanguage.JA ?
			String.format('{0}さん ' + targetLang.Att_Lbl_RequestTypeDirect + targetLang.Att_Lbl_Request + ' {1}-{2}', args) :
			String.format('{0} ' + targetLang.Att_Lbl_RequestTypeDirect + ' ' + targetLang.Att_Lbl_Request + ' {1}-{2}', args);
	}

	/**
	 * 勤務日以外の日タイプが含まれていないか判定する
	 * @param 判定対象の勤怠明細リスト
	 * @return 勤務日以外の場合はエラー
	 */
	@TestVisible
	private void checkWorkDay(List<AttRecordEntity> records) {
		for (AttRecordEntity record : records) {
			// 日タイプが勤務日(Workday)と休日出勤以外の場合はエラー
			if (record.outDayType != AttDayType.WORKDAY && record.reqHolidayWorkRequestId == null) {
				throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_DIRECT_REQUEST,
						ComMessage.msg().Att_Err_TheDayIsNotWorkDay(new List<String>{record.rcdDate.format(AppDate.FormatType.YYYY_MM_DD_SLASH)}));
			}
		}
	}

	/**
	 * 指定の期間で直行直帰申請を使用可能か、勤務体系マスタから判定する。
	 * @param summaries 勤怠サマリ
	 * @return 判定結果 期間全てにおいて、直行直帰申請が可能な場合true
	 */
	private Boolean isValidDirectRequest(AttDailyRequestService.RecordsWithSummary summaries, AppDate startDate, AppDate endDate) {
		// 勤務体系を取得
		AttWorkingTypeRepository.SearchFilter filter = new AttWorkingTypeRepository.SearchFilter();
		filter.baseIds = new Set<Id>();
		for (AttSummaryEntity summary : summaries.summaryMap.values()) {
			filter.baseIds.add(summary.workingTypeBaseId);
		}
		List<AttWorkingTypeBaseEntity> workingTypes = new AttWorkingTypeRepository().searchEntity(filter);

		// 申請日に紐づく勤務体系履歴を判定
		for (AttRecordEntity attRecord : summaries.records) {
			AttWorkingTypeHistoryEntity history = getWorkingTypeHistory(workingTypes, attRecord.rcdDate);
			if (history == null || !history.useDirectApply) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 指定した日付の勤務体系履歴を取得する。
	 * @param workingTypes 勤務体系ベース
	 * @param targetDate 対象日
	 * @param 指定日に対応する勤務体系履歴（存在しない場合はnull）
	 */
	private AttWorkingTypeHistoryEntity getWorkingTypeHistory(List<AttWorkingTypeBaseEntity> workingTypes, AppDate targetDate) {
		for (AttWorkingTypeBaseEntity base : workingTypes) {
			AttWorkingTypeHistoryEntity history = base.getHistoryByDate(targetDate);
			if (history != null) {
				return history;
			}
		}
		return null;
	}

	/**
	 * @description 承認取消時のメールを作成する
	 * @param request 申請
	 * @param applicant 申請者
	 * @param empLastModified 最終更新者
	 * @param targetLanguage 送信先ユーザの言語
	 * @return mailMessage メール内容
	 */
	public override MailService.MailParam createCancelMailMessage(AttDailyRequestEntity request, EmployeeBaseEntity applicant, EmployeeBaseEntity empLastModified, AppLanguage targetLanguage) {

		MailService.MailParam mailMessage = new MailService.MailParam();
		String language = targetLanguage.value;

		// 送信者名
		String senderDisplayName = empLastModified.fullNameL.getFullName(targetLanguage);
		mailMessage.senderDisplayName = senderDisplayName;

		// メールタイトル
		String mailSubject;
		// 申請種別を文字列に変換する
		String requestType = ComMessage.msg(language).Att_Lbl_RequestTypeDirect;
		mailSubject = ComMessage.msg(language).Att_Msg_CancelMailSubject(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType});
		mailMessage.subject = mailSubject;

		// メール本文
		String mailBody;
		// 申請日付
		String requestDate = request.requestTime.formatLocal().substring(0, 10).replace('-', '/');
		// 開始日
		String startDate = String.valueOf(AppDate.convertDate(request.startDate)).replace('-', '/');
		// 終了日
		String endDate = String.valueOf(AppDate.convertDate(request.endDate)).replace('-', '/');
		// 出勤時刻
		String startTime = request.startTime.format();
		// 退勤時刻
		String endTime = request.endTime.format();
		// 休憩1開始時刻
		String rest1StartTime = request.rest1StartTime != null ? request.rest1StartTime.format() : null;
		// 休憩1終了時刻
		String rest1EndTime = request.rest1EndTime != null ? request.rest1EndTime.format() : null;
		String rest1TimeMessage = createRestTimeMessage(rest1StartTime, rest1EndTime, 1, language);
		// 休憩2開始時刻
		String rest2StartTime = request.rest2StartTime != null ? request.rest2StartTime.format() : null;
		// 休憩2終了時刻
		String rest2EndTime = request.rest2EndTime != null ? request.rest2EndTime.format() : null;
		String rest2TimeMessage = createRestTimeMessage(rest2StartTime, rest2EndTime, 2, language);
		// 休憩3開始時刻
		String rest3StartTime = request.rest3StartTime != null ? request.rest3StartTime.format() : null;
		// 休憩3終了時刻
		String rest3EndTime = request.rest3EndTime != null ? request.rest3EndTime.format() : null;
		String rest3TimeMessage = createRestTimeMessage(rest3StartTime, rest3EndTime, 3, language);
		// 休憩4開始時刻
		String rest4StartTime = request.rest4StartTime != null ? request.rest4StartTime.format() : null;
		// 休憩4終了時刻
		String rest4EndTime = request.rest4EndTime != null ? request.rest4EndTime.format() : null;
		String rest4TimeMessage = createRestTimeMessage(rest4StartTime, rest4EndTime, 4, language);
		// 休憩5開始時刻
		String rest5StartTime = request.rest5StartTime != null ? request.rest5StartTime.format() : null;
		// 休憩5終了時刻
		String rest5EndTime = request.rest5EndTime != null ? request.rest5EndTime.format() : null;
		String rest5TimeMessage = createRestTimeMessage(rest5StartTime, rest5EndTime, 5, language);
		// 備考
		String remarks = request.remarks;
		// メール本文を作成
		mailBody = ComMessage.msg(language).Att_Msg_CancelMailMessage(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType, empLastModified.fullNameL.getFullName(targetLanguage)});
		mailBody += '\r\n\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_RequestContents + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestDate + '：' + requestDate + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestType + '：' + requestType + '\r\n';
		// 期間での申請の場合は、開始日と終了日を追加
		if (startDate.equals(endDate)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate + '\r\n';
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate + '－' + endDate + '\r\n';
		}
		mailBody += ComMessage.msg(language).Att_Lbl_WorkTime + '：' + startTime + '－' + endTime + '\r\n';
		if (String.isNotBlank(rest1TimeMessage)) {
			mailBody += rest1TimeMessage;
		}
		if (String.isNotBlank(rest2TimeMessage)) {
			mailBody += rest2TimeMessage;
		}
		if (String.isNotBlank(rest3TimeMessage)) {
			mailBody += rest3TimeMessage;
		}
		if (String.isNotBlank(rest4TimeMessage)) {
			mailBody += rest4TimeMessage;
		}
		if (String.isNotBlank(rest5TimeMessage)) {
			mailBody += rest5TimeMessage;
		}
		if (String.isNotBlank(remarks)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + remarks + '\r\n';
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + '\r\n';
		}
		// 署名
		mailBody += '\r\n---\r\n';
		mailBody += ComMessage.msg(language).Att_Msg_TeamSpirit;
		mailMessage.body = mailBody;

		return mailMessage;
	}

	/**
	 * 休憩時間の開始終了時刻の有無によって表現を変更するメソッド
	 */
	private String createRestTimeMessage(String restStartTime, String restEndTime, Integer index, String language) {

		String message;
		if (String.isNotBlank(restStartTime) && String.isNotBlank(restEndTime)) {
			message = ComMessage.msg(language).Att_Lbl_Rest + index + '：' + restStartTime + '－' + restEndTime + '\r\n';
		} else if (String.isNotBlank(restStartTime)) {
			message = ComMessage.msg(language).Att_Lbl_Rest + index + '：' + restStartTime + '－' + '\r\n';
		} else if (String.isNotBlank(restEndTime)) {
			message = ComMessage.msg(language).Att_Lbl_Rest + index + '：         －' + restEndTime + '\r\n';
		}
		return message;
	}
}