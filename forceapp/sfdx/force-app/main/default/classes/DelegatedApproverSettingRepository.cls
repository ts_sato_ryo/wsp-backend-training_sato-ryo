/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通
 *
 * 代理承認者設定のリポジトリ
 */
public with sharing class DelegatedApproverSettingRepository extends Repository.BaseRepository {

	/** リストを取得する際の並び順 */
	public enum SortOrder {
		CODE_ASC, CODE_DESC
	}
	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;
	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** 社員のリレーション名 */
	private static final String EMP_BASE_R = ComDelegatedApproverSetting__c.EmployeeBaseId__c.getDescribe().getRelationshipName();
	private static final String APPROVER_BASE_R = ComDelegatedApproverSetting__c.DelegatedApproverBaseId__c.getDescribe().getRelationshipName();
	private static final String EMP_HISTORY_R = ComEmpBase__c.CurrentHistoryId__c.getDescribe().getRelationshipName();
	private static final String USER_R = ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();
	/** 部署のリレーション名 */
	private static final String DEPT_BASE_R = ComEmpHistory__c.DepartmentBaseId__c.getDescribe().getRelationshipName();
	private static final String DEPT_HISTORY_R = ComDeptBase__c.CurrentHistoryId__c.getDescribe().getRelationshipName();

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ComDelegatedApproverSetting__c.Id,
			ComDelegatedApproverSetting__c.Name,
			ComDelegatedApproverSetting__c.EmployeeBaseId__c,
			ComDelegatedApproverSetting__c.DelegatedApproverBaseId__c,
			ComDelegatedApproverSetting__c.ApproveExpenseRequestByDelegate__c,
			ComDelegatedApproverSetting__c.ApproveExpenseReportByDelegate__c
		};
	}

	/** 検索フィルタ */
	 private class SearchFilter {
		/**  代理承認者設定ID */
		public List<Id> ids;
		/** 社員ベースID */
		public Set<Id> employeeBaseIds;
		/** 代理承認者ベースID */
		public List<Id> delegatedApproverBaseIds;
		/** 事前申請承認却下(代理) */
		public Boolean canApproveExpenseRequestByDelegate;
		/** 経費精算申請承認却下(代理) */
		public Boolean canApproveExpenseReportByDelegate;
	 }

	/**
	 * 代理承認者設定を取得する
	 * @param settingIdList 代理承認者設定IDのリスト
	 * @return 条件に一致した代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getEntityList(List<Id> settingIdList) {
		// 検索条件を作成する
		SearchFilter filter = new SearchFilter();
		filter.ids = settingIdList;

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 代理承認者設定を取得する
	 * @param employeeBaseId 社員ベースID
	 * @return 条件に一致した代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getEntityListByEmpId(Id employeeBaseId) {
		// 検索条件を作成する
		SearchFilter filter = new SearchFilter();
		filter.employeeBaseIds = new Set<Id>{employeeBaseId};

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * Get delegated approver
	 * @param employeeBaseIds Set of employee base id
	 * @return Delegated Approver Setting list
	 */
	public List<DelegatedApproverSettingEntity> getEntityListByEmpIds(Set<Id> employeeBaseIds) {
		// Create filter
		SearchFilter filter = new SearchFilter();
		filter.employeeBaseIds = employeeBaseIds;

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 代理承認者設定を取得する(経費申請代理承認者のみ)
	 * @param employeeBaseId 社員ベースID
	 * @return 条件に一致した代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getExpReportApproverList(Id employeeBaseId) {
		// 検索条件を作成する
		SearchFilter filter = new SearchFilter();
		filter.employeeBaseIds = new Set<Id>{employeeBaseId};
		filter.canApproveExpenseReportByDelegate = true;

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 代理承認者設定を取得する(経費申請代理元社員のみ)
	 * @param delegatedApproverBaseId 代理承認者ベースID
	 * @return 条件に一致した代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getAllOriginalApproverList(Id delegatedApproverBaseId) {
		// 検索条件を作成する
		SearchFilter filter = new SearchFilter();
		filter.delegatedApproverBaseIds = new List<Id>{delegatedApproverBaseId};

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 代理承認者設定を取得する(経費申請代理元社員のみ)
	 * @param delegatedApproverBaseId 代理承認者ベースID
	 * @return 条件に一致した代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getExpReportOriginalApproverList(Id delegatedApproverBaseId) {
		// 検索条件を作成する
		SearchFilter filter = new SearchFilter();
		filter.delegatedApproverBaseIds = new List<Id>{delegatedApproverBaseId};
		filter.canApproveExpenseReportByDelegate = true;

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}
	
	/**
	 * Retrieve DelegatedApproverSettingEntity based on submitter employee information (ExpRequest delegated approver only)
	 * @param employeeBaseId ID of employee
	 * @return List of DelegatedApproverSettingEntity that matches the criteria
	 */
	public List<DelegatedApproverSettingEntity> getExpRequestApproverList(Id employeeBaseId) {
		// create search filter
		SearchFilter filter = new SearchFilter();
		filter.employeeBaseIds = new Set<Id>{employeeBaseId};
		filter.canApproveExpenseRequestByDelegate = true;
		
		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}
	
	/**
	 * Retrieve DelegatedApproverSettingEntity based on delegated approver information (ExpRequest original approver only)
	 * @param delegatedApproverBaseId ID of delegated approver
	 * @return List of DelegatedApproverSettingEntity that matches the criteria
	 */
	public List<DelegatedApproverSettingEntity> getExpRequestOriginalApproverList(Id delegatedApproverBaseId) {
		// create search filter
		SearchFilter filter = new SearchFilter();
		filter.delegatedApproverBaseIds = new List<Id>{delegatedApproverBaseId};
		filter.canApproveExpenseRequestByDelegate = true;

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(DelegatedApproverSettingEntity entity) {
		return saveEntityList(new List<DelegatedApproverSettingEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<DelegatedApproverSettingEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<DelegatedApproverSettingEntity> entityList, Boolean allOrNone) {
		List<ComDelegatedApproverSetting__c> delegatedApproverSettingList = new List<ComDelegatedApproverSetting__c>();

		// エンティティからSObjectを作成する
		for (DelegatedApproverSettingEntity entity : entityList) {
			ComDelegatedApproverSetting__c delegatedApproverSetting = createObj(entity);
			if (delegatedApproverSetting.Id == null) {
				delegatedApproverSetting.EmployeeBaseId__c = entity.employeeBaseId;
			}
			delegatedApproverSettingList.add(delegatedApproverSetting);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(delegatedApproverSettingList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 代理承認者設定を検索する
	 * @param filter 検索条件
	 * @param order 並び順
	 * @return 検索結果
	 */
	public List<DelegatedApproverSettingEntity> searchEntity(SearchFilter filter, SortOrder order) {
		return searchEntity(filter, NOT_FOR_UPDATE, order);
	}

	/**
	 * 代理承認者設定を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param order 並び順
	 * @return 条件に一致した代理承認者設定のリスト（該当するレコードが存在しない場合は空のリスト）
	 */
	 public List<DelegatedApproverSettingEntity> searchEntity(SearchFilter filter, Boolean forUpdate, SortOrder order) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}
		// 関連項目（社員）
		selectFldList.addAll(getSelectFldListEmployee(EMP_BASE_R));
		// 関連項目（代理承認者）
		selectFldList.addAll(getSelectFldListEmployee(APPROVER_BASE_R));

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> ids = filter.ids;
		final Set<Id> employeeBaseIds = filter.employeeBaseIds;
		final List<Id> delegatedApproverBaseIds = filter.delegatedApproverBaseIds;
		final Boolean canApproveExpenseRequestByDelegate = filter.canApproveExpenseRequestByDelegate;
		final Boolean canApproveExpenseReportByDelegate = filter.canApproveExpenseReportByDelegate;
		if (ids != null && !ids.isEmpty()) {
			condList.add(getFieldName(ComDelegatedApproverSetting__c.Id) + ' IN :ids');
		}
		if (employeeBaseIds != null && !employeeBaseIds.isEmpty()) {
			condList.add(getFieldName(ComDelegatedApproverSetting__c.EmployeeBaseId__c) + ' IN :employeeBaseIds');
		}
		if (delegatedApproverBaseIds != null && !delegatedApproverBaseIds.isEmpty()) {
			condList.add(getFieldName(ComDelegatedApproverSetting__c.DelegatedApproverBaseId__c) + ' IN :delegatedApproverBaseIds');
		}
		if (canApproveExpenseRequestByDelegate != null) {
			condList.add(getFieldName(ComDelegatedApproverSetting__c.ApproveExpenseRequestByDelegate__c) + ' = :canApproveExpenseRequestByDelegate');
		}
		if (canApproveExpenseReportByDelegate != null) {
			condList.add(getFieldName(ComDelegatedApproverSetting__c.ApproveExpenseReportByDelegate__c) + ' = :canApproveExpenseReportByDelegate');
		}
		String whereString = buildWhereString(condList);

		// ORDER BY句
		String orderByString = ' ORDER BY ';
		if (order == SortOrder.CODE_ASC) {
			orderByString += getFieldName(EMP_BASE_R, ComEmpBase__c.Code__c) + ' ASC NULLS LAST, ' + getFieldName(APPROVER_BASE_R, ComEmpBase__c.Code__c) + ' ASC NULLS LAST';
		} else if (order == SortOrder.CODE_DESC) {
			orderByString += getFieldName(EMP_BASE_R, ComEmpBase__c.Code__c) + ' DESC NULLS LAST, ' + getFieldName(APPROVER_BASE_R, ComEmpBase__c.Code__c) + ' DESC NULLS LAST';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComDelegatedApproverSetting__c.SObjectType.getDescribe().getName() +
				+ whereString
				+ orderByString
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('DelegatedApproverSettingRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<DelegatedApproverSettingEntity> entityList = new List<DelegatedApproverSettingEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ComDelegatedApproverSetting__c)sobj));
		}

		return entityList;
	 }

	/**
	 * 社員オブジェクトのSELECT句の取得項目を作成する
	 * @param relationshipName リレーション名
	 * @return 社員オブジェクトから取得する項目一覧
	 */
	private List<String> getSelectFldListEmployee(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.Code__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.FirstName_L0__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.FirstName_L1__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.FirstName_L2__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.LastName_L0__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.LastName_L1__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.LastName_L2__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpBase__c.UserId__c));
		selectFldList.add(getFieldName(relationshipName, USER_R, User.SmallPhotoUrl));
		selectFldList.add(getFieldName(relationshipName, USER_R, User.IsActive));
		selectFldList.add(getFieldName(relationshipName, EMP_HISTORY_R, DEPT_BASE_R, ComDeptBase__c.Code__c));
		selectFldList.add(getFieldName(relationshipName, EMP_HISTORY_R, DEPT_BASE_R, DEPT_HISTORY_R, ComDeptHistory__c.Name_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMP_HISTORY_R, DEPT_BASE_R, DEPT_HISTORY_R, ComDeptHistory__c.Name_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMP_HISTORY_R, DEPT_BASE_R, DEPT_HISTORY_R, ComDeptHistory__c.Name_L2__c));
		return selectFldList;
	}

	/**
	 * ComDelegatedApproverSetting__cオブジェクトからDelegatedApproverSettingEntityを作成する
	 * @param obj 作成元のComDelegatedApproverSetting__cオブジェクト
	 * @return 作成したDelegatedApproverSettingEntity
	 */
	private DelegatedApproverSettingEntity createEntity(ComDelegatedApproverSetting__c obj) {
		DelegatedApproverSettingEntity entity = new DelegatedApproverSettingEntity();
		entity.setId(obj.Id);
		entity.name = obj.Name;
		entity.employeeBaseId = obj.EmployeeBaseId__c;
		entity.employee = new AppLookup.Employee(
				obj.EmployeeBaseId__r.Code__c,
				new AppPersonName(
					obj.EmployeeBaseId__r.FirstName_L0__c,
					obj.EmployeeBaseId__r.LastName_L0__c,
					obj.EmployeeBaseId__r.FirstName_L1__c,
					obj.EmployeeBaseId__r.LastName_L1__c,
					obj.EmployeeBaseId__r.FirstName_L2__c,
					obj.EmployeeBaseId__r.LastName_L2__c),
				obj.EmployeeBaseId__r.UserId__c,
				obj.EmployeeBaseId__r.UserId__r.SmallPhotoUrl,
				null,
				null,
				null,
				obj.EmployeeBaseId__r.UserId__r.IsActive);
		entity.department = new AppLookup.Department(
				obj.EmployeeBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.Code__c,
				new AppMultiString(
					obj.EmployeeBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.CurrentHistoryId__r.Name_L0__c,
					obj.EmployeeBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.CurrentHistoryId__r.Name_L1__c,
					obj.EmployeeBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.CurrentHistoryId__r.Name_L2__c),
				null,
				null,
				null);
		entity.delegatedApproverBaseId = obj.DelegatedApproverBaseId__c;
		entity.delegatedApprover = new AppLookup.Employee(
				obj.DelegatedApproverBaseId__r.Code__c,
				new AppPersonName(
					obj.DelegatedApproverBaseId__r.FirstName_L0__c,
					obj.DelegatedApproverBaseId__r.LastName_L0__c,
					obj.DelegatedApproverBaseId__r.FirstName_L1__c,
					obj.DelegatedApproverBaseId__r.LastName_L1__c,
					obj.DelegatedApproverBaseId__r.FirstName_L2__c,
					obj.DelegatedApproverBaseId__r.LastName_L2__c),
				obj.DelegatedApproverBaseId__r.UserId__c,
				obj.DelegatedApproverBaseId__r.UserId__r.SmallPhotoUrl,
				null,
				null,
				null,
				obj.DelegatedApproverBaseId__r.UserId__r.IsActive);
		entity.delegatedApproverDepartment = new AppLookup.Department(
				obj.DelegatedApproverBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.Code__c,
				new AppMultiString(
					obj.DelegatedApproverBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.CurrentHistoryId__r.Name_L0__c,
					obj.DelegatedApproverBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.CurrentHistoryId__r.Name_L1__c,
					obj.DelegatedApproverBaseId__r.CurrentHistoryId__r.DepartmentBaseId__r.CurrentHistoryId__r.Name_L2__c),
				null,
				null,
				null);
		entity.canApproveExpenseRequestByDelegate = obj.ApproveExpenseRequestByDelegate__c;
		entity.canApproveExpenseReportByDelegate = obj.ApproveExpenseReportByDelegate__c;
		return entity;
	}

	/**
	 * DelegatedApproverSettingEntityからComDelegatedApproverSetting__cオブジェクトを作成する。
	 * entityの変更のあった項目のみ、作成後のオブジェクトに設定する。
	 * @param entity 作成元のエンティティ
	 * @return 作成後のオブジェクト
	 */
	private ComDelegatedApproverSetting__c createObj(DelegatedApproverSettingEntity entity) {
		ComDelegatedApproverSetting__c retObj = new ComDelegatedApproverSetting__c();

		retObj.Id = entity.id;
		if (entity.isChanged(DelegatedApproverSettingEntity.Field.NAME)) {
			retObj.Name = entity.name;
		}
		if (entity.isChanged(DelegatedApproverSettingEntity.Field.DELEGATED_APPROVER_BASE_ID)) {
			retObj.DelegatedApproverBaseId__c = entity.delegatedApproverBaseId;
		}
		if (entity.isChanged(DelegatedApproverSettingEntity.Field.CAN_APPROVE_EXPENSE_REQUEST_BY_DELEGATE)) {
			retObj.ApproveExpenseRequestByDelegate__c = entity.canApproveExpenseRequestByDelegate;
		}
		if (entity.isChanged(DelegatedApproverSettingEntity.Field.CAN_APPROVE_EXPENSE_REPORT_BY_DELEGATE)) {
			retObj.ApproveExpenseReportByDelegate__c = entity.canApproveExpenseReportByDelegate;
		}
		return retObj;
	}
}