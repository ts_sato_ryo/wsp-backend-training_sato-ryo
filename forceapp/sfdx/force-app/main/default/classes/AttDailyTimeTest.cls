@isTest
private class AttDailyTimeTest {

	/**
	 * formatのテスト
	 */
	@isTest static void formatTest() {
		AttDailyTime dt;

		// 0埋めなし
		dt = new AttDailyTime(12 * 60 + 23);
		System.assertEquals('12:23', dt.format());

		// 0埋めあり
		dt = new AttDailyTime(3 * 60 + 2);
		System.assertEquals('03:02', dt.format());

		// Hourが3桁(日次なので発生しない？)
		dt = new AttDailyTime(100 * 60 + 2);
		System.assertEquals('100:02', dt.format());

	}

	/**
	 * formatHmmのテスト
	 */
	@isTest static void formatHmmTest() {
		AttDailyTime dt;

		// 0埋めなし
		dt = new AttDailyTime(12 * 60 + 23);
		System.assertEquals('12:23', dt.formatHmm());

		// minuteの0埋めあり
		dt = new AttDailyTime(3 * 60 + 2);
		System.assertEquals('3:02', dt.formatHmm());

		// Hourが3桁(日次なので発生しない？)
		dt = new AttDailyTime(100 * 60 + 2);
		System.assertEquals('100:02', dt.formatHmm());
	}

	/**
	 * maxのテスト
	 * 大きい方の値が返却されることを確認する
	 */
	@isTest static void maxTest() {
		AttDailyTime value1 = new AttDailyTime(1);
		AttDailyTime value2 = new AttDailyTime(2);

		// 値が異なる場合
		System.assertEquals(value2, AttDailyTime.max(value1, value2));
		System.assertEquals(value2, AttDailyTime.max(value2, value1));
		// 値が等しい場合
		System.assertEquals(value2, AttDailyTime.max(value2, new AttDailyTime(2)));
	}

	/**
	 * sumのテスト
	 * 正しく加算できることを確認する
	 */
	@isTest static void sumTest() {
		AttDailyTime value1 = new AttDailyTime(1);
		AttDailyTime value2 = new AttDailyTime(2);

		// 結果が正しい
		System.assertEquals(new AttDailyTime(3), value1.sum(value2));
		// 元の値オブジェクトの値が変更されていない
		System.assertEquals(new AttDailyTime(1), value1);
	}

	/**
	 * toStringのテスト
	 * 値が文字列で出力できることを確認する
	 */
	@isTest static void toStringTest() {
		AttDailyTime value1 = new AttDailyTime(123);
		System.assertEquals('123', value1.toString());
	}
}
