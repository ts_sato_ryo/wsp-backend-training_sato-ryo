/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Entity class representing the Cost Center History Record
 */
public with sharing class CostCenterHistoryEntity extends CostCenterHistoryGeneratedEntity {
	public final static Integer NAME_MAX_LENGTH = 80;
	public final static Integer LINKAGE_CODE_MAX_LENGTH = 20;

	public CostCenterHistoryEntity() {
		super();
	}

	public CostCenterHistoryEntity(ComCostCenterHistory__c sobj) {
		super(sobj);
	}

	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	/** parentBaseCode for use in CostCenterUpdateBatch and ComConfigValidator */
	public String parentBaseCode {
		get {
			if (parentBaseCode == null) {
				parentBaseCode = sobj.BaseId__r.Code__c;
			}
			return parentBaseCode;
		}
		set;
	}

	public LookupField parentBase;

	/**
	 * Create a Copy of CostCenterHistoryEntity(ID is not copied.)
	 */
	public CostCenterHistoryEntity copy() {
		CostCenterHistoryEntity copyEntity = new CostCenterHistoryEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 */
	public override ParentChildHistoryEntity copySuperEntity() {
		return copy();
	}
}