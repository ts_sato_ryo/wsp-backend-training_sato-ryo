/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * AttDirectRequestServiceのテストクラス
 */
@isTest
private class AttDirectRequestServiceTest {

	private static AttDirectRequestService requestService = new AttDirectRequestService();
	private static AttDailyRequestRepository requestRepository = new AttDailyRequestRepository();
	private static AttAttendanceService attService = new AttAttendanceService();
	private static AttSummaryRepository attRepository = new AttSummaryRepository();

	/**
	 * テストに使用する共通のデータを定義する。
	 */
	private class TestData {

		/** 申請者 */
		public EmployeeBaseEntity employee;

		/** 承認者 */
		public EmployeeBaseEntity approver01;

		/** 勤務体系 */
		public AttWorkingTypeBaseEntity workingType;

		public TestData() {
			AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
			List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(2);
			employee = testData.employee;
			approver01 = testEmpList[1];
			workingType = testData.workingType;
			// 承認者01設定
			update new ComEmpHistory__c(Id = employee.getHistory(0).id, ApproverBase01Id__c = approver01.id);

			// 振替休日を作成する
			AttLeaveEntity substituteLeave = testData.createLeave('x', '振替休暇', AttLeaveType.SUBSTITUTE, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
			testData.workingType.getHistory(0).leaveCodeList = new List<String>{substituteLeave.code};
			new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));
		}
	}

	/**
	 * 申請：正常系
	 * 勤務日に直行直帰申請出来る事を検証する。
	 * 申請エンティティに存在する直行直帰申請用の全項目を検証する。
	 */
	@isTest static void submitDay() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);
		param.rest1StartTime = AttTime.newInstance(10, 0);
		param.rest1EndTime = AttTime.newInstance(10, 10);
		param.rest2StartTime = AttTime.newInstance(11, 0);
		param.rest2EndTime = AttTime.newInstance(11, 10);
		param.rest3StartTime = AttTime.newInstance(14, 0);
		param.rest3EndTime = AttTime.newInstance(14, 10);
		param.rest4StartTime = AttTime.newInstance(15, 0);
		param.rest4EndTime = AttTime.newInstance(15, 10);
		param.rest5StartTime = AttTime.newInstance(16, 0);
		param.rest5EndTime = AttTime.newInstance(16, 10);
		param.remarks = '備考';

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.DIRECT, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.endDate);
		System.assertEquals(AttTime.newInstance(9, 0), request.startTime);
		System.assertEquals(AttTime.newInstance(18, 0), request.endTime);
		System.assertEquals(AttTime.newInstance(10, 0), request.rest1StartTime);
		System.assertEquals(AttTime.newInstance(10, 10), request.rest1EndTime);
		System.assertEquals(AttTime.newInstance(11, 0), request.rest2StartTime);
		System.assertEquals(AttTime.newInstance(11, 10), request.rest2EndTime);
		System.assertEquals(AttTime.newInstance(14, 0), request.rest3StartTime);
		System.assertEquals(AttTime.newInstance(14, 10), request.rest3EndTime);
		System.assertEquals(AttTime.newInstance(15, 0), request.rest4StartTime);
		System.assertEquals(AttTime.newInstance(15, 10), request.rest4EndTime);
		System.assertEquals(AttTime.newInstance(16, 0), request.rest5StartTime);
		System.assertEquals(AttTime.newInstance(16, 10), request.rest5EndTime);
		System.assertEquals('備考', request.remarks);
		System.assertEquals(AppRequestStatus.PENDING, request.status);
		System.assertEquals(AppCancelType.NONE, request.cancelType);
		System.assertEquals(false, request.isConfirmationRequired);

		// 勤怠明細を検証
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 3));
		System.assertEquals(request.id, attRecord.inpRequestingDirectRequestId);
		System.assertEquals(null, attRecord.inpDirectRequestId);
	}

	/**
	 * 申請：正常系
	 * 複数の勤務日に直行直帰申請出来る事を検証する。
	 * 申請エンティティの休憩時間がソートされていることを検証する。
	 */
	@isTest static void submitDays() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 5);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);
		param.rest1StartTime = AttTime.newInstance(16, 0);
		param.rest1EndTime = AttTime.newInstance(16, 10);
		param.rest3StartTime = AttTime.newInstance(14, 0);
		param.rest3EndTime = AttTime.newInstance(14, 10);
		param.rest5StartTime = AttTime.newInstance(10, 0);
		param.rest5EndTime = AttTime.newInstance(10, 10);

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.DIRECT, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 5), request.endDate);
		System.assertEquals(AttTime.newInstance(9, 0), request.startTime);
		System.assertEquals(AttTime.newInstance(18, 0), request.endTime);
		System.assertEquals(AttTime.newInstance(10, 0), request.rest1StartTime);
		System.assertEquals(AttTime.newInstance(10, 10), request.rest1EndTime);
		System.assertEquals(AttTime.newInstance(14, 0), request.rest2StartTime);
		System.assertEquals(AttTime.newInstance(14, 10), request.rest2EndTime);
		System.assertEquals(AttTime.newInstance(16, 0), request.rest3StartTime);
		System.assertEquals(AttTime.newInstance(16, 10), request.rest3EndTime);

		// 勤怠明細を検証
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 3));
			System.assertEquals(request.id, attRecord.inpRequestingDirectRequestId);
			System.assertEquals(null, attRecord.inpDirectRequestId);
		}
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 4));
			System.assertEquals(request.id, attRecord.inpRequestingDirectRequestId);
			System.assertEquals(null, attRecord.inpDirectRequestId);
		}
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 9, 5));
			System.assertEquals(request.id, attRecord.inpRequestingDirectRequestId);
			System.assertEquals(null, attRecord.inpDirectRequestId);
		}
	}

	/**
	 * 申請：正常系
	 * 月跨ぎの直行直帰申請を行える事を検証する。
	 */
	@isTest static void submitOverMonth() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 10, 31);
		param.endDate = AppDate.newInstance(2018, 11, 1);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.DIRECT, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 10, 31), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 11, 1), request.endDate);
		System.assertEquals(AttTime.newInstance(9, 0), request.startTime);
		System.assertEquals(AttTime.newInstance(18, 0), request.endTime);

		// 勤怠明細を検証
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 10, 31));
			System.assertEquals(request.id, attRecord.inpRequestingDirectRequestId);
			System.assertEquals(null, attRecord.inpDirectRequestId);
		}
		{
			// 勤務日
			AttRecordEntity attRecord = attService.getRecord(testData.employee.id, AppDate.newInstance(2018, 11, 1));
			System.assertEquals(request.id, attRecord.inpRequestingDirectRequestId);
			System.assertEquals(null, attRecord.inpDirectRequestId);
		}
	}

	/**
	 * 申請：異常系
	 * 勤務体系.直行直帰申請を使用するオプションがOffの場合、直行直帰申請出来ないことを検証する。
	 */
	@isTest static void submitUnuseDirectOption() {
		// 直行直帰申請を使用しない
		TestData testData = new TestData();
		update new AttWorkingTypeHistory__c(Id = testData.workingType.getHistoryList()[0].id, UseDirectApply__c = false);

		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_NotPermittedDirectRequest, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 直行直帰申請の開始日 > 終了日の場合、直行直帰申請出来ないことを検証する。
	 */
	@isTest static void submitInvalidDate() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 4);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_InvalidStartEndDate, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 直行直帰申請の開始日〜終了日に勤務日以外が含まれている場合、直行直帰申請出来ないことを検証する。
	 */
	@isTest static void submitNotWorkday() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 7);
		param.endDate = AppDate.newInstance(2018, 9, 10);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_TheDayIsNotWorkDay(
					new List<String>{param.startDate.addDays(1).format(AppDate.FormatType.YYYY_MM_DD_SLASH)}), e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 直行直帰申請の開始日〜終了日に勤務確定した日が含まれている場合、直行直帰申請出来ないことを検証する。
	 */
	@isTest static void submitLockedSummary() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 10, 31);
		param.endDate = AppDate.newInstance(2018, 11, 2);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 勤怠明細を更新（ロック）
		List<AttSummaryEntity> summaryList = attService.getSummaryList(param.empId, param.startDate, param.endDate);
		summaryList[0].isLocked = true;
		attRepository.saveEntity(summaryList[0]);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_Locked(new List<String>{ComMessage.msg().Att_Lbl_TimeSheet}), e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 直行直帰申請の開始日〜終了日に休職休業日が含まれている場合、直行直帰申請出来ないことを検証する。
	 */
	@isTest static void submitLeaveOfAbsence() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 6);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 勤怠明細を更新（休職休業）
		attService.getSummaryList(param.empId, param.startDate, param.endDate);
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, param.startDate);
		AttLeaveOfAbsenceEntity leaveOfAbsence = new TestData.TestDataEntity().leaveOfAbsence;
		attRecord.leaveOfAbsenceId = leaveOfAbsence.id;
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_Record_LeaveOfAbsent(
					new List<String>{ComMessage.msg().Att_Lbl_RequestTypeDirect + ComMessage.msg().Att_Lbl_Request}), e.getMessage());
		}
		Test.stopTest();
	}
	/**
	 * 申請：異常系
	 * 直行直帰申請の開始日〜終了日に欠勤申請日が含まれている場合、直行直帰申請出来ないことを検証する。
	 */
	@isTest static void submitAbsence() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 6);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 勤怠明細を更新（欠勤申請）
		attService.getSummaryList(param.empId, param.startDate, param.endDate);
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, param.startDate);
		AttDailyRequestEntity leaveRequest = createLeaveRequest(AttLeaveRange.RANGE_AM);
		attRecord.reqAbsenceRequestId = leaveRequest.id; // ダミId
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_RequestedAbsence, e.getMessage());
		}
		Test.stopTest();
	}
	/**
	 * 申請：異常系
	 * 直行直帰申請の開始日〜終了日に出退勤打刻済みの場合、直行直帰申請出来ないことを検証する。
	 */
	@isTest static void submitStamped() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 5);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 勤怠明細を更新（打刻）
		AttRecordEntity attRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		attRecord.inpStartTime = AttTime.newInstance(9, 0);
		attRecord.inpStartStampTime = AttTime.newInstance(9, 0);
		attRepository.saveRecordEntity(attRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_NotAllowInpWorkTimeOnDirect, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：正常系
	 * 半日休暇を1件取得時に、直行直帰申請出来る事を検証する。
	 */
	@isTest static void submitWithHalfLeave() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 勤怠明細を更新（休暇申請-半日）
		AttDailyRequestEntity leaveRequest = createLeaveRequest(AttLeaveRange.RANGE_AM);
		AttRecordEntity leaveRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		leaveRecord.reqLeave1RequestId = leaveRequest.id;
		leaveRecord.reqLeave1Range = leaveRequest.leaveRange;
		attRepository.saveRecordEntity(leaveRecord);

		// 申請
		Test.startTest();
		AttDailyRequestEntity request = requestService.submit(param);
		Test.stopTest();

		// 申請情報を検証
		System.assertEquals(AttRequestType.DIRECT, request.requestType);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.startDate);
		System.assertEquals(AppDate.newInstance(2018, 9, 3), request.endDate);
		System.assertEquals(AttTime.newInstance(9, 0), request.startTime);
		System.assertEquals(AttTime.newInstance(18, 0), request.endTime);

		// 勤怠明細を検証
		AttRecordEntity attRecord = attService.getRecord(testData.employee.id, param.startDate);
		System.assertEquals(request.id, attRecord.inpRequestingDirectRequestId);
		System.assertEquals(null, attRecord.inpDirectRequestId);
		System.assertEquals(leaveRequest.id, attRecord.reqLeave1RequestId);
		System.assertEquals(null, attRecord.reqRequestingLeave1RequestId);
	}

	/**
	 * 申請：異常系
	 * 全日休暇取得時に、直行直帰申請出来ない事を検証する。
	 */
	@isTest static void submitWithDayLeave() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 勤怠明細を更新（休暇申請-全日）
		AttDailyRequestEntity leaveRequest = createLeaveRequest(AttLeaveRange.RANGE_DAY);
		AttRecordEntity leaveRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		leaveRecord.reqLeave1RequestId = leaveRequest.id;
		leaveRecord.reqLeave1Range = leaveRequest.leaveRange;
		attRepository.saveRecordEntity(leaveRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			String expected = ComMessage.msg().Att_Err_RequestedDayLeave;
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 申請：異常系
	 * 午前半日休暇、午後半日休暇を取得時に、直行直帰申請出来ない事を検証する。
	 */
	@isTest static void submitWithLeaves() {
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 勤怠明細を更新（休暇申請-半日2件）
		AttDailyRequestEntity leaveRequestAM = createLeaveRequest(AttLeaveRange.RANGE_AM);
		AttDailyRequestEntity leaveRequestPM = createLeaveRequest(AttLeaveRange.RANGE_PM);
		AttRecordEntity leaveRecord = attService.getSummary(param.empId, param.startDate).getAttRecord(param.startDate);
		leaveRecord.reqLeave1RequestId = leaveRequestAM.id;
		leaveRecord.reqLeave1Range = leaveRequestAM.leaveRange;
		leaveRecord.reqLeave2RequestId = leaveRequestPM.id;
		leaveRecord.reqLeave2Range = leaveRequestPM.leaveRange;
		attRepository.saveRecordEntity(leaveRecord);

		// 申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			String expected = ComMessage.msg().Att_Err_RequestedDayLeave;
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * 申請：異常系
	 * 直行直帰申請日に直行直帰申請出来ない事を検証する。
	 */
	@isTest static void submitWithDirect() {
		// 直行直帰申請
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);
		requestService.submit(param);

		// 同日に申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_RequestedDirect, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 申請期間が長すぎる場合は申請できないことを確認する
	 */
	@isTest static void submitWithLongPeriod() {
		// 直行直帰申請
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 10, 1);
		param.endDate = AppDate.newInstance(2018, 11, 1);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 申請期間は32日間で申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String expMsg = ComMessage.msg().Att_Err_RequesePeriodTooLong(new List<String>{'31'});
			System.assertEquals(expMsg, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 申請期間が2日以上で、出勤時刻と退勤時刻の差が24時間を超える場合は申請できないことを確認する
	 */
	@isTest static void submitWithTimeDiffOver() {
		// 直行直帰申請
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 4);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(33, 1);

		// 申請期間を2日、出退勤時刻の差を24時間1分で申請
		Test.startTest();
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_OverlapWithNextDayDirectRequest, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * 申請：異常系
	 * 休憩の開始終了時刻を確認する
	 */
	@isTest static void submitWithInvalidRestTime() {
		// 直行直帰申請
		TestData testData = new TestData();
		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testData.employee.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 休憩の開始終了時刻が揃っていない場合は申請できないことを確認する
		param.rest1StartTime = AttTime.newInstance(10, 0);
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_InvalidBothRestStartEndTime(new List<String>{'1'}), e.getMessage());
		}
		// 休憩の終了時刻が開始時刻より前の場合は申請できないことを確認する
		param.rest1StartTime = AttTime.newInstance(10, 0);
		param.rest1EndTime = AttTime.newInstance(9, 59);
		try {
			requestService.submit(param);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Att_Err_InvalidRestStartEndTime(new List<String>{'1'}), e.getMessage());
		}
	}

	/**
	 * createCancelMailMessageのテスト
	 * 承認取消メールが正常に作成できることを確認する
	 */
	@isTest static void createCancelMailMessageTest() {

		AttDirectRequestService service = new AttDirectRequestService();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Flex);
		EmployeeBaseEntity testEmp1 = testData.employee;
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// 直行直帰申請のテスト
		AttDailyRequestEntity request = testData.createDirectRequest(
				AppDate.newInstance(2018, 10, 1), AppDate.newInstance(2018, 10, 2), 540, 1080);
		request.requestTime = AppDatetime.valueOf(Datetime.newInstance(2018, 10, 1));
		request.remarks = 'テスト備考';

		// 実行
		MailService.MailParam result = service.createCancelMailMessage(request, testEmp1, testEmp2, AppLanguage.JA);
		// 検証
		// 送信者名
		System.assertEquals('adminTest_L0 太郎_L0', result.senderDisplayName);
		// メールタイトル
		System.assertEquals('001_L0 太郎_L0さんの直行直帰の承認が取消されました', result.subject);
		// メール本文
		System.assertEquals('001_L0 太郎_L0さんの直行直帰の承認がadminTest_L0 太郎_L0さんによって取消されました'
				+ '\r\n\r\n'
				+ '【申請内容】\r\n'
				+ '申請日付：2018/10/01\r\n'
				+ '申請種別：直行直帰\r\n'
				+ '期間：2018/10/01－2018/10/02\r\n'
				+ '出退勤時刻：09:00－18:00\r\n'
				+ '休憩1：09:10－09:20\r\n'
				+ '休憩2：09:30－09:40\r\n'
				+ '休憩3：09:50－10:00\r\n'
				+ '休憩4：10:10－10:20\r\n'
				+ '休憩5：10:30－10:40\r\n'
				+ '備考：テスト備考\r\n'
				+ '\r\n---\r\n'
				+ 'チームスピリット', result.body);
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が無効の場合、アプリ例外が発生することを確認する
	 */
	@isTest
	static void selfApprovePermissionTestFailure() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 各種勤怠申請自己承認(本人)の権限を無効に設定する
		testData.permission.isApproveSelfAttDailyRequestByEmployee = false;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testEmp1.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 実行
		System.runAs(AttTestData.getUser(testEmp1.userId)) {
			App.NoPermissionException actEx;
			try {
				AttDailyRequestEntity request = new AttDirectRequestService().submit(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
			String expectMessage = ComMessage.msg().Att_Err_NotAllowSelfAttDailyRequest(new List<String>{testEmp1.fullNameL.getFullName()});
			System.assertEquals(expectMessage, actEx.getMessage());
		}
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が有効の場合、アプリ例外が発生しないことを確認する
	 */
	@isTest
	static void selfApprovePermissionTestSuccess() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 各種勤怠申請自己承認(本人)の権限を有効に設定する
		testData.permission.isApproveSelfAttDailyRequestByEmployee = true;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();
		param.empId = testEmp1.id;
		param.startDate = AppDate.newInstance(2018, 9, 3);
		param.endDate = AppDate.newInstance(2018, 9, 3);
		param.startTime = AttTime.newInstance(9, 0);
		param.endTime = AttTime.newInstance(18, 0);

		// 実行
		System.runAs(AttTestData.getUser(testEmp1.userId)) {
			App.NoPermissionException actEx;
			try {
				AttDailyRequestEntity request = new AttDirectRequestService().submit(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertEquals(null, actEx);
		}
	}

	/**
	 * createRequestNameのテスト
	 * 申請名が正しく作成できることを確認する
	 */
	@isTest static void createRequestNameTest() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp = testData.employee;

		AttDirectRequestService.SubmitParam param = new AttDirectRequestService.SubmitParam();

		param.startDate = AppDate.newInstance(2018, 6, 5);
		param.endDate = AppDate.newInstance(2018, 6, 5);
		param.startTime = AttTime.newInstance(9, 00);
		param.endTime = AttTime.newInstance(18, 00);

		AttDirectRequestService service = new AttDirectRequestService();

		// 日本語の場合
		AppLanguage lang = AppLanguage.JA;
		// 実行
		String requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		System.assertEquals('001 太郎1_L0さん 直行直帰申請 20180605-20180605', requestName);

		// 英語の場合
		lang = AppLanguage.EN_US;
		// 実行
		requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		// ComTestDataUtility.createEmployeesWithHistoryテストデータの表示名の作成メソッドが不正なので名＋半角スペース＋姓にならない
		System.assertEquals('001 太郎1_L1 Direct Attendance Request 20180605-20180605', requestName);
	}

	/**
	 * 休暇申請を作成し保存する。
	 * @param 休暇範囲
	 * @return 登録したエンティティ（登録したIDを保持したエンティティ）
	 */
	private static AttDailyRequestEntity createLeaveRequest(AttLeaveRange range) {
		AttDailyRequestEntity request = new AttDailyRequestEntity();
		request.requestType = AttRequestType.LEAVE;
		request.leaveRange = range;
		Repository.SaveResult result = requestRepository.saveEntity(request);
		request.setId(result.details[0].id);
		return request;
	}
}