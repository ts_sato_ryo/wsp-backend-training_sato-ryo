/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description 代理承認者設定のサービスクラス Service class for delegated approver setting function
 */
public with sharing class DelegatedApproverSettingService {

	/**
	 * @description 代理承認者設定一覧データを取得する Get delegated approver setting list
	 * @param employeeBaseId 社員ベースID
	 * @return 代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getSettingList(Id employeeBaseId) {

		// 代理承認者設定一覧を取得 Get delegated approver setting list
		List<DelegatedApproverSettingEntity> settingList = new DelegatedApproverSettingRepository().getEntityListByEmpId(employeeBaseId);
		if (settingList == null || settingList.isEmpty()) {
			return null;
		}

		return settingList;
	}

	/*
	 * Create delegated approver setting list map.
	 * @param approverIds Approver IDs
	 * @return Map of delegated approver setting. key : approver base id, value : list of delegated approver list
	 */
	public Map<Id, List<DelegatedApproverSettingEntity>> getSettingMap(Set<Id> employeeBaseId) {
		List<DelegatedApproverSettingEntity> settingListAll = new DelegatedApproverSettingRepository().getEntityListByEmpIds(employeeBaseId);

		Map<Id, List<DelegatedApproverSettingEntity>> settingMap = new Map<Id, List<DelegatedApproverSettingEntity>>();
		for (DelegatedApproverSettingEntity setting : settingListAll) {
			List<DelegatedApproverSettingEntity> settingListEmp;
			if (!settingMap.containsKey(setting.employeeBaseId)) {
				settingListEmp = new List<DelegatedApproverSettingEntity>();
			} else {
				settingListEmp = settingMap.get(setting.employeeBaseId);
			}
			settingListEmp.add(setting);
			settingMap.put(setting.employeeBaseId, settingListEmp);
		}
		return settingMap;
	}

	/**
	 * 代理承認者設定データを保存する Save Delegate Approver settings
	 * @param employee 社員ベースエンティティ EmployeeBase Entity
	 * @param newSettingList 代理承認者設定エンティティのリスト List of Delegate Approver Setting Entities
	 * NOTE: This function will remove existing Delegate Approver Settings if they are not specified in this list
	 */
	public void saveSettingList(EmployeeBaseEntity employee, List<DelegatedApproverSettingEntity> newSettingList) {
		DelegatedApproverSettingRepository repo = new DelegatedApproverSettingRepository();

		// 代理承認者設定一覧を取得 Get existing delegated approver setting map
		Map<Id, DelegatedApproverSettingEntity> existingSettingMap = new Map<Id, DelegatedApproverSettingEntity>();
		for (DelegatedApproverSettingEntity existingEntity : repo.getEntityListByEmpId(employee.id)) {
			existingSettingMap.put(existingEntity.id, existingEntity);
		}
		
		// Compare new setting list with the existing map
		if (newSettingList != null && !newSettingList.isEmpty() && !existingSettingMap.isEmpty()) {
			for (DelegatedApproverSettingEntity newEntity : newSettingList) {
				if (newEntity.id != null) {
					// Remove from map
					existingSettingMap.remove(newEntity.id);
				}
			}
		}

		// Delete settings that are removed in newSettingList
		if (!existingSettingMap.isEmpty()) {
			// 対象の代理承認者設定を削除 Delete target settings
			repo.deleteEntityList(existingSettingMap.values());
		}

		// 代理承認者設定データを保存する Save Delegate Approver Setting data
		repo.saveEntityList(newSettingList);
		// After new settings are created,
		// sharing records are going to be created in DelegatedApproverSettingTrigger.
	}

	/**
	 * 代理承認者設定データを削除する
	 * @param settingIdList 削除対象の代理承認者設定IDのリスト
	 */
	public void deleteSettingList(List<Id> settingIdList) {
		if (settingIdList == null || settingIdList.isEmpty()) {
			return;
		}

		DelegatedApproverSettingRepository repo = new DelegatedApproverSettingRepository();

		// 削除対象の代理承認者設定を取得 Get target settings
		List<DelegatedApproverSettingEntity> deleteSettingList = repo.getEntityList(settingIdList);
		if (deleteSettingList == null || deleteSettingList.isEmpty()) {
			return;
		}

		// 共有元社員の情報を取得（共有元社員はすべて同じ前提）
		Id empId = deleteSettingList[0].employeeBaseId;
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(empId, AppDate.today());

		// 対象の代理承認者設定を削除 Delete target settings
		repo.deleteEntityList(deleteSettingList);
		// After new settings are deleted,
		// sharing records are going to be deleted in DelegatedApproverSettingTrigger.
	}

}