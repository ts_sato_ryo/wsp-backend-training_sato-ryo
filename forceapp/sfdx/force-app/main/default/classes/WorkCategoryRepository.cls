/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 作業分類リポジトリ
 */
public with sharing class WorkCategoryRepository extends ValidPeriodRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * 取得対象の項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				TimeWorkCategory__c.Id,
				TimeWorkCategory__c.Name,
				TimeWorkCategory__c.Code__c,
				TimeWorkCategory__c.UniqKey__c,
				TimeWorkCategory__c.Name_L0__c,
				TimeWorkCategory__c.Name_L1__c,
				TimeWorkCategory__c.Name_L2__c,
				TimeWorkCategory__c.CompanyId__c,
				TimeWorkCategory__c.Order__c,
				TimeWorkCategory__c.ValidFrom__c,
				TimeWorkCategory__c.ValidTo__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** 作業分類ID */
		public Set<Id> ids;
		/** 会社ID */
		public Set<Id> companyIds;
		/** コード(完全一致) */
		public Set<String> codes;
		/** 取得対象日 */
		public AppDate targetDate;
		/**
		 * 取得対象日範囲
		 * 開始日から終了日のうち、1日でも有効なジョブがあれば取得対象とする。
		 * 終了日に有効なジョブも含む。
		 */
		public AppDateRange dateRange;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param jobId ジョブID
	 * @return ベースエンティティの検索結果
	 */
	public WorkCategoryEntity getEntity(Id workCategoryId) {
		WorkCategoryRepository.SearchFilter filter = new WorkCategoryRepository.SearchFilter();
		filter.ids = new Set<Id>{workCategoryId};
		List<WorkCategoryEntity> entities = searchEntity(filter, NOT_FOR_UPDATE);

		if (entities.isEmpty()) {
			return null;
		} else {
			return entities[0];
		}
	}

	/**
	 * 指定したコードと会社IDを持つエンティティを取得する
	 * @param code 取得対象のコード
	 * @param companyId 取得対象の会社ID
	 * @return ベースエンティティの検索結果
	 */
	public WorkCategoryEntity getEntityByCode(String code, Id companyId) {
		List<WorkCategoryEntity> entities = getEntityListByCode(new Set<String>{code}, companyId);

		if (entities.isEmpty()) {
			return null;
		} else {
			return entities[0];
		}
	}

	/**
	 * 指定したコードと会社IDを持つエンティティを取得する
	 * @param codes 取得対象のコード
	 * @param companyId 取得対象の会社ID
	 * @return ベースエンティティの検索結果
	 */
	public List<WorkCategoryEntity> getEntityListByCode(Set<String> codes, Id companyId) {
		WorkCategoryRepository.SearchFilter filter = new WorkCategoryRepository.SearchFilter();
		filter.codes = codes;
		filter.companyIds = new Set<Id>{companyId};
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	public List<WorkCategoryEntity> searchEntity(WorkCategoryRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	public List<WorkCategoryEntity> searchEntity(WorkCategoryRepository.SearchFilter filter, Boolean forUpdate) {

		// WHERE句
		List<String> whereList = new List<String>();
		// 作業分類IDで検索
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(TimeWorkCategory__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(TimeWorkCategory__c.Code__c) + ' IN :pCodes');
		}
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(TimeWorkCategory__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(TimeWorkCategory__c.ValidTo__c) + ' > :pTargetDate');
		}
		// 対象日範囲
		Date pStartDate;
		Date pEndDate;
		if (filter.dateRange != null) {
			pStartDate = filter.dateRange.startDate.getDate();
			pEndDate = filter.dateRange.endDate.getDate();
			whereList.add(getFieldName(ComJob__c.ValidTo__c) + ' > :pStartDate');
			whereList.add(getFieldName(ComJob__c.ValidFrom__c) + ' <= :pEndDate');
		}

		String whereString = buildWhereString(whereList);


		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + TimeWorkCategory__c.SObjectType.getDescribe().getName()
				+ whereString
				+ ' ORDER BY ' + getFieldName(TimeWorkCategory__c.Code__c) + ' Asc NULLS LAST'
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('TimeWorkCategoryRepository: SOQL=' + soql);

		// クエリ実行
		List<TimeWorkCategory__c> sObjs = (List<TimeWorkCategory__c>)Database.query(soql);

		// SObjectからエンティティを作成
		List<WorkCategoryEntity> entities = new List<WorkCategoryEntity>();
		for (TimeWorkCategory__c sObj : sObjs) {
			entities.add(new WorkCategoryEntity(sObj));
		}

		return entities;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntity> entityList) {

		List<TimeWorkCategory__c> objectList = new List<TimeWorkCategory__c>();
		for (ValidPeriodEntity entity : entityList) {
			objectList.add(((WorkCategoryEntity)entity).createSObject());
		}

		return objectList;
	}
}