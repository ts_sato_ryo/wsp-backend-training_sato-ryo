/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description タグエンティティ
 */
public with sharing class TagEntity extends TagGeneratedEntity {

	/** コードの最大文字列長さ */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 名前の最大文字列長さ */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 表示順の最小値 */
	public static final Integer ORDER_MIN_VALUE = 1;
	/** 表示順の最大値 */
	public static final Integer ORDER_MAX_VALUE = 9999;

	/** タグ名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(nameL0, nameL1, nameL2);}
	}

	/**
	 * コンストラクタ
	 */
	public TagEntity() {
		super(new ComTag__c());
	}

	public TagEntity(ComTag__c sobj) {
		super(sobj);
	}

	/**
	 * ユニークキーを作成する
	 * タグタイプ、タグコードを設定しておくこと。
	 * @param companyCode 会社コード
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]タグのユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (this.tagTypeValue == null) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]タグのユニークキーが作成できませんでした。タグタイプが設定されていません。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]タグのユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// タグタイプ.プレフィックス + '-' + 会社コード + '-' + コード
		return this.tagTypeValue.prefix + '-' + companyCode + '-' + this.code;
	}
}