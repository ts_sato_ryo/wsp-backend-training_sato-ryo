/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test for repository of Exchange Rate
 */
@isTest
private class ExpExchangeRateRepositoryTest {

	/** Test Data */
	private class RepoTestData {

		/** Organization object(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** Country object */
		public ComCountry__c countryObj;
		/** Company object */
		public ComCompany__c companyObj;
		/** Currency object */
		public ComCurrency__c currencyObj;

		/** Constructor */
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
			currencyObj = ComTestDataUtility.createCurrency('USD', 'US dollar');
		}

		/**
		 * Create Exchange Rate objects
		 */
		public List<ExpExchangeRate__c> createExpExchangeRates(String code, Integer size) {
			return ComTestDataUtility.createExpExchangeRates(code, this.companyObj.Id, this.currencyObj.Id, size);
		}

		/**
		 * Create Company object
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}
	}

	/**
	 * Test for getEntity method
	 * Confirm being enable to get entity by specified ID
	 */
	@isTest
	static void getEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpExchangeRate__c> testCurrencyRate = testData.createExpExchangeRates('SearchTest', recordSize);
		ExpExchangeRate__c targetObj = testCurrencyRate[1];

		ExpExchangeRateRepository repo = new ExpExchangeRateRepository();
		ExpExchangeRateEntity resEntity;

		// Check existing ID
		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);

		// Check not existing ID
		delete targetObj;
		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * Test for searchEntity method
	 * Confirm enable to search data properly
	 */
	@isTest
	static void searchEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		testData.createCompany('Search Test Co.');
		List<ExpExchangeRate__c> testGroups = testData.createExpExchangeRates('SearchTest', recordSize);
		ExpExchangeRate__c targetObj = testGroups[1];


		ExpExchangeRateRepository repo = new ExpExchangeRateRepository();
		ExpExchangeRateRepository.SearchFilter filter;
		List<ExpExchangeRateEntity> resEntities;

		// Search by ID
		filter = new ExpExchangeRateRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
		// Check fields value
		assertFieldValue(targetObj, testData.currencyObj, resEntities[0]);
	}

	/**
	 * Test for searchEntity method
	 * Confirm being enable to get valid data by specified date.
	 */
	@isTest
	static void searchEntityTestValidDate() {

		// Create test data
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		testData.createCompany('Search Test Co.');
		List<ExpExchangeRate__c> targetObjs = testData.createExpExchangeRates('Search Test', recordSize);
		ExpExchangeRate__c targetObj1 = targetObjs[0];
		ExpExchangeRate__c targetObj2 = targetObjs[1];
		ExpExchangeRate__c targetObj3 = targetObjs[2];
		targetObj2.ValidFrom__c = targetObj1.ValidTo__c;
		targetObj2.ValidTo__c = targetObj2.ValidFrom__c.addYears(1);
		targetObj3.ValidFrom__c = targetObj2.ValidTo__c;
		targetObj3.ValidTo__c = targetObj3.ValidFrom__c.addYears(1);
		update targetObjs;

		ExpExchangeRateRepository repo = new ExpExchangeRateRepository();
		ExpExchangeRateRepository.SearchFilter filter;
		List<ExpExchangeRateEntity> resEntities;

		// Specify start date of the data as a target date
		filter = new ExpExchangeRateRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj2.ValidFrom__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// Specify one day before the expire date of the latest data as target date
		// Should be able to get data
		filter = new ExpExchangeRateRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c.addDays(-1));
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj3.Id, resEntities[0].id);

		// Specify the expire date of the latest data as target date
		// Should not be able to get data
		filter = new ExpExchangeRateRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());
	}

	/**
	 * Check fields value
	 * @param expObj Expected value
	 * @param actEntity Actual value
	 */
	private static void assertFieldValue(ExpExchangeRate__c expObj, ComCurrency__c curObj, ExpExchangeRateEntity actEntity) {
		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.CurrencyId__c, actEntity.currencyId);
		System.assertEquals(curObj.IsoCurrencyCode__c, actEntity.currencyCode);
		System.assertEquals(new AppMultiString(curObj.Name_L0__c, curObj.Name_L1__c, curObj.Name_L2__c).getValue(),
			actEntity.currencyNameL.getValue());
		System.assertEquals(expObj.CurrencyPair__c, actEntity.currencyPair.value);
		System.assertEquals(expObj.Rate__c, actEntity.rate);
		System.assertEquals(expObj.ReverseRate__c, actEntity.reverseRate);
		System.assertEquals(AppDate.valueOf(expObj.ValidFrom__c), actEntity.validFrom);
		System.assertEquals(AppDate.valueOf(expObj.ValidTo__c), actEntity.validTo);
	}
}