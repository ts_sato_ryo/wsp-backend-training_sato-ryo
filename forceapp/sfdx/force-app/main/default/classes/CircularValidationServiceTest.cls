/**
 * @group 共通
 *
 * @description 階層関係のオブジェクト構造の再帰参照チェックサービステスト
 */
@isTest
private class CircularValidationServiceTest {
	/** 通常の階層関係が正しく登録できるテスト */
	@isTest static void testNormal() {
		// D -> C -> B -> A 
		CircularValidationService svc = new CircularValidationService();
		Boolean hasError = false;
		try {
			svc.appendChild('D', 'C');
			svc.appendChild('C', 'B');
			svc.appendChild('B', 'A');
		} catch (CircularValidationService.OverLevelException e) {
			System.assert(false, 'unexpected OverLevelException');
		} catch (CircularValidationService.CircularException e) {
			System.assert(false, 'unexpected CircularException');
		} catch (Exception e) {
			System.assert(false, 'unexpected Exception');
		}
	}
	/** ルート項目が2つある階層関係が正しく登録できるテスト */
	@isTest static void testNormalMoreRoot() {
		// E (->) D1 -> C -> B1 -> A1
		// E (->) D2 -> C -> B2 -> A2
		CircularValidationService svc = new CircularValidationService();
		try {
			svc.appendChild('E', 'D1');
			svc.appendChild('E', 'D2');
			svc.appendChild('D1', 'C');
			svc.appendChild('D2', 'C');
			svc.appendChild('C', 'B1');
			svc.appendChild('C', 'B2');
			svc.appendChild('B1', 'A1');
			svc.appendChild('B2', 'A2');
		} catch (CircularValidationService.OverLevelException e) {
			System.assert(false, 'unexpected OverLevelException');
		}catch (CircularValidationService.CircularException e) {
			System.assert(false, 'unexpected CircularException');
		} catch (Exception e) {
			System.assert(false, 'unexpected Exception');
		}
	}
	/** 2つの階層グループを正しくつなげるテスト */
	@isTest static void testNormalConnectTwoRoute() {
		// D1 -> C1 -> B1 -> A1 (->) D2
		// D2 -> C2 -> B2 -> A2
		CircularValidationService svc = new CircularValidationService();
		try {
			svc.appendChild('A1', 'D2');
			svc.appendChild('D2', 'C2');
			svc.appendChild('C2', 'B2');
			svc.appendChild('B2', 'A2');
			svc.appendChild('B1', 'A1');
			svc.appendChild('C1', 'B1');
			svc.appendChild('D1', 'C1');
		} catch (CircularValidationService.OverLevelException e) {
			System.assert(false, 'unexpected OverLevelException');
		}catch (CircularValidationService.CircularException e) {
			System.assert(false, 'unexpected CircularException');
		} catch (Exception e) {
			System.assert(false, 'unexpected Exception');
		}
	}
	/** 階層数がオーバーするテスト */
	@isTest static void testErrorOverLevel() {
		// E -> D -> C -> B (->) A
		CircularValidationService svc = new CircularValidationService(4); // 4階層指定
		Boolean hasError = false;
		try {
			svc.appendChild('B', 'A');
			svc.appendChild('C', 'B');
			svc.appendChild('D', 'C');
			svc.appendChild('E', 'D');
		} catch (CircularValidationService.OverLevelException e) {
			hasError = true;
			System.assert(true, 'expected OverLevelException');
			System.assertEquals('A', e.rootTargetId);
			System.assertEquals(4, e.levelLimit);
		} catch (CircularValidationService.CircularException e) {
			System.assert(false, 'unexpected CircularException');
		}
		System.assert(hasError);
	}
	/** 首尾回帰になった階層が登録できないテスト */
	@isTest static void testErrorSigleCircular() {
		// E -> D -> C -> B -> A (->) E
		CircularValidationService svc = new CircularValidationService();

		Boolean hasError = false;
		try {
			svc.appendChild('A', 'E');
			svc.appendChild('E', 'D');
			svc.appendChild('D', 'C');
			svc.appendChild('C', 'B');
			svc.appendChild('B', 'A');
		} catch (CircularValidationService.OverLevelException e) {
			System.assert(false, 'unexpected OverLevelException');
		} catch (CircularValidationService.CircularException e) {
			hasError = true;
			System.assert(true, 'expected CircularException');
			System.assertEquals('B', e.circularTargetId);
		}
		System.assert(hasError);
	}
	/** 途中から回帰になった階層が登録できないテスト */
	@isTest static void testErrorMiddleCircular() {
		// E -> D -> C -> B -> A
		//                 C (->) E
		CircularValidationService svc = new CircularValidationService();

		Boolean hasError = false;
		try {
			svc.appendChild('C', 'E');
			svc.appendChild('E', 'D');
			svc.appendChild('D', 'C'); // ここでエラー
			svc.appendChild('C', 'B');
			svc.appendChild('B', 'A');
		} catch (CircularValidationService.OverLevelException e) {
			System.assert(false, 'unexpected OverLevelException');
		} catch (CircularValidationService.CircularException e) {
			hasError = true;
			System.assert(true, 'expected CircularException');
			System.assertEquals('D', e.circularTargetId);
		}
		System.assert(hasError);
	}
	/** 2ルート項目で片方回帰になった階層が登録できないテスト */
	@isTest static void testErrorMultiCircular() {
		// E (->) D -> C -> B1 -> A1 -> E
		//                   C -> B2 -> A2
		CircularValidationService svc = new CircularValidationService();
		Boolean hasError = false;
		try {
			svc.appendChild('E', 'D');
			svc.appendChild('D', 'C');
			svc.appendChild('C', 'B1');
			svc.appendChild('C', 'B2');
			svc.appendChild('B1', 'A1');
			svc.appendChild('B2', 'A2');
			svc.appendChild('A1', 'E');
		} catch (CircularValidationService.CircularException e) {
			hasError = true;
			System.assert(true, 'expected CircularException');
			System.assertEquals('A1', e.circularTargetId);
		} catch (CircularValidationService.OverLevelException e) {
			System.assert(false, 'unexpected OverLevelException');
			hasError = false;
		}
		System.assert(hasError);
	}
	/** 最大階層数より多い階層数でチェックできないテスト */
	@isTest static void testInitOverMaxLevel() {
		Boolean hasError = false;
		try {
			CircularValidationService svc = new CircularValidationService(11);
		} catch (App.ParameterException e) {
			hasError = true;
			System.assert(true, 'expected exception');
		}
		System.assert(hasError);
	}
}