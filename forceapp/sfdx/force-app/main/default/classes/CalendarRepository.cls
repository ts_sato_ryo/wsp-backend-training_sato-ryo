/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * カレンダーのリポジトリ
 */
public with sharing class CalendarRepository extends Repository.BaseRepository {

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無（デフォルトはoff） */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** カレンダーのキャッシュ */
	private static final Repository.Cache CALENDAR_CACHE = new Repository.Cache();
	/** カレンダー明細のキャッシュ */
	private static final Repository.Cache RECORD_CACHE = new Repository.Cache();

	/** リレーション名（会社） */
	private static final String COMPANY_R =
			ComCalendar__c.CompanyId__c.getDescribe().getRelationshipName();
	/** 子リレーション名（勤怠明細）*/
	private static final String CHILD_RELATION_RECORDS = 'CalendarRecords__r';
	/** 取得対象の項目(カレンダー) */
	private static final Set<Schema.SObjectField> GET_CALENDAR_FIELD_SET;
	static {
		GET_CALENDAR_FIELD_SET = new Set<Schema.SObjectField> {
			ComCalendar__c.Id,
			ComCalendar__c.Name,
			ComCalendar__c.Code__c,
			ComCalendar__c.UniqKey__c,
			ComCalendar__c.CompanyId__c,
			ComCalendar__c.Name_L0__c,
			ComCalendar__c.Name_L1__c,
			ComCalendar__c.Name_L2__c,
			ComCalendar__c.Type__c,
			ComCalendar__c.Remarks__c,
			ComCalendar__c.Removed__c
		};
	}
	/** 取得対象の項目(カレンダー明細) */
	private static final Set<Schema.SObjectField> GET_RECORD_FIELD_SET;
	static {
		GET_RECORD_FIELD_SET = new Set<Schema.SObjectField> {
			ComCalendarRecord__c.Id,
			ComCalendarRecord__c.Name,
			ComCalendarRecord__c.CalendarId__c,
			ComCalendarRecord__c.Name_L0__c,
			ComCalendarRecord__c.Name_L1__c,
			ComCalendarRecord__c.Name_L2__c,
			ComCalendarRecord__c.Date__c,
			ComCalendarRecord__c.AttDayType__c,
			ComCalendarRecord__c.Remarks__c,
			ComCalendarRecord__c.Removed__c
		};
	}

	/**
	 * コンストラクタ
	 * キャッシュを利用しないインスタンスを生成する
	 */
	public CalendarRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public CalendarRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	public List<CalendarEntity> getEntityListByCompanyId(Id companyId) {
		CalendarFilter filter = new CalendarFilter();
		filter.companyId = companyId;
		return searchEntity(filter, true, false);
	}

	/**
	 * 指定したカレンダーIDのカレンダーを取得する
	 * @param calendarId 取得対象のカレンダーID
	 * @return カレンダー
	 */
	public CalendarEntity getEntityById(Id calendarId) {
		CalendarFilter filter = new CalendarFilter();
		filter.calendarIds = new List<Id>{calendarId};
		List<CalendarEntity> retList = searchEntity(filter, true, false);
		return retList.isEmpty() ? null : retList[0];
	}

	public CalendarEntity getEntityByCode(String code, Id companyId) {
		List<CalendarEntity> retList = getEntityListByCode(new List<String>{code}, companyId);
		return retList.isEmpty() ? null : retList[0];
	}
	public List<CalendarEntity> getEntityListByCode(List<String> codes, Id companyId) {
		CalendarFilter filter = new CalendarFilter();
		filter.codes = codes;
		filter.companyId = companyId;
		return searchEntity(filter, true, false);
	}
	public List<CalendarEntity> getEntityListByCode(List<String> codes) {
		CalendarFilter filter = new CalendarFilter();
		filter.codes = codes;
		return searchEntity(filter, true, false);
	}
	public List<CalendarEntity> getEntityList(Id calendarId, Id companyId, List<CalendarType> types, Boolean withRecords) {
		CalendarFilter filter = new CalendarFilter();
		if (calendarId != null) {
			filter.calendarIds = new List<Id>{calendarId};
		}
		filter.companyId = companyId;
		filter.types = types;
		return searchEntity(filter, withRecords, false);
	}

	/**
	 * 指定したカレンダーIDのカレンダーを取得する
	 * @param calendarId 取得対象のカレンダーID
	 * @return カレンダー
	 */
	public List<CalendarEntity> getEntityListById(List<Id> calendarIds) {
		Boolean includeRemoved = false; // 論理削除データは取得対象外
		Boolean withRecords = true;
		return getEntityListById(calendarIds, includeRemoved, withRecords);
	}

	/**
	 * 指定したカレンダーIDのカレンダーを取得する
	 * @param calendarId 取得対象のカレンダーID
	 * @param includeRemoved 論理削除されているカレンダーを取得対象にする場合はtrue
	 * @param withRecords 明細も一緒に取得する場合はtrue
	 * @return カレンダー
	 */
	public List<CalendarEntity> getEntityListById(List<Id> calendarIds, Boolean includeRemoved,  Boolean withRecords) {
		CalendarFilter filter = new CalendarFilter();
		filter.calendarIds = calendarIds;
		filter.includeRemoved = includeRemoved;
		return searchEntity(filter, withRecords, false);
	}

	/** 検索フィルタ */
	@testVisible
	private class CalendarFilter {
		/** 会社ID */
		public Id companyId;
		/** カレンダーID（複数） */
		public List<Id> calendarIds;
		/** カレンダーコード（複数） */
		public List<String> codes;
		/** タイプ（複数） */
		public List<CalendarType> types;
		/** 論理削除されているカレンダーとカレンダー明細を取得対象にする場合はtrue */
		public Boolean includeRemoved = false;
	}
	/**
	 * カレンダーを検索する
	 * @param filter 検索条件
	 * @param withRecords 明細も一緒に取得する場合はtrue
	 * @param forUpdate 更新用で取得する場合はtrue
	 * @return
	 */
	@testVisible
	private List<CalendarEntity> searchEntity(CalendarFilter filter,
			Boolean withRecords, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter, withRecords);
		if (isEnabledCache && !forUpdate && CALENDAR_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchEntity ReturnCache key:' + cacheKey);
			List<CalendarEntity> entities = new List<CalendarEntity>();
			for (ComCalendar__c sObj : (List<ComCalendar__c>)CALENDAR_CACHE.get(cacheKey)) {
				entities.add(createCalendarEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchEntity NoCache key:' + cacheKey);

		// SOQLを作成する
		// Calendarの項目
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_CALENDAR_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// 関連項目（会社）
		selectFldList.add(getFieldName(COMPANY_R, ComCompany__c.Code__c));
		// カレンダー明細の項目
		if (withRecords == true) {
			List<String> recordFldList = new List<String>();
			for (Schema.SObjectField fld : GET_RECORD_FIELD_SET) {
				recordFldList.add(getFieldName(fld));
			}
			String subWhere = '';
			if (filter.includeRemoved != true) {
				subWhere = ' WHERE ' + getFieldName(ComCalendarRecord__c.Removed__c) + ' = false';
			}
			selectFldList.add(
					'(SELECT ' + String.join(recordFldList, ',')
					+ ' FROM ' + CHILD_RELATION_RECORDS
					+ subWhere
					+ ' ORDER BY ' + getFieldName(ComCalendarRecord__c.Date__c) + ' Asc)');
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id companyId  = filter.companyId;
		final List<Id> calendarIds  = filter.calendarIds;
		final List<String> codes  = filter.codes;
		final List<CalendarType> types  = filter.types;
		final Boolean includeRemoved = filter.includeRemoved;
		if (companyId != null) {
			condList.add(getFieldName(ComCalendar__c.CompanyId__c) + ' = :companyId');
		}
		if (calendarIds != null) {
			condList.add(getFieldName(ComCalendar__c.Id) + ' IN :calendarIds');
		}
		if (codes != null) {
			condList.add(getFieldName(ComCalendar__c.Code__c) + ' IN :codes');
		}
		if (types != null && !types.isEmpty()) {
			List<String> strTypes = new List<String>();
			for (CalendarType type : types) {
				strTypes.add(String.valueOf(type));
			}
			condList.add(getFieldName(ComCalendar__c.Type__c) + ' = :strTypes');
		}
		// 論理削除されているレコードを検索対象外にする
		if (includeRemoved != true) {
			condList.add(getFieldName(ComCalendar__c.Removed__c) + ' = false');
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComCalendar__c.SObjectType.getDescribe().getName()
				+ buildWhereString(condList);
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(ComCalendar__c.Code__c);
		}
		// クエリ実行
		System.debug('ComCalendarRepository: SOQL==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComCalendar__c> calendarList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComCalendar__c.SObjectType);

		// 結果からエンティティを作成する
		List<CalendarEntity> entityList = new List<CalendarEntity>();
		for (ComCalendar__c calendar : calendarList) {
			entityList.add(createCalendarEntity(calendar));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			CALENDAR_CACHE.put(cacheKey, calendarList);
		}

		return entityList;
	}

	// カレンダー明細を検索する
	public List<CalendarRecordEntity> searchRecordList(Set<Id> calendarIds, AppDate startDate, AppDate endDate) {
		CalendarRecordFilter filter = new CalendarRecordFilter();
		filter.calendarIds = calendarIds;
		filter.startDate = startDate;
		filter.endDate = endDate;
		return searchRecordEntity(filter, false);
	}
	public List<CalendarRecordEntity> searchRecordList(Id recordId, Id calendarId) {
		CalendarRecordFilter filter = new CalendarRecordFilter();
		filter.calendarIds = new Set<Id>{calendarId};
		if (recordId != null) {
			filter.recordIds = new Set<Id>{recordId};
		}
		return searchRecordEntity(filter, false);
	}

	/**
	 * 指定したカレンダー明細IDのカレンダー明細を取得する
	 * @param recordId 取得対象のカレンダー明細ID
	 * @return カレンダー明細
	 */
	public CalendarRecordEntity getRecordEntityById(Id recordId) {
		CalendarRecordFilter filter = new CalendarRecordFilter();
		final Boolean includeRemoved = false; // 論理削除データは取得対象外
		return getRecordEntityById(recordId, includeRemoved);
	}

	/**
	 * 指定したカレンダー明細IDのカレンダー明細を取得する
	 * @param recordId 取得対象のカレンダー明細ID
	 * @param includeRemoved 論理削除されているカレンダーを取得対象にする場合はtrue
	 * @return カレンダー明細
	 */
	public CalendarRecordEntity getRecordEntityById(Id recordId, Boolean includeRemoved) {
		CalendarRecordFilter filter = new CalendarRecordFilter();
		filter.recordIds = new Set<Id>{recordId};
		filter.includeRemoved = includeRemoved;
		List<CalendarRecordEntity> retList = searchRecordEntity(filter, false);
		return retList.isEmpty() ? null : retList[0];
	}

	/**
	 * 指定したカレンダー明細IDのカレンダー明細を取得する
	 * @param recordId 取得対象のカレンダー明細ID
	 * @return カレンダー明細リスト
	 */
	public List<CalendarRecordEntity> getRecordEntityListById(List<Id> recordIds) {
		CalendarRecordFilter filter = new CalendarRecordFilter();
		filter.recordIds = new Set<Id>(recordIds);
		return searchRecordEntity(filter, false);
	}

	/** 検索フィルタ */
	@testVisible
	private class CalendarRecordFilter {
		/** カレンダーID */
		public Set<Id> calendarIds;
		/** カレンダー明細ID */
		public Set<Id> recordIds;
		/** 取得対象範囲(開始) */
		public AppDate startDate;
		/** 取得対象範囲(終了) */
		public AppDate endDate;
		/** 論理削除されているカレンダー明細を取得対象にする場合はtrue */
		public Boolean includeRemoved = false;
	}
	/**
	 * カレンダー明細を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue
	 * @return
	 */
	@testVisible
	private List<CalendarRecordEntity> searchRecordEntity(CalendarRecordFilter filter,
			Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && RECORD_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchRecordEntity ReturnCache key:' + cacheKey);
			List<CalendarRecordEntity> entities = new List<CalendarRecordEntity>();
			for (ComCalendarRecord__c sObj : (List<ComCalendarRecord__c>)RECORD_CACHE.get(cacheKey)) {
				entities.add(createRecordEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchRecordEntity NoCache key:' + cacheKey);

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_RECORD_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Set<Id> calendarIds  = filter.calendarIds;
		final Set<Id> recordIds  = filter.recordIds;
		final Date startDate = AppDate.convertDate(filter.startDate);
		final Date endDate = AppDate.convertDate(filter.endDate);
		final Boolean includeRemoved = filter.includeRemoved;
		if (filter.calendarIds != null) {
			condList.add(getFieldName(ComCalendarRecord__c.CalendarId__c) + ' IN :calendarIds');
		}
		if (filter.recordIds != null) {
			condList.add(getFieldName(ComCalendarRecord__c.Id) + ' IN :recordIds');
		}
		if (filter.startDate != null) {
			condList.add(getFieldName(ComCalendarRecord__c.Date__c) + ' >= :startDate');
		}
		if (filter.endDate != null) {
			condList.add(getFieldName(ComCalendarRecord__c.Date__c) + ' <= :endDate');
		}
		// 論理削除されているレコードを検索対象外にする
		if (includeRemoved != true) {
			condList.add(getFieldName(ComCalendarRecord__c.Removed__c) + ' = false');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComCalendarRecord__c.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond;
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(ComCalendarRecord__c.Date__c);
		}
		// クエリ実行
		System.debug('ComCalendarRepository: SOQL==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComCalendarRecord__c> recordList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComCalendarRecord__c.SObjectType);

		// 結果からエンティティを作成する
		List<CalendarRecordEntity> entityList = new List<CalendarRecordEntity>();
		for (ComCalendarRecord__c record : recordList) {
			entityList.add(createRecordEntity(record));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			RECORD_CACHE.put(cacheKey, recordList);
		}
		return entityList;
	}

	/**
	 * カレンダーエンティティを保存する
	 * @param entity 保存対象のカレンダーエンティティ
	 * @param withRecords カレンダー明細も一緒に保存する場合はtrue
	 * @return 保存結果
	 */
	public SaveCalendarResult saveCalendarEntity(CalendarEntity entity, Boolean withRecords) {
		return saveCalendarEntityList(new List<CalendarEntity>{entity}, withRecords, true);
	}
	/**
	 * カレンダーエンティティを保存する
	 * @param entityList 保存対象のカレンダーエンティティのリスト
	 * @param withRecords カレンダー明細も一緒に保存する場合はtrue
	 * @return 保存結果
	 */
	public SaveCalendarResult saveCalendarEntityList(List<CalendarEntity> entityList, Boolean withRecords) {
		return saveCalendarEntityList(entityList, withRecords, true);
	}

	/** カレンダーの保存結果(カレンダー明細保存結果も含む) */
	public class SaveCalendarResult extends Repository.SaveResult {
		/** カレンダー明細の処理結果の詳細 */
		public List<ResultDetail> recordDetails {get; set;}
	}
	/**
	 * カレンダーエンティティを保存する
	 * @param entityList 保存対象のカレンダーエンティティのリスト
	 * @param withRecords カレンダー明細も一緒に保存する場合はtrue
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(カレンダーとカレンダー明細の結果)
	 */
	public SaveCalendarResult saveCalendarEntityList(List<CalendarEntity> entityList, Boolean withRecords, Boolean allOrNone) {
		List<ComCalendar__c> calObjList = new List<ComCalendar__c>();

		// カレンダー保存用のオブジェクトを作成する
		for (CalendarEntity entity : entityList) {
			// エンティティからSObjectを作成する
			ComCalendar__c calObj = createCalendarObj(entity);
			calObjList.add(calObj);
		}
		// DBに保存する
		List<Database.UpsertResult> calResList = AppDatabase.doUpsert(calObjList, allOrNone);

		// カレンダー明細保存用のオブジェクトを作成する
		List<ComCalendarRecord__c> recObjList = new List<ComCalendarRecord__c>();
		List<Database.UpsertResult> recResList;
		if (withRecords) {
			for (Integer i = 0; i < calResList.size(); i++) {
				Database.UpsertResult res = calResList[i];
				CalendarEntity calEntity = entityList[i];

				// カレンダーの保存に失敗している場合は明細も保存しない次のレコードへ
				if (!res.isSuccess()) {
					continue;
				}

				// カレンダー明細リストがnull、または空の場合は保存対象外
				if ((calEntity.recordList == null) || (calEntity.recordList.isEmpty())) {
					continue;
				}

				// カレンダー明細オブジェクトをエンティティから作成する
				for (CalendarRecordEntity recEntity : calEntity.recordList) {
					ComCalendarRecord__c recObj = createRecordObj(recEntity);
					recObj.CalendarId__c = res.getId();
					recObjList.add(recObj);
				}
			}
			// カレンダー明細オブジェクトを保存する
			recResList = AppDatabase.doUpsert(recObjList, allOrNone);
		}

		// 結果作成
	 	Repository.SaveResult repoCalRes = resultFactory.createSaveResult(calResList);
		SaveCalendarResult repoRes = new SaveCalendarResult();
		repoRes.details = repoCalRes.details;
		if (withRecords) {
		 	Repository.SaveResult repoRecRes = resultFactory.createSaveResult(recResList);
			repoRes.isSuccessAll = repoCalRes.isSuccessAll && repoRecRes.isSuccessAll;
			repoRes.recordDetails = repoRecRes.details;
		} else {
			repoRes.isSuccessAll = repoCalRes.isSuccessAll;
		}

		return repoRes;
	}

	/**
	 * カレンダー明細エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveRecordEntity(CalendarRecordEntity entity) {
		return saveRecordEntityList(new List<CalendarRecordEntity>{entity}, true);
	}
	/**
	 * カレンダー明細エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果のリスト
	 */
	public SaveResult saveRecordEntityList(List<CalendarRecordEntity> entityList) {
		return saveRecordEntityList(entityList, true);
	}

	/**
	 * カレンダー明細エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveRecordEntityList(List<CalendarRecordEntity> entityList, Boolean allOrNone) {

		// カレンダー明細オブジェクトをエンティティから作成する
		List<ComCalendarRecord__c> recObjList = new List<ComCalendarRecord__c>();
		for (CalendarRecordEntity entity : entityList) {
			recObjList.add(createRecordObj(entity));
		}

		// カレンダー明細オブジェクトを保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(recObjList, allOrNone);

		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * ComCalendar__cオブジェクトからCalendarEntityを作成する
	 * @param calendarObj 作成元のComCalendar__cオブジェクト
	 * @return 作成したCalendarEntity
	 */
	private CalendarEntity createCalendarEntity(ComCalendar__c calendarObj) {

		CalendarEntity entity = new CalendarEntity();
		entity.setId(calendarObj.Id);
		entity.code = calendarObj.Code__c;
		entity.uniqKey = calendarObj.UniqKey__c;
		entity.name = calendarObj.Name;
		entity.nameL = new AppMultiString(calendarObj.Name_L0__c, calendarObj.Name_L1__c, calendarObj.Name_L2__c);
		entity.companyId = calendarObj.CompanyId__c;
		entity.companyCode = calendarObj.CompanyId__r.Code__c;
		entity.calType = CalendarType.valueOf(calendarObj.Type__c);
		entity.remarks = calendarObj.Remarks__c;
		entity.isRemoved = calendarObj.Removed__c;
		// 明細
		List<ComCalendarRecord__c> recordList = calendarObj.CalendarRecords__r;
		if (recordList != null) {
			for (ComCalendarRecord__c recordObj : recordList) {
				entity.appendRecord(createRecordEntity(recordObj));
			}
		}
		entity.resetChanged();
		return entity;
	}
	/**
	 * ComCalendarRecord__cオブジェクトからCalendarRecordEntityを作成する
	 * @param recordObj 作成元のComCalendarRecord__cオブジェクト
	 * @return 作成したCalendarRecordEntity
	 */
	private CalendarRecordEntity createRecordEntity(ComCalendarRecord__c recordObj) {
		CalendarRecordEntity entity = new CalendarRecordEntity();
		entity.setId(recordObj.Id);
		entity.calendarId = recordObj.CalendarId__c;
		entity.name = recordObj.Name;
		entity.nameL = new AppMultiString(recordObj.Name_L0__c, recordObj.Name_L1__c, recordObj.Name_L2__c);
		entity.calDate = AppDate.valueOf(recordObj.Date__c);
		entity.dayType = AttDayType.getType(recordObj.AttDayType__c);
		entity.remarks = recordObj.Remarks__c;
		entity.isRemoved = recordObj.Removed__c;

		entity.resetChanged();
		return entity;
	}

	/**
	 * カレンダーエンティティからComCalendar__cを作成する
	 * @param entity 作成元のカレンダーエンティティ
	 * @return エンティティから作成したSObject
	 */
	private ComCalendar__c createCalendarObj(CalendarEntity entity) {
		ComCalendar__c retObj = new ComCalendar__c();

		retObj.Id = entity.id;
		if (entity.isChanged(CalendarEntity.Field.Name)) {
			retObj.Name = entity.name;
		}
		if (entity.isChanged(CalendarEntity.Field.Name_L)) {
			retObj.Name_L0__c = entity.nameL.valueL0;
			retObj.Name_L1__c = entity.nameL.valueL1;
			retObj.Name_L2__c = entity.nameL.valueL2;
		}
		if (entity.isChanged(CalendarEntity.Field.CODE)) {
			retObj.Code__c = entity.code;
		}
		if (entity.isChanged(CalendarEntity.Field.UNIQ_KEY)) {
			retObj.UniqKey__c = entity.uniqKey;
		}
		if (entity.isChanged(CalendarEntity.Field.CAL_TYPE)) {
			retObj.Type__c = String.valueOf(entity.calType);
		}
		// 会社IDは変更不可のため、新規作成時のみセットする → 2018年7月時点では
		if (entity.isChanged(CalendarEntity.Field.COMPANY_ID)) {
			retObj.CompanyId__c = entity.companyId;
		}
		if (entity.isChanged(CalendarEntity.Field.REMARKS)) {
			retObj.Remarks__c = entity.remarks;
		}
		if (entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED)) {
			retObj.Removed__c = entity.isRemoved;
		}

		return retObj;
	}
	/**
	 * カレンダー明細エンティティからComCalendarRecord__cを作成する
	 * @param entity 作成元のカレンダー明細エンティティ
	 * @return エンティティから作成したSObject
	 */
	private ComCalendarRecord__c createRecordObj(CalendarRecordEntity entity) {
		Boolean isInsert = String.isBlank(entity.id);
		ComCalendarRecord__c retObj = new ComCalendarRecord__c();

		retObj.Id = entity.id;
		if (entity.isChanged(CalendarRecordEntity.Field.Name)) {
			retObj.Name = entity.name;
		}
		if (entity.isChanged(CalendarRecordEntity.Field.Name_L)) {
			retObj.Name_L0__c = entity.nameL.valueL0;
			retObj.Name_L1__c = entity.nameL.valueL1;
			retObj.Name_L2__c = entity.nameL.valueL2;
		}
		if (entity.isChanged(CalendarRecordEntity.Field.CAL_DATE)) {
			retObj.Date__c = AppDate.convertDate(entity.calDate);
		}
		if (entity.isChanged(CalendarRecordEntity.Field.CALENDAR_ID)) {
			retObj.CalendarId__c = entity.calendarId;
		}
		if (entity.isChanged(CalendarRecordEntity.Field.DAY_TYPE)) {
			retObj.AttDayType__c =  String.valueOf(entity.dayType);
		}
		if (entity.isChanged(CalendarRecordEntity.Field.REMARKS)) {
			retObj.Remarks__c = entity.remarks;
		}
		if (entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED)) {
			retObj.Removed__c = entity.isRemoved;
		}

		return retObj;
	}
}