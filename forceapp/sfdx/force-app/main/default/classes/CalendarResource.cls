/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description カレンダーレコード操作APIを実装するクラス
 */
public with sharing class CalendarResource {

	/**
	 * @desctiprion カレンダーレコードを表すクラス
	 */
	public class Calendar implements RemoteApi.RequestParam {
		/** カレンダーレコードID */
		public String id;
		/** カレンダーコード */
		public String code;
		/** カレンダー名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** カレンダー名(言語0) */
		public String name_L0;
		/** カレンダー名(言語1) */
		public String name_L1;
		/** カレンダー名(言語2) */
		public String name_L2;
		/** 会社レコードID */
		public String companyId;
		/** タイプ */
		public String type;
		/** 全社デフォルトカレンダーか否か */
		public Boolean isDefault;
		/** 備考 */
		public String remarks;

		/**
		 * パラメータを検証する
		 * @param isUpdate レコード更新時はtrue、作成時はfalse
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate(Boolean isUpdate) {
			// カレンダーレコードID
			if (isUpdate && String.isBlank(id)) {
				throw new App.ParameterException('id', id);
			}
			if (String.isNotBlank(id)) {
				try {
					Id val = (Id)id;
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}
			// カレンダーコード
			if (String.isBlank(code)) {
				throw new App.ParameterException('code', code);
			}
			// カレンダー名(L0)
			if (String.isBlank(name_L0)) {
				throw new App.ParameterException('name_L0', name_L0);
			}
			// 会社ID
			if (!isUpdate && String.isBlank(companyId)) {
				throw new App.ParameterException('companyId', companyId);
			}
			if (String.isNotBlank(companyId)) {
				try {
					Id val = (Id)companyId;
				} catch (Exception e) {
					throw new App.ParameterException('companyId', id);
				}
			}
			// カレンダータイプ
			if (!isUpdate && String.isBlank(type)) {
				throw new App.ParameterException('type', type);
			}
		}
	}

	/**
	 * @desctiprion カレンダーレコード更新APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_CALENDAR;

		/**
		 * @description カレンダーレコードを1件更新する
		 * @param req リクエストパラメータ(Calendar)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			final Boolean isUpdate = false;

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			Calendar param = (Calendar)req.getParam(Calendar.class);

			// パラメータ値の検証
			param.validate(isUpdate);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// カレンダーレコードを登録
			CalendarRepository.SaveCalendarResult result = CalendarResource.saveCalendar(param, req.getParamMap(), isUpdate);

			// レスポンスを作成
			SaveResult res = new SaveResult();
			res.Id = result.details[0].id;

			return res;
		}
	}

	/**
	 * @desctiprion カレンダー更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_CALENDAR;

		/**
		 * @description カレンダーレコードを1件更新する
		 * @param req リクエストパラメータ(Calendar)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			Calendar param = (Calendar)req.getParam(Calendar.class);

			// パラメータ値の検証
			param.validate(true);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// カレンダーレコードを更新
			CalendarResource.saveCalendar(param, req.getParamMap(), true);

			return null;
		}
	}

	/**
	 * @description カレンダーレコードを1件作成or更新する<br/>
	 *   ※このメソッドは他のクラスから参照しないでください<br/>
	 *   TODO: DAOに外出しする
	 * @param param カレンダーレコード値を含むパラメータ
	 * @param isUpdate レコード更新時はtrue、作成時はfalse
	 * @return 作成or更新したカレンダーレコード
	 */
	private static CalendarRepository.SaveCalendarResult saveCalendar(Calendar param, Map<String, Object> paramMap, Boolean isUpdate) {
		CalendarEntity calendar = new CalendarEntity();

		// レコード更新時はIDをセット
		if (isUpdate) {
			calendar.setId(param.id);
		}
		if (paramMap.containsKey('code')) {
			calendar.code = param.code;
		}
		String name_L0;
		String name_L1;
		String name_L2;
		if (paramMap.containsKey('name_L0')) {
			calendar.name = param.name_L0;
			name_L0 = param.name_L0;
		}
		if (paramMap.containsKey('name_L1')) {
			name_L1 = param.name_L1;
		}
		if (paramMap.containsKey('name_L2')) {
			name_L2 = param.name_L2;
		}
		calendar.nameL = new AppMultiString(name_L0, name_L1, name_L2);
		if (paramMap.containsKey('remarks')) {
			calendar.remarks = param.remarks;
		}
		// 以下、レコード作成時のみ値をセット
		if (!isUpdate && paramMap.containsKey('companyId')) {
			// 会社IDは主従関係の親かつ変更不可
			calendar.companyId = param.companyId;
		}
		if (!isUpdate && paramMap.containsKey('type')) {
			calendar.calType = CalendarType.valueOf(param.type);
		}

		return (new CalendarService()).saveCalendar(calendar);
	}


	/**
	 * @description カレンダーレコード検索条件を格納するクラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** カレンダーレコードID */
		public String id;
		/** 会社レコードID */
		public String companyId;
		/** タイプのリスト */
		public List<String> types;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// カレンダーレコードID
			if (String.isNotBlank(id)) {
				try {
					Id val = (Id)id;
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}
			// 会社レコードID
			if (String.isNotBlank(companyId)) {
				try {
					Id val = (Id)companyId;
				} catch (Exception e) {
					throw new App.ParameterException('companyId', companyId);
				}
			}
			// タイプ
			if (types != null && !types.isEmpty()) {
				for (String type : types) {
					if (CalendarType.valueOf(type) == null) {
						throw new App.ParameterException('types', types);
					}
				}
			}
		}
	}

	/**
	 * @description カレンダーレコード検索結果を格納するクラス
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public List<Calendar> records;
	}

	/**
	 * @description カレンダーレコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * カレンダーレコードを検索する
		 * @param req リクエストパラメータ(SearchRequest)
		 * @return レスポンス(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchRequest param = (SearchRequest)req.getParam(SearchRequest.class);

			// パラメータ値の検証
			param.validate();

			// 会社データを取得
			CompanyEntity company = new CompanyRepository().getEntity(param.companyId);
			if (company == null) {
				// メッセージ：指定された会社が見つかりません。
				throw new App.IllegalStateException(
						App.ERR_CODE_RECORD_NOT_FOUND,
						ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Company}));
			}

			// パラメータ（カレンダータイプ）を変換
			List<CalendarType> types = null;
			if (param.types != null && !param.types.isEmpty()) {
				types = new List<CalendarType>();
				for (String type : param.types) {
					types.add(CalendarType.valueOf(type));
				}
			}

			// カレンダーデータを取得する
			List<CalendarEntity> entityList = new CalendarService().getCalendarList(param.Id, param.companyId, types);

			// レスポンスクラスに変換
			List<Calendar> calendarList = new List<Calendar>();
			for (CalendarEntity entity : entityList) {
				Calendar calendar = new Calendar();
				calendar.id = entity.id;
				calendar.code = entity.code;
				calendar.name = entity.nameL.getValue();
				calendar.name_L0 = entity.nameL.valueL0;
				calendar.name_L1 = entity.nameL.valueL1;
				calendar.name_L2 = entity.nameL.valueL2;
				calendar.companyId = entity.companyId;
				calendar.type = String.valueOf(entity.calType);
				calendar.isDefault = entity.id == company.calendarId ? true : false;
				calendar.remarks = entity.remarks;

				calendarList.add(calendar);
			}

			// 成功レスポンスをセットする
			SearchResponse res = new SearchResponse();
			res.records = calendarList;

			return res;
		}
	}


	/**
	 * @desctiprion カレンダー明細レコードを表すクラス
	 */
	public class CalendarRecord implements RemoteApi.RequestParam {
		/** カレンダー明細レコードID */
		public String id;
		/** カレンダー明細名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** カレンダー明細名(言語0) */
		public String name_L0;
		/** カレンダー明細名(言語1) */
		public String name_L1;
		/** カレンダー明細名(言語2) */
		public String name_L2;
		/** 日付 */
		public String recordDate;
		/** カレンダーレコードID */
		public String calendarId;
		/** 日タイプ */
		public String dayType;
		/** 備考 */
		public String remarks;

		/**
		 * パラメータを検証する
		 * @param isUpdate レコード更新時はtrue、作成時はfalse
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate(Boolean isUpdate) {
			// カレンダー明細レコードID
			if (isUpdate) {
				if (String.isBlank(id)) {
					throw new App.ParameterException('id', id);
				} else {
					try {
						Id val = (Id)id;
					} catch (Exception e) {
						throw new App.ParameterException('id', id);
					}
				}
			}
			// カレンダー明細名(L0)
			if (String.isBlank(name_L0)) {
				throw new App.ParameterException('name_L0', name_L0);
			}
			// 日付
			if (String.isBlank(recordDate)) {
				throw new App.ParameterException('recordDate', recordDate);
			} else {
				// フォーマットチェック
				try {
					AppDate.parse(recordDate);
				} catch (Exception e) {
					throw new App.ParameterException('recordDate', recordDate);
				}
			}
			// カレンダーレコードID
			if (!isUpdate) {
				if (String.isBlank(calendarId)) {
					throw new App.ParameterException('calendarId', calendarId);
				} else {
					try {
						Id val = (Id)calendarId;
					} catch (Exception e) {
						throw new App.ParameterException('calendarId', calendarId);
					}
				}
			}
		}
	}

	/**
	 * @description カレンダーレコード削除条件を格納するクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** カレンダーレコードIDのリスト */
		public String id;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// カレンダーレコードID
			if (String.isBlank(id)) {
				throw new App.ParameterException('id', id);
			}
			try {
				Id val = (Id)id;
			} catch (Exception e) {
				throw new App.ParameterException('id', id);
			}
		}
	}

	/**
	 * @desctiprion カレンダーレコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_CALENDAR;

		/**
		 * @description カレンダー明細レコードを削除する
		 * @param req リクエストパラメータ(DeleteRecordRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);

			// パラメータ値の検証
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// カレンダーレコードを論理削除
			(new CalendarService()).logicalDeleteCalendar(param.id);

			return null;
		}
	}

	/**
	 * @description カレンダー、カレンダー明細レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したカレンダー、カレンダー明細レコードID */
		public String id;
	}

	/**
	 * @desctiprion カレンダー明細レコード登録APIを実装するクラス
	 */
	public with sharing class CreateRecordApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_CALENDAR;

		/**
		 * @description カレンダー明細レコードを1件登録する
		 * @param req リクエストパラメータ(CalendarRecord)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			CalendarRecord param = (CalendarRecord)req.getParam(CalendarRecord.class);

			// パラメータ値の検証
			param.validate(false);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// カレンダー明細レコードを作成
			Repository.SaveResult result = CalendarResource.saveCalendarRecord(param, req.getParamMap(), false);

			// レスポンスを作成
			SaveResult res = new SaveResult();
			res.Id = result.details[0].Id;

			return res;
		}
	}

	/**
	 * @desctiprion カレンダー明細レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateRecordApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_CALENDAR;

		/**
		 * @description カレンダー明細レコードを1件更新する
		 * @param req リクエストパラメータ(CalendarRecord)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			CalendarRecord param = (CalendarRecord)req.getParam(CalendarRecord.class);

			// パラメータ値の検証
			param.validate(true);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// カレンダーレコードを更新
			CalendarResource.saveCalendarRecord(param, req.getParamMap(), true);

			return null;
		}
	}

	/**
	 * @description カレンダー明細レコードを1件作成or更新する<br/>
	 *   ※このメソッドは他のクラスから参照しないでください<br/>
	 *   TODO: DAOに外出しする
	 * @param param カレンダー明細レコード値を含むパラメータ
	 * @param isUpdate レコード更新時はtrue、作成時はfalse
	 * @return 作成or更新したカレンダー明細レコード
	 */
	private static Repository.SaveResult saveCalendarRecord(CalendarRecord param, Map<String, Object> paramMap, Boolean isUpdate) {
		CalendarRecordEntity record = new CalendarRecordEntity();

		// レコード更新時はIDをセット
		if (isUpdate) {
			record.setId(param.id);
		}
		String name_L0;
		String name_L1;
		String name_L2;
		if (paramMap.containsKey('name_L0')) {
			record.name = param.name_L0;
			name_L0 = param.name_L0;
		}
		if (paramMap.containsKey('name_L1')) {
			name_L1 = param.name_L1;
		}
		if (paramMap.containsKey('name_L2')) {
			name_L2 = param.name_L2;
		}
		record.nameL = new AppMultiString(name_L0, name_L1, name_L2);
		if (paramMap.containsKey('recordDate')) {
			record.calDate = AppDate.valueOf(param.recordDate);
		}
		if (paramMap.containsKey('dayType')) {
			record.dayType = AttDayType.getType(param.dayType);
		}
		if (paramMap.containsKey('remarks')) {
			record.remarks = param.remarks;
		}
		// 以下、レコード作成時のみ値をセット
		if (!isUpdate && paramMap.containsKey('calendarId')) {
			// カレンダーIDは主従関係の親かつ変更不可
			record.calendarId = param.calendarId;
		}

		CalendarService service = new CalendarService();

		// 更新の場合は対象データを取得
		if (isUpdate) {
			CalendarRecordEntity entity = service.getCalendarRecord(param.id);
			if (entity != null) {
				record.calendarId = entity.calendarId;
			} else {
				// メッセージ：指定されたイベントが見つかりません。
				throw new App.IllegalStateException(
						App.ERR_CODE_RECORD_NOT_FOUND,
						ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Admin_Lbl_Event}));
			}
		}

		// 対象日のカレンダー明細が既に存在する場合は登録不可
		List<CalendarRecordEntity> recordList = service.getCalendarRecordList(
				record.calendarId,
				AppDate.valueOf(param.recordDate),
				AppDate.valueOf(param.recordDate));
		if (recordList != null && recordList.size() > 0) {
			if (!isUpdate || (isUpdate && recordList[0].id != param.id)) {
				// メッセージ：対象日のイベントは既に登録されています。
				throw new App.IllegalStateException(
						App.ERR_CODE_EVENT_EXISTED,
						ComMessage.msg().Admin_Err_EventExisted);
			}
		}

		return service.saveCalendarRecord(record);
	}

	/**
	 * @description カレンダー明細レコード削除条件を格納するクラス
	 */
	public class DeleteRecordRequest implements RemoteApi.RequestParam {
		/** カレンダー明細レコードIDのリスト */
		public List<String> ids;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// カレンダー明細ID
			if (ids == null || ids.isEmpty()) {
				throw new App.ParameterException('ids', ids);
			} else {
				for (String recordId : ids) {
					if (String.isNotBlank(recordId)) {
						try {
							Id.ValueOf(recordId);
						} catch (Exception e) {
							throw new App.ParameterException('id', recordId);
						}
					}
				}
			}
		}
	}

	/**
	 * @desctiprion カレンダー明細レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteRecordApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_CALENDAR;

		/**
		 * @description カレンダー明細レコードを削除する
		 * @param req リクエストパラメータ(DeleteRecordRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			DeleteRecordRequest param = (DeleteRecordRequest)req.getParam(DeleteRecordRequest.class);

			// パラメータ値の検証
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// カレンダー明細レコードを論理削除
			List<Id> recordIdList = new List<Id>();
			for (String recordId : param.ids) {
				recordIdList.add(Id.valueOf(recordId));
			}
			new CalendarService().logicalDeleteCalendarRecords(recordIdList);

			return null;
		}
	}

	/**
	 * @description カレンダー明細レコード検索条件を格納するクラス
	 */
	public class SearchRecordRequest implements RemoteApi.RequestParam {
		/** カレンダー明細レコードID */
		public String id;
		/** カレンダーレコードID */
		public String calendarId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// カレンダー明細レコードID
			if (String.isNotBlank(id)) {
				try {
					Id val = (Id)id;
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}
			// カレンダーレコードID
			if (String.isNotBlank(calendarId)) {
				try {
					Id val = (Id)calendarId;
				} catch (Exception e) {
					throw new App.ParameterException('calendarId', calendarId);
				}
			}
		}
	}

	/**
	 * @description カレンダー明細レコード検索結果を格納するクラス
	 */
	public class SearchRecordResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public List<CalendarRecord> records;
	}

	/**
	 * @description カレンダー明細レコード検索APIを実装するクラス
	 */
	public with sharing class SearchRecordApi extends RemoteApi.ResourceBase {

		/**
		 * カレンダー明細レコードを検索する
		 * @param req リクエストパラメータ(SearchRecordRequest)
		 * @return レスポンス(SearchRecordResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchRecordRequest param = (SearchRecordRequest)req.getParam(SearchRecordRequest.class);

			// パラメータ値の検証
			param.validate();

			// カレンダー明細データを取得する
			List<CalendarRecordEntity> entityList = new CalendarService().getCalendarRecordList(param.id, param.calendarId);

			// レスポンスクラスに変換
			List<CalendarRecord> calendarRecordList = new List<CalendarRecord>();
			for (CalendarRecordEntity entity : entityList) {
				CalendarRecord record = new CalendarRecord();
				record.id = entity.id;
				record.name = entity.nameL.getValue();
				record.name_L0 = entity.nameL.valueL0;
				record.name_L1 = entity.nameL.valueL1;
				record.name_L2 = entity.nameL.valueL2;
				record.recordDate = String.valueOf(AppDate.convertDate(entity.calDate));
				record.calendarId = entity.calendarId;
				record.dayType = String.valueOf(entity.dayType);
				record.remarks = entity.remarks;

				calendarRecordList.add(record);
			}

			// 成功レスポンスをセットする
			SearchRecordResponse res = new SearchRecordResponse();
			res.records = calendarRecordList;

			return res;
		}
	}
}