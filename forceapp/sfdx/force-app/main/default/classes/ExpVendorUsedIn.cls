/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Vendor UsedIn picklist
 */
public with sharing class ExpVendorUsedIn extends ValueObjectType {

	/** Expense Report Only */
	public static final ExpVendorUsedIn EXPENSE_REPORT = new ExpVendorUsedIn('ExpenseReport');
	/** Expense Report and Request */
	public static final ExpVendorUsedIn EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ExpVendorUsedIn('ExpenseRequestAndExpenseReport');

	/** Entries */
	public static final List<ExpVendorUsedIn> TYPE_LIST;
	static {
		TYPE_LIST = new List<ExpVendorUsedIn> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}

	/**
	 * Constructor
	 * @param value
	 */
	private ExpVendorUsedIn(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
	 * @param The key value to get the instance
	 * @return The instance having the value specified
	 */
	public static ExpVendorUsedIn valueOf(String value) {
		ExpVendorUsedIn retType = null;
		if (String.isNotBlank(value)) {
			for (ExpVendorUsedIn type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {

		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ExpVendorUsedIn) {
			if (this.value == ((ExpVendorUsedIn)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}
}