/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠申請サービスのテストクラス
 */
@isTest
private class AttRequestServiceTest {
	/**
	 * 休日の勤怠明細の出勤、退勤時刻をクリアする
	 */
	private static void clearAttTimeForHoliday(List<AttRecordEntity> records) {

		for (AttRecordEntity record : records) {
			if ((record.outDayType == AttDayType.HOLIDAY)
			 	|| (record.outDayType == AttDayType.LEGAL_HOLIDAY)) {
				record.inpStartTime = null;
				record.inpEndTime = null;
			}
		}
	}
	/**
	 * submitFixRequestのテスト
	 * 勤務確定申請が申請できることを確認する
	 */
	@isTest static void submitFixRequestTest() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
		// 勤怠計算を実行
		new AttCalcService().calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = summary.Id;
		param.comment = 'Test Comment';

		// 申請実行
		Test.startTest();
			AttRequestService service = new AttRequestService();
			service.submitFixRequest(param);
		Test.stopTest();

		// 勤怠サマリーを確認する
		AttSummaryEntity afterSummary = summaryRepo.getEntity(summary.Id);
		System.assertNotEquals(null, afterSummary.requestId);
		System.assertEquals(true, afterSummary.isLocked);

		// 勤怠申請を確認する
		AttRequestEntity requestEntity = requestRepo.getEntity(afterSummary.requestId);
		System.assertNotEquals(null, requestEntity);
		System.assertEquals(true, String.isNotBlank(requestEntity.name)); // 申請名は多言語のテストで確認
		System.assertEquals(afterSummary.id, requestEntity.summaryId);
		System.assertEquals(param.comment, requestEntity.comment);
		System.assertEquals(AppRequestStatus.PENDING, requestEntity.status);
		System.assertEquals(afterSummary.employeeId, requestEntity.employeeHistoryId);
		System.assertEquals(afterSummary.deptId, requestEntity.departmentHistoryId);
		System.assertEquals(testData.employee.getHistory(0).id, requestEntity.actorId);
		// 承認者は暫定対応
		System.assertEquals(testEmp2.userId, requestEntity.approver01Id);

		// 勤怠申請に添付ファイルが追加されていることを確認する
		AppAttachmentRepository attachRepo = new AppAttachmentRepository();
		List<AppAttachmentEntity> attaches = attachRepo.getEntityListByParentId(requestEntity.id);
		System.assertEquals(1, attaches.size());
		AppAttachmentEntity attach = attaches[0];
		System.assertEquals(requestEntity.name, attach.name);
		System.assertEquals(requestEntity.id, attach.parentId);
		AttSummaryService.TimeSheet timeSheet =
				(AttSummaryService.TimeSheet)JSON.deserialize(attach.bodyText, AttSummaryService.TimeSheet.class);
		// 添付ファイルの中身は軽く確認（各項目のテストはAttSummaryServiceのテストクラスで）
		System.assertEquals(afterSummary.startDate.format(), timeSheet.startDate);
		System.assertEquals(afterSummary.attRecordList.size(), timeSheet.recordList.size());

	}



	/**
	 * checkFixRequestのテスト
	 * 勤務確定申請時のチェック処理を確認する
	 */
	@isTest static void checkFixRequestTest() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(1);
		EmployeeBaseEntity testEmp = testEmpList[0];
		// 休職休業マスタ
		AttLeaveOfAbsenceEntity absenceData = testData.createLeaveOfAbsence('001');
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp.id, startDate)[0];

		// 申請データ作成
		// 終日休（欠勤カウント対象外）
		AttDailyRequestEntity leaveReqeust1 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_DAY, AppDate.newInstance(2017, 9, 4), AppDate.newInstance(2017, 9, 4), null, null);
		leaveReqeust1.employeeId = testEmp.getHistory(0).id;
		leaveReqeust1.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust1.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes1 = requestRepo.saveEntity(leaveReqeust1);
		leaveReqeust1.setId(requestRes1.details[0].id);
		// 半休（欠勤カウント対象）
		AttDailyRequestEntity leaveReqeust2 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_AM, AppDate.newInstance(2017, 9, 5), AppDate.newInstance(2017, 9, 5), null, null);
		leaveReqeust2.employeeId = testEmp.getHistory(0).id;
		leaveReqeust2.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust2.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes2 = requestRepo.saveEntity(leaveReqeust2);
		leaveReqeust2.setId(requestRes2.details[0].id);
		// 半休＋半休（欠勤カウント対象外）
		AttDailyRequestEntity leaveReqeust3_1 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_AM, AppDate.newInstance(2017, 9, 6), AppDate.newInstance(2017, 9, 6), null, null);
		leaveReqeust3_1.employeeId = testEmp.getHistory(0).id;
		leaveReqeust3_1.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust3_1.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes3_1 = requestRepo.saveEntity(leaveReqeust3_1);
		leaveReqeust3_1.setId(requestRes3_1.details[0].id);
		AttDailyRequestEntity leaveReqeust3_2 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_PM, AppDate.newInstance(2017, 9, 6), AppDate.newInstance(2017, 9, 6), null, null);
		leaveReqeust3_2.employeeId = testEmp.getHistory(0).id;
		leaveReqeust3_2.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust3_2.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes3_2 = requestRepo.saveEntity(leaveReqeust3_2);
		leaveReqeust3_2.setId(requestRes3_2.details[0].id);
		// TODO:時間単位半休（欠勤カウント対象外）
		// TODO:時間単位半休＋時間単位半休（欠勤カウント対象外）
		// TODO:半休＋時間単位半休（欠勤カウント対象）
		// TODO:時間休

		// その他休憩時間をクリア
		List<AttRecordEntity> targetRecords = testData.getRecordEntities(testEmp.id, AppDate.newInstance(2017, 9, 1), AppDate.newInstance(2017, 9, 30));
		for (AttRecordEntity recordData : targetRecords) {
			recordData.inpRestHours = null;
		}
		summaryRepo.saveRecordEntityList(targetRecords);

		// 申請を勤怠明細に紐づけ(月曜～木曜)
		targetRecords = testData.getRecordEntities(testEmp.id, AppDate.newInstance(2017, 9, 4), AppDate.newInstance(2017, 9, 6));
		targetRecords[0].inpStartTime = null;
		targetRecords[0].inpEndTime = null;
		targetRecords[0].reqRequestingLeave1RequestId = leaveReqeust1.id;
		targetRecords[1].inpStartTime = null;
		targetRecords[1].inpEndTime = null;
		targetRecords[1].reqRequestingLeave1RequestId = leaveReqeust2.id;
		targetRecords[2].inpStartTime = null;
		targetRecords[2].inpEndTime = null;
		targetRecords[2].reqRequestingLeave1RequestId = leaveReqeust3_1.id;
		targetRecords[2].reqRequestingLeave2RequestId = leaveReqeust3_2.id;
		summaryRepo.saveRecordEntityList(targetRecords);

		// 申請のステータスを承認済みに更新
		leaveReqeust1.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust1);
		leaveReqeust2.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust2);
		leaveReqeust3_1.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust3_1);
		leaveReqeust3_2.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust3_2);

		// 勤怠計算を実行
		new AttCalcService().calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// パラメータ作成
		AttRequestResource.CheckFixSummaryRequest request = new AttRequestResource.CheckFixSummaryRequest();
		request.summaryId = summary.Id;

		// 一日のみ休職休業日に設定(非通常操作)
		targetRecords = testData.getRecordEntities(testEmp.id, AppDate.newInstance(2017, 9, 7), AppDate.newInstance(2017, 9, 7));
		targetRecords[0].leaveOfAbsenceId = absenceData.id; // 休職休業日
		targetRecords[0].inpStartTime = null;
		targetRecords[0].inpEndTime = null;
		summaryRepo.saveRecordEntityList(targetRecords);

		// 申請実行
		Test.startTest();
			AttRequestResource.CheckFixSummaryApi api = new AttRequestResource.CheckFixSummaryApi();
			AttRequestResource.CheckFixSummaryResponse response =
					(AttRequestResource.CheckFixSummaryResponse)api.execute(request);
		Test.stopTest();

		System.assertEquals(ComMessage.msg().Att_Msg_FixSummaryConfirmAbsence(new List<String>{'1'}), response.confirmation[0]);
		System.assertEquals(ComMessage.msg().Att_Msg_FixSummaryConfirmInsufficientRestTime(new List<String>{'26'}), response.confirmation[1]);
	}

	/**
	 * submitFixRequestのテスト
	 * 申請対象の勤怠サマリーが見つからない場合、アプリ例外が発生することを確認する
	 */
	@isTest static void submitFixRequestTestSummaryNotFound() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
		// 勤怠計算を実行
		new AttCalcService().calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = UserInfo.getUserId(); // 存在しない勤怠サマリー
		param.comment = 'Test Comment';

		// 申請実行
		try {
			Test.startTest();
				AttRequestService service = new AttRequestService();
				service.submitFixRequest(param);
			Test.stopTest();
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NOT_FOUND_ATT_SUMMARY,
					e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * submitFixRequestのテスト
	 * 申請可能でない場合にアプリ例外が発生することを確認する
	 */
	@isTest static void submitFixRequestTestCannotSubmit() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];

		// 勤怠サマリーをロックしておく
		summary.isLocked = true;
		summary.isDirty = false;
		summaryRepo.saveEntity(summary);

		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = summary.id;
		param.comment = 'Test Comment';

		// 申請実行
		try {
			Test.startTest();
				AttRequestService service = new AttRequestService();
				service.submitFixRequest(param);
			Test.stopTest();
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_ATT_INVALID_LOCKED, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * submitFixRequestのテスト
	 * 勤怠明細の値が正しく設定されていない場合にアプリ例外が発生することを確認する
	 */
	@isTest static void submitFixRequestTestInvalidRecord() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
		// 勤怠計算を実行
		new AttCalcService().calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// 勤怠明細の休憩時間の開始時刻より終了時刻を早い時刻に設定する
		summary.attRecordList[0].inpRest1EndTime = summary.attRecordList[0].inpRest1StartTime.addMinutes(-1);
		summaryRepo.saveRecordEntity(summary.attRecordList[0]);

		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = summary.id;
		param.comment = 'Test Comment';

		// 申請実行
		try {
			Test.startTest();
				AttRequestService service = new AttRequestService();
				service.submitFixRequest(param);
			Test.stopTest();
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_ATT_INVALID_REST_TIME, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * submitFixRequestのテスト
	 * 会社のデフォルト言語で申請できていることを確認する(デフォルト言語が英語の場合)
	 */
	@isTest static void submitFixRequestTestLangJa() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
		// 勤怠計算を実行
		AttCalcService calcService = new AttCalcService();
		calcService.calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// 申請実行
		Test.startTest();

			// パラメータ作成
			AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
			param.comment = null;

			// 会社のデフォルト言語が日本語
			testData.company.Language__c = AppLanguage.JA.value;
			update testData.company;

			param.summaryId = summary.Id;
			new AttRequestService().submitFixRequest(param);

			// 勤怠申請を確認する
			// 日本語
			AttSummaryEntity afterSummary = summaryRepo.getEntity(summary.Id);
			AttRequestEntity requestEntity = requestRepo.getEntity(afterSummary.requestId);
			System.assertNotEquals(null, requestEntity);
			String employeeName = testEmp1.displayNameL.getValue(AppLanguage.JA);
			System.assertEquals(employeeName + 'さん 勤務確定申請 2017/09', requestEntity.name);

		Test.stopTest();
	}

	/**
	 * submitFixRequestのテスト
	 * 会社のデフォルト言語で申請できていることを確認する(デフォルト言語が英語の場合)
	 */
	@isTest static void submitFixRequestTestLangEnUs() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
		// 勤怠計算を実行
		AttCalcService calcService = new AttCalcService();
		calcService.calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// 申請実行
		Test.startTest();

			// パラメータ作成
			AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
			param.comment = null;

			// 会社のデフォルト言語が英語
			testData.company.Language__c = AppLanguage.EN_US.value;
			update testData.company;

			param.summaryId = summary.Id;
			new AttRequestService().submitFixRequest(param);

			// 勤怠申請を確認する
			// 英語
			AttSummaryEntity afterSummary = summaryRepo.getEntity(summary.Id);
			AttRequestEntity requestEntity = requestRepo.getEntity(afterSummary.requestId);
			System.assertNotEquals(null, requestEntity);
			String employeeName = testEmp1.displayNameL.getValue(AppLanguage.EN_US);
			System.assertEquals(employeeName + ' Monthly Attendance Request Sep 2017', requestEntity.name);

		Test.stopTest();
	}

	/**
	 * submitFixRequestのテスト
	 * 会社のデフォルト言語が設定されてない場合、想定されるアプリ例外が発生することを確認する
	 */
	@isTest static void submitFixRequestTestNotSetDefaultLang() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
		// 勤怠計算を実行
		AttCalcService calcService = new AttCalcService();
		calcService.calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		Test.startTest();
			try {
				testData.company.Language__c = null;
				update testData.company;

				// パラメータ作成
				AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
				param.comment = null;
				param.summaryId = summary.Id;

				// 申請実行
				new AttRequestService().submitFixRequest(param);
			} catch (App.IllegalStateException e) {
				System.assertEquals(App.ERR_CODE_NOT_SET_COMPANY_LANG, e.getErrorCode());
			} catch (Exception e) {
				System.debug('例外 : ' + e.getStackTraceString());
				System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
			}
		Test.stopTest();
	}

	/**
	 * submitFixRequestのテスト
	 * 勤怠期間の労働時間制を不一致チェック
	 */
	@isTest static void submitFixRequestTestCheckPeriodStatus() {
		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(1);
		EmployeeBaseEntity testEmp1 = testEmpList[0];

		// テスト社員1の承認者01設定
		testData.employee.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testData.employee.getHistory(0));

		AttAttendanceService service = new AttAttendanceService();
		Date dt = Date.today();
		AttSummaryEntity summaryData = service.getSummary(testData.employee.id, AppDate.valueOf(dt));

		// 短時間勤務設定追加(フレックスタイム)
		AttShortTimeSettingBaseEntity shortSetting =
				testData.createShortSetting(AttWorkSystem.JP_FLEX, '001', null);
		AppDate validFrom = summaryData.startDate;
		AppDate validTo = summaryData.startDate.addDays(10);
		AttPeriodStatusEntity periodStatusData = testData.createShortTimePeriodStatus(null, testData.employee.id, validFrom, validTo, shortSetting.id);
		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = summaryData.id;

		Test.startTest();
		// 申請実行
		try {
			new AttRequestService().submitFixRequest(param);
			System.assert(false, '労働制不一致エラーが発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assert(true);
			System.assertEquals(ComMessage.msg().Att_Err_InvalidWorkSystemShortTimeWorkSetting, e.getMessage());
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
		Test.stopTest();
	}
	/**
	 * createRequestNameのテスト
	 * 申請名が正しく作成できることを確認する
	 */
	@isTest static void createRequestNameTest() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testData.employee.id, startDate)[0];

		AttRequestService service = new AttRequestService();
		String requestName;

		// 言語の確認
		// 日本語の場合
		requestName = service.createRequestName(testData.employee,
				summary.startDate, summary.endDate, AppLanguage.JA, testData.workingType);
		String employeeName = testData.employee.displayNameL.getValue(AppLanguage.JA);
		System.assertEquals(employeeName + 'さん 勤務確定申請 2017/09', requestName);

		// 英語の場合
		requestName = service.createRequestName(testData.employee,
				summary.startDate, summary.endDate, AppLanguage.EN_US, testData.workingType);
		employeeName = testData.employee.displayNameL.getValue(AppLanguage.EN_US);
		System.assertEquals(employeeName + ' Monthly Attendance Request Sep 2017', requestName);

		// 勤務体系の月度表記が EndBase の場合
		// 終了日の月度で作成される
		testData.workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		requestName = service.createRequestName(testData.employee,
				AppDate.newInstance(2017, 9, 16), AppDate.newInstance(2017, 10, 15),
				AppLanguage.JA, testData.workingType);
		employeeName = testData.employee.displayNameL.getValue(AppLanguage.JA);
		System.assertEquals(employeeName + 'さん 勤務確定申請 2017/10', requestName);

	}

	/**
	 * submitFixRequestのテスト
	 * 勤務確定申請が再申請できることを確認する
	 */
	@isTest static void submitFixRequestTestResubmit() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(3);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
		// 勤怠計算を実行
		new AttCalcService().calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = summary.Id;
		param.comment = 'Test Comment';

		AttRequestService service = new AttRequestService();

		// １回目の申請を却下する
		service.submitFixRequest(param);
		AttSummaryEntity afterSummary = summaryRepo.getEntity(summary.Id);
		AttRequestEntity requestEntity = requestRepo.getEntity(afterSummary.requestId);
		requestEntity.status = AppRequestStatus.DISABLED;
		requestEntity.cancelType = AppCancelType.REJECTED;
		requestRepo.saveEntity(requestEntity);
		summary.isLocked = false;
		summaryRepo.saveEntity(summary);

		// 再申請実行
		Test.startTest();
		service.submitFixRequest(param);
		Test.stopTest();

		// 勤怠サマリーを確認する
		AttSummaryEntity afterSummary2 = summaryRepo.getEntity(summary.Id);
		System.assertEquals(afterSummary.requestId, afterSummary2.requestId); // 申請IDは変更されていないはず
		System.assertEquals(true, afterSummary2.isLocked);
		// 勤怠申請を確認する
		AttRequestEntity requestEntity2 = requestRepo.getEntity(afterSummary.requestId);
		System.assertNotEquals(null, requestEntity2);
		System.assertEquals(true, String.isNotBlank(requestEntity2.name)); // 申請名は多言語のテストで確認
		System.assertEquals(afterSummary.id, requestEntity2.summaryId);
		System.assertEquals(param.comment, requestEntity2.comment);
		System.assertEquals(AppRequestStatus.PENDING, requestEntity2.status);
		System.assertEquals(afterSummary.employeeId, requestEntity2.employeeHistoryId);
		System.assertEquals(afterSummary.deptId, requestEntity2.departmentHistoryId);
		System.assertEquals(testData.employee.getHistory(0).id, requestEntity2.actorId);
		// 勤怠申請に添付ファイルが追加されていることを確認する
		AppAttachmentRepository attachRepo = new AppAttachmentRepository();
		List<AppAttachmentEntity> attaches = attachRepo.getEntityListByParentId(requestEntity.id);
		System.assertEquals(1, attaches.size());
		AppAttachmentEntity attach = attaches[0];
		System.assertEquals(requestEntity.name, attach.name);
		System.assertEquals(requestEntity.id, attach.parentId);
	}
	/**
	 * submitFixRequestのテスト
	 * 勤務確定申請時に欠勤日数が正しくカウントされることを確認する
	 */
	@isTest static void submitFixRequestTestAbsenceDays() {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(2);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];

		// 申請データ作成
		// 終日休（欠勤カウント対象外）
		AttDailyRequestEntity leaveReqeust1 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_DAY, AppDate.newInstance(2017, 9, 4), AppDate.newInstance(2017, 9, 4), null, null);
		leaveReqeust1.employeeId = testEmp1.getHistory(0).id;
		leaveReqeust1.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust1.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes1 = requestRepo.saveEntity(leaveReqeust1);
		leaveReqeust1.setId(requestRes1.details[0].id);
		// 半休（欠勤カウント対象）
		AttDailyRequestEntity leaveReqeust2 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_AM, AppDate.newInstance(2017, 9, 5), AppDate.newInstance(2017, 9, 5), null, null);
		leaveReqeust2.employeeId = testEmp1.getHistory(0).id;
		leaveReqeust2.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust2.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes2 = requestRepo.saveEntity(leaveReqeust2);
		leaveReqeust2.setId(requestRes2.details[0].id);
		// 半休＋半休（欠勤カウント対象外）
		AttDailyRequestEntity leaveReqeust3_1 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_AM, AppDate.newInstance(2017, 9, 6), AppDate.newInstance(2017, 9, 6), null, null);
		leaveReqeust3_1.employeeId = testEmp1.getHistory(0).id;
		leaveReqeust3_1.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust3_1.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes3_1 = requestRepo.saveEntity(leaveReqeust3_1);
		leaveReqeust3_1.setId(requestRes3_1.details[0].id);
		AttDailyRequestEntity leaveReqeust3_2 = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_PM, AppDate.newInstance(2017, 9, 6), AppDate.newInstance(2017, 9, 6), null, null);
		leaveReqeust3_2.employeeId = testEmp1.getHistory(0).id;
		leaveReqeust3_2.requestTime = new AppDatetime(Datetime.newInstance(2017, 9, 1, 9, 0, 0));
		leaveReqeust3_2.status = AppRequestStatus.PENDING;
		Repository.SaveResult requestRes3_2 = requestRepo.saveEntity(leaveReqeust3_2);
		leaveReqeust3_2.setId(requestRes3_2.details[0].id);
		// TODO:時間単位半休（欠勤カウント対象外）
		// TODO:時間単位半休＋時間単位半休（欠勤カウント対象外）
		// TODO:半休＋時間単位半休（欠勤カウント対象）
		// TODO:時間休

		// 申請を勤怠明細に紐づけ
		List<AttRecordEntity> targetRecords = testData.getRecordEntities(testEmp1.id, AppDate.newInstance(2017, 9, 4), AppDate.newInstance(2017, 9, 6));
		targetRecords[0].inpStartTime = null;
		targetRecords[0].inpEndTime = null;
		targetRecords[0].reqRequestingLeave1RequestId = leaveReqeust1.id;
		targetRecords[1].inpStartTime = null;
		targetRecords[1].inpEndTime = null;
		targetRecords[1].reqRequestingLeave1RequestId = leaveReqeust2.id;
		targetRecords[2].inpStartTime = null;
		targetRecords[2].inpEndTime = null;
		targetRecords[2].reqRequestingLeave1RequestId = leaveReqeust3_1.id;
		targetRecords[2].reqRequestingLeave2RequestId = leaveReqeust3_2.id;
		summaryRepo.saveRecordEntityList(targetRecords);

		// 申請のステータスを承認済みに更新
		leaveReqeust1.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust1);
		leaveReqeust2.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust2);
		leaveReqeust3_1.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust3_1);
		leaveReqeust3_2.status = AppRequestStatus.APPROVED;
		requestRepo.saveEntity(leaveReqeust3_2);

		// 勤怠計算を実行
		new AttCalcService().calcAttendance(summary.id);
		summary = summaryRepo.getEntity(summary.id);
		clearAttTimeForHoliday(summary.attRecordList);
		summaryRepo.saveRecordEntityList(summary.attRecordList);

		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = summary.Id;
		param.comment = 'Test Comment';

		// 申請実行
		Test.startTest();
			AttRequestService service = new AttRequestService();
			service.submitFixRequest(param);
		Test.stopTest();

		// 勤怠サマリーを確認する
		AttSummaryEntity afterSummary = summaryRepo.getEntity(summary.Id);
		System.assertEquals(0.5, afterSummary.outWorkAbsenceDays.getDecValue());

		// 勤怠明細を確認する
		System.assertEquals(0, afterSummary.attRecordList[3].outWorkAbsenceDays.getDecValue());
		System.assertEquals(0.5, afterSummary.attRecordList[4].outWorkAbsenceDays.getDecValue());
		System.assertEquals(0, afterSummary.attRecordList[5].outWorkAbsenceDays.getDecValue());
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が無効の場合、アプリ例外が発生することを確認する
	 */
	@isTest
	static void selfApprovePermissionTestFailure() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務確定申請自己承認(本人)の権限を無効に設定する
		testData.permission.isApproveSelfAttRequestByEmployee = false;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		System.runAs(AttTestData.getUser(testEmp1.userId)) {

			// 勤務表データ
			AttSummaryRepository summaryRepo = new AttSummaryRepository();
			AppDate startDate = AppDate.newInstance(2017, 9, 1);
			testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
			AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
			// 勤怠計算を実行
			new AttCalcService().calcAttendance(summary.id);
			summary = summaryRepo.getEntity(summary.id);
			clearAttTimeForHoliday(summary.attRecordList);
			summaryRepo.saveRecordEntityList(summary.attRecordList);

			// パラメータ作成
			AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
			param.summaryId = summary.id;
			param.comment = 'Test Comment';

			// 実行
			App.NoPermissionException actEx;
			try {
				new AttRequestService().submitFixRequest(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
			String expectMessage = ComMessage.msg().Att_Err_NotAllowSelfAttRequest(new List<String>{testEmp1.fullNameL.getFullName()});
			System.assertEquals(expectMessage, actEx.getMessage());
		}
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が有効の場合、アプリ例外が発生しないことを確認する
	 */
	@isTest
	static void selfApprovePermissionTestSuccess() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務確定申請自己承認(本人)の権限を有効に設定する
		testData.permission.isApproveSelfAttRequestByEmployee = true;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		System.runAs(AttTestData.getUser(testEmp1.userId)) {

			// 勤務表データ
			AttSummaryRepository summaryRepo = new AttSummaryRepository();
			AppDate startDate = AppDate.newInstance(2017, 9, 1);
			testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
			AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];
			// 勤怠計算を実行
			new AttCalcService().calcAttendance(summary.id);
			summary = summaryRepo.getEntity(summary.id);
			clearAttTimeForHoliday(summary.attRecordList);
			summaryRepo.saveRecordEntityList(summary.attRecordList);

			// パラメータ作成
			AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
			param.summaryId = summary.id;
			param.comment = 'Test Comment';

			// 実行
			App.NoPermissionException actEx;
			try {
				new AttRequestService().submitFixRequest(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertEquals(null, actEx);
		}
	}

	/**
	 * submitFixRequestのテスト
	 * Salesforceユーザが無効である社員が勤務確定申請したとき適切な例外が発生することを確認する
	 */
	@isTest static void submitFixRequestTestInactiveUser() {
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRequestRepository requestRepo = new AttRequestRepository();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmpList = testData.createTestEmployees(2);
		EmployeeBaseEntity testEmp1 = testEmpList[0];
		EmployeeBaseEntity testEmp2 = testEmpList[1];

		// テスト社員1の承認者01設定
		testEmp1.getHistory(0).approverBase01Id = testEmp2.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, true);
		AttSummaryEntity summary = summaryRepo.searchEntity(testEmp1.id, startDate)[0];

		// パラメータ作成
		AttRequestService.SubmitFixRequestParam param = new AttRequestService.SubmitFixRequestParam();
		param.summaryId = summary.id;
		param.comment = 'Test Comment';

		// ※ テスト実行時の MIXED_DML_OPERATION エラーを防ぐため System.runAs(user) を使用している(下記公式ドキュメント参照)
		// https://developer.salesforce.com/docs/atlas.ja-jp.218.0.apexcode.meta/apexcode/apex_dml_non_mix_sobjects_test_methods.htm
		User testExecutingUser = new User(Id = UserInfo.getUserId());
		System.runAs(testExecutingUser) {
			// ユーザを無効にする
			User requestingUser = new User(Id = testEmp1.userId);
			requestingUser.IsActive = false;
			update requestingUser;

			// 申請実行
			try {
				Test.startTest();
					AttRequestService service = new AttRequestService();
					service.submitFixRequest(param);
				Test.stopTest();
			} catch (App.IllegalStateException e) {
				System.assertEquals(App.ERR_CODE_INACTIVE_USERS_REQUEST, e.getErrorCode());
			} catch (Exception e) {
				System.debug('例外 : ' + e.getStackTraceString());
				System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
			}
		}
	}
}