/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署履歴切り替えバッチのテストクラス
 */
@isTest
private class DepartmentCurrentHistoryUpdateBatchTest {

	/**
	 * Apexスケジュール正常系テスト
	 */
	@isTest
	static void schedulePositiveTest() {
		Test.StartTest();
		DepartmentCurrentHistoryUpdateBatch sch = new DepartmentCurrentHistoryUpdateBatch();
		Id schId = System.schedule('部署履歴切り替えスケジュール', '0 0 0 * * ?', sch);
		Test.StopTest();
		System.assertNotEquals(null, schId);
	}

	/**
	 * バッチ正常系テスト
	 */
	@isTest
	static void batchPositiveTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<ComDeptBase__c> baseObjs = ComTestDataUtility.createDepartmentsWithHistory(
				'Test', companyObj.Id, baseNum, historyNum);
		ComDeptBase__c base1 = baseObjs[0];
		List<ComDeptHistory__c> historyList1 =
				[SELECT Id, BaseId__c, Name_L0__c FROM ComDeptHistory__c WHERE BaseId__c = :base1.Id ORDER BY ValidFrom__c Asc];

		// CurrentHistoryId__c を空欄にしておく
		base1.Name = 'AAA';
		base1.CurrentHistoryId__c = null;
		update base1;

		Test.StartTest();
		DepartmentCurrentHistoryUpdateBatch b = new DepartmentCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
		Test.StopTest();

		System.assertNotEquals(null, jobId);

		ComDeptBase__c resBase = [SELECT Id, Name, CurrentHistoryId__c FROM ComDeptBase__c WHERE Id = :base1.Id];
		System.assertEquals(historyList1[0].Id, resBase.CurrentHistoryId__c);
		System.assertEquals(historyList1[0].Name_L0__c, resBase.Name);
	}
}