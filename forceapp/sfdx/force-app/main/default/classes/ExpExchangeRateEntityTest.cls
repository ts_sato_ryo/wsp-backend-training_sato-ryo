/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpExchangeRateEntity
 */
@isTest
private class ExpExchangeRateEntityTest {

	/**
	 * Test Data Class
	 */
	private class TestData extends TestData.TestDataEntity {
		/** Exchange Rate Data */
		public List<ExpExchangeRateEntity> exchangeRates = new List<ExpExchangeRateEntity>();
		/** Currency object */
		public ComCurrency__c currencyObj;

		/**
		 * Constructor
		 */
		public TestData() {
			super();
			this.currencyObj = ComTestDataUtility.createCurrency('USD', 'US dollar');
			// Create entity
			ComTestDataUtility.createExpExchangeRates('RateCode', this.company.id, this.currencyObj.Id, 3);
			this.exchangeRates = (new ExpExchangeRateRepository()).getEntityList(null);
		}

		/**
		 * Create Exchange Rate object
		 * @code Exchange Rate Code
		 * @size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpExchangeRateEntity> createExpExchangeRates(String code, Integer size) {
			return createExpExchangeRates(code, this.company.id, this.currencyObj.Id, size);
		}

		/**
		 * Create Exchange Rate object
		 * @code Exchange Rate Code
		 * @companyId Company ID
		 * @size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpExchangeRateEntity> createExpExchangeRates(String code, Id companyId, Id currencyId, Integer size) {
			ComTestDataUtility.createExpExchangeRates(code, companyId, currencyId, size);
			return (new ExpExchangeRateRepository()).getEntityList(null);
		}
	}

	/**
	 * Confirm unique key is created properly
	 */
	@isTest static void createUniqKeyTest() {
		ExpExchangeRateEntity entity = new ExpExchangeRateEntity();

		// When rate code and company code are not empty, key would be created.
		entity.code = 'RateCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// When rate code is empty, exception would occur.
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occurred');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// When company code is empty, exception would occur.
		entity.code = 'ExpTypeCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occurred');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/**
	 * Test for coping entity
	 * Confirm entity is copied properly
	 */
	@isTest static void copyTest() {

		Test.startTest();

		ExpExchangeRateEntityTest.TestData testData = new TestData();
		ExpExchangeRateEntity orgEntity = testData.createExpExchangeRates('Test', 2)[1];

		ExpExchangeRateEntity copyEntity = orgEntity.copy();

		Test.stopTest();

		System.assertEquals(null, copyEntity.id);  // ID is not copied
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.currencyId, copyEntity.currencyId);
		System.assertEquals(orgEntity.currencyPair.value, copyEntity.currencyPair.value);
		System.assertEquals(orgEntity.rate, copyEntity.rate);
		System.assertEquals(orgEntity.reverseRate, copyEntity.reverseRate);
		System.assertEquals(orgEntity.validFrom, copyEntity.validFrom);
		System.assertEquals(orgEntity.validTo, copyEntity.validTo);
	}

	@isTest static void isCounterCurrencyTest() {

		Test.startTest();

		ExpExchangeRateEntityTest.TestData testData = new TestData();

		ExpExchangeRateEntity entity = testData.exchangeRates[0];

		System.assertEquals(false, entity.isCounterCurrency());
		System.assertEquals(entity.rate, entity.calculationRate);

		entity.currencyCode = 'JPY';

		System.assertEquals(true, entity.isCounterCurrency());
		System.assertEquals(entity.reverseRate, entity.calculationRate);
	}
}