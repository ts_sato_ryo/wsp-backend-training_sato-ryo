/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description CalendarRepositoryのテストクラス
*/
@isTest
private class CalendarRepositoryTest {
	@isTest static void getRecordListTest() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		ComCalendar__c calendarA = ComTestDataUtility.createCalendar('a', company.Id);
		ComCalendar__c calendarB = ComTestDataUtility.createCalendar('b', company.Id);

		Date targetDate = Date.valueOf('2017-10-10');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c calendarA1 =
				ComTestDataUtility.createCalendarRecord('a1', calendarA.Id, targetDate, dayType);

		targetDate = Date.valueOf('2017-10-11');
		dayType = AttDayType.HOLIDAY;
		ComCalendarRecord__c calendarA2 =
				ComTestDataUtility.createCalendarRecord('a2', calendarA.Id, targetDate, dayType);

		targetDate = Date.valueOf('2017-10-11');
		dayType = AttDayType.LEGAL_HOLIDAY;
		ComCalendarRecord__c calendarB1 =
				ComTestDataUtility.createCalendarRecord('b1', calendarB.Id, targetDate, dayType);

		CalendarRepository calRep = new CalendarRepository();
		Test.startTest();
		AppDate startDate = AppDate.newInstance(2017, 10, 10);
		AppDate endDate = AppDate.newInstance(2017, 10, 11);
		List<CalendarRecordEntity> recordDataList =
				calRep.searchRecordList(new Set<Id>{calendarA.Id}, startDate, endDate);
		Test.stopTest();

		System.assertEquals(2, recordDataList.size());

		CalendarRecordEntity recordData = recordDataList[0];
		System.assertEquals(Date.valueOf('2017-10-10'), AppConverter.dateValue(recordData.calDate));
		System.assertEquals(true, AttDayType.WORKDAY.equals(recordData.dayType));

		recordData = recordDataList[1];
		System.assertEquals(Date.valueOf('2017-10-11'), AppConverter.dateValue(recordData.calDate));
		System.assertEquals(true, AttDayType.HOLIDAY.equals(recordData.dayType));
	}

	/**
	 * searchEntityのテスト
	 * 取得対象の項目の値が正しく取得できていることを確認する
	 */
	@isTest static void searchEntityTestAllField() {
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyA = ComTestDataUtility.createCompany('testA', country.Id);
		ComCompany__c companyB = ComTestDataUtility.createCompany('testB', country.Id);
		ComCalendar__c calendarA = ComTestDataUtility.createCalendar('a', companyA.Id);
		ComCalendar__c calendarB = ComTestDataUtility.createCalendar('b', companyB.Id);

		ComCalendarRecord__c calendarA1 = ComTestDataUtility.createCalendarRecord(
				'a1', calendarA.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarA2 = ComTestDataUtility.createCalendarRecord(
				'a2', calendarA.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);
		ComCalendarRecord__c calendarB1 = ComTestDataUtility.createCalendarRecord(
				'b1', calendarB.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarB2 = ComTestDataUtility.createCalendarRecord(
				'b2', calendarB.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);

		// 検索する
		Boolean withRecords = true;
		Boolean forUpdate = false;
		CalendarRepository calRep = new CalendarRepository();
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		filter.calendarIds = new List<Id>{calendarA.Id};
		List<CalendarEntity> resCalendarList = calRep.searchEntity(filter, withRecords, forUpdate);

		System.assertEquals(1, resCalendarList.size());
		System.assertEquals(2, resCalendarList[0].recordList.size());
		assertCalendarEntity(getCalendarObj(calendarA.Id), resCalendarList[0]);
		assertCalendarRecordEntity(getCalendarRecordObj(calendarA1.Id), resCalendarList[0].recordList[0]);
	}

	/**
	 * searchEntityのテスト
	 * 検索フィルタの削除フラグが正しく動作していることを確認する
	 */
	@isTest static void searchEntityTestIncludeRemoved() {
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyA = ComTestDataUtility.createCompany('testA', country.Id);
		ComCompany__c companyB = ComTestDataUtility.createCompany('testB', country.Id);
		ComCalendar__c calendarA = ComTestDataUtility.createCalendar('a', companyA.Id);
		ComCalendar__c calendarB = ComTestDataUtility.createCalendar('b', companyB.Id);

		ComCalendarRecord__c calendarA1 = ComTestDataUtility.createCalendarRecord(
				'a1', calendarA.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarA2 = ComTestDataUtility.createCalendarRecord(
				'a2', calendarA.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);
		ComCalendarRecord__c calendarB1 = ComTestDataUtility.createCalendarRecord(
				'b1', calendarB.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarB2 = ComTestDataUtility.createCalendarRecord(
				'b2', calendarB.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);

		// 論理削除にする
		calendarB.Removed__c = true;
		update calendarB;
		calendarA1.Removed__c = true;
		update calendarA1;

		Boolean withRecords = true;
		Boolean forUpdate = false;
		CalendarRepository calRep = new CalendarRepository();
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		List<CalendarEntity> resCalendarList;

		// Test1: 論理削除されているレコードは取得できない
		filter.includeRemoved = false;
		resCalendarList = calRep.searchEntity(filter, withRecords, forUpdate);
		System.assertEquals(1, resCalendarList.size());
		System.assertEquals(1, resCalendarList[0].recordList.size());
		System.assertEquals(calendarA.Id, resCalendarList[0].Id);
		System.assertEquals(calendarA2.Id, resCalendarList[0].recordList[0].Id);

		// Test2: 論理削除されているレコードも取得できる
		filter.includeRemoved = true;
		resCalendarList = calRep.searchEntity(filter, withRecords, forUpdate);
		System.assertEquals(2, resCalendarList.size());
		System.assertEquals(2, resCalendarList[0].recordList.size());
		System.assertEquals(2, resCalendarList[1].recordList.size());
		System.assertEquals(calendarA.Id, resCalendarList[0].Id);
		System.assertEquals(calendarA1.Id, resCalendarList[0].recordList[0].Id);
		System.assertEquals(calendarB.Id, resCalendarList[1].Id);
		System.assertEquals(calendarB1.Id, resCalendarList[1].recordList[0].Id);
	}

	/**
	 * searchRecordEntityのテスト
	 * 項目の値が正しく取得できていることを確認する
	 */
	@isTest static void searchRecordEntityTestAllField() {
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyA = ComTestDataUtility.createCompany('testA', country.Id);
		ComCompany__c companyB = ComTestDataUtility.createCompany('testB', country.Id);
		ComCalendar__c calendarA = ComTestDataUtility.createCalendar('a', companyA.Id);
		ComCalendar__c calendarB = ComTestDataUtility.createCalendar('b', companyB.Id);

		ComCalendarRecord__c calendarA1 = ComTestDataUtility.createCalendarRecord(
				'a1', calendarA.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarA2 = ComTestDataUtility.createCalendarRecord(
				'a2', calendarA.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);
		ComCalendarRecord__c calendarB1 = ComTestDataUtility.createCalendarRecord(
				'b1', calendarB.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarB2 = ComTestDataUtility.createCalendarRecord(
				'b2', calendarB.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);

		Boolean forUpdate = false;
		CalendarRepository calRep = new CalendarRepository();
		CalendarRepository.CalendarRecordFilter filter = new CalendarRepository.CalendarRecordFilter();
		filter.recordIds = new Set<Id>{calendarA1.Id};
		List<CalendarRecordEntity> resCalendarList = calRep.searchRecordEntity(filter, forUpdate);

		System.assertEquals(1, resCalendarList.size());
		assertCalendarRecordEntity(getCalendarRecordObj(calendarA1.Id), resCalendarList[0]);
	}

	/**
	 * searchRecordEntityのテスト
	 * 検索フィルタの削除フラグが正しく動作していることを確認する
	 */
	@isTest static void searchRecordEntityTestIncludeRemoved() {
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyA = ComTestDataUtility.createCompany('testA', country.Id);
		ComCompany__c companyB = ComTestDataUtility.createCompany('testB', country.Id);
		ComCalendar__c calendarA = ComTestDataUtility.createCalendar('a', companyA.Id);
		ComCalendar__c calendarB = ComTestDataUtility.createCalendar('b', companyB.Id);

		ComCalendarRecord__c calendarA1 = ComTestDataUtility.createCalendarRecord(
				'a1', calendarA.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarA2 = ComTestDataUtility.createCalendarRecord(
				'a2', calendarA.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);
		ComCalendarRecord__c calendarB1 = ComTestDataUtility.createCalendarRecord(
				'b1', calendarB.Id, Date.newInstance(2017, 10, 10), AttDayType.WORKDAY);
		ComCalendarRecord__c calendarB2 = ComTestDataUtility.createCalendarRecord(
				'b2', calendarB.Id, Date.newInstance(2017, 10, 11), AttDayType.HOLIDAY);

		// 論理削除にする
		calendarB.Removed__c = true;
		update calendarB;
		calendarA1.Removed__c = true;
		update calendarA1;

		Boolean forUpdate = false;
		CalendarRepository calRep = new CalendarRepository();
		CalendarRepository.CalendarRecordFilter filter = new CalendarRepository.CalendarRecordFilter();
		filter.calendarIds = new Set<Id>{calendarA.Id};
		List<CalendarRecordEntity> resCalendarList;

		// Test1: 論理削除されているレコードは取得できない
		filter.includeRemoved = false;
		resCalendarList = calRep.searchRecordEntity(filter, forUpdate);
		System.assertEquals(1, resCalendarList.size());
		System.assertEquals(calendarA2.Id, resCalendarList[0].Id);

		// Test2: 論理削除されているレコードも取得できる
		filter.includeRemoved = true;
		resCalendarList = calRep.searchRecordEntity(filter, forUpdate);
		System.assertEquals(2, resCalendarList.size());
		System.assertEquals(calendarA1.Id, resCalendarList[0].Id);
		System.assertEquals(calendarA2.Id, resCalendarList[1].Id);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。(カレンダー)
	 */
	@isTest static void searchFromCacheCalendar() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);

		// キャッシュをonでインスタンスを生成
		CalendarRepository repository = new CalendarRepository(true);

		// 検索（DBから取得）
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		filter.calendarIds = new List<Id>{calendarObj.Id};
		CalendarEntity calendar = repository.searchEntity(filter, false, false)[0];

		// 取得対象のレコードを更新する
		calendarObj.Code__c = 'NotCache';
		upsert calendarObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		CalendarEntity cachedCalendar = repository.searchEntity(filter, false, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendar.id, cachedCalendar.id);
		System.assertNotEquals('NotCache', cachedCalendar.code);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。(カレンダー + カレンダー明細)
	 */
	@isTest static void searchFromCacheCalendarWithRecord() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);
		ComCalendarRecord__c calendarRecordObj =
				ComTestDataUtility.createCalendarRecord('TestRecord', calendarObj.Id, Date.valueOf('2017-10-10'), AttDayType.WORKDAY);

		// キャッシュをonでインスタンスを生成
		CalendarRepository repository = new CalendarRepository(true);

		// 検索（DBから取得）
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		filter.calendarIds = new List<Id>{calendarObj.Id};
		// カレンダー明細も取得する
		CalendarEntity calendar = repository.searchEntity(filter, true, false)[0];
		CalendarRecordEntity calendarRecord = calendar.recordList[0];

		// 取得対象のレコードを更新する
		calendarObj.Code__c = 'NotCache';
		upsert calendarObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		CalendarEntity cachedCalendar = repository.searchEntity(filter, true, false)[0];
		CalendarRecordEntity cachedCalendarRecord = cachedCalendar.recordList[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendar.id, cachedCalendar.id);
		System.assertEquals(calendarRecord.id, cachedCalendarRecord.id);
		System.assertNotEquals('NotCache', cachedCalendar.code);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。(カレンダー明細)
	 */
	@isTest static void searchFromCacheCalendarRecord() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);
		ComCalendarRecord__c calendarRecordObj =
				ComTestDataUtility.createCalendarRecord('TestRecord', calendarObj.Id, Date.valueOf('2017-10-10'), AttDayType.WORKDAY);

		// キャッシュをonでインスタンスを生成
		CalendarRepository repository = new CalendarRepository(true);

		// 検索（DBから取得）
		CalendarRepository.CalendarRecordFilter filter = new CalendarRepository.CalendarRecordFilter();
		filter.recordIds = new Set<Id>{calendarRecordObj.Id};
		CalendarRecordEntity calendarRecord = repository.searchRecordEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		calendarRecordObj.Name = 'NotCache';
		upsert calendarRecordObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		CalendarRecordEntity cachedCalendarRecord = repository.searchRecordEntity(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendarRecord.id, cachedCalendarRecord.id);
		System.assertNotEquals('NotCache', cachedCalendarRecord.name);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する。(カレンダー)
	 */
	@isTest static void searchFromCacheMultipleInstanceCalendar() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);

		// 検索（DBから取得）
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		filter.calendarIds = new List<Id>{calendarObj.Id};
		CalendarEntity calendar = new CalendarRepository(true).searchEntity(filter, false, false)[0];

		// 取得対象のレコードを更新する
		calendarObj.Code__c = 'NotCache';
		upsert calendarObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		CalendarEntity cachedCalendar = new CalendarRepository(true).searchEntity(filter, false, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendar.id, cachedCalendar.id);
		System.assertNotEquals('NotCache', cachedCalendar.code);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する。(カレンダー明細)
	 */
	@isTest static void searchFromCacheMultipleInstanceCalendarReocrd() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);
		ComCalendarRecord__c calendarRecordObj =
				ComTestDataUtility.createCalendarRecord('TestRecord', calendarObj.Id, Date.valueOf('2017-10-10'), AttDayType.WORKDAY);

		// 検索（DBから取得）
		CalendarRepository.CalendarRecordFilter filter = new CalendarRepository.CalendarRecordFilter();
		filter.recordIds = new Set<Id>{calendarRecordObj.Id};
		CalendarRecordEntity calendarRecord = new CalendarRepository(true).searchRecordEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		calendarRecordObj.Name = 'NotCache';
		upsert calendarRecordObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		CalendarRecordEntity cachedCalendarRecord = new CalendarRepository(true).searchRecordEntity(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendarRecord.id, cachedCalendarRecord.id);
		System.assertNotEquals('NotCache', cachedCalendarRecord.name);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。(カレンダー)
	 */
	@isTest static void searchBaseNotFromCacheDifferentConditionCalendar() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);

		// キャッシュをonでインスタンスを生成
		CalendarRepository repository = new CalendarRepository(true);

		// 検索（DBから取得）
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		filter.calendarIds = new List<Id>{calendarObj.Id};
		CalendarEntity calendar = repository.searchEntity(filter, false, false)[0];

		// 取得対象のレコードを更新する
		calendarObj.Code__c = 'NotCache';
		upsert calendarObj;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.codes = new List<String>{calendarObj.Code__c};
		CalendarEntity cachedCalendar = repository.searchEntity(filter, false, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendar.id, cachedCalendar.id);
		System.assertEquals('NotCache', cachedCalendar.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。(カレンダー明細)
	 */
	@isTest static void searchBaseNotFromCacheDifferentConditionCalendarRecord() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);
		ComCalendarRecord__c calendarRecordObj =
				ComTestDataUtility.createCalendarRecord('TestRecord', calendarObj.Id, Date.valueOf('2017-10-10'), AttDayType.WORKDAY);

		// キャッシュをonでインスタンスを生成
		CalendarRepository repository = new CalendarRepository(true);

		// 検索（DBから取得）
		CalendarRepository.CalendarRecordFilter filter = new CalendarRepository.CalendarRecordFilter();
		filter.recordIds = new Set<Id>{calendarRecordObj.Id};
		CalendarRecordEntity calendarRecord = repository.searchRecordEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		calendarRecordObj.Name = 'NotCache';
		upsert calendarRecordObj;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.startDate = AppDate.valueOf(calendarRecordObj.Date__c);
		CalendarRecordEntity cachedCalendarRecord = repository.searchRecordEntity(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendarRecord.id, cachedCalendarRecord.id);
		System.assertEquals('NotCache', cachedCalendarRecord.name);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。(カレンダー)
	 */
	@isTest static void searchNotFromCacheForUpdateCalendar() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);

		// キャッシュをonでインスタンスを生成
		CalendarRepository repository = new CalendarRepository(true);

		// 検索（DBから取得）
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		filter.calendarIds = new List<Id>{calendarObj.Id};
		CalendarEntity calendar = repository.searchEntity(filter, false, false)[0];

		// 取得対象のレコードを更新する
		calendarObj.Code__c = 'NotCache';
		upsert calendarObj;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		CalendarEntity cachedCalendar = repository.searchEntity(filter, false, true)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendar.id, cachedCalendar.id);
		System.assertEquals('NotCache', cachedCalendar.code);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。(カレンダー明細)
	 */
	@isTest static void searchNotFromCacheForUpdateCalendarRecord() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);
		ComCalendarRecord__c calendarRecordObj =
				ComTestDataUtility.createCalendarRecord('TestRecord', calendarObj.Id, Date.valueOf('2017-10-10'), AttDayType.WORKDAY);

		// キャッシュをonでインスタンスを生成
		CalendarRepository repository = new CalendarRepository(true);

		// 検索（DBから取得）
		CalendarRepository.CalendarRecordFilter filter = new CalendarRepository.CalendarRecordFilter();
		filter.recordIds = new Set<Id>{calendarRecordObj.Id};
		CalendarRecordEntity calendarRecord = repository.searchRecordEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		calendarRecordObj.Name = 'NotCache';
		upsert calendarRecordObj;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		CalendarRecordEntity cachedCalendarRecord = repository.searchRecordEntity(filter, true)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendarRecord.id, cachedCalendarRecord.id);
		System.assertEquals('NotCache', cachedCalendarRecord.name);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。(カレンダー)
	 */
	@isTest static void searchDisableCacheCalendar() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);

		// キャッシュをoffでインスタンスを生成
		CalendarRepository repository = new CalendarRepository();

		// 検索（DBから取得）
		CalendarRepository.CalendarFilter filter = new CalendarRepository.CalendarFilter();
		filter.calendarIds = new List<Id>{calendarObj.Id};
		CalendarEntity calendar = repository.searchEntity(filter, false, false)[0];

		// 取得対象のレコードを更新する
		calendarObj.Code__c = 'NotCache';
		upsert calendarObj;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		CalendarEntity cachedCalendar = repository.searchEntity(filter, false, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendar.id, cachedCalendar.id);
		System.assertEquals('NotCache', cachedCalendar.code);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。(カレンダー明細)
	 */
	@isTest static void searchDisableCacheCalendarRecord() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendarObj = ComTestDataUtility.createCalendar('Test', companyObj.Id);
		ComCalendarRecord__c calendarRecordObj =
				ComTestDataUtility.createCalendarRecord('TestRecord', calendarObj.Id, Date.valueOf('2017-10-10'), AttDayType.WORKDAY);

		// キャッシュをoffでインスタンスを生成
		CalendarRepository repository = new CalendarRepository();

		// 検索（DBから取得）
		CalendarRepository.CalendarRecordFilter filter = new CalendarRepository.CalendarRecordFilter();
		filter.recordIds = new Set<Id>{calendarRecordObj.Id};
		CalendarRecordEntity calendarRecord = repository.searchRecordEntity(filter, false)[0];

		// 取得対象のレコードを更新する
		calendarRecordObj.Name = 'NotCache';
		upsert calendarRecordObj;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		CalendarRecordEntity cachedCalendarRecord = repository.searchRecordEntity(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(calendarRecord.id, cachedCalendarRecord.id);
		System.assertEquals('NotCache', cachedCalendarRecord.name);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		CalendarRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new CalendarRepository().isEnabledCache);
	}

	/**
	 * 指定したカレンダーIDのレコードを取得する
	 */
	private static ComCalendar__c getCalendarObj(Id calendarId) {
		ComCalendar__c calObj = [
				SELECT
					Id, Name, CompanyId__c, Code__c, UniqKey__c, Name_L0__c,  Name_L1__c,  Name_L2__c,
					Type__c, Remarks__c, Removed__c
				FROM
					ComCalendar__c
				WHERE
					Id = :calendarId];
		return calObj;
	}

	/**
	 * 指定したカレンダー明細IDのレコードを取得する
	 */
	private static ComCalendarRecord__c getCalendarRecordObj(Id calendarRecordId) {
		ComCalendarRecord__c calObj = [
				SELECT
					Id, Name, CalendarId__c, Date__c, AttDayType__c, Name_L0__c,  Name_L1__c,  Name_L2__c,
					Remarks__c, Removed__c
				FROM
					ComCalendarRecord__c
				WHERE
					Id = :calendarRecordId];
		return calObj;
	}

	/**
	 * 検索で取得したカレンダーの各項目の値が期待値に一致することを確認する
	 */
	private static void assertCalendarEntity(ComCalendar__c expObj, CalendarEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL.valueL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL.valueL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL.valueL2);
		System.assertEquals(expObj.Type__c, actEntity.calType.value);
		System.assertEquals(expObj.Remarks__c, actEntity.remarks);
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);
	}

	/**
	 * 検索で取得したカレンダー明細の各項目の値が期待値に一致することを確認する
	 */
	private static void assertCalendarRecordEntity(ComCalendarRecord__c expObj, CalendarRecordEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.CalendarId__c, actEntity.calendarId);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL.valueL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL.valueL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL.valueL2);
		System.assertEquals(expObj.Date__c, actEntity.calDate.getDate());
		System.assertEquals(expObj.AttDayType__c, actEntity.dayType.value);
		System.assertEquals(expObj.Remarks__c, actEntity.remarks);
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);
	}

	/**
	 * saveCalendarEntityのテスト
	 * 対象の項目の値が正しく保存できていることを確認する
	 */
	@isTest static void saveCalendarEntityTestAllField() {
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('testA', country.Id);

		CalendarEntity calendarA = new CalendarEntity();
		calendarA.name = 'TestA';
		calendarA.companyId = company.Id;
		calendarA.nameL = new AppMultiString('TestA L0', 'TestA L1', 'TestA L2');
		calendarA.code = 'TestA Code';
		calendarA.uniqKey = company.Code__c + 'Test Code';
		calendarA.calType = CalendarType.ATTENDANCE;
		calendarA.remarks = 'TestA Remarks';
		calendarA.isRemoved = true;

		CalendarRecordEntity calendarA1 = new CalendarRecordEntity();
		calendarA1.name = 'TestA1';
		calendarA1.nameL = new AppMultiString('TestA1 L0', 'TestA1 L1', 'TestA1 L2');
		calendarA1.calDate = AppDate.newInstance(2017, 10, 11);
		calendarA1.dayType = AttDayType.HOLIDAY;
		calendarA1.remarks = 'TestA1 Remarks';
		calendarA1.isRemoved = true;

		CalendarRecordEntity calendarA2 = new CalendarRecordEntity();
		calendarA2.name = 'TestA2';
		calendarA2.nameL = new AppMultiString('TestA2 L0', 'TestA2 L1', 'TestA2 L2');
		calendarA2.calDate = AppDate.newInstance(2017, 10, 10);
		calendarA2.dayType = AttDayType.WORKDAY;
		calendarA2.remarks = 'TestA2 Remarks';
		calendarA2.isRemoved = false;

		calendarA.recordList.add(calendarA1);
		calendarA.recordList.add(calendarA2);

		Boolean withRecords = true;
		CalendarRepository calRep = new CalendarRepository();
		CalendarRepository.SaveCalendarResult saveResult = calRep.saveCalendarEntity(calendarA, withRecords);

		System.assertEquals(true, saveResult.isSuccessAll);
		System.assertEquals(2, saveResult.recordDetails.size());
		assertCalendarObj(calendarA, getCalendarObj(saveResult.details[0].id));
		assertCalendarRecordObj(calendarA1, getCalendarRecordObj(saveResult.recordDetails[0].id), saveResult.details[0].id);
	}

	/**
	 * カレンダーエンティティの各項目の値が正しく保存されていることを確認する
	 */
	private static void assertCalendarObj(CalendarEntity expEntity, ComCalendar__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.nameL.valueL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL.valueL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL.valueL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.calType.value, actObj.Type__c);
		System.assertEquals(expEntity.remarks, actObj.Remarks__c);
		System.assertEquals(expEntity.isRemoved, actObj.Removed__c);
	}

	/**
	 * カレンダー明細エンティティの各項目の値が正しく保存されていることを確認する
	 */
	private static void assertCalendarRecordObj(CalendarRecordEntity expEntity, ComCalendarRecord__c actObj, Id calendarId) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(calendarId, actObj.CalendarId__c);
		System.assertEquals(expEntity.nameL.valueL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL.valueL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL.valueL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.calDate.getDate(), actObj.Date__c);
		System.assertEquals(expEntity.dayType.value, actObj.AttDayType__c);
		System.assertEquals(expEntity.remarks, actObj.Remarks__c);
		System.assertEquals(expEntity.isRemoved, actObj.Removed__c);
	}

}