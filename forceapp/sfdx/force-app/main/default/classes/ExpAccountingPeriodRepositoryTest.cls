/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test for repository of Accounting Period
 */
@isTest
private class ExpAccountingPeriodRepositoryTest {

	/** Test Data */
	private class RepoTestData {

		/** Country object */
		public ComCountry__c countryObj;
		/** Company object */
		public ComCompany__c companyObj;

		/** Constructor */
		public RepoTestData() {
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
		}

		/**
		 * Create Accounting Period objects
		 */
		public List<ExpAccountingPeriod__c> createExpAccountingPeriods(String code, Integer size) {
			return ComTestDataUtility.createExpAccountingPeriods(code, this.companyObj.Id, size);
		}

		/**
		 * Create Company object
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}
	}

	/**
	 * Test for getEntity method
	 * Confirm being enable to get entity by specified ID
	 */
	@isTest
	static void getEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpAccountingPeriod__c> testCurrencyRate = testData.createExpAccountingPeriods('SearchTest', recordSize);
		ExpAccountingPeriod__c targetObj = testCurrencyRate[1];

		ExpAccountingPeriodRepository repo = new ExpAccountingPeriodRepository();
		ExpAccountingPeriodEntity resEntity;

		// Check existing ID
		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);

		// Check not existing ID
		delete targetObj;
		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * Test for searchEntity method
	 * Confirm enable to search data properly
	 */
	@isTest
	static void searchEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		testData.createCompany('Search Test Co.');
		List<ExpAccountingPeriod__c> testGroups = testData.createExpAccountingPeriods('SearchTest', recordSize);
		ExpAccountingPeriod__c targetObj = testGroups[1];


		ExpAccountingPeriodRepository repo = new ExpAccountingPeriodRepository();
		ExpAccountingPeriodRepository.SearchFilter filter;
		List<ExpAccountingPeriodEntity> resEntities;

		// Search by ID
		filter = new ExpAccountingPeriodRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
		// Check fields value
		assertFieldValue(targetObj, resEntities[0]);
	}

	/**
	 * Test for searchEntity method
	 * Confirm being enable to get valid data by specified date.
	 */
	@isTest
	static void searchEntityTestValidDate() {

		// Create test data
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		testData.createCompany('Search Test Co.');
		List<ExpAccountingPeriod__c> targetObjs = testData.createExpAccountingPeriods('Search Test', recordSize);
		ExpAccountingPeriod__c targetObj1 = targetObjs[0];
		ExpAccountingPeriod__c targetObj2 = targetObjs[1];
		ExpAccountingPeriod__c targetObj3 = targetObjs[2];
		targetObj2.ValidFrom__c = targetObj1.ValidTo__c;
		targetObj2.ValidTo__c = targetObj2.ValidFrom__c.addYears(1);
		targetObj3.ValidFrom__c = targetObj2.ValidTo__c;
		targetObj3.ValidTo__c = targetObj3.ValidFrom__c.addYears(1);
		update targetObjs;

		ExpAccountingPeriodRepository repo = new ExpAccountingPeriodRepository();
		ExpAccountingPeriodRepository.SearchFilter filter;
		List<ExpAccountingPeriodEntity> resEntities;

		// Specify start date of the data as a target date
		filter = new ExpAccountingPeriodRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj2.ValidFrom__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// Specify one day before the expire date of the latest data as target date
		// Should be able to get data
		filter = new ExpAccountingPeriodRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c.addDays(-1));
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj3.Id, resEntities[0].id);

		// Specify the expire date of the latest data as target date
		// Should not be able to get data
		filter = new ExpAccountingPeriodRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());
	}

	/**
	 * Check fields value
	 * @param expObj Expected value
	 * @param actEntity Actual value
	 */
	private static void assertFieldValue(ExpAccountingPeriod__c expObj, ExpAccountingPeriodEntity actEntity) {
		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(AppDate.valueOf(expObj.ValidFrom__c), actEntity.validFrom);
		System.assertEquals(AppDate.valueOf(expObj.ValidTo__c), actEntity.validTo);
		System.assertEquals(AppDate.valueOf(expObj.RecordingDate__c), actEntity.recordingDate);
		System.assertEquals(expObj.Active__c, actEntity.active);
	}

}