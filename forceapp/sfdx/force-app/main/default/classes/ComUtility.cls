/**
 * 共通ツールクラス
 */
public with sharing class ComUtility {

	/**
	 * ログインユーザの言語に対応するマスタデータの姓名項目の値を設定する
	 * @param lastNameL0 言語0(デフォルト)の姓
	 * @param firstNameL0 言語0(デフォルト)の名
	 * @param lastNameL1 言語1の姓
	 * @param firstNameL1 言語1の名
	 * @param lastNameL2 言語2の姓
	 * @param firstNameL2 言語2の名
	 * @return 言語と項目に該当する値
	 */
	public static String getTransNameValue( String lastNameL0, String firstNameL0,
											String lastNameL1, String firstNameL1,
											String lastNameL2, String firstNameL2) {
		return getTransNameValue(userinfo.getLanguage(),
									lastNameL0, firstNameL0,
									lastNameL1, firstNameL1,
									lastNameL2, firstNameL2);
	}

	/**
	 * 指定された言語に対応するマスタデータの姓名項目の値を設定する
	 * @param language 表示言語
	 * @param lastNameL0 言語0(デフォルト)の姓
	 * @param firstNameL0 言語0(デフォルト)の名
	 * @param lastNameL1 言語1の姓
	 * @param firstNameL1 言語1の名
	 * @param lastNameL2 言語2の姓
	 * @param firstNameL2 言語2の名
	 * @return 言語と項目に該当する値
	 */
	public static String getTransNameValue(String language,
												String lastNameL0, String firstNameL0,
												String lastNameL1, String firstNameL1,
												String lastNameL2, String firstNameL2) {
		ComDto.OrgSettingDto setting = ComLogic.getOrgSetting();
		Map<String, String> valueMap = new Map<String, String>{
			ComConst.LANG_TYPE_BASE => getName(setting.language0, lastNameL0, firstNameL0),
			ComConst.LANG_TYPE_EXT1 => getName(setting.language1, lastNameL1, firstNameL1),
			ComConst.LANG_TYPE_EXT2 => getName(setting.language2, lastNameL2, firstNameL2)
		};
		return getTransFieldValue(language, valueMap);
	}

	/**
	 * ログインユーザの言語に対応するマスタデータの指定項目の値を設定する
	 * @param valueL0 言語0(デフォルト)の値
	 * @param valueL1 言語1の値
	 * @param valueL2 言語2の値
	 * @return 言語と項目に該当する値
	 */
	public static String getTransFieldValue(String valueL0, String valueL1, String valueL2) {
		return getTransFieldValue(userInfo.getLanguage(), valueL0, valueL1, valueL2);
	}

	/**
	 * 指定された言語に対応するマスタデータの指定項目の値を設定する
	 * @param language 表示言語
	 * @param valueL0 言語0(デフォルト)の値
	 * @param valueL1 言語1の値
	 * @param valueL2 言語2の値
	 * @return 言語と項目に該当する値
	 */
	public static String getTransFieldValue(String language,
												String valueL0, String valueL1, String valueL2) {
		Map<String, String> valueMap = new Map<String, String> {
			ComConst.LANG_TYPE_BASE => valueL0,
			ComConst.LANG_TYPE_EXT1 => valueL1,
			ComConst.LANG_TYPE_EXT2 => valueL2
		};
		return getTransFieldValue(language, valueMap);
	}

	/**
	 * 指定された言語に対応するマスタデータの指定項目の値を取得する
	 * @param language 表示言語
	 * @param valueMap 言語マップ
	 * @return 言語と項目に該当する値
	 */
	private static String getTransFieldValue(String language, Map<String, String> valueMap) {
		// 言語タイプを取得
		String langType = getLangType(language);
		// 存在しない場合は基準言語タイプで返す
		if (!valueMap.containsKey(langType)) {
			langType = ComConst.LANG_TYPE_BASE;
		}
		// 値が入力されていない場合は基準言語タイプで返す
		if (String.isBlank(valueMap.get(langType))) {
			langType = ComConst.LANG_TYPE_BASE;
		}
		return String.isBlank(valueMap.get(langType)) ? '' : valueMap.get(langType);
	}

	/**
	 * 会社言語設定から指定された言語と一致する言語インデックスを取得
	 * @param language ユーザ情報
	 * @return 言語タイプ
	 */
	public static String getLangType(String language) {
		ComDto.OrgSettingDto setting = ComLogic.getOrgSetting();
		// 一致している言語を取得する
		for (String langType : setting.languageMap.keyset()) {
			String lang = setting.languageMap.get(langType);
			if(lang == language) {
				return langType;
			}
		}
		// その他の言語の場合、基準言語タイプを返す
		return ComConst.LANG_TYPE_BASE;
	}

	/**
	 * 姓と名、及び言語を指定して名前を取得
	 * @param language 表示言語
	 * @param lastName 姓
	 * @param firstName 名
	 * @return 名前（姓と名の間に半角スペースを入れて返す、姓と名が空白の場合空白を返す）
	 */
	public static String getName(String language, String lastName, String firstName) {
		// 成型
		lastName = String.isBlank(lastName) ? '' : lastName.trim();
		firstName = String.isBlank(firstName) ? '' : firstName.trim();
		// 姓と名が入力されているか
		if (String.isBlank(lastName) && String.isBlank(firstName)) {
			return '';
		}
		// 姓名表記であるか
		if (isLastFirstName(language)) {
			return (lastName + ' ' + firstName).trim();
		}
		return (firstName + ' ' + lastName).trim();
	}

	/**
	 * 指定された言語は姓名表記であるか
	 * @param language 表示言語
	 * @return 姓名表記である
	 */
	public static Boolean isLastFirstName(String language) {
		// NOTE: 姓名表記が増えた場合、ここに条件を追記
		return language == ComConst.LANG_JA;
	}

	/**
	 * テスト用Exceptionを発行する
	 * @param testExcetpion ComTestException
	 */
	public static void throwTestException(ComTestException testException) {
		if (Test.isRunningTest()) {
			throw testException;
		}
	}


	/**
	 * @description ユーザ言語と一致する組織の言語設定のキー(0〜2)を取得する
	 * @return 言語キー(0〜2のいずれか)
	 */
	public static Integer getUserLanguageKey() {
		return getLanguageKey(UserInfo.getLanguage());
	}

	/**
	 * @description 指定した言語と一致する組織の言語設定のキー(0〜2)を取得する
	 *              (TODO)リファクタリング
	 * @return 言語キー(0〜2のいずれか)
	 */
	public static Integer getLanguageKey(String targetLang) {
		// 組織設定が存在しないなど、デフォルト状態では0を返す
		// 必要に応じて、本関数の戻り値をキャッシュしても良い
		Integer ret = 0;

		List<ComOrgSetting__c> orglist = [SELECT Language_0__c, Language_1__c, Language_2__c
			FROM ComOrgSetting__c
			ORDER BY CreatedDate
			LIMIT 1];

		// ユーザ言語が組織の言語L0〜L2のいずれかにマッチする場合は0〜2を返す
		if (orglist.size() > 0) {
			ComOrgSetting__c org = orglist[0];
			Map<String, Integer> langIndexMap = new Map<String, Integer> {
				org.Language_0__c => 0,
				org.Language_1__c => 1,
				org.Language_2__c => 2
			};
			if (langIndexMap.containsKey(targetLang)) {
				ret = langIndexMap.get(targetLang);
			}
		}

		return ret;
	}

	/*
	 * Check if the 2 provided objects are equal or not in a null safe manner.
	 * This handles the either-one-is-null comparison and if calls and return the {@link equals} method result.
	 * @param o1 Object 1
	 * @param o2 Object 2
	 * @return true if the 2 objects are the same same
	 */
	public static Boolean isEqual(Object o1, Object o2) {
		if (o1 === o2) {
			return true;
		} else if (checkIfOnlyOneIsNull(o1, o2)) {
			return false;
		}
		return o1.equals(o2);
	}

	/*
	 * Check if only one of the objects is null.
	 * @param o1 Object 1
	 * @param o2 Object 2
	 * @return true if only one of the o1 or o2 is null, and the other isn't
	 */
	public static Boolean checkIfOnlyOneIsNull(Object o1, Object o2) {
		return (((o1 == null) && (o2 != null)) ||
			((o1 != null) && (o2 == null)));
	}
}
