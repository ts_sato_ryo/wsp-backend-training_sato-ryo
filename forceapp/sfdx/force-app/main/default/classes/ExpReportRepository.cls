/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 経費精算のリポジトリ
 */
public with sharing class ExpReportRepository extends Repository.BaseRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** Number of Extended Items for each type */
	private static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/** String representation of Extended Item Field of the sObject */
	private static final String EXTENDED_ITEM_TEXT = ExpReport__c.ExtendedItemText01Value__c.getDescribe().getName().substringBefore('01');
	private static final String EXTENDED_ITEM_PICKLIST = ExpReport__c.ExtendedItemPicklist01Value__c.getDescribe().getName().substringBefore('01');
	private static final String EXTENDED_ITEM_LOOKUP = ExpReport__c.ExtendedItemLookup01Value__c.getDescribe().getName().substringBefore('01');
	private static final String EXTENDED_ITEM_DATE = ExpReport__c.ExtendedItemDate01Value__c.getDescribe().getName().substringBefore('01');

	private static final String REPORT_TYPE_EXTENDED_ITEM_TEXT = ExpReportType__c.ExtendedItemText01TextId__c.getDescribe().getName().substringBefore('01');
	private static final String REPORT_TYPE_EXTENDED_ITEM_PICKLIST = ExpReportType__c.ExtendedItemPicklist01TextId__c.getDescribe().getName().substringBefore('01');
	private static final String REPORT_TYPE_EXTENDED_ITEM_LOOKUP = ExpReportType__c.ExtendedItemLookup01TextId__c.getDescribe().getName().substringBefore('01');
	private static final String REPORT_TYPE_EXTENDED_ITEM_DATE = ExpReportType__c.ExtendedItemDate01TextId__c.getDescribe().getName().substringBefore('01');

	private static final String EXTENDED_ITEM_REQUIRED_FOR = 'RequiredFor__c';
	private static final String EXTENDED_ITEM_LOOKUP_ID = 'Id__c';
	private static final String EXTENDED_ITEM_ID = 'TextId__c';
	private static final String EXTENDED_ITEM_Value = 'Value__c';

	// Relationships
	private static final String REQUEST_R = ExpReport__c.ExpRequestId__c.getDescribe().getRelationshipName();
	private static final String DEPT_HISTORY_R = ExpReport__c.DepartmentHistoryId__c.getDescribe().getRelationshipName();
	private static final String DEPT_BASE_R = ComDeptHistory__c.BaseId__c.getDescribe().getRelationshipName();
	private static final String PRE_REQUEST_R = ExpReport__c.ExpPreRequestId__c.getDescribe().getRelationshipName();
	private static final String JOB_R = ExpReport__c.JobId__c.getDescribe().getRelationshipName();
	private static final String COST_CENTER_HISTORY_R = ExpReport__c.CostCenterHistoryId__c.getDescribe().getRelationshipName();
	private static final String COST_CENTER_BASE_R = ComCostCenterHistory__c.BaseId__c.getDescribe().getRelationshipName();
	private static final String EMP_HISTORY_R = ExpReport__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	private static final String EMP_BASE_R = ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();
	private static final String EXP_REPORT_TYPE_R = ExpReport__c.ExpReportTypeId__c.getDescribe().getRelationshipName();
	private static final String EXP_VENDOR_R = ExpReport__c.VendorId__c.getDescribe().getRelationshipName();

	/*
	 * Field Selector to specify which column groups to retrieve from the DB
	 */
	public enum FieldSelector {
		REPORT_BASIC,
		REPORT_DETAIL,
		PRE_REQUEST_DETAIL,
		REQUEST_BASIC,
		REQUEST_DETAIL
	}

	/*
	 * First level relationship columns of Requests
	 */
	private static final List<String> REQUEST_BASIC_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpReportRequest__c.Status__c,
			ExpReportRequest__c.CancelType__c,
			ExpReportRequest__c.ConfirmationRequired__c,
			ExpReportRequest__c.RequestTime__c
		};
		REQUEST_BASIC_FIELD_LIST = Repository.generateFieldNameList(REQUEST_R, fieldList);
	}

	/*
	 * Following set of Request Columns are included:
	 * - Department History
	 * - Department Base
	 */
	private static final List<String> REQUEST_DETAIL_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> requestDepartmentHistoryFields = new Set<Schema.SObjectField> {
			ComDeptHistory__c.Name_L0__c,
			ComDeptHistory__c.Name_L1__c,
			ComDeptHistory__c.Name_L2__c
		};
		REQUEST_DETAIL_FIELD_LIST = Repository.generateFieldNameList(REQUEST_R + '.' + DEPT_HISTORY_R, requestDepartmentHistoryFields);

		final Set<Schema.SObjectField> requestDepartmentBaseFields = new Set<Schema.SObjectField> {
			ComDeptBase__c.Code__c
		};
		REQUEST_DETAIL_FIELD_LIST.addAll(Repository.generateFieldNameList(
			REQUEST_R + '.' + DEPT_HISTORY_R + '.' + DEPT_BASE_R, requestDepartmentBaseFields));
	}

	/*
	 * Pre Request columns
	 */
	private static final List<String> PRE_REQUEST_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpRequest__c.RequestNo__c,
			ExpRequest__c.Subject__c,
			ExpRequest__c.Purpose__c,
			ExpRequest__c.TotalAmount__c
		};
		PRE_REQUEST_FIELD_LIST = Repository.generateFieldNameList(PRE_REQUEST_R, fieldList);
	}

	/*
	 * Job Columns
	 */
	private static final List<String> JOB_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ComJob__c.Name_L0__c,
			ComJob__c.Name_L1__c,
			ComJob__c.Name_L2__c,
			ComJob__c.Code__c
		};
		JOB_FIELD_LIST = Repository.generateFieldNameList(JOB_R, fieldList);
	}

	/*
	 * Cost Center History and Base Columns
	 */
	private static final List<String> COST_CENTER_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> costCenterHistoryFields = new Set<Schema.SObjectField> {
			ComCostCenterHistory__c.Name_L0__c,
			ComCostCenterHistory__c.Name_L1__c,
			ComCostCenterHistory__c.Name_L2__c,
			ComCostCenterHistory__c.LinkageCode__c
		};
		COST_CENTER_FIELD_LIST = Repository.generateFieldNameList(COST_CENTER_HISTORY_R, costCenterHistoryFields);

		final Set<Schema.SObjectField> costCenterBaseFields = new Set<Schema.SObjectField> {
			ComCostCenterBase__c.Code__c
		};
		COST_CENTER_FIELD_LIST.addAll(Repository.generateFieldNameList(COST_CENTER_HISTORY_R + '.' + COST_CENTER_BASE_R,
				costCenterBaseFields));
	}

	private static final List<String> EMP_BASE_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> historyFields = new Set<Schema.SObjectField>{
			ComEmpHistory__c.BaseId__c
		};
		EMP_BASE_FIELD_LIST = Repository.generateFieldNameList(EMP_HISTORY_R, historyFields);

		final Set<Schema.SObjectField> baseFields = new Set<Schema.SObjectField>{
			ComEmpBase__c.FirstName_L0__c,
			ComEmpBase__c.FirstName_L1__c,
			ComEmpBase__c.FirstName_L2__c,
			ComEmpBase__c.LastName_L0__c,
			ComEmpBase__c.LastName_L1__c,
			ComEmpBase__c.LastName_L2__c,
			ComEmpBase__c.Code__c,
			ComEmpBase__c.CompanyId__c
		};
		EMP_BASE_FIELD_LIST.addAll(Repository.generateFieldNameList(EMP_HISTORY_R + '.' + EMP_BASE_R, baseFields));
	}

	/*
	 * Columns from ExpReportType
	 */
	private static final List<String> REPORT_TYPE_FIELD_LIST;
	static {
		Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpReportType__c.Name_L0__c,
			ExpReportType__c.Name_L1__c,
			ExpReportType__c.Name_L2__c,
			ExpReportType__c.Code__c,
			ExpReportType__c.UseFileAttachment__c,
			ExpReportType__c.VendorUsedIn__c,
			ExpReportType__c.VendorRequiredFor__c
		};
		// Select the Extended Item Required For Fields
		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');
			fields.add(ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					REPORT_TYPE_EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR));
			fields.add(ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
							REPORT_TYPE_EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR));
			fields.add(ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
							REPORT_TYPE_EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR));
			fields.add(ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
							REPORT_TYPE_EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR));
		}

		REPORT_TYPE_FIELD_LIST = Repository.generateFieldNameList(EXP_REPORT_TYPE_R, fields);
	}

	/*
	 * Vendor Columns
	 */
	private static final List<String> VENDOR_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpVendor__c.Name_L0__c,
			ExpVendor__c.Name_L1__c,
			ExpVendor__c.Name_L2__c,
			ExpVendor__c.Code__c,
			ExpVendor__c.PaymentDueDateUsage__c
		};
		VENDOR_FIELD_LIST = Repository.generateFieldNameList(EXP_VENDOR_R, fields);
	}

	/*
	 * ExpReport overview information columns
	 */
	private static final List<String> REPORT_BASIC_FIELD_LIST;
	static {
		// NOTE: currently, mobile doesn't support Vendor yet, and hence ReportTypeId and VendorId are needed in the Basic,
		// so that it can be filtered out in the FE.
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpReport__c.Name,
			ExpReport__c.ExpRequestId__c,
			ExpReport__c.ExpPreRequestId__c,
			ExpReport__c.TotalAmount__c,
			ExpReport__c.ReportNo__c,
			ExpReport__c.Subject__c,
			ExpReport__c.ExpReportTypeId__c,
			ExpReport__c.MobileIncompatibleCount__c,
			ExpReport__c.VendorId__c
		};
		REPORT_BASIC_FIELD_LIST = Repository.generateFieldNameList(fields);
	}

	/*
	 * ExpReport Detail columns excluding the relationship and extended items
	 */
	private static final List<String> REPORT_DETAIL_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpReport__c.AccountingDate__c,
			ExpReport__c.AccountingPeriodId__c,
			ExpReport__c.Prefix__c,
			ExpReport__c.SequenceNo__c,
			ExpReport__c.DepartmentHistoryId__c,
			ExpReport__c.EmployeeHistoryId__c,
			ExpReport__c.Remarks__c,
			ExpReport__c.JobId__c,
			ExpReport__c.PaymentDueDate__c,
			ExpReport__c.CostCenterHistoryId__c,
			ExpReport__c.LastModifiedDate
		};
		REPORT_DETAIL_FIELD_LIST = Repository.generateFieldNameList(fields);
	}

	/*
	 * Extended Items Columns
	 */
	private static final List<String> EXTENDED_ITEM_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> idFields = new Set<Schema.SObjectField>();
		final Set<Schema.SObjectField> lookupIdFields = new Set<Schema.SObjectField>();
		final Set<Schema.SObjectField> valueFields = new Set<Schema.SObjectField>();
		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			// Get Extended Item IDs from Report Type - Lookup is retrieved directly from Report
			idFields.add(ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					REPORT_TYPE_EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID));
			idFields.add(ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					REPORT_TYPE_EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID));
			idFields.add(ExpReportType__c.sObjectType.getDescribe().fields.getMap().get(
					REPORT_TYPE_EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID));
			lookupIdFields.add(ExpReport__c.sObjectType.getDescribe().fields.getMap().get(
					EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_LOOKUP_ID));

			// Get Extended Item Value from ExpReport
			valueFields.add(ExpReport__c.sObjectType.getDescribe().fields.getMap().get(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_Value));
			valueFields.add(ExpReport__c.sObjectType.getDescribe().fields.getMap().get(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_Value));
			valueFields.add(ExpReport__c.sObjectType.getDescribe().fields.getMap().get(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_Value));
			valueFields.add(ExpReport__c.sObjectType.getDescribe().fields.getMap().get(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_Value));
		}
		EXTENDED_ITEM_FIELD_LIST = Repository.generateFieldNameList(valueFields);
		EXTENDED_ITEM_FIELD_LIST.addAll(Repository.generateFieldNameList(lookupIdFields));
		EXTENDED_ITEM_FIELD_LIST.addAll(Repository.generateFieldNameList(EXP_REPORT_TYPE_R, idFields));
	}

	/** 検索フィルタ */
	private class Filter {
		/** 経費精算ID */
		public List<Id> ids;
		/** 社員ベースID */
		public List<Id> empIds;
		/** 経費申請タイプID */
		public List<Id> expReportTypeIds;
		/** 会社ID */
		public List<Id> companyIds;
		/** Extended item ID */
		public Id extendedItemId;
		/** 経費申請ID */
		public Id requestId;
		/** Accounting Period Id */
		public Id accountingPeriodId;
		/** Status Set */
		public Set<String> statusSet;
	}

	/**
	 * 経費精算エンティティを取得する
	 * @param reportId 経費精算ID
	 * @return 経費精算エンティティ
	 */
	public ExpReportEntity getEntity(Id reportId) {
		// 検索実行
		List<ExpReportEntity> entityList = getEntityList(new List<Id>{reportId});

		ExpReportEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * 経費精算エンティティを取得する
	 * @param requestId 経費申請ID
	 * @return 経費精算エンティティ
	 */
	public ExpReportEntity getEntityByRequestId(Id requestId) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.requestId = requestId;

		// 検索を実行する
		final Boolean forUpdate = false;

		List<ExpReportEntity> entityList = searchEntity(filter, forUpdate);

		ExpReportEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * 経費精算エンティティのリストを取得する
	 * @param reportIds 経費精算ID
	 * @return 条件に一致した経費精算エンティティのリスト
	 */
	public List<ExpReportEntity> getEntityList(List<Id> reportIds) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.ids = reportIds;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate);
	}

	/**
	 * Get a list of ExpReportEntity for the given Employee Ids.
	 * The columns to retrieved can be specified by fieldsToSelect.
	 * @param empIds 社員ベースID
	 * @param fieldsToSelect Set of FieldSelector to specify which columns to Retrieve from DB
	 * @param isApproved true for getting only approved list which is used in approved tab in expense-pc. false for getting not approved list which is used in .
	 * @return id list for pagination
	 */
	public List<ExpReportEntity> getEntityListByEmpId(List<Id> empIds, Set<FieldSelector> fieldsToSelect, Boolean isApproved) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.empIds = empIds;
		if(isApproved == true){
			filter.statusSet = new Set<String>{
				AppRequestStatus.APPROVED.toString()
			};
		} else if (isApproved == false){
			filter.statusSet = new Set<String>{
				AppRequestStatus.DISABLED.toString(),
				AppRequestStatus.PENDING.toString(),
				AppRequestStatus.REAPPLYING.toString(),
				null
			};
		}
		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate, SEARCH_RECORDS_NUMBER_MAX, fieldsToSelect);
	}

	/**
	 * Get Entity List By Report Id List
	 * @param empIds
	 * @param fieldsToSelect
	 * @return result of search entity
	 */
	public List<ExpReportEntity> getEntityListByReportIdList(Set<FieldSelector> fieldsToSelect, List<Id> reportIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.ids = reportIdList;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate, SEARCH_RECORDS_NUMBER_MAX, fieldsToSelect);
	}

	/**
	 * 経費精算エンティティのリストを取得する
	 * @param reportTypeId 経費申請タイプID
	 * @return 条件に一致した経費精算エンティティのリスト
	 */
	public List<ExpReportEntity> getEntityListByExpReportTypeId(Id reportTypeId) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.expReportTypeIds = new List<Id>{reportTypeId};

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate);
	}

	/*
	 * Retrieve list of ExpReports which uses the provided Accounting Period ID.
	 * @param accountingPeriodId
	 * @param limitNumber if null or less than 0, maximum record number will be returned.
	 *
	 * @return List<ExpReportEntity> list of ExpReportEntity which uses the provided {@code accountingPeriodId}
	 */
	public List<ExpReportEntity> getEntityListByAccountingPeriodId(Id accountingPeriodId, Integer limitNumber) {
		Filter filter = new Filter();
		filter.accountingPeriodId = accountingPeriodId;

		// 検索を実行する
		final Boolean forUpdate = false;
		limitNumber = (limitNumber == null || limitNumber <= 0) ? SEARCH_RECORDS_NUMBER_MAX: limitNumber;
		return searchEntity(filter, forUpdate, limitNumber, null);
	}

	/**
	 * Get a list of expense record items
	 * @param extendedItemId  Extended item ID
	 * @return List of expense record item entities searched by the condition
	 */
	public List<ExpReportEntity> getEntityListByExtendedItemId(Id extendedItemId) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.extendedItemId = extendedItemId;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(ExpReportEntity entity) {
		return saveEntityList(new List<ExpReportEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<ExpReportEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<ExpReportEntity> entityList, Boolean allOrNone) {
		List<ExpReport__c> objList = new List<ExpReport__c>();

		// エンティティからSObjectを作成する
		for (ExpReportEntity entity : entityList) {
			ExpReport__c obj = createObj(entity);
			objList.add(obj);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(objList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 経費精算レポートエンティティが存在するかチェックする。/ check if expense report entities exist or not
	 * @param companyId 会社Id / id of the company
	 * @return boolean （存在する場合はtrue/しない場合はfalse） / if exist true. not false.
	 */
	public Boolean checkExistEntity(Id companyId){
		Filter filter = new Filter();
		List<Id> companyIds = new List<Id>();
		companyIds.add(companyId);
		filter.companyIds = companyIds;
		List<ExpReportEntity> entityList = new List<ExpReportEntity>();
		entityList = searchEntity(filter, false, 1, new Set<FieldSelector>{FieldSelector.REPORT_BASIC});
		return (entityList.size() > 0);
	}

	/**
	 * 経費精算を検索する / search expense report
	 * @param filter 検索条件 / search conditions
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる) / true if search for update(will be locked)
	 * @return 条件に一致した経費精算エンティティのリスト / List of expense report which meet the condition
	 */
	private List<ExpReportEntity> searchEntity(Filter filter, Boolean forUpdate) {
		return searchEntity(filter, forUpdate, SEARCH_RECORDS_NUMBER_MAX, null);
	}

	/**
	 * 経費精算を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param limitNumber maximum number of Records to retrieve
	 * @param fieldsToSelect Set of FieldSelector to specify which columns to retrieve. If null, all columns will be retrieved. If empty set, only ID will be retrieved.
	 * @return 条件に一致した経費精算エンティティのリスト
	 */
	private List<ExpReportEntity> searchEntity(Filter filter, Boolean forUpdate, Integer limitNumber, Set<FieldSelector> fieldsToSelect) {
		// SELECT対象項目リスト
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(ExpReport__c.Id));

		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REPORT_BASIC)) {
			selectFldList.addAll(REPORT_BASIC_FIELD_LIST);
			// NOTE: Currently, Mobile doesn't support Vendor yet. And the below information is used to filter out Reports with Vendor in Mobile.
			selectFldList.addAll(REPORT_TYPE_FIELD_LIST);
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REPORT_DETAIL)) {
			selectFldList.addAll(REPORT_DETAIL_FIELD_LIST);

			// 関連項目（ジョブ）JOB
			selectFldList.addAll(JOB_FIELD_LIST);

			// Cost Center
			selectFldList.addAll(COST_CENTER_FIELD_LIST);

			// Extended Items
			selectFldList.addAll(EXTENDED_ITEM_FIELD_LIST);

			// Employee History Base
			selectFldList.addAll(EMP_BASE_FIELD_LIST);

			selectFldList.addAll(VENDOR_FIELD_LIST);
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REQUEST_BASIC)) {
			selectFldList.addAll(REQUEST_BASIC_FIELD_LIST);
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REQUEST_DETAIL)) {
			selectFldList.addAll(REQUEST_DETAIL_FIELD_LIST);
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.PRE_REQUEST_DETAIL)) {
			selectFldList.addAll(PRE_REQUEST_FIELD_LIST);
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> ids = filter.ids;
		final List<Id> empIds = filter.empIds;
		final List<Id> companyIds = filter.companyIds;
		final List<Id> expReportTypeIds = filter.expReportTypeIds;
		final Id requestId = filter.requestId;
		final Id accountingPeriodId = filter.accountingPeriodId;
		final Set<String> statusSet = filter.statusSet;
		if (ids != null && !ids.isEmpty()) {
			condList.add(getFieldName(ExpReport__c.Id) + ' IN :ids');
		}
		if (empIds != null && !empIds.isEmpty()) {
			condList.add(getFieldName(EMP_HISTORY_R, EMP_BASE_R, ComEmpBase__c.Id) + ' IN :empIds');
		}
		if (companyIds != null && !companyIds.isEmpty()) {
			condList.add(getFieldName(EMP_HISTORY_R, EMP_BASE_R, ComEmpBase__c.CompanyId__c) + ' IN :companyIds');
		}
		if (expReportTypeIds != null && !expReportTypeIds.isEmpty()) {
			condList.add(getFieldName(ExpReport__c.ExpReportTypeId__c) + ' IN :expReportTypeIds');
		}
		if (requestId != null) {
			condList.add(getFieldName(ExpReport__c.ExpRequestId__c) + ' = :requestId');
		}
		if (accountingPeriodId != null) {
			condList.add(getFieldName(ExpReport__c.AccountingPeriodId__c) + ' = :accountingPeriodId');
		}
		if (statusSet != null) {
			condList.add(getFieldName(REQUEST_R, ExpReportRequest__c.Status__c) + ' IN :statusSet');
		}
		// Search by extended item ID
		final Id pExtendedItemId;
		if (filter.extendedItemId != null) {
			pExtendedItemId = filter.extendedItemId;
			condList.add('(' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate01TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate02TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate03TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate04TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate05TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate06TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate07TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate08TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate09TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemDate10TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText01TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText02TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText03TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText04TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText05TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText06TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText07TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText08TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText09TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemText10TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup01Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup02Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup03Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup04Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup05Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup06Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup07Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup08Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup09Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(ExpReport__c.ExtendedItemLookup10Id__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist01TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist02TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist03TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist04TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist05TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist06TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist07TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist08TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist09TextId__c) + ' = :pExtendedItemId OR ' +
				getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.ExtendedItemPicklist10TextId__c) + ' = :pExtendedItemId)');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// SOQLを作成する
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ExpReport__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		soql += ' ORDER BY ' + getFieldName(ExpReport__c.SequenceNo__c) + ' Desc, CreatedDate Desc'
				+ ' LIMIT :limitNumber';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('ExpReportRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<ExpReportEntity> entityList = new List<ExpReportEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ExpReport__c) sobj, fieldsToSelect));
		}

		return entityList;
	}

	/**
	 * ExpReport__cオブジェクトからExpReportEntityを作成する
	 * @param obj 作成元のExpReport__cオブジェクト
	 * @param fieldsToSelect specify which fields were retrieved from the Database
	 * @return 作成したExpReportEntity
	 */
	private ExpReportEntity createEntity(ExpReport__c obj, Set<FieldSelector> fieldsToSelect) {
		ExpReportEntity entity = new ExpReportEntity();
		entity.setId(obj.Id);

		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REPORT_BASIC)) {
			entity.name = obj.Name;
			entity.expRequestId = obj.ExpRequestId__c;
			entity.expPreRequestId = obj.ExpPreRequestId__c;
			entity.totalAmount = obj.TotalAmount__c;
			entity.reportNo = obj.ReportNo__c;
			entity.subject = obj.Subject__c;
			entity.expReportTypeId = obj.ExpReportTypeId__c;
			entity.expReportTypeNameL = new AppMultiString(
					obj.ExpReportTypeId__r.Name_L0__c,
					obj.ExpReportTypeId__r.Name_L1__c,
					obj.ExpReportTypeId__r.Name_L2__c);
			entity.expReportTypeCode = obj.ExpReportTypeId__r.Code__c;
			entity.mobileIncompatibleCount = obj.MobileIncompatibleCount__c;
			entity.useFileAttachment = obj.ExpReportTypeId__r.UseFileAttachment__c;
			entity.vendorId = obj.VendorId__c;
			entity.vendorUsedIn = ExpVendorUsedIn.valueOf(obj.ExpReportTypeId__r.VendorUsedIn__c);
			entity.vendorRequiredFor = ExpVendorRequiredFor.valueOf(obj.ExpReportTypeId__r.VendorRequiredFor__c);

			// Set Extended Item Required For Fields
			if (obj.ExpReportTypeId__c != null) {
				// Only if using ExpReportType, then need to populate.
				for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
					String index = String.valueOf(i + 1).leftPad(2, '0');
					entity.setExtendedItemDateRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
							EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR)));
					entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
							EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR)));
					entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
							EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR)));
					entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) obj.ExpReportTypeId__r.get(
							EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR)));
				}
			}
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REPORT_DETAIL)) {
			entity.accountingDate = AppDate.valueOf(obj.AccountingDate__c);
			entity.accountingPeriodId = obj.AccountingPeriodId__c;
			entity.prefix = obj.Prefix__c;
			entity.sequenceNo = Integer.valueOf(obj.SequenceNo__c);
			entity.departmentHistoryId = obj.DepartmentHistoryId__c;

			entity.employeeHistoryId = obj.EmployeeHistoryId__c;
			entity.employeeBaseId = obj.EmployeeHistoryId__r.BaseId__c;
			entity.employeeNameL = new AppPersonName(
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c, obj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c, obj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c, obj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c);
			entity.employeeCode = obj.EmployeeHistoryId__r.BaseId__r.Code__c;
			entity.companyId = obj.EmployeeHistoryId__r.BaseId__r.CompanyId__c;
			entity.remarks = obj.Remarks__c;
			entity.jobId = obj.jobId__c;
			entity.jobNameL = new AppMultiString(
					obj.JobId__r.Name_L0__c,
					obj.JobId__r.Name_L1__c,
					obj.JobId__r.Name_L2__c);
			entity.jobCode = obj.JobId__r.Code__c;
			entity.vendorNameL = new AppMultiString(
					obj.VendorId__r.Name_L0__c,
					obj.VendorId__r.Name_L1__c,
					obj.VendorId__r.Name_L2__c);
			entity.vendorCode = obj.VendorId__r.Code__c;
			entity.vendorPaymentDueDateUsage = ExpVendorPaymentDueDateUsage.valueOf(obj.VendorId__r.PaymentDueDateUsage__c);
			entity.paymentDueDate = AppDate.valueOf(obj.PaymentDueDate__c);

			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');

				// ID
				entity.setExtendedItemDateId(i, (Id) obj.ExpReportTypeId__r.get(REPORT_TYPE_EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID));
				entity.setExtendedItemTextId(i, (Id) obj.ExpReportTypeId__r.get(REPORT_TYPE_EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID));
				entity.setExtendedItemLookupId(i, (Id) obj.get(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_LOOKUP_ID));
				entity.setExtendedItemPicklistId(i, (Id) obj.ExpReportTypeId__r.get(REPORT_TYPE_EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID));

				// Value
				entity.setExtendedItemDateValue(i, (Date) obj.get(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_Value));
				entity.setExtendedItemTextValue(i, (String) obj.get(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_Value));
				entity.setExtendedItemLookupValue(i, (String) obj.get(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_Value));
				entity.setExtendedItemPicklistValue(i, (String) obj.get(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_Value));
			}

			entity.costCenterHistoryId = obj.CostCenterHistoryId__c;
			entity.costCenterNameL = new AppMultiString(
					obj.CostCenterHistoryId__r.Name_L0__c,
					obj.CostCenterHistoryId__r.Name_L1__c,
					obj.CostCenterHistoryId__r.Name_L2__c);
			entity.costCenterLinkageCode = obj.CostCenterHistoryId__r.LinkageCode__c;
			entity.costCenterCode = obj.CostCenterHistoryId__r.BaseId__r.Code__c;
			entity.lastModifiedDate = new AppDatetime(obj.LastModifiedDate);
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.PRE_REQUEST_DETAIL)) {
			if (String.isNotBlank(entity.expPreRequestId)) {
				entity.expPreRequest = new ExpRequestEntity();
				entity.expPreRequest.setId(obj.ExpPreRequestId__c);
				entity.expPreRequest.requestNo = obj.ExpPreRequestId__r.RequestNo__c;
				entity.expPreRequest.subject = obj.ExpPreRequestId__r.Subject__c;
				entity.expPreRequest.purpose = obj.ExpPreRequestId__r.Purpose__c;
				entity.expPreRequest.totalAmount = obj.ExpPreRequestId__r.TotalAmount__c;
			}
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REQUEST_BASIC)) {
			entity.expRequest = new ExpReportEntity.Request(
					AppRequestStatus.valueOf(obj.ExpRequestId__r.Status__c),
					AppCancelType.valueOf(obj.ExpRequestId__r.CancelType__c),
					obj.ExpRequestId__r.ConfirmationRequired__c,
					AppDatetime.valueOf(obj.ExpRequestId__r.RequestTime__c));
		}
		if (fieldsToSelect == null || fieldsToSelect.contains(FieldSelector.REQUEST_DETAIL)) {
			entity.departmentCode = obj.ExpRequestId__r.DepartmentHistoryId__r.BaseId__r.Code__c;
			entity.departmentNameL = new AppMultiString(
					obj.ExpRequestId__r.DepartmentHistoryId__r.Name_L0__c,
					obj.ExpRequestId__r.DepartmentHistoryId__r.Name_L1__c,
					obj.ExpRequestId__r.DepartmentHistoryId__r.Name_L2__c);
		}

		entity.resetChanged();
		return entity;
	}

	/**
	 * ExpReportEntityからExpReport__cオブジェクトを作成する
	 * @param entity 作成元のExpReportEntity
	 * @return 作成したExpReport__cオブジェクト
	 */
	private ExpReport__c createObj(ExpReportEntity entity) {
		ExpReport__c retObj = new ExpReport__c();

		retObj.Id = entity.id;
		if (entity.isChanged(ExpReport.ReportBaseField.NAME)) {
			retObj.Name = entity.name;
		}
		if (entity.isChanged(ExpReportEntity.Field.EXP_REQUEST_ID)) {
			retObj.ExpRequestId__c = entity.expRequestId;
		}
		if (entity.isChanged(ExpReportEntity.Field.EXP_PRE_REQUEST_ID)) {
			retObj.ExpPreRequestId__c = entity.expPreRequestId;
		}
		if (entity.isChanged(ExpReportEntity.Field.ACCOUNTING_DATE)) {
			retObj.AccountingDate__c = AppDate.convertDate(entity.accountingDate);
		}
		if (entity.isChanged(ExpReportEntity.Field.ACCOUNTING_PERIOD_ID)) {
			retObj.AccountingPeriodId__c = entity.accountingPeriodId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.TOTAL_AMOUNT)) {
			retObj.TotalAmount__c = entity.totalAmount;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.PREFIX)) {
			retObj.Prefix__c = entity.prefix;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.SEQUENCE_NO)) {
			retObj.SequenceNo__c = entity.sequenceNo;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.DEPARTMENT_HISTORY_ID)) {
			retObj.DepartmentHistoryId__c = entity.departmentHistoryId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.EMPLOYEE_HISTORY_ID)) {
			retObj.EmployeeHistoryId__c = entity.employeeHistoryId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.SUBJECT)) {
			retObj.Subject__c = entity.subject;
		}
		if (entity.isChanged(ExpReportEntity.Field.REMARKS)) {
			retObj.Remarks__c = entity.remarks;
		}
		if (entity.isChanged(ExpReportEntity.Field.MOBILE_INCOMPATIBLE_COUNT)) {
			retObj.MobileIncompatibleCount__c = entity.mobileIncompatibleCount;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.JOB_ID)) {
			retObj.JobId__c = entity.jobId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.EXP_REPORT_TYPE_ID)) {
			retObj.ExpReportTypeId__c = entity.expReportTypeId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.VENDOR_ID)) {
			retObj.VendorId__c = entity.vendorId;
		}
		if (entity.isChanged(ExpReport.ReportBaseField.PAYMENT_DUE_DATE)) {
			retObj.PaymentDueDate__c = AppDate.convertDate(entity.paymentDueDate);
		}
		if (entity.isChanged(ExpReport.ReportBaseField.COST_CENTER_HISTORY_ID)) {
			retObj.CostCenterHistoryId__c = entity.costCenterHistoryId;
		}

		Boolean extendedItemDateValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemDateValueList);
		Boolean extendedItemLookupIdChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemLookupIdList);
		Boolean extendedItemLookupValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemLookupValueList);
		Boolean extendedItemTextValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemTextValueList);
		Boolean extendedItemPicklistValueChanged = entity.isChanged(ExpExtendedItemEntity.ExtendedItemField.ExtendedItemPicklistValueList);

		//Extended Item Lookup: ID & Value fields
		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			if (extendedItemDateValueChanged) {
				retObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_Value, entity.getExtendedItemDateValueAsDate(i));
			}

			if (extendedItemLookupIdChanged) {
				retObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_LOOKUP_ID, entity.getExtendedItemLookupId(i));
			}
			if (extendedItemLookupValueChanged) {
				retObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_Value, entity.getExtendedItemLookupValue(i));
			}

			if (extendedItemTextValueChanged) {
				retObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_Value, entity.getExtendedItemTextValue(i));
			}

			if (extendedItemPicklistValueChanged) {
				retObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_Value, entity.getExtendedItemPicklistValue(i));
			}
		}

		return retObj;
	}

	// TODO: 以下は暫定処理。仕様がFixしたら作り直す。
	/**
	 * 経費精算の申請Noを採番する
	 * @return 採番した申請No
	 */
	public Decimal getNewSequenceNo() {
		Decimal seqNo = 0;

		for (ExpReport__c lastReport : [
				SELECT SequenceNo__c
				FROM ExpReport__c
				ORDER BY SequenceNo__c DESC NULLS LAST
				LIMIT 1])
		{
			if (lastReport.SequenceNo__c != null) {
				seqNo = lastReport.SequenceNo__c;
			}
		}

		return seqNo+1;
	}
}
