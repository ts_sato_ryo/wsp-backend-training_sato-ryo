/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group プランナー
 *
 * プランナーマスタ管理系APIの権限チェック機能を提供するクラス
 * プランナーマスタ管理系のResourceクラス以外からは呼び出さないでください
 */
public with sharing class PlannerConfigResourcePermission {

	/** API実行に必要な権限 */
	public enum Permission {
		/** プランナー機能設定の管理 */
		MANAGE_PLANNER_SETTING
	}

	/** 社員の権限サービス */
	private EmployeePermissionService permissionService;

	/**
	 * コンストラクタ
	 */
	public PlannerConfigResourcePermission() {
		this.permissionService = new EmployeePermissionService();
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(PlannerConfigResourcePermission.Permission requiredPermission) {
		hasExecutePermission(new List<PlannerConfigResourcePermission.Permission>{requiredPermission});
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(List<PlannerConfigResourcePermission.Permission> requiredPermissionList) {
		List<PlannerConfigResourcePermission.Permission> errorPermissionList =
				new List<PlannerConfigResourcePermission.Permission>();

		for (Permission requiredPermission : requiredPermissionList) {
			// プランナー機能設定の管理
			if (requiredPermission == Permission.MANAGE_PLANNER_SETTING) {
				if (! permissionService.hasManagePlannerSetting) {
					errorPermissionList.add(Permission.MANAGE_PLANNER_SETTING);
				}
			// Error! 実装ミス
			} else {
				throw new App.UnsupportedException('Unsupported Permission(' + requiredPermission + ')');
			}
		}

		if (! errorPermissionList.isEmpty()) {
			// デバッグ用にログを出力しておく
			System.debug('--- Insufficient permission for API=' + errorPermissionList);
			throw new App.NoPermissionException(ComMessage.msg().Com_Err_NoApiPermission);
		}
	}
}