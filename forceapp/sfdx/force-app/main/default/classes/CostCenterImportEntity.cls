/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通 Common
 *
 * コストセンターインポートのエンティティ Cost Center Import Entity
 */
public with sharing class CostCenterImportEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
 	public enum Field {
 		ImportBatchId,
 		Status,
 		Error
 	}

 	/** 値を変更した項目のリスト */
 	private Set<Field> isChangedFieldSet;

 	/**
	 * コンストラクタ
	 */
 	public CostCenterImportEntity() {
 		isChangedFieldSet = new Set<Field>();
 	}

 	/** インポートバッチID */
 	public Id importBatchId {
 		get;
 		set {
 			ImportBatchId = value;
 			setChanged(Field.ImportBatchId);
 		}
 	}

 	/** 処理ステータス */
	public ImportStatus status {
		get;
		set {
			status = value;
			setChanged(Field.Status);
		}
	}

	/** エラー情報 */
	public String error {
		get;
		set {
			error = value;
			setChanged(Field.Error);
		}
	}

	/** 改訂日 */
	public AppDate revisionDate {get; set;}
	/** コストセンターコード */
	public String code {get; set;}
	/** 会社コード */
	public String companyCode {get; set;}
	/** コストセンター名(L0) */
	public String name_L0 {get; set;}
	/** コストセンター名(L1) */
	public String name_L1 {get; set;}
	/** コストセンター名(L2) */
	public String name_L2 {get; set;}
	/** External System Linkage Code */
	public String linkageCode {get; set;}
	/** コストセンター署コード */
	public String parentBaseCode {get; set;}
	/** 履歴コメント */
	public String historyComment {get; set;}
	/** 有効開始日 */
	public AppDate validFrom {get; set;}
	/** 失効日 */
	public AppDate validTo {get; set;}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}

}