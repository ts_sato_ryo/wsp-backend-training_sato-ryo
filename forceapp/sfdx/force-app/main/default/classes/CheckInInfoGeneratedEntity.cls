/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 共通
  *
 * (廃止)チェックイン情報エンティティ
 * このエンティティは廃止になりました。ComLocationEntity を使用してください。
 * This entity has been deprecated. Use ComLocationEntity instead.
 */
public abstract with sharing class CheckInInfoGeneratedEntity {
}