/**
 * 勤怠日次時間
 * 日次で発生する可能性がある分で表した時間
 */
public with sharing class AttDailyTime extends ValueObject implements Comparable {

	/** 1時間あたりの分 */
	private static final Integer MINUTE_PER_HOUR = 60;

	/**
	 * 日次で発生する可能性がある分で表した時間(Hours)
	 */
	public Integer value {
		private get;
		private set {
			// 必要なら値を検証する
			this.value = value;
		}
	}

	/**
	 * Integer型の値をAttTimeに変換する
	 * @param Integer型の値
	 * @return 作成したAttTime
	 */
	public static AttDailyTime valueOf(Integer value) {
		if (value == null) {
			return null;
		}
		return new AttDailyTime(value);
	}

	/**
	 * Decimal型の値をAttTimeに変換する
	 * @param Decimal型の値
	 * @return 作成したAttTime
	 */
	public static AttDailyTime valueOf(Decimal value) {
		return valueOf(Integer.valueOf(value));
	}

	/**
	 * 2つのAttDailyTimeのうち大きい方を返す
	 * @param value1 比較対象の値1
	 * @param value2 比較対象の値2
	 * @return 大きい方の値
	 */
	public static AttDailyTime max(AttDailyTime value1, AttDailyTime value2) {
		if (value1.compareTo(value2) >= 1) {
			return value1;
		}
		return value2;
	}

	/**
	 * AttDailyTime型の値を現値に追加する
	 * FIXME: ValueObjectのメソッドなのに副作用がある
	 * @param 追加対象
	 */
	public void add(AttDailyTime targetValue) {
		if (value != null && targetValue != null) {
			value += targetValue.getIntValue();
		}
	}

	/**
	 * AttDailyTime型の値を加算し、結果を新しいオブジェクトとして返却する
	 * @param 追加対象
	 * @return 加算結果(新しいAttDailyTime)
	 */
	 public AttDailyTime sum(AttDailyTime targetValue) {
		 return new AttDailyTime(this.value + targetValue.getIntValue());
	 }

	/**
	 * コンストラクタ（分を指定）
	 * @param value その日の0:00を0とした分で表す時刻
	 */
	public AttDailyTime(Integer value) {
		this.value = value;
	}

	/**
	 * 値をInteger型に変換する
	 * @return Intger型に変換した値
	 */
	public static Integer convertInt(AttDailyTime attValue) {
		if (attValue == null) {
			return null;
		}
		return attValue.value;
	}

	/**
	 * 値をInteger型で取得する
	 * @return Intger型に変換した値
	 */
	public Integer getIntValue() {
		return value;
	}

	/**
	 * 値をDecimal型で取得する
	 * @return Decimal型に変換した値
	 */
	public Decimal getDecimalValue() {
		return Decimal.valueOf(value);
	}
	/**
	 * 時間単位に切上げ
	 * @return Integer 切上げした分単位の値
	 */
	public Integer getFitHourValue() {
		Integer hours = this.getIntValue() / 60;
		if (this.getIntValue() > hours * 60) {
			hours++;
		}
		return hours * 60;
	}
	/**
	 * 時間単位に切上げ
	 * @return Integer 切上げした分単位の値
	 */
	static public Integer getFitHourValue(Integer targetValue) {
		Integer hours = Integer.valueOf(targetValue/60);
		if (targetValue > hours * 60) {
			hours++;
		}
		return hours * 60;
	}

	/**
	 * hh:mmの形式の文字列で取得する(Hourも0埋め実施)
	 * @return hh:mm形式の文字列
	 */
	public String format() {
		Integer hours = Integer.valueOf(this.value / MINUTE_PER_HOUR);
		Integer minutes = this.value - (hours * MINUTE_PER_HOUR);

		String formatString = String.valueOf(hours).leftPad(2, '0') + ':'
				+ String.valueOf(minutes).leftPad(2, '0');

		return formatString;
	}

	/**
	 * h:mmの形式の文字列で取得する(Hourは0埋めしない)
	 * @return h:mm形式の文字列
	 */
	public String formatHmm() {
		Integer hours = Integer.valueOf(this.value / MINUTE_PER_HOUR);
		Integer minutes = this.value - (hours * MINUTE_PER_HOUR);

		String formatString = String.valueOf(hours) + ':' + String.valueOf(minutes).leftPad(2, '0');

		return formatString;
	}
	/**
	 * オブジェクトの値が等しいかどうか判定する
	 * @param compare 比較対象のオブジェクト
	 */
	public override Boolean equals(Object compare) {
		if (compare instanceof AttDailyTime) {
			return this.value == ((AttDailyTime)compare).value;
		}
		return false;
	}

	/**
	 * 比較の結果であるinteger値を返します
	 * @param 比較対象
	 * @return このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満
	 */
	public Integer compareTo(Object compareTo) {
		AttDailyTime compareTime = (AttDailyTime)compareTo;
		if (this.value == compareTime.value) {
			return 0;
		} else if (this.value > compareTime.value) {
			return 1;
		}
		// this.value < compareTime.value
		return -1;
	}

	/**
	 * (デバッグ用)値を文字列で返却する
	 */
	override public String toString() {
		return this.value.format();
	}
}