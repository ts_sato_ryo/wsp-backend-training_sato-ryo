/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分ベースエンティティを表すクラス Entity class for ExpTaxTypeBase object
 */
public with sharing class ExpTaxTypeBaseEntity extends ExpTaxTypeBaseGeneratedEntity {
	/** コードの最大文字数 Max length of the code */
	public static final Integer CODE_MAX_LENGTH = 20;

	public ExpTaxTypeBaseEntity() {
		super();
	}

	public ExpTaxTypeBaseEntity(ExpTaxTypeBase__c sobj) {
		super(sobj);
	}

	/** 履歴エンティティのリスト List of history entities */
	protected List<ExpTaxTypeHistoryEntity> historyList;

	/** Country 国 */
	public LookupField country {
		get {
			if (countryId == null) {
				return null;
			}
			if (country == null) {
				country = new ParentChildBaseEntity.LookupField(
						new AppMultiString(
							sobj.CountryId__r.Name_L0__c,
							sobj.CountryId__r.Name_L1__c,
							sobj.CountryId__r.Name_L2__c));
			}
			return country;
		}
		private set;
	}

	/**
	 * 履歴エンティティリストから指定したIDの履歴を取得する Get history entity of the specified ID
	 * @return 履歴エンティティ、ただし存在しない場合はnull History entity(If not exists, return null)
	 */
	public ExpTaxTypeHistoryEntity getHistoryById(Id historyId) {
		return (ExpTaxTypeHistoryEntity)super.getSuperHistoryById(historyId);
	}

	/**
	 * 履歴エンティティリストを取得する Get the list of history entities
	 * @return 履歴エンティティリスト List of history entities
	 */
	public List<ExpTaxTypeHistoryEntity> getHistoryList() {
		return historyList;
	}

	/**
	 * 履歴エンティティをリストに追加する Add history entity to the list
	 * @param history 追加する履歴エンティティ history entity
	 */
	 public void addHistory(ExpTaxTypeHistoryEntity history) {
		 if (historyList == null) {
		 	historyList = new List<ExpTaxTypeHistoryEntity>();
		 }
		 historyList.add(history);
	 }

	 /**
	  * 履歴エンティティを取得する Get history entity of the specified index
	  * @return 履歴エンティティ、ただし存在しない場合はnull History entity(If not exists, return null)
	  */
	 public ExpTaxTypeHistoryEntity getHistory(Integer index) {
		 if (historyList == null) {
			 return null;
		 }
		 return historyList[index];
	 }

	 /**
	  * 履歴の基底クラスに変換する Convert type of the history entity class to base class
	  */
	 protected override List<ParentChildHistoryEntity> convertSuperHistoryList() {
		 return (List<ParentChildHistoryEntity>)historyList;
	 }

	/**
	 * Create a Copy of CostCenterBaseEntity(ID is not copied.)
	 */
	public ExpTaxTypeBaseEntity copy() {
		ExpTaxTypeBaseEntity copyEntity = new ExpTaxTypeBaseEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 */
	public override ParentChildBaseEntity copySuperEntity() {
		return copy();
	}
}