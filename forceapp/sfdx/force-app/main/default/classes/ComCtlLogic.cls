/**
 * コントローラ用共通処理ロジック
 */
public with sharing class ComCtlLogic {

	// ロールバック用セーブポイント
	private static SavePoint ctlSavepoint = null;
	// 参照カウント
	private static Integer referenceCount = 0;
	// エラーメッセージテスト
	@testVisible private static Boolean isTestExecuteErrorMessage = false;

	/**
	 * コントローラ関数実行関数
	 */
	public static void executeControllerStart() {
		// エントリコントローラの場合、セーブポイントを作成
		if (referenceCount == 0) {
			ctlSavepoint = Database.setSavepoint();
		}
		// 参照カウントを増加する
		referenceCount++;
	}

	/**
	 * コントローラ関数例外関数
	 */
	public static void executeControllerThrow(Exception ex, BaseResultDto resultDto) {
		// 他コントローラからの呼び出しの場合、親コントローラにてcatchするため、throwする
		if (referenceCount > 1) {
			throw ex;
		}
		// テスト時にはExceptionとして返すためthrowさせる
		if (test.isRunningTest() && !isTestExecuteErrorMessage) {
			throw ex;
		}
		// エラーメッセージを格納
		resultDto.setErrorMessage(ex.getMessage());
		// エントリコントローラの場合、ロールバックする
		Database.rollback(ctlSavepoint);
	}

	/**
	 * コントローラ関数終了関数
	 */
	public static void executeControllerEnd() {
		// 参照カウントを削減する
		referenceCount--;
	}
}