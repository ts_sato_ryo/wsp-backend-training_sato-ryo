/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * TimeRequestRepositoryのテスト
 */
@isTest
private class TimeRequestRepositoryTest {

	/**
	 * getTimeRequestListをテストする
	 */
	@isTest static void getTimeRequestListTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summary = testData.createSummaryEntity();
		TimeRequestEntity request = testData.createRequestEntity(summary);

		TimeRequestRepository rep = new TimeRequestRepository();
		Test.startTest();
		List<TimeRequestEntity> requestList = rep.getTimeRequestList(new List<ID>{request.Id});
		Test.stopTest();

		System.assertEquals(1, requestList.size());
		System.assertEquals(request.name, requestList[0].name);
		System.assertEquals(summary.Id, requestList[0].timeSummaryId);
		System.assertEquals(request.status, requestList[0].status);
		System.assertEquals(request.isConfirmationRequired, requestList[0].isConfirmationRequired);
		System.assertEquals(request.cancelType, requestList[0].cancelType);
		System.assertEquals(request.comment, requestList[0].comment);
		System.assertEquals(request.departmentId, requestList[0].departmentId);
		System.assertEquals(request.actorId, requestList[0].actorId);
	}

	/**
	 * 申請日時の昇順で検索することを検証する
	 */
	@isTest static void getTimeRequestListSortedTest() {
		// テストデータ作成
		TimeTestData testData = new TimeTestData(AppDate.newInstance(2019, 4, 1));
		TimeSummary__c summary = testData.createMonthlySummary(AppDate.newInstance(2019, 4, 1));
		TimeRequest__c request1 = testData.createRequest(summary);
		TimeRequest__c request2 = testData.createRequest(summary);
		TimeRequest__c request3 = testData.createRequest(summary);
		request3.RequestTime__c = Datetime.newInstance(2019, 4, 1, 8, 59, 59);
		request1.RequestTime__c = Datetime.newInstance(2019, 4, 1, 9,  0,  0);
		request2.RequestTime__c = Datetime.newInstance(2019, 4, 2, 9,  0,  0);
		update new List<TimeRequest__c>{request1, request2, request3};
		
		// 検索
		TimeRequestRepository repo = new TimeRequestRepository();
		Test.startTest();
		List<TimeRequestEntity> requests = repo.getTimeRequestList(new List<Id>{request1.Id, request2.Id, request3.Id});
		Test.stopTest();

		// 日付の昇順に並んでいることを検証する
		System.assertEquals(3, requests.size());
		System.assertEquals(request3.Id, requests[0].id);
		System.assertEquals(request1.Id, requests[1].id);
		System.assertEquals(request2.Id, requests[2].id);
	}

	/**
	 * saveEntityをテストする
	 */
	@isTest static void SaveTimeRequestTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summary = testData.createSummaryEntity();

		TimeRequestEntity newRequest = new TimeRequestEntity();
		newRequest.name = 'test';
		newRequest.timeSummaryId = summary.Id;
		newRequest.status = AppRequestStatus.DISABLED;
		newRequest.isConfirmationRequired = true;
		newRequest.cancelType = AppCancelType.REJECTED;
		newRequest.comment = 'コメント';
		newRequest.employeeId = testData.emp.CurrentHistoryId__c;
		newRequest.departmentId = testData.dept.CurrentHistoryId__c;
		newRequest.actorId = testData.emp.CurrentHistoryId__c;

		TimeRequestRepository rep = new TimeRequestRepository();
		Test.startTest();
		Repository.SaveResult result = rep.saveEntity(newRequest);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);
		System.assertEquals(1, result.details.size());
		System.assertNotEquals(null, result.details[0].id);
		Id requestId = result.details[0].id;

		// 作成された申請データを取得
		TimeRequest__c request = [
				SELECT
					Id,
					Name,
					TimeSummaryId__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					Comment__c,
					EmployeeHistoryId__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					OwnerId
				FROM TimeRequest__c
				WHERE Id = :requestId
				LIMIT 1];

		System.assertEquals(newRequest.name, request.Name);
		System.assertEquals(newRequest.timeSummaryId, request.TimeSummaryId__c);
		System.assertEquals(newRequest.status.value, request.Status__c);
		System.assertEquals(newRequest.isConfirmationRequired, request.ConfirmationRequired__c);
		System.assertEquals(newRequest.cancelType.value, request.CancelType__c);
		System.assertEquals(newRequest.comment, request.Comment__c);
		System.assertEquals(newRequest.employeeId, request.EmployeeHistoryId__c);
		System.assertEquals(newRequest.departmentId, request.DepartmentHistoryId__c);
		System.assertEquals(newRequest.actorId, request.ActorHistoryId__c);
	}
}