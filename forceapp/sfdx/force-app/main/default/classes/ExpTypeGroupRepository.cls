/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * @description 費目グループマスタのリポジトリ
 */
public with sharing class ExpTypeGroupRepository extends ValidPeriodRepositoryOld {

	/**
	 * リストを取得する際の並び順
	 */
	public enum SortOrder {
		CODE_ASC, CODE_DESC, ORDER_ASC, ORDER_DESC
	}

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** 親費目グループのリレーション名 */
	private static final String PARENT_R = ExpTypeGroup__c.ParentId__c.getDescribe().getRelationshipName();
	/** 子リレーション名（費目グループ） */
	private static final String CHILD_RELATION_GROUPS = 'ExpTypeGroups__r';
	/** 子リレーション名（費目） */
	private static final String CHILD_RELATION_EXP_TYPES = 'ExpTypes__r';

	/**
	 * 取得対象の項目名
	 * 項目が追加されたらfieldListに追加してください
	 */
	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ExpTypeGroup__c.Id,
				ExpTypeGroup__c.Name,
				ExpTypeGroup__c.Code__c,
				ExpTypeGroup__c.UniqKey__c,
				ExpTypeGroup__c.Name_L0__c,
				ExpTypeGroup__c.Name_L1__c,
				ExpTypeGroup__c.Name_L2__c,
				ExpTypeGroup__c.CompanyId__c,
				ExpTypeGroup__c.ParentId__c,
				ExpTypeGroup__c.Description_L0__c,
				ExpTypeGroup__c.Description_L1__c,
				ExpTypeGroup__c.Description_L2__c,
				ExpTypeGroup__c.Order__c,
				ExpTypeGroup__c.ValidFrom__c,
				ExpTypeGroup__c.ValidTo__c};

		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** 取得対象の項目（子の費目グループ） */
	private static final Set<Schema.SObjectField> GET_CHILD_GROUP_FIELD_SET;
	static {
		GET_CHILD_GROUP_FIELD_SET = new Set<Schema.SObjectField> {
			ExpTypeGroup__c.Id
		};
	}

	/** 取得対象の項目（子の費目） */
	private static final Set<Schema.SObjectField> GET_CHILD_EXP_TYPE_FIELD_SET;
	static {
		GET_CHILD_EXP_TYPE_FIELD_SET = new Set<Schema.SObjectField> {
			ExpType__c.Id
		};
	}

	/**
	 * 指定したIDのエンティティを取得する
	 * @param id 取得対象のID
	 * @return 指定したIDのエンティティ、ただし存在しない場合はnull
	 */
	public ExpTypeGroupEntity getEntity(Id id) {
		List<ExpTypeGroupEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したエンティティを取得する
	 * @param ids 取得対象のIDのリスト
	 * @return 指定したIDのエンティティのリスト、ただし存在しない場合はnull
	 */
	public List<ExpTypeGroupEntity> getEntityList(List<Id> ids) {
		ExpTypeGroupRepository.SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 指定したコードを持つエンティティを取得する
	 * @param code 費目グループコード
	 * @param companyId 会社ID
	 * @return 指定したコードを持つエンティティ、存在しない場合はnull
	 */
	public ExpTypeGroupEntity getEntityByCode(String code, Id companyId) {
		ExpTypeGroupRepository.SearchFilter filter = new SearchFilter();
		filter.codes = new Set<String>{code};
		filter.companyIds = new Set<Id>{companyId};

		List<ExpTypeGroupEntity> entityList = searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定した親費目グループIDを持つエンティティを取得する
	 * @param parentIds 親費目グループIDのリスト
	 * @return 指定した親費目グループIDを持つエンティティ、存在しない場合はnull
	 */
	public List<ExpTypeGroupEntity> getEntityListByParentId(List<Id> parentIds) {
		ExpTypeGroupRepository.SearchFilter filter = new SearchFilter();
		filter.parentIds = parentIds == null ? null : new Set<Id>(parentIds);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.ORDER_ASC);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** 費目グループID */
		public Set<Id> ids;
		/** 会社ID */
		public Set<Id> companyIds;
		/** コード(完全一致) */
		public Set<String> codes;
		/** 取得対象日 */
		public AppDate targetDate;
		/** 親費目グループ未設定 */
		public Boolean hasNoParent;
		/** 親費目グループID */
		public Set<Id> parentIds;
	}

	/**
	 * 費目グループを検索する
	 * @param filter 検索条件
	 * @return 検索結果
	 */
	public List<ExpTypeGroupEntity> searchEntity(ExpTypeGroupRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 費目グループを検索する
	 * @param filter 検索条件
	 * @param order 並び順
	 * @return 検索結果
	 */
	public List<ExpTypeGroupEntity> searchEntity(ExpTypeGroupRepository.SearchFilter filter, SortOrder order) {
		return searchEntity(filter, NOT_FOR_UPDATE, order);
	}

	/**
	 * 費目グループを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param order 並び順
	 * @return 検索結果
	 */
	public List<ExpTypeGroupEntity> searchEntity(ExpTypeGroupRepository.SearchFilter filter, Boolean forUpdate, SortOrder order) {

		// WHERE句
		List<String> whereList = new List<String>();
		// 費目グループIDで検索
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ExpTypeGroup__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ExpTypeGroup__c.Code__c) + ' IN :pCodes');
		}
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if (filter.targetDate != null) {
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ExpTypeGroup__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ExpTypeGroup__c.ValidTo__c) + ' > :pTargetDate');
		}
		// 親費目グループが未設定のレコードを検索
		if (filter.hasNoParent != null && filter.hasNoParent == true) {
			whereList.add(getFieldName(ExpTypeGroup__c.ParentId__c) + ' = null');
		}
		// 親費目グループIDで検索
		Set<Id> pParentIds;
		if (filter.parentIds != null) {
			pParentIds = filter.parentIds;
			whereList.add(getFieldName(ExpTypeGroup__c.ParentId__c) + ' IN :pParentIds');
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY句
		String orderByString = ' ORDER BY ';
		if (order == SortOrder.CODE_ASC) {
			orderByString += getFieldName(ExpTypeGroup__c.Code__c) + ' ASC NULLS LAST';
		} else if (order == SortOrder.CODE_DESC) {
			orderByString += getFieldName(ExpTypeGroup__c.Code__c) + ' DESC NULLS LAST';
		} else if (order == SortOrder.ORDER_ASC) {
			orderByString += getFieldName(ExpTypeGroup__c.Order__c) + ' NULLS LAST, ' + getFieldName(ExpTypeGroup__c.Code__c) + ' ASC NULLS LAST';
		} else if (order == SortOrder.ORDER_DESC) {
			orderByString += getFieldName(ExpTypeGroup__c.Order__c) + ' NULLS LAST, ' + getFieldName(ExpTypeGroup__c.Code__c) + ' DESC NULLS LAST';
		}

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_FIELD_NAME_LIST);
		// 関連項目（親費目グループ）
		selectFieldList.add(getFieldName(PARENT_R, ExpTypeGroup__c.Name_L0__c));
		selectFieldList.add(getFieldName(PARENT_R, ExpTypeGroup__c.Name_L1__c));
		selectFieldList.add(getFieldName(PARENT_R, ExpTypeGroup__c.Name_L2__c));

		// 子の費目グループ
		List<String> childGroupFieldList = new List<String>();
		for (Schema.SObjectField field : GET_CHILD_GROUP_FIELD_SET) {
			childGroupFieldList.add(getFieldName(field));
		}
		selectFieldList.add(
				'(SELECT ' + String.join(childGroupFieldList, ',')
				+ ' FROM ' + CHILD_RELATION_GROUPS + ')');

		// 子の費目
		List<String> childExpTypeFieldList = new List<String>();
		for (Schema.SObjectField field : GET_CHILD_EXP_TYPE_FIELD_SET) {
			childExpTypeFieldList.add(getFieldName(field));
		}
		selectFieldList.add(
				'(SELECT ' + String.join(childExpTypeFieldList, ',')
				+ ' FROM ' + CHILD_RELATION_EXP_TYPES + ')');

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpTypeGroup__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderByString
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('ExpTypeGroupRepository: SOQL=' + soql);

		// クエリ実行
		List<ExpTypeGroup__c> sObjs = (List<ExpTypeGroup__c>)Database.query(soql);

		// SObjectからエンティティを作成
		List<ExpTypeGroupEntity> entities = new List<ExpTypeGroupEntity>();
		for (ExpTypeGroup__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntityOld> entityList) {

		List<ExpTypeGroup__c> objectList = new List<ExpTypeGroup__c>();
		for (ValidPeriodEntityOld entity : entityList) {
			objectList.add(createObject((ExpTypeGroupEntity)entity));
		}

		return objectList;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entity オブジェクト作成元のエンティティ
	 * @return 保存用のオブジェクト
	 */
	private ExpTypeGroup__c createObject(ExpTypeGroupEntity entity) {

		ExpTypeGroup__c sObj = new ExpTypeGroup__c();

		// 有効期間型共通の項目を設定する
		setValidPeriodEntityValueToObject(entity, sObj);

		if (entity.isChanged(ExpTypeGroupEntity.Field.CODE)) {
			sObj.Code__c = entity.code;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.NAME_L0)) {
			sObj.Name_L0__c = entity.nameL0;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.NAME_L1)) {
			sObj.Name_L1__c = entity.nameL1;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.NAME_L2)) {
			sObj.Name_L2__c = entity.nameL2;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.COMPANY_ID)) {
			sObj.CompanyId__c = entity.companyId;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.UNIQ_KEY)) {
			sObj.UniqKey__c = entity.uniqKey;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.PARENT_ID)) {
			sObj.ParentId__c = entity.parentId;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.DESCRIPTION_L0)) {
			sObj.Description_L0__c = entity.descriptionL0;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.DESCRIPTION_L1)) {
			sObj.Description_L1__c = entity.descriptionL1;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.DESCRIPTION_L2)) {
			sObj.Description_L2__c = entity.descriptionL2;
		}
		if (entity.isChanged(ExpTypeGroupEntity.Field.ORDER)) {
			sObj.Order__c = entity.order;
		}

		return sObj;
	}

	/**
	 * SObjectからエンティティを作成する
	 * @param obj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private ExpTypeGroupEntity createEntity(ExpTypeGroup__c sObj) {

		ExpTypeGroupEntity entity = new ExpTypeGroupEntity();

		// 有効期間型共通の項目を設定する
		setValidPeriodObjectValueToEntity(sObj, entity);

		entity.code = sObj.Code__c;
		entity.uniqKey = sObj.UniqKey__c;
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.companyId = sObj.CompanyId__c;
		entity.parentId = sObj.ParentId__c;
		entity.parentGroup = new ValidPeriodEntityOld.LookupField(
				new AppMultiString(
					sObj.ParentId__r.Name_L0__c,
					sObj.ParentId__r.Name_L1__c,
					sObj.ParentId__r.Name_L2__c));
		entity.descriptionL0 = sObj.Description_L0__c;
		entity.descriptionL1 = sObj.Description_L1__c;
		entity.descriptionL2 = sObj.Description_L2__c;
		entity.order = (Integer)sObj.Order__c;

		// 子の費目グループ
		if (sObj.ExpTypeGroups__r != null) {
			List<ExpTypeGroupEntity> groupEntityList = new List<ExpTypeGroupEntity>();
			for (ExpTypeGroup__c groupObj : sObj.ExpTypeGroups__r) {
				groupEntityList.add(createChildGroupEntity(groupObj));
			}
			entity.replaceChildGroupList(groupEntityList);
		}

		// 子の費目
		if (sObj.ExpTypes__r != null) {
			List<ExpTypeEntity> expTypeEntityList = new List<ExpTypeEntity>();
			for (ExpType__c expTypeObj : sObj.ExpTypes__r) {
				expTypeEntityList.add(createChildExpTypeEntity(expTypeObj));
			}
			entity.replaceChildExpTypeList(expTypeEntityList);
		}

		entity.resetChanged();

		return entity;
	}

	/**
	 * SObjectから子の費目グループエンティティを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private ExpTypeGroupEntity createChildGroupEntity(ExpTypeGroup__c sObj) {

		ExpTypeGroupEntity entity = new ExpTypeGroupEntity();
		entity.setId(sObj.Id);

		entity.resetChanged();

		return entity;
	}

	/**
	 * SObjectから子の費目エンティティを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private ExpTypeEntity createChildExpTypeEntity(ExpType__c sObj) {

		ExpTypeEntity entity = new ExpTypeEntity();
		entity.setId(sObj.Id);

		entity.resetChanged();

		return entity;
	}
}