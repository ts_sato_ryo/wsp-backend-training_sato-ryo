/**
 * TeamSpirit 勤怠インポートバッチのエンティティクラス。
 *
 * 当クラスはツールによって自動生成しているため、手動で修正しても上書きされる可能性があります。
 * 業務ロジックを追加する場合は、サブクラスを作成してください。
 *
 * Since this class is automatically generated by the tool, it may be overwritten even if it is corrected manually.
 * If you want to add business logic, please create a subclass.
 */
public abstract with sharing class AttImportBatchGeneratedEntity extends BaseEntity {

	/** フィールド名とフィールドのマップ */
	private static final Map<String, Schema.SObjectField> SOBJECT_FIELD_MAP = Schema.SObjectType.AttImportBatch__c.fields.getMap();

	/** TeamSpirit 勤怠インポートバッチ */
	protected AttImportBatch__c sobj;

	/**
	 * デフォルトコンストラクタ
	 */
	public AttImportBatchGeneratedEntity() {
		super(new AttImportBatch__c(), SOBJECT_FIELD_MAP);
		this.sobj = (AttImportBatch__c)super.sobj;
	}

	/**
	 * コンストラクタ
	 */
	public AttImportBatchGeneratedEntity(AttImportBatch__c sobj) {
		super(sobj, SOBJECT_FIELD_MAP);
		this.sobj = sobj;
	}

	/**
	 * 実行者
	 * Actor
	 */
	public static final Schema.SObjectField FIELD_ACTOR_HISTORY_ID = AttImportBatch__c.ActorHistoryId__c;
	public Id actorHistoryId {
		get {
			return (Id)getFieldValue(FIELD_ACTOR_HISTORY_ID);
		}
		set {
			setFieldValue(FIELD_ACTOR_HISTORY_ID, value);
		}
	}

	/**
	 * コメント
	 * Comment
	 */
	public static final Schema.SObjectField FIELD_COMMENT = AttImportBatch__c.Comment__c;
	public String comment {
		get {
			return (String)getFieldValue(FIELD_COMMENT);
		}
		set {
			setFieldValue(FIELD_COMMENT, value);
		}
	}

	/**
	 * 処理件数
	 * Count
	 */
	public static final Schema.SObjectField FIELD_COUNT = AttImportBatch__c.Count__c;
	public Integer count {
		get {
			return Integer.valueOf(getFieldValue(FIELD_COUNT));
		}
		set {
			setFieldValue(FIELD_COUNT, value);
		}
	}

	/**
	 * 実行者の部署
	 * Department
	 */
	public static final Schema.SObjectField FIELD_DEPARTMENT_HISTORY_ID = AttImportBatch__c.DepartmentHistoryId__c;
	public Id departmentHistoryId {
		get {
			return (Id)getFieldValue(FIELD_DEPARTMENT_HISTORY_ID);
		}
		set {
			setFieldValue(FIELD_DEPARTMENT_HISTORY_ID, value);
		}
	}

	/**
	 * 失敗件数
	 * Failure Count
	 */
	public static final Schema.SObjectField FIELD_FAILURE_COUNT = AttImportBatch__c.FailureCount__c;
	public Integer failureCount {
		get {
			return Integer.valueOf(getFieldValue(FIELD_FAILURE_COUNT));
		}
		set {
			setFieldValue(FIELD_FAILURE_COUNT, value);
		}
	}

	/**
	 * インポート日時
	 * Import Time
	 */
	public static final Schema.SObjectField FIELD_IMPORT_TIME = AttImportBatch__c.ImportTime__c;
	public AppDateTime importTime {
		get {
			return AppDatetime.valueOf((Datetime)getFieldValue(FIELD_IMPORT_TIME));
		}
		set {
			setFieldValue(FIELD_IMPORT_TIME, AppConverter.datetimeValue(value));
		}
	}

	/**
	 * インポートバッチ名
	 * TeamSpirit Attendance Import Batch Name
	 */
	public static final Schema.SObjectField FIELD_NAME = AttImportBatch__c.Name;
	public String name {
		get {
			return (String)getFieldValue(FIELD_NAME);
		}
		set {
			setFieldValue(FIELD_NAME, value);
		}
	}

	/**
	 * ステータス
	 * Status
	 */
	public static final Schema.SObjectField FIELD_STATUS = AttImportBatch__c.Status__c;
	public ImportBatchStatus status {
		get {
			return ImportBatchStatus.valueOf((String)getFieldValue(FIELD_STATUS));
		}
		set {
			setFieldValue(FIELD_STATUS, ImportBatchStatus.getValue((ImportBatchStatus)value));
		}
	}

	/**
	 * 成功件数
	 * Success Count
	 */
	public static final Schema.SObjectField FIELD_SUCCESS_COUNT = AttImportBatch__c.SuccessCount__c;
	public Integer successCount {
		get {
			return Integer.valueOf(getFieldValue(FIELD_SUCCESS_COUNT));
		}
		set {
			setFieldValue(FIELD_SUCCESS_COUNT, value);
		}
	}

	/**
	 * 種別
	 * Type
	 */
	public static final Schema.SObjectField FIELD_IMPORT_TYPE = AttImportBatch__c.Type__c;
	public AttImportBatchType importType {
		get {
			return AttImportBatchType.valueOf((String)getFieldValue(FIELD_IMPORT_TYPE));
		}
		set {
			setFieldValue(FIELD_IMPORT_TYPE, AttImportBatchType.getValue((AttImportBatchType)value));
		}
	}

	/**
	 * 会社
	 * Company
	 */
	public static final Schema.SObjectField FIELD_COMPANY_ID = AttImportBatch__c.CompanyId__c;
	public Id companyId {
		get {
			return (Id)getFieldValue(FIELD_COMPANY_ID);
		}
		set {
			setFieldValue(FIELD_COMPANY_ID, value);
		}
	}

	/**
	 * Idと値を更新した項目を設定したSObjectを生成する。
	 * @return 生成したSObjecgt
	 */
	public virtual AttImportBatch__c createSObject() {
		AttImportBatch__c sobj = new AttImportBatch__c(Id = super.id);
		for (Schema.SObjectField field : changedValues.keySet()) {
			sobj.put(field, changedValues.get(field));
		}
		return sobj;
	}

}