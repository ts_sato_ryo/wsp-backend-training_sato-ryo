/*
 * @author TeamSpirit Inc.
 * 
 * @date 2018
 * 
 * @description
 * 
 * @group Expense
 */
public with sharing class ExpJournalEntity extends Entity {

	public enum Field {
		AuthorizedTime,
		CompanyCode,
		CreditAccountCode,
		CreditAccountName,
		CreditAmount,
		CurrencyCode,
		ExchangeRate,
		ExpenseTypeDebitAccountCode,
		ExpenseTypeDebitAccountName,
		ExpenseTypeDebitSubAccountCode,
		ExpenseTypeDebitSubAccountName,
		ExportedTime,
		ExpReportId,
		ExtractDate,
		JournalNo,
		LocalAmount,
		Name,
		PaymentDate,
		RecordAmount,
		RecordDate,
		RecordId,
		RecordingDate,
		RecordItemAmount,
		RecordItemAmountWithoutTax,
		RecordItemCostCenterCode,
		RecordItemCostCenterName,
		RecordItemDate,
		RecordItemExpenseTypeCode,
		RecordItemExpenseTypeId,
		RecordItemExtendedItemDateList,
		RecordItemExtendedItemLookupList,
		RecordItemExtendedItemPicklistList,
		RecordItemExtendedItemTextList,
		RecordItemId,
		RecordItemJobCode,
		RecordItemJobName,
		RecordItemRemarks,
		RecordItemRowNo,
		RecordItemTaxAmount,
		RecordRemarks,
		RecordRowNo,
		Remark,
		ReportAmount,
		ReportApprovedDate,
		ReportCostCenterCode,
		ReportCostCenterName,
		ReportDate,
		ReportEmployeeCode,
		ReportEmployeeDepartmentCode,
		ReportEmployeeDepartmentName,
		ReportEmployeeName,
		ReportExtendedItemDateList,
		ReportExtendedItemLookupList,
		ReportExtendedItemPicklistList,
		ReportExtendedItemTextList,
		ReportJobCode,
		ReportJobName,
		ReportNo,
		ReportPaymentDueDate,
		ReportSubject,
		ReportType,
		ReportVendorCode,
		TaxTypeCode,
		TaxTypeId,
		TaxTypeName
	}

	public enum RequestField {
		ApprovedDate,
		Purpose,
		RequestAmount,
		RequestCostCenterCode,
		RequestCostCenterName,
		RequestDate,
		RequestEmployeeCode,
		RequestEmployeeDepartmentCode,
		RequestEmployeeDepartmentName,
		RequestEmployeeName,
		RequestExtendedItemDateList,
		RequestExtendedItemLookupList,
		RequestExtendedItemPicklistList,
		RequestExtendedItemTextList,
		RequestJobCode,
		RequestJobName,
		RequestNo,
		RequestReportType,
		RequestSubject,
		ScheduleDate
	}

	private Set<Field> isChangedFieldSet;
	private Set<RequestField> isChangedRequestFieldSet;

	public ExpJournalEntity() {
		isChangedFieldSet = new Set<Field>();
		isChangedRequestFieldSet = new Set<RequestField>();

		reportExtendedItemDateList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		reportExtendedItemLookupList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		reportExtendedItemTextList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		reportExtendedItemPicklistList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];

		recordItemExtendedItemDateList = new String[ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		recordItemExtendedItemLookupList = new String[ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		recordItemExtendedItemTextList = new String[ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		recordItemExtendedItemPicklistList = new String[ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT];

		requestExtendedItemDateList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		requestExtendedItemLookupList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		requestExtendedItemTextList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];
		requestExtendedItemPicklistList = new String[ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT];
	}

	/** Date approved by Manager for the ExpRequest (Pre-Application) */
	public AppDate approvedDate {
		get;
		set {
			approvedDate = value;
			setChanged(RequestField.ApprovedDate);
		}
	}

	public AppDateTime authorizedTime {
		get;
		set {
			authorizedTime = value;
			setChanged(Field.AuthorizedTime);
		}
	}

	public String companyCode {
		get;
		set {
			companyCode = value;
			setChanged(Field.CompanyCode);
		}
	}
	
	public String creditAccountCode {
		get;
		set {
			creditAccountCode = value;
			setChanged(Field.CreditAccountCode);
		}
	}
	
	public String creditAccountName {
		get;
		set {
			creditAccountName = value;
			setChanged(Field.CreditAccountName);
		}
	}
	
	public Decimal creditAmount {
		get;
		set {
			creditAmount = value;
			setChanged(Field.CreditAmount);
		}
	}

	public String currencyCode {
		get;
		set {
			currencyCode = value;
			setChanged(Field.CurrencyCode);
		}
	}

	public Decimal exchangeRate {
		get;
		set {
			exchangeRate = value;
			setChanged(Field.ExchangeRate);
		}
	}

	public String expenseTypeDebitAccountCode {
		get;
		set {
			expenseTypeDebitAccountCode = value;
			setChanged(Field.ExpenseTypeDebitAccountCode);
		}
	}

	public String expenseTypeDebitAccountName {
		get;
		set {
			expenseTypeDebitAccountName = value;
			setChanged(Field.ExpenseTypeDebitAccountName);
		}
	}

	public String expenseTypeDebitSubAccountCode {
		get;
		set {
			expenseTypeDebitSubAccountCode = value;
			setChanged(Field.ExpenseTypeDebitSubAccountCode);
		}
	}

	public String expenseTypeDebitSubAccountName {
		get;
		set {
			expenseTypeDebitSubAccountName = value;
			setChanged(Field.ExpenseTypeDebitSubAccountName);
		}
	}

	public AppDatetime exportedTime {
		get;
		set {
			exportedTime = value;
			setChanged(Field.ExportedTime);
		}
	}

	public AppDate extractDate {
		get;
		set {
			extractDate = value;
			setChanged(Field.ExtractDate);
		}
	}

	public String journalNo {
		get;
		set {
			journalNo = value;
			setChanged(Field.JournalNo);
		}
	}

	public Decimal localAmount {
		get;
		set {
			localAmount = value;
			setChanged(Field.LocalAmount);
		}
	}

	public String name {
		get;
		set {
			name = value;
			setChanged(Field.Name);
		}
	}

	public AppDate paymentDate {
		get;
		set {
			paymentDate = value;
			setChanged(Field.PaymentDate);
		}
	}

	public String purpose {
		get;
		set {
			purpose = value;
			setChanged(RequestField.Purpose);
		}
	}

	public Decimal recordAmount {
		get;
		set {
			recordAmount = value;
			setChanged(Field.RecordAmount);
		}
	}

	public AppDate recordDate {
		get;
		set {
			recordDate = value;
			setChanged(Field.RecordDate);
		}
	}

	public Id recordId {
		get;
		set {
			recordId = value;
			setChanged(Field.RecordId);
		}
	}

	/*
	 * Accounting Date
	 */
	public AppDate recordingDate {
		get;
		set {
			recordingDate = value;
			setChanged(Field.RecordingDate);
		}
	}

	public Decimal recordItemAmount {
		get;
		set {
			recordItemAmount = value;
			setChanged(Field.RecordItemAmount);
		}
	}

	public Decimal recordItemAmountWithoutTax {
		get;
		set {
			recordItemAmountWithoutTax = value;
			setChanged(Field.RecordItemAmountWithoutTax);
		}
	}

	/*
	 * Cost Center Linkage Code
	 */
	public String recordItemCostCenterCode {
		get;
		set {
			recordItemCostCenterCode = value;
			setChanged(Field.RecordItemCostCenterCode);
		}
	}

	public String recordItemCostCenterName {
		get;
		set {
			recordItemCostCenterName = value;
			setChanged(Field.RecordItemCostCenterName);
		}
	}

	public AppDate recordItemDate {
		get;
		set {
			recordItemDate = value;
			setChanged(Field.RecordItemDate);
		}
	}

	public String recordItemExpenseTypeCode {
		get;
		set {
			recordItemExpenseTypeCode = value;
			setChanged(Field.RecordItemExpenseTypeCode);
		}
	}

	public Id recordItemExpenseTypeId {
		get;
		set {
			recordItemExpenseTypeId = value;
			setChanged(Field.RecordItemExpenseTypeId);
		}
	}

	/** Date Extended Item */
	private List<String> recordItemExtendedItemDateList;
	public void setRecordItemExtendedItemDate(Integer index, String value) {
		recordItemExtendedItemDateList[index] = value;
		setChanged(Field.RecordItemExtendedItemDateList);
	}
	public void setRecordItemExtendedItemDate(Integer index, Date value) {
		AppDate convertedDate = AppDate.valueOf(value);
		recordItemExtendedItemDateList[index] = convertedDate == null ? null : convertedDate.toString();
		setChanged(Field.RecordItemExtendedItemDateList);
	}
	public Date getRecordItemExtendedItemDate(Integer index) {
		AppDate convertedDate = AppDate.valueOf(this.recordItemExtendedItemDateList[index]);
		return convertedDate == null ? null : convertedDate.getDate();
	}

	/** Lookup Extended Item */
	private List<String> recordItemExtendedItemLookupList;
	public void setRecordItemExtendedItemLookup(Integer index, String value) {
		recordItemExtendedItemLookupList[index] = value;
		setChanged(Field.RecordItemExtendedItemLookupList);
	}
	public String getRecordItemExtendedItemLookup(Integer index) {
		return recordItemExtendedItemLookupList[index];
	}


	/** Extended Item Picklist */
	private List<String> recordItemExtendedItemPicklistList;
	public void setRecordItemExtendedItemPicklist(Integer index, String value) {
		recordItemExtendedItemPicklistList[index] = value;
		setChanged(Field.RecordItemExtendedItemPicklistList);
	}
	public String getRecordItemExtendedItemPicklist(Integer index) {
		return recordItemExtendedItemPicklistList[index];
	}

	/** Extended Item Text */
	private List<String> recordItemExtendedItemTextList;
	public void setRecordItemExtendedItemText(Integer index, String value) {
		recordItemExtendedItemTextList[index] = value;
		setChanged(Field.RecordItemExtendedItemTextList);
	}
	public String getRecordItemExtendedItemText(Integer index) {
		return recordItemExtendedItemTextList[index];
	}

	public Id recordItemId {
		get;
		set {
			recordItemId = value;
			setChanged(Field.RecordItemId);
		}
	}

	public String recordItemJobCode {
		get;
		set {
			recordItemJobCode = value;
			setChanged(Field.RecordItemJobCode);
		}
	}

	public String recordItemJobName {
		get;
		set {
			recordItemJobName = value;
			setChanged(Field.RecordItemJobName);
		}
	}

	public String recordItemRemarks {
		get;
		set {
			recordItemRemarks = value;
			setChanged(Field.RecordItemRemarks);
		}
	}

	public Integer recordItemRowNo {
		get;
		set {
			recordItemRowNo = value;
			setChanged(Field.RecordItemRowNo);
		}
	}

	public Decimal recordItemTaxAmount {
		get;
		set {
			recordItemTaxAmount = value;
			setChanged(Field.RecordItemTaxAmount);
		}
	}

	public String recordRemarks {
		get;
		set {
			recordRemarks = value;
			setChanged(Field.RecordRemarks);
		}
	}

	public Integer recordRowNo {
		get;
		set {
			recordRowNo = value;
			setChanged(Field.RecordRowNo);
		}
	}

	public String remark {
		get;
		set {
			remark = value;
			setChanged(Field.Remark);
		}
	}

	public Decimal reportAmount {
		get;
		set {
			reportAmount = value;
			setChanged(Field.ReportAmount);
		}
	}

	/*
	 * Date Approved by the last approver who is not from Accounting Department
	 */
	public AppDate reportApprovedDate {
		get;
		set {
			reportApprovedDate = value;
			setChanged(Field.ReportApprovedDate);
		}
	}

	public String reportCostCenterCode {
		get;
		set {
			reportCostCenterCode = value;
			setChanged(Field.ReportCostCenterCode);
		}
	}

	public String reportCostCenterName {
		get;
		set {
			reportCostCenterName = value;
			setChanged(Field.ReportCostCenterName);
		}
	}

	/*
	 * Report Submission Date
	 */
	public AppDate reportDate {
		get;
		set {
			reportDate = value;
			setChanged(Field.ReportDate);
		}
	}

	public String reportEmployeeCode {
		get;
		set {
			reportEmployeeCode = value;
			setChanged(Field.ReportEmployeeCode);
		}
	}

	public String reportEmployeeDepartmentCode {
		get;
		set {
			reportEmployeeDepartmentCode = value;
			setChanged(Field.ReportEmployeeDepartmentCode);
		}
	}

	public String reportEmployeeDepartmentName {
		get;
		set {
			reportEmployeeDepartmentName = value;
			setChanged(Field.ReportEmployeeDepartmentName);
		}
	}

	public String reportEmployeeName {
		get;
		set {
			reportEmployeeName = value;
			setChanged(Field.ReportEmployeeName);
		}
	}

	/** Date Extended Item */
	private List<String> reportExtendedItemDateList;
	public void setReportExtendedItemDate(Integer index, String value) {
		reportExtendedItemDateList[index] = value;
		setChanged(Field.ReportExtendedItemDateList);
	}
	public void setReportExtendedItemDate(Integer index, Date value) {
		AppDate convertedDate = AppDate.valueOf(value);
		reportExtendedItemDateList[index] = convertedDate == null ? null : convertedDate.toString();
		setChanged(Field.ReportExtendedItemDateList);
	}
	public Date getReportExtendedItemDate(Integer index) {
		AppDate convertedDate = AppDate.valueOf(this.reportExtendedItemDateList[index]);
		return convertedDate == null ? null : convertedDate.getDate();
	}

	/** Lookup Extended Item */
	private List<String> reportExtendedItemLookupList;
	public void setReportExtendedItemLookup(Integer index, String value) {
		reportExtendedItemLookupList[index] = value;
		setChanged(Field.ReportExtendedItemLookupList);
	}
	public String getReportExtendedItemLookup(Integer index) {
		return reportExtendedItemLookupList[index];
	}

	/** Extended Item Picklist */
	private List<String> reportExtendedItemPicklistList;
	public void setReportExtendedItemPicklist(Integer index, String value) {
		reportExtendedItemPicklistList[index] = value;
		setChanged(Field.ReportExtendedItemPicklistList);
	}
	public String getReportExtendedItemPicklist(Integer index) {
		return reportExtendedItemPicklistList[index];
	}

	/** Extended Item Text */
	private List<String> reportExtendedItemTextList;
	public void setReportExtendedItemText(Integer index, String value) {
		reportExtendedItemTextList[index] = value;
		setChanged(Field.ReportExtendedItemTextList);
	}
	public String getReportExtendedItemText(Integer index) {
		return reportExtendedItemTextList[index];
	}

	public Id expReportId {
		get;
		set {
			expReportId = value;
			setChanged(Field.ExpReportId);
		}
	}

	public String reportJobCode {
		get;
		set {
			reportJobCode = value;
			setChanged(Field.ReportJobCode);
		}
	}

	public String reportJobName {
		get;
		set {
			reportJobName = value;
			setChanged(Field.ReportJobName);
		}
	}

	public String reportNo {
		get;
		set {
			reportNo = value;
			setChanged(Field.ReportNo);
		}
	}
	
	public AppDate reportPaymentDueDate {
		get;
		set {
			reportPaymentDueDate = value;
			setChanged(Field.ReportPaymentDueDate);
		}
	}

	public String reportSubject {
		get;
		set {
			reportSubject = value;
			setChanged(Field.ReportSubject);
		}
	}

	/*
	 * Report Type Code
	 */
	public String reportType {
		get;
		set {
			reportType = value;
			setChanged(Field.ReportType);
		}
	}
	
	public String reportVendorCode {
		get;
		set {
			reportVendorCode = value;
			setChanged(Field.ReportVendorCode);
		}
	}

	public Decimal requestAmount {
		get;
		set {
			requestAmount = value;
			setChanged(RequestField.RequestAmount);
		}
	}

	/*
	 * CostCenter Linkage Code
	 */
	public String requestCostCenterCode {
		get;
		set {
			requestCostCenterCode = value;
			setChanged(RequestField.RequestCostCenterCode);
		}
	}

	public String requestCostCenterName {
		get;
		set {
			requestCostCenterName = value;
			setChanged(RequestField.RequestCostCenterName);
		}
	}

	/** Request Submitted Date (Pre-Application) */
	public AppDate requestDate {
		get;
		set {
			requestDate = value;
			setChanged(RequestField.RequestDate);
		}
	}

	public String requestEmployeeCode {
		get;
		set {
			requestEmployeeCode = value;
			setChanged(RequestField.RequestEmployeeCode);
		}
	}

	public String requestEmployeeDepartmentCode {
		get;
		set {
			requestEmployeeDepartmentCode = value;
			setChanged(RequestField.RequestEmployeeDepartmentCode);
		}
	}

	public String requestEmployeeDepartmentName {
		get;
		set {
			requestEmployeeDepartmentName = value;
			setChanged(RequestField.RequestEmployeeDepartmentName);
		}
	}

	public String requestEmployeeName {
		get;
		set {
			requestEmployeeName = value;
			setChanged(RequestField.RequestEmployeeName);
		}
	}

	/** Date Extended Item */
	private List<String> requestExtendedItemDateList;
	public void setRequestExtendedItemDate(Integer index, String value) {
		requestExtendedItemDateList[index] = value;
		setChanged(RequestField.RequestExtendedItemDateList);
	}
	public void setRequestExtendedItemDate(Integer index, Date value) {
		AppDate convertedDate = AppDate.valueOf(value);
		requestExtendedItemDateList[index] = convertedDate == null ? null : convertedDate.toString();
		setChanged(RequestField.RequestExtendedItemDateList);
	}
	public Date getRequestExtendedItemDate(Integer index) {
		AppDate convertedDate = AppDate.valueOf(this.requestExtendedItemDateList[index]);
		return convertedDate == null ? null : convertedDate.getDate();
	}

	/** Lookup Extended Item */
	private List<String> requestExtendedItemLookupList;
	public void setRequestExtendedItemLookup(Integer index, String value) {
		requestExtendedItemLookupList[index] = value;
		setChanged(RequestField.RequestExtendedItemLookupList);
	}
	public String getRequestExtendedItemLookup(Integer index) {
		return requestExtendedItemLookupList[index];
	}

	/** Extended Item Picklist */
	private List<String> requestExtendedItemPicklistList;
	public void setRequestExtendedItemPicklist(Integer index, String value) {
		requestExtendedItemPicklistList[index] = value;
		setChanged(RequestField.RequestExtendedItemPicklistList);
	}
	public String getRequestExtendedItemPicklist(Integer index) {
		return requestExtendedItemPicklistList[index];
	}

	/** Extended Item Text */
	private List<String> requestExtendedItemTextList;
	public void setRequestExtendedItemText(Integer index, String value) {
		requestExtendedItemTextList[index] = value;
		setChanged(RequestField.RequestExtendedItemTextList);
	}
	public String getRequestExtendedItemText(Integer index) {
		return requestExtendedItemTextList[index];
	}

	public String requestJobCode {
		get;
		set {
			requestJobCode = value;
			setChanged(RequestField.RequestJobCode);
		}
	}

	public String requestJobName {
		get;
		set {
			requestJobName = value;
			setChanged(RequestField.RequestJobName);
		}
	}

	public String requestNo {
		get;
		set {
			requestNo = value;
			setChanged(RequestField.RequestNo);
		}
	}

	/*
	 * Report Type Code used in the Expense Request
	 */
	public String requestReportType {
		get;
		set {
			requestReportType = value;
			setChanged(RequestField.RequestReportType);
		}
	}

	public String requestSubject {
		get;
		set {
			requestSubject = value;
			setChanged(RequestField.RequestSubject);
		}
	}

	public AppDate scheduleDate {
		get;
		set {
			scheduleDate = value;
			setChanged(RequestField.ScheduleDate);
		}
	}

	public String taxTypeCode {
		get;
		set {
			taxTypeCode = value;
			setChanged(Field.TaxTypeCode);
		}
	}

	public Id taxTypeId {
		get;
		set {
			taxTypeId = value;
			setChanged(Field.TaxTypeId);
		}
	}

	public String taxTypeName {
		get;
		set {
			taxTypeName = value;
			setChanged(Field.TaxTypeName);
		}
	}

	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}
	private void setChanged(RequestField field) {
		isChangedRequestFieldSet.add(field);

	}

	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}
	public Boolean isChanged(RequestField field) {
		return isChangedRequestFieldSet.contains(field);
	}

	public void resetChanged() {
		this.isChangedFieldSet.clear();
		this.isChangedRequestFieldSet.clear();
	}
}