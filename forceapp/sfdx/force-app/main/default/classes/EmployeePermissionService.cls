/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 社員の権限に関するサービスを提供するクラス
 * 権限マスタに関する処理はPermissionServiceクラスに実装してください
 */
public with sharing class EmployeePermissionService {

	/** 全てのデータの編集権限を持っている場合はtrue */
	public Boolean hasModifyAllData {
		get {
			if (hasModifyAllData == null) {
				Id userId = employeeBase == null ? UserInfo.getUserId() : employeeBase.userId;

				PermissionSetAssignmentRepository.SearchFilter filter =
						new PermissionSetAssignmentRepository.SearchFilter();
				filter.assigneeIds = new List<Id>{userId};
				filter.isModifyAllData = true;
				List<PermissionSetAssignmentEntity> assigns = new PermissionSetAssignmentRepository().searchEntityList(filter, false);
				hasModifyAllData = assigns.isEmpty() ? false : true;
			}
			return hasModifyAllData;
		}

		private set;
	}

	//----------------------------------------------
	// 勤怠系(Attendance)
	//----------------------------------------------

	/** 勤務表表示(代理)権限を持っている場合はtrue */
	public Boolean hasViewAttTimeSheetByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_VIEW_ATT_TIME_SHEET_BY_DELEGATE);}
	}

	/** 勤務表編集(代理)権限を持っている場合はtrue */
	public Boolean hasEditAttTimeSheetByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_EDIT_ATT_TIME_SHEET_BY_DELEGATE);}
	}

	/** 各種勤怠申請申請・申請削除(代理)権限を持っている場合はtrue */
	public Boolean hasSubmitAttDailyRequestByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_SUBMIT_ATT_DAILY_REQUEST_BY_DELEGATE);}
	}

	/** 各種勤怠申請承認却下(代理)権限を持っている場合はtrue */
	public Boolean hasApproveAttDailyRequestByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_APPROVE_ATT_DAILY_REQUEST_BY_DELEGATE);}
	}

	/** 各種勤怠申請自己承認(本人)権限を持っている場合はtrue */
	public Boolean hasApproveSelfAttDailyRequestByEmployee {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_APPROVE_SELF_ATT_DAILY_REQUEST_BY_EMPLOYEE);}
	}

	/** 各種勤怠申請申請取消(代理)権限を持っている場合はtrue */
	public Boolean hasCancelAttDailyRequestByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_CANCEL_ATT_DAILY_REQUEST_BY_DELEGATE);}
	}

	/** 各種勤怠申請承認取消(本人)権限を持っている場合はtrue */
	public Boolean hasCancelAttDailyApprovalByEmployee {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_CANCEL_ATT_DAILY_APPROVAL_BY_EMPLOYEE);}
	}

	/** 各種勤怠申請承認取消(代理)権限を持っている場合はtrue */
	public Boolean hasCancelAttDailyApprovalByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_CANCEL_ATT_DAILY_APPROVAL_BY_DELEGATE);}
	}

	/** 勤務確定申請申請(代理)権限を持っている場合はtrue */
	public Boolean hasSubmitAttRequestByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_SUBMIT_ATT_REQUEST_BY_DELEGATE);}
	}

	/** 勤務確定申請申請承認却下(代理)権限を持っている場合はtrue */
	public Boolean hasApproveAttRequestByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_APPROVE_ATT_REQUEST_BY_DELEGATE);}
	}

	/** 勤務確定申請自己承認(本人)権限を持っている場合はtrue */
	public Boolean hasApproveSelfAttRequestByEmployee {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_APPROVE_SELF_ATT_REQUEST_BY_EMPLOYEE);}
	}

	/** 勤務確定申請申請取消(代理)権限を持っている場合はtrue */
	public Boolean hasCancelAttRequestByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_CANCEL_ATT_REQUEST_BY_DELEGATE);}
	}

	/** 勤務確定申請承認取消(本人)権限を持っている場合はtrue */
	public Boolean hasCancelAttApprovalByEmployee {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_CANCEL_ATT_APPROVAL_BY_EMPLOYEE);}
	}

	/** 勤務確定申請承認取消(代理)権限を持っている場合はtrue */
	public Boolean hasCancelAttApprovalByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_CANCEL_ATT_APPROVAL_BY_DELEGATE);}
	}

	//----------------------------------------------
	// 工数系(TimeTrack)
	//----------------------------------------------

	/** 工数実績表示（代理）権限を持っている場合はtrue */
	public Boolean hasViewTimeTrackByDelegate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_VIEW_TIME_TRACK_BY_DELEGATE);}
	}

	//----------------------------------------------
	// マスタ管理系系(Master)
	//----------------------------------------------
	/** 全体設定の管理権限を持っている場合はtrue */
	public Boolean hasManageOverallSetting {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_OVERALL_SETTING);}
	}
	/** 会社の切り替えの権限を持っている場合はtrue */
	public Boolean hasSwitchCompany {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_SWITCH_COMPANY);}
	}
	/** 部署の管理権限を持っている場合はtrue */
	public Boolean hasManageDepartment {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_DEPARTMENT);}
	}
	/** 社員の管理権限を持っている場合はtrue */
	public Boolean hasManageEmployee {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EMPLOYEE);}
	}
	/** カレンダーの管理権限を持っている場合はtrue */
	public Boolean hasManageCalendar {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_CALENDAR);}
	}
	/** ジョブタイプの管理権限を持っている場合はtrue */
	public Boolean hasManageJobType {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_JOB_TYPE);}
	}
	/** ジョブの管理権限を持っている場合はtrue */
	public Boolean hasManageJob {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_JOB);}
	}
	/** モバイル機能設定の管理権限を持っている場合はtrue */
	public Boolean hasManageMobileSetting {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_MOBILE_SETTING);}
	}
	/** プランナー機能設定の管理権限を持っている場合はtrue */
	public Boolean hasManagePlannerSetting {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_PLANNER_SETTING);}
	}
	/** アクセス権限設定の管理権限を持っている場合はtrue */
	public Boolean hasManagePermission {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_PERMISSION);}
	}
	/** 休暇の管理権限を持っている場合はtrue */
	public Boolean hasManageAttLeave {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_LEAVE);}
	}
	/** 短時間勤務設定の管理権限を持っている場合はtrue */
	public Boolean hasManageAttShortTimeWorkSetting {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_SHORT_TIME_WORK_SETTING);}
	}
	/** 休職・休業の管理権限を持っている場合はtrue */
	public Boolean hasManageAttLeaveOfAbsence {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_LEAVE_OF_ABSENCE);}
	}
	/** 勤務体系の管理権限を持っている場合はtrue */
	public Boolean hasManageAttWorkingType {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_WORKING_TYPE);}
	}
	/** 勤務パターンの管理権限を持っている場合はtrue */
	public Boolean hasManageAttPattern {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_PATTERN);}
	}
	/** 残業警告設定の管理権限を持っている場合はtrue */
	public Boolean hasManageAttAgreementAlertSetting {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_AGREEMENT_ALERT_SETTING);}
	}
	/** 休暇管理の管理権限を持っている場合はtrue */
	public Boolean hasManageAttLeaveGrant {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_LEAVE_GRANT);}
	}
	/** 短時間勤務設定適用の管理権限を持っている場合はtrue */
	public Boolean hasManageAttShortTimeWorkSettingApply {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY);}
	}
	/** 休職・休業適用の管理権限を持っている場合はtrue */
	public Boolean hasManageAttLeaveOfAbsenceApply {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY);}
	}
	/** 勤務パターン適用の管理権限を持っている場合はtrue */
	public Boolean hasManageAttPatternApply {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_ATT_PATTERN_APPLY);}
	}
	/** 工数設定の管理権限を持っている場合はtrue */
	public Boolean hasManageTimeSetting {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_TIME_SETTING);}
	}
	/** 作業分類の管理権限を持っている場合はtrue */
	public Boolean hasManageTimeWorkCategory {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_TIME_WORK_CATEGORY);}
	}
	/** 費目グループの管理権限を持っている場合はtrue / if have admin permission to expens type group, return true */
	public Boolean hasManageExpTypeGroup {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_TYPE_GROUP);}
	}
	/** 費目の管理権限を持っている場合はtrue / if have admin permission to expens etype, return true */
	public Boolean hasManageExpenseType {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_TYPE);}
	}
	/** 税区分の管理権限を持っている場合はtrue / if have admin permission to tax type, return true */
	public Boolean hasManageTaxType {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_TAX_TYPE);}
	}
	/** 経費設定の管理権限を持っている場合はtrue / if have admin permission to expense setting, return true */
	public Boolean hasManageExpSetting {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_SETTING);}
	}
	/** 為替レートの管理権限を持っている場合はtrue / if have admin permission to exchange rate, return true */
	public Boolean hasManageExchangeRate {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_EXCHANGE_RATE);}
	}
	/** 計上期間の管理権限を持っている場合はtrue / if have admin permission to accounting period, return true */
	public Boolean hasManageAccountingPeriod {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_ACCOUNTING_PERIOD);}
	}
	/** 申請種別の管理権限を持っている場合はtrue / if have admin permission to report type, return true */
	public Boolean hasManageReportType {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_REPORT_TYPE);}
	}
	/** コストセンターの管理権限を持っている場合はtrue / if have admin permission to cost center, return true */
	public Boolean hasManageCostCenter {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_COST_CENTER);}
	}
	/** 支払先の管理権限を持っている場合はtrue / if have admin permission to vendor, return true */
	public Boolean hasManageVendor {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_VENDOR);}
	}
	/** 拡張項目の管理権限を持っている場合はtrue / if have admin permission to extended item, return true */
	public Boolean hasManageExtendedItem {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_EXTENDED_ITEM);}
	}
	/** 従業員グループの管理権限を持っている場合はtrue / if have admin permission to employee group, return true */
	public Boolean hasManageEmployeeGroup {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_EMPLOYEE_GROUP);}
	}
	/** if have admin permission to custom hint, return true */
	public Boolean hasManageExpCustomHint {
		get {return hasPermission(PermissionGeneratedEntity.FIELD_IS_MANAGE_EXP_CUSTOM_HINT);}
	}



	/** 権限のキャッシュデータ(key:Permission Id) */
	private static Map<Id, PermissionEntity> permissionMap = new Map<Id, PermissionEntity>();

	/**
	 * 権限のキャッシュをクリアする
	 */
	@testVisible
	private static void clearPermissionMap() {
		permissionMap.clear();
	}

	/** 権限チェック対象の社員 */
	private EmployeeBaseEntity employeeBase;

	/** チェック対象日時点で有効な社員履歴が参照している権限(将来的には複数の権限が割り当てられる可能性あり) */
	private PermissionEntity employeePermission;


	/**
	 * コンストラクタ
	 * チェック対象日は今日(実行日)の日付
	 * @param employeeBaseId チェック対象の社員ベースID
	 */
	public EmployeePermissionService(Id employeeBaseId) {
		this(new EmployeeRepository().getEntity(employeeBaseId), AppDate.today());
	}

	/**
	 * コンストラクタ
	 * チェック対象日は今日(実行日)の日付
	 * @param employeeBase チェック対象の社員(ベース＋チェック対象日に有効な履歴)
	 */
	public EmployeePermissionService(EmployeeBaseEntity employeeBase) {
		this(employeeBase, AppDate.today());
	}

	/**
	 * コンストラクタ
	 * @param employeeBase チェック対象の社員(ベース＋チェック対象日に有効な履歴)
	 * @param targetDate チェック対象の日付
	 */
	public EmployeePermissionService(EmployeeBaseEntity employeeBase, AppDate targetDate) {
		if (employeeBase == null) {
			// メッセージ：該当する社員が見つかりません
			throw new App.RecordNotFoundException(
					ComMessage.msg().Com_Err_NotFound(new List<String>{ComMessage.msg().Com_Lbl_Employee}));
		}
		this.employeeBase = employeeBase;

		EmployeeHistoryEntity employeeHistory = employeeBase.getHistoryByDate(targetDate);
		if (employeeHistory == null) {
			// メッセージ：該当の社員は有効期間外です
			throw new App.IllegalStateException(
				ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Employee}));
		}

		this.employeePermission = getPermissionById(employeeHistory.permissionId);
	}

	/**
	 * コンストラクタ
	 * ログインユーザがチェック対象の場合は本コンストラクタを利用してください
	 */
	public EmployeePermissionService() {
		final AppDate targetDate = AppDate.today();
		EmployeeBaseEntity employeeBase;
		try {
			employeeBase = new EmployeeService().getEmployeeByUserIdFromCache(UserInfo.getUserId(), targetDate);
		} catch (App.IllegalStateException e) {
			// 取得できなくてもエラーにしない（実行ユーザが社員登録されていないシステム管理者の場合があるため）
		} catch (App.RecordNotFoundException re) {
			// 取得できなくてもエラーにしない（実行ユーザが社員登録されていないシステム管理者の場合があるため）
		}

		if (employeeBase != null) {
			this(employeeBase, targetDate);
		}
	}

	/**
	 * 権限情報を本クラスのpermissionMapに一括で設定する
	 * 権限データ取得クエリの実行回数を削減したい場合に利用してください
	 */
	public static void setPermissionList(List<PermissionEntity> permissionList) {
		for (PermissionEntity permission : permissionList) {
			permissionMap.put(permission.id, permission);
		}
	}

	/**
	 * 指定したIDの権限を取得する(キャッシュ機能あり)
	 * @param permissionId 取得対象の権限ID
	 * @return 権限、ただし権限が見つからなかった場合はnull
	 */
	private static PermissionEntity getPermissionById(Id permissionId) {
		PermissionEntity permission = permissionMap.get(permissionId);
		if (permission == null) {
			permission = new PermissionRepository().getEntityById(permissionId);
			permissionMap.put(permissionId, permission);
		}
		return permission;
	}

	/**
	 * 社員が指定した権限を持っているかをチェックする
	 * @param チェック対象の権限項目
	 * @return 権限を持っている場合はtrue, そうでない場合はfalse
	 */
	private Boolean hasPermission(Schema.SObjectField field) {

		// システム管理者(すべてのデータの編集権限を持っている)は全ての権限が付与されているものとする
		if (this.hasModifyAllData) {
			return true;
		}

		// 社員の権限が論理削除されている、または割り当てられていない場合は全ての権限はないものとする
		if (this.employeePermission == null) {
			return false;
		}

		return (Boolean)this.employeePermission.getFieldValue(field);
	}

}