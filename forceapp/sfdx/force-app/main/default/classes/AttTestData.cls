/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 勤怠
  *
  * 勤怠のテストデータクラス
  */
@isTest
@TestVisible
private class AttTestData {

	/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
	public ComOrgSetting__c org;
	/** 会社オブジェクト */
	public ComCompany__c company {get; private set;}
	/** 部署オブジェクト */
	public ComDeptBase__c dept {get; private set;}
	/** 権限 */
	public PermissionEntity permission {get; private set;}
	/** 社員(実行ユーザ) */
	public EmployeeBaseEntity employee {get; private set;}
	/** 勤務体系 */
	public AttWorkingTypeBaseEntity workingType {get; private set;}
	/** 休暇 */
	public List<AttLeaveEntity> leaveList {get; private set;}

	/**
	 * コンストラクタ（組織と会社と社員を作成する)
	 */
	public AttTestData() {
		this.org = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		this.company = createCompany();
		this.dept = ComTestDataUtility.createDepartmentWithHistory('AttTest Dept', company.Id);
		// 履歴の開始日は無限
		ComDeptHistory__c history = new ComDeptHistory__c(
				Id = dept.CurrentHistoryId__c,
				ValidFrom__c = Date.newInstance(1900,1,1),
				ValidTo__c = Date.newInstance(2100, 12, 31));
		update history;
		this.permission = new PermissionEntity(ComTestDataUtility.createPermission('AttTest Permission', company.Id));
		this.employee = createEmployee('001', userInfo.getUserId(), company.Id);
		this.leaveList = createLeaveList();
	}

	/**
	 * コンストラクタ（社員に勤務体系を設定する)
	 * @param wkType 労働時間制
	 */
	public AttTestData(AttWorkSystem wkType) {
		this();
		workingType = createWorkingType(wkType);
		setWorkingType(employee, workingType);
	}

	/**
	 * 会社を作成する
	 */
	private ComCompany__c createCompany() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		company.Language__c = 'ja';
		update company;
		return company;
	}

	/**
	 * 社員エンティティを作成して登録する
	 * @param code 社員コード
	 * @param userId ユーザID
	 * @param companyId 会社ID
	 */
	private EmployeeBaseEntity createEmployee(String code, Id userId, Id companyId) {

		ComEmpBase__c empObj = ComTestDataUtility.createEmployeeWithHistory(code, companyId, null, userId, this.permission.id);
		// 履歴の開始日は無限
		ComEmpHistory__c history = new ComEmpHistory__c(
				Id = empObj.CurrentHistoryId__c,
				DepartmentBaseId__c = dept.Id,
				ValidFrom__c = Date.newInstance(1900,1,1),
				ValidTo__c = Date.newInstance(2100, 12, 31));
		update history;

		EmployeeBaseEntity entity = new EmployeeRepository().getEntity(empObj.Id);
		return entity;
	}

	/**
	 * 標準ユーザと社員エンティティを作成して登録する
	 * @param code 社員コード
	 * @param companyId 会社ID
	 * @return 標準ユーザの社員
	 */
	public EmployeeBaseEntity createEmployeeStandardUser(String code) {
		User standardUser = ComTestDataUtility.createStandardUser(code);
		return createEmployee(code, standardUser.Id, this.company.Id);
	}

	/**
	 * 社員からSystem.runAs(User)用のユーザを作成する
	 */
	public User createRunAsUserFromEmployee(EmployeeBaseEntity employee) {
		return new User(Id = employee.userId);
	}

	/**
	 * 勤務体系データを作成する
	 * @param 労働時間制
	 */
	public AttWorkingTypeBaseEntity createWorkingType(AttWorkSystem wkType) {

		AttWorkingTypeBaseEntity base = new AttWorkingTypeBaseEntity();
		base.name = 'TestWorkingType';
		base.code = base.name + String.valueOf(Integer.valueOf(Math.random() * 1000));
		base.uniqKey = company.Code__c + '-' + base.code;
		base.companyId = company.Id;
		base.payrollPeriod = AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month;
		base.startMonthOfYear = 1;
		base.startDateOfMonth = 1;
		base.startDateOfWeek = 0;
		base.yearMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		base.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		base.workSystem = wkType;

		AttWorkingTypeHistoryEntity history = new AttWorkingTypeHistoryEntity();
		base.addHistory(history);
		history.validFrom = AppDate.newInstance(1900, 1, 1);
		history.validTo = AppDate.newInstance(2100, 12, 31);
		history.uniqKey = base.code + '_' + history.validFrom.formatYYYYMMDD()
				+ '_' + history.validTo.formatYYYYMMDD();
		history.nameL0 = base.name + '_L0';
		history.nameL1 = base.name + '_L1';
		history.nameL2 = base.name + '_L2';

		history.startTime = AttTime.valueOf(9*60);
		history.endTime = AttTime.valueOf(18*60);
		if (wkType == AttWorkSystem.JP_Flex) {
			history.flexStartTime = AttTime.valueOf(7*60);
			history.flexEndTime = AttTime.valueOf(22*60);
			history.isIncludeHolidayWorkInPlainTime = true;
		}
		if (wkType == AttWorkSystem.JP_Discretion) {
			history.deemedWorkHours = AttDailyTime.valueOf(9*60);
		}
		history.legalWorkTimeADay = AttDailyTime.valueOf(8*60);
		if (wkType == AttWorkSystem.JP_Flex) {
			history.contractedWorkHours = AttDailyTime.valueOf(9*60);
		} else if (wkType == AttWorkSystem.JP_Discretion) {
			history.contractedWorkHours = AttDailyTime.valueOf(8*60);
		} else {
			history.contractedWorkHours = AttDailyTime.valueOf(8*60);
		}
		history.legalWorkTimeAWeek = AttDailyTime.valueOf(40*60);
		history.rest1StartTime = AttTime.valueOf(12*60);
		history.rest1EndTime = AttTime.valueOf(13*60);
		history.weeklyDayTypeSUN = AttDayType.LEGAL_HOLIDAY;
		history.weeklyDayTypeMON = AttDayType.WORKDAY;
		history.weeklyDayTypeTUE = AttDayType.WORKDAY;
		history.weeklyDayTypeWED = AttDayType.WORKDAY;
		history.weeklyDayTypeTHU = AttDayType.WORKDAY;
		history.weeklyDayTypeFRI = AttDayType.WORKDAY;
		history.weeklyDayTypeSAT = AttDayType.HOLIDAY;
		//午前半休
		history.amStartTime = AttTime.valueOf(13*60);
		history.amEndTime = AttTime.valueOf(18*60);
		if (wkType == AttWorkSystem.JP_Flex) {
			history.amContractedWorkHours = AttDailyTime.valueOf(5*60);
		} else {
			history.amContractedWorkHours = AttDailyTime.valueOf(4*60 );
		}
		history.amRest1StartTime = AttTime.valueOf(14*60);
		history.amRest1EndTime = AttTime.valueOf(15*60);
		history.useAMHalfDayLeave = true;
		//午後半休
		history.pmStartTime = AttTime.valueOf(9*60);
		history.pmEndTime = AttTime.valueOf(13*60);
		if (wkType == AttWorkSystem.JP_Flex) {
			history.pmContractedWorkHours = AttDailyTime.valueOf(4*60);
		} else {
			history.pmContractedWorkHours = AttDailyTime.valueOf(3*60);
		}
		history.pmRest1StartTime = AttTime.valueOf(9*60);
		history.pmRest1EndTime = AttTime.valueOf(10*60);
		history.usePMHalfDayLeave = true;

		// 利用機能系オプション
		history.useEarlyStartWorkApply = true;
		history.useOvertimeWorkApply = true;
		history.allowToChangeApproverSelf = true;
		if (wkType != AttWorkSystem.JP_Manager) {
			history.useLegalRestCheck1 = true;
			history.legalRestCheck1WorkTimeThreshold = AttDailyTime.valueOf(6*60);
			history.legalRestCheck1RequiredRestTime = AttDailyTime.valueOf(45);
			history.useLegalRestCheck2 = true;
			history.legalRestCheck2WorkTimeThreshold = AttDailyTime.valueOf(8*60);
			history.legalRestCheck2RequiredRestTime = AttDailyTime.valueOf(60);
		}
		history.useDirectApply = true;
		history.directApplyStartTime = AttTime.valueOf(9*60);
		history.directApplyEndTime = AttTime.valueOf(18*60);
		history.directApplyRest1StartTime = AttTime.valueOf(10*60);
		history.directApplyRest1EndTime = AttTime.valueOf(10*60 + 10);
		history.directApplyRest2StartTime = AttTime.valueOf(11*60 + 30);
		history.directApplyRest2EndTime = AttTime.valueOf(11*60 + 40);
		history.directApplyRest3StartTime = AttTime.valueOf(12*60);
		history.directApplyRest3EndTime = AttTime.valueOf(12*60 + 10);
		history.directApplyRest4StartTime = AttTime.valueOf(14*60 + 30);
		history.directApplyRest4EndTime = AttTime.valueOf(14*60 + 40);
		history.directApplyRest5StartTime = AttTime.valueOf(16*60);
		history.directApplyRest5EndTime = AttTime.valueOf(16*60 + 10);

		// 休暇
		if (! leaveList.isEmpty()) {
			history.leaveCodeList = new List<String>();
			for (AttLeaveEntity leave : leaveList) {
				history.leaveCodeList.add(leave.code);
			}
		}

		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		base.setId(repo.saveEntity(base).details[0].Id);

		// currentHistoryId を設定する
		base = repo.getEntity(base.id, AppDate.today());
		base.currentHistoryId = base.getHistory(0).id;
		repo.saveBaseEntity(base);

		return base;
	}

	// TODO: 勤務パターンオブジェクトを削除したため、一旦コメントアウト
	// // 勤務パターンを作成
	// // FIXME:entityとリポジトリに変更
	// public AttPattern__c createPattern(Integer startTime, Integer endTime) {
	// 	AttPattern__c pattern = new AttPattern__c(
	// 			StartTime__c = startTime,
	// 			EndTime__c = endTime,
	// 			ContractedWorkHours__c = endTime - startTime,
	// 			Rest1StartTime__c = startTime + 60,
	// 			Rest1EndTime__c = startTime + 90,
	// 			Rest2StartTime__c = startTime + 120,
	// 			Rest2EndTime__c = startTime + 150,
	// 			Rest3StartTime__c = startTime + 180,
	// 			Rest3EndTime__c = startTime + 210,
	// 			Rest4StartTime__c = startTime + 240,
	// 			Rest4EndTime__c = startTime + 270,
	// 			Rest5StartTime__c = startTime + 300,
	// 			Rest5EndTime__c = startTime + 330,
	// 			RestTimeWoStartEndTime__c = 1,
	// 			//午前半休
	// 			AMStartTime__c = startTime,
	// 			AMEndTime__c = startTime + 3*60,
	// 			AMContractedWorkHours__c = startTime + 3*60 - startTime,
	// 			AMRest1StartTime__c = startTime + 10,
	// 			AMRest1EndTime__c = startTime + 20,
	// 			AMRest2StartTime__c = startTime + 30,
	// 			AMRest2EndTime__c = startTime + 40,
	// 			AMRest3StartTime__c = startTime + 60,
	// 			AMRest3EndTime__c = startTime + 70,
	// 			AMRest4StartTime__c = startTime + 100,
	// 			AMRest4EndTime__c = startTime + 110,
	// 			AMRest5StartTime__c = startTime + 130,
	// 			AMRest5EndTime__c = startTime + 140,
	// 			AMRestTimeWoStartEndTime__c = 10,
	// 			//午後半休
	// 			PMStartTime__c = startTime + 4*60,
	// 			PMEndTime__c = endTime,
	// 			PMContractedWorkHours__c = startTime - startTime - 4*60,
	// 			PMRest1StartTime__c = startTime + 4*60 + 10,
	// 			PMRest1EndTime__c = startTime + 4*60 + 20,
	// 			PMRest2StartTime__c = startTime + 4*60 + 30,
	// 			PMRest2EndTime__c = startTime + 4*60 + 40,
	// 			PMRest3StartTime__c = startTime + 4*60 + 60,
	// 			PMRest3EndTime__c = startTime + 4*60 + 70,
	// 			PMRest4StartTime__c = startTime + 4*60 + 100,
	// 			PMRest4EndTime__c = startTime + 4*60 + 110,
	// 			PMRest5StartTime__c = startTime + 4*60 + 130,
	// 			PMRest5EndTime__c = startTime + 4*60 + 140,
	// 			PMRestTimeWoStartEndTime__c = 20);
	// 	insert pattern;
	// 	return pattern;
	// }

	/**
	 * 社員に勤務体系を設定する
	 * @param employee 設定対象の社員
	 * @param workingType 設定する勤務体系
	 */
	public void setWorkingType(EmployeeBaseEntity employee, AttWorkingTypeBaseEntity workingType) {
		EmployeeHistoryEntity empHistory = employee.getHistory(0);
		empHistory.workingTypeId = workingType != null ? workingType.id : null;
		new EmployeeRepository().saveHistoryEntity(empHistory);
	}

	/**
	 * テスト用の社員を作成する(実行ユーザ以外のユーザを作成して割りあてる)
	 * 勤務体系は workingType に設定されている勤務体系
	 * @param 作成する社員数
	 * @return テストユーザ（プロパティ testEmployeeList からも取得可能)
	 */
	public List<EmployeeBaseEntity> createTestEmployees(Integer numberOfEmp) {

		// ユーザを作成する
		List<User> testUserList =
				ComTestDataUtility.createUsers(
					'Test', UserInfo.getLanguage(), UserInfo.getLocale(), numberOfEmp);
		List<EmployeeBaseEntity> retList = new List<EmployeeBaseEntity>();
		List<ComEmpHistory__c> empHistoryList = new List<ComEmpHistory__c>();


		// 社員を作成する
		Integer index = 1;
		for (User u : testUserList) {
			EmployeeBaseEntity emp = createEmployee(u.Lastname, u.Id, company.Id);

			empHistoryList.add(new ComEmpHistory__c(
					Id = emp.getHistory(0).id,
					WorkingTypeBaseId__c = workingType.id));
			emp.getHistory(0).workingTypeId = workingType.id; // 上で設定済みのようですが必要？
			retList.add(emp);
		}
		update empHistoryList;

		return retList;
	}

	/**
	 * 標準権限をもつテスト用の社員を作成する(実行ユーザ以外のユーザを作成して割りあてる)
	 * 勤務体系は workingType に設定されている勤務体系
	 * @param 作成する社員数
	 * @return 標準権限をもつテストユーザ（プロパティ testEmployeeList からも取得可能)
	 */
	public List<EmployeeBaseEntity> createTestEmployeesWithStandardProf(Integer numberOfEmp) {

		// ユーザを作成する
		List<User> testUserList = ComTestDataUtility.createStandardUsers('Test', numberOfEmp);
		List<EmployeeBaseEntity> retList = new List<EmployeeBaseEntity>();
		List<ComEmpHistory__c> empHistoryList = new List<ComEmpHistory__c>();

		// 社員を作成する
		Integer index = 1;
		for (User u : testUserList) {
			EmployeeBaseEntity emp = createEmployee(u.Lastname, u.Id, company.Id);

			empHistoryList.add(new ComEmpHistory__c(
					Id = emp.getHistory(0).id,
					WorkingTypeBaseId__c = workingType.id));
			emp.getHistory(0).workingTypeId = workingType.id; // 上で設定済みのようですが必要？
			retList.add(emp);
		}
		update empHistoryList;

		return retList;
	}

	/**
	 *　ユーザの言語を変更する
	 */
	public static void setLanguage(Id userId, String lang) {

		// 社員の言語を組織で設定されているL2する
		User u = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :userId];
		u.LanguageLocaleKey = lang;
		update u;
	}

	/**
	 *　ユーザを取得する
	 */
	public static User getUser(Id userId) {
		return [SELECT Id, Name, LanguageLocaleKey FROM User WHERE Id = :userId];
	}

	/**
	 * 指定した月の勤怠データ（サマリー＋明細）を作成し、DBに登録する(月度専用)
	 * @param employeeId 社員ID
	 * @param workingTypeId 勤務体系ID
	 * @param startDate 開始日
	 * @param createPreNext 対象日の前の月度と次の月度も作成する場合はtrue
	 *
	 */
	public void createAttSummaryWithRecordsMonth(
			Id employeeHistoryId, Id workingTypeHistoryId, AppDate startDate, Boolean createPreNext) {

		createAttSummaryWithRecordsMonth(employeeHistoryId, workingTypeHistoryId, null, startDate, createPreNext);
	}

	/**
	 * 指定した月の勤怠データ（サマリー＋明細）を作成し、DBに登録する(月度専用)
	 * @param employeeId 社員ID
	 * @param workingTypeId 勤務体系ID
	 * @param deptHistoryId 部署ID
	 * @param startDate 開始日
	 * @param createPreNext 対象日の前の月度と次の月度も作成する場合はtrue
	 *
	 */
	public void createAttSummaryWithRecordsMonth(
			Id employeeHistoryId, Id workingTypeHistoryId, Id deptHistoryId, AppDate startDate, Boolean createPreNext) {
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		createAttSummaryWithRecords(employeeHistoryId, workingTypeHistoryId, deptHistoryId, startDate, endDate);
		if (createPreNext) {
			// 前月
			AppDate preStartDate = startDate.addMonths(-1);
			AppDate preEndDate = preStartDate.addMonths(1).addDays(-1);
			createAttSummaryWithRecords(employeeHistoryId, workingTypeHistoryId, deptHistoryId, preStartDate, preEndDate);

			// 翌月
			AppDate nextStartDate = startDate.addMonths(1);
			AppDate nextEndDate = nextStartDate.addMonths(1).addDays(-1);
			createAttSummaryWithRecords(employeeHistoryId, workingTypeHistoryId, deptHistoryId, nextStartDate, nextEndDate);
		}
	}

	/**
	 * 指定した月の勤怠データ（サマリー＋明細）を作成し、DBに登録する
	 * @param employeeId 社員ID
	 * @param workingTypeId 勤務体系ID
	 * @param startDate 開始日
	 * @param endDate 終了日
	 *
	 */
	public void createAttSummaryWithRecords(
			Id employeeHistoryId, Id workingTypeHistoryId, AppDate startDate, AppDate endDate) {
		createAttSummaryWithRecords(
			 employeeHistoryId, workingTypeHistoryId, null, startDate, endDate);
	}

	/**
	 * 指定した月の勤怠データ（サマリー＋明細）を作成し、DBに登録する
	 * @param employeeId 社員ID
	 * @param workingTypeId 勤務体系ID
	 * @param deptHistoryId 部署ID
	 * @param startDate 開始日
	 * @param endDate 終了日
	 *
	 */
	public void createAttSummaryWithRecords(
			Id employeeHistoryId, Id workingTypeHistoryId, Id deptHistoryId, AppDate startDate, AppDate endDate) {

		// サマリーデータ作成
		AttSummary__c summary = new AttSummary__c(
				Name = 'Test' + startDate.format(),
				EmployeeHistoryId__c = employeeHistoryId,
				DeptHistoryId__c = deptHistoryId,
				UniqKey__c = 'Test' + employeeHistoryId + startDate.formatYYYYMMDD(),
				StartDate__c = startDate.getDate(),
				EndDate__c = endDate.getDate(),
				SummaryName__c = startDate.format(AppDate.FormatType.YYYYMM),
				WorkingTypeHistoryId__c = workingTypeHistoryId,
				Dirty__c = true); // 再計算させるため
		insert summary;

		// 明細データ作成
		List<AttRecord__c> records = new List<AttRecord__c>();
		for (Date dt = startDate.getDate(); dt <= endDate.getDate(); dt = dt.addDays(1)) {
			records.add(new AttRecord__c(
					SummaryId__c = summary.Id,
					Date__c = dt,
					OutSummaryStartDate__c = startDate.getDate(),
					OutSummaryEndDate__c = endDate.getDate(),
					UniqKey__c = 'Test'+ employeeHistoryId + AppDate.valueOf(dt).formatYYYYMMDD(),
					InpStartTime__c = AttTime.newInstance(9, dt.day()).getIntValue(),
					InpEndTime__c = AttTime.newInstance(20, dt.day()).getIntValue(),
					InpStartStampTime__c = AttTime.newInstance(9, dt.day() + 10).getIntValue(),
					InpEndStampTime__c = AttTime.newInstance(20, dt.day() + 10).getIntValue(),
					InpRest1StartTime__c = AttTime.newInstance(11, dt.day()).getIntValue(),
					InpRest1EndTime__c = AttTime.newInstance(11, dt.day() + 10).getIntValue(),
					InpRest2StartTime__c = AttTime.newInstance(13, dt.day()).getIntValue(),
					InpRest2EndTime__c = AttTime.newInstance(13, dt.day() + 10).getIntValue(),
					InpRest3StartTime__c = AttTime.newInstance(15, dt.day()).getIntValue(),
					InpRest3EndTime__c = AttTime.newInstance(15, dt.day() + 10).getIntValue(),
					InpRest4StartTime__c = AttTime.newInstance(17, dt.day()).getIntValue(),
					InpRest4EndTime__c = AttTime.newInstance(17, dt.day() + 10).getIntValue(),
					InpRest5StartTime__c = AttTime.newInstance(19, dt.day()).getIntValue(),
					InpRest5EndTime__c = AttTime.newInstance(19, dt.day() + 10).getIntValue(),
					InpRestHours__c = AttDailyTime.valueOf(dt.day() + 10).getIntValue(),
					InpLastUpdateTime__c = Datetime.now()));
		}
		insert records;
	}

	/**
	 * 指定した期間の勤怠明細を取得する
	 */
	public List<AttRecordEntity> getRecordEntities(Id employeeId, AppDate startDate, AppDate endDate) {
		AttSummaryRepository repo = new AttSummaryRepository();
		return repo.searchRecordEntityList(employeeId, startDate, endDate);
	}

	/**
	 * テスト用の勤怠明細レコードを取得する
	 * @param employeeId 社員ID
	 * @param startDate 取得対象開始日
	 * @param endDate 取得対象終了日
	 * @return 勤怠明細レコード一覧
	 */
	public List<AttRecord__c> getRecords(Id employeeId, AppDate startDate, AppDate endDate) {
		return [
				SELECT
					SummaryId__c, Date__c, OutSummaryStartDate__c, OutSummaryEndDate__c,
					UniqKey__c, InpStartTime__c, InpEndTime__c,
					InpStartStampTime__c, InpEndStampTime__c,
					InpRest1StartTime__c, InpRest1EndTime__c,
					InpRest2StartTime__c, InpRest2EndTime__c,
					InpRest3StartTime__c, InpRest3EndTime__c,
					InpRest4StartTime__c, InpRest4EndTime__c,
					InpRest5StartTime__c, InpRest5EndTime__c,
					OutDayType__c, OutRestTime__c, OutRealWorkTime__c,
					CusWorkTime01__c, CusWorkTime02__c, CusWorkTime03__c,
					CusWorkTime04__c, CusWorkTime05__c, CusWorkTime06__c,
					OutLateArriveLostTime__c, OutEarlyLeaveLostTime__c, OutRestLostTime__c,
					OutShiftStartTime__c, OutShiftEndTime__c,
					OutRest1StartTime__c, OutRest1EndTime__c,
					OutRest2StartTime__c, OutRest2EndTime__c,
					OutRest3StartTime__c, OutRest3EndTime__c,
					OutRest4StartTime__c, OutRest4EndTime__c,
					OutRest5StartTime__c, OutRest5EndTime__c,
					OutInsufficientRestTime__c, OutVirtualWorkTime__c,
					OutStartTime__c, OutEndTime__c
				FROM
					AttRecord__c
				WHERE
					SummaryId__r.EmployeeHistoryId__r.BaseId__c = :employeeId
					AND Date__c >= :startDate.getDate() AND Date__c <= :endDate.getDate()
				ORDER BY Date__c Asc];
	}

	/**
	 * 休暇マスタのテストデータを作成する
	 */
	public List<AttLeaveEntity> createLeaveList() {

		List<AttLeaveEntity> leaves = new List<AttLeaveEntity>();
		leaves.add(
				createLeave('001', '有休', AttLeaveType.PAID, false, new List<AttLeaveRange>{
					AttLeaveRange.RANGE_DAY,
					AttLeaveRange.RANGE_AM,
					AttLeaveRange.RANGE_PM,
					AttLeaveRange.RANGE_HALF,
					AttLeaveRange.RANGE_TIME}));
		leaves.add(
				createLeave('002', '無休', AttLeaveType.UNPAID, false, new List<AttLeaveRange>{
					AttLeaveRange.RANGE_DAY}));
		return leaves;
	}

	/**
	 * 本インスタンスのleaveListのデータを削除する
	 */
	public void deleteLeave() {
		List<Id> deleteIds = new List<Id>();
		for (AttLeaveEntity leave : this.leaveList) {
			deleteIds.add(leave.id);
		}
		delete [SELECT Id FROM AttLeave__c WHERE Id IN :deleteIds];
		this.leaveList = null;
	}

	/**
	 * テスト用の休暇マスタを作成する
	 * @param code コード
	 * @param name 休暇名
	 * @param leaveType 休暇タイプ
	 * @param daysManaged 日数管理
	 * @param leaveRange 休暇範囲
	 * @return 作成した休暇マスタ
	 */
	public AttLeaveEntity createLeave(String code, String name, AttLeaveType leaveType, boolean daysManaged, List<AttLeaveRange> leaveRanges) {
		return createLeaves(code, name, leaveType, daysManaged, leaveRanges, 1)[0];
	}

	/**
	 * テスト用の休暇マスタを一括作成する
	 * @param code コード
	 * @param name 休暇名
	 * @param leaveType 休暇タイプ
	 * @param daysManaged 日数管理
	 * @param leaveRange 休暇範囲
	 * @param size 作成件数
	 * @return 作成した休暇マスタ
	 */
	public List<AttLeaveEntity> createLeaves(String code, String name, AttLeaveType leaveType, boolean daysManaged, List<AttLeaveRange> leaveRanges, Integer size) {
		List<AttLeave__c> attLeaveList = new List<AttLeave__c>();
		for (Integer i = 0; i < size; i++) {
			String num = size > 1 ? String.valueOf(i + 1) : '';
			AttLeave__c attLeave = new AttLeave__c(
				Name = name + num,
				CompanyId__c = this.company.Id,
				Code__c = code + num,
				UniqKey__c = code + num,
				Name_L0__c = name + num + 'L0',
				Name_L1__c = name + num + 'L1',
				Name_L2__c = name + num + 'L2',
				Type__c = leaveType.value,
				DaysManaged__c = daysManaged,
				ValidFrom__c = Date.newInstance(1900, 1, 1),
				ValidTo__c = Date.newInstance(2100, 12, 31),
				Range__c = AppConverter.stringValue(leaveRanges),
				RequireReason__c = false
			);
			attLeaveList.add(attLeave);
		}
		insert attLeaveList;

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		List<AttLeaveEntity> entityList = new List<AttLeaveEntity>();
		for (AttLeave__c l:attLeaveList) {
			AttLeaveEntity ent = new AttLeaveEntity(l);
			entityList.add(ent);
		}
		return entityList;
	}

	/**
	 * テスト用の休暇サマリを作成する
	 * @param smmaryId 勤怠サマリID
	 * @param leaveId 付与対象休暇ID
	 * @param daysLeft 残日数
	 * @param daysTaken 消費日数
	 * @return 作成した休暇マスタ
	 */
	public AttManagedLeaveSummaryEntity createLeaveSummary(Id summaryId, Id leaveId, Decimal daysGranted, Decimal daysLeft, Decimal daysTaken) {
		return createLeaveSummaries(summaryId, leaveId, daysGranted, daysLeft, daysTaken, 1)[0];
	}

	/**
	 * テスト用の休暇サマリを一括作成する
	 * @param size 作成件数
	 * @param smmaryId 勤怠サマリID
	 * @param leaveId 付与対象休暇ID
	 * @param daysLeft 残日数
	 * @param daysTaken 消費日数
	 * @return 作成した休暇マスタ
	 */
	public List<AttManagedLeaveSummaryEntity> createLeaveSummaries(Id summaryId, Id leaveId, Decimal daysGranted, Decimal daysLeft, Decimal daysTaken, Integer size) {
		List<AttManagedLeaveSummary__c> leaveSummaryList = new List<AttManagedLeaveSummary__c>();
		for (Integer i = 0; i < size; i++) {
			String num = size > 1 ? String.valueOf(i + 1) : '';
			AttManagedLeaveSummary__c leaveSummary = new AttManagedLeaveSummary__c(
				AttSummaryId__c = summaryId,
				LeaveId__c = leaveId,
//				DaysGranted__c = daysGranted,
				DaysLeft__c = daysLeft,
				DaysTaken__c = daysTaken
			);
			leaveSummaryList.add(leaveSummary);
		}
		insert leaveSummaryList;

		AttManagedLeaveSummaryRepository leaveRep = new AttManagedLeaveSummaryRepository();
		List<AttManagedLeaveSummaryEntity> entityList = new List<AttManagedLeaveSummaryEntity>();
		for (AttManagedLeaveSummary__c l:leaveSummaryList) {
			AttManagedLeaveSummaryEntity ent = leaveRep.createEntity(l);
			entityList.add(ent);
		}
		return entityList;
	}

	/**
	 * テスト用の休暇付与マスタを作成する
	 * @param empId　社員ID
	 * @param leaveId 休暇ID
	 * @param daysGranted 付与日数
	 * @param daysLeft 残日数
	 * @param validFrom 利用可能開始日
	 * @param validTO 利用可能開始失効日
	 * @return 作成した休暇付与マスタ
	 */
	public AttManagedLeaveGrantEntity createLeaveGrant(Id empId, Id leaveId, Decimal daysGranted, Decimal daysLeft,AppDate validFrom,AppDate validTo) {
		return createLeaveGrants(empId, leaveId, daysGranted, daysLeft, validFrom, validTo, 1)[0];
	}

	/**
	 * テスト用の休暇付与マスタを一括作成する
	 * @param empId　社員ID
	 * @param leaveId 休暇ID
	 * @param daysGranted 付与日数
	 * @param daysLeft 残日数
	 * @param validFrom 利用可能開始日
	 * @param validTO 利用可能開始失効日
	 * @param size 作成件数
	 * @return 作成した休暇付与マスタリスト
	 */
	public List<AttManagedLeaveGrantEntity> createLeaveGrants(Id empId, Id leaveId, Decimal daysGranted, Decimal daysLeft,AppDate validFrom,AppDate validTo, Integer size) {
		List<AttManagedLeaveGrant__c> leaveGrantList = new List<AttManagedLeaveGrant__c>();
		for (Integer i = 0; i < size; i++) {
			String num = size > 1 ? String.valueOf(i + 1) : '';
			AttManagedLeaveGrant__c leaveGrant = new AttManagedLeaveGrant__c(
				EmployeeBaseId__c = empId,
				LeaveId__c = leaveId,
				DaysGranted__c = daysGranted,
				DaysLeft__c = daysLeft,
				ValidFrom__c = validFrom.getDate(),
			ValidTo__c = validTo.getDate(),
				Comment__c = 'comment' + num
			);
			leaveGrantList.add(leaveGrant);
		}
		insert leaveGrantList;

		AttManagedLeaveGrantRepository leaveGrantRep = new AttManagedLeaveGrantRepository();
		List<AttManagedLeaveGrantEntity> entityList = new List<AttManagedLeaveGrantEntity>();
		for (AttManagedLeaveGrant__c l:leaveGrantList) {
			AttManagedLeaveGrantEntity ent = leaveGrantRep.createEntity(l);
			entityList.add(ent);
		}
		return entityList;
	}

	/**
	 * テスト用の休暇付与サマリを作成する
	 * @param smmaryId 休暇サマリID
	 * @param leaveGrantId 付与対象休暇ID
	 * @param daysLeft 残日数
	 * @param daysTaken 消費日数
	 * @return 作成した休暇マスタ
	 */
	public AttManagedLeaveGrantSummaryEntity createLeaveGrantSummary(Id leaveSummaryId, Id leaveGrantId, Decimal daysLeft, Decimal daysTaken) {
		return createLeaveGrantSummaries(leaveSummaryId, leaveGrantId, daysLeft, daysTaken, 1)[0];
	}

	/**
	 * テスト用の休暇付与サマリを一括作成する
	 * @param smmaryId 休暇サマリID
	 * @param leaveGrantId 付与対象休暇ID
	 * @param daysLeft 残日数
	 * @param daysTaken 消費日数
	 * @param size 作成件数
	 * @return 作成した休暇マスタ
	 */
	public List<AttManagedLeaveGrantSummaryEntity> createLeaveGrantSummaries(Id leaveSummaryId, Id leaveGrantId, Decimal daysLeft, Decimal daysTaken, Integer size) {
		List<AttManagedLeaveGrantSummary__c> leaveGrantSummaryList = new List<AttManagedLeaveGrantSummary__c>();
		for (Integer i = 0; i < size; i++) {
			String num = size > 1 ? String.valueOf(i + 1) : '';
			AttManagedLeaveGrantSummary__c leaveGrantSummary = new AttManagedLeaveGrantSummary__c(
				LeaveSummaryId__c = leaveSummaryId,
				LeaveGrantId__c = leaveGrantId,
				DaysLeft__c = daysLeft,
				DaysTaken__c = daysTaken
			);
			leaveGrantSummaryList.add(leaveGrantSummary);
		}
		insert leaveGrantSummaryList;


		AttManagedLeaveGrantSummaryRepository leaveGrantRep = new AttManagedLeaveGrantSummaryRepository();
		List<AttManagedLeaveGrantSummaryEntity> entityList = new List<AttManagedLeaveGrantSummaryEntity>();
		for (AttManagedLeaveGrantSummary__c l:leaveGrantSummaryList) {
			AttManagedLeaveGrantSummaryEntity ent = leaveGrantRep.createEntity(l);
			entityList.add(ent);
		}
		return entityList;
	}

	/**
	 * テスト用の休暇申請を作成する
	 * @param leaveId 休暇Id
	 * @param leaveRange 休暇範囲
	 * @return 作成した休暇申請
	 */
	public AttDailyRequestEntity createLeaveRequest(Id leaveId, AttLeaveRange leaveRange, Integer startTime, Integer endTime) {
		AttDailyRequest__c attRequest = new AttDailyRequest__c(
				EmployeeHistoryId__c = this.employee.getHistory(0).id,
				OwnerId = UserInfo.getUserId(),
				RequestType__c = AppConverter.stringValue(AttRequestType.LEAVE),
				Status__c = AppConverter.stringValue(AppRequestStatus.PENDING),
				AttLeaveId__c = leaveId,
				AttLeaveType__c = AppConverter.stringValue(AttLeaveType.PAID),
				AttLeaveRange__c = AppConverter.stringValue(leaveRange),
				StartTime__c = startTime,
				EndTime__c = endTime
		);
		insert attRequest;

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		return requestRep.createEntity(attRequest);
	}

	/**
	 * 休暇申請データを作成する(ステータス：申請中)
	 * @param leave 休暇マスタ
	 * @param leaveRange 休暇範囲
	 * @param startDate 開始日
	 * @param endDate 終了日
	 */
	public AttDailyRequestEntity createLeaveRequest(
			AttLeaveEntity leave, AttLeaveRange leaveRange,
			AppDate startDate, AppDate endDate, AttTime startTime, AttTime endTime) {
		return createLeaveRequest(leave, leaveRange, startDate, endDate, startTime, endTime, null);
	}

	/**
	 * 休暇申請データを作成する(ステータス：申請中)
	 * @param leave 休暇マスタ
	 * @param leaveRange 休暇範囲
	 * @param startDate 開始日
	 * @param endDate 終了日
	 */
	public AttDailyRequestEntity createLeaveRequest(
			AttLeaveEntity leave, AttLeaveRange leaveRange,
			AppDate startDate, AppDate endDate, AttTime startTime, AttTime endTime, Decimal leaveDays) {

		AttDailyRequestEntity request = new AttDailyRequestEntity();
		request.employeeId = this.employee.getHistory(0).id;
		request.requestType = AttRequestType.LEAVE;
		request.leaveId = leave.id;
		request.leaveType = leave.leaveType;
		request.leaveRange = leaveRange;
		request.startDate = startDate;
		request.endDate = endDate;
		request.startTime = startTime;
		request.endTime = endTime;
		request.leaveDays = leaveDays;
		request.status = AppRequestStatus.PENDING;
		if (request.startTime != null && request.endTime != null) {
			request.leaveTotalTime = request.endTime.getIntValue() - request.startTime.getIntValue();
		}

		AttDailyRequestRepository repo = new AttDailyRequestRepository();
		Repository.SaveResult result = repo.saveEntity(request);
		request = repo.getLeaveById(result.details[0].id);
		return request;
	}

	/**
	 * 時間外勤務申請データを作成する（ステータス：申請中）
	 * @param requestType 早朝勤務申請/残業申請
	 * @param targetDate 対象日
	 * @param startTime 開始時間
	 * @param endTime 終了時間
	 */
	public AttDailyRequestEntity createEarlyStartWorkOrOvertimeRequest(AttRequestType requestType, AppDate targetDate,
			 AttTime startTime, AttTime endTime) {

		AttDailyRequestEntity request = new AttDailyRequestEntity();
		request.requestType = requestType;
		request.status = AppRequestStatus.PENDING;
		request.startDate = targetDate;
		request.endDate = targetDate;
		request.startTime = startTime;
		request.endTime = endTime;
		request.remarks = '備考';

		AttDailyRequestRepository repo = new AttDailyRequestRepository();
		Repository.SaveResult result = repo.saveEntity(request);
		request = repo.getEntity(result.details[0].id);
		return request;
	}

	/**
	 * テスト用の休日出勤申請を作成する
	 * @return 作成した休暇申請
	 */
	public AttDailyRequestEntity createHolidayWorkRequest(Integer startTime, Integer endTime) {
		AttDailyRequest__c attRequest = new AttDailyRequest__c(
				RequestType__c = AppConverter.stringValue(AttRequestType.HOLIDAYWORK),
				Status__c = AppConverter.stringValue(AppRequestStatus.PENDING),
				StartTime__c = startTime,
				EndTime__c = endTime
		);
		insert attRequest;

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		return requestRep.createEntity(attRequest);
	}
	/**
	 * テスト用の早朝勤務申請を作成する
	 */
	public AttDailyRequestEntity createEarlyStartWorkRequest(Integer startTime, Integer endTime) {
		AttDailyRequest__c attRequest = new AttDailyRequest__c(
				RequestType__c = AppConverter.stringValue(AttRequestType.EARLY_START_WORK),
				Status__c = AppConverter.stringValue(AppRequestStatus.PENDING),
				StartTime__c = startTime,
				EndTime__c = endTime
		);
		insert attRequest;

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		return requestRep.createEntity(attRequest);
	}
	/**
	 * テスト用の残業申請を作成する
	 */
	public AttDailyRequestEntity createOvertimeWorkRequest(Integer startTime, Integer endTime) {
		AttDailyRequest__c attRequest = new AttDailyRequest__c(
				RequestType__c = AppConverter.stringValue(AttRequestType.OVERTIME_WORK),
				Status__c = AppConverter.stringValue(AppRequestStatus.PENDING),
				StartTime__c = startTime,
				EndTime__c = endTime
		);
		insert attRequest;

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		return requestRep.createEntity(attRequest);
	}

	/**
	 * テスト用の休日出勤申請を作成する
	 * @return 休日出勤申請
	 */
	public AttDailyRequestEntity createHolidayWorkRequest(AppDate targetDate, AttTime startTime, AttTime endTime) {
		AttDailyRequest__c attRequest = new AttDailyRequest__c(
				RequestType__c = AppConverter.stringValue(AttRequestType.HOLIDAYWORK),
				StartDate__c = targetDate.getDate(),
				EndDate__c = targetDate.getDate(),
				Status__c = AppConverter.stringValue(AppRequestStatus.PENDING),
				StartTime__c = startTime.getIntValue(),
				EndTime__c = endTime.getIntValue()
		);
		insert attRequest;

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		return requestRep.createEntity(attRequest);
	}
	/**
	 * テスト用の直行直帰申請を作成する
	 */
	public AttDailyRequestEntity createDirectRequest(AppDate startDate, AppDate endDate, Integer startTime, Integer endTime) {
		AttDailyRequest__c attRequest = new AttDailyRequest__c(
				RequestType__c = AppConverter.stringValue(AttRequestType.DIRECT),
				Status__c = AppConverter.stringValue(AppRequestStatus.PENDING),
				StartDate__c = startDate.getDate(),
				EndDate__c = endDate.getDate(),
				StartTime__c = startTime,
				EndTime__c = endTime,
				Rest1StartTime__c = startTime + 10,
				Rest1EndTime__c = startTime + 20,
				Rest2StartTime__c = startTime + 30,
				Rest2EndTime__c = startTime + 40,
				Rest3StartTime__c = startTime + 50,
				Rest3EndTime__c = startTime + 60,
				Rest4StartTime__c = startTime + 70,
				Rest4EndTime__c = startTime + 80,
				Rest5StartTime__c = startTime + 90,
				Rest5EndTime__c = startTime + 100
		);
		insert attRequest;

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		return requestRep.createEntity(attRequest);
	}

	/**
	 * テスト用の勤務時間変更申請を作成する
	 * @param patternId 勤務パターンId
	 * @param leaveRange 休暇範囲
	 * @return 作成した休暇申請
	 */
	public AttDailyRequestEntity createPatternRequest(Id patternId, AppDate startDate, AppDate endDate) {
		AttDailyRequest__c attRequest = new AttDailyRequest__c(
				EmployeeHistoryId__c = this.employee.getHistory(0).id,
				OwnerId = UserInfo.getUserId(),
				RequestType__c = AppConverter.stringValue(AttRequestType.PATTERN),
				Status__c = AppConverter.stringValue(AppRequestStatus.PENDING),
				AttPatternId__c = patternId,
				StartDate__c = AppDate.convertDate(startDate),
				EndDate__c = AppDate.convertDate(endDate)
		);
		insert attRequest;

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		return requestRep.createEntity(attRequest);
	}

	// 勤怠確定申請を作成
	public AttRequestEntity createRequest(Id employeeHistoryId, Id summaryId, AppRequestStatus status, AppCancelType cancelType) {
		AttRequest__c request = new AttRequest__c(
				Name = 'Test Name',
				AttSummaryId__c = summaryId == null ? null : summaryId,
				Comment__c = 'Test Comment',
				ProcessComment__c = 'Test ProcessComment',
				Status__c = status.value,
				CancelType__c = cancelType != null ? cancelType.value : null,
				EmployeeHistoryId__c = employeeHistoryId,
				RequestTime__c = System.now());
		insert request;

		AttRequestRepository requestRep = new AttRequestRepository();
		return requestRep.getEntity(request.Id);
	}

	/** 指定した社員を取得する */
	public EmployeeBaseEntity getEmp(Id empId, AppDate targetDate) {
		return new EmployeeRepository().getEntity(empId, targetDate);
	}

	public AttShortTimeSettingBaseEntity createShortSetting(AttWorkSystem workSystem, String code, List<AppDate> validFromList) {
		ComTag__c tagTypeObj =
					ComTestDataUtility.createTag('xx', TagType.SHORTEN_WORK_REASON, this.company.id, 0, false);

		AttShortTimeSettingBaseEntity base = new AttShortTimeSettingBaseEntity();
		base.companyId = this.company.Id;
		base.code = code;
		base.workSystem = workSystem;
		base.name = code + '_l0';
		base.reasonId = tagTypeObj.id;

		if (validFromList == null || validFromList.isEmpty()) {
				validFromList = new List<AppDate>{ValidPeriodEntity.VALID_FROM_MIN};
		}
		else {
			validFromList.add(0, ValidPeriodEntity.VALID_FROM_MIN);
		}
		List<AttShortTimeSettingHistoryEntity> histList =new List<AttShortTimeSettingHistoryEntity>();
		for (AppDate validFrom : validFromList) {
			AttShortTimeSettingHistoryEntity hist = new AttShortTimeSettingHistoryEntity();
			hist.name = hist.nameL0 = hist.nameL1= base.name;
			hist.validFrom = validFrom;
			if (workSystem == AttWorkSystem.JP_FLEX) {
				// フレックス制は私用外出時間の上限を設定できないため0にする
				hist.allowableTimeOfShortWork = 60 + histList.size();
				hist.allowableTimeOfLateArrival = 30 + histList.size();
				hist.allowableTimeOfEarlyLeave = 30 + histList.size();
				hist.allowableTimeOfIrregularRest = 0;
			} else {
				hist.allowableTimeOfShortWork = 90 + histList.size();
				hist.allowableTimeOfLateArrival = 30 + histList.size();
				hist.allowableTimeOfEarlyLeave = 30 + histList.size();
				hist.allowableTimeOfIrregularRest = 30 + histList.size();
			}
			histList.add(hist);
		}
		for (Integer i = 0; i <= histList.size() - 2; i++) {
			histList[i].validTo = histList[i+1].validFrom;
		}
		histList[histList.size()-1].validTo = ParentChildHistoryEntity.VALID_TO_MAX;
		for (AttShortTimeSettingHistoryEntity hist : histList) {
			base.addHistory(hist);
		}
		AttShortTimeSettingService service = new AttShortTimeSettingService();
		service.updateUniqKey(base);
		Id resId = service.saveNewEntity(base);

		return new AttShortTimeSettingRepository().getEntity(resId);
	}
	// 短時間勤務の勤怠期間を作成
	public AttPeriodStatusEntity createShortTimePeriodStatus(Id periodStausId, Id empId, AppDate validFrom, AppDate validTo, Id shortSettingId) {
		AttPeriodStatusEntity periodStatusData = new AttPeriodStatusEntity();
		periodStatusData.setId(periodStausId);
		periodStatusData.employeeBaseId = empId;
		periodStatusData.validFrom = validFrom;
		periodStatusData.validTo = validTo;
		periodStatusData.shortTimeWorkSettingBaseId = shortSettingId;
		periodStatusData.type = AttPeriodStatusType.SHORT_TIME_WORK_SETTING;

		Repository.SaveResult saveResult = new AttPeriodStatusRepository().saveEntity(periodStatusData);
		periodStatusData.setId(saveResult.details[0].id);
		return periodStatusData;
	}
	public AttLeaveOfAbsenceEntity createLeaveOfAbsence(String code) {
		AttLeaveOfAbsence__c absence = new AttLeaveOfAbsence__c();
		absence.CompanyId__c = this.company.Id;
		absence.Code__c = code;
		absence.Name = code + '_l0';
		absence.Name_L0__c = code + '_l0';
		absence.Name_L1__c = code + '_l1';
		absence.UniqKey__c = code;
		insert absence;
		AttLeaveOfAbsenceEntity retData = new AttLeaveOfAbsenceEntity();
		retData.setId(absence.id);
		return retData;
	}
	// 休職休業の勤怠期間を作成
	public AttPeriodStatusEntity createAbsencePeriodStatus(Id periodStausId, Id empId, AppDate validFrom, AppDate validTo, Id leaveOfAbsenceId) {
		AttPeriodStatusEntity periodStatusData = new AttPeriodStatusEntity();
		periodStatusData.setId(periodStausId);
		periodStatusData.employeeBaseId = empId;
		periodStatusData.validFrom = validFrom;
		periodStatusData.validTo = validTo;
		periodStatusData.leaveOfAbsenceId = leaveOfAbsenceId;
		periodStatusData.type = AttPeriodStatusType.ABSENCE;

		Repository.SaveResult saveResult = new AttPeriodStatusRepository().saveEntity(periodStatusData);
		periodStatusData.setId(saveResult.details[0].id);
		return periodStatusData;
	}
	// 勤務パターンを作成
	public AttPatternEntity createPattern(AttWorkSystem wkType, String code) {
		AttPatternEntity patternData = this.workingType.getHistory(0).createPatternEntity();
		patternData.companyId = this.company.id;
		patternData.workSystem = wkType;
		patternData.code = code;
		patternData.nameL0 = code + 'l0';
		patternData.nameL1 = code + 'l1';
		patternData.uniqKey = this.company.Id + code;
		Repository.SaveResult saveResult = new AttPatternRepository2().saveEntity(patternData);
		patternData.setId(saveResult.details[0].id);
		return patternData;
	}
	/**
	 * 対象日の出退勤時刻を削除する
	 * @param empId 社員ID
	 * @param startDate 開始日
	 * @param endDate 終了日
	 */
	public void deleteInpStartEndTime(Id empId, AppDate startDate, AppDate endDate) {
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		List<AttRecordEntity> attRecords = summaryRepo.searchRecordEntityList(empId, startDate, endDate);
		for (AttRecordEntity record : attRecords) {
			record.inpStartTime = null;
			record.inpEndTime = null;
		}
		summaryRepo.saveRecordEntityList(attRecords);
	}
}