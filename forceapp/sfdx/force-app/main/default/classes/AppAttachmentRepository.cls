/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group アプリ
 *
 * 添付ファイルのリポジトリクラス
 */
public with sharing class AppAttachmentRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				Attachment.Id,
				Attachment.Name,
				Attachment.Body,
				Attachment.ParentId
		};
	}

	/**
	 * 添付ファイルを保存する
	 * @param entity 保存対象の添付ファイル
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AppAttachmentEntity entity) {
		return saveEntityList(new List<AppAttachmentEntity>{entity});
	}

	/** すべてのレコードを保存する(1レコードでも失敗したらロールバック) */
	private static final Boolean IS_ALL_SAVE = true;
	/**
	 * 添付ファイルを保存する
	 * @param entityList 保存対象の添付ファイル
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AppAttachmentEntity> entityList) {

		List<Attachment> saveObjList = new List<Attachment>();

		// エンティティからSObjectを作成する
		for (AppAttachmentEntity entity : entityList) {
			saveObjList.add(createObj(entity));
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(saveObjList, IS_ALL_SAVE);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}


	/**
	 * 添付ファイルを取得する
	 * @param parentId 添付先のオブジェクトのID
	 * @param name 添付ファイル名
	 */
	public List<AppAttachmentEntity> getEntityListByParentId(Id parentId) {
		AppAttachmentRepository.SearchFilter filter = new SearchFilter();
		filter.parentIds = new List<Id>{parentId};
		return searchEntityList(filter);
	}

	/**
	 * 添付ファイルを取得する
	 * @param parentIds 添付先のオブジェクトIDのリスト
	 */
	public List<AppAttachmentEntity> getEntityListByParentId(List<Id> parentIds) {
		AppAttachmentRepository.SearchFilter filter = new SearchFilter();
		filter.parentIds = parentIds;
		return searchEntityList(filter);
	}

	/**
	 * 添付ファイルを取得する
	 * @param parentId 添付先のオブジェクトのID
	 * @param name 添付ファイル名
	 */
	public List<AppAttachmentEntity> getEntityListByParentId(Id parentId, String name) {
		AppAttachmentRepository.SearchFilter filter = new SearchFilter();
		filter.parentIds = new List<Id>{parentId};
		filter.name = name;
		return searchEntityList(filter);
	}

	/**
	 * 検索条件
	 */
	private class SearchFilter {
		/** 添付ファイルID */
		List<Id> ids;
		/** 親ID(添付先のオブジェクトID) */
		List<Id> parentIds;
		/** ファイル名 */
		String name;
	}

	/**
	 * 添付ファイルを検索する
	 * @param filter 検索条件
	 * @return 条件に一致した申請一覧
	 */
	private List<AppAttachmentEntity> searchEntityList(
			AppAttachmentRepository.SearchFilter filter) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> ids = filter.ids;
		final List<Id> parentIds = filter.parentIds;
		final List<String> names = filter.name == null ? null : new List<String>{filter.name};
		if (ids != null) {
			condList.add(getFieldName(Attachment.Id) + ' IN :ids');
		}
		if (parentIds != null) {
			condList.add(getFieldName(Attachment.ParentId) + ' IN :parentIds');
		}
		if (names != null) {
			condList.add(getFieldName(Attachment.Name) + ' IN :names');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + Attachment.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond
				+ ' ORDER BY ' + getFieldName(Attachment.LastModifiedDate) + ' Desc';

		// クエリ実行
		System.debug('AppAttachmentepository.searchEntityList: SOQL ==>' + soql);
		List<Attachment> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<AppAttachmentEntity> entityList = new List<AppAttachmentEntity>();
		for (Attachment attDailyRequest : sObjList) {
			entityList.add(createEntity(attDailyRequest));
		}

		return entityList;
	}

	/**
	 * AttRequest__cからAttRequestEntityを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AppAttachmentEntity createEntity(Attachment sObj) {
		AppAttachmentEntity entity = new AppAttachmentEntity();
		entity.setId(sObj.Id);
		entity.name = sObj.Name;
		entity.body = sObj.Body;
		entity.parentId = sObj.ParentId;

		entity.resetChanged();
		return entity;
	}

	/**
	 * エンティティからSObjectを作成する
	 * @param entity 作成元のエンティティ
	 * @return SObject
	 */
	private Attachment createObj(AppAttachmentEntity entity) {
		Attachment sObj = new Attachment();
		sObj.Id = entity.id;
		if (entity.isChanged(AppAttachmentEntity.Field.NAME)) {
			sObj.Name = entity.name;
		}
		if (entity.isChanged(AppAttachmentEntity.Field.BODY)) {
			sObj.Body = entity.body;
		}

		if (entity.isChanged(AppAttachmentEntity.Field.PARENT_ID)) {
			// 新規のみ更新可能
			if (entity.id == null) {
				sObj.ParentId = entity.parentId;
			} else {
				// ログに警告を出しておく
				System.debug('添付ファイルの親IDは新規の場合のみ設定可能です。');
			}
		}

		return sObj;
	}
}
