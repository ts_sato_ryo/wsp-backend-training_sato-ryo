/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttAgreementAlertSettingEntityのテスト
 */
@isTest
private class AttAgreementAlertSettingEntityTest {

	/**
	 * テストデータクラス
	 */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * 残業設定オブジェクトを作成する
		 */
		public List<AttAgreementAlertSettingEntity> createAgreementAlertSettings(String name, Integer size) {
			return createAgreementAlertSettings(name, this.company.id, size);
		}

		/**
		 * 残業設定オブジェクトを作成する
		 */
		public List<AttAgreementAlertSettingEntity> createAgreementAlertSettings(
				String name, Id companyId, Integer size) {
			ComTestDataUtility.createAgreementAlertSettings(name, companyId, size);
			return (new AttAgreementAlertSettingRepository()).getEntityList(null);
		}

		/**
		 * アラート設定エンティティを作成する(DB保存なし)
		 */
		public AttAgreementAlertSettingEntity createAgreementAlertSettingEntity(String name) {
			AttAgreementAlertSettingEntity entity = new AttAgreementAlertSettingEntity();

			entity.name = name;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.code = name;
			entity.uniqKey = this.company.code + '-' + entity.code;
			entity.companyId = this.company.id;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);
			entity.monthlyAgreementHourWarning1 = 30;
			entity.monthlyAgreementHourWarning2 = 40;
			entity.monthlyAgreementHourLimit = 45;
			entity.monthlyAgreementHourWarningSpecial1 = 50;
			entity.monthlyAgreementHourWarningSpecial2 = 55;
			entity.monthlyAgreementHourLimitSpecial = 60;

			return entity;
		}
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		AttAgreementAlertSettingEntity entity = new AttAgreementAlertSettingEntity();

		// 会社コードと36協定アラート設定コードが空でない場合
		// ユニークキーが作成される
		entity.code = 'SettingCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// 36協定アラート設定コードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'SettingCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/**
	 * copyのテスト
	 * エンティティが複製できることを確認する
	 */
	@isTest static void copyTest() {
		AttAgreementAlertSettingEntityTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity orgEntity = testData.createAgreementAlertSettingEntity('Test');

		AttAgreementAlertSettingEntity copyEntity = orgEntity.copy();

		System.assertEquals(null, copyEntity.id);  // IDはコピーされない
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.validFrom, copyEntity.validFrom);
		System.assertEquals(orgEntity.validTo, copyEntity.validTo);
		System.assertEquals(orgEntity.nameL0, copyEntity.nameL0);
		System.assertEquals(orgEntity.nameL1, copyEntity.nameL1);
		System.assertEquals(orgEntity.nameL2, copyEntity.nameL2);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.monthlyAgreementHourWarning1, copyEntity.monthlyAgreementHourWarning1);
		System.assertEquals(orgEntity.monthlyAgreementHourWarning2, copyEntity.monthlyAgreementHourWarning2);
		System.assertEquals(orgEntity.monthlyAgreementHourLimit, copyEntity.monthlyAgreementHourLimit);
		System.assertEquals(orgEntity.monthlyAgreementHourWarningSpecial1, copyEntity.monthlyAgreementHourWarningSpecial1);
		System.assertEquals(orgEntity.monthlyAgreementHourWarningSpecial2, copyEntity.monthlyAgreementHourWarningSpecial2);
		System.assertEquals(orgEntity.monthlyAgreementHourLimitSpecial, copyEntity.monthlyAgreementHourLimitSpecial);

	}
}
