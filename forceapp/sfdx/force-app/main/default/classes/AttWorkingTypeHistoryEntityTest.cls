/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * @description 勤務体系履歴エンティティのテスト
 */
@isTest
private class AttWorkingTypeHistoryEntityTest {

	/**
	 * エンティティを複製できることを確認する
	 */
	@isTest static void copyTest() {
		TestData.TestDataEntity data = new TestData.TestDataEntity();
		List<String> leaveCodeList = new List<String>{'leave001', 'leave002'};
		AttWorkingTypeHistoryEntity history = TestData.createWorkingTypeHistoryEntity('EntityTest', data.workingType.id, leaveCodeList);
		history.patternCodeList = new List<String>{'pattern001', 'pattern002'};

		AttWorkingTypeHistoryEntity copyHistory = history.copy();

		System.assertEquals(history.name, copyHistory.name);
		System.assertEquals(history.baseId, copyHistory.baseId);
		System.assertEquals(history.validFrom, copyHistory.validFrom);
		System.assertEquals(history.validTo, copyHistory.validTo);
		System.assertEquals(history.historyComment, copyHistory.historyComment);
		System.assertEquals(history.uniqKey, copyHistory.uniqKey);
		System.assertEquals(history.isRemoved, copyHistory.isRemoved);

		System.assertEquals(history.nameL0, copyHistory.nameL0);
		System.assertEquals(history.nameL1, copyHistory.nameL1);
		System.assertEquals(history.nameL2, copyHistory.nameL2);
		System.assertEquals(history.weeklyDayTypeSUN, copyHistory.weeklyDayTypeSUN);
		System.assertEquals(history.weeklyDayTypeMON, copyHistory.weeklyDayTypeMON);
		System.assertEquals(history.weeklyDayTypeTUE, copyHistory.weeklyDayTypeTUE);
		System.assertEquals(history.weeklyDayTypeWED, copyHistory.weeklyDayTypeWED);
		System.assertEquals(history.weeklyDayTypeTHU, copyHistory.weeklyDayTypeTHU);
		System.assertEquals(history.weeklyDayTypeFRI, copyHistory.weeklyDayTypeFRI);
		System.assertEquals(history.weeklyDayTypeSAT, copyHistory.weeklyDayTypeSAT);
		System.assertEquals(history.automaticLegalHolidayAssign, copyHistory.automaticLegalHolidayAssign);
		System.assertEquals(history.boundaryOfStartTime, copyHistory.boundaryOfStartTime);
		System.assertEquals(history.boundaryOfEndTime, copyHistory.boundaryOfEndTime);
		System.assertEquals(history.automaticLegalHolidayAssign, copyHistory.automaticLegalHolidayAssign);
		System.assertEquals(history.divideWorkHoursAtDayBoundary, copyHistory.divideWorkHoursAtDayBoundary);
		System.assertEquals(history.startOfNightWork, copyHistory.startOfNightWork);
		System.assertEquals(history.endOfNightWork, copyHistory.endOfNightWork);
		System.assertEquals(history.legalHolidayAssignmentDays, copyHistory.legalHolidayAssignmentDays);
		System.assertEquals(history.legalHolidayAssignmentPeriod, copyHistory.legalHolidayAssignmentPeriod);
		System.assertEquals(history.legalWorkTimeADay, copyHistory.legalWorkTimeADay);
		System.assertEquals(history.useAMHalfDayLeave, copyHistory.useAMHalfDayLeave);
		System.assertEquals(history.usePMHalfDayLeave, copyHistory.usePMHalfDayLeave);
		System.assertEquals(history.useHalfDayLeave, copyHistory.useHalfDayLeave);
		System.assertEquals(history.legalWorkTimeAWeek, copyHistory.legalWorkTimeAWeek);
		System.assertEquals(history.offsetOvertimeAndDeductionTime, copyHistory.offsetOvertimeAndDeductionTime);
		System.assertEquals(history.allowanceOnEveryHoliday, copyHistory.allowanceOnEveryHoliday);
		System.assertEquals(history.allowanceForOverContracted, copyHistory.allowanceForOverContracted);
		System.assertEquals(history.basicSalaryForLegalHoliday, copyHistory.basicSalaryForLegalHoliday);
		System.assertEquals(history.legalHolidayAllowanceForAll, copyHistory.legalHolidayAllowanceForAll);
		System.assertEquals(history.isIncludeHolidayWorkInPlainTime, copyHistory.isIncludeHolidayWorkInPlainTime);
		System.assertEquals(history.allowWorkDuringHalfDayLeave, copyHistory.allowWorkDuringHalfDayLeave);
		// 勤務時間系
		System.assertEquals(history.startTime, copyHistory.startTime);
		System.assertEquals(history.endTime, copyHistory.endTime);
		System.assertEquals(history.contractedWorkHours, copyHistory.contractedWorkHours);
		System.assertEquals(history.rest1StartTime, copyHistory.rest1StartTime);
		System.assertEquals(history.rest1EndTime, copyHistory.rest1EndTime);
		System.assertEquals(history.rest2StartTime, copyHistory.rest2StartTime);
		System.assertEquals(history.rest2EndTime, copyHistory.rest2EndTime);
		System.assertEquals(history.rest3StartTime, copyHistory.rest3StartTime);
		System.assertEquals(history.rest3EndTime, copyHistory.rest3EndTime);
		System.assertEquals(history.rest4StartTime, copyHistory.rest4StartTime);
		System.assertEquals(history.rest4EndTime, copyHistory.rest4EndTime);
		System.assertEquals(history.rest5StartTime, copyHistory.rest5StartTime);
		System.assertEquals(history.rest5EndTime, copyHistory.rest5EndTime);
		System.assertEquals(history.restTimeWoStartEndTime, copyHistory.restTimeWoStartEndTime);
		// 午前半休
		System.assertEquals(history.amStartTime, copyHistory.amStartTime);
		System.assertEquals(history.amEndTime, copyHistory.amEndTime);
		System.assertEquals(history.amContractedWorkHours, copyHistory.amContractedWorkHours);
		System.assertEquals(history.amRest1StartTime, copyHistory.amRest1StartTime);
		System.assertEquals(history.amRest1EndTime, copyHistory.amRest1EndTime);
		System.assertEquals(history.amRest2StartTime, copyHistory.amRest2StartTime);
		System.assertEquals(history.amRest2EndTime, copyHistory.amRest2EndTime);
		System.assertEquals(history.amRest3StartTime, copyHistory.amRest3StartTime);
		System.assertEquals(history.amRest3EndTime, copyHistory.amRest3EndTime);
		System.assertEquals(history.amRest4StartTime, copyHistory.amRest4StartTime);
		System.assertEquals(history.amRest4EndTime, copyHistory.amRest4EndTime);
		System.assertEquals(history.amRest5StartTime, copyHistory.amRest5StartTime);
		System.assertEquals(history.amRest5EndTime, copyHistory.amRest5EndTime);
		System.assertEquals(history.amRestTimeWoStartEndTime, copyHistory.amRestTimeWoStartEndTime);
		// 午後半休
		System.assertEquals(history.pmStartTime, copyHistory.pmStartTime);
		System.assertEquals(history.pmEndTime, copyHistory.pmEndTime);
		System.assertEquals(history.pmContractedWorkHours, copyHistory.pmContractedWorkHours);
		System.assertEquals(history.pmRest1StartTime, copyHistory.pmRest1StartTime);
		System.assertEquals(history.pmRest1EndTime, copyHistory.pmRest1EndTime);
		System.assertEquals(history.pmRest2StartTime, copyHistory.pmRest2StartTime);
		System.assertEquals(history.pmRest2EndTime, copyHistory.pmRest2EndTime);
		System.assertEquals(history.pmRest3StartTime, copyHistory.pmRest3StartTime);
		System.assertEquals(history.pmRest3EndTime, copyHistory.pmRest3EndTime);
		System.assertEquals(history.pmRest4StartTime, copyHistory.pmRest4StartTime);
		System.assertEquals(history.pmRest4EndTime, copyHistory.pmRest4EndTime);
		System.assertEquals(history.pmRest5StartTime, copyHistory.pmRest5StartTime);
		System.assertEquals(history.pmRest5EndTime, copyHistory.pmRest5EndTime);
		System.assertEquals(history.pmRestTimeWoStartEndTime, copyHistory.pmRestTimeWoStartEndTime);
		// 時間指定半休
		System.assertEquals(history.halfDayLeaveHours, copyHistory.halfDayLeaveHours);

		// 申請系
		System.assertEquals(history.addCompensationTimeToWorkHours, copyHistory.addCompensationTimeToWorkHours);
		System.assertEquals(history.permitOvertimeWorkUntilContractedHours, copyHistory.permitOvertimeWorkUntilContractedHours);
		System.assertEquals(history.allowToChangeApproverSelf, copyHistory.allowToChangeApproverSelf);

		// 関連リスト系オプション
		System.assertEquals(history.leaveCodeList.size(), copyHistory.leaveCodeList.size());
		for (Integer i = 0; i < history.leaveCodeList.size(); i++) {
			System.assertEquals(history.leaveCodeList[i], copyHistory.leaveCodeList[i]);
		}
		copyHistory.leaveCodeList.add('copy entity only'); // リストが複製されていることを確認
		System.assertEquals(copyHistory.leaveCodeList.size() - 1, history.leaveCodeList.size());
		System.assertEquals(history.patternCodeList.size(), copyHistory.patternCodeList.size());
		for (Integer i = 0; i < history.patternCodeList.size(); i++) {
			System.assertEquals(history.patternCodeList[i], copyHistory.patternCodeList[i]);
		}

		// 利用機能系オプション
		System.assertEquals(history.allocatedSubstituteLeaveType, copyHistory.allocatedSubstituteLeaveType);
		System.assertEquals(history.useAbsenceApply, copyHistory.useAbsenceApply);

		System.assertEquals(history.useLegalRestCheck1, copyHistory.useLegalRestCheck1);
		System.assertEquals(history.legalRestCheck1WorkTimeThreshold, copyHistory.legalRestCheck1WorkTimeThreshold);
		System.assertEquals(history.legalRestCheck1RequiredRestTime, copyHistory.legalRestCheck1RequiredRestTime);
		System.assertEquals(history.useLegalRestCheck2, copyHistory.useLegalRestCheck2);
		System.assertEquals(history.legalRestCheck2WorkTimeThreshold, copyHistory.legalRestCheck2WorkTimeThreshold);
		System.assertEquals(history.legalRestCheck2RequiredRestTime, copyHistory.legalRestCheck2RequiredRestTime);

		// 直行直帰申請
		System.assertEquals(history.useDirectApply, copyHistory.useDirectApply);
		System.assertEquals(history.directApplyStartTime, copyHistory.directApplyStartTime);
		System.assertEquals(history.directApplyEndTime, copyHistory.directApplyEndTime);
		System.assertEquals(history.directApplyRest1StartTime, copyHistory.directApplyRest1StartTime);
		System.assertEquals(history.directApplyRest1EndTime, copyHistory.directApplyRest1EndTime);
		System.assertEquals(history.directApplyRest2StartTime, copyHistory.directApplyRest2StartTime);
		System.assertEquals(history.directApplyRest2EndTime, copyHistory.directApplyRest2EndTime);
		System.assertEquals(history.directApplyRest3StartTime, copyHistory.directApplyRest3StartTime);
		System.assertEquals(history.directApplyRest3EndTime, copyHistory.directApplyRest3EndTime);
		System.assertEquals(history.directApplyRest4StartTime, copyHistory.directApplyRest4StartTime);
		System.assertEquals(history.directApplyRest4EndTime, copyHistory.directApplyRest4EndTime);
		System.assertEquals(history.directApplyRest5StartTime, copyHistory.directApplyRest5StartTime);
		System.assertEquals(history.directApplyRest5EndTime, copyHistory.directApplyRest5EndTime);
	}

	/**
	 * isAvailablePatternのテスト
	 * 勤務パターンが利用可能かどうかのチェックを正しく行えることを確認する
	 */
	@isTest static void isAvailablePatternTest() {
		AttWorkingTypeHistoryEntity entity = new AttWorkingTypeHistoryEntity();
		entity.patternCodeList = new List<String>{'Code1', 'Code2'};

		// Test1: 利用可能な勤務パターンを指定する
		System.assertEquals(true, entity.isAvailablePattern('Code2'));

		// Test2: 利用不可の勤務パターンを指定する
		System.assertEquals(false, entity.isAvailablePattern('Code'));

		// Test3: 利用不可の勤務パターンを指定する(大文字小文字を区別している)
		System.assertEquals(false, entity.isAvailablePattern('code1'));
	}
}