/**
 * @group 工数
 *
 * @description
 * 工数管理の業務バリデーションとトランザクションエンティティのバリデーションを定義するクラス
 */
public with sharing class TimeValidator {
	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();
	/**
	 * 工数確定申請エンティティのバリデーション
	 */
	public class TimeRequestEntityValidator extends Validator.BaseValidator {
		/** 工数確定申請 */
		private TimeRequestEntity entity;
		/**
		 * コンストラクタ
		 * @param entity 検証対象の工数確定申請
		 */
		public TimeRequestEntityValidator(TimeRequestEntity entity) {
			this.entity = entity;
		}

		/**
		 * バリデーションを行う
		 */
		public override Validator.Result validate() {
			validateName();
			return this.result;
		}

		/**
		 * 工数確定申請のバリデーションを実行する
		 */
		private void validateName() {
			// 工数確定申請名が設定されている
			if (String.isBlank(this.entity.name)) {
				result.addError(
						App.ERR_CODE_NOT_SET_VALUE,
						MESSAGE.Com_Err_NotSetValueAofB(new List<String>{MESSAGE.Time_Lbl_TimeTrackRequest, MESSAGE.Admin_Lbl_Name}));
			}
		}
	}

	/**
	 * 工数サマリーエンティティのバリデーション
	 */
	public class TimeSummaryEntityValidator extends Validator.BaseValidator {
		/** 工数サマリー */
		private TimeSummaryEntity entity;
		/**
		 * コンストラクタ
		 * @param entity 検証対象の工数サマリー
		 */
		public TimeSummaryEntityValidator(TimeSummaryEntity entity) {
			this.entity = entity;
		}

		/**
		 * バリデーションを行う
		 */
		public override Validator.Result validate() {
			validateName();
			for (TimeRecordEntity record : entity.recordList) {
				TimeValidator.TimeRecordEntityValidator recordValidator =
						new TimeValidator.TimeRecordEntityValidator(record);
				result.addErrors(recordValidator.validate().getErrors());
			}
			return this.result;
		}

		/**
		 * 工数サマリー名のバリデーションを実行する
		 */
		private void validateName() {
			// 工数サマリー名が設定されている
			if (String.isBlank(this.entity.name)) {
				result.addError(
						App.ERR_CODE_NOT_SET_VALUE,
						MESSAGE.Com_Err_NotSetValueAofB(new List<String>{MESSAGE.Trac_Lbl_TimeSummary, MESSAGE.Admin_Lbl_Name}));
			}
		}
	}

	/**
	 * 工数明細エンティティのバリデーション
	 * - 各工数明細内訳に対してもバリデーションを行う
	 */
	public class TimeRecordEntityValidator extends Validator.BaseValidator {
		/** 工数明細 */
		private TimeRecordEntity entity;
		/**
		 * コンストラクタ
		 * @param entity 検証対象の工数明細
		 */
		public TimeRecordEntityValidator(TimeRecordEntity entity) {
			this.entity = entity;
		}

		/**
		 * バリデーションを行う
		 */
		public override Validator.Result validate() {
			validateName();
			validateNote();
			if (entity.recordItemList == null) {
				return this.result;
			}
			for (TimeRecordItemEntity recordItem : entity.recordItemList) {
				TimeValidator.TimeRecordItemEntityValidator recordItemValidator =
						new TimeValidator.TimeRecordItemEntityValidator(recordItem);
				result.addErrors(recordItemValidator.validate().getErrors());
			}
			return this.result;
		}

		/**
		 * 工数明細名のバリデーションを実行する
		 */
		private void validateName() {
			// 工数明細名が設定されている
			if (String.isBlank(this.entity.name)) {
				result.addError(
						App.ERR_CODE_NOT_SET_VALUE,
						MESSAGE.Com_Err_NotSetValueAofB(new List<String>{MESSAGE.Time_Lbl_TimeTrackRecord, MESSAGE.Admin_Lbl_Name}));
			}
		}

		/**
		 * 作業報告のバリデーションを実行する
		 */
		private void validateNote() {
			if (String.isNotBlank(this.entity.note)) {
				// 桁数チェック
				if (isInvalidMaxLengthOver(this.entity.note, TimeRecordEntity.NOTE_MAX_LENGTH)) {
					result.addError(
							App.ERR_CODE_NOT_SET_VALUE,
							MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Cal_Lbl_WorkReport, String.valueOf(TimeRecordEntity.NOTE_MAX_LENGTH)}));
				}
			}
		}
	}

	/**
	 * 工数明細内訳エンティティのバリデーション
	 */
	public class TimeRecordItemEntityValidator extends Validator.BaseValidator {
		/** 工数明細内訳 */
		private TimeRecordItemEntity entity;
		/**
		 * コンストラクタ
		 * @param entity 検証対象の工数明細内訳
		 */
		public TimeRecordItemEntityValidator(TimeRecordItemEntity entity) {
			this.entity = entity;
		}

		/**
		 * バリデーションを行う
		 */
		public override Validator.Result validate() {
			validateName();
			return this.result;
		}

		/**
		 * 工数明細内訳のバリデーションを実行する
		 */
		private void validateName() {
			// 工数明細内訳名が設定されている
			if (String.isBlank(this.entity.name)) {
				result.addError(
						App.ERR_CODE_NOT_SET_VALUE,
						MESSAGE.Com_Err_NotSetValueAofB(new List<String>{MESSAGE.Time_Lbl_TimeTrackRecordItem, MESSAGE.Admin_Lbl_Name}));
			}
		}
	}
}
