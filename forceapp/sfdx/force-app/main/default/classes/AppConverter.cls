 /**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * 値オブジェクトをプリミティブデータ型に変換する機能を提供するクラス
  */
public with sharing class AppConverter {

	/**
	 * AttTimeをInteger型に変換する
	 * @param attTimeToInt Integer型に変換する値
	 */
	public static Integer intValue(AttTime attTimeToInt) {
		return AttTime.convertInt(attTimeToInt);
	}

	/**
	 * AttTimeをInteger型に変換する(nullの場合はデフォルト値)
	 * @param attTimeToInt Integer型に変換する値
	 * @param defaultValue デフォルト値
	 */
	public static Integer intValue(AttTime attTimeToInt, Integer defaultValue) {
		Integer retValue = AttTime.convertInt(attTimeToInt);
		return retValue == null ? defaultValue : retValue;
	}

	/**
	 * AttDailyTimeをInteger型に変換する
	 * @param AttDailyTimeToInt Integer型に変換する値
	 */
	public static Integer intValue(AttDailyTime attDailyTimeToInt) {
		return AttDailyTime.convertInt(attDailyTimeToInt);
	}

	/**
	 * AttDailyTimeをInteger型に変換する(nullの場合はデフォルト値)
	 * @param AttDailyTimeToInt Integer型に変換する値
	 * @param defaultValue デフォルト値
	 */
	public static Integer intValue(AttDailyTime attDailyTimeToInt, Integer defaultValue) {
		Integer retValue =  AttDailyTime.convertInt(attDailyTimeToInt);
		return retValue == null ? defaultValue : retValue;
	}

	/**
	 * AttDaysをInteger型に変換する
	 * @param AttDaysToInt Integer型に変換する値
	 */
	public static Integer intValue(AttDays attDaysToInt) {
		return AttDays.convertInt(attDaysToInt);
	}

	/**
	 * 文字列をInteger型に変換する。
	 * @param stringValue Integer型に変換する文字列
	 * @return 変換後の整数値、ただしstringValueがnull,空文字の場合はnullを返却
	 */
	public static Integer intValue(String stringValue) {
		if (String.isBlank(stringValue)) {
			return null;
		}
		return Integer.valueOf(stringValue.trim());
	}

	public static Integer intValue(AttDays attDaysToInt, Integer defaultValue) {
		Integer retValue =  AttDays.convertInt(attDaysToInt);
		return retValue == null ? defaultValue : retValue;
	}
	public static Decimal decValue(AttDays attDaysToDec) {
		return AttDays.convertDec(attDaysToDec);
	}
	public static Decimal decValue(AttDays attDaysToDec, Decimal defaultValue) {
		Decimal retValue =  AttDays.convertDec(attDaysToDec);
		return retValue == null ? defaultValue : retValue;
	}
	/**
	 * AppDateをDate型に変換する
	 * @param appDateToDate Date型に変換する値
	 */
	public static Date dateValue(AppDate appDateToDate) {
		return AppDate.convertDate(appDateToDate);
	}
	/**
	 * AppDatepimeをDatetime型に変換する
	 * @param appDateTimeToDateTime DateTime型に変換する値
	 */
	public static Datetime datetimeValue(AppDatetime appDateTimeToDateTime) {
		return AppDatetime.convertDatetime(appDateTimeToDateTime);
	}
	/**
	 * ValueObjectTypeをString型に変換する(ValueObjectType.valueを返却)
	 * @param typeToString 変換する値
	 */
	public static String stringValue(ValueObjectType typeToString) {
		return ValueObjectType.getValue(typeToString);
	}
	/**
	 * List<ValueObjectType>をセミコロンで区切りするtring型に変換する
	 * @param typeToString 変換する値
	 */
	public static String stringValue(List<ValueObjectType> typesToString) {
		if (typesToString == null) {
			return null;
		}
		List<String> valueList = new List<String>();
		for (ValueObjectType valueObject: typesToString) {
			String valueString = stringValue(valueObject);
			if (!String.isBlank(valueString)) {
				valueList.add(valueString);
			}
		}
		return String.join(valueList, ';');
	}
	/**
	 * AppMultiStringをString型に変換する(実行ユーザの言語の文字列を返却)
	 * @param typeToString 変換する値
	 */
	public static String stringValue(AppMultiString multiToString) {
		return AppMultiString.stringValue(multiToString);
	}
}
