public with sharing class DateUtil {
	/** 曜日の判定で使用する既知の日曜日 */
	private static final Date BASE_SUNDAY = Date.newInstance(1900, 1, 7);
	
	/**
	 * Apex DateTime型を ISO8601 形式の文字列に変換
	 */
	public static String dateTimeToISO8601(DateTime sfDateTime) {
		if (sfDateTime == null) {
			return null;
		}
		return sfDateTime.formatGmt('YYYY-MM-dd\'T\'HH:mm:ss') + 'Z';
	}

	/**
	 * Apex Date型を ISO8601 形式の文字列に変換
	 */
	public static String dateToISO8601(Date sfDate) {
		if (sfDate == null) {
			return null;
		}
		Datetime dt = DateTime.newInstance(sfDate.year(), sfDate.month(), sfDate.day());
		return dt.formatGmt('YYYY-MM-dd\'T\'HH:mm:ss') + 'Z';
	}

	/**
	 * ISO8601 形式の文字列を Apex Date型に変換
	 */
	public static Date ISO8601ToDate(String dateTimeString) {
		JSONParser parser = JSON.createParser('{"t":"' + dateTimeString + '"}');

		parser.nextToken();
		parser.nextValue();

		return DateTime.newInstance(parser.getDateTimeValue().getTime()).date();
	}

	/**
	 * YYYY-MM-DD 形式の文字列を日付型に変換
	 */
	public static Date stringToDate(String dateString) {
		if (String.isBlank(dateString)) {
			return null;
		}
		return Date.valueOf(dateString);
	}

	/**
	 * Apex DateTime 型をミリ秒に変換
	 */
	public static Long convertToMsec(DateTime sfDateTime) {
		if (sfDateTime == null) {
			return null;
		}
		return sfDateTime.getTime();
	}

	/**
	 * Apex Date 型をミリ秒に変換
	 */
	public static Long convertToMsec(Date sfDate) {
		if (sfDate == null) {
			return null;
		}
		Datetime dt = DateTime.newInstance(sfDate.year(), sfDate.month(), sfDate.day());
		return dt.getTime();
	}

	/**
	 * ミリ秒を Apex DateTime 型に変換
	 */
	public static DateTime convertMsecToDateTime(Long msec) {
		if (msec == null) {
			return null;
		}
		return DateTime.newInstance(msec);
	}

	/**
	 * ミリ秒を Apex Date 型に変換
	 */
	public static Date convertMsecToDate(Long msec) {
		if (msec == null) {
			return null;
		}
		return DateTime.newInstance(msec).date();
	}
	
	/**
	 *日付の曜日を取得する
	 */
	public static Integer dayOfWeek(Date dt) {
		return Math.mod(BASE_SUNDAY.daysBetween(dt), 7);
	}
	/**
	 *日付の日を取得する
	 */
	public static Integer dayOfMonth(Date dt) {
		return dt.day();
	}
	/**
	 *日付の月日数を取得する
	 */
	public static Integer daysOfMonth(Date dt) {
		return Date.daysInMonth(dt.year(), dt.month());
	}
}