/**
  * @group 共通
  *
  * 位置情報リポジトリ
  */
public with sharing class ComLocationRepository extends Repository.BaseRepository {
	/** 取得対象の項目(位置情報) */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ComLocation__c.Id,
			ComLocation__c.Name,
			ComLocation__c.Type__c,
			ComLocation__c.EmployeeBaseId__c,
			ComLocation__c.AttRecordId__c,
			ComLocation__c.Type__c,
			ComLocation__c.Source__c,
			ComLocation__c.DateTime__c,
			ComLocation__c.Location__Longitude__s,
			ComLocation__c.Location__Latitude__s,
			ComLocation__c.AttStampType__c,
			ComLocation__c.Comment__c
		};
	}
	/** FOR UPDATE でクエリしない */
	private static final Boolean NOT_FOR_UPDATE = false;
		/** 検索フィルタ */
	private class SearchFilter {
		/** Id */
		public List<Id> ids;
	}
	/**
	 * 指定した位置情報を取得する
	 * @param 取得対象の位置情報ID
	 * @return 申請、ただし見つからなかった場合はnull
	 */
	public ComLocationEntity getEntity(Id id) {
		List<ComLocationEntity> entities = getEntityList(new List<Id>{id});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定した位置情報を取得する
	 * @param 取得対象の位置情報リスト
	 * @return 位置情報リスト
	 */
	public List<ComLocationEntity> getEntityList(List<Id> ids) {
		SearchFilter filter = new SearchFilter();
		filter.ids = ids;

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 位置情報を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した申請一覧(申請日付順)
	 */
	private List<ComLocationEntity> searchEntityList(
			SearchFilter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> ids = filter.ids;
		if (ids != null) {
			condList.add(getFieldName(ComLocation__c.Id) + ' IN :ids');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComLocation__c.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond
				+ ' ORDER BY ' + getFieldName(ComLocation__c.DateTime__c) + ' Asc';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// クエリ実行
		System.debug('ComLocationRepository.searchEntityList: SOQL ==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<ComLocationEntity> entityList = new List<ComLocationEntity>();
		for (SObject sObj : sObjList) {
			entityList.add(new ComLocationEntity((ComLocation__c)sObj));
		}

		return entityList;
	}
	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(ComLocationEntity entity) {
		return saveEntityList(new List<ComLocationEntity>{entity});
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<ComLocationEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<ComLocationEntity> entityList, Boolean allOrNone) {
		List<ComLocation__c> events = new List<ComLocation__c>();
		for (ComLocationEntity entity : entityList) {
			events.add(entity.createSObject());
		}
		// 保存
		List<Database.UpsertResult> result = AppDatabase.doUpsert(events, allOrNone);
		return resultFactory.createSaveResult(result);
	}
}
