/**
 * 共通処理用定数
 * TODO 本クラスは将来的に廃止する予定です。
 */
public with sharing class ComConst {
	// TODO 未使用項目の整理。とりあえず未使用確認済みの項目をコメントアウト yamabi
//	// 承認申請ステータス
//	public final static String REQUEST_STATUS_PENDING = 'Pending';
//	public final static String REQUEST_STATUS_APPROVING = 'Approval In';
//	public final static String REQUEST_STATUS_APPROVED = 'Approved';
//	public final static String REQUEST_STATUS_REJECTED = 'Rejected';
//	public final static String REQUEST_STATUS_CANCELED = 'Removed';
//	public final static String REQUEST_STATUS_PROCESSED = 'Processed';

//	// 承認フローステータス(ProcessInstanceHistoryのStepStatus項目値)
//	public final static String STEP_STATUS_APPROVED = 'Approved';
//	public final static String STEP_STATUS_REJECTED = 'Rejected';

	// 言語略称(暫定：userのLanguageLocaleKey項目)
	public final static String LANG_EN = 'en_US';
	public final static String LANG_JA = 'ja';
//	public final static String LANG_ZH = 'zh_CN';

	// 言語項目末尾タイプ
	public final static String LANG_TYPE_BASE = 'L0'; // 基準言語
	public final static String LANG_TYPE_EXT1 = 'L1'; // 拡張言語1
	public final static String LANG_TYPE_EXT2 = 'L2'; // 拡張言語2

//	// 申請種別
//	public final static String REQUEST_TYPE_EXP = 'exp'; // 経費申請
//	public final static String REQUEST_TYPE_TEP = 'tep'; // 事前申請

//	// 承認アクション
//	public final static String REQUEST_ACTION_APPROVE = 'Approve'; // 承認
//	public final static String REQUEST_ACTION_REJECT = 'Reject'; // 却下

	// ページングサイズ
	public final static Integer PAGING_PAGE_SIZE = 200;

	// 一覧最大取得数（StandardSetControllerの上限数）
	public final static Integer VIEWRECORD_LIMIT_COUNT = 10000;

	/*
	// 承認プロセスのステップ割当先
	public final static String STEP_ASSIGNTO_MANAGER = 'manager';
	public final static String STEP_ASSIGNTO_SF_MANAGER = 'SFManager';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER1 = 'deptApprover1';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER2 = 'deptApprover2';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER3 = 'deptApprover3';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER4 = 'deptApprover4';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER5 = 'deptApprover5';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER6 = 'deptApprover6';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER7 = 'deptApprover7';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER8 = 'deptApprover8';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER9 = 'deptApprover9';
	public final static String STEP_ASSIGNTO_DEPT_APPROVER10 = 'deptApprover10';
	public final static String STEP_ASSIGNTO_EMP_APPROVER1 = 'empApprover1';
	public final static String STEP_ASSIGNTO_EMP_APPROVER2 = 'empApprover2';
	public final static String STEP_ASSIGNTO_EMP_APPROVER3 = 'empApprover3';
	public final static String STEP_ASSIGNTO_EMP_APPROVER4 = 'empApprover4';
	public final static String STEP_ASSIGNTO_EMP_APPROVER5 = 'empApprover5';
	public final static String STEP_ASSIGNTO_EMP_APPROVER6 = 'empApprover6';
	public final static String STEP_ASSIGNTO_EMP_APPROVER7 = 'empApprover7';
	public final static String STEP_ASSIGNTO_EMP_APPROVER8 = 'empApprover8';
	public final static String STEP_ASSIGNTO_EMP_APPROVER9 = 'empApprover9';
	public final static String STEP_ASSIGNTO_EMP_APPROVER10 = 'empApprover10';
	public final static String STEP_ASSIGNTO_JOB_LEADER = 'jobLeader';
	public final static String STEP_ASSIGNTO_COST_CENTER_MANAGER = 'costCenterManager';
	public final static String STEP_ASSIGNTO_GROUP = 'group_';
*/
//	// 承認プロセスの変更可能ユーザ
//	public final static String STEP_DELEGATETO_DISABLED = 'disabled';
//	public final static String STEP_DELEGATETO_MANAGER = 'manager';
//	public final static String STEP_DELEGATETO_SF_MANAGER = 'SFManager';
//	public final static String STEP_DELEGATETO_UPPER_ROLE = 'upperRoleUser';
//	public final static String STEP_DELEGATETO_DEPT_MANAGER = 'deptManager';
//	public final static String STEP_DELEGATETO_DEPT_ASSISTANT_MANAGER = 'deptAssistantManager';
//	public final static String STEP_DELEGATETO_DEPT_APPROVER = 'deptApprover';
//	public final static String STEP_DELEGATETO_EMP_APPROVER = 'empApprover';

//	// 暗号形式
//	public final static Integer CRYPTO_AESKEY_SIZE = 256;
//	public final static String CRYPTO_ALGORITHMNAME = 'AES256';

//	// 端数処理形式
//	public final static String ROUNDING_CEIL = 'Ceil';
//	public final static String ROUNDING_FLOOR = 'Floor';
//	public final static String ROUNDING_ROUND = 'Round';

//	// テキスト改行記号
//	public final static String LINE_BREAK = '\r\n';
//	// csv区切り文字
//	public final static String CSV_SEPARATOR = ',';

//	// ファイル文字コード
//	public final static String CHARSET_SHIFTJIS = 'Shift_JIS';
}
