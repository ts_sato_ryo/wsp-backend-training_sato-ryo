/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for CostCenterCurrentHistoryUpdateBatch
 */
@isTest
private class CostCenterCurrentHistoryUpdateBatchTest {

	/**
	 * Apexスケジュール正常系テスト Positive test for Executing batch
	 */
	@isTest
	static void schedulePositiveTest() {
		Test.StartTest();

		CostCenterCurrentHistoryUpdateBatch sch = new CostCenterCurrentHistoryUpdateBatch();
		Id schId = System.schedule('Cost Center History Switching Schedule', '0 0 0 * * ?', sch);

		Test.StopTest();

		System.assertNotEquals(null, schId);
	}

	/**
	 * バッチ正常系テスト Positive test for batch process
	 */
	@isTest
	static void batchPositiveTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<ComCostCenterBase__c> baseObjs = ComTestDataUtility.createCostCentersWithHistory('Test', companyObj.Id, baseNum, historyNum, true, 0);

		ComCostCenterBase__c base1 = baseObjs[0];
		List<ComCostCenterHistory__c> historyList1 =
				[SELECT Id, BaseId__c, Name_L0__c FROM ComCostCenterHistory__c WHERE BaseId__c = :base1.Id ORDER BY ValidFrom__c Asc];

		// CurrentHistoryId__c を空欄にしておく Set null to CurrentHistoryId__c
		base1.Name = 'AAA';
		base1.CurrentHistoryId__c = null;
		update base1;

		Test.StartTest();

		CostCenterCurrentHistoryUpdateBatch b = new CostCenterCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);

		Test.StopTest();

		System.assertNotEquals(null, jobId);

		ComCostCenterBase__c resBase = [SELECT Id, Name, CurrentHistoryId__c FROM ComCostCenterBase__c WHERE Id = :base1.Id];
		System.assertEquals(historyList1[0].Id, resBase.CurrentHistoryId__c);
		System.assertEquals(historyList1[0].Name_L0__c, resBase.Name);
	}
}