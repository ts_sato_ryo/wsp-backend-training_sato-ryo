/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署サービステスト
 */
@isTest
private class DepartmentServiceTest {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj {get; private set;}
		/** 権限オブジェクト */
		public ComPermission__c permissionObj;
		/** 親部署 */
		ComDeptBase__c parentDeptObj;
		/** 上長 */
		ComEmpBase__c managerObj;

		/** コンストラクタ */
		public TestData() {
			this.orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			this.countryObj = ComTestDataUtility.createCountry('Japan');
			this.companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
			this.permissionObj = ComTestDataUtility.createPermission('Test Permission', this.companyObj.Id);
			this.parentDeptObj = ComTestDataUtility.createDepartmentWithHistory('親部署', this.companyObj.Id);
			this.managerObj = ComTestDataUtility.createEmployeeWithHistory('Test社員', this.companyObj.Id, null, Userinfo.getUserId(), this.permissionObj.Id);
		}

		/**
		 * テスト用の部署エンティティを作成する
		 */
		public DepartmentBaseEntity createDeptEntity(String name, String code) {

			DepartmentBaseEntity baseEntity = new DepartmentBaseEntity();
			baseEntity.name = name;
			baseEntity.currentHistoryId = parentDeptObj.CurrentHistoryId__c;	// とりあえず適当に設定しておく
			baseEntity.code = code;
			baseEntity.companyId = companyObj.id;
			baseEntity.addHistory(createDeptHistoryEntity(name + '1', code, AppDate.newInstance(2017, 12, 15), AppDate.newInstance(2018, 4, 1)));
			baseEntity.addHistory(createDeptHistoryEntity(name + '2', code, AppDate.newInstance(2018, 4, 1), AppDate.newInstance(2019, 1, 1)));

			return baseEntity;
		}

		/**
		 * テスト用の部署エンティティを作成する
		 */
		public DepartmentHistoryEntity createDeptHistoryEntity(String name, String code, AppDate validFrom, AppDate validTo) {
			DepartmentHistoryEntity historyEntity = new DepartmentHistoryEntity();
			historyEntity.name = name;
			historyEntity.nameL0 = name + '_L0';
			historyEntity.nameL1 = name + '_L1';
			historyEntity.nameL2 = name + '_L2';
			historyEntity.historyComment = '改訂コメント';
			historyEntity.validFrom = validFrom;
			historyEntity.validTo = validTo;
			historyEntity.isRemoved = false;
			historyEntity.managerId = managerObj.Id;
			historyEntity.parentBaseId = parentDeptObj.Id;
			historyEntity.uniqKey = historyEntity.createUniqKey(code);

			return historyEntity;
		}

		/**
		 * ベースエンティティを作成し、登録する
		 */
		public List<DepartmentBaseEntity> createDeptBaseEntityList(String name, Integer baseSize, Integer historySize) {
			List<ComDeptBase__c> deptList = createDepartmentsWithHistory(name, baseSize, historySize);
			List<Id> baseIdList = new List<Id>();
			for (ComDeptBase__c dept : deptList) {
				baseIdList.add(dept.Id);
			}

			return (new DepartmentRepository()).getEntityList(baseIdList, null);
		}

		/**
		 * 部署のベースと履歴のオブジェクトを作成する
		 * @pame 部署名
		 * @param baseSize 作成するベースの数
		 * @param historySize 各ベースに対して作成する履歴オブジェクトの数
		 */
		public List<ComDeptBase__c> createDepartmentsWithHistory(String name, Integer baseSize, Integer historySize) {
			List<ComDeptBase__c> deptList =  ComTestDataUtility.createDepartmentsWithHistory(
					name, this.companyObj.Id, baseSize, historySize);

			// 履歴に親部署を設定
			List<ComDeptHistory__c> historyList = [SELECT Id, ParentBaseId__c FROM ComDeptHistory__c];
			for (ComDeptHistory__c history : historyList) {
				history.ParentBaseId__c = this.parentDeptObj.Id;
			}
			update historyList;
			return deptList;
		}

	/**
	 * テスト用の部署エンティティを作成する
	 */
	public DepartmentBaseEntity createDeptEntityWithHistory(String name, String code) {
		DepartmentBaseEntity baseEntity = new DepartmentBaseEntity();
		baseEntity.name = name;
		baseEntity.currentHistoryId = parentDeptObj.CurrentHistoryId__c;	// とりあえず適当に設定しておく
		baseEntity.code = code;
		baseEntity.uniqKey = baseEntity.createUniqKey(companyObj.Code__c, code);
		baseEntity.companyId = companyObj.Id;
		baseEntity.addHistory(createDeptHistoryEntity(name + '1', code, AppDate.newInstance(2017, 12, 15), AppDate.newInstance(2018, 4, 1)));
		baseEntity.addHistory(createDeptHistoryEntity(name + '2', code, AppDate.newInstance(2018, 4, 1), AppDate.newInstance(2019, 1, 1)));

		return baseEntity;
	}
	}

	/**
	 * 子部署が存在するかどうかを確認することができる
	 */
	@isTest static void hasChildrenTest() {
		DepartmentRepository repo = new DepartmentRepository();
		DepartmentServiceTest.TestData testData = new TestData();

		DepartmentBaseEntity base = testData.createDeptBaseEntityList('Test', 1, 1).get(0);

		DepartmentService service = new DepartmentService();

		// 子部署が存在する場合
		System.assertEquals(true, service.hasChildren(testData.parentDeptObj.Id, AppDate.today()));

		// 子部署は存在するが、指定した日には無効な部署の場合
		// 対象日が考慮されていることを確認する
		System.assertEquals(false, service.hasChildren(testData.parentDeptObj.Id, AppDate.newInstance(1900, 1, 1)));

		// 子部署が存在しない場合
		System.assertEquals(false, service.hasChildren(base.id, AppDate.today()));
	}

	/**
	 * 指定した日の履歴が取得できることを確認する
	 */
	@isTest static void getHistoryMapTest() {
		DepartmentRepository repo = new DepartmentRepository();
		DepartmentServiceTest.TestData testData = new TestData();

		DepartmentBaseEntity base = testData.createDeptBaseEntityList('Test', 1, 1).get(0);
		Id baseId = base.id;
		List<ComDeptHistory__c> histories = [SELECT Id, ParentBaseId__c, ValidFrom__c FROM ComDeptHistory__c WHERE BaseId__c = :baseId];
		ComDeptHistory__c targetHistoryObj = histories[0];
		Id parentBaseId = targetHistoryObj.ParentBaseId__c;
		AppDate targetDate = AppDate.valueOf(targetHistoryObj.ValidFrom__c);

		DepartmentService service = new DepartmentService();
		Map<Id, DepartmentHistoryEntity> resHistoryMap = service.getHistoryMap(new List<Id>{parentBaseId}, targetDate);

		System.assertEquals(1, resHistoryMap.size());
		DepartmentHistoryEntity parentHistory = resHistoryMap.get(parentBaseId);
		System.assertNotEquals(null, parentHistory);
		System.assertEquals(parentBaseId, parentHistory.baseId);
		System.assert(parentHistory.validFrom.getDate() <= targetDate.getDate()
				&& parentHistory.validTo.getDate() > targetDate.getDate(),
				'対象日が有効期間に含まれていません');
	}

	/**
	 * 履歴が改定できることを確認する
	 * 改定内容が伝搬されることを確認する
	 */
	@isTest static void reviseHistoryTest() {
		DepartmentRepository repo = new DepartmentRepository();
		DepartmentServiceTest.TestData testData = new TestData();

		final Integer historyNum = 5;
		DepartmentBaseEntity base = testData.createDeptBaseEntityList('Test', 1, historyNum).get(0);

		// 部署の管理者を変更する
		ComEmpBase__c managerObj1 = ComTestDataUtility.createEmployeeWithHistory('管理者1', testData.companyObj.Id, null, Userinfo.getUserId(), testData.permissionObj.Id);
		ComEmpBase__c managerObj2 = ComTestDataUtility.createEmployeeWithHistory('管理者2', testData.companyObj.Id, null, Userinfo.getUserId(), testData.permissionObj.Id);

		AppDate validFrom = base.getHistoryList().get(1).validFrom.addDays(30);
		AppDate validTo = validFrom.addDays(10);
		DepartmentHistoryEntity targetHistory = testData.createDeptHistoryEntity('改定テスト', base.code, validFrom, validTo);
		targetHistory.baseId = base.id;
		targetHistory.remarks = '改定後の備考'; // 備考は各履歴で値が異なるので伝搬されないはず
		targetHistory.historyComment = '改定履歴コメント'; // 履歴管理項目は伝搬されないはず
		targetHistory.managerId = managerObj1.Id;

		// 末尾の履歴の管理者を変更しておく
		base.getHistoryList().get(historyNum - 1).managerId = managerObj2.Id;
		repo.saveHistoryEntity(base.getHistoryList().get(historyNum - 1));

		// 改定実行
		DepartmentService service = new DepartmentService();
		service.reviseHistory(targetHistory);


		List<DepartmentHistoryEntity> resHistoryList = repo.getHistoryEntityByBaseId(base.id);
		System.assertEquals(historyNum + 1, resHistoryList.size());
		// 改定されていることを確認
		System.assertEquals(validFrom, resHistoryList[1].validTo); // 改定前履歴
		System.assertEquals(validFrom, resHistoryList[2].validFrom); // 改定後履歴

		// // デバッグ
		// System.debug('>>> 改定後の履歴');
		// for (DepartmentHistoryEntity history : resHistoryList) {
		// 	System.debug(history.name + '(' + history.validFrom + '-' + history.validTo + '): managerId = ' + history.managerId + ', remarks =' + history.remarks);
		// }

		// 伝搬されていることを確認
		// 末尾の履歴の管理者はすでに変更されているので伝搬対象外
		for (Integer i = 3; i < historyNum; i++) {
			System.assertEquals(targetHistory.nameL0, resHistoryList[i].nameL0);
			System.assertEquals(targetHistory.nameL1, resHistoryList[i].nameL1);
			System.assertEquals(targetHistory.nameL2, resHistoryList[i].nameL2);
			System.assertEquals(targetHistory.managerId, resHistoryList[i].managerId);

			// 備考は各履歴で値が異なるので伝搬されないはず
			System.assertEquals(base.getHistoryList().get(i - 1).remarks, resHistoryList[i].remarks);

			// 履歴管理項目は伝搬されない
			System.assertEquals(base.getHistoryList().get(i - 1).historyComment, resHistoryList[i].historyComment);

		}

		// 改定前の履歴には伝搬されていない
		for (Integer i = 0; i <= 1; i++) {
			System.assertEquals(base.getHistoryList().get(i).managerId, resHistoryList[i].managerId);
		}

		// 末尾の履歴の管理者はすでに変更されているので伝搬されない
		Integer tailIdx = resHistoryList.size() - 1;
		System.assertEquals(base.getHistoryList().get(base.getHistoryList().size() - 1).managerId,
				resHistoryList[tailIdx].managerId);
		// 名前は伝搬されている
		System.assertEquals(targetHistory.nameL0, resHistoryList[tailIdx].nameL0);
		System.assertEquals(targetHistory.nameL1, resHistoryList[tailIdx].nameL1);
		System.assertEquals(targetHistory.nameL2, resHistoryList[tailIdx].nameL2);

		// 伝搬対象の履歴の元の履歴は論理削除されている
		List<ComDeptHistory__c> removedList = [SELECT Id, ValidFrom__c FROM ComDeptHistory__c WHERE BaseId__c = :base.Id AND Removed__c = true];
		// for (ComDeptHistory__c history : removedList) {
		// 	System.debug('Removed = ' + history.ValidFrom__c);
		// }
		System.assertEquals(historyNum - 2, removedList.size());

	}

	/**
	 * 部署ベースのコードの重複チェックが正しくできることを確認する
	 */
	@isTest static void isCodeDuplicatedTest() {
		DepartmentRepository repo = new DepartmentRepository();
		DepartmentServiceTest.TestData testData = new TestData();

		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<DepartmentBaseEntity> bases = testData.createDeptBaseEntityList('Test', baseNum, historyNum);
		DepartmentBaseEntity targetBase = bases[1];

		DepartmentService service = new DepartmentService();

		// 会社内で重複している → true
		targetBase.code = bases[0].code;
		System.assertEquals(true, service.isCodeDuplicated(targetBase));

		// 会社をまたいで重複している → false
		ComCompany__c ompanyObj2 = ComTestDataUtility.createCompany('Test2', testData.countryObj.Id);
		targetBase.companyId = ompanyObj2.Id;
		targetBase.code = bases[0].code;
		System.assertEquals(false, service.isCodeDuplicated(targetBase));

		// 組織全体でも重複していない → false
		targetBase.code = targetBase.code + 'Test';
		System.assertEquals(false, service.isCodeDuplicated(targetBase));

	}
	/**
	 * 改定の回帰チェック
	 */
	@isTest static void reviseHistoryTestValidateCircular() {
		DepartmentService service = new DepartmentService();
		DepartmentRepository rep = new DepartmentRepository();
		TestData testData = new TestData();
		DepartmentBaseEntity targetBase1 = testData.createDeptEntityWithHistory('Test1', 'Test001');
		targetBase1.setId(service.saveNewEntity(targetBase1));
		DepartmentBaseEntity targetBase2 = testData.createDeptEntityWithHistory('Test2', 'Test002');
		targetBase2.getHistoryList()[0].parentBaseId = targetBase1.id;
		targetBase2.getHistoryList()[1].parentBaseId = targetBase1.id;
		targetBase2.setId(service.saveNewEntity(targetBase2));
		DepartmentBaseEntity targetBase3 = testData.createDeptEntityWithHistory('Test3', 'Test003');
		targetBase3.getHistoryList()[0].parentBaseId = targetBase2.id;
		targetBase3.getHistoryList()[1].parentBaseId = targetBase2.id;
		targetBase3.setId(service.saveNewEntity(targetBase3));

		List<DepartmentHistoryEntity> history1List = rep.getHistoryEntityByBaseId(targetBase1.id);
		// 部署1の履歴の上位部署を部署3に設定
		history1List[0].parentBaseId = targetBase3.id;
		Test.startTest();
		Boolean hasError = false;
		try {
			service.reviseHistory(history1List[0]);
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assert(true);
			System.assertEquals(ComMessage.msg().Com_Err_CircularHierarchy(
				new List<String>{targetBase2.getHistoryList()[0].nameL.getValue(),
					ComMessage.msg().Admin_Lbl_ParentDepartName}), e.getMessage());
		}
		System.assert(hasError);
		Test.stopTest();
	}
	/**
	 * 新規の階層制限チェック
	 */
	@isTest static void saveNewEntityTestValidateLevelLimit() {
		DepartmentService service = new DepartmentService();
		DepartmentRepository rep = new DepartmentRepository();
		TestData testData = new TestData();
		// 10階層部署を作成
		DepartmentBaseEntity targetBase0 = testData.createDeptEntityWithHistory('Test0', 'Test000');
		targetBase0.setId(service.saveNewEntity(targetBase0));
		Id parentId = targetBase0.id;
		for (Integer i = 1; i <= 9; i++) {
			DepartmentBaseEntity targetBase = testData.createDeptEntityWithHistory('Test' + i, 'Test00' + i);
			targetBase.getHistoryList()[0].parentBaseId = parentId;
			parentId = service.saveNewEntity(targetBase);
		}
		Test.startTest();
		Boolean hasError = false;
		try {
			//11階層を追加
			DepartmentBaseEntity targetBase11 = testData.createDeptEntityWithHistory('Test11', 'Test011');
			targetBase11.getHistoryList()[0].parentBaseId = parentId;
			service.saveNewEntity(targetBase11);
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assert(true);
			System.assertEquals(ComMessage.msg().Com_Err_MaxHierarchyOver(new List<String>{ComMessage.msg().Admin_Lbl_Department, '10'}), e.getMessage());
		}
		System.assert(hasError);
		Test.stopTest();
	}
	/**
	 * 履歴削除後の回帰チェック
	 */
	@isTest static void deleteHistoryListWithPropagationTestValidateCircular() {
		DepartmentService service = new DepartmentService();
		DepartmentRepository rep = new DepartmentRepository();
		TestData testData = new TestData();
		DepartmentBaseEntity targetBase01 = testData.createDeptEntityWithHistory('Test01', 'Test01');
		targetBase01.setId(service.saveNewEntity(targetBase01));
		DepartmentBaseEntity targetBase02 = testData.createDeptEntityWithHistory('Test02', 'Test02');
		// 部署2の履歴1の親部署を部署1を参照
		targetBase02.getHistoryList()[0].parentBaseId = targetBase01.id;
		targetBase02.setId(service.saveNewEntity(targetBase02));

		List<DepartmentHistoryEntity> history1List = rep.getHistoryEntityByBaseId(targetBase01.id);
		// 部署1の履歴2の親部署を部署2に改定
		history1List[1].parentBaseId = targetBase02.id;
		service.reviseHistory(history1List[1]);

		Test.startTest();
		Boolean hasError = false;
		try {
			List<DepartmentHistoryEntity> history2List = rep.getHistoryEntityByBaseId(targetBase02.id);
			// 部署2の履歴2を削除(履歴1を伸ばして、部署1と相互参照になってしまう)
			service.deleteHistoryListWithPropagation(new List<Id>{history2List[1].id});
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assert(true);
			System.assertEquals(ComMessage.msg().Com_Err_CircularHierarchy(
					new List<String>{history1List[0].nameL.getValue(), ComMessage.msg().Admin_Lbl_ParentDepartName}), e.getMessage());
		}
		System.assert(hasError);
		Test.stopTest();
	}

	/**
	 * getDepartmentのテスト
	 * リポジトリのキャッシュが有効な場合、対象日が異なる場合もキャッシュが利用されていることを確認する
	 */
	@isTest static void getDepartmentTestEnableRepositoryCache() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 5;
		TestData testData = new TestData();
		List<DepartmentBaseEntity> testDepartmentList =
				testData.createDeptBaseEntityList('test', numberOfBase, numberOfHistory);
		DepartmentBaseEntity testDepartment = testDepartmentList[0];

		Test.startTest();
		DepartmentRepository.enableCache();
		DepartmentService service = new DepartmentService();
		DepartmentBaseEntity res1 = service.getDepartment(testDepartment.id, testDepartment.getHistory(0).validFrom);

		Integer startQueryCount = Limits.getQueries();
		DepartmentBaseEntity res2 = service.getDepartment(testDepartment.id, testDepartment.getHistory(0).validFrom.addMonths(1));
		DepartmentBaseEntity resHistoryAll = service.getDepartment(testDepartment.id, null);
		Integer endQueryCount = Limits.getQueries();
		Test.stopTest();

		System.assertEquals(testDepartment.id, res1.id);
		System.assertEquals(testDepartment.id, res2.id);
		System.assertEquals(testDepartment.id, resHistoryAll.id);
		System.assertEquals(numberOfHistory, resHistoryAll.getHistoryList().size());
		// キャッシュが効いているためクエリは実行されていないはず
		System.assertEquals(0, endQueryCount - startQueryCount);
	}

	/**
	 * getDepartmentのテスト
	 * 部署が見つからない場合、想定されるアプリ例外が発生することを確認する
	 */
	@isTest static void getDepartmentTestNotFound() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 1;
		TestData testData = new TestData();
		List<DepartmentBaseEntity> testDepartmentList =
				testData.createDeptBaseEntityList('test', numberOfBase, numberOfHistory);
		DepartmentBaseEntity testDepartment = testDepartmentList[0];
		App.RecordNotFoundException actEx;

		Test.startTest();
		DepartmentRepository.enableCache();
		DepartmentService service = new DepartmentService();
		try {
			DepartmentBaseEntity res = service.getDepartment(UserInfo.getUserId(), AppDate.today());
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}

		TestUtil.assertNotNull(actEx, '例外が発生しませんでした。');
		String expMsg = ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Department});
		System.assertEquals(expMsg, actEx.getMessage());
	}

	/**
	 * getDepartmentのテスト
	 * 部署が失効している場合、想定される例外が発生することを確認する
	 */
	@isTest static void getDepartmentTestExpire() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 1;
		TestData testData = new TestData();
		List<DepartmentBaseEntity> testDepartmentList =
				testData.createDeptBaseEntityList('test', numberOfBase, numberOfHistory);
		DepartmentBaseEntity testDepartment = testDepartmentList[0];
		App.RecordNotFoundException actEx;

		Test.startTest();
		DepartmentRepository.enableCache();
		DepartmentService service = new DepartmentService();
		try {
			DepartmentBaseEntity res = service.getDepartment(testDepartment.id, testDepartment.getHistory(0).validTo);
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}

		TestUtil.assertNotNull(actEx, '例外が発生しませんでした。');
		String expMsg = ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Department});
		System.assertEquals(expMsg, actEx.getMessage());
	}

	/**
	 * getDepartmentByRangeのテスト
	 * 指定した期間の履歴が取得できることを確認する
	 */
	@isTest static void getDepartmentByRangeTest() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 5;
		TestData testData = new TestData();
		List<DepartmentBaseEntity> testDepartmentList =
				testData.createDeptBaseEntityList('test', numberOfBase, numberOfHistory);
		DepartmentBaseEntity testDepartment = testDepartmentList[0];

		AppDate startDate = testDepartment.getHistory(0).validFrom.addDays(2);
		AppDate endDate = testDepartment.getHistory(1).validTo.addDays(-2);

		Test.startTest();
			DepartmentBaseEntity res =
					new DepartmentService().getDepartmentByRange(testDepartment.id,  new AppDateRange(startDate, endDate));
		Test.stopTest();

		System.assertEquals(testDepartment.id, res.id);
		System.assertEquals(2, res.getHistoryList().size());
	}

	/**
	 * getDepartmentByRangeのテスト
	 * 無効な期間を含む場合、想定される例外が発生することを確認する
	 */
	@isTest static void getDepartmentByRangeTestInvalidPeriod() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 5;
		TestData testData = new TestData();
		List<DepartmentBaseEntity> testDepartmentList =
				testData.createDeptBaseEntityList('test', numberOfBase, numberOfHistory);
		DepartmentBaseEntity testDepartment = testDepartmentList[0];
		App.RecordNotFoundException actEx;
		String expMessage = ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Department});

		// Test1 対象期間の開始日が有効期間外の場合
		actEx = null;
		try {
			AppDate startDate = testDepartment.getHistory(0).validFrom.addDays(-1);
			AppDate endDate = testDepartment.getHistory(1).validTo.addDays(-2);
			DepartmentBaseEntity res =
					new DepartmentService().getDepartmentByRange(testDepartment.id, new AppDateRange(startDate, endDate));
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx);
		System.assertEquals(expMessage, actEx.getMessage());

		// Test2 対象期間の終了日が有効期間外の場合
		actEx = null;
		try {
			AppDate startDate = testDepartment.getHistory(0).validFrom.addDays(-1);
			AppDate endDate = testDepartment.getHistory(1).validTo;
			DepartmentBaseEntity res =
					new DepartmentService().getDepartmentByRange(testDepartment.id, new AppDateRange(startDate, endDate));
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx);
		System.assertEquals(expMessage, actEx.getMessage());
	}
}