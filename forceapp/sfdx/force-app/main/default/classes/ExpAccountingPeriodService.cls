/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Service class for Accounting Period
 */
public class ExpAccountingPeriodService {

	private static final Boolean CHECK_BY_ID = true;
	private static final Boolean CHECK_BY_CODE = false;

	/**
   * Create a new Accounting Period record
   * @param entity Entity to be saved
   * @return Repository.SaveResult
   */
	public static Repository.SaveResult createAccountingPeriod(ExpAccountingPeriodEntity entity) {

		validateEntity(entity);

		setUniqKey(entity);

		// If target record exists, throw error.
		if (isCodeAlreadyInUsed(entity)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		return new ExpAccountingPeriodRepository().saveEntity(entity);
	}

	/**
   * Validate entity
   * @param entity Target entity to be validated
   */
	private static void validateEntity(ExpAccountingPeriodEntity entity) {
		Validator.Result result = new ExpConfigValidator.ExpAccountingPeriodValidator(entity).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * Update a new Accounting Period record
	 * @param entity Entity to be updated
	 * @return
	 */
	public static Repository.SaveResult updateAccountingPeriod(ExpAccountingPeriodEntity newEntity) {

		// Validate entity value
		validateEntity(newEntity);

		// Set unique key to entity
		setUniqKey(newEntity);

		ExpAccountingPeriodEntity existingEntity = retrieveExistingAccountingPeriodEntity(newEntity.id);
		// If target record dose not exist, throw error.
		if (existingEntity == null) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_RecordNotFound);
		}

		// If the new code is not the same as existing code, check if the code is already in used by another Accounting Period.
		if (!existingEntity.code.equals(newEntity.code)) {
			if (isCodeAlreadyInUsed(newEntity)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
			}
		}

		// If the new Recording Date is not the same as existing Recording Date, check if the AccountingPeriod is alreayd in used in Transactions
		if (existingEntity.recordingDate.compareTo(newEntity.recordingDate) != 0) {
			if (existsRelatedTransactionRecords(newEntity.id)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_CannotChangeReference);
			}
		}

		return new ExpAccountingPeriodRepository().saveEntity(newEntity);
	}

	/**
 * Delete a new Accounting Period record
 * @param entity Entity to be deleted
 * @return
 */
	public static void deleteAccountingPeriod(Id id) {
		// If target record has been deleted, do nothing
		if (retrieveExistingAccountingPeriodEntity(id) == null) {
			return;
		}

		ExpAccountingPeriodEntity entity = new ExpAccountingPeriodEntity();
		entity.setId(id);

		new ExpAccountingPeriodRepository().deleteEntity(id);
	}

	/**
 * Search a Accounting Period record by record ID
 * @param id Target record ID
 * @return List of fetched Currency records
 */
	public static List<ExpAccountingPeriodEntity> searchAccountingPeriod(Map<String, Object> paramMap) {
		ExpAccountingPeriodRepository.SearchFilter filter = new ExpAccountingPeriodRepository.SearchFilter();

		if (paramMap != null) {
			Id companyId = (Id)paramMap.get('companyId');
			filter.companyIds = String.isNotBlank(companyId) ? new Set<Id>{companyId} : null;

			if (paramMap.containsKey('active') && (paramMap.get('active') != null)) {
				filter.active = (Boolean) paramMap.get('active') ? filter.ONLY_ACTIVE : filter.ONLY_INACTIVE;
			} else {
				filter.active = filter.ALL;
			}
		}
		return new ExpAccountingPeriodRepository().searchEntity(filter);
	}

	/**
	 * Set unique key to the entity
	 * @param entity Target entity
	 */
	private static void setUniqKey(ExpAccountingPeriodEntity entity) {
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		entity.uniqKey = entity.createUniqKey(company.code);
	}

	/**
	 * Check if Code is already in used
	 * @param entity Target entity
	 */
	private static Boolean isCodeAlreadyInUsed(ExpAccountingPeriodEntity entity) {
		ExpAccountingPeriodRepository.SearchFilter filter = new ExpAccountingPeriodRepository.SearchFilter();

		filter.companyIds = new Set<Id>{ entity.companyId };
		filter.codes = new Set<String>{ entity.code };
		List<ExpAccountingPeriodEntity> result = new ExpAccountingPeriodRepository().searchEntity(filter);
		return (result.size() > 0) && (result[0] != null) && (entity.id != result[0].id);
	}

	/*
	 * Retrieve Accounting Period Entity for the given ID
	 */
	private static ExpAccountingPeriodEntity retrieveExistingAccountingPeriodEntity(Id accountingPeriodId) {
		return new ExpAccountingPeriodRepository().getEntity(accountingPeriodId);
	}

	/*
	 * Check if the provided Accounting Period ID is already in used in any of the Expense Reports
	 */
	private static Boolean existsRelatedTransactionRecords(Id entityId) {
		// Only Expense Report uses the Accounting Period. Check if there is any existing report using it.
		List<ExpReportEntity> expReportList = new ExpReportRepository().getEntityListByAccountingPeriodId(entityId, 1);
		return (!expReportList.isEmpty());
	}
}