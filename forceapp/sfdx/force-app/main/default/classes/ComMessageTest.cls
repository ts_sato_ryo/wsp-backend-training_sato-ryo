@isTest
private class ComMessageTest {

	/**
	 * 正しく言語マップが取得されるかテスト
	 */
	@isTest private static void msgCheckLangMapTest() {
		System.assertEquals(ComMessage.msg(), ComMessage.msgMap.values()[0]);

		System.assertEquals(ComMessage.msg('ja'), ComMessage.msgMap.get('ja'));
		System.assertEquals(ComMessage.msg('en_US'), ComMessage.msgMap.get('en_US'));
		System.assertEquals(ComMessage.msg('test'), ComMessage.msgMap.get('test'));
	}

	/**
	 * 正しく言語マップが変換されるかテスト
	 */
	@isTest private static void msgConvertTest() {

		String noParamValue = 'test';
		String param1Value = noParamValue + '[%1]';
		String param2Value = noParamValue + '[%1][%2]';
		String param3Value = noParamValue + '[%1][%2][%3]';

		String param1 = '1';
		String param2 = '2';
		String param3 = '3';

		Test.startTest();

		String noParamConvertValue = ComMessage.msg().convert(noParamValue);
		try {
			ComMessage.msg().convert(noParamValue, new List<String>{ param1 });
			System.assert(false);
		}
		catch (ComTestException ex) { }

		String param1ConvertValue = ComMessage.msg().convert(param1Value, new List<String>{ param1 });
		try {
			ComMessage.msg().convert(param1Value);
			System.assert(false);
		}
		catch (ComTestException ex) { }
		try {
			ComMessage.msg().convert(param1Value, new List<String>{ param1, param2 });
			System.assert(false);
		}
		catch (ComTestException ex) { }

		String param2ConvertValue = ComMessage.msg().convert(param2Value, new List<String>{ param1, param2 });
		try {
			ComMessage.msg().convert(param2Value);
			System.assert(false);
		}
		catch (ComTestException ex) { }
		try {
			ComMessage.msg().convert(param2Value, new List<String>{ param1 });
			System.assert(false);
		}
		catch (ComTestException ex) { }
		try {
			ComMessage.msg().convert(param2Value, new List<String>{ param1, param2, param3 });
			System.assert(false);
		}
		catch (ComTestException ex) { }


		String param3ConvertValue = ComMessage.msg().convert(param3Value, new List<String>{ param1, param2, param3 });
		try {
			ComMessage.msg().convert(param3Value);
			System.assert(false);
		}
		catch (ComTestException ex) { }
		try {
			ComMessage.msg().convert(param3Value, new List<String>{ param1, param2 });
			System.assert(false);
		}
		catch (ComTestException ex) { }

		Test.stopTest();

		System.assertEquals(noParamValue, noParamConvertValue);
		System.assertEquals(noParamValue + param1, param1ConvertValue);
		System.assertEquals(noParamValue + param1 + param2, param2ConvertValue);
		System.assertEquals(noParamValue + param1 + param2 + param3, param3ConvertValue);
	}

}