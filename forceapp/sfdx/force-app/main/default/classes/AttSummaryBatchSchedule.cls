/**
 * @group 勤怠
 *
 * @description 勤怠サマリー調整バッチスケジュールクラス
 */
public with sharing class AttSummaryBatchSchedule implements Schedulable {

	/**
	 * 勤怠サマリー調整バッチの実行スケジュールを登録する
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc) {

		// 対象月度は今月度とする
		List<AppDate> targetDates = new List<AppDate>();
		targetDates.add(AppDate.today());
		for (AppDate targetDate : targetDates) {
			AttSummaryBatch batch = new AttSummaryBatch(targetDate);
			Id jobId = Database.executeBatch(batch, AttSummaryBatch.MAX_BATCH_SCOPE);
		}
	}
}