/**
 * @group 勤怠
 *
 * @description AttSummaryBatchのテスト
 */
@isTest
private class AttSummaryBatchTest {

	/** @description バッチのスコープ(2以上の値は設定不可) */
	private static final Integer BATCH_SCOPE = 1;

	/**
	 * @description テストデータ
	 */
	private class BatchTestData {

		/** @description AttTestDataを内包する */
		private AttTestData attTest;

		/** @description 勤務体系 */
		private AttWorkingTypeBaseEntity workingType;

		/** @description 社員 */
		private EmployeeBaseEntity employee;

		/**
		 * @description コンストラクタ
		 */
		public BatchTestData() {
			attTest = new AttTestData(AttWorkSystem.JP_Fix);
			this.workingType = attTest.workingType;
			this.employee = attTest.employee;
		}

		/**
		 * @description 勤怠サマリーを作成する
		 * @param employeeHistoryId 社員履歴ID
		 * @param workingTypeHistoryId 勤務体系履歴ID
		 * @param startDate 勤怠サマリー開始日
		 */
		public void createAttSummary(Id employeeHistoryId, Id workingTypeHistoryId, AppDate startDate) {
			final Boolean createPreNext = false;
			attTest.createAttSummaryWithRecordsMonth(employeeHistoryId, workingTypeHistoryId, startDate, createPreNext);
		}

		/**
		 * 勤務体系データを作成する
		 * @param 労働時間制
		 * @return 作成された勤務体系
		 */
		public AttWorkingTypeBaseEntity createWorkingType(AttWorkSystem wkType) {
			return attTest.createWorkingType(wkType);
		}

		/**
		 * 標準ユーザと社員エンティティを作成して登録する
		 * @param code 社員コード
		 * @return 標準ユーザの社員
		 */
		public EmployeeBaseEntity createEmployeeStandardUser(String code) {
			EmployeeBaseEntity employee = attTest.createEmployeeStandardUser(code);
			EmployeeHistoryEntity employeeHistory = employee.getHistory(0);
			employeeHistory.workingTypeId = attTest.workingType.id;
			new EmployeeRepository().saveHistoryEntity(employeeHistory);
			return employee;
		}
	}

	/**
	 * @description バッチ実行テスト(正常系)
	 * 対象月度に勤怠サマリーが存在しない場合、新規に勤怠サマリーが作成される事を確認する
	 */
	@isTest
	static void executeTestCreateNewSummary() {
		BatchTestData testData = new BatchTestData();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.today();

		// バッチ実行
		// テストではバッチのexecuteは1つしか実行できない
		Test.StartTest();
		AttSummaryBatch b = new AttSummaryBatch(targetDate);
		Id jobId = Database.executeBatch(b, BATCH_SCOPE);
		Test.StopTest();

		System.assertNotEquals(null, jobId);

		// 勤怠サマリーが作成されている
		List<AttSummaryEntity> summaryList = new AttSummaryRepository().getEntityList(null, true, false);
		System.assertEquals(1, summaryList.size());
		AttSummaryEntity summary = summaryList[0];
		System.assertEquals(employee.getHistory(0).id, summary.employeeId);
		System.assertEquals(AppDate.newInstance(targetDate.year(), targetDate.month(), testData.workingType.startDateOfMonth), summary.startDate);
	}

	/**
	 * @description バッチ実行テスト(正常系)
	 * 既に勤怠サマリーが存在している場合、新たに勤怠サマリーが作成されないことを確認する
	 */
	@isTest
	static void executeTestNotCreateNewSummary() {
		BatchTestData testData = new BatchTestData();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.today();

		// 起算日
		AppDate startDate = AppDate.newInstance(targetDate.year(), targetDate.month(), testData.workingType.startDateOfMonth);

		// あらかじめ勤怠サマリーを作成しておく
		testData.createAttSummary(employee.getHistory(0).id, workingType.getHistory(0).id, startDate);

		// バッチ実行
		// テストではバッチのexecuteは1つしか実行できない
		Test.StartTest();
		AttSummaryBatch b = new AttSummaryBatch(targetDate);
		Id jobId = Database.executeBatch(b, BATCH_SCOPE);
		Test.StopTest();

		System.assertNotEquals(null, jobId);

		// 新たに勤怠サマリーが作成されていない
		List<AttSummaryEntity> summaryList = new AttSummaryRepository().getEntityList(null, true, false);
		System.assertEquals(1, summaryList.size());
		AttSummaryEntity summary = summaryList[0];
		System.assertEquals(employee.getHistory(0).id, summary.employeeId);
		System.assertEquals(startDate, summary.startDate);
	}

	/**
	 * @description バッチ実行テスト(正常系)
	 * 既に勤怠サマリーが存在している場合でも、DirtyフラグがONの場合は再計算されることを確認する
	 */
	@isTest
	static void executeTestDirtySummary() {
		AttSummaryRepository summaryRepo = new AttSummaryRepository();

		BatchTestData testData = new BatchTestData();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.newInstance(2019, 6, 3);

		// 起算日
		AppDate startDate = AppDate.newInstance(targetDate.year(), targetDate.month(), testData.workingType.startDateOfMonth);

		// あらかじめ勤怠サマリーを作成しておく
		testData.createAttSummary(employee.getHistory(0).id, workingType.getHistory(0).id, startDate);
		AttSummaryEntity summary = summaryRepo.getEntityList(null, true, false).get(0);
		summary.isDirty = true;
		summaryRepo.saveEntity(summary);

		// バッチ実行
		// テストではバッチのexecuteは1つしか実行できない
		Test.StartTest();
		AttSummaryBatch b = new AttSummaryBatch(targetDate);
		Id jobId = Database.executeBatch(b, BATCH_SCOPE);
		Test.StopTest();

		System.assertNotEquals(null, jobId);

		// 勤怠サマリーが新規に作成されていない
		List<AttSummaryEntity> summaryList = summaryRepo.getEntityList(null, true, false);
		System.assertEquals(1, summaryList.size());
		// 勤怠サマリーが再計算されている(Dirtyフラグがfalseになっている)
		AttSummaryEntity resSummary = summaryList[0];
		System.assertEquals(summary.id, resSummary.id);
		System.assertEquals(false, resSummary.isDirty);
		System.assertEquals(employee.getHistory(0).id, summary.employeeId);
		System.assertEquals(startDate, summary.startDate);
	}

	/**
	 * @description バッチ実行テスト(正常系)
	 * 勤怠サマリー作成後に社員を改定した場合、勤怠サマリーが更新されることを確認する
	 */
	@isTest
	static void executeTestRevisedEmployee() {
		BatchTestData testData = new BatchTestData();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.today();

		// 起算日
		AppDate startDate = AppDate.newInstance(targetDate.year(), targetDate.month(), testData.workingType.startDateOfMonth);

		// あらかじめ勤怠サマリーを作成しておく
		testData.createAttSummary(employee.getHistory(0).id, workingType.getHistory(0).id, startDate);

		// 社員を改定する
		EmployeeRepository empRepo = new EmployeeRepository();
		AttWorkingTypeBaseEntity newWorkingType = testData.createWorkingType(AttWorkSystem.JP_Flex);
		EmployeeHistoryEntity newEmployeeHistory = employee.getHistory(0).copy();
		newEmployeeHistory.workingTypeId = newWorkingType.id;
		employee.getHistory(0).isRemoved = true;
		employee.getHistory(0).uniqkey = employee.getHistory(0).id;
		empRepo.saveHistoryEntity(employee.getHistory(0));
		Id newEmployeeHistoryId = empRepo.saveHistoryEntity(newEmployeeHistory).details[0].id;

		// バッチ実行
		// テストではバッチのexecuteは1つしか実行できない
		Test.StartTest();
		AttSummaryBatch b = new AttSummaryBatch(targetDate);
		Id jobId = Database.executeBatch(b, BATCH_SCOPE);
		Test.StopTest();

		System.assertNotEquals(null, jobId);

		// 勤怠サマリーが更新されている
		List<AttSummaryEntity> summaryList = new AttSummaryRepository().getEntityList(null, true, false);
		System.assertEquals(1, summaryList.size());
		AttSummaryEntity summary = summaryList[0];
		System.assertEquals(newEmployeeHistoryId, summary.employeeId); // 改定後の履歴に更新されている
		System.assertEquals(startDate, summary.startDate);
	}

	/**
	 * @description バッチ実行テスト(正常系)
	 * 勤務体系未設定の社員は勤怠サマリー作成対象外であることを確認する
	 */
	@isTest
	static void executeTestEmployeeWorkingTypeNull() {
		BatchTestData testData = new BatchTestData();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 社員の勤務体系を未設定にする
		employee.getHistory(0).workingTypeId = null;
		new EmployeeRepository().saveHistoryEntity(employee.getHistory(0));

		// 対象日
		AppDate targetDate = AppDate.today();

		// バッチ実行
		// テストではバッチのexecuteは1つしか実行できない
		Test.StartTest();
		AttSummaryBatch b = new AttSummaryBatch(targetDate);
		Id jobId = Database.executeBatch(b, BATCH_SCOPE);
		Test.StopTest();

		System.assertNotEquals(null, jobId);

		// 勤怠サマリーが作成されていない
		List<AttSummaryEntity> summaryList = new AttSummaryRepository().getEntityList(null, true, false);
		System.assertEquals(0, summaryList.size());
	}

/**
	 * @description バッチ実行テスト(正常系)
	 * 対象日に無効な社員は勤怠サマリー作成対象外であることを確認する
	 */
	@isTest
	static void executeTestExpiredEmployee() {
		BatchTestData testData = new BatchTestData();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.today();

		// 社員を失効させる
		employee.getHistory(0).validFrom = targetDate.addMonths(-1);
		employee.getHistory(0).validTo = targetDate;
		new EmployeeRepository().saveHistoryEntity(employee.getHistory(0));

		// バッチ実行
		// テストではバッチのexecuteは1つしか実行できない
		Test.StartTest();
		AttSummaryBatch b = new AttSummaryBatch(targetDate);
		Id jobId = Database.executeBatch(b, BATCH_SCOPE);
		Test.StopTest();

		System.assertNotEquals(null, jobId);

		// 勤怠サマリーが作成されていない
		List<AttSummaryEntity> summaryList = new AttSummaryRepository().getEntityList(null, true, false);
		System.assertEquals(0, summaryList.size());
	}

	/**
	 * @description バッチ実行テスト(異常系)
	 * execute()のパラメータscopeリストのサイズが1より多い場合に想定される例外が発生することを確認する
	 */
	@isTest
	static void executeTestInvalidScopeSize() {
		BatchTestData testData = new BatchTestData();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;
		EmployeeBaseEntity employee2 = testData.createEmployeeStandardUser('Test2');

		// 対象日
		AppDate targetDate = AppDate.today();

		// バッチ実行
		App.ParameterException actEx;
		try {
			Test.StartTest();
				Database.executeBatch(new AttSummaryBatch(targetDate), BATCH_SCOPE + 1);
			Test.StopTest();
		} catch(App.ParameterException e) {
			actEx = e;
		}

		System.assertNotEquals(null, actEx);
		System.assertEquals('Scope size is too large (scope.size()=2)', actEx.getMessage());
	}
}