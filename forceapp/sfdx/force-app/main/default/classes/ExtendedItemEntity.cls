/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 拡張項目のエンティティ
 */
public with sharing class ExtendedItemEntity extends LogicalDeleteEntity {

	/** コードの最大文字列長さ */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 名前の最大文字列長さ */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 最大文字数の最小値 */
	public static final Integer LIMIT_LENGTH_MIN_VALUE = 1;
	/** 最大文字数の最大値 */
	public static final Integer LIMIT_LENGTH_MAX_VALUE = 255;
	/** 説明の最大文字数 */
	public static final Integer DESCRIPTION_MAX_LENGTH = 1024;
	/** 選択リストの最大数 */
	public static final Integer PICKLIST_MAX_SIZE = 25;
	/** 選択リストラベルの最大文字列長さ */
	public static final Integer PICKLIST_LABEL_MAX_LENGTH = 80;
	/** 選択リスト値の最大文字列長さ */
	public static final Integer PICKLIST_VALUE_MAX_LENGTH = 80;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		CODE,
		UNIQ_KEY,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		COMPANY_ID,
		INPUT_TYPE,
		LIMIT_LENGTH,
		DEFAULT_VALUE_TEXT,
		PICKLIST,
		EXTENDED_ITEM_CUSTOM_ID,
		DESCRIPTION_L0,
		DESCRIPTION_L1,
		DESCRIPTION_L2
	}

	/** 値を変更した項目のリスト */
	private Set<Field> changedFieldSet;

	/** 選択リスト情報 */
	public class PicklistOption {
		/** ラベル(L0) */
		public String label_L0;
		/** ラベル(L1) */
		public String label_L1;
		/** ラベル(L2) */
		public String label_L2;
		/** Label (Multi String) */
		public AppMultiString labelL;
		/** ラベル(翻訳) */
		public String label;
		/** 値 */
		public String value;
	}

	/** 拡張項目名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** 拡張項目コード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	/** ユニークキー */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}

	/** 拡張項目名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	/** 拡張項目名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	/** 拡張項目名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/** 拡張項目名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}

	/** 会社ID */
	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	/** 入力タイプ */
	public ExtendedItemInputType inputType {
		get;
		set {
			inputType = value;
			setChanged(Field.INPUT_TYPE);
		}
	}

	/** 最大文字数 */
	public Integer limitLength {
		get;
		set {
			limitLength = value;
			setChanged(Field.LIMIT_LENGTH);
		}
	}

	/** 初期値（テキスト） */
	public String defaultValueText {
		get;
		set {
			defaultValueText = value;
			setChanged(Field.DEFAULT_VALUE_TEXT);
		}
	}

	/** 選択リスト */
	public List<PicklistOption> picklist {
		get;
		set {
			picklist = value;
			setChanged(Field.PICKLIST);
		}
	}

	public Id extendedItemCustomId {
		get;
		set {
			extendedItemCustomId = value;
			setChanged(Field.EXTENDED_ITEM_CUSTOM_ID);
		}
	}

	/*
	 * Data is not saved to the Repo. Only for Display
	 */
	public AppMultiString extendedItemCustomName;

	/** 説明(L0) */
	public String descriptionL0 {
		get;
		set {
			descriptionL0 = value;
			setChanged(Field.DESCRIPTION_L0);
		}
	}

	/** 説明(L1) */
	public String descriptionL1 {
		get;
		set {
			descriptionL1 = value;
			setChanged(Field.DESCRIPTION_L1);
		}
	}

	/** 説明(L2) */
	public String descriptionL2 {
		get;
		set {
			descriptionL2 = value;
			setChanged(Field.DESCRIPTION_L2);
		}
	}

	/** 説明(翻訳) */
	public AppMultiString descriptionL {
		get {return new AppMultiString(this.descriptionL0, this.descriptionL1, this.descriptionL2);}
	}


	/**
	 * コンストラクタ
	 */
	public ExtendedItemEntity() {
		changedFieldSet = new Set<Field>();
	}

	/**
	 * ユニークキーを作成する
	 * @param companyCode 会社コード
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]拡張項目のユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]拡張項目のユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + コード
		return companyCode + '-' + this.code;
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.changedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	/**
	 * (親クラスを含まない)クラス内の項目変更情報をリセットする
	 */
	protected override void resetChangedInSubClass() {
		this.changedFieldSet.clear();
	}
}