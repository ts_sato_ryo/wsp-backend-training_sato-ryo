/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 履歴管理方式が有効期間型のマスタのエンティティ基底クラス
 */
public abstract with sharing class ValidPeriodEntity extends BaseEntity {

	/** 有効開始日の最小日付 */
	public static final AppDate VALID_FROM_MIN = AppDate.newInstance(1900, 1, 1);
	/** 有効開始日の最大日付 */
	public static final AppDate VALID_FROM_MAX = AppDate.newInstance(2100, 12, 31);
	/** 失効日の最小日付 */
	public static final AppDate VALID_TO_MIN = AppDate.newInstance(1900, 1, 1);
	/** 失効日の最大日付 */
	public static final AppDate VALID_TO_MAX = AppDate.newInstance(2101, 1, 1);

	/**
	 * コンストラクタ
	 * @param sobj 参照するSObject
	 */
	public ValidPeriodEntity(SObject sobj, Map<String, Schema.SObjectField> sobjectFieldMap) {
		super(sobj, sobjectFieldMap);
	}

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目値 */
		public AppMultiString nameL;
		/** コンストラクタ */
		public LookupField(AppMultiString nameL) {
			this.nameL = nameL;
		}
	}

	/** レコード名 */
	public static final String FIELD_NAME_NAME = 'Name';
	public String name {
		get {
			return (String)getFieldValue(FIELD_NAME_NAME);
		}
		set {
			setFieldValue(FIELD_NAME_NAME, value);
		}
	}

	/** 有効開始日 */
	public static final String FIELD_NAME_VALID_FROM = createNameWithNamespace('ValidFrom__c');
	public AppDate validFrom {
		get {
			return AppDate.valueOf((Date)getFieldValue(FIELD_NAME_VALID_FROM));
		}
		set {
			setFieldValue(FIELD_NAME_VALID_FROM, AppConverter.dateValue(value));
		}
	}

	/** 失効日 */
	public static final String FIELD_NAME_VALID_TO = createNameWithNamespace('ValidTo__c');
	public AppDate validTo {
		get {
			return AppDate.valueOf((Date)getFieldValue(FIELD_NAME_VALID_TO));
		}
		set {
			setFieldValue(FIELD_NAME_VALID_TO, AppConverter.dateValue(value));
		}
	}

	/**
	 * エンティティが指定した日に有効であるかをチェックする
	 * @param targetDate 指定日
	 * @return エンティティが指定した日に有効である場合はtrue、そうでない場合はfalse
	 *         有効開始日、失効日のどちらかがnullの場合もfalse
	 */
	public Boolean isValid(AppDate targetDate) {
		if (targetDate == null) {
			return false;
		}
		Date targetDt = targetDate.getDate();

		if (validFrom == null || validTo == null) {
			return false;
		}
		Date fromDt = validFrom.getDate();
		Date toDt = validTo.getDate();
		return fromDt <= targetDt && targetDt < toDt;
	}
}