/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * @description ExpTypeGroupEntityのテスト
 */
@isTest
private class ExpTypeGroupEntityTest {

	/**
	 * テストデータクラス
	 */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * 費目グループオブジェクトを作成する
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Integer size) {
			return createExpTypeGroups(name, this.company.id, size);
		}

		/**
		 * 費目グループオブジェクトを作成する
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpTypeGroups(name, companyId, size);
			return (new ExpTypeGroupRepository()).getEntityList(null);
		}

		/**
		 * 費目グループエンティティを作成する(DB保存なし)
		 */
		public ExpTypeGroupEntity createExpTypeGroupEntity(String name) {
			ExpTypeGroupEntity entity = new ExpTypeGroupEntity();

			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.company.code + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.company.id;
			entity.parentId = null;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.order = 1;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);

			return entity;
		}
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		ExpTypeGroupEntity entity = new ExpTypeGroupEntity();

		// 会社コードと費目グループコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'GroupCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// 費目グループコードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'GroupCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/**
	 * copyのテスト
	 * エンティティが複製できることを確認する
	 */
	@isTest static void copyTest() {
		ExpTypeGroupEntityTest.TestData testData = new TestData();
		ExpTypeGroupEntity orgEntity = testData.createExpTypeGroups('Test', 2)[1];

		ExpTypeGroupEntity copyEntity = orgEntity.copy();

		System.assertEquals(null, copyEntity.id);  // IDはコピーされない
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.nameL0, copyEntity.nameL0);
		System.assertEquals(orgEntity.nameL1, copyEntity.nameL1);
		System.assertEquals(orgEntity.nameL2, copyEntity.nameL2);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.parentId, copyEntity.parentId);
		System.assertEquals(orgEntity.descriptionL0, copyEntity.descriptionL0);
		System.assertEquals(orgEntity.descriptionL1, copyEntity.descriptionL1);
		System.assertEquals(orgEntity.descriptionL2, copyEntity.descriptionL2);
		System.assertEquals(orgEntity.order, copyEntity.order);
		System.assertEquals(orgEntity.validFrom, copyEntity.validFrom);
		System.assertEquals(orgEntity.validTo, copyEntity.validTo);
	}
}