/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 工数
 *
 * 工数マスタ管理系APIの権限チェック機能を提供するクラス
 * 工数マスタ管理系のResourceクラス以外からは呼び出さないでください
 */
public with sharing class TimeConfigResourcePermission {

	/** API実行に必要な権限 */
	public enum Permission {
		/** 工数設定の管理 */
		MANAGE_TIME_SETTING,
		/** 作業分類の管理 */
		MANAGE_TIME_WORK_CATEGORY
	}

	/** 社員の権限サービス */
	private EmployeePermissionService permissionService;

	/**
	 * コンストラクタ
	 */
	public TimeConfigResourcePermission() {
		this.permissionService = new EmployeePermissionService();
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(TimeConfigResourcePermission.Permission requiredPermission) {
		hasExecutePermission(new List<TimeConfigResourcePermission.Permission>{requiredPermission});
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(List<TimeConfigResourcePermission.Permission> requiredPermissionList) {
		List<TimeConfigResourcePermission.Permission> errorPermissionList =
				new List<TimeConfigResourcePermission.Permission>();

		for (Permission requiredPermission : requiredPermissionList) {
			// 工数設定の管理
			if (requiredPermission == Permission.MANAGE_TIME_SETTING) {
				if (! permissionService.hasManageTimeSetting) {
					errorPermissionList.add(Permission.MANAGE_TIME_SETTING);
				}
			// 作業分類の管理
			} else if (requiredPermission == Permission.MANAGE_TIME_WORK_CATEGORY) {
				if (! permissionService.hasManageTimeWorkCategory) {
					errorPermissionList.add(Permission.MANAGE_TIME_WORK_CATEGORY);
				}
			// Error! 実装ミス
			} else {
				throw new App.UnsupportedException('Unsupported Permission(' + requiredPermission + ')');
			}
		}

		if (! errorPermissionList.isEmpty()) {
			// デバッグ用にログを出力しておく
			System.debug('--- Insufficient permission for API=' + errorPermissionList);
			throw new App.NoPermissionException(ComMessage.msg().Com_Err_NoApiPermission);
		}
	}
}