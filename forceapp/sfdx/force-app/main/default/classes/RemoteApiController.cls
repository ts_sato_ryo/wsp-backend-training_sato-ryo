/**
 * アプリで唯一のVisualforceページコントローラ。ページとApexの橋渡しのみを行う。
 * #ビジネスロジックの変更を理由に編集することは禁止です
 * #FEパフォーマンスの都合上やむを得ず本クラスからVFページにプロパティ値を提供する場合は
 *  事前に必ずBEテックリードと相談してください
 * TODO: 旧コントローラからの移行期間を設けるため、一時的にvirtualクラスとして定義しています
 */
public virtual with sharing class RemoteApiController {

	/**
	 * プロパティ値を格納するためのコンストラクタ
	 */
	public RemoteApiController() {
		this.languageLocaleKey = UserInfo.getLanguage().replace('_', '-');
		EmployeeBaseEntity employee;
		try {
			this.employeePremission = getPermission(employee);
		} catch (Exception e) {
			// ページが表示されるように権限なしで設定しておく
			this.employeePremission = new RemoteApiController.Permission();
		}
	}

	/**
	 * @return
	 * ブラウザタブに表示されるページのタイトルのMap
	 * - webpack.config.production.babel.js のpageTitle項目で指定したものをキーとして、翻訳ラベルを返す。
	 * - キーは日英対応表作成ルールのフォーマットの記法に準ずる。
	 * - Mapに追加したら、仕様書「詳細仕様：Webタブ表示」に追加してください。
	 * Map of page title displayed on Web Tab
	 *  - Returns the translation label with the key specified in the pageTitle item of webpack.config.production.babel.js
	 *  - The key follows the notation of the Japanese-English correspondence table creation rule format.
	 *  - Please also change the document '"Detailed specification: Web tab display" if you add the map item.
	 */
	public Map<String, String> pageTitleMap {
		get {
			Map<String, String> titleMap = new Map<String, String> {
				// Common
				'Admin' => ComMessage.msg().Com_Tab_Admin,
				'OAuthResultView' => ComMessage.msg().Com_Tab_OAuthResultView,
				'Approval' => ComMessage.msg().Com_Tab_Approval,
				'Team' => ComMessage.msg().Com_Tab_Team,
				'Planner' => ComMessage.msg().Com_Tab_Planner,
				// Attendance
				'TimeAttendance' => ComMessage.msg().Com_Tab_TimeAttendance,
				// Time Tracking
				'Tracking' => ComMessage.msg().Com_Tab_Tracking,
				// Expenses
				'Expenses' => ComMessage.msg().Com_Tab_Expenses,
				'Requests' => ComMessage.msg().Com_Tab_Requests,
				'FinanceApproval' => ComMessage.msg().Com_Tab_FinanceApproval};
			return titleMap;
		}
	}

	/**
	 * ユーザの言語設定(kebab-case)
	 * ※FEのパフォーマンスの都合上やむを得ずここに定義しています
	 * ユーザ設定の値をこのクラスに追加する場合は、事前にBEテックリードと相談してください
	 */
	public String languageLocaleKey { get; private set; }

	/** * カスタムラベルマップ(JSON) */
	public String customMessageMapJson {
		get {
			return JSON.serialize(this.customMessageMap);
		}
	}

	/** * カスタムラベルマップ */
	private Map<String, Map<String, String>> customMessageMap {
		get {
			// FE側でカスタマイズ可能であるかの判定に利用するためメッセージIDの先頭に「$」をつける
			if (customMessageMap == null) {
				Map<String, Map<String, String>> orgLangMsgMap = ComMessage.getChangedCustomMessageMap();
				customMessageMap = new Map<String, Map<String, String>>();
				for (String lang : orgLangMsgMap.keySet()) {
					Map<String, String> orgMsgMap = orgLangMsgMap.get(lang);
					Map<String, String> msgMap = new Map<String, String>();
					for (String msgId : orgMsgMap.keySet()) {
						msgMap.put('$'+ msgId, orgMsgMap.get(msgId));
					}
					customMessageMap.put(lang, msgMap);
				}
			}
			return this.customMessageMap;
		}
		set;
	}

	/** ログインユーザ社員の権限情報(※FEのパフォーマンスの都合上やむを得ずここに定義しています) */
	@testVisible
	private Permission employeePremission;

	/** ログインユーザ社員の権限情報(JSON) */
	public String employeePremissionJson {
		get {
			return JSON.serialize(this.employeePremission);
		}
	}

	/**
	 * 権限情報
	 * 各権限の初期値は原則falseにしてください
	 */
	private class Permission {
		//-----------------------
		// 勤怠系(Attendance)
		//-----------------------
		/** 勤務表表示(代理)権限 */
		public Boolean viewAttTimeSheetByDelegate = false;
		/** 勤務表編集(代理)権限 */
		public Boolean editAttTimeSheetByDelegate = false;
		/** 各種勤怠申請申請・申請削除(代理)権限 */
		public Boolean submitAttDailyRequestByDelegate = false;
		/** 各種勤怠申請承認却下(代理)権限 */
		public Boolean approveAttDailyRequestByDelegate = false;
		/** 各種勤怠申請自己承認(本人)権限 */
		public Boolean approveSelfAttDailyRequestByEmployee = false;
		/** 各種勤怠申請申請取消(代理)権限 */
		public Boolean cancelAttDailyRequestByDelegate = false;
		/** 各種勤怠申請承認取消(本人)権限 */
		public Boolean cancelAttDailyApprovalByEmployee = false;
		/** 各種勤怠申請承認取消(代理)権限 */
		public Boolean cancelAttDailyApprovalByDelegate = false;
		/** 勤務確定申請申請(代理)権限 */
		public Boolean submitAttRequestByDelegate = false;
		/** 勤務確定申請申請承認却下(代理)権限 */
		public Boolean approveAttRequestByDelegate = false;
		/** 勤務確定申請自己承認(本人)権限 */
		public Boolean approveSelfAttRequestByEmployee = false;
		/** 勤務確定申請申請取消(代理)権限 */
		public Boolean cancelAttRequestByDelegate = false;
		/** 勤務確定申請承認取消(本人)権限 */
		public Boolean cancelAttApprovalByEmployee = false;
		/** 勤務確定申請承認取消(代理)権限 */
		public Boolean cancelAttApprovalByDelegate = false;

		//-----------------------
		// 工数系(TimeTrack)
		//-----------------------
		/** 工数実績表示(代理) */
		public Boolean viewTimeTrackByDelegate = false;

		//-----------------------
		// マスタ管理系(Master)
		//-----------------------
		/** 全体設定の管理権限 */
		public Boolean manageOverallSetting = false;
		/** 会社の切り替えの権限 */
		public Boolean switchCompany = false;
		/** 部署の管理権限 */
		public Boolean manageDepartment = false;
		/** 社員の管理権限 */
		public Boolean manageEmployee = false;
		/** カレンダーの管理権限 */
		public Boolean manageCalendar = false;
		/** ジョブタイプの管理権限 */
		public Boolean manageJobType = false;
		/** ジョブの管理権限 */
		public Boolean manageJob = false;
		/** モバイル機能設定の管理権限 */
		public Boolean manageMobileSetting = false;
		/** プランナー機能設定の管理権限 */
		public Boolean managePlannerSetting = false;
		/** アクセス権限設定の管理権限 */
		public Boolean managePermission = false;
		/** 休暇の管理 */
		public Boolean manageAttLeave = false;
		/** 短時間勤務設定の管理 */
		public Boolean manageAttShortTimeWorkSetting = false;
		/** 休職・休業の管理 */
		public Boolean manageAttLeaveOfAbsence = false;
		/** 勤務体系の管理 */
		public Boolean manageAttWorkingType = false;
		/** 勤務パターンの管理 */
		public Boolean manageAttPattern = false;
		/** 残業警告設定の管理 */
		public Boolean manageAttAgreementAlertSetting = false;
		/** 休暇管理の管理 */
		public Boolean manageAttLeaveGrant = false;
		/** 勤務パターン適用の管理 */
		public Boolean manageAttPatternApply = false;
		/** 短時間勤務設定適用の管理 */
		public Boolean manageAttShortTimeWorkSettingApply = false;
		/** 休職・休業適用の管理 */
		public Boolean manageAttLeaveOfAbsenceApply = false;
		/** 工数設定の管理 */
		public Boolean manageTimeSetting = false;
		/** 作業分類の管理権 */
		public Boolean manageTimeWorkCategory = false;
		/** manage of expense type group */
		public Boolean manageExpTypeGroup = false;
		/** manage of expense type */
		public Boolean manageExpenseType = false;
		/** manage of tax type */
		public Boolean manageTaxType = false;
		/** manage of expense setting */
		public Boolean manageExpSetting = false;
		/** manage of exchenage rate*/
		public Boolean manageExchangeRate = false;
		/** manage of accounting rate */
		public Boolean manageAccountingPeriod = false;
		/** manage of report type */
		public Boolean manageReportType = false;
		/** manage of cost center */
		public Boolean manageCostCenter = false;
		/** manage of vendor */
		public Boolean manageVendor = false;
		/** manage of extended item */
		public Boolean manageExtendedItem = false;
		/** manage of group */
		public Boolean manageEmployeeGroup = false;
		/** manage of custom hint */
		public Boolean manageExpCustomHint = false;
		
	}

	/**
	 * 今日時点のログインユーザの社員の権限を取得する
	 */
	private RemoteApiController.Permission getPermission(EmployeeBaseEntity employee) {
		EmployeePermissionService service = new EmployeePermissionService();
		RemoteApiController.Permission permission = new RemoteApiController.Permission();
		permission.viewAttTimeSheetByDelegate = service.hasViewAttTimeSheetByDelegate;
		permission.editAttTimeSheetByDelegate = service.hasEditAttTimeSheetByDelegate;
		permission.submitAttDailyRequestByDelegate = service.hasSubmitAttDailyRequestByDelegate;
		permission.approveAttDailyRequestByDelegate = service.hasApproveAttDailyRequestByDelegate;
		permission.approveSelfAttDailyRequestByEmployee = service.hasApproveSelfAttDailyRequestByEmployee;
		permission.cancelAttDailyRequestByDelegate = service.hasCancelAttDailyRequestByDelegate;
		permission.cancelAttDailyApprovalByEmployee = service.hasCancelAttDailyApprovalByEmployee;
		permission.cancelAttDailyApprovalByDelegate = service.hasCancelAttDailyApprovalByDelegate;
		permission.submitAttRequestByDelegate = service.hasSubmitAttRequestByDelegate;
		permission.approveAttRequestByDelegate = service.hasApproveAttRequestByDelegate;
		permission.approveSelfAttRequestByEmployee = service.hasApproveSelfAttRequestByEmployee;
		permission.cancelAttRequestByDelegate = service.hasCancelAttRequestByDelegate;
		permission.cancelAttApprovalByEmployee = service.hasCancelAttApprovalByEmployee;
		permission.cancelAttApprovalByDelegate = service.hasCancelAttApprovalByDelegate;

		permission.viewTimeTrackByDelegate = service.hasViewTimeTrackByDelegate;

		permission.manageOverallSetting = service.hasManageOverallSetting;
		permission.switchCompany = service.hasSwitchCompany;
		permission.manageDepartment = service.hasManageDepartment;
		permission.manageEmployee = service.hasManageEmployee;
		permission.manageCalendar = service.hasManageCalendar;
		permission.manageJobType = service.hasManageJobType;
		permission.manageJob = service.hasManageJob;
		permission.manageMobileSetting = service.hasManageMobileSetting;
		permission.managePlannerSetting = service.hasManagePlannerSetting;
		permission.managePermission = service.hasManagePermission;
		permission.manageAttLeave = service.hasManageAttLeave;
		permission.manageAttShortTimeWorkSetting = service.hasManageAttShortTimeWorkSetting;
		permission.manageAttLeaveOfAbsence = service.hasManageAttLeaveOfAbsence;
		permission.manageAttPatternApply = service.hasManageAttPatternApply;
		permission.manageAttWorkingType = service.hasManageAttWorkingType;
		permission.manageAttPattern = service.hasManageAttPattern;
		permission.manageAttAgreementAlertSetting = service.hasManageAttAgreementAlertSetting;
		permission.manageAttLeaveGrant = service.hasManageAttLeaveGrant;
		permission.manageAttShortTimeWorkSettingApply = service.hasManageAttShortTimeWorkSettingApply;
		permission.manageAttLeaveOfAbsenceApply = service.hasManageAttLeaveOfAbsenceApply;
		permission.manageTimeSetting = service.hasManageTimeSetting;
		permission.manageTimeWorkCategory = service.hasManageTimeWorkCategory;

		permission.manageExpTypeGroup = service.hasManageExpTypeGroup;
		permission.manageExpenseType = service.hasManageExpenseType;
		permission.manageTaxType = service.hasManageTaxType;
		permission.manageExpSetting = service.hasManageExpSetting;
		permission.manageExchangeRate = service.hasManageExchangeRate;
		permission.manageAccountingPeriod = service.hasManageAccountingPeriod;
		permission.manageReportType = service.hasManageReportType;
		permission.manageCostCenter = service.hasManageCostCenter;
		permission.manageVendor = service.hasManageVendor;
		permission.manageExtendedItem = service.hasManageExtendedItem;
		permission.manageEmployeeGroup = service.hasManageEmployeeGroup;
		permission.manageExpCustomHint = service.hasManageExpCustomHint;

		return permission;
	}

	/**
	 * アプリで唯一のRemoteActionメソッド。Visualforceページからのリクエストは必ずここを通る
	 * @param  paramJSON リクエストパラメータ(JSON形式)。形式は次のとおり:
	 *     [String] path APIのパス
	 *     [Object] param API実行パラメータ
	 * @return  APIレスポンス(JSON形式)
	 */
	@RemoteAction
	public static String invoke(String paramJSON){
		// APIを実行
		RemoteApi.ResponseBase res = RemoteApiRoute.execute(paramJSON);
		System.debug(LoggingLevel.DEBUG, 'RemoteApiController : ' + AppLimits.getInstance());

		// 実行結果をJSON文字列に変換して返す
		return JSON.serialize(res);
	}
}