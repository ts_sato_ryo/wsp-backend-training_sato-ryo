/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 工数
 *
 * @description TimeConfigValidatorのテスト
 */
@isTest
private class TimeConfigValidatorTest {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 工数設定のラベル */
	private static final String TIME_SETTING_LABEL = ComMessage.msg().Admin_Lbl_TimeSetting;

	/** 会社 */
	private static final ComCompany__c companyObj;
	static {
		companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
	}

	/**
	 * 月の工数設定ベースエンティティのテストデータを作成する
	 */
	private static TimeSettingBaseEntity createMonthlyTimeSettingBaseEntity() {
		TimeSettingBaseEntity entity = new TimeSettingBaseEntity();
		entity.name = 'Test';
		entity.code = 'test001';
		entity.companyId = companyObj.Id;
		entity.startDateOfMonth = 1;
		entity.summaryPeriod = TimeSettingBaseGeneratedEntity.SummaryPeriodType.Month;
		entity.monthMark = TimeSettingBaseGeneratedEntity.MonthMarkType.BeginBase;
		return entity;
	}

	/**
	 * 週の工数設定ベースエンティティのテストデータを作成する
	 */
	private static TimeSettingBaseEntity createWeeklyTimeSettingBaseEntity() {
		TimeSettingBaseEntity entity = new TimeSettingBaseEntity();
		entity.name = 'TestWeekly';
		entity.code = 'test002';
		entity.companyId = companyObj.Id;
		entity.summaryPeriod = TimeSettingBaseGeneratedEntity.SummaryPeriodType.Week;
		entity.setStartDayOfWeek('Thursday');
		entity.monthMark = TimeSettingBaseGeneratedEntity.MonthMarkType.BeginBase;
		return entity;
	}

	/**
	 * 工数設定履歴エンティティのテストデータを作成する
	 */
	private static TimeSettingHistoryEntity createTimeSettingHistoryEntity() {
		TimeSettingHistoryEntity entity = new TimeSettingHistoryEntity();
		entity.name = 'Test';
		entity.nameL0 = 'Test_L0';
		entity.baseId = UserInfo.getUserId();
		entity.uniqKey = '123456';
		entity.validFrom = AppDate.newInstance(1900, 1, 2);
		entity.validTo = AppDate.newInstance(2100, 12, 31);
		entity.historyComment = 'Test Comment';
		return entity;
	}

	/**
	 * 作業分類エンティティのテストデータを作成する
	 */
	private static WorkCategoryEntity createWorkCategoryEntity() {
		WorkCategoryEntity entity = new WorkCategoryEntity();
		entity.name = 'Test';
		entity.nameL0 = 'Test_L0';
		entity.code = 'test001';
		entity.companyId = companyObj.Id;
		entity.order = 1;
		entity.validFrom = AppDate.newInstance(1900, 1, 2);
		entity.validTo = AppDate.newInstance(2100, 12, 31);
		return entity;
	}

	/**
	 * 指定した文字数のテスト文字列を作成する
	 */
	private static String createString(Integer length) {
		String s = '';
		for (Integer i = 0; i < length; i++) {
			s += 'T';
		}
		return s;
	}


	/**
	 * TimeSettingBaseValidatorのテスト
	 * 工数設定ベースバリデーターが正しく動作することを確認する
	 */
	@isTest static void timeSettingBaseValidatorTest() {
		TimeConfigValidator.TimeSettingBaseValidator validator;
		Validator.Result result;
		TimeSettingBaseEntity entity;

		// 全ての項目に正しい値が設定されている → OK
		entity = createMonthlyTimeSettingBaseEntity();
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// コードが設定されていない →　NG
		entity = createMonthlyTimeSettingBaseEntity();
		entity.code = null;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Com_Lbl_Code}), result.getErrors().get(0).message);
		// コードが最大文字を超えている → NG
		entity.code = createString(TimeSettingBaseEntity.CODE_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		assertErrorMaxLengthOver(result.getErrors().get(0), MESSAGE.Com_Lbl_Code, TimeSettingBaseEntity.CODE_MAX_LENGTH);
		// コードが最大文字 → OK
		entity.code = createString(TimeSettingBaseEntity.CODE_MAX_LENGTH);
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 会社IDが設定されていない → NG
		entity = createMonthlyTimeSettingBaseEntity();
		entity.companyId = null;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Com_Lbl_Company}), result.getErrors().get(0).message);

		// サマリー期間が設定されていない → NG
		entity = createMonthlyTimeSettingBaseEntity();
		entity.summaryPeriod = null;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Admin_Lbl_SummaryPeriod}), result.getErrors().get(0).message);

		// 月度の開始日が設定されていない → NG
		entity = createMonthlyTimeSettingBaseEntity();
		entity.startDateOfMonth = null;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Admin_Lbl_StartDateOfMonth}), result.getErrors().get(0).message);
		// 月度の開始日が1未満である → NG
		entity = createMonthlyTimeSettingBaseEntity();
		entity.startDateOfMonth = 0;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		assertErrorValueRange(result.getErrors().get(0),
				MESSAGE.Admin_Lbl_StartDateOfMonth,
				TimeSettingBaseEntity.START_DATE_OF_MONTH_MIN, TimeSettingBaseEntity.START_DATE_OF_MONTH_MAX);
		// 月度の開始日が29である → NG
		entity = createMonthlyTimeSettingBaseEntity();
		entity.startDateOfMonth = 29;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		assertErrorValueRange(result.getErrors().get(0),
				MESSAGE.Admin_Lbl_StartDateOfMonth,
				TimeSettingBaseEntity.START_DATE_OF_MONTH_MIN, TimeSettingBaseEntity.START_DATE_OF_MONTH_MAX);
		// 月度の開始日が28である → OK
		entity = createMonthlyTimeSettingBaseEntity();
		entity.startDateOfMonth = 28;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 月度表記が設定されていない → NG
		entity = createMonthlyTimeSettingBaseEntity();
		entity.monthMark = null;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Admin_Lbl_MonthMark}), result.getErrors().get(0).message);
	}

		/**
	 * TimeSettingBaseValidatorのテスト
	 * 週で工数設定ベースバリデーターが正しく動作することを確認する
	 */
	@isTest static void timeSettingBaseValidatorWeeklyTest() {
		TimeConfigValidator.TimeSettingBaseValidator validator;
		Validator.Result result;
		TimeSettingBaseEntity entity;

		// 全ての項目に正しい値が設定されている → OK
		entity = createWeeklyTimeSettingBaseEntity();
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 起算曜日が設定されていない → NG
		entity = createWeeklyTimeSettingBaseEntity();
		entity.startDayOfWeek = null;
		result = (new TimeConfigValidator.TimeSettingBaseValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Admin_Lbl_StartDayOfWeek}), result.getErrors().get(0).message);
	}

	/**
	 * TimeSettingHistoryValidatorのテスト
	 * 工数設定履歴バリデーターが正しく動作することを確認する
	 */
	@isTest static void timeSettingHistoryValidatorTest() {
		TimeConfigValidator.TimeSettingHistoryValidator validator;
		Validator.Result result;
		TimeSettingHistoryEntity entity;
		TimeSettingBaseEntity baseEntity;

		// 全ての項目に正しい値が設定されている → OK
		entity = createTimeSettingHistoryEntity();
		baseEntity = createMonthlyTimeSettingBaseEntity();
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 履歴管理項目がチェックされていることを確認する
		entity = createTimeSettingHistoryEntity();
		entity.validFrom = null;
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(false, result.isSuccess());

		// 工数設定名が設定されていない → NG
		entity = createTimeSettingHistoryEntity();
		entity.nameL0 = '';
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{createNameLangLabal(MESSAGE.Admin_Lbl_Language0)}), result.getErrors().get(0).message);

		// 工数設定名(L0)が最大文字を超えている → NG
		entity.nameL0 = createString(TimeSettingHistoryEntity.NAME_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(false, result.isSuccess());
		assertErrorMaxLengthOver(result.getErrors().get(0), createNameLangLabal(MESSAGE.Admin_Lbl_Language0),
				TimeSettingHistoryEntity.NAME_MAX_LENGTH);
		// 工数設定名(L0)が最大文字 → OK
		entity.nameL0 = createString(TimeSettingHistoryEntity.NAME_MAX_LENGTH);
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 工数設定名(L1)が最大文字を超えている → NG
		entity.nameL1 = createString(TimeSettingHistoryEntity.NAME_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(false, result.isSuccess());
		assertErrorMaxLengthOver(result.getErrors().get(0), createNameLangLabal(MESSAGE.Admin_Lbl_Language1),
				TimeSettingHistoryEntity.NAME_MAX_LENGTH);
		// 工数設定名(L1)が最大文字 → OK
		entity.nameL1 = createString(TimeSettingHistoryEntity.NAME_MAX_LENGTH);
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 工数設定名(L2)が最大文字を超えている → NG
		entity.nameL2 = createString(TimeSettingHistoryEntity.NAME_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(false, result.isSuccess());
		assertErrorMaxLengthOver(result.getErrors().get(0), createNameLangLabal(MESSAGE.Admin_Lbl_Language2),
				TimeSettingHistoryEntity.NAME_MAX_LENGTH);
		// 工数設定名(L2)が最大文字 → OK
		entity.nameL2 = createString(TimeSettingHistoryEntity.NAME_MAX_LENGTH);
		result = (new TimeConfigValidator.TimeSettingHistoryValidator(baseEntity, entity)).validate();
		System.assertEquals(true, result.isSuccess());

	}

	/**
	 * WorkCategoryValidatorのテスト
	 * 作業分類バリデーターが正しく動作することを確認する
	 */
	@isTest static void workCategoryValidatorTest() {
		TimeConfigValidator.WorkCategoryValidator validator;
		Validator.Result result;
		WorkCategoryEntity entity;

		// 全ての項目に正しい値が設定されている → OK
		entity = createWorkCategoryEntity();
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// コードが設定されていない → NG
		entity = createWorkCategoryEntity();
		entity.code = '';
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Com_Lbl_Code}), result.getErrors().get(0).message);
		// コードが最大文字数を超えている → NG
		entity.code = createString(WorkCategoryEntity.CODE_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(
				new List<String>{MESSAGE.Com_Lbl_Code, WorkCategoryEntity.CODE_MAX_LENGTH.format()}),
				result.getErrors().get(0).message);
		// コードが最大文字数 → OK
		entity.code = createString(WorkCategoryEntity.CODE_MAX_LENGTH);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 会社IDが設定されていない → NG
		entity = createWorkCategoryEntity();
		entity.companyId = null;
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{MESSAGE.Com_Lbl_Company}), result.getErrors().get(0).message);

		// 作業分類名(L0)が設定されていない→ NG
		entity = createWorkCategoryEntity();
		entity.nameL0 = '';
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		String nameL0Label = MESSAGE.Admin_Lbl_Name;
		System.assertEquals(MESSAGE.Com_Err_NotSetValue(
				new List<String>{nameL0Label}), result.getErrors().get(0).message);

		// 作業分類名(L0)の文字数が最大文字数を超えている → NG
		entity = createWorkCategoryEntity();
		entity.nameL0 = createString(WorkCategoryEntity.NAME_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(
				new List<String>{MESSAGE.Admin_Lbl_Name, WorkCategoryEntity.NAME_MAX_LENGTH.format()}),
				result.getErrors().get(0).message);
		// 最大文字数 → OK
		entity.nameL0 = createString(WorkCategoryEntity.NAME_MAX_LENGTH);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 作業分類名(L1)の文字数が最大文字数を超えている → NG
		entity = createWorkCategoryEntity();
		entity.nameL1 = createString(WorkCategoryEntity.NAME_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(
				new List<String>{MESSAGE.Admin_Lbl_Name, WorkCategoryEntity.NAME_MAX_LENGTH.format()}),
				result.getErrors().get(0).message);
		// 最大文字数 → OK
		entity.nameL1 = createString(WorkCategoryEntity.NAME_MAX_LENGTH);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 作業分類名(L2)の文字数が最大文字数を超えている → NG
		entity = createWorkCategoryEntity();
		entity.nameL2 = createString(WorkCategoryEntity.NAME_MAX_LENGTH + 1);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(
				new List<String>{MESSAGE.Admin_Lbl_Name, WorkCategoryEntity.NAME_MAX_LENGTH.format()}),
				result.getErrors().get(0).message);
		// 最大文字数 → OK
		entity.nameL2 = createString(WorkCategoryEntity.NAME_MAX_LENGTH);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());

		// 表示順が0以下である → NG
		entity = createWorkCategoryEntity();
		entity.order = 0;
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_InvalidValueMoreEq(
				new List<String>{MESSAGE.Admin_Lbl_Order, WorkCategoryEntity.ORDER_MIN_VALUE.format()}),
				result.getErrors().get(0).message);
		// 表示順が1である → OK
		entity.order = 1;
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());
		// 表示順が上限値である → OK
		entity.order = AttLeaveEntity.ORDER_MAX_VALUE;
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(true, result.isSuccess());
		// 表示順が上限値より大きい値である → NG
		entity.order = WorkCategoryEntity.ORDER_MAX_VALUE + 1;
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
		System.assertEquals(MESSAGE.Com_Err_InvalidValueTooLarge(new List<String>{MESSAGE.Admin_Lbl_Order}),
				result.getErrors().get(0).message);

		// 有効期間が不正 → NG
		entity = createWorkCategoryEntity();
		entity.validTo = ValidPeriodEntity.VALID_TO_MAX.addDays(1);
		result = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		System.assertEquals(false, result.isSuccess());
	}

	/**
	 * 最大文字数を超えているエラーが正しいかどうかを検証する
	 * @param error 検証対象
	 * @param field 対象項目名
	 * @param maxLength 最大文字数
	 */
	private static void assertErrorMaxLengthOver(Validator.Error error, String field, Integer maxLength) {
		System.assertEquals(App.ERR_CODE_INVALID_VALUE, error.code);
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(
				new List<String>{field, maxLength.format()}), error.message);
	}

	/**
	 * 値が範囲外の場合のエラーが正しいかどうかを検証する
	 * @param error 検証対象
	 * @param objName オブジェクト名
	 * @param field 対象項目名
	 * @param minValue 最小値
	 * @param maxValue 最大値
	 */
	private static void assertErrorValueRange(Validator.Error error,
			String fieldName, Integer minValue, Integer maxValue) {
		System.assertEquals(App.ERR_CODE_INVALID_VALUE, error.code);

		String message = ComMessage.msg().Com_Err_InvalidValueRange(
				new List<String>{fieldName, minValue.format(), maxValue.format()});
		System.assertEquals(message, error.message);
	}

	/**
	 * 名前項目のラベルを作成する
	 * @return 設定名(langLabel)
	 */
	private static String createNameLangLabal(String langLabel) {
		return MESSAGE.Admin_Lbl_Name + '(' + langLabel + ')';
	}

}
