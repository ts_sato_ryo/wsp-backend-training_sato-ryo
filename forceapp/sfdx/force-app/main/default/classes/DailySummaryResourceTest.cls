/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group プランナー
*
* @description DailySummaryResourceのテストクラス
*/
@isTest
private class DailySummaryResourceTest {

	/**
	 * デイリーサマリーデータ取得（GetApi）のリクエストパラメータのテスト
	 * 不正な値を入力した時、エラーが発生することを確認する
	 */
	@isTest
	static void getParamValidateTest() {
		// 社員IDが不正
		{
			// テストデータ作成
			DailySummaryResource.GetParam param = new DailySummaryResource.GetParam();
			param.empId = 'invalid ID';
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ invalid empId!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'empId', 'invalid ID'}),
					actEx.getMessage());
		}
		// 日付が空
		{
			// テストデータ作成
			DailySummaryResource.GetParam param = new DailySummaryResource.GetParam();
			param.empId = UserInfo.getUserId();// ダミー
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ not nullable targetDate!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'targetDate'}),
					actEx.getMessage());
		}
		// 日付が不正
		{
			// テストデータ作成
			DailySummaryResource.GetParam param = new DailySummaryResource.GetParam();
			param.empId = UserInfo.getUserId();// ダミー
			param.targetDate = 'invalid Date';
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ invalid targetDate!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'targetDate', 'invalid Date'}),
					actEx.getMessage());
		}
	}

	/**
	 * @description デイリーサマリーデータ取得処理のテスト
	 */
	@isTest
	static void getApiTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id, permission.Id,
				new AppDateRange(AppDate.newInstance(2016,1,1), AppDate.newInstance(2100, 12, 31)));
		ComJobType__c jobType = ComTestDataUtility.createJobType('Test', company.Id);
		TimeSettingBase__c timeSetting = ComTestDataUtility.createTimeSettingWithHistory('TimeSetting', company.Id);
		TimeSettingHistory__c timeSettingHistory = [SELECT id FROM TimeSettingHistory__c WHERE BaseId__c = :timeSetting.Id LIMIT 1];

		// 作業分類を作成
		List<TimeWorkCategory__c> workCategoryList = new List<TimeWorkCategory__c>();
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類1-1',
			Name_L0__c='作業分類1-1',
			Name_L1__c='作業分類1-1',
			Name_L2__c='作業分類1-1',
			Code__c='01-001',
			UniqKey__c='01-001',
			CompanyId__c = emp.CompanyId__c,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			Order__c = 1
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類1-2',
			Name_L0__c='作業分類1-2',
			Name_L1__c='作業分類1-2',
			Name_L2__c='作業分類1-2',
			Code__c='01-002',
			UniqKey__c='01-002',
			CompanyId__c = emp.CompanyId__c,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			Order__c = 2
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類1-3',
			Name_L0__c='作業分類1-3',
			Name_L1__c='作業分類1-3',
			Name_L2__c='作業分類1-3',
			Code__c='01-003',
			UniqKey__c='01-003',
			CompanyId__c = emp.CompanyId__c,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2016, 12, 31),
			Order__c = 3
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類2-1',
			Name_L0__c='作業分類2-1',
			Name_L1__c='作業分類2-1',
			Name_L2__c='作業分類2-1',
			Code__c='02-001',
			UniqKey__c='02-001',
			CompanyId__c = emp.CompanyId__c,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			Order__c = 4
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類2-2',
			Name_L0__c='作業分類2-2',
			Name_L1__c='作業分類2-2',
			Name_L2__c='作業分類2-2',
			Code__c='02-002',
			UniqKey__c='02-002',
			CompanyId__c = emp.CompanyId__c,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			Order__c = 5
		));
		insert workCategoryList;

		// ジョブタイプ別作業分類
		List<TimeJobTypeWorkCategory__c> jobTypeWorkCategories = new List<TimeJobTypeWorkCategory__c> {
			new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategoryList[0].Id),
			new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategoryList[1].Id),
			new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategoryList[2].Id),
			new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategoryList[3].Id),
			new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategoryList[4].Id)
		};
		insert jobTypeWorkCategories;

		// ジョブを作成
		List<ComJob__c> jobList = new List<ComJob__c>();
		jobList.add(new ComJob__c(
			Name = 'ジョブ1',
			Name_L0__c = 'ジョブ1',
			Name_L1__c = 'ジョブ1',
			Name_L2__c = 'ジョブ1',
			Code__c = '0001',
			UniqKey__c = '0001',
			CompanyId__c = emp.CompanyId__c,
			JobTypeId__c = jobType.Id,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			DirectCharged__c = true
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ2',
			Name_L0__c = 'ジョブ2',
			Name_L1__c = 'ジョブ2',
			Name_L2__c = 'ジョブ2',
			Code__c = '0002',
			UniqKey__c = '0002',
			CompanyId__c = emp.CompanyId__c,
			JobTypeId__c = jobType.Id,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			DirectCharged__c = true
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ3',
			Name_L0__c = 'ジョブ3',
			Name_L1__c = 'ジョブ3',
			Name_L2__c = 'ジョブ3',
			Code__c = '0003',
			UniqKey__c = '0003',
			CompanyId__c = emp.CompanyId__c,
			JobTypeId__c = jobType.Id,
			DirectCharged__c = true,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2016, 12, 31)
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ4',
			Name_L0__c = 'ジョブ4',
			Name_L1__c = 'ジョブ4',
			Name_L2__c = 'ジョブ4',
			Code__c = '0004',
			UniqKey__c = '0004',
			CompanyId__c = emp.CompanyId__c,
			JobTypeId__c = jobType.Id,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			DirectCharged__c = true
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ5',
			Name_L0__c = 'ジョブ5',
			Name_L1__c = 'ジョブ5',
			Name_L2__c = 'ジョブ5',
			Code__c = '0005',
			UniqKey__c = '0005',
			CompanyId__c = emp.CompanyId__c,
			JobTypeId__c = jobType.Id,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31),
			DirectCharged__c = true
		));
		insert jobList;

		// 予定を作成
		PlanEvent__c planEvent1 = new PlanEvent__c(
			CreatedServiceBy__c = 'salesforce',
			OwnerId = user.Id,
			Subject__c = 'User1の予定1',
			StartDateTime__c = Datetime.newInstance(2017, 6, 13, 0, 0, 0),
			EndDateTime__c = Datetime.newInstance(2017, 6, 13, 1, 0, 0),
			IsAllDay__c = false,
			Location__c = '東京',
			Description__c = 'こんにちは',
			IsOuting__c = true,
			ResponseStatus__c = null,
			JobId__c = jobList[4].Id,
			WorkCategoryId__c = workCategoryList[4].Id,
			ContactId__c = null
		);
		PlanEvent__c planEvent2 = new PlanEvent__c(
			CreatedServiceBy__c = 'salesforce',
			OwnerId = user.Id,
			Subject__c = 'User1の予定2',
			StartDateTime__c = Datetime.newInstance(2017, 6, 13, 23, 0, 0),
			EndDateTime__c = Datetime.newInstance(2017, 6, 13, 23, 59, 0),
			IsAllDay__c = false,
			Location__c = '東京',
			Description__c = 'こんにちは',
			IsOuting__c = true,
			ResponseStatus__c = null,
			JobId__c = jobList[4].Id,
			WorkCategoryId__c = workCategoryList[4].Id,
			ContactId__c = null
		);
		insert new List<PlanEvent__c>{planEvent1, planEvent2};

		// 個人設定の表示タスク情報を作成
		List<PersonalSettingEntity.Task> taskList = new List<PersonalSettingEntity.Task>();
		// 1件目：ジョブ2 - 作業分類2-1
		PersonalSettingEntity.Task task1 = new PersonalSettingEntity.Task();
		task1.jobCode = jobList[1].Code__c;
		task1.workCategoryCode = workCategoryList[3].Code__c;
		taskList.add(task1);
		// 2件目：ジョブ3 - 作業分類なし（有効期限切れ）
		PersonalSettingEntity.Task task2 = new PersonalSettingEntity.Task();
		task2.jobCode = jobList[2].Code__c;
		task2.workCategoryCode = null;
		taskList.add(task2);
		// 3件目：ジョブ1 - 作業分類なし
		PersonalSettingEntity.Task task3 = new PersonalSettingEntity.Task();
		task3.jobCode = jobList[0].Code__c;
		task3.workCategoryCode = null;
		taskList.add(task3);
		PersonalSettingRepository repo = new PersonalSettingRepository();
		PersonalSettingEntity setting = new PersonalSettingEntity();
		setting.employeeBaseId = emp.Id;
		setting.timeInputSetting.taskList = taskList;
		repo.saveEntity(setting);

		// 工数サマリーを作成
		TimeSummary__c timeSummary = new TimeSummary__c(
			UniqKey__c = emp.Code__c + '201706' + '1',
			EmployeeHistoryId__c = emp.CurrentHistoryId__c,
			TimeSettingHistoryId__c = timeSettingHistory.Id,
			SummaryName__c = '201706',
			SubNo__c = 1,
			StartDate__c = Date.newInstance(2017, 6, 1),
			EndDate__c = Date.newInstance(2017, 6, 30)
		);
		insert timeSummary;

		// 工数明細を作成
		TimeRecord__c timeRecord = new TimeRecord__c(
			UniqKey__c = timeSummary.Id + '20170613',
			TimeSummaryId__c = timeSummary.Id,
			Date__c = Date.newInstance(2017, 6, 13),
			Note__c = '作業報告',
			Output__c = 'アウトプット'
		);
		insert timeRecord;

		// 工数内訳を作成
		List<TimeRecordItem__c> recordItemList = new List<TimeRecordItem__c>();
		recordItemList.add(new TimeRecordItem__c(
			TimeRecordId__c = timeRecord.Id,
			JobId__c = jobList[1].Id,
			WorkCategoryId__c = workCategoryList[3].Id,
			Order__c = 1,
			TimeDirect__c = true,
			Time__c = 120
		));
		recordItemList.add(new TimeRecordItem__c(
			TimeRecordId__c = timeRecord.Id,
			JobId__c = jobList[3].Id,
			WorkCategoryId__c = null,
			Order__c = 2,
			TimeDirect__c = true,
			Time__c = 240
		));
		recordItemList.add(new TimeRecordItem__c(
			TimeRecordId__c = timeRecord.Id,
			JobId__c = jobList[4].Id,
			WorkCategoryId__c = null,
			Order__c = 3,
			TimeDirect__c = false,
			Time__c = 60,
			Volume__c = 120,
			Ratio__c = 50
		));
		recordItemList.add(new TimeRecordItem__c(
			TimeRecordId__c = timeRecord.Id,
			JobId__c = jobList[4].Id,
			WorkCategoryId__c = workCategoryList[4].Id,
			Order__c = 4,
			TimeDirect__c = false,
			Time__c = 60,
			Volume__c = 120,
			Ratio__c = 50
		));

		insert recordItemList;

		Test.startTest();

		DailySummaryResource.GetParam param = new DailySummaryResource.GetParam();
		param.empId = emp.Id;
		param.targetDate = '2017-06-13';

		DailySummaryResource.GetResult res;
		System.runAs(user) {
			DailySummaryResource.GetApi api = new DailySummaryResource.GetApi();
			res = (DailySummaryResource.GetResult)api.execute(param);
		}

		Test.stopTest();

		// タスク件数：5件
		System.assertEquals(5, res.taskList.size());
		// 1件目：ジョブ2 - 作業分類2-1
		System.assertEquals(jobList[1].Id, res.taskList[0].jobId);
		System.assertEquals(jobList[1].Code__c, res.taskList[0].jobCode);
		System.assertEquals(jobList[1].Name_L0__c, res.taskList[0].jobName);
		System.assertEquals(jobList[1].DirectCharged__c, res.taskList[0].isDirectCharged);
		System.assertEquals(workCategoryList[3].Id, res.taskList[0].workCategoryId);
		System.assertEquals(workCategoryList[3].Code__c, res.taskList[0].workCategoryCode);
		System.assertEquals(workCategoryList[3].Name_L0__c, res.taskList[0].workCategoryName);
		System.assertEquals(recordItemList[0].TimeDirect__c, res.taskList[0].isDirectInput);
		System.assertEquals(recordItemList[0].Time__c, res.taskList[0].taskTime);
		System.assertEquals(4, res.taskList[0].workCategoryList.size());
		// 2件目：ジョブ4 - 作業分類なし
		System.assertEquals(jobList[3].Id, res.taskList[1].jobId);
		System.assertEquals(jobList[3].Code__c, res.taskList[1].jobCode);
		System.assertEquals(jobList[3].Name_L0__c, res.taskList[1].jobName);
		System.assertEquals(jobList[3].DirectCharged__c, res.taskList[1].isDirectCharged);
		System.assertEquals(null, res.taskList[1].workCategoryId);
		System.assertEquals(null, res.taskList[1].workCategoryCode);
		System.assertEquals(null, res.taskList[1].workCategoryName);
		System.assertEquals(recordItemList[1].TimeDirect__c, res.taskList[1].isDirectInput);
		System.assertEquals(recordItemList[1].Time__c, res.taskList[1].taskTime);
		System.assertEquals(4, res.taskList[1].workCategoryList.size());
		// 3件目：ジョブ1 - 作業分類なし
		System.assertEquals(jobList[4].Id, res.taskList[2].jobId);
		System.assertEquals(jobList[4].Code__c, res.taskList[2].jobCode);
		System.assertEquals(jobList[4].Name_L0__c, res.taskList[2].jobName);
		System.assertEquals(jobList[4].DirectCharged__c, res.taskList[2].isDirectCharged);
		System.assertEquals(null, res.taskList[2].workCategoryId);
		System.assertEquals(null, res.taskList[2].workCategoryCode);
		System.assertEquals(null, res.taskList[2].workCategoryName);
		System.assertEquals(recordItemList[2].TimeDirect__c, res.taskList[2].isDirectInput);
		System.assertEquals(recordItemList[2].Time__c, res.taskList[2].taskTime);
		System.assertEquals(recordItemList[2].Volume__c, res.taskList[2].volume);
		System.assertEquals(recordItemList[2].Ratio__c, res.taskList[2].ratio);
		System.assertEquals(4, res.taskList[3].workCategoryList.size());
		// 4件目：ジョブ1 - 作業分類1-1
		System.assertEquals(jobList[4].Id, res.taskList[3].jobId);
		System.assertEquals(jobList[4].Code__c, res.taskList[3].jobCode);
		System.assertEquals(jobList[4].Name_L0__c, res.taskList[3].jobName);
		System.assertEquals(jobList[4].DirectCharged__c, res.taskList[3].isDirectCharged);
		System.assertEquals(workCategoryList[4].Id, res.taskList[3].workCategoryId);
		System.assertEquals(workCategoryList[4].Code__c, res.taskList[3].workCategoryCode);
		System.assertEquals(workCategoryList[4].Name_L0__c, res.taskList[3].workCategoryName);
		System.assertEquals(recordItemList[3].Time__c, res.taskList[3].taskTime);
		System.assertEquals(recordItemList[3].Volume__c, res.taskList[3].volume);
		System.assertEquals(recordItemList[3].Ratio__c, res.taskList[3].ratio);
		System.assertEquals(4, res.taskList[3].workCategoryList.size());
		System.assertEquals(2, res.taskList[3].eventList.size());
		// 予定データ
		System.assertEquals(planEvent1.id, res.taskList[3].eventList[0].id);
		System.assertEquals(planEvent1.Subject__c, res.taskList[3].eventList[0].subject);
		System.assertEquals(DateUtil.dateTimeToISO8601(planEvent1.StartDateTime__c), res.taskList[3].eventList[0].startDateTime);
		System.assertEquals(DateUtil.dateTimeToISO8601(planEvent1.EndDateTime__c), res.taskList[3].eventList[0].endDateTime);
		System.assertEquals(planEvent2.id, res.taskList[3].eventList[1].id);
		System.assertEquals(planEvent2.Subject__c, res.taskList[3].eventList[1].subject);
		System.assertEquals(DateUtil.dateTimeToISO8601(planEvent2.StartDateTime__c), res.taskList[3].eventList[1].startDateTime);
		System.assertEquals(DateUtil.dateTimeToISO8601(planEvent2.EndDateTime__c), res.taskList[3].eventList[1].endDateTime);

		// 5件目（個人設定）：ジョブ1 - 作業分類なし
		System.assertEquals(jobList[0].Id, res.taskList[4].jobId);
		System.assertEquals(jobList[0].Code__c, res.taskList[4].jobCode);
		System.assertEquals(jobList[0].Name_L0__c, res.taskList[4].jobName);
		System.assertEquals(jobList[0].DirectCharged__c, res.taskList[4].isDirectCharged);
		System.assertEquals(null, res.taskList[4].workCategoryId);
		System.assertEquals(null, res.taskList[4].workCategoryCode);
		System.assertEquals(null, res.taskList[4].workCategoryName);
		System.assertEquals(0, res.taskList[4].taskTime);
		System.assertEquals(null, res.taskList[4].volume);
		System.assertEquals(null, res.taskList[4].ratio);
		System.assertEquals(4, res.taskList[4].workCategoryList.size());
		System.assertEquals(0, res.taskList[4].eventList.size());

		// 申請ステータス、作業報告、アウトプット
		System.assertEquals('NotRequested', res.status);
		System.assertEquals(timeRecord.Note__c, res.note);
		System.assertEquals(timeRecord.Output__c, res.output);
	}

	/**
	 * GetApi のテスト
	 * 工数実績表示(代理)権限を持っていない管理者が勤怠を表示できないことを確認する
	 */
	@isTest
	static void getApiTestNoPermission() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id, permission.Id);

		// 該当権限を削除
		ComPermission__c noDelegate = ComTestDataUtility.createPermission('noDelegate', company.Id);
		noDelegate.ViewTimeTrackByDelegate__c = false;
		update noDelegate;
		User mngUser = ComTestDataUtility.createStandardUser('manager');
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, mngUser.Id, noDelegate.Id);

		// リクエストパラメータ
		DailySummaryResource.GetParam param = new DailySummaryResource.GetParam();
		param.empId = emp.Id;
		param.targetDate = '2017-06-13';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(mngUser) {
			try {
				Test.startTest();
				DailySummaryResource.GetApi api = new DailySummaryResource.GetApi();
				DailySummaryResource.GetResult res = (DailySummaryResource.GetResult)api.execute(param);
				Test.stopTest();
			} catch (App.NoPermissionException e) {
				actEx = e;
			}

			// 検証
			System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
			String expectMessage = ComMessage.msg().Com_Err_NoApiPermission;
			System.assertEquals(expectMessage, actEx.getMessage());
		}
	}

	/**
	 * デイリーサマリーデータ保存（SaveApi）のリクエストパラメータのテスト
	 * 不正な値を入力した時、エラーが発生することを確認する
	 */
	@isTest
	static void saveParamValidateTest() {
		// 社員IDが不正
		{
			// テストデータ作成
			DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
			param.empId = 'invalid ID';
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ invalid empId!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'empId', 'invalid ID'}),
					actEx.getMessage());
		}
		// 日付が空
		{
			// テストデータ作成
			DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
			param.empId = UserInfo.getUserId();// ダミー
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ not nullable targetDate!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'targetDate'}),
					actEx.getMessage());
		}
		// 日付が不正
		{
			// テストデータ作成
			DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
			param.empId = UserInfo.getUserId();// ダミー
			param.targetDate = 'invalid Date';
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ invalid targetDate!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'targetDate', 'invalid Date'}),
					actEx.getMessage());
		}
		// タスクのジョブIDが空
		{
			// テストデータ作成
			DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
			param.empId = UserInfo.getUserId();// ダミー
			param.targetDate = '2019-05-01';
			DailySummaryResource.Task task = new DailySummaryResource.Task();
			param.taskList = new List<DailySummaryResource.Task>{task};
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ not nullable jobId!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'jobId'}),
					actEx.getMessage());
		}
		// タスクのジョブIDが不正
		{
			// テストデータ作成
			DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
			param.empId = UserInfo.getUserId();// ダミー
			param.targetDate = '2019-05-12';
			DailySummaryResource.Task task = new DailySummaryResource.Task();
			task.jobId = 'invalid ID';
			param.taskList = new List<DailySummaryResource.Task>{task};
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ invalid jobId!!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'jobId', 'invalid ID'}),
					actEx.getMessage());
		}
		// タスクの入力方法が不正
		{
			// テストデータ作成
			DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
			param.empId = UserInfo.getUserId();// ダミー
			param.targetDate = '2019-05-12';
			DailySummaryResource.Task task = new DailySummaryResource.Task();
			task.jobId = UserInfo.getUserId();// ダミー
			task.isDirectInput = true;
			DailySummaryResource.Task task2 = new DailySummaryResource.Task();
			task2.jobId = UserInfo.getUserId();// ダミー
			param.taskList = new List<DailySummaryResource.Task>{task, task2};
			// 実行
			App.ParameterException actEx;
			try {
				param.validate();
			} catch (App.ParameterException e) {
				actEx = e;
			} catch (Exception ex) {
				TestUtil.fail('!!!Unexpected Error occured!!!');
			}
			// 検証
			System.assertNotEquals(null, actEx, '!!!Error Not Occured @ not nullable isDirectInput !!!');
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'isDirectInput'}),
					actEx.getMessage());
		}
	}

	/**
	 * @description デイリーサマリーデータ保存処理のテスト
	 */
	@isTest
	static void saveApiTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity emp = testData.employee;
		ComJobType__c jobType = ComTestDataUtility.createJobType('Test', testData.company.id);

		// 工数設定（ベース）を作成
		TimeSettingBase__c settingBase = new TimeSettingBase__c(
			Code__c = '0001',
			UniqKey__c = testData.company.Code__c + '-0001',
			CompanyId__c = emp.companyId,
			SummaryPeriod__c = 'Month',
			StartDateOfMonth__c = 16,
			MonthMark__c = 'BeginBase'
		);
		insert settingBase;

		// 工数設定（履歴）を作成
		TimeSettingHistory__c settingHistory = new TimeSettingHistory__c(
			BaseId__c = settingBase.Id,
			Name_L0__c= '工数設定',
			UniqKey__c = '00012017010120171231',
			ValidFrom__c = Date.newInstance(2017, 1, 1),
			ValidTo__c = Date.newInstance(2017, 12, 31),
			Removed__c = false
		);
		insert settingHistory;

		// 工数設定を社員に紐づけ
		EmployeeRepository empRepo = new EmployeeRepository();
		emp.getHistory(0).timeSettingId = settingBase.Id;
		empRepo.saveHistoryEntity(emp.getHistory(0));

		// 作業分類を作成
		List<TimeWorkCategory__c> workCategoryList = new List<TimeWorkCategory__c>();
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類1-1',
			Name_L0__c='作業分類1-1',
			Name_L1__c='作業分類1-1',
			Name_L2__c='作業分類1-1',
			Code__c='01-001',
			UniqKey__c='01-001',
			CompanyId__c = emp.companyId,
			Order__c = 1,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31)
		));
		workCategoryList.add(new TimeWorkCategory__c(
			Name = '作業分類2-1',
			Name_L0__c='作業分類2-1',
			Name_L1__c='作業分類2-1',
			Name_L2__c='作業分類2-1',
			Code__c='02-001',
			UniqKey__c='02-001',
			CompanyId__c = emp.companyId,
			Order__c = 2,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31)
		));
		insert workCategoryList;
		// ジョブタイプ別作業分類
		List<TimeJobTypeWorkCategory__c> jobTypeWorkCategories = new List<TimeJobTypeWorkCategory__c> {
			new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategoryList[0].Id),
			new TimeJobTypeWorkCategory__c(JobTypeId__c = jobType.Id, WorkCategoryId__c = workCategoryList[1].Id)
		};
		insert jobTypeWorkCategories;

		// ジョブを作成
		List<ComJob__c> jobList = new List<ComJob__c>();
		jobList.add(new ComJob__c(
			Name = 'ジョブ1',
			Name_L0__c = 'ジョブ1',
			Name_L1__c = 'ジョブ1',
			Name_L2__c = 'ジョブ1',
			Code__c = '0001',
			UniqKey__c = '0001',
			CompanyId__c = emp.companyId,
			JobTypeId__c = jobType.Id,
			DirectCharged__c = true,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31)
		));
		jobList.add(new ComJob__c(
			Name = 'ジョブ2',
			Name_L0__c = 'ジョブ2',
			Name_L1__c = 'ジョブ2',
			Name_L2__c = 'ジョブ2',
			Code__c = '0002',
			UniqKey__c = '0002',
			CompanyId__c = emp.companyId,
			JobTypeId__c = jobType.Id,
			DirectCharged__c = true,
			ValidFrom__c = Date.newInstance(2016, 1, 1),
			ValidTo__c = Date.newInstance(2100, 12, 31)
		));
		insert jobList;

		// 勤怠データ作成
		AttAttendanceService attService = new AttAttendanceService();
		AttAttendanceService.SaveDailyAttTimeParam attParam = new AttAttendanceService.SaveDailyAttTimeParam();
		attParam.targetDate = AppDate.newInstance(2017, 6, 13);
		attParam.startTime = AttTime.newInstance(9,00);
		attParam.endTime = AttTime.newInstance(18,00);
		attParam.rest1StartTime = AttTime.newInstance(12,00);
		attParam.rest1EndTime = AttTime.newInstance(13,00);
		attService.saveDailyAttTimeParam(emp.Id, attParam);

		Test.startTest();

		DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
		param.empId = emp.Id;
		param.targetDate = '2017-06-13';
		param.note = '作業報告';
		param.output = 'アウトプット';

		List<DailySummaryResource.Task> taskList = new List<DailySummaryResource.Task>();
		DailySummaryResource.Task task1 = new DailySummaryResource.Task();
		task1.jobId = jobList[0].Id;
		task1.workCategoryId = null;
		task1.isDirectInput = true;
		task1.taskTime = 120;
		taskList.add(task1);
		DailySummaryResource.Task task2 = new DailySummaryResource.Task();
		task2.jobId = jobList[0].Id;
		task2.workCategoryId = workCategoryList[0].Id;
		task2.isDirectInput = true;
		task2.taskTime = 240;
		taskList.add(task2);
		DailySummaryResource.Task task3 = new DailySummaryResource.Task();
		task3.jobId = jobList[1].Id;
		task3.workCategoryId = null;
		task3.isDirectInput = false;
		task3.taskTime = 0;
		task3.volume = 120;
		task3.ratio = 40;
		taskList.add(task3);
		DailySummaryResource.Task task4 = new DailySummaryResource.Task();
		task4.jobId = jobList[1].Id;
		task4.workCategoryId = workCategoryList[0].Id;
		task4.isDirectInput = false;
		task4.taskTime = 0;
		task4.volume = 120;
		task4.ratio = 60;
		taskList.add(task4);
		// 時間入力-実績なし
		DailySummaryResource.Task task5 = new DailySummaryResource.Task();
		task5.jobId = jobList[0].Id;
		task5.workCategoryId = workCategoryList[1].Id;
		task5.isDirectInput = true;
		task5.taskTime = 0;
		taskList.add(task5);
		// 割合入力-実績なし
		DailySummaryResource.Task task6 = new DailySummaryResource.Task();
		task6.jobId = jobList[1].Id;
		task6.workCategoryId = workCategoryList[1].Id;
		task6.isDirectInput = false;
		task6.ratio = 0;
		taskList.add(task6);
		param.taskList = taskList;

		DailySummaryResource.SaveApi api = new DailySummaryResource.SaveApi();
		api.execute(param);

		Test.stopTest();

		Date targetDate = Date.valueOf(param.targetDate);

		// 作成された工数サマリーデータを取得
		TimeSummary__c timeSummary = [
				SELECT
					Id,
					Name,
					UniqKey__c,
					EmployeeHistoryId__c,
					SummaryName__c,
					SubNo__c,
					StartDate__c,
					EndDate__c
				FROM TimeSummary__c
				WHERE EmployeeHistoryId__r.BaseId__c = :emp.Id
				AND StartDate__c <= :targetDate
				AND EndDate__c >= :targetDate
				LIMIT 1];

		// 工数サマリー
		System.assertEquals(emp.code + emp.fullNameL.getFullName() + '20170516-20170615', timeSummary.Name);
		// System.assertEquals(emp.code + emp.name + '20170516-20170615', timeSummary.Name);
		System.assertEquals(emp.id + '2017051', timeSummary.UniqKey__c);
		System.assertEquals(emp.getHistory(0).id, timeSummary.EmployeeHistoryId__c);
		System.assertEquals('201705', timeSummary.SummaryName__c);
		System.assertEquals(1, timeSummary.SubNo__c);
		System.assertEquals(Date.newInstance(2017, 5, 16), timeSummary.StartDate__c);
		System.assertEquals(Date.newInstance(2017, 6, 15), timeSummary.EndDate__c);

		/*// 作成された勤怠明細データを取得
		Map<Date, Id> recordIdMapByDate = new Map<Date, Id>();
		AttAttendanceService attService = new AttAttendanceService();
		List<AttSummaryEntity> summaryList = attService.getSummaryList(emp.Id, AppDate.valueOf(timeSummary.StartDate__c), AppDate.valueOf(timeSummary.EndDate__c));
		for (AttSummaryEntity summary : summaryList) {
			for (AttRecordEntity record : summary.attRecordList) {
				recordIdMapByDate.put(record.rcdDate.getDate(), record.id);
			}
		}*/

		// 作成された工数明細データを取得
		List<TimeRecord__c> timeRecordList = [
			SELECT
				Id,
				Name,
				UniqKey__c,
				TimeSummaryId__c,
				Date__c,
				Note__c,
				Output__c,
				AttRecordId__c,
				(SELECT
					Id,
					JobId__c,
					WorkCategoryId__c,
					Order__c,
					TimeDirect__c,
					Time__c,
					Volume__c,
					Ratio__c
				FROM TimeRecordItems__r
				ORDER BY Order__c)
			FROM TimeRecord__c
			WHERE TimeSummaryId__r.EmployeeHistoryId__r.BaseId__c = :emp.Id];

		// 工数明細と勤怠明細の紐づけ
		TimeRecord__c timeRecord;
		for (TimeRecord__c record : timeRecordList) {
			// System.assertEquals(record.AttRecordId__c, recordIdMapByDate.get(record.Date__c));

			if (record.Date__c == targetDate) {
				timeRecord = record;
			}
		}

		// 工数明細
		System.assertEquals(emp.code + emp.fullNameL.getFullName() + '20170613', timeRecord.Name);
		// System.assertEquals(emp.code + emp.name + '20170613', timeRecord.Name);
		System.assertEquals(timeSummary.Id + '20170613', timeRecord.UniqKey__c);
		System.assertEquals(timeSummary.Id, timeRecord.TimeSummaryId__c);
		System.assertEquals(targetDate, timeRecord.Date__c);
		System.assertEquals(param.note, timeRecord.Note__c);
		System.assertEquals(param.output, timeRecord.Output__c);

		// 工数内訳：1件目
		System.assertEquals(4, timeRecord.TimeRecordItems__r.size());
		TimeRecordItem__c recordItem1 = timeRecord.TimeRecordItems__r[0];
		System.assertEquals(task1.jobId, recordItem1.JobId__c);
		System.assertEquals(task1.workCategoryId, recordItem1.WorkCategoryId__c);
		System.assertEquals(task1.isDirectInput, recordItem1.TimeDirect__c);
		System.assertEquals(task1.taskTime, recordItem1.Time__c);
		System.assertEquals(1, recordItem1.Order__c);
		// 工数内訳：2件目
		TimeRecordItem__c recordItem2 = timeRecord.TimeRecordItems__r[1];
		System.assertEquals(task2.jobId, recordItem2.JobId__c);
		System.assertEquals(task2.workCategoryId, recordItem2.WorkCategoryId__c);
		System.assertEquals(task2.isDirectInput, recordItem2.TimeDirect__c);
		System.assertEquals(task2.taskTime, recordItem2.Time__c);
		System.assertEquals(2, recordItem2.Order__c);
		// 工数内訳：3件目
		TimeRecordItem__c recordItem3 = timeRecord.TimeRecordItems__r[2];
		System.assertEquals(task3.jobId, recordItem3.JobId__c);
		System.assertEquals(task3.workCategoryId, recordItem3.WorkCategoryId__c);
		System.assertEquals(task3.isDirectInput, recordItem3.TimeDirect__c);
		System.assertEquals(48, recordItem3.Time__c);
		System.assertEquals(task3.volume, recordItem3.Volume__c);
		System.assertEquals(task3.ratio, recordItem3.Ratio__c);
		System.assertEquals(3, recordItem3.Order__c);
		// 工数内訳：4件目
		TimeRecordItem__c recordItem4 = timeRecord.TimeRecordItems__r[3];
		System.assertEquals(task4.jobId, recordItem4.JobId__c);
		System.assertEquals(task4.workCategoryId, recordItem4.WorkCategoryId__c);
		System.assertEquals(task4.isDirectInput, recordItem4.TimeDirect__c);
		System.assertEquals(72, recordItem4.Time__c);
		System.assertEquals(task4.volume, recordItem4.Volume__c);
		System.assertEquals(task4.ratio, recordItem4.Ratio__c);
		System.assertEquals(4, recordItem4.Order__c);

		// 個人設定の工数実績入力設定を取得
		ComPersonalSetting__c personalSetting = [
				SELECT
					Id,
					TimeInputSetting__c
				FROM ComPersonalSetting__c
				WHERE EmployeeBaseId__c = :emp.Id];

		Map<String, Object> timeInputSetting = (Map<String, Object>)JSON.deserializeUntyped(personalSetting.TimeInputSetting__c);
		List<Object> jsonTaskList = (List<Object>)timeInputSetting.get('taskList');

		System.assertEquals(6, jsonTaskList.size());
		// 1件目：ジョブ1 - 作業分類なし
		Map<String, Object> jsonTask1 = (Map<String, Object>)jsonTaskList[0];
		System.assertEquals(jobList[0].Code__c, jsonTask1.get('jobCode'));
		System.assertEquals(null, jsonTask1.get('workCategoryCode'));
		// 2件目：ジョブ1 - 作業分類1-1
		Map<String, Object> jsonTask2 = (Map<String, Object>)jsonTaskList[1];
		System.assertEquals(jobList[0].Code__c, jsonTask2.get('jobCode'));
		System.assertEquals(workCategoryList[0].Code__c, jsonTask2.get('workCategoryCode'));
		// 3件目：ジョブ2 - 作業分類なし
		Map<String, Object> jsonTask3 = (Map<String, Object>)jsonTaskList[2];
		System.assertEquals(jobList[1].Code__c, jsonTask3.get('jobCode'));
		System.assertEquals(null, jsonTask3.get('workCategoryCode'));
		// 4件目：ジョブ2 - 作業分類1-1
		Map<String, Object> jsonTask4 = (Map<String, Object>)jsonTaskList[3];
		System.assertEquals(jobList[1].Code__c, jsonTask4.get('jobCode'));
		System.assertEquals(workCategoryList[0].Code__c, jsonTask4.get('workCategoryCode'));
		// 5件目：ジョブ1 - 作業分類2-1
		Map<String, Object> jsonTask5 = (Map<String, Object>)jsonTaskList[4];
		System.assertEquals(jobList[0].Code__c, jsonTask5.get('jobCode'));
		System.assertEquals(workCategoryList[1].Code__c, jsonTask5.get('workCategoryCode'));
		// 6件目：ジョブ2 - 作業分類2-1
		Map<String, Object> jsonTask6 = (Map<String, Object>)jsonTaskList[5];
		System.assertEquals(jobList[1].Code__c, jsonTask6.get('jobCode'));
		System.assertEquals(workCategoryList[1].Code__c, jsonTask6.get('workCategoryCode'));
	}

	/**
	 * @description デイリーサマリーデータ保存処理のテスト
	 * 社員が保存対象日に有効な工数設定を持っていない場合にエラーが発生することを確認する
	 */
	@isTest
	static void saveApiTestTimeSettingInvalid() {
		// テストデータ作成
		TimeTestData testData = new TimeTestData(AppDate.newInstance(2017, 7, 1));

		// パラメータ作成
		DailySummaryResource.SaveParam param = new DailySummaryResource.SaveParam();
		param.empId = testData.emp.Id;
		param.targetDate = '2017-06-13';
		param.note = '作業報告';
		param.output = 'アウトプット';

		List<DailySummaryResource.Task> taskList = new List<DailySummaryResource.Task>();
		DailySummaryResource.Task task1 = new DailySummaryResource.Task();
		task1.jobId = testData.jobList[0].Id;
		task1.workCategoryId = null;
		task1.isDirectInput = true;
		task1.taskTime = 120;
		taskList.add(task1);
		param.taskList = taskList;

		// Case -1 : 工数設定が有効でない
		// 実行
		App.IllegalStateException actEx;
		try {
			Test.startTest();
			DailySummaryResource.SaveApi api = new DailySummaryResource.SaveApi();
			api.execute(param);
			Test.stopTest();
		} catch (App.IllegalStateException e) {
			actEx = e;
		}
		// 検証
		System.assertNotEquals(null, actEx, '!!!Excepion is not occured!!!');
		System.assertEquals(App.ERR_CODE_NOT_FOUND_TIME_SETTING, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Trac_Err_NotFoundAOfB(new List<String>{ComMessage.msg().Com_Lbl_Employee, ComMessage.msg().Trac_Lbl_TimeTrackSetting}),
				 actEx.getMessage());
		actEx = null;

		// Case -2 : 有効な社員履歴に工数設定がない
		// 工数設定の紐づけを削除
		testData.empHistory.TimeSettingBaseId__c = null;
		update testData.empHistory;
		EmployeeRepository.clearCache();
		// 実行
		try {
			DailySummaryResource.SaveApi api = new DailySummaryResource.SaveApi();
			api.execute(param);
		} catch (App.IllegalStateException e) {
			actEx = e;
		}

		// 検証
		System.assertNotEquals(null, actEx, '!!!Excepion is not occured!!!');
		System.assertEquals(App.ERR_CODE_NOT_SET_TIME_SETTING, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Trac_Err_FieldNotSet(new List<String>{ComMessage.msg().Com_Lbl_Employee, ComMessage.msg().Trac_Lbl_TimeTrackSetting}),
				 actEx.getMessage());
	}
}