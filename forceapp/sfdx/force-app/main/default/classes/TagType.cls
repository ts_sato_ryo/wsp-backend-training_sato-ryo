/**
 * タグタイプ
 */
public with sharing class TagType extends ValueObjectType {

	/** ジョブ割当グループ */
	public static final TagType JOB_ASSIGN_GROUP = new TagType('JobAssignGroup').prefix('JA');
	/** 時短理由 */
	public static final TagType SHORTEN_WORK_REASON = new TagType('ShortenWorkReason').prefix('SW');

	/** タグタイプのリスト（種別が追加されら本リストにも追加してください) */
	public static final List<TagType> TYPE_LIST;
	static {
		TYPE_LIST = new List<TagType> {
			JOB_ASSIGN_GROUP,
			SHORTEN_WORK_REASON
		};
	}

	/** 接頭辞 */
	public String prefix {get; private set;}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private TagType(String value) {
		super(value);
	}

	/**
	 * 接頭辞を設定する
	 * @param prefix 接頭辞
	 */
	public TagType prefix(String prefix) {
		this.prefix = prefix;
		return this;
	}

	/**
	 * 値からTagTypeを取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つTagType、ただし該当するTagTypeが存在しない場合はnull
	 */
	public static TagType getType(String value) {
		TagType retType = null;
		if (String.isNotBlank(value)) {
			for (TagType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// TagType以外の場合はfalse
		Boolean eq;
		if (compare instanceof TagType) {
			// 値が同じであればtrue
			if (this.value == ((TagType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}