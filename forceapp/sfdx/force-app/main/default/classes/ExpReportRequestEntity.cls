/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 経費申請のエンティティ
 */
public with sharing class ExpReportRequestEntity extends Entity {

	/** コメントの最大文字数 */
	private static final Integer COMMENT_MAX_LENGTH = 1000;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		Name,
		OwnerId,
		ExpReportId,
		TotalAmount,
		ReportNo,
		Subject,
		Comment,
		ProcessComment,
		CancelComment,
		Status,
		IsConfirmationRequired,
		CancelType,
		EmployeeHistoryId,
		RequestTime,
		DepartmentHistoryId,
		ActorHistoryId,
		LastApproveTime,
		LastApproverId,
		Approver01Id,
		AccountingStatus,
		PaymentDate,
		AuthorizedTime,
		ExportedTime
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ
	 */
	public ExpReportRequestEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/** 経費申請名 */
	public String name {
		get;
		set {
			if (String.isBlank(value)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('name');
				throw ex;
			}
			name = value;
			setChanged(Field.Name);
		}
	}

	/** 所有者ID */
	public Id ownerId {
		get;
		set {
			ownerId = value;
			setChanged(Field.OwnerId);
		}
	}

	/** 経費精算ID */
	public Id expReportId {
		get;
		set {
			expReportId = value;
			setChanged(Field.ExpReportId);
		}
	}

	/** 合計金額 */
	public Decimal totalAmount {
		get;
		set {
			totalAmount = value;
			setChanged(Field.totalAmount);
		}
	}

	/** 申請番号 */
	public String reportNo {
		get;
		set {
			reportNo = value;
			setChanged(Field.ReportNo);
		}
	}

	/** 件名 */
	public String subject {
		get;
		set {
			subject = value;
			setChanged(Field.Subject);
		}
	}

	/** 申請コメント */
	public String comment {
		get;
		set {
			// 文字数チェック
			if ((value != null) && (value.length() > COMMENT_MAX_LENGTH)) {
				throw new App.IllegalStateException(
						ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{
									ComMessage.msg().Att_Lbl_Comment, String.valueOf(COMMENT_MAX_LENGTH)}));
			}
			comment = value;
			setChanged(Field.Comment);
		}
	}

	/** 承認却下コメント */
	public String processComment {
		get;
		set {
			// 文字数チェック
			if ((value != null) && (value.length() > COMMENT_MAX_LENGTH)) {
				throw new App.IllegalStateException(
						ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{
									ComMessage.msg().Att_Lbl_Comment, String.valueOf(COMMENT_MAX_LENGTH)}));
			}
			processComment = value;
			setChanged(Field.ProcessComment);
		}
	}

	/** 取消コメント */
	public String cancelComment {
		get;
		set {
			// 文字数チェック
			if ((value != null) && (value.length() > COMMENT_MAX_LENGTH)) {
				throw new App.IllegalStateException(
						ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{
									ComMessage.msg().Att_Lbl_Comment, String.valueOf(COMMENT_MAX_LENGTH)}));
			}
			cancelComment = value;
			setChanged(Field.CancelComment);
		}
	}

	/** ステータス */
	public AppRequestStatus status {
		get;
		set {
			status = value;
			setChanged(Field.Status);
		}
	}

	/** 要確認フラグ */
	public Boolean isConfirmationRequired {
		get;
		set {
			isConfirmationRequired = value;
			setChanged(Field.IsConfirmationRequired);
		}
	}

	/** 取消種別 */
	public AppCancelType cancelType {
		get;
		set {
			cancelType = value;
			setChanged(Field.CancelType);
		}
	}

	/** 社員履歴ID */
	public Id employeeHistoryId {
		get;
		set {
			employeeHistoryId = value;
			setChanged(Field.EmployeeHistoryId);
		}
	}

	/** 社員ベースID(参照専用) */
	public Id employeeBaseId {
		get;
		set;
	}

	/** 社員の情報(参照専用) */
	public AppLookup.Employee employee {
		get;
		set;
	}

	/** 申請日時 */
	public AppDatetime requestTime {
		get;
		set {
			requestTime = value;
			setChanged(Field.RequestTime);
		}
	}

	/** 部署履歴ID */
	public Id departmentHistoryId {
		get;
		set {
			departmentHistoryId = value;
			setChanged(Field.DepartmentHistoryId);
		}
	}

	/** 部署の情報(参照専用) */
	public AppLookup.Department department {
		get;
		set;
	}

	/** 申請者ID */
	public Id actorHistoryId {
		get;
		set {
			actorHistoryId = value;
			setChanged(Field.ActorHistoryId);
		}
	}

	/** 申請者の情報(参照専用) */
	public AppLookup.Employee actor {
		get;
		set;
	}

	/** 最終承認日時 */
	public AppDatetime lastApproveTime {
		get;
		set {
			lastApproveTime = value;
			setChanged(Field.LastApproveTime);
		}
	}

	/** 最終承認者ID */
	public Id lastApproverId {
		get;
		set {
			lastApproverId = value;
			setChanged(Field.LastApproverId);
		}
	}

	/** 承認者1 */
	public Id approver01Id {
		get;
		set {
			approver01Id = value;
			setChanged(Field.Approver01Id);
		}
	}

	/** 精算確定ステータス */
	public ExpAccountingStatus accountingStatus {
		get;
		set {
			accountingStatus = value;
			setChanged(Field.AccountingStatus);
		}
	}
	
	/** The date on which payment was made */
	public AppDate paymentDate {
		get;
		set {
			paymentDate = value;
			setChanged(Field.PaymentDate);
		}
	}

	/** The date where the Accounting Clerk approved the Expense Report. */
	public AppDatetime authorizedTime {
		get;
		set {
			authorizedTime = value;
			setChanged(Field.AuthorizedTime);
		}
	}

	/** The DateTime when the ExpReport is exported to the Journal object. */
	public AppDateTime exportedTime {
		get;
		set {
			exportedTime = value;
			setChanged(Field.ExportedTime);
		}
	}

	/** 領収書添付あり */
	public Boolean hasReceipts {
		get;
		set {
			hasReceipts = value;
		}
	}

	/** ユーザにレコードに対する編集アクセス権があるかどうか(参照専用) */
	public Boolean hasEditAccess {
		get;
		set;
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}

	/**
	 * 申請ステータスの詳細
	 */
	public Enum DetailStatus {
		NOT_REQUESTED,			// 未申請
		PENDING,					// 承認待ち
		APPROVED,				// 承認済み
		FULLY_PAID,				// 支払い済み
		REJECTED,				// 却下
		REMOVED,					// 申請取消
		CANCELED,				// 承認取消
		ACCOUNTING_AUTHORIZED,	// 経理承認済み
		ACCOUNTING_REJECTED,		// 経理却下
		JOURNAL_CREATED			// 仕訳データ作成済み
	}

	/**
	 * 申請の詳細ステータスを取得する
	 */
	public ExpReportRequestEntity.DetailStatus getDetailStatus() {
		ExpReportRequestEntity.DetailStatus detailStatus;

		if (this.id == null) {
			// 未申請
			detailStatus = ExpReportRequestEntity.DetailStatus.NOT_REQUESTED;
		} else {
			if (this.status == AppRequestStatus.PENDING) {
				// ステータス：申請中
				detailStatus = ExpReportRequestEntity.DetailStatus.PENDING;
			} else if (this.status == AppRequestStatus.APPROVED) {
				if (this.accountingStatus == ExpAccountingStatus.FULLY_PAID) {
					detailStatus = ExpReportRequestEntity.DetailStatus.FULLY_PAID;
				} else if (this.accountingStatus == ExpAccountingStatus.AUTHORIZED) {
					if (this.exportedTime == null) {
						// ステータス：経理承認済み
						detailStatus = ExpReportRequestEntity.DetailStatus.ACCOUNTING_AUTHORIZED;
					} else {
						// ステータス：仕訳データ作成済み
						detailStatus = ExpReportRequestEntity.DetailStatus.JOURNAL_CREATED;
					}
				} else {
					// ステータス：承認済み
					detailStatus = ExpReportRequestEntity.DetailStatus.APPROVED;
				}
			} else if (this.status == AppRequestStatus.DISABLED) {
				// ステータス：無効
				if (this.cancelType == AppCancelType.REJECTED) {
					// 取消種別：却下
					detailStatus = ExpReportRequestEntity.DetailStatus.REJECTED;
				} else if (this.cancelType == AppCancelType.REMOVED) {
					// 取消種別：申請取消
					detailStatus = ExpReportRequestEntity.DetailStatus.REMOVED;
				} else if (this.cancelType == AppCancelType.CANCELED) {
					if (this.accountingStatus == ExpAccountingStatus.REJECTED) {
						// ステータス：経理却下
						detailStatus = ExpReportRequestEntity.DetailStatus.ACCOUNTING_REJECTED;
					} else {
						// 取消種別：承認取消
						detailStatus = ExpReportRequestEntity.DetailStatus.CANCELED;
					}
				} else {
					// 本来ありえないはず。直接オブジェクトを操作した場合や不具合により発生
					// メッセージ：申請のステータスが取得できませんでした。取消種別の値が正しくありません。
					throw new App.IllegalStateException(
							ComMessage.msg().Exp_Err_GetStatus(new List<String>{
								ComMessage.msg().Exp_Lbl_Request})
							+ ComMessage.msg().Com_Err_InvalidParameter(new List<String>{
								ComMessage.msg().Exp_Lbl_CancelType, AppConverter.stringValue(this.cancelType)}));
				}
			} else {
				// ステータスが設定されていないので未申請としておく
				detailStatus = ExpReportRequestEntity.DetailStatus.NOT_REQUESTED;
			}
		}

		return detailStatus;
	}
}