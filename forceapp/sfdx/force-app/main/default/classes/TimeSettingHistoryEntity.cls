/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数設定の履歴エンティティ
 */
public with sharing class TimeSettingHistoryEntity extends TimeSettingHistoryGeneratedEntity {

	/** 工数設定名Lの最大文字数 */
	public final static Integer NAME_MAX_LENGTH = 80;

	/**
	 * デフォルトコンストラクタ
	 */
	public TimeSettingHistoryEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public TimeSettingHistoryEntity(TimeSettingHistory__c sobj) {
		super(sobj);
	}

	/** 工数設定名(多言語) 参照のみ */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public TimeSettingHistoryEntity copy() {
		TimeSettingHistoryEntity copyEntity = new TimeSettingHistoryEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildHistoryEntity copySuperEntity() {
		return copy();
	}
}
