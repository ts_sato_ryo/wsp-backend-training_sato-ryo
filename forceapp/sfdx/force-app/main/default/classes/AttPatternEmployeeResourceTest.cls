/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description AttPatternEmployeeResourceのテストクラス
*/
@isTest
private class AttPatternEmployeeResourceTest {

	/**
	 * テストで使用するマスタデータを登録する
	 */
	@testSetup
	static void setup() {
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('テスト会社', country.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		User user = ComTestDataUtility.createUser('user', 'ja', 'ja_JP');
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('テスト社員', company.Id, dept.Id, user.Id, permission.Id);
	}
	/** 国ID */
	private static Id countryId() {
		return [SELECT Id FROM ComCountry__c LIMIT 1].Id;
	}
	/** 会社ID */
	private static Id companyId() {
		return [SELECT Id FROM ComCompany__c LIMIT 1].Id;
	}
	/** 部署履歴ID */
	private static Id departmentHistoryId() {
		return [SELECT Id FROM ComDeptHistory__c LIMIT 1].Id;
	}
	/** 社員ベースID */
	private static Id employeeBaseId() {
		return [SELECT Id FROM ComEmpBase__c LIMIT 1].Id;
	}
	/** 社員履歴ID */
	private static Id employeeHistoryId() {
		return [SELECT Id FROM ComEmpHistory__c LIMIT 1].Id;
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト(正常系)
	 * インポートバッチとインポートオブジェクトに登録できることを確認する
	 */
	@isTest
	static void batchExecutionApiTestRegister() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity runEmployee = testData.employee;
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		EmployeeBaseEntity testEmployee2 = testData.createEmployeeWithStandardUser('Test2');
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');
		AttPatternEntity testPattern2 = testData.createWorkPattern('Test Pattern2');

		// リクエストパラメータ
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testEmployee1.code;
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.EmployeePatternExecution record2 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record2.employeeCode = testEmployee2.code;
		record2.patternCode = testPattern2.code;
		record2.dayType = 'H';
		record2.targetDate = AppDate.today().addDays(1).format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1, record2};

		// 実行
		Test.startTest();
		AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
		api.execute(request);
		Test.stopTest();

		// 検証
		// バッチレコードが登録されている
		List<AttImportBatchEntity> batchList = new AttImportBatchRepository().getImportBatchList(testData.company.id);
		System.assertEquals(1, batchList.size());
		AttImportBatchEntity batch = batchList[0];
		System.assertEquals(request.companyId, batch.companyId);
		System.assertEquals(AttImportBatchType.ATT_PATTERN_APPLY, batch.importType);
		System.assertEquals(request.comment, batch.comment);
		System.assertEquals(runEmployee.getHistory(0).id, batch.actorHistoryId);
		System.assertEquals(testData.department.getHistory(0).id, batch.departmentHistoryId);
		System.assertEquals(request.records.size(), batch.count);
		System.assertEquals(0, batch.successCount);
		System.assertEquals(0, batch.failureCount);
		System.assertNotEquals(null, batch.importTime);
		System.assertEquals(ImportBatchStatus.WAITING, batch.status);
		// 勤務パターン適用情報が登録されている
		List<AttPatternApplyImportEntity> patternImportList = new AttPatternApplyImportRepository().getPatternList(batch.id);
		System.assertEquals(request.records.size(), patternImportList.size());
		for (Integer i = 0; i < request.records.size(); i++) {
			AttPatternEmployeeResource.EmployeePatternExecution record = request.records[i];
			AttPatternApplyImportEntity patternImport = patternImportList[i];
			System.assertEquals(i + 1,  patternImport.importNo);
			System.assertEquals(batch.id, patternImport.importBatchId);
			System.assertEquals(record.targetDate, patternImport.shiftDate);
			System.assertEquals(record.employeeCode, patternImport.employeeCode);
			System.assertEquals(record.patternCode, patternImport.patternCode);
			System.assertEquals(record.dayType, patternImport.dayType);
			System.assertEquals(ImportStatus.WAITING, patternImport.status);
		}
	}

/**
	 * @desctiprion BatchExecutionApiのテスト(正常系)
	 * 実行ユーザ社員が所属する会社と異なる会社を指定してもインポートバッチオブジェクトが正しく登録できることを確認する
	 */
	@isTest
	static void batchExecutionApiTestCompanyId() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		EmployeeBaseEntity testEmployee2 = testData.createEmployeeWithStandardUser('Test2');
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');
		AttPatternEntity testPattern2 = testData.createWorkPattern('Test Pattern2');

		// 別会社の実行ユーザ
		CompanyEntity testCompany2  = testData.createCompany('Test2');
		EmployeeBaseEntity runEmployee = testData.createEmployeeWithStandardUser('runEmployee');
		runEmployee.companyId = testCompany2.id;
		new EmployeeRepository().saveBaseEntity(runEmployee);

		// リクエストパラメータ
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testEmployee1.code;
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.EmployeePatternExecution record2 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record2.employeeCode = testEmployee2.code;
		record2.patternCode = testPattern2.code;
		record2.dayType = 'H';
		record2.targetDate = AppDate.today().addDays(1).format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1, record2};

		// 実行
		Test.startTest();
		System.runAs(testData.createRunAsUserFromEmployee(runEmployee)) {
			AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
			api.execute(request);
		}
		Test.stopTest();

		// 検証
		// 会社IDが実行ユーザ社員の会社ではなくリクエストパラメータの会社IDになっている
		List<AttImportBatchEntity> batchList = new AttImportBatchRepository().getImportBatchList(testData.company.id);
		System.assertEquals(1, batchList.size());
		AttImportBatchEntity batch = batchList[0];
		System.assertEquals(request.companyId, batch.companyId);
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト(正常系)
	 * 勤務パターン適用バッチが実行されることを確認する
	 */
	@isTest
	static void batchExecutionApiTestExcecuteBatch() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity runEmployee = testData.employee;
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');

		// 社員に勤務体系を設定する
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		testEmployee1.getHistory(0).workingTypeId = testData.workingType.id;
		new EmployeeRepository().saveHistoryEntity(testEmployee1.getHistory(0));

		// 勤務体系に勤務パターンを設定する
		testData.workingType.getHistory(0).patternCodeList = new List<String>{testPattern1.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		// リクエストパラメータ
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testEmployee1.code;
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1};

		// 実行
		Test.startTest();
		AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
		api.skipBatchForTest = false;
		api.execute(request);
		Test.stopTest();

		// 検証
		// バッチ処理が終了している
		List<AttImportBatchEntity> batchList = new AttImportBatchRepository().getImportBatchList(testData.company.id);
		System.assertEquals(1, batchList.size());
		AttImportBatchEntity batch = batchList[0];
		System.assertEquals(ImportBatchStatus.COMPLETED, batch.status);
		System.assertEquals(1, batch.count);
		System.assertEquals(1, batch.successCount);
		System.assertEquals(0, batch.failureCount);
		System.assertNotEquals(null, batch.importTime);
		System.assertEquals(request.companyId, batch.companyId);
		System.assertEquals(AttImportBatchType.ATT_PATTERN_APPLY, batch.importType);
		System.assertEquals(request.comment, batch.comment);
		System.assertEquals(runEmployee.getHistory(0).id, batch.actorHistoryId);
		System.assertEquals(testData.department.getHistory(0).id, batch.departmentHistoryId);

		// 勤務パターン適用情報がのステータスが更新されている
		List<AttPatternApplyImportEntity> patternImportList = new AttPatternApplyImportRepository().getPatternList(batch.id);
		System.assertEquals(request.records.size(), patternImportList.size());
		AttPatternApplyImportEntity patternImport = patternImportList[0];
		System.assertEquals(null, patternImport.error);
		System.assertEquals(ImportStatus.SUCCESS, patternImport.status);
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト(正常系)
	 * API実行ユーザが部署に所属していない場合、バッチレコードの部署が空欄になることを確認する
	 */
	@isTest
	static void batchExecutionApiTestNotBelongingToDepartment() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');
		EmployeeBaseEntity runEmployee = testData.employee;
		runEmployee.getHistory(0).departmentId = null;
		new EmployeeRepository().saveHistoryEntity(runEmployee.getHistory(0));

		// リクエストパラメータ
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testEmployee1.code;
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1};

		// 実行
		AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
		api.execute(request);

		// 検証
		// バッチレコードの実効ユーザの部署が空欄である
		List<AttImportBatchEntity> batchList = new AttImportBatchRepository().getImportBatchList(testData.company.id);
		System.assertEquals(1, batchList.size());
		AttImportBatchEntity batch = batchList[0];
		System.assertEquals(null, batch.departmentHistoryId);
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト(正常系)
	 * API実行権限を持っている標準ユーザがAPIを実行できることを確認する
	 */
	@isTest
	static void batchExecutionApiTestHasPermission() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('StandardUser');
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');

		// 勤務パターン適用権限を有効に設定する
		testData.permission.isManageAttPatternApply = true;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testData.employee.code;
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertEquals(null, actEx, 'App.NoPermissionExceptionが発生しました');
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト(異常系)
	 * API実行権限を持っていない場合、想定される例外が発生することを確認する
	 */
	@isTest
	static void batchExecutionApiTestNoPermission() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('StandardUser');
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');

		// 勤務パターン適用権限を無効に設定する
		testData.permission.isManageAttPatternApply = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testData.employee.code;
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト(異常系)
	 * 実行ユーザが社員でない場合、想定される例外が発生することを確認する
	 */
	@isTest
	static void batchExecutionApiTesNoEmployeeUser() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity runEmployee = testData.employee;
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');

		// 実効するユーザの社員を失効させる
		runEmployee.getHistory(0).validTo = AppDate.today();
		runEmployee.getHistory(0).validFrom = AppDate.today().addDays(-10);
		new EmployeeRepository().saveHistoryEntity(runEmployee.getHistory(0));

		// リクエストパラメータ
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testData.employee.code;
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1};

		// API実行
		App.IllegalStateException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(runEmployee)) {
			try {
				AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
				api.execute(request);
			} catch (App.IllegalStateException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.IllegalStateExceptionが発生しませんでした');
		String expMessage = ComMessage.msg().Com_Err_RequiredEmployeeToExcecute;
		System.assertEquals(expMessage, actEx.getMessage());
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト
	 * 勤務パターン適用のデータが検証されていることを確認する
	 */
	@isTest
	static void batchExecutionApiTestValidateRecords() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity runEmployee = testData.employee;
		EmployeeBaseEntity testEmployee1 = testData.createEmployeeWithStandardUser('Test1');
		AttPatternEntity testPattern1 = testData.createWorkPattern('Test Pattern1');

		// リクエストパラメータ
		// 社員コードを257文字以上に設定する
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = String.valueOf('0123456789').repeat('', 26);
		record1.patternCode = testPattern1.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = 'Test Comment';
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1};

		// 実行
		App.ParameterException actEx;
		try {
			AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
			api.execute(request);
		} catch (App.ParameterException e) {
			actEx = e;
		}

		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(App.ERR_CODE_INVALID_VALUE, actEx.getErrorCode(), actEx.getMessage());
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト(異常系)
	 * リクエストパラメータの値が正しくない場合、想定した例外が発生することを確認する
	 */
	@isTest
	static void batchExecutionApiTestValidateParameter() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();

		AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
		App.ParameterException actEx;

		// Test1: 会社IDが設定されていない
		actEx = null;
		try {
			AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
			request.companyId = null;
			request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>();
			api.execute(request);
		} catch (App.ParameterException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{'companyId'}), actEx.getMessage());

		// Test2: 会社IDにID以外の文字列が設定されている
		actEx = null;
		try {
			AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
			request.companyId = 'testtest';
			request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>();
			api.execute(request);
		} catch (App.ParameterException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}), actEx.getMessage());

		// Test3: 勤務パターン適用一覧が設定されていない
		actEx = null;
		try {
			AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
			request.companyId = testData.company.id;
			request.records = null;
			api.execute(request);
		} catch (App.ParameterException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{'records'}), actEx.getMessage());

		// Test4: 勤務パターン適用一覧が処理可能な上限件数を超えている
		actEx = null;
		try {
			AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
			request.companyId = testData.company.id;
			request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>();
			for (Integer i = 0; i < AttPatternEmployeeBatch.LIMIT_RECORDS_COUNT + 1; i++) {
				request.records.add(new AttPatternEmployeeResource.EmployeePatternExecution());
			}
			api.execute(request);
		} catch (App.ParameterException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Att_Err_PatternApplyOverRecordCount, actEx.getMessage());
	}

	/**
	 * @desctiprion BatchExecutionApiのテスト
	 * バッチのデータが検証されていることを確認する
	 */
	@isTest
	static void batchExecutionApiTestValidateBatch() {

		// テストデータ
		TestData.TestDataEntity testData = new TestData.TestDataEntity();

		// リクエストパラメータ
		// コメントを1000文字より多くしておく
		AttPatternEmployeeResource.EmployeePatternExecution record1 = new AttPatternEmployeeResource.EmployeePatternExecution();
		record1.employeeCode = testData.employee.code;
		record1.patternCode = testData.workPattern.code;
		record1.dayType = 'w';
		record1.targetDate = AppDate.today().format();
		AttPatternEmployeeResource.BatchExecutionRequest request = new AttPatternEmployeeResource.BatchExecutionRequest();
		request.companyId = testData.company.id;
		request.comment = String.valueOf('0123456789').repeat('', 101);
		request.records = new List<AttPatternEmployeeResource.EmployeePatternExecution>{record1};

		// 実行
		App.ParameterException actEx;
		try {
			AttPatternEmployeeResource.BatchExecutionApi api = new AttPatternEmployeeResource.BatchExecutionApi();
			api.execute(request);
		} catch (App.ParameterException e) {
			actEx = e;
		}

		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(App.ERR_CODE_INVALID_VALUE, actEx.getErrorCode(), actEx.getMessage());
	}

	/**
	 * 勤務パターン適用結果一覧を取得する(正常系:companyIdで検索)
	 */
	@isTest
	static void batchListApiTestByCompanyId() {

		// テストデータ作成
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());

		Test.startTest();
		// companyIdで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'companyId' => companyId()
		};

		// 実行
		AttPatternEmployeeResource.BatchListApi api = new AttPatternEmployeeResource.BatchListApi();
		AttPatternEmployeeResource.BatchListResponse res = (AttPatternEmployeeResource.BatchListResponse)api.execute(paramMap);
		Test.stopTest();

		// 検証
		System.assertEquals(1, res.records.size());
		System.assertEquals(importBatch.Id, res.records[0].id);
		System.assertEquals(importBatch.ImportTime__c.formatGMT('YYYY-MM-dd\'T\'HH:mm:ss\'Z\''), res.records[0].importDateTime);
		System.assertEquals(importBatch.Status__c, res.records[0].status);
		EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
		empfilter.historyIds = new Set<Id>{importBatch.ActorHistoryId__c};
		EmployeeBaseEntity empBase = new EmployeeRepository().searchEntity(empfilter)[0];
		System.assertEquals(empBase.fullNameL.getFullName(), res.records[0].actorName);
		DepartmentHistoryEntity deptHistory = new DepartmentRepository().getHistoryEntity(importBatch.DepartmentHistoryId__c);
		System.assertEquals(deptHistory.nameL.getValue(), res.records[0].departmentName);
		System.assertEquals(importBatch.Comment__c, res.records[0].comment);
		System.assertEquals(importBatch.Count__c, res.records[0].count);
		System.assertEquals(importBatch.SuccessCount__c, res.records[0].successCount);
		System.assertEquals(importBatch.FailureCount__c, res.records[0].failureCount);
	}

	/**
	 * 勤務パターン適用結果一覧を取得する(異常系:companyIdが無効な文字列))
	 */
	@isTest
	static void batchListApiTestInvalidCompanyId() {

		// テストデータ作成
		AttPatternEmployeeResource.BatchListRequest param = new AttPatternEmployeeResource.BatchListRequest();
		param.companyId = 'InvalidCompanyId';

		// 実行
		AttPatternEmployeeResource.BatchListApi api = new AttPatternEmployeeResource.BatchListApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
			System.assert(false);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}), ex.getMessage());
	}

	/**
	 * 社員履歴IDと社員ベースのMapが取得可能なことを検証する
	 * (BatchListApiのprivateメソッドのテスト)
	 */
	@isTest
	static void getEmployeeMapTest() {

		// テストデータ作成
		Set<Id> empHistoryIdSet = new Set<Id>{employeeHistoryId()};

		// 実行
		Map<Id, EmployeeBaseEntity> empMap = new AttPatternEmployeeResource.BatchListApi().getEmployeeMap(empHistoryIdSet);

		// 検証
		System.assertEquals(1, empMap.size());
		// 社員ベースIDの検証
		EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
		empfilter.historyIds = empHistoryIdSet;
		EmployeeBaseEntity empBase = new EmployeeRepository().searchEntity(empfilter)[0];
		System.assertEquals(empBase.id, empMap.get(empHistoryIdSet.iterator().next()).id);
	}

	/**
	 * 部署履歴IDと部署ベースのMapが取得可能なことを検証する
   * (BatchListApiのprivateメソッドのテスト)
	 */
	@isTest
	static void getDepartmentMapTest() {

		// テストデータ作成
		Set<Id> deptHistoryIdSet = new Set<Id>{departmentHistoryId()};

		// 実行
		Map<Id, DepartmentBaseEntity> deptMap = new AttPatternEmployeeResource.BatchListApi().getDepartmentMap(deptHistoryIdSet);

		// 検証
		System.assertEquals(1, deptMap.size());
		// 部署ベースIDの検証
		DepartmentRepository.SearchFilter deptFilter = new DepartmentRepository.SearchFilter();
		deptFilter.historyIds = new List<Id>(deptHistoryIdSet);
		DepartmentBaseEntity deptBase = new DepartmentRepository().searchEntity(deptFilter)[0];
		System.assertEquals(deptBase.id, deptMap.get(deptHistoryIdSet.iterator().next()).id);
	}

	/**
	 * 会社IDと会社のMapが取得可能なことを検証する
	 */
	@isTest
	static void getCompanyMapTest() {

		// テストデータ作成
		DepartmentBaseEntity deptBaseEntity = new DepartmentBaseEntity();
		deptBaseEntity.companyId = companyId();
		List<DepartmentBaseEntity> deptBaseList = new List<DepartmentBaseEntity>{deptBaseEntity};

		// 実行
		Map<Id, CompanyEntity> companyMap = new AttPatternEmployeeResource.BatchListApi().getCompanyMap(deptBaseList);

		// 検証
		System.assertEquals(1, companyMap.size());
		// 会社IDの検証
		System.assertEquals(deptBaseEntity.companyId, companyMap.get(deptBaseEntity.companyId).id);
	}

	/**
	 * BatchRecordのListが正常に作成可能なことを検証する
	 * 部署が別会社の場合、部署名 + (会社名)になることを確認する
	 * (BatchListApiのprivateメソッドのテスト)
	 */
	@isTest
	static void createBatchRecordListTest() {

		// テストデータ作成
		// 勤怠インポートバッチエンティティのListを作成
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());
		List<AttImportBatchEntity> entityList = new AttImportBatchRepository().getImportBatchList(importBatch.CompanyId__c);

		// 社員履歴IDと社員ベースエンティティのMapを作成
		EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
		empfilter.historyIds = new Set<Id>{importBatch.ActorHistoryId__c};
		EmployeeBaseEntity empBase = new EmployeeRepository().searchEntity(empfilter)[0];
		Map<Id, EmployeeBaseEntity> empMap = new Map<Id, EmployeeBaseEntity>{
			importBatch.ActorHistoryId__c => empBase
		};

		// 部署履歴IDと部署ベースエンティティのMapを作成
		DepartmentRepository.SearchFilter deptFilter = new DepartmentRepository.SearchFilter();
		deptFilter.historyIds = new List<Id>{importBatch.DepartmentHistoryId__c};
		DepartmentBaseEntity deptBase = new DepartmentRepository().searchEntity(deptFilter)[0];
		// 部署に別会社のIDを設定する
		ComCompany__c otherCompany = ComTestDataUtility.createCompany('別のテスト会社', countryId());
		deptBase.companyId = otherCompany.Id;
		Map<Id, DepartmentBaseEntity> deptMap = new Map<Id, DepartmentBaseEntity>{
			importBatch.DepartmentHistoryId__c => deptBase
		};

		// 会社IDと会社エンティティのMapを作成
		CompanyEntity company = new CompanyRepository().getEntity(deptBase.companyId);
		Map<Id, CompanyEntity> companyMap = new Map<Id, CompanyEntity>{
			company.id => company
		};

		// 実行
		List<AttPatternEmployeeResource.BatchRecord> batchRecordList = new AttPatternEmployeeResource.BatchListApi().createBatchRecordList(entityList, empMap, deptMap, companyMap);

		// 検証
		System.assertEquals(1, batchRecordList.size());
		// 項目値の検証
		System.assertEquals(importBatch.Id, batchRecordList[0].id);
		System.assertEquals(importBatch.ImportTime__c.formatGMT('YYYY-MM-dd\'T\'HH:mm:ss\'Z\''), batchRecordList[0].importDateTime);
		System.assertEquals(importBatch.Status__c, batchRecordList[0].status);
		System.assertEquals(empBase.fullNameL.getFullName(), batchRecordList[0].actorName);
		DepartmentHistoryEntity deptHistory = new DepartmentRepository().getHistoryEntity(importBatch.DepartmentHistoryId__c);
		System.assertEquals(deptHistory.nameL.getValue() + '(' + company.nameL.getValue() + ')', batchRecordList[0].departmentName);
		System.assertEquals(importBatch.Comment__c, batchRecordList[0].comment);
		System.assertEquals(importBatch.Count__c, batchRecordList[0].count);
		System.assertEquals(importBatch.SuccessCount__c, batchRecordList[0].successCount);
		System.assertEquals(importBatch.FailureCount__c, batchRecordList[0].failureCount);
	}

	/**
	 * BatchRecordのListが正常に作成可能なことを検証する
	 * 実行者名と実行者の部署名がNULLになることを確認する
	 * (BatchListApiのprivateメソッドのテスト)
	 */
	@isTest
	static void createBatchRecordListTestNameNull() {

		// テストデータ作成
		// 勤怠インポートバッチエンティティのListを作成
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());
		List<AttImportBatchEntity> entityList = new AttImportBatchRepository().getImportBatchList(importBatch.CompanyId__c);

		// 社員履歴IDと社員ベースエンティティのMapを作成(空のMAP)
		Map<Id, EmployeeBaseEntity> empMap = new Map<Id, EmployeeBaseEntity>();
		// 部署履歴IDと部署ベースエンティティのMapを作成(空のMAP)
		Map<Id, DepartmentBaseEntity> deptMap = new Map<Id, DepartmentBaseEntity>();
		// 会社IDと会社エンティティのMapを作成(空のMAP)
		Map<Id, CompanyEntity> companyMap = new Map<Id, CompanyEntity>();

		// 実行
		List<AttPatternEmployeeResource.BatchRecord> batchRecordList = new AttPatternEmployeeResource.BatchListApi().createBatchRecordList(entityList, empMap, deptMap, companyMap);

		// 検証
		System.assertEquals(1, batchRecordList.size());
		// 項目値の検証
		System.assertEquals(importBatch.Id, batchRecordList[0].id);
		System.assertEquals(importBatch.ImportTime__c.formatGMT('YYYY-MM-dd\'T\'HH:mm:ss\'Z\''), batchRecordList[0].importDateTime);
		System.assertEquals(importBatch.Status__c, batchRecordList[0].status);
		System.assertEquals(null, batchRecordList[0].actorName);
		System.assertEquals(null, batchRecordList[0].departmentName);
		System.assertEquals(importBatch.Comment__c, batchRecordList[0].comment);
		System.assertEquals(importBatch.Count__c, batchRecordList[0].count);
		System.assertEquals(importBatch.SuccessCount__c, batchRecordList[0].successCount);
		System.assertEquals(importBatch.FailureCount__c, batchRecordList[0].failureCount);
	}

	/**
	 * 勤務パターン適用結果詳細を取得する(正常系:idで検索)
	 */
	@isTest
	static void batchDetailApiTestById() {

		// テストデータ作成
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());
		AttPatternApplyImport__c patternApplyImport = ComTestDataUtility.createAttPatternApplyImport(importBatch.Id, 'EMP001', 'PTN001');

		Test.startTest();
		// idで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => importBatch.Id
		};

		// 実行
		AttPatternEmployeeResource.BatchDetailApi api = new AttPatternEmployeeResource.BatchDetailApi();
		AttPatternEmployeeResource.BatchDetailResponse res = (AttPatternEmployeeResource.BatchDetailResponse)api.execute(paramMap);
		Test.stopTest();

		// 検証
		System.assertEquals(1, res.records.size());
		System.assertEquals(patternApplyImport.EmployeeCode__c, res.records[0].employeeCode);
		System.assertEquals(patternApplyImport.Date__c, res.records[0].targetDate);
		System.assertEquals(patternApplyImport.PatternCode__c, res.records[0].patternCode);
		System.assertEquals(patternApplyImport.DayType__c, res.records[0].dayType);
		System.assertEquals(patternApplyImport.Status__c, res.records[0].status);
		System.assertEquals(patternApplyImport.Error__c, res.records[0].errorDetail);
	}

	/**
	 * 勤務パターン適用結果詳細を取得する(正常系:レスポンスの並び順がインポート順の昇順になること)
	 */
	@isTest
	static void batchDetailApiTestOrderByImportNo() {

		// テストデータ作成
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());
		List<AttPatternApplyImport__c> patternApplyImportList = ComTestDataUtility.createAttPatternApplyImport(importBatch.Id, 'EMP001', 'PTN001', 3);

		// インポート順を変える
		patternApplyImportList[0].No__c = 2;
		patternApplyImportList[1].No__c = 3;
		patternApplyImportList[2].No__c = 1;
		upsert patternApplyImportList;

		Test.startTest();
		// idで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => importBatch.Id
		};

		// 実行
		AttPatternEmployeeResource.BatchDetailApi api = new AttPatternEmployeeResource.BatchDetailApi();
		AttPatternEmployeeResource.BatchDetailResponse res = (AttPatternEmployeeResource.BatchDetailResponse)api.execute(paramMap);
		Test.stopTest();

		// 検証
		System.assertEquals(3, res.records.size());
		System.assertEquals(patternApplyImportList[2].Error__c, res.records[0].errorDetail);
		System.assertEquals(patternApplyImportList[0].Error__c, res.records[1].errorDetail);
		System.assertEquals(patternApplyImportList[1].Error__c, res.records[2].errorDetail);
	}

	/**
	 * 勤務パターン適用結果詳細を取得する(異常系:idが無効な文字列))
	 */
	@isTest
	static void batchDetailApiTestInvalidId() {

		// テストデータ作成
		AttPatternEmployeeResource.BatchDetailRequest param = new AttPatternEmployeeResource.BatchDetailRequest();
		param.id = 'InvalidId';

		// 実行
		AttPatternEmployeeResource.BatchDetailApi api = new AttPatternEmployeeResource.BatchDetailApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), ex.getMessage());
	}

	/**
	 * 勤務パターン適用結果詳細を取得する(異常系:idがNULL))
	 */
	@isTest
	static void batchDetailApiTestNullId() {

		// テストデータ作成
		AttPatternEmployeeResource.BatchDetailRequest param = new AttPatternEmployeeResource.BatchDetailRequest();

		// 実行
		AttPatternEmployeeResource.BatchDetailApi api = new AttPatternEmployeeResource.BatchDetailApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'id'}), ex.getMessage());
	}

	/**
	 * 勤務パターン適用結果詳細を取得する(正常系:アクセス権限あり))
	 * API実行権限を持っている標準ユーザがAPIを実行できることを確認する
	 */
	@isTest
	static void batchDetailApiTestHasPermission() {

		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('StandardUser');
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());
		AttPatternApplyImport__c patternApplyImport = ComTestDataUtility.createAttPatternApplyImport(importBatch.Id, 'EMP001', 'PTN001');

		// 勤務パターン適用権限を有効に設定する
		testData.permission.isManageAttPatternApply = true;
		new PermissionRepository().saveEntity(testData.permission);

		// idで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => importBatch.Id
		};

		// 実行
		App.NoPermissionException ex;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttPatternEmployeeResource.BatchDetailApi api = new AttPatternEmployeeResource.BatchDetailApi();
				api.execute(paramMap);
			} catch (App.NoPermissionException e) {
				ex = e;
			}
		}

		// 検証
		System.assertEquals(null, ex, 'App.NoPermissionExceptionが発生しました');
	}

	/**
	 * 勤務パターン適用結果詳細を取得する(異常系:アクセス権限なし))
	 * API実行権限を持っていない場合、想定される例外が発生することを確認する
	 */
	@isTest
	static void batchDetailApiTestNoPermission() {

		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('StandardUser');
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());
		AttPatternApplyImport__c patternApplyImport = ComTestDataUtility.createAttPatternApplyImport(importBatch.Id, 'EMP001', 'PTN001');

		// 勤務パターン適用権限を無効に設定する
		testData.permission.isManageAttPatternApply = false;
		new PermissionRepository().saveEntity(testData.permission);

		// idで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => importBatch.Id
		};

		// 実行
		App.NoPermissionException ex;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttPatternEmployeeResource.BatchDetailApi api = new AttPatternEmployeeResource.BatchDetailApi();
				api.execute(paramMap);
			} catch (App.NoPermissionException e) {
				ex = e;
			}
		}

		// 検証
		System.assertNotEquals(null, ex, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, ex.getMessage());
	}

	/**
	 * 勤務パターン適用結果詳細を取得する(異常系:検索結果が0件)
	 */
	@isTest
	static void batchDetailApiTestRecordNotFound() {

		// テストデータ作成
		AttImportBatch__c importBatch = ComTestDataUtility.createAttImportBatch('Test勤怠インポートバッチ', companyId(), employeeHistoryId(), departmentHistoryId());

		Test.startTest();
		// idで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => importBatch.Id
		};

		// 実行
		App.RecordNotFoundException ex;
		try {
			AttPatternEmployeeResource.BatchDetailApi api = new AttPatternEmployeeResource.BatchDetailApi();
			api.execute(paramMap);
		} catch (App.RecordNotFoundException e) {
			ex = e;
		}
		Test.stopTest();

		// 検証
		System.assertNotEquals(null, ex, 'App.RecordNotFoundExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Att_Err_LogNotFound, ex.getMessage());
	}
}