/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 拡張項目のリポジトリ
 */
public with sharing class ExtendedItemRepository extends LogicalDeleteRepository {

	/** レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/** オブジェクト名 */
	private static final String BASE_OBJECT_NAME = ComExtendedItem__c.SObjectType.getDescribe().getName();

	/** Relationship for Custom Extended Item */
	private static final String EXTENDED_ITEM_CUSTOM_R = ComExtendedItem__c.ExtendedItemCustomId__c.getDescribe().getRelationshipName();

	/**
	 * 取得対象の項目名
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ComExtendedItem__c.Id,
				ComExtendedItem__c.Name,
				ComExtendedItem__c.Code__c,
				ComExtendedItem__c.UniqKey__c,
				ComExtendedItem__c.Name_L0__c,
				ComExtendedItem__c.Name_L1__c,
				ComExtendedItem__c.Name_L2__c,
				ComExtendedItem__c.CompanyId__c,
				ComExtendedItem__c.InputType__c,
				ComExtendedItem__c.LimitLength__c,
				ComExtendedItem__c.DefaultValueText__c,
				ComExtendedItem__c.Picklist__c,
				ComExtendedItem__c.ExtendedItemCustomId__c,
				ComExtendedItem__c.Description_L0__c,
				ComExtendedItem__c.Description_L1__c,
				ComExtendedItem__c.Description_L2__c,
				ComExtendedItem__c.Removed__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** 拡張項目IDセット */
		public Set<Id> ids;
		/** 拡張項目コードセット */
		public Set<String> codes;
		/** 会社IDセット */
		public Set<Id> companyIds;
		/** 入力タイプ */
		public ExtendedItemInputType inputType;
		/** 論理削除済みのレコードも取得対象とする場合はtrue */
		public Boolean includeRemoved;
	}

	/**
	 * 指定したIDを持つ拡張項目を取得する
	 * 論理削除済みのレコードは取得対象外。
	 * @param itemId 取得対象のID
	 * @return 指定したIDを持つ拡張項目、ただし該当する拡張項目が存在しない場合はnull
	 */
	public ExtendedItemEntity getEntity(Id itemId) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>{itemId};
		List<ExtendedItemEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 指定したIDを持つ拡張項目を取得する
	 * 論理削除済みのレコードは取得対象外。
	 * @param itemIdList 取得対象のID
	 * @return 指定したIDを持つ拡張項目、該当する拡張項目が存在しない場合は空のリストを返却する
	 */
	public List<ExtendedItemEntity> getEntityList(List<Id> itemIdList) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>(itemIdList);
		return searchEntityList(filter);
	}

	/**
	 * 会社IDとコードを条件に拡張項目を検索する。
	 * 論理削除済みのレコードは取得対象外。
	 * @param companyId 会社ID
	 * @param code コード
	 * @return 検索結果 該当するマスタが存在しない場合はnull
	 */
	public  ExtendedItemEntity getEntityByCode(Id companyId, String code) {
		SearchFilter filter = new SearchFilter();
		filter.companyIds = new Set<Id>{companyId};
		filter.codes = new Set<String>{code};
		List<ExtendedItemEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 検索フィルタの条件に一致するジョブタイプをコードの昇順で取得する。
	 * 当メソッドではレコードをロックしない。(forUpdateではない)
	 * @param filter 検索フィルタ
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<ExtendedItemEntity> searchEntityList(SearchFilter filter) {
		return searchEntityList(filter, false);
	}

	/**
	 * 検索フィルタの条件に一致する拡張項目をコードの昇順で取得する。
	 * @param filter 検索フィルタ
	 * @param forUpdate 更新用で取得する場合はtrue(ロックする)
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<ExtendedItemEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);

		// Add Custom Extended Item Names
		selectFieldList.add(getFieldName(EXTENDED_ITEM_CUSTOM_R, ComExtendedItemCustom__c.Name_L0__c));
		selectFieldList.add(getFieldName(EXTENDED_ITEM_CUSTOM_R, ComExtendedItemCustom__c.Name_L1__c));
		selectFieldList.add(getFieldName(EXTENDED_ITEM_CUSTOM_R, ComExtendedItemCustom__c.Name_L2__c));

		// WHERE句を生成
		List<String> conditions = new List<String>();
		final Set<Id> ids = filter.ids;
		if (ids != null) {
			conditions.add(createInExpression(ComExtendedItem__c.Id, 'ids'));
		}
		final Set<Id> companyIds = filter.companyIds;
		if (companyIds != null) {
			conditions.add(createInExpression(ComExtendedItem__c.CompanyId__c, 'companyIds'));
		}
		final Set<String> codes = filter.codes;
		if (codes != null) {
			conditions.add(createInExpression(ComExtendedItem__c.Code__c, 'codes'));
		}
		final String inputType = ExtendedItemInputType.getValue(filter.inputType);
		if (inputType != null) {
			conditions.add(createInExpression(ComExtendedItem__c.InputType__c, 'inputType'));
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			conditions.add(createEqExpression(ComExtendedItem__c.Removed__c, false));
		}

		// SOQLを生成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + BASE_OBJECT_NAME
				+ buildWhereString(conditions)
				+ ' ORDER BY ' + getFieldName(ComExtendedItem__c.Code__c) + ' Asc';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		System.debug('ExtendedItemRepository: SOQL=' + soql);

		// クエリ実行
		List<ComExtendedItem__c> sObjList = Database.query(soql);
		List<ExtendedItemEntity> entityList = new List<ExtendedItemEntity>();
		for (ComExtendedItem__c sobj : sObjList) {
			entityList.add(createEntity(sobj));
		}
		return entityList;
	}

	/**
	 * 拡張項目を保存する
	 * @param entity 保存対象の拡張項目
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntity(ExtendedItemEntity entity) {
		 return saveEntityList(new List<ExtendedItemEntity>{entity});
	 }

	/**
	 * 拡張項目を保存する
	 * @param entityList 保存対象のジョブタイプ
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntityList(List<ExtendedItemEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	 }

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public virtual Repository.SaveResult saveEntityList(List<ExtendedItemEntity> entityList, Boolean allOrNone) {

		// エンティティをSObjectに変換する
		List<ComExtendedItem__c> sObjList = new List<ComExtendedItem__c>();
		for (ExtendedItemEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}

		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/**
	 * エンティティからSObjectを作成する
	 * @param entity 作成元のエンティティ（nullは許容しない）
	 * @return 作成したsObject
	 */
	private ComExtendedItem__c createSObject(ExtendedItemEntity entity) {

		ComExtendedItem__c sobj = new ComExtendedItem__c(Id = entity.Id);
		if (entity.isChanged(ExtendedItemEntity.Field.NAME)) {
			sobj.Name = entity.name;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.CODE)) {
			sobj.Code__c = entity.code;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.UNIQ_KEY)) {
			sobj.UniqKey__c = entity.uniqKey;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.NAME_L0)) {
			sobj.Name_L0__c = entity.nameL0;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.NAME_L1)) {
			sobj.Name_L1__c = entity.nameL1;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.NAME_L2)) {
			sobj.Name_L2__c = entity.nameL2;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.COMPANY_ID)) {
			sobj.CompanyId__c = entity.companyId;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.INPUT_TYPE)) {
			sobj.InputType__c = ExtendedItemInputType.getValue(entity.inputType);
		}

		if (entity.isChanged(ExtendedItemEntity.Field.LIMIT_LENGTH)) {
			sobj.LimitLength__c = entity.limitLength;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.DEFAULT_VALUE_TEXT)) {
			sobj.DefaultValueText__c = entity.defaultValueText;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.PICKLIST)) {
			sobj.Picklist__c = entity.picklist == null ? null : JSON.serialize(entity.picklist);
		}

		if (entity.isChanged(ExtendedItemEntity.Field.EXTENDED_ITEM_CUSTOM_ID)) {
			sobj.ExtendedItemCustomId__c = entity.extendedItemCustomId;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.DESCRIPTION_L0)) {
			sobj.Description_L0__c = entity.descriptionL0;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.DESCRIPTION_L1)) {
			sobj.Description_L1__c = entity.descriptionL1;
		}

		if (entity.isChanged(ExtendedItemEntity.Field.DESCRIPTION_L2)) {
			sobj.Description_L2__c = entity.descriptionL2;
		}

		if (entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED)) {
			sobj.Removed__c = entity.isRemoved;
		}

		return sobj;
	}

	/**
	 * ComExtendedItem__cオブジェクトからエンティティを作成する
	 * @param sObj 作成元のSObjct
	 * @return 作成したエンティティ
	 */
	private ExtendedItemEntity createEntity(ComExtendedItem__c sobj) {
		ExtendedItemEntity entity = new ExtendedItemEntity();
		entity.setId(sobj.Id);
		entity.code = sobj.Code__c;
		entity.uniqKey = sobj.UniqKey__c;
		entity.name = sobj.Name;
		entity.nameL0 = sobj.Name_L0__c;
		entity.nameL1 = sobj.Name_L1__c;
		entity.nameL2 = sobj.Name_L2__c;
		entity.companyId = sobj.CompanyId__c;
		entity.inputType = ExtendedItemInputType.valueOf(sobj.InputType__c);
		entity.limitLength = (Integer)sobj.LimitLength__c;
		entity.defaultValueText = sobj.DefaultValueText__c;
		if (sobj.Picklist__c != null) {
			entity.picklist = (List<ExtendedItemEntity.PicklistOption>)JSON.deserialize(sobj.Picklist__c, List<ExtendedItemEntity.PicklistOption>.class);
		}
		entity.extendedItemCustomId = sobj.ExtendedItemCustomId__c;
		entity.extendedItemCustomName = new AppMultiString(
				sObj.ExtendedItemCustomId__r.Name_L0__c,
				sObj.ExtendedItemCustomId__r.Name_L1__c,
				sObj.ExtendedItemCustomId__r.Name_L2__c);
		entity.descriptionL0 = sobj.Description_L0__c;
		entity.descriptionL1 = sobj.Description_L1__c;
		entity.descriptionL2 = sobj.Description_L2__c;
		entity.isRemoved = sobj.Removed__c;

		// 変更情報をリセットする
		entity.resetChanged();

		return entity;
	}
}