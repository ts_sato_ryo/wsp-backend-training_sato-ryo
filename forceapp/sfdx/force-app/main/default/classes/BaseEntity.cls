/**
 * エンティティ基底クラス
 * 以前はEntity.clsをエンティティの基底クラスとしていましたが、SObjectをラップする設計変更に伴い、このクラスが作成されました。
 * ツールによって自動生成したエンティティクラスは、このクラスのサブクラスとして定義してください。
 */
public abstract with sharing class BaseEntity {

	/**
	 * エンティティが参照するSObject
	 */
	protected final SObject sobj;

	/**
	 * フィールド名とフィールドのMap
	 */
	protected final Map<String, Schema.SObjectField> sobjectFieldMap;

	/**
	 * 更新した値を保持するMap
	 */
	protected final Map<Schema.SObjectField, Object> changedValues;

	/**
	 * コンストラクタ
	 * @param sobj 参照するSObject
	 * @param sobjectFieldMap フィールド名とフィールドのMap
	 */
	public BaseEntity(SObject sobj, Map<String, Schema.SObjectField> sobjectFieldMap) {
		this.sobj = sobj;
		this.sobjectFieldMap = sobjectFieldMap;
		this.changedValues = new Map<Schema.SObjectField, Object>();
	}

	/**
	 * SObjectの名前空間のプレフィックス   Namespace prefix of SObjects
	 * 名前空間がない場合は空文字となる(nullではない)
	 * Example: tswsp__
	 */
	private static final String NAMESPACE_PREFIX;
	static {
		// 名前空間のプレフィックスが取得できれば良いため、本アプリケーションに含まれるカスタム項目であれば何でも良い
		Schema.DescribeFieldResult field = ComCompany__c.Code__c.getDescribe();
		NAMESPACE_PREFIX = field.getName().removeEnd(field.getLocalName());
	}

	/**
	 * 名前空間のプレフィックス付きの名前を作成する
	 * @param localName 名前空間がついていない対象の名前(例:Code__c)
	 * @return 名前空間のプレフィックス付きの名前(例:tswsp__Code__c)
	 */
	public static String createNameWithNamespace(String localName) {
		return NAMESPACE_PREFIX + localName;
	}

	/**  ID */
	public Id id {
		get {
			return (Id)sobj.get('Id');
		}
		private set;
	}

	/** IDを設定する */
	public void setId(String id) {
		try {
			// this.id = id; TODO 自動生成クラスはプロパティに値を保持していない。親クラスでの扱いを検討する
			this.sobj.Id = id;
		} catch (Exception e) {
			//
			throw e;
		}
	}

	/** IDを設定する */
	public void setId(Id id) {
		// this.id = id;
		this.sobj.Id = id;
	}

	/** 作成者 ID */
	public Id createdById {
		get {
			return (Id)sobj.get('CreatedById');
		}
		private set;
	}

	/** 作成日 */
	public AppDateTime createdDate {
		get {
			return AppDateTime.valueOf((Datetime)sobj.get('CreatedDate'));
		}
		private set;
	}

	/** 削除 */
	public Boolean isDeleted {
		get {
			return (Boolean)sobj.get('IsDeleted');
		}
		private set;
	}

	/** 最終更新者 ID */
	public Id lastModifiedById {
		get {
			return (Id)sobj.get('LastModifiedById');
		}
		private set;
	}

	/** 最終更新日 */
	public AppDateTime lastModifiedDate {
		get {
			return AppDateTime.valueOf((Datetime)sobj.get('LastModifiedDate'));
		}
		private set;
	}

	/** System Modstamp */
	public AppDateTime systemModstamp {
		get {
			return AppDateTime.valueOf((Datetime)sobj.get('SystemModstamp'));
		}
		private set;
	}

	/**
	 * エンティティの項目を指定して値を取得する。
	 * @field 取得対象のフィールド
	 * @return エンティティの値
	 */
	public Object getFieldValue(Schema.SObjectField field) {
		return changedValues.containsKey(field) ? changedValues.get(field) : sobj.get(field);
	}

	/**
	 * エンティティの項目名を指定して値を取得する。
	 * @field 取得対象のフィールド名
	 * @return エンティティの値
	 */
	public Object getFieldValue(String fieldName) {
		Schema.SObjectField field = sobjectFieldMap.get(fieldName);
		return changedValues.containsKey(field) ? changedValues.get(field) : sobj.get(field);
	}

	/**
	 * エンティティの項目を指定して値を設定する。
	 * @field 設定対象のフィールド
	 * @value 値
	 */
	public void setFieldValue(Schema.SObjectField field, Object value) {
		changedValues.put(field, value);
	}

	/**
	 * エンティティの項目名を指定して値を設定する。
	 * @fieldName 設定対象のフィールド名
	 * @value 値
	 */
	public void setFieldValue(String fieldName, Object value) {
		Schema.SObjectField field = sobjectFieldMap.get(fieldName);
		changedValues.put(field, value);
	}

	/**
	 * @description Gets the field whose value has been changed
	 * 値を更新した項目を取得する
	 */
	 public Set<Schema.SObjectField> getChangedFieldSet() {
		 return changedValues.keySet();
	 }

	/**
	 * @description Return true if the value of the field has been changed.
	 * 指定した項目の値が変更されている場合はtrueを返却する
	 */
	 public Boolean isChanged(Schema.SObjectField field) {
		 return changedValues.containsKey(field);
	 }

	/**
	 * @description Return true if the value of the field has been changed.
	 * 指定した項目の値が変更されている場合はtrueを返却する
	 */
	 public Boolean isChanged(String fieldName) {
		Schema.SObjectField field = sobjectFieldMap.get(fieldName);
		return changedValues.containsKey(field);
	 }

	/**
	 * @description Set all field values to cloneEntity.
	 * 本エンティティに設定されているすべての項目値をcloneEntityに設定する
	 * Excluded field:
	 *  - ID field
	 *  - List<Object> type field
	 */
	protected void setAllFieldValues(BaseEntity cloneEntity) {

		// SObjectの項目値を設定する
		// setFieldValue()で設定しないとDB保存時に値が保存されないため
		for (String fieldName : this.sObj.getPopulatedFieldsAsMap().keySet()) {
			Schema.SObjectField field = this.sobjectFieldMap.get(fieldName);
			// 変更されている場合は処理対象外
			if (this.changedValues.containsKey(field)){
				continue;
			}
			// 他のオブジェクトへの参照項目は処理対象外
			if (! this.sobjectFieldMap.containsKey(fieldName)) {
				continue;
			}

			Schema.DescribeFieldResult fieldDesc = field.getDescribe();
			// IDは対象外
			if (fieldDesc.getName() == 'Id') {
				continue;
			}

			Object value = this.getFieldValue(fieldName);
			// List<Object>型は対象外
			if (value instanceof List<Object>) {
				continue;
			}

			cloneEntity.setFieldValue(fieldName, this.getFieldValue(fieldName));
		}

		// 変更された項目値を設定する
		// (新規作成時などgetPopulatedFieldsAsMap()の値が取得できない場合があるため)
		for (Schema.SObjectField field : getChangedFieldSet()) {
			Object value = this.getFieldValue(field);
			if (value instanceof List<Object>) {
				cloneEntity.setFieldValue(field, ((List<Object>)value).clone());
			} else {
				cloneEntity.setFieldValue(field, this.getFieldValue(field));
			}
		}
	}

	/**
	 * @returm SObjectの名前空間のプレフィックス   Namespace prefix of SObjects
	 *         名前空間がない場合は空文字となる(nullではない) It will be empty String if there is no namespace(not null)
	 *         Example: tswsp__
	 */
	protected String getNameSpace() {
		return NAMESPACE_PREFIX;
	}
}