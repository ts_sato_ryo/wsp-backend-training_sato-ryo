/**
 * アプリケーションレベルで定義する必要があるクラスを定義するためのクラス
 * 例外などアプリケーション全体で同じ定義を利用する必要があるクラスを本クラスのサブクラスとして定義してください。
 */
public with sharing class App {

	// Comparable IFの比較値
	/** 比較対象より後である */
	public static final Integer COMPARE_AFTER = 1;
	/** 比較対象より前である */
	public static final Integer COMPARE_BEFORE = -1;
	/** 比較対象と同じであれ */
	public static final Integer COMPARE_EQUAL = 0;

 	/**
 	 * 基本クラス
 	 * アプリケーション例外を定義する場合は本クラスを継承して定義してください
 	 */
	public abstract class BaseException extends Exception {
		// エラーコードを返すメソッド
		public abstract String getErrorCode();
	}

	/**
 	 * パラメータが不正であることを示す例外
 	 */
	public class ParameterException extends BaseException {
		private String code = App.ERR_CODE_INVALID_PARAMETER;
		public override String getErrorCode(){ return this.code; }

		/**
		 * コンストラクタ（パラメータ名と値を指定）
		 * パラメータ名と値からエラーメッセージを作成する。
		 */
		public ParameterException(String name, Object value) {
			this();
			setMessageInvalid(name, value);
		}

		/**
		 * コードを設定する
		 */
		public void setCode(String code) {
			this.code = code;
		}

		/**
		 * エラーメッセージを設定する(メッセージ：[name] is invalid.(name=[value]))
		 * @param name パラメータ名
		 * @param value パラメータ値
		 */
		public void setMessageInvalid(String name, Object value) {
			// メッセージ：[name]の値が正しくありません。([name]=[value])
			this.setMessage(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{name, String.valueOf(value)}));
		}

		/**
		 * エラーメッセージを設定する(メッセージ：[name]が設定されていません)
		 * @param name パラメータ名
		 */
		public void setMessageIsBlank(String name) {
			this.setMessage(ComMessage.msg().Com_Err_NullValue(new List<String>{name}));
		}

		/**
		 * エラーメッセージを設定する(文字数オーバー)
		 * @param name パラメータ名
		 * @param max 最大文字列長さ
		 */
		public void setMessageStringLengthOver(String name, Integer max) {
			this.setMessage(ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{name, max.format()}));
		}

	}

	/**
	 * データ型が不正であることを示す例外
	 */
	public class TypeException extends BaseException {
		public override String getErrorCode(){ return App.ERR_CODE_INVALID_TYPE; }

		/**
		 * コンストラクタ（パラメータ名を指定）
		 * パラメータ名からエラーメッセージを作成する。
		 */
		public TypeException(String name, Object value) {
			// TODO エラーメッセージ
			this(name + ' の型が正しくありません。(' + name + '=' + value + ')');
		}
	}

	/**
	 * 不正または不適切な状態でメソッドが呼び出されたことを示す例外
	 */
	public class IllegalStateException extends BaseException {
		private String code = 'ILLEGAL_STATE';
		public override String getErrorCode(){ return this.code; }

		/**
		 * コンストラクタ（メッセージとコードを指定)
		 */
		public IllegalStateException(String code, String message) {
			this(message);
			this.code = code;
		}
	}

	/**
	 * 本来あるべきはずのレコードが見つからなかったことを示す例外
	 */
	public class RecordNotFoundException extends BaseException {
		private String code = App.ERR_CODE_RECORD_NOT_FOUND;
		public override String getErrorCode(){ return this.code; }

		/**
		 * コンストラクタ(エラーコード指定)
		 */
		public RecordNotFoundException(String code, String message) {
			this(message);
			this.code = code;
		}
	}

	/**
	 * 対応していない値が設定されていることを示す例外
	 * (選択リストの値が追加されたが、追加された値に対する処理が実装されていない場合などに利用）
	 */
	public class UnsupportedException extends BaseException {
		private String code = 'UNSUPPORTED_VALUE';
		public override String getErrorCode(){ return this.code; }

		/**
		 * コンストラクタ（メッセージとコードを指定)
		 */
		public UnsupportedException(String code, String message) {
			this(message);
			this.code = code;
		}
	}

	/**
	 * アクセス権限を持たない操作を実行しようとしたことを示す例外
	 */
	public class NoPermissionException extends BaseException {
		private String code = 'NO_PERMISSION';
		public override String getErrorCode(){ return this.code; }

		/**
		 * コンストラクタ（メッセージとコードを指定)
		 */
		public NoPermissionException(String code, String message) {
			this(message);
			this.code = code;
		}
	}

	//==============================================================
	// 以下はエラーコード定義
	//==============================================================
	// アプリ共通
	/** 値が設定されていない */
	public static final String ERR_CODE_NOT_SET_VALUE = 'NOT_SET_VALUE';
	/** 値が設定されている */
	public static final String ERR_CODE_VALUE_IS_SET = 'VALUE_IS_SET';
	/** 値が正しくない */
	public static final String ERR_CODE_INVALID_VALUE = 'INVALID_VALUE';
	/** パラメータが正しくない */
	public static final String ERR_CODE_INVALID_PARAMETER = 'INVALID_PARAMETER';
	/** データ型が不正 */
	public static final String ERR_CODE_INVALID_TYPE = 'INVALID_TYPE';
	/** 本来あるべきはずのレコードが見つからない */
	public static final String ERR_CODE_RECORD_NOT_FOUND = 'RECORD_NOT_FOUND';
	/** 未対応の値が設定されている(選択リストの値など) */
	public static final String ERR_CODE_UNSUPPORTED_VALUE = 'UNSUPPORTED_VALUE';

	// マスタ・会社
	/** エラーコード：会社のデフォルト言語が設定されていない */
	public static final String ERR_CODE_NOT_SET_COMPANY_LANG = 'NOT_SET_COMPANY_LANG';

	// マスタ・社員
	/** エラーコード：社員が見つからない */
	public static final String ERR_CODE_NOT_FOUND_EMP = 'NOT_FOUND_EMP';
	/** エラーコード：社員が無効である */
	public static final String ERR_CODE_INVALID_EMP = 'INVALID_EMP';
	/** エラーコード：社員の上長が設定されていない */
	public static final String ERR_CODE_NOT_SET_EMP_MANAGER = 'NOT_SET_EMP_MANAGER';
	/** エラーコード：社員の上長が見つからない */
	public static final String ERR_CODE_NOT_FOUND_MANAGER = 'NOT_FOUND_EMP_MANAGER';
	/** エラーコード：社員の勤務体系が見つからない */
	public static final String ERR_CODE_NOT_FOUND_WORKINGTYPE = 'NOT_FOUND_EMP_WORKINGTYPE';
	/** エラーコード：社員を特定できない */
	public static final String ERR_CODE_CANNOT_IDENTIFY_EMPLOYEE = 'CANNOT_IDENTIFY_EMPLOYEE';

	// マスタ・カレンダー
	/** エラーコード：対象日のイベントが既に存在する */
	public static final String ERR_CODE_EVENT_EXISTED = 'EVENT_EXISTED';

	// マスタ・勤務体系
	/** エラーコード：勤務体系の年度の開始月が設定されていない */
	public static final String ERR_CODE_NOT_SET_BEGIN_MONTH_OF_YEAR = 'NOT_SET_WORKINGTYPE_BEGIN_MONTH_OF_YEAR';
	/** エラーコード：勤務体系の月度の開始日が設定されていない */
	public static final String ERR_CODE_NOT_SET_BEGIN_DAY_OF_MONTH = 'ATT_NOT_SET_WORKINGTYPE_BEGIN_DAY_OF_MONTH';
	/** エラーコード：勤務体系に設定されている休暇が見つからない */
	public static final String ERR_CODE_NOT_FOUND_WORKINGTYPE_LEAVE = 'NOT_FOUND_WORKINGTYPE_LEAVE';

	// マスタ・短時間勤務設定
	/** エラーコード：遅刻、早退、私用外出の許可する時間の合計が、1日あたりの許可する時間未満 */
	public static final String ERR_CODE_TOTAL_TIME_ALLOWED_LESS_THAN_PER_DAY = 'TOTAL_TIME_ALLOWED_LESS_THAN_PER_DAY';
	/** エラーコード：遅刻、早退、私用外出の許可する時間が、1日あたりの許可する時間を超えている */
	public static final String ERR_CODE_EACH_TIME_ALLOWED_OVER_THAN_PER_DAY = 'EACH_TIME_ALLOWED_OVER_THAN_PER_DAY';

	// マスタ・休暇
	/** エラーコード：指定された休暇が見つからない */
	public static final String ERR_CODE_NOT_FOUND_LEAVE = 'NOT_FOUND_LEAVE';
	/** エラーコード：年次有休休暇が1件のみ登録できる */
	public static final String ERR_CODE_ANNUAL_LEAVE_EXISTED = 'ANNUAL_LEAVE_EXISTED';
	/** エラーコード：[マスタデータ名]は[日付名]で有効ではありません。 */
	public static final String ERR_CODE_NOT_VALID_AFTER_DATE = 'ERR_CODE_NOT_VALID_AFTER_DATE';
	/** エラーコード：[休暇名]が[日付名]で利用されておりません。 */
	public static final String ERR_CODE_LEAVE_NOT_USED_AFTER_DATE = 'ERR_CODE_LEAVE_NOT_USED_AFTER_DATE';
	/** エラーコード：指定された休暇付与が見つからない */
	public static final String ERR_CODE_NOT_FOUND_GRANT = 'NOT_FOUND_GRANT';

	// マスタ・勤務パターン
	/** エラーコード：指定された勤務パターンが見つからない */
	public static final String ERR_CODE_NOT_FOUND_PATTERN = 'NOT_FOUND_PATTERN';

	// マスタ・ジョブ割当
	/** エラーコード：期間が不正 */
	public static final String ERR_CODE_INVALID_PERIOD = 'INVALID_PERIOD';

	// マスタデータ連携
	/** エラーコード：インポート対象のデータ件数が上限を超えている */
	public static final String ERR_CODE_TOO_MANY_IMPORT_RECORDS = 'TOO_MANY_IMPORT_RECORDS';

	// 個人設定
	/** エラーコード：個人設定を特定できない */
	public static final String ERR_CODE_CANNOT_IDENTIFY_PERSONAL_SETTING = 'CANNOT_IDENTIFY_PERSONAL_SETTING';
	public static final String ERR_CODE_SEARCH_CONDITION_EXCEED_MAX_SIZE = 'ERR_CODE_SEARCH_CONDITION_EXCEED_MAX_SIZE';

	// 承認申請
	/** エラーコード：申請が見つからない */
	public static final String ERR_CODE_NOT_FOUND_REQUEST = 'APPR_NOT_FOUND_REQUEST';
	/** エラーコード：添付ファイルが見つからない */
	public static final String ERR_CODE_NOT_FOUND_ATTACHMENT = 'APPR_NOT_FOUND_ATTACHMENT';
	/** エラーコード：許可されていない申請 */
	public static final String ERR_CODE_NOT_PERMITTID_REQUEST = 'APPR_NOT_PERMITTID_REQUEST';
	/** エラーコード：申請できない */
	public static final String ERR_CODE_CANNOT_REQUEST = 'APPR_CANNOT_REQUEST';
	/** エラーコード：承認内容変更申請できない */
	public static final String ERR_CODE_CANNOT_REAPPLY = 'APPR_CANNOT_REAPPLY';
	/** エラーコード：申請を承認できない */
	public static final String ERR_CODE_CANNOT_APPROVE_REQUEST = 'APPR_CANNOT_APPROVE_REQUEST';
	/** エラーコード：申請を却下できない */
	public static final String ERR_CODE_CANNOT_REJECT_REQUEST = 'APPR_CANNOT_REJECT_REQUEST';
	/** エラーコード：申請を取消しできない */
	public static final String ERR_CODE_CANNOT_CANCEL_REQUEST = 'APPR_CANNOT_CANCEL_REQUEST';
	/** エラーコード：承認済み申請を取消できない */
	public static final String ERR_CODE_CANNOT_CANCEL_APPROVAL = 'APPR_CANNOT_CANCEL_APPROVAL';
	/** エラーコード：申請を取り消す権限がない */
	public static final String ERR_CODE_NO_PERMISSIONS_CANCEL_REQUEST = 'APPR_NO_PERMISSIONS_CANCEL_REQUEST';
	/** エラーコード：申請を削除できない */
	public static final String ERR_CODE_CANNOT_REMOVE_REQUEST = 'APPR_CANNOT_REMOVE_REQUEST';
	/** エラーコード：再申請元申請が見つからない */
	public static final String ERR_CODE_NOT_FOUND_ORIGINAL_REQUEST = 'APPR_NOT_FOUND_ORIGINAL_REQUEST';
	/** エラーコード：承認者が見つからない */
	public static final String ERR_CODE_NOT_FOUND_APPROVER = 'APPR_NOT_FOUND_APPROVER';
	/** エラーコード：申請元の社員のSalesforceユーザが無効 */
	public static final String ERR_CODE_INACTIVE_USERS_REQUEST = 'APPR_INACTIVE_USERS_REQUEST';

	// 勤怠
	/** エラーコード：勤怠機能が利用できない */
	public static final String ERR_CODE_CANNOT_USE_ATTENDANCE = 'CANNOT_USE_ATTENDANCE';
	/** エラーコード：勤怠サマリーが見つからない */
	public static final String ERR_CODE_NOT_FOUND_ATT_SUMMARY = 'ATT_NOT_FOUND_SUMMARY';
	public static final String ERR_CODE_ATT_INVALID_LOCKED = 'ATT_INVALID_LOCKED';
	public static final String ERR_CODE_ATT_STAMP_FAILED = 'ATT_STAMP_FAILED';
	public static final String ERR_CODE_ATT_INVALID_ABSENT = 'ATT_INVALID_ABSENT';
	public static final String ERR_CODE_ATT_INVALID_DIRECT = 'ATT_INVALID_DIRECT';
	public static final String ERR_CODE_ATT_INVALID_DAY_TYPE = 'ATT_INVALID_DAY_TYPE';
	public static final String ERR_CODE_ATT_LEAVE_DAY_OVER = 'ATT_LEAVE_DAY_OVER';
	public static final String ERR_CODE_ATT_LEAVE_REQUESTING = 'ATT_LEAVE_REQUESTING';
	public static final String ERR_CODE_ATT_HOLIDAYWORK_REQUESTING = 'ATT_HOLIDAYWORK_REQUESTING';
	public static final String ERR_CODE_ATT_ABSENCE_REQUESTING = 'ATT_ABSENCE_REQUESTING';
	public static final String ERR_CODE_ATT_LEAVE_CONFLICT = 'ATT_LEAVE_CONFLICT';
	public static final String ERR_CODE_ATT_LEAVE_AM_ONLY1 = 'ATT_LEAVE_AM_ONLY1';
	public static final String ERR_CODE_ATT_LEAVE_PM_ONLY1 = 'ATT_LEAVE_PM_ONLY1';
	public static final String ERR_CODE_ATT_LEAVE_OUT_CONTRACT = 'ATT_LEAVE_OUT_CONTRACT';
	public static final String ERR_CODE_ATT_INVALID_START_END_TIME = 'ATT_INVALID_START_END_TIME';
	public static final String ERR_CODE_ATT_NOT_ALLOW_INP_WORKTIME = 'ATT_NOT_ALLOW_INP_WORKTIME';
	/** 勤怠計算に必要な設定情報が見つからない */
	public static final String ERR_CODE_ATT_NOT_FOUND_ATT_CALC_CONFIG = 'ATT_NOT_FOUND_ATT_CALC_CONFIG';
	/** 休憩時間が不正 */
	public static final String ERR_CODE_ATT_INVALID_REST_TIME = 'ATT_INVALID_REST_TIME';
	/** 申請タイプが不正 */
	public static final String ERR_CODE_ATT_INVALID_REQUEST_TYPE = 'ATT_INVALID_REQUEST_TYPE';
	/** 申請日時が不正 */
	public static final String ERR_CODE_ATT_INVALID_REQUEST_DATETIME = 'ATT_INVALID_REQUEST_DATETIME';
	/** 休日出勤申請が不正 */
	public static final String ERR_CODE_ATT_INVALID_REQUEST_HOLIDAY_WORK = 'ATT_INVALID_REQUEST_HOLIDAY_WORK';
	/** 時間外勤務申請が不正 */
	public static final String ERR_CODE_ATT_INVALID_REQUEST_OVAR_TIME_WORK = 'ATT_INVALID_REQUEST_OVER_TIME_WORK';
	/** 休暇申請が不正 */
	public static final String ERR_CODE_ATT_INVALID_REQUEST_LEAVE = 'ATT_INVALID_REQUEST_LEAVE';
	/** 直行直帰申請が不正 */
	public static final String ERR_CODE_ATT_INVALID_REQUEST_DIRECT = 'ATT_INVALID_REQUEST_DIRECT';
	/** 同じ申請が既に申請されました */
	public static final String ERR_CODE_ATT_REQUET_REQUESTING = 'ATT_REQUET_REQUESTING';
	/** 申請中の明細があります */
	public static final String ERR_CODE_ATT_RECORD_REQUESTING = 'ATT_RECORD_REQUESTING';
	/** 勤務表が再計算する必要あり */
	public static final String ERR_CODE_ATT_SUMMARY_DIRTY = 'ATT_SUMMARY_DIRTY';
	/** 出勤時刻が正しくない */
	public static final String ERR_CODE_ATT_INVALID_START_TIME = 'ATT_INVALID_START_TIME';
	/** 退勤時刻が正しくない */
	public static final String ERR_CODE_ATT_INVALID_END_TIME = 'ATT_INVALID_END_TIME';
	/** 実効勤務パターンの始業時刻が正しくない */
	public static final String ERR_CODE_ATT_INVALID_OUT_WORKING_PATTERN_START_TIME = 'ERR_CODE_ATT_INVALID_OUT_WORKING_PATTERN_START_TIME';
	/** 実効勤務パターンの終業時刻が正しくない */
	public static final String ERR_CODE_ATT_INVALID_OUT_WORKING_PATTERN_END_TIME = 'ERR_CODE_ATT_INVALID_OUT_WORKING_PATTERN_END_TIME';
	/** 実効勤務パターンが正しくない */
	public static final String ERR_CODE_ATT_INVALID_OUT_WORKING_PATTERN = 'ERR_CODE_ATT_INVALID_OUT_WORKING_PATTERN';
	/** 勤務日の勤務時刻が正しくない */
	public static final String ERR_CODE_ATT_INVALID_WORKDAY_TIME = 'ATT_INVALID_WORKDAY_TIME';
	/** 休日の勤務時刻が正しくない */
	public static final String ERR_CODE_ATT_INVALID_HOLIDAY_TIME = 'ATT_INVALID_HOLIDAY_TIME';
	/** 項目未設定 */
	public static final String ERR_CODE_ATT_FIELD_NULL = 'ATT_FIELD_NULL';
	/** 項目が０以下 */
	public static final String ERR_CODE_ATT_FIELD_ZERO = 'ATT_FIELD_LESS_ZERO';
	/** 時間重複 */
	public static final String ERR_CODE_ATT_TIME_OVERLAP = 'ATT_TIME_OVERLAP';
	/** 開始時刻と終了時刻が逆 */
	public static final String ERR_CODE_ATT_TIME_START_AFTER_END = 'ATT_TIME_START_AFTER_END';
	/** 開始時刻と終了時刻が共に登録エラー */
	public static final String ERR_CODE_ATT_TIME_BOTH_START_END = 'ATT_TIME_BOTH_START_END';
	/** フレックス時間帯にコアタイムが含まれない */
	public static final String ERR_CODE_ATT_NOT_INCLUDE_CORETIME = 'ATT_NOT_INCLUDE_CORETIME';
	/** 所定労働時間と始業終業時刻の計算結果が合わないエラー */
	public static final String ERR_CODE_ATT_CONTRACTED_HOURS_NOT_MATCH = 'ATT_CONTRACTED_HOURS_NOT_MATCH';
	/** フレックス時間帯と所定労働時間の計算結果が合わないエラーを作成 */
	public static final String ERR_CODE_ATT_FlEX_WORK_TIME_NOT_MATCH = 'ATT_FlEX_WORK_TIME_NOT_MATCH';
	/** 再出勤打刻時に休憩回数の上限を超過しているエラー */
	public static final String ERR_CODE_ATT_RESTART_STAMP_OVER_REST_LIMIT = 'ATT_RESTART_STAMP_OVER_REST_LIMIT';
	/** 既に休暇申請が存在する日に対して勤務時間を入力しようとしている */
	public static final String ERR_CODE_ATT_LEAVE_REQUESTS_ALREADY_EXIST = 'ERR_CODE_ATT_LEAVE_REQUESTS_ALREADY_EXIST';
	/** 申請中の直行直帰申請が存在する日に対して勤務時間を入力しようとしている */
	public static final String ERR_CODE_ATT_DIRECT_REQUESTING = 'ERR_CODE_ATT_DIRECT_REQUESTING';
	/** 不正な欠勤申請 */
	public static final String ERR_CODE_ATT_INVALID_ABSENCE_REQUEST = 'ATT_INVALID_ABSENCE_REQUEST';
	/** 不正な休日出勤申請 */
	public static final String ERR_CODE_ATT_INVALID_HOLIDAY_WORK_REQUEST = 'ATT_INVALID_HOLIDAY_WORK_REQUEST';
	/** 不正な直行直帰申請 */
	public static final String ERR_CODE_ATT_INVALID_DIRECT_REQUEST = 'ATT_INVALID_DIRECT_REQUEST';
	/** 不正な早朝勤務申請 */
	public static final String ERR_CODE_ATT_INVALID_EARLY_START_WORK_REQUEST = 'ATT_INVALID_EARLY_START_WORK_REQUEST';
	/** 不正な残業申請 */
	public static final String ERR_CODE_ATT_INVALID_OVERTIME_WORK_REQUEST = 'ATT_INVALID_OVERTIME_WORK_REQUEST';
	/** 不正な勤務時間変更申請 */
	public static final String ERR_CODE_ATT_INVALID_PATTERN_REQUEST = 'ATT_INVALID_PATTERN_REQUEST';
	// 有休
	/** 残日数が足りない */
	public static final String ERR_CODE_ATT_DAYS_LEFT_NOT_ENOUGH = 'ATT_DAYS_LEFT_NOT_ENOUGH';
	/** 残日数が足りない（勤務確定済みの場合） */
	public static final String ERR_CODE_ATT_DAYS_LEFT_NOT_ENOUGH_LOCKED_ATT_SUMMARY = 'ATT_DAYS_LEFT_NOT_ENOUGH_LOCKED_ATT_SUMMARY';

	// 工数
	/** エラーコード：工数管理機能が利用できない */
	public static final String ERR_CODE_CANNOT_USE_TIME_TRACKING = 'CANNOT_USE_TIME_TRACKING';
	/** エラーコード：工数確定申請が利用できない */
	public static final String ERR_CODE_CANNOT_USE_TIME_REQUEST = 'CANNOT_USE_TIME_REQUEST';
	/** エラーコード：社員の工数設定が設定されていない */
	public static final String ERR_CODE_NOT_SET_TIME_SETTING = 'TIME_NOT_SET_TIME_SETTING';
	/** エラーコード：社員の工数設定が見つからない */
	public static final String ERR_CODE_NOT_FOUND_TIME_SETTING = 'TIME_NOT_FOUND_TIME_SETTING';
	/** エラーコード：工数サマリーが複数存在する */
	public static final String ERR_CODE_MULTIPLE_SUMMARIES = 'TIME_MULTIPLE_SUMMARIES';
	/** エラーコード：工数サマリーがロックされている */
	public static final String ERR_CODE_TIME_SUMMARY_LOCKED = 'TIME_SUMMARY_LOCKED';
	/** エラーコード：不正なタスクが存在する */
	public static final String ERR_CODE_ILLEGAL_TASKS = 'ERR_CODE_ILLEGAL_TASKS';
	/** The specified Parent is different from existing Parent and changing parent isn't allowed. */
	public static final String ERR_CODE_PARENT_CHANGE = 'ERR_CODE_PARENT_CHANGE';
	/** エラーコード：親ジョブに自分自身を設定しているエラー */
	public static final String ERR_CODE_PARENT_OF_ITSELF = 'ERR_CODE_PARENT_OF_ITSELF';
	/** The specified Parent is also an offspring of the record (Parent is also the child) */
	public static final String ERR_CODE_PARENTING_CHILD = 'ERR_CODE_PARENTING_CHILD';
	/** Exceeds the maximum allowed layer of hierarchy */
	public static final String ERR_CODE_EXCEED_MAX_HIERARCHY = 'ERR_CODE_EXCEED_MAX_HIERARCHY';

	// Planner
	/** エラーコード：予定が見つからない */
	public static final String ERR_CODE_PLANNER_NOT_FOUND_PLAN_EVENT = 'PLANNER_NOT_FOUND_PLAN_EVENT';

	// 経費
	/** エラーコード：経費精算が見つからない */
	public static final String ERR_CODE_NOT_FOUND_EXP_REPORT = 'EXP_NOT_FOUND_REPORT';
	/** エラーコード：経費精算が利用できない */
	public static final String ERR_CODE_CANNOT_USE_EXPENSE = 'EXP_CANNOT_USE';
	/** Error Code: Expense Request Function is not available */
	public static final String ERR_CODE_CANNOT_USE_EXPENSE_REQUEST = 'EXP_REQUEST_CANNOT_USE';
	/** Extended Items between the Report/Record data and Master Data Mismatched Error Code */
	public static final String ERR_CODE_EXTENDED_ITEM_MISMATCHED = 'EXP_EXTENDED_ITEM_MISMATCHED';
	/** エラーコード：基準通貨が存在しない */
	public static final String ERR_CODE_NO_BASE_CURRENCY = 'EXP_NO_BASE_CURRENCY';
	/** エラーコード：証憑が存在しない */
	public static final String ERR_CODE_NO_EVIDENCE = 'EXP_NO_EVIDENCE';
	/** エラーコード：証憑が存在する */
	public static final String ERR_CODE_EVIDENCE_FOUND = 'EXP_EVIDENCE_FOUND';
	/** エラーコード：既に申請されている */
	public static final String ERR_CODE_EXP_REQUEST_REQUESTING = 'EXP_REQUEST_REQUESTING';
	/** エラーコード：既に承認されている */
	public static final String ERR_CODE_EXP_REQUEST_APPROVED = 'EXP_REQUEST_APROVED';
	/** Expense Type Link Error Code*/
	public static final String ERR_CODE_EXP_TYPE_LINK = 'EXP_TYPE_LINK_ERROR';
	/** Accounting Period Error Code */
	public static final String ERR_CODE_NOT_FOUND_ACCOUNTING_PERIOD = 'EXP_NOT_FOUND_ACCOUNTING_PERIOD';
	/** Report Type Not Found Error Code */
	public static final String ERR_CODE_NOT_FOUND_REPORT_TYPE = 'EXP_NOT_FOUND_REPORT_TYPE';
	/** Report Type Not Specified Error Code */
	public static final String ERR_CODE_NO_REPORT_TYPE = 'EXP_NO_REPORT_TYPE';
	/** Foreign Currency Related Error Code */
	public static final String ERR_CODE_EXP_FOREIGN_CURRENCY = 'EXP_FOREIGN_CURRENCY';
}