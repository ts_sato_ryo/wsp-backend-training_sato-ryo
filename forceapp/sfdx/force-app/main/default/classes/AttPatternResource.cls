/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 勤務パターンレコード操作APIを実装するクラス
 */
public with sharing class AttPatternResource {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 勤務パターンレコードを表すクラス
	 */
	public class AttPattern implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 勤務パターン名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 勤務パターン名(言語0) */
		public String name_L0;
		/** 勤務パターン名(言語1) */
		public String name_L1;
		/** 勤務パターン名(言語2) */
		public String name_L2;
		/** 勤務パターンコード */
		public String code;
		/** 会社レコードID */
		public String companyId;
		/** 有効開始日 */
		public String validDateFrom;
		/** 失効日 */
		public String validDateTo;
		/** 並び順 */
		public String order;
		/** 労働時間制 */
		public String workSystem;
		/** 午前半休を使用する */
		public Boolean useAMHalfDayLeave;
		/** 午後半休を使用する */
		public Boolean usePMHalfDayLeave;
		/** 始業時刻 */
		public Integer startTime;
		/** 終業時刻 */
		public Integer endTime;
		/** 所定労働時間 */
		public Integer contractedWorkHours;
		/** 所定休憩1開始時刻 */
		public Integer rest1StartTime;
		/** 所定休憩1終了時刻 */
		public Integer rest1EndTime;
		/** 所定休憩2開始時刻 */
		public Integer rest2StartTime;
		/** 所定休憩2終了時刻 */
		public Integer rest2EndTime;
		/** 所定休憩3開始時刻 */
		public Integer rest3StartTime;
		/** 所定休憩3終了時刻 */
		public Integer rest3EndTime;
		/** 所定休憩4開始時刻 */
		public Integer rest4StartTime;
		/** 所定休憩4終了時刻 */
		public Integer rest4EndTime;
		/** 所定休憩5開始時刻 */
		public Integer rest5StartTime;
		/** 所定休憩5終了時刻 */
		public Integer rest5EndTime;
		/** 午前半休始業時刻 */
		public Integer amStartTime;
		/** 午前半休終業時刻 */
		public Integer amEndTime;
		/** 午前半休所定労働時間 */
		public Integer amContractedWorkHours;
		/** 午前半休所定休憩1開始時刻 */
		public Integer amRest1StartTime;
		/** 午前半休所定休憩1終了時刻 */
		public Integer amRest1EndTime;
		/** 午前半休所定休憩2開始時刻 */
		public Integer amRest2StartTime;
		/** 午前半休所定休憩2終了時刻 */
		public Integer amRest2EndTime;
		/** 午前半休所定休憩3開始時刻 */
		public Integer amRest3StartTime;
		/** 午前半休所定休憩3終了時刻 */
		public Integer amRest3EndTime;
		/** 午前半休所定休憩4開始時刻 */
		public Integer amRest4StartTime;
		/** 午前半休所定休憩4終了時刻 */
		public Integer amRest4EndTime;
		/** 午前半休所定休憩5開始時刻 */
		public Integer amRest5StartTime;
		/** 午前半休所定休憩5終了時刻 */
		public Integer amRest5EndTime;
		/** 午後半休始業時刻 */
		public Integer pmStartTime;
		/** 午後半休終業時刻 */
		public Integer pmEndTime;
		/** 午後半休所定労働時間 */
		public Integer pmContractedWorkHours;
		/** 午後半休所定休憩1開始時刻 */
		public Integer pmRest1StartTime;
		/** 午後半休所定休憩1終了時刻 */
		public Integer pmRest1EndTime;
		/** 午後半休所定休憩2開始時刻 */
		public Integer pmRest2StartTime;
		/** 午後半休所定休憩2終了時刻 */
		public Integer pmRest2EndTime;
		/** 午後半休所定休憩3開始時刻 */
		public Integer pmRest3StartTime;
		/** 午後半休所定休憩3終了時刻 */
		public Integer pmRest3EndTime;
		/** 午後半休所定休憩4開始時刻 */
		public Integer pmRest4StartTime;
		/** 午後半休所定休憩4終了時刻 */
		public Integer pmRest4EndTime;
		/** 午後半休所定休憩5開始時刻 */
		public Integer pmRest5StartTime;
		/** 午後半休所定休憩5終了時刻 */
		public Integer pmRest5EndTime;
		/** 出勤時刻の境界時刻 */
		public Integer boundaryOfStartTime;
		/** 退勤時刻の境界時刻 */
		public Integer boundaryOfEndTime;

		/**
		 * パラメータを検証する
		 * 他の項目の必須チェックはAttConfigValidatorで実施する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}
			// 表示順
			if (String.isNotBlank(this.order)) {
				AppParser.validateParseInteger(MESSAGE.Admin_Lbl_Order, this.order.trim());
			}
			// 有効開始日
			if (String.isNotBlank(this.validDateFrom)) {
				try {
					AppDate.parse(this.validDateFrom);
				} catch (Exception e) {
					throw new App.ParameterException('validDateFrom', this.validDateFrom);
				}
			}
			// 失効日
			if (String.isNotBlank(this.validDateTo)) {
				try {
					AppDate.parse(this.validDateTo);
				} catch (Exception e) {
					throw new App.ParameterException('validDateTo', this.validDateTo);
				}
			}
		}

		/**
		 * パラメータの値を設定した勤務パターンエンティティを作成する
		 * @param paramMap リクエストパラメータのMap
		 * @return 勤務パターンエンティティ
		 */
		public AttPatternEntity createPatternEntity(Map<String, Object> paramMap) {

			AttPatternEntity e;

			// IDが設定されていれば更新
			Boolean isUpdate = this.id != null;

			if (isUpdate) {
				// 更新の場合は既存の値を取得する
				e = new AttPatternRepository2().getPatternById(this.id);
				if (e == null) {
					throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
				}
			} else {
				// 新規の場合はエンティティを作成する
				e = new AttPatternEntity();
			}
			if (paramMap.containsKey('name_L0')) {
				e.name = this.name_L0;
				e.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				e.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				e.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('code')) {
				e.code = this.code;
			}
			if (paramMap.containsKey('companyId') && !isUpdate) {
				// 更新時は設定しない
				e.companyId = this.companyId;
			}
			if (paramMap.containsKey('validDateFrom')) {
				e.validfrom = AppDate.valueOf(this.validDateFrom);
			}
			if (e.validFrom == null) {
				// 有効開始日のデフォルトは実行日
				e.validFrom = AppDate.today();
			}
			if (paramMap.containsKey('validDateTo')) {
				e.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (e.validTo == null) {
				// 失効日のデフォルト
				e.validTo = ValidPeriodEntity.VALID_TO_MAX;
			}
			if (paramMap.containsKey('order')) {
				e.order = AppConverter.intValue(this.order);
			}
			if (paramMap.containsKey('workSystem')) {
				e.workSystem = AttWorkSystem.valueOf(this.workSystem);
			}
			if (paramMap.containsKey('useAMHalfDayLeave')) {
				e.useAMHalfDayLeave = this.useAMHalfDayLeave;
			}
			if (paramMap.containsKey('usePMHalfDayLeave')) {
				e.usePMHalfDayLeave = this.usePMHalfDayLeave;
			}

			// 勤務時間系
			if (paramMap.containsKey('startTime')) {
				e.startTime = AttTime.valueOf(this.startTime);
			}
			if (paramMap.containsKey('endTime')) {
				e.endTime = AttTime.valueOf(this.endTime);
			}
			if (paramMap.containsKey('contractedWorkHours')) {
				e.contractedWorkHours = AttDailyTime.valueOf(this.contractedWorkHours);
			}
			if (paramMap.containsKey('rest1StartTime')) {
				e.rest1StartTime = AttTime.valueOf(this.rest1StartTime);
			}
			if (paramMap.containsKey('rest1EndTime')) {
				e.rest1EndTime = AttTime.valueOf(this.rest1EndTime);
			}
			if (paramMap.containsKey('rest2StartTime')) {
				e.rest2StartTime = AttTime.valueOf(this.rest2StartTime);
			}
			if (paramMap.containsKey('rest2EndTime')) {
				e.rest2EndTime = AttTime.valueOf(this.rest2EndTime);
			}
			if (paramMap.containsKey('rest3StartTime')) {
				e.rest3StartTime = AttTime.valueOf(this.rest3StartTime);
			}
			if (paramMap.containsKey('rest3EndTime')) {
				e.rest3EndTime = AttTime.valueOf(this.rest3EndTime);
			}
			if (paramMap.containsKey('rest4StartTime')) {
				e.rest4StartTime = AttTime.valueOf(this.rest4StartTime);
			}
			if (paramMap.containsKey('rest4EndTime')) {
				e.rest4EndTime = AttTime.valueOf(this.rest4EndTime);
			}
			if (paramMap.containsKey('rest5StartTime')) {
				e.rest5StartTime = AttTime.valueOf(this.rest5StartTime);
			}
			if (paramMap.containsKey('rest5EndTime')) {
				e.rest5EndTime = AttTime.valueOf(this.rest5EndTime);
			}
			if (paramMap.containsKey('amStartTime')) {
				e.amStartTime = AttTime.valueOf(this.amStartTime);
			}
			if (paramMap.containsKey('amEndTime')) {
				e.amEndTime = AttTime.valueOf(this.amEndTime);
			}
			if (paramMap.containsKey('amContractedWorkHours')) {
				e.amContractedWorkHours = AttDailyTime.valueOf(this.amContractedWorkHours);
			}
			if (paramMap.containsKey('amRest1StartTime')) {
				e.amRest1StartTime = AttTime.valueOf(this.amRest1StartTime);
			}
			if (paramMap.containsKey('amRest1EndTime')) {
				e.amRest1EndTime = AttTime.valueOf(this.amRest1EndTime);
			}
			if (paramMap.containsKey('amRest2StartTime')) {
				e.amRest2StartTime = AttTime.valueOf(this.amRest2StartTime);
			}
			if (paramMap.containsKey('amRest2EndTime')) {
				e.amRest2EndTime = AttTime.valueOf(this.amRest2EndTime);
			}
			if (paramMap.containsKey('amRest3StartTime')) {
				e.amRest3StartTime = AttTime.valueOf(this.amRest3StartTime);
			}
			if (paramMap.containsKey('amRest3EndTime')) {
				e.amRest3EndTime = AttTime.valueOf(this.amRest3EndTime);
			}
			if (paramMap.containsKey('amRest4StartTime')) {
				e.amRest4StartTime = AttTime.valueOf(this.amRest4StartTime);
			}
			if (paramMap.containsKey('amRest4EndTime')) {
				e.amRest4EndTime = AttTime.valueOf(this.amRest4EndTime);
			}
			if (paramMap.containsKey('amRest5StartTime')) {
				e.amRest5StartTime = AttTime.valueOf(this.amRest5StartTime);
			}
			if (paramMap.containsKey('amRest5EndTime')) {
				e.amRest5EndTime = AttTime.valueOf(this.amRest5EndTime);
			}
			if (paramMap.containsKey('pmStartTime')) {
				e.pmStartTime = AttTime.valueOf(this.pmStartTime);
			}
			if (paramMap.containsKey('pmEndTime')) {
				e.pmEndTime = AttTime.valueOf(this.pmEndTime);
			}
			if (paramMap.containsKey('pmContractedWorkHours')) {
				e.pmContractedWorkHours = AttDailyTime.valueOf(this.pmContractedWorkHours);
			}
			if (paramMap.containsKey('pmRest1StartTime')) {
				e.pmRest1StartTime = AttTime.valueOf(this.pmRest1StartTime);
			}
			if (paramMap.containsKey('pmRest1EndTime')) {
				e.pmRest1EndTime = AttTime.valueOf(this.pmRest1EndTime);
			}
			if (paramMap.containsKey('pmRest2StartTime')) {
				e.pmRest2StartTime = AttTime.valueOf(this.pmRest2StartTime);
			}
			if (paramMap.containsKey('pmRest2EndTime')) {
				e.pmRest2EndTime = AttTime.valueOf(this.pmRest2EndTime);
			}
			if (paramMap.containsKey('pmRest3StartTime')) {
				e.pmRest3StartTime = AttTime.valueOf(this.pmRest3StartTime);
			}
			if (paramMap.containsKey('pmRest3EndTime')) {
				e.pmRest3EndTime = AttTime.valueOf(this.pmRest3EndTime);
			}
			if (paramMap.containsKey('pmRest4StartTime')) {
				e.pmRest4StartTime = AttTime.valueOf(this.pmRest4StartTime);
			}
			if (paramMap.containsKey('pmRest4EndTime')) {
				e.pmRest4EndTime = AttTime.valueOf(this.pmRest4EndTime);
			}
			if (paramMap.containsKey('pmRest5StartTime')) {
				e.pmRest5StartTime = AttTime.valueOf(this.pmRest5StartTime);
			}
			if (paramMap.containsKey('pmRest5EndTime')) {
				e.pmRest5EndTime = AttTime.valueOf(this.pmRest5EndTime);
			}
			if (paramMap.containsKey('boundaryOfStartTime')) {
				e.boundaryOfStartTime = AttTime.valueOf(this.boundaryOfStartTime);
			}
			if (paramMap.containsKey('boundaryOfEndTime')) {
				e.boundaryOfEndTime = AttTime.valueOf(this.boundaryOfEndTime);
			}
			return e;
		}
	}

	/**
	 * @description ルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目 */
		public String name;
	}

	/**
	 * @description 勤務パターンレコード作成結果レスポンス
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したレコードID */
		public String id;
	}

	/**
	 * @description 勤務パターンレコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_PATTERN;

		/**
		 * @description 勤務パターンレコードを1件作成する
		 * @param  req リクエストパラメータ(AttPattern)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			AttPatternResource.AttPattern param = (AttPatternResource.AttPattern)req.getParam(AttPatternResource.AttPattern.class);

			// パラメータのバリデーション
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 勤務パターンエンティティの作成
			AttPatternEntity entity = param.createPatternEntity(req.getParamMap());

			// 保存する
			AttPatternService service = new AttPatternService();
			Repository.SaveResult result = service.savePatternEntity(entity);

			// 成功レスポンスをセットする
			SaveResult res = new SaveResult();
			res.id = result.details[0].Id;
			return res;
		}
	}

	/**
	 * @desctiprion 勤務パターンレコード更新処理を実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_PATTERN;

		/**
		 * @description 勤務パターンレコードを1件更新する
		 * @param req リクエストパラメータ(AttPattern)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			AttPatternResource.AttPattern param = (AttPatternResource.AttPattern)req.getParam(AttPatternResource.AttPattern.class);

			// IDがブランクの場合はエラー
			if (String.isBlank(param.id)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('id');
				throw e;
			}

			// パラメータのバリデーション
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 勤務パターンエンティティの作成
			AttPatternEntity entity = param.createPatternEntity(req.getParamMap());

			// 更新する
			AttPatternService service = new AttPatternService();
			Repository.SaveResult result = service.savePatternEntity(entity);

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 勤務パターンレコードの削除パラメータを定義するクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;
	}

	/**
	 * 勤務パターンレコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_PATTERN;

		/**
		 * @description 勤務パターンレコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);

			// IDがブランクの場合はエラー
			if (String.isBlank(param.id)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('id');
				throw e;
			}

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new AttPatternRepository2().deleteEntity(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 勤務パターンレコードの検索条件を格納するクラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 会社レコードID */
		public String companyId;
		/** 対象日(YYYY-MM-DD) */
		public String targetDate;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// レコードID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}
			// 会社レコードID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}
			// 対象日
			if (String.isNotBlank(this.targetDate)) {
				try {
					AppDate.parse(this.targetDate);
				} catch (Exception e) {
					throw new App.ParameterException('targetDate', this.targetDate);
				}
			}
		}
	}

	/**
	 * @description 勤務パターンレコードの検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public AttPattern[] records;
	}

	/**
	 * @description 勤務パターンレコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 勤務パターンレコードを検索する
		 * @param req リクエストパラメータ(SearchRequest)
		 * @return レスポンス(SearchResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchRequest param = (SearchRequest)req.getParam(SearchRequest.class);

			// パラメータのバリデーション
			param.validate();

			// 検索フィルタを作成(パラメータで指定されている検索条件を設定)
			AttPatternRepository2.SearchFilter filter = new AttPatternRepository2.SearchFilter();
			if (String.isNotBlank(param.id)) {
				filter.ids =  new Set<Id>{param.id};
			}
			filter.companyId = param.companyId;
			filter.targetDate = param.targetDate != null ? AppDate.valueOf(param.targetDate) : null;

			// 検索
			List<AttPatternEntity> entityList = new AttPatternRepository2().searchEntityList(filter);

			// レスポンスクラスに変換
			List<AttPattern> patternList = new List<AttPattern>();
			for (AttPatternEntity ent : entityList) {
				AttPattern p = new AttPattern();
				p.id = ent.id;
				p.name = ent.nameL.getValue();
				p.name_L0 = ent.nameL.valueL0;
				p.name_L1 = ent.nameL.valueL1;
				p.name_L2 = ent.nameL.valueL2;
				p.code = ent.code;
				p.companyId = ent.companyId;
				p.order = String.valueOf(ent.order);
				p.validDateFrom = ent.validFrom.format();
				p.validDateTo = ent.validTo.format();
				p.workSystem = AttWorkSystem.getValue(ent.workSystem);
				p.useAMHalfDayLeave = ent.useAMHalfDayLeave;
				p.usePMHalfDayLeave = ent.usePMHalfDayLeave;
				// 勤務時間系
				p.startTime = AppConverter.intValue(ent.startTime);
				p.endTime = AppConverter.intValue(ent.endTime);
				p.contractedWorkHours = AppConverter.intValue(ent.contractedWorkHours);
				p.rest1StartTime = AppConverter.intValue(ent.rest1StartTime);
				p.rest1EndTime = AppConverter.intValue(ent.rest1EndTime);
				p.rest2StartTime = AppConverter.intValue(ent.rest2StartTime);
				p.rest2EndTime = AppConverter.intValue(ent.rest2EndTime);
				p.rest3StartTime = AppConverter.intValue(ent.rest3StartTime);
				p.rest3EndTime = AppConverter.intValue(ent.rest3EndTime);
				p.rest4StartTime = AppConverter.intValue(ent.rest4StartTime);
				p.rest4EndTime = AppConverter.intValue(ent.rest4EndTime);
				p.rest5StartTime = AppConverter.intValue(ent.rest5StartTime);
				p.rest5EndTime = AppConverter.intValue(ent.rest5EndTime);
				p.amStartTime = AppConverter.intValue(ent.amStartTime);
				p.amEndTime = AppConverter.intValue(ent.amEndTime);
				p.amContractedWorkHours = AppConverter.intValue(ent.amContractedWorkHours);
				p.amRest1StartTime = AppConverter.intValue(ent.amRest1StartTime);
				p.amRest1EndTime = AppConverter.intValue(ent.amRest1EndTime);
				p.amRest2StartTime = AppConverter.intValue(ent.amRest2StartTime);
				p.amRest2EndTime = AppConverter.intValue(ent.amRest2EndTime);
				p.amRest3StartTime = AppConverter.intValue(ent.amRest3StartTime);
				p.amRest3EndTime = AppConverter.intValue(ent.amRest3EndTime);
				p.amRest4StartTime = AppConverter.intValue(ent.amRest4StartTime);
				p.amRest4EndTime = AppConverter.intValue(ent.amRest4EndTime);
				p.amRest5StartTime = AppConverter.intValue(ent.amRest5StartTime);
				p.amRest5EndTime = AppConverter.intValue(ent.amRest5EndTime);
				p.pmStartTime = AppConverter.intValue(ent.pmStartTime);
				p.pmEndTime = AppConverter.intValue(ent.pmEndTime);
				p.pmContractedWorkHours = AppConverter.intValue(ent.pmContractedWorkHours);
				p.pmRest1StartTime = AppConverter.intValue(ent.pmRest1StartTime);
				p.pmRest1EndTime = AppConverter.intValue(ent.pmRest1EndTime);
				p.pmRest2StartTime = AppConverter.intValue(ent.pmRest2StartTime);
				p.pmRest2EndTime = AppConverter.intValue(ent.pmRest2EndTime);
				p.pmRest3StartTime = AppConverter.intValue(ent.pmRest3StartTime);
				p.pmRest3EndTime = AppConverter.intValue(ent.pmRest3EndTime);
				p.pmRest4StartTime = AppConverter.intValue(ent.pmRest4StartTime);
				p.pmRest4EndTime = AppConverter.intValue(ent.pmRest4EndTime);
				p.pmRest5StartTime = AppConverter.intValue(ent.pmRest5StartTime);
				p.pmRest5EndTime = AppConverter.intValue(ent.pmRest5EndTime);
				p.boundaryOfStartTime = AppConverter.intValue(ent.boundaryOfStartTime);
				p.boundaryOfEndTime = AppConverter.intValue(ent.boundaryOfEndTime);
				patternList.add(p);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = patternList;
			return res;
		}
	}
}