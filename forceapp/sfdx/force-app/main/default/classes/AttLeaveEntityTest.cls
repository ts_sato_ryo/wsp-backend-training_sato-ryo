/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 勤怠
  *
  * 休暇マスタのエンティティのテスト
  */
@isTest
private class AttLeaveEntityTest {

	/**
	 * テストデータ
	 */
	private class EntityTestData extends TestData.TestDataEntity {
	}

	/**
	 * エンティティの複製ができることを確認する
	 */
	@isTest static void copyTest() {

		EntityTestData testData = new EntityTestData();

		AttLeaveEntity orgEntity = new AttLeaveEntity();
		orgEntity.name = 'Test';
		orgEntity.validFrom = AppDate.today();
		orgEntity.validTo = orgEntity.validFrom.addMonths(12);
		orgEntity.code = orgEntity.name + '_code';
		orgEntity.uniqKey = orgEntity.code + '_uniqKey';
		orgEntity.companyId = testData.company.id;
		orgEntity.nameL0 = orgEntity.name + '_L0';
		orgEntity.nameL1 = orgEntity.name + '_L1';
		orgEntity.nameL2 = orgEntity.name + '_L2';
		orgEntity.daysManaged = true;
		orgEntity.leaveRanges = new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF};
		orgEntity.countType = AttCountType.ATTENDANCE;
		orgEntity.summaryItemNo = 77;
		orgEntity.order = 1;
		orgEntity.leaveType = AttLeaveType.PAID;
		orgEntity.requireReason = true;

		AttLeaveEntity copyEntity = orgEntity.copy();
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.validFrom, copyEntity.validFrom);
		System.assertEquals(orgEntity.validTo, copyEntity.validTo);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.nameL0, copyEntity.nameL0);
		System.assertEquals(orgEntity.nameL1, copyEntity.nameL1);
		System.assertEquals(orgEntity.nameL2, copyEntity.nameL2);
		System.assertEquals(orgEntity.daysManaged, copyEntity.daysManaged);
		System.assertEquals(orgEntity.countType, copyEntity.countType);
		System.assertEquals(orgEntity.summaryItemNo, copyEntity.summaryItemNo);
		System.assertEquals(orgEntity.order, copyEntity.order);
		System.assertEquals(orgEntity.leaveType, copyEntity.leaveType);
		System.assertEquals(orgEntity.leaveRanges.size(), copyEntity.leaveRanges.size());
		System.assertEquals(orgEntity.requireReason, copyEntity.requireReason);

		// リストはコピーされているため、コピー元に追加してもコピーしたエンティティには影響がでない
		orgEntity.leaveRanges.add(AttLeaveRange.RANGE_HALF);
		System.assertEquals(orgEntity.leaveRanges.size() - 1, copyEntity.leaveRanges.size());
	}
}
