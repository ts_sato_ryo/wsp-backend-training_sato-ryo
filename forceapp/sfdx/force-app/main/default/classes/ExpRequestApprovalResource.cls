/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description Resource class of Expense Request Approval
 */
public with sharing class ExpRequestApprovalResource {
	
	/**
	 * Parameter for submitting an approval request
	 */
	public class SubmitRequestParam implements RemoteApi.RequestParam {
		/** Report ID */
		public String reportId;
		/** Application comment */
		public String comment;
	}
	
	/**
	 * Response after submitting an approval request
	 */
	public class SubmitRequestResponse implements RemoteApi.ResponseParam {
		// レスポンスパラメータなし No response parameters
	}
	
	/**
	 * API to submit a approval request of Expense request
	 */
	public with sharing class SubmitExpRequestApprovalApi extends RemoteApi.ResourceBase {

		/**
		 * Submit a approval request of Expense request
		 * @param  req Request parameter (SubmitRequestParam)
		 * @return Response (null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SubmitRequestParam param = (SubmitRequestParam)req.getParam(ExpRequestApprovalResource.SubmitRequestParam.class);

			validateParam(param);

			// Check if Expense module is available
			ExpCommonUtil.checkCanUseExpenseRequest(null);

			// Rollback once saving fail
			Savepoint sp = Database.setSavepoint();

			try {
				// Submit approval request
				ExpRequestApprovalService.submitRequest(param.reportId, param.comment);
			} catch (Exception e) {
				// Rollback
				Database.rollback(sp);
				throw e;
			}

			return null;
		}

		private void validateParam(SubmitRequestParam param) {
			ExpCommonUtil.validateId('reportId', param.reportId, true);
		}
	}

	/*
	 * Request param for Recalling the Expense Request Approval Submission
	 */
	public class CancelExpRequestApprovalParam implements RemoteApi.RequestParam {
		/** Id Of Request Approval */
		public String requestId;
		/** Comment to cancel submission */
		public String comment;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			ExpCommonUtil.validateId('requestId', requestId, true);
		}
	}

	/**
	 * Response param for Recalling the Expense Request Approval Submission
	 */
	public class CancelExpRequestApprovalResponse implements RemoteApi.ResponseParam {
	}

	/**
	 * API for Recalling Expense Request Approval Submission
	 */
	public with sharing class CancelExpRequestApprovalApi extends RemoteApi.ResourceBase {

		/** Flag for Roll Back Testing */
		@TestVisible
		private Boolean isRollbackTest = false;

		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			CancelExpRequestApprovalParam param = (CancelExpRequestApprovalParam) req.getParam(ExpRequestApprovalResource.CancelExpRequestApprovalParam.class);

			param.validate();
			ExpCommonUtil.checkCanUseExpenseRequest(ExpCommonUtil.getEmployeeByUserId(UserInfo.getUserId()));

			Savepoint sp = Database.setSavepoint();

			try {
				new ExpRequestApprovalService().cancelRequest(param.requestId, param.comment);

				if (isRollbackTest) {
					throw new App.IllegalStateException('CancelExpReportRequestApi Rollback Test');
				}

				return new CancelExpRequestApprovalResponse();
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}
		}
	}
}