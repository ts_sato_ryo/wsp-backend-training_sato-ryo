/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 承認
 *
 * ApprovalProcessRepositoryのテスト
 */
@isTest
private class ApprovalProcessRepositoryTest {

	/**
	 * getRequestIdListをテストする
	 */
	@isTest static void getRequestIdListTest() {
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');

		Test.startTest();

		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		List<Id> requestIdList = repo.getRequestIdList(user.Id, TimeRequest__c.SObjectType.getDescribe().getName());

		Test.stopTest();

		// プロセスインスタンスの情報を取得できないため実行のみ
		System.assertEquals(0, requestIdList.size());
	}

	/**
	 * getRequestListをテストする
	 */
	@isTest static void getRequestListTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summary = testData.createSummaryEntity();

		TimeRequest__c request = new TimeRequest__c();
		request.TimeSummaryId__c = summary.Id;
		insert request;

		Test.startTest();

		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		List<ApprovalProcessEntity> resultRequestList = repo.getRequestList(new List<Id>{request.Id});

		Test.stopTest();

		// プロセスインスタンスの情報を取得できないため実行のみ
		System.assertEquals(0, resultRequestList.size());
	}

	/**
	 * getHistoryListをテストする
	 */
	@isTest static void getHistoryListTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summary = testData.createSummaryEntity();

		TimeRequest__c request = new TimeRequest__c();
		request.TimeSummaryId__c = summary.Id;
		insert request;

		Test.startTest();

		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		List<ApprovalProcessHistoryEntity> historyList = repo.getHistoryList(request.Id);

		Test.stopTest();

		// プロセスインスタンスの情報を取得できないため実行のみ
		System.assertEquals(0, historyList.size());
	}

	/**
	 * getWorkItemListをテストする
	 */
	@isTest static void getWorkItemListTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summary = testData.createSummaryEntity();

		TimeRequest__c request = new TimeRequest__c();
		request.TimeSummaryId__c = summary.Id;
		insert request;

		Test.startTest();

		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		List<Id> workItemList = repo.getWorkItemList(new List<Id>{request.Id});

		Test.stopTest();

		// プロセスインスタンスの情報を取得できないため実行のみ
		System.assertEquals(0, workItemList.size());
	}

	/*
	 * Common function for report and employee setting
	 */
	private static void reportAndEmployeeSetting(ExpTestData testData, List<ExpReportEntity> reportList, EmployeeBaseEntity managerEmpEntity) {
		List<ExpRecordEntity> record1 = testData.createExpRecords(reportList[0].id, Date.today(), testData.expType, testData.employee, 3);
		List<ExpRecordEntity> record2 = testData.createExpRecords(reportList[1].id, Date.today(), testData.expType, testData.employee, 3);

		// Set manager to employee
		testData.employee.getHistory(0).managerId = managerEmpEntity.id;
		new EmployeeRepository().saveEntity(testData.employee);
	}

	/*
	 * Test of getExpApprovedRejectedRequestIdList -- Status: Approval
	 */
	@isTest static void getExpApprovedRejectedRequestIdListApproveTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity = testEmployeeList[1];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser = [SELECT Id FROM User WHERE Id = :submitterEmpEntity.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
			new ExpReportRequestService().submitRequest(reportList[1].id, 'comment');
		}

		Id reportRequestIdForApproval = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Approve 1 request
			new ApprovalService().approve(new List<Id>{reportRequestIdForApproval}, 'comment');

			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Approved.name());
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpApprovedRejectedRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(1, idList.size());
		System.assertEquals(reportRequestIdForApproval, idList[0]);
	}

	/*
	 * Test of getExpApprovedRejectedRequestIdList -- Status: Reject
	 */
	@isTest static void getExpApprovedRejectedRequestIdListRejectTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity = testEmployeeList[1];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser = [SELECT Id FROM User WHERE Id = :submitterEmpEntity.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
			new ExpReportRequestService().submitRequest(reportList[1].id, 'comment');
		}

		Id reportRequestIdForApproval = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Reject 1 request
			new ApprovalService().reject(new List<Id>{reportRequestIdForApproval}, 'comment');

			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Rejected.name());
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpApprovedRejectedRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(1, idList.size());
		System.assertEquals(reportRequestIdForApproval, idList[0]);
	}

	/*
	 * Test of getExpApprovedRejectedRequestIdList -- empBaseId
	 */
	@isTest static void getExpApprovedRejectedRequestIdListEmpBaseIdTest() {
		ExpTestData testData = new ExpTestData();

		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(3);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity1 = testEmployeeList[1];
		EmployeeBaseEntity submitterEmpEntity2 = testEmployeeList[2];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser1 = [SELECT Id FROM User WHERE Id = :submitterEmpEntity1.userId LIMIT 1];
		User submitterUser2 = [SELECT Id FROM User WHERE Id = :submitterEmpEntity2.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		ExpReportRequestService expRepReqService = new ExpReportRequestService();
		expRepReqService.isTestModeAndBypassExpReportValidation = true;

		System.runAs(submitterUser1) {
			expRepReqService.submitRequest(reportList[0].id, 'comment');
		}
		System.runAs(submitterUser2) {
			expRepReqService.submitRequest(reportList[1].id, 'comment');
		}

		Id reportRequestIdFromSubmitter1 = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;
		Id reportRequestIdFromSubmitter2 = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[1].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Approve all requests
			new ApprovalService().approve(new List<Id>{reportRequestIdFromSubmitter1}, 'comment');
			new ApprovalService().approve(new List<Id>{reportRequestIdFromSubmitter2}, 'comment');

			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Approved.name()); // add condition of request only from submitter1
			filter.empBaseIdList.add(submitterEmpEntity1.id);
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpApprovedRejectedRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(1, idList.size());
		System.assertEquals(reportRequestIdFromSubmitter1, idList[0]);
	}

	/*
	 * Test of getExpApprovedRejectedRequestIdList -- DateRange positive
	 * Submit request today & search condition is today -> expected result: can search the request
	 */
	@isTest static void getExpApprovedRejectedRequestIdListDateRangePositiveTest() {
		ExpTestData testData = new ExpTestData();
		Date todayDate = DateTime.now().dateGMT();
		List<ExpReportEntity> reportList = testData.createExpReports(todayDate, testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity = testEmployeeList[1];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser = [SELECT Id FROM User WHERE Id = :submitterEmpEntity.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
		}

		Id reportRequestIdForApproval = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Approve 1 request
			new ApprovalService().approve(new List<Id>{reportRequestIdForApproval}, 'comment');
			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Approved.name()); // add condition of request only from submitter1
			filter.requestDateRange.startDate = todayDate;
			filter.requestDateRange.endDate = todayDate;
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpApprovedRejectedRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(1, idList.size());
		System.assertEquals(reportRequestIdForApproval, idList[0]);
	}


	/*
	 * Test of getExpApprovedRejectedRequestIdList -- DateRange negative
	 * Submit request today & search condition is the next day -> expected result: cannot search the request
	 */
	@isTest static void getExpApprovedRejectedRequestIdListDateRangeNegativeTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity = testEmployeeList[1];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser = [SELECT Id FROM User WHERE Id = :submitterEmpEntity.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
		}

		Id reportRequestIdForApproval = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Approve 1 request
			new ApprovalService().approve(new List<Id>{reportRequestIdForApproval}, 'comment');
			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Approved.name()); // add condition of request only from submitter1
			// set condition range as tomorrow even though submit today. expected return is 0 ids.
			filter.requestDateRange.startDate = Date.today().addDays(1);
			filter.requestDateRange.endDate = Date.today().addDays(1);
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpApprovedRejectedRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(0, idList.size());
	}

	/*
	 * Test of getExpPendingRequestIdList -- Pending (Positive Test)
	 */
	@isTest static void getExpPendingRequestIdListPendingTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity = testEmployeeList[1];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser = [SELECT Id FROM User WHERE Id = :submitterEmpEntity.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
			new ExpReportRequestService().submitRequest(reportList[1].id, 'comment');
		}

		Id reportRequestIdForApproval = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;
		Id reportRequestIdForPending = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[1].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Approve 1 request
			new ApprovalService().approve(new List<Id>{reportRequestIdForApproval}, 'comment');

			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Pending.name());
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpPendingRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(1, idList.size());
		System.assertEquals(reportRequestIdForPending, idList[0]);
	}

	/*
	 * Test of getExpPendingRequestIdList -- EmpBaseId
	 */
	@isTest static void getExpPendingRequestIdListPendingEmpBaseIdTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(3);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity1 = testEmployeeList[1];
		EmployeeBaseEntity submitterEmpEntity2 = testEmployeeList[2];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser1 = [SELECT Id FROM User WHERE Id = :submitterEmpEntity1.userId LIMIT 1];
		User submitterUser2 = [SELECT Id FROM User WHERE Id = :submitterEmpEntity2.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser1) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
		}
		System.runAs(submitterUser2) {
			new ExpReportRequestService().submitRequest(reportList[1].id, 'comment');
		}

		Id reportRequestIdFromSubmitter1 = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {

			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Pending.name());
			filter.empBaseIdList.add(submitterEmpEntity1.id);
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpPendingRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(1, idList.size());
		System.assertEquals(reportRequestIdFromSubmitter1, idList[0]);
	}

	/*
	 * Test of getExpPendingRequestIdList -- DateRange positive
	 * Submit request today & search condition is today -> expected result: can search the request
	 */
	@isTest static void getExpPendingRequestIdListDateRangePositiveTest() {
		ExpTestData testData = new ExpTestData();
		Date todayDate = DateTime.now().dateGMT();
		List<ExpReportEntity> reportList = testData.createExpReports(todayDate, testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity = testEmployeeList[1];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser = [SELECT Id FROM User WHERE Id = :submitterEmpEntity.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
		}

		Id reportRequestIdForPending = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Approve 1 request
			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Pending.name());
			filter.requestDateRange.startDate = todayDate;
			filter.requestDateRange.endDate = todayDate;
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpPendingRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(1, idList.size());
		System.assertEquals(reportRequestIdForPending, idList[0]);
	}

	/*
	 * Test of getExpPendingRequestIdList -- DateRange negative
	 * submit request today & search condition is the next day -> expected result: cannot search the request
	 */
	@isTest static void getExpPendingRequestIdListDateRangeNegativeTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 2);
		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(testData.expReportType.Id, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		EmployeeBaseEntity managerEmpEntity = testEmployeeList[0];
		EmployeeBaseEntity submitterEmpEntity = testEmployeeList[1];
		reportAndEmployeeSetting(testData, reportList, managerEmpEntity);

		User submitterUser = [SELECT Id FROM User WHERE Id = :submitterEmpEntity.userId LIMIT 1];
		User managerUser = [SELECT Id FROM User WHERE Id = :managerEmpEntity.userId LIMIT 1];

		Test.startTest();

		System.runAs(submitterUser) {
			new ExpReportRequestService().submitRequest(reportList[0].id, 'comment');
		}

		Id reportRequestIdForPending = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id=:reportList[0].Id LIMIT 1].ExpRequestId__c;

		List<Id> idList;
		System.runAs(managerUser) {
			// Approve 1 request
			ApprovalProcessRepository.AdvancedSearchFilter filter = new ApprovalProcessRepository.AdvancedSearchFilter();
			filter.statusList.add(ApprovalService.ProcessInstanceStatus.Pending.name());
			filter.requestDateRange.startDate = Date.today().addDays(1);
			filter.requestDateRange.endDate = Date.today().addDays(1);
			String objectName = ExpReportRequest__c.SObjectType.getDescribe().getName();
			ApprovalProcessRepository apRepo = new ApprovalProcessRepository();
			idList = apRepo.getExpPendingRequestIdList(filter, objectName, managerEmpEntity.userId);

		}
		Test.stopTest();

		System.assertEquals(0, idList.size());
	}


}