/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description PersonalSettingEntityのテストクラス
 */
@isTest
private class PersonalSettingEntityTest {

	/**
	 * nameのテスト
	 * 不正な値を設定した場合に例外が発生することを確認する
	 */
	@isTest static void nameTest() {
		try {
			PersonalSettingEntity entity = new PersonalSettingEntity();
			entity.name = '';
			System.assert(false, '例外が発生しませんでした');
		} catch(App.ParameterException e) {
			System.assert(true);
		} catch(Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}
	}

	/**
	 * isChangedのテスト
	 * 変更情報が正しく取得できることを確認する
	 */
	@isTest static void isChangedTest() {
		PersonalSettingEntity entity = new PersonalSettingEntity();

		// 変更前
		System.assertEquals(false, entity.isChanged(PersonalSettingEntity.Field.Name));

		// 変更後
		entity.name = 'aaaa';
		System.assertEquals(true, entity.isChanged(PersonalSettingEntity.Field.Name));
	}

	/**
	 * resetChangedのテスト
	 * 変更情報がリセットできることを確認する
	 */
	@isTest static void resetChangedTest() {
		PersonalSettingEntity entity = new PersonalSettingEntity();

		// 変更してみる
		entity.name = 'aaaa';
		System.assertEquals(true, entity.isChanged(PersonalSettingEntity.Field.Name));

		// 変更情報リセット
		entity.resetChanged();
		System.assertEquals(false, entity.isChanged(PersonalSettingEntity.Field.Name));
	}
}