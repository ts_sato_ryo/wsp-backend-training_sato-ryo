/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠日次申請のトリガーハンドラー
 */
public with sharing class AttDailyRequestTriggerHandler extends RequestTriggerHandler {
	public override Set<Id> getApprovedRequestIds() {
		return approvedRequestMap.keySet();
	}
	public override Set<Id> getDisabledRequestIds() {
		return disabledRequestMap.keySet();
	}

	public Set<Id> removeRequestIds {
		get {
			return removeRequestMap.keySet();
		}
	}

	public Set<Id> reapplyingToDisabledRequestIds {
		get {
			return reapplyingToDisabledRequestMap.keySet();
		}
	}

	public List<AttRecord__c> updatedAttRecordList {get; private set;}
	public List<AttSummary__c> updatedAttSummaryList {
		get {
			List<AttSummary__c> retList = new List<AttSummary__c>();
			for (Id summaryId : dirtySummaryIds) {
				retList.add(new AttSummary__c(Id = summaryId, Dirty__c = true));
			}
			return retList;
		}
	}
	private Map<Id, AttDailyRequest__c> approvedRequestMap = new Map<id, AttDailyRequest__c>();
	private Map<Id, AttDailyRequest__c> disabledRequestMap = new Map<Id, AttDailyRequest__c>();
	private Map<Id, AttDailyRequest__c> removeRequestMap = new Map<Id, AttDailyRequest__c>();
	private Map<Id, AttDailyRequest__c> reapplyingToDisabledRequestMap = new Map<Id, AttDailyRequest__c>();
	private Set<Id> dirtySummaryIds = new Set<Id>();

	public AttDailyRequestTriggerHandler(Map<Id, AttDailyRequest__c> oldMap, Map<Id, AttDailyRequest__c> newMap) {
		super();
		this.updatedAttRecordList = new List<AttRecord__c>();
		this.init(oldMap, newMap);
	}
	// 承認プロセス情報のセット処理(承認者、承認日時、コメントなど)
	public override void applyProcessInfo() {
		Set<Id> targetObjectIds = new Set<Id>();
		targetObjectIds.addAll(approvedRequestIds);
		targetObjectIds.addAll(disabledRequestIds);
		Map<Id, ProcessInstanceStep> processInfoMap = getProcessInfo(targetObjectIds);

		for(AttDailyRequest__c dailyRequest : approvedRequestMap.values()) {
			if (!processInfoMap.containsKey(dailyRequest.id)) {
				continue;
			}
			ProcessInstanceStep pis = processInfoMap.get(dailyRequest.id);
			dailyRequest.LastApproveTime__c = pis.CreatedDate;
			dailyRequest.LastApproverId__c = pis.ActorId;
			dailyRequest.ProcessComment__c = pis.Comments;
		}
		for(AttDailyRequest__c dailyRequest : disabledRequestMap.values()) {
			dailyRequest.ConfirmationRequired__c = true; // 要確認フラグ
			if (!processInfoMap.containsKey(dailyRequest.id)) {
				continue;
			}
			ProcessInstanceStep pis = processInfoMap.get(dailyRequest.id);
			// 却下
			if (pis.StepStatus == STEP_STATUS_REJECTED) {
				dailyRequest.CancelType__c = AppCancelType.REJECTED.value;
				dailyRequest.ProcessComment__c = pis.Comments;
			}
			// 申請取消
			else if (pis.StepStatus == STEP_STATUS_REMOVED) {
				dailyRequest.CancelType__c = AppCancelType.REMOVED.value;
				// TODO: 取消コメントを申請オブジェクトに保存する
			}
			// 承認取消
			else if (pis.StepStatus == STEP_STATUS_APPROVED ) {
				dailyRequest.CancelType__c = AppCancelType.CANCELED.value;
				dailyRequest.LastApproveTime__c = null;
				dailyRequest.LastApproverId__c = null;
				dailyRequest.ProcessComment__c = null;
			}
		}
	}

	// 承認/却下時の勤怠明細のセット処理(申請情報コピー、クリア)
	public void applyAttRecordInfo(final List<AttRecord__c> attRecordList) {
		this.updatedAttRecordList.clear();
		this.dirtySummaryIds.clear();

		for (AttRecord__c attRecord : attRecordList) {
			Boolean isTarget = false;
			Id requestId = attRecord.ReqRequestingLeave1RequestId__c;
			// 休暇1承認済み、申請データをコピー
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyLeaveRequestInfo(1, attRecord, this.approvedRequestMap.get(requestId));
			}
			requestId = attRecord.ReqRequestingLeave2RequestId__c;
			// 休暇2承認済み、申請データをコピー
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyLeaveRequestInfo(2, attRecord, this.approvedRequestMap.get(requestId));
			}
			requestId = attRecord.ReqRequestingHolidayWorkRequestId__c;
			// 休日出勤申請承認済み、申請データをコピー
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyHolidayWorkRequestInfo(attRecord, this.approvedRequestMap.get(requestId));
			}
			requestId = attRecord.ReqRequestingEarlyStartWorkRequestId__c;
			// 早朝勤務申請承認済み、申請データをコピー
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyEarlyStartWorkRequestInfo(attRecord, this.approvedRequestMap.get(requestId));
			}
			requestId = attRecord.ReqRequestingOvertimeWorkRequestId__c;
			// 残業申請承認済み、申請データをコピー
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyOvertimeWorkRequestInfo(attRecord, this.approvedRequestMap.get(requestId));
			}
			requestId = attRecord.ReqRequestingAbsenceRequestId__c;
			// 欠勤申請承認済み、申請データをコピー
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyAbsenceRequestInfo(attRecord, this.approvedRequestMap.get(requestId));
			}
			// 直行直帰承認済み、申請データをコピー
			requestId = attRecord.InpRequestingDirectRequestId__c;
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyDirectRequestInfo(attRecord, this.approvedRequestMap.get(requestId));
			}
			// 勤務時間変更申請承認済み、申請データをコピー
			requestId = attRecord.ReqRequestingPatternRequestId__c;
			if (this.approvedRequestMap.containsKey(requestId)) {
				isTarget = true;
				this.copyPatternRequestInfo(attRecord, this.approvedRequestMap.get(requestId));
			}

			if (isTarget) {
				this.updatedAttRecordList.add(attRecord);
				this.dirtySummaryIds.add(attRecord.SummaryId__c);
			}
		}
	}

	// 承認取消時の勤怠明細の適用解除処理(申請情報クリア)
	public void unapplyAttRecordInfo(final List<AttRecord__c> attRecordList) {
		this.updatedAttRecordList.clear();
		this.dirtySummaryIds.clear();

		for (AttRecord__c attRecord : attRecordList) {
			Boolean isTarget = false;
			Id requestId = attRecord.ReqLeave1RequestId__c;
			// 休暇1承認済み、申請データを初期化
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				clearLeaveRequestInfo(1, attRecord);
			}
			requestId = attRecord.ReqLeave2RequestId__c;
			// 休暇2承認済み、申請データを初期化
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				clearLeaveRequestInfo(2, attRecord);
			}
			requestId = attRecord.ReqHolidayWorkRequestId__c;
			// 休日出勤申請承認済み、申請データを初期化
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqHolidayWorkRequestId__c = null;
				attRecord.ReqHolidayWorkStartTime__c = null;
				attRecord.ReqHolidayWorkEndTime__c = null;
				attRecord.ReqHolidayWorkSubstituteLeaveType__c = null;
				attRecord.ReqHolidayWorkSubstituteLeaveRange__c = null;
				attRecord.ReqHolidayWorkSubstituteLeave1Date__c = null;
				attRecord.ReqHolidayWorkSubstituteLeave1Range__c = null;
				attRecord.ReqRequestingHolidayWorkRequestId__c = requestId;
			}
			requestId = attRecord.ReqEarlyStartWorkRequestId__c;
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqEarlyStartWorkRequestId__c = null;
				attRecord.ReqEarlyStartWorkStartTime__c = null;
				attRecord.ReqEarlyStartWorkEndTime__c = null;
				attRecord.ReqRequestingEarlyStartWorkRequestId__c = requestId;
			}
			requestId = attRecord.ReqOvertimeWorkRequestId__c;
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqOvertimeWorkRequestId__c = null;
				attRecord.ReqOvertimeWorkStartTime__c = null;
				attRecord.ReqOvertimeWorkEndTime__c = null;
				attRecord.ReqRequestingOvertimeWorkRequestId__c = requestId;
			}
			// 欠勤申請
			requestId = attRecord.ReqAbsenceRequestId__c;
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqAbsenceRequestId__c = null;
				attRecord.ReqRequestingAbsenceRequestId__c = requestId;
			}
			// 直行申請
			requestId = attRecord.InpDirectRequestId__c;
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.InpDirectRequestId__c = null;
				attRecord.InpDirectStartTime__c = null;
				attRecord.InpDirectEndTime__c = null;
				attRecord.InpDirectRest1StartTime__c = null;
				attRecord.InpDirectRest1EndTime__c = null;
				attRecord.InpDirectRest2StartTime__c = null;
				attRecord.InpDirectRest2EndTime__c = null;
				attRecord.InpDirectRest3StartTime__c = null;
				attRecord.InpDirectRest3EndTime__c = null;
				attRecord.InpDirectRest4StartTime__c = null;
				attRecord.InpDirectRest4EndTime__c = null;
				attRecord.InpDirectRest5StartTime__c = null;
				attRecord.InpDirectRest5EndTime__c = null;
				// 実の出退勤時刻にも反映
				attRecord.InpStartTime__c = null;
				attRecord.InpStartStampTime__c = null;
				attRecord.InpEndTime__c = null;
				attRecord.InpEndStampTime__c =  null;
				attRecord.InpRest1StartTime__c =  null;
				attRecord.InpRest1EndTime__c =  null;
				attRecord.InpRest2StartTime__c =  null;
				attRecord.InpRest2EndTime__c =  null;
				attRecord.InpRest3StartTime__c =  null;
				attRecord.InpRest3EndTime__c =  null;
				attRecord.InpRest4StartTime__c =  null;
				attRecord.InpRest4EndTime__c =  null;
				attRecord.InpRest5StartTime__c = null;
				attRecord.InpRest5EndTime__c =  null;
				attRecord.InpLastUpdateTime__c = null;
				attRecord.InpRequestingDirectRequestId__c = requestId;
			}
			// 勤務時間変更申請
			requestId = attRecord.ReqPatternRequestId__c;
			if (this.disabledRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqPatternRequestId__c = null;
				attRecord.ReqRequestingPatternRequestId__c = requestId;
				attRecord.ReqPatternId__c = null;
			}
			if (isTarget) {
				this.updatedAttRecordList.add(attRecord);
				this.dirtySummaryIds.add(attRecord.SummaryId__c);
			}
		}
	}
	// 申請取下の勤怠明細の申請情報クリア処理
	public void removeAttRecordInfo(final List<AttRecord__c> attRecordList) {
		this.updatedAttRecordList.clear();

		for (AttRecord__c attRecord : attRecordList) {
			Boolean isTarget = false;
			Id requestId = attRecord.ReqRequestingLeave1RequestId__c;

			// 休暇1申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingLeave1RequestId__c = null;
			}
			requestId = attRecord.ReqRequestingLeave2RequestId__c;
			// 休暇2申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingLeave2RequestId__c = null;
			}
			requestId = attRecord.ReqRequestingHolidayWorkRequestId__c;
			// 休日出勤申請申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingHolidayWorkRequestId__c = null;
			}
			requestId = attRecord.ReqRequestingLeave1RequestId__r.ParentRequestId__c;
			// 振替休暇1申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingLeave1RequestId__c = null;
			}
			requestId = attRecord.ReqRequestingLeave2RequestId__r.ParentRequestId__c;
			// 振替休暇2申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingLeave2RequestId__c = null;
			}
			requestId = attRecord.ReqRequestingEarlyStartWorkRequestId__c;
			// 早朝勤務申請申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingEarlyStartWorkRequestId__c = null;
			}
			requestId = attRecord.ReqRequestingOvertimeWorkRequestId__c;
			// 残業申請申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingOvertimeWorkRequestId__c = null;
			}
			requestId = attRecord.ReqRequestingAbsenceRequestId__c;
			// 欠勤申請申請中IDを初期化
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingAbsenceRequestId__c = null;
			}
			// 直行直帰申請中IDを初期化
			requestId = attRecord.InpRequestingDirectRequestId__c;
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.InpRequestingDirectRequestId__c = null;
			}
			// 勤務時間変更申請中IDを初期化
			requestId = attRecord.ReqRequestingPatternRequestId__c;
			if (this.removeRequestMap.containsKey(requestId)) {
				isTarget = true;
				attRecord.ReqRequestingPatternRequestId__c = null;
			}
			if (isTarget) {
				this.updatedAttRecordList.add(attRecord);
			}
		}
	}
	// 承認内容変更申請が承認された時に元の振替休日を無効化する
	public void reapplyAttRecordInfo(final Set<Id> reappliedRequestId, final List<AttRecord__c> attRecordList) {
		this.updatedAttRecordList.clear();
		this.dirtySummaryIds.clear();
		for (AttRecord__c attRecord : attRecordList) {
			Boolean isTarget = false;

			// 古い休暇1承認済み申請を初期化
			if (reappliedRequestId.contains(attRecord.ReqLeave1RequestId__c)) {
				isTarget = true;
				attRecord.ReqLeave1RequestId__c = null;
				attRecord.ReqLeave1Type__c = null;
				attRecord.ReqLeave1Id__c = null;
				attRecord.ReqLeave1Range__c = null;
				attRecord.ReqLeave1StartTime__c = null;
				attRecord.ReqLeave1EndTime__c = null;
				attRecord.ReqLeave1SubstituteDayType__c = null;
			}
			// 古い休暇2承認済み申請を初期化
			if (reappliedRequestId.contains(attRecord.ReqLeave2RequestId__c)) {
				isTarget = true;
				attRecord.ReqLeave2RequestId__c = null;
				attRecord.ReqLeave2Type__c = null;
				attRecord.ReqLeave2Id__c = null;
				attRecord.ReqLeave2Range__c = null;
				attRecord.ReqLeave2StartTime__c = null;
				attRecord.ReqLeave2EndTime__c = null;
				attRecord.ReqLeave2SubstituteDayType__c = null;
			}
			// 古い休日出勤申請を初期化
			if (reappliedRequestId.contains(attRecord.ReqHolidayWorkRequestId__c)) {
				isTarget = true;
				attRecord.ReqHolidayWorkRequestId__c = null;
				attRecord.ReqHolidayWorkStartTime__c = null;
				attRecord.ReqHolidayWorkEndTime__c = null;
				attRecord.ReqHolidayWorkSubstituteLeaveType__c = null;
				attRecord.ReqHolidayWorkSubstituteLeaveRange__c = null;
				attRecord.ReqHolidayWorkSubstituteLeave1Date__c = null;
				attRecord.ReqHolidayWorkSubstituteLeave1Range__c = null;
			}
			if (isTarget) {
				this.updatedAttRecordList.add(attRecord);
				this.dirtySummaryIds.add(attRecord.SummaryId__c);
			}
		}
	}

	/**
	 * 指定した休暇消費を削除する（休暇情報の再計算はしない）
	 * @param leaveTakeList 削除対象の休暇消費
	 */
	public void deleteManagedLeaveTakenList(List<AttManagedLeaveTake__c> leaveTakeList) {

		AttManagedLeaveTakeRepository takeRep = new AttManagedLeaveTakeRepository();
		List<AttManagedLeaveTakeEntity> leaveTakeEntityList = new List<AttManagedLeaveTakeEntity>();
		for (AttManagedLeaveTake__c leaveTake : leaveTakeList) {
			AttManagedLeaveTakeEntity leaveTakeEntity = new AttManagedLeaveTakeEntity();
			leaveTakeEntity.setId(leaveTake.Id);
			leaveTakeEntityList.add(leaveTakeEntity);
		}
		takeRep.deleteEntityList(leaveTakeEntityList);
	}

	/**
	 * 休暇申請から休暇消費を作成する（永続化はしない）
	 * @param leaveRequestList 承認済みになった休暇申請のリスト
	 * @param leaveSummaryList 勤務確定していない月の休暇サマリーのリスト
	 */
	public List<AttManagedLeaveTake__c> createManagedLeaveTakenList(
			List<AttDailyRequest__c> leaveRequestList,
			List<AttManagedLeaveSummary__c> leaveSummaryList) {
		// 社員、休暇Idごとに申請と休暇サマリを作成
		Map<Id, Map<Id, List<AttDailyRequest__c>>> requstMap = new Map<Id, Map<Id, List<AttDailyRequest__c>>>();
		Map<Id, Map<Id, List<AttManagedLeaveSummary__c>>> summaryMap = new Map<Id, Map<Id, List<AttManagedLeaveSummary__c>>>();
		for (AttDailyRequest__c request : leaveRequestList) {
			Id empId = request.EmployeeHistoryId__r.BaseId__c;
			Id leaveId = request.AttLeaveId__c;
			if (requstMap.containskey(empId)) {
				Map<Id, List<AttDailyRequest__c>> leaveRequestMap = requstMap.get(empId);
				if (leaveRequestMap.containsKey(leaveId)) {
					leaveRequestMap.get(leaveId).add(request);
				}
				else {
					leaveRequestMap.put(leaveId, new List<AttDailyRequest__c>{request});
				}
			}
			else {
				requstMap.put(empId, new Map<Id, List<AttDailyRequest__c>>{
						leaveId => new List<AttDailyRequest__c>{request}});
			}
		}
		for (AttManagedLeaveSummary__c leaveSummary : leaveSummaryList) {
			Id empId = leaveSummary.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c;
			Id leaveId = leaveSummary.LeaveId__c;
			if (summaryMap.containskey(empId)) {
				Map<Id, List<AttManagedLeaveSummary__c>> leaveSummaryMap = summaryMap.get(empId);
				if (leaveSummaryMap.containsKey(leaveId)) {
					leaveSummaryMap.get(leaveId).add(leaveSummary);
				}
				else {
					leaveSummaryMap.put(leaveId, new List<AttManagedLeaveSummary__c>{leaveSummary});
				}
			}
			else {
				summaryMap.put(empId, new Map<Id, List<AttManagedLeaveSummary__c>>{
						leaveId => new List<AttManagedLeaveSummary__c>{leaveSummary}});
			}
		}
		List<AttManagedLeaveTake__c> retList = new List<AttManagedLeaveTake__c>();
		AttManagedLeaveService managedLeaveService = new AttManagedLeaveService();
		for (Id empId : requstMap.keySet()) {
			Map<Id, List<AttDailyRequest__c>> leaveRequestMap = requstMap.get(empId);
			Map<Id, List<AttManagedLeaveSummary__c>> leaveSummaryMap = summaryMap.get(empId);
			for (Id leaveId : leaveRequestMap.keySet()) {
				for (AttDailyRequest__c dailyRequest : leaveRequestMap.get(leaveId)) {
					AttDailyRequestEntity dailyRequestData = convertAttDailyRequestEntity(dailyRequest);
					List<AttManagedLeaveSummaryEntity> leaveSummaryDataList = convertLeaveSummaryList(leaveSummaryMap.get(leaveId));
					// 休暇消費を作成
					List<AttManagedLeaveTakeEntity> takenDataList = managedLeaveService.convertLeaveRequestToTakeEntity(dailyRequestData, leaveSummaryDataList);
					// オブジェクトに変換
					retList.addAll(convertLeaveTakeList(takenDataList));
				}
			}
		}
		return retList;
	}
	public void recalcManagedLeaveSummary(
			List<AttManagedLeaveSummary__c> leaveSummaryList,
			List<AttManagedLeaveTake__c> takenListAll,
			List<AttManagedLeaveGrant__c> grantListAll,
			List<AttManagedLeaveSummary__c> leaveSummaryListUpdate,
			List<AttManagedLeaveGrantSummary__c> grantSummaryListInsert) {

		List<AttManagedLeaveSummaryEntity> leaveSummaryDataList =
				convertLeaveSummaryList(leaveSummaryList);
		List<AttManagedLeaveTakeEntity> takenDataList =
				convertLeaveTakeList(takenListAll);
		List<AttManagedLeaveGrantEntity> grantDataList =
				convertLeaveGrantList(grantListAll);

		AttManagedLeaveService managedLeaveService = new AttManagedLeaveService();
		List<AttManagedLeaveSummaryEntity> leaveSummaryListRecalced =
				managedLeaveService.calcLeaveSummaryAll(
						leaveSummaryDataList,
						takenDataList,
						grantDataList);

		// 結果結果のサマリと付与サマリをObjectへ変換
		List<AttManagedLeaveGrantSummaryEntity> grantSummaryList = new List<AttManagedLeaveGrantSummaryEntity>();
		for (AttManagedLeaveSummaryEntity leaveSummaryData : leaveSummaryListRecalced) {
			grantSummaryList.addAll(leaveSummaryData.grantSummaryList);
		}
		leaveSummaryListUpdate.addAll(convertLeaveSummaryList(leaveSummaryListRecalced));
		grantSummaryListInsert.addAll(convertLeaveGrantSummaryList(grantSummaryList));
	}
	// 承認済みの場合、現申請項目を上書きする
	private void copyLeaveRequestInfo(Integer requestNo, AttRecord__c copyTo, AttDailyRequest__c copyFrom) {
		if (requestNo == 1) {
			copyTo.ReqLeave1RequestId__c = copyFrom.Id;
			copyTo.ReqLeave1Type__c = copyFrom.AttLeaveType__c;
			copyTo.ReqLeave1Id__c = copyFrom.AttLeaveId__c;
			copyTo.ReqLeave1Range__c = copyFrom.AttLeaveRange__c;
			copyTo.ReqLeave1StartTime__c = copyFrom.StartTime__c;
			copyTo.ReqLeave1EndTime__c = copyFrom.EndTime__c;
			copyTo.ReqLeave1SubstituteDayType__c = copyFrom.SubstituteLeaveDayType__c;

			copyTo.ReqRequestingLeave1RequestId__c = null;
		}
		else {
			copyTo.ReqLeave2RequestId__c = copyFrom.Id;
			copyTo.ReqLeave2Type__c = copyFrom.AttLeaveType__c;
			copyTo.ReqLeave2Id__c = copyFrom.AttLeaveId__c;
			copyTo.ReqLeave2Range__c = copyFrom.AttLeaveRange__c;
			copyTo.ReqLeave2StartTime__c = copyFrom.StartTime__c;
			copyTo.ReqLeave2EndTime__c = copyFrom.EndTime__c;
			copyTo.ReqLeave2SubstituteDayType__c = copyFrom.SubstituteLeaveDayType__c;

			copyTo.ReqRequestingLeave2RequestId__c = null;
		}
	}
	// 承認取消の場合、現申請項目をクリアする
	private void clearLeaveRequestInfo(Integer requestNo, AttRecord__c clearRecord) {
		if (requestNo == 1) {
			clearRecord.ReqRequestingLeave1RequestId__c = clearRecord.ReqLeave1RequestId__c;
			clearRecord.ReqLeave1RequestId__c = null;
			clearRecord.ReqLeave1Type__c = null;
			clearRecord.ReqLeave1Id__c = null;
			clearRecord.ReqLeave1Range__c = null;
			clearRecord.ReqLeave1StartTime__c = null;
			clearRecord.ReqLeave1EndTime__c = null;
			clearRecord.ReqLeave1SubstituteDayType__c = null;
		}
		else {
			clearRecord.ReqRequestingLeave2RequestId__c = clearRecord.ReqLeave2RequestId__c;
			clearRecord.ReqLeave2RequestId__c = null;
			clearRecord.ReqLeave2Type__c = null;
			clearRecord.ReqLeave2Id__c = null;
			clearRecord.ReqLeave2Range__c = null;
			clearRecord.ReqLeave2StartTime__c = null;
			clearRecord.ReqLeave2EndTime__c =null;
			clearRecord.ReqLeave1SubstituteDayType__c = null;
		}
	}
	// 休日出勤申請承認済みの場合、現申請項目を上書きする
	private void copyHolidayWorkRequestInfo(AttRecord__c copyTo, AttDailyRequest__c copyFrom) {
		copyTo.ReqHolidayWorkRequestId__c = copyFrom.Id;
		copyTo.ReqHolidayWorkStartTime__c = copyFrom.StartTime__c;
		copyTo.ReqHolidayWorkEndTime__c = copyFrom.EndTime__c;
		copyTo.ReqHolidayWorkSubstituteLeaveType__c = copyFrom.AllocatedSubstituteLeaveType__c;
		copyTo.ReqHolidayWorkSubstituteLeaveRange__c = copyFrom.AllocatedSubstituteLeaveRange__c;
		copyTo.ReqHolidayWorkSubstituteLeave1Date__c = copyFrom.SubstituteLeave1Date__c;
		copyTo.ReqHolidayWorkSubstituteLeave1Range__c = copyFrom.SubstituteLeave1Range__c;

		// 振替以外の場合、所定休憩をコピー(TODO:申請時休憩時間入力予定)
		if (copyTo.ReqHolidayWorkSubstituteLeaveType__c != AttSubstituteLeaveType.SUBSTITUTE.value
				&& copyTo.InpLastUpdateTime__c == null) {
			if (copyTo.SummaryId__r != null && copyTo.SummaryId__r.WorkingTypeHistoryId__r != null) {
				AttWorkingTypeHistory__c workingTypeData = copyTo.SummaryId__r.WorkingTypeHistoryId__r;

				copyTo.InpRest1StartTime__c = workingTypeData.Rest1StartTime__c;
				copyTo.InpRest1EndTime__c = workingTypeData.Rest1EndTime__c;
				copyTo.InpRest2StartTime__c = workingTypeData.Rest2StartTime__c;
				copyTo.InpRest2EndTime__c = workingTypeData.Rest2EndTime__c;
				copyTo.InpRest3StartTime__c = workingTypeData.Rest3StartTime__c;
				copyTo.InpRest3EndTime__c = workingTypeData.Rest3EndTime__c;
				copyTo.InpRest4StartTime__c = workingTypeData.Rest4StartTime__c;
				copyTo.InpRest4EndTime__c = workingTypeData.Rest4EndTime__c;
				copyTo.InpRest5StartTime__c = workingTypeData.Rest5StartTime__c;
				copyTo.InpRest5EndTime__c = workingTypeData.Rest5EndTime__c;
				copyTo.InpLastUpdateTime__c = DateTime.now();
			}
		}
		copyTo.ReqRequestingHolidayWorkRequestId__c = null;
	}
	// 早朝勤務申請承認済みの場合、現申請項目を上書きする
	private void copyEarlyStartWorkRequestInfo(AttRecord__c copyTo, AttDailyRequest__c copyFrom) {
		copyTo.ReqEarlyStartWorkRequestId__c = copyFrom.Id;
		copyTo.ReqEarlyStartWorkStartTime__c = copyFrom.StartTime__c;
		copyTo.ReqEarlyStartWorkEndTime__c = copyFrom.EndTime__c;

		copyTo.ReqRequestingEarlyStartWorkRequestId__c = null;
	}
	// 残業申請承認済みの場合、現申請項目を上書きする
	private void copyOvertimeWorkRequestInfo(AttRecord__c copyTo, AttDailyRequest__c copyFrom) {
		copyTo.ReqOvertimeWorkRequestId__c = copyFrom.Id;
		copyTo.ReqOvertimeWorkStartTime__c = copyFrom.StartTime__c;
		copyTo.ReqOvertimeWorkEndTime__c = copyFrom.EndTime__c;

		copyTo.ReqRequestingOvertimeWorkRequestId__c = null;
	}
	// 欠勤申請承認済みの場合、現申請項目を上書きする
	private void copyAbsenceRequestInfo(AttRecord__c copyTo, AttDailyRequest__c copyFrom) {
		copyTo.ReqAbsenceRequestId__c = copyFrom.Id;

		copyTo.ReqRequestingAbsenceRequestId__c = null;
	}
	// 直行直帰申請承認済みの場合、現申請項目を上書きする
	private void copyDirectRequestInfo(AttRecord__c copyTo, AttDailyRequest__c copyFrom) {
		copyTo.InpDirectRequestId__c = copyFrom.Id;
		copyTo.InpDirectStartTime__c = copyFrom.StartTime__c;
		copyTo.InpDirectEndTime__c = copyFrom.EndTime__c;
		copyTo.InpDirectRest1StartTime__c = copyFrom.Rest1StartTime__c;
		copyTo.InpDirectRest1EndTime__c = copyFrom.Rest1EndTime__c;
		copyTo.InpDirectRest2StartTime__c = copyFrom.Rest2StartTime__c;
		copyTo.InpDirectRest2EndTime__c = copyFrom.Rest2EndTime__c;
		copyTo.InpDirectRest3StartTime__c = copyFrom.Rest3StartTime__c;
		copyTo.InpDirectRest3EndTime__c = copyFrom.Rest3EndTime__c;
		copyTo.InpDirectRest4StartTime__c = copyFrom.Rest4StartTime__c;
		copyTo.InpDirectRest4EndTime__c = copyFrom.Rest4EndTime__c;
		copyTo.InpDirectRest5StartTime__c = copyFrom.Rest5StartTime__c;
		copyTo.InpDirectRest5EndTime__c = copyFrom.Rest5EndTime__c;
		// 実の出退勤時刻にも反映
		copyTo.InpStartTime__c = copyFrom.StartTime__c;
		copyTo.InpStartStampTime__c = copyFrom.StartTime__c;
		copyTo.InpEndTime__c = copyFrom.EndTime__c;
		copyTo.InpEndStampTime__c = copyFrom.EndTime__c;
		copyTo.InpRest1StartTime__c = copyFrom.Rest1StartTime__c;
		copyTo.InpRest1EndTime__c = copyFrom.Rest1EndTime__c;
		copyTo.InpRest2StartTime__c = copyFrom.Rest2StartTime__c;
		copyTo.InpRest2EndTime__c = copyFrom.Rest2EndTime__c;
		copyTo.InpRest3StartTime__c = copyFrom.Rest3StartTime__c;
		copyTo.InpRest3EndTime__c = copyFrom.Rest3EndTime__c;
		copyTo.InpRest4StartTime__c = copyFrom.Rest4StartTime__c;
		copyTo.InpRest4EndTime__c = copyFrom.Rest4EndTime__c;
		copyTo.InpRest5StartTime__c = copyFrom.Rest5StartTime__c;
		copyTo.InpRest5EndTime__c = copyFrom.Rest5EndTime__c;
		copyTo.InpLastUpdateTime__c = AppDateTime.now().getDatetime();

		copyTo.InpRequestingDirectRequestId__c = null;
	}
	// 勤務時間変更申請承認済みの場合、現申請項目を上書きする
	private void copyPatternRequestInfo(AttRecord__c copyTo, AttDailyRequest__c copyFrom) {
		copyTo.ReqPatternRequestId__c = copyFrom.Id;
		copyTo.ReqPatternId__c = copyFrom.AttPatternId__c;

		copyTo.ReqRequestingPatternRequestId__c = null;
	}
	// 承認済み・却下になった勤怠日次申請Idを取得
	private void init(Map<Id, AttDailyRequest__c> oldMap, Map<Id, AttDailyRequest__c> newMap) {
		for (Id requestId : newMap.keySet()) {
			AttDailyRequest__c dailyRequestNew = newMap.get(requestId);
			AttDailyRequest__c dailyRequestOld = oldMap.get(requestId);
			// 承認内容変更申請を削除した場合：元申請のステータスが「変更承認待ち」から「承認済み」になるが、元々承認済みの申請なので何もしない
			if(AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(dailyRequestNew.Status__c))
						&& AppRequestStatus.REAPPLYING.equals(AppRequestStatus.valueOf(dailyRequestOld.Status__c))){
				continue;
			}
			// 承認内容変更申請を承認した場合：元申請のステータスが「変更承認待ち」から「無効」になる
			else if(AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(dailyRequestNew.Status__c))
						&& AppRequestStatus.REAPPLYING.equals(AppRequestStatus.valueOf(dailyRequestOld.Status__c))){
				reapplyingToDisabledRequestMap.put(dailyRequestNew.Id, dailyRequestNew);
			}
			// 承認判定：ステータス→承認済み
			else if (AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(dailyRequestNew.Status__c))
						&& !AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(dailyRequestOld.Status__c))) {
				approvedRequestMap.put(dailyRequestNew.Id, dailyRequestNew);
			}
			// 承認判定：ステータス→無効(却下、取消)
			else if (AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(dailyRequestNew.Status__c))
						&& !AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(dailyRequestOld.Status__c))) {
				disabledRequestMap.put(dailyRequestNew.Id, dailyRequestNew);
			}
			// 承認判定：ステータス無効から変更なし(取下)
			else if (AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(dailyRequestNew.Status__c))
						&& dailyRequestOld.ConfirmationRequired__c == true
						&& dailyRequestNew.ConfirmationRequired__c == false) {
				removeRequestMap.put(dailyRequestNew.Id, dailyRequestNew);
			}
		}
	}
	private AttDailyRequestEntity convertAttDailyRequestEntity(AttDailyRequest__c attDailyRequest) {
		AttDailyRequestEntity retEntity = new AttDailyRequestEntity();
		AttDailyRequestRepository dailyRequestRep = new AttDailyRequestRepository();
		retEntity.setId(attDailyRequest.Id);
		// 社員BaseIdを利用
		retEntity.employeeId = attDailyRequest.EmployeeHistoryId__r.BaseId__c;
		retEntity.requestType = AttRequestType.valueOf(attDailyRequest.RequestType__c);
		retEntity.leaveId = attDailyRequest.AttLeaveId__c;
		retEntity.leaveRange = AttLeaveRange.valueOf(attDailyRequest.AttLeaveRange__c);
		retEntity.startDate = AppDate.valueOf(attDailyRequest.StartDate__c);
		retEntity.endDate = AppDate.valueOf(attDailyRequest.EndDate__c);
		retEntity.leaveDays = attDailyRequest.LeaveDays__c;
		retEntity.excludingDateList = AttDailyRequestEntity.convertExcludingDateToList(attDailyRequest.ExcludingDate__c);
		return retEntity;
	}
	private List<AttManagedLeaveSummaryEntity> convertLeaveSummaryList(List<AttManagedLeaveSummary__c> summaryList) {
		List<AttManagedLeaveSummaryEntity> retList = new List<AttManagedLeaveSummaryEntity>();
		for (AttManagedLeaveSummary__c summary : summaryList) {
			AttManagedLeaveSummaryEntity retEntity = new AttManagedLeaveSummaryEntity();
			retEntity.setId(summary.Id);
			retEntity.employeeId = summary.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c;
			retEntity.leaveId = summary.LeaveId__c;
			retEntity.startDate = AppDate.valueOf(summary.AttSummaryId__r.StartDate__c);
			retEntity.endDate = AppDate.valueOf(summary.AttSummaryId__r.EndDate__c);
			retList.add(retEntity);
		}
		return retList;
	}
	private List<AttManagedLeaveSummary__c> convertLeaveSummaryList(List<AttManagedLeaveSummaryEntity> entityList) {
		List<AttManagedLeaveSummary__c> retList = new List<AttManagedLeaveSummary__c>();
		for (AttManagedLeaveSummaryEntity entity : entityList) {
			AttManagedLeaveSummary__c retObj = new AttManagedLeaveSummary__c();
			retObj.Id = entity.Id;
			retObj.DaysTaken__c = AppConverter.decValue(entity.daysTaken);
			retObj.DaysLeft__c = AppConverter.decValue(entity.daysLeft);

			retList.add(retObj);
		}
		return retList;
	}
	private List<AttManagedLeaveTake__c> convertLeaveTakeList(List<AttManagedLeaveTakeEntity> takenDataList) {
		List<AttManagedLeaveTake__c> retList = new List<AttManagedLeaveTake__c>();
		for (AttManagedLeaveTakeEntity takenData : takenDataList) {
			retList.add(new AttManagedLeaveTake__c(
					LeaveSummaryId__c = takenData.leaveSummaryId,
					LeaveRequestId__c = takenData.leaveRequestId,
					DaysTaken__c = AppConverter.decValue(takenData.daysTaken),
					StartDate__c = AppConverter.dateValue(takenData.startDate),
					EndDate__c = AppConverter.dateValue(takenData.endDate)));
		}
		return retList;
	}
	private List<AttManagedLeaveTakeEntity> convertLeaveTakeList(List<AttManagedLeaveTake__c> takeList) {
		List<AttManagedLeaveTakeEntity> retList = new List<AttManagedLeaveTakeEntity>();
		for (AttManagedLeaveTake__c take : takeList) {
			AttManagedLeaveTakeEntity retEntity = new AttManagedLeaveTakeEntity();
			retEntity.setId(take.Id);
			retEntity.employeeId = take.LeaveSummaryId__r.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c;
			retEntity.leaveSummaryId = take.LeaveSummaryId__c;
			retEntity.leaveRequestId = take.LeaveRequestId__c;
			retEntity.leaveId = take.LeaveRequestId__r.AttLeaveId__c;
			retEntity.startDate = AppDate.valueOf(take.StartDate__c);
			retEntity.endDate = AppDate.valueOf(take.EndDate__c);
			retEntity.daysTaken = AttDays.valueOf(take.DaysTaken__c);
			retList.add(retEntity);
		}
		return retList;
	}
	private List<AttManagedLeaveGrantEntity> convertLeaveGrantList(List<AttManagedLeaveGrant__c> grantList) {
		List<AttManagedLeaveGrantEntity> retList = new List<AttManagedLeaveGrantEntity>();
		for (AttManagedLeaveGrant__c grant : grantList) {
			AttManagedLeaveGrantEntity retEntity = new AttManagedLeaveGrantEntity();
			retEntity.setId(grant.Id);

			retEntity.employeeId = grant.EmployeeBaseId__c;
			retEntity.leaveId = grant.LeaveId__c;
			retEntity.originalDaysGranted = AttDays.valueOf(grant.OriginalDaysGranted__c);
			retEntity.daysGranted = AttDays.valueOf(grant.DaysGranted__c);
			retEntity.daysLeft = AttDays.valueOf(grant.DaysLeft__c);
			retEntity.validDateFrom = AppDate.valueOf(grant.ValidFrom__c);
			retEntity.validDateTo = AppDate.valueOf(grant.ValidTo__c);
			retList.add(retEntity);
		}
		return retList;
	}
	private List<AttManagedLeaveGrantSummary__c> convertLeaveGrantSummaryList(List<AttManagedLeaveGrantSummaryEntity> entityList) {
		List<AttManagedLeaveGrantSummary__c> retList = new List<AttManagedLeaveGrantSummary__c>();
		for (AttManagedLeaveGrantSummaryEntity entity : entityList) {
			AttManagedLeaveGrantSummary__c retObj = new AttManagedLeaveGrantSummary__c();

			retObj.LeaveSummaryId__c = entity.summaryId;
			retObj.LeaveGrantId__c = entity.grantId;
			retObj.DaysTaken__c = AppConverter.decValue(entity.daysTaken);
			retObj.DaysLeft__c = AppConverter.decValue(entity.daysLeft);

			retList.add(retObj);
		}
		return retList;
	}
}