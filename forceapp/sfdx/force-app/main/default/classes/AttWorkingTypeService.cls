/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 勤務体系のサービスクラス
 */
public with sharing class AttWorkingTypeService extends ParentChildService {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 勤務体系リポジトリ */
	private AttWorkingTypeRepository workingTypeRepo = new AttWorkingTypeRepository();

	/** コンストラクタ */
	public AttWorkingTypeService() {
		super(new AttWorkingTypeRepository());
	}

	/**
	 * 全ての履歴を持った勤務体系をMapで取得する。キーは勤務体系のベースID
	 * @param baseIds 取得対象のベースID
	 */
	public Map<Id, AttWorkingTypeBaseEntity> getBaseMap(List<Id> baseIds) {

		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAll = null;
		List<AttWorkingTypeBaseEntity> bases = workingTypeRepo.getEntityList(baseIds, targetAll);

		// 検索しやすいようにMapに変換(キーはbaseId)
		Map<Id, AttWorkingTypeBaseEntity> baseMap = new Map<Id, AttWorkingTypeBaseEntity>();
		for (AttWorkingTypeBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		return baseMap;
	}

	/**
	 * 社員を1件改定する
	 * 改定する前に改定可能かどうかの検証を行う
	 * @param history 改定する社員履歴エンティティ
	 */
	public void reviseHistoryWithValidation(AttWorkingTypeHistoryEntity history) {

		// 改定可能かを検証する
		List<AttWorkingTypeService.ValidateResult> revisionResultList =
				validateRevisionHistoryList(new List<AttWorkingTypeHistoryEntity>{history});
		if (revisionResultList[0].hasError) {
			throw revisionResultList[0].exceptionList[0];
		}

		// 履歴を改定する
		reviseHistory(history);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntity(Id baseId) {
		// ベースに紐づく全ての履歴を取得する
		return workingTypeRepo.getEntity(baseId);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildBaseEntity> getEntityList(List<Id> baseIds) {
		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAllDate = null; // 全ての履歴が対象
		return workingTypeRepo.getEntityList(baseIds, targetAllDate);
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntityByCode(String code) {
		AttWorkingTypeRepository.SearchFilter filter = new AttWorkingTypeRepository.SearchFilter();
		filter.codes = new Set<String>{code};
		List<AttWorkingTypeBaseEntity> entityList = workingTypeRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildHistoryEntity getHistoryEntity(Id historyId) {
		// ベースに紐づく全ての履歴を取得する
		return workingTypeRepo.getHistoryEntity(historyId);
	}

	/**
	 * 指定した履歴IDの履歴エンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		// ベースに紐づく全ての履歴を取得する
		return workingTypeRepo.getHistoryEntityList(historyIds);
	}

	/**
	 * ベースエンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveBaseEntity(ParentChildBaseEntity base) {
		return (new AttConfigValidator.AttWorkingTypeBaseValidator((AttWorkingTypeBaseEntity)base)).validate();
	}

	/**
	 * 履歴エンティティの値を検証する
	 * @param history 検証対象の履歴エンティティ
	 * @param base 参照用のベースエンティティ（検証はしない）
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveHistoryEntity(ParentChildBaseEntity base, ParentChildHistoryEntity history) {
		return (new AttConfigValidator.AttWorkingTypeHistoryValidator((AttWorkingTypeBaseEntity)base, (AttWorkingTypeHistoryEntity)history)).validate();
	}

	/**
	 * 履歴エンティティが持つnameL0を取得する
	 * @param history 取得対象の履歴エンティティ
	 * @return 履歴エンティティのnameL0、ただしnameL0を持っていない場合はnull
	 */
	protected override String getNameL0FromHistory(ParentChildHistoryEntity history) {
		return ((AttWorkingTypeHistoryEntity)history).nameL0;
	}

	/**
	 * エンティティのコードが重複しているかどうかを確認する(会社単位での重複チェック)
	 * @param base チェック対象のベースエンティティ
	 * @param companyCode 会社コード
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	@TestVisible
	protected override Boolean isCodeDuplicated(ParentChildBaseEntity base) {
		AttWorkingTypeBaseEntity empBase = (AttWorkingTypeBaseEntity)base;
		AttWorkingTypeBaseEntity duplicateBase = getEntityByCode(empBase.code, empBase.companyId);

		if (duplicateBase == null) {
			// 重複していない
			return false;
		}
		if (duplicateBase.id == empBase.id) {
			// 重複していない
			return false;
		}

		// ここまできたら重複している
		return true;
	}

	/**
	 * 指定した勤務体系IDの対象日時点の勤務体系を取得する
	 * @param workingTypeBaseId 勤務体系のベースID
	 * @return 勤務体系、有効な履歴を全て持つ
	 * @throws App.RecordNotFoundException 勤務体系が見つからない場合、対象期間で有効な履歴が見つからない場合
	 */
	public AttWorkingTypeBaseEntity getWorkingType(Id workingTypeBaseId) {
		return getWorkingTypeByRange(workingTypeBaseId, null);
	}

	/**
	 * 指定した勤務体系IDの対象日時点の勤務体系を取得する
	 * @param workingTypeBaseId 勤務体系のベースID
	 * @param targetDate 対象日、nullの場合は全履歴が取得対象
	 * @return 勤務体系
	 * @throws App.RecordNotFoundException 勤務体系が見つからない場合、対象期間で有効な履歴が見つからない場合
	 */
	public AttWorkingTypeBaseEntity getWorkingType(Id workingTypeBaseId, AppDate targetDate) {
		AppDateRange range = targetDate == null ? null : new AppDateRange(targetDate, targetDate);
		return getWorkingTypeByRange(workingTypeBaseId, range);
	}

	/**
	 * 指定した勤務体系を取得する、履歴は指定した期間に有効期間が1日以上含まれる履歴が返却対象となる
	 * @param workingTypeBaseId 勤務体系のベースID
	 * @param targetRange 取得対象の期間、nullの場合は全履歴が返却される
	 *                    target period. if targetRange is null, return all valid history.
	 * @return 勤務体系
	 * @throws App.RecordNotFoundException 勤務体系が見つからない場合、対象期間で有効な履歴が見つからない場合
	 */
	public AttWorkingTypeBaseEntity getWorkingTypeByRange(Id workingTypeBaseId, AppDateRange targetRange) {
		// すべての有効な履歴も含めて対象の社員を取得する
		AttWorkingTypeBaseEntity workingType = workingTypeRepo.getEntity(workingTypeBaseId);
		if (workingType == null) {
			// メッセージ：指定された勤務体系が見つかりません。
			throw new App.RecordNotFoundException(
					ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Att_Lbl_WorkingType}));
		}

		// 対象期間がnullの場合は全履歴を返却
		if (targetRange == null) {
			return workingType;
		}

		// 指定した期間でマスタが有効であることをチェック
		if (! workingType.isValid(targetRange.startDate) || ! workingType.isValid(targetRange.endDate)) {
			// メッセージ：指定された勤務体系は有効期間外です
			throw new App.RecordNotFoundException(
					ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Att_Lbl_WorkingType}));
		}

		// 対象日に有効な履歴を取得する
		List<ParentChildHistoryEntity> targetHistoryList = workingType.getSuperHistoryByDate(targetRange);

		// 対象日以外の履歴を削除する
		workingType.clearHistoryList();
		workingType.addHistoryAll(targetHistoryList);

		return workingType;
	}

	/**
	 * 指定したIDの勤務体系履歴を取得する（論理削除済みの履歴も対象とする）
	 * @param workingTypeHistoryId 勤務体系履歴ID
	 * @return 勤務体系履歴エンティティ
	 * @throws App.IllegalStateException 勤務体系履歴が見つからない場合
	 */
	public AttWorkingTypeHistoryEntity getWorkingTypeHistoryIncludedLogicalDelete(Id workingTypeHistoryId) {
		AttWorkingTypeRepository.SearchFilter wtFilter = new AttWorkingTypeRepository.SearchFilter();
		wtFilter.historyIds = new Set<Id>{workingTypeHistoryId};
		wtFilter.includeRemoved = true; // 論理削除のエンティティも対象
		List<AttWorkingTypeBaseEntity> workingTypeBaseList = workingTypeRepo.searchEntity(wtFilter);
		if (workingTypeBaseList.isEmpty()) {
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_WorkingType}));
		}
		return (AttWorkingTypeHistoryEntity)workingTypeBaseList[0].getSuperHistoryById(workingTypeHistoryId);
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	private AttWorkingTypeBaseEntity getEntityByCode(String code, Id companyId) {
		AttWorkingTypeRepository.SearchFilter filter = new AttWorkingTypeRepository.SearchFilter();
		filter.codes = new Set<String>{code};
		filter.companyIds = new Set<Id>{companyId};
		List<AttWorkingTypeBaseEntity> entityList = workingTypeRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * エンティティのユニークキーを更新する
	 * (会社のコードが変更されている可能性があるため、常に更新を行う)
	 * @param base ユニークキーを作成するエンティティ
	 */
	public void updateUniqKey(AttWorkingTypeBaseEntity base) {

		Id companyId = base.companyId;
		String code = base.code;

		// 既存のエンティティを更新する場合はエンティティに更新対象の項目以外は値が設定されていない可能性があるため
		// リポジトリから更新前のエンティティを取得する
		if ((base.id != null) && ((companyId == null) || (String.isBlank(code)))) {
			// 既存のエンティティを取得する
			AttWorkingTypeBaseEntity orgBase = workingTypeRepo.getBaseEntity(base.id);
			if (orgBase == null) {
				// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			if (companyId == null) {
				companyId = orgBase.companyId;
			}
			if (String.isBlank(code)) {
				code = orgBase.code;
			}
		}

		if (companyId == null ) {
			// メッセージ：会社が設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Company}));
		}
		if (String.isBlank(code)) {
			// メッセージ：コードが設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}));
		}
		CompanyEntity company = getCompany(companyId);
		base.uniqKey = base.createUniqKey(company.code, code);
	}

	/**
	 * 検証結果クラス
	 */
	public class ValidateResult extends ParentChildService.ValidateResult {}

	/**
	 * 勤務体系履歴が改定可能かどうかをチェックする
	 * @param historyList 改定対象の社員履歴リスト
	 * @return チェック結果
	 */
	public List<AttWorkingTypeService.ValidateResult> validateRevisionHistoryList(List<AttWorkingTypeHistoryEntity> historyList) {
		List<ValidateResult> resultList = new List<ValidateResult>();

		// 勤務体系のベースを取得
		Set<Id> workingTypeBaseIdSet = new Set<Id>();
		for (AttWorkingTypeHistoryEntity history : historyList) {
			workingTypeBaseIdSet.add(history.baseId);
		}
		List<AttWorkingTypeBaseEntity> workingTypeBaseList =
				new AttWorkingTypeRepository().getBaseEntityList(new List<Id>(workingTypeBaseIdSet), false);
		Map<Id, AttWorkingTypeBaseEntity> workingTypeBaseMap = new Map<Id, AttWorkingTypeBaseEntity>();
		for (AttWorkingTypeBaseEntity workingType : workingTypeBaseList) {
			workingTypeBaseMap.put(workingType.id, workingType);
		}

		// 勤務体系をチェックする
		for (AttWorkingTypeHistoryEntity history : historyList) {
			ValidateResult result = new ValidateResult();
			resultList.add(result);

			AttWorkingTypeBaseEntity base = workingTypeBaseMap.get(history.baseId);
			if (base == null) {
				// メッセージ：該当する勤務体系が見つかりません。
				String msg = MESSAGE.Com_Err_NotFound(new List<String>{MESSAGE.Att_Lbl_WorkingType});
				result.exceptionList.add(new App.RecordNotFoundException(msg));
				continue;
			}

			// 改定日が月度の開始日と一致しているかを確認
			if (history.validFrom.day() != base.startDateOfMonth) {
				// メッセージ：改定日は月度の開始日にしてください。
				String msg = MESSAGE.Admin_Err_InvalidRevisionDateWorkingType(new List<String>{MESSAGE.Att_Lbl_WorkingTypeBeginDayOfMonth});
				result.exceptionList.add(new App.ParameterException(msg));
				continue;
			}
		}

		return resultList;
	}
}