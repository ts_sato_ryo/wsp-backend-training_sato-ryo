/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Entity Class for ExpExchangeRate Object
 */
public with sharing class ExpExchangeRateEntity extends ExpExchangeRateGeneratedEntity {

	/** Max length of the code field */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** Scale of exchange rate */
	public static final Integer RATE_INT_MAX_LENGTH = 12;
	public static final Integer RATE_DECIMAL_MAX_LENGTH = 6;

	/**
	 * Constructor
	 */
	public ExpExchangeRateEntity() {
		super(new ExpExchangeRate__c());
	}

	/**
	 * Constructor
	 */
	public ExpExchangeRateEntity(ExpExchangeRate__c sobj) {
		super(sobj);
	}

	/** Currency Code(For reference) */
	public String currencyCode {
		get {
			if (currencyCode == null) {
				currencyCode = sobj.CurrencyId__r.IsoCurrencyCode__c;
			}
			return currencyCode;
		}
		set {
			currencyCode = value;
		}
	}

	/** Currency Name(For reference) */
	public AppMultiString currencyNameL {
		get {
			if (currencyNameL == null) {
				currencyNameL = new AppMultiString(sObj.CurrencyId__r.Name_L0__c, sObj.CurrencyId__r.Name_L1__c, sObj.CurrencyId__r.Name_L2__c);
			}
			return currencyNameL;
		}
		set {
			currencyNameL = value;
		}
	}

	public Boolean isCounterCurrency() {
		if (String.isBlank(this.currencyCode)) {
			App.ParameterException e = new App.ParameterException();
			e.setMessageIsBlank('ExpExchangeRateEntity : currencyCode is blank');
			throw e;
		}

		if (this.currencyPair == null) {
			App.ParameterException e = new App.ParameterException();
			e.setMessageIsBlank('ExpExchangeRateEntity : currencyPair is null');
			throw e;
		}

		String counterCurrency = this.currencyPair.value.substring(4, 7);
		return currencyCode.equals(counterCurrency);
	}

	public Decimal calculationRate {
		get {
				return this.isCounterCurrency() ? this.reverseRate : this.rate;
			}
	}

	/**
	 * Duplicate entity(ID is not copied)
	 * @return Duplicated entity
	 */
	public ExpExchangeRateEntity copy() {
		ExpExchangeRateEntity copyEntity = new ExpExchangeRateEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}
}