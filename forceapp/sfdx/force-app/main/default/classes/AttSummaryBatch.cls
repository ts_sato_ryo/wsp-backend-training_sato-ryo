/**
 * @group 勤怠
 *
 * @description 勤怠サマリー調整バッチ
 *              ・対象日を含む勤怠サマリーが存在しない場合、新規に勤怠サマリーを作成する
 *              ・対象日を含む勤怠サマリーが存在するが、社員の履歴が改定されている場合は勤怠計算を行う
 *              ・対象日を含む勤怠サマリーが存在するが、勤怠サマリーのDirtyフラグがONの場合は勤怠計算を行う
 *              注意：本バッチを実行する場合はscopeにAttSummaryBatch.MAX_BATCH_SCOPEを指定してください
 */
public with sharing class AttSummaryBatch implements Database.Batchable<sObject>, Database.Stateful {

	/**
	 * 設定可能なバッチの最大スコープ
	 */
	public static final Integer MAX_BATCH_SCOPE = 1;

	/** @description 対象日 */
	private final AppDate targetDate;

	/**
	 * @description コンストラクタ
	 * @param targetDate 対象日
	 */
	public AttSummaryBatch(AppDate targetDate) {
		this.targetDate = targetDate;
	}

	/**
	 * @description バッチ開始処理
	 * @param bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {

		//--------------------------------------------------------------------
		// TODO 将来的には社員の会社の勤怠利用フラグ(ComCompany__c.UseAttendance__c)が
		//      ONになっていることを確認する必要あり
		//--------------------------------------------------------------------

		// 下記の条件をすべて満たす勤怠サマリー新規作成対象、勤怠計算対象の社員を抽出する
		//  ・対象日を期間に含む勤怠サマリーを持っていない
		//  ・対象日を期間に含む勤怠サマリーを持っているが、Dirtyフラグがtrueである
		//  ・対象日時点で社員が有効である
		//  ・対象日時点で社員に勤務体系が設定されている
		Date td = this.targetDate.getDate();
		String query = 'SELECT Id, BaseId__c, BaseId__r.Code__c, BaseId__r.DisplayName_L0__c, BaseId__r.CompanyId__r.Name_L0__c'
				+ ' FROM ComEmpHistory__c'
				+ ' WHERE Id NOT IN ('
					+ 'SELECT EmployeeHistoryId__c FROM AttSummary__c'
					+ ' WHERE Dirty__c = false AND (StartDate__c <= :td AND EndDate__c >= :td))'
				+ ' AND ValidFrom__c <= :td AND ValidTo__c > :td'
				+ ' AND Removed__c = false'
				+ ' AND WorkingTypeBaseId__c != null'
				+ ' ORDER BY UniqKey__c';
		return Database.getQueryLocator(query);
	}

	/**
	 * @description バッチ実行処理
	 * @param bc バッチコンテキスト
	 * @param scope バッチ処理対象の社員履歴のレコードのリスト
	 */
	public void execute(Database.BatchableContext bc, List<ComEmpHistory__c> scope) {

		// リポジトリのキャッシュ有効化
		enableMasterCaches();

		// scopeリストのレコード数は1件(それ以上はガバナ制限のエラーが発生する可能性があるため)
		if (scope.size() > MAX_BATCH_SCOPE) {
			// 本エラーが発生する場合はバグのため多言語化不要
			throw new App.ParameterException('Scope size is too large (scope.size()=' + scope.size() + ')');
		}

		ComEmpHistory__c employeeHistory = scope[0];

		System.debug('AttSummaryBatch.execute START: '
				+ employeeHistory.BaseId__r.DisplayName_L0__c + '(' + employeeHistory.BaseId__r.Code__c + ')');

		// 勤怠サマリーを新規作成、作成済みの場合は必要に応じて勤怠計算を行う
		try {
			AttAttendanceService service = new AttAttendanceService();
			service.getSummary(employeeHistory.BaseId__c, this.targetDate);
		} catch (Exception e) {
			// エラーメッセージに会社名、社員の氏名とコードを追加しておく
			String msg = employeeHistory.BaseId__r.CompanyId__r.Name_L0__c
					+ ' ' + employeeHistory.BaseId__r.DisplayName_L0__c
					+ '(' + employeeHistory.BaseId__r.Code__c + '): '
					+ e.getMessage();
			e.setMessage(msg);
			throw e;
		}
	}

	/**
	 * @description バッチ終了処理
	 * @param bc バッチコンテキスト
	 */
	public void finish(Database.BatchableContext bc) {
	}

	/**
	 * 勤怠処理で利用するマスタのキャッシュ機能を有効化する
	 * キャッシュを利用しない場合はリポジトリのコンストラクト時に指定する
	 */
	private void enableMasterCaches() {
		// 親子型マスタ
		EmployeeRepository.enableCache();
		DepartmentRepository.enableCache();
		AttWorkingTypeRepository.enableCache();
		TimeSettingRepository.enableCache();
		AttShortTimeSettingRepository.enableCache();
		// 有効期間型マスタ
		AttAgreementAlertSettingRepository.enableCache();
		AttLeaveOfAbsenceRepository.enableCache();
		AttLeaveRepository.enableCache();
		AttPatternRepository2.enableCache();
		// 論理削除型マスタ
		CalendarRepository.enableCache();
		CompanyRepository.enableCache();
	}
}