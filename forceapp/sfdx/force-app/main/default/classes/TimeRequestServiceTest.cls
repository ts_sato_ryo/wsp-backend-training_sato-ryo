/**
 * @group 工数
 *
 * 工数確定申請サービスのテストクラス
 */
@isTest
private class TimeRequestServiceTest {

	private static final TimeRequestService service = new TimeRequestService();

	/**
	 * 工数設定で工数確定申請を利用しない設定の場合、工数確定申請できない事を検証する
	 */
	@isTest
	static void cannotUseSubmitRequestTest() {
		TimeTestData testData = new TimeTestData(AppDate.newInstance(2019, 4, 1));

		// 工数確定利用フラグをOffにする
		testData.timeSetting.UseRequest__c = false;
		update testData.timeSetting;

		System.runAs(testData.user) {
			try {
				service.submit(testData.emp.Id, AppDate.newInstance(2019, 4, 1), 'comment');
				TestUtil.fail('例外が発生しませんでした');
			} catch (App.IllegalStateException e) {
				System.assertEquals(App.ERR_CODE_CANNOT_USE_TIME_REQUEST, e.getErrorCode());
				System.assertEquals(ComMessage.msg().Time_Err_CannotUseRequest, e.getMessage());
			}
		}
	}

	/**
	 * 工数確定の承認者を正しく取得できることを検証する
	 */
	@isTest
	static void getNextApproverTest() {
		// 承認者ユーザ/社員を作成
		TimeTestData testData = new TimeTestData(AppDate.newInstance(2019, 4, 1));
		ComEmpBase__c approver1 = testData.createEmployeeStandardUser('approver1');
		ComEmpBase__c approver2 = testData.createEmployeeStandardUser('approver2');

		// 社員履歴に承認者を設定
		ComEmpHistory__c history1 = testData.empHistory;
		history1.ValidFrom__c = Date.newInstance(2019, 4, 1);
		history1.ValidTo__c = Date.newInstance(2019, 5, 1);
		history1.ApproverBase01Id__c = approver1.Id;

		ComEmpHistory__c history2 = history1.clone();
		history2.UniqKey__c = 'history2';
		history2.ValidFrom__c = Date.newInstance(2019, 5, 1);
		history2.ValidTo__c = Date.newInstance(2019, 6, 1);
		history2.ApproverBase01Id__c = approver2.Id;

		ComEmpHistory__c history3 = history2.clone();
		history3.UniqKey__c = 'history3';
		history3.ValidFrom__c = Date.newInstance(2019, 6, 1);
		history3.ValidTo__c = Date.newInstance(2101, 1, 1);
		history3.ApproverBase01Id__c = approver1.Id;
		upsert new List<ComEmpHistory__c> {history1, history2, history3};

		// 工数サマリーを作成
		TimeSummary__c summaryObj = testData.createMonthlySummary(AppDate.newInstance(2019, 5, 1));
		TimeSummaryEntity summary = new TimeSummaryRepository().getSummary(summaryObj.Id);

		// 承認者を取得
		EmployeeBaseEntity approver = service.getNextApprover(testData.emp.Id, summary);
		// サマリーの終了日時点の履歴を取得することを検証
		System.assertEquals(approver2.Id, approver.Id);
	}

	/**
	 * 工数サマリースナップショットを文字列とクラスに変換できることを検証する
	 * トップレベルの項目のみ検証する
	 */
	@isTest
	static void convertSnapshotTest() {
		// 変換対象のスナップショットを作成する
		TimeRequestService.TimeSummarySnapshot source = new TimeRequestService.TimeSummarySnapshot();
		source.summaryId = 'summaryId';
		source.employeeName = 'employeeName';
		source.employeePhotoUrl = 'URL';
		source.startDate = '2019-04-01';
		source.endDate = '2019-04-30';
		source.workTime = 9600;

		// スナップショットから添付ファイルを作成する
		AppAttachmentEntity attach = new AppAttachmentEntity();
		attach.bodyText = source.toJson();

		// 添付ファイルからスナップショットに変換する
		TimeRequestService.TimeSummarySnapshot dest = new TimeRequestService.TimeSummarySnapshot(attach);
		System.assertEquals('summaryId', dest.summaryId);
		System.assertEquals('employeeName', dest.employeeName);
		System.assertEquals('URL', dest.employeePhotoUrl);
		System.assertEquals('2019-04-01', dest.startDate);
		System.assertEquals('2019-04-30', dest.endDate);
		System.assertEquals(9600, dest.workTime);
	}

	/**
	 * 工数サマリースナップショットを文字列とクラスに変換できることを検証する
	 * 工数サマリーレコードのみ検証する
	 */
	@isTest
	static void convertSnapshotSummaryRecordTest() {
		// 工数実績サマリーレコードを作成する
		TimeRequestService.TimeSummarySnapshot source = new TimeRequestService.TimeSummarySnapshot();
		TimeRequestService.TaskSummaryRecord record1 = new TimeRequestService.TaskSummaryRecord();
		record1.jobId = 'JobId';
		record1.jobCode = 'JobCode';
		record1.jobName = 'JobName';
		record1.workCategoryId = 'WorkCategoryId';
		record1.workCategoryCode = 'WorkCategoryCode';
		record1.workCategoryName = 'WorkCategoryName';
		record1.workTimeRatio = 100.0;
		record1.workTime = 10;
		record1.isDefaultJob = true;
		TimeRequestService.TaskSummaryRecord record2 = new TimeRequestService.TaskSummaryRecord();
		record2.jobId = 'JobId2';
		TimeRequestService.TaskSummaryRecord record3 = new TimeRequestService.TaskSummaryRecord();
		record3.jobId = 'JobId3';
		source.summaryRecords = new List<TimeRequestService.TaskSummaryRecord>{record1, record2, record3};

		// スナップショットから添付ファイルを作成する
		AppAttachmentEntity attach = new AppAttachmentEntity();
		attach.bodyText = source.toJson();

		// 添付ファイルからスナップショットに変換する
		TimeRequestService.TimeSummarySnapshot dest = new TimeRequestService.TimeSummarySnapshot(attach);
		System.assertEquals(3, dest.summaryRecords.size());
		System.assertEquals('JobId', dest.summaryRecords[0].jobId);
		System.assertEquals('JobCode', dest.summaryRecords[0].jobCode);
		System.assertEquals('JobName', dest.summaryRecords[0].jobName);
		System.assertEquals('WorkCategoryId', dest.summaryRecords[0].workCategoryId);
		System.assertEquals('WorkCategoryCode', dest.summaryRecords[0].workCategoryCode);
		System.assertEquals('WorkCategoryName', dest.summaryRecords[0].workCategoryName);

		// リストの順が保証されることを検証
		System.assertEquals('JobId2', dest.summaryRecords[1].jobId);
		System.assertEquals('JobId3', dest.summaryRecords[2].jobId);
	}

	/**
	 * 工数サマリースナップショットを文字列とクラスに変換できることを検証する
	 * 工数明細レコードのみ検証する
	 */
	@isTest
	static void convertSnapshotTimeRecordTest() {
		// 工数明細を作成する
		TimeRequestService.TimeSummarySnapshot source = new TimeRequestService.TimeSummarySnapshot();
		TimeRequestService.TimeRecord record = new TimeRequestService.TimeRecord();
		record.recordDate = '2019-04-01';
		record.note = 'note';
		record.recordItemList = new List<TimeRequestService.TimeRecordItem>();
		source.timeRecords = new List<TimeRequestService.TimeRecord>{record};

		// 工数内訳を作成
		TimeRequestService.TimeRecordItem item = new TimeRequestService.TimeRecordItem();
		item.jobId = 'JobId';
		item.jobCode = 'JobCode';
		item.jobName = 'JobName';
		item.workCategoryId = 'WorkCategoryId';
		item.WorkCategoryCode = 'WorkCategoryCode';
		item.workCategoryName = 'WorkCategoryName';
		item.isDirectInput = true;
		item.taskTime = 100;
		item.ratio = null;
		item.taskNote = 'taskNote';
		item.order = 1;
		record.recordItemList.add(item);

		// スナップショットから添付ファイルを作成する
		AppAttachmentEntity attach = new AppAttachmentEntity();
		attach.bodyText = source.toJson();

		// 添付ファイルからスナップショットに変換する
		TimeRequestService.TimeSummarySnapshot dest = new TimeRequestService.TimeSummarySnapshot(attach);
		System.assertEquals(1, dest.timeRecords.size());
		System.assertEquals('2019-04-01', dest.timeRecords[0].recordDate);
		System.assertEquals('note', dest.timeRecords[0].note);
		System.assertEquals(1, dest.timeRecords[0].recordItemList.size());
		TimeRequestService.TimeRecordItem destItem = dest.timeRecords[0].recordItemList[0];
		System.assertEquals('JobId', destItem.jobId);
		System.assertEquals('JobCode', destItem.jobCode);
		System.assertEquals('JobName', destItem.jobName);
		System.assertEquals('WorkCategoryId', destItem.workCategoryId);
		System.assertEquals('WorkCategoryCode', destItem.workCategoryCode);
		System.assertEquals('WorkCategoryName', destItem.workCategoryName);
		System.assertEquals(true, destItem.isDirectInput);
		System.assertEquals(100, destItem.taskTime);
		System.assertEquals(null, destItem.ratio);
		System.assertEquals('taskNote', destItem.taskNote);
		System.assertEquals(1, destItem.order);
	}

	/**
	 * createRequestName のテスト
	 * 日本語・英語の言語によって正しい申請名を取得できること
	 * 月次・週次のサマリー期間によって正しい申請名を取得できること
	 */
	@isTest
	static void createRequestNameTest() {
		TimeRequestService service = new TimeRequestService();
		// 言語設定を作成(L0=日本語, L1=英語)
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		// テストデータ作成
		// - 社員
		EmployeeBaseEntity employee = new EmployeeBaseEntity();
		employee.lastNameL0 = '日本語';
		employee.firstNameL0 = '太郎';
		employee.lastNameL1 = 'Japanese';
		employee.firstNameL1 = 'Taro';
		employee.displayNameL0 = employee.getDefaultDisplayNameL0(AppLanguage.JA);
		employee.displayNameL1 = employee.getDefaultDisplayNameL1(AppLanguage.EN_US);

		// Case 1: サマリー単位が月次　会社言語が日本語
		{
			// テストデータ作成
			// - サマリー
			TimeSummaryEntity summary = new TimeSummaryEntity();
			summary.startDate = AppDate.newInstance(2019,8,15);
			summary.endDate = AppDate.newInstance(2019,9,14);
			// - 工数設定
			TimeSettingBaseEntity timeSetting = new TimeSettingBaseEntity();
			timeSetting.setMonthMark('BeginBase');
			timeSetting.setSummaryPeriod('Month');

			// 実行
			String actualRequestName = service.createRequestName(
					employee, AppLanguage.JA, summary, timeSetting);

			// 検証
			System.assertEquals('日本語 太郎さん 工数確定 2019/08', actualRequestName);
		}
		// Case 2: サマリー単位が月次　会社言語が英語
		{
			// テストデータ作成
			// - サマリー
			TimeSummaryEntity summary = new TimeSummaryEntity();
			summary.startDate = AppDate.newInstance(2019, 8, 15);
			summary.endDate = AppDate.newInstance(2019, 9, 14);
			// - 工数設定
			TimeSettingBaseEntity timeSetting = new TimeSettingBaseEntity();
			timeSetting.setMonthMark('BeginBase');
			timeSetting.setSummaryPeriod('Month');

			// 実行
			String actualRequestName = service.createRequestName(
					employee, AppLanguage.EN_US, summary, timeSetting);

			// 検証
			System.assertEquals('Taro Japanese Time Report Aug 2019', actualRequestName);
		}
		// Case 3: サマリー単位が週次　会社言語が日本語
		{
			// テストデータ作成
			// - サマリー
			TimeSummaryEntity summary = new TimeSummaryEntity();
			summary.startDate = AppDate.newInstance(2019, 7, 28);
			summary.endDate = AppDate.newInstance(2019, 8, 3);
			summary.subNo = 4;
			// - 工数設定
			TimeSettingBaseEntity timeSetting = new TimeSettingBaseEntity();
			timeSetting.setMonthMark('BeginBase');
			timeSetting.setSummaryPeriod('Week');

			// 実行
			String actualRequestName = service.createRequestName(
					employee, AppLanguage.JA, summary, timeSetting);

			// 検証
			System.assertEquals('日本語 太郎さん 工数確定 2019/07 4週目', actualRequestName);
		}
		// Case 4: サマリー単位が週次　会社言語が英語
		{
			// テストデータ作成
			// - サマリー
			TimeSummaryEntity summary = new TimeSummaryEntity();
			summary.startDate = AppDate.newInstance(2019, 7, 28);
			summary.endDate = AppDate.newInstance(2019, 8, 3);
			summary.subNo = 1;
			// - 工数設定
			TimeSettingBaseEntity timeSetting = new TimeSettingBaseEntity();
			timeSetting.setMonthMark('EndBase');
			timeSetting.setSummaryPeriod('Week');

			// 実行
			String actualRequestName = service.createRequestName(
					employee, AppLanguage.EN_US, summary, timeSetting);

			// 検証
			System.assertEquals('Taro Japanese Time Report Aug 2019 1st Week', actualRequestName);
		}
	}
}
