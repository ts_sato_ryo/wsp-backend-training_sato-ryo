/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 勤怠
 *
 * 短時間勤務実績時間移行バッチ（Summer'19にアップグレードする際に実行するバッチです）
 * 短時間勤務設定が適用されている社員の勤怠サマリに対して、短時間勤務実績時間を計算して保存する
 */
public with sharing class AttShortenedWorkTimeMigrationBatch implements Database.Batchable<sObject>, Database.Stateful {

	/**
	 * バッチ開始処理
	 * @param bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {

		// フレックス制の短時間勤務設定のみを対象とする（短時間勤務実績時間の計算方法は労働時間制によって異なるため）
		String targetWorkSystem = AttWorkSystem.JP_FLEX.value;

		// クエリを作成する（勤怠期間から社員IDを取得する）
		String query = 'SELECT Id,' +
				' EmployeeBaseId__c' +
				' FROM AttPeriodStatus__c' +
				' WHERE ShortTimeWorkSettingBaseId__r.WorkSystem__c = :targetWorkSystem' +
				' AND IsDeleted = false';
		return Database.getQueryLocator(query);
	}

	/**
	 * バッチ実行処理
	 * @param bc バッチコンテキスト
	 * @param scope バッチ処理対象レコードのリスト
	 */
	public void execute(Database.BatchableContext bc, List<AttPeriodStatus__c> scope) {

		// 勤怠期間から社員IDを取得する
		Set<Id> empBaseIdSet = new Set<Id>();
		for (AttPeriodStatus__c periodStatusObj : scope) {
			empBaseIdSet.add(periodStatusObj.EmployeeBaseId__c);
		}

		// 社員の勤怠サマリを取得する
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttSummaryRepository.SummaryFilter summaryFilter = new AttSummaryRepository.SummaryFilter();
		summaryFilter.employeeIdList = new List<Id>(empBaseIdSet);
		List<AttSummaryEntity> summaryList = summaryRepo.searchEntity(summaryFilter, true, true);

		// 勤務体系ベースを全て取得する
		AttWorkingTypeRepository.SearchBaseFilter workingTypefilter = new AttWorkingTypeRepository.SearchBaseFilter();
		List<AttWorkingTypeBaseEntity> workingTypeList = new AttWorkingTypeRepository().searchBaseEntity(workingTypefilter, false);
		// フレックス制の勤務体系IDを纏める
		Set<Id> workingTypeIdsOnlyFlex = new Set<Id>();
		for (AttWorkingTypeBaseEntity workingType : workingTypeList) {
			if (workingType.workSystem == AttWorkSystem.JP_FLEX) {
				workingTypeIdsOnlyFlex.add(workingType.id);
			}
		}

		// 短時間勤務実績時間を計算する
		AttCalcService calcService = new AttCalcService();
		for (AttSummaryEntity summary : summaryList) {
			// 短時間勤務が適用されていて、労働時間制がフレックス制の勤怠サマリのみ計算する
			if (summary.isShortWorkTimeEvenOneDay() && workingTypeIdsOnlyFlex.contains(summary.workingTypeBaseId)) {
				summary.outActualShortenedWorkTime = calcService.calcFlexShortededWorkTimeForBatch(summary);
			}
		}

		summaryRepo.saveEntityList(summaryList);
	}

	/**
	 * バッチ終了処理
	 * @param bc バッチコンテキスト
	 */
	public void finish(Database.BatchableContext bc) {
		// 処理なし
	}
}