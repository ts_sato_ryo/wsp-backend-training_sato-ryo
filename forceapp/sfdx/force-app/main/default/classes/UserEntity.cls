/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description Salesforceユーザのエンティティ
 */
public with sharing class UserEntity extends Entity {

	/**
	 * @description 参照先のプロファイル
	 */
	public class Profile {
		/** プロファイル名 */
		public String name;
	}

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		USER_NAME,
		USER_EMAIL,
		LANGUAGE,
		PHOTO_URL,
		PROFILE_ID
	}


	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/** ユーザ氏名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** ユーザ名(SalesforceログインID) */
	public String userName {
		get;
		set {
			userName = value;
			setChanged(Field.USER_NAME);
		}
	}
	/** メールアドレス */
	public String email {
		get;
		set {
			email = value;
			setChanged(Field.USER_EMAIL);
		}
	}
	/** ユーザの言語 */
	public String language {
		get;
		set {
			language = value;
			setChanged(Field.LANGUAGE);
		}
	}

	/** プロファイル写真のURL */
	public String smallPhotoUrl {
		get;
		set {
			smallPhotoUrl = value;
			setChanged(Field.PHOTO_URL);
		}
	}

	/** プロファイルID */
	public String profileId {
		get;
		set {
			profileId = value;
			setChanged(Field.PROFILE_ID);
		}
	}

	/** プロファイル(参照専用) */
	public UserEntity.Profile profile {
		get;
		set;
	}

	/**
	 * コンストラクタ
	 */
	public UserEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}


}