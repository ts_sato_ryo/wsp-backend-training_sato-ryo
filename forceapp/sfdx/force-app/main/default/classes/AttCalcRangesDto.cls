/**
 * FIXME：V5既存ソースコードをリプレース
 */
public with sharing class AttCalcRangesDto {
	public static AttCalcRangeDto R(Integer x, Integer y) { return new AttCalcRangeDto(x,y); }
	public static AttCalcRangeDto R(AttCalcRangeDto r) { return new AttCalcRangeDto(r); }
	public static AttCalcRangesDto RS() { return new AttCalcRangesDto(); }
	public static AttCalcRangesDto RS(AttCalcRangesDto rs) { return new AttCalcRangesDto(rs); }
	public static AttCalcRangesDto RS(List<AttCalcRangeDto> rList) { return new AttCalcRangesDto(rList); }
	public static AttCalcRangesDto RS(Integer x, Integer y) { return (x < y) ? new AttCalcRangesDto(x,y) : new AttCalcRangesDto(); }

	private List<AttCalcRangeDto> ranges;
	public AttCalcRangesDto() { ranges = new List<AttCalcRangeDto>(); }
	public AttCalcRangesDto(Integer st, Integer et) { ranges = new List<AttCalcRangeDto>(); ranges.add(new AttCalcRangeDto(st, et)); }
	public AttCalcRangesDto(Integer st, Integer et, Integer f) { ranges = new List<AttCalcRangeDto>(); ranges.add(new AttCalcRangeDto(st, et, f)); }
	public AttCalcRangesDto(AttCalcRangesDto tr) { ranges = new List<AttCalcRangeDto>(tr.ranges); }
	public AttCalcRangesDto(List<AttCalcRangeDto> rangeList) { ranges = rangeList; }
	public Integer size() { return ranges.size(); }
	public AttCalcRangeDto get(Integer i) { return ranges.get(i); }
	public AttCalcRangeDto getFirst() { return (ranges.size() <= 0) ? null : ranges.get(0); }
	public AttCalcRangeDto getLast(Integer i) { return (ranges.size() <= 0) ? null : ranges.get(ranges.size()-1); }
	public void add(AttCalcRangeDto r) { if(r != null && r.st < r.et) ranges.add(r); }
	public void forceAdd(AttCalcRangeDto r) { if(r != null) ranges.add(r); }
	public void add(Integer i, AttCalcRangeDto r) { if(r != null && r.st < r.et) ranges.add(i, r); }
	public void add(AttCalcRangesDto tr) { for(AttCalcRangeDto r: tr.ranges) add(r); }
	public Integer width() { Integer w = 0; for(AttCalcRangeDto r: ranges) w += r.width(); return w; }
	public Integer width(Integer t) { Integer w = 0; for(AttCalcRangeDto r: ranges) if(r.flag==t)w += r.width(); return w; }
	public void removeAt(Integer n) { ranges.remove(n); }
	public Integer timeFromStart(Integer w) {
		for(AttCalcRangeDto r: ranges) {
			if(w <= r.width()) return r.st + w;
			w -= r.width();
		}
		return (ranges.size() <= 0) ? 0 : ranges.get(ranges.size()-1).et;
	}
	public Integer timeFromEnd(Integer w) {
		for(Integer i = ranges.size() - 1; i >= 0; i--) {
			AttCalcRangeDto r = ranges.get(i);
			if(w <= r.width()) return r.et - w;
			w -= r.width();
		}
		return (ranges.size() <= 0) ? 0 : ranges.get(0).st;
	}
	/**
	 * 時刻 t より前の範囲にフラグfを設定する.設定した時間の合計を返す
	 */
	public Integer markBefore(Integer t, Integer f) {
		Integer w = 0;
		for(Integer i = 0; i < ranges.size(); i++) {
			AttCalcRangeDto r = ranges.get(i);
			if(r.st>=t) break;
			else if(r.et<=t) { r.flag = f; w += r.et - r.st; }
			else { ranges.add(i, new AttCalcRangeDto(r.st, t, f)); w += t - r.st; r.st = t; break;  }
		}
		return w;
	}
	/**
	 * 時刻 t より後ろの範囲にフラグfを設定する.設定した時間の合計を返す
	 */
	public Integer markAfter(Integer t, Integer f) {
		Integer w = 0;
			for(Integer i = ranges.size()-1; i >= 0; i--) {
			AttCalcRangeDto r = ranges.get(i);
			if(r.et<=t) break;
			else if(r.st>=t) { r.flag = f; w += r.et - r.st; }
			else { ranges.add(i, new AttCalcRangeDto(r.st, t, r.flag));  r.st = t; r.flag = f; w += r.et - t;  break;  }
		}
		return w;
	}
	/**
	 * 時刻 t1 よりt2の間の範囲にフラグfを設定する.設定した時間の合計を返す
	 */
	public Integer markRange(Integer t1, Integer t2, Integer f) {
		Integer w = 0;
		for(Integer i = 0; i < ranges.size(); i++) {
			AttCalcRangeDto r = ranges.get(i);
			if(r.et <= t1) continue;
			if(r.st >= t2) break;
			if(r.st < t1) {
				AttCalcRangeDto r1 = new AttCalcRangeDto(r.st, t1, r.flag);
				ranges.add(i++, r1);
				r.st = t1;
			}
			if(r.et > t2) {
				if(t1 < t2) {
					AttCalcRangeDto r2 = new AttCalcRangeDto(r.st, t2, f);
					w += t2 - r.st;
					ranges.add(i++, r2);
				}
				r.st = t2;
				break;
		   	}
		   	r.flag = f;
		   	w += r.et - r.st;
		}
		return w;
	}
	/**
	 * フラグfの時刻を削除する
	 */
	public void remove(Integer f) {
		for(Integer i = ranges.size()-1; i >= 0; i--) {
			AttCalcRangeDto r = ranges.get(i);
			if(r.flag == f) ranges.remove(i);
		}
	}
	/**
	 * フラグfの時刻を削除する
	 */
	public void remove(String id) {
		for(Integer i = ranges.size()-1; i >= 0; i--) {
			AttCalcRangeDto r = ranges.get(i);
			if(r.Id == id) ranges.remove(i);
		}
	}
	/**
	 * 時刻 t1 よりt2の間の範囲にフラグfを足す.設定した時間の合計を返す
	 */
	public Integer addMarkRange(Integer t1, Integer t2, Integer f) {
		Integer w = 0;
		if(t1 >= t2) return w;
		for(Integer i = 0; i < ranges.size(); i++) {
			AttCalcRangeDto r = ranges.get(i);
			if(r.et <= t1) continue;
			if(r.st >= t2) break;
			if(r.st < t1) {
				AttCalcRangeDto r1 = new AttCalcRangeDto(r.st, t1, r.flag);
				ranges.add(i++, r1);
				r.st = t1;
			}
			if(r.et > t2) {
				AttCalcRangeDto r2 = new AttCalcRangeDto(r.st, t2, r.flag + f);
				w += t2 - r.st;
				ranges.add(i++, r2);
				r.st = t2;
				break;
			}
			r.flag += f;
			w += r.et - r.st;
		}
		return w;
	}
	public Integer startTime() { return (ranges.size() > 0) ? ranges.get(0).st : null; }
	public Integer endTime() { final Integer n = ranges.size(); return (n > 0) ? ranges.get(n - 1).et : null; }
	/**
	 * tより前の範囲だけを切り出す
	 * @param t 基準時刻
	 * @return tより前の範囲を含むRanges
 	 */
	public AttCalcRangesDto cutBefore(Integer t) { AttCalcRangesDto rs = new AttCalcRangesDto(); for(AttCalcRangeDto r: ranges) rs.add(r.cutBefore(t)); return rs; }
	/**
	 * tより後の範囲だけを切り出す
	 * @param t 基準時刻
	 * @return tより後の範囲を含むRanges
	 */
	public AttCalcRangesDto cutAfter(Integer t) { AttCalcRangesDto rs = new AttCalcRangesDto(); for(AttCalcRangeDto r: ranges) rs.add(r.cutAfter(t)); return rs; }
	/**
	 * 指定された間の範囲だけを切り出す
	 * @param st 開始時刻
	 * @param et 終了時刻
	 * @return (s,e)の間の範囲を含むRanges
	 */
	public AttCalcRangesDto cutBetween(Integer st, Integer et) { AttCalcRangesDto rs = new AttCalcRangesDto(); for(AttCalcRangeDto r: ranges) rs.add(r.cutBetween(st, et)); return rs; }
	public AttCalcRangesDto exclude(AttCalcRangeDto r0) { AttCalcRangesDto rs = new AttCalcRangesDto(); for(AttCalcRangeDto r: ranges) r.exclude(rs, r0); return rs; }
	/**
	 * thisの中でrs0と重なっている部分を取り除いたものを返す
	 * @param rs0 取り除く範囲
	 * @return rs0が取り除かれたRanges
	 */
	public AttCalcRangesDto exclude(AttCalcRangesDto rs0) {
		if(ranges.size() == 0 || rs0.size() == 0) return this;
		Integer j = 0;
		AttCalcRangeDto r0 = rs0.ranges.get(j++);
		AttCalcRangesDto rs = new AttCalcRangesDto();
		for(AttCalcRangeDto r: ranges) {
			while(r0 != null && r0.et < r.et) {
				if(r0.et > r.st) {
					rs.add(r.cutBefore(r0.st));
					r = new AttCalcRangeDto(r0.et, r.et, r.flag);
				}
				r0 = (j < rs0.ranges.size()) ? rs0.ranges.get(j++) : null;
			}
			rs.add((r0 == null) ? r : r.cutBefore(r0.st));
		}
		return rs;
	}
	public AttCalcRangesDto include(AttCalcRangesDto rs0) {
		AttCalcRangesDto rs = new AttCalcRangesDto();
		if(ranges.size() == 0 || rs0.size() == 0) return rs;
		for(AttCalcRangeDto r: ranges) {
			for(AttCalcRangeDto r0: rs0.ranges) {
				if(r.et <= r0.st) break;
				rs.add(r.cutBetween(r0.st, r0.et));
			}
		}
		return rs;
	}

	public void insertOrdered(AttCalcRangeDto r0) {
		if(r0 == null) return;
		//後ろからチェックする（ガバナ対応）
		Integer i = ranges.size();
		for(;i > 0; i--)
		{
				AttCalcRangeDto r = ranges.get(i-1);
				if(r0.st0 > r.st0 || (r0.st0 == r.st0 && r0.et0 > r.et0) || (r0.st0 == r.st0 && r0.et0 == r.et0 && r0.flag >= r.flag))
				{
					if(i < ranges.size())
						ranges.add(i, r0);
					else
						ranges.add(r0);
					return;
				}
		}
		if(i < ranges.size())
				ranges.add(i,r0);
		else
			ranges.add(r0);
	}
	public void norm() {
		if(ranges.size() <= 1) return;
		AttCalcRangeDto r0 = ranges.get(ranges.size() - 1);
		for(Integer i = ranges.size() - 2; i >= 0; i--) {
			AttCalcRangeDto r = ranges.get(i);
			if(r.et >= r0.st && r.flag == r0.flag) {
				r.et = r0.et;
				ranges.remove(i + 1);
			}
			r0 = r;
		}
	}
	public void insertOrdered(AttCalcRangesDto tr) { for(AttCalcRangeDto r: tr.ranges) insertOrdered(r); }
	public void insertAndMerge(AttCalcRangeDto r0) {
		if(r0 == null) return;
		Integer i = 0;
		AttCalcRangeDto r;
		for(; i < ranges.size(); i++) {
			r = ranges.get(i);
			if(r.et >= r0.st) break;
		}
		if(i < ranges.size() && r.st <= r0.et) {
			Integer st = r.st;
			Integer f = r0.flag;
			if(r0.st < st) st = r0.st;
			if(f == 0) f = r.flag;
			Integer et = r0.et;
			Integer j = i;
			for(;j < ranges.size(); j++) {
				r = ranges.get(j);
				if(r.st > r0.et) break;
				if(r.et > et) et = r.et;
				if(f == 0) f = r.flag;
			}
			if(r0.st != st || r0.et != et)
				r0 = new AttCalcRangeDto(st, et, f);
			while(j > i)
				ranges.remove(--j);
		}
		if(i < ranges.size())
			ranges.add(i, r0);
		else
			ranges.add(r0);
	}
	public void insertAndOverwrite(AttCalcRangeDto r0) {
		List<AttCalcRangeDto> rangesNew = new List<AttCalcRangeDto>();
		if(r0 == null) return;
		Integer i = 0;
		AttCalcRangeDto r;
		for(; i < ranges.size(); i++) {
			r = ranges.get(i);
			if(r.et >= r0.st) break;
			rangesNew.add(r);
		}
		if(i < ranges.size() && r.st < r0.st)
			rangesNew.add(r.cutBefore(r0.st));
		rangesNew.add(r0);
		for(;i < ranges.size(); i++) {
			r = ranges.get(i);
			if(r.et > r0.et) break;
		}
		if(i < ranges.size()) {
			r = r.cutAfter(r0.et);
			if(r != null)
				rangesNew.add(r);
			i++;
		}
		for(;i < ranges.size(); i++) {
			r = ranges.get(i);
			rangesNew.add(r);
		}
		ranges = rangesNew;
	}
	public AttCalcRangesDto insertAndMerge(AttCalcRangesDto tr) {
		for(AttCalcRangeDto r: tr.ranges) {
			insertAndMerge(r);
		}
		return RS(ranges);
	}
	public void insertAndOverwrite(AttCalcRangesDto tr) { for(AttCalcRangeDto r: tr.ranges) insertAndOverwrite(r); }
	public void setFlag(Integer f) { for(AttCalcRangeDto r: ranges) r.flag = f; }
	public boolean equals(AttCalcRangesDto tr) {
		if(this == tr) return true;
		if(size() != tr.size()) return false;
		for(Integer i = 0; i < ranges.size(); i++)
			if(! ranges.get(i).equals(tr.ranges.get(i))) return false;
		return true;
	}
	public String toStr() {
		String sep = ''; String str = '[';
		for(AttCalcRangeDto r: ranges) {
			str = str + sep + r.toStr(); sep = ',';
		}
		return str + ']';
	}

	// 以下、新ロジック
	// 関数マッピング表
	// merge -> insertAndMerge
	// mask -> include
	/**
	 * 含まれる範囲の合計時刻を返す
	 * @return 合計時刻
	 */
	public Integer total() {
		Integer w = 0;
		for(AttCalcRangeDto r: ranges)
			w += r.time();
		return w;
	}
	public override String toString() {
		List<String> retList= new List<String>();
		 for(AttCalcRangeDto r: ranges) {
			retList.add(r.toString());
		}
		return '[' + String.join(retList, ',') + ']';
	}
}