/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数確定申請のトリガーハンドラー
 */
public with sharing class TimeRequestTriggerHandler extends RequestTriggerHandler{
	public override Set<Id> getApprovedRequestIds() {
		return approvedRequestMap.keySet();
	}

	public override Set<Id> getDisabledRequestIds() {
		return disabledRequestMap.keySet();
	}
	private Map<Id, TimeRequest__c> approvedRequestMap = new Map<id, TimeRequest__c>();
	private Map<Id, TimeRequest__c> disabledRequestMap = new Map<id, TimeRequest__c>();

	/** 更新対象の工数サマリー */
	public List<TimeSummary__c> updatedTimeSummaryList {
		get {
			List<TimeSummary__c> retList = new List<TimeSummary__c>();
			for (TimeRequest__c disabledRequest : disabledRequestMap.values()) {
				retList.add(new TimeSummary__c(Id = disabledRequest.TimeSummaryId__c, Locked__c = false));
			}
			return retList;
		}
		private set;
	}

	/**
	 * コンストラクタ
	 * @param oldMap
	 * @param newMap
	 */
	public TimeRequestTriggerHandler(Map<Id, TimeRequest__c> oldMap, Map<Id, TimeRequest__c> newMap) {
		super();
		this.init(oldMap, newMap);
	}

	/**
	 * 申請データを更新する
	 */
	public override void applyProcessInfo() {
		// 承認履歴を取得
		Set<Id> targetObjectIds = new Set<Id>();
		targetObjectIds.addAll(approvedRequestIds);
		targetObjectIds.addAll(disabledRequestIds);
		Map<Id, ProcessInstanceStep> processInfoMap = getProcessInfo(targetObjectIds);

		// 承認済みになった申請レコードを更新
		for (TimeRequest__c request : approvedRequestMap.values()) {
			if (!processInfoMap.containsKey(request.id)) {
				continue;
			}
			ProcessInstanceStep pis = processInfoMap.get(request.id);
			request.LastApproverId__c = pis.ActorId;
			request.LastApproveTime__c = pis.CreatedDate;
			request.ProcessComment__c = pis.Comments;
		}

		// 却下された申請レコードを更新
		for (TimeRequest__c request : disabledRequestMap.values()) {
			request.ConfirmationRequired__c = true;
			if (!processInfoMap.containsKey(request.id)) {
				continue;
			}
			ProcessInstanceStep pis = processInfoMap.get(request.id);
			// 却下
			if (pis.StepStatus == STEP_STATUS_REJECTED) {
				request.CancelType__c = AppCancelType.REJECTED.value;
				request.ProcessComment__c = pis.Comments;
			}
			// 申請取消
			else if (pis.StepStatus == STEP_STATUS_REMOVED) {
				request.CancelType__c = AppCancelType.REMOVED.value;
				request.CancelComment__c = pis.comments;
			}
			// 承認取消
			else if (pis.StepStatus == STEP_STATUS_APPROVED ) {
				request.CancelType__c = AppCancelType.CANCELED.value;
				request.LastApproveTime__c = null;
				request.LastApproverId__c = null;
				request.ProcessComment__c = null;
			}
		}
	}

	/**
	 * 承認済み・却下・申請取消になった工数確定申請IDを取得する
	 * @param oldMap
	 * @param newMap
	 */
	private void init(Map<Id, TimeRequest__c> oldMap, Map<Id, TimeRequest__c> newMap) {
		for (Id requestId : newMap.keySet()) {
			TimeRequest__c requestNew = newMap.get(requestId);
			TimeRequest__c requestOld = oldMap.get(requestId);

			// 承認判定：ステータス→承認済み
			if (AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(requestNew.Status__c))
						&& !AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(requestOld.Status__c))) {
				approvedRequestMap.put(requestNew.Id, requestNew);
			}
			else if (AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(requestNew.Status__c))
						&& !AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(requestOld.Status__c))) {
				disabledRequestMap.put(requestNew.Id, requestNew);
			}
		}
	}
}