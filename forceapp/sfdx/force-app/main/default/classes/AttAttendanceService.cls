/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠に関するサービスを提供するクラス
 */
public with sharing class AttAttendanceService implements Service {

	/** 社員サービス */
	private EmployeeService empService;
	/** 勤務体系サービス */
	private AttWorkingTypeService workingTypeService;

	/**
	 * コンストラクタ
	 */
	public AttAttendanceService() {
		 empService = new EmployeeService();
		 workingTypeService = new AttWorkingTypeService();
	}

	/**
	 * 選択可能な勤怠サマリー情報
	 */
	public class SelectPeriod {
		/** 勤怠サマリー名 */
		public String name;
		/** 開始日 */
		public AppDate startDate;
		/** 終了日 */
		public AppDate endDate;
	}

	/**
	 * 選択可能な集計期間の一覧を作成する
	 * @param employeeId 社員ID
	 * @param targetDate 対象日付
	 * @return 勤怠データ（明細リストを含むサマリー)
	 */
	public List<AttAttendanceService.SelectPeriod> createPeriodList(
			final Id employeeId, final AppDate targetDate) {

		// 社員を取得する(全ての履歴付きで取得)
		EmployeeBaseEntity employee = getEmployee(employeeId);
		EmployeeHistoryEntity currentEmpHistory = getEmployeeHistory(employee, targetDate);

		// 勤怠に必要な情報が設定されていることを確認
		validateEmployeeHistory(currentEmpHistory);

		// 勤務体系情報を取得する(ベースのみ)
		AttWorkingTypeBaseEntity workingType = getWorkingType(currentEmpHistory.workingTypeId);
		validateWorkingType(workingType, null);

		// 過去1年分＋現在＋未来2ヶ月分の勤怠サマリー一覧を作成する
		// 将来的には
		//   社員の履歴を考慮する予定。
		//   入社日、退社日を考慮する予定
		//   清算期間が月次以外にも対応する予定
		List<AttAttendanceService.SelectPeriod> selectList;

		// 現在は月度のみ対応
		if (workingType.payrollPeriod == AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month) {
			// 月度の場合
			selectList = createSelectSummaryListMonth(targetDate, workingType, employee);
		} else {
			// メッセージ：勤務体系に未対応の清算期間が設定されています。（清算期間=[%1])
			throw new App.UnsupportedException(
					ComMessage.msg().Att_Err_WorkingTypePayrollPeriodNotSupported(new List<String>{workingType.payrollPeriod.name()}));
		}

		return selectList;
	}

	/**
	 * 選択可能な勤怠サマリー一覧を作成する(月度)
	 * @param targetDate 基準となる日
	 * @param workingType 勤務体系(使用するのはベースのみ)
	 * @param employee 社員(全ての履歴を持っていること)
	 * @return 勤怠サマリー一覧
	 */
	private List<AttAttendanceService.SelectPeriod> createSelectSummaryListMonth(
			AppDate targetDate, AttWorkingTypeBaseEntity workingType, EmployeeBaseEntity employee) {
		final Integer listSize = 15; // 作成するリストのアイテム数(月度数)

		// 開始日（2ヶ月先）
		final AppDate startDate = targetDate.addMonths(2);

		// リスト作成
		final String lang = UserInfo.getLanguage();
		List<AttAttendanceService.SelectPeriod> selectList = new List<SelectPeriod>();
		for (Integer i = 0; i < listSize; i++) {
			// AttWorkingTypeの清算期間と起算日情報から、月度の範囲の日付を確定する
			AppDate dt = startDate.addMonths(i * -1);
			SummaryRange range = calcSummaryRange(workingType.payrollPeriod, workingType.startDateOfMonth, dt);
			SelectPeriod sm = new SelectPeriod();
			sm.startDate = range.startDate;
			sm.endDate = range.endDate;

			EmployeeHistoryEntity startEmployee = employee.getHistoryByDate(sm.startDate);
			EmployeeHistoryEntity endEmployee = employee.getHistoryByDate(sm.endDate);

			// 月度の全期間で社員が無効であるならば、リストに追加しない
			Date employeeStartDate = employee.getOldestSuperHistory().validFrom.getDate();
			Date employeeEndDate = employee.getLatestSuperHistory().validTo.addDays(-1).getDate(); // 失効日の前日
			if (! (sm.startDate.getDate() <= employeeEndDate
					&& sm.endDate.getDate() >= employeeStartDate)) {
				// 全く期間が重複していないのでリストには追加しない
				continue;
			}

			// 月度開始日時点で社員が有効でなければ、開始日を変更する
			if (sm.startDate.getDate() < employeeStartDate) {
				sm.startDate = AppDate.valueOf(employeeStartDate);
			}

			// 月度終了日時点で社員が有効でなければ、終了日を変更する
			if (sm.endDate.getDate() > employeeEndDate) {
				sm.endDate = AppDate.valueOf(employeeEndDate);
			}

			// 集計期間名
			AppDate nameDate;
			if (workingType.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase) {
				nameDate = range.startDate;
			} else if (workingType.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase) {
				nameDate = range.endDate;
			}
			sm.name = createSelectSummryName(nameDate, lang);

			selectList.add(sm);
		}

		return selectList;
	}

	/**
	 * 選択可能な勤怠サマリーの表示名を作成する（多言語対応)
	 * @param dt 対象日
	 * @param lang ユーザの言語
	 */
	private static String createSelectSummryName(AppDate dt, String lang) {
		String name;
		if (lang == AppLanguage.JA.value) {
			// 日本語(例: xxxx年xx月)
			name = dt.year() + '年' + dt.format(AppDate.FormatType.MM) + '月';

		} else {
			// 英語(例: Jul 2017)
			name = dt.format(AppDate.FormatType.MMMYYYY);
		}
		return name;
	}

	/**
	 * 勤務時間保存用のパラメータ
	 */
	public class SaveDailyAttTimeParam {
		/** 登録対象日(YYYY-MM-DD) */
		public AppDate targetDate;
		/** 出勤時刻 */
		public AttTime startTime;
		/** 退勤時刻 */
		public AttTime endTime;
		/** 休憩1開始時刻 */
		public AttTime rest1StartTime;
		/** 休憩1終了時刻 */
		public AttTime rest1EndTime;
		/** 休憩2開始時刻 */
		public AttTime rest2StartTime;
		/** 休憩2終了時刻 */
		public AttTime rest2EndTime;
		/** 休憩3開始時刻 */
		public AttTime rest3StartTime;
		/** 休憩3終了時刻 */
		public AttTime rest3EndTime;
		/** 休憩4開始時刻 */
		public AttTime rest4StartTime;
		/** 休憩4終了時刻 */
		public AttTime rest4EndTime;
		/** 休憩5開始時刻 */
		public AttTime rest5StartTime;
		/** 休憩5終了時刻 */
		public AttTime rest5EndTime;
		/** その他の休憩時間 */
		public AttDailyTime restHours;
	}

	/**
	 * 勤務時刻を保存する
	 * @param empId 社員ID
	 * @param param 勤務時間情報
	 */
	public void saveDailyAttTimeParam(Id empId, AttAttendanceService.SaveDailyAttTimeParam param) {

		// 勤怠計算に必要な情報が設定されているか確認する
		EmployeeBaseEntity employee = getEmployee(empId);
		EmployeeHistoryEntity employeeHistory = getEmployeeHistory(employee, param.targetDate);
		validateEmployee(employee, employeeHistory) ;

		// 更新対象の勤怠明細を取得する
		AppDate dayBefore = param.targetDate.addDays(-1);
		AppDate dayAfter = param.targetDate.addDays(+1);
		AttSummaryRepository repo = new AttSummaryRepository();
		List<AttRecordEntity> records = repo.searchRecordEntityList(empId, dayBefore, dayAfter, false);
		AttRecordEntity recordTarget, recordBefore, recordAfter;
		for (AttRecordEntity record : records) {
			if (record.rcdDate == dayBefore) {
				recordBefore = record;
			} else if (record.rcdDate == dayAfter) {
				recordAfter = record;
			} else if (record.rcdDate == param.targetDate) {
				recordTarget = record;
			}
		}

		if (recordTarget == null) {
			// 保存対象のレコードが存在しない場合、新規に勤怠データを作成する
			createAttSummaryWithCalc(empId, param.targetDate);
			records = repo.searchRecordEntity(empId, param.targetDate);
			if (records.isEmpty()) {
			// メッセージ：該当する勤怠明細が見つかりませんでした
				throw new App.RecordNotFoundException(
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_Record}));
			}
			recordTarget = records[0];
		} else {
			if (recordTarget.isSummaryDirty) {
				new AttCalcService().calcAttendance(recordTarget.summaryId);
			}
			// 計算後の勤怠明細を再取得
			recordTarget = repo.getRecordEntity(recordTarget.id);
		}

		// 入力勤務時刻が更新可能であるかチェックする
		recordTarget.inpStartTime = param.startTime;
		recordTarget.inpEndTime = param.endTime;
		AttValidator.UpdateAttTimeValidator updateAttTimeValidator =
				new AttValidator.UpdateAttTimeValidator(recordTarget, recordBefore, recordAfter);
		Validator.Result updateAttTimeValidatorRes = updateAttTimeValidator.validate();
		if (! updateAttTimeValidatorRes.isSuccess()) {
			Validator.Error error = updateAttTimeValidatorRes.getErrors().get(0);
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 1~5の休暇を時刻順でソートする
		List<AttTimePeriod> restPeriodList = AttTimePeriod.sortByStartTime(
				param.rest1StartTime, param.rest1EndTime,
				param.rest2StartTime, param.rest2EndTime,
				param.rest3StartTime, param.rest3EndTime,
				param.rest4StartTime, param.rest4EndTime,
				param.rest5StartTime, param.rest5EndTime
			);

		// 勤怠明細レコードに時刻を設定する
		if (restPeriodList.size() > 0) {
			recordTarget.inpRest1StartTime = restPeriodList[0].startTime;
			recordTarget.inpRest1EndTime = restPeriodList[0].endTime;
		}
		if (restPeriodList.size() > 1) {
			recordTarget.inpRest2StartTime = restPeriodList[1].startTime;
			recordTarget.inpRest2EndTime = restPeriodList[1].endTime;
		}
		if (restPeriodList.size() > 2) {
			recordTarget.inpRest3StartTime = restPeriodList[2].startTime;
			recordTarget.inpRest3EndTime = restPeriodList[2].endTime;
		}
		if (restPeriodList.size() > 3) {
			recordTarget.inpRest4StartTime = restPeriodList[3].startTime;
			recordTarget.inpRest4EndTime = restPeriodList[3].endTime;
		}
		if (restPeriodList.size() > 4) {
			recordTarget.inpRest5StartTime = restPeriodList[4].startTime;
			recordTarget.inpRest5EndTime = restPeriodList[4].endTime;
		}
		// 勤怠明細の入力更新時刻項目(inpLastUpdateTime)を設定する
		recordTarget.inpLastUpdateTime = AppDateTime.now();
		// その他の休憩時間を設定する
		recordTarget.inpRestHours = param.restHours;

		// 保存可能かどうか検証する
		AttValidator.AttTimeValidator attTimeValidator = new AttValidator.AttTimeValidator(recordTarget, recordBefore, recordAfter);
		Validator.Result attTimeValidatorRes = attTimeValidator.validate();
		if (! attTimeValidatorRes.isSuccess()) {
			// FIXME: エラーが複数検出された場合の処理
			Validator.Error error = attTimeValidatorRes.getErrors().get(0);
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 保存する
		repo.saveRecordEntity(recordTarget);

		// 勤怠計算を実行する
		new AttCalcService().calcAttendance(recordTarget.summaryId);

	}

	/**
	 * 日付と社員IDを指定して該当する勤怠データ(勤怠サマリー＋勤怠明細）を取得する
	 * 存在しない場合は、新規に勤怠データを作成します。
	 * @param employeeId 社員ID
	 * @param targetDate 対象開始日
	 * @param targetDate 対象終了日
	 * @return 勤怠データ（明細リストを含むサマリー)
	 */
	public List<AttSummaryEntity> getSummaryList(final Id employeeId, final AppDate startDate, final AppDate endDate) {

		List<AttSummaryEntity> summaries = new List<AttSummaryEntity>();
		for (Date targetDate = startDate.getDate(); targetDate <= endDate.getDate();
				targetDate = targetDate.addDays(1)) {

			// 対象日を含むサマリーリストがあるか確認する
			Boolean exists = false;
			for (AttSummaryEntity summary : summaries) {
				if ((targetDate >= summary.startDate.getDate()) && (targetDate <= summary.endDate.getDate())) {
					exists = true;
					break;
				}
			}

			// サマリーが存在していないのであれば取得する
			if (! exists) {
				summaries.add(getSummary(employeeId, AppDate.valueOf(targetDate)));
			}
		}

		return summaries;
	}

	/**
	 * 日付と社員IDを指定して該当する勤怠データ(勤怠サマリー＋勤怠明細）を取得する
	 * 存在しない場合は、新規に勤怠データを作成します。
	 * @param employeeId 社員ID
	 * @param targetDate 対象日付
	 * @return 勤怠データ（明細リストを含むサマリー)
	 */
	public AttSummaryEntity getSummary(final Id employeeId, final AppDate targetDate) {

		// 社員IDで指定日を含むAttSummaryEntityを取得する
		AttSummaryRepository repo = new AttSummaryRepository();
		List<AttSummaryEntity> summaryList = repo.searchEntity(employeeId, targetDate);

		// 新規の場合
		if ((summaryList == null) || (summaryList.isEmpty())) {
			// AttSummaryEntityが存在しない場合は新規に作成する
			return createAttSummaryWithCalc(employeeId, targetDate);
		}

		// 既存の場合
		// TODO 該当するAttSummaryEntityが複数レコードが存在している場合の対応（とりあえず例外を投げておく)
		if (summaryList.size() > 1) {
			List<Id> summaryIdList = new List<Id>();
			for (AttSummaryEntity summary : summaryList) {
				summaryIdList.add(summary.id);
			}
			throw new App.IllegalStateException('該当する勤怠サマリーが複数存在します。AttSummaryId=' + String.join(summaryIdList, ','));
		}

		return adjustSummary(summaryList[0]);
	}

	/**
	 * 勤怠サマリーIDを指定して勤怠サマリーを取得する
	 * 再計算フラグが ON の場合は勤怠計算を実施する
	 * @param summaryId 取得対象の勤怠サマリーID
	 * @return 勤怠サマリー、ただしサマリーが見つからなかった場合はnull
	 */
	public AttSummaryEntity getSummaryById(Id summaryId) {
		AttSummaryRepository repo = new AttSummaryRepository();
		AttSummaryEntity summary = repo.getEntity(summaryId);

		if (summary == null) {
			return null;
		}

		return adjustSummary(summary);
	}

	/**
	 * 勤怠サマリーを調整する
	 * 社員の有効期間に合わせて勤怠データを調整する。また、必要に応じて勤怠計算を実行する。
	 * @param 調整対象の勤怠サマリー
	 * @return 調整後の勤怠サマリー
	 */
	private AttSummaryEntity adjustSummary(AttSummaryEntity summary) {
		// 勤怠明細データを社員の有効期間に合わせる
		AttSummaryEntity adjustedSummary = adjustAttRecrodList(new List<AttSummaryEntity>{summary}).get(0);

		// 勤怠計算を実行する
		if (adjustedSummary.isDirty || new AttCalcService().needRecalcSummary(adjustedSummary)) {
			new AttCalcService().calcAttendance(adjustedSummary.id);
			return new AttSummaryRepository().getEntity(adjustedSummary.id);
		}

		return adjustedSummary;
	}


	/**
	 * 勤怠明細レコードを社員の有効期間に合わせて調整する
	 * 勤怠レコードが社員の有効期間中のみ存在するように調整します。
	 * 明細レコードが追加、削除されても勤怠計算は実行されません。
	 * 変更が発生しない場合はパラメータの summaryList をそのまま返却します。(処理時間を考慮して)
	 * @param summaryList 調整対象の勤怠サマリー＋勤怠明細リスト。(同一社員のサマリーのみ)
	 * @return 調整後の勤怠データ
	 */
	@testVisible
	private List<AttSummaryEntity> adjustAttRecrodList(List<AttSummaryEntity> summaryList) {
		AttSummaryRepository repo = new AttSummaryRepository();

		List<AttRecordEntity> deleteRecordList = new List<AttRecordEntity>();
		List<AttRecordEntity> addRecordList = new List<AttRecordEntity>();
		List<AttSummaryEntity> modifySummaryList = new List<AttSummaryEntity>();
		List<Id> summaryIdList = new List<Id>();

		for (AttSummaryEntity summary : summaryList) {
			summaryIdList.add(summary.id);

			// ロックされている場合は何もしない
			if (summary.isLocked) {
				continue;
			}

			// 対象月度の期間を計算する
			AttWorkingTypeBaseEntity workingType = getWorkingType(summary.workingTypeBaseId);
			SummaryRange range = calcSummaryRange(workingType.payrollPeriod, workingType.startDateOfMonth, summary.startDate);

			// 社員の有効期間を考慮した開始・終了日を計算する
			EmployeeBaseEntity employee = getEmployee(summary.employeeBaseId);
			// 開始日
			Date summaryStartDate;
			Date empStartDate = employee.getOldestSuperHistory().validFrom.getDate();
			if (empStartDate > range.startDate.getDate()) {
				summaryStartDate = empStartDate;
			} else {
				summaryStartDate = range.startDate.getDate();
			}
			// 終了日
			Date summaryEndDate;
			Date empEndDate = employee.getLatestSuperHistory().validTo.getDate().addDays(-1);
			if (empEndDate < range.endDate.getDate()) {
				summaryEndDate = empEndDate;
			} else {
				summaryEndDate = range.endDate.getDate();
			}

			// サマリー期間が0日以下の場合は社員が無効な月度
			if (summaryStartDate > summaryEndDate) {
				// メッセージ：該当の社員は有効期間外です
				throw new App.IllegalStateException(
					ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Employee}));
			}

			// 現在の勤怠サマリーの開始終了日が上記の開始終了日に一致していれば変更なし
			// ここで終了。次のサマリーへ
			if (summaryStartDate == summary.startDate.getDate() && summaryEndDate == summary.endDate.getDate()) {
				continue;
			}

			// 不要な明細の削除
			for (AttRecordEntity record : summary.attRecordList) {
				Date recordDate = record.rcdDate.getDate();
				if (recordDate < summaryStartDate) {
					deleteRecordList.add(record);
				} else if (recordDate > summaryEndDate) {
					deleteRecordList.add(record);
				}
			}

			// 不足している明細の追加
			if (summaryStartDate < summary.startDate.getDate()) {
				for (Date recordDate = summaryStartDate; recordDate < summary.startDate.getDate(); recordDate += 1) {
					addRecordList.add(createNewRecordEntity(employee, summary, AppDate.valueOf(recordDate)));
				}
			}
			if (summaryEndDate > summary.endDate.getDate()) {
				for (Date recordDate = summary.endDate.getDate().addDays(1); recordDate <= summaryEndDate; recordDate += 1) {
					addRecordList.add(createNewRecordEntity(employee, summary, AppDate.valueOf(recordDate)));
				}
			}

			// サマリー期間変更
			AttSummaryEntity modifySummary = new AttSummaryEntity();
			modifySummary.setId(summary.Id);
			modifySummary.startDate = AppDate.valueOf(summaryStartDate);
			modifySummary.endDate = AppDate.valueOf(summaryEndDate);
			modifySummary.isDirty = true;
			modifySummaryList.add(modifySummary);
		}

		// 勤怠明細追加
		if (! addRecordList.isEmpty()) {
			repo.saveRecordEntityList(addRecordList);
		}

		// 勤怠明細削除
		if (! deleteRecordList.isEmpty()) {
			// 有効な各種勤怠申請が存在している場合は削除不可。エラーとする
			List<AttDailyRequestEntity> dailyRequest = getDailyRequests(
					summaryList[0].employeeBaseId, deleteRecordList[0].rcdDate, deleteRecordList[deleteRecordList.size() - 1].rcdDate);

			for (AttDailyRequestEntity request : dailyRequest) {
				// 無効な申請はチェック不要
				if (request.status == AppRequestStatus.DISABLED
						&& request.isConfirmationRequired == false) {
					continue;
				}

				// 削除対象の明細が存在していることを確認する
				for (AttRecordEntity record : deleteRecordList) {
					Date recordDate = record.rcdDate.getDate();
					if (recordDate >= request.startDate.getDate() && recordDate <= request.endDate.getDate()) {
						// メッセージ：社員の有効期間外に勤怠申請が存在します。社員の有効期間を変更してください。
						String msg = ComMessage.msg().Att_Err_ExistRequestInvalidEmployee
								+ '(' + recordDate.format() + ')';
						throw new App.IllegalStateException(msg);
					}
				}
			}

			// 明細がロックされている場合も削除不可
			for (AttRecordEntity record : deleteRecordList) {
				if (record.isLocked == true) {
					// メッセージ：社員の有効期間外に勤務確定している日が存在します。社員の有効期間を変更してください。
					String msg = ComMessage.msg().Att_Err_ExistFixRecordInvalidEmployee
							+ '(' + record.rcdDate.format() + ')';
					throw new App.IllegalStateException(msg);
				}
			}

			repo.deleteRecords(deleteRecordList);
		}

		// 勤怠サマリー更新
		List<AttSummaryEntity> resultSummaryList = new List<AttSummaryEntity>();
		if (! modifySummaryList.isEmpty()) {
			repo.saveEntityList(modifySummaryList);
			resultSummaryList = repo.getEntityList(summaryIdList, true, false);
		} else {
			// 処理時間削減のため、変更が発生していない場合は引数で受け取ったサマリーをそのまま返却
			resultSummaryList = summaryList;
		}

		return resultSummaryList;
	}

	/**
	 * 指定した期間の日次申請を取得する
	 * @param employeeId 社員ベースID
	 * @param startDate 取得対象開始日
	 * @param endDate 取得対象終了日
	 */
	private List<AttDailyRequestEntity> getDailyRequests(Id employeeId, AppDate startDate, AppDate endDate) {

		AttDailyRequestRepository.RequestFilter filter =
				new AttDailyRequestRepository.RequestFilter();
		filter.employeeId = employeeId;
		filter.startDate = startDate;
		filter.endDate = endDate;
		filter.isDuration = true;

		final Boolean forUpdate = false;
		return new AttDailyRequestRepository().searchEntityList(filter, forUpdate);
	}

	/**
	 * 勤怠データを作成する（勤怠計算済み、休憩時間(入力)にデフォルト値設定
	 * @param employeeId 社員ID
	 * @param targetDate 対象日付
	 * @return 勤怠データ(サマリー + 明細一覧)
	 */
	private AttSummaryEntity createAttSummaryWithCalc(Id employeeId, AppDate targetDate) {

		AttSummaryRepository repo = new AttSummaryRepository();

		// 勤怠データ作成
		AttSummaryEntity newSummary = createSummary(employeeId, targetDate);
		Id summaryId = newSummary.id;

		// 勤怠データを社員の有効期間に合わせる
		newSummary = adjustAttRecrodList(new List<AttSummaryEntity>{newSummary}).get(0);

		// 勤怠計算を実行する
		new AttCalcService().calcAttendance(summaryId);

		// 計算後の勤怠データを取得
		AttSummaryEntity calcSummaryEntity = repo.getEntity(summaryId);

		// 休暇サマリを強制作成する
		new AttManagedLeaveService().initLeaveSummary(calcSummaryEntity);

		return calcSummaryEntity;
	}

	/**
	 * 社員と日付を指定して該当する勤怠明細を取得する(勤怠計算が必要な場合は計算する)
	 * @param employeeId 社員ID
	 * @param targetDate 対象日付
	 * @return 勤怠明細
	 */
	public AttRecordEntity getRecordWithCalc(final Id empId, final AppDate targetDate) {
		AttSummaryEntity summary = getSummary(empId, targetDate);
		return summary.getAttRecord(targetDate);
	}

	/**
	 * 社員と日付を指定して該当する勤怠明細を取得する
	 * 存在しない場合は,nullを返す
	 * @param employeeId 社員ID
	 * @param targetDate 対象日付
	 * @return 勤怠明細
	 */
	public AttRecordEntity getRecord(final Id empId, final AppDate targetDate) {
		// 社員IDで指定日を含むAttSummaryEntityを取得する
		AttSummaryRepository repo = new AttSummaryRepository();

		List<AttRecordEntity> recordDataList = repo.searchRecordEntity(empId, targetDate);

		if ((recordDataList == null) || (recordDataList.isEmpty())) {
			return null;
		}
		return recordDataList[0];
	}

	/**
	 * 指定された日付を含む勤怠データを作成する
	 * @param employeeId 社員ID
	 * @param targetDate 対象日
	 * @return 勤怠データ（明細リストを含むサマリー)
	 */
	@TestVisible
	private AttSummaryEntity createSummary(final Id employeeId, final AppDate targetDate) {
		// 社員(履歴含む)を取得する
		EmployeeBaseEntity employee = getEmployee(employeeId);
		EmployeeHistoryEntity employeeHistory = getEmployeeHistory(employee, targetDate);
		// 勤怠に必要な情報が設定されていることを確認
		validateEmployee(employee, employeeHistory);

		// 対象日に該当する勤務体系情報を取得する
		AttWorkingTypeBaseEntity workingType = getWorkingType(employeeHistory.workingTypeId, targetDate);
		validateWorkingType(workingType);

		// 勤怠サマリーエンティティを作成する
		AttSummaryRepository repo = new AttSummaryRepository();
		AttSummaryEntity summary = createNewSummaryEntity(workingType, employee, targetDate);
		AttSummaryRepository.SavaSummaryResult res = repo.saveEntity(summary);
		summary.setId(res.details[0].id);

		// 勤怠明細エンティティを作成する
		List<AttRecordEntity> recordList = new List<AttRecordEntity>();
		AppDate rcdDate = summary.startDate;
		while (rcdDate.getDate() <= summary.endDate.getDate()) {
			recordList.add(createNewRecordEntity(employee, summary, rcdDate));
			rcdDate = rcdDate.addDays(1);
		}
		repo.saveRecordEntityList(recordList);

		// 各エンティティの参照項目取得するためリポジトリから取得したエンティティを返却する
		return repo.getEntity(summary.id);
	}

	/**
	 * 新規の勤怠サマリーエンティティを作成する
	 * @param workTypeEntity 勤務体系エンティティ
	 * @param employeeEntity 社員エンティティ
	 * @param rcdDate 作成対象日
	 * @return 勤怠サマリーエンティティ
	 */
	@TestVisible
	private AttSummaryEntity createNewSummaryEntity(
			AttWorkingTypeBaseEntity workTypeEntity, EmployeeBaseEntity employeeEntity, AppDate targetDate) {

		// AttWorkingTypeの清算期間と起算日情報から、月度の範囲の日付を確定する
		SummaryRange range = calcSummaryRange(
				workTypeEntity.payrollPeriod, workTypeEntity.startDateOfMonth, targetDate);

		// 勤怠サマリーを作成する
		AttSummaryEntity entity = new AttSummaryEntity();
		// TODO 社員名は会社のデフォルト言語？
		entity.name = AttSummaryEntity.createName(
				employeeEntity.code, employeeEntity.fullNameL.getFullName(), range.startDate, range.endDate);
		entity.ownerId = employeeEntity.userId;
		entity.employeeId = employeeEntity.getHistory(0).id;
		entity.workingTypeId = workTypeEntity.getHistory(0).id;
		entity.startDate = range.startDate;
		entity.endDate = range.endDate;
		entity.summaryName = AttSummaryEntity.createSummaryName(
				workTypeEntity.monthMark, range.startDate, range.endDate);
		// FIXME: SubNo算出（ひとまず1固定）
		entity.subNo = 1;

		// UniqKey 社員ベースID + 月度YYYYMM + SubNo
		entity.uniqKey = employeeEntity.id + entity.summaryName + entity.subNo;

		return entity;
	}

	/**
	 * 新規の勤怠明細エンティティを作成する
	 * @param employeeEntity 社員エンティティ
	 * @param summaryEntity 勤怠サマリーエンティティ
	 * @param rcdDate 作成対象日
	 * @return 勤怠明細エンティティ
	 */
	@TestVisible
	private AttRecordEntity createNewRecordEntity(
			EmployeeBaseEntity employeeEntity, AttSummaryEntity summaryEntity, AppDate rcdDate) {

		AttRecordEntity entity = new AttRecordEntity();
		// TODO 社員名は会社のデフォルト言語？
		entity.name = AttRecordEntity.createRecordName(
				employeeEntity.code, employeeEntity.fullNameL.getFullName(), rcdDate);
		entity.rcdDate = rcdDate;
		entity.summaryId = summaryEntity.Id;

		// UniqKey 勤怠サマリーID + YYYYMMDD
		entity.uniqKey = summaryEntity.id + rcdDate.formatYYYYMMDD();

		return entity;
	}

	/** サマリーの範囲情報 */
	@TestVisible
	private class SummaryRange {
		/** 開始日 */
		public AppDate startDate;
		/** 終了日 */
		public AppDate endDate;

		public SummaryRange(AppDate startDate, AppDate endDate) {
			this.startDate = startDate;
			this.endDate = endDate;
		}
	}

	/**
	 * 清算期間と起算日情報から対象日を含むサマリーの範囲を算出する
	 * @param payrollPeriod 清算期間
	 * @param startDateOfMonth 起算日
	 * @param targetDate 対象日
	 * @return サマリー範囲情報
	 */
	@TestVisible
	private SummaryRange calcSummaryRange(
			AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType payrollPeriod,
			Integer startDateOfMonth,
			AppDate targetDate) {


		if (payrollPeriod == null) {
			// メッセージ：勤務体系に清算期間が設定されていません
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_FieldNotSet(new List<String>{
						ComMessage.msg().Att_Lbl_WorkingType,
						ComMessage.msg().Att_Lbl_WorkingTypePayrollPeriod}));
		}

		//---------------------------------
		//--- 現在、清算期間は月次のみ対応しています。以下は月次の場合の処理---
		//---------------------------------
		if (payrollPeriod != AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month) {
			// メッセージ：勤務体系に未対応の清算期間が設定されています。（清算期間=[%1])
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_WorkingTypePayrollPeriodNotSupported(new List<String>{payrollPeriod.name()}));
		}
		if (startDateOfMonth == null) {
			// メッセージ：勤務体系に月度の開始日が設定されておりません。
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_FieldNotSet(new List<String>{
						ComMessage.msg().Att_Lbl_WorkingType,
						ComMessage.msg().Att_Lbl_WorkingTypeBeginDayOfMonth}));
		}

		// 開始日
		AppDate startDate;
		if (targetDate.day() >= startDateOfMonth) {
			// 対象月が開始月となる
			startDate = AppDate.newInstance(
					targetDate.year(), targetDate.month(), startDateOfMonth);
		} else {
			// 前月が開始月となる
			startDate = AppDate.newInstance(
					targetDate.year(), targetDate.month(), startDateOfMonth).addMonths(-1);
		}

		// 終了日
		// endDate = startDateの1カ月後 - 1日
		AppDate endDate = startDate.addMonths(1).addDays(-1);

		return new SummaryRange(startDate, endDate);
	}

	/**
	 * 社員情報を取得する(キャッシュ機能あり)
	 * @param employeeId 社員ベースID
	 * @return 全履歴を持つ社員
	 * @throws App.RecordNotFoundException 社員が見つからない場合
	 */
	public EmployeeBaseEntity getEmployee(final Id employeeId) {
		return empService.getEmployeeFromCache(employeeId);
	}

	/**
	 * 指定された日付時点の社員情報を取得する
	 * @param employeeId 社員ベースID
	 * @param targetDate 対象日
	 * @return 指定した対象日の履歴を持つ社員、ただし対象日を指定しなかった場合は全履歴を持つ社員が返却される
	 * @throws App.IllegalStateException 社員が見つからない場合、社員が有効期間外の場合
	 */
	@TestVisible
	public EmployeeBaseEntity getEmployee(final Id employeeId, final AppDate targetDate) {
		return empService.getEmployee(employeeId, targetDate);
	}

	/**
	 * 指定された日付の社員履歴を取得する
	 * @param employeeBase 社員ベース(全ての履歴を持っていること)
	 * @param targetDate 取得対象日
	 * @return 対象日に有効な履歴
	 * @throws App.IllegalStateException 指定された日が有効期間外の場合
	 */
	public EmployeeHistoryEntity getEmployeeHistory(EmployeeBaseEntity employeeBase, AppDate targetDate) {
		EmployeeHistoryEntity employeeHistory = employeeBase.getHistoryByDate(targetDate);
		if (employeeHistory == null) {
			throw new App.IllegalStateException(
					ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Employee}));
		}
		return employeeHistory;
	}

	/**
	 * 社員エンティティに勤怠に必要な情報が正しく設定されているかを確認する。
	 * 履歴は履歴リストの先頭の履歴のみチェックする
	 * 設定されていない場合は例外が発生する
	 */
	public void validateEmployee(EmployeeBaseEntity employee) {
		// ベースエンティティで検証が必要な項目はないため、履歴のみチェック
		if (employee.getHistoryList() == null || employee.getHistoryList().isEmpty()) {
			validateEmployeeHistory(employee.getHistory(0));
		}
	}

	/**
	 * 社員エンティティに勤怠に必要な情報が正しく設定されているかを確認する。
	 * 設定されていない場合は例外が発生する
	 */
	public void validateEmployee(EmployeeBaseEntity employee, EmployeeHistoryEntity employeeHistory) {
		if (employeeHistory != null) {
			validateEmployeeHistory(employee.getHistory(0));
		}
	}

	/**
	 * 社員履歴に勤怠に必要な情報が正しく設定されているかを確認する。
	 * 設定されていない場合は例外が発生する
	 * @throws App.IllegalStateException 不正な値が設定されている場合
	 */
	public void validateEmployeeHistory(EmployeeHistoryEntity empHistory) {
		// 社員に勤務体系が設定されていない場合はエラーとする
		if (empHistory.workingTypeId == null) {
			// メッセージ：社員の勤務体系が設定されておりません
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_FieldNotSet(new List<String>{
						ComMessage.msg().Com_Lbl_Employee,
						ComMessage.msg().Att_Lbl_WorkingType}));
		}
	}

	/**
	 * 勤務体系を取得する
	 * @param workingTypeBaseId 勤務体系ベースID
	 * @return 全履歴を持つ勤務体系
	 * @throws App.RecordNotFoundException 勤務体系が見つからない場合
	 */
	public AttWorkingTypeBaseEntity getWorkingType(final Id workingTypeBaseId) {
		return workingTypeService.getWorkingType(workingTypeBaseId);
	}

	/**
	 * 勤務体系を取得する
	 * @param workingTypeBaseId 勤務体系ベースID
	 * @param targetDate 対象日
	 * @return 対象日の履歴を持つ勤務体系
	 * @throws App.RecordNotFoundException 勤務体系が見つからない場合
	 */
	public AttWorkingTypeBaseEntity getWorkingType(final Id workingTypeBaseId, AppDate targetDate) {
		return workingTypeService.getWorkingType(workingTypeBaseId, targetDate);
	}

	/**
	 * 勤務体系のバリデーションを実行する
	 * 履歴のチェックは先頭の1件のみ
	 */
	public void validateWorkingType(AttWorkingTypeBaseEntity workingTypeData) {
		AttWorkingTypeHistoryEntity workingTypeHistory;
		if ((workingTypeData.getHistoryList() != null) && (! workingTypeData.getHistoryList().isEmpty())) {
			workingTypeHistory = workingTypeData.getHistory(0);
		}

		validateWorkingType(workingTypeData, workingTypeHistory);
	}

	/**
	 * 勤務体系のバリデーションを実行する
	 * @param wokingTypeBase 勤務体系ベース, nullの場合はバリデーションは実行されない
	 * @param workingTypeHistory 勤務体系履歴、nullの場合はバリデーションは実行されない
	 * @throws App.IllegalStateException 不正な値が設定されている場合
	 */
	public void validateWorkingType(AttWorkingTypeBaseEntity wokingTypeBase, AttWorkingTypeHistoryEntity workingTypeHistory) {

		// 勤務体系ベース
		if (wokingTypeBase != null) {
			AttConfigValidator.AttWorkingTypeBaseValidator validator =
					new AttConfigValidator.AttWorkingTypeBaseValidator(wokingTypeBase);
			Validator.Result baseValidatorRes = validator.validate();
			if (! baseValidatorRes.isSuccess()) {
				Validator.Error error = baseValidatorRes.getErrors().get(0);
				throw new App.IllegalStateException(error.code, error.message);
			}
		}

		// 勤務体系履歴
		if (workingTypeHistory != null) {
			AttConfigValidator.AttWorkingTypeHistoryValidator historyValidator =
					new AttConfigValidator.AttWorkingTypeHistoryValidator(wokingTypeBase, workingTypeHistory);
			Validator.Result historyValidatorRes = historyValidator.validate();
			if (! historyValidatorRes.isSuccess()) {
				Validator.Error error = historyValidatorRes.getErrors().get(0);
				throw new App.IllegalStateException(error.code, error.message);
			}
		}
	}

		/**
	 * @description 勤怠利用する所属会社の所属社員か判定します
	 * @param employee 判定する社員のベースエンティティ
	 * @throws App.IllegalStateException 社員の所属会社が勤怠機能を利用しない設定の場合
	 * @return 社員の所属会社が勤怠機能を利用する場合はtrueを返す
	 */
	 public Boolean canUseAttendance(EmployeeBaseEntity employee){
		// 勤怠機能の権限がない場合エラーを吐く
		if(!employee.companyInfo.useAttendance){
			throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_USE_ATTENDANCE,
					ComMessage.msg().Att_Err_CannotUseAttendance);
		}
		return true;
	}

	/** 勤務可能判定の戻り値 */
	 public class CheckWorkingDaysParam {
		/** 勤務可能判定情報 */
		public WorkingDayCheckResult workingDayCheckResult;
		/** 判定日ごとの実行ステータス */
		public Boolean isRecordSuccess;
		/** エラー情報 */
		public RecordError recordError;
	}

	/**
	 * @description 勤務日判定情報
	 */
	public class WorkingDayCheckResult {
		/** 判定区分 */
		public WorkingDayCheckType workingCheckType;
		/** 勤務可能フラグ */
		public Boolean isWorkDay;
		public WorkingDayCheckResult(Boolean isWorkDay, WorkingDayCheckType workingCheckType){
			this.isWorkDay = isWorkDay == null ? false : isWorkDay;
			this.workingCheckType = workingCheckType;
		}
	}

	/**
	 * @description 判定日ごとのエラー情報
	 */
	public class RecordError {
		/** エラー区分 */
		public String errorCode;
		/** エラーメッセージ */
		public String message;
		public RecordError(String errorCode, String message){
			this.errorCode = errorCode;
			this.message = message;
		}
	}

	/** 勤務判定区分 */
	public Enum WorkingDayCheckType {
		Leave,
		Holiday,
		LegalHoliday,
		LeaveOfAbsence,
		Absence
	}
	public static final Map<WorkingDayCheckType, String> WORKING_DAY_CHECK_TYPE_MAP;
	static {
		WORKING_DAY_CHECK_TYPE_MAP = new Map<WorkingDayCheckType, String>{
			WorkingDayCheckType.Leave => 'Leave',
			WorkingDayCheckType.Holiday => 'Holiday',
			WorkingDayCheckType.LegalHoliday => 'LegalHoliday',
			WorkingDayCheckType.LeaveOfAbsence => 'LeaveOfAbsence',
			WorkingDayCheckType.Absence => 'Absence'
		};
	}

	/**
	 * 勤務可能な日かどうかを判定します。
	 * 勤務可能な日とは以下の場合です。
	 * ・ 通常の勤務日
	 * ・ 振替休日の休日出勤日
	 * @param employee 判定対象の社員
	 * @param targetDates 判定対象日のリスト
	 * @return Map<AppDate, CheckWorkingDaysParam> 判定対象日をキーにした勤務可能な日かどうかの判定結果
	 */
	public Map<AppDate, CheckWorkingDaysParam> checkWorkingDays(EmployeeBaseEntity employee, List<AppDate> targetDates) {
		// 1. 勤怠計算
		// - 判定日の最大期間を取得
		AppDate targetRangeStartDate = null;
		AppDate targetRangeEndDate = null;
		for (AppDate targetDate : targetDates) {
			if (targetRangeStartDate == null || targetDate.isBefore(targetRangeStartDate)) {
				targetRangeStartDate = targetDate;
			}
			if (targetRangeEndDate == null || targetDate.isAfter(targetRangeEndDate)) {
				targetRangeEndDate = targetDate;
			}
		}
		// - 対象の勤怠サマリを取得
		List<AttSummaryEntity> summaryList = new AttSummaryRepository().searchEntity(employee.id, targetRangeEndDate, targetRangeStartDate);
		// - 再計算対象の判定
		Set<Id> reCalcTargetSummarySet = new Set<Id>();
		for (AttSummaryEntity summary : summaryList) {
			if (summary.isDirty) {
				for (AppDate targetDate : targetDates) {
					if ((summary.startDate.compareTo(targetDate) != App.COMPARE_AFTER)
							&& (summary.endDate.compareTo(targetDate) != App.COMPARE_BEFORE)) {
						reCalcTargetSummarySet.add(summary.id);
					}
				}
			}
		}
		// - 再計算
		if (reCalcTargetSummarySet.size() <= 2) {// ３件を越えるとガバナエラーで勤怠計算の再計算できない
			for (Id id : reCalcTargetSummarySet) {
				new AttCalcService().calcAttendance(id);
			}
		}

		// 2. 勤怠明細取得
		List<AttRecordEntity> recordList = new AttSummaryRepository().searchRecordEntityList(employee.id, targetDates, false);
		Map<AppDate, AttRecordEntity> recordMap = new Map<AppDate, AttRecordEntity>();
		for (AttRecordEntity record : recordList) {
			recordMap.put(record.rcdDate, record);
		}

		// 3. 勤務可能判定
		 Map<AppDate, CheckWorkingDaysParam> results = new Map<AppDate, CheckWorkingDaysParam>();
		 for (AppDate targetDate : targetDates) {
			 CheckWorkingDaysParam checkWorkingDaysParam = new CheckWorkingDaysParam();
			// - 判定日の勤怠明細取得
			AttRecordEntity record = recordMap.get(targetDate);
			// - 勤怠明細がなければレコードエラー 勤務可能判定しない
			if (record == null) {
				checkWorkingDaysParam.recordError = new RecordError(
						'ATT_RECORD_NOT_FOUND',
						ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Att_Lbl_Record}));
				checkWorkingDaysParam.isRecordSuccess = false;
				results.put(targetDate, checkWorkingDaysParam);
				continue;
			}
			// - 再計算していない場合はレコードワーニング 勤務可能判定する
			if (record.isSummaryDirty) {
				checkWorkingDaysParam.recordError = new RecordError(
						'ILLEGAL_ATT_CALCULATION',
						ComMessage.msg().Att_Err_SummaryDirty);
			}
			// - 勤怠明細から勤務可能日判定
			checkWorkingDaysParam.workingDayCheckResult = checkWorkingDay(record);
			checkWorkingDaysParam.isRecordSuccess = true;
			results.put(targetDate, checkWorkingDaysParam);
		 }

		 return results;
	}
	/**
	 * 勤怠明細を元に、勤務可能な日かどうかの判定を行います。
	 * 勤務の実績（出退勤打刻など）で判定はしません。
	 * @param record　勤怠明細
	 * @return WorkingDayCheckResult 判定結果
	 */
	@TestVisible
	private WorkingDayCheckResult checkWorkingDay(AttRecordEntity record) {
		WorkingDayCheckResult workingDayCheckResult = new WorkingDayCheckResult(false, null);
		// 休職・休業判定
		if (record.leaveOfAbsenceId != null) {
			workingDayCheckResult.workingCheckType = WorkingDayCheckType.LeaveOfAbsence;
		}
		// 勤務日判定
		if (record.outDayType == AttDayType.WORKDAY) {
			// - 欠勤判定
			if (record.reqAbsenceRequestId != null) {
				workingDayCheckResult.workingCheckType = WorkingDayCheckType.Absence;
			}
			// - 休暇判定
			if (record.outLeaveRange != null && record.outLeaveRange == AttLeaveRange.RANGE_DAY) {
				workingDayCheckResult.workingCheckType = WorkingDayCheckType.Leave;
			}
			// 通常の勤務日は勤務可能
			if (workingDayCheckResult.workingCheckType == null) {
				workingDayCheckResult.isWorkDay = true;
			}
		} else {// 休日判定
			// - 休日出勤判定
			if (record.reqHolidayWorkRequestId == null) {
				if (record.outDayType == AttDayType.HOLIDAY) {
					workingDayCheckResult.workingCheckType = WorkingDayCheckType.Holiday;
				} else if (record.outDayType == AttDayType.LEGAL_HOLIDAY){
					workingDayCheckResult.workingCheckType = WorkingDayCheckType.LegalHoliday;
				} else if (record.outDayType == AttDayType.PREFERRED_LEGAL_HOLIDAY) {
					workingDayCheckResult.workingCheckType = WorkingDayCheckType.LegalHoliday;
				}
			}
			// 休日出勤日は勤務可能
			if (workingDayCheckResult.workingCheckType == null) {
				workingDayCheckResult.isWorkDay = true;
			}
		}
		return workingDayCheckResult;
	}
}