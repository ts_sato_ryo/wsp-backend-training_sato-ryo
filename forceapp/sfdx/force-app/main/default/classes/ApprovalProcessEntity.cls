/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 承認
 *
 * プロセスインスタンスのエンティティ
 */
public with sharing class ApprovalProcessEntity extends Entity {

	/** 対象オブジェクトID */
	public String targetObjectId {get; set;}
	/** 対象オブジェクト名 */
	public String targetObjectName {get; set;}
	/** 最後に承認、却下、または取り消したアクター */
	public Id lastActorId {get; set;}

	/** プロセスインスタンス履歴のリスト */
	public List<ApprovalProcessHistoryEntity> historyList {get; private set;}

	/**
	 * プロセスインスタンス履歴リストをリプレースする（リストをコピーする）
	 * @param historyList リプレース対象のプロセスインスタンス履歴リスト
	 */
	public void replaceHistoryList(List<ApprovalProcessHistoryEntity> historyList) {
		this.historyList = new List<ApprovalProcessHistoryEntity>(historyList);
	}
}