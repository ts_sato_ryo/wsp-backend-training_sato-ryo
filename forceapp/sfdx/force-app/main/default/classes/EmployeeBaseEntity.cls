/**
 * @group 共通
 *
 * @description 社員ベースエンティティ
 */
public with sharing class EmployeeBaseEntity extends EmployeeBaseGeneratedEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 表示名の最大文字数 */
	public static final Integer DISPLAY_NAME_MAX_LENGTH = 20;
	/** FirstNameの最大文字数 */
	public static final Integer FIRST_NAME_MAX_LENGTH = 40;
	/** LastNameの最大文字数 */
	public static final Integer LAST_NAME_MAX_LENGTH = 80;
	/** MiddleNameの最大文字数 */
	public static final Integer MIDDLE_NAME_MAX_LENGTH = 40;

	/**
	 * @desctiprion ユーザ項目を表すクラス
	 */
	public class User {
		/** 顔写真URL */
		public String photoUrl;
		/** ユーザ名 */
		public String userName;
		/** ユーザの言語 */
		public AppLanguage languageLocalekey;
		/** 有効 */
		public Boolean isActive;
	}

	/**
	 * @desctiprion 会社の情報
	 */
	public class Company extends ParentChildBaseCompanyEntity.Company {
		public Boolean useAttendance;
		public Boolean useExpense;
		public Boolean useExpenseRequest;
		public Boolean usePlanner;
		public Boolean useWorkTime;
		public Id currencyId;
		/** コンストラクタ */
		public Company(String code) {
			super(code);
		}
	}

	/**
	 * デフォルトコンストラクタ
	 */
	public EmployeeBaseEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public EmployeeBaseEntity(ComEmpBase__c sobj) {
		super(sobj);
	}

	/** 履歴エンティティのリスト */
	protected List<EmployeeHistoryEntity> historyList;

	/** 会社(参照専用) */
	public LookupField company {
		get {
			if (company == null) {
				company = new LookupField(
					new AppMultiString(
							sobj.CompanyId__r.Name_L0__c,
							sobj.CompanyId__r.Name_L1__c,
							sobj.CompanyId__r.Name_L2__c));
			}
			return company;
		}
		private set;
	}
	/** 会社情報(参照専用) */
	public EmployeeBaseEntity.Company companyInfo {
		get {
			if (companyInfo == null) {
				companyInfo = new EmployeeBaseEntity.Company(sobj.CompanyId__r.Code__c);
				companyInfo.useAttendance = sobj.CompanyId__r.UseAttendance__c;
				companyInfo.useExpense = sobj.CompanyId__r.UseExpense__c;
				companyInfo.useExpenseRequest = sobj.CompanyId__r.UseExpenseRequest__c;
				companyInfo.usePlanner = sobj.CompanyId__r.UsePlanner__c;
				companyInfo.useWorkTime = sobj.CompanyId__r.UseWorkTime__c;
				companyInfo.currencyId = sobj.CompanyId__r.currencyId__c;
			}
			return companyInfo;
		}
		private set;
	}
	/** ユーザ */
	public EmployeeBaseEntity.User user {
		get {
			if (userId == null) {
				return null;
			}
			if (user == null) {
				user = new EmployeeBaseEntity.User();
				user.photoUrl = sobj.UserId__r.SmallPhotoUrl;
				user.userName = sobj.UserId__r.Username;
				user.languageLocalekey = AppLanguage.valueof(sobj.UserId__r.LanguageLocalekey);
				user.isActive = sobj.UserId__r.isActive;
			}
			return user;
		}
		private set;
	}
	/** 表示名(取得専用。組織の言語設定に応じて言語が切り替わる) */
	public AppMultiString displayNameL {
		get {
			return new AppMultiString(displayNameL0, displayNameL1, displayNameL2);
		}
	}
	/** 社員名(取得専用。組織の言語設定に応じて言語が切り替わる) */
	public AppPersonName fullNameL {
		get {
			return new AppPersonName(
					firstNameL0, lastNameL0,
					firstNameL1, lastNameL1,
					firstNameL2, lastNameL2);
		}
	}

	/**
	 * 履歴の基底クラスに変換する
	 */
	protected override List<ParentChildHistoryEntity> convertSuperHistoryList() {
		return (List<ParentChildHistoryEntity>)historyList;
	}

	/**
	 * 履歴エンティティリストを取得する
	 * @return 履歴エンティティリスト
	 */
	public List<EmployeeHistoryEntity> getHistoryList() {
		return historyList;
	}

	/**
	 * 履歴エンティティをリストに追加する
	 * @param history 追加する履歴エンティティ
	 */
	 public void addHistory(EmployeeHistoryEntity history) {
		if (historyList == null) {
			historyList = new List<EmployeeHistoryEntity>();
		}
		historyList.add(history);
	}

	 /**
	  * 部署履歴リストを取得する
	  * @return 部署名。履歴が存在しない場合はnull
	  */
	 public EmployeeHistoryEntity getHistory(Integer index) {
		 if (historyList == null) {
			 return null;
		 }
		 return historyList[index];
	 }

	 /**
	 * 社員履歴エンティティリストから指定したIDの履歴を取得する
	 * @return 履歴エンティティ、ただし存在しない場合はnull
	 */
	public EmployeeHistoryEntity getHistoryById(Id historyId) {
		return (EmployeeHistoryEntity)super.getSuperHistoryById(historyId);
	}

	/**
	 * 表示名(L0)のデフォルト値を取得する
	 * @param langL0 組織設定の主言語
	 * @return 主言語に応じた表示名
	 */
	public String getDefaultDisplayNameL0(AppLanguage langL0) {
		return buildDisplayName(this.firstNameL0, this.lastNameL0, langL0);
	}

	/**
	 * 表示名(L1)のデフォルト値を取得する
	 * @param langL1 組織設定の副言語
	 * @return 副言語に応じた表示名
	 */
	public String getDefaultDisplayNameL1(AppLanguage langL1) {
		return buildDisplayName(this.firstNameL1, this.lastNameL1, langL1);
	}

	/**
	 * 表示名(L2)のデフォルト値を取得する
	 * @param langL2 組織設定の副々言語
	 * @return 副々言語に応じた表示名
	 */
	public String getDefaultDisplayNameL2(AppLanguage langL2) {
		return buildDisplayName(this.firstNameL2, this.lastNameL2, langL2);
	}

	/**
	 * 表示名を作成する
	 * 言語によらず「姓＋（半角スペース）＋名」にする
	 * 文字数上限オーバーした場合は「姓」のみにする
	 * @param firstName 名
	 * @param lastName 姓
	 * @param lang 言語
	 * @return 言語に応じた表示名
	 *
	 */
	@TestVisible
	private String buildDisplayName(String firstName, String lastName, AppLanguage lang)  {

		// 姓、名の両方がnullの場合はnullを返却
		if (firstName == null && lastName == null) {
			return null;
		}

		if (lang == AppLanguage.EN_US) {
			// 言語設定が英語
			return buildEnglishDisplayName(firstName, lastName);
		}

		// 言語設定が日本語または言語設定なし
		return buildJapaneseDisplayName(firstName, lastName);
	}

	/**
	 * 日本語その他言語の表示名を作成する
	 * @param firstName 名
	 * @param lastName 姓
	 * @return 日本語の並び順の表示名
	 *  - 「姓＋（半角スペース）＋名」にする
	 *    例) 田中 太郎
	 *  - 文字数上限オーバーした場合は「姓」のみにする
	 *    例) 田中
	 */
	private String buildJapaneseDisplayName(String firstName, String lastName)  {
		String displayName = '';

		// 姓
		if (String.isNotBlank(lastName)) {
			displayName = lastName;
		}

		// 名
		if (String.isNotBlank(firstName)) {
			if (String.isNotBlank(displayName)) {
				displayName += ' '; // 半角スペース
			}
			displayName += firstName;
		}

		// 最大文字数を超えていたら名前の一部を返却
		if (displayName.length() > DISPLAY_NAME_MAX_LENGTH) {
			if (String.isNotBlank(lastName)) {
				// 姓が設定されてれば、姓のみ返却
				displayName = lastName.left(DISPLAY_NAME_MAX_LENGTH);
			} else if (String.isNotBlank(firstName)){
				// 姓が設定されておらず、名のみ設定されていれば名のみ返却
				displayName = firstName.left(DISPLAY_NAME_MAX_LENGTH);
			}
		}
		return displayName;
	}

	/**
	 * 言語が英語(アメリカ)の場合の表示名を作成する
	 * @param firstName 名
	 * @param lastName 姓
	 * @return 英語の並び順の表示名
	 *  - 「名＋（半角スペース）＋姓」にする
	 *    例) Taro Tanaka
	 *  - 文字数上限オーバーした場合は「名」のみにする
	 *    例) Taro
	 */
	private String buildEnglishDisplayName(String firstName, String lastName)  {
		String displayName = '';

		// デフォルト => 名 +（半角スペース）+ 姓
		if (String.isNotBlank(firstName)) {
			displayName = firstName;
		}
		if (String.isNotBlank(lastName)) {
			if (String.isNotBlank(displayName)) {
				displayName += ' '; // 半角スペース
			}
			displayName += lastName;
		}

		// 最大文字数を超えていたら名前の一部を返却
		if (displayName.length() > DISPLAY_NAME_MAX_LENGTH) {
			if (String.isNotBlank(firstName)) {
				// 最大桁以上の場合 かつ 名がある => 名のみ返却
				displayName = firstName.left(DISPLAY_NAME_MAX_LENGTH);
			} else if (String.isNotBlank(lastName)){
				// 最大桁以上の場合 かつ 名が無い => 姓
				// ※　姓必須のためこれ以降の考慮不要
				displayName = lastName.left(DISPLAY_NAME_MAX_LENGTH);
			}
		}
		return displayName;
	}

	/**
	 * 有効期間終了日に相当する項目のラベルを取得する
	 * @return 有効期間終了日に相当する項目のラベル
	 */
	public override String getValidEndDateLabel() {
		return ComMessage.msg().Admin_Lbl_ResignationDate;
	}

	/**
	 * 指定した日に有効な履歴を取得する
	 * 履歴リストに論理削除されている履歴が含まれている場合は利用しないでください
	 * @param targetDate 対象日
	 * @return 有効な履歴、ただし有効な履歴が存在しない場合はnull
	 */
	public EmployeeHistoryEntity getHistoryByDate(AppDate targetDate) {
		return (EmployeeHistoryEntity)getSuperHistoryByDate(targetDate);
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public EmployeeBaseEntity copy() {
		EmployeeBaseEntity copyEntity = new EmployeeBaseEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildBaseEntity copySuperEntity() {
		return copy();
	}
}