/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * カレンダータイプ
 */
public with sharing class CalendarType extends ValueObjectType {

	/** 勤怠 */
	public static final CalendarType ATTENDANCE = new CalendarType('Attendance');
	/** 経費 */
	public static final CalendarType EXPENSE = new CalendarType('Expense');

	/** カレンダータイプのリスト */
	public static final List<CalendarType> TYPE_LIST;
	static {
		TYPE_LIST = new List<CalendarType> {
			ATTENDANCE,
			EXPENSE
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private CalendarType(String value) {
		super(value);
	}

	/**
	 * 値からCalendarTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つCalendarType、ただし該当するCalendarTypeが存在しない場合はnull
	 */
	public static CalendarType valueOf(String value) {
		CalendarType retType = null;
		if (String.isNotBlank(value)) {
			for (CalendarType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// CalendarType以外の場合はfalse
		Boolean eq;
		if (compare instanceof CalendarType) {
			// 値が同じであればtrue
			if (this.value == ((CalendarType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}