@isTest
private class AttLeaveOfAbsenceRepositoryTest {

	private static AttLeaveOfAbsenceRepository repository = new AttLeaveOfAbsenceRepository();

	private class RepoTestData {
		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public List<ComCompany__c> companyObjs;
		/** 検索テスト用の休職休業オブジェクト */
		public List<AttLeaveOfAbsence__c> leaveObjs;

		/** コンストラクタ　*/
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObjs = new List<ComCompany__c>{
					ComTestDataUtility.createCompany('Company1', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company2', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company3', this.countryObj.Id)
			};
			leaveObjs = createLeavesForSearchTest(companyObjs);
		}

		/** 検索テスト用の休職休業オブジェクトを作成する */
		private List<AttLeaveOfAbsence__c> createLeavesForSearchTest(List<ComCompany__c> companies) {
			List<AttLeaveOfAbsence__c> objs = new List<AttLeaveOfAbsence__c>();
			objs.add(ComTestDataUtility.createLeaveOfAbsence(companies[0], 'leave1_1', false));
			objs.add(ComTestDataUtility.createLeaveOfAbsence(companies[1], 'leave2_2', false));
			objs.add(ComTestDataUtility.createLeaveOfAbsence(companies[1], 'leave2_3', true));
			objs.add(ComTestDataUtility.createLeaveOfAbsence(companies[1], 'leave2_1', false));
			objs.add(ComTestDataUtility.createLeaveOfAbsence(companies[1], 'leave2_4', false));
			objs.add(ComTestDataUtility.createLeaveOfAbsence(companies[0], 'leave3_1', false));
			return objs;
		}
	}

	/**
	 * searchByIdのテスト
	 * 指定したIDで取得できること、取得した値をエンティティに正しく設定することを検証する
	 */
	@isTest static void searchByIdTest() {
		RepoTestData data = new RepoTestData();
		AttLeaveOfAbsenceEntity entity = repository.searchById(data.leaveObjs[0].id);
		System.assertEquals(data.leaveObjs[0].id, entity.id);
		System.assertEquals(data.companyObjs[0].id, entity.companyId);
		System.assertEquals(data.leaveObjs[0].UniqKey__c, entity.uniqKey);
		System.assertEquals('leave1_1', entity.code);
		System.assert(entity.name != null);
		System.assertEquals('leave1_1_L0', entity.nameL0);
		System.assertEquals('leave1_1_L1', entity.nameL1);
		System.assertEquals('leave1_1_L2', entity.nameL2);
		System.assert(!entity.isRemoved);
	}

	/**
	 * searchByIdのテスト
	 * 指定したIDのレコードが削除済みの場合、取得しない事を検証する
	 */
	@isTest static void searchByIdTestRemovedRecord() {
		RepoTestData data = new RepoTestData();
		AttLeaveOfAbsenceEntity entity = repository.searchById(data.leaveObjs[2].id);
		System.assert(entity == null);
	}

	/**
	 * searchByCodeのテスト
	 * 指定した会社、コードのレコードを取得する事を検証する
	 */
	@isTest static void searchByCodeTest() {
		RepoTestData data = new RepoTestData();
		AttLeaveOfAbsenceEntity entity = repository.searchByCode(data.companyObjs[1].id, 'leave2_2');
		System.assertEquals(data.leaveObjs[1].id, entity.id);
	}

	/**
	 * searchByCodeのテスト
	 * 指定した会社、コードのいずれかが一致しない場合、レコードを取得しない事を検証する
	 */
	@isTest static void searchByCodeTestUnmatch() {
		RepoTestData data = new RepoTestData();
		AttLeaveOfAbsenceEntity entity = repository.searchByCode(data.companyObjs[0].id, 'leave2_2');
		System.assert(entity == null);
	}

	/**
	 * searchEntityListeのテスト
	 * 論理削除済み以外のレコードを、コードの昇順で取得する事を検証する
	 */
	@isTest static void searchEntityListTest() {
		RepoTestData data = new RepoTestData();
		AttLeaveOfAbsenceRepository.SearchFilter filter = new AttLeaveOfAbsenceRepository.SearchFilter();
		filter.companyId = data.companyObjs[1].id;
		List<AttLeaveOfAbsenceEntity> entities = repository.searchEntityList(filter);

		System.assertEquals(3, entities.size());
		System.assertEquals('leave2_1', entities[0].code);
		System.assertEquals('leave2_2', entities[1].code);
		System.assertEquals('leave2_4', entities[2].code);
	}

	/**
	 * createSObjectのテスト
	 * エンティティからsObjectに想定通りに項目が詰め替えていることを検証する。
	 */
	@isTest static void createSObjectTest() {
		RepoTestData data = new RepoTestData();
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.setId(data.leaveObjs[0].Id);
		entity.companyId = data.companyObjs[0].id;
		entity.code = 'leave_1';
		entity.uniqKey = 'company_1-leave_1';
		entity.name = 'leave_name_1';
		entity.nameL0 = 'leave_nameL0_1';
		entity.nameL1 = 'leave_nameL1_1';
		entity.nameL2 = 'leave_nameL2_1';
		entity.isRemoved = false;

		AttLeaveOfAbsence__c retSObj = repository.createSObject(entity);
		System.assertEquals(entity.id, retSObj.Id);
		System.assertEquals(entity.companyId, retSObj.CompanyId__c);
		System.assertEquals('leave_1', retSObj.Code__c);
		System.assertEquals('company_1-leave_1', retSObj.UniqKey__c);
		System.assertEquals('leave_name_1', retSObj.Name);
		System.assertEquals('leave_nameL0_1', retSObj.Name_L0__c);
		System.assertEquals('leave_nameL1_1', retSObj.Name_L1__c);
		System.assertEquals('leave_nameL2_1', retSObj.Name_L2__c);
		System.assertEquals(false, retSObj.Removed__c);
	}

	/**
	 * saveEntityのテスト
	 * 新規の休職休業エンティティが1件登録されることを検証する。
	 */
	@isTest static void saveEntityTestInsert() {
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.companyId = ComTestDataUtility.createTestCompany().id;
		entity.code = 'leave_1';
		entity.uniqKey = 'company_1-leave_1';
		entity.name = 'leave_name_1';
		entity.nameL0 = 'leave_nameL0_1';
		entity.nameL1 = 'leave_nameL1_1';
		entity.nameL2 = 'leave_nameL2_1';
		entity.isRemoved = false;

		Repository.SaveResult result = new Repository.SaveResult();
		result = repository.saveEntity(entity);
		System.assertEquals(true, result.isSuccessAll);

		List<AttLeaveOfAbsence__c> retSObjList = [SELECT Id, UniqKey__c FROM AttLeaveOfAbsence__c];
		System.assertEquals(1, retSObjList.size());
		System.assertEquals('company_1-leave_1', retSObjList[0].UniqKey__c);
		System.assertEquals(result.details[0].id, retSObjList[0].Id);
	}

	/**
	 * saveEntityのテスト
	 * 休職休業エンティティと既存レコードのIDが一致する場合、新規に登録されずに既存レコードが更新されることを検証する。
	 */
	@isTest static void saveEntityTestUpdate() {
		RepoTestData data = new RepoTestData();
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.setId(data.leaveObjs[0].Id);
		entity.companyId = data.companyObjs[0].id;
		entity.code = 'leave_1';
		entity.uniqKey = 'company_1-leave_1';
		entity.name = 'leave_name_1';
		entity.nameL0 = 'leave_nameL0_1';
		entity.nameL1 = 'leave_nameL1_1';
		entity.nameL2 = 'leave_nameL2_1';
		entity.isRemoved = false;

		List<AttLeaveOfAbsence__c> retSObjListBefore = [SELECT Id, UniqKey__c FROM AttLeaveOfAbsence__c];
		System.assertEquals(6, retSObjListBefore.size());

		Repository.SaveResult result = new Repository.SaveResult();
		result = repository.saveEntity(entity);
		System.assertEquals(true, result.isSuccessAll);

		List<AttLeaveOfAbsence__c> retSObjListAfter = [SELECT Id, UniqKey__c FROM AttLeaveOfAbsence__c];
		System.assertEquals(6, retSObjListAfter.size());
		System.assertEquals('company_1-leave_1', retSObjListAfter[0].UniqKey__c);
		System.assertEquals(result.details[0].id, retSObjListAfter[0].Id);
	}

	/**
	 * saveEntityのテスト
	 * 新規の休職休業エンティティがNUllの場合、例外が発生して登録されずに終了することを検証する。
	 */
	@isTest static void saveEntityTestNullArgs() {
		try {
			repository.saveEntity(null);
			System.assert(false);
		} catch (Exception e) {
		}
	}

	/**
	 * saveEntityのテスト
	 * リストの中に、登録と更新のレコードが混在する場合の結果を検証する。
	 * 複数件の既存レコードと、休職休業エンティティが存在する時に、
	 * 休職休業エンティティと既存レコードのIDが一致する場合、新規に登録されずに既存レコードが更新されることを検証する。
	 * またIDが一致しない休職休業エンティティは新規に登録されることを検証する。
	 */
	@isTest static void saveEntityListTestInsertUpdate() {
		RepoTestData data = new RepoTestData();

		//更新
		List<AttLeaveOfAbsenceEntity> entityList = new List<AttLeaveOfAbsenceEntity>();
		AttLeaveOfAbsenceEntity entity_1 = new AttLeaveOfAbsenceEntity();
		entity_1.setId(data.leaveObjs[0].Id);
		entity_1.companyId = data.companyObjs[0].id;
		entity_1.code = 'leave_1';
		entity_1.uniqKey = 'company_1-leave_1';
		entity_1.name = 'leave_name_1';
		entity_1.nameL0 = 'leave_nameL0_1';
		entity_1.nameL1 = 'leave_nameL1_1';
		entity_1.nameL2 = 'leave_nameL2_1';
		entity_1.isRemoved = false;

		entityList.add(entity_1);

		//新規登録
		AttLeaveOfAbsenceEntity entity_2 = new AttLeaveOfAbsenceEntity();
		entity_2.companyId = data.companyObjs[0].id;
		entity_2.code = 'leave_2';
		entity_2.uniqKey = 'company_2-leave_2';
		entity_2.name = 'leave_name_2';
		entity_2.nameL0 = 'leave_nameL0_2';
		entity_2.nameL1 = 'leave_nameL1_2';
		entity_2.nameL2 = 'leave_nameL2_2';
		entity_2.isRemoved = true;

		entityList.add(entity_2);

		//新規登録（必須項目のみ）
		AttLeaveOfAbsenceEntity entity_3 = new AttLeaveOfAbsenceEntity();
		entity_3.companyId = data.companyObjs[0].id;
		entity_3.code = 'leave_3';
		entity_3.uniqKey = 'company_3-leave_3';
		entity_3.nameL0 = 'leave_nameL0_3';

		entityList.add(entity_3);

		//更新（必須項目のみ）
		AttLeaveOfAbsenceEntity entity_4 = new AttLeaveOfAbsenceEntity();
		entity_4.setId(data.leaveObjs[1].Id);
		entity_4.code = 'leave_4';
		entity_4.uniqKey = 'company_4-leave_4';
		entity_4.nameL0 = 'leave_nameL0_4';

		entityList.add(entity_4);

		List<AttLeaveOfAbsence__c> retSObjListBefore = [SELECT Id, UniqKey__c FROM AttLeaveOfAbsence__c];
		System.assertEquals(6, retSObjListBefore.size());

		Repository.SaveResult result = new Repository.SaveResult();
		result = repository.saveEntityList(entityList);
		System.assertEquals(true, result.isSuccessAll);

		List<AttLeaveOfAbsence__c> retSObjListAfter = [SELECT Id, UniqKey__c, CompanyId__c, Code__c, Name, Name_L0__c, Name_L1__c, Name_L2__c, Removed__c  FROM AttLeaveOfAbsence__c];
		System.assertEquals(8, retSObjListAfter.size());
		System.assertEquals('company_1-leave_1', retSObjListAfter[0].UniqKey__c);
		System.assertEquals('company_2-leave_2', retSObjListAfter[6].UniqKey__c);
		System.assertEquals('company_3-leave_3', retSObjListAfter[7].UniqKey__c);
		System.assertEquals('company_4-leave_4', retSObjListAfter[1].UniqKey__c);
		System.assertEquals(result.details[0].id, retSObjListAfter[0].Id);
		System.assertEquals(result.details[1].id, retSObjListAfter[6].Id);
		System.assertEquals(result.details[2].id, retSObjListAfter[7].Id);
		System.assertEquals(result.details[3].id, retSObjListAfter[1].Id);

		//新規作成の検証（必須項目が登録されていること、必須項目以外がデフォルト値であること）
		System.assertEquals(data.companyObjs[0].id, retSObjListAfter[7].CompanyId__c);
		System.assertEquals('leave_3', retSObjListAfter[7].Code__c);
		System.assert(retSObjListAfter[7].Name != null);
		System.assertEquals('leave_nameL0_3', retSObjListAfter[7].Name_L0__c);
		System.assert(retSObjListAfter[7].Name_L1__c == null);
		System.assert(retSObjListAfter[7].Name_L2__c == null);
		System.assert(!retSObjListAfter[7].Removed__c);

		//更新の検証（必須項目が更新されていること、必須項目以外が変更されていないこと）
		System.assertEquals(data.leaveObjs[1].CompanyId__c, retSObjListAfter[1].CompanyId__c);
		System.assertEquals('leave_4', retSObjListAfter[1].Code__c);
		System.assert(retSObjListAfter[1].Name != null);
		System.assertEquals('leave_nameL0_4', retSObjListAfter[1].Name_L0__c);
		System.assertEquals('leave2_2_L1', retSObjListAfter[1].Name_L1__c);
		System.assertEquals('leave2_2_L2', retSObjListAfter[1].Name_L2__c);
		System.assert(!retSObjListAfter[1].Removed__c);

	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		RepoTestData testData = new RepoTestData();
		AttLeaveOfAbsence__c leaveOfAbsenceObj = testData.leaveObjs[0];

		// キャッシュをonでインスタンスを生成
		AttLeaveOfAbsenceRepository repository = new AttLeaveOfAbsenceRepository(true);

		// 検索（DBから取得）
		AttLeaveOfAbsenceRepository.SearchFilter filter = new AttLeaveOfAbsenceRepository.SearchFilter();
		filter.ids = new Set<String>{leaveOfAbsenceObj.Id};
		AttLeaveOfAbsenceEntity leaveOfAbsence = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveOfAbsenceObj.Code__c = 'NotCache';
		upsert leaveOfAbsenceObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveOfAbsenceEntity cachedLeaveOfAbsence = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(leaveOfAbsence.id, cachedLeaveOfAbsence.id);
		System.assertNotEquals('NotCache', cachedLeaveOfAbsence.code);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		RepoTestData testData = new RepoTestData();
		AttLeaveOfAbsence__c leaveOfAbsenceObj = testData.leaveObjs[0];

		// 検索（DBから取得）
		AttLeaveOfAbsenceRepository.SearchFilter filter = new AttLeaveOfAbsenceRepository.SearchFilter();
		filter.ids = new Set<String>{leaveOfAbsenceObj.Id};
		AttLeaveOfAbsenceEntity leaveOfAbsence = new AttLeaveOfAbsenceRepository(true).searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveOfAbsenceObj.Code__c = 'NotCache';
		upsert leaveOfAbsenceObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveOfAbsenceEntity cachedLeaveOfAbsence = new AttLeaveOfAbsenceRepository(true).searchEntityList(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(leaveOfAbsence.id, cachedLeaveOfAbsence.id);
		System.assertNotEquals('NotCache', cachedLeaveOfAbsence.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheDifferentCondition() {
		RepoTestData testData = new RepoTestData();
		AttLeaveOfAbsence__c leaveOfAbsenceObj = testData.leaveObjs[0];

		// キャッシュをonでインスタンスを生成
		AttLeaveOfAbsenceRepository repository = new AttLeaveOfAbsenceRepository(true);

		// 検索（DBから取得）
		AttLeaveOfAbsenceRepository.SearchFilter filter = new AttLeaveOfAbsenceRepository.SearchFilter();
		filter.ids = new Set<String>{leaveOfAbsenceObj.Id};
		AttLeaveOfAbsenceEntity leaveOfAbsence = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveOfAbsenceObj.Code__c = 'NotCache';
		upsert leaveOfAbsenceObj;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.ids.add(testData.companyObjs[0].Id); // 検索結果に該当しないIDを追加する
		AttLeaveOfAbsenceEntity cachedLeaveOfAbsence = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(leaveOfAbsence.id, cachedLeaveOfAbsence.id);
		System.assertEquals('NotCache', cachedLeaveOfAbsence.code);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		RepoTestData testData = new RepoTestData();
		AttLeaveOfAbsence__c leaveOfAbsenceObj = testData.leaveObjs[0];

		// キャッシュをonでインスタンスを生成
		AttLeaveOfAbsenceRepository repository = new AttLeaveOfAbsenceRepository(true);

		// 検索（DBから取得）
		AttLeaveOfAbsenceRepository.SearchFilter filter = new AttLeaveOfAbsenceRepository.SearchFilter();
		filter.ids = new Set<String>{leaveOfAbsenceObj.Id};
		AttLeaveOfAbsenceEntity leaveOfAbsence = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveOfAbsenceObj.Code__c = 'NotCache';
		upsert leaveOfAbsenceObj;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveOfAbsenceEntity cachedLeaveOfAbsence = repository.searchEntityList(filter, true)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(leaveOfAbsence.id, cachedLeaveOfAbsence.id);
		System.assertEquals('NotCache', cachedLeaveOfAbsence.code);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		RepoTestData testData = new RepoTestData();
		AttLeaveOfAbsence__c leaveOfAbsenceObj = testData.leaveObjs[0];

		// キャッシュをoffでインスタンスを生成
		AttLeaveOfAbsenceRepository repository = new AttLeaveOfAbsenceRepository();

		// 検索（DBから取得）
		AttLeaveOfAbsenceRepository.SearchFilter filter = new AttLeaveOfAbsenceRepository.SearchFilter();
		filter.ids = new Set<String>{leaveOfAbsenceObj.Id};
		AttLeaveOfAbsenceEntity leaveOfAbsence = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveOfAbsenceObj.Code__c = 'NotCache';
		upsert leaveOfAbsenceObj;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveOfAbsenceEntity cachedLeaveOfAbsence = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(leaveOfAbsence.id, cachedLeaveOfAbsence.id);
		System.assertEquals('NotCache', cachedLeaveOfAbsence.code);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		AttLeaveOfAbsenceRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new AttLeaveOfAbsenceRepository().isEnabledCache);
	}
}