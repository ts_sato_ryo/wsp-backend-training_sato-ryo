/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * 休日出勤申請のサービスクラス
  * 当クラスはAttAttDailyRequestServiceからのみ実行する事を前提としています。
  * Resourceクラスなどから直接実行しないで下さい。
  */
public with sharing class AttHolidayWorkRequestService extends AttDailyRequestService.AbstractRequestProcessor {

	private final AttDailyRequestRepository dailyRequestRepository = new AttDailyRequestRepository();

	/** 検索した勤怠サマリーのキャッシュ */
	private final List<AttSummaryEntity> summaryCache = new List<AttSummaryEntity>();

	/**
	 * 申請・再申請パラメータ
	 */
	public class SubmitParam extends AttDailyRequestService.SubmitParam {
		/** 申請日 */
		public AppDate targetDate;
		/** 始業時刻 */
		public AttTime startTime;
		/** 終業時刻 */
		public AttTime endTime;
		/** 振替休日取得有無 */
		public AttSubstituteLeaveType substituteLeaveType;
		/** 振替先休日 */
		public AppDate substituteDate;
		/** 備考 */
		public String remarks;
	}

	/**
	 * 承認内容変更申請・再申請パラメータ
	 */
	public class ReapplyParam extends AttDailyRequestService.ReapplyParam {
		/** 申請日 */
		public AppDate targetDate;
		/** 始業時刻 */
		public AttTime startTime;
		/** 終業時刻 */
		public AttTime endTime;
		/** 振替休日取得有無 */
		public AttSubstituteLeaveType substituteLeaveType;
		/** 振替先休日 */
		public AppDate substituteDate;
		/** 備考 */
		public String remarks;
	}

	/**
	 * 休日出勤申請の申請/再申請を行う。
	 *
	 * @param param 申請情報
	 * @return 作成した申請情報
	 */
	public override AttDailyRequestEntity submit(AttDailyRequestService.SubmitParam submitParam) {
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest submit QueryCount1:' + Limits.getQueries());
		SubmitParam param = (SubmitParam)submitParam;

		// 再申請の場合は削除を行う
		if (param.requestId != null) {
			super.remove(dailyRequestRepository.getEntity(param.requestId));
		}
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest submit QueryCount2:' + Limits.getQueries());

		// 社員を取得する
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(param.empId, param.targetDate);
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest submit QueryCount3:' + Limits.getQueries());


		// 申請エンティティを作成する
		AttDailyRequestEntity request = createHolidayWorkRequest(param, employee);
		AttValidator.DailyRequestValidator requestValidator = new AttValidator.DailyRequestValidator(request);
		Validator.Result requestValidatorResult = requestValidator.validate();
		if (! requestValidatorResult.isSuccess()) {
			Validator.Error error = requestValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest submit QueryCount4:' + Limits.getQueries());

		// 申請を保存する
		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();
		Repository.SaveResult saveRes = requestRepo.saveEntity(request);
		request.setId(saveRes.details[0].id);
		Id requestId = saveRes.details[0].id;
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest submit QueryCount5:' + Limits.getQueries());

		// 休日出勤申請バリデーション
		AttRecordEntity holiday = getAttRecord(employee, param.targetDate);
		AttValidator.HolidayWorkRequestSubmitValidator holidayWorkValidator =
			new AttValidator.HolidayWorkRequestSubmitValidator(holiday, request);
		Validator.Result holidayWorkValidatorResult = holidayWorkValidator.validate();
		if (!holidayWorkValidatorResult.isSuccess()) {
			Validator.Error error = holidayWorkValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 勤怠明細エンティティの申請IDを更新する
		holiday.reqRequestingHolidayWorkRequestId = requestId;

		// 更新対象の勤怠明細
		List<AttRecordEntity> updateRecords = new List<AttRecordEntity>();
		updateRecords.add(holiday);

		// 振替申請の場合
		if (AttSubstituteLeaveType.SUBSTITUTE.equals(param.substituteLeaveType)) {
			// 振替先の明細を取得
			AttSummaryEntity substituteSummary = getAttSummary(employee, param.substituteDate);
			AttRecordEntity substituteRecord = substituteSummary.getAttRecord(param.substituteDate);

			// 振替休日のバリデーション
			AttValidator.SubstituteLeaveRequestSubmitValidator substituteValidator =
					new AttValidator.SubstituteLeaveRequestSubmitValidator(substituteRecord);
			Validator.Result validatorResult = substituteValidator.validate();
			if (!validatorResult.isSuccess()) {
				Validator.Error error = validatorResult.getErrors()[0];
				throw new App.IllegalStateException(error.code, error.message);
			}
			// 振替申請作成(振替取得日の勤怠設定をチェックしない)
			AttDailyRequestEntity substituteRequest = createSubstituteLeaveRequest(getAttSummary(employee, param.targetDate), request, param.substituteDate, holiday.outOriginalDayType);
			saveRes = requestRepo.saveEntity(substituteRequest);
			substituteRequest.setId(saveRes.details[0].id);
			System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest submit QueryCount6:' + Limits.getQueries());

			// IDが一緒の場合は、再申請
			if (substituteRecord.reqRequestingLeave1RequestId == null || substituteRecord.reqRequestingLeave1RequestId == substituteRequest.id) {
				substituteRecord.reqRequestingLeave1RequestId = substituteRequest.id;
			}
			else if (substituteRecord.reqRequestingLeave2RequestId == null || substituteRecord.reqRequestingLeave2RequestId == substituteRequest.id) {
				substituteRecord.reqRequestingLeave2RequestId = substituteRequest.id;
			}
			else {
				throw new App.IllegalStateException(
						ComMessage.msg().Att_Err_SubstituteDate +
						ComMessage.msg().Att_Slt_SubstituteDateRemoveRequest); // 振替取得日の申請を取下げください
			}
			updateRecords.add(substituteRecord);
		}

		// 勤怠明細を保存
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		summaryRepo.saveRecordEntityList(updateRecords);

		// 申請する(申請コメントなし)
		super.submitProcess(request, null);
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest submit QueryCount7:' + Limits.getQueries());

		// 親の申請を返却する
		return request;
	}

	/**
	 * 休日出勤申請の承認内容変更申請/再申請を行う。
	 *
	 * @param param 申請情報
	 * @return 作成した申請情報
	 */
	public override AttDailyRequestEntity reapply(AttDailyRequestService.ReapplyParam reapplyParam) {
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest reapply QueryCount1:' + Limits.getQueries());
		ReapplyParam param = (ReapplyParam)reapplyParam;

		// 再申請の場合は取下げを行う
		if (param.requestId != null) {
			super.remove(dailyRequestRepository.getEntity(param.requestId));
		}
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest reapply QueryCount2:' + Limits.getQueries());

		// 変更対象の申請を取得
		AttDailyRequestEntity approvedParent = dailyRequestRepository.getEntity(param.originalRequestId);
		AttDailyRequestEntity approvedChild = dailyRequestRepository.getEntityByParentId(param.originalRequestId);
		if (approvedParent == null ||
			(AttSubstituteLeaveType.SUBSTITUTE.equals(approvedParent.substituteLeaveType) && approvedChild == null)) {
			throw new App.RecordNotFoundException();
		}
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest reapply QueryCount3:' + Limits.getQueries());

		// 承認内容元申請の状態をチェック
		if (!approvedParent.canReapply()) {
			throw new App.IllegalStateException(App.ERR_CODE_CANNOT_REAPPLY, ComMessage.msg().Att_Err_CannotProcessRequest);
		}

		// 申請内容に変更がない場合はエラーとする
		if (!isRequestChanged(approvedParent, approvedChild, param)) {
			App.ParameterException e = new App.ParameterException();
			e.setCode(App.ERR_CODE_ATT_INVALID_HOLIDAY_WORK_REQUEST);
			e.setMessage(ComMessage.msg().Att_Err_NoChangedRequest);
			throw e;
		}

		// 申請者を取得
		EmployeeService employeeService = new EmployeeService();
		EmployeeBaseEntity employee = employeeService.getEmployeeByUserIdFromCache(approvedParent.ownerId, approvedParent.startDate);
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest reapply QueryCount4:' + Limits.getQueries());


		// 新たに休日出勤申請を登録する
		AttDailyRequestEntity holidayRequest = createHolidayWorkRequest(param, employee);
		Repository.SaveResult result = dailyRequestRepository.saveEntity(holidayRequest);
		holidayRequest.setId(result.details[0].id);

		// 勤怠明細を取得し、バリデーションを実行する
		Boolean isSubstituteRequest = AttSubstituteLeaveType.SUBSTITUTE.equals(param.substituteLeaveType);
		AttRecordEntity holidayBefore = getAttRecord(employee, approvedParent.startDate);
		AttRecordEntity holidayAfter = getAttRecord(employee, param.targetDate);
		AttRecordEntity substituteBefore = approvedChild == null ? null : getAttRecord(employee, approvedChild.startDate);
		AttRecordEntity substituteAfter =  isSubstituteRequest ? getAttRecord(employee, param.substituteDate) : null;
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest reapply QueryCount5:' + Limits.getQueries());

		AttValidator.HolidayWorkRequestReapplyValidator validator =
				new AttValidator.HolidayWorkRequestReapplyValidator(holidayBefore, holidayAfter, substituteBefore, substituteAfter, approvedParent, holidayRequest);
		Validator.Result validateResult = validator.validate();
		if (!validateResult.isSuccess()) {
			Validator.Error error = validateResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 既存の申請のステータスを更新する
		List<AttDailyRequestEntity> updateRequests = new List<AttDailyRequestEntity>();
		approvedParent.status = AppRequestStatus.REAPPLYING;
		updateRequests.add(approvedParent);
		if (approvedChild != null) {
			approvedChild.status = AppRequestStatus.REAPPLYING;
			updateRequests.add(approvedChild);
		}
		dailyRequestRepository.saveEntityList(updateRequests);

		// 勤怠明細に申請IDを設定する
		holidayAfter.reqRequestingHolidayWorkRequestId = holidayRequest.id;

		// 振替申請作成
		List<AttRecordEntity> updateRecords = new List<AttRecordEntity>();
		updateRecords.add(holidayAfter);
		if (isSubstituteRequest) {
			AttDailyRequestEntity substituteRequest = createSubstituteLeaveRequest(getAttSummary(employee, param.substituteDate), holidayRequest, param.substituteDate, holidayAfter.outOriginalDayType);
			substituteRequest.setId(dailyRequestRepository.saveEntity(substituteRequest).details[0].id);
			System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest reapply QueryCount6:' + Limits.getQueries());

			if (substituteAfter.reqRequestingLeave1RequestId == null) {
				substituteAfter.reqRequestingLeave1RequestId = substituteRequest.id;
			} else if (substituteAfter.reqRequestingLeave2RequestId == null) {
				substituteAfter.reqRequestingLeave2RequestId = substituteRequest.id;
			} else {
				throw new App.IllegalStateException(
						ComMessage.msg().Att_Err_SubstituteDate +
						ComMessage.msg().Att_Slt_SubstituteDateRemoveRequest); // 振替取得日の申請を取下げください
			}
			updateRecords.add(substituteAfter);
		}

		// 勤怠明細を保存する
		AttSummaryRepository summaryRepository = new AttSummaryRepository();
		summaryRepository.saveRecordEntityList(updateRecords);

		// 申請する(申請コメントなし)
		super.submitProcess(holidayRequest, null);
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest reapply QueryCount7:' + Limits.getQueries());

		// 親の申請を返却する
		return holidayRequest;
	}

	/**
	 * 申請の取下処理を行う。
	 * 承認内容変更申請の場合、元の申請を承認済みに戻す。
	 * @param request 対象の申請
	 * @throws App.IllegalStateException 申請が存在しない場合
	 * @throws App.IllegalStateException 申請ステータスが「無効」ではない場合
	 */
	public override void remove(AttDailyRequestEntity request) {
		super.remove(request);

		// 振替申請の場合は、子申請も同時に更新する
		AttDailyRequestEntity child = dailyRequestRepository.getEntityByParentId(request.id);
		if (child != null) {
			child.isConfirmationRequired = false;
			dailyRequestRepository.saveEntity(child);
		}
	}

	/**
	 * @description 承認取消処理を行う
	 * @param request 対象の申請
	 * @throws App.IllegalStateException 申請ステータスが「承認済み」ではない場合
	 */
	public override void cancelApproval(AttDailyRequestEntity request) {
		if (!request.canCancelApproval()) {
			throw new App.IllegalStateException(App.ERR_CODE_CANNOT_CANCEL_APPROVAL, ComMessage.msg().Att_Err_CannotProcessRequest);
		}

		EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserId(request.ownerId, request.startDate);

		// 申請対象となる勤怠明細レコードとサマリーを取得する
		AttSummaryRepository attSummaryReposiroty = new AttSummaryRepository();
		List<AttRecordEntity> records = attSummaryReposiroty.searchRecordEntityList(employee.id, request.startDate, request.endDate);

		// 振替申請の場合、振替先の明細を取得
		if (AttSubstituteLeaveType.SUBSTITUTE.equals(request.substituteLeaveType)) {
			records.addAll(attSummaryReposiroty.searchRecordEntityList(employee.id, request.substituteLeave1Date, request.substituteLeave1Date));
		}

		AttValidator.CancelDailyRequestValidator cancelDailyValidator = new AttValidator.CancelDailyRequestValidator(records);
		Validator.Result requestValidatorResult = cancelDailyValidator.validate();
		if (! requestValidatorResult.isSuccess()) {
			Validator.Error error = requestValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 承認取消
		request.status = AppRequestStatus.DISABLED; // TODO エンティティのメソッドにする
		dailyRequestRepository.saveEntity(request);

		// 最終更新者を取得するため、再度日次申請エンティティを取得する
		request = dailyRequestRepository.getEntity(request.Id);

		// メール送信（エラーがある場合も、承認取消の処理は中断しない）
		try {
			sendCancelMail(request);
		} catch (Exception e) {
			System.debug('エラーメッセージ: ' + e.getMessage());
			System.debug('エラー発生箇所: ' + e.getLineNumber() + '[' + e.getTypeName() + ']');
		}
	}

	/**
	 * 申請内容に変更があるか判定する
	 * @param holiday 承認済みの休日出勤申請
	 * @param substitute 承認済みの休暇申請（振替申請） もとの申請に振替休日がない場合はnull
	 * @param param 承認内容変更申請
	 */
	@TestVisible
	private Boolean isRequestChanged(AttDailyRequestEntity holiday, AttDailyRequestEntity substitute, ReapplyParam param) {
		return isHolidayWorkChanged(holiday, param) || isSubstituteChanged(substitute, param);
	}
	// 休日出勤日に変更があるか判定する
	private Boolean isHolidayWorkChanged(AttDailyRequestEntity approved, ReapplyParam param) {
		// 休日出勤日
		if (!approved.startDate.equals(param.targetDate)) {
			return true;
		}
		// 開始時刻
		if (!approved.startTime.equals(param.startTime)) {
			return true;
		}
		// 終了時刻
		if (!approved.endTime.equals(param.endTime)) {
			return true;
		}
		// 備考
		String approvedRemarks = approved.remarks == null ? '' : approved.remarks;
		String requestRemarks =  param.remarks == null ? '' : param.remarks;
		if (!approvedRemarks.equals(requestRemarks)) {
			return true;
		}
		return false;
	}
	// 振替休日に変更があるか判定する
	private Boolean isSubstituteChanged(AttDailyRequestEntity approved, ReapplyParam param) {
		// 振替なし→振替なし
		if (approved == null && param.substituteLeaveType == null) {
			return false;
		}
		// 振替なし→振替あり or 振替あり→振替なし
		if ((approved == null && param.substituteLeaveType != null) ||
			approved != null && param.substituteLeaveType == null) {
			return true;
		}
		// 振替休日の判定
		return !approved.startDate.equals(param.substituteDate);
	}

	/**
	 * 休日出勤申請用の勤怠日次申請エンティティを作成する
	 * @param param 休日出勤申請パラメータ
	 * @param employeeName 社員名
	 * @param employee 社員
	 * @return 作成した日時申請エンティティ
	 */
	private AttDailyRequestEntity createHolidayWorkRequest(SubmitParam param, EmployeeBaseEntity employee) {
		AttDailyRequestEntity entity = new AttDailyRequestEntity();
		entity.setId(param.requestId);
		entity.name = createHolidayWorkRequestName(employee, param.targetDate, param.startTime, param.endTime, getCompanyLanguage(employee.companyId));
		entity.requestType = AttRequestType.HOLIDAYWORK;
		entity.status = AppRequestStatus.PENDING;
		entity.requestTime = AppDatetime.now();
		entity.remarks = param.remarks;
		entity.startDate = param.targetDate;
		entity.endDate = param.targetDate;
		entity.startTime = param.startTime;
		entity.endTime = param.endTime;
		if (AttSubstituteLeaveType.SUBSTITUTE.equals(param.substituteLeaveType)) {
			entity.substituteLeaveRange = AttLeaveRange.RANGE_DAY;
			entity.substituteLeaveType = AttSubstituteLeaveType.SUBSTITUTE;
			entity.substituteLeave1Date = param.substituteDate;
			entity.substituteLeave1Range = AttLeaveRange.RANGE_DAY;
		} else {
			entity.substituteLeaveRange = null;
			entity.substituteLeaveType = null;
			entity.substituteLeave1Date = null;
			entity.substituteLeave1Range = null;
		}
		entity.cancelType = AppCancelType.NONE;
		entity.isConfirmationRequired = false;
		super.setRequestCommInfo(entity, employee);
		return entity;
	}

	/**
	 * 休日出勤申請用の承認内容変更申請の勤怠日次申請エンティティを作成する
	 * @param param 休日出勤 承認内容変更申請パラメータ
	 * @param employeeName 社員名
	 * @param employee 社員
	 * @return 作成した日時申請エンティティ
	 */
	private AttDailyRequestEntity createHolidayWorkRequest(ReapplyParam param, EmployeeBaseEntity employee) {
		// 申請パラメータを作成
		SubmitParam createParam = new SubmitParam();
		createParam.requestId = param.requestId;
		createParam.targetDate = param.targetDate;
		createParam.startTime = param.startTime;
		createParam.endTime = param.endTime;
		createParam.substituteLeaveType = param.substituteLeaveType;
		createParam.substituteDate = param.substituteDate;
		createParam.remarks = param.remarks;
		AttDailyRequestEntity entity = createHolidayWorkRequest(createParam, employee);

		// 承認内容変更申請用に申請元申請IDを設定する
		entity.originalRequestId = param.originalRequestId;
		return entity;
	}

	/**
	 * 振替申請用の勤怠日次申請エンティティを作成する
	 * @return 作成した日時申請エンティティ
	 */
	private AttDailyRequestEntity createSubstituteLeaveRequest(
			AttSummaryEntity attSummary,
			AttDailyRequestEntity parentRequest,
			AppDate substituteDate,
			AttDayType substituteDayType) {

		AttWorkingTypeBaseEntity workingType = new AttWorkingTypeService().getWorkingType(attSummary.workingTypeBaseId, attSummary.endDate);
		AttWorkingTypeHistoryEntity workingTypeHistory = workingType.getHistory(0);

		// 振替休暇マスタを取得する
		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveEntity substituteLeave = null;
		for (AttLeaveEntity leaveData : leaveRep.getLeaveByCodes(workingTypeHistory.leaveCodeList, workingType.companyId)) {
			if (AttLeaveType.SUBSTITUTE.equals(leaveData.leaveType)) {
				substituteLeave = leaveData;
				break;
			}
		}
		if (substituteLeave == null) {
			throw new App.IllegalStateException(ComMessage.msg().Att_Err_NotSetSubstituteLeave);
		}

		// 再申請の場合は、同じレコードを使用する
	 	AttDailyRequestEntity entity = dailyRequestRepository.getEntityByParentId(parentRequest.Id);
		if (entity == null) {
			entity = new AttDailyRequestEntity();
		}
		entity.parentRequestId = parentRequest.id;
		entity.requestType = AttRequestType.LEAVE;
		entity.status = parentRequest.status;
		entity.requestTime = parentRequest.requestTime;
		entity.startDate = substituteDate;
		entity.endDate = substituteDate;
		entity.leaveId = substituteLeave.id;
		entity.leaveType = substituteLeave.leaveType;
		entity.leaveRange = AttLeaveRange.RANGE_DAY;
		entity.substituteLeaveDayType = substituteDayType;
		entity.leaveTotalTime = workingTypeHistory.contractedWorkHours.getIntValue();
		entity.employeeId = parentRequest.employeeId;
		entity.actorId = parentRequest.actorId;
		entity.ownerId = parentRequest.ownerId;
		return entity;
	}

	/**
	 * 休日出勤申請の申請名を作成する
	 * @param employeeName 社員名
	 * @param targetDate 対象日
	 * @param startTime 開始時刻
	 * @param endTime 修了時刻
	 * @param lang 作成言語
	 * @return 申請名
	 */
	@testVisible
	private String createHolidayWorkRequestName(EmployeeBaseEntity employee,
			AppDate targetDate, AttTime startTime, AttTime endTime, AppLanguage lang) {

		// 日付フォーマット
		final AppDate.FormatType dateFormat = AppDate.FormatType.YYYYMMDD;

		// バインド文字列
		List<String> args = new List<String>{
			employee.displayNameL.getValue(lang),
			targetDate.format(dateFormat),
			startTime.format(),
			endTime.format()};

		return lang == AppLanguage.JA ?
			String.format('{0}さん 休日出勤申請 {1} {2}–{3}', args) :
			String.format('{0} Holiday Work Request {1} {2}–{3}', args);
	}

	/**
	 * 勤怠サマリーを取得する。
	 * 対象データを検索済みの場合はキャッシュから取得し、未検索の場合はDBへ検索し結果を返す。
	 * @param employee 対象社員
	 * @param targetDate 取得対象日
	 * @return 勤怠サマリー
	 */
	private AttSummaryEntity getAttSummary(EmployeeBaseEntity employee, AppDate targetDate) {
		// キャッシュに存在する場合
		for (AttSummaryEntity summary : summaryCache) {
			if (summary.hasAttRecord(targetDate)) {
				return summary;
			}
		}

		AttSummaryEntity summary = new AttAttendanceService().getSummary(employee.id, targetDate);
		System.debug(LoggingLevel.DEBUG, 'HolidayWorkRequest getAttSummary QueryCount:' + Limits.getQueries());
		summaryCache.add(summary);
		return summary;
	}

	/**
	 * 勤怠明細を取得する。
	 * 対象データを検索済みの場合はキャッシュから取得し、未検索の場合はDBへ検索し結果を返す。
	 * @param employee 対象社員
	 * @param targetDate 取得対象日
	 * @return 勤怠明細
	 */
	private AttRecordEntity getAttRecord(EmployeeBaseEntity employee, AppDate targetDate) {
		return getAttSummary(employee, targetDate).getAttRecord(targetDate);
	}

	/**
	 * @description 承認取消時のメールを作成する
	 * @param request 申請
	 * @param applicant 申請者
	 * @param empLastModified 最終更新者
	 * @param targetLanguage 送信先ユーザの言語
	 * @return mailMessage メール内容
	 */
	public override MailService.MailParam createCancelMailMessage(AttDailyRequestEntity request, EmployeeBaseEntity applicant, EmployeeBaseEntity empLastModified, AppLanguage targetLanguage) {

		MailService.MailParam mailMessage = new MailService.MailParam();
		String language = targetLanguage.value;

		// 送信者名
		String senderDisplayName = empLastModified.fullNameL.getFullName(targetLanguage);
		mailMessage.senderDisplayName = senderDisplayName;

		// メールタイトル
		String mailSubject;
		// 申請種別を文字列に変換する
		String requestType = ComMessage.msg(language).Att_Lbl_RequestTypeHolidayWork;
		mailSubject = ComMessage.msg(language).Att_Msg_CancelMailSubject(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType});
		mailMessage.subject = mailSubject;

		// メール本文
		String mailBody;
		// 申請日付
		String requestDate = request.requestTime.formatLocal().substring(0, 10).replace('-', '/');
		// 開始日
		String startDate = String.valueOf(AppDate.convertDate(request.startDate)).replace('-', '/');
		// 開始時刻
		String startTime = request.startTime.format();
		// 終了時刻
		String endTime = request.endTime.format();
		// 取得休暇タイプ
		String substituteLeaveType = convertSubstituteLeaveType(request.substituteLeaveType, language);
		// 振替休日取得日
		String substituteDate = request.substituteLeave1Date != null ? request.substituteLeave1Date.format().replace('-', '/') : null;
		// 備考
		String remarks = request.remarks;
		// メール本文を作成
		mailBody = ComMessage.msg(language).Att_Msg_CancelMailMessage(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType, empLastModified.fullNameL.getFullName(targetLanguage)});
		mailBody += '\r\n\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_RequestContents + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestDate + '：' + requestDate + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestType + '：' + requestType + '\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate;
		mailBody += ' ' + startTime + '－' + endTime + '\r\n';
		if (String.isNotBlank(substituteLeaveType)) {
			mailBody += ComMessage.msg(language).Att_Lbl_ReplacementDayOff + '：' + substituteLeaveType + '\r\n';
		}
		if (String.isNotBlank(substituteDate)) {
			mailBody += ComMessage.msg(language).Att_Lbl_ScheduledDateOfSubstitute + '：' + substituteDate + '\r\n';
		}
		if (String.isNotBlank(remarks)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + remarks + '\r\n';
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + '\r\n';
		}
		// 署名
		mailBody += '\r\n---\r\n';
		mailBody += ComMessage.msg(language).Att_Msg_TeamSpirit;
		mailMessage.body = mailBody;

		return mailMessage;
	}

	/**
	 * 取得休暇タイプ
	 */
	private String convertSubstituteLeaveType(AttSubstituteLeaveType substituteLeaveType, String targerLanguage) {

		String substituteLeaveTypeStr;
		if (AttSubstituteLeaveType.SUBSTITUTE.equals(substituteLeaveType)) {
			substituteLeaveTypeStr = ComMessage.msg(targerLanguage).Att_Lbl_SubstituteHoliday;
		}
		return substituteLeaveTypeStr;
	}
}