/**
 * 共通処理ロジック
 */
public with sharing class ComLogic {

	private static ComDto.OrgSettingDto settingDto = null;
	private static Map<Id, ComDto.EmpDto> empDtoMap = new Map<Id, ComDto.EmpDto>();

	/**
	 * 組織設定情報を取得する処理（既に取得されている場合、クエリしない）
	 * @return 組織設定情報
	 */
	public static ComDto.OrgSettingDto getOrgSetting() {
		// 同一トランザクション内にて既に取得されている場合はクエリしない
		if (settingDto != null) {
			return settingDto;
		}

		// デフォルト設定
		ComOrgSetting__c defaultObj = new ComOrgSetting__c();
		defaultObj.Language_0__c = ComConst.LANG_EN;
		ComDto.OrgSettingDto dto = new ComDto.OrgSettingDto(defaultObj);

		for (ComOrgSetting__c setting : [
			SELECT Language_0__c, Language_1__c, Language_2__c
			FROM ComOrgSetting__c
			ORDER BY CreatedDate, Id
			LIMIT 1]) {
			dto = new ComDto.OrgSettingDto(setting);
		}
		settingDto = dto;
		return settingDto;
	}

	/**
	 * ユーザーIdにより社員情報を取得する処理
	 * @param userId 対象とするユーザーId。nullの場合、ログインユーザーのId。
	 * @return 社員情報
	 */
	public static ComDto.EmpDto getEmpByUserId(Id userId) {
		return new ComDto.EmpDto(new ComEmpBase__c());
	}

	/**
	 * ログインユーザが取得できるジョブ情報を取得
	 * @param parentId 親ジョブId
	 * @return ジョブ一覧
	 */
	public static List<ComDto.JobDto> getJobList(Id parentId, String targetDate) {
		return new List<ComDto.JobDto>();
	}

	/**
	 * 削除予定
	 * @param  pId   [description]
	 * @param  pDate [description]
	 * @return       [description]
	 */
	public static List<ComDto.WorkCategoryDto> getWorkCategoryList(Id pId, Date pDate) {
		return new List<ComDto.WorkCategoryDto>();
	}

}