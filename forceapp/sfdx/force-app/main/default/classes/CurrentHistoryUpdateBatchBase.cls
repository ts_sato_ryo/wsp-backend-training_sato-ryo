/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 履歴切り替えバッチの基本クラス
 */
public abstract with sharing class CurrentHistoryUpdateBatchBase implements Database.Batchable<sObject>, Database.Stateful {

	/** ベースオブジェクトのAPI参照名 */
	protected final String BASE_OBJECT_NAME;
	/** 履歴オブジェクトのAPI参照名 */
	protected final String HISTORY_OBJECT_NAME;
	/** Name項目のAPI参照名 */
	private final String NAME_FIELD_NAME = 'Name';
	/** ベースオブジェクト.現在の履歴項目のAPI参照名 */
	private final String CURRENT_ID_FIELD_NAME = 'CurrentHistoryId__c';
	/** 履歴オブジェクト.有効開始日のAPI参照名 */
	private final String VALID_FROM_FIELD_NAME = 'ValidFrom__c';
	/** 履歴オブジェクト.失効日のAPI参照名 */
	private final String VALID_TO_FIELD_NAME = 'ValidTo__c';
	/** 履歴オブジェクト.Name(L0)のAPI参照名 */
	private final String NAME_L0_FIELD_NAME = 'Name_L0__c';

	/** ベースオブジェクトのName項目を更新しないオブジェクト(履歴オブジェクトのName_L0を取得しません) */
	private final Set<SObjectType> NOT_UPDATE_NAME_BASE_OBJECT =
			new Set<SObjectType>{ComEmpBase__c.getSObjectType()};

	/**
	 * コンストラクタ
	 * @param  base_oobject_name    [description]
	 * @param  history_oobject_name [description]
	 * @return                      [description]
	 */
	public CurrentHistoryUpdateBatchBase(SObjectType base_object_type, SObjectType history_object_type) {
		BASE_OBJECT_NAME = base_object_type.getDescribe().getName();
		HISTORY_OBJECT_NAME = history_object_type.getDescribe().getName();
	}

	/**
	 * バッチ開始処理
	 * @param  bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public virtual Database.QueryLocator start(Database.BatchableContext bc) {
		// ベースレコードを全取得する
		String soql = 'SELECT Id FROM ' + BASE_OBJECT_NAME;
		system.debug('@@@@' + soql);
		return Database.getQueryLocator(soql);
	}

	/**
	 * バッチ実行処理
	 * @param  bc    バッチコンテキスト
	 * @param  scope
	 */
	public virtual void execute(Database.BatchableContext bc, List<SObject> scope) {
		final SObjectType BASE_OBJECT_TYPE = Schema.getGlobalDescribe().get(BASE_OBJECT_NAME);
		Date currentDate = System.today();

		Set<Id> baseIdSet = new Set<Id>();
		for (SObject sobj:scope) {
			baseIdSet.add(sobj.id);
		}

		String soql = 'SELECT ' +
				+ 'Id, BaseId__c, ValidFrom__c, ValidTo__c';
		if (! NOT_UPDATE_NAME_BASE_OBJECT.contains(BASE_OBJECT_TYPE)) {
			soql += ', Name_L0__c';
		}
		soql += ' FROM ' + HISTORY_OBJECT_NAME +
			' WHERE BaseId__c IN :baseIdSet' +
			' AND Removed__c = false' +
			' ORDER BY ValidFrom__c DESC, ValidTo__c DESC';

		List<SObject> historyRecords = (List<SObject>)Database.query(soql);

		// key => ベースID, value => 現在の履歴ID のマッピング
		Map<Id, Id> currentHistoryIdMap = new Map<Id, Id>();
		// key => ベースID, value => 最新の履歴ID のマッピング
		Map<Id, Id> latestHistoryIdMap = new Map<Id, Id>();
		// key => 履歴ID, value => 履歴オブジェクト のマッピング
		Map<Id, SObject> historyMap = new Map<Id, SObject>();

		for (SObject h:historyRecords) {
			Id baseId = (Id)h.get('BaseId__c');
			Date validFrom = (Date)h.get(VALID_FROM_FIELD_NAME);
			Date validTo = (Date)h.get(VALID_TO_FIELD_NAME);

			historyMap.put(h.Id, h);

			if (!currentHistoryIdMap.containsKey(baseId) && validFrom <= currentDate && validTo >= currentDate) {
				currentHistoryIdMap.put(baseId, h.Id);
			}
			if (!latestHistoryIdMap.containsKey(baseId)) {
				latestHistoryIdMap.put(baseId, h.Id);
			}
		}

		// ベースレコードのCurrentIdを更新
		List<SObject> upBaseRecordList = new List<SObject>();
		for (SObject sobj:scope) {
			Id baseId = sobj.Id;
			Id currentId = null;
			if (currentHistoryIdMap.containsKey(baseId)) {
				// 現在の履歴があれば、履歴IDをセット
				currentId = currentHistoryIdMap.get(baseId);
			} else if (latestHistoryIdMap.containsKey(baseId)) {
				// 現在の履歴が無ければ、最新の履歴IDをセット
				currentId = latestHistoryIdMap.get(baseId);
			}

			Sobject upBase = BASE_OBJECT_TYPE.newSObject(baseId);
			upBase.put('Id', baseId);
			upBase.put(CURRENT_ID_FIELD_NAME, currentId);
			upBaseRecordList.add(upBase);
		}

		// Name更新対象オブジェクトであればNameを設定する
		if (! NOT_UPDATE_NAME_BASE_OBJECT.contains(BASE_OBJECT_TYPE)) {
			for (SObject upBase : upBaseRecordList) {
				Id currentId = (Id)upBase.get(CURRENT_ID_FIELD_NAME);
				if (currentId != null) {
					SObject currentHistory = historyMap.get(currentId);
					upBase.put(NAME_FIELD_NAME, currentHistory.get(NAME_L0_FIELD_NAME));
				}
			}
		}

		// TODO: 例外通知
		AppDatabase.doUpdate(upBaseRecordList);

	}

	public virtual void finish(Database.BatchableContext bc) {
	}

}
