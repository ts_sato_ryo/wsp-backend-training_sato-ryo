/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * @description TimeRequestEntityのテストクラス
 */
@isTest
private class TimeRequestEntityTest {

	/**
	 * 参照項目テスト
	 * 参照項目がない場合にエラーを発生しないことを確認する
	 * 参照項目がない場合にnull または 初期値が返ってくることを確認する
	 */
	 @isTest static void noReferenceItemTest() {
		// timeSummaryIdでひもづくもの
		// summaryName・startDate・endDate
		 {
			 try {
				 TimeRequestEntity entity = new TimeRequestEntity();
				 // Case-1 : timeSummaryIdの紐付けがない
				 System.assertEquals(null, entity.summaryName);
				 System.assertEquals(null, entity.startDate);
				 System.assertEquals(null, entity.endDate);

				 // Case-2 : timeSummaryIdの紐付けがある
				 entity.timeSummaryId = UserInfo.getUserId();// UserInfo.getUserId()はダミーコード
				 System.assertEquals(null, entity.summaryName);
				 System.assertEquals(null, entity.startDate);
				 System.assertEquals(null, entity.endDate);
			 } catch (Exception e) {
				 TestUtil.fail('Unexpected Error occured ！' + e.getCause());
			 }
		 }
		// employeeIdで紐付くもの
		// employeeNameL・photoUrl
		 {
			 try {
				 TimeRequestEntity entity = new TimeRequestEntity();
				 // Case-1 : employeeIdの紐付けがない
				 System.assertEquals(null, entity.employeeNameL);
				 System.assertEquals(null, entity.photoUrl);

				 // Case-2 : employeeIdの紐付けがある
				 entity.employeeId = UserInfo.getUserId();// UserInfo.getUserId()はダミーコード
				 System.assertEquals(null, entity.employeeNameL.firstNameL0);
				 System.assertEquals(null, entity.employeeNameL.firstNameL1);
				 System.assertEquals(null, entity.employeeNameL.firstNameL2);
				 System.assertEquals(null, entity.employeeNameL.lastNameL0);
				 System.assertEquals(null, entity.employeeNameL.lastNameL1);
				 System.assertEquals(null, entity.employeeNameL.lastNameL2);
				 System.assertEquals(null, entity.photoUrl);

			 } catch (Exception e) {
				 TestUtil.fail('Unexpected Error occured ！' + e.getCause());
			 }
		 }
		// departmentIdで紐付くもの
		// employeeBaseId・employeeNameL・photoUrl・departmentBaseId
		 {
			 try {
				 TimeRequestEntity entity = new TimeRequestEntity();
				 // Case-1 : departmentIdの紐付けがない
				 System.assertEquals(null, entity.departmentNameL);

				 // Case-2 : departmentIdの紐付けがある
				 entity.departmentId = UserInfo.getUserId();// UserInfo.getUserId()はダミーコード
				 System.assertEquals(null, entity.departmentNameL.valueL0);
				 System.assertEquals(null, entity.departmentNameL.valueL1);
				 System.assertEquals(null, entity.departmentNameL.valueL2);

			 } catch (Exception e) {
				 TestUtil.fail('Unexpected Error occured ！' + e.getCause());
			 }
		 }
	 }

	/**
	 * getStatusLabelのテスト
	 * ステータスが正しく取得できることを確認する
	 */
	@isTest static void getStatusLabelTest() {
		// 未申請
		System.assertEquals('NotRequested', TimeRequestEntity.getStatusLabel(null, null));
		// 申請中
		System.assertEquals('Pending', TimeRequestEntity.getStatusLabel(AppRequestStatus.PENDING, null));
		// 承認済み
		System.assertEquals('Approved', TimeRequestEntity.getStatusLabel(AppRequestStatus.APPROVED, null));
		// 却下
		System.assertEquals('Rejected', TimeRequestEntity.getStatusLabel(AppRequestStatus.DISABLED, AppCancelType.REJECTED));
		// 申請取消
		System.assertEquals('Removed', TimeRequestEntity.getStatusLabel(AppRequestStatus.DISABLED, AppCancelType.REMOVED));
		// 承認取消
		System.assertEquals('Canceled', TimeRequestEntity.getStatusLabel(AppRequestStatus.DISABLED, AppCancelType.CANCELED));

	}

}