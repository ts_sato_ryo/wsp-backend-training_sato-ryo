@isTest
private class ComDtoTest {

	/**
	 * 一覧情報取得用Dto（空リスト）
	 */
	@isTest private static void ListViewDtoEmptyTest() {
		List<ComEmpBase__c> objList = new List<ComEmpBase__c>();

		Test.startTest();

		// ページング情報なし
		ComDto.ListViewDto noPagingDto = new ComDto.ListViewDto(objList);
		// ページング情報あり
		ComDto.ListViewDto pagingDto = generatePagingListViewDto(objList);

		Test.stopTest();

		assertListViewDto(noPagingDto, 1, objList.size(), objList.size(), 1, false);
		assertListViewDto(pagingDto, 1, ComConst.PAGING_PAGE_SIZE, objList.size(), ((Integer)(objList.size() / ComConst.PAGING_PAGE_SIZE) + 1), false);
	}

	/**
	 * 一覧情報取得用Dto（ページングサイズリスト）
	 */
	@isTest private static void ListViewDtoPagingSizeTest() {
		List<ComEmpBase__c> objList = new List<ComEmpBase__c>();

		for (Integer index = 0 ; index < ComConst.PAGING_PAGE_SIZE ; index++ ) {
			objList.add(new ComEmpBase__c());
		}

		Test.startTest();

		// ページング情報なし
		ComDto.ListViewDto noPagingDto = new ComDto.ListViewDto(objList);
		// ページング情報あり
		ComDto.ListViewDto pagingDto = generatePagingListViewDto(objList);

		Test.stopTest();

		assertListViewDto(noPagingDto, 1, objList.size(), objList.size(), 1, false);
		assertListViewDto(pagingDto, 1, ComConst.PAGING_PAGE_SIZE, objList.size(), ((Integer)(objList.size() / ComConst.PAGING_PAGE_SIZE) + 1), false);
	}

	/**
	 * 一覧情報取得用Dto（ページングサイズオーバーリスト）
	 */
	@isTest private static void ListViewDtoPagingSizeOverTest() {
		List<ComEmpBase__c> objList = new List<ComEmpBase__c>();

		for (Integer index = 0 ; index < ComConst.PAGING_PAGE_SIZE + 1 ; index++ ) {
			objList.add(new ComEmpBase__c());
		}

		Test.startTest();

		// ページング情報なし
		ComDto.ListViewDto noPagingDto = new ComDto.ListViewDto(objList);
		// ページング情報あり
		ComDto.ListViewDto pagingDto = generatePagingListViewDto(objList);

		Test.stopTest();

		assertListViewDto(noPagingDto, 1, objList.size(), objList.size(), 1, false);
		assertListViewDto(pagingDto, 1, ComConst.PAGING_PAGE_SIZE, objList.size(), ((Integer)(objList.size() / ComConst.PAGING_PAGE_SIZE) + 1), false);
	}

	/**
	 * 一覧情報取得用Dto（リミットサイズ）
	 */
	@isTest private static void ListViewDtoLimitSizeTest() {
		List<ComEmpBase__c> objList = new List<ComEmpBase__c>();
		for (Integer index = 0 ; index < ComConst.VIEWRECORD_LIMIT_COUNT ; index++ ) {
			objList.add(new ComEmpBase__c());
		}

		Test.startTest();

		// ページング情報なし
		ComDto.ListViewDto noPagingDto = new ComDto.ListViewDto(objList);
		// ページング情報あり
		ComDto.ListViewDto pagingDto = generatePagingListViewDto(objList);

		Test.stopTest();

		assertListViewDto(noPagingDto, 1, objList.size(), objList.size(), 1, false);
		assertListViewDto(pagingDto, 1, ComConst.PAGING_PAGE_SIZE, objList.size(), ((Integer)(objList.size() / ComConst.PAGING_PAGE_SIZE) + 1), false);
	}

	/**
	 * 一覧情報取得用Dto（リミットサイズオーバー）
	 */
	@isTest private static void ListViewDtoLimitSizeOverTest() {
		List<ComEmpBase__c> objList = new List<ComEmpBase__c>();
		for (Integer index = 0 ; index < ComConst.VIEWRECORD_LIMIT_COUNT + 1 ; index++ ) {
			objList.add(new ComEmpBase__c());
		}

		Test.startTest();

		// ページング情報なし
		ComDto.ListViewDto noPagingDto = new ComDto.ListViewDto(objList);
		// ページング情報あり
		ComDto.ListViewDto pagingDto = generatePagingListViewDto(objList);

		Test.stopTest();

		assertListViewDto(noPagingDto, 1, objList.size(), objList.size(), 1, false);
		assertListViewDto(pagingDto, 1, ComConst.PAGING_PAGE_SIZE, ComConst.VIEWRECORD_LIMIT_COUNT, ((Integer)(objList.size() / ComConst.PAGING_PAGE_SIZE) + 1), true);
	}

	/**
	 * PagingのListViewDtoを取得
	 * @param empList 格納リスト
	 * @return PagingのListViewDto
	 */
	private static ComDto.ListViewDto generatePagingListViewDto(List<ComEmpBase__c> empList) {
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(empList);
		ssc.setPageNumber(1);
		ssc.setPageSize(ComConst.PAGING_PAGE_SIZE);
		return new ComDto.ListViewDto(ssc.getRecords(), ssc);
	}

	/**
	 * 一覧情報取得用DtoのAssertセット
	 * @param チェックDto
	 * @param pageNo
	 * @param pageSize
	 * @param totalSize
	 * @param maxPageSize
	 * @param exceedsLimit
	 */
	private static void assertListViewDto(ComDto.ListViewDto checkDto, Integer pageNo, Integer pageSize, Integer totalSize, Integer maxPageNo, Boolean exceedsLimit) {
		System.assertEquals(checkDto.pageNo, pageNo);
		System.assertEquals(checkDto.pageSize, pageSize);
		System.assertEquals(checkDto.totalSize, totalSize);
		System.assertEquals(checkDto.maxPageNo, maxPageNo);
		System.assertEquals(checkDto.exceedsLimit, exceedsLimit);
	}

}