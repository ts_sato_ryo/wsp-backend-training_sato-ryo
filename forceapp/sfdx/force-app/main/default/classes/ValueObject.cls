/**
 * ValueObject基底クラス
 */
public abstract with sharing class ValueObject {

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public abstract Boolean equals(Object compare);

}