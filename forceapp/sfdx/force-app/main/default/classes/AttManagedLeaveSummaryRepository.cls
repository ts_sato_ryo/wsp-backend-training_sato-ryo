/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇消サマリのリポジトリ
 */
public with sharing class AttManagedLeaveSummaryRepository extends Repository.BaseRepository {
	/** 有休サマリ取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_SUMMARY_FIELD_SET;
	static {
		GET_SUMMARY_FIELD_SET = new Set<Schema.SObjectField> {
				AttManagedLeaveSummary__c.Id,
				AttManagedLeaveSummary__c.AttSummaryId__c,
				AttManagedLeaveSummary__c.LeaveId__c,
				AttManagedLeaveSummary__c.DaysTaken__c,
				AttManagedLeaveSummary__c.DaysLeft__c,
				AttManagedLeaveSummary__c.DaysExpired__c,
				AttManagedLeaveSummary__c.DaysAdjusted__c,
				AttManagedLeaveSummary__c.LastModifiedDate
		};
	}
	/** リレーション名（休暇サマリの休暇Id) */
	private static final String ATT_LEAVE_R =
			AttManagedLeaveSummary__c.LeaveId__c.getDescribe().getRelationshipName();
	/** リレーション名（休暇サマリの勤怠サマリId) */
	private static final String ATT_SUMMARY_R =
			AttManagedLeaveSummary__c.AttSummaryId__c.getDescribe().getRelationshipName();
	/** リレーション名（勤怠サマリの社員履歴Id) */
	private static final String ATT_EMP_HIST_R =
			AttSummary__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	/** リレーション名（休暇付与サマリ) */
	private static final String GRANT_SUMMARY_R = 'LeaveGrantSummaries__r';
	/** 検索フィルタ (検索API実行用) */
	public class SearchFilter {
		public Id empId;
		public Set<Id> empIds;
		public Set<Id> attSummaryIds;
		public Set<Id> leaveIds;
		public AppDate dateFrom;
		public AppDate dateTo;
		public Boolean onlyUnAdmit;
	}
	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AttManagedLeaveSummaryEntity entity) {
		return saveEntityList(new List<AttManagedLeaveSummaryEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveSummaryEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveSummaryEntity> entityList, Boolean allOrNone) {

		List<AttManagedLeaveSummary__c> takeList = new List<AttManagedLeaveSummary__c>();

		for (AttManagedLeaveSummaryEntity entity : entityList) {
			// エンティティからSObjectを作成する
			takeList.add(createObj(entity));
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(takeList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}
	public List<AttManagedLeaveSummaryEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {
		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_SUMMARY_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// 休暇種別
		selectFldList.add(ATT_LEAVE_R + '.' + getFieldName(AttLeave__c.Type__c));
		selectFldList.add(ATT_LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L0__c));
		selectFldList.add(ATT_LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L1__c));
		selectFldList.add(ATT_LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L2__c));
		selectFldList.add(ATT_SUMMARY_R + '.' + getFieldName(AttSummary__c.StartDate__c));
		selectFldList.add(ATT_SUMMARY_R + '.' + getFieldName(AttSummary__c.EndDate__c));
		selectFldList.add(Att_SUMMARY_R + '.' + ATT_EMP_HIST_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c));
		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id empId = filter.empId;
		final Set<Id> empIds = filter.empIds;
		final Set<Id> attSummaryIds = filter.attSummaryIds;
		final Set<Id> leaveIds = filter.leaveIds;
		final Date dateFrom = AppConverter.dateValue(filter.dateFrom);
		final Date dateTo = AppConverter.dateValue(filter.dateTo);

		if (!String.isBlank(empId)) {
			condList.add(Att_SUMMARY_R + '.' + ATT_EMP_HIST_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c) + ' = :empId');
		}
		if (empIds != null) {
			condList.add(Att_SUMMARY_R + '.' + ATT_EMP_HIST_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c) + ' IN :empIds');
		}
		if (attSummaryIds != null) {
			condList.add(getFieldName(AttManagedLeaveSummary__c.AttSummaryId__c) + ' IN :attSummaryIds');
		}
		if (leaveIds != null) {
			condList.add(getFieldName(AttManagedLeaveSummary__c.LeaveId__c) + ' IN :leaveIds');
		}
		if (dateFrom != null) {
			condList.add(ATT_SUMMARY_R + '.' + getFieldName(AttSummary__c.EndDate__c) + ' >= :dateFrom');
		}
		if (dateTo != null) {
			condList.add(ATT_SUMMARY_R + '.' + getFieldName(AttSummary__c.StartDate__c) + ' <= :dateTo');
		}
		if (filter.onlyUnAdmit == true) {
			condList.add(Att_SUMMARY_R + '.' + getFieldName(AttSummary__c.Locked__c) + ' = false');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ') +
					+', (SELECT LeaveGrantId__r.DaysGranted__c FROM ' + GRANT_SUMMARY_R + ') '
				+ ' FROM ' + AttManagedLeaveSummary__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (!forUpdate) {
			soql += ' ORDER BY ' + ATT_SUMMARY_R + '.' + getFieldName(AttSummary__c.StartDate__c);
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// 結果からエンティティを作成する
		List<AttManagedLeaveSummaryEntity> entityList = new List<AttManagedLeaveSummaryEntity>();
		AppLimits.getInstance().setCurrentQueryCount();
		for (AttManagedLeaveSummary__c grant : Database.query(soql)) {
			entityList.add(createEntity(grant));
		}
		AppLimits.getInstance().incrementQueryCount(AttManagedLeaveSummary__c.SObjectType);

		return entityList;
	}
	public AttManagedLeaveSummaryEntity createEntity(AttManagedLeaveSummary__c summary) {
		AttManagedLeaveSummaryEntity retEntity = new AttManagedLeaveSummaryEntity();
		retEntity.setId(summary.Id);

		retEntity.attSummaryId = summary.AttSummaryId__c;
		retEntity.employeeId = summary.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c;
		retEntity.leaveId = summary.LeaveId__c;
		retEntity.leaveType = AttLeaveType.valueOf(summary.LeaveId__r.Type__c);
		retEntity.leaveNameL = new AppMultiString(
			summary.LeaveId__r.Name_L0__c,
			summary.LeaveId__r.Name_L1__c,
			summary.LeaveId__r.Name_L2__c);
		retEntity.daysTaken = AttDays.valueOf(summary.DaysTaken__c);
		retEntity.daysLeft = AttDays.valueOf(summary.DaysLeft__c);
		retEntity.daysExpired = AttDays.valueOf(summary.DaysExpired__c);
		retEntity.daysAdjusted = AttDays.valueOf(summary.DaysAdjusted__c);
		retEntity.startDate = AppDate.valueOf(summary.AttSummaryId__r.StartDate__c);
		retEntity.endDate = AppDate.valueOf(summary.AttSummaryId__r.EndDate__c);
		Decimal daysGranted = 0;
		for (AttManagedLeaveGrantSummary__c grantSummary : summary.LeaveGrantSummaries__r) {
			daysGranted += grantSummary.LeaveGrantId__r.DaysGranted__c;
		}
		retEntity.leaveType = AttLeaveType.valueOf(summary.LeaveId__r.Type__c);
		retEntity.daysGranted = AttDays.valueOf(daysGranted);
		retEntity.lastModifiedDate = AppDateTime.valueOf(summary.LastModifiedDate);
		retEntity.resetChanged();
		return retEntity;
	}
	private AttManagedLeaveSummary__c createObj(AttManagedLeaveSummaryEntity entity) {
		Boolean isInsert = String.isBlank(entity.id);
		AttManagedLeaveSummary__c retObject = new AttManagedLeaveSummary__c();
		retObject.Id = entity.Id;

		if (isInsert && entity.isChanged(AttManagedLeaveSummaryEntity.Field.ATT_SUMMARY_ID)) {
			retObject.AttSummaryId__c = entity.attSummaryId;
		}
		if (isInsert && entity.isChanged(AttManagedLeaveSummaryEntity.Field.LEAVE_ID)) {
			retObject.LeaveId__c = entity.leaveId;
		}
		if (entity.isChanged(AttManagedLeaveSummaryEntity.Field.DAYS_TAKEN)) {
			retObject.DaysTaken__c = AppConverter.decValue(entity.daysTaken);
		}
		if (entity.isChanged(AttManagedLeaveSummaryEntity.Field.DAYS_LEFT)) {
			retObject.DaysLeft__c = AppConverter.decValue(entity.daysLeft);
		}
		if (entity.isChanged(AttManagedLeaveSummaryEntity.Field.DAYS_EXPIRED)) {
			retObject.DaysExpired__c = AppConverter.decValue(entity.daysExpired);
		}
		if (entity.isChanged(AttManagedLeaveSummaryEntity.Field.DAYS_ADJUSTED)) {
			retObject.DaysAdjusted__c = AppConverter.decValue(entity.daysAdjusted);
		}
		return retObject;
	}
}