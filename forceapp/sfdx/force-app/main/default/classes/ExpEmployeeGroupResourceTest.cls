/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Expense Employee Group Resource Test
 */
@IsTest
private class ExpEmployeeGroupResourceTest {
	static final ComMsgBase MESSAGE = ComMessage.msg();
	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);
	static final ComEmpBase__c employee = ComTestDataUtility.createEmployeeWithHistory(UserInfo.getLastName(), companyObj.Id, null, UserInfo.getUserId());

	/*
	 * Test Data Class
	 */
	private class TestData  {
		/** Expense Employee Group Data */
		public List<ExpEmployeeGroupEntity> expEmployeeGroupList;

		/** Constructor */
		public TestData() {
			expEmployeeGroupList = createExpEmployeeGroupList('Testcode', companyObj.Id, 3);
		}

		/**
		 * Create and Insert Employee Group List
		 * @param code Test Data Prefix Code
		 * @param companyId Company ID
		 * @param size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpEmployeeGroupEntity> createExpEmployeeGroupList(String code, Id companyId, Integer size) {
			expEmployeeGroupList = new List<ExpEmployeeGroupEntity>();
			ExpEmployeeGroupRepository repo = new ExpEmployeeGroupRepository();
			ExpEmpGroupExpReportTypeLinkRepository linkRepo = new ExpEmpGroupExpReportTypeLinkRepository();

			List<ExpEmployeeGroup__c> expEmpGroupList =
					ComTestDataUtility.createExpEmployeeGroupList(code, companyId, size);
			List<Id> expEmpGroupIdList = new List<Id>();
			for (ExpEmployeeGroup__c expEmpGroup : expEmpGroupList){
				expEmpGroupIdList.add(expEmpGroup.Id);
			}
			List<Id> reportTypeIdList = createReportTypeIdList('test');
			List<ExpEmpGroupExpReportTypeLink__c> ExpEmpGroupExpReportTypeLinkList =
					ComTestDataUtility.createExpEmployeeGroupReportTypeLinkList(expEmpGroupIdList, reportTypeIdList);

			List<ExpEmpGroupExpReportTypeLinkEntity> entityList = new List<ExpEmpGroupExpReportTypeLinkEntity>();
			for (ExpEmpGroupExpReportTypeLink__c expEmpGroupSObj : ExpEmpGroupExpReportTypeLinkList) {
				entityList.add(linkRepo.createEntity(expEmpGroupSObj));
			}
			Map<Id, List<Id>> linkMap = linkRepo.createMap(entityList);

			for (ExpEmployeeGroup__c expEmpGroupSObj : expEmpGroupList) {
				ExpEmployeeGroupEntity entity = repo.createEntity(expEmpGroupSObj);
				entity.reportTypeIdList = linkMap.get(entity.id);
				expEmployeeGroupList.add(entity);
			}
			return expEmployeeGroupList;
		}
	}


	/**
	 * Create and Insert report type list
	 * @param name Name of report type
	 * @return List of report type ids created
	 */
	private static List<Id> createReportTypeIdList(String name) {
		List<ExpReportType__c> expReportTypeList =
				ComTestDataUtility.createExpReportTypes(name, companyObj.Id, 3);
		List<Id> expReportTypeIdList = new List<Id>();
		for (ExpReportType__c expReportType : expReportTypeList){
			expReportTypeIdList.add(expReportType.Id);
		}
		return expReportTypeIdList;
	}

	/**
	 * Create one Expense Employee Group Positive Test
	 */
	@isTest
	static void createApiTest() {
		List<Id> reportTypeIdList = createReportTypeIdList('test');
		ExpEmployeeGroupResource.UpsertParam param = new ExpEmployeeGroupResource.UpsertParam();
		setTestParam(param, reportTypeIdList);

		Test.startTest();

		ExpEmployeeGroupResource.createApi api = new ExpEmployeeGroupResource.createApi();
		ExpEmployeeGroupResource.SaveResult result = (ExpEmployeeGroupResource.SaveResult)api.execute(param);

		Test.stopTest();

		List<ExpEmployeeGroup__c> records = getExpenseEmployeeGroupSObjectFROMSOQL(result.id);
		List<ExpEmpGroupExpReportTypeLink__c> linkObjList = getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(result.id);

		//Confirm new record was created
		System.assertEquals(1, records.size());
		System.assertEquals(3, linkObjList.size());
		assertFields(param, records[0], linkObjList);
	}

	/**
	 * Create Duplicate Expense Employee Group by using the same param
	 */
	@isTest
	static void creatDuplicateExpenseEmployeeGroupTest() {
		List<Id> reportTypeIdList = createReportTypeIdList('test');
		ExpEmployeeGroupResource.UpsertParam param = new ExpEmployeeGroupResource.UpsertParam();
		setTestParam(param, reportTypeIdList);

		Test.startTest();

		ExpEmployeeGroupResource.createApi api = new ExpEmployeeGroupResource.createApi();
		ExpEmployeeGroupResource.SaveResult result = (ExpEmployeeGroupResource.SaveResult)api.execute(param);
		try{
			result = (ExpEmployeeGroupResource.SaveResult)api.execute(param);
			TestUtil.fail('No exception thrown for invalid update.');
		} catch (Exception e) {
			System.assertEquals(MESSAGE.Com_Err_DuplicateCode, e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Create Expense Employee Group with Invalid Parameter: Negative Test
	 */
	@isTest
	static void createApiParamNegativeTest(){
		List<Id> reportTypeIdList = createReportTypeIdList('test');
		ExpEmployeeGroupResource.UpsertParam param = new ExpEmployeeGroupResource.UpsertParam();
		setTestParam(param, reportTypeIdList);

		ExpEmployeeGroupResource.createApi api = new ExpEmployeeGroupResource.createApi();
		App.ParameterException ex1;
		App.ParameterException ex2;
		App.ParameterException ex3;
		App.ParameterException ex4;
		App.ParameterException ex5;
		App.ParameterException ex6;
		App.ParameterException ex7;
		App.ParameterException ex8;

		Test.startTest();

		//Code blank
		param.code = '';
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex1 = e;
		}

		//Code Length with 21 characters
		param.code = ''.leftPad(21, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex2 = e;
		}

		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L0 = '';
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex3 = e;
		}

		//Name Length with 81 characters
		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L0 = ''.leftPad(81, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex4 = e;
		}

		//Name Length with 81 characters
		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L1 = ''.leftPad(81, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex5 = e;
		}

		//Name Length with 81 characters
		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L2 = ''.leftPad(81, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex6 = e;
		}

		//Description Length with 1025 character
		setTestParam(param, reportTypeIdList); //Set back Param
		param.description_L0 = ''.leftPad(1025, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex7 = e;
		}

		// ReportTypeIdList is not valid Id
		List<String> wrongReportTypeIdList = new List<String>();
		try {
			wrongReportTypeIdList.add('wrongId');
			setTestParam(param, wrongReportTypeIdList); //Set back Param
			api.execute(param);
		} catch (App.ParameterException e) {
			ex8 = e;
		}

		Test.stopTest();

		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Com_Lbl_Code}), ex1.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Com_Lbl_Code, ExpEmployeeGroupEntity.CODE_MAX_LENGTH.format()}), ex2.getMessage());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_Name}), ex3.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Name, ExpEmployeeGroupEntity.NAME_MAX_LENGTH.format()}), ex4.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Name, ExpEmployeeGroupEntity.NAME_MAX_LENGTH.format()}), ex5.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Name, ExpEmployeeGroupEntity.NAME_MAX_LENGTH.format()}), ex6.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Description, ExpEmployeeGroupEntity.DESCRIPTION_MAX_LENGTH.format()}), ex7.getMessage());
		System.assertEquals(MESSAGE.Com_Err_InvalidParameter(new List<String>{'reportTypeIdList', 'wrongId'} ), ex8.getMessage());
	}

	/**
	 * Search Expense Employee Group with Expense Employee Group Id
	 */
	@isTest
	static void searchApiTest() {

		TestData testData = new TestData();
		ExpEmployeeGroupResource.SearchParam param = new ExpEmployeeGroupResource.SearchParam();
		param.id = testData.expEmployeeGroupList[0].id;

		Test.startTest();

		ExpEmployeeGroupResource.SearchApi api = new ExpEmployeeGroupResource.SearchApi();
		ExpEmployeeGroupResource.SearchResult res = (ExpEmployeeGroupResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());

		ExpEmployeeGroupResource.ExpEmployeeGroupRecord result = res.records[0];
		System.assertEquals(testData.expEmployeeGroupList[0].id, result.id);
	}

	/**
	 * Search Expense Employee Group with Company id
	 */
	@isTest
	static void searchApiWithCompanyIdTest() {

		List<ExpEmployeeGroup__c> testRecord = ComTestDataUtility.createExpEmployeeGroupList('Test', companyObj.Id, 3);

		ExpEmployeeGroupResource.SearchParam param = new ExpEmployeeGroupResource.SearchParam();
		param.companyId = companyObj.Id;

		Test.startTest();

		ExpEmployeeGroupResource.SearchApi api = new ExpEmployeeGroupResource.SearchApi();
		ExpEmployeeGroupResource.SearchResult res = (ExpEmployeeGroupResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(3, res.records.size());

		ExpEmployeeGroupResource.ExpEmployeeGroupRecord result = res.records[0];
		System.assertEquals(testRecord[0].Id, result.id);
	}

	/**
	 * Search Expense Employee Group record not found Test
	 */
	@isTest
	static void searchApiNoRecordFoundTest() {
		TestData testData = new TestData();
		ExpEmployeeGroupResource.SearchParam param = new ExpEmployeeGroupResource.SearchParam();
		param.id = testData.expEmployeeGroupList[1].id;

		delete getExpenseEmployeeGroupSObjectFROMSOQL(testData.expEmployeeGroupList[1].id);
		delete getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(testData.expEmployeeGroupList[1].id);

		Test.startTest();

		ExpEmployeeGroupResource.SearchApi api = new ExpEmployeeGroupResource.SearchApi();
		ExpEmployeeGroupResource.SearchResult res = (ExpEmployeeGroupResource.SearchResult)api.execute(param);

		Test.stopTest();

		System.assertEquals(0, res.records.size());

	}

	/**
	 *  Search Expense Employee Group and non active group are not shown in the result
	 */
	@isTest
	static void searchApiInactiveNotFoundTest() {
		List<ExpEmployeeGroup__c> testRecord = ComTestDataUtility.createExpEmployeeGroupList('Test', companyObj.Id, 3);
		testRecord[0].Active__c = False;
		update testRecord[0];

		ExpEmployeeGroupResource.SearchParam paramActive = new ExpEmployeeGroupResource.SearchParam();
		paramActive.active = true;
		ExpEmployeeGroupResource.SearchParam paramNotActive = new ExpEmployeeGroupResource.SearchParam();
		paramNotActive.active = false;

		Test.startTest();

		ExpEmployeeGroupResource.SearchApi api = new ExpEmployeeGroupResource.SearchApi();
		ExpEmployeeGroupResource.SearchResult resActive = (ExpEmployeeGroupResource.SearchResult)api.execute(paramActive);
		ExpEmployeeGroupResource.SearchResult resNotActive = (ExpEmployeeGroupResource.SearchResult)api.execute(paramNotActive);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, resActive.records);
		System.assertEquals(2, resActive.records.size());
		System.assertNotEquals(null, resNotActive.records);
		System.assertEquals(1, resNotActive.records.size());
	}

	/**
	 * Update Api Test Positive Test: 2 cases are covered
	 * Case 1 : All Expense Employee Group fields are updated
	 * Case 2 : Partial field update (In this case: name_L0)
	 */
	@isTest
	static void updateApiTest() {
		TestData testData = new TestData();
		List<Id> reportTypeIdList = createReportTypeIdList('test2');

		ExpEmployeeGroupResource.UpsertParam param = new ExpEmployeeGroupResource.UpsertParam();

		// Case 1 : All Expense Employee Group fields are updated
		setTestParam(param, reportTypeIdList);
		param.id = testData.expEmployeeGroupList[0].id;

		ExpEmployeeGroupResource.UpdateApi api = new ExpEmployeeGroupResource.UpdateApi();
		ExpEmployeeGroupResource.SaveResult result = (ExpEmployeeGroupResource.SaveResult)api.execute(param);

		List<ExpEmployeeGroup__c> records = getExpenseEmployeeGroupSObjectFROMSOQL(result.id);
		List<ExpEmpGroupExpReportTypeLink__c> linkObjList = getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(result.id);

		// Confirm Existing Record was Updated
		System.assertEquals(1, records.size());
		assertFields(param, records[0], linkObjList);
		System.assertEquals(companyObj.Code__c + '-' + param.code, records[0].UniqKey__c);

		// Case 2 : Partial field update (In this case: name_L0)
		param.name_L0 = 'Updated Name';
		ExpEmployeeGroupResource.SaveResult resultUpdateNameOnly = (ExpEmployeeGroupResource.SaveResult)api.execute(param);
		result = (ExpEmployeeGroupResource.SaveResult)api.execute(param);
		records = getExpenseEmployeeGroupSObjectFROMSOQL(resultUpdateNameOnly.id);

		System.assertEquals(1, records.size());
		ExpEmployeeGroupResourceTest.assertFields(param, records[0], linkObjList);
		System.assertEquals(companyObj.Code__c + '-' + param.code, records[0].UniqKey__c);

		// Case 3 : Partial field update (In this case: reportTypeIdList)
		List<Id> reportTypeIdListForPartialTest = createReportTypeIdList('test3');
		param.reportTypeIdList = reportTypeIdListForPartialTest;
		ExpEmployeeGroupResource.SaveResult resultUpdateReportTypeIdListOnly = (ExpEmployeeGroupResource.SaveResult)api.execute(param);
		result = (ExpEmployeeGroupResource.SaveResult)api.execute(param);
		records = getExpenseEmployeeGroupSObjectFROMSOQL(resultUpdateReportTypeIdListOnly.id);
		linkObjList = getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(resultUpdateReportTypeIdListOnly.id);

		System.assertEquals(1, records.size());
		ExpEmployeeGroupResourceTest.assertFields(param, records[0], linkObjList);
		System.assertEquals(companyObj.Code__c + '-' + param.code, records[0].UniqKey__c);
	}

	/*
	 * Update Expense Employee Group with Invalid Parameter: Negative Test
	 */
	@isTest
	static void updateApiParamNegativeTest() {
		List<Id> reportTypeIdList = createReportTypeIdList('test2');

		ExpEmployeeGroupResource.UpsertParam param = new ExpEmployeeGroupResource.UpsertParam();
		setTestParam(param, reportTypeIdList);
		ExpEmployeeGroupResource.UpdateApi api = new ExpEmployeeGroupResource.UpdateApi();
		App.ParameterException ex1;
		App.ParameterException ex2;
		App.ParameterException ex3;
		App.ParameterException ex4;
		App.ParameterException ex5;
		App.ParameterException ex6;
		App.ParameterException ex7;
		App.ParameterException ex8;

		Test.startTest();

		//Code blank
		param.code = '';
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex1 = e;
		}

		//Code Length with 21 characters
		param.code = ''.leftPad(21, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex2 = e;
		}

		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L0 = '';
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex3 = e;
		}

		//Name Length with 81 characters
		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L0 = ''.leftPad(81, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex4 = e;
		}

		//Name Length with 81 characters
		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L1 = ''.leftPad(81, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex5 = e;
		}

		//Name Length with 81 characters
		setTestParam(param, reportTypeIdList); //Set back Param
		param.name_L2 = ''.leftPad(81, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex6 = e;
		}

		//Description Length with 1025 character
		setTestParam(param, reportTypeIdList); //Set back Param
		param.description_L0 = ''.leftPad(1025, '123456789-');
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex7 = e;
		}

		// ReportTypeIdList is not valid Id
		List<String> wrongReportTypeIdList = new List<String>();
		try {
			wrongReportTypeIdList.add('wrongId');
			setTestParam(param, wrongReportTypeIdList); //Set back Param
			api.execute(param);
		} catch (App.ParameterException e) {
			ex8 = e;
		}

		Test.stopTest();

		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_Code}), ex1.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Code, ExpEmployeeGroupEntity.CODE_MAX_LENGTH.format()}), ex2.getMessage());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_Name}), ex3.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Name, ExpEmployeeGroupEntity.NAME_MAX_LENGTH.format()}), ex4.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Name, ExpEmployeeGroupEntity.NAME_MAX_LENGTH.format()}), ex5.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Name, ExpEmployeeGroupEntity.NAME_MAX_LENGTH.format()}), ex6.getMessage());
		System.assertEquals(MESSAGE.Com_Err_MaxLengthOver(new List<String>{MESSAGE.Admin_Lbl_Description, ExpEmployeeGroupEntity.DESCRIPTION_MAX_LENGTH.format()}), ex7.getMessage());
		System.assertEquals(MESSAGE.Com_Err_InvalidParameter(new List<String>{'reportTypeIdList', 'wrongId'} ), ex8.getMessage());
	}

	/**
	 * Delete Expense Employee Group Positive Test
	 */
	@isTest
	static void deleteApiTest() {
		TestData testData = new TestData();

		ExpEmployeeGroupResource.DeleteParam param = new ExpEmployeeGroupResource.DeleteParam();
		param.id = testData.expEmployeeGroupList[0].id;

		Test.startTest();

		ExpEmployeeGroupResource.DeleteApi api = new ExpEmployeeGroupResource.DeleteApi();
		api.execute(param);

		Test.stopTest();

		System.assertEquals(0, [SELECT COUNT() FROM ExpEmployeeGroup__c WHERE Id =: param.id]);
		System.assertEquals(0, [SELECT COUNT() FROM ExpEmpGroupExpReportTypeLink__c WHERE ExpEmpGroupId__c =: param.id]);
	}

	/**
	 * Delete Expense Employee Group with Invalid Parameter : Negative Test
	 */
	@isTest
	static void deleteApiParamNegativeTest() {
		TestData testData = new TestData();

		ExpEmployeeGroupResource.DeleteParam param = new ExpEmployeeGroupResource.DeleteParam();
		param.id = '';

		Test.startTest();

		try {
			ExpEmployeeGroupResource.DeleteApi api = new ExpEmployeeGroupResource.DeleteApi();
			api.execute(param);
			TestUtil.fail('No exception thrown for invalid param delete.');
		} catch (App.ParameterException e){
			System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'id'}), e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Delete Expense Employee Group Roll Back Test
	 */
	@isTest
	static void deleteApiRollBackTest(){
		TestData testData = new TestData();

		ExpEmployeeGroupResource.DeleteParam param = new ExpEmployeeGroupResource.DeleteParam();
		param.id = testData.expEmployeeGroupList[0].id;

		ExpEmployeeGroupResource.isRollbackTest = true;

		Test.startTest();

		try {
			ExpEmployeeGroupResource.DeleteApi api = new ExpEmployeeGroupResource.DeleteApi();
			api.execute(param);
			TestUtil.fail('No exception is thrown for delete rollback.');
		} catch (Exception e) {
			System.assertEquals('Expense Employee Group roll back test exception!', e.getMessage());
		}

		Test.stopTest();

		List<ExpEmployeeGroup__c> records = getExpenseEmployeeGroupSObjectFROMSOQL(testData.expEmployeeGroupList[0].id);

		// Confirm record is not deleted
		System.assertNotEquals(null, records);
		System.assertEquals(1, records.size());
	}

	/**
	 * SOQL statement to get ExpEmployeeGroup__c list
	 * @param id Expense Employee Group Id
	 * @return List of Expense Employee Group
	 */
	private static List<ExpEmployeeGroup__c> getExpenseEmployeeGroupSObjectFROMSOQL(Id id) {
		return [SELECT
			Id,
			Active__c,
			Code__c,
			CompanyId__c,
			Description_L0__c,
			Description_L1__c,
			Description_L2__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			UniqKey__c

		FROM ExpEmployeeGroup__c
		WHERE Id =: id];
	}

	/**
	 * SOQL statement to get ExpEmpGroupExpReportTypeLink__c list
	 * @param id Expense Employee Group Id
	 * @return List of ExpEmpGroupExpReportTypeLink object
	 */
	private static List<ExpEmpGroupExpReportTypeLink__c> getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(Id empGroupId) {
		return [SELECT
		Id,
		ExpEmpGroupId__c,
		ExpReportTypeId__c,
		Order__c
		FROM ExpEmpGroupExpReportTypeLink__c
		WHERE ExpEmpGroupId__c =: empGroupId
		ORDER BY Order__c
		];
	}

	/**
	 * SOQL Set test parameter
	 * @param param Parameter variable to be set
	 * @param reportTypeIdList Report Type Id List to set
	 * @return none
	 */
	private static void setTestParam(ExpEmployeeGroupResource.UpsertParam param, List<String> reportTypeIdList) {
		param.active = true;
		param.code = 'code';
		param.companyId = companyObj.id;
		param.description_L0 = 'DescriptionL0';
		param.description_L1 = 'DescriptionL1';
		param.description_L2 = 'DescriptionL2';
		param.name_L0 = 'nameL0';
		param.name_L1 = 'nameL1';
		param.name_L2 = 'nameL2';
		param.reportTypeIdList = reportTypeIdList;
	}

	/**
	 * Assert expected parameter with test sObject field
	 * @param param Parameter to be compared
	 * @param sObjTest Expense Employee Group S Object
	 * @param linkObjList Expense Employee Group Report Type S Object
	 */
	private static void assertFields(ExpEmployeeGroupResource.UpsertParam param, ExpEmployeeGroup__c sObjTest, List<ExpEmpGroupExpReportTypeLink__c> linkObjList) {
		System.assertEquals(param.active, sObjTest.Active__c);
		System.assertEquals(param.code, sObjTest.Code__c);
		System.assertEquals(param.companyId, sObjTest.CompanyId__c);
		System.assertEquals(param.description_L0, sObjTest.Description_L0__c);
		System.assertEquals(param.description_L1, sObjTest.Description_L1__c);
		System.assertEquals(param.description_L2, sObjTest.Description_L2__c);
		System.assertEquals(param.name_L0, sObjTest.Name_L0__c);
		System.assertEquals(param.name_L1, sObjTest.Name_L1__c);
		System.assertEquals(param.name_L2, sObjTest.Name_L2__c);
		for(Integer i=0; i<param.reportTypeIdList.size(); i++){
			System.assertEquals(param.reportTypeIdList[i], linkObjList[i].ExpReportTypeId__c);
		}
	}

}