/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 会社のリポジトリ
 */
public with sharing class CompanyRepository extends Repository.BaseRepository {

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無（デフォルトはoff） */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャッシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** キャッシュ */
	private static final Repository.Cache CACHE = new Repository.Cache();

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				ComCompany__c.Id,
				ComCompany__c.Name,
				ComCompany__c.Code__c,
				ComCompany__c.Name_L0__c,
				ComCompany__c.Name_L1__c,
				ComCompany__c.Name_L2__c,
				ComCompany__c.CountryId__c,
				ComCompany__c.CurrencyId__c,
				ComCompany__c.Language__c,
				ComCompany__c.UseAttendance__c,
				ComCompany__c.UseExpense__c,
				ComCompany__c.UseExpenseRequest__c,
				ComCompany__c.UseWorkTime__c,
				ComCompany__c.UsePlanner__c,
				ComCompany__c.CalendarId__c,
				ComCompany__c.PlannerDefaultView__c,
				ComCompany__c.UseCalendarAccess__c,
				ComCompany__c.CalendarAccessService__c,
				ComCompany__c.UseCompanyTaxMaster__c,
				ComCompany__c.JorudanFareType__c,
				ComCompany__c.JorudanAreaPreference__c,
				ComCompany__c.JorudanUseChargedExpress__c,
				ComCompany__c.JorudanChargedExpressDistance__c,
				ComCompany__c.JorudanSeatPreference__c,
				ComCompany__c.JorudanRouteSort__c,
				ComCompany__c.JorudanCommuterPass__c,
				ComCompany__c.JorudanHighwayBus__c,
				ComCompany__c.RequireLocationAtMobileStamp__c,
				ComCompany__c.AllowTaxAmountChange__c
		};
	}

	/** 国のリレーション名 */
	private static final String COUNTRY_R = ComCompany__c.CountryId__c.getDescribe().getRelationshipName();
	/**
	 * 取得対象の国オブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_COUNTRY_FIELD_NAME_LIST;
	static {

		// 取得対象の項目定義(国)
		final Set<Schema.SObjectField> countryFieldList = new Set<Schema.SObjectField> {
			ComCountry__c.Name_L0__c,
			ComCountry__c.Name_L1__c,
			ComCountry__c.Name_L2__c};

		GET_COUNTRY_FIELD_NAME_LIST = Repository.generateFieldNameList(COUNTRY_R, countryFieldList);
	}

	/** Relationship name of Currency */
	private static final String CURRENCY_R = ComCompany__c.CurrencyId__c.getDescribe().getRelationshipName();
	/**
	 * Name fields of the Currency object
	 */
	private static final List<String> GET_CURRENCY_FIELD_NAME_LIST;
	static {

		// Definition of the target name fields
		final Set<Schema.SObjectField> currencyFieldList = new Set<Schema.SObjectField> {
			ComCurrency__c.Name_L0__c,
			ComCurrency__c.Name_L1__c,
			ComCurrency__c.Name_L2__c,
			ComCurrency__c.Code__c,
			ComCurrency__c.Symbol__c,
			ComCurrency__c.DecimalPlaces__c
			};

		GET_CURRENCY_FIELD_NAME_LIST = Repository.generateFieldNameList(CURRENCY_R, currencyFieldList);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** 会社ID */
		public List<Id> companyIds;
		/** 会社コード */
		public List<String> companyCodes;
		/** カレンダーID */
		public List<Id> calendarIds;
	}

	/** FOR UPDATE でクエリしない */
	private static final Boolean NOT_FOR_UPDATE = false;

	/**
	 * コンストラクタ
	 * キャッシュを利用しないインスタンスを生成する
	 */
	public CompanyRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public CompanyRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/**
	 * 指定したIDの会社を取得する
	 * @param companyId 取得対象の会社ID
	 * @return 会社、ただし見つからなかった場合はnull
	 */
	public CompanyEntity getEntity(Id companyId) {
		List<CompanyEntity> entities = getEntityList(new List<Id>{companyId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したIDの会社を取得する
	 * @param companyIds 取得対象の会社IDリスト、nullの場合は全件取得
	 * @return 会社リスト
	 */
	public List<CompanyEntity> getEntityList(List<Id> companyIds) {
		CompanyRepository.SearchFilter filter = new SearchFilter();
		filter.companyIds = companyIds;

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定した会社コードの会社を取得する
	 * @param companyCodes 取得対象の会社コードリスト
	 * @return 会社リスト
	 */
	public List<CompanyEntity> getEntityList(List<String> companyCodes) {
		CompanyRepository.SearchFilter filter = new SearchFilter();
		filter.companyCodes = companyCodes;

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果
	 */
	public List<CompanyEntity> searchEntityList(CompanyRepository.SearchFilter filter) {
		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果
	 */
	@TestVisible
	private List<CompanyEntity> searchEntityList(
			CompanyRepository.SearchFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchEntityList ReturnCache key:' + cacheKey);
			List<CompanyEntity> entities = new List<CompanyEntity>();
			for (ComCompany__c sObj : (List<ComCompany__c>)CACHE.get(cacheKey)) {
				entities.add(createEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchEntityList NoCache key:' + cacheKey);

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// 国の項目
		selectFldList.addAll(GET_COUNTRY_FIELD_NAME_LIST);
		// Add Currency fields
		selectFldList.addAll(GET_CURRENCY_FIELD_NAME_LIST);

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> companyIds = filter.companyIds;
		final List<String> companyCodes = filter.companyCodes;
		final List<String> calendarIds = filter.calendarIds;
		if (companyIds != null) {
			condList.add(getFieldName(ComCompany__c.Id) + ' IN :companyIds');
		}
		if (companyCodes != null) {
			condList.add(getFieldName(ComCompany__c.Code__c) + ' IN :companyCodes');
		}
		if (calendarIds != null) {
			condList.add(getFieldName(ComCompany__c.CalendarId__c) + ' IN :calendarIds');
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComCompany__c.SObjectType.getDescribe().getName()
				+ buildWhereString(condList);
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(ComCompany__c.Code__c) + ' Asc';
		}
		// クエリ実行
		System.debug('CompanyRepository.searchEntityList: SOQL ==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComCompany__c> sObjList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComCompany__c.SObjectType);

		// 結果からエンティティを作成する
		List<CompanyEntity> entityList = new List<CompanyEntity>();
		for (ComCompany__c sObj : sObjList) {
			entityList.add(createEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			CACHE.put(cacheKey, sObjList);
		}
		return entityList;
	}

	/**
	 * エンティティをレコードとして保存する
	 * @param entity 作成元のエンティティ
	 * @return レコード保存結果
	 */
	public SaveResult saveEntity(CompanyEntity entity) {
		return saveEntityList(new List<CompanyEntity>{ entity }, true);
	}

	/**
	 * エンティティをレコードとして保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return レコード保存結果
	 */
	public SaveResult saveEntityList(List<CompanyEntity> entityList, Boolean allOrNone) {

		List<ComCompany__c> sObjList = createSobjects(entityList);

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sObjList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * ComCompany__cからCompanyEntityを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private CompanyEntity createEntity(ComCompany__c sObj) {
		CompanyEntity entity = new CompanyEntity();
		entity.setId(sObj.Id);
		entity.name = sObj.Name;
		entity.code = sObj.Code__c;
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.countryId = sObj.CountryId__c;
		entity.currencyId = sObj.CurrencyId__c;
		entity.language = AppLanguage.valueof(sObj.Language__c);
		entity.useAttendance = sObj.UseAttendance__c;
		entity.useExpense = sObj.UseExpense__c;
		entity.useExpenseRequest = sObj.UseExpenseRequest__c;
		entity.useWorkTime = sObj.UseWorkTime__c;
		entity.usePlanner = sObj.UsePlanner__c;
		entity.calendarId = sObj.CalendarId__c;
		entity.plannerDefaultView = PlannerViewType.valueOf(sObj.PlannerDefaultView__c);
		entity.useCalendarAccess = sObj.UseCalendarAccess__c;
		entity.calendarAccessService = CalendarAccessServiceName.valueOf(sObj.CalendarAccessService__c);
		entity.useCompanyTaxMaster = sObj.UseCompanyTaxMaster__c;
		entity.jorudanFareType = sObj.JorudanFareType__c;
		entity.jorudanAreaPreference = sObj.JorudanAreaPreference__c;
		entity.jorudanUseChargedExpress = sObj.JorudanUseChargedExpress__c;
		entity.jorudanChargedExpressDistance = sObj.JorudanChargedExpressDistance__c;
		entity.jorudanSeatPreference = sObj.JorudanSeatPreference__c;
		entity.jorudanRouteSort = sObj.JorudanRouteSort__c;
		entity.jorudanCommuterPass = sObj.JorudanCommuterPass__c;
		entity.jorudanHighwayBus = sObj.JorudanHighwayBus__c;
		entity.requireLocationAtMobileStamp = sObj.RequireLocationAtMobileStamp__c;
		entity.allowTaxAmountChange = sObj.AllowTaxAmountChange__c;

		if (sObj.CountryId__c != null) {
			entity.countryNameL = new AppMultiString(
					sObj.CountryId__r.Name_L0__c, sObj.CountryId__r.Name_L1__c, sObj.CountryId__r.Name_L2__c);
		}
		if (sObj.CurrencyId__c != null) {
			entity.currencyInfo = new CompanyEntity.CurrencyInfo();
			entity.currencyInfo.nameL = new AppMultiString(
				sObj.CurrencyId__r.Name_L0__c, sObj.CurrencyId__r.Name_L1__c, sObj.CurrencyId__r.Name_L2__c);
			entity.currencyInfo.code = sObj.CurrencyId__r.Code__c;
			entity.currencyInfo.symbol = sObj.CurrencyId__r.Symbol__c;
			entity.currencyInfo.decimalPlaces = (Integer)sObj.CurrencyId__r.DecimalPlaces__c;
		}

		entity.resetChanged();

		return entity;
	}

	/**
	 * CompanyEntityからComCompany__cを作成する
	 * @param entity 作成元のエンティティ
	 * @return 作成したSObject
	 */
	@TestVisible
	private ComCompany__c createSObject(CompanyEntity entity) {
		Boolean isInsert = String.isBlank(entity.Id);
		ComCompany__c sObj = new ComCompany__c(Id = entity.Id);

		if (entity.isChanged(CompanyEntity.Field.NAME)) {
			sObj.Name = entity.name;
		}
		if (entity.isChanged(CompanyEntity.Field.NAME_L0)) {
			sObj.Name_L0__c = entity.nameL.valueL0;
		}
		if (entity.isChanged(CompanyEntity.Field.NAME_L1)) {
			sObj.Name_L1__c = entity.nameL.valueL1;
		}
		if (entity.isChanged(CompanyEntity.Field.NAME_L2)) {
			sObj.Name_L2__c = entity.nameL.valueL2;
		}
		if (entity.isChanged(CompanyEntity.Field.CODE)) {
			sObj.Code__c = entity.code;
		}
		if (entity.isChanged(CompanyEntity.Field.COUNTRY_ID)) {
			sObj.CountryId__c = entity.countryId;
		}
		if (entity.isChanged(CompanyEntity.Field.CURRENCY_ID)) {
			sObj.CurrencyId__c = entity.currencyId;
		}
		if (entity.isChanged(CompanyEntity.Field.LANGUAGE)) {
			sObj.Language__c = entity.language.value;
		}
		if (entity.isChanged(CompanyEntity.Field.USE_ATTENDANCE)) {
			sObj.UseAttendance__c = entity.useAttendance;
		}
		if (entity.isChanged(CompanyEntity.Field.USE_EXPENSE)) {
			sObj.UseExpense__c = entity.useExpense;
		}
		if (entity.isChanged(CompanyEntity.Field.USE_EXPENSE_REQUEST)) {
			sObj.UseExpenseRequest__c = entity.useExpenseRequest;
		}
		if (entity.isChanged(CompanyEntity.Field.USE_WORKTIME)) {
			sObj.UseWorkTime__c = entity.useWorkTime;
		}
		if (entity.isChanged(CompanyEntity.Field.USE_PLANNER)) {
			sObj.UsePlanner__c = entity.usePlanner;
		}
		if (entity.isChanged(CompanyEntity.Field.CALENDAR_ID)) {
			sObj.CalendarId__c = entity.calendarId;
		}
		if (entity.isChanged(CompanyEntity.Field.PLANNER_DEFAULT_VIEW)) {
			sObj.PlannerDefaultView__c = entity.plannerDefaultView.value;
		}
		if (entity.isChanged(CompanyEntity.Field.USE_CALENDAR_ACCESS)) {
			sObj.UseCalendarAccess__c = entity.useCalendarAccess;
		}
		if (entity.isChanged(CompanyEntity.Field.CALENDAR_ACCESS_SERVICE)) {
			sObj.CalendarAccessService__c = AppConverter.stringValue(entity.calendarAccessService);
		}
		if (entity.isChanged(CompanyEntity.Field.USE_COMPANY_TAX_MASTER)) {
			sObj.UseCompanyTaxMaster__c = entity.useCompanyTaxMaster;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_FARE_TYPE)) {
			sObj.JorudanFareType__c = entity.jorudanFareType;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_AREA_PREFERENCE)) {
			sObj.JorudanAreaPreference__c = entity.jorudanAreaPreference;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_USE_CHARGED_EXPRESS)) {
			sObj.JorudanUseChargedExpress__c = entity.jorudanUseChargedExpress;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_CHARGED_EXPRESS_DISTANCE)) {
			sObj.JorudanChargedExpressDistance__c = entity.jorudanChargedExpressDistance;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_SEAT_PREFERENCE)) {
			sObj.JorudanSeatPreference__c = entity.jorudanSeatPreference;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_ROUTE_SORT)) {
			sObj.JorudanRouteSort__c = entity.jorudanRouteSort;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_COMMUTER_PASS)) {
			sObj.JorudanCommuterPass__c = entity.jorudanCommuterPass;
		}
		if (entity.isChanged(CompanyEntity.Field.JORUDAN_HIGHWAY_BUS)) {
			sObj.JorudanHighwayBus__c = entity.jorudanHighwayBus;
		}

		if (entity.isChanged(CompanyEntity.Field.REQUIRE_LOCATION_AT_MOBILE_STAMP)) {
			sObj.RequireLocationAtMobileStamp__c = entity.requireLocationAtMobileStamp;
		}
		if (entity.isChanged(CompanyEntity.Field.ALLOW_TAX_AMOUNT_CHANGE)) {
			sObj.AllowTaxAmountChange__c = entity.allowTaxAmountChange;
		}
		// 新規作成時、チェックボックス型項目がnullだとINVALID_TYPE_ON_FIELD_IN_RECORDエラーになるため
		// デフォルト値をセットする
		if (isInsert) {
			if (sObj.UseAttendance__c == null) {
				sObj.UseAttendance__c = (Boolean)ComCompany__c.UseAttendance__c.getDescribe().getDefaultValue();
			}
			if (sObj.UseExpense__c == null) {
				sObj.UseExpense__c = (Boolean)ComCompany__c.UseExpense__c.getDescribe().getDefaultValue();
			}
			if (sObj.UseExpenseRequest__c == null) {
				sObj.UseExpenseRequest__c = (Boolean) ComCompany__c.UseExpenseRequest__c.getDescribe().getDefaultValue();
			}
			if (sObj.UseWorkTime__c == null) {
				sObj.UseWorkTime__c = (Boolean)ComCompany__c.UseWorkTime__c.getDescribe().getDefaultValue();
			}
			if (sObj.UsePlanner__c == null) {
				sObj.UsePlanner__c = (Boolean)ComCompany__c.UsePlanner__c.getDescribe().getDefaultValue();
			}
			if (sObj.AllowTaxAmountChange__c == null) {
				sObj.AllowTaxAmountChange__c = (Boolean) ComCompany__c.AllowTaxAmountChange__c.getDescribe().getDefaultValue();
			}
		}

		return sObj;
	}

	/**
	 * CompanyEntityからComCompany__cを作成する
	 * @param entity 作成元のエンティティ
	 * @return 作成したSObject
	 */
	@TestVisible
	private List<ComCompany__c> createSObjects(List<CompanyEntity> entities) {
		List<ComCompany__c> retList = new List<ComCompany__c>();
		for (CompanyEntity entity:entities) {
			retList.add(createSobject(entity));
		}
		return retList;
	}

	/**
	 * エンティティに対応するレコードを削除する
	 * @param  entityId 削除対象ID
	 * @return 削除結果
	 */
	public override DeleteResult deleteEntity(Id entityId) {
		// IDがブランクの場合はエラー
		if (String.isBlank(entityId)) {
			throw new App.ParameterException('IDを指定してください');
		}
		return super.deleteEntity(entityId);
	}


}