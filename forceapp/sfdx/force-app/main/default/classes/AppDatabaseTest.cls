/**
 * AppDatabaseのテスト
 */
@isTest
private class AppDatabaseTest {

	/**
	 * 単一レコードの新規作成テスト
	 */
	@isTest private static void doInsertSingleRecordTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c baseCompany = ComTestDataUtility.createCompany('TestCompany', country.Id);
		delete baseCompany;
		ComCompany__c company = baseCompany.clone();

		Test.startTest();

		AppDatabase.doInsert(company);
		try {
			AppDatabase.doInsert(company);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		company = null;
		Database.SaveResult nullResult = AppDatabase.doInsert(company);

		Test.stopTest();

		System.assertEquals(null, nullResult);
	}

	/**
	 * 単一レコードの新規作成、エラー時続行テスト
	 */
	@isTest private static void doInsertSingleRecordNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c baseCompany = ComTestDataUtility.createCompany('TestCompany', country.Id);
		delete baseCompany;
		ComCompany__c company = baseCompany.clone();
		Database.SaveResult successResult;
		Database.SaveResult failedResult;
		Database.SaveResult nullResult;

		Test.startTest();

		successResult = AppDatabase.doInsert(company, false);
		failedResult = AppDatabase.doInsert(company, false);
		company = null;
		nullResult = AppDatabase.doInsert(company, false);

		Test.stopTest();

		System.assertEquals(true, successResult.isSuccess());
		System.assertEquals(false, failedResult.isSuccess());
		System.assertEquals(null, nullResult);
	}

	/**
	 * 複数レコードの新規作成テスト
	 */
	@isTest private static void doInsertRecordListTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> baseCompanyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		ComCompany__c baseCompany1 = baseCompanyList[0];
		ComCompany__c baseCompany2 = baseCompanyList[1];
		delete baseCompany1;
		delete baseCompany2;
		ComCompany__c company1 = baseCompany1.clone();
		ComCompany__c company2 = baseCompany2.clone();
		List<ComCompany__c> companyList = new List<ComCompany__c> {
			company1,
			company2
		};

		Test.startTest();

		AppDatabase.doInsert(companyList);
		try {
			AppDatabase.doInsert(companyList);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		companyList = null;
		List<Database.SaveResult> nullResultList = AppDatabase.doInsert(companyList);
		List<Database.SaveResult> emptyResultList = AppDatabase.doInsert(new List<ComCompany__c>());

		Test.stopTest();

		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 複数レコードの新規作成、エラー時続行テスト
	 */
	@isTest private static void doInsertRecordListNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> baseCompanyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		ComCompany__c baseCompany1 = baseCompanyList[0];
		ComCompany__c baseCompany2 = baseCompanyList[1];
		delete baseCompany1;
		delete baseCompany2;
		ComCompany__c company1 = baseCompany1.clone();
		ComCompany__c company2 = baseCompany2.clone();
		List<ComCompany__c> companyList = new List<ComCompany__c> {
			company1,
			company2
		};
		List<Database.SaveResult> successResultList;
		List<Database.SaveResult> failedResultList;
		List<Database.SaveResult> nullResultList;
		List<Database.SaveResult> emptyResultList;

		Test.startTest();

		successResultList = AppDatabase.doInsert(companyList, false);
		failedResultList = AppDatabase.doInsert(companyList, false);
		companyList = null;
		nullResultList = AppDatabase.doInsert(companyList, false);
		emptyResultList = AppDatabase.doInsert(new List<ComCompany__c>(), false);

		Test.stopTest();

		System.assertEquals(true, successResultList[0].isSuccess());
		System.assertEquals(false, failedResultList[0].isSuccess());
		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}


	/**
	 * 単一レコードの更新テスト
	 */
	@isTest private static void doUpdateSingleRecordTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);

		Test.startTest();

		AppDatabase.doUpdate(company);

		delete company;
		try {
			AppDatabase.doUpdate(company);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		company = null;
		Database.SaveResult nullResult = AppDatabase.doUpdate(company);

		Test.stopTest();

		System.assertEquals(null, nullResult);
	}

	/**
	 * 単一レコードの更新、エラー時続行テスト
	 */
	@isTest private static void doUpdateSingleRecordNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);
		Database.SaveResult successResult;
		Database.SaveResult failedResult;
		Database.SaveResult nullResult;

		Test.startTest();

		successResult = AppDatabase.doUpdate(company, false);
		delete company;
		failedResult = AppDatabase.doUpdate(company, false);
		company = null;
		nullResult = AppDatabase.doUpdate(company, false);

		Test.stopTest();

		System.assertEquals(true, successResult.isSuccess());
		System.assertEquals(false, failedResult.isSuccess());
		System.assertEquals(null, nullResult);
	}

	/**
	 * 複数レコードの更新テスト
	 */
	@isTest private static void doUpdateRecordListTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);

		Test.startTest();

		AppDatabase.doUpdate(companyList);

		delete companyList;

		try {
			AppDatabase.doUpdate(companyList);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		companyList = null;
		List<Database.SaveResult> nullResultList = AppDatabase.doUpdate(companyList);
		List<Database.SaveResult> emptyResultList = AppDatabase.doUpdate(new List<ComCompany__c>());

		Test.stopTest();

		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 複数レコードの更新、エラー時続行テスト
	 */
	@isTest private static void doUpdateRecordListNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		List<Database.SaveResult> successResultList;
		List<Database.SaveResult> failedResultList;
		List<Database.SaveResult> nullResultList;
		List<Database.SaveResult> emptyResultList;

		Test.startTest();

		successResultList = AppDatabase.doUpdate(companyList, false);
		delete companyList;
		failedResultList = AppDatabase.doUpdate(companyList, false);
		companyList = null;
		nullResultList = AppDatabase.doUpdate(companyList, false);
		emptyResultList = AppDatabase.doUpdate(new List<ComCompany__c>(), false);

		Test.stopTest();

		System.assertEquals(true, successResultList[0].isSuccess());
		System.assertEquals(false, failedResultList[0].isSuccess());
		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}


	/**
	 * 単一レコードの新規作成/更新テスト
	 */
	@isTest private static void doUpsertSingleRecordTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCountry', country.Id, 2);
		ComCompany__c baseCompany = companyList[0];
		delete baseCompany;
		ComCompany__c insertCompany = baseCompany.clone();
		ComCompany__c updateCompany = companyList[1];

		Test.startTest();

		AppDatabase.doUpsert(insertCompany);
		AppDatabase.doUpsert(updateCompany);

		insertCompany = baseCompany.clone();
		insertCompany.Code__c = null;
		delete updateCompany;

		try {
			AppDatabase.doUpsert(insertCompany);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		try {
			AppDatabase.doUpsert(updateCompany);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		insertCompany = null;
		Database.UpsertResult nullResult = AppDatabase.doUpsert(insertCompany);

		Test.stopTest();

		System.assertEquals(null, nullResult);
	}

	/**
	 * 単一レコードの新規作成/更新、エラー時続行テスト
	 */
	@isTest private static void doUpsertSingleRecordNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		ComCompany__c baseCompany = companyList[0];
		delete baseCompany;
		ComCompany__c insertCompany = baseCompany.clone();
		ComCompany__c updateCompany = companyList[1];
		Database.UpsertResult successInsertResult;
		Database.UpsertResult successUpdateResult;
		Database.UpsertResult failedInsertResult;
		Database.UpsertResult failedUpdateResult;
		Database.UpsertResult nullResult;

		Test.startTest();

		successInsertResult = AppDatabase.doUpsert(insertCompany, false);
		successUpdateResult = AppDatabase.doUpsert(updateCompany, false);
		insertCompany = baseCompany.clone();
		insertCompany.Code__c = null;
		delete updateCompany;
		failedInsertResult = AppDatabase.doUpsert(insertCompany, false);
		failedUpdateResult = AppDatabase.doUpsert(updateCompany, false);
		insertCompany = null;
		nullResult = AppDatabase.doUpsert(insertCompany, false);

		Test.stopTest();

		System.assertEquals(true, successInsertResult.isCreated());
		System.assertEquals(true, successInsertResult.isSuccess());
		System.assertEquals(false, successUpdateResult.isCreated());
		System.assertEquals(true, successUpdateResult.isSuccess());
		System.assertEquals(false, failedInsertResult.isSuccess());
		System.assertEquals(false, failedUpdateResult.isSuccess());
		System.assertEquals(null, nullResult);
	}

	/**
	 * 複数レコードの新規作成/更新テスト
	 */
	@isTest private static void doUpsertRecordListTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companylist = ComTestDataUtility.createCompanies('TestCompany', country.Id, 4);
		ComCompany__c baseCompany1 = companylist[0];
		ComCompany__c baseCompany2 = companylist[1];
		delete baseCompany1;
		delete baseCompany2;
		ComCompany__c insertCompany1 = baseCompany1.clone();
		ComCompany__c insertCompany2 = baseCompany2.clone();
		List<ComCompany__c> insertCompanyList = new List<ComCompany__c> {
			insertCompany1,
			insertCompany2
		};

		ComCompany__c updateCompany1 = companylist[2];
		ComCompany__c updateCompany2 = companylist[3];
		List<ComCompany__c> updateCompanyList = new List<ComCompany__c> {
			updateCompany1,
			updateCompany2
		};

		Test.startTest();

		AppDatabase.doUpsert(insertCompanyList);
		AppDatabase.doUpsert(updateCompanyList);

		insertCompany1 = baseCompany1.clone();
		insertCompany2 = baseCompany2.clone();
		insertCompany1.Code__c = null;
		insertCompany2.Code__c = null;
		insertCompanyList = new List<ComCompany__c> {
			insertCompany1,
			insertCompany2
		};
		delete updateCompanyList;

		try {
			AppDatabase.doUpsert(insertCompanyList);
			System.assert(false);
		}
		catch ( Exception ex ) { }

		try {
			AppDatabase.doUpsert(updateCompanyList);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		insertCompanyList = null;
		List<Database.UpsertResult> nullResultList = AppDatabase.doUpsert(insertCompanyList);
		List<Database.UpsertResult> emptyResultList = AppDatabase.doUpsert(new List<ComCompany__c>());

		Test.stopTest();

		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 複数レコードの新規作成/更新、エラー時続行テスト
	 */
	@isTest private static void doUpsertRecordListNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companylist = ComTestDataUtility.createCompanies('TestCompany', country.Id, 4);
		ComCompany__c baseCompany1 = companylist[0];
		ComCompany__c baseCompany2 = companylist[1];
		delete baseCompany1;
		delete baseCompany2;
		ComCompany__c insertCompany1 = baseCompany1.clone();
		ComCompany__c insertCompany2 = baseCompany2.clone();
		List<ComCompany__c> insertCompanyList = new List<ComCompany__c> {
			insertCompany1,
			insertCompany2
		};

		ComCompany__c updateCompany1 = companylist[2];
		ComCompany__c updateCompany2 = companylist[3];
		List<ComCompany__c> updateCompanyList = new List<ComCompany__c> {
			updateCompany1,
			updateCompany2
		};
		List<Database.UpsertResult> successInsertResultList;
		List<Database.UpsertResult> failedInsertResultList;
		List<Database.UpsertResult> successUpdateResultList;
		List<Database.UpsertResult> failedUpdateResultList;
		List<Database.UpsertResult> nullResultList;
		List<Database.UpsertResult> emptyResultList;

		Test.startTest();

		successInsertResultList = AppDatabase.doUpsert(insertCompanyList, false);
		successUpdateResultList = AppDatabase.doUpsert(updateCompanyList, false);
		insertCompany1 = baseCompany1.clone();
		insertCompany2 = baseCompany2.clone();
		insertCompany1.Code__c = null;
		insertCompany2.Code__c = null;
		insertCompanyList = new List<ComCompany__c> {
			insertCompany1,
			insertCompany2
		};
		delete updateCompanyList;
		failedInsertResultList = AppDatabase.doUpsert(insertCompanyList, false);
		failedUpdateResultList = AppDatabase.doUpsert(updateCompanyList, false);
		insertCompanyList = null;
		nullResultList = AppDatabase.doUpsert(insertCompanyList, false);
		emptyResultList = AppDatabase.doUpsert(new List<ComCompany__c>(), false);


		Test.stopTest();

		System.assertEquals(true, successInsertResultList[0].isCreated());
		System.assertEquals(true, successInsertResultList[0].isSuccess());
		System.assertEquals(false, successUpdateResultList[0].isCreated());
		System.assertEquals(true, successUpdateResultList[0].isSuccess());
		System.assertEquals(false, failedInsertResultList[0].isSuccess());
		System.assertEquals(false, failedUpdateResultList[0].isSuccess());
		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}


	/**
	 * 単一レコードの削除テスト
	 */
	@isTest private static void doDeleteSingleRecordTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);

		Test.startTest();

		AppDatabase.doDelete(company);

		try {
			AppDatabase.doDelete(company);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		company = null;
		Database.DeleteResult nullResult = AppDatabase.doDelete(company);

		Test.stopTest();

		System.assertEquals(null, nullResult);
	}

	/**
	 * 単一レコードの削除、エラー時続行テスト
	 */
	@isTest private static void doDeleteSingleRecordNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);
		Database.DeleteResult successResult;
		Database.DeleteResult failedResult;
		Database.DeleteResult nullResult;

		Test.startTest();

		successResult = AppDatabase.doDelete(company, false);
		failedResult = AppDatabase.doDelete(company, false);
		company = null;
		nullResult = AppDatabase.doDelete(company, false);

		Test.stopTest();

		System.assertEquals(true, successResult.isSuccess());
		System.assertEquals(false, failedResult.isSuccess());
		System.assertEquals(null, nullResult);
	}

	/**
	 * 複数レコードの削除テスト
	 */
	@isTest private static void doDeleteRecordListTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);

		Test.startTest();

		AppDatabase.doDelete(companyList);

		try {
			AppDatabase.doDelete(companyList);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		companyList = null;
		List<Database.DeleteResult> nullResultList = AppDatabase.doDelete(companyList);
		List<Database.DeleteResult> emptyResultList = AppDatabase.doDelete(new List<ComCompany__c>());

		Test.stopTest();

		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 複数レコードの削除、エラー時続行テスト
	 */
	@isTest private static void doDeleteRecordListNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		List<Database.DeleteResult> successResultList;
		List<Database.DeleteResult> failedResultList;
		List<Database.DeleteResult> nullResultList;
		List<Database.DeleteResult> emptyResultList;

		Test.startTest();

		successResultList = AppDatabase.doDelete(companyList, false);
		failedResultList = AppDatabase.doDelete(companyList, false);
		companyList = null;
		nullResultList = AppDatabase.doDelete(companyList, false);
		emptyResultList = AppDatabase.doDelete(new List<ComCompany__c>(), false);

		Test.stopTest();

		System.assertEquals(true, successResultList[0].isSuccess());
		System.assertEquals(false, failedResultList[0].isSuccess());
		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}


	/**
	 * 単一レコードIdの削除テスト
	 */
	@isTest private static void doDeleteSingleRecordIdTest() {
//	@isTest private static void doDeleteSingleRecordIdTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);
		Id companyId = company.Id;

		Test.startTest();

		AppDatabase.doDelete(companyId);
		try {
			AppDatabase.doDelete(companyId);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		companyId = null;
		Database.DeleteResult nullResult = AppDatabase.doDelete(companyId);

		Test.stopTest();

		System.assertEquals(null, nullResult);
	}

	/**
	 * 単一レコードIdの削除、エラー時続行テスト
	 */
	@isTest private static void doDeleteSingleRecordIdNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);
		Id companyId = company.Id;
		Database.DeleteResult successResult;
		Database.DeleteResult failedResult;
		Database.DeleteResult nullResult;

		Test.startTest();

		successResult = AppDatabase.doDelete(companyId, false);
		failedResult = AppDatabase.doDelete(companyId, false);
		companyId = null;
		nullResult = AppDatabase.doDelete(companyId, false);

		Test.stopTest();

		System.assertEquals(true, successResult.isSuccess());
		System.assertEquals(false, failedResult.isSuccess());
		System.assertEquals(null, nullResult);
	}

	/**
	 * 複数レコードIdの削除テスト
	 */
	@isTest private static void doDeleteRecordIdListTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		ComCompany__c company1 = companyList[0];
		ComCompany__c company2 = companyList[1];
		List<Id> companyIdList = new List<Id> {
			company1.Id,
			company2.Id
		};

		Test.startTest();

		AppDatabase.doDelete(companyIdList);

		try {
			AppDatabase.doDelete(companyIdList);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		companyIdList = null;
		List<Database.DeleteResult> nullResultList = AppDatabase.doDelete(companyIdList);
		List<Database.DeleteResult> emptyResultList = AppDatabase.doDelete(new List<Id>());

		Test.stopTest();

		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 複数レコードIdの削除、エラー時続行テスト
	 */
	@isTest private static void doDeleteRecordIdListNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		ComCompany__c company1 = companyList[0];
		ComCompany__c company2 = companyList[1];
		List<Id> companyIdList = new List<Id> {
			company1.Id,
			company2.Id
		};
		List<Database.DeleteResult> successResultList;
		List<Database.DeleteResult> failedResultList;
		List<Database.DeleteResult> nullResultList;
		List<Database.DeleteResult> emptyResultList;

		Test.startTest();

		successResultList = AppDatabase.doDelete(companyIdList, false);
		failedResultList = AppDatabase.doDelete(companyIdList, false);
		companyIdList = null;
		nullResultList = AppDatabase.doDelete(companyIdList, false);
		emptyResultList = AppDatabase.doDelete(new List<Id>(), false);

		Test.stopTest();

		System.assertEquals(true, successResultList[0].isSuccess());
		System.assertEquals(false, failedResultList[0].isSuccess());
		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 複数レコードIdの削除テスト
	 */
	@isTest private static void doDeleteRecordIdSetTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		ComCompany__c company1 = companyList[0];
		ComCompany__c company2 = companyList[1];
		Set<Id> companyIdSet = new Set<Id> {
			company1.Id,
			company2.Id
		};

		Test.startTest();

		AppDatabase.doDelete(companyIdSet);

		try {
			AppDatabase.doDelete(companyIdSet);
			System.assert(false);
		}
		catch ( Exception ex ) { }
		companyIdSet = null;
		List<Database.DeleteResult> nullResultList = AppDatabase.doDelete(companyIdSet);
		List<Database.DeleteResult> emptyResultList = AppDatabase.doDelete(new Set<Id>());

		Test.stopTest();

		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 複数レコードIdの削除、エラー時続行テスト
	 */
	@isTest private static void doDeleteRecordIdSetNoAllOrNoneTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('TestCompany', country.Id, 2);
		ComCompany__c company1 = companylist[0];
		ComCompany__c company2 = companyList[1];
		Set<Id> companyIdSet = new Set<Id> {
			company1.Id,
			company2.Id
		};
		List<Database.DeleteResult> successResultList;
		List<Database.DeleteResult> failedResultList;
		List<Database.DeleteResult> nullResultList;
		List<Database.DeleteResult> emptyResultList;

		Test.startTest();

		successResultList = AppDatabase.doDelete(companyIdSet, false);
		failedResultList = AppDatabase.doDelete(companyIdSet, false);
		companyIdSet = null;
		nullResultList = AppDatabase.doDelete(companyIdSet, false);
		emptyResultList = AppDatabase.doDelete(new Set<Id>(), false);

		Test.stopTest();

		System.assertEquals(true, successResultList[0].isSuccess());
		System.assertEquals(false, failedResultList[0].isSuccess());
		System.assertEquals(true, nullResultList.isEmpty());
		System.assertEquals(true, emptyResultList.isEmpty());
	}

	/**
	 * 新規作成権限なしテスト
	 */
	@isTest private static void doInsertNoPermissionTest() {
		ComCompany__c company = new ComCompany__c();

		String objectName = ComCompany__c.SObjectType.getDescribe().getName();
		String fieldName = ComCompany__c.Name_L1__c.getDescribe().getName();

		Test.startTest();

		AppDatabase.isTestObjectPermissionError = true;
		try {
			AppDatabase.doInsert(company);
			System.assert(false);
		}
		catch ( ComSemiNormalException ex ) {
			System.assertEquals(ComMessage.msg().Com_Err_NoObjectInsert(new List<String>{ objectName }), ex.getMessage());
		}

		AppDatabase.isTestObjectPermissionError = false;
		AppDatabase.isTestFieldPermissionError = true;
		company = new ComCompany__c();
		try {
			company.Name_L1__c = 'test';
			AppDatabase.doInsert(company);
			System.assert(false);
		}
		catch ( ComSemiNormalException ex ) {
			System.assertEquals(ComMessage.msg().Com_Err_NoFieldInsert(new List<String>{ objectName, fieldName }), ex.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 更新権限なしテスト
	 */
	@isTest private static void doUpdateNoPermissionTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);

		String objectName = ComCompany__c.SObjectType.getDescribe().getName();
		String fieldName = ComCompany__c.Name_L1__c.getDescribe().getName();

		Test.startTest();

		AppDatabase.isTestObjectPermissionError = true;
		try {
			AppDatabase.doUpdate(company);
			System.assert(false);
		}
		catch ( ComSemiNormalException ex ) {
			System.assertEquals(ComMessage.msg().Com_Err_NoObjectUpdate(new List<String>{ objectName }), ex.getMessage());
		}

		AppDatabase.isTestObjectPermissionError = false;
		AppDatabase.isTestFieldPermissionError = true;
		company = new ComCompany__c(Id = company.Id);
		try {
			company.Name_L1__c = 'test';
			AppDatabase.doUpdate(company);
			System.assert(false);
		}
		catch ( ComSemiNormalException ex ) {
			System.assertEquals(ComMessage.msg().Com_Err_NoFieldUpdate(new List<String>{ objectName, fieldName }), ex.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 削除権限なしテスト
	 */
	@isTest private static void doDeleteNoPermissionTest() {
		ComCountry__c country = ComTestDataUtility.createCountry('TestCountry');
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', country.Id);

		String objectName = ComCompany__c.SObjectType.getDescribe().getName();

		Test.startTest();

		AppDatabase.isTestObjectPermissionError = true;
		try {
			AppDatabase.doDelete(company);
			System.assert(false);
		}
		catch ( ComSemiNormalException ex ) {
			System.assertEquals(ComMessage.msg().Com_Err_NoObjectDelete(new List<String>{ objectName }), ex.getMessage());
		}

		Test.stopTest();
	}

}
