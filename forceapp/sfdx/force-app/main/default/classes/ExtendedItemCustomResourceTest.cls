/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Test Class for ExtendedItemCustom and ExtendedItemCustomOption Resource
 * 
 * @group Expense
 */
@IsTest
private class ExtendedItemCustomResourceTest {
	@testSetup static void setup() {
		TestData.setupMaster();
	}

	/*
	 * Test that the Custom Extended Item can be searched correctly using ID
	 */
	@IsTest static void searchExtendedItemCustomByIdPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, 10);

		ComExtendedItemCustom__c targetTestData = customList.get(5);

		ExtendedItemCustomResource.ExtendedItemCustomSearchCondition searchCondition = new ExtendedItemCustomResource.ExtendedItemCustomSearchCondition();
		searchCondition.id = targetTestData.Id;

		ExtendedItemCustomResource.SearchApi api = new ExtendedItemCustomResource.SearchApi();
		ExtendedItemCustomResource.ExtendedItemCustomSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomSearchResult) api.execute(searchCondition);

		System.assertEquals(1, response.records.size());
		verifyExtendedItemCustom(targetTestData, response.records.get(0));
	}

	/*
	 * Test that the Custom Extended Item can be searched correctly using Company ID
	 */
	@IsTest static void searchExtendedItemCustomByCompanyIdPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, 5);

		// Create another company and Custom Extended Items in that company.
		CompanyEntity anotherCompany = testDataEntity.createCompany('COM_B');
		ComTestDataUtility.createExtendedItemCustoms('Another EIC', anotherCompany.id, 5);

		ExtendedItemCustomResource.ExtendedItemCustomSearchCondition searchCondition = new ExtendedItemCustomResource.ExtendedItemCustomSearchCondition();
		searchCondition.companyId = testDataEntity.company.id;

		ExtendedItemCustomResource.SearchApi api = new ExtendedItemCustomResource.SearchApi();
		ExtendedItemCustomResource.ExtendedItemCustomSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomSearchResult) api.execute(searchCondition);

		// Verify that the Search only returns the Custom Extended Items in the specified company.
		System.assertEquals(5, response.records.size());
		// Since it's ordered by Code ASC, can use simple index to index comparison
		for (Integer i = 0; i < 5; i++) {
			verifyExtendedItemCustom(customList.get(i), response.records.get(i));
		}
	}

	/*
	 * Test that the Custom Extended Item can be searched correctly using Code
	 */
	@IsTest static void searchExtendedItemCustomByCodePositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, 10);

		ComExtendedItemCustom__c targetTestData = customList.get(5);

		ExtendedItemCustomResource.ExtendedItemCustomSearchCondition searchCondition = new ExtendedItemCustomResource.ExtendedItemCustomSearchCondition();
		searchCondition.code = targetTestData.Code__c;

		ExtendedItemCustomResource.SearchApi api = new ExtendedItemCustomResource.SearchApi();
		ExtendedItemCustomResource.ExtendedItemCustomSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomSearchResult) api.execute(searchCondition);

		System.assertEquals(1, response.records.size());
		verifyExtendedItemCustom(targetTestData, response.records.get(0));
	}

	/*
	 * Test that the correct Exception will be thrown when the search Param isn't valid.
	 */
	@IsTest static void searchExtendedItemCustomParamNegativeTest() {
		ExtendedItemCustomResource.SearchApi api = new ExtendedItemCustomResource.SearchApi();

		// ID is not valid ID => NG
		ExtendedItemCustomResource.ExtendedItemCustomSearchCondition invalidIdSearchCondition = new ExtendedItemCustomResource.ExtendedItemCustomSearchCondition();
		invalidIdSearchCondition.id = 'invalid-id';
		Exception invalidIdException;
		try {
			api.execute(invalidIdSearchCondition);
		} catch (Exception e) {
			invalidIdException = e;
		}
		System.assertNotEquals(null, invalidIdException);
		System.assertEquals(true, invalidIdException instanceof App.ParameterException);
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), invalidIdException.getMessage());

		// CompanyID is not valid ID => NG
		ExtendedItemCustomResource.ExtendedItemCustomSearchCondition invalidCompanyIdSearchCondition = new ExtendedItemCustomResource.ExtendedItemCustomSearchCondition();
		invalidCompanyIdSearchCondition.companyId = 'invalid-id';
		Exception invalidCompanyIdException;
		try {
			api.execute(invalidCompanyIdSearchCondition);
		} catch (Exception e) {
			invalidCompanyIdException = e;
		}
		System.assertNotEquals(null, invalidCompanyIdException);
		System.assertEquals(true, invalidCompanyIdException instanceof App.ParameterException);
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}), invalidCompanyIdException.getMessage());
	}

	/*
	 * Verify the fields of the reponse against the expected.
	 */
	private static void verifyExtendedItemCustom(ComExtendedItemCustom__c targetTestData, ExtendedItemCustomResource.ExtendedItemCustom responseRecord) {
		System.assertEquals(targetTestData.Id, responseRecord.id);
		System.assertEquals(targetTestData.Code__c, responseRecord.code);
		System.assertEquals(targetTestData.Name_L0__c, responseRecord.nameL0);
		System.assertEquals(targetTestData.Name_L1__c, responseRecord.nameL1);
		System.assertEquals(targetTestData.Name_L2__c, responseRecord.nameL2);
	}

	/*
	 * Test that the Custom Extended Item Option can be searched correctly using ID
	 */
	@IsTest static void searchExtendedItemCustomOptionByIdPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, 10);
		List<ComExtendedItemCustomOption__c> customOptionList = ComTestDataUtility.createExtendedItemCustomOptions('EICO', customList, 10);

		ComExtendedItemCustomOption__c targetTestData = customOptionList.get(5);

		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition searchCondition = new ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition();
		searchCondition.id = targetTestData.Id;

		ExtendedItemCustomResource.SearchOptionApi api = new ExtendedItemCustomResource.SearchOptionApi();
		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);

		System.assertEquals(1, response.records.size());
		verifyExtendedItemCustomOption(targetTestData, response.records.get(0));
	}

	/*
	 * Test that the Custom Extended Item Option can be searched correctly using Code
	 */
	@IsTest static void searchExtendedItemCustomOptionByCodePositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		Integer totalExtendedItemCustoms = 10;
		Integer totalExtendedItemCustomOptions = 8;
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, totalExtendedItemCustoms);
		List<ComExtendedItemCustomOption__c> customOptionList = ComTestDataUtility.createExtendedItemCustomOptions('EICO', customList, totalExtendedItemCustomOptions);      //results in 80 Options

		Integer offsetIndex = 5;
		ComExtendedItemCustomOption__c targetTestData = customOptionList.get(offsetIndex);

		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition searchCondition = new ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition();
		searchCondition.code = targetTestData.Code__c;

		ExtendedItemCustomResource.SearchOptionApi api = new ExtendedItemCustomResource.SearchOptionApi();
		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);

		System.assertEquals(totalExtendedItemCustoms, response.records.size());        //Since there are 10 Custom Extended Items, and the Options are using 1 to 8 for codes


		// Since all Codes are the same, should not rely on sorting by Code order when validating
		Map<Id, ComExtendedItemCustomOption__c> expectedDataMap = new Map<Id, ComExtendedItemCustomOption__c>();
		for (Integer i = 0; i < totalExtendedItemCustoms; i++) {
			ComExtendedItemCustomOption__c customOption = customOptionList.get(offsetIndex + (totalExtendedItemCustomOptions * i));
			expectedDataMap.put(customOption.Id, customOption);
		}

		for (ExtendedItemCustomResource.ExtendedItemCustomOption responseRecord: response.records) {
			verifyExtendedItemCustomOption(expectedDataMap.get(responseRecord.id), responseRecord);
		}
	}

	/*
	 * Test that the Custom Extended Item Option can be searched correctly using the Extended Item Custom ID they belongs to
	 */
	@IsTest static void searchExtendedItemCustomOptionByExtendedItemCustomIdPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		Integer totalExtendedItemCustoms = 3;
		Integer totalExtendedItemCustomOptions = 10;
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, totalExtendedItemCustoms);
		List<ComExtendedItemCustomOption__c> customOptionList = ComTestDataUtility.createExtendedItemCustomOptions('EICO', customList, totalExtendedItemCustomOptions);      //results in 30 Options


		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition searchCondition = new ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition();
		searchCondition.extendedItemCustomId = customList[1].Id;        // Using the middle one

		ExtendedItemCustomResource.SearchOptionApi api = new ExtendedItemCustomResource.SearchOptionApi();
		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);

		System.assertEquals(totalExtendedItemCustomOptions, response.records.size());

		// Since all Codes are the same, should not rely on sorting by Code order when validating
		Map<Id, ComExtendedItemCustomOption__c> expectedDataMap = new Map<Id, ComExtendedItemCustomOption__c>();
		for (Integer i = 0; i < totalExtendedItemCustomOptions; i++) {
			// Since using middle, offset by 10 (the first 10 options belongs to the customList[0]
			ComExtendedItemCustomOption__c customOption = customOptionList.get(totalExtendedItemCustomOptions + i);
			expectedDataMap.put(customOption.Id, customOption);
		}

		for (ExtendedItemCustomResource.ExtendedItemCustomOption responseRecord: response.records) {
			verifyExtendedItemCustomOption(expectedDataMap.get(responseRecord.id), responseRecord);
		}
	}

	/*
	 * Test that the Custom Extended Item Option can be searched correctly using a query which is either in name or code
	 */
	@IsTest static void searchExtendedItemCustomOptionByQueryPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		Integer totalExtendedItemCustoms = 3;
		Integer totalExtendedItemCustomOptions = 100;
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, totalExtendedItemCustoms);
		List<ComExtendedItemCustomOption__c> customOptionList = ComTestDataUtility.createExtendedItemCustomOptions('EICO', customList, totalExtendedItemCustomOptions);      //results in 300 Options

		// Prepare data for testing
		customOptionList[5].Name_L0__c = 'Query Test Name AAA';
		customOptionList[6].Code__c = 'Query Test Code BBB';
		customOptionList[7].Name_L0__c = 'Special % Character';
		customOptionList[8].Name_L0__c = 'Special .* Characters';
		customOptionList[9].Name_L0__c = 'Special \\ Characters';
		customOptionList[10].Name_L0__c = 'Special _ Characters';
		customOptionList[11].Name_L0__c = 'Special \' Characters';
		customOptionList[12].Name_L0__c = 'Special " Characters';
		UPDATE customOptionList;

		ExtendedItemCustomResource.SearchOptionApi api = new ExtendedItemCustomResource.SearchOptionApi();
		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition searchCondition = new ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition();

		// Case 1: Search Query is done in both code and name
		searchCondition.query = 'Query Test';
		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(2, response.records.size());
		System.assertEquals('Query Test Name AAA', response.records[0].nameL0);
		System.assertEquals('Query Test Code BBB', response.records[1].code);
		System.assertEquals(false, response.hasMore);

		//Case 2: Search Query will only returns correct data is the query is specific enough
		searchCondition.query = 'BBB';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(1, response.records.size());
		System.assertEquals('Query Test Code BBB', response.records[0].code);
		System.assertEquals(false, response.hasMore);

		//Case 3: Limit test
		searchCondition.query = 'EICO';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(100, response.records.size());
		System.assertEquals(true, response.hasMore);

		//Case 4: Special Character `%`
		searchCondition.query = '%';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(1, response.records.size());
		System.assertEquals(customOptionList[7].Name_L0__c, response.records[0].nameL0);
		System.assertEquals(false, response.hasMore);

		//Case 5: Special Character `.*`
		searchCondition.query = '.*';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(1, response.records.size());
		System.assertEquals(customOptionList[8].Name_L0__c, response.records[0].nameL0);
		System.assertEquals(false, response.hasMore);

		//Case 6: Special Character `\`
		searchCondition.query = '\\';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(1, response.records.size());
		System.assertEquals(customOptionList[9].Name_L0__c, response.records[0].nameL0);
		System.assertEquals(false, response.hasMore);

		//Case 7: Special Character `_`
		searchCondition.query = '_';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(1, response.records.size());
		System.assertEquals(customOptionList[10].Name_L0__c, response.records[0].nameL0);
		System.assertEquals(false, response.hasMore);

		//Case 8: Special Character `'`
		searchCondition.query = '\'';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(1, response.records.size());
		System.assertEquals(customOptionList[11].Name_L0__c, response.records[0].nameL0);
		System.assertEquals(false, response.hasMore);

		//Case 9: Special Character `"`
		searchCondition.query = '"';
		response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(searchCondition);
		System.assertEquals(1, response.records.size());
		System.assertEquals(customOptionList[12].Name_L0__c, response.records[0].nameL0);
		System.assertEquals(false, response.hasMore);
	}

	/*
	 * Test that the correct Exception will be thrown when the Option Search Param isn't valid.
	 */
	@IsTest static void searchExtendedItemCustomOptionParamNegativeTest() {
		ExtendedItemCustomResource.SearchOptionApi api = new ExtendedItemCustomResource.SearchOptionApi();

		// ID is not valid ID => NG
		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition invalidIdSearchCondition = new ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition();
		invalidIdSearchCondition.id = 'invalid-id';
		Exception invalidIdException;
		try {
			api.execute(invalidIdSearchCondition);
		} catch (Exception e) {
			invalidIdException = e;
		}
		System.assertNotEquals(null, invalidIdException);
		System.assertEquals(true, invalidIdException instanceof App.ParameterException);
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), invalidIdException.getMessage());

		// ExtendedItemCustomId is not valid ID => NG
		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition invalidExtendedItemCustomIdSearchCondition = new ExtendedItemCustomResource.ExtendedItemCustomOptionSearchCondition();
		invalidExtendedItemCustomIdSearchCondition.extendedItemCustomId = 'invalid-id';
		Exception invalidExtendedItemCustomIdException;
		try {
			api.execute(invalidExtendedItemCustomIdSearchCondition);
		} catch (Exception e) {
			invalidExtendedItemCustomIdException = e;
		}
		System.assertNotEquals(null, invalidExtendedItemCustomIdException);
		System.assertEquals(true, invalidExtendedItemCustomIdException instanceof App.ParameterException);
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'extendedItemCustomId'}), invalidExtendedItemCustomIdException.getMessage());
	}

	@IsTest static void getRecentlyUsedOptionListApi() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		Integer totalExtendedItemCustoms = 1;
		Integer totalExtendedItemCustomOptions = 20;
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('EIC', testDataEntity.company.id, totalExtendedItemCustoms);
		List<ComExtendedItemCustomOption__c> customOptionList = ComTestDataUtility.createExtendedItemCustomOptions('A', customList, totalExtendedItemCustomOptions);
		List<ComExtendedItem__c> extendedItemLookupList = ComTestDataUtility.createExtendedItemLookups('EL', testDataEntity.company.id, customList);

		List<String> codes = new List<String>();
		for (Integer i = 10; i > 0; i--) {
			codes.add(customOptionList.get(i).Code__c);
		}

		ExpRecentlyUsed__c recentlyUsed = New ExpRecentlyUsed__c(
				EmployeeBaseId__c = testDataEntity.employee.id,
				MasterType__c = ExpRecentlyUsedEntity.MasterType.ExtendedItemLookup.name(),
				TargetId__c = extendedItemLookupList.get(0).Id,
				RecentlyUsedValues__c = JSON.serialize(codes)
		);
		INSERT recentlyUsed;

		Test.startTest();
		ExtendedItemCustomResource.GetRecentlyUsedOptionListParam param = new ExtendedItemCustomResource.GetRecentlyUsedOptionListParam();
		param.employeeBaseId = testDataEntity.employee.id;
		param.extendedItemLookupId = extendedItemLookupList[0].Id;
		param.extendedItemCustomId = customList[0].Id;
		ExtendedItemCustomResource.GetRecentlyUsedOptionListApi api =
				new ExtendedItemCustomResource.GetRecentlyUsedOptionListApi();

		ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult response = (ExtendedItemCustomResource.ExtendedItemCustomOptionSearchResult) api.execute(param);
		Test.stopTest();
		System.assertEquals(10, response.records.size());
		for (Integer i = 0; i < 10; i++) {
			System.assertEquals(codes.get(i), response.records.get(i).code);
		}
	}

	/*
	 * Verify the fields of the response against the expected.
	 */
	private static void verifyExtendedItemCustomOption(ComExtendedItemCustomOption__c targetTestData, ExtendedItemCustomResource.ExtendedItemCustomOption responseRecord) {
		System.assertEquals(targetTestData.Id, responseRecord.id);
		System.assertEquals(targetTestData.Code__c, responseRecord.code);
		System.assertEquals(targetTestData.ExtendedItemCustomId__c, responseRecord.extendedItemCustomId);
		System.assertEquals(targetTestData.Name_L0__c, responseRecord.nameL0);
		System.assertEquals(targetTestData.Name_L1__c, responseRecord.nameL1);
		System.assertEquals(targetTestData.Name_L2__c, responseRecord.nameL2);
	}
}