/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpTypeGroupService
 */
@isTest
private class ExpTypeGroupServiceTest {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * Test data class
	 */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * Create expense type group records
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Integer size) {
			return createExpTypeGroups(name, this.company.id, size);
		}

		/**
		 * Create expense type group records
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpTypeGroups(name, companyId, size);
			return (new ExpTypeGroupRepository()).getEntityList(null);
		}

		/**
		 * Create expense type records
		 */
		public List<ExpTypeEntity> createExpTypes(String name, Integer size) {
			return createExpTypes(name, this.company.id, size);
		}

		/**
		 * Create expense type records
		 */
		public List<ExpTypeEntity> createExpTypes(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpTypes(name, companyId, size);
			return (new ExpTypeRepository()).getEntityList(null);
		}

		/**
		 * Create an expense type group entity(Do not update to Object)
		 */
		public ExpTypeGroupEntity createExpTypeGroupEntity(String name) {
			ExpTypeGroupEntity entity = new ExpTypeGroupEntity();

			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.company.code + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.company.id;
			entity.parentId = null;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.order = 1;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);

			return entity;
		}
	}

	/**
	 * Test for createNewExpTypeGroup method
	 * Check if an new record is correctly created
	 */
	@isTest static void createNewExpTypeGroupTest() {
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		ExpTypeGroupEntity newEntity = testData.createExpTypeGroupEntity('New Group');

		Id resId = (new ExpTypeGroupService()).createNewExpTypeGroup(newEntity);
		System.assertNotEquals(null, resId);

		ExpTypeGroupEntity savedEntity = new ExpTypeGroupRepository().getEntity(resId);
		System.assertNotEquals(null, savedEntity);
		assertSaveAllField(newEntity, savedEntity);
	}

	/**
	 * Test for createNewExpTypeGroup method
	 * Check if exception is thrown if parameter include incorrect value
	 */
	@isTest static void createNewExpTypeGroupTestValidation() {
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		ExpTypeGroupEntity newEntity = testData.createExpTypeGroupEntity('New Group');
		newEntity.nameL0 = '';

		try {
			Id resId = (new ExpTypeGroupService()).createNewExpTypeGroup(newEntity);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
			String msg = MESSAGE.Com_Err_NotSetValue(new List<String>{MESSAGE.Admin_Lbl_Name});
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Test for createNewExpTypeGroup method
	 * Check if exception is thrown if the same code already exists in the company
	 */
	@isTest static void createNewExpTypeGroupTestDuplicateCode() {
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		ExpTypeGroupEntity testGroup = testData.createExpTypeGroups('Test', 1).get(0);
		ExpTypeGroupEntity newEntity = testData.createExpTypeGroupEntity('New Group');
		newEntity.code = testGroup.code;

		try {
			Id resId = (new ExpTypeGroupService()).createNewExpTypeGroup(newEntity);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Com_Err_DuplicateCode;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Test for createNewExpTypeGroup method
	 * Check if exception is thrown if the specified parent id is invalid
	 */
	@isTest static void createNewExpTypeGroupTestInvalidParent() {
		final Integer testGroupSize = 4;
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		List<ExpTypeGroupEntity> testGroups = testData.createExpTypeGroups('Test', testGroupSize);
		ExpTypeGroupEntity newEntity = testData.createExpTypeGroupEntity('New Group');

		// parent record can not be found
		newEntity.parentId = testGroups[0].id;
		(new ExpTypeGroupService()).deleteExpTypeGroup(testGroups[0].id);
		try {
			(new ExpTypeGroupService()).createNewExpTypeGroup(newEntity);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.RecordNotFoundException e) {
			System.assert(true);
		}

		// parent id of the parent entity is set(hierarchy can not be 3 levels or more)
		testGroups[3].parentId = testGroups[2].id;
		(new ExpTypeGroupRepository()).saveEntity(testGroups[3]);
		newEntity.parentId = testGroups[3].id;
		try {
			(new ExpTypeGroupService()).createNewExpTypeGroup(newEntity);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		}

		// validFrom of parent > validFrom of the target entity
		testGroups[1].validFrom = newEntity.validFrom.addDays(1);
		(new ExpTypeGroupRepository()).saveEntity(testGroups[1]);
		newEntity.parentId = testGroups[1].id;
		try {
			(new ExpTypeGroupService()).createNewExpTypeGroup(newEntity);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		}
	}

	/**
	 * Test for updateExpTypeGroup method
	 * Check if all fields of the target record are correctly updated
	 */
	@isTest static void updateExpTypeGroupTestAllField() {
		final Integer testGroupSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();

		List<ExpTypeGroupEntity> testGroups = testData.createExpTypeGroups('Test', testGroupSize);
		ExpTypeGroupEntity targetGroup = testGroups[1];
		targetGroup.code = targetGroup.id;
		targetGroup.nameL0 = targetGroup.nameL0 + 'Update';
		targetGroup.nameL1 = targetGroup.nameL1 + 'Update';
		targetGroup.nameL2 = targetGroup.nameL2 + 'Update';
		targetGroup.companyId = testData.company.id;
		targetGroup.parentId = testGroups[0].id;
		targetGroup.descriptionL0 = targetGroup.descriptionL0 + 'Update';
		targetGroup.descriptionL1 = targetGroup.descriptionL1 + 'Update';
		targetGroup.descriptionL2 = targetGroup.descriptionL2 + 'Update';
		targetGroup.order = targetGroup.order + 10;
		targetGroup.validFrom = targetGroup.validFrom.addDays(10);
		targetGroup.validTo = targetGroup.validTo.addDays(10);

		// update
		(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);

		// assertion
		ExpTypeGroupEntity savedEntity = new ExpTypeGroupRepository().getEntity(targetGroup.id);
		assertSaveAllField(targetGroup, savedEntity);
	}

	/**
	 * Test for updateExpTypeGroup method
	 * Check if some fields of the target record are correctly updated
	 */
	@isTest static void updateExpTypeGroupTestSomeField() {
		final Integer testGroupSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();

		ExpTypeGroupEntity targetGroup = testData.createExpTypeGroups('Test', testGroupSize).get(0);

		// entity for update
		ExpTypeGroupEntity updateGroup = new ExpTypeGroupEntity();
		updateGroup.setId(targetGroup.id);
		updateGroup.nameL0 = targetGroup.nameL0 + 'Update';
		updateGroup.order = targetGroup.order + 10;

		// update
		(new ExpTypeGroupService()).updateExpTypeGroup(updateGroup);

		// assertion
		ExpTypeGroupEntity savedEntity = new ExpTypeGroupRepository().getEntity(updateGroup.id);
		System.assertEquals(updateGroup.nameL0, savedEntity.nameL0);
		System.assertEquals(updateGroup.order, savedEntity.order);
		// the value of other fields are not updated
		System.assertEquals(targetGroup.nameL1, savedEntity.nameL1);
	}

	/**
	 * Test for updateExpTypeGroup method
	 * Check if exception is thrown if parameter include incorrect value
	 */
	@isTest static void updateExpTypeGroupTestValidation() {
		final Integer testGroupSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();

		ExpTypeGroupEntity targetGroup = testData.createExpTypeGroups('Test', testGroupSize).get(0);
		targetGroup.nameL0 = null;

		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
			String msg = MESSAGE.Com_Err_NotSetValue(new List<String>{MESSAGE.Admin_Lbl_Name});
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Test for updateExpTypeGroup method
	 * Check if exception is thrown if the same code already exists in the company
	 */
	@isTest static void updateExpTypeGroupTestDuplicateCode() {
		final Integer testGroupSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();

		List<ExpTypeGroupEntity> targetGroups = testData.createExpTypeGroups('Test', testGroupSize);
		ExpTypeGroupEntity targetGroup = targetGroups[1];
		targetGroup.code = targetGroups[2].code;

		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Com_Err_DuplicateCode;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Test for updateExpTypeGroup method
	 * Check if exception is thrown if the specified parent id is invalid
	 */
	@isTest static void updateExpTypeGroupTestInvalidParent() {
		final Integer testGroupSize = 7;
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		List<ExpTypeGroupEntity> testGroups = testData.createExpTypeGroups('Test', testGroupSize);
		ExpTypeGroupEntity targetGroup = testGroups[5];

		// parent record can not be found
		targetGroup.parentId = testGroups[0].id;
		(new ExpTypeGroupService()).deleteExpTypeGroup(testGroups[0].id);
		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.RecordNotFoundException e) {
			System.assert(true);
		}

		// parent record can not be found
		targetGroup.parentId = testGroups[0].id;
		(new ExpTypeGroupService()).deleteExpTypeGroup(testGroups[0].id);
		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.RecordNotFoundException e) {
			System.assert(true);
		}

		// parent id of the parent entity is set(hierarchy can not be 3 levels or more)
		testGroups[3].parentId = testGroups[2].id;
		(new ExpTypeGroupRepository()).saveEntity(testGroups[3]);
		targetGroup.parentId = testGroups[3].id;
		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		}

		// parent id of the parent entity is set(hierarchy can not be 3 levels or more)
		testGroups[1].validFrom = targetGroup.validFrom.addDays(1);
		(new ExpTypeGroupRepository()).saveEntity(testGroups[1]);
		targetGroup.parentId = testGroups[1].id;
		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		}

		// the target entity already has child groups(hierarchy can not be 3 levels or more)
		testGroups[6].parentId = targetGroup.id;
		(new ExpTypeGroupService()).updateExpTypeGroup(testGroups[6]);
		targetGroup.parentId = testGroups[4].id;
		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		}
	}

	/**
	 * Test for updateExpTypeGroup method
	 * Check if the target record can not be found
	 */
	@isTest static void updateExpTypeGroupTestRecordNotFound() {
		final Integer testGroupSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();

		List<ExpTypeGroupEntity> targetGroups = testData.createExpTypeGroups('Test', testGroupSize);
		ExpTypeGroupEntity targetGroup = targetGroups[1];
		(new ExpTypeGroupRepository()).deleteEntity(targetGroup.id);

		try {
			(new ExpTypeGroupService()).updateExpTypeGroup(targetGroup);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.RecordNotFoundException e) {
			String msg = MESSAGE.Com_Err_RecordNotFound;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Check if each field of the record is correctly set
	 */
	private static void assertSaveAllField(ExpTypeGroupEntity expEntity, ExpTypeGroupEntity actEntity) {

		System.assertEquals(expEntity.code, actEntity.code);
		System.assertEquals(expEntity.nameL0, actEntity.nameL0);
		System.assertEquals(expEntity.nameL1, actEntity.nameL1);
		System.assertEquals(expEntity.nameL2, actEntity.nameL2);
		System.assertEquals(expEntity.companyId, actEntity.companyId);
		System.assertEquals(expEntity.parentId, actEntity.parentId);
		System.assertEquals(expEntity.descriptionL0, actEntity.descriptionL0);
		System.assertEquals(expEntity.descriptionL1, actEntity.descriptionL1);
		System.assertEquals(expEntity.descriptionL2, actEntity.descriptionL2);
		System.assertEquals(expEntity.order, actEntity.order);
		System.assertEquals(expEntity.validFrom, actEntity.validFrom);
		System.assertEquals(expEntity.validTo, actEntity.validTo);
	}

	/**
	 * Test for deleteExpTypeGroup method
	 * Check if the target record is correctly deleted
	 */
	@isTest static void deleteTest() {
		final Integer testGroupSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		ExpTypeGroupEntity targetGroup = testData.createExpTypeGroups('Test', testGroupSize).get(0);

		// delete
		(new ExpTypeGroupService()).deleteExpTypeGroup(targetGroup.id);

		// check if the target record is deleted
		ExpTypeGroupEntity resGroup = (new ExpTypeGroupRepository()).getEntity(targetGroup.id);
		System.assertEquals(null, resGroup);
	}

	/**
	 * Test for deleteExpTypeGroup method
	 * Check if exception is not thrown if the target record is already deleted
	 */
	@isTest static void deleteTestRecordNotFound() {
		final Integer testGroupSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		ExpTypeGroupEntity targetGroup = testData.createExpTypeGroups('Test', testGroupSize).get(0);

		// delete the target record
		(new ExpTypeGroupRepository()).deleteEntity(targetGroup);

		// delete(the target record is already deleted)
		try {
			(new ExpTypeGroupService()).deleteExpTypeGroup(targetGroup.id);
		} catch (Exception e) {
			System.debug('Exception: ' + e.getStackTraceString());
			System.assert(false, 'Unexpected exception occurred.' + e.getMessage());
		}
	}

	/**
	 * Test for deleteExpTypeGroup method
	 * Check if exception is thrown if the id in request parameter is null
	 */
	@isTest static void deleteTestHasChildren() {
		final Integer testGroupSize = 3;
		final Integer testExpTypeSize = 3;
		ExpTypeGroupServiceTest.TestData testData = new TestData();
		List<ExpTypeGroupEntity> testGroups = testData.createExpTypeGroups('Test', testGroupSize);
		List<ExpTypeEntity> testExpTypes = testData.createExpTypes('Test', testExpTypeSize);

		ExpTypeGroupEntity targetGroup1 = testGroups[0];

		// set parent id
		testGroups[1].parentId = targetGroup1.id;
		(new ExpTypeGroupRepository()).saveEntity(testGroups[1]);

		// delete(exists expense type group whose parent is the target record)
		ExpTypeGroupResource.DeleteApi api = new ExpTypeGroupResource.DeleteApi();
		try {
			(new ExpTypeGroupService()).deleteExpTypeGroup(targetGroup1.id);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		}

		ExpTypeGroupEntity targetGroup2 = testGroups[2];

		// set parent id
		testExpTypes[0].parentGroupId = targetGroup2.id;
		(new ExpTypeRepository()).saveEntity(testExpTypes[0]);

		// delete(exists expense type whose parent is the target record)
		try {
			(new ExpTypeGroupService()).deleteExpTypeGroup(targetGroup2.id);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		}
	}
}