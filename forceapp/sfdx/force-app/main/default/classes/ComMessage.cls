public with sharing class ComMessage {

	@TestVisible
	private static Map<String, ComMsgBase> msgMap = new Map<String, ComMsgBase>();

	/**
	 * 指定された言語の固定文言Mapを取得
	 * @param language 指定言語
	 * @return 固定文言Map
	 */
	public static ComMsgBase msg(String language) {
		if (msgMap.containsKey(language) == false) {
			if (language == 'en_US') {
				// 英語
				msgMap.put(language, new ComMsg_en_US());
			} else {
				// それ以外の場合は日本語
				msgMap.put(language, new ComMsg_ja());
			}
		}
		return msgMap.get(language);
	}

	/**
	 * ログインユーザ言語の固定文言Mapを取得
	 * @return 固定文言Map
	 */
	public static ComMsgBase msg() {
		return msg(userinfo.getLanguage());
	}

	/** @description カスタムラベルサービス */
	private static ComCustomLabelService customLabelService = new ComCustomLabelService();

	/**
	 * @description ユーザによって変更されたラベル/メッセージをMapで取得する
	 * @return ユーザによって変更されたラベル
	 */
	public static Map<String, Map<String, String>> getChangedCustomMessageMap() {
		// 現在はラベルのみ対応
		return customLabelService.getCustomLabelMap();
	}
}