/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 拡張項目必須
 */
public with sharing class ComExtendedItemRequiredFor extends ValueObjectType {
	
	/** 経費精算のみ */
	public static final ComExtendedItemRequiredFor EXPENSE_REPORT = new ComExtendedItemRequiredFor('ExpenseReport');
	/** 事前申請と経費精算 */
	public static final ComExtendedItemRequiredFor EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ComExtendedItemRequiredFor('ExpenseRequestAndExpenseReport');
	
	/** 拡張項目必須のリスト（明細タイプが追加されら本リストにも追加してください) */
	public static final List<ComExtendedItemRequiredFor> TYPE_LIST;
	static {
		TYPE_LIST = new List<ComExtendedItemRequiredFor> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}
	
	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ComExtendedItemRequiredFor(String value) {
		super(value);
	}
	
	/**
	 * 値からComExtendedItemRequiredForを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つComExtendedItemRequiredFor、ただし該当するComExtendedItemRequiredForが存在しない場合はnull
	 */
	public static ComExtendedItemRequiredFor valueOf(String value) {
		ComExtendedItemRequiredFor retType = null;
		if (String.isNotBlank(value)) {
			for (ComExtendedItemRequiredFor type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/*
	 * Get the String value for the ComExtendedItemRequiredFor
	 * @param requiredFor ComExtendedItemRequiredFor
	 * @return null if requiredFor is null. Otherwise, return the String value.
	 */
	public static String getValue(ComExtendedItemRequiredFor requiredFor) {
		return requiredFor == null ? null : requiredFor.value;
	}
	
	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ComExtendedItemRequiredFor以外の場合はfalse
		Boolean eq;
		if (compare instanceof ComExtendedItemRequiredFor) {
			// 値が同じであればtrue
			if (this.value == ((ComExtendedItemRequiredFor)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}