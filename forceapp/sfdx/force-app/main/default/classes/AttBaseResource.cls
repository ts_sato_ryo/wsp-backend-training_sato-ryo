/**
 * @group 勤怠
 * @description 勤怠リソースクラスの基底クラス
 */
public with sharing abstract class AttBaseResource extends RemoteApi.ResourceBase {
	/**
	 * 勤怠処理で利用するマスタのキャッシュ機能を有効化する
	 * キャッシュを利用しない場合はリポジトリのコンストラクト時に指定する
	 */
	@TestVisible
	protected virtual void enableMasterCaches() {
		// 親子型マスタ
		EmployeeRepository.enableCache();
		DepartmentRepository.enableCache();
		AttWorkingTypeRepository.enableCache();
		TimeSettingRepository.enableCache();
		AttShortTimeSettingRepository.enableCache();
		// 有効期間型マスタ
		AttAgreementAlertSettingRepository.enableCache();
		AttLeaveOfAbsenceRepository.enableCache();
		AttLeaveRepository.enableCache();
		AttPatternRepository2.enableCache();
		// 論理削除型マスタ
		CalendarRepository.enableCache();
		CompanyRepository.enableCache();
	}
}
