/**
 * 日時の値オブジェクト
 */
public with sharing class AppDatetime extends ValueObject implements Comparable {
	private static DateTime customNow;

	/** 保持する日時 */
	private Datetime value;

	/**
	 * コンストラクタ（日時文字列を指定）
	 * @param datetimeString 日時文字列(UTC, フォーマットはISO8601「YYYY-MM-DDTHH:mm:ssZ」)
	 */
	public AppDatetime(String datetimeString) {
		if (String.isBlank(datetimeString)) {
			throw new App.ParameterException('datetimeString', datetimeString);
		}

		try {
			value = DateTime.valueOfGmt(datetimeString.replace('Z','').replace('T', ' '));
		} catch (Exception e) {
			throw new App.ParameterException('Invalid datetime format :' + datetimeString);
		}
	}

	/**
	 * コンストラクタ（Datetime型の値を指定）
	 * @param value Datetime型の値
	 */
	public AppDatetime(Datetime value) {
		if (value == null) {
			throw new App.ParameterException('value', value);
		}
		this.value = value;
	}
	/**
	 * 現在時刻をセットする(テスト実行時にnowメソッドで取得する時刻を指定したい場合に使用する)
	 */
	@TestVisible
	private static void setNow(DateTime now) {
		AppDatetime.customNow = now;
	}
	/**
	 * 現在時刻でインスタンスを作成する
	 */
	public static AppDatetime now() {
		if (Test.isRunningTest()) {
			return new AppDatetime(customNow == null ? Datetime.now() : customNow);
		}

		// テスト実行時以外は必ず現在時刻を返す
		return new AppDatetime(Datetime.now());
	}
	/**
	 * DateTime型に変換する
	 * @param appValue 変換対象の値S
	 * @return 変換したDataTime型の値、ただしappValueがnullの場合はnull
	 */
	public static DateTime convertDatetime(AppDateTime appValue) {
		if (appValue == null) {
			return null;
		}
		return appValue.value;
	}
	/**
	 * 指定した文字列のAppDatetimeを返却する(UTC)。
	 * @param datetimeString 日時文字列(UTC, フォーマットはISO8601「YYYY-MM-DDTHH:mm:ssZ」)
	 * @return 作成したAttDatetimeインスタンス
	 */
	public static AppDatetime parse(String value) {
		return valueOfUtc(value);
	}

	/**
	 * 指定したDatetimeからAppDatetimeを作成する
	 * @param Datetime
	 * @return 作成したAttDatetimeインスタンス
	 */
	public static AppDatetime valueOf(Datetime dt){
		if (dt == null) {
			return null;
		}
		return new AppDatetime(dt);
	}

	/**
	 * 指定した文字列のAppDatetimeを返却する(UTC)。
	 * @param datetimeString 日時文字列(UTC, フォーマットはISO8601「YYYY-MM-DDTHH:mm:ssZ」)
	 * @return 作成したAttDatetimeインスタンス
	 */
	public static AppDatetime valueOfUtc(String datetimeString) {
		return new AppDatetime(datetimeString);
	}

	/**
	 * 指定した文字列のAppDatetimeを返却する(ローカルタイム)。
	 * @param datetimeString 日時文字列(ローカルタイム, フォーマットは「YYYY-MM-DD HH:mm:ss」)
	 * @return 作成したAttDatetimeインスタンス
	 */
	public static AppDatetime valueOfLocal(String datetimeString) {
		if (String.isBlank(datetimeString)) {
			throw new App.ParameterException('datetimeString', datetimeString);
		}

		AppDatetime dt;
		try {
			dt = new AppDatetime(DateTime.valueOf(datetimeString));
		} catch (Exception e) {
			throw new App.ParameterException('Invalid datetime format :' + datetimeString);
		}
		return dt;
	}

	/**
	 * 指定した日数を加算する
	 * @param 加算する日数
	 * @return 加算後のAttDatetime
	 */
	public AppDatetime addDays(Integer days) {
		return new AppDatetime(value.addDays(days));
	}

	/**
	 * 指定した時間数を加算する
	 * @param 加算する時間
	 * @return 加算後のAttDatetime
	 */
	public AppDatetime addHours(Integer hours) {
		return new AppDatetime(value.addHours(hours));
	}

	/**
	 * 指定した分数を加算する
	 * @param 加算する分数
	 * @return 加算後のAttDatetime
	 */
	public AppDatetime addMinutes(Integer minutes) {
		return new AppDatetime(value.addMinutes(minutes));
	}

	/**
	 * 指定した月数を加算する
	 * @param 加算する月数
	 * @return 加算後のAttDatetime
	 */
	public AppDatetime addMonths(Integer months) {
		return new AppDatetime(value.addMonths(months));
	}

	/**
	 * 指定した秒数を加算する
	 * @param 加算する秒数
	 * @return 加算後のAttDatetime
	 */
	public AppDatetime addSeconds(Integer seconds) {
		return new AppDatetime(value.addSeconds(seconds));
	}

	/**
	 * 指定した年数を加算する
	 * @param 加算する年数
	 * @return 加算後のAttDatetime
	 */
	public AppDatetime addYears(Integer years) {
		return new AppDatetime(value.addYears(years));
	}

	/**
	 * 日時をISO8601形式(UTC, YYYY-MM-DDTHH:mm:ssZ)の文字列として返却する
	 * 例：2017-07-26T13:01:20Z
	 */
	public String format() {
		return value.formatGmt('YYYY-MM-dd\'T\'HH:mm:ss\'Z\'');
	}

	/**
	 * 日時をローカルタイムゾーン, YYYY-MM-DDTHH:mm:ssの文字列として返却する
	 * 例：2017-07-26T13:01:20
	 */
	public String formatLocal() {
		return value.format('YYYY-MM-dd\' \'HH:mm:ss');
	}
	/**
	 * 日時をローカルタイムゾーン, DD-MM-YYYY HH:mm:ssの文字列として返却する
	 * 例：26-07-2017 13:01:20
	 */
	public String formatLocalDDMMYYYY() {
		return value.format('dd-MM-YYYY\' \'HH:mm:ss');
	}

	/**
	 * Datetime型に変換したオブジェクトを取得する
	 */
	public Datetime getDatetime() {
		return Datetime.valueOf(value);
	}
	/**
	 * Date型に変換したオブジェクトを取得する
	 */
	public Date getDate() {
		return this.value.date();
	}
	/**
	 * Time型に変換したオブジェクトを取得する
	 */
	public Time getTime() {
		return this.value.time();
	}
	/**
	 * オブジェクトの値が等しいかどうか判定する
 	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		if (compare instanceof AppDatetime) {
				return this.value == ((AppDatetime)compare).value;
		}
		return false;
	}

	/**
	 * 比較の結果であるinteger値を返します
	 * @param 比較対象
	 * @return このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満
	 */
	public Integer compareTo(Object compareTo) {
		AppDatetime compareDt = (AppDatetime)compareTo;
		Long sub = this.value.getTime() - compareDt.value.getTime();

		Integer retVal;
		if (sub == 0) {
			// 等しいので0を返却
			retVal = 0;
		} else if (sub >= 1) {
			// このインスタンスの値の方が大きいので1を返却
			retVal = 1;
		} else {
			// このインスタンスの値の方が小さいので-1を返却
			retVal = -1;
		}
		return retVal;
	}
	/**
	 * 対象時刻より前かどうかをチェックする
	 * @param 比較対象
	 * @return チェック結果
	 */
	public Boolean isBefore(AppDatetime targetTime) {
		return this.compareTo(targetTime) < 0;
	}
	/** toStringのオーバーライド */
	override public String toString() {
		return this.format();
	}

}
