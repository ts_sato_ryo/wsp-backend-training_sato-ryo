/**
 * @group 勤怠
 *
 * @description 勤務パターン社員一括適用バッチのテスト
 */
@isTest
private class AttPatternEmployeeBatchTest {

	private static final ComMsgBase MESSAGE_L = ComMessage.msg();

	/**
	 * @description テストデータ
	 */
	private class BatchTestData extends TestData.TestDataEntity {

		/**
		 * @description コンストラクタ
		 */
		public BatchTestData() {
			super();
			this.employee.getHistory(0).workingTypeId = this.workingType.id;
			new EmployeeRepository().saveHistoryEntity(this.employee.getHistory(0));
		}

		/**
		 * @description 勤務体系のテストデータを作成する
		 */
		public override AttWorkingTypeBaseEntity createWorkingType(String name) {
			AttWorkingTypeBaseEntity workingType = super.createWorkingType(name);
			// 勤務パターンを設定する
			workingType.getHistory(0).patternCodeList = new List<String>{this.workPattern.code};
			new AttWorkingTypeRepository().saveHistoryEntity(workingType.getHistory(0));
			return workingType;
		}

		/**
		 * @description 勤務パターン社員適用インポートデータを作成する
		 * @param recordNumber 作成するレコード数
		 * @return 作成したインポートデータ
		 */
		public List<AttPatternApplyImportEntity> createAttPatternApplyImportEntityList(Integer recordNumber) {
			Id importBatchId = ComTestDataUtility.createAttImportBatch('test', this.company.id, null, null).Id;
			List<AttPatternApplyImportEntity> entityList = new List<AttPatternApplyImportEntity>();
			for (Integer i = 0; i < recordNumber; i++) {
				AttPatternApplyImportEntity entity = new AttPatternApplyImportEntity();
				entity.importBatchId = importBatchId;
				entity.employeeCode = this.employee.code;
				entity.patternCode = this.workPattern.code;
				entity.shiftDate = AppDate.today().format();
				entity.dayType = 'W';
				entity.importNo = i + 1;
				entityList.add(entity);
			}
			Repository.SaveResult res = new AttPatternApplyImportRepository().saveEntityList(entityList);
			for (Integer i = 0; i < recordNumber; i++) {
				entityList[i].setId(res.details[i].id);
			}
			return entityList;
		}
	}

	/**
	 * @description constructorのテスト
	 * 対象のバッチが見つからない場合、想定されたアプリ例外が発生することを確認する
	 */
	@isTest static void constructorTestNotFoundBatch() {
		BatchTestData testData = new BatchTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternApplyImportEntity applyImport = applyImportList[0];

		App.RecordNotFoundException actEx;
		try {
			Id importBatchId = UserInfo.getUserId();
			Test.startTest();
			AttPatternEmployeeBatch batch = new AttPatternEmployeeBatch(importBatchId);
			Database.executeBatch(batch);
			Test.stopTest();
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.RecordNotFoundExceptionが発生しませんでした');
	}

	/**
	 * @description constructorのテスト
	 * 対象のバッチの処理種別が勤務パターン適用でない場合、想定されたエラーが発生することを確認する
	 */
	@isTest static void constructorTestNotInvalidImportType() {
		BatchTestData testData = new BatchTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternApplyImportEntity applyImport = applyImportList[0];
		Id importBatchId = applyImport.importBatchId;

		AttImportBatchRepository batchRepo = new AttImportBatchRepository();
		AttImportBatchEntity batch = batchRepo.getEntity(importBatchId);
		batch.importType = null;
		batchRepo.saveEntity(batch);

		App.ParameterException actEx;
		try {
			Test.startTest();
			Database.executeBatch(new AttPatternEmployeeBatch(importBatchId));
			Test.stopTest();
		} catch (App.ParameterException e) {
			actEx = e;
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした');
	}

	/**
	 * @description
	 * バッチが正常に処理できることを確認する(成功件数がカウントされている)
	 */
	@isTest static void executeTestSuccessCount() {
		BatchTestData testData = new BatchTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternApplyImportEntity applyImport = applyImportList[0];
		Id importBatchId = applyImport.importBatchId;

		Test.startTest();
		AttPatternEmployeeBatch batch = new AttPatternEmployeeBatch(importBatchId);
		Database.executeBatch(batch);
		Test.stopTest();

		// 検証
		AttImportBatchEntity resBatch = new AttImportBatchRepository().getEntity(importBatchId);
		System.assertNotEquals(null, resBatch.importTime);
		System.assertEquals(ImportBatchStatus.COMPLETED, resBatch.status);
		System.assertEquals(1, resBatch.count);
		System.assertEquals(1, resBatch.successCount);
		System.assertEquals(0, resBatch.failureCount);
		List<AttPatternApplyImportEntity> resImportList = new AttPatternApplyImportRepository().getPatternList(importBatchId);
		AttPatternApplyImportEntity resImport = resImportList[0];
		System.assertEquals(ImportStatus.SUCCESS, resImport.status);
	}

	/**
	 * @description
	 * バッチが正常に処理できることを確認する(失敗件数がカウントされている)
	 */
	@isTest static void executeTestErrorCount() {
		BatchTestData testData = new BatchTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternApplyImportEntity applyImport = applyImportList[0];
		Id importBatchId = applyImport.importBatchId;

		// インポートデータに不正な値を設定する
		applyImport.shiftDate = 'aaaa';
		new AttPatternApplyImportRepository().saveEntity(applyImport);

		Test.startTest();
		AttPatternEmployeeBatch batch = new AttPatternEmployeeBatch(importBatchId);
		Database.executeBatch(batch);
		Test.stopTest();

		// 検証
		AttImportBatchEntity resBatch = new AttImportBatchRepository().getEntity(importBatchId);
		System.assertNotEquals(null, resBatch.importTime);
		System.assertEquals(ImportBatchStatus.COMPLETED, resBatch.status);
		System.assertEquals(1, resBatch.count);
		System.assertEquals(0, resBatch.successCount);
		System.assertEquals(1, resBatch.failureCount);
		List<AttPatternApplyImportEntity> resImportList = new AttPatternApplyImportRepository().getPatternList(importBatchId);
		AttPatternApplyImportEntity resImport = resImportList[0];
		System.assertEquals(ImportStatus.ERROR, resImport.status);
	}

	/**
	 * @description finishのテスト
	 * excecuteでキャッチできない例外が発生してstatusが未処理のインポートレコードが存在している場合は失敗に変更されることを確認する
	 */
	@isTest static void finishTestUpdateImportStatus() {
		BatchTestData testData = new BatchTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(5);
		AttPatternApplyImportEntity applyImport = applyImportList[0];
		Id importBatchId = applyImport.importBatchId;
		applyImportList[2].status = ImportStatus.SUCCESS;
		applyImportList[3].status = ImportStatus.SUCCESS;
		applyImportList[4].status = ImportStatus.SUCCESS;
		new AttPatternApplyImportRepository().saveEntityList(applyImportList);

		// インポートデータに不正な値を設定する
		applyImport.shiftDate = 'aaaa';
		new AttPatternApplyImportRepository().saveEntity(applyImport);


		AttPatternEmployeeBatch batch = new AttPatternEmployeeBatch(importBatchId);
		batch.processCount = applyImportList.size();
		batch.successCount = applyImportList.size() - 2;
		batch.failureCount = 0;

		Test.startTest();
		batch.finish(null);
		Test.stopTest();

		// 検証
		AttImportBatchEntity resBatch = new AttImportBatchRepository().getEntity(importBatchId);
		System.assertNotEquals(null, resBatch.importTime);
		System.assertEquals(ImportBatchStatus.COMPLETED, resBatch.status);
		System.assertEquals(5, resBatch.count);
		System.assertEquals(3, resBatch.successCount);
		System.assertEquals(2, resBatch.failureCount);
		List<AttPatternApplyImportEntity> resImportList = new AttPatternApplyImportRepository().getPatternList(importBatchId);
		AttPatternApplyImportEntity resImport = resImportList[0];
		System.assertEquals(ImportStatus.ERROR, resImport.status);
	}
}