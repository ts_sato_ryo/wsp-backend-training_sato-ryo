/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分ベースエンティティを表すクラスのテスト Test class for ExpTaxTypeBaseEntity
 */
@isTest
private class ExpTaxTypeBaseEntityTest {

	/**
	 * テストデータ Test data
	 */
	private class TestData {

		/** 会社オブジェクト Company object */
		public ComCompany__c companyObj {get; private set;}

		/** コンストラクタ Constructor */
		public TestData() {
			companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
		}

		/**
		 * テスト用の税区分ベースエンティティを作成する Create tax type base entity(without saving to database)
		 */
		public ExpTaxTypeBaseEntity createTaxTypeBaseEntity(String name, String code) {

			ExpTaxTypeBaseEntity baseEntity = new ExpTaxTypeBaseEntity();
			baseEntity.name = name;
			baseEntity.code = code;
			baseEntity.uniqKey = baseEntity.createUniqKey(companyObj.Code__c, code);
			baseEntity.countryId = null;
			baseEntity.companyId = companyObj.id;

			return baseEntity;
		}
	}

	/**
	 * エンティティの複製ができることを確認する Confirm the entity is copied properly
	 */
	@isTest static void copyTest() {
		TestData testData = new TestData();
		ExpTaxTypeBaseEntity taxTypeBase = testData.createTaxTypeBaseEntity('Tax Type', '001');

		ExpTaxTypeBaseEntity copyEntity = taxTypeBase.copy();

		System.assertEquals(taxTypeBase.name, copyEntity.name);
		System.assertEquals(taxTypeBase.code, copyEntity.code);
		System.assertEquals(taxTypeBase.uniqKey, copyEntity.uniqKey);
		System.assertEquals(taxTypeBase.countryId, copyEntity.countryId);
		System.assertEquals(taxTypeBase.companyId, copyEntity.companyId);
	}

	/**
	 * ユニークキーが正しく作成できることを確認する Confirm the unique key is created properly
	 */
	@isTest static void createUniqKeyTest() {
		ExpTaxTypeBaseEntity entity = new ExpTaxTypeBaseEntity();

		// 会社コードと税区分コードが空でない場合 If company code and tax type code are both set
		// ユニークキーが作成される Unique key shoud be created
		entity.code = 'TaxTypeCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode, entity.code));

		// 税区分コードが空の場合 If tax type code is blank
		// 例外が発生する Exception should be thrown
		entity.code = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合 If companuy code is blank
		// 例外が発生する Exception should be thrown
		entity.code = 'TaxTypeCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}
}