/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Resoruce class for expense type group master mainenance
 */
public with sharing class ExpTypeResource {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/*
	 * Foreign Currency Usage Options
	 */
	public enum ForeignCurrencyUsage {
		NOT_USED,
		FLEXIBLE,
		FIXED
	}
	public static final Map<String, ForeignCurrencyUsage> FOREIGN_CURRENCY_USAGE_MAP;
	static {
		final Map<String, ForeignCurrencyUsage> fieldMap = new Map<String, ForeignCurrencyUsage>();
		for (ForeignCurrencyUsage f : ForeignCurrencyUsage.values()) {
			fieldMap.put(f.name(), f);
		}
		FOREIGN_CURRENCY_USAGE_MAP = fieldMap;
	}

	/*
	 * Param for Fixed Allowance Option
	 */
	public class FixedAllowanceOptionParam {
		/** Id */
		public String id;
		/** Label of Fixed Allowance Option (Read ONLY) */
		public String label;
		/** Label of Fixed Allowance Option (L0)*/
		public String label_L0;
		/** Label of Fixed Allowance Option (L1)*/
		public String label_L1;
		/** Label of Fixed Allowance Option (L2)*/
		public String label_L2;
		/** Amount allocated for the Fixed Allowance Option */
		public Decimal allowanceAmount;

		public ExpFixedAllowanceOptionEntity createEntity(Integer order) {
			ExpFixedAllowanceOptionEntity optionEntity = new ExpFixedAllowanceOptionEntity();
			optionEntity.setId(this.id);
			optionEntity.labelL0 = this.label_L0;
			optionEntity.labelL1 = this.label_L1;
			optionEntity.labelL2 = this.label_L2;
			optionEntity.allowanceAmount = this.allowanceAmount;
			optionEntity.order = order;

			return optionEntity;
		}
	}

	/**
	 * @description Request parameter for CreateApi, UpdateApi
	 */
	public class ExpTypeParam extends ExpParam.ExtendedItemConfigParam implements RemoteApi.RequestParam {
		/** ID */
		public String id;
		/** 費目グループ名(翻訳) */
		public String name;
		/** 費目グループ名(L0) */
		public String name_L0;
		/** 費目グループ名(L1) */
		public String name_L1;
		/** 費目グループ名(L2) */
		public String name_L2;
		/** コード */
		public String code;
		/** 会社ID */
		public String companyId;
		/** 有効開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 親費目グループID */
		public String parentGroupId;
		/** 親費目グループ */
		public LookupField parentGroup;
		/** Exp Type Group Code */
		public String parentGroupCode;
		/** 説明(翻訳) */
		public String description;
		/** 説明(L0) */
		public String description_L0;
		/** 説明(L1) */
		public String description_L1;
		/** 説明(L2) */
		public String description_L2;
		/** 領収書添付 */
		public String fileAttachment;
		/** 経費明細タイプ */
		public String recordType;
		/** 費用用途 */
		public String usage;
		/** 税区分1ID */
		public String taxType1Id;
		/** 税区分1 */
		public LookupField taxType1;
		/** 税区分2ID */
		public String taxType2Id;
		/** 税区分2 */
		public LookupField taxType2;
		/** 税区分3ID */
		public String taxType3Id;
		/** 税区分3 */
		public LookupField taxType3;
		/** 並び順 */
		/** Code for Debit Account */
		public String debitAccountCode;
		/** Name of Debit Account */
		public String debitAccountName;
		/** Code for Sub Debit Account */
		public String debitSubAccountCode;
		/** Name of Sub Debit Account */
		public String debitSubAccountName;
		 /** 並び順 */
		public Integer order;
		/** Use Foreign Currency Flag (Reference ONLY) */
		public Boolean useForeignCurrency;
		/** Specifies the Foreign Currency Usage Setting */
		public String foreignCurrencyUsage;
		/** Fixed Foreign Currency Id */
		public String fixedForeignCurrencyId;
		/** Amount for Single Type Fixed Allowance */
		public Decimal fixedAllowanceSingleAmount;
		/** hierarchial parent expense type group name list  */
		public List<String> hierarchyParentNameList;
		/** Child ExpType Ids linked to the ExpType */
		public List<String> expTypeChildIds;
		/** List of Fixed Allowance Options */
		public List<FixedAllowanceOptionParam> fixedAllowanceOptionList;

		/**
		 * @description Create ExpTypeEntity from the request parameter value.
		 * @param paramMap Map of the request parameter
		 * @return Created entity
		 */
		public ExpTypeEntity createEntity(Map<String, Object> paramMap) {
			ExpTypeEntity entity = new ExpTypeEntity();

			entity.setId(this.id);
			if (paramMap.containsKey('name_L0')) {
				entity.nameL0 = this.name_L0;

				// TODO nameは仮置きでnameL0を設定
				entity.name = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('validDateFrom')){
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			// If validFrom is null, set today as default value
			if (entity.validFrom == null) {
				entity.validFrom = AppDate.today();
			}
			if (paramMap.containsKey('validDateTo')) {
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (entity.validTo == null) {
				entity.validTo = ValidPeriodEntityOld.VALID_TO_MAX;
			}
			if (paramMap.containsKey('parentGroupId')) {
				entity.parentGroupId = this.parentGroupId;
			}
			if (paramMap.containsKey('description_L0')) {
				entity.descriptionL0 = this.description_L0;
			}
			if (paramMap.containsKey('description_L1')) {
				entity.descriptionL1 = this.description_L1;
			}
			if (paramMap.containsKey('description_L2')) {
				entity.descriptionL2 = this.description_L2;
			}
			if (paramMap.containsKey('fileAttachment')) {
				entity.fileAttachment = ExpFileAttachment.valueOf(this.fileAttachment);
			}
			if (paramMap.containsKey('recordType')) {
				entity.recordType = ExpRecordType.valueOf(this.recordType);
			}
			if (paramMap.containsKey('usage')) {
				entity.setUsage(this.usage);
			}
			if (paramMap.containsKey('taxType1Id')) {
				entity.taxTypeBase1Id = this.taxType1Id;
			}
			if (paramMap.containsKey('taxType2Id')) {
				entity.taxTypeBase2Id = this.taxType2Id;
			}
			if (paramMap.containsKey('taxType3Id')) {
				entity.taxTypeBase3Id = this.taxType3Id;
			}

			for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				entity.setExtendedItemDateId(i, this.getExtendedItemDateId(i));
				entity.setExtendedItemDateUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemDateUsedIn(i)));
				entity.setExtendedItemDateRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemDateRequiredFor(i)));

				entity.setExtendedItemLookupId(i, this.getExtendedItemLookupId(i));
				entity.setExtendedItemLookupUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemLookupUsedIn(i)));
				entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemLookupRequiredFor(i)));

				entity.setExtendedItemPicklistId(i, this.getExtendedItemPicklistId(i));
				entity.setExtendedItemPicklistUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemPicklistUsedIn(i)));
				entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemPicklistRequiredFor(i)));

				entity.setExtendedItemTextId(i, this.getExtendedItemTextId(i));
				entity.setExtendedItemTextUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemTextUsedIn(i)));
				entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemTextRequiredFor(i)));
			}

			if (paramMap.containsKey('debitAccountCode')) {
				entity.debitAccountCode = this.debitAccountCode;
			}
			if (paramMap.containsKey('debitAccountName')) {
				entity.debitAccountName = this.debitAccountName;
			}
			if (paramMap.containsKey('debitSubAccountCode')) {
				entity.debitSubAccountCode = this.debitSubAccountCode;
			}
			if (paramMap.containsKey('debitSubAccountName')) {
				entity.debitSubAccountName = this.debitSubAccountName;
			}
			if (paramMap.containsKey('order')) {
				entity.order = this.order;
			}
			if (paramMap.containsKey('fixedForeignCurrencyId')) {
				entity.fixedForeignCurrencyId = this.fixedForeignCurrencyId;
			}
			if (paramMap.containsKey('foreignCurrencyUsage')) {
				entity.useForeignCurrency = !this.foreignCurrencyUsage.equals(ExpTypeResource.ForeignCurrencyUsage.NOT_USED.name());
			}
			if (!this.foreignCurrencyUsage.equals(ExpTypeResource.ForeignCurrencyUsage.FIXED.name())) {
				entity.fixedForeignCurrencyId = null;
			}
			if (paramMap.containsKey('fixedAllowanceSingleAmount')) {
				entity.fixedAllowanceSingleAmount = this.fixedAllowanceSingleAmount;
			}
			if (paramMap.containsKey('fixedAllowanceOptionList')) {
				if (this.fixedAllowanceOptionList != null) {
					List<ExpFixedAllowanceOptionEntity> optionEntityList = new List<ExpFixedAllowanceOptionEntity>();
					for (Integer i = 0; i < this.fixedAllowanceOptionList.size(); i++) {
						optionEntityList.add(this.fixedAllowanceOptionList.get(i).createEntity(i));
					}
					entity.replaceFixedAllowanceOptionEntityList(optionEntityList);
				}
			}
			return entity;
		}
	}

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		public String name;
	}

	/**
	 * @description Response parameter
	 */
	public class SaveResponse implements RemoteApi.ResponseParam {
		/** Id of the created record */
		public String id;
	}

	/**
	 * @description Implementation class of expense type group creation API
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_TYPE;

		/**
		 * @description Create a new expense type group
		 * @param req Request parameter(ExpTypeParam)
		 * @return Response parameter(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpTypeParam param = (ExpTypeParam)req.getParam(ExpTypeResource.ExpTypeParam.class);

			// validate request parmeter
			validateParam(param);

			// create entity from request parameter
			ExpTypeEntity entity = param.createEntity(req.getParamMap());

			// save
			Savepoint sp = Database.setSavepoint();
			Id resId;
			try {
				resId = new ExpTypeService().createNewExpType(entity, param.expTypeChildIds);
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			// create response parameter
			ExpTypeResource.SaveResponse res = new ExpTypeResource.SaveResponse();
			res.Id = resId;

			return res;
		}

		/**
		 * Validate request parmeter
		 * @param param Target entity
		 */
		private void validateParam(ExpTypeResource.ExpTypeParam param) {
			// Company ID
 			ExpCommonUtil.validateId('companyId', param.companyId, true);

			// Child ExpType Id List
			ExpCommonUtil.validateIdList('expTypeChildIds', param.expTypeChildIds, false);

			// Validate Record Type - if Record Type is not HotelFee, cannot have child ExpType Links
			if (!param.recordType.equals(ExpRecordType.HOTEL_FEE.value) && param.expTypeChildIds != null && !param.expTypeChildIds.isEmpty()) {
				throw new App.ParameterException(MESSAGE.Exp_Err_CannotHaveExpTypeLinks);
			}

			// Foreign Currency Setting is Required
			if (FOREIGN_CURRENCY_USAGE_MAP.get(param.foreignCurrencyUsage) == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_NullValue(new List<String>{'foreignCurrencyUsage'}));
			}

			// Foreign Currency ID is required if Foreign Currency Usage is fixed
			Boolean isCurrencyIdRequired = FOREIGN_CURRENCY_USAGE_MAP.get(param.foreignCurrencyUsage) == ForeignCurrencyUsage.FIXED;
			ExpCommonUtil.validateId('fixedForeignCurrencyId', param.fixedForeignCurrencyId, isCurrencyIdRequired);

			// Fixed Allowance Options is required if Fixed Allowance Multi
			if (param.recordType.equals(ExpRecordType.FIXED_ALLOWANCE_MULTI.value)) {
				if (param.fixedAllowanceOptionList == null || param.fixedAllowanceOptionList.isEmpty()) {
					throw new App.ParameterException(MESSAGE.Exp_Err_NoFixedAllowanceOption);
				} else {
					for (FixedAllowanceOptionParam optionParam : param.fixedAllowanceOptionList) {
						ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Admin_Lbl_Label, optionParam.label_L0);
						if (optionParam.allowanceAmount == null || optionParam.allowanceAmount <=0) {
							throw new App.ParameterException(MESSAGE.Com_Err_InvalidAmount);
						}
					}
				}
			}
		}
	}

	/**
	 * @description Implementation class of expense type group updating API
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_TYPE;

		/**
		 * @description Update an expense type group
		 * @param req Request parameter(ExpTypeParam)
		 * @return Response parameter(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpTypeParam param = (ExpTypeParam)req.getParam(ExpTypeResource.ExpTypeParam.class);

			// validate request parmeter
			validateParam(param);

			// create entity from request parameter
			ExpTypeEntity entity = param.createEntity(req.getParamMap());

			// save
			new ExpTypeService().updateExpType(entity, param.expTypeChildIds);

			// no response parameter
			return null;
		}

		/**
		 * Validate request parmeter
		 * @param param Target entity
		 */
		private void validateParam(ExpTypeResource.ExpTypeParam param) {
			// ID
			ExpCommonUtil.validateId('ID', param.id, true);

			// Validate Record Type - if Record Type is not HotelFee, cannot have child ExpType Links
			if (!param.recordType.equals(ExpRecordType.HOTEL_FEE.value) && param.expTypeChildIds != null && !param.expTypeChildIds.isEmpty()) {
				throw new App.ParameterException(MESSAGE.Exp_Err_CannotHaveExpTypeLinks);
			}
			// Foreign Currency Setting is Required
			if (FOREIGN_CURRENCY_USAGE_MAP.get(param.foreignCurrencyUsage) == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_NullValue(new List<String>{'foreignCurrencyUsage'}));
			}

			// Foreign Currency ID is required if Foreign Currency Usage is fixed
			Boolean isCurrencyIdRequired = FOREIGN_CURRENCY_USAGE_MAP.get(param.foreignCurrencyUsage) == ForeignCurrencyUsage.FIXED;
			ExpCommonUtil.validateId('fixedForeignCurrencyId', param.fixedForeignCurrencyId, isCurrencyIdRequired);

			// Fixed Allowance Options is required if Fixed Allowance Multi
			if (param.recordType.equals(ExpRecordType.FIXED_ALLOWANCE_MULTI.value)) {
				if (param.fixedAllowanceOptionList == null || param.fixedAllowanceOptionList.isEmpty()) {
					throw new App.ParameterException(MESSAGE.Exp_Err_NoFixedAllowanceOption);
				} else {
					for (FixedAllowanceOptionParam optionParam : param.fixedAllowanceOptionList) {
						ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Admin_Lbl_Label, optionParam.label_L0);
						if (optionParam.allowanceAmount == null || optionParam.allowanceAmount <=0) {
							throw new App.ParameterException(MESSAGE.Com_Err_InvalidAmount);
						}
					}
				}
			}
		}
	}

	/**
	 * @description Request parameter for DeleteApi
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** Record id to be deleted */
		public String id;

		/**
		 * Validate request parameter
		 */
		public void validate() {
			// ID
			ExpCommonUtil.validateId('id', this.id, true);
		}
	}

	/**
	 * @description Implementation class of expense type group deleting API
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_TYPE;

		/**
		 * @description Delete an expense type group
		 * @param req Request parameter(DeleteRequest)
		 * @return Response parameter(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpTypeResource.DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);

			// validate request parmeter
			param.validate();

			// delete
			Savepoint sp = Database.setSavepoint();
			try {
				// NOTE: Validation of the links between Parent and Child ExpTypes will be done in the ExpValidator upon submission for approval
				(new ExpTypeService()).deleteExpType(param.id);
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			// no response parameter
			return null;
		 }
	}

	/**
	 * @description Request parameter for SearchApi
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** Expense Type Name */
		public String name;
		/** 関連する会社レコードID */
		public String companyId;
		/** Accounting Period End Date*/
		public String endDate;
		/** Accounting Period Start Date */
		public String startDate;
		/** 対象日 */
		public Date targetDate;
		/** Get with Extended Item Info */
		public Boolean withExtendedItems = false;
		/** Record Type */
		public String recordType;
		/** File Attachment */
		public String fileAttachment;
		/** Specifies whether to filter the Extended Items or Not */
		public String usedIn;
		/** Expense Type Code */
		public String code;
		/** Expense Group Code */
		public String expGroupCode;
		/** Expense Group Name */
		public String expGroupName;
		/** Report Type Id */
		public String expReportTypeId;
		/** Expense Type Name or Code */
		public String query;
		/** With Parent Hierarchy (Mobile set false) */
		public Boolean withParentHierarchy;
		/** Filter Expense Type based on User's Employee Group associated. Cannot be used together with expReportTypeId param. */
		public Boolean filterByEmployeeGroup;
		/** Foreign Currency Usage */
		public String foreignCurrencyUsage;
		/** Currency Id for Fixed Foreign Currency */
		public String fixedForeignCurrencyId;

		/**
		 * Validate request parameter
		 */
		public void validate() {

			// ID
			ExpCommonUtil.validateId('id', this.id, false);

			// 会社ID
			ExpCommonUtil.validateId('companyId', this.companyId, false);

			// 明細タイプ
			if (String.isNotBlank(this.recordType)) {
				if (ExpRecordType.valueOf(this.recordType) == null) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'recordType'}));
				}
			}

			// 領収書添付
			if (String.isNotBlank(this.fileAttachment)) {
				if (ExpFileAttachment.valueOf(this.fileAttachment) == null) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'fileAttachment'}));
				}
			}

			if (String.isNotBlank(this.usedIn)) {
				ExpCommonUtil.validateUsedIn('usedIn', this.usedIn);
			}

			// Fixed Foreign Currency ID
			ExpCommonUtil.validateId('fixedForeignCurrencyId', this.fixedForeignCurrencyId, false);
			ExpCommonUtil.validateDate('endDate', this.endDate, false);
			ExpCommonUtil.validateDate('startDate', this.startDate, false);
		}
	}

	public class RecentlyUsedRequest implements RemoteApi.RequestParam {
		public String employeeBaseId;
		public String targetDate;
		public String usedIn;
		public String companyId;
		public String recordType;
		public String reportTypeId;

		/**
		 * Validate request parameter
		 */
		public void validate() {
			ExpCommonUtil.validateId('employeeBaseId', this.employeeBaseId, true);
			if (String.isNotEmpty(this.targetDate)) {
				ExpCommonUtil.validateDate('targetDate', this.targetDate);
			}
			if (String.isNotEmpty(this.usedIn)) {
				ExpCommonUtil.validateUsedIn('usedIn', this.usedIn);
			}
			ExpCommonUtil.validateId('companyId', this.companyId, true);
			if (String.isNotEmpty(this.recordType) && ExpRecordType.valueOf(this.recordType) == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'recordType'}));
			}
			// When the FE give null reportTypeId user error msg is shown
			if (this.reportTypeId == null) {
				throw new App.ParameterException(MESSAGE.Exp_Err_SelectValidItem(new List<String>{MESSAGE.Exp_Lbl_ReportType}));
			} else {
				ExpCommonUtil.validateId('reportTypeId', this.reportTypeId, false);
			}
		}
	}

	/**
	 * @description Response parameter for SearchApi
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** Search result */
		public ExpTypeParam[] records;
	}

	/**
	 * @description Implementation class of expense type group searching API
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search expense type group records
		 * @param req Request parameter(SearchRequest)
		 * @return Response parameter(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// convert request parameter from Map to SearchRequest
			SearchRequest param = (ExpTypeResource.SearchRequest)req.getParam(ExpTypeResource.SearchRequest.class);
			Map<String, Object> paramMap = req.getParamMap();

			// validate request parmeter
			param.validate();

			// create search filter condition
			ExpTypeRepository.SearchFilter filter = new ExpTypeRepository.SearchFilter();
			// if parameter 'id' is set, add id to condition
			if (String.isNotEmpty(param.id)) {
				filter.ids = new Set<Id>{param.id};
			}
			// if parameter 'name' is set, add id to condition
			if (String.isNotEmpty(param.name)) {
				filter.name = param.name;
			}
			// if parameter 'companyId' is set, add company id to condition
			if (String.isNotEmpty(param.companyId)) {
				filter.companyIds = new Set<Id>{param.companyId};
			}
			// if parameter 'endDate' is set, add enddate to condition
			if (String.isNotEmpty(param.endDate)) {
				filter.endDate = AppDate.valueOf(param.endDate);
			}
			// if parameter 'startDate' is set, add startdate to condition
			if (String.isNotEmpty(param.startDate)) {
				filter.startDate = AppDate.valueOf(param.startDate);
			}
			// if parameter 'targetDate' is set, add date to condition
			if (param.targetDate != null) {
				filter.targetDate = AppDate.valueOf(param.targetDate);
			}
			// if parameter 'recordType' is set, add record type to condition
			if (paramMap.containsKey('recordType')) {
				filter.recordType = ExpRecordType.valueOf(param.recordType);
			}
			// if parameter 'fileAttachment' is set, add file attachment to condition
			if (paramMap.containsKey('fileAttachment')) {
				filter.fileAttachment = ExpFileAttachment.valueOf(param.fileAttachment);
			}
			// if parameter 'code' is set
			if (String.isNotEmpty(param.code)) {
				filter.code = param.code;
			}
			// if parameter 'expGroupCode' is set
			if (String.isNotEmpty(param.expGroupCode)) {
				filter.expGroupCode = param.expGroupCode;
			}
			// if parameter 'expGroupCode' is set
			if (paramMap.containsKey('expGroupName') && String.isNotEmpty(param.expGroupName)) {
				filter.expGroupName = param.expGroupName;
			}
			if (String.isNotEmpty(param.query)) {
				filter.query = param.query;
			}

			if (paramMap.containsKey('foreignCurrencyUsage') && param.foreignCurrencyUsage != null) {
				filter.useForeignCurrency = !param.foreignCurrencyUsage.equals(ExpTypeResource.ForeignCurrencyUsage.NOT_USED.name());
				if (param.foreignCurrencyUsage.equals(ExpTypeResource.ForeignCurrencyUsage.FIXED.name())) {
					if (String.isNotBlank(param.fixedForeignCurrencyId)) {
						filter.fixedForeignCurrencyIds = new Set<Id>{param.fixedForeignCurrencyId};
					}
				}
			}

			ExpTypeService service = new ExpTypeService();

			if (param.filterByEmployeeGroup == true) {
				filter.ids = service.getAvailableExpTypeIdsForEmployeeGroup(filter.targetDate, filter.startDate, filter.endDate);
			}

			// if parameter 'expReportTypeId' is set
			if (paramMap.containsKey('expReportTypeId') && String.isNotEmpty(param.expReportTypeId)) {
				filter.ids = service.getAvailableExpTypeFromReportType(new Set<Id>{param.expReportTypeId});
			}

			List<ExpTypeParam> records = new List<ExpTypeParam>();

			// search
			List<ExpTypeEntity> entityList = service.searchExpType(filter, param.usedIn, param.withExtendedItems, true);

			// 税区分のエンティティを取得
			Map<Id, ExpTaxTypeHistoryEntity> taxTypeHistoryMap = service.getTaxTypeHistory(entityList, filter.targetDate);
			Map<Id, List<string>> hierarchyParentNamesMap = null;
			if (param.withParentHierarchy != null && param.withParentHierarchy) {
				hierarchyParentNamesMap  = service.getParentHierarchyRemoveInvalidExpense(entityList, filter.targetDate, filter.companyIds);
			}

			// convert search results to response format
			for (ExpTypeEntity entity : entityList) {
				records.add(createExpTypeParam(entity, taxTypeHistoryMap, hierarchyParentNamesMap));
			}

			// create response parameter
			SearchResponse res = new SearchResponse();
			res.records = records;

			return res;
		}
	}

	/**
	 * @description Implementation class of expense type group searching API
	 */
	public with sharing class GetRecentlyUsedList extends RemoteApi.ResourceBase {

		/**
		 * @description Search expense type group records
		 * @param req Request parameter(SearchRequest)
		 * @return Response parameter(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// convert request parameter from Map to SearchRequest
			RecentlyUsedRequest param = (ExpTypeResource.RecentlyUsedRequest)req.getParam(ExpTypeResource.RecentlyUsedRequest.class);

			// validate request parameter
			param.validate();

			ExpRecordType recordType = ExpRecordType.valueOf(param.recordType);
			// if targetDate is not defined considered today
			AppDate targetDate = String.isEmpty(param.targetDate) ? AppDate.today() : AppDate.valueOf(param.targetDate);
			// search
			ExpTypeService service = new ExpTypeService();
			List<ExpTypeEntity> entityList = service.getRecentlyUsedExpenseTypeList(param.employeeBaseId, recordType, targetDate, param.reportTypeId, param.usedIn);

			Map<Id, ExpTaxTypeHistoryEntity> taxTypeHistoryMap = service.getTaxTypeHistory(entityList, targetDate);
			Map<Id, List<string>> hierarchyParentNamesMap = new ExpTypeService().getParentHierarchyRemoveInvalidExpense(entityList, targetDate, new Set<Id>{param.companyId});

			List<ExpTypeParam> records = new List<ExpTypeParam>();
			// convert search results to response format
			for (ExpTypeEntity entity : entityList) {
				records.add(createExpTypeParam(entity, taxTypeHistoryMap, hierarchyParentNamesMap));
			}

			// create response parameter
			SearchResponse res = new SearchResponse();
			res.records = records;

			return res;
		}
	}

	/**
	 * @description Request parameter for SearchLinkApi
	 */
	public class SearchLinkRequest implements RemoteApi.RequestParam {
		/** レコードID */
		public String parentId;
		/** 対象日 */
		public Date targetDate;
		/** Get with Extended Item Info */
		public Boolean withExtendedItems = false;
		/** Specifies whether to filter the Extended Items or Not */
		public String usedIn;

		/**
		 * Validate request parameter
		 */
		public void validate() {

			// ParentID
			ExpCommonUtil.validateId('parentId', this.parentId, true);

			// TargetDate
			ExpCommonUtil.validateNotNull('targetDate', targetDate);

			// UsedIn
			if (String.isNotBlank(this.usedIn)) {
				ExpCommonUtil.validateUsedIn('usedIn', this.usedIn);
			}
		}
	}

	/**
	 * @description Response parameter for SearchLinkApi
	 */
	public class SearchLinkResponse implements RemoteApi.ResponseParam {
		/** Search result */
		public ExpTypeParam[] childExpTypeList;
	}

	/**
	 * @description Implementation class of child expense type searching API
	 */
	public with sharing class SearchLinkApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search child expense type records based on parent expense type Id
		 * @param req Request parameter(SearchRequest)
		 * @return Response parameter(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// convert request parameter from Map to SearchLinkRequest
			SearchLinkRequest param = (ExpTypeResource.SearchLinkRequest)req.getParam(ExpTypeResource.SearchLinkRequest.class);
			Map<String, Object> paramMap = req.getParamMap();

			// validate request parmeter
			param.validate();

			// create search filter condition
			ExpTypeExpTypeLinkRepository.SearchFilter filter = new ExpTypeExpTypeLinkRepository.SearchFilter();
			filter.expTypeParentIds = new Set<Id>{param.parentId};
			filter.targetDate = AppDate.valueOf(param.targetDate);
			ExpTypeService service = new ExpTypeService();

			// search
			List<ExpTypeEntity> entitylist = service.searchChildExpType(filter, param.usedIn, param.withExtendedItems);

			// 税区分のエンティティを取得 Get Tax History Entities
			Map<Id, ExpTaxTypeHistoryEntity> taxTypeHistoryMap = service.getTaxTypeHistory(entitylist, filter.targetDate);

			// convert search results to response format
			List<ExpTypeParam> records = new List<ExpTypeParam>();
			for (ExpTypeEntity entity : entitylist) {
				ExpTypeParam response = createExpTypeParam(entity, taxTypeHistoryMap, new Map<Id, List<String>>()); // map is init to give empty list
				records.add(response);
			}

			// create response parameter
			SearchLinkResponse res = new SearchLinkResponse();
			res.childExpTypeList = records;

			return res;
		}

	}

	/**
	 * Convert ExpTypeEntity to ExpTypeParam
	 * @param entity Source entity
	 * @param taxTypeHistoryMap Map of ExpTaxTypeHistoryEntity(key:tax type base ID)
	 * @param hierarchyParentNamesMap hierarchy Map of ParentNames(key: expense type id, value: parent expense type group name list)
	 * @return Converted param data
	 */
	private static ExpTypeParam createExpTypeParam(ExpTypeEntity entity, Map<Id, ExpTaxTypeHistoryEntity> taxTypeHistoryMap, Map<Id, List<string>> hierarchyParentNamesMap) {
		ExpTypeParam param = new ExpTypeParam();

		param.id = entity.id;
		param.code = entity.code;
		param.name = entity.nameL.getValue();
		param.name_L0 = entity.nameL0;
		param.name_L1 = entity.nameL1;
		param.name_L2 = entity.nameL2;
		param.companyId = entity.companyId;
		param.parentGroupId = entity.parentGroupId;
		param.parentGroup = new LookupField();
		if (entity.parentGroup != null) {
			param.parentGroup.name = entity.parentGroup.nameL.getValue();
			param.parentGroupCode = entity.parentGroup.code;
		}
		param.description = entity.descriptionL.getValue();
		param.description_L0 = entity.descriptionL0;
		param.description_L1 = entity.descriptionL1;
		param.description_L2 = entity.descriptionL2;
		param.order = entity.order;
		param.usage = entity.usage.name();
		param.fileAttachment = ExpFileAttachment.getValue(entity.fileAttachment);
		param.recordType = ExpRecordType.getValue(entity.recordType);
		param.taxType1Id = entity.taxTypeBase1Id;
		param.taxType1 = new LookupField();
		if (entity.taxTypeBase1Id != null && taxTypeHistoryMap.containsKey(entity.taxTypeBase1Id)) {
			param.taxType1.name = taxTypeHistoryMap.get(entity.taxTypeBase1Id).nameL.getValue();
		}
		param.taxType2Id = entity.taxTypeBase2Id;
		param.taxType2 = new LookupField();
		if (entity.taxTypeBase2Id != null && taxTypeHistoryMap.containsKey(entity.taxTypeBase2Id)) {
			param.taxType2.name = taxTypeHistoryMap.get(entity.taxTypeBase2Id).nameL.getValue();
		}
		param.taxType3Id = entity.taxTypeBase3Id;
		param.taxType3 = new LookupField();
		if (entity.taxTypeBase3Id != null && taxTypeHistoryMap.containsKey(entity.taxTypeBase3Id)) {
			param.taxType3.name = taxTypeHistoryMap.get(entity.taxTypeBase3Id).nameL.getValue();
		}

		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			param.setExtendedItemDateId(i, entity.getExtendedItemDateId(i));
			param.setExtendedItemDateUsedIn(i, getExtendedItemDetail(entity.getExtendedItemDateUsedIn(i)));
			param.setExtendedItemDateRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemDateRequiredFor(i)));
			param.setExtendedItemDateInfo(i, entity.getExtendedItemDateInfo(i));

			param.setExtendedItemLookupId(i, entity.getExtendedItemLookupId(i));
			param.setExtendedItemLookupUsedIn(i, getExtendedItemDetail(entity.getExtendedItemLookupUsedIn(i)));
			param.setExtendedItemLookupRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemLookupRequiredFor(i)));
			param.setExtendedItemLookupInfo(i, entity.getExtendedItemLookupInfo(i));

			param.setExtendedItemPicklistId(i, entity.getExtendedItemPicklistId(i));
			param.setExtendedItemPicklistUsedIn(i, getExtendedItemDetail(entity.getExtendedItemPicklistUsedIn(i)));
			param.setExtendedItemPicklistRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemPicklistRequiredFor(i)));
			param.setExtendedItemPicklistInfo(i, entity.getExtendedItemPicklistInfo(i));

			param.setExtendedItemTextId(i, entity.getExtendedItemTextId(i));
			param.setExtendedItemTextUsedIn(i, getExtendedItemDetail(entity.getExtendedItemTextUsedIn(i)));
			param.setExtendedItemTextRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemTextRequiredFor(i)));
			param.setExtendedItemTextInfo(i, entity.getExtendedItemTextInfo(i));
		}
		param.debitAccountCode = entity.debitAccountCode;
		param.debitAccountName = entity.debitAccountName;
		param.debitSubAccountCode = entity.debitSubAccountCode;
		param.debitSubAccountName = entity.debitSubAccountName;
		param.validDateFrom = AppConverter.dateValue(entity.validFrom);
		param.validDateTo = AppConverter.dateValue(entity.validTo);
		param.useForeignCurrency = entity.useForeignCurrency;
		param.fixedForeignCurrencyId = entity.fixedForeignCurrencyId;
		if (entity.useForeignCurrency == true) {
			param.foreignCurrencyUsage = entity.fixedForeignCurrencyId == null ? ForeignCurrencyUsage.FLEXIBLE.name() : ForeignCurrencyUsage.FIXED.name();
		} else {
			param.foreignCurrencyUsage = ForeignCurrencyUsage.NOT_USED.name();
		}
		param.fixedAllowanceSingleAmount = entity.fixedAllowanceSingleAmount;
		param.expTypeChildIds = entity.expTypeChildIdList;
		param.fixedAllowanceOptionList = new List<FixedAllowanceOptionParam>();
		for (ExpFixedAllowanceOptionEntity allowanceOptionEntity : entity.fixedAllowanceOptionEntityList) {
			FixedAllowanceOptionParam allowanceOptionParam = new FixedAllowanceOptionParam();
			allowanceOptionParam.id = allowanceOptionEntity.id;
			allowanceOptionParam.allowanceAmount = allowanceOptionEntity.allowanceAmount;
			allowanceOptionParam.label = allowanceOptionEntity.labelL.getValue();
			allowanceOptionParam.label_L0 = allowanceOptionEntity.labelL0;
			allowanceOptionParam.label_L1 = allowanceOptionEntity.labelL1;
			allowanceOptionParam.label_L2 = allowanceOptionEntity.labelL2;
			param.fixedAllowanceOptionList.add(allowanceOptionParam);
		}

		List<String> hierarchyParentNameList;
		if (hierarchyParentNamesMap != null) {
			hierarchyParentNameList = hierarchyParentNamesMap.get(entity.id);
			if (hierarchyParentNameList == null) {
				hierarchyParentNameList = new List<String>();
			}
		}
		param.hierarchyParentNameList = hierarchyParentNameList;

		return param;
	}

	/*
	 * Retrieves the value of ExtendedItemUsedIn and ExtendedItemRequiredFor entity fields
	 */
	private static String getExtendedItemDetail(ValueObjectType entityValue) {
		return (entityValue == null) ? null : entityValue.value;
	}
}