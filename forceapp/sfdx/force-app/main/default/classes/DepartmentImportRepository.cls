/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 部署インポートのリポジトリ
 */
public with sharing class DepartmentImportRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ComDepartmentImport__c.Id,
			ComDepartmentImport__c.Name,
			ComDepartmentImport__c.ImportBatchId__c,
			ComDepartmentImport__c.RevisionDate__c,
			ComDepartmentImport__c.Code__c,
			ComDepartmentImport__c.CompanyCode__c,
			ComDepartmentImport__c.Name_L0__c,
			ComDepartmentImport__c.Name_L1__c,
			ComDepartmentImport__c.Name_L2__c,
			ComDepartmentImport__c.ManagerBaseCode__c,
			ComDepartmentImport__c.AssistantManager1Code__c,
			ComDepartmentImport__c.AssistantManager2Code__c,
			ComDepartmentImport__c.ParentBaseCode__c,
			ComDepartmentImport__c.HistoryComment__c,
			ComDepartmentImport__c.Status__c,
			ComDepartmentImport__c.Error__c
		};
	}

	/** 検索フィルタ */
	 private class Filter {
		/** インポートバッチID */
		public Id importBatchId;
		/** インポートID */
		public List<Id> importIdList;
		/** ステータス */
		public List<String> status;
	 }

	/**
	 * 部署インポートエンティティを取得する
	 * @param importId インポートID
	 * @return 部署インポートエンティティ
	 */
	public DepartmentImportEntity getEntity(Id importId) {
		// 検索実行
		List<DepartmentImportEntity> entityList = getEntityList(new List<Id>{importId});

		DepartmentImportEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * 部署インポートエンティティのリストを取得する
	 * @param importIdList 部署インポートIDのリスト
	 * @return 条件に一致した部署インポートエンティティのリスト
	 */
	public List<DepartmentImportEntity> getEntityList(List<Id> importIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.importIdList = importIdList;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, null, forUpdate);
	}

	/**
	 * 指定したインポートバッチIDで処理対象の部署インポートエンティティのリストを取得する
	 * @param importBatchId インポートバッチID
	 * @param status 部署インポートデータのステータス
	 * @param max 取得上限件数
	 * @return 条件に一致した部署インポートエンティティのリスト
	 */
	public List<DepartmentImportEntity> getEntityListByImportBatchId(Id importBatchId, List<String> status, Integer max) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.importBatchId = importBatchId;
		filter.status = status;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, max, forUpdate);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(DepartmentImportEntity entity) {
		return saveEntityList(new List<DepartmentImportEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<DepartmentImportEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<DepartmentImportEntity> entityList, Boolean allOrNone) {
		List<ComDepartmentImport__c> objList = new List<ComDepartmentImport__c>();

		// エンティティからSObjectを作成する
		for (DepartmentImportEntity entity : entityList) {
			ComDepartmentImport__c obj = createObj(entity);
			objList.add(obj);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(objList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 部署インポートを検索する
	 * @param filter 検索条件
	 * @param max 取得上限件数
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した部署インポートエンティティのリスト
	 */
	 private List<DepartmentImportEntity> searchEntity(Filter filter, Integer max, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id importBatchId = filter.importBatchId;
		final List<Id> importIds = filter.importIdList;
		final List<String> status = filter.status;
		condList.add(getFieldName(ComDepartmentImport__c.ImportBatchId__c) + ' = :importBatchId');
		if ((importIds != null) && (!importIds.isEmpty())) {
			condList.add(getFieldName(ComDepartmentImport__c.Id) + ' IN :importIds');
		}
		if ((status != null) && (!status.isEmpty())) {
			condList.add(getFieldName(ComDepartmentImport__c.Status__c) + ' IN :status');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComDepartmentImport__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (max != null) {
			soql += ' LIMIT ' + max;
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('DepartmentImportRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<DepartmentImportEntity> entityList = new List<DepartmentImportEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ComDepartmentImport__c)sobj));
		}

		return entityList;
	 }

	/**
	 * ComDepartmentImport__cオブジェクトからDepartmentImportEntityを作成する
	 * @param obj 作成元のComDepartmentImport__cオブジェクト
	 * @return 作成したDepartmentImportEntity
	 */
	private DepartmentImportEntity createEntity(ComDepartmentImport__c obj) {
		DepartmentImportEntity entity = new DepartmentImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.code = obj.Code__c;
		entity.companyCode = obj.CompanyCode__c;
		entity.name_L0 = obj.Name_L0__c;
		entity.name_L1 = obj.Name_L1__c;
		entity.name_L2 = obj.Name_L2__c;
		entity.managerBaseCode = obj.ManagerBaseCode__c;
		entity.assistantManager1Code = obj.AssistantManager1Code__c;
		entity.assistantManager2Code = obj.AssistantManager2Code__c;
		entity.parentBaseCode = obj.ParentBaseCode__c;
		entity.historyComment = obj.HistoryComment__c;
		entity.status = ImportStatus.valueOf(obj.Status__c);
		entity.error = obj.Error__c;

		entity.resetChanged();
		return entity;
	}

	/**
	 * DepartmentImportEntityからComDepartmentImport__cオブジェクトを作成する
	 * @param entity 作成元のDepartmentImportEntity
	 * @return 作成したComDepartmentImport__cオブジェクト
	 */
	private ComDepartmentImport__c createObj(DepartmentImportEntity entity) {
		ComDepartmentImport__c retObj = new ComDepartmentImport__c();

		retObj.Id = entity.id;
		if (entity.isChanged(DepartmentImportEntity.Field.ImportBatchId)) {
			retObj.ImportBatchId__c = entity.importBatchId;
		}
		if (entity.isChanged(DepartmentImportEntity.Field.Status)) {
			retObj.Status__c = entity.status.value;
		}
		if (entity.isChanged(DepartmentImportEntity.Field.Error)) {
			retObj.Error__c = entity.error;
		}

		return retObj;
	}
}
