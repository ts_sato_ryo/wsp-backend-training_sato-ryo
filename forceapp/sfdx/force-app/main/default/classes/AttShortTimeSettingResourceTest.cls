/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttShortTimeSettingResourceのテストクラス
 */
@isTest
private class AttShortTimeSettingResourceTest {

	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

		/**
		 * 短時間勤務設定を作成する
		 */
		public AttShortTimeSettingBaseEntity createShortSetting(String name) {
			AttShortTimeWorkSettingBase__c settingObj =
					ComTestDataUtility.createAttShortTimeSettingWithHistory(name, this.company.id, this.tag.id);
			return new AttShortTimeSettingRepository().getEntity(settingObj.Id);
		}
	}

	/**
	 * 短時間勤務設定レコードを1件作成する(正常系)
	 * DBに保存正しく保存されていることを確認する
	 */
	@isTest
	static void createTestSave() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		Date currentDate = Date.today();

		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.code = '001';
		param.companyId = testData.company.Id;
		param.reasonId = testData.tag.id;
		param.workSystem = AttWorkSystem.JP_FLEX.value;
		param.validDateFrom = currentDate - 30;
		param.validDateTo = currentDate;
		param.comment = 'テストコメント';
		param.allowableTimeOfShortWork = 300;
		param.allowableTimeOfLateArrival = 120;
		param.allowableTimeOfEarlyLeave = 180;
		param.allowableTimeOfIrregularRest = 240;

		Test.startTest();
		AttShortTimeSettingResource.CreateApi api = new AttShortTimeSettingResource.CreateApi();
		AttShortTimeSettingResource.CreateResponse res = (AttShortTimeSettingResource.CreateResponse)api.execute(param);
		Test.stopTest();

		System.assertNotEquals(null, res.id);

		// レコードが作成されている
		AttShortTimeSettingBaseEntity resBase = new AttShortTimeSettingRepository().getEntity(res.Id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(1, resBase.getHistoryList().size());
		AttShortTimeSettingHistoryEntity resHistory = resBase.getHistory(0);

		// ベースレコード値の検証
		System.assertEquals(param.code, resBase.code);
		System.assertEquals(testData.company.code + '-' + param.code, resBase.uniqKey);
		System.assertEquals(param.companyId, resBase.companyId);
		System.assertEquals(resHistory.Id, resBase.currentHistoryId);
		System.assertEquals(param.reasonId, resBase.reasonId);
		System.assertEquals(param.workSystem, resBase.workSystem.value);

		// 履歴レコード値の検証
		System.assertEquals(resBase.id, resHistory.baseId);
		String uniqKey = resBase.uniqKey + '-' +
							new AppDate(param.validDateFrom).formatYYYYMMDD() + '-' +
							new AppDate(param.validDateTo).formatYYYYMMDD();
		System.assertEquals(uniqKey, resHistory.uniqKey);
		System.assertEquals(param.name_L0, resHistory.nameL0);
		System.assertEquals(param.name_L1, resHistory.nameL1);
		System.assertEquals(param.name_L2, resHistory.nameL2);
		System.assertEquals(param.validDateFrom, resHistory.validFrom.getDate());
		System.assertEquals(param.validDateTo, resHistory.validTo.getDate());
		System.assertEquals(param.comment, resHistory.historyComment);
		System.assertEquals(param.allowableTimeOfShortWork, resHistory.allowableTimeOfShortWork);
		System.assertEquals(param.allowableTimeOfLateArrival, resHistory.allowableTimeOfLateArrival);
		System.assertEquals(param.allowableTimeOfEarlyLeave, resHistory.allowableTimeOfEarlyLeave);
		// 現在、フレックスの私用外出は0で初期化される（https://teamspiritdev.atlassian.net/browse/GENIE-11333）
		System.assertEquals(0, resHistory.allowableTimeOfIrregularRest);
	}

	/**
	 * 短時間勤務設定レコードを1件作成する(正常系)
	 * パラメータにデフォルト値が正しく適用されることを確認する
	 */
	@isTest
	static void createTestDefaultValue() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		Date currentDate = Date.today();


		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.code = '001';
		param.companyId = testData.company.Id;
		param.reasonId = testData.tag.id;
		param.workSystem = AttWorkSystem.JP_FLEX.value;
		param.validDateFrom = null;
		param.validDateTo = null;
		param.comment = 'テストコメント';
		param.allowableTimeOfShortWork = 300;
		param.allowableTimeOfLateArrival = 120;
		param.allowableTimeOfEarlyLeave = 180;
		param.allowableTimeOfIrregularRest = 240;

		Test.startTest();
		AttShortTimeSettingResource.CreateApi api = new AttShortTimeSettingResource.CreateApi();
		AttShortTimeSettingResource.CreateResponse res = (AttShortTimeSettingResource.CreateResponse)api.execute(param);
		Test.stopTest();

		System.assertNotEquals(null, res.id);

		// レコードが作成されている
		AttShortTimeSettingBaseEntity resBase = new AttShortTimeSettingRepository().getEntity(res.id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(1, resBase.getHistoryList().size());
		AttShortTimeSettingHistoryEntity resHistory = resBase.getHistory(0);

		// 関係のある項目のみ確認
		System.assertEquals(Date.today(), resHistory.validFrom.getDate());
		System.assertEquals(Date.newInstance(2101, 1, 1), resHistory.validTo.getDate());
	}

	/**
	 * 短時間勤務設定レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		ResourceTestData testData = new ResourceTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.name_L0 = 'テスト設定_L0';
		param.code = '001';
		param.companyId = testData.company.Id;
		param.reasonId = testData.tag.id;
		param.workSystem = AttWorkSystem.JP_FLEX.value;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttShortTimeSettingResource.CreateApi api = new AttShortTimeSettingResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 短時間勤務設定レコードを1件更新する(正常系)
	 * DBに保存正しく保存されていることを確認する
	 */
	@isTest
	static void updateTestSave() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		TagEntity updateTag = testData.createTag('Test時短理由_Update', TagType.SHORTEN_WORK_REASON);
		AttShortTimeSettingBaseEntity targetBase = testData.createShortSetting('Test');

		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.id = targetBase.id;
		param.code = '001_Update';
		param.companyId = testData.company.Id;
		param.reasonId = updateTag.id;
		param.workSystem = AttWorkSystem.JP_FIX.value;

		Test.startTest();
		AttShortTimeSettingResource.UpdateApi api = new AttShortTimeSettingResource.UpdateApi();
		api.execute(param);
		Test.stopTest();

		// 更新後のレコードを取得
		AttShortTimeSettingBaseEntity resBase = new AttShortTimeSettingRepository().getEntity(targetBase.id);
		System.assertNotEquals(null, resBase);
		AttShortTimeSettingHistoryEntity resHistory = resBase.getHistory(0);

		// ベースレコード値の検証
		System.assertEquals(param.code, resBase.code);
		System.assertEquals(testData.company.code + '-' + param.code, resBase.uniqKey);
		System.assertEquals(param.companyId, resBase.companyId);
		System.assertEquals(resHistory.id, resBase.currentHistoryId);
		System.assertEquals(param.reasonId, resBase.reasonId);
		System.assertEquals(param.workSystem, resBase.workSystem.value);
	}

	/**
	 * 短時間勤務設定レコードを1件更新する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		ResourceTestData testData = new ResourceTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		AttShortTimeSettingBaseEntity targetBase = testData.createShortTimeSetting('Update', testData.tag.id);

		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.id = targetBase.id;
		param.code = '001_Update';

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttShortTimeSettingResource.CreateApi api = new AttShortTimeSettingResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 短時間勤務設定レコードを1件削除する(異常系)
	 * 削除対象のidが正しくない場合、アプリ例外が発生することを確認する
	 */
	@isTest
	static void updateNegativeTestInvalidId() {
		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		TagEntity updateTag = testData.createTag('Test時短理由_Update', TagType.SHORTEN_WORK_REASON);

		// Test1: idがnullの場合
		try {
			AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
			param.id = null;
			param.code = '001_Update';
			param.companyId = testData.company.Id;
			param.reasonId = updateTag.id;

			AttShortTimeSettingResource.UpdateApi api = new AttShortTimeSettingResource.UpdateApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}

		// Test2: idがIDではない文字列の場合
		try {
			AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
			param.id = 'aaaa';
			param.code = '001_Update';
			param.companyId = testData.company.Id;
			param.reasonId = updateTag.id;

			AttShortTimeSettingResource.UpdateApi api = new AttShortTimeSettingResource.UpdateApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}
	}

	/**
	 * 短時間勤務設定レコードを1件削除する(正常系)
	 * 指定したIDのレコードが削除できることを確認する
	 */
	@isTest
	static void deleteTestDelete() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		AttShortTimeSettingBaseEntity targetBase = testData.createShortSetting('Test');
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);

		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.id = targetBase.id;

		Test.startTest();
		AttShortTimeSettingResource.DeleteApi api = new AttShortTimeSettingResource.DeleteApi();
		api.execute(param);
		Test.stopTest();

		// レコードは物理削除されている
		AttShortTimeSettingBaseEntity resBase = new AttShortTimeSettingRepository().getEntity(targetBase.id);
		System.assertEquals(null, resBase);
	}

	/**
	 * 短時間勤務設定レコードを1件削除する(正常系)
	 * 削除済みのレコードのIDを指定した場合、エラーが発生しないことを確認する
	 */
	@isTest
	static void deleteTestRemovedRecord() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		AttShortTimeSettingBaseEntity targetBase = testData.createShortSetting('Test');
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);

		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.id = targetBase.id;

		// 削除してしまう
		new AttShortTimeSettingRepository().deleteEntity(targetBase);

		Test.startTest();
		AttShortTimeSettingResource.DeleteApi api = new AttShortTimeSettingResource.DeleteApi();
		api.execute(param);
		Test.stopTest();

		// レコードは物理削除されている
		AttShortTimeSettingBaseEntity resBase = new AttShortTimeSettingRepository().getEntity(targetBase.id);
		System.assertEquals(null, resBase);
	}

	/**
	 * 短時間勤務設定レコードを1件削除する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		ResourceTestData testData = new ResourceTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		AttShortTimeSettingBaseEntity targetBase = testData.createShortTimeSetting('Update', testData.tag.id);

		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
		param.id = targetBase.id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttShortTimeSettingResource.DeleteApi api = new AttShortTimeSettingResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 短時間勤務設定レコードを1件削除する(異常系)
	 * 削除対象のidが正しくない場合、想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void deleteNegativeTestInvalidId() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		AttShortTimeSettingBaseEntity targetBase = testData.createShortSetting('Test');
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);

		// Test1: IDがnullの場合
		try {
			AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
			param.id = null;

			AttShortTimeSettingResource.DeleteApi api = new AttShortTimeSettingResource.DeleteApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}

		// Test2: IDがIDではない文字列の場合
		try {
			AttShortTimeSettingResource.Setting param = new AttShortTimeSettingResource.Setting();
			param.id = 'aaa';

			AttShortTimeSettingResource.DeleteApi api = new AttShortTimeSettingResource.DeleteApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}
	}

	/**
	 * 短時間勤務設定レコードを検索する(正常系)
	 * 指定したベースIDのレコードが取得できることを確認する
	 */
	@isTest
	static void searchTestById() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		AttShortTimeSettingBaseEntity targetBase = testData.createShortSetting('Test');
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);
		AttShortTimeSettingBaseEntity targetBase2 = testData.createShortSetting('Test 2');
		AttShortTimeSettingHistoryEntity targetHistory2 = targetBase2.getHistory(0);

		AttShortTimeSettingResource.SearchRequest param = new AttShortTimeSettingResource.SearchRequest();
		param.id = targetBase2.id;

		Test.startTest();
		AttShortTimeSettingResource.SearchApi api = new AttShortTimeSettingResource.SearchApi();
		AttShortTimeSettingResource.SearchResponse res = (AttShortTimeSettingResource.SearchResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(1, res.records.size());
		AttShortTimeSettingResource.Setting record = res.records[0];

		// ベースレコード値の検証
		System.assertEquals(targetBase2.id, record.id);
		System.assertEquals(targetBase2.code, record.code);
		System.assertEquals(targetBase2.companyId, record.companyId);
		System.assertEquals(targetBase2.reasonId, record.reasonId);
		System.assertEquals(targetHistory2.id, record.historyId);
		System.assertEquals(targetHistory2.nameL.getValue(), record.name);
		System.assertEquals(targetHistory2.nameL0, record.name_L0);
		System.assertEquals(targetHistory2.nameL1, record.name_L1);
		System.assertEquals(targetHistory2.nameL2, record.name_L2);
		System.assertEquals(targetHistory2.validFrom.getDate(), record.validDateFrom);
		System.assertEquals(targetHistory2.validTo.getDate(), record.validDateTo);
		System.assertEquals(targetHistory2.historyComment, record.comment);
		System.assertEquals(targetHistory2.allowableTimeOfShortWork, record.allowableTimeOfShortWork);
		System.assertEquals(targetHistory2.allowableTimeOfLateArrival, record.allowableTimeOfLateArrival);
		System.assertEquals(targetHistory2.allowableTimeOfEarlyLeave, record.allowableTimeOfEarlyLeave);
		System.assertEquals(targetHistory2.allowableTimeOfIrregularRest, record.allowableTimeOfIrregularRest);
	}

	/**
	 * 短時間勤務設定レコードを検索する(正常系)
	 * 指定した会社IDのレコードが取得できることを確認する
	 */
	@isTest
	static void searchTestByCompanyId() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		AttShortTimeSettingBaseEntity targetBase = testData.createShortSetting('Test');
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);
		AttShortTimeSettingBaseEntity targetBase2 = testData.createShortSetting('Test 2');
		AttShortTimeSettingHistoryEntity targetHistory2 = targetBase2.getHistory(0);

		// targetBaseの会社IDを変更しておく
		CompanyEntity company = testData.createCompany('SearchTest');
		targetBase.companyId = company.id;
		new AttShortTimeSettingRepository().saveBaseEntity(targetBase);

		AttShortTimeSettingResource.SearchRequest param = new AttShortTimeSettingResource.SearchRequest();
		param.companyId = targetBase2.companyId;

		Test.startTest();
		AttShortTimeSettingResource.SearchApi api = new AttShortTimeSettingResource.SearchApi();
		AttShortTimeSettingResource.SearchResponse res = (AttShortTimeSettingResource.SearchResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(1, res.records.size());
		AttShortTimeSettingResource.Setting record = res.records[0];

		// 対象のレコードが取得できているかを確認
		System.assertEquals(targetBase2.id, record.id);
	}

	/**
	 * 短時間勤務設定レコードを検索する(正常系)
	 * 指定した日付に有効なレコードが取得できることを確認する
	 */
	@isTest
	static void searchTestByTargetDate() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		AttShortTimeSettingBaseEntity targetBase = testData.createShortSetting('Test');
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);
		AttShortTimeSettingBaseEntity targetBase2 = testData.createShortSetting('Test 2');
		AttShortTimeSettingHistoryEntity targetHistory2 = targetBase2.getHistory(0);

		AttShortTimeSettingResource.SearchApi api = new AttShortTimeSettingResource.SearchApi();
		AttShortTimeSettingResource.SearchResponse res;
		AttShortTimeSettingResource.SearchRequest param;

		// 有効な履歴レコードが存在する日を指定
		param = new AttShortTimeSettingResource.SearchRequest();
		param.id = targetBase2.id;
		param.targetDate = targetHistory2.validFrom.getDate();
		res = (AttShortTimeSettingResource.SearchResponse)api.execute(param);

		System.assertEquals(1, res.records.size());
		AttShortTimeSettingResource.Setting record = res.records[0];
		System.assertEquals(targetBase2.id, record.id);
		System.assertEquals(targetHistory2.id, record.historyId);

		// 有効な履歴レコードが存在しない日を指定
		param = new AttShortTimeSettingResource.SearchRequest();
		param.id = targetBase2.id;
		param.targetDate = targetHistory2.validFrom.getDate().addDays(-1);
		res = (AttShortTimeSettingResource.SearchResponse)api.execute(param);
		System.assertEquals(0, res.records.size());

	}

	/**
	 * 短時間勤務設定レコードを検索する(正常系)
	 * 指定した社員IDに紐づく勤務体系の労働時間制とマッチするレコードが取得できることを確認する
	 */
	@isTest
	static void searchTestByEmployeeId() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		// 労働時間制がフレックス労働制の短時間勤務設定を作成
		AttShortTimeSettingBaseEntity shortTimeBase1 = testData.createShortSetting('フレックス労働制');
		shortTimeBase1.workSystem = AttWorkSystem.JP_FLEX;
		AttShortTimeSettingHistoryEntity shortTimeHis1 = shortTimeBase1.getHistory(0);
		shortTimeHis1.validFrom = AppDate.valueOf('2019-04-01');
		shortTimeHis1.validTo = AppDate.valueOf('2020-04-01');

		// 労働時間制が固定時間労働制の短時間勤務設定を作成
		AttShortTimeSettingBaseEntity shortTimeBase2 = testData.createShortSetting('固定時間労働制');
		shortTimeBase2.workSystem = AttWorkSystem.JP_FIX;
		AttShortTimeSettingHistoryEntity shortTimeHis2 = shortTimeBase2.getHistory(0);
		shortTimeHis2.validFrom = AppDate.valueOf('2019-04-01');
		shortTimeHis2.validTo = AppDate.valueOf('2020-04-01');

		new AttShortTimeSettingRepository().saveEntityList(new List<AttShortTimeSettingBaseEntity>{shortTimeBase1, shortTimeBase2});

		// 社員の有効期間を修正して、固定労働時間制の勤務体系を設定
		EmployeeHistoryEntity empHis = testData.employee.getHistory(0);
		empHis.validFrom = AppDate.valueOf('2019-04-01');
		empHis.validTo = AppDate.valueOf('2020-04-01');
		empHis.workingTypeId = testData.workingType.id;
		new EmployeeRepository().saveEntity(testData.employee);

		AttWorkingTypeBaseEntity workingTypeBase = new AttWorkingTypeRepository().getEntity(empHis.workingTypeId);
		workingTypeBase.workSystem = AttWorkSystem.JP_FIX;
		new AttWorkingTypeRepository().saveEntity(workingTypeBase);

		AttShortTimeSettingResource.SearchApi api = new AttShortTimeSettingResource.SearchApi();
		AttShortTimeSettingResource.SearchResponse res;
		AttShortTimeSettingResource.SearchRequest param;

		// 社員と短時間勤務設定が有効な日付を指定
		param = new AttShortTimeSettingResource.SearchRequest();
		param.employeeId = testData.employee.id;
		param.targetDate = empHis.validFrom.getDate();
		res = (AttShortTimeSettingResource.SearchResponse)api.execute(param);

		// 検証
		System.assertEquals(1, res.records.size());
		AttShortTimeSettingResource.Setting record = res.records[0];
		System.assertEquals(shortTimeBase2.id, record.id);
		System.assertEquals(shortTimeHis2.id, record.historyId);
	}

	/**
	 * 短時間勤務設定レコードを検索する(異常系)
	 * 指定した社員IDの社員が指定日に有効期間外の場合、異常終了することを確認する
	 */
	@isTest
	static void searchTestByEmployeeIdInvalidPeriod() {

		AttShortTimeSettingResourceTest.ResourceTestData testData = new ResourceTestData();
		// 労働時間制がフレックス労働制の短時間勤務設定を作成
		AttShortTimeSettingBaseEntity shortTimeBase1 = testData.createShortSetting('フレックス労働制');
		shortTimeBase1.workSystem = AttWorkSystem.JP_FLEX;
		AttShortTimeSettingHistoryEntity shortTimeHis1 = shortTimeBase1.getHistory(0);
		shortTimeHis1.validFrom = AppDate.valueOf('2019-04-01');
		shortTimeHis1.validTo = AppDate.valueOf('2020-04-01');
		new AttShortTimeSettingRepository().saveEntity(shortTimeBase1);

		// 社員の有効期間を修正して、フレックス労働制の勤務体系を設定
		EmployeeHistoryEntity empHis = testData.employee.getHistory(0);
		empHis.validFrom = AppDate.valueOf('2019-04-01');
		empHis.validTo = AppDate.valueOf('2020-04-01');
		empHis.workingTypeId = testData.workingType.id;
		new EmployeeRepository().saveEntity(testData.employee);

		AttWorkingTypeBaseEntity workingTypeBase = new AttWorkingTypeRepository().getEntity(empHis.workingTypeId);
		workingTypeBase.workSystem = AttWorkSystem.JP_FLEX;
		new AttWorkingTypeRepository().saveEntity(workingTypeBase);

		AttShortTimeSettingResource.SearchApi api = new AttShortTimeSettingResource.SearchApi();
		AttShortTimeSettingResource.SearchResponse res;
		AttShortTimeSettingResource.SearchRequest param;

		// 社員の有効期間外の日付を指定
		param = new AttShortTimeSettingResource.SearchRequest();
		param.employeeId = testData.employee.id;
		param.targetDate = empHis.validFrom.addDays(-1).getDate();
		try {
			res = (AttShortTimeSettingResource.SearchResponse)api.execute(param);
		} catch (App.RecordNotFoundException e) {
			System.assertEquals(
					ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Employee}), e.getMessage());
		}
	}
}
