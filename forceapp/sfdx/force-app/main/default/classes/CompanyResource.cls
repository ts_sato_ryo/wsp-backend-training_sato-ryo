/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 会社レコード操作APIを実装するクラス
 */
public with sharing class CompanyResource {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @description 会社レコードを表すクラス
	 */
	public class Company implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 会社名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 会社名(言語0) */
		public String name_L0;
		/** 会社名(言語1) */
		public String name_L1;
		/** 会社名(言語2) */
		public String name_L2;
		/** 会社コード */
		public String code;
		/** 国レコードID */
		public String countryId;
		/** 国ルックアップ */
		public LookupField country;
		/** Currency record ID */
		public String currencyId;
		/** Currency look up */
		public LookupField currencyField;
		/** 言語 */
		public String language;
		/** 表示用言語 */
		public String languageName;
		/** 勤怠機能が利用可 */
		public Boolean useAttendance;
		/** 経費申請機能が利用可 */
		public Boolean useExpense;
		/** Indicator Flag for Expense Request (advance application) function availability */
		public Boolean useExpenseRequest;
		/** 工数管理機能が利用可 */
		public Boolean useWorkTime;
		/** プランナー機能が利用可 */
		public Boolean usePlanner;
		/** プランナーデフォルト表示 */
		public String plannerDefaultView;
		/** ジョルダン定期区間取り扱い */
		public String jorudanCommuterPass;
		/** 会社の税区分マスタを仕様 */
		public Boolean useCompanyTaxMaster;
		/** ジョルダン運賃種別 */
		public String jorudanFareType;
		/** ジョルダン高速バス利用 */
		public String jorudanHighwayBus;
		/** ジョルダンエリア優先 */
		public String jorudanAreaPreference;
		/** ジョルダン有料特急利用 */
		public String jorudanUseChargedExpress;
		/** ジョルダン有料特急利用距離 */
		public String jorudanChargedExpressDistance;
		/** ジョルダン優先座席 */
		public String jorudanSeatPreference;
		/** ジョルダン路線表示順 */
		public String jorudanRouteSort;
		/** Allow to Modify Tax Amount */
		public Boolean allowTaxAmountChange;
	}

	/**
	 * @description レコードのルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目 */
		public String name;
		/** レコードのCode項目 */
		public String code;
	}

	/**
	 * @description 会社レコード作成or更新結果レスポンス
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したレコードID */
		public String id;
	}

	/**
	 * @description 会社レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description 会社レコードを1件作成する
		 * @param  req リクエストパラメータ(Company)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// #以下の場合は例外 (RemoteApi.TypeException) が発生する
			//   ・データ型が一致しないパラメータがある
			CompanyResource.Company param = (CompanyResource.Company)req.getParam(CompanyResource.Company.class);
			Repository.SaveResult sr = CompanyResource.saveRecord(param, req.getParamMap(), false);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			Id companyId = sr.details[0].Id;

			// 会社のデフォルトカレンダーを作成
			CalendarService calService = new CalendarService();
			Id calendarId = calService.createCompanyDefaultCalendar(companyId, param.code, CalendarType.ATTENDANCE);

			// 作成したカレンダーのIDを会社にセットして更新
			CompanyEntity company = new CompanyEntity();
			company.setId(companyId);
			company.calendarId = calendarId;
			new CompanyRepository().saveEntity(company);

			// 成功レスポンスをセットする
			SaveResult res = new SaveResult();
			res.id = companyId;
			return res;
		}
	}

	/**
	 * @description 会社レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description 会社レコードを1件更新する
		 * @param  req リクエストパラメータ(Company)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			CompanyResource.Company param = (CompanyResource.Company)req.getParam(CompanyResource.Company.class);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			CompanyResource.saveRecord(param, req.getParamMap(), true);

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 会社レコードを1件作成or更新する<br/>
	 *   ※このメソッドは他のクラスから参照しないでください<br/>
	 *   TODO: DAOに外出しする
	 * @param  param 会社レコード値を含むパラメータ
	 * @param  isUpdate レコード更新時はtrue、作成時はfalse
	 * @return  作成or更新した会社レコードID
	 */
	public static Repository.SaveResult saveRecord(Company param, Map<String, Object> paramMap, Boolean isUpdate){

		CompanyRepository repo = new CompanyRepository();

		if (isUpdate && String.isBlank(param.id)) {
			throw new App.ParameterException('パラメータ "id" を指定してください');
		}
		// 会社レコードの値をセットする
		CompanyEntity entity;
		if (isUpdate) {
			// 更新の場合、既存のエンティティを取得する
			entity = repo.getEntity(param.id);
			if (entity == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
		} else {
			// 新規の場合
			entity = new CompanyEntity();
		}

		// ジョルダン有料特急利用距離が最大値より大きい
		if (String.isNotBlank(param.jorudanChargedExpressDistance)) {
			AppParser.validateParseInteger(ComMessage.msg().Admin_Lbl_JorudanChargedExpressDistance, param.jorudanChargedExpressDistance.trim(), ExpTransitJorudanService.USE_CHARGED_DISTANCE_MAX);
		}

		Boolean changeCurrencyId = false;

		if (isUpdate) {
			entity.setId(param.id);
		}
		if(paramMap.containsKey('name_L0')) {
			entity.name = param.name_L0;
			entity.nameL0 = param.name_L0;
		}
		if(paramMap.containsKey('name_L1')) {
			entity.nameL1 = param.name_L1;
		}
		if(paramMap.containsKey('name_L2')) {
			entity.nameL2 = param.name_L2;
		}
		if(paramMap.containsKey('code')){
			entity.code = param.code;
		}
		if(paramMap.containsKey('countryId')){
			entity.countryId = param.countryId;
		}
		if(paramMap.containsKey('currencyId')){
			if(entity.currencyId != param.currencyId){
				changeCurrencyId = true;
			}
			entity.currencyId = param.currencyId;
		}
		if(paramMap.containsKey('language')){
			entity.language = AppLanguage.valueOf(param.language);
		}
		if(paramMap.containsKey('useAttendance')){
			entity.useAttendance = param.useAttendance;
		}
		if(paramMap.containsKey('useExpense')){
			entity.useExpense = param.useExpense;
		} else if (!isUpdate) {
			entity.useExpense = true;
		}
		if(paramMap.containsKey('useExpenseRequest')){
			entity.useExpenseRequest = param.useExpenseRequest;
		} else if (!isUpdate) {
			entity.useExpenseRequest = true;
		}
		if(paramMap.containsKey('useWorkTime')){
			entity.useWorkTime = param.useWorkTime;
		}
		if(paramMap.containsKey('usePlanner')){
			entity.usePlanner = param.usePlanner;
		}
		if(paramMap.containsKey('useCompanyTaxMaster')){
			entity.useCompanyTaxMaster = param.useCompanyTaxMaster;
		} else if (!isUpdate) {
			entity.useCompanyTaxMaster = true;
		}
		if(paramMap.containsKey('plannerDefaultView')){
			entity.plannerDefaultView = PlannerViewType.valueOf(param.plannerDefaultView);
		}
		if(paramMap.containsKey('jorudanFareType')){
			entity.jorudanFareType = AppConverter.intValue(param.jorudanFareType);
		}
		if(paramMap.containsKey('jorudanAreaPreference')){
			entity.jorudanAreaPreference = param.jorudanAreaPreference;
		}
		if(paramMap.containsKey('jorudanUseChargedExpress')){
			entity.jorudanUseChargedExpress = AppConverter.intValue(param.jorudanUseChargedExpress);
		}
		if(paramMap.containsKey('jorudanChargedExpressDistance')){
			entity.jorudanChargedExpressDistance = AppConverter.intValue(param.jorudanChargedExpressDistance);
		}
		if(paramMap.containsKey('jorudanSeatPreference')){
			entity.jorudanSeatPreference = AppConverter.intValue(param.jorudanSeatPreference);
		}
		if(paramMap.containsKey('jorudanRouteSort')){
			entity.jorudanRouteSort = AppConverter.intValue(param.jorudanRouteSort);
		}
		if(paramMap.containsKey('jorudanCommuterPass')){
			entity.jorudanCommuterPass = AppConverter.intValue(param.jorudanCommuterPass);
		}
		if(paramMap.containsKey('jorudanHighwayBus')){
			entity.jorudanHighwayBus = AppConverter.intValue(param.jorudanHighwayBus);
		}
		if(paramMap.containsKey('allowTaxAmountChange')){
			entity.allowTaxAmountChange = param.allowTaxAmountChange;
		} else if (!isUpdate) {
			entity.allowTaxAmountChange = false;
		}
		Validator.Result validResult = (new ComConfigValidator.CompanyValidator(entity)).validate();
		if (! validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// 基準通貨は、既にレポートが存在している場合更新できない / base currency cannot be updated if there is at least one report
		if(isUpdate && entity.useExpense && changeCurrencyId){
			ExpReportRepository reportRepo = new ExpReportRepository();
			if(reportRepo.checkExistEntity(param.id)){
				throw new App.ParameterException(MESSAGE.Exp_Err_BaseCurrencyCannnotChanged);
			}
		}
		// Not allows to change the base currency if there is any existing pre-request Expense application
		if(isUpdate && entity.useExpenseRequest && changeCurrencyId){
			ExpRequestService service = new ExpRequestService();
			if(service.expenseRequestApplicationsExist(param.id)){
				throw new App.ParameterException(MESSAGE.Exp_Err_BaseCurrencyCannnotChanged);
			}
		}
		// コードの重複チェック
		if (CompanyResource.isExistCode(entity)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		return new CompanyRepository().saveEntity(entity);
	}

	/**
	 * @description 会社レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;
	}

	/**
	 * 会社レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description 会社レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new CompanyRepository().deleteEntity(param.Id);
			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

					// TODO 暫定的にデバッグログを出力しておきます
					System.debug(e);

					// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
					//      暫定対応として下記のようなエラーメッセージで対応します。
					// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
					e.setMessage(ComMessage.msg().Com_Err_FaildDeleteReference);
					throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		}

	}

	/**
	 * @description 会社レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
	}

	/**
	 * @description 会社レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public Company[] records;
	}

	/**
	 * @description 会社レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 会社レコードを検索する
		 * @param  req リクエストパラメータ(SearchCondition)
		 * @return レスポンス(SearchResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);
			Map<String, Object> paramMap = req.getParamMap();

			CompanyRepository.SearchFilter filter = new CompanyRepository.SearchFilter();
			// パラメータ id が指定されている場合は、idで検索
			if(paramMap.containsKey('id')){
				filter.companyIds = new List<Id>{param.Id};
			}

			// 検索する
			List<CompanyEntity> searchRecords = new CompanyRepository().searchEntityList(filter);

			// レスポンスクラスに変換
			List<Company> companyList = new List<Company>();
			for(CompanyEntity sr : searchRecords) {
				Company c = new Company();
				c.id = sr.Id;
				c.name = sr.nameL.getValue();
				c.name_L0 = sr.nameL0;
				c.name_L1 = sr.nameL1;
				c.name_L2 = sr.nameL2;
				c.code = sr.code;
				c.countryId = sr.countryId;
				c.country = new LookupField();
				if (sr.countryNameL != null) {
					c.country.name = sr.countryNameL.getValue();
				}
				c.currencyId = sr.currencyId;
				c.currencyField = new LookupField();
				if (sr.currencyInfo != null) {
					c.currencyField.name = sr.currencyInfo.nameL.getValue();
					c.currencyField.code = sr.currencyInfo.code;
				}
				c.language = AppConverter.stringValue(sr.language);
				if (sr.language != null) {
					c.languageName = sr.language.label;
				}
				c.useAttendance = sr.useAttendance;
				c.useExpense = sr.useExpense;
				c.useExpenseRequest = sr.useExpenseRequest;
				c.useWorkTime = sr.useWorkTime;
				c.usePlanner = sr.usePlanner;
				c.plannerDefaultView = sr.plannerDefaultView == null ? PlannerViewType.WEEKLY.value : sr.plannerDefaultView.value;
				c.useCompanyTaxMaster = sr.useCompanyTaxMaster;

				c.jorudanFareType = String.valueOf(sr.jorudanFareType);
				c.jorudanAreaPreference = sr.jorudanAreaPreference;
				c.jorudanUseChargedExpress = String.valueOf(sr.jorudanUseChargedExpress);
				c.jorudanChargedExpressDistance = String.valueOf(sr.jorudanChargedExpressDistance);
				c.jorudanSeatPreference = String.valueOf(sr.jorudanSeatPreference);
				c.jorudanRouteSort = String.valueOf(sr.jorudanRouteSort);
				c.jorudanCommuterPass = String.valueOf(sr.jorudanCommuterPass);
				c.jorudanHighwayBus = String.valueOf(sr.jorudanHighwayBus);

				c.allowTaxAmountChange = sr.allowTaxAmountChange;

				companyList.add(c);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = companyList;
			return res;
		}
	}

	/**
	 * @description コードが重複していないことを確認する
	 * @param entity 登録対象のエンティティ
	 * @return true(重複あり) / false(重複なし)
	 */
	private static boolean isExistCode(CompanyEntity entity) {
		CompanyRepository repo = new CompanyRepository();
		List<CompanyEntity> result = repo.getEntityList(new List<String>{entity.code});
		if (result.isEmpty()) {
			return false;
		}
		return result[0].id != entity.id;
	}
}

