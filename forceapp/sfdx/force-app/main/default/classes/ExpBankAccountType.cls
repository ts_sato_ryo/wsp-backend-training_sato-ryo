/*
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Bank Account Type pick list
 */
public with sharing class ExpBankAccountType extends ValueObjectType {

	/** Checking Account */
	public static final ExpBankAccountType CHECKING = new ExpBankAccountType('Checking');
	/** Savings Account */
	public static final ExpBankAccountType SAVINGS = new ExpBankAccountType('Savings');

	/** List of the entries */
	public static final List<ExpBankAccountType> TYPE_LIST;
	static {
		TYPE_LIST = new List<ExpBankAccountType> {
			CHECKING,
			SAVINGS
		};
	}

	/**
   * Constructor
   * @param value Value of the pick list entry
   */
	private ExpBankAccountType(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
   * @param The key value to get the instance
   * @return The instance having the value specified
	 */
	public static ExpBankAccountType valueOf(String value) {
		ExpBankAccountType retType = null;
		if (String.isNotBlank(value)) {
			for (ExpBankAccountType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {
		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ExpBankAccountType) {
			if (this.value == ((ExpBankAccountType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}

}