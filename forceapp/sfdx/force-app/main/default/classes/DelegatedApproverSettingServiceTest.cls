/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description Test class for DelegatedApproverSettingService
 */
@isTest
private class DelegatedApproverSettingServiceTest {

	/**
	 * 代理承認者設定一覧取得処理のテスト Test for getting delegated approver setting list
	 */
	@isTest
	static void getSettingListTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComDeptBase__c departmentObj = ComTestDataUtility.createDepartmentsWithHistory('Department', companyObj.Id, 1)[0];
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, departmentObj.Id, UserInfo.getUserId(), permissionObj.Id);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, departmentObj.Id, UserInfo.getUserId(), permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[2].Code__c = '003';
		update delegatedApproverObjs;

		List<ComDelegatedApproverSetting__c> settings = new List<ComDelegatedApproverSetting__c>();
		for (Integer i = 0; i < delegatedApproverObjs.size(); i++) {
			ComDelegatedApproverSetting__c setting = ComTestDataUtility.createDelegatedApproverSetting(employeeObj.Id, delegatedApproverObjs[i].Id);
			settings.add(setting);
		}

		Test.startTest();

		List<DelegatedApproverSettingEntity> result = new DelegatedApproverSettingService().getSettingList(employeeObj.Id);

		Test.stopTest();

		// 検証
		System.assertEquals(delegatedApproverObjs.size(), result.size());
		for (Integer i = 0; i < result.size(); i++) {
			System.assertEquals(settings[i].EmployeeBaseId__c, result[i].employeeBaseId);
			System.assertEquals(settings[i].DelegatedApproverBaseId__c, result[i].delegatedApproverBaseId);
			System.assertEquals(settings[i].ApproveExpenseRequestByDelegate__c, result[i].canApproveExpenseRequestByDelegate);
			System.assertEquals(settings[i].ApproveExpenseReportByDelegate__c, result[i].canApproveExpenseReportByDelegate);
		}
	}

	/**
	 * 代理承認者設定保存処理のテスト Test for saving delegated approver setting list
	 */
	@isTest
	static void saveSettingListTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		companyObj.Language__c = 'ja';
		update companyObj;
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		User managerUserObj = ComTestDataUtility.createUser('ManagerUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c managerObj = ComTestDataUtility.createEmployeeWithHistory('Manager', companyObj.Id, null, managerUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		// 社員に上長を設定
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(employeeObj.Id, AppDate.today());
		employee.getHistory(0).managerId = managerObj.Id;
		new EmployeeRepository().saveEntity(employee);

		ExpTestData testData = new ExpTestData();
		// 事前申請データ
		ExpRequestEntity expRequest = testData.createExpRequest(Date.today(), null, employee, null);
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(expRequest.id, 'Test');
		// 経費精算データ
		ExpReportEntity expReport = testData.createExpReport(Date.today(), null, employee, null);
		ExpReportRequestService expReportRequestService = new ExpReportRequestService();
		expReportRequestService.isTestModeAndBypassExpReportValidation = true;
		expReportRequestService.submitRequest(expReport.id, 'Test');

		List<DelegatedApproverSettingEntity> settings = new List<DelegatedApproverSettingEntity>();
		settings.add(createEntity(managerObj.Id, delegatedApproverObjs[0].Id));
		settings.add(createEntity(managerObj.Id, delegatedApproverObjs[1].Id));
		settings.add(createEntity(managerObj.Id, delegatedApproverObjs[2].Id));

		EmployeeBaseEntity manager = new EmployeeService().getEmployee(managerObj.Id, AppDate.today());

		Test.startTest();

		DelegatedApproverSettingService service = new DelegatedApproverSettingService();
		service.saveSettingList(manager, settings);

		Test.stopTest();

		// 代理承認者設定が保存されているかの確認
		List<DelegatedApproverSettingEntity> result = new DelegatedApproverSettingService().getSettingList(managerObj.Id);
		System.assertEquals(settings.size(), result.size());
		for (Integer i = 0; i < result.size(); i++) {
			System.assertEquals(settings[i].employeeBaseId, result[i].employeeBaseId);
			System.assertEquals(settings[i].delegatedApproverBaseId, result[i].delegatedApproverBaseId);
			System.assertEquals(settings[i].canApproveExpenseRequestByDelegate, result[i].canApproveExpenseRequestByDelegate);
			System.assertEquals(settings[i].canApproveExpenseReportByDelegate, result[i].canApproveExpenseReportByDelegate);
		}

		// 既存の事前申請が代理承認者に共有されているかの確認
		ExpRequestApprovalRepository expReqRepo = new ExpRequestApprovalRepository();
		ExpRequestApprovalEntity expRequestApproval = expReqRepo.getEntityListFromRequestId(new Set<Id>{expRequest.id})[0];
		List<AppShareEntity> expReqShareList = expReqRepo.getShareEntityListByApprovalId(new List<Id>{expRequestApproval.id});
		Map<Id, AppShareEntity> expReqShareMapByApproverId = new Map<Id, AppShareEntity>();
		for (AppShareEntity share : expReqShareList) {
			expReqShareMapByApproverId.put(share.userOrGroupId, share);
		}
		for (Integer i = 0; i < settings.size(); i++) {
			AppShareEntity share = expReqShareMapByApproverId.get(delegatedApproverUserObj[i].Id);
			// TODO : Fix test case
			//System.assertEquals(expRequestApproval.id, share.parentId);
		}

		// 既存の経費申請が代理承認者に共有されているかの確認
		ExpReportRequestRepository expRepRepo = new ExpReportRequestRepository();
		ExpReportRequestEntity expReportRequest = expRepRepo.getEntityListFromReportId(new List<Id>{expReport.id})[0];
		List<AppShareEntity> expRepShareList = expRepRepo.getShareEntityListByRequestId(new List<Id>{expReportRequest.id});
		Map<Id, AppShareEntity> expRepShareMapByApproverId = new Map<Id, AppShareEntity>();
		for (AppShareEntity share : expRepShareList) {
			expRepShareMapByApproverId.put(share.userOrGroupId, share);
		}
		for (Integer i = 0; i < settings.size(); i++) {
			AppShareEntity share = expRepShareMapByApproverId.get(delegatedApproverUserObj[i].Id);
			// TODO : Fix test case
			// System.assertEquals(expReportRequest.id, share.parentId);
		}
	}

	/**
	 * 代理承認者設定保存処理のテスト Test for saving delegated approver setting list
	 * Ensure save updates records in parameter list and deletes existing records not defined in pameter list
	 */
	@isTest
	static void saveSettingListTestUpsert() {
		// データ登録 Initalise Data
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		companyObj.Language__c = 'ja';
		update companyObj;
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		User managerUserObj = ComTestDataUtility.createUser('ManagerUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c managerObj = ComTestDataUtility.createEmployeeWithHistory('Manager', companyObj.Id, null, managerUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 4);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 4);
		// 社員コードが昇順になるように更新 Update Employee Code in ascending order
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		delegatedApproverObjs[3].Code__c = '004';
		delegatedApproverObjs[3].UserId__c = delegatedApproverUserObj[3].Id;
		update delegatedApproverObjs;

		// 社員に上長を設定 Set managers
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(employeeObj.Id, AppDate.today());
		employee.getHistory(0).managerId = managerObj.Id;
		new EmployeeRepository().saveEntity(employee);

		ExpTestData testData = new ExpTestData();
		// 事前申請データ Set ExpRequest Data
		ExpRequestEntity expRequest = testData.createExpRequest(Date.today(), null, employee, null);
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(expRequest.id, 'Test');
		// 経費精算データ Set ExpReport Data
		ExpReportEntity expReport = testData.createExpReport(Date.today(), null, employee, null);
		ExpReportRequestService expReportRequestService = new ExpReportRequestService();
		expReportRequestService.isTestModeAndBypassExpReportValidation = true;
		expReportRequestService.submitRequest(expReport.id, 'Test');

		EmployeeBaseEntity manager = new EmployeeService().getEmployee(managerObj.Id, AppDate.today());

		DelegatedApproverSettingService service = new DelegatedApproverSettingService();

		// 代理承認者設定 Set Delegated Approver Settings
		List<DelegatedApproverSettingEntity> existingSettingList = new List<DelegatedApproverSettingEntity>();
		DelegatedApproverSettingEntity newEntity1 = createEntity(managerObj.Id, delegatedApproverObjs[0].Id);
		DelegatedApproverSettingEntity newEntity2 = createEntity(managerObj.Id, delegatedApproverObjs[1].Id);
		DelegatedApproverSettingEntity newEntity3 = createEntity(managerObj.Id, delegatedApproverObjs[2].Id);
		DelegatedApproverSettingEntity newEntity4 = createEntity(managerObj.Id, delegatedApproverObjs[3].Id);
		existingSettingList.add(newEntity1);
		existingSettingList.add(newEntity2);
		existingSettingList.add(newEntity3);
		existingSettingList.add(newEntity4);
		service.saveSettingList(manager, existingSettingList);

		// Update Delegated Approver Settings
		// Delegated Approver 1 => remove ExpRequest access
		// Delegated Approver 2 => remove ExpReport access
		List<DelegatedApproverSettingEntity> newSettingList = new List<DelegatedApproverSettingEntity>();
		newEntity1.canApproveExpenseRequestByDelegate = false;
		newEntity2.canApproveExpenseReportByDelegate = false;
		newSettingList.add(newEntity1);
		newSettingList.add(newEntity2);
		
		Test.startTest();

		service.saveSettingList(manager, newSettingList);

		Test.stopTest();

		// 代理承認者設定が保存されているかの確認 Verify Delegate Approver Settings are saved correctly
		List<DelegatedApproverSettingEntity> result = service.getSettingList(managerObj.Id);
		System.assertEquals(newSettingList.size(), result.size());
		for (Integer i = 0; i < result.size(); i++) {
			System.assertEquals(newSettingList[i].employeeBaseId, result[i].employeeBaseId);
			System.assertEquals(newSettingList[i].delegatedApproverBaseId, result[i].delegatedApproverBaseId);
			System.assertEquals(newSettingList[i].canApproveExpenseRequestByDelegate, result[i].canApproveExpenseRequestByDelegate);
			System.assertEquals(newSettingList[i].canApproveExpenseReportByDelegate, result[i].canApproveExpenseReportByDelegate);
		}

		// 既存の事前申請が代理承認者に共有されているかの確認 Verify ExpRequests are shared with Delegated Approver 2
		ExpRequestApprovalRepository expReqRepo = new ExpRequestApprovalRepository();
		ExpRequestApprovalEntity expRequestApproval = expReqRepo.getEntityListFromRequestId(new Set<Id>{expRequest.id})[0];
		List<AppShareEntity> expReqShareList = expReqRepo.getShareEntityListByApprovalId(new List<Id>{expRequestApproval.id});
		Map<Id, AppShareEntity> expReqShareMapByApproverId = new Map<Id, AppShareEntity>();
		for (AppShareEntity share : expReqShareList) {
			expReqShareMapByApproverId.put(share.userOrGroupId, share);
		}
		AppShareEntity shareDetail = expReqShareMapByApproverId.get(delegatedApproverUserObj[1].Id);
		// TODO : Fix test case
		// System.assertEquals(expRequestApproval.id, shareDetail.parentId);

		// 既存の経費申請が代理承認者に共有されているかの確認 Verify ExpReports are shared with Delegated Approver 1
		ExpReportRequestRepository expReportRequestRepo = new ExpReportRequestRepository();
		ExpReportRequestEntity expReportRequest = expReportRequestRepo.getEntityListFromReportId(new List<Id>{expReport.id})[0];
		List<AppShareEntity> expRepShareList = expReportRequestRepo.getShareEntityListByRequestId(new List<Id>{expReportRequest.id});
		Map<Id, AppShareEntity> expRepShareMapByApproverId = new Map<Id, AppShareEntity>();
		for (AppShareEntity share : expRepShareList) {
			expRepShareMapByApproverId.put(share.userOrGroupId, share);
		}
		AppShareEntity shareDetail2 = expRepShareMapByApproverId.get(delegatedApproverUserObj[0].Id);
		// TODO : Fix test case
		// System.assertEquals(expReportRequest.id, shareDetail2.parentId);
	}

	/**
	 * 代理承認者設定保存処理のテスト Test for saving delegated approver setting list
	 * Ensure if parameter list is empty, delete all existing setting records for the employee
	 */
	@isTest
	static void saveSettingListTestDeleteAll() {
		// データ登録 Initalise Data
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		companyObj.Language__c = 'ja';
		update companyObj;
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		User managerUserObj = ComTestDataUtility.createUser('ManagerUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c managerObj = ComTestDataUtility.createEmployeeWithHistory('Manager', companyObj.Id, null, managerUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 3);
		// 社員コードが昇順になるように更新 Update Employee Code in ascending order
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		// 社員に上長を設定 Set managers
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(employeeObj.Id, AppDate.today());
		employee.getHistory(0).managerId = managerObj.Id;
		new EmployeeRepository().saveEntity(employee);

		ExpTestData testData = new ExpTestData();
		// 事前申請データ Set ExpRequest Data
		ExpRequestEntity expRequest = testData.createExpRequest(Date.today(), null, employee, null);
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(expRequest.id, 'Test');
		// 経費精算データ Set ExpReport Data
		ExpReportEntity expReport = testData.createExpReport(Date.today(), null, employee, null);
		ExpReportRequestService expReportRequestService = new ExpReportRequestService();
		expReportRequestService.isTestModeAndBypassExpReportValidation = true;
		expReportRequestService.submitRequest(expReport.id, 'Test');

		DelegatedApproverSettingService service = new DelegatedApproverSettingService();

		// 代理承認者設定 Set Delegated Approver Settings
		List<DelegatedApproverSettingEntity> settings = new List<DelegatedApproverSettingEntity>();
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[0].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[1].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[2].Id));
		service.saveSettingList(employee, settings);

		Test.startTest();

		service.saveSettingList(employee, new List<DelegatedApproverSettingEntity>());

		Test.stopTest();

		// 代理承認者設定が削除されているかの確認 Verify Delegate Approver Settings are saved correctly i.e. all deleted
		List<DelegatedApproverSettingEntity> result = service.getSettingList(employeeObj.Id);
		System.assertEquals(null, result);

		// 事前申請の共有が削除されているかの確認 Verify ExpRequests are removed from Delegated Approvers
		ExpRequestApprovalRepository expReqRepo = new ExpRequestApprovalRepository();
		ExpRequestApprovalEntity expRequestApproval = expReqRepo.getEntityListFromRequestId(new Set<Id>{expRequest.id})[0];
		List<AppShareEntity> expReqShareList = expReqRepo.getShareEntityListByApprovalId(new List<Id>{expRequestApproval.id});
		System.assertEquals(true, expReqShareList.isEmpty());

		// 経費申請の共有が削除されているかの確認 Verify ExpReports are removed from Delegated Approvers
		ExpReportRequestRepository expReportRequestRepo = new ExpReportRequestRepository();
		ExpReportRequestEntity expReportRequest = expReportRequestRepo.getEntityListFromReportId(new List<Id>{expReport.id})[0];
		List<AppShareEntity> expRepShareList = expReportRequestRepo.getShareEntityListByRequestId(new List<Id>{expReportRequest.id});
		System.assertEquals(true, expRepShareList.isEmpty());
	}

	/**
	 * 代理承認者設定削除処理のテスト Test for deleting delegated approver setting list
	 */
	@isTest
	static void deleteSettingListTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		companyObj.Language__c = 'ja';
		update companyObj;
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		User managerUserObj = ComTestDataUtility.createUser('ManagerUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c managerObj = ComTestDataUtility.createEmployeeWithHistory('Manager', companyObj.Id, null, managerUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		// 社員に上長を設定
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(employeeObj.Id, AppDate.today());
		employee.getHistory(0).managerId = managerObj.Id;
		new EmployeeRepository().saveEntity(employee);

		ExpTestData testData = new ExpTestData();
		// 事前申請データ
		ExpRequestEntity expRequest = testData.createExpRequest(Date.today(), null, employee, null);
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(expRequest.id, 'Test');
		// 経費精算データ
		ExpReportEntity expReport = testData.createExpReport(Date.today(), null, employee, null);
		ExpReportRequestService expReportRequestService = new ExpReportRequestService();
		expReportRequestService.isTestModeAndBypassExpReportValidation = true;
		expReportRequestService.submitRequest(expReport.id, 'Test');

		DelegatedApproverSettingService service = new DelegatedApproverSettingService();

		// 代理承認者設定
		List<DelegatedApproverSettingEntity> settings = new List<DelegatedApproverSettingEntity>();
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[0].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[1].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[2].Id));
		service.saveSettingList(employee, settings);

		List<Id> deleteSettingIdList = new List<Id>();
		for (DelegatedApproverSettingEntity setting : service.getSettingList(employeeObj.Id)) {
			deleteSettingIdList.add(setting.id);
		}

		Test.startTest();

		service.deleteSettingList(deleteSettingIdList);

		Test.stopTest();

		// 代理承認者設定が削除されているかの確認
		List<DelegatedApproverSettingEntity> result = service.getSettingList(employeeObj.Id);
		System.assertEquals(null, result);

		// 事前申請の共有が削除されているかの確認
		ExpRequestApprovalRepository expReqRepo = new ExpRequestApprovalRepository();
		ExpRequestApprovalEntity expRequestApproval = expReqRepo.getEntityListFromRequestId(new Set<Id>{expRequest.id})[0];
		List<AppShareEntity> expReqShareList = expReqRepo.getShareEntityListByApprovalId(new List<Id>{expRequestApproval.id});
		System.assertEquals(true, expReqShareList.isEmpty());

		// 経費申請の共有が削除されているかの確認
		ExpReportRequestRepository expRepRepo = new ExpReportRequestRepository();
		ExpReportRequestEntity expReportRequest = expRepRepo.getEntityListFromReportId(new List<Id>{expReport.id})[0];
		List<AppShareEntity> expRepShareList = expRepRepo.getShareEntityListByRequestId(new List<Id>{expReportRequest.id});
		System.assertEquals(true, expRepShareList.isEmpty());
	}

	/**
	 * 代理承認者設定エンティティを作成する Create delegated approver setting entity
	 * @param empId 社員ベースID
	 * @param delegatedApproverId 代理承認者ベースID
	 * @return 作成したエンティティ
	 */
	private static DelegatedApproverSettingEntity createEntity(Id empId, Id delegatedApproverId) {
		DelegatedApproverSettingEntity setting = new DelegatedApproverSettingEntity();
		setting.employeeBaseId = empId;
		setting.delegatedApproverBaseId = delegatedApproverId;
		setting.canApproveExpenseRequestByDelegate = true;
		setting.canApproveExpenseReportByDelegate = true;

		return setting;
	}
}