/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for ExpExchangeRateService
 */
@isTest
private class ExpExchangeRateServiceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);
	static final ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('USD', 'US Dollar');

	/**
	 * Create a new Exchange record(Positive case)
	 */
	@isTest
	static void createExchangeTest() {
		ExpExchangeRateEntity entity = new  ExpExchangeRateEntity();
		setTestParam(entity);

		Test.startTest();

		// Create new record
		Repository.SaveResult result = ExpExchangeRateService.createExchangeRate(entity);

		Test.stopTest();

		System.assertEquals(1, result.details.size());

		List<ExpExchangeRate__c> records =
		[SELECT
			Code__c,
			UniqKey__c,
			CompanyId__c,
			CurrencyId__c,
			CurrencyPair__c,
			Rate__c,
			ReverseRate__c,
			ValidFrom__c,
			ValidTo__c
			FROM ExpExchangeRate__c
			WHERE Id =: result.details[0].id];

		// Confirm new record was created
		System.assertEquals(1, records.size());

		System.assertEquals(entity.code, records[0].Code__c);
		System.assertEquals(entity.uniqKey, records[0].UniqKey__c);
		System.assertEquals(entity.companyId, records[0].CompanyId__c);
		System.assertEquals(entity.currencyId, records[0].CurrencyId__c);
		System.assertEquals(entity.currencyPair.value, records[0].CurrencyPair__c);
		System.assertEquals(entity.rate, records[0].Rate__c);
		System.assertEquals(entity.reverseRate, records[0].ReverseRate__c);
		System.assertEquals(entity.validFrom, AppDate.valueOf(records[0].ValidFrom__c));
		System.assertEquals(entity.validTo, AppDate.valueOf(records[0].ValidTo__c));
	}

	/**
	 * Create a new Exchange record(Positive case : same code in different companies can be saved)
	 */
	@isTest
	static void createExchangeUniqTest() {
		ExpExchangeRateEntity entity = new  ExpExchangeRateEntity();
		setTestParam(entity);

		Test.startTest();

		// Create one record
		ExpExchangeRateService.createExchangeRate(entity);

		ComCompany__c newCompany = ComTestDataUtility.createCompany('New Test Co. Ltd.', countryObj.Id);
		ExpExchangeRateEntity newEntity = new  ExpExchangeRateEntity();
		setTestParam(newEntity);
		newEntity.companyId = newCompany.Id;

		// Create new record with same code in different company
		Repository.SaveResult result = ExpExchangeRateService.createExchangeRate(newEntity);

		Test.stopTest();

		List<ExpExchangeRate__c> records =
		[SELECT
			Code__c,
			UniqKey__c,
			CompanyId__c,
			CurrencyId__c,
			CurrencyPair__c,
			Rate__c,
			ReverseRate__c,
			ValidFrom__c,
			ValidTo__c
			FROM ExpExchangeRate__c];

		// Confirm new record was created
		System.assertEquals(2, records.size());

		System.assertEquals(records[0].Code__c, records[1].Code__c);
		System.assertNotEquals(records[0].CompanyId__c, records[1].CompanyId__c);
	}

	/**
   * Create a new Exchange record(Negative case: Same code exists)
   */
	@isTest
	static void createExchangeDuplicateErrTest() {

		// Create 'TEST1' Exchange Rate first
		ComTestDataUtility.createExpExchangeRate('TEST', companyObj.Id, currencyObj.Id);

		ExpExchangeRateEntity entity = new  ExpExchangeRateEntity();
		setTestParam(entity);
		entity.code = 'TEST1';

		Test.startTest();

		APP.ParameterException ex;
		try {
			// Try to create new 'TEST1' currency again
			ExpExchangeRateService.createExchangeRate(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
 * Create a new Exchange record(Negative case: Same exchange rete exists in the valid period)
 */
	@isTest
	static void createExchangeDuplicatePeriodErrTest() {

		// Create 'TEST1' Exchange Rate first
		ComTestDataUtility.createExpExchangeRate('TEST', companyObj.Id, currencyObj.Id);

		ExpExchangeRateEntity entity = new  ExpExchangeRateEntity();
		setTestParam(entity);
		entity.code = 'TEST2';

		Test.startTest();

		APP.ParameterException ex;
		try {
			// Try to create new 'TEST1' currency again
			ExpExchangeRateService.createExchangeRate(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Exp_Err_DuplicateExchangeRate, ex.getMessage());
	}

	/**
	   * Update Exchange record(Positive case)
	   */
	@isTest
	static void updateExchangeTest() {
		// Create a Exchange record first
		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		ExpExchangeRateEntity entity = new ExpExchangeRateEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		Test.startTest();

		ExpExchangeRateService.updateExchangeRate(entity);

		Test.stopTest();

		List<ExpExchangeRate__c> records =
		[SELECT
			Code__c,
			UniqKey__c,
			CompanyId__c,
			CurrencyId__c,
			CurrencyPair__c,
			Rate__c,
			ReverseRate__c,
			ValidFrom__c,
			ValidTo__c
			FROM ExpExchangeRate__c
			WHERE Id =: testRecord.id];

		//Confirm the record was updated
		System.assertEquals(1, records.size());

		System.assertEquals(entity.code, records[0].Code__c);
		System.assertEquals(entity.uniqKey, records[0].UniqKey__c);
		System.assertEquals(entity.companyId, records[0].CompanyId__c);
		System.assertEquals(entity.currencyId, records[0].CurrencyId__c);
		System.assertEquals(entity.currencyPair.value, records[0].CurrencyPair__c);
		System.assertEquals(entity.rate, records[0].Rate__c);
		System.assertEquals(entity.reverseRate, records[0].ReverseRate__c);
		System.assertEquals(entity.validFrom, AppDate.valueOf(records[0].ValidFrom__c));
		System.assertEquals(entity.validTo, AppDate.valueOf(records[0].ValidTo__c));
	}

	/**
   * Update Exchange record(Negative case: Update code is duplicate)
   */
	@isTest
	static void updateExchangeDuplicateErrTest() {
		// Create Exchange test records
		ExpExchangeRate__c test1Record = ComTestDataUtility.createExpExchangeRate('Test1', companyObj.Id, currencyObj.Id);
		ExpExchangeRate__c test2Record = ComTestDataUtility.createExpExchangeRate('Test2', companyObj.Id, currencyObj.Id);

		// Create Test1 Exchange entity
		ExpExchangeRateEntity entity = new ExpExchangeRateEntity();
		setTestParam(entity);
		entity.setId(test1Record.Id);

		// Set Test2 code to Test1 Exchange entity
		entity.code = test2Record.Code__c;

		Test.startTest();

		APP.ParameterException ex;
		try {
			ExpExchangeRateService.updateExchangeRate(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
 * Update Exchange record(Negative case: Target record is no found)
 */
	@isTest
	static void updateExchangeNotFoundTest() {
		// Create a Exchange record first
		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		ExpExchangeRateEntity entity = new ExpExchangeRateEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		// Delete the record before updating
		delete testRecord;

		Test.startTest();

		APP.ParameterException ex;
		try {
			ExpExchangeRateService.updateExchangeRate(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm the expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_RecordNotFound, ex.getMessage());
	}

	/**
	 * Delete Exchange record(Positive case)
	 */
	@isTest
	static void deleteExchangeTest() {

		// Create the target data to delete
		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		Id id = testRecord.Id;

		Test.startTest();

		// Delete the target
		ExpExchangeRateService.deleteExchangeRate(id);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ExpExchangeRate__c WHERE Id =: id]);
	}

	/**
   * Delete Exchange record(Positive case: Target has been deleted)
   */
	@isTest
	static void deleteExchangeAlreadyDeletedTest() {

		// Create the target data to delete
		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		Id id = testRecord.Id;

		// Delete the target record in advance
		delete testRecord;

		Test.startTest();

		DmlException ex = null;
		try {
			// Try to Delete the target
			ExpExchangeRateService.deleteExchangeRate(id);
		} catch (DmlException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm exception did not occur
		System.assertEquals(null, ex);
	}

	/**
	 * Search Exchange data(Positive case)
   */
	@isTest
	static void searchExchangeTest() {

		ExpExchangeRate__c testRecord = ComTestDataUtility.createExpExchangeRate('Test', companyObj.Id, currencyObj.Id);

		Map<String, Object> param = new Map<String, Object>();
		param.put('id', testRecord.Id);
		param.put('code', testRecord.Code__c);
		param.put('companyId', testRecord.CompanyId__c);
		param.put('currencyId', testRecord.CurrencyId__c);
		Date targetDate = testRecord.ValidFrom__c.addDays(1);
		param.put('targetDate',  targetDate.year() + '-' + targetDate.month() + '-' + targetDate.day());

		List<ExpExchangeRateEntity> result =  ExpExchangeRateService.searchExchangeRate(param);
		System.assertEquals(1, result.size());

		ExpExchangeRateEntity entity = result[0];
		System.assertEquals(testRecord.Id, entity.id);
		System.assertEquals(testRecord.Code__c, entity.code);
		System.assertEquals(testRecord.UniqKey__c, entity.uniqKey);
		System.assertEquals(testRecord.CompanyId__c, entity.companyId);
		System.assertEquals(testRecord.CurrencyId__c, entity.currencyId);
		System.assertEquals(testRecord.CurrencyPair__c, entity.currencyPair.value);
		System.assertEquals(testRecord.Rate__c, entity.rate);
		System.assertEquals(testRecord.ReverseRate__c, entity.reverseRate);
		System.assertEquals(AppDate.valueOf(testRecord.ValidFrom__c), entity.validFrom);
		System.assertEquals(AppDate.valueOf(testRecord.ValidTo__c), entity.validTo);
	}

	/**
	 * Search all Exchange data(Positive case)
   */
	@isTest
	static void searchExchangeAllTest() {

		ComTestDataUtility.createExpExchangeRates('Test', companyObj.Id, currencyObj.Id, 3);
		// Fetch data order by code
		List<ExpExchangeRate__c> testRecords = [SELECT
			Id,
			Code__c,
			UniqKey__c,
			CompanyId__c,
			CurrencyId__c,
			CurrencyPair__c,
			Rate__c,
			ReverseRate__c,
			ValidFrom__c,
			ValidTo__c
			FROM ExpExchangeRate__c
			ORDER BY Code__c NULLS LAST];

		Test.startTest();

		// When searching by nul ID, return all data
		List<ExpExchangeRateEntity> result =  ExpExchangeRateService.searchExchangeRate(null);

		Test.stopTest();

		// Confirm all data were fetched
		System.assertEquals(testRecords.size(), result.size());

		for (Integer i = 0, n = testRecords.size(); i < n; i++) {
			System.assertEquals(testRecords[i].Id, result[i].id);
			System.assertEquals(testRecords[i].Code__c, result[i].code);
			System.assertEquals(testRecords[i].UniqKey__c, result[i].uniqKey);
			System.assertEquals(testRecords[i].CompanyId__c, result[i].companyId);
			System.assertEquals(testRecords[i].CurrencyId__c, result[i].currencyId);
			System.assertEquals(testRecords[i].CurrencyPair__c, result[i].currencyPair.value);
			System.assertEquals(testRecords[i].Rate__c, result[i].rate);
			System.assertEquals(testRecords[i].ReverseRate__c, result[i].reverseRate);
			System.assertEquals(AppDate.valueOf(testRecords[i].ValidFrom__c), result[i].validFrom);
			System.assertEquals(AppDate.valueOf(testRecords[i].ValidTo__c), result[i].validTo);
		}
	}

	/**
   * Set test parameter to entity
   * @param param Target object to set data
   */
	static void setTestParam(ExpExchangeRateEntity entity) {
		entity.code = 'TEST';
		entity.companyId = companyObj.Id;
		entity.currencyId = currencyObj.Id;
		entity.currencyPair = ExpCurrencyPair.valueOf('USD_JPY');
		entity.rate = 0.009091;
		entity.reverseRate = (1 / entity.rate).setScale(6);
		entity.validFrom = AppDate.today();
		entity.validTo = AppDate.today().addMonths(1);
	}

}