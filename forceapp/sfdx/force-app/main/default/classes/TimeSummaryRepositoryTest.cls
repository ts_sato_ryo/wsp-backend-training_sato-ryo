/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * TimeSummaryRepositoryのテスト
 */
@isTest
private class TimeSummaryRepositoryTest {

	/**
	 * getSummaryListをテストする
	 */
	@isTest private static void getSummaryListTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummary__c summary = testData.createSummary(AppDate.newInstance(2017, 7, 1), AppDate.newInstance(2017, 7, 31));
		summary.Locked__c = true;
		update summary;

		Test.startTest();

		// パラメータ
		String empId = testData.emp.Id;
		AppDate targetDate = new AppDate('2017-07-10');

		// 実行
		TimeSummaryRepository rep = new TimeSummaryRepository();
		List<TimeSummaryEntity> summaryList = rep.getSummaryList(empId, targetDate);

		Test.stopTest();

		System.assertEquals(1, summaryList.size());
		System.assertEquals(summary.EmployeeHistoryId__c, summaryList[0].employeeId);
		System.assertEquals(summary.TimeSettingHistoryId__c, summaryList[0].timeSettingHistoryId);
		System.assertEquals(summary.SummaryName__c, summaryList[0].summaryName);
		System.assertEquals(Date.newInstance(2017, 7, 1), AppDate.convertDate(summaryList[0].startDate));
		System.assertEquals(Date.newInstance(2017, 7, 31), AppDate.convertDate(summaryList[0].endDate));
		System.assertEquals(true, summaryList[0].isLocked);
	}

	/**
	 * getRecordListBySummaryIdをテストする
	 */
	@isTest private static void getRecordListBySummaryIdTest() {
		TimeTestData testData = new TimeTestData();
		TimeSummaryEntity summary = testData.createSummaryEntity();

		// 工数明細を作成
		List<TimeRecord__c> recordDataList = new List<TimeRecord__c>();
		for(Date d = summary.startDate.getDate(); d <= summary.endDate.getDate(); d = d.addDays(1)) {
			recordDataList.add(new TimeRecord__c(
				UniqKey__c = summary.Id + String.valueOf(d.year()) + String.valueOf(d.month()).leftPad(2, '0') + String.valueOf(d.day()).leftPad(2, '0'),
				TimeSummaryId__c = summary.Id,
				Date__c = d,
				Note__c = '作業報告',
				Output__c = 'アウトプット'
			));
		}
		insert recordDataList;

		// 工数内訳を作成
		List<TimeRecordItem__c> recordItemList = new List<TimeRecordItem__c>();
		for (Integer i = 0; i < recordDataList.size(); i++) {
			recordItemList.add(new TimeRecordItem__c(
				TimeRecordId__c = recordDataList[i].Id,
				JobId__c = testData.jobList[0].Id,
				WorkCategoryId__c = testData.workCategoryList[0].Id,
				Order__c = 1,
				TimeDirect__c = true,
				Time__c = 480
			));
		}
		insert recordItemList;

		Test.startTest();

		// 実行
		TimeSummaryRepository rep = new TimeSummaryRepository();
		List<TimeRecordEntity> recordList = rep.getRecordListBySummaryId(summary.Id);

		Test.stopTest();

		// 工数明細リスト
		System.assertEquals(31, recordList.size());
		for (Integer i = 0; i < recordList.size(); i++) {
			System.assertEquals(recordDataList[i].Date__c, AppDate.convertDate(recordList[i].recordDate));
			System.assertEquals(recordDataList[i].Note__c, recordList[i].note);
			System.assertEquals(false, recordList[i].isSummaryLocked);
			System.assertEquals(testData.jobList[0].Code__c, recordList[i].recordItemList[0].jobCode);
			System.assertEquals(testData.jobList[0].Name_L0__c, recordList[i].recordItemList[0].jobName);
			System.assertEquals(testData.workCategoryList[0].Code__c, recordList[i].recordItemList[0].workCategoryCode);
			System.assertEquals(testData.workCategoryList[0].Name_L0__c, recordList[i].recordItemList[0].workCategoryName);
			System.assertEquals(Integer.valueOf(recordItemList[i].Time__c), recordList[i].recordItemList[0].workTime);
			System.assertEquals(Integer.valueOf(recordItemList[i].Order__c), recordList[i].recordItemList[0].order);
		}
	}

	/**
	 * getRecordのテスト
	 * 工数確定申請の参照項目を取得できることを検証する
	 */
	@isTest private static void getRecordReferencedRequestFieldTest() {
		// データ作成
		AppDate targetDate = AppDate.newInstance(2019, 4, 1);
		TimeTestData testData = new TimeTestData(targetDate);
		TimeSummary__c summary = testData.createMonthlySummary(targetDate);
		List<TimeRecord__c> records = testData.createRecordList(summary);
		TimeRequest__c request = testData.createRequest(summary);
		summary.RequestId__c = request.Id;
		summary.Locked__c = true;
		update summary;

		// 実行
		Test.startTest();
		TimeSummaryRepository rep = new TimeSummaryRepository();
		TimeRecordEntity record = rep.getRecord(testData.emp.Id, targetDate);
		Test.stopTest();

		// 工数確定フラグ
		System.assertEquals(true, record.isSummaryLocked);
	}

	/**
	 * searchSummaryMapのテスト
	 * - Mapで取得できることを確認する
	 */
	@isTest
	static void searchSummaryMapTest() {
		TimeTestData testData = new TimeTestData();
		List<TimeSummaryEntity> summaryList = new List<TimeSummaryEntity>{
				testData.createSummaryEntity(AppDate.newInstance(2019, 4, 5)),
				testData.createSummaryEntity(AppDate.newInstance(2019, 5, 5)),
				testData.createSummaryEntity(AppDate.newInstance(2019, 6, 5)),
				testData.createSummaryEntity(AppDate.newInstance(2019, 7, 5))};

		// 実行
		TimeSummaryRepository repo = new TimeSummaryRepository();
		Map<Id, TimeSummaryEntity> summaryMap;
		try {
			Test.startTest();
			summaryMap = repo.searchSummaryMap(testData.emp.id, new AppDateRange(AppDate.newInstance(2019, 5, 5), AppDate.newInstance(2019, 7, 10)));
			Test.stopTest();
		} catch (Exception e){
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}

		// 検証
		// キーがサマリIDであることを確認する
		// TODO 実行結果に想定したエンティティが設定されていることを検証する
		System.assertEquals(3, summaryMap.size());
		System.assert(summaryMap.containsKey(summaryList[1].id));
		System.assert(summaryMap.containsKey(summaryList[2].id));
		System.assert(summaryMap.containsKey(summaryList[3].id));
	}

	/**
	 * searchSummaryMapのテスト
	 * - 該当するデータが存在しない場合に空のMapで取得できることを確認する
	 */
	@isTest
	static void searchSummaryMapTestEmpty() {

		// 実行
		TimeSummaryRepository repo = new TimeSummaryRepository();
		Map<Id, TimeSummaryEntity> summaryMap;
		try {
			Test.startTest();
			summaryMap = repo.searchSummaryMap(UserInfo.getUserId(), new AppDateRange(AppDate.newInstance(2018, 5, 5), AppDate.newInstance(2018, 7, 10)));
			Test.stopTest();
		} catch (Exception e){
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}

		// 検証
		System.assertEquals(0, summaryMap.size());
	}

	/**
	 * saveSummaryEntityをテストする
	 */
	@isTest static void saveSummaryEntityTest() {
		TimeTestData testData = new TimeTestData();

		TimeSummaryEntity newSummary = new TimeSummaryEntity();
		newSummary.name = 'test';
		newSummary.uniqKey = testData.emp.Code__c + '201707' + '1';
		newSummary.employeeId = testData.emp.CurrentHistoryId__c;
		newSummary.timeSettingHistoryId = testData.timeSettingHistory.Id;
		newSummary.summaryName = '201707';
		newSummary.subNo = 1;
		newSummary.startDate = new AppDate('2017-07-01');
		newSummary.endDate = new AppDate('2017-07-31');
		newSummary.isLocked = true;

		// 工数明細を作成
		List<TimeRecordEntity> newRecordList = new List<TimeRecordEntity>();
		for(Date d = newSummary.startDate.getDate(); d <= newSummary.endDate.getDate(); d = d.addDays(1)) {
			TimeRecordEntity newRecord = new TimeRecordEntity();
			newRecord.name = 'test';
			newRecord.uniqKey = newSummary.id + (new AppDate(d)).formatYYYYMMDD();
			newRecord.timeSummaryId = newSummary.id;
			newRecord.recordDate = AppDate.valueOf(d);
			newRecord.note = '作業報告';
			newRecord.output = 'アウトプット';

			newRecordList.add(newRecord);
		}
		newSummary.replaceRecordList(newRecordList);

		TimeSummaryRepository rep = new TimeSummaryRepository();
		Test.startTest();
		Repository.SaveResult result = rep.saveSummaryEntity(newSummary, true);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);
		System.assertEquals(1, result.details.size());
		System.assertNotEquals(null, result.details[0].id);
		Id summaryId = result.details[0].id;

		// 作成された工数データを取得
		Date paramDate = Date.newInstance(2017, 7, 10);
		TimeSummary__c summary = [
				SELECT
					Id,
					Name,
					UniqKey__c,
					EmployeeHistoryId__c,
					TimeSettingHistoryId__c,
					SummaryName__c,
					SubNo__c,
					StartDate__c,
					EndDate__c,
					Locked__c
				FROM TimeSummary__c
				WHERE EmployeeHistoryId__c = :testData.emp.CurrentHistoryId__c
				AND StartDate__c <= :Date.valueOf(paramDate)
				AND EndDate__c >= :Date.valueOf(paramDate)
				LIMIT 1];

		// 工数サマリー
		System.assertEquals(newSummary.name, summary.Name);
		System.assertEquals(newSummary.uniqKey, summary.UniqKey__c);
		System.assertEquals(newSummary.employeeId, summary.EmployeeHistoryId__c);
		System.assertEquals(newSummary.timeSettingHistoryId, summary.TimeSettingHistoryId__c);
		System.assertEquals(newSummary.summaryName, summary.SummaryName__c);
		System.assertEquals(newSummary.subNo, summary.SubNo__c);
		System.assertEquals(newSummary.startDate.getDate(), summary.StartDate__c);
		System.assertEquals(newSummary.endDate.getDate(), summary.EndDate__c);
		System.assertEquals(newSummary.isLocked, summary.Locked__c);

		List<TimeRecord__c> recordList = [
			SELECT
				Id,
				Name,
				UniqKey__c,
				TimeSummaryId__c,
				TimeSummaryId__r.Locked__c,
				Date__c,
				Note__c,
				Output__c,
				(SELECT
					Id,
					JobId__c,
					WorkCategoryId__c,
					Order__c,
					TimeDirect__c,
					Time__c,
					Volume__c,
					Ratio__c
				FROM TimeRecordItems__r
				ORDER BY Order__c)
			FROM TimeRecord__c
			WHERE TimeSummaryId__c = :summary.Id];

		// 工数明細
		System.assertEquals(31, recordList.size());
		for (Integer i = 0; i < recordList.size(); i++) {
			String strDate = (new AppDate(recordList[i].Date__c)).formatYYYYMMDD();
			System.assertEquals(newRecordList[i].name, recordList[i].Name);
			System.assertEquals(summary.Id + strDate, recordList[i].UniqKey__c);
			System.assertEquals(newRecordList[i].recordDate.getDate(), recordList[i].Date__c);
			System.assertEquals(newRecordList[i].note, recordList[i].Note__c);
			System.assertEquals(newRecordList[i].output, recordList[i].Output__c);
			System.assertEquals(true, recordList[i].TimeSummaryId__r.Locked__c);
			System.assertEquals(0, recordList[i].TimeRecordItems__r.size());
		}
	}
}