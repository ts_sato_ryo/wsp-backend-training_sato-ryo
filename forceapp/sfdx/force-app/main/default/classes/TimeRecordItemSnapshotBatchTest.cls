/**
 * @group 工数
 *
 * TimeRecordItemSnapshotBatchTestのテストクラス
 */
@isTest
private class TimeRecordItemSnapshotBatchTest {
	
	/**
	 * 工数未確確定のサマリーのスナップショットを作成する事を検証する
	 */
	@isTest
	static void executeTest() {
		//　データ作成
		AppDate targetDate = AppDate.newInstance(2019, 4, 1);
		TimeTestData testData = new TimeTestData(targetDate);
		ComJob__c job = testData.jobList[0];
		TimeSummary__c summary = testData.createMonthlySummary(targetDate);
		TimeRecord__c record = testData.createRecordList(summary)[0];
		TimeRecordItem__c item = testData.createRecordItem(record, job);

		// 実行
		Test.StartTest();
		TimeRecordItemSnapshotBatch batch = new TimeRecordItemSnapshotBatch();
		Id jobId = Database.executeBatch(batch, TimeRecordItemSnapshotBatch.SCOPE_LIMIT);
		Test.StopTest();

		// 検証
		List<TimeRecordItemSnapshot__c> snapshots = [Select Id, JobIdLevel1__c From TimeRecordItemSnapshot__c Where TimeRecordItemId__c = :item.Id];
		System.assertEquals(1, snapshots.size());
		System.assertEquals(job.Id, snapshots[0].JobIdLevel1__c);
	}

	/**
	 * 工数確定済みのサマリーはスナップショットを作成しない事を検証する
	 */
	@isTest
	static void executeTestRequestedSummary() {
		//　データ作成
		AppDate targetDate = AppDate.newInstance(2019, 4, 1);
		TimeTestData testData = new TimeTestData(targetDate);
		ComJob__c job = testData.jobList[0];

		// 工数確定済みの工数サマリー作成する
		TimeSummary__c summary = testData.createMonthlySummary(targetDate);
		TimeRecord__c record = testData.createRecordList(summary)[0];
		TimeRecordItem__c item = testData.createRecordItem(record, job);
		TimeRequest__c request = testData.createRequest(summary);
		summary.RequestId__c = request.Id;
		update summary;

		// 実行
		Test.StartTest();
		TimeRecordItemSnapshotBatch batch = new TimeRecordItemSnapshotBatch();
		Id jobId = Database.executeBatch(batch, TimeRecordItemSnapshotBatch.SCOPE_LIMIT);
		Test.StopTest();

		// 検証
		List<TimeRecordItemSnapshot__c> snapshots = [Select Id From TimeRecordItemSnapshot__c Where TimeRecordItemId__c = :item.Id];
		System.assertEquals(0, snapshots.size());
	}

	/**
	 * バッチスコープの上限を超えている場合、バッチが異常終了することを検証する
	 */
	@isTest
	static void executeTestOverScopeLimit() {
		//　データ作成
		AppDate targetDate = AppDate.newInstance(2019, 4, 1);
		TimeTestData testData = new TimeTestData(targetDate);

		// 上限値を超えるサマリーを作成する
		Integer overLimit = TimeRecordItemSnapshotBatch.SCOPE_LIMIT + 1;
		for (Integer i = 0; i < overLimit; i++) {
			TimeSummary__c summary = testData.createMonthlySummary(targetDate.addMonths(i));
			TimeRecord__c record = testData.createRecordList(summary)[0];
			TimeRecordItem__c item = testData.createRecordItem(record, testData.jobList[0]);
		}

		// 実行
		App.IllegalStateException actualException;
		TimeRecordItemSnapshotBatch batch = new TimeRecordItemSnapshotBatch();
		try {
			Test.StartTest();	
			Id jobId = Database.executeBatch(batch, overLimit);
			Test.StopTest();
			System.assert(false);
		} catch (App.IllegalStateException e) {
			System.assertEquals('ILLEGAL_STATE', e.getErrorCode());
		}
		
		// レコードが作成されていないことを検証
		List<TimeRecordItemSnapshot__c> snapshots = [Select Id From TimeRecordItemSnapshot__c];
		System.assertEquals(0, snapshots.size());
	}
}
