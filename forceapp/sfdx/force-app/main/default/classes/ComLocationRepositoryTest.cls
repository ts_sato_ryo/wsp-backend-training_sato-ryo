/**
  * @group 共通
  *
  * 位置情報リポジトリのテスト
  */
@isTest
private class ComLocationRepositoryTest {
	/**
	 * 取得ができることを確認
	 */
	@isTest private static void getEntityTest() {
		AppDate targetDate = AppDate.newInstance(2019, 4, 17);
		AppDatetime targetDateTime = new AppDateTime('2019-04-17T00:00:00Z');
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		testData.createAttSummaryWithRecords(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id,
		 	targetDate, targetDate);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		List<AttSummaryEntity> summaryList = summaryRepo.searchEntity(testData.employee.id, targetDate);
		AttRecordEntity record = summaryRepo.getRecordEntity(summaryList[0].id, targetDate, false);

		ComLocation__c sobj = new ComLocation__c(
			Name = 'ComLocationName',
			EmployeeBaseId__c = testData.employee.id,
			Type__c = LocationType.ATT_STAMP.value,
			DateTime__c = targetDateTime.getDatetime(),
			Location__latitude__s = 35.676302,
			Location__longitude__s = 139.770166,
			Source__c = 'Mobile',
			Comment__c = 'コメント',
			AttRecordId__c = record.id,
			AttStampType__c = 'ClockIn'
		);

		Database.SaveResult saveResult = Database.insert(sobj);
		Id targetId = saveResult.id;

		ComLocationRepository repo = new ComLocationRepository();
		Test.startTest();
			ComLocationEntity targetEntity = repo.getEntity(targetId);
		Test.stopTest();

		System.assertEquals(targetId, targetEntity.id);
		System.assertEquals(sobj.EmployeeBaseId__c, targetEntity.employeeBaseId);
		System.assertEquals(sobj.Type__c, targetEntity.rcdType.value);
		System.assertEquals(sobj.DateTime__c, targetEntity.rcdDateTime.getDatetime());
		System.assertEquals(sobj.Location__latitude__s, targetEntity.rcdLocation.getLatitude());
		System.assertEquals(sobj.Location__longitude__s, targetEntity.rcdLocation.getLongitude());
		System.assertEquals(sobj.Source__c, targetEntity.source.value);
		System.assertEquals(sobj.Comment__c, targetEntity.comment);
		System.assertEquals(sobj.AttRecordId__c, targetEntity.attRecordId);
		System.assertEquals(sobj.AttStampType__c, targetEntity.rcdAttStampType.value);
	}

	/**
	 * 保存ができることを確認
	 */
	@isTest private static void saveEntityTest() {
		AppDate targetDate = AppDate.newInstance(2019, 4, 17);
		AppDatetime targetDateTime = new AppDateTime('2019-04-17T00:00:00Z');
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		testData.createAttSummaryWithRecords(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id,
		 	targetDate, targetDate);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		List<AttSummaryEntity> summaryList = summaryRepo.searchEntity(testData.employee.id, targetDate);
		AttRecordEntity record = summaryRepo.getRecordEntity(summaryList[0].id, targetDate, false);

		ComLocationEntity entity = new ComLocationEntity();
		entity.rcdType = LocationType.ATT_STAMP;
		entity.rcdAttStampType = AttStampType.CLOCK_IN;
		entity.employeeBaseId = testData.employee.id;
		entity.comment = 'コメント';
		entity.rcdDateTime = targetDateTime;
		entity.rcdLocation = Location.newInstance(35.676302, 139.770166);
		entity.source = LocationSource.Mobile;
		entity.attRecordId = record.id;

		ComLocationRepository repo = new ComLocationRepository();
		Test.startTest();
			Repository.SaveResult result = repo.saveEntity(entity);
		Test.stopTest();

		Id entityId = result.details[0].id;

		List<ComLocation__c> sobjList = [
			SELECT
					Type__c,
					AttStampType__c,
					EmployeeBaseId__c,
					Comment__c,
					DateTime__c,
					Location__Latitude__s,
					Location__Longitude__s,
					Source__c,
					AttRecordId__c
			FROM ComLocation__c
			WHERE Id = :entityId
		];

		System.assertEquals(1, sobjList.size());

		ComLocation__c sobj = sobjList[0];
		System.assertEquals(entity.rcdType.value, sobj.Type__c);
		System.assertEquals(entity.rcdAttStampType.value, sobj.AttStampType__c);
		System.assertEquals(entity.employeeBaseId, sobj.EmployeeBaseId__c);
		System.assertEquals(entity.comment, sobj.Comment__c);
		System.assertEquals(AppDateTime.convertDateTime(entity.rcdDateTime), sobj.DateTime__c);
		System.assertEquals(entity.rcdLocation.getLatitude(), sobj.Location__Latitude__s);
		System.assertEquals(entity.rcdLocation.getLongitude(), sobj.Location__Longitude__s);
		System.assertEquals(entity.source.value, sobj.Source__c);
		System.assertEquals(entity.attRecordId, sobj.AttRecordId__c);
	}
}
