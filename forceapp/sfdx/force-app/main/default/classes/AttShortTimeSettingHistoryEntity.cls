/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 短時間勤務設定履歴エンティティ
 */
public with sharing class AttShortTimeSettingHistoryEntity extends AttShortTimeHistoryGeneratedEntity {
	/** 短時間勤務設定名の最大文字数 */
	public final static Integer NAME_MAX_LENGTH = 80;
	/** 短時間勤務の許可時間の下限値 */
	public final static Integer ALLOWABLE_TIME_MIN = 0;
	/** 短時間勤務の許可時間の上限値 */
	public final static Integer ALLOWABLE_TIME_MAX = 480;

	/**
	 * デフォルトコンストラクタ
	 */
	public AttShortTimeSettingHistoryEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public AttShortTimeSettingHistoryEntity(AttShortTimeWorkSettingHistory__c sobj) {
		super(sobj);
	}

	/** 短時間勤務設定名(多言語) 参照のみ */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public AttShortTimeSettingHistoryEntity copy() {
		AttShortTimeSettingHistoryEntity copyEntity = new AttShortTimeSettingHistoryEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildHistoryEntity copySuperEntity() {
		return copy();
	}
}
