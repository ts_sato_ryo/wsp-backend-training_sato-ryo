@isTest
private class AttSummaryEntityTest {

	/**
	 * Request.equalsのテスト
	 * 正しく値が比較できていることを確認する
	 */
	@isTest static void requestEqualsTest() {
		AttSummaryEntity.Request request = new AttSummaryEntity.Request(
				AppRequestStatus.DISABLED, AppCancelType.REMOVED, true);

		AttSummaryEntity.Request compare;

		// 等しい場合
		compare = new AttSummaryEntity.Request(
				AppRequestStatus.DISABLED, AppCancelType.REMOVED, true);
		System.assertEquals(request, compare);

		// 値が異なる場合、等しくない
		compare = new AttSummaryEntity.Request(
				AppRequestStatus.PENDING, AppCancelType.REMOVED, true);
		System.assertNotEquals(request, compare);
		compare = new AttSummaryEntity.Request(
				AppRequestStatus.DISABLED, AppCancelType.REJECTED, true);
		System.assertNotEquals(request, compare);
		compare = new AttSummaryEntity.Request(
				AppRequestStatus.DISABLED, AppCancelType.REMOVED, false);
		System.assertNotEquals(request, compare);

		// 型が異なる場合、等しくない
		System.assertEquals(false, request.equals(Integer.valueOf(123)));

		// null
		System.assertEquals(false, request.equals(null));
	}

	/**
	 * nameのテスト
	 * 不正な値を設定した場合に例外が発生することを確認する
	 */
	@isTest static void nameTest() {
		try {
			AttSummaryEntity entity = new AttSummaryEntity();
			entity.name = '';
		} catch(App.ParameterException e) {
			System.assert(true);
		} catch(Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}
	}

	/**
	 * createNameのテスト
	 */
	@isTest static void createNameTest() {
		String empCode;
		String empName;
		AppDate startDate;
		AppDate endDate;

		// 勤怠記録名が作成できることを確認する
		empCode = '0123';
		empName = 'Test Emp';
		startDate = AppDate.newInstance(2017, 7, 1);
		endDate = AppDate.newInstance(2017, 7, 31);
		System.assertEquals(
				empCode+empName+'20170701-20170731',
				AttSummaryEntity.createName(empCode, empName, startDate, endDate));


		// empCodeが空の場合、例外が発生する
		empCode = '';
		empName = 'Test Emp';
		startDate = AppDate.newInstance(2017, 7, 1);
		endDate = AppDate.newInstance(2017, 7, 31);
		try {
			AttSummaryEntity.createName(empCode, empName, startDate, endDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

		// empNameが空の場合、例外が発生する
		empCode = '0123';
		empName = ' ';
		startDate = AppDate.newInstance(2017, 7, 1);
		endDate = AppDate.newInstance(2017, 7, 31);
		try {
			AttSummaryEntity.createName(empCode, empName, startDate, endDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

		// startDateがnullの場合、例外が発生する
		empCode = '0123';
		empName = 'Test Emp';
		startDate = null;
		endDate = AppDate.newInstance(2017, 7, 31);
		try {
			AttSummaryEntity.createName(empCode, empName, startDate, endDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

		// endDateがnullの場合、例外が発生する
		empCode = '0123';
		empName = 'Test Emp';
		startDate = AppDate.newInstance(2017, 7, 31);
		endDate = null;
		try {
			AttSummaryEntity.createName(empCode, empName, startDate, endDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}
	}

	/**
	 * createSummaryNameのテスト
	 */
	@isTest static void createSummaryNameTest() {
		AttWorkingTypeBaseGeneratedEntity.MarkType markType;
		AppDate startDate = AppDate.newInstance(2017, 7, 16);
		AppDate endDate = AppDate.newInstance(2017, 8, 15);

		// markTypeがBeginBaseの場合、開始日の月で作成される
		markType = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		System.assertEquals('201707', AttSummaryEntity.createSummaryName(markType, startDate, endDate));

		// markTypeがEndBaseの場合、開始日の月で作成される
		markType = AttWorkingTypeBaseGeneratedEntity.MarkType.endBase;
		System.assertEquals('201708', AttSummaryEntity.createSummaryName(markType, startDate, endDate));

		// markTypeがnullの場合例外が発生する
		markType = null;
		try {
			AttSummaryEntity.createSummaryName(markType, startDate, endDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

		// startDateがnullの場合例外が発生する
		markType = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		startDate = null;
		endDate = AppDate.newInstance(2017, 8, 15);
		try {
			AttSummaryEntity.createSummaryName(markType, startDate, endDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

		// startDateがnullの場合例外が発生する
		markType = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		startDate = AppDate.newInstance(2017, 7, 16);
		endDate = null;
		try {
			AttSummaryEntity.createSummaryName(markType, startDate, endDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}
	}

	/**
	 * 勤務確定申請詳細ステータスのテスト
	 */
	@isTest private static void getRqeustDetailStatusTest() {
		AttSummaryEntity entity = new AttSummaryEntity();
		Id testRequestId = ComTestDataUtility.getDummyId(new AttRequestEntity().createSObject().getSObjectType());

		// 未申請
		// 申請IDが設定されていない
		entity.requestId = null;
		System.assertEquals(AttRequestEntity.DetailStatus.NOT_REQUESTED, entity.getRequestDetailStatus());
		// ステータスが設定されていない
		entity.requestId = testRequestId;
		entity.request = new AttSummaryEntity.Request(null, null, false);
		System.assertEquals(AttRequestEntity.DetailStatus.NOT_REQUESTED, entity.getRequestDetailStatus());

		// 承認待ち
		entity.requestId = testRequestId;
		entity.request = new AttSummaryEntity.Request(AppRequestStatus.PENDING, null, false);
		System.assertEquals(AttRequestEntity.DetailStatus.PENDING, entity.getRequestDetailStatus());

		// 承認済み
		entity.requestId = testRequestId;
		entity.request = new AttSummaryEntity.Request(AppRequestStatus.APPROVED, null, false);
		System.assertEquals(AttRequestEntity.DetailStatus.APPROVED, entity.getRequestDetailStatus());

		// 却下
		entity.requestId = testRequestId;
		entity.request = new AttSummaryEntity.Request(AppRequestStatus.DISABLED, AppCancelType.REJECTED, false);
		System.assertEquals(AttRequestEntity.DetailStatus.REJECTED, entity.getRequestDetailStatus());

		// 申請取消
		entity.requestId = testRequestId;
		entity.request = new AttSummaryEntity.Request(AppRequestStatus.DISABLED, AppCancelType.REMOVED, false);
		System.assertEquals(AttRequestEntity.DetailStatus.REMOVED, entity.getRequestDetailStatus());

		// 承認取消
		entity.requestId = testRequestId;
		entity.request = new AttSummaryEntity.Request(AppRequestStatus.DISABLED, AppCancelType.CANCELED, false);
		System.assertEquals(AttRequestEntity.DetailStatus.CANCELED, entity.getRequestDetailStatus());

		// ステータスが無効なのに取消種別がなしの場合はエラー
		entity.requestId = testRequestId;
		try {
			entity.request = new AttSummaryEntity.Request(AppRequestStatus.DISABLED, AppCancelType.NONE, false);
			entity.getRequestDetailStatus();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			// OK
			System.debug(e.getMessage());
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * hasAttRecordのテスト
	 */
	@isTest private static void hasAttRecordTest() {
		AttSummaryEntity summary = new AttSummaryEntity();
		AttRecordEntity record1 = new AttRecordEntity();
		record1.rcdDate = AppDate.newInstance(2018, 11, 1);
		AttRecordEntity record2 = new AttRecordEntity();
		record2.rcdDate = AppDate.newInstance(2018, 11, 2);
		AttRecordEntity record3 = new AttRecordEntity();
		record3.rcdDate = AppDate.newInstance(2018, 11, 3);
		summary.replaceAttRecordList(new List<AttRecordEntity>{record1, record2, record3});

		// 境界値の判定を検証する
		System.assert(!summary.hasAttRecord(AppDate.newInstance(2018, 10, 31)));
		System.assert(summary.hasAttRecord(AppDate.newInstance(2018, 11, 1)));
		System.assert(summary.hasAttRecord(AppDate.newInstance(2018, 11, 2)));
		System.assert(summary.hasAttRecord(AppDate.newInstance(2018, 11, 3)));
		System.assert(!summary.hasAttRecord(AppDate.newInstance(2018, 11, 4)));
	}

	// 勤怠時間内訳取得テスト
	@isTest private static void createWorkTimeDetailEntityTest() {
		Map<String, List<Map<String, Integer>>> detailMap = new Map<String, List<Map<String, Integer>>>();

		detailMap.put(AttSummaryRepository.WORK_TIME_KEY_CILI, new List<Map<String, Integer>>());
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_CILI).add(
				new Map<String, Integer>{'start' => 1, 'end' => 2});

		detailMap.put(AttSummaryRepository.WORK_TIME_KEY_CILO, new List<Map<String, Integer>>());
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_CILO).add(
				new Map<String, Integer>{'start' => 11, 'end' => 12});
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_CILO).add(
				new Map<String, Integer>{'start' => 13, 'end' => 14});

		detailMap.put(AttSummaryRepository.WORK_TIME_KEY_COLI, new List<Map<String, Integer>>());
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLI).add(
				new Map<String, Integer>{'start' => 21, 'end' => 22});
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLI).add(
				new Map<String, Integer>{'start' => 23, 'end' => 24});
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLI).add(
				new Map<String, Integer>{'start' => 25, 'end' => 26});

		detailMap.put(AttSummaryRepository.WORK_TIME_KEY_COLO, new List<Map<String, Integer>>());
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLO).add(
				new Map<String, Integer>{'start' => 31, 'end' => 32});
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLO).add(
				new Map<String, Integer>{'start' => 33, 'end' => 34});
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLO).add(
				new Map<String, Integer>{'start' => 35, 'end' => 36});
		detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLO).add(
				new Map<String, Integer>{'start' => 37, 'end' => 38});

		detailMap.put(AttSummaryRepository.WORK_TIME_KEY_CILH, new List<Map<String, Integer>>());

		Test.startTest();
		AttRecordEntity attRecord = new AttRecordEntity();
		AttRecordEntity.AttWorkTimeDetail detailData = attRecord.createWorkTimeDetailEntity(JSON.serialize(detailMap));
		Test.stopTest();

		System.assertNotEquals(null, detailData);

		System.assertNotEquals(null, detailData.CILIPeriods);
		System.assertEquals(1, detailData.CILIPeriods.size());
		System.assertEquals(1, detailData.CILIPeriods[0].startTime.getIntValue());
		System.assertEquals(2, detailData.CILIPeriods[0].endTime.getIntValue());

		System.assertNotEquals(null, detailData.CILOPeriods);
		System.assertEquals(2, detailData.CILOPeriods.size());
		System.assertEquals(11, detailData.CILOPeriods[0].startTime.getIntValue());
		System.assertEquals(12, detailData.CILOPeriods[0].endTime.getIntValue());
		System.assertEquals(13, detailData.CILOPeriods[1].startTime.getIntValue());
		System.assertEquals(14, detailData.CILOPeriods[1].endTime.getIntValue());

		System.assertNotEquals(null, detailData.COLIPeriods);
		System.assertEquals(3, detailData.COLIPeriods.size());
		System.assertEquals(21, detailData.COLIPeriods[0].startTime.getIntValue());
		System.assertEquals(22, detailData.COLIPeriods[0].endTime.getIntValue());
		System.assertEquals(23, detailData.COLIPeriods[1].startTime.getIntValue());
		System.assertEquals(24, detailData.COLIPeriods[1].endTime.getIntValue());
		System.assertEquals(25, detailData.COLIPeriods[2].startTime.getIntValue());
		System.assertEquals(26, detailData.COLIPeriods[2].endTime.getIntValue());

		System.assertNotEquals(null, detailData.COLOPeriods);
		System.assertEquals(4, detailData.COLOPeriods.size());
		System.assertEquals(31, detailData.COLOPeriods[0].startTime.getIntValue());
		System.assertEquals(32, detailData.COLOPeriods[0].endTime.getIntValue());
		System.assertEquals(33, detailData.COLOPeriods[1].startTime.getIntValue());
		System.assertEquals(34, detailData.COLOPeriods[1].endTime.getIntValue());
		System.assertEquals(35, detailData.COLOPeriods[2].startTime.getIntValue());
		System.assertEquals(36, detailData.COLOPeriods[2].endTime.getIntValue());
		System.assertEquals(37, detailData.COLOPeriods[3].startTime.getIntValue());
		System.assertEquals(38, detailData.COLOPeriods[3].endTime.getIntValue());

		System.assertNotEquals(null, detailData.CILHPeriods);
		System.assertEquals(0, detailData.CILHPeriods.size());

		System.assertNotEquals(null, detailData.COLHPeriods);
		System.assertEquals(0, detailData.COLHPeriods.size());
	}

	// 勤怠時間内訳作成テスト
	@isTest private static void createWorkTimeDetailObjTest() {
		AttRecordEntity.AttWorkTimeDetail detailData = new AttRecordEntity.AttWorkTimeDetail();

		detailData.appendCILIPeriod(new AttTimePeriod(AttTime.valueOf(1), AttTime.valueOf(2)));

		detailData.appendCILOPeriod(new AttTimePeriod(AttTime.valueOf(11), AttTime.valueOf(12)));
		detailData.appendCILOPeriod(new AttTimePeriod(AttTime.valueOf(13), AttTime.valueOf(14)));

		detailData.appendCOLIPeriod(new AttTimePeriod(AttTime.valueOf(21), AttTime.valueOf(22)));
		detailData.appendCOLIPeriod(new AttTimePeriod(AttTime.valueOf(23), AttTime.valueOf(24)));
		detailData.appendCOLIPeriod(new AttTimePeriod(AttTime.valueOf(25), AttTime.valueOf(26)));

		detailData.appendCOLOPeriod(new AttTimePeriod(AttTime.valueOf(31), AttTime.valueOf(32)));
		detailData.appendCOLOPeriod(new AttTimePeriod(AttTime.valueOf(33), AttTime.valueOf(34)));
		detailData.appendCOLOPeriod(new AttTimePeriod(AttTime.valueOf(35), AttTime.valueOf(36)));
		detailData.appendCOLOPeriod(new AttTimePeriod(AttTime.valueOf(37), AttTime.valueOf(38)));

		Test.startTest();
		AttRecordEntity attRecord = new AttRecordEntity();
		String detailJson = attRecord.createWorkTimeDetailJson(detailData);
		Test.stopTest();

		Map<String, Object> detailMap = (Map<String, Object>)JSON.deserializeUntyped(detailJson);

		System.assertNotEquals(null, detailMap);

		System.assertEquals(true, detailMap.containsKey(AttSummaryRepository.WORK_TIME_KEY_CILI));
		List<Object> periodList = (List<Object>)detailMap.get(AttSummaryRepository.WORK_TIME_KEY_CILI);
		System.assertNotEquals(null, periodList);
		System.assertEquals(1, periodList.size());
		Map<String, Object> period = (Map<String, Object>)periodList[0];
		System.assertEquals(1, period.get('start'));
		System.assertEquals(2, period.get('end'));

		periodList = (List<Object>)detailMap.get(AttSummaryRepository.WORK_TIME_KEY_CILO);
		System.assertNotEquals(null, periodList);
		System.assertEquals(2, periodList.size());
		period = (Map<String, Object>)periodList[0];
		System.assertEquals(11, period.get('start'));
		System.assertEquals(12, period.get('end'));
		period = (Map<String, Object>)periodList[1];
		System.assertEquals(13, period.get('start'));
		System.assertEquals(14, period.get('end'));

		periodList = (List<Object>)detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLI);
		System.assertNotEquals(null, periodList);
		System.assertEquals(3, periodList.size());
		period = (Map<String, Object>)periodList[0];
		System.assertEquals(21, period.get('start'));
		System.assertEquals(22, period.get('end'));
		period = (Map<String, Object>)periodList[1];
		System.assertEquals(23, period.get('start'));
		System.assertEquals(24, period.get('end'));
		period = (Map<String, Object>)periodList[2];
		System.assertEquals(25, period.get('start'));
		System.assertEquals(26, period.get('end'));

		periodList = (List<Object>)detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLO);
		System.assertNotEquals(null, periodList);
		System.assertEquals(4, periodList.size());
		period = (Map<String, Object>)periodList[0];
		System.assertEquals(31, period.get('start'));
		System.assertEquals(32, period.get('end'));
		period = (Map<String, Object>)periodList[1];
		System.assertEquals(33, period.get('start'));
		System.assertEquals(34, period.get('end'));
		period = (Map<String, Object>)periodList[2];
		System.assertEquals(35, period.get('start'));
		System.assertEquals(36, period.get('end'));
		period = (Map<String, Object>)periodList[3];
		System.assertEquals(37, period.get('start'));
		System.assertEquals(38, period.get('end'));

		periodList = (List<Object>)detailMap.get(AttSummaryRepository.WORK_TIME_KEY_CILH);
		System.assertNotEquals(null, periodList);
		System.assertEquals(0, periodList.size());

		periodList = (List<Object>)detailMap.get(AttSummaryRepository.WORK_TIME_KEY_COLH);
		System.assertNotEquals(null, periodList);
		System.assertEquals(0, periodList.size());
	}

	/**
	 * isShortWorkTimeEvenOneDayのテスト
	 * 勤怠明細で1日でも短時間勤務が適用されている場合、trueを返却することを確認する
	 */
	@isTest
	private static void isShortWorkTimeEvenOneDayTrue() {
		// テストデータ作成
		AttSummaryEntity summary = new AttSummaryEntity();
		AttRecordEntity record1 = new AttRecordEntity();
		record1.calShortTimeWorkSettingHistoryId = null;
		AttRecordEntity record2 = new AttRecordEntity();
		record2.calShortTimeWorkSettingHistoryId = null;
		AttRecordEntity record3 = new AttRecordEntity();
		record3.calShortTimeWorkSettingHistoryId = UserInfo.getUserId(); // ダミーID
		summary.replaceAttRecordList(new List<AttRecordEntity>{record1, record2, record3});
		// 実行/検証
		System.assert(summary.isShortWorkTimeEvenOneDay());
	}

	/**
	 * isShortWorkTimeEvenOneDayのテスト
	 * 勤怠明細で1日も短時間勤務が適用されていない場合、falseを返却することを確認する
	 */
	@isTest
	private static void isShortWorkTimeEvenOneDayFalse() {
		// テストデータ作成
		AttSummaryEntity summary = new AttSummaryEntity();
		AttRecordEntity record1 = new AttRecordEntity();
		record1.calShortTimeWorkSettingHistoryId = null;
		AttRecordEntity record2 = new AttRecordEntity();
		record2.calShortTimeWorkSettingHistoryId = null;
		AttRecordEntity record3 = new AttRecordEntity();
		record3.calShortTimeWorkSettingHistoryId = null;
		summary.replaceAttRecordList(new List<AttRecordEntity>{record1, record2, record3});
		// 実行/検証
		System.assert(!summary.isShortWorkTimeEvenOneDay());
	}
}
