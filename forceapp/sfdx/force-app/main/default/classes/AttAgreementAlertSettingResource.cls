/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 36協定アラート設定マスタのリソース
 */
public with sharing class AttAgreementAlertSettingResource {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 1時間当たりの分数 */
	private static final Integer MINUTES_PER_HOUR = 60;

	/**
	 * 値の単位を時間から分単位に変換する
	 * 変換した値が整数値でない場合は例外が発生します。
	 */
	private static Integer covertHourToMinute(String fieldName, Decimal hourValue) {
		if (hourValue == null) {
			return null;
		}

		// 整数値でない場合はエラーとする
		// (ここでやるべきでないと思いますが。。)
		if (hourValue - hourValue.intValue() > 0) {
// TODO エラーメッセージ
			throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{fieldName}));
		}

		Decimal minute = hourValue * MINUTES_PER_HOUR;
		return minute.intValue();
	}

	/**
	 * 値の単位を分単位から時間単位に変換する
	 */
	private static Integer convertMinuteToHour(Integer minuteValue) {
		if (minuteValue == null) {
			return null;
		}
		Decimal hour = minuteValue / MINUTES_PER_HOUR;
		return hour.intValue();
	}

	/**
	 * 36協定アラート設定パラメータ
	 */
	public class AgreementAlertSettingParam implements RemoteApi.RequestParam {
		/** ID */
		public String id;
		/** コード */
		public String code;
		/** 36協定アラート設定名(翻訳) */
		public String name;
		/** 36協定アラート設定名(L0) */
		public String name_L0;
		/** 36協定アラート設定名(L1) */
		public String name_L1;
		/** 36協定アラート設定名(L2) */
		public String name_L2;
		/** 会社ID */
		public String companyId;
		/** 有効開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 協定月警告時間1[Hour] */
		public Decimal monthlyAgreementHourWarning1;
		/** 協定月警告時間2[Hour] */
		public Decimal monthlyAgreementHourWarning2;
		/** 協定月限度時間[Hour] */
		public Decimal monthlyAgreementHourLimit;
		/** 協定月警告時間・特別1[Hour] */
		public Decimal monthlyAgreementHourWarningSpecial1;
		/** 協定月警告時間・特別2[Hour] */
		public Decimal monthlyAgreementHourWarningSpecial2;
		/** 協定月限度時間・特別[Hour] */
		public Decimal monthlyAgreementHourLimitSpecial;

		/**
		 * パラメータの値を設定したベースエンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public AttAgreementAlertSettingEntity createEntity(Map<String, Object> paramMap) {

			AttAgreementAlertSettingEntity entity = new AttAgreementAlertSettingEntity();

			entity.setId(this.id);

			if(paramMap.containsKey('code')){
				entity.code = this.code;
			}
			if(paramMap.containsKey('name_L0')){
				entity.nameL0 = this.name_L0;

				// TODO nameは仮置きでnameL0を設定
				entity.name = this.name_L0;
			}
			if(paramMap.containsKey('name_L1')){
				entity.nameL1 = this.name_L1;
			}
			if(paramMap.containsKey('name_L2')){
				entity.nameL2 = this.name_L2;
			}
			if(paramMap.containsKey('companyId')){
				entity.companyId = this.companyId;
			}
			if(paramMap.containsKey('validDateFrom')){
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			// 有効開始日のデフォルトは実行日
			if (entity.validFrom == null) {
				entity.validFrom = AppDate.today();
			}
			if(paramMap.containsKey('validDateTo')){
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (entity.validTo == null) {
				entity.validTo = ValidPeriodEntity.VALID_TO_MAX;
			}
			if(paramMap.containsKey('monthlyAgreementHourWarning1')){
				entity.monthlyAgreementHourWarning1 =
						covertHourToMinute(MESSAGE.Admin_Lbl_AgreementHourWarning1, this.monthlyAgreementHourWarning1);
			}
			if(paramMap.containsKey('monthlyAgreementHourWarning2')){
				entity.monthlyAgreementHourWarning2 =
						covertHourToMinute(MESSAGE.Admin_Lbl_AgreementHourWarning2, this.monthlyAgreementHourWarning2);
			}
			if(paramMap.containsKey('monthlyAgreementHourLimit')){
				entity.monthlyAgreementHourLimit =
						covertHourToMinute(MESSAGE.Admin_Lbl_AgreementHourLimit, this.monthlyAgreementHourLimit);
			}
			if(paramMap.containsKey('monthlyAgreementHourWarningSpecial1')){
				entity.monthlyAgreementHourWarningSpecial1 =
						covertHourToMinute(MESSAGE.Admin_Lbl_AgreementHourWarningSpecial1, this.monthlyAgreementHourWarningSpecial1);
			}
			if(paramMap.containsKey('monthlyAgreementHourWarningSpecial2')){
				entity.monthlyAgreementHourWarningSpecial2 =
						covertHourToMinute(MESSAGE.Admin_Lbl_AgreementHourWarningSpecial2, this.monthlyAgreementHourWarningSpecial2);
			}
			if(paramMap.containsKey('monthlyAgreementHourLimitSpecial')){
				entity.monthlyAgreementHourLimitSpecial =
						covertHourToMinute(MESSAGE.Admin_Lbl_AgreementHourLimitSpecial, this.monthlyAgreementHourLimitSpecial);
			}

			return entity;
		}


	}

	/**
	 * @description 36協定アラート設定レコード作成結果レスポンス
	 */
	public class SaveResponse implements RemoteApi.ResponseParam {
		/** 作成したレコードID */
		public String id;
	}

	/**
	 * @description 36協定アラート設定作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_AGREEMENT_ALERT_SETTING;

		/**
		 * @description 社員レコードを1件作成する
		 * @param  req リクエストパラメータ(Employee)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			AgreementAlertSettingParam param =
				(AgreementAlertSettingParam)req.getParam(AttAgreementAlertSettingResource.AgreementAlertSettingParam.class);

			// 権限チェック Paramに対するValidationがないのでここで実行
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// パラメータからエンティティを作成
			AttAgreementAlertSettingEntity entity = param.createEntity(req.getParamMap());

			// 保存する
			Id resId = new AttAgreementAlertSettingService().createNewAlertSetting(entity);

			AttAgreementAlertSettingResource.SaveResponse res = new AttAgreementAlertSettingResource.SaveResponse();
			res.Id = resId;
			return res;
		}
	}

	/**
	 * @description 36協定アラート設定作成APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_AGREEMENT_ALERT_SETTING;

		/**
		 * @description 社員レコードを1件作成する
		 * @param  req リクエストパラメータ(Employee)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			AgreementAlertSettingParam param =
				(AgreementAlertSettingParam)req.getParam(AttAgreementAlertSettingResource.AgreementAlertSettingParam.class);

			// パラメータのバリデーション
			validateParam(param);

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// パラメータからエンティティを作成
			AttAgreementAlertSettingEntity entity = param.createEntity(req.getParamMap());

			// 保存する
			(new AttAgreementAlertSettingService()).updateAlertSetting(entity);

			// レスポンスパラメータなし
			return null;
		}

		/**
		 * 更新APIのリクエストパラメータのバリデーションを実行
		 */
		private void validateParam(AttAgreementAlertSettingResource.AgreementAlertSettingParam param) {
			// ID
			if (String.isBlank(param.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('ID');
				throw ex;
			}
		}

	}

	/**
	 * @description 社員ベースレコードの削除パラメータを定義するクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {

		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}

	}

	/**
	 * @description 社員ベースレコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_AGREEMENT_ALERT_SETTING;

		/**
		 * @description 社員レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			AttAgreementAlertSettingResource.DeleteRequest param =
					(DeleteRequest)req.getParam(DeleteRequest.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
	 			// レコードを削除
	 			(new AttAgreementAlertSettingRepository()).deleteEntity(param.id);
	 		} catch (DmlException e) {
	 			// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

	 				// TODO 暫定的にデバッグログを出力しておきます
	 				System.debug(e);

	 				// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
	 				//      現状では工数の問題で実装ができないため下記のようなエラーメッセージで対応します。
	 				// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
	  				e.setMessage(MESSAGE.Com_Err_FaildDeleteReference);
	 				throw e;
	 			}
	 		}

			// 成功レスポンスをセットする
			return null;
		 }

	}

	/**
	 * @description レコード検索条件を格納するクラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 関連する会社レコードID */
		public String companyId;
		/** 対象日 */
		public Date targetDate;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.Id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// 会社ID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}
		}

	}

	/**
	 * @description レコード検索結果を格納するクラス
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public AgreementAlertSettingParam[] records;
	}

	/**
	 * @description 社員レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 社員レコードを1件登録する
		 * @param  parameter リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchRequest param =
					(AttAgreementAlertSettingResource.SearchRequest)req.getParam(AttAgreementAlertSettingResource.SearchRequest.class);
			Map<String, Object> paramMap = req.getParamMap();

			// パラメータのバリデーション
			param.validate();

			// 検索フィルタ作成
			AttAgreementAlertSettingRepository.SearchFilter filter =
					new AttAgreementAlertSettingRepository.SearchFilter();
			// パラメータ id が指定されている場合は、idで検索
			if(paramMap.containsKey('id')){
				filter.ids = new Set<Id>{param.id};
			}
			// パラメータ companyId が指定されている場合は、会社IDで検索
			if(paramMap.containsKey('companyId')){
				filter.companyIds = new Set<Id>{param.companyId};
			}
			// パラメータ targetDate が指定されている場合は、対象日で検索
			if(paramMap.containsKey('targetDate')){
				filter.targetDate = AppDate.valueOf(param.targetDate);
			}

			List<AttAgreementAlertSettingEntity> entityList =
					(new AttAgreementAlertSettingRepository()).searchEntity(filter);

			// レスポンスクラスに変換
			List<AgreementAlertSettingParam> records = new List<AgreementAlertSettingParam>();
			for(AttAgreementAlertSettingEntity entity : entitylist) {
				records.add(createAgreementAlertSettingParam(entity));
			}

			// 成功レスポンスをセットする
			SearchResponse res = new SearchResponse();
			res.records = records;
			return res;
		}

		/**
		 * AgreementAlertSettingParamを作成する
		 */
		private AgreementAlertSettingParam createAgreementAlertSettingParam(AttAgreementAlertSettingEntity entity) {
			AgreementAlertSettingParam param = new AgreementAlertSettingParam();
			param.id = entity.id;
			param.code = entity.code;
			param.name = entity.nameL.getValue();
			param.name_L0 = entity.nameL0;
			param.name_L1 = entity.nameL1;
			param.name_L2 = entity.nameL2;
			param.companyId = entity.companyId;
			param.validDateFrom = AppConverter.dateValue(entity.validFrom);
			param.validDateTo = AppConverter.dateValue(entity.validTo);
			param.monthlyAgreementHourWarning1 = convertMinuteToHour(entity.monthlyAgreementHourWarning1);
			param.monthlyAgreementHourWarning2 = convertMinuteToHour(entity.monthlyAgreementHourWarning2);
			param.monthlyAgreementHourLimit = convertMinuteToHour(entity.monthlyAgreementHourLimit);
			param.monthlyAgreementHourWarningSpecial1 = convertMinuteToHour(entity.monthlyAgreementHourWarningSpecial1);
			param.monthlyAgreementHourWarningSpecial2 = convertMinuteToHour(entity.monthlyAgreementHourWarningSpecial2);
			param.monthlyAgreementHourLimitSpecial = convertMinuteToHour(entity.monthlyAgreementHourLimitSpecial);

			return param;
		}
	}


}
