/*
 * @author TeamSpirit Inc.
 * 
 * @date 2018
 * 
 * @description Repository for Expense Journal
 * 
 * @group Expense
 */
public with sharing class ExpJournalRepository extends Repository.BaseRepository {

	/** Number of Extended Items for each type */
	private static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/** String representation of Extended Item Field of the sObject */
	private static final String RECORD_ITEM = ExpJournal__c.RecordItemExtendedItemLookup01__c.getDescribe().getName().substringBefore('ExtendedItem');
	private static final String REPORT = ExpJournal__c.ReportExtendedItemLookup01__c.getDescribe().getName().substringBefore('ExtendedItem');
	private static final String REQUEST = ExpJournal__c.RequestExtendedItemLookup01__c.getDescribe().getName().substringBefore('ExtendedItem');
	private static final String EXTENDED_ITEM_DATE = 'ExtendedItemDate';
	private static final String EXTENDED_ITEM_TEXT = 'ExtendedItemText';
	private static final String EXTENDED_ITEM_PICKLIST = 'ExtendedItemPicklist';
	private static final String EXTENDED_ITEM_LOOKUP = 'ExtendedItemLookup';
	private static final String FIELD_POSTFIX= '__c';

	/*
	 * Sorting Order for Journal
	 */
	public enum SortOrder {
		JOURNAL_NO_ASC
	}

	/** List of fields from the ExpJournal__c */
	private static final List<String> GET_FIELD_NAME_LIST;

	static {
		final Set<Schema.SObjectField> fieldList = new Set<SObjectField> {
				ExpJournal__c.Id,
				ExpJournal__c.Name,
				ExpJournal__c.ApprovedDate__c,
				ExpJournal__c.AuthorizedTime__c,
				ExpJournal__c.CompanyCode__c,
				ExpJournal__c.CreditAccountCode__c,
				ExpJournal__c.CreditAccountName__c,
				ExpJournal__c.CreditAmount__c,
				ExpJournal__c.CurrencyCode__c,
				ExpJournal__c.ExchangeRate__c,
				ExpJournal__c.ExpenseTypeDebitAccountCode__c,
				ExpJournal__c.ExpenseTypeDebitAccountName__c,
				ExpJournal__c.ExpenseTypeDebitSubAccountCode__c,
				ExpJournal__c.ExpenseTypeDebitSubAccountName__c,
				ExpJournal__c.ExportedTime__c,
				ExpJournal__c.ExpReportId__c,
				ExpJournal__c.ExtractDate__c,
				ExpJournal__c.JournalNo__c,
				ExpJournal__c.LocalAmount__c,
				ExpJournal__c.PaymentDate__c,
				ExpJournal__c.Purpose__c,
				ExpJournal__c.RecordAmount__c,
				ExpJournal__c.RecordDate__c,
				ExpJournal__c.RecordId__c,
				ExpJournal__c.RecordingDate__c,
				ExpJournal__c.RecordItemAmount__c,
				ExpJournal__c.RecordItemAmountWithoutTax__c,
				ExpJournal__c.RecordItemCostCenterCode__c,
				ExpJournal__c.RecordItemCostCenterName__c,
				ExpJournal__c.RecordItemDate__c,
				ExpJournal__c.RecordItemExpenseTypeCode__c,
				ExpJournal__c.RecordItemExpenseTypeId__c,
				ExpJournal__c.RecordItemExtendedItemDate01__c,
				ExpJournal__c.RecordItemExtendedItemDate02__c,
				ExpJournal__c.RecordItemExtendedItemDate03__c,
				ExpJournal__c.RecordItemExtendedItemDate04__c,
				ExpJournal__c.RecordItemExtendedItemDate05__c,
				ExpJournal__c.RecordItemExtendedItemDate06__c,
				ExpJournal__c.RecordItemExtendedItemDate07__c,
				ExpJournal__c.RecordItemExtendedItemDate08__c,
				ExpJournal__c.RecordItemExtendedItemDate09__c,
				ExpJournal__c.RecordItemExtendedItemDate10__c,
				ExpJournal__c.RecordItemExtendedItemLookup01__c,
				ExpJournal__c.RecordItemExtendedItemLookup02__c,
				ExpJournal__c.RecordItemExtendedItemLookup03__c,
				ExpJournal__c.RecordItemExtendedItemLookup04__c,
				ExpJournal__c.RecordItemExtendedItemLookup05__c,
				ExpJournal__c.RecordItemExtendedItemLookup06__c,
				ExpJournal__c.RecordItemExtendedItemLookup07__c,
				ExpJournal__c.RecordItemExtendedItemLookup08__c,
				ExpJournal__c.RecordItemExtendedItemLookup09__c,
				ExpJournal__c.RecordItemExtendedItemLookup10__c,
				ExpJournal__c.RecordItemExtendedItemPickList01__c,
				ExpJournal__c.RecordItemExtendedItemPickList02__c,
				ExpJournal__c.RecordItemExtendedItemPickList03__c,
				ExpJournal__c.RecordItemExtendedItemPickList04__c,
				ExpJournal__c.RecordItemExtendedItemPickList05__c,
				ExpJournal__c.RecordItemExtendedItemPickList06__c,
				ExpJournal__c.RecordItemExtendedItemPickList07__c,
				ExpJournal__c.RecordItemExtendedItemPickList08__c,
				ExpJournal__c.RecordItemExtendedItemPickList09__c,
				ExpJournal__c.RecordItemExtendedItemPickList10__c,
				ExpJournal__c.RecordItemExtendedItemText01__c,
				ExpJournal__c.RecordItemExtendedItemText02__c,
				ExpJournal__c.RecordItemExtendedItemText03__c,
				ExpJournal__c.RecordItemExtendedItemText04__c,
				ExpJournal__c.RecordItemExtendedItemText05__c,
				ExpJournal__c.RecordItemExtendedItemText06__c,
				ExpJournal__c.RecordItemExtendedItemText07__c,
				ExpJournal__c.RecordItemExtendedItemText08__c,
				ExpJournal__c.RecordItemExtendedItemText09__c,
				ExpJournal__c.RecordItemExtendedItemText10__c,
				ExpJournal__c.RecordItemId__c,
				ExpJournal__c.RecordItemJobCode__c,
				ExpJournal__c.RecordItemJobName__c,
				ExpJournal__c.RecordItemRemarks__c,
				ExpJournal__c.RecordItemRowNo__c,
				ExpJournal__c.RecordItemTaxAmount__c,
				ExpJournal__c.RecordRemarks__c,
				ExpJournal__c.RecordRowNo__c,
				ExpJournal__c.Remark__c,
				ExpJournal__c.ReportAmount__c,
				ExpJournal__c.ReportApprovedDate__c,
				ExpJournal__c.ReportCostCenterCode__c,
				ExpJournal__c.ReportCostCenterName__c,
				ExpJournal__c.ReportDate__c,
				ExpJournal__c.ReportEmployeeCode__c,
				ExpJournal__c.ReportEmployeeDepartmentCode__c,
				ExpJournal__c.ReportEmployeeDepartmentName__c,
				ExpJournal__c.ReportEmployeeName__c,
				ExpJournal__c.ReportExtendedItemDate01__c,
				ExpJournal__c.ReportExtendedItemDate02__c,
				ExpJournal__c.ReportExtendedItemDate03__c,
				ExpJournal__c.ReportExtendedItemDate04__c,
				ExpJournal__c.ReportExtendedItemDate05__c,
				ExpJournal__c.ReportExtendedItemDate06__c,
				ExpJournal__c.ReportExtendedItemDate07__c,
				ExpJournal__c.ReportExtendedItemDate08__c,
				ExpJournal__c.ReportExtendedItemDate09__c,
				ExpJournal__c.ReportExtendedItemDate10__c,
				ExpJournal__c.ReportExtendedItemLookup01__c,
				ExpJournal__c.ReportExtendedItemLookup02__c,
				ExpJournal__c.ReportExtendedItemLookup03__c,
				ExpJournal__c.ReportExtendedItemLookup04__c,
				ExpJournal__c.ReportExtendedItemLookup05__c,
				ExpJournal__c.ReportExtendedItemLookup06__c,
				ExpJournal__c.ReportExtendedItemLookup07__c,
				ExpJournal__c.ReportExtendedItemLookup08__c,
				ExpJournal__c.ReportExtendedItemLookup09__c,
				ExpJournal__c.ReportExtendedItemLookup10__c,
				ExpJournal__c.ReportExtendedItemPickList01__c,
				ExpJournal__c.ReportExtendedItemPickList02__c,
				ExpJournal__c.ReportExtendedItemPickList03__c,
				ExpJournal__c.ReportExtendedItemPickList04__c,
				ExpJournal__c.ReportExtendedItemPickList05__c,
				ExpJournal__c.ReportExtendedItemPickList06__c,
				ExpJournal__c.ReportExtendedItemPickList07__c,
				ExpJournal__c.ReportExtendedItemPickList08__c,
				ExpJournal__c.ReportExtendedItemPickList09__c,
				ExpJournal__c.ReportExtendedItemPickList10__c,
				ExpJournal__c.ReportExtendedItemText01__c,
				ExpJournal__c.ReportExtendedItemText02__c,
				ExpJournal__c.ReportExtendedItemText03__c,
				ExpJournal__c.ReportExtendedItemText04__c,
				ExpJournal__c.ReportExtendedItemText05__c,
				ExpJournal__c.ReportExtendedItemText06__c,
				ExpJournal__c.ReportExtendedItemText07__c,
				ExpJournal__c.ReportExtendedItemText08__c,
				ExpJournal__c.ReportExtendedItemText09__c,
				ExpJournal__c.ReportExtendedItemText10__c,
				ExpJournal__c.ReportJobCode__c,
				ExpJournal__c.ReportJobName__c,
				ExpJournal__c.ReportNo__c,
				ExpJournal__c.ReportPaymentDueDate__c,
				ExpJournal__c.ReportSubject__c,
				ExpJournal__c.ReportType__c,
				ExpJournal__c.ReportVendorCode__c,
				ExpJournal__c.RequestAmount__c,
				ExpJournal__c.RequestCostCenterCode__c,
				ExpJournal__c.RequestCostCenterName__c,
				ExpJournal__c.RequestDate__c,
				ExpJournal__c.RequestEmployeeCode__c,
				ExpJournal__c.RequestEmployeeDepartmentCode__c,
				ExpJournal__c.RequestEmployeeDepartmentName__c,
				ExpJournal__c.RequestEmployeeName__c,
				ExpJournal__c.RequestExtendedItemDate01__c,
				ExpJournal__c.RequestExtendedItemDate02__c,
				ExpJournal__c.RequestExtendedItemDate03__c,
				ExpJournal__c.RequestExtendedItemDate04__c,
				ExpJournal__c.RequestExtendedItemDate05__c,
				ExpJournal__c.RequestExtendedItemDate06__c,
				ExpJournal__c.RequestExtendedItemDate07__c,
				ExpJournal__c.RequestExtendedItemDate08__c,
				ExpJournal__c.RequestExtendedItemDate09__c,
				ExpJournal__c.RequestExtendedItemDate10__c,
				ExpJournal__c.RequestExtendedItemLookup01__c,
				ExpJournal__c.RequestExtendedItemLookup02__c,
				ExpJournal__c.RequestExtendedItemLookup03__c,
				ExpJournal__c.RequestExtendedItemLookup04__c,
				ExpJournal__c.RequestExtendedItemLookup05__c,
				ExpJournal__c.RequestExtendedItemLookup06__c,
				ExpJournal__c.RequestExtendedItemLookup07__c,
				ExpJournal__c.RequestExtendedItemLookup08__c,
				ExpJournal__c.RequestExtendedItemLookup09__c,
				ExpJournal__c.RequestExtendedItemLookup10__c,
				ExpJournal__c.RequestExtendedItemPickList01__c,
				ExpJournal__c.RequestExtendedItemPickList02__c,
				ExpJournal__c.RequestExtendedItemPickList03__c,
				ExpJournal__c.RequestExtendedItemPickList04__c,
				ExpJournal__c.RequestExtendedItemPickList05__c,
				ExpJournal__c.RequestExtendedItemPickList06__c,
				ExpJournal__c.RequestExtendedItemPickList07__c,
				ExpJournal__c.RequestExtendedItemPickList08__c,
				ExpJournal__c.RequestExtendedItemPickList09__c,
				ExpJournal__c.RequestExtendedItemPickList10__c,
				ExpJournal__c.RequestExtendedItemText01__c,
				ExpJournal__c.RequestExtendedItemText02__c,
				ExpJournal__c.RequestExtendedItemText03__c,
				ExpJournal__c.RequestExtendedItemText04__c,
				ExpJournal__c.RequestExtendedItemText05__c,
				ExpJournal__c.RequestExtendedItemText06__c,
				ExpJournal__c.RequestExtendedItemText07__c,
				ExpJournal__c.RequestExtendedItemText08__c,
				ExpJournal__c.RequestExtendedItemText09__c,
				ExpJournal__c.RequestExtendedItemText10__c,
				ExpJournal__c.RequestJobCode__c,
				ExpJournal__c.RequestJobName__c,
				ExpJournal__c.RequestNo__c,
				ExpJournal__c.RequestReportType__c,
				ExpJournal__c.RequestSubject__c,
				ExpJournal__c.ScheduleDate__c,
				ExpJournal__c.TaxTypeCode__c,
				ExpJournal__c.TaxTypeId__c,
				ExpJournal__c.TaxTypeName__c
			};
		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/*
	 * Search filter for Expense Journal Records
	 */
	public class SearchFilter {
		/** Expense Journal No */
		public Set<String> journalNos;
		/** Expense Report Ids which the Journal is created based upon */
		public Set<Id> reportIds;
	}

	/*
	 * Search the Expense Journal Records based on the filter. Sorting order is NOT guarantee.
	 *
	 * @param filter contains the search criteria
	 * @param forUpdate specify whether the search is for update or not
	 *
	 * @return List<ExpJournalEntity> list of ExpenseJournalEntities which met the search criteria. Empty list if not found any.
	 */
	public List<ExpJournalEntity> searchEntity(SearchFilter filter, Boolean forUpdate) {
		return searchEntity(filter, forUpdate, null);
	}


	/*
	 * Search the Expense Journal Records based on the filter and sort order.
	 *
	 * @param filter contains the search criteria
	 * @param forUpdate specify whether the search is for update or not
	 * @param order specify the sorting order
	 *
	 * @return List<ExpJournalEntity> list of ExpenseJournalEntities which met the search criteria. Empty list if not found any.
	 */
	public List<ExpJournalEntity> searchEntity(SearchFilter filter, Boolean forUpdate, SortOrder order) {
		List<String> baseWhereList = new List<String>();
		Set<String> journalNos;
		Set<Id> reportIds;
		if (filter.journalNos != null && !filter.journalNos.isEmpty()) {
			journalNos = filter.journalNos;
			baseWhereList.add(getFieldName(ExpJournal__c.JournalNo__c) + ' IN :journalNos');
		}
		if (filter.reportIds != null && !filter.reportIds.isEmpty()) {
			reportIds =  filter.reportIds;
			baseWhereList.add(getFieldName(ExpJournal__c.ExpReportId__c) + ' IN :reportIds');
		}

		List<String> selectFiledList = new List<String>();
		selectFiledList.addAll(GET_FIELD_NAME_LIST);

		String soql = String.format('SELECT {0} FROM {1} {2}',
				new String[] {
					String.join(selectFiledList, ','),
					ExpJournal__c.SObjectType.getDescribe().getName(),
					buildWhereString(baseWhereList)
				});
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else if (order != null) {
			String orderByString = ' ORDER BY ';
			//Since forUpdate cannot be used together with ORDER BY, only include ORDER BY if forUpdate is false.
			if (order == SortOrder.JOURNAL_NO_ASC) {
				soql += orderByString + getFieldName(ExpJournal__c.JournalNo__c) +  ' ASC NULLS LAST';
			}
		}

		List<ExpJournal__c> sObjs = (List<ExpJournal__c>) Database.query(soql);

		List<ExpJournalEntity> entities = new List<ExpJournalEntity>();
		for (ExpJournal__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}
		return entities;
	}

	/*
	 * Convert and save the entity list to the DB.'
	 * Only the fields which are marked as 'changed' in the entity will be updated/saved to the DB.
	 *
	 * @param entityList list of ExpJournalEntity to save to DB
	 * @param allOrNone specify whether it should be successful or fail when some of the records failed.
	 *
	 * @return Repository.SaveResult result of the Upsert
	 */
	public Repository.SaveResult saveEntityList(List<ExpJournalEntity> entityList, Boolean allOrNone) {
		List<ExpJournal__c> objList = new List<ExpJournal__c>();

		// Create sObjects from Entity
		for (ExpJournalEntity entity : entityList) {
			ExpJournal__c obj = createObj(entity);
			objList.add(obj);
		}

		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(objList, allOrNone);

		return resultFactory.createSaveResult(recResList);
	}

	/*
	 * Create Entity from the Journal sOBject
	 * @param journal ExpJournal__c sOBject
	 * @return ExpJournalEntity journal entity created from the sObject
	 */
	private ExpJournalEntity createEntity(ExpJournal__c journal) {
		ExpJournalEntity entity = new ExpJournalEntity();

		entity.setId(journal.Id);
		entity.name = journal.Name;

		//Report Fields
		entity.AuthorizedTime = AppDatetime.valueOf(journal.AuthorizedTime__c);
		entity.CompanyCode = journal.CompanyCode__c;
		entity.CreditAccountCode = journal.CreditAccountCode__c;
		entity.CreditAccountName = journal.CreditAccountName__c;
		entity.CreditAmount = journal.CreditAmount__c;
		entity.CurrencyCode = journal.CurrencyCode__c;
		entity.ExchangeRate = journal.ExchangeRate__c;
		entity.ExpenseTypeDebitAccountCode = journal.ExpenseTypeDebitAccountCode__c;
		entity.ExpenseTypeDebitAccountName = journal.ExpenseTypeDebitAccountName__c;
		entity.ExpenseTypeDebitSubAccountCode = journal.ExpenseTypeDebitSubAccountCode__c;
		entity.ExpenseTypeDebitSubAccountName = journal.ExpenseTypeDebitSubAccountName__c;
		entity.ExportedTime = AppDatetime.valueOf(journal.ExportedTime__c);
		entity.expReportId = journal.ExpReportId__c;
		entity.ExtractDate = AppDate.valueOf(journal.ExtractDate__c);
		entity.JournalNo = journal.JournalNo__c;
		entity.LocalAmount = journal.LocalAmount__c;
		entity.PaymentDate = AppDate.valueOf(journal.PaymentDate__c);
		entity.RecordAmount = journal.RecordAmount__c;
		entity.RecordDate = AppDate.valueOf(journal.RecordDate__c);
		entity.RecordId = journal.RecordId__c;
		entity.RecordingDate = AppDate.valueOf(journal.RecordingDate__c);
		entity.RecordItemAmount = journal.RecordItemAmount__c;
		entity.RecordItemAmountWithoutTax = journal.RecordItemAmountWithoutTax__c;
		entity.RecordItemCostCenterCode = journal.RecordItemCostCenterCode__c;
		entity.RecordItemCostCenterName = journal.RecordItemCostCenterName__c;
		entity.RecordItemDate = AppDate.valueOf(journal.RecordItemDate__c);
		entity.RecordItemExpenseTypeCode = journal.RecordItemExpenseTypeCode__c;
		entity.RecordItemExpenseTypeId = journal.RecordItemExpenseTypeId__c;

		entity.RecordItemId = journal.RecordItemId__c;
		entity.RecordItemJobCode = journal.RecordItemJobCode__c;
		entity.RecordItemJobName = journal.RecordItemJobName__c;
		entity.recordItemRemarks = journal.RecordItemRemarks__c;
		entity.recordItemRowNo = Integer.valueOf(journal.RecordItemRowNo__c);
		entity.RecordItemTaxAmount = journal.RecordItemTaxAmount__c;
		entity.recordRemarks = journal.RecordRemarks__c;
		entity.recordRowNo = Integer.valueOf(journal.RecordRowNo__c);
		entity.Remark = journal.Remark__c;
		entity.ReportAmount = journal.ReportAmount__c;
		entity.ReportApprovedDate = AppDate.valueOf(journal.ReportApprovedDate__c);
		entity.ReportCostCenterCode = journal.ReportCostCenterCode__c;
		entity.ReportCostCenterName = journal.ReportCostCenterName__c;
		entity.ReportDate = AppDate.valueOf(journal.ReportDate__c);
		entity.ReportEmployeeCode = journal.ReportEmployeeCode__c;
		entity.ReportEmployeeDepartmentCode = journal.ReportEmployeeDepartmentCode__c;
		entity.ReportEmployeeDepartmentName = journal.ReportEmployeeDepartmentName__c;
		entity.ReportEmployeeName = journal.ReportEmployeeName__c;
		entity.ReportJobCode = journal.ReportJobCode__c;
		entity.ReportJobName = journal.ReportJobName__c;
		entity.ReportNo = journal.ReportNo__c;
		entity.ReportPaymentDueDate = AppDate.valueOf(journal.ReportPaymentDueDate__c);
		entity.ReportSubject = journal.ReportSubject__c;
		entity.ReportType = journal.ReportType__c;
		entity.ReportVendorCode = journal.ReportVendorCode__c;
		entity.TaxTypeCode = journal.TaxTypeCode__c;
		entity.TaxTypeId = journal.TaxTypeId__c;
		entity.TaxTypeName = journal.TaxTypeName__c;

		//Request Fields
		entity.ApprovedDate = AppDate.valueOf(journal.ApprovedDate__c);
		entity.Purpose = journal.Purpose__c;
		entity.RequestAmount = journal.RequestAmount__c;
		entity.RequestCostCenterCode = journal.RequestCostCenterCode__c;
		entity.RequestCostCenterName = journal.RequestCostCenterName__c;
		entity.RequestDate = AppDate.valueOf(journal.RequestDate__c);
		entity.RequestEmployeeCode = journal.RequestEmployeeCode__c;
		entity.RequestEmployeeDepartmentCode = journal.RequestEmployeeDepartmentCode__c;
		entity.RequestEmployeeDepartmentName = journal.RequestEmployeeDepartmentName__c;
		entity.RequestEmployeeName = journal.RequestEmployeeName__c;
		entity.RequestJobCode = journal.RequestJobCode__c;
		entity.RequestJobName = journal.RequestJobName__c;
		entity.RequestNo = journal.RequestNo__c;
		entity.requestReportType = journal.RequestReportType__c;
		entity.RequestSubject = journal.RequestSubject__c;
		entity.ScheduleDate = AppDate.valueOf(journal.ScheduleDate__c);


		// Extended Items
		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');
			// Report
			entity.setReportExtendedItemDate(i, (Date) journal.get(REPORT + EXTENDED_ITEM_DATE + index + FIELD_POSTFIX));
			entity.setReportExtendedItemText(i, (String) journal.get(REPORT + EXTENDED_ITEM_TEXT + index + FIELD_POSTFIX));
			entity.setReportExtendedItemPicklist(i, (String) journal.get(REPORT + EXTENDED_ITEM_PICKLIST + index + FIELD_POSTFIX));
			entity.setReportExtendedItemLookup(i, (String) journal.get(REPORT + EXTENDED_ITEM_LOOKUP + index + FIELD_POSTFIX));

			// Record Item
			entity.setRecordItemExtendedItemDate(i, (Date) journal.get(RECORD_ITEM + EXTENDED_ITEM_DATE + index + FIELD_POSTFIX));
			entity.setRecordItemExtendedItemText(i, (String) journal.get(RECORD_ITEM + EXTENDED_ITEM_TEXT + index + FIELD_POSTFIX));
			entity.setRecordItemExtendedItemPicklist(i, (String) journal.get(RECORD_ITEM + EXTENDED_ITEM_PICKLIST + index + FIELD_POSTFIX));
			entity.setRecordItemExtendedItemLookup(i, (String) journal.get(RECORD_ITEM + EXTENDED_ITEM_LOOKUP + index + FIELD_POSTFIX));

			// Request
			entity.setRequestExtendedItemDate(i, (Date) journal.get(REQUEST + EXTENDED_ITEM_DATE + index + FIELD_POSTFIX));
			entity.setRequestExtendedItemText(i, (String) journal.get(REQUEST + EXTENDED_ITEM_TEXT + index + FIELD_POSTFIX));
			entity.setRequestExtendedItemPicklist(i, (String) journal.get(REQUEST + EXTENDED_ITEM_PICKLIST + index + FIELD_POSTFIX));
			entity.setRequestExtendedItemLookup(i, (String) journal.get(REQUEST + EXTENDED_ITEM_LOOKUP + index + FIELD_POSTFIX));
		}

		entity.resetChanged();

		return entity;
	}

	/*
	 * Create ExpJournal__c sObject from the ExpJournalEntity.
	 * Only the changed fields will be included in the created sOBject.
	 *
	 * @param entity ExpJournalEntity instance with the changed fields marked with isChanged flag
	 * @return ExpJournal__c sObject with the changed values from the Entity copied over.
	 */
	private ExpJournal__c createObj(ExpJournalEntity entity) {
		ExpJournal__c retObj = new ExpJournal__c();

		retObj.Id = entity.id;
		if (entity.isChanged(ExpJournalEntity.Field.Name)) {
			retObj.Name = entity.name;
		}

		//Report Fields
		if (entity.isChanged(ExpJournalEntity.Field.AuthorizedTime)) {
			retObj.AuthorizedTime__c = entity.AuthorizedTime.getDatetime();
		}
		if (entity.isChanged(ExpJournalEntity.Field.CompanyCode)) {
			retObj.CompanyCode__c = entity.CompanyCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.CreditAccountCode)) {
			retObj.CreditAccountCode__c = entity.CreditAccountCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.CreditAccountName)) {
			retObj.CreditAccountName__c = entity.CreditAccountName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.CreditAmount)) {
			retObj.CreditAmount__c = entity.CreditAmount;
		}
		if (entity.isChanged(ExpJournalEntity.Field.CurrencyCode)) {
			retObj.CurrencyCode__c = entity.CurrencyCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExchangeRate)) {
			retObj.ExchangeRate__c = entity.ExchangeRate;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExpenseTypeDebitAccountCode)) {
			retObj.ExpenseTypeDebitAccountCode__c = entity.ExpenseTypeDebitAccountCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExpenseTypeDebitAccountName)) {
			retObj.ExpenseTypeDebitAccountName__c = entity.ExpenseTypeDebitAccountName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExpenseTypeDebitSubAccountCode)) {
			retObj.ExpenseTypeDebitSubAccountCode__c = entity.ExpenseTypeDebitSubAccountCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExpenseTypeDebitSubAccountName)) {
			retObj.ExpenseTypeDebitSubAccountName__c = entity.ExpenseTypeDebitSubAccountName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExportedTime)) {
			retObj.ExportedTime__c = entity.ExportedTime.getDatetime();
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExpReportId)) {
			retObj.ExpReportId__c = entity.expReportId;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ExtractDate)) {
			retObj.ExtractDate__c = entity.ExtractDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.Field.JournalNo)) {
			retObj.JournalNo__c = entity.JournalNo;
		}
		if (entity.isChanged(ExpJournalEntity.Field.LocalAmount)) {
			retObj.LocalAmount__c = entity.LocalAmount;
		}
		if (entity.isChanged(ExpJournalEntity.Field.PaymentDate)) {
			retObj.PaymentDate__c = entity.PaymentDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordAmount)) {
			retObj.RecordAmount__c = entity.RecordAmount;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordDate)) {
			retObj.RecordDate__c = entity.RecordDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordId)) {
			retObj.RecordId__c = entity.RecordId;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordingDate)) {
			retObj.RecordingDate__c = entity.RecordingDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemAmount)) {
			retObj.RecordItemAmount__c = entity.RecordItemAmount;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemAmountWithoutTax)) {
			retObj.RecordItemAmountWithoutTax__c = entity.RecordItemAmountWithoutTax;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemCostCenterCode)) {
			retObj.RecordItemCostCenterCode__c = entity.RecordItemCostCenterCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemCostCenterName)) {
			retObj.RecordItemCostCenterName__c = entity.RecordItemCostCenterName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemDate)) {
			retObj.RecordItemDate__c = entity.RecordItemDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemExpenseTypeCode)) {
			retObj.RecordItemExpenseTypeCode__c = entity.RecordItemExpenseTypeCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemExpenseTypeId)) {
			retObj.RecordItemExpenseTypeId__c = entity.RecordItemExpenseTypeId;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemExtendedItemDateList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(RECORD_ITEM + EXTENDED_ITEM_DATE + index + FIELD_POSTFIX, entity.getRecordItemExtendedItemDate(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemExtendedItemLookupList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(RECORD_ITEM + EXTENDED_ITEM_LOOKUP + index + FIELD_POSTFIX, entity.getRecordItemExtendedItemLookup(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemExtendedItemPicklistList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(RECORD_ITEM + EXTENDED_ITEM_PICKLIST + index + FIELD_POSTFIX, entity.getRecordItemExtendedItemPicklist(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemExtendedItemTextList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(RECORD_ITEM + EXTENDED_ITEM_TEXT + index + FIELD_POSTFIX, entity.getRecordItemExtendedItemText(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemId)) {
			retObj.RecordItemId__c = entity.RecordItemId;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemJobCode)) {
			retObj.RecordItemJobCode__c = entity.RecordItemJobCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemJobName)) {
			retObj.RecordItemJobName__c = entity.RecordItemJobName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemRemarks)) {
			retObj.RecordItemRemarks__c = entity.recordItemRemarks;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemRowNo)) {
			retObj.RecordItemRowNo__c = entity.recordItemRowNo;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordItemTaxAmount)) {
			retObj.RecordItemTaxAmount__c = entity.RecordItemTaxAmount;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordRemarks)) {
			retObj.RecordRemarks__c = entity.recordRemarks;
		}
		if (entity.isChanged(ExpJournalEntity.Field.RecordRowNo)) {
			retObj.RecordRowNo__c = entity.recordRowNo;
		}
		if (entity.isChanged(ExpJournalEntity.Field.Remark)) {
			retObj.Remark__c = entity.Remark;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportAmount)) {
			retObj.ReportAmount__c = entity.ReportAmount;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportApprovedDate)) {
			retObj.ReportApprovedDate__c = entity.ReportApprovedDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportCostCenterCode)) {
			retObj.ReportCostCenterCode__c = entity.ReportCostCenterCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportCostCenterName)) {
			retObj.ReportCostCenterName__c = entity.ReportCostCenterName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportDate)) {
			retObj.ReportDate__c = entity.ReportDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportEmployeeCode)) {
			retObj.ReportEmployeeCode__c = entity.ReportEmployeeCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportEmployeeDepartmentCode)) {
			retObj.ReportEmployeeDepartmentCode__c = entity.ReportEmployeeDepartmentCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportEmployeeDepartmentName)) {
			retObj.ReportEmployeeDepartmentName__c = entity.ReportEmployeeDepartmentName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportEmployeeName)) {
			retObj.ReportEmployeeName__c = entity.ReportEmployeeName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportExtendedItemDateList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REPORT + EXTENDED_ITEM_DATE + index + FIELD_POSTFIX, entity.getReportExtendedItemDate(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportExtendedItemLookupList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REPORT + EXTENDED_ITEM_LOOKUP + index + FIELD_POSTFIX, entity.getReportExtendedItemLookup(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportExtendedItemPicklistList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REPORT + EXTENDED_ITEM_PICKLIST + index + FIELD_POSTFIX, entity.getReportExtendedItemPicklist(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportExtendedItemTextList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REPORT + EXTENDED_ITEM_TEXT + index + FIELD_POSTFIX, entity.getReportExtendedItemText(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportJobCode)) {
			retObj.ReportJobCode__c = entity.ReportJobCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportJobName)) {
			retObj.ReportJobName__c = entity.ReportJobName;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportNo)) {
			retObj.ReportNo__c = entity.ReportNo;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportPaymentDueDate)) {
			retObj.ReportPaymentDueDate__c = AppDate.convertDate(entity.ReportPaymentDueDate);
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportSubject)) {
			retObj.ReportSubject__c = entity.ReportSubject;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportType)) {
			retObj.ReportType__c = entity.ReportType;
		}
		if (entity.isChanged(ExpJournalEntity.Field.ReportVendorCode)) {
			retObj.ReportVendorCode__c = entity.ReportVendorCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.TaxTypeCode)) {
			retObj.TaxTypeCode__c = entity.TaxTypeCode;
		}
		if (entity.isChanged(ExpJournalEntity.Field.TaxTypeId)) {
			retObj.TaxTypeId__c = entity.TaxTypeId;
		}
		if (entity.isChanged(ExpJournalEntity.Field.TaxTypeName)) {
			retObj.TaxTypeName__c = entity.TaxTypeName;
		}

		//Request Fields
		if (entity.isChanged(ExpJournalEntity.RequestField.ApprovedDate)) {
			retObj.ApprovedDate__c = entity.ApprovedDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.Purpose)) {
			retObj.Purpose__c = entity.Purpose;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestAmount)) {
			retObj.RequestAmount__c = entity.RequestAmount;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestCostCenterCode)) {
			retObj.RequestCostCenterCode__c = entity.RequestCostCenterCode;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestCostCenterName)) {
			retObj.RequestCostCenterName__c = entity.RequestCostCenterName;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestDate)) {
			retObj.RequestDate__c = entity.RequestDate.getDate();
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestEmployeeCode)) {
			retObj.RequestEmployeeCode__c = entity.RequestEmployeeCode;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestEmployeeDepartmentCode)) {
			retObj.RequestEmployeeDepartmentCode__c = entity.RequestEmployeeDepartmentCode;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestEmployeeDepartmentName)) {
			retObj.RequestEmployeeDepartmentName__c = entity.RequestEmployeeDepartmentName;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestEmployeeName)) {
			retObj.RequestEmployeeName__c = entity.RequestEmployeeName;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestExtendedItemDateList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REQUEST + EXTENDED_ITEM_DATE + index + FIELD_POSTFIX, entity.getRequestExtendedItemDate(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestExtendedItemLookupList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REQUEST + EXTENDED_ITEM_LOOKUP + index + FIELD_POSTFIX, entity.getRequestExtendedItemLookup(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestExtendedItemPicklistList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REQUEST + EXTENDED_ITEM_PICKLIST + index + FIELD_POSTFIX, entity.getRequestExtendedItemPicklist(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestExtendedItemTextList)) {
			for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
				String index = String.valueOf(i + 1).leftPad(2, '0');
				retObj.put(REQUEST + EXTENDED_ITEM_TEXT + index + FIELD_POSTFIX, entity.getRequestExtendedItemText(i));
			}
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestJobCode)) {
			retObj.RequestJobCode__c = entity.RequestJobCode;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestJobName)) {
			retObj.RequestJobName__c = entity.RequestJobName;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestNo)) {
			retObj.RequestNo__c = entity.RequestNo;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestReportType)) {
			retObj.RequestReportType__c = entity.requestReportType;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.RequestSubject)) {
			retObj.RequestSubject__c = entity.RequestSubject;
		}
		if (entity.isChanged(ExpJournalEntity.RequestField.ScheduleDate)) {
			retObj.ScheduleDate__c = entity.ScheduleDate.getDate();
		}

		return retObj;
	}

	public Decimal getNewJournalNo() {
		Decimal journalNo = 0;

		for (ExpJournal__c lastJournal : [
				SELECT JournalNo__c
				FROM ExpJournal__c
				ORDER BY JournalNo__c DESC NULLS LAST
				LIMIT 1]) {
			if (lastJournal.JournalNo__c != null) {
				journalNo = Decimal.valueOf(lastJournal.JournalNo__c);
			}
		}

		return journalNo + 1;
	}
}