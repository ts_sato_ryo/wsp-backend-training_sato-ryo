/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分サービス Service class for tax type master
 */
public with sharing class ExpTaxTypeService extends ParentChildService {

	/** メッセージ Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 税区分リポジトリ Tax type repository */
	private ExpTaxTypeRepository taxTypeRepo = new ExpTaxTypeRepository();

	/**
	 * コンストラクタ Constructor
	 */
	public ExpTaxTypeService() {
		super(new ExpTaxTypeRepository());
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntity(Id baseId) {
		// ベースに紐づく全ての履歴を取得する
		return taxTypeRepo.getEntity(baseId);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildBaseEntity> getEntityList(List<Id> baseIds) {
		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAllDate = null; // 全ての履歴が対象
		return taxTypeRepo.getEntityList(baseIds, targetAllDate);
	}

	/**
	 * TODO 廃止予定 マスタは会社コード+マスタコードで一意となるため
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntityByCode(String code) {
		return null;
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildHistoryEntity getHistoryEntity(Id historyId) {
		// ベースに紐づく全ての履歴を取得する
		return taxTypeRepo.getHistoryEntity(historyId);
	}

	/**
	 * 指定した履歴IDの履歴エンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		// ベースに紐づく全ての履歴を取得する
		return taxTypeRepo.getHistoryEntityList(historyIds);
	}

	/**
	 * ベースエンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveBaseEntity(ParentChildBaseEntity base) {
		return (new ExpConfigValidator.ExpTaxTypeBaseValidator((ExpTaxTypeBaseEntity)base)).validate();
	}

	/**
	 * 履歴エンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveHistoryEntity(ParentChildBaseEntity base, ParentChildHistoryEntity history) {
		return (new ExpConfigValidator.ExpTaxTypeHistoryValidator((ExpTaxTypeBaseEntity)base, (ExpTaxTypeHistoryEntity)history)).validate();
	}

	/**
	 * 履歴エンティティが持つnameL0を取得する
	 * @param history 取得対象の履歴エンティティ
	 * @return 履歴エンティティのnameL0、ただしnameL0を持っていない場合はnull
	 */
	protected override String getNameL0FromHistory(ParentChildHistoryEntity history) {
		return ((ExpTaxTypeHistoryEntity)history).nameL0;
	}

	/**
	 * エンティティのコードが重複しているかどうかを確認する(会社単位での重複チェック)
	 * @param base チェック対象のベースエンティティ
	 * @param companyCode 会社コード
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	@TestVisible
	protected override Boolean isCodeDuplicated(ParentChildBaseEntity base) {
		ExpTaxTypeBaseEntity taxTypeBase = (ExpTaxTypeBaseEntity)base;
		ExpTaxTypeBaseEntity duplicateTaxType = getEntityByCode(taxTypeBase.code, taxTypeBase.companyId);

		if (duplicateTaxType == null) {
			// 重複していない
			return false;
		}
		if (duplicateTaxType.id == taxTypeBase.id) {
			// 重複していない
			return false;
		}

		// ここまできたら重複している
		return true;
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	private ExpTaxTypeBaseEntity getEntityByCode(String code, Id companyId) {
		ExpTaxTypeRepository.SearchFilter filter = new ExpTaxTypeRepository.SearchFilter();
		filter.codes = new List<String>{code};
		filter.companyIds = new List<Id>{companyId};
		List<ExpTaxTypeBaseEntity> entityList = taxTypeRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * エンティティのユニークキーを更新する
	 *  Update/Regenerate the UniqKey accordingly to the changed fields.
	 * (会社のコードが変更されている可能性があるため、常に更新を行う)
	 * @param base ユニークキーを作成するエンティティ
	 */
	public void updateUniqKey(ExpTaxTypeBaseEntity base) {
		Id countryId = base.countryId;
		Id companyId = base.companyId;
		String code = base.code;

		if ((base.id != null) && ((countryId == null) || (companyId == null) || (String.isBlank(code)))) {
			// 既存のエンティティを取得する
			ExpTaxTypeBaseEntity orgBase = taxTypeRepo.getBaseEntity(base.id);
			if (orgBase == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			if (! base.isChanged(ExpTaxTypeBaseGeneratedEntity.FIELD_COUNTRY_ID)) {
				countryId = orgBase.countryId;
			}
			if (! base.isChanged(ParentChildBaseCompanyEntity.FIELD_NAME_COMPANY_ID)) {
				companyId = orgBase.companyId;
			}
			if (! base.isChanged(ParentChildBaseCompanyEntity.FIELD_NAME_CODE)) {
				code = orgBase.code;
			}
		}

		// 国IDが設定されている場合は国の税区分とみなす
		if (countryId != null) {
			// ユニークキーはレコードID（更新時にベースIDを設定する、新規作成時は便宜的に国IDを設定する）
			base.uniqKey = base.id == null ? countryId : base.id;
			return;
		}

		if (companyId == null ) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Company}));
		}
		if (String.isBlank(code)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}));
		}
		CompanyEntity company = getCompany(companyId);
		base.uniqKey = base.createUniqKey(company.code, code);
	}

	/**
	 * 指定した日に有効な税区分履歴をMapで取得する
	 * @param baseIds 税区分ベースIDのリスト
	 * @param targetDate 対象日
	 * @return 税区分履歴のMap(キーは税区分ベースID)
	 */
	public Map<Id, ExpTaxTypeHistoryEntity> getHistoryMap(List<Id> baseIds, AppDate targetDate) {
		List<ExpTaxTypeBaseEntity> bases = new ExpTaxTypeRepository().getEntityList(baseIds, targetDate);
		Map<Id, ExpTaxTypeHistoryEntity> historyMap = new Map<Id, ExpTaxTypeHistoryEntity>();
		for (ExpTaxTypeBaseEntity base : bases) {
			List<ExpTaxTypeHistoryEntity> histories = base.getHistoryList();
			if (histories != null && !histories.isEmpty()) {
				historyMap.put(base.id, histories[0]);
			}
		}
		return historyMap;
	}
}