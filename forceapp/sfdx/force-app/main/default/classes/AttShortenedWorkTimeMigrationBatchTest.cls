/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 勤怠
 *
 * 短時間勤務実績時間移行バッチのテスト
 */
@isTest
private class AttShortenedWorkTimeMigrationBatchTest {

	/**
	 * @description バッチ実行テスト(正常系)
	 * 勤怠サマリの短時間勤務実績時間が計算され、保存されることを確認する
	 */
	@isTest static void executeTestSuccess() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_FLEX);
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.valueOf('2019-05-01');
		// 短時間勤務設定を作成
		AttShortTimeSettingBaseEntity shortSettingBase = testData.createShortSetting(AttWorkSystem.JP_FLEX, 'test', null);
		// 勤怠期間を作成
		AttPeriodStatusEntity periodStatus = testData.createShortTimePeriodStatus(null, employee.id, targetDate, targetDate.addMonths(1), shortSettingBase.id);
		// 起算日
		AppDate startDate = AppDate.newInstance(targetDate.year(), targetDate.month(), workingType.startDateOfMonth);
		// 勤怠サマリを作成（短時間勤務を適用したサマリ）
		AttSummaryEntity summary = new AttAttendanceService().getSummary(employee.id, startDate);
		summary.outActualShortenedWorkTime = null;
		summary.outShortableWorkTime = new AttDailyTime(1200);
		summary.outPlainTime = new AttDailyTime(8600);
		summary.outVirtualWorkTime = new AttDailyTime(0);
		summary.outContractedWorkHours = new AttDailyTime(9600);
		new AttSummaryRepository().saveEntity(summary);

		// 実行
		AttShortenedWorkTimeMigrationBatch batch = new AttShortenedWorkTimeMigrationBatch();
		Test.startTest();
		Database.executeBatch(batch);
		Test.stopTest();

		// 検証
		AttSummaryEntity summaryResult = new AttSummaryRepository().getEntityList(null, true, false)[0];
		System.assert(summaryResult.isShortWorkTimeEvenOneDay());
		// 短時間勤務実績が計算されている
		System.assertEquals(1000, AttDailyTime.convertInt(summaryResult.outActualShortenedWorkTime));
	}

	/**
	 * @description バッチ実行テスト(正常系)
	 * 短時間勤務が適用されていない勤怠サマリは計算されないことを確認する
	 */
	@isTest static void executeTestSuccessNotAppliedSummary() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_FLEX);
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.valueOf('2019-05-01');
		// 短時間勤務設定を作成
		AttShortTimeSettingBaseEntity shortSettingBase = testData.createShortSetting(AttWorkSystem.JP_FLEX, 'test', null);
		// 勤怠期間を作成
		AttPeriodStatusEntity periodStatus = testData.createShortTimePeriodStatus(null, employee.id, targetDate, targetDate.addMonths(1), shortSettingBase.id);
		// 起算日（短時間勤務が適用されている勤怠期間に該当しない日）
		AppDate startDate = AppDate.newInstance(targetDate.addMonths(1).year(), targetDate.addMonths(1).month(), workingType.startDateOfMonth);
		// 勤怠サマリを作成（短時間勤務が適用されていないサマリ）
		AttSummaryEntity summary = new AttAttendanceService().getSummary(employee.id, startDate);
		summary.outActualShortenedWorkTime = null;
		summary.outShortableWorkTime = new AttDailyTime(1200);
		summary.outPlainTime = new AttDailyTime(8600);
		summary.outVirtualWorkTime = new AttDailyTime(0);
		summary.outContractedWorkHours = new AttDailyTime(9600);
		new AttSummaryRepository().saveEntity(summary);

		// 実行
		AttShortenedWorkTimeMigrationBatch batch = new AttShortenedWorkTimeMigrationBatch();
		Test.startTest();
		Database.executeBatch(batch);
		Test.stopTest();

		// 検証
		AttSummaryEntity summaryResult = new AttSummaryRepository().getEntityList(null, true, false)[0];
		System.assert(!summaryResult.isShortWorkTimeEvenOneDay());
		// 短時間勤務実績が計算されていない
		System.assertEquals(null, AttDailyTime.convertInt(summaryResult.outActualShortenedWorkTime));
	}

	/**
	 * @description バッチ実行テスト(正常系)
	 * フレックス制以外の労働時間制の勤怠サマリは計算されないことを確認する
	 */
	@isTest static void executeTestSuccessOnlyFlex() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_FIX);
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		EmployeeBaseEntity employee = testData.employee;

		// 対象日
		AppDate targetDate = AppDate.valueOf('2019-05-01');
		// 短時間勤務設定を作成（固定労働時間制）
		AttShortTimeSettingBaseEntity shortSettingBase = testData.createShortSetting(AttWorkSystem.JP_FIX, 'test', null);
		// 勤怠期間を作成
		AttPeriodStatusEntity periodStatus = testData.createShortTimePeriodStatus(null, employee.id, targetDate, targetDate.addMonths(1), shortSettingBase.id);
		// 起算日
		AppDate startDate = AppDate.newInstance(targetDate.year(), targetDate.month(), workingType.startDateOfMonth);
		// 勤怠サマリを作成（短時間勤務を適用したサマリ）
		AttSummaryEntity summary = new AttAttendanceService().getSummary(employee.id, startDate);
		summary.outActualShortenedWorkTime = null;
		summary.outShortableWorkTime = new AttDailyTime(1200);
		summary.outPlainTime = new AttDailyTime(8600);
		summary.outVirtualWorkTime = new AttDailyTime(0);
		summary.outContractedWorkHours = new AttDailyTime(9600);
		new AttSummaryRepository().saveEntity(summary);

		// 実行
		AttShortenedWorkTimeMigrationBatch batch = new AttShortenedWorkTimeMigrationBatch();
		Test.startTest();
		Database.executeBatch(batch);
		Test.stopTest();

		// 検証
		AttSummaryEntity summaryResult = new AttSummaryRepository().getEntityList(null, true, false)[0];
		System.assert(summaryResult.isShortWorkTimeEvenOneDay());
		// 短時間勤務実績時間は計算されない
		System.assertEquals(null, AttDailyTime.convertInt(summaryResult.outActualShortenedWorkTime));
	}
}