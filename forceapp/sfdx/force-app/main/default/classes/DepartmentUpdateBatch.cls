/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 既存部署更新バッチ処理
 */
public with sharing class DepartmentUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {

	/** インポートバッチID */
	private Id importBatchId;

	/** 既存の会社データ */
	@TestVisible
	private Map<String, CompanyEntity> companyMap;
	/** 既存の部署データ */
	@TestVisible
	// キーは会社コード・部署コード
	private Map<List<String>, DepartmentBaseEntity> deptMap;
	/** 既存の社員データ */
	@TestVisible
	// キーは会社コード・社員コード
	private Map<List<String>, EmployeeBaseEntity> empMap;

	/** 成功件数 */
	private Integer successCount = 0;
	/** 失敗件数 */
	private Integer errorCount = 0;
	/** 例外発生 */
	private Boolean exceptionOccurred = false;


	/**
	 * コンストラクタ
	 * @param importBatchId インポートバッチID
	 */
	public DepartmentUpdateBatch(Id importBatchId) {
		this.importBatchId = importBatchId;
	}

	/**
	 * バッチ開始処理
	 * @param bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {
		// 部署インポートデータを取得
		String query = 'SELECT Id, ' +
							'Name, ' +
							'ImportBatchId__c, ' +
							'RevisionDate__c, ' +
							'AbolishmentDate__c, ' +
							'Code__c, ' +
							'CompanyCode__c, ' +
							'Name_L0__c, ' +
							'Name_L1__c, ' +
							'Name_L2__c, ' +
							'ManagerBaseCode__c, ' +
							'AssistantManager1Code__c, ' +
							'AssistantManager2Code__c, ' +
							'ParentBaseCode__c, ' +
							'HistoryComment__c, ' +
							'Status__c, ' +
							'Error__c ' +
						'FROM ComDepartmentImport__c ' +
						'WHERE ImportBatchId__c = :importBatchId ' +
						'AND Status__c IN (\'Waiting\', \'Error\') ' +
						'ORDER BY Code__c, RevisionDate__c, AbolishmentDate__c, CreatedDate';

		return Database.getQueryLocator(query);
	}

	/**
	 * バッチ実行処理
	 * @param bc バッチコンテキスト
	 * @param scope バッチ処理対象レコードのリスト
	 */
	public void execute(Database.BatchableContext bc, List<ComDepartmentImport__c> scope) {
		this.companyMap = new Map<String, CompanyEntity>();
		this.deptMap = new Map<List<String>, DepartmentBaseEntity>();
		this.empMap = new Map<List<String>, EmployeeBaseEntity>();

		List<DepartmentImportEntity> deptImpList = new List<DepartmentImportEntity>();
		List<DepartmentImportEntity> deptImpValidPeriodList = new List<DepartmentImportEntity>();

		Set<String> companyCodeSet = new Set<String>();
		Set<String> deptCodeSet = new Set<String>();
		Set<String> empCodeSet = new Set<String>();
		for (ComDepartmentImport__c obj : scope) {

			DepartmentImportEntity entity = this.createDeptImpEntity(obj);

			// 廃止日が設定されている場合は有効期間終了日の更新を行う
			// 廃止日と改定日が両方設定されている場合は廃止日が優先される
			if (entity.abolishmentDate != null) {
				deptImpValidPeriodList.add(entity);
			} else {
				deptImpList.add(entity);
			}

			// 参照先のコードを取得する
			String code = ImportBatchService.convertValue(entity.code);
			if (String.isNotEmpty(code)) {
				deptCodeSet.add(code);
			}
			String companyCode = ImportBatchService.convertValue(entity.companyCode);
			if (String.isNotEmpty(companyCode)) {
				companyCodeSet.add(companyCode);
			}
			String managerBaseCode = ImportBatchService.convertValue(entity.managerBaseCode);
			if (String.isNotEmpty(managerBaseCode)) {
				empCodeSet.add(managerBaseCode);
			}
			String assistantManager1Code = ImportBatchService.convertValue(entity.assistantManager1Code);
			if (String.isNotEmpty(assistantManager1Code)) {
				empCodeSet.add(assistantManager1Code);
			}
			String assistantManager2Code = ImportBatchService.convertValue(entity.assistantManager2Code);
			if (String.isNotEmpty(assistantManager2Code)) {
				empCodeSet.add(assistantManager2Code);
			}
			String parentBaseCode = ImportBatchService.convertValue(entity.parentBaseCode);
			if (String.isNotEmpty(parentBaseCode)) {
				deptCodeSet.add(parentBaseCode);
			}
		}

		// 参照先のデータを取得
		// 会社
		if (companyCodeSet.size() > 0) {
			CompanyRepository companyRepo = new CompanyRepository();
			for (CompanyEntity company : companyRepo.getEntityList(new List<String>(companyCodeSet))) {
				this.companyMap.put(company.code, company);
			}
		}
		// 部署
		DepartmentService deptService = new DepartmentService();
		if (deptCodeSet.size() > 0) {
			for (ParentChildBaseEntity dept : deptService.getEntityListByCode(new List<String>(deptCodeSet))) {
				List<String> key = new List<String>{((DepartmentBaseEntity)dept).company.code, ((DepartmentBaseEntity)dept).code};
				this.deptMap.put(key, (DepartmentBaseEntity)dept);
			}
		}
		// 社員
		if (empCodeSet.size() > 0) {
			EmployeeService empService = new EmployeeService();
			for (ParentChildBaseEntity emp : empService.getEntityListByCode(new List<String>(empCodeSet))) {
				List<String> key = new List<String>{((EmployeeBaseEntity)emp).companyInfo.code, ((EmployeeBaseEntity)emp).code};
				this.empMap.put(key, (EmployeeBaseEntity)emp);
			}
		}

		// 部署データの更新
		updateDepartmentList(deptImpList);

		// 部署の有効期間終了日の更新
		updateValidEndDateList(deptImpValidPeriodList);
	}

	/**
	 * バッチ終了処理
	 * @param bc バッチコンテキスト
	 */
	public void finish(Database.BatchableContext bc) {
		// インポートバッチデータを更新
		ImportBatchRepository batchRepo = new ImportBatchRepository();
		ImportBatchEntity batch = batchRepo.getEntity(this.importBatchId);
		batch.status = this.exceptionOccurred ? ImportBatchStatus.FAILED : ImportBatchStatus.PROCESSING;
		batch.successCount += this.successCount;
		batch.failureCount += this.errorCount;
		batchRepo.saveEntity(batch);

		if (!this.exceptionOccurred) {
			if (!Test.isRunningTest()) {
				// 次の処理を呼び出し（既存社員更新）
				EmployeeUpdateBatch empUpdate = new EmployeeUpdateBatch(importBatchId);
				Database.executeBatch(empUpdate, ImportBatchService.BATCH_SIZE_EMP_UPDATE);
			}
		}
	}

	//--------------------------------------------------------------------------------

	/**
	 * 部署インポートエンティティを作成する
	 * @param obj 部署インポートオブジェクトデータ
	 * @return 部署インポートエンティティ
	 */
	private DepartmentImportEntity createDeptImpEntity(ComDepartmentImport__c obj) {
		DepartmentImportEntity entity = new DepartmentImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.abolishmentDate = AppDate.valueOf(obj.AbolishmentDate__c);
		entity.code = obj.Code__c;
		entity.companyCode = obj.CompanyCode__c;
		entity.name_L0 = obj.Name_L0__c;
		entity.name_L1 = obj.Name_L1__c;
		entity.name_L2 = obj.Name_L2__c;
		entity.managerBaseCode = obj.ManagerBaseCode__c;
		entity.assistantManager1Code = obj.AssistantManager1Code__c;
		entity.assistantManager2Code = obj.AssistantManager2Code__c;
		entity.parentBaseCode = obj.ParentBaseCode__c;
		entity.historyComment = obj.HistoryComment__c;
		entity.status = ImportStatus.valueOf(obj.Status__c);
		entity.error = obj.Error__c;

		entity.resetChanged();
		return entity;
	}

	/**
	 * バリデーションを行う(部署データ更新用)
	 */
	@TestVisible
	private void validateDeptImpEntityForUpdate(DepartmentImportEntity deptImp) {
		// 既存データ
		List<String> deptMapKey = new List<String>{deptImp.companyCode, deptImp.code};
		DepartmentBaseEntity dept = this.deptMap.get(deptMapKey);

		// 部署名(L0)がクリアされている
		String name_L0 = ImportBatchService.convertValue(deptImp.name_L0);
		if (String.isNotBlank(deptImp.name_L0) && String.isBlank(name_L0)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Batch_Err_CanNotClear(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentName});
			this.errorCount++;
			return;
		}

		String companyCode = ImportBatchService.convertValue(deptImp.companyCode);

		// 管理者
		String managerBaseCode = ImportBatchService.convertValue(deptImp.managerBaseCode);
		if (String.isNotBlank(managerBaseCode)) {
			if (!validateManager(companyCode, managerBaseCode, deptImp.revisionDate)) {
				deptImp.status = ImportStatus.Error;
				deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentManagerCode});
				return;
			}
		}
		// 補助管理者1
		String assistantManager1Code = ImportBatchService.convertValue(deptImp.assistantManager1Code);
		if (String.isNotBlank(assistantManager1Code)) {
			if (!validateManager(companyCode, assistantManager1Code, deptImp.revisionDate)) {
				deptImp.status = ImportStatus.Error;
				deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentAssistantManager1Code});
				return;
			}
		}
		// 補助管理者2
		String assistantManager2Code = ImportBatchService.convertValue(deptImp.assistantManager2Code);
		if (String.isNotBlank(assistantManager2Code)) {
			if (!validateManager(companyCode, assistantManager2Code, deptImp.revisionDate)) {
				deptImp.status = ImportStatus.Error;
				deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentAssistantManager2Code});
				return;
			}
		}
		// 親部署
		String parentBaseCode = ImportBatchService.convertValue(deptImp.parentBaseCode);
		if (String.isNotBlank(parentBaseCode)) {
			List<String> key = new List<String>{companyCode, parentBaseCode};

			// 存在しない
			if (!this.deptMap.containsKey(key)) {
				deptImp.status = ImportStatus.Error;
				deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_ParentDepartmentCode});
				return;
			}
			// 有効開始日 > 改訂日
			if (this.deptMap.containsKey(key) && this.deptMap.get(key).getSuperHistoryByDate(deptImp.revisionDate) == null) {
				deptImp.status = ImportStatus.Error;
				deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_ParentDepartmentCode});
				return;
			}
		}
	}

	/**
	 * バリデーションを行う(有効期間更新用)
	 */
	@TestVisible
	private void validateDeptImpEntityForValidPeriod(DepartmentImportEntity deptImp) {

		// 会社コードが未設定
		String companyCode = ImportBatchService.convertValue(deptImp.companyCode);
		if (String.isBlank(companyCode)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			return;
		}
		// 指定した会社が存在しない
		if (!this.companyMap.containsKey(companyCode)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			return;
		}

		// 部署コードが未設定
		String deptCode = ImportBatchService.convertValue(deptImp.code);
		if (String.isBlank(deptCode)) {
			deptImp.status = ImportStatus.Error;
			deptImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentCode});
			return;
		}
		// 指定した部署が存在しない
		List<String> deptMapKey = new List<String>{companyCode, deptCode};
		if (!this.deptMap.containsKey(deptMapKey)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentCode});
			return;
		}
	}

	private Boolean validateManager(String companyCode, String managerCode, AppDate revisionDate) {
		List<String> key = new List<String>{companyCode, managerCode};

		// 存在しない
		if (!this.empMap.containsKey(key)) {
			return false;
		}
		// 有効開始日 > 改訂日
		if (this.empMap.containsKey(key) && this.empMap.get(key).getSuperHistoryByDate(revisionDate) == null) {
			return false;
		}
		return true;
	}
	/**
	 * 部署エンティティを作成する（ベース）
	 */
	private DepartmentBaseEntity createDeptBaseEntity(DepartmentImportEntity deptImp) {
		DepartmentBaseEntity base = new DepartmentBaseEntity();

		String companyCode = ImportBatchService.convertValue(deptImp.companyCode);
		String code = ImportBatchService.convertValue(deptImp.code);
		List<String> deptMapKey = new List<String>{companyCode, code};
		base.setId(this.deptMap.get(deptMapKey).id);
		base.code = code;
		base.uniqKey = base.createUniqKey(companyCode, code);
		base.companyId = this.companyMap.get(companyCode).id;
		if (String.isNotBlank(deptImp.name_L0)) {
			base.name = ImportBatchService.convertValue(deptImp.name_L0);
		}

		return base;
	}

	/**
	 * 部署エンティティを作成する（履歴）
	 */
	private DepartmentHistoryEntity createDeptHistoryEntity(DepartmentImportEntity deptImp) {
		DepartmentHistoryEntity history = new DepartmentHistoryEntity();

		String companyCode = ImportBatchService.convertValue(deptImp.companyCode);
		String code = ImportBatchService.convertValue(deptImp.code);
		List<String> deptMapKey = new List<String>{companyCode, code};
		history.baseId = this.deptMap.get(deptMapKey).id;
		if (String.isNotBlank(deptImp.name_L0)) {
			history.name = ImportBatchService.convertValue(deptImp.name_L0);
			history.nameL0 = ImportBatchService.convertValue(deptImp.name_L0);
		}
		if (String.isNotBlank(deptImp.name_L1)) {
			history.nameL1 = ImportBatchService.convertValue(deptImp.name_L1);
		}
		if (String.isNotBlank(deptImp.name_L2)) {
			history.nameL2 = ImportBatchService.convertValue(deptImp.name_L2);
		}
		if (String.isNotBlank(deptImp.managerBaseCode)) {
			String managerBaseCode = ImportBatchService.convertValue(deptImp.managerBaseCode);
			List<String> empMapKey = new List<String>{companyCode, managerBaseCode};
			history.managerId = (managerBaseCode == null) ? null : this.empMap.get(empMapKey).id;
		}
		if (String.isNotBlank(deptImp.assistantManager1Code)) {
			String assistantManager1Code = ImportBatchService.convertValue(deptImp.assistantManager1Code);
			List<String> empMapKey = new List<String>{companyCode, assistantManager1Code};
			history.assistantManager1Id = (assistantManager1Code == null) ? null : this.empMap.get(empMapKey).id;
		}
		if (String.isNotBlank(deptImp.assistantManager2Code)) {
			String assistantManager2Code = ImportBatchService.convertValue(deptImp.assistantManager2Code);
			List<String> empMapKey = new List<String>{companyCode, assistantManager2Code};
			history.assistantManager2Id = (assistantManager2Code == null) ? null : this.empMap.get(empMapKey).id;
		}
		if (String.isNotBlank(deptImp.parentBaseCode)) {
			String parentBaseCode = ImportBatchService.convertValue(deptImp.parentBaseCode);
			deptMapKey = new List<String>{companyCode, parentBaseCode};
			history.parentBaseId = (parentBaseCode == null) ? null : this.deptMap.get(deptMapKey).id;
		}

		// 項目(履歴管理項目は除く)に値が設定されたかをチェック
		Boolean isChanged = false;
		final Set<String> historyManagementFieldSet = ParentChildHistoryEntity.HISTORY_MANAGEMENT_FIELD_SET;
		for (Schema.SObjectField field : history.getChangedFieldSet()) {
			if (! historyManagementFieldSet.contains(field.getDescribe().getName())) {
				isChanged = true;
				break;
			}
		}
		// 値が全く設定されていない場合はnullを返却（改訂処理は行わない）
		if (!isChanged) {
			return null;
		}

		// 履歴コメントは必ず設定する
		history.historyComment = ImportBatchService.convertValue(deptImp.historyComment);
		history.validFrom = deptImp.revisionDate;

		return history;
	}

	/**
	 * 部署マスタを更新する
	 * @param deptImpList 更新対象の部署インポートデータ
	 */
	private void updateDepartmentList(List<DepartmentImportEntity> deptImpList) {
		Savepoint sp = Database.setSavepoint();

		// 既存データの更新
		DepartmentService deptService = new DepartmentService();
		for (DepartmentImportEntity deptImp : deptImpList) {
			try {
				// 既存データが存在しない or エラーがある場合は何もしない
				String companyCode = ImportBatchService.convertValue(deptImp.companyCode);
				String deptCode = ImportBatchService.convertValue(deptImp.code);
				List<String> deptMapKey = new List<String>{companyCode, deptCode};
				if (!this.deptMap.containsKey(deptMapKey) || ImportStatus.Error.equals(deptImp.status)) {
					continue;
				}

				// バリデーション
				this.validateDeptImpEntityForUpdate(deptImp);

				// エラーがある場合は何もしない
				if (ImportStatus.Error.equals(deptImp.status)) {
					this.errorCount++;
					continue;
				}

				// 更新用の部署エンティティを作成
				DepartmentBaseEntity deptBase = this.createDeptBaseEntity(deptImp);
				DepartmentHistoryEntity deptHistory = this.createDeptHistoryEntity(deptImp);

				// 更新
				deptService.updateBase(deptBase);
				if (deptHistory != null) {
					deptService.reviseHistory(deptHistory);
				}

				deptImp.status = ImportStatus.SUCCESS;
				deptImp.error = null;
				this.successCount++;
			} catch (App.BaseException e) {
				deptImp.status = ImportStatus.ERROR;
				deptImp.error = e.getMessage();
				this.errorCount++;
			} catch (Exception e) {
System.debug('*** ERROR e=' + e);
System.debug('*** ERROR e=' + e.getStackTraceString());
				Database.rollback(sp);
				deptImp.status = ImportStatus.ERROR;
				deptImp.error = e.getMessage();
				this.errorCount++;
				this.exceptionOccurred = true;
				break;
			}
		}

		// 予期せぬ例外が発生した場合は成功したインポートデータのステータスを戻す
		if (this.exceptionOccurred) {
			for (DepartmentImportEntity deptImp : deptImpList) {
				if (ImportStatus.SUCCESS.equals(deptImp.status)) {
					deptImp.status = ImportStatus.WAITING;
					this.successCount--;
				}
			}
		}

		// 部署インポートデータを更新
		DepartmentImportRepository deptImpRepo = new DepartmentImportRepository();
		deptImpRepo.saveEntityList(deptImpList);
	}

	/**
	 * 社員マスタの有効期間終了日を更新する
	 * @param empImpList 更新対象の社員インポートデータ
	 */
	private void updateValidEndDateList(List<DepartmentImportEntity> deptImpList) {
		Savepoint sp = Database.setSavepoint();

		// 有効期間終了日の更新
		DepartmentService deptService = new DepartmentService();
		for (DepartmentImportEntity deptImp : deptImpList) {
			try {
				// バリデーション
				this.validateDeptImpEntityForValidPeriod(deptImp);

				// バリデーションエラーがある場合は失敗件数をインクリメント
				if (ImportStatus.Error.equals(deptImp.status)) {
					this.errorCount++;
				}

				// 既存データが存在しない or エラーがある場合は何もしない
				String companyCode = ImportBatchService.convertValue(deptImp.companyCode);
				String deptCode = ImportBatchService.convertValue(deptImp.code);
				List<String> deptMapKey = new List<String>{companyCode, deptCode};
				if (!this.deptMap.containsKey(deptMapKey) || ImportStatus.Error.equals(deptImp.status)) {
					continue;
				}
				// 有効期間終了日を更新(廃止日の翌日が失効日)
				Id baseId = this.deptMap.get(deptMapKey).id;
				AppDate validToDate = deptImp.abolishmentDate.addDays(1);
				deptService.updateBaseValidToDate(baseId, validToDate);

				deptImp.status = ImportStatus.SUCCESS;
				deptImp.error = null;
				this.successCount++;
			} catch (App.BaseException e) {
				deptImp.status = ImportStatus.ERROR;
				deptImp.error = e.getMessage();
				this.errorCount++;
			} catch (Exception e) {
				System.debug(LoggingLevel.DEBUG, e.getStackTraceString());
				Database.rollback(sp);
				deptImp.status = ImportStatus.ERROR;
				deptImp.error = e.getMessage();
				this.errorCount++;
				this.exceptionOccurred = true;
				break;
			}
		}

		// 予期せぬ例外が発生した場合は成功したインポートデータのステータスを戻す
		if (this.exceptionOccurred) {
			for (DepartmentImportEntity deptImp : deptImpList) {
				if (ImportStatus.SUCCESS.equals(deptImp.status)) {
					deptImp.status = ImportStatus.WAITING;
					this.successCount--;
				}
			}
		}

		// 部署インポートデータを更新
		DepartmentImportRepository deptImpRepo = new DepartmentImportRepository();
		deptImpRepo.saveEntityList(deptImpList);
	}
}