/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description ジョブサービスクラス
*/
public with sharing class JobService implements Service {
	/** ジョブ階層最大数 */
	private static final Integer MAX_LEVEL = 10;
	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * ジョブを保存する
	 */
	public Id saveJobEntity(JobEntity savedEntity) {
		JobRepository jobRepo = new JobRepository();

		Boolean isParentChanged = false; // 親ジョブ変更有無フラグ
		// 保存用のエンティティを作成
		JobEntity entity;
		if (savedEntity.id == null) {
			// 新規の場合
			entity = savedEntity;
		} else {
			// 更新の場合は既存の値を取得する
			entity = jobRepo.getEntity(savedEntity.id);
			if (entity == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			isParentChanged = entity.parentId != savedEntity.parentId;
			// 更新値を上書きする
			for (Schema.SObjectField field : savedEntity.getChangedFieldSet()) {
				entity.setFieldValue(field, savedEntity.getFieldValue(field));
			}
		}

		// バリデーション実行
		Validator.Result validResult = (new ComConfigValidator.JobValidator(entity)).validate();
		if (! validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// ジョブコードの重複チェック
		JobEntity duplicateCodeEntity = jobRepo.getEntityByCode(entity.code, entity.companyId);
		if ((duplicateCodeEntity != null) && (duplicateCodeEntity.id != entity.id)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		// ユニークキーを設定する
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		entity.uniqKey = entity.createUniqKey(company.code);

		// 保存する
		Repository.SaveResult result = jobRepo.saveEntity(entity);

		// 再帰チェック
		Boolean isNew = entity.Id == null;
		Id jobId = result.details[0].id;
		entity.setId(jobId);
		if (isNew || isParentChanged) {
			validateCircular(entity, isNew);
		}

		return jobId;
	}

	/**
	 * デフォルトジョブを取得する。
	 * @param employee 取得対象の社員
	 * @return デフォルトジョブ デフォルトジョブの設定が存在しない場合はnull
	 */
	public JobEntity getDefaultJob(EmployeeBaseEntity employee) {
		PersonalSettingEntity personalSetting = new PersonalSettingRepository().getPersonalSetting(employee.id);
		if (personalSetting == null || personalSetting.defaultJobId == null) {
			return null;
		}
		return new JobRepository().getEntity(personalSetting.defaultJobId);
	}

	/**
	 * 社員が参照できる経費のジョブを全て取得する
	 * 取得条件は以下の通り
	 * １．経費選択可能なジョブである
	 * ２．有効期間内のジョブである
	 * ３．ジョブが個別設定の場合、社員に割当済み且つ、割当期間内である
	 *
	 * @param employee 対象社員
	 * @param targetDate 対象日
	 * @return ジョブの一覧（存在しない場合は空のリスト）
	 */
	public List<JobEntity> getActiveExpenseJob(EmployeeBaseEntity employee, AppDate targetDate) {
		return getActiveExpenseJob(employee, targetDate, null);
	}

	/**
	 * 親ジョブを指定して、社員が参照できる経費のジョブを全て取得する
	 * 取得条件は以下の通り
	 * １．経費選択可能なジョブである
	 * ２．有効期間内のジョブである
	 * ３．ジョブが個別設定の場合、社員に割当済み且つ、割当期間内である
	 *
	 * @param employee 対象社員
	 * @param targetDate 対象日
	 * @param parentId 親ジョブ（未指定の場合は条件に含めない）
	 * @return ジョブの一覧（存在しない場合は空のリスト）
	 */
	public List<JobEntity> getActiveExpenseJob(EmployeeBaseEntity employee, AppDate targetDate, Id parentId) {
		// 会社共通の経費のジョブを取得
		JobRepository repository = new JobRepository();
		JobRepository.SearchFilter companyJobFilter = new JobRepository.SearchFilter();
		companyJobFilter.companyIds = new Set<Id>{employee.companyId};
		companyJobFilter.isScopedAssignment = false;
		companyJobFilter.isSelectableExpense = true;
		companyJobFilter.targetDate = targetDate;
		if (parentId != null) {
			companyJobFilter.parentIds = new Set<Id>{parentId};
		}
		List<JobEntity> jobs = repository.searchEntity(companyJobFilter);

		// ジョブ割当を取得
		List<JobAssignEntity> jobAssigns = new JobAssignService().searchJobAssign(employee.id, targetDate);

		// アサイン個別指定のジョブを取得
		JobRepository.SearchFilter assignedFilter = new JobRepository.SearchFilter();
		assignedFilter.ids = new Set<Id>();
		for (JobAssignEntity jobAssign : jobAssigns) {
			assignedFilter.ids.add(jobAssign.jobId);
		}
		assignedFilter.isScopedAssignment = true;
		assignedFilter.isSelectableExpense = true;
		assignedFilter.targetDate = targetDate;
		if (parentId != null) {
			assignedFilter.parentIds = new Set<Id>{parentId};
		}
		jobs.addAll(repository.searchEntity(assignedFilter));
		List<JobEntity> sortedJobs = sortJobList(jobs);

		// 参照可能なジョブのみ抽出する
		return extractActiveJob(sortedJobs, jobAssigns, new AppDateRange(targetDate, targetDate)).values()[0];
	}

	/**
	 * 社員が参照できる工数のジョブを全て取得する
	 * 取得条件は以下の通り
	 * １．工数選択可能なジョブである
	 * ２．有効期間内のジョブである
	 * ３．ジョブが個別設定の場合、社員に割当済み且つ、割当期間内である
	 *
	 * @param employee 対象社員
	 * @param targetDate 対象日
	 * @return ジョブの一覧（存在しない場合は空のリスト）
	 */
	public List<JobEntity> getActiveTimeTrackJob(EmployeeBaseEntity employee, AppDate targetDate) {
		return getActiveTimeTrackJob(employee, targetDate, null);
	}

	/**
	 * 親ジョブを指定して、社員が参照できる工数のジョブを全て取得する
	 * 取得条件は以下の通り
	 * １．工数選択可能なジョブである
	 * ２．有効期間内のジョブである
	 * ３．ジョブが個別設定の場合、社員に割当済み且つ、割当期間内である
	 *
	 * @param employee 対象社員
	 * @param targetDate 対象日
	 * @param parentId 親ジョブ（未指定の場合は条件に含めない）
	 * @return ジョブの一覧（存在しない場合は空のリスト）
	 */
	public List<JobEntity> getActiveTimeTrackJob(EmployeeBaseEntity employee, AppDate targetDate, Id parentId) {
		return getActiveTimeTrackJob(employee, new AppDateRange(targetDate, targetDate), parentId).values()[0];
	}

	/**
	 * 社員が参照できる工数のジョブを全て取得する
	 * 取得条件は以下の通り
	 * １．工数選択可能なジョブである
	 * ２．有効期間内のジョブである（取得対象日の範囲のうち、1日でも有効なもの）
	 * ３．ジョブが個別設定の場合、社員に割当済み且つ、割当期間内である
	 * @param employee 取得対象の社員
	 * @param dateRange 取得対象日の範囲
	 * @return 日付ごとのジョブの一覧
	 */
	public Map<AppDate, List<JobEntity>> getActiveTimeTrackJob(EmployeeBaseEntity employee, AppDateRange dateRange) {
		return getActiveTimeTrackJob(employee, dateRange, null);
	}

	/**
	 * 社員が参照できる工数のジョブを全て取得する
	 * 取得条件は以下の通り
	 * １．工数選択可能なジョブである
	 * ２．有効期間内のジョブである（取得対象日の範囲のうち、1日でも有効なもの）
	 * ３．ジョブが個別設定の場合、社員に割当済み且つ、割当期間内である
	 * @param employee 取得対象の社員
	 * @param dateRange 取得対象日の範囲
	 * @return 日付ごとのジョブの一覧
	 */
	private Map<AppDate, List<JobEntity>> getActiveTimeTrackJob(EmployeeBaseEntity employee, AppDateRange dateRange, Id parentId) {
		// 会社共通の工数のジョブを取得
		JobRepository repository = new JobRepository();
		JobRepository.SearchFilter companyJobFilter = new JobRepository.SearchFilter();
		companyJobFilter.companyIds = new Set<Id>{employee.companyId};
		companyJobFilter.isScopedAssignment = false;
		companyJobFilter.isSelectableTimeTrack = true;
		companyJobFilter.dateRange = dateRange;
		if (parentId != null) {
			companyJobFilter.parentIds = new Set<Id>{parentId};
		}
		List<JobEntity> jobs = repository.searchEntity(companyJobFilter);

		// ジョブ割当を取得
		List<JobAssignEntity> jobAssigns = new JobAssignService().searchJobAssign(employee.id, dateRange.startDate, dateRange.endDate);

		// アサイン個別指定のジョブを取得
		JobRepository.SearchFilter assignedFilter = new JobRepository.SearchFilter();
		assignedFilter.ids = new Set<Id>();
		for (JobAssignEntity jobAssign : jobAssigns) {
			assignedFilter.ids.add(jobAssign.jobId);
		}
		assignedFilter.isScopedAssignment = true;
		assignedFilter.isSelectableTimeTrack = true;
		assignedFilter.dateRange = dateRange;
		if (parentId != null) {
			assignedFilter.parentIds = new Set<Id>{parentId};
		}
		jobs.addAll(repository.searchEntity(assignedFilter));
		List<JobEntity> sortedJobs = sortJobList(jobs);

		// 参照可能なジョブのみ抽出する
		return extractActiveJob(sortedJobs, jobAssigns, dateRange);
	}

	/**
	 * Search Job based on the param defined
	 * If returnHierarchyNameMap is null it indicates no further filtering based on employee group or active is requried
	 * @param entityId Job Id
	 * @param companyId Company Id
	 * @param parentId Parent Job Id
	 * @param targetDate Target Date to find
	 * @param query Job Name or Code
	 * @param returnHierarchyNameMap If 'null' it indicates call from Admin Page and no filtering is done, if not 'null' return Found Job's parent names
	 * @return List of Job Entity found
	 */
	public List<JobEntity> searchJobList(Id entityId, Id companyId, Id parentId, AppDate targetDate, String query, Map<Id, List<String>> returnHierarchyNameMap,String empId) {
		EmployeeBaseEntity employee = null;
		if (returnHierarchyNameMap != null) {
			employee = getEmployee(empId, UserInfo.getUserId(), targetDate);
		}

		return searchJobList(constructIdSetFromString(entityId), constructIdSetFromString(companyId),
			 constructIdSetFromString(parentId), targetDate, query, returnHierarchyNameMap, employee);
	}

	/**
	 * Search Job
	 * If returnHierarchyNameMap is null it indicates no further filtering based on employee group or active is requried
	 * @param entityIdSet Set of Job Id
	 * @param companyIdSet Set of Company Id
	 * @param parentIdSet Set of Parent Job Id
	 * @param targetDate Target Date to find
	 * @param query Job Name or Code
	 * @param returnHierarchyNameMap If 'null' it indicates call from Admin Page and no filtering is done, if not 'null' return Found Job's parent names
	 * @param employee Employee Base
	 * @return List of Job Entity found
	 */
	public List<JobEntity> searchJobList(Set<Id> entityIdSet, Set<Id> companyId, Set<Id> parentIdSet, AppDate targetDate, String query, Map<Id, List<String>> returnHierarchyNameMap, EmployeeBaseEntity employee) {
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.ids = entityIdSet;
		filter.companyIds = companyId;
		filter.parentIds = parentIdSet;
		filter.targetDate = targetDate;
		filter.query = query;

		List<JobEntity> searchRecordList = new JobRepository().searchEntity(filter);

		if (returnHierarchyNameMap != null) { // Means it has usedIn and need to get hierarchy parent names and filter job based on Employee
			returnHierarchyNameMap.putAll(filterActiveJobWithParentName(searchRecordList, targetDate, employee));
		}

		return searchRecordList;
	}

	/**
	 * Get Recently Used Job List based on employee Id
	 * @param employeeId Employee Base Id, can be null
	 * @param returnHierarchyNameMap Return found parents hierarchy name list
	 * @return Recently Used Job Entity List
	 */
	public List<JobEntity> getRecentlyUsedJobList(Id employeeId, Map<Id, List<String>> returnHierarchyNameMap, AppDate targetDate) {
		EmployeeBaseEntity employee = null;
		List<JobEntity> resultJobList = new List<JobEntity>();
		if (employeeId != null){
			employee = ExpCommonUtil.getEmployeeBaseEntity(employeeId);
		}
		ExpRecentlyUsedService expRecentlyUsedService = new ExpRecentlyUsedService();
		ExpRecentlyUsedEntity expRecUsedEntity = expRecentlyUsedService.getExistingExpRecentlyUsedEntity(employeeId, ExpRecentlyUsedEntity.MasterType.Job, null);

		if (expRecUsedEntity != null) {
			List<Id> recentlyUsedOrderedIdList = expRecUsedEntity.getRecentlyUsedValues();
			Set<Id> entityIdSet = new Set<Id>(recentlyUsedOrderedIdList);
			List<JobEntity> tempJobList = this.searchJobList(entityIdSet, null, null, targetDate, null, returnHierarchyNameMap, employee);

			// To reorder based on the recentlyUsedOrderedIdList make the found job to Map
			Map<Id, JobEntity> jobEntityMap = new Map<Id, JobEntity>();
			for (JobEntity job : tempJobList) {
				jobEntityMap.put(job.id, job);
			}

			for (Id jobId : recentlyUsedOrderedIdList) {
				if (jobEntityMap.containsKey(jobId)) {
					resultJobList.add(jobEntityMap.get(jobId));
				}
			}

		}

		return resultJobList;
	}

	/*
	 * Return a Set containing the ID. If the ID string is blank or invalid, return null
	 */
	private Set<Id> constructIdSetFromString(String idStr) {
		if (String.isBlank(idStr) || !(idStr instanceof Id)) {
			return null;
		}
		return new Set<Id>{idStr};
	}
	/**
	 * 以下の条件で、ジョブの一覧から有効なジョブを抽出します。
	 * １．ジョブが全社公開で有効期限内である
	 * ２．ジョブが個別設定ジョブで、社員に割当済み且つ、割当期間が有効である
	 * @param jobs 抽出対象のジョブ
	 * @param jobAssigns ジョブ割当のリスト
	 * @param dateRange 取得対象日の範囲
	 * @return 日付ごとの有効なジョブの一覧
	 */
	private Map<AppDate, List<JobEntity>> extractActiveJob(List<JobEntity> jobs, List<JobAssignEntity> jobAssigns, AppDateRange dateRange) {
		Map<AppDate, List<JobEntity>> result = new Map<AppDate, List<JobEntity>>();
		for (AppDate targetDate : dateRange.values()) {
			result.put(targetDate, extractActiveJob(jobs, jobAssigns, targetDate));
		}
		return result;
	}

	private List<JobEntity> extractActiveJob(List<JobEntity> jobs, List<JobAssignEntity> jobAssigns, AppDate targetDate) {
		// 社員に割当済みのジョブを取得
		Set<Id> assignedJobs = new Set<Id>(); // ListだとIdのcontainsが動作しない
		for (JobAssignEntity jobAssign : jobAssigns) {
			// ジョブ割当が有効期間外の場合は対象外
			if (!jobAssign.isInPeriod(targetDate)) {
				continue;
			}
			assignedJobs.add(jobAssign.jobId);
		}

		List<JobEntity> activeJobs = new List<JobEntity>();
		for (JobEntity job : jobs) {
			// ジョブが有効期間外の場合は対象外
			if (!job.isValid(targetDate)) {
				continue;
			}
			// 個別設定で未割当のジョブは対象外
			if (job.isScopedAssignment && !assignedJobs.contains(job.id)) {
				continue;
			}
			activeJobs.add(job);
		}
		return activeJobs;
	}

	/**
	 * ジョブエンティティのリストを、ジョブコードの昇順でソートする
	 * @param jobs ジョブエンティティのリスト
	 * @return 新たに生成したソート済みのリスト
	 */
	@TestVisible
	private List<JobEntity> sortJobList(List<JobEntity> jobs) {
		// key:code value:JobEntity
		Map<String, JobEntity> jobMap = new Map<String, JobEntity>();
		for (JobEntity job : jobs) {
			jobMap.put(job.code, job);
		}
		// コード順にソート
		List<String> codes = new List<String>(jobMap.keySet());
		codes.sort();

		List<JobEntity> sortedList = new List<JobEntity>();
		for (String code : codes) {
			sortedList.add(jobMap.get(code));
		}
		return sortedList;
	}

	/*
	 * ジョブの階層回帰チェックを行う
	 *@param job 新規や更新されたジョブデータ
	 */
	private void validateCircular(JobEntity targetJob, Boolean isNew) {
		if (targetJob.parentId == null) {
			return;
		}
		JobRepository jobRep = new JobRepository();
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();

		CircularValidationService circularValidator = new CircularValidationService(MAX_LEVEL);
		try {
			appendParentJob(circularValidator, targetJob.Id, targetJob.parentId); // 1-2階層目
			// 対象ジョブの2~6階層までの親ジョブを取得
			filter.Ids = new Set<Id>{targetJob.Id};
			filter.includeHigherParents = true;
			Id level6ParentId = null;
			for (JobEntity job : jobRep.searchEntity(filter)) {
				appendParentJob(circularValidator, job.parentId, job.level2ParentId); // 3階層目
				appendParentJob(circularValidator, job.level2ParentId, job.level3ParentId); // 4階層目
				appendParentJob(circularValidator, job.level3ParentId, job.level4ParentId); // 5階層目
				appendParentJob(circularValidator, job.level4ParentId, job.level5ParentId); // 6階層目
				appendParentJob(circularValidator, job.level5ParentId, job.level6ParentId); // 7階層目
				if (job.level6ParentId != null) {
					level6ParentId = job.level6ParentId;
				}
			}
			// 6階層目のジョブがある場合、さらに上位ジョブを検索する
			if (level6ParentId != null) {
				filter.Ids = new Set<Id>{level6ParentId};
				for (JobEntity job : jobRep.searchEntity(filter)) {
					appendParentJob(circularValidator, job.id, job.parentId); // 8階層目
					appendParentJob(circularValidator, job.parentId, job.level2ParentId); // 9階層目
					appendParentJob(circularValidator, job.level2ParentId, job.level3ParentId); // 10階層目
					appendParentJob(circularValidator, job.level3ParentId, job.level4ParentId); // 11階層目(エラー)
				}
			}
			// 更新の場合、対象ジョブが親とするジョブの階層をチェックする(親ジョブを変更した場合のみ想定)
			if (!isNew) {
				filter.Ids = null;
				filter.parentIds = new Set<Id>{targetJob.Id};
				Id lowestJobId = null;
				Set<Id> parentIds = new Set<Id>();
				for (JobEntity job : jobRep.searchEntity(filter)) {
					parentIds.add(job.parentId);
					parentIds.add(job.level2ParentId);
					parentIds.add(job.level3ParentId);
					parentIds.add(job.level4ParentId);
					parentIds.add(job.level5ParentId);
					parentIds.add(job.level6ParentId);
					appendParentJob(circularValidator, job.level5ParentId, job.level6ParentId);
					appendParentJob(circularValidator, job.level4ParentId, job.level5ParentId);
					appendParentJob(circularValidator, job.level3ParentId, job.level4ParentId);
					appendParentJob(circularValidator, job.level2ParentId, job.level3ParentId);
					appendParentJob(circularValidator, job.parentId, job.level2ParentId);
					appendParentJob(circularValidator, job.id, job.parentId);
					// 最下位のジョブIdを記録
					if (!parentIds.contains(job.id)) {
						lowestJobId = job.id;
					}
				}
				if (lowestJobId != null) {
					filter.parentIds = new Set<Id>{lowestJobId};
					for (JobEntity job : jobRep.searchEntity(filter)) {
						appendParentJob(circularValidator, job.level5ParentId, job.level6ParentId);
						appendParentJob(circularValidator, job.level4ParentId, job.level5ParentId);
						appendParentJob(circularValidator, job.level3ParentId, job.level4ParentId);
						appendParentJob(circularValidator, job.level2ParentId, job.level3ParentId);
						appendParentJob(circularValidator, job.parentId, job.level2ParentId);
						appendParentJob(circularValidator, job.id, job.parentId);
					}
				}
			}
		} catch (CircularValidationService.CircularException circulatEx) {
			String jobId = circulatEx.circularTargetId;
			String circularTargetName = null;
			JobEntity circulatJob = jobRep.getEntity(jobId);
			throw new App.IllegalStateException(createCircularErrorMsg(circulatJob.nameL.getValue()));
		} catch (CircularValidationService.OverLevelException overLevelEx) {
			throw new App.IllegalStateException(createOverLevelErrorMsg(overLevelEx.levelLimit));
		}
	}
	/*
	 * ジョブの1階層関係を登録する
	 *@param circularValidator 再帰チェックバリデーター
	 *@param jobId 対象ジョブId
	 *@param parentId 親ジョブId
	 */
	private void appendParentJob(CircularValidationService circularValidator, Id jobId, Id parentId) {
		if (jobId == null) {
			return;
		}
		circularValidator.appendChild(jobId, parentId);
	}

	/*
	 * 階層の回帰チェックのエラーメッセージを作成する
	 *「ジョブ名」の親ジョブの設定が循環参照になっております。
	 */
	private String createCircularErrorMsg(String targetName) {
		String upObjectLabel = ComMessage.msg().Com_Lbl_ParentJob;
		return ComMessage.msg().Com_Err_CircularHierarchy(new List<String>{targetName, upObjectLabel});

	}
	/*
	 * 階層の階層数オーバーのエラーメッセージを作成する
	 * ジョブの階層が[階層数上限]を超えております。
	 */
	private String createOverLevelErrorMsg(Integer levelLimit) {
		return ComMessage.msg().Com_Err_MaxHierarchyOver(new List<String>{
				ComMessage.msg().Admin_Lbl_Job, String.valueOf(levelLimit)});
	}

	/**
	 * Filter Job Search and retrieve parent hierarchy names
	 * Note: Hierarchy Parent Job name list is ordered in ascending order (i.e. parent, grandparent, greatgrandparent)
	 * Remove Searched Job List compared against with avaliableJobList which filtered based on employee
	 * @param entityList Initial Search Result to return and is modified accordingly
	 * @param targetDate Target Search Date
	 * @param employee Employee Base Entity Can be Null
	 * @return Map where hierarchy name list linked with Job Id
	 */
	@TestVisible
	private Map<Id, List<String>> filterActiveJobWithParentName(List<JobEntity> entityList, AppDate targetDate, EmployeeBaseEntity employee) {
		Map<Id, JobEntity> avaliableEntityMap = new Map<Id, JobEntity>();
		Map <Id, List<String>> returnHierarchyMap = new Map<Id, List<String>>();
		if (employee == null) {
			employee = ExpCommonUtil.getEmployeeByUserId(UserInfo.getUserId(), targetDate);
		}

		List<JobEntity> avaliableJobList = getActiveExpenseJob(employee, targetDate);
		if (!avaliableJobList.isEmpty()) {
			for (JobEntity entity : avaliableJobList) {
				avaliableEntityMap.put(entity.id, entity);
			}

			Integer i = 0;
			while (i < entityList.size()) { // Currently this loop will do the removal of Job in entityList
				Boolean toDelete = false;
				JobEntity entity = entityList.get(i);
				Id currParentId = entity.parentId;
				// Check current Job is in the Active Job List
				if (!avaliableEntityMap.containsKey(entity.id)) {
					toDelete = true;
				} else {
					while (currParentId != null) { // Search when it has parent id
						JobEntity currJob = avaliableEntityMap.get(currParentId); // make as currJob to search
						if (currJob != null) {
							if (!returnHierarchyMap.containsKey(entity.id)) { // It's new Parent Name list to add
								returnHierarchyMap.put(entity.id, new List<String>{currJob.nameL.getValue()});
							} else { // Add on in Parent Name List
								returnHierarchyMap.get(entity.id).add(currJob.nameL.getValue());
							}
							currParentId = currJob.parentId;
						} else { // it's invalid Parent so stop finding and remove it from the return
							toDelete = true;
							currParentId = null;
						}
					}
				}

				if (toDelete) {
					returnHierarchyMap.remove(currParentId);
					entityList.remove(i);
					continue;
				}
				i ++;
			}
		} else {
			entityList.clear();
		}
		return returnHierarchyMap;

	}

	/**
	 * empIdもしくはuserIdを条件として社員情報を取得する。
	 * @param empId 検索条件の社員ID
	 * @param userId empIdが未設定の場合に検索条件とするユーザID
	 * @param targetDate 取得対象日
	 * @throw App.IllegalStateException 該当する社員が存在しない場合
	 */
	private static EmployeeBaseEntity getEmployee(Id empId, Id userId, AppDate targetDate) {
		if (empId == null) {
			return ExpCommonUtil.getEmployeeByUserId(userId, targetDate);
		}
		return ExpCommonUtil.getEmployee(empId, targetDate);
	}
}