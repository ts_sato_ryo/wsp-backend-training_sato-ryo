/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 休職休業APIを実行するクラス
 */
 public with sharing class AttLeaveOfAbsenceResource {

	/**
	 * 休職休業レコード1件の情報を保持するクラス
	 */
	public class LeaveOfAbsence {
		public String id;
		public String code;
		public String name;
		public String name_L0;
		public String name_L1;
		public String name_L2;
		public String companyId;
	}

	/**
	 * 検索APIのリクエストクラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** ID */
		public String id;
		/** 会社ID */
		public String companyId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// id
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}
			// 会社ID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}
		}
	}

	/**
	 * 検索APIのレスポンスクラス
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		public List<LeaveOfAbsence> records = new List<LeaveOfAbsence>();
	}


	/**
	 * @description 休職休業検索を実行するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 休職休業マスタを検索する
		 * @param  request リクエストパラメータ(SearchRequest)
		 * @return 検索結果(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// 入力値チェック
			SearchRequest param = (SearchRequest)request.getParam(SearchRequest.class);
			param.validate();

			// 検索
			AttLeaveOfAbsenceRepository.SearchFilter filter = new AttLeaveOfAbsenceRepository.SearchFilter();
			filter.companyId = param.companyId;
			if (param.id != null) {
				filter.ids.add(param.id);
			}
			List<AttLeaveOfAbsenceEntity> leaveOfAbsences = new AttLeaveOfAbsenceRepository().searchEntityList(filter);

			// レスポンス生成
			SearchResponse response = new SearchResponse();
			for (AttLeaveOfAbsenceEntity entity : leaveOfAbsences) {
				response.records.add(toResponse(entity));
			}
			return response;
		}

		/*
		 * エンティティをレスポンスの型に変換する
		 * @param entity 変換対象のエンティティ
		 * @return レスポンスのレコード型
		 */
		@testVisible
		private AttLeaveOfAbsenceResource.LeaveOfAbsence toResponse(AttLeaveOfAbsenceEntity entity) {
			AttLeaveOfAbsenceResource.LeaveOfAbsence result = new AttLeaveOfAbsenceResource.LeaveOfAbsence();
			result.id = entity.id;
			result.code = entity.code;
			result.name = entity.nameL.getValue();
			result.name_L0 = entity.nameL0;
			result.name_L1 = entity.nameL1;
			result.name_L2 = entity.nameL2;
			result.companyId = entity.companyId;
			return result;
		}
	}

		/**
	 * 登録APIのリクエストクラス
	 */
	public class CreateRequest implements RemoteApi.RequestParam {
		/** 会社レコードID */
		public String companyId;
		/** 休職休業コード */
		public String code;
		/** 休職休業名 (L0) */
		public String name_L0;
		/** 休職休業名 (L1) */
		public String name_L1;
		/** 休職休業名 (L2) */
		public String name_L2;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// 会社レコードID
			if (String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('companyId');
				throw e;
			}
			try {
				System.Id.valueOf(this.companyId);
			} catch (StringException e) {
				throw new App.ParameterException('companyId', companyId);
			}
			// 休職休業コード
			if (String.isBlank(this.code)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('code');
				throw e;
			}
			// 休職休業名 (L0)
			if (String.isBlank(this.name_L0)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('name_L0');
				throw e;
			}
		}
	}

	/**
	 * 登録APIのレスポンスクラス
	 */
	public class CreateResponse implements RemoteApi.ResponseParam {
		/** 作成した休職休業マスタのID */
		public String id;
	}

	/**
	 * @description 休職休業登録を実行するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE;

		/**
		 * @description 休職休業マスタを1件登録する
		 * @param  request リクエストパラメータ(CreateRequest)
		 * @return 登録結果(CreateResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request request) {

			// 入力値チェック
			CreateRequest param = (CreateRequest)request.getParam(CreateRequest.class);
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// エンティティ作成
			Map<String, Object> paramMap = request.getParamMap();
			AttLeaveOfAbsenceEntity entity = toEntity(param, paramMap);

			// コードの重複チェック
			if (AttLeaveOfAbsenceResource.isExistCode(entity, false)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
			}

			// エンティティのバリデーションチェック
			Validator.Result validResult = new AttConfigValidator.AttLeaveOfAbsenceValidator(entity).validate();
			if (!validResult.isSuccess()) {
				Validator.Error error = validResult.getErrors().get(0);
				App.ParameterException e = new App.ParameterException(error.message);
				e.setCode(error.code);
				throw e;
			}

			// 登録
			AttLeaveOfAbsenceRepository repo = new AttLeaveOfAbsenceRepository();
			Repository.SaveResult sr = repo.saveEntity(entity);

			// レスポンス作成
			Id resultId = sr.details[0].Id;
			CreateResponse res = new CreateResponse();
			res.id = resultId;
			return res;
		}

		/*
		 * リクエストをエンティティに変換する
		 * @param request 変換対象のリクエスト
		 * @return entity 変換後のエンティティ
		 */
		@testVisible
		private AttLeaveOfAbsenceEntity toEntity(CreateRequest param, Map<String, Object> paramMap) {

			AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();

			entity.companyId = param.companyId;
			entity.code = param.code;
			entity.nameL0 = param.name_L0;
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = param.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = param.name_L2;
			}
			// ユニークキー作成
			CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
			entity.uniqKey = entity.createUniqKey(company.code);
			return entity;
		}
	}

	/**
	* 更新APIのリクエストクラス
	*/
	public class UpdateRequest implements RemoteApi.RequestParam {
		/** 更新対象のレコードID */
		public String id;
		/** 休職休業コード */
		public String code;
		/** 休職休業名 (L0) */
		public String name_L0;
		/** 休職休業名 (L1) */
		public String name_L1;
		/** 休職休業名 (L2) */
		public String name_L2;

		/**
		* パラメータを検証する
		*/
		public void validate() {
			// 更新対象のレコードID
			if (String.isBlank(this.id)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('id');
				throw e;
			}
			try {
				System.Id.valueOf(this.id);
			} catch (StringException e) {
				throw new App.ParameterException('id', id);
			}
		}
	}

	/**
	* @description 休職休業更新を実行するクラス
	*/
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE;

		/**
		* @description 休職休業マスタを1件更新する
		* @param  request リクエストパラメータ(UpdateRequest)
		* @return 更新結果(null)
		*/
		public override RemoteApi.ResponseParam execute (RemoteApi.Request request) {

			// 入力値チェック
			UpdateRequest param = (UpdateRequest)request.getParam(UpdateRequest.class);
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 更新対象のレコードを検索
			AttLeaveOfAbsenceRepository repo = new AttLeaveOfAbsenceRepository();
			AttLeaveOfAbsenceEntity entity = repo.searchbyId(param.id);
			if (entity == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}

			// エンティティ作成
			Map<String, Object> paramMap = request.getParamMap();
			setEntity(param, paramMap, entity);

			// コードの重複チェック
			if (AttLeaveOfAbsenceResource.isExistCode(entity, true)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
			}

			// エンティティのバリデーションチェック
			Validator.Result validResult = new AttConfigValidator.AttLeaveOfAbsenceValidator(entity).validate();
			if (!validResult.isSuccess()) {
				Validator.Error error = validResult.getErrors().get(0);
				App.ParameterException e = new App.ParameterException(error.message);
				e.setCode(error.code);
				throw e;
			}

			// 更新
			repo.saveEntity(entity);

			// レスポンス作成
			return null;
		}

		/*
		* リクエストをエンティティに設定する
		* @param param リクエストパラメータ(UpdateRequest)
		* @param entity 更新対象のエンティティ
		*/
		@testVisible
		private void setEntity(UpdateRequest param, Map<String, Object> paramMap, AttLeaveOfAbsenceEntity entity) {

			if (paramMap.containsKey('code')) {
				entity.code = param.code;
				// ユニークキー作成
				CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
				entity.uniqKey = entity.createUniqKey(company.code);
			}
			if (paramMap.containsKey('name_L0')) {
				entity.nameL0 = param.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = param.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = param.name_L2;
			}
		}
	}

	/*
	* 会社ごとにコードが重複していないことを確認する
	* @param entity 登録対象のエンティティ
	* @param isUpdate レコード更新時はtrue、作成時はfalse
	* @return true(重複あり) / false(重複なし)
	*/
	@testVisible
	private static boolean isExistCode(AttLeaveOfAbsenceEntity entity, boolean isUpdate) {

		AttLeaveOfAbsenceRepository repo = new AttLeaveOfAbsenceRepository();
		AttLeaveOfAbsenceEntity result = repo.searchByCode(entity.companyId, entity.code);
		if (isUpdate) {
			// 更新の場合、エンティティと検索結果のidが同じであるか確認する
			return result != null && result.Id != entity.Id;
		} else {
			// 新規の場合
			return result != null;
		}
	}

	/**
	 * 削除APIのリクエストクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象のレコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// 削除対象のレコードID
			if (String.isBlank(this.id)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('id');
				throw e;
			}
			try {
				System.Id.valueOf(this.id);
			} catch (StringException e) {
				throw new App.ParameterException('id', id);
			}
		}
	}

	/**
	 * @description 休職休業削除を実行するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE;

		/**
		 * @description 休職休業マスタを1件削除する
		 * @param request リクエストパラメータ(DeleteRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request request) {

			// 入力値チェック
			DeleteRequest param = (DeleteRequest)request.getParam(DeleteRequest.class);
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 削除
			LogicalDeleteService service = new LogicalDeleteService(new AttLeaveOfAbsenceRepository());
			service.deleteEntity(param.id);

			// レスポンス作成
			return null;
		}
	}
}