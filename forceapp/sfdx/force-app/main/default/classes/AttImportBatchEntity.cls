/**
  * @group 勤怠
  *
  * @desctiprion 勤怠インポートバッチのエンティティ
  */
public with sharing class AttImportBatchEntity extends AttImportBatchGeneratedEntity {

	/** @desctiprion コメントの最大文字数 */
	public static final Integer COMMENT_MAX_LENGTH = 1000;

	/**
	 * コンストラクタ
	 */
	public AttImportBatchEntity() {
		super(new AttImportBatch__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttImportBatchEntity(AttImportBatch__c sobj) {
		super(sobj);
	}
}