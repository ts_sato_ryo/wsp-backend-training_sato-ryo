/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 個人設定のリポジトリ
 */
public with sharing class PersonalSettingRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ComPersonalSetting__c.Id,
			ComPersonalSetting__c.Name,
			ComPersonalSetting__c.EmployeeBaseId__c,
			ComPersonalSetting__c.TimeInputSetting__c,
			ComPersonalSetting__c.DefaultJobId__c,
			ComPersonalSetting__c.ApproverBase01Id__c,
			ComPersonalSetting__c.FinanceApprovalSearchConditionList__c
		};
	}

	/** 検索フィルタ */
	 private class Filter {
		/** 社員ベースID */
		public List<Id> employeeBaseIdList;
	 }


	/**
	 * 個人設定を取得する
	 * @param employeeBaseId 社員ベースID
	 * @return 個人設定エンティティ（該当するレコードが存在しない場合はnull）
	 * @throws App.IllegalStateException 社員に関連するレコードが複数登録されている場合
	 */
	public PersonalSettingEntity getPersonalSetting(Id employeeBaseId) {
		// 検索実行
		List<PersonalSettingEntity> settingList = getPersonalSettingList(new List<Id>{employeeBaseId});
		if (settingList.isEmpty()) {
			return null;
		}

		// 社員に関連するレコードが複数存在する場合はエラーとする
		if (settingList.size() != 1) {
			throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_IDENTIFY_PERSONAL_SETTING,
					ComMessage.msg().Att_Err_MoreThan1Found(new List<String>{ComMessage.msg().Admin_Lbl_PersonalSettings}));
		}
		return settingList[0];
	}

	/**
	 * 個人設定を取得する
	 * @param employeeBaseIdList 社員ベースIDのリスト
	 * @return 条件に一致した個人設定エンティティのリスト
	 */
	public List<PersonalSettingEntity> getPersonalSettingList(List<Id> employeeBaseIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.employeeBaseIdList = employeeBaseIdList;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchPersonalSetting(filter, forUpdate);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(PersonalSettingEntity entity) {
		return saveEntityList(new List<PersonalSettingEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<PersonalSettingEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<PersonalSettingEntity> entityList, Boolean allOrNone) {
		List<ComPersonalSetting__c> personalSettingList = new List<ComPersonalSetting__c>();

		// エンティティからSObjectを作成する
		for (PersonalSettingEntity entity : entityList) {
			ComPersonalSetting__c personalSetting = createObj(entity);
			if (personalSetting.Id == null) {
				personalSetting.EmployeeBaseId__c = entity.employeeBaseId;
			}
			personalSettingList.add(personalSetting);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(personalSettingList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 個人設定を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した個人設定のリスト（該当するレコードが存在しない場合はからのリスト）
	 */
	 private List<PersonalSettingEntity> searchPersonalSetting(Filter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> employeeBaseIds = filter.employeeBaseIdList;
		if ((employeeBaseIds != null) && (! employeeBaseIds.isEmpty())) {
			condList.add(getFieldName(ComPersonalSetting__c.EmployeeBaseId__c) + ' IN :employeeBaseIds');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComPersonalSetting__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('PersonalSettingRepository.searchPersonalSetting: SOQL==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List<SObject> sobjList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComPersonalSetting__c.SObjectType);

		// 結果からエンティティを作成する
		List<PersonalSettingEntity> entityList = new List<PersonalSettingEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ComPersonalSetting__c)sobj));
		}

		return entityList;
	 }

	/**
	 * ComPersonalSetting__cオブジェクトからPersonalSettingEntityを作成する
	 * @param obj 作成元のComPersonalSetting__cオブジェクト
	 * @return 作成したPersonalSettingEntity
	 */
	@testVisible
	private PersonalSettingEntity createEntity(ComPersonalSetting__c obj) {
		PersonalSettingEntity entity = new PersonalSettingEntity();
		entity.setId(obj.Id);
		entity.name = obj.Name;
		entity.employeeBaseId = obj.EmployeeBaseId__c;
		if (!String.isBlank(obj.TimeInputSetting__c)) {
			entity.timeInputSetting = (PersonalSettingEntity.TimeInputSetting)JSON.deserialize(obj.TimeInputSetting__c, PersonalSettingEntity.TimeInputSetting.class);
		}
		entity.defaultJobId = obj.DefaultJobId__c;
		entity.approverBase01Id = obj.ApproverBase01Id__c;
		if (obj.FinanceApprovalSearchConditionList__c != null) {
			entity.searchConditionList = (List<PersonalSettingEntity.SearchCondition>)JSON.deserialize(obj.FinanceApprovalSearchConditionList__c, List<PersonalSettingEntity.SearchCondition>.class);
		}
		return entity;
	}

	/**
	 * PersonalSettingEntityからPersonalSettingEntityを作成する。
	 * entityの変更のあった項目のみ、作成後のオブジェクトに設定する。
	 * @param entity 作成元のオブジェクト
	 * @return 作成後のオブジェクト
	 */
	@testVisible
	private ComPersonalSetting__c createObj(PersonalSettingEntity entity) {
		ComPersonalSetting__c retObj = new ComPersonalSetting__c();

		retObj.Id = entity.id;
		if (entity.isChanged(PersonalSettingEntity.Field.Name)) {
			retObj.Name = entity.name;
		}
//		if (entity.isChanged(PersonalSettingEntity.Field.EmployeeBaseId)) {
//			retObj.EmployeeBaseId__c = entity.employeeBaseId;
//		}
		if (entity.isChanged(PersonalSettingEntity.Field.TimeInputSetting)) {
			if (entity.timeInputSetting == null || entity.timeInputSetting.taskList == null) {
				// 意図的に未設定にした場合、インスタンス生成時の状態で保存する
				entity.timeInputSetting = new PersonalSettingEntity.timeInputSetting();
			}
			retObj.TimeInputSetting__c = JSON.serialize(entity.timeInputSetting);
		}
		if (entity.isChanged(PersonalSettingEntity.Field.DefaultJobId)) {
			retObj.DefaultJobId__c = entity.defaultJobId;
		}
		if (entity.isChanged(PersonalSettingEntity.Field.ApproverBase01Id)) {
			retObj.ApproverBase01Id__c = entity.approverBase01Id;
		}
		if (entity.isChanged(PersonalSettingEntity.Field.SearchConditionList)) {
			String searchConditinoString = JSON.serialize(entity.searchConditionList);
			if(searchConditinoString.length()>=entity.MAX_SEARCH_CONDITION_STRING_SIZE){
				throw new App.IllegalStateException(
					App.ERR_CODE_SEARCH_CONDITION_EXCEED_MAX_SIZE,
					ComMessage.msg().Exp_Err_ExceedSearchConditionLimitation);
			}
			retObj.FinanceApprovalSearchConditionList__c = entity.searchConditionList == null ? null : searchConditinoString;
		}

		return retObj;
	}
}