/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description Custom Hint Resource
 */
public with sharing class CustomHintResource {
	static final ComMsgBase MESSAGE = ComMessage.msg();

	public virtual class CustomHintBasicSetting {
		/** Company ID */
		public string companyId;
		/** Module Type */
		public string moduleType;

		public CustomHintBasicSetting() {
		}

		public CustomHintBasicSetting(String companyId, String moduleType) {
			this.companyId = companyId;
			this.moduleType = moduleType;
		}

		public void validate() {
			ExpCommonUtil.validateId('companyId', this.companyId, true);
			
			// validate if module type is valid
			if (CustomHintEntity.MODULE_TYPE_MAP.get(this.moduleType) == null) {
				APP.ParameterException e = new App.ParameterException();
				e.setMessage(MESSAGE.Com_Err_InvalidValue(new List<String>{'moduleType'}));
				throw e;
			}
		}
	}

	/** Description in default language only */
	public virtual class CustomHintDefaultSetting extends CustomHintBasicSetting {
		/** Report Header - Accounting Period */
		public string reportHeaderAccountingPeriod;
		/** Report Header - Report Type */
		public string reportHeaderReportType;
		/** Report Header - Record Date */
		public string reportHeaderRecordDate;
		/** Report Header - Job */
		public string reportHeaderJob;
		/** Report Header - Cost Center */
		public string reportHeaderCostCenter;
		/** Report Header - Vendor */
		public string reportHeaderVendor;
		/** Report Header - Remarks */
		public string reportHeaderRemarks;
		/** Report Header - Scheduled Date */
		public string reportHeaderScheduledDate;
		/** Report Header - Purpose */
		public string reportHeaderPurpose;
		/** Record - Date */
		public string recordDate;
		/** Request Record - Date */
		public string requestRecordDate;
		/** Record - Expense Type */
		public string recordExpenseType;
		/** Record - Summary */
		public string recordSummary;
		/** Record - Receipt */
		public string recordReceipt;
		/** Record - Quotation */
		public string recordQuotation;
		/** Record - Job */
		public string recordJob;
		/** Record - Cost Center */
		public string recordCostCenter;

		public CustomHintDefaultSetting() {
			super();
		}

		public CustomHintDefaultSetting(String companyId, String moduleType) {
			super(companyId, moduleType);
		}

		public CustomHintDefaultSetting(List<CustomHintEntity> entityList, String companyId, String moduleType) {
			super(companyId, moduleType);

			for(CustomHintEntity entity : entityList) {
				/** Report Header - Accounting Period */
				if (entity.fieldName.equals('reportHeaderAccountingPeriod')) {
					this.reportHeaderAccountingPeriod = entity.description.getValue();
				}
				/** Report Header - Report Type */
				else if (entity.fieldName.equals('reportHeaderReportType')) {
					this.reportHeaderReportType = entity.description.getValue();
				}
				/** Report Header - Record Date */
				else if (entity.fieldName.equals('reportHeaderRecordDate')) {
					this.reportHeaderRecordDate = entity.description.getValue();
				}
				/** Report Header - Job */
				else if (entity.fieldName.equals('reportHeaderJob')) {
					this.reportHeaderJob = entity.description.getValue();
				}
				/** Report Header - Cost Center */
				else if (entity.fieldName.equals('reportHeaderCostCenter')) {
					this.reportHeaderCostCenter = entity.description.getValue();
				}
				/** Report Header - Vendor */
				else if (entity.fieldName.equals('reportHeaderVendor')) {
					this.reportHeaderVendor = entity.description.getValue();
				}
				/** Report Header - Remarks */
				else if (entity.fieldName.equals('reportHeaderRemarks')) {
					this.reportHeaderRemarks = entity.description.getValue();
				}
				/** Report Header - Scheduled Date */
				else if (entity.fieldName.equals('reportHeaderScheduledDate')) {
					this.reportHeaderScheduledDate = entity.description.getValue();
				}
				/** Report Header - Purpose */
				else if (entity.fieldName.equals('reportHeaderPurpose')) {
					this.reportHeaderPurpose = entity.description.getValue();
				}
				/** Record - Date */
				else if (entity.fieldName.equals('recordDate')) {
					this.recordDate = entity.description.getValue();
				}
				/** Request Record - Date */
				else if (entity.fieldName.equals('requestRecordDate')) {
					this.requestRecordDate = entity.description.getValue();
				}
				/** Record - Expense Type */
				else if (entity.fieldName.equals('recordExpenseType')) {
					this.recordExpenseType = entity.description.getValue();
				}
				/** Record - Summary */
				else if (entity.fieldName.equals('recordSummary')) {
					this.recordSummary = entity.description.getValue();
				}
				/** Record - Receipt */
				else if (entity.fieldName.equals('recordReceipt')) {
					this.recordReceipt = entity.description.getValue();
				}
				/** Record - Quotation */
				else if (entity.fieldName.equals('recordQuotation')) {
					this.recordQuotation = entity.description.getValue();
				}
				/** Record - Job */
				else if (entity.fieldName.equals('recordJob')) {
					this.recordJob = entity.description.getValue();
				}
				/** Record - Cost Center */
				else if (entity.fieldName.equals('recordCostCenter')) {
					this.recordCostCenter = entity.description.getValue();
				}
			}
		}
	}

	/** All descriptions */
	public virtual class CustomHintSetting extends CustomHintDefaultSetting {
		/** Report Header - Accounting Period */
		public string reportHeaderAccountingPeriod_L0;
		public string reportHeaderAccountingPeriod_L1;
		public string reportHeaderAccountingPeriod_L2;
		/** Report Header - Report Type */
		public string reportHeaderReportType_L0;
		public string reportHeaderReportType_L1;
		public string reportHeaderReportType_L2;
		/** Report Header - Record Date */
		public string reportHeaderRecordDate_L0;
		public string reportHeaderRecordDate_L1;
		public string reportHeaderRecordDate_L2;
		/** Report Header - Job */
		public string reportHeaderJob_L0;
		public string reportHeaderJob_L1;
		public string reportHeaderJob_L2;
		/** Report Header - Cost Center */
		public string reportHeaderCostCenter_L0;
		public string reportHeaderCostCenter_L1;
		public string reportHeaderCostCenter_L2;
		/** Report Header - Vendor */
		public string reportHeaderVendor_L0;
		public string reportHeaderVendor_L1;
		public string reportHeaderVendor_L2;
		/** Report Header - Remarks */
		public string reportHeaderRemarks_L0;
		public string reportHeaderRemarks_L1;
		public string reportHeaderRemarks_L2;
		/** Report Header - Scheduled Date */
		public string reportHeaderScheduledDate_L0;
		public string reportHeaderScheduledDate_L1;
		public string reportHeaderScheduledDate_L2;
		/** Report Header - Purpose */
		public string reportHeaderPurpose_L0;
		public string reportHeaderPurpose_L1;
		public string reportHeaderPurpose_L2;
		/** Record - Date */
		public string recordDate_L0;
		public string recordDate_L1;
		public string recordDate_L2;
		/** Request Record - Date */
		public string requestRecordDate_L0;
		public string requestRecordDate_L1;
		public string requestRecordDate_L2;
		/** Record - Expense Type */
		public string recordExpenseType_L0;
		public string recordExpenseType_L1;
		public string recordExpenseType_L2;
		/** Record - Summary */
		public string recordSummary_L0;
		public string recordSummary_L1;
		public string recordSummary_L2;
		/** Record - Receipt */
		public string recordReceipt_L0;
		public string recordReceipt_L1;
		public string recordReceipt_L2;
		/** Record - Quotation */
		public string recordQuotation_L0;
		public string recordQuotation_L1;
		public string recordQuotation_L2;
		/** Record - Job */
		public string recordJob_L0;
		public string recordJob_L1;
		public string recordJob_L2;
		/** Record - Cost Center */
		public string recordCostCenter_L0;
		public string recordCostCenter_L1;
		public string recordCostCenter_L2;

		public CustomHintSetting() {
			super();
		}

		public CustomHintSetting(List<CustomHintEntity> entityList, String companyId, String moduleType) {
			super(companyId, moduleType);

			for(CustomHintEntity entity : entityList) {
				/** Report Header - Accounting Period */
				if (entity.fieldName.equals('reportHeaderAccountingPeriod')) {
					this.reportHeaderAccountingPeriod = entity.description.getValue();
					this.reportHeaderAccountingPeriod_L0 = entity.description_L0;
					this.reportHeaderAccountingPeriod_L1 = entity.description_L1;
					this.reportHeaderAccountingPeriod_L2 = entity.description_L2;
				}
				/** Report Header - Report Type */
				else if (entity.fieldName.equals('reportHeaderReportType')) {
					this.reportHeaderReportType = entity.description.getValue();
					this.reportHeaderReportType_L0 = entity.description_L0;
					this.reportHeaderReportType_L1 = entity.description_L1;
					this.reportHeaderReportType_L2 = entity.description_L2;
				}
				/** Report Header - Record Date */
				else if (entity.fieldName.equals('reportHeaderRecordDate')) {
					this.reportHeaderRecordDate = entity.description.getValue();
					this.reportHeaderRecordDate_L0 = entity.description_L0;
					this.reportHeaderRecordDate_L1 = entity.description_L1;
					this.reportHeaderRecordDate_L2 = entity.description_L2;
				}
				/** Report Header - Job */
				else if (entity.fieldName.equals('reportHeaderJob')) {
					this.reportHeaderJob = entity.description.getValue();
					this.reportHeaderJob_L0 = entity.description_L0;
					this.reportHeaderJob_L1 = entity.description_L1;
					this.reportHeaderJob_L2 = entity.description_L2;
				}
				/** Report Header - Cost Center */
				else if (entity.fieldName.equals('reportHeaderCostCenter')) {
					this.reportHeaderCostCenter = entity.description.getValue();
					this.reportHeaderCostCenter_L0 = entity.description_L0;
					this.reportHeaderCostCenter_L1 = entity.description_L1;
					this.reportHeaderCostCenter_L2 = entity.description_L2;
				}
				/** Report Header - Vendor */
				else if (entity.fieldName.equals('reportHeaderVendor')) {
					this.reportHeaderVendor = entity.description.getValue();
					this.reportHeaderVendor_L0 = entity.description_L0;
					this.reportHeaderVendor_L1 = entity.description_L1;
					this.reportHeaderVendor_L2 = entity.description_L2;
				}
				/** Report Header - Remarks */
				else if (entity.fieldName.equals('reportHeaderRemarks')) {
					this.reportHeaderRemarks = entity.description.getValue();
					this.reportHeaderRemarks_L0 = entity.description_L0;
					this.reportHeaderRemarks_L1 = entity.description_L1;
					this.reportHeaderRemarks_L2 = entity.description_L2;
				}
				/** Report Header - Scheduled Date */
				else if (entity.fieldName.equals('reportHeaderScheduledDate')) {
					this.reportHeaderScheduledDate = entity.description.getValue();
					this.reportHeaderScheduledDate_L0 = entity.description_L0;
					this.reportHeaderScheduledDate_L1 = entity.description_L1;
					this.reportHeaderScheduledDate_L2 = entity.description_L2;
				}
				/** Report Header - Purpose */
				else if (entity.fieldName.equals('reportHeaderPurpose')) {
					this.reportHeaderPurpose = entity.description.getValue();
					this.reportHeaderPurpose_L0 = entity.description_L0;
					this.reportHeaderPurpose_L1 = entity.description_L1;
					this.reportHeaderPurpose_L2 = entity.description_L2;
				}
				/** Record - Date */
				else if (entity.fieldName.equals('recordDate')) {
					this.recordDate = entity.description.getValue();
					this.recordDate_L0 = entity.description_L0;
					this.recordDate_L1 = entity.description_L1;
					this.recordDate_L2 = entity.description_L2;
				}
				/** Request Record - Date */
				else if (entity.fieldName.equals('requestRecordDate')) {
					this.requestRecordDate = entity.description.getValue();
					this.requestRecordDate_L0 = entity.description_L0;
					this.requestRecordDate_L1 = entity.description_L1;
					this.requestRecordDate_L2 = entity.description_L2;
				}
				/** Record - Expense Type */
				else if (entity.fieldName.equals('recordExpenseType')) {
					this.recordExpenseType = entity.description.getValue();
					this.recordExpenseType_L0 = entity.description_L0;
					this.recordExpenseType_L1 = entity.description_L1;
					this.recordExpenseType_L2 = entity.description_L2;
				}
				/** Record - Summary */
				else if (entity.fieldName.equals('recordSummary')) {
					this.recordSummary = entity.description.getValue();
					this.recordSummary_L0 = entity.description_L0;
					this.recordSummary_L1 = entity.description_L1;
					this.recordSummary_L2 = entity.description_L2;
				}
				/** Record - Receipt */
				else if (entity.fieldName.equals('recordReceipt')) {
					this.recordReceipt = entity.description.getValue();
					this.recordReceipt_L0 = entity.description_L0;
					this.recordReceipt_L1 = entity.description_L1;
					this.recordReceipt_L2 = entity.description_L2;
				}
				/** Record - Quotation */
				else if (entity.fieldName.equals('recordQuotation')) {
					this.recordQuotation = entity.description.getValue();
					this.recordQuotation_L0 = entity.description_L0;
					this.recordQuotation_L1 = entity.description_L1;
					this.recordQuotation_L2 = entity.description_L2;
				}
				/** Record - Job */
				else if (entity.fieldName.equals('recordJob')) {
					this.recordJob = entity.description.getValue();
					this.recordJob_L0 = entity.description_L0;
					this.recordJob_L1 = entity.description_L1;
					this.recordJob_L2 = entity.description_L2;
				}
				/** Record - Cost Center */
				else if (entity.fieldName.equals('recordCostCenter')) {
					this.recordCostCenter = entity.description.getValue();
					this.recordCostCenter_L0 = entity.description_L0;
					this.recordCostCenter_L1 = entity.description_L1;
					this.recordCostCenter_L2 = entity.description_L2;
				}
			}
		}

		/** Create entity list from request parameter */
		public List<CustomHintEntity> createEntityList(Map<String, Object> paramMap) {
			List<CustomHintEntity> entityList = new List<CustomHintEntity>();
			CustomHintEntity entity;

			/** Report Header - Accounting Period */
			if (paramMap.containsKey('reportHeaderAccountingPeriod_L0')) {
				entity = createEntity('reportHeaderAccountingPeriod', this.reportHeaderAccountingPeriod_L0, this.reportHeaderAccountingPeriod_L1, this.reportHeaderAccountingPeriod_L2);
				entityList.add(entity);
			}
			/** Report Header - Report Type */
			if (paramMap.containsKey('reportHeaderReportType_L0')) {
				entity = createEntity('reportHeaderReportType', this.reportHeaderReportType_L0, this.reportHeaderReportType_L1, this.reportHeaderReportType_L2);
				entityList.add(entity);
			}
			/** Report Header - Record Date */
			if (paramMap.containsKey('reportHeaderRecordDate_L0')) {
				entity = createEntity('reportHeaderRecordDate', this.reportHeaderRecordDate_L0, this.reportHeaderRecordDate_L1, this.reportHeaderRecordDate_L2);
				entityList.add(entity);
			}
			/** Report Header - Job */
			if (paramMap.containsKey('reportHeaderJob_L0')) {
				entity = createEntity('reportHeaderJob', this.reportHeaderJob_L0, this.reportHeaderJob_L1, this.reportHeaderJob_L2);
				entityList.add(entity);
			}
			/** Report Header - Cost Center */
			if (paramMap.containsKey('reportHeaderCostCenter_L0')) {
				entity = createEntity('reportHeaderCostCenter', this.reportHeaderCostCenter_L0, this.reportHeaderCostCenter_L1, this.reportHeaderCostCenter_L2);
				entityList.add(entity);
			}
			/** Report Header - Vendor */
			if (paramMap.containsKey('reportHeaderVendor_L0')) {
				entity = createEntity('reportHeaderVendor', this.reportHeaderVendor_L0, this.reportHeaderVendor_L1, this.reportHeaderVendor_L2);
				entityList.add(entity);
			} 
			/** Report Header - Remarks */
			if (paramMap.containsKey('reportHeaderRemarks_L0')) {
				entity = createEntity('reportHeaderRemarks', this.reportHeaderRemarks_L0, this.reportHeaderRemarks_L1, this.reportHeaderRemarks_L2);
				entityList.add(entity);
			}
			/** Report Header - Scheduled Date */
			if (paramMap.containsKey('reportHeaderScheduledDate_L0')) {
				entity = createEntity('reportHeaderScheduledDate', this.reportHeaderScheduledDate_L0, this.reportHeaderScheduledDate_L1, this.reportHeaderScheduledDate_L2);
				entityList.add(entity);
			}
			/** Report Header - Purpose */
			if (paramMap.containsKey('reportHeaderPurpose_L0')) {
				entity = createEntity('reportHeaderPurpose', this.reportHeaderPurpose_L0, this.reportHeaderPurpose_L1, this.reportHeaderPurpose_L2);
				entityList.add(entity);
			}
			/** Record - Date */
			if (paramMap.containsKey('recordDate_L0')) {
				entity = createEntity('recordDate', this.recordDate_L0, this.recordDate_L1, this.recordDate_L2);
				entityList.add(entity);
			}
			/** Request Record - Date */
			if (paramMap.containsKey('requestRecordDate_L0')) {
				entity = createEntity('requestRecordDate', this.requestRecordDate_L0, this.requestRecordDate_L1, this.requestRecordDate_L2);
				entityList.add(entity);
			}
			/** Record - Expense Type */
			if (paramMap.containsKey('recordExpenseType_L0')) {
				entity = createEntity('recordExpenseType', this.recordExpenseType_L0, this.recordExpenseType_L1, this.recordExpenseType_L2);
				entityList.add(entity);
			}
			/** Record - Summary */
			if (paramMap.containsKey('recordSummary_L0')) {
				entity = createEntity('recordSummary', this.recordSummary_L0, this.recordSummary_L1, this.recordSummary_L2);
				entityList.add(entity);
			}
			/** Record - Receipt */
			if (paramMap.containsKey('recordReceipt_L0')) {
				entity = createEntity('recordReceipt', this.recordReceipt_L0, this.recordReceipt_L1, this.recordReceipt_L2);
				entityList.add(entity);
			}
			/** Record - Quotation */
			if (paramMap.containsKey('recordQuotation_L0')) {
				entity = createEntity('recordQuotation', this.recordQuotation_L0, this.recordQuotation_L1, this.recordQuotation_L2);
				entityList.add(entity);
			}
			/** Record - Job */
			if (paramMap.containsKey('recordJob_L0')) {
				entity = createEntity('recordJob', this.recordJob_L0, this.recordJob_L1, this.recordJob_L2);
				entityList.add(entity);
			}
			/** Record - Cost Center */
			if (paramMap.containsKey('recordCostCenter_L0')) {
				entity = createEntity('recordCostCenter', this.recordCostCenter_L0, this.recordCostCenter_L1, this.recordCostCenter_L2);
				entityList.add(entity);
			}
			
			return entityList;
		}

		private CustomHintEntity createEntity(String keyName, String description_L0, String description_L1, String description_L2) {
			CustomHintEntity entity = new CustomHintEntity();
			entity.companyId = this.companyId;
			entity.moduleType = CustomHintEntity.MODULE_TYPE_MAP.get(this.moduleType);
			entity.fieldName = keyName;
			entity.description_L0 = description_L0;
			entity.description_L1 = description_L1;
			entity.description_L2 = description_L2;
			return entity;
		}
	}

	/**
	 * @description Parameter for new creation and update
	 */
	public class SaveParam extends CustomHintSetting implements RemoteApi.RequestParam {
	}

	/**
	 * @description API to save custom hints
	 */
	public with sharing class SaveApi extends RemoteApi.ResourceBase {
		private final ComResourcePermission.Permission requriedPermission =
					ComResourcePermission.Permission.MANAGE_EXP_CUSTOM_HINT;

		/**
		 * @description Save custom hints
		 * @param req Request parameter (SaveParam)
		 * @return SaveResult
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			SaveParam param = (SaveParam)req.getParam(SaveParam.class);
			param.validate();

			List<CustomHintEntity> customHintList = param.createEntityList(req.getParamMap());
			new CustomHintService().saveCustomHintList(customHintList);

			return null;
		}
	}

	/**
	 * @description Parameter for retrieving custom hints
	 */
	public class SearchParam extends CustomHintBasicSetting implements RemoteApi.RequestParam {
	}

	/**
	 * @description Response parameter for retrieving custom hints
	 */
	public class SearchCustomHintResponse implements RemoteApi.ResponseParam {
		public List<CustomHintSetting> records;

		public SearchCustomHintResponse(List<CustomHintEntity> entityList, String companyId, String moduleType) {
			this.records = new List<CustomHintSetting>{new CustomHintSetting(entityList, companyId, moduleType)};
		}
	}

	/**
	 * @description API to search custom hints
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search custom hints
		 * @param req Request parameter (SearchParam)
		 * @return SearchCustomHintResponse
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SearchParam param = (SearchParam)req.getParam(SearchParam.class);
			param.validate();

			List<CustomHintEntity> customHintList = new CustomHintService().getCustomHintList(param.companyId, param.moduleType);
			SearchCustomHintResponse result = new SearchCustomHintResponse(customHintList, param.companyId, param.moduleType);
			return result;
		}
	}

	/**
	 * @description Parameter for retrieving custom hints (description in default language only)
	 */
	public class GetParam extends CustomHintBasicSetting implements RemoteApi.RequestParam {
	}

	/**
	 * @description Response parameter for retrieving custom hints
	 */
	public class GetCustomHintResponse extends CustomHintDefaultSetting implements RemoteApi.ResponseParam {
		public GetCustomHintResponse(List<CustomHintEntity> entityList, String companyId, String moduleType) {
			super(entityList, companyId, moduleType);
		}
	}
	
	/**
	 * @description API to get custom hints
	 */
	public with sharing class GetApi extends RemoteApi.ResourceBase {/**
		 * @description Get custom hints
		 * @param req Request parameter (GetParam)
		 * @return GetCustomHintResponse
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			GetParam param = (GetParam)req.getParam(GetParam.class);
			param.validate();

			List<CustomHintEntity> customHintList = new CustomHintService().getCustomHintList(param.companyId, param.moduleType);
			GetCustomHintResponse result = new GetCustomHintResponse(customHintList, param.companyId, param.moduleType);
			return result;
		}

	}
}