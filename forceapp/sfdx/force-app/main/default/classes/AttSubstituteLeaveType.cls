public with sharing class AttSubstituteLeaveType extends ValueObjectType {
	/** 振替 */
	public static final AttSubstituteLeaveType SUBSTITUTE = new AttSubstituteLeaveType('Substitute');

	/** 休日出勤取得代休タイプのリスト（タイプが追加されら本リストにも追加してください) */
	public static final List<AttSubstituteLeaveType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttSubstituteLeaveType> {
			SUBSTITUTE
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttSubstituteLeaveType(String value) {
		super(value);
	}

	/**
	 * AttSubstituteLeaveType
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttSubstituteLeaveType
	 */
	public static AttSubstituteLeaveType valueOf(String value) {
		AttSubstituteLeaveType retType = null;
		if (String.isNotBlank(value)) {
			for (AttSubstituteLeaveType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 「;」区切りする値からAttSubstituteLeaveType一覧を取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttSubstituteLeaveType
	 */
	public static List<AttSubstituteLeaveType> valuesOf(String value) {
		List<AttSubstituteLeaveType> retList = new List<AttSubstituteLeaveType>();
		if (String.isBlank(value)) {
			return retList;
		}
		for (String typeString : value.split(';')) {
			AttSubstituteLeaveType retType = null;
			if (String.isNotBlank(typeString)) {
				for (AttSubstituteLeaveType type : TYPE_LIST) {
					if (typeString == type.value) {
						retList.add(type);
						break;
					}
				}
			}
		}
		return retList;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttSubstituteLeaveType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttSubstituteLeaveType) {
			// 値が同じであればtrue
			if (this.value == ((AttSubstituteLeaveType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}