/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 共通
  *
 * カレンダー明細のエンティティ
 */
public with sharing class CalendarRecordEntity extends LogicalDeleteEntity {

	/** カレンダー明細名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 備考の最大文字数 */
	public static final Integer REMARKS_MAX_LENGTH = 255;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		CALENDAR_ID,
		Name,
		Name_L,
		CAL_DATE,
		DAY_TYPE,
		REMARKS
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ
	 */
	public CalendarRecordEntity() {
		isChangedFieldSet = new Set<Field>();
	}
	/** カレンダーID */
	public String calendarId {
		get;
		set {
			calendarId = value;
			setChanged(Field.CALENDAR_ID);
		}
	}
	/** カレンダー明細名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.Name);
		}
	}
	/** カレンダー明細名(L0-2) */
	public AppMultiString nameL {
		get;
		set {
			nameL = value;
			setChanged(Field.Name_L);
		}
	}
	public AppDate calDate {
		get;
		set {
			calDate = value;
			setChanged(Field.CAL_DATE);
		}
	}
	public AttDayType dayType {
		get;
		set {
			dayType = value;
			setChanged(Field.DAY_TYPE);
		}
	}
	public String remarks {
		get;
		set {
			remarks = value;
			setChanged(Field.REMARKS);
		}
	}
	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	protected override void resetChangedInSubClass() {
		isChangedFieldSet.clear();
	}

	/**
	 * エンティティの複製を作成する
	 */
	public CalendarRecordEntity copy() {
		// ディープcloneではないので注意
		// エンティティの仕様が変更される予定なので暫定対応でcloneを呼び出しておく
		// (仕様が変更されたらcloneが簡単に実装できるようになるはず)
		return this.clone();
	}
}