/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 個人設定情報APIを実装するクラス
 */
 public with sharing class PersonalSettingResource {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

    /*
	 * @desctiprion 個人設定情報取得のリクエストパラメータを表すクラス
	 */
	public class GetRequest implements RemoteApi.RequestParam {
		/** 社員ID */
		public String empId;

		/**
		 * パラメータのチェックを行う
		 */
		public void validateParam() {
			if (!String.isBlank(empId)) {
				try {
					System.Id.valueOf(this.empId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'()'}));
				}
			}
		}
	}

    /*
	 * @desctiprion 個人設定情報を表すクラス
	 */
	public class PersonalSetting implements RemoteApi.ResponseParam {
		/** プランナーデフォルト表示 */
		public String plannerDefaultView;

		/** デフォルトジョブ */
		public DefaultJob defaultJob = new DefaultJob();

		/** 検索条件 / search condition */
		public List<PersonalSettingEntity.SearchCondition> searchConditionList = new List<PersonalSettingEntity.SearchCondition>();
	}

	/*
	 * @desctiprion デフォルトジョブを表すクラス
	 */
	public class DefaultJob {
		/** ジョブID */
		public String id;
		/** ジョブコード */
		public String code;
		/** ジョブ名 */
		public String name;
		/** 有効期間の開始日(yyyy-MM-dd) */
		public Date validDateFrom;
		/** 有効期間の終了日(yyyy-MM-dd) */
		public Date validDateTo;
	}

	/**
	 * @description 個人設定情報取得APIを実装するクラス
	 */
	public with sharing class GetApi extends RemoteApi.ResourceBase {

		// こんな感じのメンバ変数を追加
		private Map<Id, CompanyEntity> companyMap = new Map<Id, CompanyEntity>();

		/**
		 * @description 社員レコードを1件作成する
		 * @param  req リクエストパラメータ(パラメータなし)
		 * @return レスポンス(PersonalSetting)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			GetRequest param = (GetRequest)req.getParam(GetRequest.class);
			param.validateParam();

			// 社員情報を取得
			EmployeeService employeeService = new EmployeeService();
			EmployeeBaseEntity employee;
			if (String.isBlank(param.empId)) {
				// 社員IDが設定されていない場合、ログインユーザから社員情報を取得する
				employee = employeeService.getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
			} else {
				employee = employeeService.getEmployee(param.empId, AppDate.today());
			}

			// レスポンス生成
			PersonalSettingResource.PersonalSetting response = new PersonalSettingResource.PersonalSetting();
			response.plannerDefaultView = getPlannerDefaultView(employee).value;

			// デフォルトジョブを取得
			JobService jobService = new JobService();
			JobEntity job = jobService.getDefaultJob(employee);
			if (job != null) {
				response.defaultJob.id = job.id;
				response.defaultJob.code = job.code;
				response.defaultJob.name = job.nameL.getValue();
				response.defaultJob.validDateFrom = AppDate.convertDate(job.validFrom);
				response.defaultJob.validDateTo = AppDate.convertDate(job.validTo);
			}

			// get search condition of expense finance approval
			// レコード検索
			PersonalSettingRepository repository = new PersonalSettingRepository();
			PersonalSettingEntity entity = repository.getPersonalSetting(employee.id);
			if(entity != null & useFinancialApproval(employee)){
				response.searchConditionList = entity.searchConditionList;
			}
			return response;
		}

		private CompanyEntity getCompanyEntity(Id companyId) {
			if (companyMap.containsKey(companyId)) {
					return companyMap.get(companyId);
			}

			CompanyEntity entity = new CompanyRepository().getEntity(companyId);
			companyMap.put(companyId, entity);
			return entity;
		}

		private Boolean useFinancialApproval(employeeBaseEntity employee) {
			CompanyEntity company = getCompanyEntity(employee.companyId);
			return company.useExpense;
		}

		/*
		 * 社員のプランナーデフォルト表示を取得する。
		 * 会社マスタの設定がnullの場合、"Weekly"を返す。
		 * 将来的に 会社マスタ > 個人設定 > デフォルト値を返す仕様になる。
		 */
		@testVisible
		private PlannerViewType getPlannerDefaultView(EmployeeBaseEntity employee) {
			// 会社マスタを取得
			CompanyRepository companyRepository = new CompanyRepository();
			CompanyEntity company = getCompanyEntity(employee.companyId);
			return company.plannerDefaultView == null ? PlannerViewType.WEEKLY : company.plannerDefaultView;
		}
	}

	/*
	 * @desctiprion 個人設定更新のリクエストを表すクラス
	 */
	public class UpdateRequest implements RemoteApi.RequestParam {

		/** デフォルトジョブID */
		public String defaultJobId;
		/** 承認者01 */
		public String approverBase01Id;

		public List<PersonalSettingEntity.SearchCondition> searchConditionList;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// デフォルトジョブID
			if (!String.isBlank(defaultJobId)) {
				try {
					System.Id.valueOf(defaultJobId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'defaultJobId'}));
				}
			}
			// デフォルトジョブID
			if (!String.isBlank(approverBase01Id)) {
				try {
					System.Id.valueOf(approverBase01Id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'approverBase01Id'}));
				}
			}
		}
	}

	/*
	 * @desctiprion 個人設定更新のレスポンスを表すクラス
	 */
	public class UpdateResponse implements RemoteApi.ResponseParam {
	}

	/**
	 * @description 個人設定情報更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/**
		 * @description 社員レコードを1件作成する
		 * @param  req リクエストパラメータ(パラメータなし)
		 * @return レスポンス(UpdateResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// 入力値チェック
			UpdateRequest param = (UpdateRequest)req.getParam(UpdateRequest.class);
			param.validate();

			// 社員情報を取得
			EmployeeService employeeService = new EmployeeService();
			EmployeeBaseEntity employee = employeeService.getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());

			// レコード検索
			PersonalSettingRepository repository = new PersonalSettingRepository();
			PersonalSettingEntity entity = repository.getPersonalSetting(employee.id);
			if (entity == null) {
				entity = new PersonalSettingEntity();
				entity.employeeBaseId = employee.id;
			}
			// パラメータ設定
			Map<String, Object> paramMap = req.getParamMap();
			if (paramMap.containsKey('defaultJobId')) {
				entity.defaultJobId = param.defaultJobId;
			}
			if (paramMap.containsKey('approverBase01Id')) {
				entity.approverBase01Id = param.approverBase01Id;

				// 承認者01のユーザーの有効状態をチェック
				if (entity.approverBase01Id != null) {
					EmployeeBaseEntity approver01Base =
						new EmployeeRepository().getEntity(entity.approverBase01Id);
					if (approver01Base != null && approver01Base.User.isActive != true) {
						throw new App.IllegalStateException(MESSAGE.Att_Err_NotActiveApprover);
					}
				}
			}
			if (paramMap.containsKey('searchConditionList')) {
				entity.searchConditionList = param.searchConditionList;
			}

			repository.saveEntity(entity);
			return new PersonalSettingResource.UpdateResponse();
		}
	}
}