/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Service class for Vendor
 */
public class ExpVendorService {

	private static final Boolean CHECK_BY_ID = true;
	private static final Boolean CHECK_BY_CODE = false;

	/**
   * Create a new Vendor record
   * @param entity Entity to be saved
   * @return Repository.SaveResult
   */
	public static Repository.SaveResult createVendor(ExpVendorEntity entity) {

		validateEntity(entity);

		setUniqKey(entity);

		// If target record exists, throw error.
		if (isExistRecord(entity, CHECK_BY_CODE)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		return new ExpVendorRepository().saveEntity(entity);
	}

	/**
   * Validate entity
   * @param entity Target entity to be validated
   */
	private static void validateEntity(ExpVendorEntity entity) {
		Validator.Result result = new ExpConfigValidator.ExpVendorValidator(entity).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * Update a new Accounting Period record
	 * @param entity Entity to be updated
	 * @return
	 */
	public static Repository.SaveResult updateVendor(ExpVendorEntity entity) {

		// Validate entity value
		validateEntity(entity);

		// Set unique key to entity
		setUniqKey(entity);

		// If target record dose not exist, throw error.
		if (!isExistRecord(entity, CHECK_BY_ID)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_RecordNotFound);
		}

		// If target the code record exists, throw error.
		if (isExistRecord(entity, CHECK_BY_CODE)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		return new ExpVendorRepository().saveEntity(entity);
	}

	/**
 * Delete a new Accounting Period record
 * @param entity Entity to be deleted
 * @return
 */
	public static void deleteVendor(Id id) {

		ExpVendorEntity entity = new ExpVendorEntity();
		entity.setId(id);

		// If target record has been deleted, do nothing
		if (!isExistRecord(entity, CHECK_BY_ID)) return;

		try {
		  new ExpVendorRepository().deleteEntity(id);
		} catch (DmlException e) {
			if (e.getDmlType(0) == System.StatusCode.DELETE_FAILED) {
				throw new App.IllegalStateException(ComMessage.msg().Com_Err_CannotDeleteReference);
			} else {
				throw e;
			}
		}
	}

	/**
 * Search a Accounting Period record by record ID
 * @param id Target record ID
 * @return List of fetched Currency records
 */
	public static List<ExpVendorEntity> searchVendor(Map<String, Object> paramMap) {
		return  searchVendor(paramMap, ExpVendorRepository.SEARCH_RECORDS_NUMBER_MAX);
	}

	/**
 * Search a Accounting Period record by record ID
 * @param id Target record ID
 * @return List of fetched Currency records
 */
	public static List<ExpVendorEntity> searchVendor(Map<String, Object> paramMap, Integer resultLimit) {
		ExpVendorRepository.SearchFilter filter = new ExpVendorRepository.SearchFilter();

		if (paramMap != null) {
			Id vendorId = (Id)paramMap.get('id');
			filter.ids = String.isNotBlank(vendorId) ? new Set<Id>{vendorId} : null;

			Id companyId = (Id)paramMap.get('companyId');
			filter.companyIds = String.isNotBlank(companyId) ? new Set<Id>{companyId} : null;

			if (paramMap.containsKey('active') && (paramMap.get('active') != null)) {
				filter.active = (Boolean) paramMap.get('active') ? filter.ONLY_ACTIVE : filter.ONLY_INACTIVE;
			} else {
				filter.active = filter.ALL;
			}
			filter.nameOrCodeQuery = (String) paramMap.get('query');
		}
		return new ExpVendorRepository().searchEntity(filter, resultLimit);
	}

	/**
	 * Get recently used vendors
	 * @param employeeBaseId Target employee id
	 */
	public static List<ExpVendorEntity> getRecentlyUsedVendorList(Id employeeBaseId) {
		ExpRecentlyUsedRepository repo = new ExpRecentlyUsedRepository();
		ExpRecentlyUsedRepository.SearchFilter filter = new ExpRecentlyUsedRepository.SearchFilter();
		filter.employeeBaseId = employeeBaseId;
		filter.targetIds = null; // don't use this param for vendor
		filter.masterType = ExpRecentlyUsedEntity.MasterType.Vendor;
		List<ExpRecentlyUsedEntity> recentlyUsedEntityList = repo.searchEntity(filter);
		ExpRecentlyUsedEntity recentlyUsedEntity = recentlyUsedEntityList.isEmpty() ? null: recentlyUsedEntityList.get(0);

		List<ExpVendorEntity> result = new List<ExpVendorEntity>();
		if (recentlyUsedEntity != null) {
			List<Id> recentlyUsedIdList = (List<Id>)recentlyUsedEntity.getRecentlyUsedValues();
			ExpVendorRepository.SearchFilter vendorFilter = new ExpVendorRepository.SearchFilter();
			vendorFilter.ids = new Set<Id>(recentlyUsedIdList);
			vendorFilter.active = vendorFilter.ONLY_ACTIVE; // Inactive vendors should not be displayed.
			Map<Id, ExpVendorEntity> vendorMap = convertVendorListToMap(new ExpVendorRepository().searchEntity(vendorFilter));
			for (Id vendorId : recentlyUsedIdList) {
				ExpVendorEntity vendorEntity = vendorMap.get(vendorId);
				if (vendorEntity != null) {
					result.add(vendorEntity);
				}
			}
		}
		return result;
	}

	/*
	 * Create vendor entity map from list
	 * @param vendorList List of Vendor entity
	 * @return Map of Vendor entity. (Key : Id of Vendor)
	 */
	private static Map<Id, ExpVendorEntity> convertVendorListToMap(List<ExpVendorEntity> vendorList) {
		Map<Id, ExpVendorEntity> resultMap = new Map<Id, ExpVendorEntity>();
		for (ExpVendorEntity vendor : vendorList) {
			resultMap.put(vendor.Id, vendor);
		}
		return resultMap;
	}

	/**
	 * Set unique key to the entity
	 * @param entity Target entity
	 */
	private static void setUniqKey(ExpVendorEntity entity) {
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		entity.createUniqKey(company.code);
	}

	/**
	 * Check if the target record exists
	 * @param entity Target entity
	 * @param isCheckById true: Search data by ID, false: Search data by code(and company)
	 */
	private static Boolean isExistRecord(ExpVendorEntity entity, Boolean isCheckById) {
		ExpVendorRepository.SearchFilter filter = new ExpVendorRepository.SearchFilter();
		if (isCheckById) {
			ExpVendorEntity result = new ExpVendorRepository().getEntity(entity.id);
			return (result != null);
		} else {
			filter.companyIds = new Set<Id>{ entity.companyId };
			filter.codes = new Set<String>{ entity.code };
			List<ExpVendorEntity> result = new ExpVendorRepository().searchEntity(filter);
			return (result.size() > 0) && (result[0] != null) && (entity.id <> result[0].id);
		}
	}
}