/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Job UsedIn picklist
 */
public with sharing class ComJobUsedIn extends ValueObjectType {
    
    /** Expense Report Only */
	public static final ComJobUsedIn EXPENSE_REPORT = new ComJobUsedIn('ExpenseReport');
	/** Expense Report and Request */
	public static final ComJobUsedIn EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ComJobUsedIn('ExpenseRequestAndExpenseReport');

	/** Entries */
	public static final List<ComJobUsedIn> TYPE_LIST;
	static {
		TYPE_LIST = new List<ComJobUsedIn> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}

	/**
	 * Constructor
	 * @param value
	 */
	private ComJobUsedIn(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
	 * @param The key value to get the instance
	 * @return The instance having the value specified
	 */
	public static ComJobUsedIn valueOf(String value) {
		ComJobUsedIn retType = null;
		if (String.isNotBlank(value)) {
			for (ComJobUsedIn type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {

		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ComJobUsedIn) {
			if (this.value == ((ComJobUsedIn)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}
}
