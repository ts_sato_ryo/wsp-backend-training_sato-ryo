/**
 * 勤怠申請の種別の定義
 */
public with sharing class AttRequestType extends ValueObjectType {

	/** 休暇申請 */
	public static final AttRequestType LEAVE;
	/** 休日出勤申請 */
	public static final AttRequestType HOLIDAYWORK;
	/** 早朝勤務申請 */
	public static final AttRequestType EARLY_START_WORK;
	/** 残業申請 */
	public static final AttRequestType OVERTIME_WORK;
	/** 欠勤申請 */
	public static final AttRequestType ABSENCE;
	/** 直行直帰申請 */
	public static final AttRequestType DIRECT;
	/** 勤務時間変更申請 */
	public static final AttRequestType PATTERN;
	static {

		// TODO 【暫定対応】
		// パッケージにするとトランスレーションワークベンチの翻訳のラベルが取得できない現象が発生している。
		// 対応方針が決まるまでは、en_US.csvの翻訳を利用する
		LEAVE = new AttRequestType('Leave', ComMessage.msg().Att_Lbl_RequestTypeLeave);
		HOLIDAYWORK = new AttRequestType('HolidayWork', ComMessage.msg().Att_Lbl_RequestTypeHolidayWork);
		EARLY_START_WORK = new AttRequestType('EarlyStartWork', ComMessage.msg().Att_Lbl_RequestTypeEarlyStartWork);
		OVERTIME_WORK = new AttRequestType('OvertimeWork', ComMessage.msg().Att_Lbl_RequestTypeOvertimeWork);
		ABSENCE = new AttRequestType('Absence', ComMessage.msg().Att_Lbl_RequestTypeAbsence);
		DIRECT = new AttRequestType('Direct', ComMessage.msg().Att_Lbl_RequestTypeDirect);
		PATTERN = new AttRequestType('Pattern', ComMessage.msg().Att_Lbl_RequestTypePattern);
//		// 多言語化対応した選択リストのラベルが必要なため、インスタンスはPicklistの値に基づき生成する
//		List<Schema.PicklistEntry> pickList =
//				AttDailyRequest__c.RequestType__c.getDescribe().getPicklistValues();
//		for (Schema.PicklistEntry entry : pickList) {
//			// 休暇申請
//			if (entry.getValue() == 'Leave') {
//				LEAVE = new AttRequestType(entry.getValue(), entry.getLabel());
//			}
//			else if (entry.getValue() == 'HolidayWork') {
//				HOLIDAYWORK = new AttRequestType(entry.getValue(), entry.getLabel());
//			}
//		}
	}


	/** 申請のリスト（種別が追加されら本リストにも追加してください) */
	public static final List<AttRequestType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttRequestType> {
			LEAVE,
			HOLIDAYWORK,
			EARLY_START_WORK,
			OVERTIME_WORK,
			ABSENCE,
			DIRECT,
			PATTERN
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttRequestType(String value, String label) {
		super(value, label);
	}

	/**
	 * AttRequestType
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttRequestType
	 */
	public static AttRequestType valueOf(String value) {
		AttRequestType retType = null;
		if (String.isNotBlank(value)) {
			for (AttRequestType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
					break;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttRequestType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttRequestType) {
			// 値が同じであればtrue
			if (this.value == ((AttRequestType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}