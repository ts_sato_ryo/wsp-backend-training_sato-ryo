/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description LogicalDeleteServiceのテストクラス
 */
@isTest
private class LogicalDeleteServiceTest {

	private class TestData {
		/** 組織 */
		public ComOrgSetting__c org;
		/** 国 */
		public ComCountry__c country;
		/** 会社 */
		public ComCompany__c company;
		/** タグ */
		public List<ComTag__c> tags;
		/** 短時間勤務設定 */
		public List<AttShortTimeWorkSettingBase__c> shortTimeSettings;

		/** コンストラクタ　*/
		public TestData() {
			org = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			country = ComTestDataUtility.createCountry('Japan');
			company = ComTestDataUtility.createCompany('Company1', this.country.Id);
			tags = new List<ComTag__c> {
				ComTestDataUtility.createTag('tag1_1', TagType.JOB_ASSIGN_GROUP, company.Id, 1, false),
				ComTestDataUtility.createTag('tag1_2', TagType.JOB_ASSIGN_GROUP, company.Id, 1, false)
			};
			shortTimeSettings = new List<AttShortTimeWorkSettingBase__c> {
				ComTestDataUtility.createAttShortTimeSettingWithHistory('ShortTimeSetting', company.id, tags[0].id)
			};
		}
	}

	/**
	 * エンティティ削除のテスト
	 * レコードが参照されていない場合、正常に物理削除出来ることを検証する。
	 */
	@isTest
	static void deleteEntityTestSuccess() {
		TestData data = new TestData();
		LogicalDeleteService service = new LogicalDeleteService(new TagRepository());
		service.deleteEntity(data.tags[1].id);
	}

	/**
	 * エンティティ削除のテスト
	 * レコードが参照されている場合、例外が発生する事を検証する。
	 */
	@isTest
	static void deleteEntityTestFailure() {
		TestData data = new TestData();
		LogicalDeleteService service = new LogicalDeleteService(new TagRepository());
		try {
			service.deleteEntity(data.tags[0].id);
			System.assert(false);
		} catch (DmlException e) {
			System.assertEquals(ComMessage.msg().Com_Err_FaildDeleteReference, e.getMessage());
		}
	}
}