/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description プランナー設定用のサービスクラス。
 */
public with sharing class PlannerSettingService implements Service {

	/** 認証状態 */
	public enum AuthStatus {
		AUTHORIZED,                    // 認証済（認証成功）
		UNAUTHORIZED,                  // 未認証（認証失敗）
		REMOTE_SITE_SETTINGS_INACTIVE, // リモートサイトが有効化されていない
		API_CONNECTION_FAILED          // 外部カレンダーへの接続に失敗
	}

	/** メッセージ */
	private final static ComMsgBase MESSAGE = ComMessage.msg();

	private final static Integer CRYPTO_AESKEY_SIZE = 256;
	private final static String CRYPTO_ALGORITHMNAME = 'AES256';
	private final static Integer ONETIMEKEY_EXPIRATION_MINUTES = 10;

	private CompanyPrivateSettingRepository cpsRepo = new CompanyPrivateSettingRepository();
	private CompanyRepository comRepo = new CompanyRepository();

	/*
	 * 外部カレンダー連携の設定の保存
	 * 会社別非公開設定（カスタム設定）が存在しなければ作成する
	 * @param comEntity 会社エンティティ
	 * @return Id 保存したレコードのID
	 */
	public Id savePlannerSetting(CompanyEntity comEntity) {

		// 更新対象の会社エンティティを取得する
		CompanyEntity updateEntity = comRepo.getEntity(comEntity.Id);
		if (updateEntity == null) {
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
		}

		// 会社別非公開設定（カスタム設定）が存在しなければ作成する
		createPrivateSetting(comEntity.Id);

		// 認証情報をクリアする
		clearPrivateSetting(comEntity, updateEntity);

		// エンティティに保存項目をセットする
		updateEntity.useCalendarAccess = comEntity.useCalendarAccess;

		// 外部カレンダー連携が有効の場合のみ、外部連携サービスを保存する
		if (updateEntity.useCalendarAccess) {
			updateEntity.calendarAccessService = comEntity.calendarAccessService;
		}

		// 保存
		Repository.SaveResult comResult = comRepo.saveEntity(updateEntity);

		return comResult.details[0].id;
	}

	/*
	 * 会社別非公開設定（カスタム設定）の作成
	 * レコードが存在しない場合のみ、新規作成する
	 * @param companyId 会社ID
	 */
	@testVisible
	private void createPrivateSetting(Id companyId) {

		// レコードの存在チェック
		CompanyPrivateSettingEntity cpsEntity = cpsRepo.getEntity(companyId);
		if (cpsEntity == null) {
			cpsEntity = new CompanyPrivateSettingEntity();
			cpsEntity.companyId = companyId;
			// 新規作成
			cpsRepo.saveEntity(cpsEntity);
		}
	}

	/*
	 * 会社別非公開設定（カスタム設定）のクリア
	 * 以下の場合に認証情報（暗号化キー、Office365API認証キー）をクリアする
	 * ・カレンダー連携が無効の場合
	 * ・連携対象サービスを変更した場合
	 * @param comEntity 保存項目を含む会社エンティティ
	 * @param updateEntity 更新対象の会社エンティティ
	 */
	@testVisible
	private void clearPrivateSetting(CompanyEntity comEntity, CompanyEntity updateEntity) {

		// カレンダー連携が無効、または連携対象サービスを変更した場合
		if (!comEntity.useCalendarAccess || comEntity.calendarAccessService != updateEntity.calendarAccessService) {

			CompanyPrivateSettingEntity cpsEntity = cpsRepo.getEntity(comEntity.Id);
			cpsEntity.cryptoKey = null;
			cpsEntity.office365AuthCode = null;
			// 更新
			cpsRepo.saveEntity(cpsEntity);
		}
	}

	/*
	 * 認証情報の保存
	 * ステートの検証後に外部サービスから取得した認証コードを保存する
	 * @param entity 保存対象のエンティティ
	 * @param state 検証用のステート情報
	 */
	public Id saveOAuthCode(CompanyPrivateSettingEntity entity, String state) {

		// stateの検証
		if (state == null || !isValidState(state, entity.companyId)) {
			throw new App.ParameterException(ComMessage.msg().Admin_Err_VerificationFailed);
		}

		// リポジトリを通して保存する
		Repository.SaveResult result = cpsRepo.saveEntity(entity);
		return result.details[0].id;
	}

	/*
	 * 認証状態を確認する
	 * OAuth認証が正しく行われるかを確認する
	 * 認証コードが存在しない場合は未認証（認証失敗）を返却する
	 * @param 会社ID
	 * @return 認証結果
	 */
	public String checkAuth(Id companyId) {

		// 認証コードが存在しない場合は未認証（認証失敗）とする
		String authCode = Office365Service.getAuthCode(companyId);
		if (String.isEmpty(authCode)) {
			return AuthStatus.UNAUTHORIZED.name();
		}

		// 認証チェック
		String authStatus = Office365Service.checkAuth(companyId);
		return authStatus;
	}

	/*
	 * 認証URLの生成
	 * 連携対象のサービスの認証URLとステート情報を繋げて返却する
	 * @param companyId 会社ID
	 * @return 認証URL
	 */
	public String generateOAuthUrl(Id companyId) {

		String authUrl;

		// Office365の認証URLを取得
		authUrl = Office365Service.getOAuthEndpointUrl();

		// ステート情報の作成
		String state = generateState(companyId);

		return authUrl += '&state=' + EncodingUtil.urlEncode(state, 'UTF-8');
	}

	/*
	 * stateの生成
	 * ワンタイムキー、エラーメッセージ表示用言語設定、リダイレクト先URLを用いる
	 * @param companyId 会社ID
	 * @return state
	 */
	@testVisible
	private String generateState(Id companyId) {

		String oneTimeKey = generateOneTimeKey(companyId);
		String lang = UserInfo.getLanguage();
		String sfUrl = URL.getSalesforceBaseUrl().toExternalForm();
		String company = companyId;
		String state = oneTimeKey + ',' + lang + ',' + sfUrl + ',' + company;

		return state;
	}

	/*
	 * stateの検証
	 * @param state ステート情報
	 * @param companyId 会社ID
	 * @return 検証結果（true:検証OK/false:検証NG）
	 */
	@testVisible
	private Boolean isValidState(String state, Id companyId) {

		List<String> stateList = state.split(',', 0);
		if (stateList.size() != 4) {
			return false;
		}

		String encryptedOneTimeKey = stateList[0];
		String localekey = stateList[1];
		String sfUrl = stateList[2];
		return isValidOneTimeKey(encryptedOneTimeKey, companyId) && sfUrl == URL.getSalesforceBaseUrl().toExternalForm();
	}

	/*
	 * ワンタイムキーの生成
	 * ユーザIDと現在日付情報を暗号化する
	 * @param companyId 会社ID
	 * @return encryptedOneTimeKey ワンタイムキー
	 */
	@testVisible
	private String generateOneTimeKey(Id companyId) {

		// ワンタイムキー生成（ユーザID＋現在日付情報）
		Id userId = UserInfo.getUserId();
		String publishedDateTimeString = String.valueOfGmt(Datetime.now());
		String oneTimeKey = (String)userId + publishedDateTimeString;

		// 暗号化キー設定
		setCryptoKey(companyId);

		// ワンタイムキーの暗号化
		return encryptData(oneTimeKey, companyId);
	}

	/*
	 * ワンタイムキーの検証
	 * ユーザIDが同一であるか、認証日時が現時刻-10分〜現時刻の間であるかを判定する
	 * @param encryptedOneTimeKey ワンタイムキー
	 * @param companyId 会社ID
	 * @return 検証結果（true:検証OK/false:検証NG）
	 */
	@testVisible
	private Boolean isValidOneTimeKey(String encryptedOneTimeKey, Id companyId) {

		// ワンタイムキーの復号化
		String decryptedOneTimeKey = decryptData(encryptedOneTimeKey, companyId);

		// ユーザIDの検証
		Id userId = UserInfo.getUserId();
		if (decryptedOneTimeKey == null || decryptedOneTimeKey.indexOf(userId) != 0) {
			return false;
		}

		// 日付情報の検証
		String publishedDateTimeString = decryptedOneTimeKey.replace(userId, '');
		Datetime publishedDateTime;
		try {
			publishedDateTime = Datetime.valueOfGmt(publishedDateTimeString);
		} catch (TypeException te) {
			return false;
		}

		Datetime expirationDatetime = publishedDateTime.addMinutes(ONETIMEKEY_EXPIRATION_MINUTES);
		Datetime nowDatetime = Datetime.now();
		return nowDatetime >= publishedDateTime && nowDatetime <= expirationDatetime;
	}

	/*
	 * ワンタイムキー暗号化（暗号化キーが生成されている前提）
	 * @param data 元データ
	 * @return 暗号化データ（Base64エンコード）
	 */
	@testVisible
	private String encryptData(String data, Id companyId) {

		// 暗号化キーの取得
		Blob key = getCryptoKey(companyId);
		if (String.isBlank(data) || key == null) {
			return '';
		}

		// 元データをBlobに変換する
		Blob blobData = Blob.valueOf(data);

		// 暗号化してBase64で返す
		return EncodingUtil.base64Encode(Crypto.encryptWithManagedIV(CRYPTO_ALGORITHMNAME, key, blobData));
	}

	/*
	 * ワンタイムキー復号化（暗号化キーが生成されている前提）
	 * @param data 暗号化データ（Base64エンコード）
	 * @return 復号済みデータ（復号化に失敗した場合はNULLを返す）
	 */
	@testVisible
	private String decryptData(String data, Id companyId) {

		// 暗号化キーの取得
		Blob key = getCryptoKey(companyId);
		if (String.isBlank(data) || key == null) {
			return null;
		}

		// 暗号化データ（Base64）をデコードする
		Blob decodeData = EncodingUtil.base64Decode(data);

		// 復号化後、Stringに変換して返す
		try {
			return Crypto.decryptWithManagedIV(CRYPTO_ALGORITHMNAME, key, decodeData).toString();
		} catch (Exception e) {
			System.debug(e);
			return null;
		}
	}

	/**
	 * 暗号化キーを設定（設定済の場合は何もしない）
	 * @param companyId 会社ID
	 */
	@testVisible
	private void setCryptoKey(Id companyId) {

		CompanyPrivateSettingEntity entity = cpsRepo.getEntity(companyId);
		if (entity == null) {
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
		}

		if (String.isEmpty(entity.cryptoKey)) {
			entity.cryptoKey = EncodingUtil.base64Encode(Crypto.generateAesKey(CRYPTO_AESKEY_SIZE));
			Repository.SaveResult result = cpsRepo.saveEntity(entity);
		}
	}

	/**
	 * 暗号化キーを取得
	 * @param companyId 会社ID
	 * @return 暗号化キー（暗号化キーが設定されていない場合はNULLを返す）
	 */
	@testVisible
	private Blob getCryptoKey(Id companyId) {

		CompanyPrivateSettingEntity entity = cpsRepo.getEntity(companyId);

		if (entity == null || String.isBlank(entity.cryptoKey)) {
			return null;
		} else {
			return EncodingUtil.base64Decode(entity.cryptoKey);
		}
	}
}
