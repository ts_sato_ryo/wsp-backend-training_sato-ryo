/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通 Common
 *
 * コンテンツドキュメントのリポジトリクラスのテスト Test class for AppContentDocumentRepository
 */
@isTest
private class AppContentDocumentRepositoryTest {

	/**
	 * saveLinkのテスト Test for saveLink method
	 * 新規の保存ができることを確認する Verify new record is created
	 */
	@isTest static void saveLinkTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee,
				testData.job, testData.costCenter.CurrentHistoryId__c,3);
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportList[0].id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c,3);

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion content = new ContentVersion(
			Title = 'Test',
			PathOnClient = 'Test.jpg',
			VersionData = Blob.valueOf(bodyString));
		insert content;

		AppContentDocumentLinkEntity link = new AppContentDocumentLinkEntity();
		link.contentDocumentId = [SELECT ContentDocumentId FROM ContentVersion WHERE Title = :content.Title LIMIT 1].ContentDocumentId;
		link.linkedEntityId = recordList[0].id;

		Test.startTest();

		AppContentDocumentRepository repo = new AppContentDocumentRepository();
		Repository.SaveResult result = repo.saveLink(link);

		Test.stopTest();

		// 保存できていることを確認 Ensure new record is saved
		ContentDocumentLink resObj = [
			SELECT
				Id,
				ContentDocumentId,
				LinkedEntityId,
				ShareType
			FROM ContentDocumentLink
			WHERE Id = :result.details[0].Id];
		System.assertEquals(link.contentDocumentId, resObj.ContentDocumentId);
		System.assertEquals(link.linkedEntityId, resObj.LinkedEntityId);
		System.assertEquals(AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER, resObj.ShareType);
	}

	/**
	 * getEntityListのテスト Test for getEntityList method
	 * ドキュメントを正しく取得できることを確認する Verify the retrieved ContentDocument List
	 */
	@isTest static void getEntityListTest() {

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion content1 = new ContentVersion(
			Title = 'Test1',
			PathOnClient = 'Test1.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content2 = new ContentVersion(
			Title = 'Test2',
			PathOnClient = 'Test2.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content3 = new ContentVersion(
			Title = 'Test3',
			PathOnClient = 'Test3.jpg',
			VersionData = Blob.valueOf(bodyString));
		List<ContentVersion> contentVersionList = new List<ContentVersion>();
		contentVersionList.add(content1);
		contentVersionList.add(content2);
		contentVersionList.add(content3);
		insert contentVersionList;

		Test.startTest();

		// ファイルを取得する Get files
		AppContentDocumentRepository repo = new AppContentDocumentRepository();
		List<AppContentDocumentEntity> resList = repo.getEntityList(null, UserInfo.getUserId());

		Test.stopTest();

		List<ContentDocument> contentDocumentList = [
			SELECT
				Id,
				OwnerId,
				Title,
				FileType,
				LatestPublishedVersionId,
				ContentModifiedDate
			FROM ContentDocument
			ORDER BY ContentModifiedDate ASC];

		System.assertEquals(3, resList.size());
		for (Integer i = 0; i < resList.size(); i++) {
			System.assertEquals(contentDocumentList[i].Id, resList[i].id);
			System.assertEquals(contentDocumentList[i].OwnerId, resList[i].ownerId);
			System.assertEquals(contentDocumentList[i].Title, resList[i].title);
			System.assertEquals(contentDocumentList[i].FileType, resList[i].fileType);
			System.assertEquals(contentDocumentList[i].LatestPublishedVersionId, resList[i].latestPublishedVersionId);
			System.assertEquals(contentDocumentList[i].ContentModifiedDate, resList[i].contentModifiedDate.getDatetime());
		}
	}

	/**
	 * getEntityListのテスト Test for getEntityList method
	 * ドキュメントを正しく取得できることを確認する Verify the retrieved ContentDocument List 
	 */
	@isTest static void getEntityListByIdTest() {

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion content1 = new ContentVersion(
			Title = 'Test1',
			PathOnClient = 'Test1.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content2 = new ContentVersion(
			Title = 'Test2',
			PathOnClient = 'Test2.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content3 = new ContentVersion(
			Title = 'Test3',
			PathOnClient = 'Test3.jpg',
			VersionData = Blob.valueOf(bodyString));
		List<ContentVersion> contentVersionList = new List<ContentVersion>();
		contentVersionList.add(content1);
		contentVersionList.add(content2);
		contentVersionList.add(content3);
		insert contentVersionList;

		List<Id> contentDocumentIdList = new List<Id>();
		for (ContentDocument content : [SELECT Id FROM ContentDocument]) {
			contentDocumentIdList.add(content.id);
		}

		Test.startTest();

		// ファイルを取得する Get files
		AppContentDocumentRepository repo = new AppContentDocumentRepository();
		List<AppContentDocumentEntity> resList = repo.getEntityList(contentDocumentIdList);

		Test.stopTest();

		List<ContentDocument> contentDocumentList = [
			SELECT
				Id,
				OwnerId,
				Title,
				FileType,
				LatestPublishedVersionId,
				ContentModifiedDate
			FROM ContentDocument
			WHERE Id IN :contentDocumentIdList
			ORDER BY ContentModifiedDate ASC];

		System.assertEquals(3, resList.size());
		for (Integer i = 0; i < resList.size(); i++) {
			System.assertEquals(contentDocumentList[i].Id, resList[i].id);
			System.assertEquals(contentDocumentList[i].OwnerId, resList[i].ownerId);
			System.assertEquals(contentDocumentList[i].Title, resList[i].title);
			System.assertEquals(contentDocumentList[i].FileType, resList[i].fileType);
			System.assertEquals(contentDocumentList[i].LatestPublishedVersionId, resList[i].latestPublishedVersionId);
			System.assertEquals(contentDocumentList[i].ContentModifiedDate, resList[i].contentModifiedDate.getDatetime());
		}
	}

	/**
	 * getLinkEntityListByLinkedEntityIdのテスト Test for getLinkEntityListByLinkedEntityId method
	 * ドキュメントリンクを正しく取得できることを確認する Verify the retrieved AppContentDocumentLinkEntity List
	 */
	@isTest static void getLinkEntityListByLinkedEntityIdTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee,
				testData.job, testData.costCenter.CurrentHistoryId__c,3);
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportList[0].id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c,3);

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion content1 = new ContentVersion(
			Title = 'Test1',
			PathOnClient = 'Test1.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content2 = new ContentVersion(
			Title = 'Test2',
			PathOnClient = 'Test2.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content3 = new ContentVersion(
			Title = 'Test3',
			PathOnClient = 'Test3.jpg',
			VersionData = Blob.valueOf(bodyString));
		List<ContentVersion> contentVersionList = new List<ContentVersion>();
		contentVersionList.add(content1);
		contentVersionList.add(content2);
		contentVersionList.add(content3);
		insert contentVersionList;

		// 経費明細とファイルを紐づける Link expense records and files
		Id contentDocumentId1 = [SELECT ContentDocumentId FROM ContentVersion WHERE Title = :content1.Title LIMIT 1].ContentDocumentId;
		ContentDocumentLink link1 = new ContentDocumentLink(
			ContentDocumentId = contentDocumentId1,
			LinkedEntityId = recordList[0].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		Id contentDocumentId2 = [SELECT ContentDocumentId FROM ContentVersion WHERE Title = :content2.Title LIMIT 1].ContentDocumentId;
		ContentDocumentLink link2 = new ContentDocumentLink(
			ContentDocumentId = contentDocumentId2,
			LinkedEntityId = recordList[1].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		List<ContentDocumentLink> linkList = new List<ContentDocumentLink>();
		linkList.add(link1);
		linkList.add(link2);
		insert linkList;

		Test.startTest();

		// ドキュメントリンクを取得する Get content document links
		AppContentDocumentRepository repo = new AppContentDocumentRepository();
		List<AppContentDocumentLinkEntity> resList = repo.getLinkEntityListByLinkedEntityId(new List<Id>{recordList[0].id, recordList[1].id, recordList[2].id});

		Test.stopTest();

		System.assertEquals(2, resList.size());
		System.assertEquals(contentDocumentId1, resList[0].contentDocumentId);
		System.assertEquals(recordList[0].id, resList[0].linkedEntityId);
		System.assertEquals(contentDocumentId2, resList[1].contentDocumentId);
		System.assertEquals(recordList[1].id, resList[1].linkedEntityId);
	}
	
	/**
	 * getNoOfLinksByContentDocumentIdのテスト Test for getNoOfLinksByContentDocumentId method
	 * ドキュメントカウントを正しく取得できることを確認する Verify count value retrieved
	 */
	@isTest static void getNoOfLinksByContentDocumentIdTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee,
				testData.job, testData.costCenter.CurrentHistoryId__c,3);
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportList[0].id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c,3);

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion content1 = new ContentVersion(
			Title = 'Test1',
			PathOnClient = 'Test1.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content2 = new ContentVersion(
			Title = 'Test2',
			PathOnClient = 'Test2.jpg',
			VersionData = Blob.valueOf(bodyString));
		ContentVersion content3 = new ContentVersion(
			Title = 'Test3',
			PathOnClient = 'Test3.jpg',
			VersionData = Blob.valueOf(bodyString));
		List<ContentVersion> contentVersionList = new List<ContentVersion>();
		contentVersionList.add(content1);
		contentVersionList.add(content2);
		contentVersionList.add(content3);
		insert contentVersionList;

		/* 経費明細とファイルを紐づける Link expense records and files
		 * File 1 => Link with ExpRecord
		 * File 2 => Link with ExpRecord
		 * File 3 => No links
		 */
		List<ContentVersion> insertedContentVersionList = [SELECT Title, ContentDocumentId FROM ContentVersion];
		
		ContentDocumentLink link1 = new ContentDocumentLink(
			ContentDocumentId = insertedContentVersionList[0].ContentDocumentId,
			LinkedEntityId = recordList[0].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		ContentDocumentLink link2 = new ContentDocumentLink(
			ContentDocumentId = insertedContentVersionList[1].ContentDocumentId,
			LinkedEntityId = recordList[1].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		List<ContentDocumentLink> linkList = new List<ContentDocumentLink>();
		linkList.add(link1);
		linkList.add(link2);
		insert linkList;

		Test.startTest();
		
		// Get number of links for Files 2 and 3
		AppContentDocumentRepository repo = new AppContentDocumentRepository();
		Integer result = repo.getNoOfLinksByContentDocumentId(new List<Id>{insertedContentVersionList[1].ContentDocumentId, 
				insertedContentVersionList[2].ContentDocumentId});

		Test.stopTest();
		
		// Confirm count result is 3 (2 for ContentVersion + 1 for ExpRecord)
		System.assertEquals(3, result);
	}
}