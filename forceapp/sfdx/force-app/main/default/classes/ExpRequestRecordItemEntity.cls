/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * 経費内訳のエンティティ Entity class for ExpRequestRecordItem
 */
public with sharing class ExpRequestRecordItemEntity extends ExpRecordItem {

	/** Fields definition */
	public enum Field {
		EXP_REQUEST_RECORD_ID
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;


	/** Constructor */
	public ExpRequestRecordItemEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** Request Record ID */
	public Id expRequestRecordId {
		get;
		set {
			expRequestRecordId = value;
			setChanged(Field.EXP_REQUEST_RECORD_ID);
		}
	}


	/**
	 * Mark field as changed
	 * @param field Changed field
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
	}

	/*
	* Return the field value
	* @param name Target field name
	*/
	public override Object getValue(String name) {
		Field field = ExpRequestRecordItemEntity.FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

		return super.getValue(name);
	}

	/**
	* Return the field value
	* @param field Target field to get the value
	* */
	private Object getValue(ExpRequestRecordItemEntity.Field field) {
		if (field == ExpRequestRecordItemEntity.Field.EXP_REQUEST_RECORD_ID) {
			return expRequestRecordId;
		}

		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}