/**
 * 開始終了時刻のペア値オブジェクト
 */
public with sharing class AttTimePeriod implements Comparable {
		public AttTime startTime {get; private set;}
		public AttTime endTime {get; private set;}

		public AttTimePeriod(AttTime startTime, AttTime endTime) {
			this.startTime = startTime;
			this.endTime = endTime;
		}
		/**
		 * 開始時刻、終了時刻の昇順で5個までの時間帯をソートする。
		 */
		public static List<AttTimePeriod> sortByStartTime(
				AttTime startTime1, AttTime endTime1,
				AttTime startTime2, AttTime endTime2,
				AttTime startTime3, AttTime endTime3,
				AttTime startTime4, AttTime endTime4,
				AttTime startTime5, AttTime endTime5) {

			List<AttTimePeriod> sortList = new List<AttTimePeriod>();
			sortList.add(new AttTimePeriod(startTime1, endTime1));
			sortList.add(new AttTimePeriod(startTime2, endTime2));
			sortList.add(new AttTimePeriod(startTime3, endTime3));
			sortList.add(new AttTimePeriod(startTime4, endTime4));
			sortList.add(new AttTimePeriod(startTime5, endTime5));
			sortList.sort();
			return sortList;
		}
		/**
		 * 比較の結果であるinteger値を返します
		 * @param 比較対象
		 * @return このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満
		 */
		public Integer compareTo(Object compareTo) {
			AttTimePeriod compareTime = (AttTimePeriod)compareTo;
			Integer startTime = AppConverter.intValue(this.startTime);
			Integer endTime = AppConverter.intValue(this.endTime);
			Integer startTimeTo = AppConverter.intValue(compareTime.startTime);
			Integer endTimeTo = AppConverter.intValue(compareTime.endTime);

			// 両方がnullのケース
			if ((startTime == null && endTime == null)) {
				return App.COMPARE_AFTER;
			}
			if ((startTimeTo == null && endTimeTo == null)) {
				return App.COMPARE_BEFORE;
			}

			// 片方がnullのケース
			if (startTime == null) {
				if (startTimeTo == null) {
					return endTime < endTimeTo ? App.COMPARE_BEFORE : App.COMPARE_AFTER;
				} else if (endTimeTo == null) {
					return App.COMPARE_AFTER;
				} else {
					return App.COMPARE_AFTER;
				}
			} else if (endTime == null) {
				if (startTimeTo == null) {
					return App.COMPARE_BEFORE;
				} else if (endTimeTo == null) {
					return startTime < startTimeTo ? App.COMPARE_BEFORE : App.COMPARE_AFTER;
				} else {
					return App.COMPARE_AFTER;
				}
			// 両方ともnullではない場合　
			} else {
				if (startTimeTo == null) {
					return App.COMPARE_BEFORE;
				} else if (endTimeTo == null) {
					return App.COMPARE_BEFORE;
				} else {
					if (startTime == StartTimeTo) {
						return endTime < endTimeTo ? App.COMPARE_BEFORE : App.COMPARE_AFTER;
					}
					return startTime < startTimeTo ? App.COMPARE_BEFORE : App.COMPARE_AFTER;
				}
			}
		}
	}