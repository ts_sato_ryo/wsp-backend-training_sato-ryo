/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description ExpOCREntity
 *
 * @group 経費 Expense
 */
public with sharing class ExpOCREntity extends Entity {

	public enum Field {
		COMPANY_ID,
		EMPLOYEE_HISTORY_ID,
		END_DATE_TIME,
		OCR_ALL_TEXT,
		OCR_AMOUNT,
		OCR_DATE,
		OCR_PROCESS_STATUS,
		OCR_RESPONSE,
		RECEIPT_ID,
		START_DATE_TIME,
		TASK_ID
	}

	private Set<Field> isChangedFieldSet;

	/*
	 * Constructor
	 */
	public ExpOCREntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	public Id employeeHistoryId {
		get;
		set {
			employeeHistoryId = value;
			setChanged(Field.EMPLOYEE_HISTORY_ID);
		}
	}

	public AppDatetime endDateTime {
		get;
		set {
			endDateTime = value;
			setChanged(Field.END_DATE_TIME);
		}
	}

	public String ocrAllText {
		get;
		set {
			ocrAllText = value;
			setChanged(Field.OCR_ALL_TEXT);
		}
	}

	public Decimal ocrAmount {
		get;
		set {
			ocrAmount = value;
			setChanged(Field.OCR_AMOUNT);
		}
	}

	public AppDate ocrDate {
		get;
		set {
			ocrDate = value;
			setChanged(Field.OCR_DATE);
		}
	}

	public String ocrProcessStatus {
		get;
		set {
			ocrProcessStatus = value;
			setChanged(Field.OCR_PROCESS_STATUS);
		}
	}

	public String ocrResponse {
		get;
		set {
			ocrResponse = value;
			setChanged(Field.OCR_RESPONSE);
		}
	}

	public String receiptId {
		get;
		set {
			receiptId = value;
			setChanged(Field.RECEIPT_ID);
		}
	}

	public AppDatetime startDateTime {
		get;
		set {
			startDateTime = value;
			setChanged(Field.START_DATE_TIME);
		}
	}

	public String taskId {
		get;
		set {
			taskId = value;
			setChanged(Field.TASK_ID);
		}
	}

	/*
	 * 項目の値を変更済みに設定する Mark field as changed
	 * @param Field 変更した項目 Field Changed field
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/*
	 * 項目の変更情報を取得する Check if the value of the field has been changed
	 * @param field 取得対象の項目 Target field
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse True if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

 	/*
	 * 項目の変更情報をリセットする Reset the changed status
	 */
	public virtual void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}