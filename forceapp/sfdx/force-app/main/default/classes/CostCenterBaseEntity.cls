/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Entity class representing the Cost Center Base Record
 */
public with sharing class CostCenterBaseEntity  extends CostCenterBaseGeneratedEntity {
	/**
	 * Maximum Lenth Allows for the Cost Center Code
	 * This is different from the linkage code
	 */
	public static final Integer CODE_MAX_LENGTH = 20;

	/*
	 * Maximum Level of Cost Center Hierarchy Allows.
	 */
	public static final Integer HIERARCHY_MAX_LEVEL= 10;

	public CostCenterBaseEntity() {
		super();
	}

	public CostCenterBaseEntity(ComCostCenterBase__c sobj) {
		super(sobj);
	}

	@testVisible
	protected List<CostCenterHistoryEntity> historyList;

	public ParentChildBaseCompanyEntity.Company company {
		get {
			if (company == null) {
				company = new ParentChildBaseCompanyEntity.Company(sobj.CompanyId__r.Code__c);
			}
			return company;
		}
		set;
	}

	/*
	 * Get the CostCenterHistoryEntity List
	 */
	public List<CostCenterHistoryEntity> getHistoryList() {
		return historyList;
	}

	/*
	 * Add history to the base entity history list.
	 */
	public void addHistory(CostCenterHistoryEntity history) {
		if (historyList == null) {
			historyList = new List<CostCenterHistoryEntity>();
		}
		historyList.add(history);
	}

	/*
	 * Retrieve the history for the given index.
	 */
	public CostCenterHistoryEntity getHistory(Integer index) {
		if (historyList == null) {
			return null;
		}
		return historyList[index];
	}

	/*
	 * Return the CostCenterHistoryEntity List as the ParentChildHistoryEntity List
	 */
	protected override List<ParentChildHistoryEntity> convertSuperHistoryList() {
		return (List<ParentChildHistoryEntity>) historyList;
	}

	/**
	 * Create a Copy of CostCenterBaseEntity(ID is not copied.)
	 */
	public CostCenterBaseEntity copy() {
		CostCenterBaseEntity copyEntity = new CostCenterBaseEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 */
	public override ParentChildBaseEntity copySuperEntity() {
		return copy();
	}
}