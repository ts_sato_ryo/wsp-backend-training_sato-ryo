/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Service class for Exchange Rate
 */
public class ExpExchangeRateService {

	private enum CheckMode {
		CHECK_BY_ID,
		CHECK_BY_CODE,
		CHECK_BY_CURRENCY
	}

	/**
   * Create a new Exchange Rate record
   * @param entity Entity to be saved
   * @return Repository.SaveResult
   */
	public static Repository.SaveResult createExchangeRate(ExpExchangeRateEntity entity) {

		validateEntity(entity);

		setUniqKey(entity);

		// If target record exists, throw error.
		if (isExistRecord(entity, CheckMode.CHECK_BY_CODE)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		if (isExistRecord(entity, CheckMode.CHECK_BY_CURRENCY)) {
			throw new App.ParameterException(ComMessage.msg().Exp_Err_DuplicateExchangeRate);
		}

		return new ExpExchangeRateRepository().saveEntity(entity);
	}

	/**
	 * Update a new Exchange Rate record
	 * @param entity Entity to be updated
	 * @return
	 */
	public static Repository.SaveResult updateExchangeRate(ExpExchangeRateEntity entity) {

		// Validate entity value
		validateEntity(entity);

		// Set unique key to entity
		setUniqKey(entity);

		// If target record dose not exist, throw error.
		if (!isExistRecord(entity, CheckMode.CHECK_BY_ID)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_RecordNotFound);
		}

		// If target the code record exists, throw error.
		if (isExistRecord(entity, CheckMode.CHECK_BY_CODE)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		if (isExistRecord(entity, CheckMode.CHECK_BY_CURRENCY)) {
			throw new App.ParameterException(ComMessage.msg().Exp_Err_DuplicateExchangeRate);
		}

		return new ExpExchangeRateRepository().saveEntity(entity);
	}

	/**
   * Delete a new Exchange Rate record
   * @param entity Entity to be deleted
   * @return
   */
	public static void deleteExchangeRate(Id id) {

		ExpExchangeRateEntity entity = new ExpExchangeRateEntity();
		entity.setId(id);

		// If target record has been deleted, do nothing
		if (!isExistRecord(entity, CheckMode.CHECK_BY_ID)) return;

		new ExpExchangeRateRepository().deleteEntity(id);
	}

	/**
   * Search a Exchange Rate record by record ID
   * @param id Target record ID
   * @return List of fetched Currency records
   */
	public static List<ExpExchangeRateEntity> searchExchangeRate(Map<String, Object> paramMap) {
		ExpExchangeRateRepository.SearchFilter filter = new ExpExchangeRateRepository.SearchFilter();

		if (paramMap != null) {
			Id id = (Id)paramMap.get('id');
			filter.ids = String.isBlank(id) ? null : new Set<Id>{id};

			String code = (String)paramMap.get('code');
			filter.codes = String.isBlank(code) ? null : new Set<String>{code};

			Id companyId = (Id)paramMap.get('companyId');
			filter.companyIds = String.isBlank(companyId) ? null : new Set<Id>{companyId};

			Id currencyId = (Id)paramMap.get('currencyId');
			filter.currencyIds = String.isBlank(currencyId) ? null : new Set<Id>{currencyId};

			String targetDate = (String)paramMap.get('targetDate');
			filter.targetDate = String.isBlank(targetDate) ? null : AppDate.valueOf(targetDate);
		}
		return new ExpExchangeRateRepository().searchEntity(filter);
	}

	/**
   * Validate entity
   * @param entity Target entity to be validated
   */
	private static void validateEntity(ExpExchangeRateEntity entity) {
		Validator.Result result = new ExpConfigValidator.ExpExchangeRateValidator(entity).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * Set unique key to the entity
	 * @param entity Target entity
	 */
	private static void setUniqKey(ExpExchangeRateEntity entity) {
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		entity.uniqKey = entity.createUniqKey(company.code);
	}

	/**
	 * Check if the target record exists
	 * @param entity Target entity
	 * @param isCheckById true: Search data by ID, false: Search data by code(and company)
	 */
	private static Boolean isExistRecord(ExpExchangeRateEntity entity, CheckMode mode) {
		ExpExchangeRateRepository.SearchFilter filter = new ExpExchangeRateRepository.SearchFilter();
		ExpExchangeRateRepository rep = new ExpExchangeRateRepository();
		if (mode == CheckMode.CHECK_BY_ID) {
			return (rep.getEntity(entity.id) != null);
		} else if (mode == CheckMode.CHECK_BY_CURRENCY) {
			filter.companyIds = new Set<Id>{ entity.companyId };
			filter.currencyIds = new Set<Id>{ entity.currencyId };
			filter.targetPeriod = new List<AppDate>{entity.validFrom, entity.validTo};
			return checkResult(rep.searchEntity(filter), entity);
		} else {
			filter.companyIds = new Set<Id>{ entity.companyId };
			filter.codes = new Set<String>{ entity.code };
			return checkResult(rep.searchEntity(filter), entity);
		}
	}

	private static Boolean checkResult(List<ExpExchangeRateEntity> result, ExpExchangeRateEntity entity) {
		return (result.size() > 0) && (result[0] != null) && (entity.id <> result[0].id);
	}
}