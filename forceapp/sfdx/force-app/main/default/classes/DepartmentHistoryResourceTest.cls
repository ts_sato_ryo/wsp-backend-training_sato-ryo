/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description DepartmentHistoryResourceのテストクラス
*/
@isTest
private class DepartmentHistoryResourceTest {

	/**
	 * 部署履歴レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComDeptBase__c base = ComTestDataUtility.createDepartmentWithHistory('部署ベース', company.Id);
		ComDeptBase__c parent = ComTestDataUtility.createDepartmentWithHistory('親部署', company.Id);
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, base.Id, Userinfo.getUserId(), permission.Id);
		// 履歴の最新日を今日にする
		List<ComDeptHistory__c> testHistoryList = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c =:base.Id];
		Date today = System.today();
		for(Integer i = 0, n = testHistoryList.size(); i < n; i++){
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		Test.startTest();

		DepartmentHistoryResource.DepartmentHistory param = new DepartmentHistoryResource.DepartmentHistory();
		param.baseId = base.Id;
		param.name_L0 = 'テスト部署_L0';
		param.name_L1 = 'テスト部署_L1';
		param.name_L2 = 'テスト部署_L2';
		param.parentId = parent.Id;
		param.managerId = manager.Id;
		param.remarks = 'Test備考';
		param.comment = 'Testコメント';
		param.validDateFrom = today + 1;
		param.validDateTo = today + 31;

		DepartmentHistoryResource.CreateApi api = new DepartmentHistoryResource.CreateApi();
		DepartmentHistoryResource.SaveResult res = (DepartmentHistoryResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 部署ベース・履歴レコードが1件ずつ作成されること
		List<ComDeptBase__c> newBaseRecords = [SELECT
				Id,
				Name,
				Code__c,
				CompanyId__c,
				CurrentHistoryId__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ParentBaseId__c,
						ManagerBaseId__c,
						Remarks__c,
						HistoryComment__c,
						ValidFrom__c,
						ValidTo__c
					FROM Histories__r
					WHERE Id =: res.Id
				)
			FROM ComDeptBase__c
			WHERE Id != :parent.Id];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].Histories__r.size());

		// 値の検証
		ComDeptBase__c newBase = newBaseRecords[0];
		ComDeptHistory__c newHistory = newBase.Histories__r[0];

		// レスポンス値の検証
		System.assertEquals(newHistory.Id, res.Id);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = newBase.Code__c +
			'-' + DateTime.newInstance(param.validDateFrom, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd') +
			'-' + DateTime.newInstance(param.validDateTo, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd');
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.name_L0, newHistory.Name);
		System.assertEquals(param.name_L0, newHistory.Name_L0__c);
		System.assertEquals(param.name_L1, newHistory.Name_L1__c);
		System.assertEquals(param.name_L2, newHistory.Name_L2__c);
		System.assertEquals(param.parentId, newHistory.ParentBaseId__c);
		System.assertEquals(param.managerId, newHistory.ManagerBaseId__c);
		System.assertEquals(param.remarks, newHistory.Remarks__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
	}

	/**
	 * 部署履歴レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 部署管理権限を無効に設定する
		testData.permission.isManageDepartment = false;
		new PermissionRepository().saveEntity(testData.permission);

		DepartmentHistoryResource.DepartmentHistory param = new DepartmentHistoryResource.DepartmentHistory();
		param.baseId = testData.department.id;
		param.name_L0 = 'テスト部署_L0';
		param.validDateFrom = Date.today() + 1;
		param.validDateTo = param.validDateFrom + 31;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				DepartmentHistoryResource.CreateApi api = new DepartmentHistoryResource.CreateApi();
				DepartmentHistoryResource.SaveResult res = (DepartmentHistoryResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 部署履歴レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeRequiredErrorTest() {
		Test.startTest();

		DepartmentHistoryResource.DepartmentHistory param = new DepartmentHistoryResource.DepartmentHistory();
		DepartmentHistoryResource.CreateApi api = new DepartmentHistoryResource.CreateApi();
		App.ParameterException ex;
		// DmlException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
		// } catch (DmlException e) {
			ex = e;
		}

		Test.stopTest();

//		System.assertEquals('REQUIRED_FIELD_MISSING', ex.getDmlStatusCode(0));
	}

	/**
	 * 部署履歴レコードを1件作成する(異常系:有効開始日が不正)
	 */
	@isTest
	static void creativeInvalidValidDateErrorTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c base = ComTestDataUtility.createDepartmentWithHistory('部署ベース', company.Id);
		ComDeptBase__c parent = ComTestDataUtility.createDepartmentWithHistory('親部署', company.Id);

		// 履歴の最新日を今日にする
		List<ComDeptHistory__c> testHistoryList = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c =:base.Id];
		Date today = System.today();
		for(Integer i = 0, n = testHistoryList.size(); i < n; i++){
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		Test.startTest();

		DepartmentHistoryResource.DepartmentHistory param = new DepartmentHistoryResource.DepartmentHistory();
		param.baseId = base.Id;
		param.name_L0 = 'テスト部署_L0';
		param.name_L1 = 'テスト部署_L1';
		param.name_L2 = 'テスト部署_L2';
		param.remarks = 'Test備考';
		// 最新履歴の有効開始日と同日の改定を可能にしたため、日付をtoday-1に変更
		param.validDateFrom = today - 1;
		// param.validDateFrom = today;  // "最新日付より後"ではないのでエラー

		DepartmentHistoryResource.CreateApi api = new DepartmentHistoryResource.CreateApi();

		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assert(ex != null);
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Admin_Err_InvalidRevisionDate, ex.getMessage());

	}

	//-------------------------------------
	// 履歴更新APIは現在利用できません。
	//-------------------------------------
	// /**
	//  * 部署レコードを1件更新する(正常系)
	//  */
	// @isTest
	// static void updatePositiveTest() {
   //
	// 	ComCompany__c company = ComTestDataUtility.createTestCompany();
	// 	ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test親部署', company.Id);
	// 	ComDeptHistory__c history = [SELECT Id FROM ComDeptHistory__c LIMIT 1];
   //
	// 	Test.startTest();
   //
	// 	// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
	// 	// 全パラメータの検証は createPositiveTest() で行っている
	// 	Map<String, Object> param = new Map<String, Object> {
	// 		'id' => history.id,
	// 		'name_L0' => '部署履歴更新テスト'
	// 	};
   //
	// 	DepartmentHistoryResource.UpdateApi api = new DepartmentHistoryResource.UpdateApi();
	// 	Object res = api.execute(param);
   //
	// 	Test.stopTest();
   //
	// 	System.assertEquals(null, res);
   //
	// 	// 部署レコードが更新されていること
	// 	List<ComDeptHistory__c> deptList = [SELECT
	// 			Name_L0__c
	// 		FROM ComDeptHistory__c
	// 		WHERE Id =:(String)param.get('id')];
   //
	// 	System.assertEquals(1, deptList.size());
	// 	System.assertEquals((String)param.get('name_L0'), deptList[0].Name_L0__c);
	// }
   //
	// /**
	//  * 部署レコードを1件更新する(異常系:必須パラメータが未設定)
	//  */
	// @isTest
	// static void updateNegativeTest() {
	// 	Test.startTest();
   //
	// 	DepartmentResource.Department param = new DepartmentResource.Department();
   //
	// 	App.ParameterException ex;
	// 	try {
	// 		Object res = (new DepartmentResource.UpdateApi()).execute(param);
	// 	} catch (App.ParameterException e) {
	// 		ex = e;
	// 	}
   //
	// 	Test.stopTest();
   //
	// 	System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	// }

	/**
	 * 部署履歴レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);
		ComDeptHistory__c delRecord = [SELECT Name_L0__c, UniqKey__c, BaseId__c, ValidFrom__c, ValidTo__c FROM ComDeptHistory__c LIMIT 1];
		// ベースに対して履歴が2件以上存在する場合のみ削除が可能なため、レコードを追加する
		ComDeptHistory__c history2 = delRecord.clone();
		history2.UniqKey__c += '_2';
		insert history2;
		Test.startTest();

		DepartmentHistoryResource.DeleteOption param = new DepartmentHistoryResource.DeleteOption();
		param.id = delRecord.id;

		// １回目では削除が成功し、２回目ではスキップすること
		(new DepartmentHistoryResource.DeleteApi()).execute(param);
		(new DepartmentHistoryResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 部署レコードが論理削除されること
		List<ComDeptHistory__c> recordList = [SELECT Id, Removed__c FROM ComDeptHistory__c WHERE Id =:delRecord.Id];
		System.assertEquals(1, recordList.size());
		System.assertEquals(true, recordList[0].Removed__c);
	}

	/**
	 * 部署履歴レコードを1件削除する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		DepartmentBaseEntity delDepartment = testData.createDepartment('削除');

		// 部署管理権限を無効に設定する
		testData.permission.isManageDepartment = false;
		new PermissionRepository().saveEntity(testData.permission);

		DepartmentHistoryResource.DeleteOption param = new DepartmentHistoryResource.DeleteOption();
		param.id = delDepartment.getHistory(0).id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				(new DepartmentHistoryResource.DeleteApi()).execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 部署履歴レコードを1件削除する(異常系:ベースに対して既存履歴が1件のみ)
	 */
	@isTest
	static void deleteNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);
		ComDeptHistory__c delRecord = [SELECT UniqKey__c, BaseId__c FROM ComDeptHistory__c LIMIT 1];

		Test.startTest();

		DepartmentHistoryResource.DeleteOption param = new DepartmentHistoryResource.DeleteOption();
		param.id = delRecord.id;

		// 削除エラーが発生し、履歴削除されないこと
		App.IllegalStateException ex;

		try {
			(new DepartmentHistoryResource.DeleteApi()).execute(param);
		} catch (App.IllegalStateException e) {
			ex = e;
		}

		Test.stopTest();

		System.assert(ex != null);
		System.assertEquals(1, [SELECT COUNT() FROM ComDeptHistory__c WHERE Id =:delRecord.Id]);

	}

	/**
	 * 部署履歴レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		DepartmentHistoryResource.DeleteOption param = new DepartmentHistoryResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new DepartmentHistoryResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 部署履歴レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<ComDeptBase__c> departments = ComTestDataUtility.createDepartmentsWithHistory('テスト部署', company.Id, 3);
		ComDeptHistory__c history = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c IN :departments LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		DepartmentHistoryResource.SearchApi api = new DepartmentHistoryResource.SearchApi();
		DepartmentHistoryResource.SearchResult res = (DepartmentHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 部署レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComDeptHistory__c history = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :dept.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		DepartmentHistoryResource.SearchApi api = new DepartmentHistoryResource.SearchApi();
		DepartmentHistoryResource.SearchResult res = (DepartmentHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [
				SELECT Name_L1__c,ManagerBaseId__c,
					ManagerBaseId__r.FirstName_L0__c, ManagerBaseId__r.LastName_L0__c,
					ManagerBaseId__r.FirstName_L1__c, ManagerBaseId__r.LastName_L1__c,
					ManagerBaseId__r.FirstName_L2__c, ManagerBaseId__r.LastName_L2__c
				FROM ComDeptHistory__c WHERE Id =:history.Id LIMIT 1];
		System.assertEquals(history.Name_L1__c, res.records[0].name);
		if (history.ManagerBaseId__c != null) {
			String managerName = new AppPersonName(
					history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
					history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
					history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(managerName, res.records[0].manager.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].manager.name));
		}
		// System.assertEquals(history.ManagerBaseId__r.FullName_L1__c, res.records[0].manager.name);
	}

	/**
	 * 部署レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComDeptHistory__c history = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :dept.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		DepartmentHistoryResource.SearchApi api = new DepartmentHistoryResource.SearchApi();
		DepartmentHistoryResource.SearchResult res = (DepartmentHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [
				SELECT Name_L2__c,ManagerBaseId__c,
					ManagerBaseId__r.FirstName_L0__c, ManagerBaseId__r.LastName_L0__c,
					ManagerBaseId__r.FirstName_L1__c, ManagerBaseId__r.LastName_L1__c,
					ManagerBaseId__r.FirstName_L2__c, ManagerBaseId__r.LastName_L2__c
				FROM ComDeptHistory__c WHERE BaseId__c =:dept.Id LIMIT 1];
		System.assertEquals(history.Name_L2__c, res.records[0].name);
		if (history.ManagerBaseId__c != null) {
			String managerName = new AppPersonName(
					history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
					history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
					history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(managerName, res.records[0].manager.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].manager.name));
		}
	}

	/**
	 * 部署レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		List<ComDeptBase__c> departments = ComTestDataUtility.createDepartmentsWithHistory('Test親部署', company.Id, 3);
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, departments[0].Id, Userinfo.getUserId(), permission.Id);
		ComDeptHistory__c testHistory = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c =:departments[0].Id LIMIT 1];
		testHistory.ParentBaseId__c = departments[1].Id;
		testHistory.ManagerBaseId__c = manager.Id;
		testHistory.Remarks__c = 'テスト備考';
		update testHistory;

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>();
		DepartmentHistoryResource.SearchApi api = new DepartmentHistoryResource.SearchApi();
		DepartmentHistoryResource.SearchResult res = (DepartmentHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		List<ComDeptHistory__c> historyList = [
			SELECT
				Id,
				BaseId__c,
				UniqKey__c,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				ParentBaseId__c,
				ParentBaseId__r.Name,
				ManagerBaseId__c,
				ManagerBaseId__r.FirstName_L0__c,
				ManagerBaseId__r.FirstName_L1__c,
				ManagerBaseId__r.FirstName_L2__c,
				ManagerBaseId__r.LastName_L0__c,
				ManagerBaseId__r.LastName_L1__c,
				ManagerBaseId__r.LastName_L2__c,
//				ManagerBaseId__r.FullName_L0__c,
				Remarks__c,
				ValidFrom__c,
				ValidTo__c,
				HistoryComment__c
			FROM ComDeptHistory__c];

		ComDeptHistory__c parentHistory = [SELECT Id, Name_L0__c From ComDeptHistory__c WHERE BaseId__c = :testHistory.ParentBaseId__c LIMIT 1];

		// 部署レコードが取得できること
		System.assertEquals(historyList.size(), res.records.size());

		for (Integer i = 0, n = historyList.size(); i < n; i++) {
			ComDeptHistory__c history = historyList[i];
			DepartmentHistoryResource.DepartmentHistory dto = res.records[i];

			System.assertEquals(history.Id, dto.id);
			System.assertEquals(history.BaseId__c, dto.baseId);
			System.assertEquals(history.Name_L0__c, dto.name);	// 部署名のデフォルトはL0を返す
			System.assertEquals(history.Name_L0__c, dto.name_L0);
			System.assertEquals(history.Name_L1__c, dto.name_L1);
			System.assertEquals(history.Name_L2__c, dto.name_L2);
			System.assertEquals(history.ParentBaseId__c, dto.parentId);
			// System.assertEquals(history.ParentBaseId__r.Name, dto.parent.name);
			System.assertEquals(history.ManagerBaseId__c, dto.managerId);
//			System.assertEquals(history.ManagerBaseId__r.FullName_L0__c, dto.manager.name);
			System.assertEquals(history.ValidFrom__c, dto.validDateFrom);
			System.assertEquals(history.ValidTo__c, dto.validDateTo);
			System.assertEquals(history.Remarks__c, dto.remarks);
			System.assertEquals(history.HistoryComment__c, dto.comment);

			if (dto.parentId != null) {
				System.assertEquals(parentHistory.Name_L0__c, dto.parent.name);
			} else {
				System.assertEquals(true, String.isBlank(dto.parent.name));
			}

			if (dto.managerId != null) {
				AppPersonName managerFullName = (new AppPersonName(
						history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
						history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
						history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c));
				System.assertEquals(managerFullName.getFullName(), dto.manager.name);
			} else {
				System.assertEquals(true, String.isBlank(dto.manager.name));
			}
		}

	}

}
