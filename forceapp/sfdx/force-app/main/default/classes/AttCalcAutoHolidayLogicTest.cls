/**
 */
@isTest
private class AttCalcAutoHolidayLogicTest {
	static AttCalcAutoHolidayLogic logic = new AttCalcAutoHolidayLogic();

	private class Day implements AttCalcAutoHolidayLogic.iTargetDay {
		public boolean isAdmit = false;
		public boolean isHolidayWork = false;
		public boolean isLegalHoliday= false;
		public AttCalcAutoHolidayLogic.DayType dayType;
		public Day(AttCalcAutoHolidayLogic.DayType dayType ) {
			this.dayType = dayType;
		}
		public Day(AttCalcAutoHolidayLogic.DayType dayType, boolean isHolidayWork) {
			this(dayType);
			this.isHolidayWork = isHolidayWork;
		}
		public Boolean isAdmit() {
			return isAdmit;
		}

		public AttCalcAutoHolidayLogic.DayType getDayType() {
			return dayType;
		}

		public Boolean isHolidayWork() {
			return isHolidayWork;
		}

		public Boolean isLegalHoliday() {
			return isLegalHoliday;
		}
	}

	private class Model implements AttCalcAutoHolidayLogic.iInput, AttCalcAutoHolidayLogic.iOutput {
		public List<Day> dayList;
		public Integer legalHolidayCount;
		public Boolean[] legalHolidayList;
		public Integer noAssigndLeginHolidayCount = 0;
		public Model(String week, Integer n) {
			dayList = new List<Day>();
			for(Integer c : week.getChars()) {
				if (c == 'h'.getChars()[0]) {
					dayList.add(new Day(AttCalcAutoHolidayLogic.DayType.Holiday));
				} else if (c == 'l'.getChars()[0]) {
					dayList.add(new Day(AttCalcAutoHolidayLogic.DayType.LegalHoliday));
				}else if (c == 'p'.getChars()[0]) {
					dayList.add(new Day(AttCalcAutoHolidayLogic.DayType.PreferredLegalHoliday));
				} else if (c == 'H'.getChars()[0]) {
					dayList.add(new Day(AttCalcAutoHolidayLogic.DayType.Holiday, true));
				} else if (c == 'L'.getChars()[0]) {
					dayList.add(new Day(AttCalcAutoHolidayLogic.DayType.LegalHoliday, true));
				}else if (c == 'P'.getChars()[0]) {
					dayList.add(new Day(AttCalcAutoHolidayLogic.DayType.PreferredLegalHoliday, true));
				}else  {
					dayList.add(new Day(AttCalcAutoHolidayLogic.DayType.Workday));
				}
			}
			legalHolidayCount = n;
		}
		public void setIsAdmit(Integer frm, Integer to, Boolean fix) {
			for(Integer i = frm; i < to; i++)
				dayList[i].isAdmit = fix;
		}
		public void setHolidayWork(Integer i, Boolean b) {
			System.assertEquals(false, dayList[i].isAdmit());
			dayList[i].isHolidayWork = b;
		}
		public void setDayType(Integer i, AttCalcAutoHolidayLogic.DayType t) {
			System.assertEquals(false, dayList[i].isAdmit());
			dayList[i].dayType = t;
		}

		public Integer getLegalHolidayCount() {
			return this.legalHolidayCount;
		}

		public List<AttCalcAutoHolidayLogic.iTargetDay> getTargetDayList() {
			return this.dayList;
		}

		public void setLegalHoliday(Boolean[] days) {
			this.legalHolidayList = days;
			for(Integer i = 0; i < dayList.size(); i++)
				dayList[i].isLegalHoliday = days[i];
		}
		public void assertHoliday(List<Integer> h) {
			List<Boolean> b = new Boolean[legalHolidayList.size()];
			for (Integer i =0; i <= b.size()-1; i++) {
				b[i] = false;
			}
			for(Integer i: h) {
				b[i] = true;
			}
			assertArrayEquals(b, legalHolidayList);
			System.assertEquals(0, noAssigndLeginHolidayCount);
		}
		private void assertArrayEquals(List<Boolean> i1, List<Boolean> i2) {
			System.assertEquals(i1.size(), i2.size());
			for (Integer i =0; i <= i1.size()-1; i++) {
				System.assertEquals(i1[i], i2[i]);
			}
		}
	}
	
	static private void patternTest(Integer n, String week, List<Integer> h) {
		Model m = new Model(week, n);
		Integer l = week.length();
		// 確定なしの場合のテスト
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(h);
		// 期間の前半を確定した場合のテスト
		m.setIsAdmit(0,l/2, true);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(h);
		// 期間の前半の確定を解除した後のテスト
		m.setIsAdmit(0,l, false);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(h);
		// 期間の後半を確定した場合のテスト
		m.setIsAdmit(l/2,l, true);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(h);
	}

	/**
	 * 法定休日決定の基本的なパターンのテスト
	 * 
	 * 法定休日(l)>休日出勤してない優先法定休日(p)>休日出勤してない休日(h)>休日出勤している優先法定休日(P)>休日出勤している休日(H)>勤務日(.)
	 * の順で同じレベルのものが複数ある場合はあとのものが法定休日として選ばれているかのテスト
	 */

	@isTest static void testApplyBasicCase() {
		patternTest(1, 'lphH...', new List<Integer>{0});
		patternTest(1, 'Hhpl...', new List<Integer>{3});
		patternTest(1, 'LphH...', new List<Integer>{0});
		patternTest(1, 'HhpL...', new List<Integer>{3});
		patternTest(1, 'phH....', new List<Integer>{0});
		patternTest(1, 'Hhp....', new List<Integer>{2});
		patternTest(1, 'hhPH...', new List<Integer>{1});
		patternTest(1, 'HPhh...', new List<Integer>{3});
		patternTest(1, 'PH.....', new List<Integer>{0});
		patternTest(1, 'HP.....', new List<Integer>{1});
		patternTest(1, 'HH.....', new List<Integer>{1});
		patternTest(1, '.......', new List<Integer>{6});
		patternTest(4, 'p.....hp.....hp.....hp.....h', new List<Integer>{0, 7, 14, 21});
		patternTest(4, 'h.....hh.....hh.....hh.....h', new List<Integer>{14, 20, 21, 27});
		patternTest(4, 'llllpppphhhhPPPPHHHH........', new List<Integer>{0, 1, 2, 3});
		patternTest(4, 'HHHHPPPPhhhhppppllll........', new List<Integer>{16, 17, 18, 19});
		patternTest(4, 'lllpppphhhhPPPPHHHH.........', new List<Integer>{0, 1, 2, 6});
		patternTest(4, 'HHHHPPPPhhhhpppplll.........', new List<Integer>{15, 16, 17, 18});
		patternTest(4, 'lpppphhhhPPPPHHHH...........', new List<Integer>{0, 2, 3, 4});
		patternTest(4, 'HHHHPPPPhhhhppppl...........', new List<Integer>{13, 14, 15, 16});
		patternTest(4, 'pppphhhhPPPPHHHH............', new List<Integer>{0, 1, 2, 3});
		patternTest(4, 'HHHHPPPPhhhhpppp............', new List<Integer>{12, 13, 14, 15});
		patternTest(4, 'ppphhhhPPPPHHHH.............', new List<Integer>{0, 1, 2, 6});
		patternTest(4, 'HHHHPPPPhhhhppp.............', new List<Integer>{11, 12, 13, 14});
		patternTest(4, 'phhhhPPPPHHHH...............', new List<Integer>{0, 2, 3, 4});
		patternTest(4, 'HHHHPPPPhhhhp...............', new List<Integer>{9, 10, 11, 12});
		patternTest(4, 'hhhhPPPPHHHH................', new List<Integer>{0, 1, 2, 3});
		patternTest(4, 'HHHHPPPPhhhh................', new List<Integer>{8, 9, 10, 11});
		patternTest(4, 'hhhPPPPHHHH.................', new List<Integer>{0, 1, 2, 6});
		patternTest(4, 'HHHHPPPPhhh.................', new List<Integer>{7, 8, 9, 10});
		patternTest(4, 'hPPPPHHHH...................', new List<Integer>{0, 2, 3, 4});
		patternTest(4, 'HHHHPPPPh...................', new List<Integer>{5, 6, 7, 8});
		patternTest(4, 'PPPPHHHH....................', new List<Integer>{0, 1, 2, 3});
		patternTest(4, 'HHHHPPPP....................', new List<Integer>{4, 5, 6, 7});
		patternTest(4, 'PPPHHHH.....................', new List<Integer>{0, 1, 2, 6});
		patternTest(4, 'HHHHPPP.....................', new List<Integer>{3, 4, 5, 6});
		patternTest(4, 'PHHHH.......................', new List<Integer>{0, 2, 3, 4});
		patternTest(4, 'HHHHP.......................', new List<Integer>{1, 2, 3, 4});
		patternTest(4, 'HHHH........................', new List<Integer>{0, 1, 2, 3});
		patternTest(4, 'HHHHH.......................', new List<Integer>{1, 2, 3, 4});
		patternTest(4, 'HHH.........................', new List<Integer>{0, 1, 2, 27});
		patternTest(4, 'H...........................', new List<Integer>{0, 25, 26, 27});
		patternTest(4, '............................', new List<Integer>{24, 25, 26, 27});
		patternTest(4, 'lphPHlphPH..................', new List<Integer>{0, 1, 5, 6});
		patternTest(4, 'HPhplHPhpl..................', new List<Integer>{3, 4, 8, 9});
		patternTest(4, 'phPHlphPH...................', new List<Integer>{0, 4, 5, 6});
		patternTest(4, 'HPhpHPhpl...................', new List<Integer>{3, 6, 7, 8});
		patternTest(4, 'phPHlhPH....................', new List<Integer>{0, 1, 4, 5});
		patternTest(4, 'HPhpHPhl....................', new List<Integer>{2, 3, 6, 7});
		patternTest(4, 'phPHlPH.....................', new List<Integer>{0, 1, 4, 5});
		patternTest(4, 'HPhpHPl.....................', new List<Integer>{2, 3, 5, 6});
		patternTest(4, 'phPHlH......................', new List<Integer>{0, 1, 2, 4});
		patternTest(4, 'HhpHPl......................', new List<Integer>{1, 2, 4, 5});
		patternTest(4, 'phPHl.......................', new List<Integer>{0, 1, 2, 4});
		patternTest(4, 'HhpPl.......................', new List<Integer>{1, 2, 3, 4});
		patternTest(4, 'phPH........................', new List<Integer>{0, 1, 2, 3});
		patternTest(4, 'HhpP........................', new List<Integer>{0, 1, 2, 3});
		patternTest(4, 'phP.........................', new List<Integer>{0, 1, 2, 27});
		patternTest(4, 'HhP.........................', new List<Integer>{0, 1, 2, 27});
	}
	/**
	 * 法定休日自動判定と確定のテスト01
	 * 
	 * 一度選ばれた法定休日指定が確定後、未確定の日の休日出勤が変わったことで
	 * 影響を受けないテスト
	 * 
	 */

	@isTest static void testApplyBasicFix01() {
		// １週１休、日曜起算日
		Model m = new Model('h.....H', 1);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{0}); // 土曜は出勤しているので、日曜が法定休日

		m.setIsAdmit(0, 3, true); // 日-火を確定済みにする
		m.setHolidayWork(6, false); // 土曜の休日出勤をクリアする
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{0}); // 日曜は確定済みなので法定休日は移動しない

		m.setIsAdmit(0, 3, false); // 確定済みを解除
		AttCalcAutoHolidayLogic.apply(m, m); 
		m.assertHoliday(new List<Integer>{6}); // 法定休日は土曜に移動する
	}
	/**
	 * 法定休日自動判定と確定のテスト02
	 * 
	 * 休日出勤してない優先法定休日は、確定済みで法定休日指定の対象になることを確認
	 * 
	 */

	@isTest static void testApplyBasicFix02() {
		// １週１休、日曜起算日
		Model m = new Model('p.....p', 1);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{6}); // 最初は後ろの土曜が法定休日

		m.setIsAdmit(0, 3, true); // 日-火を確定済みにする
		m.setDayType(6, AttCalcAutoHolidayLogic.DayType.Workday); // 土曜を勤務日にする
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{0}); // 日曜は確定済みでも勤務してないので法定休日になる
	}
	/**
	 * 法定休日自動判定と確定のテスト03
	 * 
	 * 休日出勤してない休日は、確定済みで法定休日指定の対象になることを確認
	 * 
	 */

	@isTest static void testApplyBasicFix03() {
		// １週１休、日曜起算日
		Model m = new Model('h.....h', 1);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{6}); // 最初は後ろの土曜が法定休日

		m.setIsAdmit(0, 3, true); // 日-火を確定済みにする
		m.setHolidayWork(6, true); // 土曜を休日出勤にする
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{0}); // 日曜は確定済みでも勤務してないので法定休日になる
	}
	/**
	 * 法定休日自動判定と確定のテスト04
	 * 
	 * 休日出勤してない休日は、確定済みで法定休日指定の対象になることを確認
	 * 
	 */

	@isTest static void testApplyBasicFix04() {
		// １週１休、日曜起算日
		Model m = new Model('P.....h', 1);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{6}); // 最初は後ろの土曜が法定休日

		m.setIsAdmit(0, 3, true); // 日-火を確定済みにする
		m.setHolidayWork(6, true); // 土曜を休日出勤にする
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{6}); // 日曜は確定済みで休日出勤をしているので法定休日にならない
	}
	/**
	 * 法定休日自動判定と確定のテスト05
	 * 
	 * 休日出勤してない休日は、確定済みで法定休日指定の対象になることを確認
	 * 
	 */

	@isTest static void testApplyBasicFix05() {
		// １週１日、日曜起算日
		Model m = new Model('H.....h', 1);
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{6}); // 最初は後ろの土曜が法定休日

		m.setIsAdmit(0, 3, true); // 日-火を確定済みにする
		m.setDayType(6, AttCalcAutoHolidayLogic.DayType.Workday); // 土曜を勤務日に変える
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{6}); // 日曜は確定済みで休日出勤をしているので法定休日にならない
	}
	/**
	 * 法定休日自動判定と確定のテスト06
	 * 
	 * 既に確定済みの平日は法定休日にならない
	 * 
	 */

	@isTest static void testApplyBasicFix06() {
		// １週１日、日曜起算日
		Model m = new Model('.......', 1); // 休日なし
		m.setIsAdmit(4, 7, true); // 木-土を確定済みにする
		AttCalcAutoHolidayLogic.apply(m, m);
		m.assertHoliday(new List<Integer>{3}); // 最後の確定されていない勤務日が法定休日になる
	}
}