/*
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Currency Pair pick list
 */
public class ExpCurrencyPair extends ValueObjectType {

	/** List of the entries */
	private static final List<ExpCurrencyPair> TYPE_LIST;
	/** Map of the entries (Key : value of the entry) */
	private static final Map<String, ExpCurrencyPair> TYPE_MAP;

	/**
	 * Static Initializer
 	 */
	static {
		TYPE_LIST = new List<ExpCurrencyPair>();
		TYPE_MAP = new Map<String, ExpCurrencyPair>();

		List<Schema.PicklistEntry> pickList = ExpExchangeRate__c.CurrencyPair__c.getDescribe().getPicklistValues();

		for (Schema.PicklistEntry entry : pickList) {
			// Set only active entry
			if (entry.isActive()) {
				ExpCurrencyPair c = new ExpCurrencyPair(entry.getValue(), entry.getLabel());
				TYPE_LIST.add(c);
				TYPE_MAP.put(c.value, c);
			}
		}
	}

	/**
	 * Constructor
	 * @param value Value of the pick list entry
	 * @param label Label of the pick list entry
	 */
	private ExpCurrencyPair(String value, String label) {
		super(value, label);
	}

	/**
   * Return the value whose key matches with specified string
   * @param The key value to get the instance
   * @return The instance having the value specified
   */
	public static ExpCurrencyPair valueOf(String value) {
		return TYPE_MAP.get(value);
	}

	/**
	 * Return the value witch contains both currency code
	 * @param Two currency codes
	 * @return The instance having the two currency codes given
	 */
	public static ExpCurrencyPair valueOf(String currencyA, String currencyB) {

		ExpCurrencyPair result = null;

		for (ExpCurrencyPair p : TYPE_LIST) {
			String A = p.value.substring(0, 3);
			String B = p.value.substring(4, 7);

			Boolean matchA = (A.equals(currencyA) || (A.equals(currencyB)));
			Boolean matchB = (B.equals(currencyA) || (B.equals(currencyB)));

			if (matchA && matchB) {
				result = p;
				break;
			}
		}

		return result;
	}

	/**
	 * Return the type list
	 * @return TYPE_LIST
	 */
	public static List<ExpCurrencyPair> getTypeList() {
		return TYPE_LIST;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {
		// if null, return false.
		if (compare == null) {
			return false;
		}

		// if not a instance of ExpCurrencyPair, return false.
		if (!(compare instanceof ExpCurrencyPair)) {
			return false;
		}

		ExpCurrencyPair compareObj = (ExpCurrencyPair) compare;
		return (this.value.equals(compareObj.value));
	}
}