/**
 * 有休計算ロジックテストクラス
 */
@isTest
private class AttManagedLeaveCalcLogicTest {
	// ******** 未確定休暇サマリ計算テストケース
	// 初回
	@isTest static void calcUnfixedNothing() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = Date.today();
		unfixedSummary.endDate = Date.today().addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(1, retList.size());
		System.assertEquals(0, retList[0].daysTakenTotal);
		System.assertEquals(0, retList[0].daysLeftTotal);
		System.assertEquals(0, retList[0].daysExpiredTotal);
	}

	// 付与あり、消費なし
	@isTest static void calcUnfixedOnlyGranted() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 今休暇サマリ
		Date dt = Date.today();
		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt;
		unfixedSummary.endDate = dt.addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);
		// 今月末失効1日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addMonths(1);
		grantedData.daysGranted = 1;
		grantedList.add(grantedData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(1, retList.size());
		System.assertEquals(0, retList[0].daysTakenTotal);
		System.assertEquals(11, retList[0].daysLeftTotal);
		System.assertEquals(1, retList[0].daysExpiredTotal);

		System.assertEquals(2, retList[0].grantedSummaryList.size());
		System.assertEquals(0, retList[0].grantedSummaryList[0].daysTaken);
		System.assertEquals(1, retList[0].grantedSummaryList[0].daysLeft);
		System.assertEquals(0, retList[0].grantedSummaryList[1].daysTaken);
		System.assertEquals(10, retList[0].grantedSummaryList[1].daysLeft);
	}
	// 付与あり、消費なし、複数サマリ
	@isTest static void calcMoreUnfixedOnlyGranted() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 休暇サマリ
		Date dt = Date.today();
		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt;
		unfixedSummary.endDate = dt.addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt.addMonths(1);
		unfixedSummary.endDate = dt.addMonths(2).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);
		// 今月末失効1日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addMonths(1);
		grantedData.daysGranted = 1;
		grantedList.add(grantedData);

		// 来月末失効1日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(1);
		grantedData.validDateTo = dt.addMonths(2);
		grantedData.daysGranted = 2;
		grantedList.add(grantedData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(2, retList.size());
		// 11日有休、1日失効
		System.assertEquals(0, retList[0].daysTakenTotal);
		System.assertEquals(11, retList[0].daysLeftTotal);
		System.assertEquals(1, retList[0].daysExpiredTotal);
		System.assertEquals(2, retList[0].grantedSummaryList.size());
		System.assertEquals(0, retList[0].grantedSummaryList[0].daysTaken);
		System.assertEquals(1, retList[0].grantedSummaryList[0].daysLeft);
		System.assertEquals(0, retList[0].grantedSummaryList[1].daysTaken);
		System.assertEquals(10, retList[0].grantedSummaryList[1].daysLeft);

		// 12日有休、2日失効
		System.assertEquals(0, retList[1].daysTakenTotal);
		System.assertEquals(12, retList[1].daysLeftTotal);
		System.assertEquals(2, retList[1].daysExpiredTotal);
		System.assertEquals(2, retList[1].grantedSummaryList.size());
		System.assertEquals(0, retList[1].grantedSummaryList[0].daysTaken);
		System.assertEquals(2, retList[1].grantedSummaryList[0].daysLeft);
		System.assertEquals(0, retList[1].grantedSummaryList[1].daysTaken);
		System.assertEquals(10, retList[1].grantedSummaryList[1].daysLeft);
	}

	// 付与あり、消費あり
	@isTest static void calcUnfixedWithTaken() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 今休暇サマリ
		Date dt = Date.today();
		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt;
		unfixedSummary.endDate = dt.addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);
		// 今月末失効5日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addMonths(1).addDays(-1);
		grantedData.daysGranted = 5;
		grantedList.add(grantedData);

		// 2日消費
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(11);
		takenData.daysTaken = 2;
		takenList.add(takenData);
		// 1日消費
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(18);
		takenData.endDate = dt.addDays(18);
		takenData.daysTaken = 1;
		takenList.add(takenData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(1, retList.size());
		System.assertEquals(3, retList[0].daysTakenTotal);
		System.assertEquals(12, retList[0].daysLeftTotal);
		System.assertEquals(2, retList[0].daysExpiredTotal);

		System.assertEquals(2, retList[0].grantedSummaryList.size());
		System.assertEquals(3, retList[0].grantedSummaryList[0].daysTaken);
		System.assertEquals(2, retList[0].grantedSummaryList[0].daysLeft);
		System.assertEquals(0, retList[0].grantedSummaryList[1].daysTaken);
		System.assertEquals(10, retList[0].grantedSummaryList[1].daysLeft);
	}

	// 付与あり、消費あり、複数サマリ
	@isTest static void calcMoreUnfixedWithTaken() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 休暇サマリ
		Date dt = Date.today();
		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt;
		unfixedSummary.endDate = dt.addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt.addMonths(1);
		unfixedSummary.endDate = dt.addMonths(2).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);
		// 今月末失効5日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addMonths(1);
		grantedData.daysGranted = 5;
		grantedList.add(grantedData);

		// 来月末失効5日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(1);
		grantedData.validDateTo = dt.addMonths(2);
		grantedData.daysGranted = 5;
		grantedList.add(grantedData);

		// 今月2日消費
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(11);
		takenData.daysTaken = 2;
		takenList.add(takenData);
		// 来月1日消費
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addMonths(1).addDays(18);
		takenData.endDate = dt.addMonths(1).addDays(18);
		takenData.daysTaken = 1;
		takenList.add(takenData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(2, retList.size());
		// 15日(10+5)有休、3日失効、2日消費
		System.assertEquals(2, retList[0].daysTakenTotal);
		System.assertEquals(13, retList[0].daysLeftTotal);
		System.assertEquals(3, retList[0].daysExpiredTotal);

		System.assertEquals(2, retList[0].grantedSummaryList.size());
		System.assertEquals(2, retList[0].grantedSummaryList[0].daysTaken);
		System.assertEquals(3, retList[0].grantedSummaryList[0].daysLeft);
		System.assertEquals(0, retList[0].grantedSummaryList[1].daysTaken);
		System.assertEquals(10, retList[0].grantedSummaryList[1].daysLeft);

		// 15日(10+5)有休、4日失効、1日消費
		System.assertEquals(1, retList[1].daysTakenTotal);
		System.assertEquals(14, retList[1].daysLeftTotal);
		System.assertEquals(4, retList[1].daysExpiredTotal);

		System.assertEquals(2, retList[1].grantedSummaryList.size());
		System.assertEquals(1, retList[1].grantedSummaryList[0].daysTaken);
		System.assertEquals(4, retList[1].grantedSummaryList[0].daysLeft);
		System.assertEquals(0, retList[1].grantedSummaryList[1].daysTaken);
		System.assertEquals(10, retList[1].grantedSummaryList[1].daysLeft);
	}
	// 失効付与あり、消費あり
	@isTest static void calcUnfixedWithTakenAndExpired() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 今休暇サマリ
		Date dt = Date.today();
		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt;
		unfixedSummary.endDate = dt.addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);
		// 今月中失効1日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addDays(10);
		grantedData.validDateTo = dt.addDays(11);
		grantedData.daysGranted = 1;
		grantedList.add(grantedData);

		// 2日消費
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(11);
		takenData.daysTaken = 2;
		takenList.add(takenData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(1, retList.size());
		System.assertEquals(2, retList[0].daysTakenTotal); // 付与2の1日＋付与1の1日
		System.assertEquals(9, retList[0].daysLeftTotal); // 付与1の9日
		System.assertEquals(0, retList[0].daysExpiredTotal); // 失効を使われたため、0
	}
	// 確定済み付与あり
	@isTest static void calcUnfixedWithFixedSummary() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		Date dt = Date.today();

		// 先月から10日付与,5日消費確定
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(-1);
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedData.daysLeft = 5;
		grantedList.add(grantedData);

		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = Date.today();
		unfixedSummary.endDate = Date.today().addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 2日消費
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(11);
		takenData.daysTaken = 2;
		takenList.add(takenData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(1, retList.size());
		System.assertEquals(2, retList[0].daysTakenTotal);
		System.assertEquals(3, retList[0].daysLeftTotal);
		System.assertEquals(0, retList[0].daysExpiredTotal);
	}
	// 消費オーバー
	@isTest static void calcUnfixedWithTakenOver() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 今休暇サマリ
		Date dt = Date.today();
		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt;
		unfixedSummary.endDate = dt.addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 今月から5日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 5;
		grantedList.add(grantedData);
		// 今月末失効1日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addMonths(1);
		grantedData.daysGranted = 1;
		grantedList.add(grantedData);

		// 7日消費
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(16);
		takenData.daysTaken = 7;
		takenList.add(takenData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		Boolean hasError = false;
		String errorCode;
		try {
			AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		}
		catch (AttManagedLeaveCalcLogic.DaysNotEnoughException ex) {
			hasError = true;
		}
		Test.stopTest();

		System.assertEquals(true, hasError);
	}

	// 増減日数計算
	@isTest static void calcUnfixedWithAdjusted() {
		List<AttManagedLeaveCalcLogic.SummaryEntity> unfixedSummaryList = new List<AttManagedLeaveCalcLogic.SummaryEntity>();
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 今休暇サマリ
		Date dt = Date.today();
		AttManagedLeaveCalcLogic.SummaryEntity unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt;
		unfixedSummary.endDate = dt.addMonths(1).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		unfixedSummary = new AttManagedLeaveCalcLogic.SummaryEntity();
		unfixedSummary.startDate = dt.addMonths(1);
		unfixedSummary.endDate = dt.addMonths(2).addDays(-1);
		unfixedSummaryList.add(unfixedSummary);

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedData.originalDaysGranted = 11;
		// 今月増1日
		grantedData.adjustedList.add(new AttManagedLeaveCalcLogic.AdjustedEntity(dt.addDays(10), 1));
		// 来月減2日
		grantedData.adjustedList.add(new AttManagedLeaveCalcLogic.AdjustedEntity(dt.addMonths(1).addDays(10), -2));
		grantedList.add(grantedData);

		List<AttManagedLeaveCalcLogic.SummaryEntity> retList;
		Test.startTest();
		retList = AttManagedLeaveCalcLogic.calcUnfixedSummaryList(unfixedSummaryList, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(2, retList.size());
		System.assertEquals(12, retList[0].daysAdjustedTotal); // 当初付与日数11+調整日数1
		System.assertEquals(-2, retList[1].daysAdjustedTotal);
	}
	// ******** 残日数計算テストケース
	// 付与あり、消費なし
	@isTest static void calcDaysLeftOnlyGranted() {
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 計算日
		Date dt = Date.today();

		// 計算日から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);
		// 計算日失効1日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(-1);
		grantedData.validDateTo = dt;
		grantedData.daysGranted = 1;
		grantedList.add(grantedData);

		Test.startTest();
		Decimal daysLeft = AttManagedLeaveCalcLogic.calcDaysLeft(dt, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(10, daysLeft);
	}
	// 付与・消費あり
	@isTest static void calcDaysLeftWithTaken() {
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 計算日
		Date dt = Date.today();

		// 計算日以前から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(-1);
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);

		// 計算日前1日消化
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(-1);
		takenData.endDate = dt.addDays(-1);
		takenData.daysTaken = 1;
		takenList.add(takenData);

		// 計算日後1日消化
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(10);
		takenData.daysTaken = 1;
		takenList.add(takenData);

		Test.startTest();
		Decimal daysLeft = AttManagedLeaveCalcLogic.calcDaysLeft(dt, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(8, daysLeft);
	}

	// 確定済み
	@isTest static void calcDaysLeftWithFixedTaken() {
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 計算日
		Date dt = Date.today();

		// 計算日以前から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(-1);
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedData.daysLeft = 5; // 残日数5日確定
		grantedList.add(grantedData);

		// 計算日前2日消化
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(-2);
		takenData.endDate = dt.addDays(-1);
		takenData.daysTaken = 2;
		takenList.add(takenData);

		// 計算日後2日消化
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(11);
		takenData.daysTaken = 2;
		takenList.add(takenData);

		Test.startTest();
		Decimal daysLeft = AttManagedLeaveCalcLogic.calcDaysLeft(dt, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(1, daysLeft);
	}

	// 確定済み
	@isTest static void calcDaysLeftWithMoreGranted() {
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		// 計算日
		Date dt = Date.today();

		// 計算日以前から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(-1);
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedData.daysLeft = 5; // 残日数5日確定
		grantedList.add(grantedData);
		// 計算日以前から5日付与
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addMonths(-2);
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 5;
		grantedList.add(grantedData);

		// 計算日前6日消化
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(-6);
		takenData.endDate = dt.addDays(-1);
		takenData.daysTaken = 6;
		takenList.add(takenData);

		// 計算日後2日消化
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(11);
		takenData.daysTaken = 2;
		takenList.add(takenData);

		Test.startTest();
		Decimal daysLeft = AttManagedLeaveCalcLogic.calcDaysLeft(dt, grantedList, takenList);
		Test.stopTest();

		System.assertEquals(2, daysLeft);
	}
	// ******** 消費＆付与マッピングテストケース
	// 消費が少ない
	@isTest static void matchTakenAndGrantdTest() {
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		Date dt = Date.today();

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedData.daysLeft = 10;
		grantedList.add(grantedData);

		// 2日消費
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt;
		takenData.endDate = dt.addDays(1);
		takenData.daysTaken = 2;
		takenList.add(takenData);
		// 1日消費
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(10);
		takenData.daysTaken = 1;
		takenList.add(takenData);

		Test.startTest();
		AttManagedLeaveCalcLogic.matchTakenToGranted(takenList, grantedList);
		System.assertEquals(7, grantedData.daysLeft);
		Test.stopTest();
	}
	// 消費が多い
	@isTest static void matchTakenAndGrantdTestOver() {
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		Date dt = Date.today();

		// 今月から10日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 10;
		grantedList.add(grantedData);

		// 2日消費
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt;
		takenData.endDate = dt.addDays(1);
		takenData.daysTaken = 2;
		takenList.add(takenData);
		// 1日消費
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(10);
		takenData.endDate = dt.addDays(10);
		takenData.daysTaken = 1;
		takenList.add(takenData);

		// 8日消費(1日多い)
		takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt.addDays(20);
		takenData.endDate = dt.addDays(27);
		takenData.daysTaken = 8;
		takenList.add(takenData);

		Test.startTest();
		Boolean hasError = false;
		try {
			AttManagedLeaveCalcLogic.matchTakenToGranted(takenList, grantedList);
		}
		catch (AttManagedLeaveCalcLogic.DaysNotEnoughException e) {
			hasError = true;
			System.assertEquals(1, e.daysNotEnough);
		}
		System.assertEquals(true, hasError);
		Test.stopTest();
	}
	// 消費が失効日以降
	@isTest static void matchTakenAndGrantdTestExpired() {
		List<AttManagedLeaveCalcLogic.TakenEntity> takenList = new List<AttManagedLeaveCalcLogic.TakenEntity>();
		List<AttManagedLeaveCalcLogic.GrantedEntity> grantedList = new List<AttManagedLeaveCalcLogic.GrantedEntity>();

		Date dt = Date.today();

		// 今日から5日付与
		AttManagedLeaveCalcLogic.GrantedEntity grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt;
		grantedData.validDateTo = dt.addYears(2);
		grantedData.daysGranted = 5;
		grantedData.daysLeft = 5;
		grantedList.add(grantedData);

		// 臨時2日付与(実1日しか使えない)
		grantedData = new AttManagedLeaveCalcLogic.GrantedEntity();
		grantedData.validDateFrom = dt.addDays(3);
		grantedData.validDateTo = dt.addDays(4);
		grantedData.daysGranted = 2;
		grantedData.daysLeft = 2;
		grantedList.add(grantedData);

		// 今日から5日取得
		AttManagedLeaveCalcLogic.TakenEntity takenData = new AttManagedLeaveCalcLogic.TakenEntity();
		takenData.startDate = dt;
		takenData.endDate = dt.addDays(4);
		takenData.daysTaken = 5;
		takenList.add(takenData);

		Test.startTest();
		AttManagedLeaveCalcLogic.matchTakenToGranted(takenList, grantedList);
		System.assertEquals(1, grantedList[1].daysLeft); // 失効日先付与は1日分のみ
		System.assertEquals(1, grantedList[0].daysLeft); // 通常付与は4日分
		Test.stopTest();
	}
}