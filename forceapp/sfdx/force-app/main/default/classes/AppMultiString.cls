/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * 多言語文字列の値オブジェクト
 */
public with sharing class AppMultiString extends ValueObject {

	/** 多言語文字列(L0) */
	public String valueL0 {get; private set;}

	/** 多言語文字列(L1) */
	public String valueL1 {get; private set;}

	/** 多言語文字列(L2) */
	public String valueL2 {get; private set;}

	/**
	 * コンストラクタ
	 * @param 多言語文字列(L0)
	 */
	public AppMultiString(String l0, String l1, String l2) {
		this.valueL0 = l0;
		this.valueL1 = l1;
		this.valueL2 = l2;
	}

	/**
	 * 実行ユーザの言語に一致した文字列を返却する
	 */
	public String getValue() {
		return getValue(AppLanguage.getUserLang());
	}

	/**
	 * 指定した言語の文字列を返却する。
	 * @param 指定した言語の文字列。ただし、指定した言語の値がnullの場合はL0の値を返却する。
	 */
	public String getValue(AppLanguage lang) {

		String retValue;
		if (lang != null) {
			retValue = getValueByLangKey(lang.getLangkey());
		}

		// 値が取得できなかった場合、
		// 　→ 取得対象の言語キーがL0以外で(L0は必須項目なので必ず値が設定されているはず)、
		// 　  → en_USのキーが存在し、値がnullでなければ英語で返却
		// 　  → そうでない場合は、主言語L0で返却
		if (String.isBlank(retValue)) {
			if ((lang != null) && (lang.getLangkey() != AppLanguage.LangKey.L0)) {
				final AppLanguage.LangKey enKey = AppLanguage.EN_US.getLangkey();

				if (enKey != null) {
					retValue = getValueByLangKey(enKey);
				}
				if (String.isBlank(retValue)) {
					retValue = valueL0;
				}
			}
		}

		return retValue;
	}

	/**
	 * 言語キーに対応する値を取得する
	 * @param langKey 言語キー
	 * @return 言語キーに対応する言語
	 */
	private String getValueByLangKey(AppLanguage.LangKey langKey) {
		String retValue;
		if (langKey == AppLanguage.LangKey.L0) {
			retValue = this.valueL0;
		} else if (langKey == AppLanguage.LangKey.L1) {
			retValue = this.valueL1;
		} else if (langKey == AppLanguage.LangKey.L2) {
			retValue = this.valueL2;
		}
		return retValue;
	}

	/**
	 * 実行ユーザの言語に一致した文字列を返却する
	 * @return 文字列、objがnullの場合はnullを返却
	 */
	public static String stringValue(AppMultiString obj) {
		String retVal;
		if (obj == null) {
			retVal = null;
		} else {
			retVal = obj.getValue();
		}
		return retVal;
	}

	/**
	 * オブジェクトの値が等しいかどうか判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {
		Boolean eq;
		if (compare instanceof AppMultiString) {
			AppMultiString compareObj = (AppMultiString)compare;
			if ((compareObj.valueL0 == this.valueL0)
					&& (compareObj.valueL1 == this.valueL1)
					&& (compareObj.valueL2 == this.valueL2)) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}
		return eq;
	}
}
