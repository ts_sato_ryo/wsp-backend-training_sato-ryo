/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group Common 共通
 *
 * @description 履歴管理方式が親子型のマスタの履歴エンティティの基底クラス
 */
public abstract with sharing class ParentChildHistoryEntity extends BaseEntity {

	/** 有効開始日の最小日付 */
	public static final AppDate VALID_FROM_MIN = AppDate.newInstance(1900, 1, 1);
	/** 有効開始日の最大日付 */
	public static final AppDate VALID_FROM_MAX = AppDate.newInstance(2100, 12, 31);
	/** 失効日の最小日付 */
	public static final AppDate VALID_TO_MIN = AppDate.newInstance(1900, 1, 1);
	/** 失効日の最大日付 */
	public static final AppDate VALID_TO_MAX = AppDate.newInstance(2101, 1, 1);

	/** 履歴コメントの最大文字数 */
	public static final Integer HISTORY_COMMENT_MAX_LENGTH = 1000;

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目値 */
		public AppMultiString nameL;
		/** コンストラクタ */
		public LookupField(AppMultiString nameL) {
			this.nameL = nameL;
		}
	}

	/**
	 * コンストラクタ
	 * @param sobj 参照するSObject
	 * @param sobjectFieldMap フィールド名とフィールドのMap
	 */
	public ParentChildHistoryEntity(SObject sobj, Map<String, Schema.SObjectField> sobjectFieldMap) {
		super(sobj, sobjectFieldMap);
	}

	/** 履歴レコード名 */
	public static final String FIELD_NAME_NAME = 'Name';
	public String name {
		get {
			return (String)getFieldValue(FIELD_NAME_NAME);
		}
		set {
			setFieldValue(FIELD_NAME_NAME, value);
		}
	}

	/** ベースレコードID */
	public static final String FIELD_NAME_BASE_ID = createNameWithNamespace('BaseId__c');
	public Id baseId {
		get {
			return (Id)getFieldValue(FIELD_NAME_BASE_ID);
		}
		set {
			setFieldValue(FIELD_NAME_BASE_ID, value);
		}
	}

	/** 履歴コメント */
	public static final String FIELD_NAME_HISTORY_COMMENT = createNameWithNamespace('HistoryComment__c');
	public String historyComment {
		get {
			return (String)getFieldValue(FIELD_NAME_HISTORY_COMMENT);
		}
		set {
			setFieldValue(FIELD_NAME_HISTORY_COMMENT, value);
		}
	}

	/** 有効開始日 */
	public static final String FIELD_NAME_VALID_FROM = createNameWithNamespace('ValidFrom__c');
	public AppDate validFrom {
		get {
			return AppDate.valueOf((Date)getFieldValue(FIELD_NAME_VALID_FROM));
		}
		set {
			setFieldValue(FIELD_NAME_VALID_FROM, AppConverter.dateValue(value));
		}
	}

	/** 失効日 */
	public static final String FIELD_NAME_VALID_TO = createNameWithNamespace('ValidTo__c');
	public AppDate validTo {
		get {
			return AppDate.valueOf((Date)getFieldValue(FIELD_NAME_VALID_TO));
		}
		set {
			setFieldValue(FIELD_NAME_VALID_TO, AppConverter.dateValue(value));
		}
	}

	/** ユニークキー */
	public static final String FIELD_NAME_UNIQ_KEY = createNameWithNamespace('UniqKey__c');
	public String uniqKey {
		get {
			return (String)getFieldValue(FIELD_NAME_UNIQ_KEY);
		}
		set {
			setFieldValue(FIELD_NAME_UNIQ_KEY, value);
		}
	}

	/** 削除フラグ */
	public static final String FIELD_NAME_IS_REMOVED = createNameWithNamespace('Removed__c');
	public Boolean isRemoved {
		get {
			return getFieldValue(FIELD_NAME_IS_REMOVED) == null ? false : (Boolean)getFieldValue(FIELD_NAME_IS_REMOVED);
		}
		set {
			setFieldValue(FIELD_NAME_IS_REMOVED, value == null ? false : value);
		}
	}

	/**
	 * ユニークキーを生成する
	 * Code__c + '-' + ValidFrom__c + '-' + ValidTo__c ※日付はyyyyMMdd
	 * @return ユニークキー
	 */
	public String createUniqKey(String baseCode) {
		if (this.validFrom == null) {
			// 内部エラーのため多元語不要
			throw new App.ParameterException('The unique key could not be created because the valid from date has not been set.(有効開始日が設定されていないため、ユニークキーが作成できませんでした。)');
		}
		if (this.validTo == null) {
			// 内部エラーのため多元語不要
			throw new App.ParameterException('The unique key could not be created because the valid to date has not been set.(失効日が設定されていないため、ユニークキーが作成できませんでした。)');
		}
		return baseCode + '-' + validFrom.formatYYYYMMDD() + '-' + validTo.formatYYYYMMDD();
	}

	/**
	 * 論理削除状態に設定する
	 */
	public virtual void setLogicallyDelete() {
		// 重複を避けるためにユニークキーはIDに変更しておく
		this.uniqKey = this.id;
		this.isRemoved = true;
	}

	/**
	 * True if history is valid on the targetDate day, otherwise false
	 * 指定した日に履歴が有効であるかをチェックする
	 * @param targetDate 対象日
	 * @return 指定した日に履歴が有効な場合はtrue、そうでない場合はfalse
	 *         True if history is valid on the targetDate day, otherwise false
	 */
	public Boolean isValid(AppDate targetDate) {
		if (this.validFrom == null || this.validTo == null) {
			return false;
		}
		// 有効開始日から失効日前日までが有効期間
		return targetDate.isAfterEquals(this.validFrom) && targetDate.isBefore(this.validTo);
	}

	/**
	 * @description Get field names not to propagate.
	 * 伝搬させない項目を取得する(各Entityで項目が異なる可能性があるためメソッド経由で項目を取得できるようにしている)
	 */
	public virtual Set<String> getNonPropagationFieldNameSet() {
		return HISTORY_MANAGEMENT_FIELD_SET;
	}

	/**
	 * History management fields
	 * 履歴管理項目(改定時の伝搬対象外)
	 */
	public static final Set<String> HISTORY_MANAGEMENT_FIELD_SET = new Set<String>{
			FIELD_NAME_NAME,
			FIELD_NAME_BASE_ID,
			FIELD_NAME_HISTORY_COMMENT,
			FIELD_NAME_VALID_FROM,
			FIELD_NAME_VALID_TO,
			FIELD_NAME_UNIQ_KEY,
			FIELD_NAME_IS_REMOVED};

	/**
	 * @description Return the instance of ParentChildHistoryEntity type.
	 * Entityを複製し基底クラスで返却する。
	 */
	public abstract ParentChildHistoryEntity copySuperEntity();

}