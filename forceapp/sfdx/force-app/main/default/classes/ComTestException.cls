/*
 * テスト用Exception
 * テスト実行時のみに使用する（使用する前にTest.isRunningTestのチェックが必要）
 */
public with sharing class ComTestException extends Exception {

}