/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署エンティティを表すクラスのテスト
 */
@isTest
private class DepartmentBaseEntityTest {

	/**
	 * テストデータ
	 */
	private class EntityTestData extends TestData.TestDataEntity {}

	/**
	 * エンティティの複製ができることを確認する
	 */
	@isTest static void copyTest() {
		EntityTestData testData = new EntityTestData();

		DepartmentBaseEntity copyEntity = testData.department.copy();
		System.assertEquals(testData.department.name, copyEntity.name);
		System.assertEquals(testData.department.code, copyEntity.code);
		System.assertEquals(testData.department.uniqKey, copyEntity.uniqKey);
		System.assertEquals(testData.department.companyId, copyEntity.companyId);
	}
}
