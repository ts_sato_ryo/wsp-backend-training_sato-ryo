/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通 Common
 *
 * @description 新規コストセンター登録バッチ処理 New Cost Center Create Batch Processing
 */
public with sharing class CostCenterInsertBatch implements Database.Batchable<sObject>, Database.Stateful {

	/** インポートバッチID */
	private Id importBatchId;

	/** 既存の会社データ */
	@TestVisible
	private Map<String, CompanyEntity> companyMap;
	/** 既存のコストセンターデータ */
	@TestVisible
	// キーは会社コード・コストセンターコード Company Code => Cost Center Code Key-Value pair
	private Map<List<String>, CostCenterBaseEntity> costCenterMap;

	/** 成功件数 */
	private Integer successCount = 0;
	/** 失敗件数 */
	private Integer errorCount = 0;
	/** 例外発生 */
	private Boolean exceptionOccurred = false;


	/**
	 * コンストラクタ Constructor
	 * @param importBatchId インポートバッチID
	 */
	public CostCenterInsertBatch(Id importBatchId) {
		this.importBatchId = importBatchId;
	}

	/**
	 * バッチ開始処理 Start batch processing
	 * @param bc バッチコンテキスト Batch Context
	 * @return バッチ処理対象レコード Records to be processed in the batch
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {
		// コストセンターインポートデータを取得 Retrieve Cost Center Import data
		String query = 'SELECT Id, ' +
							'Name, ' +
							'ImportBatchId__c, ' +
							'RevisionDate__c, ' +
							'ValidTo__c, ' +
							'Code__c, ' +
							'CompanyCode__c, ' +
							'Name_L0__c, ' +
							'Name_L1__c, ' +
							'Name_L2__c, ' +
							'HistoryComment__c, ' +
							'LinkageCode__c, ' +
							'Status__c, ' +
							'Error__c ' +
						'FROM ComCostCenterImport__c ' +
						'WHERE ImportBatchId__c = :importBatchId ' +
						'AND Status__c IN (\'Waiting\', \'Error\') ' +
						'ORDER BY Code__c, RevisionDate__c, ValidTo__c, CreatedDate';

		return Database.getQueryLocator(query);
	}

	/**
	 * バッチ実行処理 Execute batch processing
	 * @param bc バッチコンテキスト Batch Context
	 * @param scope バッチ処理対象レコードのリスト List of Cost Center Import records to be created
	 */
	public void execute(Database.BatchableContext bc, List<ComCostCenterImport__c> scope) {
		this.companyMap = new Map<String, CompanyEntity>();
		this.costCenterMap = new Map<List<String>, CostCenterBaseEntity>();

		List<CostCenterImportEntity> costCenterImpList = new List<CostCenterImportEntity>();
		List<CostCenterImportEntity> costCenterImpExcludedList = new List<CostCenterImportEntity>();

		Set<String> companyCodeSet = new Set<String>();
		Set<String> costCenterCodeSet = new Set<String>();

		for (ComCostCenterImport__c obj : scope) {

			CostCenterImportEntity entity = this.createCostCenterImpEntity(obj);

			// 失効日が設定されている場合は有効期間終了日の更新のみを行なうため If ValidTo value is set, only the ValidTo field is updated
			// 本バッチでは処理対象外 The record not processed in this batch function
			if (entity.validTo != null) {
				costCenterImpExcludedList.add(entity);
				continue;
			}
			costCenterImpList.add(entity);

			// 参照先のコードを取得する Retrieve all codes
			if (String.isNotEmpty(entity.code)) {
				costCenterCodeSet.add(entity.code);
			}
			if (String.isNotEmpty(entity.companyCode)) {
				companyCodeSet.add(entity.companyCode);
			}
		}

		// 本バッチで処理対象外のデータのステータスを更新する Update the Status field of records not processed in this batch
		updateStatusOfExcludedImp(costCenterImpExcludedList);

		// 参照先のデータを取得 Retreive reference data from code
		// 会社 Company
		if (companyCodeSet.size() > 0) {
			CompanyRepository companyRepo = new CompanyRepository();
			for (CompanyEntity company : companyRepo.getEntityList(new List<String>(companyCodeSet))) {
				this.companyMap.put(company.code, company);
			}
		}
		// コストセンター Cost Center
		CostCenterService costCenterService = new CostCenterService();
		if (costCenterCodeSet.size() > 0) {
			for (ParentChildBaseEntity costCenter : costCenterService.getEntityListByCode(new List<String>(costCenterCodeSet))) {
				List<String> key = new List<String>{((CostCenterBaseEntity)costCenter).company.code, ((CostCenterBaseEntity)costCenter).code};
				this.costCenterMap.put(key, (CostCenterBaseEntity)costCenter);
			}
		}

		Savepoint sp = Database.setSavepoint();

		// 新規データの登録 Create new records
		for (CostCenterImportEntity costCenterImp : costCenterImpList) {
			try {
				// バリデーション Validation
				this.validateCostCenterImpEntity(costCenterImp);

				// 既存データが存在する or エラーがある場合は何もしない Do not do anything if existing data exists or there is an error
				List<String> costCenterMapKey = new List<String>{costCenterImp.companyCode, costCenterImp.code};
				if (this.costCenterMap.containsKey(costCenterMapKey) || ImportStatus.ERROR.equals(costCenterImp.status)) {
					continue;
				}

				// 登録用のコストセンターエンティティを作成 Create Cost Center Base Entity
				CostCenterBaseEntity costCenterBase = this.createCostCenterEntity(costCenterImp);

				// 登録 Create
				costCenterService.saveNewEntity(costCenterBase);

				costCenterImp.status = ImportStatus.WAITING;
				costCenterImp.error = null;

				// 同じコードのデータが複数件含まれているかもしれないので、Mapにも追加しておく Add to Map as there may be multiple data with the same code
				this.costCenterMap.put(costCenterMapKey, costCenterBase);
			} catch (App.BaseException e) {
				costCenterImp.status = ImportStatus.ERROR;
				costCenterImp.error = e.getMessage();
				this.errorCount++;
			} catch (Exception e) {
				Database.rollback(sp);
				costCenterImp.status = ImportStatus.ERROR;
				costCenterImp.error = e.getMessage();
				this.exceptionOccurred = true;
				break;
			}
		}

		// コストセンターインポートデータを更新 Update Cost Center Import records
		CostCenterImportRepository costCenterImpRepo = new CostCenterImportRepository();
		costCenterImpRepo.saveEntityList(costCenterImpList);
	}

	/**
	 * バッチ終了処理 Finish batch processing
	 * @param bc バッチコンテキスト Batch Context
	 */
	public void finish(Database.BatchableContext bc) {
		// インポートバッチデータを更新 Update ImportBatch records
		ImportBatchRepository batchRepo = new ImportBatchRepository();
		ImportBatchEntity batch = batchRepo.getEntity(this.importBatchId);
		batch.status = this.exceptionOccurred ? ImportBatchStatus.FAILED : ImportBatchStatus.PROCESSING;
		batch.successCount += this.successCount;
		batch.failureCount += this.errorCount;
		batchRepo.saveEntity(batch);

		if (!this.exceptionOccurred) {
			if (!Test.isRunningTest()) {
				// 次の処理を呼び出し（既存コストセンター更新） Call the next batch process (CostCenterUpdateBatch)
				CostCenterUpdateBatch costCenterUpdate = new CostCenterUpdateBatch(importBatchId);
				Database.executeBatch(costCenterUpdate, ImportBatchService.BATCH_SIZE_COST_CENTER_UPDATE);
			}
		}
	}

	//--------------------------------------------------------------------------------

	/**
	 * コストセンターインポートエンティティを作成する Create CostCenterImportEntity
	 * @param obj コストセンターインポートオブジェクトデータ Cost Center Import object record
	 * @return コストセンターインポートエンティティ CostCenterImportEntity
	 */
	private CostCenterImportEntity createCostCenterImpEntity(ComCostCenterImport__c obj) {
		CostCenterImportEntity entity = new CostCenterImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.validTo = AppDate.valueOf(obj.ValidTo__c);
		entity.code = ImportBatchService.convertValue(obj.Code__c);
		entity.companyCode = ImportBatchService.convertValue(obj.CompanyCode__c);
		entity.name_L0 = ImportBatchService.convertValue(obj.Name_L0__c);
		entity.name_L1 = ImportBatchService.convertValue(obj.Name_L1__c);
		entity.name_L2 = ImportBatchService.convertValue(obj.Name_L2__c);
		entity.historyComment = ImportBatchService.convertValue(obj.HistoryComment__c);
		entity.linkageCode = ImportBatchService.convertValue(obj.LinkageCode__c);

		entity.resetChanged();
		entity.status = ImportStatus.WAITING;
		entity.error = null;

		return entity;
	}

	/**
	 * バリデーションを行う Validates CostCenterImportEntity
	 * @param costCenterImp コストセンターインポートエンティティ CostCenterImportEntity
	 */
	@TestVisible
	private void validateCostCenterImpEntity(CostCenterImportEntity costCenterImp) {
		List<String> costCenterMapKey = new List<String>{costCenterImp.companyCode, costCenterImp.code};

		// 改訂日が未設定 Checks if Revision Date value is set
		if (costCenterImp.revisionDate == null) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_RevisionDate});
			this.errorCount++;
			return;
		}

		// コストセンターコードが未設定 Checks if Cost Center Code value is set
		if (String.isBlank(costCenterImp.code)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CostCenterCode});
			this.errorCount++;
			return;
		}

		// 会社コードが未設定 Checks if Company Code value is set
		if (String.isBlank(costCenterImp.companyCode)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			this.errorCount++;
			return;
		}
		// 指定した会社が存在しない Checks if Company exists
		if (!this.companyMap.containsKey(costCenterImp.companyCode)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			this.errorCount++;
			return;
		}

		// コストセンター名(L0)が未設定（新規の場合のみ） Checks if Cost Center Name(L0) is set (only for insertion)
		if (!this.costCenterMap.containsKey(costCenterMapKey) && String.isBlank(costCenterImp.name_L0)) {
			costCenterImp.status = ImportStatus.ERROR;
			costCenterImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CostCenterName});
			this.errorCount++;
			return;
		}
	}

	/**
	 * コストセンターエンティティを作成する（ベース＋履歴） Creates Cost Center Entity (Base and History)
	 * @param costCenterImp コストセンターインポートエンティティ CostCenterImportEntity
	 * @return コストセンターベースエンティティ CostCenterBaseEntity
	 */
	private CostCenterBaseEntity createCostCenterEntity(CostCenterImportEntity costCenterImp) {
		// ベース Base
		CostCenterBaseEntity base = new CostCenterBaseEntity();
		base.name = costCenterImp.name_L0;
		base.code = costCenterImp.code;
		base.uniqKey = base.createUniqKey(costCenterImp.companyCode, base.code);
		base.companyId = this.companyMap.get(costCenterImp.companyCode).id;

		// 履歴（相互参照項目は登録しない） History (Do NOT set reference fields here)
		CostCenterHistoryEntity history = new CostCenterHistoryEntity();
		history.name = costCenterImp.name_L0;
		history.nameL0 = costCenterImp.name_L0;
		history.nameL1 = costCenterImp.name_L1;
		history.nameL2 = costCenterImp.name_L2;
		history.historyComment = costCenterImp.historyComment;
		history.linkageCode = costCenterImp.linkageCode;
		history.validFrom = costCenterImp.revisionDate;
		history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;

		// 履歴をベースに追加 Add History record to Base record
		base.addHistory(history);

		return base;
	}

	/**
	 * 本バッチで処理対象外データのステータスを更新する Updates Status field of records excluded from this batch
	 */
	private void updateStatusOfExcludedImp(List<CostCenterImportEntity> costCenterImpExcludedList) {

		// ステータスを設定 Set Status value
		for (CostCenterImportEntity costCenterImp : costCenterImpExcludedList) {
			costCenterImp.status = ImportStatus.WAITING;
			costCenterImp.error = null;
		}

		// コストセンターインポートデータを更新 Update Cost Center Import records
		CostCenterImportRepository costCenterImpRepo = new CostCenterImportRepository();
		costCenterImpRepo.saveEntityList(costCenterImpExcludedList);
	}
}