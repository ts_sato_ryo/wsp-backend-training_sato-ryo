/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * 経費領収書添付 Expense File Attachment Picklist
 */
public with sharing class ExpFileAttachment extends ValueObjectType {

	/** 必須 Required */
	public static final ExpFileAttachment REQUIRED = new ExpFileAttachment('Required');
	/** 任意 Optional */
	public static final ExpFileAttachment OPTIONAL = new ExpFileAttachment('Optional');
	/** 不要 Not Used */
	public static final ExpFileAttachment NOT_USED = new ExpFileAttachment('NotUsed');

	/** Map of Types with Name as the Key and the ExpFileAttachment as the value */
	private static final Map<String, ExpFileAttachment> TYPE_MAP;
	static {
		final Map<String, ExpFileAttachment> typeMap = new Map<String, ExpFileAttachment>();
		typeMap.put('Required', ExpFileAttachment.REQUIRED);
		typeMap.put('Optional', ExpFileAttachment.OPTIONAL);
		typeMap.put('NotUsed', ExpFileAttachment.NOT_USED);

		TYPE_MAP = typeMap;
	}

	/**
	 * コンストラクタ（値のみ） Constructor
	 * @param value 値
	 */
	private ExpFileAttachment(String value) {
		super(value);
	}

	/**
	 * 値からExpFileAttachmentを取得する Return the value whose key matches with specified string
	 * @param value 取得対象のインスタンスが持つ値 The key value to get the instance
	 * @return パラメータで指定された値を持つExpFileAttachment、ただし該当するExpFileAttachmentが存在しない場合はnull The instance having the value specified
	 */
	public static ExpFileAttachment valueOf(String value) {
		return TYPE_MAP.get(value);
	}

	/**
	 * 値が等しいかどうかを判定する Return whether the object is same
	 * @param compare 比較対象のオブジェクト Target object to compare with
	 * @retun 等しい場合はtrue、そうでない場合はfalse If the objects are same, return true. otherwise return false
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse if null, return false
		if (compare == null) {
			return false;
		}

		if (compare instanceof ExpFileAttachment && this.value == ((ExpFileAttachment)compare).value) {
			// 値が同じであればtrue if value is the same, return true
			return true;
		} 

		return false;
	}
}
