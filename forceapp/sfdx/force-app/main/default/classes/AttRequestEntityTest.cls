/**
 * @group 勤怠
 *
 * 勤怠月次申請のエンティティのテスト
 */
@isTest
private class AttRequestEntityTest {

	/**
	 * 勤務確定申請詳細ステータスのテスト
	 */
	@isTest private static void getDetailStatusTest() {
		AttRequestEntity entity = new AttRequestEntity();
		Id testRequestId = ComTestDataUtility.getDummyId(entity.createSObject().getSObjectType());

		// 未申請
		// 申請IDが設定されていない
		System.assertEquals(AttRequestEntity.DetailStatus.NOT_REQUESTED, entity.getDetailStatus());

		// ステータスが設定されていない
		entity.setId(testRequestId);
		entity.status = null;
		entity.cancelType = null;
		System.assertEquals(AttRequestEntity.DetailStatus.NOT_REQUESTED, entity.getDetailStatus());

		// 承認待ち
		entity.setId(testRequestId);
		entity.status = AppRequestStatus.PENDING;
		entity.cancelType = null;
		System.assertEquals(AttRequestEntity.DetailStatus.PENDING, entity.getDetailStatus());

		// 承認済み
		entity.setId(testRequestId);
		entity.status = AppRequestStatus.APPROVED;
		entity.cancelType = null;
		System.assertEquals(AttRequestEntity.DetailStatus.APPROVED, entity.getDetailStatus());

		// 却下
		entity.setId(testRequestId);
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.REJECTED;
		System.assertEquals(AttRequestEntity.DetailStatus.REJECTED, entity.getDetailStatus());

		// 申請取消
		entity.setId(testRequestId);
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.REMOVED;
		System.assertEquals(AttRequestEntity.DetailStatus.REMOVED, entity.getDetailStatus());

		// 承認取消
		entity.setId(testRequestId);
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.CANCELED;
		System.assertEquals(AttRequestEntity.DetailStatus.CANCELED, entity.getDetailStatus());

		// ステータスが無効なのに取消種別がなしの場合はエラー
		entity.setId(testRequestId);
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.NONE;
		try {
			entity.getDetailStatus();
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			// OK
			System.debug(e.getMessage());
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
	}

}
