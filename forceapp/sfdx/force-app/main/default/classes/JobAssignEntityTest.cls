/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description JobAssignEntityのテスト
 */
@isTest
private class JobAssignEntityTest {

	/**
	 * 参照項目を正しく取得出来ることを検証する
	 */
	@isTest
	private static void referencePropertyTest() {
		ComJobAssign__c sobj = new ComJobAssign__c();
		sobj.employeeBaseId__r = new ComEmpBase__c();
		sobj.employeeBaseId__r.code__c = 'code';

		JobAssignEntity entity = new JobAssignEntity(sobj);
		System.assertEquals('code', entity.employeeCode);
	}
}