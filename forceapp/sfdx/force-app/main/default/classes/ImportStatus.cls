/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * インポート処理ステータス
 */
public with sharing class ImportStatus extends ValueObjectType {

	/** 未処理 */
	public static final ImportStatus WAITING = new ImportStatus('Waiting');
	/** 実行中 */
	public static final ImportStatus PROCESSING = new ImportStatus('Processing');
	/** 正常終了 */
	public static final ImportStatus SUCCESS = new ImportStatus('Success');
	/** エラー終了 */
	public static final ImportStatus ERROR = new ImportStatus('Error');

	/** インポート処理ステータスのリスト */
	public static final List<ImportStatus> STATUS_LIST;
	static {
		STATUS_LIST = new List<ImportStatus> {
			WAITING,
			PROCESSING,
			SUCCESS,
			ERROR
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ImportStatus(String value) {
		super(value);
	}

	/**
	 * 値からImportStatusを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つImportStatus、ただし該当するImportStatusが存在しない場合はnull
	 */
	public static ImportStatus valueOf(String value) {
		ImportStatus retType = null;
		if (String.isNotBlank(value)) {
			for (ImportStatus type : STATUS_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ImportStatus以外の場合はfalse
		Boolean eq;
		if (compare instanceof ImportStatus) {
			// 値が同じであればtrue
			if (this.value == ((ImportStatus)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}