/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group App
 *
 * @description Class for manages resource usage information
 * (wrapping the Apex limit class)
 */
public with sharing class AppLimits {

	/**
	 * Current count of queries
	 */
	private Integer currentQueryCount;
	/**
	 * Current CPU time
	 */
	private Integer currentCpuTime;
	/**
	 * CPU time last measured
	 */
	private Integer lastCpuTime;
	/**
	 * Processing time
	 */
	private DurationTime processingTime;
	/**
	 * Duration time class
	 */
	private class DurationTime {
		/** Start time 開始時刻 */
		public Datetime startTime;
		/** Via time 開始から終了までの特定の時間 */
		public Datetime viaTime;
		/** End time 終了時刻 */
		public Datetime endTime;

		/**
		 * Return duration time(ms) (Start Time to End Time)
		 */
		public Integer getDurationTime() {
			if (startTime == null || endTime == null) {
				return null;
			}
			return (Integer)(endTime.getTime() - startTime.getTime());
		}
		/**
		 * Return duration time(ms) (Via Time to End Time )
		 */
		public Integer getDurationTimeFromViaTime() {
			if (viaTime == null || endTime == null) {
				return null;
			}
			return (Integer)(endTime.getTime() - viaTime.getTime());
		}
	}

	// Singleton
	private AppLimits() {
		queryCountMap = new Map<String, Integer>();
		setCurrentQueryCount();
		processingTime = new DurationTime();
	}
	private static final AppLimits INSTANCE = new AppLimits();
	public static AppLimits getInstance() {
		return INSTANCE;
	}

	/**
	 * Start measuring the processing time
	 * 処理時間の計測を開始する
	 * 再計測したい場合も本メソッドを実行してください。
	 */
	public void startMeasuringProcessingTime() {
		processingTime.startTime = Datetime.now();
		processingTime.endTime = processingTime.startTime;
	}

	/**
	 * @description Map of target and count of queries
	 */
	@TestVisible
	private Map<String, Integer> queryCountMap;

	/**
	 * @description Set the current total count of queries
	 */
	public void setCurrentQueryCount() {
		currentQueryCount = Limits.getQueries();
	}

	/**
	 * @description increment the count of queries for the target object
	 * @param sobj
	 */
	public void incrementQueryCount(Schema.sObjectType type) {
		Integer incrementCount = Limits.getQueries() - currentQueryCount;
		String targetName = type.getDescribe().getName();
		if (queryCountMap.get(targetName) != null) {
			queryCountMap.put(targetName, queryCountMap.get(targetName) + incrementCount);
		} else {
			queryCountMap.put(targetName, incrementCount);
		}
	}

	/**
	 * @description Return resource usage information as a string
	 * @return resource usage information
	 */
	public override String toString() {
		// Record the current count of queries
		currentQueryCount = Limits.getQueries();
		// Record current CPU time
		currentCpuTime = Limits.getCpuTime();
		// Record current time
		processingTime.viaTime = processingTime.endTime;
		processingTime.endTime = Datetime.now();

		String limitsInfo = 'APP_LIMITS - START\n';
		// Count of queries for each object
		limitsInfo += 'APP_LIMITS - QUERY_COUNT_EACH --- ' + getQueryCountEach() + '\n';
		// Total count of queries
		limitsInfo += 'APP_LIMITS - QUERY_COUNT_TOTAL --- ' + currentQueryCount + '\n';
		// CPU time difference
		limitsInfo += 'APP_LIMITS - CPU_TIME_INTERVAL --- ' + getCpuTimeInterval() + '\n';
		// CPU time total
		limitsInfo += 'APP_LIMITS - CPU_TIME_TOTAL --- ' + currentCpuTime + '\n';
		// DML statement total
		limitsInfo += 'APP_LIMITS - DML_STATEMENT_TOTAL --- ' + Limits.getDMLStatements() + '\n';
		// Processing Time
		Integer durationTime = processingTime.getDurationTime();
		Integer durationTimeBeforeLogging = processingTime.getDurationTimeFromViaTime();
		limitsInfo += 'APP_LIMITS - PROCESSING_TIME --- ';
		limitsInfo += durationTime == null ? 'Not measured\n' : durationTime + ' (+' + durationTimeBeforeLogging + ')\n';
		limitsInfo += 'APP_LIMITS - END';

		return limitsInfo;
	}

	/**
	 * @description Return the count of queries for each object
	 * @return String containing the count of queries for each object
	 *         (ex.)AttWorkingTypeHistory : 4, AttWorkingTypeBase : 4, ComEmpHistory : 7, ComEmpBase : 7...
	 */
	private String getQueryCountEach() {
		List<String> queryCountList = new List<String>();
		for (String name : queryCountMap.keySet()) {
			Integer count = queryCountMap.get(name);
			queryCountList.add(name + ' : ' + count);
		}
		return String.join(queryCountList, ', ');
	}

	/**
	 * @description Return the difference between the last measured CPU time and the current CPU time
	 * @return CPU time difference
	 */
	private Integer getCpuTimeInterval() {
		Integer intervalTime;
		if (lastCpuTime == null) {
			intervalTime = currentCpuTime;
		} else {
			intervalTime = currentCpuTime - lastCpuTime;
		}
		// Update CPU time last measured
		lastCpuTime = currentCpuTime;
		return intervalTime;
	}
}