/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttWorkSystemのテスト
 */
@isTest
private class AttWorkSystemTest {


	/**
	 * valueOfのテスト
	 * 指定した値のAttWorkSystemが取得できることを確認する
	 */
	@isTest static void valueOfTest() {

		// 存在する場合
		System.assertEquals(AttWorkSystem.JP_FIX, AttWorkSystem.valueOf('JP:Fix'));

		// 存在しない場合はnull
		System.assertEquals(null, AttWorkSystem.valueOf('Test'));
	}

	/**
	 * equalsのテスト
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(AttWorkSystem.JP_FIX, AttWorkSystem.JP_FIX);
		System.assertEquals(AttWorkSystem.JP_FLEX, AttWorkSystem.JP_FLEX);
		System.assertEquals(AttWorkSystem.JP_MODIFIED, AttWorkSystem.JP_MODIFIED);
		System.assertEquals(AttWorkSystem.JP_MANAGER, AttWorkSystem.JP_MANAGER);

		// 等しくない場合(値が異なる) → false
		System.assertEquals(false, AttWorkSystem.JP_FIX.equals(AttWorkSystem.JP_MANAGER));

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, AttWorkSystem.JP_FIX.equals(AttDayType.WORKDAY));

		// 等しくない場合(null)
		System.assertEquals(false, AttWorkSystem.JP_FIX.equals(null));
	}
}
