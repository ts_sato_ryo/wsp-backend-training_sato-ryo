/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 短時間勤務設定ベースエンティティ
 */
public with sharing class AttShortTimeSettingBaseEntity extends AttShortTimeBaseGeneratedEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;

	/** 短時間勤務設定で利用可能な労働時間制 */
	public static final Set<AttWorkSystem> AVAILABLE_WORK_SYSTEM_SET;
	static {
		AVAILABLE_WORK_SYSTEM_SET = new Set<AttWorkSystem> {
			AttWorkSystem.JP_FIX,
			AttWorkSystem.JP_FLEX,
			AttWorkSystem.JP_MODIFIED
		};
	}

	/** 履歴エンティティのリスト */
	@testVisible
	protected List<AttShortTimeSettingHistoryEntity> historyList;

	/**
	 * デフォルトコンストラクタ
	 */
	public AttShortTimeSettingBaseEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public AttShortTimeSettingBaseEntity(AttShortTimeWorkSettingBase__c sobj) {
		super(sobj);
	}

	/** 理由(参照のみ) */
	public LookupField reason {
		get {
			if (reason == null) {
				reason = new LookupField(new AppMultiString(
						sobj.ReasonId__r.Name_L0__c,
						sobj.ReasonId__r.Name_L1__c,
						sobj.ReasonId__r.Name_L2__c));
			}
			return reason;
		}
		private set;
	}

	/**
	 * 履歴エンティティリストから指定したIDの履歴を取得する
	 * @return 履歴エンティティ、ただし存在しない場合はnull
	 */
	public AttShortTimeSettingHistoryEntity getHistoryById(Id historyId) {
		return (AttShortTimeSettingHistoryEntity)super.getSuperHistoryById(historyId);
	}
	/**
	 * 履歴エンティティリストから指定した日付を含む履歴を取得する
	 * @return 履歴エンティティ
	 */
	public AttShortTimeSettingHistoryEntity getHistoryByDate(AppDate targetDate) {
		return (AttShortTimeSettingHistoryEntity)super.getSuperHistoryByDate(targetDate);
	}
	/**
	 * 履歴エンティティリストを取得する
	 * @return 履歴エンティティリスト
	 */
	public List<AttShortTimeSettingHistoryEntity> getHistoryList() {
		return historyList;
	}

	/**
	 * 履歴エンティティをリストに追加する
	 * @param history 追加する履歴エンティティ
	 */
	 public void addHistory(AttShortTimeSettingHistoryEntity history) {
		 if (historyList == null) {
			historyList = new List<AttShortTimeSettingHistoryEntity>();
		 }
		 historyList.add(history);
	 }

	 /**
	  * 部署履歴リストを取得する
	  * @return 部署名。履歴が存在しない場合はnull
	  */
	 public AttShortTimeSettingHistoryEntity getHistory(Integer index) {
		 if (historyList == null) {
			 return null;
		 }
		 return historyList[index];
	 }

	 /**
	  * 履歴の基底クラスに変換する
	  */
	 protected override List<ParentChildHistoryEntity> convertSuperHistoryList() {
		 return (List<ParentChildHistoryEntity>)historyList;
	 }

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public AttShortTimeSettingBaseEntity copy() {
		AttShortTimeSettingBaseEntity copyEntity = new AttShortTimeSettingBaseEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildBaseEntity copySuperEntity() {
		return copy();
	}
}
