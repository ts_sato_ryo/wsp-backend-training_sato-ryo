/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 取消種別のテスト
 */
@isTest
private class AppCancelTypeTest {

	/**
	 * 指定した値のAppCancelTypeが正しく取得できることを確認する
	 */
	@isTest static void valueOfTest() {

		String value = 'Rejected';
		System.assertEquals(value, AppCancelType.valueOf('Rejected').value);
		value = 'Removed';
		System.assertEquals(value, AppCancelType.valueOf('Removed').value);
		value = 'Canceled';
		System.assertEquals(value, AppCancelType.valueOf('Canceled').value);
		value = 'Reapplied';
		System.assertEquals(value, AppCancelType.valueOf('Reapplied').value);
	}

	/**
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(AppCancelType.REJECTED, AppCancelType.valueOf('Rejected'));
		System.assertEquals(AppCancelType.REMOVED, AppCancelType.valueOf('Removed'));
		System.assertEquals(AppCancelType.CANCELED, AppCancelType.valueOf('Canceled'));
		System.assertEquals(AppCancelType.REAPPLIED, AppCancelType.valueOf('Reapplied'));

		// 等しくない場合(値が異なる) → false
		System.assertEquals(false, AppCancelType.REJECTED.equals(AppCancelType.REMOVED));

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, AppCancelType.REJECTED.equals(AttDayType.WORKDAY));

		// 等しくない場合(null)
		System.assertEquals(false, AppCancelType.REJECTED.equals(null));
	}
}