/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Class for Testing the CostCenter Service layer implementation.
 */
@IsTest
private class CostCenterServiceTest {
	private class TestData {

		public ComCompany__c companyObj {get; private set;}

		ComCostCenterBase__c parentCostCenterObj;

		public TestData() {
			companyObj = ComTestDataUtility.createTestCompany();
			parentCostCenterObj = ComTestDataUtility.createCostCentersWithHistory('Root', companyObj.Id, 1, 1)[0];
		}

		public List<CostCenterBaseEntity> createCostCenterBaseEntityList(String name, Integer baseSize, Integer historySize) {
			List<ComCostCenterBase__c> costCenterList = createCostCentersWithHistory(name, baseSize, historySize);
			List<Id> baseIdList = new List<Id>();
			for (ComCostCenterBase__c costCenter : costCenterList) {
				baseIdList.add(costCenter.Id);
			}

			return (new CostCenterRepository()).getEntityList(baseIdList, null);
		}

		public List<ComCostCenterBase__c> createCostCentersWithHistory(String name, Integer baseSize, Integer historySize) {
			List<ComCostCenterBase__c> costCenterList =  ComTestDataUtility.createCostCentersWithHistory(
				name, this.companyObj.Id, baseSize, historySize);

			// Set parent cost center in history
			List<ComCostCenterHistory__c> historyList = [SELECT Id, ParentBaseId__c FROM ComCostCenterHistory__c];
			for (ComCostCenterHistory__c history : historyList) {
				history.ParentBaseId__c = this.parentCostCenterObj.Id;
			}
			update historyList;
			return costCenterList;
		}
	}

	/**
	 * Test the behavior when the code is already in used for the same company, and for different company.
	 */
	@isTest static void isCodeDuplicatedTest() {
		CostCenterServiceTest.TestData testData = new TestData();

		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<CostCenterBaseEntity> bases = testData.createCostCenterBaseEntityList('Test', baseNum, historyNum);
		CostCenterBaseEntity targetBase = bases[1];

		CostCenterService service = new CostCenterService();

		// check that isCodeDuplicated method return true
		targetBase.code = bases[0].code;
		System.assertEquals(true, service.isCodeDuplicated(targetBase));

		// Code Duplicate should be false since it's per company
		targetBase.companyId = ComTestDataUtility.createCompany('Test2', ComTestDataUtility.createCountry('TS').id).Id;
		targetBase.code = bases[0].code;
		System.assertEquals(false, service.isCodeDuplicated(targetBase));

		targetBase.code = targetBase.code + 'Test';
		System.assertEquals(false, service.isCodeDuplicated(targetBase));
	}

	/*
	 * Test searching the Cost Center History by using Base Id and Target Date.
	 */
	@IsTest static void searchCostCenterHistoryByBaseIdAndTargetDatePositiveTest() {
		ComCompany__c companyObj = ComTestDataUtility.createTestCompany();
		//Shift the start date by 14 months from current date - so it becomes old, current, and future histories.
		List<ComCostCenterBase__c> costCenterBaseList = ComTestDataUtility.createCostCentersWithHistory('Root', companyObj.Id, 2, 3, false, -14);

		CostCenterService service = new CostCenterService();
		AppDate today = AppDate.today();
		List<CostCenterHistoryEntity> resultHistory = new List<CostCenterHistoryEntity>();
		Test.startTest();
		// Try retrieving the cost center by different Target Date.
		resultHistory.add(service.searchCostCenterHistory(costCenterBaseList[0].Id, today.addMonths(-12)));
		resultHistory.add(service.searchCostCenterHistory(costCenterBaseList[0].Id, today));
		resultHistory.add(service.searchCostCenterHistory(costCenterBaseList[0].Id, today.addMonths(12)));
		Test.stopTest();
		System.assertEquals(3, resultHistory.size());
		List<ComCostCenterHistory__c> retrievedHistoryList = [
			SELECT Id, BaseId__c
			FROM ComCostCenterHistory__c
			WHERE BaseId__c = :costCenterBaseList[0].Id
		];
		For (Integer i = 0; i < resultHistory.size(); i++) {
			System.assertEquals(retrievedHistoryList[i].Id, resultHistory[i].id);
		}
	}

	/*
	 * Test searching the Cost Center History with history Id List
	 */
	@IsTest static void searchCostCenterHistoryListWithHistoryIdListTest() {
		CostCenterService service = new CostCenterService();
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 12;
		Integer numberOfHistoryRecordPerBaseRecord = 2;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true, -1);
		List<Id> costCenterBaseIdList = new List<Id>();
		for (ComCostCenterBase__c base : costCenters) {
			costCenterBaseIdList.add(base.Id);
		}
		AppDate targetDate = AppDate.today();
		List<CostCenterBaseEntity> costCenterBaseEntityList = new CostCenterRepository().getEntityList(costCenterBaseIdList, targetDate);
		List<Id> historyIdList = new List<Id> {costCenterBaseEntityList[0].getHistory(0).id, costCenterBaseEntityList[5].getHistory(0).id};
		List<CostCenterHistoryEntity> costCenterHistoryEntityList = service.searchCCHistoryListWithHistoryIdList(historyIdList, targetDate);

		System.assertEquals(2, costCenterHistoryEntityList.size());
		assertCCHistory(costCenterBaseEntityList[0].getHistory(0), costCenterHistoryEntityList[0]);
		assertCCHistory(costCenterBaseEntityList[5].getHistory(0), costCenterHistoryEntityList[1]);
	}

	/**
	 * Test Cost Center Hierarchy Parent Name is retrieved correctly
	 */
	@isTest static void getCostCenterHierarchyParentName() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 14;
		Integer numberOfHistoryRecordPerBaseRecord = 1;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true, -1);
		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[10].Id];
		history.ParentBaseId__c = costCenters[9].Id;
		update history;

		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[8].Id];
		history.ParentBaseId__c = costCenters[9].Id;
		update history;

		// CCB8 --> CCB11
		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[7].Id];
		history.ParentBaseId__c = costCenters[10].Id;
		update history;

		// CCB7 --> CCB8
		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[6].Id];
		history.ParentBaseId__c = costCenters[7].Id;
		update history;
		// CCB1 --> CCV7
		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[0].Id];
		history.ParentBaseId__c = costCenters[6].Id;
		update history;

		// CCB3 --> CCB1
		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[2].Id];
		history.ParentBaseId__c = costCenters[0].Id;
		update history;

		// 1lvl relationship CCB2 --> CCB14
		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[1].Id];
		history.ParentBaseId__c = costCenters[13].Id;
		update history;

		// Last 3 relationship CCB5 --> CCB6 --> CCB12
		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[4].Id];
		history.ParentBaseId__c = costCenters[5].Id;
		update history;

		history = [SELECT Id FROM ComCostCenterHistory__c
		WHERE BaseId__c =: costCenters[5].Id];
		history.ParentBaseId__c = costCenters[11].Id;
		update history;

		AppDate targetDate = AppDate.today();

		// Relationship CCB3 --> CCB1 --> CCB7 --> CCB8 --> CCB11 ---> CCB10 (Find CCB3)
		CostCenterBaseEntity costCenterBaseEntity = new CostCenterRepository().getEntity(costCenters[2].Id);

		Map<Id, List<String>> keyMap =
				new CostCenterService().getParentHierarchy(new List<CostCenterBaseEntity>{costCenterBaseEntity}, targetDate);

		System.assertEquals(true, keyMap.containsKey(costCenters[2].Id));
		System.assertEquals(5, keyMap.get(costCenters[2].Id).size());

		System.assertEquals('Test Search1 L0', keyMap.get(costCenters[2].Id)[0]);
		System.assertEquals('Test Search7 L0', keyMap.get(costCenters[2].Id)[1]);
		System.assertEquals('Test Search8 L0', keyMap.get(costCenters[2].Id)[2]);
		System.assertEquals('Test Search11 L0', keyMap.get(costCenters[2].Id)[3]);
		System.assertEquals('Test Search10 L0', keyMap.get(costCenters[2].Id)[4]);

		// 1lvl relationship CCB2 --> CCB14 (Find CCB2)
		costCenterBaseEntity = new CostCenterRepository().getEntity(costCenters[1].Id);

		keyMap = new CostCenterService().getParentHierarchy(new List<CostCenterBaseEntity>{costCenterBaseEntity}, targetDate);

		System.assertEquals(true, keyMap.containsKey(costCenters[1].Id));
		System.assertEquals(1, keyMap.get(costCenters[1].Id).size());

		System.assertEquals('Test Search14 L0', keyMap.get(costCenters[1].Id)[0]);

		//Last 3 Relationship CCB5 --> CCB6 --> CCB12 (Find CCB5)
		costCenterBaseEntity = new CostCenterRepository().getEntity(costCenters[4].Id);

		keyMap = new CostCenterService().getParentHierarchy(new List<CostCenterBaseEntity>{costCenterBaseEntity}, targetDate);

		System.assertEquals(true, keyMap.containsKey(costCenters[4].Id));
		System.assertEquals(2, keyMap.get(costCenters[4].Id).size());

		System.assertEquals('Test Search6 L0', keyMap.get(costCenters[4].Id)[0]);
		System.assertEquals('Test Search12 L0', keyMap.get(costCenters[4].Id)[1]);

		// Center level search CCB7 --> CCB8 --> CCB11 ---> CCB10 (Find CCB7)
		costCenterBaseEntity = new CostCenterRepository().getEntity(costCenters[6].Id);

		keyMap = new CostCenterService().getParentHierarchy(new List<CostCenterBaseEntity>{costCenterBaseEntity}, targetDate);

		System.assertEquals(true, keyMap.containsKey(costCenters[6].Id));
		System.assertEquals(3, keyMap.get(costCenters[6].Id).size());

		System.assertEquals('Test Search8 L0', keyMap.get(costCenters[6].Id)[0]);
		System.assertEquals('Test Search11 L0', keyMap.get(costCenters[6].Id)[1]);
		System.assertEquals('Test Search10 L0', keyMap.get(costCenters[6].Id)[2]);
	}

	/**
	 * Test Recently Used Cost Center is retrieved correctly
	 */
	@isTest static void getRecentlyUsedCostCenterBaseListTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 12;
		Integer numberOfHistoryRecordPerBaseRecord = 1;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true, -1);
		ComEmpBase__c employee = ComTestDataUtility.createEmployeeWithHistory('testEmp', company.Id, null, UserInfo.getUserId());

		List<Id> costCenterBaseIdList = new List<Id>();
		for (ComCostCenterBase__c base : costCenters) {
			costCenterBaseIdList.add(base.Id);
		}
		AppDate targetDate = AppDate.today();
		List<CostCenterBaseEntity> costCenterBaseEntityList = new CostCenterRepository().getEntityList(costCenterBaseIdList, targetDate);

		ExpRecentlyUsedService expRecUsedService = new ExpRecentlyUsedService();
		// Add to recently used CostCenter
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, costCenterBaseEntityList[3].getHistory(0).id, costCenterBaseEntityList[0].getHistory(0).id);

		test.startTest();
		List<CostCenterBaseEntity> resultCCBaseEntityList = new CostCenterService().getRecentlyUsedCostCenterBaseList(employee.id, null);
		test.stopTest();

		System.assertEquals(1, resultCCBaseEntityList.size());
		System.assertEquals(false, resultCCBaseEntityList.isEmpty());
		assertCCBase(costCenterBaseEntityList[0],resultCCBaseEntityList[0]);
		assertCCHistory(costCenterBaseEntityList[0].getHistory(0),resultCCBaseEntityList[0].getHistory(0));

	}

	private static void assertCCBase(CostCenterBaseEntity expectedBaseEntity, CostCenterBaseEntity resultBaseEntity){
		System.assertEquals(expectedBaseEntity.code, resultBaseEntity.code);
		System.assertEquals(expectedBaseEntity.id, resultBaseEntity.id);
		System.assertEquals(expectedBaseEntity.companyId, resultBaseEntity.companyId);
	}

	private static void assertCCHistory(CostCenterHistoryEntity expectedHistoryEntity, CostCenterHistoryEntity resultHistoryEntity){
		System.assertEquals(expectedHistoryEntity.id, resultHistoryEntity.id);
		System.assertEquals(expectedHistoryEntity.nameL.getValue(), resultHistoryEntity.nameL.getValue());
		System.assertEquals(expectedHistoryEntity.nameL.valueL0, resultHistoryEntity.nameL.valueL0);
		System.assertEquals(expectedHistoryEntity.nameL.valueL1, resultHistoryEntity.nameL.valueL1);
		System.assertEquals(expectedHistoryEntity.nameL.valueL2, resultHistoryEntity.nameL.valueL2);
		System.assertEquals(expectedHistoryEntity.linkageCode, resultHistoryEntity.linkageCode);
		System.assertEquals(expectedHistoryEntity.validFrom.format(), resultHistoryEntity.validFrom.format());
		System.assertEquals(expectedHistoryEntity.validTo.format(), resultHistoryEntity.validTo.format());
		System.assertEquals(expectedHistoryEntity.parentBaseId, resultHistoryEntity.parentBaseId);
	}
}