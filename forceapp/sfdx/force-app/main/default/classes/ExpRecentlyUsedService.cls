/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Service class for saving and retrieving recently used
 *
 * @group Expense
 */
public with sharing class ExpRecentlyUsedService {

	@TestVisible
	private static final Integer MAX_EXTENDED_ITEM_LOOKUP_RECENTLY_USED = 10;
	@TestVisible
	private static final Integer MAX_COST_CENTER_RECENTLY_USED_COUNT = 10;
	@TestVisible
	private static final Integer MAX_JOB_RECENTLY_USED_COUNT = 10;
	@TestVisible
	private static final Integer MAX_EXPENSE_TYPE_RECENTLY_USED_COUNT = 10;

	/*
	 * Returns the list of 10 most recently used ExtendedItemCustomOptionEntity for the provided
	 * Employee and ExtendedItemLookup. If no recently used, empty list will be returned.
	 * The returned List, index 0 represents the most recently used.
	 *
	 * @param employeeBaseId ID of employee base
	 * @param extendedItemLookupId ID of ExtendedItemLookup
	 * @returns List<ExtendedItemCustomOptionEntity> list of 10 most recently used ExtendedItemCustomOptionEntity
	 */
	public List<ExtendedItemCustomOptionEntity> getRecentlyUsedExtendedItemCustomOptionList(Id employeeBaseId, Id extendedItemLookupId, Id extendedItemCustomId) {
		ExpRecentlyUsedRepository repo = new ExpRecentlyUsedRepository();
		ExpRecentlyUsedRepository.SearchFilter filter = new ExpRecentlyUsedRepository.SearchFilter();
		filter.employeeBaseId = employeeBaseId;
		filter.targetIds = new Set<String>{extendedItemLookupId};
		filter.masterType = ExpRecentlyUsedEntity.MasterType.ExtendedItemLookup;
		List<ExpRecentlyUsedEntity> recentlyUsedEntityList = repo.searchEntity(filter);
		ExpRecentlyUsedEntity recentlyUsedEntity = recentlyUsedEntityList.isEmpty() ? null: recentlyUsedEntityList.get(0);

		List<ExtendedItemCustomOptionEntity> optionEntityList = new List<ExtendedItemCustomOptionEntity>();
		if (recentlyUsedEntity != null) {
			List<String> recentlyUsedCodeList = recentlyUsedEntity.getRecentlyUsedValues();
			List<ExtendedItemCustomOptionEntity> unsortedOptionEntityList = new ExtendedItemCustomService()
					.getExtendedItemCustomOptionList(extendedItemCustomId, new Set<String>(recentlyUsedCodeList));
			Map<String, ExtendedItemCustomOptionEntity> codeOptionMap =
					new Map<String, ExtendedItemCustomOptionEntity>();
			for (ExtendedItemCustomOptionEntity optionEntity : unsortedOptionEntityList) {
				codeOptionMap.put(optionEntity.code, optionEntity);
			}
			for (String code: recentlyUsedCodeList) {
				ExtendedItemCustomOptionEntity option = codeOptionMap.get(code);
				// If an Option has been deleted, it will not be found. Skip those.
				if (option != null) {
					optionEntityList.add(option);
				}
			}
		}
		return optionEntityList;
	}

	/*
	 * Save Recently Used Extended Item Lookup's Option codes values
	 * Maximum of 10 is saved and the least recently used will be discarded when there are more than 10.
	 * The Recently used are save per Employee per ExtendedItem (for example, if there are 2 ExtendedItemLookup which uses
	 * the same ExtendedItemCustom, they will be treated as different extendedItem and both will be saved)
	 *
	 * @param extendedItemIdOptionCodeMap Key: ExtendedItemLookup ID, Value: ExtendedItemCustomOptionCode
	 * @param employeeBaseId ID of employee base
	 */
	public void saveRecentlyUsedExtendedItemLookupValues(Map<Id, Set<String>> extendedItemIdOptionCodeMap, Id employeeBaseId) {
		Map<Id, ExpRecentlyUsedEntity> existingRecentlyUsedMap = this.getExistingRecentlyUsedMap(employeeBaseId, extendedItemIdOptionCodeMap.keySet());

		List<ExpRecentlyUsedEntity> toUpdateRecentlyUsedEntityList = new List<ExpRecentlyUsedEntity>();
		Set<Id> extendedItemIdSet = extendedItemIdOptionCodeMap.keySet();
		for (Id extendedItemId : extendedItemIdSet) {
			ExpRecentlyUsedEntity oldRecentlyUsedEntity = existingRecentlyUsedMap.get(extendedItemId);
			if (oldRecentlyUsedEntity == null) {
				oldRecentlyUsedEntity = new ExpRecentlyUsedEntity();
				oldRecentlyUsedEntity.employeeBaseId = employeeBaseId;
				oldRecentlyUsedEntity.targetId = extendedItemId;
				oldRecentlyUsedEntity.masterType = ExpRecentlyUsedEntity.MasterType.ExtendedItemLookup;
			}
			Set<String> newlyUsedValueSet = extendedItemIdOptionCodeMap.get(extendedItemId);
			List<String> updatedRecentlyUsedList = new List<String>(newlyUsedValueSet);

			Integer count = updatedRecentlyUsedList.size();
			Integer index = 0;
			while (count < MAX_EXTENDED_ITEM_LOOKUP_RECENTLY_USED
					&& index < oldRecentlyUsedEntity.getRecentlyUsedValuesSize()) {
				if (!newlyUsedValueSet.contains(oldRecentlyUsedEntity.getRecentlyUsedValue(index))) {
					updatedRecentlyUsedList.add(oldRecentlyUsedEntity.getRecentlyUsedValue(index));
					count++;
				}
				index++;
			}
			ExpRecentlyUsedEntity recentlyUsedEntity = new ExpRecentlyUsedEntity();
			recentlyUsedEntity.setId(oldRecentlyUsedEntity.id);
			recentlyUsedEntity.employeeBaseId = employeeBaseId;
			recentlyUsedEntity.targetId = extendedItemId;
			recentlyUsedEntity.masterType = ExpRecentlyUsedEntity.MasterType.ExtendedItemLookup;
			recentlyUsedEntity.replaceRecentlyUsedValues(updatedRecentlyUsedList);
			toUpdateRecentlyUsedEntityList.add(recentlyUsedEntity);
		}
		new ExpRecentlyUsedRepository().saveEntityList(toUpdateRecentlyUsedEntityList);
	}

	/*
	 * Save Recently Used Vendor values
	 * Maximum of 10 is saved and the least recently used will be discarded when there are more than 10.
	 * Only when the vendor is used newly, the recently used is updated.
	 * (In case user update the report without changing vendor, it won't update the recently used.)
	 *
	 * @param employeeBaseId ID of employee base
	 */
	public void saveRecentlyUsedVendorValues(Id employeeBaseId, Id oldVendorId, Id newVendorId) {

		if ((newVendorId != null) && (newVendorId != oldVendorId)) {
			ExpRecentlyUsedEntity recentlyUsedEntity = new ExpRecentlyUsedEntity();
			List<String> recentlyUsedValueList = new List<String>();
			ExpRecentlyUsedEntity existingEntity = this.getExistingVendorRecentlyUsed(employeeBaseId);
			if (existingEntity != null) {
				recentlyUsedEntity.setId(existingEntity.id);
				recentlyUsedValueList = existingEntity.getRecentlyUsedValues();
			}
			recentlyUsedEntity.employeeBaseId = employeeBaseId;
			recentlyUsedEntity.targetId = null;
			recentlyUsedEntity.masterType = ExpRecentlyUsedEntity.MasterType.Vendor;
			// Search and delete the same vendor id in the existing recently used values.
			for (Integer i = recentlyUsedValueList.size() - 1; i >= 0; i--) {
				String value = recentlyUsedValueList.get(i);
				if (value.equals(newVendorId)) {
					recentlyUsedValueList.remove(i);
				}
			}
			// Add the latest used vendor id on top
			if (!recentlyUsedValueList.isEmpty()) {
				recentlyUsedValueList.add(0, newVendorId);
			} else {
				recentlyUsedValueList.add(newVendorId);
			}

			// Delete exceeded values from the bottom until becoming 10 records.
			while (recentlyUsedValueList.size() > 10) {
				recentlyUsedValueList.remove(recentlyUsedValueList.size()-1);
			}
			recentlyUsedEntity.replaceRecentlyUsedValues(recentlyUsedValueList);
			new ExpRecentlyUsedRepository().saveEntity(recentlyUsedEntity);
		}
	}

	/**
	 * Save recently used Cost Center Id
	 * Maximum of 10 is saved and the least recently used will be discarded when there are more than 10.
	 * If user updates the report without changing Cost Center, it won't update the recently used.
	 * @param employeeBaseId Employee Salesforce id
	 * @param oldCCId Old Cost Center Id used in old ReportType (if it is an update)
	 * @param newCCId New Cost Center Id used (including the newly created report)
	 *
	 */
	public void saveRecentlyUsedCostCenterValues(Id employeeBaseId, Id oldCCId, Id newCCId) {
		if (newCCId != null && !newCCID.equals(oldCCId)) {
			ExpRecentlyUsedEntity  recentlyUsedEntity = new ExpRecentlyUsedEntity();
			List<String> recentlyUsedValueList = new List<String>();
			ExpRecentlyUsedEntity existingEntity = this.getExistingExpRecentlyUsedEntity(employeeBaseId, ExpRecentlyUsedEntity.MasterType.CostCenter, null);
			// When employee have previous recently used Cost Centers
			if (existingEntity != null) {
				recentlyUsedEntity.setId(existingEntity.id);
				recentlyUsedValueList = existingEntity.getRecentlyUsedValues();
			}
			recentlyUsedEntity.employeeBaseId = employeeBaseId;
			recentlyUsedEntity.targetId = null;
			recentlyUsedEntity.masterType = ExpRecentlyUsedEntity.MasterType.CostCenter;
			// Search and delete the same cost center id in the exciting recentlyUsedValueList to move it up on the list
			for (Integer i = 0; i < recentlyUsedValueList.size(); i++) { // search and remove
				if (recentlyUsedValueList[i].equals(newCCId)) {
					recentlyUsedValueList.remove(i);
					break; // Once found, remove it and no need to keep finding in later part
				}
			}
			// add back or add at the top of the list when the recently used item is less than 10
			if (recentlyUsedValueList.size() < MAX_COST_CENTER_RECENTLY_USED_COUNT) {
				if (recentlyUsedValueList.isEmpty()) {
					recentlyUsedValueList.add(newCCId);
				} else {
					recentlyUsedValueList.add(0, newCCId);
				}
			} else {
				// there won't be case there is more than 10 because of this saving logic so if is not less than 10 mean equal to 10
				recentlyUsedValueList.remove(MAX_COST_CENTER_RECENTLY_USED_COUNT - 1); // remove the last item
				recentlyUsedValueList.add(0, newCCId); // add the Id to top of the list
			}

			recentlyUsedEntity.replaceRecentlyUsedValues(recentlyUsedValueList);
			new ExpRecentlyUsedRepository().saveEntity(recentlyUsedEntity);
		}
	}

	/**
	 * Save recently used Job Id
	 * Maximum of 10 is saved and the least recently used will be discarded when there are more than 10.
	 * If user updates the report without changing Job, it won't update the recently used.
	 * @param employeeBaseId Employee Base id
	 * @param oldJobId Old Job Id used in old ReportType (if it is an update)
	 * @param newJobId New Job Id used (including the newly created report)
	 *
	 */
	public void saveRecentlyUsedJobValues(Id employeeBaseId, Id oldJobId, Id newJobId) {
		if (newJobId != null && !newJobId.equals(oldJobId)) {
			ExpRecentlyUsedEntity  recentlyUsedEntity = new ExpRecentlyUsedEntity();
			List<String> recentlyUsedValueList = new List<String>();
			ExpRecentlyUsedEntity existingEntity = this.getExistingExpRecentlyUsedEntity(employeeBaseId, ExpRecentlyUsedEntity.MasterType.Job, null);
			// When employee have previous recently used Job
			if (existingEntity != null) {
				recentlyUsedEntity.setId(existingEntity.id);
				recentlyUsedValueList = existingEntity.getRecentlyUsedValues();
			}
			recentlyUsedEntity.employeeBaseId = employeeBaseId;
			recentlyUsedEntity.targetId = null;
			recentlyUsedEntity.masterType = ExpRecentlyUsedEntity.MasterType.Job;
			// Search and delete the same job id in the exciting recentlyUsedValueList to move it up on the list
			for (Integer i = 0; i < recentlyUsedValueList.size(); i++) { // search and remove
				if (recentlyUsedValueList[i].equals(newJobId)) {
					recentlyUsedValueList.remove(i);
					break; // Once found, remove it and no need to keep finding in later part
				}
			}
			// add back or add at the top of the list when the recently used item is less than 10
			if (recentlyUsedValueList.size() < MAX_JOB_RECENTLY_USED_COUNT) {
				if (recentlyUsedValueList.isEmpty()) {
					recentlyUsedValueList.add(newJobId);
				} else {
					recentlyUsedValueList.add(0, newJobId);
				}
			} else {
				// there won't be case there is more than 10 because of this saving logic so if is not less than 10 mean equal to 10
				recentlyUsedValueList.remove(MAX_JOB_RECENTLY_USED_COUNT - 1); // remove the last item
				recentlyUsedValueList.add(0, newJobId); // add the Id to top of the list
			}

			recentlyUsedEntity.replaceRecentlyUsedValues(recentlyUsedValueList);
			new ExpRecentlyUsedRepository().saveEntity(recentlyUsedEntity);
		}
	}

	/*
	 * Save recently used expense types based on report type used by user
	 * Maximum of 10 is saved and the least recently used will be discarded when there are more than 10.
	 * If user updates the report without changing Expense Type, it won't update the recently used.
	 * @param employeeBaseId ID of the EmployeeBase
	 * @param oldExpenseTypeId Original expense type
	 * @param newExpenseTypeId New expense type
	 * @param reportTypeId Report type id
	 */
	public void saveRecentlyUsedExpenseTypeValues(Id employeeBaseId, Id oldExpenseTypeId, Id newExpenseTypeId,
												  Id reportTypeId) {

		if (newExpenseTypeId != null && !newExpenseTypeId.equals(oldExpenseTypeId)) {
			ExpRecentlyUsedEntity recentlyUsedEntity = new ExpRecentlyUsedEntity();
			List<String> existingUsedValueStringList = new List<String>();
			ExpRecentlyUsedEntity existingEntity =
					this.getExistingExpRecentlyUsedEntity(employeeBaseId, ExpRecentlyUsedEntity.MasterType.ExpenseType,
							reportTypeId);
			// When employee have previous recently used ExpenseType
			if (existingEntity != null) {
				recentlyUsedEntity.setId(existingEntity.id);
				existingUsedValueStringList = existingEntity.getRecentlyUsedValues();
			}

			// Search and delete the same expense type id in the existing recentlyUsedValueList to move it up on the list
			for (Integer i = 0; i < existingUsedValueStringList.size(); i++) {
				if (existingUsedValueStringList[i].equals(newExpenseTypeId)) {
					existingUsedValueStringList.remove(i);
					break; // Once removed no need to keep looking for the exp Type
				}
			}

			// Add the recently used Expense Type
			if (existingUsedValueStringList.size() < MAX_EXPENSE_TYPE_RECENTLY_USED_COUNT) {
				if (existingUsedValueStringList.isEmpty()) {
					existingUsedValueStringList.add(newExpenseTypeId);
				} else {
					existingUsedValueStringList.add(0, newExpenseTypeId);
				}
			} else {
				// Means got 10 recently used expense type(no case of greater than 10)
				existingUsedValueStringList.remove(MAX_EXPENSE_TYPE_RECENTLY_USED_COUNT - 1);
				existingUsedValueStringList.add(0, newExpenseTypeId);
			}

			recentlyUsedEntity.employeeBaseId = employeeBaseId;
			recentlyUsedEntity.targetId = reportTypeId;
			recentlyUsedEntity.masterType = ExpRecentlyUsedEntity.MasterType.ExpenseType;
			recentlyUsedEntity.replaceRecentlyUsedValues(existingUsedValueStringList);
			new ExpRecentlyUsedRepository().saveEntity(recentlyUsedEntity);
		}
	}

	/*
	 * Get a map representing the existing recently used values in Key: TargetId, Value: Entity
	 *
	 * @param employeeBaseId ID of the EmployeeBase
	 * @param extendedItemIdSet Set of ExtendedItem (lookup IDs)
	 *
	 * @return Map<Id, ExpRecentlyUsedEntity> Key: TargetId, Value: Entity
	 */
	private Map<Id, ExpRecentlyUsedEntity> getExistingRecentlyUsedMap(Id employeeBaseId, Set<Id> extendedItemIdSet) {
		ExpRecentlyUsedRepository repo = new ExpRecentlyUsedRepository();
		ExpRecentlyUsedRepository.SearchFilter filter = new ExpRecentlyUsedRepository.SearchFilter();
		filter.employeeBaseId = employeeBaseId;
		filter.targetIds = new Set<String>((List<String>)new List<Id>(extendedItemIdSet));
		filter.masterType = ExpRecentlyUsedEntity.MasterType.ExtendedItemLookup;

		List<ExpRecentlyUsedEntity> recentlyUsedEntityList = repo.searchEntity(filter);

		Map<Id, ExpRecentlyUsedEntity> existingRecentlyUsedMap = new Map<Id, ExpRecentlyUsedEntity>();
		for (ExpRecentlyUsedEntity entity: recentlyUsedEntityList) {
			existingRecentlyUsedMap.put(entity.targetId, entity);
		}
		return existingRecentlyUsedMap;
	}

	private ExpRecentlyUsedEntity getExistingVendorRecentlyUsed(Id employeeBaseId) {
		ExpRecentlyUsedRepository.SearchFilter filter = new ExpRecentlyUsedRepository.SearchFilter();
		filter.employeeBaseId = employeeBaseId;
		filter.masterType = ExpRecentlyUsedEntity.MasterType.Vendor;

		List<ExpRecentlyUsedEntity> recentlyUsedEntityList = new ExpRecentlyUsedRepository().searchEntity(filter);
		return !recentlyUsedEntityList.isEmpty() ? recentlyUsedEntityList.get(0) : null;
	}

	/**
	 * Get Existing Recently Used Entity
	 * @param employeeBaseId ID of EmployeeBase
	 * @masterType Recently used item value type
	 * @targetId Target Id
	 * @return ExpRecentlyUsedEntity
	 */
	public ExpRecentlyUsedEntity getExistingExpRecentlyUsedEntity(Id employeeBaseId, ExpRecentlyUsedEntity.MasterType masterType, Id targetId) {
		ExpRecentlyUsedRepository.SearchFilter filter = new ExpRecentlyUsedRepository.SearchFilter();
		filter.employeeBaseId = employeeBaseId;
		filter.masterType = masterType;
		if (targetId != null) {
			filter.targetIds = new Set<String>{targetId};
		}

		List<ExpRecentlyUsedEntity> recentlyUsedEntityList = new ExpRecentlyUsedRepository().searchEntity(filter);
		return !recentlyUsedEntityList.isEmpty() ? recentlyUsedEntityList.get(0) : null;
	}
}