/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * 早朝勤務申請・残業申請(勤怠日次申請)サービス
  */
  public with sharing class AttEarlyStartOvertimeWorkReqService extends AttDailyRequestService.AbstractRequestProcessor {

  	private final AttDailyRequestRepository dailyRequestRepository = new AttDailyRequestRepository();

	private final AttAttendanceService attService = new AttAttendanceService();

  	/** メッセージ */
	private static final ComMsgBase MESSAGE;
	static {
		MESSAGE = ComMessage.msg();
	}

	/**
	 * 早朝勤務申請/残業申請パラメータ
	 */
	public class SubmitParam extends AttDailyRequestService.SubmitParam {
		/** 対象日*/
		public AppDate targetDate;
		/** 開始時刻 */
		public AttTime startTime;
		/** 終了時刻 */
		public AttTime endTime;
		/** 備考 */
		public String remarks;
		/** リクエストタイプ（早朝勤務申請/残業申請） */
		public AttRequestType requestType;
	}

	/**
	 * 早朝勤務申請/残業申請を承認申請する
	 * @param param 申請情報
	 */
	public override AttDailyRequestEntity submit(AttDailyRequestService.SubmitParam submitParam) {
		SubmitParam param = (SubmitParam)submitParam;

		// 再申請の場合は削除する
		if (param.requestId != null) {
			super.remove(dailyRequestRepository.getEntity(param.requestId));
		}

		// 申請対象となる勤怠明細レコードとサマリーを取得する
		AttDailyRequestService.RecordsWithSummary recordsWithSummary =
				super.getRecordEntityList(param.empId, param.targetDate, null);
		Map<Id, AttSummaryEntity> summaryMap = recordsWithSummary.summaryMap;
		List<AttRecordEntity> records = recordsWithSummary.records;
		AttRecordEntity recordData = records[0];

		// 早朝勤務申請/残業申請を使用可能か判定
		isValidEarlyStartOvertimeWorkRequest(summaryMap.get(recordData.summaryId), param.requestType);

		// 社員を取得する
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity employee = empService.getEmployee(
				summaryMap.get(recordData.summaryId).employeeBaseId, param.targetDate);

		// 社員の会社のデフォルト言語取得する
		AppLanguage companyLang = super.getCompanyLanguage(employee.companyId);

		// 申請エンティティを作成する
		AttDailyRequestEntity requestEntity = createRequestEntity(param, recordsWithSummary, employee, companyLang);

		// 申請エンティティの共通バリデーション実行
		Validator.Result requestValidResult = new AttValidator.DailyRequestValidator(requestEntity).validate();
		if (!requestValidResult.isSuccess()) {
			Validator.Error error = requestValidResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 申請を保存する
		Repository.SaveResult saveRes = dailyRequestRepository.saveEntity(requestEntity);
		requestEntity.setId(saveRes.details[0].Id);

		// 勤務体系を取得する
		AttSummaryEntity summary = summaryMap.get(recordData.summaryId);
		AttWorkingTypeBaseEntity workingTypeBaseEntity = new AttDailyRequestService().getWorkingType(summary.workingTypeBaseId, summary.endDate);
		AttWorkingTypeHistoryEntity workingTypeHistoryEntity = workingTypeBaseEntity.getHistory(0);

		// 勤務パターンを取得する
		Id patternId = recordData.reqRequestingPatternPatternId != null ? recordData.reqRequestingPatternPatternId : recordData.outPatternId;
		AttPatternEntity patternEntity = new AttPatternRepository2().getPatternById(patternId);

		// 早朝勤務申請/残業申請のバリデーション実行
		AttValidator.EarlyStartWorkOrOvertimeRequestSubmitValidator earlyStartWorkOrOvertimeValidator = new AttValidator.EarlyStartWorkOrOvertimeRequestSubmitValidator(records[0], workingTypeHistoryEntity, requestEntity, patternEntity);
		Validator.Result earlyStartWorkOrOvertimeValidResult = earlyStartWorkOrOvertimeValidator.validate();
		if (!earlyStartWorkOrOvertimeValidResult.isSuccess()) {
			Validator.Error error = earlyStartWorkOrOvertimeValidResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 勤怠明細エンティティの申請IDを更新する
		if (AttRequestType.EARLY_START_WORK.equals(requestEntity.requestType)) {
			// 早朝勤務申請
			if (recordData.reqRequestingEarlyStartWorkRequestId != null
					&& recordData.reqRequestingEarlyStartWorkRequestId != requestEntity.Id) {
				throw new App.IllegalStateException(
							ComMessage.msg().Att_Err_UnconfirmedRequest); // 無効な申請を取下げください
			}
			recordData.reqRequestingEarlyStartWorkRequestId = requestEntity.Id;
		} else if (AttRequestType.OVERTIME_WORK.equals(requestEntity.requestType)) {
			// 残業申請
			if (recordData.reqRequestingOvertimeWorkRequestId != null
					&& recordData.reqRequestingOvertimeWorkRequestId != requestEntity.Id) {
				throw new App.IllegalStateException(
							MESSAGE.Att_Err_UnconfirmedRequest); // 無効な申請を取下げください
			}
			recordData.reqRequestingOvertimeWorkRequestId = requestEntity.Id;
		}

		// 勤怠明細を保存する
		new AttSummaryRepository().saveRecordEntityList(records);

		// 申請する(申請コメントなし)
		super.submitProcess(requestEntity, null);
		return requestEntity;
	}

	/**
	 * 対象日に早朝勤務申請/残業申請を使用可能か、勤務体系マスタから判定する
	 * @param summary 勤怠サマリ
	 * @param requestType 申請タイプ（早朝勤務申請/残業申請）
	 * @throws App.IllegalStateException 対象日に早朝勤務申請/残業申請が使用不可の場合
	 */
	private void isValidEarlyStartOvertimeWorkRequest(AttSummaryEntity summary, AttRequestType requestType) {
		// 勤務体系を取得
		AttWorkingTypeBaseEntity base = new AttDailyRequestService().getWorkingType(summary.workingTypeBaseId, summary.endDate);
		// 勤務体系履歴を取得
		AttWorkingTypeHistoryEntity history = base.getHistory(0);

		// 早朝勤務申請/残業申請の使用可否オプションを判定
		if (requestType == AttRequestType.EARLY_START_WORK) {
			if (history == null || !history.useEarlyStartWorkApply) {
				throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_EARLY_START_WORK_REQUEST, ComMessage.msg().Att_Err_NotPermittedEarlyStartWorkRequest);
			}
		} else {
			if (history == null || !history.useOvertimeWorkApply) {
				throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_OVERTIME_WORK_REQUEST, ComMessage.msg().Att_Err_NotPermittedOvertimeWorkRequest);
			}
		}
	}

	/**
	 * 早朝勤務申請/残業申請用の勤怠日次申請エンティティを作成する
	 * @param param 早朝勤務申請/残業申請パラメータ
	 * @param recordsWithSummary 申請対象の勤怠明細
	 * @param employee 社員ベースエンティティ
	 * @param lang 作成言語
	 * @return 作成した日次申請エンティティ
	 */
	private AttDailyRequestEntity createRequestEntity(
			AttEarlyStartOverTimeWorkReqService.SubmitParam param,
			AttDailyRequestService.RecordsWithSummary recordsWithSummary,
			EmployeeBaseEntity employee, AppLanguage lang) {

		AttDailyRequestEntity entity = new AttDailyRequestEntity();
		entity.setId(param.requestId);
		entity.name = createRequestName(param, employee, lang);
		entity.requestType = param.requestType;
		entity.status = AppRequestStatus.PENDING;
		entity.requestTime = AppDatetime.now();
		entity.remarks = param.remarks;
		entity.startDate = param.targetDate;
		entity.endDate = param.targetDate;
		entity.startTime = param.startTime;
		entity.endTime = param.endTime;

		setRequestCommInfo(entity, employee);

		return entity;
	}

	/**
	 * 早朝勤務申請/残業申請の申請名を作成する
	 * @param param 早朝勤務申請/残業申請のパラメータ
	 * @param employee 社員
	 * @param lang 作成言語
	 * @return 申請名
	 */
	@testVisible
	private String createRequestName(
			AttEarlyStartOverTimeWorkReqService.SubmitParam param,
			EmployeeBaseEntity employee, AppLanguage lang) {

		// {氏名} さん 早朝勤務申請/残業申請 {YYYYMMDD} {HH:mm}–{HH:mm}
		final AppDate.FormatType dateFormat = AppDate.FormatType.YYYYMMDD; // YYYYMMDD

		// 申請種類名
		String requestTypeName;
		ComMsgBase targetLang = ComMessage.msg(lang.value);
		// 早朝勤務申請
		if (AttRequestType.EARLY_START_WORK.equals(param.requestType)) {
			requestTypeName = targetLang.Att_Lbl_RequestTypeEarlyStartWork;
		// 残業申請
		} else {
			requestTypeName = targetLang.Att_Lbl_RequestTypeOvertimeWork;
		}
		String label = lang == AppLanguage.JA ?
			'さん ' + requestTypeName + targetLang.Att_Lbl_Request + ' ' :
			' ' + requestTypeName + ' ' + targetLang.Att_Lbl_Request + ' ';

		// 申請範囲
		String range;
		AppDate startDate = param.targetDate;
		AttTime startTime = param.startTime;
		AttTime endTime = param.endTime;
		range = startDate.format(dateFormat);
		if (startTime != null) {
			range += ' ' + startTime.format();
		}
		if (endTime != null) {
			range += '–' + endTime.format();
		}

		return employee.displayNameL.getValue(lang) + label + range;
	}

	/**
	 * @description 承認取消時のメールを作成する
	 * @param request 申請
	 * @param applicant 申請者
	 * @param empLastModified 最終更新者
	 * @param targetLanguage 送信先ユーザの言語
	 * @return mailMessage メール内容
	 */
	public override MailService.MailParam createCancelMailMessage(AttDailyRequestEntity request, EmployeeBaseEntity applicant, EmployeeBaseEntity empLastModified, AppLanguage targetLanguage) {

		MailService.MailParam mailMessage = new MailService.MailParam();
		String language = targetLanguage.value;

		// 送信者名
		String senderDisplayName = empLastModified.fullNameL.getFullName(targetLanguage);
		mailMessage.senderDisplayName = senderDisplayName;

		// メールタイトル
		String mailSubject;
		// 申請種別を文字列に変換する
		String requestType;
    if (request.requestType.value == 'EarlyStartWork') {
			requestType = ComMessage.msg(language).Att_Lbl_RequestTypeEarlyStartWork;
    } else {
			requestType = ComMessage.msg(language).Att_Lbl_RequestTypeOvertimeWork;
    }
		mailSubject = ComMessage.msg(language).Att_Msg_CancelMailSubject(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType});
		mailMessage.subject = mailSubject;

		// メール本文
		String mailBody;
		// 申請日付
		String requestDate = request.requestTime.formatLocal().substring(0, 10).replace('-', '/');
		// 開始日
		String startDate = String.valueOf(AppDate.convertDate(request.startDate)).replace('-', '/');
		// 開始時刻
		String startTime = request.startTime.format();
		// 終了時刻
		String endTime = request.endTime.format();
		// 備考
		String remarks = request.remarks;
		// メール本文を作成
		mailBody = ComMessage.msg(language).Att_Msg_CancelMailMessage(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType, empLastModified.fullNameL.getFullName(targetLanguage)});
		mailBody += '\r\n\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_RequestContents + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestDate + '：' + requestDate + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestType + '：' + requestType + '\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate;
		mailBody += ' ' + startTime + '－' + endTime + '\r\n';
		if (String.isNotBlank(remarks)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + remarks + '\r\n';
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + '\r\n';
		}
		// 署名
		mailBody += '\r\n---\r\n';
		mailBody += ComMessage.msg(language).Att_Msg_TeamSpirit;
		mailMessage.body = mailBody;

		return mailMessage;
	}
}