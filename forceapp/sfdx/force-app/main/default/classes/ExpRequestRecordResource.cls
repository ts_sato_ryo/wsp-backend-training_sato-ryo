/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Resource class providing APIs of Expense Request Record
 * 
 * @group Expense
 */
public with sharing class ExpRequestRecordResource {
	/**
	 * Parameter for saving Expense Request Record
	 * Base fields are inherited from ExpRequestService.ExpRequestRecord
	 */
	public class SaveExpRequestRecordParam extends ExpRequestService.ExpRequestRecord implements RemoteApi.RequestParam {
		/** Employee base ID */
		public String empId;
		/** Expense Request Id */
		public String requestId;
		/** Report Type Id */
		public String reportTypeId;

		/*
		 * Validate the SaveExpRecordParam param
		 * @param param SaveExpRecordParam containing the data to save
		 * @param isFinanceApproval indicate whether it's for Finance Approval
		 */
		public void validate() {
			if (this.items != null && !this.items.isEmpty()) {
				// Validate Job ID and Cost Center History ID (not required)
				ExpCommonUtil.validateId('jobId', this.items[0].jobId, false);
				ExpCommonUtil.validateId('costCenterHistoryId', this.items[0].costCenterHistoryId, false);

				for (ExpRequestService.ExpRequestRecordItem item : this.items) {
					// Extended Item Ids
					item.verifyExtendedItemIds();
					// Extended Item Date Values (if specified, check if the date is valid)
					item.verifyExtendedItemDateValues();
				}
			}
		}
	}

	/** Parameter for deleting Expense Request Record */
	public class DeleteExpRequestRecordParam implements RemoteApi.RequestParam {
		// Request Record ID
		public List<String> recordIds;
	}

	/** Parameter for Cloning Expense Request Record */
	public class CloneExpRequestRecordsParam implements RemoteApi.RequestParam {
		/** Request Record IDs */
		public List<String> recordIds;
		/** List of Date to clone the Records to */
		public List<String> targetDates;
	}

	/**
	 * Result of saving Expense Request Record
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		// Saved record ID
		public String recordId;

		public SaveResult(Id recordId) {
			this.recordId = recordId;
		}
	}

	public class CloneResult implements RemoteApi.ResponseParam {
		/** IDs of newly created records */
		public List<String> recordIds;
		/** Records Updated with new Value */
		public List<ExpParam.UpdatedClonedRecordResult> updatedRecords;
	}

	/**
	 * API to save Expense Record data
	 */
	public with sharing class SaveExpRequestRecordApi extends RemoteApi.ResourceBase {
		@TestVisible
		private Boolean isRollbackTest = false;
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SaveExpRequestRecordParam param = (SaveExpRequestRecordParam) req.getParam(SaveExpRequestRecordParam.class);
			param.validate();
			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(param.empId);

			// Check if the employee can use Expense module
			ExpCommonUtil.checkCanUseExpenseRequest(empBase);

			ExpRequestRecordEntity record = param.createEntity();
			record.expRequestId = param.requestId;
			Id reportTypeId = String.isNotEmpty(param.reportTypeId) ? Id.valueOf(param.reportTypeId) : null;
			Savepoint sp = Database.setSavepoint();
			Id recordId;
			try {
				recordId = new ExpRequestRecordService().saveExpRequestRecord(empBase, record, reportTypeId);
				if (isRollbackTest) {
					throw new App.IllegalStateException('SaveExpRequestRecordApi Rollback Test');
				}
			} catch(Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return new SaveResult(recordId);
		}
	}

	/*
	 * API to get Expense Record list
	 */
	public with sharing class DeleteExpRequestRecordApi extends RemoteApi.ResourceBase {
		@TestVisible
		private Boolean isRollbackTest = false;
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			DeleteExpRequestRecordParam param = (DeleteExpRequestRecordParam) req.getParam(DeleteExpRequestRecordParam.class);

			ExpCommonUtil.validateIdList('recordIds', param.recordIds, true);
			ExpCommonUtil.checkCanUseExpenseRequest(null);

			Savepoint sp = Database.setSavepoint();
			try {
				new ExpRequestRecordService().deleteExpRequestRecords(param.recordIds);
				if (isRollbackTest) {
					throw new App.IllegalStateException('DeleteExpRequestRecordApi Rollback Test');
				}
			} catch(Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}

	/*
	 * API to Clone Expense Records
	 */
	public with sharing class CloneExpRequestRecordsApi extends RemoteApi.ResourceBase {
		@TestVisible
		private Boolean isRollbackTest = false;
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			CloneExpRequestRecordsParam param = (CloneExpRequestRecordsParam) req.getParam(CloneExpRequestRecordsParam.class);

			ExpCommonUtil.validateIdList('recordIds', param.recordIds, true);
			if (param.targetDates == null || param.targetDates.isEmpty()) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{'targetDates'}));
			}
			for (String targetDate : param.targetDates) {
				ExpCommonUtil.validateDate('targetDates', targetDate, true);
			}

			List<AppDate> targetDateToCreateClone = new List<AppDate>();
			for (String targetDate : param.targetDates) {
				targetDateToCreateClone.add(AppDate.valueOf(targetDate));
			}

			ExpCommonUtil.checkCanUseExpenseRequest(null);

			Savepoint sp = Database.setSavepoint();
			try {
				Map<Id, ExpRequestRecordEntity> resultMap = new ExpRequestRecordService().cloneExpenseRequestRecords(param.recordIds, targetDateToCreateClone);
				CloneResult result = new CloneResult();
				result.recordIds = new List<String>();
				result.updatedRecords = new List<ExpParam.UpdatedClonedRecordResult>();
				for (Id resultId : resultMap.keySet()) {
					result.recordIds.add(resultId);
					ExpRequestRecordEntity recordEntity = resultMap.get(resultId);
					if (recordEntity != null) {
						ExpParam.UpdatedClonedRecordResult updatedRecord = new ExpParam.UpdatedClonedRecordResult();
						updatedRecord.recordId = recordEntity.id;
						// Take Item 0 as the itemization data are discarded in record clone.
						updatedRecord.expenseTypeName = recordEntity.recordItemList[0].expTypeName;
						updatedRecord.recordDate = recordEntity.recordDate.format();
						updatedRecord.isForeignCurrency = recordEntity.recordItemList[0].useForeignCurrency;
						result.updatedRecords.add(updatedRecord);
					}
				}
				if (isRollbackTest) {
					throw new App.IllegalStateException('CloneExpRequestRecordsApi Rollback Test');
				}
				return result;
			} catch(Exception e) {
				Database.rollback(sp);
				throw e;
			}
		}
	}
}