/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 親子型マスタ履歴切り替えバッチのテストクラス
 */
@isTest
private class ParentChildCurrentHistoryUpdateBatchTest {

	/**
	 * 短時間勤務設定履歴切り替えApexスケジュール正常系テスト
	 */
	@isTest
	static void schedulePositiveTest() {
		Test.StartTest();
		AttShortSettingCurrentHistoryUpdateBatch sch = new AttShortSettingCurrentHistoryUpdateBatch();
		Id schId = System.schedule('社員履歴切り替えスケジュール', '0 0 0 * * ?', sch);
		Test.StopTest();
		System.assertNotEquals(null, schId);
	}

	/**
	 * 短時間勤務設定履歴切り替えバッチ正常系テスト
	 */
	@isTest
	static void batchPositiveTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		ComTag__c tagObj = ComTestDataUtility.createTag('Test Tag', TagType.SHORTEN_WORK_REASON,
				companyObj.id, 1, false);
		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<AttShortTimeWorkSettingBase__c> baseList =
				ComTestDataUtility.createAttShortTimeSettingsWithHistories(
					'Test Setting', companyObj.Id, tagObj.Id, baseNum, historyNum);
		AttShortTimeWorkSettingBase__c base1 = baseList[0];
		List<AttShortTimeWorkSettingHistory__c> historyList1 =
				[SELECT Id, BaseId__c, Name_L0__c FROM AttShortTimeWorkSettingHistory__c WHERE BaseId__c = :base1.Id ORDER BY ValidFrom__c Asc];

		// CurrentHistoryId__c を空欄にしておく
		base1.Name = 'AAA';
		base1.CurrentHistoryId__c = null;
		update base1;

		Test.StartTest();
		AttShortSettingCurrentHistoryUpdateBatch b = new AttShortSettingCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
		// テストメソッドでは、Database.executeBatch()してもexecute()が実行されないので直接叩く
		// b.execute(null, new List<Sobject>());
		Test.StopTest();
		System.assertNotEquals(null, jobId);

		AttShortTimeWorkSettingBase__c resBase = [SELECT Id, Name, CurrentHistoryId__c FROM AttShortTimeWorkSettingBase__c WHERE Id = :base1.Id];
		System.assertEquals(historyList1[0].Id, resBase.CurrentHistoryId__c);
		// Name 項目は CurrentHistoryId__c 参照先の Name_L0__c
		System.assertEquals(historyList1[0].Name_L0__c, resBase.Name);
	}
}