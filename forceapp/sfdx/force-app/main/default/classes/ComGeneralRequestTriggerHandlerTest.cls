/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 汎用稟議申請のトリガーハンドラーテスト
 */
@isTest
private class ComGeneralRequestTriggerHandlerTest {

	/**
	 * initのテスト
	 * 承認待ちになった稟議申請IDを取得できることを確認する
	 */
	@isTest static void initTestPendingRequest() {

		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();
		ComGeneralRequest__c generalRequest = new ComGeneralRequest__c(Name = 'test');

		ComGeneralRequest__c generalRequestOld = new ComGeneralRequest__c(
					Id = generalRequest.Id,
					Status__c = AppConverter.stringValue(AppRequestStatus.DISABLED));
		ComGeneralRequest__c generalRequestNew = new ComGeneralRequest__c(
					Id = generalRequest.Id,
					Status__c = AppConverter.stringValue(AppRequestStatus.PENDING));

		oldMap.put(generalRequest.Id, generalRequestOld);
		newMap.put(generalRequest.Id, generalRequestNew);

		// 実行
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		// 検証
		System.assert(handler.pendingRequest != null);
	}

	/**
	 * initのテスト
	 * 承認済みになった稟議申請IDを取得できることを確認する
	 */
	@isTest static void initTestApprovedRequest() {

		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();
		ComGeneralRequest__c generalRequest = new ComGeneralRequest__c(Name = 'test');

		ComGeneralRequest__c generalRequestOld = new ComGeneralRequest__c(
					Id = generalRequest.Id,
					Status__c = AppConverter.stringValue(AppRequestStatus.PENDING));
		ComGeneralRequest__c generalRequestNew = new ComGeneralRequest__c(
					Id = generalRequest.Id,
					Status__c = AppConverter.stringValue(AppRequestStatus.APPROVED));

		oldMap.put(generalRequest.Id, generalRequestOld);
		newMap.put(generalRequest.Id, generalRequestNew);

		// 実行
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		// 検証
		System.assertEquals(1, handler.approvedRequestIds.size());
	}

	/**
	 * initのテスト
	 * 無効になった稟議申請IDを取得できることを確認する
	 */
	@isTest static void initTestDisabledRequest() {

		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();

		ComGeneralRequest__c generalRequest = new ComGeneralRequest__c(Name = 'test');
		ComGeneralRequest__c generalRequestOld = new ComGeneralRequest__c(
					Id = generalRequest.Id,
					Status__c = AppConverter.stringValue(AppRequestStatus.PENDING));
		ComGeneralRequest__c generalRequestNew = new ComGeneralRequest__c(
					Id = generalRequest.Id,
					Status__c = AppConverter.stringValue(AppRequestStatus.DISABLED));

		oldMap.put(generalRequest.id, generalRequestOld);
		newMap.put(generalRequest.id, generalRequestNew);

		// 実行
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		// 検証
		System.assertEquals(1, handler.disabledRequestIds.size());
	}

	/**
	 * applyGeneralRequestInfoのテスト
	 * 正常に申請情報が設定されることを確認する
	 */
	@isTest static void applyGeneralRequestInfoTestSuccess() {

		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();
		ComGeneralRequest__c generalRequest = new ComGeneralRequest__c(Name = 'test', OwnerId = UserInfo.getUserId());
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		handler.pendingRequest = generalRequest;

		// 日付時刻を設定
		AppDatetime.setNow(Datetime.newInstance(2018, 11, 29, 0, 0, 0));
		// 実行
		handler.applyGeneralRequestInfo();
		// 検証
		System.assertEquals(AppDateTime.convertDatetime(AppDateTime.now()), handler.pendingRequest.RequestTime__c);
		System.assertEquals(UserInfo.getUserId(), handler.pendingRequest.ActorId__c);
		System.assertEquals(testData.employee.getHistory(0).id, handler.pendingRequest.EmployeeHistoryId__c);
		System.assertEquals('001_L0 太郎_L0', handler.pendingRequest.EmployeeName__c);
		System.assertEquals(new DepartmentRepository().getEntity(testData.dept.Id).getHistory(0).id, handler.pendingRequest.DepartmentHistoryId__c);
		System.assertEquals('AttTest Dept L0', handler.pendingRequest.DepartmentName__c);
	}

	/**
	 * applyGeneralRequestInfoのテスト
	 * 会社のデフォルト言語によって、社員名と部署名が切り替わることを確認する
	 */
	@isTest static void applyGeneralRequestInfoTestLanguage() {

		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();
		ComGeneralRequest__c generalRequest = new ComGeneralRequest__c(Name = 'test', OwnerId = UserInfo.getUserId());
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		handler.pendingRequest = generalRequest;

		// 会社のデフォルト言語を'en_US'にする
		update new ComCompany__c(Id = testData.company.Id, Language__c = 'en_US');
		// 実行
		handler.applyGeneralRequestInfo();
		// 検証
		System.assertEquals('太郎_L1 001_L1', handler.pendingRequest.EmployeeName__c);
		System.assertEquals('AttTest Dept L1', handler.pendingRequest.DepartmentName__c);
	}

	/**
	 * applyGeneralRequestInfoのテスト
	 * 申請が存在せずにメソッドが呼ばれた場合、異常終了することを確認する
	 */
	@isTest static void applyGeneralRequestInfoTestPendingRequestNull() {

		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		handler.pendingRequest = null;

		// 実行
		try {
			handler.applyGeneralRequestInfo();
			System.assert(false);
		} catch (App.IllegalStateException e) {
			System.debug(e.getMessage());
			System.assertEquals(ComMessage.msg().Att_Err_NotFoundRequest, e.getMessage());
		}
	}

	/**
	 * applyGeneralRequestInfoのテスト
	 * 社員履歴の有効期間外の場合、異常終了することを確認する
	 */
	@isTest static void applyGeneralRequestInfoTestInvalidEmployee() {

		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();
		ComGeneralRequest__c generalRequest = new ComGeneralRequest__c(Name = 'test', OwnerId = UserInfo.getUserId());
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		handler.pendingRequest = generalRequest;

		// 社員履歴の失効日を前日に設定する
		AppDatetime.setNow(Datetime.newInstance(2018, 11, 29, 0, 0, 0));
		testData.employee.getHistory(0).validTo = AppDate.valueOf(AppDatetime.now().getDate()).minusDays(1);
		new EmployeeRepository().saveEntity(testData.employee);
		// 実行
		try {
			handler.applyGeneralRequestInfo();
			System.assert(false);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidRequestForNoEmployeesFound, e.getMessage());
		}
	}

	/**
	 * applyGeneralRequestInfoのテスト
	 * 部署履歴の有効期間外の場合、部署情報が設定されないことを確認する
	 */
	@isTest static void applyGeneralRequestInfoTestInvalidDepartment() {

		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		Map<Id, ComGeneralRequest__c> oldMap = new Map<Id, ComGeneralRequest__c>();
		Map<Id, ComGeneralRequest__c> newMap = new Map<Id, ComGeneralRequest__c>();
		ComGeneralRequest__c generalRequest = new ComGeneralRequest__c(Name = 'test', OwnerId = UserInfo.getUserId());
		ComGeneralRequestTriggerHandler handler = new ComGeneralRequestTriggerHandler(oldMap, newMap);
		handler.pendingRequest = generalRequest;

		// 部署履歴の失効日を前日に設定する
		AppDatetime.setNow(Datetime.newInstance(2018, 11, 29, 0, 0, 0));
		DepartmentHistoryEntity deptHistory = new DepartmentRepository().getEntity(testData.dept.Id).getHistory(0);
		deptHistory.validTo = AppDate.valueOf(AppDatetime.now().getDate()).minusDays(1);
		new DepartmentRepository().saveHistoryEntity(deptHistory);
		// 実行
		handler.applyGeneralRequestInfo();
		// 検証
		System.assertEquals(testData.employee.getHistory(0).id, handler.pendingRequest.EmployeeHistoryId__c);
		System.assertEquals('001_L0 太郎_L0', handler.pendingRequest.EmployeeName__c);
		System.assertEquals(null, handler.pendingRequest.DepartmentHistoryId__c);
		System.assertEquals(null, handler.pendingRequest.DepartmentName__c);
	}

	/**
	 * ComGeneralRequestTriggerのテスト
	 */
	@isTest static void comGeneralRequestTriggerTest() {

		// 新規作成時にステータスがNULLになることを確認
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		ComGeneralRequest__c generalRequest1 = new ComGeneralRequest__c(Name = 'test', Status__c = AppRequestStatus.DISABLED.value, OwnerId = UserInfo.getUserId());
		insert generalRequest1;
		ComGeneralRequest__c result = [SELECT Id, Status__c, OwnerId FROM ComGeneralRequest__c WHERE Name = 'test'][0];
		System.assertEquals(null, result.Status__c);

		// 更新できることを確認
		ComGeneralRequest__c generalRequest2 = [SELECT Id, Status__c, OwnerId FROM ComGeneralRequest__c WHERE Name = 'test'][0];
		result.Status__c = AppRequestStatus.PENDING.value;
		update generalRequest2;
	}
}