/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 履歴管理方式が有効期間型のマスタのエンティティ基底クラス
 */
public with sharing abstract class ValidPeriodRepositoryOld extends Repository.BaseRepository {

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveEntity(ValidPeriodEntityOld entity) {
		return saveEntityList(new List<ValidPeriodEntityOld>{entity}, ALL_SAVE);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveEntityList(List<ValidPeriodEntityOld> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public virtual Repository.SaveResult saveEntityList(List<ValidPeriodEntityOld> entityList, Boolean allOrNone) {

		List<SObject> saveObjectList = createObjectList(entityList);
		List<Database.UpsertResult> resList = AppDatabase.doUpsert(saveObjectList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/**
	 * SObjectに有効期間型共通の項目を設定する
	 */
	protected void setValidPeriodEntityValueToObject(ValidPeriodEntityOld entity, SObject toObj) {

		toObj.put('Id', entity.id);

		if (entity.isChanged(ValidPeriodEntityOld.Field.NAME)) {
			toObj.put('Name', entity.name);
		}
		if (entity.isChanged(ValidPeriodEntityOld.Field.VALID_FROM)) {
			toObj.put('ValidFrom__c', AppDate.convertDate(entity.validFrom));
		}
		if (entity.isChanged(ValidPeriodEntityOld.Field.VALID_TO)) {
			toObj.put('ValidTo__c', AppDate.convertDate(entity.validTo));
		}
	}

	/**
	 * SObjectに有効期間型共通の項目を設定する
	 */
	protected void setValidPeriodObjectValueToEntity(SObject obj, ValidPeriodEntityOld toEntity) {

		toEntity.setId((Id)obj.get('Id'));
		toEntity.name = (String)obj.get('Name');
		toEntity.validFrom = AppDate.valueOf((Date)obj.get('ValidFrom__c'));
		toEntity.validTo = AppDate.valueOf((Date)obj.get('ValidTo__c'));

		return;
	}


	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected abstract List<SObject> createObjectList(List<ValidPeriodEntityOld> entityList);

}
