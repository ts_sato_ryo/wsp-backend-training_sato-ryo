/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Service class for ExtendedItemCustom and ExtendedItemCustomOption
 * 
 * @group Expense
 */
public with sharing class ExtendedItemCustomService {

	private ExtendedItemCustomRepository repo;

	public ExtendedItemCustomService() {
		repo = new ExtendedItemCustomRepository();
	}

	/*
	 * Get List of {@code ExtendedItemCustomEntity}.
	 *
	 * @param filter Search filter for ExtendedItemCustomEntity
	 * @param includeOptions If set to True, then the available Options will be included in each {@code ExtendedItemCustomEntity} entity.
	 *
	 * @return List<ExtendedItemCustomEntity>
	 */
	public List<ExtendedItemCustomEntity> getExtendedItemCustomEntityList(ExtendedItemCustomRepository.SearchFilter filter, Boolean includeOptions) {
		List<ExtendedItemCustomEntity> entityList = repo.searchEntity(filter, ExtendedItemCustomRepository.SortOrder.CODE_ASC);

		if (includeOptions == true) {
			setExtendedItemCustomOptions(entityList);
		}

		return entityList;
	}

	/*
	 * Get List of {@code ExtendedItemCustomOptionEntity}.
	 * The returned list is ordered by ExtendedItemCustomOption's Code in ascending order.
	 *
	 * @param filter Search filter for ExtendedItemCustomOptionEntity
	 * @param resultLimit if specified, the returned result will be limited to the specified records
	 *
	 * @return List<ExtendedItemCustomOptionEntity>
	 */
	public List<ExtendedItemCustomOptionEntity> getExtendedItemCustomOptionEntityList(ExtendedItemCustomRepository.OptionSearchFilter filter, Integer resultLimit) {
		return repo.searchOptionEntity(filter, ExtendedItemCustomRepository.OptionSortOrder.CODE_ASC, resultLimit);
	}

	public List<ExtendedItemCustomOptionEntity> getExtendedItemCustomOptionList(Id extendedItemCustomId, Set<String> codes) {
		ExtendedItemCustomRepository.OptionSearchFilter filter = new ExtendedItemCustomRepository.OptionSearchFilter();
		filter.codes = codes;
		filter.extendedItemCustomIds = new Set<Id>{extendedItemCustomId};
		return new ExtendedItemCustomService().getExtendedItemCustomOptionEntityList(filter, null);
	}

	public Map<Id, Set<ExtendedItemCustomOptionEntity>> getExtendedItemCustomOptionMap(Set<Id> extendedItemCustomIds, Set<String> codes) {
		ExtendedItemCustomRepository.OptionSearchFilter filter = new ExtendedItemCustomRepository.OptionSearchFilter();
		filter.codes = codes;
		filter.extendedItemCustomIds = extendedItemCustomIds;

		Map<Id, Set<ExtendedItemCustomOptionEntity>> extendedItemCustomOptionSetMap = new Map<Id, Set<ExtendedItemCustomOptionEntity>>();
		List<ExtendedItemCustomOptionEntity> optionEntityList = new ExtendedItemCustomService()
				.getExtendedItemCustomOptionEntityList(filter, null);
		for (ExtendedItemCustomOptionEntity optionEntity: optionEntityList) {
			Set<ExtendedItemCustomOptionEntity> optionSet = extendedItemCustomOptionSetMap.get(optionEntity.extendedItemCustomId);
			if (optionSet == null) {
				optionSet = new Set<ExtendedItemCustomOptionEntity>();
				extendedItemCustomOptionSetMap.put(optionEntity.extendedItemCustomId, optionSet);
			}
			optionSet.add(optionEntity);
		}
		return extendedItemCustomOptionSetMap;
	}

	/*
	 * Retrieve and add the corresponding Options to each ExtendedItemCustomEntity in the List.
	 * No exception is thrown if the list is empty.
	 * @param customEntityList List of ExtendedItemCustomEntity to be updated with the ExtendedItemCustomOptionEntity
	 */
	private void setExtendedItemCustomOptions(List<ExtendedItemCustomEntity> customEntityList) {
		if (customEntityList.isEmpty()) {
			return;
		}

		// Collect ExtendedItemCustom IDs
		Map<Id, ExtendedItemCustomEntity> extendedItemCustomIdEntityMap = new Map<Id, ExtendedItemCustomEntity>();
		for (ExtendedItemCustomEntity entity: customEntityList) {
			extendedItemCustomIdEntityMap.put(entity.Id, entity);
		}

		// Retrieve all the corresponding Options
		ExtendedItemCustomRepository.OptionSearchFilter filter = new ExtendedItemCustomRepository.OptionSearchFilter();
		filter.extendedItemCustomIds = extendedItemCustomIdEntityMap.keySet();
		List<ExtendedItemCustomOptionEntity> optionList = getExtendedItemCustomOptionEntityList(filter, ExtendedItemCustomRepository.SEARCH_RECORDS_NUMBER_MAX);

		// Add the Option Entity to the corresponding ExtendedItemCustomEntity
		for (ExtendedItemCustomOptionEntity option : optionList) {
			// Since it's just adding to list, it won't trigger Field changed flag.
			extendedItemCustomIdEntityMap.get(option.extendedItemCustomId).extendedItemCustomOptionList.add(option);
		}
	}
}