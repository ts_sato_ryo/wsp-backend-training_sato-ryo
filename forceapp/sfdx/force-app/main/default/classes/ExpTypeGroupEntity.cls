/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 費目グループマスタのエンティティ
 */
public with sharing class ExpTypeGroupEntity extends ValidPeriodEntityOld {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 費目グループ名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 説明の最大文字数 */
	public static final Integer DESCRIPTION_MAX_LENGTH = 1024;
	/** 表示順の最小値 */
	public static final Integer ORDER_MIN_VALUE = 1;
	/** 表示順の最大値 */
	public static final Integer ORDER_MAX_VALUE = 9999;
	/** 親費目グループの最大階層数 */
	public static final Integer PARENT_MAX_HIERARCHY = 2;

	/**
	 * 項目の定義
	 * 項目を追加したら、getFieldValue, setFieldValueメソッドへの追加もお願いします。
	 */
	public enum Field {
		CODE,
		UNIQ_KEY,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		COMPANY_ID,
		PARENT_ID,
		DESCRIPTION_L0,
		DESCRIPTION_L1,
		DESCRIPTION_L2,
		ORDER
	}

	/** 値を変更した項目のリスト */
	protected Set<ExpTypeGroupEntity.Field> isChangedFieldSet = new Set<ExpTypeGroupEntity.Field>();

	/** 費目グループコード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	/** ユニークキー */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}

	/** 費目グループ名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	/** 費目グループ名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	/** 費目グループ名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/** 費目グループ名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}

	/** 会社ID */
	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	/** 親費目グループID */
	public Id parentId {
		get;
		set {
			parentId = value;
			setChanged(Field.PARENT_ID);
		}
	}

	/** 親費目グループ */
	public LookupField parentGroup;

	/** 説明(L0) */
	public String descriptionL0 {
		get;
		set {
			descriptionL0 = value;
			setChanged(Field.DESCRIPTION_L0);
		}
	}

	/** 説明(L1) */
	public String descriptionL1 {
		get;
		set {
			descriptionL1 = value;
			setChanged(Field.DESCRIPTION_L1);
		}
	}

	/** 説明(L2) */
	public String descriptionL2 {
		get;
		set {
			descriptionL2 = value;
			setChanged(Field.DESCRIPTION_L2);
		}
	}

	/** 説明(翻訳) */
	public AppMultiString descriptionL {
		get {return new AppMultiString(this.descriptionL0, this.descriptionL1, this.descriptionL2);}
	}

	/** 並び順 */
	public Integer order {
		get;
		set {
			order = value;
			setChanged(Field.ORDER);
		}
	}

	/** 子の費目グループリスト（参照のみ。値を変更してもDBに保存されない。） */
	public List<ExpTypeGroupEntity> childGroupList {get; set;}

	/** 子の費目リスト（参照のみ。値を変更してもDBに保存されない。） */
	public List<ExpTypeEntity> childExpTypeList {get; set;}


	/**
	 * 子の費目グループリストをリプレースする（リストをコピーする）
	 * @param groupList リプレース対象の費目グループリスト
	 */
	public void replaceChildGroupList(List<ExpTypeGroupEntity> groupList) {
		this.childGroupList = new List<ExpTypeGroupEntity>(groupList);
	}

	/**
	 * 子の費目リストをリプレースする（リストをコピーする）
	 * @param expTypeList リプレース対象の費目リスト
	 */
	public void replaceChildExpTypeList(List<ExpTypeEntity> expTypeList) {
		this.childExpTypeList = new List<ExpTypeEntity>(expTypeList);
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
	}

	/**
	 * ユニークキーを作成する
	 * インスタンス変数のcodeを指定しておくこと。
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]費目グループのユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]費目グループのユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + コード
		return companyCode + '-' + this.code;
	}

	/**
	 * エンティティの複製を作成する(IDはコピーされません)
	 */
	public ExpTypeGroupEntity copy() {
		ExpTypeGroupEntity copyEntity = new ExpTypeGroupEntity();
		for (ValidPeriodEntityOld.Field field : ValidPeriodEntityOld.Field.values()) {
			copyEntity.setFieldValue(field, this.getFieldValue(field));
		}
		for (ExpTypeGroupEntity.Field field : ExpTypeGroupEntity.Field.values()) {
			copyEntity.setFieldValue(field, this.getFieldValue(field));
		}
		return copyEntity;
	}

	/**
	 * 指定された項目の値を取得する
	 * @param targetField 対象の項目
	 * @return 項目値
	 */
	public Object getFieldValue(ExpTypeGroupEntity.Field targetField) {
		if (targetField == Field.CODE) {
			return this.code;
		}
		if (targetField == Field.UNIQ_KEY) {
			return this.uniqKey;
		}
		if (targetField == Field.NAME_L0) {
			return this.nameL0;
		}
		if (targetField == Field.NAME_L1) {
			return this.nameL1;
		}
		if (targetField == Field.NAME_L2) {
			return this.nameL2;
		}
		if (targetField == Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == Field.PARENT_ID) {
			return this.parentId;
		}
		if (targetField == Field.DESCRIPTION_L0) {
			return this.descriptionL0;
		}
		if (targetField == Field.DESCRIPTION_L1) {
			return this.descriptionL1;
		}
		if (targetField == Field.DESCRIPTION_L2) {
			return this.descriptionL2;
		}
		if (targetField == Field.ORDER) {
			return this.order;
		}

		// ここにきたらバグ
		throw new App.UnsupportedException('ExpTypeGroupEntity.getFieldValue: 未対応の項目です。targetField=' + targetField);
	}

	/**
	 * 指定された項目に値を設定する
	 * @param targetField 対象の項目
	 * @param value 設定値
	 */
	public void setFieldValue(ExpTypeGroupEntity.Field targetField, Object value) {
		if (targetField == Field.CODE) {
			this.code = (String)value;
		} else if (targetField == Field.UNIQ_KEY) {
			this.uniqKey = (String)value;
		} else if (targetField == Field.NAME_L0) {
			this.nameL0 = (String)value;
		} else if (targetField == Field.NAME_L1) {
			this.nameL1 = (String)value;
		} else if (targetField == Field.NAME_L2) {
			this.nameL2 = (String)value;
		} else if (targetField == Field.COMPANY_ID) {
			this.companyId = (Id)value;
		} else if (targetField == Field.PARENT_ID) {
			this.parentId = (Id)value;
		} else if (targetField == Field.DESCRIPTION_L0) {
			this.descriptionL0 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L1) {
			this.descriptionL1 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L2) {
			this.descriptionL2 = (String)value;
		} else if (targetField == Field.ORDER) {
			this.order = (Integer)value;

		} else {
			// ここにきたらバグ
			throw new App.UnsupportedException('ExpTypeGroupEntity.setFieldValue: 未対応の項目です。targetField=' + targetField);
		}
	}

	/**
	 * 子の費目・費目グループが存在するかどうかをチェックする
	 * @return 子が存在する場合はtrue、しない場合はfalse
	 */
	public Boolean hasChildren() {
		if ((this.childGroupList != null && !this.childGroupList.isEmpty()) ||
			(this.childExpTypeList != null && !this.childExpTypeList.isEmpty()))
		{
			return true;
		}
		return false;
	}
}