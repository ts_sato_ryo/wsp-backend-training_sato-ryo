/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 拡張項目のリソースクラス
 */
public with sharing class ExtendedItemResource {

	/** テキスト改行記号 */
	@TestVisible
	private static final String LINE_BREAK = '\n';

	/**
	 * 拡張項目パラメータ
	 */
	public virtual class ExtendedItemParam implements RemoteApi.RequestParam {
		/** 拡張項目ID */
		public String id;
		/** 拡張項目名(翻訳) */
		public String name;
		/** 拡張項目名(L0) */
		public String name_L0;
		/** 拡張項目名(L1) */
		public String name_L1;
		/** 拡張項目名(L2) */
		public String name_L2;
		/** 拡張項目コード */
		public String code;
		/** 会社ID */
		public String companyId;
		/** 入力タイプ */
		public String inputType;
		/** 最大文字数 */
		public String limitLength;
		/** 初期値(テキスト) */
		public String defaultValueText;
		/** 選択リスト(ラベルL0) */
		public String picklistLabel_L0;
		/** 選択リスト(ラベルL1) */
		public String picklistLabel_L1;
		/** 選択リスト(ラベルL2) */
		public String picklistLabel_L2;
		/** 選択リスト(値) */
		public String picklistValue;
		/** Custom Extended Item Id*/
		public String extendedItemCustomId;
		/** Custom Extended Item Lookup Field */
		public LookupField extendedItemCustom;
		/** 説明(翻訳) */
		public String description;
		/** 説明(L0) */
		public String description_L0;
		/** 説明(L1) */
		public String description_L1;
		/** 説明(L2) */
		public String description_L2;

		/** パラメータを検証する */
		public virtual void validate() {
			// 会社ID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}

			if (String.isNotBlank(this.extendedItemCustomId)) {
				try {
					System.Id.valueOf(this.extendedItemCustomId);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'extendedItemCustomId'}));
				}
			}
		}

		/**
		 * リクエストパラメータから拡張項目エンティティを作成する
		 */
		public virtual ExtendedItemEntity createExtendedItem(Map<String, Object> paramMap) {
			String[] labelList_L0;
			String[] labelList_L1;
			String[] labelList_L2;
			String[] valueList;

			ExtendedItemEntity entity = new ExtendedItemEntity();
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('name_L0')) {
				entity.name = this.name_L0;
				entity.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('inputType')) {
				entity.inputType = ExtendedItemInputType.valueOf(this.inputType);
			}
			if (paramMap.containsKey('limitLength')) {
				entity.limitLength = AppConverter.intValue(this.limitLength);
			}
			if (paramMap.containsKey('defaultValueText')) {
				entity.defaultValueText = this.defaultValueText;
			}
			if (paramMap.containsKey('picklistLabel_L0')) {
				if (String.isNotBlank(this.picklistLabel_L0)) {
					labelList_L0 = this.picklistLabel_L0.trim().split(LINE_BREAK);
				}
			}
			if (paramMap.containsKey('picklistLabel_L1')) {
				if (String.isNotBlank(this.picklistLabel_L1)) {
					labelList_L1 = this.picklistLabel_L1.trim().split(LINE_BREAK);
				}
			}
			if (paramMap.containsKey('picklistLabel_L2')) {
				if (String.isNotBlank(this.picklistLabel_L2)) {
					labelList_L2 = this.picklistLabel_L2.trim().split(LINE_BREAK);
				}
			}
			if (paramMap.containsKey('picklistValue')) {
				if (String.isNotBlank(this.picklistValue)) {
					valueList = this.picklistValue.split(LINE_BREAK);
				}
			}
			if (paramMap.containsKey('extendedItemCustomId')) {
				if (String.isNotBlank(this.extendedItemCustomId)) {
					entity.extendedItemCustomId = this.extendedItemCustomId;
				}
			}
			if (paramMap.containsKey('description_L0')) {
				entity.descriptionL0 = this.description_L0;
			}
			if (paramMap.containsKey('description_L1')) {
				entity.descriptionL1 = this.description_L1;
			}
			if (paramMap.containsKey('description_L2')) {
				entity.descriptionL2 = this.description_L2;
			}

			// 入力タイプごとの処理
			if (entity.inputType == ExtendedItemInputType.TEXT) {
				if (entity.limitLength == null) {
					entity.limitLength = ExtendedItemEntity.LIMIT_LENGTH_MAX_VALUE;
				}
				entity.picklist = null;
				entity.extendedItemCustomId = null;
			} else if (entity.inputType == ExtendedItemInputType.PICKLIST) {
				entity.picklist = createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
				entity.limitLength = null;
				entity.defaultValueText = null;
				entity.extendedItemCustomId = null;
			} else if (entity.inputType == ExtendedItemInputType.CUSTOM) {
				entity.picklist = null;
				entity.limitLength = null;
				entity.defaultValueText = null;
			} else if (entity.inputType == ExtendedItemInputType.DATE_TYPE) {
				entity.picklist = null;
				entity.limitLength = null;
				entity.defaultValueText = null;
				entity.extendedItemCustomId = null;
			}

			return entity;
		}

		/**
		 * 選択リストを作成する Create picklist
		 * @param labelList_L0 ラベル(L0)の配列 Array of label(L0)
		 * @param labelList_L1 ラベル(L1)の配列 Array of label(L1)
		 * @param labelList_L2 ラベル(L2)の配列 Array of label(L2)
		 * @param valueList 値の配列 Array of value
		 * @return 作成された選択リストデータ Created picklist
		 */
		@TestVisible
		private List<ExtendedItemEntity.PicklistOption> createPicklist(
				String[] labelList_L0, String[] labelList_L1, String[] labelList_L2, String[] valueList) {

			Integer maxSize = 0;
			labelList_L0 = removeEmpty(labelList_L0);
			labelList_L1 = removeEmpty(labelList_L1);
			labelList_L2 = removeEmpty(labelList_L2);
			valueList = removeEmpty(valueList);

			Integer labelL0Size = labelList_L0 == null ? 0 : labelList_L0.size();
			// ラベル(L0)が設定されていなかったらエラー
			if (labelL0Size == 0) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Admin_Lbl_PickListLabel}));
			}
			maxSize = labelL0Size > maxSize ? labelL0Size : maxSize;
			Integer labelL1Size = labelList_L1 == null ? 0 : labelList_L1.size();
			maxSize = labelL1Size > maxSize ? labelL1Size : maxSize;
			Integer labelL2Size = labelList_L2 == null ? 0 : labelList_L2.size();
			maxSize = labelL2Size > maxSize ? labelL2Size : maxSize;
			Integer valueSize = valueList == null ? 0 : valueList.size();
			maxSize = valueSize > maxSize ? valueSize : maxSize;
			// 値が設定されていなかったらエラー
			if (valueSize == 0) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Admin_Lbl_PickListValue}));
			}
			// 値が重複していたらエラー
			checkDupulicateValues(valueList, ComMessage.msg().Admin_Lbl_PickListValue);

			// ラベル、値の数が合っていなかったらエラー
			if (labelL0Size != maxSize || valueSize != maxSize) {
				throw new App.ParameterException(ComMessage.msg().Admin_Err_DifferentSizePicklist);
			}

			// 上限よりも多く指定されていたらエラー
			if (valueSize > ExtendedItemEntity.PICKLIST_MAX_SIZE) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_MaxSizeOver(
						new List<String>{ComMessage.msg().Admin_Lbl_PickListValue, ExtendedItemEntity.PICKLIST_MAX_SIZE.format()}));
			}

			List<ExtendedItemEntity.PicklistOption> picklist = new List<ExtendedItemEntity.PicklistOption>();
			for (Integer i = 0; i < maxSize; i++) {
				ExtendedItemEntity.PicklistOption option = new ExtendedItemEntity.PicklistOption();
				// ラベル(L0)
				if (i < labelL0Size) {
					// 文字数オーバーの場合はエラー
					checkLengthOver(labelList_L0[i], ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH, ComMessage.msg().Admin_Lbl_PickListLabel);
					option.label_L0 = labelList_L0[i];
				} else {
					option.label_L0 = null;
				}
				// ラベル(L1)
				if (i < labelL1Size) {
					// 文字数オーバーの場合はエラー
					checkLengthOver(labelList_L1[i], ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH, ComMessage.msg().Admin_Lbl_PickListLabel);
					option.label_L1 = labelList_L1[i];
				} else {
					option.label_L1 = null;
				}
				// ラベル(L2)
				if (i < labelL2Size) {
					// 文字数オーバーの場合はエラー
					checkLengthOver(labelList_L2[i], ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH, ComMessage.msg().Admin_Lbl_PickListLabel);
					option.label_L2 = labelList_L2[i];
				} else {
					option.label_L2 = null;
				}
				// 値
				if (i < valueSize) {
					// 文字数オーバーの場合はエラー
					checkLengthOver(valueList[i], ExtendedItemEntity.PICKLIST_VALUE_MAX_LENGTH, ComMessage.msg().Admin_Lbl_PickListValue);
					option.value = valueList[i];
				} else {
					option.value = null;
				}

				picklist.add(option);
			}

			return picklist;
		}

		/**
		 * 文字配列から空白を取り除く Remove empty elements from string array
		 * @param strArrayt 文字配列 Array of String
		 * @return 空白が取り除かれた配列 Empty removed array
		 */
		private String[] removeEmpty(String[] strArray) {
			List<String> strList = new List<String>();

			if (strArray == null || strArray.size() == 0) {
				return null;
			}

			for (Integer i = 0; i < strArray.size(); i++) {
				if (String.isNotBlank(strArray[i])) {
					strList.add(strArray[i]);
				}
			}

			return strList.isEmpty() ? null : (String[])strList;
		}

		/**
		 * 文字配列中に重複がないかをチェックする Check array for duplication
		 * @param strArrayt 文字配列 Array of String
		 * @param fieldName 項目名 Field name
		 */
		private void checkDupulicateValues(String[] strArray, String fieldName) {
			Set<String> strSet = new Set<String>();

			for (Integer i = 0; i < strArray.size(); i++) {
				if (strSet.contains(strArray[i])) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateValue(new List<String>{fieldName}));
				} else {
					strSet.add(strArray[i]);
				}
			}
		}

		/**
		 * 文字数が上限をオーバーしていないかをチェックする Check if string length is over limit
		 * @param str 対象の文字列 Target string
		 * @param maxLength 上限文字数 Max length of string
		 * @param fieldName 項目名 Field name
		 */
		private void checkLengthOver(String str, Integer maxLength, String fieldName) {
			if (String.isNotBlank(str) && str.length() > maxLength) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{fieldName, maxLength.format()}));
			}
		}
	}


	/**
	 * 登録APIのリクエストクラス
	 */
	public class CreateRequest extends ExtendedItemParam implements RemoteApi.RequestParam {
		/**
		 * パラメータを検証する
		 */
		public override void validate() {

			// LimitLength
			if (String.isNotBlank(this.LimitLength)) {
				AppParser.validateParseInteger(ComMessage.msg().Admin_Lbl_LimitLength, this.LimitLength.trim(), ExtendedItemEntity.DESCRIPTION_MAX_LENGTH);
			}
				super.validate();
		}
	}

	/**
	 * 登録APIのレスポンスクラス
	 */
	public class CreateResponse implements RemoteApi.ResponseParam {
		/** 拡張項目ID */
		public String id;
	}

	/**
	 * 拡張項目登録API
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EXTENDED_ITEM;

		/**
		 * @description 拡張項目を1件登録する
		 * @param request リクエストパラメータ(CreateRequest)
		 * @return 登録結果(CreateResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 入力値チェック
			CreateRequest param = (CreateRequest)request.getParam(CreateRequest.class);
			param.validate();
			Map<String, Object> paramMap = request.getParamMap();

			// 登録
			ExtendedItemEntity extendedItem = param.createExtendedItem(paramMap);
			Id resultId = new ExtendedItemService().saveExtendedItem(extendedItem);

			// レスポンス作成
			CreateResponse res = new CreateResponse();
			res.id = resultId;
			return res;
		}
	}


	/**
	 * 更新APIのリクエストクラス
	 */
	public class UpdateRequest extends ExtendedItemParam implements RemoteApi.RequestParam {

		/**
		 * パラメータを検証する
		 */
		public override void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}

			// LimitLength
			if (String.isNotBlank(this.LimitLength)) {
				AppParser.validateParseInteger(ComMessage.msg().Admin_Lbl_LimitLength, this.LimitLength.trim(), ExtendedItemEntity.DESCRIPTION_MAX_LENGTH);
			}
				super.validate();
			}

		/**
		 * リクエストパラメータから拡張項目エンティティを作成する
		 */
		public override ExtendedItemEntity createExtendedItem(Map<String, Object> paramMap) {
			ExtendedItemEntity entity = super.createExtendedItem(paramMap);
			if (paramMap.containsKey('id')) {
				entity.setId(this.id);
			}
			return entity;
		}
	}

	/**
	 * 拡張項目更新API
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EXTENDED_ITEM;

		/**
		 * @description 拡張項目を1件登録する
		 * @param request リクエストパラメータ(UpdateRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 入力値チェック
			UpdateRequest param = (UpdateRequest)request.getParam(UpdateRequest.class);
			param.validate();
			Map<String, Object> paramMap = request.getParamMap();

			// 更新
			ExtendedItemEntity extendedItem = param.createExtendedItem(paramMap);
			Id resultId = new ExtendedItemService().saveExtendedItem(extendedItem);

			return null;
		}
	}


	/**
	 * @description 削除APIのリクエストパラメータ
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象ベースレコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}
	}

	/**
	 * 拡張項目削除API
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EXTENDED_ITEM;

		/**
		 * @description 拡張項目を1件削除する
		 * @param request リクエストパラメータ(DeleteRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 入力値チェック
			DeleteRequest param = (DeleteRequest)request.getParam(DeleteRequest.class);
			param.validate();

			try {
				// レコードを削除
				new ExtendedItemService().deleteExtendedItem(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合は成功レスポンスを返す
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					 throw e;
				}
			}
			return null;
		}
	}


	/**
	 * @description 検索APIのリクエストパラメータ
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** 拡張項目ID */
		public String id;
		/** 会社ID */
		public String companyId;
		/** 入力タイプ */
		public String inputType;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// 会社ID
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.valueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}));
				}
			}

			// 入力タイプ
			if (String.isNotBlank(this.inputType)) {
				if (ExtendedItemInputType.valueOf(this.inputType) == null) {
					throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'inputType'}));
				}
			}
		}
	}

	/**
	 * @description 検索APIのリクエストパラメータ
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** 拡張項目リスト */
		public List<ExtendedItemParam> records;
	}

	/*
	 * Class representing a lookup field.
	 */
	public class LookupField {
		public String name;
	}

	/**
	 * 拡張項目検索API
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 拡張項目を1件登録する
		 * @param request リクエストパラメータ(SearchRequest)
		 * @return レスポンス(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request request) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchRequest param = (SearchRequest)request.getParam(SearchRequest.class);
			Map<String, Object> paramMap = request.getParamMap();
			param.validate();

			// パラメータ id が指定されている場合は、idで検索
			Id itemId;
			if (paramMap.containsKey('id')) {
				itemId = param.id;
			}
			// パラメータ companyId が指定されている場合は、会社IDで検索
			Id companyId;
			if (paramMap.containsKey('companyId')) {
				companyId = param.companyId;
			}
			// パラメータ inputType が指定されている場合は、入力タイプで検索
			ExtendedItemInputType inputType;
			if (paramMap.containsKey('inputType')) {
				inputType = ExtendedItemInputType.valueOf(param.inputType);
			}

			// 検索する
			List<ExtendedItemEntity> items = new ExtendedItemService().searchExtendedItems(companyId, itemId, inputType);

			// レスポンス作成
			List<ExtendedItemParam> records = new List<ExtendedItemParam>();
			for (ExtendedItemEntity item : items) {
				records.add(craeteExtendedItemParam(item));
			}
			SearchResponse res = new SearchResponse();
			res.records = records;

			return res;
		}

		/**
		 * 検索結果からレスポンスパラメータの拡張項目を作成する
		 */
		private ExtendedItemParam craeteExtendedItemParam(ExtendedItemEntity entity) {
			ExtendedItemParam param = new ExtendedItemParam();

			param.id = entity.id;
			param.name = entity.nameL.getValue();
			param.name_L0 = entity.nameL0;
			param.name_L1 = entity.nameL1;
			param.name_L2 = entity.nameL2;
			param.code = entity.code;
			param.companyId = entity.companyId;
			param.extendedItemCustomId = entity.extendedItemCustomId;
			param.extendedItemCustom = new LookupField();
			if (entity.extendedItemCustomName != null) {
				param.extendedItemCustom.name = entity.extendedItemCustomName.getValue();
			}
			param.inputType = ExtendedItemInputType.getValue(entity.inputType);
			param.limitLength = String.valueOf(entity.limitLength);
			param.defaultValueText = entity.defaultValueText;
			param.description = entity.descriptionL.getValue();
			param.description_L0 = entity.descriptionL0;
			param.description_L1 = entity.descriptionL1;
			param.description_L2 = entity.descriptionL2;
			// 選択リスト情報を作成 Create picklist info
			String picklistLabel_L0 = '';
			String picklistLabel_L1 = '';
			String picklistLabel_L2 = '';
			String picklistValue = '';
			if (entity.picklist != null) {
				for (Integer i = 0; i < entity.picklist.size(); i++) {
					String lineBreak = i > 0 ? LINE_BREAK : '';
					picklistLabel_L0 += lineBreak + (String.isBlank(entity.picklist[i].label_L0) ? '' : entity.picklist[i].label_L0);
					picklistLabel_L1 += lineBreak + (String.isBlank(entity.picklist[i].label_L1) ? '' : entity.picklist[i].label_L1);
					picklistLabel_L2 += lineBreak + (String.isBlank(entity.picklist[i].label_L2) ? '' : entity.picklist[i].label_L2);
					picklistValue += lineBreak + (String.isBlank(entity.picklist[i].value) ? '' : entity.picklist[i].value);
				}

				param.picklistLabel_L0 = picklistLabel_L0;
				param.picklistLabel_L1 = picklistLabel_L1;
				param.picklistLabel_L2 = picklistLabel_L2;
				param.picklistValue = picklistValue;
			}

			return param;
		}
	}
}