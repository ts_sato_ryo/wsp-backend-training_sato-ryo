/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数確定申請のリポジトリ
 */
public with sharing class TimeRequestRepository extends Repository.BaseRepository {

	/** リレーション名（工数サマリー） */
	private static final String SUMMARY_R =
			TimeRequest__c.TimeSummaryId__c.getDescribe().getRelationshipName();

	/** リレーション名（社員） */
	private static final String EMP_R =
			TimeRequest__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	private static final String EMP_BASE_R =
			ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();

	/** リレーション名（ユーザ） */
	private static final String USER_R =
			ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();

	/** リレーション名（部署） */
	private static final String DEPT_R =
			TimeRequest__c.DepartmentHistoryId__c.getDescribe().getRelationshipName();

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			TimeRequest__c.Id,
			TimeRequest__c.Name,
			TimeRequest__c.TimeSummaryId__c,
			TimeRequest__c.Comment__c,
			TimeRequest__c.ProcessComment__c,
			TimeRequest__c.CancelComment__c,
			TimeRequest__c.Status__c,
			TimeRequest__c.ConfirmationRequired__c,
			TimeRequest__c.CancelType__c,
			TimeRequest__c.RequestTime__c,
			TimeRequest__c.EmployeeHistoryId__c,
			TimeRequest__c.DepartmentHistoryId__c,
			TimeRequest__c.ActorHistoryId__c,
			TimeRequest__c.LastApproveTime__c,
			TimeRequest__c.LastApproverId__c,
			TimeRequest__c.Approver01Id__c,
			TimeRequest__c.OwnerId
		};
	}

	/** 検索フィルタ */
	 private class Filter {
		/** 工数確定申請ID */
		public List<Id> requestIdList;
	 }


	/**
	 * 工数確定申請を取得する
	 * @param requestId 工数確定申請ID
	 * @return 工数確定申請エンティティ
	 */
	public TimeRequestEntity getTimeRequest(Id requestId) {
		// 検索実行
		List<TimeRequestEntity> requestList = getTimeRequestList(new List<Id>{requestId});

		TimeRequestEntity request = null;
		if (requestList != null && !requestList.isEmpty()) {
			request = requestList[0];
		}

		return request;
	}

	/**
	 * 工数確定申請を取得する
	 * @param requestIdList 工数確定申請IDのリスト
	 * @return 条件に一致した工数確定申請エンティティのリスト
	 */
	public List<TimeRequestEntity> getTimeRequestList(List<Id> requestIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.requestIdList = requestIdList;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchTimeRequest(filter, forUpdate);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(TimeRequestEntity entity) {
		return saveEntityList(new List<TimeRequestEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<TimeRequestEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<TimeRequestEntity> entityList, Boolean allOrNone) {
		List<TimeRequest__c> timeRequestList = new List<TimeRequest__c>();

		// エンティティからSObjectを作成する
		for (TimeRequestEntity entity : entityList) {
			TimeRequest__c timeRequest = entity.createSObject();
			timeRequestList.add(timeRequest);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(timeRequestList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 工数確定申請を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した工数確定申請のリスト
	 */
	 private List<TimeRequestEntity> searchTimeRequest(Filter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}
		// 工数サマリー
		selectFldList.add(SUMMARY_R + '.' + getFieldName(TimeSummary__c.SummaryName__c));
		selectFldList.add(SUMMARY_R + '.' + getFieldName(TimeSummary__c.StartDate__c));
		selectFldList.add(SUMMARY_R + '.' + getFieldName(TimeSummary__c.EndDate__c));
		// 社員
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.FirstName_L0__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.LastName_L0__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.FirstName_L1__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.LastName_L1__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.FirstName_L2__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.LastName_L2__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + USER_R + '.' + getFieldName(User.SmallPhotoUrl));
		// 部署
		selectFldList.add(DEPT_R + '.' + getFieldName(ComDeptHistory__c.Name_L0__c));
		selectFldList.add(DEPT_R + '.' + getFieldName(ComDeptHistory__c.Name_L1__c));
		selectFldList.add(DEPT_R + '.' + getFieldName(ComDeptHistory__c.Name_L2__c));

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> requestIds = filter.requestIdList;
		if ((requestIds != null) && (! requestIds.isEmpty())) {
			condList.add(getFieldName(TimeRequest__c.Id) + ' IN :requestIds');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + TimeRequest__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(TimeRequest__c.RequestTime__c) + ' Asc';
		}

		// クエリ実行
		System.debug('TimeRequestRepository.searchTimeRequest: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<TimeRequestEntity> entityList = new List<TimeRequestEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(new TimeRequestEntity((TimeRequest__c)sobj));
		}

		return entityList;
	 }
}
