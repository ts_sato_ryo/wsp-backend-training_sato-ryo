/**
 * AttTimeのテストクラス
 */
@isTest
private class AttTimeTest {

	/**
	 * コンストラクタのテスト
	 */
	@isTest private static void constructorTest() {
		Integer value = 123;
		AttTime atTime = new AttTime(value);
		System.assertEquals(value, atTime.getIntValue());
	}

	/**
	 * convertIntのテスト
	 */
	@isTest private static void convertIntTest() {
		// 値が設定されている場合
		Integer value = 123;
		AttTime atTime = new AttTime(value);
		System.assertEquals(value, AttTime.convertInt(atTime));

		// Nullの場合
		System.assertEquals(null, AttTime.convertInt(null));

	}

	/**
	 * equalsのテスト
	 */
	@isTest private static void equalsTest() {
		Integer value = 123;
		AttTime atTime = new AttTime(value);

		// 型が異なる場合 -> 等しくない
		System.assertEquals(false, atTime.equals(value));

		// 型は同じで値が異なる場合 -> 等しくない
		AttTime compare = new AttTime(value + 1);
		System.assertEquals(false, atTime.equals(compare));

		// 型も値も同じ場合 -> 等しい
		compare = new AttTime(value);
		System.assertEquals(true, atTime.equals(compare));

	}

	/**
	 * addHoursのテスト
	 */
	@isTest private static void addHoursTest() {
		Integer value = 9 * 60 + 12; // 9:12
		Integer addHours = 2;
		AttTime atTime = new AttTime(value);
		System.assertEquals(value + addHours * 60, atTime.addHours(addHours).getIntValue());
	}

	/**
	 * getHourのテスト
	 */
	@isTest private static void getHourTest() {
		Integer value = 9 * 60 + 12; // 9:12
		AttTime atTime = new AttTime(value);
		System.assertEquals(9, atTime.getHour());

		value = 0 * 60 + 59; // 0:59
		atTime = new AttTime(value);
		System.assertEquals(0, atTime.getHour());

		value = null;
		atTime = new AttTime(value);
		System.assertEquals(null, atTime.getHour());
	}

	/**
	 * getMinuteのテスト
	 */
	@isTest private static void getMinuteTest() {
		Integer value = 9 * 60 + 12; // 9:12
		AttTime atTime = new AttTime(value);
		System.assertEquals(12, atTime.getMinute());

		value = 9 * 60 + 0; // 9:00
		atTime = new AttTime(value);
		System.assertEquals(0, atTime.getMinute());

		value = null;
		atTime = new AttTime(value);
		System.assertEquals(null, atTime.getMinute());
	}

	/**
	 * formatのテスト
	 */
	@isTest private static void formatTest() {
		Integer value = 9 * 60 + 2; // 09:02
		AttTime atTime = new AttTime(value);
		System.assertEquals('09:02', atTime.format());

		value = 23 * 60 + 59; // 23:59
		atTime = new AttTime(value);
		System.assertEquals('23:59', atTime.format());

		value = -1 * 60 - 02; // -01:02 (前日)
		atTime = new AttTime(value);
		System.assertEquals('-01:02', atTime.format());

		value = null;
		atTime = new AttTime(value);
		System.assertEquals(null, atTime.format());
	}

	/**
	 * toStringのテスト
	 */
	@isTest private static void toStringTest() {
		Integer value = 123;
		AttTime atTime = new AttTime(value);
		System.assertEquals(value.format(), atTime.toString());
	}
}
