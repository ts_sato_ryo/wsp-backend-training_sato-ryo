/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 勤務体系サービスのテストクラス
 */
@isTest
private class AttWorkingTypeServiceTest {

	/**
	 * テストデータ
	 */
	private class ServiceTestData extends TestData.TestDataEntity {

	}

	/**
	 * ベースエンティティが更新できることを確認する
	 */
	@isTest static void updateBaseTest() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();

		// 更新内容を設定する
		AttWorkingTypeBaseEntity updateBase = new AttWorkingTypeBaseEntity();
		updateBase.setId(testData.workingType.id);
		updateBase.startMonthOfYear = 12;
		updateBase.workSystem = AttWorkSystem.JP_Manager;

		new AttWorkingTypeService().updateBase(updateBase);

		// 更新されていることを確認する
		AttWorkingTypeBaseEntity resBase = new AttWorkingTypeRepository().getBaseEntity(updateBase.id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(updateBase.startMonthOfYear, resBase.startMonthOfYear);
		System.assertEquals(updateBase.workSystem, resBase.workSystem);
		// 他の項目は更新されていない(更新前の値のまま)
		System.assertEquals(testData.workingType.startDateOfMonth, resBase.startDateOfMonth);
		System.assertEquals(testData.workingType.startDateOfWeek, resBase.startDateOfWeek);
	}

	/**
	 * 履歴が改定できることを確認する
	 * 改定内容が伝搬されることを確認する
	 */
	@isTest static void reviseHistoryTest() {
		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();

		final Integer baseNum = 1;
		final Integer historyNum = 5;
		AttWorkingTypeBaseEntity base = testData.createWorkingTypeList('伝搬テスト', testData.leaveList, baseNum, historyNum).get(0);

		AppDate validFrom = base.getHistory(1).validFrom.addDays(10);
		AppDate validTo = validFrom.addDays(10);
		AttWorkingTypeHistoryEntity targetHistory = new AttWorkingTypeHistoryEntity();
		targetHistory.baseId = base.id;
		targetHistory.validFrom = validFrom;
		targetHistory.validTo = validTo;
		targetHistory.historyComment = '改定履歴コメント'; // 履歴管理項目は伝搬されないはず
		targetHistory.nameL0 = targetHistory.nameL0 + '_改定後';
		targetHistory.startTime = base.getHistory(1).startTime.addHours(1);
		targetHistory.directApplyStartTime = base.getHistory(1).directApplyStartTime.addHours(1);

		// 末尾の履歴のStartTimeを変更しておく
		base.getHistory(historyNum - 1).startTime = targetHistory.startTime.addHours(2);
		// 末尾の履歴の直行直帰申請を変更しておく
		base.getHistory(historyNum - 1).directApplyStartTime = targetHistory.directApplyStartTime.addHours(2);
		repo.saveHistoryEntity(base.getHistory(historyNum - 1));

		// 改定実行
		AttWorkingTypeService service = new AttWorkingTypeService();
		service.reviseHistory(targetHistory);


		List<AttWorkingTypeHistoryEntity> resHistoryList = repo.getEntity(base.id).getHistoryList();
		System.assertEquals(historyNum + 1, resHistoryList.size());
		// 改定されていることを確認
		System.assertEquals(validFrom, resHistoryList[1].validTo); // 改定前履歴
		System.assertEquals(validFrom, resHistoryList[2].validFrom); // 改定後履歴

		// // デバッグ
		// System.debug('>>> 改定後の履歴');
		// for (DepartmentHistoryEntity history : resHistoryList) {
		// 	System.debug(history.name + '(' + history.validFrom + '-' + history.validTo + '): managerId = ' + history.managerId + ', remarks =' + history.remarks);
		// }

		// 伝搬されていることを確認
		// 末尾の履歴のStartTimeはすでに変更されているので伝搬対象外
		for (Integer i = 3; i < historyNum; i++) {
			System.assertEquals(targetHistory.nameL0, resHistoryList[i].nameL0);
			System.assertEquals(targetHistory.startTime, resHistoryList[i].startTime);
			System.assertEquals(targetHistory.directApplyStartTime, resHistoryList[i].directApplyStartTime);

			// 履歴管理項目は伝搬されない
			System.assertEquals(base.getHistory(i - 1).historyComment, resHistoryList[i].historyComment);

		}

		// 改定前の履歴には伝搬されていない
		for (Integer i = 0; i <= 1; i++) {
			System.assertEquals(base.getHistory(i).nameL0, resHistoryList[i].nameL0);
			System.assertEquals(base.getHistory(i).directApplyStartTime, resHistoryList[i].directApplyStartTime);
		}

		// 末尾の履歴の部署はすでに変更されているので伝搬されない
		Integer tailIdx = resHistoryList.size() - 1;
		System.assertEquals(base.getHistory(historyNum - 1).startTime, resHistoryList[tailIdx].startTime);
		System.assertEquals(base.getHistory(historyNum - 1).directApplyStartTime, resHistoryList[tailIdx].directApplyStartTime);
		// 勤務体系名L0は伝搬されている
		System.assertEquals(targetHistory.nameL0, resHistoryList[tailIdx].nameL0);
		// 勤務体系名L1,L2の値は変更されていない
		System.assertEquals(base.getHistory(historyNum - 1).nameL1, resHistoryList[tailIdx].nameL1);
		System.assertEquals(base.getHistory(historyNum - 1).nameL2, resHistoryList[tailIdx].nameL2);

		// 伝搬対象の履歴の元の履歴は論理削除されている
		List<AttWorkingTypeHistory__c> removedList = [SELECT Id, ValidFrom__c FROM AttWorkingTypeHistory__c WHERE BaseId__c = :base.Id AND Removed__c = true];
		// for (ComDeptHistory__c history : removedList) {
		// 	System.debug('Removed = ' + history.ValidFrom__c);
		// }
		System.assertEquals(historyNum - 2, removedList.size());

	}

	/**
	 * 勤務体系ベースのコードの重複チェックが正しくできることを確認する
	 */
	@isTest static void isCodeDuplicatedTest() {
		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();

		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<AttWorkingTypeBaseEntity> bases = testData.createWorkingTypeList('伝搬テスト', testData.leaveList, baseNum, historyNum);
		AttWorkingTypeBaseEntity targetBase = bases[1];

		AttWorkingTypeService service = new AttWorkingTypeService();

		// 会社内で重複している → true
		targetBase.code = bases[0].code;
		System.assertEquals(true, service.isCodeDuplicated(targetBase));

		// 会社をまたいで重複している → false
		ComCompany__c ompanyObj2 = ComTestDataUtility.createCompany('Test2', testData.country.id);
		targetBase.companyId = ompanyObj2.Id;
		targetBase.code = bases[0].code;
		System.assertEquals(false, service.isCodeDuplicated(targetBase));

		// 組織全体でも重複していない → false
		targetBase.code = targetBase.code + 'Test';
		System.assertEquals(false, service.isCodeDuplicated(targetBase));
	}

	/**
	 * updateUniqKeyのテスト
	 * 会社IDまたはコードが変更されている場合はユニークキーが更新されることを確認する
	 */
	@isTest static void updateUniqKeyTestUpdateUniqKey() {
		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		CompanyEntity testCompany = testData.createCompany('更新テスト');

		final Integer baseNum = 2;
		final Integer historyNum = 1;
		List<AttWorkingTypeBaseEntity> bases = testData.createWorkingTypeList(
				'更新テスト', testData.leaveList, baseNum, historyNum);

		AttWorkingTypeService service = new AttWorkingTypeService();
		AttWorkingTypeBaseEntity targetBase;

		// 会社IDが更新されている
		targetBase = bases[0];
		targetBase.companyId = testCompany.id;
		service.updateUniqKey(targetBase);
		System.assertEquals(testCompany.code + '-' + targetBase.code, targetBase.uniqKey);

		// コードが更新されている
		targetBase = bases[1];
		targetBase.code = 'Changed';
		service.updateUniqKey(targetBase);
		System.assertEquals(testData.company.code + '-' + targetBase.code, targetBase.uniqKey);
	}

	/**
	 * updateUniqKeyのテスト
	 * 更新対象の勤務体系が削除されてしまっている場合、想定した例外が発生することを確認する
	 */
	@isTest static void updateUniqKeyTestNotFoundRecord() {
		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		CompanyEntity testCompany = testData.createCompany('更新テスト');

		final Integer baseNum = 2;
		final Integer historyNum = 1;
		List<AttWorkingTypeBaseEntity> bases = testData.createWorkingTypeList(
				'更新テスト', testData.leaveList, baseNum, historyNum);

		AttWorkingTypeService service = new AttWorkingTypeService();
		AttWorkingTypeBaseEntity targetBase;

		// 更新対象の勤務体系を削除しておく
		targetBase = bases[1];
		new AttWorkingTypeRepository().deleteEntity(targetBase);

		// 更新実行
		try {
			targetBase.code = 'Changed';
			service.updateUniqKey(targetBase);
		} catch (App.RecordNotFoundException e) {
			System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, e.getMessage());
		}
	}

	/**
	 * updateUniqKeyのテスト
	 * 会社ID、コードが設定されていない場合、想定されている例外が発生することを確認する
	 */
	@isTest static void updateUniqKeyTestInvalidValue() {
		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		CompanyEntity testCompany = testData.createCompany('更新テスト');

		final Integer baseNum = 2;
		final Integer historyNum = 1;
		List<AttWorkingTypeBaseEntity> bases = testData.createWorkingTypeList(
				'更新テスト', testData.leaveList, baseNum, historyNum);

		AttWorkingTypeService service = new AttWorkingTypeService();
		AttWorkingTypeBaseEntity targetBase;

		// 会社IDがnull
		targetBase = bases[0];
		try {
			targetBase.companyId = null;
			service.updateUniqKey(targetBase);
		} catch (App.ParameterException e) {
			System.assertEquals(
					ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Company}),
					e.getMessage());
		}

		// コードが空
		targetBase = bases[1];
		try {
			targetBase.code = '';
			service.updateUniqKey(targetBase);
		} catch (App.ParameterException e) {
			System.assertEquals(
					ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}),
					e.getMessage());
		}
	}

	/**
	 * reviseHistoryWithValidation のテスト
	 * 改定可能かの検証が行なわれていることと改定が行われていることを確認する
	 */
	@isTest static void reviseHistoryWithValidationTest() {
		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		CompanyEntity testCompany = testData.createCompany('更新テスト');

		final Integer baseNum = 1;
		final Integer historyNum = 1;
		AttWorkingTypeBaseEntity base = testData.createWorkingTypeList(
				'Test', testData.leaveList, baseNum, historyNum).get(0);

		AttWorkingTypeService service = new AttWorkingTypeService();
		AttWorkingTypeHistoryEntity revisionHistory = new AttWorkingTypeHistoryEntity();
		revisionHistory.baseId = base.id;

		// Test1: 検証が行われている
		App.ParameterException resEx;
		revisionHistory.validFrom =
				AppDate.newInstance(2018, 8, base.startDateOfMonth).addDays(1);
		try {
			service.reviseHistoryWithValidation(revisionHistory);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		String expMessage = ComMessage.msg().Admin_Err_InvalidRevisionDateWorkingType(
				new List<String>{ComMessage.msg().Att_Lbl_WorkingTypeBeginDayOfMonth});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test2: 改定が行われている(改定の詳細については確認しない)
		revisionHistory.validFrom =
				AppDate.newInstance(AppDate.today().year(), AppDate.today().month() + 1, base.startDateOfMonth);
		service.reviseHistoryWithValidation(revisionHistory);
		AttWorkingTypeBaseEntity resBase = new AttWorkingTypeRepository().getEntity(base.id);
		System.assertEquals(2, resBase.getHistoryList().size());
	}

	/**
	 * validateRevisionHistoryList のテスト
	 * 検証が正しく行われていることを確認する
	 */
	@isTest static void validateRevisionHistoryListTest() {
		AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		CompanyEntity testCompany = testData.createCompany('更新テスト');

		final Integer baseNum = 1;
		final Integer historyNum = 1;
		AttWorkingTypeBaseEntity base = testData.createWorkingTypeList(
				'Test', testData.leaveList, baseNum, historyNum).get(0);

		AttWorkingTypeService service = new AttWorkingTypeService();
		List<AttWorkingTypeService.ValidateResult> resultList;
		AttWorkingTypeHistoryEntity revisionHistory = new AttWorkingTypeHistoryEntity();
		String expMessage;
		revisionHistory.baseId = base.id;

		// Test1 改定日が月度の開始日と同じ場合 → 成功
		revisionHistory.validFrom = AppDate.newInstance(2018, 8, base.startDateOfMonth);
		resultList = service.validateRevisionHistoryList(new List<AttWorkingTypeHistoryEntity>{revisionHistory});
		System.assertEquals(false, resultList[0].hasError);

		// Test2 改定日が月度の開始日と異なる場合 → エラー
		revisionHistory.validFrom = AppDate.newInstance(2018, 8, base.startDateOfMonth).addDays(1);
		resultList = service.validateRevisionHistoryList(new List<AttWorkingTypeHistoryEntity>{revisionHistory});
		System.assertEquals(true, resultList[0].hasError);
		expMessage = ComMessage.msg().Admin_Err_InvalidRevisionDateWorkingType(
				new List<String>{ComMessage.msg().Att_Lbl_WorkingTypeBeginDayOfMonth});
		System.assertEquals(expMessage, resultList[0].exceptionList[0].getMessage());

		// Test3 履歴のベースIDのベースが存在しない場合 → エラー
		revisionHistory.baseId = null;
		revisionHistory.validFrom = AppDate.newInstance(2018, 8, base.startDateOfMonth);
		resultList = service.validateRevisionHistoryList(new List<AttWorkingTypeHistoryEntity>{revisionHistory});
		System.assertEquals(true, resultList[0].hasError);
		expMessage = ComMessage.msg().Com_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_WorkingType});
		System.assertEquals(expMessage, resultList[0].exceptionList[0].getMessage());
	}

	/**
	 * getWorkingTypeのテスト
	 * リポジトリのキャッシュが有効な場合、対象日が異なる場合もキャッシュが利用されていることを確認する
	 */
	@isTest static void getWorkingTypeTestEnableRepositoryCache() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 5;
		ServiceTestData testData = new ServiceTestData();
		List<AttWorkingTypeBaseEntity> testWorkingTypeList =
				testData.createWorkingTypeList('test', testData.leaveList, numberOfBase, numberOfHistory);
		AttWorkingTypeBaseEntity testWorkingType = testWorkingTypeList[0];

		Test.startTest();
		AttWorkingTypeRepository.enableCache();
		AttWorkingTypeService service = new AttWorkingTypeService();
		AttWorkingTypeBaseEntity res1 = service.getWorkingType(testWorkingType.id, testWorkingType.getHistory(0).validFrom);

		Integer startQueryCount = Limits.getQueries();
		AttWorkingTypeBaseEntity res2 = service.getWorkingType(testWorkingType.id, testWorkingType.getHistory(0).validFrom.addMonths(1));
		AttWorkingTypeBaseEntity resHistoryAll = service.getWorkingType(testWorkingType.id, null);
		Integer endQueryCount = Limits.getQueries();
		Test.stopTest();

		System.assertEquals(testWorkingType.id, res1.id);
		System.assertEquals(testWorkingType.id, res2.id);
		System.assertEquals(testWorkingType.id, resHistoryAll.id);
		System.assertEquals(numberOfHistory, resHistoryAll.getHistoryList().size());
		// キャッシュが効いているためクエリは実行されていないはず
		System.assertEquals(0, endQueryCount - startQueryCount);
	}

	/**
	 * getWorkingTypeのテスト
	 * 勤務体系が見つからない場合、想定されるアプリ例外が発生することを確認する
	 */
	@isTest static void getWorkingTypeTestNotFound() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 5;
		ServiceTestData testData = new ServiceTestData();
		List<AttWorkingTypeBaseEntity> testWorkingTypeList =
				testData.createWorkingTypeList('test', testData.leaveList, numberOfBase, numberOfHistory);
		AttWorkingTypeBaseEntity testWorkingType = testWorkingTypeList[0];
		App.RecordNotFoundException actEx;

		Test.startTest();
		AttWorkingTypeRepository.enableCache();
		AttWorkingTypeService service = new AttWorkingTypeService();
		try {
			AttWorkingTypeBaseEntity res = service.getWorkingType(UserInfo.getUserId(), AppDate.today());
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}

		TestUtil.assertNotNull(actEx, '例外が発生しませんでした。');
		String expMsg = ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Att_Lbl_WorkingType});
		System.assertEquals(expMsg, actEx.getMessage());
	}

	/**
	 * getWorkingTypeのテスト
	 * 勤務体系が失効している場合、想定される例外が発生することを確認する
	 */
	@isTest static void getWorkingTypeTesteExpire() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 1;
		ServiceTestData testData = new ServiceTestData();
		List<AttWorkingTypeBaseEntity> testWorkingTypeList =
				testData.createWorkingTypeList('test', testData.leaveList, numberOfBase, numberOfHistory);
		AttWorkingTypeBaseEntity testWorkingType = testWorkingTypeList[0];
		App.RecordNotFoundException actEx;

		Test.startTest();
		AttWorkingTypeRepository.enableCache();
		AttWorkingTypeService service = new AttWorkingTypeService();
		try {
			AttWorkingTypeBaseEntity res = service.getWorkingType(testWorkingType.id, testWorkingType.getHistory(0).validTo);
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}

		TestUtil.assertNotNull(actEx, '例外が発生しませんでした。');
		String expMsg = ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Att_Lbl_WorkingType});
		System.assertEquals(expMsg, actEx.getMessage());
	}

	/**
	 * getWorkingTypeByRangeのテスト
	 * 指定した期間の履歴が取得できることを確認する
	 */
	@isTest static void getWorkingTypeByRangeTest() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 5;
		ServiceTestData testData = new ServiceTestData();
		List<AttWorkingTypeBaseEntity> testWorkingTypeList =
				testData.createWorkingTypeList('test', testData.leaveList, numberOfBase, numberOfHistory);
		AttWorkingTypeBaseEntity testWorkingType = testWorkingTypeList[0];

		AppDate startDate = testWorkingType.getHistory(0).validFrom.addDays(2);
		AppDate endDate = testWorkingType.getHistory(1).validTo.addDays(-2);

		Test.startTest();
			AttWorkingTypeBaseEntity res =
					new AttWorkingTypeService().getWorkingTypeByRange(testWorkingType.id,  new AppDateRange(startDate, endDate));
		Test.stopTest();

		System.assertEquals(testWorkingType.id, res.id);
		System.assertEquals(2, res.getHistoryList().size());
	}

	/**
	 * getWorkingTypeByRangeのテスト
	 * 無効な期間を含む場合、想定される例外が発生することを確認する
	 */
	@isTest static void getWorkingTypeByRangeTestInvalidPeriod() {
		final Integer numberOfBase = 3;
		final Integer numberOfHistory = 5;
		ServiceTestData testData = new ServiceTestData();
		List<AttWorkingTypeBaseEntity> testWorkingTypeList =
				testData.createWorkingTypeList('test', testData.leaveList, numberOfBase, numberOfHistory);
		AttWorkingTypeBaseEntity testWorkingType = testWorkingTypeList[0];
		App.RecordNotFoundException actEx;
		String expMessage = ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Att_Lbl_WorkingType});

		// Test1 対象期間の開始日が有効期間外の場合
		actEx = null;
		try {
			AppDate startDate = testWorkingType.getHistory(0).validFrom.addDays(-1);
			AppDate endDate = testWorkingType.getHistory(1).validTo.addDays(-2);
			AttWorkingTypeBaseEntity res =
					new AttWorkingTypeService().getWorkingTypeByRange(testWorkingType.id, new AppDateRange(startDate, endDate));
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx);
		System.assertEquals(expMessage, actEx.getMessage());

		// Test2 対象期間の終了日が有効期間外の場合
		actEx = null;
		try {
			AppDate startDate = testWorkingType.getHistory(0).validFrom.addDays(-1);
			AppDate endDate = testWorkingType.getHistory(1).validTo;
			AttWorkingTypeBaseEntity res =
					new AttWorkingTypeService().getWorkingTypeByRange(testWorkingType.id, new AppDateRange(startDate, endDate));
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx);
		System.assertEquals(expMessage, actEx.getMessage());
	}
}
