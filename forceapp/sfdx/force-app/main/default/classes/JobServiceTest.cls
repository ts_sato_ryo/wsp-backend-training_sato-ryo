/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description ジョブサービスクラス
*/
@isTest
private class JobServiceTest {

	private class ServiceTestData extends TestData.TestDataEntity {

		/**
		 * ジョブエンティティを作成する(DB保存あり)
		 */
		public List<JobEntity> createJobs(String name, Integer size) {
			ComTestDataUtility.createJobs(name, this.company.id, this.jobType.id, size);

			JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
			filter.companyIds = new Set<Id>{this.company.id};

			return new JobRepository().searchEntity(filter);
		}
		/**
		 * ジョブエンティティを作成する(親ジョブ指定あり)
		 */
		public JobEntity createJob(String name, Id parentId) {
			ComTestDataUtility.createJobs(name, this.company.id, this.jobType.id, 1);
			JobEntity entity = new JobEntity();
			entity.name = name;
			entity.nameL0 = name + 'L0';
			entity.nameL1 = name + 'L1';
			entity.nameL2 = name + 'L2';
			entity.code = name;
			entity.companyId = this.company.id;
			entity.jobTypeId = this.jobType.id;
			entity.parentId = parentId;
			entity.validFrom = ValidPeriodEntity.VALID_FROM_MIN;
			entity.validTo = ValidPeriodEntity.VALID_TO_MAX;
			return entity;
		}
		/**
		 * ジョブ検索に使用するデータを作成する
		 *
		 * @return ジョブエンティティのリスト
		 * 			[0] 工数ジョブ ─ 有効期間内 ─ 全社公開
		 * 			[1]          │          └ 個別設定 ─ 割当あり ─ 割当期間内
		 * 			[2]          │                   │         └ 割当期間外（1990.1.1 ~ 2000.1.1）
		 * 			[3]          │                   └ 割当なし
		 * 			[4]          └ 有効期間外 ─ 全社公開
		 * 			[5]                     └ 個別設定 ─ 割当あり ─ 割当期間内
		 * 			[6]                              │         └ 割当期間外（1990.1.1 ~ 2000.1.1）
		 * 			[7]                              └ 割当なし
		 * 			[8] 経費ジョブ ─ 有効期間内 ─ 全社公開
		 * 			[9]          │          └ 個別設定 ─ 割当あり ─ 割当期間内
		 * 			[10]         │                   │         └ 割当期間外（1990.1.1 ~ 2000.1.1）
		 * 			[11]         │                   └ 割当なし
		 * 			[12]         └ 有効期間外 ─ 全社公開
		 * 			[13]                    └ 個別設定 ─ 割当あり ─ 割当期間内
		 * 			[14]                             │         └ 割当期間外（1990.1.1 ~ 2000.1.1）
		 * 			[15]                             └ 割当なし
		 */
		public List<JobEntity> createSearchTestData() {
			List<ComJob__c> jobs = ComTestDataUtility.createJobs('Job', this.company.id, this.jobType.id, 16);

			// 工数のジョブのインデックス
			List<Integer> timeTrackJobIndex = new List<Integer> {0, 1, 2, 3, 4, 5, 6, 7};
			// 有効期間内のジョブのインデックス
			List<Integer> validJobIndex = new List<Integer> {0, 1, 2, 3, 8, 9, 10, 11};
			// 全社公開のジョブのインデックス
			List<Integer> publicJobIndex = new List<Integer> {0, 4, 8, 12};
			// ジョブ割当済のジョブのインデックス
			List<Integer> assignedJobIndex = new List<Integer> {1, 2, 5, 6, 9, 10, 13, 14};
			// ジョブ割当が有効期間内のジョブのインデックス
			List<Integer> validAssignJobIndex = new List<Integer> {1, 5, 9, 13};

			for (Integer i = 0; i < jobs.size(); i++) {
				ComJob__c job = jobs[i];

				// 工数ジョブ or 経費ジョブ
				if (timeTrackJobIndex.contains(i)) {
					job.SelectabledExpense__c = false;
					job.SelectabledTimeTrack__c = true;
				} else {
					job.SelectabledExpense__c = true;
					job.SelectabledTimeTrack__c = false;
				}

				// 有効期間
				if (validJobIndex.contains(i)) {
					job.ValidFrom__c = ValidPeriodEntity.VALID_FROM_MIN.getDate();
					job.ValidTo__c = ValidPeriodEntity.VALID_TO_MAX.getDate();
				} else {
					job.ValidFrom__c = Date.newInstance(1990, 1, 1);
					job.ValidTo__c = Date.newInstance(2000, 1, 1);
				}

				// 全社公開 or 個別設定
				if (publicJobIndex.contains(i)) {
					job.ScopedAssignment__c = false;
				} else {
					job.ScopedAssignment__c = true;
					// ジョブ割当
					if (assignedJobIndex.contains(i)) {
						// 有効期間内のジョブ割当
						if (validAssignJobIndex.contains(i)) {
							ComTestDataUtility.createJobAssign(job.Id, employee.id, ValidPeriodEntity.VALID_FROM_MIN, ValidPeriodEntity.VALID_TO_MAX);
						} else {
							ComTestDataUtility.createJobAssign(job.Id, employee.id, AppDate.newInstance(1990, 1, 1), AppDate.newInstance(2000, 1, 1));
						}
					}
				}
			}
			upsert jobs;

			// 検索結果を返す
			JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
			filter.companyIds = new Set<Id>{this.company.id};
			return new JobRepository().searchEntity(filter);
		}
	}

	/**
	 * saveJobEntityのテスト
	 * 新規のジョブが保存できることを確認する
	 */
	@isTest static void saveJobEntityTestNew() {
		JobServiceTest.ServiceTestData testData = new ServiceTestData();

		JobEntity entity = new JobEntity();
		entity.name = 'Testジョブ';
		entity.nameL0 = entity.name + '_L0';
		entity.nameL1 = entity.name + '_L1';
		entity.nameL2 = entity.name + '_L2';
		entity.code = entity.name + '_code';
		entity.companyId = testData.company.id;
		entity.jobTypeId = testData.jobType.id;
		entity.validFrom = AppDate.today();
		entity.validTo = entity.validFrom.addMonths(12);
		entity.parentId = null;
		entity.departmentBaseId = testData.department.id;
		entity.jobOwnerBaseId = testData.employee.id;
		entity.isDirectCharged = true;
		entity.isSelectableTimeTrack = true;
		entity.isScopedAssignment = true;

		// 保存実行
		Id newId = new JobService().saveJobEntity(entity);

		System.assertNotEquals(null, newId);

		// DBへの保存はリポジトリクラスを介して行っているため、項目ごとの保存チェックは省略
		// Serviceクラスで設定している値を中心にチェックする
		JobEntity resEntity = new JobRepository().getEntity(newId);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(entity.createUniqKey(testData.company.code), resEntity.uniqkey);
	}

	/**
	 * saveJobEntityのテスト
	 * 既存のジョブが更新できることを確認する
	 */
	@isTest static void saveJobEntityTestUpdate() {
		JobServiceTest.ServiceTestData testData = new ServiceTestData();

		final Integer numberOfJob = 1;
		JobEntity orgEntity = testData.createJobs('Test', numberOfJob)[0];

		// 更新情報を設定する
		JobEntity updateEntity = new JobEntity();
		updateEntity.setId(orgEntity.id);
		updateEntity.code = orgEntity.code + '_Update';
		updateEntity.nameL0 = orgEntity.nameL0 + '_Update';
		updateEntity.validTo = orgEntity.validTo.addDays(-1);
		updateEntity.isScopedAssignment = true;

		Id resId = new JobService().saveJobEntity(updateEntity);

		System.assertNotEquals(null, resId);

		// 値が更新されていることを確認
		JobEntity resEntity = new JobRepository().getEntity(resId);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(updateEntity.code, resEntity.code);
		System.assertEquals(updateEntity.createUniqKey(testData.company.code), resEntity.uniqkey);
		System.assertEquals(updateEntity.nameL0, resEntity.nameL0);
		System.assertEquals(updateEntity.validTo, resEntity.validTo);
		System.assertEquals(updateEntity.isScopedAssignment, resEntity.isScopedAssignment);
		// 更新対象外の項目は更新されていない
		System.assertEquals(orgEntity.nameL1, resEntity.nameL1);
	}

	/**
	 * saveJobEntityのテスト
	 * コードが重複している場合はアプリ例外が発生することを確認する
	 */
	@isTest static void saveJobEntityTestDuplicateCode() {
		JobServiceTest.ServiceTestData testData = new ServiceTestData();

		final Integer numberOfJob = 3;
		List<JobEntity> jobEntities = testData.createJobs('Test', numberOfJob);
		JobEntity orgEntity = jobEntities[1];

		// 更新情報を設定する
		JobEntity updateEntity = new JobEntity();
		updateEntity.setId(orgEntity.id);
		updateEntity.code = jobEntities[0].code;

		try {
			new JobService().saveJobEntity(updateEntity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
		}
	}

	/**
	 * saveJobEntityのテスト
	 * 項目値に対するバリデーションが実行されていることを確認する
	 */
	@isTest static void saveJobEntityTestValidate() {
		JobServiceTest.ServiceTestData testData = new ServiceTestData();

		final Integer numberOfJob = 3;
		List<JobEntity> jobEntities = testData.createJobs('Test', numberOfJob);
		JobEntity orgEntity = jobEntities[1];

		// 更新情報を設定する
		JobEntity updateEntity = new JobEntity();
		updateEntity.setId(orgEntity.id);
		updateEntity.nameL0 = ''; // 必須項目に空文字を設定

		try {
			new JobService().saveJobEntity(updateEntity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
		}
	}

	/**
	 * ジョブとジョブ割当の有効期間が同一の場合、期間の境界値を正しく判定出来ることを検証する。
	 */
	@isTest static void getActiveJobBoundaryDateTest1() {
		// ジョブ
		JobServiceTest.ServiceTestData testData = new ServiceTestData();
		JobEntity job = testData.createJobs('Job', 1)[0];
		job.isScopedAssignment = true;
		job.validFrom = AppDate.newInstance(2018, 4, 1);
		job.validTo = AppDate.newInstance(2019, 4, 1);
		new JobRepository().saveEntity(job);

		// ジョブ割当
		JobAssignEntity jobAssign = new JobAssignEntity();
		jobAssign.jobId = job.id;
		jobAssign.employeeBaseId = testData.employee.id;
		jobAssign.validFrom = job.validFrom;
		jobAssign.validTo = job.validTo;
		new JobAssignRepository().saveEntity(jobAssign);

		JobService service = new JobService();
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 3, 31)).size()); // 開始日前
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 4, 1)).size());  // 開始日
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 4, 2)).size());  // 開始日後
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 31)).size()); // 失効日前
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 4, 1)).size());  // 失効日
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 4, 2)).size());  // 失効日後
	}

	/**
	 * ジョブ有効開始日 < ジョブ割当有効開始日 < ジョブ割当失効日 < ジョブ失効日 の場合
	 * 期間の境界値を正しく判定出来ることを検証する。
	 */
	@isTest static void getActiveJobBoundaryDateTest2() {
		// ジョブ
		JobServiceTest.ServiceTestData testData = new ServiceTestData();
		JobEntity job = testData.createJobs('Job', 1)[0];
		job.isScopedAssignment = true;
		job.validFrom = AppDate.newInstance(2018, 4, 1);
		job.validTo = AppDate.newInstance(2019, 4, 1);
		new JobRepository().saveEntity(job);

		// ジョブ割当
		JobAssignEntity jobAssign = new JobAssignEntity();
		jobAssign.jobId = job.id;
		jobAssign.employeeBaseId = testData.employee.id;
		jobAssign.validFrom = AppDate.newInstance(2018, 5, 1);
		jobAssign.validTo = AppDate.newInstance(2019, 3, 1);
		new JobAssignRepository().saveEntity(jobAssign);

		// ジョブ割当の境界値を検証する
		JobService service = new JobService();
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 4, 30)).size()); // 開始日前
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 1)).size());  // 開始日
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 2)).size());  // 開始日後
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 2, 28)).size()); // 失効日前
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 1)).size());  // 失効日
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 2)).size());  // 失効日後
	}

	/**
	 * ジョブ割当有効開始日 < ジョブ有効開始日 <  ジョブ失効日 < ジョブ割当失効日 の場合
	 * 期間の境界値を正しく判定出来ることを検証する。
	 */
	@isTest static void getActiveJobBoundaryDateTest3() {
		// ジョブ
		JobServiceTest.ServiceTestData testData = new ServiceTestData();
		JobEntity job = testData.createJobs('Job', 1)[0];
		job.isScopedAssignment = true;
		job.validFrom = AppDate.newInstance(2018, 5, 1);
		job.validTo = AppDate.newInstance(2019, 3, 1);
		new JobRepository().saveEntity(job);

		// ジョブ割当
		JobAssignEntity jobAssign = new JobAssignEntity();
		jobAssign.jobId = job.id;
		jobAssign.employeeBaseId = testData.employee.id;
		jobAssign.validFrom = AppDate.newInstance(2018, 4, 1);
		jobAssign.validTo = AppDate.newInstance(2019, 4, 1);
		new JobAssignRepository().saveEntity(jobAssign);

		// ジョブの境界値を検証する
		JobService service = new JobService();
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 4, 30)).size()); // 開始日前
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 1)).size());  // 開始日
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 2)).size());  // 開始日後
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 2, 28)).size()); // 失効日前
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 1)).size());  // 失効日
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 2)).size());  // 失効日後
	}

	/**
	 * ジョブ有効開始日 < ジョブ割当有効開始日 < ジョブ失効日 < ジョブ割当失効日 の場合
	 * 期間の境界値を正しく判定出来ることを検証する。
	 */
	@isTest static void getActiveJobBoundaryDateTest4() {
		// ジョブ
		JobServiceTest.ServiceTestData testData = new ServiceTestData();
		JobEntity job = testData.createJobs('Job', 1)[0];
		job.isScopedAssignment = true;
		job.validFrom = AppDate.newInstance(2018, 4, 1);
		job.validTo = AppDate.newInstance(2019, 3, 1);
		new JobRepository().saveEntity(job);

		// ジョブ割当
		JobAssignEntity jobAssign = new JobAssignEntity();
		jobAssign.jobId = job.id;
		jobAssign.employeeBaseId = testData.employee.id;
		jobAssign.validFrom = AppDate.newInstance(2018, 5, 1);
		jobAssign.validTo = AppDate.newInstance(2019, 4, 1);
		new JobAssignRepository().saveEntity(jobAssign);

		// ジョブの境界値を検証する
		JobService service = new JobService();
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 4, 30)).size()); // 開始日前
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 1)).size());  // 開始日
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 2)).size());  // 開始日後
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 2, 28)).size()); // 失効日前
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 1)).size());  // 失効日
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 2)).size());  // 失効日後
	}

	/**
	 * ジョブ割当開始日 < ジョブ開始日 < ジョブ割当失効日 < ジョブ失効日 の場合
	 * 期間の境界値を正しく判定出来ることを検証する。
	 */
	@isTest static void getActiveJobBoundaryDateTest5() {
		// ジョブ
		JobServiceTest.ServiceTestData testData = new ServiceTestData();
		JobEntity job = testData.createJobs('Job', 1)[0];
		job.isScopedAssignment = true;
		job.validFrom = AppDate.newInstance(2018, 5, 1);
		job.validTo = AppDate.newInstance(2019, 4, 1);
		new JobRepository().saveEntity(job);

		// ジョブ割当
		JobAssignEntity jobAssign = new JobAssignEntity();
		jobAssign.jobId = job.id;
		jobAssign.employeeBaseId = testData.employee.id;
		jobAssign.validFrom = AppDate.newInstance(2018, 4, 1);
		jobAssign.validTo = AppDate.newInstance(2019, 3, 1);
		new JobAssignRepository().saveEntity(jobAssign);

		// ジョブの境界値を検証する
		JobService service = new JobService();
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 4, 30)).size()); // 開始日前
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 1)).size());  // 開始日
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2018, 5, 2)).size());  // 開始日後
		System.assertEquals(1, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 2, 28)).size()); // 失効日前
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 1)).size());  // 失効日
		System.assertEquals(0, service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2019, 3, 2)).size());  // 失効日後
	}

	/**
	 * 経費の有効なジョブを取得できることを検証する
	 */
	@isTest
	static void getActiveExpenseJobTest() {
		ServiceTestData testData = new ServiceTestData();
		List<JobEntity> testJobs = testData.createSearchTestData();

		// 有効な全社公開ジョブと、割当済みの個別設定ジョブを取得していることを検証
		JobService service = new JobService();
		List<JobEntity> activeJobs = service.getActiveExpenseJob(testData.employee, AppDate.newInstance(2020, 1, 1));
		System.assertEquals(2, activeJobs.size());
		System.assertEquals(testJobs[8].id, activeJobs[0].id);
		System.assertEquals(testJobs[9].id, activeJobs[1].id);
	}

	/**
	 * 工数の有効なジョブを取得できることを検証する
	 */
	@isTest
	static void getActiveTimeTrackJobTest() {
		ServiceTestData testData = new ServiceTestData();
		List<JobEntity> testJobs = testData.createSearchTestData();

		// 有効な全社公開ジョブと、割当済みの個別設定ジョブを取得していることを検証
		JobService service = new JobService();
		List<JobEntity> activeJobs = service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2020, 1, 1));
		System.assertEquals(2, activeJobs.size());
		System.assertEquals(testJobs[0].id, activeJobs[0].id);
		System.assertEquals(testJobs[1].id, activeJobs[1].id);
	}

	/**
	 * 経費の有効なジョブをジョブコードの昇順で取得できることを検証する
	 */
	@isTest
	static void getActiveExpenseJobSortTest() {
		ServiceTestData testData = new ServiceTestData();
		List<JobEntity> testJobs = testData.createSearchTestData();
		testJobs[9].code = '001';
		testJobs[8].code = '002';
		new JobRepository().saveEntityList(testJobs);

		// ジョブコードの昇順で取得できることを検証
		JobService service = new JobService();
		List<JobEntity> activeJobs = service.getActiveExpenseJob(testData.employee, AppDate.newInstance(2020, 1, 1));
		System.assertEquals(2, activeJobs.size());
		System.assertEquals(testJobs[9].id, activeJobs[0].id);
		System.assertEquals(testJobs[8].id, activeJobs[1].id);
	}

	/**
	 * 工数の有効なジョブをジョブコードの昇順で取得できることを検証する
	 */
	@isTest
	static void getActiveTimeTrackJobSortTest() {
		ServiceTestData testData = new ServiceTestData();
		List<JobEntity> testJobs = testData.createSearchTestData();
		testJobs[1].code = '001';
		testJobs[0].code = '002';
		new JobRepository().saveEntityList(testJobs);

		// 有効な全社公開ジョブと、割当済みの個別設定ジョブを取得していることを検証
		JobService service = new JobService();
		List<JobEntity> activeJobs = service.getActiveTimeTrackJob(testData.employee, AppDate.newInstance(2020, 1, 1));
		System.assertEquals(2, activeJobs.size());
		System.assertEquals(testJobs[1].id, activeJobs[0].id);
		System.assertEquals(testJobs[0].id, activeJobs[1].id);
	}

	/**
	 * ジョブコード順でソートできることを検証する
	 */
	@isTest
	static void sortJobListTest() {
		// ジョブを作成
		JobEntity job1 = new JobEntity();
		job1.code = 'Job002';
		JobEntity job2 = new JobEntity();
		job2.code = 'Job003';
		JobEntity job3 = new JobEntity();
		job3.code = 'Job001';
		List<JobEntity> jobs = new List<JobEntity>{job1, job2, job3};

		// 実行
		JobService service = new JobService();
		List<JobEntity> sortedJobs = service.sortJobList(jobs);

		// ジョブコード順にソートされていることを検証
		System.assertEquals(3, jobs.size());
		System.assertEquals(job3, sortedJobs[0]);
		System.assertEquals(job1, sortedJobs[1]);
		System.assertEquals(job2, sortedJobs[2]);
	}

	/**
	 * Test getParentHierarchy gives correct job hierarchy parent names
	 */
	@isTest
	static void getParentHierarchyTest() {
		ServiceTestData testData = new ServiceTestData();
		EmployeeBaseEntity employeeEntity = testData.employee;

		List<ComJob__c> comJob =[SELECT Id FROM ComJob__c];
		delete comJob;

		JobService service = new JobService();
		JobEntity testJob1 = testData.createJob('JB1', null);
		testJob1.isSelectableExpense = true;
		testJob1.setId(service.saveJobEntity(testJob1));
		JobEntity testJob2 = testData.createJob('AC1', testJob1.id);
		testJob2.isSelectableExpense = true;
		testJob2.setId(service.saveJobEntity(testJob2));
		JobEntity testJob3 = testData.createJob('VY1', testJob2.id);
		testJob3.isSelectableExpense = true;
		testJob3.setId(service.saveJobEntity(testJob3));

		List<ComJob__c> comJob1 =[SELECT Id FROM ComJob__c WHERE Code__c LIKE '%-1%'];
		System.assertEquals(3, comJob1.size());
		delete comJob1;

		AppDate targetDate = AppDate.today();
		List<JobEntity> entity = service.searchJobList(null, testData.company.id, null, targetDate, testJob3.code, null, null);

		System.assertEquals(1, entity.size());
		System.assertEquals(testJob3.code, entity[0].code);


		Map<Id, List<String>> parentNameMap = service.filterActiveJobWithParentName(new List<JobEntity> {entity[0]}, targetDate, null);
		System.assertEquals(true, parentNameMap != null);
		System.assertEquals(true, parentNameMap.get(testJob3.Id) != null);
		List<String> names = parentNameMap.get(testJob3.Id);
		System.assertEquals(2, names.size());
		System.assertEquals(testJob2.nameL.getValue(), names[0]);
		System.assertEquals(testJob1.nameL.getValue(), names[1]);
	}


	/**
	 * Test getRecentlyUsedJobList
	 */
	@isTest
	static void getRecentlyUsedJobListTest() {
		ServiceTestData testData = new ServiceTestData();
		List<JobEntity> testJobList = testData.createSearchTestData();

		ExpRecentlyUsedService expRecUsedService = new ExpRecentlyUsedService();
		// Add to recently used CostCenter
		expRecUsedService.saveRecentlyUsedJobValues(testData.employee.id, testJobList[3].id, testJobList[8].id);

		test.startTest();
		List<JobEntity> resultJobEntityList = new JobService().getRecentlyUsedJobList(testData.employee.id, new Map<Id, List<String>>(), AppDate.today());
		test.stopTest();

		System.assertEquals(1, resultJobEntityList.size());
		System.assertEquals(false, resultJobEntityList.isEmpty());
		System.assertEquals(testJobList[8].id, resultJobEntityList[0].id);
		System.assertEquals(testJobList[8].code, resultJobEntityList[0].code);
		System.assertEquals(testJobList[8].nameL0, resultJobEntityList[0].nameL0);
	}

	/**
	 * Test getRecentlyUsedJobList that is not active
	 */
	@isTest
	static void getRecentlyJobListInvalidJobTest() {
		ServiceTestData testData = new ServiceTestData();
		List<JobEntity> testJobList = testData.createSearchTestData();

		ExpRecentlyUsedService expRecUsedService = new ExpRecentlyUsedService();
		// Add to recently used CostCenter
		expRecUsedService.saveRecentlyUsedJobValues(testData.employee.id, testJobList[3].id, testJobList[0].id);

		List<JobEntity> resultJobEntityList = new JobService().getRecentlyUsedJobList(testData.employee.id, new Map<Id, List<String>>(), AppDate.today());

		// The Job is not usable in expesne type therefore, it is not return
		System.assertEquals(0, resultJobEntityList.size());
		System.assertEquals(true, resultJobEntityList.isEmpty());

		expRecUsedService.saveRecentlyUsedJobValues(testData.employee.id, testJobList[3].id, testJobList[10].id);
		resultJobEntityList = new JobService().getRecentlyUsedJobList(testData.employee.id, new Map<Id, List<String>>(), AppDate.today());

		// The Job is not active, it is not returned
		System.assertEquals(0, resultJobEntityList.size());
		System.assertEquals(true, resultJobEntityList.isEmpty());
	}
	/**
	 * 階層チェック(新規ジョブ)
	 */
	@isTest
	static void circularValidatorTestNewJobOver() {
		ServiceTestData testData = new ServiceTestData();
		JobService service = new JobService();
		Id parentId = null;
		for (Integer i = 1; i <= 10; i++) {
			JobEntity newJob = testData.createJob('Job' + i, parentId);
			parentId = service.saveJobEntity(newJob);
		}
		Boolean hasError = false;
		try {
			JobEntity newJob = testData.createJob('Job' + 11, parentId);
			service.saveJobEntity(newJob);
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assertEquals(ComMessage.msg().Com_Err_MaxHierarchyOver(new List<String>{ComMessage.msg().Admin_Lbl_Job, '10'}), e.getMessage());
		}
		System.assert(hasError);
	}
	/**
	 * 階層チェック(2ジョブ階層接続)
	 */
	@isTest
	static void circularValidatorTestUpdateJobOver() {
		ServiceTestData testData = new ServiceTestData();
		JobService service = new JobService();
		// ジョブ階層関係2件作成
		// 階層1：6階層
		JobEntity job = testData.createJob('Job0', null);
		Id parentId1 = service.saveJobEntity(job);
		job.setId(parentId1);
		for (Integer i = 1; i <= 5; i++) {
			JobEntity newJob = testData.createJob('Job' + i, parentId1);
			parentId1 = service.saveJobEntity(newJob);
		}
		// 階層2：5階層
		Id parentId2 = null;
		for (Integer i = 6; i <= 10; i++) {
			JobEntity newJob = testData.createJob('Job' + i, parentId2);
			parentId2 = service.saveJobEntity(newJob);
		}

		Boolean hasError = false;
		try {
			job.parentId = parentId2; // 2ジョブ階層を接続(11階層になる)
			service.saveJobEntity(job);
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assertEquals(ComMessage.msg().Com_Err_MaxHierarchyOver(new List<String>{ComMessage.msg().Admin_Lbl_Job, '10'}), e.getMessage());
		}
		System.assert(hasError);
	}
	/**
	 * 階層チェック(循環参照)
	 */
	@isTest
	static void circularValidatorTestUpdate() {
		ServiceTestData testData = new ServiceTestData();
		JobService service = new JobService();

		JobEntity job1 = testData.createJob('Job1', null);
		job1.setId(service.saveJobEntity(job1));
		JobEntity job2 = testData.createJob('Job2', job1.id);
		job2.setId(service.saveJobEntity(job2));
		JobEntity job3 = testData.createJob('Job3', job2.id);
		job3.setId(service.saveJobEntity(job3));
		Boolean hasError = false;
		try {
			job1.parentId = job3.id; // 循環参照
			service.saveJobEntity(job1);
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assertEquals(ComMessage.msg().Com_Err_CircularHierarchy(new List<String>{job2.nameL.getValue(), ComMessage.msg().Com_Lbl_ParentJob}), e.getMessage());
		}
		System.assert(hasError);
	}
	/**
	 * 階層チェック(ルートジョブに親設定)
	 */
	@isTest
	static void circularValidatorTestUpdateRootParent() {
		ServiceTestData testData = new ServiceTestData();
		JobService service = new JobService();
		JobEntity aloneJob = testData.createJob('Job0', null);
		aloneJob.setId(service.saveJobEntity(aloneJob));

		// 10階層のジョブ階層作成
		JobEntity rootJob = testData.createJob('Job1', null);
		rootJob.setId(service.saveJobEntity(rootJob));
		Id parentId = rootJob.id;
		for (Integer i = 2; i <= 10; i++) {
			JobEntity newJob = testData.createJob('Job' + i, parentId);
			parentId = service.saveJobEntity(newJob);
		}
		Boolean hasError = false;
		try {
			rootJob.parentId = aloneJob.id;
			service.saveJobEntity(rootJob);
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assertEquals(ComMessage.msg().Com_Err_MaxHierarchyOver(new List<String>{ComMessage.msg().Admin_Lbl_Job, '10'}), e.getMessage());
		}
		System.assert(hasError);
	}
}