/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 休暇マスタのサービスクラス
 */
public with sharing class AttLeaveService implements Service {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * 年次有休休暇登録時チェック処理
	 * @param companyId チェック対象会社ID
	 */
	public void checkAnnualLeaveAdd(Id companyId) {
		// 既存の年次有休休暇を取得する
		if (getAnnualLeave(companyId) != null) {
			throw new App.IllegalStateException(
					App.ERR_CODE_ANNUAL_LEAVE_EXISTED,
					ComMessage.msg().Admin_Err_LeaveAnnualExisted +
					ComMessage.msg().Admin_Slt_LeaveAnnualExisted);
		}
	}

	/**
	 * 年次有休休暇取得処理
	 * TODO：有効期間条件
	 * @param companyId 取得対象会社ID
	 */
	public AttLeaveEntity getAnnualLeave(Id companyId) {
		// 既存の年次有休休暇を取得する
		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.SearchFilter searchParam = new AttLeaveRepository.SearchFilter();

		searchParam.companyId = companyId;
		searchParam.leaveType = AttLeaveType.ANNUAL;

		// TODO:複数チェック
		for (AttLeaveEntity annualLeave : leaveRep.searchEntityList(searchParam)) {
			return annualLeave;
		}
		return null;
	}

	/**
	 * 日数管理休暇取得処理
	 * TODO：有効期間条件
	 * @param companyId 取得対象会社ID
	 */
	public List<AttLeaveEntity> getManagedLeaveList(Id companyId) {
		// 既存の日数管理休暇を取得する
		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.SearchFilter searchParam = new AttLeaveRepository.SearchFilter();

		searchParam.companyId = companyId;
		searchParam.daysManaged = true;
		searchParam.exceptLeaveType = AttLeaveType.ANNUAL;

		return leaveRep.searchEntityList(searchParam);
	}

	/**
	 * 休暇マスタエンティティを検証する
	 * 不正な値が設定されている場合はApp.ParameterExceptionが発生します
	 */
	public void validateLeave(AttLeaveEntity entity) {
		// 休暇マスタエンティティの項目の値を検証する
		Validator.Result  result = (new AttConfigValidator.AttLeaveValidator(entity)).validate();
		if (! result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * 休暇を保存する
	 */
	public Repository.SaveResult saveLeaveEntity(AttLeaveEntity savedEntity) {
		AttLeaveRepository leaveRepo = new AttLeaveRepository();

		// 保存用のエンティティを作成
		AttLeaveEntity entity;
		if (savedEntity.id == null) {
			// 新規の場合
			entity = savedEntity;
		} else {
			// 更新の場合は既存の値を取得する
			entity = leaveRepo.getLeaveById(savedEntity.id);
			if (entity == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			// 更新値を上書きする
			for (Schema.SObjectField field : savedEntity.getChangedFieldSet()) {
				entity.setFieldValue(field, savedEntity.getFieldValue(field));
			}
		}

		// 各項目のチェック
		validateLeave(entity);

		// コードの重複をチェックする
		AttLeaveEntity duplicateCodeEntity = leaveRepo.getLeaveByCode(entity.code, entity.companyId);
		if ((duplicateCodeEntity != null) && (duplicateCodeEntity.id != entity.id)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		// ユニークキーを設定する
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		if (company == null) {
				// メッセージ：指定された会社が見つかりません
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		entity.uniqKey = entity.createUniqKey(company.code);

		// 年次有休休暇の場合、唯一チェックを行う
		// TODO:休暇種別変更時のチェックを追加
		if (entity.Id == null && AttLeaveType.ANNUAL.equals(entity.leaveType)) {
			checkAnnualLeaveAdd(entity.companyId);
		}

		// 保存する
		Repository.SaveResult result = leaveRepo.saveEntity(entity);
		return result;
	}
}
