/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 申請のトリガーハンドラー基底クラス
 */
public abstract class RequestTriggerHandler {
	// 承認ステップステータス：承認済み
	public static final String STEP_STATUS_APPROVED = 'Approved';
	// 承認ステップステータス：却下
	public static final String STEP_STATUS_REJECTED = 'Rejected';
	// 承認ステップステータス：申請取消
	public static final String STEP_STATUS_REMOVED = 'Removed';

	// 承認済み申請Id
	public Set<Id> approvedRequestIds {get { return getApprovedRequestIds();}}
	public abstract Set<Id> getApprovedRequestIds();
	// 無効申請Id
	public Set<Id> disabledRequestIds {get { return getDisabledRequestIds();}}
	public abstract Set<Id> getDisabledRequestIds();
	// 承認プロセス情報を申請に適用する
	public abstract void applyProcessInfo();

	// 承認済み、却下、承認取消になる承認プロセス情報を取得する
	// ログインユーザーが承認プロセス情報を取得可能を前提とする
	protected Map<Id, ProcessInstanceStep> getProcessInfo(Set<Id> requestIds) {
		Map<Id, ProcessInstanceStep> retMap = new Map<Id, ProcessInstanceStep>();
		// 最新の承認プロセス情報を取得 
		for (ProcessInstanceStep processStep : [
				SELECT Id, CreatedDate, ActorId, Comments, StepStatus, ProcessInstance.TargetObjectId 
				FROM ProcessInstanceStep 
				WHERE ProcessInstance.TargetObjectId IN :requestIds 
					AND StepStatus IN (
						:STEP_STATUS_APPROVED, 
						:STEP_STATUS_REJECTED,
						:STEP_STATUS_REMOVED)
				ORDER BY ProcessInstance.TargetObjectId, CreatedDate DESC]) {

			Id objectId = processStep.ProcessInstance.TargetObjectId;
			if (!retMap.containsKey(objectId)) {
				retMap.put(objectId, processStep);
			}
		}
		return retMap;
	}
}