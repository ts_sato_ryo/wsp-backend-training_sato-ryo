/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Test class for ExpCommonUtil
 */
@IsTest
private class ExpCommonUtilTest {
	/**
	 * canUseExpens(引数なし)のテスト
	 * 経費申請機能を利用できることを確認する
	 */
	@isTest static void canUseExpenseWithoutEmployeeTest() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();

		Boolean isSuccess = ExpCommonUtil.canUseExpense();
		System.assertEquals(true, isSuccess);

		// 経費申請機能がOFFの場合
		// アプリ例外が発生する
		try {
			//経費申請機能をOFFにする
			testData.changeUseExpense(false);
			ExpCommonUtil.canUseExpense();
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * canUseExpens(引数なし)のテスト
	 * 基準通貨がない時にエラーを吐くことを確認 / check error is thrown when no base currency
	 */
	@isTest static void canUseExpenseBaseCurrencyWithoutEmployeeTest() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();

		Boolean isSuccess = ExpCommonUtil.canUseExpense();
		System.assertEquals(true, isSuccess);

		// 経費申請機能がOFFの場合
		// アプリ例外が発生する
		try {
			// 基準通貨をnullにセットする / set base currency null
			testData.company.CurrencyId__c = null;
			update testData.company;
			ExpCommonUtil.canUseExpense();
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NO_BASE_CURRENCY, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}


	/**
	 * canUseExpense(引数有り)のテスト / test of canUseExpense (with arguments)
	 * 経費申請機能を利用できることを確認する / check if use expense function
	 */
	@isTest static void canUseExpenseExpenseFunctionTest() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();

		Boolean isSuccess = ExpCommonUtil.canUseExpense(testData.employee);
		System.assertEquals(true, isSuccess);

		// 経費申請機能がOFFの場合
		// アプリ例外が発生する
		try {
			//経費申請機能をOFFにする
			testData.changeUseExpense(false);
			ExpCommonUtil.canUseExpense(testData.employee);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE, e.getErrorCode());
		}

		Test.stopTest();
	}

	/**
	 * canUseExpense(引数有り)のテスト / test of canUseExpense (with arguments)
	 * 基準通貨がない時にエラーを吐くことを確認 / check error is thrown when no base currency
	 */
	@isTest static void canUseExpenseBaseCurrencyTest() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();
		Boolean isSuccess = ExpCommonUtil.canUseExpense(testData.employee);
		Test.stopTest();

		System.assertEquals(true, isSuccess);
		// 基準通貨がない場合アプリ例外が発生する / App exception is thrown when no base currency
		try {
			// 基準通貨をnullにセットする / set base currency null
			testData.company.CurrencyId__c = null;
			update testData.company;
			ExpCommonUtil.canUseExpense(testData.employee);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NO_BASE_CURRENCY, e.getErrorCode());
		}
	}

	/*
	 * Test of checkCanUseExpenseRequest (with arguments)
	 * Check if use expense function
	 */
	@isTest static void checkCanUseExpenseRequestTestExpenseFunction() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();
		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		Test.stopTest();

		System.assertEquals(true, isSuccess);

		// In case UseExpenseRequest flag is false, Exception happens
		try {
			// Turn off UseExpenseRequest flag
			testData.changeUseExpenseRequest(false);
			ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE_REQUEST, e.getErrorCode());
		}
	}

	/**
	 * Test of checkCanUseExpenseRequest (with arguments)
	 * Check error is thrown when no base currency
	 */
	@isTest static void checkCanUseExpenseRequestTestBaseCurrency() {
		ExpTestData testData = new ExpTestData();
		Test.startTest();
		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		Test.stopTest();
		System.assertEquals(true, isSuccess);
		// App exception is thrown when no base currency
		try {
			// set base currency null
			testData.company.CurrencyId__c = null;
			update testData.company;
			ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NO_BASE_CURRENCY, e.getErrorCode());
		}
	}

	/**
	 * Test of checkCanUseExpenseRequest without arguments
	 * Check exception is thrown when there are no employees
	 */
	@isTest static void checkCanUseExpenseRequestTestWithoutEmployee() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();
		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(null);
		Test.stopTest();

		System.assertEquals(true, isSuccess);

		// In case UseExpenseRequest flag is false, Exception happens
		try {
			// Turn off UseExpense flag
			testData.changeUseExpenseRequest(false);
			ExpCommonUtil.checkCanUseExpenseRequest(null);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE_REQUEST, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * Test of checkCanUseExpenseRequest (not arguments)
	 * check error is thrown when no base currency
	 */
	@isTest static void checkCanUseExpenseRequestTestBaseCurrencyWithoutEmployee() {
		ExpTestData testData = new ExpTestData();
		Test.startTest();
		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(null);
		Test.stopTest();
		System.assertEquals(true, isSuccess);
		// In case UseExpenseRequest flag is false
		// Exception happens
		try {
			// set base currency null
			testData.company.CurrencyId__c = null;
			update testData.company;
			ExpCommonUtil.checkCanUseExpenseRequest(null);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NO_BASE_CURRENCY, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * canUseExpenseRequest(引数なし)のテスト
	 * 事前申請機能を利用できることを確認する
	 */
	@isTest static void canUseExpenseRequestWithoutEmployeeTest() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();

		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(null);
		System.assertEquals(true, isSuccess);

		// 経費申請機能がOFFの場合
		// アプリ例外が発生する
		try {
			//経費申請機能をOFFにする
			testData.changeUseExpenseRequest(false);
			ExpCommonUtil.checkCanUseExpenseRequest(null);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE_REQUEST, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * canUseExpenseRequest(引数有り)のテスト / test of canUseExpense (with arguments)
	 * 事前申請機能を利用できることを確認する / check if use expense request function
	 */
	@isTest static void canUseExpenseRequestFunctionTest() {
		ExpTestData testData = new ExpTestData();

		Test.startTest();

		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		System.assertEquals(true, isSuccess);

		// 経費申請機能がOFFの場合
		// アプリ例外が発生する
		try {
			//経費申請機能をOFFにする
			testData.changeUseExpenseRequest(false);
			ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE_REQUEST, e.getErrorCode());
		}

		Test.stopTest();
	}


	/**
	 * test of checkCanUseExpenseRequest (with arguments)
	 * check error is thrown when no base currency
	 */
	@isTest static void canUseExpenseRequestTestBaseCurrency() {
		ExpTestData testData = new ExpTestData();
		Test.startTest();
		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		Test.stopTest();
		System.assertEquals(true, isSuccess);
		// App exception is thrown when no base currency
		try {
			// set base currency null
			testData.company.CurrencyId__c = null;
			update testData.company;
			ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NO_BASE_CURRENCY, e.getErrorCode());
		}
	}

	/**
	 * test of checkCanUseExpenseRequest (not arguments)
	 * check error is thrown when no base currency
	 */
	@isTest static void canUseExpenseRequestTestBaseCurrencyWithoutEmployee() {
		ExpTestData testData = new ExpTestData();
		Test.startTest();
		Boolean isSuccess = ExpCommonUtil.checkCanUseExpenseRequest(testData.employee);
		Test.stopTest();
		System.assertEquals(true, isSuccess);
		// In case UseExpenseRequest flag is false
		// Exception happens
		try {
			// set base currency null
			testData.company.CurrencyId__c = null;
			update testData.company;
			ExpCommonUtil.checkCanUseExpenseRequest(null);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NO_BASE_CURRENCY, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * Validate Null Test
	 */
	@isTest static void validateNullTest() {
		Test.startTest();
		try {
			ExpCommonUtil.validateNotNull('An Obj', '123');
			ExpCommonUtil.validateNotNull('An Null Obj', null);
		} catch(Exception e) {
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'An Null Obj'}), e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Validate Empty String Test
	 */
	@isTest static void validateStringTest() {
		Test.startTest();

		try {
			ExpCommonUtil.validateStringIsNotBlank('A String', '123');
			ExpCommonUtil.validateStringIsNotBlank('An Empty String', '');
		} catch(Exception e) {
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'An Empty String'}), e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Validate Date Test
	 */
	@isTest static void validateDateTest() {
		Test.startTest();

		try {
			ExpCommonUtil.validateDate('A Date', String.valueOf(AppDate.today()));
			ExpCommonUtil.validateDate('An Empty Date', '');
		} catch(Exception e) {
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'An Empty Date'}), e.getMessage());
		}

		try {
			ExpCommonUtil.validateDate('An Invalid Date', '18132019');
		} catch (Exception e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'An Invalid Date', '18132019'}), e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Validate Date Test - When it's optional, then no exception is thrown for null and empty string
	 */
	@isTest static void validateDateOptionalTest() {
		Test.startTest();

		Exception expectedException;
		try {
			ExpCommonUtil.validateDate('An Empty Date', '', false);
			ExpCommonUtil.validateDate('An Null Date', null, false);
		} catch(Exception e) {
			expectedException = e;
		}
		Test.stopTest();
		System.assertEquals(null, expectedException);
	}

	/**
	 * Validate UsedIn Test
	 */
	@isTest static void validateUsedInTest() {
		Test.startTest();

		try {
			ExpCommonUtil.validateUsedIn('Invalid UsedIn', 'xyz');
		} catch (Exception e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'Invalid UsedIn'}), e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Validate String Length Test - Exception Only
	 */
	@isTest static void validateStringLengthTest() {
		Test.startTest();

		try {
			ExpCommonUtil.validateStringLength('Invalid String length', '123456', 5);
		} catch (Exception e) {
			System.assertEquals(ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{'Invalid String length', '5'}), e.getMessage());
		}

		Test.stopTest();
	}

}