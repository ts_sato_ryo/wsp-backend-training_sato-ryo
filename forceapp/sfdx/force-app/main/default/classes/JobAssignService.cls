/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブ割当のサービスクラス
 */
public with sharing class JobAssignService implements Service {

	private JobRepository jobRepository = new JobRepository();

	private JobAssignRepository jobAssignRepository = new JobAssignRepository();

	/**
	 * ジョブ割当レコードを登録する
	 * @param jobId ジョブID
	 * @param validFrom 有効開始日
	 * @param validTo 失効日
	 * @param employeeIds 登録対象の社員ID
	 * @return 登録したレコード情報
	 * @throws App.RecordNotFoundException jobIdに該当するジョブが存在しない場合
	 * @throws App.IllegalStateException validFrom・validToがジョブの有効期間外の場合
	 */
	public List<JobAssignEntity> createJobAssign(Id jobId, AppDate validFrom, AppDate validTo, List<Id> employeeIds) {
		JobEntity job = jobRepository.getEntity(jobId);

		// ジョブが存在しない場合は処理終了
		if (job == null) {
			String message = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Admin_Lbl_Job});
			throw new App.RecordNotFoundException(message);
		}

		// エンティティ生成
		List<JobAssignEntity> entities = new List<JobAssignEntity>();
		for (Id employeeId : employeeIds) {
			JobAssignEntity entity = new JobAssignEntity();
			entity.jobId = jobId;
			entity.validFrom = validFrom;
			entity.validTo = validTo;
			entity.employeeBaseId = employeeId;

			// エンティティのバリデーションを実行
			ComConfigValidator.JobAssignValidator validator = new ComConfigValidator.JobAssignValidator(job, entity);
			Validator.Result result = validator.validate();
			if (!result.isSuccess()) {
				Validator.Error error = result.getErrors()[0];
				throw new App.IllegalStateException(error.code, error.message);
			}
			entities.add(entity);
		}

		// レコード登録
		jobAssignRepository.saveEntityList(entities);
		return entities;
	}

	/**
	 * ジョブ割り当てレコードを更新する
	 * ※ jobAssignIdsは同一Jobが前提
	 * @param jobAssignIds ジョブ割当ID
	 * @param validDateFrom 有効開始日
	 * @param validDateTo 失効日
	 * @return 更新したジョブ割当のリスト
	 * @throws App.RecordNotFoundException ジョブのデータが取得できない場合
	 * @throws App.ParameterException jobAssignIdsに紐づくジョブが複数件ある場合
	 */
	public List<JobAssignEntity> updateJobAssign(List<Id> jobAssignIds, AppDate validDateFrom, Appdate validDateTo) {
		// 更新対象を取得
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		filter.ids = new Set<Id>(jobAssignIds);
		List<JobAssignEntity> updateTargets = jobAssignRepository.searchEntity(filter);

		// ジョブ割当からJob取得
		JobEntity job;
		if (updateTargets != null && !updateTargets.isEmpty()) {
			Set<Id> jobIdSet = new Set<Id>();
			for (JobAssignEntity assign : updateTargets) {
				jobIdSet.add(assign.jobId);
			}
			if (jobIdSet.size() > 1) {//  - 取得時複数件Jobが該当している場合はエラー
				String message = ComMessage.msg().Admin_Err_InvalidJobAssignWithMultiJob;
				throw new App.ParameterException(message);
			}
			job = jobRepository.getEntity(updateTargets.get(0).jobId);
		}

		// Jobに対するバリデーション実行
		//  - Jobが存在しない場合はエラー
		if (job == null) {
			String message = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Admin_Lbl_Job});
			throw new App.RecordNotFoundException(message);
		}

		// エンティティ作成（有効開始日・失効日を反映）
		for (JobAssignEntity entity : updateTargets) {
			entity.validFrom = validDateFrom;
			entity.validTo = validDateTo;

			// バリデーションを実行
			ComConfigValidator.JobAssignValidator validator = new ComConfigValidator.JobAssignValidator(job, entity);
			Validator.Result result = validator.validate();
			if (!result.isSuccess()) {
				Validator.Error error = result.getErrors()[0];
				throw new App.IllegalStateException(error.code, error.message);
			}
		}

		// レコード更新
		jobAssignRepository.saveEntityList(updateTargets);
		return updateTargets;
	}

	/**
	 * ジョブ割り当てレコードを削除する
	 * @param jobAssignIds ジョブ割当ID
	 * @return 更新したジョブ割当のリスト
	 */
	public void deleteJobAssign(List<Id> jobAssignIds) {
		//ジョブ割当IDが空の場合はエラー
		if(jobAssignIds == null || jobAssignIds.isEmpty()){
			App.ParameterException e = new App.ParameterException();
			e.setMessageIsBlank('jobAssignIds');
			throw e;
		}
		// エンティティ作成
		List<JobAssignEntity> deleteEntities = new List<JobAssignEntity>();
		for (Id jobAssignId : jobAssignIds) {
			JobAssignEntity entity = new JobAssignEntity();
			entity.setId(jobAssignId);
			deleteEntities.add(entity);
		}
		// レコード更新
		jobAssignRepository.deleteEntityList(deleteEntities);
	}

	/**
	 * 社員と対象日を条件に、ジョブ割当レコードを検索する
	 * @param employeeId 社員ID
	 * @param targetDate 対象日
	 * @return ジョブ割当のリスト
	 */
	public List<JobAssignEntity> searchJobAssign(Id employeeId, AppDate targetDate) {
		return searchJobAssign(employeeId, targetDate, targetDate);
	}

	/**
	 * 社員と対象日の範囲を条件に、ジョブ割当レコードを検索する
	 * @param employeeId 社員ID
	 * @param startDate 対象開始日
	 * @param endDate 対象終了日
	 * @return ジョブ割当のリスト
	 */
	public List<JobAssignEntity> searchJobAssign(Id employeeId, AppDate startDate, AppDate endDate) {
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		filter.employeeIds.add(employeeId);
		filter.dateRange = new AppDateRange(startDate, endDate);
		return jobAssignRepository.searchEntity(filter);
	}

	/**
	 * 検索条件に一致するジョブ割当レコードを検索する。
	 * 各引数が未指定（null）の場合は、検索条件に含めない。
	 * @param jobId ジョブID
	 * @param employeeId 社員ID
	 */
	public List<JobAssignEntity> searchJobAssign(Id jobId, Id employeeId) {
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		if (jobId != null) {
			filter.jobIds.add(jobId);
		}
		if (employeeId != null) {
			filter.jobIds.add(employeeId);
		}
		return jobAssignRepository.searchEntity(filter);
	}
}