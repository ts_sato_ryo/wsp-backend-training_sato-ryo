/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 工数設定履歴切り替えバッチ
 */
public with sharing class TimeSettingCurrentHistoryUpdateBatch extends CurrentHistoryUpdateBatchBase implements Schedulable {

	/**
	 * コンストラクタ
	 */
	public TimeSettingCurrentHistoryUpdateBatch() {
		// ベース・履歴オブジェクトのAPI参照名を渡す
		super(TimeSettingBase__c.getSObjectType(), TimeSettingHistory__c.getSObjectType());
	}

	/**
	 * Apex実行スケジュールを登録する
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc) {
		TimeSettingCurrentHistoryUpdateBatch b = new TimeSettingCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
	}

	/**
	 * バッチ開始処理
	 * @param BC バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public override Database.QueryLocator start(Database.BatchableContext BC) {
		// ベースオブジェクトを全取得する
		return super.start(bc);
	}

	/**
	 * バッチ実行処理
	 * @param BC バッチコンテキスト
	 * @param scope
	 */
	public override void execute(Database.BatchableContext BC, List<Sobject> scope) {
		super.execute(bc, scope);
	}

	/**
	 * バッチ終了処理
	 * @param BC バッチコンテキスト
	 */
	public override void finish(Database.BatchableContext bc) {
		super.finish(bc);
	}
}
