/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Repository Class for Fixed Allowance Option used in Fixed Allowance Multi
 * 
 * @group Expense
 */
public with sharing class ExpFixedAllowanceOptionRepository extends Repository.BaseRepository {
	/**　Max number of records to fetch */
	private final static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	private static final String BASE_OBJECT_NAME = ExpFixedAllowanceOption__c.SObjectType.getDescribe().getName();

	private static final List<String> GET_BASE_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<SObjectField> {
			ExpFixedAllowanceOption__c.Id,
			ExpFixedAllowanceOption__c.Name,
			ExpFixedAllowanceOption__c.AllowanceAmount__c,
			ExpFixedAllowanceOption__c.CurrencyId__c,
			ExpFixedAllowanceOption__c.ExpTypeId__c,
			ExpFixedAllowanceOption__c.Label_L0__c,
			ExpFixedAllowanceOption__c.Label_L1__c,
			ExpFixedAllowanceOption__c.Label_L2__c,
			ExpFixedAllowanceOption__c.Order__c
		};
		GET_BASE_FIELD_LIST = Repository.generateFieldNameList(fieldList);
	}

	/*
	 * Search Filter to search for Fixed Allowance Options
	 */
	public class SearchFilter {
		public Set<Id> ids;
		public Set<Id> expTypeIds;
	}

	/*
	 * Search Fixed Allowance Option Entity based on the Filter
	 * @param filter Search Filter
	 */
	public List<ExpFixedAllowanceOptionEntity> searchEntityList(SearchFilter filter) {
		List<String> selectFieldList = new List<String>();

		selectFieldList.addAll(GET_BASE_FIELD_LIST);

		List<String> whereConditionList = new List<String>();


		final Set<Id> ids;
		if (filter.ids != null && !filter.ids.isEmpty()) {
			ids = filter.ids;
			whereConditionList.add(createInExpression(ExpFixedAllowanceOption__c.Id, 'ids'));
		}

		final Set<Id> expTypeIds;
		if (filter.expTypeIds != null && !filter.expTypeIds.isEmpty()) {
			expTypeIds = filter.expTypeIds;
			whereConditionList.add(createInExpression(ExpFixedAllowanceOption__c.ExpTypeId__c, 'expTypeIds'));
		}

		String soql = 'SELECT ' + String.join(selectFieldList, ',')
			+ ' FROM ' + BASE_OBJECT_NAME
			+ buildWhereString(whereConditionList)
			+ ' ORDER BY ' + getFieldName(ExpFixedAllowanceOption__c.Order__c) + ' ASC'
			+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';

		List<ExpFixedAllowanceOption__c> sObjList = Database.query(soql);

		List<ExpFixedAllowanceOptionEntity> entityList = createEntityList(sObjList);
		return entityList;
	}

	/**
	 * Save Entity List with All or None as True.
	 * @param entityList List of ExpFixedAllowanceOption entities
	 * @return Save Result
	 */
	public Repository.SaveResult saveEntityList(List<ExpFixedAllowanceOptionEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * Save Entity List
	 * @param entityList List of ExpFixedAllowanceOption entities
	 * @param allOrNone Whether to save partially when error occurred
	 * @return Save Result
	 */
	public virtual Repository.SaveResult saveEntityList(List<ExpFixedAllowanceOptionEntity> entityList, Boolean allOrNone) {
		List<ExpFixedAllowanceOption__c> sObjList = new List<ExpFixedAllowanceOption__c>();
		for (ExpFixedAllowanceOptionEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}

		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/**
	 * Create entity list from object list
	 * @param sObjList List of target objects to create
	 * @return Created entity list. Empty list is returned if no option found
	 */
	private List<ExpFixedAllowanceOptionEntity> createEntityList(List<ExpFixedAllowanceOption__c> sObjList) {
		List<ExpFixedAllowanceOptionEntity> entityList = new List<ExpFixedAllowanceOptionEntity>();

		if (!sObjList.isEmpty()) {
			for (ExpFixedAllowanceOption__c sObj : sObjList) {
				ExpFixedAllowanceOptionEntity entity = new ExpFixedAllowanceOptionEntity();
				entity.setId(sObj.Id);
				entity.AllowanceAmount = sObj.AllowanceAmount__c;
				entity.currencyId = sObj.CurrencyId__c;
				entity.expTypeId = sObj.ExpTypeId__c;
				entity.labelL0 = sObj.Label_L0__c;
				entity.labelL1 = sObj.Label_L1__c;
				entity.labelL2 = sObj.Label_L2__c;
				entity.order = Integer.valueOf(sObj.Order__c);

				entity.resetChanged();

				entityList.add(entity);
			}
		}

		return entityList;
	}

	/**
	 * Create object from entity
	 * @param entity Target entity to create sObject from (cannot be null)
	 * @return ExpFixedAllowanceOption__c Created sObject
	 */
	private ExpFixedAllowanceOption__c createSObject(ExpFixedAllowanceOptionEntity entity) {
		ExpFixedAllowanceOption__c sObj = new ExpFixedAllowanceOption__c();
		sObj.Id = entity.Id;

		if (entity.isChanged(ExpFixedAllowanceOptionEntity.Field.ALLOWANCE_AMOUNT)) {
			sObj.AllowanceAmount__c = entity.allowanceAmount;
		}
		if (entity.isChanged(ExpFixedAllowanceOptionEntity.Field.CURRENCY_ID)) {
			sObj.CurrencyId__c = entity.currencyId;
		}
		if (entity.isChanged(ExpFixedAllowanceOptionEntity.Field.EXP_TYPE_ID)) {
			sObj.ExpTypeId__c = entity.expTypeId;
		}
		if (entity.isChanged(ExpFixedAllowanceOptionEntity.Field.LABEL_L0)) {
			sObj.Label_L0__c = entity.labelL0;
		}
		if (entity.isChanged(ExpFixedAllowanceOptionEntity.Field.LABEL_L1)) {
			sObj.Label_L1__c = entity.labelL1;
		}
		if (entity.isChanged(ExpFixedAllowanceOptionEntity.Field.LABEL_L2)) {
			sObj.Label_L2__c = entity.labelL2;
		}
		if (entity.isChanged(ExpFixedAllowanceOptionEntity.Field.ORDER)) {
			sObj.Order__c = entity.order;
		}

		return sObj;
	}
}