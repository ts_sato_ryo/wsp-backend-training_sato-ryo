@isTest
private class TagRepositoryTest {

	private class RepoTestData {
		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public List<ComCompany__c> companyObjs;
		/** 検索テスト用のタグオブジェクト */
		public List<ComTag__c> tagObjs;

		/** コンストラクタ　*/
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObjs = new List<ComCompany__c>{
					ComTestDataUtility.createCompany('Company1', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company2', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company3', this.countryObj.Id)
			};
			tagObjs = createTagsForSearchTest(companyObjs);
		}

		/** 検索テスト用のタグオブジェクトを作成する */
		private List<ComTag__c> createTagsForSearchTest(List<ComCompany__c> companies) {
			List<ComTag__c> objs = new List<ComTag__c>();
			objs.add(ComTestDataUtility.createTag('tag1_1', TagType.JOB_ASSIGN_GROUP, companies[0].Id, 5, false));
			objs.add(ComTestDataUtility.createTag('tag1_2', TagType.JOB_ASSIGN_GROUP, companies[0].Id, 5, false));
			objs.add(ComTestDataUtility.createTag('tag1_3', TagType.JOB_ASSIGN_GROUP, companies[0].Id, 4, false));
			objs.add(ComTestDataUtility.createTag('tag1_4', TagType.SHORTEN_WORK_REASON, companies[0].Id, 2, false));
			objs.add(ComTestDataUtility.createTag('tag1_5', TagType.SHORTEN_WORK_REASON, companies[0].Id, 2, false));
			objs.add(ComTestDataUtility.createTag('tag1_6', TagType.SHORTEN_WORK_REASON, companies[0].Id, 1, false));
			objs.add(ComTestDataUtility.createTag('tag1_7', TagType.JOB_ASSIGN_GROUP, companies[0].Id, 7, true));
			objs.add(ComTestDataUtility.createTag('tag2', TagType.JOB_ASSIGN_GROUP, companies[1].Id, 8, false));
			objs.add(ComTestDataUtility.createTag('tag3', TagType.SHORTEN_WORK_REASON, companies[2].Id, 9, false));
			return objs;
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityTestById() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();
		TagEntity resTag;

		// 存在するタグIDを指定した場合
		// エンティティが取得できる
		resTag = repo.getEntity(testData.tagObjs[0].Id);
		System.assertNotEquals(null, resTag);
		System.assertEquals(testData.tagObjs[0].Id, resTag.id);

		// 論理削除されているタグIDを指定した場合
		// nullが返却される
		testData.tagObjs[0].Removed__c = true;
		update testData.tagObjs[0];
		resTag = repo.getEntity(testData.tagObjs[0].Id);
		System.assertEquals(null, resTag);

	}

	/**
	 * searchEntityListのテスト
	 * タグIDが一致するレコードを取得する事を確認する
	 */
	@isTest static void searchEntityListTestSearchById() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// タグIDで検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.ids = new Set<Id>{testData.tagObjs[0].Id};

		// 1件取得することを確認
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assertEquals(1, entities.size());

		// エンティティに正しく設定することを確認
		ComTag__c tag = testData.tagObjs[0];
		System.assertEquals(tag.Id, entities[0].Id);
		System.assertEquals(tag.UniqKey__c, entities[0].uniqKey);
		System.assertEquals(tag.Code__c, entities[0].code);
		System.assertEquals(tag.TagType__c, entities[0].tagTypeValue.value);
		System.assertEquals(tag.CompanyId__c, entities[0].companyId);
		System.assertEquals(tag.Name_L0__c, entities[0].nameL0);
		System.assertEquals(tag.Name_L1__c, entities[0].nameL1);
		System.assertEquals(tag.Name_L2__c, entities[0].nameL2);
		System.assertEquals(tag.Order__c, entities[0].order);
		System.assert(!entities[0].isRemoved);
	}

	/**
	 * searchEntityListのテスト
	 * タグIDが一致しないレコードを取得しない事を確認する
	 */
	@isTest static void searchEntityListTestSearchByIdNoResult() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// タグIDで検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.ids = new Set<Id>{Id.valueOf('001xa000003DIlo')};

		// 取得結果が存在しないを確認
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assert(entities.isEmpty());
	}

	/**
	 * searchEntityListのテスト
	 * 会社IDが一致するレコードを取得する事を確認する
	 */
	@isTest static void searchEntityListTestSearchByCompany() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// 会社IDで検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.companyIds = new Set<String>{testData.companyObjs[0].Id};

		// 削除済みレコード以外を全て取得していること、並び順、コードの昇順で取得していることを確認
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assertEquals(6, entities.size());
		System.assertEquals('tag1_6', entities[0].code);
		System.assertEquals('tag1_4', entities[1].code);
		System.assertEquals('tag1_5', entities[2].code);
		System.assertEquals('tag1_3', entities[3].code);
		System.assertEquals('tag1_1', entities[4].code);
		System.assertEquals('tag1_2', entities[5].code);
	}

	/**
	 * searchEntityListのテスト
	 * 会社IDが一致しないレコードを取得しない事を確認する
	 */
	@isTest static void searchEntityListTestSearchByCompanyNoResult() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// 会社IDで検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.companyIds = new Set<String>{'TEST_COMPANY_ID'};

		// 取得結果が存在しないを確認
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assert(entities.isEmpty());
	}

	/**
	 * searchEntityListのテスト
	 * タグタイプが一致するレコードを取得する事を確認する
	 */
	@isTest static void searchEntityListTestSearchByTagType() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// タグタイプで検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.tagTypes = new Set<String>{TagType.JOB_ASSIGN_GROUP.value};

		// タグタイプが一致するレコードを取得する事を確認
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assertEquals(4, entities.size());
		System.assertEquals('tag1_3', entities[0].code);
		System.assertEquals('tag1_1', entities[1].code);
		System.assertEquals('tag1_2', entities[2].code);
		System.assertEquals('tag2', entities[3].code);
	}

	/**
	 * searchEntityListのテスト
	 * タグタイプが一致しないレコードを取得しない事を確認する
	 */
	@isTest static void searchEntityListTestSearchByTagTypeNoResult() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// タグタイプで検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.tagTypes = new Set<String>{'TEST_TAG_TYPE'};

		// タグタイプが一致するレコードを取得する事を確認
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assert(entities.isEmpty());
	}

	/**
	 * searchEntityListのテスト
	 * 会社とタグタイプに一致するレコードを取得する事を確認する
	 */
	@isTest static void searchEntityListTestSearchByCompanyAndTagType() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// 全件検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.companyIds = new Set<String>{testData.companyObjs[0].id};
		filter.tagTypes = new Set<String>{TagType.SHORTEN_WORK_REASON.value};

		// 削除済み以外のレコードを全て取得する事を確認する
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assertEquals(3, entities.size());
		System.assertEquals('tag1_6', entities[0].code);
		System.assertEquals('tag1_4', entities[1].code);
		System.assertEquals('tag1_5', entities[2].code);
	}

	/**
	 * searchEntityListのテスト
	 * 削除済み以外のレコードを全て取得する事を確認する
	 */
	@isTest static void searchEntityListTestSearchAll() {
		RepoTestData testData = new RepoTestData();
		TagRepository repo = new TagRepository();

		// 全件検索
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();

		// 削除済み以外のレコードを全て取得する事を確認する
		List<TagEntity> entities = repo.searchEntityList(filter);
		System.assertEquals(8, entities.size());
		System.assertEquals('tag1_6', entities[0].code);
		System.assertEquals('tag1_4', entities[1].code);
		System.assertEquals('tag1_5', entities[2].code);
		System.assertEquals('tag1_3', entities[3].code);
		System.assertEquals('tag1_1', entities[4].code);
		System.assertEquals('tag1_2', entities[5].code);
		System.assertEquals('tag2', entities[6].code);
		System.assertEquals('tag3', entities[7].code);
	}
}
