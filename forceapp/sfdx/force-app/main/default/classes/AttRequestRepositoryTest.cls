/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠月次申請のリポジトリのテスト
 */
@isTest
private class AttRequestRepositoryTest {

	private class TestData {
		public ComCompany__c company;
		public ComDeptBase__c deptBase;
		public ComDeptHistory__c deptHistory;
		public AttWorkingTypeBase__c workingTypeBase;
		public ComPermission__c permission;
		public List<ComEmpBase__c> employeeBases;
		public List<ComEmpHistory__c> employeeHistories;
		public List<User> users;

		public TestData(Integer numberOfEmployees) {
			this.company = createCompany();
			this.deptBase = ComTestDataUtility.createDepartmentWithHistory('Test Dept', company.Id);
			this.deptHistory = [SELECT Id, Name, Name_L0__c, Name_L1__c, Name_L2__c FROM ComDeptHistory__c WHERE BaseId__c = :deptBase.Id];
			this.workingTypeBase = ComTestDataUtility.createAttWorkingTypeWithHistory('Test', company.Id);
			this.permission = ComTestDataUtility.createPermission('Test', company.Id);
			this.users = ComTestDataUtility.createUsers('Test', 'ja', 'ja_JP', numberOfEmployees);

			// 社員
			employeeBases = new List<ComEmpBase__c>();
			for (Integer i = 0; i < numberOfEmployees; i++) {
				employeeBases.add(createEmployeeBase(String.valueOf(i + 1), this.company.Id, this.deptBase.Id, this.users[i]));
			}
			insert employeeBases;
			employeeHistories = createEmployeeHistories(employeeBases);
		}

		/**
		 * 会社を作成する
		 */
		private ComCompany__c createCompany() {
			ComCompany__c company;
			List<ComCompany__c> companies = [SELECT Id, Name From ComCompany__c LIMIT 1];
			if (companies.isEmpty()) {
				company = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
			} else {
				company = companies[0];
			}
			return company;
		}

		/**
		 * テスト用の社員を作成する
		 * @param numberOfEmp 作成件数
		 * @return 社員リスト（履歴オブジェクトを返却)
		 */
		private ComEmpBase__c createEmployeeBase(String code, Id companyId, Id departmentBaseId, User user) {
			final String lastName = 'Yamada';
			final String firstName = 'Hanako';
			ComEmpBase__c base = new ComEmpBase__c(
					Name = lastName + ' ' + firstName + code,
					LastName_L0__c = lastName + '_L0',
					LastName_L1__c = lastName + '_L1',
					LastName_L2__c = lastName + '_L2',
					FirstName_L0__c = firstName + '_L0',
					FirstName_L1__c = firstName + '_L1',
					FirstName_L2__c = firstName + '_L2',
					DisplayName_L0__c = lastName + ' ' + firstName + '_L0',
					CompanyId__c = companyId,
					Code__c = code,
					UniqKey__c = code + '-key',
					UserId__c = user.Id);
			return base;
		}

		/**
		 * 現時点で有効な社員履歴オブジェクトを作成する
		 * @param 社員ベースオブジェクト
		 * @return 社員履歴オブジェクト
		 */
		private List<ComEmpHistory__c> createEmployeeHistories(List<ComEmpBase__c> employeeBases) {
			List<ComEmpHistory__c> histories = new List<ComEmpHistory__c>();

			for (Integer i = 0; i < employeeBases.size(); i++) {
				ComEmpHistory__c history = new ComEmpHistory__c(
						BaseId__c = employeeBases[i].Id,
						UniqKey__c = employeeBases[i].Code__c + '_' + String.valueOf(i * Math.random()),
						DepartmentBaseId__c = null,
						ManagerBaseId__c = null,
						PermissionId__c = this.permission.Id,
						HistoryComment__c = 'Comment_' + i,
						ValidFrom__c = System.today(),
						ValidTo__c = System.today() + 180);
				histories.add(history);
			}

			insert histories;

			// Baseに現在の履歴IDをセット
			for (Integer i = 0; i < employeeBases.size();  i++) {
				employeeBases[i].CurrentHistoryId__c = histories[i].Id;
			}
			update employeeBases;

			return histories;
		}

		/**
		 * 勤怠サマリーを作成する
		 */
		public AttSummary__c createSummary(Id employeeId, Id workingTypeId, AppDate startDate, AppDate endDate) {
			// サマリーデータ作成
			AttSummary__c summary = new AttSummary__c(
					Name = 'Test' + startDate.format(),
					EmployeeHistoryId__c = employeeId,
					WorkingTypeHistoryId__c = workingTypeId,
					UniqKey__c = 'Test' + startDate.formatYYYYMMDD(),
					StartDate__c = startDate.getDate(),
					EndDate__c = endDate.getDate(),
					SummaryName__c = startDate.format(AppDate.FormatType.YYYYMM)); // 再計算させるため
			return summary;
		}

		/**
		 * 申請オブジェクトを作成する
		 */
		public AttRequest__c createRequest(Id summaryId, AppRequestStatus status, AppCancelType cancelType,
		 		Id employeeHistoryId, Id actorId, Id departmentId, Id lastApproverId, Id approver01Id) {
			AttRequest__c request = new AttRequest__c(
					Name = 'Test Name',
					AttSummaryId__c = summaryId == null ? null : summaryId,
					Comment__c = 'Test Comment',
					ProcessComment__c = 'Test ProcessComment',
					Status__c = status.value,
					ConfirmationRequired__c = true,
					CancelType__c = cancelType.value,
					EmployeeHistoryId__c = employeeHistoryId,
					RequestTime__c = System.now(),
					DepartmentHistoryId__c = departmentId,
					ActorHistoryId__c = actorId,
					LastApproveTime__c = System.now().addDays(1),
					LastApproverId__c = lastApproverId,
					Approver01Id__c = approver01Id);
			return request;
		}
	}

	/**
	 * getEntityのテスト
	 * 指定した申請IDのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityTest() {

		final Integer numberOfEmployees = 3;
		AttRequestRepositoryTest.TestData testData = new TestData(numberOfEmployees);
		AttSummary__c summary1 = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 10, 1),
				AppDate.newInstance(2017, 10, 31));
		AttSummary__c summary2 = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 11, 1),
				AppDate.newInstance(2017, 11, 30));
		insert new List<AttSummary__c>{summary1, summary2};

		AttRequest__c request1 = testData.createRequest(summary1.Id, AppRequestStatus.DISABLED, AppCancelType.REMOVED,
				testData.employeeHistories[0].Id, testData.employeeHistories[1].Id,
				testData.deptHistory.Id, testData.employeeBases[0].UserId__c, testData.employeeBases[1].UserId__c);
		AttRequest__c request2 = testData.createRequest(summary2.Id, AppRequestStatus.DISABLED, AppCancelType.REMOVED,
				testData.employeeHistories[0].Id, testData.employeeHistories[1].Id,
				testData.deptHistory.Id, testData.employeeBases[0].UserId__c, testData.employeeBases[1].UserId__c);
		insert new List<AttRequest__c>{request1, request2};

		Test.startTest();
			AttRequestRepository repo = new AttRequestRepository();
			AttRequestEntity entity = repo.getEntity(request2.Id);
		Test.stopTest();
		System.assertNotEquals(null, entity);
		assertEntity(request2, entity);
	}

	/**
	 * getEntityのテスト
	 * 指定した申請IDのエンティティが存在しない場合はnullが返却されることを確認する
	 */
	@isTest static void getEntityTestNotFound() {
		AttRequestRepository repo = new AttRequestRepository();
		AttRequestEntity entity = repo.getEntity(UserInfo.getUserId());

		System.assertEquals(null, entity);
	}

	/**
	 * getEntityListBySummaryIdのテスト
	 * 指定したサマリーIDを持つエンティティが取得できることを確認する
	 */
	@isTest static void getEntityListBySummaryIdTest() {

		final Integer numberOfEmployees = 3;
		AttRequestRepositoryTest.TestData testData = new TestData(numberOfEmployees);
		AttSummary__c summary1 = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 10, 1),
				AppDate.newInstance(2017, 10, 31));
		AttSummary__c summary2 = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 11, 1),
				AppDate.newInstance(2017, 11, 30));
		insert new List<AttSummary__c>{summary1, summary2};

		AttRequest__c request1 = testData.createRequest(summary1.Id, AppRequestStatus.DISABLED, AppCancelType.REMOVED,
				testData.employeeHistories[0].Id, testData.employeeHistories[1].Id,
				testData.deptHistory.Id, testData.employeeBases[0].UserId__c, testData.employeeBases[1].UserId__c);
		AttRequest__c request2 = testData.createRequest(summary2.Id, AppRequestStatus.DISABLED, AppCancelType.REMOVED,
				testData.employeeHistories[0].Id, testData.employeeHistories[1].Id,
				testData.deptHistory.Id, testData.employeeBases[0].UserId__c, testData.employeeBases[1].UserId__c);
		insert new List<AttRequest__c>{request1, request2};

		Test.startTest();
			AttRequestRepository repo = new AttRequestRepository();
			List<AttRequestEntity> entities = repo.getEntityListBySummaryId(summary1.Id);
		Test.stopTest();
		System.assertEquals(1, entities.size());
		assertEntity(request1, entities[0]);
	}

	/**
	 * saveEntityのテスト
	 * 必須項目のみ値が設定されている状態で保存できることを確認する
	 */
	@isTest static void saveEntityTestEmpty() {
		final Integer numberOfEmployees = 3;
		AttRequestRepositoryTest.TestData testData = new TestData(numberOfEmployees);
		AttSummary__c summary = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 10, 1),
				AppDate.newInstance(2017, 10, 31));
		insert summary;

		AttRequestEntity entity = new AttRequestEntity();
		entity.name = 'Test';
		entity.summaryId = summary.Id;

		Test.startTest();
			AttRequestRepository repo = new AttRequestRepository();
			Repository.SaveResult result = repo.saveEntity(entity);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);
		AttRequest__c savedObj = getRequest(result.details[0].id);
		System.assertNotEquals(null, savedObj);
		System.assertEquals(entity.name, savedObj.Name);
	}

	/**
	 * getEntityListByStatusのテスト
	 * 指定したステータスのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityListByStatusTest() {

		final Integer numberOfEmployees = 3;
		AttRequestRepositoryTest.TestData testData = new TestData(numberOfEmployees);
		AttSummary__c summary1 = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 10, 1),
				AppDate.newInstance(2017, 10, 31));
		AttSummary__c summary2 = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 11, 1),
				AppDate.newInstance(2017, 11, 30));
		insert new List<AttSummary__c>{summary1, summary2};

		AttRequest__c requestDisable = testData.createRequest(summary1.Id, AppRequestStatus.DISABLED, AppCancelType.REMOVED,
				testData.employeeHistories[0].Id, testData.employeeHistories[1].Id,
				testData.deptHistory.Id, testData.employeeBases[0].UserId__c, testData.employeeBases[1].UserId__c);
		AttRequest__c requestPending = testData.createRequest(summary2.Id, AppRequestStatus.PENDING, AppCancelType.REMOVED,
				testData.employeeHistories[0].Id, testData.employeeHistories[1].Id,
				testData.deptHistory.Id, testData.employeeBases[0].UserId__c, testData.employeeBases[1].UserId__c);
		AttRequest__c requestApproved = testData.createRequest(summary2.Id, AppRequestStatus.APPROVED, AppCancelType.REMOVED,
				testData.employeeHistories[0].Id, testData.employeeHistories[1].Id,
				testData.deptHistory.Id, testData.employeeBases[0].UserId__c, testData.employeeBases[1].UserId__c);
		insert new List<AttRequest__c>{requestDisable, requestPending, requestApproved};

		AttRequestRepository repo = new AttRequestRepository();
		List<AppRequestStatus> targetStatusList;
		List<AttRequestEntity> entityList;

		// Test1: ステータスを1個指定
		targetStatusList = new List<AppRequestStatus>{AppRequestStatus.PENDING};
		entityList = repo.getEntityListByStatus(targetStatusList);
		System.assertEquals(1, entityList.size());
		System.assertEquals(requestPending.Id, entityList[0].id);

		// Test2: ステータスを複数指定
		targetStatusList = new List<AppRequestStatus>{AppRequestStatus.DISABLED, AppRequestStatus.APPROVED};
		entityList = repo.getEntityListByStatus(targetStatusList);
		System.assertEquals(2, entityList.size());
		System.assertEquals(requestDisable.Id, entityList[0].id);
		System.assertEquals(requestApproved.Id, entityList[1].id);
	}

	/**
	 * saveEntityのテスト
	 * 全てのエンティティの項目が保存できることを確認する
	 */
	@isTest static void saveEntityTestAllField() {

		final Integer numberOfEmployees = 3;
		AttRequestRepositoryTest.TestData testData = new TestData(numberOfEmployees);

		AttSummary__c summary1 = testData.createSummary(
				testData.employeeBases[0].CurrentHistoryId__c,
				testData.workingTypeBase.CurrentHistoryId__c,
				AppDate.newInstance(2017, 10, 1),
				AppDate.newInstance(2017, 10, 31));
		insert summary1;

		AttRequestEntity entity = new AttRequestEntity();
		entity.name = 'Test';
		entity.summaryId = summary1.Id;
		entity.comment = 'Test Comment';
		entity.processComment = 'Test ProcessComment';
		entity.status = AppRequestStatus.DISABLED;
		entity.isConfirmationRequired = true;
		entity.cancelType = AppCancelType.REJECTED;
		entity.employeeHistoryId = testData.employeeHistories[0].Id;
		entity.requestTime = AppDatetime.now();
		entity.departmentHistoryId = testData.deptHistory.Id;
		entity.actorId = testData.employeeHistories[1].Id;
		entity.lastApproveTime = AppDatetime.now().addDays(1);
		entity.lastApproverId = testData.employeeBases[0].UserId__c;
		entity.approver01Id = testData.employeeBases[1].UserId__c;

		Test.startTest();
			AttRequestRepository repo = new AttRequestRepository();
			Repository.SaveResult result = repo.saveEntity(entity);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);
		AttRequest__c savedObj = getRequest(result.details[0].id);
		System.assertNotEquals(null, savedObj);
		assertSObject(entity, savedObj);
	}

	/**
	 * 取得したAttRequest__cの値を検証する
	 * @param expEntity 期待する値を持つエンティティ
	 * @param actObj 実際のSObject
	 */
	private static void assertSObject(AttRequestEntity expEntity, AttRequest__c actObj) {

		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.summaryId, actObj.AttSummaryId__c);
		System.assertEquals(expEntity.comment, actObj.Comment__c);
		System.assertEquals(expEntity.processComment, actObj.ProcessComment__c);
		System.assertEquals(expEntity.status.value, actObj.Status__c);
		System.assertEquals(expEntity.isConfirmationRequired, actObj.ConfirmationRequired__c);
		System.assertEquals(expEntity.cancelType.value, actObj.CancelType__c);
		System.assertEquals(expEntity.requestTime.getDatetime(), actObj.RequestTime__c);
		System.assertEquals(expEntity.lastApproveTime.getDatetime(), actObj.LastApproveTime__c);
		System.assertEquals(expEntity.lastApproverId, actObj.LastApproverId__c);
		System.assertEquals(expEntity.employeeHistoryId, actObj.EmployeeHistoryId__c);
		System.assertEquals(expEntity.actorId, actObj.ActorHistoryId__c);
		System.assertEquals(expEntity.departmentHistoryId, actObj.DepartmentHistoryId__c);
		System.assertEquals(expEntity.approver01Id, actObj.Approver01Id__c);

	}

	/**
	 * 取得したAttRequestEntityの値を検証する
	 * @param expObj 期待する値を持つSObject
	 * @param actEntity 実際に取得した値
	 */
	private static void assertEntity(AttRequest__c expObj, AttRequestEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.AttSummaryId__c, actEntity.summaryId);
		System.assertEquals(expObj.Comment__c, actEntity.comment);
		System.assertEquals(expObj.ProcessComment__c, actEntity.processComment);
		System.assertEquals(expObj.Status__c, actEntity.status.value);
		System.assertEquals(expObj.ConfirmationRequired__c, actEntity.isConfirmationRequired);
		System.assertEquals(expObj.CancelType__c, actEntity.cancelType.value);
		System.assertEquals(expObj.RequestTime__c, actEntity.requestTime.getDatetime());
		System.assertEquals(expObj.LastApproveTime__c, actEntity.lastApproveTime.getDatetime());
		System.assertEquals(expObj.LastApproverId__c, actEntity.lastApproverId);
		System.assertEquals(expObj.Approver01Id__c, actEntity.approver01Id);

		// 社員の情報
		System.assertEquals(expObj.EmployeeHistoryId__c, actEntity.employeeHistoryId);
		ComEmpHistory__c expEmpHistoryObj = getEmployeeHistory(expObj.EmployeeHistoryId__c);
		System.assertEquals(expEmpHistoryObj.BaseId__c, actEntity.employeeBaseId);
		assertEmployee(expEmpHistoryObj, actEntity.employee);
		System.assertEquals(expObj.ActorHistoryId__c, actEntity.actorId);
		assertEmployee(getEmployeeHistory(expObj.ActorHistoryId__c), actEntity.actor);

		// 部署の情報
		System.assertEquals(expObj.DepartmentHistoryId__c, actEntity.departmentHistoryId);
		assertDepartment(getDepartmentHistory(expObj.DepartmentHistoryId__c), actEntity.department);
	}

	/**
	 * 社員の情報が取得できていることを確認する
	 * @param expObj 期待する値を持つSObject
	 * @param actEntity 実際に取得した値
	 */
	private static void assertEmployee(ComEmpHistory__c expObj, AttLookup.Employee actEntity) {
		System.assertEquals(expObj.BaseId__r.FirstName_L0__c, actEntity.fullName.firstNameL0);
		System.assertEquals(expObj.BaseId__r.FirstName_L1__c, actEntity.fullName.firstNameL1);
		System.assertEquals(expObj.BaseId__r.FirstName_L2__c, actEntity.fullName.firstNameL2);
		System.assertEquals(expObj.BaseId__r.LastName_L0__c, actEntity.fullName.lastNameL0);
		System.assertEquals(expObj.BaseId__r.LastName_L1__c, actEntity.fullName.lastNameL1);
		System.assertEquals(expObj.BaseId__r.LastName_L2__c, actEntity.fullName.lastNameL2);
		System.assertEquals(expObj.ValidFrom__c, actEntity.validFrom.getDate());
		System.assertEquals(expObj.ValidTo__c, actEntity.validTo.getDate());
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);
	}

	/**
	 * 部署の情報が取得できていることを確認する
	 * @param expObj 期待する値を持つSObject
	 * @param actEntity 実際に取得した値
	 */
	private static void assertDepartment(ComDeptHistory__c expObj, AttLookup.Department actEntity) {
		System.assertEquals(expObj.Name_L0__c, actEntity.name.valueL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.name.valueL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.name.valueL2);
		System.assertEquals(expObj.ValidTo__c, actEntity.validTo.getDate());
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);
	}

	/**
	 * 指定した履歴IDの社員履歴オブジェクトを取得する
	 */
	private static ComEmpHistory__c getEmployeeHistory(Id historyId) {
		List<ComEmpHistory__c> objs = [
				SELECT Id, Name,
					BaseId__r.FirstName_L0__c, BaseId__r.FirstName_L1__c, BaseId__r.FirstName_L2__c,
					BaseId__r.LastName_L0__c, BaseId__r.LastName_L1__c, BaseId__r.LastName_L2__c,
					ValidFrom__c, ValidTo__c, Removed__c
				FROM ComEmpHistory__c
				WHERE Id = :historyId];
		return objs[0];
	}

	/**
	 * 指定した履歴IDの部署履歴オブジェクトを取得する
	 */
	private static ComDeptHistory__c getDepartmentHistory(Id historyId) {
		List<ComDeptHistory__c> objs = [
				SELECT Id, Name, Name_L0__c, Name_L1__c, Name_L2__c,
					ValidFrom__c, ValidTo__c, Removed__c
				FROM ComDeptHistory__c
				WHERE Id = :historyId];
		return objs[0];
	}

	/**
	 * 指定した申請IDの申請オブジェクトを取得する
	 */
	private static AttRequest__c getRequest(Id requestId) {
		List<AttRequest__c> objs = [
				SELECT
					Id, Name, AttSummaryId__c, Comment__c, ProcessComment__c,
					Status__c, ConfirmationRequired__c, CancelType__c,
					EmployeeHistoryId__c, RequestTime__c, DepartmentHistoryId__c,
					ActorHistoryId__c, LastApproveTime__c, LastApproverId__c,
					Approver01Id__c
				FROM AttRequest__c
				WHERE Id = :requestId];
		return objs[0];
	}
}
