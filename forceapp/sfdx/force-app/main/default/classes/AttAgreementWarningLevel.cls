/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 36協定警告レベル
 */
public with sharing class AttAgreementWarningLevel extends ValueObjectType {
	/** 警告1 */
	public static final AttAgreementWarningLevel WARNING1 = new AttAgreementWarningLevel('Warning1');
	/** 警告2 */
	public static final AttAgreementWarningLevel WARNING2 = new AttAgreementWarningLevel('Warning2');
	/** 限度(キーワードのため[_NORMAL]を追加) */
	public static final AttAgreementWarningLevel LIMIT_NORMAL = new AttAgreementWarningLevel('Limit');
	/** 警告1・特別 */
	public static final AttAgreementWarningLevel WARNING1_SPECIAL = new AttAgreementWarningLevel('Warning1Special');
	/** 警告2・特別 */
	public static final AttAgreementWarningLevel WARNING2_SPECIAL = new AttAgreementWarningLevel('Warning2Special');
	/** 限度・特別 */
	public static final AttAgreementWarningLevel LIMIT_SPECIAL = new AttAgreementWarningLevel('LimitSpecial');

	/** 警告レベル別のリス */
	public static final List<AttAgreementWarningLevel> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttAgreementWarningLevel> {
			WARNING1,
			WARNING2,
			LIMIT_NORMAL,
			WARNING1_SPECIAL,
			WARNING2_SPECIAL,
			LIMIT_SPECIAL
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttAgreementWarningLevel(String value) {
		super(value);
	}

	/**
	 * AttAgreementWarningLevel
	 * @param 取得対象のインスタンスが持つ値
	 * @return AttAgreementWarningLevel、ただし該当するAppCancelTypeが存在しない場合はnull
	 */
	public static AttAgreementWarningLevel valueOf(String value) {
		AttAgreementWarningLevel retType = null;
		if (String.isNotBlank(value)) {
			for (AttAgreementWarningLevel type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttDayType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttAgreementWarningLevel) {
			// 値が同じであればtrue
			if (this.value == ((AttAgreementWarningLevel)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}