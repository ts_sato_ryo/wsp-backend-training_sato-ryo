/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数サマリーのリポジトリ
 */
public with sharing class TimeSummaryRepository extends Repository.BaseRepository {

	/** 子リレーション名（工数明細） */
	private static final String CHILD_RELATION_RECORDS = 'TimeTrackRecords__r';

	/** 子リレーション名（工数内訳） */
	private static final String CHILD_RELATION_RECORD_ITEMS = 'TimeRecordItems__r';

	/** リレーション名（工数確定申請） */
	private static final String TIME_REQUEST_R =
			TimeSummary__c.RequestId__c.getDescribe().getRelationshipName();

	/** リレーション名（社員） */
	private static final String EMP_R =
			TimeSummary__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	private static final String EMP_BASE_R =
			ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();

	/** リレーション名（ユーザ） */
	private static final String USER_R =
			ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();

	/** リレーション名（工数サマリー） */
	private static final String SUMMARY_R =
			TimeRecord__c.TimeSummaryId__c.getDescribe().getRelationshipName();

	/** リレーション名（ジョブ） */
	private static final String JOB_R =
			TimeRecordItem__c.JobId__c.getDescribe().getRelationshipName();

	/** リレーション名（作業分類） */
	private static final String WORK_CATEGORY_R =
			TimeRecordItem__c.WorkCategoryId__c.getDescribe().getRelationshipName();

	/** 取得対象の項目(工数サマリー) */
	private static final Set<Schema.SObjectField> GET_SUMMARY_FIELD_SET;
	static {
		GET_SUMMARY_FIELD_SET = new Set<Schema.SObjectField> {
			TimeSummary__c.Id,
			TimeSummary__c.Name,
			TimeSummary__c.UniqKey__c,
			TimeSummary__c.RequestId__c,
			TimeSummary__c.EmployeeHistoryId__c,
			TimeSummary__c.TimeSettingHistoryId__c,
			TimeSummary__c.SummaryName__c,
			TimeSummary__c.SubNo__c,
			TimeSummary__c.StartDate__c,
			TimeSummary__c.EndDate__c,
			TimeSummary__c.Time__c,
			TimeSummary__c.Locked__c
		};
	}

	/** 取得対象の項目(工数明細) */
	private static final Set<Schema.SObjectField> GET_RECORD_FIELD_SET;
	static {
		GET_RECORD_FIELD_SET = new Set<Schema.SObjectField> {
			TimeRecord__c.Id,
			TimeRecord__c.Name,
			TimeRecord__c.UniqKey__c,
			TimeRecord__c.TimeSummaryId__c,
			TimeRecord__c.Date__c,
			TimeRecord__c.Note__c,
			TimeRecord__c.Output__c,
			TimeRecord__c.Time__c
		};
	}

	/** 取得対象の項目(工数内訳) */
	private static final Set<Schema.SObjectField> GET_RECORD_ITEM_FIELD_SET;
	static {
		GET_RECORD_ITEM_FIELD_SET = new Set<Schema.SObjectField> {
			TimeRecordItem__c.Id,
			TimeRecordItem__c.Name,
			TimeRecordItem__c.TimeRecordId__c,
			TimeRecordItem__c.JobId__c,
			TimeRecordItem__c.WorkCategoryId__c,
			TimeRecordItem__c.Order__c,
			TimeRecordItem__c.TaskNote__c,
			TimeRecordItem__c.TimeDirect__c,
			TimeRecordItem__c.Time__c,
			TimeRecordItem__c.Volume__c,
			TimeRecordItem__c.Ratio__c
		};
	}

	/** 検索フィルタ(工数サマリー) */
	private class SummaryFilter {
		/** 工数サマリーID */
		public List<Id> summaryIds;
		/** 社員ID */
		public List<Id> employeeIdList;
		/** 取得対象範囲(開始) */
		public AppDate startDate;
		/** 取得対象範囲(終了) */
		public AppDate endDate;
	}

	/** 検索フィルタ(工数明細) */
	private class RecordFilter {
		/** 工数サマリーID */
		public List<Id> summaryIdList;
		/** 社員ID */
		public Id employeeId;
		/** 取得対象範囲(開始) */
		public AppDate startDate;
		/** 取得対象範囲(終了) */
		public AppDate endDate;
	}

	/** 工数サマリーの保存結果(工数明細保存結果も含む) */
	public class SavaSummaryResult extends Repository.SaveResult {
		/** 工数明細の処理結果の詳細 */
		public List<ResultDetail> recordDetails {get; set;}
	}

	/** 工数明細の保存結果(工数内訳保存結果も含む) */
	public class SavaRecordResult extends Repository.SaveResult {
		/** 工数内訳の処理結果の詳細 */
		public List<ResultDetail> recordItemDetails {get; set;}
	}

	/**
	 * 工数サマリーを取得する
	 * @param summaryId 社員ID
	 * @return 条件に一致した工数サマリー(工数明細を含まない)
	 */
	public TimeSummaryEntity getSummary(Id summaryId) {
		// 検索条件を作成する
		SummaryFilter filter = new SummaryFilter();
		filter.summaryIds = new List<Id> {summaryId};

		// 検索を実行する
		final Boolean withRecords = false;
		final Boolean forUpdate = false;
		List<TimeSummaryEntity> summaries = searchSummary(filter, withRecords, forUpdate);
		return summaries.isEmpty() ? null : summaries[0];
	}

	/**
	 * 工数サマリーを取得する
	 * @param employeeId 社員ID
	 * @param targetDate 対象日付
	 * @return 条件に一致した工数サマリーのリスト(工数明細含まない)
	 */
	public List<TimeSummaryEntity> getSummaryList(Id employeeId, AppDate targetDate) {
		// 検索条件を作成する
		SummaryFilter filter = new SummaryFilter();
		filter.employeeIdList = new List<Id> {employeeId};
		filter.startDate = targetDate;
		filter.endDate = targetDate;

		// 検索を実行する
		final Boolean withRecords = false;
		final Boolean forUpdate = false;

		return searchSummary(filter, withRecords, forUpdate);
	}

	/**
	 * 工数明細を取得する
	 * @param summaryId 工数サマリーID
	 * @return 条件に一致した工数明細のリスト(日付順、工数内訳含む)
	 */
	public List<TimeRecordEntity> getRecordListBySummaryId(Id summaryId) {
		// 検索条件を作成する
		RecordFilter filter = new RecordFilter();
		filter.summaryIdList = new List<Id> {summaryId};

		// 検索を実行する
		final Boolean withRecordItems = true;
		final Boolean forUpdate = false;

		return searchRecord(filter, withRecordItems, forUpdate);
	}

	/**
	 * 工数明細を取得する
	 * @param employeeId 社員ID
	 * @param targetDate 対象日
	 * @return 条件に一致した工数明細(工数内訳含む)
	 *         該当データが存在しない場合はnull
	 */
	public TimeRecordEntity getRecord(Id employeeId, AppDate targetDate) {
		List<TimeRecordEntity> records = getRecordList(employeeId, targetDate, targetDate);
		return records.isEmpty() ? null : records[0];
	}

	/**
	 * 工数明細を取得する
	 * @param employeeId 社員ID
	 * @param startDate 対象開始日
	 * @param endDate 対象終了日
	 * @return 条件に一致した工数明細(工数内訳含む)
	 */
	public List <TimeRecordEntity> getRecordList(Id employeeId, AppDate startDate, AppDate endDate) {
		// 検索条件を作成する
		RecordFilter filter = new RecordFilter();
		filter.employeeId = employeeId;
		filter.startDate = startDate;
		filter.endDate = endDate;

		// 検索を実行する
		final Boolean withRecordItems = true;
		final Boolean forUpdate = false;

		return searchRecord(filter, withRecordItems, forUpdate);
	}

	/**
	 * 工数サマリーエンティティを保存する
	 * @param entity 保存対象の工数サマリーエンティティ
	 * @param withRecords 工数明細も一緒に保存する場合はtrue
	 * @return 保存結果
	 */
	public SavaSummaryResult saveSummaryEntity(TimeSummaryEntity entity, Boolean withRecords) {
		return saveSummaryEntityList(new List<TimeSummaryEntity>{entity}, withRecords, true);
	}

	/**
	 * 工数サマリーエンティティを保存する
	 * @param entityList 保存対象の工数サマリーエンティティのリスト
	 * @param withRecords 工数明細も一緒に保存する場合はtrue
	 * @return 保存結果
	 */
	public SavaSummaryResult saveSummaryEntityList(List<TimeSummaryEntity> entityList, Boolean withRecords) {
		return saveSummaryEntityList(entityList, withRecords, true);
	}

	/**
	 * 工数サマリーエンティティを保存する
	 * @param entityList 保存対象の工数サマリーエンティティのリスト
	 * @param withRecords 工数明細も一緒に保存する場合はtrue
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(工数サマリーと工数明細の結果)
	 */
	public SavaSummaryResult saveSummaryEntityList(List<TimeSummaryEntity> entityList, Boolean withRecords, Boolean allOrNone) {
		List<TimeSummary__c> sumObjList = new List<TimeSummary__c>();

		// 工数サマリー保存用のオブジェクトを作成する
		for (TimeSummaryEntity entity : entityList) {
			// エンティティからSObjectを作成する
			TimeSummary__c sumObj = entity.createSObject();
			sumObjList.add(sumObj);
		}
		// DBに保存する
		List<Database.UpsertResult> sumResList = AppDatabase.doUpsert(sumObjList, allOrNone);

		// 工数明細保存用のオブジェクトを作成する
		List<TimeRecord__c> recObjList = new List<TimeRecord__c>();
		List<Database.UpsertResult> recResList;
		if (withRecords) {
			for (Integer i = 0; i < sumResList.size(); i++) {
				Database.UpsertResult res = sumResList[i];
				TimeSummary__c sumObj = sumObjList[i];
				TimeSummaryEntity sumEntity = entityList[i];

				// サマリーの保存に失敗している場合は明細も保存しない次のレコードへ
				if (!res.isSuccess()) {
					continue;
				}

				// 工数明細リストがnull、または空の場合は保存対象外
				if ((sumEntity.recordList == null) || (sumEntity.recordList.isEmpty())) {
					continue;
				}

				// 工数明細オブジェクトをエンティティから作成する
				for (TimeRecordEntity recEntity : sumEntity.recordList) {
					TimeRecord__c recObj = recEntity.createSObject();
					if (recObj.Id == null) {
						recObj.TimeSummaryId__c = res.getId();
						recObj.UniqKey__c = res.getId() + recEntity.recordDate.formatYYYYMMDD();
					}
					recObjList.add(recObj);
				}
			}
			// 工数明細オブジェクトを保存する
			recResList = AppDatabase.doUpsert(recObjList, allOrNone);
		}

		// 結果作成
	 	Repository.SaveResult repoSumRes = resultFactory.createSaveResult(sumResList);
		SavaSummaryResult repoRes = new SavaSummaryResult();
		repoRes.details = repoSumRes.details;
		if (withRecords) {
		 	Repository.SaveResult repoRecRes = resultFactory.createSaveResult(recResList);
			repoRes.isSuccessAll = repoSumRes.isSuccessAll && repoRecRes.isSuccessAll;
			repoRes.recordDetails = repoRecRes.details;
		} else {
			repoRes.isSuccessAll = repoSumRes.isSuccessAll;
		}

		return repoRes;
	}

	/**
	 * 工数明細エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @param withRecordItems 工数内訳も一緒に保存する場合はtrue
	 * @return 保存結果
	 */
	public SaveResult saveRecordEntity(TimeRecordEntity entity, Boolean withRecordItems) {
		return saveRecordEntityList(new List<TimeRecordEntity>{entity}, withRecordItems, true);
	}

	/**
	 * 工数明細エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param withRecordItems 工数内訳も一緒に保存する場合はtrue
	 * @return 保存結果
	 */
	public SaveResult saveRecordEntityList(List<TimeRecordEntity> entityList, Boolean withRecordItems) {
		return saveRecordEntityList(entityList, withRecordItems, true);
	}

	/**
	 * 工数明細エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param withRecordItems 工数内訳も一緒に保存する場合はtrue
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveRecordEntityList(List<TimeRecordEntity> entityList, Boolean withRecordItems, Boolean allOrNone) {
		List<TimeRecord__c> recObjList = new List<TimeRecord__c>();

		// 工数明細保存用のオブジェクトを作成する
		for (TimeRecordEntity entity : entityList) {
			// エンティティからSObjectを作成する
			TimeRecord__c recObj = entity.createSObject();
			recObjList.add(recObj);
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(recObjList, allOrNone);

		// 工数内訳保存用のオブジェクトを作成する
		List<TimeRecordItem__c> itemObjList = new List<TimeRecordItem__c>();
		List<Database.UpsertResult> itemResList;
		if (withRecordItems) {
			for (Integer i = 0; i < recResList.size(); i++) {
				Database.UpsertResult res = recResList[i];
				TimeRecord__c recObj = recObjList[i];
				TimeRecordEntity recEntity = entityList[i];

				// 明細の保存に失敗している場合は内訳も保存しない次のレコードへ
				if (!res.isSuccess()) {
					continue;
				}

				// 工数内訳リストがnull、または空の場合は保存対象外
				if ((recEntity.recordItemList == null) || (recEntity.recordItemList.isEmpty())) {
					continue;
				}

				// 工数内訳オブジェクトをエンティティから作成する
				for (TimeRecordItemEntity itemEntity : recEntity.recordItemList) {
					TimeRecordItem__c itemObj = itemEntity.createSObject();
					if (itemObj.Id == null) {
						itemObj.TimeRecordId__c = res.getId();
					}
					itemObjList.add(itemObj);
				}
			}
			// 工数内訳オブジェクトを保存する
			itemResList = AppDatabase.doUpsert(itemObjList, allOrNone);
		}

		// 結果作成
	 	Repository.SaveResult repoRecRes = resultFactory.createSaveResult(recResList);
		SavaRecordResult repoRes = new SavaRecordResult();
		repoRes.details = repoRecRes.details;
		if (withRecordItems) {
		 	Repository.SaveResult repoItemRes = resultFactory.createSaveResult(itemResList);
			repoRes.isSuccessAll = repoRecRes.isSuccessAll && repoItemRes.isSuccessAll;
			repoRes.recordItemDetails = repoItemRes.details;
		} else {
			repoRes.isSuccessAll = repoRecRes.isSuccessAll;
		}

		return repoRes;
	}

	/**
	 * 工数サマリーを検索する
	 * @param employeeId 社員ID(ベース)
	 * @param range 対象期間
	 * @return IDをキーにした条件に一致する工数サマリーのマップ
	 * 対象期間内の日を1日でも含む工数サマリーを全て取得する
	 */
	public Map<Id, TimeSummaryEntity> searchSummaryMap(Id employeeId, AppDateRange range) {
		SummaryFilter filter = new SummaryFilter();
		filter.employeeIdList = new List<Id>{employeeId};
		// 対象期間内の日を1日でも含む　を満たすために以下のように設定する
		//  = 対象期間終了日が有効期間開始日より前
		//  = 対象期間開始日が有効期間終了日より後
		filter.startDate = range.endDate;
		filter.endDate = range.startDate;

		List<TimeSummaryEntity> summaryList = searchSummary(filter, true, false);
		Map<Id, TimeSummaryEntity> summaryMap = new Map<Id, TimeSummaryEntity>();
		if (summaryList == null || summaryList.isEmpty()){
			return summaryMap;
		}
		for (TimeSummaryEntity summary : summaryList) {
			summaryMap.put(summary.id, summary);
		}
		return summaryMap;
	}

	/**
	 * 工数サマリーを検索する
	 * @param filter 検索条件
	 * @param withRecords 工数明細も一緒に取得する場合はtrue
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した工数サマリーのリスト
	 */
	private List<TimeSummaryEntity> searchSummary(SummaryFilter filter,
			Boolean withRecords, Boolean forUpdate) {

		// SOQLを作成する
		// 工数サマリーの項目
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_SUMMARY_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// 工数確定申請
		selectFldList.add(TIME_REQUEST_R + '.' + getFieldName(TimeRequest__c.Status__c));
		selectFldList.add(TIME_REQUEST_R + '.' + getFieldName(TimeRequest__c.ConfirmationRequired__c));
		selectFldList.add(TIME_REQUEST_R + '.' + getFieldName(TimeRequest__c.CancelType__c));
		// 社員
		selectFldList.add(EMP_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.FirstName_L0__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.LastName_L0__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.FirstName_L1__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.LastName_L1__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.FirstName_L2__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + getFieldName(ComEmpBase__c.LastName_L2__c));
		selectFldList.add(EMP_R + '.' + EMP_BASE_R + '.' + USER_R + '.' + getFieldName(User.SmallPhotoUrl));
		selectFldList.add(EMP_R + '.' + getFieldName(ComEmpHistory__c.DepartmentBaseId__c));

		// 工数明細の項目
		if (withRecords == true) {
			List<String> recordFldList = new List<String>();
			for (Schema.SObjectField fld : GET_RECORD_FIELD_SET) {
				recordFldList.add(getFieldName(fld));
			}
			selectFldList.add(
					'(SELECT ' + String.join(recordFldList, ',')
					+ ' FROM ' + CHILD_RELATION_RECORDS
					+ ' ORDER BY ' + getFieldName(TimeRecord__c.Date__c) + ' Asc)');
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> summaryIds = filter.summaryIds;
		final List<Id> empIds = filter.employeeIdList;
		final Date startDate = AppDate.convertDate(filter.startDate);
		final Date endDate = AppDate.convertDate(filter.endDate);
		if ((summaryIds != null) && (!summaryIds.isEmpty())) {
			condList.add(getFieldName(TimeSummary__c.Id) + ' IN :summaryIds');
		}
		if ((empIds != null) && (!empIds.isEmpty())) {
			condList.add(EMP_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c) + ' IN :empIds');
		}
		if (filter.startDate != null) {
			condList.add(getFieldName(TimeSummary__c.StartDate__c) + ' <= :startDate');
		}
		if (filter.endDate != null) {
			condList.add(getFieldName(TimeSummary__c.EndDate__c) + ' >= :endDate');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + TimeSummary__c.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond;
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('TimeSummaryRepository.searchSummary: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<TimeSummaryEntity> entityList = new List<TimeSummaryEntity>();
		for (SObject sObj : sObjList) {
			entityList.add(createSummaryEntity((TimeSummary__c)sObj));
		}

		return entityList;
	}

	/**
	 * 工数明細を検索する
	 * @param filter 検索条件
	 * @param withRecordItems 工数内訳も一緒に取得する場合はtrue
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した工数明細のリスト(日付順)
	 */
	private List<TimeRecordEntity> searchRecord(RecordFilter filter,
			Boolean withRecordItems, Boolean forUpdate) {

		// SOQLを作成する
		// 工数明細の項目
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_RECORD_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// 工数サマリー
		selectFldList.add(SUMMARY_R + '.' + getFieldName(TimeSummary__c.Locked__c));

		// 工数内訳の項目
		if (withRecordItems == true) {
			List<String> recordFldList = new List<String>();
			for (Schema.SObjectField fld : GET_RECORD_ITEM_FIELD_SET) {
				recordFldList.add(getFieldName(fld));
			}
			// ジョブ
			recordFldList.add(JOB_R + '.' + getFieldName(ComJob__c.Code__c));
			recordFldList.add(JOB_R + '.' + getFieldName(ComJob__c.Name_L0__c));
			recordFldList.add(JOB_R + '.' + getFieldName(ComJob__c.Name_L1__c));
			recordFldList.add(JOB_R + '.' + getFieldName(ComJob__c.Name_L2__c));
			recordFldList.add(JOB_R + '.' + getFieldName(ComJob__c.DirectCharged__c));
			// 作業分類
			recordFldList.add(WORK_CATEGORY_R + '.' + getFieldName(TimeWorkCategory__c.Code__c));
			recordFldList.add(WORK_CATEGORY_R + '.' + getFieldName(TimeWorkCategory__c.Name_L0__c));
			recordFldList.add(WORK_CATEGORY_R + '.' + getFieldName(TimeWorkCategory__c.Name_L1__c));
			recordFldList.add(WORK_CATEGORY_R + '.' + getFieldName(TimeWorkCategory__c.Name_L2__c));

			selectFldList.add(
					'(SELECT ' + String.join(recordFldList, ',')
					+ ' FROM ' + CHILD_RELATION_RECORD_ITEMS
					+ ' ORDER BY ' + getFieldName(TimeRecordItem__c.Order__c) + ' Asc)');
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> summaryIds = filter.summaryIdList;
		final Id employeeId = filter.employeeId;
		final Date startDate = AppDate.convertDate(filter.startDate);
		final Date endDate = AppDate.convertDate(filter.endDate);
		if ((summaryIds != null) && (! summaryIds.isEmpty())) {
			condList.add(getFieldName(TimeRecord__c.TimeSummaryId__c) + ' IN :summaryIds');
		}
		if (filter.employeeId != null) {
			condList.add(SUMMARY_R + '.' + EMP_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c) + ' = :employeeId');
		}
		if (filter.startDate != null) {
			condList.add(getFieldName(TimeRecord__c.Date__c) + ' >= :startDate');
		}
		if (filter.endDate != null) {
			condList.add(getFieldName(TimeRecord__c.Date__c) + ' <= :endDate');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + TimeRecord__c.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond
				+ ' ORDER BY ' + getFieldName(TimeRecord__c.Date__c) + ' Asc';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('TimeSummaryRepository.searchRecord: SOQL==>' + soql);
		List<SObject> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<TimeRecordEntity> entityList = new List<TimeRecordEntity>();
		for (SObject sObj : sObjList) {
			entityList.add(createRecordEntity((TimeRecord__c)sObj));
		}

		return entityList;
	}

	/**
	 * TimeSummary__cオブジェクトからTimeSummaryEntityを作成する
	 * @param sumObj 作成元のTimeSummary__cオブジェクト
	 * @return 作成したTimeSummaryEntity
	 */
	@testVisible
	private TimeSummaryEntity createSummaryEntity(TimeSummary__c sumObj) {
		TimeSummaryEntity entity = new TimeSummaryEntity(sumObj);

		// 工数明細
		List<TimeRecord__c> rcdObjList = sumObj.TimeTrackRecords__r;
		if (rcdObjList != null) {
			List<TimeRecordEntity> recordList = new List<TimeRecordEntity>();
			for (TimeRecord__c rcdObj : rcdObjList) {
				recordList.add(createRecordEntity(rcdObj));
			}
			entity.replaceRecordList(recordList);
		}

		return entity;
	}

	/**
	 * TimeRecord__cオブジェクトからTimeRecordEntityを作成する
	 * @param rcdObj TimeRecord__cオブジェクト
	 * @return 作成したTimeRecordEntity
	 */
	@TestVisible
	private TimeRecordEntity createRecordEntity(TimeRecord__c rcdObj) {
		TimeRecordEntity entity = new TimeRecordEntity(rcdObj);

		// 工数内訳
		List<TimeRecordItem__c> itemObjList = rcdObj.TimeRecordItems__r;
		if (itemObjList != null) {
			List<TimeRecordItemEntity> recordItemList = new List<TimeRecordItemEntity>();
			for (TimeRecordItem__c itemObj : itemObjList) {
				recordItemList.add(new TimeRecordItemEntity(itemObj));
			}
			entity.replaceRecordItemList(recordItemList);
		}

		return entity;
	}
}