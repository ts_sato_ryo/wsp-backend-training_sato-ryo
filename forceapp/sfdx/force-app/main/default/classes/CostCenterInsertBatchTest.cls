/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通 Common
 *
 * @description CostCenterInsertBatchのテストクラス CostCenterInsertBatch Test Class
 */
@isTest
private class CostCenterInsertBatchTest {
	
	/**
	 * 新規コストセンター登録のテスト Insert Batch Test
	 * エラーなしで処理が完了することを確認する Verify process completes without error
	 */
	@isTest
	static void executeBatchTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);
		List<ComCostCenterBase__c> testCostCenters = ComTestDataUtility.createCostCentersWithHistory('コストセンター', testCompany.Id, 1, 1);
		testCostCenters[0].Code__c = 'batch1';
		update testCostCenters[0];

		// コストセンターインポートデータを作成 Create CostCenterImport records
		List<ComCostCenterImport__c> costCenterImpList = new List<ComCostCenterImport__c>();
		for(Integer i=0; i<10; i++) {
			Integer cnt = i + 1;

			ComCostCenterImport__c costCenterImp = new ComCostCenterImport__c();
			costCenterImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
			costCenterImp.Code__c = 'batch' + cnt;
			costCenterImp.CompanyCode__c = testCompany.Code__c;
			costCenterImp.Name_L0__c = 'costctr' + cnt;
			costCenterImp.Name_L1__c = 'コストセンター' + cnt;
			costCenterImp.Name_L2__c = 'CostCenter' + cnt;
			costCenterImp.LinkageCode__c = '000001';
			costCenterImp.ParentBaseCode__c = '000001';
			costCenterImp.HistoryComment__c = 'バッチ登録';
			costCenterImp.Status__c = ImportStatus.WAITING.value;

			costCenterImpList.add(costCenterImp);
		}
		insert costCenterImpList;

		// インポートバッチデータを作成 Create ImportBatch
		ImportBatchService service = new ImportBatchService(ImportBatchType.COST_CENTER, null);
		Id importBatchId = service.createImportBatchData();

		Test.startTest();

		// 新規コストセンター登録バッチ起動 Execute batch for CostCenterInsertBatch 
		CostCenterInsertBatch costCenterInsert = new CostCenterInsertBatch(importBatchId);
		Database.executeBatch(costCenterInsert);

		Test.stopTest();

		List<ComCostCenterBase__c> costCenterList = [
				SELECT
					Id,
					Name,
					Code__c,
					CompanyId__r.Code__c,
					(SELECT
						Id,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						LinkageCode__c,
						ParentBaseId__r.Code__c,
						HistoryComment__c,
						ValidFrom__c,
						ValidTo__c
					 FROM Histories__r)
				FROM ComCostCenterBase__c];

		Map<String, ComCostCenterBase__c> costCenterMap = new Map<String, ComCostCenterBase__c>();
		for (ComCostCenterBase__c costCenter : costCenterList) {
			costCenterMap.put(costCenter.Code__c, costCenter);
		}

		List<ComCostCenterImport__c> AfterCostCenterImpList = [
				SELECT
					Id,
					RevisionDate__c,
					Code__c,
					CompanyCode__c,
					Name_L0__c,
					Name_L1__c,
					Name_L2__c,
					LinkageCode__c,
					ParentBaseCode__c,
					HistoryComment__c,
					Status__c,
					Error__c
				FROM ComCostCenterImport__c
				ORDER BY Code__c];

		// インポートデータがコストセンターマスタに反映されていることを確認 Verify CostCenterImport records have been inserted successfully
		// 1件目は既存データで処理対象外のため、2件目以降を確認する First record is an existing data, second record and above should be inserted
		for(Integer i=1; i<10; i++) {
			ComCostCenterImport__c costCenterImp = AfterCostCenterImpList[i];
			System.assertEquals(ImportStatus.WAITING.value, costCenterImp.Status__c);
			System.assertEquals(null, costCenterImp.Error__c);

			ComCostCenterBase__c costCenterBase = costCenterMap.get(costCenterImp.Code__c);
			System.assertEquals(costCenterImp.Name_L0__c, costCenterBase.Name);
			System.assertEquals(costCenterImp.Code__c, costCenterBase.Code__c);
			System.assertEquals(costCenterImp.CompanyCode__c, costCenterBase.CompanyId__r.Code__c);

			ComCostCenterHistory__c costCenterHistory = costCenterBase.Histories__r[0];
			System.assertEquals(costCenterImp.Name_L0__c, costCenterHistory.Name);
			System.assertEquals(costCenterImp.Name_L0__c, costCenterHistory.Name_L0__c);
			System.assertEquals(costCenterImp.Name_L1__c, costCenterHistory.Name_L1__c);
			System.assertEquals(costCenterImp.Name_L2__c, costCenterHistory.Name_L2__c);
			System.assertEquals(costCenterImp.LinkageCode__c, costCenterHistory.LinkageCode__c);
			System.assertEquals(null, costCenterHistory.ParentBaseId__r.Code__c);
			System.assertEquals(costCenterImp.HistoryComment__c, costCenterHistory.HistoryComment__c);
			System.assertEquals(costCenterImp.RevisionDate__c, costCenterHistory.ValidFrom__c);
			System.assertEquals(Date.newInstance(2101, 1, 1), costCenterHistory.ValidTo__c);
		}

		ComImportBatch__c batch = [
				SELECT
					Status__c,
					Type__c,
					Count__c,
					SuccessCount__c,
					FailureCount__c
				FROM ComImportBatch__c
				WHERE Id = :importBatchId];

		// インポートバッチデータが正しく更新されていることを確認 Verify CostCenterImport records are updated correctly
		System.assertEquals(ImportBatchStatus.PROCESSING.value, batch.Status__c);
		System.assertEquals(ImportBatchType.COST_CENTER.value, batch.Type__c);
		System.assertEquals(costCenterImpList.size(), batch.Count__c);
		System.assertEquals(0, batch.SuccessCount__c);
		System.assertEquals(0, batch.FailureCount__c);
	}

	/**
	 * 新規コストセンター登録のテスト Insert Batch Test
	 * エラーで登録できなかったインポートデータがステータスが「Error」に更新されることを確認する Verify error and data is not imported
	 */
	@isTest
	static void executeBatchErrorTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);

		// コストセンターインポートデータを作成 Create CostCenterImport records
		List<ComCostCenterImport__c> costCenterImpList = new List<ComCostCenterImport__c>();
		// 1件目 Record 1
		ComCostCenterImport__c costCenterImp1 = new ComCostCenterImport__c();
		costCenterImp1.RevisionDate__c = Date.newInstance(2018, 1, 1);
		costCenterImp1.Code__c = 'batch1';
		costCenterImp1.CompanyCode__c = testCompany.Code__c;
		costCenterImp1.Name_L0__c = 'costctr1';
		costCenterImp1.HistoryComment__c = 'バッチ登録';
		costCenterImp1.Status__c = ImportStatus.WAITING.value;
		costCenterImpList.add(costCenterImp1);
		// 2件目 コストセンター名(L0)未設定 Record 2 Cost Center Name(L0) not set
		ComCostCenterImport__c costCenterImp2 = new ComCostCenterImport__c();
		costCenterImp2.RevisionDate__c = Date.newInstance(2018, 1, 1);
		costCenterImp2.Code__c = 'batch2';
		costCenterImp2.CompanyCode__c = testCompany.Code__c;
		costCenterImp2.Name_L0__c = null;
		costCenterImp2.HistoryComment__c = 'バッチ登録';
		costCenterImp2.Status__c = ImportStatus.WAITING.value;
		costCenterImpList.add(costCenterImp2);

		insert costCenterImpList;

		// インポートバッチデータを作成 Create ImportBatch
		ImportBatchService service = new ImportBatchService(ImportBatchType.COST_CENTER, null);
		Id importBatchId = service.createImportBatchData();

		Test.startTest();

		// 新規コストセンター登録バッチ起動 Execute batch for CostCenterInsertBatch
		CostCenterInsertBatch costCenterInsert = new CostCenterInsertBatch(importBatchId);
		Database.executeBatch(costCenterInsert);

		Test.stopTest();

		List<ComCostCenterImport__c> AfterCostCenterImpList = [
				SELECT
					Id,
					RevisionDate__c,
					Code__c,
					CompanyCode__c,
					Name_L0__c,
					Name_L1__c,
					Name_L2__c,
					LinkageCode__c,
					ParentBaseCode__c,
					HistoryComment__c,
					Status__c,
					Error__c
				FROM ComCostCenterImport__c
				ORDER BY Code__c];

		// インポートデータのステータスを確認する Verify Status of CostCenterImport records
		// 1件目は登録成功 Record 1 is imported successfully
		System.assertEquals(ImportStatus.WAITING.value, AfterCostCenterImpList[0].Status__c);
		// 2件目はコストセンター名(L0)未設定のためエラー Record 2 is not imported as Cost Center Name(L0) is not set
		System.assertEquals(ImportStatus.ERROR.value, AfterCostCenterImpList[1].Status__c);
	}

	/**
	 * バリデーションのテスト Validation Test
	 */
	@isTest
	static void validateCostCenterImpEntityTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);

		CostCenterInsertBatch costCenterInsert = new CostCenterInsertBatch(null);
		costCenterInsert.companyMap = new Map<String, CompanyEntity>();
		costCenterInsert.costCenterMap = new Map<List<String>, CostCenterBaseEntity>();

		// 会社データをバッチクラスのMapに格納しておく Store Company information
		CompanyRepository companyRepo = new CompanyRepository();
		for (CompanyEntity company : companyRepo.getEntityList(new List<String>{testCompany.Code__c})) {
			costCenterInsert.companyMap.put(company.code, company);
		}

		// コストセンターインポートエンティティを作成 Create CostCenterImport entities
		List<CostCenterImportEntity> costCenterImpList = new List<CostCenterImportEntity>();
		for(Integer i=0; i<5; i++) {
			Integer cnt = i + 1;

			CostCenterImportEntity costCenterImp = new CostCenterImportEntity();
			costCenterImp.revisionDate = AppDate.newInstance(2018, 1, 1);
			costCenterImp.code = 'batch' + cnt;
			costCenterImp.companyCode = testCompany.Code__c;
			costCenterImp.name_L0 = 'costctr' + cnt;
			costCenterImp.name_L1 = 'コストセンター' + cnt;
			costCenterImp.name_L2 = 'CostCenter' + cnt;
			costCenterImp.linkageCode = '000001';
			costCenterImp.parentBaseCode = '000001';
			costCenterImp.historyComment = 'バッチ登録';

			costCenterImpList.add(costCenterImp);
		}

		Test.startTest();

		// 改訂日が未設定 Revision Date not set
		CostCenterImportEntity costCenterImp = costCenterImpList[0];
		costCenterImp.revisionDate = null;
		costCenterInsert.validateCostCenterImpEntity(costCenterImp);
		System.assertEquals(ImportStatus.ERROR, costCenterImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_RevisionDate}), costCenterImp.error);

		// コストセンターコードが未設定 Cost Center Code not set
		costCenterImp = costCenterImpList[1];
		costCenterImp.code = null;
		costCenterInsert.validateCostCenterImpEntity(costCenterImp);
		System.assertEquals(ImportStatus.ERROR, costCenterImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CostCenterCode}), costCenterImp.error);

		// 会社コードが未設定 Company Code not set
		costCenterImp = costCenterImpList[2];
		costCenterImp.companyCode = null;
		costCenterInsert.validateCostCenterImpEntity(costCenterImp);
		System.assertEquals(ImportStatus.ERROR, costCenterImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode}), costCenterImp.error);

		// 指定した会社が存在しない Company does not exist
		costCenterImp = costCenterImpList[3];
		costCenterImp.companyCode = '999';
		costCenterInsert.validateCostCenterImpEntity(costCenterImp);
		System.assertEquals(ImportStatus.ERROR, costCenterImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode}), costCenterImp.error);

		// コストセンター名(L0)が未設定 Cost Center Name(L0) not set
		costCenterImp = costCenterImpList[4];
		costCenterImp.name_L0 = null;
		costCenterInsert.validateCostCenterImpEntity(costCenterImp);
		System.assertEquals(ImportStatus.ERROR, costCenterImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CostCenterName}), costCenterImp.error);

		Test.stopTest();
	}

	/**
	 * インポートデータに有効期間終了日を更新するデータが混ざっている場合のテスト Insert Batch Test with ValidTo value
	 * エラーなしで処理が完了することを確認する Verify process completes without error
	 */
	@isTest
	static void executeBatchTestUpdateValidEndDate() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);
		List<ComCostCenterBase__c> testCostCenters = ComTestDataUtility.createCostCentersWithHistory('コストセンター', testCompany.Id, 1, 1);
		testCostCenters[0].Code__c = 'batch1';
		update testCostCenters[0];

		// コストセンターインポートデータを作成 Create CostCenterImport records
		// 最初の2件は新規登録データ First 2 records are insert records
		List<ComCostCenterImport__c> costCenterImpList = new List<ComCostCenterImport__c>();
		for(Integer i=0; i<2; i++) {
			Integer cnt = i + 1;

			ComCostCenterImport__c costCenterImp = new ComCostCenterImport__c();
			costCenterImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
			costCenterImp.Code__c = 'batch' + cnt;
			costCenterImp.CompanyCode__c = testCompany.Code__c;
			costCenterImp.Name_L0__c = 'costctr' + cnt;
			costCenterImp.Name_L1__c = 'コストセンター' + cnt;
			costCenterImp.Name_L2__c = 'CostCenter' + cnt;
			costCenterImp.LinkageCode__c = '000001';
			costCenterImp.ParentBaseCode__c = '000001';
			costCenterImp.HistoryComment__c = 'バッチ登録';
			costCenterImp.Status__c = ImportStatus.WAITING.value;

			costCenterImpList.add(costCenterImp);
		}
		// 次の2件は有効期間終了日更新データ Next 2 records are update ValidTo records
		for(Integer i=0; i<2; i++) {
			Integer cnt = i + 1;

			ComCostCenterImport__c costCenterImp = new ComCostCenterImport__c();
			costCenterImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
			costCenterImp.ValidTo__c = Date.newInstance(2018, 1, 1);
			costCenterImp.Code__c = 'batch' + cnt;
			costCenterImp.CompanyCode__c = testCompany.Code__c;
			costCenterImp.Status__c = ImportStatus.WAITING.value;

			costCenterImpList.add(costCenterImp);
		}
		insert costCenterImpList;

		// インポートバッチデータを作成 Create ImportBatch
		ImportBatchService service = new ImportBatchService(ImportBatchType.COST_CENTER, null);
		Id importBatchId = service.createImportBatchData();

		Test.startTest();

		// 新規コストセンター登録バッチ起動 Execute batch for CostCenterInsertBatch
		CostCenterInsertBatch costCenterInsert = new CostCenterInsertBatch(importBatchId);
		Database.executeBatch(costCenterInsert);

		Test.stopTest();

		List<ComCostCenterImport__c> AfterCostCenterImpList = [
				SELECT
					Id,
					RevisionDate__c,
					Code__c,
					CompanyCode__c,
					Status__c,
					Error__c
				FROM ComCostCenterImport__c
				ORDER BY Code__c];

		// インポートデータが正常に処理されたことを確認 Verify CostCenterImport records have been inserted successfully
		for (ComCostCenterImport__c costCenterImp : AfterCostCenterImpList) {
			System.assertEquals(ImportStatus.WAITING.value, costCenterImp.Status__c);
			System.assertEquals(null, costCenterImp.Error__c);
		}

		ComImportBatch__c batch = [
				SELECT
					Status__c,
					Type__c,
					Count__c,
					SuccessCount__c,
					FailureCount__c
				FROM ComImportBatch__c
				WHERE Id = :importBatchId];

		// インポートバッチデータが正しく更新されていることを確認 Verify CostCenterImport records are updated correctly
		System.assertEquals(ImportBatchStatus.PROCESSING.value, batch.Status__c);
		System.assertEquals(ImportBatchType.COST_CENTER.value, batch.Type__c);
		System.assertEquals(costCenterImpList.size(), batch.Count__c);
		System.assertEquals(0, batch.SuccessCount__c);
		System.assertEquals(0, batch.FailureCount__c);
	}
}