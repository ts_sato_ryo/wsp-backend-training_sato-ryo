/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 経費申請タイプのリソース Resource class for Expense Report Type
 */
public with sharing class ExpReportTypeResource {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** Used for Filtering ExtendedItem from ExpReportType based on usage */
	private static final String USED_IN_REQUEST = 'Request';
	/** Enum values represent Whether the item is required */
	private enum RequiredOption {
		UNUSED,
		OPTIONAL,
		REQUIRED
	}

	/**
	 * @description Request parameter for CreateApi, UpdateApi
	 */
	public class ExpReportTypeParam extends ExpParam.ExtendedItemConfigParam implements RemoteApi.RequestParam {
		/** ID */
		public String id;
		/** 有効 */
		public Boolean active;
		/** コード */
		public String code;
		/** 会社ID */
		public String companyId;
		/** Use Cost Center */
		public String costCenterUsedIn;
		/** Cost Center Required */
		public String costCenterRequiredFor;
		public String isCostCenterRequired;
		/** 説明(翻訳) */
		public String description;
		/** 説明(L0) */
		public String description_L0;
		/** 説明(L1) */
		public String description_L1;
		/** 説明(L2) */
		public String description_L2;
		/** Use Job */
		public String jobUsedIn;
		/** Job Required */
		public String jobRequiredFor;
		public String isJobRequired;
		/** Use File Attachment for Report Type*/
		public Boolean useFileAttachment;
		/** Use Vendor */
		public String vendorUsedIn;
		/** Vendor Required */
		public String vendorRequiredFor;
		public String isVendorRequired;
		/** Expense Type linked to Report Type */
		public List<String> expTypeIds;
		/** Expense Type Id and Name */
		public List<ExpTypeIdNameParam> expTypeList;
		/** 経費申請タイプ名(翻訳) */
		public String name;
		/** 経費申請タイプ名(L0) */
		public String name_L0;
		/** 経費申請タイプ名(L1) */
		public String name_L1;
		/** 経費申請タイプ名(L2) */
		public String name_L2;

		/**
		 * @description Create ExpReportTypeEntity from the request parameter value.
		 * @param paramMap Map of the request parameter
		 * @return Created entity
		 */
		public ExpReportTypeEntity createEntity(Map<String, Object> paramMap) {
			ExpReportTypeEntity entity = new ExpReportTypeEntity();

			entity.setId(this.id);
			if (paramMap.containsKey('active')) {
				entity.active = this.active;
			}
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('costCenterUsedIn')) {
				entity.costCenterUsedIn = ComCostCenterUsedIn.valueOf(this.costCenterUsedIn);
			}
			if (paramMap.containsKey('costCenterRequiredFor')) {
				entity.costCenterRequiredFor = ComCostCenterRequiredFor.valueOf(this.costCenterRequiredFor);
			}
			if (paramMap.containsKey('description_L0')) {
				entity.descriptionL0 = this.description_L0;
			}
			if (paramMap.containsKey('description_L1')) {
				entity.descriptionL1 = this.description_L1;
			}
			if (paramMap.containsKey('description_L2')) {
				entity.descriptionL2 = this.description_L2;
			}
			if (paramMap.containsKey('jobUsedIn')) {
				entity.jobUsedIn = ComJobUsedIn.valueOf(this.jobUsedIn);
			}
			if (paramMap.containsKey('jobRequiredFor')) {
				entity.jobRequiredFor = ComJobRequiredFor.valueOf(this.jobRequiredFor);
			}
			if (paramMap.containsKey('useFileAttachment')) {
				entity.useFileAttachment = this.useFileAttachment;
			}
			if (paramMap.containsKey('vendorUsedIn')) {
				entity.vendorUsedIn = ExpVendorUsedIn.valueOf(this.vendorUsedIn);
			}
			if (paramMap.containsKey('vendorRequiredFor')) {
				entity.vendorRequiredFor = ExpVendorRequiredFor.valueOf(this.vendorRequiredFor);
			}

			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				entity.setExtendedItemDateId(i, this.getExtendedItemDateId(i));
				entity.setExtendedItemDateUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemDateUsedIn(i)));
				entity.setExtendedItemDateRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemDateRequiredFor(i)));

				entity.setExtendedItemLookupId(i, this.getExtendedItemLookupId(i));
				entity.setExtendedItemLookupUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemLookupUsedIn(i)));
				entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemLookupRequiredFor(i)));

				entity.setExtendedItemPicklistId(i, this.getExtendedItemPicklistId(i));
				entity.setExtendedItemPicklistUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemPicklistUsedIn(i)));
				entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemPicklistRequiredFor(i)));

				entity.setExtendedItemTextId(i, this.getExtendedItemTextId(i));
				entity.setExtendedItemTextUsedIn(i, ComExtendedItemUsedIn.valueOf(this.getExtendedItemTextUsedIn(i)));
				entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.valueOf(this.getExtendedItemTextRequiredFor(i)));
			}

			if (paramMap.containsKey('name_L0')) {
				entity.nameL0 = this.name_L0;

				// TODO nameは仮置きでnameL0を設定
				entity.name = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}

			return entity;
		}
	}

	/*
	 * Expense Type Name and Id Response
	 */
	public class ExpTypeIdNameParam {
		public String expTypeId;
		public String expTypeName;

		public ExpTypeIdNameParam(String id, String name) {
			this.expTypeId = id;
			this.expTypeName = name;
		}
	}

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		public String name;
	}

	/**
	 * @description Response parameter
	 */
	public class SaveResponse implements RemoteApi.ResponseParam {
		/** Id of the created record */
		public String id;
	}

	/**
	 * @description Implementation class of expense report type creation API
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_REPORT_TYPE;

		/**
		 * @description Create a new expense report type
		 * @param req Request parameter(ExpReportTypeParam)
		 * @return Response parameter(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpReportTypeParam param = (ExpReportTypeParam)req.getParam(ExpReportTypeResource.ExpReportTypeParam.class);

			// validate request parmeter
			validateParam(param);

			// create entity from request parameter
			ExpReportTypeEntity entity = param.createEntity(req.getParamMap());

			Savepoint sp = Database.setSavepoint();
			Id resId;
			try {
				// save
				resId = new ExpReportTypeService().createNewExpReportType(entity, param.expTypeIds);
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			// create response parameter
			ExpReportTypeResource.SaveResponse res = new ExpReportTypeResource.SaveResponse();
			res.Id = resId;

			return res;
		}

		/**
		 * Validate request parmeter
		 * @param param Target entity
		 */
		private void validateParam(ExpReportTypeResource.ExpReportTypeParam param) {
			// Active
			ExpCommonUtil.validateNotNull('Active', param.active);

			// Code
			ExpCommonUtil.validateStringIsNotBlank('Code', param.code);

			// Company ID
			ExpCommonUtil.validateId('Company Id', param.companyId, true);

			// Expense Type Id List
			ExpCommonUtil.validateIdList('ExpType Id List', param.expTypeIds, false);
		}
	}

	/**
	 * @description Implementation class of expense report type updating API
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_REPORT_TYPE;

		/**
		 * @description Update an expense report type
		 * @param req Request parameter(ExpReportTypeParam)
		 * @return Response parameter(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpReportTypeParam param = (ExpReportTypeParam)req.getParam(ExpReportTypeResource.ExpReportTypeParam.class);

			// validate request parmeter
			validateParam(param);

			// create entity from request parameter
			ExpReportTypeEntity entity = param.createEntity(req.getParamMap());

			Savepoint sp = Database.setSavepoint();
			try {
				// save
				new ExpReportTypeService().updateExpReportType(entity, param.expTypeIds);
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			// no response parameter
			return null;
		}

		/**
		 * Validate request parmeter
		 * @param param Target entity
		 */
		private void validateParam(ExpReportTypeResource.ExpReportTypeParam param) {
			// ID
			ExpCommonUtil.validateId('ID', param.id, true);

			// Active
			ExpCommonUtil.validateNotNull('Active', param.active);

			// Code
			ExpCommonUtil.validateStringIsNotBlank('Code', param.code);

			// Company ID
			ExpCommonUtil.validateId('Company Id', param.companyId, true);

			// Name(L0)
			ExpCommonUtil.validateStringIsNotBlank('Name(L0)', param.name_L0);

			// Expense Type Ids
			ExpCommonUtil.validateIdList('ExpType Id List', param.expTypeIds, false);
		}
	}

	/**
	 * @description Request parameter for DeleteApi
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** Record id to be deleted */
		public String id;

		/**
		 * Validate request parameter
		 */
		public void validate() {
			// ID
			ExpCommonUtil.validateId('id', this.id, true);
		}
	}

	/**
	 * @description Implementation class of expense report type deleting API
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_REPORT_TYPE;

		/**
		 * @description Delete an expense report type
		 * @param req Request parameter(DeleteRequest)
		 * @return Response parameter(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpReportTypeResource.DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);

			// validate request parmeter
			param.validate();

			// delete
			(new ExpReportTypeService()).deleteExpReportType(param.id);

			// no response parameter
			return null;
		 }
	}

	/**
	 * @description Request parameter for SearchApi
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** レコードID Record ID */
		public String id;
		/** 関連する会社レコードID Related Company Record ID */
		public String companyId;
		/** コード Code */
		public String code;
		/** 有効 Active */
		public Boolean active;
		/** Specifies whether to filter 1) the Extended Items or Not 2) Report type based on Employee Group */
		public String usedIn;
		/** Target Date */
		public String targetDate;
		/** Accounting Period start date */
		public String startDate;
		/** Accounting Period end date */
		public String endDate;
		/** Include Expense Type Name */
		public Boolean withExpTypeName;
		/** Current employee base Id; when it is not passed consider report type filtering with employee group is not required */
		public Id empId;

		/**
		 * Validate request parameter
		 */
		public void validate() {
			// ID
			ExpCommonUtil.validateId('id', this.id, false);

			// 会社ID
			ExpCommonUtil.validateId('companyId', this.companyId, false);

			// employee Id
			ExpCommonUtil.validateId('empId', this.empId, false);

			//usedIn
			if (String.isNotBlank(this.usedIn)) {
				ExpCommonUtil.validateUsedIn('usedIn', this.usedIn);
			}

			ExpCommonUtil.validateDate('endDate', this.endDate, false);
			ExpCommonUtil.validateDate('startDate', this.startDate, false);
			ExpCommonUtil.validateDate('targetDate', this.targetDate, false);
		}
	}

	/**
	 * @description Response parameter for SearchApi
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** Search result */
		public ExpReportTypeParam[] records;
	}

	/**
	 * @description Implementation class of expense report type searching API
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search expense report type records
		 * @param req Request parameter(SearchRequest)
		 * @return Response parameter(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// convert request parameter from Map to SearchRequest
			SearchRequest param = (ExpReportTypeResource.SearchRequest)req.getParam(ExpReportTypeResource.SearchRequest.class);
			Map<String, Object> paramMap = req.getParamMap();

			// validate request parmeter
			param.validate();

			// create search filter condition
			ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();
			// if parameter 'id' is set, add id to condition
			if (paramMap.containsKey('id')) {
				filter.ids = new Set<Id>{param.id};
			}
			// if parameter 'companyId' is set, add company id to condition
			if (paramMap.containsKey('companyId')) {
				filter.companyIds = new Set<Id>{param.companyId};
			}
			// if parameter 'code' is set, add code to condition
			if (paramMap.containsKey('code')) {
				filter.codes = new Set<String>{param.code};
			}
			// if parameter 'active' is set, add active to condition
			if (paramMap.containsKey('active')) {
				filter.active = param.active;
			}
			AppDate targetDate = String.isNotBlank(param.targetDate) ? AppDate.valueOf(param.targetDate) : AppDate.today();
			AppDate startDate = AppDate.valueOf(param.startDate);
			AppDate endDate = AppDate.valueOf(param.endDate);

			// Perform Search
			List<ExpReportTypeService.ExpReportTypeWithExpTypeLink> expReportExpTypeList =
					new ExpReportTypeService().searchExpReportTypeWithExpType(filter, param.usedIn, targetDate, startDate, endDate, param.withExpTypeName, param.empId);
			List<ExpReportTypeEntity> expReportTypeEntityList = new List<ExpReportTypeEntity>();
			for (ExpReportTypeService.ExpReportTypeWithExpTypeLink entity : expReportExpTypeList) {
				expReportTypeEntityList.add(entity.expReportType);
			}

			ExpReportTypeService reportTypeService = new ExpReportTypeService();

			ExpService.CallType callType;
			if (String.isNotBlank(param.usedIn)) {
				callType = ExpService.CALL_TYPE_MAP.get(param.usedIn);
				// Filter out the Extended Item from ExpReportType if usedIn is specified as `Request`
				if (callType == ExpService.CallType.REQUEST) {
					reportTypeService.filterOutUsedInReportOnlyExtendedItems(expReportTypeEntityList);
				}
			}

			reportTypeService.setExtendedItemInfo(expReportTypeEntityList, callType);

			// convert search results to response format
			List<ExpReportTypeParam> records = new List<ExpReportTypeParam>();

			for (ExpReportTypeService.ExpReportTypeWithExpTypeLink entity : expReportExpTypeList) {
				ExpReportTypeParam response = createExpReportTypeParam(entity.expReportType);
				response.isCostCenterRequired = getCostCenterRequiredValue(callType, entity.expReportType.costCenterUsedIn, entity.expReportType.costCenterRequiredFor);
				response.isJobRequired = getJobRequiredValue(callType, entity.expReportType.jobUsedIn, entity.expReportType.jobRequiredFor);
				response.isVendorRequired = getVendorRequiredValue(callType, entity.expReportType.vendorUsedIn, entity.expReportType.vendorRequiredFor);
				if (param.withExpTypeName == True) {
					response.expTypeList = createExpTypeNameListResponse(entity.expTypeList);
					// To Remove the Report type that doesn't have any expense type (For mobile)
					if (response.expTypeList == null || response.expTypeList.isEmpty()) {
						continue;
					}
				} else {
					response.expTypeIds = entity.expTypeIdList;
				}

				records.add(response);
			}
			// create response parameter
			SearchResponse res = new SearchResponse();
			res.records = records;

			return res;
		}

		/*
		 * Get {@code RequiredOption} value from the parameters
		 * @param usedIn Where the report type use Cost Center in
		 * @param requiredFor Where report type is required
		 * @param callType Where the report search API is called from
		 * @return {@code RequiredOption} value
		 */
		@TestVisible
		private String getCostCenterRequiredValue(ExpService.CallType callType, ComCostCenterUsedIn usedIn, ComCostCenterRequiredFor requiredFor) {
			// Called from the admin screen.
			if (callType == null) {
				return null;
			}
			// In case the report type doesn't use Cost Center.
			if (usedIn == null) {
				return RequiredOption.UNUSED.name();
			}

			// In case the API is called from Report
			if (callType == ExpService.CallType.REPORT) {
				return (requiredFor != null) ? RequiredOption.REQUIRED.name() : RequiredOption.OPTIONAL.name();
			}

			// In case the API is called from Request
			if (callType == ExpService.CallType.REQUEST) {
				if (usedIn == ComCostCenterUsedIn.EXPENSE_REPORT) {
					return RequiredOption.UNUSED.name();
				} else {
					return (requiredFor == ComCostCenterRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT) ? RequiredOption.REQUIRED.name() : RequiredOption.OPTIONAL.name();
				}
			}
			throw new App.ParameterException('costCenterUsedIn', usedIn);
		}

		/*
		 * Get {@code RequiredOption} value from the parameters
		 * @param usedIn Where the report type use Job in
		 * @param requiredFor Where report type is required
		 * @param callType Where the report search API is called from
		 * @return {@code RequiredOption} value
		 */
		@TestVisible
		private String getJobRequiredValue(ExpService.CallType callType, ComJobUsedIn usedIn, ComJobRequiredFor requiredFor) {
			// Called from the admin screen.
			if (callType == null) {
				return null;
			}
			// In case the report type doesn't use Job.
			if (usedIn == null) {
				return RequiredOption.UNUSED.name();
			}

			// In case the API is called from Report
			if (callType == ExpService.CallType.REPORT) {
				return (requiredFor != null) ? RequiredOption.REQUIRED.name() : RequiredOption.OPTIONAL.name();
			}

			// In case the API is called from Request
			if (callType == ExpService.CallType.REQUEST) {
				if (usedIn == ComJobUsedIn.EXPENSE_REPORT) {
					return RequiredOption.UNUSED.name();
				} else {
					return (requiredFor == ComJobRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT) ? RequiredOption.REQUIRED.name() : RequiredOption.OPTIONAL.name();
				}
			}
			throw new App.ParameterException('jobUsedIn', usedIn);
		}

		/*
		 * Get {@code RequiredOption} value from the parameters
		 * @param usedIn Where the report type use vendor in
		 * @param requiredFor Where report type is required
		 * @param callType Where the report search API is called from
		 * @return {@code RequiredOption} value
		 */
		@TestVisible
		private String getVendorRequiredValue(ExpService.CallType callType, ExpVendorUsedIn usedIn, ExpVendorRequiredFor requiredFor) {
			// Called from the admin screen.
			if (callType == null) {
				return null;
			}
			// In case the report type doesn't use vendor.
			if (usedIn == null) {
				return RequiredOption.UNUSED.name();
			}

			// In case the API is called from Report
			if (callType == ExpService.CallType.REPORT) {
				return (requiredFor != null) ? RequiredOption.REQUIRED.name() : RequiredOption.OPTIONAL.name();
			}

			// In case the API is called from Request
			if (callType == ExpService.CallType.REQUEST) {
				if (usedIn == ExpVendorUsedIn.EXPENSE_REPORT) {
					return RequiredOption.UNUSED.name();
				} else {
					return (requiredFor == ExpVendorRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT) ? RequiredOption.REQUIRED.name() : RequiredOption.OPTIONAL.name();
				}
			}
			throw new App.IllegalStateException('ExpReportTypeResource.getVendorRequiredValue: unexpected parameter usedIn=' + usedIn + ',requiredFor='+requiredFor+'callType='+callType);
		}

		/**
		 * Convert ExpReportTypeEntity to ExpReportTypeParam
		 * @param entity Source entity
		 * @return Converted param data
		 */
		private ExpReportTypeParam createExpReportTypeParam(ExpReportTypeEntity entity) {
			ExpReportTypeParam param = new ExpReportTypeParam();

			param.id = entity.id;
			param.active = entity.active;
			param.code = entity.code;
			param.companyId = entity.companyId;
			param.description = entity.descriptionL.getValue();
			param.description_L0 = entity.descriptionL0;
			param.description_L1 = entity.descriptionL1;
			param.description_L2 = entity.descriptionL2;
			if (entity.costCenterRequiredFor != null) {
				param.costCenterRequiredFor = entity.costCenterRequiredFor.value;
			}
			if (entity.costCenterUsedIn != null) {
				param.costCenterUsedIn = entity.costCenterUsedIn.value;
			}
			if (entity.jobRequiredFor != null) {
				param.jobRequiredFor = entity.jobRequiredFor.value;
			}
			if (entity.jobUsedIn != null) {
				param.jobUsedIn = entity.jobUsedIn.value;
			}
			param.useFileAttachment = entity.useFileAttachment;
			if (entity.vendorRequiredFor != null) {
				param.vendorRequiredFor = entity.vendorRequiredFor.value;
			}
			if (entity.vendorUsedIn != null) {
				param.vendorUsedIn = entity.vendorUsedIn.value;
			}
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				param.setExtendedItemDateId(i, entity.getExtendedItemDateId(i));
				param.setExtendedItemDateUsedIn(i, getExtendedItemDetail(entity.getExtendedItemDateUsedIn(i)));
				param.setExtendedItemDateRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemDateRequiredFor(i)));
				param.setExtendedItemDateInfo(i, entity.getExtendedItemDateInfo(i));

				param.setExtendedItemLookupId(i, entity.getExtendedItemLookupId(i));
				param.setExtendedItemLookupUsedIn(i, getExtendedItemDetail(entity.getExtendedItemLookupUsedIn(i)));
				param.setExtendedItemLookupRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemLookupRequiredFor(i)));
				param.setExtendedItemLookupInfo(i, entity.getExtendedItemLookupInfo(i));

				param.setExtendedItemPicklistId(i, entity.getExtendedItemPicklistId(i));
				param.setExtendedItemPicklistUsedIn(i, getExtendedItemDetail(entity.getExtendedItemPicklistUsedIn(i)));
				param.setExtendedItemPicklistRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemPicklistRequiredFor(i)));
				param.setExtendedItemPicklistInfo(i, entity.getExtendedItemPicklistInfo(i));

				param.setExtendedItemTextId(i, entity.getExtendedItemTextId(i));
				param.setExtendedItemTextUsedIn(i, getExtendedItemDetail(entity.getExtendedItemTextUsedIn(i)));
				param.setExtendedItemTextRequiredFor(i, getExtendedItemDetail(entity.getExtendedItemTextRequiredFor(i)));
				param.setExtendedItemTextInfo(i, entity.getExtendedItemTextInfo(i));
			}
			param.name = entity.nameL.getValue();
			param.name_L0 = entity.nameL0;
			param.name_L1 = entity.nameL1;
			param.name_L2 = entity.nameL2;

			return param;
		}

		/*
		 * Retrieves the value of ExtendedItemUsedIn and ExtendedItemRequiredFor entity fields
		 */
		private String getExtendedItemDetail(ValueObjectType entityValue) {
			return (entityValue == null) ? null : entityValue.value;
		}

		/**
		 * Create Expense Type Id and Name response param
		 * @expTypeIdNameList List of expense type id and name list in ExpTypeIdName form
		 * @return List of ExpTypeIdNameParam
		 */
		private List<ExpTypeIdNameParam> createExpTypeNameListResponse(List<ExpReportTypeService.ExpTypeIdName> expTypeIdNameList) {
			List<ExpTypeIdNameParam> expTypeIdNameParamList = new List<ExpTypeIdNameParam>();
			if (expTypeIdNameList != null && !expTypeIdNameList.isEmpty()) {
				for(ExpReportTypeService.ExpTypeIdName expTypeIdName : expTypeIdNameList) {
					ExpTypeIdNameParam param = new ExpTypeIdNameParam(expTypeIdName.expTypeId, expTypeIdName.expTypeName);

					expTypeIdNameParamList.add(param);
				}
			}
			return expTypeIdNameParamList;
		}
	}
}

