/**
 * 勤怠インポートバッチ種別
 */
public with sharing class AttImportBatchType extends ValueObjectType {

	/** 勤務パターン適用 */
	public static final AttImportBatchType ATT_PATTERN_APPLY = new AttImportBatchType('AttPatternApply');

	/** インポートバッチ種別のリスト（種別が追加されら本リストにも追加してください） */
	public static final List<AttImportBatchType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttImportBatchType> {
			ATT_PATTERN_APPLY
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttImportBatchType(String value) {
		super(value);
	}

	/**
	 * 値からAttImportBatchTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttImportBatchType、ただし該当するImportBatchTypeが存在しない場合はnull
	 */
	public static AttImportBatchType valueOf(String value) {
		AttImportBatchType retType = null;
		if (String.isNotBlank(value)) {
			for (AttImportBatchType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ImportBatchType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttImportBatchType) {
			// 値が同じであればtrue
			if (this.value == ((AttImportBatchType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}