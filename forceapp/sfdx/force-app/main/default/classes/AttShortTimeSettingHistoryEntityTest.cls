/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttShortTimeSettingHistoryEntityのテスト
 */
@isTest
private class AttShortTimeSettingHistoryEntityTest {

	/**
	 * テスト用の履歴エンティティを作成する
	 */
	private static AttShortTimeSettingHistoryEntity createHistoryEntity() {
		AttShortTimeSettingHistoryEntity history = new AttShortTimeSettingHistoryEntity();
		history.baseId = UserInfo.getUserId();
		history.historyComment = 'テスト履歴コメント';
		history.validFrom = AppDate.newInstance(2017, 1, 11);
		history.validTo = AppDate.newInstance(2017, 2, 11);
		history.isRemoved = true;
		history.uniqKey = history.validFrom.format() + history.validTo.format();
		history.name = 'テスト履歴';
		history.nameL0 = 'テスト履歴_L0';
		history.nameL1 = 'テスト履歴_L1';
		history.nameL2 = 'テスト履歴_L2';
		history.allowableTimeOfShortWork = 300;
		history.allowableTimeOfLateArrival = 120;
		history.allowableTimeOfEarlyLeave = 180;
		history.allowableTimeOfIrregularRest = 240;

		return history;
	}

	/**
	 * エンティティを複製できることを確認する
	 */
	@isTest static void copyTest() {
		AttShortTimeSettingHistoryEntity history = createHistoryEntity();

		AttShortTimeSettingHistoryEntity copyHistory = history.copy();

		System.assertEquals(history.nameL0, copyHistory.nameL0);
		System.assertEquals(history.nameL1, copyHistory.nameL1);
		System.assertEquals(history.nameL2, copyHistory.nameL2);
		System.assertEquals(history.allowableTimeOfShortWork, copyHistory.allowableTimeOfShortWork);
		System.assertEquals(history.allowableTimeOfLateArrival, copyHistory.allowableTimeOfLateArrival);
		System.assertEquals(history.allowableTimeOfEarlyLeave, copyHistory.allowableTimeOfEarlyLeave);
		System.assertEquals(history.allowableTimeOfIrregularRest, copyHistory.allowableTimeOfIrregularRest);

		System.assertEquals(history.name, copyHistory.name);
		System.assertEquals(history.baseId, copyHistory.baseId);
		System.assertEquals(history.historyComment, copyHistory.historyComment);
		System.assertEquals(history.validFrom, copyHistory.validFrom);
		System.assertEquals(history.validTo, copyHistory.validTo);
		System.assertEquals(history.isRemoved, copyHistory.isRemoved);
		System.assertEquals(history.uniqKey, copyHistory.uniqKey);
	}
}
