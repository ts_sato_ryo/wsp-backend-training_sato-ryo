@isTest
private class AttDailyRequestEntityTest {

	/**
	 * getDetailStatusのテスト
	 * StatusとisRejectedの値に応じたステータスが取得できることを確認する
	 */
	@isTest static void saveLeaveRequestTest() {
		AttDailyRequestEntity entity = new AttDailyRequestEntity();

		// statusがPENDINGの場合、承認待ちが返却される
		entity.status = AppRequestStatus.PENDING;
		entity.cancelType = AppCancelType.NONE;
		System.assertEquals(AttDailyRequestEntity.DetailStatus.APPROVAL_IN, entity.getDetailStatus());

		// statusが APPROVED の場合、承認済みが返却される
		entity.status = AppRequestStatus.APPROVED;
		entity.cancelType = AppCancelType.NONE;
		System.assertEquals(AttDailyRequestEntity.DetailStatus.APPROVED, entity.getDetailStatus());

		// statusが DISABLED, cancelTypeが REJECTED の場合、却下が返却される
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.REJECTED;
		System.assertEquals(AttDailyRequestEntity.DetailStatus.REJECTED, entity.getDetailStatus());

		// statusが DISABLED,cancelTypeが REMOVED の場合、申請取消が返却される
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.REMOVED;
		System.assertEquals(AttDailyRequestEntity.DetailStatus.REMOVED, entity.getDetailStatus());

		// statusが DISABLED,cancelTypeが CANCELED の場合、承認取消が返却される
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.CANCELED;
		System.assertEquals(AttDailyRequestEntity.DetailStatus.CANCELED, entity.getDetailStatus());

		// statusが DISABLED,cancelTypeが 上記のいずれの値でもない場合、アプリ例外が発生する
		entity.status = AppRequestStatus.DISABLED;
		entity.cancelType = AppCancelType.NONE;
		try {
			AttDailyRequestEntity.DetailStatus detaiStatus = entity.getDetailStatus();
			System.assert(false, '例外が発生しませんでした。detaiStatus=' + detaiStatus);
		} catch (App.IllegalStateException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// statusが REAPPLYING の場合、変更承認待ちが返却される
		entity.status = AppRequestStatus.REAPPLYING;
		entity.cancelType = AppCancelType.NONE;
		System.assertEquals(AttDailyRequestEntity.DetailStatus.REAPPLYING, entity.getDetailStatus());

		// statusがnullの場合、nullが返却される
		entity.status = null;
		System.assertEquals(null, entity.getDetailStatus());

	}

	/**
	 * getRoundedUpLeaveTotalTimeのテスト
	 * 切り上げ後の値が正しいことを確認する
	 */
	@isTest
	static void getRoundedUpLeaveTotalTimeTest() {
		AttDailyRequestEntity entity = new AttDailyRequestEntity();

		// leaveTotalTimeがnullの場合はnull
		entity.leaveTotalTime = null;
		System.assertEquals(null, entity.getRoundedUpLeaveTotalTime());

		// 切り上げ処理が不要な場合
		entity.leaveTotalTime = 120;
		System.assertEquals(120, entity.getRoundedUpLeaveTotalTime());

		// 切り上げ処理が必要な場合
		entity.leaveTotalTime = 119;
		System.assertEquals(120, entity.getRoundedUpLeaveTotalTime());
		entity.leaveTotalTime = 121;
		System.assertEquals(180, entity.getRoundedUpLeaveTotalTime());
	}
}