/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Class for Cost Center History APIs
 */
public with sharing class CostCenterHistoryResource extends Repository.BaseRepository {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	public class LookupField {
		public String name;
	}

	/*
	 * Try and convert the given String to Id type.
	 * If failed, ParameterException with InvalidValue msg will be thrown with the given errorMsg.
	 */
	private static void verifyValidIdValue(String idString, Boolean isRequired, String errorMsg) {
		if (String.isNotBlank(idString)) {
			try {
				System.Id.valueOf(idString);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{errorMsg}));
			}
		} else {
			if (isRequired) {
				throw new App.ParameterException(MESSAGE.Com_Err_Specify(new List<String>{errorMsg}));
			}
		}
	}

	/*
	 * Request Param for CostCenterHistory Api.
	 * Base on the API, the corresponding fileds will be used.
	 */
	public class CostCenterHistory implements RemoteApi.RequestParam {
		public String id;
		public String baseId;
		public String linkageCode;
		public String name;
		public String name_L0;
		public String name_L1;
		public String name_L2;
		public String parentId;
		public LookupField parent;
		public String comment;
		public String validDateFrom;
		public String validDateTo;

		public CostCenterHistoryEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {
			CostCenterHistoryEntity history = new CostCenterHistoryEntity();

			if (isUpdate && String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_Specify(new List<String>{'ID'}));
			}

			if (isUpdate) {
				history.setId(this.id);
			}

			if (!isUpdate && paramMap.containsKey('baseId')) {
				history.baseId = this.baseId;
			}
			if (paramMap.containsKey('linkageCode')) {
				history.linkageCode = this.linkageCode;
			}
			if (paramMap.containsKey('name_L0')) {
				history.nameL0 = this.name_L0;
				history.name = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				history.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				history.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('parentId')) {
				history.parentBaseId = this.parentId;
			}
			if (paramMap.containsKey('comment')) {
				history.historyComment = this.comment;
			}
			if (paramMap.containsKey('validDateFrom')) {
				history.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (history.validFrom == null) {
				history.validFrom = AppDate.today();
			}
			if (paramMap.containsKey('validDateTo')) {
				history.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (history.validTo == null) {
				history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}

			return history;
		}
	}

	public virtual class SaveResult implements RemoteApi.ResponseParam {
		public String id;
	}

	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_COST_CENTER;

		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			final Boolean isUpdate = false;
			CostCenterService service = new CostCenterService();

			CostCenterHistory param = (CostCenterHistory) req.getParam(CostCenterHistoryResource.CostCenterHistory.class);
			CostCenterHistoryEntity history = param.createEntity(req.getParamMap(), isUpdate);

			service.saveCostCenterHistoryRecord(history);

			CostCenterBaseEntity updatedBase = service.getEntity(history.baseId, history.validFrom);
			SaveResult result = new SaveResult();
			result.id = updatedBase.getHistoryList().get(0).id;
			return result;
		}
	}

	/*
	 * Request Parameter for the DeleteApi
	 */
	public class DeleteParam implements RemoteApi.RequestParam {
		/*
		 * The ID of the Cost Center History Record to be deleted.
		 */
		public String id;

		public void validate() {
			verifyValidIdValue(this.id, true, 'ID');
		}
	}

	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_COST_CENTER;

		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			DeleteParam param = (DeleteParam) req.getParam(DeleteParam.class);
			param.validate();
			CostCenterService service = new CostCenterService();

			try {
				service.deleteCostCenterHistory(param.id);
			} catch (App.RecordNotFoundException e) {
				// If user is trying to delete an non existing, no error is thrown.
			}

			//Exception will be thrown in the above statements if the operation fails.
			return null;
		}
	}

	public class SearchResult implements RemoteApi.ResponseParam {
		public CostCenterHistory[] records;
	}

	public class SearchCondition implements  RemoteApi.RequestParam {
		public String id;
		public String baseId;

		/*
		 * Validate the Request Param for the SearchAPI
		 */
		public void validate() {
			verifyValidIdValue(this.id, false, 'id');
			verifyValidIdValue(this.baseId, false, 'baseId');
		}
	}

	public with sharing class SearchApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SearchCondition param = (SearchCondition) req.getParam(SearchCondition.class);
			param.validate();

			CostCenterService service = new CostCenterService();
			List<CostCenterBaseEntity> baseEntityList = service.searchCostCenterWithHistory(param.baseId, param.id);

			Set<Id> parentIdSet = new Set<Id>();
			for (CostCenterBaseEntity baseEntity : baseEntityList) {
				for (CostCenterHistoryEntity historyEntity : baseEntity.getHistoryList()) {
					if (historyEntity.parentBaseId != null) {
						parentIdSet.add(historyEntity.parentBaseId);
					}
				}
			}
			Map<Id, CostCenterBaseEntity> parentBaseMap = service.getBaseMap(new List<Id>(parentIdSet));

			List<CostCenterHistory> historyList = new List<CostCenterHistory>();
			for (CostCenterBaseEntity baseEntity : baseEntityList) {
				for (CostCenterHistoryEntity historyEntity : baseEntity.getHistoryList()) {
					CostCenterHistory cch = new CostCenterHistory();
					cch.id = historyEntity.id;
					cch.baseId = historyEntity.baseId;
					cch.name = historyEntity.nameL.getValue();
					cch.name_L0 = historyEntity.nameL.valueL0;
					cch.name_L1 = historyEntity.nameL.valueL1;
					cch.name_L2 = historyEntity.nameL.valueL2;
					cch.linkageCode = historyEntity.linkageCode;
					cch.validDateFrom = historyEntity.validFrom.format();
					cch.validDateTo = historyEntity.validTo.format();
					cch.comment = historyEntity.historyComment;

					cch.parentId = historyEntity.parentBaseId;
					cch.parent = new LookupField();
					cch.parent.name = historyEntity.parentBaseId != null ?
					                  getHistoryName(parentBaseMap.get(historyEntity.parentBaseId), historyEntity.validFrom): null;
					historyList.add(cch);
				}
			}

			SearchResult result = new SearchResult();
			result.records = historyList;
			return result;
		}
		private String getHistoryName(CostCenterBaseEntity base, AppDate targetDate) {
			CostCenterHistoryEntity history = (CostCenterHistoryEntity) base.getSuperHistoryByDate(targetDate);
			if (history == null) {
				return null;
			}
			return history.nameL.getValue();
		}
	}
}