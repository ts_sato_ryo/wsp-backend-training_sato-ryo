/**
 * @group 勤怠
 *
 * 勤怠月次申請のエンティティ
 */
public with sharing class AttRequestEntity extends AttRequestGeneratedEntity {

	/** コメントの最大文字数 */
	private static final Integer COMMENT_MAX_LENGTH = 1000;

	/**
	 * コンストラクタ
	 */
	public AttRequestEntity() {
		super(new AttRequest__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttRequestEntity(AttRequest__c sobj) {
		super(sobj);
	}

	/** 期間名(参照専用) */
	public String summaryName {
		get {
			return sObj.AttSummaryId__r.SummaryName__c;
		}
		private set;
	}
	/**
	 * (テスト用)期間名を設定する
	 * @param summaryName 期間名
	 */
	@testVisible
	private void setSummaryName(String summaryName) {
		if (sobj.AttSummaryId__r == null) {
			sobj.AttSummaryId__r = new AttSummary__c();
		}
		sobj.AttSummaryId__r.SummaryName__c = summaryName;
	}

	/**
	 * 申請コメント
	 * Comment
	 * TODO GENIE-11356でバリデーション処理をバリデータクラスへ移行したらGeneratedEntityで定義する予定
	 */
	public static final Schema.SObjectField FIELD_COMMENT = AttRequest__c.Comment__c;
	public String comment {
		get {
			return (String)getFieldValue(FIELD_COMMENT);
		}
		set {
			// 文字数チェック (GENIE-11356でバリデータクラスに移行する予定)
			if ((value != null) && (value.length() > COMMENT_MAX_LENGTH)) {
				throw new App.IllegalStateException(
						ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{
									ComMessage.msg().Att_Lbl_Comment, String.valueOf(COMMENT_MAX_LENGTH)}));
			}
			setFieldValue(FIELD_COMMENT, value);
		}
	}
	/**
	 * 承認却下コメント
	 * Process Comment
	 * TODO GENIE-11356でバリデーション処理をバリデータクラスへ移行したらGeneratedEntityで定義する予定
	 */
	public static final Schema.SObjectField FIELD_PROCESS_COMMENT = AttRequest__c.ProcessComment__c;
	public String processComment {
		get {
			return (String)getFieldValue(FIELD_PROCESS_COMMENT);
		}
		set {
			// 文字数チェック (GENIE-11356でバリデータクラスに移行する予定)
			if ((value != null) && (value.length() > COMMENT_MAX_LENGTH)) {
				throw new App.IllegalStateException(
						ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{
									ComMessage.msg().Att_Lbl_Comment, String.valueOf(COMMENT_MAX_LENGTH)}));
			}
			setFieldValue(FIELD_PROCESS_COMMENT, value);
		}
	}
	/**
	 * 取消コメント
	 * Cancel Comment
	 * TODO GENIE-11356でバリデーション処理をバリデータクラスへ移行したらGeneratedEntityで定義する予定
	 */
	public static final Schema.SObjectField FIELD_CANCEL_COMMENT = AttRequest__c.CancelComment__c;
	public String cancelComment {
		get {
			return (String)getFieldValue(FIELD_CANCEL_COMMENT);
		}
		set {
			// 文字数チェック
			if ((value != null) && (value.length() > COMMENT_MAX_LENGTH)) {
				throw new App.IllegalStateException(
						ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{
									ComMessage.msg().Att_Lbl_Comment, String.valueOf(COMMENT_MAX_LENGTH)}));
			}
			setFieldValue(FIELD_CANCEL_COMMENT, value);
		}
	}
	/** 社員ベースID(参照専用) */
	public Id employeeBaseId {
		get {
			return sobj.EmployeeHistoryId__r.BaseId__c;
		}
		private set;
	}
	/** 社員の情報(参照専用) */
	public AttLookup.Employee employee {
		get {
			if (employee == null) {
				employee = new AttLookup.Employee(
						new AppPersonName(
						sobj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c,
						sobj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
						sobj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c,
						sobj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
						sobj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c,
						sobj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c),
						sobj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
						AppDate.valueOf(sobj.EmployeeHistoryId__r.ValidFrom__c),
						AppDate.valueOf(sobj.EmployeeHistoryId__r.ValidTo__c),
						sobj.EmployeeHistoryId__r.Removed__c);
			}
			return employee;
		}
		private set;
	}
	/**
	 * (テスト用)社員の参照情報を設定する
	 * @param employeeHistory 社員情報
	 */
	@testVisible
	private void setEmployee(AttLookup.Employee employee) {
		this.employee = employee;
	}

	/** 部署の情報(参照専用) */
	public AttLookup.Department department {
		get {
			if (department == null) {
				department = new AttLookup.Department(
					new AppMultiString(
						sobj.DepartmentHistoryId__r.Name_L0__c,
						sobj.DepartmentHistoryId__r.Name_L1__c,
						sobj.DepartmentHistoryId__r.Name_L2__c),
					AppDate.valueOf(sobj.DepartmentHistoryId__r.ValidFrom__c),
					AppDate.valueOf(sobj.DepartmentHistoryId__r.ValidTo__c),
					sobj.DepartmentHistoryId__r.Removed__c);
			}
			return department;
		}
		set;
	}
	/** 申請者の情報(参照専用) */
	public AttLookup.Employee actor {
		get {
			if (actor == null) {
				actor = new AttLookup.Employee(
						new AppPersonName(
							sobj.ActorHistoryId__r.BaseId__r.FirstName_L0__c,
							sobj.ActorHistoryId__r.BaseId__r.LastName_L0__c,
							sobj.ActorHistoryId__r.BaseId__r.FirstName_L1__c,
							sobj.ActorHistoryId__r.BaseId__r.LastName_L1__c,
							sobj.ActorHistoryId__r.BaseId__r.FirstName_L2__c,
							sobj.ActorHistoryId__r.BaseId__r.LastName_L2__c),
						sobj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
						AppDate.valueOf(sobj.ActorHistoryId__r.ValidFrom__c),
						AppDate.valueOf(sobj.ActorHistoryId__r.ValidTo__c),
						sobj.ActorHistoryId__r.Removed__c);
			}
			return actor;
		}
		set;
	}
	/** ユーザにレコードに対する編集アクセス権があるかどうか(参照専用) */
	public Boolean hasEditAccess {
		get {
			return sObj.UserRecordAccess.HasEditAccess;
		}
		private set;
	}

	/**
	 * 申請ステータスの詳細
	 */
	public Enum DetailStatus {
		NOT_REQUESTED, 	// 未申請
		PENDING, 		// 承認待ち
		APPROVED, 		// 承認済み
		REJECTED,		// 却下
		REMOVED,			// 申請取消
		CANCELED			// 承認取消
	}

	/**
	 * 申請の詳細ステータスを取得する
	 */
	public AttRequestEntity.DetailStatus getDetailStatus() {
		AttRequestEntity.DetailStatus detailStatus;
		if (this.id == null) {
			// 未申請
			detailStatus = AttRequestEntity.DetailStatus.NOT_REQUESTED;
		} else {
			if (this.status == AppRequestStatus.PENDING) {
				// ステータス：申請中
				detailStatus = AttRequestEntity.DetailStatus.PENDING;
			} else if (this.status == AppRequestStatus.APPROVED) {
				// ステータス：承認済み
				detailStatus = AttRequestEntity.DetailStatus.APPROVED;
			} else if (this.status == AppRequestStatus.DISABLED) {
				// ステータス：無効
				if (this.cancelType == AppCancelType.REJECTED) {
					// 取消種別：却下
					detailStatus = AttRequestEntity.DetailStatus.REJECTED;
				} else if (this.cancelType == AppCancelType.REMOVED) {
					// 取消種別：申請取消
					detailStatus = AttRequestEntity.DetailStatus.REMOVED;
				} else if (this.cancelType == AppCancelType.CANCELED) {
					// 取消種別：承認取消
					detailStatus = AttRequestEntity.DetailStatus.CANCELED;
				} else {
					// 本来ありえないはず。直接オブジェクトを操作した場合や不具合により発生
					// メッセージ：申請のステータスが取得できませんでした。取消種別の値が正しくありません。
					throw new App.IllegalStateException(
							ComMessage.msg().Att_Err_GetStatus(new List<String>{
								ComMessage.msg().Att_Lbl_Request})
							+ ComMessage.msg().Com_Err_InvalidParameter(new List<String>{
								ComMessage.msg().Att_Lbl_CancelType, AppConverter.stringValue(this.cancelType)}));
				}
			} else {
				// ステータスが設定されていないので未申請としておく
				detailStatus = AttRequestEntity.DetailStatus.NOT_REQUESTED;
			}
		}

		return detailStatus;
	}

	/**
	 * 代理申請か判定する
	 * @return 代理申請の場合true
	 */
	public Boolean isDelegatedRequest() {
		return employeeHistoryId != actorId;
	}
}
