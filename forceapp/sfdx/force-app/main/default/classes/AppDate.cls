/**
 * 日付の値オブジェクト
 */
public with sharing class AppDate extends ValueObject implements Comparable {

	/** 曜日　*/
	public Enum DayOfWeek {
		SUN, MON, TUE, WED, THU, FRI, SAT
	}
	public static final AppDate MAX_DATE = AppDate.valueOf(Date.valueOf('1700-01-01'));
	public static final AppDate MIN_DATE = AppDate.valueOf(Date.valueOf('4000-12-31'));
	/** 曜日の判定で使用するマップ */
	private static final Map<Integer, DayOfWeek> DAY_OF_WEEK_MAP;
	static {
		DAY_OF_WEEK_MAP = new Map<Integer, DayOfWeek> {
				0 => DayOfWeek.SUN,
				1 => DayOfWeek.MON,
				2 => DayOfWeek.TUE,
				3 => DayOfWeek.WED,
				4 => DayOfWeek.THU,
				5 => DayOfWeek.FRI,
				6 => DayOfWeek.SAT
			};
	}

	/**
	 * today()の戻り値を任意の値に変更するためのフィールド
	 * テストケース以外からは設定しないこと
	 */
	@TestVisible
	private static AppDate customToday;

	/** 保持する日付 */
	private Date value;

	/**
	 * コンストラクタ（日時文字列を指定）
	 * @param dateString 日時文字列(フォーマットはISO8601「YYYY-MM-DD」)
	 */
	public AppDate(String dateString) {

		// TODO フォーマットチェック？

		if (String.isBlank(dateString)) {
			throw new App.ParameterException('dateString', dateString);
		}
		value = Date.valueOf(dateString);
	}

	/**
	 * コンストラクタ（Date型の値を指定）
	 * @param value Date型の値
	 */
	public AppDate(Date value) {
		if (value == null) {
			throw new App.ParameterException('value is null.');
		}
		this.value = value;
	}

	/**
	 * System.DateをAppDateに変換する
	 * @param value System.Dateの値
	 * @return 作成したAppDate
	 */
	public static AppDate valueOf(Date value) {
		if (value == null) {
			return null;
		}
		return new AppDate(value);
	}

		/**
		 * YYYY-MM-DD文字列をAppDateに変換する
		 * @param value System.Dateの値
		 * @return 作成したAppDate
		 */
		public static AppDate valueOf(String value) {
			if (String.isBlank(value)) {
				return null;
			}
			return new AppDate(value);
		}

		/**
		 * YYYY-MM-DD文字列をAppDateに変換する
		 * @param value System.Dateの値
		 * @return 作成したAppDate
		 */
		public static AppDate parse(String value) {
			return valueOf(value);
		}

	/**
	 * 年、月、日からインスタンスを作成する
	 * @param year 年
	 * @param month 月
	 * @param day 日
	 * @return 作成したAppDate
	 */
	public static AppDate newInstance(Integer year, Integer month, Integer day) {
		return new AppDate(Date.newInstance(year, month, day));
	}

	/**
	 * 現在時刻でインスタンスを作成する
	 */
	public static AppDate today() {
		if (Test.isRunningTest()) {
			return customToday == null ? new AppDate(Date.today()) : customToday;
		}

		// テスト実行時以外は必ず当日を返す
		return new AppDate(Date.today());
	}

	/**
	 * Date型に変換する
	 * @param appValue 変換対象の値S
	 * @return 変換したData型の値、ただしappValueがnullの場合はnull
	 */
	public static Date convertDate(AppDate appValue) {
		if (appValue == null) {
			return null;
		}
		return appValue.value;
	}

	/**
	 * 日付を比較して、より過去の日付を返す
	 * @param date1 比較対象の日付
	 * @param date2 比較対象の日付
	 * @return date1とdate2のうち、過去の日付
	 *         値が等しい場合は、date1を返す
	 */
	public static AppDate min(AppDate date1, AppDate date2) {
		if (date1.equals(date2)) {
			return date1;
		}
		return date1.isBefore(date2) ? date1 : date2;
	}

	/**
	 * 日付を比較して、より未来の日付を返す
	 * @param date1 比較対象の日付
	 * @param date2 比較対象の日付
	 * @return date1とdate2のうち、より未来の日付
	 *         値が等しい場合は、date1を返す
	 */
	public static AppDate max(AppDate date1, AppDate date2) {
		if (date1.equals(date2)) {
			return date1;
		}
		return date1.isAfter(date2) ? date1 : date2;
	}

	/**
	 * Data型の値を返却する
	 * @return Date型の値
	 */
	public Date getDate() {
		return value;
	}

	/**
	 * 日を返却する
	 * @return 日
	 */
	public Integer day() {
		return value.day();
	}

	/**
	 * 月を返却する
	 * @return 月
	 */
	public Integer month() {
		return value.month();
	}

	/**
	 * 年を返却する
	 * @return 年
	 */
	public Integer year() {
		return value.year();
	}

	/**
	 * 曜日を返却する(整数値)
	 *   0: 日曜日
	 *   1: 月曜日
	 *   2: 火曜日
	 *   3: 水曜日
	 *   4: 木曜日
	 *   5: 金曜日
	 *   6: 土曜日
	 * @return 曜日
	 */
	public Integer dayOfWeekByInt() {
		return DateUtil.dayOfWeek(value);
	}

	/**
	 * 曜日を返却する(AppDate.DayOfWeekで返却)
	 * @return 曜日
	 */
	public DayOfWeek dayOfWeek() {
		return DAY_OF_WEEK_MAP.get(dayOfWeekByInt());
	}

	/**
	 * YYYY-MM-DDのフォーマットで返却する。
	 * @return 日付文字列
	 */
	public String format() {
		return format(FormatType.YYYY_MM_DD);
	}

	/**
	 * yyyyMMddのフォーマットで返却する。(2017-07-01 -> 20170701)
	 * @return 日付文字列
	 */
	public String formatYYYYMMDD() {
		return format(FormatType.YYYYMMDD);
	}

	/** 書式タイプ(FORMAT_MAPにも追加すること) */
	public enum FormatType {
		YYYY_MM_DD, // 2017-07-03
		YYYY_MM_DD_SLASH, // 2017/07/03
		YYYY_M_D_SLASH, // 2018/7/3
		YYYY_MM_SLASH, // 2017/07
		YYYYMMDD, // 20170703
		YYYYMM, // 201707
		MMMYYYY, // Jul 2017
		MMMMYYYY, // Octorber 2017
		MM // 07
	}

	/** 書式マップ */
	private static final Map<FormatType, String> FORMAT_MAP;
	static {
		FORMAT_MAP = new Map<FormatType, String> {
				FormatType.YYYY_MM_DD => 'yyyy-MM-dd',
				FormatType.YYYY_MM_DD_SLASH => 'yyyy/MM/dd',
				FormatType.YYYY_M_D_SLASH => 'yyyy/M/d',
				FormatType.YYYY_MM_SLASH => 'yyyy/MM',
				FormatType.YYYYMMDD => 'yyyyMMdd',
				FormatType.YYYYMM => 'yyyyMM',
				FormatType.MMMYYYY => 'MMM yyyy',
				FormatType.MMMMYYYY => 'MMMM yyyy',
				FormatType.MM => 'MM'
		};
	}

	/**
	 * 指定されたフォーマットの文字列で返却する
	 * @return 日付文字列
	 */
	public String format(FormatType format) {
		return Datetime.newInstance(value.year(), value.month(), value.day()).
				format(FORMAT_MAP.get(format));
	}

	/**
	 * 指定した追加月数をdateに加算する
	 * @param addMonths 追加月数
	 * @return 追加後のAppDate(新規にインスタンスを作成します)
	 */
	public AppDate addMonths(Integer addMonths) {
		return new AppDate(value.addMonths(addMonths));
	}

	/**
	 * 指定した追加日数をdateに加算する
	 * @param addDays 追加日数
	 * @return 追加後のAppDate(新規にインスタンスを作成します)
	 */
	public AppDate addDays(Integer addDays) {
		return new AppDate(value.addDays(addDays));
	}

	/**
	 * 指定した日数をdateに減算する
	 * @param minusDays 減算日数
	 * @return 追加後のAppDate(新規にインスタンスを作成します)
	 */
	public AppDate minusDays(Integer minusDays) {
		return new AppDate(value.addDays(-minusDays));
	}

	/**
	 * メソッドをコールした日付と指定された日付の間の日数を返却する
	 * @param 日付
	 * @return secondDate との間の日数
	 */
	public Integer daysBetween(AppDate secondDate) {
		return this.value.daysBetween(AppDate.convertDate(secondDate));
	}

	/**
	 * オブジェクトの値が等しいかどうか判定する
 	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {
		if (compare instanceof AppDate) {
			return this.value == ((AppDate)compare).value;
		}
		return false;
	}

	/**
	 * オブジェクトの値が等しいかどうか判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public Integer hashCode() {
		if (value == null) {
			return null;
		}
		return Integer.valueOf(formatYYYYMMDD());
	}

	/**
	 * 比較の結果であるinteger値を返します
	 * @param 比較対象
	 * @return このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満
	 */
	public Integer compareTo(Object compareTo) {
		AppDate compareDate = (AppDate)compareTo;
		if (this.value == compareDate.value) {
			return App.COMPARE_EQUAL;
		} else if (this.value > compareDate.value) {
			return App.COMPARE_AFTER;
		}
		// this.value < compareDate.value
		return App.COMPARE_BEFORE;
	}

	/**
	 * 対象時刻より前かどうかをチェックする
	 * @param 比較対象
	 * @return チェック結果
	 */
	public Boolean isBefore(AppDate targetDate) {
		return this.compareTo(targetDate) < 0;
	}

	/**
	 * 対象時刻以前かどうかをチェックする
	 * @param 比較対象
	 * @return チェック結果
	 */
	public Boolean isBeforeEquals(AppDate targetDate) {
		return this.compareTo(targetDate) <= 0;
	}

	/**
	 * 対象時刻より後かどうかをチェックする
	 * @param 比較対象
	 * @return チェック結果
	 */
	public Boolean isAfter(AppDate targetDate) {
		return this.compareTo(targetDate) > 0;
	}
	/**
	 * 対象時刻以降かどうかをチェックする
	 * @param 比較対象
	 * @return チェック結果
	 */
	public Boolean isAfterEquals(AppDate targetDate) {
		return this.compareTo(targetDate) >= 0;
	}
	/** toStringのオーバーライド */
	override public String toString() {
		return this.format();
	}
}