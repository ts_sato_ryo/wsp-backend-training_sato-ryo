/*
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Test for ExpCurrencyPair class
 */
@isTest
private class ExpCurrencyPairTest {

	/**
	 * Test for valueOf method
	 */
	@isTest
	static void valueOfTest() {

		// Get USD/JPY pair
		ExpCurrencyPair usd_jpy = ExpCurrencyPair.valueOf('USD_JPY');
		// Confirm expected value has been got
		System.assertEquals('USD_JPY', usd_jpy.value);
		System.assertEquals('USD/JPY', usd_jpy.label);

		// Get not existing currency
		ExpCurrencyPair nullPair = ExpCurrencyPair.valueOf('TEST');
		// Confirm null returned
		System.assertEquals(null, nullPair);
	}

	/**
   * Test for valueOf method
   */
	@isTest
	static void valueOfPairTest() {

		// Get USD/JPY pair
		ExpCurrencyPair usd_jpy1 = ExpCurrencyPair.valueOf('USD', 'JPY');
		// Confirm expected value has been got
		System.assertEquals('USD_JPY', usd_jpy1.value);
		System.assertEquals('USD/JPY', usd_jpy1.label);

		// Get USD/JPY pair (reverse)
		ExpCurrencyPair usd_jpy2 = ExpCurrencyPair.valueOf('JPY', 'USD');
		// Confirm expected value has been got
		System.assertEquals('USD_JPY', usd_jpy2.value);
		System.assertEquals('USD/JPY', usd_jpy2.label);

		// Get not existing currency
		ExpCurrencyPair nullPair = ExpCurrencyPair.valueOf('TEST', 'TEST');
		// Confirm null returned
		System.assertEquals(null, nullPair);
	}

	/**
	 * Test for getTypeListTst method
	 */
	@isTest
	static void getTypeListTst() {
		List<ExpCurrencyPair> typeList = ExpCurrencyPair.getTypeList();
		System.assert( typeList.size() > 0);
	}

	/**
	 * Test for equals method
	 */
	@isTest
	static void equalsTest() {

		ExpCurrencyPair pair1 = ExpCurrencyPair.valueOf('USD_JPY');
		ExpCurrencyPair pair2 = ExpCurrencyPair.valueOf('USD_JPY');

		String COMPARE = 'compare';

		System.assert(pair1.equals(pair2));
		System.assert(!pair1.equals(COMPARE));
		System.assert(!pair1.equals(null));
	}
}
