/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Service class for managing Cost Center.
 */
public with sharing class CostCenterService extends ParentChildService {

	private CostCenterRepository costCenterRepo = new CostCenterRepository();

	public CostCenterService() {
		super(new CostCenterRepository());
	}

	/*
	 * Create a new CostCenterHistory for an existing Cost Center Base.
	 */
	public void saveCostCenterHistoryRecord(CostCenterHistoryEntity historyEntity) {
		// Checks if specified BaseId points to an existing Cost Center Base record
		CostCenterBaseEntity baseEntity = getEntityById(historyEntity.baseId);
		if (baseEntity == null) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_CostCenter}));
		}

		// Linkage Code was changed not to be unique and there will be no checking of linkageCode
		/*if (historyEntity.linkageCode != null && isLinkageCodeDuplicated(historyEntity)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCostCenterCode);
		}*/

		//Check if existing history is "newer" than the specified history (meaning if there is already another history after)
		CostCenterHistoryEntity latestExistingHistory = (CostCenterHistoryEntity) baseEntity.getLatestSuperHistory();
		if (historyEntity.validFrom.getDate() < latestExistingHistory.validFrom.getDate()) {
			throw new App.ParameterException(ComMessage.msg().Admin_Err_InvalidRevisionDate);
		}

		// Check if there is a gap between new History ValidFrom date and existing latest HistoryValidTo date
		// Throws Exception if there is a gap. History cannot be created with gap, and auto extending is not supported.
		if (latestExistingHistory.validTo.daysBetween(historyEntity.validFrom) > 0) {
			throw new App.ParameterException(ComMessage.msg().Admin_Err_InvalidRevisionGap);
		}

		if (historyEntity.validTo.getDate() != latestExistingHistory.validTo.getDate()) {
			updateValidTo(historyEntity.baseId, historyEntity.validTo);
		}

		reviseHistory(historyEntity);
	}

	public List<CostCenterBaseEntity> searchCostCenterBases(Id baseId, Id companyId, Id parentId, AppDate targetDate, String query) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
		filter.baseIds = constructIdListFromString(baseId);
		filter.companyIds = constructIdListFromString(companyId);
		filter.parentIds = constructIdListFromString(parentId);
		filter.targetDate = targetDate;
		filter.query = query;
		return this.costCenterRepo.searchEntity(filter);
	}

	/*
	 * Retrieved CostCenterBaseEntity List for the target values.
	 */
	public List<CostCenterBaseEntity> searchCostCenterWithHistory(Id baseId, Id historyId) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();

		filter.historyIds = constructIdListFromString(historyId);
		filter.baseIds = constructIdListFromString(baseId);
		return this.costCenterRepo.searchEntityWithHistory(filter, false, CostCenterRepository.HISTORY_SORT_ORDER.VALID_DESC);
	}

	/*
	 * Search Cost Center History for the given Base Id and Target Date.
	 * Return null if not found or not valid.
	 */
	public CostCenterHistoryEntity searchCostCenterHistory(Id baseId, AppDate targetDate) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();

		filter.baseIds = constructIdListFromString(baseId);
		filter.targetDate = targetDate;
		List<CostCenterHistoryEntity> histories = this.costCenterRepo.searchHistoryEntity(filter, false, CostCenterRepository.HISTORY_SORT_ORDER.VALID_DESC);
		if (histories == null || histories.size() == 0) {
			return null;
		}
		return histories[0];
	}

	/**
	 * Search Cost Center History for the given History Id List and Target Date.
	 * @param historyIdList
	 * @param targetDate
	 * @return CostCenterHistoryEntity list empty list if returned is not found
	 */
	@TestVisible
	private List<CostCenterHistoryEntity> searchCCHistoryListWithHistoryIdList(List<Id> historyIdList, AppDate targetDate) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();

		filter.historyIds = historyIdList;
		filter.targetDate = targetDate;
		List<CostCenterHistoryEntity> historyEntityList = this.costCenterRepo.searchHistoryEntity(filter, false, CostCenterRepository.HISTORY_SORT_ORDER.VALID_ASC);
		return historyEntityList;
	}
	/**
	 * Search Cost Center Base for the given Base Id List and Target Date.
	 * @param baseIdList
	 * @param targetDate
	 * @return CostCenterBaseEntity list empty list if returned is not found
	 */
	@TestVisible
	private List<CostCenterBaseEntity> searchCCBaseListWithBaseIdList(List<Id> baseIdList, AppDate targetDate) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();

		filter.baseIds = baseIdList;
		filter.targetDate = targetDate;
		List<CostCenterBaseEntity> baseEntityList = this.costCenterRepo.searchEntityWithHistory(filter, false, CostCenterRepository.HISTORY_SORT_ORDER.VALID_ASC);
		return baseEntityList;
	}

	/*
	 * Delete the Cost Center History if there is no cost center using them
	 */
	public void deleteCostCenterHistory(Id deleteTargetHistoryId) {
		CostCenterBaseEntity base = costCenterRepo.getEntityByHistoryId(deleteTargetHistoryId);

		if (base == null) {
			throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_DeleteRecordNotFound);
		}
		List<CostCenterHistoryEntity> historyList = costCenterRepo.getHistoryEntityByBaseId(base.id, CostCenterRepository.HISTORY_SORT_ORDER.VALID_DESC);
		//Check to make sure it's not the only one (it's possible that a record is a future record, but it's the only one).
		if (historyList.size() < 2) {
			throw new App.IllegalStateException(ComMessage.msg().Com_Err_CannotDeletedNotExistHistory);
		}

		//The Query was sorted in descending order. So index 0 is the latest.
		CostCenterHistoryEntity latestHistory = historyList[0];

		// In the front UI, the Delete button is disabled if it's not the latest history
		// But just to be sure, check and throw Exception.
		if (latestHistory.id != deleteTargetHistoryId) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_CannotDeletedInBetweenHistory);
		}

		// If it's Same day OR Before, then not allows to delete.
		// positiveNumber: later; 0: Same Date; negative_number: Earlier
		if (AppDate.today().daysBetween(latestHistory.validFrom) < 1) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_CannotDeletedCurrentAndPastHistory);
		}

		latestHistory.setLogicallyDelete();
		costCenterRepo.saveHistoryEntity(latestHistory);

		//Since only the future history are allowed to be deleted, it's not necessary to update the `currentHistoryId`
	}

	/*
	 * Return a List containing the ID. If the ID string is blank or invalid, return null
	 */
	private List<Id> constructIdListFromString(String idStr) {
		if (String.isBlank(idStr) || !(idStr instanceof Id)) {
			return null;
		}
		return new List<Id>{idStr};
	}

	/*
	 * Given a List of Base IDs, and the target date, check if there is any cost center
	 * that specify the Base ID as the parent. The result is returned as a MAP with the
	 * BaseID as the key and whether the base has any suh-cost center as the value.
	 */
	public Map<Id, Boolean> hasChildren(List<Id> baseIds, AppDate targetDate) {
		final Boolean hasNotChildren = false;
		Map<Id, Boolean> hasChildrenMap = new Map<Id, Boolean>();
		for (Id baseId : baseIds) {
			hasChildrenMap.put(baseId, hasNotChildren);
		}

		final Boolean hasChildren = true;
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
		filter.parentIds = baseIds;
		filter.targetDate = targetDate;
		List<CostCenterBaseEntity> baseList = costCenterRepo.searchEntityWithHistory(
			filter, false, CostCenterRepository.HISTORY_SORT_ORDER.VALID_ASC);
		for (CostCenterBaseEntity base : baseList) {
			for (CostCenterHistoryEntity history : base.getHistoryList()) {
				hasChildrenMap.put(history.parentBaseId, hasChildren);
			}
		}
		return hasChildrenMap;
	}

	/*
	 * Get the corresponding history record for the given base IDs and target date.
	 * The data is retured as a map with the BaseId as the Key
	 * and the corresponding History Record (at the target date) as the value.
	 */
	public Map<Id, CostCenterHistoryEntity> getHistoryMap(List<Id> baseIds, AppDate targetDate) {
		List<CostCenterBaseEntity> bases = costCenterRepo.getEntityList(baseIds, targetDate);
		Map<Id, CostCenterHistoryEntity> historyMap = new Map<Id, CostCenterHistoryEntity>();
		for (CostCenterBaseEntity base : bases) {
			List<CostCenterHistoryEntity> histories = base.getHistoryList();
			if ((histories != null) && (!histories.isEmpty())) {
				historyMap.put(base.id, histories[0]);
			}
		}
		return historyMap;
	}

	/*
	 * Given a list of Base Record IDs, return a Map with the
	 * Base ID as the key and the BaseEntity as the value.
	 */
	public Map<Id, CostCenterBaseEntity> getBaseMap(List<Id> baseIds) {
		final AppDate targetAll = null;
		List<CostCenterBaseEntity> bases = costCenterRepo.getEntityList(baseIds, targetAll);

		Map<Id, CostCenterBaseEntity> baseMap = new Map<Id, CostCenterBaseEntity>();
		for (CostCenterBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}
		return baseMap;
	}

	protected override ParentChildBaseEntity getEntity(Id baseId) {
		return costCenterRepo.getEntity(baseId);
	}

	/*
	 * Retrieve the CostCenterBase entity based on the provided BaseId and targetDate.
	 * If targetDate is not null, then it will use the targetDate to filter the ValidFrom and ValidTo.
	 *  */
	public CostCenterBaseEntity getEntity(Id baseId, AppDate targetDate) {
		return costCenterRepo.getEntity(baseId, targetDate);
	}

	/*
	 * This method is the same as <code>getEntity</code> method.
	 * But this has the Public access (because the other method is defined in Parent and only have proctected access).
	 */
	public CostCenterBaseEntity getEntityById(Id baseId) {
		return costCenterRepo.getEntity(baseId);
	}

	protected override List<ParentChildBaseEntity> getEntityList(List<Id> baseIds) {
		final AppDate targetAllDate = null;
		return costCenterRepo.getEntityList(baseIds, targetAllDate);
	}

	protected override ParentChildBaseEntity getEntityByCode(String code) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
		filter.codes = new List<String>{code};
		List<CostCenterBaseEntity> entityList = costCenterRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	public List<ParentChildBaseEntity> getEntityListByCode(List<String> codes) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
		filter.codes = codes;
		return costCenterRepo.searchEntity(filter);
	}

	protected override ParentChildHistoryEntity getHistoryEntity(Id historyId) {
		return costCenterRepo.getHistoryEntity(historyId);
	}

	protected override List<ParentChildHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		return costCenterRepo.getHistoryEntityList(historyIds);
	}

	protected override Validator.Result validateSaveBaseEntity(ParentChildBaseEntity base) {
		return (new ComConfigValidator.CostCenterBaseValidator((CostCenterBaseEntity) base)).validate();
	}

	protected override Validator.Result validateSaveHistoryEntity(ParentChildBaseEntity base, ParentChildHistoryEntity history) {
		return (new ComConfigValidator.CostCenterHistoryValidator((CostCenterBaseEntity) base, (CostCenterHistoryEntity) history)).validate();
	}

	protected override String getNameL0FromHistory(ParentChildHistoryEntity history) {
		return ((CostCenterHistoryEntity) history).nameL0;
	}

	@TestVisible
	protected override Boolean isCodeDuplicated(ParentChildBaseEntity base) {
		CostCenterBaseEntity costCenterBase = (CostCenterBaseEntity) base;
		CostCenterBaseEntity duplicateBase = getEntityByCode(costCenterBase.code, costCenterBase.companyId);

		if (duplicateBase == null) {
			return false;
		}
		if (duplicateBase.id == costCenterBase.id) {
			return false;
		}
		return true;
	}

	/*
	 * Check if the given Linkage Code is already in used in the system.
	 */
	/*public Boolean isLinkageCodeDuplicated(CostCenterHistoryEntity historyEntity) {
	    // Linkage Code was changed not to be unique and there will be no checking of linkageCode
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
		filter.linkageCodes = new List<String>{historyEntity.linkageCode};
		List<CostCenterBaseEntity> entityList = costCenterRepo.searchEntity(filter);
		return !entityList.isEmpty();
	}*/

	/*
	 * Retrieve CostCenterBase using Code and Company ID.
	 * */
	private CostCenterBaseEntity getEntityByCode(String code, Id companyId) {
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
		filter.codes = new List<String>{code};
		filter.companyIds = new List<Id>{companyId};
		List<CostCenterBaseEntity> entityList = costCenterRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/*
	 *  Update/Regenerate the UniqKey accordingly to the changed fields.
	 */
	public void updateUniqKey(CostCenterBaseEntity base) {
		Id companyId = base.companyId;
		String code = base.code;

		if ((base.id != null) && ((companyId == null) || (String.isBlank(code)))) {
			CostCenterBaseEntity orgBase = costCenterRepo.getBaseEntity(base.id);
			if (orgBase == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			if (companyId == null) {
				companyId = orgBase.companyId;
			}
			if (String.isBlank(code)) {
				code = orgBase.code;
			}
		}

		if (companyId == null ) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Company}));
		}
		if (String.isBlank(code)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}));
		}
		CompanyEntity company = getCompany(companyId);
		base.uniqKey = base.createUniqKey(company.code, code);
	}

	/**
	 * Get Parents Cost Center Name in hierarchy order
	 * @param baseEntityList List of baseEntity that need to acquire Parent Hierarchy name
	 * @param targetDate
	 * @return Map where hierarchy name list linked with base Id
	 *
	 * Note: Hierarchy name list is ordered in ascending order (i.e. parent, grandparent, greatgrandparent)
	 */
	public Map <Id, List<String>> getParentHierarchy(List<CostCenterBaseEntity> baseEntityList, AppDate targetDate) {
		Map <Id, List<String>> returnHierarchyMap = new Map<Id, List<String>>();
		CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
		filter.targetDate = targetDate;
		// Get all cost center history entity avaliable in targetDate no future cost center is retrieved and sort order is null because order is not important
		List<CostCenterHistoryEntity> currAllHistoryEntityList = this.costCenterRepo.searchHistoryEntity(filter, false, null);

		if (!currAllHistoryEntityList.isEmpty()) {
			// Create Map with BaseId as key and CostCenterHistoryEntity as value
			Map<Id, CostCenterHistoryEntity> historyEntityMap = new Map<Id, CostCenterHistoryEntity>();
			for (CostCenterHistoryEntity historyEntity : currAllHistoryEntityList) {
				historyEntityMap.put(historyEntity.baseId, historyEntity);
			}
			// For heap size control clear out the processed list
			currAllHistoryEntityList = null;

			Integer iBaseEntity = 0;

			// Currently this loop will do the removal of baseEntityList
			while (iBaseEntity < baseEntityList.size()) {
				Boolean toDelete = false;
				// Current CostCenterBaseEntity
				CostCenterBaseEntity entity = baseEntityList[iBaseEntity];
				Id currParentBaseId = entity.getHistory(0).parentBaseId;
				// Search While it still has valid parent ID
				while (currParentBaseId != null) {
					// If parent Id is not null get the history Entity from the historyEntityMap
					if (historyEntityMap.containsKey(currParentBaseId)) {
						CostCenterHistoryEntity currHistoryEntity = historyEntityMap.get(currParentBaseId);
						// Add parent name list
						if (!returnHierarchyMap.containsKey(entity.id)) {
							returnHierarchyMap.put(entity.id, new List<String>{currHistoryEntity.nameL.getValue()});
						} else {
							returnHierarchyMap.get(entity.id).add(currHistoryEntity.nameL.getValue());
						}
						currParentBaseId = currHistoryEntity.parentBaseId;
					} else { // mean invalid parent therefore, remove
						toDelete = true;
						currParentBaseId = null;
					}
				}
				// If toDelete flag is true remove baseEntity from baseEntityList and remove returnHierarchyMap for the cost center
				if (toDelete) {
					returnHierarchyMap.remove(baseEntityList[iBaseEntity].id);
					baseEntityList.remove(iBaseEntity);
					continue;
				}
				iBaseEntity++;
			}
		}

		return returnHierarchyMap;
	}

	/**
	 * Get Recently Used CostCenter Base List
	 * @param empBaseId Employee Base Id
	 * @param baseEntityList Empty List act as return value
	 * @return CostCenterBaseEntity Lust
	 */
	public List<CostCenterBaseEntity> getRecentlyUsedCostCenterBaseList(String empBaseId, AppDate targetDate) {
		List<CostCenterBaseEntity> baseEntityList = new List<CostCenterBaseEntity>();
		ExpRecentlyUsedService expRecentlyUsedService = new ExpRecentlyUsedService();
		ExpRecentlyUsedEntity expRecUsedEntity = expRecentlyUsedService.getExistingExpRecentlyUsedEntity(empBaseId, ExpRecentlyUsedEntity.MasterType.CostCenter, null);

		if (expRecUsedEntity != null) {
			List<Id> historyIdList = expRecUsedEntity.getRecentlyUsedValues();
			if (!historyIdList.isEmpty()) {
				List<CostCenterHistoryEntity> costCenterHistoryEntityList = searchCCHistoryListWithHistoryIdList(historyIdList, targetDate);

				if (!costCenterHistoryEntityList.isEmpty()) {
					Map<Id, CostCenterHistoryEntity> baseHistoryMap = new Map<Id, CostCenterHistoryEntity>();
					// Create the parent history map
					for (CostCenterHistoryEntity ccHistory : costCenterHistoryEntityList) {
						baseHistoryMap.put(ccHistory.baseId, ccHistory);
					}
					// History Id with CostCenterBaseEntity Map
					Map<Id, CostCenterBaseEntity> historyBaseMap = new Map<Id, CostCenterBaseEntity>();

					List<CostCenterBaseEntity> costCenterBaseEntityList = searchCCBaseListWithBaseIdList(new List<Id>(baseHistoryMap.keySet()), targetDate);

					for (CostCenterBaseEntity baseEntity: costCenterBaseEntityList) {
						historyBaseMap.put(baseHistoryMap.get(baseEntity.id).id, baseEntity);
					}

					for (Id historyId : historyIdList) { // Add base entity list according to the order of historyIdList
						if (historyBaseMap.containsKey(historyId)) { // In order not to hold null Object in the list
							baseEntityList.add(historyBaseMap.get(historyId));
						}

					}

				}
			}
		}

		return baseEntityList;
	}
}