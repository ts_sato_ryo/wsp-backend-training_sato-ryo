/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description JobAssignServiceのテストクラス
*/
@isTest
private class JobAssignServiceTest {

	private static JobAssignService service = new JobAssignService();

	/**
	 * テストで使用するマスタデータを登録する
	 */
	@testSetup
	static void setup() {
		ComCompany__c company = ComTestDataUtility.createCompany('テスト会社', null);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		User user = ComTestDataUtility.createUser('user', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('テスト社員', company.Id, null, user.id, permission.Id);
		ComJobType__c jobType = ComTestDataUtility.createJobType('テストジョブタイプ', company.Id);
		ComJob__c job = ComTestDataUtility.createJob('Job', company.id, jobType.Id);
	}
	/** テスト用のユーザ */
	private static User getUser() {
		return [SELECT Id From User ORDER BY CreatedDate DESC LIMIT 1];
	}
	/** 社員 */
	private static ComEmpBase__c getEmployee() {
		return [SELECT Id, CompanyId__c, UserId__c FROM ComEmpBase__c LIMIT 1];
	}
	/** ジョブ */
	private static ComJob__c getJob() {
		return [SELECT Id, ValidFrom__c, ValidTo__c, CompanyId__c, JobTypeId__c FROM ComJob__c LIMIT 1];
	}
	/** アクセス権限 */
	private static ComPermission__c getPermission() {
		return [SELECT Id From ComPermission__c LIMIT1];
	}
	/** ジョブ割当 */
	private static List<ComJobAssign__c> getJobAssigns() {
		return [SELECT Id, JobId__c, EmployeeBaseId__c, ValidFrom__c, ValidTo__c FROM ComJobAssign__c];
	}

	/**
	 * ジョブ割当レコード1件を正常に登録出来ることを検証する
	 */
	@isTest
	static void createJobAssignTestSingleRecord() {
		List<JobAssignEntity> entities;
		System.runAs(getUser()) {
			AppDate validFrom = AppDate.newInstance(2018, 4, 1);
			AppDate validTo = AppDate.newInstance(2019, 4, 1);
			entities = service.createJobAssign(getJob().Id, validFrom, validTo, new List<Id>{getEmployee().Id});
		}

		// 検証
		List<ComJobAssign__c> sobjs = getJobAssigns();
		System.assertEquals(1, entities.size());
		System.assertEquals(1, sobjs.size());
		System.assertEquals(sobjs[0].Id, entities[0].Id);
		System.assertEquals(getJob().Id, sobjs[0].JobId__c);
		System.assertEquals(getEmployee().Id, sobjs[0].EmployeeBaseId__c);
		System.assertEquals(Date.newInstance(2018, 4, 1), sobjs[0].ValidFrom__c);
		System.assertEquals(Date.newInstance(2019, 4, 1), sobjs[0].ValidTo__c);
	}

	/**
	 * ジョブ割当レコード複数件を正常に登録出来ることを検証する
	 */
	@isTest
	static void createJobAssignTestMultiRecord() {
		// 社員登録
		ComEmpBase__c emp1 = getEmployee();
		ComEmpBase__c emp2 = ComTestDataUtility.createEmployeeWithHistory('テスト社員2', emp1.CompanyId__c, null, emp1.UserId__c, getPermission().Id);
		ComEmpBase__c emp3 = ComTestDataUtility.createEmployeeWithHistory('テスト社員3', emp1.CompanyId__c, null, emp1.UserId__c, getPermission().Id);

		List<JobAssignEntity> entities;
		System.runAs(getUser()) {
			AppDate validFrom = AppDate.newInstance(2018, 4, 1);
			AppDate validTo = AppDate.newInstance(2019, 4, 1);
			List<Id> employeeIds = new List<Id>{emp1.Id, emp2.Id, emp3.Id};
			entities = service.createJobAssign(getJob().Id, validFrom, validTo, employeeIds);
		}

		// 検証
		List<ComJobAssign__c> sobjs = getJobAssigns();
		System.assertEquals(3, entities.size());
		System.assertEquals(sobjs[0].Id, entities[0].id);
		System.assertEquals(sobjs[1].Id, entities[1].id);
		System.assertEquals(sobjs[2].Id, entities[2].id);
		System.assertEquals(emp1.Id, sobjs[0].EmployeeBaseId__c);
		System.assertEquals(emp2.Id, sobjs[1].EmployeeBaseId__c);
		System.assertEquals(emp3.Id, sobjs[2].EmployeeBaseId__c);
	}

	/**
	 * 作成対象のジョブが存在しない場合に、エラーが発生することを検証する
	 */
	@isTest
	static void createJobAssignTestInvalidJobId() {
		Id invalidId = UserInfo.getUserId();
		try {
			System.runAs(getUser()) {
				service.createJobAssign(invalidId, null, null, null);
				System.assert(false, '例外が発生しませんでした。');
			}
		} catch (App.RecordNotFoundException e) {
			String expected = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Admin_Lbl_Job});
			System.assertEquals(expected, e.getMessage());
		}
	}

	/**
	 * updateJobAssignのテスト
	 * 正常にジョブ割当レコード1件を正常に更新出来ることを検証する
	 */
	@isTest
	static void updateJobAssignTestSingleRecord(){
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				getJob().Id,
				getEmployee().Id,
				AppDate.newInstance(2018, 4, 1),
				AppDate.newInstance(2019, 4, 1));

		//テスト実行
		List<Id> updateTargetIds = new List<Id>{jobAssign.id};
		AppDate validFrom = AppDate.newInstance(2018, 4, 5);
		AppDate validTo = AppDate.newInstance(2019, 4, 10);
		List<JobAssignEntity> entities;
		System.runAs(getUser()) {
			entities = service.updateJobAssign(updateTargetIds, validFrom, validTo);
		}

		// 検証
		List<ComJobAssign__c> sobjs = getJobAssigns();
		System.assertEquals(1, entities.size());
		System.assertEquals(1, sobjs.size());
		System.assertEquals(sobjs[0].Id, entities[0].Id);
		System.assertEquals(Date.newInstance(2018, 4, 5), sobjs[0].ValidFrom__c);
		System.assertEquals(Date.newInstance(2019, 4, 10), sobjs[0].ValidTo__c);
	}

	/**
	 * updateJobAssignのテスト
	 * ジョブ割当レコード複数件を正常に更新出来ることを検証する
	 */
	@isTest
	static void updateJobAssignTestMultiRecord() {
		// テストデータ作成
		ComEmpBase__c emp1 = getEmployee();
		ComEmpBase__c emp2 = ComTestDataUtility.createEmployeeWithHistory('テスト社員2', emp1.CompanyId__c, null, emp1.UserId__c, getPermission().Id);
		List<ComJobAssign__c> jobAssigns = new List<ComJobAssign__c>();
		jobAssigns.add(ComTestDataUtility.createJobAssign(
				getJob().Id,
				emp1.Id,
				AppDate.newInstance(2017, 9, 1),
				AppDate.newInstance(2019, 8, 31)));
		jobAssigns.add(ComTestDataUtility.createJobAssign(
				getJob().Id,
				emp2.Id,
				AppDate.newInstance(2017, 9, 1),
				AppDate.newInstance(2019, 8, 31)));

		//テスト実行
		List<Id> updateTargetIds = new List<Id>();
		for (ComJobAssign__c jobAssign : jobAssigns) {
			updateTargetIds.add(jobAssign.id);
		}
		AppDate validFrom = AppDate.newInstance(2017, 4, 7);
		AppDate validTo = AppDate.newInstance(2019, 8, 31);
		List<JobAssignEntity> entities;
		System.runAs(getUser()) {
			entities = service.updateJobAssign(updateTargetIds, validFrom, validTo);
		}

		// 検証
		List<ComJobAssign__c> sobjs = getJobAssigns();
		System.assertEquals(2, entities.size());
		for (ComJobAssign__c sobj : sobjs) {
			for (JobAssignEntity entity : entities) {
				if (sobj.Id == entity.Id) {
					System.assertEquals(entity.employeeBaseId, sobj.EmployeeBaseId__c);
					System.assertEquals(Date.newInstance(2017, 4, 7), sobj.ValidFrom__c);
					System.assertEquals(Date.newInstance(2019, 8, 31), sobj.ValidTo__c);
				}
			}
		}
	}

	/**
	 * updateJobAssignのテスト
	 * 取得時複数件Jobが該当している場合はエラーが発生することを確認する
	 */
	@isTest
	static void updateJobAssignTestInvalidMultiJob() {
		// テストデータ作成
		ComEmpBase__c emp1 = getEmployee();
		ComJob__c job = getJob();
		ComJob__c job2 = ComTestDataUtility.createJob('Job2', job.CompanyId__c, job.JobTypeId__c);
		List<ComJobAssign__c> jobAssigns = new List<ComJobAssign__c>();
		jobAssigns.add(ComTestDataUtility.createJobAssign(
				job.Id,
				emp1.Id,
				AppDate.newInstance(2018, 5, 1),
				AppDate.newInstance(2019, 6, 1)));
		jobAssigns.add(ComTestDataUtility.createJobAssign(
				job2.Id,
				emp1.Id,
				AppDate.newInstance(2018, 7, 1),
				AppDate.newInstance(2019, 8, 1)));

		//テスト実行
		List<Id> updateTargetIds = new List<Id>();
		for (ComJobAssign__c jobAssign : jobAssigns) {
			updateTargetIds.add(jobAssign.id);
		}
		AppDate validFrom = AppDate.newInstance(2018, 9, 5);
		AppDate validTo = AppDate.newInstance(2019, 10, 10);
		try {
			System.runAs(getUser()) {
				service.updateJobAssign(updateTargetIds, validFrom, validTo);
			}
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, e.getErrorCode());
		} catch (Exception e) {
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}
	}

	/**
	 * updateJobAssignのテスト
	 * Jobが存在しない場合はエラーが発生することを確認する
	 */
	@isTest
	static void updateJobAssignTestInvalidNotFoundJob() {
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				getJob().Id,
				getEmployee().Id,
				AppDate.newInstance(2018, 1, 1),
				AppDate.newInstance(2019, 1, 1));
		// 該当ジョブを削除
		ComJob__c job = getJob();
		delete job;

		//テスト実行
		List<Id> updateTargetIds = new List<Id>{jobAssign.id};
		AppDate validFrom = AppDate.newInstance(2018, 1, 5);
		AppDate validTo = AppDate.newInstance(2019, 1, 10);
		try {
			System.runAs(getUser()) {
				service.updateJobAssign(updateTargetIds, validFrom, validTo);
			}
		} catch (App.RecordNotFoundException e) {
			System.assertEquals(App.ERR_CODE_RECORD_NOT_FOUND, e.getErrorCode());
		} catch (Exception e) {
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}
	}

	/**
	 * deleteJobAssignのテスト
	 * 正常にジョブ割当レコード1件を正常に削除出来ることを検証する
	 */
	@isTest
	static void deleteJobAssignTestSingleRecord(){
		// テストデータ作成
		ComJobAssign__c jobAssign = ComTestDataUtility.createJobAssign(
				getJob().Id,
				getEmployee().Id,
				AppDate.newInstance(2018, 4, 1),
				AppDate.newInstance(2019, 4, 1));

		//テスト実行
		List<Id> deleteTargetIds = new List<Id>{jobAssign.id};
		System.runAs(getUser()) {
			service.deleteJobAssign(deleteTargetIds);
		}

		// 検証
		List<ComJobAssign__c> sobjs = getJobAssigns();
		System.assertEquals(0, sobjs.size());
	}

	/**
	 * deleteJobAssignのテスト
	 * ジョブ割当レコード複数件を正常に削除出来ることを検証する
	 */
	@isTest
	static void deleteJobAssignTestMultiRecord() {
		// テストデータ作成
		ComEmpBase__c emp1 = getEmployee();
		ComEmpBase__c emp2 = ComTestDataUtility.createEmployeeWithHistory('テスト社員2', emp1.CompanyId__c, null, emp1.UserId__c, getPermission().Id);
		List<ComJobAssign__c> jobAssigns = new List<ComJobAssign__c>();
		jobAssigns.add(ComTestDataUtility.createJobAssign(
				getJob().Id,
				emp1.Id,
				AppDate.newInstance(2017, 9, 1),
				AppDate.newInstance(2019, 8, 31)));
		jobAssigns.add(ComTestDataUtility.createJobAssign(
				getJob().Id,
				emp2.Id,
				AppDate.newInstance(2017, 9, 1),
				AppDate.newInstance(2019, 8, 31)));

		//テスト実行
		List<Id> deleteTargetIds = new List<Id>();
		for (ComJobAssign__c jobAssign : jobAssigns) {
			deleteTargetIds.add(jobAssign.id);
		}
		System.runAs(getUser()) {
			service.deleteJobAssign(deleteTargetIds);
		}

		// 検証
		List<ComJobAssign__c> sobjs = getJobAssigns();
		System.assertEquals(0, sobjs.size());
	}

	/**
	 * deleteJobAssignのテスト
	 * 空のジョブ割当IDが渡された場合、エラーが発生することを確認する
	 */
	@isTest
	static void deleteJobAssignTestEmptyIds() {
		//nullの場合
		try {
			System.runAs(getUser()) {
				service.deleteJobAssign(null);
			}
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'jobAssignIds'});
			System.assertEquals(expected, e.getMessage());
		} catch (Exception e) {
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}

		//空のリスト場合
		try {
			System.runAs(getUser()) {
				service.deleteJobAssign(new List<Id>());
			}
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Com_Err_NullValue(new List<String>{'jobAssignIds'});
			System.assertEquals(expected, e.getMessage());
		} catch (Exception e) {
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}
	}
}