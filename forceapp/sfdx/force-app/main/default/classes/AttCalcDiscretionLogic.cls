/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * 裁量労働制の勤怠計算ロジック
  */
public with sharing class AttCalcDiscretionLogic extends AttCalcLogic {
	public AttCalcDiscretionLogic(iOutput output, IInput input) {
		super(output, input);
	}

	// 設定した週の起算日に開始曜日に合わせる
	public override Date getStartDateWeekly(Date d) {
		Integer dayOfWeek = DateUtil.dayOfWeek(d);
		return d.addDays(
				input.getConfig(d).beginDayOfWeek > dayOfWeek
					? input.getConfig(d).beginDayOfWeek - dayOfWeek - 7
					: input.getConfig(d).beginDayOfWeek - dayOfWeek
		);
	}

	private Integer calcAllowance(Day day, WorkTimeCategory c) {
		return calcAllowance(day, c.dayType, c.isContractOut, c.isLegalOut, c.isNight, c.isCompensatory);
	}
	private Integer calcAllowanceWithoutNight(Day day, WorkTimeCategory c) {
		return calcAllowance(day, c.dayType, c.isContractOut, c.isLegalOut, false, false);
	}
	/**
	 * @deicription 割増率の計算
	 * @param day 日
	 * @param dayType 日種別
	 * @param isContractOut 所定外
	 * @param isLegalOut 法定外
	 * @param isNight 深夜勤務
	 * @param isCompensatory 代休発生
	 * @return 割増率
	 */
	private Integer calcAllowance(Day day, AttCalcAutoHolidayLogic.DayType dayType, boolean isContractOut, boolean isLegalOut, boolean isNight, boolean isCompensatory) {
		Integer up = 0;
		// 所定外
		if (isContractOut) {
			up += 100;
		}
		// 法定休日
		if (dayType == AttCalcAutoHolidayLogic.DayType.LegalHoliday) {
			up += 35;
		// 勤務日または所定休日
		} else {
			// 法定外
			if (isLegalOut) {
				up += 25;
			// 所定外法定内
			} else if (isContractOut) {
				// 法定内残業も割増を払う場合
				if (input.getConfig(day.getDate()).isPayAllowanceWhenOverContructedWorkHours) {
					up += 25;
				// 所定休日の勤務は法定内も割増を払う場合
				} else if (dayType == AttCalcAutoHolidayLogic.DayType.Holiday
						&& input.getConfig(day.getDate()).isPayAllowanceOnEveryHoliday) {
					up += 25;
				}
			}
		}
		// 深夜労働
		if (isNight) {
			up += 25;
		}
		// 代休発生
		if (isCompensatory) {
			up -= 100;
		}
		return up;
	}

	// 労働時間出力引数
	private class OutputWorkTimeParam {
		OutputWorkTimeParam(Integer compensatoryTime, Integer inputRestTime) {
			this.compensatoryTime = compensatoryTime == null ? 0 : compensatoryTime;
			this.inputRestTime = inputRestTime == null ? 0 : inputRestTime;
		}
		public Integer compensatoryTime; // 代休発生時間
		public Integer inputRestTime; // 取得休憩時間
	}

	/**
	 * @deicription 労働時間出力
	 * @param outputDay 出力日
	 * @param adjustmentDay 判定日
	 * @param workTimeRanges 勤務時間（実勤務時間を用いる）
	 * @param isContractOut 所定外
	 * @param isLegalOut 法定外
	 * @param param 労働時間出力引数
	 */
	private void outputWorkTime(Day outputDay, Day adjustmentDay, AttCalcRangesDto workTimeRanges, boolean isContractOut, boolean isLegalOut, OutputWorkTimeParam param) {
		if (workTimeRanges.total() > 0) {
			// ------------------
			// 日中の勤務時間を計算する
			// ------------------
			Integer dayTime = workTimeRanges.total(); // 日中時間
			Integer dayRestTime = Math.min(param.inputRestTime, dayTime); // 日中取得休憩
			param.inputRestTime -= dayRestTime;
			dayTime -= dayRestTime;
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, false), dayTime);

			if (!adjustmentDay.isLegalHoliday()) {
				// ------------------
				// 法定休日ではない日は週の法定基準オーバーの対象を計算するための勤務時間を計算する
				// ------------------
				if (isLegalOut) {
					// 法定外の労働は労基の基準外時間に計算
					outputDay.output.legalOutTime += dayTime; // 労基基準外時間
				} else {
					// 法定内の労働は労基の基準内時間に計算
					outputDay.output.legalInTime += dayTime; // 労働基準内時間

					// ------------------
					// さらに割増手当を払っていない時間を別途計算する
					// ------------------
					// 所定外法定内の場合
					if (isContractOut) {
						// 所定外で法定内の場合
						//  勤務日で"法定内残業も割増を払う"がOFFなら払っていない
						//  所定休日で"所定休日の勤務は法定内も割増を払う"も"法定内残業も割増を払う"もOFFなら払っていない
						if (adjustmentDay.isWorkDay()) {
							// 法定内残業も割増を払わない
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) {
								outputDay.output.regularRateWorkTime += dayTime;
							}
						}
						else {
							// 所定休日の勤務は法定内も割増を払わない かつ 法定内残業も割増を払わない
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceOnEveryHoliday
									&& !input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) {
								outputDay.output.regularRateWorkTime += dayTime;
							}
						}
					}
					// 所定内の時間は全部記録する
					else {
						outputDay.output.regularRateWorkTime += dayTime;
					}
				}
			}
		}
	}

	/**
	 * @deicription 労働時間出力
	 * @param outputDay 出力日
	 * @param adjustmentDay 判定日
	 * @param workTime 勤務時間（設定値を用いる）
	 * @param isContractOut 所定外
	 * @param isLegalOut 法定外
	 * @param param 労働時間出力引数
	 */
	private void outputWorkTime(Day outputDay, Day adjustmentDay, Integer workTime, boolean isContractOut, boolean isLegalOut) {
		if (workTime > 0) {
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, false), workTime);
			if (!adjustmentDay.isLegalHoliday()) {
				// ------------------
				// 法定休日ではない日は週の法定基準オーバーの対象を計算するための勤務時間を計算する
				// ------------------
				if (isLegalOut) {
					// 法定外の労働は労基の基準外時間に計算
					outputDay.output.legalOutTime += workTime; // 労基基準外時間
				} else {
					// 法定内の労働は労基の基準内時間に計算
					outputDay.output.legalInTime += workTime; // 労働基準内時間

					// ------------------
					// さらに割増手当を払っていない時間を別途計算する
					// ------------------
					// 所定外法定内の場合
					if (isContractOut) {
						// 所定外で法定内の場合
						// 勤務日で"法定内残業も割増を払う"がOFFなら払っていない
						// 所定休日で"所定休日の勤務は法定内も割増を払う"も"法定内残業も割増を払う"もOFFなら払っていない
						if (adjustmentDay.isWorkDay()) {
							// 法定内残業も割増を払わない
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) {
								outputDay.output.regularRateWorkTime += workTime;
							}
						}
						else {
							// 所定休日の勤務は法定内も割増を払わない かつ 法定内残業も割増を払わない
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceOnEveryHoliday
									&& !input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) {
								outputDay.output.regularRateWorkTime += workTime;
							}
						}
					}
					// 所定内法定内の場合
					else {
						// 所定内の時間は全部記録する
						outputDay.output.regularRateWorkTime += workTime;
					}
				}
			}
		}
	}

	// 日次勤怠計算処理(裁量労働制)
	public override Boolean calcAttendanceDaily(Day day, Day dayNext) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcDiscretionLogic.calcAttendanceDaily start');
		System.debug(LoggingLevel.DEBUG, '---day is ' + day.getDate());

		// 出退勤有無フラグ
		Boolean withStartEndTime = (day.input.inputStartTime != null || day.input.inputEndTime != null);
		day.output.realWorkDay = (withStartEndTime ? 1 : 0);
		day.output.legalHolidayWorkCount = (day.isLegalHoliday() && withStartEndTime ? 1 : 0);
		day.output.holidayWorkCount = (day.isHoliday() && withStartEndTime ? 1 : 0);
		day.output.workDay = (day.isWorkDay() ? 1 : 0);
		day.output.convertedWorkTime = day.input.convertedWorkTime;

		// 出勤時刻または退勤時刻がない場合は計算せずに処理を終了する
		if (day.input.inputStartTime == null || day.input.inputEndTime == null) {
			System.debug(LoggingLevel.DEBUG, '---AttCalcDiscretionLogic.calcAttendanceDaily return');
			return false;
		}

		// 対象日の設定
		Config conf = input.getConfig(day.getDate());

		// 勤務時間
		AttCalcRangesDto workTimeRanges = AttCalcRangesDto.RS(day.input.inputStartTime, day.input.inputEndTime).exclude(day.input.inputRestRanges).exclude(day.input.convertdRestRanges);
		// 取得休憩
		AttCalcRangesDto inputRestRanges = day.input.inputRestRanges.cutBetween(day.input.inputStartTime, day.input.inputEndTime);

		// 許可されてない勤務時間は認めない場合
		workTimeRanges = workTimeRanges.include(day.input.allowedWorkRanges);
		inputRestRanges = inputRestRanges.include(day.input.allowedWorkRanges);

		Integer realWorkTime = workTimeRanges.total();
		day.output.realWorkTime = Math.max(0, realWorkTime - day.input.inputRestTime);
		day.output.unpaidLeaveLostTime = 0;
		day.output.paidLeaveTime = 0;
		day.output.unpaidLeaveTime = 0;
		day.output.earlyLeaveTime = 0;
		day.output.lateArriveTime = 0;
		day.output.breakTime = Math.min(realWOrkTime, day.input.inputRestTime) + inputRestRanges.total();
		day.output.irregularBreakTime = 0;
		day.output.earlyLeaveLostTime = 0;
		day.output.lateArriveLostTime = 0;
		day.output.breakLostTime = 0;
		day.output.authorizedEarlyLeaveTime = 0;
		day.output.authorizedLateArriveTime = 0;
		day.output.authorizedBreakTime = 0;
		day.output.unknowBreakTime = 0;
		AttCalcRangesDto workRangeNextDay = AttCalcRangesDto.RS();
		day.output.regularRateWorkTime = 0;
		day.output.legalInTime = 0;
		day.output.legalOutTime = 0;
		AttCalcRangesDto nightTime = workTimeRanges.exclude(conf.workTimePeriodDaily);

		// 翌日と連続して勤務計算可能ではない場合
		if (!getCanCalcContinueNextDay(day, dayNext)) {
			System.debug(LoggingLevel.DEBUG, '---can not CalcContinue to NextDay');

			if ((day.isLegalHoliday() && !dayNext.isLegalHoliday()) // 今日が法定休日で翌日が違うとき
			||	(!day.isLegalHoliday() && dayNext.isLegalHoliday()) // 今日が法定休日でなくて翌日が法定休日のとき
			||	(!day.isHoliday() && dayNext.isHoliday() && !conf.isIncludeHolidayWorkInDeemedTime)) { // 今日が勤務日で明日が裁量対象でない所定休日のとき

				// 今日と明日を分けて計算する必要がある
				Integer boundaryTime = conf.boundaryTimeOfDay;
				boundaryTime += 24 * 60;
				workRangeNextDay = workTimeRanges.cutAfter(boundaryTime);
				workTimeRanges = workTimeRanges.cutBefore(boundaryTime);
			}
		}
		// 労働時間出力引数
		OutputWorkTimeParam param = new OutputWorkTimeParam(day.input.compensatoryDayTime, day.input.inputRestTime);
		// 勤務日
		if (day.isWorkDay()) {
			// 法定休日
			if (day.isLegalHoliday()) {
				outputWorkTime(day, day, conf.deemedWorkTime, true, true);
				// 所定外法定外に入れる(グラフ表示用)
				day.output.outContractOutLegalRanges = workTimeRanges.insertAndMerge(workRangeNextDay);
			}
			else {
				// 法定外残業
				Integer outLegalOvertime = Math.max(conf.deemedWorkTime - conf.legalWorkTimeDaily, 0);
				// 法定内残業
				Integer inLegalOvertime = Math.max(conf.deemedWorkTime - outLegalOvertime - day.input.contractWorkTime, 0);
				// 所定外法定外の労働時間
				outputWorkTime(day, day, outLegalOvertime, true, true);
				// 所定外法定内の労働時間
				outputWorkTime(day, day, inLegalOvertime, true, false);
				// 所定内法定内の労働時間
				outputWorkTime(day, day, conf.deemedWorkTime - outLegalOvertime - inLegalOvertime, false, false);
				// 所定内法定外の翌日労働時間
				outputWorkTime(day, dayNext, workRangeNextDay.total(), false, true);
				// グラフ表示用項目(所定内法定内)
				day.output.inContractInLegalRanges = workTimeRanges.insertAndMerge(workRangeNextDay);
			}
			day.output.deemedWorkTime = conf.deemedWorkTime;
		}
		// 法定休日
		else if (day.isLegalHoliday()) {
			// 所定外法定外の労働時間
			outputWorkTime(day, day, workTimeRanges, true, true, param);
			// 所定外法定外の翌日労働時間
			outputWorkTime(day, dayNext, workRangeNextDay, true, true, param);
			day.output.customWorkTimes[6] = Math.min(day.input.compensatoryDayTime, workTimeRanges.total() + workRangeNextDay.total());
			day.output.deemedWorkTime = 0;
			// グラフ表示用項目(所定外法定外)
			day.output.outContractOutLegalRanges = workTimeRanges.insertAndMerge(workRangeNextDay);
		}
		// 所定休日（みなし労働時間に含む）
		else if (conf.isIncludeHolidayWorkInDeemedTime) {
			// 法定外残業
			Integer outLegalOvertime = Math.max(conf.deemedWorkTime - conf.legalWorkTimeDaily, 0);
			// 所定外法定外の労働時間
			outputWorkTime(day, day, outLegalOvertime, true, true);
			// 所定外法定内の労働時間
			outputWorkTime(day, day, conf.deemedWorkTime - outLegalOvertime, true, false);
			// 所定外法定外の翌日労働時間
			outputWorkTime(day, dayNext, workRangeNextDay, true, true, param);
			day.output.customWorkTimes[6] = Math.min(day.input.compensatoryDayTime, conf.deemedWorkTime);
			day.output.deemedWorkTime = conf.deemedWorkTime;
			Integer t1 = workTimeRanges.timeFromStart(conf.legalWorkTimeDaily);
			// グラフ表示用項目(所定外法定内)
			day.output.outContractInLegalRanges = workTimeRanges.cutBefore(t1);
			// グラフ表示用項目(所定外法定外)
			day.output.outContractOutLegalRanges = workTimeRanges.cutAfter(t1).insertAndMerge(workRangeNextDay);
		}
		// 所定休日（みなし労働時間に含まない）
		else {
			Integer t1 = workTimeRanges.timeFromStart(conf.legalWorkTimeDaily);
			// 法定内残業
			AttCalcRangesDto outContractInLegalRanges = workTimeRanges.cutBefore(t1);
			// 法定外残業
			AttCalcRangesDto outContractOutLegalRanges = workTimeRanges.cutAfter(t1);
			// 所定外法定内の労働時間
			outputWorkTime(day, day, outContractInLegalRanges, true, false, param);
			// 所定外法定外の労働時間
			outputWorkTime(day, day, outContractOutLegalRanges, true, true, param);
			// 所定外法定外の翌日労働時間
			outputWorkTime(day, dayNext, workRangeNextDay, true, true, param);
			day.output.customWorkTimes[6] = Math.min(day.input.compensatoryDayTime, workTimeRanges.total());
			day.output.deemedWorkTime = 0;
			// グラフ表示用項目(所定外法定内)
			day.output.outContractInLegalRanges = outContractInLegalRanges;
			// グラフ表示用項目(所定外法定外)
			day.output.outContractOutLegalRanges = outContractOutLegalRanges.insertAndMerge(workRangeNextDay);
		}
		day.output.customWorkTimes[5] = nightTime.total();

		return true;
	}

	// 週単位勤怠計算処理(裁量労働制)
	public override List<Date> calcAttendanceWeekly(Date d) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcDiscretionLogic.calcAttendanceWeekly start');

		Integer daysInPeriod = 7;
		// 変更日
		List<Date> updateDateList = new List<Date>();
		Day[] dayList = new Day[daysInPeriod];
		for (Integer i = 0; i < daysInPeriod; i++) {
			dayList[i] = db.get(d.addDays(i));
		}

		// 労基基準内時間合計
		Integer legalWorkTimeSum = 0;
		// 割増未支給労働時間合計
		Integer noPaidAllowanceTimeSum = 0;

		for (Integer i = 0; i < daysInPeriod; i++) {
			if (dayList[i] == null) {
				continue;
			}
			System.debug(LoggingLevel.DEBUG, '---calc d is ' + dayList[i].getDate());
			// 週の法定労働時間
			Integer legalWorkTimeAWeek = input.getConfig(dayList[i].input.targetDate).legalWorkTimeAWeek;
			// 前日までの労基基準内時間合計
			Integer legalInTimeTotalUntilPreDay = legalWorkTimeSum;
			// 前日までの割増未支給労働時間合計
			Integer regularRateWorkTimeTotalUntilPreDay = noPaidAllowanceTimeSum;
			legalWorkTimeSum += dayList[i].output.legalInTime;
			noPaidAllowanceTimeSum += dayList[i].output.regularRateWorkTime;
			System.debug(LoggingLevel.DEBUG, '---calc legalWorkTimeSum is ' + legalWorkTimeSum);
			System.debug(LoggingLevel.DEBUG, '---calc noPaidAllowanceTimeSum is ' + noPaidAllowanceTimeSum);
			// 労基基準超過時間
			Integer regularOverTime = 0;
			if (legalWorkTimeSum > legalWorkTimeAWeek)
			{
				regularOverTime = legalWorkTimeSum - Math.max(legalInTimeTotalUntilPreDay, legalWorkTimeAWeek);
			}
			// 割増手当追加発生時間
			Integer additionalAllowanceTime = 0;
			if (noPaidAllowanceTimeSum > legalWorkTimeAWeek)
			{
				additionalAllowanceTime = noPaidAllowanceTimeSum - Math.max(regularRateWorkTimeTotalUntilPreDay, legalWorkTimeAWeek);
			}
			// 変更があったかどうかのチェック
			if (dayList[i].output.regularOverTime != regularOverTime
					|| dayList[i].output.additionalAllowanceTime != additionalAllowanceTime
					|| dayList[i].output.noPaidAllowanceTimeSum != noPaidAllowanceTimeSum) {
				dayList[i].output.regularOverTime = regularOverTime;
				dayList[i].output.noPaidAllowanceTimeSum = noPaidAllowanceTimeSum;
				dayList[i].output.additionalAllowanceTime = additionalAllowanceTime;
				updateDateList.add(dayList[i].input.targetDate);
			}
			System.debug(LoggingLevel.DEBUG, '---calc regularOverTime is ' + dayList[i].output.regularOverTime);
			System.debug(LoggingLevel.DEBUG, '---calc noPaidAllowanceTimeSum is ' + dayList[i].output.noPaidAllowanceTimeSum);
			System.debug(LoggingLevel.DEBUG, '---calc additionalAllowanceTime is ' + dayList[i].output.additionalAllowanceTime);
		}
		System.debug(LoggingLevel.DEBUG, '---AttCalcDiscretionLogic.calcAttendanceWeekly end');
		return updateDateList;
	}
	// 月次勤怠計算処理(裁量労働制)
	public override void calcAttendanceMonthly(Date d) {
		Date startDate = input.getStartDateMonthly(d);
		Date endDate = input.getEndDateMonthly(d);
		TotalOutput totalOutput = new TotalOutput();
		for (Date dt = startDate; dt <= endDate; dt = dt.addDays(1)) {
			Day day = db.get(dt);
			if (day == null) {
				continue;
			}
			totalOutput.realWorkTime += day.output.realWorkTime;
			totalOutput.convertedWorkTime += day.output.convertedWorkTime;
			totalOutput.unpaidLeaveLostTime += day.output.unpaidLeaveLostTime;
			totalOutput.paidLeaveTime += day.output.paidLeaveTime;
			totalOutput.unpaidLeaveTime += day.output.unpaidLeaveTime;
			totalOutput.lateArriveTime += day.output.lateArriveTime;
			totalOutput.earlyLeaveTime += day.output.earlyLeaveTime;
			totalOutput.breakTime += day.output.breakTime;
			totalOutput.lateArriveLostTime += day.output.lateArriveLostTime;
			totalOutput.earlyLeaveLostTime += day.output.earlyLeaveLostTime;
			totalOutput.breakLostTime += day.output.breakLostTime;
			totalOutput.authorizedLateArriveTime += day.output.authorizedLateArriveTime;
			totalOutput.authorizedEarlyLeaveTime += day.output.authorizedEarlyLeaveTime;
			totalOutput.authorizedBreakTime += day.output.authorizedBreakTime;
			totalOutput.paidBreakTime += day.output.paidBreakTime;
			totalOutput.authorizedOffTime += day.output.authorizedOffTime;
			totalOutput.legalOutTime += day.output.legalOutTime + day.output.regularOverTime;
			totalOutput.legalInTime += day.output.legalInTime - day.output.regularOverTime;
			totalOutput.regularRateWorkTime += day.output.regularRateWorkTime;
			totalOutput.realWorkDay += day.output.RealWorkDay;
			totalOutput.workDay += day.output.workDay;
			totalOutput.legalHolidayWorkCount += day.output.legalHolidayWorkCount;
			totalOutput.holidayWorkCount += day.output.holidayWorkCount;
			// 勤務時間分類の集計
			for (WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
				Integer work = day.output.workTimeCategoryMap.get(c);
				Integer sum = totalOutput.workTimeCategoryMap.containsKey(c)
									? totalOutput.workTimeCategoryMap.get(c)
									: 0;
				totalOutput.workTimeCategoryMap.put(c,sum + work);
			}
			// カスタム労働時間の集計
			for (Integer j = 0; j < day.output.customWorkTimes.size(); j++)
				totalOutput.customWorkTimes[j] += day.output.customWorkTimes[j];
		}
		output.updateTotal(startDate, totalOutput);
	}
	// カスタマイズ再計算処理
	public override void calcAttendanceCustom(Day day) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcDiscretionLogic.calcAttendanceCustom start');
		System.debug(LoggingLevel.DEBUG, '---day is ' + day.getDate());
		for (Integer i = 0; i < day.output.customWorkTimes.size(); i++) {
			// 深夜勤務時間と代休控除時間は初期化しない
			if (i != 5 && i != 6) {
				day.output.customWorkTimes[i] = 0;
			}
		}
		for (WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
			Integer rate = calcAllowanceWithoutNight(day, c);
			Integer work = day.output.workTimeCategoryMap.get(c);
			if (rate == 100) {
				day.output.customWorkTimes[0] += work;
			}
			else if (rate == 125) {
				day.output.customWorkTimes[1] += work;
			}
			else if (rate == 135) {
				day.output.customWorkTimes[2] += work;
			}
			else if (rate == 25) {
				day.output.customWorkTimes[3] += work;
			}
			else if (rate == 35) {
				day.output.customWorkTimes[4] += work;
			}
		}
		System.debug(LoggingLevel.DEBUG, '---additionalAllowanceTime is ' + day.output.additionalAllowanceTime);
		// 所定内＋時間外割増の時間を、法定外に変換する（便利上）
		// 週次割増相殺分
		Integer allowanceOffsetedWeekly = Math.min(day.output.customWorkTimes[0], day.output.additionalAllowanceTime);
		day.output.customWorkTimes[0] -= allowanceOffsetedWeekly;
		day.output.customWorkTimes[1] += allowanceOffsetedWeekly;
		day.output.customWorkTimes[3] += day.output.additionalAllowanceTime - allowanceOffsetedWeekly;

		System.debug(LoggingLevel.DEBUG, '---AttCalcDiscretionLogic.calcAttendanceCustom end');
	}
}