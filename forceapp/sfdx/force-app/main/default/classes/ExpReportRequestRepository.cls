/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 経費申請のリポジトリ
 */
public with sharing class ExpReportRequestRepository extends Repository.BaseRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** クエリ実行時のソートキー */
	public enum SortKey {
		REPORT_NO,
		STATUS,
		REQUEST_TIME,
		SUBJECT,
		TOTAL_AMOUNT,
		EMPLOYEE_CODE,
		EMPLOYEE_NAME,
		DEPARTMENT_CODE,
		DEPARTMENT_NAME
	}


	/** クエリ実行時のソート順 */
	public enum SortOrder {
		ORDER_ASC,
		ORDER_DESC
	}

	/** 社員のリレーション名 */
	private static final String EMP_HISTRORY_R = ExpReportRequest__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	private static final String EMP_BASE_R = ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();
	private static final String USER_R = ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();
	/** 部署のリレーション名 */
	private static final String DEPT_HISTORY_R = ExpReportRequest__c.DepartmentHistoryId__c.getDescribe().getRelationshipName();
	private static final String DEPT_BASE_R = ComDeptHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/** 申請者のリレーション名 */
	private static final String ACTOR_HISTRORY_R = ExpReportRequest__c.ActorHistoryId__c.getDescribe().getRelationshipName();
	/** 経費精算reportのリレーション名 */
	private static final String EXPENSE_REPORT_R = ExpReportRequest__c.ExpReportId__c.getDescribe().getRelationshipName();

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ExpReportRequest__c.Id,
			ExpReportRequest__c.Name,
			ExpReportRequest__c.OwnerId,
			ExpReportRequest__c.ExpReportId__c,
			ExpReportRequest__c.TotalAmount__c,
			ExpReportRequest__c.ReportNo__c,
			ExpReportRequest__c.Subject__c,
			ExpReportRequest__c.Comment__c,
			ExpReportRequest__c.ProcessComment__c,
			ExpReportRequest__c.CancelComment__c,
			ExpReportRequest__c.Status__c,
			ExpReportRequest__c.ConfirmationRequired__c,
			ExpReportRequest__c.CancelType__c,
			ExpReportRequest__c.EmployeeHistoryId__c,
			ExpReportRequest__c.RequestTime__c,
			ExpReportRequest__c.DepartmentHistoryId__c,
			ExpReportRequest__c.ActorHistoryId__c,
			ExpReportRequest__c.LastApproveTime__c,
			ExpReportRequest__c.LastApproverId__c,
			ExpReportRequest__c.Approver01Id__c,
			ExpReportRequest__c.AccountingStatus__c,
			ExpReportRequest__c.PaymentDate__c,
			ExpReportRequest__c.AuthorizedTime__c,
			ExpReportRequest__c.ExportedTime__c
		};
	}

	/** UserRecordAccessの取得対象項目 */
	private static final List<String> GET_USER_RECORD_ACCESS_FIELD_NAME_LIST;
	/** UserRecordAccessの外部キー(リレーション名は取得できない) */
	private static final String USER_RECORD_ACCESS_R = 'UserRecordAccess';
	static {
		// 取得対象の項目定義(UserRecordAccess)
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			UserRecordAccess.HasEditAccess};
		GET_USER_RECORD_ACCESS_FIELD_NAME_LIST = Repository.generateFieldNameList(USER_RECORD_ACCESS_R, fieldList);
	}

	/** 共有設定のアクセスレベル */
	private static String SHARE_ACCESS_LEVEL = 'Edit';
	/** 共有の理由 */
	public static String SHARE_ROW_CAUSE = Schema.ExpReportRequest__Share.RowCause.DelegatedApprover__c;

	/** 取得対象の項目(共有情報) */
	private static final Set<Schema.SObjectField> GET_SHARE_FIELD_SET;
	static {
		GET_SHARE_FIELD_SET = new Set<Schema.SObjectField> {
			ExpReportRequest__Share.Id,
			ExpReportRequest__Share.ParentId,
			ExpReportRequest__Share.UserOrGroupId,
			ExpReportRequest__Share.AccessLevel,
			ExpReportRequest__Share.RowCause
		};
	}

	/** 検索フィルタ */
	 public class Filter {
		/** 経費申請ID */
		public List<Id> ids;
		/** 経費精算ID */
		public List<Id> reportIds;
		/** 会社ID */
		public Id companyId;
		/** 申請ステータス */
		public List<String> statusList;
		/** 経理承認一覧画面の検索に利用するステータス */
		public List<String> financeStatusList;
		/** 部署履歴ID */
		public List<Id> departmentBaseIds;
		/** 社員履歴Id */
		public List<Id> empBaseIds;
		/** Approver01 Id */
		public Id approver01Id;
		/** 申請日 **/
		public ExpReportRequestRepository.DateRange requestDateRange;
		/** 計上日 **/
		public ExpReportRequestRepository.DateRange accountingDateRange;
		/** 申請番号 **/
		public String reportNo;


		/** 領収書添付の有無 / if receipt attached **/
		// public Boolean hasReceipt;
		/** 費目 **/
		// public List<Id> expTypeIds;
		/** 金額 **/
		public ExpReportRequestRepository.AmountRange amountRange;
	 }

	 /** 日付のレンジ */
	 public class DateRange {
		/** 開始日 **/
		public Date startDate;
		/** 終了日 **/
		public Date endDate;
	 }

	 /** 金額のレンジ */
	 public class AmountRange {
		/** 開始日 **/
		public Decimal startAmount;
		/** 終了日 **/
		public Decimal endAmount;
	 }

	 /** 検索フィルタ（共有情報） */
	 private class  ShareFilter {
		/** 経費申請ID */
		public List<Id> requestIds;
	 }

	/**
	 * 経費申請エンティティを取得する
	 * @param requestId 経費申請ID
	 * @return 経費申請エンティティ
	 */
	public ExpReportRequestEntity getEntity(Id requestId) {
		// 検索実行
		List<ExpReportRequestEntity> entityList = getEntityList(new List<Id>{requestId});

		ExpReportRequestEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * 経費申請エンティティのリストを取得する
	 * @param reportIds 経費申請ID
	 * @return 条件に一致した経費申請エンティティのリスト
	 */
	public List<ExpReportRequestEntity> getEntityList(List<Id> requestIds) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.ids = requestIds;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate, null, null);
	}

	/**
	 * 経費申請エンティティのリストを取得する
	 * @param reportIds 経費精算ID
	 * @return 条件に一致した経費申請エンティティのリスト
	 */
	public List<ExpReportRequestEntity> getEntityListFromReportId(List<Id> reportIds) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.reportIds = reportIds;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate, null, null);
	}

	/**
	 * 経費申請エンティティのリストを取得する
	 * @param empBaseId 社員ベースID
	 * @param status ステータス
	 * @return 条件に一致した経費申請エンティティのリスト
	 */
	public List<ExpReportRequestEntity> getEntityListByEmpId(Id empBaseId, AppRequestStatus status) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.empBaseIds = new List<Id>{empBaseId};
		filter.statusList = status == null ? null : new List<String>{status.value};

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate, null, null);
	}

	/**
		 * Get report request entity list
		 * @param approver01Id Approver01 User ID
		 * @param status Status
		 * @return report request entity list matching
		 */
	public List<ExpReportRequestEntity> getEntityListByApprover01Id(Id approver01Id, AppRequestStatus status) {
		Filter filter = new Filter();
		filter.approver01Id = approver01Id;
		filter.statusList = status == null ? null : new List<String>{status.value};

		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate, null, null);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(ExpReportRequestEntity entity) {
		return saveEntityList(new List<ExpReportRequestEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<ExpReportRequestEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<ExpReportRequestEntity> entityList, Boolean allOrNone) {
		List<ExpReportRequest__c> objList = new List<ExpReportRequest__c>();

		// エンティティからSObjectを作成する
		for (ExpReportRequestEntity entity : entityList) {
			ExpReportRequest__c obj = createObj(entity);
			objList.add(obj);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(objList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 経費申請を検索するpublicメソッド / public method for searching expense request
	 * @param filter 検索条件
	 * @param sortKey ソートキー
	 * @param sortOrder ソート順
	 * @return 条件に一致した経費申請エンティティのリスト
	 */
	public List<ExpReportRequestEntity> searchEntity(Filter filter, SortKey sortKey, SortOrder sortOrder) {
        // 検索を実行する
        final Boolean forUpdate = false;
        return searchEntity(filter, forUpdate, sortKey, sortOrder);
    }

	/**
	 * 経費申請を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortKey ソートキー
	 * @param sortOrder ソート順
	 * @return 条件に一致した経費申請エンティティのリスト
	 */
	 private List<ExpReportRequestEntity> searchEntity(Filter filter, Boolean forUpdate, SortKey sortKey, SortOrder sortOrder) {

		// SELECT対象項目リスト
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// 関連項目（社員）
		selectFldList.addAll(getSelectFldListEmployee(EMP_HISTRORY_R));
		// 関連項目（部署）
		selectFldList.addAll(getSelectFldListDepartment(DEPT_HISTORY_R));
		// 関連項目（申請者）
		selectFldList.addAll(getSelectFldListEmployee(ACTOR_HISTRORY_R));
		// UserRecordAccess
		selectFldList.addAll(GET_USER_RECORD_ACCESS_FIELD_NAME_LIST);

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> ids = filter.ids;
		if (ids != null && !ids.isEmpty()) {
			condList.add(getFieldName(ExpReportRequest__c.Id) + ' IN :ids');
		}
		final List<Id> reportIds = filter.reportIds;
		if (reportIds != null && !reportIds.isEmpty()) {
			condList.add(getFieldName(ExpReportRequest__c.ExpReportId__c) + ' IN :reportIds');
		}
		final Id companyId = filter.companyId;
		if (companyId != null) {
			condList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.CompanyId__c) + ' = :companyId');
		}

		if (filter.reportNo != null) {
			final String reportNo = '%' + filter.reportNo + '%';
			condList.add(getFieldName(EXPENSE_REPORT_R, ExpReport__c.ReportNo__c) + ' LIKE :reportNo');
		}

		final List<String> statusList = new List<String>();
		if (filter.statusList != null && !filter.statusList.isEmpty()) {
			for (String status : filter.statusList) {
				statusList.add(status);
			}
			condList.add(getFieldName(ExpReportRequest__c.Status__c) + ' IN :statusList');
		}
		final List<Id> departmentBaseIds = filter.departmentBaseIds;
		if (departmentBaseIds != null && !departmentBaseIds.isEmpty()) {
			condList.add(getFieldName(DEPT_HISTORY_R, DEPT_BASE_R, ComDeptBase__c.Id) + ' IN :departmentBaseIds');
		}
		final List<Id> empBaseIds = filter.empBaseIds;
		if (empBaseIds != null && !empBaseIds.isEmpty()) {
			condList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.Id) + ' IN :empBaseIds');
		}
		final Id approver01Id = filter.approver01Id;
		if (approver01Id != null) {
			condList.add(getFieldName(ExpReportRequest__c.Approver01Id__c) + ' = :approver01Id');
		}

		// 申請日
		if(filter.requestDateRange!=null){
			final Date requestDateStart = filter.requestDateRange.startDate;
			Datetime requestDateTimeStart = Datetime.newInstance(requestDateStart.year(), requestDateStart.month(), requestDateStart.day(), 0, 0, 0);
			if (requestDateTimeStart != null) {
				condList.add(getFieldName(ExpReportRequest__c.RequestTime__c) + ' >= :requestDateTimeStart');
			}
			final Date requestDateEnd = filter.requestDateRange.endDate;
			Datetime requestDateTimeEnd = Datetime.newInstance(requestDateEnd.year(), requestDateEnd.month(), requestDateEnd.day(), 23, 59, 59);
			if(requestDateTimeEnd!=null){
				condList.add(getFieldName(ExpReportRequest__c.RequestTime__c) + ' <= :requestDateTimeEnd');
			}
		}

		// 計上日
		if(filter.accountingDateRange!=null){
				final Date accountingDateStart = filter.accountingDateRange.startDate;
				if (accountingDateStart != null) {
					condList.add(getFieldName(EXPENSE_REPORT_R, ExpReport__c.AccountingDate__c) + ' >= :accountingDateStart');
				}

				if(filter.accountingDateRange.endDate!=null){
					final Date accountingDateEnd = filter.accountingDateRange.endDate;
					if (accountingDateEnd != null) {
						condList.add(getFieldName(EXPENSE_REPORT_R, ExpReport__c.AccountingDate__c) + ' <= :accountingDateEnd');
					}
				}
		}

		if(filter.amountRange!=null){
				final Decimal startAmount = filter.amountRange.startAmount;
				if (startAmount != null) {
					condList.add(getFieldName(EXPENSE_REPORT_R, ExpReport__c.TotalAmount__c) + ' >= :startAmount');
				}
				final Decimal endAmount = filter.amountRange.endAmount;
				if (endAmount != null) {
					condList.add(getFieldName(EXPENSE_REPORT_R, ExpReport__c.TotalAmount__c) + ' <= :endAmount');
				}
		}

		String statusSearchSql1 = 'Status__c = \'Approved\' AND CancelType__c = null AND AccountingStatus__c = \'None\' AND AuthorizedTime__c = null AND ExportedTime__c = null AND PaymentDate__c = null';
		String statusSearchSql2 = 'Status__c = \'Approved\' AND CancelType__c = null AND AccountingStatus__c = \'Authorized\' AND AuthorizedTime__c != null AND ExportedTime__c = null AND PaymentDate__c = null';
		String statusSearchSql3 = 'Status__c = \'Disabled\' AND CancelType__c = \'Canceled\' AND AccountingStatus__c = \'Rejected\' AND AuthorizedTime__c = null AND ExportedTime__c = null AND PaymentDate__c = null';
		String statusSearchSql4 = 'Status__c = \'Approved\' AND CancelType__c = null AND AccountingStatus__c = \'Authorized\' AND AuthorizedTime__c != null AND ExportedTime__c != null AND PaymentDate__c = null';
		String statusSearchSql5 = 'Status__c = \'Approved\' AND CancelType__c = null AND AccountingStatus__c = \'Fully Paid\' AND AuthorizedTime__c != null AND ExportedTime__c != null AND PaymentDate__c != null';

		List<String> orList = new List<String>();
		if(filter.financeStatusList != null && !filter.financeStatusList.isEmpty()){
			if(filter.financeStatusList.contains('Approved')){
				orList.add(statusSearchSql1);
			}
			if(filter.financeStatusList.contains('AccountingAuthorized')){
				orList.add(statusSearchSql2);
			}
			if(filter.financeStatusList.contains('AccountingRejected')){
				orList.add(statusSearchSql3);
			}
			if(filter.financeStatusList.contains('JournalCreated')){
				orList.add(statusSearchSql4);
			}
			if(filter.financeStatusList.contains('Fully Paid')){
				orList.add(statusSearchSql5);
			}
		}

		String OrCond;
		if (orList != null && !orList.isEmpty()) {
			// ORで条件をつなぐ
			OrCond = '(' + String.join(orList, ') OR (') + ')';
			condList.add(OrCond);
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// ORDER BY句
		if (sortKey == null || sortOrder == null) {
			sortKey = ExpReportRequestRepository.SortKey.REPORT_NO;
			sortOrder = ExpReportRequestRepository.SortOrder.ORDER_DESC;
		}

		List<String> sortKeyList = new List<String>();
		// 第1ソートキー
		if (sortKey == ExpReportRequestRepository.SortKey.REPORT_NO) {
			sortKeyList.add(getFieldName(ExpReportRequest__c.ReportNo__c));
		} else if (sortKey == ExpReportRequestRepository.SortKey.STATUS) {
			sortKeyList.add(getFieldName(ExpReportRequest__c.Status__c));
			sortKeyList.add(getFieldName(ExpReportRequest__c.AccountingStatus__c));
			sortKeyList.add(getFieldName(ExpReportRequest__c.AuthorizedTime__c));
			sortKeyList.add(getFieldName(ExpReportRequest__c.ExportedTime__c));
		} else if (sortKey == ExpReportRequestRepository.SortKey.REQUEST_TIME) {
			sortKeyList.add(getFieldName(ExpReportRequest__c.RequestTime__c));
		} else if (sortKey == ExpReportRequestRepository.SortKey.SUBJECT) {
			sortKeyList.add(getFieldName(ExpReportRequest__c.Subject__c));
		} else if (sortKey == ExpReportRequestRepository.SortKey.TOTAL_AMOUNT) {
			sortKeyList.add(getFieldName(ExpReportRequest__c.TotalAmount__c));
		} else if (sortKey == ExpReportRequestRepository.SortKey.EMPLOYEE_CODE) {
			sortKeyList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.Code__c));
		} else if (sortKey == ExpReportRequestRepository.SortKey.EMPLOYEE_NAME) {
			AppLanguage.LangKey langKey = AppLanguage.getUserLang().getLangkey();
			if (langKey == AppLanguage.LangKey.L0) {
				sortKeyList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.LastName_L0__c));
				sortKeyList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.FirstName_L0__c));
			} else if (langKey == AppLanguage.LangKey.L1) {
				sortKeyList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.LastName_L1__c));
				sortKeyList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.FirstName_L1__c));
			} else if (langKey == AppLanguage.LangKey.L2) {
				sortKeyList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.LastName_L2__c));
				sortKeyList.add(getFieldName(EMP_HISTRORY_R, EMP_BASE_R, ComEmpBase__c.FirstName_L2__c));
			}
		} else if (sortKey == ExpReportRequestRepository.SortKey.DEPARTMENT_CODE) {
			sortKeyList.add(getFieldName(DEPT_HISTORY_R, DEPT_BASE_R, ComDeptBase__c.Code__c));
		} else if (sortKey == ExpReportRequestRepository.SortKey.DEPARTMENT_NAME) {
			AppLanguage.LangKey langKey = AppLanguage.getUserLang().getLangkey();
			if (langKey == AppLanguage.LangKey.L0) {
				sortKeyList.add(getFieldName(DEPT_HISTORY_R, ComDeptHistory__c.Name_L0__c));
			} else if (langKey == AppLanguage.LangKey.L1) {
				sortKeyList.add(getFieldName(DEPT_HISTORY_R, ComDeptHistory__c.Name_L1__c));
			} else if (langKey == AppLanguage.LangKey.L2) {
				sortKeyList.add(getFieldName(DEPT_HISTORY_R, ComDeptHistory__c.Name_L2__c));
			}
		}
		// 第2ソートキー
		if (sortKey != ExpReportRequestRepository.SortKey.REPORT_NO) {
			sortKeyList.add(getFieldName(ExpReportRequest__c.ReportNo__c));
		}
		// ソート順
		String order;
		if (sortOrder == ExpReportRequestRepository.SortOrder.ORDER_ASC) {
			order = ' ASC NULLS LAST';
		} else if (sortOrder == ExpReportRequestRepository.SortOrder.ORDER_DESC) {
			order = ' DESC NULLS LAST';
		}

		String orderBy = ' ORDER BY ' + String.join(sortKeyList, order + ', ') + order;

		// SOQLを作成する
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ExpReportRequest__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		soql += orderBy
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('ExpReportRequestRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<ExpReportRequestEntity> entityList = new List<ExpReportRequestEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ExpReportRequest__c)sobj));
		}

		return entityList;
	 }

	/**
	 * 共有エンティティのリストを取得する
	 * @param requestIds 経費申請ID
	 * @return 条件に一致した共有エンティティのリスト
	 */
	public List<AppShareEntity> getShareEntityListByRequestId(List<Id> requestIds) {
		// 検索条件を作成する
		ShareFilter filter = new ShareFilter();
		filter.requestIds = requestIds;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchShareEntity(filter, forUpdate);
	}

	/**
	 * 経費申請の共有設定を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したエンティティのリスト
	 */
	 private List<AppShareEntity> searchShareEntity(ShareFilter filter, Boolean forUpdate) {

		// SELECT対象項目リスト
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_SHARE_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> requestIds = filter.requestIds;
		if (requestIds != null && !requestIds.isEmpty()) {
			condList.add(getFieldName(ExpReportRequest__Share.ParentId) + ' IN :requestIds');
		}
		condList.add(getFieldName(ExpReportRequest__Share.RowCause) + ' = :SHARE_ROW_CAUSE');

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// SOQLを作成する
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ExpReportRequest__Share.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		soql += ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('ExpReportRequestRepository.searchShareEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<AppShareEntity> entityList = new List<AppShareEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createShareEntity((ExpReportRequest__Share)sobj));
		}

		return entityList;
	 } 

	/**
	 * 社員オブジェクトのSELECT句の取得項目を作成する
	 * @param relationshipName 社員項目のリレーションシップ名
	 * @return 社員オブジェクトから取得する項目一覧
	 */
	private List<String> getSelectFldListEmployee(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.Code__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.FirstName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.FirstName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.FirstName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.LastName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.LastName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.LastName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, ComEmpBase__c.UserId__c));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, USER_R, User.SmallPhotoUrl));
		selectFldList.add(getFieldName(relationshipName, EMP_BASE_R, USER_R, User.IsActive));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidFrom__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidTo__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.Removed__c));
		return selectFldList;
	}

	/**
	 * 部署オブジェクトのSELECT句の取得項目を作成する
	 * @param relationshipName 部署項目のリレーションシップ名
	 * @return 部署オブジェクトから取得する項目一覧
	 */
	private List<String> getSelectFldListDepartment(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, DEPT_BASE_R, ComDeptBase__c.Code__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L0__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L1__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L2__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.ValidFrom__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.ValidTo__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Removed__c));
		return selectFldList;
	}

	/**
	 * ExpReportRequest__cオブジェクトからExpReportRequestEntityを作成する
	 * @param obj 作成元のExpReportRequest__cオブジェクト
	 * @return 作成したExpReportRequestEntity
	 */
	private ExpReportRequestEntity createEntity(ExpReportRequest__c obj) {
		ExpReportRequestEntity entity = new ExpReportRequestEntity();

		entity.setId(obj.Id);
		entity.name = obj.Name;
		entity.ownerId = obj.OwnerId;
		entity.expReportId = obj.ExpReportId__c;
		entity.totalAmount = obj.TotalAmount__c;
		entity.reportNo = obj.ReportNo__c;
		entity.subject = obj.Subject__c;
		entity.comment = obj.Comment__c;
		entity.cancelComment = obj.CancelComment__c;
		entity.processComment = obj.ProcessComment__c;
		entity.status = AppRequestStatus.valueOf(obj.Status__c);
		entity.isConfirmationRequired = obj.ConfirmationRequired__c;
		entity.cancelType = AppCancelType.valueOf(obj.CancelType__c);
		entity.employeeHistoryId = obj.EmployeeHistoryId__c;
		entity.employeeBaseId = obj.EmployeeHistoryId__r.BaseId__c;
		entity.employee = new AppLookup.Employee(
				obj.EmployeeHistoryId__r.BaseId__r.Code__c,
				new AppPersonName(
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c,
					obj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c,
					obj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
					obj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c,
					obj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c),
				obj.EmployeeHistoryId__r.BaseId__r.UserId__c,
				obj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
				AppDate.valueOf(obj.EmployeeHistoryId__r.ValidFrom__c),
				AppDate.valueOf(obj.EmployeeHistoryId__r.ValidTo__c),
				obj.EmployeeHistoryId__r.Removed__c,
				obj.EmployeeHistoryId__r.BaseId__r.UserId__r.isActive);
		entity.requestTime = AppDatetime.valueOf(obj.RequestTime__c);
		entity.departmentHistoryId = obj.DepartmentHistoryId__c;
		entity.department = new AppLookup.Department(
				obj.DepartmentHistoryId__r.BaseId__r.Code__c,
				new AppMultiString(
					obj.DepartmentHistoryId__r.Name_L0__c,
					obj.DepartmentHistoryId__r.Name_L1__c,
					obj.DepartmentHistoryId__r.Name_L2__c),
				AppDate.valueOf(obj.DepartmentHistoryId__r.ValidFrom__c),
				AppDate.valueOf(obj.DepartmentHistoryId__r.ValidTo__c),
				obj.DepartmentHistoryId__r.Removed__c);
		entity.actorHistoryId = obj.ActorHistoryId__c;
		entity.actor = new AppLookup.Employee(
				obj.ActorHistoryId__r.BaseId__r.Code__c,
				new AppPersonName(
					obj.ActorHistoryId__r.BaseId__r.FirstName_L0__c,
					obj.ActorHistoryId__r.BaseId__r.LastName_L0__c,
					obj.ActorHistoryId__r.BaseId__r.FirstName_L1__c,
					obj.ActorHistoryId__r.BaseId__r.LastName_L1__c,
					obj.ActorHistoryId__r.BaseId__r.FirstName_L2__c,
					obj.ActorHistoryId__r.BaseId__r.LastName_L2__c),
				obj.ActorHistoryId__r.BaseId__r.UserId__c,
				obj.ActorHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
				AppDate.valueOf(obj.ActorHistoryId__r.ValidFrom__c),
				AppDate.valueOf(obj.ActorHistoryId__r.ValidTo__c),
				obj.ActorHistoryId__r.Removed__c,
				obj.ActorHistoryId__r.BaseId__r.UserId__r.isActive);
		entity.lastApproveTime = AppDatetime.valueOf(obj.LastApproveTime__c);
		entity.lastApproverId = obj.LastApproverId__c;
		entity.approver01Id = obj.Approver01Id__c;
		entity.accountingStatus = ExpAccountingStatus.valueOf(obj.AccountingStatus__c);
		entity.paymentDate = AppDate.valueOf(obj.PaymentDate__c);
		entity.authorizedTime = AppDatetime.valueOf(obj.AuthorizedTime__c);
		entity.exportedTime = AppDatetime.valueOf(obj.ExportedTime__c);
		entity.hasEditAccess = obj.UserRecordAccess.HasEditAccess;

		entity.resetChanged();
		return entity;
	}

	/**
	 * ExpReportRequestEntityからExpReportRequest__cオブジェクトを作成する
	 * @param entity 作成元のExpReportRequestEntity
	 * @return 作成したExpReportRequest__cオブジェクト
	 */
	private ExpReportRequest__c createObj(ExpReportRequestEntity entity) {
		ExpReportRequest__c retObj = new ExpReportRequest__c();

		retObj.Id = entity.id;
		if (entity.isChanged(ExpReportRequestEntity.Field.Name)) {
			retObj.Name = entity.name;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.ExpReportId)) {
			retObj.ExpReportId__c = entity.expReportId;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.TotalAmount)) {
			retObj.TotalAmount__c = entity.totalAmount;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.ReportNo)) {
			retObj.ReportNo__c = entity.reportNo;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.Subject)) {
			retObj.Subject__c = entity.subject;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.Comment)) {
			retObj.Comment__c = entity.comment;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.ProcessComment)) {
			retObj.ProcessComment__c = entity.processComment;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.CancelComment)) {
			retObj.CancelComment__c = entity.cancelComment;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.Status)) {
			retObj.Status__c = AppConverter.stringValue(entity.status);
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.IsConfirmationRequired)) {
			retObj.ConfirmationRequired__c = entity.isConfirmationRequired;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.CancelType)) {
			retObj.CancelType__c = AppConverter.stringValue(entity.cancelType);
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.EmployeeHistoryId)) {
			retObj.EmployeeHistoryId__c = entity.employeeHistoryId;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.RequestTime)) {
			retObj.RequestTime__c = AppDatetime.convertDatetime(entity.requestTime);
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.DepartmentHistoryId)) {
			retObj.DepartmentHistoryId__c = entity.departmentHistoryId;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.ActorHistoryId)) {
			retObj.ActorHistoryId__c = entity.actorHistoryId;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.LastApproveTime)) {
			retObj.LastApproveTime__c = AppDatetime.convertDatetime(entity.lastApproveTime);
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.LastApproverId)) {
			retObj.LastApproverId__c = entity.lastApproverId;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.Approver01Id)) {
			retObj.Approver01Id__c = entity.approver01Id;
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.AccountingStatus)) {
			retObj.AccountingStatus__c = AppConverter.stringValue(entity.accountingStatus);
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.PaymentDate)) {
			retObj.PaymentDate__c = AppDate.convertDate(entity.paymentDate);
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.AuthorizedTime)) {
			retObj.AuthorizedTime__c = AppDatetime.convertDatetime(entity.authorizedTime);
		}
		if (entity.isChanged(ExpReportRequestEntity.Field.ExportedTime)) {
			retObj.ExportedTime__c = AppDatetime.convertDatetime(entity.exportedTime);
		}

		return retObj;
	}

	/**
	 * ExpReportRequest__ShareオブジェクトからAppShareEntityを作成する
	 * @param obj 作成元のExpReportRequest__Shareオブジェクト
	 * @return 作成したAppShareEntity
	 */
	private AppShareEntity createShareEntity(ExpReportRequest__Share obj) {
		AppShareEntity entity = new AppShareEntity();

		entity.setId(obj.Id);
		entity.parentId = obj.ParentId;
		entity.userOrGroupId = obj.UserOrGroupId;

		entity.resetChanged();
		return entity;
	}
}