/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description JobResourceのテストクラス
*/
@isTest
private class JobResourceTest {

	/**
	 * 基本的なマスタデータを登録する
	 */
	@testSetup static void setupTestData() {
		ComCompany__c company = ComTestDataUtility.createCompany('テスト会社', null);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('テスト社員', company.Id, null, UserInfo.getUserId(), permission.Id);
		ComJobType__c jobType = ComTestDataUtility.createJobType('テストジョブタイプ', company.Id);
	}

	/** テストデータ(会社) */
	private static ComCompany__c getCompany() {
		return [SELECT Id, Name, Code__c FROM ComCompany__c LIMIT 1];
	}

	/** テストデータ(部署) */
	private static ComDeptBase__c getDepartment() {
		return [SELECT Id, Name, CompanyId__c FROM ComDeptBase__c LIMIT 1];
	}

	/** テストデータ(社員) */
	private static ComEmpBase__c getEmployee() {
		return [SELECT Id, Name FROM ComEmpBase__c LIMIT 1];
	}

	/** テストデータ(ジョブタイプ) */
	private static ComJobType__c getJobType() {
		return [SELECT Id, Name FROM ComJobType__c LIMIT 1];
	}

	/** テストデータ(権限) */
	private static ComPermission__c getPermission() {
		return [SELECT Id, Name FROM ComPermission__c LIMIT 1];
	}

	/**
	 * ジョブレコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		ComJob__c parent = ComTestDataUtility.createJob('テスト親ジョブ', dept.CompanyId__c, jobType.Id);

		Test.startTest();

		JobResource.Job param = new JobResource.Job();
		param.name_L0 = 'テストジョブ_L0';
		param.name_L1 = 'テストジョブ_L1';
		param.name_L2 = 'テストジョブ_L2';
		param.code = '001';
		param.companyId = dept.CompanyId__c;
		param.departmentId = dept.Id;
		param.jobOwnerId = emp.Id;
		param.parentId = parent.Id;
		param.jobTypeId = jobType.Id;
		param.isDirectCharged = false;
		param.isScopedAssignment = true;
		param.validDateFrom = Date.today().addDays(10);
		param.validDateTo = Date.today().addDays(20);
		param.isSelectableExpense = false;
		param.isSelectableTimeTrack = true;

		JobResource.CreateApi api = new JobResource.CreateApi();
		JobResource.SaveResult res = (JobResource.SaveResult)api.execute(param);

		Test.stopTest();

		// ジョブレコードが1件作成されること
		List<ComJob__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CompanyId__c,
				DepartmentBaseId__c,
				JobOwnerBaseId__c,
				ParentId__c,
				DirectCharged__c,
				ScopedAssignment__c,
				ValidFrom__c,
				ValidTo__c,
				SelectabledExpense__c,
				SelectabledTimeTrack__c
			FROM ComJob__c
			WHERE Id !=:parent.Id];

		System.assertEquals(1, newRecords.size());

		ComJob__c newRecord = newRecords[0];
		System.assertEquals(newRecord.Id, res.Id);
		System.assertEquals(param.name_L0, newRecord.Name);
		System.assertEquals(param.name_L0, newRecord.Name_L0__c);
		System.assertEquals(param.name_L1, newRecord.Name_L1__c);
		System.assertEquals(param.name_L2, newRecord.Name_L2__c);
		System.assertEquals(param.code, newRecord.Code__c);
		System.assertEquals(param.companyId, newRecord.CompanyId__c);
		System.assertEquals(param.departmentId, newRecord.DepartmentBaseId__c);
		System.assertEquals(param.jobOwnerId, newRecord.JobOwnerBaseId__c);
		System.assertEquals(param.parentId, newRecord.ParentId__c);
		System.assertEquals(param.validDateFrom, newRecord.ValidFrom__c);
		System.assertEquals(param.validDateTo, newRecord.ValidTo__c);
		System.assertEquals(param.isScopedAssignment, newRecord.ScopedAssignment__c);
		System.assertEquals(param.isSelectableExpense, newRecord.SelectabledExpense__c);
		System.assertEquals(param.isSelectableTimeTrack, newRecord.SelectabledTimeTrack__c);
	}

	/**
	 * ジョブレコードを1件作成する(正常系:デフォルト値が設定される)
	 */
	@isTest
	static void createPositiveTestDefaultParam() {

		ComDeptBase__c dept = getDepartment();
		ComJobType__c jobType = getJobType();
		ComJob__c parent = ComTestDataUtility.createJob('テスト親ジョブ', dept.CompanyId__c, jobType.Id);

		Test.startTest();

		JobResource.Job param = new JobResource.Job();
		param.name_L0 = 'テストジョブ_L0';
		param.code = '001';
		param.companyId = dept.CompanyId__c;
		param.jobTypeId = jobType.Id;

		JobResource.CreateApi api = new JobResource.CreateApi();
		JobResource.SaveResult res = (JobResource.SaveResult)api.execute(param);

		Test.stopTest();

		// ジョブレコードが1件作成されること
		List<ComJob__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				Code__c,
				CompanyId__c,
				JobTypeId__c,
				DirectCharged__c,
				ScopedAssignment__c,
				ValidFrom__c,
				ValidTo__c,
				SelectabledExpense__c,
				SelectabledTimeTrack__c
			FROM ComJob__c
			WHERE Id !=:parent.Id];

		System.assertEquals(1, newRecords.size());

		ComJob__c newRecord = newRecords[0];
		System.assertEquals(newRecord.Id, res.Id);
		System.assertEquals(param.code, newRecord.Code__c);
		System.assertEquals(param.companyId, newRecord.CompanyId__c);
		System.assertEquals(param.jobTypeId, newRecord.JobTypeId__c);
		System.assertEquals(true, newRecord.DirectCharged__c);
		System.assertEquals(false, newRecord.ScopedAssignment__c);
		System.assertEquals(Date.today(), newRecord.ValidFrom__c);
		System.assertEquals(Date.newInstance(2101, 1, 1), newRecord.ValidTo__c);
		System.assertEquals(true, newRecord.SelectabledExpense__c);
		System.assertEquals(true, newRecord.SelectabledTimeTrack__c);
	}

	/**
	 * ジョブレコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeNegativeTest() {
		ComCompany__c company = getCompany();

		Test.startTest();

		JobResource.Job param = new JobResource.Job();
		param.code = 'test01';
		param.companyId = company.Id;
		JobResource.CreateApi api = new JobResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertNotEquals(null, ex, '例外が発生しませんでした');
		System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, ex.getErrorCode());
	}

	/**
	 * ジョブレコードを1件作成する(異常系:Jobの管理権限なし)
	 */
	@isTest
	static void creativeNegativeTestNoPermission() {

		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		ComJob__c parent = ComTestDataUtility.createJob('テスト親ジョブ', dept.CompanyId__c, jobType.Id);

		// 権限を無効に設定する
		ComPermission__c permission = getPermission();
		permission.ManageJob__c = false;
		update permission;
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		User stdUser = ComTestDataUtility.createStandardUser('テスト標準ユーザ');
		ComEmpBase__c stdEmp = ComTestDataUtility.createEmployeeWithHistory(
					'標準テストユーザー', dept.CompanyId__c, dept.id, stdUser.id, permission.id);

		// リクエストパラメータ
		JobResource.Job param = new JobResource.Job();
		param.name_L0 = 'テストジョブ_L0';
		param.name_L1 = 'テストジョブ_L1';
		param.name_L2 = 'テストジョブ_L2';
		param.code = '001';
		param.companyId = dept.CompanyId__c;
		param.departmentId = dept.Id;
		param.jobOwnerId = emp.Id;
		param.parentId = parent.Id;
		param.jobTypeId = jobType.Id;
		param.isDirectCharged = false;
		param.isScopedAssignment = true;
		param.validDateFrom = Date.today().addDays(10);
		param.validDateTo = Date.today().addDays(20);

		// API実行
		App.NoPermissionException actEx;
		System.runAs(stdUser) {
			try {
				JobResource.CreateApi api = new JobResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * ジョブレコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {
		ComCompany__c company = getCompany();
		ComJobType__c jobType = getJobType();
		ComJob__c job = ComTestDataUtility.createJob('テストジョブ', company.Id, jobType.Id);

		Test.startTest();

		// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
		// 全パラメータの検証は createPositiveTest() で行っている
		Map<String, Object> param = new Map<String, Object> {
			'id' => job.id,
			'code' => '002',
			'name_L0' => 'Test'
		};

		JobResource.UpdateApi api = new JobResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// 部署レコードが更新されていること
		List<ComJob__c> jobList = [SELECT Code__c FROM ComJob__c WHERE Id =:(String)param.get('id')];
		System.assertEquals(1, jobList.size());
		System.assertEquals((String)param.get('code'), jobList[0].Code__c);

	}

	/**
	 * ジョブレコードを1件更新する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		Test.startTest();

		JobResource.Job param = new JobResource.Job();

		App.ParameterException ex;
		try {
			Object res = (new JobResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * ジョブレコードを1件更新する(異常系:ジョブの管理権限なし)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {

		ComDeptBase__c dept = getDepartment();
		ComCompany__c company = getCompany();
		ComJobType__c jobType = getJobType();
		ComJob__c job = ComTestDataUtility.createJob('テストジョブ', company.Id, jobType.Id);

		// 権限を無効に設定する
		ComPermission__c permission = getPermission();
		permission.ManageJob__c = false;
		update permission;
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		User stdUser = ComTestDataUtility.createStandardUser('テスト標準ユーザ');
		ComEmpBase__c stdEmp = ComTestDataUtility.createEmployeeWithHistory(
					'標準テストユーザー', dept.CompanyId__c, dept.id, stdUser.id, permission.id);

		// リクエストパラメータ
		Map<String, Object> param = new Map<String, Object> {
			'id' => job.id,
			'code' => '002',
			'name_L0' => 'Test'
		};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(stdUser) {
			try {
				JobResource.UpdateApi api = new JobResource.UpdateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * ジョブレコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		ComCompany__c company = getCompany();
		ComJobType__c jobType = getJobType();
		ComJob__c testRecord = ComTestDataUtility.createJob('テストジョブ', company.Id, jobType.Id);

		Test.startTest();

		JobResource.DeleteOption param = new JobResource.DeleteOption();
		param.id = testRecord.id;

		Object res = (new JobResource.DeleteApi()).execute(param);

		Test.stopTest();

		// ジョブレコードが削除されること
		List<ComJob__c> JobList = [SELECT Id FROM ComJob__c];
		System.assertEquals(0, JobList.size());
	}

	/**
	 * ジョブレコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		JobResource.DeleteOption param = new JobResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new JobResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * ジョブレコードを1件削除する(異常系:DMLException発生)
	 * TODO: 標準ユーザからジョブレコードが削除できてしまいテストが失敗するため(原因不明)、保留
	 */
	@isTest
	static void deleteNegativeTest() {

		ComDeptBase__c dept = getDepartment();
		ComPermission__c permission = getPermission();
		User stdUser = ComTestDataUtility.createStandardUser('テスト標準ユーザ');
		ComEmpBase__c stdEmp = ComTestDataUtility.createEmployeeWithHistory(
					'標準テストユーザー', dept.CompanyId__c, dept.id, stdUser.id, permission.id);
		ComCompany__c company = getCompany();
		ComJobType__c jobType = getJobType();
		ComJob__c delRecord = ComTestDataUtility.createJob('テストジョブ', company.Id, jobType.Id);

		Test.startTest();

		JobResource.DeleteOption param = new JobResource.DeleteOption();
		param.Id = delRecord.Id;

		DmlException ex;

		System.runAs(stdUser){
			try {
				Object res = (new JobResource.DeleteApi()).execute(param);
			} catch (DmlException e) {
				ex = e;
			}
		}

		Test.stopTest();

		System.assertEquals('INSUFFICIENT_ACCESS_OR_READONLY', ex.getDmlStatusCode(0));
	}

	/**
	 * ジョブレコードを1件削除する(異常系:ジョブの管理権限なし)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {

		ComDeptBase__c dept = getDepartment();
		ComCompany__c company = getCompany();
		ComJobType__c jobType = getJobType();
		ComJob__c testRecord = ComTestDataUtility.createJob('テストジョブ', company.Id, jobType.Id);

		// 権限を無効に設定する
		ComPermission__c permission = getPermission();
		permission.ManageJob__c = false;
		update permission;
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		User stdUser = ComTestDataUtility.createStandardUser('テスト標準ユーザ');
		ComEmpBase__c stdEmp = ComTestDataUtility.createEmployeeWithHistory(
					'標準テストユーザー', dept.CompanyId__c, dept.id, stdUser.id, permission.id);

		// リクエストパラメータ
		JobResource.DeleteOption param = new JobResource.DeleteOption();
		param.id = testRecord.id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(stdUser) {
			try {
				JobResource.DeleteApi api = new JobResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * ジョブレコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {

		ComCompany__c company = getCompany();
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		List<ComJob__c> testJobs = ComTestDataUtility.createJobs('テストジョブ', company.Id, jobType.Id, 3);

		ComJob__c testJob = testJobs[0];
		testJob.ParentId__c = testJobs[1].Id;
		testJob.CompanyId__c = dept.CompanyId__c;
		testJob.DepartmentBaseId__c = dept.Id;
		testJob.JobOwnerBaseId__c = emp.Id;
		update testJob;

		Test.startTest();

		JobResource.SearchCondition param = new JobResource.SearchCondition();
		param.id = testJob.id;
		param.companyId = testJob.CompanyId__c;
		param.departmentId = testJob.DepartmentBaseId__c;
		param.parentId = testJob.ParentId__c;

		JobResource.SearchApi api = new JobResource.SearchApi();
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(param);

		Test.stopTest();

		// ジョブレコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(testJob.Id, res.records[0].id);
		// 項目値の検証はsearchAllPositiveTest()で行うため省略

	}

	/**
	 * ジョブレコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(
				ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = getCompany();
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		List<ComJob__c> testJobs = ComTestDataUtility.createJobs('テストジョブ', company.Id, jobType.Id, 2);

		ComJob__c testJob = testJobs[0];
		testJob.ParentId__c = testJobs[1].Id;
		testJob.CompanyId__c = dept.CompanyId__c;
		testJob.DepartmentBaseId__c = dept.Id;
		testJob.JobOwnerBaseId__c = emp.Id;
		update testJob;

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => testJob.Id
		};
		JobResource.SearchApi api = new JobResource.SearchApi();
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// ジョブレコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(testJob.Id, res.records[0].id);

		// 多言語対応項目の検証
		testJob = [SELECT
				Name_L1__c,
				ParentId__r.Name_L1__c,
				DepartmentBaseId__r.CurrentHistoryId__r.Name_L1__c,
				JobOwnerBaseId__c,
				JobOwnerBaseId__r.FirstName_L0__c,
				JobOwnerBaseId__r.LastName_L0__c,
				JobOwnerBaseId__r.FirstName_L1__c,
				JobOwnerBaseId__r.LastName_L1__c,
				JobOwnerBaseId__r.FirstName_L2__c,
				JobOwnerBaseId__r.LastName_L2__c
			FROM ComJob__c
			WHERE Id =: testJob.Id];

		System.assertEquals(testJob.Name_L1__c, res.records[0].name);
		System.assertEquals(testJob.ParentId__r.Name_L1__c, res.records[0].parent.name);
		System.assertEquals(testJob.DepartmentBaseId__r.CurrentHistoryId__r.Name_L1__c, res.records[0].department.name);
		if (res.records[0].jobOwnerId != null) {
			String jobOwnerName = new AppPersonName(
					testJob.JobOwnerBaseId__r.FirstName_L0__c, testJob.JobOwnerBaseId__r.LastName_L0__c,
					testJob.JobOwnerBaseId__r.FirstName_L1__c, testJob.JobOwnerBaseId__r.LastName_L1__c,
					testJob.JobOwnerBaseId__r.FirstName_L2__c, testJob.JobOwnerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(jobOwnerName, res.records[0].jobOwner.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].jobOwner.name));
		}
	}

	/**
	 * ジョブレコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(
				ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = getCompany();
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		List<ComJob__c> testJobs = ComTestDataUtility.createJobs('テストジョブ', company.Id, jobType.Id, 2);

		ComJob__c testJob = testJobs[0];
		testJob.ParentId__c = testJobs[1].Id;
		testJob.CompanyId__c = dept.CompanyId__c;
		testJob.DepartmentBaseId__c = dept.Id;
		testJob.JobOwnerBaseId__c = emp.Id;
		update testJob;

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => testJob.Id
		};
		JobResource.SearchApi api = new JobResource.SearchApi();
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// ジョブレコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(testJob.Id, res.records[0].id);

		// 多言語対応項目の検証
		testJob = [SELECT
				Name_L2__c,
				ParentId__r.Name_L2__c,
				DepartmentBaseId__r.CurrentHistoryId__r.Name_L2__c,
				JobOwnerBaseId__r.FirstName_L0__c,
				JobOwnerBaseId__r.LastName_L0__c,
				JobOwnerBaseId__r.FirstName_L1__c,
				JobOwnerBaseId__r.LastName_L1__c,
				JobOwnerBaseId__r.FirstName_L2__c,
				JobOwnerBaseId__r.LastName_L2__c
			FROM ComJob__c
			WHERE Id =: testJob.Id];

		System.assertEquals(testJob.Name_L2__c, res.records[0].name);
		System.assertEquals(testJob.ParentId__r.Name_L2__c, res.records[0].parent.name);
		System.assertEquals(testJob.DepartmentBaseId__r.CurrentHistoryId__r.Name_L2__c, res.records[0].department.name);
		if (res.records[0].jobOwnerId != null) {
			String jobOwnerName = new AppPersonName(
					testJob.JobOwnerBaseId__r.FirstName_L0__c, testJob.JobOwnerBaseId__r.LastName_L0__c,
					testJob.JobOwnerBaseId__r.FirstName_L1__c, testJob.JobOwnerBaseId__r.LastName_L1__c,
					testJob.JobOwnerBaseId__r.FirstName_L2__c, testJob.JobOwnerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(jobOwnerName, res.records[0].jobOwner.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].jobOwner.name));
		}
	}

	/**
	 * ジョブレコードを全件検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComCompany__c company = getCompany();
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		List<ComJob__c> testJobs = ComTestDataUtility.createJobs('テストジョブ', company.Id, jobType.Id, 5);

		ComJob__c testJob = testJobs[0];
		testJob.ParentId__c = testJobs[1].Id;
		testJob.CompanyId__c = dept.CompanyId__c;
		testJob.DepartmentBaseId__c = dept.Id;
		testJob.JobOwnerBaseId__c = emp.Id;
		update testJob;

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>();
		JobResource.SearchApi api = new JobResource.SearchApi();
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// ジョブレコードが取得できること
		System.assertEquals(testJobs.size(), res.records.size());

		// 作成したテストデータをコード順で再取得する
		List<ComJob__c> joblist = [SELECT
				Id,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CompanyId__c,
				DepartmentBaseId__c,
				DepartmentBaseId__r.CurrentHistoryId__r.Name_L0__c,
				ParentId__c,
				ParentId__r.Name_L0__c,
				JobTypeId__c,
				JobTypeId__r.Name_L0__c,
				JobTypeId__r.Name_L1__c,
				JobTypeId__r.Name_L2__c,
				JobOwnerBaseId__c,
				JobOwnerBaseId__r.FirstName_L0__c,
				JobOwnerBaseId__r.FirstName_L1__c,
				JobOwnerBaseId__r.FirstName_L2__c,
				JobOwnerBaseId__r.LastName_L0__c,
				JobOwnerBaseId__r.LastName_L1__c,
				JobOwnerBaseId__r.LastName_L2__c,
				ValidFrom__c,
				ValidTo__c,
				DirectCharged__c,
				ScopedAssignment__c,
				SelectabledExpense__c,
				SelectabledTimeTrack__c
			FROM ComJob__c
			ORDER BY Code__c NULLS LAST];

		for (Integer i = 0, n = testJobs.size(); i < n; i++) {
			JobResource.Job resJob = res.records[i];
			ComJob__c job = joblist[i];

			System.assertEquals(job.Id, resJob.id);
			System.assertEquals(job.Name_L0__c, resJob.name);	//デフォルトではL0の値を返す
			System.assertEquals(job.Name_L0__c, resJob.name_L0);
			System.assertEquals(job.Name_L1__c, resJob.name_L1);
			System.assertEquals(job.Name_L2__c, resJob.name_L2);
			System.assertEquals(job.Code__c, resJob.code);
			System.assertEquals(job.CompanyId__c, resJob.companyId);
			System.assertEquals(job.DepartmentBaseId__c, resJob.departmentId);
			System.assertEquals(job.DepartmentBaseId__r.CurrentHistoryId__r.Name_L0__c, resJob.department.name);	//デフォルトではL0の値を返す
			System.assertEquals(job.ParentId__c, resJob.parentId);
			System.assertEquals(job.ParentId__r.Name_L0__c, resJob.parent.name);	//デフォルトではL0の値を返す
			System.assertEquals(job.JobTypeId__c, resJob.jobTypeId);
			System.assertEquals(job.JobTypeId__r.Name_L0__c, resJob.jobType.name);	//デフォルトではL0の値を返す
			System.assertEquals(job.JobOwnerBaseId__c, resJob.jobOwnerId);
			if (resJob.jobOwnerId != null) {
				String fullName = new AppPersonName(
						job.JobOwnerBaseId__r.FirstName_L0__c, job.JobOwnerBaseId__r.LastName_L0__c,
						job.JobOwnerBaseId__r.FirstName_L1__c, job.JobOwnerBaseId__r.LastName_L1__c,
						job.JobOwnerBaseId__r.FirstName_L2__c, job.JobOwnerBaseId__r.LastName_L2__c).getFullName();
				System.assertEquals(fullName, resJob.jobOwner.name);
			} else {
				System.assertEquals(true, String.isBlank(resJob.jobOwner.name));
			}
			System.assertEquals(job.ValidFrom__c, resJob.validDateFrom);
			System.assertEquals(job.ValidTo__c, resJob.validDateTo);
			System.assertEquals(job.DirectCharged__c, resJob.isDirectCharged);
			System.assertEquals(job.ScopedAssignment__c, resJob.isScopedAssignment);
			System.assertEquals(job.SelectabledExpense__c, resJob.isSelectableExpense);
			System.assertEquals(job.SelectabledTimeTrack__c, resJob.isSelectableTimeTrack);
		}
	}

	/**
	 * Search Job With Query
	 * Search by Name and Search by Code
	 */
	@isTest
	static void searchJobWithQuery() {
		ComCompany__c company = getCompany();
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		Integer jobSize = 2;
		List<ComJob__c> testJobs = ComTestDataUtility.createJobs('Test Job', company.Id, jobType.Id, jobSize);
		testJobs[0].Code__c = 'ABC1';
		testJobs[1].Code__c = '*XYZ' ;
		update testJobs;

		JobResource.SearchCondition param = new JobResource.SearchCondition();
		JobResource.SearchApi api = new JobResource.SearchApi();

		// Search by Name
		param.query = 'Test';

		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(param);

		System.assertEquals(jobSize, res.records.size());
		List<ComJob__c> jobList = getJobList();
		for (Integer i = 0; i < jobSize; i++) {
			JobResource.Job resJob = res.records[i];
			ComJob__c job = joblist[i];

			assertJobObj(job, resJob);
		}

		// Search by code
		param.query = '*X';

		res = (JobResource.SearchResult)api.execute(param);

		System.assertEquals(1, res.records.size());
		assertJobObj(jobList[1], res.records[0]);
	}
	/**
	 * Search Job with Query and parents hierarchy name test
	 */
	@isTest
	static void searchAllPositiveTestWithHierarchy() {

		ComCompany__c company = getCompany();
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		List<ComJob__c> testJobs = ComTestDataUtility.createJobs('テストジョブ', company.Id, jobType.Id, 8);
		// Make hierarchy job0 -> job1 -> job2 -> job3 -> job5
		testJobs[0].ParentId__c = testJobs[1].Id;
		testJobs[1].ParentId__c = testJobs[2].Id;
		testJobs[2].ParentId__c = testJobs[3].Id;
		testJobs[3].ParentId__c = testJobs[5].Id;
		// Make hierarchy job7 -> job6
		ComJob__c testJob = testJobs[0];
		testJob.CompanyId__c = dept.CompanyId__c;
		testJob.DepartmentBaseId__c = dept.Id;
		testJob.JobOwnerBaseId__c = emp.Id;
		update testJobs;

		Test.startTest();

		JobResource.SearchApi api = new JobResource.SearchApi();
		JobResource.SearchCondition param = new JobResource.SearchCondition();
		param.query = testJob.Name;
		param.usedIn = 'REPORT';
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Found the Job
		System.assertEquals(1, res.records.size());

		// Get the job list from DB to compare
		List<ComJob__c> joblist = getJobList();

		JobResource.Job resJob = res.records[0];
		ComJob__c job = joblist[0];

		assertJobObj(job, resJob);

		System.assertEquals(4, resJob.hierarchyParentNameList.size());
		System.assertEquals(testJobs[1].Name_L0__c, resJob.hierarchyParentNameList[0]);
		System.assertEquals(testJobs[2].Name_L0__c, resJob.hierarchyParentNameList[1]);
		System.assertEquals(testJobs[3].Name_L0__c, resJob.hierarchyParentNameList[2]);
		System.assertEquals(testJobs[5].Name_L0__c, resJob.hierarchyParentNameList[3]);
	}

	/**
	 * Search Job With employee Id and targetDate
	 */
	@isTest
	static void searchWithEmployeeIdPositiveTest() {
		Integer jobSize = 4;
		ComCompany__c company = getCompany();
		ComDeptBase__c dept = getDepartment();
		ComEmpBase__c emp = getEmployee();
		ComJobType__c jobType = getJobType();
		List<ComJob__c> testJobs = ComTestDataUtility.createJobs('テストジョブ', company.Id, jobType.Id, jobSize);

		User u = ComTestDataUtility.createStandardUser('standarduser' + DateTime.now().getTime() + '@testorg.com');
		ComEmpBase__c empMe = ComTestDataUtility.createEmployeeWithHistory('TestingMe', company.id, dept.id, u.Id);
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(testJobs[0].Id, empMe.Id, AppDate.today(), AppDate.today().addMonths(1));
		testJobs[0].ScopedAssignment__c = true;
		update testJobs;

		//Employee With 4 Jobs
		JobResource.SearchApi api = new JobResource.SearchApi();
		JobResource.SearchCondition param = new JobResource.SearchCondition();
		param.empId = empMe.Id;
		param.usedIn = 'REPORT';
		param.targetDate = AppDate.today().toString();
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(param);


		// Employee has 4 Jobs
		System.assertEquals(jobSize, res.records.size());

		// Get the job list from DB to compare
		List<ComJob__c> joblist = getJobList();

		for (Integer i = 0; i < jobSize; i++) {
			JobResource.Job resJob = res.records[i];
			ComJob__c job = joblist[i];

			assertJobObj(job, resJob);
		}

		// Employee With 3 Jobs
		param.empId = emp.Id;
		param.usedIn = 'REPORT';
		param.targetDate = AppDate.today().toString();
		res = (JobResource.SearchResult)api.execute(param);

		// Employee has 3 Jobs
		System.assertEquals(3, res.records.size());

		for (Integer i = 1; i < jobSize; i++) {
			JobResource.Job resJob = res.records[i-1];
			ComJob__c job = joblist[i];

			assertJobObj(job, resJob);
		}

	}


	/**
	 * Job Recently Used Api Test
	 */
	@IsTest static void getRecentlyUpdatedJobTest() {
		// Create Jobs
		ComCompany__c company = getCompany();
		ComEmpBase__c employee = getEmployee();
		ComJobType__c jobType = getJobType();
		Integer numberOfJobRecords = 12;

		List<ComJob__c> jobList = ComTestDataUtility.createJobs('JJ', company.Id, jobType.Id, numberOfJobRecords);
		List<Id> jobIdList = new List<Id>();
		for (ComJob__c job : jobList) {
			jobIdList.add(job.Id);
		}


		ExpRecentlyUsedService expRecUsedService = new ExpRecentlyUsedService();
		// Add to recently used Job
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, jobList[3].Id, jobList[0].Id);

		// Retrieve CostCenters Check everything is correct
		JobResource.GetRecentlyUsedListParam param = new JobResource.GetRecentlyUsedListParam();
		param.employeeBaseId = employee.id;
		param.targetDate = AppDate.today().toString();

		JobResource.GetRecentlyUsedListApi api = new JobResource.GetRecentlyUsedListApi();
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(param);

		List<ComJob__c> insertedJobList = getJobList();

		// Retrieve 1
		System.assertEquals(1, res.records.size());
		JobResource.Job resultJob = res.records[0];
		assertJobObj(insertedJobList[0], resultJob);

	}

	/**
	 * Job Recently Used Api Test With max number
	 */
	@IsTest static void getRecentlyUpdatedJobMaxTest() {
		// Create Jobs
		ComCompany__c company = getCompany();
		ComEmpBase__c employee = getEmployee();
		ComJobType__c jobType = getJobType();
		Integer numberOfJobRecords = 12;

		List<ComJob__c> jobList = ComTestDataUtility.createJobs('JJ', company.Id, jobType.Id, numberOfJobRecords);

		AppDate targetDate = AppDate.today();

		test.startTest();

		ExpRecentlyUsedService expRecUsedService = new ExpRecentlyUsedService();
		// Add to recently used Job
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, jobList[3].id, jobList[0].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[1].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[2].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[3].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[4].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[5].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[6].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[7].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[8].id);
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[9].id);

		Integer maxLimit = 10;
		// Retrieve Jobs Check everything is correct
		JobResource.GetRecentlyUsedListParam param = new JobResource.GetRecentlyUsedListParam();
		param.employeeBaseId = employee.id;

		JobResource.GetRecentlyUsedListApi api = new JobResource.GetRecentlyUsedListApi();
		JobResource.SearchResult res = (JobResource.SearchResult)api.execute(param);

		System.assertEquals(10, res.records.size());

		List<ComJob__c> insertedJobList = getJobList();

		for (Integer i = 0; i < maxLimit; i++) {
			ComJob__c job = insertedJobList[maxLimit-i-1];

			// Retrieve Job
			JobResource.Job resultJob = res.records[i];
			assertJobObj(job, resultJob);
		}

		// Add New one and test it only contain latest 10
		expRecUsedService.saveRecentlyUsedJobValues(employee.id, null, jobList[11].id);
		res = (JobResource.SearchResult)api.execute(param);

		System.assertEquals(10, res.records.size());

		assertJobObj(insertedJobList[11], res.records[0]);
		assertJobObj(insertedJobList[9], res.records[1]);
		assertJobObj(insertedJobList[8], res.records[2]);
		assertJobObj(insertedJobList[7], res.records[3]);
		assertJobObj(insertedJobList[6], res.records[4]);
		assertJobObj(insertedJobList[5], res.records[5]);
		assertJobObj(insertedJobList[4], res.records[6]);
		assertJobObj(insertedJobList[3], res.records[7]);
		assertJobObj(insertedJobList[2], res.records[8]);
		assertJobObj(insertedJobList[1], res.records[9]);

		test.stopTest();
	}
	/*
	 * Assert Searched Job Result based on job
	 * @param job Job SObject
	 * @param resJob JobResource Job Search Result
	 */
	private static void assertJobObj(ComJob__c job, JobResource.Job resJob) {
		System.assertEquals(job.Code__c, resJob.code);
		System.assertEquals(job.Id, resJob.id);
		System.assertEquals(job.Name_L0__c, resJob.name);	//デフォルトではL0の値を返す
		System.assertEquals(job.Name_L0__c, resJob.name_L0);
		System.assertEquals(job.Name_L1__c, resJob.name_L1);
		System.assertEquals(job.Name_L2__c, resJob.name_L2);
		System.assertEquals(job.CompanyId__c, resJob.companyId);
		System.assertEquals(job.DepartmentBaseId__c, resJob.departmentId);
		System.assertEquals(job.DepartmentBaseId__r.CurrentHistoryId__r.Name_L0__c, resJob.department.name);	//デフォルトではL0の値を返す
		System.assertEquals(job.ParentId__c, resJob.parentId);
		System.assertEquals(job.ParentId__r.Name_L0__c, resJob.parent.name);	//デフォルトではL0の値を返す
		System.assertEquals(job.JobTypeId__c, resJob.jobTypeId);
		/*System.assertEquals(job.JobTypeId__r.Name_L0__c, resJob.jobType.name);	//デフォルトではL0の値を返す*/
		System.assertEquals(job.JobOwnerBaseId__c, resJob.jobOwnerId);
		if (resJob.jobOwnerId != null) {
			String fullName = new AppPersonName(
					job.JobOwnerBaseId__r.FirstName_L0__c, job.JobOwnerBaseId__r.LastName_L0__c,
					job.JobOwnerBaseId__r.FirstName_L1__c, job.JobOwnerBaseId__r.LastName_L1__c,
					job.JobOwnerBaseId__r.FirstName_L2__c, job.JobOwnerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(fullName, resJob.jobOwner.name);
		} else {
			System.assertEquals(true, String.isBlank(resJob.jobOwner.name));
		}
		System.assertEquals(job.ValidFrom__c, resJob.validDateFrom);
		System.assertEquals(job.ValidTo__c, resJob.validDateTo);
		System.assertEquals(job.DirectCharged__c, resJob.isDirectCharged);
		System.assertEquals(job.ScopedAssignment__c, resJob.isScopedAssignment);
		System.assertEquals(job.SelectabledExpense__c, resJob.isSelectableExpense);
		System.assertEquals(job.SelectabledTimeTrack__c, resJob.isSelectableTimeTrack);
	}

	private static List<ComJob__c> getJobList() {
		return [SELECT
		Id,
		Name_L0__c,
		Name_L1__c,
		Name_L2__c,
		Code__c,
		CompanyId__c,
		DepartmentBaseId__c,
		DepartmentBaseId__r.CurrentHistoryId__r.Name_L0__c,
		ParentId__c,
		ParentId__r.Name_L0__c,
		JobTypeId__c,
		JobTypeId__r.Name_L0__c,
		JobTypeId__r.Name_L1__c,
		JobTypeId__r.Name_L2__c,
		JobOwnerBaseId__c,
		JobOwnerBaseId__r.FirstName_L0__c,
		JobOwnerBaseId__r.FirstName_L1__c,
		JobOwnerBaseId__r.FirstName_L2__c,
		JobOwnerBaseId__r.LastName_L0__c,
		JobOwnerBaseId__r.LastName_L1__c,
		JobOwnerBaseId__r.LastName_L2__c,
		ValidFrom__c,
		ValidTo__c,
		DirectCharged__c,
		ScopedAssignment__c,
		SelectabledExpense__c,
		SelectabledTimeTrack__c
		FROM ComJob__c
		ORDER BY Code__c NULLS LAST];
	}
}