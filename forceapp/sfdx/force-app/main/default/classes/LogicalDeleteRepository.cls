/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 履歴管理方式が論理削除型のマスタのエンティティ基底クラス
 */
public with sharing abstract class LogicalDeleteRepository extends Repository.BaseRepository {

	// NOTE:論理削除型のマスタは、レコードが参照されている場合は論理削除する方針だが、検討が十分ではないため現段階では実装しない。
}