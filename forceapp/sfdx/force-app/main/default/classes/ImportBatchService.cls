/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description マスタインポートバッチ処理用サービス
 */
public with sharing class ImportBatchService {

	/** インポート件数の上限 */
	public static final Integer MAX_COUNT = 9999;

	/** バッチサイズ（新規コストセンター登録） */
	public static final Integer BATCH_SIZE_COST_CENTER_INSERT = 40;
	/** バッチサイズ（既存コストセンター更新） */
	public static final Integer BATCH_SIZE_COST_CENTER_UPDATE = 30;
	/** バッチサイズ（新規部署登録） */
	public static final Integer BATCH_SIZE_DEPT_INSERT = 40;
	/** バッチサイズ（既存部署更新） */
	public static final Integer BATCH_SIZE_DEPT_UPDATE = 30;
	/** バッチサイズ（新規社員登録） */
	public static final Integer BATCH_SIZE_EMP_INSERT = 40;
	/** バッチサイズ（既存社員更新） */
	public static final Integer BATCH_SIZE_EMP_UPDATE = 30;

	/** 処理件数 */
	public Integer processCount {get; private set;}

	/** 種別 */
	private ImportBatchType type;
	/** インポートバッチID */
	private Id importBatchId;

	/** インポートバッチリポジトリ */
	private ImportBatchRepository batchRepo = new ImportBatchRepository();
	/** コストセンターリポジトリ */
	private CostCenterImportRepository costCenterImpRepo = new CostCenterImportRepository();
	/** 部署リポジトリ */
	private DepartmentImportRepository deptImpRepo = new DepartmentImportRepository();
	/** 社員リポジトリ */
	private EmployeeImportRepository empImpRepo = new EmployeeImportRepository();

	/** コストセンターインポートエンティティのリスト */
	private List<CostCenterImportEntity> costCenterImpEntityList;
	/** 部署インポートエンティティのリスト */
	private List<DepartmentImportEntity> deptImpEntityList;
	/** 社員インポートエンティティのリスト */
	private List<EmployeeImportEntity> empImpEntityList;


	/**
	 * コンストラクタ
	 * @param type 種別
	 * @param importBatchId インポートバッチID
	 */
	public ImportBatchService(ImportBatchType type, Id importBatchId) {
		this.type = type;
		this.importBatchId = importBatchId;
		this.processCount = 0;
		this.init();
	}

	/**
	 * インポートバッチデータを作成する
	 * ・インポートバッチデータを作成または更新する
	 * ・新規バッチ処理の場合は対象のインポートデータにバッチIDを設定する
	 * @return インポートバッチID
	 */
	public Id createImportBatchData() {

		// インポートバッチエンティティを作成
		ImportBatchEntity batchEntity = this.createImportBatchEntity();

		if (String.isBlank(this.importBatchId)) {
			// 新規バッチの場合はインポートバッチデータを登録
			Repository.SaveResult result = this.batchRepo.saveEntity(batchEntity);
			this.importBatchId = result.details[0].id;

			if (ImportBatchType.COST_CENTER.equals(this.type)) {
				// インポートデータにバッチIDをセット
				// コストセンター
				if (!this.costCenterImpEntityList.isEmpty()) {
					for (CostCenterImportEntity costCenterImp : this.costCenterImpEntityList) {
						costCenterImp.importBatchId = this.importBatchId;
					}
					this.costCenterImpRepo.saveEntityList(this.costCenterImpEntityList, true);
				}
			} else if (ImportBatchType.EMPLOYEE_DEPT.equals(this.type)) {
				// インポートデータにバッチIDをセット
				// 部署
				if (!this.deptImpEntityList.isEmpty()) {
					for (DepartmentImportEntity deptImp : this.deptImpEntityList) {
						deptImp.importBatchId = this.importBatchId;
					}
					this.deptImpRepo.saveEntityList(this.deptImpEntityList, true);
				}
				// 社員
				if (!this.empImpEntityList.isEmpty()) {
					for (EmployeeImportEntity empImp : this.empImpEntityList) {
						empImp.importBatchId = this.importBatchId;
					}
					this.empImpRepo.saveEntityList(this.empImpEntityList, true);
				}
			} else if (ImportBatchType.JOB.equals(this.type)) {
			}

		} else {
			// 既存バッチの再実行の場合はインポートバッチデータを更新
			ImportBatchEntity prevBatch = this.batchRepo.getEntity(this.importBatchId);
			if (prevBatch == null) {
				// メッセージ：指定されたインポートバッチIDが見つかりません。
				throw new App.RecordNotFoundException(
						ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_ImportBatchId}));
			}

			batchEntity.setId(prevBatch.id);
			this.batchRepo.saveEntity(batchEntity);
		}

		return this.importBatchId;
	}

	/**
	 * バッチ処理を起動する
	 */
	public void executeBatch() {
		// コストセンター
		if (ImportBatchType.COST_CENTER.equals(this.type)) {
			// 新規コストセンター登録
			CostCenterInsertBatch costCenterInsert = new CostCenterInsertBatch(this.importBatchId);
			Database.executeBatch(costCenterInsert, BATCH_SIZE_COST_CENTER_INSERT);
		// 社員・部署
		} else if (ImportBatchType.EMPLOYEE_DEPT.equals(this.type)) {
			// 新規部署登録
			DepartmentInsertBatch deptInsert = new DepartmentInsertBatch(this.importBatchId);
			Database.executeBatch(deptInsert, BATCH_SIZE_DEPT_INSERT);
		// ジョブ
		} else if (ImportBatchType.JOB.equals(this.type)) {
		}
	}

	//--------------------------------------------------------------------------------

	/**
	 * 初期処理
	 */
	private void init() {
		List<String> status = new List<String>{ImportStatus.WAITING.value, ImportStatus.ERROR.value};

		// コストセンター
		if (ImportBatchType.COST_CENTER.equals(this.type)) {
			this.costCenterImpEntityList = this.costCenterImpRepo.getEntityListByImportBatchId(this.importBatchId, status, MAX_COUNT+1);
			this.processCount = this.costCenterImpEntityList == null ? 0 : this.costCenterImpEntityList.size();
		// 社員・部署
		} else if (ImportBatchType.EMPLOYEE_DEPT.equals(this.type)) {
			// 部署
			this.deptImpEntityList = this.deptImpRepo.getEntityListByImportBatchId(this.importBatchId, status, MAX_COUNT+1);
			Integer deptImpCnt = this.deptImpEntityList == null ? 0 : this.deptImpEntityList.size();
			// 社員
			this.empImpEntityList = this.empImpRepo.getEntityListByImportBatchId(this.importBatchId, status, MAX_COUNT+1);
			Integer empImpCnt = this.empImpEntityList == null ? 0 : this.empImpEntityList.size();

			this.processCount = deptImpCnt + empImpCnt;
		// ジョブ
		} else if (ImportBatchType.JOB.equals(this.type)) {
		}
	}

	/**
	 * インポートバッチエンティティを作成する
	 * @return インポートバッチエンティティ
	 */
	private ImportBatchEntity createImportBatchEntity() {
		ImportBatchEntity entity = new ImportBatchEntity();

		entity.name = 'Batch Import ' + this.type.value + ' ' + Datetime.valueOfGmt(String.valueOf(system.now()));
		entity.targetDate = AppDate.today();
		entity.status = ImportBatchStatus.PROCESSING;
		entity.type = this.type;
		entity.count = this.processCount;
		entity.successCount = 0;
		entity.failureCount = 0;

		return entity;
	}

	/**
	 * 値を変換する
	 * ・先頭の「.」をクリアする
	 * @param value 対象文字列
	 * @return 変換後の文字列
	 */
	public static String convertValue(String value) {
		if (String.isBlank(value)) {
			return null;
		}

		value = value.removeStart('.');

		if (String.isBlank(value)) {
			return null;
		}

		return value;
	}
}