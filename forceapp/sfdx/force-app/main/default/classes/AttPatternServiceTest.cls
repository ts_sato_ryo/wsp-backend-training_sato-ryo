/**
 * 勤務パターンマスタのサービスのテストクラス
 */
@isTest
private class AttPatternServiceTest {

	private static AttPatternRepository2 patternRepository = new AttPatternRepository2();
	private static AttPatternService patternService = new AttPatternService();

	/**
	 * 勤務パターンマスタのテストデータを作成する
	 */
	private static AttPatternEntity creatAttPatternEntity(String name, Id companyId, String companyCode) {
		AttPatternEntity entity = new AttPatternEntity();
		entity.name = name;
		entity.companyId = companyId;
		entity.code = name;
		entity.nameL0 = name + '_L0';
		entity.nameL1 = name + '_L1';
		entity.nameL2 = name + '_L2';
		entity.uniqKey = companyCode + '-' + entity.code;
		entity.validFrom = AppDate.today();
		entity.validTo = AppDate.today().addDays(10);
		entity.order = 1;
		entity.workSystem = AttWorkSystem.JP_MODIFIED;
		entity.contractedWorkHours = AttDailyTime.valueOf(8 * 60);
		entity.startTime = AttTime.valueOf(9 * 60);
		entity.endTime = AttTime.valueOf(18 * 60);
		entity.rest1StartTime = AttTime.valueOf(12 * 60);
		entity.rest1EndTime = AttTime.valueOf(13 * 60);
		entity.useAMHalfDayLeave = true;
		entity.aMContractedWorkHours = AttDailyTime.valueOf(4 * 60);
		entity.aMStartTime = AttTime.valueOf(14 * 60);
		entity.aMEndTime = AttTime.valueOf(18 * 60 + 30);
		entity.aMRest1StartTime = AttTime.valueOf(15 * 60);
		entity.aMRest1EndTime = AttTime.valueOf(15 * 60 + 30);
		entity.usePMHalfDayLeave = true;
		entity.pMContractedWorkHours = AttDailyTime.valueOf(4 * 60);
		entity.pMStartTime = AttTime.valueOf(9 * 60);
		entity.pMEndTime = AttTime.valueOf(13 * 60 + 30);
		entity.pMRest1StartTime = AttTime.valueOf(11 * 60);
		entity.pMRest1EndTime = AttTime.valueOf(11 * 60 + 30);
		entity.boundaryOfStartTime = AttTime.valueOf(0);
		entity.boundaryOfEndTime = AttTime.valueOf(5*60);
		return entity;
	}

	/**
	 * savePatternEntityのテスト
	 * 新規の勤務パターンが保存できることを確認する
	 */
	@istest static void savePatternEntityTestNew() {

		// テストデータ作成
		AttTestData testData = new AttTestData();
		AttPatternEntity patternEntity = creatAttPatternEntity('Test', testData.company.id, testData.company.Code__c);

		// 実行
		Repository.SaveResult result = patternService.savePatternEntity(patternEntity);

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		AttPatternEntity resultPatternEntity = patternRepository.getPatternById(result.details[0].Id);
		System.assertEquals(patternEntity.createUniqKey(testData.company.Code__c), resultPatternEntity.uniqkey);
		System.debug(resultPatternEntity.uniqKey);
	}

	/**
	 * savePatternEntityのテスト
	 * 勤務パターンが更新できることを確認する
	 */
	@istest static void savePatternEntityTestUpdate() {

		// テストデータ作成
		AttTestData testData = new AttTestData();
		AttPatternEntity patternEntity = creatAttPatternEntity('Test', testData.company.id, testData.company.Code__c);
		Id patternEntityId = patternRepository.saveEntity(patternEntity).details[0].Id;

		// 更新情報を設定する
		AttPatternEntity updateEntity = patternRepository.getPatternById(patternEntityId);
		updateEntity.code = patternEntity.code + '_Update';
		updateEntity.nameL0 = patternEntity.nameL0 + '_Update';
		updateEntity.validTo = patternEntity.validTo.addDays(-1);

		// 実行
		Repository.SaveResult result = patternService.savePatternEntity(updateEntity);

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		AttPatternEntity resultPatternEntity = patternRepository.getPatternById(result.details[0].Id);
		System.assertEquals(updateEntity.createUniqKey(testData.company.Code__c), resultPatternEntity.uniqkey);
		System.assertEquals(updateEntity.nameL0, resultPatternEntity.nameL0);
		System.assertEquals(updateEntity.validTo, resultPatternEntity.validTo);
	}

	/**
	 * savePatternEntityのテスト
	 * コードが重複している場合はアプリ例外が発生することを確認する
	 */
	@isTest static void savePatternEntityTestDuplicateCode() {

		// テストデータ作成
		AttTestData testData = new AttTestData();
		AttPatternEntity patternEntity1 = creatAttPatternEntity('Test1', testData.company.id, testData.company.Code__c);
		patternEntity1.code = 'code1';
		Id patternEntity1Id = patternRepository.saveEntity(patternEntity1).details[0].Id;
		AttPatternEntity patternEntity2 = creatAttPatternEntity('Test2', testData.company.id, testData.company.Code__c);
		patternEntity2.code = 'code2';
		patternRepository.saveEntity(patternEntity2);

		// 更新情報を設定する
		AttPatternEntity updateEntity = patternRepository.getPatternById(patternEntity1Id);
		updateEntity.code = patternEntity2.code;

		try {
			// 実行
			patternService.savePatternEntity(updateEntity);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
		}
	}

	/**
	 * savePatternEntityのテスト
	 * 項目値に対するバリデーションが実行されていることを確認する
	 */
	@isTest static void savePatternEntityTestValidate() {

		// テストデータ作成
		AttTestData testData = new AttTestData();
		AttPatternEntity patternEntity = creatAttPatternEntity('Test', testData.company.id, testData.company.Code__c);

		// 有効開始日と失効日の順番を逆にする
		patternEntity.validFrom = AppDate.today();
		patternEntity.validTo = AppDate.today().addDays(-1);

		try {
			// 実行
			patternService.savePatternEntity(patternEntity);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(App.ERR_CODE_INVALID_VALUE, e.getErrorCode());
		}
	}

	/**
	 * getPatternIdのテスト
	 * Idを検索条件に正しく値が取得できることを確認する
	 */
	 @isTest static void getPatternByIdTest() {
		 // テストデータ作成
		AttTestData testData = new AttTestData();
		AttPatternEntity expPattern = creatAttPatternEntity('gettest', testData.company.id, testData.company.Code__c);
		AttPattern__c expObj = expPattern.createSObject();
		insert expObj;
		expPattern.setId(expObj.id);

		// Case 1: idを指定して正しい値が取得できる
		AttPatternEntity actPattern;
		try {
			actPattern = patternService.getPatternById(expPattern.id);
		} catch (Exception e) {
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}
		// 検証
		assertPatternEntity(expPattern, actPattern, testData.org);

		// Case 2: 存在しないidを指定してnullが取得できる
		actPattern = null;
		try {
			actPattern = patternService.getPatternById(testData.company.id);
		} catch (Exception e) {
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}
		// 検証
		System.assertEquals(null, actPattern);

		// Case 3: nullを指定した場合はnullが取得できる
		actPattern = null;
		try {
			Id testId = null;
			actPattern = patternService.getPatternById(testId);
		} catch (Exception e) {
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}
		// 検証
		System.assertEquals(null, actPattern);
	 }

	 /**
	  * 検索で取得した権限の各項目の値が期待値に一致することを確認する
		* @param expPattern 期待値の勤務パターン
		* @param actPattern 検証する勤務パターン
		*/
	 private static void assertPatternEntity(AttPatternEntity expPattern, AttPatternEntity actPattern, ComOrgSetting__c org) {
		if (actPattern == null && expPattern != null) {
			TestUtil.fail('該当するデータがありません');
		}
		System.assertEquals(expPattern.id, actPattern.id);
		// 多言語対応項目の検証
		if (org != null) {
			if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_0__c)) {
				System.assertEquals(expPattern.nameL0, actPattern.nameL.getValue());
			} else if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_1__c)) {
				System.assertEquals(expPattern.nameL1, actPattern.nameL.getValue());
			} else if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_2__c)) {
				System.assertEquals(expPattern.nameL2, actPattern.nameL.getValue());
			}
		} else {
			System.assertEquals(expPattern.nameL0, actPattern.nameL.getValue());
		}
		System.assertEquals(expPattern.companyId, actPattern.companyId);
		System.assertEquals(expPattern.code, actPattern.code);
		System.assertEquals(expPattern.uniqKey, actPattern.uniqKey);
		System.assertEquals(expPattern.nameL0, actPattern.nameL0);
		System.assertEquals(expPattern.nameL1, actPattern.nameL1);
		System.assertEquals(expPattern.nameL2, actPattern.nameL2);

		// サンプル項目で検証
		System.assertEquals(expPattern.startTime, actPattern.startTime);
		System.assertEquals(expPattern.endTime, actPattern.endTime);
		System.assertEquals(expPattern.rest1StartTime, actPattern.rest1StartTime);
		System.assertEquals(expPattern.rest1EndTime, actPattern.rest1EndTime);
	 }
}