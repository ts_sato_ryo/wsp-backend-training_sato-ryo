/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description ユーザレコード操作APIを実装するクラス<br/>
 *              ログインユーザ自身の情報を取得する場合は、UserSettingResouceクラスのAPIを使用してください。
 */
public with sharing class UserResource {

	/**
	 * @desctiprion ユーザのレスポンスパラメータ
	 */
	 public class UserParam {
	 	/** レコードID */
	 	public String id;
	 	/** ユーザ氏名 */
	 	public String name;
	 	/** ユーザ名 */
	 	public String userName;
	 	/** プロファイルID */
	 	public String profileId;
	 	/** プロファイル */
	 	public LookupField profile;

		/**
		 * UserEntityからインスタンスを作成する
		 */
		public UserParam(UserEntity entity) {
			this.id = entity.id;
			this.name = entity.name;
			this.userName = entity.userName;
			this.profileId = entity.profileId;
			if (entity.profile != null) {
				this.profile = new LookupField();
				this.profile.name = entity.profile.name;
			}
		}
	 }

	 /**
	  * @description ルックアップ項目を表すクラス
	  */
	 public class LookupField {
	 	/** レコードのName項目 */
	 	public String name;
	 }

	/**
	 * @description ユーザレコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException　不正な値が設定されている場合
		 */
		public void validate() {
			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.ValueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}
		}

	}

	/**
	 * @description ユーザレコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public UserResource.UserParam[] records;
	}

	/**
	 * @description ユーザレコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description ユーザレコードを1件検索する
		 * @param  parameter リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);
			Map<String, Object> paramMap = req.getParamMap();

			// 検索パラメータ作成
			List<Id> userIds = null;
			if(paramMap.containsKey('id')){
				userIds = new List<Id>{param.Id};
			}

			// 検索実行
			UserRepository repo = new UserRepository();
			List<UserEntity> userEntities = repo.getEntityList(userIds);
			List<UserResource.UserParam> users = new List<UserResource.UserParam>();
			for (UserEntity userEntity : userEntities) {
				users.add(new UserResource.UserParam(userEntity));
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = users;
			return res;
		}
	}

}
