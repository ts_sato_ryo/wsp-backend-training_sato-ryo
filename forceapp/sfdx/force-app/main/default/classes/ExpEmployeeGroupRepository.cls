/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Expense Employee Group Repository
 */
public with sharing class ExpEmployeeGroupRepository extends Repository.BaseRepository {

	/** Max record number to fetch */
	public static final Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	public static final Boolean IS_NOT_FOR_UPDATE = false;

	/**
	 * Save Expense Employee Group Entity
	 * @param entity Expense Employee Group Entity
	 * @reutrn save result
	 */
	public Repository.SaveResult saveEntity(ExpEmployeeGroupEntity entity) {
		return saveEntityList(new List<ExpEmployeeGroupEntity>{ entity });
	}

	/**
	 *  Save Expense Employee Group Entity List
	 *  @param entityList List of Expense Employee Group Enityt
	 *  @return save result
	 */
	public Repository.SaveResult saveEntityList(List<ExpEmployeeGroupEntity> entityList) {

		List<ExpEmployeeGroup__c> sObjList = createSObjectList(entityList);

		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);

		return resultFactory.createSaveResult(resultList);
	}
	/**
	 * Search Filter for Expense Employee Group
	 */
	public class SearchFilter {
		/** Expense Employee Group Active Flag */
		public Boolean active;
		/** Expense Employee Group Id */
		public Set<Id> idSet;
		/** Expense Employee Group Code */
		public Set<String> codeSet;
		/** Company ID */
		public Set<Id> companyIdSet;

	}
	/**
	 * Get Active Expense Employee Group by Code Set
	 * @param codeSet ExpEmployeeGroup Code Set to search
	 * @return List of ExpEmployeeGroupEntity found
	 */
	public List<ExpEmployeeGroupEntity> getActiveEntityListByCodeSet(Set<String> codeSet) {
		ExpEmployeeGroupRepository.SearchFilter filter = new ExpEmployeeGroupRepository.SearchFilter();
		filter.codeSet = codeSet;
		filter.active = true;
		return searchEntity(filter, IS_NOT_FOR_UPDATE, SEARCH_RECORDS_NUMBER_MAX);
	}

	/**
	 * Search Expense Employee Group using Search Filter
	 * This method will retrieve Max number of search record with boolean forUpdate false
	 * @param filter ExpEmployeeGroupRepository SearchFilter
	 * @return List of ExpEmployeeGroupEntity found
	 */
	public List<ExpEmployeeGroupEntity> searchEntity(ExpEmployeeGroupRepository.SearchFilter filter) {
		return searchEntity(filter, IS_NOT_FOR_UPDATE, SEARCH_RECORDS_NUMBER_MAX);
	}

	/**
	 * Search Expense Employee Group using Search Filter
	 * @param filter ExpEmployeeGroupRepository SearchFilter
	 * @param forUpdate Boolean to indicate is for update search or not
	 * @param resultLimit Number of record to retrieve
	 * @return List of ExpEmployeeGroupEntity found
	 */
	public List<ExpEmployeeGroupEntity> searchEntity(ExpEmployeeGroupRepository.SearchFilter filter, Boolean forUpdate, Integer resultLimit) {
		// WHERE statement
		List<String> whereList = new List<String>();

		// Filter by IDs
		Set<Id> pIds;
		if (filter.idSet != null) {
			pIds = filter.idSet;
			whereList.add('Id IN :pIds');
		}

		 // Filter by Expense Employee Group Code
		Set<String> pCodes;
		if (filter.codeSet != null) {
			pCodes = filter.codeSet;
			whereList.add(getFieldName(ExpEmployeeGroup__c.Code__c) + ' = :pCodes');
		}

		// Filter by Company IDs
		Set<Id> pCompanyIds;
		if (filter.companyIdSet != null) {
			pCompanyIds = filter.companyIdSet;
			whereList.add(getFieldName(ExpEmployeeGroup__c.CompanyId__c) + ' IN :pCompanyIds');
		}

		// Filter by Group is Active
		if (filter.active != null) {
			if (filter.active) {
				whereList.add(getFieldName(ExpEmployeeGroup__c.Active__c) + ' = TRUE');
			}
			else {
				whereList.add(getFieldName(ExpEmployeeGroup__c.Active__c) + ' = FALSE');
			}
		}

		String whereString = buildWhereString(whereList);

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		// Regular fields
		for (Schema.SObjectField fld : SELECT_FIELD_SET) { //Retrive all the column for now
				selectFieldList.add(getFieldName(fld));
		}

		// Build SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
			+ ' FROM ' + ExpEmployeeGroup__c.SObjectType.getDescribe().getName()
			+ whereString
			+ ' LIMIT :resultLimit';

		// Execute query
		List<ExpEmployeeGroup__c> sObjs = (List<ExpEmployeeGroup__c>)Database.query(soql);

		// Create entity from SObject
		List<ExpEmployeeGroupEntity> entityList = new List<ExpEmployeeGroupEntity>();
		for (ExpEmployeeGroup__c sObj : sObjs) {
			entityList.add(createEntity(sObj));
		}

		return entityList;

	}

	/**
	 * Target fields to fetch data
	 * If field is added, add same field to baseFieldSet
	 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		// Target fields definition
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
			ExpEmployeeGroup__c.Id,
			ExpEmployeeGroup__c.Active__c,
			ExpEmployeeGroup__c.Code__c,
			ExpEmployeeGroup__c.CompanyId__c,
			ExpEmployeeGroup__c.Description_L0__c,
			ExpEmployeeGroup__c.Description_L1__c,
			ExpEmployeeGroup__c.Description_L2__c,
			ExpEmployeeGroup__c.Name,
			ExpEmployeeGroup__c.Name_L0__c,
			ExpEmployeeGroup__c.Name_L1__c,
			ExpEmployeeGroup__c.Name_L2__c,
			ExpEmployeeGroup__c.UniqKey__c
			};
	}

	@TestVisible
	private ExpEmployeeGroupEntity createEntity(ExpEmployeeGroup__c expEmployeeGroupSObj) {
		ExpEmployeeGroupEntity entity = new ExpEmployeeGroupEntity();

		entity.setId(expEmployeeGroupSObj.Id);
		entity.active = expEmployeeGroupSObj.Active__c;
		entity.code = expEmployeeGroupSObj.Code__c;
		entity.companyId = expEmployeeGroupSObj.CompanyId__c;
		entity.descriptionL0 = expEmployeeGroupSObj.Description_L0__c;
		entity.descriptionL1 = expEmployeeGroupSObj.Description_L1__c;
		entity.descriptionL2 = expEmployeeGroupSObj.Description_L2__c;
		entity.nameL0 = expEmployeeGroupSObj.Name_L0__c;
		entity.nameL1 = expEmployeeGroupSObj.Name_L1__c;
		entity.nameL2 = expEmployeeGroupSObj.Name_L2__c;
		entity.uniqKey = expEmployeeGroupSObj.UniqKey__c;
		entity.resetChanged();

		return entity;
	}

	private ExpEmployeeGroup__c createSObject(ExpEmployeeGroupEntity expEmpGroupEntity) {
		ExpEmployeeGroup__c empGroupSObj = new ExpEmployeeGroup__c();

		empGroupSObj.Id = expEmpGroupEntity.id;
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.ACTIVE)) {
			empGroupSObj.Active__c = expEmpGroupEntity.active;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.CODE)) {
			empGroupSObj.Code__c = expEmpGroupEntity.code;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.COMPANY_ID)) {
			empGroupSObj.CompanyId__c = expEmpGroupEntity.companyId;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.DESCRIPTION_L0)) {
			empGroupSObj.Description_L0__c = expEmpGroupEntity.descriptionL0;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.DESCRIPTION_L1)) {
			empGroupSObj.Description_L1__c = expEmpGroupEntity.descriptionL1;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.DESCRIPTION_L2)) {
			empGroupSObj.Description_L2__c = expEmpGroupEntity.descriptionL2;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.NAME_L0)) {
			empGroupSObj.Name_L0__c = expEmpGroupEntity.nameL0;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.NAME_L1)) {
			empGroupSObj.Name_L1__c = expEmpGroupEntity.nameL1;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.NAME_L2)) {
			empGroupSObj.Name_L2__c = expEmpGroupEntity.nameL2;
		}
		if (expEmpGroupEntity.isChanged(ExpEmployeeGroupEntity.Field.UNIQ_KEY)) {
			empGroupSObj.UniqKey__c = expEmpGroupEntity.uniqKey;
		}

		return empGroupSObj;
	}

	private List<ExpEmployeeGroup__c> createSObjectList(List<ExpEmployeeGroupEntity> expEmployeeGroupEntityList) {
		List<ExpEmployeeGroup__c> sObjectList = new List<ExpEmployeeGroup__c>();

		for (ExpEmployeeGroupEntity entity:expEmployeeGroupEntityList) {
			sObjectList.add(createSObject(entity));
		}

		return sObjectList;
	}

}