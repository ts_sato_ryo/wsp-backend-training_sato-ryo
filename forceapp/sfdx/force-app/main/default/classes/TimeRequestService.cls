/**
 * @group 工数
 *
 * 工数確定申請に関するサービスを提供するクラス
 */
public with sharing class TimeRequestService implements Service {

	private static final String ACTION_TYPE_REMOVED = 'Removed';

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	private final EmployeeService empService = new EmployeeService();

	private final TimeService timeService = new TimeService();

	private final TimeSettingService timeSettingService = new TimeSettingService();

	/**
	 * @description スナップショットとして添付ファイルに保存する工数サマリーの情報
	 */
	public class TimeSummarySnapshot {

		/** 工数サマリーのID */
		public String summaryId;
		/** 社員名 */
		public String employeeName;
		/** 社員の写真のURL */
		public String employeePhotoUrl;
		/** 工数サマリーの開始日 */
		public String startDate;
		/** 工数サマリーの終了日 */
		public String endDate;
		/** 工数サマリー期間の作業時間合計(分) */
		public Integer workTime;
		/** 工数実績サマリーのリスト */
		public List<TaskSummaryRecord> summaryRecords = new List<TaskSummaryRecord>();
		/** 工数実績のリスト */
		public List<TimeRecord> timeRecords = new List<TimeRecord>();

		/** テスト用のコンストラクタ */
		@TestVisible
		private TimeSummarySnapshot() {
		}

		/**
		 * 工数サマリーからインスタンスを生成する
		 * @param summary 工数サマリー
		 * @param summaryRecords 工数サマリーレコードリスト
		 * @param lang 言語
		 */
		public TimeSummarySnapshot(TimeSummaryEntity summary, List<TimeService.TaskSummaryRecord> summaryRecords, AppLanguage lang) {
			this.summaryId = summary.id;
			this.employeeName = summary.employeeName;
			this.employeePhotoUrl = summary.employeePhotoUrl;
			this.startDate = summary.startDate.format();
			this.endDate = summary.endDate.format();
			this.workTime = summary.workTime;

			// 工数実績サマリーを作成
			for (TimeService.TaskSummaryRecord record : summaryRecords) {
				 this.summaryRecords.add(new TaskSummaryRecord(record, lang));
			}

			// 工数実績を作成
			for (TimeRecordEntity record : summary.recordList) {
				this.timeRecords.add(new TimeRecord(record, lang));
			}
		}

		/**
		 * 添付ファイルからインスタンスを生成する
		 * @param attach 添付ファイル
		 */
		public TimeSummarySnapshot(AppAttachmentEntity attach) {
			TimeSummarySnapshot snapshot = (TimeSummarySnapshot)JSON.deserialize(attach.bodyText, TimeSummarySnapshot.class);
			this.summaryId = snapshot.summaryId;
			this.employeeName = snapshot.employeeName;
			this.employeePhotoUrl = snapshot.employeePhotoUrl;
			this.startDate = snapshot.startDate;
			this.endDate = snapshot.endDate;
			this.workTime = snapshot.workTime;
			this.summaryRecords = snapshot.summaryRecords;
			this.timeRecords = snapshot.timeRecords;
		}

		/**
		 * インスタンスをJson文字列に変換する
		 * @return 変換したJson文字列
		 */
		public String toJson() {
			return JSON.serialize(this);
		}
	}

	/**
	 * スナップショットとして添付ファイルに保存する工数実績サマリーレコード
	 */
	public class TaskSummaryRecord {
		/** ジョブID */
		public String jobId;
		/** ジョブコード */
		public String jobCode;
		/** ジョブ名 */
		public String jobName;
		/** 作業分類ID */
		public String workCategoryId;
		/** 作業分類コード */
		public String workCategoryCode;
		/** 作業分類名 */
		public String workCategoryName;
		/** 残工数ジョブかどうか */
		public Boolean isDefaultJob;
		/**
		 * 実績作業時間比率(%)
		 * 小数点第1位まで有効(取りうる値の例: 50.0)
		 * 分子：実績作業時間 分母：サマリの作業時間合計
		 */
		public Decimal workTimeRatio;
		/** 実績作業時間(分) */
		public Integer workTime;

		/** テスト用のコンストラクタ */
		@TestVisible
		private TaskSummaryRecord() {
		}

		/**
		 * TimeService.TaskSummaryRecordからスナップショット保存用の情報を作成する
		 * @param record 工数実績サマリーのレコード
		 */
		public TaskSummaryRecord(TimeService.TaskSummaryRecord record, AppLanguage lang) {
			this.jobId = record.jobId;
			this.jobCode = record.jobCode;
			this.jobName = record.jobNameL.getValue(lang);
			if (record.workCategoryId != null) {
				this.workCategoryId = record.workCategoryId;
				this.workCategoryCode = record.workCategoryCode;
				this.workCategoryName = record.workCategoryNameL.getValue(lang);
			}
			this.isDefaultJob = record.isDefaultJob;
			this.workTimeRatio = record.workTimeRatio;
			this.workTime = record.workTime;
		}
	}

	/**
	 * @description スナップショットとして添付ファイルに保存する工数明細の情報
	 */
	public class TimeRecord {
		/** 日付 */
		public String recordDate;
		/** 日次の作業報告 */
		public String note;
		/** 工数内訳のリスト */
		public List<TimeRecordItem> recordItemList = new List<TimeRecordItem>();

		/** テスト用のコンストラクタ */
		@TestVisible
		private TimeRecord() {
		}

		/**
		 * コンストラクタ（言語キー指定）
		 */
		public TimeRecord(TimeRecordEntity entity, AppLanguage lang) {
			recordDate = entity.recordDate.format();
			note = entity.note;
			for (TimeRecordItemEntity recordItemEntity : entity.recordItemList) {
				recordItemList.add(new TimeRecordItem(recordItemEntity, lang));
			}
		}
	}

	/**
	 * @description スナップショットとして添付ファイルに保存する工数内訳の情報
	 */
	public class TimeRecordItem {
		/** ジョブID */
		public String jobId;
		/** ジョブコード */
		public String jobCode;
		/** ジョブ名 */
		public String jobName;
		/** 作業分類ID */
		public String workCategoryId;
		/** 作業分類コード */
		public String workCategoryCode;
		/** 作業分類名 */
		public String workCategoryName;
		/** 入力形式 */
		public Boolean isDirectInput;
		/** 工数（分） */
		public Integer taskTime;
		/** 比率 */
		public Integer ratio;
		/** ボリューム */
		public Integer volume;
		/** タスクの作業報告 */
		public String taskNote;
		/** 並び順 */
		public Integer order;

		/** テスト用のコンストラクタ */
		@TestVisible
		private TimeRecordItem() {
		}

		/**
		 * コンストラクタ（言語キー指定）
		 */
		public TimeRecordItem(TimeRecordItemEntity entity, AppLanguage lang) {
			jobId = entity.jobId;
			jobCode = entity.jobCode;
			jobName = entity.jobNameL.getValue(lang);
			if (entity.workCategoryId != null) {
				workCategoryId = entity.workCategoryId;
				workCategoryCode = entity.workCategoryCode;
				workCategoryName = entity.workCategoryNameL.getValue(lang);
			}
			ratio = entity.ratio;
			taskTime = entity.workTime;
			taskNote = entity.taskNote;
			order = entity.order;
		}
	}

	/**
	 * 工数確定申請を行う
	 * @param employeeId 社員ベースID
	 * @param targetDate 申請する工数サマリーの範囲内の日
	 * @param comment 申請コメント
	 * @return 作成した工数確定申請エンティティ
	 * @throws App.IllegalStateException 工数設定の工数利用フラグがOffの場合
	 * @throws App.IllegalStateException 無効なタスクが工数内訳に保存されている場合
	 */
	public TimeRequestEntity submit(Id employeeId, AppDate targetDate, String comment) {
		TimeSummaryRepository sumRepo = new TimeSummaryRepository();

		// 申請社員を取得
		EmployeeBaseEntity employee = empService.getEmployee(employeeId, targetDate);

		// 工数確定申請を利用しない設定の場合はエラーとする
		TimeSettingBaseEntity timeSetting = timeSettingService.getTimeSetting(employeeId, targetDate);
		if (!timeSetting.useRequest) {
			throw new App.IllegalStateException(App.ERR_CODE_CANNOT_USE_TIME_REQUEST, MESSAGE.Time_Err_CannotUseRequest);
		}

		// 社員の会社のデフォルト言語を取得
		AppLanguage companyLang = getCompanyLanguage(employee.companyId);

		// 工数サマリーを取得する
		TimeSummaryEntity summary = timeService.getSummary(employeeId, targetDate);

		// スナップショットを作成する
		TimeRecordItemSnapshotService snapshotService = new TimeRecordItemSnapshotService(employee.companyId);
		snapshotService.save(summary.recordList);

		summary.isLocked = true;
		if (summary.id == null) {
			// 新規データの場合は保存
			TimeSummaryRepository.SavaSummaryResult res = sumRepo.saveSummaryEntity(summary, true);
			// 保存したデータを再取得
			summary = timeService.getSummary(employeeId, targetDate); // TODO 再取得が不要な方法を検討する（サマリーのIDや、数式項目を取得できれば良いはず）
		} else {
			sumRepo.saveSummaryEntity(summary, false);
		}

		// 申請データを作成する
		TimeRequestEntity request = createRequestData(employee, companyLang, summary, comment, timeSetting);

		// 工数サマリーレコードを取得する
		List<TimeService.TaskSummaryRecord> summaryRecords = timeService.createTaskSummary(employeeId, targetDate);

		// 明細・内訳データをJSON化して申請レコードにファイル添付
		TimeSummarySnapshot snapshot = new TimeSummarySnapshot(summary, summaryRecords, companyLang);
		attachDetailData(request, snapshot.toJson());

		// 申請実行
		submitProcess(request.Id, comment);
		return request;
	}

	/**
	 * @description 工数確定申請データを作成する
	 * @param emp 社員オブジェクト
	 * @@aram lang 言語
	 * @param summary 工数サマリーオブジェクト
	 * @param comment コメント
	 * @param timeSetting 工数設定
	 * @return 工数確定申請オブジェクト
	 */
	private TimeRequestEntity createRequestData(final EmployeeBaseEntity employee, final AppLanguage lang,
			final TimeSummaryEntity summary, final String comment,
			TimeSettingBaseEntity timeSetting) {
		TimeRequestRepository reqRepo = new TimeRequestRepository();
		TimeRequestEntity request;

		if (summary.requestId == null) {
			timeService.validateTasks(summary);

			// 申請データを作成
			request = createRequest(employee.id, lang, summary, comment, timeSetting);
			// 作成した申請データを検証
			TimeValidator.TimeRequestEntityValidator validator = new TimeValidator.TimeRequestEntityValidator(request);
			Validator.Result validateResult = validator.validate();
			if (validateResult.hasError()) {
				throw new  App.IllegalStateException(
					'[SystemError]' + validateResult.getErrors());
			}
			Repository.SaveResult result = reqRepo.saveEntity(request);
			request.setId(result.details[0].id);

			// サマリーデータに申請データを紐づけ
			summary.requestId = request.id;
			TimeSummaryRepository sumRepo = new TimeSummaryRepository();
			sumRepo.saveSummaryEntity(summary, false);
		} else {
			// 申請データを取得
			request = reqRepo.getTimeRequest(summary.requestId);

			if (! request.status.equals(AppRequestStatus.DISABLED)) {
				// メッセージ：申請することができません。
				throw new App.IllegalStateException(
						App.ERR_CODE_CANNOT_REQUEST,
						ComMessage.msg().Trac_Err_CannotRequest);
			}
			timeService.validateTasks(summary);

			// 申請者の情報を取得
			TimeResource.Util util = new TimeResource.Util();
			EmployeeBaseEntity actor;
			if (employee.userId == UserInfo.getUserId()) {
				actor = employee;
			} else {
				// ログインユーザ(申請操作を行ったユーザ)の社員情報を取得
				actor = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
				// actor = util.getEmployeeByUserId(UserInfo.getUserId());
			}

			// 承認者の情報を取得
			EmployeeBaseEntity approver = getNextApprover(employee.id, summary);

			// 申請データを更新
			request.approver01Id = approver.userId;
			request.comment = comment;
			request.status = AppRequestStatus.PENDING;
			request.requestTime = AppDatetime.now();
			request.actorId = actor.getHistory(0).id;
			request.isConfirmationRequired = false;
			request.cancelType = AppCancelType.NONE;
			reqRepo.saveEntity(request);
		}

		return request;
	}

	/**
	 * 工数確定申請時に添付ファイルに保存したスナップショットを取得する
	 * @param requestId 工数確定申請ID
	 * @return 工数確定申請のスナップショット
	 */
	public TimeSummarySnapshot getTimeSummarySnapshot(Id requestId) {
		// 更新日時の降順で取得するので、一番最後の更新したレコードを返す
		AppAttachmentRepository repo = new AppAttachmentRepository();
		List<AppAttachmentEntity> attachList = repo.getEntityListByParentId(requestId);
		if (attachList == null || attachList.isEmpty()) {
			return null;
		}

		AppAttachmentEntity attach = attachList[0];
		return new TimeSummarySnapshot(attach);
	}

	/**
	 * @description 工数確定申請レコードにファイルを添付する
	 * @param request 工数確定申請エンティティ
	 * @param body 添付内容
	 */
	private void attachDetailData(TimeRequestEntity request, String body) {
		AppAttachmentRepository repo = new AppAttachmentRepository();

		// 添付ファイルを取得
		// FIXME 申請時と再申請時で会社のデフォルト言語が異なる場合に、同一の申請でも複数の添付ファイルが作成される
		List<AppAttachmentEntity> attaches = repo.getEntityListByParentId(request.id, request.name);
		AppAttachmentEntity attach;
		if ((attaches == null) || (attaches.isEmpty())) {
			// ない場合は新規作成
			attach = new AppAttachmentEntity();
			attach.name = request.name;
			attach.parentId = request.id;
		} else {
			attach = attaches[0];

			// 同じファイル名で複数存在している場合は、ログを出力しておく
			if (attaches.size() > 1) {
				System.debug(System.LoggingLevel.WARN,
					'工数確定申請レコードに同名の添付ファイルが複数存在します。(ParentId=' + request.id + 'ファイル名=' + request.name + ')');
			}
		}
		attach.bodyText = body;

		// 保存
		repo.saveEntity(attach);
	}

	/**
	 * @description 承認申請を行う
	 * @param requestId 申請対象レコードのID
	 * @param comment コメント
	 */
	private void submitProcess(Id requestId, String comment) {
		Approval.ProcessSubmitRequest processReq = new Approval.ProcessSubmitRequest();
		processReq.setObjectId(requestId);
		processReq.setComments(comment);
		// TODO: 承認者を指定
//		if (approverId != null) {
//			processReq.setNextApproverIds(new Id[]{approverId});
//		}

		if (!test.isRunningTest()) {
			Approval.process(processReq);
		}
	}

	/**
	 * @description 取消処理を行う
	 * @param requestId 申請ID
	 * @param comment コメント
	 */
	public void cancelProcess(Id requestId, String comment) {
		// 申請データを取得
		TimeRequestRepository repo = new TimeRequestRepository();
		TimeRequestEntity request = repo.getTimeRequest(requestId);
		if (request == null) {
			// メッセージ：該当する申請が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_REQUEST,
					ComMessage.msg().Trac_Err_NotFoundRequest);
		}

		if (request.status.equals(AppRequestStatus.PENDING)) {
			// 申請取消
			removeRequest(request, comment);
		} else if (request.status.equals(AppRequestStatus.APPROVED)) {
			// 承認取消
			cancelRequest(request, comment);
		} else {
			// メッセージ：該当の申請は取り消せません。
			throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_CANCEL_REQUEST,
					ComMessage.msg().Trac_Err_CannotRemoveRequest);
		}
	}

	/**
	 * @description 月次工数確定申請一覧データを取得する
	 * @param  requestIdList 工数確定申請IDのリスト
	 * @return 工数確定申請オブジェクトのリスト
	 */
	public List<TimeRequestEntity> getRequestList(final List<Id> requestIdList) {
		TimeRequestRepository repo = new TimeRequestRepository();

		// 工数確定申請データを取得
		List<TimeRequestEntity> requestList = repo.getTimeRequestList(requestIdList);

		return requestList;
	}

	/**
	 * @description 月次工数確定申請データを取得する
	 * @param  requestId 工数確定申請ID
	 * @return 工数確定申請オブジェクト
	 */
	public TimeRequestEntity getRequestById(final Id requestId) {
		TimeRequestRepository repo = new TimeRequestRepository();

		// 工数確定申請データを取得
		return repo.getTimeRequest(requestId);
	}

	/**
	 * 申請情報を取得
	 * @param summary 工数サマリーオブジェクト
	 */
	 public TimeRequestEntity getRequest(TimeSummaryEntity summary) {
		return new TimeRequestRepository().getTimeRequest(summary.requestId);
	 }

	/*
	 * 工数確定申請の承認者を取得する
	 * 工数確定申請のステータスに関わらず、次の承認者として指定する社員を取得する
	 * @param summary 工数サマリー
	 * @return 承認者の社員情報（承認者が設定されていない場合はnull）
	 */
	public EmployeeBaseEntity getNextApprover(Id employeeId, TimeSummaryEntity summary) {
		AppDateRange validRange = timeService.getEmployeeValidRange(employeeId, summary);
		return getApprover(employeeId, validRange.endDate);
	}

	/**
	 * 日付を指定して社員の工数確定申請の承認者を取得する
	 *
	 * 現状は一律、承認者選択を可能とする設定で承認者01を取得する
	 * @param employeeId 社員ベースId
	 * @param targetDate 取得対象の日付
	 * @return 承認者の社員情報（承認者が設定されていない場合はnull）
	 */
	private EmployeeBaseEntity getApprover(Id employeeId, AppDate targetDate) {
		Boolean allowToChangeApproverSelf = true;
		Map<AppDate, EmployeeBaseEntity> approver01Map = empService.getApprover01(allowToChangeApproverSelf, employeeId, new List<AppDate>{targetDate});
		return approver01Map.get(targetDate);
	}

	/**
	 * @description 工数確定申請データを作成する
	 * @param employeeBaseId 社員ベースID
	 * @param lang 申請データのNameを切り替えるための言語
	 * @param summary 工数サマリーオブジェクト
	 * @param comment 申請コメント
	 * @param timeSetting 工数設定
	 * @return 工数確定申請オブジェクト
	 */
	private TimeRequestEntity createRequest(final Id employeeBaseId, final AppLanguage lang, final TimeSummaryEntity summary, String comment,
			final TimeSettingBaseEntity timeSetting) {

		// 部署情報を取得（サマリー期間内で社員が失効する場合は、社員の終了日時点のマスタを取得する）
		AppDateRange employeeValidRange = timeService.getEmployeeValidRange(employeeBaseId, summary);
		DepartmentRepository repo = new DepartmentRepository();
		DepartmentBaseEntity department = repo.getEntity(summary.departmentBaseId, employeeValidRange.endDate);

		// 申請者の社員情報を取得
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity employee = empService.getEmployeeFromCache(employeeBaseId);
		EmployeeBaseEntity actor = empService.getEmployeeByUserIdFromCache(UserInfo.getUserId(), AppDate.today());

		// 工数確定申請データ作成
		TimeRequestEntity request = new TimeRequestEntity();
		request.name = createRequestName(employee, lang, summary, timeSetting);
		request.timeSummaryId = summary.id;
		request.comment = comment;
		request.status = AppRequestStatus.PENDING;
		request.requestTime = AppDatetime.now();
		request.employeeId = summary.employeeId;
		request.departmentId = department == null ? null : department.getHistoryByDate(employeeValidRange.endDate).id;
		request.actorId = actor.getHistory(0).id;
		request.ownerId = employee.userId;

		// 現状は一律、承認者選択を可能とする設定で、サマリーの終了日時点の承認者01を承認者とする
		EmployeeBaseEntity approver = getNextApprover(employeeBaseId, summary);
		if (approver == null) {
			// メッセージ：承認者が見つかりません。承認者が設定されていないか、有効期間外か、またはSalesforceユーザが無効です。
			throw new App.IllegalStateException(App.ERR_CODE_NOT_FOUND_APPROVER, ComMessage.msg().Time_Err_NotFoundApprover);
		}
		request.approver01Id = approver.userId;
		return request;
	}

	/**
	 * 工数確定申請の申請名を作成する
	 * @param employee 社員
	 * @param lang 言語
	 * @param summary 工数サマリーエンティティ
	 * @param timeSetting 工数設定
	 * @return 申請名
	 * ・月次確定の場合
	 * 	・日本語
	 * 		{氏名}さん 工数確定 {YYYY/MM}
	 * 	・英語
	 * 		{氏名} Time Report {MMM YYYY}
	 * ・週次確定の場合
	 * 	・日本語
	 * 		{氏名}さん 工数確定 {YYYY/MM} {subNo}週目
	 * 		例)
	 * 		管理者 001さん 工数確定 2019/08 1週目
	 * 	・英語
	 * 		{氏名} Time Report {MMM YYYY} {subNo}{ordinal Suffix} Week
	 * 		例)
	 * 		Time Report Aug 2019 1st Week
	 */
	@TestVisible
	private String createRequestName(EmployeeBaseEntity employee, AppLanguage lang, TimeSummaryEntity summary,
			TimeSettingBaseEntity timeSetting) {
		String employeeName = employee.displayNameL.getValue(lang);

		AppDate targetDate;
		if (timeSetting.monthMark == TimeSettingBaseGeneratedEntity.MonthMarkType.BeginBase) {
			targetDate = summary.startDate;
		} else if (timeSetting.monthMark == TimeSettingBaseGeneratedEntity.MonthMarkType.EndBase) {
			targetDate = summary.endDate;
		}

		String requestName;
		if (lang == AppLanguage.JA) {
			// 日本語の場合
			// 申請名： {氏名}さん 工数確定 {YYYY/MM}
			requestName = employeeName + 'さん 工数確定 '
					+ targetDate.format(AppDate.FormatType.YYYY_MM_SLASH);
		} else {
			// 英語の場合
			// 申請名： {氏名} Time Report {MMM YYYY}
			requestName = employeeName + ' Time Report '
					+ targetDate.format(AppDate.FormatType.MMMYYYY);
		}

		if (timeSetting.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Week) {
			requestName += createWeeklySuffix(lang, summary.subNo);
		}

		return requestName;
	}

	/**
	 * 表示名で利用する週単位の場合の接尾語を返す
	 * 英語の場合　1st week
	 * 日本語の場合 x週目
	 * @param lang 作成する言語
	 * @param subNo サブNo
	 * @return 週単位の場合の接尾語
	 */
	private String createWeeklySuffix(AppLanguage lang, Integer subNo) {
		String weeklySuffix;

		// 表示名を作成する
		if (lang == AppLanguage.JA) {
			// 日本語(例: x週目)
			weeklySuffix = String.format(' {0}週目',
					new List<String>{String.valueOf(subNo)});
		} else {
			// ●週目/英語の接尾辞を取得
			String ordinalSuffixEnUs;
			if (subNo == 1) {// Ordinalは最大5なのでそれ以降の考慮不要
				ordinalSuffixEnUs = 'st';
			} else if (subNo == 2) {
				ordinalSuffixEnUs = 'nd';
			} else if (subNo == 3) {
				ordinalSuffixEnUs = 'rd';
			} else {
				ordinalSuffixEnUs = 'th';
			}
			// 英語(例: 1st week)
			weeklySuffix = String.format(' {0}{1} Week',
					new List<String>{String.valueOf(subNo), ordinalSuffixEnUs});
		}
		return weeklySuffix;
	}

	/**
	 * @description 申請取消を行う
	 * @param request 申請エンティティ
	 * @param comment コメント
	 */
	private void removeRequest(TimeRequestEntity request, String comment) {
		if (!test.isRunningTest()) {
			// 承認/却下対象の承認申請データを取得
			ApprovalProcessRepository appRepo = new ApprovalProcessRepository();
			List<Id> workItemIdList = appRepo.getWorkItemList(new List<Id>{request.id});
			if (workItemIdList == null || workItemIdList.isEmpty()) {
				// メッセージ：該当の申請は取り消せません。
				throw new App.IllegalStateException(
						App.ERR_CODE_CANNOT_CANCEL_REQUEST,
						ComMessage.msg().Trac_Err_CannotRemoveRequest);
			}

			// 取消実行
			Approval.ProcessWorkitemRequest processReq = new Approval.ProcessWorkitemRequest();
			processReq.setWorkItemId(workItemIdList[0]);
			processReq.setAction(ACTION_TYPE_REMOVED);
			processReq.setComments(comment);

			Approval.process(processReq);
		}
	}

	/**
	 * @description 承認取消を行う
	 * @param request 申請エンティティ
	 * @param comment コメント
	 */
	private void cancelRequest(TimeRequestEntity request, String comment) {
		TimeRequestRepository repo = new TimeRequestRepository();

		// 申請データを更新
		request.status = AppRequestStatus.DISABLED;
		request.isConfirmationRequired = true;
		request.cancelType = AppCancelType.CANCELED;
		request.cancelComment = comment;
		request.processComment = null;
		request.lastApproveTime = null;
		request.lastApproverId = null;
		repo.saveEntity(request);
	}

	/**
	 * 会社のデフォルト言語を取得する
	 * @param companyId 取得対象の会社ID
	 * @return 言語
	 * @throws App.IllegalStateException 会社が見つからない場合、言語が未設定の場合
	 */
	private AppLanguage getCompanyLanguage(Id companyId) {
		CompanyRepository repo = new CompanyRepository();
		CompanyEntity company = repo.getEntity(companyId);
		if (company == null || company.language == null) {
			// メッセージ：会社のデフォルト言語が設定されていません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_SET_COMPANY_LANG,
					ComMessage.msg().Trac_Err_FieldNotSet(new List<String>{
					ComMessage.msg().Com_Lbl_Company,
					ComMessage.msg().Com_Lbl_DefaultLanguage}));
		}
		return company.language;
	}
}