/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 短時間勤務設定のリポジトリ
 */
public with sharing class AttShortTimeSettingRepository extends ParentChildRepository {

	/**
	 * 履歴リストを取得する際の並び順
	 */
	public enum HistorySortOrder {
		VALID_FROM_ASC, VALID_FROM_DESC
	}

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}

	/** キャッシュの利用有無（デフォルトはoff） */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** 短時間勤務設定ベースのキャッシュ */
	private static final Repository.Cache BASE_CACHE = new Repository.Cache();
	/** 短時間勤務設定履歴のキャッシュ */
	private static final Repository.Cache HISTORY_CACHE = new Repository.Cache();

	/** 短時間勤務設定ベースのリレーション名 */
	private static final String HISTORY_BASE_R = AttShortTimeWorkSettingHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/**
	 * 取得対象のベースオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> baseFieldList = new Set<Schema.SObjectField> {
			AttShortTimeWorkSettingBase__c.Id,
			AttShortTimeWorkSettingBase__c.Name,
			AttShortTimeWorkSettingBase__c.CurrentHistoryId__c,
			AttShortTimeWorkSettingBase__c.Code__c,
			AttShortTimeWorkSettingBase__c.UniqKey__c,
			AttShortTimeWorkSettingBase__c.CompanyId__c,
			AttShortTimeWorkSettingBase__c.ReasonId__c,
			AttShortTimeWorkSettingBase__c.WorkSystem__c
		};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(baseFieldList);
	}

	/**
	 * 取得対象の履歴オブジェクトの項目名
	 * 項目が追加されたらhistoryFieldSetに追加してください
	 */
	private static final List<String> GET_HISTORY_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> historyFieldSet = new Set<Schema.SObjectField> {
			AttShortTimeWorkSettingHistory__c.Id,
			AttShortTimeWorkSettingHistory__c.BaseId__c,
			AttShortTimeWorkSettingHistory__c.UniqKey__c,
			AttShortTimeWorkSettingHistory__c.ValidFrom__c,
			AttShortTimeWorkSettingHistory__c.ValidTo__c,
			AttShortTimeWorkSettingHistory__c.HistoryComment__c,
			AttShortTimeWorkSettingHistory__c.Removed__c,
			AttShortTimeWorkSettingHistory__c.Name,
			AttShortTimeWorkSettingHistory__c.Name_L0__c,
			AttShortTimeWorkSettingHistory__c.Name_L1__c,
			AttShortTimeWorkSettingHistory__c.Name_L2__c,
			AttShortTimeWorkSettingHistory__c.AllowableTimeOfShortWork__c,
			AttShortTimeWorkSettingHistory__c.AllowableTimeOfLateArrival__c,
			AttShortTimeWorkSettingHistory__c.AllowableTimeOfEarlyLeave__c,
			AttShortTimeWorkSettingHistory__c.AllowableTimeOfIrregularRest__c
		};

		GET_HISTORY_FIELD_NAME_LIST = Repository.generateFieldNameList(historyFieldSet);
	}

	/**
	 * 取得対象のタグの項目名
	 */
	private static final List<String> GET_BASE_REASON_FIELD_NAME_LIST;
	/** 部署管理者のリレーション名 */
	private static final String BASE_REASON_R = AttShortTimeWorkSettingBase__c.ReasonId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> tagFieldList = new Set<Schema.SObjectField> {
				ComTag__c.Name_L0__c,
				ComTag__c.Name_L1__c,
				ComTag__c.Name_L2__c};

		GET_BASE_REASON_FIELD_NAME_LIST = Repository.generateFieldNameList(BASE_REASON_R, tagFieldList);
	}

	/**
	 * コンストラクタ
	 * キャッシュ利用有無はデフォルト値が適用される
	 */
	public AttShortTimeSettingRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public AttShortTimeSettingRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/**
	 * 指定したエンティティを取得する(ベース+全ての履歴)
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティと、ベースに紐づく全ての履歴エンティティ
	 */
	public AttShortTimeSettingBaseEntity getEntity(Id baseId) {
		return getEntity(baseId, null);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public AttShortTimeSettingBaseEntity getEntity(Id baseId, AppDate targetDate) {

		List<AttShortTimeSettingBaseEntity> entities = getEntityList(new List<Id>{baseId}, targetDate);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseIds 取得対象のベースIDのリスト、nullの場合は全てが取得対象となる
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、履歴が存在しない場合はnull
	 */
	public List<AttShortTimeSettingBaseEntity> getEntityList(List<Id> baseIds, AppDate targetDate) {
		AttShortTimeSettingRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new Set<Id>(baseIds);
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param codes 取得対象の工数設定コードのリスト、nullの場合は全てが取得対象となる
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したコードのベースエンティティと、対象日に有効な履歴エンティティのリスト。ただし、履歴が存在しない場合はnull
	 */
	public List<AttShortTimeSettingBaseEntity> getEntityListByCode(List<String> codes, AppDate targetDate) {
		AttShortTimeSettingRepository.SearchFilter filter = new SearchFilter();
		filter.codes = new Set<String>(codes);
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public AttShortTimeSettingBaseEntity getBaseEntity(Id baseId) {
		return getBaseEntity(baseId, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public AttShortTimeSettingBaseEntity getBaseEntity(Id baseId, Boolean forUpdate) {
		List<AttShortTimeSettingBaseEntity> bases = getBaseEntityList(new List<Id>{baseId}, forUpdate);
		if (bases.isEmpty()) {
			return null;
		}
		return bases[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseIds 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティのリスト。履歴は含まない
	 */
	public List<AttShortTimeSettingBaseEntity> getBaseEntityList(List<Id> baseIds, Boolean forUpdate) {
		AttShortTimeSettingRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.baseIds = new List<Id>(baseIds);
		return searchBaseEntity(filter, forUpdate);
	}

	/**
	 * 指定したコードの工数設定基本情報を取得する(ベースのみ)
	 * @param codes 工数設定のコードのリスト
	 * @return AttWorkingTypeEntityのリスト
	 */
	public List<AttShortTimeSettingBaseEntity> getBaseEntityListByCode(List<String> codes) {
		AttShortTimeSettingRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.codes = new Set<String>(codes);

		return searchBaseEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyId 取得対象の履歴ID
	 * @return 履歴エンティティ。ただし、存在しない場合はnull
	 */
	public AttShortTimeSettingHistoryEntity getHistoryEntity(Id historyId) {
		List<AttShortTimeSettingHistoryEntity> entities = getHistoryEntityList(new List<id>{historyId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyIds 取得対象の履歴ID
	 * @return 履歴エンティティのリスト。
	 */
	public List<AttShortTimeSettingHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		AttShortTimeSettingRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new Set<Id>(historyIds);

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する。有効開始日の昇順
	 */
	public List<AttShortTimeSettingBaseEntity> searchEntity(SearchFilter filter) {
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストのソート順
	 * @return 履歴エンティティの検索
	 */
	public List<AttShortTimeSettingHistoryEntity> searchHistoryEntity(SearchFilter filter) {
		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/** 検索フィルタ(履歴の検索時に使用します) */
	public class SearchFilter {
		/** 勤務パターンベースID */
		public Set<Id> baseIds;
		/** 勤務パターン履歴ID */
		public Set<Id> historyIds;
		/** 会社ID */
		public Set<Id> companyIds;
		/** 取得対象日 */
		public AppDate targetDate;
		/** コード(完全一致) */
		public Set<String> codes;
		/** 労働時間制 */
		public AttWorkSystem workSystem;

		/** 論理削除されている履歴を取得対象にする場合はtrue */
		public Boolean includeRemoved;
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストの並び順
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する
	 */
	public List<AttShortTimeSettingBaseEntity> searchEntityWithHistory(SearchFilter filter, Boolean forUpdate,
			AttShortTimeSettingRepository.HistorySortOrder sortOrder) {

		// 履歴を検索
		List<AttShortTimeSettingHistoryEntity> histories = searchHistoryEntity(filter, forUpdate, sortOrder);

		// 取得対象のベースIDリストを作成
		Set<Id> targetBaseIds = new Set<Id>();
		for (AttShortTimeSettingHistoryEntity history : histories) {
			targetBaseIds.add(history.baseId);
		}

		// ベースエンティティを取得する
		SearchBaseFilter baseFilter = new SearchBaseFilter();
		baseFilter.baseIds = new List<Id>(targetBaseIds);
		List<AttShortTimeSettingBaseEntity> bases = searchBaseEntity(baseFilter, forUpdate);

		// ベースに履歴を追加するためMapに変換する
		Map<Id, AttShortTimeSettingBaseEntity> baseMap = new Map<Id, AttShortTimeSettingBaseEntity>();
		for (AttShortTimeSettingBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		// ベースエンティティに履歴を追加する
		for (AttShortTimeSettingHistoryEntity history : histories) {
			AttShortTimeSettingBaseEntity base = baseMap.get(history.baseId);
			base.addHistory(history);
		}

		return bases;
	}

	/** ベース用の検索フィルタ */
	public class SearchBaseFilter {
		/** 勤務パターンベースID */
		public List<Id> baseIds;
		/** コード(完全一致) */
		public Set<String> codes;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	private List<AttShortTimeSettingBaseEntity> searchBaseEntity(SearchBaseFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && BASE_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchBaseEntity ReturnCache key:' + cacheKey);
			List<AttShortTimeSettingBaseEntity> entities = new List<AttShortTimeSettingBaseEntity>();
			for (AttShortTimeWorkSettingBase__c sObj : (List<AttShortTimeWorkSettingBase__c>)BASE_CACHE.get(cacheKey)) {
				entities.add(createBaseEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchBaseEntity NoCache key:' + cacheKey);

		// ベース WHERE句
		List<String> baseWhereList = new List<String>();
		// ベースIDで検索
		List<Id> pIds;
		if (filter.baseIds != null) {
			pIds = filter.baseIds;
			baseWhereList.add('Id IN :pIds');
		}
		// コードで検索(完全一致)
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			baseWhereList.add(getFieldName(AttShortTimeWorkSettingBase__c.Code__c) + ' IN :pCodes');
		}
		String whereString = buildWhereString(baseWhereList);


		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// ベース項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 理由(ComTag)項目
		selectFieldList.addAll(GET_BASE_REASON_FIELD_NAME_LIST);

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + AttShortTimeWorkSettingBase__c.SObjectType.getDescribe().getName()
				+ whereString;
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(AttShortTimeWorkSettingBase__c.Code__c);
		}

		System.debug('TimeSettingRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttShortTimeWorkSettingBase__c> sObjs = (List<AttShortTimeWorkSettingBase__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttShortTimeWorkSettingBase__c.SObjectType);

		// SObjectからエンティティを作成
		List<AttShortTimeSettingBaseEntity> entities = new List<AttShortTimeSettingBaseEntity>();
		for (AttShortTimeWorkSettingBase__c sObj : sObjs) {
			entities.add(createBaseEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			BASE_CACHE.put(cacheKey, sObjs);
		}
		return entities;
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストのソート順
	 * @return 履歴エンティティの検索
	 */
	public List<AttShortTimeSettingHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, HistorySortOrder sortOrder) {
		// バインド変数が必要なため、クエリ作成処理をモジュール化していない

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter, sortOrder);
		if (isEnabledCache && !forUpdate && HISTORY_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity ReturnCache key:' + cacheKey);
			List<AttShortTimeSettingHistoryEntity> entities = new List<AttShortTimeSettingHistoryEntity>();
			for (AttShortTimeWorkSettingHistory__c sObj : (List<AttShortTimeWorkSettingHistory__c>)HISTORY_CACHE.get(cacheKey)) {
				entities.add(createHistoryEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity NoCache key:' + cacheKey);

		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 履歴項目
		selectFieldList.addAll(GET_HISTORY_FIELD_NAME_LIST);

		// WHERE句
		List<String> whereList = new List<String>();
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(AttShortTimeWorkSettingHistory__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(AttShortTimeWorkSettingHistory__c.ValidTo__c) + ' > :pTargetDate');
		}
		// 履歴IDで検索
		Set<Id> pHistoryIds;
		if(filter.historyIds != null){
			pHistoryIds = filter.historyIds;
			whereList.add(getFieldName(AttShortTimeWorkSettingHistory__c.Id) + ' IN :pHistoryIds');
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			whereList.add(getFieldName(AttShortTimeWorkSettingHistory__c.Removed__c) + ' = false');
		}
		// ベースIDで検索
		Set<Id> pBaseIds;
		if (filter.baseIds != null) {
			pBaseIds = filter.baseIds;
			whereList.add(getFieldName(HISTORY_BASE_R, AttShortTimeWorkSettingBase__c.Id) + ' IN :pBaseIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(HISTORY_BASE_R, AttShortTimeWorkSettingBase__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(HISTORY_BASE_R, AttShortTimeWorkSettingBase__c.Code__c) + ' IN :pCodes');
		}
		// 労働時間制で検索
		String pWorkSystem;
		if (filter.workSystem != null) {
			pWorkSystem = filter.workSystem.value;
			whereList.add(getFieldName(HISTORY_BASE_R, AttShortTimeWorkSettingBase__c.WorkSystem__c) + ' = :pWorkSystem');
		}

		// ORDER BY句
		String orderBy = ' ORDER BY ' + getFieldName(HISTORY_BASE_R, AttShortTimeWorkSettingBase__c.Code__c);
		if (sortOrder == HistorySortOrder.VALID_FROM_ASC) {
			orderBy += ', ' + getFieldName(AttShortTimeWorkSettingHistory__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == HistorySortOrder.VALID_FROM_DESC) {
			orderBy += ', ' + getFieldName(AttShortTimeWorkSettingHistory__c.ValidFrom__c) + ' DESC NULLS LAST';
		}

		// クエリ作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM AttShortTimeWorkSettingHistory__c' +
				+ buildWhereString(whereList);
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
			soql += ' FOR UPDATE';
		} else {
			soql += orderBy;
			soql += ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		}
		System.debug('AttShortTimeSettingRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttShortTimeWorkSettingHistory__c> sObjs = (List<AttShortTimeWorkSettingHistory__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttShortTimeWorkSettingHistory__c.SObjectType);
		List<AttShortTimeSettingHistoryEntity> entities = new List<AttShortTimeSettingHistoryEntity>();
		for (AttShortTimeWorkSettingHistory__c sObj : sObjs) {
			entities.add(createHistoryEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			HISTORY_CACHE.put(cacheKey, sobjs);
		}
		return entities;
	}


	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AttShortTimeSettingBaseEntity createBaseEntity(AttShortTimeWorkSettingBase__c baseObj) {
		return new AttShortTimeSettingBaseEntity(baseObj);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AttShortTimeSettingHistoryEntity createHistoryEntity(AttShortTimeWorkSettingHistory__c historyObj) {
		return new AttShortTimeSettingHistoryEntity(historyObj);
	}

	/**
	 * ベースエンティティからオブジェクトを作成する（各リポジトリで実装する）
	 */
	protected override SObject createBaseObj(ParentChildBaseEntity superBaseEntity) {
		return ((AttShortTimeSettingBaseEntity)superBaseEntity).createSObject();
	}

	/**
	 * 履歴エンティティからオブジェクトを作成する
	 * @param historyEntity 作成元の履歴エンティティ
	 * @return 作成した履歴オブジェクト
	 */
	protected override SObject createHistoryObj(ParentChildHistoryEntity superHistoryEntity) {
		return ((AttShortTimeSettingHistoryEntity)superHistoryEntity).createSObject();
	}

	/**
	 * ベースオブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone) {
		List<AttShortTimeWorkSettingBase__c> deptList = new List<AttShortTimeWorkSettingBase__c>();
		for (SObject obj : baseObjList) {
			deptList.add((AttShortTimeWorkSettingBase__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * 履歴オブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone) {
		List<AttShortTimeWorkSettingHistory__c> deptList = new List<AttShortTimeWorkSettingHistory__c>();
		for (SObject obj : historyObjList) {
			deptList.add((AttShortTimeWorkSettingHistory__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}
}
