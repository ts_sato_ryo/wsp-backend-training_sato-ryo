/**
 * @group 勤怠
 *
 * @description 勤務パターン社員適用サービスのテスト
 */
@isTest
private class AttPatternEmployeeApplyingServiceTest {

	private static final ComMsgBase MESSAGE_L = ComMessage.msg();

	/**
	 * @description テストデータ
	 */
	private class SerivceTestData extends TestData.TestDataEntity {

		/**
		* @description コンストラクタ
		*/
		public SerivceTestData() {
			super();
			this.employee.getHistory(0).workingTypeId = this.workingType.id;
			new EmployeeRepository().saveHistoryEntity(this.employee.getHistory(0));
		}

		/**
		 * @description 勤務体系のテストデータを作成する
		 */
		public override AttWorkingTypeBaseEntity createWorkingType(String name) {
			AttWorkingTypeBaseEntity workingType = super.createWorkingType(name);
			// 勤務パターンを設定する
			workingType.getHistory(0).patternCodeList = new List<String>{this.workPattern.code};
			new AttWorkingTypeRepository().saveHistoryEntity(workingType.getHistory(0));
			return workingType;
		}

		/**
		 * @description 指定した会社の勤務体系を作成する
		 * @param companyId 会社ID
		 * @param name 勤務体系名
		 */
		public AttWorkingTypeBaseEntity createWorkingType(Id companyId, String name) {
			AttWorkingTypeBaseEntity workingType = super.createWorkingType(name);
			workingType.companyId = companyId;
			new AttWorkingTypeRepository().saveBaseEntity(workingType);
			return workingType;
		}

		/**
		 * @description 指定した会社の標準ユーザの社員のテストデータを作成する
		 * @param companyId 会社ID
		 * @param lastName 社員の姓
		 */
		public EmployeeBaseEntity createEmployeeWithStandardUser(Id companyId, String lastName) {
			EmployeeBaseEntity employee = createEmployeeWithStandardUser(lastName);
			employee.companyId = companyId;
			new EmployeeRepository().saveBaseEntity(employee);
			return employee;
		}

		/**
		 * @description 勤務パターン社員適用インポートデータを作成する
		 * @param recordNumber 作成するレコード数
		 * @return 作成したインポートデータ
		 */
		public List<AttPatternApplyImportEntity> createAttPatternApplyImportEntityList(Integer recordNumber) {
			Id importBatchId = ComTestDataUtility.createAttImportBatch('test', this.company.id, null, null).Id;
			List<AttPatternApplyImportEntity> entityList = new List<AttPatternApplyImportEntity>();
			for (Integer i = 0; i < recordNumber; i++) {
				AttPatternApplyImportEntity entity = new AttPatternApplyImportEntity();
				entity.importBatchId = importBatchId;
				entity.employeeCode = this.employee.code;
				entity.patternCode = this.workPattern.code;
				entity.shiftDate = AppDate.today().format();
				entity.dayType = 'W';
				entity.importNo = i + 1;
				entityList.add(entity);
			}
			Repository.SaveResult res = new AttPatternApplyImportRepository().saveEntityList(entityList);
			for (Integer i = 0; i < recordNumber; i++) {
				entityList[i].setId(res.details[i].id);
			}
			return entityList;
		}
	}

	/**
	 * @description parseDateForApplyingのテスト
	 * 勤務パターン適用インポートの日付のパースが正しくできることを確認する
	 */
	@isTest static void parseDateForApplyingTest() {
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AppDate resultDate;

		// Test1 YYYY-MM-DDの場合 → OK
		System.assertEquals(AppDate.newInstance(2018, 1, 3), service.parseDateForApplying('2018-01-03'));

		// Test2 ユーザのロケールの日付の場合 → OK
		User jpUser = ComTestDataUtility.createUser('jp-user', 'ja', 'ja_JP');
		System.runAs(jpUser) {
			System.assertEquals(AppDate.newInstance(2018, 1, 3), service.parseDateForApplying('2018/01/03'));
		}

		// Test3 nullの場合はnullが返却
		System.assertEquals(null, service.parseDateForApplying(null));

		// Test4 サポート外の書式を指定された場合は例外が発生する
		Exception actEx;
		try {
			service.parseDateForApplying('20190103');
		} catch (Exception e) {
			actEx = e;
		}
		System.assertNotEquals(null, actEx);
	}

	/**
	 * @description parseDayTypeのテスト
	 * 日タイプがAttDayTypeに正しく変換できることを確認する
	 */
	@isTest static void parseDayTypeTest() {
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AppDate resultDate;

		// Test1 インポートの日タイプ文字列が正しく変換されている
		System.assertEquals(AttDayType.WORKDAY, service.parseDayType('W'));
		System.assertEquals(AttDayType.HOLIDAY, service.parseDayType('H'));
		System.assertEquals(AttDayType.LEGAL_HOLIDAY, service.parseDayType('L'));

		// Test2 nullの場合はnullが返却される
		System.assertEquals(null, service.parseDayType(null));

		// Test3 大文字小文字を区別していない
		System.assertEquals(AttDayType.HOLIDAY, service.parseDayType('h'));
	}

	/**
	 * @description
	 * 勤務パターン適用インポート情報の検証が正しく行われていることを確認する
	 */
	@isTest static void validateImportTest() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(10);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;
		String expMessage;

		// すべての値が正しく設定されている
		targetApplyImport = applyImportList[0];
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(true, result.isSuccess, result);

		// 社員コードが設定されていない場合はエラー
		targetApplyImport = applyImportList[1];
		targetApplyImport.employeeCode = null;
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		expMessage = MESSAGE_L.Com_Err_NotSpecified(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyEmployeeCode});
		System.assertEquals(expMessage, result.errorList[0].message);

		// 適用対象日が設定されていない場合はエラー
		targetApplyImport = applyImportList[2];
		targetApplyImport.shiftDate = null;
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		expMessage = MESSAGE_L.Com_Err_NotSpecified(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyDate});
		System.assertEquals(expMessage, result.errorList[0].message);

		// 適用対象日書式が正しくない場合はエラー
		targetApplyImport = applyImportList[3];
		targetApplyImport.shiftDate = '20181201';
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		expMessage = MESSAGE_L.Com_Err_InvalidFormat(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyDate});
		System.assertEquals(expMessage, result.errorList[0].message);

		// 日タイプが正しくない場合はエラー
		targetApplyImport = applyImportList[4];
		targetApplyImport.dayType = 'A';
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		expMessage = MESSAGE_L.Com_Err_SpecifiedInvalid(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyDayType});
		System.assertEquals(expMessage, result.errorList[0].message);

		// 勤務パターンコードが指定されている場合で日タイプが設定されていない場合はエラー
		targetApplyImport = applyImportList[5];
		targetApplyImport.dayType = '';
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		expMessage = MESSAGE_L.Att_Err_PatternApplyRequiedWorkDay;
		System.assertEquals(expMessage, result.errorList[0].message);

		// 勤務パターンコードが指定されている場合で日タイプが休日の場合はエラー
		targetApplyImport = applyImportList[6];
		targetApplyImport.dayType = 'H';
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		expMessage = MESSAGE_L.Att_Err_PatternApplyRequiedWorkDay;
		System.assertEquals(expMessage, result.errorList[0].message);

		// 勤務パターンコードが指定されている場合で日タイプが法定休日の場合はエラー
		targetApplyImport = applyImportList[7];
		targetApplyImport.dayType = 'L';
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		expMessage = MESSAGE_L.Att_Err_PatternApplyRequiedWorkDay;
		System.assertEquals(expMessage, result.errorList[0].message);

		// 複数のエラーを検出することができる
		targetApplyImport = applyImportList[8];
		targetApplyImport.employeeCode = null;
		targetApplyImport.dayType = 'TEST';
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		System.assertEquals(2, result.errorList.size(), result);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートに他の会社の社員の社員コードを指定した場合、エラーとなることを確認する
	 */
	@isTest static void validateImportTestEmployeeNotFound() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.createCompany('Test2').id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Att_Err_SpecifiedNotFound(new List<String>{MESSAGE_L.Com_Lbl_Employee});
		System.assertEquals(expMessage, result.errorList[0].message);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートに対象日に失効している社員が指定された場合はエラーになることを確認する
	 */
	@isTest static void validateImportTestEmployeeExpired() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		targetApplyImport.shiftDate = testData.employee.getHistory(0).validTo.format();
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Com_Err_SpecifiedOutOfValidPeirod(new List<String>{MESSAGE_L.Com_Lbl_Employee});
		System.assertEquals(expMessage, result.errorList[0].message);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートに他の会社の勤務パターンのコードを指定した場合、エラーとなることを確認する
	 */
	@isTest static void validateImportTestPatternNotFound() {
		SerivceTestData testData = new SerivceTestData();
		Id test2CompanyId = testData.createCompany('Test2').id;
		AttWorkingTypeBaseEntity test2WorkingType = testData.createWorkingType(test2CompanyId, 'Test2');
		EmployeeBaseEntity test2Employee = testData.createEmployeeWithStandardUser(test2CompanyId, 'Test2');
		test2Employee.getHistory(0).workingTypeId = test2WorkingType.id;
		new EmployeeRepository().saveHistoryEntity(test2Employee.getHistory(0));

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		targetApplyImport.employeeCode = test2Employee.code;
		result = service.applyPatternToEmployee(targetApplyImport, test2CompanyId);
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Att_Err_SpecifiedNotFound(new List<String>{MESSAGE_L.Att_Lbl_WorkingPattern});
		System.assertEquals(expMessage, result.errorList[0].message);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートに対象日に失効している社員が指定された場合はエラーになることを確認する
	 */
	@isTest static void validateImportTestPatternExpired() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.company.id;

		// 勤務パターンの有効期間を社員と異なるようにしておく
		testData.workPattern.validFrom = testData.employee.getHistory(0).validTo;
		testData.workPattern.validTo = testData.workPattern.validFrom.addMonths(1);
		new AttPatternRepository2().saveEntity(testData.workPattern);

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		targetApplyImport.shiftDate = testData.employee.getHistory(0).validFrom.format();
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Com_Err_SpecifiedOutOfValidPeirod(new List<String>{MESSAGE_L.Att_Lbl_WorkingPattern});
		System.assertEquals(expMessage, result.errorList[0].message);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートの社員に勤務体系が設定されていない場合、エラーとなることを確認する
	 */
	@isTest static void validateImportTestEmployeeWorkingTypeIsNull() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.company.id;

		// 社員の勤務体系を削除する
		testData.employee.getHistory(0).workingTypeId = null;
		new EmployeeRepository().saveHistoryEntity(testData.employee.getHistory(0));

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Com_Err_SpecifiedNotSet(
					new List<String>{MESSAGE_L.Com_Lbl_Employee, MESSAGE_L.Att_Lbl_WorkingType});
		System.assertEquals(expMessage, result.errorList[0].message);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートの社員の勤務体系が対象日に失効している場合はエラーになることを確認する
	 */
	@isTest static void validateImportTestWorkingTypeExpired() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.company.id;

		// 勤務体系の有効期間を社員と異なるようにしておく
		testData.workingType.getHistory(0).validFrom = testData.employee.getHistory(0).validTo;
		testData.workingType.getHistory(0).validTo = testData.workingType.getHistory(0).validFrom.addMonths(1);
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		targetApplyImport.shiftDate = testData.employee.getHistory(0).validFrom.format();
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Com_Err_SpecifiedOutOfValidPeirodAofB(
					new List<String>{MESSAGE_L.Com_Lbl_Employee, MESSAGE_L.Att_Lbl_WorkingType});
		System.assertEquals(expMessage, result.errorList[0].message);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートの社員の勤務体系で指定した勤務パターンが許可されていない場合はエラーになることを確認する
	 */
	@isTest static void validateImportTestNotAvailablePattern() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.company.id;

		// 勤務体系の勤務パターンリストを空にする
		testData.workingType.getHistory(0).patternCodeList = new List<String>();
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		targetApplyImport.shiftDate = testData.employee.getHistory(0).validFrom.format();
		result = service.applyPatternToEmployee(targetApplyImport, companyId);
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Att_Err_SpecifiedPatternNotPermitted;
		System.assertEquals(expMessage, result.errorList[0].message);
	}

	/**
	 * @description
	 * 勤務パターン適用インポートの情報に基づいて社員の勤怠明細に勤務パターンと日タイプが設定されることを確認する
	 */
	@isTest static void validateImportTestSettingRecord() {
		SerivceTestData testData = new SerivceTestData();
		Id companyId = testData.company.id;

		List<AttPatternApplyImportEntity> applyImportList = testData.createAttPatternApplyImportEntityList(1);
		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result;
		AttPatternApplyImportEntity targetApplyImport;

		targetApplyImport = applyImportList[0];
		result = service.applyPatternToEmployee(targetApplyImport, companyId);

		// 検証
		System.assertEquals(true, result.isSuccess, result);
		AppDate targetDate = AppDate.valueOf(targetApplyImport.shiftDate);
		List<AttRecordEntity> resRecordList =
				new AttSummaryRepository().searchRecordEntityList(testData.employee.id, targetDate, targetDate);
		System.assertEquals(false, resRecordList.isEmpty());
		AttRecordEntity resRecord = resRecordList[0];
		System.assertEquals(testData.workPattern.id, resRecord.patternId);
		System.assertEquals(AttDayType.WORKDAY, resRecord.dayType);
		AttSummaryEntity resSummary = new AttSummaryRepository().getEntity(resRecord.summaryId, false, false);
		// System.assertEquals(true, resSummary.isDirty);
	}

	/**
	 * @description
	 * 社員の勤怠明細の勤務パターンと日タイプをクリアできることを確認する
	 */
	@isTest static void validateImportTestClearRecord() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Modified);
		Id companyId = testData.company.Id;

		AttPatternEntity pattern = testData.createPattern(AttWorkSystem.JP_Modified, 'Test');
		pattern.validFrom = ValidPeriodEntity.VALID_FROM_MIN;
		pattern.validTo = ValidPeriodEntity.VALID_TO_MAX;
		new AttPatternRepository2().saveEntity(pattern);

		testData.workingType.getHistory(0).patternCodeList = new List<String>{pattern.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		AttImportBatch__c batchObj = ComTestDataUtility.createAttImportBatch('Test', companyId, null, null);
		AttPatternApplyImport__c targetApplyImportObj = ComTestDataUtility.createAttPatternApplyImport(batchObj.Id, testData.employee.code, pattern.code);
		AttPatternApplyImportEntity targetApplyImport = new AttPatternApplyImportEntity(targetApplyImportObj);
		targetApplyImport.patternCode = '';
		targetApplyImport.dayType = '';
		AppDate targetDate = AppDate.valueOf(targetApplyImport.shiftDate);

		// 勤怠サマリーと明細作成
		testData.createAttSummaryWithRecordsMonth(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id,
				AppDate.valueOf(targetApplyImport.shiftDate), false);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		List<AttSummaryEntity> summaryList = summaryRepo.searchEntity(testData.employee.id, targetDate);
		AttRecordEntity record = summaryRepo.getRecordEntity(summaryList[0].id, targetDate, false);
		record.dayType = AttDayType.WORKDAY;
		record.patternId = pattern.Id;
		summaryRepo.saveRecordEntity(record);

		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result
				= service.applyPatternToEmployee(targetApplyImport, companyId);

		// 検証
		System.assertEquals(true, result.isSuccess, result);
		List<AttRecordEntity> resRecordList = summaryRepo.searchRecordEntityList(testData.employee.id, targetDate, targetDate);
		System.assertEquals(false, resRecordList.isEmpty());
		AttRecordEntity resRecord = resRecordList[0];
		System.assertEquals(null, resRecord.patternId);
		System.assertEquals(null, resRecord.dayType);
	}

	/**
	 * @description
	 * 社員の勤怠明細の勤務パターンと日タイプをクリアする際に勤怠明細が検証されていることを確認する
	 */
	@isTest static void validateImportTestValidateClearRecord() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Modified);
		Id companyId = testData.company.Id;

		AttPatternEntity pattern = testData.createPattern(AttWorkSystem.JP_Modified, 'Test');
		pattern.validFrom = ValidPeriodEntity.VALID_FROM_MIN;
		pattern.validTo = ValidPeriodEntity.VALID_TO_MAX;
		new AttPatternRepository2().saveEntity(pattern);

		testData.workingType.getHistory(0).patternCodeList = new List<String>{pattern.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		AppDate targetDate = AppDate.newInstance(2019, 1, 16);

		AttImportBatch__c batchObj = ComTestDataUtility.createAttImportBatch('Test', companyId, null, null);
		AttPatternApplyImport__c targetApplyImportObj = ComTestDataUtility.createAttPatternApplyImport(batchObj.Id, testData.employee.code, pattern.code);
		AttPatternApplyImportEntity targetApplyImport = new AttPatternApplyImportEntity(targetApplyImportObj);
		targetApplyImport.patternCode = '';
		targetApplyImport.dayType = '';
		targetApplyImport.shiftDate = targetDate.format();

		// 休日出勤申請を作成
		AttDailyRequestEntity holidayReq = testData.createHolidayWorkRequest(AttTime.newInstance(9, 00).getIntValue(), AttTime.newInstance(18,00).getIntValue());

		// 勤怠サマリーと明細作成
		testData.createAttSummaryWithRecordsMonth(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id,
				targetDate, false);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		List<AttSummaryEntity> summaryList = summaryRepo.searchEntity(testData.employee.id, targetDate);
		AttRecordEntity record = summaryRepo.getRecordEntity(summaryList[0].id, targetDate, false);
		record.dayType = AttDayType.HOLIDAY;
		// record.patternId = pattern.Id;
		record.reqRequestingHolidayWorkRequestId = holidayReq.id;
		summaryRepo.saveRecordEntity(record);

		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result
				= service.applyPatternToEmployee(targetApplyImport, companyId);

		// 検証
		// 元が勤務日のため、クリアできないはず
		System.assertEquals(false, result.isSuccess, result);
		List<AttRecordEntity> resRecordList = summaryRepo.searchRecordEntityList(testData.employee.id, targetDate, targetDate);
		System.assertEquals(false, resRecordList.isEmpty());
		AttRecordEntity resRecord = resRecordList[0];
		System.assertEquals(record.patternId, resRecord.patternId);
		System.assertEquals(record.dayType, resRecord.dayType);
	}

	/**
	 * @description
	 * 勤務パターンが適用可能であることを検証していることを確認する(バリデータの呼び出しチェック)
	 */
	@isTest static void validateImportTestLockedSummary() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Modified);
		Id companyId = testData.company.Id;

		AttPatternEntity pattern = testData.createPattern(AttWorkSystem.JP_Modified, 'Test');
		pattern.validFrom = ValidPeriodEntity.VALID_FROM_MIN;
		pattern.validTo = ValidPeriodEntity.VALID_TO_MAX;
		new AttPatternRepository2().saveEntity(pattern);

		testData.workingType.getHistory(0).patternCodeList = new List<String>{pattern.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		AttImportBatch__c batchObj = ComTestDataUtility.createAttImportBatch('Test', companyId, null, null);
		AttPatternApplyImport__c targetApplyImportObj = ComTestDataUtility.createAttPatternApplyImport(batchObj.Id, testData.employee.code, pattern.code);
		AttPatternApplyImportEntity targetApplyImport = new AttPatternApplyImportEntity(targetApplyImportObj);

		// 勤怠サマリーをロックしておく
		testData.createAttSummaryWithRecordsMonth(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id,
				AppDate.valueOf(targetApplyImport.shiftDate), false);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		List<AttSummaryEntity> summaryList = summaryRepo.searchEntity(testData.employee.id, AppDate.valueOf(targetApplyImport.shiftDate));
		summaryList[0].isLocked = true;
		summaryRepo.saveEntity(summaryList[0]);

		AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
		AttPatternEmployeeApplyingService.Result result = service.applyPatternToEmployee(targetApplyImport, companyId);

		// 検証
		System.assertEquals(false, result.isSuccess, result);
		String expMessage = MESSAGE_L.Att_Err_CannotApplyLocked;
		System.assertEquals(expMessage, result.errorList[0].message);
	}
}