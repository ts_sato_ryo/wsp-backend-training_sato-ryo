/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 勤怠期間リソースクラス
 */
public with sharing class AttPeriodStatusResource {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * 短時間勤務設定
	 */
	public class ShortTimeSettingParam {
		/** 短時間勤務設定名 */
		public String name;
	}

	/**
	 * 休職休業
	 */
	public class LeaveOfAbsenceParam {
		/** 休職休業名 */
		public String name;
	}

	/**
	 * 勤怠期間パラメータ
	 */
	public abstract class PeriodStatusParam implements RemoteApi.RequestParam {
		/** 勤怠期間パラメータID */
		public String id;
		/** 社員ベースID */
		public String employeeId;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** コメント */
		public String comment;

		/**
		 * パラメータの値を設定した勤怠期間エンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public virtual AttPeriodStatusEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {

			AttPeriodStatusEntity entity = new AttPeriodStatusEntity();

			// レコード更新時はIDをセット
			if (isUpdate) {
				entity.setId(this.id);
			}
			if (paramMap.containsKey('employeeId')) {
				entity.employeeBaseId = this.employeeId;
			}
			if (paramMap.containsKey('validDateFrom')) {
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (paramMap.containsKey('validDateTo')) {
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (paramMap.containsKey('comment')) {
				entity.comment = this.comment;
			}

			return entity;
		}
	}

	/**
	 * 休職休業の勤怠期間パラメータ
	 */
	public class PeriodStatusLeaveOfAbsenceParam
			extends PeriodStatusParam implements RemoteApi.RequestParam {
		/** 休職休業ID */
		public String leaveOfAbsenceId;
		/** 休職休業名(参照専用) */
		public LeaveOfAbsenceParam leaveOfAbsence;

		/**
		 * パラメータの値を設定した勤怠期間エンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public override AttPeriodStatusEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {

			AttPeriodStatusEntity entity = super.createEntity(paramMap, isUpdate);

			entity.type = AttPeriodStatusType.ABSENCE;

			if (paramMap.containsKey('leaveOfAbsenceId')) {
				entity.leaveOfAbsenceId = this.leaveOfAbsenceId;
			}

			return entity;
		}

		/**
		 * パラメータを検証する(新規作成API用)
		 */
		public void validateForNew() {

			// 社員ID
			if (String.isNotBlank(this.employeeId)) {
				try {
					System.Id.valueOf(this.employeeId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'employeeId'}));
				}
			}

			// 短時間勤務設定ID
			if (String.isNotBlank(this.leaveOfAbsenceId)) {
				try {
					System.Id.valueOf(this.leaveOfAbsenceId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'leaveOfAbsenceId'}));
				}
			}
		}

		/**
		 * パラメータを検証する(更新API用)
		 */
		private void validateForUpdate() {

			// ID
			if (String.isBlank(this.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('id');
				throw ex;
			}
			try {
				System.Id.valueOf(this.id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}

			validateForNew();
		}
	}

	/**
	 * 短時間勤務設定用の勤怠期間パラメータ
	 */
	public class PeriodStatusShortSettingParam
			extends PeriodStatusParam implements RemoteApi.RequestParam {

		/** 短時間勤務設定ベースID */
		public String shortTimeWorkSettingId;
		/** 短時間勤務設定名(参照専用) */
		public ShortTimeSettingParam shortTimeWorkSetting;

		/**
		 * パラメータの値を設定した勤怠期間エンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public override AttPeriodStatusEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {

			AttPeriodStatusEntity entity = super.createEntity(paramMap, isUpdate);

			entity.type = AttPeriodStatusType.SHORT_TIME_WORK_SETTING;

			if (paramMap.containsKey('shortTimeWorkSettingId')) {
				entity.shortTimeWorkSettingBaseId = this.shortTimeWorkSettingId;
			}

			return entity;
		}

		/**
		 * パラメータを検証する(新規作成API用)
		 */
		public void validateForNew() {

			// 社員ID
			if (String.isNotBlank(this.employeeId)) {
				try {
					System.Id.valueOf(this.employeeId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'employeeId'}));
				}
			}

			// 短時間勤務設定ID
			if (String.isNotBlank(this.shortTimeWorkSettingId)) {
				try {
					System.Id.valueOf(this.shortTimeWorkSettingId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'shortTimeWorkSettingId'}));
				}
			}
		}

		/**
		 * パラメータを検証する(更新API用)
		 */
		private void validateForUpdate() {

			// ID
			if (String.isBlank(this.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('id');
				throw ex;
			}
			try {
				System.Id.valueOf(this.id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}

			validateForNew();
		}
	}

	/**
	 * @description 短時間勤務設定適用APIを実装するクラス
	 */
	public with sharing class ShortTimeSettingCreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY;

		/**
		 * @description 短時間勤務設定用の勤怠期間レコードを作成する
		 * @param req リクエストパラメータ(PeriodStatusShortSettingParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			PeriodStatusShortSettingParam param =
					(PeriodStatusShortSettingParam)req.getParam(PeriodStatusShortSettingParam.class);

			param.validateForNew();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 勤怠期間エンティティを作成する
			final Boolean isUpdate = false;
			AttPeriodStatusEntity periodStatus = param.createEntity(req.getParamMap(), isUpdate);

			Savepoint sp = Database.setSavepoint();
			try {
				AttPeriodStatusService service = new AttPeriodStatusService();

				// 勤怠期間エンティティのバリデーションを実行する
				service.validatePeriodStatus(periodStatus);

				// 休職休業が適用可能か確認する
				service.validateShortTimeSetting(periodStatus);

				// 保存する
				service.savePeriodStatus(periodStatus);
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}

	/**
	 * @description 休職休業適用APIを実装するクラス
	 */
	public with sharing class LeaveOfAbsenceCreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY;

		/**
		 * @description 休職休業用の勤怠期間レコードを作成する
		 * @param req リクエストパラメータ(PeriodStatusLeaveOfAbsenceParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			PeriodStatusLeaveOfAbsenceParam param =
					(PeriodStatusLeaveOfAbsenceParam)req.getParam(PeriodStatusLeaveOfAbsenceParam.class);

			param.validateForNew();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 勤怠期間エンティティを作成する
			final Boolean isUpdate = false;
			AttPeriodStatusEntity periodStatus = param.createEntity(req.getParamMap(), isUpdate);

			Savepoint sp = Database.setSavepoint();
			try {
				AttPeriodStatusService service = new AttPeriodStatusService();

				// 勤怠期間エンティティのバリデーションを実行する
				service.validatePeriodStatus(periodStatus);

				// 休職休業が適用可能か確認する
				service.validateLeaveOfAbsence(periodStatus);

				// 保存する
				service.savePeriodStatus(periodStatus);
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}

	/**
	 * @description 短時間勤務設定適用更新APIを実装するクラス
	 */
	public with sharing class ShortTimeSettingUpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY;

		/**
		 * @description 短時間勤務設定用の勤怠期間レコードを作成する
		 * @param req リクエストパラメータ(PeriodStatusShortSettingParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			PeriodStatusShortSettingParam param =
					(PeriodStatusShortSettingParam)req.getParam(PeriodStatusShortSettingParam.class);

			param.validateForUpdate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 勤怠期間エンティティを作成する
			final Boolean isUpdate = true;
			AttPeriodStatusEntity periodStatus = param.createEntity(req.getParamMap(), isUpdate);

			Savepoint sp = Database.setSavepoint();
			try {
				AttPeriodStatusService service = new AttPeriodStatusService();

				// 勤怠期間エンティティのバリデーションを実行する
				service.validatePeriodStatus(periodStatus);

				// 短時間勤務設定が適用可能か確認する
				service.validateShortTimeSetting(periodStatus);

				// 保存する
				service.savePeriodStatus(periodStatus);

			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}

	/**
	 * @description 休職休業適用更新APIを実装するクラス
	 */
	public with sharing class LeaveOfAbsenceUpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY;

		/**
		 * @description 休職休業用の勤怠期間レコードを作成する
		 * @param req リクエストパラメータ(PeriodStatusLeaveOfAbsenceParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			PeriodStatusLeaveOfAbsenceParam param =
					(PeriodStatusLeaveOfAbsenceParam)req.getParam(PeriodStatusLeaveOfAbsenceParam.class);

			param.validateForUpdate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 勤怠期間エンティティを作成する
			final Boolean isUpdate = true;
			AttPeriodStatusEntity periodStatus = param.createEntity(req.getParamMap(), isUpdate);

			Savepoint sp = Database.setSavepoint();
			try {
				AttPeriodStatusService service = new AttPeriodStatusService();

				// 勤怠期間エンティティのバリデーションを実行する
				service.validatePeriodStatus(periodStatus);

				// 休職休業が適用可能か確認する
				service.validateLeaveOfAbsence(periodStatus);

				// 保存する
				service.savePeriodStatus(periodStatus);

			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}

	/**
	 * @description 勤怠期間適用を削除するAPIのリクエストパラメータ
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 勤怠期間ID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 勤怠期間ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_NotSetValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}
	}

	/**
	 * @description 短時間勤務設定適用削除APIを実装するクラス
	 */
	public with sharing class ShortTimeSettingDeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY;

		/**
		 * @description 短時間勤務設定用の勤怠期間レコードを削除する
		 * @param req リクエストパラメータ(DeleteRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);

			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			Savepoint sp = Database.setSavepoint();
			try {
				AttPeriodStatusService service = new AttPeriodStatusService();

				try {
					// 短時間勤務設定の適用が削除可能か確認する
					service.validateDeletableShortTimeSetting(param.id);

					// 削除する
					service.deletePeriodStatus(param.id);

				} catch (App.RecordNotFoundException e) {
					// すでに削除済みの場合は処理成功とみなす(API仕様)

				} catch (DmlException e) {
					// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
					if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
						System.debug(e.getMessage());
						throw e;
					}
				}
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}
			return null;
		}
	}

	/**
	 * @description 休職休業適用削除APIを実装するクラス
	 */
	public with sharing class LeaveOfAbsenceDeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY;

		/**
		 * @description 休職休業の勤怠期間レコードを削除する
		 * @param req リクエストパラメータ(DeleteRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);

			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			Savepoint sp = Database.setSavepoint();
			try {
				AttPeriodStatusService service = new AttPeriodStatusService();

				try {
					// 休職休業の適用が削除可能か確認する
					service.validateDeletableLeaveOfAbsence(param.id);

					// 削除する
					service.deletePeriodStatus(param.id);

				} catch (App.RecordNotFoundException e) {
					// すでに削除済みの場合は処理成功とみなす(API仕様)

				} catch (DmlException e) {
					// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
					if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
						System.debug(e.getMessage());
						throw e;
					}
				}
			} catch (Exception e) {
				Database.rollback(sp);
				throw e;
			}
			return null;
		}
	}

	/**
	 * @description 短時間勤務設定の勤怠適用一覧を取得するAPIのリクエストパラメータ
	 */
	public class ShortTimeSettingListRequest implements RemoteApi.RequestParam {
		/** 社員ID */
		public String employeeId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 社員ID
			if (String.isBlank(this.employeeId)) {
				throw new App.ParameterException(MESSAGE.Com_Err_NotSetValue(new List<String>{'employeeId'}));
			}
			try {
				System.Id.valueOf(this.employeeId);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'employeeId'}));
			}
		}
	}

	/**
	 * @description 短時間勤務設定の勤怠適用一覧を取得するAPIのレスポンスパラメータ
	 */
	public class ShortTimeSettingListResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public PeriodStatusShortSettingParam[] records;
	}

	/**
	 * @description 短時間勤務設定適用APIを実装するクラス
	 */
	public with sharing class ShortTimeSettingListApi extends RemoteApi.ResourceBase {

		/**
		 * @description 短時間勤務設定用の勤怠期間レコードを検索する
		 * @param req リクエストパラメータ(ShortTimeSettingListRequest)
		 * @return レスポンス(ShortTimeSettingListResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			ShortTimeSettingListRequest param =
					(ShortTimeSettingListRequest)req.getParam(ShortTimeSettingListRequest.class);
			Map<String, Object> paramMap = req.getParamMap();

			// パラメータのバリデーション
			param.validate();

			// 検索フィルタ作成
			AttPeriodStatusRepository.SearchFilter filter =
					new AttPeriodStatusRepository.SearchFilter();
			filter.periodStatusTypes = new Set<AttPeriodStatusType>{AttPeriodStatusType.SHORT_TIME_WORK_SETTING};
			filter.employeeIds = new Set<Id>{param.employeeId};

			// 検索実行
			List<AttPeriodStatusEntity> entityList =
					(new AttPeriodStatusRepository()).searchEntityList(filter, false,
					AttPeriodStatusRepository.SortOrder.VALID_FROM_DESC);

			// 短時間勤務設定のIDの一覧を作成する
			Set<Id> shortTimeSettingIdSet = new Set<Id>();
			for(AttPeriodStatusEntity entity : entitylist) {
				if (entity.shortTimeWorkSettingBaseId != null) {
					shortTimeSettingIdSet.add(entity.shortTimeWorkSettingBaseId);
				}
			}
			List<AttShortTimeSettingBaseEntity> shortTimeSettingList =
				new AttShortTimeSettingRepository().getEntityList(new List<Id>(shortTimeSettingIdSet), null);
			Map<Id, AttShortTimeSettingBaseEntity> shortTimeSettingMap =
					new Map<Id, AttShortTimeSettingBaseEntity>();
			for (AttShortTimeSettingBaseEntity shortTimeSetting : shortTimeSettingList) {
				shortTimeSettingMap.put(shortTimeSetting.id, shortTimeSetting);
			}


			// レスポンスクラスに変換
			List<PeriodStatusShortSettingParam> records = new List<PeriodStatusShortSettingParam>();
			for(AttPeriodStatusEntity entity : entitylist) {
				records.add(createPeriodStatusShortSettingParam(entity, shortTimeSettingMap));
			}

			// 成功レスポンスをセットする
			ShortTimeSettingListResponse res = new ShortTimeSettingListResponse();
			res.records = records;
			return res;
		}

		/**
		 * 勤怠期間エンティティからレスポンスのパラメータを作成する
		 */
		private PeriodStatusShortSettingParam createPeriodStatusShortSettingParam(
				AttPeriodStatusEntity entity,
				Map<Id, AttShortTimeSettingBaseEntity> shortTimeSettingMap) {
			PeriodStatusShortSettingParam param = new PeriodStatusShortSettingParam();
			param.id = entity.id;
			param.employeeId = entity.employeeBaseId;
			param.shortTimeWorkSettingId = entity.shortTimeWorkSettingBaseId;
			param.shortTimeWorkSetting = new ShortTimeSettingParam();
			if (entity.shortTimeWorkSettingBaseId != null) {
				AttShortTimeSettingBaseEntity shortTimeSettingBase =
						shortTimeSettingMap.get(entity.shortTimeWorkSettingBaseId);
				param.shortTimeWorkSetting.name = getShortTimeSettingName(
						shortTimeSettingBase, entity.validFrom);
			}
			param.validDateFrom = AppConverter.dateValue(entity.validFrom);
			param.validDateTo = AppConverter.dateValue(entity.validTo);
			param.comment = entity.comment;

			// 短時間勤務設定名を取得する

			return param;
		}

		/**
		 * 短時間勤務設定名を取得する
		 */
		private String getShortTimeSettingName(
				AttShortTimeSettingBaseEntity shortTimeSettingBase, AppDate targetDate) {
			AttShortTimeSettingHistoryEntity shortTimeSettingHistory =
					(AttShortTimeSettingHistoryEntity)shortTimeSettingBase.getsuperHistoryByDate(targetDate);

			String name;
			if (shortTimeSettingHistory == null) {
				// 有効期間外の場合は近い履歴の名前を返却する
				AttShortTimeSettingHistoryEntity oldestHistory =
						(AttShortTimeSettingHistoryEntity)shortTimeSettingBase.getOldestSuperHistory();
				if (targetDate.getDate() < oldestHistory.validFrom.getDate()) {
					name = oldestHistory.nameL.getValue();
				} else {
					AttShortTimeSettingHistoryEntity latestHistory =
							(AttShortTimeSettingHistoryEntity)shortTimeSettingBase.getLatestSuperHistory();
					name = latestHistory.nameL.getValue();
				}
			} else {
				name = shortTimeSettingHistory.nameL.getValue();
			}

			return name;
		}
	}

	/**
	 * @description 休職休業の勤怠適用一覧を取得するAPIのリクエストパラメータ
	 */
	public class LeaveOfAbsenceListRequest implements RemoteApi.RequestParam {
		/** 社員ID */
		public String employeeId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// 社員ID
			if (String.isBlank(this.employeeId)) {
				throw new App.ParameterException(MESSAGE.Com_Err_NotSetValue(new List<String>{'employeeId'}));
			}
			try {
				System.Id.valueOf(this.employeeId);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'employeeId'}));
			}
		}
	}

	/**
	 * @description 休職休業の勤怠適用一覧を取得するAPIのレスポンスパラメータ
	 */
	public class LeaveOfAbsenceListResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public PeriodStatusLeaveOfAbsenceParam[] records;
	}

	/**
	 * @description 休職休業適用一覧取得APIを実装するクラス
	 */
	public with sharing class LeaveOfAbsenceListApi extends RemoteApi.ResourceBase {

		/**
		 * @description 休職休業の適用レコードを検索する
		 * @param req リクエストパラメータ(LeaveOfAbsenceListRequest)
		 * @return レスポンス(LeaveOfAbsenceListResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			LeaveOfAbsenceListRequest param =
					(LeaveOfAbsenceListRequest)req.getParam(LeaveOfAbsenceListRequest.class);
			Map<String, Object> paramMap = req.getParamMap();

			// パラメータのバリデーション
			param.validate();

			// 検索フィルタ作成
			AttPeriodStatusRepository.SearchFilter filter =
					new AttPeriodStatusRepository.SearchFilter();
			filter.periodStatusTypes = new Set<AttPeriodStatusType>{AttPeriodStatusType.ABSENCE};
			filter.employeeIds = new Set<Id>{param.employeeId};

			// 検索実行
			List<AttPeriodStatusEntity> entityList =
					(new AttPeriodStatusRepository()).searchEntityList(filter, false,
					AttPeriodStatusRepository.SortOrder.VALID_FROM_DESC);

			// レスポンスクラスに変換
			List<PeriodStatusLeaveOfAbsenceParam> records = new List<PeriodStatusLeaveOfAbsenceParam>();
			for(AttPeriodStatusEntity entity : entitylist) {
				records.add(createPeriodStatusLeaveOfAbsenceParam(entity));
			}

			// 成功レスポンスをセットする
			LeaveOfAbsenceListResponse res = new LeaveOfAbsenceListResponse();
			res.records = records;
			return res;
		}

		/**
		 * 勤怠期間エンティティからレスポンスのパラメータを作成する
		 */
		private PeriodStatusLeaveOfAbsenceParam createPeriodStatusLeaveOfAbsenceParam(
				AttPeriodStatusEntity entity) {
			PeriodStatusLeaveOfAbsenceParam param = new PeriodStatusLeaveOfAbsenceParam();
			param.id = entity.id;
			param.employeeId = entity.employeeBaseId;
			param.leaveOfAbsenceId = entity.leaveOfAbsenceId;
			param.leaveOfAbsence = new AttPeriodStatusResource.LeaveOfAbsenceParam();
			if (entity.leaveOfAbsenceId != null) {
				param.leaveOfAbsence.name = entity.leaveOfAbsence.nameL.getValue();
			}
			param.validDateFrom = AppConverter.dateValue(entity.validFrom);
			param.validDateTo = AppConverter.dateValue(entity.validTo);
			param.comment = entity.comment;

			return param;
		}
	}
}
