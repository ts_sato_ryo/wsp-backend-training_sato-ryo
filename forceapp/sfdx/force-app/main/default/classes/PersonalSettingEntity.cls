/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 個人設定のエンティティ
 */
public with sharing class PersonalSettingEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		Name,
		EmployeeBaseId,
		TimeInputSetting,
		DefaultJobId,
		ApproverBase01Id,
		SearchConditionList
	}

	public Decimal MAX_SEARCH_CONDITION_STRING_SIZE = 10000;

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ
	 */
	public PersonalSettingEntity() {
		isChangedFieldSet = new Set<Field>();
		timeInputSetting = new TimeInputSetting();
		searchConditionList = new List<SearchCondition>();
	}

	/** 個人設定名 */
	public String name {
		get;
		set {
			if (String.isBlank(value)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('name');
				throw ex;
			}
			name = value;
			setChanged(Field.Name);
		}
	}

	/** 社員ベースID */
	public Id employeeBaseId {
		get;
		set {
			employeeBaseId = value;
			setChanged(Field.employeeBaseId);
		}
	}

	/** 工数実績入力設定 */
	public TimeInputSetting timeInputSetting {
		get {
			// TODO TimeInputSettingのクラス内で変更を判定する方法を検討する
			setChanged(Field.TimeInputSetting);
			return timeInputSetting;
		}
		set {
			timeInputSetting = value;
			setChanged(Field.TimeInputSetting);
		}
	}

	/** デフォルトジョブID */
	public Id defaultJobId {
		get;
		set {
			defaultJobId = value;
			setChanged(Field.DefaultJobId);
		}
	}
	/** 承認者 */
	public Id approverBase01Id {
		get;
		set {
			approverBase01Id = value;
			setChanged(Field.ApproverBase01Id);
		}
	}

	/** search setting */
	public List<SearchCondition> searchConditionList {
		get;
		set {
			searchConditionList = value;
			setChanged(Field.SearchConditionList);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}

	/** search condition of expense finance approval */
	public class SearchCondition {
		public String name;
		public Set<String> departmentBaseIds;
		public Set<String> empBaseIds;
		public Set<String> financeStatusList;
		public ExpReportRequestRepository.DateRange requestDateRange;
		public ExpReportRequestRepository.DateRange accountingDateRange;
		public String reportNo;
		public ExpReportRequestRepository.AmountRange amountRange;
		public Set<String> detail;

		/**
		 * コンストラクタ
		 */
		public SearchCondition() {
			departmentBaseIds = new Set<String>();
			empBaseIds = new Set<String>();
			financeStatusList = new Set<String>();
			requestDateRange = new ExpReportRequestRepository.DateRange();
			accountingDateRange = new ExpReportRequestRepository.DateRange();
			amountRange = new ExpReportRequestRepository.AmountRange();
			detail = new Set<String>();

		}
	}

	/**
	 * 工数実績入力設定のJson項目を保持するクラス
	 */
	public class TimeInputSetting {
		public List<Task> taskList = new List<Task>();

		/**
		 * タスクを1件追加する
		 * @param jobCode ジョブコード
		 * @param WorkCategoryCode 作業分類コード
		 */
		public void addTask(String jobCode, String workCategoryCode) {
			Task task = new Task();
			task.jobCode = jobCode;
			task.workCategoryCode = workCategoryCode;
			taskList.add(task);
		}
	}

	/**
	 * タスク1件を保持するクラス
	 */
	public class Task {
		/** ジョブコード */
		public String jobCode;
		/** 作業分類コード */
		public String workCategoryCode;
	}
}