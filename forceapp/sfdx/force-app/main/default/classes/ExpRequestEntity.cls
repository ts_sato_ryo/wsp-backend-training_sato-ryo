/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 *  Entity of Expense Request
 */
public with sharing class ExpRequestEntity extends ExpReport {

	/** Max length of subject */
	public static final Integer MAX_SUBJECT_LENGTH = 80;
	/** Max length of purpose */
	public static final Integer MAX_PURPOSE_LENGTH = 255;
	/** Default Request No. value */
	public static final String DEFAULT_REQUEST_NO = '00000000';

	/** Object of approval request info */
	public class Request extends ExpReport.Request {
		/** Claimed */
		public Boolean claimed {public get; private set;}

		/** コンストラクタ Constructor */
		public Request(AppRequestStatus status, Boolean claimed, AppCancelType cancelType, Boolean isConfirmationRequired, AppDatetime requestTime) {
			this.status = status;
			this.claimed = claimed;
			this.cancelType = cancelType;
			this.isConfirmationRequired = isConfirmationRequired;
			this.requestTime = requestTime;
		}

		/**
 		 * Return whether
 		 * @param compare target object to compare
 		 */
 		public override Boolean equals(Object compare) {
			Boolean ret;
			if (compare instanceof ExpRequestEntity.Request) {
				ExpRequestEntity.Request compareRequest = (ExpRequestEntity.Request)compare;
				if ((this.status != compareRequest.status)
						|| (this.cancelType != compareRequest.cancelType)
						|| (this.isConfirmationRequired != compareRequest.isConfirmationRequired)
						|| (this.requestTime != compareRequest.requestTime)) {
					ret = false;
				} else {
					ret = true;
				}
			} else {
				ret = false;
			}
			return ret;
		}
	}

	/** Fields definition */
	public enum Field {
		EXP_REQUEST_APPROVAL_ID,
		PURPOSE,
		RECORD_LIST,
		RECORDING_DATE,
		REQUEST_NO,
		SCHEDULED_DATE
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;

	/**
	 * Constructor
	 */
	public ExpRequestEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** Approval request ID */
	public Id expRequestApprovalId {
		get;
		set {
			expRequestApprovalId = value;
			setChanged(Field.EXP_REQUEST_APPROVAL_ID);
		}
	}

	/** Approval request info */
	public Request expRequestApproval {get; set;}

	/** Remarks */
	public String purpose {
		get;
		set {
			purpose = value;
			setChanged(Field.PURPOSE);
		}
	}

	/** Recording Date */
	public AppDate recordingDate {
		get;
		set {
			recordingDate = value;
			setChanged(Field.RECORDING_DATE);
		}
	}

	/** Request No */
	public String requestNo {
		get;
		set {
			requestNo = value;
			setChanged(Field.REQUEST_NO);
		}
	}

	/** Scheduled Date */
	public AppDate scheduledDate {
		get;
		set {
			scheduledDate = value;
			setChanged(Field.SCHEDULED_DATE);
		}
	}

	/** Request record list */
	public List<ExpRequestRecordEntity> recordList {get; private set;}


	/**
	 * Replace record list(duppulicate list)
	 * @param RECORD_LIST target
	 */
	public void replaceRecordList(List<ExpRequestRecordEntity> sourceList) {
		this.recordList = new List<ExpRequestRecordEntity>(sourceList);
		setChanged(Field.RECORD_LIST);
	}

	/**
	 * Mark field as changed
	 * @param Field Changed field
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
		// Reset record list as well
		if (recordList != null) {
			for (ExpRequestRecordEntity entity : recordList) {
				entity.resetChanged();
			}
		}
	}

	/**
	 * Get detail status of the request
	 */
	public ExpRequestApprovalEntity.DetailStatus getRequestDetailStatus() {
		ExpRequestApprovalEntity requestEntity = new ExpRequestApprovalEntity();

		requestEntity.setId(this.expRequestApprovalId);
		if (this.expRequestApproval != null) {
			requestEntity.status = this.expRequestApproval.status;
			requestEntity.claimed = this.expRequestApproval.claimed;
			requestEntity.cancelType = this.expRequestApproval.cancelType;
		}

		return requestEntity.getDetailStatus();
	}

	/*
	* Return the field value
	* @param name Target field name
	*/
	public override Object getValue(String name) {
		Field field = ExpRequestEntity.FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

		return super.getValue(name);
	}

	public Object getValue(ExpRequestEntity.Field field) {
		if (field == ExpRequestEntity.Field.EXP_REQUEST_APPROVAL_ID) {
			return this.expRequestApprovalId;
		} else if (field == ExpRequestEntity.Field.RECORDING_DATE) {
			return this.recordingDate;
		} else if (field == ExpRequestEntity.Field.SCHEDULED_DATE) {
			return this.scheduledDate;
		} else if (field == ExpRequestEntity.Field.PURPOSE) {
			return this.purpose;
		} else if (field == ExpRequestEntity.Field.REQUEST_NO) {
			return this.requestNo;
		}
		
		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}