/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttShortTimeSettingBaseEntityテスト
 */
@isTest
private class AttShortTimeSettingBaseEntityTest {

	/**
	 * テストデータ
	 */
	private class EntityTestData extends TestData.TestDataEntity {

		public AttShortTimeSettingBaseEntity createAttPatternBase(String name) {
			AttShortTimeSettingBaseEntity base = new AttShortTimeSettingBaseEntity();
			base.name = name;
			base.code = name + '_code';
			base.uniqkey = this.company.code + '-' + name;
			base.companyId = this.company.id;
			base.reasonId = this.tag.id;
			base.workSystem = AttWorkSystem.JP_Fix;

			return base;
		}
	}

	/**
	 * エンティティの複製ができることを確認する
	 */
	@isTest static void copyTest() {
		AttShortTimeSettingBaseEntityTest.EntityTestData testData = new EntityTestData();

		AttShortTimeSettingBaseEntity orgEntity = testData.createAttPatternBase('Test Pattern');
		AttShortTimeSettingBaseEntity copyEntity = orgEntity.copy();
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqkey, copyEntity.uniqkey);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.reasonId, copyEntity.reasonId);
		System.assertEquals(orgEntity.workSystem, copyEntity.workSystem);
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		AttShortTimeSettingBaseEntity entity = new AttShortTimeSettingBaseEntity();

		// 会社コードとジョブコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'JobCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode, entity.code));

		// ジョブコードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'JobCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}
}
