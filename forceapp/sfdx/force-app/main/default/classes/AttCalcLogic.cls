/**
 * 勤怠計算基本ロジック
 */
public abstract with sharing class AttCalcLogic {
	private final static Integer CUSTOM_WORK_TIME_COUNT = 20;

	// 勤怠計算入力用IF定義
	public interface iInput {
		// 指定日付の勤怠設定定義を取得
		Config getConfig(Date d);
		// 指定日付の日次情報を取得
		InputDaily getDay(Date d);
		// 月次開始日を取得
		Date getStartDateMonthly(Date d);
		// 月次終了日を取得
		Date getEndDateMonthly(Date d);
		// 計算対象の日付一覧を取得
		List<Date> getTargetDateList();
	}
	// 勤怠計算出力用IF定義
	public interface iOutput {
		// 日情報を更新する
		void updateDay(Date d, OutputDaily o);
		// 統計(月次)情報を更新する
		void updateTotal(Date targetDate, TotalOutput o);
		// 指定日付の日次出力を取得
		OutputDaily getOutputDay(Date d);
	}

	// 以下、各種勤務体系計算ロジック実現関数
	public abstract Date getStartDateWeekly(Date d);
	public abstract Boolean calcAttendanceDaily(Day day, Day next);
	public abstract List<Date> calcAttendanceWeekly(Date d);
	public abstract void calcAttendanceMonthly(Date d);
	public abstract void calcAttendanceCustom(Day day);

	// 以下、各種勤務体系利用する関数
	/**
	 * 翌日と連動して勤怠計算が可能な場合にtrueを返却する
	 */
	protected Boolean getCanCalcContinueNextDay(Day day, Day dayNext) {
		if (dayNext == null) {
			return true;
		}
		if (input.getConfig(day.getDate()).classificationNextDayWork == ClassificationNextDayWork.LegalHolidayOnly) {
			return day.isLegalHoliday() == dayNext.isLegalHoliday();
		}
		else if (input.getConfig(day.getDate()).classificationNextDayWork == ClassificationNextDayWork.BothHoliday ) {
			return day.getDayType() == dayNext.getDayType();
		}
		return true;
	}

	// 以下、各種勤務体系利用する定義
	protected iInput input;
	protected iOutput output;
	protected DayDB db;

	/** 変形労働時間制の変形期間を定義する(年単位は未実装) */
	public enum ModifiedWorkingSystemUnit {
		/** でない */
		None,
		/** 週単位 */
		Weekly,
		/** 月単位 */
		Monthly
	}
	// 勤怠設定定義
	public virtual class Config {
		public AttCalcRangesDto ContractRestRanges = new AttCalcRangesDto();
		public Integer contractRestTime = 0;
		public Integer legalWorkTimeDaily = 8*60;
		public Integer deemedWorkTime = 0;
		/**
		 * 控除と残業の相殺をするかどうかのオプション
		 * @return 控除と残業の相殺をしない
		 */
		public Boolean isNoOffsetOvertimeByUndertime = false;
		/**
		 * 所定時間まで申請無しで許可するかどうかのオプション
		 * @return 所定時間まで申請無しで許可
		 */
		public Boolean isPermitOvertimeWorkUntilNormalHours = false;
		/**
		 * 遅刻/早退/休憩補填時間を勤務換算時間として扱うかのオプション
		 * @return 遅刻/早退/休憩補填時間を勤務換算時間として扱う
		 */
		public Boolean isIncludeCompensationTimeToWorkHours = false;
		/**
		 * 勤務日から始まって所定勤務時間が法定休日にかかる時間帯で勤務する場合
		 *
		 * V5互換のためのオプション
		 *
		 * 法定休日にかかった所定勤務時間は、所定内法定外に分類して基本給を払わない計算を行うが
		 * この時間も基本給を支払う時間としてカウントする
		 * "勤怠計算の暦日区分"が"法定休日のみ暦日で分ける"の場合、これをtrueにしないとV5と
		 * 互換にならない。
		 *
		 * @return 法定休日の所定内勤務にも基本給を払う
		 */
		public Boolean isPayBasicSalaryLegalHolidayContractedWorkHours = false;
		/**
		 * 法定休日の自動決定を行うかどうかを返す
		 *
		 * @return true 行う。false 行わない
		 */
		public Boolean isAutoLegalHolidayAssign = true;
		/**
		 * 勤怠の年度の起算月を返す。
		 *
		 * @return 月(1..12)
		 */
		public Integer beginMonthOfYear = 1;
		/**
		 * 法定休日自動決定の期間の日数を返す。
		 *
		 * @return 期間の日数(週:7, 4週:28)
		 */
		public Integer legalHolidayAssignmentPeriod = 7;
		/**
		 * 法定休日自動決定の期間内の法定休日の日数を返す
		 *
		 * @return 期間の日数(1週1日:1, 4週4日:4)
		 */
		public Integer legalHolidayAssignmentDays = 1;
		/**
		 * 週の起算日の開始曜日を返す
		 *
		 * @return 曜日(0:日,1:月,..6:土)
		 */
		public Integer beginDayOfWeek = 0;
		/**
		 * 月の起算日
		 *
		 * 月の起算日を 1..31で設定する
		 *
		 * @return 月の起算日
		 */
		public Integer beginDayOfMonth = 1;

		/**
		 * 週の法定労働時間
		 *
		 * 週の法定労働時間を分で設定する
		 */
		public Integer legalWorkTimeAWeek = 40*60;

		/**
		 * 月の法定労働時間
		 *
		 * 月の法定労働時間を返す(実際は月によってかわる)
		 */
		public Integer legalWorkTimeMonthly = 31*40*60/7;

		/**
		 * 月の所定労働時間
		 *
		 * 月の所定労働時間を返す(実際は月によってかわる)
		 */
		public Integer contractedWorkTimeMonthly = 20*8*60;
		/**
		 * 勤怠計算の暦日区分
		 *
		 * ２日にまたがる勤務の暦日の分類方法を指定する
		 */
		public AttCalcLogic.ClassificationNextDayWork classificationNextDayWork = AttCalcLogic.ClassificationNextDayWork.LegalHolidayOnly;

		/**
		 * 勤務日を分ける境界時刻を(分)で設定する
		 *
		 * 0= 0:00 300 = 5:00
		 *
		 */
		public Integer boundaryTimeOfDay = 0;

		/**
		 * 日中とみなす勤務時間帯の時刻範囲
		 *
		 * 前日、翌日の分まで設定すること
		 * 例 [前日5:00-前日22:00,5:00-22:00,29:00-46:00]
		 */
		public AttCalcRangesDto workTimePeriodDaily = AttCalcRangesDto.RS(
				new List<AttCalcRangeDto>{
						AttCalcRangesDto.R(-19*60, -2*60),
						AttCalcRangesDto.R(5*60, 22*60),
						AttCalcRangesDto.R(29*60, 46*60)});

		/**
		 * 所定休日の勤務は法定内も割増を払う
		 *
		 * 所定休日の勤務は一日の法定労働時間を超えるか、週の法定労働時間を越えないと
		 * 法定外の割増が払われないが、所定休日の勤務に限り法定内も割増を払う場合
		 */
		public Boolean isPayAllowanceOnEveryHoliday = false;

		/**
		 * 所定休日の勤務も法定休日と同じ割増を払う
		 *
		 * 所定休日の勤務も法定休日と同じ割増を払う場合
		 */
		public Boolean isPayLegalHolidayAllowanceOnEveryHoliday = false;

		/**
		 * 法定内残業も割増を払う
		 *
		 * 法定内残業も法定外と同じ割増を払う場合
		 */
		public Boolean isPayAllowanceWhenOverContructedWorkHours = false;

		/**
		 * 所定休日の労働時間をフレックス対象労働時間に含める
		 *
		 * フレックス勤務の場合のみ、勤怠計算時に参照する
		 */
		public Boolean isIncludeHolidayWorkInPlainTime = false;

		/**
		 * 所定休日の労働時間をみなし労働時間に含める
		 *
		 * 裁量労働制の場合のみ、勤怠計算時に参照する
		 */
		public Boolean isIncludeHolidayWorkInDeemedTime = false;

		/**
		 * コアタイムなし
		 *
		 * フレックス勤務の場合のみ、勤怠計算時に参照する
		 */
		public Boolean withoutCoreTime = false;

		/**
		 * 変形労働制の期間区分
		 * (Javaの変形労働制に同じ)
		 * 法定内残業も法定外と同じ割増を払う場合
		 * 現在は月単位しかサポートされない
		 * FIXME : 勤務体系から期間区分を取得する
		 */
		public AttCalcLogic.ModifiedWorkingSystemUnit modifiedWorkingSystemUnit = AttCalcLogic.ModifiedWorkingSystemUnit.Monthly;
		/**
		 * 変形労働制の月数
		 *
		 * 変形労働制が月単位の場合のみ意味を持つ
		 * 12の約数であること(1,2,3,4,6,12)
		 */
		public Integer monthCountInModifiedWorkingSystemUnit = 1;

		/**
		 * 変形労働制の起算月
		 * この月から、変形労働制期間の月数毎の期間で期間の計算を行う
		 *
		 * 変形労働制が月単位で月数>1の場合のみ意味を持つ
		 */
		public Integer beginMonthOfYearInModifiedWorkingSystemUnit = 1;

		/**
		 * 変形労働制の週数
		 *
		 * 変形労働制が週単位の場合のみ意味を持つ
		 * また現在は =1 しかサポートされない
		 */
		public Integer weekCountInModifiedWorkingSystemUnit = 1;
	}

	/**
	 * ２日またがる勤務の計算方法を定義する
	 *
	 * 暦日で分けない：翌日にまたがった時間も前日の種別で勤怠計算を行う
	 * 法定休日のみ暦日で分ける：翌日が法定休日の場合のみ境界時刻で分けて計算を行う
	 * 所定と法定休日を暦日で分ける：翌日が所定休日や法定休日の場合に境界時刻で分けて計算を行う
	 */
	public enum ClassificationNextDayWork { None, LegalHolidayOnly, BothHoliday }

	/**
	 * 休暇の範囲を定義する
	 */
	public enum HolidayRange { Day, AM, PM, Half, Times }

	/**
	 * 休暇の種類を定義する
	 */
	public enum HolidayClass { Paid, Compensatory, Substiute, Unpaid }

	/**
	 * 勤務時間の種類を定義する
	 */
	public class WorkTimeCategory {
		public Boolean isLegalOut;
		public Boolean isContractOut;
		public AttCalcAutoHolidayLogic.DayType dayType;
		public Boolean isNight;
		public Boolean isCompensatory;
		public WorkTimeCategory(Boolean isLegalOut, Boolean isContractOut, AttCalcAutoHolidayLogic.DayType d, Boolean isNight, Boolean isCompensatory) {
			// TODO
			// assert !((d == 日種別.法定休日) && !法定外); // 法定休日なら法定外
			// assert !(代休有り && !所定外); // 代休有りなら所定外
			// assert d != 日種別.優先法定休日; // 優先法定休日はこの分類で扱わない
			this.isLegalOut = isLegalOut;
			this.isContractOut = isContractOut;
			this.dayType = d;
			this.isNight = isNight;
			this.isCompensatory = isCompensatory;
		}
		public Integer hashCode() {
			return String.valueOf(isLegalOut).hashCode() * 3 + String.valueOf(isContractOut).hashCode() * 5 + dayType.hashCode() * 7 + String.valueOf(isNight).hashCode() * 11 + String.valueOf(isCompensatory).hashCode();
		}
		public Boolean equals(Object obj) {
			if(obj == null)
				return false;
			WorkTimeCategory o = (WorkTimeCategory)obj;
			return isLegalOut == o.isLegalOut && isContractOut == o.isContractOut && dayType == o.dayType && isNight == o.isNight && isCompensatory == o.isCompensatory;
		}
	}

	/**
	 * 日の入力情報を定義する(計算用)
	 */
	public class InputDaily {
		public Date targetDate;
		public Integer inputStartTime;
		public Integer inputEndTime;
		public AttCalcRangesDto inputRestRanges = new AttCalcRangesDto();
		public Integer inputRestTime = 0;
		public AttCalcAutoHolidayLogic.DayType dayType;
		public Integer startTime;
		public Integer endTime;
		public Integer flexStartTime;
		public Integer flexEndTime;
		public Boolean isLegalHoliday = false;
		public Integer contractWorkTime = 0;
		public AttCalcRangesDto permitedLostTimeRanges = new AttCalcRangesDto();
		/** 勤務換算休憩 */
		public AttCalcRangesDto convertdRestRanges = new AttCalcRangesDto();
		/** 勤務換算時間 */
		public Integer convertedWorkTime = 0;
		/** 無給休暇休憩 */
		public AttCalcRangesDto unpaidLeaveRestRanges = new AttCalcRangesDto();
		/** 無給休暇時間 */
		public Integer unpaidLeaveTime = 0;
		public Integer compensatoryDayTime = 0;
		public AttCalcRangesDto allowedWorkRanges = new AttCalcRangesDto();
		public Boolean isAdmit = false;
		public Boolean isHolidayWork = false;
		public Boolean isRequestUpdated = false;
		public Boolean isCalenderUpdated = false;
		// 以下、日ごとの勤務パターンを設定された場合利用する項目
		public AttCalcRangesDto contractRestRanges = new AttCalcRangesDto();
		public Integer contractRestTime = 0;
		// 全日休暇フラグ
		public Boolean isFullDayLeave = false;
		// 休職休業フラグ
		public Boolean isAbsence = false;
	}

	/**
	 * 日のOutput情報を定義する(計算結果)
	 */
	public class OutputDaily {
		/** 実労働時間 */
		public Integer realWorkTime = 0;
		/** 勤務換算時間 */
		public Integer convertedWorkTime = 0;
		/** 無給控除時間 */
		public Integer unpaidLeaveLostTime = 0;
		/** 有給休暇時間 */
		public Integer paidLeaveTime = 0;
		/** 無給休暇時間 */
		public Integer unpaidLeaveTime = 0;
		/** 遅刻時間 */
		public Integer lateArriveTime = 0;
		/** 早退時間 */
		public Integer earlyLeaveTime = 0;
		/** 休憩時間 */
		public Integer breakTime = 0;
		/** 未処理休憩時間 */
		public Integer unknowBreakTime = 0; // 未処理休憩時間
		/** 遅刻控除時間 */
		public Integer lateArriveLostTime = 0;
		/** 早退控除時間 */
		public Integer earlyLeaveLostTime = 0;
		/** 休憩控除時間 */
		public Integer breakLostTime = 0;
		/** 遅刻補填時間 */
		public Integer authorizedLateArriveTime = 0;
		/** 早退補填時間 */
		public Integer authorizedEarlyLeaveTime = 0;
		/** 休憩補填時間 */
		public Integer authorizedBreakTime = 0;
		/** 有休時間 */
		public Integer paidBreakTime = 0;
		/** 補填時間 */
		public Integer authorizedOffTime = 0;
		/** 労働基準外時間 */
		public Integer legalOutTime = 0;
		/** 労働基準内時間 */
		public Integer legalInTime = 0;
		/** 割増未支給労働時間 */
		public Integer regularRateWorkTime = 0;
		/** 労働時間 */
		public Map<WorkTimeCategory, Integer> workTimeCategoryMap = new Map<WorkTimeCategory, Integer>();
		/** 所定勤務日 */
		public Integer workDay = 0;
		/** 出勤日 */
		public Integer realWorkDay = 0;
		/** 法定休日出勤日 */
		public Integer legalHolidayWorkCount = 0;
		/** 所定休日出勤日 */
		public Integer holidayWorkCount = 0;
		/** 労基基準超過時間 */
		public Integer regularOverTime = 0;
		/** 割増手当追加発生時間 */
		public Integer additionalAllowanceTime = 0;
		/** 追加所定外勤務発生時間 */
		public Integer additionalOutContractTime = 0;
		/** 労基基準内時間合計 */
		public Integer legalWorkTimeSum = 0;
		public Integer noPaidAllowanceTimeSum = 0;
		public Integer convertedWorkTimeSum = 0;
		/** みなし労働時間 */
		public Integer deemedWorkTime = 0;
		public Integer irregularBreakTime = 0;
		public List<Integer> customWorkTimes = new List<Integer>();
		// 種別ごとの勤務時間を計算するマップ
		public AttCalcRangesDto inContractInLegalRanges = AttCalcRangesDto.RS();
		public AttCalcRangesDto inContractOutLegalRanges = AttCalcRangesDto.RS();
		public AttCalcRangesDto outContractInLegalRanges = AttCalcRangesDto.RS();
		public AttCalcRangesDto outContractOutLegalRanges = AttCalcRangesDto.RS();

		public OutputDaily() {
			for (Integer i = 0; i <= CUSTOM_WORK_TIME_COUNT; i++) {
				this.customWorkTimes.add(0);
			}
		}
	}

	/**
	 * 統計(月次)のOutput情報を定義する
	 */
	public class TotalOutput {
		/** 実労働時間 */
		public Integer realWorkTime = 0;
		/** 勤務換算時間 */
		public Integer convertedWorkTime = 0;
		/** 無給控除時間 */
		public Integer unpaidLeaveLostTime = 0;
		/** 有給休暇時間 */
		public Integer paidLeaveTime = 0;
		/** 無給休暇時間 */
		public Integer unpaidLeaveTime = 0;
		/** 遅刻時間 */
		public Integer lateArriveTime = 0;
		/** 早退時間 */
		public Integer earlyLeaveTime = 0;
		/** 休憩時間 */
		public Integer breakTime = 0;
		/** 遅刻控除時間 */
		public Integer lateArriveLostTime = 0;
		/** 早退控除時間 */
		public Integer earlyLeaveLostTime = 0;
		/** 休憩控除時間 */
		public Integer breakLostTime = 0;
		/** 遅刻補填時間 */
		public Integer authorizedLateArriveTime = 0;
		/** 早退補填時間 */
		public Integer authorizedEarlyLeaveTime = 0;
		/** 休憩補填時間 */
		public Integer authorizedBreakTime = 0;
		/** 有休時間 */
		public Integer paidBreakTime = 0;
		/** 補填時間 */
		public Integer authorizedOffTime = 0;
		/** 労働基準外時間 */
		public Integer legalOutTime = 0;
		/** 労働基準内時間 */
		public Integer legalInTime = 0;
		/** 割増未支給労働時間 */
		public Integer regularRateWorkTime = 0;
		/** 労働時間 */
		public Map<WorkTimeCategory, Integer> workTimeCategoryMap = new Map<WorkTimeCategory, Integer>();
		/** 所定勤務日 */
		public Integer workDay = 0;
		/** 出勤日 */
		public Integer realWorkDay = 0;
		/** 法定休日出勤日 */
		public Integer legalHolidayWorkCount = 0;
		/** 所定休日出勤日 */
		public Integer holidayWorkCount = 0;
		/** カスタム労働時間 */
		public List<Integer> customWorkTimes = new List<Integer>();

		public TotalOutput() {
			for (Integer i = 0; i <= CUSTOM_WORK_TIME_COUNT; i++) {
				customWorkTimes.add(0);
			}
		}
	}

	/**
	 * 法定休日変更計算が必要な場合にtrueを返却する
	 */
	private Boolean isNeedAutoLegalHolidayAssign(Day day) {
		// 本来はdayの中を見て法定休日の再判定が必要かチェックするのが望ましいが
		// 初版は、法定休日自動判定の設定なら常に再計算を行う設定にする。
		return input.getConfig(day.getDate()).isAutoLegalHolidayAssign;
	}

	/**
	 * 法定休日を自動割当判定開始日を取得する
	 * @param 勤怠計算設定
	 * @param 対象日付
	 * @return 自動割当判定開始日
	 */
	private Date getAutoLegalHolidayAssignStartDate(Config conf, Date d) {
		Date start = null;
		if(conf.legalHolidayAssignmentPeriod == 7) {
			// 期間が7日の場合は、週で行う
			Integer daysOfWeek = DateUtil.dayOfWeek(d);
			start = d.addDays(
					conf.beginDayOfWeek > daysOfWeek
						? (conf.beginDayOfWeek - daysOfWeek - 7) // 先週の開始日
						: (conf.beginDayOfWeek - daysOfWeek) // 今週の開始日
			);
		}
		else {
			// 期間が7日でない場合は、起算日から期間の日数毎で行う
			Date beginDayOfYear = Date.newInstance(d.year(), conf.beginMonthOfYear, conf.beginDayOfMonth);
			if(beginDayOfYear> d) {
				beginDayOfYear = beginDayOfYear.addYears(-1); // 前年度に変更
			}
			Integer period = (Integer)beginDayOfYear.daysBetween(d);
			period -= Math.mod(period, conf.legalHolidayAssignmentPeriod);
			// 期間が次の年度にかかる場合(期末の中途半端の期間)は、自動判定を行わない
			if(!(beginDayOfYear.addYears(1) < (beginDayOfYear.addDays(period + conf.legalHolidayAssignmentPeriod)))) {
				start = beginDayOfYear.addDays(period);
			}
		}
		return start;
	}

	/**
	 * 法定休日自動割当用日次情報を定義
	 */
	public class Day implements AttCalcAutoHolidayLogic.iTargetDay {
		public InputDaily input;
		public OutputDaily output;
		/** 法定休日指定 */
		public Boolean isLegalHolidayAssign = false;
		public Date getDate() {
			return input.targetDate;
		}
		public Day(InputDaily input) {
			this(input, new OutputDaily());
		}
		public Day(inputDaily input, outputDaily output) {
			this.input = input;
			this.output = output;
		}
		/**
		 * @return 確定済みの場合はtrue
		 */
		public Boolean isAdmit() {
			return input.isAdmit;
		}
		public AttCalcAutoHolidayLogic.DayType dayType() {
			return input.dayType;
		}
		/**
		 * @return 休日出勤の場合はtrue
		 */
		public Boolean isHolidayWork() {
			return input.isHolidayWork;
		}
		/**
		 * @return 休日出勤の場合はtrue
		 */
		public Boolean isLegalHolidayAssign() {
			return isLegalHolidayAssign;
		}
		/**
		 * @return 勤務日の場合はtrue
		 */
		public Boolean isWorkDay() { // 勤務日
			return input.dayType == AttCalcAutoHolidayLogic.DayType.Workday;
		}
		/**
		 * @return 法定休日の場合はtrue
		 */
		public Boolean isLegalHoliday() { // 法定休日
			return input.dayType == AttCalcAutoHolidayLogic.DayType.LegalHoliday || isLegalHolidayAssign;
		}
		/**
		 * @return 所定休日の場合はtrue
		 */
		public Boolean isHoliday() { // 所定休日
			return !isWorkDay() && !isLegalHoliday();
		}
		/**
		 * 日タイプを取得する
		 */
		public AttCalcAutoHolidayLogic.DayType getDayType() {
			if(isLegalHoliday()) return AttCalcAutoHolidayLogic.DayType.LegalHoliday;
			if(isWorkDay()) return AttCalcAutoHolidayLogic.DayType.Workday;
			return AttCalcAutoHolidayLogic.DayType.Holiday;
		}
	}

	/**
	 * 勤怠計算対象日情報を管理する
	 * 日次の計算結果を積み上げ対象
	 */
	public class DayDB {
		private iInput input;
		private iOutput output;
		private Map<Date, Day> db = new Map<Date, Day>();
		public DayDB(iInput input, iOutput output) {
			this.input = input;
			this.output = output;
		}
		public Day get(Date d) {
			Day r = db.get(d);
			if(r == null) {
				InputDaily inputDaily = input.getDay(d);
				OutputDaily outputDaily = output.getOutputDay(d);
				if (inputDaily != null && outputDaily != null) {
					db.put(d, r = new Day(inputDaily, outputDaily));
				} else if (inputDaily != null) {
					db.put(d, r = new Day(inputDaily));
				}
			}
			return r;
		}
	}

	/**
	 * 法定休日自動割当の入力IFを実現
	 */
	public class LegalHolidayAutoAssignInput implements AttCalcAutoHolidayLogic.iInput {
		public List<AttCalcAutoHolidayLogic.iTargetDay> targetDayList;
		public Integer legalHolidayCount;

		public LegalHolidayAutoAssignInput(DayDB db, Date startDate, Integer periodDays, Integer legalHolidayCount) {
			this.targetDayList = new List<AttCalcAutoHolidayLogic.iTargetDay>();
			for(Integer i = 0; i < periodDays; i++) {
				this.targetDayList.add(db.get(startDate.addDays(i)));
			}
			this.legalHolidayCount = legalHolidayCount;
		}
		public Integer getLegalHolidayCount() {
			return this.legalHolidayCount;
		}
		public List<AttCalcAutoHolidayLogic.iTargetDay> getTargetDayList() {
			return this.targetDayList;
		}
	}

	/**
	 * 法定休日自動割当の出力IF
	 */
	public class LegalHolidayAutoAssignOutput implements AttCalcAutoHolidayLogic.iOutput {
		private List<Boolean> legalHolidayList;

		public LegalHolidayAutoAssignOutput() {
		}

		public List<Boolean> getLegalHolidayList() {
			return this.legalHolidayList;
		}

		public void setLegalHoliday(List<Boolean> days) {
			legalHolidayList = days;
		}
	}

	/**
	 * コンストラクタ
	 */
	public AttCalcLogic(iOutput output, iInput input) {
		this.input = input;
		this.output = output;
		this.db = new DayDB(input, output);
	}

	/**
	 * 法定休日自動判定
	 */
	private List<Date> getLegalHolidayAutoAssignd(DayDB db, Config conf, Date start) {
		LegalHolidayAutoAssignInput tmpin = new LegalHolidayAutoAssignInput(db, start, conf.legalHolidayAssignmentPeriod, conf.legalHolidayAssignmentDays);
		LegalHolidayAutoAssignOutput tmpout = new LegalHolidayAutoAssignOutput();
		AttCalcAutoHolidayLogic.apply(tmpout, tmpin);
		List<Date> updateDateList = new List<Date>();
		// 法定休日の判定は初期値と変更された対象を取得
		for(Integer i = 0; i < conf.legalHolidayAssignmentPeriod; i++) {
			Date d = start.addDays(i);
			Day day = db.get(d);
			if(day.isLegalHoliday() != day.input.isLegalHoliday) {
				updateDateList.add(d);
			}
		}
		return updateDateList;
	}

	// 勤怠計算入口
	public void apply() {
		// 再計算対象日のコピー
		Set<Date> recalcTargetDateSet = new Set<Date>(input.getTargetDateList());

		// -------- 法定休日自動判定の呼び出し
		// 再計算対象日が法定休日自動判定の必要があるかチェックして
		// 判定が必要な場合対象期間の再判定を行う
		// 再判定の結果、再計算対象日が増える可能性がある
		TreeSet recalcLegalHolidayDateSet = new TreeSet(); // 昇順を保証したいのでTreeSetを使用する
		for(Date d: recalcTargetDateSet) {
			if (input.getConfig(d).isAutoLegalHolidayAssign) {
				Day day = db.get(d);
				if(isNeedAutoLegalHolidayAssign(day)) {
					recalcLegalHolidayDateSet.add(getAutoLegalHolidayAssignStartDate(input.getConfig(d), d));
				}
			}
		}
		for(Date d: recalcLegalHolidayDateSet.getItems()) {
			recalcTargetDateSet.addAll(getLegalHolidayAutoAssignd(db, input.getConfig(d), d));
		}
		// -------- calcAttendanceDaily
		// 再計算対象日について日次再計算を行う
		// 計算結果が変更された場合、その週の週次再計算を行うため記録する
		TreeSet recalWeeklyDateSet = new TreeSet(); // 昇順を保証したいのでTreeSetを使用する
		TreeSet recalMonthlyDateSet = new TreeSet(); // 昇順を保証したいのでTreeSetを使用する
		for(Date d: recalcTargetDateSet) {
			Day day = db.get(d);
			Day dayNext = db.get(d.addDays(1));
			// 勤怠計算外の各種判定ロジック
			// 勤務日
			day.output.workDay = (day.isWorkDay() ? 1 : 0);
			// 勤怠計算外の各種集計
			// 勤務相当時間
			day.output.convertedWorkTime = day.input.convertedWorkTime;
			// 無給控除時間
			day.output.unpaidLeaveLostTime = day.input.unpaidLeaveTime;
			// 全日休暇、休職休業の場合、勤怠計算しない
			if (day.input.isFullDayLeave
					|| day.input.isAbsence) {
				continue;
			}
			System.debug(LoggingLevel.DEBUG, '---calcAttendanceDaily of ' + d);
			if(calcAttendanceDaily(day, dayNext)) {
				recalcTargetDateSet.add(d);
				Date w = getStartDateWeekly(d);
				if(w != null) {
					recalWeeklyDateSet.add(w);
				}
				Date m = input.getStartDateMonthly(d);
				if(m != null) {
					recalMonthlyDateSet.add(m);
				}
			}
		}
		// -------- 週次勤怠計算
		// 週次集計が必要な週の積上げ計算を行う
		// 計算結果が変更された場合、その週の週次再計算を行うため記録する
		for(Date d: recalWeeklyDateSet.getItems()) {
			List<Date> otherTargetDateList = calcAttendanceWeekly(d);
			for(Date w: otherTargetDateList) {
				recalcTargetDateSet.add(w);
			}
		}
		TreeSet recalCustomDateSet = new TreeSet(); // 昇順を保証したいのでTreeSetを使用する
		for(Date d: recalcTargetDateSet) {
			recalCustomDateSet.add(d);
			Date m = input.getStartDateMonthly(d);
			if(m != null) {
				recalMonthlyDateSet.add(m);
			}
		}
		// -------- カスタマイズ勤怠計算
		// カスタマイズが必要な日次の計算を行う
		// 計算結果が変更された場合、再計算対象に追加する
		for(Date d: recalCustomDateSet.getItems()) {
			Day day = db.get(d);
			calcAttendanceCustom(day);
		}
		// -------- 月次計算
		// 再計算が必要な月次の積上集計と出力を行う
		for(Date d: recalMonthlyDateSet.getItems()) {
			calcAttendanceMonthly(d);
		}
		// -------- 日次出力
		// 再計算を行った日次を出力する
		for(Date d: recalcTargetDateSet) {
			Day day = db.get(d);
			output.updateDay(d, day.output);
		}
	}

	// JAVAのTreeSetを実現するクラス
	// 日付でソートされるsetを実現
	private class TreeSet {
		private Set<Date> dateSet = new Set<Date>();
		public TreeSet() {}
		public void add(Date item) {
			dateSet.add(item);
		}
		public List<Date> getItems() {
			List<Date> dateList = new List<Date>(dateSet);
			dateList.sort();
			return dateList;
		}
	}
}