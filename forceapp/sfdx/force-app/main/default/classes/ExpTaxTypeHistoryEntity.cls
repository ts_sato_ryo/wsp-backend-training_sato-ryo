/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分履歴エンティティを表すクラス Entity class for ExpTaxTypeHistory object
 */
public with sharing class ExpTaxTypeHistoryEntity extends ExpTaxTypeHistoryGeneratedEntity {
	/** 税区分名Lの最大文字数 Max length of the name */
	public final static Integer NAME_MAX_LENGTH = 80;
	/** 税率の最小値 Minimum tax rate */
	public static final Decimal TAX_RATE_MIN_VALUE = 0.00;
	/** 税率の最大値 Max tax rate */
	public static final Decimal TAX_RATE_MAX_VALUE = 99.99;
	/** 税率の小数桁数 Scale of tax rate */
	public static final Integer TAX_RATE_SCALE = 2;

	public ExpTaxTypeHistoryEntity() {
		super();
	}

	public ExpTaxTypeHistoryEntity(ExpTaxTypeHistory__c sobj) {
		super(sobj);
	}

	/** 税区分名(多言語) 参照のみ */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
		private set;
	}

	/**
	 * Create a Copy of CostCenterHistoryEntity(ID is not copied.)
	 */
	public ExpTaxTypeHistoryEntity copy() {
		ExpTaxTypeHistoryEntity copyEntity = new ExpTaxTypeHistoryEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 */
	public override ParentChildHistoryEntity copySuperEntity() {
		return copy();
	}
}