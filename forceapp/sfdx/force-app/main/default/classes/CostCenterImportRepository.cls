/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通 Common
 *
 * コストセンターインポートのリポジトリ Cost Center Import Repository
 */
public with sharing class CostCenterImportRepository extends Repository.BaseRepository {
	
	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ComCostCenterImport__c.Id,
			ComCostCenterImport__c.Name,
			ComCostCenterImport__c.ImportBatchId__c,
			ComCostCenterImport__c.RevisionDate__c,
			ComCostCenterImport__c.Code__c,
			ComCostCenterImport__c.CompanyCode__c,
			ComCostCenterImport__c.Name_L0__c,
			ComCostCenterImport__c.Name_L1__c,
			ComCostCenterImport__c.Name_L2__c,
			ComCostCenterImport__c.LinkageCode__c,
			ComCostCenterImport__c.ParentBaseCode__c,
			ComCostCenterImport__c.HistoryComment__c,
			ComCostCenterImport__c.Status__c,
			ComCostCenterImport__c.Error__c
		};
	}

	/** 検索フィルタ */
	private class Filter {
		/** インポートバッチID */
		public Id importBatchId;
		/** インポートID */
		public List<Id> importIdList;
		/** ステータス */
		public List<String> status;
	}

	/**
	 * コストセンターインポートエンティティを取得する
	 * @param importId インポートID
	 * @return コストセンターインポートエンティティ
	 */
	public CostCenterImportEntity getEntity(Id importId) {
		// 検索実行
		List<CostCenterImportEntity> entityList = getEntityList(new List<Id>{importId});

		CostCenterImportEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * コストセンターインポートエンティティのリストを取得する
	 * @param importIdList コストセンターインポートIDのリスト
	 * @return 条件に一致したコストセンターインポートエンティティのリスト
	 */
	public List<CostCenterImportEntity> getEntityList(List<Id> importIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.importIdList = importIdList;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, null, forUpdate);
	}

	/**
	 * 指定したインポートバッチIDで処理対象のコストセンターインポートエンティティのリストを取得する
	 * @param importBatchId インポートバッチID
	 * @param status コストセンターインポートデータのステータス
	 * @param max 取得上限件数
	 * @return 条件に一致したコストセンターインポートエンティティのリスト
	 */
	public List<CostCenterImportEntity> getEntityListByImportBatchId(Id importBatchId, List<String> status, Integer max) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.importBatchId = importBatchId;
		filter.status = status;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, max, forUpdate);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(CostCenterImportEntity entity) {
		return saveEntityList(new List<CostCenterImportEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<CostCenterImportEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<CostCenterImportEntity> entityList, Boolean allOrNone) {
		List<ComCostCenterImport__c> objList = new List<ComCostCenterImport__c>();

		// エンティティからSObjectを作成する
		for (CostCenterImportEntity entity : entityList) {
			ComCostCenterImport__c obj = createObj(entity);
			objList.add(obj);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(objList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * コストセンターインポートを検索する
	 * @param filter 検索条件
	 * @param max 取得上限件数
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したコストセンターインポートエンティティのリスト
	 */
	private List<CostCenterImportEntity> searchEntity(Filter filter, Integer max, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id importBatchId = filter.importBatchId;
		final List<Id> importIds = filter.importIdList;
		final List<String> status = filter.status;
		condList.add(getFieldName(ComCostCenterImport__c.ImportBatchId__c) + ' = :importBatchId');
		if ((importIds != null) && (!importIds.isEmpty())) {
			condList.add(getFieldName(ComCostCenterImport__c.Id) + ' IN :importIds');
		}
		if ((status != null) && (!status.isEmpty())) {
			condList.add(getFieldName(ComCostCenterImport__c.Status__c) + ' IN :status');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComCostCenterImport__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (max != null) {
			soql += ' LIMIT ' + max;
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('CostCenterImportRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<CostCenterImportEntity> entityList = new List<CostCenterImportEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ComCostCenterImport__c)sobj));
		}

		return entityList;
	}

	/**
	 * ComCostCenterImport__cオブジェクトからCostCenterImportEntityを作成する
	 * @param obj 作成元のComCostCenterImport__cオブジェクト
	 * @return 作成したCostCenterImportEntity
	 */
	private CostCenterImportEntity createEntity(ComCostCenterImport__c obj) {
		CostCenterImportEntity entity = new CostCenterImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.code = obj.Code__c;
		entity.companyCode = obj.CompanyCode__c;
		entity.name_L0 = obj.Name_L0__c;
		entity.name_L1 = obj.Name_L1__c;
		entity.name_L2 = obj.Name_L2__c;
		entity.linkageCode = obj.LinkageCode__c;
		entity.parentBaseCode = obj.ParentBaseCode__c;
		entity.historyComment = obj.HistoryComment__c;
		entity.status = ImportStatus.valueOf(obj.Status__c);
		entity.error = obj.Error__c;

		entity.resetChanged();
		return entity;
	}

	/**
	 * CostCenterImportEntityからComCostCenterImport__cオブジェクトを作成する
	 * @param entity 作成元のCostCenterImportEntity
	 * @return 作成したComCostCenterImport__cオブジェクト
	 */
	private ComCostCenterImport__c createObj(CostCenterImportEntity entity) {
		ComCostCenterImport__c retObj = new ComCostCenterImport__c();

		retObj.Id = entity.id;
		if (entity.isChanged(CostCenterImportEntity.Field.ImportBatchId)) {
			retObj.ImportBatchId__c = entity.importBatchId;
		}
		if (entity.isChanged(CostCenterImportEntity.Field.Status)) {
			retObj.Status__c = entity.status.value;
		}
		if (entity.isChanged(CostCenterImportEntity.Field.Error)) {
			retObj.Error__c = entity.error;
		}

		return retObj;
	}
}