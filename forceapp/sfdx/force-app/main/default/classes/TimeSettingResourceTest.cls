/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description TimeSettingResourceのテストクラス
*/
@isTest
private class TimeSettingResourceTest {

	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

		public Date currentDate;

		/**
		 * コンストラクタ
		 * 標準ユーザーと設定を作成する
		 */
		public ResourceTestData(){
			super();
			currentDate = Date.today();
		}

	}

	/**
	 * 工数設定レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Date currentDate = Date.today();

		Test.startTest();

		TimeSettingResource.TimeSetting param = new TimeSettingResource.TimeSetting();
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.code = '001';
		param.companyId = company.Id;
		param.summaryPeriod = 'Month';
		param.startDateOfMonth = '1';
		param.startDayOfWeek = 'Sunday';
		param.monthMark = 'BeginBase';
		param.useRequest = true;
		param.validDateFrom = currentDate - 30;
		param.validDateTo = currentDate;
		param.comment = 'テストコメント';

		TimeSettingResource.CreateApi api = new TimeSettingResource.CreateApi();
		TimeSettingResource.SaveResult res = (TimeSettingResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 工数設定ベース・履歴レコードが1件ずつ作成されること
		List<TimeSettingBase__c> newBaseRecords = [
			SELECT
				Id,
				Name,
				Code__c,
				CompanyId__c,
				CurrentHistoryId__c,
				SummaryPeriod__c,
				StartDateOfMonth__c,
				StartDayOfWeek__c,
				MonthMark__c,
				UseRequest__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ValidFrom__c,
						ValidTo__c,
						HistoryComment__c
					FROM Histories__r
				)
			FROM TimeSettingBase__c
			WHERE CompanyId__c = :company.Id];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].Histories__r.size());

		// 値の検証
		TimeSettingBase__c newBase = newBaseRecords[0];
		TimeSettingHistory__c newHistory = newBase.Histories__r[0];

		// レスポンス値の検証
		System.assertEquals(newBase.Id, res.Id);

		// ベースレコード値の検証
		System.assertEquals(param.name_L0, newBase.Name);
		System.assertEquals(param.code, newBase.Code__c);
		System.assertEquals(param.companyId, newBase.CompanyId__c);
		System.assertEquals(newHistory.Id, newBase.CurrentHistoryId__c);
		System.assertEquals(param.summaryPeriod, newBase.SummaryPeriod__c);
		System.assertEquals(Integer.valueOf(param.startDateOfMonth), newBase.StartDateOfMonth__c);
		System.assertEquals(null, newBase.StartDayOfWeek__c);
		System.assertEquals(param.monthMark, newBase.MonthMark__c);
		System.assertEquals(param.useRequest, newBase.UseRequest__c);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = company.Code__c + '-' +
							param.code + '-' +
							new AppDate(param.validDateFrom).formatYYYYMMDD() + '-' +
							new AppDate(param.validDateTo).formatYYYYMMDD();
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.name_L0, newHistory.Name);
		System.assertEquals(param.name_L0, newHistory.Name_L0__c);
		System.assertEquals(param.name_L1, newHistory.Name_L1__c);
		System.assertEquals(param.name_L2, newHistory.Name_L2__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
	}

	/**
	 * 工数設定レコードを1件作成する(週)
	 */
	@isTest
	static void createWeekTest() {
		ResourceTestData testData = new ResourceTestData();

		Test.startTest();

		TimeSettingResource.TimeSetting param = new TimeSettingResource.TimeSetting();
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.code = '001';
		param.companyId = testData.company.Id;
		param.summaryPeriod = 'Week';
		param.startDateOfMonth = '1';
		param.startDayOfWeek = 'Monday';
		param.monthMark = 'BeginBase';
		param.useRequest = true;
		param.validDateFrom = testData.currentDate - 30;
		param.validDateTo = testData.currentDate;
		param.comment = 'テストコメント';

		TimeSettingResource.CreateApi api = new TimeSettingResource.CreateApi();
		TimeSettingResource.SaveResult res = (TimeSettingResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 工数設定ベース・履歴レコードが1件ずつ作成されること
		List<TimeSettingBase__c> newBaseRecords = [
			SELECT
				Id,
				Name,
				Code__c,
				CompanyId__c,
				CurrentHistoryId__c,
				SummaryPeriod__c,
				StartDateOfMonth__c,
				StartDayOfWeek__c,
				MonthMark__c,
				UseRequest__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ValidFrom__c,
						ValidTo__c,
						HistoryComment__c
					FROM Histories__r
				)
			FROM TimeSettingBase__c
			WHERE CompanyId__c = :testData.company.Id];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].Histories__r.size());

		// 値の検証
		TimeSettingBase__c newBase = newBaseRecords[0];
		TimeSettingHistory__c newHistory = newBase.Histories__r[0];

		// レスポンス値の検証
		System.assertEquals(newBase.Id, res.Id);

		// ベースレコード値の検証
		System.assertEquals(param.name_L0, newBase.Name);
		System.assertEquals(param.code, newBase.Code__c);
		System.assertEquals(param.companyId, newBase.CompanyId__c);
		System.assertEquals(newHistory.Id, newBase.CurrentHistoryId__c);
		System.assertEquals(param.summaryPeriod, newBase.SummaryPeriod__c);
		System.assertEquals(null, newBase.StartDateOfMonth__c);// 連携されないことを保証する
		System.assertEquals(param.startDayOfWeek, newBase.StartDayOfWeek__c);
		System.assertEquals(param.monthMark, newBase.MonthMark__c);
		System.assertEquals(param.useRequest, newBase.UseRequest__c);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = testData.company.code + '-' +
							param.code + '-' +
							new AppDate(param.validDateFrom).formatYYYYMMDD() + '-' +
							new AppDate(param.validDateTo).formatYYYYMMDD();
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.name_L0, newHistory.Name);
		System.assertEquals(param.name_L0, newHistory.Name_L0__c);
		System.assertEquals(param.name_L1, newHistory.Name_L1__c);
		System.assertEquals(param.name_L2, newHistory.Name_L2__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
	}

	/**
	 * 工数設定レコードを1件作成する(異常系：工数設定の管理権限なし)
	 */
	@isTest
	static void creativeNegativeTestNoPermission() {
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 工数設定の管理権限を無効に設定する
		testData.permission.isManageTimeSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		TimeSettingResource.TimeSetting param = new TimeSettingResource.TimeSetting();
		param.name_L0 = 'テスト設定_L0';
		param.name_L1 = 'テスト設定_L1';
		param.name_L2 = 'テスト設定_L2';
		param.code = '001';
		param.companyId = testData.company.Id;
		param.summaryPeriod = 'Month';
		param.startDateOfMonth = '1';
		param.monthMark = 'BeginBase';
		param.useRequest = true;
		param.validDateFrom = testData.currentDate - 30;
		param.validDateTo = testData.currentDate;
		param.comment = 'テストコメント';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TimeSettingResource.CreateApi api = new TimeSettingResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg(AppLanguage.JA.value).Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 工数設定レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeNegativeTest() {
		Test.startTest();

		TimeSettingResource.TimeSetting param = new TimeSettingResource.TimeSetting();
		TimeSettingResource.CreateApi api = new TimeSettingResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/**
	 * 工数設定レコードを1件作成する(異常系:月度起算日が正しくない)
	 */
	@isTest
	static void creativeNegativeTestStartOfDateMonth() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Date currentDate = Date.today();

		TimeSettingResource.TimeSetting param = new TimeSettingResource.TimeSetting();
		param.name_L0 = 'テスト設定_L0';
		param.code = '001';
		param.companyId = company.Id;
		param.summaryPeriod = 'Month';
		param.monthMark = 'BeginBase';
		param.validDateFrom = currentDate - 30;
		param.validDateTo = currentDate;
		TimeSettingResource.CreateApi api = new TimeSettingResource.CreateApi();

		// 整数以外の値
		try {
			param.startDateOfMonth = '1.2';
			Object res = api.execute(param);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.TypeException e) {
			System.assertEquals(App.ERR_CODE_INVALID_TYPE, e.getErrorCode());
		}

		// 有効範囲外の値
		// 範囲の詳細テストはバリデータで実施
		try {
			param.startDateOfMonth = '29';
			Object res = api.execute(param);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_INVALID_VALUE, e.getErrorCode());
		}

	}

	/**
	 * 工数設定レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c setting = ComTestDataUtility.createTimeSettingWithHistory('Test設定', company.Id);

		Test.startTest();

		Map<String, Object> param = new Map<String, Object> {
			'id' => setting.id,
			'code' => '002'
		};

		TimeSettingResource.UpdateApi api = new TimeSettingResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// 工数設定レコードが更新されていること
		List<TimeSettingBase__c> timeSettingList = [
			SELECT
				Code__c
			FROM TimeSettingBase__c
			WHERE Id = :(String)param.get('id')];

		System.assertEquals(1, timeSettingList.size());
		System.assertEquals((String)param.get('code'), timeSettingList[0].Code__c);
	}

	/**
	 * 工数設定レコードを1件更新する(異常系：工数設定の管理権限なし)
	 */
	@isTest
	static void updatePositiveTestNoPermission() {
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 工数設定の管理権限を無効に設定する
		testData.permission.isManageTimeSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		Map<String, Object> param = new Map<String, Object> {
			'id' => testData.timeSetting.id,
			'code' => '002'
		};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TimeSettingResource.UpdateApi api = new TimeSettingResource.UpdateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg(AppLanguage.JA.value).Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 工数設定レコードを1件更新する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		Test.startTest();

		TimeSettingResource.TimeSetting param = new TimeSettingResource.TimeSetting();

		App.ParameterException ex;
		try {
			Object res = (new TimeSettingResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 工数設定レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c delRecord = ComTestDataUtility.createTimeSettingWithHistory('Test設定', company.Id);

		Test.startTest();

		TimeSettingResource.DeleteOption param = new TimeSettingResource.DeleteOption();
		param.id = delRecord.id;

		Object res = (new TimeSettingResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 部署レコードが削除されること
		List<TimeSettingBase__c> timeSettingList = [SELECT Id FROM TimeSettingBase__c];
		System.assertEquals(0, timeSettingList.size());
	}

	/**
	 * 工数設定レコードを1件削除する(異常系：工数設定の管理権限なし)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 工数設定の管理権限を無効に設定する
		testData.permission.isManageTimeSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		TimeSettingResource.DeleteOption param = new TimeSettingResource.DeleteOption();
		param.id = testData.timeSetting.id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TimeSettingResource.DeleteApi api = new TimeSettingResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg(AppLanguage.JA.value).Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 工数設定レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		TimeSettingResource.DeleteOption param = new TimeSettingResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new TimeSettingResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 工数設定レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeSettingBase__c> settings = ComTestDataUtility.createTimeSettingsWithHistory('Test設定', company.Id, 3);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => settings[0].id
		};

		TimeSettingResource.SearchApi api = new TimeSettingResource.SearchApi();
		TimeSettingResource.SearchResult res = (TimeSettingResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 工数設定レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 工数設定レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c setting = ComTestDataUtility.createTimeSettingWithHistory('Test設定', company.Id);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => setting.id
		};

		TimeSettingResource.SearchApi api = new TimeSettingResource.SearchApi();
		TimeSettingResource.SearchResult res = (TimeSettingResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 工数設定レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		TimeSettingHistory__c history = [SELECT Name_L1__c FROM TimeSettingHistory__c WHERE BaseId__c =:setting.Id LIMIT 1];
		System.assertEquals(history.Name_L1__c, res.records[0].name);
	}

	/**
	 * 工数設定レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c setting = ComTestDataUtility.createTimeSettingWithHistory('Test設定', company.Id);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => setting.id
		};

		TimeSettingResource.SearchApi api = new TimeSettingResource.SearchApi();
		TimeSettingResource.SearchResult res = (TimeSettingResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 工数設定レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		TimeSettingHistory__c history = [SELECT Name_L2__c FROM TimeSettingHistory__c WHERE BaseId__c =:setting.Id LIMIT 1];
		System.assertEquals(history.Name_L2__c, res.records[0].name);
	}

	/**
	 * 工数設定レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeSettingBase__c> settings = ComTestDataUtility.createTimeSettingsWithHistory('Test設定', company.Id, 3);

		Test.startTest();

		Map<String, Object> param = new Map<String, Object>();
		TimeSettingResource.SearchApi api = new TimeSettingResource.SearchApi();
		TimeSettingResource.SearchResult res = (TimeSettingResource.SearchResult)api.execute(param);

		Test.stopTest();

		settings = [
			SELECT
				Id,
				Name,
				Code__c,
				CompanyId__c,
				CurrentHistoryId__c,
				SummaryPeriod__c,
				StartDateOfMonth__c,
				MonthMark__c,
				UseRequest__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ValidFrom__c,
						ValidTo__c
				FROM Histories__r)
			FROM TimeSettingBase__c];

		// 工数設定レコードが取得できること
		System.assertEquals(settings.size(), res.records.size());

		for (Integer i = 0, n = settings.size(); i < n; i++) {
			TimeSettingBase__c base = settings[i];
			TimeSettingHistory__c history = base.Histories__r[0];
			TimeSettingResource.TimeSetting dto = res.records[i];

			System.assertEquals(base.Id, dto.id);
			System.assertEquals(base.Code__c, dto.code);
			System.assertEquals(base.CompanyId__c, dto.companyId);
			System.assertEquals(base.SummaryPeriod__c, dto.summaryPeriod);
			System.assertEquals(base.StartDateOfMonth__c, Integer.valueOf(dto.startDateOfMonth));
			System.assertEquals(base.MonthMark__c, dto.monthMark);
			System.assertEquals(base.UseRequest__c, dto.useRequest);
			System.assertEquals(history.Id, dto.historyId);
			System.assertEquals(history.Name_L0__c, dto.name);	// 部署名のデフォルトはL0を返す
			System.assertEquals(history.Name_L0__c, dto.name_L0);
			System.assertEquals(history.Name_L1__c, dto.name_L1);
			System.assertEquals(history.Name_L2__c, dto.name_L2);
			System.assertEquals(history.ValidFrom__c, dto.validDateFrom);
			System.assertEquals(history.ValidTo__c, dto.validDateTo);
		}
	}

	/**
	 * 週の工数設定レコードを検索する(正常系)
	 */
	@isTest
	static void searchWeeklyTest() {
		ResourceTestData testData = new ResourceTestData();
		TimeSettingBaseEntity setting = testData.timeSetting;
		setting.setSummaryPeriod('Week');
		setting.setStartDayOfWeek('Tuesday');
		update setting.createSObject();
		// ダミーを作っておく
		List<TimeSettingBase__c> settings = ComTestDataUtility.createTimeSettingsWithHistory('Test設定', testData.company.id, 3);

		Test.startTest();
		TimeSettingResource.SearchCondition param = new TimeSettingResource.SearchCondition();
		param.id = setting.id;
		TimeSettingResource.SearchApi api = new TimeSettingResource.SearchApi();
		TimeSettingResource.SearchResult res = (TimeSettingResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 工数設定レコードが取得できること
		System.assertEquals(1, res.records.size());
		TimeSettingResource.TimeSetting result = res.records[0];
		TimeSettingHistoryEntity history = setting.getHistory(0);
		System.assertEquals(setting.id, result.id);
		System.assertEquals(setting.code, result.code);
		System.assertEquals(setting.companyId, result.companyId);
		System.assertEquals(String.valueOf(setting.summaryPeriod), result.summaryPeriod);
		System.assertEquals(String.valueOf(setting.startDayOfWeek), result.startDayOfWeek);
		System.assertEquals(String.valueOf(setting.monthMark), result.monthMark);
		System.assertEquals(setting.useRequest, result.useRequest);
		System.assertEquals(history.Id, result.historyId);
		System.assertEquals(history.nameL0, result.name_L0);
		System.assertEquals(history.nameL1, result.name_L1);
		System.assertEquals(history.nameL2, result.name_L2);
		System.assertEquals(AppConverter.dateValue(history.validFrom), result.validDateFrom);
		System.assertEquals(AppConverter.dateValue(history.validTo), result.validDateTo);
	}
}
