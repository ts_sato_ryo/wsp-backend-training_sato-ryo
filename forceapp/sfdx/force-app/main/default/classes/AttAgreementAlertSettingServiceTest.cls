/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttAgreementAlertSettingServiceのテスト
 */
@isTest
private class AttAgreementAlertSettingServiceTest {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * テストデータクラス
	 */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * 残業設定オブジェクトを作成する
		 */
		public List<AttAgreementAlertSettingEntity> createAgreementAlertSettings(String name, Integer size) {
			return createAgreementAlertSettings(name, this.company.id, size);
		}

		/**
		 *アラート設定オブジェクトを作成す(DB保存あり)
		 */
		public List<AttAgreementAlertSettingEntity> createAgreementAlertSettings(
				String name, Id companyId, Integer size) {
			ComTestDataUtility.createAgreementAlertSettings(name, companyId, size);
			return (new AttAgreementAlertSettingRepository()).getEntityList(null);
		}

		/**
		 * アラート設定エンティティを作成する(DB保存なし)
		 */
		public AttAgreementAlertSettingEntity createAgreementAlertSettingEntity(String name) {
			AttAgreementAlertSettingEntity entity = new AttAgreementAlertSettingEntity();

			entity.name = name;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.code = name;
			entity.companyId = this.company.id;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);
			entity.monthlyAgreementHourWarning1 = 30;
			entity.monthlyAgreementHourWarning2 = 40;
			entity.monthlyAgreementHourLimit = 45;
			entity.monthlyAgreementHourWarningSpecial1 = 50;
			entity.monthlyAgreementHourWarningSpecial2 = 55;
			entity.monthlyAgreementHourLimitSpecial = 60;

			return entity;
		}

	}

	/**
	 * createNewAlertSettingのテスト
	 * 新規のアラート設定が保存できることを確認する
	 */
	@isTest static void createNewAlertSettingTest() {
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity newEntity = testData.createAgreementAlertSettingEntity('New Setting');

		Id resId = (new AttAgreementAlertSettingService()).createNewAlertSetting(newEntity);
		System.assertNotEquals(null, resId);

		AttAgreementAlertSettingEntity savedEntity =
				new AttAgreementAlertSettingRepository().getEntity(resId);
		System.assertNotEquals(null, savedEntity);
		assertSaveAllField(newEntity, savedEntity);
	}

	/**
	 * createNewAlertSettingのテスト
	 * 警告1の値がnullの場合、警告2の値が警告1に移動することを確認する
	 */
	@isTest static void createNewAlertSettingTestSlideWarningHour() {
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity newEntity = testData.createAgreementAlertSettingEntity('New Setting');
		newEntity.monthlyAgreementHourWarning1 = null;
		newEntity.monthlyAgreementHourWarningSpecial1 = null;

		Id resId = (new AttAgreementAlertSettingService()).createNewAlertSetting(newEntity);
		System.assertNotEquals(null, resId);

		AttAgreementAlertSettingEntity savedEntity =
				new AttAgreementAlertSettingRepository().getEntity(resId);
		System.assertNotEquals(null, savedEntity);
		// 警告1に警告2の値が移動している
		System.assertEquals(newEntity.monthlyAgreementHourWarning2, savedEntity.monthlyAgreementHourWarning1);
		System.assertEquals(newEntity.monthlyAgreementHourWarningSpecial2, savedEntity.monthlyAgreementHourWarningSpecial1);

	}

	/**
	 * createNewAlertSettingのテスト
	 * アラート設定のバリデーションが実行され、不正な値が設定されている場合はアプリ例外が発生することを確認する
	 */
	@isTest static void createNewAlertSettingTestValidation() {
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity newEntity = testData.createAgreementAlertSettingEntity('New Setting');
		newEntity.nameL0 = '';

		try {
			Id resId = (new AttAgreementAlertSettingService()).createNewAlertSetting(newEntity);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
			String msg = MESSAGE.Com_Err_NotSetValue(new List<String>{MESSAGE.Admin_Lbl_Name});
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * createNewAlertSettingのテスト
	 * すでに登録されているコードと重複している場合、アプリ例外が発生することを確認する
	 */
	@isTest static void createNewAlertSettingTestDuplicateCode() {
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity testSetting =
				testData.createAgreementAlertSettings('テスト', 1).get(0);
		AttAgreementAlertSettingEntity newEntity = testData.createAgreementAlertSettingEntity('New Setting');
		newEntity.code = testSetting.code;

		try {
			Id resId = (new AttAgreementAlertSettingService()).createNewAlertSetting(newEntity);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Com_Err_DuplicateCode;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * createNewAlertSettingのテスト
	 * アラート設定の全ての更新できることを確認する
	 */
	@isTest static void updateAlertSettingTestAllField() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();

		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('Test', testSettingSize).get(0);
		targetSetting.nameL0 = targetSetting.nameL0 + 'Update';
		targetSetting.nameL1 = targetSetting.nameL1 + 'Update';
		targetSetting.nameL2 = targetSetting.nameL2 + 'Update';
		targetSetting.code = targetSetting.id;
		targetSetting.companyId = testData.company.id;
		targetSetting.validFrom = targetSetting.validFrom.addDays(10);
		targetSetting.validTo = targetSetting.validTo.addDays(10);
		targetSetting.monthlyAgreementHourWarning1 = targetSetting.monthlyAgreementHourWarning1 - 1;
		targetSetting.monthlyAgreementHourWarning2 = targetSetting.monthlyAgreementHourWarning2 - 1;
		targetSetting.monthlyAgreementHourLimit = targetSetting.monthlyAgreementHourLimit - 1;
		targetSetting.monthlyAgreementHourWarningSpecial1 = targetSetting.monthlyAgreementHourWarningSpecial1 - 1;
		targetSetting.monthlyAgreementHourWarningSpecial2 = targetSetting.monthlyAgreementHourWarningSpecial2 - 1;
		targetSetting.monthlyAgreementHourLimitSpecial = targetSetting.monthlyAgreementHourLimitSpecial - 1;

		// 更新実行
		(new AttAgreementAlertSettingService()).updateAlertSetting(targetSetting);

		// 確認
		AttAgreementAlertSettingEntity savedEntity =
				new AttAgreementAlertSettingRepository().getEntity(targetSetting.id);
		assertSaveAllField(targetSetting, savedEntity);
	}

	/**
	 * updateAlertSettingのテスト
	 * アラート設定エンティティの一部の項目だけでも更新できることを確認する
	 */
	@isTest static void updateAlertSettingTestSomeField() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();

		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('Test', testSettingSize).get(0);

		// 更新用のエンティティ
		AttAgreementAlertSettingEntity updateSetting = new AttAgreementAlertSettingEntity();
		updateSetting.setId(targetSetting.id);
		updateSetting.nameL0 = targetSetting.nameL0 + 'Update';
		updateSetting.monthlyAgreementHourLimit = targetSetting.monthlyAgreementHourLimit - 1;

		// 更新実行
		(new AttAgreementAlertSettingService()).updateAlertSetting(updateSetting);

		// 確認
		AttAgreementAlertSettingEntity savedEntity =
				new AttAgreementAlertSettingRepository().getEntity(updateSetting.id);
		System.assertEquals(updateSetting.nameL0, savedEntity.nameL0);
		System.assertEquals(updateSetting.monthlyAgreementHourLimit, savedEntity.monthlyAgreementHourLimit);
		// 他の項目の値は更新されていない
		System.assertEquals(targetSetting.nameL1, savedEntity.nameL1);
	}

	/**
	 * updateAlertSettingのテスト
	 * 警告1の値がnullの場合、警告2の値が警告1に移動することを確認する
	 */
	@isTest static void updateAlertSettingTestSlideWarningHour() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();

		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('Test', testSettingSize).get(0);

		// 更新用のエンティティ
		AttAgreementAlertSettingEntity updateSetting = new AttAgreementAlertSettingEntity();
		updateSetting.setId(targetSetting.id);
		updateSetting.monthlyAgreementHourWarning1 = null;
		updateSetting.monthlyAgreementHourWarningSpecial1 = null;

		// 更新実行
		(new AttAgreementAlertSettingService()).updateAlertSetting(updateSetting);

		// 確認
		AttAgreementAlertSettingEntity savedEntity =
				new AttAgreementAlertSettingRepository().getEntity(updateSetting.id);

		System.assertNotEquals(null, savedEntity);
		// 警告1に警告2の値が移動している
		System.assertEquals(targetSetting.monthlyAgreementHourWarning2, savedEntity.monthlyAgreementHourWarning1);
		System.assertEquals(targetSetting.monthlyAgreementHourWarningSpecial2, savedEntity.monthlyAgreementHourWarningSpecial1);

	}

	/**
	 * updateAlertSettingのテスト
	 * アラート設定のバリデーションが実行され、不正な値が設定されている場合はアプリ例外が発生することを確認する
	 */
	@isTest static void updateAlertSettingTestValidation() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('Test', testSettingSize).get(0);
		targetSetting.nameL0 = null;

		try {
			(new AttAgreementAlertSettingService()).updateAlertSetting(targetSetting);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
			String msg = MESSAGE.Com_Err_NotSetValue(new List<String>{MESSAGE.Admin_Lbl_Name});
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * updateAlertSettingのテスト
	 * すでに登録されているコードと重複している場合、アプリ例外が発生することを確認する
	 */
	@isTest static void updateAlertSettingTestDuplicateCode() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();
		List<AttAgreementAlertSettingEntity> targetSettings =
				testData.createAgreementAlertSettings('Test', testSettingSize);
		AttAgreementAlertSettingEntity targetSetting = targetSettings[1];
		targetSetting.code = targetSettings[2].code;

		try {
			(new AttAgreementAlertSettingService()).updateAlertSetting(targetSetting);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Com_Err_DuplicateCode;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * updateAlertSettingのテスト
	 * 更新対象のレコードが存在しない場合、アプリ例外が発生することを確認する
	 */
	@isTest static void updateAlertSettingTestRecordNotFound() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingServiceTest.TestData testData = new TestData();
		List<AttAgreementAlertSettingEntity> targetSettings =
				testData.createAgreementAlertSettings('Test', testSettingSize);
		AttAgreementAlertSettingEntity targetSetting = targetSettings[1];
		(new AttAgreementAlertSettingRepository()).deleteEntity(targetSetting.id);

		try {
			(new AttAgreementAlertSettingService()).updateAlertSetting(targetSetting);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.RecordNotFoundException e) {
			String msg = MESSAGE.Com_Err_RecordNotFound;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * 検索で取得した結果の各項目の値が期待値通りであることを確認する
	 */
	private static void assertSaveAllField(
			AttAgreementAlertSettingEntity expEntity, AttAgreementAlertSettingEntity actEntity) {

		System.assertEquals(expEntity.validFrom, actEntity.validFrom);
		System.assertEquals(expEntity.validTo, actEntity.validTo);
		System.assertEquals(expEntity.nameL0, actEntity.nameL0);
		System.assertEquals(expEntity.nameL1, actEntity.nameL1);
		System.assertEquals(expEntity.nameL2, actEntity.nameL2);
		System.assertEquals(expEntity.code, actEntity.code);
		System.assertEquals(expEntity.companyId, actEntity.companyId);
		System.assertEquals(expEntity.monthlyAgreementHourWarning1, actEntity.monthlyAgreementHourWarning1);
		System.assertEquals(expEntity.monthlyAgreementHourWarning2, actEntity.monthlyAgreementHourWarning2);
		System.assertEquals(expEntity.monthlyAgreementHourLimit, actEntity.monthlyAgreementHourLimit);
		System.assertEquals(expEntity.monthlyAgreementHourWarningSpecial1, actEntity.monthlyAgreementHourWarningSpecial1);
		System.assertEquals(expEntity.monthlyAgreementHourWarningSpecial2, actEntity.monthlyAgreementHourWarningSpecial2);
		System.assertEquals(expEntity.monthlyAgreementHourLimitSpecial, actEntity.monthlyAgreementHourLimitSpecial);

	}

}
