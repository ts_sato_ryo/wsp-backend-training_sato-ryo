@isTest
private class ParentChildHistoryEntityTest {

	private class HistoryTestData extends TestData.TestDataEntity {
	}

	private static ParentChildHistoryEntity createHistory() {

		HistoryTestData testData = new HistoryTestData();
		ParentChildHistoryEntity history = testData.timesetting.getHistory(0);

		return history;
	}

	/**
	 * 履歴のユニークキーが作成できることを確認する
	 * Code__c + '-' + ValidFrom__c + '-' + ValidTo__c ※日付はyyyyMMdd
	 */
	@isTest static void createUniqKeyTest() {
		ParentChildHistoryEntity history = createHistory();
		String code = 'testCode';
		String expUniq = code + '-' + history.validFrom.formatYYYYMMDD() + '-' + history.validTo.formatYYYYMMDD();
		System.assertEquals(expUniq, history.createUniqKey(code));
	}

	/**
	 * 論理削除状態に設定できることを確認する
	 */
	@isTest static void setLogicallyDeleteTest() {
		ParentChildHistoryEntity history = createHistory();

		// この状態では論理削除ではない
		System.assertEquals(false, history.isRemoved);

		// 論理削除にする
		history.setLogicallyDelete();

		// 論理削除状態になっている。ユニークキーも更新されている
		System.assertEquals(true, history.isRemoved);
		System.assertEquals(history.id, history.uniqKey);
	}

	/**
	 * 指定した日に履歴が有効であるかどうかを正しく判定できることを確認する(有効開始日と失効日が設定されている場合)
	 */
	@isTest static void isValidTestValidDateNotNull() {
		ParentChildHistoryEntity history = createHistory();
		history.validFrom = AppDate.newInstance(2019, 4, 5);
		history.validTo = AppDate.newInstance(2019, 5, 6);

		// 有効開始日の前日の場合は無効
		System.assertEquals(false, history.isValid(history.validFrom.addDays(-1)));

		// 有効開始日の場合は有効
		System.assertEquals(true, history.isValid(history.validFrom));

		// 失効日の前日の場合は有効
		System.assertEquals(true, history.isValid(history.validTo.addDays(-1)));

		// 失効日の場合は有効
		System.assertEquals(false, history.isValid(history.validTo));
	}

	/**
	 * 指定した日に履歴が有効であるかどうかを正しく判定できることを確認する(有効開始日と失効日が設定されていない場合)
	 */
	@isTest static void isValidTestValidDateNull() {
		ParentChildHistoryEntity history = createHistory();

		// 有効開始日が未設定の場合は無効
		history.validFrom = null;
		history.validTo = AppDate.newInstance(2019, 5, 6);
		System.assertEquals(false, history.isValid(history.validTo.addDays(-1)));

		// 失効日が未設定の場合は有効
		history.validFrom = AppDate.newInstance(2019, 4, 6);
		history.validTo = null;
		System.assertEquals(false, history.isValid(history.validFrom.addDays(1)));

		// 有効開始日、失効日が未設定の場合は有効
		history.validFrom = null;
		history.validTo = null;
		System.assertEquals(false, history.isValid(AppDate.today()));
	}
}
