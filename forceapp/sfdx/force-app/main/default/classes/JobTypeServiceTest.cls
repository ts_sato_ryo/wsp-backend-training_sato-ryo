/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description JobTypeServiceのテスト
 */
@isTest
private class JobTypeServiceTest {

	/**
	 * 本クラス内の全てのテストメソッドで利用するテストデータを登録する
	 */
	@testSetup static void setup() {
		// マスタデータを登録しておく
		TestData.setupMaster();
	}


	/**
	 * テストデータクラス
	 */
	private class ServiceTestData extends TestData.TestDataEntity {

	}

	/**
	 * saveJobTypeWithWorkCategoryListテスト
	 * 新規のジョブタイプが登録できることを確認する
	 */
	@isTest static void saveJobTypeWithWorkCategoryListTestNew() {
		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);

		JobTypeEntity jobType = new JobTypeEntity();
		jobType.name = 'Test';
		jobType.nameL0 = 'Test_L0';
		jobType.nameL1 = 'Test_L1';
		jobType.nameL2 = 'Test_L2';
		jobType.code = 'Test001';
		jobType.companyId = testData.company.id;

		List<Id> workCategoryIdList = new List<Id>();
		for (WorkCategoryEntity workCategory : workCategories) {
			workCategoryIdList.add(workCategory.id);
		}

		// 保存する
		Id savedId = new JobTypeService().saveJobTypeWithWorkCategoryList(jobType, workCategoryIdList);

		// 保存できていることを確認する
		JobTypeEntity savedJobType = new JobTypeRepository().getEntity(savedId);
		TestUtil.assertNotNull(savedJobType);
		System.assertEquals(jobType.nameL0, savedJobType.nameL0);
		System.assertEquals(jobType.nameL1, savedJobType.nameL1);
		System.assertEquals(jobType.nameL2, savedJobType.nameL2);
		System.assertEquals(jobType.code, savedJobType.code);
		// ユニークキーが設定されている
		System.assertEquals(testData.company.code + '-' + jobType.code, savedJobType.uniqKey);
		// ジョブタイプ別作業分類が登録されている
		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(savedId);
		System.assertEquals(workCategorySize, savedJobTypeWorkCategories.size());
	}

	/**
	 * saveJobTypeWithWorkCategoryListテスト
	 * 既存のジョブタイプが更新できることを確認する
	 */
	@isTest static void saveJobTypeWithWorkCategoryListTestUpdate() {
		JobTypeWorkCategoryRepository jobTypeWorkCategoryRepo = new JobTypeWorkCategoryRepository();
		JobTypeRepository jobTypeRepo = new JobTypeRepository();

		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');

		// ジョブタイプ別作業分類を登録する
		JobTypeWorkCategoryEntity jobWorkCategory = new JobTypeWorkCategoryEntity();
		jobWorkCategory.jobTypeId = jobType.id;
		jobWorkCategory.workCategoryId = workCategories[0].Id;
		jobWorkCategory.setId(jobTypeWorkCategoryRepo.saveEntity(jobWorkCategory).details[0].Id);

		jobType.name = jobType.name + '_Update';
		jobType.nameL0 = jobType.nameL0 + '_Update';
		jobType.nameL1 = jobType.nameL1 + '_Update';
		jobType.nameL2 = jobType.nameL2 + '_Update';
		jobType.code = jobType.code + '_Update';

		List<Id> workCategoryIdList = new List<Id>{workCategories[2].Id};

		// 保存する
		Id savedId = new JobTypeService().saveJobTypeWithWorkCategoryList(jobType, workCategoryIdList);

		// 保存できていることを確認する
		JobTypeEntity savedJobType = new JobTypeRepository().getEntity(savedId);
		TestUtil.assertNotNull(savedJobType);
		System.assertEquals(jobType.nameL0, savedJobType.nameL0);
		System.assertEquals(jobType.nameL1, savedJobType.nameL1);
		System.assertEquals(jobType.nameL2, savedJobType.nameL2);
		System.assertEquals(jobType.code, savedJobType.code);
		// ユニークキーが設定されている
		System.assertEquals(testData.company.code + '-' + jobType.code, savedJobType.uniqKey);
		// ジョブタイプ別作業分類が登録されている
		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				jobTypeWorkCategoryRepo.getEntityListByJobType(savedId);
		System.assertEquals(workCategoryIdList.size(), savedJobTypeWorkCategories.size());
		// 既存のジョブタイプ別作業分類は削除されている
		System.assertEquals(null, jobTypeWorkCategoryRepo.getEntity(jobWorkCategory.id));
	}

	/**
	 * saveJobTypeWithWorkCategoryListテスト
	 * ジョブタイプに紐付ける作業分類リストが空でも問題なく登録できることを確認する
	 */
	@isTest static void saveJobTypeWithWorkCategoryListTesEmptyWorkCategoryList() {
		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);

		JobTypeEntity jobType = new JobTypeEntity();
		jobType.name = 'Test';
		jobType.nameL0 = 'Test_L0';
		jobType.nameL1 = 'Test_L1';
		jobType.nameL2 = 'Test_L2';
		jobType.code = 'Test001';
		jobType.companyId = testData.company.id;

		List<Id> workCategoryIdList = new List<Id>();

		// 保存する
		Id savedId = new JobTypeService().saveJobTypeWithWorkCategoryList(jobType, workCategoryIdList);

		// 保存できていることを確認する
		JobTypeEntity savedJobType = new JobTypeRepository().getEntity(savedId);
		TestUtil.assertNotNull(savedJobType);
		System.assertEquals(jobType.nameL0, savedJobType.nameL0);
		System.assertEquals(jobType.nameL1, savedJobType.nameL1);
		System.assertEquals(jobType.nameL2, savedJobType.nameL2);
		System.assertEquals(jobType.code, savedJobType.code);
		// ユニークキーが設定されている
		System.assertEquals(testData.company.code + '-' + jobType.code, savedJobType.uniqKey);
		// ジョブタイプ別作業分類が登録されていない
		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(savedId);
		System.assertEquals(workCategoryIdList.size(), savedJobTypeWorkCategories.size());
	}

	/**
	 * saveJobTypeWithWorkCategoryListテスト
	 * コードが重複している場合に想定されている例外が発生することを確認する
	 */
	@isTest static void saveJobTypeWithWorkCategoryListTestDuplicateCode() {
		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity testJobType = testData.createJobType('TestJobType');

		JobTypeEntity jobType = new JobTypeEntity();
		jobType.name = 'Test';
		jobType.nameL0 = 'Test_L0';
		jobType.nameL1 = 'Test_L1';
		jobType.nameL2 = 'Test_L2';
		jobType.code = testJobType.code;
		jobType.companyId = testData.company.id;

		List<Id> workCategoryIdList = new List<Id>();
		for (WorkCategoryEntity workCategory : workCategories) {
			workCategoryIdList.add(workCategory.id);
		}

		// 保存する
		App.ParameterException resEx;
		try {
			Id savedId = new JobTypeService().saveJobTypeWithWorkCategoryList(jobType, workCategoryIdList);
		} catch(App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, resEx.getMessage());
	}

	/**
	 * saveJobTypeWithWorkCategoryListテスト
	 * ジョブタイプの情報が正しく設定されていない場合、想定されている例外が発生することを確認する
	 */
	@isTest static void saveJobTypeWithWorkCategoryListTestInvalidJobType() {
		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);

		// コードを空文字にしておく
		JobTypeEntity jobType = new JobTypeEntity();
		jobType.name = 'Test';
		jobType.nameL0 = 'Test_L0';
		jobType.nameL1 = 'Test_L1';
		jobType.nameL2 = 'Test_L2';
		jobType.code = '';
		jobType.companyId = testData.company.id;

		List<Id> workCategoryIdList = new List<Id>();
		for (WorkCategoryEntity workCategory : workCategories) {
			workCategoryIdList.add(workCategory.id);
		}

		// 保存する
		App.ParameterException resEx;
		try {
			Id savedId = new JobTypeService().saveJobTypeWithWorkCategoryList(jobType, workCategoryIdList);
		} catch(App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		String exMessage = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code});
		System.assertEquals(exMessage, resEx.getMessage());
	}

	/**
	 * searchJobTypeListWithWorkCategory テスト
	 * 会社IDを指定した場合、正しくジョブタイプとジョブタイプ別作業分類が取得できることを確認する
	 */
	@isTest static void searchJobTypeListWithWorkCategoryTestCompanyId() {
		// テストデータ作成
		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity testJobType = testData.createJobType('TestJobType');
		JobTypeEntity testJobType2 = testData.createJobType('TestJobType2');
		List<JobTypeWorkCategoryEntity> jobTypeWorkCategoryList = new List<JobTypeWorkCategoryEntity>();
		for (WorkCategoryEntity workCategory : workCategories) {
			JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryEntity();
			entity.jobTypeId = testJobType.id;
			entity.workCategoryId = workCategory.id;
			jobTypeWorkCategoryList.add(entity);
		}
		new JobTypeWorkCategoryRepository().saveEntityList(jobTypeWorkCategoryList);

		CompanyEntity targetCompany = testData.createCompany('検索テスト');
		testJobType.companyId = targetCompany.id;
		new JobTypeRepository().saveEntity(testJobType);

		// 検索する
		List<JobTypeService.JobTypeWithWorkCategory> resJobTypes =
				new JobTypeService().searchJobTypeListWithWorkCategory(targetCompany.id, null);

		// 結果確認
		System.assertEquals(1, resJobTypes.size());
		JobTypeEntity resJobType = resJobTypes[0].jobType;
		System.assertEquals(testJobType.id, resJobType.id);
		List<JobTypeWorkCategoryEntity> resJobTypeWorkCategories = resJobTypes[0].workCategoryList;
		System.assertEquals(workCategorySize, resJobTypeWorkCategories.size());
		System.assertEquals(testJobType.id, resJobTypeWorkCategories[0].jobTypeId);
		System.assertEquals(workCategories[0].id, resJobTypeWorkCategories[0].workCategoryId);
	}

	/**
	 * searchJobTypeListWithWorkCategory テスト
	 * ジョブタイプIDを指定した場合、正しくジョブタイプとジョブタイプ別作業分類が取得できることを確認する
	 */
	@isTest static void searchJobTypeListWithWorkCategoryTestJobTypeId() {
		// テストデータ作成
		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity testJobType = testData.createJobType('TestJobType');
		JobTypeEntity testJobType2 = testData.createJobType('TestJobType2');
		List<JobTypeWorkCategoryEntity> jobTypeWorkCategoryList = new List<JobTypeWorkCategoryEntity>();
		for (WorkCategoryEntity workCategory : workCategories) {
			JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryEntity();
			entity.jobTypeId = testJobType2.id;
			entity.workCategoryId = workCategory.id;
			jobTypeWorkCategoryList.add(entity);
		}
		new JobTypeWorkCategoryRepository().saveEntityList(jobTypeWorkCategoryList);

		// 検索する
		List<JobTypeService.JobTypeWithWorkCategory> resJobTypes =
				new JobTypeService().searchJobTypeListWithWorkCategory(null, testJobType2.id);

		// 結果確認
		System.assertEquals(1, resJobTypes.size());
		JobTypeEntity resJobType = resJobTypes[0].jobType;
		System.assertEquals(testJobType2.id, testJobType2.id);
		List<JobTypeWorkCategoryEntity> resJobTypeWorkCategories = resJobTypes[0].workCategoryList;
		System.assertEquals(workCategorySize, resJobTypeWorkCategories.size());
		System.assertEquals(testJobType2.id, resJobTypeWorkCategories[0].jobTypeId);
		System.assertEquals(workCategories[0].id, resJobTypeWorkCategories[0].workCategoryId);
	}

	/**
	 * deleteJobType テスト
	 * 指定したジョブタイプが削除できることを確認する
	 */
	@isTest static void deleteJobTypeTestDeleteJobType() {
		// テストデータ作成
		final Integer workCategorySize = 3;
		ServiceTestData testData = new ServiceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity testJobType = testData.createJobType('TestJobType');
		JobTypeEntity testJobType2 = testData.createJobType('TestJobType2');
		List<JobTypeWorkCategoryEntity> jobTypeWorkCategoryList = new List<JobTypeWorkCategoryEntity>();
		for (WorkCategoryEntity workCategory : workCategories) {
			JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryEntity();
			entity.jobTypeId = testJobType2.id;
			entity.workCategoryId = workCategory.id;
			jobTypeWorkCategoryList.add(entity);
		}
		new JobTypeWorkCategoryRepository().saveEntityList(jobTypeWorkCategoryList);

		// 削除する
		Id deleteId = testJobType2.id;
		new JobTypeService().deleteJobType(deleteId);

		// 結果確認
		JobTypeEntity resJobType = new JobTypeRepository().getEntity(deleteId);
		System.assertEquals(null, resJobType);
		List<JobTypeWorkCategoryEntity> resJotTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(deleteId);
		System.assertEquals(0, resJotTypeWorkCategories.size());
	}

	/**
	 * ジョブタイプに紐づく作業分類のレコード数の境界値を検証する
	 */
	@isTest static void validateJobTypeWorkCategorySizeBoundaryTest() {
		JobTypeService service = new JobTypeService();

		// 上限値 - 1件 → OK
		List<JobTypeWorkCategoryEntity> entities = new List<JobTypeWorkCategoryEntity>();
		for (Integer i = 0; i < JobTypeService.LIMIT_WORKCATEGORY_SIZE_EACH_JOBTYPE - 1; i++) {
			entities.add(new JobTypeWorkCategoryEntity());
		}
		service.validateJobTypeWorkCategory(entities);

		// 上限値 → OK
		entities.add(new JobTypeWorkCategoryEntity());
		service.validateJobTypeWorkCategory(entities);

		// 上限値 + 1件 → NG
		entities.add(new JobTypeWorkCategoryEntity());
		try {
			service.validateJobTypeWorkCategory(entities);
			System.assert(false);
		} catch (App.ParameterException e) {
			String expected = ComMessage.msg().Time_Err_OverWorkCategoryLimitSize(
				new List<String>{String.valueOf(JobTypeService.LIMIT_WORKCATEGORY_SIZE_EACH_JOBTYPE)});
			System.assertEquals(expected, e.getMessage());
		}
	}
}