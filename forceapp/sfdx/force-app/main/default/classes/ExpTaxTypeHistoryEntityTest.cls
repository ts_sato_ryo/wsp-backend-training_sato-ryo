/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分履歴エンティティを表すクラスのテスト Test class for ExpTaxTypeHistoryEntity
 */
@isTest
private with sharing class ExpTaxTypeHistoryEntityTest {

	/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') Org setting object */
	private static ComOrgSetting__c orgObj;
	/** 会社オブジェクト Company object */
	private static ComCompany__c companyObj;

	/** テスト用の履歴エンティティを作成する Create history entity(without saving to database)*/
	private static ExpTaxTypeHistoryEntity createHistoryEntity() {
		if (orgObj == null) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		}
		if (companyObj == null) {
			companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
		}

		ExpTaxTypeHistoryEntity history = new ExpTaxTypeHistoryEntity();
		history.baseId = UserInfo.getUserId();
		history.historyComment = 'テスト履歴コメント';
		history.validFrom = AppDate.newInstance(2017, 1, 11);
		history.validTo = AppDate.newInstance(2017, 2, 11);
		history.isRemoved = true;
		history.uniqKey = history.validFrom.format() + history.validTo.format();
		history.name = 'テスト履歴';
		history.nameL0 = 'テスト履歴_L0';
		history.nameL1 = 'テスト履歴_L1';
		history.nameL2 = 'テスト履歴_L2';
		history.rate = 8.0;

		return history;
	}

	/**
	 * エンティティを複製できることを確認する Confirm the entity is copied properly
	 */
	@isTest static void copyTest() {
		ExpTaxTypeHistoryEntity history = createHistoryEntity();

		ExpTaxTypeHistoryEntity copyHistory = history.copy();

		System.assertEquals(history.nameL0, copyHistory.nameL0);
		System.assertEquals(history.nameL1, copyHistory.nameL1);
		System.assertEquals(history.nameL2, copyHistory.nameL2);
		System.assertEquals(history.rate, copyHistory.rate);

		System.assertEquals(history.name, copyHistory.name);
		System.assertEquals(history.baseId, copyHistory.baseId);
		System.assertEquals(history.historyComment, copyHistory.historyComment);
		System.assertEquals(history.validFrom, copyHistory.validFrom);
		System.assertEquals(history.validTo, copyHistory.validTo);
		System.assertEquals(history.isRemoved, copyHistory.isRemoved);
		System.assertEquals(history.uniqKey, copyHistory.uniqKey);
	}
}