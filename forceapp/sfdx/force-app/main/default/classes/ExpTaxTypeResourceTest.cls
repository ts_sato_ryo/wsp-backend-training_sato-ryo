/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description ExpTaxTypeResourceのテストクラス
 */
@isTest
private class ExpTaxTypeResourceTest {

	/**
	 * 税区分レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Date currentDate = Date.today();

		Test.startTest();

		ExpTaxTypeResource.ExpTaxType param = new ExpTaxTypeResource.ExpTaxType();
		param.name_L0 = 'テスト税区分_L0';
		param.name_L1 = 'テスト税区分_L1';
		param.name_L2 = 'テスト税区分_L2';
		param.code = '001';
		param.countryId = null;
		param.companyId = company.Id;
		param.rate = 8.0;
		param.validDateFrom = currentDate - 30;
		param.validDateTo = currentDate;
		param.comment = 'テストコメント';

		ExpTaxTypeResource.CreateApi api = new ExpTaxTypeResource.CreateApi();
		ExpTaxTypeResource.SaveResult res = (ExpTaxTypeResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 税区分ベース・履歴レコードが1件ずつ作成されること
		List<ExpTaxTypeBase__c> newBaseRecords = [
			SELECT
				Id,
				Name,
				Code__c,
				CountryId__c,
				CompanyId__c,
				CurrentHistoryId__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						Rate__c,
						ValidFrom__c,
						ValidTo__c,
						HistoryComment__c
					FROM ExpTaxTypeHistories__r)
			FROM ExpTaxTypeBase__c];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].ExpTaxTypeHistories__r.size());

		// 値の検証
		ExpTaxTypeBase__c newBase = newBaseRecords[0];
		ExpTaxTypeHistory__c newHistory = newBase.ExpTaxTypeHistories__r[0];

		// レスポンス値の検証
		System.assertEquals(newBase.Id, res.Id);

		// ベースレコード値の検証
		System.assertEquals(param.name_L0, newBase.Name);
		System.assertEquals(param.code, newBase.Code__c);
		System.assertEquals(param.countryId, newBase.CountryId__c);
		System.assertEquals(param.companyId, newBase.CompanyId__c);
		System.assertEquals(newHistory.Id, newBase.CurrentHistoryId__c);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = company.Code__c + '-' + param.code +
			'-' + DateTime.newInstance(param.validDateFrom, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd') +
			'-' + DateTime.newInstance(param.validDateTo, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd');
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.name_L0, newHistory.Name);
		System.assertEquals(param.name_L0, newHistory.Name_L0__c);
		System.assertEquals(param.name_L1, newHistory.Name_L1__c);
		System.assertEquals(param.name_L2, newHistory.Name_L2__c);
		System.assertEquals(param.rate, newHistory.Rate__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
	}

	/**
	 * 税区分レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		Date currentDate = Date.today();

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageExpTaxType = false;
		new PermissionRepository().saveEntity(testData.permission);

		ExpTaxTypeResource.ExpTaxType param = new ExpTaxTypeResource.ExpTaxType();
		param.name_L0 = 'テスト税区分_L0';
		param.name_L1 = 'テスト税区分_L1';
		param.name_L2 = 'テスト税区分_L2';
		param.code = '001';
		param.countryId = null;
		param.companyId = testData.company.id;
		param.rate = 8.0;
		param.validDateFrom = currentDate - 30;
		param.validDateTo = currentDate;
		param.comment = 'テストコメント';

		App.NoPermissionException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				ExpTaxTypeResource.CreateApi api = new ExpTaxTypeResource.CreateApi();
				Object res = api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.runAs(runAsUser) {
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 税区分レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void createNegativeTest() {

		ExpTaxTypeResource.ExpTaxType param = new ExpTaxTypeResource.ExpTaxType();
		ExpTaxTypeResource.CreateApi api = new ExpTaxTypeResource.CreateApi();
		App.ParameterException ex;

		Test.startTest();

		try {
			Object res = api.execute(param);
		} catch (App.ParameterException  e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/**
	 * 税区分レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		ComCountry__c country = ComTestDataUtility.createCountry('Test国');
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('Test税区分', null, company.Id);

		Test.startTest();

		Map<String, Object> param = new Map<String, Object> {
			'id' => taxType.id,
			'code' => '002'
		};

		ExpTaxTypeResource.UpdateApi api = new ExpTaxTypeResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// 税区分レコードが更新されていること
		List<ExpTaxTypeBase__c> taxTypeList = [
			SELECT Code__c
			FROM ExpTaxTypeBase__c
			WHERE Id =:(String)param.get('id')];

		System.assertEquals(1, taxTypeList.size());
		System.assertEquals((String)param.get('code'), taxTypeList[0].Code__c);
	}

	/**
	 * 税区分レコードを1件更新する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('Test税区分', null, testData.company.Id);

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageExpTaxType = false;
		new PermissionRepository().saveEntity(testData.permission);

		Map<String, Object> param = new Map<String, Object> {
			'id' => taxType.id,
			'code' => '002'
		};

		App.NoPermissionException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				ExpTaxTypeResource.UpdateApi api = new ExpTaxTypeResource.UpdateApi();
				Object res = api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.runAs(runAsUser) {
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 税区分レコードを1件更新する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		ExpTaxTypeResource.ExpTaxType param = new ExpTaxTypeResource.ExpTaxType();
		App.ParameterException ex;

		Test.startTest();

		try {
			Object res = (new ExpTaxTypeResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 税区分レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c delRecord = ComTestDataUtility.createTaxTypeWithHistory('Test税区分', null, company.Id);

		Test.startTest();

		ExpTaxTypeResource.DeleteOption param = new ExpTaxTypeResource.DeleteOption();
		param.id = delRecord.id;

		Object res = (new ExpTaxTypeResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 税区分レコードが削除されること
		List<ExpTaxTypeBase__c> taxTypeList = [SELECT Id FROM ExpTaxTypeBase__c];
		System.assertEquals(0, taxTypeList.size());
	}

	/**
	 * 税区分レコードを1件削除する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		ExpTaxTypeBase__c delRecord = ComTestDataUtility.createTaxTypeWithHistory('Test税区分', null, testData.company.Id);

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		ExpTaxTypeResource.DeleteOption param = new ExpTaxTypeResource.DeleteOption();
		param.id = delRecord.id;

		DmlException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				Object res = (new ExpTaxTypeResource.DeleteApi()).execute(param);
			} catch (DmlException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.runAs(runAsUser) {
			System.assertEquals('INSUFFICIENT_ACCESS_OR_READONLY', actEx.getDmlStatusCode(0));
		}
	}

	/**
	 * 税区分レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {
		ExpTaxTypeResource.DeleteOption param = new ExpTaxTypeResource.DeleteOption();
		App.ParameterException ex;

		Test.startTest();

		try {
			Object res = (new ExpTaxTypeResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 税区分レコードを1件削除する Test for DeleteApi
	 * 削除対象の税区分に紐づく費目が存在する場合、エラーが発生することを確認する
	 * Check exception is thrown if the target record is related to expense type
	 */
	@isTest static void deleteTestRelatedToTaxType() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<ExpTaxTypeBase__c> taxTypes = ComTestDataUtility.createTaxTypesWithHistory('Test税区分', null, company.Id, 3);

		// 対象レコードを費目に紐付け / associate the target record to expense type
		ExpType__c expType = ComTestDataUtility.createExpType('Test費目', company.Id);
		expType.TaxTypeBase1Id__c = taxTypes[0].Id;
		update expType;

		// 削除実行 / delete
		try {
			ExpTaxTypeResource.DeleteOption param = new ExpTaxTypeResource.DeleteOption();
			param.id = taxTypes[0].Id;
			(new ExpTaxTypeResource.DeleteApi()).execute(param);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * 税区分レコードを1件削除する Test for DeleteApi
	 * 削除対象の税区分に紐づく内訳が存在する場合、エラーが発生することを確認する
	 * Check exception is thrown if the target record is related to record item
	 */
	@isTest static void deleteTestRelatedToRecordItem() {
		final Integer testTypeSize = 3;
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType, testData.employee, 3);

		// 対象レコードを内訳に紐付け / associate the target record to record item
		recordList[0].recordItemList[0].taxTypeHistoryId = testData.taxTypeHistory.id;
		(new ExpRecordRepository()).saveEntity(recordList[0]);

		// 削除実行 / delete
		try {
			ExpTaxTypeResource.DeleteOption param = new ExpTaxTypeResource.DeleteOption();
			param.id = testData.taxTypeBase.id;
			(new ExpTaxTypeResource.DeleteApi()).execute(param);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * 税区分レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<ExpTaxTypeBase__c> taxTypes = ComTestDataUtility.createTaxTypesWithHistory('テスト税区分', null, company.Id, 3);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => taxTypes[0].id
		};

		ExpTaxTypeResource.SearchApi api = new ExpTaxTypeResource.SearchApi();
		ExpTaxTypeResource.SearchResult res = (ExpTaxTypeResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 税区分レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 税区分レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('テスト税区分', null, company.Id);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => taxType.id
		};

		ExpTaxTypeResource.SearchApi api = new ExpTaxTypeResource.SearchApi();
		ExpTaxTypeResource.SearchResult res = (ExpTaxTypeResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 税区分レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 多言語対応項目の検証
		ExpTaxTypeHistory__c history = [
				SELECT Name_L1__c
				FROM ExpTaxTypeHistory__c
				WHERE BaseId__c =:taxType.Id LIMIT 1];

		System.assertEquals(history.Name_L1__c, res.records[0].name);
	}

	/**
	 * 税区分レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('テスト税区分', null, company.Id);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => taxType.id
		};

		ExpTaxTypeResource.SearchApi api = new ExpTaxTypeResource.SearchApi();
		ExpTaxTypeResource.SearchResult res = (ExpTaxTypeResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 税区分レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		ExpTaxTypeHistory__c history = [
				SELECT Name_L2__c
				FROM ExpTaxTypeHistory__c
				WHERE BaseId__c =:taxType.Id LIMIT 1];
		System.assertEquals(history.Name_L2__c, res.records[0].name);
	}

	/**
	 * 税区分レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCountry__c country = ComTestDataUtility.createCountry('Test');
		List<ExpTaxTypeBase__c> taxTypes = ComTestDataUtility.createTaxTypesWithHistory('Test税区分', country.Id, null, 3);

		Test.startTest();

		Map<String, Object> param = new Map<String, Object>();
		ExpTaxTypeResource.SearchApi api = new ExpTaxTypeResource.SearchApi();
		ExpTaxTypeResource.SearchResult res = (ExpTaxTypeResource.SearchResult)api.execute(param);

		Test.stopTest();

		taxTypes = [
			SELECT
				Id,
				Name,
				Code__c,
				CountryId__c,
				CompanyId__c,
				CurrentHistoryId__c,
				(SELECT
						Id,
						BaseId__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						Rate__c,
						ValidFrom__c,
						ValidTo__c
				FROM ExpTaxTypeHistories__r)
			FROM ExpTaxTypeBase__c];

		// 税区分レコードが取得できること
		System.assertEquals(taxTypes.size(), res.records.size());

		for (Integer i = 0, n = taxTypes.size(); i < n; i++) {
			ExpTaxTypeBase__c base = taxTypes[i];
			ExpTaxTypeHistory__c history = base.ExpTaxTypeHistories__r[0];
			ExpTaxTypeResource.ExpTaxType dto = res.records[i];

			System.assertEquals(base.Id, dto.id);
			System.assertEquals(base.Code__c, dto.code);
			System.assertEquals(base.CountryId__c, dto.countryId);
			System.assertEquals(base.CompanyId__c, dto.companyId);
			System.assertEquals(history.Id, dto.historyId);
			System.assertEquals(history.Name_L0__c, dto.name);	// 税区分名のデフォルトはL0を返す
			System.assertEquals(history.Name_L0__c, dto.name_L0);
			System.assertEquals(history.Name_L1__c, dto.name_L1);
			System.assertEquals(history.Name_L2__c, dto.name_L2);
			System.assertEquals(history.Rate__c, dto.rate);
			System.assertEquals(history.ValidFrom__c, dto.validDateFrom);
			System.assertEquals(history.ValidTo__c, dto.validDateTo);
		}
	}
}