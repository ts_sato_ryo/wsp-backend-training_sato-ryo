/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Test class for Expense Journal Service
 *
 * @group Expense
 */
@isTest
private class ExpJournalServiceTest {

	/** String representation of Extended Item Field of the sObject */
	private static final String POSTFIX_RECORD_ITEM = ExpJournal__c.RecordItemExtendedItemLookup01__c.getDescribe().getName().substringBefore('ExtendedItem');
	private static final String POSTFIX_REPORT = ExpJournal__c.ReportExtendedItemLookup01__c.getDescribe().getName().substringBefore('ExtendedItem');
	private static final String POSTFIX_REQUEST = ExpJournal__c.RequestExtendedItemLookup01__c.getDescribe().getName().substringBefore('ExtendedItem');
	private static final String EXTENDED_ITEM_DATE = 'ExtendedItemDate';
	private static final String EXTENDED_ITEM_TEXT = 'ExtendedItemText';
	private static final String EXTENDED_ITEM_PICKLIST = 'ExtendedItemPicklist';
	private static final String EXTENDED_ITEM_LOOKUP = 'ExtendedItemLookup';
	private static final String FIELD_POSTFIX= '__c';

	/** Test that the Job and Cost Center of Records are exported according to those of Report when exported to Journal Object */
	@isTest static void exportConfirmedReportsWithJobAndCCToJournalTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity reportEntity = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, testData.costCenter.CurrentHistoryId__c, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportEntity.id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c,3);

		// Update Job and Cost Center
		List<JobEntity> jobList = testData.createJobList(1);
		recordList[0].recordItemList[0].jobId = jobList[0].id;
		recordList[0].recordItemList[0].jobCode = jobList[0].code;
		recordList[1].recordItemList[0].jobId = null;
		recordList[2].recordItemList[0].jobId = null;
		ComCostCenterBase__c newCostCenter =  ComTestDataUtility.createCostCentersWithHistory('New Cost Center', testData.company.Id, 1, 1, true)[0];
		ComCostCenterHistory__c newCCHistory = [SELECT Id, LinkageCode__c, Name_L0__c FROM ComCostCenterHistory__c WHERE Id =: newCostCenter.CurrentHistoryId__c];
		newCCHistory.LinkageCode__c = 'new_cc';
		UPDATE newCCHistory;
		UPDATE newCostCenter;

		recordList[0].recordItemList[0].costCenterHistoryId = null;
		recordList[1].recordItemList[0].costCenterHistoryId = newCostCenter.CurrentHistoryId__c;
		recordList[1].recordItemList[0].costCenterCode = newCCHistory.LinkageCode__c;
		recordList[2].recordItemList[0].costCenterHistoryId = null;

		// Save Records
		Id recordId;
		ExpRecordService recordService = new ExpRecordService();
		recordId = recordService.saveExpRecord(testData.employee, recordList[0], reportEntity.expRequestId, reportEntity.expReportTypeId, false);
		recordId = recordService.saveExpRecord(testData.employee, recordList[1], reportEntity.expRequestId, reportEntity.expReportTypeId, false);
		recordId = recordService.saveExpRecord(testData.employee, recordList[2], reportEntity.expRequestId, reportEntity.expReportTypeId, false);

		setVendorDetails(testData.company, reportEntity);
		// Set Manager
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpReportRequestService service = new ExpReportRequestService();

		 //Submit Report
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportEntity.id, '');

		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();
		ExpReportEntity retrievedReportEntity = new ExpReportRepository().getEntity(reportEntity.id);

		// Set the Authorized Date and Last Approved Time
		ExpReportRequestEntity requestEntity = requestRepo.getEntity(retrievedReportEntity.expRequestId);
		requestEntity.authorizedTime = AppDatetime.now();
		requestEntity.lastApproveTime = AppDatetime.now();
		requestRepo.saveEntity(requestEntity);

		Test.startTest();
		ExpJournalService journalService = new ExpJournalService();
		journalService.exportConfirmedReportsToJournal(new List<Id>{reportEntity.id});
		Test.stopTest();

		List<ExpJournal__c> retObjList = retrieveJournalRecords();
		ComCostCenterHistory__c costCenterHistory;
		ComJob__c job;

		System.assertEquals(3, retObjList.size());
		for (Integer i = 0; i < retObjList.size(); i++) {
			ExpJournal__c retObj = retObjList.get(i);
			ExpRecordEntity record = recordList[i];

			if (i == 0) {
				System.assertEquals(jobList[0].code, retObj.RecordItemJobCode__c);
				System.assertEquals(jobList[0].nameL.getValue(), retObj.RecordItemJobName__c);
			} else {
				System.assertEquals(testData.job.code, retObj.RecordItemJobCode__c);
				System.assertEquals(testData.job.nameL.getValue(), retObj.RecordItemJobName__c);
			}

			if (i == 1) {
				costCenterHistory = [SELECT Id, LinkageCode__c, Name_L0__c FROM ComCostCenterHistory__c WHERE Id =: record.recordItemList[0].costCenterHistoryId LIMIT 1];
			} else {
				costCenterHistory = [SELECT Id, LinkageCode__c, Name_L0__c FROM ComCostCenterHistory__c WHERE Id =: testData.costCenter.CurrentHistoryId__c LIMIT 1];
			}

			// Cost Center
			System.assertEquals(costCenterHistory.LinkageCode__c, retObj.RecordItemCostCenterCode__c);
			System.assertEquals(costCenterHistory.Name_L0__c, retObj.RecordItemCostCenterName__c);
		}
	}

	/** Test that the Job and Cost Center of Records are exported as null when those of Report are null */
	@isTest static void exportConfirmedReportsWithNullJobAndCCToJournalTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity reportEntity = testData.createExpReports(Date.today(), testData.department, testData.employee, null, null, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportEntity.id, Date.today(), testData.expType,
				testData.employee, null,3);

		// Update Job of records to null
		recordList[0].recordItemList[0].jobId = null;
		recordList[1].recordItemList[0].jobId = null;
		recordList[2].recordItemList[0].jobId = null;

		// Save Records
		Id recordId;
		ExpRecordService recordService = new ExpRecordService();
		recordId = recordService.saveExpRecord(testData.employee, recordList[0], reportEntity.expRequestId, reportEntity.expReportTypeId, false);
		recordId = recordService.saveExpRecord(testData.employee, recordList[1], reportEntity.expRequestId, reportEntity.expReportTypeId, false);
		recordId = recordService.saveExpRecord(testData.employee, recordList[2], reportEntity.expRequestId, reportEntity.expReportTypeId, false);

		//List<ExpRecordEntity> savedRecordList = new ExpRecordRepository().getEntityListByReportId(reportEntity.Id);
		//System.assertEquals(null, savedRecordList[0].recordItemList[0].costCenterCode);

		setVendorDetails(testData.company, reportEntity);
		// Set Manager
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpReportRequestService service = new ExpReportRequestService();

		 //Submit Report
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportEntity.id, '');

		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();
		ExpReportEntity retrievedReportEntity = new ExpReportRepository().getEntity(reportEntity.id);

		// Set the Authorized Date and Last Approved Time
		ExpReportRequestEntity requestEntity = requestRepo.getEntity(retrievedReportEntity.expRequestId);
		requestEntity.authorizedTime = AppDatetime.now();
		requestEntity.lastApproveTime = AppDatetime.now();
		requestRepo.saveEntity(requestEntity);

		Test.startTest();
		ExpJournalService journalService = new ExpJournalService();
		journalService.exportConfirmedReportsToJournal(new List<Id>{reportEntity.id});
		Test.stopTest();

		List<ExpJournal__c> retObjList = retrieveJournalRecords();
		ComCostCenterHistory__c costCenterHistory;
		ComJob__c job;

		System.assertEquals(3, retObjList.size());
		for (Integer i = 0; i < retObjList.size(); i++) {
			ExpJournal__c retObj = retObjList.get(i);
			ExpRecordEntity record = recordList[i];

			// Job
			System.assertEquals(null, retObj.RecordItemJobCode__c);
			System.assertEquals(null, retObj.RecordItemJobName__c);

			// Cost Center
			System.assertEquals(null, retObj.RecordItemCostCenterCode__c);
			System.assertEquals(null, retObj.RecordItemCostCenterName__c);
		}
	}

	/** Test that the Expense Reports which are Authorized but not yet exported can be exported to Journal Object */
	@isTest static void exportConfirmedReportsToJournalTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity reportEntity = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, testData.costCenter.CurrentHistoryId__c, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportEntity.id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c,3);

		setVendorDetails(testData.company, reportEntity);
		// Set Manager
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpReportRequestService service = new ExpReportRequestService();

		 //Submit Report
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportEntity.id, '');

		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();
		ExpReportEntity retrievedReportEntity = new ExpReportRepository().getEntity(reportEntity.id);

		// Set the Authorized Date and Last Approved Time
		ExpReportRequestEntity requestEntity = requestRepo.getEntity(retrievedReportEntity.expRequestId);
		requestEntity.authorizedTime = AppDatetime.now();
		requestEntity.lastApproveTime = AppDatetime.now();
		requestRepo.saveEntity(requestEntity);

		Test.startTest();
		ExpJournalService journalService = new ExpJournalService();
		journalService.exportConfirmedReportsToJournal(new List<Id>{reportEntity.id});
		Test.stopTest();

		List<ExpJournal__c> retObjList = retrieveJournalRecords();
		ComCostCenterHistory__c costCenterHistory = [SELECT Id, LinkageCode__c, Name_L0__c FROM ComCostCenterHistory__c WHERE Id =: testData.costCenter.CurrentHistoryId__c LIMIT 1];

		System.assertEquals(3, retObjList.size());
		Integer recordNo = 1;
		Integer recordItemNo = 1;
		for (Integer i = 0; i < retObjList.size(); i++) {
			ExpJournal__c retObj = retObjList.get(i);
			ExpRecordEntity record = recordList[i];
			recordItemNo = 1;
			for (Integer recordItemIndex = 0; recordItemIndex < record.recordItemList.size(); recordItemIndex++) {
				ExpRecordItemEntity recordItem = record.recordItemList[0];
				verifyReportJournalData(testData, retrievedReportEntity, record, recordItem, retObj, requestEntity, costCenterHistory, i, recordNo, recordItemNo);
				recordItemNo = recordItemNo + 1;
			}

			// Verify that  ReportRequest is updated with Exported Time
			ExpReportRequest__c retrievedReportRequest = [SELECT ID, ExportedTime__c FROM ExpReportRequest__c WHERE ID = :retrievedReportEntity.expRequestId];
			System.assertEquals(Date.today(), retrievedReportRequest.ExportedTime__c.date());
			recordNo = recordNo + 1;
		}
	}

	/**
	 * Test that the Expense Reports started off as Request can be exported to Journal Object if they are Authorized but not yet exported.
	 */
	@isTest static void exportConfirmedRequestReportsToJournalTest() {
		ExpTestData testData = new ExpTestData();
		ExpService expService = new ExpService();
		ExpRequestEntity requestEntity = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, testData.costCenter.CurrentHistoryId__c, 1)[0];
		testData.createExpRequestRecords(requestEntity.id, Date.today(), testData.expType, testData.employee, testData.job, testData.costCenter.CurrentHistoryId__c, 2);
		// Set Manager
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// Submit the Request for Approval and set the Last Approved Time
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(requestEntity.id, 'for testing');
		ExpRequestApproval__c requestApprovalObj = [SELECT Id, ExpRequestId__c, LastApproveTime__c FROM ExpRequestApproval__c WHERE ExpRequestId__c =: requestEntity.id];
		requestApprovalObj.LastApproveTime__c = Datetime.now();
		UPDATE requestApprovalObj;

		// Create Report from Request
		ExpReportEntity reportEntity = ExpRequestService.createReportEntityFromRequest(requestEntity.id);
		Id reportId = expService.saveReportCreatedFromRequest(testData.employee, reportEntity, AppDate.today()).Id;

		// Currently, due to the amount of API calls in preparing the data, the SOQL limit is reached.
		// To resolve it, the Test.startTest() is temporarily moved to include some of the test data preparation in it's governer limit.
		Test.startTest();
		//Submit Report for Approval
		ExpReportRequestService service = new ExpReportRequestService();
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportId, '');

		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();
		ExpReportEntity rReport = new ExpReportRepository().getEntity(reportId);
		// Set Record and Record Items and Extended Items
		ExpRecordRepository recordRepo = new ExpRecordRepository();
		List<ExpRecordEntity> recordList = recordRepo.getEntityListByReportId(rReport.id);
		rReport.replaceRecordList(recordList);

		// Set the Authorized Date and Last Approved Time
		ExpReportRequestEntity rReportRequest = requestRepo.getEntity(rReport.expRequestId);
		rReportRequest.authorizedTime = AppDatetime.now();
		rReportRequest.lastApproveTime = AppDatetime.now();
		requestRepo.saveEntity(rReportRequest);


		ExpJournalService journalService = new ExpJournalService();
		journalService.exportConfirmedReportsToJournal(new List<Id>{reportId});
		Test.stopTest();

		List<ExpJournal__c> retObjList = retrieveJournalRecords();
		ComCostCenterHistory__c costCenterHistory = [SELECT Id, LinkageCode__c, Name_L0__c FROM ComCostCenterHistory__c WHERE Id =: testData.costCenter.CurrentHistoryId__c LIMIT 1];
		ExpRequestEntity rRequest = ExpRequestService.getExpRequest(requestEntity.Id, null, false);

		System.assertEquals(2, retObjList.size());
		Integer recordNo = 1;
		Integer recordItemNo = 1;
		for (Integer i = 0; i < retObjList.size(); i++) {
			ExpJournal__c retObj = retObjList.get(i);
			ExpRecordEntity reportRecord = rReport.recordList[i];
			ExpRecordItemEntity reportRecordItem = reportRecord.recordItemList[0];

			verifyReportJournalData(testData, rReport, reportRecord, reportRecordItem, retObj, rReportRequest, costCenterHistory, i, recordNo, recordItemNo);
			verifyPreRequestJournalData(testData, rRequest, retObj, costCenterHistory);
			recordNo = recordNo + 1;
		}
		// Verify that  ReportRequest is updated with Exported Time
		ExpReportRequest__c rReportRequestObj = [SELECT ID, ExportedTime__c FROM ExpReportRequest__c WHERE ID = :rReport.expRequestId];
		System.assertEquals(Date.today(), rReportRequestObj.ExportedTime__c.date());
	}

	/*
	 * Helper method to create ExpVendor information and set it to ExpReport
	 */
	private static void setVendorDetails(ComCompany__c companyObj, ExpReportEntity reportEntity) {
		// create ExpVendor object
		ExpVendor__c vendorObj = new ExpVendor__c(
			Active__c = true,
			Address__c = 'address',
			BankAccountType__c = 'Checking',
			BankAccountNumber__c = 'bankAccountNumber',
			BankCode__c = 'bankCd',
			BankName__c = 'bankName',
			BranchAddress__c = 'branchAddress',
			BranchCode__c = 'XYZ',
			BranchName__c = 'branchName',
			Code__c = 'code',
			CorrespondentBankAddress__c = 'correspondentBankAddress',
			CorrespondentBankName__c = 'correspondentBankName',
			CorrespondentBranchName__c = 'correspondentBranchName',
			CorrespondentSwiftCode__c = 'cSwiftCode',
			CompanyId__c = companyObj.Id,
			Country__c = 'JPN',
			CurrencyCode__c = 'JPY',
			IsWithholdingTax__c = true,
			Name = 'nameL0',
			Name_L0__c = 'nameL0',
			Name_L1__c = 'nameL1',
			Name_L2__c = 'nameL2',
			PayeeName__c = 'payeeName',
			PaymentTerm__c = 'paymentTerm',
			SwiftCode__c = 'swiftCode',
			ZipCode__c = 'zipCode',
			UniqKey__c = 'uniqKey'
		);
		insert vendorObj;

		// set Vendor details in ExpReport object
		ExpReport__c reportObj = [SELECT Id, PaymentDueDate__c, VendorId__c FROM ExpReport__c WHERE Id =: reportEntity.Id];
		reportObj.PaymentDueDate__c = Date.today() + 90;
		reportObj.VendorId__c = vendorObj.Id;
		update reportObj;

		// set Vendor details in ExpReport entity
		reportEntity.paymentDueDate = AppDate.valueOf(Date.today() + 90);
		reportEntity.vendorId = vendorObj.Id;
		reportEntity.vendorCode = vendorObj.Code__c;
	}

	/*
	 * Retrieve the Expense Journal Object list
	 * @return List<ExpJournal__c>
	 */
	private static List<ExpJournal__c> retrieveJournalRecords() {
		return [SELECT
			ExpReportId__c,
			CompanyCode__c,
			CreditAccountCode__c,
			CreditAccountName__c,
			CreditAmount__c,
			JournalNo__c,
			ExportedTime__c,
			ExtractDate__c,
			PaymentDate__c,
			ReportNo__c,
			ReportPaymentDueDate__c,
			ReportSubject__c,
			ReportType__c,
			ReportVendorCode__c,
			RecordingDate__c,
			ReportDate__c,
			ReportAmount__c,
			ReportCostCenterCode__c,
			ReportCostCenterName__c,
			ReportJobCode__c,
			ReportJobName__c,
			ReportExtendedItemDate01__c,
			ReportExtendedItemDate02__c,
			ReportExtendedItemDate03__c,
			ReportExtendedItemDate04__c,
			ReportExtendedItemDate05__c,
			ReportExtendedItemDate06__c,
			ReportExtendedItemDate07__c,
			ReportExtendedItemDate08__c,
			ReportExtendedItemDate09__c,
			ReportExtendedItemDate10__c,
			ReportExtendedItemText01__c,
			ReportExtendedItemText02__c,
			ReportExtendedItemText03__c,
			ReportExtendedItemText04__c,
			ReportExtendedItemText05__c,
			ReportExtendedItemText06__c,
			ReportExtendedItemText07__c,
			ReportExtendedItemText08__c,
			ReportExtendedItemText09__c,
			ReportExtendedItemText10__c,
			ReportExtendedItemPickList01__c,
			ReportExtendedItemPickList02__c,
			ReportExtendedItemPickList03__c,
			ReportExtendedItemPickList04__c,
			ReportExtendedItemPickList05__c,
			ReportExtendedItemPickList06__c,
			ReportExtendedItemPickList07__c,
			ReportExtendedItemPickList08__c,
			ReportExtendedItemPickList09__c,
			ReportExtendedItemPickList10__c,
			ReportExtendedItemLookup01__c,
			ReportExtendedItemLookup02__c,
			ReportExtendedItemLookup03__c,
			ReportExtendedItemLookup04__c,
			ReportExtendedItemLookup05__c,
			ReportExtendedItemLookup06__c,
			ReportExtendedItemLookup07__c,
			ReportExtendedItemLookup08__c,
			ReportExtendedItemLookup09__c,
			ReportExtendedItemLookup10__c,
			ReportEmployeeName__c,
			ReportEmployeeCode__c,
			ReportEmployeeDepartmentName__c,
			ReportEmployeeDepartmentCode__c,
			Remark__c,
			AuthorizedTime__c,
			ReportApprovedDate__c,
			RecordId__c,
			RecordAmount__c,
			RecordDate__c,
			RecordRemarks__c,
			RecordRowNo__c,
			RecordItemId__c,
			RecordItemDate__c,
			RecordItemExpenseTypeId__c,
			RecordItemExpenseTypeCode__c,
			ExpenseTypeDebitAccountName__c,
			ExpenseTypeDebitAccountCode__c,
			ExpenseTypeDebitSubAccountName__c,
			ExpenseTypeDebitSubAccountCode__c,
			TaxTypeId__c,
			TaxTypeCode__c,
			TaxTypeName__c,
			RecordItemAmount__c,
			RecordItemAmountWithoutTax__c,
			RecordItemTaxAmount__c,
			RecordItemCostCenterName__c,
			RecordItemCostCenterCode__c,
			RecordItemJobName__c,
			RecordItemJobCode__c,
			CurrencyCode__c,
			LocalAmount__c,
			ExchangeRate__c,
			RecordItemExtendedItemDate01__c,
			RecordItemExtendedItemDate02__c,
			RecordItemExtendedItemDate03__c,
			RecordItemExtendedItemDate04__c,
			RecordItemExtendedItemDate05__c,
			RecordItemExtendedItemDate06__c,
			RecordItemExtendedItemDate07__c,
			RecordItemExtendedItemDate08__c,
			RecordItemExtendedItemDate09__c,
			RecordItemExtendedItemDate10__c,
			RecordItemExtendedItemText01__c,
			RecordItemExtendedItemText02__c,
			RecordItemExtendedItemText03__c,
			RecordItemExtendedItemText04__c,
			RecordItemExtendedItemText05__c,
			RecordItemExtendedItemText06__c,
			RecordItemExtendedItemText07__c,
			RecordItemExtendedItemText08__c,
			RecordItemExtendedItemText09__c,
			RecordItemExtendedItemText10__c,
			RecordItemExtendedItemPickList01__c,
			RecordItemExtendedItemPickList02__c,
			RecordItemExtendedItemPickList03__c,
			RecordItemExtendedItemPickList04__c,
			RecordItemExtendedItemPickList05__c,
			RecordItemExtendedItemPickList06__c,
			RecordItemExtendedItemPickList07__c,
			RecordItemExtendedItemPickList08__c,
			RecordItemExtendedItemPickList09__c,
			RecordItemExtendedItemPickList10__c,
			RecordItemExtendedItemLookup01__c,
			RecordItemExtendedItemLookup02__c,
			RecordItemExtendedItemLookup03__c,
			RecordItemExtendedItemLookup04__c,
			RecordItemExtendedItemLookup05__c,
			RecordItemExtendedItemLookup06__c,
			RecordItemExtendedItemLookup07__c,
			RecordItemExtendedItemLookup08__c,
			RecordItemExtendedItemLookup09__c,
			RecordItemExtendedItemLookup10__c,
			RecordItemRemarks__c,
			RecordItemRowNo__c,
			RequestNo__c,
			RequestReportType__c,
			RequestSubject__c,
			ScheduleDate__c,
			RequestDate__c,
			RequestAmount__c,
			RequestCostCenterCode__c,
			RequestCostCenterName__c,
			RequestJobCode__c,
			RequestJobName__c,
			RequestExtendedItemDate01__c,
			RequestExtendedItemDate02__c,
			RequestExtendedItemDate03__c,
			RequestExtendedItemDate04__c,
			RequestExtendedItemDate05__c,
			RequestExtendedItemDate06__c,
			RequestExtendedItemDate07__c,
			RequestExtendedItemDate08__c,
			RequestExtendedItemDate09__c,
			RequestExtendedItemDate10__c,
			RequestExtendedItemText01__c,
			RequestExtendedItemText02__c,
			RequestExtendedItemText03__c,
			RequestExtendedItemText04__c,
			RequestExtendedItemText05__c,
			RequestExtendedItemText06__c,
			RequestExtendedItemText07__c,
			RequestExtendedItemText08__c,
			RequestExtendedItemText09__c,
			RequestExtendedItemText10__c,
			RequestExtendedItemPickList01__c,
			RequestExtendedItemPickList02__c,
			RequestExtendedItemPickList03__c,
			RequestExtendedItemPickList04__c,
			RequestExtendedItemPickList05__c,
			RequestExtendedItemPickList06__c,
			RequestExtendedItemPickList07__c,
			RequestExtendedItemPickList08__c,
			RequestExtendedItemPickList09__c,
			RequestExtendedItemPickList10__c,
			RequestExtendedItemLookup01__c,
			RequestExtendedItemLookup02__c,
			RequestExtendedItemLookup03__c,
			RequestExtendedItemLookup04__c,
			RequestExtendedItemLookup05__c,
			RequestExtendedItemLookup06__c,
			RequestExtendedItemLookup07__c,
			RequestExtendedItemLookup08__c,
			RequestExtendedItemLookup09__c,
			RequestExtendedItemLookup10__c,
			RequestEmployeeCode__c,
			RequestEmployeeName__c,
			RequestEmployeeDepartmentCode__c,
			RequestEmployeeDepartmentName__c,
			Purpose__c,
			ApprovedDate__c
			FROM ExpJournal__c
			ORDER BY JournalNo__c ASC
		];
	}

	/*
	 * Assert the ExpReport's related fields in the exported Journal
	 */
	private static void verifyReportJournalData(ExpTestData testData, ExpReportEntity report, ExpRecordEntity record, ExpRecordItemEntity recordItem,
			ExpJournal__c retObj, ExpReportRequestEntity requestEntity, ComCostCenterHistory__c costCenterHistory, Integer index, Integer recordNo, Integer recordItemNo) {
		System.assertNotEquals(null, report.id);
		System.assertEquals(report.id, retObj.ExpReportId__c);
		System.assertEquals(testData.company.Code__c, retObj.CompanyCode__c);
		System.assertEquals('00000000000' + String.valueOf(index + 1), retObj.JournalNo__c);
		System.assertEquals(Date.today(), retObj.ExportedTime__c.date());
		System.assertEquals(recordNo, retObj.RecordRowNo__c);
		System.assertEquals(recordItemNo, retObj.RecordItemRowNo__c);

		// Report Specific Data
		System.assertEquals(report.reportNo, retObj.ReportNo__c);
		if (report.paymentDueDate != null) {
			System.assertEquals(report.paymentDueDate.getDate(), retObj.ReportPaymentDueDate__c);
		} else {
			System.assertEquals(null, retObj.ReportPaymentDueDate__c);
		}
		System.assertEquals(report.subject, retObj.ReportSubject__c);
		System.assertEquals(report.expReportTypeCode, retObj.ReportType__c);
		System.assertEquals(report.vendorCode, retObj.ReportVendorCode__c);
		System.assertEquals(Date.today(), retObj.RecordingDate__c);
		System.assertEquals(Date.today(), retObj.ReportDate__c);
		System.assertEquals(report.totalAmount, retObj.ReportAmount__c);
		System.assertEquals(report.remarks, retObj.Remark__c);

		// Cost Center
		System.assertEquals(costCenterHistory.LinkageCode__c, retObj.ReportCostCenterCode__c);
		System.assertEquals(costCenterHistory.Name_L0__c, retObj.ReportCostCenterName__c);
		// Job
		System.assertEquals(testData.job.code, retObj.ReportJobCode__c);
		System.assertEquals(testData.job.nameL.getValue(AppLanguage.JA), retObj.ReportJobName__c);

		// Report's Extended Items
		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			String eiIndex = String.valueOf(i + 1).leftPad(2, '0');
			System.assertEquals(report.getExtendedItemDateValueAsDate(i), (Date) retObj.get(POSTFIX_REPORT + EXTENDED_ITEM_DATE + eiIndex + FIELD_POSTFIX));
			System.assertEquals(report.getExtendedItemTextValue(i), (String) retObj.get(POSTFIX_REPORT + EXTENDED_ITEM_TEXT + eiIndex + FIELD_POSTFIX));
			System.assertEquals(report.getExtendedItemPicklistValue(i), (String) retObj.get(POSTFIX_REPORT + EXTENDED_ITEM_PICKLIST + eiIndex + FIELD_POSTFIX));
			System.assertEquals(report.getExtendedItemLookupValue(i), (String) retObj.get(POSTFIX_REPORT + EXTENDED_ITEM_LOOKUP + eiIndex + FIELD_POSTFIX));
		}

		// Employee
		System.assertEquals(testData.employee.code, retObj.ReportEmployeeCode__c);
		System.assertEquals(testData.employee.getDefaultDisplayNameL0(AppLanguage.getLangByLangKey(AppLanguage.LangKey.L0)),
				retObj.ReportEmployeeName__c);
		// Department
		System.assertEquals(testData.department.code, retObj.ReportEmployeeDepartmentCode__c);
		System.assertEquals(testData.department.getHistoryList().get(0).nameL.getValue(AppLanguage.JA),
				retObj.ReportEmployeeDepartmentName__c);

		// Approved Date and Authorized Time
		System.assertEquals(AppDatetime.convertDatetime(requestEntity.authorizedTime), retObj.AuthorizedTime__c);
		System.assertEquals(requestEntity.lastApproveTime.getDate(), retObj.ReportApprovedDate__c);

		// Record
		System.assertEquals(record.id, retObj.RecordId__c);
		System.assertEquals(record.amount, retObj.RecordAmount__c);
		System.assertEquals(Date.today(), retObj.RecordDate__c);
		System.assertEquals(record.recordItemList[0].remarks, retObj.RecordRemarks__c);

		// Record Item
		System.assertEquals(recordItem.id, retObj.RecordItemId__c);
		System.assertEquals(recordItem.itemDate.getDate(), retObj.RecordItemDate__c);
		// Expense Type
		System.assertEquals(testData.expType.Id, retObj.RecordItemExpenseTypeId__c);
		System.assertEquals(testData.expType.Code__c, retObj.RecordItemExpenseTypeCode__c);
		// Debit Accounts
		System.assertEquals(testData.expType.DebitAccountCode__c, retObj.ExpenseTypeDebitAccountCode__c);
		System.assertEquals(testData.expType.DebitAccountName__c, retObj.ExpenseTypeDebitAccountName__c);
		System.assertEquals(testData.expType.DebitSubAccountCode__c, retObj.ExpenseTypeDebitSubAccountCode__c);
		System.assertEquals(testData.expType.DebitSubAccountName__c, retObj.ExpenseTypeDebitSubAccountName__c);
		//Tax
		System.assertEquals(recordItem.taxTypeCode, retObj.TaxTypeCode__c);
		System.assertEquals(recordItem.taxTypeHistoryId, retObj.TaxTypeId__c);
		System.assertEquals(recordItem.taxTypeNameL.getValue(AppLanguage.JA), retObj.TaxTypeName__c);
		//Amounts (Local Currency)
		System.assertEquals(recordItem.amount, retObj.RecordItemAmount__c);
		System.assertEquals(recordItem.withoutTax, retObj.RecordItemAmountWithoutTax__c);
		System.assertEquals(recordItem.gstVat, retObj.RecordItemTaxAmount__c);
		// Amount Foreign Currency
		if (recordItem.useForeignCurrency == True) {
			// Only check if Record is using Foreign Currency
			System.assertEquals(testData.comCurrency.Code__c, retObj.CurrencyCode__c);
			System.assertEquals(recordItem.exchangeRate, retObj.ExchangeRate__c);
			System.assertEquals(recordItem.localAmount, retObj.LocalAmount__c);
		}
		// Cost Center
		System.assertEquals(costCenterHistory.LinkageCode__c, retObj.RecordItemCostCenterCode__c);
		System.assertEquals(costCenterHistory.Name_L0__c, retObj.RecordItemCostCenterName__c);
		// Job
		System.assertEquals(testData.job.code, retObj.RecordItemJobCode__c);
		System.assertEquals(testData.job.nameL.getValue(AppLanguage.JA), retObj.RecordItemJobName__c);
		// Record Item's Extended Items
		for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			String eiIndex = String.valueOf(i + 1).leftPad(2, '0');
			System.assertEquals(recordItem.getExtendedItemDateValueAsDate(i), (Date) retObj.get(POSTFIX_RECORD_ITEM + EXTENDED_ITEM_DATE + eiIndex + FIELD_POSTFIX));
			System.assertEquals(recordItem.getExtendedItemTextValue(i), (String) retObj.get(POSTFIX_RECORD_ITEM + EXTENDED_ITEM_TEXT + eiIndex + FIELD_POSTFIX));
			System.assertEquals(recordItem.getExtendedItemPicklistValue(i), (String) retObj.get(POSTFIX_RECORD_ITEM + EXTENDED_ITEM_PICKLIST + eiIndex + FIELD_POSTFIX));
			System.assertEquals(recordItem.getExtendedItemLookupValue(i), (String) retObj.get(POSTFIX_RECORD_ITEM + EXTENDED_ITEM_LOOKUP + eiIndex + FIELD_POSTFIX));
		}
		if (recordItem.order == 0) {
			System.assertEquals(null, retObj.RecordItemRemarks__c);
		} else {
			System.assertEquals(recordItem.remarks, retObj.RecordItemRemarks__c);
		}
	}

	/*
	 *  Assert the ExpRequest's related fields in the exported Journal
	 */
	private static void verifyPreRequestJournalData(ExpTestData testData, ExpRequestEntity request,
			ExpJournal__c retObj, ComCostCenterHistory__c costCenterHistory) {
		System.assertEquals(request.requestNo, retObj.RequestNo__c);
		System.assertEquals(request.subject, retObj.RequestSubject__c);
		System.assertEquals(testData.expReportType.Code__c, retObj.RequestReportType__c);
		System.assertEquals(request.scheduledDate.getDate(), retObj.ScheduleDate__c);
		System.assertEquals(request.recordingDate.getDate(), retObj.RequestDate__c);
		System.assertEquals(request.totalAmount, retObj.RequestAmount__c);

		// Request Cost Center
		System.assertEquals(costCenterHistory.LinkageCode__c, retObj.RequestCostCenterCode__c);
		System.assertEquals(costCenterHistory.Name_L0__c, retObj.RequestCostCenterName__c);

		// Request Job
		System.assertEquals(testData.job.code, retObj.RequestJobCode__c);
		System.assertEquals(testData.job.nameL.getValue(AppLanguage.JA), retObj.RequestJobName__c);

		//Extended Items
		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			String eiIndex = String.valueOf(i + 1).leftPad(2, '0');
			System.assertEquals(request.getExtendedItemDateValueAsDate(i), (Date) retObj.get(POSTFIX_REQUEST + EXTENDED_ITEM_DATE + eiIndex + FIELD_POSTFIX));
			System.assertEquals(request.getExtendedItemTextValue(i), (String) retObj.get(POSTFIX_REQUEST + EXTENDED_ITEM_TEXT + eiIndex + FIELD_POSTFIX));
			System.assertEquals(request.getExtendedItemPicklistValue(i), (String) retObj.get(POSTFIX_REQUEST + EXTENDED_ITEM_PICKLIST + eiIndex + FIELD_POSTFIX));
			System.assertEquals(request.getExtendedItemLookupValue(i), (String) retObj.get(POSTFIX_REQUEST + EXTENDED_ITEM_LOOKUP + eiIndex + FIELD_POSTFIX));
		}

		// Employee
		System.assertEquals(testData.employee.code, retObj.RequestEmployeeCode__c);
		System.assertEquals(testData.employee.getDefaultDisplayNameL0(AppLanguage.getLangByLangKey(AppLanguage.LangKey.L0)),
				retObj.RequestEmployeeName__c);
		// Department
		System.assertEquals(testData.department.code, retObj.RequestEmployeeDepartmentCode__c);
		System.assertEquals(testData.department.getHistoryList().get(0).nameL.getValue(AppLanguage.JA),
				retObj.RequestEmployeeDepartmentName__c);

		System.assertEquals(request.purpose, retObj.Purpose__c);
		System.assertEquals(Date.today(), retObj.ApprovedDate__c);
	}
}