/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 勤怠マスタ管理系APIの権限チェック機能を提供するクラス
 * 勤怠マスタ管理系のResourceクラス以外からは呼び出さないでください
 */
public with sharing class AttConfigResourcePermission {

	/** API実行に必要な権限 */
	public enum Permission {
		/** 休暇の管理 */
		MANAGE_ATT_LEAVE,
		/** 短時間勤務設定の管理 */
		MANAGE_ATT_SHORT_TIME_WORK_SETTING,
		/** 休職・休業の管理 */
		MANAGE_ATT_LEAVE_OF_ABSENCE,
		/** 勤務体系の管理 */
		MANAGE_ATT_WORKING_TYPE,
		/** 勤務パターンの管理 */
		MANAGE_ATT_PATTERN,
		/** 残業警告設定の管理 */
		MANAGE_ATT_AGREEMENT_ALERT_SETTING,
		/** 休暇管理の管理 */
		MANAGE_ATT_LEAVE_GRANT,
		/** 短時間勤務設定適用の管理 */
		MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY,
		/** 休職・休業適用の管理 */
		MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY,
		/** 勤務パターン適用の管理 */
		MANAGE_ATT_PATTERN_APPLY
	}

	/** 社員の権限サービス */
	private EmployeePermissionService permissionService;

	/**
	 * コンストラクタ
	 */
	public AttConfigResourcePermission() {
		this.permissionService = new EmployeePermissionService();
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(AttConfigResourcePermission.Permission requiredPermission) {
		hasExecutePermission(new List<AttConfigResourcePermission.Permission>{requiredPermission});
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(List<AttConfigResourcePermission.Permission> requiredPermissionList) {
		List<AttConfigResourcePermission.Permission> errorPermissionList =
				new List<AttConfigResourcePermission.Permission>();

		for (Permission requiredPermission : requiredPermissionList) {
			// 休暇の管理
			if (requiredPermission == Permission.MANAGE_ATT_LEAVE) {
				if (! permissionService.hasManageAttLeave) {
					errorPermissionList.add(Permission.MANAGE_ATT_LEAVE);
				}
			// 短時間勤務設定の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING) {
				if (! permissionService.hasManageAttShortTimeWorkSetting) {
					errorPermissionList.add(Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING);
				}
			// 休職・休業の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_LEAVE_OF_ABSENCE) {
				if (! permissionService.hasManageAttLeaveOfAbsence) {
					errorPermissionList.add(Permission.MANAGE_ATT_LEAVE_OF_ABSENCE);
				}
			// 勤務体系の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_WORKING_TYPE) {
				if (! permissionService.hasManageAttWorkingType) {
					errorPermissionList.add(Permission.MANAGE_ATT_WORKING_TYPE);
				}
			// 勤務パターンの管理
			} else if (requiredPermission == Permission.MANAGE_ATT_PATTERN) {
				if (! permissionService.hasManageAttPattern) {
					errorPermissionList.add(Permission.MANAGE_ATT_PATTERN);
				}
			// 残業警告設定の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_AGREEMENT_ALERT_SETTING) {
				if (! permissionService.hasManageAttAgreementAlertSetting) {
					errorPermissionList.add(Permission.MANAGE_ATT_AGREEMENT_ALERT_SETTING);
				}
			// 休暇管理の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_LEAVE_GRANT) {
				if (! permissionService.hasManageAttLeaveGrant) {
					errorPermissionList.add(Permission.MANAGE_ATT_LEAVE_GRANT);
				}
			// 短時間勤務設定適用の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY) {
				if (! permissionService.hasManageAttShortTimeWorkSettingApply) {
					errorPermissionList.add(Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING_APPLY);
				}
			// 休職・休業適用の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY) {
				if (! permissionService.hasManageAttLeaveOfAbsenceApply) {
					errorPermissionList.add(Permission.MANAGE_ATT_LEAVE_OF_ABSENCE_APPLY);
				}
			// 勤務パターン適用の管理
			} else if (requiredPermission == Permission.MANAGE_ATT_PATTERN_APPLY) {
				if (! permissionService.hasManageAttPatternApply) {
					errorPermissionList.add(Permission.MANAGE_ATT_PATTERN_APPLY);
				}
			// Error! 実装ミス
			} else {
				throw new App.UnsupportedException('Unsupported Permission(' + requiredPermission + ')');
			}
		}

		if (! errorPermissionList.isEmpty()) {
			// デバッグ用にログを出力しておく
			System.debug('--- Insufficient permission for API=' + errorPermissionList);
			throw new App.NoPermissionException(ComMessage.msg().Com_Err_NoApiPermission);
		}
	}
}