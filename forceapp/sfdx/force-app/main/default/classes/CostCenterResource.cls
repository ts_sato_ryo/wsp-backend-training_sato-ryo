/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Class for Cost Center APIs
 */
public with sharing class CostCenterResource {
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	public class LookupField {
		public String name;
	}

	/*
	 * Try and convert the given String to Id type.
	 * If failed, ParameterException with InvalidValue msg will be thrown with the given errorMsg.
	 */
	private static void verifyValidIdValue(String idString, Boolean isRequired, String errorMsg) {
		if (String.isNotBlank(idString)) {
			try {
				System.Id.valueOf(idString);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{errorMsg}));
			}
		} else {
			if (isRequired) {
				throw new App.ParameterException(MESSAGE.Com_Err_Specify(new List<String>{errorMsg}));
			}
		}
	}

	/*
	 * @description Request Param for the API. Based on the API, different field will be used.
	 */
	public class CostCenter implements RemoteApi.RequestParam {
		public String id;
		public String name;
		public String historyId;
		public String name_L0;
		public String name_L1;
		public String name_L2;
		public String code;
		public String linkageCode;
		public String companyId;
		public String parentId;
		public LookupField parent;
		public String validDateFrom;
		public String validDateTo;
		public String comment;
		public Boolean hasChildren;
		public List<String> hierarchyParentNameList;

		public CostCenterBaseEntity createBaseEntity(Map<String, Object> paramMap) {
			CostCenterBaseEntity base = new CostCenterBaseEntity();
			base.setId(this.id);

			if (paramMap.containsKey('code')) {
				base.code = this.code;
			}

			// For newly Creating Case. Otherwise, not allows to change Company
			if (this.id == null && paramMap.containsKey('companyId')) {
				base.companyId = this.companyId;
			}

			if (paramMap.containsKey('name_L0')) {
				base.name = this.name_L0;
			}
			return base;
		}

		public CostCenterHistoryEntity createHistoryEntity(Map<String, Object> paramMap) {
			CostCenterHistoryEntity history = new CostCenterHistoryEntity();

			if (paramMap.containsKey('name_L0')) {
				history.nameL0 = this.name_L0;
				history.name = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				history.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				history.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('parentId')) {
				history.parentBaseId = this.parentId;
			}
			if (paramMap.containsKey('linkageCode')) {
				history.linkageCode = this.linkageCode;
			}
			if (paramMap.containsKey('validDateFrom')) {
				history.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (history.validFrom == null) {
				history.validFrom = AppDate.today();
			}
			if (paramMap.containsKey('validDateTo')) {
				history.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (history.validTo == null) {
				history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}
			if (paramMap.containsKey('comment')) {
				history.historyComment = this.comment;
			}
			return history;
		}
	}

	/*
	 * @description Response Param for Saving and Updating
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		public String id;
	}

	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** permission for running API*/
		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_COST_CENTER;

		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			CostCenter param = (CostCenter) req.getParam(CostCenterResource.CostCenter.class );
			CostCenterBaseEntity base = param.createBaseEntity(req.getParamMap());
			CostCenterHistoryEntity history = param.createHistoryEntity(req.getParamMap());
			base.addHistory(history);

			CostCenterService service = new CostCenterService();

			// Linkage Code was changed not to be unique and there will be no checking of linkageCode
			/*if (history.linkageCode != null && service.isLinkageCodeDuplicated(history)) {
				throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCostCenterCode);
			}*/

			service.updateUniqKey(base);
			Id resultId = service.saveNewEntity(base);

			CostCenterResource.SaveResult result = new CostCenterResource.SaveResult();
			result.Id = resultId;

			return result;
		}
	}

	/*
	 * API to update the code of existing Cost Centers.
	 * Currently, only the Code of the Cost Center can be updated.
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_COST_CENTER;

		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			CostCenter param = (CostCenter) req.getParam(CostCenter.class);
			validateParam(param);
			CostCenterBaseEntity base = param.createBaseEntity(req.getParamMap());
			CostCenterService service = new CostCenterService();
			service.updateUniqKey(base);
			service.updateBase(base);

			//Exception will be thrown in the above statements if the operation fails.
			return null;
		}

		private void validateParam(CostCenterResource.CostCenter param) {
			verifyValidIdValue(param.id, true,'ID');
			if (String.isBlank(param.code)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('Code');
				throw ex;
			}
		}
	}

	/*
	 * Request Parameter for the DeleteApi
	 */
	public class DeleteParam implements RemoteApi.RequestParam {
		/*
		 * The Base ID of the Cost Center to be deleted.
		 */
		public String id;

		public void validate() {
			verifyValidIdValue(this.id, true, 'ID');
		}
	}

	/*
	 * API for deleting Cost Center. Currently, only allows deleting if the Cost Center isn't use by any -
	 * No sub-Cost Center (both in past, present and future) with the exception of logically deleted costcenters.
	 * TODO: Once the cost center is integrated with employee and expense record, it shouldn't be allowed to delete as well.
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_COST_CENTER;

		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			DeleteParam param = (DeleteParam) req.getParam(DeleteParam.class);
			param.validate();
			CostCenterService service = new CostCenterService();

			// Check if there is any Cost Center which specifies this as Parent (past, current or future)
			// Except for those which have been logically removed as those records shouldn't affect the deleting of CostCenter
			CostCenterRepository.SearchFilter filter = new CostCenterRepository.SearchFilter();
			filter.parentIds = new List<Id> {param.id};
			filter.includeRemoved = false;
			List<CostCenterHistoryEntity> historyUsingDeleteTarget = (new CostCenterRepository()).searchHistoryEntity(
				filter, false, false, CostCenterRepository.HISTORY_SORT_ORDER.VALID_ASC);

			if (historyUsingDeleteTarget.size() > 0) {
				throw new App.ParameterException(MESSAGE.Com_Err_CannotDeleteReference);
			}

			try {
				service.deleteEntity(param.id);
			} catch (DmlException e) {
				// If the exception is NOT because the record has already been deleted,
				// throw exception - it is considered as success if the API tries to delete an already deleted record.
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					throw e;
				}
			}
			//Exception will be thrown in the above statements if the operation fails.
			return null;
		}
	}

	/**
	 * Request Parameter for the SearchApi
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		public String id;
		public String companyId;
		public String parentId;
		public String targetDate;
		/** Specifies whether to add in Hierarchy Cost Center Parent Names */
		public String usedIn;
		/** Cost Center Code or Cost Center Name (In mobile is null)*/
		public String query;

		/*
		 * Validate the Request Param for the Search API
		 */
		public void validate() {
			verifyValidIdValue(this.id, false, 'id');
			verifyValidIdValue(this.companyId, false, 'companyId');
			verifyValidIdValue(this.parentId, false, 'parentId');

			if (String.isNotBlank(targetDate)) {
				try {
					AppDate.valueOf(targetDate);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'targetDate'}));
				}
			}

			if (String.isNotBlank(this.usedIn)) {
				ExpCommonUtil.validateUsedIn('usedIn', this.usedIn);
			}
		}
	}

	/**
	 * Response Param for SearchAPi and GetRecentlyUsedListApi
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		public CostCenter[] records;
	}

	/**
	 * API to search Cost Centers
	 * If the targetDate is not speicified consided as today date
	 * If usedIn param is defined Cost Centers Parent Hierarchy is returned
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SearchCondition param = (SearchCondition) req.getParam(SearchCondition.class );
			param.validate();
			AppDate targetDate = param.targetDate == null ? AppDate.today(): AppDate.valueOf(param.targetDate);

			CostCenterService service = new CostCenterService();

			List<CostCenterBaseEntity> baseEntityList = service.searchCostCenterBases(param.id,
					param.companyId, param.parentId, targetDate, param.query);

			List<Id> baseIds = new List<Id>();
			List<Id> parentIds = new List<Id>();

			for (CostCenterBaseEntity baseEntity : baseEntityList) {
				baseIds.add(baseEntity.id);
				CostCenterHistoryEntity historyEntity = baseEntity.getHistory(0);
				parentIds.add(historyEntity.parentBaseId);
			}

			Map<Id, List<String>> hierarchyParentNamesMap = new Map<Id, List<String>>();
			Map<Id, CostCenterHistoryEntity> parentHistoryMap;
			if (String.isNotEmpty(param.usedIn)) {
				hierarchyParentNamesMap = service.getParentHierarchy(baseEntityList, targetDate);

			} else if (hierarchyParentNamesMap == null || hierarchyParentNamesMap.isEmpty()){
				parentHistoryMap = service.getHistoryMap(parentIds, targetDate);
			}

			Map<Id, Boolean> hasChildrenMap = service.hasChildren(baseIds, targetDate);

			return generateSearchResultResponse(baseEntityList, parentHistoryMap, hierarchyParentNamesMap, hasChildrenMap);
		}
	}

	/**
	 * Request Parameter for the GetRecentlyUsedListApi
	 */
	public class GetRecentlyUsedListParam implements RemoteApi.RequestParam {
		/** Employee Base Id */
		public String employeeBaseId;
		/** Target Date (Record or Accounting Date)*/
		public String targetDate;

		/**
		 * Validate the Request Param for the GetRecentlyUsedList API
		 */
		public void validate() {
			ExpCommonUtil.validateId('employeeBaseId', this.employeeBaseId, true);

			if (String.isNotBlank(targetDate)) {
				ExpCommonUtil.validateDate('targetDate', this.targetDate);
			}
		}
	}

	/**
	 * API to get recently used Cost Centers by employee
	 * If the recently used Cost Center is not valid at the point of the retrieval; it is not return as recently used
	 */
	public with sharing class GetRecentlyUsedListApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			CostCenterResource.GetRecentlyUsedListParam param = (CostCenterResource.GetRecentlyUsedListParam) req.getParam(GetRecentlyUsedListParam.class);
			param.validate();

			CostCenterService service = new CostCenterService();

			AppDate targetDate = String.isNotBlank(param.targetDate) ? AppDate.valueOf(param.targetDate) : AppDate.today();

			List<CostCenterBaseEntity> baseEntityList = service.getRecentlyUsedCostCenterBaseList(param.employeeBaseId, targetDate);
			List<Id> baseIds = new List<Id>();
			for (CostCenterBaseEntity baseEntity : baseEntityList) {
				baseIds.add(baseEntity.id);
			}

			Map<Id, Boolean> hasChildrenMap = service.hasChildren(baseIds, targetDate);
			Map<Id, List<String>> hierarchyParentNamesMap = service.getParentHierarchy(baseEntityList, targetDate);

			return generateSearchResultResponse(baseEntityList, null, hierarchyParentNamesMap, hasChildrenMap);
		}
	}

	/**
	 * Generate search/recently used Cost Center Response
	 * @param baseEntityList CostCenter Base Entity to be returned
	 * @param parentHistoryMap CostCenter ParentHistory Map that is used for admin page
	 * @param hierarchyParentNamesMap CostCenter HierarchyParent names
	 * @param hasChildrenMap CostCenter Base has children boolean
	 */
	private static SearchResult generateSearchResultResponse(List<CostCenterBaseEntity> baseEntityList, Map<Id, CostCenterHistoryEntity> parentHistoryMap, Map<Id, List<String>> hierarchyParentNamesMap, Map<Id, Boolean> hasChildrenMap) {
		List<CostCenter> costCenterList = new List<CostCenter>();
		for (CostCenterBaseEntity bEntity : baseEntityList) {
			CostCenterHistoryEntity hEntity = bEntity.getHistoryList().get(0);
			CostCenter cc = new CostCenter();
			cc.id = bEntity.id;
			cc.code = bEntity.code;
			cc.companyId = bEntity.companyId;
			cc.historyId = hEntity.id;
			cc.name = hEntity.nameL.getValue();
			cc.name_L0 = hEntity.nameL.valueL0;
			cc.name_L1 = hEntity.nameL.valueL1;
			cc.name_L2 = hEntity.nameL.valueL2;
			cc.linkageCode = hEntity.linkageCode;
			cc.validDateFrom = hEntity.validFrom.format();
			cc.validDateTo = hEntity.validTo.format();
			cc.parentId = hEntity.parentBaseId;
			cc.parent = new LookupField();
			cc.comment = hEntity.historyComment;
			cc.hierarchyParentNameList = new List<String>();
			if (hEntity.parentBaseId != null && parentHistoryMap != null && parentHistoryMap.containsKey(hEntity.parentBaseId)) {
				cc.parent.name = parentHistoryMap.get(hEntity.parentBaseId).nameL.getValue();
			} else if (hEntity.parentBaseId != null && hierarchyParentNamesMap.containsKey(bEntity.id)) {
				cc.parent.name = hierarchyParentNamesMap.get(bEntity.id)[0];
				cc.hierarchyParentNameList = hierarchyParentNamesMap.get(bEntity.id) != null ? hierarchyParentNamesMap.get(bEntity.id) : new List<String>();
			}
			cc.hasChildren = hasChildrenMap.get(bEntity.id);
			costCenterList.add(cc);
		}
		SearchResult result = new SearchResult();
		result.records = costCenterList;
		return result;
	}
}