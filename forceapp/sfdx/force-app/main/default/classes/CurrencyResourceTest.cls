/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for CurrencyResource
 */
@isTest
private class CurrencyResourceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * Create a new Currency record(Positive case)
	 */
	@isTest
	static void createApiTest() {

		CurrencyResource.UpsertParam param = new CurrencyResource.UpsertParam();
		setTestParam(param);

		Test.startTest();

		CurrencyResource.createApi api = new CurrencyResource.createApi();
		CurrencyResource.SaveResult result = (CurrencyResource.SaveResult)api.execute(param);

		Test.stopTest();

		List<ComCurrency__c> records =
			[SELECT
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				IsoCurrencyCode__c,
				DecimalPlaces__c,
				Symbol__c
			FROM ComCurrency__c
			WHERE Id =: result.id];

		//Confirm new record was created
		System.assertEquals(1, records.size());

		System.assertEquals(param.name_l0, records[0].Name);
		System.assertEquals(param.name_l0, records[0].Name_L0__c);
		System.assertEquals(param.name_l1, records[0].Name_L1__c);
		System.assertEquals(param.name_l2, records[0].Name_L2__c);
		System.assertEquals(param.isoCurrencyCode, records[0].IsoCurrencyCode__c);
		System.assertEquals(param.isoCurrencyCode, records[0].Code__c);
		System.assertEquals(param.symbol, records[0].Symbol__c);
	}

	/**
   * Create a new Currency record(Negative case: No permission)
	 */
	@isTest
	static void createApiNoPermissionTest() {

		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('Standard');

		// Set permission
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		CurrencyResource.UpsertParam param = new CurrencyResource.UpsertParam();
		setTestParam(param);

		App.NoPermissionException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				CurrencyResource.createApi api = new CurrencyResource.createApi();
				CurrencyResource.SaveResult result = (CurrencyResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx);
		System.runAs(runAsUser) {
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
   * Create a new Currency record(Negative case: Mandatory params are not set)
   */
	@isTest
	static void createApiParamErrTest() {

		CurrencyResource.UpsertParam param = new CurrencyResource.UpsertParam();
		setTestParam(param);

		CurrencyResource.CreateApi api = new CurrencyResource.CreateApi();
		App.ParameterException ex1;
		App.ParameterException ex2;

		Test.startTest();

		// Set ISO Currency code blank
		App.ParameterException ex;
		setTestParam(param);
		param.isoCurrencyCode = '';
		try {
			// Parameter error should occur
			api.execute(param);
		} catch (App.ParameterException e1) {
			ex1 = e1;
		}

		// Set Name blank
		setTestParam(param);
		param.name_L0 = '';
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e2) {
			ex2 = e2;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex1.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ISO Currency Code'}), ex1.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex2.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'Name'}), ex2.getMessage());
	}

	/**
	   * Update a Currency record(Positive case)
	   */
	@isTest
	static void updateApiTest() {

		// Create a Currency record first
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		CurrencyResource.UpsertParam param = new CurrencyResource.UpsertParam();
		setTestParam(param);
		param.id = testRecord.Id;

		Test.startTest();

		CurrencyResource.UpdateApi api = new CurrencyResource.UpdateApi();
		api.execute(param);

		Test.stopTest();

		List<ComCurrency__c> records =
		[SELECT
			Name,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			Code__c,
			IsoCurrencyCode__c,
			DecimalPlaces__c,
			Symbol__c
			FROM ComCurrency__c
			WHERE Id =: testRecord.id];

		//Confirm the record was updated
		System.assertEquals(1, records.size());

		System.assertEquals(param.name_l0, records[0].Name);
		System.assertEquals(param.name_l0, records[0].Name_L0__c);
		System.assertEquals(param.name_l1, records[0].Name_L1__c);
		System.assertEquals(param.name_l2, records[0].Name_L2__c);
		System.assertEquals(param.isoCurrencyCode, records[0].Code__c);
		System.assertEquals(param.isoCurrencyCode, records[0].IsoCurrencyCode__c);
		System.assertEquals(param.decimalPlaces, records[0].DecimalPlaces__c);
		System.assertEquals(param.symbol, records[0].Symbol__c);
	}

	/**
   * Update a new Currency record(Negative case: No permission)
	 */
	@isTest
	static void updateApiNoPermissionTest() {

		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('Standard');

		// Set permission
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);


		CurrencyResource.UpsertParam param = new CurrencyResource.UpsertParam();
		setTestParam(param);
		param.id = testData.currencyEntity.id;

		App.NoPermissionException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				CurrencyResource.UpdateApi api = new CurrencyResource.UpdateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx);
		System.runAs(runAsUser) {
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
   * Update a Currency record(Negative case: Mandatory param is not set)
   */
	@isTest
	static void updateApiParamErrTest() {

		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		CurrencyResource.UpsertParam param = new CurrencyResource.UpsertParam();
		setTestParam(param);
		// Do not set ID

		Test.startTest();

		CurrencyResource.UpdateApi api = new CurrencyResource.UpdateApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), ex.getMessage());
	}

	/**
	 * Delete a Currency record(Positive case)
	 */
	@isTest
	static void deleteApiTest() {

		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		CurrencyResource.DeleteParam param = new CurrencyResource.DeleteParam();
		param.id = testRecord.Id;

		Test.startTest();

		CurrencyResource.DeleteApi api = new CurrencyResource.DeleteApi();
		api.execute(param);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ComCurrency__c WHERE Id =: param.id]);
	}

	/**
   * Delete a new Currency record(Negative case: No permission)
	 */
	@isTest
	static void deleteApiNoPermissionTest() {

		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('Standard');
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		// Set permission
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		CurrencyResource.DeleteParam param = new CurrencyResource.DeleteParam();
		param.id = testRecord.Id;

		App.NoPermissionException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				CurrencyResource.DeleteApi api = new CurrencyResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx);
		System.runAs(runAsUser) {
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * Delete a Currency record(Negative case)
	 */
	@isTest
	static void deleteApiParamErrTest() {

		CurrencyResource.DeleteParam param = new CurrencyResource.DeleteParam();

		Test.startTest();

		CurrencyResource.DeleteApi api = new CurrencyResource.DeleteApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), ex.getMessage());
	}

	/**
	 * Search a Currency record by ID(Positive case)
	 */
	@isTest
	static void searchApiTest() {

		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('JPY', 'Japanse Yen');

		Test.startTest();

		CurrencyResource.SearchParam param = new CurrencyResource.SearchParam();
		param.id = testRecord.id;

		CurrencyResource.SearchApi api = new CurrencyResource.SearchApi();
		CurrencyResource.SearchResult res = (CurrencyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());

		CurrencyResource.CurrencyRecord result = res.records[0];
		System.assertEquals(testRecord.Id, result.id);

	}

	/**
   * Search a Currency record by ID(Positive case)
   */
	@isTest
	static void searchApiExcludingBaseCurrencyTest() {

		ComCurrency__c testBaseCurrency = ComTestDataUtility.createCurrency('JPY', 'Japanse Yen');
		ComCurrency__c testOtherCurrency = ComTestDataUtility.createCurrency('SGD', 'Singapore Dollar');
		ComCompany__c company = ComTestDataUtility.createCompany('test inc', null, testBaseCurrency.Id, true);


		Test.startTest();

		CurrencyResource.SearchParam param = new CurrencyResource.SearchParam();
		param.companyId = company.Id;

		CurrencyResource.SearchApi api = new CurrencyResource.SearchApi();
		CurrencyResource.SearchResult res = (CurrencyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm that only other currency record is fetched
		System.assertEquals(1, res.records.size());
		CurrencyResource.CurrencyRecord result = res.records[0];
		System.assertEquals(testOtherCurrency.Id, result.id);

	}

	/**
	 * Search a Currency record(Positive case: test for L1 name(multi lang))
	 */
	@isTest
	static void searchApiGetNameL1Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'United States dollar');

		Test.startTest();

		CurrencyResource.SearchParam param = new CurrencyResource.SearchParam();
		param.id = testRecord.id;

		CurrencyResource.SearchApi api = new CurrencyResource.SearchApi();
		CurrencyResource.SearchResult res = (CurrencyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(testRecord.Name_L1__c, res.records[0].name);
	}

	/**
	 * Search a Currency record(Positive case: test for L1 name(multi lang))
	 */
	@isTest
	static void searchApiGetNameL2Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'United States dollar');

		Test.startTest();

		CurrencyResource.SearchParam param = new CurrencyResource.SearchParam();
		param.id = testRecord.id;

		CurrencyResource.SearchApi api = new CurrencyResource.SearchApi();
		CurrencyResource.SearchResult res = (CurrencyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(testRecord.Name_L2__c, res.records[0].name);
	}

	/**
   * Search ISO Currency code list(Positive case)
   */
	@isTest
	static void searchIsoCurrencyCodeApiTest() {
		CurrencyResource.SearchIsoCodeParam param = new CurrencyResource.SearchIsoCodeParam();

		CurrencyResource.SearchIsoCurrencyCodeApi api = new CurrencyResource.SearchIsoCurrencyCodeApi();
		CurrencyResource.SearchIsoCodeResult res;

		// Search by no param
		param.label = '';
		res = (CurrencyResource.SearchIsoCodeResult)api.execute(param);

		System.assertEquals(160, res.records.size());

		// Search by '円'
		param.label = 'JPY';
		res = (CurrencyResource.SearchIsoCodeResult)api.execute(param);

		ComIsoCurrencyCode jpy = ComIsoCurrencyCode.valueOf('JPY');
		System.assertEquals(1, res.records.size());
		System.assertEquals('JPY', res.records[0].value);
		System.assertEquals(jpy.label, res.records[0].label);
	}

	/**
	 * Set test parameter tp request parameter
	 * @param param Target object to set data
	 */
	static void setTestParam(CurrencyResource.UpsertParam param) {
		param.isoCurrencyCode = 'USD';
		param.name_L0 = 'United States dollar';
		param.name_L1 = '米ドル';
		param.name_L2 = '美元';
		param.decimalPlaces = 2;
		param.symbol = '$';
	}

}
