/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署リポジトリのテストクラス
 */
@isTest
private class DepartmentRepositoryTest {

	/** テストデータクラス */
	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj {get; private set;}
		/** 権限オブジェクト */
		public ComPermission__c permissionObj;
		/** 親部署 */
		ComDeptBase__c parentDeptObj;
		/** 上長 */
		ComEmpBase__c managerObj;

		/** コンストラクタ */
		public TestData() {
			this.orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			this.companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
			this.permissionObj = ComTestDataUtility.createPermission('Test Permission', this.companyObj.Id);
			this.parentDeptObj = ComTestDataUtility.createDepartmentWithHistory('親部署', this.companyObj.Id);
			this.managerObj = ComTestDataUtility.createEmployeeWithHistory('Test社員', this.companyObj.Id, null, Userinfo.getUserId(), this.permissionObj.Id);
		}

		/**
		 * テスト用の部署エンティティを作成する
		 */
		public DepartmentBaseEntity createDeptEntity(String name, String code) {

			DepartmentBaseEntity baseEntity = new DepartmentBaseEntity();
			baseEntity.name = name;
			baseEntity.currentHistoryId = parentDeptObj.CurrentHistoryId__c;
			baseEntity.code = code;
			baseEntity.uniqKey = baseEntity.createUniqKey(companyObj.Code__c, code);
			baseEntity.companyId = companyObj.id;
			baseEntity.addHistory(createDeptHistoryEntity(name + '1', code, AppDate.newInstance(2017, 12, 15), AppDate.newInstance(2018, 4, 1)));
			baseEntity.addHistory(createDeptHistoryEntity(name + '2', code, AppDate.newInstance(2018, 4, 1), AppDate.newInstance(2019, 1, 1)));

			return baseEntity;
		}

		/**
		 * テスト用の部署エンティティを作成する
		 */
		public DepartmentHistoryEntity createDeptHistoryEntity(String name, String code, AppDate validFrom, AppDate validTo) {
			DepartmentHistoryEntity historyEntity = new DepartmentHistoryEntity();
			historyEntity.name = name;
			historyEntity.nameL0 = name + '_L0';
			historyEntity.nameL1 = name + '_L1';
			historyEntity.nameL2 = name + '_L2';
			historyEntity.historyComment = '改訂コメント';
			historyEntity.validFrom = validFrom;
			historyEntity.validTo = validTo;
			historyEntity.isRemoved = true;
			historyEntity.managerId = managerObj.Id;
			historyEntity.parentBaseId = parentDeptObj.Id;
			historyEntity.uniqKey = historyEntity.createUniqKey(code);

			return historyEntity;
		}


		public List<ComDeptBase__c> createDepartmentsWithHistory(String name, Integer baseSize, Integer historySize) {
			List<ComDeptBase__c> deptList =  ComTestDataUtility.createDepartmentsWithHistory(
					name, this.companyObj.Id, baseSize, historySize);

			// 履歴に親部署を設定
			List<ComDeptHistory__c> historyList = [SELECT Id, ParentBaseId__c FROM ComDeptHistory__c];
			for (ComDeptHistory__c history : historyList) {
				history.ParentBaseId__c = this.parentDeptObj.Id;
			}
			update historyList;
			return deptList;
		}
	}

	/**
	 * 指定したIDのベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest
	static void getEntityTest() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);

		Test.startTest();
			DepartmentRepository repo = new DepartmentRepository();
			DepartmentBaseEntity resEntity = repo.getEntity(deptBaseList[1].Id, null);
		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		assertBaseEntity(deptBaseList[1], resEntity);
		Map<Id, ComDeptHistory__c> historyMap = getHistoryMapByBaseId(deptBaseList[1].Id);
		System.assertEquals(historyMap.size(), resEntity.getHistoryList().size());
		for (DepartmentHistoryEntity historyEntity : resEntity.getHistoryList()) {
			assertHistoryEntity(historyMap.get(historyEntity.id), historyEntity);
		}
	}

	/**
	 * 存在しないベースIDを指定した場合、nullが返却されることを確認する
	 */
	@isTest
	static void getEntityTestNotExist() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		Id deleteId = deptBaseList[1].Id;
		delete deptBaseList[1];

		Test.startTest();
			DepartmentRepository repo = new DepartmentRepository();
			DepartmentBaseEntity resEntity = repo.getEntity(deleteId, null);
		Test.stopTest();

		System.assertEquals(null, resEntity);
	}

	/**
	 * 指定したIDの指定した日付で有効なベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest
	static void getEntityTestTargetDate() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		final AppDate targetDate = AppDate.today();

		Test.startTest();
			DepartmentRepository repo = new DepartmentRepository();
			DepartmentBaseEntity resEntity = repo.getEntity(deptBaseList[1].Id, targetDate);
		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		assertBaseEntity(deptBaseList[1], resEntity);
		Map<Id, ComDeptHistory__c> historyMap = getHistoryMapByBaseId(deptBaseList[1].Id);
		System.assertEquals(1, resEntity.getHistoryList().size());
		System.assertEquals(targetDate, resEntity.getHistoryList().get(0).validFrom);
	}

	/**
	 * 指定した履歴IDのベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest
	static void getEntityByHistoryIdTest() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		ComDeptBase__c targetBaseObj = deptBaseList[1];
		Map<Id, ComDeptHistory__c> historyMap = getHistoryMapByBaseId(targetBaseObj.Id);
		Id targetHistoryId = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :targetBaseObj.Id LIMIT 1].Id;

		Test.startTest();
			DepartmentRepository repo = new DepartmentRepository();
			DepartmentBaseEntity resEntity = repo.getEntityByHistoryId(targetHistoryId);
		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetBaseObj.Id, resEntity.id);
		System.assertEquals(1, resEntity.getHistoryList().size());
		System.assertEquals(targetHistoryId, resEntity.getHistoryList().get(0).id);
	}


	/**
	 * 指定したIDのベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest static void searchEntityTest() {

		// 部署名検索はsearchEntityTestDeptNameに実装しています

		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 3, 2);

		// １レコードだけ親部署を変更する
		ComDeptBase__c targetDeptBase = deptBaseList[2];
		ComDeptBase__c targetParentDeptBase = deptBaseList[0];
		List<ComDeptHistory__c> historyObjs = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :targetDeptBase.Id LIMIT 1];
		ComDeptHistory__c tergetHistory = historyObjs[0];
		tergetHistory.ParentBaseId__c = targetParentDeptBase.Id;
		update tergetHistory;

		DepartmentRepository repo = new DepartmentRepository();
		DepartmentRepository.SearchFilter filter;
		List<DepartmentBaseEntity> entityList;
		DepartmentRepository.HistorySortOrder sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_DESC;

		// 会社IDを指定して取得
		filter = new DepartmentRepository.SearchFilter();
		filter.companyIds = new List<Id>{testData.companyObj.Id};
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(4, entityList.size()); // TestDataの親部署含む
		for (DepartmentBaseEntity entity : entityList) {
			System.assertEquals(filter.companyIds[0], entity.companyId);
		}

		// コードを指定して取得
		filter = new DepartmentRepository.SearchFilter();
		filter.codes = new List<String>{deptBaseList[1].Code__c};
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		for (DepartmentBaseEntity entity : entityList) {
			System.assertEquals(filter.codes[0], entity.code);
		}

		// 親部署を指定して検索
		filter = new DepartmentRepository.SearchFilter();
		filter.parentIds = new List<Id>{targetParentDeptBase.Id};
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		System.assertEquals(targetDeptBase.Id, entityList[0].Id);
		System.assertEquals(1, entityList[0].getHistoryList().size());
		System.assertEquals(tergetHistory.ParentBaseId__c, entityList[0].getHistoryList()[0].parentBaseId);

		// 対象日を指定して検索
		historyObjs= [SELECT Id, ValidFrom__c, ValidTo__c FROM ComDeptHistory__c WHERE BaseId__c = :targetDeptBase.Id LIMIT 1];
		tergetHistory = historyObjs[0];
		tergetHistory.ValidFrom__c = Date.newInstance(2016, 12, 19);
		tergetHistory.ValidTo__c = Date.newInstance(2016, 12, 20); // 失効日
		update tergetHistory;
		filter = new DepartmentRepository.SearchFilter();
		filter.targetDate = AppDate.newInstance(2016, 12, 19);
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		System.assertEquals(targetDeptBase.Id, entityList[0].Id);
		System.assertEquals(1, entityList[0].getHistoryList().size());
		System.assertEquals(tergetHistory.Id, entityList[0].getHistoryList()[0].id);
	}

	/**
	 * 指定したIDのベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest static void searchEntityTestDeptName() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		ComDeptBase__c targetDeptBase = deptBaseList[1];
		List<ComDeptHistory__c> historyObjs = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :targetDeptBase.Id LIMIT 1];
		ComDeptHistory__c tergetHistory = historyObjs[0];

		// 部署名設定
		tergetHistory.Name_L0__c = '部署 L0 あいうえお';
		tergetHistory.Name_L1__c = '部署 L1 あいうえお';
		tergetHistory.Name_L2__c = '部署 L2 あいうえお';
		update tergetHistory;

		DepartmentRepository repo = new DepartmentRepository();
		DepartmentRepository.SearchFilter filter;
		List<DepartmentBaseEntity> entityList;
		DepartmentRepository.HistorySortOrder sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_DESC;

		// L0部分一致検索
		// 一致する
		filter = new DepartmentRepository.SearchFilter();
		filter.nameL = 'L0 あいう';
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		System.assertEquals(targetDeptBase.Id, entityList[0].Id);
		System.assertEquals(1, entityList[0].getHistoryList().size());
		System.assertEquals(tergetHistory.Id, entityList[0].getHistoryList()[0].id);

		// L1部分一致検索
		// 一致する
		filter = new DepartmentRepository.SearchFilter();
		filter.nameL = 'L1 あいう';
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		System.assertEquals(targetDeptBase.Id, entityList[0].Id);
		System.assertEquals(1, entityList[0].getHistoryList().size());
		System.assertEquals(tergetHistory.Id, entityList[0].getHistoryList()[0].id);

		// L2部分一致検索
		// 一致する
		filter = new DepartmentRepository.SearchFilter();
		filter.nameL = 'L2 あいう';
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		System.assertEquals(targetDeptBase.Id, entityList[0].Id);
		System.assertEquals(1, entityList[0].getHistoryList().size());
		System.assertEquals(tergetHistory.Id, entityList[0].getHistoryList()[0].id);

		// 一致しない
		filter = new DepartmentRepository.SearchFilter();
		filter.nameL = 'Lx';
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(0, entityList.size());

	}

	/**
	 * 検索のテスト
	 * 履歴リストの並び順が正しく取得できることを確認する
	 */
	@isTest static void searchEntityTestSortOrder() {

		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 3, 3);

		DepartmentRepository repo = new DepartmentRepository();
		DepartmentRepository.SearchFilter filter;
		filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{deptBaseList[0].Id};
		DepartmentRepository.HistorySortOrder sortOrder;
		List<DepartmentBaseEntity> entityList;
		List<DepartmentHistoryEntity> historyList;

		// 履歴リストが有効開始日の昇順
		sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_ASC;
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		historyList = entityList[0].getHistoryList();
		System.assertEquals(3, historyList.size());
		for (Integer i = 1; i < historyList.size(); i++) {
			// 前のリストより有効開始日が後のはず
			System.assert(historyList[i - 1].validFrom.getDate() < historyList[i].validFrom.getDate(),
					'有効開始日が昇順になっていません。history[i-1].validFrom=' + historyList[i - 1].validFrom.format()
					+ ',history[i].validFrom=' + historyList[i].validFrom.format());
		}

		// 履歴リストが有効開始日の降順
		sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_DESC;
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		historyList = entityList[0].getHistoryList();
		System.assertEquals(3, historyList.size());
		for (Integer i = 1; i < historyList.size(); i++) {
			// 前のリストより有効開始日が後のはず
			System.assert(historyList[i - 1].validFrom.getDate() > historyList[i].validFrom.getDate(),
					'有効開始日が降順になっていません。history[i-1].validFrom=' + historyList[i - 1].validFrom.format()
					+ ',history[i].validFrom=' + historyList[i].validFrom.format());
		}

	}

	/**
	 * ベースエンティティ取得のテスト
	 * 指定したベースIDのエンティティが取得できることを確認する
	 */
	@isTest
	static void getBaseEntityTest() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		Id targetId = deptBaseList[1].Id;

		Test.startTest();
			DepartmentRepository repo = new DepartmentRepository();
			DepartmentBaseEntity resEntity = repo.getBaseEntity(targetId);
		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetId, resEntity.id);
	}

	/**
	 * ベースエンティティ取得のテスト
	 * 存在しないベースIDを指定した場合、nullが返却されることを確認する
	 */
	@isTest
	static void getBaseEntityTestNotExist() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		Id deleteId = deptBaseList[1].Id;
		delete deptBaseList[1];

		Test.startTest();
			DepartmentRepository repo = new DepartmentRepository();
			DepartmentBaseEntity resEntity = repo.getBaseEntity(deleteId);
		Test.stopTest();

		System.assertEquals(null, resEntity);
	}

	/**
	 * 指定した履歴IDの履歴エンティティが取得できることを確認する
	 */
	@isTest static void getHistoryEntityTest() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		ComDeptBase__c targetDeptBase = deptBaseList[1];
		List<ComDeptHistory__c> historyObjs = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :targetDeptBase.Id LIMIT 1];
		ComDeptHistory__c tergetHistory = historyObjs[0];

		DepartmentRepository repo = new DepartmentRepository();
		DepartmentRepository.SearchFilter filter;
		DepartmentHistoryEntity entity;

		// 履歴エンティティが存在する場合
		entity = repo.getHistoryEntity(tergetHistory.Id);
		System.assertNotEquals(null, entity);
		System.assertEquals(tergetHistory.Id, entity.id);

		// 履歴エンティティが存在しない場合(ベースのIDで検索してみる)
		entity = repo.getHistoryEntity(targetDeptBase.Id);
		System.assertEquals(null, entity);
	}

	/**
	 * 指定したベースIDの履歴エンティティが取得できることを確認する
	 */
	@isTest static void getHistoryEntityByBaseIdTest() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		ComDeptBase__c targetDeptBase = deptBaseList[1];
		List<ComDeptHistory__c> historyObjs = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :targetDeptBase.Id];
		ComDeptHistory__c tergetHistory = historyObjs[0];

		DepartmentRepository repo = new DepartmentRepository();
		DepartmentRepository.SearchFilter filter;
		List<DepartmentHistoryEntity> entityList;

		// 履歴エンティティが存在する場合
		entityList = repo.getHistoryEntityByBaseId(targetDeptBase.Id);
		System.assertEquals(historyObjs.size(), entityList.size());
		for (DepartmentHistoryEntity entity : entityList) {
			System.assertEquals(targetDeptBase.Id, entity.baseId);
		}

		// 論理削除されているレコードは対象外
		tergetHistory.Removed__c = true;
		update tergetHistory;
		entityList = repo.getHistoryEntityByBaseId(targetDeptBase.Id);
		System.assertEquals(historyObjs.size() - 1, entityList.size());
	}

	/**
	 * 指定したベースIDと対象日の履歴エンティティが取得できることを確認する
	 */
	@isTest static void getHistoryEntityByBaseIdTestDate() {
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 3);
		ComDeptBase__c targetDeptBase = deptBaseList[1];
		List<ComDeptHistory__c> historyObjs = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c = :targetDeptBase.Id];
		ComDeptHistory__c tergetHistory = historyObjs[0];

		DepartmentRepository repo = new DepartmentRepository();
		DepartmentRepository.SearchFilter filter;
		List<DepartmentHistoryEntity> entityList;
		APpDate targetDate;

		// 対象日に履歴エンティティが存在する場合
		targetDate = AppDate.today();
		entityList = repo.getHistoryEntityByBaseId(targetDeptBase.Id, targetDate);
		System.assertEquals(1, entityList.size());
		for (DepartmentHistoryEntity entity : entityList) {
			System.assertEquals(targetDeptBase.Id, entity.baseId);
			System.assert(entity.validFrom.getDate() <= targetDate.getDate() && entity.validTo.getDate() > targetDate.getDate(),
					'対象日が有効期間に含まれていません');
		}

		// 対象日に履歴エンティティが存在しない
		// 空のリストが返却されるはず
		targetDate = AppDate.today().addMonths(-1);
		entityList = repo.getHistoryEntityByBaseId(targetDeptBase.Id, targetDate);
		System.assertEquals(true, entityList.isEmpty());

	}

	/**
	 * 新規の部署のベースと履歴エンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		// テストデータ作成
		DepartmentRepositoryTest.TestData testData = new TestData();
		DepartmentBaseEntity deptEntity = testData.createDeptEntity('Test部署', '001');

		// テスト実行
		DepartmentRepository repo = new DepartmentRepository();
		Test.startTest();
			Repository.SaveResultWithChildren result = repo.saveEntity(deptEntity);
		Test.stopTest();

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		// ベース
		Id baseId = result.details[0].id;
		List<ComDeptBase__c> deptBaseObjList = [
				SELECT Id, Name, Code__c, UniqKey__c, CurrentHistoryId__c, CompanyId__c
				FROM ComDeptBase__c
				WHERE Id = :baseId];
		System.assertEquals(1, deptBaseObjList.size());
		assertSavedBaseObj(deptEntity, deptBaseObjList[0]);
		// 履歴
		List<ComDeptHistory__c> deptHistoryObjList = [
				SELECT Id, Name, BaseId__c, UniqKey__c, Name_L0__c, Name_L1__c, Name_L2__c,
					ManagerBaseId__c, ParentBaseId__c, Remarks__c, HistoryComment__c,
					ValidFrom__c, ValidTo__c, Removed__c
				FROM ComDeptHistory__c
				WHERE BaseId__c = :baseId
				ORDER BY ValidFrom__c];
		System.assertEquals(2, deptHistoryObjList.size());
		assertSavedHistoryObj(deptEntity.getHistoryList().get(0), deptHistoryObjList[0], baseId);
	}

	/**
	 * 新規の部署履歴エンティティが保存できることを確認する
	 */
	@isTest static void saveHistoryEntityTestNew() {
		// テストデータ作成
		DepartmentRepositoryTest.TestData testData = new TestData();
		List<ComDeptBase__c> deptBaseList = testData.createDepartmentsWithHistory('Test', 2, 2);
		ComDeptBase__c tagetBaseObj = deptBaseList[1];
		DepartmentHistoryEntity targetHistoryEntity =
				testData.createDeptHistoryEntity(tagetBaseObj.Name, tagetBaseObj.Code__c, AppDate.newInstance(2019, 1, 1), AppDate.newInstance(2020, 12, 31));
		targetHistoryEntity.baseId = tagetBaseObj.Id;

		// テスト実行
		DepartmentRepository repo = new DepartmentRepository();
		Test.startTest();
			Repository.SaveResult result = repo.saveHistoryEntity(targetHistoryEntity);
		Test.stopTest();

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		// 履歴
		Id historyId = result.details[0].id;
		List<ComDeptHistory__c> deptHistoryObjList = [
				SELECT Id, Name, BaseId__c, UniqKey__c, Name_L0__c, Name_L1__c, Name_L2__c,
					ManagerBaseId__c, ParentBaseId__c, Remarks__c, HistoryComment__c,
					ValidFrom__c, ValidTo__c, Removed__c
				FROM ComDeptHistory__c
				WHERE Id = :historyId
				ORDER BY ValidFrom__c];
		System.assertEquals(1, deptHistoryObjList.size());
		assertSavedHistoryObj(targetHistoryEntity, deptHistoryObjList[0], targetHistoryEntity.baseId);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];
		ComDeptHistory__c history = getHistoryObjs(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		DepartmentRepository repository = new DepartmentRepository(true);

		// 検索（DBから取得）
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.Id};
		filter.historyIds = new List<Id>{history.Id};
		DepartmentBaseEntity department = repository.searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		DepartmentBaseEntity cachedDepartment = repository.searchEntity(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(department.id, cachedDepartment.id);
		System.assertNotEquals('NotCache', cachedDepartment.code);
		System.assertNotEquals('NotCache', cachedDepartment.getHistoryList()[0].historyComment);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];
		ComDeptHistory__c history = getHistoryObjs(base.Id).get(0);

		// 検索（DBから取得）
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.Id};
		filter.historyIds = new List<Id>{history.Id};
		DepartmentBaseEntity department = new DepartmentRepository(true).searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		DepartmentBaseEntity cachedDepartment = new DepartmentRepository(true).searchEntity(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(department.id, cachedDepartment.id);
		System.assertNotEquals('NotCache', cachedDepartment.code);
		System.assertNotEquals('NotCache', cachedDepartment.getHistoryList()[0].historyComment);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。（部署ベース検索）
	 */
	@isTest static void searchBaseNotFromCacheDifferentCondition() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];

		// キャッシュをonでインスタンスを生成
		DepartmentRepository repository = new DepartmentRepository(true);

		// 検索（DBから取得）
		List<Id> baseIds = new List<Id>{base.Id};
		DepartmentBaseEntity department = repository.getBaseEntityList(baseIds, false)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		baseIds.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		DepartmentBaseEntity searchedDepartment = repository.getBaseEntityList(baseIds, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(department.id, searchedDepartment.id);
		System.assertEquals('NotCache', searchedDepartment.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。（部署履歴検索）
	 */
	@isTest static void searchHistoryNotFromCacheDifferentCondition() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];
		ComDeptHistory__c history = getHistoryObjs(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		DepartmentRepository repository = new DepartmentRepository(true);

		// 検索（DBから取得）
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.Id};
		DepartmentRepository.HistorySortOrder sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_ASC;
		DepartmentHistoryEntity department = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.baseIds.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		DepartmentHistoryEntity searchedDepartment = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(department.id, searchedDepartment.id);
		System.assertEquals('NotCache', searchedDepartment.historyComment);
	}

	/**
	 * 部署ベースが同一で異なる履歴を検索した場合、条件に一致する履歴のみが取得結果のベースに紐づくことを検証する。（部署ベース + 履歴検索）
	 */
	@isTest static void searchEntityWityHistoryFromCacheSameBase() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];
		delete getHistoryObjs(base.Id).get(0);

		// 部署履歴を3件作成
		DepartmentHistoryEntity history1 = testData.createDeptHistoryEntity('history1', 'code1', AppDate.newInstance(2019, 4, 1), AppDate.newInstance(2019, 5, 1));
		history1.baseId = base.Id;
		history1.isRemoved = false;
		DepartmentHistoryEntity history2 = testData.createDeptHistoryEntity('history2', 'code2', AppDate.newInstance(2019, 5, 1), AppDate.newInstance(2019, 6, 1));
		history2.baseId = base.Id;
		history2.isRemoved = false;
		DepartmentHistoryEntity history3 = testData.createDeptHistoryEntity('history3', 'code3', AppDate.newInstance(2019, 6, 1), AppDate.newInstance(2019, 7, 1));
		history3.baseId = base.Id;
		history3.isRemoved = false;
		new DepartmentRepository().saveHistoryEntityList(new List<DepartmentHistoryEntity>{history1, history2, history3});

		// キャッシュをonでインスタンスを生成
		DepartmentRepository repository = new DepartmentRepository(true);

		// 検索（ベース1件 + 履歴3件）
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.Id};
		DepartmentRepository.HistorySortOrder sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_ASC;
		DepartmentBaseEntity department = repository.searchEntityWithHistory(filter, false, sortOrder)[0];
		System.assertEquals(base.id, department.id);
		System.assertEquals(3, department.getHistoryList().size());

		// 異なる条件で同じベースを再検索（ベース1件 + 履歴1件）
		filter.targetDate = AppDate.newInstance(2019, 5, 1);
		DepartmentBaseEntity searchedDepartment = repository.searchEntityWithHistory(filter, false, sortOrder)[0];
		System.assertEquals(department.id, searchedDepartment.id);
		System.assertEquals(1, searchedDepartment.getHistoryList().size());
	}

	/**
	 * ソート順が異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheDifferentSortOrder() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];
		ComDeptHistory__c history = getHistoryObjs(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		DepartmentRepository repository = new DepartmentRepository(true);

		// 検索（DBから取得）
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.historyIds = new List<Id>{history.Id};
		DepartmentRepository.HistorySortOrder sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_ASC;
		DepartmentHistoryEntity department = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 異なるソート順で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_DESC;
		DepartmentHistoryEntity searchedDepartment = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(department.id, searchedDepartment.id);
		System.assertEquals('NotCache', searchedDepartment.historyComment);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];
		ComDeptHistory__c history = getHistoryObjs(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		DepartmentRepository repository = new DepartmentRepository(true);

		// 検索（DBから取得）
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.Id};
		DepartmentRepository.HistorySortOrder sortOrder = DepartmentRepository.HistorySortOrder.VALID_FROM_ASC;
		DepartmentBaseEntity department = repository.searchEntityWithHistory(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		DepartmentBaseEntity searchedDepartment = repository.searchEntityWithHistory(filter, true, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertNotEquals(department, searchedDepartment);
		System.assertEquals('NotCache', searchedDepartment.code);
		System.assertEquals('NotCache', searchedDepartment.getHistoryList()[0].historyComment);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		TestData testData = new TestData();
		ComDeptBase__c base = testData.createDepartmentsWithHistory('Test', 1, 1)[0];
		ComDeptHistory__c history = getHistoryObjs(base.Id).get(0);

		// キャッシュをoffでインスタンスを生成
		DepartmentRepository repository = new DepartmentRepository();

		// 検索（DBから取得）
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.Id};
		filter.historyIds = new List<Id>{history.Id};
		DepartmentBaseEntity department = repository.searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		DepartmentBaseEntity searchedDepartment = repository.searchEntity(filter)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertNotEquals(department, searchedDepartment);
		System.assertEquals('NotCache', searchedDepartment.code);
		System.assertEquals('NotCache', searchedDepartment.getHistoryList()[0].historyComment);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		DepartmentRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new DepartmentRepository().isEnabledCache);
	}

	/**
	 * 指定したベースIDの履歴オブジェクトを取得する
	 */
	private static Map<Id, ComDeptHistory__c> getHistoryMapByBaseId(Id baseId) {
		return new Map<Id, ComDeptHistory__c>([
				SELECT Id, Name, BaseId__c, UniqKey__c, HistoryComment__c, ValidFrom__c, ValidTo__c,
						Name_L0__c, Name_L1__c, Name_L2__c,
						ManagerBaseId__c, ParentBaseId__c, Remarks__c, Removed__c
				FROM ComDeptHistory__c WHERE BaseId__c = :baseId]);
	}

	/**
	 * ベースエンティティが正しく取得できているかを検証する
	 */
	private static void assertBaseEntity(ComDeptBase__c expObj, DepartmentBaseEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CurrentHistoryId__c, actEntity.currentHistoryId);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		if (expObj.CompanyId__c != null) {
			ComCompany__c comapny = [SELECT Id, Code__c FROM ComCompany__c WHERE Id = :expObj.CompanyId__c];
			System.assertEquals(comapny.Code__c, actEntity.company.code);
		}
	}

	/**
	 * 履歴エンティティが正しく取得できているかを検証する
	 */
	private static void assertHistoryEntity(ComDeptHistory__c expObj, DepartmentHistoryEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.BaseId__c, actEntity.baseId);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.HistoryComment__c, actEntity.historyComment);
		System.assertEquals(expObj.ValidFrom__c, actEntity.validFrom.getDate());
		System.assertEquals(expObj.ValidTo__c, actEntity.validTo.getDate());
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);

		System.assertEquals(expObj.ParentBaseId__c, actEntity.parentBaseId);
		System.assertEquals(expObj.Remarks__c, actEntity.remarks);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);

		System.assertEquals(expObj.ManagerBaseId__c, actEntity.managerId);
		if (actEntity.managerId != null) {
			List<ComEmpBase__c> empList = [
					SELECT Id, FirstName_L0__c, LastName_L0__c, FirstName_L1__c, LastName_L1__c, FirstName_L2__c, LastName_L2__c
					FROM ComEmpBase__c WHERE id = :expObj.ManagerBaseId__c];
			System.assertEquals(empList[0].FirstName_L0__c, actEntity.manager.fullName.firstNameL0);
			System.assertEquals(empList[0].FirstName_L1__c, actEntity.manager.fullName.firstNameL1);
			System.assertEquals(empList[0].FirstName_L2__c, actEntity.manager.fullName.firstNameL2);
			System.assertEquals(empList[0].LastName_L0__c, actEntity.manager.fullName.lastNameL0);
			System.assertEquals(empList[0].LastName_L1__c, actEntity.manager.fullName.lastNameL1);
			System.assertEquals(empList[0].LastName_L2__c, actEntity.manager.fullName.lastNameL2);
		}
	}

	/**
	 * ComDeptBase__cに正しく保存できているかを検証する
	 */
	private static void assertSavedBaseObj(DepartmentBaseEntity expEntity, ComDeptBase__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.currentHistoryId, actObj.CurrentHistoryId__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
	}

	/**
	 * ComDeptHistory__cに正しく保存できているかを検証する
	 */
	private static void assertSavedHistoryObj(DepartmentHistoryEntity expEntity, ComDeptHistory__c actObj, Id baseId) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(baseId, actObj.BaseId__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.historyComment, actObj.HistoryComment__c);
		System.assertEquals(expEntity.validFrom.getDate(), actObj.ValidFrom__c);
		System.assertEquals(expEntity.validTo.getDate(), actObj.ValidTo__c);

		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.managerId, actObj.ManagerBaseId__c);
		System.assertEquals(expEntity.parentBaseId, actObj.ParentBaseId__c);
		System.assertEquals(expEntity.remarks, actObj.Remarks__c);
	}

	/**
	 * 指定したベースIDの部署履歴オブジェクトを取得する
	 */
	private static List<ComDeptHistory__c> getHistoryObjs(Id baseId) {
		return [
				SELECT
					Id, Name, BaseId__c, HistoryComment__c,
					ManagerBaseId__c, Removed__c, UniqKey__c,
					Name_L0__c, Name_L1__c, Name_L2__c,
					ValidFrom__c, ValidTo__c, ParentBaseId__c
				FROM
					ComDeptHistory__c
				WHERE
					BaseId__c = :baseId
				ORDER BY ValidFrom__c Asc	];

	}
}
