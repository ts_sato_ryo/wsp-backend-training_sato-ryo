/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 共通
  *
  * (廃止)チェックイン情報のリポジトリ
  * このリポジトリは廃止になりました。
  * This repository has been deprecated.
  */
public with sharing class CheckInInfoRepository {
}