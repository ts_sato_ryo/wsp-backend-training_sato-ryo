/**
 * @group 共通
 *
 * @description 階層関係のオブジェクト構造の再帰参照チェックサービス
 */
public with sharing class CircularValidationService {
	/**
	 * 再帰参照的になっていることを示す例外
	 */
	public class CircularException extends App.BaseException {
		private String code = 'CIRCULAR';

		public String circularTargetId {get; private set;}
		public override String getErrorCode(){ return this.code; }
		public CircularException setCircularTargetId(String circularTargetId) {
			this.circularTargetId = circularTargetId;
			return this;
		}
	}
	/**
	 * 階層が上限超過になっていることを示す例外
	 */
	public class OverLevelException extends App.BaseException {
		private String code = 'OVER_LEVEL';

		public override String getErrorCode(){ return this.code;}
		public Integer levelLimit {get; private set;}
		public String rootTargetId {get; private set;}
		public OverLevelException(Integer levelLimit, String rootTargetId) {
			this.levelLimit = levelLimit;
			this.rootTargetId = rootTargetId;
		}
	}
	// 再帰チェック対象定義
	public class NodeEntity {
		/* 対象Id **/
		public String nodeId;
		/* 上位対象 **/
		public NodeEntity parentNode;
		/* 下位階層一覧 **/
		private List<NodeEntity> childNodes;
		/* 下位階層Id一覧 **/
		private Set<String> childNodeIds;

		public NodeEntity(String nodeId) {
			this.nodeid = nodeId;
			this.childNodes = new List<NodeEntity>();
			this.childNodeIds = new Set<String>();
		}
		/*
		 * 上位階層を設定
		 * @parentNode 設定する上位階層
		 */
		public void setParent(NodeEntity parentNode) {
			this.parentNode = parentNode;
		}
		/*
		 * 下位階層を追加する
		 * @childNode 追加される下位階層
		 */
		public void appendChild(NodeEntity childNode) {
			if (!this.childNodeIds.contains(childNode.nodeId)) {
				this.childNodes.add(childNode);
				this.childNodeIds.add(childNode.nodeId);
			}
		}
		/*
		 * 自分を含めて下位階層の階数を取得する
		 */
		public Integer getLevel() {
			System.debug(LoggingLeveL.DEBUG, '---current node of getLevel is ' + this.nodeId);
			if (this.isCircular()) {
				return LEVEL_LIMIT + 1;
			}
			System.debug(LoggingLeveL.DEBUG, '---current node is not isCircular node');
			Integer childMaxLevel = 0;
			for (NodeEntity childNode : this.childNodes) {
				System.debug(LoggingLeveL.DEBUG, '---childNode of getLevel is ' + childNode.nodeId);
				childMaxLevel = Math.max(childMaxLevel, childNode.getLevel());
			}
			return childMaxLevel+1;
		}
		/*
		 * 再帰参照されているかどうかを取得する
		 */
		public Boolean isCircular() {
			if (this.childNodes.isEmpty()) {
				return false;
			}
			return this.foundNodeInChild(new Set<String>{this.nodeId});
		}
		/*
		 * 下位階層に指定Idが存在する項目を検索して、そのIdを返す(自身Id含む)
		 * @param targetNodeIds チェック対象となる項目Id一覧
		 * @return 該当する項目があればtrue
		 */
		private Boolean foundNodeInChild(Set<String> targetNodeIds) {
			System.debug(LoggingLeveL.DEBUG, '---current node of foundNodeInChild is ' + nodeId);
			System.debug(LoggingLeveL.DEBUG, '---targetNodeIds of foundNodeInChild is ' + targetNodeIds);
			for (NodeEntity childNode : this.childNodes) {
				if (targetNodeIds.contains(childNode.nodeId)) {
					System.debug(LoggingLeveL.DEBUG, '---found target node');
					return true;
				} else {
					System.debug(LoggingLeveL.DEBUG, '---cannot found target node');
					targetNodeIds.add(childNode.nodeId);
					return childNode.foundNodeInChild(targetNodeIds);
				}
			}
			return false;
		}
		/*
		 * 対象項目と下位階層の項目をツリー形式で成形する(デバッグ用)
		 * @param level 下位階層の最大階数
		 * @return ツリー形式の文字列
		 */
		public String format(Integer level) {
			if (level > LEVEL_LIMIT) return '';
			String ret = (level == 0 ? '\r\n' : '');
			ret += ('-'.repeat(level) + this.nodeId + '\r\n');
			List<NodeEntity> childNodes = this.childNodes;
			for (NodeEntity childNode : childNodes) {
				ret += childNode.format(level+1);
			}
			return ret;
		}
	}
	/*
	 * 指定しない場合の階層化可能の最大階層数
	 */
	private static Integer LEVEL_LIMIT = 10;

	private Integer maxLevel = LEVEL_LIMIT;
	private Map<String, NodeEntity> nodeMap = new Map<String, NodeEntity>();
	private Set<String> leafNodeIds = new Set<String>();
	/*
	 * コンストラクタ
	 * 最大階層数は指定しない(デフォルト値を使用)
	 */
	public CircularValidationService() {}
	/*
	 * コンストラクタ
	 * 最大階層数は指定する
	 * @param 指定する最大階層数
	 */
	public CircularValidationService (Integer maxLevel) {
		if (maxLevel > LEVEL_LIMIT) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_MaxValue(
				new List<String>{'maxLevel', String.valueOf(LEVEL_LIMIT)}));
		}
		this.maxLevel = maxLevel;
	}
	/*
	 * 再帰参照チェック対象を登録し、階層上限と再帰チェックを行う
	 * @param targetId チェック対象Id
	 * @param parentId 上位対象Id。nullの場合、最上位対象とする
	 * 再帰チェック失敗した場合、CircularExceptionが発生する
	 * 階層階数チェック失敗した場合、OverLevelExceptionが発生する
	 */
	public void appendChild(String targetId, String parentId) {
		System.debug(LoggingLeveL.DEBUG, '---CircularValidationService.appendChild ' + targetId + '-' + parentId);
		NodeEntity parentNode = null;
		if (parentId != null) {
			if (!nodeMap.containsKey(parentId)) {
				parentNode = new NodeEntity(parentId);
				nodeMap.put(parentId, parentNode);
			} 
			parentNode = nodeMap.get(parentId);
		}
		NodeEntity childNode = null;
		if (!nodeMap.containsKey(targetId)) {
			childNode = new NodeEntity(targetId);
			nodeMap.put(targetId, childNode);
		}
		childNode = nodeMap.get(targetId);
		childNode.setParent(parentNode);
		// 上位関係を構築
		if (parentNode != null) {
			parentNode.appendChild(childNode);
		}
		validateCircular(targetId);
	}
	/*
	 * 再帰チェックを行う
	 * チェック失敗した場合、CircularExceptionが発生する
	 */
	private void validateCircular(String nodeId) {
		List<NodeEntity> rootNodes = getRootNodes();
		if (rootNodes.isEmpty()) {
			throw new CircularException().setCircularTargetId(nodeId);
		}
		for (NodeEntity rootNode : rootNodes) {
			if (rootNode.isCircular()) {
				throw new CircularException().setCircularTargetId(nodeId);
			}
		}
		for (NodeEntity rootNode : rootNodes) {
			if (rootNode.getLevel() > this.maxLevel) {
				throw new OverLevelException(this.maxLevel, rootNode.nodeId);
			}
		}
	}
	/*
	 * 登録されているツリーからルート項目一覧を取得する
	 * @return ルード項目一覧
	 */
	private List<NodeEntity> getRootNodes() {
		List<NodeEntity> retList = new List<NodeEntity>();
		for (NodeEntity node : nodeMap.values()) {
			if (node.parentNode == null) {
				System.debug(LoggingLeveL.DEBUG, '---root node of getRootNodes is ' + node.format(0));
				retList.add(node);
			}
		}
		return retList;
	}
}