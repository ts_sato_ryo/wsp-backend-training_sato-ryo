/**
 * @group 工数
 *
 * @description
 * TimeValidatorTestのテストクラス
 */
 @isTest
private class TimeValidatorTest {
	/**
	 * TimeRequestValidator.validateのテスト
	 * - 適当な引数でValidatorが正常終了することを確認する
	 */
	@isTest static void timeRequestEntityValidatorTest() {
		// テストデータ作成
		TimeRequestEntity entity = new TimeRequestEntity();
		entity.name = 'valid name';

		// 実行
		TimeValidator.TimeRequestEntityValidator validator =
				new TimeValidator.TimeRequestEntityValidator(entity);
		Validator.Result result;
		try {
			Test.startTest();
			result = validator.validate();
			Test.stopTest();
		} catch (Exception e) {
			TestUtil.fail('!!! Unexpected Error Occured !!!' + e.getCause());
		}
		// 検証
		System.assertEquals(0, result.getErrors().size(), '!!! Validation Error Occured !!!' + result.getErrors());
	}

	/**
	 * TimeRequestValidator.validateのテスト
	 * - 不正な工数確定申請申請名でValidatorでエラーが発生することを確認する
	 */
	@isTest static void timeRequestEntityValidatorTestInvalidName() {
		// テストデータ作成
		TimeRequestEntity entity = new TimeRequestEntity();
		entity.name = '';

		// 実行
		TimeValidator.TimeRequestEntityValidator validator =
				new TimeValidator.TimeRequestEntityValidator(entity);
		Validator.Result result;
		try {
			Test.startTest();
			result = validator.validate();
			Test.stopTest();
		} catch (Exception e) {
			TestUtil.fail('!!! Unexpected Error Occured !!!' + e.getCause());
		}
		// 検証
		List<Validator.Error> actualList = result.getErrors();
		System.assertEquals(1, result.getErrors().size(), '!!! Validation Error Occured !!!' + actualList);
		Validator.Error actualError = actualList.get(0);
		System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, actualError.code);
		System.assertEquals(
				ComMessage.msg().Com_Err_NotSetValueAofB(new List<String>{ComMessage.msg().Time_Lbl_TimeTrackRequest, ComMessage.msg().Admin_Lbl_Name}),
				actualError.message);
	}

	/**
	 * TimeSummaryValidator.validateのテスト
	 * - 適当な引数でValidatorが正常終了することを確認する
	 */
	@isTest static void timeSummaryEntityValidatorTest() {
		// テストデータ作成
		TimeRecordItemEntity recordItem = new TimeRecordItemEntity();
		recordItem.name = 'valid name';
		TimeRecordEntity record = new TimeRecordEntity();
		record.name = 'valid name';
		record.replaceRecordItemList(new List<TimeRecordItemEntity>{recordItem});
		TimeSummaryEntity entity = new TimeSummaryEntity();
		entity.name = 'valid name';
		entity.replaceRecordList(new List<TimeRecordEntity>{record});

		// 実行
		TimeValidator.TimeSummaryEntityValidator validator =
				new TimeValidator.TimeSummaryEntityValidator(entity);
		Validator.Result result;
		try {
			Test.startTest();
			result = validator.validate();
			Test.stopTest();
		} catch (Exception e) {
			TestUtil.fail('!!! Unexpected Error Occured !!!' + e.getCause());
		}
		// 検証
		System.assertEquals(0, result.getErrors().size(), '!!! Validation Error Occured !!!' + result.getErrors());
	}

	/**
	 * TimeSummaryValidator.validateのテスト
	 * - 不正な工数サマリー名でValidatorでエラーが発生することを確認する
	 * - 不正な工数明細名でValidatorでエラーが発生することを確認する
	 * - 不正な工数明細内訳名でValidatorでエラーが発生することを確認する
	 */
	@isTest static void timeSummaryEntityValidatorTestInvalidName() {
		// テストデータ作成
		TimeRecordItemEntity recordItem = new TimeRecordItemEntity();
		recordItem.name = '';
		TimeRecordEntity record = new TimeRecordEntity();
		record.name = '';
		record.replaceRecordItemList(new List<TimeRecordItemEntity>{recordItem});
		TimeSummaryEntity entity = new TimeSummaryEntity();
		entity.name = '';
		entity.replaceRecordList(new List<TimeRecordEntity>{record});

		// 実行
		TimeValidator.TimeSummaryEntityValidator validator =
				new TimeValidator.TimeSummaryEntityValidator(entity);
		Validator.Result result;
		try {
			Test.startTest();
			result = validator.validate();
			Test.stopTest();
		} catch (Exception e) {
			TestUtil.fail('!!! Unexpected Error Occured !!!' + e.getCause());
		}
		// 検証
		List<Validator.Error> actualList = result.getErrors();
		System.assertEquals(3, result.getErrors().size(), '!!! Validation Error Occured !!!' + actualList);
		Validator.Error actualError = actualList.get(0);
		System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, actualError.code);
		System.assertEquals(
				ComMessage.msg().Com_Err_NotSetValueAofB(new List<String>{ComMessage.msg().Trac_Lbl_TimeSummary, ComMessage.msg().Admin_Lbl_Name}),
				actualError.message);
		actualError = actualList.get(1);
		System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, actualError.code);
		System.assertEquals(
				ComMessage.msg().Com_Err_NotSetValueAofB(new List<String>{ComMessage.msg().Time_Lbl_TimeTrackRecord, ComMessage.msg().Admin_Lbl_Name}),
				actualError.message);
		actualError = actualList.get(2);
		System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, actualError.code);
		System.assertEquals(
				ComMessage.msg().Com_Err_NotSetValueAofB(new List<String>{ComMessage.msg().Time_Lbl_TimeTrackRecordItem, ComMessage.msg().Admin_Lbl_Name}),
				actualError.message);
	}

	/**
	 * TimeRecordEntityValidatorのテスト
	 * - 作業報告の桁あふれエラーが発生することを確認する
	 */
	@isTest static void timeRecordEntityValidatorTestInvalidNote() {
		// テストデータ作成
		TimeRecordEntity record = new TimeRecordEntity();
		record.name = 'valid name';
		record.note = '===ダミーテキスト4096字以上===';
		for (Integer i = 0; i < 64 ; i++) {
			record.note += 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...';// 64 * 64 = 4096 文字
		}

		// 実行
		TimeValidator.TimeRecordEntityValidator validator =
				new TimeValidator.TimeRecordEntityValidator(record);
		Validator.Result result;
		try {
			Test.startTest();
			result = validator.validate();
			Test.stopTest();
		} catch (Exception e) {
			TestUtil.fail('!!! Unexpected Error Occured !!!' + e.getCause());
		}

		// 検証
		List<Validator.Error> actualList = result.getErrors();
		System.assertEquals(1, result.getErrors().size());
		Validator.Error actualError = actualList.get(0);
		System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, actualError.code);
		System.assertEquals(
				ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{ComMessage.msg().Cal_Lbl_WorkReport, String.valueOf(TimeRecordEntity.NOTE_MAX_LENGTH)}),
				actualError.message);
	}
}
