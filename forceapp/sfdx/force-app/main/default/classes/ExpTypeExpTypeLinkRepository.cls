/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description 費目別費目のリポジトリ Repository class for ExpTypeExpTypeLink
 */
public with sharing class ExpTypeExpTypeLinkRepository extends Repository.BaseRepository {
	/**　最大取得レコード件数 Max number of records to fetch */
	private final static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする Roll back transaction if there are any failures in saving */
	private final static Boolean ALL_SAVE = true;

	/** オブジェクト名 Object name */
	private static final String BASE_OBJECT_NAME = ExpTypeExpTypeLink__c.SObjectType.getDescribe().getName();
	/** 費目(親)のリレーション名 Relationship name of Parent ExpType */
	private static final String PARENT_EXP_TYPE_R = ExpTypeExpTypeLink__c.ExpTypeParentId__c.getDescribe().getRelationshipName();
	/** 費目(子)のリレーション名 Relationship name of Child ExpType */
	private static final String CHILD_EXP_TYPE_R = ExpTypeExpTypeLink__c.ExpTypeChildId__c.getDescribe().getRelationshipName();

	/**
	 * 取得対象の項目名 Target fields to fetch data
	 */
	private static final List<String> GET_BASE_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpTypeExpTypeLink__c.Id,
			ExpTypeExpTypeLink__c.Name,
			ExpTypeExpTypeLink__c.ExpTypeParentId__c,
			ExpTypeExpTypeLink__c.ExpTypeChildId__c
		};

		GET_BASE_FIELD_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** 検索フィルタ Search Filter */
	public class SearchFilter {
		/** 費目別費目IDセット Set of ExpTypeExpTypeLink Ids */
		public Set<Id> ids;
		/** 費目(親)IDセット Set of Parent ExpType Ids */
		public Set<Id> expTypeParentIds;
		/** 費目(子)IDセット Set of Child ExpType Ids */
		public Set<Id> expTypeChildIds;
		/** 取得対象日 */
		public AppDate targetDate;
	}

	/**
	 * 指定したIDを持つ費目別費目を取得する Retrieve entity by the specified Id
	 * @param id 取得対象のID ID of the target entity
	 * @return 指定したIDを持つ費目別費目、ただし該当する費目別費目が存在しない場合はnull Entity of specified ID. If no data is found, return null.
	 */
	public ExpTypeExpTypeLinkEntity getEntity(Id id) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>{id};
		List<ExpTypeExpTypeLinkEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 指定したIDを持つ費目別費目を取得する Retrieve entities by the specified Ids
	 * @param idList 取得対象のID List of IDs of the target entity
	 * @return 指定したIDを持つ費目別費目、該当する費目別費目が存在しない場合は空のリストを返却する List of Entities. If no data is found, return an empty list.
	 */
	public List<ExpTypeExpTypeLinkEntity> getEntityList(List<Id> idList) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>(idList);
		return searchEntityList(filter);
	}

	/**
	 * 指定した費目(親)IDを持つ費目別費目を取得する Retrieve entities by the specified Parent ExpType Id
	 * @param idList 取得対象の費目(親)ID ID of the target Parent ExpType
	 * @return 指定したIDを持つ費目別費目、該当する費目別費目が存在しない場合は空のリストを返却する  List of Entities. If no data is found, return an empty list.
	 */
	public List<ExpTypeExpTypeLinkEntity> getEntityListByParentExpTypeId(Id expTypeParentId) {
		SearchFilter filter = new SearchFilter();
		filter.expTypeParentIds = new Set<Id>{expTypeParentId};
		return searchEntityList(filter);
	}

	/**
	 * 指定した費目(子)IDを持つ費目別費目を取得する Retrieve entities by the specified Child ExpType Id
	 * @param idList 取得対象の費目(子)ID ID of the target Child ExpType
	 * @return 指定したIDを持つ費目別費目、該当する費目別費目が存在しない場合は空のリストを返却する  List of Entities. If no data is found, return an empty list.
	 */
	public List<ExpTypeExpTypeLinkEntity> getEntityListByChildExpTypeId(Id expTypeChildId) {
		SearchFilter filter = new SearchFilter();
		filter.expTypeChildIds = new Set<Id>{expTypeChildId};
		return searchEntityList(filter);
	}

	/*
	 * Check if the given parent&child pair is valid.
	 * @param parentId Id of parent expense type.(Hotel Fee)
	 * @param childId Id of child expense type.
	 * @return Whether the given pair is valid.
	 */
	public Boolean isValidParentChildLink(Id parentId, Id childId) {
		SearchFilter filter = new SearchFilter();
		filter.expTypeParentIds = new Set<Id>{parentId};
		filter.expTypeChildIds = new Set<Id>{childId};
		return (searchEntityList(filter).size() > 0);
	}

	/**
	 * Search ExpTypeExpTypeLinks based on the filter conditions
	 * @param filter 検索フィルタ Search filter
	 * @param withChildDetails Flag to indicate if detail information of Child ExpType is retrieved
	 * @param withExtendedItems Flag to indicate if extended item information is retrieved
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト Search result
	 */
	public List<ExpTypeExpTypeLinkEntity> searchEntityList(SearchFilter filter) {

		// SELECT対象項目リスト Target fields
		List<String> selectFieldList = new List<String>();

		// 基本項目 ExpTypeExpTypeLink base fields must be retrieved
		selectFieldList.addAll(GET_BASE_FIELD_LIST);

		// WHERE句を生成 Create WHERE statement
		List<String> conditions = new List<String>();
		final Set<Id> ids = filter.ids;
		if (ids != null) {
			conditions.add(createInExpression(ExpTypeExpTypeLink__c.Id, 'ids'));
		}
		final Set<Id> expTypeParentIds = filter.expTypeParentIds;
		if (expTypeParentIds != null) {
			conditions.add(createInExpression(ExpTypeExpTypeLink__c.ExpTypeParentId__c, 'expTypeParentIds'));
		}
		final Set<Id> expTypeChildIds = filter.expTypeChildIds;
		if (expTypeChildIds != null) {
			conditions.add(createInExpression(ExpTypeExpTypeLink__c.ExpTypeChildId__c, 'expTypeChildIds'));
		}
		Date pTargetDate;
		if (filter.targetDate != null) {
			pTargetDate = filter.targetDate.getDate();
			conditions.add(getFieldName(CHILD_EXP_TYPE_R, ExpType__c.ValidFrom__c) + ' <= :pTargetDate');
			conditions.add(getFieldName(CHILD_EXP_TYPE_R, ExpType__c.ValidTo__c) + ' > :pTargetDate');
		}

		// SOQLを生成 Create SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + BASE_OBJECT_NAME
				+ buildWhereString(conditions)
				+ ' ORDER BY ' + getFieldName(PARENT_EXP_TYPE_R, ExpType__c.Code__c) + ' ASC'
				+ ',' + getFieldName(CHILD_EXP_TYPE_R, ExpType__c.Code__c) + ' ASC'
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		System.debug('ExpTypeExpTypeLinkRepository: SOQL=' + soql);

		// クエリ実行 Execute query
		List<ExpTypeExpTypeLink__c> sObjList = Database.query(soql);

		// generate return values
		List<ExpTypeExpTypeLinkEntity> entityList = createEntityList(sObjList);
		return entityList;
	}

	/**
	 * 費目別費目を保存する Save ExpTypeExpTypeLink
	 * @param entity 保存対象の費目別費目 ExpTypeExpTypeLink entity
	 * @return 保存結果 Save Result
	 */
	public Repository.SaveResult saveEntity(ExpTypeExpTypeLinkEntity entity) {
		return saveEntityList(new List<ExpTypeExpTypeLinkEntity>{entity});
	}

	/**
	 * 費目別費目を保存する Save ExpTypeExpTypeLink
	 * @param entityList 保存対象の費目別費目 List of ExpTypeExpTypeLink entities
	 * @return 保存結果 Save Result
	 */
	public Repository.SaveResult saveEntityList(List<ExpTypeExpTypeLinkEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	}

	/**
	 * エンティティを保存する Save entity
	 * @param entity 保存対象のエンティティのリスト List of entities
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる If set to false, DML operations can complete even if a record fails
	 * @return 保存結果のリスト List of saved results
	 */
	public virtual Repository.SaveResult saveEntityList(List<ExpTypeExpTypeLinkEntity> entityList, Boolean allOrNone) {

		// エンティティをSObjectに変換する
		List<ExpTypeExpTypeLink__c> sObjList = new List<ExpTypeExpTypeLink__c>();
		for (ExpTypeExpTypeLinkEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}

		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/**
	 * エンティティからSObjectを作成する Create object from entity
	 * @param entity 作成元のエンティティ（nullは許容しない）Target entity to create (cannot be null)
	 * @return 作成したsObject Created Object
	 */
	private ExpTypeExpTypeLink__c createSObject(ExpTypeExpTypeLinkEntity entity) {
		ExpTypeExpTypeLink__c sobj = new ExpTypeExpTypeLink__c(Id = entity.Id);

		if (entity.isChanged(ExpTypeExpTypeLinkEntity.Field.EXP_TYPE_PARENT_ID)) {
			sobj.ExpTypeParentId__c = entity.expTypeParentId;
		}
		if (entity.isChanged(ExpTypeExpTypeLinkEntity.Field.EXP_TYPE_CHILD_ID)) {
			sobj.ExpTypeChildId__c = entity.expTypeChildId;
		}

		return sobj;
	}

	/**
	 * Create entity list from object list
	 * @param sObjList List of target objects to create
	 * @param childExpTypeEntityMap Map of child ExpTypeEntities to their Ids
	 * @return Created entity list
	 */
	private List<ExpTypeExpTypeLinkEntity> createEntityList(List<ExpTypeExpTypeLink__c> sObjList) {
		List<ExpTypeExpTypeLinkEntity> entityList = new List<ExpTypeExpTypeLinkEntity>();

		if (!sObjList.isEmpty()) {
			for (ExpTypeExpTypeLink__c sObj : sObjList) {
				ExpTypeExpTypeLinkEntity entity = new ExpTypeExpTypeLinkEntity();
				entity.setId(sObj.Id);
				entity.expTypeParentId = sObj.ExpTypeParentId__c;
				entity.expTypeChildId = sObj.ExpTypeChildId__c;

				entity.resetChanged();

				entityList.add(entity);
			}
		}

		return entityList;
	}
}
