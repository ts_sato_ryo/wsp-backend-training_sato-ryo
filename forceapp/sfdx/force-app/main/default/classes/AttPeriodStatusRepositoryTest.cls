/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * AttPeriodStatusRepositoryのテスト
 */
@isTest
private class AttPeriodStatusRepositoryTest {

	/** テストデータクラス */
	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj {get; private set;}
		/** タグオブジェクト */
		public ComTag__c tagObj {get; private set;}
		/** 権限 */
		public ComPermission__c permissionObj;
		/** 社員 */
		public ComEmpBase__c employeeObj;
		/** 短時間勤務設定 */
		public AttShortTimeWorkSettingBase__c shortTimeSettingObj;
		/** 休職休業設定 */
		public AttLeaveOfAbsence__c leaveOfAbsenceObj;

		/** コンストラクタ */
		public TestData() {
			this.orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			this.countryObj = ComTestDataUtility.createCountry('Japan');
			this.companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
			this.tagObj = ComTestDataUtility.createTag('Test Tag', TagType.SHORTEN_WORK_REASON,
					this.companyObj.id, 1, false);
			this.permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
			this.employeeObj = ComTestDataUtility.createEmployeeWithHistory(
					'Test Employee', this.companyObj.Id, null, UserInfo.getUserId(), permissionObj.Id);
			this.shortTimeSettingObj = ComTestDataUtility.createAttShortTimeSettingWithHistory(
					'Test', this.companyObj.Id, this.tagObj.Id);
			this.leaveOfAbsenceObj = ComTestDataUtility.createLeaveOfAbsence(
					this.companyObj, 'Test Absence', false);
		}

		/**
		 * 勤怠期間を作成する
		 */
		public List<AttPeriodStatus__c>  createAttPeriodStatuses(
				String name, Integer recordSise) {
			return ComTestDataUtility.createAttPeriodStatuses(
					name,
					this.employeeObj.Id,
					AttPeriodStatusType.SHORT_TIME_WORK_SETTING.value,
					this.shortTimeSettingObj.Id,
					this.leaveOfAbsenceObj.Id,
					recordSise);
		}

		/**
		 * 社員を作成する
		 */
		public ComEmpBase__c createEmployee(String name) {
			return ComTestDataUtility.createEmployeeWithHistory(
					name, this.companyObj.Id, null, UserInfo.getUserId(), this.permissionObj.Id);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityTestExistEntity() {

		final Integer recordSise = 3;
		AttPeriodStatusRepositoryTest.TestData testData = new TestData();
		List<AttPeriodStatus__c> targetObjList = testData.createAttPeriodStatuses('Test', recordSise);
		AttPeriodStatus__c targetObj = targetObjList[1];

		AttPeriodStatusRepository repo = new AttPeriodStatusRepository();
		AttPeriodStatusEntity resEntity;

		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのエンティティが存在しない場合はnullが返却されることを確認する
	 */
	@isTest static void getEntityTestNotExistEntity() {

		final Integer recordSise = 3;
		AttPeriodStatusRepositoryTest.TestData testData = new TestData();
		List<AttPeriodStatus__c> targetObjList = testData.createAttPeriodStatuses('Test', recordSise);
		AttPeriodStatus__c targetObj = targetObjList[1];
		delete targetObj;

		AttPeriodStatusRepository repo = new AttPeriodStatusRepository();
		AttPeriodStatusEntity resEntity;

		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityListのテスト
	 * 検索できることを確認する。有効期間の検索テストはsearchEntityListTestValidPeriodで実施。
	 */
	@isTest static void searchEntityListTest() {

		final Integer recordSise = 3;
		AttPeriodStatusRepositoryTest.TestData testData = new TestData();
		List<AttPeriodStatus__c> targetObjList = testData.createAttPeriodStatuses('Test', recordSise);
		AttPeriodStatus__c targetObj = targetObjList[1];

		AttPeriodStatusRepository repo = new AttPeriodStatusRepository();
		AttPeriodStatusRepository.SearchFilter filter;
		List<AttPeriodStatusEntity> resList;

		// 勤怠期間IDで検索
		// 全ての項目の値が取得できていることを確認する
		filter = new AttPeriodStatusRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resList = repo.searchEntityList(filter);
		System.assertEquals(1, resList.size());
		System.assertEquals(targetObj.Id, resList[0].id);
		AttPeriodStatus__c expObj = getPeriodStatusObj(resList[0].id);
		assertSearchEntity(expObj, resList[0]);

		// 社員ベースIDで検索
		ComEmpBase__c testEmployee = testData.createEmployee('社員検索テスト');
		targetObj.EmployeeBaseId__c = testEmployee.Id;
		update targetObj;
		filter = new AttPeriodStatusRepository.SearchFilter();
		filter.employeeIds = new Set<Id>{testEmployee.Id};
		resList = repo.searchEntityList(filter);
		System.assertEquals(1, resList.size());
		System.assertEquals(targetObj.Id, resList[0].id);

		// 勤怠期間種別で検索
		targetObj.Type__c = AttPeriodStatusType.ABSENCE.value;
		update targetObj;
		filter = new AttPeriodStatusRepository.SearchFilter();
		filter.periodStatusTypes = new Set<AttPeriodStatusType>{AttPeriodStatusType.ABSENCE};
		resList = repo.searchEntityList(filter);
		System.assertEquals(1, resList.size());
		System.assertEquals(targetObj.Id, resList[0].id);

	}

	/**
	 * searchEntityListのテスト
	 * 指定した対象日に有効な履歴が取得できることを確認する
	 */
	@isTest static void searchEntityTestValidDate() {

		final Integer recordSise = 3;
		AttPeriodStatusRepositoryTest.TestData testData = new TestData();
		List<AttPeriodStatus__c> targetObjList = testData.createAttPeriodStatuses('Test', recordSise);
		AttPeriodStatus__c targetObj = targetObjList[1];
		targetObj.ValidFrom__c = Date.newInstance(2000, 4, 18);
		targetObj.ValidTo__c = Date.newInstance(2001, 3, 31);
		update targetObj;

		AttPeriodStatusRepository repo = new AttPeriodStatusRepository();
		AttPeriodStatusRepository.SearchFilter filter = new AttPeriodStatusRepository.SearchFilter();
		List<AttPeriodStatusEntity> resList;

		// 取得対象日に履歴の有効期間開始日を指定
		filter.targetDate = AppDate.valueOf(targetObj.ValidFrom__c);
		resList = repo.searchEntityList(filter);
		System.assertEquals(1, resList.size());
		System.assertEquals(targetObj.Id, resList[0].id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		filter.targetDate = AppDate.valueOf(targetObj.ValidTo__c.addDays(-1));
		resList = repo.searchEntityList(filter);
		System.assertEquals(1, resList.size());
		System.assertEquals(targetObj.Id, resList[0].id);

		// 取得対象日に最新履歴の失効日を指定
		// 取得できないはず
		filter.targetDate = AppDate.valueOf(targetObj.ValidTo__c);
		resList = repo.searchEntityList(filter);
		System.assertEquals(0, resList.size());

	}

	/**
	 * saveEntityのテスト
	 * 新規エンティティの保存ができることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		AttPeriodStatusRepositoryTest.TestData testData = new TestData();

		AttPeriodStatusEntity entity = new AttPeriodStatusEntity();
		entity.name = 'テスト';
		entity.ownerId = UserInfo.getUserId();
		entity.employeeBaseId = testData.employeeObj.Id;
		entity.validFrom = AppDate.today();
		entity.validTo = entity.validFrom.addDays(10);
		entity.type = AttPeriodStatusType.SHORT_TIME_WORK_SETTING;
		entity.shortTimeWorkSettingBaseId = testData.shortTimeSettingObj.Id;
		entity.comment = '勤怠期間適用コメント';

		AttPeriodStatusRepository repo = new AttPeriodStatusRepository();
		Repository.SaveResult result = repo.saveEntity(entity);
		System.assertEquals(true, result.isSuccessAll);
		AttPeriodStatus__c resObj = getPeriodStatusObj(result.details[0].id);
		System.assertNotEquals(null, resObj);
		System.assertEquals(entity.ownerId, resObj.OwnerId);
		System.assertEquals(entity.employeeBaseId, resObj.EmployeeBaseId__c);
		System.assertEquals(entity.validFrom.getDate(), resObj.ValidFrom__c);
		System.assertEquals(entity.validTo.getDate(), resObj.ValidTo__c);
		System.assertEquals(entity.type.value, resObj.Type__c);
		System.assertEquals(entity.shortTimeWorkSettingBaseId, resObj.ShortTimeWorkSettingBaseId__c);
		System.assertEquals(entity.leaveOfAbsenceId, resObj.LeaveOfAbsenceId__c);
		System.assertEquals(entity.comment, resObj.Comment__c);
	}

	/**
	 * saveEntityのテスト
	 * 既存のエンティティが更新できることを確認する
	 */
	@isTest static void saveEntityTestUpdate() {
		final Integer recordSise = 3;
		AttPeriodStatusRepositoryTest.TestData testData = new TestData();
		List<AttPeriodStatus__c> targetObjList = testData.createAttPeriodStatuses('Test', recordSise);
		AttPeriodStatus__c targetObj = targetObjList[1];

		// saveEntityTestNewテストで各項目が保存できることを確認しているので
		// ここでは既存のレコードが更新できることを確認するだけとする
		AttPeriodStatusEntity entity = new AttPeriodStatusEntity();
		entity.setId(targetObj.Id);
		entity.name = '更新テスト';

		AttPeriodStatusRepository repo = new AttPeriodStatusRepository();
		Repository.SaveResult result = repo.saveEntity(entity);
		System.assertEquals(true, result.isSuccessAll);
		AttPeriodStatus__c resObj = getPeriodStatusObj(result.details[0].id);
		System.assertNotEquals(null, resObj);
		System.assertEquals(entity.name, resObj.Name);
	}

	/**
	 * 指定したIDの勤怠期間オブジェクトを取得する
	 */
	private static AttPeriodStatus__c getPeriodStatusObj(Id id) {
		return [
				SELECT
					Id, Name, OwnerId, EmployeeBaseId__c, ValidFrom__c, ValidTo__c,
					Type__c, ShortTimeWorkSettingBaseId__c, LeaveOfAbsenceId__c, Comment__c,
					EmployeeBaseId__r.LastName_L0__c, EmployeeBaseId__r.LastName_L1__c,
					EmployeeBaseId__r.LastName_L2__c,
					EmployeeBaseId__r.FirstName_L0__c, EmployeeBaseId__r.FirstName_L1__c,
					EmployeeBaseId__r.FirstName_L2__c,
					LeaveOfAbsenceId__r.Name_L0__c,
					LeaveOfAbsenceId__r.Name_L1__c,
					LeaveOfAbsenceId__r.Name_L2__c
				FROM
					AttPeriodStatus__c
				WHERE Id = :id
		];
	}

	/**
	 * 検索で取得したエンティティの各項目の値が取得できていることを確認する
	 */
	private static void assertSearchEntity(AttPeriodStatus__c expObj, AttPeriodStatusEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.OwnerId, actEntity.ownerId);
		System.assertEquals(expObj.EmployeeBaseId__c, actEntity.employeeBaseId);
		System.assertEquals(expObj.ValidFrom__c, actEntity.validFrom.getDate());
		System.assertEquals(expObj.ValidTo__c, actEntity.validTo.getDate());
		System.assertEquals(expObj.Type__c, actEntity.type.value);
		System.assertEquals(expObj.ShortTimeWorkSettingBaseId__c, actEntity.shortTimeWorkSettingBaseId);
		System.assertEquals(expObj.LeaveOfAbsenceId__c, actEntity.leaveOfAbsenceId);
		System.assertEquals(expObj.Comment__c, actEntity.comment);

		if (expObj.EmployeeBaseId__c != null) {
			System.assertNotEquals(null, actEntity.employee);
			AppPersonName fullNameL = actEntity.employee.fullNameL;
			System.assertEquals(expObj.EmployeeBaseId__r.FirstName_L0__c, fullNameL.firstNameL0);
			System.assertEquals(expObj.EmployeeBaseId__r.FirstName_L1__c, fullNameL.firstNameL1);
			System.assertEquals(expObj.EmployeeBaseId__r.FirstName_L2__c, fullNameL.firstNameL2);
			System.assertEquals(expObj.EmployeeBaseId__r.LastName_L0__c, fullNameL.lastNameL0);
			System.assertEquals(expObj.EmployeeBaseId__r.LastName_L1__c, fullNameL.lastNameL1);
			System.assertEquals(expObj.EmployeeBaseId__r.LastName_L2__c, fullNameL.lastNameL2);
		}

		if (expObj.LeaveOfAbsenceId__c != null) {
			AppMultiString nameL = actEntity.leaveOfAbsence.nameL;
			System.assertEquals(expObj.LeaveOfAbsenceId__r.Name_L0__c, nameL.valueL0);
			System.assertEquals(expObj.LeaveOfAbsenceId__r.Name_L1__c, nameL.valueL1);
			System.assertEquals(expObj.LeaveOfAbsenceId__r.Name_L2__c, nameL.valueL2);
		}
	}

	/**
	 * 検索で取得したエンティティの各項目の値が取得できていることを確認する
	 */
	private static void assertSavedObj(AttPeriodStatusEntity expEntity, AttPeriodStatus__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.ownerId, actObj.OwnerId);
		System.assertEquals(expEntity.employeeBaseId, actObj.EmployeeBaseId__c);
		System.assertEquals(expEntity.validFrom.getDate(), actObj.ValidFrom__c);
		System.assertEquals(expEntity.validTo.getDate(), actObj.ValidTo__c);
		System.assertEquals(expEntity.type.value, actObj.Type__c);
		System.assertEquals(expEntity.shortTimeWorkSettingBaseId, actObj.ShortTimeWorkSettingBaseId__c);
		System.assertEquals(expEntity.comment, actObj.Comment__c);
	}

}
