/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description Office365連携用のサービスクラス。
 */
public with sharing class Office365Service implements Service {

	private static final String OFFICE365_CLIENT_ID = 'eee03ef3-dbef-44e8-8357-006a5e7b07f0';
	private static final String OFFICE365_CLIENT_SECRET = 'rleiMUF317!$xqaTWIY05^*';

	private static final String OFFICE365_OAUTH_ENDPOINT_URL               = 'https://login.microsoftonline.com/common/adminconsent';
	private static final String OFFICE365_REDIRECT_URL_AFTER_AUTHORIZATION = 'https://oauth.teamspirit.co.jp/wsp/Office365OAuthRedirectPage.html';

	private static final Integer OFFICE365_API_TIMEOUT = 10000;

	// デフォルトアカウント(ユーザとしてこれを指定すると、認証を行ったユーザ自身のカレンダーを見に行く)
	private static final String OFFICE365_CALENDAR_DEFAULT_ACCOUNT = 'me';

	// メッセージ
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/*
	 * OAuth認証URLを取得する
	 * 外部カレンダー連携が有効の場合に実行される
	 * ステート情報は呼び出し元で作成する
	 * @return OAuth認証URL
	 */
	public static String getOAuthEndpointUrl() {
		String oAuthUrl = OFFICE365_OAUTH_ENDPOINT_URL
											+ '?client_id=' + EncodingUtil.urlEncode(OFFICE365_CLIENT_ID, 'UTF-8')
											+ '&redirect_uri=' + EncodingUtil.urlEncode(OFFICE365_REDIRECT_URL_AFTER_AUTHORIZATION, 'UTF-8');
		return oAuthUrl;
	}

	/*
	 * 認証状態を確認する
	 * OAuth認証が正しく行われるかを確認する
	 * @param 会社ID
	 * @return 認証結果
	 */
	public static String checkAuth(Id companyId) {

		// 予定取得開始日、終了日を取得する
		Date targetDateFrom = Date.today();
		Date targetDateTo = targetDateFrom.addDays(1);
		String eventUrl = getCalendarApiUrl(OFFICE365_CALENDAR_DEFAULT_ACCOUNT, targetDateFrom, targetDateTo);

		// APIから予定一覧を取得
		HttpResponse res;
		try {
			res = request(eventUrl, companyId);
		} catch (CalloutException ce) {
			System.debug(ce);
			if (ce.getMessage().startsWithIgnoreCase('Unauthorized endpoint')) {
				// リモートサイトが有効化されていない
				return PlannerSettingService.AuthStatus.REMOTE_SITE_SETTINGS_INACTIVE.name();
			} else {
				// 外部カレンダーへの接続に失敗
				return PlannerSettingService.AuthStatus.API_CONNECTION_FAILED.name();
			}
		}

		// ステータスコード401以外はAUTHORIZEDとする
		if (res.getStatusCode() == 401) {
			// 未認証（認証失敗）
			return PlannerSettingService.AuthStatus.UNAUTHORIZED.name();
		} else {
			// 認証済（認証成功）
			return PlannerSettingService.AuthStatus.AUTHORIZED.name();
		}
	}

	/*
	 * 指定日の予定を取得する
	 * @param ユーザID
	 * @param 予定取得開始日
	 * @param 予定取得終了日（その日の予定を含む）
	 * @return Office365の予定
	 */
	public static List<PlanEventEntity> getOffice365Events(Id userId, AppDate targetDateFrom, AppDate targetDateTo) {

		EmployeeBaseEntity empEntity = new EmployeeService().getEmployeeByUserId(userId, null);
		CompanyEntity comEntity = new CompanyRepository().getEntity(empEntity.companyId);

		List<PlanEventEntity> entityList = new List<PlanEventEntity>();

		// Office365のアカウントメールアドレスを取得
		String office365Account = getOffice365Account(userId);

		// 認証コードが存在しない場合はエラーとする
		if (String.isBlank(getAuthCode(comEntity.Id))) {
			// 未認証のエラー
			throw new App.IllegalStateException(MESSAGE.Cal_Msg_NoAuthSetting);
		}

		// URL作成
		Datetime fromDatetime = Datetime.newInstance(targetDateFrom.getDate(), Time.newInstance(0, 0, 0, 0));
		Datetime toDatetime = Datetime.newInstance(targetDateTo.getDate(), Time.newInstance(23, 59, 59, 0));
		String eventUrl = getCalendarApiUrl(office365Account, fromDatetime, toDatetime);

		// Office365のAPI実行
		HttpResponse res;
		try {
			res = request(eventUrl, comEntity.Id);
		} catch (CalloutException ce) {
			System.debug(ce);

			if (ce.getMessage().startsWithIgnoreCase('Unauthorized endpoint')) {
				// リモートサイトが有効化されていない
				throw new App.IllegalStateException(MESSAGE.Cal_Msg_RemoteSiteSettingsInActive);
			} else {
				// 外部カレンダーへの接続に失敗
				throw new App.IllegalStateException(MESSAGE.Cal_Msg_ApiConnectionFailed);
			}
		}

		// 成功ステータスの場合はメッセージをセットしない
		if (res.getStatusCode() != 200) {
			if (res.getStatusCode() == 401) {
				// 未認証（認証失敗）
				throw new App.IllegalStateException(MESSAGE.Cal_Msg_UnAuthorized);
			} else if (res.getStatusCode() == 404) {
				// 外部カレンダーに指定されたメールアドレスが登録されていない
				// 標準のメール項目をしている場合はメッセージを出さない
				return entityList;
			} else {
				// 外部カレンダーへの接続に失敗
				throw new App.IllegalStateException(MESSAGE.Cal_Msg_ApiConnectionFailed);
			}
		}

		// レスポンス作成
		String eventsResponseBody = res.getBody();
		Map<String, Object> eventsRes;
		try {
			eventsRes = (Map<String, Object>)JSON.deserializeUntyped(eventsResponseBody);
		} catch (JSONException je) {
			// Bodyが想定外の値だった場合
			throw new App.IllegalStateException(MESSAGE.Cal_Msg_ApiConnectionFailed);
		}

		List<Object> eventObjList = (List<Object>)eventsRes.get('value');

		// Office365からの予定取得に失敗した場合
		if (eventObjList == null) {
			// 外部カレンダーへの接続に失敗
			throw new App.IllegalStateException(MESSAGE.Cal_Msg_ApiConnectionFailed);
		}

		// Office365からイベント情報のエンティティに変換する
		for (Object eventObj : eventObjList) {
			Map<String, Object> eventMap = (Map<String, Object>)eventObj;
			Office365Event office365Event = new Office365Event(eventMap);
			if (!isEffectiveEvent(fromDatetime, toDatetime, office365Event)) {
				continue;
			}
			entityList.add(office365Event.toPlanEvent());
		}
		return entityList;
	}

	/*
	 * 予定取得API実行URLを取得する
	 * @param Office365アカウント
	 * @param 予定取得開始日
	 * @param 予定取得終了日
	 * @return 予定取得API実行URL
	 */
	@testVisible
	private static String getCalendarApiUrl(String Office365Account, Datetime targetDateFrom, DateTime targetDateTo) {

		String eventUrl = 'https://graph.microsoft.com/v1.0/users/' + EncodingUtil.urlEncode(office365Account, 'UTF-8') + '/calendarview';

		// 開始時刻と終了時刻
		eventUrl += '?StartDateTime=' + EncodingUtil.urlEncode(generateDateTimeString(targetDateFrom), 'UTF-8' ) + '&EndDateTime=' + EncodingUtil.urlEncode(generateDateTimeString(targetDateTo), 'UTF-8' );
		// 取得項目の指定
		eventUrl += '&$select=' + EncodingUtil.urlEncode('id,Subject,Start,End,IsAllDay', 'UTF-8');
		// 表示順を指定
		eventUrl += '&$orderby=' + EncodingUtil.urlEncode('Start/Datetime,End/Datetime', 'UTF-8');
		// 250件取得
		eventUrl += '&$top=250';
		// 非公開イベントは取得しない
		eventUrl += '&$filter=' + EncodingUtil.urlEncode('Sensitivity ne \'Private\'', 'UTF-8');

		return eventUrl;
	}

	/*
	 * リクエスト実行
	 * アクセストークン取得後に、HTTPリクエストを実行する
	 * @param 実行URL
	 * @param 会社ID
	 * @return HTTPレスポンス
	 */
	@testVisible
	private static HttpResponse request(String url, Id companyId) {

		// アクセストークン取得
		String accessToken = getAccessToken(companyId);

		// ヘッダー作成
		Map<String, String> headers = new Map<String, String>{
			'Accept' => 'application/json',
			'Authorization' => 'Bearer ' + accessToken
		};

		return requestRaw(url, 'GET', null, headers);
	}

	/*
	 * リクエスト実行
	 * @param 実行URL
	 * @param HTTPメソッド
	 * @param ボディ
	 * @param ヘッダー
	 * @return HTTPレスポンス
	 */
	@testVisible
	private static HttpResponse requestRaw(String url, String method, String body, Map<String, String> headers) {

		// リクエスト作成
		Http http = new Http();
		HttpRequest req = new HttpRequest();

		req.setEndpoint(url);
		req.setMethod(method);
		for (String name : headers.keySet()) {
			req.setHeader(name, headers.get(name));
		}
		if (body != null) {
			req.setBody(body);
		}
		req.setTimeout(OFFICE365_API_TIMEOUT);

		// リクエスト実行
		HttpResponse res = http.send(req);
		System.debug('status:' + res.getStatus());
		System.debug('statusCode:' + res.getStatusCode());
		System.debug('body:' + res.getBody());
		return res;
	}

	/*
	 * アクセストークン取得
	 * @param 会社ID
	 * @return アクセストークン
	 */
	@testVisible
	private static String getAccessToken(Id companyId) {

		// 認証コード取得 (認証コードは保存されている前提)
		String tenantId = getAuthCode(companyId);

		// ヘッダー作成
		Map<String, String> headers = new Map<String, String>{
			'Content-Type' => 'application/x-www-form-urlencoded'
		};

		// ボディ作成
		String body = 'grant_type=client_credentials'
			+ '&client_id=' + EncodingUtil.urlEncode(OFFICE365_CLIENT_ID, 'UTF-8')
			+ '&client_secret=' + EncodingUtil.urlEncode(OFFICE365_CLIENT_SECRET, 'UTF-8')
			+ '&scope=' + EncodingUtil.urlEncode('https://graph.microsoft.com/.default', 'UTF-8');

		// リクエスト実行
		HttpResponse res = requestRaw('https://login.microsoftonline.com/' + tenantId + '/oauth2/v2.0/token', 'POST', body, headers);

		// アクセストークン返却
		Map<String, Object> tokenMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
		String token = (String)tokenMap.get('access_token');
		return token;
	}

	/*
	 * 認証コードを取得
	 * @param 会社ID
	 * @return Office365の認証コード（テナントID） 存在しない場合はブランクを返却
	 */
	@testVisible
	public static String getAuthCode(Id companyId) {

		CompanyPrivateSettingRepository repo = new CompanyPrivateSettingRepository();
		CompanyPrivateSettingEntity entity = repo.getEntity(companyId);
		String tenantId = entity.office365AuthCode;

		if (String.isBlank(tenantId)) {
			return '';
		} else {
			return tenantId;
		}
	}

	/*
	 * 時刻を文字列に変換する
	 * @param 変換前の時刻
	 * @return 変換後の時刻
	 */
	@testVisible
	private static String generateDateTimeString(Datetime sfDateTime) {
		return sfDateTime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss') + 'Z';
	}

	/*
	 * Office365のアカウントを取得する（Salesforceのemailを使用することを前提）
	 * @param ユーザID
	 * @return Office365のアカウント
	 */
	@testVisible
	private static String getOffice365Account(Id userId) {

		UserEntity entity = new UserRepository().getEntity(userId);
		return entity.email;
	}

	/**
	 * Office365の予定情報がプランナーに表示すべきイベントか判定する
	 * 以下の予定は対象外とする
	 * ・検索条件の開始日時に終了する終日予定
	 * ・検索条件の開始日時に終了する終日ではない予定（開始日時と終了日時が同一のものは除く）
	 * @param searchFrom 検索条件の開始日時
	 * @param searchTo 検索条件の終了日時
	 * @param event Office365の予定情報
	 * @return プランナーに表示すべきイベントの場合true
	 */
	@TestVisible
	private static Boolean isEffectiveEvent(Datetime searchFrom, Datetime searchTo, Office365Event event) {
		if (event.isAllDay) {
			// 検索条件の開始日時に終了する終日予定は対象外
			if (event.endDatetime == Datetime.newInstanceGmt(searchFrom.date(), Time.newInstance(0, 0, 0, 0))) {
				return false;
			}
		} else {
			// 検索条件の開始日時に終了する終日ではない予定は対象外
			if (event.startDatetime != searchFrom && event.endDatetime == searchFrom) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Office365の予定情報1件を保持するクラス
	 */
	@TestVisible
	private class Office365Event {
		/** id */
		private String id;
		/** 件名 */
		private String subject;
		/** 開始時刻 */
		private Datetime startDatetime;
		/** 終了時刻 */
		private Datetime endDatetime;
		/** 終日予定フラグ */
		private Boolean isAllDay;

		/**
		 * Office365の予定情報1件を作成するコンストラクタ
		 * @param values 予定情報のMap
		 */
		@TestVisible
		private Office365Event(Map<String, Object> values) {
			this.id = (String)values.get('id');
			this.subject = (String)values.get('subject');
			this.startDateTime = generateSfDateTime((Map<String, Object>)values.get('start'));
			this.endDatetime = generateSfDateTime((Map<String, Object>)values.get('end'));
			this.isAllDay = (Boolean)values.get('isAllDay');
		}

		/*
		 * 時刻をDateTimeに変換する
		 * @param 変換前の時刻オブジェクト
		 * @return 変換後の時刻
		 */
		private Datetime generateSfDateTime(Map<String, Object> office365DateTime) {
			String dateTimeStr = (String)office365DateTime.get('dateTime');
			dateTimeStr = dateTimeStr.substring(0, 19).replace('T', ' ');

			// リクエスト時にタイムゾーンを指定していないので、デフォルト値のUTCが返ってくる。
			return Datetime.valueOfGmt(dateTimeStr); 
		}

		/**
		 * Office365の予定情報からPlanEventを作成する
		 * @return PlanEvent 作成した予定
		 */
		private PlanEventEntity toPlanEvent() {
			PlanEventEntity entity = new PlanEventEntity();
			entity.externalEventId = id;
			entity.subject = subject;
			entity.startDatetime = AppDatetime.valueOf(startDatetime);
			entity.createdServiceBy = PlanEventEntity.ServiceType.office365;
			entity.isAllDay = this.isAllDay;

			// 終日予定はPlanEventとOffice365で仕様が異なるので、PlanEventに合わせて終了日時を1日マイナスする。
			//（1日分の予定の場合、PlanEventでは開始日時/終了日時ともに当日0:00の予定として登録するが、Office365からは当日0:00〜翌日0:00の予定が返却される）
			entity.endDateTime = isAllDay ? AppDatetime.valueOf(endDatetime.addDays(-1)) : AppDatetime.valueOf(endDatetime);
			return entity;
		}
	}
}