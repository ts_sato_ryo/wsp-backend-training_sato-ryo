/**
 * AppDatetimeのテスト
 */
@isTest
private class AppDatetimeTest {

	/**
	 * コンストラクタのテスト（パラメータが文字列型）
	 */
	@isTest static void constructorTestString() {

		// 正しい形式で指定した場合、インスタンスが作成される
		AppDatetime dt = new AppDatetime('2017-07-01T12:31:20Z');
		Datetime expDt = Datetime.newInstanceGmt(2017, 7, 1, 12, 31, 20);
		System.assertEquals(expDt, dt.getDatetime());

		// ISO8601以外の形式を指定した場合、エラーが発生する
		try {
			dt = new AppDatetime('2017/07/01 12:31:20');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		// 空文字を設定した場合はエラーが発生する
		try {
			dt = new AppDatetime(' ');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * コンストラクタのテスト（パラメータがDatetime)
	 */
	@isTest static void constructorTestDatetime() {

		// 正しい形式で指定した場合、インスタンスが作成される
		Datetime dt = Datetime.newInstanceGmt(2017, 7, 1, 12, 31, 20);
		AppDatetime appDt = new AppDatetime(dt);
		System.assertEquals(dt, appDt.getDatetime());

		// null場合、エラーが発生する
		try {
			dt = null;
			appDt = new AppDatetime(dt);
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * nowのテスト
	 * 現在時刻で作成できることを確認する
	 */
	@isTest static void nowTest() {

		// 現在時刻で作成できることを確認する
		AppDatetime appDt = AppDatetime.now();
		Datetime now = Datetime.now();
		System.assert((appDt.getDatetime() > now.addSeconds(-5)) && (appDt.getDatetime() < now.addSeconds(5)));

		// 現在時刻に任意の値に設定できることを確認する
		AppDatetime.setNow(DateTime.newInstance(2017, 8, 25, 1, 2, 3));
		AppDateTime appNow = AppDatetime.now();
		System.assertEquals(1, appNow.getTime().hour());
		System.assertEquals(2, appNow.getTime().minute());
		System.assertEquals(3, appNow.getTime().second());
	}

	/**
	 * parseのテスト
	 * 指定した文字列からインスタンスが作成できることを確認する
	 */
	@isTest static void parseTest() {

		// 正しい形式で指定した場合、インスタンスが作成される
		AppDatetime dt = AppDatetime.parse('2017-07-01T12:31:20Z');
		Datetime expDt = Datetime.newInstanceGmt(2017, 7, 1, 12, 31, 20);
		System.assertEquals(expDt, dt.getDatetime());

	}

	/**
	 * valueOfUtcのテスト
	 * 指定した文字列からインスタンスが作成できることを確認する
	 */
	@isTest static void valudOfUtcTest() {

		// 正しい形式で指定した場合、インスタンスが作成される
		AppDatetime dt = AppDatetime.valueOfUtc('2017-07-01T12:31:20Z');
		Datetime expDt = Datetime.newInstanceGmt(2017, 7, 1, 12, 31, 20);
		System.assertEquals(expDt, dt.getDatetime());

	}

	/**
	 * valueOfLocalのテスト
	 * ローカルタイムゾーンのインスタンスが作成できることを確認する
	 */
	@isTest static void valueOfLocalTest() {

		// 正しい形式で指定した場合、インスタンスが作成される
		AppDatetime dt = AppDatetime.valueOfLocal('2017-07-01 12:31:20');
		Datetime expDt = Datetime.newInstance(2017, 7, 1, 12, 31, 20);
		System.assertEquals(expDt, dt.getDatetime());

		// ISO8601以外の形式を指定した場合、エラーが発生する
		try {
			dt = AppDatetime.valueOfLocal('2017/07/01 12:31:20');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		// 空文字を設定した場合はエラーが発生する
		try {
			dt = AppDatetime.valueOfLocal(' ');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * addDaysのテスト
	 * 指定した日数が加算できることを確認する
	 */
	@isTest static void addDaysTest() {
		Integer addDays = 1;
		Datetime dt = Datetime.now();
		System.assertEquals(dt.addDays(addDays), AppDatetime.valueOf(dt).addDays(addDays).getDatetime());
	}

	/**
	 * addHoursのテスト
	 * 指定した時間数が加算できることを確認する
	 */
	@isTest static void addHoursTest() {
		Integer add = 2;
		Datetime dt = Datetime.now();
		System.assertEquals(dt.addHours(add), AppDatetime.valueOf(dt).addHours(add).getDatetime());
	}

	/**
	 * addMinutesのテスト
	 * 指定した分数が加算できることを確認する
	 */
	@isTest static void addMinutesTest() {
		Integer add = 3;
		Datetime dt = Datetime.now();
		System.assertEquals(dt.addMinutes(add), AppDatetime.valueOf(dt).addMinutes(add).getDatetime());
	}

	/**
	 * addMonthsのテスト
	 * 指定した月数が加算できることを確認する
	 */
	@isTest static void addMonthsTest() {
		Integer add = 4;
		Datetime dt = Datetime.now();
		System.assertEquals(dt.addMonths(add), AppDatetime.valueOf(dt).addMonths(add).getDatetime());
	}

	/**
	 * addSecondsのテスト
	 * 指定した秒数が加算できることを確認する
	 */
	@isTest static void addSecondsTest() {
		Integer add = 5;
		Datetime dt = Datetime.now();
		System.assertEquals(dt.addSeconds(add), AppDatetime.valueOf(dt).addSeconds(add).getDatetime());
	}

	/**
	 * addYearsのテスト
	 * 指定した年数が加算できることを確認する
	 */
	@isTest static void addYearsTest() {
		Integer add = 6;
		Datetime dt = Datetime.now();
		System.assertEquals(dt.addYears(add), AppDatetime.valueOf(dt).addYears(add).getDatetime());
	}

	/**
	 * formatのテスト
	 * 正しい形式で取得できることを確認する
	 */
	@isTest static void formatTest() {
		// タイムゾーンはUTC
		AppDatetime dt = new AppDatetime(Datetime.newInstanceGmt(2017, 7, 26, 13, 59, 30));
		System.assertEquals('2017-07-26T13:59:30Z', dt.format());
	}

	/**
	 * formatLocalのテスト
	 * 正しい形式で取得できることを確認する
	 */
	@isTest static void formatLocalTest() {
		// タイムゾーンはローカルタイムゾーン
		AppDatetime dt = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 59, 30));
		System.assertEquals('2017-07-26 13:59:30', dt.formatLocal());
	}

	/**
	 * equalsのテスト
	 * 正しく比較できることを確認する
	 */
	@isTest static void equalsTest() {

		// 同じ日時の場合、値が等しい
		AppDatetime dt1 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 59, 30));
		AppDatetime dt2 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 59, 30));
		System.assertEquals(true, dt1.equals(dt2));

		// 日時が異なる場合、等しくない
		dt1 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 59, 30));
		dt2 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 59, 31));
		System.assertEquals(false, dt1.equals(dt2));

		// 型が異なる場合、等しくない
		System.assertEquals(false, dt1.equals(Datetime.now()));
	}

	/**
	 * compareToのテスト
	 * 正しく比較できることを確認する
	 */
	@isTest static void compareToTest() {

		// 日時が等しい場合0を返却する
		AppDatetime dt1 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 00, 00));
		AppDatetime dt2 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 00, 00));
		System.assertEquals(0, dt1.compareTo(dt2));

		// dt1よりdt2が大きい場合は1以上を返却する
		dt1 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 00, 00, 01));
		dt2 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 00, 00, 00));
		System.assert(dt1.compareTo(dt2) >= 1);

		// dt1よりdt2が小さい場合は0未満を返却する
		dt1 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 00, 00, 00));
		dt2 = new AppDatetime(Datetime.newInstance(2017, 7, 26, 00, 00, 01));
		System.assert(dt1.compareTo(dt2) < 0);
	}

	/**
	 * toStringのテスト
	 * 正しく文字列が取得できることを確認する
	 */
	@isTest static void toStringTest() {
		AppDatetime dt = new AppDatetime(Datetime.newInstance(2017, 7, 26, 13, 01, 23));
		System.assertEquals(dt.format(), dt.toString());
	}
}
