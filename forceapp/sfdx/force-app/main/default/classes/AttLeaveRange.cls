public with sharing class AttLeaveRange extends ValueObjectType {
	/** 全日 */
	public static final AttLeaveRange RANGE_DAY = new AttLeaveRange('Day');
	/** 午前半休 */
	public static final AttLeaveRange RANGE_AM = new AttLeaveRange('AM');
	/** 午後半休 */
	public static final AttLeaveRange RANGE_PM = new AttLeaveRange('PM');
	/** 時間指定半休 */
	public static final AttLeaveRange RANGE_HALF = new AttLeaveRange('Half');
	/** 時間指定 TIMEはキーワードのため、全定数にRANGE_をつける*/
	public static final AttLeaveRange RANGE_TIME = new AttLeaveRange('Time');

	/** 休暇範囲のリスト（範囲が追加されら本リストにも追加してください) */
	public static final List<AttLeaveRange> RANGE_LIST;
	static {
		RANGE_LIST = new List<AttLeaveRange> {
			RANGE_DAY,
			RANGE_AM,
			RANGE_PM,
			RANGE_HALF,
			RANGE_TIME
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttLeaveRange(String value) {
		super(value);
	}

	/**
	 * 値からAttLeaveRangeを取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttLeaveRange
	 */
	public static AttLeaveRange valueOf(String value) {
		AttLeaveRange retRange = null;
		if (String.isNotBlank(value)) {
			for (AttLeaveRange range : RANGE_LIST) {
				if (value == range.value) {
					retRange = range;
				}
			}
		}
		return retRange;
	}
	
	/**
	 * 「;」区切りする値からAttLeaveRange一覧を取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttLeaveRange
	 */
	public static List<AttLeaveRange> valuesOf(String value) {
		List<AttLeaveRange> retList = new List<AttLeaveRange>();
		if (String.isBlank(value)) {
			return retList;
		}
		for (String rangeString : value.split(';')) {
			AttLeaveRange retRange = null;
			if (String.isNotBlank(rangeString)) {
				for (AttLeaveRange range : RANGE_LIST) {
					if (rangeString == range.value) {
						retList.add(range);
						break;
					}
				}
			}
		}
		return retList;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttDayType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttLeaveRange) {
			// 値が同じであればtrue
			if (this.value == ((AttLeaveRange)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}

	/**
	 * 休暇範囲一覧が等しいかどうかを判定する
	 * @param ranges1 比較対象の休暇範囲一覧
	 * @param ranges2 比較対象の休暇範囲一覧
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	static public Boolean equals(List<AttLeaveRange> ranges1, List<AttLeaveRange> ranges2) {
		Set<String> rangeValues1 = new Set<String>();
		Set<String> rangeValues2 = new Set<String>();
		for (AttLeaveRange leaveRange : ranges1) {
			rangeValues1.add(leaveRange.value);
		}
		for (AttLeaveRange leaveRange : ranges2) {
			rangeValues2.add(leaveRange.value);
		}
		return rangeValues1.containsAll(rangeValues2) && rangeValues2.containsAll(rangeValues1);
	}
}
