/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Handler class for ComExtendedItemCustomOptionTrigger
 * 
 * @group Expense
 */
public with sharing class ExtItemCustomOptionTriggerHandler {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** CustomId as the Key and the Set of all the ExtendedItemCustomOption__c.Code__c belonging to the EICustom as Value*/
	private Map<Id, Set<String>> customIdOptionCodeSetMap;
	/** Set contains all the ExtendedItemCustomOption__c.Code__c to process */
	private Set<String> allCodeSet;
	/** Set contains all the ExtendedItemCustomOption__c.ExtendedItemCustomId__c to process */
	private Set<Id> allCustomIdSet;

	// Data From Trigger. Some of the field may be null base on the Trigger scenario.
	private List<ComExtendedItemCustomOption__c> oldList;
	private List<ComExtendedItemCustomOption__c> newList;
	private Map<Id, ComExtendedItemCustomOption__c> oldMap;
	private Map<Id, ComExtendedItemCustomOption__c> newMap;

	/**
	 * Constructor. It's designed as a general purpose constructor and depending on the trigger, some of the field may be null.
	 * */
	public ExtItemCustomOptionTriggerHandler(List<ComExtendedItemCustomOption__c> oldList, Map<Id, ComExtendedItemCustomOption__c> oldMap,
			List<ComExtendedItemCustomOption__c> newList, Map<Id, ComExtendedItemCustomOption__c> newMap) {
		this.oldMap = oldMap;
		this.newMap = newMap;
		this.oldList = oldList;
		this.newList = newList;

		customIdOptionCodeSetMap = new Map<Id, Set<String>>();
		allCodeSet = new Set<String>();
		allCustomIdSet = new Set<Id>();
	}

	/*
	 * Validate the Data for Insert case
	 */
	public void validateInsert() {
		// Check Duplicate Among
		checkDuplicateWithinImport(this.newList);

		// Build Map of Existing
		Map<Id, Set<String>> existingCustomIdOptionCodeSetMap = getExistingCustomIdOptionCodeMapForInsert();

		// Check against the codes which are already in the System
		checkDuplicateAgainstExisting(existingCustomIdOptionCodeSetMap, this.newList);
	}

	/*
	 * Validate the Data for Update case
	 */
	public void validateUpdate() {
		Set<Id> referenceCheckTargetSet = new Set<Id>();
		for (ComExtendedItemCustomOption__c extendedItemCustomOption : this.newList) {
			ComExtendedItemCustomOption__c previousCustomOption = oldMap.get(extendedItemCustomOption.Id);
			// If the parent ExtendedItemCustom is already referenced in any Transactions, then not allows to change the code or parent
			if (previousCustomOption.Code__c != extendedItemCustomOption.Code__c ||
							previousCustomOption.ExtendedItemCustomId__c != extendedItemCustomOption.ExtendedItemCustomId__c) {
				referenceCheckTargetSet.add(previousCustomOption.ExtendedItemCustomId__c);
			}
		}

		Set<Id> alreadyReferenced = new Set<Id>();
		// If there are Options which are changing parent or code, then find if they are already used in a transaction
		if (!referenceCheckTargetSet.isEmpty()) {
			alreadyReferenced = getAlreadyReferencedOptionIds(referenceCheckTargetSet);
		}

		// Check Duplicate among records
		List<ComExtendedItemCustomOption__c> possibleDuplicateList = new List<ComExtendedItemCustomOption__c>();
		Set<String> possibleDuplicateCodeSet = new Set<String>();
		Set<String> possibleDuplicateCustomSet = new Set<String>();
		for (ComExtendedItemCustomOption__c extendedItemCustomOption : this.newList) {
			ComExtendedItemCustomOption__c previousOption = oldMap.get(extendedItemCustomOption.Id);
			if (alreadyReferenced.contains(extendedItemCustomOption.ExtendedItemCustomId__c) ||
					alreadyReferenced.contains(previousOption.ExtendedItemCustomId__c)) {
				if (previousOption.ExtendedItemCustomId__c != extendedItemCustomOption.ExtendedItemCustomId__c) {
					extendedItemCustomOption.ExtendedItemCustomId__c.addError(MESSAGE.Com_Err_CannotChangeReference);
				}
				if (previousOption.Code__c != extendedItemCustomOption.Code__c) {
					extendedItemCustomOption.Code__c.addError(MESSAGE.Com_Err_CannotChangeReference);
				}
			} else {
				// If code is changed or if new parent, then need to check if new code is already in used or not.
				if (previousOption.Code__c != extendedItemCustomOption.Code__c ||
						previousOption.ExtendedItemCustomId__c != extendedItemCustomOption.ExtendedItemCustomId__c) {
					possibleDuplicateList.add(extendedItemCustomOption);
					possibleDuplicateCodeSet.add(extendedItemCustomOption.Code__c);
					possibleDuplicateCustomSet.add(extendedItemCustomOption.ExtendedItemCustomId__c);
				}
			}
		}
		checkDuplicateWithinImport(possibleDuplicateList);

		// Need to check only if there is Code changed.
		if (!possibleDuplicateList.isEmpty()) {
			// Build Map of Existing
			Map<Id, Set<String>> existingCustomIdOptionCodeSetMap =
					getExitingCustomIdOptionCodeMapForUpdate(possibleDuplicateCodeSet, possibleDuplicateCustomSet);

			// Only need to check if there are some records found
			checkDuplicateAgainstExisting(existingCustomIdOptionCodeSetMap, possibleDuplicateList);
		}
	}

	/*
	 * Validate the Data for Update case
	 */
	public void validateDelete() {
		for (ComExtendedItemCustomOption__c extendedItemCustomOption : this.oldList) {
			if (usedInReport(extendedItemCustomOption.ExtendedItemCustomId__c, extendedItemCustomOption.Code__c) ||
				usedInExpRecordItem(extendedItemCustomOption.ExtendedItemCustomId__c, extendedItemCustomOption.Code__c) ||
				usedInRequest(extendedItemCustomOption.ExtendedItemCustomId__c, extendedItemCustomOption.Code__c) ||
				usedInExpRequestRecordItem(extendedItemCustomOption.ExtendedItemCustomId__c, extendedItemCustomOption.Code__c)) {
				extendedItemCustomOption.Code__c.addError(MESSAGE.Com_Err_CannotChangeReference);
			}
		}
	}

	private static void processAggregatedResult(Set<Id> alreadyReferenced, List<AggregateResult> extendedItemCustomIdList) {
		for (AggregateResult ar : extendedItemCustomIdList) {
			for (Integer i = 1; i <= ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				String extendedItemCustomValue = (String) ar.get('extendedItemCustom' + String.valueOf(i));
				if (extendedItemCustomValue != null) {
					alreadyReferenced.add(extendedItemCustomValue);
				}
			}
		}
	}

	/*
	 * Check if the same Code is used in the records to be imported.
	 * This is intended for cases where the file user is using for data-loader contains
	 * duplicate code within same EICustom. When 2 or more rows are using the same Code (for the same EICustom),
	 * then all those rows will be marked as error.
	 */
	private void checkDuplicateWithinImport(List<ComExtendedItemCustomOption__c> newValueList) {
		/** Key: CustomID + OptionCode; Value: ComExtendedItemCustomOption__c */
		Map<String, ComExtendedItemCustomOption__c> customIdOptionCodeSObjMap = new Map<String, ComExtendedItemCustomOption__c>();
		Set<String> duplicateCustomIdOptionCode = new Set<String>();
		for (ComExtendedItemCustomOption__c extendedItemCustomOption: newValueList) {
			String customIdOptionCode = extendedItemCustomOption.ExtendedItemCustomId__c + extendedItemCustomOption.Code__c;
			ComExtendedItemCustomOption__c previouslyFound = customIdOptionCodeSObjMap.get(customIdOptionCode);
			if (previouslyFound != null) {
				// Since there is already another record found using the same EICustomID and Code, both should be error.
				previouslyFound.Code__c.addError(MESSAGE.Com_Err_DuplicateCode);
				extendedItemCustomOption.Code__c.addError(MESSAGE.Com_Err_DuplicateCode);
				duplicateCustomIdOptionCode.add(customIdOptionCode);
			} else {
				customIdOptionCodeSObjMap.put(customIdOptionCode, extendedItemCustomOption);
			}
		}

		for (ComExtendedItemCustomOption__c extendedItemCustomOption: newValueList) {
			// No need to consider the codes which will result in Error.
			String customIdOptionCode = extendedItemCustomOption.ExtendedItemCustomId__c + extendedItemCustomOption.Code__c;
			if (!duplicateCustomIdOptionCode.contains(customIdOptionCode)) {
				Set<String> codeSet = customIdOptionCodeSetMap.get(extendedItemCustomOption.ExtendedItemCustomId__c);
				allCustomIdSet.add(extendedItemCustomOption.ExtendedItemCustomId__c);
				if (codeSet == null) {
					codeSet = new Set<String>();
					customIdOptionCodeSetMap.put(extendedItemCustomOption.ExtendedItemCustomId__c, codeSet);
				}

				codeSet.add(extendedItemCustomOption.Code__c);
				allCodeSet.add(extendedItemCustomOption.Code__c);
			}
		}
	}

	/*
	 * Check if the Code is already in used in the system.
	 */
	private void checkDuplicateAgainstExisting(Map<Id, Set<String>> existingCustomIdOptionCodeSetMap, List<ComExtendedItemCustomOption__c> validationTargetList) {
		// Only need to check if there is some records found
		if (!existingCustomIdOptionCodeSetMap.keySet().isEmpty()) {
			for (ComExtendedItemCustomOption__c extendedItemCustomOption : validationTargetList) {
				Set<String> codeSet = existingCustomIdOptionCodeSetMap.get(extendedItemCustomOption.ExtendedItemCustomId__c);
				if (codeSet != null) {
					if (codeSet.contains(extendedItemCustomOption.Code__c)) {
						extendedItemCustomOption.Code__c.addError(MESSAGE.Exp_Err_DuplicateExtendedItemCustomOptionCode);
					}
				}
			}
		}
	}

	/*
	 * For Update case:
	 * Build the map with ExtendedItemCustomOption.ExtendedItemCustomId__c as the Key and the SET of all the ExtendedItemCustomOption.Code__c in the EICustom as value.
	 * When updating, if the code doesn't change, then no need to validate the code
	 *
	 * @param possibleDuplicateCodeSet Set of possible ExtendedItemCustomOption.Code__c
	 * @param possibleDuplicateEICustomSet Set of possible ExtendedItemCustomOption.ExtendedItemCustomId__c
	 */
	private Map<Id, Set<String>> getExitingCustomIdOptionCodeMapForUpdate(Set<String> possibleDuplicateCodeSet, Set<String> possibleDuplicateEICustomSet) {
		Map<Id, Set<String>> existingCustomIdOptionCodeSetMap = new Map<Id, Set<String>>();
		for (ComExtendedItemCustomOption__c existing :
		[SELECT Id, Code__c, ExtendedItemCustomId__c, UniqKey__c FROM ComExtendedItemCustomOption__c WHERE Code__c in :possibleDuplicateCodeSet AND ExtendedItemCustomId__c in :possibleDuplicateEICustomSet]) {
			populateExistingCustomIdOptionCodeSetMap(existing, existingCustomIdOptionCodeSetMap);
		}
		return existingCustomIdOptionCodeSetMap;
	}

	/*
	 * For Insert case:
	 * Build the map with ExtendedItemCustomOption.ExtendedItemCustomId__c as the Key and the SET of all the ExtendedItemCustomOption.Code__c in the EICustom as value.
	 */
	private Map<Id, Set<String>> getExistingCustomIdOptionCodeMapForInsert() {
		Map<Id, Set<String>> existingCustomIdOptionCodeSetMap = new Map<Id, Set<String>>();
		for (ComExtendedItemCustomOption__c existing :
		[SELECT Code__c, ExtendedItemCustomId__c FROM ComExtendedItemCustomOption__c WHERE Code__c in :allCodeSet AND ExtendedItemCustomId__c in :allCustomIdSet]) {
			populateExistingCustomIdOptionCodeSetMap(existing, existingCustomIdOptionCodeSetMap);
		}
		return existingCustomIdOptionCodeSetMap;
	}

	/*
	 * Populate the map with ExtendedItemCustomOption.ExtendedItemCustomId__c as the Key and the SET of all the ExtendedItemCustomOption.Code__c in the ExtendedItemCustom as value.
	 *
	 * @param existing ComExtendedItemCustomOption__c to be processed (depending on Insert or Update, the Items to process will be different)
	 * @param existingCustomIdOptionCodeSetMap the map to populate
	 */
	private void populateExistingCustomIdOptionCodeSetMap(ComExtendedItemCustomOption__c existing, Map<Id, Set<String>> existingCustomIdOptionCodeSetMap) {
		Set<String> codeSet = existingCustomIdOptionCodeSetMap.get(existing.ExtendedItemCustomId__c);
		if (codeSet == null) {
			codeSet = new Set<String>();
			existingCustomIdOptionCodeSetMap.put(existing.ExtendedItemCustomId__c, codeSet);
		}
		codeSet.add(existing.Code__c);
	}

	/*
	 * Extract all the ExtendedItemCustoms which are used in the ExpReport where the
	 * ExtendedItemCustomId is one of the ID in the {@code referenceCheckTargetSet}
	 */
	private static List<AggregateResult> extractFromExpReport(Set<Id> referenceCheckTargetSet) {
		List<AggregateResult> extendedItemCustomIdList = [
			SELECT
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c extendedItemCustom1,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c extendedItemCustom2,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c extendedItemCustom3,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c extendedItemCustom4,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c extendedItemCustom5,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c extendedItemCustom6,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c extendedItemCustom7,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c extendedItemCustom8,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c extendedItemCustom9,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c extendedItemCustom10
			FROM ExpReport__c
			WHERE
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet
			GROUP BY
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c
		];
		return extendedItemCustomIdList;
	}

	/*
	 * Extract all the ExtendedItemCustoms which are used in the ExpRequest where the
	 * ExtendedItemCustomId is one of the ID in the {@code referenceCheckTargetSet}
	 */
	private static List<AggregateResult> extractFromExpRequest(Set<Id> referenceCheckTargetSet) {
		List<AggregateResult> extendedItemCustomIdList = [
			SELECT
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c extendedItemCustom1,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c extendedItemCustom2,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c extendedItemCustom3,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c extendedItemCustom4,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c extendedItemCustom5,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c extendedItemCustom6,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c extendedItemCustom7,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c extendedItemCustom8,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c extendedItemCustom9,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c extendedItemCustom10
			FROM ExpRequest__c
			WHERE
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet
			GROUP BY
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c
		];
		return extendedItemCustomIdList;
	}

	/*
	 * Extract all the ExtendedItemCustoms which are used in the ExpRecordItems where the
	 * ExtendedItemCustomId is one of the ID in the {@code referenceCheckTargetSet}
	 */
	private static List<AggregateResult> extractFromExpRecordItem(Set<Id> referenceCheckTargetSet) {
		List<AggregateResult> extendedItemCustomIdList = [
			SELECT
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c extendedItemCustom1,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c extendedItemCustom2,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c extendedItemCustom3,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c extendedItemCustom4,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c extendedItemCustom5,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c extendedItemCustom6,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c extendedItemCustom7,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c extendedItemCustom8,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c extendedItemCustom9,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c extendedItemCustom10
			FROM ExpRecordItem__c
			WHERE
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet
			GROUP BY
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c
		];
		return extendedItemCustomIdList;
	}

	/*
	 * Extract all the ExtendedItemCustoms which are used in the ExpRequestRecordItems where the
	 * ExtendedItemCustomId is one of the ID in the {@code referenceCheckTargetSet}
	 */
	private static List<AggregateResult> extractFromExpRequestRecordItem(Set<Id> referenceCheckTargetSet) {
		List<AggregateResult> extendedItemCustomIdList = [
			SELECT
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c extendedItemCustom1,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c extendedItemCustom2,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c extendedItemCustom3,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c extendedItemCustom4,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c extendedItemCustom5,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c extendedItemCustom6,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c extendedItemCustom7,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c extendedItemCustom8,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c extendedItemCustom9,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c extendedItemCustom10
			FROM ExpRequestRecordItem__c
			WHERE
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet OR
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c in :referenceCheckTargetSet
			GROUP BY
				ExtendedItemLookup01Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup02Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup03Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup04Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup05Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup06Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup07Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup08Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup09Id__r.ExtendedItemCustomId__c,
				ExtendedItemLookup10Id__r.ExtendedItemCustomId__c
		];
		return extendedItemCustomIdList;
	}

	/*
	 * Checks if the given ExtendedItemCustomId and Code is used in any Report
	 *
	 * @param extendedItemCustomId ID of parent ExtendedItemCustom
	 * @param code the {@code code__c} field value of the ExtendedItemCustomOption
	 *
	 * @return true if it's used in any of the Report, and false otherwise
	 */
	private static boolean usedInReport(Id extendedItemCustomId, String code) {
		Integer numberOfReports = [
			SELECT COUNT()
			FROM ExpReport__c
			WHERE
				(ExtendedItemLookup01Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup01Value__c = :code) OR
				(ExtendedItemLookup02Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup02Value__c = :code) OR
				(ExtendedItemLookup03Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup03Value__c = :code) OR
				(ExtendedItemLookup04Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup04Value__c = :code) OR
				(ExtendedItemLookup05Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup05Value__c = :code) OR
				(ExtendedItemLookup06Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup06Value__c = :code) OR
				(ExtendedItemLookup07Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup07Value__c = :code) OR
				(ExtendedItemLookup08Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup08Value__c = :code) OR
				(ExtendedItemLookup09Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup09Value__c = :code) OR
				(ExtendedItemLookup10Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup10Value__c = :code)
			LIMIT 1
		];
		return (numberOfReports > 0);
	}

	/*
	 * Checks if the given ExtendedItemCustomId and Code is used in any Request (pre-application)
	 *
	 * @param extendedItemCustomId ID of parent ExtendedItemCustom
	 * @param code the {@code code__c} field value of the ExtendedItemCustomOption
	 *
	 * @return true if it's used in any of the Request, and false otherwise
	 */
	private static boolean usedInRequest(Id extendedItemCustomId, String code) {
		Integer numberOfRequests = [
			SELECT COUNT()
			FROM ExpRequest__c
			WHERE
				(ExtendedItemLookup01Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup01Value__c = :code) OR
				(ExtendedItemLookup02Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup02Value__c = :code) OR
				(ExtendedItemLookup03Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup03Value__c = :code) OR
				(ExtendedItemLookup04Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup04Value__c = :code) OR
				(ExtendedItemLookup05Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup05Value__c = :code) OR
				(ExtendedItemLookup06Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup06Value__c = :code) OR
				(ExtendedItemLookup07Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup07Value__c = :code) OR
				(ExtendedItemLookup08Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup08Value__c = :code) OR
				(ExtendedItemLookup09Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup09Value__c = :code) OR
				(ExtendedItemLookup10Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup10Value__c = :code)
			LIMIT 1
		];
		return (numberOfRequests > 0);
	}

	/*
	 * Checks if the given ExtendedItemCustomId and Code is used in any ReportRecordItem
	 *
	 * @param extendedItemCustomId ID of parent ExtendedItemCustom
	 * @param code the {@code code__c} field value of the ExtendedItemCustomOption
	 *
	 * @return true if it's used in any of the Report Record Item, and false otherwise
	 */
	private static boolean usedInExpRecordItem(Id extendedItemCustomId, String code) {
		Integer numberOfRecordItems = [
			SELECT COUNT()
			FROM ExpRecordItem__c
			WHERE
				(ExtendedItemLookup01Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup01Value__c = :code) OR
				(ExtendedItemLookup02Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup02Value__c = :code) OR
				(ExtendedItemLookup03Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup03Value__c = :code) OR
				(ExtendedItemLookup04Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup04Value__c = :code) OR
				(ExtendedItemLookup05Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup05Value__c = :code) OR
				(ExtendedItemLookup06Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup06Value__c = :code) OR
				(ExtendedItemLookup07Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup07Value__c = :code) OR
				(ExtendedItemLookup08Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup08Value__c = :code) OR
				(ExtendedItemLookup09Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup09Value__c = :code) OR
				(ExtendedItemLookup10Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup10Value__c = :code)
			LIMIT 1
		];
		return (numberOfRecordItems > 0);
	}

	/*
	 * Checks if the given ExtendedItemCustomId and Code is used in any Request Record Item
	 *
	 * @param extendedItemCustomId ID of parent ExtendedItemCustom
	 * @param code the {@code code__c} field value of the ExtendedItemCustomOption
	 *
	 * @return true if it's used in any of the Request Record Item, and false otherwise
	 */
	private static boolean usedInExpRequestRecordItem(Id extendedItemCustomId, String code) {
		Integer numberOfRequestRecordItems = [
			SELECT COUNT()
			FROM ExpRequestRecordItem__c
			WHERE
				(ExtendedItemLookup01Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup01Value__c = :code) OR
				(ExtendedItemLookup02Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup02Value__c = :code) OR
				(ExtendedItemLookup03Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup03Value__c = :code) OR
				(ExtendedItemLookup04Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup04Value__c = :code) OR
				(ExtendedItemLookup05Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup05Value__c = :code) OR
				(ExtendedItemLookup06Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup06Value__c = :code) OR
				(ExtendedItemLookup07Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup07Value__c = :code) OR
				(ExtendedItemLookup08Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup08Value__c = :code) OR
				(ExtendedItemLookup09Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup09Value__c = :code) OR
				(ExtendedItemLookup10Id__r.ExtendedItemCustomId__c = :extendedItemCustomId AND ExtendedItemLookup10Value__c = :code)
			LIMIT 1
		];
		return (numberOfRequestRecordItems > 0);
	}

	/*
	 * Find if the given Ids in the {@code referenceCheckTargetSet} are already in used in Transactions.
	 * And return the IDs of those already in used
	 */
	private static Set<Id> getAlreadyReferencedOptionIds(Set<Id> referenceCheckTargetSet) {
		Set<Id> alreadyReferenced = new Set<Id>();
		List<AggregateResult> alreadyInUsedExtendedItemCustomIdList = extractFromExpReport(referenceCheckTargetSet);
		processAggregatedResult(alreadyReferenced, alreadyInUsedExtendedItemCustomIdList);

		alreadyInUsedExtendedItemCustomIdList = extractFromExpRequest(referenceCheckTargetSet);
		processAggregatedResult(alreadyReferenced, alreadyInUsedExtendedItemCustomIdList);

		alreadyInUsedExtendedItemCustomIdList = extractFromExpRecordItem(referenceCheckTargetSet);
		processAggregatedResult(alreadyReferenced, alreadyInUsedExtendedItemCustomIdList);

		alreadyInUsedExtendedItemCustomIdList = extractFromExpRequestRecordItem(referenceCheckTargetSet);
		processAggregatedResult(alreadyReferenced, alreadyInUsedExtendedItemCustomIdList);

		return alreadyReferenced;
	}
}