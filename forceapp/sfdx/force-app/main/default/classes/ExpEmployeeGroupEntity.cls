/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Expense Employee Group Entity
 */
public with sharing class ExpEmployeeGroupEntity extends Entity {
	/** Expense Employee Group Code Max Length */
	public static final Integer CODE_MAX_LENGTH = 20;

	/** Expense Employee Group Name Max Length */
	public static final Integer NAME_MAX_LENGTH = 80;

	/** Expense Employee Group Description Max Length */
	public static final Integer DESCRIPTION_MAX_LENGTH = 1024;

	/** Expense Employee Group Field */
	public enum Field {
		ACTIVE,
		CODE,
		COMPANY_ID,
		DESCRIPTION_L0,
		DESCRIPTION_L1,
		DESCRIPTION_L2,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		UNIQ_KEY,
		REPORT_TYPE_ID_LIST
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** Changed Field Set */
	protected Set<ExpEmployeeGroupEntity.Field> isChangedFieldSet = new Set<ExpEmployeeGroupEntity.Field>();

	/** Active Boolean */
	public Boolean active {
		get;
		set {
			active = value;
			setChanged(ExpEmployeeGroupEntity.Field.ACTIVE);
		}
	}

	/** Code */
	public String code {
		get;
		set {
			code = value;
			setChanged(ExpEmployeeGroupEntity.Field.CODE);
		}
	}

	/** Company Id */
	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(ExpEmployeeGroupEntity.Field.COMPANY_ID);
		}
	}
	/** Description Language 0 */
	public String descriptionL0 {
		get;
		set {
			descriptionL0 = value;
			setChanged(ExpEmployeeGroupEntity.Field.DESCRIPTION_L0);
		}
	}

	/** Description Language 1 */
	public String descriptionL1 {
		get;
		set {
			descriptionL1 = value;
			setChanged(ExpEmployeeGroupEntity.Field.DESCRIPTION_L1);
		}
	}

	/** Description Language 2 */
	public String descriptionL2 {
		get;
		set {
			descriptionL2 = value;
			setChanged(ExpEmployeeGroupEntity.Field.DESCRIPTION_L2);
		}
	}

	/** Description Language in one AppMultiString */
	public AppMultiString descriptionL {
		get {return new AppMultiString(descriptionL0, descriptionL1, descriptionL2);}
	}

	/** Name Language 0 */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(ExpEmployeeGroupEntity.Field.NAME_L0);
		}
	}

	/** Name Language 1 */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(ExpEmployeeGroupEntity.Field.NAME_L1);
		}
	}

	/** Name Language 2 */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(ExpEmployeeGroupEntity.Field.NAME_L2);
		}
	}

	/** Name Language in one AppMultiString */
	public AppMultiString nameL {
		get {return new AppMultiString(nameL0, nameL1, nameL2);}
	}

	/** Unique Key */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(ExpEmployeeGroupEntity.Field.UNIQ_KEY);
		}
	}

	/** reportType Id List */
	public List<Id> reportTypeIdList {
		get;
		set {
			reportTypeIdList = value;
			setChanged(ExpEmployeeGroupEntity.Field.REPORT_TYPE_ID_LIST);
		}
	}

	/**
	 * Create unique key
	 * @param companyCode Code of company
	 * @return Unique key created
	 */
	public String createUniqKey(String companyId) {
		if (String.isBlank(companyId)) {
			// Only for debugging, no need to translate
			throw new App.ParameterException(
				'[Server Error]Failed to create unique key of "Expense Employee Group". companyCode is empty.');
		}
		if (String.isBlank(this.code)) {
			// Only for debugging, no need to translate
			throw new App.IllegalStateException(
				'[Server Error]Failed to create unique key of "Expense Employee Group". code is empty.');
		}

		return companyId + '-' + this.code;
	}

	/**
	 * Check field is changed
	 * @param field Expense employee group field
	 * @return true if the field have been changed
	 */
	public Boolean isChanged(ExpEmployeeGroupEntity.Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}

	/**
	 * Return the value of the specified field name
	 * @param targetField Target Field name
	 * @return Field enum value matched the specified field name
	 */
	public Object getFieldValue(String fieldName) {
		Field targetField = FIELD_MAP.get(fieldName);
		if (targetField == null) {
			throw new App.ParameterException('Unknown field : ' + fieldName);
		}
		return getFieldValue(targetField);
	}

	public Object getFieldValue(ExpEmployeeGroupEntity.Field targetField) {
		if (targetField == ExpEmployeeGroupEntity.Field.ACTIVE) {
			return this.active;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.CODE) {
			return this.code;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.DESCRIPTION_L0) {
			return this.descriptionL0;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.DESCRIPTION_L1) {
			return this.descriptionL1;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.DESCRIPTION_L2) {
			return this.descriptionL2;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.NAME_L0) {
			return this.nameL0;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.NAME_L1) {
			return this.nameL1;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.NAME_L2) {
			return this.nameL2;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.UNIQ_KEY) {
			return this.uniqKey;
		}
		if (targetField == ExpEmployeeGroupEntity.Field.REPORT_TYPE_ID_LIST) {
			return this.reportTypeIdList;
		}

		throw new App.UnsupportedException('ExpEmployeeGroupEntity.getFieldValue: Unexpected field. targetField='+ targetField);
	}

	/**
	 * Set the value to the specified field
	 * @param targetField Target field to set the value
	 * @pram value Value to be set
	 */
	public void setFieldValue(String fieldName, Object value) {
		Field targetField = FIELD_MAP.get(fieldName);
		if (targetField == null) {
			throw new App.ParameterException('Unknown field : ' + fieldName);
		}
		setFieldValue(targetField, value);
	}

	public void setFieldValue(Field targetField, Object value) {
		if (targetField == Field.ACTIVE) {
			this.active = (Boolean)value;
		} else if (targetField == Field.CODE) {
			this.code = (String)value;
		} else if (targetField == Field.COMPANY_ID) {
			this.companyId = (Id)value;
		} else if (targetField == Field.DESCRIPTION_L0) {
			this.descriptionL0 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L1) {
			this.descriptionL1 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L2) {
			this.descriptionL2 = (String)value;
		} else if (targetField == Field.NAME_L0) {
			this.nameL0 = (String)value;
		} else if (targetField == Field.NAME_L1) {
			this.nameL1 = (String)value;
		} else if (targetField == Field.NAME_L2) {
			this.nameL2 = (String)value;
		} else if (targetField == Field.UNIQ_KEY) {
			this.uniqKey = (String)value;
		} else if (targetField == Field.REPORT_TYPE_ID_LIST) {
			this.reportTypeIdList = (List<Id>)value;
		} else {
			throw new App.UnsupportedException('ExpEmployeeGroupEntity.setFieldValue: Unexpected field. targetField=' + targetField);
		}
	}

	/**
	 *  Record changed field
	 *  @param field Expense employee group field
	 */
	private void setChanged(ExpEmployeeGroupEntity.Field field) {
		this.isChangedFieldSet.add(field);
	}

}