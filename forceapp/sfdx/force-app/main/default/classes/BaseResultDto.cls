/**
 * コントローラ関数戻り値用ベースクラス
 */
public abstract with sharing class BaseResultDto {

	public ResultDto result;

	public class ResultDto {
		public String errorMessage;
		public Boolean isSuccess = true;
	}

	public BaseResultDto() {
		this.result = new ResultDto();
	}

	// エラーメッセージを格納
	public void setErrorMessage(String errorMessage) {
		this.result.isSuccess = false;
		this.result.errorMessage = errorMessage;
	}

}