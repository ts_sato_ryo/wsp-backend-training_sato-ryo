/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠日次申請のリポジトリ
 */
public with sharing class AttDailyRequestRepository extends Repository.BaseRepository {
	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_LIMIT_MAX = 10000;

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_REQUEST_FIELD_SET;
	static {
		GET_REQUEST_FIELD_SET = new Set<Schema.SObjectField> {
				AttDailyRequest__c.Id,
				AttDailyRequest__c.ParentRequestId__c,
				AttDailyRequest__c.Name,
				AttDailyRequest__c.ProcessComment__c,
				AttDailyRequest__c.OwnerId,
				AttDailyRequest__c.RequestType__c,
				AttDailyRequest__c.RequestTime__c,
				AttDailyRequest__c.Approver01Id__c,
				AttDailyRequest__c.AttLeaveId__c,
				AttDailyRequest__c.AttLeaveType__c,
				AttDailyRequest__c.AttLeaveRange__c,
				AttDailyRequest__c.SubstituteLeaveDayType__c,
				AttDailyRequest__c.Reason__c,
				AttDailyRequest__c.Remarks__c,
				AttDailyRequest__c.StartDate__c,
				AttDailyRequest__c.EndDate__c,
				AttDailyRequest__c.ExcludingDate__c,
				AttDailyRequest__c.LeaveTotalTime__c,
				AttDailyRequest__c.LeaveDays__c,
				AttDailyRequest__c.StartTime__c,
				AttDailyRequest__c.EndTime__c,
				AttDailyRequest__c.Rest1StartTime__c,
				AttDailyRequest__c.Rest1EndTime__c,
				AttDailyRequest__c.Rest2StartTime__c,
				AttDailyRequest__c.Rest2EndTime__c,
				AttDailyRequest__c.Rest3StartTime__c,
				AttDailyRequest__c.Rest3EndTime__c,
				AttDailyRequest__c.Rest4StartTime__c,
				AttDailyRequest__c.Rest4EndTime__c,
				AttDailyRequest__c.Rest5StartTime__c,
				AttDailyRequest__c.Rest5EndTime__c,
				AttDailyRequest__c.AllocatedSubstituteLeaveRange__c,
				AttDailyRequest__c.AllocatedSubstituteLeaveType__c,
				AttDailyRequest__c.SubstituteLeave1Date__c,
				AttDailyRequest__c.SubstituteLeave1Range__c,
				AttDailyRequest__c.AttPatternId__c,
				AttDailyRequest__c.Status__c,
				AttDailyRequest__c.ConfirmationRequired__c,
				AttDailyRequest__c.CancelType__c,
				AttDailyRequest__c.EmployeeHistoryId__c,
				AttDailyRequest__c.DepartmentHistoryId__c,
				AttDailyRequest__c.ActorHistoryId__c,
				AttDailyRequest__c.LastApproveTime__c,
				AttDailyRequest__c.LastApproverId__c,
				AttDailyRequest__c.OriginalRequestId__c,
				AttDailyRequest__c.LastModifiedById
		};
	}
	/** 休暇のリレーション名 */
	private static final String LEAVE_R = AttDailyRequest__c.AttLeaveId__c.getDescribe().getRelationshipName();
	/** 社員のリレーション名 */
	private static final String EMPLOYEE_HISTRORY_R = AttDailyRequest__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	/** 申請者のリレーション名 */
	private static final String ACTOR_HISTORY_R = AttDailyRequest__c.ActorHistoryId__c.getDescribe().getRelationshipName();
	/** 部署のリレーション名*/
	private static final String DEPARTMENT_HISTORY_R = AttDailyRequest__c.DepartmentHistoryId__c.getDescribe().getRelationshipName();
	/** 社員履歴: ベースID項目のリレーション名 */
	private static final String EMPLOYEE_BASE_R = ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/** 社員ベース: ユーザのリレーション名 */
	private static final String USER_R = ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();
	/** 承認内容変更申請元申請ののリレーション名 */
	private static final String ORIGINAL_REQUEST_R = AttDailyRequest__c.OriginalRequestId__c.getDescribe().getRelationshipName();

	/** UserRecordAccessの取得対象項目 */
	private static final List<String> GET_USER_RECORD_ACCESS_FIELD_NAME_LIST;
	/** UserRecordAccessの外部キー(リレーション名は取得できない) */
	private static final String USER_RECORD_ACCESS_R = 'UserRecordAccess';
	static {

		// 取得対象の項目定義(UserRecordAccess)
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			UserRecordAccess.HasEditAccess};
		GET_USER_RECORD_ACCESS_FIELD_NAME_LIST = Repository.generateFieldNameList(USER_RECORD_ACCESS_R, fieldList);
	}

	/** 検索フィルタ */
	public class RequestFilter {
		public Set<Id> requestIds;
		public Set<Id> parentRequestIds;
		public Set<Id> originalRequestIds;
		public Boolean isAdmitted;
		public Boolean isRequesting;
		public Boolean isReapplying;
		public Id employeeId;
		public AppDate startDate;
		public AppDate endDate;
		public Boolean isDuration;
		public String requestType;
		public Set<Id> leaveIds;
	}

	/**
	 * 指定Idの日次休暇申請一覧を取得する
	 * @param requestId 指定する申請Id
	 * @return 日次休暇申請、存在しない場合はnull
	 */
	public AttDailyRequestEntity getLeaveById(Id requestId) {
		RequestFilter filter = new RequestFilter();
		filter.requestIds = new Set<Id> {requestId};

		List<AttDailyRequestEntity> entities = searchEntityList(filter, false);
		AttDailyRequestEntity retEntity;
		if (! entities.isEmpty()) {
			retEntity = entities[0];
		} else {
			retEntity = null;
		}
		return retEntity;
	}

	/**
	 * 指定Idの日次休暇申請一覧を取得する
	 * @param requestIds 指定する申請Id
	 * @return 日次休暇申請一覧(申請日時順)
	 */
	public List<AttDailyRequestEntity> getLeaveListByIds(Set<Id> requestIds) {
		RequestFilter filter = new RequestFilter();
		filter.requestIds = requestIds;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定期間の日次休暇申請一覧を取得する
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @param isAdmitted 承認済みの場合はtrue
	 * @return 日次休暇申請一覧(申請日時順)
	 */
	public List<AttDailyRequestEntity> getLeaveList(Id employeeId, AppDate startDate, AppDate endDate, Boolean isAdmitted) {
		RequestFilter filter = new RequestFilter();
		filter.isAdmitted = isAdmitted;
		filter.employeeId = employeeId;
		filter.startDate = startDate;
		filter.endDate = endDate;
		filter.isDuration = true;
		filter.requestType = AttRequestType.LEAVE.value;

		return searchEntityList(filter, false);
	}
	/**
	 * 指定休暇の日次休暇申請一覧を取得する
	 * @param leaveIds 対象休暇
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @param isRequesting 申請中の場合はtrue
	 * @return 日次休暇申請一覧(申請日時順)
	 */
	 // TODO リポジトリ内の類似したメソッドの引数に「申請中」「承認済み」のフラグが混在している。 バグが生まれる危険性が高いのでどちらかに統一する
	public List<AttDailyRequestEntity> getLeaveList(Id employeeId, Set<Id> leaveIds, AppDate startDate, AppDate endDate, Boolean isRequesting) {
		RequestFilter filter = new RequestFilter();
		filter.isRequesting = isRequesting;
		filter.employeeId = employeeId;
		filter.leaveIds = leaveIds;
		filter.startDate = startDate;
		filter.endDate = endDate;
		filter.requestType = AttRequestType.LEAVE.value;

		return searchEntityList(filter, false);
	}
	/**
	 * 指定した休暇IDで、変更承認待ちの日次休暇申請一覧を取得する
	 * @param employeeId 対象の社員ID
	 * @param leaveIds 対象休暇
	 * @return 変更承認待ちの日次休暇申請一覧(申請日時順)
	 */
	public List<AttDailyRequestEntity> getReapplyingLeaveList(Id employeeId, Set<Id> leaveIds) {
		RequestFilter filter = new RequestFilter();
		filter.isReapplying = true;
		filter.employeeId = employeeId;
		filter.leaveIds = leaveIds;
		filter.requestType = AttRequestType.LEAVE.value;

		return searchEntityList(filter, false);
	}
	/**
	 * 指定した申請IDの申請を取得する
	 * @param 取得対象の申請ID
	 * @return 申請、ただし見つからなかった場合はnull
	 */
	public AttDailyRequestEntity getEntity(Id requestId) {
		List<AttDailyRequestEntity> entities = getEntityListByIds(new Set<Id>{requestId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定Idの申請一覧を取得する
	 * @param requestIds 指定する申請Id
	 * @return 日次申請一覧(申請日時順)
	 */
	public List<AttDailyRequestEntity> getEntityListByIds(Set<Id> requestIds) {
		RequestFilter filter = new RequestFilter();
		filter.requestIds = requestIds;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定した申請IDを親にもつ申請を取得する
	 * @param 取得対象の申請ID
	 * @return 申請、ただし見つからなかった場合はnull
	 */
	public AttDailyRequestEntity getEntityByParentId(Id requestId) {
		List<AttDailyRequestEntity> entities = getEntityListByParentIds(new Set<Id>{requestId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定Idを親にもつ申請一覧を取得する
	 * @param requestIds 指定する請Id
	 * @return 日次申請一覧(申請日時順)
	 */
	public List<AttDailyRequestEntity> getEntityListByParentIds(Set<Id> requestIds) {
		RequestFilter filter = new RequestFilter();
		filter.parentRequestIds = requestIds;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定IDをオリジナルIDでもつ申請を取得する
	 * @param 取得対象の申請ID
	 * @return 申請、ただし見つからなかった場合はnull
	 */
	public AttDailyRequestEntity getEntityByOriginalId(Id originalId) {
		List<AttDailyRequestEntity> entities = getEntityListByOriginalIds(new Set<Id>{originalId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定IDをオリジナルIDでもつ申請一覧を取得する
	 * @param 取得対象の申請IDのリスト
	 * @return 日次申請一覧（申請日付順）
	 */
	public List<AttDailyRequestEntity> getEntityListByOriginalIds(Set<Id> originalIds) {
		RequestFilter filter = new RequestFilter();
		filter.originalRequestIds = originalIds;

		return searchEntityList(filter, false);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AttDailyRequestEntity entity) {
		return saveEntityList(new List<AttDailyRequestEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AttDailyRequestEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public SaveResult saveEntityList(List<AttDailyRequestEntity> entityList, Boolean allOrNone) {

		List<AttDailyRequest__c> dailyRequestList = new List<AttDailyRequest__c>();

		for (AttDailyRequestEntity entity : entityList) {

			// エンティティからSObjectを作成する
			AttDailyRequest__c dailyRequest = createObj(entity);
			dailyRequestList.add(dailyRequest);
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(dailyRequestList, allOrNone);

		// 結果作成
		Repository.SaveResult result = resultFactory.createSaveResult(recResList);

		// 保存したIdを設定する
		for (Integer i = 0; i < entityList.size(); i++) {
			entityList[i].setId(result.details[i].id);
		}
		return result;
	}

	/**
	 * 日次申請を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した勤怠明細一覧(日付順)
	 */
	public List<AttDailyRequestEntity> searchEntityList(
			RequestFilter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_REQUEST_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// 休暇コード
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Code__c));
		// 休暇名
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L0__c));
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L1__c));
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L2__c));
		// 休暇-理由を求める
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.RequireReason__c));
		// 社員
		selectFldList.addAll(getSelectFldListEmployee(EMPLOYEE_HISTRORY_R));
		// 申請者
		selectFldList.addAll(getSelectFldListEmployee(ACTOR_HISTORY_R));
		// 部署
		selectFldList.addAll(getSelectFldListDepartment(DEPARTMENT_HISTORY_R));
		// 承認内容変更申請元申請
		selectFldList.addAll(getSelectFldListOriginalRequest(ORIGINAL_REQUEST_R));
		// UserRecordAccess
		selectFldList.addAll(GET_USER_RECORD_ACCESS_FIELD_NAME_LIST);

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Set<Id> requestIds = filter.requestIds;
		final Set<Id> parentRequestIds = filter.parentRequestIds;
		final Set<Id> originalRequestIds = filter.originalRequestIds;
		final Boolean isAdmitted = filter.isAdmitted;
		final Boolean isRequesting = filter.isRequesting;
		final Boolean isReapplying = filter.isReapplying;
		final Id employeeId = filter.employeeId;
		final Date startDate = AppDate.convertDate(filter.startDate);
		final Date endDate = AppDate.convertDate(filter.endDate);
		final Boolean isDuration = filter.isDuration;
		final String requestType = filter.requestType;
		final Set<Id> leaveIds = filter.leaveIds;
		String status;
		if (requestIds != null) {
			condList.add(getFieldName(AttDailyRequest__c.Id) + ' IN :requestIds');
		}
		if (parentRequestIds != null) {
			condList.add(getFieldName(AttDailyRequest__c.ParentRequestId__c) + ' IN :parentRequestIds');
		}
		if (originalRequestIds != null) {
			condList.add(getFieldName(AttDailyRequest__c.OriginalRequestId__c) + ' IN :originalRequestIds');
		}
		if (filter.isAdmitted != null && filter.isAdmitted) {
			status = AppRequestStatus.APPROVED.value;
			condList.add(getFieldName(AttDailyRequest__c.Status__c) + ' = :status');
		}
		else if (filter.isRequesting != null && filter.isRequesting) {
			status = AppRequestStatus.PENDING.value;
			condList.add(getFieldName(AttDailyRequest__c.Status__c) + ' = :status');
		}
		else if (filter.isReapplying != null && filter.isReapplying) {
			status = AppRequestStatus.REAPPLYING.value;
			condList.add(getFieldName(AttDailyRequest__c.Status__c) + ' = :status');
		}
		if (filter.employeeId != null) {
			condList.add(EMPLOYEE_HISTRORY_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c) + ' = :employeeId');
		}
		if (filter.isDuration != null && filter.isDuration) {
			condList.add(getFieldName(AttDailyRequest__c.EndDate__c) + ' >= :startDate');
			condList.add(getFieldName(AttDailyRequest__c.StartDate__c) + ' <= :endDate');
		} else {
			if (filter.startDate != null) {
				condList.add(getFieldName(AttDailyRequest__c.StartDate__c) + ' >= :startDate');
			}
			if (filter.endDate != null) {
				condList.add(getFieldName(AttDailyRequest__c.EndDate__c) + ' <= :endDate');
			}
		}
		if (filter.requestType != null) {
			condList.add(getFieldName(AttDailyRequest__c.RequestType__c) + ' = :requestType');
		}
		if (filter.leaveIds != null) {
			condList.add(getFieldName(AttDailyRequest__c.AttLeaveId__c) + ' IN :leaveIds');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + AttDailyRequest__c.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond
				+ ' ORDER BY ' + getFieldName(AttDailyRequest__c.RequestTime__c) + ' Asc'
				+ ' LIMIT :SEARCH_RECORDS_LIMIT_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// クエリ実行
		System.debug('AttDailyRequestRepository.searchEntityList: SOQL==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttDailyRequest__c> sObjList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttDailyRequest__c.SObjectType);

		// 結果からエンティティを作成する
		List<AttDailyRequestEntity> entityList = new List<AttDailyRequestEntity>();
		for (AttDailyRequest__c attDailyRequest : sObjList) {
			entityList.add(createEntity(attDailyRequest));
		}

		return entityList;
	}


	/**
	 * 社員オブジェクトのSELECT句の取得項目を作成する
	 * @param 社員項目のリレーションシップ名
	 * @return 社員オブジェクトから取得する項目一覧
	 */
	private List<String> getSelectFldListEmployee(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.FirstName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.FirstName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.FirstName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.LastName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.LastName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.LastName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, USER_R, User.SmallPhotoUrl));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidFrom__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidTo__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.Removed__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.BaseId__c));
		return selectFldList;
	}

	/**
	 * 部署オブジェクトのSELECT句の取得項目を作成する
	 * @param 社員項目のリレーションシップ名
	 * @return 社員オブジェクトから取得する項目一覧
	 */
	private List<String> getSelectFldListDepartment(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L0__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L1__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L2__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.ValidFrom__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.ValidTo__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Removed__c));
		return selectFldList;
	}

	/**
	 * 承認内容変更申請元申請のSELECT句の取得項目を作成する
	 * @param 承認内容変更申請元申請のリレーションシップ名
	 * @return 各種勤怠申請オブジェクトから取得する、承認内容変更申請元申請の項目一覧
	 */
	private List<String> getSelectFldListOriginalRequest(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, AttDailyRequest__c.Status__c));
		selectFldList.add(getFieldName(relationshipName, AttDailyRequest__c.CancelType__c));
		return selectFldList;
	}

	@testVisible
	private AttDailyRequest__c createObj(AttDailyRequestEntity entity) {
		return entity.createSObject();
	}

	@testVisible
	private AttDailyRequestEntity createEntity(AttDailyRequest__c attDailyRequest) {
		return new AttDailyRequestEntity(attDailyRequest);
	}

	/** excludingDateの区切り文字 */
	private static final String EXCLUDINGDATE_SEPARATOR = ',';

	/**
	 * excludingDateListをDB保存用の文字列に変換する(YYYYMMDDのカンマ区切り)
	 */
	public String convertExcludingDateListToString(List<AppDate> excludingDateList) {
		if (excludingDateList == null) {
			return null;
		}

		List<String> strDateList = new List<String>();
		for (AppDate dt : excludingDateList) {
			strDateList.add(dt.formatYYYYMMDD());
		}
		return String.join(strDateList, EXCLUDINGDATE_SEPARATOR);
	}

	/**
	 * excludingDate__cの値を日付リストに変換する
	 */
	public List<AppDate> convertExcludingDateListToList(String excludingDate) {
		if (excludingDate == null) {
			return null;
		}
		List<String> strDateList = excludingDate.split(EXCLUDINGDATE_SEPARATOR);
		List<AppDate> dateList = new List<AppDate>();
		for (String strDate : strDateList) {
			// TODO AppDateのメソッドに(YYYYMMDD->AppDate)
			Integer year = Integer.valueOf(strDate.subString(0, 4));
			Integer month = Integer.valueOf(strDate.subString(4, 6));
			Integer day = Integer.valueOf(strDate.subString(6, 8));
			dateList.add(AppDate.newInstance(year, month, day));
		}

		return dateList;
	}
}