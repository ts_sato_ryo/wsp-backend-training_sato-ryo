/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * コンテンツバージョンのリポジトリクラス
 */
public with sharing class AppContentVersionRepository extends Repository.BaseRepository {

	/** Freeze sharing on */
	@TestVisible
	private static final String SHARING_OPTION_FREEZE = 'R';

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
			ContentVersion.Id,
			ContentVersion.ContentDocumentId,
			ContentVersion.Title,
			ContentVersion.FileType,
			ContentVersion.PathOnClient,
			ContentVersion.VersionData,
			ContentVersion.CreatedDate
		};
	}

	/**
	 * コンテンツバージョンを保存する
	 * @param entity 保存対象のコンテンツバージョン
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AppContentVersionEntity entity) {
		return saveEntityList(new List<AppContentVersionEntity>{entity});
	}

	/** すべてのレコードを保存する(1レコードでも失敗したらロールバック) */
	private static final Boolean IS_ALL_SAVE = true;
	/**
	 * コンテンツバージョンを保存する
	 * @param entityList 保存対象のコンテンツバージョン
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AppContentVersionEntity> entityList) {
		List<ContentVersion> saveObjList = new List<ContentVersion>();

		// エンティティからSObjectを作成する
		for (AppContentVersionEntity entity : entityList) {
			saveObjList.add(createObj(entity));
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(saveObjList, IS_ALL_SAVE);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * コンテンツバージョンを取得する
	 * @param contentVersionId コンテンツバージョンID
	 * @return コンテンツバージョンエンティティ
	 */
	public AppContentVersionEntity getEntity(Id contentVersionId) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new List<Id>{contentVersionId};
		List<AppContentVersionEntity> entityList = searchEntityList(filter);

		AppContentVersionEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * 検索条件
	 */
	private class SearchFilter {
		/** コンテンツバージョンID */
		List<Id> ids;
	}

	/**
	 * コンテンツバージョンを検索する
	 * @param filter 検索条件
	 * @return 条件に一致したコンテンツバージョン一覧
	 */
	private List<AppContentVersionEntity> searchEntityList(SearchFilter filter) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> ids = filter.ids;
		if (ids != null) {
			condList.add(getFieldName(ContentVersion.Id) + ' IN :ids');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ContentVersion.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond
				+ ' ORDER BY ' + getFieldName(ContentVersion.Id) + ' ASC';

		// クエリ実行
		System.debug('AppContentVersionRepository.searchEntityList: SOQL ==>' + soql);
		List<ContentVersion> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<AppContentVersionEntity> entityList = new List<AppContentVersionEntity>();
		for (ContentVersion contentVersion : sObjList) {
			entityList.add(createEntity(contentVersion));
		}

		return entityList;
	}

	/**
	 * ContentVersionからAppContentVersionEntityを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AppContentVersionEntity createEntity(ContentVersion sObj) {
		AppContentVersionEntity entity = new AppContentVersionEntity();
		entity.setId(sObj.Id);
		entity.contentDocumentId = sObj.ContentDocumentId;
		entity.title = sObj.Title;
		entity.fileType = sObj.FileType;
		entity.pathOnClient = sObj.PathOnClient;
		entity.versionData = sObj.VersionData;
		entity.createdDate = sObj.CreatedDate;

		entity.resetChanged();
		return entity;
	}

	/**
	 * エンティティからSObjectを作成する
	 * @param entity 作成元のエンティティ
	 * @return SObject
	 */
	private ContentVersion createObj(AppContentVersionEntity entity) {
		ContentVersion sObj = new ContentVersion();

		sObj.Id = entity.id;
		if (entity.isChanged(AppContentVersionEntity.Field.TITLE)) {
			sObj.Title = entity.title;
		}
		if (entity.isChanged(AppContentVersionEntity.Field.PATH_ON_CLIENT)) {
			sObj.PathOnClient = entity.pathOnClient;
		}
		if (entity.isChanged(AppContentVersionEntity.Field.VERSION_DATA)) {
			sObj.VersionData = entity.versionData;
		}
		sObj.SharingOption = AppContentVersionRepository.SHARING_OPTION_FREEZE;

		return sObj;
	}
}