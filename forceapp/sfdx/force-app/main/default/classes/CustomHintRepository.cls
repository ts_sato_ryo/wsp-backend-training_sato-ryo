/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description Custom Hint Repository
 */
public with sharing class CustomHintRepository extends Repository.BaseRepository {

	/** search filters */
	public class SearchFilter {
		/** Company ID */
		public String companyId;
		/** Module Type */
		public String moduleType;
	}

	/** fields to be retrieved */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
			ComCustomHint__c.id,
			ComCustomHint__c.CompanyId__c,
			ComCustomHint__c.ModuleType__c,
			ComCustomHint__c.Field__c,
			ComCustomHint__c.Description_L0__c,
			ComCustomHint__c.Description_L1__c,
			ComCustomHint__c.Description_L2__c
		};
	}

	/**
	 * @description search custom hints using given filters
	 * @param filter Search filters
	 * @return Custom Hint List
	 */
	private List<CustomHintEntity> searchEntityList(CustomHintRepository.SearchFilter filter) {
		// create SOQL
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		
		// create condition list of WHERE clause from filter condition
		List<String> condList = new List<String>();
		final String companyId = filter.companyId;
		final String moduleType = filter.moduleType;

		if (String.isNotBlank(companyId)) {
			condList.add(getFieldName(ComCustomHint__c.CompanyId__c) + ' = :companyId');
		}
		if (String.isNotBlank(moduleType)) {
			condList.add(getFieldName(ComCustomHint__c.ModuleType__c) + ' = :moduleType');
		}
		
		String soql = 'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComCustomHint__c.SObjectType.getDescribe().getName()
				+ buildWhereString(condList);
		
		// Query execution
		System.debug('CustomHintRepository.searchEntityList: SOQL ==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComCustomHint__c> sObjList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComCustomHint__c.SObjectType);

		// Create entity from the result
		List<CustomHintEntity> entityList = new List<CustomHintEntity>();
		for (ComCustomHint__c sObj : sObjList) {
			entityList.add(createEntity(sObj));
		}
		return entityList;
	}

	/**
	 * @description save custom hints
	 * @return Save Result
	 */
	public void saveCustomHintEntityList(List<CustomHintEntity> entityList) {
		List<ComCustomHint__c> sObjList = createSObjectList(entityList);
		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);
	}

	/**
	 * @description get custom hints
	 * @return entityList list of custom hints
	 */
	public List<CustomHintEntity> getCustomHintEntityList(String companyId, String moduleType) {
		CustomHintRepository.SearchFilter filter = new SearchFilter();
		filter.companyId = companyId;
		filter.moduleType = moduleType;

		return searchEntityList(filter);
	}

	/**
	 * @description convert Custom Hint entity list to SObject list
	 * @return Custom Hint SObject list
	 */
	private List<ComCustomHint__c> createSObjectList(List<CustomHintEntity> entityList) {
		List<ComCustomHint__c> sObjectList = new List<ComCustomHint__c>();

		for (CustomHintEntity entity:entityList) {
			sObjectList.add(createSObject(entity));
		}

		return sObjectList;
	}

	/**
	 * @description convert Custom Hint entity to SObject
	 * @return Custom Hint SObject
	 */
	@TestVisible
	private ComCustomHint__c createSObject(CustomHintEntity comCustomHint) {
		ComCustomHint__c obj = new ComCustomHint__c();

		obj.Id = comCustomHint.id;
		if (comCustomHint.isChanged(CustomHintEntity.Field.MODULE_TYPE)) {
			obj.ModuleType__c = comCustomHint.moduleType.name();
		}
		if (comCustomHint.isChanged(CustomHintEntity.Field.FIELD_NAME)) {
			obj.Field__c = comCustomHint.fieldName;
		}
		if (comCustomHint.isChanged(CustomHintEntity.Field.COMPANY_ID)) {
			obj.CompanyId__c = comCustomHint.companyId;
		}
		if (comCustomHint.isChanged(CustomHintEntity.Field.DESCRIPTION_L0)) {
			obj.Description_L0__c = comCustomHint.description_L0;
		}
		if (comCustomHint.isChanged(CustomHintEntity.Field.DESCRIPTION_L1)) {
			obj.Description_L1__c = comCustomHint.description_L1;
		}
		if (comCustomHint.isChanged(CustomHintEntity.Field.DESCRIPTION_L2)) {
			obj.Description_L2__c = comCustomHint.description_L2;
		}
		return obj;
	}

	@TestVisible
	private CustomHintEntity createEntity(ComCustomHint__c obj) {
		CustomHintEntity entity = new CustomHintEntity();
		entity.id = obj.id;
		entity.companyId = obj.CompanyId__c;
		entity.moduleType = CustomHintEntity.MODULE_TYPE_MAP.get(obj.ModuleType__c);
		entity.fieldName = obj.Field__c;
		entity.description_L0 = obj.Description_L0__c;
		entity.description_L1 = obj.Description_L1__c;
		entity.description_L2 = obj.Description_L2__c;
		return entity;
	}
}