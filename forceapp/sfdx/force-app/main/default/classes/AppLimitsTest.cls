/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group App
 *
 * @description Test for AppLimits class
 */
@isTest
private class AppLimitsTest {

	/**
	 * Test for incrementQueryCount
	 * Verify that you can count the number of queries for the specified SObject
	 */
	@isTest static void incrementQueryCountTestSObjectArgument() {

		AppLimits.getInstance().setCurrentQueryCount();
		String soql = 'SELECT Id FROM ComEmpBase__c';
		List<ComEmpBase__c> sObjs = (List<ComEmpBase__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComEmpBase__c.SObjectType);
		// Verify
		String objectName = ComEmpBase__c.SObjectType.getDescribe().getName();
		System.assertEquals(1, AppLimits.getInstance().queryCountMap.get(objectName));
	}

	/**
	 * Test for incrementQueryCount
	 * Verify that the count is increased by 2 if there is a subquery
	 */
	@isTest static void incrementQueryCountTestSubQuery() {

		AttTestData testData = new AttTestData();
		AppLimits.getInstance().setCurrentQueryCount();
		String soql = 'SELECT Id, (SELECT Id FROM Histories__r) FROM ComEmpBase__c';
		List<ComEmpBase__c> sObjs = (List<ComEmpBase__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComEmpBase__c.SObjectType);
		// Verify
		String objectName = ComEmpBase__c.SObjectType.getDescribe().getName();
		System.assertEquals(2, AppLimits.getInstance().queryCountMap.get(objectName));
	}
}
