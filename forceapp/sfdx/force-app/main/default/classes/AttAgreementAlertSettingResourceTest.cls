/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * @description AttAgreementAlertSettingResourceのテストクラス
 */
@isTest
private class AttAgreementAlertSettingResourceTest {

	/**
	 * テストデータクラス
	 */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * 残業設定オブジェクトを作成する
		 */
		public List<AttAgreementAlertSettingEntity> createAgreementAlertSettings(String name, Integer size) {
			return createAgreementAlertSettings(name, this.company.id, size);
		}

		/**
		 * 残業設定オブジェクトを作成する
		 */
		public List<AttAgreementAlertSettingEntity> createAgreementAlertSettings(
				String name, Id companyId, Integer size) {
			ComTestDataUtility.createAgreementAlertSettings(name, companyId, size);
			return (new AttAgreementAlertSettingRepository()).getEntityList(null);
		}
	}

	/**
	 * 新規作成APIのテスト
	 * 全ての項目が保存できることを確認する
	 */
	@isTest static void createTestAllField() {

		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();

		AttAgreementAlertSettingResource.AgreementAlertSettingParam param =
				new AttAgreementAlertSettingResource.AgreementAlertSettingParam();
		final String name = '新規作成テスト';
		param.name_L0 = name + '_L0';
		param.name_L1 = name + '_L1';
		param.name_L2 = name + '_L2';
		param.code = name;
		param.companyId = testData.company.id;
		param.validDateFrom = Date.today().addDays(1);
		param.validDateTo = param.validDateFrom.addMonths(12);
		param.monthlyAgreementHourWarning1 = 30;
		param.monthlyAgreementHourWarning2 = 40;
		param.monthlyAgreementHourLimit = 45;
		param.monthlyAgreementHourWarningSpecial1 = 50;
		param.monthlyAgreementHourWarningSpecial2 = 55;
		param.monthlyAgreementHourLimitSpecial = 60;

		// API実行
		AttAgreementAlertSettingResource.CreateApi api = new AttAgreementAlertSettingResource.CreateApi();
		AttAgreementAlertSettingResource.SaveResponse res = (AttAgreementAlertSettingResource.SaveResponse)api.execute(param);

		// 確認
		AttAgreementAlertSettingEntity resSetting = (new AttAgreementAlertSettingRepository()).getEntity(res.id);
		System.assertNotEquals(null, resSetting);
		assertSaveAllField(param, resSetting);

	}

	/**
	 * 新規作成APIのテスト
	 * 権限のない管理者が残業警告設定を新規作成する場合、アプリ例外が発生することを確認する
	 */
	@isTest static void createTestNoPermission() {

		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 残業警告設定の管理権限を無効に設定する
		testData.permission.isManageAttAgreementAlertSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		AttAgreementAlertSettingResource.AgreementAlertSettingParam param =
				new AttAgreementAlertSettingResource.AgreementAlertSettingParam();
		final String name = '新規作成テスト';
		param.name_L0 = name + '_L0';
		param.name_L1 = name + '_L1';
		param.name_L2 = name + '_L2';
		param.code = name;
		param.companyId = testData.company.id;
		param.validDateFrom = Date.today().addDays(1);
		param.validDateTo = param.validDateFrom.addMonths(12);
		param.monthlyAgreementHourWarning1 = 30;
		param.monthlyAgreementHourWarning2 = 40;
		param.monthlyAgreementHourLimit = 45;
		param.monthlyAgreementHourWarningSpecial1 = 50;
		param.monthlyAgreementHourWarningSpecial2 = 55;
		param.monthlyAgreementHourLimitSpecial = 60;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttAgreementAlertSettingResource.CreateApi api = new AttAgreementAlertSettingResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());

	}

	/**
	 * 新規作成APIのテスト
	 * リクエストパラメータのデフォルト値が適用されることを確認する
	 */
	@isTest static void createTestDefaultValue() {

		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();

		AttAgreementAlertSettingResource.AgreementAlertSettingParam param =
				new AttAgreementAlertSettingResource.AgreementAlertSettingParam();
		final String name = '新規作成テスト';
		param.name_L0 = name + '_L0';
		param.name_L1 = name + '_L1';
		param.name_L2 = name + '_L2';
		param.code = name;
		param.companyId = testData.company.id;
		param.monthlyAgreementHourWarning1 = 30;
		param.monthlyAgreementHourWarning2 = 40;
		param.monthlyAgreementHourLimit = 45;
		param.monthlyAgreementHourWarningSpecial1 = 50;
		param.monthlyAgreementHourWarningSpecial2 = 55;
		param.monthlyAgreementHourLimitSpecial = 60;
		// 下記がデフォルト値が設定される項目
		param.validDateFrom = null;
		param.validDateTo = null;

		// API実行
		AttAgreementAlertSettingResource.CreateApi api = new AttAgreementAlertSettingResource.CreateApi();
		AttAgreementAlertSettingResource.SaveResponse res = (AttAgreementAlertSettingResource.SaveResponse)api.execute(param);

		// 確認
		AttAgreementAlertSettingEntity resSetting = (new AttAgreementAlertSettingRepository()).getEntity(res.id);
		System.assertNotEquals(null, resSetting);
		System.assertEquals(AppDate.today(), resSetting.validFrom);
		System.assertEquals(ValidPeriodEntity.VALID_TO_MAX, resSetting.validTo);

	}

	/**
	 * 更新APIのテスト
	 * 全ての項目が更新できることを確認する
	 */
	@isTest static void updateTestAllField() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('更新テスト', testSettingSize).get(0);

		AttAgreementAlertSettingResource.AgreementAlertSettingParam param =
				new AttAgreementAlertSettingResource.AgreementAlertSettingParam();
		param.id = targetSetting.id;
		param.name_L0 = targetSetting.nameL0 + 'Update';
		param.name_L1 = targetSetting.nameL1 + 'Update';
		param.name_L2 = targetSetting.nameL2 + 'Update';
		param.code = targetSetting.id;
		param.companyId = testData.company.id;
		param.validDateFrom = targetSetting.validFrom.getDate().addDays(10);
		param.validDateTo = targetSetting.validTo.getDate().addDays(10);
		param.monthlyAgreementHourWarning1 = (targetSetting.monthlyAgreementHourWarning1 - 60) / 60;
		param.monthlyAgreementHourWarning2 = (targetSetting.monthlyAgreementHourWarning2 - 60) / 60;
		param.monthlyAgreementHourLimit = (targetSetting.monthlyAgreementHourLimit - 60) / 60;
		param.monthlyAgreementHourWarningSpecial1 = (targetSetting.monthlyAgreementHourWarningSpecial1 - 60) / 60;
		param.monthlyAgreementHourWarningSpecial2 = (targetSetting.monthlyAgreementHourWarningSpecial2 - 60) / 60;
		param.monthlyAgreementHourLimitSpecial = (targetSetting.monthlyAgreementHourLimitSpecial - 60) / 60;

		Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(param));
		// 現在のオブジェクトの会社項目は更新時に変更できないため、Mapから除外しておく
		// 将来的には変更できるようになる予定。
		paramMap.remove('companyId');


		// API実行
		AttAgreementAlertSettingResource.UpdateApi api = new AttAgreementAlertSettingResource.UpdateApi();
		api.execute(paramMap);

		// 確認
		AttAgreementAlertSettingEntity resSetting = (new AttAgreementAlertSettingRepository()).getEntity(targetSetting.id);
		assertSaveAllField(param, resSetting);

	}

	/**
	 * 更新APIのテスト
	 * 権限のない管理者が残業警告設定を更新する場合、アプリ例外が発生することを確認する
	 */
	@isTest static void updateTestNoPermission() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('更新テスト', testSettingSize).get(0);
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 残業警告設定の管理権限を無効に設定する
		testData.permission.isManageAttAgreementAlertSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		AttAgreementAlertSettingResource.AgreementAlertSettingParam param =
				new AttAgreementAlertSettingResource.AgreementAlertSettingParam();
		param.id = targetSetting.id;
		param.name_L0 = targetSetting.nameL0 + 'Update';
		param.name_L1 = targetSetting.nameL1 + 'Update';
		param.name_L2 = targetSetting.nameL2 + 'Update';
		param.code = targetSetting.id;
		param.companyId = testData.company.id;
		param.validDateFrom = targetSetting.validFrom.getDate().addDays(10);
		param.validDateTo = targetSetting.validTo.getDate().addDays(10);
		param.monthlyAgreementHourWarning1 = (targetSetting.monthlyAgreementHourWarning1 - 60) / 60;
		param.monthlyAgreementHourWarning2 = (targetSetting.monthlyAgreementHourWarning2 - 60) / 60;
		param.monthlyAgreementHourLimit = (targetSetting.monthlyAgreementHourLimit - 60) / 60;
		param.monthlyAgreementHourWarningSpecial1 = (targetSetting.monthlyAgreementHourWarningSpecial1 - 60) / 60;
		param.monthlyAgreementHourWarningSpecial2 = (targetSetting.monthlyAgreementHourWarningSpecial2 - 60) / 60;
		param.monthlyAgreementHourLimitSpecial = (targetSetting.monthlyAgreementHourLimitSpecial - 60) / 60;

		Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(param));
		// 現在のオブジェクトの会社項目は更新時に変更できないため、Mapから除外しておく
		// 将来的には変更できるようになる予定。
		paramMap.remove('companyId');

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttAgreementAlertSettingResource.UpdateApi api = new AttAgreementAlertSettingResource.UpdateApi();
				api.execute(paramMap);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());

	}

	/**
	 * 更新APIのテスト
	 * リクエストパラメータにIDが指定されていない場合、アプリ例外が発生することを確認する
	 */
	@isTest static void updateTestIdNull() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('更新テスト', testSettingSize).get(0);

		AttAgreementAlertSettingResource.AgreementAlertSettingParam param =
				new AttAgreementAlertSettingResource.AgreementAlertSettingParam();
		param.id = null;
		param.name_L0 = targetSetting.nameL0 + 'Update';
		param.name_L1 = targetSetting.nameL1 + 'Update';
		param.name_L2 = targetSetting.nameL2 + 'Update';
		param.code = targetSetting.id;
		param.companyId = testData.company.id;
		param.validDateFrom = targetSetting.validFrom.getDate().addDays(10);
		param.validDateTo = targetSetting.validTo.getDate().addDays(10);
		param.monthlyAgreementHourWarning1 = targetSetting.monthlyAgreementHourWarning1 - 1;
		param.monthlyAgreementHourWarning2 = targetSetting.monthlyAgreementHourWarning2 - 1;
		param.monthlyAgreementHourLimit = targetSetting.monthlyAgreementHourLimit - 1;
		param.monthlyAgreementHourWarningSpecial1 = targetSetting.monthlyAgreementHourWarningSpecial1 - 1;
		param.monthlyAgreementHourWarningSpecial2 = targetSetting.monthlyAgreementHourWarningSpecial2 - 1;
		param.monthlyAgreementHourLimitSpecial = targetSetting.monthlyAgreementHourLimitSpecial - 1;

		Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(param));
		// 現在のオブジェクトの会社項目は更新時に変更できないため、Mapから除外しておく
		// 将来的には変更できるようになる予定。
		paramMap.remove('companyId');

		// API実行
		AttAgreementAlertSettingResource.UpdateApi api = new AttAgreementAlertSettingResource.UpdateApi();
		try {
			api.execute(paramMap);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}

	}

	/**
	 * 削除APIのテスト
	 * 指定したIDのレコードが削除できることを確認する
	 */
	@isTest static void deleteTest() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('更新テスト', testSettingSize).get(0);

		AttAgreementAlertSettingResource.DeleteRequest param =
				new AttAgreementAlertSettingResource.DeleteRequest();
		param.id = targetSetting.id;

		// API実行
		AttAgreementAlertSettingResource.DeleteApi api = new AttAgreementAlertSettingResource.DeleteApi();
		api.execute(param);

		// 削除されていることを確認
		AttAgreementAlertSettingEntity resSetting = (new AttAgreementAlertSettingRepository()).getEntity(targetSetting.id);
		System.assertEquals(null, resSetting);

	}

	/**
	 * 削除APIのテスト
	 * 削除済みのレコードIDを指定した場合、エラーが発生しないことを確認する
	 */
	@isTest static void deleteTestRecordNotFound() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('更新テスト', testSettingSize).get(0);

		AttAgreementAlertSettingResource.DeleteRequest param =
				new AttAgreementAlertSettingResource.DeleteRequest();
		param.id = targetSetting.id;

		// 削除
		(new AttAgreementAlertSettingRepository()).deleteEntity(targetSetting);

		// API実行
		AttAgreementAlertSettingResource.DeleteApi api = new AttAgreementAlertSettingResource.DeleteApi();
		api.execute(param);
	}

	/**
	 * 削除APIのテスト
	 * 権限のない管理者が残業警告設定を削除する場合、アプリ例外が発生することを確認する
	 */
	@isTest static void deleteTestNoPermission() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('更新テスト', testSettingSize).get(0);
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 残業警告設定の管理権限を無効に設定する
		testData.permission.isManageAttAgreementAlertSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		AttAgreementAlertSettingResource.DeleteRequest param =
				new AttAgreementAlertSettingResource.DeleteRequest();
		param.id = targetSetting.id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttAgreementAlertSettingResource.DeleteApi api = new AttAgreementAlertSettingResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());

	}

	/**
	 * 削除APIのテスト
	 * 削除対象のIDを指定しなかった場合、アプリ例外が発生することを確認する
	 */
	@isTest static void deleteTestIdNull() {
		final Integer testSettingSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		AttAgreementAlertSettingEntity targetSetting =
				testData.createAgreementAlertSettings('更新テスト', testSettingSize).get(0);

		AttAgreementAlertSettingResource.DeleteRequest param =
				new AttAgreementAlertSettingResource.DeleteRequest();
		param.id = null;

		// API実行
		AttAgreementAlertSettingResource.DeleteApi api = new AttAgreementAlertSettingResource.DeleteApi();
		try {
			api.execute(param);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/**
	 * 検索で取得した結果の各項目の値が期待値通りであることを確認する
	 */
	private static void assertSaveAllField(
			AttAgreementAlertSettingResource.AgreementAlertSettingParam expParam,
			AttAgreementAlertSettingEntity actEntity) {

		System.assertEquals(expParam.validDateFrom, AppConverter.dateValue(actEntity.validFrom));
		System.assertEquals(expParam.validDateTo, AppConverter.dateValue(actEntity.validTo));
		System.assertEquals(expParam.name_L0, actEntity.nameL0);
		System.assertEquals(expParam.name_L1, actEntity.nameL1);
		System.assertEquals(expParam.name_L2, actEntity.nameL2);
		System.assertEquals(expParam.code, actEntity.code);
		System.assertEquals(expParam.companyId, actEntity.companyId);
		System.assertEquals(expParam.monthlyAgreementHourWarning1, actEntity.monthlyAgreementHourWarning1 / 60);
		System.assertEquals(expParam.monthlyAgreementHourWarning2, actEntity.monthlyAgreementHourWarning2 / 60);
		System.assertEquals(expParam.monthlyAgreementHourLimit, actEntity.monthlyAgreementHourLimit / 60);
		System.assertEquals(expParam.monthlyAgreementHourWarningSpecial1, actEntity.monthlyAgreementHourWarningSpecial1 / 60);
		System.assertEquals(expParam.monthlyAgreementHourWarningSpecial2, actEntity.monthlyAgreementHourWarningSpecial2 / 60);
		System.assertEquals(expParam.monthlyAgreementHourLimitSpecial, actEntity.monthlyAgreementHourLimitSpecial / 60);

	}

	/**
	 * 検索APIのテスト
	 * リクエストパラメータに検索条件を指定しない場合アラート設定が全件取得できることを確認数る
	 * アラート設定の各項目が取得できていることも確認する
	 */
	@isTest static void searchTestAll() {
		final Integer recordSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		List<AttAgreementAlertSettingEntity> testSetting =
				testData.createAgreementAlertSettings('検索テスト', recordSize);

		AttAgreementAlertSettingResource.SearchApi api = new AttAgreementAlertSettingResource.SearchApi();
		Map<String, Object> paramMap = new Map<String, Object>{};

		Test.startTest();
			AttAgreementAlertSettingResource.SearchResponse res = (AttAgreementAlertSettingResource.SearchResponse)api.execute(paramMap);
		Test.stopTest();

		System.assertEquals(recordSize, res.records.size());
		for (Integer i = 0; i < recordSize; i++) {
			assertSearchAllField(testSetting[i], res.records[i]);
		}
	}

	/**
	 * 検索APIのテスト
	 * 指定した条件で検索できることを確認する
	 */
	@isTest static void searchTestFilter() {
		final Integer recordSize = 3;
		AttAgreementAlertSettingResourceTest.TestData testData = new TestData();
		List<AttAgreementAlertSettingEntity> testSetting =
				testData.createAgreementAlertSettings('検索テスト', recordSize);
		AttAgreementAlertSettingEntity targetSetting = testSetting[1];

		AttAgreementAlertSettingResource.SearchApi api = new AttAgreementAlertSettingResource.SearchApi();
		Map<String, Object> paramMap;
		AttAgreementAlertSettingResource.SearchResponse res;

		// アラート設定ID指定
		paramMap = new Map<String, Object>{
			'id' => targetSetting.id
		};
		res = (AttAgreementAlertSettingResource.SearchResponse)api.execute(paramMap);
		System.assertEquals(1, res.records.size());
		System.assertEquals(targetSetting.id, res.records[0].id);

		// 会社ID指定
		CompanyEntity targetCompany = testData.createCompany('会社検索テスト');
		AttAgreementAlertSettingEntity companyTestSetting =
				testData.createAgreementAlertSettings('会社検索テスト', targetCompany.id, 1).get(0);
		paramMap = new Map<String, Object>{
			'companyId' => targetCompany.id
		};
		res = (AttAgreementAlertSettingResource.SearchResponse)api.execute(paramMap);
		System.assertEquals(1, res.records.size());
		System.assertEquals(companyTestSetting.id, res.records[0].id);

		// 対象日指定
		paramMap = new Map<String, Object>{
			'targetDate' => targetSetting.validFrom.format()
		};
		res = (AttAgreementAlertSettingResource.SearchResponse)api.execute(paramMap);
		System.assertEquals(1, res.records.size());
		System.assertEquals(targetSetting.id, res.records[0].id);

	}

	/**
	 * 検索で取得した結果の各項目の値が期待値通りであることを確認する
	 */
	private static void assertSearchAllField(AttAgreementAlertSettingEntity expEntity,
			AttAgreementAlertSettingResource.AgreementAlertSettingParam actParam) {

		System.assertEquals(expEntity.id, actParam.id);
		System.assertEquals(expEntity.nameL.getValue(), actParam.name);
		System.assertEquals(AppConverter.dateValue(expEntity.validFrom), actParam.validDateFrom);
		System.assertEquals(AppConverter.dateValue(expEntity.validTo), actParam.validDateTo);
		System.assertEquals(expEntity.nameL0, actParam.name_L0);
		System.assertEquals(expEntity.nameL1, actParam.name_L1);
		System.assertEquals(expEntity.nameL2, actParam.name_L2);
		System.assertEquals(expEntity.code, actParam.code);
		System.assertEquals(expEntity.companyId, actParam.companyId);
		System.assertEquals(expEntity.monthlyAgreementHourWarning1 / 60, actParam.monthlyAgreementHourWarning1);
		System.assertEquals(expEntity.monthlyAgreementHourWarning2 / 60, actParam.monthlyAgreementHourWarning2);
		System.assertEquals(expEntity.monthlyAgreementHourLimit / 60, actParam.monthlyAgreementHourLimit);
		System.assertEquals(expEntity.monthlyAgreementHourWarningSpecial1 / 60, actParam.monthlyAgreementHourWarningSpecial1);
		System.assertEquals(expEntity.monthlyAgreementHourWarningSpecial2 / 60, actParam.monthlyAgreementHourWarningSpecial2);
		System.assertEquals(expEntity.monthlyAgreementHourLimitSpecial / 60, actParam.monthlyAgreementHourLimitSpecial);

	}

}
