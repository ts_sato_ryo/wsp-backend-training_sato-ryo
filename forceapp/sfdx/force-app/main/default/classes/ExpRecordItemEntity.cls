/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * 経費内訳のエンティティ Entity class for ExpRecordItem
 */
public with sharing class ExpRecordItemEntity extends ExpRecordItem {

	/** 項目の定義(変更管理で使用する) Fields definition */
	public enum Field {
		EXP_RECORD_ID
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** 値を変更した項目のリスト Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;


	/** コンストラクタ Constructor */
	public ExpRecordItemEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** Cost Center Linkage Code (for reference only) */
	public String costCenterLinkageCode {get; set;}

	/** 経費明細ID Expense Record ID */
	public Id expRecordId {
		get;
		set {
			expRecordId = value;
			setChanged(Field.EXP_RECORD_ID);
		}
	}

	/** Tax Type Code (for reference only) */
	public String taxTypeCode {get; set;}


	/**
	 * 項目の値を変更済みに設定する Mark field as changed
	 * @param field 変更した項目 Changed field
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する Check if the value of the field has been changed
	 * @param field 取得対象の項目 Target field
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする Reset the changed status
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
	}

	/*
	* Return the field value
	* @param name Target field name
	*/
	public override Object getValue(String name) {
		Field field = ExpRecordItemEntity.FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

		return super.getValue(name);
	}

	/**
	* Return the field value
	* @param field Target field to get the value
	* */
	public Object getValue(ExpRecordItemEntity.Field field) {
		if (field == ExpRecordItemEntity.Field.EXP_RECORD_ID) {
			return expRecordId;
		}

		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}