/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 社員履歴エンティティ
 */
public with sharing class EmployeeHistoryEntity extends EmployeeHistoryGeneratedEntity {

	/** タイトルの最大文字数 */
	public static final Integer TITLE_MAX_LENGTH = 80;

	/**
	 * デフォルトコンストラクタ
	 */
	public EmployeeHistoryEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public EmployeeHistoryEntity(ComEmpHistory__c sobj) {
		super(sobj);
	}

	/**
	 * @description Class representing Cost Center Record
	 */
	public class CostCenter {
		/** Cost Center Name */
		public AppMultiString nameL;
		/** Cost Center */
		public String code;
	}

	/**
	 * @desctiprion 部署項目を表すクラス
	 */
	public class Department {
		/** レコードのName項目値 */
		public AppMultiString nameL;
		/** 部署コード */
		public String code;
	}

	/**
	 * @desctiprion 社員のルックアップ項目を表すクラス
	 */
	public class Employee {
		/** レコードのName項目値 */
		public AppPersonName fullNameL;
		/** コンストラクタ */
		public Employee(AppPersonName fullNameL) {
			this.fullNameL = fullNameL;
		}
	}

	/**
	 * @description Class for Expense Employee Group Lookup
	 */
	public class ExpEmployeeGroup {
		/** Expense Employee Group Name */
		public AppMultiString nameL;
		/** Expense Employee Group Code */
		public String code;
	}

	/** Cost Center (reference) */
	public EmployeeHistoryEntity.CostCenter costCenter;

	/** 部署(参照のみ) */
	public EmployeeHistoryEntity.Department department;
	/** 役職 */
	public AppMultiString titleL {
		get {
			return new AppMultiString(titleL0, titleL1, titleL2);
		}
	}
	/** マネージャ */
	public EmployeeHistoryEntity.Employee manager {
		get {
			if (managerId == null) {
				return null;
			}
			if (manager == null) {
				manager = new EmployeeHistoryEntity.Employee(new AppPersonName(
						sobj.ManagerBaseId__r.FirstName_L0__c, sobj.ManagerBaseId__r.LastName_L0__c,
						sobj.ManagerBaseId__r.FirstName_L1__c, sobj.ManagerBaseId__r.LastName_L1__c,
						sobj.ManagerBaseId__r.FirstName_L2__c, sobj.ManagerBaseId__r.LastName_L2__c));
			}
			return manager;
		}
		private set;
	}
	/** 勤務体系 */
	public LookupField workingType;
	/** 工数設定 */
	public LookupField timeSetting;
	/** 36協定アラート設定 */
	public LookupField agreementAlertSetting {
		get {
			if (agreementAlertSettingId == null) {
				return null;
			}
			if (agreementAlertSetting == null) {
				agreementAlertSetting = new ParentChildHistoryEntity.LookupField(
						new AppMultiString(
							sobj.AgreementAlertSettingId__r.Name_L0__c,
							sobj.AgreementAlertSettingId__r.Name_L1__c,
							sobj.AgreementAlertSettingId__r.Name_L2__c));
			}
			return agreementAlertSetting;
		}
		private set;
	}
	/** 権限 */
	public LookupField permission {
			get {
			if (permissionId == null) {
				return null;
			}
			if (permission == null) {
				permission = new ParentChildHistoryEntity.LookupField(
						new AppMultiString(
							sobj.PermissionId__r.Name_L0__c,
							sobj.PermissionId__r.Name_L1__c,
							sobj.PermissionId__r.Name_L2__c));
			}
			return permission;
		}
		private set;
	}

	/** Expense Employee Group */
	public EmployeeHistoryEntity.ExpEmployeeGroup expEmployeeGroup {
		get {
			if (expEmployeeGroupId == null) {
				return null;
			}
			if (expEmployeeGroup == null) {
				expEmployeeGroup = new EmployeeHistoryEntity.ExpEmployeeGroup();
				expEmployeeGroup.nameL = new AppMultiString(
					sobj.ExpEmployeeGroupId__r.Name_L0__c,
					sobj.ExpEmployeeGroupId__r.Name_L1__c,
					sobj.ExpEmployeeGroupId__r.Name_L2__c);
				expEmployeeGroup.code = sobj.ExpEmployeeGroupId__r.Code__c;
			}
			return expEmployeeGroup;
		}
		set ;
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public EmployeeHistoryEntity copy() {
		EmployeeHistoryEntity copyEntity = new EmployeeHistoryEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildHistoryEntity copySuperEntity() {
		return copy();
	}

	/** Logical Delete */
	public override void setLogicallyDelete() {
		super.setLogicallyDelete();
		this.expEmployeeGroupId = null;
	}
}