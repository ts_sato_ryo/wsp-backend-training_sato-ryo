 /**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group アプリ
 *
 * 添付ファイルエンティティ
 */
public with sharing class AppAttachmentEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		BODY, NAME, PARENT_ID
	}
	/** 値を変更した項目のリスト */
	private Set<AppAttachmentEntity.Field> isChangedFieldSet;


	/** 添付ファイル名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** ファイルデータ(Blob) */
	public Blob body {
		get;
		set {
			body = value;
			setChanged(Field.BODY);
		}
	}
	/** ファイルデータ(文字列) */
	public String bodyText {
		get {
			if (body == null) {
				return null;
			}
			return body.toString();
		}
		set {
			if (bodyText == null) {
				body = null;
			}
			body = Blob.valueOf(value);
		}
	}

	/** 親ID(添付先のID) */
	public Id parentId {
		get;
		set {
			parentId = value;
			setChanged(Field.PARENT_ID);
		}
	}

	/** コンストラクタ*/
	public AppAttachmentEntity() {
		isChangedFieldSet = new Set<AppAttachmentEntity.Field>();
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}

}
