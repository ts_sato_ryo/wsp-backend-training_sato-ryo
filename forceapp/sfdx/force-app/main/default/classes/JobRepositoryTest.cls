@isTest
private class JobRepositoryTest {

	/**
	 * マスタデータを登録しておく
	 */
	@testSetup static void setup() {
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComTestDataUtility.createEmployeeWithHistory('Test', companyObj.Id, null, UserInfo.getUserId(), permission.Id);
		ComTestDataUtility.createDepartmentWithHistory('Test', companyObj.Id);
		ComTestDataUtility.createJobType('Test', companyObj.Id);
	}

	/** テストデータ */
	private class RepoTestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;
		/** 社員オブジェクト */
		public ComEmpBase__c employeeObj;
		/** 部署オブジェクト */
		public ComDeptBase__c departmentObj;
		/** ジョブタイプオブジェクト */
		public ComJobType__c jobTypeObj;

		/** コンストラクタ　*/
		public RepoTestData() {
			this.orgObj = [SELECT Id From ComOrgSetting__c LIMIT 1];
			this.countryObj = [SELECT Id From ComCountry__c LIMIT 1];
			this.companyObj = [SELECT Id, Code__c From ComCompany__c LIMIT 1];
			this.employeeObj = [SELECT Id From ComEmpBase__c LIMIT 1];
			this.departmentObj = [SELECT Id From ComDeptBase__c LIMIT 1];
			this.jobTypeObj = [SELECT Id From ComJobType__c LIMIT 1];
		}

		/**
		 * ジョブオブジェクトを作成する
		 */
		public List<ComJob__c> createJobObjs(String name, Integer size) {
			List<ComJob__c> jobs = new List<ComJob__c>();
			Date validFrom = Date.today();
			for (Integer i = 0; i < size; i++) {
				String num = String.valueOf(i + 1);
				String suffix = size > 1 ? num : '';
				ComJob__c j = new ComJob__c(
					Name = name + suffix,
					Name_L0__c = name + suffix + ' L0',
					Name_L1__c = name + suffix + ' L1',
					Name_L2__c = name + suffix + ' L2',
					Code__c = name + suffix + '_code',
					UniqKey__c = this.companyObj.Code__c + '-' + name + suffix + '_code',
					CompanyId__c = this.companyObj.Id,
					ValidFrom__c = validFrom,
					ValidTo__c = validFrom.addYears(1),
					DepartmentBaseId__c = this.departmentObj.Id,
					JobOwnerBaseId__c = this.employeeObj.Id,
					JobTypeId__c = this.jobTypeObj.Id,
					DirectCharged__c = true,
					SelectabledExpense__c = false,
					SelectabledTimeTrack__c = true
				);
				jobs.add(j);

				validFrom = j.ValidTo__c;
			}
			insert jobs;
			return jobs;
		}

		/**
		 * ジョブエンティティを作成する(DB保存なし)
		 */
		public JobEntity createJobEntity(String name) {
			JobEntity entity = new JobEntity();

			entity.name = name;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.code = name + '_code';
			entity.uniqKey = this.companyObj.Code__c + '-' + name + '_code';
			entity.companyId = this.companyObj.Id;
			entity.jobTypeId = this.jobTypeObj.Id;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);
			entity.parentId = null;
			entity.departmentBaseId = this.departmentObj.Id;
			entity.jobOwnerBaseId = this.employeeObj.Id;
			entity.isDirectCharged = true;
			entity.isScopedAssignment = true;
			entity.isSelectableExpense = true;
			entity.isSelectableTimeTrack = true;

			return entity;
		}

		/**
		 * 会社オブジェクトを作成する
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}

		/**
		 * 部署オブジェクトを作成する
		 */
		public ComDeptBase__c createDepartment(String name) {
			return ComTestDataUtility.createDepartmentWithHistory(name, this.companyObj.Id);
		}

	}

	/**
	 * searchEntityのテスト
	 * ジョブが検索できることを確認する
	 */
	@isTest static void getEntityTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetJobObj = jobObjs[1];

		// テスト実行
		JobRepository repo = new JobRepository();
		JobEntity resEntity;

		// ジョブIDで検索
		// 各項目の値が取得できているかどうかはsearchEntitで実施
		resEntity = repo.getEntity(targetJobObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetJobObj.Id, resEntity.id);

		// 存在しないIDを指定した場合
		// nullが返却される
		delete targetJobObj;
		resEntity = repo.getEntity(targetJobObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * ジョブが検索できることを確認する
	 */
	@isTest static void getEntityByCodeTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetJobObj = jobObjs[1];
		targetJobObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		update targetJobObj;

		// テスト実行
		JobRepository repo = new JobRepository();
		JobEntity resEntity;

		// ジョブコードで検索
		// 各項目の値が取得できているかどうかはsearchEntitで実施
		resEntity = repo.getEntityByCode(targetJobObj.Code__c, targetJobObj.CompanyId__c);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetJobObj.Id, resEntity.id);

		// 存在しないコードを指定した場合
		// nullが返却される
		delete targetJobObj;
		resEntity = repo.getEntityByCode(targetJobObj.Code__c, testData.companyObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * ジョブIDを条件にジョブを検索できることを確認する
	 */
	@isTest static void searchEntityByJobIdTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetJobObj = jobObjs[1];
		targetJobObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		targetJobObj.DepartmentBaseId__c = testData.createDepartment('部署で検索').Id;
		targetJobObj.ParentId__c = jobObjs[0].Id;
		update targetJobObj;
		targetJobObj = getJobObj(targetJobObj.Id); // 参照先の項目を取得するため

		// ジョブIDで検索
		JobRepository repo = new JobRepository();
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.ids = new Set<Id>{targetJobObj.Id};
		List<JobEntity> resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetJobObj.Id, resEntities[0].id);

		// 全ての項目の値が取得できていることを確認
		assertFieldValue(targetJobObj, resEntities[0]);
	}

	/**
	 * searchEntityのテスト
	 * 会社コードを条件にジョブを検索できることを確認する
	 */
	@isTest static void searchEntityByCompanyIdTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetJobObj = jobObjs[1];
		targetJobObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		targetJobObj.DepartmentBaseId__c = testData.createDepartment('部署で検索').Id;
		targetJobObj.ParentId__c = jobObjs[0].Id;
		update targetJobObj;
		targetJobObj = getJobObj(targetJobObj.Id); // 参照先の項目を取得するため

		// テスト実行
		JobRepository repo = new JobRepository();
		JobRepository.SearchFilter filter;
		List<JobEntity> resEntities;

		// 会社IDで検索
		filter = new JobRepository.SearchFilter();
		filter.companyIds = new Set<Id>{targetJobObj.CompanyId__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetJobObj.Id, resEntities[0].id);
	}
	/**
	 * searchEntityのテスト
	 * ジョブが検索できることを確認する
	 */
	@isTest static void searchEntityByCodeTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetJobObj = jobObjs[1];
		targetJobObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		targetJobObj.DepartmentBaseId__c = testData.createDepartment('部署で検索').Id;
		targetJobObj.ParentId__c = jobObjs[0].Id;
		update targetJobObj;
		targetJobObj = getJobObj(targetJobObj.Id); // 参照先の項目を取得するため

		// コード(完全一致)で検索
		JobRepository repo = new JobRepository();
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.codes = new Set<String>{targetJobObj.Code__c};
		List<JobEntity> resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetJobObj.Id, resEntities[0].id);
	}

	/**
	 * searchEntityのテスト
	 * 部署ベースIDを条件にジョブを検索できることを確認する
	 */
	@isTest static void searchEntityByDepartmentIdTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetJobObj = jobObjs[1];
		targetJobObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		targetJobObj.DepartmentBaseId__c = testData.createDepartment('部署で検索').Id;
		targetJobObj.ParentId__c = jobObjs[0].Id;
		update targetJobObj;
		targetJobObj = getJobObj(targetJobObj.Id); // 参照先の項目を取得するため

		// 部署で検索
		JobRepository repo = new JobRepository();
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.departmentIds = new Set<Id>{targetJobObj.DepartmentBaseId__c};
		List<JobEntity> resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetJobObj.Id, resEntities[0].id);
	}

	/**
	 * searchEntityのテスト
	 * 親ジョブIDを条件にジョブを検索できることを確認する
	 */
	@isTest static void searchEntityByParentIdTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetJobObj = jobObjs[1];
		targetJobObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		targetJobObj.DepartmentBaseId__c = testData.createDepartment('部署で検索').Id;
		targetJobObj.ParentId__c = jobObjs[0].Id;
		update targetJobObj;
		targetJobObj = getJobObj(targetJobObj.Id); // 参照先の項目を取得するため

		// 親ジョブで検索
		JobRepository repo = new JobRepository();
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.parentIds = new Set<Id>{targetJobObj.ParentId__c};
		List<JobEntity> resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetJobObj.Id, resEntities[0].id);
	}

	/**
	 * Search Entity By Query
	 * Query represent Name or Code
	 */
	@isTest static void searchEntityByQueryTest() {
		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', jobNumber);
		List<ComJob__c> jobXyObjs = testData.createJobObjs('X_yst', jobNumber + 1);
		List<ComJob__c> jobJJObjs = testData.createJobObjs('J.Jst', jobNumber + 2);
		List<ComJob__c> jobEstObjs = testData.createJobObjs('E*ELF', jobNumber + 3);
		List<ComJob__c> jobPerObjs = testData.createJobObjs('P%PJ', jobNumber + 4);

		JobRepository repo = new JobRepository();
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();

		// Find all Job using 'st' query
		filter.query = 'st';
		List<JobEntity> resEntityList = repo.searchEntity(filter);
		System.assertEquals(12, resEntityList.size());

		// Find Job with '_yst' query
		filter.query = '_yst';
		resEntityList = repo.searchEntity(filter);
		System.assertEquals(4, resEntityList.size());
		assertFieldValue(getJobObj(jobXyObjs[0].Id), resEntityList.get(0));

		// Find Job with '.Jst' query
		filter.query = '.Jst';
		resEntityList = repo.searchEntity(filter);
		System.assertEquals(5, resEntityList.size());
		assertFieldValue(getJobObj(jobJJObjs[0].Id), resEntityList.get(0));

		// Find Job with '*E' query
		filter.query = '*E';
		resEntityList = repo.searchEntity(filter);
		System.assertEquals(6, resEntityList.size());
		assertFieldValue(getJobObj(jobEstObjs[0].Id), resEntityList.get(0));

		// Find Job with '%' query
		filter.query = '%';
		resEntityList = repo.searchEntity(filter);
		System.assertEquals(7, resEntityList.size());
		assertFieldValue(getJobObj(jobPerObjs[0].Id), resEntityList.get(0));

		// Find Job with 'Test' query
		filter.query = 'Test';
		resEntityList = repo.searchEntity(filter);
		System.assertEquals(jobNumber, resEntityList.size());
		for (Integer i = 0; i < jobNumber; i++) {
			assertFieldValue(getJobObj(jobObjs[i].Id), resEntityList.get(i));
		}
	}

	/**
	 * searchEntityのテスト
	 * 指定した対象日に有効なエンティティが取得できることを確認する
	 */
	@isTest static void searchEntityTestValidDate() {

		final Integer jobNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> targetObjs = testData.createJobObjs('Test', jobNumber);
		ComJob__c targetObj = targetObjs[0];
		ComJob__c targetObj2 = targetObjs[1];
		ComJob__c targetObj3 = targetObjs[2];
		targetObj2.ValidFrom__c = targetObj.ValidTo__c;
		targetObj2.ValidTo__c = targetObj2.ValidFrom__c.addYears(1);
		targetObj3.ValidFrom__c = targetObj2.ValidTo__c;
		targetObj3.ValidTo__c = targetObj3.ValidFrom__c.addYears(1);
		update targetObjs;

		// テスト実行
		JobRepository repo = new JobRepository();
		JobRepository.SearchFilter filter;
		List<JobEntity> resEntities;

		// 取得対象日に履歴の有効期間開始日を指定
		filter = new JobRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj2.ValidFrom__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		filter = new JobRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c.addDays(-1));
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj3.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日を指定
		// 取得できないはず
		filter = new JobRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());
	}

	/**
	 * 対象日の範囲を条件に正しく検索出来ることを検証する
	 */
	@isTest
	static void searchEntityTestValidateDateRange() {
		// テストデータ作成
		AppDate minDate = ValidPeriodEntity.VALID_FROM_MIN;
		AppDate maxDate = ValidPeriodEntity.VALID_TO_MAX;
		List<AppDateRange> ranges = new List<AppDateRange>();
		ranges.add(new AppDateRange(minDate, AppDate.newInstance(2017, 12, 31))); // 対象外
		ranges.add(new AppDateRange(minDate, AppDate.newInstance(2018, 1, 1)));   // 対象外
		ranges.add(new AppDateRange(minDate, AppDate.newInstance(2018, 1, 2)));
		ranges.add(new AppDateRange(minDate, AppDate.newInstance(2018, 1, 31)));
		ranges.add(new AppDateRange(minDate, AppDate.newInstance(2018, 2, 1)));
		ranges.add(new AppDateRange(minDate, AppDate.newInstance(2018, 2, 2)));
		ranges.add(new AppDateRange(AppDate.newInstance(2017, 12, 31), maxDate));
		ranges.add(new AppDateRange(AppDate.newInstance(2018, 1, 1), maxDate));
		ranges.add(new AppDateRange(AppDate.newInstance(2018, 1, 2), maxDate));
		ranges.add(new AppDateRange(AppDate.newInstance(2018, 1, 31), maxDate));
		ranges.add(new AppDateRange(AppDate.newInstance(2018, 2, 1), maxDate)); // 対象外
		ranges.add(new AppDateRange(AppDate.newInstance(2018, 2, 2), maxDate)); // 対象外
		ranges.add(new AppDateRange(AppDate.newInstance(2018, 1, 10), AppDate.newInstance(2018, 1, 20)));

		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobs = testData.createJobObjs('Test', ranges.size());
		for (Integer i = 0; i < ranges.size(); i++) {
			jobs[i].ValidFrom__c = ranges[i].startDate.getDate();
			jobs[i].ValidTo__c = ranges[i].endDate.getDate();
		}
		upsert jobs;

		// 検索
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.dateRange = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2018, 1, 31));
		List<JobEntity> entities = new JobRepository().searchEntity(filter);
		System.assertEquals(9, entities.size());
		for (JobEntity entity : entities) {
			System.assertNotEquals(jobs[0].id, entity.id);
			System.assertNotEquals(jobs[1].id, entity.id);
			System.assertNotEquals(jobs[10].id, entity.id);
			System.assertNotEquals(jobs[11].id, entity.id);
		}
	}

	/**
	 * searchEntityのテスト
	 * 経費選択可能フラグを条件にジョブを検索できることを確認する
	 */
	@isTest static void searchEntityBySelectableExpenseTest() {
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', 4);
		jobObjs[0].SelectabledExpense__c = false;
		jobObjs[0].SelectabledTimeTrack__c = false;
		jobObjs[1].SelectabledExpense__c = false;
		jobObjs[1].SelectabledTimeTrack__c = true;
		jobObjs[2].SelectabledExpense__c = true;
		jobObjs[2].SelectabledTimeTrack__c = false;
		jobObjs[3].SelectabledExpense__c = true;
		jobObjs[3].SelectabledTimeTrack__c = true;
		upsert jobObjs;

		// 経費選択可能フラグ指定なし
		JobRepository repo = new JobRepository();
		List<JobEntity> jobs = repo.searchEntity(new JobRepository.SearchFilter());
		System.assertEquals(4, jobs.size());
		System.assertEquals(jobObjs[0].Id, jobs[0].id);
		System.assertEquals(jobObjs[1].Id, jobs[1].id);
		System.assertEquals(jobObjs[2].Id, jobs[2].id);
		System.assertEquals(jobObjs[3].Id, jobs[3].id);

		// 経費選択可能フラグ ON
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.isSelectableExpense = true;
		jobs = repo.searchEntity(filter);

		System.assertEquals(2, jobs.size());
		System.assertEquals(jobObjs[2].Id, jobs[0].id);
		System.assertEquals(jobObjs[3].Id, jobs[1].id);
		System.assertEquals(true, jobs[0].isSelectableExpense);
		System.assertEquals(true, jobs[1].isSelectableExpense);

		// 経費選択可能フラグ OFF
		filter = new JobRepository.SearchFilter();
		filter.isSelectableExpense = false;
		jobs = repo.searchEntity(filter);
		System.assertEquals(2, jobs.size());
		System.assertEquals(jobObjs[0].Id, jobs[0].id);
		System.assertEquals(jobObjs[1].Id, jobs[1].id);
		System.assertEquals(false, jobs[0].isSelectableExpense);
		System.assertEquals(false, jobs[1].isSelectableExpense);
	}

	/**
	 * searchEntityのテスト
	 * 工数選択可能フラグを条件にジョブを検索できることを確認する
	 */
	@isTest static void searchEntityBySelectableTimeTrackTest() {
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', 4);
		jobObjs[0].SelectabledExpense__c = false;
		jobObjs[0].SelectabledTimeTrack__c = false;
		jobObjs[1].SelectabledExpense__c = false;
		jobObjs[1].SelectabledTimeTrack__c = true;
		jobObjs[2].SelectabledExpense__c = true;
		jobObjs[2].SelectabledTimeTrack__c = false;
		jobObjs[3].SelectabledExpense__c = true;
		jobObjs[3].SelectabledTimeTrack__c = true;
		upsert jobObjs;

		// 経費選択可能フラグ指定なし
		JobRepository repo = new JobRepository();
		List<JobEntity> entities = repo.searchEntity(new JobRepository.SearchFilter());
		System.assertEquals(4, entities.size());
		System.assertEquals(jobObjs[0].Id, entities[0].id);
		System.assertEquals(jobObjs[1].Id, entities[1].id);
		System.assertEquals(jobObjs[2].Id, entities[2].id);
		System.assertEquals(jobObjs[3].Id, entities[3].id);

		// 経費選択可能フラグ ON
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.isSelectableTimeTrack = true;
		entities = repo.searchEntity(filter);
		System.assertEquals(2, entities.size());
		System.assertEquals(jobObjs[1].Id, entities[0].id);
		System.assertEquals(jobObjs[3].Id, entities[1].id);
		System.assertEquals(true, entities[0].isSelectableTimeTrack);
		System.assertEquals(true, entities[1].isSelectableTimeTrack);

		// 経費選択可能フラグ OFF
		filter = new JobRepository.SearchFilter();
		filter.isSelectableTimeTrack = false;
		entities = repo.searchEntity(filter);
		System.assertEquals(2, entities.size());
		System.assertEquals(jobObjs[0].Id, entities[0].id);
		System.assertEquals(jobObjs[2].Id, entities[1].id);
		System.assertEquals(false, entities[1].isSelectableTimeTrack);
		System.assertEquals(false, entities[1].isSelectableTimeTrack);
	}

	/**
	 * searchEntityのテスト
	 * アサイン個別指定フラグを条件にジョブを検索できることを確認する
	 */
	@isTest static void searchEntityByIsScopedAssignmentTest() {
		RepoTestData testData = new RepoTestData();
		List<ComJob__c> jobObjs = testData.createJobObjs('Test', 2);
		jobObjs[0].ScopedAssignment__c = true;
		jobObjs[1].ScopedAssignment__c = false;
		upsert jobObjs;

		// アサイン個別指定 指定なし
		JobRepository repo = new JobRepository();
		List<JobEntity> entities = repo.searchEntity(new JobRepository.SearchFilter());
		System.assertEquals(2, entities.size());
		System.assertEquals(jobObjs[0].Id, entities[0].id);
		System.assertEquals(jobObjs[1].Id, entities[1].id);

		// アサイン個別指定 true
		JobRepository.SearchFilter filter = new JobRepository.SearchFilter();
		filter.isScopedAssignment = true;
		entities = repo.searchEntity(filter);
		System.assertEquals(1, entities.size());
		System.assertEquals(jobObjs[0].Id, entities[0].id);

		// アサイン個別指定 false
		filter = new JobRepository.SearchFilter();
		filter.isScopedAssignment = false;
		entities = repo.searchEntity(filter);
		System.assertEquals(1, entities.size());
		System.assertEquals(jobObjs[1].Id, entities[0].id);
	}
	
	/**
	 * ジョブが保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		RepoTestData testData = new RepoTestData();
		JobEntity entity = testData.createJobEntity('Test');

		// テスト実行
		JobRepository repo = new JobRepository();
		Repository.SaveResult result = repo.saveEntity(entity);

		// 確認
		System.assertEquals(true, result.isSuccessAll);

		ComJob__c resObj1 = getJobObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(entity, resObj1);
	}


	/**
	 * ジョブオブジェクトを取得する
	 */
	private static ComJob__c getJobObj(Id jobId) {
		return [
				SELECT
					Id, Name, Name_L0__c, Name_L1__c, Name_L2__c, Code__c, UniqKey__c, CompanyId__c,
					ValidFrom__c, ValidTo__c, SelectabledExpense__c, SelectabledTimeTrack__c, JobTypeId__c,
					ParentId__c, DepartmentBaseId__c, JobOwnerBaseId__c, DirectCharged__c, ScopedAssignment__c,
					ParentId__r.Name_L0__c, ParentId__r.Name_L1__c, ParentId__r.Name_L2__c,
					JobOwnerBaseId__r.FirstName_L0__c, JobOwnerBaseId__r.LastName_L0__c,
					JobOwnerBaseId__r.FirstName_L1__c, JobOwnerBaseId__r.LastName_L1__c,
					JobOwnerBaseId__r.FirstName_L2__c, JobOwnerBaseId__r.LastName_L2__c,
					JobTypeId__r.Name_L0__c, JobTypeId__r.Name_L1__c, JobTypeId__r.Name_L2__c
				FROM
					ComJob__c
				WHERE
					Id = :jobId];
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(ComJob__c expObj, JobEntity actEntity) {

		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(AppDate.valueOf(expObj.ValidFrom__c), actEntity.validFrom);
		System.assertEquals(AppDate.valueOf(expObj.ValidTo__c), actEntity.validTo);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.JobTypeId__c, actEntity.jobTypeId);
		System.assertEquals(expObj.ParentId__c, actEntity.parentId);
		System.assertEquals(expObj.DepartmentBaseId__c, actEntity.departmentBaseId);
		System.assertEquals(expObj.JobOwnerBaseId__c, actEntity.jobOwnerBaseId);
		System.assertEquals(expObj.DirectCharged__c, actEntity.isDirectCharged);
		System.assertEquals(expObj.SelectabledExpense__c, actEntity.isSelectableExpense);
		System.assertEquals(expObj.SelectabledTimeTrack__c, actEntity.isSelectableTimeTrack);
		System.assertEquals(expObj.ScopedAssignment__c, actEntity.isScopedAssignment);

		// ジョブオーナー
		if (expObj.JobOwnerBaseId__c != null) {
			System.assertEquals(expObj.JobOwnerBaseId__r.FirstName_L0__c, actEntity.jobOwner.fullName.firstNameL0);
			System.assertEquals(expObj.JobOwnerBaseId__r.FirstName_L1__c, actEntity.jobOwner.fullName.firstNameL1);
			System.assertEquals(expObj.JobOwnerBaseId__r.FirstName_L2__c, actEntity.jobOwner.fullName.firstNameL2);
			System.assertEquals(expObj.JobOwnerBaseId__r.LastName_L0__c, actEntity.jobOwner.fullName.lastNameL0);
			System.assertEquals(expObj.JobOwnerBaseId__r.LastName_L1__c, actEntity.jobOwner.fullName.lastNameL1);
			System.assertEquals(expObj.JobOwnerBaseId__r.LastName_L2__c, actEntity.jobOwner.fullName.lastNameL2);
		}

		// 親ジョブ
		if (expObj.ParentId__c != null) {
			System.assertEquals(expObj.ParentId__r.Name_L0__c, actEntity.parent.nameL.valueL0);
			System.assertEquals(expObj.ParentId__r.Name_L1__c, actEntity.parent.nameL.valueL1);
			System.assertEquals(expObj.ParentId__r.Name_L2__c, actEntity.parent.nameL.valueL2);
		}

		// ジョブタイプ
		if (expObj.JobTypeId__c != null) {
			System.assertEquals(expObj.JobTypeId__r.Name_L0__c, actEntity.jobType.nameL.valueL0);
			System.assertEquals(expObj.JobTypeId__r.Name_L1__c, actEntity.jobType.nameL.valueL1);
			System.assertEquals(expObj.JobTypeId__r.Name_L2__c, actEntity.jobType.nameL.valueL2);
		}
	}

	/**
	 * 各項目の値を検証する
	 * @param expEntity 期待値
	 * @param actObj 実際の値
	 */
	private static void assertFieldValue(JobEntity expEntity, ComJob__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.validFrom, AppDate.valueOf(actObj.ValidFrom__c));
		System.assertEquals(expEntity.validTo, AppDate.valueOf(actObj.ValidTo__c));
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
		System.assertEquals(expEntity.parentId, actObj.ParentId__c);
		System.assertEquals(expEntity.departmentBaseId, actObj.DepartmentBaseId__c);
		System.assertEquals(expEntity.jobOwnerBaseId, actObj.JobOwnerBaseId__c);
		System.assertEquals(expEntity.isDirectCharged, actObj.DirectCharged__c);
		System.assertEquals(expEntity.isScopedAssignment, actObj.ScopedAssignment__c);
		System.assertEquals(expEntity.isSelectableExpense, actObj.SelectabledExpense__c);
		System.assertEquals(expEntity.isSelectableTimeTrack, actObj.SelectabledTimeTrack__c);
	}

}