/**
* @author TeamSpirit Inc.
* @date 2019
*
* @group 共通
*
* @description TagService のテストクラス
*/
@isTest
private class TagServiceTest {

	/**
	 * 保存のテスト（正常系）
	 * 新規作成
	 */
	 @isTest
	static void saveTagTestCreate() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity entity = new TagEntity();
		entity.companyId = testData.company.id;
		entity.code = '001';
		entity.tagTypeValue = TagType.SHORTEN_WORK_REASON;
		entity.nameL0 = 'テスト設定_L0';
		entity.nameL1 = 'テスト設定_L1';
		entity.nameL2 = 'テスト設定_L2';
		entity.order = 1;

		Test.startTest();
		Repository.SaveResult result = new TagService().saveTag(entity);
		Test.stopTest();

		// レコードが作成されている
		TagEntity resultEntity = new TagRepository().getEntity(result.details[0].id);
		System.assertNotEquals(null, entity);

		// レコード値の検証
		System.assertEquals(entity.code, resultEntity.code);
		System.assertEquals(entity.companyId, resultEntity.companyId);
		System.assertEquals(entity.tagTypeValue, resultEntity.tagTypeValue);
		System.assertEquals(entity.nameL0, resultEntity.nameL0);
		System.assertEquals(entity.nameL1, resultEntity.nameL1);
		System.assertEquals(entity.nameL2, resultEntity.nameL2);
		System.assertEquals(entity.order, resultEntity.order);
	}

	/**
	 * 保存のテスト（正常系）
	 * 更新
	 */
	@isTest
	static void saveTagTestUpdate() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity baseTagEntity = testData.createTag('TestTag', TagType.SHORTEN_WORK_REASON);

		TagEntity entity = new TagEntity();
		entity.setId(baseTagEntity.id);
		entity.companyId = testData.company.id;
		entity.code = '001';
		entity.tagTypeValue = TagType.JOB_ASSIGN_GROUP;
		entity.nameL0 = 'テスト設定_L0';
		entity.nameL1 = 'テスト設定_L1';
		entity.nameL2 = 'テスト設定_L2';
		entity.order = 1;

		Test.startTest();
		Repository.SaveResult result = new TagService().saveTag(entity);
		Test.stopTest();

		// レコードが作成されている
		TagEntity resultEntity = new TagRepository().getEntity(result.details[0].id);

		// レコード値の検証
		System.assertEquals(entity.id, resultEntity.id);
		System.assertEquals(entity.code, resultEntity.code);
		System.assertEquals(entity.companyId, resultEntity.companyId);
		System.assertEquals(entity.tagTypeValue, resultEntity.tagTypeValue);
		System.assertEquals(entity.nameL0, resultEntity.nameL0);
		System.assertEquals(entity.nameL1, resultEntity.nameL1);
		System.assertEquals(entity.nameL2, resultEntity.nameL2);
		System.assertEquals(entity.order, resultEntity.order);
	}

	/**
	 * 保存のテスト（異常系）
	 * 更新でエンティティが存在しない
	 */
	 @isTest
	 static void saveTagNegativeTestNotFoundEntity() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity baseTagEntity = testData.createTag('TestTag', TagType.SHORTEN_WORK_REASON);
		TagEntity entity = new TagEntity();
		entity.setId(baseTagEntity.id);

		App.RecordNotFoundException ex;
		Test.startTest();
		try {
			new TagRepository().deleteEntity(baseTagEntity);
			new TagService().saveTag(entity);
		} catch (App.RecordNotFoundException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('RECORD_NOT_FOUND', ex.getErrorCode());
	 }

	/**
	 * 保存のテスト（異常系）
	 * 会社が存在しない場合
	 */
	 @isTest
	 static void saveTagNegativeTestNotFoundCompany() {
		TagEntity entity = new TagEntity();

		App.RecordNotFoundException ex;
		Test.startTest();
		try {
			new TagService().saveTag(entity);
		} catch (App.RecordNotFoundException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('RECORD_NOT_FOUND', ex.getErrorCode());
	 }

	/**
	 * 保存のテスト（異常系）
	 * コードが重複する場合
	 */
	 @isTest
	 static void saveTagNegativeTestUniqKeyDuplication() {
		TagService service = new TagService();
		TestData.TestDataEntity testData = new TestData.TestDataEntity();

		App.ParameterException ex;
		Test.startTest();
		try {
			TagEntity baseTagEntity = new TagEntity();
			baseTagEntity.name = 'TEST';
			baseTagEntity.nameL0 = 'TEST';
			baseTagEntity.code = 'TEST';
			baseTagEntity.companyId = testData.company.id;
			baseTagEntity.tagTypeValue = TagType.SHORTEN_WORK_REASON;
			service.saveTag(baseTagEntity);

			TagEntity entity = new TagEntity();
			entity.name = baseTagEntity.name;
			entity.nameL0 = baseTagEntity.nameL0;
			entity.code = baseTagEntity.code;
			entity.companyId = baseTagEntity.companyId;
			entity.tagTypeValue = baseTagEntity.tagTypeValue;
			service.saveTag(entity);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	 }

	/**
	 * 保存のテスト（異常系）
	 * バリデーションエラー（名前が存在しない）
	 */
	 @isTest
	 static void saveTagNegativeTestValidationError() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity entity = new TagEntity();
		entity.code = 'TEST';
		entity.companyId = testData.company.id;
		entity.tagTypeValue = TagType.SHORTEN_WORK_REASON;

		App.ParameterException ex;
		Test.startTest();
		try {
			new TagService().saveTag(entity);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals('NOT_SET_VALUE', ex.getErrorCode());
	 }

	/**
	 * 保存のテスト（異常系）
	 * バリデーションエラー（コードが指定されていない、またはスペースしか入力されていない）
	 */
	 @isTest
	 static void saveTagNegativeTestNoCode() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		TagEntity entity = new TagEntity();
		entity.code = ' ';
		entity.name = '短時間勤務理由';
		entity.nameL0 = '短時間勤務理由';
		entity.companyId = testData.company.id;
		entity.tagTypeValue = TagType.SHORTEN_WORK_REASON;

		App.ParameterException ex;
		Test.startTest();
		try {
			new TagService().saveTag(entity);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}), ex.getMessage());
	 }
}