/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 勤怠
  *
  * フレックス労働制の勤怠計算ロジック
  */
public with sharing class AttCalcFlexLogic extends AttCalcLogic {
	AttCalcOverTimeFlexLogic calcOverTimeLogic = new AttCalcOverTimeFlexLogic();
	public AttCalcFlexLogic(iOutput output, iInput input) {
		super(output, input);
	}

	public override Date getStartDateWeekly(Date d) {
		return input.getStartDateMonthly(d); // フレックスタイム制の週計算＝月計算
	}
	private Integer getDayCountWeekly(Date d) {
		return input.getStartDateMonthly(d).daysBetween(input.getEndDateMonthly(d)) + 1;
	}
	private class CalcAttendanceDailyInput extends AttCalcOverTimeFlexLogic.Input {
		private Day day;
		private Config conf;
		public CalcAttendanceDailyInput(AttCalcLogic.iInput input, Day day) {
			this.day = day;
			this.conf = input.getConfig(day.getDate());
			this.inputStartTime = day.input.inputStartTime;
			this.inputEndTime = day.input.inputEndTime;
			this.inputRestRanges = day.input.inputRestRanges;
			this.inputRestTime = day.input.inputRestTime;
			this.startTime = day.input.startTime; // コアタイム開始時刻
			this.startTime = day.input.startTime; // コアタイム開始時刻
			this.endTime = day.input.endTime; // コアタイム終了時刻
			this.flexStartTime = day.input.flexStartTime; // フレックスタイム開始時刻
			this.flexEndTime = day.input.flexEndTime; // フレックスタイム終了時刻
			this.withoutCoreTime = conf.withoutCoreTime;
			this.contractWorkTime = day.input.contractWorkTime;
			this.permitedLostTimeRanges = day.input.permitedLostTimeRanges;
			this.convertdRestRanges = day.input.convertdRestRanges;
			this.convertedWorkTime = day.input.convertedWorkTime;
			this.unpaidLeaveRestRanges = day.input.unpaidleaveRestRanges;
			this.unpaidLeaveTime = day.input.unpaidLeaveTime;
			this.allowedWorkRanges = day.input.allowedWorkRanges;
			this.contractRestRanges = day.input.contractRestRanges ;
			this.contractRestTime = day.input.contractRestTime;
			this.legalWorkTime = conf.LegalWorkTimeDaily;
			this.isWorkDay = day.isWorkDay();
			this.isLegalHoliday = day.input.isLegalHoliday;
			this.isNoOffsetOvertimeByUndertime = conf.isNoOffsetOvertimeByUndertime;
			this.isPermitOvertimeWorkUntilNormalHours = conf.isPermitOvertimeWorkUntilNormalHours;
			this.isIncludeCompensationTimeToWorkHours = conf.isIncludeCompensationTimeToWorkHours;
			this.isIncludeHolidayWorkInPlainTime = conf.isIncludeHolidayWorkInPlainTime;
		}

		public override String toString() {
			return day.toString();
		}
	}

	// フレックスの日次勤怠計算は、
	// 所定の観点で所定内-所定外を区別しない。所定内に集計する
	// 法定の観点で法定内と法定外は区別しない。法定内に集計するが、法定休日は区別する
	// フレックスで法定休日所定内がありうるか？ ない？
	public override Boolean calcAttendanceDaily(Day day, Day dayNext) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcFlexLogic.calcAttendanceDaily start');
		System.debug(LoggingLevel.DEBUG, '---target day is ' + day);
		// 勤務日は 所定内法定内 ... 所定勤務時間まで
		CalcAttendanceDailyInput tmpInput = new CalcAttendanceDailyInput(input, day);
		AttCalcOverTimeFlexLogic.Output tmpOutput = new AttCalcOverTimeFlexLogic.Output();
		calcOverTimeLogic.apply(tmpOutput, tmpInput);
		day.output.realWorkTime = tmpOutput.realWorkTime;
		day.output.convertedWorkTime = tmpOutput.convertedWorkTime;
		day.output.unpaidLeaveLostTime = 0;
		day.output.paidLeaveTime = tmpOutput.paidLeaveTime;
		day.output.unpaidLeaveTime = tmpOutput.unpaidLeaveTime;
		day.output.lateArriveTime = tmpOutput.lateArriveTime;
		day.output.earlyLeaveTime = tmpOutput.earlyLeaveTime;
		day.output.breakTime = tmpOutput.breakTime;
		day.output.lateArriveLostTime = 0;
		day.output.earlyLeaveLostTime = 0;
		day.output.breakLostTime = 0;
		day.output.authorizedLateArriveTime = tmpOutput.authorizedLateArriveTime;
		day.output.authorizedEarlyLeaveTime = tmpOutput.authorizedEarlyLeaveTime;
		day.output.authorizedBreakTime = tmpOutput.authorizedBreakTime;

		AttCalcRangesDto inContractInLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeFlexLogic.OverTimeCategory.inContractInLegal);
		AttCalcRangesDto inContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeFlexLogic.OverTimeCategory.inContractOutLegal);
		AttCalcRangesDto outContractInLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeFlexLogic.OverTimeCategory.outContractInLegal);
		AttCalcRangesDto outContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeFlexLogic.OverTimeCategory.outContractOutLegal);

		AttCalcRangesDto inContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto inContractOutLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractOutLegal_nextDay = AttCalcRangesDto.RS();

		if (!getCanCalcContinueNextDay(day, dayNext)) {
			Integer boundaryTime = input.getConfig(day.getDate()).boundaryTimeOfDay;
			boundaryTime += 24 * 60; // 翌日かを判定するため、24時間後にする
			inContractInLegal_nextDay = inContractInLegal.cutAfter(boundaryTime);
			inContractOutLegal_nextDay = inContractOutLegal.cutAfter(boundaryTime);
			outContractInLegal_nextDay = outContractInLegal.cutAfter(boundaryTime);
			outContractOutLegal_nextDay = outContractOutLegal.cutAfter(boundaryTime);

			inContractInLegal = inContractInLegal.cutBefore(boundaryTime);
			inContractOutLegal = inContractOutLegal.cutBefore(boundaryTime);
			outContractInLegal = outContractInLegal.cutBefore(boundaryTime);
			outContractOutLegal = outContractOutLegal.cutBefore(boundaryTime);
		}
		system.debug(LoggingLevel.DEBUG, '---inContractInLegal is ' + inContractInLegal);
		system.debug(LoggingLevel.DEBUG, '---inContractOutLegal is ' + inContractOutLegal);
		system.debug(LoggingLevel.DEBUG, '---outContractInLegal is ' + outContractInLegal);
		system.debug(LoggingLevel.DEBUG, '---outContractOutLegal is ' + outContractOutLegal);
		system.debug(LoggingLevel.DEBUG, '---inContractInLegal nextDay is ' + inContractInLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---inContractOutLegal nextDay is ' + inContractOutLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---outContractInLegal nextDay is ' + outContractInLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---outContractOutLegal nextDay is ' + outContractOutLegal_nextDay);

		day.output.legalInTime = 0;
		day.output.legalOutTime = 0;
		Integer compensatoryDayTime = day.input.compensatoryDayTime;

		// 代休発生時間と相殺しながら勤務時間の出力を行う
		OutputWorkTimeParam param = new OutputWorkTimeParam(compensatoryDayTime, day.input.inputRestTime);
		outputWorkTime(day, day, inContractInLegal, false, false, param);
		outputWorkTime(day, dayNext, inContractInLegal_nextDay, false, false, param);
		outputWorkTime(day, day, inContractOutLegal, false, true, param);
		outputWorkTime(day, dayNext, inContractOutLegal_nextDay, false, true, param);
		outputWorkTime(day, day, outContractOutLegal, true, true, param);
		outputWorkTime(day, dayNext, outContractOutLegal_nextDay, true, true, param);
		outputWorkTime(day, day, outContractInLegal, true, false, param);
		outputWorkTime(day, dayNext, outContractInLegal_nextDay, true, false, param);

		day.output.breakTime += Math.max(0, day.input.inputRestTime - param.inputRestTime);

		Boolean withStartEndTime = (day.input.inputStartTime != null || day.input.inputEndTime != null);
		day.output.realWorkDay = (withStartEndTime ? 1 : 0);
		day.output.legalHolidayWorkCount = (day.isLegalHoliday() && withStartEndTime ? 1 : 0);
		day.output.holidayWorkCount = (day.isHoliday() && withStartEndTime ? 1 : 0);

		// 勤務時間内訳(翌日分とマージする)
		day.output.inContractInLegalRanges = inContractInLegal.insertAndMerge(inContractInLegal_nextDay);
		day.output.inContractOutLegalRanges = inContractOutLegal.insertAndMerge(inContractOutLegal_nextDay);
		day.output.outContractInLegalRanges = outContractInLegal.insertAndMerge(outContractInLegal_nextDay);
		day.output.outContractOutLegalRanges = outContractOutLegal.insertAndMerge(outContractOutLegal_nextDay);
		System.debug(LoggingLevel.DEBUG, '---day.output after calced is ' + day.output);
		System.debug(LoggingLevel.DEBUG, '---AttCalcFlexLogic.calcAttendanceDaily start');
		return true;
	}

	// 週単位勤怠計算処理(フレックスタイム制)
	public override List<Date> calcAttendanceWeekly(Date d) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcFlexLogic.calcAttendanceWeekly start');
		System.debug(LoggingLevel.DEBUG, '---target Date is ' + d);
		Integer daysInPeriod = getDayCountWeekly(d); // 実際は月次の日数
		List<Date> updateDateList = new List<Date>();
		Day[] dayList = new Day[daysInPeriod];
		for (Integer i = 0; i < daysInPeriod; i++) {
			dayList[i] = db.get(d.addDays(i));
		}
		Integer legalWorkTimeSum = 0;
		Integer noPaidAllowanceTimeSum = 0;
		Integer convertedWorkTimeSum = 0;
		for (Integer i = 0; i < daysInPeriod; i++) {
			if (dayList[i] == null) {
				continue;
			}
			convertedWorkTimeSum += dayList[i].output.convertedWorkTime; // 勤務換算時間
			dayList[i].output.convertedWorkTimeSum = convertedWorkTimeSum; // 勤務換算時間合計
		}
		for (Integer i = 0; i < daysInPeriod; i++){
			if (dayList[i] == null) {
				continue;
			}
			Integer contractedWorkTimeMonthly = input.getConfig(dayList[i].input.targetDate).contractedWorkTimeMonthly;
			Integer legalWorkTimeMonthly = input.getConfig(dayList[i].input.targetDate).legalWorkTimeMonthly;
			Integer legalInTimeTotalUntilPreDay = legalWorkTimeSum; // 前日までの労基基準内時間合計
			Integer regularRateWorkTimeTotalUntilPreDay = noPaidAllowanceTimeSum; // 前日までの割増未支給労働時間合計
			legalWorkTimeSum += dayList[i].output.legalInTime; // 労基基準内時間
			noPaidAllowanceTimeSum += dayList[i].output.regularRateWorkTime; // 割増未支給労働時間
			Integer additionalAllowanceTime = 0; // 割増手当追加発生時間
			Integer additionalOutContractTime = 0; // 追加所定外勤務発生時間
			if (noPaidAllowanceTimeSum > contractedWorkTimeMonthly - convertedWorkTimeSum) {
				additionalOutContractTime = noPaidAllowanceTimeSum - Math.max(regularRateWorkTimeTotalUntilPreDay, contractedWorkTimeMonthly - convertedWorkTimeSum);
			}
			if (noPaidAllowanceTimeSum > legalWorkTimeMonthly) {
				additionalAllowanceTime = noPaidAllowanceTimeSum - Math.max(regularRateWorkTimeTotalUntilPreDay, legalWorkTimeMonthly);
			}
			Integer regularOverTime = 0;
			if (legalWorkTimeSum > legalWorkTimeMonthly) {
				regularOverTime = legalWorkTimeSum - Math.max(legalInTimeTotalUntilPreDay, legalWorkTimeMonthly);
			}
			// 変更があったかどうかのチェック
			if (dayList[i].output.regularOverTime != regularOverTime
					|| dayList[i].output.additionalAllowanceTime != additionalAllowanceTime
					|| dayList[i].output.additionalOutContractTime != additionalOutContractTime
					|| dayList[i].output.legalWorkTimeSum != legalWorkTimeSum
					|| dayList[i].output.noPaidAllowanceTimeSum != noPaidAllowanceTimeSum) {
				dayList[i].output.regularOverTime = regularOverTime;
				dayList[i].output.additionalAllowanceTime = additionalAllowanceTime;
				dayList[i].output.additionalOutContractTime = additionalOutContractTime;
				dayList[i].output.legalWorkTimeSum = legalWorkTimeSum;
				dayList[i].output.noPaidAllowanceTimeSum = noPaidAllowanceTimeSum;
				updateDateList.add(dayList[i].input.targetDate);
			}
		}
		System.debug(LoggingLevel.DEBUG, '---day list calced is ' + dayList);
		System.debug(LoggingLevel.DEBUG, '---AttCalcFlexLogic.calcAttendanceWeekly end');
		return updateDateList;
	}
	// 月次勤怠計算処理(フレックス時間制)
	public override void calcAttendanceMonthly(Date d) {
		Date startDate = input.getStartDateMonthly(d);
		Date endDate = input.getEndDateMonthly(d);
		TotalOutput totalOutput = new TotalOutput();
		for (Date dt = startDate; dt <= endDate; dt = dt.addDays(1)) {
			Day day = db.get(dt);
			if (day == null) {
				continue;
			}
			totalOutput.realWorkTime += day.output.realWorkTime;
			totalOutput.convertedWorkTime += day.output.convertedWorkTime;
			totalOutput.unpaidLeaveLostTime += day.output.unpaidLeaveLostTime;
			totalOutput.paidLeaveTime += day.output.paidLeaveTime;
			totalOutput.unpaidLeaveTime += day.output.unpaidLeaveTime;
			totalOutput.lateArriveTime += day.output.lateArriveTime;
			totalOutput.earlyLeaveTime += day.output.earlyLeaveTime;
			totalOutput.breakTime += day.output.breakTime;
			totalOutput.lateArriveLostTime += day.output.lateArriveLostTime;
			totalOutput.earlyLeaveLostTime += day.output.earlyLeaveLostTime;
			totalOutput.breakLostTime += day.output.breakLostTime;
			totalOutput.authorizedLateArriveTime += day.output.authorizedLateArriveTime;
			totalOutput.authorizedEarlyLeaveTime += day.output.authorizedEarlyLeaveTime;
			totalOutput.authorizedBreakTime += day.output.authorizedBreakTime;
			totalOutput.paidBreakTime += day.output.paidBreakTime;
			totalOutput.authorizedOffTime += day.output.authorizedOffTime;
			totalOutput.legalOutTime += day.output.legalOutTime + day.output.regularOverTime;
			totalOutput.legalInTime += day.output.legalInTime - day.output.regularOverTime;
			totalOutput.regularRateWorkTime += day.output.regularRateWorkTime;
			totalOutput.realWorkDay += day.output.realWorkDay;
			totalOutput.workDay += day.output.workDay;
			totalOutput.legalHolidayWorkCount += day.output.legalHolidayWorkCount;
			totalOutput.holidayWorkCount += day.output.holidayWorkCount;
			for (WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
				Integer work = day.output.workTimeCategoryMap.get(c);
				Integer sum = totalOutput.workTimeCategoryMap.containsKey(c)
									? totalOutput.workTimeCategoryMap.get(c)
									: 0;
				totalOutput.workTimeCategoryMap.put(c, sum + work);
			}
			for (Integer j = 0; j < day.output.customWorkTimes.size(); j++)
				totalOutput.customWorkTimes[j] += day.output.customWorkTimes[j];
		}
		output.updateTotal(startDate, totalOutput);
	}

	public override void calcAttendanceCustom(Day day) {
		for(Integer i = 0; i < day.output.customWorkTimes.size(); i++)
			day.output.customWorkTimes[i] = 0;
		for(WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
			Integer work = day.output.workTimeCategoryMap.get(c);
			if (c.dayType == AttCalcAutoHolidayLogic.DayType.LegalHoliday) {
				if (c.isContractOut) {
					day.output.customWorkTimes[2] += work;
				}
				else {
					day.output.customWorkTimes[4] += work;
				}
			}
			else if (c.isLegalOut) {
				day.output.customWorkTimes[1] += work;
			}
			if(c.isNight)
				day.output.customWorkTimes[5] += work;
			if(c.isCompensatory)
				day.output.customWorkTimes[6] += work;
		}
		Integer allowanceOffseted = Math.min(day.output.additionalOutContractTime, day.output.additionalAllowanceTime);
		day.output.customWorkTimes[0] += day.output.additionalOutContractTime - allowanceOffseted;
		day.output.customWorkTimes[1] += allowanceOffseted;
		day.output.customWorkTimes[3] += day.output.additionalAllowanceTime - allowanceOffseted;
	}
	// 労働時間出力引数
	private class OutputWorkTimeParam {
		OutputWorkTimeParam(Integer compensatorTime, Integer inputRestTime) {
			this.compensatoryTime = compensatoryTime == null ? 0 : compensatoryTime;
			this.inputRestTime = inputRestTime == null ? 0 : inputRestTime;
		}
		public Integer compensatoryTime; // 代休発生時間
		public Integer inputRestTime; // 取得休憩時間
	}
	// 労働時間出力
	private void outputWorkTime(Day outputDay, Day adjustmentDay, AttCalcRangesDto workTimeRanges, boolean isContractOut, boolean isLegalOut, OutputWorkTimeParam param) {
		if (workTimeRanges.total() > 0) {
			// フレックス対象外の所定外
			Boolean isContractOutHolidayWork = adjustmentDay.isHoliday() && !input.getConfig(adjustmentDay.getDate()).isIncludeHolidayWorkInPlainTime;
			isLegalOut = outputDay.isLegalHoliday() || adjustmentDay.isLegalHoliday() || isContractOutHolidayWork;
			isContractOut = outputDay.isLegalHoliday() || (adjustmentDay.isLegalHoliday() && isContractOut) || isContractOutHolidayWork;

			// ------------------
			// 日中の勤務時間を計算する
			// ------------------
			Integer dayTime = workTimeRanges.include(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			Integer dayRestTime = Math.min(param.inputRestTime, dayTime);
			param.inputRestTime -= dayRestTime;
			dayTime -= dayRestTime;
			// 所定外で代休発生時間がある場合は、代休ありの勤務時間を計算する
			Integer dayCompensatoryTime = isContractOut ? Math.min(dayTime, param.compensatoryTime) : 0;
			if (dayCompensatoryTime > 0) {
				outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, true), dayCompensatoryTime);
			}
			// 残りの時間を出力する
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, false), dayTime - dayCompensatoryTime);
			// 代休と相殺した時間を差し引く
			param.compensatoryTime -= dayCompensatoryTime;

			// ------------------
			// 夜間の勤務時間を計算する
			// ------------------
			Integer nightTime = workTimeRanges.exclude(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			Integer inputRestTimeNight = Math.min(param.inputRestTime, nightTime);
			param.inputRestTime -= inputRestTimeNight;
			nightTime -= inputRestTimeNight;
			// 所定外で代休発生時間がある場合は、代休ありの勤務時間を計算する
			Integer nightCompensatoryTime = isContractOut ? Math.min(nightTime, param.compensatoryTime) : 0;
			if (nightCompensatoryTime > 0) {
				Integer t = getOrDefaultTime(outputDay.output.workTimeCategoryMap, new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, true));
				outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, true), nightCompensatoryTime + t);
			}
			// 残りの時間を出力する
			Integer t = getOrDefaultTime(outputDay.output.workTimeCategoryMap, new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, false));
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, false), nightTime - nightCompensatoryTime + t);
			// 代休と相殺した時間を差し引く
			param.compensatoryTime -= nightCompensatoryTime;

			if (!adjustmentDay.isLegalHoliday()) {
				outputDay.output.legalInTime += (dayTime + nightTime);
				if (!isLegalOut) {
					outputDay.output.regularRateWorkTime += (dayTime + nightTime);
				}
			}
		}
	}
	// 空の初期値を返せる処理
	private AttCalcRangesDto getOrDefaultRanges(
			Map<AttCalcOverTimeFlexLogic.OverTimeCategory,
			AttCalcRangesDto> rangesMap, AttCalcOverTimeFlexLogic.OverTimeCategory key) {
		if (rangesMap.containsKey(key)) {
			return rangesMap.get(key);
		}
		return AttCalcRangesDto.RS();
	}
	// 存在しない労働時間を0を返す処理
	private Integer getOrDefaultTime(
			Map<WorkTimeCategory, Integer> workTimeCategoryMap,
			WorkTimeCategory key) {
		return workTimeCategoryMap.containsKey(key)
				? workTimeCategoryMap.get(key)
				: 0;
	}
}