/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description アクセス権限のリポジトリ(PermissionRepository)のテスト
 */
@isTest
private class PermissionRepositoryTest {

	/** テストデータ */
	private class RepoTestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;

		public User userObj;

		/** コンストラクタ　*/
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
			userObj = ComTestDataUtility.createUser('ExeUser', 'ja', UserInfo.getLocale());
		}

		/** 会社オブジェクトを作成する */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}

		/** 言語設定を更新する */
		public void updateOrgLang(String lang0, String lang1, String lang2) {
			ComOrgSetting__c org = [SELECT Id, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c LIMIT 1];
			org.Language_0__c = lang0;
			org.Language_1__c = lang1;
			org.Language_2__c = lang2;
			update org;
		}

		/** 作成会社の権限を作成する */
		public List<ComPermission__c> createPermissions(String code, Integer size) {
			return ComTestDataUtility.createPermissions(code, this.companyObj.id, size);
		}

		/** 権限を作成する */
		public List<ComPermission__c> createPermissions(String code, Id companyId, Integer size) {
			return ComTestDataUtility.createPermissions(code, companyId, size);
		}

		/** 権限の論理削除を行う */
		public ComPermission__c logicalDelete(ComPermission__c deleteTarget) {
			deleteTarget.Removed__c = true;
			update deleteTarget;
			return deleteTarget;
		}
	}

		/**
	 * @description getEntityByIdのテスト
	 * getEntityByIdメソッドで正しく取得できることを検証する
	 */
	@isTest static void getEntityByIdTest() {
		PermissionRepository testRepo = new PermissionRepository();
		// テストデータ作成
		RepoTestData testData = new RepoTestData();
		List<ComPermission__c> expPermissionList = testData.createPermissions('ByIdTest', 1);
		ComPermission__c expPermission = expPermissionList.get(0);
		// 実行
		Test.startTest();
		PermissionEntity searchedPermission = testRepo.getEntityById(expPermission.id);
		Test.stopTest();
		// 検証
		assertPermissionEntity(expPermission, searchedPermission);
	}

	/**
	 * @description getEntityListByCodeのテスト
	 * getEntityListByCodeメソッドで正しく取得できることを検証する
	 */
	@isTest static void getEntityListByCodeTest() {
		PermissionRepository testRepo = new PermissionRepository();
		// テストデータ作成
		RepoTestData testData = new RepoTestData();
		List<ComPermission__c> permissionList = testData.createPermissions('ByCodeTest', 3);
		List<ComPermission__c> expPermissionList = new List<ComPermission__c>{permissionList.get(0), permissionList.get(1)};
		// 検索前準備
		List<String> permissionCodes = new List<String>{expPermissionList.get(0).Code__c, expPermissionList.get(1).Code__c};
		Test.startTest();
		List<PermissionEntity> searchedPermissionList = testRepo.getEntityListByCode(permissionCodes, testData.companyObj.id);
		Test.stopTest();
		// 検証
		System.assertEquals(expPermissionList.size(), searchedPermissionList.size());
		if (searchedPermissionList.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			for (ComPermission__c exp : expPermissionList) {
				PermissionEntity actEntity = null;
				for (PermissionEntity act : searchedPermissionList) {
					if (exp.Code__c == act.code) {
						actEntity = act;
					}
				}
				assertPermissionEntity(exp, actEntity);
			}
		}
	}

	/**
	 * @description searchEntityListのテスト
	 * searchEntityListメソッドで正しく取得できることを検証する
	 */
	@isTest static void searchEntityListTest() {
		PermissionRepository testRepo = new PermissionRepository();
		// テストデータ作成
		RepoTestData testData = new RepoTestData();
		List<ComPermission__c> permissionList = testData.createPermissions('getListTest', 3);
		List<ComPermission__c> expPermissionList = new List<ComPermission__c>{permissionList.get(0), permissionList.get(1)};
		// 検索前準備
		PermissionRepository.SearchFilter filter = new PermissionRepository.SearchFilter();
		filter.permissionCodes = new List<String>{expPermissionList.get(0).Code__c, expPermissionList.get(1).Code__c};
		Test.startTest();
		List<PermissionEntity> searchedPermissionList = testRepo.searchEntityList(filter);
		Test.stopTest();
		// 検証
		System.assertEquals(expPermissionList.size(), searchedPermissionList.size());
		if (searchedPermissionList.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			for (ComPermission__c exp : expPermissionList) {
				PermissionEntity actEntity = null;
				for (PermissionEntity act : searchedPermissionList) {
					if (exp.Code__c == act.code) {
						actEntity = act;
					}
				}
				assertPermissionEntity(exp, actEntity);
			}
		}
	}

	/**
	 * @description searchEntityのテスト
	 * 検索条件を指定せずに検索が正常終了するパターンを検証する
	 */
	@isTest static void searchEntityNoFilterTest() {
		PermissionRepository testRepo = new PermissionRepository();
		// テストデータ作成
		RepoTestData testData = new RepoTestData();
		List<ComPermission__c> expPermissionList = testData.createPermissions('NoFilter', 2);

		// 検索前準備
		PermissionRepository.SearchFilter filter = new PermissionRepository.SearchFilter();
		Boolean isForUpdate = false;

		Test.startTest();
		List<PermissionEntity> searchedPermissionList = testRepo.searchEntity(filter, isForUpdate);
		Test.stopTest();

		// 検証
		System.assertEquals(expPermissionList.size(), searchedPermissionList.size());
		if (searchedPermissionList.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			for (ComPermission__c exp : expPermissionList) {
				PermissionEntity actEntity = null;
				for (PermissionEntity act : searchedPermissionList) {
					if (exp.id == act.id) {
						actEntity = act;
					}
				}
				assertPermissionEntity(exp, actEntity);
			}
		}
	}

	/**
	 * @description searchEntityのテスト
	 * 存在しないレコードの検索が正常終了することを検証する
	 */
	@isTest static void searchEntityNotFoundTest() {
		PermissionRepository testRepo = new PermissionRepository();
		// テストデータ作成
		RepoTestData testData = new RepoTestData();
		List<ComPermission__c> permissionList = testData.createPermissions('NotFound', 1);
		String invalidCode = permissionList.get(0).Code__c + 'invalidCode';
		// 検索前準備
		PermissionRepository.SearchFilter filter = new PermissionRepository.SearchFilter();
		filter.permissionCodes = new List<String>{invalidCode};
		Boolean isForUpdate = false;
		Test.startTest();
		List<PermissionEntity> searchedPermissionList = testRepo.searchEntity(filter, isForUpdate);
		Test.stopTest();
		// 検証
		System.assert(searchedPermissionList.isEmpty());
	}

	/**
	 * @description searchEntityのテスト
	 * 複数件該当する検索を行い正しく値を取得できることを検証する
	 */
	@isTest static void searchEntityNormalTest() {
		PermissionRepository testRepo = new PermissionRepository();
		// テストデータ作成
		RepoTestData testData = new RepoTestData();
		List<ComPermission__c> expPermissionList = testData.createPermissions('normaltest', 2);
		ComPermission__c expPermission1 = expPermissionList.get(0);
		ComPermission__c expPermission2 = expPermissionList.get(1);
		// 検索前準備
		PermissionRepository.SearchFilter filter = new PermissionRepository.SearchFilter();
		filter.permissionIds = new List<Id>{expPermission1.id, expPermission2.id};
		Boolean isForUpdate = false;
		Test.startTest();
		List<PermissionEntity> searchedPermissionList = testRepo.searchEntity(filter, isForUpdate);
		Test.stopTest();
		// 検証
		System.assertEquals(expPermissionList.size(), searchedPermissionList.size());
		if (searchedPermissionList.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			assertPermissionEntity(expPermission1, searchedPermissionList.get(0));
			assertPermissionEntity(expPermission2, searchedPermissionList.get(1));
		}
	}

	/**
	 * @description searchEntityのテスト
	 * 各検索条件を使って有効に検索できるかどうかを検証する
	 */
	@isTest static void searchEntityFilterTest() {
		PermissionRepository testRepo = new PermissionRepository();
		// テストデータ作成
		RepoTestData testData = new RepoTestData();
		List<ComPermission__c> expPermissionList = testData.createPermissions('filtertest', 10);
		expPermissionList.addAll(testData.createPermissions('filtertest', testData.createCompany('another').id, 1));
		expPermissionList.set(7, testData.logicalDelete(expPermissionList.get(7)));
		Boolean isForUpdate = false;

		// 検索条件 会社ID
		// 検索前準備
		PermissionRepository.SearchFilter filterCompany = new PermissionRepository.SearchFilter();
		filterCompany.companyId = expPermissionList.get(10).CompanyId__c;
		List<PermissionEntity> searchedPermissionList1 = testRepo.searchEntity(filterCompany, isForUpdate);
		// 検証
		if (searchedPermissionList1.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			System.assertEquals(1, searchedPermissionList1.size());
			assertPermissionEntity(expPermissionList.get(10), searchedPermissionList1.get(0));
		}

		// 検索条件 権限ID（複数)
		PermissionRepository.SearchFilter filterId = new PermissionRepository.SearchFilter();
		filterId.permissionIds = new List<Id>{expPermissionList.get(1).id};
		List<PermissionEntity> searchedPermissionList2 = testRepo.searchEntity(filterId,isForUpdate);
		// 検証
		if (searchedPermissionList2.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			System.assertEquals(1, searchedPermissionList2.size());
			assertPermissionEntity(expPermissionList.get(1),searchedPermissionList2.get(0));
		}

		// 検索条件 権限コード（複数)
		PermissionRepository.SearchFilter filterCode = new PermissionRepository.SearchFilter();
		filterCode.permissionCodes = new List<String>{expPermissionList.get(0).Code__c};
		filterCode.companyId = expPermissionList.get(0).CompanyId__c;
		List<PermissionEntity> searchedPermissionList3 = testRepo.searchEntity(filterCode,isForUpdate);
		// 検証
		if (searchedPermissionList3.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			System.assertEquals(1, searchedPermissionList3.size());
			assertPermissionEntity(expPermissionList.get(0), searchedPermissionList3.get(0));
		}

		// 検索条件 削除フラグ
		PermissionRepository.SearchFilter filterIncludeRemoved = new PermissionRepository.SearchFilter();
		filterIncludeRemoved.includeRemoved = true;
		filterIncludeRemoved.permissionIds = new List<Id>{expPermissionList.get(7).id};
		List<PermissionEntity> searchedPermissionList4 = testRepo.searchEntity(filterIncludeRemoved,isForUpdate);
		// 検証
		if (searchedPermissionList4.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			System.assertEquals(1, searchedPermissionList4.size());
			assertPermissionEntity(expPermissionList.get(7), searchedPermissionList4.get(0));
		}
	}

	/**
	 * @description searchEntityのテスト
	 * 多言語名称が正しく取得できていることを検証する
	 */
	@isTest static void searchEntityMultiLangTest() {
		PermissionRepository testRepo = new PermissionRepository();
		//ケース共通テストデータ作成
		RepoTestData testData = new RepoTestData();
		Boolean isForUpdate = false;

		//Case1: L0が正しく取得できる
		//テストデータ作成
		// - 権限の作成
		ComPermission__c expPermissionL0 = testData.createPermissions('MultiLang1',1).get(0);
		PermissionRepository.SearchFilter filterL0 = new PermissionRepository.SearchFilter();
		filterL0.permissionCodes = new List<String>{expPermissionL0.Code__c};
		filterL0.companyId = expPermissionL0.CompanyId__c;
		List<PermissionEntity> searchedPermissionListL0 = testRepo.searchEntity(filterL0, isForUpdate);
		// 検証
		if (searchedPermissionListL0.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			System.assertEquals(1, searchedPermissionListL0.size());
			assertPermissionEntity(expPermissionL0, searchedPermissionListL0.get(0));
		}

		//Case1: L1が正しく取得できる
		// - 設定の更新　多言語検証用
		testData.updateOrgLang(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		//テストデータ作成
		// - 権限の作成
		ComPermission__c expPermissionL1 = testData.createPermissions('MultiLang2',1).get(0);
		PermissionRepository.SearchFilter filterL1 = new PermissionRepository.SearchFilter();
		filterL1.permissionCodes = new List<String>{expPermissionL1.Code__c};
		filterL1.companyId = expPermissionL1.CompanyId__c;
		List<PermissionEntity> searchedPermissionListL1 = testRepo.searchEntity(filterL1, isForUpdate);
		// 検証
		if (searchedPermissionListL1.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			System.assertEquals(1, searchedPermissionListL1.size());
			assertPermissionEntity(expPermissionL1, searchedPermissionListL1.get(0));
		}

		//Case3: L2が正しく取得できる
		// - 設定の更新　多言語検証用
		testData.updateOrgLang(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		//テストデータ作成
		// - 権限の作成
		ComPermission__c expPermissionL2 = testData.createPermissions('MultiLang3',1).get(0);
		PermissionRepository.SearchFilter filterL2 = new PermissionRepository.SearchFilter();
		filterL2.permissionCodes = new List<String>{expPermissionL2.Code__c};
		filterL2.companyId = expPermissionL2.CompanyId__c;
		List<PermissionEntity> searchedPermissionListL2 = testRepo.searchEntity(filterL2, isForUpdate);
		// 検証
		if (searchedPermissionListL2.isEmpty()) {
			TestUtil.fail('searchEntityでデータの取得に失敗しました');
		} else {
			System.assertEquals(1, searchedPermissionListL2.size());
			assertPermissionEntity(expPermissionL2, searchedPermissionListL2.get(0));
		}
	}

	/**
	 * 検索で取得した権限の各項目の値が期待値に一致することを確認する
	 */
	private static void assertPermissionEntity(ComPermission__c expObj, PermissionEntity actEntity) {
		assertPermissionEntity(expObj, actEntity, null);
	}
	private static void assertPermissionEntity(ComPermission__c expObj, PermissionEntity actEntity, ComOrgSetting__c org) {
		if (actEntity == null && expObj != null) {
			TestUtil.fail('該当するデータがありません');
		}

		// 検証
		System.assertEquals(expObj.id, actEntity.id);
		// 多言語対応項目の検証
		if (org != null) {
			if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_0__c)) {
				System.assertEquals(expObj.Name_L0__c, actEntity.nameL.getValue());
			} else if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_1__c)) {
				System.assertEquals(expObj.Name_L1__c, actEntity.nameL.getValue());
			} else if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_2__c)) {
				System.assertEquals(expObj.Name_L2__c, actEntity.nameL.getValue());
			}
		} else {
			System.assertEquals(expObj.Name_L0__c, actEntity.nameL.getValue());
		}
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);

		// 勤怠トランザクション系権限の検証
		System.assertEquals(expObj.ViewAttTimeSheetByDelegate__c, actEntity.isViewAttTimeSheetByDelegate);
		System.assertEquals(expObj.EditAttTimeSheetByDelegate__c, actEntity.isEditAttTimeSheetByDelegate);

		// 工数トランザクション系の権限
		System.assertEquals(expObj.ViewTimeTrackByDelegate__c, actEntity.isViewTimeTrackByDelegate);

		// 勤怠申請系権限の検証
		System.assertEquals(expObj.ApproveAttDailyRequestByDelegate__c, actEntity.isApproveAttDailyRequestByDelegate);
		System.assertEquals(expObj.ApproveAttRequestByDelegate__c, actEntity.isApproveAttRequestByDelegate);
		System.assertEquals(expObj.ApproveSelfAttDailyRequestByEmployee__c, actEntity.isApproveSelfAttDailyRequestByEmployee);
		System.assertEquals(expObj.ApproveSelfAttRequestByEmployee__c, actEntity.isApproveSelfAttRequestByEmployee);
		System.assertEquals(expObj.CancelAttApprovalByDelegate__c, actEntity.isCancelAttApprovalByDelegate);
		System.assertEquals(expObj.CancelAttApprovalByEmployee__c, actEntity.isCancelAttApprovalByEmployee);
		System.assertEquals(expObj.CancelAttDailyApprovalByDelegate__c, actEntity.isCancelAttDailyApprovalByDelegate);
		System.assertEquals(expObj.CancelAttDailyApprovalByEmployee__c, actEntity.isCancelAttDailyApprovalByEmployee);
		System.assertEquals(expObj.CancelAttDailyRequestByDelegate__c, actEntity.isCancelAttDailyRequestByDelegate);
		System.assertEquals(expObj.CancelAttRequestByDelegate__c, actEntity.isCancelAttRequestByDelegate);
		System.assertEquals(expObj.SubmitAttDailyRequestByDelegate__c, actEntity.isSubmitAttDailyRequestByDelegate);
		System.assertEquals(expObj.SubmitAttRequestByDelegate__c, actEntity.isSubmitAttRequestByDelegate);

		// 管理系権限の検証
		System.assertEquals(expObj.ManageAttAgreementAlertSetting__c, actEntity.isManageAttAgreementAlertSetting);
		System.assertEquals(expObj.ManageAttLeave__c, actEntity.isManageAttLeave);
		System.assertEquals(expObj.ManageAttLeaveGrant__c, actEntity.isManageAttLeaveGrant);
		System.assertEquals(expObj.ManageAttLeaveOfAbsence__c, actEntity.isManageAttLeaveOfAbsence);
		System.assertEquals(expObj.ManageAttLeaveOfAbsenceApply__c, actEntity.isManageAttLeaveOfAbsenceApply);
		System.assertEquals(expObj.ManageAttPattern__c, actEntity.isManageAttPattern);
		System.assertEquals(expObj.ManageAttShortTimeWorkSetting__c, actEntity.isManageAttShortTimeWorkSetting);
		System.assertEquals(expObj.ManageAttShortTimeWorkSettingApply__c, actEntity.isManageAttShortTimeWorkSettingApply);
		System.assertEquals(expObj.ManageAttWorkingType__c, actEntity.isManageAttWorkingType);
		System.assertEquals(expObj.ManageCalendar__c, actEntity.isManageCalendar);
		System.assertEquals(expObj.ManageDepartment__c, actEntity.isManageDepartment);
		System.assertEquals(expObj.ManageEmployee__c, actEntity.isManageEmployee);
		System.assertEquals(expObj.ManageJob__c, actEntity.isManageJob);
		System.assertEquals(expObj.ManageJobType__c, actEntity.isManageJobType);
		System.assertEquals(expObj.ManageMobileSetting__c, actEntity.isManageMobileSetting);
		System.assertEquals(expObj.ManageOverallSetting__c, actEntity.isManageOverallSetting);
		System.assertEquals(expObj.ManagePermission__c, actEntity.isManagePermission);
		System.assertEquals(expObj.ManagePlannerSetting__c, actEntity.isManagePlannerSetting);
		System.assertEquals(expObj.ManageTimeSetting__c, actEntity.isManageTimeSetting);
		System.assertEquals(expObj.ManageTimeWorkCategory__c, actEntity.isManageTimeWorkCategory);
		System.assertEquals(expObj.SwitchCompany__c, actEntity.isSwitchCompany);
		System.assertEquals(expObj.ManageExpTypeGroup__c, actEntity.isManageExpTypeGroup);
		System.assertEquals(expObj.ManageExpType__c, actEntity.isManageExpType);
		System.assertEquals(expObj.ManageExpTaxType__c, actEntity.isManageExpTaxType);
		System.assertEquals(expObj.ManageExpSetting__c, actEntity.isManageExpSetting);
		System.assertEquals(expObj.ManageExpExchangeRate__c, actEntity.isManageExpExchangeRate);
		System.assertEquals(expObj.ManageExpAccountingPeriod__c, actEntity.isManageExpAccountingPeriod);
		System.assertEquals(expObj.ManageExpReportType__c, actEntity.isManageExpReportType);
		System.assertEquals(expObj.ManageExpCostCenter__c, actEntity.isManageExpCostCenter);
		System.assertEquals(expObj.ManageExpVendor__c, actEntity.isManageExpVendor);
		System.assertEquals(expObj.ManageExpExtendedItem__c, actEntity.isManageExpExtendedItem);
		System.assertEquals(expObj.ManageExpEmployeeGroup__c, actEntity.isManageExpEmployeeGroup);
		System.assertEquals(expObj.ManageExpCustomHint__c, actEntity.isManageExpCustomHint);
	}

	/**
	 * @description saveEntityのテスト
	 * 保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		PermissionRepository testRepo = new PermissionRepository();

		//ケース共通テストデータ作成
		RepoTestData testData = new RepoTestData();

		// 保存データ
		PermissionEntity newPermission = new PermissionEntity();
		newPermission.companyId = testData.companyObj.Id;
		newPermission.name = 'Save Test';
		newPermission.nameL0 = 'Save Test L0';
		newPermission.code = 'SaveTest';
		newPermission.uniqKey = testData.companyObj.Code__c + '-' + 'SaveTest';
		newPermission.isViewAttTimeSheetByDelegate = true;
		newPermission.isCancelAttApprovalByDelegate = true;
		newPermission.isManageAttAgreementAlertSetting = true;
		newPermission.isManagePlannerSetting = true;
		newPermission.isViewTimeTrackByDelegate = true;
		newPermission.isManageExpCustomHint = true;

		// 保存する
		Test.startTest();
		Repository.SaveResult result = new PermissionRepository().saveEntity(newPermission);
		Test.stopTest();

		// 結果確認
		// 自動生成されている項目の確認は省略
		System.assertEquals(true, result.isSuccessAll);
		ComPermission__c resultObj = getPermissionObj(result.details[0].id);
		System.assertNotEquals(null, resultObj);
		assertSavedObject(newPermission, resultObj);
	}

	/**
	 * 各項目が保存できていることを確認する
	 * @param expEntity 期待値
	 * @param actObj 保存結果のオブジェクト
	 */
	private static void assertSavedObject(PermissionEntity expEntity, ComPermission__c actObj) {

		// エンティティが自動生成されているため、全ての項目の確認は不要iqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.isViewAttTimeSheetByDelegate, actObj.ViewAttTimeSheetByDelegate__c);
		System.assertEquals(expEntity.isCancelAttApprovalByDelegate, actObj.CancelAttApprovalByDelegate__c);
		System.assertEquals(expEntity.isManageAttAgreementAlertSetting, actObj.ManageAttAgreementAlertSetting__c);
		System.assertEquals(expEntity.isManagePlannerSetting, actObj.ManagePlannerSetting__c);
		System.assertEquals(expEntity.isViewTimeTrackByDelegate, actObj.ViewTimeTrackByDelegate__c);
		System.assertEquals(expEntity.isManageExpCustomHint, actObj.ManageExpCustomHint__c);
	}

	/**
	 * オブジェクトを取得する
	 */
	private static ComPermission__c getPermissionObj(Id permissionId) {
		return [SELECT Id, Name, Name_L0__c, Code__c, UniqKey__c,
				ViewAttTimeSheetByDelegate__c, CancelAttApprovalByDelegate__c, ManageAttAgreementAlertSetting__c, ManagePlannerSetting__c, ViewTimeTrackByDelegate__c, ManageExpCustomHint__c
				FROM ComPermission__c WHERE Id = :permissionId];
	}
}