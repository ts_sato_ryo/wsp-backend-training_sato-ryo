/**
 * テスト用Utilityクラス
 */
@isTest
@TestVisible
private class TestUtil {
	public static final String TESTDOMAIN = '@test.co.jp';
	public static final String TESTPROFILE_EN = 'System Administrator';
	public static final String TESTPROFILE_JP = 'システム管理者';

	public virtual class DataBuilder {
		public Date startDate = Date.newInstance(2010,1,1);
		public Date baseDate = Date.newInstance(2010,1,5);
		public Date endDate = Date.newInstance(2010,1,31);
		public Integer yearMonth = baseDate.year()*100+baseDate.month();
		public List<User> user = new List<User>();
		public List<ComCountry__c> country = new List<ComCountry__c>();
		public List<ComCompany__c > company = new List<ComCompany__c >();

		public DataBuilder() {
		}

		public virtual void initializeUser() {
			Profile prof = [select id from profile where name=:TESTPROFILE_EN or name=:TESTPROFILE_JP];
			System.assert(prof != null);
			user.add(new User(
				alias = 'SU-152',
				email='standarduser1'+TESTDOMAIN,
				emailencodingkey='UTF-8',
				lastname='Testing1',
				languagelocalekey='ja',
				localesidkey='ja_JP',
				profileid = prof.Id,
				timezonesidkey='Asia/Tokyo',
				username='standarduser01'+TESTDOMAIN
			));
			user.add(new User(
				alias = 'T-34',
				email='standarduser2'+TESTDOMAIN,
				emailencodingkey='UTF-8',
				lastname='Testing2',
				languagelocalekey='ja',
				localesidkey='ja_JP',
				profileid = prof.Id,
				timezonesidkey='Asia/Tokyo',
				username='standarduser02'+TESTDOMAIN
			));
			user.add(new User(
				alias = 'J-20',
				email='standarduser3'+TESTDOMAIN,
				emailencodingkey='UTF-8',
				lastname='Testing3',
				languagelocalekey='ja',
				localesidkey='ja_JP',
				profileid = prof.Id,
				timezonesidkey='Asia/Tokyo',
				username='standarduser03'+TESTDOMAIN
			));
			user.add(new User(
				alias = 'Y-10',
				email='standarduser4'+TESTDOMAIN,
				emailencodingkey='UTF-8',
				lastname='Testing4',
				languagelocalekey='ja',
				localesidkey='ja_JP',
				profileid = prof.Id,
				timezonesidkey='Asia/Tokyo',
				username='standarduser04'+TESTDOMAIN
			));
		}

		public virtual void initializeCountry() {
			country.add(new ComCountry__c(Name='日本', Name_L0__c = '日本', Code__c='JP'));
		}

		public virtual void initializeCompany() {
			company.add(new ComCompany__c(Name='Company1', Name_L0__c='会社1', Code__c='00001', CountryId__c=country[0].Id));
		}

		public virtual void initialize() {
			initializeUser();
			if(user.size()>0) insert user;
			initializeCountry();
			if(country.size()>0)insert country;
			initializeCompany();
			if(company.size()>0) insert company;
		}

	}

	/**
	 * Calloutが含まれるBatchableのApexテスト用関数
	 *
	 * Spring 17よりApexテストでBatchableを起動すると、AsyncApexJobのDML操作後にCalloutが実行され
	 * 以下のエラーが発生するようになった問題の対応
	 * "common.apex.runtime.impl.ExecutionException: You have uncommitted work pending. Please commit or rollback before calling out"
	 *
	 * @param batch 実行するとBatchableのインスタンス
	 * @param batchSize バッチサイズ
	 */
	public static void executeCalloutBatchTest(Database.Batchable<SObject> batch, Integer batchSize) {
		Database.QueryLocator ql = (Database.QueryLocator)batch.start(null);
		List<SObject> objList = new List<SObject>();
		Database.QueryLocatorIterator it =  ql.iterator();
		while (it.hasNext()) {
			objList.add(it.next());
			if (objList.size() >= batchSize) {
				batch.execute(null, objList);
				objList.clear();
			}
		}
		if (!objList.isEmpty()) {
			batch.execute(null, objList);
		}
		batch.finish(null);
	}

	/**
	 * LastModifiedDateが正しくセットされないため前後1秒以内であればアサーションを通すようにするためのユーティリティ関数
	 * テストとして意味はないが、本来テストすべきテストケースとして残しておくために使用する
	 *
	 * @param exptected 期待する日時
	 * @param actual アサート対象のの日時
	 * return 同じかどうか
	 */
	public static Boolean equalDateTimeAmbiguously(DateTime expected, DateTime actual) {
		DateTime fromDateTime = expected.addSeconds(-1);
		DateTime toDateTime = expected.addSeconds(1);
		return fromDateTime <= expected && expected <= toDateTime;
	}

	/**
	 * テストが失敗したことをあわらす
	 */
	public static void fail(String message) {
		System.assert(false, message);
	}

	/**
	 * 値がnullでないことを検証する
	 */
	public static void assertNotNull(Object actual) {
		System.assertNotEquals(null, actual);
	}

	/**
	 * 値がnullでないことを検証する
	 * @actual 実際の値
	 * @mesasge エラーの場合に表示されるメッセージ
	 */
	public static void assertNotNull(Object actual, String message) {
		System.assertNotEquals(null, actual, message);
	}
}
