/**
  * フレックス労働制の勤怠計算ロジックのテストクラス
  */
@isTest
private class AttCalcManagerLogicTest {

	static AttCalcDto.AttConfig initAttConfig() {
		AttCalcDto.AttConfig config = new AttCalcDto.AttConfig();
		// 所定休憩範囲
		config.contractRestRanges = AttCalcRangesDto.RS(12*60,13*60);
		// 所定休憩時間
		config.contractRestTime = 0;
		// 法定労働時間
		config.legalWorkTimeDaily = 8*60; // 8h
		// 控除と残業の相殺をしない
		config.isNoOffsetOvertimeByUndertime = false;
		// 所定時間まで申請無しで許可
		config.isPermitOvertimeWorkUntilNormalHours = false;
		// 補填時間は勤務時間に加える
		config.isIncludeCompensationTimeToWorkHours = false;
		// 法定休日の所定内勤務にも基本給を払う
		config.isPayBasicSalaryLegalHolidayContractedWorkHours = false;
		// 法定休日自動判定
		config.isAutoLegalHolidayAssign = false;
		// 年度の起算月
		config.beginMonthOfYear = 1;
		// 法定休日自動決定の期間の日数
		config.legalHolidayAssignmentPeriod = 7;
		// 法定休日の日数
		config.legalHolidayAssignmentDays = 1;
		// 週の起算日の開始曜日
		config.beginDayOfWeek = 0;
		// 月の起算日
		config.beginDayOfMonth = 1;
		// 勤怠計算の暦日区分
		config.classificationNextDayWork = AttCalcLogic.ClassificationNextDayWork.None;
		// 勤務日の境界時刻
		config.boundaryTimeOfDay = 0;
		// 日中勤務時間帯
		config.workTimePeriodDaily = AttCalcRangesDto.RS(5*60,22*60);
		// 所定休日の勤務は法定内も割増を払う
		config.isPayAllowanceOnEveryHoliday = false;
		// 所定休日の勤務も法定休日と同じ割増を払う
		config.isPayLegalHolidayAllowanceOnEveryHoliday = false;
		// 法定内残業も割増を払う
		config.isPayAllowanceWhenOverContructedWorkHours = false;

		return config;
	}
	static AttCalcLogic.InputDaily createInputDaily(
			Date targetDate,
			AttCalcAutoHolidayLogic.DayType dayType,
			Integer inputStartTime,
			Integer inputEndTime) {
		AttCalcLogic.InputDaily inputDaily = new AttCalcLogic.InputDaily();
		inputDaily.targetDate = targetDate;
		inputDaily.dayType = dayType;
		inputDaily.inputStartTime = inputStartTime;
		inputDaily.inputEndTime = inputEndTime;
		inputDaily.startTime = 9*60;
		inputDaily.endTime = 18*60;
		inputDaily.contractWorkTime = 60*8;
		inputDaily.inputRestRanges = AttCalcRangesDto.RS(12*60,13*60);
		inputDaily.contractRestRanges = AttCalcRangesDto.RS(12*60,13*60);
		return inputDaily;
	}
	// 休日の勤怠計算対象を作成
	static AttCalcLogic.InputDaily createHolidayWorkInputDaily(
			AttCalcAutoHolidayLogic.DayType holidayType,
			Date targetDate,
			Integer inputStartTime,
			Integer inputEndTime) {
		AttCalcLogic.InputDaily inputDaily = new AttCalcLogic.InputDaily();
		inputDaily.targetDate = targetDate;
		inputDaily.dayType = holidayType;
		inputDaily.isLegalHoliday = holidayType == AttCalcAutoHolidayLogic.DayType.LegalHoliday;
		inputDaily.inputStartTime = inputStartTime;
		inputDaily.inputEndTime = inputEndTime;
		inputDaily.inputRestRanges = AttCalcRangesDto.RS(12*60,13*60);
		inputDaily.allowedWorkRanges = AttCalcRangesDto.RS(inputStartTime, inputEndTime);
		return inputDaily;
	}

	// 通常出退社(Manager1_1)
	@isTest static void calcAttNomal() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createInputDaily(
				startDate, AttCalcAutoHolidayLogic.DayType.WorkDay,9*60, 18*60));

		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();
		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*8, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*8, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(9*60, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(18*60, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 早朝＋残業(Manager1_2)
	@isTest static void calcAttEarlyInLateOut() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createInputDaily(
				startDate, AttCalcAutoHolidayLogic.DayType.WorkDay,8*60, 19*60));

		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*10, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*10, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(8*60, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(19*60, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 遅刻＋早退(Manager1_3)
	@isTest static void calcAttLateInEarlyOut() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createInputDaily(
				startDate, AttCalcAutoHolidayLogic.DayType.WorkDay,11*60+30, 14*60+30));

		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*2, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*2, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLateArriveTime, 0)); // 遅刻時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLateArriveLostTime, 0)); // 遅刻控除時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outEarlyLeaveTime, 0)); // 早退時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outEarlyLeaveLostTime, 0)); // 早退控除時間

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(11*60+30, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(14*60+30, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 遅刻＋残業(Manager1_4)
	@isTest static void calcAttLateInLateOut() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createInputDaily(
				startDate, AttCalcAutoHolidayLogic.DayType.WorkDay,11*60+30, 21*60));

		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*8.5, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*8.5, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLateArriveTime, 0)); // 遅刻時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLateArriveLostTime, 0)); // 遅刻控除時間

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(11*60+30, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(21*60, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 早朝＋早退(Manager1_5)
	@isTest static void calcAttEarlyInEarlyOut() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createInputDaily(
				startDate, AttCalcAutoHolidayLogic.DayType.WorkDay,5*60, 14*60+30));

		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*8.5, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*8.5, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outEarlyLeaveTime, 0)); // 早退時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outEarlyLeaveLostTime, 0)); // 早退控除時間

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(5*60, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(14*60+30, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 深夜(Manager1_6)
	@isTest static void calcAttMoreRest() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createInputDaily(
				startDate, AttCalcAutoHolidayLogic.DayType.WorkDay,9*60, 23*60));
		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*13, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*13, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.cusWorkTime06, 0)); // 深夜労働

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(9*60, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(23*60, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 所定休日出勤(Manager1_7)
	@isTest static void calcAttHoliday() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createHolidayWorkInputDaily(
				AttCalcAutoHolidayLogic.DayType.HoliDay, startDate, 8*60, 21*60));
		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*12, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*12, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.cusWorkTime06, 0)); // 深夜労働

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(8*60, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(21*60, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 法定休日出勤(Manager1_8)
	@isTest static void calcAttLegalHoliday() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(),1);
		inputDailyList.add(createHolidayWorkInputDaily(
				AttCalcAutoHolidayLogic.DayType.LegalHoliDay, startDate, 8*60, 21*60));
		calcData.setInputDayList(inputDailyList, startDate, startDate);

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		AttRecordEntity attRecordData = recordDataList[0];
		System.assertEquals(60*12, AppConverter.intValue(attRecordData.outRealWorkTime, 0)); // 実労働時間
		System.assertEquals(60*1, AppConverter.intValue(attRecordData.outRestTime, 0)); // 実休憩時間
		System.assertEquals(60*12, AppConverter.intValue(attRecordData.outLegalWorkTime, 0)); // 法定内労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.outLegalOverTime, 0)); // 法定外労働時間
		System.assertEquals(60*0, AppConverter.intValue(attRecordData.cusWorkTime06, 0)); // 深夜労働

		// 内訳検証
		AttRecordEntity.AttWorkTimeDetail detail = attRecordData.outWorkTimeDetail;
		System.assertNotEquals(null, detail);
		System.assertEquals(2, detail.CILIPeriods.size());
		System.assertEquals(0, detail.CILOPeriods.size());
		System.assertEquals(0, detail.COLIPeriods.size());
		System.assertEquals(0, detail.COLOPeriods.size());
		List<AttTimePeriod> ciliPeriods = detail.CILIPeriods;
		System.assertEquals(8*60, AppConverter.intValue(ciliPeriods[0].startTime));
		System.assertEquals(12*60, AppConverter.intValue(ciliPeriods[0].endTime));
		System.assertEquals(13*60, AppConverter.intValue(ciliPeriods[1].startTime));
		System.assertEquals(21*60, AppConverter.intValue(ciliPeriods[1].endTime));
	}
	// 出勤日数計算
	@isTest static void calcRealDays() {
		AttCalcDto.AttConfig config = initAttConfig();
		AttCalcDto.AttCalcLogicDto calcData = new AttCalcDto.AttCalcLogicDto(config);
		List<AttCalcLogic.InputDaily> inputDailyList = new List<AttCalcLogic.InputDaily>();

		Date startDate = Date.today();
		inputDailyList.add(createInputDaily(startDate, AttCalcAutoHolidayLogic.DayType.Workday, 9*60, 20*60));
		inputDailyList.add(createInputDaily(startDate.addDays(1), AttCalcAutoHolidayLogic.DayType.Workday, 9*60, null));
		inputDailyList.add(createInputDaily(startDate.addDays(2), AttCalcAutoHolidayLogic.DayType.Workday, null, 20*60));
		inputDailyList.add(createInputDaily(startDate.addDays(3), AttCalcAutoHolidayLogic.DayType.Workday, null, null));

		inputDailyList.add(createInputDaily(startDate.addDays(4), AttCalcAutoHolidayLogic.DayType.Holiday, 9*60, 20*60));
		inputDailyList.add(createInputDaily(startDate.addDays(5), AttCalcAutoHolidayLogic.DayType.Holiday,  9*60, null));
		inputDailyList.add(createInputDaily(startDate.addDays(6), AttCalcAutoHolidayLogic.DayType.Holiday, null, 20*60));
		inputDailyList.add(createInputDaily(startDate.addDays(7), AttCalcAutoHolidayLogic.DayType.Holiday, null, null));

		inputDailyList.add(createInputDaily(startDate.addDays(8), AttCalcAutoHolidayLogic.DayType.LegalHoliday, 9*60, 20*60));
		inputDailyList.add(createInputDaily(startDate.addDays(9), AttCalcAutoHolidayLogic.DayType.LegalHoliday, 9*60, null));
		inputDailyList.add(createInputDaily(startDate.addDays(10), AttCalcAutoHolidayLogic.DayType.LegalHoliday, null, 20*60));
		inputDailyList.add(createInputDaily(startDate.addDays(11), AttCalcAutoHolidayLogic.DayType.LegalHoliday, null, null));

		calcData.setInputDayList(inputDailyList, startDate, startDate.addDays(11));

		AttCalcManagerLogic managerLogic = new AttCalcManagerLogic(calcData, calcData);
		managerLogic.apply();
		List<AttRecordEntity> recordDataList = calcData.getRecordResultList();

		System.assertEquals(false, recordDataList.isEmpty());
		System.assertEquals(1, AppConverter.intValue(recordDataList[0].outRealWorkDay));
		System.assertEquals(1, AppConverter.intValue(recordDataList[1].outRealWorkDay));
		System.assertEquals(1, AppConverter.intValue(recordDataList[2].outRealWorkDay));
		System.assertEquals(0, AppConverter.intValue(recordDataList[3].outRealWorkDay));

		System.assertEquals(1, recordDataList[4].outHolidayWorkCount);
		System.assertEquals(1, recordDataList[5].outHolidayWorkCount);
		System.assertEquals(1, recordDataList[6].outHolidayWorkCount);
		System.assertEquals(0, recordDataList[7].outHolidayWorkCount);

		System.assertEquals(1, recordDataList[8].outLegalHolidayWorkCount);
		System.assertEquals(1, recordDataList[9].outLegalHolidayWorkCount);
		System.assertEquals(1, recordDataList[10].outLegalHolidayWorkCount);
		System.assertEquals(0, recordDataList[11].outLegalHolidayWorkCount);
	}
}