/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description JobEntityのテスト
 */
@isTest
private class JobEntityTest {

	/**
	 * テストデータクラス
	 */
	private class EntityTestData extends TestData.TestDataEntity {

	}

	/**
	 * エンティティの複製ができることを確認する
	 */
	@isTest static void copyTest() {

		EntityTestData testData = new EntityTestData();

		JobEntity orgEntity = new JobEntity();
		orgEntity.name = 'Testジョブ';
		orgEntity.validFrom = AppDate.today();
		orgEntity.validTo = orgEntity.validFrom.addMonths(12);
		orgEntity.code = orgEntity.name + '_code';
		orgEntity.uniqKey = orgEntity.code + '_uniqKey';
		orgEntity.companyId = testData.company.id;
		orgEntity.nameL0 = orgEntity.name + '_L0';
		orgEntity.nameL1 = orgEntity.name + '_L1';
		orgEntity.nameL2 = orgEntity.name + '_L2';
		orgEntity.parentId = UserInfo.getUserId();
		orgEntity.departmentBaseId = testData.department.id;
		orgEntity.jobOwnerBaseId = testData.employee.id;
		orgEntity.isDirectCharged = true;
		orgEntity.isScopedAssignment = true;
		orgEntity.isSelectableExpense = true;
		orgEntity.isSelectableTimeTrack = true;

		JobEntity copyEntity = orgEntity.copy();
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.validFrom, copyEntity.validFrom);
		System.assertEquals(orgEntity.validTo, copyEntity.validTo);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.nameL0, copyEntity.nameL0);
		System.assertEquals(orgEntity.nameL1, copyEntity.nameL1);
		System.assertEquals(orgEntity.nameL2, copyEntity.nameL2);
		System.assertEquals(orgEntity.parentId, copyEntity.parentId);
		System.assertEquals(orgEntity.departmentBaseId, copyEntity.departmentBaseId);
		System.assertEquals(orgEntity.jobOwnerBaseId, copyEntity.jobOwnerBaseId);
		System.assertEquals(orgEntity.isDirectCharged, copyEntity.isDirectCharged);
		System.assertEquals(orgEntity.isScopedAssignment, copyEntity.isScopedAssignment);
		System.assertEquals(orgEntity.isSelectableExpense, copyEntity.isSelectableExpense);
		System.assertEquals(orgEntity.isSelectableTimeTrack, copyEntity.isSelectableTimeTrack);
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		JobEntity entity = new JobEntity();

		// 会社コードとジョブコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'JobCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// ジョブコードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'JobCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}

	}

}
