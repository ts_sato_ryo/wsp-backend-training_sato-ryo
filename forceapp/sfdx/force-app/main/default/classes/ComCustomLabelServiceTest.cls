/**
 * @group 共通
 *
 * @description カスタムラベルサービス
 */
@isTest
private class ComCustomLabelServiceTest {

	/**
	 * デフォルトラベルが取得できることを確認する
	 */
	@isTest static void getDefaultLabelTest() {
		List<ComCustomLabel__mdt> labelList = [SELECT DeveloperName, QualifiedApiName, MasterLabel,
			En_US_Default__c, En_US_Custom__c, Ja_Default__c, Ja_Custom__c
			FROM ComCustomLabel__mdt];
		ComCustomLabel__mdt targetLabel = labelList[0];

		ComCustomLabelService service = new ComCustomLabelService();
		String defaultLabel = service.getDefaultLabel(targetLabel.DeveloperName, AppLanguage.JA);

		System.assertEquals(targetLabel.Ja_Default__c, defaultLabel);
	}

}