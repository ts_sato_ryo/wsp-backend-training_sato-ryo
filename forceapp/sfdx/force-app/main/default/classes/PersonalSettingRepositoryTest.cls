/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * PersonalSettingRepositoryのテスト
 */
@isTest
private class PersonalSettingRepositoryTest {

	/**
	 * getPersonalSettingをテストする
	 */
	@isTest static void getPersonalSettingTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id);
		List<ComJob__c> jobs = ComTestDataUtility.createLayeredJobs();
		User approver01User = ComTestDataUtility.createUser('approver01', 'ja', 'ja_JP');
		ComEmpBase__c approver01 = ComTestDataUtility.createEmployeeWithHistory('approver01', company.Id, null, approver01User.Id);

		PersonalSettingRepository repo = new PersonalSettingRepository();

		// 個人設定データを作成
		PersonalSettingEntity newSetting = new PersonalSettingEntity();
		newSetting.name = 'test';
		newSetting.employeeBaseId = emp.Id;
		newSetting.defaultJobId = jobs.get(3).Id;
		newSetting.approverBase01Id = approver01.id;

		PersonalSettingEntity.Task task1 = new PersonalSettingEntity.Task();
		newSetting.timeInputSetting.addTask('jobCode1', 'workCategoryCode1');
		newSetting.timeInputSetting.addTask('jobCode2', 'workCategoryCode2');

		PersonalSettingEntity.SearchCondition searchCondition = new PersonalSettingEntity.SearchCondition();
		searchCondition.departmentBaseIds.add('departmentBaseId1');
		searchCondition.departmentBaseIds.add('departmentBaseId2');
		searchCondition.empBaseIds.add('empBaseId1');
		searchCondition.empBaseIds.add('empBaseId2');
		List<PersonalSettingEntity.SearchCondition> searchConditionList = new List<PersonalSettingEntity.SearchCondition>();
		searchConditionList.add(searchCondition);
		newSetting.searchConditionList = searchConditionList;

		repo.saveEntityList(new List<PersonalSettingEntity>{newSetting});

		Test.startTest();
		PersonalSettingEntity setting = repo.getPersonalSetting(emp.Id);
		Test.stopTest();


		System.assertEquals(newSetting.name, setting.Name);
		System.assertEquals(newSetting.employeeBaseId, setting.employeeBaseId);
		System.assertEquals(newSetting.defaultJobId, setting.defaultJobId);
		System.assert(setting.timeInputSetting != null);
		System.assertEquals(2, setting.timeInputSetting.taskList.size());
		System.assertEquals('jobCode1', setting.timeInputSetting.taskList[0].jobCode);
		System.assertEquals('workCategoryCode1', setting.timeInputSetting.taskList[0].workCategoryCode);
		System.assertEquals('jobCode2', setting.timeInputSetting.taskList[1].jobCode);
		System.assertEquals('workCategoryCode2', setting.timeInputSetting.taskList[1].workCategoryCode);
		System.assertEquals(approver01.id, setting.approverBase01Id);
		System.assertEquals(1, setting.searchConditionList.size());
		System.assertEquals(2, setting.searchConditionList[0].departmentBaseIds.size());
		System.assertEquals(2, setting.searchConditionList[0].empBaseIds.size());
		System.assertEquals(true, setting.searchConditionList[0].departmentBaseIds.contains('departmentBaseId1'));
		System.assertEquals(true, setting.searchConditionList[0].departmentBaseIds.contains('departmentBaseId2'));
		System.assertEquals(true, setting.searchConditionList[0].empBaseIds.contains('empBaseId1'));
		System.assertEquals(true, setting.searchConditionList[0].empBaseIds.contains('empBaseId2'));
	}

	/**
	 * getPersonalSettingをテストする
	 * 社員に該当するレコードが複数存在する場合、例外が発生する事を検証する
	 */
	@isTest static void getPersonalSettingTestThrowException() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id);
		ComTestDataUtility.createPersonalSetting(emp.id, null, null);
		ComTestDataUtility.createPersonalSetting(emp.id, null, null);

		try {
			Test.startTest();
			PersonalSettingRepository repo = new PersonalSettingRepository();
			PersonalSettingEntity setting = repo.getPersonalSetting(emp.Id);
			Test.stopTest();
			System.assert(false);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_IDENTIFY_PERSONAL_SETTING, e.getErrorCode());
		}
	}

	/**
	 * saveEntityをテストする
	 */
	@isTest static void SaveEntityTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id);
		List<ComJob__c> jobs = ComTestDataUtility.createLayeredJobs();
		User approver01User = ComTestDataUtility.createUser('approver01', 'ja', 'ja_JP');
		ComEmpBase__c approver01 = ComTestDataUtility.createEmployeeWithHistory('approver01', company.Id, null, approver01User.Id);

		PersonalSettingEntity newSetting = new PersonalSettingEntity();
		newSetting.name = 'test';
		newSetting.employeeBaseId = emp.Id;
		newSetting.timeInputSetting.addTask('jobCode1', 'workCategoryCode1');
		newSetting.defaultJobId = jobs.get(3).Id;
		newSetting.approverBase01Id = approver01.id;

		PersonalSettingEntity.SearchCondition searchCondition = new PersonalSettingEntity.SearchCondition();
		searchCondition.departmentBaseIds.add('departmentBaseId1');
		searchCondition.departmentBaseIds.add('departmentBaseId2');
		searchCondition.empBaseIds.add('empBaseId1');
		searchCondition.empBaseIds.add('empBaseId2');
		List<PersonalSettingEntity.SearchCondition> searchConditionList = new List<PersonalSettingEntity.SearchCondition>();
		searchConditionList.add(searchCondition);
		newSetting.searchConditionList = searchConditionList;

		PersonalSettingRepository repo = new PersonalSettingRepository();
		Test.startTest();
		repo.saveEntity(newSetting);
		Test.stopTest();

		// 作成された個人設定データを取得
		ComPersonalSetting__c setting = [
				SELECT
					Id,
					Name,
					EmployeeBaseId__c,
					TimeInputSetting__c,
					DefaultJobId__c,
					ApproverBase01Id__c,
					FinanceApprovalSearchConditionList__c
				FROM ComPersonalSetting__c
				WHERE EmployeeBaseId__c = :emp.Id
				LIMIT 1];

		System.assertEquals(newSetting.name, setting.Name);
		System.assertEquals(newSetting.employeeBaseId, setting.EmployeeBaseId__c);
		System.assertEquals(JSON.serialize(newSetting.timeInputSetting), setting.TimeInputSetting__c);
		System.assertEquals(newSetting.defaultJobId, setting.DefaultJobId__c);
		System.assertEquals(newSetting.approverBase01Id, setting.ApproverBase01Id__c);
		System.assertEquals(JSON.serialize(newSetting.searchConditionList), setting.FinanceApprovalSearchConditionList__c);
	}

	/**
	 * createEntityのJson項目変換結果を検証する
	 */
	@isTest static void createEntityTimeSettingTest() {
		PersonalSettingRepository repository = new PersonalSettingRepository();
		{
			// 変換前：null ⇒ 変換後：empty list
			ComPersonalSetting__c sobj = new ComPersonalSetting__c(Name = 'name');
			PersonalSettingEntity entity = repository.createEntity(sobj);
			System.assert(entity.timeInputSetting.taskList.isEmpty());
		}
		{
			// 変換前：ブランク ⇒ 変換後：empty list
			ComPersonalSetting__c sobj = new ComPersonalSetting__c(Name = 'name', TimeInputSetting__c = '');
			PersonalSettingEntity entity = repository.createEntity(sobj);
			System.assert(entity.timeInputSetting.taskList.isEmpty());
		}
		{
			// 変換前：ブランクJson ⇒ 変換後：null list ※当ケースは実装上ありえない
			ComPersonalSetting__c sobj = new ComPersonalSetting__c(Name = 'name', TimeInputSetting__c = '{}');
			PersonalSettingEntity entity = repository.createEntity(sobj);
			System.assert(entity.timeInputSetting.taskList == null);
		}
		{
			// 変換前：taskList null ⇒ 変換後：empty list ※当ケースは実装上ありえない
			ComPersonalSetting__c sobj = new ComPersonalSetting__c(Name = 'name', TimeInputSetting__c = '{"taskList":null}');
			PersonalSettingEntity entity = repository.createEntity(sobj);
			System.assert(entity.timeInputSetting.taskList == null);
		}
		{
			// 変換前：taskList empty ⇒ 変換後：empty list
			ComPersonalSetting__c sobj = new ComPersonalSetting__c(Name = 'name', TimeInputSetting__c = '{"taskList":[]}');
			PersonalSettingEntity entity = repository.createEntity(sobj);
			System.assert(entity.timeInputSetting.taskList.isEmpty());
		}
		{
			// リストあり
			ComPersonalSetting__c sobj = new ComPersonalSetting__c(Name = 'name',
				TimeInputSetting__c = '{"taskList":[{"jobCode":"job1", "workCategoryCode":"wc1"},{"jobCode":"job2", "workCategoryCode":"wc2"}]}');
			PersonalSettingEntity entity = repository.createEntity(sobj);
			System.assertEquals(2, entity.timeInputSetting.taskList.size());
			System.assertEquals('job1', entity.timeInputSetting.taskList[0].jobCode);
			System.assertEquals('wc1', entity.timeInputSetting.taskList[0].workCategoryCode);
			System.assertEquals('job2', entity.timeInputSetting.taskList[1].jobCode);
			System.assertEquals('wc2', entity.timeInputSetting.taskList[1].workCategoryCode);
		}
	}

	/**
	 * createObjのJson項目変換結果を検証する
	 */
	@isTest static void createObjTimeSettingTest() {
		PersonalSettingRepository repository = new PersonalSettingRepository();
		{
			// 変換前：new ⇒ 変換後：empty list
			PersonalSettingEntity entity = new PersonalSettingEntity();
			entity.timeInputSetting = new PersonalSettingEntity.TimeInputSetting();
			ComPersonalSetting__c sobj = repository.createObj(entity);
			System.assertEquals('{"taskList":[]}', sobj.TimeInputSetting__c);
		}
		{
			// 変換前：null ⇒ 変換後：empty list
			PersonalSettingEntity entity = new PersonalSettingEntity();
			entity.timeInputSetting = null;
			ComPersonalSetting__c sobj = repository.createObj(entity);
			System.assertEquals('{"taskList":[]}', sobj.TimeInputSetting__c);
		}
		{
			// 変換前：null list ⇒ 変換後：empty list
			PersonalSettingEntity entity = new PersonalSettingEntity();
			entity.timeInputSetting.taskList = null;
			ComPersonalSetting__c sobj = repository.createObj(entity);
			System.assertEquals('{"taskList":[]}', sobj.TimeInputSetting__c);
		}
		{
			PersonalSettingEntity entity = new PersonalSettingEntity();
			entity.timeInputSetting.addTask('job1', 'wc1');
			entity.timeInputSetting.addTask('job2', null);
			ComPersonalSetting__c sobj = repository.createObj(entity);
			System.assertEquals('{"taskList":[{"workCategoryCode":"wc1","jobCode":"job1"},{"workCategoryCode":null,"jobCode":"job2"}]}', sobj.TimeInputSetting__c);
		}
	}
}