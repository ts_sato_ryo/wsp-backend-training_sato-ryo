/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Resource class providing APIs of Expense Request
 */
public with sharing class ExpRequestResource {

	/**
	 * Parameter for getting request list
	 */
	public class GetListParam implements RemoteApi.RequestParam {
		/** Status */
		public List<String> reportIds = new List<String>();

		public void validate() {
			if(reportIds.size()<=0){
				throw new App.ParameterException('reportIds', reportIds);
			}
			for(String id : reportIds){
				ExpCommonUtil.validateId('reportIds', id, true);
			}
		}
	}

	/**
	 * Parameter for getting request id list
	 */
	public class GetIdListParam implements RemoteApi.RequestParam {
		public Boolean isApproved = false;
	}

	/**
 * Parameter for getting request detail
 */
	public class GetParam implements RemoteApi.RequestParam {
		/** Report ID */
		public String reportId;

		public void validate() {
			ExpCommonUtil.validateId('reportId', reportId, true);
		}
	}

	/**
	 * Parameter for saving
	 */
	public class SaveParam extends ExpRequestService.ExpRequestBase implements RemoteApi.RequestParam {
		/** Employee ID */
		public String empId;
	}

	/**
	 * Parameter for deletion
   */
	public class DeleteParam implements RemoteApi.RequestParam {
		/** Report ID */
		public String reportId;
	}

	/**
   * Parameter for creating expense report
   */
	public class CreateReportParam implements RemoteApi.RequestParam {
		/** Employee ID */
		public String empId;
		/** Report ID */
		public String reportId;
	}

	/**
	 * Result of getting request list
	 */
	public class GetListResult implements RemoteApi.ResponseParam {
		/** Report ID */
		public List<ExpRequestService.ExpRequest> reports;
	}

	/**
	 * Result of getting request id list
	 */
	public class GetIdListResult implements RemoteApi.ResponseParam {
		/** Report ID List */
		public List<Id> reportIdList = new List<Id>();
		/** total size of report id list */
		public Integer totalSize=0;
	}

	/**
	 * Result of getting request detail
	 */
	public class GetResult extends ExpRequestService.ExpRequestBase implements RemoteApi.ResponseParam {}

	/**
   * Result of saving request data
   */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** Saved report ID */
		public String reportId;
	}

	public class CreateReportFromRequestResult implements RemoteApi.ResponseParam {
		/** Saved report ID */
		public String reportId;
		public List<ExpParam.UpdatedClonedRecordResult> updatedRecords;
	}

	/**
	 * API to get Expense request list
	 */
	public with sharing class GetExpRequestListApi extends RemoteApi.ResourceBase {

		/**
		 * Get Expense request list
		 * @param  req Request parameter(GetListParam)
		 * @return Response (GetListResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			GetListParam param = (GetListParam)req.getParam(ExpRequestResource.GetListParam.class);

			param.validate();

			// Get employee
			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(null);

			// Check if the employee can use Expense
			ExpCommonUtil.checkCanUseExpenseRequest(empBase);

			// Get Request list
			List<ExpRequestEntity> entityList = ExpRequestService.getExpRequestListByReportIdList(param.reportIds);

			return makeResponse(entityList);
		}

		private GetListResult makeResponse(List<ExpRequestEntity> entityList) {
			List<ExpRequestService.ExpRequest> reports = new List<ExpRequestService.ExpRequest>();

			for (ExpRequestEntity entity : entityList) {
				ExpRequestService.ExpRequest report = new ExpRequestService.ExpRequest();
				report.setData(entity);
				reports.add(report);
			}

			GetListResult result = new GetListResult();
			result.reports = reports;
			return result;
		}
	}

	/**
	 * API to get Expense request id list
	 */
	public with sharing class GetExpRequestIdListApi extends RemoteApi.ResourceBase {

		/**
		 * Get Expense request list
		 * @param  req Request parameter(GetListParam)
		 * @return Response (GetListResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			GetIdListParam param = (GetIdListParam)req.getParam(ExpRequestResource.GetIdListParam.class);

			// Get employee
			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(null);

			// Check if the employee can use Expense
			ExpCommonUtil.checkCanUseExpenseRequest(empBase);

			GetIdListResult res = new GetIdListResult();
			List<Id> idList = ExpRequestService.getExpRequestIdList(empBase.id,  null, true, param.isApproved);
			if (idList != null && !idList.isEmpty()) {
				res.reportIdList = idList;
				res.totalSize = idList.size();
			}
			return res;
		}
	}

	/**
	 * API to get Expense request detail
	 */
	public with sharing class GetExpRequestApi extends RemoteApi.ResourceBase {

		/**
		 * Get Expense request detail
		 * @param  req Request parameter(GetParam)
		 * @return Response (GetResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			GetParam param = (GetParam)req.getParam(ExpRequestResource.GetParam.class);

			param.validate();

			// Check if the employee can use Expense
			ExpCommonUtil.checkCanUseExpenseRequest(null);

			// Get request data
			ExpRequestEntity entity = ExpRequestService.getExpRequest(param.reportId);

			if (entity == null) {
				throw new App.IllegalStateException(
						App.ERR_CODE_NOT_FOUND_EXP_REPORT,
						ComMessage.msg().Exp_Err_NotFoundRequest);
			}
			return makeResponse(entity);
		}

		private GetResult makeResponse(ExpRequestEntity entity) {
			GetResult result = new GetResult();
			result.setData(entity);
			result.setRecords(entity.recordList);
			return result;
		}
	}

	/**
	 * API to save Expense request data
	 */
	public with sharing class SaveExpRequestApi extends RemoteApi.ResourceBase {

		/** For Testing the Rollback */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * Save Expense request data
		 * @param  req Request parameter (SaveParam)
		 * @return Response (SaveExpRequestResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SaveParam param = (SaveParam)req.getParam(ExpRequestResource.SaveParam.class);

			validateParam(param);

			// Create entity from parameter
			ExpRequestEntity report = param.createEntity();

			// Get employee
			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(param.empId);

			// Check if the employee can use Expense module
			ExpCommonUtil.checkCanUseExpenseRequest(empBase);

			// Rollback once saving fail
			Savepoint sp = Database.setSavepoint();

			Id resId;
			try {
				// Save request data
				resId = ExpRequestService.saveExpRequestSummary(empBase, report, report.recordingDate);

				if (isRollbackTest) {
					throw new App.IllegalStateException('SaveExpRequestApi Rollback Test');
				}
			} catch (Exception e) {
				// Rollback
				Database.rollback(sp);
				throw e;
			}

			// Set response
			SaveResult response = new SaveResult();
			response.reportId = resId;

			return response;
		}

		private void validateParam(SaveParam param) {
			ExpCommonUtil.validateStringIsNotBlank('subject', param.subject);
			// CheckNull('purpose', param.purpose);
			ExpCommonUtil.validateDate('recordingDate', param.accountingDate);
			ExpCommonUtil.validateStringLength(ComMessage.msg().Exp_Lbl_Title, param.subject, ExpRequestEntity.MAX_SUBJECT_LENGTH);
			ExpCommonUtil.validateStringLength(ComMessage.msg().Exp_Lbl_Purpose, param.purpose, ExpRequestEntity.MAX_PURPOSE_LENGTH);
			// Extended Item Ids
			param.verifyExtendedItemIds();
			// Extended Item Date Values (if specified, check if the date is valid)
			param.verifyExtendedItemDateValues();
			if (param.records != null) {
				for (ExpRequestService.ExpRequestRecord record : param.records) {
					if (record.items != null) {
						for (ExpRequestService.ExpRequestRecordItem item : record.items) {
							// Extended Item Ids
							item.verifyExtendedItemIds();
							// Extended Item Date Values (if specified, check if the date is valid)
							item.verifyExtendedItemDateValues();
						}
					}
				}
			}
		}
	}

	/**
	 * API to delete Expense request data
	 */
	public with sharing class DeleteExpRequestApi extends RemoteApi.ResourceBase {

		/** For Testing the Rollback */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * Delete Expense request data
		 * @param  req Request parameter (SaveParam)
		 * @return Response (null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			DeleteParam param = (DeleteParam)req.getParam(DeleteParam.class);

			validateParam(param);

			// Check if Expense module is available
			ExpCommonUtil.checkCanUseExpenseRequest(null);

			// Rollback once saving fail
			Savepoint sp = Database.setSavepoint();

			try {
				// Delete request data
				ExpRequestService.deleteExpRequest(param.reportId);

				if (isRollbackTest) {
					throw new App.IllegalStateException('DeleteExpRequestApi Rollback Test');
				}
			} catch (Exception e) {
				// Rollback
				Database.rollback(sp);
				throw e;
			}
			return null;
		}

		private void validateParam(DeleteParam param) {
			ExpCommonUtil.validateId('reportId', param.reportId, true);
		}
	}

	/*
	 @description Parameter class for CloneExpRequestApi
	 */
	public class CloneExpRequestParam implements RemoteApi.RequestParam {
		// report Id
		public String reportId;
		// employee Id
		public String empId;

		public void validate() {
			// 経費精算ID
			ExpCommonUtil.validateId('reportId', reportId, true);
			// employee ID
			ExpCommonUtil.validateId('empId', empId, false);
		}
	}

	/*
	 @description Result parameter class for CloneExpRequestApi
	 */
	public class CloneExpRequestResult implements RemoteApi.ResponseParam {
		/*
		 保存した経費精算データのレコードID
		 */
		public String reportId;
	}

	/*
	 @description Clone existing request API class
	 */
	public with sharing class CloneExpRequestApi extends RemoteApi.ResourceBase {

		@TestVisible
		private Boolean isRollbackTest = false;

		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			CloneExpRequestParam param = (CloneExpRequestParam)req.getParam(CloneExpRequestParam.class);

			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(param.empId);

			// validation
			param.validate();

			//check if Expense module can be used
			ExpCommonUtil.canUseExpense();

			// roll back point
			Savepoint sp = Database.setSavepoint();

			Id resId;
			try {
				// clone request
				resId = new ExpRequestService().cloneExpRequest(empBase, param.reportId);

				// rollback test
				if (isRollbackTest) {
					throw new App.IllegalStateException('CloneExpRequestApi Rollback Test');
				}
			} catch (Exception e) {
				// rollback
				Database.rollback(sp);
				throw e;
			}

			// response
			CloneExpRequestResult res = new CloneExpRequestResult();
			res.reportId = resId;

			return res;
		}
	}

	/**
	 * API to create Expense report from request
	 */
	public with sharing class CreateExpReportApi extends RemoteApi.ResourceBase {

		/** For Testing the Rollback */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * Save Expense request data
		 * @param  req Request parameter (SaveParam)
		 * @return Response (SaveExpRequestResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			CreateReportParam param = (CreateReportParam)req.getParam(ExpRequestResource.CreateReportParam.class);

			validateParam(param);

			// Get employee
			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(param.empId);

			// Check if Expense module is available
			ExpCommonUtil.checkCanUseExpenseRequest(null);

			ExpReportEntity report = ExpRequestService.createReportEntityFromRequest(param.reportId);

			// Rollback once saving fail
			Savepoint sp = Database.setSavepoint();

			ExpReportEntity createdReportEntity;
			try {
				// Save request data
				createdReportEntity = new ExpService().saveReportCreatedFromRequest(empBase, report, report.accountingDate);
				if (isRollbackTest) {
					throw new App.IllegalStateException('CreateExpReportApi Rollback Test');
				}
			} catch (Exception e) {
				// Rollback
				Database.rollback(sp);
				throw e;
			}

			// Set response
			CreateReportFromRequestResult response = new CreateReportFromRequestResult();
			response.reportId = createdReportEntity.id;
			response.updatedRecords = new List<ExpParam.UpdatedClonedRecordResult>();
			for (ExpRecordEntity updatedRecord : createdReportEntity.recordList) {
				ExpParam.UpdatedClonedRecordResult updatedRecordResult = new ExpParam.UpdatedClonedRecordResult();
				updatedRecordResult.expenseTypeName = updatedRecord.recordItemList[0].expTypeName;
				updatedRecordResult.recordDate = updatedRecord.recordDate.format();
				updatedRecordResult.isForeignCurrency = updatedRecord.recordItemList[0].useForeignCurrency;
				response.updatedRecords.add(updatedRecordResult);
			}

			return response;
		}

		private void validateParam(CreateReportParam param) {
			ExpCommonUtil.validateId('empId', param.empId, false);
			ExpCommonUtil.validateId('reportId', param.reportId, true);
		}
	}
}