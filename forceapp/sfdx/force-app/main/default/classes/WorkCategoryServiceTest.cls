/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description WorkCategoryServiceのテストクラス
*/
@isTest
private class WorkCategoryServiceTest {

	/**
	 * 作業分類保存処理のテスト
	 * 新規の作業分類が保存できることを確認する
	 */
	@isTest static void saveWorkCategoryTestNew() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();

		WorkCategoryEntity entity = new WorkCategoryEntity();
		entity.nameL0 = 'テスト作業分類_L0';
		entity.nameL1 = 'テスト作業分類_L1';
		entity.nameL2 = 'テスト作業分類_L2';
		entity.name = entity.nameL0;
		entity.code = '001';
		entity.companyId = company.Id;
		entity.validFrom = AppDate.today();
		entity.validTo = AppDate.today().addDays(60);
		entity.order = 1;

		Test.startTest();

		WorkCategoryService service = new WorkCategoryService();
		Repository.SaveResult result = service.saveWorkCategory(entity);
		Id workCategoryId = result.details[0].id;

		Test.stopTest();

		// 作業分類レコードが1件作成されること
		List<TimeWorkCategory__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				UniqKey__c,
				CompanyId__c,
				ValidFrom__c,
				ValidTo__c,
				Order__c
			FROM TimeWorkCategory__c];

		System.assertEquals(1, newRecords.size());

		TimeWorkCategory__c newRecord = newRecords[0];
		System.assertEquals(newRecord.Id, workCategoryId);
		System.assertEquals(entity.nameL0, newRecord.Name);
		System.assertEquals(entity.nameL0, newRecord.Name_L0__c);
		System.assertEquals(entity.nameL1, newRecord.Name_L1__c);
		System.assertEquals(entity.nameL2, newRecord.Name_L2__c);
		System.assertEquals(entity.code, newRecord.Code__c);
		System.assertEquals(company.Code__c + '-' + entity.code, newRecord.UniqKey__c);
		System.assertEquals(entity.companyId, newRecord.CompanyId__c);
		System.assertEquals(AppDate.convertDate(entity.validFrom), newRecord.ValidFrom__c);
		System.assertEquals(AppDate.convertDate(entity.validTo), newRecord.ValidTo__c);
		System.assertEquals(entity.order, newRecord.Order__c);
	}

	/**
	 * 作業分類保存処理のテスト
	 * 既存の作業分類が更新できることを確認する
	 */
	@isTest static void saveWorkCategoryTestUpdate() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		TimeWorkCategory__c workCategory = ComTestDataUtility.createWorkCategory('test', company.Id);

		WorkCategoryRepository repo = new WorkCategoryRepository();
		WorkCategoryEntity entity = repo.getEntity(workCategory.Id);
		entity.nameL0 = 'updateL0';
		entity.nameL1 = 'updateL1';
		entity.nameL2 = 'updateL2';
		entity.code = 'update';
		entity.order = 1;
		entity.validFrom = AppDate.today();
		entity.validTo = AppDate.today().addDays(60);

		Test.startTest();

		WorkCategoryService service = new WorkCategoryService();
		service.saveWorkCategory(entity);

		Test.stopTest();

		WorkCategoryEntity resEntity = repo.getEntity(workCategory.Id);

		System.assertEquals(entity.name, resEntity.name);
		System.assertEquals(entity.nameL0, resEntity.nameL0);
		System.assertEquals(entity.nameL1, resEntity.nameL1);
		System.assertEquals(entity.nameL2, resEntity.nameL2);
		System.assertEquals(entity.code, resEntity.code);
		System.assertEquals(company.Code__c + '-' + entity.code, resEntity.uniqKey);
	}

	/**
	 * 作業分類保存処理のテスト
	 * コードが重複している場合はアプリ例外が発生することを確認する
	 */
	@isTest static void saveWorkCategoryTestDuplicateCode() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		TimeWorkCategory__c workCategory1 = ComTestDataUtility.createWorkCategory('test1', company.Id);
		TimeWorkCategory__c workCategory2 = ComTestDataUtility.createWorkCategory('test2', company.Id);

		WorkCategoryRepository repo = new WorkCategoryRepository();
		WorkCategoryEntity entity = repo.getEntity(workCategory2.Id);
		entity.code = workCategory1.Code__c;

		try {
			WorkCategoryService service = new WorkCategoryService();
			service.saveWorkCategory(entity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
		}
	}

	/**
	 * 作業分類保存処理のテスト
	 * 項目値に対するバリデーションが実行されていることを確認する
	 */
	@isTest static void saveWorkCategoryTestValidate() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		TimeWorkCategory__c workCategory = ComTestDataUtility.createWorkCategory('test', company.Id);

		WorkCategoryRepository repo = new WorkCategoryRepository();
		WorkCategoryEntity entity = repo.getEntity(workCategory.Id);
		entity.nameL0 = '';

		try {
			WorkCategoryService service = new WorkCategoryService();
			service.saveWorkCategory(entity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
		}
	}
}