/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group Common
 *
 * @description 履歴管理方式が親子型のマスタのベースエンティティの基底クラス
 */
public abstract with sharing class ParentChildBaseEntity extends BaseEntity {

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目値 */
		public AppMultiString nameL;
		/** コンストラクタ */
		public LookupField(AppMultiString nameL) {
			this.nameL = nameL;
		}
	}

	/**
	 * コンストラクタ
	 * @param sobj 参照するSObject
	 * @param sobjectFieldMap フィールド名とフィールドのMap
	 */
	public ParentChildBaseEntity(SObject sobj, Map<String, Schema.SObjectField> sobjectFieldMap) {
		super(sobj, sobjectFieldMap);
	}


	/** 履歴リスト */
	@testVisible
	protected List<ParentChildHistoryEntity> superHistoryList {
		get {
			return convertSuperHistoryList();
		}
	}

	/**
	 * Master Name
	 * マスタ名
	 */
	public static final String FIELD_NAME_NAME = 'NAME';
	public String name {
		get {
			return (String)getFieldValue(FIELD_NAME_NAME);
		}
		set {
			setFieldValue(FIELD_NAME_NAME, value);
		}
	}

	/**
	 * Current History ID
	 * 現時点で有効な履歴ID
	 */
	public static final String FIELD_NAME_CURRENT_HISTORY_ID = createNameWithNamespace('CurrentHistoryId__c');
	public Id currentHistoryId {
		get {
			return (Id)getFieldValue(FIELD_NAME_CURRENT_HISTORY_ID);
		}
		set {
			setFieldValue(FIELD_NAME_CURRENT_HISTORY_ID, value);
		}
	}

	/** 一意となるコードまたはユニークキーを取得する */
	public abstract String getUniqCode();

	/**
	 * 履歴エンティティリストが空かどうか
	 * @return 履歴エンティティリストがnullまたは0の場合はtrue, そうでない場合はfalse
	 */
	public Boolean isEmptyHistoryList() {
		if (this.superHistoryList == null) {
			return true;
		}
		return this.superHistoryList.isEmpty();
	}

	/**
	 * 履歴エンティティリストが空でないかどうか
	 * @return 履歴エンティティリストの要素数が0より多い場合はtrue, そうでない場合はfalse
	 */
	public Boolean isNotEmptyHistoryList() {
		return ! isEmptyHistoryList();
	}

	/**
	 * 履歴エンティティリストを取得する。(履歴エンティティの基底ラスで)
	 */
	public List<ParentChildHistoryEntity> getSuperHistoryList() {
		return this.superHistoryList;
	}

	/**
	 * Removes all elements from a history list,
	 * 履歴リストが持つ履歴を全て削除する
	 */
	public void clearHistoryList() {
		this.superHistoryList.clear();
	}

	/**
	 * Add all specified history to history list
	 * 指定されたすべての履歴を履歴リストに追加する
	 * @param historyList 追加対象の履歴リスト
	 */
	public void addHistoryAll(List<ParentChildHistoryEntity> historyList) {
		this.superHistoryList.addAll(historyList);
	}

	/**
	 * 履歴エンティティリストを取得する。(履歴エンティティの基底ラスで)
	 */
	protected abstract List<ParentChildHistoryEntity> convertSuperHistoryList();

	/**
	 * 指定したIDの履歴エンティティを取得する
	 * @param targetId 取得対象ID
	 * @return 履歴、ただし履歴が存在しない場合はnull
	 */
	public ParentChildHistoryEntity getSuperHistoryById(Id targetId) {
		if (this.superHistoryList == null) {
			return null;
		}

		for (ParentChildHistoryEntity history : this.superHistoryList) {
			if (history.id == targetId) {
				return history;
			}
		}
		return null;
	}

	/**
	 * 指定した日に有効な履歴エンティティを取得する
	 * @param targetDate 対象日
	 * @return 有効な履歴、ただし有効な履歴が存在しない場合はnull
	 */
	public ParentChildHistoryEntity getSuperHistoryByDate(AppDate targetDate) {
		if (this.superHistoryList == null) {
			return null;
		}

		Date td = targetDate.getDate();
		for (ParentChildHistoryEntity history : this.superHistoryList) {
			// 有効開始日、失効日が設定されていない履歴、論理削除されている履歴は対象外
			// if ((history.validFrom == null) || (history.validTo == null)) {
			if ((history.validFrom == null) || (history.validTo == null) || (history.isRemoved == true)) {
				continue;
			}

			// 有効開始日 <= 対象日 < 失効日
			if ((history.validFrom.getDate() <= td) && (history.validTo.getDate() > td)) {
				return history;
			}
		}
		return null;
	}

	/**
	 * Get history that are valid for the specified period
	 * 指定した期間に有効な履歴エンティティを取得する(指定した期間中に1日でも有効になる履歴が対象)
	 * @param range 取得対象期間
	 * @param endDate 取得対象終了日
	 * @return 有効な履歴のリスト
	 */
	public List<ParentChildHistoryEntity> getSuperHistoryByDate(AppDateRange range) {
		return getSuperHistoryByDate(range.startDate, range.endDate);
	}

	/**
	 * Get history that are valid for the specified period
	 * 指定した期間に有効な履歴エンティティを取得する(指定した期間中に1日でも有効になる履歴が対象)
	 * @param startDate 取得対象開始日
	 * @param endDate 取得対象終了日
	 * @return 有効な履歴のリスト
	 */
	public List<ParentChildHistoryEntity> getSuperHistoryByDate(AppDate startDate, AppDate endDate) {
		if (this.superHistoryList == null) {
			return null;
		}

		List<ParentChildHistoryEntity> histories = new List<ParentChildHistoryEntity>();
		for (ParentChildHistoryEntity history : this.superHistoryList) {
			if (history.isRemoved == true) {
				continue;
			}

			// 有効開始日 <= 取得対象終了日、 失効日 > 取得対象開始日
			if ((history.validFrom.isBeforeEquals(endDate)) && (history.validTo.isAfter(startDate))) {
				histories.add(history);
			}
		}
		return histories;
	}

	/**
	 * 指定した日に有効な履歴が存在するかをチェックする
	 * @param targetDate 対象日
	 * @return 有効な履歴が存在する場合はtrue、そうでない場合はfalse
	 */
	public Boolean isValid(AppDate targetDate) {
		return getSuperHistoryByDate(targetDate) != null;
	}

	/**
	 * 指定した期間に有効な履歴エンティティを取得する(全ての有効期間が指定した期間に含まれる)
	 * @param startDate 取得対象開始日
	 * @param endDate 取得対象終了日
	 * @return 有効な履歴のリスト
	 */
	public List<ParentChildHistoryEntity> getSuperHistoryIncludedPeriod(AppDate startDate, AppDate endDate) {
		if (this.superHistoryList == null) {
			return null;
		}

		final Date sd = startDate.getDate();
		final Date ed = endDate.getDate();
		List<ParentChildHistoryEntity> histories = new List<ParentChildHistoryEntity>();
		for (ParentChildHistoryEntity history : this.superHistoryList) {
			if (history.isRemoved == true) {
				continue;
			}
			// 有効開始日 >= 取得対象開始日、 失効日-1<= 取得対象終了日
			if ((history.validFrom.getDate() >= sd) && (history.validTo.getDate().addDays(-1) <= ed)) {
				histories.add(history);
			}
		}
		return histories;
	}

	/**
	 * 履歴エンティティリストから最も古い有効な履歴を取得する
	 * @return 履歴の中でもっとも有効開始日が古い履歴
	 */
	public ParentChildHistoryEntity getOldestSuperHistory() {
		if (this.superHistoryList == null) {
			return null;
		}

		ParentChildHistoryEntity retHistory;
		for (ParentChildHistoryEntity history : this.superHistoryList) {
			// 論理削除済みの履歴は対象外
			if (history.isRemoved == true) {
				continue;
			}
			if (retHistory == null) {
				retHistory = history;
			} else {
				if ((history.validFrom != null)
						&& (history.validFrom.getDate() < retHistory.validFrom.getDate())) {
					retHistory = history;
				}
			}
		}
		return retHistory;
	}

	/**
	 * 履歴エンティティリストから最も新しい有効な履歴を取得する
	 * @return 履歴の中でもっとも有効開始日が新しい履歴
	 */
	public ParentChildHistoryEntity getLatestSuperHistory() {
		if (this.superHistoryList == null) {
			return null;
		}

		ParentChildHistoryEntity retHistory;
		for (ParentChildHistoryEntity history : this.superHistoryList) {
			// 論理削除済みの履歴は対象外
			if (history.isRemoved == true) {
				continue;
			}
			if (retHistory == null) {
				retHistory = history;
			} else {
				if ((history.validFrom != null)
						&& (history.validFrom.getDate() > retHistory.validFrom.getDate())) {
					retHistory = history;
				}
			}
		}
		return retHistory;
	}

	/**
	 * 履歴全体での有効期間開始日を取得する
	 * @return 有効期間開始日、ただし有効な履歴が存在しない場合なnull
	 */
	public AppDate getValidStartDate() {
		ParentChildHistoryEntity history = getOldestSuperHistory();
		if (history == null) {
			return null;
		}
		return history.validFrom;
	}

	/**
	 * 履歴全体での有効期間終了日を取得する(失効日-1日)
	 * @return 有効期間終了日、ただし有効な履歴が存在しない場合なnull
	 */
	public AppDate getValidEndDate() {
		ParentChildHistoryEntity history = getLatestSuperHistory();
		if (history == null || history.validTo == null) {
			return null;
		}
		return history.validTo.addDays(-1);
	}

	/**
	 * 有効期間終了日に相当する項目のラベルを取得する(社員の場合は退職日)
	 * @return 有効期間終了日に相当する項目のラベル
	 */
	public virtual String getValidEndDateLabel() {
		throw new App.UnsupportedException('[ERROR] ParentChildBaseEntity.getValidEndDateLabel() not implemented.');
	}

	/**
	 * @description Return the instance of ParentChildBaseEntity type.
	 * Entityを複製し基底クラスで返却する。
	 */
	public abstract ParentChildBaseEntity copySuperEntity();
}