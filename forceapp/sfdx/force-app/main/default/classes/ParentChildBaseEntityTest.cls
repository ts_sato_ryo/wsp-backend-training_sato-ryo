/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 履歴管理方式が親子型のマスタのベースエンティティの基底クラス
 */
@isTest
private class ParentChildBaseEntityTest {

	private static final ParentChildRepository repo = new TimeSettingRepository();

	private static final ComCompany__c companyObj;
	static {
		companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
	}

	private static ParentChildBaseEntity createBaseEntity(String name) {
		TimeSettingBaseEntity base = new TimeSettingBaseEntity();
		base.companyId = companyObj.Id;
		base.name = name;
		base.code = name;
		base.uniqKey = companyObj.Code__c + '-' + base.code;
		base.historyList = new List<TimeSettingHistoryEntity>();
		return base;
	}

	/**
	 * Create a valid history
	 * 有効な履歴を作成する
	 * @param name 履歴名
	 * @param validFrom 有効開始日
	 * @param validTo 失効日
	 * @return 作成した履歴
	 */
	private static ParentChildHistoryEntity createHistoryEntity(String name, AppDate validFrom, AppDate validTo) {
		TimeSettingHistoryEntity history = new TimeSettingHistoryEntity();
		history.name = name;
		history.nameL0 = name;
		history.validFrom = validFrom;
		history.validTo = validTo;
		if (validFrom != null && validTo != null) {
			history.uniqKey = history.createUniqKey(name);
		}
		return history;
	}

	/**
	 * Create a logically deleted history
	 * 論理削除済みの履歴を作成する
	 * @param name 履歴名
	 * @param validFrom 有効開始日
	 * @param validTo 失効日
	 * @return 作成した履歴
	 */
	private static ParentChildHistoryEntity createRemovedHistoryEntity(String name, AppDate validFrom, AppDate validTo) {
		ParentChildHistoryEntity history = createHistoryEntity(name, validFrom, validTo);
		history.isRemoved = true;
		return history;
	}

	/**
	 * もっとも有効開始日の古い履歴を取得する
	 */
	@isTest static void getOldestSuperHistoryTest() {

		// 履歴が存在する場合
		ParentChildHistoryEntity removedHistory = createHistoryEntity('History 6', AppDate.newInstance(2017, 11, 1), AppDate.newInstance(2017, 12, 20));
		removedHistory.isRemoved = true;
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 21), AppDate.newInstance(2017, 12, 22)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2017, 12, 20), AppDate.newInstance(2017, 12, 21)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2017, 12, 20), AppDate.newInstance(2017, 12, 21)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2017, 12, 18), AppDate.newInstance(2017, 12, 19)));
		base.superHistoryList.add(createHistoryEntity('History 4', AppDate.newInstance(2017, 12, 19), AppDate.newInstance(2017, 12, 20)));
		base.superHistoryList.add(createHistoryEntity('History 5', null, AppDate.newInstance(2017, 12, 20)));
		base.superHistoryList.add(removedHistory);

		// 履歴が存在する場合、もっとも有効開始日が古い履歴が取得できる
		ParentChildHistoryEntity oldestHistory = base.superHistoryList.get(3);
		System.assertEquals(oldestHistory.validFrom, base.getOldestSuperHistory().validFrom);

		// 履歴が存在しない場合
		DepartmentBaseEntity baseEmpty = new DepartmentBaseEntity();
		System.assertEquals(null, baseEmpty.getOldestSuperHistory());
	}

	/**
	 * もっとも有効開始日の新しい履歴を取得する
	 */
	@isTest static void getLatestSuperHistoryTest() {
		ParentChildHistoryEntity latestHistory = createHistoryEntity('History 3', AppDate.newInstance(2017, 12, 22), AppDate.newInstance(2017, 12, 23));
		ParentChildHistoryEntity removedHistory = createHistoryEntity('History 6', AppDate.newInstance(2019, 2, 1), AppDate.newInstance(2019, 3, 1));
		removedHistory.isRemoved = true;
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 21), AppDate.newInstance(2017, 12, 22)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2017, 12, 20), AppDate.newInstance(2017, 12, 21)));
		base.superHistoryList.add(latestHistory);
		base.superHistoryList.add(createHistoryEntity('History 4', AppDate.newInstance(2017, 12, 19), AppDate.newInstance(2017, 12, 20)));
		base.superHistoryList.add(createHistoryEntity('History 5', null, AppDate.newInstance(2019, 12, 20)));
		base.superHistoryList.add(removedHistory);

		// 履歴が存在する場合
		System.assertEquals(latestHistory.validFrom, base.getLatestSuperHistory().validFrom);

		// 履歴が存在しない場合
		DepartmentBaseEntity baseEmpty = new DepartmentBaseEntity();
		System.assertEquals(null, baseEmpty.getLatestSuperHistory());
	}

	/**
	 * 履歴全体の有効開始日を取得する
	 */
	@isTest static void getValidStartDateTest() {

		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 21), AppDate.newInstance(2017, 12, 22)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2017, 12, 20), AppDate.newInstance(2017, 12, 21)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2017, 12, 20), AppDate.newInstance(2017, 12, 21)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2017, 12, 18), AppDate.newInstance(2017, 12, 19)));
		base.superHistoryList.add(createHistoryEntity('History 4', AppDate.newInstance(2017, 12, 19), AppDate.newInstance(2017, 12, 20)));
		base.superHistoryList.add(createHistoryEntity('History 5', null, AppDate.newInstance(2017, 12, 20)));

		// 履歴が存在する場合、もっとも有効開始日が古い履歴履歴の開始日が取得できる
		ParentChildHistoryEntity oldestHistory = base.superHistoryList.get(3);
		System.assertEquals(oldestHistory.validFrom, base.getValidStartDate());

		// 履歴が存在しない場合
		DepartmentBaseEntity baseEmpty = new DepartmentBaseEntity();
		System.assertEquals(null, baseEmpty.getValidStartDate());
	}

	/**
	 * 履歴全体の有効期間終了日を取得する
	 */
	@isTest static void getValidEndtDateTest() {

		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 21), AppDate.newInstance(2017, 12, 22)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2017, 12, 20), AppDate.newInstance(2017, 12, 21)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2017, 12, 18), AppDate.newInstance(2017, 12, 19)));
		base.superHistoryList.add(createHistoryEntity('History 4', AppDate.newInstance(2017, 12, 19), AppDate.newInstance(2017, 12, 20)));
		base.superHistoryList.add(createHistoryEntity('History 5', null, AppDate.newInstance(2017, 12, 20)));

		// 履歴が存在する場合、もっとも失効日が最新の履歴の失効日-1日が取得できる
		ParentChildHistoryEntity latestHistory = base.superHistoryList.get(0);
		System.assertEquals(latestHistory.validTo.addDays(-1), base.getValidEndDate());

		// 履歴が存在しない場合
		DepartmentBaseEntity baseEmpty = new DepartmentBaseEntity();
		System.assertEquals(null, baseEmpty.getValidEndDate());
	}

	/**
	 * 指定したIDの履歴を取得する
	 */
	@isTest static void getsuperHistoryByIdTest() {
		AppDate today = AppDate.today();
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 25), AppDate.newInstance(2018, 1, 1)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));

		repo.saveEntity(base);
		List<TimeSettingHistory__c> historyObjs = [SELECT Id, Name FROM TimeSettingHistory__c ORDER BY ValidFrom__c];
		base.superHistoryList[0].setId(historyObjs[0].Id);
		base.superHistoryList[1].setId(historyObjs[1].Id);
		base.superHistoryList[2].setId(historyObjs[2].Id);

		TimeSettingHistory__c targetObj;
		Id targetId;

		// 履歴リストに存在するIDを指定
		targetObj = historyObjs[1];
		targetId = targetObj.Id;
		System.assertEquals(targetObj.Name, base.getsuperHistoryById(targetObj.Id).name);

		// 履歴リストに存在しないIDを指定 -> nullが返却される
		targetId = Userinfo.getUserId();
		System.assertEquals(null, base.getsuperHistoryById(targetId));
	}

	/**
	 * 指定した日付で有効な履歴を取得する
	 */
	@isTest static void getsuperHistoryByDateTest() {
		AppDate today = AppDate.today();
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 25), AppDate.newInstance(2018, 1, 1)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));
		base.superHistoryList.add(createHistoryEntity('History 4', AppDate.newInstance(2018,  4, 15), null));
		base.superHistoryList.add(createHistoryEntity('History 5', null, AppDate.newInstance(2018, 10, 1)));

		ParentChildHistoryEntity history6 = createHistoryEntity('History 6', AppDate.newInstance(2018, 10, 1), AppDate.newInstance(2018, 11, 1));
		history6.isRemoved = true;
		base.superHistoryList.add(history6);

		AppDate targetDate;

		// 有効期間に含まれている日を指定
		targetDate = AppDate.newInstance(2018, 1, 15);
		System.assertEquals('History 2', base.getsuperHistoryByDate(targetDate).name);

		// 有効開始日を指定
		targetDate = base.superHistoryList.get(2).validFrom;
		System.assertEquals('History 3', base.getsuperHistoryByDate(targetDate).name);

		// 失効日前日を指定
		targetDate = base.superHistoryList.get(2).validTo.addDays(-1);
		System.assertEquals('History 3', base.getsuperHistoryByDate(targetDate).name);

		// マスタ全体の有効開始日の前日を指定 -> nullが返却される
		targetDate = base.superHistoryList.get(0).validFrom.addDays(-1);
		System.assertEquals(null, base.getsuperHistoryByDate(targetDate));

		// マスタ全体の失効日を指定 -> nullが返却される
		targetDate = base.superHistoryList.get(2).validTo;
		System.assertEquals(null, base.getsuperHistoryByDate(targetDate));

		// 履歴は存在するが論理削除されている -> nullが返却される
		targetDate = base.superHistoryList.get(5).validTo;
		System.assertEquals(null, base.getsuperHistoryByDate(targetDate));
	}

	/**
	 * isValidのテスト
	 */
	@isTest static void isValidTest() {
		AppDate today = AppDate.today();
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 25), AppDate.newInstance(2018, 1, 1)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));
		base.superHistoryList.add(createHistoryEntity('History 4', AppDate.newInstance(2018,  4, 15), null));
		base.superHistoryList.add(createHistoryEntity('History 5', null, AppDate.newInstance(2018, 10, 1)));

		ParentChildHistoryEntity history6 = createHistoryEntity('History 6', AppDate.newInstance(2018, 10, 1), AppDate.newInstance(2018, 11, 1));
		history6.isRemoved = true;
		base.superHistoryList.add(history6);

		AppDate targetDate;

		// 有効期間に含まれている日を指定 → 有効
		targetDate = AppDate.newInstance(2018, 1, 15);
		System.assertEquals(true, base.isValid(targetDate));

		// 有効開始日を指定 → 有効
		targetDate = base.superHistoryList.get(2).validFrom;
		System.assertEquals(true, base.isValid(targetDate));

		// 失効日前日を指定 → 有効
		targetDate = base.superHistoryList.get(2).validTo.addDays(-1);
		System.assertEquals(true, base.isValid(targetDate));

		// マスタ全体の有効開始日の前日を指定 -> 無効
		targetDate = base.superHistoryList.get(0).validFrom.addDays(-1);
		System.assertEquals(false, base.isValid(targetDate));

		// マスタ全体の失効日を指定 -> 無効
		targetDate = base.superHistoryList.get(2).validTo;
		System.assertEquals(false, base.isValid(targetDate));

		// 履歴は存在するが論理削除されている -> 無効
		targetDate = base.superHistoryList.get(5).validTo;
		System.assertEquals(false, base.isValid(targetDate));
	}

	/**
	 * 指定した期間に含まれる履歴を取得する
	 */
	@isTest static void getSuperHistoryIncludedPeriodTestPeriod() {
		AppDate today = AppDate.today();
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 25), AppDate.newInstance(2018, 1, 1)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));
		base.superHistoryList.add(createRemovedHistoryEntity('History 4 Removed', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));

		AppDate startDate;
		AppDate  endDate;
		List<ParentChildHistoryEntity> histories;

		// 有効期間に含まれている日を指定
		startDate = AppDate.newInstance(2017, 12, 31);
		endDate = AppDate.newInstance(2018, 4, 14);
		histories = base.getSuperHistoryIncludedPeriod(startDate, endDate);
		System.assertEquals(1, histories.size());
		System.assertEquals('History 2', histories[0].name);

		// 開始日に有効開始日を指定
		startDate = AppDate.newInstance(2018, 1, 1);
		endDate = AppDate.newInstance(2018, 4, 15);
		histories = base.getSuperHistoryIncludedPeriod(startDate, endDate);
		System.assertEquals(1, histories.size());
		System.assertEquals('History 2', histories[0].name);

		// マスタ全体の有効開始日の前日を指定 -> 空のリストが返却される
		startDate = base.superHistoryList.get(0).validFrom.addDays(-1);
		endDate = startDate;
		histories = base.getSuperHistoryIncludedPeriod(startDate, endDate);
		System.assertEquals(true, histories.isEmpty());

		// 取得対象終了日がマスタ全体の失効日より後
		startDate = AppDate.newInstance(2018, 1, 1);
		endDate = AppDate.newInstance(2018, 10, 2);
		histories = base.getSuperHistoryIncludedPeriod(startDate, endDate);
		System.assertEquals(2, histories.size());

		// マスタ全体の有効期間外を指定 -> 空のリストが返却される
		startDate = AppDate.newInstance(2018, 10, 2);
		endDate = AppDate.newInstance(2018, 10, 3);
		histories = base.getSuperHistoryIncludedPeriod(startDate, endDate);
		System.assertEquals(true, histories.isEmpty());
	}

	/**
	 * 指定した期間に有効な履歴を取得する
	 */
	@isTest static void getHistoryByDateTest() {
		AppDate today = AppDate.today();
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 25), AppDate.newInstance(2018, 1, 1)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));
		base.superHistoryList.add(createRemovedHistoryEntity('History 4 Removed', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));

		AppDate startDate;
		AppDate endDate;
		List<ParentChildHistoryEntity> histories;

		// 開始日に有効開始日を指定
		startDate = AppDate.newInstance(2017, 12, 25);
		endDate = AppDate.newInstance(2017, 12, 30);
		histories = base.getSuperHistoryByDate(startDate, endDate);
		System.assertEquals(1, histories.size());
		System.assertEquals('History 1', histories[0].name);

		// 開始日に失効日の前日を指定
		startDate = AppDate.newInstance(2018, 9, 30);
		endDate = AppDate.newInstance(2018, 10, 31);
		histories = base.getSuperHistoryByDate(startDate, endDate);
		System.assertEquals(1, histories.size());
		System.assertEquals('History 3', histories[0].name);

		// 開始日に失効日を指定 → 取得できないはず
		startDate = AppDate.newInstance(2018, 10, 1);
		endDate = AppDate.newInstance(2018, 10, 31);
		histories = base.getSuperHistoryByDate(startDate, endDate);
		System.assertEquals(0, histories.size());

		// 終了日に有効開始日を指定
		startDate = AppDate.newInstance(2017, 12, 25);
		endDate = AppDate.newInstance(2017, 12, 30);
		histories = base.getSuperHistoryByDate(startDate, endDate);
		System.assertEquals(1, histories.size());
		System.assertEquals('History 1', histories[0].name);

		// 終了日に有効開始日の前日を指定 → 取得できないはず
		startDate = AppDate.newInstance(2017, 10, 31);
		endDate = AppDate.newInstance(2017, 12, 24);
		histories = base.getSuperHistoryByDate(startDate, endDate);
		System.assertEquals(0, histories.size());

		// 論理削除されている履歴は取得対象
		startDate = AppDate.newInstance(2018, 4, 25);
		endDate = AppDate.newInstance(2018, 4, 30);
		histories = base.getSuperHistoryByDate(startDate, endDate);
		System.assertEquals(1, histories.size());
		System.assertEquals('History 3', histories[0].name);

		// 取得対象の履歴が複数
		startDate = AppDate.newInstance(2017, 12, 24);
		endDate = AppDate.newInstance(2018, 1, 10);
		histories = base.getSuperHistoryByDate(startDate, endDate);
		System.assertEquals(2, histories.size());
		System.assertEquals('History 1', histories[0].name);
		System.assertEquals('History 2', histories[1].name);
	}

	/**
	 * clearHistoryListのテスト
	 * 履歴リストの履歴が全て削除されることを確認する
	 */
	@isTest static void clearHistoryListTest() {
		AppDate today = AppDate.today();
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 25), AppDate.newInstance(2018, 1, 1)));
		base.superHistoryList.add(createHistoryEntity('History 2', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));
		base.superHistoryList.add(createHistoryEntity('History 3', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));
		base.superHistoryList.add(createRemovedHistoryEntity('History 4 Removed', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));

		base.clearHistoryList();
		System.assertEquals(0, ((TimeSettingBaseEntity)base).getHistoryList().size());
	}

	/**
	 * addHistoryAllテスト
	 * 履歴リストの全ての履歴が追加できることを確認する
	 */
	@isTest static void addHistoryAllTest() {
		AppDate today = AppDate.today();
		ParentChildBaseEntity base = createBaseEntity('Test Base');
		base.superHistoryList.add(createHistoryEntity('History 1', AppDate.newInstance(2017, 12, 25), AppDate.newInstance(2018, 1, 1)));

		List<ParentChildHistoryEntity> historyList = new List<ParentChildHistoryEntity>();
		historyList.add(createHistoryEntity('History 2', AppDate.newInstance(2018,  1,  1), AppDate.newInstance(2018, 4, 15)));
		historyList.add(createHistoryEntity('History 3', AppDate.newInstance(2018,  4, 15), AppDate.newInstance(2018, 10, 1)));

		base.addHistoryAll(historyList);
		System.assertEquals(3, ((TimeSettingBaseEntity)base).getHistoryList().size());
	}
}
