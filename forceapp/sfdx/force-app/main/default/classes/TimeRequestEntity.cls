/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数確定申請のエンティティ
 */
public with sharing class TimeRequestEntity extends TimeRequestGeneratedEntity {

	private static final String STATUS_LABEL_NOT_REQUESTED = 'NotRequested';


	/**
	 * コンストラクタ
	 */
	public TimeRequestEntity() {
		super(new TimeRequest__c());
	}

	/**
	 * コンストラクタ
	 */
	public TimeRequestEntity(TimeRequest__c sobj) {
		super(sobj);
	}

	/** 期間名（参照のみ。値を変更してもDBに保存されない。） */
	public String summaryName {
		get {
			if (this.timeSummaryId == null) {
				return null;
			}
			if (summaryName == null) {
				summaryName = sObj.TimeSummaryId__r.SummaryName__c;
			}
			return summaryName;
		}
		private set;
	}

	/** 月度開始日（参照のみ。値を変更してもDBに保存されない。） */
	public AppDate startDate {
		get {
			if (this.timeSummaryId == null) {
				return null;
			}
			if (startDate == null) {
				startDate = AppDate.valueOf(sObj.TimeSummaryId__r.StartDate__c);
			}
			return startDate;
		}
		private set;
	}

	/** 月度終了日（参照のみ。値を変更してもDBに保存されない。） */
	public AppDate endDate {
		get {
			if (this.timeSummaryId == null) {
				return null;
			}
			if (endDate == null) {
				endDate = AppDate.valueOf(sObj.TimeSummaryId__r.EndDate__c);
			}
			return endDate;
		}
		private set;
	}

	/** 社員名（参照のみ。値を変更してもDBに保存されない。） */
	public String employeeName {get {return employeeNameL.getFullName();}}
	public AppPersonName employeeNameL {
		get {
			if (this.employeeId == null) {
				return null;
			}
			if (employeeNameL == null) {
				employeeNameL = new AppPersonName(
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c,
						sObj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c,
						sObj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c,
						sObj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c);
			}
			return employeeNameL;
		}
		private set;
	}

	/** 顔写真URL（参照のみ。値を変更してもDBに保存されない。） */
	public String photoUrl {
		get {
			if (this.employeeId == null) {
				return null;
			}
			if (photoUrl == null) {
				photoUrl = sObj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl;
			}
			return photoUrl;
		}
		private set;
	}

	/** 部署名（参照のみ。値を変更してもDBに保存されない。） */
	public String departmentName {get {return AppMultiString.stringValue(departmentNameL);}}
	public AppMultiString departmentNameL {
		get {
			if (this.departmentId == null) {
				return null;
			}
			if (departmentNameL == null) {
				departmentNameL = new AppMultiString(
						sObj.DepartmentHistoryId__r.Name_L0__c,
						sObj.DepartmentHistoryId__r.Name_L1__c,
						sObj.DepartmentHistoryId__r.Name_L2__c);
			}
			return departmentNameL;
		}
		private set;
	}

	/** 添付ファイル */
	public Attachment attachment {get; set;}

	/**
	 * 表示用のステータスを取得する
	 * FIXME: FEと調整の上、AttRequestEntity・AttDailyRequestEntity.getDatailStatusと挙動を合わせる
	 * @param status 申請ステータス
	 * @param cancelType 取消種別
	 * @return ステータス
	 */
	public static String getStatusLabel(AppRequestStatus status, AppCancelType cancelType) {
		String s;

		if (status == null) {
			// 未申請
			return STATUS_LABEL_NOT_REQUESTED;
		}
		if (status.equals(AppRequestStatus.PENDING)) {
			// 申請中
			return AppRequestStatus.PENDING.value;
		}
		if (status.equals(AppRequestStatus.APPROVED)) {
			// 承認済み
			return AppRequestStatus.APPROVED.value;
		}
		if (status.equals(AppRequestStatus.DISABLED)) {
			if (cancelType.equals(AppCancelType.REJECTED)) {
				// 却下
				return AppCancelType.REJECTED.value;
			}
			if (cancelType.equals(AppCancelType.REMOVED)) {
				// 申請取消
				return AppCancelType.REMOVED.value;
			}
			if (cancelType.equals(AppCancelType.CANCELED)) {
				// 承認取消
				return AppCancelType.CANCELED.value;
			}
		}
		return null;
	}
}