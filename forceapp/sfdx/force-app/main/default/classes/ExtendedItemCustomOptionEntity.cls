/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Entity class for Custom Extended Item Option
 * 
 * @group Expense
 */
public class ExtendedItemCustomOptionEntity extends Entity {

	public static final Integer CODE_MAX_LENGTH = 20;
	public static final Integer NAME_MAX_LENGTH = 80;

	public ExtendedItemCustomOptionEntity() {
		this.changedFieldSet = new Set<Field>();
	}

	public enum Field {
		EXTENDED_ITEM_CUSTOM_ID,
		CODE,
		NAME,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		UNIQ_KEY
	}

	public String extendedItemCustomId {
		get;
		set {
			extendedItemCustomId = value;
			setChanged(Field.EXTENDED_ITEM_CUSTOM_ID);
		}
	}

	public String code {
		get;
		set {
			code = value;
			setChanged(Field.Code);
		}
	}

	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}

	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}

	private transient Set<Field> changedFieldSet;

	private void setChanged(Field field) {
		if (this.changedFieldSet == null) {
			this.changedFieldSet = new Set<Field>();
		}
		this.changedFieldSet.add(field);
	}

	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	public void resetChanged() {
		this.changedFieldSet.clear();
	}

	/*
	 * Create Unique Key for the Extended Item Custom Object.
	 * Unique Key will be created as follow: {@code extendedItemCustomUniqKey + '-' + code}
	 *
	 * COMMENTED OUT AS CURRENTLY THERE IS NO SUPPORT TO CREATE VIA API OR CODE
	 */
	/*public String createUniqKey(String extendedItemCustomUniqKey) {
		if (String.isBlank(extendedItemCustomUniqKey)) {
			throw new App.ParameterException('Custom Extended Item Unique Key is empty.');
		}
		if (String.isBlank(this.code)) {
			throw new App.IllegalStateException('Custom Extended Item Code is empty.');
		}
		return extendedItemCustomUniqKey + '-' + this.code;
	}*/
}