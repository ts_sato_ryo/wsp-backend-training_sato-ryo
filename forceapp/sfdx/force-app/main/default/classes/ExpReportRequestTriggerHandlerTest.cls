/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 経費
*
* @description ExpReportRequestTriggerHandlerのテストクラス
*/
@isTest
private class ExpReportRequestTriggerHandlerTest {

	/**
	 * @description 申請レコード更新処理（承認済み）のテスト
	 */
	@isTest
	static void updateRequestApproveTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請を実行
		ExpReportRequestService expReportRequestService = new ExpReportRequestService();
		expReportRequestService.isTestModeAndBypassExpReportValidation = true;
		expReportRequestService.submitRequest(report.id, null);

		// 申請IDを取得
		Id requestId = [SELECT Id FROM ExpReportRequest__c WHERE ExpReportId__c = :report.id LIMIT 1][0].Id;

		Test.startTest();

		ExpReportRequestEntity requestEntity = new ExpReportRequestEntity();
		requestEntity.setId(requestId);
		requestEntity.status = AppRequestStatus.APPROVED;

		ExpReportRequestRepository repo = new ExpReportRequestRepository();
		repo.saveEntity(requestEntity);

		Test.stopTest();

		// 申請データを取得
		ExpReportRequest__c request = [
				SELECT
					Id,
					ConfirmationRequired__c,
					CancelType__c
				FROM ExpReportRequest__c
				WHERE Id = :requestEntity.id];

		System.assertEquals(false, request.ConfirmationRequired__c);
		System.assertEquals(null, request.CancelType__c);
	}

	/**
	 * @description 申請レコード更新処理（却下）のテスト
	 */
	@isTest
	static void updateRequestRejectTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請を実行
		ExpReportRequestService expReportRequestService = new ExpReportRequestService();
		expReportRequestService.isTestModeAndBypassExpReportValidation = true;
		expReportRequestService.submitRequest(report.id, null);

		// 申請IDを取得
		Id requestId = [SELECT Id FROM ExpReportRequest__c WHERE ExpReportId__c = :report.id LIMIT 1][0].Id;

		Test.startTest();

		ExpReportRequestEntity requestEntity = new ExpReportRequestEntity();
		requestEntity.setId(requestId);
		requestEntity.status = AppRequestStatus.DISABLED;
		requestEntity.cancelType = AppCancelType.REJECTED;

		ExpReportRequestRepository repo = new ExpReportRequestRepository();
		repo.saveEntity(requestEntity);

		Test.stopTest();

		// 申請データを取得
		ExpReportRequest__c request = [
				SELECT
					Id,
					ConfirmationRequired__c,
					CancelType__c
				FROM ExpReportRequest__c
				WHERE Id = :requestEntity.id];

		System.assertEquals(true, request.ConfirmationRequired__c);
	}
}