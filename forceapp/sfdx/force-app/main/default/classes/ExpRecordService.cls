/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Service class of Expense Record
 */
public with sharing class ExpRecordService extends ExpRecordTransactionDataService {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/*
	 * Get a list of expense record whose report id is null and based on expense type id passed
	 * @param empId Target employee ID
	 * @param expTypeIdList Expense Type Id List
	 * @return List of expense record
	 */
	public List<ExpRecordEntity> getNoReportExpRecordList(Id empId, List<Id> expTypeIdList, AppDate startDate, AppDate endDate) {
		List<ExpRecordEntity> recordList = new ExpRecordRepository().getNoReportEntityList(empId, startDate, endDate);
		if (expTypeIdList != null && !expTypeIdList.isEmpty()) {
			recordList = filterRecordListByExpType(recordList, expTypeIdList);
		}
		return recordList;
	}

	/*
	 * Save a expense record
	 * @param empBase Owner employee base entity
	 * @param record Record data to be saved
	 * @param Id of ExpReportRequest (only needed to financial approval)
	 * @param isFinanceApproval indicates whether the change is by Accounting Clerk
	 * @return Created record ID
	 */
	public Id saveExpRecord(EmployeeBaseEntity empBase, ExpRecordEntity record, Id expRequestId, Id reportTypeId, Boolean isFinanceApproval) {
		ExpRecordRepository recordRepository = new ExpRecordRepository();
		// Set necessary IDs to the record
		if (!isFinanceApproval) {
			setEmployeeHistoryIdToRecord(empBase, record);
		}

		// Validate existing record
		ExpRecordEntity oldRecord;
		if (String.isNotBlank(record.id)) {
			// If there is record ID, then retrieve the existing Record by ID
			oldRecord = recordRepository.getEntityById(record.id);
			if (oldRecord == null) {
				App.RecordNotFoundException e = new App.RecordNotFoundException();
				e.setMessage(MESSAGE.Exp_Err_NotFoundRecord);
				throw e;
			}

			// Verify that the Parent Report is still the same. Record cannot be moved to a different report
			if (oldRecord.expReportId != null && oldRecord.expReportId != record.expReportId) {
				throw new App.IllegalStateException(App.ERR_CODE_PARENT_CHANGE, MESSAGE.Exp_Msg_CannotChangeParent(new List<String>{'expReportId'}));
			}
		}

		// Check for existing record items
		if (oldRecord != null) {
			removeDeletedRecordItemsFromRecord(record, oldRecord);
		}

		setRecordTypeAndFileAttachmentToRecords(new List<ExpRecordEntity>{record, oldRecord});

		// Validate Record
		validateRecord(record);

		// If there is existing Report, then update the total amount. If total amount is not necessary to be updated, then this will be remained as null.
		Decimal oldTotalAmount;
		Decimal newTotalAmount;
		if (String.isNotBlank(record.expReportId)) {
			// Only update the report if the old record amount and new record amount are different OR
			// Mobile incompatibility is changed
			Decimal oldRecordAmount = oldRecord == null ? 0 : oldRecord.amount;
			ExpRecordType oldRecordType = oldRecord == null ? null: oldRecord.recordType;
			ExpFileAttachment oldFileAttachment = oldRecord == null ? null: oldRecord.fileAttachment;
			if (oldRecordAmount != record.amount ||
					oldRecordType != record.recordType ||
					oldFileAttachment != record.fileAttachment) {
				// Retrieve Old Report
				ExpReportRepository reportRepository = new ExpReportRepository();
				ExpReportEntity reportEntity = reportRepository.getEntity(record.expReportId);

				// Calculate New Total Amount for Report
				oldTotalAmount = reportEntity.totalAmount;
				// Subtract the original record amount from the total amount
				reportEntity.totalAmount = reportEntity.totalAmount - oldRecordAmount + record.amount;
				newTotalAmount = reportEntity.totalAmount;

				// Calculate New Total Mobile Incompatible Count
				if (oldRecordType == ExpRecordType.HOTEL_FEE) {
					reportEntity.mobileIncompatibleCount--;
				}
				if (record.recordType == ExpRecordType.HOTEL_FEE) {
					reportEntity.mobileIncompatibleCount++;
				}

				try {
					reportRepository.saveEntity(reportEntity);
				} catch (DmlException e) {
					throwOutOfRangeException(e);
				}
			}
		}

		// Save Expense Record
		Repository.SaveResult saveResult;
		try {
			saveResult = recordRepository.saveEntity(record);
		} catch (DmlException e) {
			throwOutOfRangeException(e);
		}
		// Set record ID to the ExpRecord entity
		record.setId(saveResult.details[0].id);

		// Save receipt attached to the record
		saveRecordReceipt(record, empBase);

		// If Finance Approval updates the ExpRecord
		ExpRecordEntity newRecordEntity = record;
		if (isFinanceApproval && !String.isBlank(expRequestId)) {
			newRecordEntity = recordRepository.getEntityById(record.id);

			// Set Extended Item Info and Custom Option Names to the Record
			ExpService service = new ExpService();
			List<ExpRecordEntity> recordList = new List<ExpRecordEntity>{newRecordEntity};
			service.setExtendedItemInfoToRecords(recordList);
			service.setSelectedExtendedItemCustomOptionName(null, recordList);

			ExpReportRequestEntity request = new ExpReportRequestRepository().getEntity(expRequestId);
			if (request != null) {
				// Update the attachment(ExpReport) of ExpReportRequest
				AppAttachmentRepository attachRepo = new AppAttachmentRepository();
				AppAttachmentEntity attachment = attachRepo.getEntityListByParentId(request.id, request.name)[0];

				// For the Records, and Record Items, instead of retrieving from respective Repo, use the one from attachment
				ExpService.ExpReportParam existingAttachmentBody = (ExpService.ExpReportParam) JSON.deserialize(attachment.bodyText, ExpService.ExpReportParam.class);
				if (newTotalAmount != null) {
					existingAttachmentBody.totalAmount = newTotalAmount;
					request.totalAmount = newTotalAmount;
					ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();
					requestRepo.saveEntity(request);
				}
				AppLanguage companyLang = ExpReportRequestService.getCompanyLanguage(empBase.companyId);
				ExpService.ExpRecordParam newRecordAttachBody = new ExpService.ExpRecordParam(newRecordEntity, companyLang);
				// Since this method only save the Report summary data, not necessary to update the records part of existing attachment
				List<ExpService.ExpRecordParam> records = new List<ExpService.ExpRecordParam>();
				for (ExpService.ExpRecordParam existingRecordAttachBody : existingAttachmentBody.records) {
					if (existingRecordAttachBody.recordId != newRecordAttachBody.recordId) {
						records.add(existingRecordAttachBody);
					} else {
						records.add(newRecordAttachBody);
					}
				}
				existingAttachmentBody.records = records;
				attachment.bodyText = JSON.serialize(existingAttachmentBody);
				attachRepo.saveEntity(attachment);
			}
			// Create Modification History if the Total Amount Has been Changed
			if (newTotalAmount != null) {
				ExpReportEntity newReport = new ExpReportEntity();
				newReport.setId(newRecordEntity.expReportId);
				newReport.expRequestId = expRequestId;
				newReport.totalAmount = newTotalAmount;
				newReport.lastModifiedDate = newRecordEntity.lastModifiedDate;

				ExpReportEntity oldReport = new ExpReportEntity();
				oldReport.totalAmount = oldTotalAmount;

				ExpModificationHistoryLogic.ReportModificationMaker reportModificationMaker = new ExpModificationHistoryLogic.ReportModificationMaker(empBase, oldReport, newReport);
				reportModificationMaker.saveModificationHistory();
			}
		}

		// Update Recently Used values for ExpRecord Extended Item Lookup Values
		List<ExpRecordEntity> oldRecordList = oldRecord == null ? new List<ExpRecordEntity>() : new List<ExpRecordEntity>{oldRecord};
		List<ExpRecordEntity> newRecordList = new List<ExpRecordEntity>{newRecordEntity};
		Map<Id, Set<String>> extItemLookupValuesMap = ExpService.extractChangedExtendedItemLookupValues(null, null, oldRecordList, newRecordList);
		ExpRecentlyUsedService ruService = new ExpRecentlyUsedService();
		ruService.saveRecentlyUsedExtendedItemLookupValues(extItemLookupValuesMap, empBase.id);

		// Update Recently Used values for Job and Cost Center values of record
		ExpRecordItemEntity oldRecordItem = (oldRecord == null || oldRecord.recordItemList == null || oldRecord.recordItemList.isEmpty()) ? null : oldRecord.recordItemList[0];
		ExpRecordItemEntity newRecordItem = (newRecordEntity == null || newRecordEntity.recordItemList == null || newRecordEntity.recordItemList.isEmpty()) ? null : newRecordEntity.recordItemList[0];
		ruService.saveRecentlyUsedCostCenterValues(empBase.id, oldRecordItem != null ? oldRecordItem.costCenterHistoryId : null, newRecordItem != null ? newRecordItem.costCenterHistoryId : null);
		ruService.saveRecentlyUsedJobValues(empBase.id, oldRecordItem != null ? oldRecordItem.jobId : null, newRecordItem != null ? newRecordItem.jobId : null);

		// Update modification history if Finance Approval updates the ExpRecord
		if (isFinanceApproval) {
			ExpModificationHistoryLogic.RecordAndRecordItemModificationMaker maker = new ExpModificationHistoryLogic.RecordAndRecordItemModificationMaker(empBase, oldRecord, newRecordEntity, newRecordEntity.expReportId, expRequestId);
			maker.saveModificationHistory();
		}
		if (reportTypeId != null) {
			Id oldExpenseTypeId;
			if (oldRecord != null && oldRecord.recordItemList!=null && !oldRecord.recordItemList.isEmpty()) {
				// Record's Expense Type will be always retrieved from the item[0]
				oldExpenseTypeId = oldRecord.recordItemList[0].expTypeId;
			}

			Id newExpenseTypeId;
			if (newRecordEntity.recordItemList!=null && !newRecordEntity.recordItemList.isEmpty()) {
				// Record's Expense Type will be always retrieved from the item[0]
				newExpenseTypeId = newRecordEntity.recordItemList[0].expTypeId;
			}

			ruService.saveRecentlyUsedExpenseTypeValues(empBase.id, oldExpenseTypeId, newExpenseTypeId, reportTypeId);
		}

		return record.id;
	}

	/*
	 * Remove record items deleted on the screen from the parent record.
	 * @param newRecord Parent record passed.
	 * @param oldRecord Old parent record before updating.
	 */
	private void removeDeletedRecordItemsFromRecord(ExpRecordEntity newRecord, ExpRecordEntity oldRecord) {
		if (oldRecord.recordItemList.isEmpty()) {
			return; // no need to delete any records
		}

		Set<Id> newRecordItemIdSet = ExpCommonUtil.getIds(newRecord.recordItemList);

		List<ExpRecordItemEntity> deletedItemList = new List<ExpRecordItemEntity>();
		for (ExpRecordItemEntity oldRecordItem : oldRecord.recordItemList) {
			// If old record item is not in new record items, add it to deleted list.
			if (!newRecordItemIdSet.contains(oldRecordItem.id)) {
				deletedItemList.add(oldRecordItem);
			}
		}

		// Delete existing ExpRecordItems if record to be saved does not contain the ExpRecordItem
		if (!deletedItemList.isEmpty()) {
			new ExpRecordRepository().deleteEntityList(deletedItemList);
		}
	}

	/*
	 * Delete expense request records, and update the total amount of Request
	 * @param List<Id> List of target expense request record IDs
	 */
	public void deleteExpRecords(List<Id> recordIdList) {
		ExpRecordRepository recordRepository = new ExpRecordRepository();
		List<ExpRecordEntity> recordList = recordRepository.getEntityListByIds(recordIdList);

		// If number of Records found doesn't match with number of Ids, throw the exception
		if (recordIdList.size() != recordList.size()) {
			App.RecordNotFoundException e = new App.RecordNotFoundException();
			e.setMessage(ComMessage.msg().Exp_Err_NotFoundRecord);
			throw e;
		}

		// Since it will be checked that all Records belong to the same Report (below), can take any Record
		Id targetExpReportId = recordList[0].expReportId;

		if (targetExpReportId != null) {
			// NO need to process the Record Type and File attachement if there is no Parent Report Type
			setRecordTypeAndFileAttachmentToRecords(recordList);
		}

		Set<Id> expRecordIdSet = new Set<Id>();
		Decimal recordTotalAmount = 0;
		Decimal totalMobileIncompatibleCount = 0;
		for (ExpRecordEntity recordEntity: recordList) {
			expRecordIdSet.add(recordEntity.expReportId);

			recordTotalAmount += recordEntity.amount;
			if (!isMobileCompatible(recordEntity)) {
				totalMobileIncompatibleCount++;
			}
		}

		if (expRecordIdSet.size() != 1) {
			// If the set contains more than one, then it means the records belong to different ExpReport. Throw Exception.
			throw new App.ParameterException(App.ERR_CODE_UNSUPPORTED_VALUE,
					ComMessage.msg().Com_Err_InvalidData(new List<String>{'expReportId'}));
		}

		if (targetExpReportId != null && (recordTotalAmount != 0 || totalMobileIncompatibleCount != 0)) {
			// Only need to update if the records belong to a Report, and
			 // Total Amount of Record is non-zero or no-Non mobile Incompatible Record
			ExpReportRepository reportRepository = new ExpReportRepository();
			ExpReportEntity reportEntity = reportRepository.getEntity(targetExpReportId);
			reportEntity.totalAmount = reportEntity.totalAmount - recordTotalAmount;
			reportEntity.mobileIncompatibleCount -= totalMobileIncompatibleCount;
			try {
				reportRepository.saveEntity(reportEntity);
			} catch (DmlException e) {
				throwOutOfRangeException(e);
			}
		}
		
		// Delete ExpRecord Edit History records
		ExpModificationHistoryRepository.SearchFilter expHistoryFilter = new ExpModificationHistoryRepository.SearchFilter();
		expHistoryFilter.recordIds = new Set<Id>();
		expHistoryFilter.recordIds.addAll(recordIdList);
		ExpModificationHistoryRepository expHistoryRepo = new ExpModificationHistoryRepository();
		List<ExpModificationHistoryEntity> historyEntities = expHistoryRepo.searchEntity(expHistoryFilter);
		if (!historyEntities.isEmpty()) {
			expHistoryRepo.deleteEntityList(historyEntities);
		}

		recordRepository.deleteEntityList(recordList);
	}

	/*
	 * Set employee history ID to Record
	 * @param empBase EmployeeBaseEntity used to retrieve the employee history
	 * @param record ExpRecordEntity to set the Employee History To
 	 */
	private void setEmployeeHistoryIdToRecord(EmployeeBaseEntity empBase, ExpRecordEntity record) {
		EmployeeHistoryEntity empHistory = new EmployeeService().getEmployee(empBase.id, record.recordDate).getHistory(0);
		record.employeeHistoryId = empHistory.id;
	}

	/*
	 * Set Record type and File Attachment to Record from the Expense Type
	 * @param recordList List of ExpRecordEntity to set the values to.
 	 */
	private void setRecordTypeAndFileAttachmentToRecords(List<ExpRecordEntity> recordList) {
		// Record's Record Type will be always retrieved from the item[0]
		// When Record Type is Hotel Fee, then item[0] contains the Record data. Otherwise, there will always be only 1 recordItem in the record
		List<Id> expTypeIdList = new List<Id>();
		for (ExpRecordEntity recordEntity : recordList) {
			if (recordEntity != null) {
				expTypeIdList.add(recordEntity.recordItemList[0].expTypeId);
			}
		}
		List<ExpTypeEntity> expTypeEntityList = new ExpTypeRepository().getEntityList(expTypeIdList);

		Map<Id, ExpTypeEntity> expTypeEntityMap = new Map<Id, ExpTypeEntity>();
		for (ExpTypeEntity expType: expTypeEntityList) {
			expTypeEntityMap.put(expType.id, expType);
		}

		for (ExpRecordEntity recordEntity : recordList) {
			if (recordEntity != null) {
				ExpTypeEntity expType = expTypeEntityMap.get(recordEntity.recordItemList[0].expTypeId);
				recordEntity.recordType = expType.recordType;
				recordEntity.fileAttachment = expType.fileAttachment;
			}
		}
	}

	/*
	 * Save receipt attached to the record, and if the receipt is changed, delete the existing link.
	 * NOTE: Currently only 1 receipt is attached per record
	 * @param record ExpRecordEntity containing the Receipt
	 */
	private void saveRecordReceipt(ExpRecordEntity record, EmployeeBaseEntity empBase) {
		// NOTE: Currently only 1 receipt is attached per Record

		// Get Existing Receipt associated with the record
		AppContentDocumentRepository contentRepo = new AppContentDocumentRepository();

		List<AppContentDocumentLinkEntity> existingContentDocumentLinkEntityList = contentRepo.getLinkEntityListByLinkedEntityId(new List<Id>{record.id});
		List<AppContentDocumentLinkEntity> deleteLinkList = new List<AppContentDocumentLinkEntity>();

		AppContentDocumentLinkEntity link = getLinkEntityFromRecord(record);

		Boolean needToCreateLink = (link != null);
		for (AppContentDocumentLinkEntity existingLink : existingContentDocumentLinkEntityList) {
			if (link == null) {
				deleteLinkList.add(existingLink);
			} else if (link.contentDocumentId != existingLink.contentDocumentId) {
				deleteLinkList.add(existingLink);
			} else if (existingLink.linkedEntityId == record.id &&
				existingLink.contentDocumentId == link.contentDocumentId) {
				needToCreateLink = false;
			}
		}
		if (deleteLinkList.size() > 0) {
			// From the ContentDocument, get any existing contentDocumentLink and add those to the deleteLinkList
			List<AppContentDocumentEntity> existingContentDocumentEntityList = contentRepo.getEntityList(new List<Id>{existingContentDocumentLinkEntityList[0].contentDocumentId});
			for (AppContentDocumentEntity existingDocument : existingContentDocumentEntityList) {
				for (AppContentDocumentLinkEntity existingDocumentLink : existingDocument.contentDocumentLinkList) {
					// Delete links to both ExpRecord and ExpReportRequest
					if (existingDocumentLink.linkedEntityId != empBase.userId) {
						deleteLinkList.add(existingDocumentLink);
					}
				}
			}
			contentRepo.deleteEntityList(deleteLinkList);
		}

		if (needToCreateLink) {
			link.linkedEntityId = record.id;
			new AppContentDocumentRepository().saveLink(link);
		}
	}

	/*
	 * Extract the Link entity from the ExpRecordEntity
	 * @param record ExpRecordEntity to extract the AppContentDocumentLinkEntity from
	 * @return if exists, return AppContentDocumentLinkEntity. Otherwise, null.
	 */
	private static AppContentDocumentLinkEntity getLinkEntityFromRecord(ExpRecordEntity record) {
		if ((record.receiptList == null) || record.receiptList.isEmpty()) {
			return null;
		}

		AppContentDocumentEntity content = record.receiptList[0]; // now only one attachment is allowed
		if (content.contentDocumentLinkList == null || content.contentDocumentLinkList.isEmpty()) {
			return null;
		}
		AppContentDocumentLinkEntity link = content.contentDocumentLinkList[0];
		return link;
	}

	/*
	 * Clone the Expense Request Records on the given dates
	 *
	 * @param recordIds List of Record IDs to Clone
	 * @param clonedRecordTargetDateList List of target date to create the clone records
	 * @return Map with the following structure Map<K: Id of newly created record, V: Entity if Changed; else null>
	 */
	public Map<Id, ExpRecordEntity> cloneExpenseRecords(List<Id> recordIds, List<AppDate> clonedRecordTargetDateList) {
		List<ExpRecordEntity> recordEntityList = new ExpRecordRepository().getEntityListByIds(recordIds);
		final ComMsgBase MSG = ComMessage.msg();
		if (recordEntityList.isEmpty() || recordEntityList.size() != recordIds.size()) {
			throw new App.RecordNotFoundException(MSG.Com_Err_RecordNotFound);
		}

		// Verify that all Records Belong to the same Request
		Set<Id> expReportIdSet = new Set<Id>();
		for (ExpRecordEntity recordEntity : recordEntityList) {
			expReportIdSet.add(recordEntity.expReportId);
		}
		if (expReportIdSet.size() != 1) {
			throw new App.ParameterException(MSG.Exp_Err_CannotCloneRecordsFromDifferentRequest);
		}

		Id reportId = recordEntityList[0].expReportId;
		Decimal totalAmountByClonedRecords = 0;

		Set<Id> expenseTypeIdSet = new Set<Id>();
		Set<Id> taxTypeBaseIdSet = new Set<Id>();
		Set<Id> currencyIdSet = new Set<Id>();
		// Pre Process the Record and Collect the ID needed for further processing
		processForRecordCloningAndCollectIds(recordEntityList, expenseTypeIdSet, taxTypeBaseIdSet, currencyIdSet);

		// Find Earlier and Latest Date from the Given date List
		AppDate earliestDate = ValidPeriodEntity.VALID_TO_MAX;
		AppDate latestDate = ValidPeriodEntity.VALID_FROM_MIN;
		for (AppDate targetDate : clonedRecordTargetDateList) {
			if (targetDate.isBefore(earliestDate)) {
				earliestDate = targetDate;
			}
			if (targetDate.isAfter(latestDate)) {
				latestDate = targetDate;
			}
		}

		// Check that all Expenses are Valid at the given date
		ExpTypeRepository.SearchFilter expTypeFilter = new ExpTypeRepository.SearchFilter();
		expTypeFilter.ids = expenseTypeIdSet;
		expTypeFilter.validFrom = earliestDate;
		expTypeFilter.validTo = latestDate;
		List<ExpTypeEntity> expTypeEntityList = (new ExpTypeRepository()).searchEntity(expTypeFilter);
		// Since All the Expense Type in the Set will be used at all the given target Dates, if the return size doesn't match, then error.
		if (expTypeEntityList.size() != expenseTypeIdSet.size()) {
			throw new App.RecordNotFoundException(MSG.Com_Err_OutOfValidPeriod(
				new List<String>{MSG.Exp_Lbl_ExpenseType}));
		}

		// Can take from any since all the Expense type will be belong to the same Company
		CompanyEntity company = ExpCommonUtil.getCompany(expTypeEntityList[0].companyId);
		if (company == null) {
			throw new App.RecordNotFoundException(MSG.Com_Err_RecordNotFound);
		}

		// Get Tax Information in a map => Map<K: Tax Base ID, V: Map<K: Target Date, V: Tax History Entity>>
		Map<Id, Map<AppDate, ExpTaxTypeHistoryEntity>> baseTargetHistoryMap = getTaxTypeMapForRecordCloning(taxTypeBaseIdSet, clonedRecordTargetDateList, earliestDate, latestDate);

		// Get Exchange Rate Information in a map =>  Map<K: Currency ID, V: Map<K: Target Date, V: Exchange Rate>>
		Map<Id, Map<AppDate, Decimal>> currencyTargetExchangeRateEntityMap = getExchangeRateMapForRecordCloning(
			currencyIdSet, clonedRecordTargetDateList, earliestDate, latestDate);

		CurrencyRepository.SearchFilter filter = new CurrencyRepository.SearchFilter();
		filter.ids = new Set<Id> {company.currencyId};

		// Since only looking for company currency
		CurrencyEntity companyCurrency = new CurrencyRepository().searchEntity(filter, false)[0];

		List<ExpRecordEntity> expRecordEntityList = new List<ExpRecordEntity>();
		Set<Integer> autoUpdatedIndexSet = new Set<Integer>();
		Decimal totalMobileIncompatibleCount = 0;
		for (AppDate targetDate : clonedRecordTargetDateList) {
			for (ExpRecordEntity originalRecord : recordEntityList) {
				ExpRecordEntity clonedRecord = getCloneWithCloneableFields(originalRecord);
				if (!isMobileCompatible(clonedRecord)) {
					totalMobileIncompatibleCount++;
				}
				Boolean autoUpdated = false;
				// Update the Date to the Target Date & Parent Report ID
				clonedRecord.expReportId = reportId;
				clonedRecord.recordDate = targetDate;
				clonedRecord.withoutTax = 0;
				clonedRecord.amount = 0;
				// If Record Type is Hotel Fee, don't do anything since the Item 0 doesn't contain Tax or Exchange Rate data
				if (clonedRecord.recordType != ExpRecordType.HOTEL_FEE) {
					for (ExpRecordItemEntity clonedRecordItem : clonedRecord.recordItemList) {
						clonedRecordItem.itemDate = targetDate;
						if (clonedRecordItem.useForeignCurrency == true) {
							autoUpdated = processExchangeRateDataForRecordCloning(clonedRecordItem, currencyTargetExchangeRateEntityMap, companyCurrency.decimalPlaces);
						} else {
							autoUpdated = processTaxRateDataForRecordCloning(clonedRecordItem, baseTargetHistoryMap, companyCurrency.decimalPlaces);
							clonedRecord.withoutTax += clonedRecordItem.withoutTax;
						}
						clonedRecord.amount += clonedRecordItem.amount;
					}
				} else {
					ExpRecordItemEntity clonedRecordItem = clonedRecord.recordItemList[0];
					if (clonedRecordItem.useForeignCurrency == true) {
						clonedRecordItem.amount = 0;
					} else {
						clonedRecordItem.gstVat = 0;
						clonedRecordItem.withoutTax = 0;
					}
					clonedRecord.amount += clonedRecordItem.amount;
				}
				totalAmountByClonedRecords += clonedRecord.amount;
				expRecordEntityList.add(clonedRecord);
				if (autoUpdated == true) {
					autoUpdatedIndexSet.add(expRecordEntityList.size() - 1);
				}
			}
		}

		Map<Id, ExpRecordEntity> clonedRecordMap = new Map<Id, ExpRecordEntity>();
		if (!expRecordEntityList.isEmpty()) {
			try {
				ExpRecordRepository.SaveRecordResult result = (ExpRecordRepository.SaveRecordResult) new ExpRecordRepository().saveEntityList(expRecordEntityList);
				if (result.isSuccessAll != true) {
					for (Repository.ResultDetail resultDetail: result.details) {
						if (resultDetail.isSuccess != true) {
							throw new App.IllegalStateException(App.ERR_CODE_UNSUPPORTED_VALUE, resultDetail.errorMessage);
						}
					}
				}
				for (integer i = 0; i < expRecordEntityList.size(); i++) {
					expRecordEntityList[i].setId(result.details[i].id);
					if (autoUpdatedIndexSet.contains(i)) {
						clonedRecordMap.put(expRecordEntityList[i].id, expRecordEntityList[i]);
					} else {
						clonedRecordMap.put(expRecordEntityList[i].id, null);
					}
				}
				// Need to Update Report Total Amount
				if (totalAmountByClonedRecords != 0 || totalMobileIncompatibleCount != 0) {
					ExpReportRepository reportRepo = new ExpReportRepository();
					ExpReportEntity reportEntity = reportRepo.getEntity(reportId);
					if (reportEntity == null) {
						throw new App.RecordNotFoundException(MSG.Exp_Err_ReportNotFound);
					}
					reportEntity.totalAmount += totalAmountByClonedRecords;
					reportEntity.mobileIncompatibleCount += totalMobileIncompatibleCount;
					reportRepo.saveEntity(reportEntity);
				}
			} catch (DmlException e) {
				if (e.getDmlType(0) == System.StatusCode.NUMBER_OUTSIDE_VALID_RANGE) {
					throw new App.IllegalStateException(App.ERR_CODE_UNSUPPORTED_VALUE, MSG.Com_Err_InvalidValueTooLarge(new List<String>{ComMessage.msg().Exp_Lbl_Amount}));
				}
				throw e;
			}
		}
		return clonedRecordMap;
	}

	/*
	 * Create a new instance from the provided original record (shallow clone)
	 * @param originalRecord Record to copy the data from
	 * @return ExpRecordEntity a new entity containing the cloneable data
	 */
	public static ExpRecordEntity getCloneWithCloneableFields(ExpRecordEntity originalRecord) {
		ExpRecordEntity clonedRecord = new ExpRecordEntity();
		clonedRecord.name = originalRecord.name;
		clonedRecord.employeeHistoryId = originalRecord.employeeHistoryId;
		clonedRecord.recordDate = originalRecord.recordDate;
		clonedRecord.amount = originalRecord.amount;
		clonedRecord.withoutTax = originalRecord.withoutTax;
		clonedRecord.localAmount = originalRecord.localAmount;
		clonedRecord.order = originalRecord.order;
		clonedRecord.recordType = originalRecord.recordType;
		clonedRecord.typeData = originalRecord.typeData;

		List<ExpRecordItemEntity> expRecordItemEntityList = new List<ExpRecordItemEntity>();
		for (ExpRecordItemEntity originalRecordItem : originalRecord.recordItemList) {
			ExpRecordItemEntity clonedRecordItem = new ExpRecordItemEntity();
			clonedRecordItem.name = originalRecordItem.name;
			clonedRecordItem.localAmount = originalRecordItem.localAmount;
			clonedRecordItem.itemDate = originalRecordItem.itemDate;
			clonedRecordItem.expTypeId = originalRecordItem.expTypeId;
			clonedRecordItem.expTypeNameL = originalRecordItem.expTypeNameL;	//READ ONLY (FOR REFERENCE)
			clonedRecordItem.amount = originalRecordItem.amount;
			clonedRecordItem.withoutTax = originalRecordItem.withoutTax;
			clonedRecordItem.taxTypeHistoryId = originalRecordItem.taxTypeHistoryId;
			clonedRecordItem.taxRate = originalRecordItem.taxRate;
			clonedRecordItem.taxTypeBaseId = originalRecordItem.taxTypeBaseId;
			clonedRecordItem.taxTypeCode = originalRecordItem.taxTypeCode;
			clonedRecordItem.gstVat = originalRecordItem.gstVat;
			clonedRecordItem.taxManual = originalRecordItem.taxManual;
			clonedRecordItem.useFixedForeignCurrency = originalRecordItem.useFixedForeignCurrency;
			clonedRecordItem.useForeignCurrency = originalRecordItem.useForeignCurrency;
			clonedRecordItem.currencyId = originalRecordItem.currencyId;
			clonedRecordItem.currencyInfo = originalRecordItem.currencyInfo;
			clonedRecordItem.exchangeRate = originalRecordItem.exchangeRate;
			clonedRecordItem.originalExchangeRate = originalRecordItem.originalExchangeRate;
			clonedRecordItem.exchangeRateManual = originalRecordItem.exchangeRateManual;
			clonedRecordItem.remarks = originalRecordItem.remarks;
			clonedRecordItem.order = originalRecordItem.order;
			clonedRecordItem.jobId = originalRecordItem.jobId;
			clonedRecordItem.jobCode = originalRecordItem.jobCode;
			clonedRecordItem.costCenterHistoryId = originalRecordItem.costCenterHistoryId;
			clonedRecordItem.costCenterLinkageCode = originalRecordItem.costCenterLinkageCode;
			clonedRecordItem.fixedAllowanceOptionId = originalRecordItem.fixedAllowanceOptionId;

			for (Integer i = 0; i < ExpExtendedItemEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				// Only Lookup ID needs to be saved, and the other IDs are retrieved directly from the Expense Type
				clonedRecordItem.setExtendedItemLookupId(i, originalRecordItem.getExtendedItemLookupId(i));

				clonedRecordItem.setExtendedItemDateValue(i, originalRecordItem.getExtendedItemDateValue(i));
				clonedRecordItem.setExtendedItemLookupValue(i, originalRecordItem.getExtendedItemLookupValue(i));
				clonedRecordItem.setExtendedItemPicklistValue(i, originalRecordItem.getExtendedItemPicklistValue(i));
				clonedRecordItem.setExtendedItemTextValue(i, originalRecordItem.getExtendedItemTextValue(i));
			}

			expRecordItemEntityList.add(clonedRecordItem);
		}
		clonedRecord.replaceRecordItemList(expRecordItemEntityList);
		return clonedRecord;
	}

	/*
	 * Pre-process the Records before cloning, and collect the necessary IDs form the Records and Record Items.
	 * @param recordEntityList List of source Records
	 * @param expenseTypeIdSet Set to populate the Expense Type ID to
	 * @param taxTypeBaseIdSet Set to populate the Tax Type Base ID to
	 * @param currencyIdSet Set to populate the Currency ID to
	 */
	private static void processForRecordCloningAndCollectIds(List<ExpRecordEntity> recordEntityList, Set<Id> expenseTypeIdSet, Set<Id> taxTypeBaseIdSet, Set<Id> currencyIdSet) {
		for (ExpRecordEntity recordEntity : recordEntityList) {
			// Reset Itemization Record Items before cloning
			if (recordEntity.recordType == ExpRecordType.HOTEL_FEE) {
				recordEntity.replaceRecordItemList(new List<ExpRecordItemEntity>{recordEntity.recordItemList[0]});
			}

			for (ExpRecordItemEntity recordItemEntity : recordEntity.recordItemList) {
				//  Only verify the Tax Type and Exchange Rate if it's not Hotel Fee
				 //  as Hotel Fee doesn't have tax and exchange rate on the Base Record (meaning RecordItem[0])
				if (recordEntity.recordType != ExpRecordType.HOTEL_FEE) {
					// Update with the correct tax amount on the given day
					if (recordItemEntity.useForeignCurrency == False) {
						taxTypeBaseIdSet.add(recordItemEntity.taxTypeBaseId);
					} else if (recordItemEntity.useForeignCurrency == True) {
						currencyIdSet.add(recordItemEntity.currencyId);
					}
				}
				expenseTypeIdSet.add(recordItemEntity.expTypeId);
			}
		}
	}

	/*
	 * Validate Record
	 */
	private void validateRecord(ExpRecordEntity record) {
		// ToDo : will add validation (ExpValidator.checkRecord)
		// some features are yet to be implemented for mobile app.
		// so data can be inconsistent.
		// after enough features are implemented, will add validations.
	}

	/*
	 * Throws an IllegalStateException with custom message if exception is of Invalid Number Range
	 * @param e DmlException
	 */
	private static void throwOutOfRangeException(DmlException e) {
		if (e.getDmlType(0) == System.StatusCode.NUMBER_OUTSIDE_VALID_RANGE) {
			throw new App.IllegalStateException(
					App.ERR_CODE_UNSUPPORTED_VALUE,
					ComMessage.msg().Com_Err_InvalidValueTooLarge(new List<String>{ComMessage.msg().Exp_Lbl_Amount}));
		}
		throw e;
	}

	private static Boolean isMobileCompatible(ExpRecordEntity recordEntity) {
		if (recordEntity.recordType == ExpRecordType.HOTEL_FEE) {
			return false;
		}
		return true;
	}

	/*
	 * To Filter out the Record based on the Expense Type Id
	 * @param recordEntityList Record Entity List to filter
	 * @param expTypeIdList Expense Type Id List
	 * @return List of Expense Record Entity
	 */
	private List<ExpRecordEntity> filterRecordListByExpType(List<ExpRecordEntity> recordEntityList, List<Id> expTypeIdList) {
		Set<Id> expTypeIdSet = new Set<Id>();
		expTypeIdSet.addAll(expTypeIdList);

		List<ExpRecordEntity> listToReturn = new List<ExpRecordEntity>();

		for (ExpRecordEntity entity : recordEntityList) {
			// Record's Expense Type will be always retrieved from the item[0]
			if (expTypeIdSet.contains(entity.recordItemList[0].expTypeId)) {
				listToReturn.add(entity);
			}
		}
		return listToReturn;
	}

}