/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description ジョブレコードを操作するAPIを実装するクラス
*/
public with sharing class JobResource {

	/**
	 * @description ジョブを表すクラス
	 */
	public class Job implements RemoteApi.RequestParam {
		/** ジョブのレコードID */
		public String id;
		/** ジョブ名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** ジョブ名(言語0) */
		public String name_L0;
		/** ジョブ名(言語1) */
		public String name_L1;
		/** ジョブ名(言語2) */
		public String name_L2;
		/** ジョブコード */
		public String code;
		/** 会社のレコードID */
		public String companyId;
		/** ジョブタイプID */
		public String jobTypeId;
		/** ジョブタイプのルックアップ */
		public LookupField jobType;
		/** 部署のレコードID */
		public String departmentId;
		/** 部署のルックアップ */
		public LookupField department;
		/** 親ジョブのレコードID */
		public String parentId;
		/** 親ジョブのルックアップ項目 */
		public LookupField parent;
		/** Hierarchy Parent Name List of Job */
		public List<String> hierarchyParentNameList;
		/** ジョブオーナー(社員)のレコードID */
		public String jobOwnerId;
		/** ジョブオーナー(社員)のルックアップ項目 */
		public LookupField jobOwner;
		/** 有効期間の開始日(yyyy-MM-dd) */
		public Date validDateFrom;
		/** 有効期間の終了日(yyyy-MM-dd) */
		public Date validDateTo;
		/** 直課フラグ */
		public Boolean isDirectCharged;
		/** アサイン個別指定フラグ */
		public Boolean isScopedAssignment;
		/** 経費精算選択可能フラグ */
		public Boolean isSelectableExpense;
		/** 工数選択可能フラグ */
		public Boolean isSelectableTimeTrack;
	}

	/**
	 * @description ルックアップ項目を表すクラス
	 */
	public class LookupField implements RemoteApi.ResponseParam {
		/** レコードのName項目値 */
		public String name;
	}

	/**
	 * @description ジョブレコード作成結果
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成されたレコードID */
		public String id;
	}

	/**
	 * @description ジョブレコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB;

		/**
		 * @description ジョブレコードを1件更新する
		 * @param req リクエストパラメータ (JobResource.Job)
		 * @return レスポンス (JobResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			JobResource.Job param = (JobResource.Job)req.getParam(JobResource.Job.class);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			Id recordId = JobResource.saveRecord(param, req.getParamMap(), false);

			SaveResult res = new SaveResult();
			res.id = recordId;
			return res;
		}

	}

	/**
	 * @description ジョブレコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB;

		/**
		 * @description ジョブレコードを1件更新する
		 * @param  req リクエストパラメータ (JobResource.Job)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			JobResource.Job param = (JobResource.Job)req.getParam(JobResource.Job.class);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			JobResource.saveRecord(param, req.getParamMap(), true);
			return null;
		}

	}

	/**
	 * @description ジョブレコード削除処理のパラメータクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
	}

	/**
	 * @description ジョブレコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB;

		/**
		 * @description ジョブレコードを1件削除する
		 * @param  req リクエストパラメータ (JobResource.DeleteOption)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// IDがブランクの場合はエラー
			if (String.isBlank(param.id)) {
				throw new App.ParameterException('パラメータ "id" を指定して下さい');
			}

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new JobRepository().deleteEntity(param.Id);
				// AppDatabase.doDelete(param.Id);
			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

					// TODO 暫定的にデバッグログを出力しておきます
					System.debug(e);

					// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
					//      暫定対応として下記のようなエラーメッセージで対応します。
					// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
					e.setMessage(ComMessage.msg().Com_Err_FaildDeleteReference);
					throw e;
				}
		 	}

			return null;
		}
	}

	/**
	 * @description ジョブレコード検索処理のパラメータクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** ジョブレコードID */
		public String id;
		/** 会社のレコードID */
		public String companyId;
		/** 部署のレコードID */
		public String departmentId;
		/** Employee Base Id */
		public String empId;
		/** 親ジョブのレコードID */
		public String parentId;
		/** Job Code or Job Name (In mobile is null) */
		public String query;
		/** Target Date */
		public String targetDate;
		/** Specifies whether to add in Hierarchy Job Parent Names */
		public String usedIn;

		public void validate() {
			// All the name are not referenced to user setting since those error are targeted to developer
			ExpCommonUtil.validateId('id', this.id, false);
			ExpCommonUtil.validateId('companyId', this.companyId, false);
			ExpCommonUtil.validateId('departmentId', this.departmentId, false);
			ExpCommonUtil.validateId('empId', this.empId, false);
			ExpCommonUtil.validateId('parentId', this.parentId, false);

			if (String.isNotBlank(targetDate)) {
				ExpCommonUtil.validateDate('targetDate', this.targetDate);
			}

			if (String.isNotBlank(usedIn)) {
				ExpCommonUtil.validateUsedIn('usedIn', this.usedIn);
			}
		}
	}

	/**
	 * @description ジョブレコード検索処理のレスポンスクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public Job[] records;
	}

	/**
	 * @description ジョブレコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description ジョブレコードを検索する
		 * @param  req リクエストパラメータ (JobResource.SearchCondition)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);
			param.validate();
			Map<Id, List<String>> hierarchyParentNamesMap = null; // Map is empty when called from Admin page
			AppDate targetDate = null;// if it's used for Admin targetDate should be null
			if (String.isNotBlank(param.usedIn)) { // Jobs need to be filtered by employee id
				targetDate = String.isNotBlank(param.targetDate) ? AppDate.valueOf(param.targetDate) : AppDate.today();
				hierarchyParentNamesMap = new Map<Id, List<String>>();// Instantiate to indicate that it is called from other pages and need to filter Job Search by Employee
			}

			JobService service = new JobService();
			// レコードを検索
			List<JobEntity> searchRecords = service.searchJobList(param.id, param.companyId, param.parentId, targetDate, param.query, hierarchyParentNamesMap, param.empId);
			if (hierarchyParentNamesMap == null) {
				hierarchyParentNamesMap = new Map<Id, List<String>>();
			}
			// 現状の仕様に従い論理削除も取得対象(もともとはComJob__c.DepartmentBaseId__r.CurrentHistoryId__rから取得していた)
			Set<Id> deptBaseIds = new Set<Id>();
			for (JobEntity sr : searchRecords) {
				if (sr.departmentBaseId != null) {
					deptBaseIds.add(sr.departmentBaseId);
				}
			}
			DepartmentRepository.SearchFilter deptFilter = new DepartmentRepository.SearchFilter();
			deptFilter.includeRemoved = true;
			deptFilter.baseIds = new List<Id>(deptBaseIds);
			List<DepartmentBaseEntity> deptBases = new DepartmentRepository().searchEntity(deptFilter);
			Map<Id, DepartmentBaseEntity> deptBaseMap = new Map<Id, DepartmentBaseEntity>();
			for (DepartmentBaseEntity deptBase : deptBases) {
				deptBaseMap.put(deptBase.id, deptBase);
			}

			// レスポンスクラスに変換
			return generateSearchResultResponse(searchRecords, deptBaseMap, hierarchyParentNamesMap);
		}
	}

	/**
	 * Request Parameter for GetRecentlyUsedListApi
	 */
	public class GetRecentlyUsedListParam implements RemoteApi.RequestParam {
		/** Employee Base Id */
		public String employeeBaseId;
		/** Target Date (Record or Accounting Date)*/
		public String targetDate;

		/**
		 * Validate the Request Param for the GetRecentlyUsedList API
		 */
		public void validate() {
			ExpCommonUtil.validateId('employeeBaseId', this.employeeBaseId, true);

			if (String.isNotBlank(targetDate)) {
				ExpCommonUtil.validateDate('targetDate', this.targetDate);
			}
		}

	}

	/**
	 * API to get recently used Job by employee
	 * If the recently used Job is not valid at the point of the retrieval; it is not return as recently used
	 */
	public with sharing class GetRecentlyUsedListApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			JobResource.GetRecentlyUsedListParam param = (JobResource.GetRecentlyUsedListParam) req.getParam(JobResource.GetRecentlyUsedListParam.class);
			param.validate();

			AppDate targetDate = String.isNotBlank(param.targetDate) ? AppDate.valueOf(param.targetDate) : AppDate.today();

			Map<Id, List<String>> hierarchyParentNamesMap = new Map<Id, List<String>>();
			JobService service = new JobService();

			List<JobEntity> searchRecords = service.getRecentlyUsedJobList(param.employeeBaseId, hierarchyParentNamesMap, targetDate);

			Set<Id> deptBaseIds = new Set<Id>();
			for (JobEntity sr : searchRecords) {
				if (sr.departmentBaseId != null) {
					deptBaseIds.add(sr.departmentBaseId);
				}
			}
			DepartmentRepository.SearchFilter deptFilter = new DepartmentRepository.SearchFilter();
			deptFilter.includeRemoved = true;
			deptFilter.baseIds = new List<Id>(deptBaseIds);
			List<DepartmentBaseEntity> deptBases = new DepartmentRepository().searchEntity(deptFilter);
			Map<Id, DepartmentBaseEntity> deptBaseMap = new Map<Id, DepartmentBaseEntity>();
			for (DepartmentBaseEntity deptBase : deptBases) {
				deptBaseMap.put(deptBase.id, deptBase);
			}

			return generateSearchResultResponse(searchRecords, deptBaseMap, hierarchyParentNamesMap);
		}
	}

	/**
	 * @description ジョブレコードを1件作成または更新する<br/>
	 *    ※他のクラスから参照しないで下さい<br/>
	 * @param  param ジョブのレコード値を持つパラメータ
	 * @param  paramMap ジョブのレコード値を持つパラメータのマップ
	 * @param  isUpdate レコード更新時はtrue
	 * @return 作成または更新したジョブレコード
	 */
	public static Id saveRecord(JobResource.Job param, Map<String, Object> paramMap, Boolean isUpdate) {
		Set<String> paramKeys = paramMap.keySet();

		// レコード更新時はID必須
		if (isUpdate && String.isBlank(param.id)) {
			throw new App.ParameterException('パラメータ "id" を指定してください');
		}

		JobEntity entity = new JobEntity();

		if (paramKeys.contains('name_L0')) {
			entity.name = param.name_L0;
			entity.nameL0 = param.name_L0;
		}
		if (paramKeys.contains('name_L1')) {
			entity.nameL1 = param.name_L1;
		}
		if (paramKeys.contains('name_L2')) {
			entity.nameL2 = param.name_L2;
		}
		if (paramKeys.contains('code')) {
			entity.code = param.code;
		}
		if (paramKeys.contains('companyId')) {
			entity.companyId = param.companyId;
		}
		if (paramKeys.contains('jobTypeId')) {
			entity.jobTypeId = param.jobTypeId;
		}
		if (paramKeys.contains('departmentId')) {
			entity.departmentBaseId = param.departmentId;
		}
		if (paramKeys.contains('parentId')) {
			entity.parentId = param.parentId;
		}
		if (paramKeys.contains('jobOwnerId')) {
			entity.jobOwnerBaseId = param.jobOwnerId;
		}
		if (paramKeys.contains('validDateFrom')) {
			entity.validFrom = AppDate.valueOf(param.validDateFrom);
		}
		if (paramKeys.contains('validDateTo')) {
			entity.validTo = AppDate.valueOf(param.validDateTo);
		}
		if (paramKeys.contains('isDirectCharged')) {
			entity.isDirectCharged = param.isDirectCharged;
		}
		if (paramKeys.contains('isScopedAssignment')) {
			entity.isScopedAssignment = param.isScopedAssignment;
		}
		if (paramKeys.contains('isSelectableExpense')) {
			entity.isSelectableExpense = param.isSelectableExpense;
		}
		if (paramKeys.contains('isSelectableTimeTrack')) {
			entity.isSelectableTimeTrack = param.isSelectableTimeTrack;
		}

		if (!isUpdate) {
			// 新規作成時、チェックボックス型項目は値をセットしないとINVALID_TYPE_ON_FIELD_IN_RECORDエラーが発生するため
			// デフォルト値をセットする
			if (param.isDirectCharged == null) {
				entity.isDirectCharged = (Boolean)ComJob__c.DirectCharged__c.getDescribe().getDefaultValue();
			}
			if (param.isScopedAssignment == null) {
				entity.isScopedAssignment = (Boolean)ComJob__c.ScopedAssignment__c.getDescribe().getDefaultValue();
			}
			if (param.isSelectableExpense == null) {
				entity.isSelectableExpense = (Boolean)ComJob__c.SelectabledExpense__c.getDescribe().getDefaultValue();
			}
			if (param.isSelectableTimeTrack == null) {
				entity.isSelectableTimeTrack = (Boolean)ComJob__c.SelectabledTimeTrack__c.getDescribe().getDefaultValue();
			}
		} else {
			entity.setId(param.id);
		}

		// デフォルト値設定
		if (entity.validFrom == null) {
			entity.validFrom = AppDate.today();
		}
		if (entity.validTo == null) {
			entity.validTo = ValidPeriodEntity.VALID_TO_MAX;
		}

		return new JobService().saveJobEntity(entity);
	}

	/**
	 * Generate search Job Response
	 * @param searchRecords Records to be returned
	 * @param detpBaseMap Deparment Base Entity linked with its id
	 * @hierarchyParentNamesMap Hierarchy parents name linked with job entity id
	 */
	private static SearchResult generateSearchResultResponse(List<JobEntity> searchRecords, Map<Id, DepartmentBaseEntity> deptBaseMap, Map<Id, List<String>> hierarchyParentNamesMap) {
		// レスポンスクラスに変換
		List<Job> jobList = new List<Job>();
		for (JobEntity sr : searchRecords) {
			Job j = new Job();
			j.id = sr.id;
			j.name = sr.nameL.getValue();
			j.name_L0 = sr.nameL0;
			j.name_L1 = sr.nameL1;
			j.name_L2 = sr.nameL2;
			j.code = sr.code;
			j.companyId = sr.companyId;
			j.departmentId = sr.departmentBaseId;
			j.department = new LookupField();
			if (j.departmentId != null) {
				String deptName = null;
				DepartmentBaseEntity deptBase = deptBaseMap.get(j.departmentId);
				if (deptBase != null && deptBase.currentHistoryId != null) {
					j.department.name = deptBase.getHistoryById(deptBase.currentHistoryId).nameL.getValue();
				}
			}
			j.hierarchyParentNameList = hierarchyParentNamesMap.get(sr.id) != null ? hierarchyParentNamesMap.get(sr.id) : new List<String>();

			j.parentId = sr.parentId;
			j.parent = new LookupField();
			if (sr.parentId != null) {
				j.parent.name = sr.parent.nameL.getValue();
			}
			j.jobOwnerId = sr.jobOwnerBaseId;
			j.jobOwner = new LookupField();
			if (sr.jobOwnerBaseId != null) {
				j.jobOwner.name = sr.jobOwner.fullName.getFullName();
			}
			j.jobTypeId = sr.jobTypeId;
			j.jobType = new LookupField();
			if (sr.jobTypeId != null) {
				j.jobType.name = sr.jobType.nameL.getValue();
			}
			j.validDateFrom = AppConverter.dateValue(sr.validFrom);
			j.validDateTo = AppConverter.dateValue(sr.validTo);
			j.isDirectCharged = sr.isDirectCharged;
			j.isScopedAssignment = sr.isScopedAssignment;
			j.isSelectableExpense = sr.isSelectableExpense;
			j.isSelectableTimeTrack = sr.isSelectableTimeTrack;

			jobList.add(j);
		}

		// 成功レスポンスをセットする
		SearchResult res = new SearchResult();
		res.records = jobList;
		return res;
	}

}
