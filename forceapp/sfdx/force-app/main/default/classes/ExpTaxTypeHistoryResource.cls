/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分履歴レコード操作APIを実装するクラス Resource class for ExpTaxTypeHistory object
 */
public with sharing class ExpTaxTypeHistoryResource extends Repository.BaseRepository {

	/** メッセージ Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 税区分履歴レコードを表すクラス Tax type history data class
	 */
	public class ExpTaxTypeHistory implements RemoteApi.RequestParam {
		/** 履歴レコードID */
		public String id;
		/** ベースレコードID */
		public String baseId;
		/** 税区分名 ※取得専用。組織の言語設定に応じて言語が切り替わる */
		public String name;
		/** 税区分名(言語0) */
		public String name_L0;
		/** 税区分名(言語1) */
		public String name_L1;
		/** 税区分名(言語2) */
		public String name_L2;
		/** 税率 */
		public Decimal rate;
		/** 履歴コメント */
		public String comment;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;

		/**
		 * @description 税区分履歴エンティティを作成する Create tax type history entity
		 * @param  paramMap リクエストパラメータのMap
		 * @param  isUpdate レコード更新時はtrue、作成時はfalse
		 * @return  作成or更新した税区分履歴レコード
		 */
		public ExpTaxTypeHistoryEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {
			ExpTaxTypeHistoryEntity entity = new ExpTaxTypeHistoryEntity();

			// 更新時にIDがブランクの場合はエラー
			if (isUpdate && String.isBlank(this.id)) {
				// メッセージ：IDを指定してください
				throw new App.ParameterException(MESSAGE.Com_Err_Specify(new List<String>{'ID'}));
			}

			// レコード更新時はIDをセット
			if(isUpdate){
				entity.setId(this.id);
			}
			// ベースIDは更新不可のため、新規作成時のみセット
			if (!isUpdate && paramMap.containsKey('baseId')) {
				entity.baseId = this.baseId;
			}
			if (paramMap.containsKey('name_L0')) {
				entity.name = this.name_L0;
				entity.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('rate')) {
				entity.rate = this.rate;
			}
			if (paramMap.containsKey('comment')) {
				entity.historyComment = this.comment;
			}
			if (paramMap.containsKey('validDateFrom')) {
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (entity.validFrom == null) {
				// デフォルトは実行日
				entity.validFrom = AppDate.today();
			}
			if (paramMap.containsKey('validDateTo')) {
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (entity.validTo == null) {
				// デフォルト値は失効日の最大日付
				entity.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}

			return entity;
		}
	}

	/**
	 * @description 税区分履歴レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成した履歴レコードID */
		public String id;
	}

	/**
	 * @description 税区分履歴レコード作成APIを実装するクラス API to Create a new tax type
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EXP_TAX_TYPE;

		/**
		 * @description 税区分履歴レコードを1件作成する Create a new tax type record
		 * @param  req リクエストパラメータ(ExpTaxTypeHistory) Request parameter
		 * @return レスポンス(SaveResult) Response
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			//------------------------------------------------------------------------
			// 【仮仕様】
			// 現在指定可能な改定日(有効開始日)は末尾の履歴の有効開始日以降とする。
			// 改定機能自体は実装済みでマスタ全体の有効期間内の日付で改定できるようになっているが
			// UI側が対応できていないため。
			//------------------------------------------------------------------------

			final Boolean isUpdate = false;
			ExpTaxTypeService service = new ExpTaxTypeService();
			ExpTaxTypeRepository repo = new ExpTaxTypeRepository();

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			ExpTaxTypeHistory param = (ExpTaxTypeHistory)req.getParam(ExpTaxTypeHistory.class);
			ExpTaxTypeHistoryEntity newHistory = param.createEntity(req.getParamMap(), isUpdate);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 末尾の履歴(有効開始日が最新の履歴)を取得する
			ExpTaxTypeBaseEntity base = repo.getEntity(newHistory.baseId);
			if (base == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Admin_Lbl_ExpTaxType}));
			}
			ExpTaxTypeHistoryEntity latestHistory = (ExpTaxTypeHistoryEntity)base.getLatestSuperHistory();

			// 追加の履歴の有効開始日が末尾の履歴の有効開始日以降であることを確認する
			if (newHistory.validFrom.getDate() < latestHistory.validFrom.getDate()) {
				// メッセージ：改定日は最新の履歴の有効開始日以降を指定してください
				throw new App.ParameterException(MESSAGE.Admin_Err_InvalidRevisionDate);
			}

			// マスタ全体の失効日(末尾の履歴の失効日)を更新する
			if (newHistory.validTo.getDate() != latestHistory.validTo.getDate()) {
				service.updateValidTo(newHistory.baseId, newHistory.validTo);
			}
			// 履歴を改定する
			service.reviseHistory(newHistory);

			// 改定日に有効な履歴のIDを取得する
			ExpTaxTypeBaseEntity revisedBase = repo.getEntity(newHistory.baseId, newHistory.validFrom);
			SaveResult res = new SaveResult();
			res.Id = revisedBase.getHistoryList().get(0).id;
			return res;
		}
	}

	/**
	 * @desctiprion 税区分履歴レコード更新APIを実装するクラス API to update a tax type
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/**
		 * @description 税区分履歴レコードを1件更新する Update a tax type record
		 * @param  req リクエストパラメータ(ExpTaxTypeHistory) Request parameter
		 * @return レスポンス(null) Response
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// TODO: 必要になったら実装してください。 Implement when needed
			throw new App.UnsupportedException('税区分履歴更新APIは利用できません。');

			// // パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// ExpTaxTypeHistory param = (ExpTaxTypeHistory)req.getParam(ExpTaxTypeHistory.class);
			// ExpTaxTypeResource.saveRecord(param, req.getParamMap(), true);

			// // 成功レスポンスをセットする
			// return null;
		}
	}

	/**
	 * @description 税区分レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// ID
			ExpCommonUtil.validateId('id', this.id, true);
		}
	}

	/**
	 * 税区分レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EXP_TAX_TYPE;

		/**
		 * @description 税区分履歴レコードを1件削除する(論理削除)
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);
			try {
				// レコードを削除
				new ExpTaxTypeService().deleteHistoryListWithPropagation(new List<Id>{param.id});
			} catch (App.RecordNotFoundException e) {
				// すでに削除済みの場合は成功レスポンスを返す
			}

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 税区分履歴レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** 税区分履歴レコードID */
		public String id;
		/** 関連する税区分ベースレコードID */
		public String baseId;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {
			// 履歴ID
			ExpCommonUtil.validateId('empId', this.id, false);

			// ベースID
			ExpCommonUtil.validateId('empId', this.baseId, false);
		}
	}

	/**
	 * @description 税区分履歴レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public ExpTaxTypeHistory[] records;
	}

	/**
	 * @description 税区分履歴レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 税区分履歴レコードを検索する
		 * @param  req リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);

			// パラメータのバリデーション
			param.validate();

			// 税区分履歴を検索
			ExpTaxTypeRepository.SearchFilter filter = new ExpTaxTypeRepository.SearchFilter();
			filter.historyIds = String.isBlank(param.id) ? null : new List<Id>{param.id};
			filter.baseIds = String.isBlank(param.baseId) ? null : new List<Id>{param.baseId};
			List<ExpTaxTypeBaseEntity> baseEntityList =
					(new ExpTaxTypeRepository()).searchEntityWithHistory(filter, false, ExpTaxTypeRepository.HistorySortOrder.VALID_FROM_DESC);

			// エンティティをレスポンスオブジェクトに変換
			List<ExpTaxTypeHistory> historyList = new List<ExpTaxTypeHistory>();
			for (ExpTaxTypeBaseEntity baseEntity : baseEntityList) {
				for (ExpTaxTypeHistoryEntity historyEntity : baseEntity.getHistoryList()) {
					ExpTaxTypeHistory history = new ExpTaxTypeHistory();
					history.id = historyEntity.id;
					history.baseId = historyEntity.baseId;
					history.name = historyEntity.nameL.getValue();
					history.name_L0 = historyEntity.nameL.valueL0;
					history.name_L1 = historyEntity.nameL.valueL1;
					history.name_L2 = historyEntity.nameL.valueL2;
					history.validDateFrom = AppConverter.dateValue(historyEntity.validFrom);
					history.validDateTo = AppConverter.dateValue(historyEntity.validTo);
					history.rate = historyEntity.rate;
					history.comment = historyEntity.historyComment;

					historyList.add(history);
				}
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = historyList;
			return res;
		}
	}
}