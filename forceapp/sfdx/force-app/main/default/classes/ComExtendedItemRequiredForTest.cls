/**
 * ComExtendedItemRequiredForのテスト
 */
@isTest
private class ComExtendedItemRequiredForTest {

	/**
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(ComExtendedItemRequiredFor.EXPENSE_REPORT, ComExtendedItemRequiredFor.valueOf('ExpenseReport'));
		System.assertEquals(ComExtendedItemRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ComExtendedItemRequiredFor.valueOf('ExpenseRequestAndExpenseReport'));

		// 等しくない場合(値が異なる) → false
		System.assertEquals(false, ComExtendedItemRequiredFor.EXPENSE_REPORT.equals(ComExtendedItemRequiredFor.valueOf('ExpenseRequestAndExpenseReport')));

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, ComExtendedItemRequiredFor.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// 等しくない場合(null)
		System.assertEquals(false, ComExtendedItemRequiredFor.EXPENSE_REPORT.equals(null));
	}
}