/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * コンテンツドキュメントエンティティ
 */
public with sharing class AppContentDocumentEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		CONTENT_DOCUMENT_LINK_LIST
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/** 所有者ID（参照のみ。値を変更してもDBに保存されない。） */
	public Id ownerId {get; set;}

	/** タイトル（参照のみ。値を変更してもDBに保存されない。） */
	public String title {get; set;}

	/** ファイルタイプ（参照のみ。値を変更してもDBに保存されない。） */
	public String fileType {get; set;}

	/** 最新コンテンツバージョンID（参照のみ。値を変更してもDBに保存されない。） */
	public ID latestPublishedVersionId {get; set;}

	/** 変更日時（参照のみ。値を変更してもDBに保存されない。） */
	public AppDatetime contentModifiedDate {get; set;}

	/** コンテンツドキュメントリンクリスト */
	public List<AppContentDocumentLinkEntity> contentDocumentLinkList {get; private set;}

	/** コンストラクタ */
	public AppContentDocumentEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/**
	 * コンテンツドキュメントリンクリストをリプレースする（リストをコピーする）
	 * @param contentDocumentLinkList リプレース対象のコンテンツドキュメントリンクリスト
	 */
	public void replaceContentDocumentLinkList(List<AppContentDocumentLinkEntity> contentDocumentLinkList) {
		this.contentDocumentLinkList = new List<AppContentDocumentLinkEntity>(contentDocumentLinkList);
		setChanged(Field.CONTENT_DOCUMENT_LINK_LIST);
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
		// コンテンツドキュメントリンクリストの変更もリセットする
		if (this.contentDocumentLinkList != null) {
			for (AppContentDocumentLinkEntity entity : this.contentDocumentLinkList) {
				entity.resetChanged();
			}
		}
	}
}