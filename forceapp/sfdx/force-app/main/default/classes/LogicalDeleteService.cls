/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 履歴管理方式が論理削除型マスタのサービスクラス
 */
public with sharing class LogicalDeleteService implements Service {

	/** 論理削除型のリポジトリ */
	private LogicalDeleteRepository repository;

	/**
	 * コンストラクタ
	 * @param repository 論理削除型のリポジトリ
	 */
	public LogicalDeleteService(LogicalDeleteRepository repository) {
		this.repository = repository;
	}

	/**
	 * エンティティを削除する。
	 * NOTE:論理削除型のマスタは、レコードが参照されている場合は論理削除する方針だが、検討が十分ではないため現段階では実装せず、一律物理削除とする。
	 * @param id 削除対象のID
	 * @throw DmlException 参照しているレコードが存在する場合
	 */
	public void deleteEntity(Id id) {
		try {
			repository.deleteEntity(id);
		} catch (DmlException e) {
			if (e.getDmlType(0) == System.StatusCode.DELETE_FAILED) {
 				e.setMessage(ComMessage.msg().Com_Err_FaildDeleteReference);
			}
			throw e;
		}
	}

	/**
	 * エンティティを削除する。
	 * NOTE:論理削除型のマスタは、レコードが参照されている場合は論理削除する方針だが、検討が十分ではないため現段階では実装せず、一律物理削除とする。
	 * @param entity 削除対象のエンティティ
	 * @throw DmlException 参照しているレコードが存在する場合
	 */
	public void deleteEntity(LogicalDeleteEntity entity) {
		deleteEntity(entity.id);
	}

}