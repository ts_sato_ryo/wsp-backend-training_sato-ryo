/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Service class of Expense Request Approval
 */
public with sharing class ExpRequestApprovalService {

	private static final String ACTION_TYPE_REMOVED = 'Removed';

	/** Prefix of Request No */
	private static String REQUEST_NO_PREFIX = 'pre-req';

	/** Report Type Validation Bypassing */
	@TestVisible
	private static Boolean isTestModeAndBypassExpRequestValidation = false;
	
	/**
	 * Submits a Expense Request for approval
	 * @param requestId Target record ID to submit
	 * @param comment application comment
	 */
	public static void submitRequest(Id requestId, String comment) {

		// Check if target data exists
		ExpRequestEntity entity = checkDataExists(requestId);

		// Get employee info
		EmployeeBaseEntity empBase = new EmployeeService().getEmployee(entity.employeeBaseId, entity.recordingDate);

		// Validate entity
		if (!isTestModeAndBypassExpRequestValidation) {
			validateRequest(entity, empBase.companyId);
		}

		entity.resetChanged();
		// For new registration, generate Request No.
		if (entity.requestNo.equals(ExpRequestEntity.DEFAULT_REQUEST_NO)) {
			setRequestNo(entity, empBase);
		}

		// Create request entity
		ExpRequestApprovalEntity approval = createRequestApproval(entity, comment, empBase);
		approval.requestNo = entity.requestNo;

		// Save request
		ExpRequestApprovalRepository approvalRepo = new ExpRequestApprovalRepository();
		Repository.SaveResult result = approvalRepo.saveEntity(approval);
		approval.setId(result.details[0].id);

		// Attach to request and save request data
		ExpRequestService.ExpRequest attachBody = saveExpRequestAsAttachment(entity, approval);

		// For new registration, update Approval Id into ExpRequest record
		if (entity.expRequestApprovalId != approval.id) {
			entity.expRequestApprovalId = approval.id;
		}
		
		if (entity.isChanged(ExpRequestEntity.Field.REQUEST_NO) || entity.isChanged(ExpRequestEntity.Field.EXP_REQUEST_APPROVAL_ID)) {
			new ExpRequestRepository().saveEntity(entity);
		}

		// Create link between request and receipt/file
		createLinkToRequest(attachBody, approval.id);

		// Submit request
		submitProcess(approval.id, comment);
	}
	
	/**
	 * Check if target data exists
	 * @param requestId Target record ID
	 * @return Entity of request
	 */
	private static ExpRequestEntity checkDataExists(Id requestId) {
		ExpRequestEntity entity = ExpRequestService.getExpRequest(requestId);
		if (entity == null) {
			ExpRequestService.throwRequestNotFoundException();
		}
		return entity;
	}
	
	/**
	 * Validation before submitting request
	 * @param entity Entity of request
	 */
	private static void validateRequest(ExpRequestEntity entity, Id companyId) {
		ExpValidator.PreRequestSubmitValidator validator = new ExpValidator.PreRequestSubmitValidator(entity, companyId);
		Validator.Result result = validator.validate();
		if (!result.isSuccess()) {
			List<String> codes = new List<String>();
			List<String> messages = new List<String>();
			
			List<Validator.Error> errors = result.getErrors();
			for (Validator.Error error : errors) {
				codes.add(error.code);
				messages.add(error.message);
			}
			
			throw new App.IllegalStateException(String.join(codes, ','), String.join(messages, '\n'));
		}
	}
	
	/*
	 * Set request no to the entity
	 * @param entity Target entity to set the request no
	 * @param empBase Employee base entity of the owner of the request
	 */
	private static void setRequestNo(ExpRequestEntity entity, EmployeeBaseEntity empBase) {
		entity.prefix = REQUEST_NO_PREFIX;
		entity.sequenceNo = getNewSequenceNo();
		entity.requestNo = entity.prefix + String.valueOf(entity.sequenceNo).leftPad(8, '0');
		entity.name = ExpRequestEntity.createName(empBase.code, empBase.fullNameL.getFullName(), entity.requestNo);
	}
	
	/*
	 * Get latest sequence no of request no
	 */
	private static Integer getNewSequenceNo() {
		Integer currentMax = new ExpRequestRepository().getMaxSequence();
		return currentMax + 1;
	}
	
	/**
	 * Create request entity
	 * @param entity Target request entity
	 * @param comment Application comment
	 * @param employee Employee base entity
	 * @return Entity of request
	 */
	private static ExpRequestApprovalEntity createRequestApproval(ExpRequestEntity entity, String comment,
			EmployeeBaseEntity empBase) {

		// Get employee info
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity actor = empService.getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());

		// Get company language
		AppLanguage lang = ExpRequestService.getCompanyLanguage(empBase.companyId);

		// Create request name
		String requestName = new ExpService().createRequestName(entity.requestNo, entity.totalAmount, entity.employeeNameL, entity.subject, lang);

		// Create approval entity
		ExpRequestApprovalEntity approval = new ExpRequestApprovalEntity();
		if (entity.expRequestApprovalId != null) {
			// Set existing ID
			approval.setId(entity.expRequestApprovalId);
		}
		approval.name = requestName;
		approval.expRequestId = entity.id;
		approval.totalAmount = entity.totalAmount;
		approval.requestNo = entity.requestNo;
		approval.subject = entity.subject;
		approval.comment = comment;
		approval.status = AppRequestStatus.PENDING;
		approval.Claimed = false;
		approval.cancelType = null;
		approval.isConfirmationRequired = false;
		approval.requestTime = AppDateTime.now();
		approval.employeeHistoryId = entity.employeeHistoryId;
		approval.departmentHistoryId = entity.departmentHistoryId;
		approval.actorHistoryId = actor.getHistory(0).id;

		// Set approver ID
		approval.approver01Id =  GetApproverId(entity, empBase);

		return approval;
	}
	
	/**
	 * Get approver of the request
	 * @param entity Entity of request
	 * @param empBase Base Entity of Employee
	 * @return Id of approver
	 */
	// TODO: For now, approver is fixed as the manager of the employee
	private static Id getApproverId(ExpRequestEntity entity, EmployeeBaseEntity empBase) {
		EmployeeHistoryEntity empHistory = empBase.getHistory(0);
		if (empHistory.managerId == null) {
			throw new App.IllegalStateException(
				App.ERR_CODE_NOT_SET_EMP_MANAGER,
				ComMessage.msg().Exp_Err_FieldNotSet(new List<String>{
					ComMessage.msg().Com_Lbl_Employee,
					ComMessage.msg().Com_Lbl_Manager}));
		}
		EmployeeBaseEntity manager = new EmployeeService().getEmployee(empHistory.managerId, entity.recordingDate);
		if (manager == null) {
			throw new App.IllegalStateException(
				App.ERR_CODE_NOT_FOUND_MANAGER,
				ComMessage.msg().Exp_Err_NotFoundAofB(new List<String>{
					ComMessage.msg().Com_Lbl_Employee,
					ComMessage.msg().Com_Lbl_Manager}));
		}
		return manager.userId;
	}
	
	/**
	 * Save expense request data as attachment of its expense request approval
	 * @param entity Entity of request
	 * @param approval Entity of Expense Request Approval
	 * @return Attach body data
	 */
	private static ExpRequestService.ExpRequest saveExpRequestAsAttachment(ExpRequestEntity entity, ExpRequestApprovalEntity approval) {
		AppAttachmentRepository attachRepo = new AppAttachmentRepository();

		ExpRequestService.ExpRequest attachBody = new ExpRequestService.ExpRequest();
		attachBody.setData(entity);
		attachBody.setRecords(entity.recordList);
		List<AppAttachmentEntity> attaches = attachRepo.getEntityListByParentId(approval.id, approval.name);
		AppAttachmentEntity attach;
		if ((attaches == null) || (attaches.isEmpty())) {
			attach = new AppAttachmentEntity();
			attach.name = approval.name;
			attach.parentId = approval.id;
		} else {
			attach = attaches[0];

			// If there are duplicate file names, output it to debug log
			if (attaches.size() > 1) {
				System.debug(System.LoggingLevel.WARN,
						'経費申請レコードに同名の添付ファイルが複数存在します。(ParentId=' + approval.id + 'ファイル名=' + approval.name + ')');
			}
		}
		attach.bodyText = JSON.serialize(attachBody);
		attachRepo.saveEntity(attach);

		return attachBody;
	}

	/**
	 * Create link between request and file, record and receipt
	 * @param requestParam ExpRequest param that contains request and records info
	 * @param approvalId Approval ID
	 */
	private static void createLinkToRequest(ExpRequestService.ExpRequest requestParam, Id approvalId) {
		// Of receipts linked to records, retrieve receipts also linked to request
		List<Id> entityIdList = new List<Id>();
		for (ExpRequestService.ExpRequestRecord record : requestParam.records) {
			if (String.isNotBlank(record.receiptId)) {
				entityIdList.add(record.recordId);
			}
		}

		// File attachment to the Id list to find the relevant ContentDocument
		if (requestParam.attachedFileId != null) {
			entityIdList.add(requestParam.reportId);
		}

		Set<Id> linkedReceiptIdSet = new Set<Id>();
		if (!entityIdList.isEmpty()) {
			for (AppContentDocumentEntity content : new FileService().getContentDocumentEntityList(entityIdList)) {
				for (AppContentDocumentLinkEntity link : content.contentDocumentLinkList) {
					if (link.linkedEntityId == approvalId) {
						linkedReceiptIdSet.add(link.contentDocumentId);
					}
				}
			}
		}

		// If there is no link to request yet, create it
		List<AppContentDocumentLinkEntity> linkList = new List<AppContentDocumentLinkEntity>();
		for (ExpRequestService.ExpRequestRecord record : requestParam.records) {
			if (String.isNotBlank(record.receiptId) && !linkedReceiptIdSet.contains(record.receiptId)) {
				AppContentDocumentLinkEntity link = new AppContentDocumentLinkEntity();
				link.contentDocumentId = record.receiptId;
				link.linkedEntityId = approvalId;
				linkList.add(link);
			}
		}

		// Create File attachment link if haven't created
		if (requestParam.attachedFileId != null && !linkedReceiptIdSet.contains(requestParam.attachedFileId)) {
			AppContentDocumentLinkEntity link = new AppContentDocumentLinkEntity();
			link.contentDocumentId = requestParam.attachedFileId;
			link.linkedEntityId = approvalId;
			linkList.add(link);
		}

		if (!linkList.isEmpty()) {
			new AppContentDocumentRepository().saveLinkList(linkList);
		}
	}

	/**
	 * Submit request
	 * TODO In the future, will make logic of processing request common.
	 * @param requestId Target record ID
	 * @param comment Comment
	 */
	private static void submitProcess(Id requestId, String comment) {
		Approval.ProcessSubmitRequest processReq = new Approval.ProcessSubmitRequest();
		processReq.setObjectId(requestId);
		processReq.setComments(comment);

		Approval.process(processReq);
	}

	/*
	 * Cancel/Recall the Request Approval Submission
	 * @param requestId Id of ExpRequestApproval associated with the ExpRequest__c
	 * @param comment Comment to include in the Cancel
	 */
	public void cancelRequest(Id requestId, String comment) {
		ExpRequestApprovalRepository repo = new ExpRequestApprovalRepository();
		ExpRequestApprovalEntity request = repo.getEntity(requestId);
		if (request == null) {
			throw new App.IllegalStateException(App.ERR_CODE_NOT_FOUND_REQUEST,
				ComMessage.msg().Exp_Err_NotFoundRequest);
		}

		if (AppRequestStatus.PENDING.equals(request.status)) {
			cancelRequestApprovalProcess(request, comment);
		} else {
			throw new App.IllegalStateException(
				App.ERR_CODE_CANNOT_CANCEL_REQUEST, ComMessage.msg().Exp_Err_CannotCancelRequest);
		}
	}

	/*
	 * Process the Approval Cancellation Process
	 * @param request ExpRequestApprovalEntity which represents the current Pending Request
	 * @param comment Comment to include the the Cancel Process
	 */
	private void cancelRequestApprovalProcess(ExpRequestApprovalEntity request, String comment) {
		if (!test.isRunningTest()) {
			// 承認/却下対象の承認申請データを取得
			ApprovalProcessRepository appRepo = new ApprovalProcessRepository();
			List<Id> workItemIdList = appRepo.getWorkItemList(new List<Id>{request.id});
			if (workItemIdList == null || workItemIdList.isEmpty()) {
				throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_CANCEL_REQUEST,
					ComMessage.msg().Exp_Err_CannotCancelRequest);
			}

			Approval.ProcessWorkitemRequest processReq = new Approval.ProcessWorkitemRequest();
			processReq.setWorkItemId(workItemIdList[0]);
			processReq.setAction(ACTION_TYPE_REMOVED);
			processReq.setComments(comment);

			try {
				Approval.process(processReq);
			} catch(DmlException e) {
				if(e.getMessage().indexOf('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY') >= 0) {
					throw new App.IllegalStateException(
						App.ERR_CODE_NO_PERMISSIONS_CANCEL_REQUEST,
						ComMessage.msg().Exp_Err_NoPermissionsRemoveRequest);
				} else {
					throw e;
				}
			}
		}
	}
	
	/**
	 * @description Get a list of expense request
	 * @param requestIdList List of target request ID
	 * @return List of request approval entity
	 */
	public static List<ExpRequestApprovalEntity> getRequestApprovalList(final List<Id> approvalIdList) {
		ExpRequestApprovalRepository repo = new ExpRequestApprovalRepository();

		// Get a list of request approval
		List<ExpRequestApprovalEntity> approvalList = repo.getEntityList(approvalIdList);

		return approvalList;
	}
	
}