/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description RestBatchImportV1Resourceのテストクラス
 */
@isTest
private class RestBatchImportV1ResourceTest {

	/**
	 * マスタインポートバッチ処理実行APIのテスト
	 */
	@isTest
	static void doPostTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('日本', null);

		// 部署インポートデータを作成
		ComDepartmentImport__c deptImp = new ComDepartmentImport__c();
		deptImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
		deptImp.Code__c = 'batch1';
		deptImp.CompanyCode__c = testCompany.Code__c;
		deptImp.Name_L0__c = 'dept1';
		deptImp.HistoryComment__c = 'バッチ登録';
		deptImp.Status__c = ImportStatus.WAITING.value;
		insert deptImp;

		Test.startTest();

		RestRequest restReq = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": "EmployeeDept", "importBatchId": null}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		Test.stopTest();

		// 成功していることを確認する
		System.assertEquals(200, RestContext.response.statusCode);
	}

	/**
	 * マスタインポートバッチ処理実行APIのテスト（再実行）
	 */
	@isTest
	static void doPostTestAgainTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('日本', null);

		// 部署インポートデータを作成
		ComDepartmentImport__c deptImp = new ComDepartmentImport__c();
		deptImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
		deptImp.Code__c = 'batch1';
		deptImp.CompanyCode__c = testCompany.Code__c;
		deptImp.Name_L0__c = 'dept1';
		deptImp.HistoryComment__c = 'バッチ登録';
		deptImp.Status__c = ImportStatus.WAITING.value;
		insert deptImp;

		// インポートバッチデータを作成
		ImportBatchService service = new ImportBatchService(ImportBatchType.EMPLOYEE_DEPT, null);
		Id importBatchId = service.createImportBatchData();

		Test.startTest();

		RestRequest restReq = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": "EmployeeDept", "importBatchId": "' + importBatchId + '"}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		Test.stopTest();

		// 成功していることを確認する
		System.assertEquals(200, RestContext.response.statusCode);
	}

	/**
	 * マスタインポートバッチ処理実行APIのテスト（インポートデータなし）
	 */
	@isTest
	static void doPostNoDataTest() {
		RestRequest restReq  = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": "EmployeeDept", "importBatchId": null}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		// 失敗していることを確認する
		System.assertEquals(400, RestContext.response.statusCode);
		RemoteApi.ErrorResponse res =
			(RemoteApi.ErrorResponse)System.JSON.deserialize(RestContext.response.responseBody.toString(), RemoteApi.ErrorResponse.class);
		System.assertEquals(false, res.isSuccess);
		System.assertEquals(App.ERR_CODE_RECORD_NOT_FOUND, res.error.errorCode);
		System.assertEquals(ComMessage.msg().Batch_Err_RecordNotFound, res.error.message);
	}

	/**
	 * マスタインポートバッチ処理実行APIのテスト（再実行時にエラー）
	 */
	@isTest
	static void doPostTestAgainNoBatchTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('日本', null);

		// 部署インポートデータを作成
		ComDepartmentImport__c deptImp = new ComDepartmentImport__c();
		deptImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
		deptImp.Code__c = 'batch1';
		deptImp.CompanyCode__c = testCompany.Code__c;
		deptImp.Name_L0__c = 'dept1';
		deptImp.HistoryComment__c = 'バッチ登録';
		deptImp.Status__c = ImportStatus.WAITING.value;
		insert deptImp;

		// インポートバッチデータを作成
		ImportBatchService service = new ImportBatchService(ImportBatchType.EMPLOYEE_DEPT, null);
		Id importBatchId = service.createImportBatchData();

		// インポートバッチデータを削除
		delete [SELECT Id FROM ComImportBatch__c WHERE Id = :importBatchId];

		Test.startTest();

		RestRequest restReq  = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": "EmployeeDept", "importBatchId": "' + importBatchId + '"}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		Test.stopTest();

		// 失敗していることを確認する
		System.assertEquals(400, RestContext.response.statusCode);
		RemoteApi.ErrorResponse res =
			(RemoteApi.ErrorResponse)System.JSON.deserialize(RestContext.response.responseBody.toString(), RemoteApi.ErrorResponse.class);
		System.assertEquals(false, res.isSuccess);
		System.assertEquals(App.ERR_CODE_RECORD_NOT_FOUND, res.error.errorCode);
		System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_ImportBatchId}), res.error.message);
	}

	/**
	 * マスタインポートバッチ処理実行APIのテスト（種別が未指定）
	 */
	@isTest
	static void doPostTestNoTypeTest() {
		RestRequest restReq  = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": null, "importBatchId": null}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		// 失敗していることを確認する
		System.assertEquals(400, RestContext.response.statusCode);
		RemoteApi.ErrorResponse res =
			(RemoteApi.ErrorResponse)System.JSON.deserialize(RestContext.response.responseBody.toString(), RemoteApi.ErrorResponse.class);
		System.assertEquals(false, res.isSuccess);
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, res.error.errorCode);
	}

	/**
	 * マスタインポートバッチ処理実行APIのテスト（種別が不正）
	 */
	@isTest
	static void doPostInvalidTypeTest() {
		RestRequest restReq  = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": "test", "importBatchId": null}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		// 失敗していることを確認する
		System.assertEquals(400, RestContext.response.statusCode);
		RemoteApi.ErrorResponse res =
			(RemoteApi.ErrorResponse)System.JSON.deserialize(RestContext.response.responseBody.toString(), RemoteApi.ErrorResponse.class);
		System.assertEquals(false, res.isSuccess);
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, res.error.errorCode);
	}

	/**
	 * マスタインポートバッチ処理実行APIのテスト（インポートバッチIDが不正）
	 */
	@isTest
	static void doPostInvalidBatchIdTest() {
		RestRequest restReq  = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": "EmployeeDept", "importBatchId": "test"}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		// 失敗していることを確認する
		System.assertEquals(400, RestContext.response.statusCode);
		RemoteApi.ErrorResponse res =
			(RemoteApi.ErrorResponse)System.JSON.deserialize(RestContext.response.responseBody.toString(), RemoteApi.ErrorResponse.class);
		System.assertEquals(false, res.isSuccess);
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, res.error.errorCode);
	}

	/**
	 * マスタインポートバッチ処理実行APIのテスト（その他のエラー）
	 */
	@isTest
	static void doPostErrorTest() {
		RestRequest restReq  = new RestRequest();
		restReq.requestURI = '/services/apexrest/v1/batch-import/exec';
		restReq.httpMethod = 'POST';
		restReq.addHeader('Content-Type', 'application/json');
		restReq.requestBody = Blob.valueOf('{"type": "EmployeeDept", "importBatchId: null}');
		RestContext.request = restReq ;
		RestResponse restRes = new RestResponse();
		RestContext.response = restRes;

		// REST API実行
		RestBatchImportV1Resource.doPost();

		// 失敗していることを確認する
		System.assertEquals(500, RestContext.response.statusCode);
		RemoteApi.ErrorResponse res =
			(RemoteApi.ErrorResponse)System.JSON.deserialize(RestContext.response.responseBody.toString(), RemoteApi.ErrorResponse.class);
		System.assertEquals(false, res.isSuccess);
		System.assertEquals('UNEXPECTED_ERROR', res.error.errorCode);
	}
}