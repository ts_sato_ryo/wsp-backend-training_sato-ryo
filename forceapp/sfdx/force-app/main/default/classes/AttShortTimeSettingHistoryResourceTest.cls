/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttShortTimeSettingHistoryResourceのテストクラス
 */
@isTest
private class AttShortTimeSettingHistoryResourceTest {

	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

		/**
		 * 短時間勤務設定を作成する
		 */
		public List<AttShortTimeSettingBaseEntity> createShortSettingList(
				String name, Integer baseSize, Integer historySize) {
			List<AttShortTimeWorkSettingBase__c> settingObjList =
					ComTestDataUtility.createAttShortTimeSettingsWithHistories(
							name, this.company.id, this.tag.id, baseSize, historySize);

			List<Id> baseIds = new List<Id>();
			for (AttShortTimeWorkSettingBase__c settingObj : settingObjList) {
				baseIds.add(settingObj.Id);
			}

			return new AttShortTimeSettingRepository().getEntityList(baseIds, null);

		}
	}

	/**
	 * 短時間勤務設定履歴レコードを新規登録する(正常系)
	 * 履歴リストの末尾に新たな履歴が登録できることを確認する
	 */
	@isTest
	static void createTestTailHistory() {

		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 1;
		final Integer historySize = 1;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[0];
		AttShortTimeSettingHistoryEntity prevHistory = targetBase.getHistory(0); // 改定前履歴

		AttShortTimeSettingHistoryResource.SettingHistory param = new AttShortTimeSettingHistoryResource.SettingHistory();
		param.baseId = targetBase.id;
		param.validDateFrom = prevHistory.validFrom.getDate().addDays(10);
		param.validDateTo = param.validDateFrom.addDays(10);
		param.comment = '改定テスト';
		param.name_L0 = prevHistory.nameL0 + '_改定';
		param.name_L1 = prevHistory.nameL1;
		param.name_L2 = prevHistory.nameL2;
		param.allowableTimeOfShortWork = prevHistory.allowableTimeOfShortWork + 4;
		param.allowableTimeOfLateArrival = prevHistory.allowableTimeOfLateArrival + 1;
		param.allowableTimeOfEarlyLeave = prevHistory.allowableTimeOfEarlyLeave + 2;
		param.allowableTimeOfIrregularRest = prevHistory.allowableTimeOfIrregularRest + 3;

		Test.startTest();
		AttShortTimeSettingHistoryResource.CreateApi api = new AttShortTimeSettingHistoryResource.CreateApi();
		AttShortTimeSettingHistoryResource.CreateResponse res = (AttShortTimeSettingHistoryResource.CreateResponse)api.execute(param);
		Test.stopTest();

		System.assertNotEquals(null, res.id);

		// 登録後のレコードを取得
		AttShortTimeSettingHistoryEntity newHistory = new AttShortTimeSettingRepository().getHistoryEntity(res.id);
		System.assertNotEquals(null, newHistory);

		// 項目値の検証
		// 改定後履歴(新規に追加された履歴)
		System.assertEquals(param.baseId, newHistory.baseId);
		String uniqKey = targetBase.uniqKey + '-' +
							new AppDate(param.validDateFrom).formatYYYYMMDD() + '-' +
							new AppDate(param.validDateTo).formatYYYYMMDD();
		System.assertEquals(uniqKey, newHistory.uniqKey);
		System.assertEquals(param.name_L0, newHistory.nameL0);
		System.assertEquals(param.name_L1, newHistory.nameL1);
		System.assertEquals(param.name_L2, newHistory.nameL2);
		System.assertEquals(param.validDateFrom, newHistory.validFrom.getDate());
		System.assertEquals(param.validDateTo, newHistory.validTo.getDate());
		System.assertEquals(param.comment, newHistory.historyComment);
		System.assertEquals(param.allowableTimeOfLateArrival, newHistory.allowableTimeOfLateArrival);
		System.assertEquals(param.allowableTimeOfEarlyLeave, newHistory.allowableTimeOfEarlyLeave);
		System.assertEquals(param.allowableTimeOfIrregularRest, newHistory.allowableTimeOfIrregularRest);
	}

	/**
	 * 短時間勤務設定レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		final Integer baseSize = 1;
		final Integer historySize = 1;
		ResourceTestData testData = new ResourceTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingHistoryEntity prevHistory = testBases[0].getHistory(0); // 改定前履歴

		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttShortTimeSettingHistoryResource.SettingHistory param = new AttShortTimeSettingHistoryResource.SettingHistory();
		param.baseId = testBases[0].id;
		param.validDateFrom = prevHistory.validFrom.getDate().addDays(10);
		param.validDateTo = param.validDateFrom.addDays(10);
		param.name_L0 = prevHistory.nameL0 + '_改定';

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttShortTimeSettingHistoryResource.CreateApi api = new AttShortTimeSettingHistoryResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 短時間勤務設定履歴レコードを新規登録する(異常系)
	 * ベースIDが正しくない場合、アプリ例外が発生することを確認する
	 */
	@isTest
	static void createTestInvalidBaseId() {

		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 1;
		final Integer historySize = 1;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[0];
		AttShortTimeSettingHistoryEntity prevHistory = targetBase.getHistory(0); // 改定前履歴

		AttShortTimeSettingHistoryResource.SettingHistory param = new AttShortTimeSettingHistoryResource.SettingHistory();
		param.validDateFrom = prevHistory.validFrom.getDate().addDays(10);
		param.validDateTo = param.validDateFrom.addDays(10);
		param.name_L0 = prevHistory.nameL0 + '_改定';

		AttShortTimeSettingHistoryResource.CreateApi api = new AttShortTimeSettingHistoryResource.CreateApi();

		// Test1: ベースIDがnullの場合
		param.baseId = null;
		try {
			AttShortTimeSettingHistoryResource.CreateResponse res = (AttShortTimeSettingHistoryResource.CreateResponse)api.execute(param);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'baseId'}), e.getMessage());
		}

		// Test2: ベースIDがID以外の文字列の場合
		param.baseId = 'testtesttesttestesttest';
		try {
			AttShortTimeSettingHistoryResource.CreateResponse res = (AttShortTimeSettingHistoryResource.CreateResponse)api.execute(param);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'baseId'}), e.getMessage());
		}

		// Test3: 存在しないベースIDを指定した場合
		param.baseId = UserInfo.getUserId();
		try {
			AttShortTimeSettingHistoryResource.CreateResponse res = (AttShortTimeSettingHistoryResource.CreateResponse)api.execute(param);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(
					new List<String>{ComMessage.msg().Admin_Lbl_ShortTimeWorkSetting}),
					e.getMessage());
		}
	}

	/**
	 * 短時間勤務設定履歴レコードを新規登録する(異常系)
	 * 改定日が既存の最新履歴の有効開始日よりも前の場合、アプリ例外が発生することを確認する
	 */
	@isTest
	static void createTestInvalidRevisionDate() {

		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 1;
		final Integer historySize = 1;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[0];
		AttShortTimeSettingHistoryEntity prevHistory = targetBase.getHistory(0); // 改定前履歴

		AttShortTimeSettingHistoryResource.SettingHistory param = new AttShortTimeSettingHistoryResource.SettingHistory();
		param.baseId = targetBase.id;
		param.validDateFrom = prevHistory.validFrom.getDate().addDays(-1);
		param.validDateTo = param.validDateFrom.addDays(10);
		param.comment = '改定テスト';
		param.name_L0 = prevHistory.nameL0 + '_改定';
		param.name_L1 = prevHistory.nameL1;
		param.name_L2 = prevHistory.nameL2;
		param.allowableTimeOfShortWork = prevHistory.allowableTimeOfShortWork + 4;
		param.allowableTimeOfLateArrival = prevHistory.allowableTimeOfLateArrival + 1;
		param.allowableTimeOfEarlyLeave = prevHistory.allowableTimeOfEarlyLeave + 2;
		param.allowableTimeOfIrregularRest = prevHistory.allowableTimeOfIrregularRest + 3;

		AttShortTimeSettingHistoryResource.CreateApi api = new AttShortTimeSettingHistoryResource.CreateApi();

		try {
			AttShortTimeSettingHistoryResource.CreateResponse res = (AttShortTimeSettingHistoryResource.CreateResponse)api.execute(param);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Admin_Err_InvalidRevisionDate, e.getMessage());
		}
	}

	/**
	 * 短時間勤務設定レコードを検索する(正常系)
	 * 指定した履歴IDのレコードが取得できることを確認する
	 */
	@isTest
	static void searchTestById() {

		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 3;
		final Integer historySize = 2;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[0];
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);

		AttShortTimeSettingHistoryResource.SearchRequest param = new AttShortTimeSettingHistoryResource.SearchRequest();
		param.id = targetHistory.id;

		Test.startTest();
		AttShortTimeSettingHistoryResource.SearchApi api = new AttShortTimeSettingHistoryResource.SearchApi();
		AttShortTimeSettingHistoryResource.SearchResponse res = (AttShortTimeSettingHistoryResource.SearchResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(1, res.records.size());
		AttShortTimeSettingHistoryResource.SettingHistory record = res.records[0];

		// ベースレコード値の検証
		System.assertEquals(targetHistory.id, record.id);
		System.assertEquals(targetHistory.baseId, record.baseId);
		System.assertEquals(targetHistory.nameL.getValue(), record.name);
		System.assertEquals(targetHistory.nameL0, record.name_L0);
		System.assertEquals(targetHistory.nameL1, record.name_L1);
		System.assertEquals(targetHistory.nameL2, record.name_L2);
		System.assertEquals(targetHistory.validFrom.getDate(), record.validDateFrom);
		System.assertEquals(targetHistory.validTo.getDate(), record.validDateTo);
		System.assertEquals(targetHistory.historyComment, record.comment);
		System.assertEquals(targetHistory.allowableTimeOfShortWork, record.allowableTimeOfShortWork);
		System.assertEquals(targetHistory.allowableTimeOfLateArrival, record.allowableTimeOfLateArrival);
		System.assertEquals(targetHistory.allowableTimeOfEarlyLeave, record.allowableTimeOfEarlyLeave);
		System.assertEquals(targetHistory.allowableTimeOfIrregularRest, record.allowableTimeOfIrregularRest);
	}

	/**
	 * 短時間勤務設定レコードを検索する(正常系)
	 * 指定したIDの履歴レコードが論理削除されることを確認する
	 */
	@isTest
	static void deleteTestById() {

		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 2;
		final Integer historySize = 2;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[1];
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(1);

		AttShortTimeSettingHistoryResource.DeleteRequest param = new AttShortTimeSettingHistoryResource.DeleteRequest();
		param.id = targetHistory.id;

		Test.startTest();
		AttShortTimeSettingHistoryResource.DeleteApi api = new AttShortTimeSettingHistoryResource.DeleteApi();
		api.execute(param);
		Test.stopTest();

		// 論理削除されていることを確認する
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{param.id};
		filter.includeRemoved = true;
		List<AttShortTimeSettingHistoryEntity> resHistories = new AttShortTimeSettingRepository().searchHistoryEntity(filter);

		System.assertEquals(1, resHistories.size());
		System.assertEquals(true, resHistories[0].isRemoved);
	}

	/**
	 * 短時間勤務設定履歴レコードを1件削除する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		final Integer baseSize = 2;
		final Integer historySize = 2;
		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[1];

		// 短時間勤務設定管理権限を無効に設定する
		testData.permission.isManageAttShortTimeWorkSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttShortTimeSettingHistoryResource.DeleteRequest param = new AttShortTimeSettingHistoryResource.DeleteRequest();
		param.id = targetBase.getHistory(0).id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttShortTimeSettingHistoryResource.DeleteApi api = new AttShortTimeSettingHistoryResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 短時間勤務設定レコードを削除する(正常系)
	 * 削除済みの履歴IDを指定した場合、正常に終了することを確認する
	 */
	@isTest
	static void deleteTestByRemovedId() {

		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 2;
		final Integer historySize = 2;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[1];
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(1);

		AttShortTimeSettingHistoryResource.DeleteRequest param = new AttShortTimeSettingHistoryResource.DeleteRequest();
		param.id = targetHistory.id;

		// 削除対象の履歴を論理削除しておく
		targetHistory.isRemoved = true;
		new AttShortTimeSettingRepository().saveHistoryEntity(targetHistory);

		// 削除しても例外が発生しなければOK
		Test.startTest();
		AttShortTimeSettingHistoryResource.DeleteApi api = new AttShortTimeSettingHistoryResource.DeleteApi();
		api.execute(param);
		Test.stopTest();
	}

	/**
	 * 短時間勤務設定レコードを削除する(異常系)
	 * 履歴IDを指定しなかった場合、想定している例外が発生することを確認する
	 */
	@isTest
	static void deleteTestInvalidId() {
		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 2;
		final Integer historySize = 2;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[1];
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(1);

		AttShortTimeSettingHistoryResource.DeleteRequest param = new AttShortTimeSettingHistoryResource.DeleteRequest();
		AttShortTimeSettingHistoryResource.DeleteApi api = new AttShortTimeSettingHistoryResource.DeleteApi();

		// IDがnullの場合
		param.id = null;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), e.getMessage());
		}

		// ID以外の文字列が設定されている場合
		param.id = 'testtesttesttestesttest';
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), e.getMessage());
		}
	}

	/**
	 * 短時間勤務設定レコードを検索する(正常系)
	 * 指定したベースIDを持つ履歴レコードが取得できることを確認する
	 */
	@isTest
	static void searchTestByBaseId() {
		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();
		final Integer baseSize = 3;
		final Integer historySize = 2;
		List<AttShortTimeSettingBaseEntity> testBases =
				testData.createShortSettingList('Test',baseSize, historySize);
		AttShortTimeSettingBaseEntity targetBase = testBases[1];
		AttShortTimeSettingHistoryEntity targetHistory = targetBase.getHistory(0);

		AttShortTimeSettingHistoryResource.SearchRequest param = new AttShortTimeSettingHistoryResource.SearchRequest();
		param.baseId = targetBase.id;

		Test.startTest();
		AttShortTimeSettingHistoryResource.SearchApi api = new AttShortTimeSettingHistoryResource.SearchApi();
		AttShortTimeSettingHistoryResource.SearchResponse res = (AttShortTimeSettingHistoryResource.SearchResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(historySize, res.records.size());

		// リストが有効開始日の降順になっている
		System.assert(res.records[0].validDateFrom >= res.records[1].validDateFrom,
				'検索結果リストが有効開始日の降順ではありません');

		// ベースレコード値の検証
		AttShortTimeSettingHistoryResource.SettingHistory record = res.records[1];
		System.assertEquals(targetHistory.id, record.id);
		System.assertEquals(targetHistory.baseId, record.baseId);
		System.assertEquals(targetHistory.nameL.getValue(), record.name);
		System.assertEquals(targetHistory.nameL0, record.name_L0);
		System.assertEquals(targetHistory.nameL1, record.name_L1);
		System.assertEquals(targetHistory.nameL2, record.name_L2);
		System.assertEquals(targetHistory.validFrom.getDate(), record.validDateFrom);
		System.assertEquals(targetHistory.validTo.getDate(), record.validDateTo);
		System.assertEquals(targetHistory.historyComment, record.comment);
		System.assertEquals(targetHistory.allowableTimeOfShortWork, record.allowableTimeOfShortWork);
		System.assertEquals(targetHistory.allowableTimeOfLateArrival, record.allowableTimeOfLateArrival);
		System.assertEquals(targetHistory.allowableTimeOfEarlyLeave, record.allowableTimeOfEarlyLeave);
		System.assertEquals(targetHistory.allowableTimeOfIrregularRest, record.allowableTimeOfIrregularRest);
	}

	/**
	 * 短時間勤務設定履歴レコードを検索する(異常系)
	 * 検索対象のidが正しくない場合、想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void searchNegativeTestInvalidId() {
		AttShortTimeSettingHistoryResourceTest.ResourceTestData testData = new ResourceTestData();

		// Test1-1: idがnullの場合
		try {
			AttShortTimeSettingHistoryResource.SearchRequest param = new AttShortTimeSettingHistoryResource.SearchRequest();
			param.id = null;

			AttShortTimeSettingHistoryResource.SearchApi api = new AttShortTimeSettingHistoryResource.SearchApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}

		// Test1-2: idがIDではない文字列の場合
		try {
			AttShortTimeSettingHistoryResource.SearchRequest param = new AttShortTimeSettingHistoryResource.SearchRequest();
			param.id = 'aaa';

			AttShortTimeSettingHistoryResource.SearchApi api = new AttShortTimeSettingHistoryResource.SearchApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}

		// Test2-1: baseIdがnullの場合
		try {
			AttShortTimeSettingHistoryResource.SearchRequest param = new AttShortTimeSettingHistoryResource.SearchRequest();
			param.baseId = null;

			AttShortTimeSettingHistoryResource.SearchApi api = new AttShortTimeSettingHistoryResource.SearchApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}

		// Test2-2: baseIdがIDではない文字列の場合
		try {
			AttShortTimeSettingHistoryResource.SearchRequest param = new AttShortTimeSettingHistoryResource.SearchRequest();
			param.baseId = 'aaa';

			AttShortTimeSettingHistoryResource.SearchApi api = new AttShortTimeSettingHistoryResource.SearchApi();
			api.execute(param);
		} catch (App.ParameterException e) {
			// OK
		}
	}
}
