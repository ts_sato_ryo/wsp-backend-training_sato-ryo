/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 社員の権限に関するサービスを提供するクラスのテスト
 */
@isTest
private class EmployeePermissionServiceTest {

	/** 権限リポジトリ */
	private static PermissionRepository permissionRepo = new PermissionRepository();

	/** テストデータ */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * 標準ユーザの社員を作成する
		 * @param 社員名
		 * @return 標準ユーザの社員
		 */
		public EmployeeBaseEntity createEmployeeOfStandarUser(String name) {
			User standardUser = ComTestDataUtility.createStandardUser('standard-' + name);
			return createEmployee(name, null, standardUser.Id);
		}

	}

	/**
	 * システム管理者のテスト
	 * システム管理者の場合、hasModifyAllData=trueとなることを確認する
	 * システム管理者の場合、権限項目の値にかかわらず全ての権限が付与されていることを確認する
	 */
	@isTest static void adminTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		User adminUser = ComTestDataUtility.createUser('Admin', 'ja', 'ja_JP');
		User standardUser = ComTestDataUtility.createStandardUser('Standard');
		EmployeeBaseEntity employee = testData.createEmployee('Test', null, adminUser.Id);
		// 権限をfalseにしておく
		testData.permission.isViewAttTimeSheetByDelegate = false;
		new PermissionRepository().saveEntity(testData.permission);

		// Test1: システム管理者である場合 → true
		System.assertEquals(true, new EmployeePermissionService(employee).hasModifyAllData);

		// Test2: システム管理者である場合は全ての権限を持つ
		System.assertEquals(true, new EmployeePermissionService(employee).hasViewAttTimeSheetByDelegate);

		// Test3: システム管理者はない場合 → false
		System.runAs(standardUser) {
			employee.userId = standardUser.Id;
			new EmployeeRepository().saveBaseEntity(employee);
		}
		System.assertEquals(false, new EmployeePermissionService(employee).hasModifyAllData);

		// Test4: システム管理者でない場合は権限で付与されたアクセス権限のみ持つ
		System.assertEquals(false, new EmployeePermissionService(employee).hasViewAttTimeSheetByDelegate);
	}

	/**
	 * コンストラクタのテスト
	 * 社員未割り当てのシステム管理者ユーザの場合、hasModifyAllData=trueとなることを確認する
	 */
	@isTest static void constructorTestAdmin() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		User adminUser = ComTestDataUtility.createUser('Admin', 'ja', 'ja_JP');

		// 権限をfalseにしておく
		testData.permission.isViewAttTimeSheetByDelegate = false;
		new PermissionRepository().saveEntity(testData.permission);

		System.runAs(adminUser) {
			System.assertEquals(true, new EmployeePermissionService().hasModifyAllData);
		}
	}

	/**
	 * コンストラクタのテスト
	 */
	@isTest static void constructorTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		User adminUser = ComTestDataUtility.createUser('Admin', 'ja', 'ja_JP');
		EmployeeBaseEntity employee = testData.createEmployee('Test', null, adminUser.Id);

		// 権限をtrueにしておく
		testData.permission.isViewAttTimeSheetByDelegate = true;
		new PermissionRepository().saveEntity(testData.permission);

		// Test1: 社員IDを指定
		System.assertEquals(true, new EmployeePermissionService(employee.Id).hasViewAttTimeSheetByDelegate);

		// Test2: 社員を指定
		System.assertEquals(true, new EmployeePermissionService(employee).hasViewAttTimeSheetByDelegate);

		// Test3: 社員と対象日を指定
		AppDate targetDate = employee.getHistory(0).validFrom;
		System.assertEquals(true, new EmployeePermissionService(employee, targetDate).hasViewAttTimeSheetByDelegate);
	}

	/**
	 * コンストラクタのテスト
	 * 社員が見つからない場合に想定されるアプリ例外が発生することを確認する
	 */
	@isTest static void constructorTestEmployeeNotFound() {
		TestData testData = new EmployeePermissionServiceTest.TestData();

		String expMessage;

		// Test1: 社員を設定しない場合
		App.RecordNotFoundException actRecordNotFoundEx;
		try {
			expMessage = ComMessage.msg().Com_Err_NotFound(new List<String>{ComMessage.msg().Com_Lbl_Employee});
			EmployeeBaseEntity employee = null;
			new EmployeePermissionService(employee, AppDate.today());
		} catch (App.RecordNotFoundException e) {
			actRecordNotFoundEx = e;
		}
		System.assertNotEquals(null, actRecordNotFoundEx);
		System.assertEquals(expMessage, actRecordNotFoundEx.getMessage());

		// Test2: チェック対象日が社員の有効期間外の場合
		App.IllegalStateException actIllegalStateEx;
		try {
			expMessage = ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Com_Lbl_Employee});
			EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');
			AppDate targetDate = employee.getHistory(0).validTo;
			new EmployeePermissionService(employee, targetDate);
		} catch (App.IllegalStateException e) {
			actIllegalStateEx = e;
		}
		System.assertNotEquals(null, actIllegalStateEx);
		System.assertEquals(expMessage, actIllegalStateEx.getMessage());
	}

	/**
	 * 権限が論理削除されている場合のテスト
	 * 社員に割り当てられている権限が論理削除されている場合は権限が付与されないことを確認する
	 */
	@isTest static void logicalDeletedPermissionTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// 権限を論理削除しておく
		testData.permission.isRemoved = true;
		testData.permission.isViewAttTimeSheetByDelegate = true;
		new PermissionRepository().saveEntity(testData.permission);

		// 権限はtrueだが論理削除されているため、falseが返却される
		System.assertEquals(false, new EmployeePermissionService(employee).hasViewAttTimeSheetByDelegate);
	}

	/**
	 * 勤務表表示(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasViewAttTimeSheetByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 勤務表の表示(代理)権限が付与されている場合
		testData.permission.isViewAttTimeSheetByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasViewAttTimeSheetByDelegate);

		// Test2: 勤務表の表示(代理)権限が付与されていない場合
		testData.permission.isViewAttTimeSheetByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasViewAttTimeSheetByDelegate);
	}

	/**
	 * 勤務表編集(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasEditAttTimeSheetByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 勤務表の表示(代理)権限が付与されている場合
		testData.permission.isEditAttTimeSheetByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasEditAttTimeSheetByDelegate);

		// Test2: 勤務表の表示(代理)権限が付与されていない場合
		testData.permission.isEditAttTimeSheetByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasEditAttTimeSheetByDelegate);
	}

	/**
	 * 各種勤怠申請申請・申請削除(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasSubmitAttDailyRequestByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isSubmitAttDailyRequestByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasSubmitAttDailyRequestByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isSubmitAttDailyRequestByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasSubmitAttDailyRequestByDelegate);
	}

	/**
	 * 各種勤怠申請承認却下(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasApproveAttDailyRequestByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isApproveAttDailyRequestByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasApproveAttDailyRequestByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isApproveAttDailyRequestByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasApproveAttDailyRequestByDelegate);
	}

	/**
	 * 各種勤怠申請自己承認(本人)権限が正しく取得できることを確認する
	 */
	@isTest static void hasApproveSelfAttDailyRequestByEmployeeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isApproveSelfAttDailyRequestByEmployee = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasApproveSelfAttDailyRequestByEmployee);

		// Test2: 権限が付与されていない場合
		testData.permission.isApproveSelfAttDailyRequestByEmployee = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasApproveSelfAttDailyRequestByEmployee);
	}

	/**
	 * 各種勤怠申請申請取消(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasCancelAttDailyRequestByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isCancelAttDailyRequestByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasCancelAttDailyRequestByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isCancelAttDailyRequestByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasCancelAttDailyRequestByDelegate);
	}

	/**
	 * 各種勤怠申請承認取消(本人)権限が正しく取得できることを確認する
	 */
	@isTest static void hasCancelAttDailyApprovalByEmployeeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isCancelAttDailyApprovalByEmployee = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasCancelAttDailyApprovalByEmployee);

		// Test2: 権限が付与されていない場合
		testData.permission.isCancelAttDailyApprovalByEmployee = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasCancelAttDailyApprovalByEmployee);
	}

	/**
	 * 各種勤怠申請承認取消(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasCancelAttDailyApprovalByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isCancelAttDailyApprovalByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasCancelAttDailyApprovalByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isCancelAttDailyApprovalByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasCancelAttDailyApprovalByDelegate);
	}

	/**
	 * 勤務確定申請申請(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasSubmitAttRequestByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isSubmitAttRequestByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasSubmitAttRequestByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isSubmitAttRequestByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasSubmitAttRequestByDelegate);
	}

	/**
	 * 勤務確定申請申請(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasApproveAttRequestByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isApproveAttRequestByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasApproveAttRequestByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isApproveAttRequestByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasApproveAttRequestByDelegate);
	}

	/**
	 * 勤務確定申請自己承認(本人)権限が正しく取得できることを確認する
	 */
	@isTest static void hasApproveSelfAttRequestByEmployeeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isApproveSelfAttRequestByEmployee = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasApproveSelfAttRequestByEmployee);

		// Test2: 権限が付与されていない場合
		testData.permission.isApproveSelfAttRequestByEmployee = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasApproveSelfAttRequestByEmployee);
	}

	/**
	 * 勤務確定申請申請取消(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasCancelAttRequestByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isCancelAttRequestByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasCancelAttRequestByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isCancelAttRequestByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasCancelAttRequestByDelegate);
	}

	/**
	 * 勤務確定申請承認取消(本人)権限が正しく取得できることを確認する
	 */
	@isTest static void hasCancelAttApprovalByEmployeeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isCancelAttApprovalByEmployee = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasCancelAttApprovalByEmployee);

		// Test2: 権限が付与されていない場合
		testData.permission.isCancelAttApprovalByEmployee = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasCancelAttApprovalByEmployee);
	}

	/**
	 * 勤務確定申請承認取消(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasCancelAttApprovalByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isCancelAttApprovalByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasCancelAttApprovalByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isCancelAttApprovalByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasCancelAttApprovalByDelegate);
	}

	/**
	 * 工数実績表示(代理)権限が正しく取得できることを確認する
	 */
	@isTest static void hasViewTimeTrackByDelegateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isViewTimeTrackByDelegate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasViewTimeTrackByDelegate);

		// Test2: 権限が付与されていない場合
		testData.permission.isViewTimeTrackByDelegate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasViewTimeTrackByDelegate);
	}


	/**
	 * 全体設定の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageOverallSettingTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageOverallSetting = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageOverallSetting);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageOverallSetting = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageOverallSetting);
	}

	/**
	 * 会社の切り替えの権限が正しく取得できることを確認する
	 */
	@isTest static void hasSwitchCompanyTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isSwitchCompany = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasSwitchCompany);

		// Test2: 権限が付与されていない場合
		testData.permission.isSwitchCompany = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasSwitchCompany);
	}

	/**
	 * 部署の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageDepartmentTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageDepartment = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageDepartment);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageDepartment = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageDepartment);
	}

	/**
	 * 社員の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageEmployeeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageEmployee = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageEmployee);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageEmployee = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageEmployee);
	}

	/**
	 * カレンダーの管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageCalendarTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageCalendar = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageCalendar);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageCalendar = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageCalendar);
	}

	/**
	 * ジョブタイプの管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageJobTypeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageJobType = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageJobType);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageJobType = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageJobType);
	}

	/**
	 * ジョブの管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageJobTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageJob = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageJob);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageJob = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageJob);
	}

	/**
	 * モバイル機能設定の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageMobileSettingTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageMobileSetting = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageMobileSetting);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageMobileSetting = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageMobileSetting);
	}

	/**
	 * プランナー機能設定の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManagePlannerSettingTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManagePlannerSetting = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManagePlannerSetting);

		// Test2: 権限が付与されていない場合
		testData.permission.isManagePlannerSetting = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManagePlannerSetting);
	}

	/**
	 * アクセス権限設定管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManagePermissionTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManagePermission = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManagePermission);

		// Test2: 権限が付与されていない場合
		testData.permission.isManagePermission = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManagePermission);
	}

	/**
	 * 休暇の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttLeaveTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttLeave = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttLeave);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttLeave = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttLeave);
	}

	/**
	 * 短時間勤務設定の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttShortTimeWorkSettingTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttShortTimeWorkSetting = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttShortTimeWorkSetting);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttShortTimeWorkSetting = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttShortTimeWorkSetting);
	}

	/**
	 * 休職・休業の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttLeaveOfAbsenceTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttLeaveOfAbsence = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttLeaveOfAbsence);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttLeaveOfAbsence = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttLeaveOfAbsence);
	}

	/**
	 * 勤務体系の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttWorkingTypeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttWorkingType = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttWorkingType);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttWorkingType = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttWorkingType);
	}

	/**
	 * 勤務パターンの管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttPatternTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttPattern = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttPattern);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttPattern = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttPattern);
	}

	/**
	 * 残業警告設定の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttAgreementAlertSettingTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttAgreementAlertSetting = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttAgreementAlertSetting);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttAgreementAlertSetting = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttAgreementAlertSetting);
	}

	/**
	 * 休暇管理の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttLeaveGrantTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttLeaveGrant = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttLeaveGrant);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttLeaveGrant = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttLeaveGrant);
	}

	/**
	 * 短時間勤務設定適用の権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttShortTimeWorkSettingApplyTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttShortTimeWorkSettingApply = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttShortTimeWorkSettingApply);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttShortTimeWorkSettingApply = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttShortTimeWorkSettingApply);
	}

	/**
	 * 休職・休業適用の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttLeaveOfAbsenceApplyTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttLeaveOfAbsenceApply = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttLeaveOfAbsenceApply);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttLeaveOfAbsenceApply = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttLeaveOfAbsenceApply);
	}

	/**
	 * 勤務パターン適用の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageAttPatternApplyTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageAttPatternApply = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAttPatternApply);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageAttPatternApply = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAttPatternApply);
	}

	/**
	 * 工数設定の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageTimeSettingTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageTimeSetting = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageTimeSetting);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageTimeSetting = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageTimeSetting);
	}

	/**
	 * 作業分類の管理権限が正しく取得できることを確認する
	 */
	@isTest static void hasManageTimeWorkCategoryTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合
		testData.permission.isManageTimeWorkCategory = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageTimeWorkCategory);

		// Test2: 権限が付与されていない場合
		testData.permission.isManageTimeWorkCategory = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageTimeWorkCategory);
	}

	/**
	 * 費目グループの管理権限が正しく取得できることを確認する / check if can get value of expense type group
	 */
	@isTest static void hasManageExpTypeGroupTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpTypeGroup = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageExpTypeGroup);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpTypeGroup = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageExpTypeGroup);
	}

	/**
	 * 費目の管理権限が正しく取得できることを確認する / check if can get value of expense type
	 */
	@isTest static void hasManageExpTypeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpType = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageExpenseType);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpType = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageExpenseType);
	}

	/**
	 * 税区分の管理権限が正しく取得できることを確認する / check if can get value of tax type
	 */
	@isTest static void hasManageTaxTypeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpTaxType = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageTaxType);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpTaxType = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageTaxType);
	}

	/**
	 * 経費設定の管理権限が正しく取得できることを確認する / check if can get value of expense setting
	 */
	@isTest static void hasManageExpSettingTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpSetting = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageExpSetting);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpSetting = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageExpSetting);
	}

	/**
	 * 為替レートの管理権限が正しく取得できることを確認する / check if can get value of exchange rate
	 */
	@isTest static void hasManageExchangeRateTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpExchangeRate = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageExchangeRate);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpExchangeRate = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageExchangeRate);
	}

	/**
	 * 計上期間の管理権限が正しく取得できることを確認する / check if can get value of accounting period
	 */
	@isTest static void hasManageAccountingPeriodTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpAccountingPeriod = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageAccountingPeriod);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpAccountingPeriod = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageAccountingPeriod);
	}

	/**
	 * 申請種別の管理権限が正しく取得できることを確認する / check if can get value of report type
	 */
	@isTest static void hasManageReportTypeTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpReportType = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageReportType);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpReportType = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageReportType);
	}

	/**
	 * コストセンターの管理権限が正しく取得できることを確認する / check if can get value of cost center
	 */
	@isTest static void hasManageCostCenterTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpCostCenter = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageCostCenter);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpCostCenter = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageCostCenter);
	}

	/**
	 * 支払先の管理権限が正しく取得できることを確認する / check if can get value of vendor
	 */
	@isTest static void hasManageVendorTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpVendor = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageVendor);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpVendor = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageVendor);
	}

	/**
	 * 拡張項目の管理権限が正しく取得できることを確認する / check if can get value of extended item
	 */
	@isTest static void hasManageExtendedItemTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpExtendedItem = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageExtendedItem);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpExtendedItem = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageExtendedItem);
	}

	/**
	 * 従業員スループの管理権限が正しく取得できることを確認する / check if can get value of employee group
	 */
	@isTest static void hasManageEmployeeGroupTest() {
		TestData testData = new EmployeePermissionServiceTest.TestData();
		EmployeeBaseEntity employee = testData.createEmployeeOfStandarUser('Test');

		// Test1: 権限が付与されている場合 / case permission is given
		testData.permission.isManageExpEmployeeGroup = true;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(true, createServiceInstance(employee).hasManageEmployeeGroup);

		// Test2: 権限が付与されていない場合 / case permission is NOT given
		testData.permission.isManageExpEmployeeGroup = false;
		permissionRepo.saveEntity(testData.permission);
		System.assertEquals(false, createServiceInstance(employee).hasManageEmployeeGroup);
	}

	/**
	 * 権限キャッシュをクリアしてEmployeePermissionServiceインスタンスを生成する
	 * @param employee 対象の社員
	 * @return EmployeePermissionServiceのインスタンス
	 */
	private static EmployeePermissionService createServiceInstance(EmployeeBaseEntity employee) {
		EmployeePermissionService.clearPermissionMap();
		return new EmployeePermissionService(employee);
	}
}