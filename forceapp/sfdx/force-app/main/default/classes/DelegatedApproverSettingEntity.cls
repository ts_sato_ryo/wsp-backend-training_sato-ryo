/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通
 *
 * 代理承認者設定のエンティティ
 */
public with sharing class DelegatedApproverSettingEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		EMPLOYEE_BASE_ID,
		DELEGATED_APPROVER_BASE_ID,
		CAN_APPROVE_EXPENSE_REQUEST_BY_DELEGATE,
		CAN_APPROVE_EXPENSE_REPORT_BY_DELEGATE
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ
	 */
	public DelegatedApproverSettingEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/** 代理承認者設定名 */
	public String name {
		get;
		set {
			if (String.isBlank(value)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('name');
				throw ex;
			}
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** 社員ベースID */
	public Id employeeBaseId {
		get;
		set {
			employeeBaseId = value;
			setChanged(Field.EMPLOYEE_BASE_ID);
		}
	}

	/** 社員の情報(参照専用) */
	public AppLookup.Employee employee {get; set;}

	/** 社員の部署情報(参照専用) */
	public AppLookup.Department department {get; set;}

	/** 代理承認者ベースID */
	public Id delegatedApproverBaseId {
		get;
		set {
			delegatedApproverBaseId = value;
			setChanged(Field.DELEGATED_APPROVER_BASE_ID);
		}
	}

	/** 代理承認者の情報(参照専用) */
	public AppLookup.Employee delegatedApprover {get; set;}

	/** 代理承認者の部署情報(参照専用) */
	public AppLookup.Department delegatedApproverDepartment {get; set;}

	/** 事前申請承認却下(代理) */
	public Boolean canApproveExpenseRequestByDelegate {
		get;
		set {
			canApproveExpenseRequestByDelegate = value;
			setChanged(Field.CAN_APPROVE_EXPENSE_REQUEST_BY_DELEGATE);
		}
	}

	/** 経費精算申請承認却下(代理) */
	public Boolean canApproveExpenseReportByDelegate {
		get;
		set {
			canApproveExpenseReportByDelegate = value;
			setChanged(Field.CAN_APPROVE_EXPENSE_REPORT_BY_DELEGATE);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}
}