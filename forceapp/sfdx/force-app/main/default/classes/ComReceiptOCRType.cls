/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Define Receipt OCR Type
 * add suffix otherwise get `identifier name is reserved` error
 * https://salesforce.stackexchange.com/questions/2276/how-do-you-deserialize-json-properties-that-are-reserved-words-in-apex
 *
 * @group 経費 Expense
 */
public class ComReceiptOCRType {
	public class OCRDate {
		public String normalizedValue_x;
	}

	public class Total {
		public String normalizedValue_x;
	}

	public class Receipt {
		public OCRDate date_x;
		public Total total_x;
		//		public String recognizedText_x; // TODO: fix this later
	}

	public class Receipts {
		public Receipt receipt_x;
		public String count_x;
	}

	public class OcrResult {
		public Receipts receipts_x;
	}
}