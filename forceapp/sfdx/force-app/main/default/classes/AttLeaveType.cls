public with sharing class AttLeaveType extends ValueObjectType {
	/** 年次有休 */
	public static final AttLeaveType ANNUAL = new AttLeaveType('Annual');
	/** 有休 */
	public static final AttLeaveType PAID = new AttLeaveType('Paid');
	/** 無休 */
	public static final AttLeaveType UNPAID = new AttLeaveType('Unpaid');
	/** 振替 */
	public static final AttLeaveType SUBSTITUTE = new AttLeaveType('Substitute');
	/** 有休 */
	public static final AttLeaveType COMPENSATORY = new AttLeaveType('Compensatory');
	
	/** 休暇範囲のリスト（範囲が追加されら本リストにも追加してください) */
	public static final List<AttLeaveType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttLeaveType> {
			ANNUAL,
			PAID,
			UNPAID,
			SUBSTITUTE,
			COMPENSATORY
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttLeaveType(String value) {
		super(value);
	}

	/**
	 * AttLeaveType
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttLeaveType
	 */
	public static AttLeaveType valueOf(String value) {
		AttLeaveType retType = null;
		if (String.isNotBlank(value)) {
			for (AttLeaveType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {
		
		// nullの場合はfalse
		if (compare == null) {
			return false;
		}
		
		// AttDayType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttLeaveType) {
			// 値が同じであればtrue
			if (this.value == ((AttLeaveType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}

	/**
	 * 有休とするかどうかをチェックする
	 * @param leaveType チェック対象の休暇種別
	 * @retun true：年次有休、有休；false：それ以外
	 */
	public static Boolean isAsPaidLeave(AttLeaveType leaveType) {
		return ANNUAL.equals(leaveType) || PAID.equals(leaveType);
	}
}