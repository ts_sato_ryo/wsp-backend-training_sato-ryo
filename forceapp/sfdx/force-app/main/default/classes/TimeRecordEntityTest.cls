/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * @description TimeRecordEntityのテストクラス
 */
@isTest
private class TimeRecordEntityTest {

	/**
	 * createName のテスト
	 * 適当な値を設定した場合にemployeeCode+employeeName+targetDateとなっていることを確認する
	 */
	@isTest static void createNameTest() {
		String actName = null;
		try {
			actName = TimeRecordEntity.createName('E001', '京橋太郎', AppDate.newInstance(2019, 5, 1));
		} catch(Exception e) {
			TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
		}
		System.assertEquals('E001京橋太郎20190501', actName);
	}

	/**
	 * createName のテスト
	 * 不正な値を設定した場合に例外が発生することを確認する
	 */
	@isTest static void createNameErrorTest() {
		{
			App.ParameterException actEx;
			try {
				TimeRecordEntity.createName(null, '京橋太郎', AppDate.newInstance(2019, 5, 1));
			} catch(App.ParameterException e) {
				actEx = e;
			} catch(Exception e) {
				TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals('employeeCode is blank', actEx.getMessage());
		}
		{
			App.ParameterException actEx;
			try {
				TimeRecordEntity.createName('E001', null, AppDate.newInstance(2019, 5, 1));
			} catch(App.ParameterException e) {
				actEx = e;
			} catch(Exception e) {
				TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals('employeeName is blank', actEx.getMessage());
		}
		{
			App.ParameterException actEx;
			try {
				TimeRecordEntity.createName('E001', '京橋太郎', null);
			} catch(App.ParameterException e) {
				actEx = e;
			} catch(Exception e) {
				TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals('targetDate is null', actEx.getMessage());
		}
	}

	/**
	 * 工数内訳取得のテスト
	 * 値の有無に関わらず、nullが返らないことを検証する
	 */
	@isTest static void recordItemListTest() {

		// インスタンス生成
		TimeRecordEntity record = new TimeRecordEntity();
		System.assertEquals(true, record.recordItemList.isEmpty());

		// nullのリストを設定
		record.replaceRecordItemList(null);
		System.assertEquals(true, record.recordItemList.isEmpty());

		// 工数内訳を設定
		record.replaceRecordItemList(new List<TimeRecordItemEntity> {new TimeRecordItemEntity()});
		System.assertEquals(false, record.recordItemList.isEmpty());
		System.assertEquals(1, record.recordItemList.size());
	}

	/**
	 * 工数サマリーロックフラグのテスト
	 * 申請の有無、申請のステータスによって正しい値を返すことを検証する
	 */
	 @isTest static void isSummaryLockedTest() {
		 TimeRecord__c recordObj = new TimeRecord__c();
		{
			// 初期値
			TimeRecordEntity record = new TimeRecordEntity();
			System.assertEquals(false, record.isSummaryLocked);
		}
		{
			// オブジェクトから生成(サマリーなし)
			TimeRecordEntity record = new TimeRecordEntity(recordObj);
			System.assertEquals(false, record.isSummaryLocked);
		}
		recordObj.TimeSummaryId__r = new TimeSummary__c();
		{
			// オブジェクトから生成(サマリーあり)
			TimeRecordEntity record = new TimeRecordEntity(recordObj);
			System.assertEquals(false, record.isSummaryLocked);
		}
		{
			// オブジェクトから指定あり
			recordObj.TimeSummaryId__r.Locked__c = true;
			TimeRecordEntity record = new TimeRecordEntity(recordObj);
			System.assertEquals(true, record.isSummaryLocked);
		}
	 }
}