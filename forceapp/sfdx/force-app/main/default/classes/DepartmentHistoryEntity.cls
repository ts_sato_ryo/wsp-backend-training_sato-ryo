/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署履歴エンティティを表すクラス
 */
public with sharing class DepartmentHistoryEntity extends DepartmentHistoryGeneratedEntity {

	/** 部署名Lの最大文字数 */
	public final static Integer NAME_MAX_LENGTH = 80;
	/** 備考の最大文字数 */
	public final static Integer REMARKS_MAX_LENGTH = 255;

	/**
	 * @desctiprion 社員のルックアップ項目を表すクラス
	 */
	public class Employee {
		/** レコードのName項目値 */
		public AppPersonName fullName;
		/** コンストラクタ */
		public Employee(AppPersonName fullName) {
			this.fullName = fullName;
		}
	}

	/**
	 * デフォルトコンストラクタ
	 */
	public DepartmentHistoryEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public DepartmentHistoryEntity(ComDeptHistory__c sobj) {
		super(sobj);
	}

	/** 部署名(多言語) 参照のみ */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
		private set;
	}

	/** 部署管理者 */
	public Employee manager {
		get {
			if (this.managerId == null) {
				return null;
			}
			if (manager == null) {
				manager = new DepartmentHistoryEntity.Employee(new AppPersonName(
						sobj.ManagerBaseId__r.FirstName_L0__c, sobj.ManagerBaseId__r.LastName_L0__c,
						sobj.ManagerBaseId__r.FirstName_L1__c, sobj.ManagerBaseId__r.LastName_L1__c,
						sobj.ManagerBaseId__r.FirstName_L2__c, sobj.ManagerBaseId__r.LastName_L2__c));
			}
			return manager;
		}
		private set;
	}

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public DepartmentHistoryEntity copy() {
		DepartmentHistoryEntity copyEntity = new DepartmentHistoryEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildHistoryEntity copySuperEntity() {
		return copy();
	}
}