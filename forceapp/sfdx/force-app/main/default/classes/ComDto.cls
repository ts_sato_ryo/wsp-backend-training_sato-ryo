/**
 * 共通処理ロジック用DTOコンテナ
 */
public with sharing class ComDto {

	/**
	 * 一覧情報取得用Dto
	 */
	public class ListViewDto {
		public Integer pageNo; // 表示しているページ番号
		public Integer pageSize; // １ページに表示するレコード数
		public Integer totalSize; // 取得された全レコード数（上限10000）
		public Integer maxPageNo; // 最大ページ番号
		public Boolean exceedsLimit; // 10000レコードをオーバーしているか
		public List<Object> resultList; // 表示しているページのレコード一覧

		public ListViewDto(List<Object> resultList) {
			this.resultList = resultList;
			this.pageNo = 1;
			this.pageSize = resultList.size();
			this.totalSize = resultList.size();
			this.maxPageNo = 1;
			// FIXME: ページングなし一覧取得時の上限値が必要
			this.exceedsLimit = false;
		}

		public ListViewDto(List<Object> resultList, ApexPages.StandardSetController ssc) {
			this.resultList = resultList;
			this.pageNo = ssc.getPageNumber();
			this.pageSize = ssc.getPageSize();
			this.totalSize = ssc.getResultSize();
			this.maxPageNo = this.pageSize != 0 ? ((Integer)(this.totalSize / this.pageSize) + 1) : 1;
			this.exceedsLimit = !ssc.getCompleteResult();
		}
	}

	/**
	 * 組織設定情報Dto
	 */
	public class OrgSettingDto {
		public String language0; // 基準言語
		public String language1;
		public String language2;
		public Map<String, String> languageMap;

		public OrgSettingDto(ComOrgSetting__c setting) {
			this.language0 = setting.Language_0__c;
			this.language1 = setting.Language_1__c;
			this.language2 = setting.Language_2__c;
			this.languageMap = new Map<String, String>{
				ComConst.LANG_TYPE_BASE => this.language0,
				ComConst.LANG_TYPE_EXT1 => this.language1,
				ComConst.LANG_TYPE_EXT2 => this.language2
			};
		}
	}

	/**
	 * 社員情報Dto
	 * マスタ多言語対応のため、BaseDto継承
	 */
	public class EmpDto extends BaseDto {
		public String displayName;
		public String code;
		public Id userId;
		public Id managerId;
		public Id departmentId;
		public Id workingTypeId;
		public String title;
		public Id gradeId;
		public Id companyId;
		public Id additionalDepartmentId;
		public String additionalTitle;
		public Date startDate;
		public Date resignationDate;
		public Id approver1Id;
		public Id approver2Id;
		public Id approver3Id;
		public Id approver4Id;
		public Id approver5Id;
		public Id approver6Id;
		public Id approver7Id;
		public Id approver8Id;
		public Id approver9Id;
		public Id approver10Id;

		public CompanyDto companyInfo;
		public UserDto userInfo;
		public GradeDto gradeInfo;
		public EmpDto(ComEmpBase__c emp) {
			// dummy
		}
	}
	/**
	 * ユーザー情報Dto
	 */
	public class UserDto {
		public Id id;
		public String name;
		public String language;
		public String photoUrl;
		public Id managerId;

		public UserDto(User user) {
			this.id = user.Id;
			this.name = user.Name;
			this.language = user.LanguageLocaleKey;
			this.photoUrl = user.SmallPhotoUrl;
			this.managerId = user.ManagerId;
		}
	}

	/**
	 * グレード情報Dto
	 */
	public class GradeDto extends BaseDto{
		public String code;
		public Integer order;

		public GradeDto(ComGrade__c grade) {
			this.id = grade.Id;
			this.name = ComUtility.getTransFieldValue(grade.Name_L0__c, grade.Name_L1__c, grade.Name_L2__c);
			this.code = grade.Code__c;
			this.order = Integer.valueOf(grade.Order__c);
		}
	}

	/**
	 * 会社情報Dto
	 * マスタ多言語対応のため、BaseDto継承
	 */
	public class CompanyDto extends BaseDto{
		public String code;
		public Id countryId;
		public Boolean useAttendance;
		public Boolean useExpense;
		public Boolean useWorkTime;
		public String taxRounding;
		public Id defaultPaymentExpenseTypeId;
		public Id defaultSourceAccountId;

		public CompanyDto(ComCompany__c company) {
			this.id = company.Id;
			this.name = ComUtility.getTransFieldValue(company.Name_L0__c, company.Name_L1__c, company.Name_L2__c);
			this.code = company.Code__c;
			this.countryId = company.CountryId__c;
			this.useAttendance = company.UseAttendance__c;
			this.useWorkTime = company.UseWorkTime__c;
		}
	}

	/**
	 * 承認情報Dto
	 */
	public class ApproveDto {
		public Id objectId;
		public Id actorId;
		public UserDto actorInfo;
		public String comment;
		public Datetime actedDatetime;

		// 承認情報をセット
		public ApproveDto(ProcessInstanceHistory approveProcess) {
			this.objectId = approveProcess.TargetObjectId;
			this.actorId = approveProcess.ActorId;
			this.comment = approveProcess.Comments;
			this.actedDatetime = approveProcess.CreatedDate;
		}

		// 承認者のユーザー情報をセット
		public void setUserInfo(User user) {
			this.actorInfo = new UserDto(user);
		}
	}

	/**
	 * 添付ファイルDto
	 */
	public class AttachmentDto {
		public Id id;
		public Id parentId;
		public String fileName;
		public String fileBody;
		public String description;

		public AttachmentDto() { }

		// 添付ファイル表示用情報をセット
		public AttachmentDto(Attachment attachmentObj) {
			this.id = attachmentObj.Id;
			this.fileName = attachmentObj.Name;
			this.description = attachmentObj.Description;
		}
	}

	/**
	 * ジョブDto
	 */
	public class JobDto extends BaseDto{
		public String code;
		public Boolean isDirectCharged;
		public Boolean selectabledExpense;
		public Boolean selectabledTimeTrack;
		public Id parentId;
		public Integer childrenCount;

		public JobDto(ComJob__c job) {
			this.id = job.Id;
			this.name = ComUtility.getTransFieldValue(job.Name_L0__c, job.Name_L1__c, job.Name_L2__c);
			this.code = job.Code__c;
			this.isDirectCharged = job.DirectCharged__c;
			this.selectabledTimeTrack = job.SelectabledTimeTrack__c;
			this.parentId = job.ParentId__c;
			this.childrenCount = 0;
			for (ComJob__c children : job.Children__r ) {
				this.childrenCount++;
			}
		}
	}

	/**
	 * 作業分類Dto
	 */
	public class WorkCategoryDto extends BaseDto {
		public WorkCategoryDto(TimeWorkCategory__c workCategory) {
			this.id = workCategory.Id;
			this.name = ComUtility.getTransFieldValue(workCategory.Name_L0__c, workCategory.Name_L1__c, workCategory.Name_L2__c);
		}
	}

}