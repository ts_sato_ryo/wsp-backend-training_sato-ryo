/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * パース機能を提供するクラス
 */
public with sharing class AppParser {

	private static final ComMsgBase MESSAGE = ComMessage.msg();


	/**
	 * 文字列が整数型に変換できるかどうかをチェックする
	 * MaxValをnullにして同名の別関数に渡す。
	 * @param fieldName 項目名。エラーメッセージに利用
	 * @stringValue 変換対象の値
	 */
	public static void validateParseInteger(String fieldName, String stringValue) {
		validateParseInteger(fieldName, stringValue, null);
	}


	/**
	 * 文字列が整数型に変換できるかどうかをチェックする
	 * 変換できない場合はApp.TypeException例外が発生する
	 * @param fieldName 項目名。エラーメッセージに利用
	 * @param stringValue 変換対象の値
	 * @param maxValue 最大値の値
	 */
	public static void validateParseInteger(String fieldName, String stringValue, Integer maxValue) {

		// TODO 例外の型の検討(App.ParameterExceptionで良いのか)

		// nullであれば何もしない
		if (String.isBlank(stringValue)) {
			return;
		}

		// Integerに変換できるかチェック
		try {
			Integer.valueOf(stringValue);
			// 例外が発生なければOK
			return;
		} catch (Exception e) {
			// 次の検証に進む
		}

		// Decimal に変換できるかチェック
		String errMessage = null;
		try {
			Decimal decimalValue = Decimal.valueOf(stringValue);

			if (decimalValue.scale() > 0) {
				// 小数点以下の数値あり。「整数値を指定してください」エラーとする
				errMessage = MESSAGE.Com_Err_InvalidValueInteger(new List<String>{fieldName});
			} else {
				// maxValが設定されている時は、「最大値は(maxVal)以下にしてください。」エラーとする。設定されていない場合は、「値が正しくないエラー」としておく。
				errMessage = maxValue!=null ?
					ComMessage.msg().Com_Err_MaxValue(new List<String>{fieldName, String.valueOf(maxValue)})
					: MESSAGE.Com_Err_InvalidValue(new List<String>{fieldName});
			}
		} catch (Exception e) {
			// 数値に変換できない文字列。「整数値を指定してください」エラーとする
			errMessage = MESSAGE.Com_Err_InvalidValueInteger(new List<String>{fieldName});
		}
		throw new App.TypeException(errMessage);
	}

	/**
	 * 文字列をInteger型に変換する。
	 * @param stringValue Integer型に変換する文字列
	 * @return 変換後の整数値、ただしstringValueがnull,空文字の場合はnullを返却
	 */
	public static Integer parseInt(String stringValue) {
		if (String.isBlank(stringValue)) {
			return null;
		}
		return Integer.valueOf(stringValue.trim());
	}

}
