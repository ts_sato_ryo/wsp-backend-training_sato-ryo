/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description タグAPIを実行するクラス
 */
 public with sharing class TagResource {

	/**
	 * @description タグを表すクラス
	 */
	public class Tag implements RemoteApi.RequestParam, RemoteApi.ResponseParam {
		/** ID */
		public String id;
		/** コード */
		public String code;
		/** タグタイプ */
		public String tagType;
		/** タグ名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** タグ名(言語0) */
		public String name_L0;
		/** タグ名(言語1) */
		public String name_L1;
		/** タグ名(言語2) */
		public String name_L2;
		/** 会社ID */
		public String companyId;
		/** 並び順 */
		public Integer order;
	}


	/**
	 * @description タグ検索処理のパラメータクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** タグID */
		public String id;
		/** 会社ID */
		public String companyId;
		/** タグタイプ */
		public String tagType;
	}

	/**
	 * @description タグ検索処理のレスポンスクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public Tag[] records;
	}

	/**
	 * @description タグレコード作成結果
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成されたレコードID */
		public String id;
	}

	/**
	 * @description タグ検索のAPIクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description タグを検索する
		 * @param  req リクエストパラメータ(SearchCondition)
		 * @return レスポンス(SearchResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);
			Map<String, Object> paramMap = req.getParamMap();

			System.debug(paramMap);

			// 検索条件設定
			TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
			if (paramMap.containsKey('id')){
				filter.ids = new Set<Id>{param.Id};
			}
			if (paramMap.containsKey('companyId')){
				filter.companyIds = new Set<String>{param.companyId};
			}
			if (paramMap.containsKey('tagType')){
				filter.tagTypes = new Set<String>{param.tagType};
			}
			// 検索
			List<TagEntity> searchRecords = new TagRepository().searchEntityList(filter);

			// レスポンスクラスに変換
			List<Tag> tags = new List<Tag>();
			for (TagEntity entity : searchRecords) {
				tags.add(toResponse(entity));
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = tags;
			return res;
		}

		/**
		 * エンティティからレスポンスクラスへ変換する
		 * @param entity 変換対象のエンティティ
		 */
		public Tag toResponse(TagEntity entity) {
			Tag tag = new Tag();
			tag.id = entity.id;
			tag.code = entity.code;
			tag.tagType = entity.tagTypeValue.value;
			tag.name = entity.nameL.getValue();
			tag.name_L0 = entity.nameL0;
			tag.name_L1 = entity.nameL1;
			tag.name_L2 = entity.nameL2;
			tag.companyId = entity.companyId;
			tag.order = entity.order;
			return tag;
		}
	}

	/**
	 * @description タグタイプからアクセス権限があるかどうかを確認する
	 * @param TagType
	 * @return null
	 */
	 private static void hasExecutePermission (Map<String, AttConfigResourcePermission.Permission> permissions, TagType tagType)  {
		AttConfigResourcePermission.Permission permission = permissions.get(tagType.prefix);
		if (permission == null) {
			throw new App.IllegalStateException('[サーバエラー]アクセス権限が見つかりませんでした。');
		}
		new AttConfigResourcePermission().hasExecutePermission(permission);
	 }

	/**
	 * @description タグレコードを1件作成または更新する
	 * @param  param レコード値を含むパラメータ
	 * @param  paramMap レコード値を含むパラメータのマップ
	 * @param  isUpdate レコード更新時はtrue
	 * @return 作成または更新した作業分類レコード
	 */
	private static Id saveRecord (Tag param, Map<String, Object> paramMap, Boolean isUpdate) {
		Set<String> paramKeys = paramMap.keySet();

		// レコード更新時はID必須
		if (isUpdate && String.isBlank(param.id)) {
			throw new App.ParameterException('パラメータ "id" を指定してください');
		}

		TagEntity entity = new TagEntity();

		if (isUpdate) {
			entity.setId(param.Id);
		}
		if (paramKeys.contains('name_L0')) {
			entity.nameL0 = param.name_L0;

			// TODO nameは仮置きでnameL0を設定
			entity.name = param.name_L0;
		}
		if (paramKeys.contains('name_L1')) {
			entity.nameL1 = param.name_L1;
		}
		if (paramKeys.contains('name_L2')) {
			entity.nameL2 = param.name_L2;
		}
		if (paramKeys.contains('tagType')) {
			entity.tagTypeValue = TagType.getType(param.tagType);
		}
		if (paramKeys.contains('code')) {
			entity.code = param.code;
		}
		if (paramKeys.contains('companyId')) {
			entity.companyId = param.companyId;
		}
		if (paramKeys.contains('order')) {
			entity.order = param.order;
		}

		Repository.SaveResult result = (new TagService()).saveTag(entity);
		return result.details[0].id;
	}

	/**
	 * @description タグレコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final Map<String, AttConfigResourcePermission.Permission> requriedPermission =
			// タイプ毎に設定をします。
			new Map<String, AttConfigResourcePermission.Permission>{
				// 短時間勤務
				TagType.SHORTEN_WORK_REASON.prefix =>
					AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING
			};

		/**
		 * @description 作業分類レコードを1件作成する
		 * @param req リクエストパラメータ (WorkCategoryResource.WorkCategory)
		 * @return レスポンス (WorkCategoryResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			Tag param = (Tag)req.getParam(Tag.class);

			// 権限チェック
			TagResource.hasExecutePermission(requriedPermission, TagType.getType(param.tagType));

			Id recordId = TagResource.saveRecord(param, req.getParamMap(), false);

			SaveResult res = new SaveResult();
			res.id = recordId;
			return res;
		}
	}

	/**
	 * @description タグレコード作成APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final Map<String, AttConfigResourcePermission.Permission> requriedPermission =
			// タイプ毎に設定をします。
			new Map<String, AttConfigResourcePermission.Permission>{
				// 短時間勤務
				TagType.SHORTEN_WORK_REASON.prefix =>
					AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING
			};

		/**
		 * @description 作業分類レコードを1件作成する
		 * @param req リクエストパラメータ (WorkCategoryResource.WorkCategory)
		 * @return レスポンス (WorkCategoryResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			Tag param = (Tag)req.getParam(Tag.class);

			// 権限チェック
			TagResource.hasExecutePermission(requriedPermission, TagType.getType(param.tagType));

			TagResource.saveRecord(param, req.getParamMap(), true);
			return null;
		}
	}

	/**
	 * @description タグレコード削除処理のパラメータ
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
	}

	/**
	 * @description タグレコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final Map<String, AttConfigResourcePermission.Permission> requriedPermission =
			// タイプ毎に設定をします。
			new Map<String, AttConfigResourcePermission.Permission>{
				// 短時間勤務
				TagType.SHORTEN_WORK_REASON.prefix =>
					AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING
			};

		/**
		 * @description タグレコードを1件更新する
		 * @param req リクエストパラメータ (WorkCategoryResource.DeleteOption)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// IDがブランクの場合はエラー
			if (String.isBlank(param.id)) {
				throw new App.ParameterException('パラメータ "id" を指定して下さい');
			}

			TagEntity entity = new TagRepository().getEntity(param.id);

			if (entity == null) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}

			// 権限チェック
			TagResource.hasExecutePermission(requriedPermission, entity.tagTypeValue);

			try {
				// レコードを削除
				new TagRepository().deleteEntity(param.Id);
			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

					// TODO 暫定的にデバッグログを出力しておきます
					System.debug(e);

					// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
					//      暫定対応として下記のようなエラーメッセージで対応します。
					// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
					e.setMessage(ComMessage.msg().Com_Err_FaildDeleteReference);
					throw e;
				}
			}

			return null;
		}

	}
}
