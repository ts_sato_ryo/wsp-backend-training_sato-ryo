/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通
 *
 * タグに関するサービスを提供するクラス
 */
public with sharing class TagService {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * タグを保存する
	 * @param savedEntity タグエンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveTag(TagEntity savedEntity) {
		TagRepository rep = new TagRepository();

		// 保存用のエンティティを作成
		TagEntity entity;
		if (savedEntity.id == null) {
			// 新規の場合
			entity = savedEntity;
		} else {
			// 更新の場合は既存の値を取得する
			entity = rep.getEntity(savedEntity.id);
			if (entity == null) {
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
			}
			// 更新値を上書きする
			for (Schema.SObjectField field : savedEntity.getChangedFieldSet()) {
				entity.setFieldValue(field, savedEntity.getFieldValue(field));
			}
		}

		// ユニークキーを設定する
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		if (String.isBlank(entity.code)) {
			// メッセージ：コードが設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}));
		}
		entity.uniqKey = entity.createUniqKey(company.code);

		// バリデーション実行
		Validator.Result validResult = (new ComConfigValidator.TagValidator(entity)).validate();
		if (! validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// コードの重複をチェックする
		TagEntity duplicateCodeEntity = rep.getEntityByCode(entity.code, entity.companyId);
		if ((duplicateCodeEntity != null) && (duplicateCodeEntity.id != entity.id)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		return rep.saveEntity(entity);
	}
}