/**
 * @group 共通
 *
 * PermissionEntityのテスト
 */
@isTest
private class PermissionEntityTest {
	/**
	 * createUniqKeyのテスト
	 * 適切な引数でユニークキーが正しく作成できることを確認する
	 * コードが空の場合、異常終了することを確認する
	 * 会社コードが空の場合、異常終了することを確認する
	 */
	@isTest static void createUniqKeyTest() {
		PermissionEntity entity = new PermissionEntity();

		// Case 1: 会社コードとコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'PermissionCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// Case 2: コードが空の場合
		// 例外が発生する
		entity.code = '';
		App.IllegalStateException actIllegalEx;
		try {
			entity.createUniqKey(companyCode);
		} catch(App.IllegalStateException e) {
			actIllegalEx = e;
		}
		System.assertNotEquals(null, actIllegalEx, 'App.IllegalStateExceptionが発生しませんでした。');
		System.assertEquals('ILLEGAL_STATE', actIllegalEx.getErrorCode());
		System.assertEquals('[サーバエラー]アクセス権限設定のユニークキーが作成できませんでした。コードが設定されていません。', actIllegalEx.getMessage());

		// Case 3: 会社コードが空の場合
		// 例外が発生する
		entity.code = 'CalendarCode';
		companyCode = '';
		App.ParameterException actParamEx;
		try {
			entity.createUniqKey(companyCode);
		} catch(App.ParameterException e) {
			actParamEx = e;
		}
		System.assertNotEquals(null, actParamEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actParamEx.getErrorCode());
		System.assertEquals('[サーバエラー]アクセス権限設定のユニークキーが作成できませんでした。会社コードが空です。', actParamEx.getMessage());

	}
}