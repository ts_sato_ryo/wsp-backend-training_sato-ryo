/**
 * @group 勤怠
 *
 * 変形労働時間制の勤怠計算ロジック
 * 変形労働時間制度固有の勤怠計算ロジックの特徴は以下です。
 * -1 出力結果の労基基準外時間・労働基準内時間・割増未支給労働時間が勤務時間合計になる
 * -2 出力結果の労働基準内時間が勤務時間合計になる
 * -3 週次勤怠計算結果の労基基準超過時間が労基基準超過時間-週の所定勤務時間合計になる
 * -4 週次勤怠計算結果の割増手当追加発生時間が割増未支給労働時間合計-週の所定勤務時間合計になる
 * -5 月次勤怠計算結果の労基基準超過時間が
 *    週次勤怠計算結果の労基基準超過時間 += 前日までの労基基準内時間 + 当日の労基基準内時間 - max(前日までの労基基準内時間, 期間の法定労働時間)になる
 * -6 月次勤怠計算結果の割増手当追加発生時間が
 *    週次勤怠計算結果の割増未支給労働時間 += 前日までの割増未支給労働時間 + 当日の割増未支給労働時間 - max(前日までの割増未支給労働時間, 期間の法定労働時間)になる
 */
public with sharing class AttCalcModifiedLogic  extends AttCalcLogic {
	// Javaでは別のロジックだが、差分はないので一旦固定と同じ日次残業計算ロジックを利用
	/** 日次残業計算ロジック */
	AttCalcOverTimeLogic calcOverTimeLogic = new AttCalcOverTimeLogic();
	/** コンストラクタ */
	public AttCalcModifiedLogic(iOutput output, iInput input) {
		super(output, input);
	}

	/**
	 * 週次計算開始日を取得
	 * ※ 月次計算開始日は日次勤怠計算Inのメソッドとして保持
	 * @param d 対象週に含まれる日付
	 */
	public override Date getStartDateWeekly(Date d) {
		Integer dayOfWeek = DateUtil.dayOfWeek(d);
		return d.addDays(
				input.getConfig(d).beginDayOfWeek > dayOfWeek
					? input.getConfig(d).beginDayOfWeek - dayOfWeek - 7
					: input.getConfig(d).beginDayOfWeek - dayOfWeek
		);
	}
	/**
	 * 日次残業計算ロジックの引数のサブクラス
	 */
	private class CalcAttendanceDailyInput extends AttCalcOverTimeLogic.Input {
		/** 対象日 */
		private Day day;
		/** 設定 */
		private Config conf;
		/**
		 * コンストラクタ
		 * @param input 勤怠計算引数
		 * @param day 計算対象日
		 */
		public CalcAttendanceDailyInput(AttCalcLogic.iInput input, Day day) {
			this.day = day;
			this.conf = input.getConfig(day.getDate());
			this.inputStartTime = day.input.inputStartTime;
			this.inputEndTime = day.input.inputEndTime;
			this.inputRestRanges = day.input.inputRestRanges;
			this.inputRestTime = day.input.inputRestTime;
			this.startTime = day.input.startTime;
			this.endTime = day.input.endTime;
			this.contractWorkTime = day.input.contractWorkTime;
			this.permitedLostTimeRanges = day.input.permitedLostTimeRanges;
			this.convertdRestRanges = day.input.convertdRestRanges;
			this.convertedWorkTime = day.input.convertedWorkTime;
			this.unpaidLeaveRestRanges = day.input.unpaidLeaveRestRanges;
			this.unpaidLeaveTime = day.input.unpaidLeaveTime;
			this.allowedWorkRanges = day.input.allowedWorkRanges;
			this.contractRestRanges = day.input.contractRestRanges ;
			this.contractRestTime = day.input.contractRestTime;
			this.legalWorkTime = conf.LegalWorkTimeDaily;
			this.isWorkDay = day.isWorkDay();
			this.isLegalHoliday = day.input.isLegalHoliday;
			this.isNoOffsetOvertimeByUndertime = conf.isNoOffsetOvertimeByUndertime;
			this.isPermitOvertimeWorkUntilNormalHours = conf.isPermitOvertimeWorkUntilNormalHours;
			this.isIncludeCompensationTimeToWorkHours = conf.isIncludeCompensationTimeToWorkHours;
		}

		/** toStringメソッド */
		public override String toString() {
			return day.toString();
		}
	}

	/**
	 * 日次勤怠計算処理(変形労働時間制)
	 * @param day 日次勤怠計算対象日
	 * @param dayNext 計算対象日翌日　※翌日も対象日の計算に必要
	 * @return 日次勤怠計算の計算結果が変更されたかどうか
	 */
	public override Boolean calcAttendanceDaily(Day day, Day dayNext) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceDaily start');
		System.debug(LoggingLevel.DEBUG, '---day is ' + day.getDate());

		CalcAttendanceDailyInput tmpInput = new CalcAttendanceDailyInput(input, day);
		AttCalcOverTimeLogic.Output tmpOutput = new AttCalcOverTimeLogic.Output();
		// 残業計算と計算結果の記録
		calcOverTimeLogic.apply(tmpOutput, tmpInput);
		day.output.realWorkTime = tmpOutput.realWorkTime;
		day.output.convertedWorkTime = tmpOutput.convertedWorkTime;
		day.output.unpaidLeaveLostTime = tmpOutput.unpaidLeaveLostTime;
		day.output.paidLeaveTime = tmpOutput.paidLeaveTime;
		day.output.unpaidLeaveTime = tmpOutput.unpaidLeaveTime;
		day.output.lateArriveTime = tmpOutput.lateArriveTime;
		day.output.earlyLeaveTime = tmpOutput.earlyLeaveTime;
		day.output.breakTime = tmpOutput.breakTime;
		day.output.lateArriveLostTime = tmpOutput.lateArriveLostTime;
		day.output.earlyLeaveLostTime = tmpOutput.earlyLeaveLostTime;
		day.output.breakLostTime = tmpOutput.breakLostTime;
		day.output.authorizedLateArriveTime = tmpOutput.authorizedLateArriveTime;
		day.output.authorizedEarlyLeaveTime = tmpOutput.authorizedEarlyLeaveTime;
		day.output.authorizedBreakTime = tmpOutput.authorizedBreakTime;
		day.output.unknowBreakTime = tmpOutput.unknowBreakTime;
		system.debug(LoggingLevel.DEBUG, '---workTimeCategoryMap is ' + tmpOutput.workTimeCategoryMap);
		// ****** 変形労働制の日単位の法定外計算 ********
		// FIXME: GENIE-10266の暫定対応（変形の日次残業計算ロジックを作るか相談）
		// AttCalcRangesDto inContractInLegal =
		// getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractInLegal);
		// AttCalcRangesDto inContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractOutLegal);
		AttCalcRangesDto inContractInLegal =
				getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractInLegal)
				.insertAndMerge(getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractOutLegal));
		// ******
		AttCalcRangesDto inContractOutLegal = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractInLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.outContractInLegal);
		AttCalcRangesDto outContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.outContractOutLegal);
		system.debug(LoggingLevel.DEBUG, '---inContractInLegal is ' + inContractInLegal);
		system.debug(LoggingLevel.DEBUG, '---inContractOutLegal is ' + inContractOutLegal);
		system.debug(LoggingLevel.DEBUG, '---outContractInLegal is ' + outContractInLegal);
		system.debug(LoggingLevel.DEBUG, '---outContractOutLegal is ' + outContractOutLegal);
		AttCalcRangesDto inContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto inContractOutLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractOutLegal_nextDay = AttCalcRangesDto.RS();
		day.output.legalOutTime = inContractOutLegal.total() + outContractOutLegal.total();
		day.output.legalInTime = inContractInLegal.total() + outContractInLegal.total();

		if (!getCanCalcContinueNextDay(day, dayNext)) {
			System.debug(LoggingLevel.DEBUG, '---can not CalcContinue to NextDay');
			Integer boundaryTime = input.getConfig(day.getDate()).boundaryTimeOfDay;
			boundaryTime += 24 * 60; // 翌日かを判定するため、24時間後にする
			Integer legalWorkTime = tmpInput.legalWorkTime;
			if(day.isLegalHoliday() && ! dayNext.isLegalHoliday()) { // 法定休日の翌日の勤務時間の取扱
				System.debug(LoggingLevel.DEBUG, '---LegalHoliday to non-LegalHoliday');
				// 法定休日には所定外法定外しか発生しないはずなのでそれだけを扱う
				final AttCalcRangesDto workRangeNextDay = outContractOutLegal.cutAfter(boundaryTime);
				System.debug(LoggingLevel.DEBUG, '---workRangeNextDay is ' + workRangeNextDay);
				outContractInLegal_nextDay = calcOverTimeLogic.getWorkRangesByStartTime(legalWorkTime, workRangeNextDay, AttCalcRangesDto.RS(), AttCalcRangesDto.RS());
				outContractOutLegal_nextDay = calcOverTimeLogic.getWorkRangesByEndTime(legalWorkTime, workRangeNextDay, AttCalcRangesDto.RS(), AttCalcRangesDto.RS());
			}
			else {
				System.debug(LoggingLevel.DEBUG, '---other day');
				outContractInLegal_nextDay = outContractInLegal.cutAfter(boundaryTime);
				outContractOutLegal_nextDay = outContractOutLegal.cutAfter(boundaryTime);
				inContractInLegal_nextDay = inContractInLegal.cutAfter(boundaryTime);
				// 翌日が法定休日で、所定内法定休日が発生する場合、法定休日の所定内勤務にも基本給を払うの設定に従う
				if (dayNext.isLegalHoliday() && input.getConfig(day.getDate()).isPayBasicSalaryLegalHolidayContractedWorkHours) {
					outContractOutLegal_nextDay = outContractOutLegal_nextDay.insertAndMerge(inContractOutLegal.cutAfter(boundaryTime));
				}
				else {
					inContractOutLegal_nextDay = inContractOutLegal.cutAfter(boundaryTime);
				}
			}
			inContractInLegal = inContractInLegal.cutBefore(boundaryTime);
			outContractInLegal = outContractInLegal.cutBefore(boundaryTime);
			outContractOutLegal = outContractOutLegal.cutBefore(boundaryTime);
			// 当日が法定休日で、所定内法定休日が発生する場合、法定休日の所定内勤務にも基本給を払うの設定に従う
			if(day.isLegalHoliday() && input.getConfig(day.getDate()).isPayBasicSalaryLegalHolidayContractedWorkHours) {
				outContractOutLegal = outContractOutLegal.insertAndMerge(outContractOutLegal.cutBefore(boundaryTime));
				inContractOutLegal = AttCalcRangesDto.RS();
			}
			else {
				inContractOutLegal = inContractOutLegal.cutBefore(boundaryTime);
			}
		}
		Integer compensatoryDayTime = day.input.compensatoryDayTime;
		day.output.regularRateWorkTime = 0;
		day.output.legalInTime = 0;
		day.output.legalOutTime = 0;

		system.debug(LoggingLevel.DEBUG, '---inContractInLegal nextDay is ' + inContractInLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---inContractOutLegal nextDay is ' + inContractOutLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---outContractInLegal nextDay is ' + outContractInLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---outContractOutLegal nextDay is ' + outContractOutLegal_nextDay);

		// 代休発生時間と相殺しながら勤務時間の出力を行う
		OutputWorkTimeParam param = new OutputWorkTimeParam(compensatoryDayTime, day.output.unknowBreakTime);
		outputWorkTime(day, day, outContractOutLegal, true, true, param);
		outputWorkTime(day, dayNext, outContractOutLegal_nextDay, true, true, param);
		outputWorkTime(day, day, inContractOutLegal, false, true, param);
		outputWorkTime(day, dayNext, inContractOutLegal_nextDay, false, true, param);
		outputWorkTime(day, day, outContractInLegal, true, false, param);
		outputWorkTime(day, dayNext, outContractInLegal_nextDay, true, false, param);
		// 所定内法定内処理前の入力休憩時間
		Integer inputRestTimeBeforeInContract = param.inputRestTime;
		outputWorkTime(day, day, inContractInLegal, false, false, param);
		outputWorkTime(day, dayNext, inContractInLegal_nextDay, false, false, param);
		day.output.breakTime += Math.max(0, day.output.unknowBreakTime - param.inputRestTime); // 未処理休憩時間 のうち取れたものを休憩時間に追加する
		// 所定内法定内処理に使われる入力休憩時間を控除する
		day.output.breakLostTime += Math.max(0, inputRestTimeBeforeInContract - param.inputRestTime);
		// 労基内労働時間から所定内使ったその他休憩時間を引く
		day.output.legalInTime -= (day.input.inputRestTime - day.output.unknowBreakTime);
		// 出退勤有無フラグ
		Boolean withStartEndTime = (day.input.inputStartTime != null || day.input.inputEndTime != null);
		day.output.realWorkDay = (withStartEndTime ? 1 : 0);
		day.output.legalHolidayWorkCount = (day.isLegalHoliday() && withStartEndTime ? 1 : 0);
		day.output.holidayWorkCount = (day.isHoliday() && withStartEndTime ? 1 : 0);
		// 勤務時間内訳(翌日分とマージする)
		day.output.inContractInLegalRanges = inContractInLegal.insertAndMerge(inContractInLegal_nextDay);
		day.output.inContractOutLegalRanges = inContractOutLegal.insertAndMerge(inContractOutLegal_nextDay);
		day.output.outContractInLegalRanges = outContractInLegal.insertAndMerge(outContractInLegal_nextDay);
		day.output.outContractOutLegalRanges = outContractOutLegal.insertAndMerge(outContractOutLegal_nextDay);
		System.debug(LoggingLevel.DEBUG, '---day.output is ' + day.output);
		System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceDaily end');
		return true;
	}

	/**
	 * 週単位勤怠計算処理(変形労働時間制)
	 * @param day 週次勤怠計算対象週内の1日
	 * @return List<Date> 週次の計算結果が変わった日付のリスト
	 */
	public override List<Date> calcAttendanceWeekly(Date d) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceWeekly start');

		Integer daysInPeriod = 7;// 期間の日数
		List<Date> updateDateList = new List<Date>();
		Day[] dayList = new Day[daysInPeriod];
		for (Integer i = 0; i < daysInPeriod; i++) {
			dayList[i] = db.get(d.addDays(i));
		}

		Integer legalWorkTimeSum = 0;// 労基基準内時間合計
		Integer noPaidAllowanceTimeSum = 0;// 割増未支給労働時間合計

		// ******* 変形労働制の変更点(変形期間設定により週の基準時間の算出) 開始
		Integer weeklyContractedWorkHoursSum = 0; // 週の所定勤務時間合計
		if (input.getConfig(d).modifiedWorkingSystemUnit == AttCalcLogic.ModifiedWorkingSystemUnit.Weekly
				&& input.getConfig(d).weekCountInModifiedWorkingSystemUnit <= 1) {
					// 1週間の変形労働制なら、週の法定労働時間をそのまま使う
					weeklyContractedWorkHoursSum = input.getConfig(d).legalWorkTimeAWeek;
		} else {
					// それ以外なら、実際の所定勤務時間を週で合計する
					for(Integer i = 0; i < daysInPeriod ; i++) {
						weeklyContractedWorkHoursSum += 0;
						if (dayList[i] != null) {
							weeklyContractedWorkHoursSum += dayList[i].input.contractWorkTime;
						}
					}
		}
		final Integer weeklyStandardTime = Math.max(weeklyContractedWorkHoursSum, input.getConfig(d).legalWorkTimeAWeek);// 週の基準時間
		System.debug(LoggingLevel.DEBUG, '---calc weeklyStandardTime is ' + weeklyStandardTime
				+ ' weeklyContractedWorkHoursSum is ' + weeklyContractedWorkHoursSum
				+ ' legalWorkTimeAWeek is ' + input.getConfig(d).legalWorkTimeAWeek);
		// ******* 変形労働制の変更点 終了

		for (Integer i = 0; i < daysInPeriod; i++) {
			if (dayList[i] == null) {
				continue;
			}
			System.debug(LoggingLevel.DEBUG, '---calc d is ' + dayList[i].getDate());
			// Integer legalWorkTimeAWeek = input.getConfig(dayList[i].input.targetDate).legalWorkTimeAWeek;
			Integer legalInTimeTotalUntilPreDay = legalWorkTimeSum;
			Integer regularRateWorkTimeTotalUntilPreDay = noPaidAllowanceTimeSum;
			legalWorkTimeSum += dayList[i].output.legalInTime;
			noPaidAllowanceTimeSum += dayList[i].output.regularRateWorkTime;
			System.debug(LoggingLevel.DEBUG, '---calc legalWorkTimeSum is ' + legalWorkTimeSum);
			System.debug(LoggingLevel.DEBUG, '---calc noPaidAllowanceTimeSum is ' + noPaidAllowanceTimeSum);

			Integer regularOverTime = 0;// 労基基準超過時間
			// ******* 変形労働制の変更点(労基基準超過時間の計算) 開始
			if (legalWorkTimeSum > weeklyStandardTime) {
				regularOverTime = noPaidAllowanceTimeSum - Math.max(legalInTimeTotalUntilPreDay, weeklyStandardTime);
			}
			// ******* 変形労働制の変更点 終了

			Integer additionalAllowanceTime = 0;// 割増手当追加発生時間
			// ******* 変形労働制の変更点(割増手当追加発生時間の計算) 開始
			if (noPaidAllowanceTimeSum > weeklyStandardTime) {
				additionalAllowanceTime = noPaidAllowanceTimeSum - Math.max(regularRateWorkTimeTotalUntilPreDay, weeklyStandardTime);
			}
			// ******* 変形労働制変更点 終了

			// 変更があったかどうかのチェック
			if (dayList[i].output.regularOverTime != regularOverTime
					|| dayList[i].output.additionalAllowanceTime != additionalAllowanceTime
					|| dayList[i].output.noPaidAllowanceTimeSum != noPaidAllowanceTimeSum) {
						dayList[i].output.regularOverTime = regularOverTime;
						dayList[i].output.noPaidAllowanceTimeSum = noPaidAllowanceTimeSum;
						dayList[i].output.additionalAllowanceTime = additionalAllowanceTime;
						dayList[i].output.legalWorkTimeSum = legalWorkTimeSum;
						updateDateList.add(dayList[i].input.targetDate);
			}
			System.debug(LoggingLevel.DEBUG, '---calc regularOverTime is ' + dayList[i].output.regularOverTime);
			System.debug(LoggingLevel.DEBUG, '---calc noPaidAllowanceTimeSum is ' + dayList[i].output.noPaidAllowanceTimeSum);
			System.debug(LoggingLevel.DEBUG, '---calc additionalAllowanceTime is ' + dayList[i].output.additionalAllowanceTime);
		}
		System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceWeekly end');
		return updateDateList;
	}

	/**
	 * 月次勤怠計算処理(変形労働時間制)
	 * @param day 月次勤怠計算対象週内の1日
	 */
	public override void calcAttendanceMonthly(Date d) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceMonthly start');
		Date startDate = input.getStartDateMonthly(d);
		Date endDate = input.getEndDateMonthly(d);
		TotalOutput totalOutput = new TotalOutput();

		Integer legalInTimeTotalUntilPreDay = 0;// 前日までの労基基準内時間
		Integer regularRateWorkTimeTotalUntilPreDay = 0;// 前日までの割増未支給労働時間
		Integer legalWorkTimeInPeriod = 0;// 期間の法定労働時間
		if (input.getConfig(d).modifiedWorkingSystemUnit == AttCalcLogic.ModifiedWorkingSystemUnit.Monthly) {
			if (input.getConfig(d).monthCountInModifiedWorkingSystemUnit <= 1) {
				Integer daysInPeriod = startDate.daysBetween(endDate) + 1;
				// 期間の法定労働時間 = 月内週数(暦日/7日)*時間から分への換算係数(60)*月内の週平均の法定労働時間(日本では労基法で40)
				legalWorkTimeInPeriod = 40 * 60 * daysInPeriod / 7;

			// 2ヶ月〜1年の変形労働制
			} else {
				// TODO: 期間の法定労働時間の計算
				// TODO: 前月までの労基基準内時間 と 割増未支給労働時間 の集計
			}
		} else if (input.getConfig(d).modifiedWorkingSystemUnit == AttCalcLogic.ModifiedWorkingSystemUnit.Weekly) {
			// TODO:
			// assert in.設定().変形労働制期間の週数 == 1;
		}
		System.debug(LoggingLevel.DEBUG, '---calc monthly legalWorkTimeInPeriod is ' + legalWorkTimeInPeriod);
		for (Date dt = startDate; dt <= endDate; dt = dt.addDays(1)) {
			Day day = db.get(dt);
			if (day == null) {
				continue;
			}

			// ******* 変形労働制の変更点(月次の法定労働時間の再調整) 開始
			// 週次で行ったものを、月次の法定労働時間で再調整
			Integer legalInTimeTotalUntilToday = day.output.legalInTime - day.output.regularOverTime;// 当日の労基基準内時間
			Integer regularRateWorkTimeToday = day.output.regularRateWorkTime - day.output.additionalAllowanceTime;// 当日の割増未支給労働時間
			if(legalInTimeTotalUntilPreDay + legalInTimeTotalUntilToday > legalWorkTimeInPeriod) {
				// day.out.労基基準超過時間 += 前日までの労基基準内時間 + 当日の労基基準内時間 - max(前日までの労基基準内時間, 期間の法定労働時間);
				day.output.regularOverTime += legalInTimeTotalUntilPreDay + legalInTimeTotalUntilToday - Math.max(legalInTimeTotalUntilPreDay, legalWorkTimeInPeriod);
			}
			System.debug(LoggingLevel.DEBUG, '---calc monthly regularRateWorkTimeTotalUntilPreDay + regularRateWorkTimeToday is ' + (regularRateWorkTimeTotalUntilPreDay + regularRateWorkTimeToday));
			if(regularRateWorkTimeTotalUntilPreDay + regularRateWorkTimeToday > legalWorkTimeInPeriod) {
				Integer regularRateWorkTimePerDay = 0;// 週の割増未支給労働時間をあらかじめセットする
				// day.out.割増未支給労働時間 += 前日までの割増未支給労働時間 + 当日の割増未支給労働時間 - max(前日までの割増未支給労働時間, 期間の法定労働時間);
				regularRateWorkTimePerDay += regularRateWorkTimeTotalUntilPreDay + regularRateWorkTimeToday - Math.max(regularRateWorkTimeTotalUntilPreDay, legalWorkTimeInPeriod);
				day.output.additionalAllowanceTime += regularRateWorkTimePerDay;
			}
			// 前日までの労基基準内時間 += 当日の労基基準内時間;
			legalInTimeTotalUntilPreDay += legalInTimeTotalUntilToday;
			// 前日までの割増未支給労働時間 += 当日の割増未支給労働時間;
			regularRateWorkTimeTotalUntilPreDay += regularRateWorkTimeToday;
			System.debug(LoggingLevel.DEBUG, '---calc monthly regularOverTime is ' + day.output.regularOverTime);
			System.debug(LoggingLevel.DEBUG, '---calc monthly regularRateWorkTime is ' + day.output.regularRateWorkTime);
			System.debug(LoggingLevel.DEBUG, '---calc monthly additionalAllowanceTime is ' + day.output.additionalAllowanceTime);

			// カスタマイズ再計算変形(day);
			calcAttendanceCustomModified(day);
			// ******* 変形労働制の変更点 終了

			totalOutput.realWorkTime += day.output.realWorkTime;
			totalOutput.convertedWorkTime += day.output.convertedWorkTime;
			totalOutput.unpaidLeaveLostTime += day.output.unpaidLeaveLostTime;
			totalOutput.paidLeaveTime += day.output.paidLeaveTime;
			totalOutput.unpaidLeaveTime += day.output.unpaidLeaveTime;
			totalOutput.lateArriveTime += day.output.lateArriveTime;
			totalOutput.earlyLeaveTime += day.output.earlyLeaveTime;
			totalOutput.breakTime += day.output.breakTime;
			totalOutput.lateArriveLostTime += day.output.lateArriveLostTime;
			totalOutput.earlyLeaveLostTime += day.output.earlyLeaveLostTime;
			totalOutput.breakLostTime += day.output.breakLostTime;
			totalOutput.authorizedLateArriveTime += day.output.authorizedLateArriveTime;
			totalOutput.authorizedEarlyLeaveTime += day.output.authorizedEarlyLeaveTime;
			totalOutput.authorizedBreakTime += day.output.authorizedBreakTime;
			totalOutput.paidBreakTime += day.output.paidBreakTime;
			totalOutput.authorizedOffTime += day.output.authorizedOffTime;
			totalOutput.legalOutTime += day.output.legalOutTime  + day.output.regularOverTime;
			totalOutput.legalInTime += day.output.legalInTime - day.output.regularOverTime;
			totalOutput.regularRateWorkTime += day.output.noPaidAllowanceTimeSum - day.output.additionalAllowanceTime;// 割増未支給労働時間
			totalOutput.realWorkDay += day.output.RealWorkDay;
			totalOutput.workDay += day.output.workDay;
			totalOutput.legalHolidayWorkCount += day.output.legalHolidayWorkCount;
			totalOutput.holidayWorkCount += day.output.holidayWorkCount;
			for (WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
				Integer work = day.output.workTimeCategoryMap.get(c);
				Integer sum = totalOutput.workTimeCategoryMap.containsKey(c)
									? totalOutput.workTimeCategoryMap.get(c)
									: 0;
				totalOutput.workTimeCategoryMap.put(c,sum + work);
			}
			for (Integer j = 0; j < day.output.customWorkTimes.size(); j++) {
				totalOutput.customWorkTimes[j] += day.output.customWorkTimes[j];
			}
		}
		output.updateTotal(startDate, totalOutput);
				System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceMonthly end');
	}

	/**
	 * 変形労働時間制のカスタマイズ勤怠計算項目の計算
	 * @param day カスタマイズ勤怠計算計算対象日
	 */
	private void calcAttendanceCustomModified(Day day) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceCustomModified start');
		System.debug(LoggingLevel.DEBUG, '---day is ' + day.getDate());
		for(Integer i = 0; i < day.output.customWorkTimes.size(); i++) {
			day.output.customWorkTimes[i] = 0;
		}
		for(WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
			Integer rate = calcAllowanceWithoutNight(day, c);
			Integer work = day.output.workTimeCategoryMap.get(c);
			if (rate == 100) {
				day.output.customWorkTimes[0] += work;
			}
			else if (rate == 125) {
				day.output.customWorkTimes[1] += work;
			}
			else if (rate == 135) {
				day.output.customWorkTimes[2] += work;
			}
			else if (rate == 25) {
				day.output.customWorkTimes[3] += work;
			}
			else if (rate == 35) {
				day.output.customWorkTimes[4] += work;
			}
			if(c.isNight)
				day.output.customWorkTimes[5] += work;
			if(c.isCompensatory)
				day.output.customWorkTimes[6] += work;
		}
		System.debug(LoggingLevel.DEBUG, '---day.output.additionalAllowanceTime is ' + day.output.additionalAllowanceTime);
		// 所定内＋時間外割増の時間を、法定外に変換する(便利上)
		Integer allowanceOffsetedWeekly = Math.min(day.output.customWorkTimes[0], day.output.additionalAllowanceTime);// 週間相殺分
		day.output.customWorkTimes[0] -= allowanceOffsetedWeekly;
		day.output.customWorkTimes[1] += allowanceOffsetedWeekly;
		day.output.customWorkTimes[3] += day.output.additionalAllowanceTime - allowanceOffsetedWeekly;

		System.debug(LoggingLevel.DEBUG, '---AttCalcModifiedLogic.calcAttendanceCustomModified end');
	}

	/**
	 * 割増率の計算
	 * @param day 計算対象日
	 * @param c 勤務時間の種類
	 */
	private Integer calcAllowance(Day day, WorkTimeCategory c) {
		return calcAllowance(day, c.dayType, c.isContractOut, c.isLegalOut, c.isNight, c.isCompensatory);
	}

	/**
	 * 代休・夜間勤務時間を除いた割増率の計算
	 * @param day 計算対象日
	 * @param c 勤務時間の種類
	 */
	private Integer calcAllowanceWithoutNight(Day day, WorkTimeCategory c) {
		return calcAllowance(day, c.dayType, c.isContractOut, c.isLegalOut, false, false);
	}

	/**
	 * 割増率の計算
	 * @param day 計算対象日
	 * @param dayType 日種別
	 * @param isContractOut 所定外
	 * @param isLegalOut 法定外
	 * @param isNight 夜間勤務
	 * @param isCompensatory 代休発生
	 */
	private Integer calcAllowance(Day day, AttCalcAutoHolidayLogic.DayType dayType, boolean isContractOut, boolean isLegalOut, boolean isNight, boolean isCompensatory) {
		Integer up = 0;
		if (isContractOut) {
			up += 100;
		}
		if (dayType == AttCalcAutoHolidayLogic.DayType.LegalHoliday) {
			up += 35;
		} else {
			if (isLegalOut) {
				up += 25;
			} else if (isContractOut) {
				if (input.getConfig(day.getDate()).isPayAllowanceWhenOverContructedWorkHours) {
					up += 25;
				} else if (dayType == AttCalcAutoHolidayLogic.DayType.Holiday
						&& input.getConfig(day.getDate()).isPayAllowanceOnEveryHoliday) {
					up += 25;
				}
			}
		}
		if (isNight) {
			up += 25;
		}
		if (isCompensatory) {
			up -= 100;
		}
		return up;
	}

	/**
	 * カスタマイズ勤怠計算項目の計算
	 * 固定など他の労働制では通常のタイミングで計算を行わず、月次で行う
	 * AttCalcModifirdLogic.calcAttendanceCustomModifiedを参照
	 * @param day カスタマイズ勤怠計算計算対象日
	 */
	public override void calcAttendanceCustom(Day day) {
		// ****** 変形労働制の変更点(カスタム勤怠計算は月次で行うので何もしない) 開始
		// ****** 変形労働制の変更点 終了
	}

	/**
	 * 労働時間出力引数
	 */
	private class OutputWorkTimeParam {
		/**
		* 労働時間出力引数(コンストラクタ)
		* @param compensatoryDayTime 代休発生時間
		* @param inputRestTime 取得休憩時間
	 */
		OutputWorkTimeParam(Integer compensatorTime, Integer inputRestTime) {
			this.compensatoryTime = compensatoryTime == null ? 0 : compensatoryTime;
			this.inputRestTime = inputRestTime == null ? 0 : inputRestTime;
		}
		/** 代休発生時間 */
		public Integer compensatoryTime;
		/** 取得休憩時間 */
		public Integer inputRestTime;
	}

	/**
	 * 労働時間出力
	 * @param outputDay 出力日
	 * @param adjustmentDay 判定日
	 * @param workTimeRanges 勤務時間
	 * @param isContractOut 所定外かどうか
	 * @param isLegalOut 法定外かどうか
	 * @param compensatoryDayTime 代休発生時間
	 * @param inputRestTime 代休発生時間
	 * @return 計算後の代休発生時間(引数の代休発生時間ー日中の代休発生時間ー夜間の代休発生時間)
	 */
	private void outputWorkTime(Day outputDay, Day adjustmentDay, AttCalcRangesDto workTimeRanges, boolean isContractOut, boolean isLegalOut, OutputWorkTimeParam param) {
		if (workTimeRanges.total() > 0) {
			// ------------------
			// 日中の勤務時間を計算する
			// ------------------
			Integer dayTime = workTimeRanges.include(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			Integer dayRestTime = Math.min(param.inputRestTime, dayTime); // 日中取得休憩
			param.inputRestTime -= dayRestTime;
			dayTime -= dayRestTime;
			// 所定外で代休発生時間がある場合は、代休ありの勤務時間を計算する
			Integer dayCompensatoryTime = isContractOut ? Math.min(dayTime, param.compensatoryTime) : 0;
			if (dayCompensatoryTime > 0) {
				outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, true), dayCompensatoryTime);
			}
			// 残りの時間を出力する
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, false), dayTime - dayCompensatoryTime);
			// 代休と相殺した時間を差し引く
			param.compensatoryTime -= dayCompensatoryTime;

			// ------------------
			// 夜間の勤務時間を計算する
			// ------------------
			Integer nightTime = workTimeRanges.exclude(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			Integer inputRestTimeNight = Math.min(param.inputRestTime, nightTime);
			param.inputRestTime -= inputRestTimeNight;
			nightTime -= inputRestTimeNight;
			// 所定外で代休発生時間がある場合は、代休ありの勤務時間を計算する
			Integer nightCompensatoryTime = isContractOut ? Math.min(nightTime, param.compensatoryTime) : 0;
			if (nightCompensatoryTime > 0) {
				outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, true), nightCompensatoryTime);
			}
			// 残りの時間を出力する
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, false), nightTime - nightCompensatoryTime);
			// 代休と相殺した時間を差し引く
			param.compensatoryTime -= nightCompensatoryTime;

			if (!adjustmentDay.isLegalHoliday()) {
				// ------------------
				// 法定休日ではない日は週の法定基準オーバーの対象を計算するための勤務時間を計算する
				// ------------------
				if (isLegalOut) {
					// 法定外の労働は労基の基準外時間に計算
					outputDay.output.legalOutTime += (dayTime + nightTime); // 労基基準外時間
				} else {
					// 法定内の労働は労基の基準内時間に計算
					outputDay.output.legalInTime += (dayTime + nightTime); // 労基基準内時間

					// ------------------
					// さらに割増手当を払っていない時間を別途計算する
					// ------------------
					// 所定外法定内の場合
					if (isContractOut) {
						// 所定外で法定内の場合
						//  勤務日で"法定内残業も割増を払う"がOFFなら払っていない
						//  所定休日で"法定内残業も割増を払う"も"法定内残業も割増を払う"もOFFなら払っていない
						if (adjustmentDay.isWorkDay()) {
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) { // 法定内残業も割増を払
								outputDay.output.regularRateWorkTime += (dayTime + nightTime);
							}
						}
						else {
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceOnEveryHoliday // 所定休日の勤務は法定内も割増を払う
									&& !input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) { // 法定内残業も割増を払う
								outputDay.output.regularRateWorkTime += (dayTime + nightTime);
							}
						}
					}
					// 所定内の時間は全部記録する
					else {
						outputDay.output.regularRateWorkTime += (dayTime + nightTime);
					}
				}
			}

		}
	}

	/**
	 * 空の初期値を返せる処理（ユーティリティメソッド）
	 * 種別ごとの勤務時間を計算するマップに勤務時間が入っていれば、該当する勤務時間を返し、なければ空のAttCalcRangesDtoを返却する
	 * @param rangesMap 種別ごとの勤務時間を計算するマップ
	 * @param key 種別ごとの勤務時間を表すキー
	 *  --- InContractInLegal 法定内所定内勤務時間
	 *  --- InContractOutLegal 法定内所定外勤務時間
	 *  --- OutContractInLegal 法定外所定内勤務時間
	 *  --- OutContractOutLegal 法定外所定外勤務時間
	 */
	private AttCalcRangesDto getOrDefaultRanges(
			Map<AttCalcOverTimeLogic.OverTimeCategory,
			AttCalcRangesDto> rangesMap, AttCalcOverTimeLogic.OverTimeCategory key) {
		if (rangesMap.containsKey(key)) {
			return rangesMap.get(key);
		}
		return AttCalcRangesDto.RS();
	}
}