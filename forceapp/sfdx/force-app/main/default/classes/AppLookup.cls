/**
 * 参照の項目の情報を持つ値オブジェクト
 * 値オブジェクトはサブクラスとして定義してください。
 */
public with sharing class AppLookup {

	/** 部署の情報*/
	public class Department extends AppLookup.Master {
		/** 部署コード */
		public String code {public get; private set;}
		/** 部署名 */
		public AppMultiString name {public get; private set;}
		/** コンストラクタ */
		public Department(String code, AppMultiString name, AppDate validFrom, AppDate validTo, Boolean isRemoved) {
			super(validFrom, validTo, isRemoved);
			this.code = code;
			this.name = name;
		}

		/**
		 * オブジェクトの値が等しいかどうか判定する
		 * @param compare 比較対象のオブジェクト
		 */
		public override Boolean equals(Object compare) {
			Boolean ret;
			if (compare instanceof Department) {
				Department compareDept = (Department)compare;
				if (this.name != compareDept.name) {
					ret = false;
				} else if (! equalsMasterField(compareDept)) {
					ret = false;
				} else {
					ret = true;
				}
			} else {
				ret = false;
			}
			return ret;
		}
	}

	/** 社員の付加情報(参照専用)*/
	public class Employee extends AppLookup.Master {
		/** 社員コード */
		public String code {public get; private set;}
		/** 社員の氏名 */
		public AppPersonName fullName {public get; private set;}
		/** */
		public Id userId {public get; private set;}
		/** Indicate is Active Salesforce User Acc */
		public Boolean isActiveSFUserAcc {public get; private set;}
		/** 顔写真URL */
		public String photoUrl {public get; private set;}
		public Employee(String code, AppPersonName fullName, Id userId, String photoUrl, AppDate validFrom, AppDate validTo, Boolean isRemoved, Boolean isActive) {
			super(validFrom, validTo, isRemoved);
			this.code = code;
			this.fullName = fullName;
			this.userId = userId;
			this.photoUrl = photoUrl;
			this.isActiveSFUserAcc = isActive;
		}

		/**
		 * オブジェクトの値が等しいかどうか判定する
		 * @param compare 比較対象のオブジェクト
		 */
		public override Boolean equals(Object compare) {
			Boolean ret;
			if (compare instanceof Employee) {
				Employee compareEmp = (Employee)compare;
				if (this.fullName != compareEmp.fullName) {
					ret = false;
				} else if (! equalsMasterField(compareEmp)) {
					ret = false;
				} else {
					ret = true;
				}
			} else {
				ret = false;
			}
			return ret;
		}
	}

	/** マスターの情報の値オブジェクト*/
	public abstract class Master extends ValueObject {
		/** 有効開始日 */
		public AppDate validFrom {public get; private set;}
		/** 有効終了日 */
		public AppDate validTo {public get; private set;}
		/** 削除フラグ */
		public Boolean isRemoved {public get; private set;}

		public Master(AppDate validFrom, AppDate validTo, Boolean isRemoved) {
			this.validFrom = validFrom;
			this.validTo = validTo;
			this.isRemoved = isRemoved;
		}

		/**
		 * オブジェクトの値が等しいかどうか判定する
		 * @param compare 比較対象のオブジェクト
		 */
		public Boolean equalsMasterField(Master compare) {
			Boolean ret;
			if ((this.isRemoved != compare.isRemoved)
					|| (this.validFrom != compare.validFrom)
					|| (this.validTo != compare.validTo)) {
					ret = false;
			} else {
				ret = true;
			}
			return ret;
		}
	}
}