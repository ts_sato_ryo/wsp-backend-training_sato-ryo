/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description PlannerSettingServiceのテストクラス
 */
@isTest
private class PlannerSettingServiceTest {

	/**
	 * テストデータ
	 */
	private class TestData {

		public ComCompany__c createTestData() {
			CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();
			ComCompany__c testCompany = ComTestDataUtility.createCompany('testCompany', ComTestDataUtility.createCountry('ja').Id);
			CompanyPrivateSettingEntity entity1 = new CompanyPrivateSettingEntity();
			entity1.companyId = testCompany.Id;
			repository.saveEntity(entity1);
			return testCompany;
		}
	}

	/*
	 * saveOAuthCodeのテスト
	 * 認証コードを保存できることを検証
	 */
	@isTest
	static void saveOAuthCodeTestSaveCode() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();

		// パラメータ作成
		String state = service.generateState(testCompany.Id);
		CompanyPrivateSettingEntity entity2 = repository.getEntity(testCompany.Id);
		entity2.office365AuthCode = 'Test';

		// 実行
		Id resultId = service.saveOAuthCode(entity2, state);

		// 検証
		CompanyPrivateSettingEntity resultEntity = repository.getEntity(testCompany.Id);
		System.assertEquals(entity2.Id, resultId);
		System.assertEquals('Test', resultEntity.office365AuthCode);
		System.assertEquals(entity2.cryptoKey, resultEntity.cryptoKey);
	}

	/*
	 * saveOAuthCodeのテスト
	 * ステートの検証で例外が発生することを検証
	 * 認証日時が10分以内ではない場合
	 */
	@isTest
	static void saveOAuthCodeTestInvalidTime() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();

		// パラメータ作成
		Id userId = UserInfo.getUserId();
		String publishedDateTimeString = String.valueOfGmt(Datetime.now().addMinutes(10));
		String oneTimeKey = (String)userId + publishedDateTimeString;
		service.setCryptoKey(testCompany.Id);
		String oneTimeKey2 = service.encryptData(oneTimeKey, testCompany.Id);
		String lang = UserInfo.getLanguage();
		String sfUrl = URL.getSalesforceBaseUrl().toExternalForm();
		String company = testCompany.id;
		String state = oneTimeKey2 + ',' + lang + ',' + sfUrl + ',' + company;
		CompanyPrivateSettingEntity entity2 = repository.getEntity(testCompany.Id);
		entity2.office365AuthCode = 'Test';

		// 実行
		try {
			Id resultId = service.saveOAuthCode(entity2, state);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Admin_Err_VerificationFailed, e.getMessage());
		}
	}

	/*
	 * saveOAuthCodeのテスト
	 * ステートの検証で例外が発生することを検証
	 * UserIdが異なるケース
	 */
	@isTest
	static void saveOAuthCodeTestInvalidUser() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();

		// パラメータ作成
		String state = service.generateState(testCompany.Id);
		CompanyPrivateSettingEntity entity2 = repository.getEntity(testCompany.Id);
		entity2.office365AuthCode = 'Test';

		// 実行
		System.runAs(ComTestDataUtility.createUser('testName', 'ja', UserInfo.getLocale())) {
			try {
				Id resultId = service.saveOAuthCode(entity2, state);
				System.assert(false);
			} catch (App.ParameterException e) {
				// 検証
				System.assertEquals(ComMessage.msg().Admin_Err_VerificationFailed, e.getMessage());
			}
		}
	}

	/*
	 * saveOAuthCodeのテスト
	 * ステートの検証で例外が発生することを検証
	 * ステートのサイズが不足しているケース
	 */
	@isTest
	static void saveOAuthCodeTestInvalidState() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();

		// パラメータ作成
		String state = 'ja,http://';
		CompanyPrivateSettingEntity entity2 = repository.getEntity(testCompany.Id);
		entity2.office365AuthCode = 'Test';

		// 実行
		try {
			Id resultId = service.saveOAuthCode(entity2, state);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Admin_Err_VerificationFailed, e.getMessage());
		}
	}

	/*
	 * savePlannerSettingのテスト
	 * 外部カレンダー連携の設定を保存できることを検証
	 */
	@isTest
	static void savePlannerSettingTestSuccess() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyRepository repo = new CompanyRepository();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		CompanyEntity resultEntityBefore = repo.getEntity(testCompany.Id);
		System.assert(!resultEntityBefore.useCalendarAccess);
		System.assert(resultEntityBefore.calendarAccessService == null);

		// 会社別非公開設定（カスタム設定）の件数
		List<CompanyPrivateSetting__c> cpsObjectsBefore = [SELECT Id FROM CompanyPrivateSetting__c];
		System.assertEquals(1, cpsObjectsBefore.size());

		// パラメータ作成
		CompanyEntity comEntity = new CompanyEntity();
		comEntity.setId(testCompany.Id);
		comEntity.useCalendarAccess = true;
		comEntity.calendarAccessService = CalendarAccessServiceName.OFFICE_365;

		// 実行
		Id resultId = service.savePlannerSetting(comEntity);

		// 検証
		CompanyEntity resultEntityAfter = repo.getEntity(testCompany.Id);
		System.assert(resultEntityAfter.useCalendarAccess);
		System.assertEquals(CalendarAccessServiceName.OFFICE_365, resultEntityAfter.calendarAccessService);
		System.assertEquals(testCompany.Id, resultId);

		// 会社別非公開設定（カスタム設定）の件数
		List<CompanyPrivateSetting__c> cpsObjectsAfter = [SELECT Id FROM CompanyPrivateSetting__c];
		System.assertEquals(1, cpsObjectsAfter.size());
	}

	/*
	 * savePlannerSettingのテスト
	 * 外部カレンダー連携が無効の場合に、外部連携サービスが保存されないことを検証
	 */
	@isTest
	static void savePlannerSettingTestInActiveCalendar() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyRepository repo = new CompanyRepository();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		CompanyEntity comEntityBefore = repo.getEntity(testCompany.Id);
		comEntityBefore.useCalendarAccess = true;
		comEntityBefore.calendarAccessService = CalendarAccessServiceName.OFFICE_365;
		repo.saveEntity(comEntityBefore);

		// パラメータ作成
		CompanyEntity comEntity = new CompanyEntity();
		comEntity.setId(testCompany.Id);
		comEntity.useCalendarAccess = false;
		comEntity.calendarAccessService = null;

		// 実行
		Id resultId = service.savePlannerSetting(comEntity);

		// 検証
		CompanyEntity resultEntityAfter = repo.getEntity(testCompany.Id);
		System.assert(!resultEntityAfter.useCalendarAccess);
		System.assertEquals(CalendarAccessServiceName.OFFICE_365, resultEntityAfter.calendarAccessService);
		System.assertEquals(testCompany.Id, resultId);
	}

	/*
	 * savePlannerSettingのテスト
	 * 更新対象の会社エンティティを取得できずに例外が発生することを検証
	 */
	@isTest
	static void savePlannerSettingTestCompanyNull() {

		PlannerSettingService service = new PlannerSettingService();

		// パラメータ作成
		CompanyEntity comEntity = new CompanyEntity();
		comEntity.setId('a0I7F000004H0TuUAK');
		comEntity.useCalendarAccess = true;
		comEntity.calendarAccessService = CalendarAccessServiceName.OFFICE_365;

		// 実行
		try {
			service.savePlannerSetting(comEntity);
			System.assert(false);
		} catch (App.RecordNotFoundException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, e.getMessage());
		}
	}

	/*
	 * createPrivateSettingのテスト
	 * 会社別非公開設定（カスタム設定）が正常に作成されることを検証する
	 */
	@isTest
	static void createPrivateSettingTestCreate() {

		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();
		PlannerSettingService service = new PlannerSettingService();

		// データセット
		ComCompany__c testCompany = ComTestDataUtility.createCompany('testCompany', ComTestDataUtility.createCountry('ja').Id);
		List<CompanyPrivateSetting__c> cpsObjectsBefore = [SELECT Id FROM CompanyPrivateSetting__c];
		System.assertEquals(0, cpsObjectsBefore.size());

		// 実行
		service.createPrivateSetting(testCompany.Id);

		// 検証
		CompanyPrivateSettingEntity cpsEntity = repository.getEntity(testCompany.Id);
		System.assertEquals(testCompany.Id, cpsEntity.companyId);
		System.assert(cpsEntity.cryptoKey == null);
		System.assert(cpsEntity.office365AuthCode == null);
		List<CompanyPrivateSetting__c> cpsObjectsAfter = [SELECT Id FROM CompanyPrivateSetting__c];
		System.assertEquals(1, cpsObjectsAfter.size());
	}

	/*
	 * createPrivateSettingのテスト
	 * 会社別非公開設定（カスタム設定）が作成されないことを検証する
	 */
	@isTest
	static void createPrivateSettingTestNotCreate() {

		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();
		PlannerSettingService service = new PlannerSettingService();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		List<CompanyPrivateSetting__c> cpsObjectsBefore = [SELECT Id FROM CompanyPrivateSetting__c];
		System.assertEquals(1, cpsObjectsBefore.size());

		// 実行
		service.createPrivateSetting(testCompany.Id);

		// 検証
		CompanyPrivateSettingEntity cpsEntity = repository.getEntity(testCompany.Id);
		System.assertEquals(testCompany.Id, cpsEntity.companyId);
		System.assert(cpsEntity.cryptoKey == null);
		System.assert(cpsEntity.office365AuthCode == null);
		List<CompanyPrivateSetting__c> cpsObjectsAfter = [SELECT Id FROM CompanyPrivateSetting__c];
		System.assertEquals(1, cpsObjectsAfter.size());
	}

	/*
	 * clearPrivateSettingのテスト
	 * 認証情報（暗号化キー、Office365API認証キー）が正常にクリアされることを検証する
	 * カレンダー連携が無効の場合
	 */
	@isTest
	static void clearPrivateSettingTestInActive() {

		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();
		PlannerSettingService service = new PlannerSettingService();
		CompanyRepository repo = new CompanyRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		CompanyPrivateSettingEntity cpsEntityBefore = repository.getEntity(testCompany.Id);
		cpsEntityBefore.cryptoKey = 'testCryptoKey';
		cpsEntityBefore.office365AuthCode = 'testOffice365AuthCode';
		repository.saveEntity(cpsEntityBefore);

		// 更新対象のエンティティに値をセット
		CompanyEntity updateEntity = repo.getEntity(testCompany.Id);
		updateEntity.useCalendarAccess = true;
		updateEntity.calendarAccessService = CalendarAccessServiceName.OFFICE_365;

		// 保存項目の値をセット
		CompanyEntity comEntity = new CompanyEntity();
		comEntity.setId(testCompany.Id);
		comEntity.useCalendarAccess = false;
		comEntity.calendarAccessService = CalendarAccessServiceName.OFFICE_365;

		// 実行
		service.clearPrivateSetting(comEntity, updateEntity);

		// 検証
		CompanyPrivateSettingEntity cpsEntityAfter = repository.getEntity(testCompany.Id);
		System.assert(cpsEntityAfter.cryptoKey == null);
		System.assert(cpsEntityAfter.office365AuthCode == null);
	}

	/*
	 * clearPrivateSettingのテスト
	 * 認証情報（暗号化キー、Office365API認証キー）が正常にクリアされることを検証する
	 * 連携対象サービスを変更した場合
	 */
	@isTest
	static void clearPrivateSettingTestChangeService() {

		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();
		PlannerSettingService service = new PlannerSettingService();
		CompanyRepository repo = new CompanyRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		CompanyPrivateSettingEntity cpsEntityBefore = repository.getEntity(testCompany.Id);
		cpsEntityBefore.cryptoKey = 'testCryptoKey';
		cpsEntityBefore.office365AuthCode = 'testOffice365AuthCode';
		repository.saveEntity(cpsEntityBefore);

		// 更新対象のエンティティに値をセット
		CompanyEntity updateEntity = repo.getEntity(testCompany.Id);
		updateEntity.useCalendarAccess = true;
		updateEntity.calendarAccessService = CalendarAccessServiceName.OFFICE_365;

		// 保存項目の値をセット
		CompanyEntity comEntity = new CompanyEntity();
		comEntity.setId(testCompany.Id);
		comEntity.useCalendarAccess = true;
		comEntity.calendarAccessService = null;
//	comEntity.calendarAccessService = 'Gsuite';

		// 実行
		service.clearPrivateSetting(comEntity, updateEntity);

		// 検証
		CompanyPrivateSettingEntity cpsEntityAfter = repository.getEntity(testCompany.Id);
		System.assert(cpsEntityAfter.cryptoKey == null);
		System.assert(cpsEntityAfter.office365AuthCode == null);
	}

	/*
	 * clearPrivateSettingのテスト
	 * 認証情報（暗号化キー、Office365API認証キー）がクリアされないことを検証する
	 */
	@isTest
	static void clearPrivateSettingTestNotClear() {

		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();
		PlannerSettingService service = new PlannerSettingService();
		CompanyRepository repo = new CompanyRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		CompanyPrivateSettingEntity cpsEntityBefore = repository.getEntity(testCompany.Id);
		cpsEntityBefore.cryptoKey = 'testCryptoKey';
		cpsEntityBefore.office365AuthCode = 'testOffice365AuthCode';
		repository.saveEntity(cpsEntityBefore);

		// 更新対象のエンティティに値をセット
		CompanyEntity updateEntity = repo.getEntity(testCompany.Id);
		updateEntity.useCalendarAccess = true;
		updateEntity.calendarAccessService = CalendarAccessServiceName.OFFICE_365;

		// 保存項目の値をセット
		CompanyEntity comEntity = new CompanyEntity();
		comEntity.setId(testCompany.Id);
		comEntity.useCalendarAccess = true;
		comEntity.calendarAccessService = CalendarAccessServiceName.OFFICE_365;

		// 実行
		service.clearPrivateSetting(comEntity, updateEntity);

		// 検証
		CompanyPrivateSettingEntity cpsEntityAfter = repository.getEntity(testCompany.Id);
		System.assertEquals('testCryptoKey', cpsEntityAfter.cryptoKey);
		System.assertEquals('testOffice365AuthCode', cpsEntityAfter.office365AuthCode);
	}

	/*
	 * checkAuthのテスト
	 * 認証状態を正しく確認できることを確認
	 */
	@isTest
	static void checkAuthTestAuthorized() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		CompanyPrivateSettingEntity entity = repository.getEntity(testCompany.Id);
		entity.office365AuthCode = 'AuthCode';
		repository.saveEntity(entity);

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'body' => '{"value":[]}',
				'status' => 'OK',
				'statuscode' => '200'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		String result = service.checkAuth(testCompany.Id);
		Test.stopTest();

		// 検証
		System.assertEquals('AUTHORIZED', result);
	}

	/*
	 * checkAuthのテスト
	 * 認証コードが存在しない場合に未認証（認証失敗）が返却されることを確認
	 */
	@isTest
	static void checkAuthTestUnAuthorized() {

		PlannerSettingService service = new PlannerSettingService();
		CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();
		CompanyPrivateSettingEntity entity = repository.getEntity(testCompany.Id);
		System.assert(entity.office365AuthCode == null);

		// 実行
		String result = service.checkAuth(testCompany.Id);

		// 検証
		System.assertEquals('UNAUTHORIZED', result);
	}

	/*
	 * generateOAuthUrlのテスト
	 * 想定通りの認証URLが返却されることを確認
	 * ステート情報は他テストで確認済みのため、検証対象外とする
	 */
	@isTest
	static void generateOAuthUrlTestSuccess() {

		PlannerSettingService service = new PlannerSettingService();

		// データセット
		ComCompany__c testCompany = new TestData().createTestData();

		// 実行
		String resultUrl = service.generateOAuthUrl(testCompany.Id);
		System.debug(resultUrl);

		// 検証
		System.assertEquals('https://login.microsoftonline.com/common/adminconsent?client_id=eee03ef3-dbef-44e8-8357-006a5e7b07f0&redirect_uri=https%3A%2F%2Foauth.teamspirit.co.jp%2Fwsp%2FOffice365OAuthRedirectPage.html&state=', resultUrl.substring(0, 197));
	}

	/*
	 * generateOAuthUrlのテスト
	 * 会社別非公開設定（カスタム設定）エンティティを取得できずに例外が発生することを検証
	 */
	@isTest
	static void generateOAuthUrlTestPrivateSettingNull() {

		PlannerSettingService service = new PlannerSettingService();

		// データセット
		ComCompany__c testCompany = ComTestDataUtility.createCompany('testCompany', ComTestDataUtility.createCountry('ja').Id);

		// 実行
		try {
			String resultUrl = service.generateOAuthUrl(testCompany.Id);
			System.assert(false);
		} catch (App.RecordNotFoundException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, e.getMessage());
		}
	}
}