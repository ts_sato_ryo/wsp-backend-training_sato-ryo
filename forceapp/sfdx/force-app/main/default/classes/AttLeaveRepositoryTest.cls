/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 休暇マスタのリポジトリテストクラス
 */
@isTest
private class AttLeaveRepositoryTest {

	@isTest static void getLeaveTest() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		Test.startTest();
		List<AttLeaveEntity> leaveDataList = leaveRep.getLeaveListByCompanyId(testData.company.id);
		Test.stopTest();

		System.assertEquals(2, leaveDataList.size());
		for (AttLeaveEntity leaveData : leaveDataList) {
			AttLeaveEntity targetLeave = leaveData.id == leave1.id ? leave1 : leave2;

			System.assertEquals(targetLeave.code, leaveData.code);
			System.assertEquals(targetLeave.uniqKey, leaveData.uniqKey);
			System.assertEquals(targetLeave.name, leaveData.name);
			System.assertEquals(targetLeave.nameL.valueL0, leaveData.nameL.valueL0);
			System.assertEquals(targetLeave.nameL.valueL1, leaveData.nameL.valueL1);
			System.assertEquals(targetLeave.nameL.valueL2, leaveData.nameL.valueL2);
			System.assertEquals(targetLeave.leaveType, leaveData.leaveType);
			System.assertEquals(targetLeave.daysManaged, leaveData.daysManaged);
			System.assertEquals(true, AttLeaveRange.equals(targetLeave.leaveRanges, leaveData.leaveRanges));
			System.assertEquals(targetLeave.validFrom, leaveData.validFrom);
			System.assertEquals(targetLeave.validTo, leaveData.validTo);
		}
	}
	@isTest static void searchLeaveTest() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.SearchFilter searchParam = new AttLeaveRepository.SearchFilter();
		searchParam.leaveType = AttLeaveType.PAID;
		searchParam.companyId = testData.company.id;
		Test.startTest();
		List<AttLeaveEntity> leaveDataList = leaveRep.searchEntityList(searchParam);
		Test.stopTest();

		System.assertEquals(1, leaveDataList.size());
		System.assertEquals(leave1.id, leaveDataList[0].id);
	}

	/**
	 * searchLeaveTestById
	 * idによるフィルタが正しく行われる事を確認。
	 */
	@isTest static void searchLeaveTestById() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});
		
		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.LeaveFilter searchParam = new AttLeaveRepository.LeaveFilter();
		searchParam.leaveIds = new Set<Id>{ leave1.id };

		Test.startTest();
		List<AttLeaveEntity> leaveDataList = leaveRep.searchEntityList(searchParam, false);
		Test.stopTest();

		// leave1のデータが取得できる事を確認
		System.assertEquals(1, leaveDataList.size());
		System.assertEquals(leave1.id, leaveDataList[0].id);
	}

	/**
	 * searchLeaveTestByCompanyId
	 * companyIdによるフィルタが正しく行われる事を確認。
	 */
	@isTest static void searchLeaveTestByCompanyId() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.LeaveFilter searchParam = new AttLeaveRepository.LeaveFilter();

		Test.startTest();
		//leaveDataList1 に2件取得ケースを記述
		searchParam.companyId = testData.company.id;
		List<AttLeaveEntity> leaveDataList1 = leaveRep.searchEntityList(searchParam, false);

		//leaveDataList2 に0件取得ケースを記述
		searchParam.companyId = UserInfo.getUserId();
		List<AttLeaveEntity> leaveDataList2 = leaveRep.searchEntityList(searchParam, false);
		Test.stopTest();

		// 2件のデータが取得できる事を確認
		System.assertEquals(2, leaveDataList1.size());
		// データが取得できない事を確認
		System.assertEquals(0, leaveDataList2.size());
	}

	/**
	 * searchLeaveTestByCodes
	 * codesによるフィルタが正しく行われる事を確認。
	 */
	@isTest static void searchLeaveTestByCodes() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});
		AttLeaveEntity leave3 = testData.createLeave('c', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.LeaveFilter searchParam = new AttLeaveRepository.LeaveFilter();
		searchParam.codes = new List<String> {leave1.code, leave2.code};
		Test.startTest();
		List<AttLeaveEntity> leaveDataList = leaveRep.searchEntityList(searchParam, false);
		Test.stopTest();

		// 2件のデータが取得できる事を確認
		System.assertEquals(2, leaveDataList.size());

		// leave1とleave2が検索された事を確認(ORDER指定があるので順番は固定になっているはず。)
		System.assertEquals(leave1.id, leaveDataList[0].id);
		System.assertEquals(leave2.id, leaveDataList[1].id);
	}

	/**
	 * searchLeaveTestByDaysManaged
	 * daysManagedによるフィルタが正しく行われる事を確認。
	 */
	@isTest static void searchLeaveTestByDaysManaged() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});
		AttLeaveEntity leave3 = testData.createLeave('c', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.LeaveFilter searchParam = new AttLeaveRepository.LeaveFilter();
		searchParam.daysManaged = false;
		Test.startTest();
		List<AttLeaveEntity> leaveDataList = leaveRep.searchEntityList(searchParam, false);
		Test.stopTest();

		// daysManaged=falseのみのデータが取得できる事を確認
		System.assertEquals(1, leaveDataList.size());
		System.assertEquals(leave1.id, leaveDataList[0].id);
	}

	/**
	 * searchLeaveTestByLeaveType
	 * LeaveType(休暇タイプ)によるフィルタが正しく行われる事を確認。
	 */
	@isTest static void searchLeaveTestByLeaveType() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});
		AttLeaveEntity leave3 = testData.createLeave('c', '有休', AttLeaveType.UNPAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.LeaveFilter searchParam = new AttLeaveRepository.LeaveFilter();
		searchParam.leaveType = AttLeaveType.PAID;
		Test.startTest();
		List<AttLeaveEntity> leaveDataList = leaveRep.searchEntityList(searchParam, false);
		Test.stopTest();

		// PAIDのみのデータが取得できる事を確認
		System.assertEquals(1, leaveDataList.size());
		System.assertEquals(leave1.id, leaveDataList[0].id);
	}

	/**
	 * searchLeaveTestByExceptLeaveType
	 * exceptLeaveType(検索対象外の休暇タイプ)によるフィルタが正しく行われる事を確認。
	 */
	@isTest static void searchLeaveTestByExceptLeaveType() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});
		AttLeaveEntity leave3 = testData.createLeave('c', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		AttLeaveRepository.LeaveFilter searchParam = new AttLeaveRepository.LeaveFilter();
		searchParam.exceptLeaveType = AttLeaveType.PAID;
		Test.startTest();
		List<AttLeaveEntity> leaveDataList = leaveRep.searchEntityList(searchParam, false);
		Test.stopTest();

		// UNPAIDのみのデータが取得できる事を確認
		System.assertEquals(1, leaveDataList.size());
		System.assertEquals(leave2.id, leaveDataList[0].id);
	}

	/**
	 * エンティティ取得のテスト
	 * 指定した対象日に有効な履歴が取得できることを確認する
	 */
	@isTest static void getLeaveByCodesTestValidDate() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leave1 = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});
		AttLeaveEntity leave2 = testData.createLeave('b', '無休', AttLeaveType.UNPAID, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});

		AttLeave__c leaveObj1 = [SELECT Id, ValidFrom__c, ValidTo__c FROM AttLeave__c WHERE Id = :leave1.id];
		AttLeave__c leaveObj2 = [SELECT Id, ValidFrom__c, ValidTo__c FROM AttLeave__c WHERE Id = :leave2.id];
		leaveObj1.ValidFrom__c = Date.today();
		leaveObj1.ValidTo__c = leaveObj1.ValidFrom__c.addMonths(1);
		leaveObj2.ValidFrom__c = leaveObj1.ValidTo__c;
		leaveObj2.ValidTo__c = leaveObj2.ValidFrom__c.addMonths(1);
		update new List<AttLeave__c>{leaveObj1, leaveObj2};

		AttLeaveRepository repo = new AttLeaveRepository();
		List<AttLeaveEntity> resLeaves;
		List<String> codes = new List<String> {leave1.code, leave2.code};
		Id companyId = testData.company.id;
		AppDate targetDate;

		// 取得対象日に履歴の有効期間開始日を指定
		targetDate = AppDate.valueOf(leaveObj2.ValidFrom__c);
		resLeaves = repo.getLeaveByCodes(codes, companyId, targetDate);
		System.assertEquals(1, resLeaves.size());
		System.assertEquals(leave2.Id, resLeaves[0].id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		targetDate = AppDate.valueOf(leaveObj2.ValidTo__c.addDays(-1));
		resLeaves = repo.getLeaveByCodes(codes, companyId, targetDate);
		System.assertEquals(1, resLeaves.size());
		System.assertEquals(leave2.Id, resLeaves[0].id);

		// 取得対象日に失効日を指定
		// 取得できないはず
		targetDate = AppDate.valueOf(leaveObj2.ValidTo__c);
		resLeaves = repo.getLeaveByCodes(codes, companyId, targetDate);
		System.assertEquals(0, resLeaves.size());

	}

	@isTest static void saveLeaveTest() {
		AttTestData testData = new AttTestData();

		AttLeaveEntity newLeaveData = new AttLeaveEntity();
		newLeaveData.companyId = testData.company.id;
		newLeaveData.code = 'code';
		newLeaveData.uniqKey = testData.company.Code__c + '-' + newLeaveData.code;
		newLeaveData.name = 'name';
		newLeaveData.nameL0 = 'namel0';
		newLeaveData.nameL1 = 'namel1';
		newLeaveData.nameL2 = 'namel2';
		newLeaveData.leaveType = AttLeaveType.PAID;
		newLeaveData.daysManaged = true;
		newLeaveData.leaveRanges = new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF};
		newLeaveData.validFrom = AppDate.today();
		newLeaveData.validTo = AppDate.today().addDays(10);
		newLeaveData.requireReason = true;

		AttLeaveRepository leaveRep = new AttLeaveRepository();
		Test.startTest();
		Repository.SaveResult result = leaveRep.saveEntity(newLeaveData);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);
		System.assertEquals(1, result.details.size());
		System.assertNotEquals(null, result.details[0].id);
		Id leaveId = result.details[0].id;

		AttLeave__c attLeave =
				[SELECT CompanyId__c, Code__c, Name, NAME_L0__c, NAME_L1__c, Name_L2__c,
					Type__c, DaysManaged__c, Range__c, ValidFrom__c, ValidTo__c, RequireReason__c
				FROM AttLeave__c
				WHERE Id = :leaveId];

		System.assertEquals(newLeaveData.code, attLeave.Code__c);
		System.assertEquals(newLeaveData.name, attLeave.Name);
		System.assertEquals(newLeaveData.nameL.valueL0, attLeave.Name_L0__c);
		System.assertEquals(newLeaveData.nameL.valueL1, attLeave.Name_L1__c);
		System.assertEquals(newLeaveData.nameL.valueL2, attLeave.Name_L2__c);
		System.assertEquals(AppConverter.stringValue(newLeaveData.leaveType), attLeave.Type__c);
		System.assertEquals(newLeaveData.daysManaged, attLeave.DaysManaged__c);
		System.assertEquals(true, AttLeaveRange.equals(newLeaveData.leaveRanges, AttLeaveRange.valuesOf(attLeave.Range__c)));
		System.assertEquals(newLeaveData.requireReason, attLeave.RequireReason__c);
		System.assertEquals(newLeaveData.validFrom.getDate(), attLeave.ValidFrom__c);
		System.assertEquals(newLeaveData.validTo.getDate(), attLeave.ValidTo__c);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leaveEntity = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		// キャッシュをonでインスタンスを生成
		AttLeaveRepository repository = new AttLeaveRepository(true);

		// 検索（DBから取得）
		AttLeaveRepository.LeaveFilter filter = new AttLeaveRepository.LeaveFilter();
		filter.leaveIds = new Set<Id>{leaveEntity.id};
		AttLeaveEntity leave = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveEntity.code = 'NotCache';
		new AttLeaveRepository().saveEntity(leaveEntity);

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveEntity cachedLeave = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(leave.id, cachedLeave.id);
		System.assertNotEquals('NotCache', cachedLeave.code);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leaveEntity = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		// 検索（DBから取得）
		AttLeaveRepository.LeaveFilter filter = new AttLeaveRepository.LeaveFilter();
		filter.leaveIds = new Set<Id>{leaveEntity.id};
		AttLeaveEntity leave = new AttLeaveRepository(true).searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveEntity.code = 'NotCache';
		new AttLeaveRepository().saveEntity(leaveEntity);

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveEntity cachedLeave = new AttLeaveRepository(true).searchEntityList(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(leave.id, cachedLeave.id);
		System.assertNotEquals('NotCache', cachedLeave.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheDifferentCondition() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leaveEntity = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		// キャッシュをonでインスタンスを生成
		AttLeaveRepository repository = new AttLeaveRepository(true);

		// 検索（DBから取得）
		AttLeaveRepository.LeaveFilter filter = new AttLeaveRepository.LeaveFilter();
		filter.leaveIds = new Set<Id>{leaveEntity.id};
		AttLeaveEntity leave = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveEntity.code = 'NotCache';
		new AttLeaveRepository().saveEntity(leaveEntity);

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.leaveIds.add(testData.company.Id); // 検索結果に該当しないIDを追加する
		AttLeaveEntity cachedLeave = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(leave.id, cachedLeave.id);
		System.assertEquals('NotCache', cachedLeave.code);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leaveEntity = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		// キャッシュをonでインスタンスを生成
		AttLeaveRepository repository = new AttLeaveRepository(true);

		// 検索（DBから取得）
		AttLeaveRepository.LeaveFilter filter = new AttLeaveRepository.LeaveFilter();
		filter.leaveIds = new Set<Id>{leaveEntity.id};
		AttLeaveEntity leave = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveEntity.code = 'NotCache';
		new AttLeaveRepository().saveEntity(leaveEntity);

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveEntity cachedLeave = repository.searchEntityList(filter, true)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(leave.id, cachedLeave.id);
		System.assertEquals('NotCache', cachedLeave.code);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();
		AttLeaveEntity leaveEntity = testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF});

		// キャッシュをoffでインスタンスを生成
		AttLeaveRepository repository = new AttLeaveRepository();

		// 検索（DBから取得）
		AttLeaveRepository.LeaveFilter filter = new AttLeaveRepository.LeaveFilter();
		filter.leaveIds = new Set<Id>{leaveEntity.id};
		AttLeaveEntity leave = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		leaveEntity.code = 'NotCache';
		new AttLeaveRepository().saveEntity(leaveEntity);

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttLeaveEntity cachedLeave = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(leave.id, cachedLeave.id);
		System.assertEquals('NotCache', cachedLeave.code);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		AttLeaveRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new AttLeaveRepository().isEnabledCache);
	}
}
