/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * マスタ管理系APIの権限チェック機能を提供するクラス
 * マスタ管理系のResourceクラス以外からは呼び出さないでください
 */
public with sharing class ComResourcePermission {

	/** API実行に必要な権限 */
	public enum Permission {
		/** 全体設定の管理 */
		MANAGE_OVERALL_SETTING,
		/** 部署の管理 */
		MANAGE_DEPARTMENT,
		/** 社員の管理 */
		MANAGE_EMPLOYEE,
		/** カレンダーの管理 */
		MANAGE_CALENDAR,
		/** ジョブタイプの管理 */
		MANAGE_JOB_TYPE,
		/** ジョブの管理 */
		MANAGE_JOB,
		/** モバイル機能設定の管理 */
		MANAGE_MOBILE_SETTING,
		/** アクセス権限設定の管理 */
		MANAGE_PERMISSION,
		/** management permission of accounting period */
		MANAGE_EXP_ACCOUNTING_PERIOD,
		/** management permission of cost center */
		MANAGE_EXP_COST_CENTER,
		/** management permission of exchange rate */
		MANAGE_EXP_EXCHANGE_RATE,
		/** management permission of extended item */
		MANAGE_EXP_EXTENDED_ITEM,
		/** management permission of report type */
		MANAGE_EXP_REPORT_TYPE,
		/** management permission of expense setting*/
		MANAGE_EXP_SETTING,
		/** management permission of tax type */
		MANAGE_EXP_TAX_TYPE,
		/** management permission of expense type group */
		MANAGE_EXP_TYPE_GROUP,
		/** management permission of expense type */
		MANAGE_EXP_TYPE,
		/** management permission of vendor */
		MANAGE_EXP_VENDOR,
		/** management permission of employee group */
		MANAGE_EXP_EMPLOYEE_GROUP,
		/** management permission of custom hint */
		MANAGE_EXP_CUSTOM_HINT
	}

	/** 社員の権限サービス */
	private EmployeePermissionService permissionService;

	/**
	 * コンストラクタ
	 */
	public ComResourcePermission() {
		this.permissionService = new EmployeePermissionService();
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(ComResourcePermission.Permission requiredPermission) {
		hasExecutePermission(new List<ComResourcePermission.Permission>{requiredPermission});
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasExecutePermission(List<ComResourcePermission.Permission> requiredPermissionList) {
		List<ComResourcePermission.Permission> errorPermissionList =
				new List<ComResourcePermission.Permission>();

		for (Permission requiredPermission : requiredPermissionList) {
			// 全体設定の管理
			if (requiredPermission == Permission.MANAGE_OVERALL_SETTING) {
				if (! permissionService.hasManageOverallSetting) {
					errorPermissionList.add(Permission.MANAGE_OVERALL_SETTING);
				}
			// 部署の管理
			} else if (requiredPermission == Permission.MANAGE_DEPARTMENT) {
				if (! permissionService.hasManageDepartment) {
					errorPermissionList.add(Permission.MANAGE_DEPARTMENT);
				}
			// 社員の管理
			} else if (requiredPermission == Permission.MANAGE_EMPLOYEE) {
				if (! permissionService.hasManageEmployee) {
					errorPermissionList.add(Permission.MANAGE_EMPLOYEE);
				}
			// カレンダーの管理
			} else if (requiredPermission == Permission.MANAGE_CALENDAR) {
				if (! permissionService.hasManageCalendar) {
					errorPermissionList.add(Permission.MANAGE_CALENDAR);
				}
			// ジョブタイプの管理
			} else if (requiredPermission == Permission.MANAGE_JOB_TYPE) {
				if (! permissionService.hasManageJobType) {
					errorPermissionList.add(Permission.MANAGE_JOB_TYPE);
				}
			// ジョブの管理
			} else if (requiredPermission == Permission.MANAGE_JOB) {
				if (! permissionService.hasManageJob) {
					errorPermissionList.add(Permission.MANAGE_JOB);
				}
			// モバイル機能設定の管理
			} else if (requiredPermission == Permission.MANAGE_MOBILE_SETTING) {
				if (! permissionService.hasManageMobileSetting) {
					errorPermissionList.add(Permission.MANAGE_MOBILE_SETTING);
				}
			// アクセス権限設定の管理
			} else if (requiredPermission == Permission.MANAGE_PERMISSION) {
				if (! permissionService.hasManagePermission) {
					errorPermissionList.add(Permission.MANAGE_PERMISSION);
				}

			// management setting of accounting period
			} else if (requiredPermission == Permission.MANAGE_EXP_ACCOUNTING_PERIOD) {
				if (! permissionService.hasManageAccountingPeriod) {
					errorPermissionList.add(Permission.MANAGE_EXP_ACCOUNTING_PERIOD);
				}
			// management setting of cost center
			} else if (requiredPermission == Permission.MANAGE_EXP_COST_CENTER) {
				if (! permissionService.hasManageCostCenter) {
					errorPermissionList.add(Permission.MANAGE_EXP_COST_CENTER);
				}
			// management setting of exchange rate
			} else if (requiredPermission == Permission.MANAGE_EXP_EXCHANGE_RATE) {
				if (! permissionService.hasManageExchangeRate) {
					errorPermissionList.add(Permission.MANAGE_EXP_EXCHANGE_RATE);
				}
			// management setting of extended item
			} else if (requiredPermission == Permission.MANAGE_EXP_EXTENDED_ITEM) {
				if (! permissionService.hasManageExtendedItem) {
					errorPermissionList.add(Permission.MANAGE_EXP_EXTENDED_ITEM);
				}
			// management setting of report type
			} else if (requiredPermission == Permission.MANAGE_EXP_REPORT_TYPE) {
				if (! permissionService.hasManageReportType) {
					errorPermissionList.add(Permission.MANAGE_EXP_REPORT_TYPE);
				}
			// management setting of expense setting
			} else if (requiredPermission == Permission.MANAGE_EXP_SETTING) {
				if (! permissionService.hasManageExpSetting) {
					errorPermissionList.add(Permission.MANAGE_EXP_SETTING);
				}
			// management setting of expense tax type
			} else if (requiredPermission == Permission.MANAGE_EXP_TAX_TYPE) {
				if (! permissionService.hasManageTaxType) {
					errorPermissionList.add(Permission.MANAGE_EXP_TAX_TYPE);
				}
			// management setting of expense type group
			} else if (requiredPermission == Permission.MANAGE_EXP_TYPE_GROUP) {
				if (! permissionService.hasManageExpTypeGroup) {
					errorPermissionList.add(Permission.MANAGE_EXP_TYPE_GROUP);
				}
			// management setting of expense type
			} else if (requiredPermission == Permission.MANAGE_EXP_TYPE) {
				if (! permissionService.hasManageExpenseType) {
					errorPermissionList.add(Permission.MANAGE_EXP_TYPE);
				}
			// management setting of vendor
			} else if (requiredPermission == Permission.MANAGE_EXP_VENDOR) {
				if (! permissionService.hasManageVendor) {
					errorPermissionList.add(Permission.MANAGE_EXP_VENDOR);
				}
			// management setting of employee group
			} else if (requiredPermission == Permission.MANAGE_EXP_EMPLOYEE_GROUP) {
				if (! permissionService.hasManageEmployeeGroup) {
					errorPermissionList.add(Permission.MANAGE_EXP_EMPLOYEE_GROUP);
				}
			// management setting of custom hint
			} else if (requiredPermission == Permission.MANAGE_EXP_CUSTOM_HINT) {
				if (! permissionService.hasManageExpCustomHint) {
					errorPermissionList.add(Permission.MANAGE_EXP_CUSTOM_HINT);
				}
			}
			// Error! 実装ミス
			else {
				throw new App.UnsupportedException('Unsupported Permission(' + requiredPermission + ')');
			}
		}

		if (! errorPermissionList.isEmpty()) {
			// デバッグ用にログを出力しておく
			System.debug('--- Insufficient permission for API=' + errorPermissionList);
			throw new App.NoPermissionException(ComMessage.msg().Com_Err_NoApiPermission);
		}
	}
}