/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 権限に関するサービスを提供するクラス
 */
@isTest
private class PermissionServiceTest {
	/**
	 * @description getPermissionListのテスト
	 * 指定した会社が存在しない場合に例外が発生することを確認する
	 */
	@isTest static void getPermissionListNotFoundCompanyTest() {
		PermissionService testService = new PermissionService();
		// テストデータ作成
		// - 会社の作成
		List<ComCompany__c> companies = ComTestDataUtility.createCompanies('test', null, 2);
		ComCompany__c deleteTarget = companies.get(0);
		delete deleteTarget;
		try {
			testService.getPermissionList(null, deleteTarget.id);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Company}), e.getMessage());
		}
	}

	/**
	 * @description getPermissionListのテスト
	 * 引数で正しく検索できることを確認する
	 */
	@isTest static void getPermissionListTest() {
		PermissionService testService = new PermissionService();
		// テストデータ作成
		// - 会社の作成
		List<ComCompany__c> companies = ComTestDataUtility.createCompanies('test', null, 2);
		ComCompany__c company = companies.get(0);
		ComCompany__c anotherCompany = companies.get(1);
		List<ComPermission__c> permissionList = new List<ComPermission__c>();
		permissionList.addAll(ComTestDataUtility.createPermissions('ABC', company.id, 2));//並び順検証のため
		permissionList.addAll(ComTestDataUtility.createPermissions('BCD', company.id, 2));
		permissionList.addAll(ComTestDataUtility.createPermissions('ACF', anotherCompany.id, 2));
		// - 論理削除レコードの作成
		ComPermission__c removedPermission = permissionList.get(3);
		removedPermission.Removed__c = true;
		update removedPermission;
		permissionList.set(3, removedPermission);

		// 会社レコードIDで検索できることを確認する
		// 実行
		List<PermissionEntity> actCpmpanyList;
		try {
			actCpmpanyList = testService.getPermissionList(null, company.id);
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました');
		}
		//検証
		System.assertEquals(3, actCpmpanyList.size());
		assertPermissionEntity(permissionList.get(0), actCpmpanyList.get(0));
		assertPermissionEntity(permissionList.get(1), actCpmpanyList.get(1));
		assertPermissionEntity(permissionList.get(2), actCpmpanyList.get(2));

		// 権限レコードIDで検索できることを確認する
		// 実行
		List<PermissionEntity> actIdList;
		List<Id> filterIdList = new List<Id>{permissionList.get(0).id, permissionList.get(5).id};
		try {
			actIdList = testService.getPermissionList(filterIdList, null);
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました');
		}
		//検証
		System.assertEquals(2, actIdList.size());
		assertPermissionEntity(permissionList.get(0), actIdList.get(0));
		assertPermissionEntity(permissionList.get(5), actIdList.get(1));
	}

	/**
	 * @description savePermissionのテスト
	 * 正常値の引数で正しく新規作成できることを確認する
	 * 正常値の引数で正しく更新できることを確認する
	 */
	@isTest static void savePermissionTest() {
		PermissionService testService = new PermissionService();
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		// - 更新対象権限の作成
		PermissionEntity saveTarget = createDefaultPermission('SaveCode', 'Save Permission Name', testData.company);
		saveTarget.isSwitchCompany = true;

		// 正常値の引数で正しく新規作成できることを確認する
		// 実行
		Id resultId;
		try {
			resultId = testService.savePermission(saveTarget);
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました' + e.getMessage() + e.getStackTraceString());
		}
		//検証
		PermissionEntity actualPermissionEntity = new PermissionRepository().getEntityById(resultId);
		assertPermissionEntity(saveTarget, actualPermissionEntity);

		// 正常値の引数で正しく更新できることを確認する
		// テストデータ更新
		saveTarget.setId(resultId);
		saveTarget.isSwitchCompany = false;
		// 実行
		try {
			resultId = testService.savePermission(saveTarget);
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました' + e.getMessage() + e.getStackTraceString());
		}
		//検証
		System.assertEquals(saveTarget.id, resultId);
		actualPermissionEntity = new PermissionRepository().getEntityById(resultId);
		assertPermissionEntity(saveTarget, actualPermissionEntity);
	}

	/**
	 * @description savePermissionのテスト
	 * 最低限項目がセットされた状態で値が正しくセットされていることを確認する
	 */
	@isTest static void savePermissionTestNegative() {
		PermissionService testService = new PermissionService();
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		PermissionEntity newPermission = createDefaultPermission('svtgt001', 'Save Test Negative Name', testData.company);

		// 正常値の引数で正しく新規作成できることを確認する
		// 実行
		Id resultId;
		try {
			resultId = testService.savePermission(newPermission);
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました' + e.getMessage() + e.getStackTraceString());
		}
		//検証
		PermissionEntity actualPermissionEntity = new PermissionRepository().getEntityById(resultId);
		assertPermissionEntity(newPermission, actualPermissionEntity);
	}

	/**
	 * @description savePermissionのテスト
	 * 重複するコードがある場合にエラーが発生することを確認する
	 */
	@isTest static void savePermissionTestDuplicateCodeError() {
		PermissionService testService = new PermissionService();
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		List<ComPermission__c> permissionList = new List<ComPermission__c>();
		permissionList.add(createDefaultPermission('svtgt002', 'Save Test Duplicate Name', testData.company).createSObject());
		permissionList.add(createDefaultPermission('svtgt003', 'Save Test Duplicate Name', testData.company).createSObject());
		insert permissionList;

		PermissionEntity newCreatePermission = createDefaultPermission('svtgt002', 'Save Test Name', testData.company);

		// 新規作成の場合
		// 実行
		App.ParameterException actEx;
		Id resultId;
		try {
			resultId = testService.savePermission(newCreatePermission);
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました' + e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, actEx.getMessage());

		// 更新の場合
		// 更新データの作成
		PermissionEntity updatePermission = new PermissionEntity(permissionList.get(1));
		updatePermission.code = 'svtgt002';
		// 実行
		try {
			resultId = testService.savePermission(updatePermission);
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました' + e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, actEx.getMessage());
	}

	/**
	 * @description savePermissionのテスト
	 * 指定した会社が存在しない場合に例外が発生することを確認する
	 */
	@isTest static void savePermissionNotFoundCompanyTest() {
		PermissionService testService = new PermissionService();
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		CompanyEntity deleteCompany = testData.createCompany('CmpNotFound');
		ComPermission__c saveTarget = createDefaultPermission('svtgt002', 'Save Test Company Not Found Name', deleteCompany).createSObject();
		delete new ComCompany__c(id = deleteCompany.id);

		App.RecordNotFoundException actEx;
		// 実行
		try {
			testService.savePermission(new PermissionEntity(saveTarget));
		} catch (App.RecordNotFoundException e) {
			actEx = e;
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました' + e.getMessage() + e.getStackTraceString());
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.RecordNotFoundExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_RECORD_NOT_FOUND, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Company}), actEx.getMessage());
	}

	/**
	 * 検索で取得した権限の各項目の値が期待値に一致することを確認する
	 */
	private static void assertPermissionEntity(PermissionEntity expEntity, PermissionEntity actEntity) {
		ComPermission__c expObj = expEntity.createSObject();
		assertPermissionEntity(expObj, actEntity);
	}

	/**
	 * 権限を作成する(必須項目のみセット)
	 */
	private static PermissionEntity createDefaultPermission(String code, String name, CompanyEntity company) {
		PermissionEntity permission = new PermissionEntity();
		permission.companyId = company.Id;
		permission.name = name;
		permission.nameL0 = name;
		permission.code = code;
		permission.uniqKey = company.code + '-' + code;
		return permission;
	}

	/**
	 * 検索で取得した権限の各項目の値が期待値に一致することを確認する
	 */
	private static void assertPermissionEntity(ComPermission__c expObj, PermissionEntity actEntity) {
		if (actEntity == null && expObj != null) {
			TestUtil.fail('該当するデータがありません');
		}
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);

		// 勤怠トランザクション系権限の検証
		System.assertEquals(expObj.ViewAttTimeSheetByDelegate__c, actEntity.isViewAttTimeSheetByDelegate);
		System.assertEquals(expObj.EditAttTimeSheetByDelegate__c, actEntity.isEditAttTimeSheetByDelegate);

		// 勤怠申請系
		System.assertEquals(expObj.CancelAttApprovalByDelegate__c, actEntity.isCancelAttApprovalByDelegate);
		System.assertEquals(expObj.CancelAttApprovalByEmployee__c, actEntity.isCancelAttApprovalByEmployee);
		System.assertEquals(expObj.CancelAttDailyApprovalByDelegate__c, actEntity.isCancelAttDailyApprovalByDelegate);
		System.assertEquals(expObj.CancelAttDailyApprovalByEmployee__c, actEntity.isCancelAttDailyApprovalByEmployee);
		System.assertEquals(expObj.CancelAttDailyRequestByDelegate__c, actEntity.isCancelAttDailyRequestByDelegate);
		System.assertEquals(expObj.CancelAttRequestByDelegate__c, actEntity.isCancelAttRequestByDelegate);
		System.assertEquals(expObj.SubmitAttDailyRequestByDelegate__c, actEntity.isSubmitAttDailyRequestByDelegate);
		System.assertEquals(expObj.SubmitAttRequestByDelegate__c, actEntity.isSubmitAttRequestByDelegate);

		// 管理系権限の検証
		System.assertEquals(expObj.ManageAttAgreementAlertSetting__c, actEntity.isManageAttAgreementAlertSetting);
		System.assertEquals(expObj.ManageAttLeave__c, actEntity.isManageAttLeave);
		System.assertEquals(expObj.ManageAttLeaveGrant__c, actEntity.isManageAttLeaveGrant);
		System.assertEquals(expObj.ManageAttLeaveOfAbsence__c, actEntity.isManageAttLeaveOfAbsence);
		System.assertEquals(expObj.ManageAttLeaveOfAbsenceApply__c, actEntity.isManageAttLeaveOfAbsenceApply);
		System.assertEquals(expObj.ManageAttShortTimeWorkSetting__c, actEntity.isManageAttShortTimeWorkSetting);
		System.assertEquals(expObj.ManageAttShortTimeWorkSettingApply__c, actEntity.isManageAttShortTimeWorkSettingApply);
		System.assertEquals(expObj.ManageAttWorkingType__c, actEntity.isManageAttWorkingType);
		System.assertEquals(expObj.ManageCalendar__c, actEntity.isManageCalendar);
		System.assertEquals(expObj.ManageDepartment__c, actEntity.isManageDepartment);
		System.assertEquals(expObj.ManageEmployee__c, actEntity.isManageEmployee);
		System.assertEquals(expObj.ManageJob__c, actEntity.isManageJob);
		System.assertEquals(expObj.ManageJobType__c, actEntity.isManageJobType);
		System.assertEquals(expObj.ManageOverallSetting__c, actEntity.isManageOverallSetting);
		System.assertEquals(expObj.ManagePermission__c, actEntity.isManagePermission);
		System.assertEquals(expObj.ManageTimeSetting__c, actEntity.isManageTimeSetting);
		System.assertEquals(expObj.ManageTimeWorkCategory__c, actEntity.isManageTimeWorkCategory);
		System.assertEquals(expObj.SwitchCompany__c, actEntity.isSwitchCompany);
		System.assertEquals(expObj.ManageExpCustomHint__c, actEntity.isManageExpCustomHint);
	}
}