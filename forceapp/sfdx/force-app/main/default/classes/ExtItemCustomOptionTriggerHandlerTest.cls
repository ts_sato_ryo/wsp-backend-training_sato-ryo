/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Test class for ExtendedItemCustomOptionTriggerHandler
 *
 * @group Expense
 */
@isTest
private class ExtItemCustomOptionTriggerHandlerTest {

	@TestSetup
	static void setup() {
		TestData.setupMaster();
	}

	/*
	 * Test that records can be updated successfully.
	 */
	@isTest static void insertPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('init', testDataEntity.company.id, 2);
		ComTestDataUtility.createExtendedItemCustomOptions('init', customList, 10);

		List<ComExtendedItemCustomOption__c> newList = new List<ComExtendedItemCustomOption__c> ();

		String name = 'new custom';
		String code = 'new_custom';
		ComExtendedItemCustom__c customA = customList[0];
		ComExtendedItemCustom__c customB = customList[1];
		List<ComExtendedItemCustomOption__c> customAExtendItemCustomOptionList = createExtendedItemCustomOptionSObjects(name, code, customA.id, customA.Code__c, 5);
		List<ComExtendedItemCustomOption__c> customBExtendItemCustomOptionList = createExtendedItemCustomOptionSObjects(name, code, customB.id, customB.Code__c, 5);

		newList.addAll(customAExtendItemCustomOptionList);
		newList.addAll(customBExtendItemCustomOptionList);

		INSERT newList;

		List<ComExtendedItemCustomOption__c> retList = [SELECT Id FROM ComExtendedItemCustomOption__c];
		System.assertEquals(30, retList.size());    // Initially, 10 in each EI Custom. Added 5 more to each EI Custom.
	}

	/*
	 * Test that records can be updated successfully.
	 */
	@isTest static void updatePositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('init', testDataEntity.company.id, 3);
		ComTestDataUtility.createExtendedItemCustomOptions('init', customList, 10);

		ComExtendedItemCustom__c customA = customList[0];
		ComExtendedItemCustom__c customB = customList[1];
		ComExtendedItemCustom__c customC = customList[2];

		// Test that if code is modified and the value is NOT in used, it can be updated.
		// Test that if code is NOT modified, then no error is thrown.
		// Test that even if the Code is the same, if the EI Custom is different, NO error is thrown
		List<ComExtendedItemCustomOption__c> toUpdateList = [SELECT Id, Code__c, Name_L0__c, ExtendedItemCustomId__c FROM ComExtendedItemCustomOption__c];
		for (ComExtendedItemCustomOption__c sObj : toUpdateList) {
			if (sObj.ExtendedItemCustomId__c == customB.id) {
				sObj.Name_L0__c = sObj.Name_L0__c + '-edited';
			} else {
				// For EI Custom A and EI Custom C, the Custom Extended Items Options will use different Code value.
				sObj.Code__c = sObj.Code__c + '-edited';
			}

		}
		UPDATE toUpdateList;

		List<ComExtendedItemCustomOption__c> retList = [SELECT Id, Code__c, Name_L0__c, ExtendedItemCustomId__c FROM ComExtendedItemCustomOption__c];
		for (ComExtendedItemCustomOption__c retSObj : retList) {
			if (retSObj.ExtendedItemCustomId__c == customB.id) {
				System.assertEquals(true, retSObj.Name_L0__c.endsWith('-edited'));
			} else {
				System.assertEquals(true, retSObj.Code__c.endsWith('-edited'));
			}
		}
	}

	/*
	 * Test that if the record to be inserted is using which which are already in used in the same EI Custom, error is thrown.
	 */
	@isTest static void insertAlreadyInUsedNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('init', testDataEntity.company.id, 2);
		ComExtendedItemCustom__c customA = customList[0];
		ComExtendedItemCustom__c customB = customList[1];
		ComTestDataUtility.createExtendedItemCustomOptions('init', new List<ComExtendedItemCustom__c>{customA}, 5);
		List<ComExtendedItemCustomOption__c> customBOptionList = ComTestDataUtility.createExtendedItemCustomOptions('init', new List<ComExtendedItemCustom__c>{customB}, 10);

		// Since the Custom A already has 5 custom extended items option, this will make 5 already in used + 5 New
		for (Integer i = 0; i < customBOptionList.size(); i++) {
			customBOptionList.get(i).Id = null;
			customBOptionList.get(i).ExtendedItemCustomId__c = customA.id;
			// Since unique key is already controlled at SF obj level, not necessary to test it.
			customBOptionList.get(i).UniqKey__c = customBOptionList.get(i).UniqKey__c + '_new_' + i;
		}

		Exception expectedException;
		try {
			INSERT customBOptionList;
		} catch (Exception e) {
			expectedException = e;
		}

		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Exp_Err_DuplicateExtendedItemCustomOptionCode));

		List<ComExtendedItemCustomOption__c> retList = [
				SELECT Id, Code__c, ExtendedItemCustomId__c
				FROM ComExtendedItemCustomOption__c WHERE ExtendedItemCustomId__c = :customA.id
		];
		// There shouldn't have any change
		System.assertEquals(5, retList.size());
	}

	/*
	 * Test that error is thrown
	 *  - if the record to be updated is using which which are already in used in the EI Custom
	 *  - if the record is to be move to another parent, but that parent already has an option with the same code as the child
	 */
	@isTest static void updateAlreadyInUsedNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('init', testDataEntity.company.id, 2);
		ComExtendedItemCustom__c customA = customList[0];
		ComExtendedItemCustom__c customB = customList[1];
		List<ComExtendedItemCustomOption__c> customAOptionList = ComTestDataUtility.createExtendedItemCustomOptions('init', new List<ComExtendedItemCustom__c>{customA}, 5);
		List<ComExtendedItemCustomOption__c> customBOptionList = ComTestDataUtility.createExtendedItemCustomOptions('init', new List<ComExtendedItemCustom__c>{customB}, 10);

		// WHEN THE NEWLY SPECIFIED CODE IS ALREADY USED BY ANOTHER OPTION IN THE SAME CUSTOM => NG
		// Use a code which is already in used in the system.
		Integer targetIndex = 5;
		customBOptionList.get(targetIndex).Code__c = customBOptionList.get(0).Code__c;
		Exception changedCodeInUsedInSameCustomException;
		try {
			UPDATE customBOptionList;
		} catch (Exception e) {
			changedCodeInUsedInSameCustomException = e;
		}
		System.assertNotEquals(null, changedCodeInUsedInSameCustomException);
		System.assertEquals(true, changedCodeInUsedInSameCustomException instanceof DmlException);
		System.assertEquals(true, changedCodeInUsedInSameCustomException.getMessage().contains(ComMessage.msg().Exp_Err_DuplicateExtendedItemCustomOptionCode));

		// WHEN CHANGING THE PARENT CUSTOM, THE CURRENT EXISTING CODE IS ALREADY IN USED IN THE NEW PARENT CUSTOM => NG
		targetIndex = 2;
		customAOptionList.get(targetIndex).ExtendedItemCustomId__c = customB.Id;
		Exception codeAlreadyInUsedInNewParentException;
		try {
			UPDATE customAOptionList;
		} catch (Exception e) {
			codeAlreadyInUsedInNewParentException = e;
		}
		System.assertNotEquals(null, codeAlreadyInUsedInNewParentException);
		System.assertEquals(true, codeAlreadyInUsedInNewParentException instanceof DmlException);
		System.assertEquals(true, codeAlreadyInUsedInNewParentException.getMessage().contains(ComMessage.msg().Exp_Err_DuplicateExtendedItemCustomOptionCode));
	}

	/*
	 * Test that when the records which are to be inserted contains duplicate code, error is thrown.
	 */
	@isTest static void insertDuplicateWithinNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('init', testDataEntity.company.id, 1);


		ComExtendedItemCustom__c customA = customList[0];
		List<ComExtendedItemCustomOption__c> optionList = createExtendedItemCustomOptionSObjects('Test', 'TEST', customA.id, customA.Code__c, 5);

		optionList.get(1).Code__c = optionList.get(4).Code__c;
		optionList.get(2).Code__c = optionList.get(3).Code__c;

		Exception expectedException;
		try {
			INSERT optionList;
		} catch (Exception e) {
			expectedException = e;
		}

		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_DuplicateCode));
	}

	/*
	 * Test that when the records which are to be updated contains duplicate code, error is thrown.
	 */
	@isTest static void updateDuplicateWithinNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('init', testDataEntity.company.id, 1);
		List<ComExtendedItemCustomOption__c> optionList = ComTestDataUtility.createExtendedItemCustomOptions('init', customList, 10);


		// Change all the codes (since it won't validate if the code isn't changed)
		for (ComExtendedItemCustomOption__c sObj: optionList) {
			sObj.Code__c = sObj.Code__c + '-edited';
		}
		// Create Duplicate within the records to be updated
		optionList.get(1).Code__c = optionList.get(4).Code__c;

		Exception expectedException;
		try {
			UPDATE optionList;
		} catch (Exception e) {
			expectedException = e;
		}

		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_DuplicateCode));
	}

	/*
	 * Test that if the Parent Extended Item Custom is already in used
	 * by Report/Request/RecordItem/RequestItem (transactions), then user
	 * cannot modify the Code or change the parent of the Options.
	 */
	@isTest static void updatingAlreadyReferencedInTransactionsTest() {
		ComOrgSetting__c org = [SELECT ID, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];
		ComCountry__c country = [SELECT ID, Name, Name_L0__c, Name_L1__c, Name_L2__c, Code__c FROM ComCountry__c];
		ExpTestData testData = new ExpTestData(org, country);
		//Prepare one option for use in ReParenting Test
		ComExtendedItemCustomOption__c optionForReParentingTest = testData.extendedItemCustomOptionList.get(1);
		optionForReParentingTest.Code__c = 'Unique';
		UPDATE optionForReParentingTest;

		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);
		List<ExpRecordEntity> recordList = testData.createExpRecords(reportList[0].id, Date.today(), testData.expType, testData.employee, 3);

		List<ComExtendedItemCustom__c> floatingCustomList =  ComTestDataUtility.createExtendedItemCustoms('new', testData.company.Id, 1);
		List<ComExtendedItemCustomOption__c> floatingOptionList = ComTestDataUtility.createExtendedItemCustomOptions('nOption', floatingCustomList, 10);
		List<ComExtendedItem__c> floatingExtendedItemList = ComTestDataUtility.createExtendedItemLookups('lk', testData.company.Id, floatingCustomList);

		ExpReportType__c floatingReportType = ComTestDataUtility.createExpReportType('float', testData.company.Id);
		floatingReportType.ExtendedItemLookup01TextId__c = floatingExtendedItemList[0].Id;
		UPDATE floatingReportType;
		ExpType__c floatingExpenseType = ComTestDataUtility.createExpType('floatExp', testData.company.Id);
		floatingExpenseType.ExtendedItemLookup01TextId__c = floatingExtendedItemList[0].Id;
		UPDATE floatingExpenseType;

		Test.startTest();
		// Test 1: Update Code when parent Custom Object is Already In Used in transactions => NG
		ComExtendedItemCustomOption__c alreadyInUsedOption = testData.extendedItemCustomOptionList.get(0);
		alreadyInUsedOption.Code__c = 'changed';
		Exception expectedException;
		try {
			UPDATE alreadyInUsedOption;
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_CannotChangeReference));

		// Test 2: Update Parent when parent Custom Object is already In Used in transactions => NG
		optionForReParentingTest.ExtendedItemCustomId__c = floatingCustomList[0].Id; //Re-parent
		expectedException = null;
		try {
			UPDATE optionForReParentingTest;
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_CannotChangeReference));

		//Test 3: Can update code if the parent Custom object is not in used in transactions => OK
		ComExtendedItemCustomOption__c floatingOption = floatingOptionList.get(1);
		floatingOption.Code__c = 'changed';
		expectedException = null;
		try {
			UPDATE floatingOption;
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertEquals(null, expectedException);
		ComExtendedItemCustomOption__c retrievedFloatingOption = [SELECT ID, Code__c FROM ComExtendedItemCustomOption__c WHERE ID = :floatingOption.Id];
		System.assertEquals(floatingOption.Code__c, retrievedFloatingOption.Code__c);

		//Test 4: Can update parent if the current parent Custom object is not in used in transactions => OK
		floatingOption = floatingOptionList.get(1);
		floatingOption.ExtendedItemCustomId__c = testData.extendedItemCustomList[0].Id;
		expectedException = null;
		try {
			UPDATE floatingOption;
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertEquals(null, expectedException);
		retrievedFloatingOption = [SELECT ID, ExtendedItemCustomId__c FROM ComExtendedItemCustomOption__c WHERE ID = :floatingOption.Id];
		System.assertEquals(floatingOption.ExtendedItemCustomId__c, retrievedFloatingOption.ExtendedItemCustomId__c);
		Test.stopTest();
	}

	/*
	 * Test that if the Custom Option is already in used
	 * by Report/Request/RecordItem/RequestItem (transactions), then user
	 * cannot delete it.
	 */
	@isTest static void deletingAlreadyReferencedInTransactionsTest() {
		ComOrgSetting__c org = [SELECT ID, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];
		ComCountry__c country = [SELECT ID, Name, Name_L0__c, Name_L1__c, Name_L2__c, Code__c FROM ComCountry__c];
		ExpTestData testData = new ExpTestData(org, country);

		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);

		Test.startTest();

		// Deleting Option already used in Report
		ExpReport__c expReport = [SELECT ID, ExtendedItemLookup01Id__c, ExtendedItemLookup01Value__c  From ExpReport__c WHERE ID = :reportList[0].id];
		ComExtendedItemCustomOption__c option = testData.extendedItemCustomOptionList[0];
		expReport.ExtendedItemLookup01Value__c = option.Code__c;
		UPDATE expReport;

		Exception expectedException;
		try {
			DELETE [SELECT ID, Code__c FROM ComExtendedItemCustomOption__c WHERE ID = :option.Id AND Code__c = :option.Code__c];
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_CannotChangeReference));
		DELETE [SELECT ID FROM ExpReport__c];

		// Deleting Option already used in Record
		List<ExpRecordEntity> recordList = testData.createExpRecords(null, Date.today(), testData.expType, testData.employee, 3);
		ExpRecordItem__c recordItem = [SELECT ID, ExtendedItemLookup01Id__c, ExtendedItemLookup01Value__c, ExpRecordId__c  From ExpRecordItem__c WHERE ExpRecordId__c = :recordList[0].id LIMIT 1];
		option = testData.extendedItemCustomOptionList[1];
		recordItem.ExtendedItemLookup01Value__c = option.Code__c;
		UPDATE recordItem;

		expectedException = null;
		try {
			DELETE [SELECT ID, Code__c FROM ComExtendedItemCustomOption__c WHERE ID = :option.Id AND Code__c = :option.Code__c ];
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_CannotChangeReference));
		DELETE [SELECT ID FROM ExpRecord__c];

		// Deleting Option already used in Request
		List<ExpRequestEntity> requestList = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 3);
		ExpRequest__c expRequest = [SELECT ID, ExtendedItemLookup01Id__c, ExtendedItemLookup01Value__c  From ExpRequest__c WHERE ID = :requestList[0].id];
		option = testData.extendedItemCustomOptionList[2];
		expRequest.ExtendedItemLookup01Value__c = option.Code__c;
		UPDATE expRequest;

		expectedException = null;
		try {
			DELETE [SELECT ID, Code__c FROM ComExtendedItemCustomOption__c WHERE ID = :option.Id AND Code__c = :option.Code__c];
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_CannotChangeReference));
		DELETE [SELECT ID FROM ExpRequest__c];

		// Deleting Option already used in RequestRecord
		List<ExpRequestRecordEntity> requestRecordList = testData.createExpRequestRecords(null, Date.today(), testData.expType, testData.employee, testData.job, 3);
		ExpRequestRecordItem__c requestRecordItem = [SELECT ID, ExtendedItemLookup01Id__c, ExtendedItemLookup01Value__c, ExpRequestRecordId__c  From ExpRequestRecordItem__c WHERE ExpRequestRecordId__c = :requestRecordList[0].id LIMIT 1];
		option = testData.extendedItemCustomOptionList[3];
		requestRecordItem.ExtendedItemLookup01Value__c = option.Code__c;
		UPDATE requestRecordItem;

		expectedException = null;
		try {
			DELETE [SELECT ID, Code__c FROM ComExtendedItemCustomOption__c WHERE ID = :option.Id AND Code__c = :option.Code__c];
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_CannotChangeReference));
		DELETE [SELECT ID FROM ExpRequestRecord__c];

		//Option can be deleted if it's not used anywhere
		DELETE option;
		System.assertEquals(true, [SELECT ID FROM ComExtendedItemCustomOption__c WHERE ID = :option.Id].isEmpty());
		Test.stopTest();
	}

	/*
	 * For creating extended item option sObj (without inserting them to the DB)
	 */
	private static List<ComExtendedItemCustomOption__c> createExtendedItemCustomOptionSObjects(String name, String code, Id customId, String customCode, Integer size) {
		List<ComExtendedItemCustomOption__c> sObjList = new List<ComExtendedItemCustomOption__c>();
		for (Integer i = 0; i < size; i++) {
			ComExtendedItemCustomOption__c newCustom = new ComExtendedItemCustomOption__c(
					Name = name + ' ' + i,
					Name_L0__c = name + ' ' + i + ' L0',
					Name_L1__c = name + ' ' + i + ' L1',
					Name_L2__c = name + ' ' + i + ' L2',
					Code__c = code + '_' + i,
					ExtendedItemCustomId__c = customId,
					UniqKey__c = customCode + code + '_' + i
			);
			sObjList.add(newCustom);
		}
		return sObjList;
	}
}