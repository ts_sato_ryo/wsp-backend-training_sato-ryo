/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Service class of Expense Request Record
 * 
 * @group Expense
 */
public class ExpRequestRecordService extends ExpRecordTransactionDataService {
	/*
	 * Save an expense request record
	 * @param empBase Owner employee base entity
	 * @param record Request Record data to be saved
	 * @return Created request record ID
	 */
	public Id saveExpRequestRecord(EmployeeBaseEntity empBase, ExpRequestRecordEntity record, Id reportTypeId) {
		// Set Employee ID to Record
		setEmployeeHistoryIdToRecord(empBase, record);
		// Set Expense Type to Record
		setRecordTypeToRecord(record);

		ExpRequestRecordEntity oldRecord;
		if (String.isNotBlank(record.id)) {
			oldRecord = new ExpRequestRecordRepository().getEntityById(record.id);
			if (oldRecord == null) {
				App.RecordNotFoundException e = new App.RecordNotFoundException();
				e.setMessage(ComMessage.msg().Exp_Err_NotFoundRecord);
				throw e;
			}

			// Verify that the Parent Request is still the name. Record cannot be moved to a different Expense Request
			if (oldRecord.expRequestId != null && oldRecord.expRequestId != record.expRequestId) {
				throw new App.IllegalStateException(App.ERR_CODE_PARENT_CHANGE, ComMessage.msg().Exp_Msg_CannotChangeParent(new List<String>{'expRequestId'}));
			}
		}

		if (oldRecord != null) {
			removeDeletedRecordItemsFromRecord(record, oldRecord);
		}

		// If there is existing Request, then update the total amount
		if (String.isNotBlank(record.expRequestId)) {
			// Only update the Request if the old record amount and new record amount are different
			Decimal oldRecordAmount = oldRecord == null ? 0 : oldRecord.amount;
			if (oldRecordAmount != record.amount) {
				ExpRequestRepository requestRepository = new ExpRequestRepository();
				ExpRequestEntity requestEntity = requestRepository.getEntity(record.expRequestId);

				// Subtract the original record amount from the total amount
				requestEntity.totalAmount = requestEntity.totalAmount - oldRecordAmount + record.amount;
				try {
					requestRepository.saveEntity(requestEntity);
				} catch (DmlException e) {
					throwOutOfRangeException(e);
				}
			}
		}

		// Save Expense Record
		Repository.SaveResult saveResult;
		try {
			saveResult = new ExpRequestRecordRepository().saveEntity(record);
		} catch (DmlException e) {
			throwOutOfRangeException(e);
		}
		// Set record ID
		record.setId(saveResult.details[0].id);

		// Save receipt attached to the record
		saveRecordReceipt(record, empBase);

		List<ExpRequestRecordEntity> oldRecordList = oldRecord == null ? new List<ExpRequestRecordEntity>() : new List<ExpRequestRecordEntity>{oldRecord};
		ExpRequestRecordEntity newRecordEntity = record;
		Map<Id, Set<String>> extItemLookupValuesMap = ExpRequestService.extractChangedExtendedItemLookupValues(null, null, oldRecordList, new List<ExpRequestRecordEntity>{record});
		ExpRecentlyUsedService ruService = new ExpRecentlyUsedService();
		ruService.saveRecentlyUsedExtendedItemLookupValues(extItemLookupValuesMap, empBase.id);
		
		// Update Recently Used values for Job and Cost Center values of record
		ExpRequestRecordItemEntity oldRecordItem = (oldRecord == null || oldRecord.recordItemList == null || oldRecord.recordItemList.isEmpty()) ? null : oldRecord.recordItemList[0];
		ExpRequestRecordItemEntity newRecordItem = (newRecordEntity == null || newRecordEntity.recordItemList == null || newRecordEntity.recordItemList.isEmpty()) ? null : newRecordEntity.recordItemList[0];
		ruService.saveRecentlyUsedCostCenterValues(empBase.id, oldRecordItem != null ? oldRecordItem.costCenterHistoryId : null, newRecordItem != null ? newRecordItem.costCenterHistoryId : null);
		ruService.saveRecentlyUsedJobValues(empBase.id, oldRecordItem != null ? oldRecordItem.jobId : null, newRecordItem != null ? newRecordItem.jobId : null);

		if (reportTypeId != null) {
			Id oldExpenseTypeId;
			if (oldRecord != null && oldRecord.recordItemList!=null && !oldRecord.recordItemList.isEmpty()) {
				// Record's Expense Type will be always retrieved from the item[0]
				oldExpenseTypeId = oldRecord.recordItemList[0].expTypeId;
			}

			Id newExpenseTypeId;
			if (newRecordEntity.recordItemList!=null && !newRecordEntity.recordItemList.isEmpty()) {
				// Record's Expense Type will be always retrieved from the item[0]
				newExpenseTypeId = newRecordEntity.recordItemList[0].expTypeId;
			}

			ruService.saveRecentlyUsedExpenseTypeValues(empBase.id, oldExpenseTypeId, newExpenseTypeId, reportTypeId);

		}

		return record.id;
	}

	/*
	 * Remove record items deleted on the screen from the parent record.
	 * @param newRecord Parent record passed.
	 * @param oldRecord Old parent record before updating.
	 */
	private void removeDeletedRecordItemsFromRecord(ExpRequestRecordEntity newRecord, ExpRequestRecordEntity oldRecord) {
		if (oldRecord.recordItemList.isEmpty()) {
			return; // no need to delete any records
		}

		Set<Id> newRecordItemIdSet = ExpCommonUtil.getIds(newRecord.recordItemList);

		List<ExpRequestRecordItemEntity> deletedItemList = new List<ExpRequestRecordItemEntity>();
		for (ExpRequestRecordItemEntity oldRecordItem : oldRecord.recordItemList) {
			// If old record item is not in new record items, add it to deleted list.
			if (!newRecordItemIdSet.contains(oldRecordItem.id)) {
				deletedItemList.add(oldRecordItem);
			}
		}

		// Delete existing ExpRecordItems if record to be saved does not contain the ExpRecordItem
		if (!deletedItemList.isEmpty()) {
			new ExpRequestRecordRepository().deleteEntityList(deletedItemList);
		}
	}

	/*
	 * Delete expense request records, and update the total amount of Request
	 * @param List<Id> List of target expense request record IDs
	 */
	public void deleteExpRequestRecords(List<Id> recordIdList) {
		ExpRequestRecordRepository requestRecordRepository = new ExpRequestRecordRepository();
		List<ExpRequestRecordEntity> recordList = requestRecordRepository.getEntityListByIds(recordIdList);

		// If number of Records found doesn't match with number of Ids, throw the exception
		if (recordIdList.size() != recordList.size()) {
			App.RecordNotFoundException e = new App.RecordNotFoundException();
			e.setMessage(ComMessage.msg().Exp_Err_NotFoundRecord);
			throw e;
		}

		Set<Id> expRequestIdSet = new Set<Id>();
		Decimal recordTotalAmount = 0;
		for (ExpRequestRecordEntity recordEntity: recordList) {
			if (String.isBlank(recordEntity.expRequestId)) {
				// Currently, floating ExpRequestRecord are not yet supported.
				throw new App.IllegalStateException(App.ERR_CODE_NOT_SET_VALUE, ComMessage.msg().Com_Err_NotSetValue(new List<String>{'expRequestId'}));
			}
			expRequestIdSet.add(recordEntity.expRequestId);
			recordTotalAmount += recordEntity.amount;
		}

		if (expRequestIdSet.size() != 1) {
			// If the set contains more than one, then it means the records belong to different ExpRequest. Throw Exception.
			throw new App.ParameterException(App.ERR_CODE_UNSUPPORTED_VALUE,
					ComMessage.msg().Com_Err_InvalidData(new List<String>{'expRequestId'}));
		}

		//Only need to update the Request Total Amount if it's non-zero.
		if (recordTotalAmount != 0) {
			ExpRequestRepository requestRepository = new ExpRequestRepository();
			// Since it's already checked that there is no null expRequestId, and all RequestRecords belong to the same Request, can take any Record
			ExpRequestEntity requestEntity = requestRepository.getEntity(recordList[0].expRequestId);
			requestEntity.totalAmount = requestEntity.totalAmount - recordTotalAmount;
			requestRepository.saveEntity(requestEntity);
		}

		requestRecordRepository.deleteEntityList(recordList);
	}

	/*
	 * Clone the Expense Request Records on the given dates
	 *
	 * @param recordIds List of Record IDs to Clone
	 * @param clonedRecordTargetDateList List of target date to create the clone records
	 * @return Map with the following structure Map<K: Id of newly created record, V: Entity if Changed; else null>
	 */
	public Map<Id, ExpRequestRecordEntity> cloneExpenseRequestRecords(List<Id> recordIds, List<AppDate> clonedRecordTargetDateList) {
		List<ExpRequestRecordEntity> recordEntityList = new ExpRequestRecordRepository().getEntityListByIds(recordIds);
		final ComMsgBase MSG = ComMessage.msg();
		if (recordEntityList.isEmpty() || recordEntityList.size() != recordIds.size()) {
			throw new App.RecordNotFoundException(MSG.Com_Err_RecordNotFound);
		}

		// Verify that all Records Belong to the same Request
		Set<Id> expRequestIdSet = new Set<Id>();
		for (ExpRequestRecordEntity recordEntity : recordEntityList) {
			expRequestIdSet.add(recordEntity.expRequestId);
		}
		if (expRequestIdSet.size() != 1) {
			throw new App.ParameterException(MSG.Exp_Err_CannotCloneRecordsFromDifferentRequest);
		}

		Id requestID = recordEntityList[0].expRequestId;
		Decimal totalAmountByClonedRecords = 0;

		Set<Id> expenseTypeIdSet = new Set<Id>();
		Set<Id> taxTypeBaseIdSet = new Set<Id>();
		Set<Id> currencyIdSet = new Set<Id>();
		// Pre Process the Record and Collect the ID needed for further processing
		processForRecordCloningAndCollectIds(recordEntityList, expenseTypeIdSet, taxTypeBaseIdSet, currencyIdSet);

		// Find Earlier and Latest Date from the Given date List
		AppDate earliestDate = ValidPeriodEntity.VALID_TO_MAX;
		AppDate latestDate = ValidPeriodEntity.VALID_FROM_MIN;
		for (AppDate targetDate : clonedRecordTargetDateList) {
			if (targetDate.isBefore(earliestDate)) {
				earliestDate = targetDate;
			}
			if (targetDate.isAfter(latestDate)) {
				latestDate = targetDate;
			}
		}

		// Check that all Expenses are Valid at the given date
		ExpTypeRepository.SearchFilter expTypeFilter = new ExpTypeRepository.SearchFilter();
		expTypeFilter.ids = expenseTypeIdSet;
		expTypeFilter.validFrom = earliestDate;
		expTypeFilter.validTo = latestDate;
		List<ExpTypeEntity> expTypeEntityList = (new ExpTypeRepository()).searchEntity(expTypeFilter);
		// Since All the Expense Type in the Set will be used at all the given target Dates, if the return size doesn't match, then error.
		if (expTypeEntityList.size() != expenseTypeIdSet.size()) {
			throw new App.RecordNotFoundException(MSG.Com_Err_OutOfValidPeriod(
				new List<String>{MSG.Exp_Lbl_ExpenseType}));
		}

		// Can take from any since all the Expense type will be belong to the same Company
		CompanyEntity company = ExpCommonUtil.getCompany(expTypeEntityList[0].companyId);
		if (company == null) {
			throw new App.RecordNotFoundException(MSG.Com_Err_RecordNotFound);
		}

		// Get Tax Information in a map => Map<K: Tax Base ID, V: Map<K: Target Date, V: Tax History Entity>>
		Map<Id, Map<AppDate, ExpTaxTypeHistoryEntity>> baseTargetHistoryMap = getTaxTypeMapForRecordCloning(taxTypeBaseIdSet, clonedRecordTargetDateList, earliestDate, latestDate);

		// Get Exchange Rate Information in a map =>  Map<K: Currency ID, V: Map<K: Target Date, V: Exchange Rate>>
		Map<Id, Map<AppDate, Decimal>> currencyTargetExchangeRateEntityMap = getExchangeRateMapForRecordCloning(
			currencyIdSet, clonedRecordTargetDateList, earliestDate, latestDate);

		CurrencyRepository.SearchFilter filter = new CurrencyRepository.SearchFilter();
		filter.ids = new Set<Id> {company.currencyId};

		// Since only looking for company currency
		CurrencyEntity companyCurrency = new CurrencyRepository().searchEntity(filter, false)[0];

		List<ExpRequestRecordEntity> expRecordEntityList = new List<ExpRequestRecordEntity>();
		Set<Integer> autoUpdatedIndexSet = new Set<Integer>();
		for (AppDate targetDate : clonedRecordTargetDateList) {
			for (ExpRequestRecordEntity originalRecord : recordEntityList) {
				ExpRequestRecordEntity clonedRecord = getCloneWithCloneableFields(originalRecord);
				Boolean autoUpdated = false;
				// Update the Date to the Target Date & Parent Request ID
				clonedRecord.expRequestId = requestID;
				clonedRecord.recordDate = targetDate;
				clonedRecord.withoutTax = 0;
				clonedRecord.amount = 0;
				// If Record Type is Hotel Fee, don't do anything since the Item 0 doesn't contain Tax or Exchange Rate data
				if (clonedRecord.recordType != ExpRecordType.HOTEL_FEE) {
					for (ExpRequestRecordItemEntity clonedRecordItem : clonedRecord.recordItemList) {
						clonedRecordItem.itemDate = targetDate;
						if (clonedRecordItem.useForeignCurrency == true) {
							autoUpdated = processExchangeRateDataForRecordCloning(clonedRecordItem, currencyTargetExchangeRateEntityMap, companyCurrency.decimalPlaces);
						} else {
							autoUpdated = processTaxRateDataForRecordCloning(clonedRecordItem, baseTargetHistoryMap, companyCurrency.decimalPlaces);
							clonedRecord.withoutTax += clonedRecordItem.withoutTax;
						}
						clonedRecord.amount += clonedRecordItem.amount;
					}
				} else {
					ExpRequestRecordItemEntity clonedRecordItem = clonedRecord.recordItemList[0];
					if (clonedRecordItem.useForeignCurrency == true) {
						clonedRecordItem.amount = 0;
					} else {
						clonedRecordItem.gstVat = 0;
						clonedRecordItem.withoutTax = 0;
					}
					clonedRecord.amount += clonedRecordItem.amount;
				}
				totalAmountByClonedRecords += clonedRecord.amount;
				expRecordEntityList.add(clonedRecord);
				if (autoUpdated == true) {
					autoUpdatedIndexSet.add(expRecordEntityList.size() - 1);
				}
			}
		}

		Map<Id, ExpRequestRecordEntity> clonedRecordMap = new Map<Id, ExpRequestRecordEntity>();
		if (!expRecordEntityList.isEmpty()) {
			try {
				ExpRequestRecordRepository.SaveRequestRecordResult result = (ExpRequestRecordRepository.SaveRequestRecordResult) new ExpRequestRecordRepository().saveEntityList(expRecordEntityList);
				if (result.isSuccessAll != true) {
					for (Repository.ResultDetail resultDetail: result.details) {
						if (resultDetail.isSuccess != true) {
							throw new App.IllegalStateException(App.ERR_CODE_UNSUPPORTED_VALUE, resultDetail.errorMessage);
						}
					}
				}
				for (integer i = 0; i < expRecordEntityList.size(); i++) {
					expRecordEntityList[i].setId(result.details[i].id);
					if (autoUpdatedIndexSet.contains(i)) {
						clonedRecordMap.put(expRecordEntityList[i].id, expRecordEntityList[i]);
					} else {
						clonedRecordMap.put(expRecordEntityList[i].id, null);
					}
				}
				// Need to Update Request Total Amount
				if (totalAmountByClonedRecords != 0) {
					ExpRequestRepository requestRepo = new ExpRequestRepository();
					ExpRequestEntity requestEntity = requestRepo.getEntity(requestID);
					if (requestEntity == null) {
						throw new App.RecordNotFoundException(MSG.Exp_Err_ReportNotFound);
					}
					requestEntity.totalAmount += totalAmountByClonedRecords;
					requestRepo.saveEntity(requestEntity);
				}
			} catch (DmlException e) {
				if (e.getDmlType(0) == System.StatusCode.NUMBER_OUTSIDE_VALID_RANGE) {
					throw new App.IllegalStateException(App.ERR_CODE_UNSUPPORTED_VALUE, MSG.Com_Err_InvalidValueTooLarge(new List<String>{ComMessage.msg().Exp_Lbl_Amount}));
				}
				throw e;
			}
		}
		return clonedRecordMap;
	}

	/*
	 * Create a new instance from the provided original record (shallow clone)
	 * @param originalRecord Record to copy the data from
	 * @return ExpRequestRecordEntity a new entity containing the clonable data
	 */
	public static ExpRequestRecordEntity getCloneWithCloneableFields(ExpRequestRecordEntity originalRecord) {
		ExpRequestRecordEntity clonedRecord = new ExpRequestRecordEntity();
		clonedRecord.name = originalRecord.name;
		clonedRecord.employeeHistoryId = originalRecord.employeeHistoryId;
		clonedRecord.recordDate = originalRecord.recordDate;
		clonedRecord.amount = originalRecord.amount;
		clonedRecord.withoutTax = originalRecord.withoutTax;
		clonedRecord.localAmount = originalRecord.localAmount;
		clonedRecord.order = originalRecord.order;
		clonedRecord.recordType = originalRecord.recordType;
		clonedRecord.typeData = originalRecord.typeData;

		List<ExpRequestRecordItemEntity> expRecordItemEntityList = new List<ExpRequestRecordItemEntity>();
		for (ExpRequestRecordItemEntity originalRecordItem : originalRecord.recordItemList) {
			ExpRequestRecordItemEntity clonedRecordItem = new ExpRequestRecordItemEntity();
			clonedRecordItem.name = originalRecordItem.name;
			clonedRecordItem.itemDate = originalRecordItem.itemDate;
			clonedRecordItem.expTypeId = originalRecordItem.expTypeId;
			clonedRecordItem.expTypeNameL = originalRecordItem.expTypeNameL;	//READ ONLY (FOR REFERENCE)
			clonedRecordItem.amount = originalRecordItem.amount;
			clonedRecordItem.withoutTax = originalRecordItem.withoutTax;
			clonedRecordItem.localAmount = originalRecordItem.localAmount;
			clonedRecordItem.taxTypeHistoryId = originalRecordItem.taxTypeHistoryId;
			clonedRecordItem.taxRate = originalRecordItem.taxRate;
			clonedRecordItem.taxTypeBaseId = originalRecordItem.taxTypeBaseId;
			clonedRecordItem.gstVat = originalRecordItem.gstVat;
			clonedRecordItem.taxManual = originalRecordItem.taxManual;
			clonedRecordItem.useFixedForeignCurrency = originalRecordItem.useFixedForeignCurrency;
			clonedRecordItem.useForeignCurrency = originalRecordItem.useForeignCurrency;
			clonedRecordItem.currencyId = originalRecordItem.currencyId;
			clonedRecordItem.currencyInfo = originalRecordItem.currencyInfo;
			clonedRecordItem.exchangeRate = originalRecordItem.exchangeRate;
			clonedRecordItem.originalExchangeRate = originalRecordItem.originalExchangeRate;
			clonedRecordItem.exchangeRateManual = originalRecordItem.exchangeRateManual;
			clonedRecordItem.remarks = originalRecordItem.remarks;
			clonedRecordItem.order = originalRecordItem.order;
			clonedRecordItem.jobId = originalRecordItem.jobId;
			clonedRecordItem.costCenterHistoryId = originalRecordItem.costCenterHistoryId;
			clonedRecordItem.fixedAllowanceOptionId = originalRecordItem.fixedAllowanceOptionId;

			for (Integer i = 0; i < ExpExtendedItemEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				// Only Lookup ID needs to be saved, and the other IDs are retrieved directly from the Expense Type
				clonedRecordItem.setExtendedItemLookupId(i, originalRecordItem.getExtendedItemLookupId(i));

				clonedRecordItem.setExtendedItemDateValue(i, originalRecordItem.getExtendedItemDateValue(i));
				clonedRecordItem.setExtendedItemLookupValue(i, originalRecordItem.getExtendedItemLookupValue(i));
				clonedRecordItem.setExtendedItemPicklistValue(i, originalRecordItem.getExtendedItemPicklistValue(i));
				clonedRecordItem.setExtendedItemTextValue(i, originalRecordItem.getExtendedItemTextValue(i));
			}

			expRecordItemEntityList.add(clonedRecordItem);
		}

		clonedRecord.replaceRecordItemList(expRecordItemEntityList);
		return clonedRecord;
	}

	/*
	 * Set employee history ID to Record
	 * @param empBase EmployeeBaseEntity used to retrieve the employee history
	 * @param record ExpRequestRecordEntity to set the Employee History To
 	 */
	private void setEmployeeHistoryIdToRecord(EmployeeBaseEntity empBase, ExpRequestRecordEntity record) {
		EmployeeHistoryEntity empHistory = new EmployeeService().getEmployee(empBase.id, record.recordDate).getHistory(0);
		record.employeeHistoryId = empHistory.id;
	}

	/*
	 * Set record type
 	 */
	private void setRecordTypeToRecord(ExpRequestRecordEntity record) {
		// Record's Record Type will be always retrieved from the item[0]
		// When Record Type is Hotel Fee, then item[0] contains the Record data. Otherwise, there will always be only 1 recordItem in the record
		ExpTypeEntity expType = new ExpTypeRepository().getEntity(record.recordItemList[0].expTypeId);
		record.recordType = expType.recordType;
	}

	/*
	 * Save receipt attached to the record, and if the receipt is changed, delete the existing link.
	 * @param record ExpRequestRecordEntity containing the Receipt
	 */
	private void saveRecordReceipt(ExpRequestRecordEntity record, EmployeeBaseEntity empBase) {
		// Get Existing Receipt associated with the record
		AppContentDocumentRepository contentRepo = new AppContentDocumentRepository();

		List<AppContentDocumentLinkEntity> existingContentDocumentLinkEntityList = contentRepo.getLinkEntityListByLinkedEntityId(new List<Id>{record.id});
		List<AppContentDocumentLinkEntity> deleteLinkList = new List<AppContentDocumentLinkEntity>();

		AppContentDocumentLinkEntity link = getLinkEntityFromRecord(record);

		Boolean needToCreateLink = (link != null);
		for (AppContentDocumentLinkEntity existingLink : existingContentDocumentLinkEntityList) {
			if (link == null) {
				deleteLinkList.add(existingLink);
			} else if (link.contentDocumentId != existingLink.contentDocumentId) {
				deleteLinkList.add(existingLink);
			} else if (existingLink.linkedEntityId == record.id &&
				existingLink.contentDocumentId == link.contentDocumentId) {
				needToCreateLink = false;
			}
		}
		if (deleteLinkList.size() > 0) {
			// From the ContentDocument, get any existing contentDocumentLink and add those to the deleteLinkList
			List<AppContentDocumentEntity> existingContentDocumentEntityList = contentRepo.getEntityList(new List<Id>{existingContentDocumentLinkEntityList[0].contentDocumentId});
			for (AppContentDocumentEntity existingDocument : existingContentDocumentEntityList) {
				for (AppContentDocumentLinkEntity existingDocumentLink : existingDocument.contentDocumentLinkList) {
					// Delete links to both ExpRecord and ExpReportRequest
					if (existingDocumentLink.linkedEntityId != empBase.userId) {
						deleteLinkList.add(existingDocumentLink);
					}
				}
			}
			contentRepo.deleteEntityList(deleteLinkList);
		}

		if (needToCreateLink) {
			link.linkedEntityId = record.id;
			new AppContentDocumentRepository().saveLink(link);
		}
	}

	/*
	 * Extract the Link entity from the ExpRequestRecordEntity
	 *
	 * @return if exists, return AppContentDocumentLinkEntity. Otherwise, null.
	 */
	private static AppContentDocumentLinkEntity getLinkEntityFromRecord(ExpRequestRecordEntity record) {
		if ((record.receiptList == null) || record.receiptList.isEmpty()) {
			return null;
		}

		AppContentDocumentEntity content = record.receiptList[0]; // now only one attachment is allowed
		if (content.contentDocumentLinkList == null || content.contentDocumentLinkList.isEmpty()) {
			return null;
		}
		AppContentDocumentLinkEntity link = content.contentDocumentLinkList[0];
		return link;
	}

	/*
	 * If the DmlException is of Number Outside the Valid Range, throw custom Unsupported Exception.
	 * Otherwise, throw the original Exception.
	 */
	private static void throwOutOfRangeException(DmlException e) {
		if (e.getDmlType(0) == System.StatusCode.NUMBER_OUTSIDE_VALID_RANGE) {
			throw new App.IllegalStateException(
					App.ERR_CODE_UNSUPPORTED_VALUE,
					ComMessage.msg().Com_Err_InvalidValueTooLarge(new List<String>{ComMessage.msg().Exp_Lbl_Amount}));
		}
		throw e;
	}

	/*
	 * Pre-process the Records before cloning, and collect the necessary IDs form the Records and Record Items.
	 * @param recordEntityList List of source Records
	 * @param expenseTypeIdSet Set to populate the Expense Type ID to
	 * @param taxTypeBaseIdSet Set to populate the Tax Type Base ID to
	 * @param currencyIdSet Set to populate the Currency ID to
	 */
	private static void processForRecordCloningAndCollectIds(List<ExpRequestRecordEntity> recordEntityList, Set<Id> expenseTypeIdSet, Set<Id> taxTypeBaseIdSet, Set<Id> currencyIdSet) {
		for (ExpRequestRecordEntity recordEntity : recordEntityList) {
			// Reset Itemization Record Items before cloning
			if (recordEntity.recordType == ExpRecordType.HOTEL_FEE) {
				recordEntity.replaceRecordItemList(new List<ExpRequestRecordItemEntity>{recordEntity.recordItemList[0]});
			}

			for (ExpRequestRecordItemEntity recordItemEntity : recordEntity.recordItemList) {
				//  Only verify the Tax Type and Exchange Rate if it's not Hotel Fee
				//  as Hotel Fee doesn't have tax and exchange rate on the Base Record (meaning RecordItem[0])
				if (recordEntity.recordType != ExpRecordType.HOTEL_FEE) {
					// Update with the correct tax amount on the given day
					if (recordItemEntity.useForeignCurrency == False) {
						taxTypeBaseIdSet.add(recordItemEntity.taxTypeBaseId);
					} else if (recordItemEntity.useForeignCurrency == True) {
						currencyIdSet.add(recordItemEntity.currencyId);
					}
				}
				expenseTypeIdSet.add(recordItemEntity.expTypeId);
			}
		}
	}
}