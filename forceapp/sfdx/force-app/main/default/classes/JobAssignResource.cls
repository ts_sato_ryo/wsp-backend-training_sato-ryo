/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description ジョブ割当レコードを操作するAPIを実装するクラス
*/
public with sharing class JobAssignResource {

	/**
	 * @description ジョブ割当登録のリクエスト情報を保持するクラス
	 */
	public class CreateRequest implements RemoteApi.RequestParam {
		/** ジョブID */
		public String jobId;
		/** 有効開始日 */
		public String validDateFrom;
		/** 失効日 */
		public String validDateTo;
		/** 割当対象社員 */
		public List<String> employeeIds;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// ジョブID
			if (String.isBlank(jobId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('jobId');
				throw e;
			}
			try {
				Id.ValueOf(jobId);
			} catch (Exception e) {
				throw new App.ParameterException('jobId', jobId);
			}

			// 有効開始日
			if (String.isBlank(validDateFrom)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('validDateFrom');
				throw e;
			}
			try {
				AppDate.parse(validDateFrom);
			} catch (Exception e) {
				throw new App.ParameterException('validDateFrom', validDateFrom);
			}

			// 失効日
			if (String.isNotBlank(validDateTo)) {
				try {
					AppDate.parse(validDateTo);
				} catch (Exception e) {
					throw new App.ParameterException('validDateTo', validDateTo);
				}
			}

			// 社員ID
			if (employeeIds == null || employeeIds.isEmpty()) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('employeeIds');
				throw e;
			}
			for (String employeeId : employeeIds) {
				try {
					Id.ValueOf(employeeId);
				} catch (Exception e) {
					throw new App.ParameterException('employeeId', employeeId);
				}
			}
		}
	}

	/**
	 * @description ジョブ割当登録結果の情報を保持するクラス
	 */
	public class CreateResponse implements RemoteApi.ResponseParam {
		/** 登録したレコードのID */
		public List<String> ids = new List<String>();
	}

	/**
	 * @description ジョブ割当レコードを登録するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB;

		/**
		 * @description ジョブに対して社員の割当を登録する
		 * @param  request リクエストパラメータ
		 * @return 登録したジョブ割当IDのリスト
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request request) {
			CreateRequest param = (CreateRequest)request.getParam(CreateRequest.class);
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// レコード登録
			JobAssignService service = new JobAssignService();
			AppDate validFrom = AppDate.parse(param.validDateFrom);
			AppDate validTo = param.validDateTo == null ? ValidPeriodEntity.VALID_TO_MAX : AppDate.parse(param.validDateTo);
			List<JobAssignEntity> entities = service.createJobAssign(param.jobId, validFrom, validTo, param.employeeIds);

			// レスポンス生成
			CreateResponse response = new CreateResponse();
			for (JobAssignEntity entity : entities) {
				response.ids.add(entity.id);
			}
			return response;
		}
	}

	/**
	 * @description ジョブ割当更新のリクエスト情報を保持するクラス
	 */
	public class UpdateRequest implements RemoteApi.RequestParam {
		/** 更新対象のジョブ割当IDのリスト */
		public List<String> ids;
		/** 有効開始日 */
		public String validDateFrom;
		/** 失効日 */
		public String validDateTo;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// 更新対象のジョブ割当IDのリスト
			if (ids == null || ids.isEmpty()) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('ids');
				throw e;
			}
			for (String id : ids) {
				try {
					Id val = (Id)id;
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}

			// 有効開始日
			if (String.isBlank(validDateFrom)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('validDateFrom');
				throw e;
			}
			try {
				AppDate.parse(validDateFrom);
			} catch (Exception e) {
				throw new App.ParameterException('validDateFrom', validDateFrom);
			}

			// 失効日
			if (String.isNotBlank(validDateTo)) {
				try {
					AppDate.parse(validDateTo);
				} catch (Exception e) {
					throw new App.ParameterException('validDateTo', validDateTo);
				}
			}
		}
	}

	/**
	 * @description ジョブ割当更新結果の情報を保持するクラス
	 */
	public class UpdateResponse implements RemoteApi.ResponseParam {
		/** 登録したレコードのID */
		public List<String> ids = new List<String>();
	}

	/**
	 * @description ジョブ割当レコードを更新するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB;

		/**
		 * @description ジョブに対して社員の割当を更新する
		 * @param  request リクエストパラメータ
		 * @return 更新したジョブ割当IDのリスト
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request request) {
			UpdateRequest param = (UpdateRequest)request.getParam(UpdateRequest.class);
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// レコード登録
			JobAssignService service = new JobAssignService();
			AppDate validFrom = AppDate.parse(param.validDateFrom);
			AppDate validTo = param.validDateTo == null ? ValidPeriodEntity.VALID_TO_MAX : AppDate.parse(param.validDateTo);
			List<JobAssignEntity> entities = service.updateJobAssign(param.ids, validFrom, validTo);

			// レスポンス生成
			UpdateResponse response = new UpdateResponse();
			for (JobAssignEntity entity : entities) {
				response.ids.add(entity.id);
			}
			return response;
		}
	}

	/**
	 * @description ジョブ割当削除のリクエスト情報を保持するクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象のジョブ割当IDのリスト */
		public List<String> ids;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// 削除対象のジョブ割当IDのリスト
			if (ids == null || ids.isEmpty()) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('ids');
				throw e;
			}
			for (String id : ids) {
				try {
					Id val = (Id)id;
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}
		}
	}

	/**
	 * @description ジョブ割当削除結果の情報を保持するクラス
	 */
	public class DeleteResponse implements RemoteApi.ResponseParam {
		// 何も返さない
	}

	/**
	 * @description ジョブ割当レコードを削除するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_JOB;

		/**
		 * @description ジョブに対して社員の割当を更新する
		 * @param  request リクエストパラメータ
		 * @return 更新したジョブ割当IDのリスト
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request request) {
			DeleteRequest param = (DeleteRequest)request.getParam(DeleteRequest.class);
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// レコード登録
			JobAssignService service = new JobAssignService();
			service.deleteJobAssign(param.ids);

			// レスポンス生成
			return new DeleteResponse();
		}
	}

	/**
	 * @description ジョブ割当検索のリクエスト情報クラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** ジョブID */
		public String jobId;
		/** 社員Id */
		public String employeeId;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// ジョブID
			if (String.isNotBlank(jobId)) {
				try {
					Id.ValueOf(jobId);
				} catch (Exception e) {
					throw new App.ParameterException('jobId', jobId);
				}
			}
			// 社員ID
			if (String.isNotBlank(employeeId)) {
				try {
					Id.ValueOf(employeeId);
				} catch (Exception e) {
					throw new App.ParameterException('employeeId', employeeId);
				}
			}
		}
	}

	/**
	 * @description ジョブ割当検索結果の情報を保持するクラス
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** 検索結果 */
		public List<JobAssign> records = new List<JobAssign>();
	}

	/**
	 * ジョブ割当1件の情報を保持するクラス
	 */
	public class JobAssign {
		/** ジョブ割当Id */
		public String id;
		/** ジョブId */
		public String jobId;
		/** 社員ベースId */
		public String employeeId;
		/** 社員コード */
		public String employeeCode;
		/** 社員名 */
		public String employeeName;
		/** 部署ベースId */
		public String departmentId;
		/** 部署コード */
		public String departmentCode;
		/** 部署名 */
		public String departmentName;
		/** 有効開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;

		/**
		 * コンストラクタ
		 * 割当期間に該当する部署履歴が存在しない場合に使用する
		 */
		public JobAssign(JobAssignEntity assign) {
			this.id = assign.id;
			this.jobId = assign.jobId;
			this.validDateFrom = AppDate.convertDate(assign.validFrom);
			this.validDateTo = AppDate.convertDate(assign.validTo);

			// 社員
			this.employeeId = assign.employeeBaseId;
			this.employeeCode = assign.employeeCode;
			this.employeeName = assign.employeeNameL.getFullName();
		}


		/**
		 * コンストラクタ
		 * 割当期間に該当する部署履歴が存在する場合に使用する
		 */
		public JobAssign(JobAssignEntity assign, DepartmentBaseEntity deptBase, DepartmentHistoryEntity deptHistory) {
			this(assign);

			// 部署
			this.departmentId = deptBase.id;
			this.departmentCode = deptBase.code;
			this.departmentName = deptHistory.nameL.getValue();
		}
	}

	/**
	 * @description ジョブ割当レコードを検索するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description ジョブ割当レコードを検索する
		 * @param  request リクエストパラメータ
		 * @return 検索結果
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request request) {
			SearchRequest param = (SearchRequest)request.getParam(SearchRequest.class);
			param.validate();

			// ジョブ割当を検索
			JobAssignService jobAssignService = new JobAssignService();
			List<JobAssignEntity> assigns = jobAssignService.searchJobAssign(param.jobId, param.employeeId);
			if (assigns.isEmpty()) {
				return new SearchResponse();
			}

			// 社員を検索
			EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
			empFilter.baseIds = new Set<Id>();
			for (JobAssignEntity assign : assigns) {
				empFilter.baseIds.add(assign.employeeBaseId);
			}
			List<EmployeeBaseEntity> employees = new EmployeeRepository().searchEntity(empFilter);

			// 部署を検索
			Set<Id> deptBaseIds = new Set<Id>();
			for (EmployeeBaseEntity employee : employees) {
				for (EmployeeHistoryEntity history : employee.getHistoryList()) {
					deptBaseIds.add(history.departmentId);
				}
			}
			DepartmentRepository.SearchFilter deptFilter = new DepartmentRepository.SearchFilter();
			deptFilter.baseIds = new List<Id>(deptBaseIds);
			List<DepartmentBaseEntity> departments = new DepartmentRepository().searchEntity(deptFilter);

			// レスポンスを作成
			SearchResponse response = new SearchResponse();
			response.records = createJobAssign(assigns, employees, departments);
			return response;
		}

		/**
		 * 社員と部署の情報から、ジョブ割当のレスポンスを生成する
		 * @param assigns レスポンス対象のジョブ割当
		 * @param employees 全履歴を含む社員情報のリスト
		 * @param departments 全履歴を含む部署情報のリスト
		 * @return ジョブ割当のリスト
		 */
		@TestVisible
		private List<JobAssign> createJobAssign(List<JobAssignEntity> assigns, List<EmployeeBaseEntity> employees, List<DepartmentBaseEntity> departments) {
			// 社員、部署をベースIdごとに振り分け
			Map<Id, EmployeeBaseEntity> employeeMap = new Map<Id, EmployeeBaseEntity>();
			for (EmployeeBaseEntity employee : employees) {
				employeeMap.put(employee.id, employee);
			}
			Map<Id, DepartmentBaseEntity> departmentMap = new Map<Id, DepartmentBaseEntity>();
			for (DepartmentBaseEntity department : departments) {
				departmentMap.put(department.id, department);
			}

			List<JobAssign> result = new List<JobAssign>();
			for (JobAssignEntity assign : assigns) {
				// 社員履歴を取得
				EmployeeBaseEntity employee = employeeMap.get(assign.employeeBaseId);
				EmployeeHistoryEntity empHistory = employee.getHistoryByDate(assign.validFrom);

				// 開始日に該当する社員履歴が存在しない場合、もしくは部署が未設定の場合
				if (empHistory == null || empHistory.departmentId == null) {
					result.add(new JobAssign(assign));
					continue;
				}

				// 部署履歴を取得
				DepartmentBaseEntity deptBase = departmentMap.get(empHistory.departmentId);
				DepartmentHistoryEntity deptHistory = deptBase.getHistoryByDate(assign.validFrom);

				// 開始日に該当する部署が存在しない場合
				if (deptHistory == null) {
					result.add(new JobAssign(assign));
					continue;
				}

				// 社員・部署の履歴がともに存在する場合
				result.add(new JobAssign(assign, deptBase, deptHistory));
			}

			return result;
		}
	}
}