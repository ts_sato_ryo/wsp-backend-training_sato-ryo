/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Resource class for ExtendedItemCustom and ExtendedItemCustomOption
 * 
 * @group Expense
 */
public with sharing class ExtendedItemCustomResource {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** Total number of records to returns */
	private static final Integer OPTION_RESULT_LIMIT = 100;

	/*
	 * Request Param for the searching the Custom Extended Item
	 */
	public class ExtendedItemCustomSearchCondition implements RemoteApi.RequestParam {
		public String companyId;
		public String id;
		public String code;

		/*
		 * Validate that the request params are valid
		 */
		public void validate() {
			ExtendedItemCustomResource.verifyValidIdValue(this.id, false, 'id');
			ExtendedItemCustomResource.verifyValidIdValue(this.companyId, false, 'companyId');
		}
	}

	/*
	 * Response VO representing a record of ExtendedItemCustom
	 * The Options {@code ExtendedItemCustomOption} are not included in the reponse to keep the response lean.
	 */
	public class ExtendedItemCustom {
		public String id;
		public String code;
		public String name;
		public String nameL0;
		public String nameL1;
		public String nameL2;
	}

	/*
	 * Response for the Custom Extended Item Search Result
	 */
	public virtual class ExtendedItemCustomSearchResult implements RemoteApi.ResponseParam {
		public List<ExtendedItemCustom> records;
	}

	/*
	 * API for searching Custom Extended Items.
	 * The Options are not included in the response to keep the response lean.
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			ExtendedItemCustomSearchCondition param = (ExtendedItemCustomSearchCondition) req.getParam(ExtendedItemCustomSearchCondition.class );
			param.validate();
			ExtendedItemCustomRepository.SearchFilter filter = createSearchFilter(param);
			List<ExtendedItemCustomEntity> entityList = new ExtendedItemCustomService().getExtendedItemCustomEntityList(filter, false);

			List<ExtendedItemCustom> records = new List<ExtendedItemCustom>();
			for (ExtendedItemCustomEntity entity : entityList) {
				records.add(createExtendedItemCustomResponse(entity));
			}

			ExtendedItemCustomSearchResult res = new ExtendedItemCustomSearchResult();
			res.records = records;
			return res;
		}

		/*
		 * Create an instance of {@code ExtendedItemCustom} Response VO for the given {@code ExtendedItemCustomEntity}
		 */
		private ExtendedItemCustom createExtendedItemCustomResponse(ExtendedItemCustomEntity entity) {
			ExtendedItemCustom param = new ExtendedItemCustom();
			param.id = entity.id;
			param.code = entity.code;
			param.name = entity.nameL.getValue();
			param.nameL0 = entity.nameL0;
			param.nameL1 = entity.nameL1;
			param.nameL2 = entity.nameL2;

			return param;
		}

		/*
		 * Create SearchFilter for searching Custom Extended Item
		 */
		private ExtendedItemCustomRepository.SearchFilter createSearchFilter(ExtendedItemCustomSearchCondition param) {
			ExtendedItemCustomRepository.SearchFilter filter = new ExtendedItemCustomRepository.SearchFilter();
			if (String.isNotBlank(param.id)) {
				filter.ids = new Set<Id>{param.id};
			}
			if (String.isNotBlank(param.companyId)) {
				filter.companyIds = new Set<Id>{param.companyId};
			}
			if (String.isNotBlank(param.code)) {
				filter.codes = new Set<String>{param.code};
			}
			return filter;
		}
	}


	/*
	 * Request Param for the searching the Custom Extended Item Options
	 */
	public class ExtendedItemCustomOptionSearchCondition implements RemoteApi.RequestParam {
		public String extendedItemCustomId;
		public String id;
		public String code;
		public String query;

		/*
		 * Validate that the request params are valid
		 */
		public void validate() {
			ExtendedItemCustomResource.verifyValidIdValue(this.id, false, 'id');
			ExtendedItemCustomResource.verifyValidIdValue(this.extendedItemCustomId, false, 'extendedItemCustomId');
		}
	}

	/*
     * Response VO representing a record of ExtendedItemCustomOption
     */
	public class ExtendedItemCustomOption {
		public String id;
		public String code;
		public String name;
		public String nameL0;
		public String nameL1;
		public String nameL2;
		public String extendedItemCustomId;
	}

	/*
	 * Response for the Custom Extended Item Option Search Result
	 */
	public virtual class ExtendedItemCustomOptionSearchResult implements RemoteApi.ResponseParam {
		public List<ExtendedItemCustomOption> records;
		public Boolean hasMore;
	}

	/*
	 * Create an instance of {@code ExtendedItemCustomOption} Response VO for the given {@code ExtendedItemCustomOptionEntity}
	 */
	private static ExtendedItemCustomOption createExtendedItemCustomOptionResponse(ExtendedItemCustomOptionEntity entity) {
		ExtendedItemCustomOption param = new ExtendedItemCustomOption();
		param.id = entity.id;
		param.code = entity.code;
		param.name = entity.nameL.getValue();
		param.nameL0 = entity.nameL0;
		param.nameL1 = entity.nameL1;
		param.nameL2 = entity.nameL2;
		param.extendedItemCustomId = entity.extendedItemCustomId;

		return param;
	}

	/*
	 * API for searching Custom Extended Item Options.
	 */
	public with sharing class SearchOptionApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			ExtendedItemCustomOptionSearchCondition param = (ExtendedItemCustomOptionSearchCondition) req.getParam(ExtendedItemCustomOptionSearchCondition.class );
			param.validate();
			ExtendedItemCustomRepository.OptionSearchFilter filter = createSearchFilter(param);
			// Retrieve one extra data to checks whether there is more data than is necessary or not
			List<ExtendedItemCustomOptionEntity> entityList = new ExtendedItemCustomService().getExtendedItemCustomOptionEntityList(filter, OPTION_RESULT_LIMIT + 1);

			List<ExtendedItemCustomOption> records = new List<ExtendedItemCustomOption>();
			Integer numberOfRecordsToReturn = entityList.size() > OPTION_RESULT_LIMIT ? OPTION_RESULT_LIMIT: entityList.size();
			for (Integer i = 0; i < numberOfRecordsToReturn; i++) {
				records.add(createExtendedItemCustomOptionResponse(entityList.get(i)));
			}

			ExtendedItemCustomOptionSearchResult res = new ExtendedItemCustomOptionSearchResult();
			res.records = records;
			res.hasMore = entityList.size() > OPTION_RESULT_LIMIT;     // If entity list is the same, that means it has more data.
			return res;
		}

		/*
		 * Create SearchFilter for searching Custom Extended Item
		 */
		private ExtendedItemCustomRepository.OptionSearchFilter createSearchFilter(ExtendedItemCustomOptionSearchCondition param) {
			ExtendedItemCustomRepository.OptionSearchFilter filter = new ExtendedItemCustomRepository.OptionSearchFilter();
			if (String.isNotBlank(param.id)) {
				filter.ids = new Set<Id>{param.id};
			}
			if (String.isNotBlank(param.extendedItemCustomId)) {
				filter.extendedItemCustomIds = new Set<Id>{param.extendedItemCustomId};
			}
			if (String.isNotBlank(param.code)) {
				filter.codes = new Set<String>{param.code};
			}
			if (String.isNotBlank(param.query)) {
				filter.nameOrCodeQuery = param.query;
			}
			return filter;
		}
	}

	public class GetRecentlyUsedOptionListParam implements RemoteApi.RequestParam {
		public String extendedItemCustomId;
		public String extendedItemLookupId;
		public String employeeBaseId;

		/*
		 * Validate that the request params are valid
		 */
		public void validate() {
			ExtendedItemCustomResource.verifyValidIdValue(this.extendedItemCustomId, true, 'extendedItemCustomId');
			ExtendedItemCustomResource.verifyValidIdValue(this.extendedItemLookupId, true, 'extendedItemLookupId');
			ExtendedItemCustomResource.verifyValidIdValue(this.employeeBaseId, true, 'employeeBaseId');
		}
	}

	public with sharing class GetRecentlyUsedOptionListApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			GetRecentlyUsedOptionListParam param = (GetRecentlyUsedOptionListParam) req.getParam(GetRecentlyUsedOptionListParam.class );
			param.validate();

			List<ExtendedItemCustomOptionEntity> recentlyUsedOptionList = new ExpRecentlyUsedService()
					.getRecentlyUsedExtendedItemCustomOptionList(param.employeeBaseId, param.extendedItemLookupId, param.extendedItemCustomId);

			List<ExtendedItemCustomOption> records = new List<ExtendedItemCustomOption>();
			for (ExtendedItemCustomOptionEntity entity: recentlyUsedOptionList) {
				records.add(createExtendedItemCustomOptionResponse(entity));
			}

			ExtendedItemCustomOptionSearchResult res = new ExtendedItemCustomOptionSearchResult();
			res.records = records;
			res.hasMore = false;
			return res;
		}
	}

	/*
	 * Check and validate if the given idString is of valid Salesforce ID.
	 * And throw {@code ParameterException} if the validation criteria is NOT met.
	 *
	 * @param idString String to validate
	 * @param isRequired if set to True, will throw {@code ParameterException} if the String is empty or null
	 * @param fieldName The name of the field to include in the exception message.
	 *
	 * @throws App.ParameterException
	 */
	private static void verifyValidIdValue(String idString, Boolean isRequired, String fieldName) {
		if (String.isNotBlank(idString)) {
			try {
				System.Id.valueOf(idString);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{fieldName}));
			}
		} else {
			if (isRequired == True) {
				throw new App.ParameterException(MESSAGE.Com_Err_Specify(new List<String>{fieldName}));
			}
		}
	}
}