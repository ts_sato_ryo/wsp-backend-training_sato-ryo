/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Batch Class for Exporting Journal
 * 
 * @group Expense
 */
public with sharing class ExpJournalBatch implements Database.Batchable<sObject> {

	/*
	 * Get the target Reports which should be exported to the Journal
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {
		// 1. If a report has Authorized Time set => Have been approved by Accounting Clerk
		// 2. If a report has ExportedTime set => Have been exported to Journal
		// If Authorized Time is set AND no Exported Time => Target to Export to Journal
		String query = 'SELECT Id, ExpReportId__c, AuthorizedTime__c, ExportedTime__c FROM ExpReportRequest__c' +
				' WHERE AuthorizedTime__c != null AND ExportedTime__c = null';

		return Database.getQueryLocator(query);
	}

	/*
	 * Export the Reports which have been Authorized but not yet Exported.
	 * IMPORTANT: Due to the amount of data being processed, limit the size of the batch
	 * using the SCOPE.
	 */
	public void execute(Database.BatchableContext bc, List<ExpReportRequest__c> reportRequestList) {
		List<Id> confirmedReportIdList = new List<Id>();
		for (ExpReportRequest__c reportRequest : reportRequestList) {
			confirmedReportIdList.add(reportRequest.ExpReportId__c);
		}
		ExpJournalService service = new ExpJournalService();
		service.exportConfirmedReportsToJournal(confirmedReportIdList);
	}

	/*
	 * Gets invoked when the batch job finishes. Place any clean up code in this method.
	 */
	public void finish(Database.BatchableContext bc) {

	}
}