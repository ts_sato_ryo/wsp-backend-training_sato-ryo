/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 拡張項目のサービスクラス
 */
public with sharing class ExtendedItemService implements Service {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 拡張項目のリポジトリ */
	private ExtendedItemRepository extendedItemRepo = new ExtendedItemRepository();

	/** Extended Item info */
	public class ExtendedItemInfo {
		/** Name */
		public AppMultiString nameL;
		/** Name */
		public String name;
		/** Input Type */
		public String inputType;
		/** Limit Length */
		public Integer limitLength;
		/** Required Flag */
		public Boolean isRequired;
		/** Default value of text */
		public String defaultValueText;
		/** Picklist */
		public List<ExtendedItemEntity.PicklistOption> picklist;
		/** Extended Item Custom sObject ID */
		public String extendedItemCustomId;
		/** Description */
		public string description;
	}

	/*
	 * Creates and returns a shallow clone of ExtenedItemInfo - only primitive types are cloned and
	 * deep cloning is NOT performed on the Objects
	 * @param info Source Extended Item Info
	 * @return New shallow copy of Extended Item Info
	 */
	public static ExtendedItemInfo shallowCloneExtendedItemInfo(ExtendedItemInfo info) {
		if (info == null) {
			return null;
		}
		ExtendedItemInfo newInfo = new ExtendedItemInfo();
		newInfo.nameL = info.nameL;
		newInfo.name = info.name;
		newInfo.inputType = info.inputType;
		newInfo.limitLength = info.limitLength;
		newInfo.isRequired = info.isRequired;
		newInfo.defaultValueText = info.defaultValueText;
		newInfo.picklist = info.picklist;
		newInfo.extendedItemCustomId = info.extendedItemCustomId;
		newInfo.description = info.description;
		return newInfo;
	}

	/**
	 * 拡張項目を保存する
	 * @param item 保存する拡張項目
	 * @return 保存した拡張項目のID
	 */
	public Id saveExtendedItem(ExtendedItemEntity item) {

		// 拡張項目のバリデーションを行う
		validateExtendedItem(item);

		// エンティティにユニークキーを設定する
		setUniqKey(item);

		// 拡張項目を保存する
		Repository.SaveResult saveResult = extendedItemRepo.saveEntity(item);

		return saveResult.details[0].id;
	}

	/**
	 * 拡張項目のバリデーションを実行する
	 * @param item 検証対象の拡張項目
	 * @throws App.ParameterException 不正な値が設定されている場合
	 */
	private void validateExtendedItem(ExtendedItemEntity item) {

		// 拡張項目エンティティの検証
		Validator.Result validResult = new ComConfigValidator.ExtendedItemValidator(item).validate();
		if (!validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		ExtendedItemEntity savedItem = extendedItemRepo.getEntityByCode(item.companyId, item.code);
		if (savedItem != null) {
			// コードが会社内で重複していないかを確認する
			if (item.id != savedItem.id) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
			}

			// 入力タイプが変更されている場合 When the record type has been changed
			if (item.inputType != savedItem.inputType) {
				// 関連するレコードが存在する場合はエラー If related records exist, exception is thrown
				if (existsRelatedRecords(item.id)) {
					throw new App.IllegalStateException(MESSAGE.Com_Err_CannotChangeReference);
				}
			} else if (isChangedExtendedItemValues(item, savedItem)) {
				// When the picklist or the related ExtendedItemCustomId has been changed OR Text limit Length is decreased
				// AND related transaction records exist, exception is thrown
				if (existsRelatedTransactionRecords(item.id)) {
					throw new App.IllegalStateException(MESSAGE.Com_Err_CannotChangeReference);
				}
			}
		}
	}

	/*
	 * Check if the following fields of the Extended Item have been changed
	 * 1. For PICKLIST type
	 *    1.1 List of available picklist values
	 * 2. For Lookup Type
	 *    2.1 the ID of linked extendedItemCustomId
	 * 3. For Text Type
	 *    3.1 Limit Length value is smaller than existing (bigger doesn't matter since it doesn't affect the validation)
	 *
	 * @param newItem entity representing the new values
	 * @param existingItem entity representing the existing values in the DB
	 *
	 * @returns true if any of the fields specified above are changed.
	 */
	private static boolean isChangedExtendedItemValues(ExtendedItemEntity newItem, ExtendedItemEntity existingItem) {
		if (newItem.inputType == ExtendedItemInputType.TEXT) {
			// Only if the limit is smaller. It doesn't have any impact on the existing if it's increased.
			return (newItem.limitLength < existingItem.limitLength);
		} else if (newItem.inputType == ExtendedItemInputType.PICKLIST) {
			return (String.valueOf(newItem.pickList) != String.valueOf(existingItem.pickList));
		} else if (newItem.inputType == ExtendedItemInputType.CUSTOM) {
			return (newItem.extendedItemCustomId != existingItem.extendedItemCustomId);
		}
		return false;
	}

	/**
	 * 拡張項目にユニークキーを設定する
	 * @param ユニークキーを設定する拡張項目
	 */
	private void setUniqKey(ExtendedItemEntity item) {
		CompanyEntity company = new CompanyRepository().getEntity(item.companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		item.uniqKey = item.createUniqKey(company.code);
	}

	/**
	 * 拡張項目を検索して返却する
	 * @param companyId 会社ID、nullの場合は検索条件に含まない
	 * @param itemId 拡張項目ID、nullの場合は検索条件に含まない
	 */
	public List<ExtendedItemEntity> searchExtendedItems(Id companyId, Id itemId, ExtendedItemInputType inputType) {

		// 拡張項目を検索する
		ExtendedItemRepository.SearchFilter itemFilter = new ExtendedItemRepository.SearchFilter();
		if (companyId != null) {
			itemFilter.companyIds = new Set<Id>{companyId};
		}
		if (itemId != null) {
			itemFilter.ids = new Set<Id>{itemId};
		}
		List<ExtendedItemEntity> itemList = new ExtendedItemRepository().searchEntityList(itemFilter);

		return itemList;
	}

	/**
	 * 拡張項目を削除する。
	 * NOTE:論理削除型のマスタは、レコードが参照されている場合は論理削除する方針だが、検討が十分ではないため現段階では実装せず、一律物理削除とする。
	 * @param itemId 削除対象のID
	 * @throw DmlException 参照しているレコードが存在する場合、削除対象が既に削除されている場合(System.StatusCode.ENTITY_IS_DELETED)
	 */
	public void deleteExtendedItem(Id itemId) {

		// 関連するレコードが存在する場合はエラー If related records exist, exception is thrown
		if (existsRelatedRecords(itemId)) {
			throw new App.IllegalStateException(MESSAGE.Com_Err_CannotDeleteReference);
		}

		new LogicalDeleteService(extendedItemRepo).deleteEntity(itemId);
	}

	/**
	 * 関連するレコードが存在するかチェックする Check if related records exist
	 * @param itemId チェック対象の拡張項目ID Extended item ID
	 * @return 関連するレコードが存在する場合はtrue、存在しない場合はfalse If exists return true, otherwise return false
	 */
	private Boolean existsRelatedRecords(Id itemId) {

		// 対象の拡張項目が紐づいている経費申請タイプを取得 Search for expense report type item whose target extended item is associated
		List<ExpReportTypeEntity> expReportTypeList = new ExpReportTypeRepository().getEntityListByExtendedItemId(new List<Id>{itemId});

		// 経費申請タイプが存在する場合はtrue If related records exist, return true
		if (expReportTypeList != null && !expReportTypeList.isEmpty()) {
			return true;
		}

		// 対象の拡張項目が紐づいている費目を検索 Search for expense type whose target extended item is associated
		List<ExpTypeEntity> expTypeList = new ExpTypeRepository().getEntityListByExtendedItemId(new List<Id>{itemId});

		// 費目が存在する場合はtrue If related records exist, return true
		if (expTypeList != null && !expTypeList.isEmpty()) {
			return true;
		}

		return existsRelatedTransactionRecords(itemId);
	}

	/**
	 * 関連するトランザクションレコードが存在するかチェックする Check if related transaction records exist
	 * @param itemId チェック対象の拡張項目ID Extended item ID
	 * @return 関連するレコードが存在する場合はtrue、存在しない場合はfalse If exists return true, otherwise return false
	 */
	private Boolean existsRelatedTransactionRecords(Id itemId) {

		// 対象の拡張項目が紐づいている経費精算を取得 Search for expense report item whose target extended item is associated
		List<ExpReportEntity> expReportList = new ExpReportRepository().getEntityListByExtendedItemId(itemId);

		// 経費精算が存在する場合はtrue If related records exist, return true
		if (expReportList != null && !expReportList.isEmpty()) {
			return true;
		}

		// 対象の拡張項目が紐づいている事前申請を取得 Search for expense request item whose target extended item is associated
		List<ExpRequestEntity> expRequestList = new ExpRequestRepository().getEntityListByExtendedItemId(itemId);

		// 事前申請が存在する場合はtrue If related records exist, return true
		if (expRequestList != null && !expRequestList.isEmpty()) {
			return true;
		}

		// 対象の拡張項目が紐づいている経費内訳を取得 Search for expense record item whose target extended item is associated
		List<ExpRecordItemEntity> recordItemList = new ExpRecordRepository().getRecordItemEntityListByExtendedItemId(itemId);

		// 経費内訳が存在する場合はtrue If related records exist, return true
		if (recordItemList != null && !recordItemList.isEmpty()) {
			return true;
		}

		// 対象の拡張項目が紐づいている事前申請内訳を取得 Search for expense request record item whose target extended item is associated
		List<ExpRequestRecordItemEntity> requestRecordItemList = new ExpRequestRecordRepository().getRecordItemEntityListByExtendedItemId(itemId);

		// 事前申請内訳が存在する場合はtrue If related records exist, return true
		if (requestRecordItemList != null && !requestRecordItemList.isEmpty()) {
			return true;
		}

		return false;
	}

	/**
	 * 拡張項目情報を指定する Get Extended Item info
	 * @param itemIds 取得対象の拡張項目ID Target IDs
	 * @return 取得した拡張項目情報のマップ(キー:拡張項目ID 値:拡張項目情報) Map of Extended Item info(key:Extended Item ID, value:Extended Item info)
	 */
	public Map<Id, ExtendedItemInfo> getExtendedItemInfo(List<Id> itemIds) {
		Map<Id, ExtendedItemInfo> extendedItemInfoMap = new Map<Id, ExtendedItemInfo>();

		if (itemIds != null && !itemIds.isEmpty()) {
			List<ExtendedItemEntity> extendedItemList = new ExtendedItemRepository().getEntityList(itemIds);

			for (ExtendedItemEntity item : extendedItemList) {
				ExtendedItemInfo info = new ExtendedItemInfo();
				info.nameL = item.nameL;
				info.name = item.nameL.getValue();
				info.inputType = ExtendedItemInputType.getValue(item.inputType);
				info.limitLength = item.limitLength;
				info.defaultValueText = item.defaultValueText;
				info.picklist = item.picklist;
				if (info.picklist != null) {
					for (ExtendedItemEntity.PicklistOption option : info.picklist) {
						option.labelL = new AppMultiString(option.label_L0, option.label_L1, option.label_L2);
						option.label = option.labelL.getValue();

					}
				}
				info.extendedItemCustomId = item.extendedItemCustomId;
				info.description = item.descriptionL.getValue();

				extendedItemInfoMap.put(item.id, info);
			}
		}

		return extendedItemInfoMap;
	}
}