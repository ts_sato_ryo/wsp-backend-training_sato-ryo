/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 工数系APIの権限チェック機能を提供するクラス
 * 工数系Resourceクラス以外からは呼び出さないでください
 */
public with sharing class TimeResourcePermission {

    /** API実行に必要な権限 */
	public enum Permission {
		/** 工数実績表示(代理) */
		VIEW_TIME_TRACK_BY_DELEGATE
	}

	/** 社員の権限サービス */
	private EmployeePermissionService permissionService;

	/** ログインユーザの社員 */
	private EmployeeBaseEntity loginEmployee {
		get;
		set;
	}

	/**
	 * コンストラクタ
	 */
	public TimeResourcePermission() {
		this.permissionService = new EmployeePermissionService();
		// ログインユーザ社員を取得しておく
		try {
			this.loginEmployee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
		} catch (App.IllegalStateException e) {
			// システム管理者の場合もあるので何もしない
		} catch (App.RecordNotFoundException ex) {
			// システム管理者の場合もあるので何もしない
		}
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @param targetEmployeeId 処理対象(データ取得対象、申請対象)の社員ベースID
	 *        代理で実行するか否かの判定に使用する。処理対象がログインユーザの社員の場合はnull
	 */
	public void hasExecutePermission(
			List<TimeResourcePermission.Permission> requiredPermissionList, String targetEmployeeId) {

		if (isDelegateEmployee(targetEmployeeId)) {
			// 代理で実行できることをチェックする
			hasPermissionByDelegate(requiredPermissionList);
		} else {
			// 本人が実行できることをチェックする
			// hasPermissionByEmployee(requiredPermissionList);
		}
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員履歴IDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @param employeeHistoryId 処理対象(データ取得対象、申請対象)の社員履歴ID
	 *        代理で実行するか否かの判定に使用する。処理対象がログインユーザの社員の場合はnull
	 */
	public void hasExecutePermissionByEmployeeHistoryId(
			List<TimeResourcePermission.Permission> requiredPermissionList, Id employeeHistoryId) {
		EmployeeRepository.SearchFilter filter = new EmployeeRepository.SearchFilter();
		filter.historyIds = new Set<Id>{employeeHistoryId};
		filter.includeRemoved = true;
		List<EmployeeBaseEntity> employeeList = new EmployeeRepository().searchEntity(filter);
		if (! employeeList.isEmpty()) {
			hasExecutePermission(requiredPermissionList, employeeList[0].id);
		}
	}

	/**
	 * 指定された権限(代理)を持っているかをチェックする
	 * @param requiredPermissionList チェック対象の代理権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasPermissionByDelegate(List<TimeResourcePermission.Permission> requiredPermissionList) {
		List<TimeResourcePermission.Permission> errorPermissionList = new List<TimeResourcePermission.Permission>();
		for (Permission requiredPermission : requiredPermissionList) {
			if (requiredPermission == Permission.VIEW_TIME_TRACK_BY_DELEGATE) {
				if (! permissionService.hasViewTimeTrackByDelegate) {
					errorPermissionList.add(Permission.VIEW_TIME_TRACK_BY_DELEGATE);
				}
			}
		}

		if (! errorPermissionList.isEmpty()) {
			// デバッグ用にログを出力しておく
			System.debug('--- API実行に不足している権限=' + errorPermissionList);
			throw new App.NoPermissionException(ComMessage.msg().Com_Err_NoApiPermission);
		}
	}

	/**
	 * 指定された権限(本人)を持っているかをチェックする
	 * @param requiredPermissionList チェック対象の代理権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
    /* TODO 本人の権限判定が追加されたら実装する
	private void hasPermissionByEmployee(List<TimeResourcePermission.Permission> requiredPermissionList) {
	}*/

	/**
	 * ログインユーザが代理社員であるかどうかを確認する
	 * @param empBaseId 処理対象の社員ベースID、nullの場合はログインユーザの社員データが処理対象とみなす
	 * @return 代理社員の場合はtrue、そうでない場合はfalse
	 */
	private Boolean isDelegateEmployee(String empBaseId) {

		// ログインユーザに社員が設定されていない場合は(システム管理者ユーザなど)
		// 代理社員で実行するものとみなす
		if (this.loginEmployee == null) {
			return true;
		}

		// empBaseIdが空文字、またはnullの場合は代理ではないとする
		if (String.isBlank(empBaseId)) {
			return false;
		}

		if (this.loginEmployee.id != empBaseId) {
			return true;
		}
		return false;
	}
}