/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * JobTypeWorkCategoryRepositorのテスト
 */
@isTest
private class JobTypeWorkCategoryRepositoryTest {

private class RepoTestData {
		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public List<ComCompany__c> companyObjs;
		/** ジョブタイプオブジェクト */
		public List<ComJobType__c> jobTypeObjs;
		/** 作業分類オブジェクト */
		public List<TimeWorkCategory__c> workCategoryObjs;
		/** ジョブタイプ別作業分類オブジェクト */
		public List<TimeJobTypeWorkCategory__c> jotTypeWorkCategoryObjs;

		/** コンストラクタ　*/
		public RepoTestData(Integer jobTypeSize, Integer workCategorySize) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObjs = new List<ComCompany__c> {
					ComTestDataUtility.createCompany('Company1', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company2', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company3', this.countryObj.Id)
			};

			jobTypeObjs = ComTestDataUtility.createJobTypes('Test Job Type', this.companyObjs[0].Id, jobTypeSize);
			workCategoryObjs = ComTestDataUtility.createWorkCategories('Test Work Category', this.companyObjs[0].Id, workCategorySize);

			jotTypeWorkCategoryObjs = new List<TimeJobTypeWorkCategory__c>();
			for (ComJobType__c jobTypeObj : jobTypeObjs) {
				for (TimeWorkCategory__c workCategoryObj : workCategoryObjs) {
					TimeJobTypeWorkCategory__c jw = new TimeJobTypeWorkCategory__c(
							JobTypeId__c = jobTypeObj.Id, WorkCategoryId__c = workCategoryObj.Id);
					jotTypeWorkCategoryObjs.add(jw);
				}
			}
			insert jotTypeWorkCategoryObjs;
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityTest() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		TimeJobTypeWorkCategory__c target = getSObject(testData.jotTypeWorkCategoryObjs[4].Id);
		JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryRepository().getEntity(target.Id);

		// 各項目の値が正しく取得できているかのチェックはsearchEntityListのテストで行う
		TestUtil.assertNotNull(entity);
		assertEntity(target, entity);
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのレコードが存在しない場合、nullが返却されることを確認する
	 */
	@isTest static void getEntityTestNotFound() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		Id targetId = UserInfo.getUserId();
		JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryRepository().getEntity(targetId);

		System.assertEquals(null, entity);
	}

	/**
	 * getEntityListのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityListTest() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		TimeJobTypeWorkCategory__c target1 = getSObject(testData.jotTypeWorkCategoryObjs[1].Id);
		TimeJobTypeWorkCategory__c target2 = getSObject(testData.jotTypeWorkCategoryObjs[5].Id);
		List<JobTypeWorkCategoryEntity> entities =
				new JobTypeWorkCategoryRepository().getEntityList(new List<Id>{target2.Id, target1.Id});

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * getEntityListByJobTypeのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityListByJobTypeTest() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		TimeJobTypeWorkCategory__c target1 = getSObject(testData.jotTypeWorkCategoryObjs[0].Id);
		TimeJobTypeWorkCategory__c target2 = getSObject(testData.jotTypeWorkCategoryObjs[1].Id);
		List<JobTypeWorkCategoryEntity> entities =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(target1.JobTypeId__c);

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * searchEntityListのテスト
	 * 全ての項目の値が正しく取得できていることを確認する
	 */
	@isTest static void searchEntityListTestAllField() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		TimeJobTypeWorkCategory__c targetObj = getSObject(testData.jotTypeWorkCategoryObjs[1].Id);

		JobTypeWorkCategoryRepository.SearchFilter filter = new JobTypeWorkCategoryRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		List<JobTypeWorkCategoryEntity> entities = new JobTypeWorkCategoryRepository().searchEntityList(filter);

		System.assertEquals(1, entities.size());
		assertEntity(targetObj, entities[0]);
	}

	/**
	 * searchEntityListのテスト
	 * 指定した条件で取得できることを確認する
	 */
	@isTest static void searchEntityListTestFilter() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		// 同じジョブタイプ
		TimeJobTypeWorkCategory__c targetObj1 = getSObject(testData.jotTypeWorkCategoryObjs[0].Id);
		TimeJobTypeWorkCategory__c targetObj2 = getSObject(testData.jotTypeWorkCategoryObjs[1].Id);

		JobTypeWorkCategoryRepository.SearchFilter filter;
		JobTypeWorkCategoryRepository repo = new JobTypeWorkCategoryRepository();
		List<JobTypeWorkCategoryEntity> resEntities;

		// TEST1: IDで検索
		filter = new JobTypeWorkCategoryRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj2.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// TEST2: ジョブタイプIDで検索、検索結果は作業分類の表示順のはず
		TimeWorkCategory__c work1 = testData.workCategoryObjs[1];
		work1.Order__c = 1;
		TimeWorkCategory__c work2 = testData.workCategoryObjs[0];
		work2.Order__c = 2;
		update new List<TimeWorkCategory__c>{work1, work2};
		targetObj1.WorkCategoryId__c = work2.Id;
		targetObj2.WorkCategoryId__c = work1.Id;
		update new List<TimeJobTypeWorkCategory__c>{targetObj1, targetObj2};
		targetObj1.WorkCategoryId__c = work2.Id;
		filter = new JobTypeWorkCategoryRepository.SearchFilter();
		filter.jobTypeIds = new Set<Id>{targetObj1.JobTypeId__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(2, resEntities.size());
		System.assertEquals(targetObj2.JobTypeId__c, resEntities[0].jobTypeId);
		System.assertEquals(targetObj1.JobTypeId__c, resEntities[1].jobTypeId);
	}

	/**
	 * saveEntityのテスト
	 * 新規保存できることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		// テストデータのジョブタイプ別作業分類を削除しておく
		delete testData.jotTypeWorkCategoryObjs;

		JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryEntity();
		entity.jobTypeId = testData.jobTypeObjs[2].Id;
		entity.workCategoryId = testData.workCategoryObjs[1].Id;

		Repository.SaveResult result = new JobTypeWorkCategoryRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		TimeJobTypeWorkCategory__c resObj = getSObject(result.details[0].id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityのテスト
	 * 既存のエンティティを保存できることを確認する
	 */
	@isTest static void saveEntityTestUpdate() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		TimeJobTypeWorkCategory__c target = testData.jotTypeWorkCategoryObjs[0];
		JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryEntity();
		entity.setId(target.Id);
		entity.jobTypeId = testData.jobTypeObjs[2].Id;
		entity.workCategoryId = testData.workCategoryObjs[1].Id;

		Repository.SaveResult result = new JobTypeWorkCategoryRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		TimeJobTypeWorkCategory__c resObj = getSObject(target.Id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityListのテスト
	 * 新規、既存のエンティティが保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		final Integer jobTypeSize = 3;
		final Integer workCategorySize = 2;
		RepoTestData testData = new RepoTestData(jobTypeSize, workCategorySize);

		// 既存
		TimeJobTypeWorkCategory__c orgObj = testData.jotTypeWorkCategoryObjs[0];
		JobTypeWorkCategoryEntity entityUd = new JobTypeWorkCategoryEntity();
		entityUd.setId(orgObj.Id);
		entityUd.jobTypeId = testData.jobTypeObjs[2].Id;
		entityUd.workCategoryId = testData.workCategoryObjs[1].Id;

		// 新規
		JobTypeWorkCategoryEntity entityNew = new JobTypeWorkCategoryEntity();
		entityNew.jobTypeId = testData.jobTypeObjs[2].Id;
		entityNew.workCategoryId = testData.workCategoryObjs[1].Id;

		Repository.SaveResult result =
				new JobTypeWorkCategoryRepository().saveEntityList(new List<JobTypeWorkCategoryEntity>{entityUd, entityNew});

		System.assertEquals(true, result.isSuccessAll);
		TimeJobTypeWorkCategory__c resObjUd = getSObject(orgObj.Id);
		assertSObject(entityUd, resObjUd);
		TimeJobTypeWorkCategory__c resObjNew = getSObject(result.details[1].id);
		assertSObject(entityNew, resObjNew);
	}

	/**
	 * 取得したエンティティが期待値通りであることを確認する
	 */
	private static void assertEntity(TimeJobTypeWorkCategory__c expObj, JobTypeWorkCategoryEntity actEntity) {
		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.JobTypeId__c, actEntity.jobTypeId);
		System.assertEquals(expObj.WorkCategoryId__c, actEntity.workCategoryId);

		System.assertEquals(expObj.JobTypeId__r.Name_L0__c, actEntity.jobType.nameL.valueL0);
		System.assertEquals(expObj.JobTypeId__r.Name_L1__c, actEntity.jobType.nameL.valueL1);
		System.assertEquals(expObj.JobTypeId__r.Name_L2__c, actEntity.jobType.nameL.valueL2);

		System.assertEquals(expObj.WorkCategoryId__r.Name_L0__c, actEntity.workCategory.nameL.valueL0);
		System.assertEquals(expObj.WorkCategoryId__r.Name_L1__c, actEntity.workCategory.nameL.valueL1);
		System.assertEquals(expObj.WorkCategoryId__r.Name_L2__c, actEntity.workCategory.nameL.valueL2);
		System.assertEquals(expObj.WorkCategoryId__r.Order__c, actEntity.workCategory.order);
		System.assertEquals(expObj.WorkCategoryId__r.ValidFrom__c, actEntity.workCategory.validFrom.getDate());
		System.assertEquals(expObj.WorkCategoryId__r.ValidTo__c, actEntity.workCategory.validTo.getDate());
	}

	/**
	 * 保存したSObjectが期待値通りであることを確認する
	 */
	private static void assertSObject(JobTypeWorkCategoryEntity expEntity, TimeJobTypeWorkCategory__c actObj) {
		System.assertEquals(expEntity.jobTypeId, actObj.JobTypeId__c);
		System.assertEquals(expEntity.workCategoryId, actObj.WorkCategoryId__c);
	}

	/**
	 * 指定したIDのTimeWorkCategory__cを取得する
	 */
	private static TimeJobTypeWorkCategory__c getSObject(Id id) {
		return [
				SELECT Id, Name, JobTypeId__c, WorkCategoryId__c,
					JobTypeId__r.Name_L0__c, JobTypeId__r.Name_L1__c, JobTypeId__r.Name_L2__c,
					WorkCategoryId__r.Name_L0__c, WorkCategoryId__r.Name_L1__c, WorkCategoryId__r.Name_L2__c,
					WorkCategoryId__r.ValidFrom__c, WorkCategoryId__r.ValidTo__c, WorkCategoryId__r.Order__c
				FROM TimeJobTypeWorkCategory__c
				WHERE Id = :id];
	}
}