/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpAccountingPeriodEntity
 */
@isTest
private class ExpAccountingPeriodEntityTest {

	/**
	 * Test Data Class
	 */
	private class TestData extends TestData.TestDataEntity {
		/** Accounting Period Data */
		public List<ExpAccountingPeriodEntity> accountingPeriods = new List<ExpAccountingPeriodEntity>();

		/**
		 * Constructor
		 */
		public TestData() {
			super();
			// Create entity
			ComTestDataUtility.createExpAccountingPeriods('TestCode', this.company.id, 3);
			this.accountingPeriods = (new ExpAccountingPeriodRepository()).getEntityList(null);
		}

		/**
		 * Create Accounting Period object
		 * @code Accounting Period Code
		 * @size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpAccountingPeriodEntity> createExpAccountingPeriods(String code, Integer size) {
			return createExpAccountingPeriods(code, this.company.id, size);
		}

		/**
		 * Create Accounting Period object
		 * @code Accounting Period Code
		 * @companyId Company ID
		 * @size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpAccountingPeriodEntity> createExpAccountingPeriods(String code, Id companyId, Integer size) {
			ComTestDataUtility.createExpAccountingPeriods(code, companyId, size);
			return (new ExpAccountingPeriodRepository()).getEntityList(null);
		}
	}

	/**
	 * Confirm unique key is created properly
	 */
	@isTest static void createUniqKeyTest() {
		ExpAccountingPeriodEntity entity = new ExpAccountingPeriodEntity();

		// When rate code and company code are not empty, key would be created.
		entity.code = 'TestCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// When rate code is empty, exception would occur.
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occurred');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// When company code is empty, exception would occur.
		entity.code = 'TestCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occurred');
		} catch(App.ParameterException e) {
			// OK
		}
	}
}