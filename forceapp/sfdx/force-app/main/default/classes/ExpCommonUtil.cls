/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Common Untility Class For Expense Team
 */
public class ExpCommonUtil {

	/**
	 * Check Object is not null
	 * @param name String Name
	 * @param value Object value
	 * @throws APP.ParameterException()
	 */
	public static void validateNotNull(String name, Object value) {
		if (value == null) {
			APP.ParameterException e = new App.ParameterException();
			e.setMessageIsBlank(name);

			throw e;
		}
	}

	/**
	 * Check String is not null or blank
	 * @param name String Name
	 * @param value Object value
	 * @throws APP.ParameterException()
	 */
	public static void validateStringIsNotBlank(String name, String value) {
		if (String.isBlank(value)) {
			APP.ParameterException e = new App.ParameterException();
			e.setMessageIsBlank(name);

			throw e;
		}
	}

	/**
	 * Check Id is not null, blank and valid Id
	 * @param name String Name
	 * @param value Object value
	 * @param isRequired Boolean vlaue to indicate check null and only throw error when required
	 * @throws APP.ParameterException()
	 */
	public static void validateId(String name, String value, Boolean isRequired) {
		if (isRequired) {
			validateStringIsNotBlank(name, value);
		}
		if (String.isNotBlank(value)) {
			try {
				Id.valueOf(value);
			} catch (Exception e) {
				throw new App.ParameterException(name, value);
			}
		}
	}

	/**
	 * Check Date is not null, blank and valid Date
	 * @param name String Name
	 * @param value Object value
	 * @throws APP.ParameterException()
	 */
	public static void validateDate(String name, String value) {
		validateDate(name, value, true);
	}

	/**
	 * Check Date is valid Date
	 * @param name String Name
	 * @param value Object value
	 * @param isRequired if set to True, it will throws exception if the value isn't specified
	 * @throws APP.ParameterException()
	 */
	public static void validateDate(String name, String value, Boolean isRequired) {
		if (isRequired == true) {
			validateStringIsNotBlank(name, value);
		}
		try {
			AppDate.parse(value);
		} catch (Exception e) {
			throw new App.ParameterException(name, value);
		}
	}

	/**
	 * Check UsedIn is not null, blank and valid Date
	 * @param name String Name
	 * @param value Object value
	 * @throws APP.ParameterException()
	 */
	public static void validateUsedIn(String name, String value) {
		if (ExpService.CALL_TYPE_MAP.get(value) == null) {
			APP.ParameterException e = new App.ParameterException();
			e.setMessage(ComMessage.msg().Com_Err_InvalidValue(new List<String>{name}));

			throw e;
		}
	}

	/**
	 * Check Param is not null, blank and valid length
	 * @param name String Name
	 * @param value Object value
	 * @param expectedLength Integer value of expected param length
	 * @throws APP.ParameterException()
	 */
	public static void validateStringLength(String name, String value, Integer expectedLength){
		if (String.isNotBlank(value)) {
			if (value.length() > expectedLength) {
				APP.ParameterException e = new App.ParameterException();
				e.setMessageStringLengthOver(name, expectedLength);

				throw e;
			}
		}
	}

	/**
	 * Check Ids in a list is not null, blank and valid length
	 * @param name String Name
	 * @param valueList List of Object value
	 * @param isRequired Boolean vlaue to indicate check null and only throw error when required
	 * @throws APP.ParameterException()
	 */
	public static void validateIdList(String name, List<String> valueList, Boolean isRequired) {
		if (valueList == null || valueList.isEmpty()) {
			if (isRequired) {
				APP.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank(name);

				throw e;
			}
		} else {
			for (String stringValue : valueList) {
				validateId(name, stringValue, isRequired);
			}
		}
	}

	/**
	 * Get EmployeeBaseEntity by using employee id
	 * @param empId Employee Id
	 * @return Emloyee Base Entity
	 */
	public static EmployeeBaseEntity getEmployeeBaseEntity(String empId) {
		if (String.isBlank(empId)) {
			// 社員IDが設定されていない場合、ログインユーザから社員情報を取得する
			return ExpCommonUtil.getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
		} else {
			return ExpCommonUtil.getEmployee(empId, AppDate.today());
		}
	}

	/**
	 * Get Employee By User Id
	 * @param userID employee Id
	 * @return Employee Base Entity
	 * @throws App.IllegalStateException 社員が見つからない場合、複数登録されている場合
	 */
	public static EmployeeBaseEntity getEmployeeByUserId(Id userId) {
		return getEmployeeByUserId(userID, AppDate.today());
	}

	/**
	 * 指定したユーザIDを持つ社員を取得する
	 * @param userId ユーザID
	 * @return 社員ベースエンティティ
	 * @throws App.IllegalStateException 社員が見つからない場合、複数登録されている場合
	 */
	public static EmployeeBaseEntity getEmployeeByUserId(Id userId, AppDate targetDate) {
		EmployeeService empService = new EmployeeService();
		return empService.getEmployeeByUserId(userId, targetDate);
	}

	/**
	 * 指定した社員IDの社員を取得する
	 * @param empId 社員ベースID
	 * @return 社員ベースエンティティ
	 * @throws App.IllegalStateException 社員が見つからない場合
	 */
	public static EmployeeBaseEntity getEmployee(Id empId, AppDate targetDate) {
		EmployeeRepository repo = new EmployeeRepository();
		EmployeeBaseEntity employee = repo.getEntity(empId, targetDate);
		if (employee == null) {
			// メッセージ：指定された社員が見つかりません。
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Employee})
					+ '(ID='+ empId + ')');
		}
		return employee;
	}

	/**
	 * 経費申請機能が利用できることを確認する関数（利用できないとエラーが返る）
	 */
	public static Boolean canUseExpense(){
		EmployeeBaseEntity empBase;
		// ユーザ情報を取得
		Id userId = UserInfo.getUserId();
		AppDate targetDate = AppDate.today();
		// ユーザに紐付く社員情報を取得
		empBase = getEmployeeByUserId(userId, targetDate);
		return canUseExpense(empBase);
	}

	/**
	 * 経費申請機能が利用できることを確認する関数（利用できないとエラーが返る）
	 * @param empBase 社員のベースエンティティ
	 * @return boolean 利用できる場合はtrueを返す
	 */
	public static Boolean canUseExpense(EmployeeBaseEntity empBase){
		// 経費機能の権限がない場合エラーを吐く / if not authorized to use exp function, throw error.
		if (!empBase.companyInfo.useExpense){
			throw new App.IllegalStateException(
				App.ERR_CODE_CANNOT_USE_EXPENSE,
				ComMessage.msg().Exp_Msg_CantUseExpense);
		}
		// 基準通貨がない場合、エラーを吐く / if no base currency, throw error.
		if (empBase.companyInfo.currencyId == null){
			throw new App.IllegalStateException(
				App.ERR_CODE_NO_BASE_CURRENCY,
				ComMessage.msg().Exp_Msg_NoBaseCurrencyForExpense);
		}
		return true;
	}

	/*
	 * Check if employee can use Expense Request module
	 * If the employee cannot use Expense Request module, throw error
	 * @param empBase Target employee entity
	 */
	public static Boolean checkCanUseExpenseRequest(EmployeeBaseEntity empBase) {
		if (empBase == null) {
			return canUseExpenseRequest();
		}
		return canUseExpenseRequest(empBase);
	}

	private static Boolean canUseExpenseRequest() {
		EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeByUserId(UserInfo.getUserId());

		return canUseExpenseRequest(empBase);
	}

	/**
	 * Checks if the function to use Expense Request (advance Expense Application) is enabled
	 * @param empBase Employee Base Entity
	 * @return True if the function is enabled
	 */
	private static Boolean canUseExpenseRequest(EmployeeBaseEntity empBase){
		// If not authorized to use expense request function, throw error.
		if (!empBase.companyInfo.useExpenseRequest){
			throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_USE_EXPENSE_REQUEST,
					ComMessage.msg().Exp_Msg_CantUseExpenseRequest);
		}
		// If no base currency, throw error.
		if (empBase.companyInfo.currencyId == null){
			throw new App.IllegalStateException(
					App.ERR_CODE_NO_BASE_CURRENCY,
					ComMessage.msg().Exp_Msg_NoBaseCurrencyForRequest);
		}
		return true;
	}

	/**
	 * Get company entity by company id from CompanyRepository
	 * @param companyId Id of Company
	 * @return Company Entity
	 */
	public static CompanyEntity getCompany(String companyId){
		CompanyRepository repo = new CompanyRepository();
		CompanyEntity company = repo.getEntity(companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません。
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Company})
					+ '(ID='+ companyId + ')');
		}
		return company;
	}

	/*
	 * Create a set of id from the given entity list.
	 * @param entityList Source of map
	 * @return Set of id
	 */
	public static Set<Id> getIds(List<Entity> entityList) {
		Set<Id> ids = new Set<Id>();
		for (Entity entity : entityList) {
			ids.add(entity.id);
		}
		return ids;
	}

	/*
	 * Share ExpRequestApproval with delegated approvers
	 * @param requestId ID of ExpRequest approval to be shared
	 * @param employeeBaseId Employee Base ID of applicant
	 */
	public List<AppShareEntity> createShareEntityList(Id requestId, Id employeeBaseId) {

		// Retrieve delegated approver setting entities
		List<DelegatedApproverSettingEntity> settingList =
				new DelegatedApproverSettingRepository().getExpRequestApproverList(employeeBaseId);

		// Retrieve delegated approver employee info
		List<Id> approverIdList = new List<Id>();
		for (DelegatedApproverSettingEntity setting : settingList) {
			approverIdList.add(setting.delegatedApproverBaseId);
		}
		Map<Id, EmployeeBaseEntity> approverMap = new EmployeeService().getBaseMap(approverIdList);

		List<Id> approverUserIdList = new List<Id>();
		for (DelegatedApproverSettingEntity setting : settingList) {
			if (approverMap.containsKey(setting.delegatedApproverBaseId)) {
				approverUserIdList.add(approverMap.get(setting.delegatedApproverBaseId).userId);
			}
		}

		// Share approval record with delegated approvers
		List<AppShareEntity> shareList = new List<AppShareEntity>();
		if (!approverIdList.isEmpty()) {
			for (Id userId : approverUserIdList) {
				AppShareEntity share = new AppShareEntity();
				share.parentId = requestId;
				share.userOrGroupId = userId;
				shareList.add(share);
			}
		}
		return shareList;
	}


}