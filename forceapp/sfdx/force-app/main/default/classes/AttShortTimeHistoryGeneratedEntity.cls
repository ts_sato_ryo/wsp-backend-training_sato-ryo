/**
 * TeamSpirit 短時間勤務設定(履歴)のエンティティクラス。
 *
 * 当クラスはツールによって自動生成しているため、手動で修正しても上書きされる可能性があります。
 * 業務ロジックを追加する場合は、サブクラスを作成してください。
 *
 * Since this class is automatically generated by the tool, it may be overwritten even if it is corrected manually.
 * If you want to add business logic, please create a subclass.
 */
public abstract with sharing class AttShortTimeHistoryGeneratedEntity extends ParentChildHistoryEntity {

	/** フィールド名とフィールドのマップ */
	private static final Map<String, Schema.SObjectField> SOBJECT_FIELD_MAP = Schema.SObjectType.AttShortTimeWorkSettingHistory__c.fields.getMap();

	/** TeamSpirit 短時間勤務設定(履歴) */
	protected AttShortTimeWorkSettingHistory__c sobj;

	/**
	 * デフォルトコンストラクタ
	 */
	public AttShortTimeHistoryGeneratedEntity() {
		super(new AttShortTimeWorkSettingHistory__c(), SOBJECT_FIELD_MAP);
		this.sobj = (AttShortTimeWorkSettingHistory__c)super.sobj;
	}

	/**
	 * コンストラクタ
	 */
	public AttShortTimeHistoryGeneratedEntity(AttShortTimeWorkSettingHistory__c sobj) {
		super(sobj, SOBJECT_FIELD_MAP);
		this.sobj = sobj;
	}

	/**
	 * 許可する早退時間の上限
	 * Allowable Time Of Early Leave
	 */
	public static final Schema.SObjectField FIELD_ALLOWABLE_TIME_OF_EARLY_LEAVE = AttShortTimeWorkSettingHistory__c.AllowableTimeOfEarlyLeave__c;
	public Integer allowableTimeOfEarlyLeave {
		get {
			return Integer.valueOf(getFieldValue(FIELD_ALLOWABLE_TIME_OF_EARLY_LEAVE));
		}
		set {
			setFieldValue(FIELD_ALLOWABLE_TIME_OF_EARLY_LEAVE, value);
		}
	}

	/**
	 * 許可する私用外出の上限
	 * Allowable Time Of Irregular Rest
	 */
	public static final Schema.SObjectField FIELD_ALLOWABLE_TIME_OF_IRREGULAR_REST = AttShortTimeWorkSettingHistory__c.AllowableTimeOfIrregularRest__c;
	public Integer allowableTimeOfIrregularRest {
		get {
			return Integer.valueOf(getFieldValue(FIELD_ALLOWABLE_TIME_OF_IRREGULAR_REST));
		}
		set {
			setFieldValue(FIELD_ALLOWABLE_TIME_OF_IRREGULAR_REST, value);
		}
	}

	/**
	 * 許可する遅刻時間の上限
	 * Allowable Time Of Late Arrival
	 */
	public static final Schema.SObjectField FIELD_ALLOWABLE_TIME_OF_LATE_ARRIVAL = AttShortTimeWorkSettingHistory__c.AllowableTimeOfLateArrival__c;
	public Integer allowableTimeOfLateArrival {
		get {
			return Integer.valueOf(getFieldValue(FIELD_ALLOWABLE_TIME_OF_LATE_ARRIVAL));
		}
		set {
			setFieldValue(FIELD_ALLOWABLE_TIME_OF_LATE_ARRIVAL, value);
		}
	}

	/**
	 * 許可する短時間勤務時間
	 * Allowable Time Of Short Work
	 */
	public static final Schema.SObjectField FIELD_ALLOWABLE_TIME_OF_SHORT_WORK = AttShortTimeWorkSettingHistory__c.AllowableTimeOfShortWork__c;
	public Integer allowableTimeOfShortWork {
		get {
			return Integer.valueOf(getFieldValue(FIELD_ALLOWABLE_TIME_OF_SHORT_WORK));
		}
		set {
			setFieldValue(FIELD_ALLOWABLE_TIME_OF_SHORT_WORK, value);
		}
	}

	/**
	 * 履歴コメント
	 * History Comment
	 */
	public static final Schema.SObjectField FIELD_HISTORY_COMMENT = AttShortTimeWorkSettingHistory__c.HistoryComment__c;
	public String historyComment {
		get {
			return (String)getFieldValue(FIELD_HISTORY_COMMENT);
		}
		set {
			setFieldValue(FIELD_HISTORY_COMMENT, value);
		}
	}

	/**
	 * 短時間勤務設定名(L1)
	 * Attendance Short Time Work Name(L1)
	 */
	public static final Schema.SObjectField FIELD_NAME_L1 = AttShortTimeWorkSettingHistory__c.Name_L1__c;
	public String nameL1 {
		get {
			return (String)getFieldValue(FIELD_NAME_L1);
		}
		set {
			setFieldValue(FIELD_NAME_L1, value);
		}
	}

	/**
	 * 短時間勤務設定名(L2)
	 * Attendance Short Time Work Name(L2)
	 */
	public static final Schema.SObjectField FIELD_NAME_L2 = AttShortTimeWorkSettingHistory__c.Name_L2__c;
	public String nameL2 {
		get {
			return (String)getFieldValue(FIELD_NAME_L2);
		}
		set {
			setFieldValue(FIELD_NAME_L2, value);
		}
	}

	/**
	 * 有効開始日
	 * Valid From
	 */
	public static final Schema.SObjectField FIELD_VALID_FROM = AttShortTimeWorkSettingHistory__c.ValidFrom__c;
	public AppDate validFrom {
		get {
			return AppDate.valueOf((Date)getFieldValue(FIELD_VALID_FROM));
		}
		set {
			setFieldValue(FIELD_VALID_FROM, AppConverter.dateValue(value));
		}
	}

	/**
	 * 失効日
	 * Valid To
	 */
	public static final Schema.SObjectField FIELD_VALID_TO = AttShortTimeWorkSettingHistory__c.ValidTo__c;
	public AppDate validTo {
		get {
			return AppDate.valueOf((Date)getFieldValue(FIELD_VALID_TO));
		}
		set {
			setFieldValue(FIELD_VALID_TO, AppConverter.dateValue(value));
		}
	}

	/**
	 * 短時間勤務設定ベース
	 * Attendance Short Time Work Setting Base
	 */
	public static final Schema.SObjectField FIELD_BASE_ID = AttShortTimeWorkSettingHistory__c.BaseId__c;
	public Id baseId {
		get {
			return (Id)getFieldValue(FIELD_BASE_ID);
		}
		set {
			setFieldValue(FIELD_BASE_ID, value);
		}
	}

	/**
	 * 短時間勤務設定名(L0)
	 * Attendance Short Time Work Name(L0)
	 */
	public static final Schema.SObjectField FIELD_NAME_L0 = AttShortTimeWorkSettingHistory__c.Name_L0__c;
	public String nameL0 {
		get {
			return (String)getFieldValue(FIELD_NAME_L0);
		}
		set {
			setFieldValue(FIELD_NAME_L0, value);
		}
	}

	/**
	 * 削除フラグ
	 * Removed
	 */
	public static final Schema.SObjectField FIELD_IS_REMOVED = AttShortTimeWorkSettingHistory__c.Removed__c;
	public Boolean isRemoved {
		get {
			return getFieldValue(FIELD_IS_REMOVED) == null ? false : (Boolean)getFieldValue(FIELD_IS_REMOVED);
		}
		set {
			setFieldValue(FIELD_IS_REMOVED, value == null ? false : value);
		}
	}

	/**
	 * Idと値を更新した項目を設定したSObjectを生成する。
	 * @return 生成したSObjecgt
	 */
	public virtual AttShortTimeWorkSettingHistory__c createSObject() {
		AttShortTimeWorkSettingHistory__c sobj = new AttShortTimeWorkSettingHistory__c(Id = super.id);
		for (Schema.SObjectField field : changedValues.keySet()) {
			sobj.put(field, changedValues.get(field));
		}
		return sobj;
	}

}