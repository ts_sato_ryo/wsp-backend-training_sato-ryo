/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署リポジトリ
 */
public with sharing class DepartmentRepository extends ParentChildRepository {

	/**
	 * 履歴リストを取得する際の並び順
	 */
	public enum HistorySortOrder {
		VALID_FROM_ASC, VALID_FROM_DESC
	}

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無 */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** 部署ベースのキャッシュ */
	private static final Repository.Cache BASE_CACHE = new Repository.Cache();
	/** 部署履歴のキャッシュ */
	private static final Repository.Cache HISTORY_CACHE = new Repository.Cache();

	/** 部署ベースのリレーション名 */
	private static final String HISTORY_BASE_R = ComDeptHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/**
	 * 取得対象のベースオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> baseFieldList = new Set<Schema.SObjectField> {
			ComDeptBase__c.Id,
			ComDeptBase__c.Name,
			ComDeptBase__c.CurrentHistoryId__c,
			ComDeptBase__c.Code__c,
			ComDeptBase__c.UniqKey__c,
			ComDeptBase__c.CompanyId__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(baseFieldList);
	}

	/** 部署ベースの会社のリレーション名 */
	private static final String BASE_COMPANY_R = ComDeptBase__c.CompanyId__c.getDescribe().getRelationshipName();
	/**
	 * 取得対象のベースオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_COMPANY_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> companyFieldList = new Set<Schema.SObjectField> {
			ComCompany__c.Code__c};

		GET_BASE_COMPANY_FIELD_NAME_LIST = Repository.generateFieldNameList(BASE_COMPANY_R, companyFieldList);
	}

	/**
	 * 取得対象の履歴オブジェクトの項目名
	 * 項目が追加されたらhistoryFieldSetに追加してください
	 */
	private static final List<String> GET_HISTORY_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> historyFieldSet = new Set<Schema.SObjectField> {
			ComDeptHistory__c.Id,
			ComDeptHistory__c.BaseId__c,
			ComDeptHistory__c.UniqKey__c,
			ComDeptHistory__c.ValidFrom__c,
			ComDeptHistory__c.ValidTo__c,
			ComDeptHistory__c.HistoryComment__c,
			ComDeptHistory__c.Removed__c,
			ComDeptHistory__c.Name,
			ComDeptHistory__c.Name_L0__c,
			ComDeptHistory__c.Name_L1__c,
			ComDeptHistory__c.Name_L2__c,
			ComDeptHistory__c.ManagerBaseId__c,
			ComDeptHistory__c.AssistantManagerBase1Id__c,
			ComDeptHistory__c.AssistantManagerBase2Id__c,
			ComDeptHistory__c.ParentBaseId__c,
			ComDeptHistory__c.Remarks__c};

		GET_HISTORY_FIELD_NAME_LIST = Repository.generateFieldNameList(historyFieldSet);
	}

	/**
	 * 取得対象の部署管理者オブジェクトの項目名
	 */
	private static final List<String> GET_HISTORY_MANAGER_FIELD_NAME_LIST;
	/** 部署管理者のリレーション名 */
	private static final String HISTORY_MANAGER_R = ComDeptHistory__c.ManagerBaseId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> employeeFieldList = new Set<Schema.SObjectField> {
			ComEmpBase__c.FirstName_L0__c,
			ComEmpBase__c.LastName_L0__c,
			ComEmpBase__c.FirstName_L1__c,
			ComEmpBase__c.LastName_L1__c,
			ComEmpBase__c.FirstName_L2__c,
			ComEmpBase__c.LastName_L2__c};

		GET_HISTORY_MANAGER_FIELD_NAME_LIST = Repository.generateFieldNameList(HISTORY_MANAGER_R, employeeFieldList);
	}

	/**
	 * コンストラクタ
	 */
	public DepartmentRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * Specify the use of cache and create instance.
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public DepartmentRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/**
	 * 指定したエンティティを取得する(ベース+全ての履歴)
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティと、ベースに紐づく全ての履歴エンティティ
	 */
	public DepartmentBaseEntity getEntity(Id baseId) {
		return getEntity(baseId, null);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public DepartmentBaseEntity getEntity(Id baseId, AppDate targetDate) {
		DepartmentRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};
		filter.targetDate = targetDate;

		List<DepartmentBaseEntity> entities = searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseIds 取得対象のベースIDのリスト、nullの場合は全てが取得対象となる
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、履歴が存在しない場合はnull
	 */
	public List<DepartmentBaseEntity> getEntityList(List<Id> baseIds, AppDate targetDate) {
		DepartmentRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = baseIds;
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定した履歴IDの履歴エンティティとその親であるベースエンティティを取得する
	 * @param historyId 履歴ID
	 * @return ベースと履歴エンティティ。ただし、対象の履歴IDが存在しない場合はnull
	 */
	public DepartmentBaseEntity getEntityByHistoryId(Id historyId) {
		DepartmentRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new List<Id>{historyId};

		List<DepartmentBaseEntity> entities = searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public DepartmentBaseEntity getBaseEntity(Id baseId) {
		return getBaseEntity(baseId, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public DepartmentBaseEntity getBaseEntity(Id baseId, Boolean forUpdate) {
		List<DepartmentBaseEntity> bases = getBaseEntityList(new List<Id>{baseId}, forUpdate);
		if (bases.isEmpty()) {
			return null;
		}
		return bases[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseIds 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティのリスト。履歴は含まない
	 */
	public List<DepartmentBaseEntity> getBaseEntityList(List<Id> baseIds, Boolean forUpdate) {
		DepartmentRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.baseIds = new List<Id>(baseIds);
		return searchBaseEntity(filter, forUpdate);
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyId 取得対象の履歴ID
	 * @return 履歴エンティティ。ただし、存在しない場合はnull
	 */
	public DepartmentHistoryEntity getHistoryEntity(Id historyId) {
		DepartmentRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new List<Id>{historyId};

		List<DepartmentHistoryEntity> entities = searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyIds 取得対象の履歴ID
	 * @return 履歴エンティティのリスト。
	 */
	public List<DepartmentHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		DepartmentRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = historyIds;

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースIDの履歴エンティティを取得する(履歴エンティティのみ)
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティ
	 */
	public List<DepartmentHistoryEntity> getHistoryEntityByBaseId(Id baseId) {
		return getHistoryEntityByBaseId(baseId, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースIDの履歴エンティティを取得する(履歴エンティティのみ)、履歴リストの取得順を指定可能
	 * @param baseId 取得対象のベースID
	 * @param sortOrder 履歴リストの並び順
	 * @return 指定したベースIDのエンティティ
	 */
	public List<DepartmentHistoryEntity> getHistoryEntityByBaseId(Id baseId, HistorySortOrder sortOrder) {
		DepartmentRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, sortOrder);
	}

	/**
	 * 指定したベースIDの履歴エンティティを取得する(履歴エンティティのみ)、履歴リストの取得順を指定可能
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日
	 * @return 指定したベースIDのエンティティ。有効開始日の昇順
	 */
	public List<DepartmentHistoryEntity> getHistoryEntityByBaseId(Id baseId, AppDate targetDate) {
		DepartmentRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};
		filter.targetDate = targetDate;

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}
	/**
	 * 指定会社に指定期間に有効な部署履歴を取得する
	 * @param companyId 指定会社Id
	 * @param startDate 指定期間の開始日
	 * @param endDate 指定期間の終了日
	 * @return 検索結果の部署履歴リスト。
	 */
	public List<DepartmentHistoryEntity> getHistoryEntityListByCompany(Id companyId, AppDate startDate, AppDate endDate) {
		SearchFilter filter = new SearchFilter();
		filter.companyIds = new List<Id>{companyId};
		filter.targetStartDate = startDate;
		filter.targetEndDate = endDate;

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}
	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する。有効開始日の昇順
	 */
	public List<DepartmentBaseEntity> searchEntity(SearchFilter filter) {
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/** 検索フィルタ(履歴の検索時に使用します) */
	public class SearchFilter {
		/** 部署ベースID */
		public List<Id> baseIds;
		/** 部署履歴ID */
		public List<Id> historyIds;
		/** 会社ID */
		public List<Id> companyIds;
		/** 親部署ID */
		public List<Id> parentIds;
		/** 取得対象日 */
		public AppDate targetDate;
		/** 取得対象期間(開始日) */
		public AppDate targetStartDate;
		/** 取得対象期間(終了日) */
		public AppDate targetEndDate;
		/** コード(完全一致) */
		public List<String> codes;

		/** 部署名L(L0,L1,L2が検索対象)（部分一致） */
		public String nameL;

		/** 論理削除されている履歴を取得対象にする場合はtrue */
		public Boolean includeRemoved;
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストの並び順
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する
	 */
	public List<DepartmentBaseEntity> searchEntityWithHistory(SearchFilter filter, Boolean forUpdate,
			DepartmentRepository.HistorySortOrder sortOrder) {

		// 履歴を検索
		List<DepartmentHistoryEntity> histories = searchHistoryEntity(filter, forUpdate, sortOrder);

		// 取得対象のベースIDリストを作成
		Set<Id> targetBaseIds = new Set<Id>();
		for (DepartmentHistoryEntity history : histories) {
			targetBaseIds.add(history.baseId);
		}

		// ベースエンティティを取得する
		SearchBaseFilter baseFilter = new SearchBaseFilter();
		baseFilter.baseIds = new List<Id>(targetBaseIds);
		List<DepartmentBaseEntity> bases = searchBaseEntity(baseFilter, forUpdate);

		// ベースに履歴を追加するためMapに変換する
		Map<Id, DepartmentBaseEntity> baseMap = new Map<Id, DepartmentBaseEntity>();
		for (DepartmentBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		// ベースエンティティに履歴を追加する
		for (DepartmentHistoryEntity history : histories) {
			DepartmentBaseEntity base = baseMap.get(history.baseId);
			base.addHistory(history);
		}

		return bases;
	}

	/** ベース用の検索フィルタ */
	public class SearchBaseFilter {
		/** 部署ベースID */
		public List<Id> baseIds;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	private List<DepartmentBaseEntity> searchBaseEntity(SearchBaseFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && BASE_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchBaseEntity ReturnCache key:' + cacheKey);
			List<DepartmentBaseEntity> entities = new List<DepartmentBaseEntity>();
			for (ComDeptBase__c sObj : (List<ComDeptBase__c>)BASE_CACHE.get(cacheKey)) {
				entities.add(createBaseEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchBaseEntity NoCache key:' + cacheKey);

		// ベース WHERE句
		List<String> baseWhereList = new List<String>();
		// ベースIDで検索
		List<Id> pIds;
		if (filter.baseIds != null) {
			pIds = filter.baseIds;
			baseWhereList.add('Id IN :pIds');
		}
		String whereString = buildWhereString(baseWhereList);


		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// ベース項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 会社項目
		selectFieldList.addAll(GET_BASE_COMPANY_FIELD_NAME_LIST);

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ComDeptBase__c.SObjectType.getDescribe().getName()
				+ whereString;
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(ComDeptBase__c.Code__c);
		}

		System.debug('DepartmentRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComDeptBase__c> sObjs = (List<ComDeptBase__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComDeptBase__c.SObjectType);
		// SObjectからエンティティを作成
		List<DepartmentBaseEntity> entities = new List<DepartmentBaseEntity>();
		for (ComDeptBase__c sObj : sObjs) {
			entities.add(createBaseEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			BASE_CACHE.put(cacheKey, sObjs);
		}
		return entities;
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストのソート順
	 * @return 履歴エンティティの検索
	 */
	public List<DepartmentHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, HistorySortOrder sortOrder) {
		// バインド変数が必要なため、クエリ作成処理をモジュール化していない

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter, sortOrder);
		if (isEnabledCache && !forUpdate && HISTORY_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity ReturnCache key:' + cacheKey);
			List<DepartmentHistoryEntity> entities = new List<DepartmentHistoryEntity>();
			for (ComDeptHistory__c sObj : (List<ComDeptHistory__c>)HISTORY_CACHE.get(cacheKey)) {
				entities.add(createHistoryEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity NoCache key:' + cacheKey);

		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 履歴項目
		selectFieldList.addAll(GET_HISTORY_FIELD_NAME_LIST);
		// 管理者
		selectFieldList.addAll(GET_HISTORY_MANAGER_FIELD_NAME_LIST);


		// WHERE句
		List<String> whereList = new List<String>();
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		Date pTargetStartDate;
		Date pTargetEndDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ComDeptHistory__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ComDeptHistory__c.ValidTo__c) + ' > :pTargetDate');
		} else if (filter.targetStartDate != null && filter.targetEndDate != null) {
			pTargetStartDate = filter.targetStartDate.getDate();
			pTargetEndDate = filter.targetEndDate.getDate();
			whereList.add(getFieldName(ComDeptHistory__c.ValidFrom__c) + ' <= :pTargetEndDate');
			whereList.add(getFieldName(ComDeptHistory__c.ValidTo__c) + ' > :pTargetStartDate');
		}
		// 履歴IDで検索
		List<Id> pHistoryIds;
		if(filter.historyIds != null){
			pHistoryIds = filter.historyIds;
			whereList.add(getFieldName(ComDeptHistory__c.Id) + ' IN :pHistoryIds');
		}
		// 親部署IDで検索
		List<Id> pParentIds;
		if(filter.parentIds != null){
			pParentIds = filter.parentIds;
			whereList.add(getFieldName(ComDeptHistory__c.ParentBaseId__c) + ' IN :pParentIds');
		}
		// 部署名で検索
		String pNameL;
		if(String.isNotBlank(filter.nameL)){
			pNameL = '%' + filter.nameL + '%';
			String s = getFieldName(ComDeptHistory__c.Name_L0__c) + ' LIKE :pNameL OR '
					+ getFieldName(ComDeptHistory__c.Name_L1__c) + ' LIKE :pNameL OR '
					+ getFieldName(ComDeptHistory__c.Name_L2__c) + ' LIKE :pNameL';
			whereList.add(s);
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			whereList.add(getFieldName(ComDeptHistory__c.Removed__c) + ' = false');
		}
		// ベースIDで検索
		List<Id> pBaseIds;
		if (filter.baseIds != null) {
			pBaseIds = filter.baseIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ComDeptBase__c.Id) + ' IN :pBaseIds');
		}
		// 会社IDで検索
		List<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ComDeptBase__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		List<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(HISTORY_BASE_R, ComDeptBase__c.Code__c) + ' IN :pCodes');
		}


		// ORDER BY句
		String orderBy = ' ORDER BY ' + getFieldName(HISTORY_BASE_R, ComDeptBase__c.Code__c);
		if (sortOrder == HistorySortOrder.VALID_FROM_ASC) {
			orderBy += ', ' + getFieldName(ComDeptHistory__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == HistorySortOrder.VALID_FROM_DESC) {
			orderBy += ', ' + getFieldName(ComDeptHistory__c.ValidFrom__c) + ' DESC NULLS LAST';
		}

		// クエリ作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ComDeptHistory__c' +
				+ buildWhereString(whereList);
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += orderBy;
		}
		System.debug('DepartmentRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComDeptHistory__c> sObjs = (List<ComDeptHistory__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComDeptHistory__c.SObjectType);
		List<DepartmentHistoryEntity> entities = new List<DepartmentHistoryEntity>();
		for (ComDeptHistory__c sObj : sObjs) {
			entities.add(createHistoryEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			HISTORY_CACHE.put(cacheKey, sobjs);
		}
		return entities;
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private DepartmentBaseEntity createBaseEntity(ComDeptBase__c baseObj) {
		return new DepartmentBaseEntity(baseObj);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private DepartmentHistoryEntity createHistoryEntity(ComDeptHistory__c historyObj) {
		return new DepartmentHistoryEntity(historyObj);
	}

	/**
	 * ベースオブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone) {
		List<ComDeptBase__c> deptList = new List<ComDeptBase__c>();
		for (SObject obj : baseObjList) {
			deptList.add((ComDeptBase__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * 履歴オブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone) {
		List<ComDeptHistory__c> deptList = new List<ComDeptHistory__c>();
		for (SObject obj : historyObjList) {
			deptList.add((ComDeptHistory__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * ベースエンティティからオブジェクトを作成する（各リポジトリで実装する）
	 */
	protected override SObject createBaseObj(ParentChildBaseEntity superBaseEntity) {
		return ((DepartmentBaseEntity)superBaseEntity).createSObject();
	}

	/**
	 * 履歴エンティティからオブジェクトを作成する
	 * @param historyEntity 作成元の履歴エンティティ
	 * @return 作成した履歴オブジェクト
	 */
	protected override SObject createHistoryObj(ParentChildHistoryEntity superHistoryEntity) {
		return ((DepartmentHistoryEntity)superHistoryEntity).createSObject();
	}
}