/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * ジョブタイプ別作業分類のリポジトリ
 */
public with sharing class JobTypeWorkCategoryRepository extends Repository.BaseRepository {

	/**　最大取得レコード件数 */
	private final static Integer SEARCH_RECORDS_NUMBER_MAX = 20000;

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/** オブジェクト名 */
	private static final String BASE_OBJECT_NAME = TimeJobTypeWorkCategory__c.SObjectType.getDescribe().getName();

	/**
	 * 取得対象の項目名
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				TimeJobTypeWorkCategory__c.Id,
				TimeJobTypeWorkCategory__c.Name,
				TimeJobTypeWorkCategory__c.JobTypeId__c,
				TimeJobTypeWorkCategory__c.WorkCategoryId__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/**
	 * 取得対象のジョブタイプオブジェクトの項目名
	 */
	private static final List<String> GET_JOB_TYPE_FIELD_NAME_LIST;
	/** ジョブタイプのリレーション名 */
	private static final String JOB_TYPE_R = TimeJobTypeWorkCategory__c.JobTypeId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ComJobType__c.Name_L0__c,
			ComJobType__c.Name_L1__c,
			ComJobType__c.Name_L2__c};

		GET_JOB_TYPE_FIELD_NAME_LIST = Repository.generateFieldNameList(JOB_TYPE_R, fieldList);
	}

	/**
	 * 取得対象の作業分類オブジェクトの項目名
	 */
	private static final List<String> GET_WORK_CATEGORY_FIELD_NAME_LIST;
	/** 作業分類のリレーション名 */
	private static final String WORK_CATEGORY_R = TimeJobTypeWorkCategory__c.WorkCategoryId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			TimeWorkCategory__c.Name_L0__c,
			TimeWorkCategory__c.Name_L1__c,
			TimeWorkCategory__c.Name_L2__c,
			TimeWorkCategory__c.ValidFrom__c,
			TimeWorkCategory__c.ValidTo__c,
			TimeWorkCategory__c.Order__c};

		GET_WORK_CATEGORY_FIELD_NAME_LIST = Repository.generateFieldNameList(WORK_CATEGORY_R, fieldList);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** ジョブタイプ別作業分類IDセット */
		public Set<Id> ids;
		/** ジョブタイプIDセット */
		public Set<Id> jobTypeIds;
	}

	/**
	 * 指定したIDを持つジョブタイプ別作業分類を取得する
	 * @param id 取得対象のID
	 * @return 指定したIDを持つジョブタイプ別作業分類、ただし該当するジョブタイプが存在しない場合はnull
	 */
	public JobTypeWorkCategoryEntity getEntity(Id id) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>{id};
		List<JobTypeWorkCategoryEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 指定したIDを持つジョブタイプ別作業分類を取得する
	 * @param idList 取得対象のID
	 * @return 指定したIDを持つジョブタイプ、該当するジョブタイプが存在しない場合は空のリストを返却する
	 */
	public List<JobTypeWorkCategoryEntity> getEntityList(List<Id> idList) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>(idList);
		return searchEntityList(filter);
	}

	/**
	 * 指定したジョブタイプIDを持つジョブタイプ別作業分類を取得する
	 * @param idList 取得対象のジョブタイプID
	 * @return 指定したIDを持つジョブタイプ、該当するジョブタイプが存在しない場合は空のリストを返却する
	 */
	public List<JobTypeWorkCategoryEntity> getEntityListByJobType(Id jobTypeId) {
		SearchFilter filter = new SearchFilter();
		filter.jobTypeIds = new Set<Id>{jobTypeId};
		return searchEntityList(filter);
	}

	/**
	 * 検索フィルタの条件に一致するジョブタイプを取得する。
	 * リストの並び順はジョブタイプのコード、作業分類の並び順、作業分類のコード
	 * 当メソッドではレコードをロックしない。(forUpdateではない)
	 * @param filter 検索フィルタ
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<JobTypeWorkCategoryEntity> searchEntityList(SearchFilter filter) {
		return searchEntityList(filter, false);
	}

	/**
	 * 検索フィルタの条件に一致するジョブタイプをコードの昇順で取得する。
	 * リストの並び順はジョブタイプのコード、作業分類の並び順、作業分類のコード
	 * @param filter 検索フィルタ
	 * @param forUpdate 更新用で取得する場合はtrue(ロックする)
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<JobTypeWorkCategoryEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// ジョブタイプ項目
		selectFieldList.addAll(GET_JOB_TYPE_FIELD_NAME_LIST);
		// 作業分類項目
		selectFieldList.addAll(GET_WORK_CATEGORY_FIELD_NAME_LIST);

		// WHERE句を生成
		List<String> conditions = new List<String>();
		final Set<Id> ids = filter.ids;
		if (ids != null) {
			conditions.add(createInExpression(TimeJobTypeWorkCategory__c.Id, 'ids'));
		}
		final Set<Id> jobTypeIds = filter.jobTypeIds;
		if (jobTypeIds != null) {
			conditions.add(createInExpression(TimeJobTypeWorkCategory__c.JobTypeId__c, 'jobTypeIds'));
		}

		// SOQLを生成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + BASE_OBJECT_NAME
				+ buildWhereString(conditions)
				+ ' ORDER BY ' + getFieldName(JOB_TYPE_R, ComJobType__c.Code__c) + ' ASC'
				+ ',' + getFieldName(WORK_CATEGORY_R, TimeWorkCategory__c.Order__c) + ' ASC NULLS LAST'
				+ ',' + getFieldName(WORK_CATEGORY_R, TimeWorkCategory__c.Code__c) + ' ASC'
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		System.debug('JobTypeWorkCategoryRepository: SOQL=' + soql);

		// クエリ実行
		List<TimeJobTypeWorkCategory__c> sObjList = Database.query(soql);
		List<JobTypeWorkCategoryEntity> entityList = new List<JobTypeWorkCategoryEntity>();
		for (TimeJobTypeWorkCategory__c sobj : sObjList) {
			entityList.add(new JobTypeWorkCategoryEntity(sobj));
		}
		return entityList;
	}

	/**
	 * ジョブタイプ別作業分類を保存する
	 * @param entityList 保存対象のジョブタイプ別作業分類
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntity(JobTypeWorkCategoryEntity entity) {
		 return saveEntityList(new List<JobTypeWorkCategoryEntity>{entity});
	 }

	/**
	 * ジョブタイプ別作業分類を保存する
	 * @param entityList 保存対象のジョブタイプ別作業分類
	 * @return 保存結果
	 */
	 public Repository.SaveResult saveEntityList(List<JobTypeWorkCategoryEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	 }

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public virtual Repository.SaveResult saveEntityList(List<JobTypeWorkCategoryEntity> entityList, Boolean allOrNone) {

		// エンティティをSObjectに変換する
		List<TimeJobTypeWorkCategory__c> sObjList = new List<TimeJobTypeWorkCategory__c>();
		for (JobTypeWorkCategoryEntity entity : entityList) {
			sObjList.add(entity.createSObject());
		}

		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}
}