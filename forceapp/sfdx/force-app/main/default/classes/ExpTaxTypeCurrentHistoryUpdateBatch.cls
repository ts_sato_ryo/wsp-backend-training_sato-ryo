/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分履歴切り替えバッチ Batch class for updating current history of ExpTaxTypeBase object
 */
public with sharing class ExpTaxTypeCurrentHistoryUpdateBatch extends CurrentHistoryUpdateBatchBase implements Schedulable {

	/**
	 * コンストラクタ Constructor
	 */
	public ExpTaxTypeCurrentHistoryUpdateBatch() {
		// ベース・履歴オブジェクトのAPI参照名を渡す Set base and history object name
		super(ExpTaxTypeBase__c.getSObjectType(), ExpTaxTypeHistory__c.getSObjectType());
	}

	/**
	 * Apex実行スケジュールを登録する Execute batch
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc) {
		ExpTaxTypeCurrentHistoryUpdateBatch b = new ExpTaxTypeCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
	}

	/**
	 * バッチ開始処理 Execute start prcoess of batch
	 * @param BC バッチコンテキスト
	 * @return バッチ処理対象レコード Target records
	 */
	public override Database.QueryLocator start(Database.BatchableContext BC) {
		// ベースオブジェクトを全取得する Get all base records
		return super.start(BC);
	}

	/**
	 * バッチ実行処理 Execute main process of batch
	 * @param BC バッチコンテキスト
	 * @param scope Target records
	 */
	public override void execute(Database.BatchableContext BC, List<Sobject> scope) {
		super.execute(BC, scope);
	}

	/**
	 * バッチ終了処理 Execute end process of batch
	 * @param BC バッチコンテキスト
	 */
	public override void finish(Database.BatchableContext BC) {
		super.finish(BC);
	}
}