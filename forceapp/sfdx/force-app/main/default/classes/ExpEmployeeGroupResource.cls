/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Expense Employee Group Resource
 */
public with sharing class ExpEmployeeGroupResource {

	@TestVisible
	private static Boolean isRollbackTest = false;

	static final ComMsgBase MESSAGE = ComMessage.msg();

	/** base class of ExpEmployeeGroup */
	abstract public class ExpEmployeeGroupBase {
		/** Record ID */
		public String id;
		/** Active */
		public Boolean active;
		/** Expense Employee Group code */
		public String code;
		/** Company ID */
		public String companyId;
		/** Expense Employee Group Description Language 0 */
		public String description_L0;
		/** Expense Employee Group Description Language 1 */
		public String description_L1;
		/** Expense Employee Group Description Language 2 */
		public String description_L2;
		/** Expense Employee Group Name */
		public String name;
		/** Expense Employee Group Name Language 0 */
		public String name_L0;
		/** Expense Employee Group Name Language 1 */
		public String name_L1;
		/** Expense Employee Group Name Language 2 */
		public String name_L2;

		/** Create entity from request parameter */
		public ExpEmployeeGroupEntity createEntity(Map<String, Object> paramMap) {
			ExpEmployeeGroupEntity entity = new ExpEmployeeGroupEntity();

			entity.setId(this.id);
			if (paramMap.containsKey('active')) {
				entity.active = this.active;
			}
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('description_L0')) {
				entity.descriptionL0 = this.description_L0;
			}
			if (paramMap.containsKey('description_L1')) {
				entity.descriptionL1 = this.description_L1;
			}
			if (paramMap.containsKey('description_L2')) {
				entity.descriptionL2 = this.description_L2;
			}
			if (paramMap.containsKey('name_L0')) {
				entity.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			return entity;
		}

		/** Set data from entity */
		public void setData(ExpEmployeeGroupEntity entity) {
			this.id = entity.id;
			this.active = entity.active;
			this.code = entity.code;
			this.companyId = entity.companyId;
			this.description_L0 = entity.descriptionL0;
			this.description_L1 = entity.descriptionL1;
			this.description_L2 = entity.descriptionL2;
			this.name = entity.nameL.getValue();
			this.name_L0 = entity.nameL0;
			this.name_L1 = entity.nameL1;
			this.name_L2 = entity.nameL2;
		}
	}

	abstract public class ExpEmployeeGroupRecord extends ExpEmployeeGroupBase {
		public List<String> reportTypeIdList;

		/*
		 * get reportTypeIdList from request parameter
		 */
		public List<String> getReportTypeIdList(Map<String, Object> paramMap) {
			List<String> reportTypeIdList = new List<String>();
			if (paramMap.containsKey('reportTypeIdList')) {
				reportTypeIdList = this.reportTypeIdList;
			}
			return reportTypeIdList;
		}

		/*
		 * Set report type id list from entity
		 */
		public void setReportTypeIdList(ExpEmployeeGroupEntity entity) {
			this.reportTypeIdList = entity.reportTypeIdList;
		}
	}

	/**
	 * @description Parameter for new creation and update
	 */
	public class UpsertParam extends ExpEmployeeGroupRecord implements RemoteApi.RequestParam {
		public void validate() {
			ExpCommonUtil.validateId('companyId', this.companyId, true);
			ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Com_Lbl_Code, this.code);
			ExpCommonUtil.validateStringLength(MESSAGE.Com_Lbl_Code, this.code, ExpEmployeeGroupEntity.CODE_MAX_LENGTH);
			ExpCommonUtil.validateStringLength(MESSAGE.Admin_Lbl_Description, this.description_L0, ExpEmployeeGroupEntity.DESCRIPTION_MAX_LENGTH);
			ExpCommonUtil.validateStringLength(MESSAGE.Admin_Lbl_Description, this.description_L1, ExpEmployeeGroupEntity.DESCRIPTION_MAX_LENGTH);
			ExpCommonUtil.validateStringLength(MESSAGE.Admin_Lbl_Description, this.description_L2, ExpEmployeeGroupEntity.DESCRIPTION_MAX_LENGTH);
			ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Admin_Lbl_Name, this.name_L0);
			ExpCommonUtil.validateStringLength(MESSAGE.Admin_Lbl_Name, this.name_L0, ExpEmployeeGroupEntity.NAME_MAX_LENGTH);
			ExpCommonUtil.validateStringLength(MESSAGE.Admin_Lbl_Name, this.name_L1, ExpEmployeeGroupEntity.NAME_MAX_LENGTH);
			ExpCommonUtil.validateStringLength(MESSAGE.Admin_Lbl_Name, this.name_L2, ExpEmployeeGroupEntity.NAME_MAX_LENGTH);
			ExpCommonUtil.validateIdList('reportTypeIdList', this.reportTypeIdList, false);
		}
	}

	/**
	 * @description Parameter for deletion
	 */
	public class DeleteParam implements RemoteApi.RequestParam {
		/** Expense Employee Group Id */
		public String id;

		public void validateParam() {
			ExpCommonUtil.validateId('id', this.id, true);
		}
	}

	/**
	 * @description Result of saving
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** ID of the created group */
		public String id;
	}

	/**
	 * @description Parameter for search and get
	 */
	public class SearchParam implements RemoteApi.RequestParam {
		/** Expense Employee Group Active */
		public Boolean active;
		/** Expense Employee Group Company id */
		public String companyId;
		/** Expense Employee Group id */
		public String id;

	}

	/**
	 * @description Search Result
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		public ExpEmployeeGroupRecord[] records;
	}

	/**
	 * @description API to create a new Expense Employee Group record
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
					ComResourcePermission.Permission.MANAGE_EXP_EMPLOYEE_GROUP;

		/**
		 * @description Create a new Expense employee group
		 * @param  req Request parameter (UpsertParam)
		 * @return Response (SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpEmployeeGroupResource.UpsertParam param = (ExpEmployeeGroupResource.UpsertParam) req.getParam(ExpEmployeeGroupResource.UpsertParam.class);

			param.validate();

			ExpEmployeeGroupEntity entity = param.createEntity(req.getParamMap());
			List<String> reportTypeIdList = param.getReportTypeIdList(req.getParamMap());
			Id result = ExpEmployeeGroupService.createExpenseEmployeeGroup(entity, reportTypeIdList);

			SaveResult response = new SaveResult();
			response.id = result;
			return response;
		}
	}

	/**
	 * @description Search Expense Employee Group API
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {
		/**
		 * @description Search Expense Emplolyee Group
		 * @param req SearchParam of Expense Employee Group Resource
		 * @return expense employee group record
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			ExpEmployeeGroupResource.SearchParam param = (ExpEmployeeGroupResource.SearchParam) req.getParam(SearchParam.class);

			validateParam(param);

			List<ExpEmployeeGroupEntity> expEmpGroupList = ExpEmployeeGroupService.searchExpenseEmployeeGroupList(param.companyId, param.id, true, param.active);

			return makeSearchResponse(expEmpGroupList);
		}

		private void validateParam(ExpEmployeeGroupResource.SearchParam param) {
			ExpCommonUtil.validateId('companyId', param.companyId, false);
			ExpCommonUtil.validateId('id', param.id, false);
		}

		private ExpEmployeeGroupResource.SearchResult makeSearchResponse(List<ExpEmployeeGroupEntity> expEmpGroupList) {
			List<ExpEmployeeGroupRecord> recordList = new List<ExpEmployeeGroupRecord>();

			for (ExpEmployeeGroupEntity entity:expEmpGroupList) {
				UpsertParam record = new UpsertParam();
				record.setData(entity);
				record.setReportTypeIdList(entity);
				recordList.add(record);
			}

			ExpEmployeeGroupResource.SearchResult result = new SearchResult();
			result.records = recordList;
			return result;
		}
	}

	/**
	 * @description Update Expense Employee Group API
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EMPLOYEE_GROUP;

		/**
		 * @description Update Expense Employee Group
		 * @param req Expense Employee Group Resource Upsert Param
		 * @return SaveResult
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpEmployeeGroupResource.UpsertParam param = (ExpEmployeeGroupResource.UpsertParam) req.getParam(ExpEmployeeGroupResource.UpsertParam.class);

			param.validate();

			ExpEmployeeGroupEntity entity = param.createEntity(req.getParamMap());
			List<String> reportTypeIdList = param.getReportTypeIdList(req.getParamMap());
			Id result = ExpEmployeeGroupService.updateExpenseEmployeeGroup(entity, reportTypeIdList);

			ExpEmployeeGroupResource.SaveResult response = new SaveResult();
			response.id = result;
			return response;
		}
	}

	/**
	 * @description Delete Expense Employee Group API
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EMPLOYEE_GROUP;

		/**
		 * @description Update Expense Employee Group
		 * @param req Expense Employee Group Resource Delete Param
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpEmployeeGroupResource.DeleteParam param = (ExpEmployeeGroupResource.DeleteParam) req.getParam(ExpEmployeeGroupResource.DeleteParam.class);

			param.validateParam();

			Savepoint sp = Database.setSavepoint();

			try {
				ExpEmployeeGroupService.deleteExpEmployeeGroup(param.id);

				if (isRollBackTest) {
					throw new App.IllegalStateException('Expense Employee Group roll back test exception!');
				}
			} catch (Exception e) {
				// Rollback all the DB update once exception occur.
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}
}