/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠日次申請のリポジトリテストクラス
 */
@isTest
private class AttDailyRequestRepositoryTest {
	private static AttDailyRequest__c createLeaveRequest(
			Id leaveId, AttLeaveRange leaveRange, Date startDate, Integer startTime,
			AppRequestStatus status, AppCancelType cancelType, String reason,
			String remarks, Integer leaveTotalTime, Id approver01Id,
			Id deptHisotryId, Id empHisotoryId, Id actorHistoryId) {

		AttDailyRequest__c retObj = new AttDailyRequest__c();

		retObj.Name = 'テスト申請' + startDate.format();
		retObj.RequestType__c = AttRequestType.LEAVE.value;
		retObj.Approver01Id__c = approver01Id;
		retObj.AttLeaveId__c = leaveId;
		retObj.AttLeaveType__c = AttLeaveType.PAID.value;
		retObj.AttLeaveRange__c = leaveRange.value;
		retObj.Reason__c = reason;
		retObj.Remarks__c = remarks;
		retObj.StartDate__c = startDate;
		retObj.EndDate__c = startDate.addDays(10);
		retObj.StartTime__c = startTime;
		retObj.EndTime__c = startTime + 5*60;
		retObj.Status__c = status.value;
		retObj.LeaveTotalTime__c = leaveTotalTime;
		retObj.ConfirmationRequired__c = true;
		retObj.CancelType__c = cancelType.value;
		retObj.RequestTime__c = System.now();
		retObj.DepartmentHistoryId__c = deptHisotryId;
		retObj.EmployeeHistoryId__c = empHisotoryId;
		retObj.ActorHistoryId__c = actorHistoryId;

		insert retObj;
		return retObj;
	}

	@isTest static void saveLeaveRequestTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity leave1 = testData.createLeave('b', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});

		Date dt = Date.today();
		AttDailyRequestEntity requestData = new AttDailyRequestEntity();
		requestData.name = 'テスト申請';
		requestData.processComment = 'テストコメント';
		requestData.ownerId = UserInfo.getUserId();
		requestData.requestType = AttRequestType.LEAVE;
		requestData.approver01Id = testData.createTestEmployees(1)[0].userId;
		requestData.leaveId = leave1.Id;
		requestData.leaveType = AttLeaveType.PAID;
		requestData.leaveRange = AttLeaveRange.RANGE_DAY;
		requestData.reason = 'テスト理由';
		requestData.remarks = 'テスト備考';
		requestData.startDate = AppDate.valueOf(dt);
		requestData.endDate = AppDate.valueOf(dt.addDays(10));
		requestData.startTime = AttTime.valueOf(10*60);
		requestData.endTime = AttTime.valueOf(16*60);
		requestData.status = AppRequestStatus.PENDING;
		requestData.cancelType = AppCancelType.REJECTED;
		requestData.leaveTotalTime = 16;
		requestData.isConfirmationRequired = true;
		requestData.requestTime = AppDatetime.now();
		requestData.excludingDateList = new List<AppDate>{
				AppDate.newInstance(2017, 10, 16),
				AppDate.newInstance(2017, 10, 17)};
		Datetime now = System.now();
		requestData.lastApproveTime = new AppDatetime(now);
		requestData.lastApproverId = Userinfo.getUserId();

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		Test.startTest();
		Repository.SaveResult result = requestRep.saveEntity(requestData);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);
		System.assertEquals(1, result.details.size());
		System.assertNotEquals(null, result.details[0].id);
		Id requestId = result.details[0].id;

		AttDailyRequest__c dailyRequest =
				[SELECT Name, OwnerId, ProcessComment__c, RequestType__c, Approver01Id__c,
					AttLeaveId__c, AttLeaveType__c, AttLeaveRange__c, Reason__c, Remarks__c,
					StartDate__c, EndDate__c, StartTime__c, EndTime__c,
					Status__c, CancelType__c, ConfirmationRequired__c,
					LeaveTotalTime__c, RequestTime__c, ExcludingDate__c,
					LastApproveTime__c, LastApproverId__c
				FROM AttDailyRequest__c
				WHERE Id = :requestId];
		System.assertEquals(requestData.name, dailyRequest.Name);
		System.assertEquals(requestData.ownerId, dailyRequest.OwnerId);
		System.assertEquals(requestData.processComment, dailyRequest.ProcessComment__c);
		System.assertEquals(AttRequestType.LEAVE.value, dailyRequest.RequestType__c);
		System.assertEquals(leave1.id, dailyRequest.AttLeaveId__c);
		System.assertEquals(AttLeaveType.PAID.value, dailyRequest.AttLeaveType__c);
		System.assertEquals(AttLeaveRange.RANGE_DAY.value, dailyRequest.AttLeaveRange__c);
		System.assertEquals(requestData.reason, dailyRequest.Reason__c);
		System.assertEquals(requestData.remarks, dailyRequest.Remarks__c);
		System.assertEquals(dt, dailyRequest.StartDate__c);
		System.assertEquals(dt.addDays(10), dailyRequest.EndDate__c);
		System.assertEquals(10*60, dailyRequest.StartTime__c);
		System.assertEquals(16*60, dailyRequest.EndTime__c);
		System.assertEquals(AppRequestStatus.PENDING.value, dailyRequest.Status__c);
		System.assertEquals(requestData.cancelType.value, dailyRequest.CancelType__c);
		System.assertEquals(requestData.requestTime.getDatetime(), dailyRequest.RequestTime__c);
		System.assertEquals(requestData.leaveTotalTime, dailyRequest.LeaveTotalTime__c);
		System.assertEquals(true, dailyRequest.ConfirmationRequired__c);
		// 除外日はカンマつなぎ
		System.assertEquals(requestData.excludingDateList[0].formatYYYYMMDD()
				+ ',' + requestData.excludingDateList[1].formatYYYYMMDD(), dailyRequest.ExcludingDate__c);
		System.assertEquals(now, dailyRequest.LastApproveTime__c);
		System.assertEquals(Userinfo.getUserId(), dailyRequest.LastApproverId__c);

	}

	@isTest static void getLeaveRequestTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(3);

		AttLeaveEntity leave1 = testData.createLeave('b', '有休', AttLeaveType.PAID, true,new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});
		AttDailyRequest__c leaveRequest = createLeaveRequest(
				leave1.Id, AttLeaveRange.RANGE_AM, Date.today(), 10*60, AppRequestStatus.PENDING, AppCancelType.REMOVED, 'テスト理由', 'テスト備考', 16,
				testEmployeeList[0].userId, testData.dept.CurrentHistoryId__c,
				testEmployeeList[1].getHistory(0).id, testEmployeeList[2].getHistory(0).id);
		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		Test.startTest();
		List<AttDailyRequestEntity> entityList = requestRep.getLeaveListByIds(new Set<Id>{leaveRequest.id});
		Test.stopTest();

		System.assertEquals(1, entityList.size());
		assertEntity(leaveRequest, leave1, entityList[0]);
	}

	/**
	 * getEntityListのテスト
	 * 指定したIDのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityListTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(3);
		AttLeaveEntity leave1 = testData.createLeave('b', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM});

		AttDailyRequest__c leaveRequest = createLeaveRequest(
				leave1.Id, AttLeaveRange.RANGE_AM, Date.today(), 10*60, AppRequestStatus.PENDING, AppCancelType.REJECTED, 'テスト理由', 'テスト備考', 16,
				testEmployeeList[0].userId, testData.dept.CurrentHistoryId__c,
				testEmployeeList[1].getHistory(0).id, testEmployeeList[2].getHistory(0).id);

		AttDailyRequestRepository requestRep = new AttDailyRequestRepository();
		Test.startTest();
		List<AttDailyRequestEntity> entityList = requestRep.getEntityListByIds(new Set<Id>{leaveRequest.id});
		Test.stopTest();

		System.assertEquals(1, entityList.size());
		assertEntity(leaveRequest, leave1, entityList[0]);
	}

	/**
	 * 承認内容変更申請元申請の情報を正しく取得できることを検証する
	 */
	@isTest static void getOriginalRequestEntityTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 元申請
		AttDailyRequest__c original = new AttDailyRequest__c(
			RequestType__c = AttRequestType.HOLIDAYWORK.value,
			Status__c = AppRequestStatus.DISABLED.value,
			CancelType__c = AppCancelType.REAPPLIED.value
		);
		insert original;

		// 承認内容変更申請
		AttDailyRequest__c reapply = new AttDailyRequest__c(
			OriginalRequestId__c = original.id,
			RequestType__c = AttRequestType.HOLIDAYWORK.value,
			Status__c = AppRequestStatus.PENDING.value,
			CancelType__c = AppCancelType.NONE.value
		);
		insert reapply;

		// 検証
		AttDailyRequestEntity request = new AttDailyRequestRepository().getEntity(reapply.id);
		System.assertEquals(AppRequestStatus.PENDING, request.status);
		System.assertEquals(AppCancelType.NONE, request.cancelType);
		System.assertEquals(original.id, request.originalRequestId);
		System.assertEquals(AppRequestStatus.DISABLED, request.originalRequestStatus);
		System.assertEquals(AppCancelType.REAPPLIED, request.originalRequestCancelType);
	}

	/**
	 * getEntityByOriginalIdsのテスト
	 * 指定したIDのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityByOriginalIdTest() {

		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();
		AttTestData testData = new AttTestData();

		// テストデータ作成
		AttDailyRequestEntity originalRequest = testData.createHolidayWorkRequest(
					AppDate.newInstance(2018, 7, 1), AttTime.newInstance(9, 0), AttTime.newInstance(18, 0));
		AttDailyRequestEntity reapplyRequest = testData.createHolidayWorkRequest(
					AppDate.newInstance(2018, 7, 1), AttTime.newInstance(9, 0), AttTime.newInstance(18, 0));

		reapplyRequest.originalRequestId = originalRequest.id;
		requestRepo.saveEntityList(new List<AttDailyRequestEntity>{reapplyRequest});

		// 実行
		AttDailyRequestEntity resultEntity = requestRepo.getEntityByOriginalId(originalRequest.id);

		// 検証
		System.assertEquals(reapplyRequest.id, resultEntity.id);
	}

	/**
	 * getReapplyingLeaveListのテスト
	 * 指定した休暇IDで変更承認待ちの休暇申請のみを取得できることを確認する
	 */
	@isTest static void getReapplyingLeaveList() {

		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();
		AttTestData testData = new AttTestData();
		List<AttDailyRequestEntity> testEntityList = new List<AttDailyRequestEntity>();

		// 取得したい休暇申請
		AttDailyRequestEntity reapplyingRequest = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_DAY, AppDate.newInstance(2019, 1, 8), AppDate.newInstance(2019, 1, 8), null, null);
		reapplyingRequest.status = AppRequestStatus.REAPPLYING;
		testEntityList.add(reapplyingRequest);

		// ステータスが変更承認待ち以外の休暇申請
		AttDailyRequestEntity diabledRequest = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_DAY, AppDate.newInstance(2019, 1, 9), AppDate.newInstance(2019, 1, 9), null, null);
		diabledRequest.status = AppRequestStatus.DISABLED;
		testEntityList.add(diabledRequest);
		AttDailyRequestEntity pendingRequest = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_DAY, AppDate.newInstance(2019, 1, 10), AppDate.newInstance(2019, 1, 10), null, null);
		pendingRequest.status = AppRequestStatus.PENDING;
		testEntityList.add(pendingRequest);
		AttDailyRequestEntity approvedRequest = testData.createLeaveRequest(
				testData.leaveList[0], AttLeaveRange.RANGE_DAY, AppDate.newInstance(2019, 1, 11), AppDate.newInstance(2019, 1, 11), null, null);
		approvedRequest.status = AppRequestStatus.APPROVED;
		testEntityList.add(approvedRequest);

		// 別休暇IDの休暇申請
		AttDailyRequestEntity reapplyingRequestOtherId = testData.createLeaveRequest(
				testData.leaveList[1], AttLeaveRange.RANGE_DAY, AppDate.newInstance(2019, 1, 15), AppDate.newInstance(2019, 1, 15), null, null);
		reapplyingRequest.status = AppRequestStatus.REAPPLYING;
		testEntityList.add(reapplyingRequestOtherId);

		// 休暇申請以外の申請
		AttDailyRequestEntity holidayWorkRequest = testData.createHolidayWorkRequest(
				AppDate.newInstance(2019, 1, 16), AttTime.newInstance(9, 0), AttTime.newInstance(18, 0));
		holidayWorkRequest.status = AppRequestStatus.REAPPLYING;
		testEntityList.add(holidayWorkRequest);

		requestRepo.saveEntityList(testEntityList);

		// 実行
		List<AttDailyRequestEntity> resultEntityList = requestRepo.getReapplyingLeaveList(
				testData.employee.id, new Set<Id>{testData.leaveList[0].id});

		// 検証
		System.assertEquals(1, resultEntityList.size());
		System.assertEquals(AppDate.newInstance(2019, 1, 8), resultEntityList[0].startDate);
	}

	/**
	 * 期待した値が取得できていることを確認する
	 */
	private static void assertEntity(AttDailyRequest__c expObj, AttLeaveEntity expLeave, AttDailyRequestEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.ProcessComment__c, actEntity.processComment);
		System.assertEquals(expObj.RequestType__c, actEntity.RequestType.value);
		System.assertEquals(expObj.AttLeaveId__c, actEntity.leaveId);
		System.assertEquals(expLeave.nameL.valueL0, actEntity.leaveNameL.valueL0);
		System.assertEquals(expLeave.nameL.valueL1, actEntity.leaveNameL.valueL1);
		System.assertEquals(expLeave.nameL.valueL2, actEntity.leaveNameL.valueL2);
		System.assertEquals(expObj.AttLeaveType__c, actEntity.leaveType.value);
		System.assertEquals(expObj.AttLeaveRange__c, actEntity.leaveRange.value);
		System.assertEquals(expObj.Reason__c, actEntity.reason);
		System.assertEquals(expObj.Remarks__c, actEntity.remarks);
		System.assertEquals(expObj.StartDate__c, AppConverter.dateValue(actEntity.startDate));
		System.assertEquals(expObj.EndDate__c, AppConverter.dateValue(actEntity.endDate));
		System.assertEquals(expObj.StartTime__c, AppConverter.intValue(actEntity.startTime));
		System.assertEquals(expObj.EndTime__c, AppConverter.intValue(actEntity.endTime));
		System.assertEquals(expObj.Status__c, actEntity.status.value);
		System.assertEquals(expObj.RequestTime__c, actEntity.requestTime.getDatetime());
		System.assertEquals(expObj.LeaveTotalTime__c, actEntity.leaveTotalTime);
		System.assertEquals(expObj.ConfirmationRequired__c, actEntity.isConfirmationRequired);
		System.assertEquals(expObj.LastApproveTime__c, AppDatetime.convertDatetime(actEntity.lastApproveTime));
		System.assertEquals(expObj.LastApproverId__c, actEntity.lastApproverId);

		// 社員の情報
		System.assertEquals(expObj.EmployeeHistoryId__c, actEntity.employeeId);
		ComEmpHistory__c expEmpHistory = getEmployeeHistory(expObj.EmployeeHistoryId__c);
		System.assertEquals(expEmpHistory.BaseId__c, actEntity.employeeBaseId);
		assertEmployee(expEmpHistory, actEntity.employee);
		System.assertEquals(expObj.ActorHistoryId__c, actEntity.actorId);
		assertEmployee(getEmployeeHistory(expObj.ActorHistoryId__c), actEntity.actor);

		// 部署の情報
		System.assertEquals(expObj.DepartmentHistoryId__c, actEntity.departmentId);
		assertDepartment(getDepartmentHistory(expObj.DepartmentHistoryId__c), actEntity.department);

		// TODO ExcludingDate__c のテスト
	}

	/**
	 * 社員の情報が取得できていることを確認する
	 * @param expObj 期待する値を持つSObject
	 * @param actEntity 実際に取得した値
	 */
	private static void assertEmployee(ComEmpHistory__c expObj, AttLookup.Employee actEntity) {
		System.assertEquals(expObj.BaseId__r.FirstName_L0__c, actEntity.fullName.firstNameL0);
		System.assertEquals(expObj.BaseId__r.FirstName_L1__c, actEntity.fullName.firstNameL1);
		System.assertEquals(expObj.BaseId__r.FirstName_L2__c, actEntity.fullName.firstNameL2);
		System.assertEquals(expObj.BaseId__r.LastName_L0__c, actEntity.fullName.lastNameL0);
		System.assertEquals(expObj.BaseId__r.LastName_L1__c, actEntity.fullName.lastNameL1);
		System.assertEquals(expObj.BaseId__r.LastName_L2__c, actEntity.fullName.lastNameL2);
		System.assertEquals(expObj.ValidFrom__c, AppConverter.dateValue(actEntity.validFrom));
		System.assertEquals(expObj.ValidTo__c, AppConverter.dateValue(actEntity.validTo));
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);
	}

	/**
	 * 部署の情報が取得できていることを確認する
	 * @param expObj 期待する値を持つSObject
	 * @param actEntity 実際に取得した値
	 */
	private static void assertDepartment(ComDeptHistory__c expObj, AttLookup.Department actEntity) {
		System.assertEquals(expObj.Name_L0__c, actEntity.name.valueL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.name.valueL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.name.valueL2);
		System.assertEquals(expObj.ValidFrom__c, AppConverter.dateValue(actEntity.validFrom));
		System.assertEquals(expObj.ValidTo__c, AppConverter.dateValue(actEntity.validTo));
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);
	}

	/**
	 * 指定した履歴IDの社員履歴オブジェクトを取得する
	 */
	private static ComEmpHistory__c getEmployeeHistory(Id historyId) {
		List<ComEmpHistory__c> objs = [
				SELECT Id, Name,
					BaseId__r.FirstName_L0__c, BaseId__r.FirstName_L1__c, BaseId__r.FirstName_L2__c,
					BaseId__r.LastName_L0__c, BaseId__r.LastName_L1__c, BaseId__r.LastName_L2__c,
					ValidFrom__c, ValidTo__c, Removed__c
				FROM ComEmpHistory__c
				WHERE Id = :historyId];
		return objs[0];
	}

	/**
	 * 指定した履歴IDの部署履歴オブジェクトを取得する
	 */
	private static ComDeptHistory__c getDepartmentHistory(Id historyId) {
		List<ComDeptHistory__c> objs = [
				SELECT Id, Name, Name_L0__c, Name_L1__c, Name_L2__c,
					ValidFrom__c, ValidTo__c, Removed__c
				FROM ComDeptHistory__c
				WHERE Id = :historyId];
		return objs[0];
	}

}