/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 36協定アラート設定のサービスクラス
 */
public with sharing class AttAgreementAlertSettingService {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * 新規のアラート設定を保存する
	 * 【注意!】警告時間1がnullの場合、警告時間2の値が警告時間1に移動して保存されます。
	 * @param 保存対象のエンティティ
	 * @return 保存したエンティティのID
	 */
	public Id createNewAlertSetting(AttAgreementAlertSettingEntity newEntity) {

		// 保存用のエンティティを作成
		// 下記で警告値が変更される可能性もあるため
		AttAgreementAlertSettingEntity entity = newEntity.copy();

		// 保存する
		return saveAlertSetting(entity);
	}

	/**
	 * アラート設定を更新する。
	 * 【注意!】警告時間1がnullの場合、警告時間2の値が警告時間1に移動して保存されます。
	 * @param updateEntity 更新値が設定されているエンティティ(IDは必須)
	 */
	public void updateAlertSetting(AttAgreementAlertSettingEntity updateEntity) {

		AttAgreementAlertSettingRepository repo = new AttAgreementAlertSettingRepository();

		// 更新対象のレコード存在確認
		AttAgreementAlertSettingEntity entity = repo.getEntity(updateEntity.id);
		if (entity == null) {
			// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
		}

		// 更新する値を設定する
		setUpdateValueToEntity(entity, updateEntity);

		// 保存する
		saveAlertSetting(entity);
	}

	/**
	 * アラート設定を保存する。コードが変更されている場合は履歴のユニークキーもあわせて更新する
	 * 【注意!】警告時間1がnullの場合、警告時間2の値が警告時間1に移動して保存されます。
	 * @param entity 保存対象のエンティティ
	 * @return 保存したエンティティのID
	 */
	private Id saveAlertSetting(AttAgreementAlertSettingEntity entity) {

		AttAgreementAlertSettingRepository repo = new AttAgreementAlertSettingRepository();

		// 警告1の値が未設定の場合、警告2の値を警告1に移動する
		slideWarningHour(entity);

		// ユニークキーを更新する
		updateUniqKey(entity);

		// 保存対象のエンティティを検証する
		validateAlertSettingEntity(entity);

		// コードの重複チェック
		if (isDuplicatedCode(entity)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		// 保存する
		Repository.SaveResult saveResult = (new AttAgreementAlertSettingRepository()).saveEntity(entity);

		return saveResult.details[0].id;
	}

	/**
	 * アラート設定のユニークキーを更新する(DBヘの更新は行わない)
	 * @param entity 更新対象のエンティティ。companyId, codeを設定しておくこと
	 */
	private void updateUniqKey(AttAgreementAlertSettingEntity entity) {
			// 会社コードを取得
			CompanyEntity company = getCompany(entity.companyId);
			if (company == null) {
				// メッセージ：指定された会社が見つかりません
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
			}
			entity.uniqKey = entity.createUniqKey(company.code);
	}

	/**
	 * エンティティに更新する値を設定する
	 */
	private void setUpdateValueToEntity(
			AttAgreementAlertSettingEntity toEntity, AttAgreementAlertSettingEntity updateEntity) {

		// 値が変更された項目のみを設定する
		for (Schema.SObjectField field : updateEntity.getChangedFieldSet()) {
			toEntity.setFieldValue(field, updateEntity.getFieldValue(field));
		}
	}

	/**
	 * 警告1の値がnullの場合、警告2の値を警告1に移動する(通常、特別両方)
	 *  警告1 = null, 警告2 = 30 ならば、警告1 = 30, 警告2 = nullに変更されます。
	 * @param entity 対象のエンティティ
	 */
	private void slideWarningHour(AttAgreementAlertSettingEntity entity) {

		// 警告時間(通常)
		if (entity.monthlyAgreementHourWarning1 == null
				&& entity.monthlyAgreementHourWarning2 != null) {
			entity.monthlyAgreementHourWarning1 = entity.monthlyAgreementHourWarning2;
			entity.monthlyAgreementHourWarning2 = null;
		}

		// 警告時間(特別)
		if (entity.monthlyAgreementHourWarningSpecial1 == null
				&& entity.monthlyAgreementHourWarningSpecial2 != null) {
			entity.monthlyAgreementHourWarningSpecial1 = entity.monthlyAgreementHourWarningSpecial2;
			entity.monthlyAgreementHourWarningSpecial2 = null;
		}
	}


	/**
	 * コードが同じ会社内で重複しているかどうかをチェックする
	 * @param entity チェック対象のエンティティ
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	private Boolean isDuplicatedCode(AttAgreementAlertSettingEntity entity) {
		AttAgreementAlertSettingEntity codeEntity =
				getAlertSettingEntityByCode(entity.code, entity.companyId);

		Boolean isDuplicated;
		if ((codeEntity != null) && (codeEntity.id != entity.id)) {
			return true;
		}
		return false;
	}

	/**
	 * 指定したコードを持つエンティティを取得する
	 * @code コード
	 * @companyId 会社ID
	 * @return 指定したコードを持つエンティティ、存在しない場合はnull
	 */
	private AttAgreementAlertSettingEntity getAlertSettingEntityByCode(String code, Id companyId) {
		AttAgreementAlertSettingRepository.SearchFilter filter =
				new AttAgreementAlertSettingRepository.SearchFilter();
		filter.codes = new Set<String>{code};
		filter.companyIds = new Set<Id>{companyId};

		List<AttAgreementAlertSettingEntity> entities =
				(new AttAgreementAlertSettingRepository()).searchEntity(filter);
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * アラート設定エンティティのバリデーションを実行する
	 * 不正な値が設定されている場合は例外が発生する
	 */
	private void validateAlertSettingEntity(AttAgreementAlertSettingEntity entity) {
		Validator.Result result =
				(new AttConfigValidator.AttAgreementAlertSettingValidator(entity)).validate();
		if (! result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/** 会社のキャッシュ */
	private Map<Id, CompanyEntity> companyCache = new Map<Id, CompanyEntity>();
	/**
	 * 会社を取得する(キャッシュ機能あり)
	 * @param companyId 取得対象の会社ID
	 * @return 会社
	 * @throws App.RecordNotFoundException 指定した会社が見つからなかった場合
	 */
	private CompanyEntity getCompany(Id companyId) {
		CompanyEntity company = companyCache.get(companyId);
		if (company == null) {
			company = new CompanyRepository().getEntity(companyId);
			if (company == null) {
				// メッセージ：指定された会社が見つかりません
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
			}
			companyCache.put(companyId, company);
		}
		return company;
	}
}
