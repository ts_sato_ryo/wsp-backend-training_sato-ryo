/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for ExpVendorService
 */
@isTest
private class ExpVendorServiceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);

	/**
	 * Create a new Vendor record(Positive case)
	 */
	@isTest
	static void createVendorTest() {
		ExpVendorEntity entity = new  ExpVendorEntity();
		setTestParam(entity);

		Test.startTest();

		// Create new record
		Repository.SaveResult result = ExpVendorService.createVendor(entity);

		Test.stopTest();

		System.assertEquals(1, result.details.size());

		List<ExpVendor__c> records =
			[SELECT
				Id,
				Active__c,
				Address__c,
				BankAccountType__c,
				BankAccountNumber__c,
				BankCode__c,
				BankName__c,
				BranchAddress__c,
				BranchCode__c,
				BranchName__c,
				Code__c,
				CompanyId__c,
				Country__c,
				CurrencyCode__c,
				CorrespondentBankAddress__c,
				CorrespondentBankName__c,
				CorrespondentBranchName__c,
				CorrespondentSwiftCode__c,
				IsWithholdingTax__c,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				PayeeName__c,
				PaymentTerm__c,
				SwiftCode__c,
				UniqKey__c,
				ZipCode__c
			FROM ExpVendor__c
			WHERE Id =: result.details[0].id];

		// Confirm new record was created
		System.assertEquals(1, records.size());

		assertFields(entity, records[0]);
	}

	/**
	 * Create a new Vendor record(Positive case : same code in different companies can be saved)
	 */
	@isTest
	static void createVendorUniqTest() {
		ExpVendorEntity entity = new  ExpVendorEntity();
		setTestParam(entity);

		Test.startTest();

		// Create one record
		ExpVendorService.createVendor(entity);

		ComCompany__c newCompany = ComTestDataUtility.createCompany('New Test Co. Ltd.', countryObj.Id);
		ExpVendorEntity newEntity = new  ExpVendorEntity();
		setTestParam(newEntity);
		newEntity.companyId = newCompany.Id;

		// Create new record with same code in different company
		Repository.SaveResult result = ExpVendorService.createVendor(newEntity);

		Test.stopTest();

		List<ExpVendor__c> records =
		[SELECT
			Code__c,
			CompanyId__c
			FROM ExpVendor__c];

		// Confirm new record was created
		System.assertEquals(2, records.size());

		System.assertEquals(records[0].Code__c, records[1].Code__c);
		System.assertNotEquals(records[0].CompanyId__c, records[1].CompanyId__c);
	}

	/**
   * Create a new Vendor record(Negative case: Same code exists)
   */
	@isTest
	static void createVendorDuplicateErrTest() {

		// Create 'TEST1' code record first
		ComTestDataUtility.createExpVendor('TEST', companyObj.Id);

		ExpVendorEntity entity = new  ExpVendorEntity();
		setTestParam(entity);
		entity.code = 'TEST1';

		Test.startTest();

		APP.ParameterException ex;
		try {
			// Try to create new 'TEST1' code record again
			ExpVendorService.createVendor(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
	   * Update Vendor record(Positive case)
	   */
	@isTest
	static void updateVendorTest() {
		// Create a Vendor record first
		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		ExpVendorEntity entity = new ExpVendorEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		Test.startTest();

		ExpVendorService.updateVendor(entity);

		Test.stopTest();

		List<ExpVendor__c> records =
		[SELECT
			Active__c,
			Address__c,
			BankAccountType__c,
			BankAccountNumber__c,
			BankCode__c,
			BankName__c,
			BranchAddress__c,
			BranchCode__c,
			BranchName__c,
			Code__c,
			CompanyId__c,
			Country__c,
			CurrencyCode__c,
			CorrespondentBankAddress__c,
			CorrespondentBankName__c,
			CorrespondentBranchName__c,
			CorrespondentSwiftCode__c,
			IsWithholdingTax__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			PayeeName__c,
			PaymentTerm__c,
			SwiftCode__c,
			UniqKey__c,
			ZipCode__c
		FROM ExpVendor__c
		WHERE Id =: testRecord.id];

		//Confirm the record was updated
		System.assertEquals(1, records.size());

		assertFields(entity, records[0]);
	}

	/**
   * Update Vendor record(Negative case: Update code is duplicate)
   */
	@isTest
	static void updateVendorDuplicateErrTest() {
		// Create test records
		ExpVendor__c test1Record = ComTestDataUtility.createExpVendor('Test1', companyObj.Id);
		ExpVendor__c test2Record = ComTestDataUtility.createExpVendor('Test2', companyObj.Id);

		// Create Test1 entity
		ExpVendorEntity entity = new ExpVendorEntity();
		setTestParam(entity);
		entity.setId(test1Record.Id);

		// Set Test2 code to Test1 entity
		entity.code = test2Record.Code__c;

		Test.startTest();

		APP.ParameterException ex;
		try {
			ExpVendorService.updateVendor(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
 * Update Vendor record(Negative case: Target record is no found)
 */
	@isTest
	static void updateVendorNotFoundTest() {
		// Create a Vendor record first
		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		ExpVendorEntity entity = new ExpVendorEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		// Delete the record before updating
		delete testRecord;

		Test.startTest();

		APP.ParameterException ex;
		try {
			ExpVendorService.updateVendor(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm the expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_RecordNotFound, ex.getMessage());
	}

	/**
	 * Delete Vendor record(Positive case)
	 */
	@isTest
	static void deleteVendorTest() {

		// Create the target data to delete
		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		Id id = testRecord.Id;

		Test.startTest();

		// Delete the target
		ExpVendorService.deleteVendor(id);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ExpVendor__c WHERE Id =: id]);
	}

	/**
   * Delete Vendor record(Positive case: Target has been deleted)
   */
	@isTest
	static void deleteVendorAlreadyDeletedTest() {

		// Create the target data to delete
		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		Id id = testRecord.Id;

		// Delete the target record in advance
		delete testRecord;

		Test.startTest();

		DmlException ex = null;
		try {
			// Try to Delete the target
			ExpVendorService.deleteVendor(id);
		} catch (DmlException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm exception did not occur
		System.assertEquals(null, ex);
	}

	/**
   * Search Vendor data(Positive case)
 	 */
	@isTest
	static void searchVendorTest() {

		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		Map<String, Object> param = new Map<String, Object>();
		// All conditions
		param.put('companyId', testRecord.CompanyId__c);
		param.put('active', testRecord.Active__c);

		List<ExpVendorEntity> result =  ExpVendorService.searchVendor(param);
		System.assertEquals(1, result.size());

		System.assertEquals(testRecord.Id, result[0].id);
		assertFields(result[0], testRecord);

		// Company ID
		param.clear();
		param.put('companyId', testRecord.CompanyId__c);
		result = ExpVendorService.searchVendor(param);
		System.assertEquals(1, result.size());

		// Active
		param.clear();
		param.put('active', testRecord.Active__c);
		result = ExpVendorService.searchVendor(param);
		System.assertEquals(1, result.size());
	}

	/**
	 * Search Vendor data(Negative case)
	 */
	@isTest
	static void searchVendorNoResultTest() {

		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		Map<String, Object> param = new Map<String, Object>();
		List<ExpVendorEntity> result;

		// Invalid Company ID
		param.clear();
		param.put('companyId', testRecord.Id);
		result = ExpVendorService.searchVendor(param);
		System.assertEquals(0, result.size());

		// Invalid Active
		param.clear();
		param.put('active', !testRecord.Active__c);
		result = ExpVendorService.searchVendor(param);
		System.assertEquals(0, result.size());
	}

	/**
	 * Search all Vendor data(Positive case)
   */
	@isTest
	static void searchVendorAllTest() {

		ComTestDataUtility.createExpVendors('Test', companyObj.Id, 3);
		// Fetch data order by code
		List<ExpVendor__c> testRecords =
		[SELECT
			Id,
			Active__c,
			Address__c,
			BankAccountType__c,
			BankAccountNumber__c,
			BankCode__c,
			BankName__c,
			BranchAddress__c,
			BranchCode__c,
			BranchName__c,
			Code__c,
			CompanyId__c,
			Country__c,
			CurrencyCode__c,
			CorrespondentBankAddress__c,
			CorrespondentBankName__c,
			CorrespondentBranchName__c,
			CorrespondentSwiftCode__c,
			IsWithholdingTax__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			PayeeName__c,
			PaymentTerm__c,
			SwiftCode__c,
			UniqKey__c,
			ZipCode__c
		FROM ExpVendor__c
		ORDER BY Code__c NULLS LAST];

		Test.startTest();

		// When searching by nul ID, return all data
		List<ExpVendorEntity> result =  ExpVendorService.searchVendor(null);

		Test.stopTest();

		// Confirm all data were fetched
		System.assertEquals(testRecords.size(), result.size());

		for (Integer i = 0, n = testRecords.size(); i < n; i++) {
			System.assertEquals(testRecords[i].Id, result[i].id);
			assertFields(result[i], testRecords[i]);
		}
	}

	/**
   * Set test parameter to entity
   * @param param Target object to set data
   */
	static void setTestParam(ExpVendorEntity entity) {
		entity.active = true;
		entity.address = 'address';
		entity.bankAccountType = ExpBankAccountType.CHECKING;
		entity.bankAccountNumber = 'bankAccountNumber';
		entity.bankCode = 'bankCd';
		entity.bankName = 'bankName';
		entity.branchAddress = 'branchAddress';
		entity.branchCode = 'XYZ';
		entity.branchName = 'branchName';
		entity.code = 'code';
		entity.correspondentBankAddress = 'correspondentBankAddress';
		entity.correspondentBankName = 'correspondentBankName';
		entity.correspondentBranchName = 'correspondentBranchName';
		entity.correspondentSwiftCode = 'cSwiftCode';
		entity.companyId = companyObj.Id;
		entity.country = 'JPN';
		entity.currencyCode = ComIsoCurrencyCode.valueOf('JPY');
		entity.isWithholdingTax = true;
		entity.nameL0 = 'nameL0';
		entity.nameL0 = 'nameL0';
		entity.nameL1 = 'nameL1';
		entity.nameL2 = 'nameL2';
		entity.payeeName = 'payeeName';
		entity.paymentTerm = 'paymentTerm';
		entity.swiftCode = 'swiftCode';
		entity.zipCode = 'zipCode';
		entity.uniqKey = 'uniqKey';
	}

	static void assertFields(ExpVendorEntity entity, ExpVendor__c sObj) {
		System.assertEquals(sObj.Active__c, entity.active);
		System.assertEquals(sObj.Address__c, entity.address);
		System.assertEquals(sObj.BankAccountType__c, entity.bankAccountType.value);
		System.assertEquals(sObj.BankAccountNumber__c, entity.bankAccountNumber);
		System.assertEquals(sObj.BankCode__c, entity.bankCode);
		System.assertEquals(sObj.BankName__c, entity.bankName);
		System.assertEquals(sObj.BranchAddress__c, entity.branchAddress);
		System.assertEquals(sObj.BranchCode__c, entity.branchCode);
		System.assertEquals(sObj.BranchName__c, entity.branchName);
		System.assertEquals(sObj.Code__c, entity.code);
		System.assertEquals(sObj.CorrespondentBankAddress__c, entity.correspondentBankAddress);
		System.assertEquals(sObj.CorrespondentBankName__c, entity.correspondentBankName);
		System.assertEquals(sObj.CorrespondentBranchName__c, entity.correspondentBranchName);
		System.assertEquals(sObj.CorrespondentSwiftCode__c, entity.correspondentSwiftCode);
		System.assertEquals(sObj.CompanyId__c, entity.companyId);
		System.assertEquals(sObj.Country__c, entity.country);
		System.assertEquals(sObj.CurrencyCode__c, entity.currencyCode.value);
		System.assertEquals(sObj.IsWithholdingTax__c, entity.isWithholdingTax);
		System.assertEquals(sObj.Name_L0__c, entity.nameL0);
		System.assertEquals(sObj.Name_L1__c, entity.nameL1);
		System.assertEquals(sObj.Name_L2__c, entity.nameL2);
		System.assertEquals(sObj.PayeeName__c, entity.payeeName);
		System.assertEquals(sObj.PaymentTerm__c, entity.paymentTerm);
		System.assertEquals(sObj.SwiftCode__c, entity.swiftCode);
		System.assertEquals(sObj.ZipCode__c, entity.zipCode);
		System.assertEquals(sObj.UniqKey__c, entity.uniqKey);
	}

}