/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description ExpReportTypeServiceのテスト Test class for ExpReportTypeService
 */
@isTest
private class ExpReportTypeServiceTest {

	/** Message */
	private final static ComMsgBase MESSAGE = ComMessage.msg();
	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/**
	 * テストデータクラス Test Data Class
	 */
	private class TestData extends TestData.TestDataEntity {
		/** Expense Employee Group */
		public ExpEmployeeGroup__c employeeGroup;
		public List<ExtendedItemEntity> extendedItemDate;
		/** 拡張項目データ Extended item data */
		public List<ExtendedItemEntity> extendedItemsText = new List<ExtendedItemEntity>();
		/** 拡張項目(選択リスト)データ Extended item data(Picklist) */
		public List<ExtendedItemEntity> extendedItemsPicklist = new List<ExtendedItemEntity>();

		public List<ExtendedItemEntity> extendedItemsLookup = new List<ExtendedItemEntity>();
		public List<ComExtendedItemCustom__c> extendedItemCustoms;
		/** 拡張項目使用 */
		public ComExtendedItemUsedIn extendedItemUsedIn;
		/** 拡張項目必須 */
		public ComExtendedItemRequiredFor extendedItemRequiredFor;

		/**
		 * コンストラクタ / Constructor
		 */
		public TestData() {
			super();
			// Create Employee Group
			employeeGroup = ComTestDataUtility.createExpEmployeeGroup('EmpGp', this.company.Id);
			// Create Employee with Employee Group Assigned
			ComEmpBase__c employeeObject = ComTestDataUtility.createEmployeeWithExpEmpGroup('Sakura', this.company.Id, this.department.Id, UserInfo.getUserId(), this.employeeGroup.Id);
			employee = (new EmployeeRepository()).getEntity(employeeObject.Id);
			// 拡張項目データ作成 Create extended item data
			List<Id> extendedItemIdList = new List<Id>();
			// テキスト Text
			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItems('Extended Item Text', this.company.id, 20)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItemsText = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);
			// 選択リスト Picklist
			extendedItemIdList = new List<Id>();
			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItems('Extended Item Picklist', this.company.id, ExtendedItemInputType.PICKLIST, 20)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItemsPicklist = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);
			//Lookup
			extendedItemIdList = new List<Id>();
			this.extendedItemCustoms = ComTestDataUtility.createExtendedItemCustoms('Custom Obj', this.company.id, 20);

			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItemLookups('EILookup', this.company.id, this.extendedItemCustoms)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItemsLookup = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);

			//Date
			extendedItemIdList = new List<Id>();
			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItemDates('Extended Item Date', this.company.id, 20)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItemDate = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);

			// 拡張項目使用作成
			this.extendedItemUsedIn = ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			// 拡張項目必須作成
			this.extendedItemRequiredFor = ComExtendedItemRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
		}

		/**
		 * 経費申請タイプオブジェクトを作成する Create Expense Report Type records
		 */
		public List<ExpReportTypeEntity> createExpReportTypes(String name, Integer size) {
			return createExpReportTypes(name, this.company.id, size);
		}

		/**
		 * 経費申請タイプオブジェクトを作成する Create Expense Report Type records
		 */
		public List<ExpReportTypeEntity> createExpReportTypes(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpReportTypes(name, companyId, size);
			return (new ExpReportTypeRepository()).getEntityList(null);
		}

		/**
		 * 経費申請タイプエンティティを作成する(DB保存なし) Create an Expense Report Type Entity (Do not update to DB)
		 */
		public ExpReportTypeEntity createExpReportTypeEntity(String name) {
			ExpReportTypeEntity entity = new ExpReportTypeEntity();

			entity.active = true;
			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.company.code + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.company.id;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.costCenterRequiredFor = ComCostCenterRequiredFor.EXPENSE_REPORT;
			entity.costCenterUsedIn = ComCostCenterUsedIn.EXPENSE_REPORT;
			entity.jobRequiredFor = ComJobRequiredFor.EXPENSE_REPORT;
			entity.jobUsedIn = ComJobUsedIn.EXPENSE_REPORT;
			entity.useFileAttachment = true;
			entity.vendorRequiredFor = ExpVendorRequiredFor.EXPENSE_REPORT;
			entity.vendorUsedIn = ExpVendorUsedIn.EXPENSE_REPORT;

			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				entity.setExtendedItemDateId(i, this.extendedItemDate[i].id);
				entity.setExtendedItemDateUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemDateRequiredFor(i, this.extendedItemRequiredFor);

				entity.setExtendedItemLookupId(i, this.extendedItemsLookup[i].id);
				entity.setExtendedItemLookupUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemLookupRequiredFor(i, this.extendedItemRequiredFor);

				entity.setExtendedItemTextId(i, this.extendedItemsText[i].id);
				entity.setExtendedItemTextUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemTextRequiredFor(i, this.extendedItemRequiredFor);

				entity.setExtendedItemPicklistId(i, this.extendedItemsPicklist[i].id);
				entity.setExtendedItemPicklistUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemPicklistRequiredFor(i, this.extendedItemRequiredFor);
			}

			return entity;
		}

		/**
		 * Link Report Type to Employee Group
		 * @param empGroupId Employee Group Id
		 * @param reportTypeId Rport Type Id
		 */
		public void linkExpReportTypeExpEmpGroup(Id empGroupId, Id reportTypeId) {
			linkExpReportTypeExpEmpGroup(empGroupId, new List<Id>{reportTypeId});
		}

		/**
		 * Link Report Type to Employee Group
		 * @param empGroupId Employee Group Id
		 * @param reportTypeIdList Rport Type Id List
		 */
		public void linkExpReportTypeExpEmpGroup(Id empGroupId, List<Id> reportTypeIdList) {
			ComTestDataUtility.createExpEmployeeGroupReportTypeLinkList(new List<Id>{empGroupId}, reportTypeIdList);
		}

		/*
		 * Link Report Type to Expense Types
		 * @param reportTypeId Report Type Id
		 * @param expTypeIdList Expense Type Id List
		 */
		public void linkExpReportTypeExpTypes(Id reportTypeId, List<Id> expTypeIdList) {
			ComTestDataUtility.createExpReportTypeExpTypeLink(reportTypeId, expTypeIdList);
		}
	}

	/**
	 * Test for createNewExpReportType method
	 * Check if an new record is correctly created
	 */
	@isTest static void createNewExpReportTypeTest() {
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity newEntity = testData.createExpReportTypeEntity('New Exp Report Type');

		Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, null);
		System.assertNotEquals(null, resId);

		ExpReportTypeEntity savedEntity = new ExpReportTypeRepository().getEntity(resId);
		System.assertNotEquals(null, savedEntity);
		assertSaveAllField(newEntity, savedEntity);
	}

	/**
	 * Test for updateExpReportType method
	 * Check if all fields of the target record are correctly updated
	 */
	@isTest static void updateExpReportTypeTestAllField() {
		final Integer testExpReportTypeSize = 2;
		ExpReportTypeServiceTest.TestData testData = new TestData();

		List<ExpReportTypeEntity> testExpReportTypes = testData.createExpReportTypes('Test', testExpReportTypeSize);
		new ExpReportTypeRepository().saveEntityList(testExpReportTypes, ALL_SAVE);

		ExpReportTypeEntity targetExpReportType = testExpReportTypes[1];
		targetExpReportType.active = false;
		targetExpReportType.code = targetExpReportType.code + 'Update';
		targetExpReportType.companyId = testData.company.id;
		targetExpReportType.descriptionL0 = targetExpReportType.descriptionL0 + 'Update';
		targetExpReportType.descriptionL1 = targetExpReportType.descriptionL1 + 'Update';
		targetExpReportType.descriptionL2 = targetExpReportType.descriptionL2 + 'Update';
		targetExpReportType.nameL0 = targetExpReportType.nameL0 + 'Update';
		targetExpReportType.nameL1 = targetExpReportType.nameL1 + 'Update';
		targetExpReportType.nameL2 = targetExpReportType.nameL2 + 'Update';
		targetExpReportType.uniqKey = targetExpReportType.createUniqKey(testData.company.code);
		targetExpReportType.costCenterRequiredFor = ComCostCenterRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
		targetExpReportType.costCenterUsedIn = ComCostCenterUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
		targetExpReportType.jobRequiredFor = ComJobRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
		targetExpReportType.jobUsedIn = ComJobUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
		targetExpReportType.vendorRequiredFor = ExpVendorRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
		targetExpReportType.vendorUsedIn = ExpVendorUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;

		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			targetExpReportType.setExtendedItemDateId(i, testData.extendedItemDate[i + 10].id);
			targetExpReportType.setExtendedItemDateUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REPORT);
			targetExpReportType.setExtendedItemDateRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);

			targetExpReportType.setExtendedItemTextId(i, testData.extendedItemsText[i + 10].id);
			targetExpReportType.setExtendedItemTextUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REPORT);
			targetExpReportType.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);

			targetExpReportType.setExtendedItemPicklistId(i, testData.extendedItemsPicklist[i + 10].id);
			targetExpReportType.setExtendedItemPicklistUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REPORT);
			targetExpReportType.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);

			targetExpReportType.setExtendedItemLookupId(i, testData.extendedItemsLookup[i + 10].id);
			targetExpReportType.setExtendedItemLookupUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REPORT);
			targetExpReportType.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);
		}
		// update
		(new ExpReportTypeService()).updateExpReportType(targetExpReportType, null);

		// assertion
		ExpReportTypeEntity savedEntity = new ExpReportTypeRepository().getEntity(targetExpReportType.id);
		assertSaveAllField(targetExpReportType, savedEntity);
	}

	/**
	 * Test for updateExpReportType method
	 * Check if some fields of the target record are correctly updated
	 */
	@isTest static void updateExpReportTypeTestSomeField() {
		final Integer testExpReportTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();

		ExpReportTypeEntity targetExpReportType = testData.createExpReportTypes('Test', testExpReportTypeSize).get(0);

		// entity for update
		ExpReportTypeEntity updateExpReportType = new ExpReportTypeEntity();
		updateExpReportType.setId(targetExpReportType.id);
		updateExpReportType.active = false;
		updateExpReportType.nameL0 = targetExpReportType.nameL0 + 'Update';
		updateExpReportType.descriptionL0 = targetExpReportType.descriptionL0 + 'Update';

		// update
		(new ExpReportTypeService()).updateExpReportType(updateExpReportType, null);

		// assertion
		ExpReportTypeEntity savedEntity = new ExpReportTypeRepository().getEntity(updateExpReportType.id);
		System.assertEquals(updateExpReportType.active, false);
		System.assertEquals(updateExpReportType.nameL0, savedEntity.nameL0);
		System.assertEquals(updateExpReportType.descriptionL0, savedEntity.descriptionL0);
		// the value of other fields are not updated
		System.assertEquals(targetExpReportType.nameL1, savedEntity.nameL1);
	}

	/**
	 * Test for createNewExpReportType method
	 * Check if exception is thrown if parameter include incorrect value
	 */
	@isTest static void createNewExpReportTypeTestValidation() {
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity newEntity = testData.createExpReportTypeEntity('New ExpReportType');
		newEntity.nameL0 = '';

		try {
			Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, null);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
			String msg = MESSAGE.Com_Err_NotSetValue(new List<String>{MESSAGE.Admin_Lbl_Name});
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Test for createNewExpReportType method
	 * Check if exception is thrown if the same code already exists in the company
	 */
	@isTest static void createNewExpReportTypeTestDuplicateCode() {
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity testEntity = testData.createExpReportTypes('Test', 1).get(0);
		ExpReportTypeEntity newEntity = testData.createExpReportTypeEntity('New ExpReportType');
		newEntity.code = testEntity.code;

		try {
			Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, null);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Com_Err_DuplicateCode;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Negative Test for createNewExpReportType method
	 * Check if exception is thrown if the extended item is duplicated
	 */
	@isTest static void createNewExpReportTypeNegativeTestExtendedItem() {
		ExpReportTypeServiceTest.TestData testData = new TestData();

		// Date
		ExpReportTypeEntity newEntity = testData.createExpReportTypeEntity('Test2');
		try {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				// Set all Picklist ID to be the same
				newEntity.setExtendedItemDateId(i, newEntity.getExtendedItemDateId(0));
			}
			Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, null);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Exp_Err_DuplicateExtendedItem;
			System.assertEquals(msg, e.getMessage());
		}

		// Lookup
		newEntity = testData.createExpReportTypeEntity('Test2');
		try {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				// Set all Picklist ID to be the same
				newEntity.setExtendedItemLookupId(i, newEntity.getExtendedItemLookupId(0));
			}
			Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, null);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Exp_Err_DuplicateExtendedItem;
			System.assertEquals(msg, e.getMessage());
		}

		// テキスト Text
		newEntity = testData.createExpReportTypeEntity('Test1');
		try {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				// Set all Picklist ID to be the same
				newEntity.setExtendedItemTextId(i, newEntity.getExtendedItemTextId(0));
			}
			Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, null);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Exp_Err_DuplicateExtendedItem;
			System.assertEquals(msg, e.getMessage());
		}

		// 選択リスト Picklist
		newEntity = testData.createExpReportTypeEntity('Test2');
		try {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				// Set all Picklist ID to be the same
				newEntity.setExtendedItemPicklistId(i, newEntity.getExtendedItemPicklistId(0));
			}
			Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, null);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			String msg = MESSAGE.Exp_Err_DuplicateExtendedItem;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Test for createNewExpReportType method with Expense Type Id link
	 * Check if an new record is correctly created
	 */
	@isTest static void createAndSearchNewExpReportTypeWithExpTypeTest() {
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity newEntity = testData.createExpReportTypeEntity('New Exp Report Type');
		List<ExpType__c> expTypeSObject = ComTestDataUtility.createExpTypes('Type', testData.company.id, 3);
		Set<String> expTypeIds = new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[2].Id};

		Id resId = (new ExpReportTypeService()).createNewExpReportType(newEntity, new List<String>(expTypeIds));
		System.assertNotEquals(null, resId);

		ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{resId};

		List<ExpReportTypeService.ExpReportTypeWithExpTypeLink> savedEntity = new ExpReportTypeService().searchExpReportTypeWithExpType(filter, null, null, null, null, false, null);
		System.assertNotEquals(null, savedEntity);
		System.assertEquals(true, expTypeIds.contains(savedEntity[0].expTypeIdList[0]));
		System.assertEquals(true, expTypeIds.contains(savedEntity[0].expTypeIdList[1]));
		System.assertEquals(true, expTypeIds.contains(savedEntity[0].expTypeIdList[2]));
	}

	/**
	 * Test for searchExpReportWithExpType method with Filtered Employee Group Test
	 * In this test, 3 test cases are covered -
	 * 1) When employee belong to an employee group but the employee group have no report type link
	 * 2) When employee belong to an employee group and report type is linked
	 * 3) When empllyee belong to no employeee gorup throw exception
	 */
	@isTest static void searchNewExpReportTypeFilteredWithEmployeeGroupTest() {
		final Integer testExpReportTypeSize = 2;
		final Integer testExpTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		List<ExpReportTypeEntity> newReportTypeEntityList = testData.createExpReportTypes('Report', testExpReportTypeSize);
		List<ExpType__c> expTypeSObject = ComTestDataUtility.createExpTypes('Type', testData.company.id, testExpTypeSize);
		Set<String> expTypeIds = new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[2].Id};

		Id resId = newReportTypeEntityList[0].id;

		ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();

		// When Employee belongs to employee group but no report is linked to the employee group
		List<ExpReportTypeService.ExpReportTypeWithExpTypeLink> reportEntityList = new ExpReportTypeService().searchExpReportTypeWithExpType(filter, 'REPORT', null, null, null, false, testData.employee.id);
		System.assertEquals(0, reportEntityList.size());

		// Add EmployeeGroup and Report Type link
		testData.linkExpReportTypeExpEmpGroup(testData.employeeGroup.Id, resId);
		// When EmployeeGroup have linked Report Type
		reportEntityList = new ExpReportTypeService().searchExpReportTypeWithExpType(filter, 'REPORT', null,null, null, false, testData.employee.id);
		System.assertEquals(1, reportEntityList.size());
		System.assertEquals(newReportTypeEntityList[0].id, reportEntityList[0].expReportType.id);

		// For FA page where the search param didn't consist of employee Id didn't filter with employeeGroup and return all avaliable Report Type
		filter.ids = null; // Need to clear the filter as it is used in same test case and modified in previous case
		reportEntityList = new ExpReportTypeService().searchExpReportTypeWithExpType(filter, 'REPORT', null, null, null, false, null);
		System.assertEquals(2, reportEntityList.size());

		// When Employee didn't belong to any Employee group so return exception
		EmployeeHistoryEntity employeeHistoryEntity = testData.employee.getHistory(0);
		employeeHistoryEntity.expEmployeeGroupId = null;
		new EmployeeService().reviseHistoryWithValidation(testData.employee, employeeHistoryEntity);
		try {
			new ExpReportTypeService().searchExpReportTypeWithExpType(filter, 'REPORT', null, null, null, false, testData.employee.id);
			TestUtil.fail('No exception is thrown for no employee group assigned to employee');
		} catch (Exception e) {
			System.assertEquals(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Exp_Lbl_EmployeeGroup}), e.getMessage());
		}
	}

	/**
	 * Test for searchExpReportWithExpType method with expense type name
	 */
	@isTest static void searchNewExpReportTypeWithExpTypeNameTest() {
		final Integer testExpReportTypeSize = 2;
		final Integer testExpTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		List<ExpReportTypeEntity> newReportTypeEntityList = testData.createExpReportTypes('Report', testExpReportTypeSize);
		List<ExpType__c> expTypeSObject = ComTestDataUtility.createExpTypes('Type', testData.company.id, testExpTypeSize);
		expTypeSObject[0].Name_L0__c = 'XYZ';
		expTypeSObject[0].Name_L1__c = 'XYZ';
		expTypeSObject[1].Name_L0__c = 'ABC';
		expTypeSObject[1].Name_L1__c = 'ABC';
		expTypeSObject[2].Name_L0__c = 'JKL';
		expTypeSObject[2].Name_L1__c = 'JKL';
		update expTypeSObject;
		Set<String> expTypeIds = new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[2].Id};

		Id resId = newReportTypeEntityList[0].id;

		// Add EmployeeGroup and Report Type link
		testData.linkExpReportTypeExpEmpGroup(testData.employeeGroup.Id, resId);
		testData.linkExpReportTypeExpTypes(resId, new List<String>(expTypeIds));

		ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{resId};

		// Expense Type Name and Id
		List<ExpReportTypeService.ExpReportTypeWithExpTypeLink> reportTypeEntityList = new ExpReportTypeService().searchExpReportTypeWithExpType(filter, null, null,null, null, true, null);
		System.assertEquals(1, reportTypeEntityList.size());
		System.assertEquals(newReportTypeEntityList[0].id, reportTypeEntityList[0].expReportType.id);
		System.assertEquals('ABC', reportTypeEntityList[0].expTypeList[0].expTypeName);
		System.assertEquals(expTypeSObject[1].Id, reportTypeEntityList[0].expTypeList[0].expTypeId);
		System.assertEquals('JKL', reportTypeEntityList[0].expTypeList[1].expTypeName);
		System.assertEquals(expTypeSObject[2].Id, reportTypeEntityList[0].expTypeList[1].expTypeId);
		System.assertEquals('XYZ', reportTypeEntityList[0].expTypeList[2].expTypeName);
		System.assertEquals(expTypeSObject[0].Id, reportTypeEntityList[0].expTypeList[2].expTypeId);

	}

	/**
	 * Test for searchExpReportWithExpType method with Accounting Period
	 */
	@isTest static void searchNewExpReportTypeWithAccPeriodTest() {
		final Integer testExpReportTypeSize = 2;
		final Integer testExpTypeSize = 7;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		List<ExpReportTypeEntity> newReportTypeEntityList = testData.createExpReportTypes('Report', testExpReportTypeSize);
		List<ExpType__c> expTypeSObject = ComTestDataUtility.createExpTypes('Type', testData.company.id, testExpTypeSize);
		for (Integer i = 0; i < expTypeSObject.size(); i++) {
			expTypeSObject[i].ValidFrom__c = Date.today().addDays(i * 7);
			expTypeSObject[i].ValidTo__c = Date.today().addDays((i+1) * 7);
		}
		update expTypeSObject;
		Set<String> expTypeIds = new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[2].Id, expTypeSObject[3].Id, expTypeSObject[4].Id, expTypeSObject[5].Id};

		Id resId = newReportTypeEntityList[0].id;

		// Add EmployeeGroup and Report Type link
		testData.linkExpReportTypeExpEmpGroup(testData.employeeGroup.Id, resId);
		testData.linkExpReportTypeExpTypes(resId, new List<String>(expTypeIds));

		ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{resId};

		// Expense Type Name and Id
		List<ExpReportTypeService.ExpReportTypeWithExpTypeLink> reportTypeEntityList = new ExpReportTypeService()
				.searchExpReportTypeWithExpType(filter, null, null, AppDate.today(), AppDate.today().addDays(30), true, null);
		System.assertEquals(1, reportTypeEntityList.size());
		System.assertEquals(5, reportTypeEntityList[0].expTypeList.size());
		System.assertEquals(newReportTypeEntityList[0].id, reportTypeEntityList[0].expReportType.id);
		for (Integer i = 0; i < reportTypeEntityList[0].expTypeList.size();  i++) {
			System.assertEquals(expTypeSObject[i].Id, reportTypeEntityList[0].expTypeList[i].expTypeId);
		}
	}

	/**
	 * Test for searchExpReportWithExpType method with Filtered Employee Group Test in sorted report type order
	 */
	@isTest static void searchExpReportSortedWithEmployeeGroupTest() {
		final Integer testExpReportTypeSize = 4;
		final Integer testExpTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		List<ExpReportTypeEntity> newReportTypeEntityList = testData.createExpReportTypes('Report', testExpReportTypeSize);
		List<ExpType__c> expTypeSObject = ComTestDataUtility.createExpTypes('Type', testData.company.id, testExpTypeSize);

		Id resId = newReportTypeEntityList[0].id;

		ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();

		// Add EmployeeGroup and Report Type link
		testData.linkExpReportTypeExpEmpGroup(testData.employeeGroup.Id, new List<Id>{newReportTypeEntityList[2].id, newReportTypeEntityList[0].id, newReportTypeEntityList[1].id});

		// When EmployeeGroup have linked Report Type
		List<ExpReportTypeService.ExpReportTypeWithExpTypeLink> reportEntityList = new ExpReportTypeService().searchExpReportTypeWithExpType(filter, 'REPORT', null, null, null, false, testData.employee.id);
		System.assertEquals(3, reportEntityList.size());
		System.assertEquals(newReportTypeEntityList[2].id, reportEntityList[0].expReportType.id);
		System.assertEquals(newReportTypeEntityList[0].id, reportEntityList[1].expReportType.id);
		System.assertEquals(newReportTypeEntityList[1].id, reportEntityList[2].expReportType.id);

	}

	/**
	 * Test for updateExpReportType method
	 * Check if the target record can not be found
	 */
	@isTest static void updateExpReportTypeTestRecordNotFound() {
		final Integer testExpReportTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();

		List<ExpReportTypeEntity> targetExpReportTypess = testData.createExpReportTypes('Test', testExpReportTypeSize);
		ExpReportTypeEntity targetExpReportType = targetExpReportTypess[1];
		(new ExpReportTypeRepository()).deleteEntity(targetExpReportType.id);

		try {
			(new ExpReportTypeService()).updateExpReportType(targetExpReportType, null);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.RecordNotFoundException e) {
			String msg = MESSAGE.Com_Err_RecordNotFound;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * Test for updateExpReportType method
	 * Check if exception is thrown if extended item setting is changed
	 */
	@isTest static void updateExpReportTypeTestExtendedItemChanged() {
		ExpReportTypeServiceTest.TestData testData = new TestData();

		 // Link extended item to report type
		ExpReportTypeEntity reportType = testData.createExpReportTypes('Test', 1)[0];

		// Create Expense report
		ExpReport__c expReport = new ExpReport__c(
			Name = 'Test ExpReport',
			ExpReportTypeId__c = reportType.id,
			ExtendedItemText01Value__c = 'ExtendedItemText01'
		);
		insert expReport;

		Test.startTest();
		// Case 1: Change Date Type Extended Item Id
		Id originalId = reportType.getExtendedItemDateId(9);
		reportType.setExtendedItemDateId(9, testData.extendedItemDate[15].id);
		assertReferencedExceptionIsThrown(reportType);

		 // Case 2: Change Lookup Type Extended Item Id
		reportType.setExtendedItemDateId(9, originalId);	//Reset to original
		originalId = reportType.getExtendedItemLookupId(9);
		reportType.setExtendedItemLookupId(9, testData.extendedItemsLookup[15].id);
		assertReferencedExceptionIsThrown(reportType);

		 // Case 3: Change Picklist Type Extended Item Id
		reportType.setExtendedItemLookupId(9, originalId);	//Reset to original
		originalId = reportType.getExtendedItemPicklistId(9);
		reportType.setExtendedItemPicklistId(9, testData.extendedItemsPicklist[15].id);
		assertReferencedExceptionIsThrown(reportType);

		 // Case 4: Change Text Type Extended Item Id
		reportType.setExtendedItemPicklistId(9, originalId);	//Reset to original
		reportType.setExtendedItemTextId(9, testData.extendedItemsText[15].id);
		assertReferencedExceptionIsThrown(reportType);
	}

	/**
	 * Update Expense Report Type with Expense Type Link Changes
	 */
	@isTest static void updateExpReportTypeExpTypeLinkTest(){
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity newEntity = testData.createExpReportTypeEntity('New Exp Report Type');
		List<ExpType__c> expTypeSObject = ComTestDataUtility.createExpTypes('Type', testData.company.id, 4);
		Set<String> expTypeIds = new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[2].Id};

		ExpReportTypeService expReportTypeService = new ExpReportTypeService();

		Id resId = expReportTypeService.createNewExpReportType(newEntity, new List<String>(expTypeIds));
		System.assertNotEquals(null, resId);

		Set<String> newExpTypeIds =new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[3].Id};
		newEntity.setId(resId);
		expReportTypeService.updateExpReportType(newEntity, new List<String>(newExpTypeIds));

		ExpReportTypeRepository.SearchFilter filter = new ExpReportTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{resId};

		List<ExpReportTypeService.ExpReportTypeWithExpTypeLink> savedEntity = expReportTypeService.searchExpReportTypeWithExpType(filter, null, null,null, null, false, null);
		System.assertNotEquals(null, savedEntity);
		System.assertEquals(true, newExpTypeIds.contains(savedEntity[0].expTypeIdList[0]));
		System.assertEquals(true, newExpTypeIds.contains(savedEntity[0].expTypeIdList[1]));
		System.assertEquals(true, newExpTypeIds.contains(savedEntity[0].expTypeIdList[2]));
	}

	/**
	 * Check if each field of the record is correctly set
	 */
	private static void assertSaveAllField(ExpReportTypeEntity expEntity, ExpReportTypeEntity actEntity) {
		System.assertEquals(expEntity.name, actEntity.name);
		System.assertEquals(expEntity.active, actEntity.active);
		System.assertEquals(expEntity.code, actEntity.code);
		System.assertEquals(expEntity.companyId, actEntity.companyId);
		System.assertEquals(expEntity.descriptionL0, actEntity.descriptionL0);
		System.assertEquals(expEntity.descriptionL1, actEntity.descriptionL1);
		System.assertEquals(expEntity.descriptionL2, actEntity.descriptionL2);
		System.assertEquals(expEntity.nameL0, actEntity.nameL0);
		System.assertEquals(expEntity.nameL1, actEntity.nameL1);
		System.assertEquals(expEntity.nameL2, actEntity.nameL2);
		System.assertEquals(expEntity.uniqKey, actEntity.uniqKey);

		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			System.assertEquals(expEntity.getExtendedItemDateId(i), actEntity.getExtendedItemDateId(i));
			System.assertEquals(expEntity.getExtendedItemDateUsedIn(i), actEntity.getExtendedItemDateUsedIn(i));
			System.assertEquals(expEntity.getExtendedItemDateRequiredFor(i), actEntity.getExtendedItemDateRequiredFor(i));

			System.assertEquals(expEntity.getExtendedItemLookupId(i), actEntity.getExtendedItemLookupId(i));
			System.assertEquals(expEntity.getExtendedItemLookupUsedIn(i), actEntity.getExtendedItemLookupUsedIn(i));
			System.assertEquals(expEntity.getExtendedItemLookupRequiredFor(i), actEntity.getExtendedItemLookupRequiredFor(i));

			System.assertEquals(expEntity.getExtendedItemPicklistId(i), actEntity.getExtendedItemPicklistId(i));
			System.assertEquals(expEntity.getExtendedItemPicklistUsedIn(i), actEntity.getExtendedItemPicklistUsedIn(i));
			System.assertEquals(expEntity.getExtendedItemPicklistRequiredFor(i), actEntity.getExtendedItemPicklistRequiredFor(i));

			System.assertEquals(expEntity.getExtendedItemTextId(i), actEntity.getExtendedItemTextId(i));
			System.assertEquals(expEntity.getExtendedItemTextUsedIn(i), actEntity.getExtendedItemTextUsedIn(i));
			System.assertEquals(expEntity.getExtendedItemTextRequiredFor(i), actEntity.getExtendedItemTextRequiredFor(i));
		}
	}

	/**
	 * Test for deleteExpReportTypeGroup method
	 * Check if the target record is correctly deleted
	 */
	@isTest static void deleteTest() {
		final Integer testExpReportTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity targetExpReportType = testData.createExpReportTypes('Test', testExpReportTypeSize).get(0);

		// delete
		(new ExpReportTypeService()).deleteExpReportType(targetExpReportType.id);

		// check if the target record is deleted
		ExpReportTypeEntity resExpReportType = (new ExpReportTypeRepository()).getEntity(targetExpReportType.id);
		System.assertEquals(null, resExpReportType);
	}

	/**
	 * Test for deleteExpReportTypeGroup method
	 * Check if exception is not thrown if the target record is already deleted
	 */
	@isTest static void deleteTestRecordNotFound() {
		final Integer testExpReportTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity targetExpReportType = testData.createExpReportTypes('Test', testExpReportTypeSize).get(0);

		// delete the target record
		(new ExpReportTypeRepository()).deleteEntity(targetExpReportType);

		// delete(the target record is already deleted)
		try {
			(new ExpReportTypeService()).deleteExpReportType(targetExpReportType.id);
		} catch (Exception e) {
			System.debug('Exception: ' + e.getStackTraceString());
			System.assert(false, 'Unexpected exception occurred.' + e.getMessage());
		}
	}

	/**
	 * Test for deleteExpReportType method
	 * Check if exception is thrown if the expense report type is related to expense report
	 */
	 @isTest static void deleteTestRelatedExpenseReport() {
	 	final Integer testExpReportTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity targetExpReportType = testData.createExpReportTypes('Test', testExpReportTypeSize).get(0);

		// create expense report
		ExpReport__c expReport = new ExpReport__c(
			Name = 'Test ExpReport',
			ExpReportTypeId__c = targetExpReportType.id
		);
		insert expReport;

		// delete
		try {
			(new ExpReportTypeService()).deleteExpReportType(targetExpReportType.id);
			TestUtil.fail('Exception was not thrown.');
		} catch (Exception e) {
			String msg = MESSAGE.Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	 }

	/**
	 * Test for deleteExpReportType method
	 * Check if exception is thrown if the expense report type is related to expense request
	 */
	 @isTest static void deleteTestRelatedExpenseRequest() {
	 	final Integer testExpReportTypeSize = 3;
		ExpReportTypeServiceTest.TestData testData = new TestData();
		ExpReportTypeEntity targetExpReportType = testData.createExpReportTypes('Test', testExpReportTypeSize).get(0);

		// create expense request
		ExpRequest__c expRequest = new ExpRequest__c(
			Name = 'Test ExpRequest',
			ScheduledDate__c = System.today() + 5,
			RecordingDate__c = System.today() + 5,
			ExpReportTypeId__c = targetExpReportType.id
		);
		insert expRequest;

		// delete
		try {
			(new ExpReportTypeService()).deleteExpReportType(targetExpReportType.id);
			TestUtil.fail('Exception was not thrown.');
		} catch (Exception e) {
			String msg = MESSAGE.Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	 }

	/**
	 * Test for deleteExpReportType method
	 * Check if Expense Type Link item is delete when the report is deleted
	 */
	@isTest static void deleteTestRelatedExpenseReportExpenseTypeLinkTest() {
		ExpReportTypeServiceTest.TestData testData = new TestData();
		List<ExpReportTypeEntity> newEntityList = testData.createExpReportTypes('NERT', 2);
		List<ExpType__c> expTypeSObject = ComTestDataUtility.createExpTypes('Type', testData.company.id, 4);
		Set<String> expTypeIdSet = new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[2].Id};
		Set<String> expTypeIdSet2 =new Set<String> {expTypeSObject[0].Id, expTypeSObject[1].Id, expTypeSObject[3].Id};
		List<ExpReportTypeExpTypeLinkEntity> expLink1 = createExpReportTypeExpTypeLinkEnity(newEntityList[0].id, expTypeIdSet);
		List<ExpReportTypeExpTypeLinkEntity> expLink2 = createExpReportTypeExpTypeLinkEnity(newEntityList[1].id, expTypeIdSet2);
		ExpReportTypeExpTypeLinkRepository linkRepo = new ExpReportTypeExpTypeLinkRepository();
		linkRepo.saveEntityList(expLink1);
		linkRepo.saveEntityList(expLink2);

		List<ExpReportTypeExpTypeLinkEntity> expResult = linkRepo.getEntityListByExpReportTypeId(new Set<Id>{newEntityList[0].id});
		System.assertEquals(3, expResult.size());

		(new ExpReportTypeService()).deleteExpReportType(newEntityList[0].id);

		expResult = linkRepo.getEntityListByExpReportTypeId(new Set<Id>{newEntityList[0].id});

		System.assertEquals(0, expResult.size());
	}

	/**
	 * Create ExpReportTypeExpTypeLink Entity List
	 * @param reportId Report Id to link
	 * @param expTypeIdSet Expense Id Set to link
	 * @return ExpReportTypeExpTypeLinkEntity List
	 */
	private static List<ExpReportTypeExpTypeLinkEntity> createExpReportTypeExpTypeLinkEnity(Id reportId, Set<String> expTypeIdSet) {
		// Create List of Report Type and Expense Type Link Entity
		List<ExpReportTypeExpTypeLinkEntity> entityList = new List<ExpReportTypeExpTypeLinkEntity>();
		for (Id expTypeId : expTypeIdSet) {
			ExpReportTypeExpTypeLinkEntity entity = new ExpReportTypeExpTypeLinkEntity();
			entity.expReportTypeId = reportId;
			entity.expTypeId = expTypeId;

			entityList.add(entity);
		}
		return entityList;
	}

	/*
	 * Assert that Cannot Change Reference error is thrown when updating the given reportType
	 * @param reportTypeEntity Report Type containing the updated values
	 */
	private static void assertReferencedExceptionIsThrown(ExpReportTypeEntity reportTypeEntity) {
		App.IllegalStateException resEx;
		try {
			new ExpReportTypeService().updateExpReportType(reportTypeEntity, null);
		} catch(App.IllegalStateException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした: Exception Not Thrown!');
		String exMessage = ComMessage.msg().Com_Err_CannotChangeReference;
		System.assertEquals(exMessage, resEx.getMessage());
	}
}