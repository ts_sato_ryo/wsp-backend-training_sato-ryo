/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * trigger handler
 */
public with sharing class ExpRequestApprovalTriggerHandler extends RequestTriggerHandler {

	public override Set<Id> getApprovedRequestIds() {
		return approvedRequestMap.keySet();
	}

	public override Set<Id> getDisabledRequestIds() {
		return disabledRequestMap.keySet();
	}

	private Map<Id, ExpRequestApproval__c> approvedRequestMap = new Map<Id, ExpRequestApproval__c>();
	private Map<Id, ExpRequestApproval__c> disabledRequestMap = new Map<Id, ExpRequestApproval__c>();

	/**
	 * Constructor
	 * @param oldMap
	 * @param newMap
	 */
	public expRequestApprovalTriggerHandler(Map<Id, ExpRequestApproval__c> oldMap, Map<Id, ExpRequestApproval__c> newMap) {
		super();
		this.init(oldMap, newMap);
	}

	/**
	 * Update applied data
	 */
	public override void applyProcessInfo() {
		// Get approval history
		Set<Id> targetObjectIds = new Set<Id>();
		targetObjectIds.addAll(approvedRequestIds);
		targetObjectIds.addAll(disabledRequestIds);
		Map<Id, ProcessInstanceStep> processInfoMap = getProcessInfo(targetObjectIds);

		// Update approved request records
		for (ExpRequestApproval__c request : approvedRequestMap.values()) {
			if (!processInfoMap.containsKey(request.Id)) {
				continue;
			}

			ProcessInstanceStep pis = processInfoMap.get(request.Id);
			request.LastApproverId__c = pis.ActorId;
			request.LastApproveTime__c = pis.CreatedDate;
			request.ProcessComment__c = pis.Comments;
		}

		// Update discared request records
		for (ExpRequestApproval__c request : disabledRequestMap.values()) {
			request.ConfirmationRequired__c = true;

			if (!processInfoMap.containsKey(request.Id)) {
				continue;
			}

			ProcessInstanceStep pis = processInfoMap.get(request.Id);
			// Rejected
			if (pis.StepStatus == STEP_STATUS_REJECTED) {
				request.CancelType__c = AppCancelType.REJECTED.value;
				request.ProcessComment__c = pis.Comments;
			} else if (pis.StepStatus == STEP_STATUS_REMOVED) {
				request.CancelType__c = AppCancelType.REMOVED.value;
				request.CancelComment__c = pis.Comments;
			}
		}
	}

	/**
	 * Get approved / discarded request IDs
	 * @param oldMap
	 * @param newMap
	 */
	private void init(Map<Id, ExpRequestApproval__c> oldMap, Map<Id, ExpRequestApproval__c> newMap) {
		for (Id requestId : newMap.keySet()) {
			ExpRequestApproval__c requestNew = newMap.get(requestId);
			ExpRequestApproval__c requestOld = oldMap.get(requestId);

			// Approved
			if (requestNew.Status__c == ApprovalService.Status.Approved.name() &&
				requestOld.Status__c != ApprovalService.Status.Approved.name())
			{
				approvedRequestMap.put(requestNew.Id, requestNew);
				continue;
			}
			// Discarded
			if (requestNew.Status__c == ApprovalService.Status.Disabled.name() &&
				requestOld.Status__c != ApprovalService.Status.Disabled.name())
			{
				disabledRequestMap.put(requestNew.Id, requestNew);
				continue;
			}
		}
	}
}