/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttLeaveOfAbsenceEntityのテストクラス
 */
@isTest
private class AttLeaveOfAbsenceEntityTest {

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.code = 'AttLeaveOfAbsenceCode';
		System.assertEquals('CompanyCode-AttLeaveOfAbsenceCode', entity.createUniqKey('CompanyCode'));
	}

	/*
	 * ユニークキー作成テスト
	 * 会社コードが空の場合、例外が発生することを確認する
	 */
	@isTest static void createUniqKeyTestWhenCompanyCodeIsBrankThenThrough() {
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.code = 'AttLeaveOfAbsenceCode';
		try {
			entity.createUniqKey('');
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// OK
		}
	}

	/*
	 * ユニークキー作成テスト
	 * コードが空の場合、例外が発生することを確認する
	 */
	@isTest static void createUniqKeyTestWhenCodeIsBrankThenThrough() {
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		try {
			entity.createUniqKey('CompanyCode');
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}
	}

	/*
	 * エンティティコピーのテスト
	 * ID以外の項目をコピーしていることを確認する
	 */
	@isTest static void copyTest() {
		AttLeaveOfAbsenceEntity src = new AttLeaveOfAbsenceEntity();
		src.companyId = 'companyId';
		src.code = 'code';
		src.uniqKey = 'uniqKey';
		src.name = 'name';
		src.nameL0 = 'nameL0';
		src.nameL1 = 'nameL1';
		src.nameL2 = 'nameL2';
		src.isRemoved = true;

		AttLeaveOfAbsenceEntity dest = src.copy();
		System.assertEquals('companyId', dest.companyId);
		System.assertEquals('code', dest.code);
		System.assertEquals('uniqKey', dest.uniqKey);
		System.assertEquals('name', dest.name);
		System.assertEquals('nameL0', dest.nameL0);
		System.assertEquals('nameL1', dest.nameL1);
		System.assertEquals('nameL2', dest.nameL2);
		System.assertEquals(true, dest.isRemoved);
	}

	/*
	 * resetChangedのテスト
	 * 項目の変更情報がリセットされていることを確認する
	 */
	@isTest static void resetChangedTest() {
		AttLeaveOfAbsenceEntity src = new AttLeaveOfAbsenceEntity();
		src.companyId = 'companyId';
		src.code = 'code';
		src.uniqKey = 'uniqKey';
		src.name = 'name';
		src.nameL0 = 'nameL0';
		src.nameL1 = 'nameL1';
		src.nameL2 = 'nameL2';
		src.isRemoved = true;

		src.resetChanged();

		System.assertEquals(false, src.isChanged(AttLeaveOfAbsenceEntity.Field.CODE));
		System.assertEquals(false, src.isChanged(LogicalDeleteEntity.Field.IS_REMOVED));
	}
}