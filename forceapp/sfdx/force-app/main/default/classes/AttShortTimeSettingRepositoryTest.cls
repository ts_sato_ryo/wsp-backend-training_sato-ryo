/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * AttShortTimeSettingRepositoryのテスト
 */
@isTest
private class AttShortTimeSettingRepositoryTest {

	/** テストデータクラス */
	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj {get; private set;}
		/** タグオブジェクト */
		public ComTag__c tagObj {get; private set;}

		/** コンストラクタ */
		public TestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
			tagObj = ComTestDataUtility.createTag('Test Tag', TagType.SHORTEN_WORK_REASON,
					this.companyObj.id, 1, false);
		}

		/**
		 * 短時間勤務設定のテストデータを作成する
		 */
		public List<AttShortTimeWorkSettingBase__c>  createAttShortTimeSettingsWithHistories(String name, Integer baseSize, Integer historySize) {
			return ComTestDataUtility.createAttShortTimeSettingsWithHistories(
					name, this.companyObj.Id, this.tagObj.Id, baseSize, historySize);
		}

		/**
		 * 短時間勤務設定履歴エンティティを作成する(DB保存なし)
		 */
		public AttShortTimeSettingHistoryEntity createAttShortTimeSettingHistory(String name, AppDate validFrom, AppDate validTo) {
			AttShortTimeSettingHistoryEntity history = new AttShortTimeSettingHistoryEntity();
			history.name = name;
			history.historyComment = '履歴コメント';
			history.validFrom = validFrom;
			history.validTo = validTo;
			history.isRemoved = false;
			history.uniqKey = name + '_' ; validFrom.formatYYYYMMDD();
			history.nameL0 = '短時間勤務設定_L0';
			history.nameL1 = '短時間勤務設定_L1';
			history.nameL2 = '短時間勤務設定_L2';
			return history;
		}


	}

	/**
	 * sarchEntityのテスト
	 * 検索できることを確認する
	 */
	@isTest static void sarchEntityTest() {

		final Integer baseSize = 3;
		final Integer historySize = 2;
		AttShortTimeSettingRepositoryTest.TestData testData = new TestData();
		List<AttShortTimeWorkSettingBase__c> baseList = testData.createAttShortTimeSettingsWithHistories('Test', baseSize, historySize);
		AttShortTimeWorkSettingBase__c targetBase = baseList[1];
		List<AttShortTimeWorkSettingHistory__c> targetHistoryList = [
				SELECT Id, Name, Removed__c, ValidFrom__c, ValidTo__c FROM AttShortTimeWorkSettingHistory__c WHERE BaseId__c = :targetBase.Id
				ORDER BY ValidFrom__c Asc];
		AttShortTimeWorkSettingHistory__c targetHistory = targetHistoryList[0];


		AttShortTimeSettingRepository repo = new AttShortTimeSettingRepository();
		AttShortTimeSettingRepository.SearchFilter filter;
		List<AttShortTimeSettingBaseEntity> resBaseList;

		// ベースIDで検索
		// 全ての項目の値が取得できていることを確認する
		filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{targetBase.Id};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetBase.Id, resBaseList[0].id);
		System.assertEquals(historySize, resBaseList[0].getHistoryList().size());
		AttShortTimeWorkSettingBase__c expBaseObj = getBaseObj(resBaseList[0].id);
		AttShortTimeWorkSettingHistory__c expHistoryObj = getHistoryObjList(resBaseList[0].id).get(0);
		assertBaseEntity(expBaseObj, resBaseList[0]);
		assertHistoryEntity(expHistoryObj, resBaseList[0].getHistory(0));

		// 履歴IDで検索
		filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{targetHistory.Id};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetHistory.Id, resBaseList[0].getHistory(0).id);

		// 会社IDで検索
		ComCompany__c targetCompany = ComTestDataUtility.createCompany('Tareget Company', testData.countryObj.Id);
		AttShortTimeWorkSettingBase__c patternCompany = baseList[2];
		patternCompany.CompanyId__c = targetCompany.Id;
		update patternCompany;
		filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.companyIds = new Set<Id>{targetCompany.Id};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(patternCompany.Id, resBaseList[0].id);

		// コード(完全一致)
		filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.codes = new Set<String>{targetBase.Code__c};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetBase.Id, resBaseList[0].id);

		// 論理削除されている履歴を取得対象にする場合はtrue
		targetHistory.Removed__c = true;
		update targetHistory;
		filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{targetHistory.Id};
		// falseなのでtargetHistoryは取得できない
		filter.includeRemoved = false;
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(0, resBaseList.size());
		// trueなのでtargetHistoryは取得できる
		filter.includeRemoved = true;
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetHistory.Id, resBaseList[0].getHistory(0).id);
	}

	/**
	 * エンティティ取得のテスト
	 * 指定した対象日に有効な履歴が取得できることを確認する
	 */
	@isTest static void searchEntityTestValidDate() {

		// テストデータ作成
		final Integer baseSize = 3;
		final Integer historySize = 1;
		AttShortTimeSettingRepositoryTest.TestData testData = new TestData();
		List<AttShortTimeWorkSettingBase__c> bases = testData.createAttShortTimeSettingsWithHistories('Test', baseSize, historySize);
		AttShortTimeWorkSettingBase__c targetBase = getBaseObj(bases[0].Id);
		AttShortTimeWorkSettingHistory__c targetHistory = getHistoryObjList(targetBase.Id).get(0);
		AttShortTimeWorkSettingHistory__c targetHistory2 = targetHistory.clone();
		targetHistory2.ValidFrom__c = targetHistory.ValidTo__c;
		targetHistory2.ValidTo__c = targetHistory2.ValidFrom__c.addMonths(1);
		targetHistory2.UniqKey__c = targetBase.Code__c + '_' + targetHistory2.ValidFrom__c;
		AttShortTimeWorkSettingHistory__c targetHistory3 = targetHistory.clone();
		targetHistory3.ValidFrom__c = targetHistory2.ValidTo__c;
		targetHistory3.ValidTo__c = targetHistory3.ValidFrom__c.addMonths(1);
		targetHistory3.UniqKey__c = targetBase.Code__c + '_' + targetHistory3.ValidFrom__c;
		insert new List<AttShortTimeWorkSettingHistory__c>{targetHistory2, targetHistory3};

		AttShortTimeSettingRepository repo = new AttShortTimeSettingRepository();
		List<AttShortTimeSettingBaseEntity> resBases;
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{targetBase.Id};

		// 取得対象日に履歴の有効期間開始日を指定
		filter.targetDate = AppDate.valueOf(targetHistory2.ValidFrom__c);
		resBases = repo.searchEntity(filter);
		System.assertEquals(1, resBases.size());
		System.assertEquals(targetBase.Id, resBases[0].id);
		System.assertEquals(targetHistory2.Id, resBases[0].getHistory(0).id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		filter.targetDate = AppDate.valueOf(targetHistory3.ValidTo__c.addDays(-1));
		resBases = repo.searchEntity(filter);
		System.assertEquals(1, resBases.size());
		System.assertEquals(targetBase.Id, resBases[0].id);
		System.assertEquals(targetHistory3.Id, resBases[0].getHistory(0).id);

		// 取得対象日に最新履歴の失効日を指定
		// 取得できないはず
		filter.targetDate = AppDate.valueOf(targetHistory3.ValidTo__c);
		resBases = repo.searchEntity(filter);
		System.assertEquals(0, resBases.size());

	}

	/**
	 * 新規の工数設定履歴エンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		AttShortTimeSettingRepositoryTest.TestData testData = new TestData();
		AttShortTimeSettingBaseEntity base = new AttShortTimeSettingBaseEntity();
		AttShortTimeSettingHistoryEntity history = new AttShortTimeSettingHistoryEntity();
		base.name = 'Test';
		base.code = 'Test Code';
		base.uniqKey = testData.companyObj.Code__c + '-' + base.code;
		base.companyId = testData.companyObj.Id;
		base.reasonId = testData.tagObj.Id;
		base.workSystem = AttWorkSystem.JP_FLEX;
		base.addHistory(history);

		history.name = base.name + ' History';
		history.uniqKey = base.uniqKey + '_History';
		history.historyComment = '改定コメント';
		history.validFrom = AppDate.today();
		history.validTo = history.validFrom.addMonths(1);
		history.nameL0 = history.name + '_L0';
		history.nameL1 = history.name + '_L1';
		history.nameL2 = history.name + '_L2';
		history.isRemoved = false;
		history.allowableTimeOfShortWork = 300;
		history.allowableTimeOfLateArrival = 120;
		history.allowableTimeOfEarlyLeave = 180;
		history.allowableTimeOfIrregularRest = 240;

		Test.startTest();
			Repository.SaveResultWithChildren result = new AttShortTimeSettingRepository().saveEntity(base);
		Test.stopTest();

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		Id baseId = result.details[0].id;
		AttShortTimeWorkSettingBase__c resBaseObj = getBaseObj(baseId);
		assertSavedBaseObj(base, resBaseObj);
		List<AttShortTimeWorkSettingHistory__c> resHistoryList = getHistoryObjList(baseId);
		System.assertEquals(1, resHistoryList.size());
		AttShortTimeWorkSettingHistory__c resHistoryObj = resHistoryList[0];
		assertSavedHistoryObj(base.getHistory(0), resHistoryObj, baseId);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		AttShortTimeWorkSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		AttShortTimeSettingRepository repository = new AttShortTimeSettingRepository(true);

		// 検索（DBから取得）
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		filter.historyIds = new Set<Id>{history.Id};
		AttShortTimeSettingBaseEntity shortTimeSetting = repository.searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttShortTimeSettingBaseEntity cachedShortTimeSetting = repository.searchEntity(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(shortTimeSetting.id, cachedShortTimeSetting.id);
		System.assertNotEquals('NotCache', cachedShortTimeSetting.code);
		System.assertNotEquals('NotCache', cachedShortTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		AttShortTimeWorkSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// 検索（DBから取得）
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		filter.historyIds = new Set<Id>{history.Id};
		AttShortTimeSettingBaseEntity shortTimeSetting = new AttShortTimeSettingRepository(true).searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttShortTimeSettingBaseEntity cachedShortTimeSetting = new AttShortTimeSettingRepository(true).searchEntity(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(shortTimeSetting.id, cachedShortTimeSetting.id);
		System.assertNotEquals('NotCache', cachedShortTimeSetting.code);
		System.assertNotEquals('NotCache', cachedShortTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。（短時間勤務設定ベース検索）
	 */
	@isTest static void searchBaseNotFromCacheDifferentCondition() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		AttShortTimeWorkSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		AttShortTimeSettingRepository repository = new AttShortTimeSettingRepository(true);

		// 検索（DBから取得）
		List<Id> baseIds = new List<Id>{base.Id};
		AttShortTimeSettingBaseEntity shortTimeSetting = repository.getBaseEntityList(baseIds, false)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		baseIds.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		AttShortTimeSettingBaseEntity searchedShortTimeSetting = repository.getBaseEntityList(baseIds, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(shortTimeSetting.id, searchedShortTimeSetting.id);
		System.assertEquals('NotCache', searchedShortTimeSetting.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。（短時間勤務設定履歴検索）
	 */
	@isTest static void searchHistoryNotFromCacheDifferentCondition() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		AttShortTimeWorkSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		AttShortTimeSettingRepository repository = new AttShortTimeSettingRepository(true);

		// 検索（DBから取得）
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		AttShortTimeSettingRepository.HistorySortOrder sortOrder = AttShortTimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		AttShortTimeSettingHistoryEntity shortTimeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.baseIds.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		AttShortTimeSettingHistoryEntity searchedShortTimeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(shortTimeSetting.id, searchedShortTimeSetting.id);
		System.assertEquals('NotCache', searchedShortTimeSetting.historyComment);
	}


	/**
	 * 短時間勤務設定ベースが同一で異なる履歴を検索した場合、条件に一致する履歴のみが取得結果のベースに紐づくことを検証する。（短時間勤務設定ベース + 履歴検索）
	 */
	@isTest static void searchEntityWityHistoryFromCacheSameBase() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		delete getHistoryObjList(base.Id).get(0);

		// 短時間勤務設定履歴を3件作成
		AttShortTimeSettingHistoryEntity history1 = testData.createAttShortTimeSettingHistory('history1', AppDate.newInstance(2019, 4, 1), AppDate.newInstance(2019, 5, 1));
		history1.baseId = base.Id;
		AttShortTimeSettingHistoryEntity history2 = testData.createAttShortTimeSettingHistory('history2', AppDate.newInstance(2019, 5, 1), AppDate.newInstance(2019, 6, 1));
		history2.baseId = base.Id;
		AttShortTimeSettingHistoryEntity history3 = testData.createAttShortTimeSettingHistory('history3', AppDate.newInstance(2019, 6, 1), AppDate.newInstance(2019, 7, 1));
		history3.baseId = base.Id;
		new AttShortTimeSettingRepository().saveHistoryEntityList(new List<AttShortTimeSettingHistoryEntity>{history1, history2, history3});

		// キャッシュをonでインスタンスを生成
		AttShortTimeSettingRepository repository = new AttShortTimeSettingRepository(true);

		// 検索（ベース1件 + 履歴3件）
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		AttShortTimeSettingRepository.HistorySortOrder sortOrder = AttShortTimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		AttShortTimeSettingBaseEntity shortTimeSetting = repository.searchEntityWithHistory(filter, false, sortOrder)[0];
		System.assertEquals(base.id, shortTimeSetting.id);
		System.assertEquals(3, shortTimeSetting.getHistoryList().size());

		// 異なる条件で同じベースを再検索（ベース1件 + 履歴1件）
		filter.targetDate = AppDate.newInstance(2019, 5, 1);
		AttShortTimeSettingBaseEntity searchedShortTimeSetting = repository.searchEntityWithHistory(filter, false, sortOrder)[0];
		System.assertEquals(shortTimeSetting.id, searchedShortTimeSetting.id);
		System.assertEquals(1, searchedShortTimeSetting.getHistoryList().size());
	}

	/**
	 * ソート順が異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheDifferentSortOrder() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		AttShortTimeWorkSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		AttShortTimeSettingRepository repository = new AttShortTimeSettingRepository(true);

		// 検索（DBから取得）
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{history.Id};
		AttShortTimeSettingRepository.HistorySortOrder sortOrder = AttShortTimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		AttShortTimeSettingHistoryEntity shortTimeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 異なるソート順で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		sortOrder = AttShortTimeSettingRepository.HistorySortOrder.VALID_FROM_DESC;
		AttShortTimeSettingHistoryEntity searchedShortTimeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(shortTimeSetting.id, searchedShortTimeSetting.id);
		System.assertEquals('NotCache', searchedShortTimeSetting.historyComment);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		AttShortTimeWorkSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		AttShortTimeSettingRepository repository = new AttShortTimeSettingRepository(true);

		// 検索（DBから取得）
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		AttShortTimeSettingRepository.HistorySortOrder sortOrder = AttShortTimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		AttShortTimeSettingBaseEntity shortTimeSetting = repository.searchEntityWithHistory(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttShortTimeSettingBaseEntity searchedShortTimeSetting = repository.searchEntityWithHistory(filter, true, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertNotEquals(shortTimeSetting, searchedShortTimeSetting);
		System.assertEquals('NotCache', searchedShortTimeSetting.code);
		System.assertEquals('NotCache', searchedShortTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		TestData testData = new TestData();
		AttShortTimeWorkSettingBase__c base = testData.createAttShortTimeSettingsWithHistories('Test', 1, 1)[0];
		AttShortTimeWorkSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをoffでインスタンスを生成
		AttShortTimeSettingRepository repository = new AttShortTimeSettingRepository();

		// 検索（DBから取得）
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		filter.historyIds = new Set<Id>{history.Id};
		AttShortTimeSettingBaseEntity shortTimeSetting = repository.searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttShortTimeSettingBaseEntity searchedShortTimeSetting = repository.searchEntity(filter)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertNotEquals(shortTimeSetting, searchedShortTimeSetting);
		System.assertEquals('NotCache', searchedShortTimeSetting.code);
		System.assertEquals('NotCache', searchedShortTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		AttShortTimeSettingRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new AttShortTimeSettingRepository().isEnabledCache);
	}

	/**
	 * 指定したIDの短時間勤務設定ベースオブジェクトを取得する
	 */
	private static AttShortTimeWorkSettingBase__c getBaseObj(Id baseId) {
		return [
				SELECT
					Id, Name, CurrentHistoryId__c, Code__c, CompanyId__c, UniqKey__c,
					ReasonId__c, WorkSystem__c,
					ReasonId__r.Name_L0__c, ReasonId__r.Name_L1__c, ReasonId__r.Name_L2__c
				FROM
					AttShortTimeWorkSettingBase__c
				WHERE Id = :baseId
		];
	}

	/**
	 * 指定したIDの短時間勤務設定履歴オブジェクトを取得する
	 */
	private static List<AttShortTimeWorkSettingHistory__c> getHistoryObjList(Id baseId) {
		return [
				SELECT
					Id, Name, BaseId__c, UniqKey__c, Removed__c,
					HistoryComment__c, ValidFrom__c, ValidTo__c,
					Name_L0__c, Name_L1__c, Name_L2__c,
					AllowableTimeOfShortWork__c,
					AllowableTimeOfLateArrival__c, AllowableTimeOfEarlyLeave__c,
					AllowableTimeOfIrregularRest__c
				FROM
					AttShortTimeWorkSettingHistory__c
				WHERE
					BaseId__c = :baseId
		];
	}

	/**
	 * ベースエンティティが正しく取得できているかを検証する
	 */
	private static void assertBaseEntity(AttShortTimeWorkSettingBase__c expObj, AttShortTimeSettingBaseEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		// System.assertEquals(expObj.CurrentHistoryId__c, actEntity.currentHistoryId);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);

		System.assertEquals(expObj.ReasonId__c, actEntity.reasonId);
		if (actEntity.reasonId != null) {
			System.assertEquals(expObj.ReasonId__r.Name_L0__c, actEntity.reason.nameL.valueL0);
			System.assertEquals(expObj.ReasonId__r.Name_L1__c, actEntity.reason.nameL.valueL1);
			System.assertEquals(expObj.ReasonId__r.Name_L2__c, actEntity.reason.nameL.valueL2);
		}

		System.assertEquals(expObj.WorkSystem__c, actEntity.workSystem.value);
	}

	/**
	 * 履歴エンティティが正しく取得できているかを検証する
	 */
	private static void assertHistoryEntity(AttShortTimeWorkSettingHistory__c expObj, AttShortTimeSettingHistoryEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.BaseId__c, actEntity.baseId);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.HistoryComment__c, actEntity.historyComment);
		System.assertEquals(expObj.ValidFrom__c, actEntity.validFrom.getDate());
		System.assertEquals(expObj.ValidTo__c, actEntity.validTo.getDate());
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);

		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.AllowableTimeOfShortWork__c, actEntity.allowableTimeOfShortWork);
		System.assertEquals(expObj.AllowableTimeOfLateArrival__c, actEntity.allowableTimeOfLateArrival);
		System.assertEquals(expObj.AllowableTimeOfEarlyLeave__c, actEntity.allowableTimeOfEarlyLeave);
		System.assertEquals(expObj.AllowableTimeOfIrregularRest__c, actEntity.allowableTimeOfIrregularRest);
	}

	/**
	 * AttShortTimeWorkSettingBase__cに正しく保存できているかを検証する
	 */
	private static void assertSavedBaseObj(AttShortTimeSettingBaseEntity expEntity, AttShortTimeWorkSettingBase__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.currentHistoryId, actObj.CurrentHistoryId__c);
		System.assertEquals(expEntity.reasonId, actObj.ReasonId__c);
		System.assertEquals(expEntity.workSystem.value, actObj.WorkSystem__c);
	}

	/**
	 * AttShortTimeWorkSettingHistory__cに正しく保存できているかを検証する
	 */
	private static void assertSavedHistoryObj(AttShortTimeSettingHistoryEntity expEntity, AttShortTimeWorkSettingHistory__c actObj, Id baseId) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(baseId, actObj.BaseId__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.historyComment, actObj.HistoryComment__c);
		System.assertEquals(expEntity.validFrom.getDate(), actObj.ValidFrom__c);
		System.assertEquals(expEntity.validTo.getDate(), actObj.ValidTo__c);

		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.allowableTimeOfShortWork, actObj.AllowableTimeOfShortWork__c);
		System.assertEquals(expEntity.allowableTimeOfLateArrival, actObj.AllowableTimeOfLateArrival__c);
		System.assertEquals(expEntity.allowableTimeOfEarlyLeave, actObj.AllowableTimeOfEarlyLeave__c);
		System.assertEquals(expEntity.allowableTimeOfIrregularRest, actObj.AllowableTimeOfIrregularRest__c);
	}
}
