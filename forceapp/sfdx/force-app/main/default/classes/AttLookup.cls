/**
 * 参照の項目の情報を持つ値オブジェクト
 * 値オブジェクトはサブクラスとして定義してください。
 */
public with sharing class AttLookup {

	/** 部署の情報*/
	public class Department extends AttLookup.Master {
		/** 部署名 */
		public AppMultiString name {public get; private set;}
		/** コンストラクタ */
		public Department(AppMultiString name, AppDate validFrom, AppDate validTo, Boolean isRemoved) {
			super(validFrom, validTo, isRemoved);
			this.name = name;
		}

		/**
		 * オブジェクトの値が等しいかどうか判定する
		 * @param compare 比較対象のオブジェクト
		 */
		public override Boolean equals(Object compare) {
			Boolean ret;
			if (compare instanceof Department) {
				Department compareDept = (Department)compare;
				if (this.name != compareDept.name) {
					ret = false;
				} else if (! equalsMasterField(compareDept)) {
					ret = false;
				} else {
					ret = true;
				}
			} else {
				ret = false;
			}
			return ret;
		}
	}

	/** 社員の付加情報(参照専用)*/
	public class Employee extends AttLookup.Master {
		/** 社員の氏名 */
		public AppPersonName fullName {public get; private set;}
		/** 顔写真URL */
		public String photoUrl {public get; private set;}
		public Employee(AppPersonName fullName, String photoUrl, AppDate validFrom, AppDate validTo, Boolean isRemoved) {
			super(validFrom, validTo, isRemoved);
			this.fullName = fullName;
			this.photoUrl = photoUrl;
		}

		/**
		 * オブジェクトの値が等しいかどうか判定する
		 * @param compare 比較対象のオブジェクト
		 */
		public override Boolean equals(Object compare) {
			Boolean ret;
			if (compare instanceof Employee) {
				Employee compareEmp = (Employee)compare;
				if (this.fullName != compareEmp.fullName) {
					ret = false;
				} else if (! equalsMasterField(compareEmp)) {
					ret = false;
				} else {
					ret = true;
				}
			} else {
				ret = false;
			}
			return ret;
		}
	}

	/** マスターの情報の値オブジェクト*/
	public abstract class Master extends ValueObject {
		/** 有効開始日 */
		public AppDate validFrom {public get; private set;}
		/** 有効終了日 */
		public AppDate validTo {public get; private set;}
		/** 削除フラグ */
		public Boolean isRemoved {public get; private set;}

		public Master(AppDate validFrom, AppDate validTo, Boolean isRemoved) {
			this.validFrom = validFrom;
			this.validTo = validTo;
			this.isRemoved = isRemoved;
		}

		/**
		 * オブジェクトの値が等しいかどうか判定する
		 * @param compare 比較対象のオブジェクト
		 */
		public Boolean equalsMasterField(Master compare) {
			Boolean ret;
			if ((this.isRemoved != compare.isRemoved)
					|| (this.validFrom != compare.validFrom)
					|| (this.validTo != compare.validTo)) {
					ret = false;
			} else {
				ret = true;
			}
			return ret;
		}
	}
}
