/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通
 *
 * @description DelegatedApproverEntityのテストクラス
 */
@isTest
private class DelegatedApproverSettingEntityTest {

	/**
	 * nameのテスト
	 * 不正な値を設定した場合に例外が発生することを確認する
	 */
	@isTest static void nameTest() {
		try {
			DelegatedApproverSettingEntity entity = new DelegatedApproverSettingEntity();
			entity.name = '';
			System.assert(false, '例外が発生しませんでした');
		} catch(App.ParameterException e) {
			System.assert(true);
		} catch(Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}
	}

	/**
	 * isChangedのテスト
	 * 変更情報が正しく取得できることを確認する
	 */
	@isTest static void isChangedTest() {
		DelegatedApproverSettingEntity entity = new DelegatedApproverSettingEntity();

		// 変更前
		System.assertEquals(false, entity.isChanged(DelegatedApproverSettingEntity.Field.NAME));

		// 変更後
		entity.name = 'aaaa';
		System.assertEquals(true, entity.isChanged(DelegatedApproverSettingEntity.Field.NAME));
	}

	/**
	 * resetChangedのテスト
	 * 変更情報がリセットできることを確認する
	 */
	@isTest static void resetChangedTest() {
		DelegatedApproverSettingEntity entity = new DelegatedApproverSettingEntity();

		// 変更してみる
		entity.name = 'aaaa';
		System.assertEquals(true, entity.isChanged(DelegatedApproverSettingEntity.Field.NAME));

		// 変更情報リセット
		entity.resetChanged();
		System.assertEquals(false, entity.isChanged(DelegatedApproverSettingEntity.Field.NAME));
	}
}