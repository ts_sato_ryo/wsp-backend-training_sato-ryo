/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 社員ベースエンティティのテスト
 */
@isTest
private class EmployeeBaseEntityTest {

	/**
	 * テストデータ
	 */
	private class EntityTestData extends TestData.TestDataEntity {

		/*
		 * テスト用の社員ベース+履歴エンティティを作成する(DB保存あり)
		 */
		public List<EmployeeBaseEntity> createEmployeeEntityList(String name, Integer baseSize, Integer historySize) {
			List<EmployeeBaseEntity> bases = new List<EmployeeBaseEntity>();
			for (Integer bi = 0; bi < baseSize; bi++) {
				EmployeeBaseEntity base = new EmployeeBaseEntity();
				base.name = name + '_' + bi;
				base.code = base.name + '01';
				base.uniqKey = this.company.code + '-' + base.code;
				base.companyId = this.company.id;
				base.userId = UserInfo.getUserId();
				base.firstNameL0 = name + '名_L0';
				base.firstNameL1 = name + '名_L1';
				base.firstNameL2 = name + '名_L2';
				base.middleNameL0 = name + 'ミドル_L0';
				base.middleNameL1 = name + 'ミドル_L1';
				base.middleNameL2 = name + 'ミドル_L2';
				base.lastNameL0 = name + '姓_L0';
				base.lastNameL1 = name + '姓_L1';
				base.lastNameL2 = name + '姓_L2';
				base.displayNameL0 = name + '表示名_L0';
				base.displayNameL1 = name + '表示名_L1';
				base.displayNameL2 = name + '表示名_L2';
				base.validFrom = AppDate.today();
				base.validTo = base.validFrom.addDays(30);

				AppDate validFrom = AppDate.newInstance(2018, 1, 1);
				for (Integer i = 0; i < historySize; i++) {
					AppDate validTo = validFrom.addMonths(1);
					base.addHistory(createEmployeeHistory(base.name + '_' + i, base.code, validFrom, validTo));
					validFrom = validTo;
				}
				bases.add(base);
			}

			EmployeeRepository repo = new EmployeeRepository();
			Repository.SaveResultWithChildren result = repo.saveEntityList(bases);
			List<Id> baseIds = new List<Id>();
			for (Integer bi = 0; bi < baseSize; bi++) {
				baseIds.add(result.details[bi].id);
			}
			bases = repo.getEntityList(baseIds, null);

			return bases;
		}

		/**
		 * 社員履歴エンティティを作成する(DB保存なし)
		 */
		public EmployeeHistoryEntity createEmployeeHistory(String name, String code, AppDate validFrom, AppDate validTo) {
			EmployeeHistoryEntity history = new EmployeeHistoryEntity();
			history.name = name;
			history.historyComment = '履歴コメント';
			history.validFrom = validFrom;
			history.validTo = validTo;
			history.isRemoved = false;
			history.uniqKey = code + '_' + validFrom.formatYYYYMMDD();
			history.departmentId = this.department.Id;
			history.titleL0 = '役職_L0';
			history.titleL1 = '役職_L1';
			history.titleL2 = '役職_L2';
			history.gradeId = this.grade.Id;
			// history.managerId = this.manager == null ? null : this.manager.Id;
			history.calendarId = this.calendar.Id;
			history.workingTypeId = this.workingType.Id;
			history.timeSettingId = this.timeSetting.Id;
			history.permissionId = this.permission.Id;

			return history;
		}
	}


	/**
	 * エンティティの複製ができることを確認する
	 */
	@isTest static void copyTest() {
		EntityTestData testData = new EntityTestData();
		List<EmployeeBaseEntity> empList = testData.createEmployeeEntityList('Entityテスト', 1, 1);
		EmployeeBaseEntity targetEmp = empList[0];

		EmployeeBaseEntity copyEntity = targetEmp.copy();
		System.assertEquals(targetEmp.name, copyEntity.name);
		System.assertEquals(targetEmp.code, copyEntity.code);
		System.assertEquals(targetEmp.uniqKey, copyEntity.uniqKey);
		System.assertEquals(targetEmp.companyId, copyEntity.companyId);
		System.assertEquals(targetEmp.firstNameL0, copyEntity.firstNameL0);
		System.assertEquals(targetEmp.firstNameL1, copyEntity.firstNameL1);
		System.assertEquals(targetEmp.firstNameL2, copyEntity.firstNameL2);
		System.assertEquals(targetEmp.middleNameL0, copyEntity.middleNameL0);
		System.assertEquals(targetEmp.middleNameL1, copyEntity.middleNameL1);
		System.assertEquals(targetEmp.middleNameL2, copyEntity.middleNameL2);
		System.assertEquals(targetEmp.lastNameL0, copyEntity.lastNameL0);
		System.assertEquals(targetEmp.lastNameL1, copyEntity.lastNameL1);
		System.assertEquals(targetEmp.lastNameL2, copyEntity.lastNameL2);
		System.assertEquals(targetEmp.displayNameL0, copyEntity.displayNameL0);
		System.assertEquals(targetEmp.displayNameL1, copyEntity.displayNameL1);
		System.assertEquals(targetEmp.displayNameL2, copyEntity.displayNameL2);
		System.assertEquals(targetEmp.validFrom, copyEntity.validFrom);
		System.assertEquals(targetEmp.validTo, copyEntity.validTo);
	}

	/**
	 * getDefaultDisplayNameL0, getDefaultDisplayNameL1, getDefaultDisplayNameL2 のテスト
	 * 言語の指定が無い場合にエラーが発生しないことを確認する
	 * 表示名のデフォルト値が取得できることを確認する
	 */
	@isTest static void getDefaultDisplayNameTest() {
		EntityTestData testData = new EntityTestData();
		EmployeeBaseEntity emp = testData.createEmployeeEntityList('テスト', 1, 1).get(0);

		// 各言語で取得できる
		System.assertEquals(emp.lastNameL0 + ' ' + emp.firstNameL0, emp.getDefaultDisplayNameL0(AppLanguage.JA));
		System.assertEquals(emp.firstNameL1 + ' ' + emp.lastNameL1, emp.getDefaultDisplayNameL1(AppLanguage.EN_US));
		System.assertEquals(emp.lastNameL2 + ' ' + emp.firstNameL2, emp.getDefaultDisplayNameL2(null));

		// 姓のみが設定されており、20文字を超えない場合
		// 姓のみが返却される
		emp.firstNameL0 = null;
		System.assertEquals(emp.lastNameL0, emp.getDefaultDisplayNameL0(AppLanguage.JA));

		// 名のみが設定されており、20文字を超えない場合
		// 名のみが返却される
		emp.lastNameL0 = null;
		emp.firstNameL0 = 'テスト名_L0';
		System.assertEquals(emp.firstNameL0, emp.getDefaultDisplayNameL0(AppLanguage.JA));

		// 姓、名が設定されており、合わせて20文字の場合
		// 姓のみが返却される
		emp.lastNameL0 = 'テスト姓_L0123';
		emp.firstNameL0 = 'テスト名_L0123';
		System.assertEquals(emp.lastNameL0, emp.getDefaultDisplayNameL0(AppLanguage.JA));
	}

	/**
	 * getDefaultDisplayNameL0, getDefaultDisplayNameL1, getDefaultDisplayNameL2 のテスト
	 * 英語の表示名が取得できることを確認する
	 */
	@isTest static void getEnglishDisplayNameTest() {
		EntityTestData testData = new EntityTestData();
		EmployeeBaseEntity emp = testData.createEmployeeEntityList('Test', 1, 1).get(0);

		// 姓のみが設定されており、20文字を超えない場合
		// 姓のみが返却される
		emp.firstNameL0 = null;
		System.assertEquals(emp.lastNameL0, emp.getDefaultDisplayNameL0(AppLanguage.EN_US));

		// 名のみが設定されており、20文字を超えない場合
		// 名のみが返却される
		emp.lastNameL1 = null;
		emp.firstNameL1 = 'TestFirstName_L0123';
		System.assertEquals(emp.firstNameL1, emp.getDefaultDisplayNameL1(AppLanguage.EN_US));

		// 姓、名が設定されており、合わせて20文字の場合
		// 名のみが返却される
		emp.lastNameL2 = 'TestLastName_L0123';
		emp.firstNameL2 = 'TestFirstName_L0123';
		System.assertEquals(emp.firstNameL2, emp.getDefaultDisplayNameL2(AppLanguage.EN_US));

		// 姓、名が設定されており、合わせて20文字以上の場合
		// 20文字で切り取られた名のみが返却される
		emp.lastNameL0 = 'TestLastName_20LengthOver';
		emp.firstNameL0 = 'TestFirstName_20LengthOver';
		System.assertEquals('TestFirstName_20Leng', emp.getDefaultDisplayNameL0(AppLanguage.EN_US));
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		EmployeeBaseEntity entity = new EmployeeBaseEntity();

		// 会社コードとジョブコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'MasterCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode, entity.code));

		// 勤務体系コードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'MasterCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}
}
