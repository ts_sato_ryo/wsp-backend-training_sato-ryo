/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * JobTypeRepositorのテスト
 */
@isTest
private class JobTypeRepositoryTest {

private class RepoTestData {
		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public List<ComCompany__c> companyObjs;
		/** ジョブタイプオブジェクト */
		public List<ComJobType__c> jobTypeObjs;

		/** コンストラクタ　*/
		public RepoTestData(Integer jobTypeSize) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObjs = new List<ComCompany__c>{
					ComTestDataUtility.createCompany('Company1', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company2', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company3', this.countryObj.Id)
			};
			jobTypeObjs = ComTestDataUtility.createJobTypes('Test Job Type', this.companyObjs[0].Id, jobTypeSize);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityTest() {
		final Integer jobTypeSize = 3;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		ComJobType__c target = testData.jobTypeObjs[1];
		JobTypeEntity entity = new JobTypeRepository().getEntity(target.Id);

		TestUtil.assertNotNull(entity);
		assertEntity(target, entity);
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのレコードが存在しない場合、nullが返却されることを確認する
	 */
	@isTest static void getEntityTestNotFound() {
		final Integer jobTypeSize = 3;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		Id targetId = UserInfo.getUserId();
		JobTypeEntity entity = new JobTypeRepository().getEntity(targetId);

		System.assertEquals(null, entity);
	}

	/**
	 * getEntityListのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityListTest() {
		final Integer jobTypeSize = 5;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		ComJobType__c target1 = testData.jobTypeObjs[1];
		ComJobType__c target2 = testData.jobTypeObjs[2];
		List<JobTypeEntity> entities =
				new JobTypeRepository().getEntityList(new List<Id>{target2.Id, target1.Id});

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * getEntityByCodeのテスト
	 * 指定したコードのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityByCodeTest() {
		final Integer jobTypeSize = 5;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		ComJobType__c targetObj = testData.jobTypeObjs[1];
		targetObj.Code__c = 'Code Test';
		targetObj.CompanyId__c = testData.companyObjs[2].Id;
		update targetObj;
		JobTypeEntity entity = new JobTypeRepository().getEntityByCode(targetObj.CompanyId__c, targetObj.Code__c);

		TestUtil.assertNotNull(entity);
		assertEntity(targetObj, entity);
	}

	/**
	 * searchEntityListのテスト
	 * 全ての項目の値が正しく取得できていることを確認する
	 */
	@isTest static void searchEntityListTestAllField() {
		final Integer jobTypeSize = 5;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		ComJobType__c targetObj = testData.jobTypeObjs[1];

		JobTypeRepository.SearchFilter filter = new JobTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		List<JobTypeEntity> entities = new JobTypeRepository().searchEntityList(filter);

		System.assertEquals(1, entities.size());
		assertEntity(targetObj, entities[0]);
	}

	/**
	 * searchEntityListのテスト
	 * 指定した条件で取得できることを確認する
	 */
	@isTest static void searchEntityListTestFilter() {
		final Integer jobTypeSize = 5;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		ComJobType__c targetObj = testData.jobTypeObjs[1];
		targetObj.Code__c = 'Code Test';
		targetObj.CompanyId__c = testData.companyObjs[2].Id;
		update targetObj;

		JobTypeRepository.SearchFilter filter;
		JobTypeRepository repo = new JobTypeRepository();
		List<JobTypeEntity> resEntities;

		// TEST1: IDで検索
		filter = new JobTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

		// TEST2: 会社IDで検索
		filter = new JobTypeRepository.SearchFilter();
		filter.companyIds = new Set<Id>{targetObj.CompanyId__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

		// TEST3: コードDで検索
		filter = new JobTypeRepository.SearchFilter();
		filter.codes = new Set<String>{targetObj.Code__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

		// TEST4: 通常は論理削除済みのジョブタイプは検索されない
		targetObj.Removed__c = true;
		update targetObj;
		filter = new JobTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(0, resEntities.size());

		// TEST5: 論理削除済みのジョブタイプも検索対象
		targetObj.Removed__c = true;
		update targetObj;
		filter = new JobTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		filter.includeRemoved = true;
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
	}

	/**
	 * saveEntityのテスト
	 * 新規保存できることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		final Integer jobTypeSize = 0;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		JobTypeEntity entity = new JobTypeEntity();
		entity.name = 'Test';
		entity.nameL0 = 'Test_L0';
		entity.nameL1 = 'Test_L1';
		entity.nameL2 = 'Test_L2';
		entity.code = 'TestCode';
		entity.uniqKey = testData.companyObjs[0].Code__c + '-' + entity.code;
		entity.companyId = testData.companyObjs[0].Id;
		entity.isRemoved = true;

		Repository.SaveResult result = new JobTypeRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ComJobType__c resObj = getSObject(result.details[0].id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityのテスト
	 * 既存のエンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestUpdate() {
		final Integer jobTypeSize = 1;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		ComJobType__c orgObj = testData.jobTypeObjs[0];
		JobTypeEntity entity = new JobTypeEntity();
		entity.setId(orgObj.Id);
		entity.name = orgObj.Name + '_Update';
		entity.nameL0 = orgObj.Name_L0__c + '_Update';
		entity.nameL1 = orgObj.Name_L1__c + '_Update';
		entity.nameL2 = orgObj.Name_L2__c + '_Update';
		entity.code = orgObj.Code__c + '_Update';
		entity.uniqKey = testData.companyObjs[0].Code__c + '-' + entity.code + '_Update';
		entity.companyId = testData.companyObjs[1].Id;
		entity.isRemoved = true;

		Repository.SaveResult result = new JobTypeRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ComJobType__c resObj = getSObject(orgObj.Id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityListのテスト
	 * 新規、既存のエンティティが保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		final Integer jobTypeSize = 1;
		RepoTestData testData = new RepoTestData(jobTypeSize);

		// 既存
		ComJobType__c orgObj = testData.jobTypeObjs[0];
		JobTypeEntity entityUd = new JobTypeEntity();
		entityUd.setId(orgObj.Id);
		entityUd.name = orgObj.Name + '_Update';
		entityUd.nameL0 = orgObj.Name_L0__c + '_Update';
		entityUd.nameL1 = orgObj.Name_L1__c + '_Update';
		entityUd.nameL2 = orgObj.Name_L2__c + '_Update';
		entityUd.code = orgObj.Code__c + '_Update';
		entityUd.uniqKey = testData.companyObjs[0].Code__c + '-' + entityUd.code + '_Update';
		entityUd.companyId = testData.companyObjs[1].Id;
		entityUd.isRemoved = true;

		// 新規
		JobTypeEntity entityNew = new JobTypeEntity();
		entityNew.name = 'TestNew';
		entityNew.nameL0 = 'TestNew_L0';
		entityNew.nameL1 = 'TestNew_L1';
		entityNew.nameL2 = 'TestNew_L2';
		entityNew.code = 'TestCode';
		entityNew.uniqKey = testData.companyObjs[0].Code__c + '-' + entityNew.code;
		entityNew.companyId = testData.companyObjs[0].Id;
		entityNew.isRemoved = false;

		Repository.SaveResult result = new JobTypeRepository().saveEntityList(new List<JobTypeEntity>{entityUd, entityNew});

		System.assertEquals(true, result.isSuccessAll);
		ComJobType__c resObjUd = getSObject(orgObj.Id);
		assertSObject(entityUd, resObjUd);
		ComJobType__c resObjNew = getSObject(result.details[1].id);
		assertSObject(entityNew, resObjNew);
	}

	/**
	 * 取得したエンティティが期待値通りであることを確認する
	 */
	private static void assertEntity(ComJobType__c expObj, JobTypeEntity actEntity) {
		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
	}

	/**
	 * 保存したSObjectが期待値通りであることを確認する
	 */
	private static void assertSObject(JobTypeEntity expEntity, ComJobType__c actObj) {
		System.assertEquals(expEntity.name, actObj.name);
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.isRemoved, actObj.Removed__c);
	}

	/**
	 * 指定したIDのComJobTypeを取得する
	 */
	private static ComJobType__c getSObject(Id id) {
		return [
				SELECT Id, Name, Name_L0__c, Name_L1__c, Name_L2__c, Code__c, UniqKey__c, Removed__c
				FROM ComJobType__c
				WHERE Id = :id];
	}
}