/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 工数設定履歴レコード操作APIを実装するクラス
 */
public with sharing class TimeSettingHistoryResource extends Repository.BaseRepository {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 工数設定履歴レコードを表すクラス
	 */
	public class TimeSettingHistory implements RemoteApi.RequestParam {
		/** 工数設定履歴レコードID */
		public String id;
		/** 工数設定レコードベースID */
		public String baseId;
		/** 工数設定名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 工数設定名(言語0) */
		public String name_L0;
		/** 工数設定名(言語1) */
		public String name_L1;
		/** 工数設定名(言語2) */
		public String name_L2;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 履歴コメント */
		public String comment;

		/**
		 * @description 履歴エンティティを作成する
		 * @param param 工数設定履歴レコード値を含むパラメータ
		 * @param isUpdate レコード更新時はtrue、作成時はfalse
		 * @return 作成or更新した工数設定履歴レコード
		 */
		private TimeSettingHistoryEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {

			TimeSettingHistoryEntity setting = new TimeSettingHistoryEntity();

			// レコード更新時はIDをセット
			if(isUpdate){
				setting.setId(this.id);
			}
			// ベースIDは更新不可のため、新規作成時のみセット
			if (!isUpdate && paramMap.containsKey('baseId')) {
				setting.baseId = this.baseId;
			}

			if (paramMap.containsKey('name_L0')) {
				// TODO とりあえず、nameにはname_L0を設定しておく
				setting.name = this.name_L0;
				setting.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				setting.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				setting.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('validDateFrom') && this.validDateFrom != null) {
				setting.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (paramMap.containsKey('validDateTo') && this.validDateTo != null) {
				setting.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (paramMap.containsKey('comment')) {
				setting.historyComment = this.comment;
			}

			// 有効開始日のデフォルトは実行日
			if (setting.validFrom == null) {
				setting.validFrom = AppDate.today();
			}
			// 失効日のデフォルト
			if (setting.validTo == null) {
				setting.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}

			return setting;
		}

	}


	/**
	 * @description 工数設定履歴レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成した履歴レコードID */
		public String id;
	}

	/**
	 * @description 工数設定履歴レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_SETTING;

		/**
		 * @description 工数設定履歴レコードを1件作成する
		 * @param req リクエストパラメータ(TimeSettingHistory)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// TimeSettingHistory param = (TimeSettingHistory)req.getParam(TimeSettingHistory.class);
			// TimeSettingEntity setting = TimeSettingHistoryResource.saveRecord(param, req.getParamMap(), false);
         //
			// SaveResult res = new SaveResult();
			// res.Id = setting.historyId;
			// return res;

			//------------------------------------------------------------------------
			// 【仮仕様】
			// 現在指定可能な改定日(有効開始日)は末尾の履歴の有効開始日以降とする。
			// 改定機能自体は実装済みでマスタ全体の有効期間内の日付で改定できるようになっているが
			// UI側が対応できていないため。
			//------------------------------------------------------------------------

			final Boolean isUpdate = false;
			TimeSettingService service = new TimeSettingService();
			TimeSettingRepository repo = new TimeSettingRepository();

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			TimeSettingHistory param = (TimeSettingHistory)req.getParam(TimeSettingHistory.class);

			// 権限チェック validationが無いのでパラメータ変換後に実行
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			TimeSettingHistoryEntity newHistory = param.createEntity(req.getParamMap(), isUpdate);

			// 末尾の履歴(有効開始日が最新の履歴)を取得する
			TimeSettingBaseEntity base = repo.getEntity(newHistory.baseId);
			if (base == null) {
				throw new App.ParameterException(
						MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Trac_Lbl_TimeTrackSetting}));
			}
			TimeSettingHistoryEntity latestHistory = (TimeSettingHistoryEntity)base.getLatestSuperHistory();

			// 追加の履歴の有効開始日が末尾の履歴の有効開始日以降であることを確認する
			if (newHistory.validFrom.getDate() < latestHistory.validFrom.getDate()) {
				// メッセージ：改定日は最新の履歴の有効開始日以降を指定してください
				throw new App.ParameterException(MESSAGE.Admin_Err_InvalidRevisionDate);
			}
			// 改定対象が最新履歴の場合は、マスタ全体の失効日(末尾の履歴の失効日)を更新する
			if (newHistory.validTo.getDate() != latestHistory.validTo.getDate()) {
				service.updateValidTo(newHistory.baseId, newHistory.validTo);
			}
			// 履歴を改定する
			service.reviseHistory(newHistory);

			// 改定日に有効な履歴のIDを取得する
			TimeSettingBaseEntity revisedBase = repo.getEntity(newHistory.baseId, newHistory.validFrom);
			SaveResult res = new SaveResult();
			res.Id = revisedBase.getHistoryList().get(0).id;
			return res;
		}
	}


	/**
	 * @desctiprion 工数設定履歴レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/**
		 * @description 工数設定履歴レコードを1件更新する
		 * @param req リクエストパラメータ(TimeSettingHistory)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// 一旦廃止します。必要になったら実装してください。
			throw new App.UnsupportedException('工数設定履歴更新APIは利用できません。');

			// // パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// TimeSettingHistory param = (TimeSettingHistory)req.getParam(TimeSettingHistory.class);
			// TimeSettingHistoryResource.saveRecord(param, req.getParamMap(), true);
         //
			// // 成功レスポンスをセットする
			// return null;
		}
	}


	/**
	 * @description 工数設定履歴レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}

	}

	/**
	 * 工数設定履歴レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_SETTING;

		/**
		 * @description 工数設定履歴レコードを1件削除する
		 * @param req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// #以下の場合は例外 (RemoteApi.TypeException) が発生する
			//   ・データ型が一致しないパラメータがある
			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new TimeSettingService().deleteHistoryListWithPropagation(new List<Id>{param.id});
			} catch (App.RecordNotFoundException e) {
				// すでに削除済みの場合は成功レスポンスを返す
			}

			// 成功レスポンスをセットする
			return null;

		}
	}


	/**
	 * @description 工数設定履歴レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public TimeSettingHistory[] records;
	}

	/**
	 * @description 工数設定履歴レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** 工数設定履歴レコードID */
		public String id;
		/** 関連する工数設定ベースレコードID */
		public String baseId;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {

			// 履歴ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// ベースID
			if (String.isNotBlank(this.baseId)) {
				try {
					System.Id.valueOf(this.baseId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'baseId'}));
				}
			}
		}

	}

	/**
	 * @description 工数設定履歴レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 工数設定履歴レコードを検索する
		 * @param requ リクエストパラメータ(SearchCondition)
		 * @return レスポンス(SearchResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);

			// パラメータのバリデーション
			param.validate();

			TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
			if (param.id != null) {
				filter.historyIds = new Set<Id>{param.id};
			}
			if (param.baseId != null) {
				filter.baseIds = new Set<Id>{param.baseId};
			}

			List<TimeSettingHistoryEntity> entityList =
					(new TimeSettingRepository()).searchHistoryEntity(filter, false,
					TimeSettingRepository.HistorySortOrder.VALID_FROM_DESC);


			// エンティティをレスポンスオブジェクトに変換
			List<TimeSettingHistory> historyList = new List<TimeSettingHistory>();
			for (TimeSettingHistoryEntity entity : entityList) {
				TimeSettingHistory history = new TimeSettingHistory();
				history.id = entity.id;
				history.baseId = entity.baseId;
				history.name = entity.nameL.getValue();
				history.name_L0 = entity.nameL0;
				history.name_L1 = entity.nameL1;
				history.name_L2 = entity.nameL2;
				history.validDateFrom = AppDate.convertDate(entity.validFrom);
				history.validDateTo = AppDate.convertDate(entity.validTo);
				history.comment = entity.historyComment;

				historyList.add(history);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = historyList;
			return res;
		}
	}

}
