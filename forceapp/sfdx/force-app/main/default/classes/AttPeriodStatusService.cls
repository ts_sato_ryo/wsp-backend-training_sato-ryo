/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 勤怠期間サービスクラス
 */
public with sharing class AttPeriodStatusService implements Service {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * 設定された所有者にレコードの参照権限がなかった場合に返却されるステータスコード
	 * TODO EmployeeServiceクラスの定義と共通化したい。または所有者を保存する処理を共通化するか。。
	 */
	private static final Set<System.StatusCode> FAILD_SAVE_OWNER_CODE_SET = new Set<System.StatusCode> {
		System.StatusCode.TRANSFER_REQUIRES_READ,
		System.StatusCode.OP_WITH_INVALID_USER_TYPE_EXCEPTION
	};

	/**
	 * 勤怠期間を保存する
	 * 影響のある勤怠サマリーの再計算フラグをONにする。
	 * 自動で設定する項目に値を設定し保存する。
	 * エンティティのバリデーションは行いません。
	 * @param 保存対象の勤怠期間エンティティ
	 * @return 保存したエンティティのID
	 */
	public Id savePeriodStatus(AttPeriodStatusEntity periodStatus) {

		// 社員を取得する
		EmployeeBaseEntity employee = getEmployee(periodStatus.employeeBaseId);
		if (employee == null) {
			// メッセージ：該当する社員が見つかりません
			throw new App.ParameterException(
					MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Com_Lbl_Employee}));
		}

		// nameを設定する
		// TODO 社員名は姓のみでよいかは要検討
		periodStatus.name = periodStatus.createName(employee.code, employee.lastNameL0);

		// 所有者を社員のユーザに設定する
		if (employee.userId == null) {
			throw new App.IllegalStateException(MESSAGE.Com_Err_NotSetValue(new List<String>{MESSAGE.Admin_Lbl_User}));
		}
		periodStatus.ownerId = employee.userId;


		// 勤怠期間の勤怠サマリーの再計算フラグをONにする
		if (periodStatus.id == null) {
			// 新規の場合は適用後の勤怠期間のみが対象
			setOnSummeryDirtyFlag(periodStatus.employeeBaseId,
					periodStatus.validFrom, periodStatus.validTo.addDays(-1));
		} else {
			// 更新の場合は変更前と変更後の勤怠期間のサマリーが対象
			// 変更前と変更後で有効期間が重複している部分もありますが、社員が変更されている可能性もあるため
			// 別々に更新を行います。
			setOnSummeryDirtyFlag(periodStatus.employeeBaseId,
					periodStatus.validFrom, periodStatus.validTo.addDays(-1));

			AttPeriodStatusEntity beforePeriodStatus = new AttPeriodStatusRepository().getEntity(periodStatus.id);
			if (beforePeriodStatus == null) {
				// メッセージ：該当する勤怠期間が存在しません。
				throw new App.ParameterException(
						MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Admin_Lbl_ShortTimeWorkSetting}));
			}
			setOnSummeryDirtyFlag(beforePeriodStatus.employeeBaseId,
					beforePeriodStatus.validFrom, beforePeriodStatus.validTo.addDays(-1));
		}

		// 勤怠期間を保存する
		Id savedId;
		try {
			Repository.SaveResult result = new AttPeriodStatusRepository().saveEntity(periodStatus);
			savedId = result.details[0].id;
		} catch (System.DmlException e) {
			if (FAILD_SAVE_OWNER_CODE_SET.contains(e.getDmlType(0))) {
				// 所有者であるSalesforceユーザに社員レコードの参照権限がない
				String msg = MESSAGE.Admin_Err_SfdcUserRequiredRead;
				e.setMessage(msg);
			} else if (e.getDmlType(0) == System.StatusCode.INACTIVE_OWNER_OR_USER){
				String msg = MESSAGE.Admin_Err_SfdcUserInactive;
				e.setMessage(msg);
			}
			throw e;
		}

		return savedId;
	}

	/**
	 * 勤怠期間を削除する
	 * @param periodStatusId 削除対象の勤怠期間ID
	 */
	public void deletePeriodStatus(Id periodStatusId) {

		// 勤怠期間の勤怠サマリーの再計算フラグをONにする
		// 削除対象の勤怠期間の有効期間が更新対象
		AttPeriodStatusEntity periodStatus = new AttPeriodStatusRepository().getEntity(periodStatusId);
		if (periodStatus == null) {
			// メッセージ：対象データが見つかりませんでした。削除されている可能性があります。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_DeleteRecordNotFound);
		}
		setOnSummeryDirtyFlag(periodStatus.employeeBaseId,
				periodStatus.validFrom, periodStatus.validTo.addDays(-1));

		// 勤怠期間を削除する
		new AttPeriodStatusRepository().deleteEntity(periodStatus);
	}

	/**
	 * 勤怠サマリーの再計算フラグを更新する
	 * @param employeeId 社員ベースID
	 * @param startDate 更新対象期間の開始日
	 * @param endDate 更新対象期間の終了日(勤怠期間の失効日の前日)
	 */
	private void setOnSummeryDirtyFlag(Id employeeId, AppDate startDate, AppDate endDate) {
		AttSummaryRepository repo = new AttSummaryRepository();

		// 指定期間の勤怠明細を取得する
		List<AttRecordEntity> recordList = repo.searchRecordEntityList(
				employeeId, startDate, endDate);

		Set<Id> summaryIdSet = new Set<Id>();
		for (AttRecordEntity record : recordList) {
			summaryIdSet.add(record.summaryId);
		}
		// 勤怠明細が参照している勤怠サマリーの再計算フラグをONにする
		if (! summaryIdSet.isEmpty()) {
			List<AttSummaryEntity> summaryList = repo.getEntityList(new List<Id>(summaryIdSet), false, false);
			for (AttSummaryEntity summary : summaryList) {
				summary.isDirty = true;
			}
			repo.saveEntityList(summaryList);
		}
	}

	/**
	 * 勤怠期間エンティティのバリデーションを実行する
	 * エンティティに不正な値が設定されている場合は例外が発生します。
	 * @param periodStatus 勤怠期間
	 */
	public void validatePeriodStatus(AttPeriodStatusEntity periodStatus) {

		// エンティティのバリデーション
		Validator.Result entityResult =
				(new AttConfigValidator.AttPeriodStatusValidator(periodStatus)).validate();
		if (! entityResult.isSuccess()) {
			Validator.Error error = entityResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * 短時間勤務設定が適用できるかどうかをチェックする
	 * 適用できない場合は例外が発生します。
	 * @param periodStatus 適用情報が設定されている勤怠期間
	 */
	public void validateShortTimeSetting(AttPeriodStatusEntity periodStatus) {

		// エンティティのバリデーション
		validatePeriodStatus(periodStatus);

		// チェックに必要な情報
		Id employeeId = periodStatus.employeeBaseId;
		Id shortTimeSettingId = periodStatus.shortTimeWorkSettingBaseId;
		AppDate startDate = periodStatus.validFrom;
		AppDate endDate = periodStatus.validTo.addDays(-1);

		// 既存の適用期間と重複してないことをチェックする
		if (isPeriodOverlapping(employeeId, periodStatus.id,
				AttPeriodStatusType.SHORT_TIME_WORK_SETTING,
				startDate, endDate)) {
			// メッセージ：有効期間が重複しています。
			throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusOvelapPeriod);

		}

		// 適用期間のバリデーション
		if (periodStatus.id == null) {
			// 新規の場合
			// 短時間勤務設定を新規に適用できることを確認する
			validateApplicableShortTimeSetting(employeeId, shortTimeSettingId, startDate, endDate, true);

		} else {
			// 更新の場合

			// 変更前の勤怠期間を取得する
			AttPeriodStatusEntity beforePeriodStatus = new AttPeriodStatusRepository().getEntity(periodStatus.id);
			if (beforePeriodStatus == null) {
			// メッセージ：該当する勤怠期間が存在しません。
			throw new App.RecordNotFoundException(
					MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Admin_Lbl_Apply}));
			}

			if (beforePeriodStatus.employeeBaseId != periodStatus.employeeBaseId) {
				// 社員が変更される場合
				// 短時間勤務設定を新規に適用できることを確認する
				validateApplicableShortTimeSetting(employeeId, shortTimeSettingId, startDate, endDate, true);

				// 変更前の社員の勤怠期間に勤務確定が行われていないことを確認する
				if (isLockedAttRecord(beforePeriodStatus.employeeBaseId, beforePeriodStatus.validFrom, beforePeriodStatus.validTo.addDays(-1))) {
					// メッセージ：変更前の有効期間内に勤務が確定している期間が存在します。
					throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusCanNotDelete);
				}

			} else {
				// 社員が変更されない場合

				// 変更前と変更後の差分期間で勤務確定が行われていないことを確認する
				List<AttPeriodStatusService.PeriodResult> deletePeriodList = extractDifferencePeriods(beforePeriodStatus, periodStatus);
				for (PeriodResult period : deletePeriodList) {
					if (isLockedAttRecord(employeeId, period.startDate, period.endDate)) {
						if (period.isBeforePeriod) {
							// メッセージ：変更前の有効期間内に勤務が確定している期間が存在します。
							throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusCanNotDelete);
						} else {
							// メッセージ：指定された有効期間内に勤務が確定している期間が存在します。
							throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusLockedRecord);
						}
					}
				}

				// 変更後の適用期間に短時間勤務設定が適用できることを確認する
				Boolean notAllowedFixWork;
				if (beforePeriodStatus.shortTimeWorkSettingBaseId
						!= periodStatus.shortTimeWorkSettingBaseId) {
					// 短時間勤務設定が変更される場合
					// 有効期間で勤務確定されている日が存在する場合は適用不可
					notAllowedFixWork = true;
					validateApplicableShortTimeSetting(employeeId, shortTimeSettingId,
							startDate, endDate, notAllowedFixWork);
				} else {
					// 短時間勤務設定が変更されない場合
					// 既に適用済みの期間は勤務確定されている日が存在しても適用可能
					notAllowedFixWork = false;
					validateApplicableShortTimeSetting(employeeId, shortTimeSettingId,
							startDate, endDate, notAllowedFixWork);
				}
			}
		}
	}

	/**
	 * 休職休業が適用できるかどうかをチェックする
	 * 適用できない場合は例外が発生します。
	 * @param periodStatus 適用情報が設定されている勤怠期間
	 */
	public void validateLeaveOfAbsence(AttPeriodStatusEntity periodStatus) {

		// エンティティのバリデーション
		validatePeriodStatus(periodStatus);

		// チェックに必要な情報
		Id employeeId = periodStatus.employeeBaseId;
		Id leaveOfAbsenceId = periodStatus.leaveOfAbsenceId;
		AppDate startDate = periodStatus.validFrom;
		AppDate endDate = periodStatus.validTo.addDays(-1);

		// 既存の適用期間と重複してないことをチェックする
		if (isPeriodOverlapping(
				employeeId, periodStatus.id,
				AttPeriodStatusType.ABSENCE, startDate, endDate)) {
			// メッセージ：有効期間が重複しています。
			throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusOvelapPeriod);
		}

		// 適用期間のバリデーション
		if (periodStatus.id == null) {
			// 新規の場合
			// 短時間勤務設定を新規に適用できることを確認する
			final Boolean notAllowedFixWork = true;
			validateApplicableLeaveOfAbsence(
					employeeId, leaveOfAbsenceId, startDate, endDate, notAllowedFixWork);

		} else {
			// 更新の場合

			// 変更前の勤怠期間を取得する
			AttPeriodStatusEntity beforePeriodStatus = new AttPeriodStatusRepository().getEntity(periodStatus.id);
			if (beforePeriodStatus == null) {
			// メッセージ：該当する勤怠期間が存在しません。
			throw new App.RecordNotFoundException(
					MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Admin_Lbl_Apply}));
			}

			if (beforePeriodStatus.employeeBaseId != periodStatus.employeeBaseId) {
				// 社員が変更される場合
				// 短時間勤務設定を新規に適用できることを確認する
				final Boolean notAllowedFixWork = true;
				validateApplicableLeaveOfAbsence(
						employeeId, leaveOfAbsenceId, startDate, endDate, notAllowedFixWork);

				// 変更前の社員の勤怠期間に勤務確定が行われていないことを確認する
				if (isLockedAttRecord(beforePeriodStatus.employeeBaseId, beforePeriodStatus.validFrom, beforePeriodStatus.validTo.addDays(-1))) {
					// メッセージ：変更前の有効期間内に勤務が確定している期間が存在します。
					throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusCanNotDelete);
				}

			} else {
				// 社員が変更されない場合
				// 変更前と変更後の差分期間で勤務確定が行われていないことを確認する
				List<AttPeriodStatusService.PeriodResult> deletePeriodList = extractDifferencePeriods(beforePeriodStatus, periodStatus);
				for (PeriodResult period : deletePeriodList) {
					if (isLockedAttRecord(employeeId, period.startDate, period.endDate)) {
						if (period.isBeforePeriod) {
							// メッセージ：変更前の有効期間内に勤務が確定している期間が存在します。
							throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusCanNotDelete);
						} else {
							// メッセージ：指定された有効期間内に勤務が確定している期間が存在します。
							throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusLockedRecord);
						}
					}
				}

				// 変更後の適用期間に休職休業が適用できることを確認する
				Boolean notAllowedFixWork;
				if (beforePeriodStatus.leaveOfAbsenceId != periodStatus.leaveOfAbsenceId) {
				// 休職休業が変更される場合
				// 有効期間で勤務確定されている日が存在する場合は適用不可
				notAllowedFixWork = true;
				validateApplicableLeaveOfAbsence(
							employeeId, leaveOfAbsenceId, startDate, endDate, notAllowedFixWork);
				} else {
					// 休職休業が変更されない場合
					// 変更前の有効期間で勤務確定されている日が存在しても適用可能
					notAllowedFixWork = false;
					validateApplicableLeaveOfAbsence(
							employeeId, leaveOfAbsenceId, startDate, endDate, notAllowedFixWork);
				}
			}
		}
	}

	/** 抽出した期間 */
	private class PeriodResult {
		public AppDate startDate;
		public AppDate endDate;
		public Boolean isBeforePeriod;

		/** コンストラクタ(インスタンス変数設定) */
		public PeriodResult(AppDate startDate, AppDate endDate, Boolean isBeforePeriod) {
			this.startDate = startDate;
			this.endDate = endDate;
			this.isBeforePeriod = isBeforePeriod;
		}
	}

	/**
	 * 適用の差分期間を抽出する
	 * @param beforePeriod 既存の適用期間
	 * @param afterPeriod 変更後の適用期間
	 * @return 削除対象の期間のリスト。差分期間が存在しない場合は空のリストを返却
	 */
	private List<AttPeriodStatusService.PeriodResult> extractDifferencePeriods(
				AttPeriodStatusEntity beforePeriod, AttPeriodStatusEntity afterPeriod) {

		List<AttPeriodStatusService.PeriodResult> periodList =
				new List<AttPeriodStatusService.PeriodResult>();

		Date beforeStartDate = beforePeriod.validFrom.getDate();
		Date beforeEndDate = beforePeriod.validTo.addDays(-1).getDate();
		Date afterStartDate = afterPeriod.validFrom.getDate();
		Date afterEndDate = afterPeriod.validTo.addDays(-1).getDate();

		// 有効開始日が前にずれる場合
		if (afterStartDate < beforeStartDate) {
			// 削除期間の終了日は変更後の有効開始日の前日と変更前の失効日の前日の値が小さい方
			// 「*」が新規追加期間
			// ------------------------------|---------<変更前>-----------|---------------------------
			// (1) 変更前の期間と全く重ならない
			// -------|******<変更後>****|--------------------------------------------------------------
			// (2) 一部重複
			// --------|**********************<変更後>--------------------|-------------------------------------

			Date newStartDate = afterStartDate;
			Date newEndDate;
			final Boolean isBeforePeriod = false; // 差分の期間は変更後の期間に含まれる
			if (afterEndDate < beforeStartDate) {
				// (1) 重複しない
				newEndDate = afterEndDate;
			} else {
				// (2) 一部重複
				newEndDate = beforeStartDate.addDays(-1);
			}
			periodList.add(new PeriodResult(AppDate.valueOf(newStartDate), AppDate.valueOf(newEndDate), isBeforePeriod));
		}

		// 有効開始日が後にずれる場合
		if (beforeStartDate.daysBetween(afterStartDate) > 0) {
			// 削除期間の終了日は変更後の有効開始日の前日と変更前の失効日の前日の値が小さい方
			// 「*」が削除対象期間
			// --------|---------<変更前>-----------|---------------------------
			// 一部重複
			// --------**********|---------<変更後>----------|--------------------
			// 変更前の期間と全く重ならない
			// ------****************************------|------<変更後>----|--------

			Date delStartDate = beforeStartDate;
			Date delEndDate;
			final Boolean isBeforePeriod = true; // 差分の期間は変更前の期間に含まれる
			if (beforeEndDate.addDays(-1) < afterStartDate.addDays(-1)) {
				delEndDate = beforeEndDate;
			} else {
				delEndDate = afterStartDate.addDays(-1);
			}
			periodList.add(new PeriodResult(AppDate.valueOf(delStartDate), AppDate.valueOf(delEndDate), isBeforePeriod));
		}

		// 有効期間終了日が前にずれる場合
		if (afterEndDate < beforeEndDate) {
			// 削除期間の終了日は変更後の有効開始日の前日と変更前の失効日の前日の値が小さい方
			// 「*」が削除対象機関
			// -------------------------|---------<変更前>-----------|-----------
			// (1)一部重複
			// -----------|------<変更後>----------|*****************-----------
			// (2)変更前の期間と全く重ならない
			// ---|------<変更後>----|----***************************|----------
			Date delEndDate = beforeEndDate;
			Date delStartDate;
			final Boolean isBeforePeriod = true; // 差分の期間は変更前の期間に含まれる
			if (beforeStartDate < afterEndDate) {
				// (1) 一部重複
				delStartDate = afterEndDate.addDays(1); // 有効期間終了日の翌日
			} else {
				// (2) 重複なし
				delStartDate = beforeStartDate;
			}
			periodList.add(new PeriodResult(AppDate.valueOf(delStartDate), AppDate.valueOf(delEndDate), isBeforePeriod));
		}

		// 有効期間終了日が後にずれる場合
		if (afterEndDate > beforeEndDate) {
			// 削除期間の終了日は変更後の有効開始日の前日と変更前の失効日の前日の値が小さい方
			// 「*」が対象機関
			// -------------------------|---------<変更前>-----------|---------------------
			// (1)変更前の期間と全く重ならない
			// -----------------------------------------------------------|****<変更後>***|----
			// (2)一部重複
			// -------------------------|---------<変更後>------------******|---------------

			Date newEndDate = afterEndDate;
			Date newStartDate;
			final Boolean isBeforePeriod = false; // 差分の期間は変更後の期間に含まれる
			if (afterStartDate > beforeEndDate) {
				// (1) 重複なし
				newStartDate = afterStartDate;
			} else {
				// (2) 一部重複
				newStartDate = beforeEndDate.addDays(1); // 有効期間終了日の翌日
			}
			periodList.add(new PeriodResult(AppDate.valueOf(newStartDate), AppDate.valueOf(newEndDate), isBeforePeriod));
		}

		return periodList;
	}

	/**
	 * 指定期間の勤怠期間を取得する
	 * @param employeeId 指定社員Id
	 * @param startDate 指定期間開始日
	 * @param endDate 指定期間終了日
	 * @type 勤怠期間区分
	 * @return 該当する勤怠期間一覧
	 */
	public List<AttPeriodStatusEntity> searchPeriodStatusList(
			Id employeeId, AppDate startDate, AppDate endDate, AttPeriodStatusType periodType) {

		AttPeriodStatusRepository.SearchFilter periodStatusFilter = new AttPeriodStatusRepository.SearchFilter();
		periodStatusFilter.employeeIds = new Set<Id>{employeeId};
		periodStatusFilter.periodStatusTypes = new Set<AttPeriodStatusType>{periodType};
		periodStatusFilter.targetStartDate = startDate;
		periodStatusFilter.targetEndDate = endDate;
		return new AttPeriodStatusRepository().searchEntityList(periodStatusFilter);
	}

	/**
	 * 登録済みの適用期間と重複していないことを確認する
	 * @param employeeId 適用する社員ベースID
	 * @param periodStatusId 更新対象の勤怠期間ID(新規の場合はnull)
	 * @param type 勤怠期間種別
	 * @param startDate 適用開始日
	 * @param endDate 適用終了日(失効日の前日)
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	private Boolean isPeriodOverlapping(
			Id employeeId, Id periodStatusId, AttPeriodStatusType type,
			AppDate startDate, AppDate endDate) {

		// 既存の勤怠期間を取得する
		List<AttPeriodStatusEntity> periodStatusList =
				getPeriodStatusByEmployeeId(type, employeeId);

		// 各勤怠期間の適用期間と期間が重複していないかチェックする
		for (AttPeriodStatusEntity periodStatus : periodStatusList) {

			if (periodStatus.id == periodStatusId) {
				continue;
			}

			// 重複チェック
			// 比較開始日付 <= 対象終了日付 AND 比較終了日付 >= 対象開始日付
			if ((periodStatus.validFrom.getDate() <= endDate.getDate())
					&& (periodStatus.validTo.addDays(-1).getDate() >= startDate.getDate())) {
				// 重複している
				return true;
			}
		}

		// 重複していない
		return false;
	}

	/**
	 * 短時間勤務設定を新規に適用できるかどうかを確認する
	 * @param employeeId 適用する社員ベースID
	 * @param shortTimeSettingId 短時間勤務設定ベースID
	 * @param startDate 適用開始日
	 * @param endDate 適用終了日(失効日の前日)
	 * @param notAllowedFixWork 勤務確定している場合は適用を許可しない場合はtrue
	 */
	private void validateApplicableShortTimeSetting(
			Id employeeId, Id shortTimeSettingId, AppDate startDate, AppDate endDate,
			Boolean notAllowedFixWork) {

		// 短時間勤務設定を取得する
		AttShortTimeSettingBaseEntity setting = getShortTimeSetting(shortTimeSettingId);
		if (setting == null) {
			// メッセージ：該当する短時間勤務設定が見つかりません
			throw new App.ParameterException(
					MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Admin_Lbl_ShortTimeWorkSetting}));
		}

		// 社員を取得する
		EmployeeBaseEntity employee = getEmployee(employeeId);
		if (employee == null) {
			// メッセージ：該当する社員が見つかりません
			throw new App.ParameterException(
					MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Com_Lbl_Employee}));
		}

		// 適用期間の各日に対して以下をチェックする
		//  - 短時間勤務設定が有効である
		//  - 社員が有効である
		//  - 勤務体系が有効である
		//  - 社員の勤務体系の労働時間制と短時間勤務設定の労働時間制が一致している
		for (Date targetDate = startDate.getDate(); targetDate <= endDate.getDate();
				targetDate = targetDate.addDays(1)) {

			AppDate appTargetDate = AppDate.valueOf(targetDate);

			// 短時間勤務設定が有効であることをチェック
			if (setting.getSuperHistoryByDate(appTargetDate) == null) {
				// メッセージ：指定された有効期間内に短時間勤務設定が有効ではない期間が存在します。
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusInvalidShortTimeWorkSetting);
			}

			// 社員の履歴を取得する
			EmployeeHistoryEntity empHistory =
					(EmployeeHistoryEntity)employee.getSuperHistoryByDate(appTargetDate);

			if (empHistory == null) {
				// メッセージ：指定された有効期間内に社員が有効ではない期間が存在します。
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusInvalidEmployee);
			}

			// 勤務体系が設定されている
			if (empHistory.workingTypeId == null) {
				// メッセージ：指定された有効期間内に社員の勤務体系が設定されていない期間が存在します。
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusNotSetWorkType);
			}

			// 勤務体系が適用期間で有効である
			AttWorkingTypeBaseEntity workingType = getWorkingType(empHistory.workingTypeId);
			if (workingType.getSuperHistoryByDate(appTargetDate) == null) {
				// メッセージ：指定された有効期間内に社員の勤務体系が有効ではない期間が存在します。
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusInvalidEmployeeWorkType);
			}

			// 労働時間制が短時間勤務設定と一致している
			// TODO 勤務体系の労働時間制がAttWorkSystemに対応したら修正する
			if (workingType.getWorkSystemString() != AppConverter.stringValue(setting.workSystem)) {
				// メッセージ：指定された有効期間内に、社員の勤務体系の労働時間制が短時間勤務設定の労働時間制と一致していない期間が存在します
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusInvalidWorkSystem);
			}
		}

		// 適用期間中の勤怠明細がロックされていないことを確認する
		if (notAllowedFixWork) {
			if (isLockedAttRecord(employeeId, startDate, endDate)) {
				// メッセージ：指定された有効期間内に勤務が確定している期間が存在します。
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusLockedRecord);
			}
		}
	}

	/**
	 * 休業休職を新規に適用できるかどうかを確認する
	 * @param employeeId 適用する社員ベースID
	 * @param leaveOfAbsenceId 休職休業ID
	 * @param startDate 適用開始日
	 * @param endDate 適用終了日(失効日の前日)
	 * @param notAllowedFixWork 勤務確定している場合は適用を許可しない場合はtrue
	 */
	private void validateApplicableLeaveOfAbsence(
			Id employeeId, Id leaveOfAbsenceId, AppDate startDate, AppDate endDate,
			Boolean notAllowedFixWork) {
		AttSummaryRepository summaryRepo = new AttSummaryRepository();

		// 休職休業を取得する
		AttLeaveOfAbsenceEntity absense = getLeaveOfAbsence(leaveOfAbsenceId);
		if (absense == null) {
			// メッセージ：該当する休職休業が見つかりません
			throw new App.ParameterException(
					MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Admin_Lbl_LeaveOfAbsence}));
		}

		// 社員を取得する
		EmployeeBaseEntity employee = getEmployee(employeeId);
		if (employee == null) {
			// メッセージ：該当する社員が見つかりません
			throw new App.ParameterException(
					MESSAGE.Att_Err_NotFound(new List<String>{MESSAGE.Com_Lbl_Employee}));
		}

		// 各種勤怠申請のチェック
		// 有効な申請(ステータス：承認待ち、承認済み、変更承認待ち、無効(要確認フラグ＝ON))の場合は適用不可
		List<AttDailyRequestEntity> dailyRequest = getDailyRequests(employeeId, startDate, endDate);
		for (AttDailyRequestEntity request : dailyRequest) {
			if (request.status == AppRequestStatus.APPROVED
					|| request.status == AppRequestStatus.PENDING
					|| request.status == AppRequestStatus.REAPPLYING) {
				// メッセージ : 有効期間中に勤怠申請が申請されています
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusExistRequest);
			}
			if (request.status == AppRequestStatus.DISABLED
					&& request.isConfirmationRequired == true) {
				// メッセージ : 有効期間中に勤怠申請が申請されています
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusExistRequest);
			}
		}

		// 適用期間中の勤怠明細がロックされていないことを確認する
		if (notAllowedFixWork) {
			if (isLockedAttRecord(employeeId, startDate, endDate)) {
				// メッセージ：指定された有効期間内に勤務が確定している期間が存在します。
				throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusLockedRecord);
			}
		}
	}

	/**
	 * 短時間勤務設定の適用を削除できるかどうかを確認する
	 * @param periodStatusId 削除対象の勤怠期間ID
	 */
	public void validateDeletableShortTimeSetting(Id periodStatusId) {

		// 削除対象の勤怠期間を取得
		AttPeriodStatusEntity periodStatus = new AttPeriodStatusRepository().getEntity(periodStatusId);
		if (periodStatus == null) {
			// メッセージ：対象データが見つかりませんでした。削除されている可能性があります。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_DeleteRecordNotFound);
		}

		// 念のため短時間勤務設定の勤怠期間であることを確認しておく
		if (periodStatus.type != AttPeriodStatusType.SHORT_TIME_WORK_SETTING) {
			// ここにきた場合はバグ。とりあえずメッセージの多言語化不要
			throw new App.ParameterException('指定された勤怠期間の種別が短時間勤務設定ではありません。');
		}

		validateDeletablePeriodStatus(periodStatus.employeeBaseId,
				periodStatus.validFrom, periodStatus.validTo.addDays(-1));
	}

	/**
	 * 休職休業の適用を削除できるかどうかを確認する
	 * @param periodStatusId 削除対象の勤怠期間ID
	 */
	public void validateDeletableLeaveOfAbsence(Id periodStatusId) {

		// 削除対象の勤怠期間を取得
		AttPeriodStatusEntity periodStatus = new AttPeriodStatusRepository().getEntity(periodStatusId);
		if (periodStatus == null) {
			// メッセージ：対象データが見つかりませんでした。削除されている可能性があります。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_DeleteRecordNotFound);
		}

		// 念のため休職休業の勤怠期間であることを確認しておく
		if (periodStatus.type != AttPeriodStatusType.ABSENCE) {
			// ここにきた場合はバグ。とりあえずメッセージの多言語化不要
			throw new App.ParameterException('指定された勤怠期間の種別が休職休業設定ではありません。');
		}

		validateDeletablePeriodStatus(periodStatus.employeeBaseId,
				periodStatus.validFrom, periodStatus.validTo.addDays(-1));
	}

	/**
	 * 勤怠期間の適用を削除できるかどうかを確認する
	 * @param employeeId 適用する社員ベースID
	 * @param startDate 適用開始日
	 * @param endDate 適用終了日(失効日の前日)
	 */
	private void validateDeletablePeriodStatus(
			Id employeeId, AppDate startDate, AppDate endDate) {

		// 適用期間中の勤怠明細がロックされていないことを確認する
		if (isLockedAttRecord(employeeId, startDate, endDate)) {
			// メッセージ：変更前の有効期間内に勤務が確定している期間が存在します。
			throw new App.ParameterException(MESSAGE.Att_Err_PeriodStatusCanNotDelete);
		}
	}

	/**
	 * 指定期間中の勤怠明細、勤怠サマリーがロックされているかどうかを確認する
	 * @param employeeId 適用する社員ベースID
	 * @param startDate 開始日
	 * @param endDate 終了日(失効日の前日)
	 * @return ロックされている勤怠明細が1個でも存在している場合はtrue, そうでない場合はfalse
	 */
	private Boolean isLockedAttRecord(Id employeeId, AppDate startDate, AppDate endDate) {
		AttSummaryRepository repo = new AttSummaryRepository();

		// 指定期間の勤怠明細を取得する
		List<AttRecordEntity> recordList = repo.searchRecordEntityList(
				employeeId, startDate, endDate);

		Set<Id> summaryIdSet = new Set<Id>();
		for (AttRecordEntity record : recordList) {
			if (record.isLocked) {
				// ロックされている
				return true;
			}
			summaryIdSet.add(record.summaryId);
		}

		// 勤怠明細が参照している勤怠サマリーがロックされているかを確認する
		if (! summaryIdSet.isEmpty()) {
			List<AttSummaryEntity> summaryList = repo.getEntityList(new List<Id>(summaryIdSet), false, false);
			for (AttSummaryEntity summary : summaryList) {
				if (summary.isLocked) {
					// ロックされている
					return true;
				}
			}
		}

		// ロックされていない
		return false;
	}

	/** 短時間勤務設定のキャッシュ情報(キーは短時間勤務設定ベースID) */
	private Map<Id, AttShortTimeSettingBaseEntity> shortTimeSettingMap = new Map<Id, AttShortTimeSettingBaseEntity>();
	/**
	 * 指定した短時間勤務設定を取得する(キャッシュ機能あり)
	 * @param baseId ベースID
	 * @return 短時間勤務設定のベース＋全ての履歴、ただし存在しない場合はnull
	 */
	private AttShortTimeSettingBaseEntity getShortTimeSetting(Id baseId) {
		AttShortTimeSettingBaseEntity setting = this.shortTimeSettingMap.get(baseId);
		if (setting == null) {
			setting = new AttShortTimeSettingRepository().getEntity(baseId);
			if (setting != null) {
				this.shortTimeSettingMap.put(setting.id, setting);
			}
		}
		return setting;
	}

	/** 休職休業のキャッシュ情報(キーは休職休業ID) */
	private Map<Id, AttLeaveOfAbsenceEntity> leaveOfAbsenceMap = new Map<Id, AttLeaveOfAbsenceEntity>();
	/**
	 * 指定した休職休業を取得する(キャッシュ機能あり)
	 * @param leaveOfAbsenceId 休職休業ID
	 * @return 休職休業、ただし存在しない場合はnull
	 */
	private AttLeaveOfAbsenceEntity getLeaveOfAbsence(Id leaveOfAbsenceId) {
		AttLeaveOfAbsenceEntity absence = this.leaveOfAbsenceMap.get(leaveOfAbsenceId);
		if (absence == null) {
			absence = new AttLeaveOfAbsenceRepository().searchById(leaveOfAbsenceId);
			if (absence != null) {
				this.leaveOfAbsenceMap.put(absence.id, absence);
			}
		}
		return absence;
	}

	/** 社員のキャッシュ情報(キーは社員ベースID) */
	private Map<Id, EmployeeBaseEntity> employeeMap = new Map<Id, EmployeeBaseEntity>();
	/**
	 * 指定した社員を取得する(キャッシュ機能あり)
	 * @param baseId 社員ベースID
	 * @return 社員のベース＋全ての履歴、ただし存在しない場合はnull
	 */
	private EmployeeBaseEntity getEmployee(Id baseId) {
		EmployeeBaseEntity employee = this.employeeMap.get(baseId);
		if (employee == null) {
			employee = new EmployeeRepository().getEntity(baseId);
			if (employee != null) {
				this.employeeMap.put(employee.id, employee);
			}
		}
		return employee;
	}

	/** 勤務体系のキャッシュ情報(キーは勤務体系ベースID) */
	private Map<Id, AttWorkingTypeBaseEntity> workingTypeMap = new Map<Id, AttWorkingTypeBaseEntity>();
	/**
	 * 指定した勤務体系を取得する(キャッシュ機能あり)
	 * @param baseId 勤務体系ベースID
	 * @return 勤務体系のベース＋全ての履歴、ただし存在しない場合はnull
	 */
	private AttWorkingTypeBaseEntity getWorkingType(Id baseId) {
		AttWorkingTypeBaseEntity workingType = this.workingTypeMap.get(baseId);
		if (workingType == null) {
			workingType = new AttWorkingTypeRepository().getEntity(baseId);
			if (workingType != null) {
				this.workingTypeMap.put(workingType.id, workingType);
			}
		}
		return workingType;
	}

	/**
	 * 指定された社員IDの勤怠期間を取得する
	 * @param employeeId 社員ベースID
	 * @return 勤怠期間一覧
	 */
	private List<AttPeriodStatusEntity> getPeriodStatusByEmployeeId(AttPeriodStatusType type, Id employeeId) {
		AttPeriodStatusRepository.SearchFilter filter = new AttPeriodStatusRepository.SearchFilter();
		filter.employeeIds = new Set<Id>{employeeId};
		filter.periodStatusTypes = new Set<AttPeriodStatusType>{type};

		return new AttPeriodStatusRepository().searchEntityList(filter);
	}

	/**
	 * 指定した期間の日次申請を取得する
	 * @param employeeId 社員ID
	 * @param startDate 取得対象開始日
	 * @param endDate 取得対象終了日
	 */
	private List<AttDailyRequestEntity> getDailyRequests(Id employeeId, AppDate startDate, AppDate endDate) {

		AttDailyRequestRepository.RequestFilter filter =
				new AttDailyRequestRepository.RequestFilter();
		filter.employeeId = employeeId;
		filter.startDate = startDate;
		filter.endDate = endDate;
		filter.isDuration = true;

		final Boolean forUpdate = false;
		return new AttDailyRequestRepository().searchEntityList(filter, forUpdate);
	}
}
