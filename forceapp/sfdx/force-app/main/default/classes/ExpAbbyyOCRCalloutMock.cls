/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Mock class for Abbyy OCR API
 *
 * @group Expense
 */
@isTest
public class ExpAbbyyOCRCalloutMock implements HttpCalloutMock {

	Private String ocrStatus;
	Private Boolean isHttpError;

	// contains taskId
	private static final String PROCESS_RECEIPT_RESPONSE_XML = '<?xml version="1.0" encoding="utf-8"?><response><task id="0be6e9db-059f-430c-972f-31c343aed494" registrationTime="2019-07-31T09:23:15Z" statusChangeTime="2019-07-31T09:23:15Z" status="Queued" filesCount="1" credits="0" estimatedProcessingTime="5" /></response>';
	// contains status InProgress
	private static final String TASK_STATUS_RESPONSE_XML_WITH_STATUS_INPROGRESS = '<?xml version="1.0" encoding="utf-8"?><response><task id="15666eaf-af60-4415-8439-08b7f0147a9a" registrationTime="2019-07-31T10:49:03Z" statusChangeTime="2019-07-31T10:49:04Z" status="InProgress" filesCount="1" credits="10" estimatedProcessingTime="5" /></response>';
	// contains status Complete
	private static final String TASK_STATUS_RESPONSE_XML_WITH_STATUS_COMPLETE = '<?xml version="1.0" encoding="utf-8"?><response><task id="0be6e9db-059f-430c-972f-31c343aed494" registrationTime="2019-07-31T10:07:48Z" statusChangeTime="2019-07-31T10:07:49Z" status="Completed" filesCount="1" credits="0" resultUrl="https://ocrsdkwestus.blob.core.windows.net/files/757c4866-95be-4302-b70f-3a28bc72e816.result?sv=2012-02-12&amp;se=2019-07-31T20%3A00%3A00Z&amp;sr=b&amp;si=downloadResults&amp;sig=fSZxg4o43LhZ0vxwKBP2hWFmFV3GIHAaC3k5L1VTatA%3D" /></response>';
	// contains OCR result ex) date, amount
	private static final String OCR_RESULT_XML = '<?xml version="1.0" encoding="UTF-8"?><receipts count="1" xmlns="https://www.abbyy.com/ReceiptCaptureSDK_xml/ReceiptCapture-1.1.xsd"><receipt currency="USD" rotation="RT_NoRotation"><vendor confidence="0.000" isSuspicious="true"><name confidence="58" isSuspicious="true"><classifiedValue>McDonald&apos;s</classifiedValue></name><fullAddress><text><![CDATA[14018 W MAIN STCUT OFF. LA 70345]]></text></fullAddress><address><text><![CDATA[14018 W MAIN ST]]></text></address><phone confidence="52" isSuspicious="true"><normalizedValue>9857987702</normalizedValue><recognizedValue><text>9857987702</text></recognizedValue></phone><purchaseType>Restaurant</purchaseType><city><normalizedValue>Cut Off</normalizedValue><recognizedValue><text>CUT OFF</text></recognizedValue></city><zip confidence="98" isSuspicious="false"><normalizedValue>70345</normalizedValue><recognizedValue><text>70345</text></recognizedValue></zip><administrativeRegion><normalizedValue>LA</normalizedValue><recognizedValue><text>LA</text></recognizedValue></administrativeRegion></vendor><date confidence="100" isSuspicious="false"><normalizedValue>2018-07-27</normalizedValue><recognizedValue><text>07/27/2018</text></recognizedValue></date><time confidence="99" isSuspicious="false"><normalizedValue>10:15:00</normalizedValue><recognizedValue><text>10:15 AM</text></recognizedValue></time><total confidence="0" isSuspicious="true"><normalizedValue>5.27</normalizedValue><recognizedValue><text>527</text></recognizedValue></total><tax total="false"><normalizedValue>5.27</normalizedValue><recognizedValue><text>5.27</text></recognizedValue></tax><payment type="Undefined"><value><normalizedValue>5.27</normalizedValue><recognizedValue><text>527</text></recognizedValue></value></payment><recognizedItems count="2"><item index="1"><name confidence="0" isSuspicious="true"><text/></name><count confidence="19" isSuspicious="true"><normalizedValue>1.000</normalizedValue></count><recognizedText><![CDATA[1 Egg McMuffln Ml-Hb]]></recognizedText><amountUnits>unit</amountUnits></item><item index="2"><name confidence="0" isSuspicious="true"><text/></name><count confidence="19" isSuspicious="true"><normalizedValue>1.000</normalizedValue></count><total confidence="84" isSuspicious="false"><normalizedValue>4.80</normalizedValue><recognizedValue><text>4.80</text></recognizedValue></total><recognizedText><![CDATA[1 M Coke 4]� Upcharge>I, 4.80]]></recognizedText><amountUnits>unit</amountUnits></item></recognizedItems><country confidence="100" isSuspicious="false"><normalizedValue>USA</normalizedValue></country><recognizedText><![CDATA[HUY UNt (itI UNt FREE QUARIER POUNDERW/CHEESE OR EGG MCMUFHNGo to www.wcdvolce.co* within 7 daysand tel I us about your visit.Validation Code:Expires 30 days after receipt dateValid at participating US McDonald\'s.Survey Code:06203-02420-72718-10159-00052-7McDonald\'s Restaurant #6 20314018 W MAIN STCUT OFF. LA 70345TEL# 985 798 7702uc# n               07/27/2018 10:15 AMKS# 2                           Order 42S1de11 Egg McMuffln Ml-Hb1 M Coke 4]^ Upcharge>I,                           4.80Subtotal                             q 47Tax                               5.27Take-Out Total5.27Cashless                           n qqk Change]]></recognizedText></receipt></receipts>';

	public ExpAbbyyOCRCalloutMock(String ocrStatus, Boolean isHttpError) {
		this.ocrStatus = ocrStatus;
		this.isHttpError = isHttpError;
	}

	/*
	 * response method
	 */
	public HttpResponse respond(HttpRequest req) {

		String abbyyFunctionIdentifier = req.getEndpoint().substringBefore('?').substringAfter('.com/');
		HttpResponse res;

		if (abbyyFunctionIdentifier == 'processReceipt') {
			res = createResponse(PROCESS_RECEIPT_RESPONSE_XML, 200);
		} else if (abbyyFunctionIdentifier == 'getTaskStatus') {
			if ('InProgress'.equals(this.ocrStatus)) {
				res = createResponse(TASK_STATUS_RESPONSE_XML_WITH_STATUS_INPROGRESS, 200);
			} else if (this.ocrStatus == 'Completed') {
				res = createResponse(TASK_STATUS_RESPONSE_XML_WITH_STATUS_COMPLETE, 200);
			}
		} else if (req.getEndpoint().contains('https://ocrsdkwestus.blob.core.windows.net/files')) {
			res = createResponse(OCR_RESULT_XML, 200);
		}

		if(this.isHttpError) {
			res = createResponse('', 400);
		}

		return res;
	}

	/*
	 * create Http response
	 */
	private HttpResponse createResponse(String body, Integer httpStatus) {
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/xml');
		res.setBody(body);
		res.setStatusCode(httpStatus);
		return res;
	}
}