/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 新規部署登録バッチ処理
 */
public with sharing class DepartmentInsertBatch implements Database.Batchable<sObject>, Database.Stateful {

	/** インポートバッチID */
	private Id importBatchId;

	/** 既存の会社データ */
	@TestVisible
	private Map<String, CompanyEntity> companyMap;
	/** 既存の部署データ */
	@TestVisible
	// キーは会社コード・部署コード
	private Map<List<String>, DepartmentBaseEntity> deptMap;

	/** 成功件数 */
	private Integer successCount = 0;
	/** 失敗件数 */
	private Integer errorCount = 0;
	/** 例外発生 */
	private Boolean exceptionOccurred = false;


	/**
	 * コンストラクタ
	 * @param importBatchId インポートバッチID
	 */
	public DepartmentInsertBatch(Id importBatchId) {
		this.importBatchId = importBatchId;
	}

	/**
	 * バッチ開始処理
	 * @param bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {
		// 部署インポートデータを取得
		String query = 'SELECT Id, ' +
							'Name, ' +
							'ImportBatchId__c, ' +
							'RevisionDate__c, ' +
							'AbolishmentDate__c, ' +
							'Code__c, ' +
							'CompanyCode__c, ' +
							'Name_L0__c, ' +
							'Name_L1__c, ' +
							'Name_L2__c, ' +
							'HistoryComment__c, ' +
							'Status__c, ' +
							'Error__c ' +
						'FROM ComDepartmentImport__c ' +
						'WHERE ImportBatchId__c = :importBatchId ' +
						'AND Status__c IN (\'Waiting\', \'Error\') ' +
						'ORDER BY Code__c, RevisionDate__c, AbolishmentDate__c, CreatedDate';

		return Database.getQueryLocator(query);
	}

	/**
	 * バッチ実行処理
	 * @param bc バッチコンテキスト
	 * @param scope バッチ処理対象レコードのリスト
	 */
	public void execute(Database.BatchableContext bc, List<ComDepartmentImport__c> scope) {
		this.companyMap = new Map<String, CompanyEntity>();
		this.deptMap = new Map<List<String>, DepartmentBaseEntity>();

		List<DepartmentImportEntity> deptImpList = new List<DepartmentImportEntity>();
		List<DepartmentImportEntity> deptImpExcludedList = new List<DepartmentImportEntity>();

		Set<String> companyCodeSet = new Set<String>();
		Set<String> deptCodeSet = new Set<String>();

		for (ComDepartmentImport__c obj : scope) {

			DepartmentImportEntity entity = this.createDeptImpEntity(obj);

			// 廃止日が設定されている場合は有効期間終了日の更新のみを行なうため
			// 本バッチでは処理対象外
			if (entity.abolishmentDate != null) {
				deptImpExcludedList.add(entity);
				continue;
			}
			deptImpList.add(entity);

			// 参照先のコードを取得する
			if (String.isNotEmpty(entity.code)) {
				deptCodeSet.add(entity.code);
			}
			if (String.isNotEmpty(entity.companyCode)) {
				companyCodeSet.add(entity.companyCode);
			}
		}

		// 本バッチで処理対象外のデータのステータスを更新する
		updateStatusOfExcludedImp(deptImpExcludedList);

		// 参照先のデータを取得
		// 会社
		if (companyCodeSet.size() > 0) {
			CompanyRepository companyRepo = new CompanyRepository();
			for (CompanyEntity company : companyRepo.getEntityList(new List<String>(companyCodeSet))) {
				this.companyMap.put(company.code, company);
			}
		}
		// 部署
		DepartmentService deptService = new DepartmentService();
		if (deptCodeSet.size() > 0) {
			for (ParentChildBaseEntity dept : deptService.getEntityListByCode(new List<String>(deptCodeSet))) {
				List<String> key = new List<String>{((DepartmentBaseEntity)dept).company.code, ((DepartmentBaseEntity)dept).code};
				this.deptMap.put(key, (DepartmentBaseEntity)dept);
			}
		}

		Savepoint sp = Database.setSavepoint();

		// 新規データの登録
		for (DepartmentImportEntity deptImp : deptImpList) {
			try {
				// バリデーション
				this.validateDeptImpEntity(deptImp);

				// 既存データが存在する or エラーがある場合は何もしない
				List<String> deptMapKey = new List<String>{deptImp.companyCode, deptImp.code};
				if (this.deptMap.containsKey(deptMapKey) || ImportStatus.ERROR.equals(deptImp.status)) {
					continue;
				}

				// 登録用の部署エンティティを作成
				DepartmentBaseEntity deptBase = this.createDeptEntity(deptImp);

				// 登録
				deptService.saveNewEntity(deptBase);

				deptImp.status = ImportStatus.WAITING;
				deptImp.error = null;

				// 同じコードのデータが複数件含まれているかもしれないので、Mapにも追加しておく
				this.deptMap.put(deptMapKey, deptBase);
			} catch (App.BaseException e) {
				deptImp.status = ImportStatus.ERROR;
				deptImp.error = e.getMessage();
				this.errorCount++;
			} catch (Exception e) {
				Database.rollback(sp);
				deptImp.status = ImportStatus.ERROR;
				deptImp.error = e.getMessage();
				this.exceptionOccurred = true;
				break;
			}
		}

		// 部署インポートデータを更新
		DepartmentImportRepository deptImpRepo = new DepartmentImportRepository();
		deptImpRepo.saveEntityList(deptImpList);
	}

	/**
	 * バッチ終了処理
	 * @param bc バッチコンテキスト
	 */
	public void finish(Database.BatchableContext bc) {
		// インポートバッチデータを更新
		ImportBatchRepository batchRepo = new ImportBatchRepository();
		ImportBatchEntity batch = batchRepo.getEntity(this.importBatchId);
		batch.status = this.exceptionOccurred ? ImportBatchStatus.FAILED : ImportBatchStatus.PROCESSING;
		batch.successCount += this.successCount;
		batch.failureCount += this.errorCount;
		batchRepo.saveEntity(batch);

		if (!this.exceptionOccurred) {
			if (!Test.isRunningTest()) {
				// 次の処理を呼び出し（新規社員登録）
				EmployeeInsertBatch empInsert = new EmployeeInsertBatch(importBatchId);
				Database.executeBatch(empInsert, ImportBatchService.BATCH_SIZE_EMP_INSERT);
			}
		}
	}

	//--------------------------------------------------------------------------------

	/**
	 * 部署インポートエンティティを作成する
	 * @param obj 部署インポートオブジェクトデータ
	 * @return 部署インポートエンティティ
	 */
	private DepartmentImportEntity createDeptImpEntity(ComDepartmentImport__c obj) {
		DepartmentImportEntity entity = new DepartmentImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.abolishmentDate = AppDate.valueOf(obj.AbolishmentDate__c);
		entity.code = ImportBatchService.convertValue(obj.Code__c);
		entity.companyCode = ImportBatchService.convertValue(obj.CompanyCode__c);
		entity.name_L0 = ImportBatchService.convertValue(obj.Name_L0__c);
		entity.name_L1 = ImportBatchService.convertValue(obj.Name_L1__c);
		entity.name_L2 = ImportBatchService.convertValue(obj.Name_L2__c);
		entity.historyComment = ImportBatchService.convertValue(obj.HistoryComment__c);

		entity.resetChanged();
		entity.status = ImportStatus.WAITING;
		entity.error = null;

		return entity;
	}

	/**
	 * バリデーションを行う
	 * @param deptImp 部署インポートエンティティ
	 */
	@TestVisible
	private void validateDeptImpEntity(DepartmentImportEntity deptImp) {
		List<String> deptMapKey = new List<String>{deptImp.companyCode, deptImp.code};

		// 改訂日が未設定
		if (deptImp.revisionDate == null) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_RevisionDate});
			this.errorCount++;
			return;
		}

		// 部署コードが未設定
		if (String.isBlank(deptImp.code)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentCode});
			this.errorCount++;
			return;
		}

		// 会社コードが未設定
		if (String.isBlank(deptImp.companyCode)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			this.errorCount++;
			return;
		}
		// 指定した会社が存在しない
		if (!this.companyMap.containsKey(deptImp.companyCode)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			this.errorCount++;
			return;
		}

		// 部署名(L0)が未設定（新規の場合のみ）
		if (!this.deptMap.containsKey(deptMapKey) && String.isBlank(deptImp.name_L0)) {
			deptImp.status = ImportStatus.ERROR;
			deptImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentName});
			this.errorCount++;
			return;
		}
	}

	/**
	 * 部署エンティティを作成する（ベース＋履歴）
	 * @param deptImp 部署インポートエンティティ
	 * @return 部署ベースエンティティ
	 */
	private DepartmentBaseEntity createDeptEntity(DepartmentImportEntity deptImp) {
		// ベース
		DepartmentBaseEntity base = new DepartmentBaseEntity();
		base.name = deptImp.name_L0;
		base.code = deptImp.code;
		base.uniqKey = base.createUniqKey(deptImp.companyCode, deptImp.code);
		base.companyId = this.companyMap.get(deptImp.companyCode).id;

		// 履歴（相互参照項目は登録しない）
		DepartmentHistoryEntity history = new DepartmentHistoryEntity();
		history.name = deptImp.name_L0;
		history.nameL0 = deptImp.name_L0;
		history.nameL1 = deptImp.name_L1;
		history.nameL2 = deptImp.name_L2;
		history.historyComment = deptImp.historyComment;
		history.validFrom = deptImp.revisionDate;
		history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;

		// 履歴をベースに追加
		base.addHistory(history);

		return base;
	}

	/**
	 * 本バッチで処理対象外データのステータスを更新する
	 */
	private void updateStatusOfExcludedImp(List<DepartmentImportEntity> deptImpExcludedList) {

		// ステータスを設定
		for (DepartmentImportEntity deptImp : deptImpExcludedList) {
			deptImp.status = ImportStatus.WAITING;
			deptImp.error = null;
		}

		// 社員インポートデータを更新
		DepartmentImportRepository deptImpRepo = new DepartmentImportRepository();
		deptImpRepo.saveEntityList(deptImpExcludedList);
	}
}