/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 拡張項目入力タイプ
 */
public with sharing class ExtendedItemInputType extends ValueObjectType {

	/** テキスト */
	public static final ExtendedItemInputType TEXT = new ExtendedItemInputType('Text');
	/** 選択リスト */
	public static final ExtendedItemInputType PICKLIST = new ExtendedItemInputType('Picklist');
	/** Custom Extended Item*/
	public static final ExtendedItemInputType CUSTOM = new ExtendedItemInputType('Lookup');
	/** Date Extended Item */
	public static final ExtendedItemInputType DATE_TYPE = new ExtendedItemInputType('Date');

	/** 入力タイプのリスト（入力タイプが追加されら本リストにも追加してください) */
	public static final List<ExtendedItemInputType> TYPE_LIST;
	static {
		TYPE_LIST = new List<ExtendedItemInputType> {
			TEXT,
			PICKLIST,
			CUSTOM,
			DATE_TYPE
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ExtendedItemInputType(String value) {
		super(value);
	}

	/**
	 * 値からExtendedItemInputTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つExtendedItemInputType、ただし該当するExtendedItemInputTypeが存在しない場合はnull
	 */
	public static ExtendedItemInputType valueOf(String value) {
		ExtendedItemInputType retType = null;
		if (String.isNotBlank(value)) {
			for (ExtendedItemInputType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ExtendedItemInputType以外の場合はfalse
		Boolean eq;
		if (compare instanceof ExtendedItemInputType) {
			// 値が同じであればtrue
			if (this.value == ((ExtendedItemInputType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}