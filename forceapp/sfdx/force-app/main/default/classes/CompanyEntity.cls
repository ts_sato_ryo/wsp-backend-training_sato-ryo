/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 会社エンティティ
 */
public with sharing class CompanyEntity extends LogicalDeleteEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 10;
	/** 会社名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		CODE,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		COUNTRY_ID,
		CURRENCY_ID,
		LANGUAGE,
		USE_ATTENDANCE,
		USE_EXPENSE,
		USE_EXPENSE_REQUEST,
		USE_WORKTIME,
		USE_PLANNER,
		USE_PRE_APPLICATION,
		CALENDAR_ID,
		PLANNER_DEFAULT_VIEW,
		USE_CALENDAR_ACCESS,
		CALENDAR_ACCESS_SERVICE,
		USE_COMPANY_TAX_MASTER,
		JORUDAN_FARE_TYPE,
		JORUDAN_AREA_PREFERENCE,
		JORUDAN_USE_CHARGED_EXPRESS,
		JORUDAN_CHARGED_EXPRESS_DISTANCE,
		JORUDAN_SEAT_PREFERENCE,
		JORUDAN_ROUTE_SORT,
		JORUDAN_COMMUTER_PASS,
		JORUDAN_HIGHWAY_BUS,
		REQUIRE_LOCATION_AT_MOBILE_STAMP,
		ALLOW_TAX_AMOUNT_CHANGE
	}

	/** Class of Currency lookup fields (for reference) */
	public class CurrencyInfo {
		/** Name */
		public AppMultiString nameL;
		/** Currency Code */
		public String code;
		/** Currency Symbol */
		public String symbol;
		/** Currency Decimal Places */
		public Integer decimalPlaces;
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/** 会社名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}
	/** 会社名(多言語対応) */
	public AppMultiString nameL {
		get {return new AppMultiString(nameL0, nameL1, nameL2);}
	}
	/** 会社名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}
	/** 会社名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}
	/** 会社名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}
	/** コード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}
	/** 国ID */
	public ID countryId {
		get;
		set {
			countryId = value;
			setChanged(Field.COUNTRY_ID);
		}
	}
	/** 国名(参照専用) */
	public AppMultiString countryNameL {
		get;
		set {
			countryNameL = value;
		}
	}
	/** Currency ID */
	public ID currencyId {
		get;
		set {
			currencyId = value;
			setChanged(Field.CURRENCY_ID);
		}
	}
	/** Currency information (for reference) */
	public CurrencyInfo currencyInfo;
	/** デフォルト言語 */
	public AppLanguage language {
		get;
		set {
			language = value;
			setChanged(Field.LANGUAGE);
		}
	}
	/** 勤怠機能を利用するフラグ */
	public Boolean useAttendance {
		get;
		set {
			useAttendance = value;
			setChanged(Field.USE_ATTENDANCE);
		}
	}
	/** 工数管理機能を利用するフラグ */
	public Boolean useWorkTime {
		get;
		set {
			useWorkTime = value;
			setChanged(Field.USE_WORKTIME);
		}
	}
	/** プランナー機能を利用するフラグ */
	public Boolean usePlanner {
		get;
		set {
			usePlanner = value;
			setChanged(Field.USE_PLANNER);
		}
	}
	/** 経費申請機能を利用するフラグ */
	public Boolean useExpense {
		get;
		set {
			useExpense = value;
			setChanged(Field.USE_EXPENSE);
		}
	}
	/** Flag to use Advance Expense Request */
	public Boolean useExpenseRequest {
		get;
		set {
			useExpenseRequest = value;
			setChanged(Field.USE_EXPENSE_REQUEST);
		}
	}
	/** カレンダーId */
	public Id calendarId {
		get;
		set {
			calendarId = value;
			setChanged(Field.CALENDAR_ID);
		}
	}
	/** プランナーデフォルト表示 */
	public PlannerViewType plannerDefaultView {
		get;
		set {
			plannerDefaultView = value;
			setChanged(Field.PLANNER_DEFAULT_VIEW);
		}
	}
	/** 外部カレンダー連携を利用するフラグ */
	public Boolean useCalendarAccess {
		get;
		set {
			useCalendarAccess = value;
			setChanged(Field.USE_CALENDAR_ACCESS);
		}
	}
	/** 外部カレンダー連携先 */
	public CalendarAccessServiceName calendarAccessService {
		get;
		set {
			calendarAccessService = value;
			setChanged(Field.CALENDAR_ACCESS_SERVICE);
		}
	}
	/** 会社の税区分マスタを使用するフラグ */
	public Boolean useCompanyTaxMaster {
		get;
		set {
			useCompanyTaxMaster = value;
			setChanged(Field.USE_COMPANY_TAX_MASTER);
		}
	}
	/** ジョルダン運賃種別 */
	public Decimal jorudanFareType {
		get;
		set {
			jorudanFareType = value;
			setChanged(Field.JORUDAN_FARE_TYPE);
		}
	}
	/** ジョルダンエリア優先 */
	public String jorudanAreaPreference {
		get;
		set {
			jorudanAreaPreference = value;
			setChanged(Field.JORUDAN_AREA_PREFERENCE);
		}
	}
	/** ジョルダン有料特急利用 */
	public Decimal jorudanUseChargedExpress {
		get;
		set {
			jorudanUseChargedExpress = value;
			setChanged(Field.JORUDAN_USE_CHARGED_EXPRESS);
		}
	}
	/** ジョルダン有料特急利用距離 */
	public Decimal jorudanChargedExpressDistance {
		get;
		set {
			jorudanChargedExpressDistance = value;
			setChanged(Field.JORUDAN_CHARGED_EXPRESS_DISTANCE);
		}
	}
	/** ジョルダン優先座席 */
	public Decimal jorudanSeatPreference {
		get;
		set {
			jorudanSeatPreference = value;
			setChanged(Field.JORUDAN_SEAT_PREFERENCE);
		}
	}
	/** ジョルダン路線表示順 */
	public Decimal jorudanRouteSort {
		get;
		set {
			jorudanRouteSort = value;
			setChanged(Field.JORUDAN_ROUTE_SORT);
		}
	}
	/** ジョルダン定期区間取り扱い */
	public Decimal jorudanCommuterPass {
		get;
		set {
			jorudanCommuterPass = value;
			setChanged(Field.JORUDAN_COMMUTER_PASS);
		}
	}
	/** ジョルダン高速バス利用 */
	public Decimal jorudanHighwayBus {
		get;
		set {
			jorudanHighwayBus = value;
			setChanged(Field.JORUDAN_HIGHWAY_BUS);
		}
	}
	/** モバイル打刻時に位置情報が必要 */
	public Boolean requireLocationAtMobileStamp {
		get {
			return requireLocationAtMobileStamp == null ? false : requireLocationAtMobileStamp;
		}
		set {
			requireLocationAtMobileStamp = value;
			setChanged(Field.REQUIRE_LOCATION_AT_MOBILE_STAMP);
		}
	}
	/** Allow to Modify Tax Amount */
	public Boolean allowTaxAmountChange {
		get {
			return allowTaxAmountChange == null ? false : allowTaxAmountChange;
		}
		set {
			allowTaxAmountChange = value;
			setChanged(Field.ALLOW_TAX_AMOUNT_CHANGE);
		}
	}
	/**
	 * コンストラクタ
	 */
	public CompanyEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * (親クラスを含まない)クラス内の項目変更情報をリセットする
	 */
	protected override void resetChangedInSubClass() {
		this.isChangedFieldSet.clear();
	}


}