/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 勤怠期間リポジトリ
 */
public with sharing class AttPeriodStatusRepository extends Repository.BaseRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** クエリ実行時のソート順 */
	public enum SortOrder {
		VALID_FROM_ASC, VALID_FROM_DESC
	}


	/**
	 * 取得対象の項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> SELECT_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				AttPeriodStatus__c.Id,
				AttPeriodStatus__c.Name,
				AttPeriodStatus__c.OwnerId,
				AttPeriodStatus__c.EmployeeBaseId__c,
				AttPeriodStatus__c.ValidFrom__c,
				AttPeriodStatus__c.ValidTo__c,
				AttPeriodStatus__c.Type__c,
				AttPeriodStatus__c.ShortTimeWorkSettingBaseId__c,
				AttPeriodStatus__c.LeaveOfAbsenceId__c,
				AttPeriodStatus__c.Comment__c,
				AttPeriodStatus__c.LastModifiedDate};

		SELECT_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/**
	 * 取得対象の社員の項目名
	 */
	private static final List<String> SELECT_EMPLOYEE_FIELD_NAME_LIST;
	/** 社員のリレーション名 */
	private static final String EMPLOYEE_R = AttPeriodStatus__c.EmployeeBaseId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> employeeFieldList = new Set<Schema.SObjectField> {
			ComEmpBase__c.FirstName_L0__c,
			ComEmpBase__c.LastName_L0__c,
			ComEmpBase__c.FirstName_L1__c,
			ComEmpBase__c.LastName_L1__c,
			ComEmpBase__c.FirstName_L2__c,
			ComEmpBase__c.LastName_L2__c};

		SELECT_EMPLOYEE_FIELD_NAME_LIST = Repository.generateFieldNameList(EMPLOYEE_R, employeeFieldList);
	}

	/**
	 * 休職休業の項目名
	 */
	private static final List<String> SELECT_LEAVE_OF_ABSENCE_FIELD_NAME_LIST;
	/** 休職休業のリレーション名 */
	private static final String LEAVE_OF_ABSENCE_R = AttPeriodStatus__c.LeaveOfAbsenceId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> employeeFieldList = new Set<Schema.SObjectField> {
			AttLeaveOfAbsence__c.Name_L0__c,
			AttLeaveOfAbsence__c.Name_L1__c,
			AttLeaveOfAbsence__c.Name_L2__c};

		SELECT_LEAVE_OF_ABSENCE_FIELD_NAME_LIST = Repository.generateFieldNameList(LEAVE_OF_ABSENCE_R, employeeFieldList);
	}

	/**
	 * 指定したIDの勤怠期間を取得する
	 * @param id 取得対象の勤怠期間ID
	 * @return 条件に一致した勤怠期間、存在しない場合はnull
	 */
	public AttPeriodStatusEntity getEntity(Id id) {
		List<AttPeriodStatusEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したIDの勤怠期間を取得する
	 * @param ids 取得対象の勤怠期間IDのリスト
	 * @return 条件に一致した勤怠期間
	 */
	public List<AttPeriodStatusEntity> getEntityList(List<Id> ids) {
		AttPeriodStatusRepository.SearchFilter filter = new SearchFilter();
		if (ids != null) {
			filter.ids = new Set<Id>(ids);
		}
		return searchEntityList(filter, NOT_FOR_UPDATE, AttPeriodStatusRepository.SortOrder.VALID_FROM_ASC);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** 勤怠期間ID */
		public Set<Id> ids;
		/** 社員ベースID */
		public Set<Id> employeeIds;
		/** 勤怠期間種別 */
		public Set<AttPeriodStatusType> periodStatusTypes;
		/** 取得対象日 */
		public AppDate targetDate;
		/** 取得対象期間日*/
		public AppDate targetStartDate;
		public AppDate targetEndDate;

	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果
	 */
	public List<AttPeriodStatusEntity> searchEntityList(AttPeriodStatusRepository.SearchFilter filter) {
		return searchEntityList(filter, NOT_FOR_UPDATE, AttPeriodStatusRepository.SortOrder.VALID_FROM_ASC);
	}

	/**
	 * 勤怠期間を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 検索結果の並び順
	 * @return 検索結果
	 */
	public List<AttPeriodStatusEntity> searchEntityList(
			AttPeriodStatusRepository.SearchFilter filter, Boolean forUpdate,
			AttPeriodStatusRepository.SortOrder sortOrder) {

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(SELECT_FIELD_NAME_LIST);
		// 社員参照先項目
		selectFieldList.addAll(SELECT_EMPLOYEE_FIELD_NAME_LIST);
		// 休職休業参照項目
		selectFieldList.addAll(SELECT_LEAVE_OF_ABSENCE_FIELD_NAME_LIST);


		// WHERE句
		List<String> whereList = new List<String>();
		// 36協定アラート設定IDで検索
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// 社員IDで検索
		Set<Id> pEmployeeIds;
		if (filter.employeeIds != null) {
			pEmployeeIds = filter.employeeIds;
			whereList.add(getFieldName(AttPeriodStatus__c.EmployeeBaseId__c) + ' IN :pEmployeeIds');
		}
		// 勤怠期間種別で検索
		Set<String> pPeriodStatusTypes;
		if (filter.periodStatusTypes != null) {
			pPeriodStatusTypes = new Set<String>();
			for (AttPeriodStatusType type : filter.periodStatusTypes) {
				pPeriodStatusTypes.add(AppConverter.stringValue(type));
			}
			whereList.add(getFieldName(AttPeriodStatus__c.Type__c) + ' IN :pPeriodStatusTypes');
		}
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(AttPeriodStatus__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(AttPeriodStatus__c.ValidTo__c) + ' > :pTargetDate');
		}
		Date pTargetStartDate;
		Date pTargetEndDate;
		if(filter.targetStartDate != null && filter.targetEndDate != null){
			pTargetStartDate = filter.targetStartDate.getDate();
			pTargetEndDate = filter.targetEndDate.getDate();
			whereList.add(getFieldName(AttPeriodStatus__c.ValidFrom__c) + ' <= :pTargetEndDate');
			whereList.add(getFieldName(AttPeriodStatus__c.ValidTo__c) + ' > :pTargetStartDate');
		}
		String whereString = buildWhereString(whereList);


		// ORDER BY句
		String orderBy = ' ORDER BY ';
		if (sortOrder == AttPeriodStatusRepository.SortOrder.VALID_FROM_ASC) {
			orderBy += getFieldName(AttPeriodStatus__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == AttPeriodStatusRepository.SortOrder.VALID_FROM_DESC) {
			orderBy += getFieldName(AttPeriodStatus__c.ValidFrom__c) + ' DESC NULLS LAST';
		}


		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + AttPeriodStatus__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderBy
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('AttPeriodStatusRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttPeriodStatus__c> sObjs = (List<AttPeriodStatus__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttPeriodStatus__c.SObjectType);

		// SObjectからエンティティを作成
		List<AttPeriodStatusEntity> entities = new List<AttPeriodStatusEntity>();
		for (AttPeriodStatus__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	/**
	 * エンティティをレコードとして保存する
	 * @param entity 作成元のエンティティ
	 * @return レコード保存結果
	 */
	public SaveResult saveEntity(AttPeriodStatusEntity entity) {
		return saveEntityList(new List<AttPeriodStatusEntity>{ entity }, true);
	}


	/**
	 * エンティティをレコードとして保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return レコード保存結果
	 */
	public SaveResult saveEntityList(List<AttPeriodStatusEntity> entityList) {
		return saveEntityList(entitylist, true);
	}

	/**
	 * エンティティをレコードとして保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return レコード保存結果
	 */
	public SaveResult saveEntityList(List<AttPeriodStatusEntity> entityList, Boolean allOrNone) {

		List<AttPeriodStatus__c> sObjList = createSObjectList(entityList);

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sObjList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * SObjectからエンティティを作成する
	 * @param obj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AttPeriodStatusEntity createEntity(AttPeriodStatus__c sObj) {

		AttPeriodStatusEntity entity = new AttPeriodStatusEntity();

		entity.setId(sObj.Id);
		entity.name = sObj.Name;
		entity.ownerId = sObj.OwnerId;
		entity.employeeBaseId = sObj.EmployeeBaseId__c;
		entity.type = AttPeriodStatusType.valueOf(sObj.Type__c);
		entity.validFrom = AppDate.valueOf(sObj.ValidFrom__c);
		entity.validTo = AppDate.valueOf(sObj.ValidTo__c);
		entity.shortTimeWorkSettingBaseId = sObj.ShortTimeWorkSettingBaseId__c;
		entity.leaveOfAbsenceId = sObj.LeaveOfAbsenceId__c;
		entity.comment = sObj.Comment__c;
		entity.lastModifiedDate = AppDateTime.valueOf(sObj.LastModifiedDate);

		// 社員
		if (sObj.EmployeeBaseId__c != null) {
			entity.employee = new AttPeriodStatusEntity.Employee(new AppPersonName(
				sObj.EmployeeBaseId__r.FirstName_L0__c, sObj.EmployeeBaseId__r.LastName_L0__c,
				sObj.EmployeeBaseId__r.FirstName_L1__c, sObj.EmployeeBaseId__r.LastName_L1__c,
				sObj.EmployeeBaseId__r.FirstName_L2__c, sObj.EmployeeBaseId__r.LastName_L2__c));
		}

		// 休職休業
		if (sObj.LeaveOfAbsenceId__c != null) {
			entity.leaveOfAbsence = new AttPeriodStatusEntity.LeaveOfAbsence(new AppMultiString(
					sObj.LeaveOfAbsenceId__r.Name_L0__c,
					sObj.LeaveOfAbsenceId__r.Name_L1__c,
					sObj.LeaveOfAbsenceId__r.Name_L2__c));
		}

		entity.resetChanged();

		return entity;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	private List<AttPeriodStatus__c> createSObjectList(List<AttPeriodStatusEntity> entityList) {

		List<AttPeriodStatus__c> objectList = new List<AttPeriodStatus__c>();
		for (AttPeriodStatusEntity entity : entityList) {
			objectList.add(createSObject(entity));
		}

		return objectList;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entity オブジェクト作成元のエンティティ
	 * @return 保存用のオブジェクト
	 */
	private AttPeriodStatus__c createSObject(AttPeriodStatusEntity entity) {

		AttPeriodStatus__c sObj = new AttPeriodStatus__c();

		sObj.Id = entity.id;

		if (entity.isChanged(AttPeriodStatusEntity.Field.NAME)) {
			sObj.Name = entity.name;
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.OWNER_ID)) {
			sObj.OwnerId = entity.ownerId;
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.EMPLOYEE_BASE_ID)) {
			sObj.EmployeeBaseId__c = entity.employeeBaseId;
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.VALID_FROM)) {
			sObj.ValidFrom__c = AppConverter.dateValue(entity.validFrom);
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.VALID_TO)) {
			sObj.ValidTo__c = AppConverter.dateValue(entity.validTo);
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.TYPE)) {
			sObj.Type__c = AppConverter.stringValue(entity.type);
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.SHORT_TIME_WORK_SETTING_BASE_ID)) {
			sObj.ShortTimeWorkSettingBaseId__c = entity.shortTimeWorkSettingBaseId;
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.LEAVE_OF_ABSENCE_ID)) {
			sObj.LeaveOfAbsenceId__c = entity.leaveOfAbsenceId;
		}
		if (entity.isChanged(AttPeriodStatusEntity.Field.COMMENT)) {
			sObj.Comment__c = entity.comment;
		}

		return sObj;
	}
}
