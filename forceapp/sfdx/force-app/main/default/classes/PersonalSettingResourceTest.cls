/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description PersonalSettingResourceのテストクラス
*/
@isTest
private class PersonalSettingResourceTest {

	/**
	 * 個人設定取得APIのテスト
	 * ログインユーザの情報を取得できることを検証する
	 */
	@isTest
	static void getApiTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.id, null, UserInfo.getUserId(), permissionObj.Id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.id);
		ComJob__c jobObj = ComTestDataUtility.createJob('JOB_NAME', companyObj.Id, jobTypeObj.Id);

		PersonalSettingEntity.SearchCondition searchCondition = new PersonalSettingEntity.SearchCondition();
		searchCondition.departmentBaseIds.add('testDepartmentBaseId1');
		searchCondition.departmentBaseIds.add('testDepartmentBaseId2');
		searchCondition.empBaseIds.add('testEmpBaseId1');
		searchCondition.empBaseIds.add('testEmpBaseId2');
		searchCondition.name = 'testName01';

		List<PersonalSettingEntity.SearchCondition> searchConditionList = new List<PersonalSettingEntity.SearchCondition>();
		searchConditionList.add(searchCondition);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.id, jobObj.id, searchConditionList);
		// API実行
		Test.startTest();
		PersonalSettingResource.GetApi api = new PersonalSettingResource.GetApi();
		PersonalSettingResource.PersonalSetting result = (PersonalSettingResource.PersonalSetting)api.execute(new Map<String, Object>());
		Test.stopTest();

		// 検証
		System.assertEquals(jobObj.id, result.defaultJob.id);
		System.assertEquals(jobObj.Name_L0__c, result.defaultJob.name);
		System.assertEquals(jobObj.Code__c, result.defaultJob.code);
		System.assertEquals(Date.newInstance(1900,1,1), result.defaultJob.validDateFrom);
		System.assertEquals(Date.newInstance(2101,1,1), result.defaultJob.validDateTo);
		System.assertEquals(companyObj.PlannerDefaultView__c, result.plannerDefaultView);
		System.assertEquals(searchConditionList[0].departmentBaseIds, result.searchConditionList[0].departmentBaseIds);
		System.assertEquals(searchConditionList[0].empBaseIds, result.searchConditionList[0].empBaseIds);
	}

	/**
	 * 個人設定取得APIのテスト
	 * 指定して社員の個人設定を取得できることを検証する
	 */
	@isTest
	static void getSettingOfTargetEmployeeTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.id);
		ComJob__c job1 = ComTestDataUtility.createJob('Job1', companyObj.Id, jobTypeObj.Id);
		ComJob__c job2 = ComTestDataUtility.createJob('Job2', companyObj.Id, jobTypeObj.Id);

		// 個人設定を2レコード作成する
		ComEmpBase__c employee1 = ComTestDataUtility.createEmployeeWithHistory('employee1', companyObj.id, null, UserInfo.getUserId(), permissionObj.Id);
		ComPersonalSetting__c setting1 = ComTestDataUtility.createPersonalSetting(employee1.Id, job1.Id, null);

		ComEmpBase__c employee2 = ComTestDataUtility.createEmployeeWithHistory('employee2', companyObj.id, null, UserInfo.getUserId(), permissionObj.Id);
		ComPersonalSetting__c setting2 = ComTestDataUtility.createPersonalSetting(employee2.Id, job2.Id, null);

		// API実行
		Test.startTest();
		PersonalSettingResource.GetRequest request = new PersonalSettingResource.GetRequest();
		request.empId = employee2.Id;
		PersonalSettingResource.GetApi api = new PersonalSettingResource.GetApi();
		PersonalSettingResource.PersonalSetting result = (PersonalSettingResource.PersonalSetting)api.execute(request);
		Test.stopTest();

		// 検証
		System.assertEquals(job2.id, result.defaultJob.id);
	}

	/**
	 * 個人設定取得APIのテスト（個人設定レコードなし）
	 */
	@isTest
	static void getApiTestNoPersonalSetting() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.id, null, UserInfo.getUserId(), permissionObj.Id);

		// API実行
		Test.startTest();
		PersonalSettingResource.GetApi api = new PersonalSettingResource.GetApi();
		PersonalSettingResource.PersonalSetting result = (PersonalSettingResource.PersonalSetting)api.execute(new Map<String, Object>());
		Test.stopTest();

		// 検証
		System.assert(result.defaultJob != null);
		System.assert(result.defaultJob.id == null);
		System.assert(result.defaultJob.code == null);
		System.assert(result.defaultJob.name == null);
		System.assert(result.defaultJob.validDateFrom == null);
		System.assert(result.defaultJob.validDateTo == null);
	}

	/**
	 * 個人設定取得APIのテスト（デフォルトジョブ未設定）
	 */
	@isTest
	static void getApiTestNoDefaultJob() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.id, null, UserInfo.getUserId(), permissionObj.id);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.id, null, null);

		// API実行
		Test.startTest();
		PersonalSettingResource.GetApi api = new PersonalSettingResource.GetApi();
		PersonalSettingResource.PersonalSetting result = (PersonalSettingResource.PersonalSetting)api.execute(new Map<String, Object>());
		Test.stopTest();

		// 検証
		System.assert(result.defaultJob != null);
		System.assert(result.defaultJob.id == null);
		System.assert(result.defaultJob.code == null);
		System.assert(result.defaultJob.name == null);
		System.assert(result.defaultJob.validDateFrom == null);
		System.assert(result.defaultJob.validDateTo == null);
	}

	/**
	 * プランナーデフォルト表示取得のテスト
	 * デフォルト値を取得する
	 */
	@isTest
	static void getPlannerDefaultViewTestDefaultValue() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.id, null, UserInfo.getUserId(), permissionObj.Id);
		companyObj.PlannerDefaultView__c = null;
		upsert companyObj;

		// API実行
		Test.startTest();
		EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
		PersonalSettingResource.GetApi api = new PersonalSettingResource.GetApi();
		PlannerViewType defaultView = api.getPlannerDefaultView(employee);
		Test.stopTest();

		// 検証
		System.assertEquals(PlannerViewType.WEEKLY, defaultView);
	}

	/**
	 * 個人設定更新APIのテスト
	 * レコードを新規登録することを検証する
	 */
	@isTest
	static void updateApiTestAddRecord() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.id, null, UserInfo.getUserId(), permissionObj.Id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.id);
		ComJob__c jobObj = ComTestDataUtility.createJob('JOB_NAME', companyObj.Id, jobTypeObj.Id);

		// API実行
		Test.startTest();
		PersonalSettingResource.UpdateApi api = new PersonalSettingResource.UpdateApi();
		PersonalSettingResource.UpdateRequest param = new PersonalSettingResource.UpdateRequest();
		param.defaultJobId = jobObj.id;
		PersonalSettingResource.UpdateResponse response = (PersonalSettingResource.UpdateResponse)api.execute(param);
		Test.stopTest();

		// 検証
		System.assert(response != null);
		List<ComPersonalSetting__c> personalSettings = [SELECT DefaultJobId__c FROM ComPersonalSetting__c WHERE EmployeeBaseId__c = :employeeObj.id];
		System.assertEquals(1, personalSettings.size());
		System.assertEquals(param.defaultJobId, personalSettings.get(0).DefaultJobId__c);
	}

	/**
	 * 個人設定更新APIのテスト
	 * デフォルトジョブを登録することを検証する
	 */
	@isTest
	static void updateApiTestAddDefaultJob() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.Id, null, UserInfo.getUserId(), permissionObj.Id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.Id);
		ComJob__c jobObj = ComTestDataUtility.createJob('JOB_NAME', companyObj.Id, jobTypeObj.Id);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.Id, null, null);

		// API実行
		Test.startTest();
		PersonalSettingResource.UpdateApi api = new PersonalSettingResource.UpdateApi();
		PersonalSettingResource.UpdateRequest param = new PersonalSettingResource.UpdateRequest();
		param.defaultJobId = jobObj.id;
		PersonalSettingResource.UpdateResponse response = (PersonalSettingResource.UpdateResponse)api.execute(param);
		Test.stopTest();

		// 検証
		System.assert(response != null);
		List<ComPersonalSetting__c> personalSettings = [SELECT DefaultJobId__c FROM ComPersonalSetting__c WHERE EmployeeBaseId__c = :employeeObj.id];
		System.assertEquals(1, personalSettings.size());
		System.assertEquals(param.defaultJobId, personalSettings.get(0).DefaultJobId__c);
	}

	/**
	 * 個人設定更新APIのテスト
	 * デフォルトジョブを更新することを検証する
	 */
	@isTest
	static void updateApiTestUpdateDefaultJob() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.Id, null, UserInfo.getUserId(), permissionObj.Id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.Id);
		List<ComJob__c> jobObjs = ComTestDataUtility.createJobs('JOB_NAME', companyObj.Id, jobTypeObj.Id, 3);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.Id, jobObjs.get(1).Id, null);

		// API実行
		Test.startTest();
		PersonalSettingResource.UpdateApi api = new PersonalSettingResource.UpdateApi();
		PersonalSettingResource.UpdateRequest param = new PersonalSettingResource.UpdateRequest();
		param.defaultJobId = jobObjs.get(2).id;
		PersonalSettingResource.UpdateResponse response = (PersonalSettingResource.UpdateResponse)api.execute(param);
		Test.stopTest();

		// 検証
		System.assert(response != null);
		List<ComPersonalSetting__c> personalSettings = [SELECT DefaultJobId__c FROM ComPersonalSetting__c WHERE EmployeeBaseId__c = :employeeObj.id];
		System.assertEquals(1, personalSettings.size());
		System.assertEquals(param.defaultJobId, personalSettings.get(0).DefaultJobId__c);
	}

	/**
	 * 個人設定更新APIのテスト
	 * デフォルトジョブを削除することを検証する
	 */
	@isTest
	static void updateApiTestRemoveDefaultJob() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.Id, null, UserInfo.getUserId(), permissionObj.Id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.Id);
		ComJob__c jobObj = ComTestDataUtility.createJob('JOB_NAME', companyObj.Id, jobTypeObj.Id);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.Id, jobObj.Id, null);

		// API実行
		Test.startTest();
		PersonalSettingResource.UpdateApi api = new PersonalSettingResource.UpdateApi();
		Map<String, Object> param = new Map<String, Object> {
			'defaultJobId' => null
		};
		PersonalSettingResource.UpdateResponse response = (PersonalSettingResource.UpdateResponse)api.execute(param);
		Test.stopTest();

		// 検証
		System.assert(response != null);
		List<ComPersonalSetting__c> personalSettings = [SELECT DefaultJobId__c FROM ComPersonalSetting__c WHERE EmployeeBaseId__c = :employeeObj.id];
		System.assertEquals(1, personalSettings.size());
		System.assert(personalSettings.get(0).DefaultJobId__c == null);
	}

	/**
	 * 個人設定更新APIのテスト
	 * デフォルトジョブが未指定の場合、更新しないことを検証する
	 */
	@isTest
	static void updateApiTestNoDefaultJobParam() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.id, null, UserInfo.getUserId(), permissionObj.Id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.Id);
		ComJob__c jobObj = ComTestDataUtility.createJob('JOB_NAME', companyObj.Id, jobTypeObj.Id);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.Id, jobObj.Id, null);

		// API実行
		Test.startTest();
		PersonalSettingResource.UpdateApi api = new PersonalSettingResource.UpdateApi();
		PersonalSettingResource.UpdateResponse response = (PersonalSettingResource.UpdateResponse)api.execute(new Map<String, Object>());
		Test.stopTest();

		// 検証
		System.assert(response != null);
		List<ComPersonalSetting__c> personalSettings = [SELECT DefaultJobId__c FROM ComPersonalSetting__c WHERE EmployeeBaseId__c = :employeeObj.id];
		System.assertEquals(1, personalSettings.size());
		System.assertEquals(jobObj.id, personalSettings.get(0).DefaultJobId__c);
	}

	/**
	 * 個人設定更新APIのテスト
	 * 承認者01を更新することを検証する
	 */
	@isTest
	static void updateApiTestUpdateApprover01() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('test', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.Id, null, UserInfo.getUserId(), permission.id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.Id);
		ComJob__c jobObj = ComTestDataUtility.createJob('JOB_NAME', companyObj.Id, jobTypeObj.Id);
		User approver01User = ComTestDataUtility.createUser('approver', 'ja', 'ja_JP');
		ComEmpBase__c approver01Obj = ComTestDataUtility.createEmployeeWithHistory('承認者', companyObj.Id, null, approver01User.id, permission.id);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.Id, jobObj.id, null);

		// API実行
		Test.startTest();
		PersonalSettingResource.UpdateApi api = new PersonalSettingResource.UpdateApi();
		Map<String, Object> paramMap = new Map<String, Object>();
		paramMap.put('approverBase01Id', approver01Obj.id);
		PersonalSettingResource.UpdateResponse response = (PersonalSettingResource.UpdateResponse)api.execute(paramMap);
		Test.stopTest();

		// 検証
		System.assert(response != null);
		List<ComPersonalSetting__c> personalSettings = [SELECT ApproverBase01Id__c, DefaultJobId__c FROM ComPersonalSetting__c WHERE EmployeeBaseId__c = :employeeObj.id];
		System.assertEquals(1, personalSettings.size());
		System.assertEquals(approver01Obj.id, personalSettings.get(0).ApproverBase01Id__c);
		System.assertEquals(jobObj.id, personalSettings.get(0).DefaultJobId__c); // ジョブは更新されない
	}

	/**
	 * 個人設定更新APIのテスト
	 * 検索条件を更新できることを検証する
	 */
	@isTest
	static void updateApiTestUpdateSearchCondition() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('test', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('個人設定テスト', companyObj.Id, null, UserInfo.getUserId(), permission.id);
		ComJobType__c jobTypeObj = ComTestDataUtility.createJobType('Job Type', companyObj.Id);
		ComJob__c jobObj = ComTestDataUtility.createJob('JOB_NAME', companyObj.Id, jobTypeObj.Id);

		PersonalSettingEntity.SearchCondition searchCondition = new PersonalSettingEntity.SearchCondition();
		searchCondition.departmentBaseIds.add('testDepartmentBaseId1');
		searchCondition.departmentBaseIds.add('testDepartmentBaseId2');
		searchCondition.empBaseIds.add('testEmpBaseId1');
		searchCondition.empBaseIds.add('testEmpBaseId2');
		List<PersonalSettingEntity.SearchCondition> searchConditionList = new List<PersonalSettingEntity.SearchCondition>();
		searchConditionList.add(searchCondition);
		ComPersonalSetting__c personalObj = ComTestDataUtility.createPersonalSetting(employeeObj.id, jobObj.id, searchConditionList);

		// API実行
		Test.startTest();
		PersonalSettingResource.UpdateApi api = new PersonalSettingResource.UpdateApi();
		PersonalSettingResource.UpdateRequest param = new PersonalSettingResource.UpdateRequest();
		param.searchConditionList = searchConditionList;

		PersonalSettingResource.UpdateResponse response = (PersonalSettingResource.UpdateResponse)api.execute(param);
		Test.stopTest();

		// 検証
		System.assert(response != null);
		List<ComPersonalSetting__c> personalSettings = [SELECT FinanceApprovalSearchConditionList__c FROM ComPersonalSetting__c WHERE EmployeeBaseId__c = :employeeObj.id];
		System.assertEquals(1, personalSettings.size());

		System.assertEquals(personalObj.FinanceApprovalSearchConditionList__c, personalSettings.get(0).FinanceApprovalSearchConditionList__c);
	}
}