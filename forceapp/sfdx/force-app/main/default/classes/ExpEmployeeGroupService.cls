/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Expense Employee Group Service
 */
public with sharing class ExpEmployeeGroupService {

	private static final Boolean CHECK_BY_ID = true;
	private static final Boolean CHECK_BY_CODE = false;


	/**
	 * Create Expense Employee Group
	 * @param entity Expense Employee Group Entity
	 */
	public static Id createExpenseEmployeeGroup(ExpEmployeeGroupEntity entity, List<String> reportTypeIdList) {

		validateEntity(entity);

		setUniqKey(entity);

		// If target record exists, throw error.
		if (isDuplicateRecord(entity)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		Id employeeGroupId = new ExpEmployeeGroupRepository().saveEntity(entity).details[0].id;

		if(reportTypeIdList!=null && !reportTypeIdList.isEmpty()) {
			List<ExpEmpGroupExpReportTypeLinkEntity> linkEntityList =
					createEmpGroupExpReportTypeLinkEntity(employeeGroupId, reportTypeIdList);
			new ExpEmpGroupExpReportTypeLinkRepository().saveEntityList(linkEntityList);
		}

		return employeeGroupId;
	}

	/*
	 * Create EmpGroupExpReportTypeLinkEntity
	 *
	 * @param employeeGroupId  Expense Employee Group Id
	 * @param reportTypeIdList
	 * @return List of expEmpGroupExpReportTypeLinkEntity
	 */
	public static List<ExpEmpGroupExpReportTypeLinkEntity> createEmpGroupExpReportTypeLinkEntity(Id employeeGroupId, List<String> reportTypeIdList) {
		List<ExpEmpGroupExpReportTypeLinkEntity> entityList = new List<ExpEmpGroupExpReportTypeLinkEntity>();

		Integer count = 0;
		for (Id reportTypeId : reportTypeIdList) {
			ExpEmpGroupExpReportTypeLinkEntity entity = new ExpEmpGroupExpReportTypeLinkEntity();
			entity.expEmpGroupId = employeeGroupId;
			entity.expReportTypeId = reportTypeId;
			entity.order = count++;
			entityList.add(entity);
		}
		return entityList;
	}

	/**
	 * Search Expense Employee Group
	 * @param companyId Company Id
	 * @param expEmployeeGroupId Expense Employee Group Id
	 */
	public static List<ExpEmployeeGroupEntity> searchActiveExpenseEmployeeGroupList(String companyId, String expEmployeeGroupId, Boolean withReportTypeIdList) {
		return searchExpenseEmployeeGroupList(companyId, expEmployeeGroupId, withReportTypeIdList, True);
	}

	/**
	 * Search Expense Emoloyee Group
	 * @param companyId Company Id
	 * @param expEmployeeGroupId Expense Employee Group Id
	 * @param withReportTypeIdList Boolean to retrieve reportTypeIdList as well
	 * @param active Search for active, inactive or all group
	 */

	public static List<ExpEmployeeGroupEntity> searchExpenseEmployeeGroupList(String companyId, String expEmployeeGroupId, Boolean withReportTypeIdList, Boolean active) {

		ExpEmployeeGroupRepository.SearchFilter empGroupFilter = new ExpEmployeeGroupRepository.SearchFilter();
		ExpEmpGroupExpReportTypeLinkRepository.SearchFilter mapFilter = new ExpEmpGroupExpReportTypeLinkRepository.SearchFilter();

		if (String.isNotBlank(companyId)) {
			empGroupFilter.companyIdSet = new Set<Id>{(Id)companyId};
		}
		if (String.isNotBlank(expEmployeeGroupId)) {
			empGroupFilter.idSet = new Set<Id>{(Id)expEmployeeGroupId};
		}
		if (String.isNotBlank(expEmployeeGroupId)) {
			mapFilter.empGroupIdSet = new Set<Id>{(Id)expEmployeeGroupId};
		}

		empGroupFilter.active = active;

		List<ExpEmployeeGroupEntity> entityList = new ExpEmployeeGroupRepository().searchEntity(empGroupFilter);

		if(withReportTypeIdList){
			Map<Id, List<Id>> reportTypeMap = new ExpEmpGroupExpReportTypeLinkRepository().getEntityListMap(mapFilter);
			for (ExpEmployeeGroupEntity entity : entityList) {
				entity.reportTypeIdList = reportTypeMap.get(entity.id);
			}
		}

		return entityList;
	}

	/**
	 * Update Expense Employee Group
	 * @param entity Expense Employee Group Entity
	 */
	public static Id updateExpenseEmployeeGroup(ExpEmployeeGroupEntity entity, List<String> reportTypeIdList) {
		validateEntity(entity);

		setUniqKey(entity);

		// If No Record found to update, throw error
		if (!isExistRecord(entity.id)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_RecordNotFound);
		}

		// If it is code change and the same code exists in the same company, throw error.
		if (isDuplicateRecord(entity)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		Id empGroupId = new ExpEmployeeGroupRepository().saveEntity(entity).details[0].id;

		// search & delete existing entity list
		ExpEmpGroupExpReportTypeLinkRepository repo = new ExpEmpGroupExpReportTypeLinkRepository();
		List<ExpEmpGroupExpReportTypeLinkEntity> entityList = repo.searchEntityByEmpGroupId(entity.id);
		repo.deleteEntityList(entityList);
		createExpenseEmployeeGroup(entity, reportTypeIdList); // create new entity list

		return empGroupId;
	}

	/**
	 * Delete a Expense EmployeeGroupService
	 * @param Id Entity Id to be deleted
	 */
	public static void deleteExpEmployeeGroup(Id id) {

		ExpEmployeeGroupEntity entity = new ExpEmployeeGroupEntity();
		entity.setId(id);

		ExpEmpGroupExpReportTypeLinkRepository repo = new ExpEmpGroupExpReportTypeLinkRepository();
		List<ExpEmpGroupExpReportTypeLinkEntity> linkEntityList = repo.searchEntityByEmpGroupId(id);

		try {
			new ExpEmployeeGroupRepository().deleteEntity(id);
		} catch (Exception e) {
			// Process special Error Handling
			if (e instanceof  DmlException) {
				if (((DmlException)e).getDmlType(0) == System.StatusCode.DELETE_FAILED) {
					throw new App.IllegalStateException(ComMessage.msg().Com_Err_CannotDeleteReference);
				}
			}
			throw e;
		}
	}

	/**
	 * Set unique key to the entity
	 * @param entity Target entity
	 */
	private static void setUniqKey(ExpEmployeeGroupEntity entity) {
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		entity.uniqKey = entity.createUniqKey(company.code);
	}

	/**
	 * Validate entity
	 * @param entity Target entity to be validated
	 */
	private static void validateEntity(ExpEmployeeGroupEntity entity) {
		Validator.Result result = new ExpConfigValidator.ExpEmployeeGroupValidator(entity).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}
	}

	/**
	 * Check if the given id expense employee group record exists
	 * @param entity Target entity
	 * @return Retrun true if record exists
	 */
	private static boolean isExistRecord(Id entityId) {
		ExpEmployeeGroupRepository.SearchFilter filter = new ExpEmployeeGroupRepository.SearchFilter();
		List<ExpEmployeeGroupEntity> entityList;

		filter.idSet = new Set<Id>{entityId};
		entityList = new ExpEmployeeGroupRepository().searchEntity(filter);
		return (!entityList.isEmpty());
	}

	/**
	 * Check if the target record is duplicate by checking the code exists within the same company
	 * @param entity Expense Employee Group entity
	 * @return Return true if it is duplicate
	 */
	private static boolean isDuplicateRecord(ExpEmployeeGroupEntity entity) {
		ExpEmployeeGroupRepository.SearchFilter filter = new ExpEmployeeGroupRepository.SearchFilter();
		List<ExpEmployeeGroupEntity> entityList;

		filter.codeSet = new Set<String>{entity.code};
		filter.companyIdSet = new Set<Id>{entity.companyId};

		entityList = new ExpEmployeeGroupRepository().searchEntity(filter, false, 1);
		return (!entityList.isEmpty() && (entityList[0].id != entity.id));
	}
}