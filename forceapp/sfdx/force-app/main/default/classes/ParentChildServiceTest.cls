/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 履歴管理方式が親子型マスタのサービスクラスのテスト
 */
@isTest
private class ParentChildServiceTest {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
	private static ComOrgSetting__c orgObj;
	/** 会社オブジェクト */
	private static ComCompany__c companyObj;
	/** 権限オブジェクト */
	private static ComPermission__c permissionObj;
	/** 親部署 */
	private static ComDeptBase__c parentDeptObj;
	/** 上長 */
	private static ComEmpBase__c managerObj;

	/** 基本的なマスタデータを作成する */
	private static void createBaseMaster() {
		if (orgObj == null) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		}
		if (companyObj == null) {
			companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
		}
		if (permissionObj == null) {
			permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		}
		if (parentDeptObj == null) {
			parentDeptObj = ComTestDataUtility.createDepartmentWithHistory('親部署', companyObj.Id);

		}
		if (managerObj == null) {
			managerObj = ComTestDataUtility.createEmployeeWithHistory('Test社員', companyObj.Id, null, Userinfo.getUserId(), permissionObj.Id);
		}
	}

	/** テスト用の部署を作成する */
	private static List<DepartmentBaseEntity> createDeptBaseEntityList(String name, Integer baseSize, Integer historySize) {
		createBaseMaster();

		List<ComDeptBase__c> deptObjList =  ComTestDataUtility.createDepartmentsWithHistory(
				name, companyObj.Id, baseSize, historySize);

		// 履歴に親部署を設定
		List<ComDeptHistory__c> historyList = [SELECT Id, ParentBaseId__c FROM ComDeptHistory__c];
		for (ComDeptHistory__c history : historyList) {
			history.ParentBaseId__c = parentDeptObj.Id;
		}
		update historyList;

		List<Id> baseIdList = new List<Id>();
		for (ComDeptBase__c dept : deptObjList) {
			baseIdList.add(dept.Id);
		}

		return (new DepartmentRepository()).getEntityList(baseIdList, null);
	}

	/**
	 * テスト用の部署エンティティを作成する
	 */
	private static DepartmentBaseEntity createDeptEntity(String name, String code) {
		createBaseMaster();

		DepartmentBaseEntity baseEntity = new DepartmentBaseEntity();
		baseEntity.name = name;
		baseEntity.currentHistoryId = parentDeptObj.CurrentHistoryId__c;	// とりあえず適当に設定しておく
		baseEntity.code = code;
		baseEntity.uniqKey = baseEntity.createUniqKey(companyObj.Code__c, code);
		baseEntity.companyId = companyObj.Id;
		baseEntity.addHistory(createDeptHistoryEntity(name + '1', code, AppDate.newInstance(2017, 12, 15), AppDate.newInstance(2018, 4, 1)));
		baseEntity.addHistory(createDeptHistoryEntity(name + '2', code, AppDate.newInstance(2018, 4, 1), AppDate.newInstance(2019, 1, 1)));

		return baseEntity;
	}

	/**
	 * テスト用の部署エンティティを作成する
	 */
	private static DepartmentHistoryEntity createDeptHistoryEntity(String name, String code, AppDate validFrom, AppDate validTo) {
		DepartmentHistoryEntity historyEntity = new DepartmentHistoryEntity();
		historyEntity.name = name;
		historyEntity.nameL0 = name + '_L0';
		historyEntity.nameL1 = name + '_L1';
		historyEntity.nameL2 = name + '_L2';
		historyEntity.historyComment = '改訂コメント';
		historyEntity.validFrom = validFrom;
		historyEntity.validTo = validTo;
		historyEntity.isRemoved = false;
		historyEntity.managerId = managerObj.Id;
		historyEntity.parentBaseId = parentDeptObj.Id;
		historyEntity.uniqKey = historyEntity.createUniqKey(code);

		return historyEntity;
	}


	/**
	 * ベース＋履歴が新規登録できることを確認する
	 */
	@isTest static void saveNewEntityTest() {
		DepartmentBaseEntity targetBase = createDeptEntity('Test', 'Test001');

		AppDate validFrom = AppDate.today(); // 今日の日付で登録
		AppDate validTo = validFrom.addMonths(12);
		targetBase.getHistoryList()[0].validFrom = validFrom;
		targetBase.getHistoryList()[0].validTo = validTo;
		targetBase.getHistoryList()[1].validFrom = validTo;
		targetBase.getHistoryList()[1].validTo = validTo.addMonths(12);

		Test.startTest();
			DepartmentService service = new DepartmentService();
			service.saveNewEntity(targetBase);
		Test.stopTest();

		// ベースが保存されている
		String targetCode = targetBase.code;
		List<ComDeptBase__c> baseObjList = [SELECT Id, Code__c, CurrentHistoryId__c FROM ComDeptBase__c WHERE Code__c = :targetCode];
		System.assertEquals(1, baseObjList.size());

		// 履歴が保存されている
		List<ComDeptHistory__c> historyObjList = [
				SELECT Id, UniqKey__c, ValidFrom__c, ValidTo__c FROM ComDeptHistory__c WHERE BaseId__c = :baseObjList[0].Id];
		System.assertEquals(2, historyObjList.size());
		System.assertEquals(validFrom.getDate(), historyObjList[0].ValidFrom__c);
		System.assertEquals(validTo.getDate(), historyObjList[0].ValidTo__c);
		System.assertEquals(false, String.isEmpty(historyObjList[0].UniqKey__c));

		// CurrentHistoryIDが更新されている
		System.assertEquals(historyObjList[0].Id, baseObjList[0].CurrentHistoryId__c);

	}

	/**
	 * ベース＋履歴が新規登録のテスト
	 * コードが既存のマスタと重複している場合、エラー処理が正しく行われることを確認する
	 */
	@isTest static void saveNewEntityTestDuplicateCode() {
		final Integer baseSize = 1;
		final Integer historySize = 2;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);

		DepartmentBaseEntity targetBase = createDeptEntity('Test', baseList[0].code);
		AppDate validFrom = AppDate.today(); // 今日の日付で登録
		AppDate validTo = validFrom.addDays(10);
		targetBase.getHistoryList()[0].validFrom = validFrom;
		targetBase.getHistoryList()[0].validTo = validTo;

		DepartmentService service = new DepartmentService();
		try {
			service.saveNewEntity(targetBase);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assertEquals(MESSAGE.Com_Err_DuplicateCode, e.getMessage());
		}

	}

	/**
	 * ベース＋履歴が新規登録のテスト
	 * ベースエンティティのバリデーションが実行されていることを確認する
	 */
	@isTest static void saveNewEntityTestInvalidBase() {
		// コードを空欄にする
		DepartmentBaseEntity targetBase = createDeptEntity('Test', 'Test001');
		targetBase.code = '';
		AppDate validFrom = AppDate.today(); // 今日の日付で登録
		AppDate validTo = validFrom.addDays(10);
		targetBase.getHistoryList()[0].validFrom = validFrom;
		targetBase.getHistoryList()[0].validTo = validTo;

		DepartmentService service = new DepartmentService();
		try {
			service.saveNewEntity(targetBase);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// OK
		}

	}

	/**
	 * ベース＋履歴が新規登録のテスト
	 * 履歴のバリデーションが実行されていることを確認する
	 */
	@isTest static void saveNewEntityTestInvalidHistory() {
		DepartmentBaseEntity targetBase = createDeptEntity('Test', 'Code001');
		AppDate validFrom = AppDate.today(); // 今日の日付で登録
		AppDate validTo = ParentChildHistoryEntity.VALID_TO_MAX.addDays(10);
		targetBase.getHistory(1).validFrom = validFrom;
		targetBase.getHistory(1).validTo = validTo;

		DepartmentService service = new DepartmentService();
		try {
			service.saveNewEntity(targetBase);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// OK
			System.debug(e.getMessage());
			System.assertEquals(App.ERR_CODE_INVALID_VALUE, e.getErrorCode());
		}
	}

	/**
	 * ベースエンティティが更新できることを確認する
	 */
	@isTest static void updateBaseTest() {
		DepartmentBaseEntity base = createDeptBaseEntityList('Test', 1, 1).get(0);
		base.code = 'updateTest';
		base.uniqKey = base.createUniqKey(companyObj.Code__c, base.code);

		new DepartmentService().updateBase(base);

		// ベースが更新されていることを確認する
		DepartmentBaseEntity resBase = new DepartmentRepository().getEntity(base.id);
		System.assertEquals(base.code, resBase.code);

		// 履歴のユニークキーが更新されていることを確認する
		System.assert(resBase.getHistoryList().get(0).uniqKey.startsWith(base.createUniqKey(companyObj.Code__c, resBase.code)),
				'uniqKeyが正しくありませんでした。uniqKey=' + resBase.getHistoryList().get(0).uniqKey);
	}

	/**
	 * ベースエンティティ更新時に更新対象のエンティティが存在しない場合、
	 * 想定した例外が発生することを確認する
	 */
	@isTest static void updateBaseTestRecordNotFound() {
		DepartmentBaseEntity base = createDeptBaseEntityList('Test', 1, 1).get(0);
		new DepartmentRepository().deleteEntity(base);
		base.code = 'updateTest';

		try {
			new DepartmentService().updateBase(base);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.RecordNotFoundException e) {
			// OK
			System.assertEquals(MESSAGE.Com_Err_DeleteRecordNotFound, e.getMessage());
		}
	}

	/**
	 * ベースエンティティ更新時に更新後のコードが既存のレコードのコードと重複している場合
	 * 想定した例外が発生することを確認する
	 */
	@isTest static void updateBaseTestCodeDuplicate() {
		final Integer baseNum = 2;
		List<DepartmentBaseEntity> bases = createDeptBaseEntityList('Test', baseNum, 1);
		DepartmentBaseEntity base = bases[0];
		DepartmentBaseEntity base2 = bases[1];
		base.code = base2.code;
		base.uniqKey = base2.uniqKey;

		try {
			new DepartmentService().updateBase(base);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// OK
			System.assertEquals(MESSAGE.Com_Err_DuplicateCode, e.getMessage());
		}
	}

	/**
	 * ベースエンティティ更新時にバリデーションが実行されていることを確認する
	 * 項目に不正な値が設定されている場合、例外が発生することを確認する
	 */
	@isTest static void updateBaseTestValidation() {
		DepartmentBaseEntity base = createDeptBaseEntityList('Test', 1, 1).get(0);
		base.code = '0123456789012345678901'; // 最大文字数より長い21文字

		try {
			new DepartmentService().updateBase(base);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// OK
			System.assertEquals(App.ERR_CODE_INVALID_VALUE, e.getErrorCode());
		}
	}

	/**
	 * マスタ全体の失効日が更新できることを確認する
	 */
	@isTest static void updateValidToTest() {
		DepartmentRepository repo = new DepartmentRepository();

		final Integer baseSize = 1;
		final Integer historySize = 3;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];
		AppDate validFrom = AppDate.newInstance(2017, 12, 1);
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(10);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		DepartmentService service = new DepartmentService();
		AppDate updateValidTo;

		// 失効日が最新履歴の有効開始日より後の場合
		// 失効日が更新できる
		updateValidTo = AppDate.today().addDays(1);
		service.updateValidTo(base.id, updateValidTo);
		DepartmentBaseEntity retBase = repo.getEntity(base.id);
		DepartmentHistoryEntity retHistory = retBase.getHistoryList().get(historySize - 1);
		// 有効開始日は変更されていない
		System.assertEquals(base.getHistoryList().get(historySize - 1).validFrom, retBase.getHistoryList().get(historySize - 1).validFrom);
		// 失効日は更新されている
		System.assertEquals(updateValidTo, retBase.getHistoryList().get(historySize - 1).validTo);

		// 失効日が最新履歴の有効開始日の場合
		// 有効開始日=失効日にはできないのでアプリ例外が発生する
		updateValidTo = base.getHistoryList().get(historySize - 1).validFrom;
		try {
			service.updateValidTo(base.id, updateValidTo);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// OK
			System.debug(e.getMessage());
		}

		// 失効日が最新履歴の有効開始日より前の場合
		// アプリ例外が発生する
		updateValidTo = base.getHistoryList().get(historySize - 1).validFrom.addDays(-1);
		try {
			service.updateValidTo(base.id, updateValidTo);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// OK
			System.debug(e.getMessage());
		}

	}


	/**
	 * 履歴が改定できることを確認する(同日改定)
	 * 改定日が既存の履歴の有効開始日と重複していても正しく改定できることを確認する
	 */
	@isTest static void reviseHistoryTestSameDay() {
		DepartmentRepository repo = new DepartmentRepository();

		final Integer historyNum = 5;
		DepartmentBaseEntity base = createDeptBaseEntityList('Test', 1, historyNum).get(0);

		// 部署の管理者を変更する
		ComEmpBase__c managerObj1 = ComTestDataUtility.createEmployeeWithHistory('管理者1', companyObj.Id, null, Userinfo.getUserId(), permissionObj.Id);
		ComEmpBase__c managerObj2 = ComTestDataUtility.createEmployeeWithHistory('管理者2', companyObj.Id, null, Userinfo.getUserId(), permissionObj.Id);

		AppDate validFrom = base.getHistoryList().get(1).validFrom;
		AppDate validTo = validFrom.addDays(10);
		DepartmentHistoryEntity targetHistory = createDeptHistoryEntity('改定テスト', base.code, validFrom, validTo);
		targetHistory.baseId = base.id;
		targetHistory.remarks = '改定後の備考'; // 備考は各履歴で値が異なるので伝搬されないはず
		targetHistory.managerId = managerObj1.Id;

		// 改定実行
		DepartmentService service = new DepartmentService();
		service.reviseHistory(targetHistory);

		// 同日改定の場合は改定前履歴が削除されているので、改定前と履歴の件数は変わらない
		List<DepartmentHistoryEntity> resHistoryList = repo.getHistoryEntityByBaseId(base.id);
		System.assertEquals(historyNum, resHistoryList.size());
		// 改定されていることを確認
		System.assertNotEquals(base.getHistoryList().get(1).id, resHistoryList[1].id); // 改訂後履歴になっているはず
		System.assertEquals(validFrom, resHistoryList[1].validFrom);
		System.assertEquals(base.getHistoryList().get(1).validTo, resHistoryList[1].validTo);

		// デバッグ
		System.debug('>>> 改定後の履歴');
		for (DepartmentHistoryEntity history : resHistoryList) {
			System.debug(history.name + '(' + history.validFrom + '-' + history.validTo + '): managerId = ' + history.managerId + ', remarks =' + history.remarks);
		}

		// 伝搬されていることを確認
		for (Integer i = 3; i < historyNum; i++) {
			System.assertEquals(targetHistory.nameL0, resHistoryList[i].nameL0);
			System.assertEquals(targetHistory.nameL1, resHistoryList[i].nameL1);
			System.assertEquals(targetHistory.nameL2, resHistoryList[i].nameL2);
			System.assertEquals(targetHistory.managerId, resHistoryList[i].managerId);

			// 備考は各履歴で値が異なるので伝搬されないはず
			System.assertEquals(base.getHistoryList().get(i).remarks, resHistoryList[i].remarks);
		}

		// 改定前の履歴には伝搬されていない
		System.assertEquals(base.getHistoryList().get(0).managerId, resHistoryList[0].managerId);

		// 伝搬対象の履歴の元の履歴と、改定前履歴は論理削除されている
		List<ComDeptHistory__c> removedList = [SELECT Id, ValidFrom__c FROM ComDeptHistory__c WHERE BaseId__c = :base.Id AND Removed__c = true];
		System.assertEquals(historyNum - 1, removedList.size());

	}


	/**
	 * 履歴が改定できることを確認する(履歴リストの末尾に追加)
	 */
	@isTest static void reviseHistoryTestTail() {
		DepartmentRepository repo = new DepartmentRepository();
		// DepartmentServiceTest.TestData testData = new TestData();

		DepartmentBaseEntity base = createDeptBaseEntityList('Test', 1, 1).get(0);
		base.getHistoryList().get(0).validFrom = AppDate.today().addDays(-10);
		base.getHistoryList().get(0).validTo = AppDate.today().addDays(5);
		repo.saveHistoryEntity(base.getHistoryList().get(0));
		AppDate allValidFrom = base.getHistoryList().get(0).validFrom;
		AppDate allValidTo = base.getHistoryList().get(0).validTo;

		AppDate validFrom = AppDate.today(); // 今日の日付で改定
		AppDate validTo = validFrom.addDays(10);
		DepartmentHistoryEntity targetHistory = createDeptHistoryEntity('改定テスト', base.code, validFrom, validTo);
		targetHistory.baseId = base.id;

		DepartmentService service = new DepartmentService();
		service.reviseHistory(targetHistory);

		List<DepartmentHistoryEntity> historyList = (new DepartmentRepository()).getHistoryEntityByBaseId(base.id);
		System.assertEquals(2, historyList.size());
		// 履歴リストの最後に追加されているはず
		System.assertEquals(targetHistory.nameL0, historyList[1].nameL0);
		// 前の履歴の失効日が改定日になっている
		System.assertEquals(validFrom, historyList[0].validTo);
		// 改定履歴の失効日は前の履歴の失効日になっている(改定でマスタ全体の失効日は変更されない)
		System.assertEquals(allValidTo, historyList[1].validTo);
		// CurrentHistoryIDが更新されている
		base = repo.getEntity(base.id);
		System.assertEquals(historyList[1].id, base.currentHistoryId);

	}

	/**
	 * 履歴が改定できることを確認する
	 * 改定対象のベースレコードが見つからない場合、正しくエラー処理が実行されることを確認する
	 */
	@isTest static void reviseHistoryTestNotFoundBaseRecord() {
		DepartmentRepository repo = new DepartmentRepository();

		DepartmentBaseEntity base = createDeptBaseEntityList('Test', 1, 1).get(0);
		base.getHistoryList().get(0).validFrom = AppDate.today().addDays(-10);
		base.getHistoryList().get(0).validTo = AppDate.today().addDays(5);
		repo.saveHistoryEntity(base.getHistoryList().get(0));
		AppDate allValidFrom = base.getHistoryList().get(0).validFrom;
		AppDate allValidTo = base.getHistoryList().get(0).validTo;

		AppDate validFrom = AppDate.today(); // 今日の日付で改定
		AppDate validTo = validFrom.addDays(10);
		DepartmentHistoryEntity targetHistory = createDeptHistoryEntity('改定テスト', base.code, validFrom, validTo);
		targetHistory.baseId = UserInfo.getUserId(); // 存在しないベースID

		DepartmentService service = new DepartmentService();
		try {
			service.reviseHistory(targetHistory);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.RecordNotFoundException e) {
			System.assertEquals(MESSAGE.Com_Err_DeleteRecordNotFound, e.getMessage());
		}
	}

	/**
	 * 履歴が改定できることを確認する
	 * 改定日をマスタ全体の有効期間外に設定した場合、正しくエラー処理が実行されることを確認する
	 */
	@isTest static void reviseHistoryTestOutOfValidTerm() {
		DepartmentRepository repo = new DepartmentRepository();

		DepartmentBaseEntity base = createDeptBaseEntityList('Test', 1, 1).get(0);
		base.getHistoryList().get(0).validFrom = AppDate.today().addDays(-10);
		base.getHistoryList().get(0).validTo = AppDate.today().addDays(5);
		repo.saveHistoryEntity(base.getHistoryList().get(0));
		AppDate allValidFrom = base.getHistoryList().get(0).validFrom;
		AppDate allValidTo = base.getHistoryList().get(0).validTo;

		AppDate validFrom = base.getHistoryList().get(0).validTo;  // マスタ全体の失効日
		AppDate validTo = validFrom.addDays(10);
		DepartmentHistoryEntity targetHistory = createDeptHistoryEntity('改定テスト', base.code, validFrom, validTo);
		targetHistory.baseId = base.id;

		DepartmentService service = new DepartmentService();
		try {
			service.reviseHistory(targetHistory);
			TestUtil.fail('例外が発生しませんでした。');
		} catch (App.RecordNotFoundException e) {
			System.assertEquals(MESSAGE.Com_Err_HistoryNotFound, e.getMessage());
		}
	}

	/**
	 * 履歴削除のテスト
	 * 履歴が論理削除され、前バージョンの失効日が更新されることを確認する
	 */
	@isTest static void deleteHistoryListWithPropagationTest() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 2;
		final Integer historySize = 2;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];
		DepartmentHistoryEntity targetHistory1 = baseList[0].getHistory(historySize - 1);
		DepartmentHistoryEntity targetHistory2 = baseList[1].getHistory(historySize - 1);

		// 履歴削除を実行する
		DepartmentService service = new DepartmentService();
		service.deleteHistoryListWithPropagation(new List<Id>{targetHistory1.id, targetHistory2.id});

		// 論理削除されていることを確認する
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.includeRemoved = true;
		filter.baseIds = new List<Id>{baseList[0].id, baseList[1].id};
		List<DepartmentBaseEntity> resultBases = repo.searchEntity(filter);
		System.assertEquals(2, resultBases.size());
		for (DepartmentBaseEntity resBase : resultBases) {
			DepartmentHistoryEntity removedHistory = resBase.getHistory(1);
			DepartmentHistoryEntity prevHistory = resBase.getHistory(0);
			System.assertEquals(true, removedHistory.isRemoved);
			System.assertEquals(removedHistory.id, removedHistory.uniqKey);
			// 削除した履歴の失効日が前バージョンの履歴の失効日に更新されている
			System.assertEquals(removedHistory.validTo, prevHistory.validTo);
		}
	}

	/**
	 * 履歴削除のテスト
	 * 指定した履歴が見つからない場合、正しくエラー処理が実行されることを確認する
	 */
	@isTest static void deleteHistoryListWithPropagationTestNotFoundRecord() {
		DepartmentRepository repo = new DepartmentRepository();

		final Integer baseSize = 1;
		final Integer historySize = 1;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];
		Id targetId = UserInfo.getUserId();

		DepartmentService service = new DepartmentService();
		try {
			service.deleteHistoryListWithPropagation(new List<Id>{targetId});

			// 例外が発生しなかった
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.RecordNotFoundException e) {
			System.assertEquals(MESSAGE.Com_Err_DeleteRecordNotFound, e.getMessage());
		}
	}

	/**
	 * 履歴削除のテスト
	 * 履歴が1件の場合に削除できないことを確認する
	 */
	@isTest static void deleteHistoryListWithPropagationTestOneHistory() {
		DepartmentRepository repo = new DepartmentRepository();

		final Integer baseSize = 1;
		final Integer historySize = 1;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];
		Id targetId = base.getHistoryList().get(0).id;

		DepartmentService service = new DepartmentService();
		try {
			service.deleteHistoryListWithPropagation(new List<Id>{targetId});

			// 例外が発生しなかった
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.IllegalStateException e) {
			// 削除されていないことを確認する
			List<ComDeptHistory__c> histories = [SELECT Id FROM ComDeptHistory__c WHERE Id = :targetId];
			System.assertEquals(1, histories.size());
			System.assertEquals(MESSAGE.Com_Err_CannotDeletedNotExistHistory, e.getMessage());
		}
	}


	/**
	 * ベースが物理削除できることを確認する
	 */
	@isTest static void deleteEntityTest() {
		DepartmentRepository repo = new DepartmentRepository();

		final Integer baseSize = 2;
		final Integer historySize = 2;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];
		Id targetId = base.id;

		DepartmentService service = new DepartmentService();
		service.deleteEntity(base.id);

		// 1.削除されていることを確認する
		List<ComDeptBase__c> histories = [SELECT Id FROM ComDeptBase__c WHERE Id = :targetId];
		System.assertEquals(0, histories.size());

		// 2.既に削除済みのベースエンティティを削除した場合はDML例外が発生する。
		// エラーコードは System.StatusCode.ENTITY_IS_DELETED
		try {
			service.deleteEntity(base.id);
		} catch (DmlException e) {
			System.assertEquals(System.StatusCode.ENTITY_IS_DELETED, e.getDmlType(0));
		}

		// 3.トランザクションデータから参照されている場合はDML例外が発生する
		DepartmentBaseEntity base3 = baseList[1];
		AttDailyRequestEntity attReq = new AttDailyRequestEntity();
		attReq.requestType = AttRequestType.LEAVE;
		attReq.departmentId = base3.getHistory(0).id;
		new AttDailyRequestRepository().saveEntity(attReq);
		// AttRequestEntity attReq = new AttRequestEntity();
		// attReq.departmentId = base3.getHistory(0).id;
		// (new AttRequestRepository()).saveEntity(attReq);
		try {
			service.deleteEntity(base3.id);
		} catch (DmlException e) {
			System.assertEquals(MESSAGE.Com_Err_FaildDeleteReference, e.getMessage());
		}

	}

	/**
	 * updateCurrentHistoryのテスト
	 * テスト実行日がマスタの有効期間に含まれている場合、
	 * currentHistoryIdが該当する履歴IDに更新されることを確認する
	 */
	@isTest static void updateCurrentHistoryTest() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(-10);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 現在の履歴IDを更新する
		DepartmentService service = new DepartmentService();
		service.updateCurrentHistory(base.id);
		DepartmentBaseEntity resBase = repo.getEntity(base.id);
		System.assertEquals(base.getHistoryList().get(1).id, resBase.currentHistoryId);
	}

	/**
	 * updateCurrentHistoryのテスト
	 * テスト実行日がマスタ全体の有効開始日より前の場合、
	 * currentHistoryIdがもっとも古い履歴に設定されあることを確認する
	 */
	@isTest static void updateCurrentHistoryTestBeforeValidPeriod() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(1);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 現在の履歴IDを更新する
		DepartmentService service = new DepartmentService();
		service.updateCurrentHistory(base.id);
		DepartmentBaseEntity resBase = repo.getEntity(base.id);
		System.assertEquals(base.getHistoryList().get(0).id, resBase.currentHistoryId);
	}

	/**
	 * updateCurrentHistoryのテスト
	 * テスト実行日がマスタ全体の失効日より後の場合、
	 * currentHistoryIdがもっとも古い履歴に設定されあることを確認する
	 */
	@isTest static void updateCurrentHistoryTestAfterValidPeriod() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(-50);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 現在の履歴IDを更新する
		DepartmentService service = new DepartmentService();
		service.updateCurrentHistory(base.id);
		DepartmentBaseEntity resBase = repo.getEntity(base.id);
		System.assertEquals(base.getHistoryList().get(historySize - 1).id, resBase.currentHistoryId);
	}

	/**
	 * updateBaseValidToDateのテスト
	 * 失効日が最新の履歴の有効期間内の場合(有効期間短縮)
	 * 最新の履歴の失効日が更新されることを確認する
	 */
	@isTest static void updateBaseValidToDateTestEndDateLatestHistory() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(1);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 有効期間終了日を更新する
		AppDate validToDate = base.getHistory(historySize - 1).validTo.addDays(-6);
		new DepartmentService().updateBaseValidToDate(base.id, validToDate);

		// 履歴が更新されていることを確認
		DepartmentBaseEntity resBase = repo.getEntity(base.id);
		DepartmentHistoryEntity resHistory = resBase.getHistory(historySize - 1);
		System.assertEquals(validToDate, resHistory.validTo);
		System.assertEquals(base.getHistory(historySize - 1).validFrom, resHistory.validFrom);
		System.assertEquals(false, resHistory.isRemoved);

	}

	/**
	 * updateBaseValidToDateのテスト
	 * 失効日がもっとも古いの履歴の有効期間内の場合(有効期間短縮)
	 * ・履歴の失効日が更新されることを確認する
	 * ・最も古い履歴以外の履歴は論理削除されていることを確認する
	 */
	@isTest static void updateBaseValidToDateTestEndDateOldestHistory() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(1);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 有効期間終了日を更新する
		AppDate validToDate = base.getHistory(0).validTo.addDays(-5);
		new DepartmentService().updateBaseValidToDate(base.id, validToDate);

		// 履歴が更新されていることを確認
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.id};
		filter.includeRemoved = true;
		DepartmentBaseEntity resBase = repo.searchEntity(filter).get(0);
		DepartmentHistoryEntity resHistory = resBase.getHistory(0);
		System.assertEquals(validToDate, resHistory.validTo);
		System.assertEquals(base.getHistory(0).validFrom, resHistory.validFrom);
		System.assertEquals(false, resHistory.isRemoved);

		// 有効期間終了日以降の履歴は論理削除されていることを確認
		System.assertEquals(historySize, resBase.getHistoryList().size());
		System.assertEquals(true, resBase.getHistory(1).isRemoved);
		System.assertEquals(true, resBase.getHistory(2).isRemoved);
		System.assertEquals(true, resBase.getHistory(3).isRemoved);
	}

	/**
	 * updateBaseValidToDateのテスト
	 * 失効日が最新履歴の失効日以降である場合(マスタ全体の有効期間を延ばす場合)
	 * ・最新履歴の失効日が更新されることを確認する
	 */
	@isTest static void updateBaseValidToDateTestExtending() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(1);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 失効日を更新する
		AppDate validToDate = base.getHistory(historySize - 1).validTo.addDays(11);
		new DepartmentService().updateBaseValidToDate(base.id, validToDate);

		// 履歴が更新されていることを確認
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.id};
		filter.includeRemoved = true;
		DepartmentBaseEntity resBase = repo.searchEntity(filter).get(0);
		DepartmentHistoryEntity resHistory = resBase.getHistory(historySize - 1);
		System.assertEquals(validToDate, resHistory.validTo);
		System.assertEquals(base.getHistory(historySize - 1).validFrom, resHistory.validFrom);

		// すべての履歴が論理削除されていないことを確認
		System.assertEquals(historySize, resBase.getHistoryList().size());
		for (DepartmentHistoryEntity history : resBase.getHistoryList()) {
			System.assertEquals(false, history.isRemoved);
		}
	}

	/**
	 * updateBaseValidToDateのテスト
	 * 失効日が履歴の失効日と同じ場合
	 * ・失効日以降の履歴が論理削除されていることを確認する
	 */
	@isTest static void updateBaseValidToDateTestValidToEqualHistoryValidTo() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(1);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);
			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 失効日を更新する
		// 最新履歴の1個前の履歴の失効日をマスタ全体の失効日とする
		AppDate validToDate = base.getHistory(historySize - 2).validTo;
		new DepartmentService().updateBaseValidToDate(base.id, validToDate);

		// 履歴が更新されていることを確認
		DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
		filter.baseIds = new List<Id>{base.id};
		filter.includeRemoved = true;
		DepartmentBaseEntity resBase = repo.searchEntity(filter).get(0);
		// 失効日より前の履歴は論理削除されていないこと、有効期間が変更されていないことを確認
		System.assertEquals(false, resBase.getHistory(0).isRemoved);
		System.assertEquals(base.getHistory(0).validFrom, resBase.getHistory(0).validFrom);
		System.assertEquals(base.getHistory(0).validTo, resBase.getHistory(0).validTo);
		System.assertEquals(false, resBase.getHistory(1).isRemoved);
		System.assertEquals(base.getHistory(1).validFrom, resBase.getHistory(1).validFrom);
		System.assertEquals(base.getHistory(1).validTo, resBase.getHistory(1).validTo);
		System.assertEquals(false, resBase.getHistory(2).isRemoved);
		System.assertEquals(base.getHistory(2).validFrom, resBase.getHistory(2).validFrom);
		System.assertEquals(base.getHistory(2).validTo, resBase.getHistory(2).validTo);
		// 失効日以降の履歴は論理削除されていることを確認
		System.assertEquals(true, resBase.getHistory(3).isRemoved);
	}

	/**
	 * updateBaseValidToDate のテスト
	 * currentHistoryIdが更新されることを確認する
	 */
	@isTest static void updateBaseValidToDateTestCurrentHistoryId() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(-20);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		// 有効期間終了日を更新する
		AppDate validToDate = base.getHistory(0).validTo.addDays(-6);
		new DepartmentService().updateBaseValidToDate(base.id, validToDate);

		// currentHistoryIdが更新されていることを確認
		// 今日時点で有効な履歴が存在しないためもっとも近い履歴が設定されているはず
		DepartmentBaseEntity resBase = repo.getEntity(base.id);
		System.assertEquals(base.getHistory(0).id, resBase.currentHistoryId);
	}

	/**
	 * updateBaseValidToDate のテスト
	 * validToDateが正しくない場合、想定されている例外が発生することを確認する
	 */
	@isTest static void updateBaseValidToDateTestInvalidValidToDate() {
		DepartmentRepository repo = new DepartmentRepository();

		// テストデータ作成
		final Integer baseSize = 1;
		final Integer historySize = 4;
		List<DepartmentBaseEntity> baseList = createDeptBaseEntityList('Test', baseSize, historySize);
		DepartmentBaseEntity base = baseList[0];

		// 有効開始日と失効日を設定する
		AppDate validFrom = AppDate.today().addDays(-20);
		Integer validDays = 10;
		for (DepartmentHistoryEntity history : base.getHistoryList()) {
			history.validFrom = validFrom;
			history.validTo = validFrom.addDays(validDays);

			validFrom = history.validTo;
		}
		repo.saveHistoryEntityList(base.getHistoryList());

		App.ParameterException resEx;

		// Test1 失効日を有効期間開始日に設定する(有効期間が0日になるためエラー)
		try {
			resEx = null;
			AppDate validToDate = base.getHistory(0).validFrom;
			new DepartmentService().updateBaseValidToDate(base.id, validToDate);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		System.assertNotEquals(null, resEx);
		String expMsg = ComMessage.msg().Admin_Err_InvalidValidEndDateAfterStart(new List<String>{base.getValidEndDateLabel()});
		System.assertEquals(expMsg, resEx.getMessage());

		// Test2 失効日を最大失効日の翌日に設定する
		try {
			resEx = null;
			AppDate validToDate = ParentChildHistoryEntity.VALID_TO_MAX.addDays(1);
			new DepartmentService().updateBaseValidToDate(base.id, validToDate);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		System.assertNotEquals(null, resEx);
		expMsg = ComMessage.msg().Admin_Err_InvalidValidEndDateMax(
						new List<String>{base.getValidEndDateLabel(),
						ParentChildHistoryEntity.VALID_TO_MAX.getDate().format()});
		System.assertEquals(expMsg, resEx.getMessage());

		// Test3 失効日を有効期間開始日の翌日に設定する → OK
		try {
			resEx = null;
			AppDate validToDate = base.getHistory(0).validFrom.addDays(1);
			new DepartmentService().updateBaseValidToDate(base.id, validToDate);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		System.assertEquals(null, resEx);

		// Test4 失効日を最大失効日に設定する → OK
		try {
			resEx = null;
			AppDate validToDate = ParentChildHistoryEntity.VALID_TO_MAX;
			new DepartmentService().updateBaseValidToDate(base.id, validToDate);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		System.assertEquals(null, resEx);
	}
}
