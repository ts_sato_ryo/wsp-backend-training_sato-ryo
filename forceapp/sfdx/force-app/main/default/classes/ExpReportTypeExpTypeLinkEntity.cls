/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description 経費申請タイプ別費目のエンティティ Entity class for ExpReportTypeExpTypeLink
 */
public with sharing class ExpReportTypeExpTypeLinkEntity extends Entity {

	/** 項目の定義(変更管理で使用する) Field definition */
	public enum Field {
		EXP_REPORT_TYPE_ID,
		EXP_TYPE_ID
	}

	/** 値を変更した項目のリスト Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ Constructor
	 */
	public ExpReportTypeExpTypeLinkEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** 経費申請タイプ別費目名(参照専用) ExpReportTypeExpTypeLink name (reference only) */
	public String name {
		get;
		set;
	}

	/** 経費申請タイプID Expense Report Type Id */
	public Id expReportTypeId {
		get;
		set {
			expReportTypeId = value;
			setChanged(Field.EXP_REPORT_TYPE_ID);
		}
	}

	/** 経費申請タイプ名(翻訳)(参照専用) ExpReportType Name (reference only) */
	public AppMultiString expReportTypeNameL {
		get;
		set;
	}

	/** 費目ID Expense Type Id */
	public Id expTypeId {
		get;
		set {
			expTypeId = value;
			setChanged(Field.EXP_TYPE_ID);
		}
	}

	/** 費目名(翻訳)(参照専用) ExpType Name (reference only) */
	public AppMultiString expTypeNameL {
		get;
		set;
	}

	/** 費目名(翻訳)(参照専用) ExpType Code (reference only) */
	public String expTypeCode {
		get;
		set;
	}

	/** 費目明細タイプ(参照専用) ExpType Record Type (reference only) */
	public ExpRecordType expTypeRecordType {
		get;
		set;
	}

	/** 費目有効開始日(参照専用) ExpType Valid From (reference only) */
	public AppDate expTypeValidFrom {
		get;
		set;
	}
	
	/** 費目失効日(参照専用) ExpType Valid To (reference only) */
	public AppDate expTypeValidTo {
		get;
		set;
	}

	/**
	 * 項目の値を変更済みに設定する Mark field as changed
	 * @param Field 変更した項目 Field Changed field
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する Check if the value of the field has been changed
	 * @param field 取得対象の項目 Target field
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse True if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする Reset the changed status
	 */
	public virtual void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}
