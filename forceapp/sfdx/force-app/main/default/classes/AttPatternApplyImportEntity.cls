/**
 * @description 勤務パターン適用インポートのエンティティ
 */
public with sharing class AttPatternApplyImportEntity extends AttPatternApplyImportGeneratedEntity {

	/** @description 社員コードの最大文字数 */
	public static final Integer EMPLOYEE_CODE_MAX_LENGTH = 255;
	/** @description 勤務パターンコードの最大文字数 */
	public static final Integer PATTERN_CODE_MAX_LENGTH = 255;
	/** @description 日タイプの最大文字数 */
	public static final Integer DAY_TYPE_MAX_LENGTH = 255;
	/** @description 日付の最大文字数 */
	public static final Integer SHIFT_DATE_MAX_LENGTH = 255;

	/**
	 * コンストラクタ
	 */
	public AttPatternApplyImportEntity() {
		super(new AttPatternApplyImport__c());
	}
	/**
	 * コンストラクタ
	 */
	public AttPatternApplyImportEntity(AttPatternApplyImport__c sobj) {
		super(sobj);
	}

	/**
	 * @description エラーを設定する
	 * @param エラーメッセージのリスト
	 */
	public void setError(List<String> errorMessageList) {
		this.status = ImportStatus.ERROR;
		//
		this.error = String.join(errorMessageList, '\r\n');
	}
}