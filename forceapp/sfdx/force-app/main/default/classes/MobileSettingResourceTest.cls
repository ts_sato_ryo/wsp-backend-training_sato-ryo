/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description MobileSettingResourceのテストクラス
*/
@isTest
private class MobileSettingResourceTest {

	/**
	 * モバイル機能設定保存APIのテスト
	 * 設定が保存できることを確認する
	 */
	@isTest
	static void saveApiTestNormal() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);

		// リクエストパラメータ作成
		MobileSettingResource.SaveRequest param = new MobileSettingResource.SaveRequest();
		param.companyId = company.Id;
		param.requireLocationAtMobileStamp = true;

		// API実行
		MobileSettingResource.SaveApi api = new MobileSettingResource.SaveApi();
		api.execute(param);

		// 検証
		CompanyEntity savedCompany = new CompanyRepository().getEntity(company.Id);
		System.assertEquals(param.requireLocationAtMobileStamp, savedCompany.requireLocationAtMobileStamp);
	}

	/**
	 * モバイル機能設定保存APIのテスト
	 * リクエストパラメータの値が不正な場合に想定されている例外が発生することを確認する
	 */
	@isTest
	static void saveApiTestInvalidParam() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);

		MobileSettingResource.SaveApi api = new MobileSettingResource.SaveApi();
		MobileSettingResource.SaveRequest param;
		App.ParameterException resEx;
		String expMessage;

		// Test1: 会社IDが未設定
		try {
			resEx = null;

			param = new MobileSettingResource.SaveRequest();
			param.companyId = '';
			param.requireLocationAtMobileStamp = true;
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'companyId'});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test2: 会社IDがID以外の文字列
		try {
			resEx = null;

			param = new MobileSettingResource.SaveRequest();
			param.companyId = 'aaa';
			param.requireLocationAtMobileStamp = true;
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'companyId', param.companyId});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test3: 打刻時の位置情報を送信するが未設定
		try {
			resEx = null;

			param = new MobileSettingResource.SaveRequest();
			param.companyId = company.id;
			param.requireLocationAtMobileStamp = null;
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test3: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'requireLocationAtMobileStamp'});
		System.assertEquals(expMessage, resEx.getMessage());
	}

	/**
	 * モバイル機能設定保存APIのテスト
	 * API実行権限を持っていない場合、異常終了することを検証する
	 */
	@isTest
	static void saveApiTestNoPermission() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.Id);
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// モバイル機能設定権限を無効に設定する
		testData.permission.isManageMobileSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ作成
		MobileSettingResource.SaveRequest param = new MobileSettingResource.SaveRequest();
		param.companyId = company.Id;
		param.requireLocationAtMobileStamp = true;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				MobileSettingResource.SaveApi api = new MobileSettingResource.SaveApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * モバイル機能設定取得APIのテスト
	 * 設定を取得できることを確認する
	 */
	@isTest
	static void getApiTestNormal() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		company.RequireLocationAtMobileStamp__c = true;
		update company;

		// リクエストパラメータ作成
		MobileSettingResource.GetRequest param = new MobileSettingResource.GetRequest();
		param.companyId = company.Id;

		// API実効
		MobileSettingResource.GetApi api = new MobileSettingResource.GetApi();
		MobileSettingResource.GetResponse response = (MobileSettingResource.GetResponse)api.execute(param);

		// 検証
		System.assertEquals(company.RequireLocationAtMobileStamp__c, response.requireLocationAtMobileStamp);
	}

	/**
	 * モバイル機能設定取得APIのテスト
	 * リクエストパラメータの値が不正な場合に想定されている例外が発生することを確認する
	 */
	@isTest
	static void getApiTestInvalidParam() {

		// データ登録
		ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Company', country.id);
		company.RequireLocationAtMobileStamp__c = true;
		update company;

		MobileSettingResource.GetApi api = new MobileSettingResource.GetApi();
		MobileSettingResource.GetRequest param;
		App.ParameterException resEx;
		String expMessage;

		// Test1: 会社IDが未設定
		try {
			resEx = null;

			param = new MobileSettingResource.GetRequest();
			param.companyId = '';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_NullValue(new List<String>{'companyId'});
		System.assertEquals(expMessage, resEx.getMessage());

		// Test2: 会社IDがID以外の文字列
		try {
			resEx = null;

			param = new MobileSettingResource.GetRequest();
			param.companyId = 'aaa';
			api.execute(param);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		expMessage = ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'companyId', param.companyId});
		System.assertEquals(expMessage, resEx.getMessage());
	}
}