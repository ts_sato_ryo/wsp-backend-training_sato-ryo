/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description Test class of Custom Hint Resource
 */

@isTest
private class CustomHintResourceTest {
	static final ExpTestData EXP_TEST_DATA = new ExpTestData();
	/**
	 * Verify if custom hint is saved successfully
	 */
	@isTest static void saveCustomHintListSuccessfulTest() {
		CustomHintResource.SaveParam param1 = new CustomHintResource.SaveParam();
		populateBasicTestParam(param1);

		// create description parameters
		param1.recordJob_L0 = 'abc';
		param1.recordJob_L1 = 'いいい';
		param1.recordJob_L2 = 'xxx';

		RemoteApi.ResponseParam result;
		CustomHintResource.SaveApi saveApi;
		
		CustomHintResource.SearchParam param2 = new CustomHintResource.SearchParam();
		populateBasicTestParam(param2);
		CustomHintResource.SearchCustomHintResponse searchResult;
		CustomHintResource.SearchApi searchApi;
		
		Test.startTest();
		
		try {
			saveApi = new CustomHintResource.SaveApi();
			result = saveApi.execute(param1);

			// get custom hints
			searchApi = new CustomHintResource.SearchApi();
			searchResult = (CustomHintResource.SearchCustomHintResponse) searchApi.execute(param2);

			System.assert(searchResult != null);
			System.assert(!searchResult.records.isEmpty());
			System.assertEquals(param1.recordJob_L0, searchResult.records[0].recordJob_L0);
			System.assertEquals(param1.recordJob_L1, searchResult.records[0].recordJob_L1);
			System.assertEquals(param1.recordJob_L2, searchResult.records[0].recordJob_L2);

			// update description
			param1.recordJob_L0 = 'def';
			result = saveApi.execute(param1);

			searchResult = (CustomHintResource.SearchCustomHintResponse) searchApi.execute(param2);

			System.assert(searchResult != null);
			System.assert(!searchResult.records.isEmpty());
			System.assertEquals(param1.recordJob_L0, searchResult.records[0].recordJob_L0);
			System.assertEquals(param1.recordJob_L1, searchResult.records[0].recordJob_L1);
			System.assertEquals(param1.recordJob_L2, searchResult.records[0].recordJob_L2);

		} catch (Exception e) {
			System.assert(false);
		}

		Test.stopTest();

		System.assertEquals(result, null);

		// Check record
		ComCustomHint__c record = getRecordObject(param1.companyId, param1.moduleType, 'recordJob');
		System.assert(record != null);
		System.assertEquals(param1.recordJob_L0, record.Description_L0__c);
		System.assertEquals(param1.recordJob_L1, record.Description_L1__c);
		System.assertEquals(param1.recordJob_L2, record.Description_L2__c);
	}
	/**
	 * Verify if no changes is saved when no description parameters are provided
	 */
	@isTest static void saveCustomHintListEmptyTest() {
		CustomHintResource.SaveParam param1 = new CustomHintResource.SaveParam();
		populateBasicTestParam(param1);

		// create description parameters
		param1.reportHeaderAccountingPeriod_L0 = 'abc';
		param1.reportHeaderAccountingPeriod_L1 = 'いいい';
		param1.reportHeaderAccountingPeriod_L2 = 'xxx';
		
		RemoteApi.ResponseParam result;
		
		Test.startTest();
		
		try {
			// save custom hints
			CustomHintResource.SaveApi api = new CustomHintResource.SaveApi();
			result = api.execute(param1);
			System.assertEquals(result, null);

			// save empty custom hint list
			Map<String, Object> paramMap = new Map<String, Object> {
		 		'companyId' => EXP_TEST_DATA.company.id,
 				'moduleType' => CustomHintEntity.ModuleType.Expense.name()
		 	};
			result = api.execute(paramMap);
		} catch (Exception e) {
			System.assert(false);
		}

		Test.stopTest();

		System.assertEquals(result, null);

		// Check record
		ComCustomHint__c record = getRecordObject(param1.companyId, param1.moduleType, 'reportHeaderAccountingPeriod');
		System.assert(record != null);
		System.debug(record);
		//System.assertEquals(param1.reportHeaderAccountingPeriod_L0, record.Description_L0__c);
		System.assertEquals(param1.reportHeaderAccountingPeriod_L1, record.Description_L1__c);
		System.assertEquals(param1.reportHeaderAccountingPeriod_L2, record.Description_L2__c);
	}

	/**
	 * Verify if App.ParameterException is thrown when invalid company ID is provided
	 */
	@isTest static void saveCustomHintListInvalidCompanyIdNegativeTest() {
		CustomHintResource.SaveParam param = new CustomHintResource.SaveParam();
		populateBasicTestParam(param);
		param.companyId = 'invalid id';
		RemoteApi.ResponseParam result;
		
		Test.startTest();
		
		try {
			CustomHintResource.SaveApi api = new CustomHintResource.SaveApi();
			result = api.execute(param);
			System.assert(false);
		} catch (Exception e) {
			System.assert(e instanceof App.ParameterException);
		}

		Test.stopTest();
	}

	/**
	 * Verify if App.ParameterException is thrown when invalid module type is provided
	 */
	@isTest static void saveCustomHintListInvalidModuleTypeNegativeTest() {
		CustomHintResource.SaveParam param = new CustomHintResource.SaveParam();
		populateBasicTestParam(param);
		param.moduleType = 'invalid module type';
		RemoteApi.ResponseParam result;
		
		Test.startTest();
		
		try {
			CustomHintResource.SaveApi api = new CustomHintResource.SaveApi();
			result = api.execute(param);
			System.assert(false);
		} catch (Exception e) {
			System.assert(e instanceof App.ParameterException);
		}

		Test.stopTest();
	}

	/**
	 * Verify if App.ParameterException is thrown when description longer than 400 characters is provided
	 */
	@isTest static void saveCustomHintListMaxDescriptionLengthNegativeTest() {
		CustomHintResource.SaveParam param = new CustomHintResource.SaveParam();
		populateBasicTestParam(param);
		param.reportHeaderAccountingPeriod_L0 = 'a'.repeat(401);
		RemoteApi.ResponseParam result;
		
		Test.startTest();
		
		try {
			CustomHintResource.SaveApi api = new CustomHintResource.SaveApi();
			result = api.execute(param);
			System.assert(false);
		} catch (Exception e) {
			System.assert(e instanceof App.ParameterException);
		}

		Test.stopTest();
	}

	/**
	 * Verify if custom hint can be searched successfully
	 */
	@isTest static void searchCustomHintListSuccessfulTest() {
		CustomHintResource.SaveParam param1 = new CustomHintResource.SaveParam();
		populateBasicTestParam(param1);
		
		// create description parameters
		param1.recordCostCenter_L0 = 'abc';
		param1.recordCostCenter_L1 = 'いいい';
		param1.recordCostCenter_L2 = 'xxx';
		
		RemoteApi.ResponseParam saveResult;

		CustomHintResource.SearchParam param2 = new CustomHintResource.SearchParam();
		populateBasicTestParam(param2);

		System.assertEquals(param1.companyId, param2.companyId);
		System.assertEquals(param1.moduleType, param2.moduleType);

		CustomHintResource.SearchCustomHintResponse searchResult;
		
		Test.startTest();
		
		try {
			// save custom hints
			CustomHintResource.SaveApi saveApi = new CustomHintResource.SaveApi();
			saveResult = saveApi.execute(param1);
			System.assertEquals(saveResult, null);

			// get custom hints
			CustomHintResource.SearchApi searchApi = new CustomHintResource.SearchApi();
			searchResult = (CustomHintResource.SearchCustomHintResponse) searchApi.execute(param2);
		} catch (Exception e) {
			System.debug(e);
			System.assert(false);
		}

		Test.stopTest();

		// Check saved record
		ComCustomHint__c record = getRecordObject(param1.companyId, param1.moduleType, 'recordCostCenter');
		System.assert(record != null);
		System.assertEquals(param1.recordCostCenter_L0, record.Description_L0__c);
		System.assertEquals(param1.recordCostCenter_L1, record.Description_L1__c);
		System.assertEquals(param1.recordCostCenter_L2, record.Description_L2__c);

		System.assert(searchResult != null);
		System.assert(!searchResult.records.isEmpty());

		// Check record
		System.assertEquals(param1.companyId, searchResult.records[0].companyId);
		System.assertEquals(param2.companyId, searchResult.records[0].companyId);
		System.assertEquals(param1.moduleType, searchResult.records[0].moduleType);
		System.assertEquals(param2.moduleType, searchResult.records[0].moduleType);

		System.assertEquals(param1.recordCostCenter_L0, searchResult.records[0].recordCostCenter_L0);
		System.assertEquals(param1.recordCostCenter_L1, searchResult.records[0].recordCostCenter_L1);
		System.assertEquals(param1.recordCostCenter_L2, searchResult.records[0].recordCostCenter_L2);
	}

	/**
	 * Verify if descriptions are returned as null (except company ID and module type)
	 * when there exists no custom hint yet
	 */
	@isTest static void searchCustomHintListNegativeTest() {
		CustomHintResource.SearchParam param = new CustomHintResource.SearchParam();
		populateBasicTestParam(param);
		CustomHintResource.SearchCustomHintResponse searchResult;
		
		Test.startTest();
		
		try {
			// get custom hints
			CustomHintResource.SearchApi searchApi = new CustomHintResource.SearchApi();
			searchResult = (CustomHintResource.SearchCustomHintResponse) searchApi.execute(param);
		} catch (Exception e) {
			System.debug(e.getStackTraceString());
			System.assert(false);
		}

		Test.stopTest();

		System.assert(searchResult != null);
		System.assert(!searchResult.records.isEmpty());

		// Check result
		System.assertEquals(param.companyId, searchResult.records[0].companyId);
		System.assertEquals(param.moduleType, searchResult.records[0].moduleType);

		System.assertEquals(null, searchResult.records[0].reportHeaderAccountingPeriod_L0);
		System.assertEquals(null, searchResult.records[0].reportHeaderAccountingPeriod_L1);
		System.assertEquals(null, searchResult.records[0].reportHeaderAccountingPeriod_L2);
	}

	/**
	 * Verify if custom hint can be retrieved successfully
	 */
	@isTest static void getCustomHintListSuccessfulTest() {
		CustomHintResource.SaveParam param1 = new CustomHintResource.SaveParam();
		populateBasicTestParam(param1);
		
		// create description parameters
		param1.recordCostCenter_L0 = 'abc';
		param1.recordCostCenter_L1 = 'いいい';
		param1.recordCostCenter_L2 = 'xxx';
		
		RemoteApi.ResponseParam saveResult;

		CustomHintResource.GetParam param2 = new CustomHintResource.GetParam();
		populateBasicTestParam(param2);

		System.assertEquals(param1.companyId, param2.companyId);
		System.assertEquals(param1.moduleType, param2.moduleType);

		CustomHintResource.GetCustomHintResponse getResult;
		
		Test.startTest();
		
		try {
			// save custom hints
			CustomHintResource.SaveApi saveApi = new CustomHintResource.SaveApi();
			saveResult = saveApi.execute(param1);
			System.assertEquals(saveResult, null);

			// get custom hints
			CustomHintResource.GetApi getApi = new CustomHintResource.GetApi();
			getResult = (CustomHintResource.GetCustomHintResponse) getApi.execute(param2);
		} catch (Exception e) {
			System.debug(e);
			System.assert(false);
		}

		Test.stopTest();

		// Check saved record
		ComCustomHint__c record = getRecordObject(param1.companyId, param1.moduleType, 'recordCostCenter');
		System.assert(record != null);
		System.assertEquals(param1.recordCostCenter_L0, record.Description_L0__c);
		System.assertEquals(param1.recordCostCenter_L1, record.Description_L1__c);
		System.assertEquals(param1.recordCostCenter_L2, record.Description_L2__c);

		System.assert(getResult != null);

		// Check record
		System.assertEquals(param1.companyId, getResult.companyId);
		System.assertEquals(param2.companyId, getResult.companyId);
		System.assertEquals(param1.moduleType, getResult.moduleType);
		System.assertEquals(param2.moduleType, getResult.moduleType);

		CustomHintEntity entity = new CustomHintRepository().createEntity(record);

		System.assertEquals(entity.description.getValue(), getResult.recordCostCenter);
	}

	/**
	 * Verify if descriptions are returned as null (except company ID and module type)
	 * when there exists no custom hint yet
	 */
	@isTest static void getCustomHintListNegativeTest() {
		CustomHintResource.GetParam param = new CustomHintResource.GetParam();
		populateBasicTestParam(param);
		CustomHintResource.GetCustomHintResponse getResult;
		
		Test.startTest();
		
		try {
			// get custom hints
			CustomHintResource.GetApi getApi = new CustomHintResource.GetApi();
			getResult = (CustomHintResource.GetCustomHintResponse) getApi.execute(param);
		} catch (Exception e) {
			System.debug(e.getStackTraceString());
			System.assert(false);
		}

		Test.stopTest();

		System.assert(getResult != null);

		// Check result
		System.assertEquals(param.companyId, getResult.companyId);
		System.assertEquals(param.moduleType, getResult.moduleType);

		System.assertEquals(null, getResult.reportHeaderAccountingPeriod);
	}

	static ComCustomHint__c getRecordObject(String companyId, String moduleType, String fieldName) {
		return [
			SELECT id, CompanyId__c, ModuleType__c, Field__c, Description_L0__c, Description_L1__c, Description_L2__c
			FROM ComCustomHint__c
			WHERE CompanyId__c = :companyId
			AND ModuleType__c = :moduleType
			AND Field__c = :fieldName
			][0];
	}

	// Populate basic request parameter
	static void populateBasicTestParam(CustomHintResource.CustomHintBasicSetting param) {
		param.companyId = EXP_TEST_DATA.company.id;
		param.moduleType = CustomHintEntity.ModuleType.Expense.name();
	}
}