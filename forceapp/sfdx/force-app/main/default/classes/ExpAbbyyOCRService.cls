/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description Service Class for OCR Abbyy
 * refer to the documentation;
 * https://teamspiritdev.atlassian.net/wiki/spaces/GENIE/pages/985367126/Abbyy+OCR
 */
public with sharing class ExpAbbyyOCRService {

	private static final String ABBYY_APPLICATION_NAME = 'TeamSpirit';
	private static final String ABBYY_PASSWORD = 'roFnSTZy/dbyEj0en8EStZHJ';
	private static final String BASE_URL = 'https://cloud-westus.ocrsdk.com';
	private static final String FUNCTION_RECEIPT_URL = '/processReceipt?';
	private static final String FUNCTION_GET_TASK_STATUS = '/getTaskStatus?';
	private static final String COUNTRY_JAPAN = 'japan';
	private static final String COUNTRY_SINGAPORE = 'singapore';

	/**
	 * @description ocr result value parsed from json (Abbyy data)
	 */
	public class OcrResultValue {
		public String recordDate;
		public Decimal amount;
		public String allText;
	}

	/**
	 * @description get Auth Header
	 * @param none
	 * @return auth header
	 */
	public class OCRInfo {
		public OcrResultValue result;
		public String status;
		public String taskId;
	}

	/**
	 * @description encode user password
	 * @param none
	 * @return encoded application name + password
	 */
	private String encodeUserPassword() {
		String toEncode = ABBYY_APPLICATION_NAME + ':' + ABBYY_PASSWORD;
		Blob toEncodeBlob = Blob.valueOf(toEncode);
		return EncodingUtil.base64Encode(toEncodeBlob);
	}

	/**
	 * @description get Auth Header
	 * @param none
	 * @return auth header
	 */
	private String getAuthHeader() {
		return 'BASIC ' + encodeUserPassword();
	}

	public OCRInfo searchEntityStatusByReceiptId(String receiptId){
		ExpOCREntity entity = new ExpOCRRepository().searchEntityByReceiptId(receiptId);

		return entity == null ? null : createOCRInfo(entity);
	}

	private OCRInfo createOCRInfo(ExpOCREntity entity){
		OcrResultValue v = new OcrResultValue();
		v.recordDate = entity.ocrDate != null ? entity.ocrDate.format() : null;
		v.amount = entity.ocrAmount;
		v.allText = entity.ocrAllText;

		OCRInfo s = new OCRInfo();
		s.result = v;
		s.status = entity.ocrProcessStatus;
		s.taskId = entity.taskId;
		return s;
	}

	/**
	 * @description execute OCR
	 * 	flow:
	 * 	1. upload image file to abbyy
	 * 	2. save record with status IN_PROGRESS & taskId(= used to identify image file in Abbyy)
	 * @param contentVersionId: Content Version Id to be OCR executed
	 * @return taskId (retrieved from Abbyy)
	 */
	public String executeOCR(Id contentVersionId) {

		ExpOCRRepository ocrRepo = new ExpOCRRepository();
		// if status is `In Process`, return corresponding taskId which is also fetched from DB.
		// this case is happen when user close the receipt library dialog before complete OCR for uploaded image
		ExpOCREntity ocrEntity = ocrRepo.searchEntityByReceiptId(contentVersionId);
		if(ocrEntity!=null && ocrEntity.ocrProcessStatus == ExpOCRRepository.IN_PROGRESS){
			return ocrEntity.taskId;
		}

		HttpRequest req = new HttpRequest();
		String country = AppLanguage.getUserLang()==AppLanguage.JA ? COUNTRY_JAPAN : COUNTRY_SINGAPORE ;
		String url = BASE_URL + FUNCTION_RECEIPT_URL + 'country=' + country;
		req.setEndpoint(url);
		req.setMethod('POST');
		req.setHeader('Authorization', getAuthHeader());
		req.setHeader('Content-Type', 'application/octet-stream');
		AppContentVersionEntity entity = new AppContentVersionRepository().getEntity(contentVersionId);
		if(entity == null){
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_InvalidContentVersionId);
		}
		req.setBodyAsBlob(entity.VersionData);

		Http http = new Http();
		HttpResponse res;
		try {
			res = http.send(req);
		} catch (CalloutException e) {
			throw new App.IllegalStateException(e.getMessage());
		}

		String taskId;
		if(res.getStatusCode() == 200){
			//result from abbyy is xml file so need to parse it
			taskId = res.getBodyDocument().getRootElement().getChildElement('task', null).getAttribute('id', null);

			EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
			ocrRepo.saveEntityWithStatus(contentVersionId, taskId, employee.companyId, ExpOCRRepository.IN_PROGRESS);

		} else {
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_GetAbbyyAPIError + ' Error Code: ' + res.getStatusCode());
		}
		return taskId;
	}


	/**
	 * @description get OCR status and if the status is completed, return result parsed from xml
	 * flow:
	 *	1. call Abbyy getTaskStatus API
	 *	2. if task is Completed, call getAndSaveOcrResult function; access url which contains result xml then return & save the OCR result.
	 * @param taskId: used to identify image file in Abbyy. `task` means Abbyy's OCR process.
	 * @return OCRStatus (and OCR result)
	 */
	public OCRInfo getOCRStatus(String taskId) {

		HttpRequest req = new HttpRequest();
		req.setEndpoint(BASE_URL + FUNCTION_GET_TASK_STATUS + 'taskId=' + taskId);
		req.setMethod('GET');
		req.setHeader('Authorization', getAuthHeader());

		Http http = new Http();
		HttpResponse res;
		try {
			res = http.send(req);
		} catch (CalloutException e) {
			throw new App.IllegalStateException(e.getMessage());
		}

		String status;
		if(res.getStatusCode() == 200){
			status = res.getBodyDocument().getRootElement().getChildElement('task', null).getAttribute('status', null);
		} else {
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_GetAbbyyAPIError + ' Error Code: ' + res.getStatusCode());
		}

		OCRInfo returnValue = new OCRInfo();
		returnValue.status = status;
		if(ExpOCRRepository.COMPLETED.equals(status)) {
			String resultUrl = res.getBodyDocument().getRootElement().getChildElement('task', null).getAttribute('resultUrl', null);
			returnValue.result= getAndSaveOcrResult(resultUrl, taskId);
		}

		return returnValue;
	}

	/**
	 * @description Get OCR result xml then convert to json and parse. then save the result into DB.
	 * @param resultUrl: OCR resultUrl which contains result xml.
	 * @return OcrResultValue
	 */
	private OcrResultValue getAndSaveOcrResult(String resultUrl, String taskId) {

		HttpRequest req = new HttpRequest();
		req.setEndpoint(resultUrl);
		req.setMethod('GET');
		Http http = new Http();
		HttpResponse res;
		try {
			res = http.send(req);
		} catch (CalloutException e) {
			throw new App.IllegalStateException(e.getMessage());
		}

		String suffixedJson;
		OcrResultValue r = new OcrResultValue();
		if(res.getStatusCode() == 200){
			String xml = res.getBody();
			// convert xml to json
			String jsonStr = ComXMLUtil.xmlToJson(xml).replaceAll('\\r\\n. |\\r. |\\n', '');

			try{
				Object jsonObj = Json.deserializeUntyped(jsonStr);
				// add suffix otherwise get `identifier name is reserved` error
				// refer to https://salesforce.stackexchange.com/questions/2276/how-do-you-deserialize-json-properties-that-are-reserved-words-in-apex
				suffixedJson = new ReservedWordSerializer(jsonObj).getAsString();
				ComReceiptOCRType.OcrResult ocrRes = (ComReceiptOCRType.OcrResult)Json.deserialize(suffixedJson, ComReceiptOCRType.OcrResult.class);
				r.amount = (ocrRes.receipts_x.receipt_x.total_x != null) ? Decimal.valueOf(ocrRes.receipts_x.receipt_x.total_x.normalizedValue_x) : null;
				r.recordDate = (ocrRes.receipts_x.receipt_x.date_x != null) ? ocrRes.receipts_x.receipt_x.date_x.normalizedValue_x : null;
			} catch (Exception e){ // in case cannot parse well
				r.amount = null;
				r.recordDate = null;
			}

		} else {
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_GetAbbyyAPIError + ' Error Code: ' + res.getStatusCode());
		}

		ExpOCRRepository repo = new ExpOCRRepository();
		ExpOCREntity ocrEntity = repo.searchEntityByTaskId(taskId);
		// save the OCR result information
		repo.saveEntityWithOCRResults(ocrEntity, ExpOCRRepository.COMPLETED, r.allText, r.amount, AppDate.valueOf(r.recordDate), suffixedJson);

		return r;
	}

}