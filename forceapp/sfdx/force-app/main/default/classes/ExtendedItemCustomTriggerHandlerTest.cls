/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Test class for ExtendedItemCustomTriggerHandler
 *
 * @group Expense
 */
@isTest
private class ExtendedItemCustomTriggerHandlerTest {

	@TestSetup
	static void setup() {
		TestData.setupMaster();
	}

	/*
	 * Test that records can be updated successfully.
	 */
	@isTest static void insertPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity companyA = testDataEntity.company;
		CompanyEntity companyB = testDataEntity.createCompany('another company');
		ComTestDataUtility.createExtendedItemCustoms('init', companyA.id, 10);
		ComTestDataUtility.createExtendedItemCustoms('init', companyB.id, 10);

		List<ComExtendedItemCustom__c> newList = new List<ComExtendedItemCustom__c> ();

		String name = 'new custom';
		String code = 'new_custom';
		List<ComExtendedItemCustom__c> companyAExtendItemCustomList = createExtendedItemCustomSObjects(name, code, companyA.id, companyA.code, 5);
		List<ComExtendedItemCustom__c> companyBExtendItemCustomList = createExtendedItemCustomSObjects(name, code, companyB.id, companyB.code, 5);

		newList.addAll(companyAExtendItemCustomList);
		newList.addAll(companyBExtendItemCustomList);

		INSERT newList;

		List<ComExtendedItemCustom__c> retList = [SELECT Id FROM ComExtendedItemCustom__c];
		System.assertEquals(30, retList.size());    // Initially, 10 in each company. Added 5 more to each company.
	}

	/*
	 * Test that the Validation Trigger works correctly at large amount of data.
	 * By Default data-loader process 2000 records.
	 */
	@isTest static void insertBigDataPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity companyA = testDataEntity.company;
		CompanyEntity companyB = testDataEntity.createCompany('another company');
		ComTestDataUtility.createExtendedItemCustoms('init', companyA.id, 1000);
		ComTestDataUtility.createExtendedItemCustoms('init', companyB.id, 1000);

		List<ComExtendedItemCustom__c> newList = new List<ComExtendedItemCustom__c> ();

		String name = 'new custom';
		String code = 'new_custom';
		List<ComExtendedItemCustom__c> companyAExtendItemCustomList = createExtendedItemCustomSObjects(name, code, companyA.id, companyA.code, 1000);
		List<ComExtendedItemCustom__c> companyBExtendItemCustomList = createExtendedItemCustomSObjects(name, code, companyB.id, companyB.code, 1000);

		newList.addAll(companyAExtendItemCustomList);
		newList.addAll(companyBExtendItemCustomList);

		Test.startTest();
		INSERT newList;
		Test.stopTest();

		List<ComExtendedItemCustom__c> retList = [SELECT Id FROM ComExtendedItemCustom__c];
		System.assertEquals(4000, retList.size());    // Initially, 1000 in each company. Added 1000 more to each company.
	}

	/*
	 * Test that records can be updated successfully.
	 */
	@isTest static void updatePositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity companyA = testDataEntity.company;
		CompanyEntity companyB = testDataEntity.createCompany('companyB');
		CompanyEntity companyC = testDataEntity.createCompany('companyC');
		ComTestDataUtility.createExtendedItemCustoms('init', companyA.id, 10);
		ComTestDataUtility.createExtendedItemCustoms('init', companyB.id, 10);
		ComTestDataUtility.createExtendedItemCustoms('init', companyC.id, 10);

		// Test that if code is modified and the value is NOT in used, it can be updated.
		// Test that if code is NOT modified, then no error is thrown.
		// Test that even if the Code is the same, if the company is different, NO error is thrown
		List<ComExtendedItemCustom__c> toUpdateList = [SELECT Id, Code__c, Name_L0__c, CompanyId__c FROM ComExtendedItemCustom__c];
		for (ComExtendedItemCustom__c sObj : toUpdateList) {
			if (sObj.CompanyId__c == companyB.id) {
				sObj.Name_L0__c = sObj.Name_L0__c + '-edited';
			} else {
				// For Company A and Company C, the Custom Extended Items will use the same Code value.
				sObj.Code__c = sObj.Code__c + '-edited';
			}

		}
		UPDATE toUpdateList;

		List<ComExtendedItemCustom__c> retList = [SELECT Id, Code__c, Name_L0__c, CompanyId__c FROM ComExtendedItemCustom__c];
		for (ComExtendedItemCustom__c retSObj : retList) {
			if (retSObj.CompanyId__c == companyB.id) {
				System.assertEquals(true, retSObj.Name_L0__c.endsWith('-edited'));
			} else {
				System.assertEquals(true, retSObj.Code__c.endsWith('-edited'));
			}
		}
	}

	/*
	 * Test that the Validation Trigger works correctly at large amount of data.
	 * By Default data-loader process 2000 records.
	 */
	@isTest static void updateBigDataPositiveTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity companyA = testDataEntity.company;
		CompanyEntity companyB = testDataEntity.createCompany('companyB');
		CompanyEntity companyC = testDataEntity.createCompany('companyC');
		ComTestDataUtility.createExtendedItemCustoms('init', companyA.id, 250);
		ComTestDataUtility.createExtendedItemCustoms('init', companyB.id, 1500);
		ComTestDataUtility.createExtendedItemCustoms('init', companyC.id, 250);

		// Test that if code is modified and the value is NOT in used, it can be updated.
		// Test that if code is NOT modified, then no error is thrown.
		// Test that even if the Code is the same, if the company is different, NO error is thrown
		List<ComExtendedItemCustom__c> toUpdateList = [SELECT Id, Code__c, Name_L0__c, CompanyId__c FROM ComExtendedItemCustom__c];
		for (ComExtendedItemCustom__c sObj : toUpdateList) {
			if (sObj.CompanyId__c == companyB.id) {
				sObj.Name_L0__c = sObj.Name_L0__c + '-edited';
			} else {
				// For Company A and Company C, the Custom Extended Items will use the same Code value.
				sObj.Code__c = sObj.Code__c + '-edited';
			}

		}
		UPDATE toUpdateList;
	}

	/*
	 * Test that if the record to be inserted is using which which are already in used in the company, error is thrown.
	 */
	@isTest static void insertAlreadyInUsedNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity companyA = testDataEntity.company;
		CompanyEntity companyB = testDataEntity.createCompany('company B');
		ComTestDataUtility.createExtendedItemCustoms('init', companyA.id, 5);
		List<ComExtendedItemCustom__c> companyBEIC = ComTestDataUtility.createExtendedItemCustoms('init', companyB.id, 10);

		// Since the company A already has 5 custom extended items, this will make 5 already in used + 5 New
		for (Integer i = 0; i < companyBEIC.size(); i++) {
			companyBEIC.get(i).Id = null;
			companyBEIC.get(i).CompanyId__c = companyA.id;
			// Since unique key is already controlled at SF obj level, not necessary to test it.
			companyBEIC.get(i).UniqKey__c = companyBEIC.get(i).UniqKey__c + '_new_' + i;
		}

		Exception expectedException;
		try {
			INSERT companyBEIC;
		} catch (Exception e) {
			expectedException = e;
		}

		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Exp_Err_DuplicateExtendedItemCustomCode));

		List<ComExtendedItemCustom__c> retList = [
				SELECT Id, Code__c, CompanyId__c
				FROM ComExtendedItemCustom__c WHERE CompanyId__c = :companyA.id
		];
		// There shouldn't have any change
		System.assertEquals(5, retList.size());
	}

	/*
	 * Test that if the record to be updated is using which which are already in used in the company, error is thrown.
	 */
	@isTest static void updateAlreadyInUsedNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity companyA = testDataEntity.company;
		CompanyEntity companyB = testDataEntity.createCompany('company B');
		List<ComExtendedItemCustom__c> companyAEIC = ComTestDataUtility.createExtendedItemCustoms('init', companyA.id, 5);
		List<ComExtendedItemCustom__c> companyBEIC = ComTestDataUtility.createExtendedItemCustoms('init', companyB.id, 10);

		// WHEN THE NEWLY SPECIFIED CODE IS ALREADY USED BY ANOTHER CUSTOM IN THE SAME CUSTOM => NG
		// Use a code which is already in used in the system.
		Integer targetIndex = 5;
		companyBEIC.get(targetIndex).Code__c = companyBEIC.get(0).Code__c;

		Exception expectedException;
		try {
			UPDATE companyBEIC;
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Exp_Err_DuplicateExtendedItemCustomCode));

		// WHEN CHANGING THE PARENT COMPANY, THE CURRENT EXISTING CODE IS ALREADY IN USED IN THE NEW PARENT COMPANY => NG
		targetIndex = 2;
		companyAEIC.get(targetIndex).CompanyId__c = companyB.id;
		Exception codeAlreadyInUsedInNewParentException;
		try {
			UPDATE companyAEIC;
		} catch (Exception e) {
			codeAlreadyInUsedInNewParentException = e;
		}
		System.assertNotEquals(null, codeAlreadyInUsedInNewParentException);
		System.assertEquals(true, codeAlreadyInUsedInNewParentException instanceof DmlException);
		System.assertEquals(true, codeAlreadyInUsedInNewParentException.getMessage().contains(ComMessage.msg().Exp_Err_DuplicateExtendedItemCustomCode));
	}

	/*
	 * Test that when the records which are to be inserted contains duplicate code, error is thrown.
	 */
	@isTest static void insertDuplicateWithinNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity company = testDataEntity.company;
		List<ComExtendedItemCustom__c> extendItemCustomList = createExtendedItemCustomSObjects('Test', 'TEST', company.id, company.code, 5);
		// Create Duplicate within the records to be inserted
		extendItemCustomList.get(1).Code__c = extendItemCustomList.get(4).Code__c;
		extendItemCustomList.get(2).Code__c = extendItemCustomList.get(3).Code__c;

		Exception expectedException;
		try {
			INSERT extendItemCustomList;
		} catch (Exception e) {
			expectedException = e;
		}

		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_DuplicateCode));
	}

	/*
	 * Test that when the records which are to be updated contains duplicate code, error is thrown.
	 */
	@isTest static void updateDuplicateWithinNegativeTest() {
		TestData.TestDataEntity testDataEntity = new TestData.TestDataEntity();
		CompanyEntity company = testDataEntity.company;
		List<ComExtendedItemCustom__c> extendItemCustomList = createExtendedItemCustomSObjects('Test', 'TEST', company.id, company.code, 5);
		INSERT extendItemCustomList;

		// Change all the codes (since it won't validate if the code isn't changed)
		for (ComExtendedItemCustom__c sObj: extendItemCustomList) {
			sObj.Code__c = sObj.Code__c + '-edited';
		}
		// Create Duplicate within the records to be updated
		extendItemCustomList.get(1).Code__c = extendItemCustomList.get(4).Code__c;

		Exception expectedException;
		try {
			UPDATE extendItemCustomList;
		} catch (Exception e) {
			expectedException = e;
		}

		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_DuplicateCode));
	}

	/*
	 * Test that
	 *    if an Extended Item Custom is already linked to an Extended Item, it's parent cannot be modified.
	 *    if an Extended Item Custom is NOT yet linked to any ExtendedIem, then it can be reparented.
	 */
	@isTest static void updatingAlreadyReferencedTest() {
		ComOrgSetting__c org = [SELECT ID, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];
		ComCountry__c country = [SELECT ID, Name, Name_L0__c, Name_L1__c, Name_L2__c, Code__c FROM ComCountry__c];
		ComCompany__c newCompany = ComTestDataUtility.createCompany('NC', country.Id);
		ExpTestData testData = new ExpTestData(org, country);

		ComExtendedItemCustom__c floatingExtendedItemCustom =  ComTestDataUtility.createExtendedItemCustoms('new', testData.company.Id, 1)[0];

		Test.startTest();
		// Test 1: Update the parent AND it's NOT linked to any Extended Item => OK
		floatingExtendedItemCustom.CompanyId__c = newCompany.Id;
		UPDATE floatingExtendedItemCustom;
		ComExtendedItemCustom__c retrievedCustom = [SELECT ID, CompanyId__c FROM ComExtendedItemCustom__c WHERE ID = :floatingExtendedItemCustom.Id];
		System.assertEquals(floatingExtendedItemCustom.CompanyId__c, retrievedCustom.CompanyId__c);

		// Test 2: Update the parent AND it's already linked to an Extended Item => NG
		ComExtendedItemCustom__c alreadyInUsed = testData.extendedItemCustomList[0];
		alreadyInUsed.CompanyId__c = newCompany.Id;
		Exception expectedException;
		try {
			UPDATE alreadyInUsed;
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof DmlException);
		System.assertEquals(true, expectedException.getMessage().contains(ComMessage.msg().Com_Err_CannotChangeReference));

		// Test 3: Update the code/name AND it's already linked to an Extended Item => OK
		alreadyInUsed = testData.extendedItemCustomList[1];
		alreadyInUsed.Code__c = 'NewCODE';
		alreadyInUsed.Name_L0__c = 'New NAME';
		UPDATE alreadyInUsed;
		retrievedCustom = [SELECT ID, Code__c, Name_L0__c FROM ComExtendedItemCustom__c WHERE ID = :alreadyInUsed.Id];
		System.assertEquals(alreadyInUsed.Code__c, retrievedCustom.Code__c);
		System.assertEquals(alreadyInUsed.Name_L0__c, retrievedCustom.Name_L0__c);
		Test.stopTest();
	}

	/*
	 * For creating extended item sObj (without inserting them to the DB)
	 */
	private static List<ComExtendedItemCustom__c> createExtendedItemCustomSObjects(String name, String code, Id companyId, String companyCode, Integer size) {
		List<ComExtendedItemCustom__c> sObjList = new List<ComExtendedItemCustom__c>();
		for (Integer i = 0; i < size; i++) {
			ComExtendedItemCustom__c newCustom = new ComExtendedItemCustom__c(
				Name = name + ' ' + i,
				Name_L0__c = name + ' ' + i + ' L0',
				Name_L1__c = name + ' ' + i + ' L1',
				Name_L2__c = name + ' ' + i + ' L2',
				Code__c = code + '_' + i,
				CompanyId__c = companyId,
				UniqKey__c = companyCode + code + '_' + i
			);
			sObjList.add(newCustom);
		}
		return sObjList;
	}
}