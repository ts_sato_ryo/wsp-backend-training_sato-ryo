/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 工数設定レコード操作APIを実装するクラス
 */
public with sharing class TimeSettingResource {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 工数設定レコードを表すクラス
	 */
	public class TimeSetting implements RemoteApi.RequestParam {
		/** 工数設定レコードベースID */
		public String id;
		/** 工数設定履歴レコードID */
		public String historyId;
		/** 工数設定名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 工数設定名(言語0) */
		public String name_L0;
		/** 工数設定名(言語1) */
		public String name_L1;
		/** 工数設定名(言語2) */
		public String name_L2;
		/** コード */
		public String code;
		/** 会社レコードID */
		public String companyId;
		/** サマリー期間 */
		public String summaryPeriod;
		/** 月度起算日 */
		public String startDateOfMonth;
		/** 起算曜日 */
		public String startDayOfWeek;
		/** 月度の表記 */
		public String monthMark;
		/** 工数確定申請を利用する */
		public Boolean useRequest;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 履歴コメント */
		public String comment;

		/**
		 * パラメータの値を設定したベースエンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public TimeSettingBaseEntity createBaseEntity( Map<String, Object> paramMap, Boolean isUpdate) {

			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();

			// レコード更新時はIDをセット
			if (isUpdate) {
				setting.setId(this.id);
			}
			if (!isUpdate && paramMap.containsKey('name_L0')) {
				// TODO Name項目対応(name_L0を設定しておく)
				setting.name = this.name_L0;
			}
			if (paramMap.containsKey('code')) {
				setting.code = this.code;
			}
			if (paramMap.containsKey('summaryPeriod')) {
				setting.setSummaryPeriod(this.summaryPeriod);
			}
			if (setting.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Month) {
				// サマリー期間のみ起算日有効
				if (paramMap.containsKey('startDateOfMonth')) {
					setting.startDateOfMonth = AppConverter.intValue(this.startDateOfMonth);
				}
			} else if (setting.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Week) {
				// サマリー期間のみ起算曜日有効
				if (paramMap.containsKey('startDayOfWeek')) {
					setting.setStartDayOfWeek(this.startDayOfWeek);
				}
			}
			if (paramMap.containsKey('monthMark')) {
				setting.setMonthMark(this.monthMark);
			}
			if (paramMap.containsKey('useRequest')) {
				setting.useRequest = this.useRequest;
			} else {
				setting.useRequest = true;
			}
			// 以下、レコード作成時のみ値をセット
			if (!isUpdate && paramMap.containsKey('companyId')) {
				// 会社IDは主従関係の親かつ変更不可
				setting.companyId = this.companyId;
			}

			return setting;
		}

		/**
		 * パラメータの値を設定した履歴エンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public TimeSettingHistoryEntity createHistoryEntity( Map<String, Object> paramMap) {
			TimeSettingHistoryEntity setting = new TimeSettingHistoryEntity();

			// IDが設定されていれば更新
			final Boolean isUpdate = this.id == null ? false : true;

			if (!isUpdate && paramMap.containsKey('name_L0')) {
				// TODO Name項目対応(name_L0を設定しておく)
				setting.name = this.name_L0;
				setting.nameL0 = this.name_L0;
			}
			if (!isUpdate && paramMap.containsKey('name_L1')) {
				setting.nameL1 = this.name_L1;
			}
			if (!isUpdate && paramMap.containsKey('name_L2')) {
				setting.nameL2 = this.name_L2;
			}
			if (!isUpdate && paramMap.containsKey('validDateFrom') && this.validDateFrom != null) {
				setting.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (!isUpdate && paramMap.containsKey('validDateTo')) {
				setting.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (!isUpdate && paramMap.containsKey('comment')) {
				setting.historyComment = this.comment;
			}

			// 有効開始日のデフォルトは実行日
			if (setting.validFrom == null) {
				setting.validFrom = AppDate.today();
			}
			// 失効日のデフォルト
			if (setting.validTo == null) {
				setting.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}

			return setting;
		}

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// 月度起算日
			if (String.isNotBlank(this.startDateOfMonth)) {
				AppParser.validateParseInteger(MESSAGE.Admin_Lbl_StartDateOfMonth, this.startDateOfMonth.trim());
			}
		}

	}


	/**
	 * @description 工数設定レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したベースレコードID */
		public String id;
	}

	/**
	 * @description 工数設定レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_SETTING;

		/**
		 * @description 工数設定レコードを1件作成する
		 * @param req リクエストパラメータ(TimeSetting)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			TimeSetting param = (TimeSetting)req.getParam(TimeSettingResource.TimeSetting.class);
			param.validate();

			// 権限チェック
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			final Boolean isUpdate = false;
			TimeSettingBaseEntity base = param.createBaseEntity(req.getParamMap(), isUpdate);
			base.addHistory(param.createHistoryEntity(req.getParamMap()));

			// 保存する
			TimeSettingService service = new TimeSettingService();
			service.updateUniqKey(base);
			Id resId = service.saveNewEntity(base);

			TimeSettingResource.SaveResult res = new TimeSettingResource.SaveResult();
			res.Id = resId;
			return res;
		}
	}


	/**
	 * @desctiprion 工数設定レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_SETTING;

		/**
		 * @description 工数設定レコードを1件更新する
		 * @param req リクエストパラメータ(TimeSetting)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			TimeSetting param = (TimeSetting)req.getParam(TimeSetting.class);
			param.validate();

			// パラメータのバリデーション
			validateParam(param);

			// 権限チェック
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 更新する
			final Boolean isUpdate = true;
			TimeSettingBaseEntity base = param.createBaseEntity(req.getParamMap(), isUpdate);
			TimeSettingService service = new TimeSettingService();
			service.updateUniqKey(base);
			service.updateBase(base);

			// 成功レスポンスをセットする
			return null;
	}

		/**
		 * ベース更新APIパラメータのバリデーションを実行
		 */
		private void validateParam(TimeSettingResource.TimeSetting param) {
			// ID
			if (String.isBlank(param.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('ID');
				throw ex;
			}
		}

	}


	/**
	 * @description 工数設定レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}

	}

	/**
	 * 工数設定レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final TimeConfigResourcePermission.Permission requriedPermission =
				TimeConfigResourcePermission.Permission.MANAGE_TIME_SETTING;

		/**
		 * @description 工数設定レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			 param.validate();

			 // 権限チェック
			new TimeConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				(new TimeSettingService()).deleteEntity(param.id);
				// (new DepartmentRepository()).deleteEntity(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合は成功レスポンスを返す
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					 throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		 }
	}

	/**
	 * @description 工数設定レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 会社レコードID */
		public String companyId;
		/** 有効期間の対象日 */
		public Date targetDate;
	}

	/**
	 * @description 工数設定レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public TimeSetting[] records;
	}

	/**
	 * @description 工数設定レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 工数設定レコードを検索する
		 * @param requ リクエストパラメータ(SearchCondition)
		 * @return レスポンス(SearchResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);

			TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
			if (param.id != null) {
				filter.baseIds = new Set<Id>{param.id};
			}
			if (param.companyId != null) {
				filter.companyIds = new Set<Id>{param.companyId};
			}
			if (param.targetDate != null) {
				filter.targetDate = AppDate.valueOf(param.targetDate);
			} else {
				// デフォルト値は今日の日付
				filter.targetDate = AppDate.today();
			}

			List<TimeSettingBaseEntity> entityList = (new TimeSettingRepository()).searchEntity(filter);

			// レスポンスクラスに変換
			List<TimeSetting> timeSettingList = new List<TimeSetting>();
			for (TimeSettingBaseEntity entity : entityList) {
				TimeSettingHistoryEntity historyEntity = entity.getHistory(0);
				TimeSetting setting = new TimeSetting();
				setting.id = entity.id;
				setting.code = entity.code;
				setting.companyId = entity.companyId;
				setting.summaryPeriod = String.valueOf(entity.summaryPeriod);
				setting.startDateOfMonth = String.valueOf(entity.startDateOfMonth);
				setting.startDayOfWeek = String.valueOf(entity.startDayOfWeek);
				setting.monthMark = String.valueOf(entity.monthMark);
				setting.useRequest = entity.useRequest;
				setting.historyId = historyEntity.id;
				setting.name = historyEntity.nameL.getValue();
				setting.name_L0 = historyEntity.nameL0;
				setting.name_L1 = historyEntity.nameL1;
				setting.name_L2 = historyEntity.nameL2;
				setting.validDateFrom = AppDate.convertDate(historyEntity.validFrom);
				setting.validDateTo = AppDate.convertDate(historyEntity.validTo);

				timeSettingList.add(setting);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = timeSettingList;
			return res;
		}
	}

}
