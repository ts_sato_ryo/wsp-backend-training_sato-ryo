/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpRequestApprovalResource
 */
@isTest
private class ExpRequestApprovalResourceTest {
	
	/**
	 * Confirm error happens when expense module is not available
	 * (SubmitExpRequestApi)
	 */
	@isTest static void SubmitExpRequestApiTestCanNotUse() {

		ExpTestData testData = new ExpTestData();
		// Turn off UseExpense Request flag
		testData.changeUseExpenseRequest(false);
		ExpRequestEntity report = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		Test.startTest();

		// Request parameter
		ExpRequestApprovalResource.SubmitRequestParam param = new ExpRequestApprovalResource.SubmitRequestParam();
		param.reportId = report.id;
		param.comment = 'Comment';

		try {
			// Execute API
			ExpRequestApprovalResource.SubmitExpRequestApprovalApi api = new ExpRequestApprovalResource.SubmitExpRequestApprovalApi();
			api.execute(param);

			System.assert(false, '例外が発生しませんでした。');
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_CANNOT_USE_EXPENSE_REQUEST, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Test that Expense Request Approval submission can be recalled
	 */
	@isTest
	static void CancelExpRequestApprovalApiTest() {
		ExpTestData testData = new ExpTestData();
		// Create Expense Request
		ExpRequestEntity request = testData.createExpRequest(Date.today(), testData.department, testData.employee, testData.job);

		// Link Currently Report Type to Employee Group
		testData.linkExpReportTypeExpEmpGroup(testData.employeeGroup.Id, testData.expReportType.Id);

		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpRequestApprovalResource.SubmitRequestParam param = new ExpRequestApprovalResource.SubmitRequestParam();
		param.reportId = request.id;
		param.comment = 'Submit Sample Comment';

		ExpRequestApprovalResource.SubmitExpRequestApprovalApi api = new ExpRequestApprovalResource.SubmitExpRequestApprovalApi();
		// Since the purpose is to test the Submit Cancellation, no need to do Validation
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		api.execute(param);
		// Reset Bypass Validation
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = false;

		ExpRequestApproval__c requestObj = [
			SELECT
				Id,
				Name,
				ExpRequestId__c,
				TotalAmount__c,
				RequestNo__c,
				Subject__c,
				Comment__c,
				Status__c,
				ConfirmationRequired__c,
				CancelType__c,
				EmployeeHistoryId__c,
				RequestTime__c,
				DepartmentHistoryId__c,
				ActorHistoryId__c,
				Approver01Id__c
			FROM ExpRequestApproval__c
			WHERE ExpRequestId__c = :request.id];

		Test.startTest();

		ExpRequestApprovalResource.CancelExpRequestApprovalParam cancelParam = new ExpRequestApprovalResource.CancelExpRequestApprovalParam();
		cancelParam.requestId = requestObj.Id;
		cancelParam.comment = 'Sample Cancel Comment';

		ExpRequestApprovalResource.CancelExpRequestApprovalApi cancelApi = new ExpRequestApprovalResource.CancelExpRequestApprovalApi();
		cancelApi.execute(cancelParam);

		// Since the ProcessRepository is only processed for Non-test, update it manually
		ExpRequestApprovalRepository repo = new ExpRequestApprovalRepository();
		ExpRequestApprovalEntity approvalEntity = repo.getEntity(requestObj.Id);
		approvalEntity.status = AppRequestStatus.DISABLED;
		approvalEntity.cancelType = AppCancelType.REMOVED;
		repo.saveEntity(approvalEntity);

		Test.stopTest();

		ExpRequestApproval__c expRequestApproval = [
			SELECT
				Id,
				Status__c,
				CancelType__c,
				ConfirmationRequired__c,
				CancelComment__c
			FROM ExpRequestApproval__c
			WHERE id = :requestObj.Id
			LIMIT 1];

		System.assertEquals(true, expRequestApproval.ConfirmationRequired__c);
	}
}