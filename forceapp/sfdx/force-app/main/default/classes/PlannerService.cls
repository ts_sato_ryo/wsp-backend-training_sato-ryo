/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group プランナー
 *
 * @description プランナーのサービスクラス
 */
public with sharing class PlannerService implements Service {

	private PlanEventRepository repository = new PlanEventRepository();

	/*
	 * 指定の条件に一致するイベントを、開始日時の昇順で取得する。
	 * 指定の期間に有効（終了していない）な予定を全て取得する。
	 * （対象開始日以降に開始した予定だけではなく、対象開始日に既に開始している予定も取得する）
	 *
	 * 引数以外は以下の条件で検索する。
	 * １．定期的な予定ではない
	 * ２．返答ステータスがnull、もしくは承認
	 *
	 * @param ownerId 所有者
	 * @param fromDate 対象開始日（以上）
	 * @param toDate 対象終了日（以下）
	 * @return イベント一覧（該当するレコードが存在しない場合は、空のリスト）
	 */
	public List<PlanEventEntity> searchEvents(Id ownerId, AppDate fromDate, AppDate toDate) {
		PlanEventRepository.SearchFilter filter = new PlanEventRepository.SearchFilter();
		filter.ownerIds = new Set<Id>{ownerId};
		filter.startDateTime = Datetime.newInstance(fromDate.getDate(), Time.newInstance(0, 0, 0, 0));
		filter.endDateTime = Datetime.newInstance(toDate.getDate(), Time.newInstance(23, 59, 0, 0));
		filter.isOverwrap = true;
		return repository.searchEntityList(filter);
	}

	/**
	 * 社員の勤務体系に設定された1日の境界時刻を考慮した予定データを取得する（※未実装 現状は0:00〜23:59の予定を取得している）
	 * @param employee 社員
	 * @param targetDate 取得対象の日付
	 * @return イベント一覧（該当するレコードが存在しない場合は、空のリスト）
	 */
	public List<PlanEventEntity> searchEvents(EmployeeBaseEntity employee, AppDate targetDate) {
		PlanEventRepository.SearchFilter filter = new PlanEventRepository.SearchFilter();
		filter.ownerIds = new Set<Id>{employee.userId};
		filter.startDateTime = Datetime.newInstance(targetDate.getDate(), Time.newInstance(0, 0, 0, 0));
		filter.endDateTime = Datetime.newInstance(targetDate.getDate(), Time.newInstance(23, 59, 0, 0));
		filter.isOverwrap = true;
		return repository.searchEntityList(filter);
	}

	/**
	 * 指定したIDのエンティティを検索する
	 * @param eventId 取得対象のID
	 * @return 検索結果 該当するレコードが存在しない場合はnull
	 */
	public PlanEventEntity getEntity(Id eventId) {
		return new PlanEventRepository().getEntity(eventId);
	}

	/*
	 * イベントを1件保存する。
	 * entity.idが設定されている場合はレコードを更新し、設定されていない場合は登録する。
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果のレコードID
	 */
	public Id saveEvent(PlanEventEntity entity) {
		Repository.SaveResult result = repository.saveEntity(entity);
		return result.details[0].id;
	}

	/*
	 * イベントを1件削除する。
	 *
	 * 削除前に最終更新サービス、最終更新日時を設定し、削除を行う。
	 * 「NOTE: カレンダー同期機能のため、削除する予定に対しても以下の項目の更新が必要」とコメントが残っていたが仕様が不明。
	 * 機能を追加する過程で、不要なら削除を検討する。
	 * @param entity 保存対象のエンティティ
	 * @return 削除結果のレコードID
	 */
	public Id deleteEvent(Id id) {
		PlanEventEntity entity = repository.getEntity(id);
		entity.lastModifiedService = PlanEventEntity.ServiceType.teamspirit;
		entity.lastModifiedDateTime = AppDatetime.now();
		saveEvent(entity);

		Repository.DeleteResult result = repository.deleteEntity(entity);
		return result.details[0].id;
	}
}