/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数設定のリポジトリ
 */
public with sharing class TimeSettingRepository extends ParentChildRepository {

	/**
	 * 履歴リストを取得する際の並び順
	 */
	public enum HistorySortOrder {
		VALID_FROM_ASC, VALID_FROM_DESC
	}

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無 */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** 工数設定ベースのキャッシュ */
	private static final Repository.Cache BASE_CACHE = new Repository.Cache();
	/** 工数設定履歴のキャッシュ */
	private static final Repository.Cache HISTORY_CACHE = new Repository.Cache();

	/** 会社のリレーション名 */
	private static final String COMPANY_R = TimeSettingBase__c.CompanyId__c.getDescribe().getRelationshipName();
	/** 部署ベースのリレーション名 */
	private static final String HISTORY_BASE_R = ComDeptHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/**
	 * 取得対象のベースオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> baseFieldList = new Set<Schema.SObjectField> {
			TimeSettingBase__c.Id,
			TimeSettingBase__c.Name,
			TimeSettingBase__c.CurrentHistoryId__c,
			TimeSettingBase__c.Code__c,
			TimeSettingBase__c.UniqKey__c,
			TimeSettingBase__c.CompanyId__c,
			TimeSettingBase__c.SummaryPeriod__c,
			TimeSettingBase__c.StartDateOfMonth__c,
			TimeSettingBase__c.StartDayOfWeek__c,
			TimeSettingBase__c.MonthMark__c,
			TimeSettingBase__c.UseRequest__c
		};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(baseFieldList);
	}

	/**
	 * 取得対象の履歴オブジェクトの項目名
	 * 項目が追加されたらhistoryFieldSetに追加してください
	 */
	private static final List<String> GET_HISTORY_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> historyFieldSet = new Set<Schema.SObjectField> {
			TimeSettingHistory__c.Id,
			TimeSettingHistory__c.BaseId__c,
			TimeSettingHistory__c.UniqKey__c,
			TimeSettingHistory__c.ValidFrom__c,
			TimeSettingHistory__c.ValidTo__c,
			TimeSettingHistory__c.HistoryComment__c,
			TimeSettingHistory__c.Removed__c,
			TimeSettingHistory__c.Name,
			TimeSettingHistory__c.Name_L0__c,
			TimeSettingHistory__c.Name_L1__c,
			TimeSettingHistory__c.Name_L2__c
		};

		GET_HISTORY_FIELD_NAME_LIST = Repository.generateFieldNameList(historyFieldSet);
	}

	/**
	 * コンストラクタ
	 */
	public TimeSettingRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * Specify the use of cache and create instance.
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public TimeSettingRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/**
	 * 指定したエンティティを取得する(ベース+全ての履歴)
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティと、ベースに紐づく全ての履歴エンティティ
	 */
	public TimeSettingBaseEntity getEntity(Id baseId) {
		return getEntity(baseId, null);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public TimeSettingBaseEntity getEntity(Id baseId, AppDate targetDate) {

		List<TimeSettingBaseEntity> entities = getEntityList(new List<Id>{baseId}, targetDate);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseIds 取得対象のベースIDのリスト、nullの場合は全てが取得対象となる
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、履歴が存在しない場合はnull
	 */
	public List<TimeSettingBaseEntity> getEntityList(List<Id> baseIds, AppDate targetDate) {
		TimeSettingRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new Set<Id>(baseIds);
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param codes 取得対象の工数設定コードのリスト、nullの場合は全てが取得対象となる
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したコードのベースエンティティと、対象日に有効な履歴エンティティのリスト。ただし、履歴が存在しない場合はnull
	 */
	public List<TimeSettingBaseEntity> getEntityListByCode(List<String> codes, AppDate targetDate) {
		TimeSettingRepository.SearchFilter filter = new SearchFilter();
		filter.codes = new Set<String>(codes);
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public TimeSettingBaseEntity getBaseEntity(Id baseId) {
		return getBaseEntity(baseId, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public TimeSettingBaseEntity getBaseEntity(Id baseId, Boolean forUpdate) {
		List<TimeSettingBaseEntity> bases = getBaseEntityList(new List<Id>{baseId}, forUpdate);
		if (bases.isEmpty()) {
			return null;
		}
		return bases[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseIds 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティのリスト。履歴は含まない
	 */
	public List<TimeSettingBaseEntity> getBaseEntityList(List<Id> baseIds, Boolean forUpdate) {
		TimeSettingRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.baseIds = new List<Id>(baseIds);
		return searchBaseEntity(filter, forUpdate);
	}

	/**
	 * 指定したコードの工数設定基本情報を取得する(ベースのみ)
	 * @param codes 工数設定のコードのリスト
	 * @return AttWorkingTypeEntityのリスト
	 */
	public List<TimeSettingBaseEntity> getBaseEntityListByCode(List<String> codes) {
		TimeSettingRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.codes = new Set<String>(codes);

		return searchBaseEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyId 取得対象の履歴ID
	 * @return 履歴エンティティ。ただし、存在しない場合はnull
	 */
	public TimeSettingHistoryEntity getHistoryEntity(Id historyId) {
		List<TimeSettingHistoryEntity> entities = getHistoryEntityList(new List<id>{historyId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyIds 取得対象の履歴ID
	 * @return 履歴エンティティのリスト。
	 */
	public List<TimeSettingHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		TimeSettingRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new Set<Id>(historyIds);

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する。有効開始日の昇順
	 */
	public List<TimeSettingBaseEntity> searchEntity(SearchFilter filter) {
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @return IDをキーにした検索結果のエンティティマップ。検索の結果、履歴を1件以上持つエンティティのみ返却する。有効開始日の昇順
	 */
	public Map<Id, TimeSettingBaseEntity> searchEntityMap(SearchFilter filter) {
		Map<Id, TimeSettingBaseEntity> settingMap = new Map<Id, TimeSettingBaseEntity>();

		List<TimeSettingBaseEntity> settingList = searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);

		if (settingList == null || settingList.isEmpty()) {
			return settingMap;
		}

		for (TimeSettingBaseEntity setting :  settingList) {
			settingMap.put(setting.id, setting);
		}
		return settingMap;
	}

	/** 検索フィルタ(履歴の検索時に使用します) */
	public class SearchFilter {
		/** 工数設定ベースID */
		public Set<Id> baseIds;
		/** 工数履歴ID */
		public Set<Id> historyIds;
		/** 会社ID */
		public Set<Id> companyIds;
		/** 取得対象日 */
		public AppDate targetDate;
		/** コード(完全一致) */
		public Set<String> codes;

		/** 論理削除されている履歴を取得対象にする場合はtrue */
		public Boolean includeRemoved;
		/**
		 * 取得対象日範囲
		 * 開始日から終了日のうち、1日でも有効な工数設定があれば取得対象とする。
		 * 終了日に有効な工数設定も含む。
		 */
		public AppDateRange dateRange;
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストの並び順
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する
	 */
	public List<TimeSettingBaseEntity> searchEntityWithHistory(SearchFilter filter, Boolean forUpdate,
			TimeSettingRepository.HistorySortOrder sortOrder) {

		// 履歴を検索
		List<TimeSettingHistoryEntity> histories = searchHistoryEntity(filter, forUpdate, sortOrder);

		// 取得対象のベースIDリストを作成
		Set<Id> targetBaseIds = new Set<Id>();
		for (TimeSettingHistoryEntity history : histories) {
			targetBaseIds.add(history.baseId);
		}

		// ベースエンティティを取得する
		SearchBaseFilter baseFilter = new SearchBaseFilter();
		baseFilter.baseIds = new List<Id>(targetBaseIds);
		List<TimeSettingBaseEntity> bases = searchBaseEntity(baseFilter, forUpdate);

		// ベースに履歴を追加するためMapに変換する
		Map<Id, TimeSettingBaseEntity> baseMap = new Map<Id, TimeSettingBaseEntity>();
		for (TimeSettingBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		// ベースエンティティに履歴を追加する
		for (TimeSettingHistoryEntity history : histories) {
			TimeSettingBaseEntity base = baseMap.get(history.baseId);
			base.addHistory(history);
		}

		return bases;
	}

	/** ベース用の検索フィルタ */
	public class SearchBaseFilter {
		/** 工数設定ベースID */
		public List<Id> baseIds;
		/** コード(完全一致) */
		public Set<String> codes;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	private List<TimeSettingBaseEntity> searchBaseEntity(SearchBaseFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && BASE_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchBaseEntity ReturnCache key:' + cacheKey);
			List<TimeSettingBaseEntity> entities = new List<TimeSettingBaseEntity>();
			for (TimeSettingBase__c sObj : (List<TimeSettingBase__c>)BASE_CACHE.get(cacheKey)) {
				entities.add(createBaseEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchBaseEntity NoCache key:' + cacheKey);

		// ベース WHERE句
		List<String> baseWhereList = new List<String>();
		// ベースIDで検索
		List<Id> pIds;
		if (filter.baseIds != null) {
			pIds = filter.baseIds;
			baseWhereList.add('Id IN :pIds');
		}
		// コードで検索(完全一致)
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			baseWhereList.add(getFieldName(TimeSettingBase__c.Code__c) + ' IN :pCodes');
		}
		String whereString = buildWhereString(baseWhereList);


		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// ベース項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 関連項目（会社）
		selectFieldList.add(getFieldName(COMPANY_R, ComCompany__c.Code__c));

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + TimeSettingBase__c.SObjectType.getDescribe().getName()
				+ whereString;
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(TimeSettingBase__c.Code__c);
		}

		System.debug('TimeSettingRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<TimeSettingBase__c> sObjs = (List<TimeSettingBase__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(TimeSettingBase__c.SObjectType);

		// SObjectからエンティティを作成
		List<TimeSettingBaseEntity> entities = new List<TimeSettingBaseEntity>();
		for (TimeSettingBase__c sObj : sObjs) {
			entities.add(createBaseEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			BASE_CACHE.put(cacheKey, sObjs);
		}
		return entities;
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストのソート順
	 * @return 履歴エンティティの検索
	 */
	public List<TimeSettingHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, HistorySortOrder sortOrder) {
		// バインド変数が必要なため、クエリ作成処理をモジュール化していない

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter, sortOrder);
		if (isEnabledCache && !forUpdate && HISTORY_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity ReturnCache key:' + cacheKey);
			List<TimeSettingHistoryEntity> entities = new List<TimeSettingHistoryEntity>();
			for (TimeSettingHistory__c sObj : (List<TimeSettingHistory__c>)HISTORY_CACHE.get(cacheKey)) {
				entities.add(createHistoryEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity NoCache key:' + cacheKey);

		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 履歴項目
		selectFieldList.addAll(GET_HISTORY_FIELD_NAME_LIST);

		// WHERE句
		List<String> whereList = new List<String>();
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(TimeSettingHistory__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(TimeSettingHistory__c.ValidTo__c) + ' > :pTargetDate');
		}
		// 対象日範囲
		Date pStartDate;
		Date pEndDate;
		if (filter.dateRange != null) {
			pStartDate = filter.dateRange.startDate.getDate();
			pEndDate = filter.dateRange.endDate.getDate();
			whereList.add(getFieldName(TimeSettingHistory__c.ValidTo__c) + ' > :pStartDate');
			whereList.add(getFieldName(TimeSettingHistory__c.ValidFrom__c) + ' <= :pEndDate');
		}
		// 履歴IDで検索
		Set<Id> pHistoryIds;
		if(filter.historyIds != null){
			pHistoryIds = filter.historyIds;
			whereList.add(getFieldName(TimeSettingHistory__c.Id) + ' IN :pHistoryIds');
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			whereList.add(getFieldName(TimeSettingHistory__c.Removed__c) + ' = false');
		}
		// ベースIDで検索
		Set<Id> pBaseIds;
		if (filter.baseIds != null) {
			pBaseIds = filter.baseIds;
			whereList.add(getFieldName(HISTORY_BASE_R, TimeSettingBase__c.Id) + ' IN :pBaseIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(HISTORY_BASE_R, TimeSettingBase__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(HISTORY_BASE_R, TimeSettingBase__c.Code__c) + ' IN :pCodes');
		}


		// ORDER BY句
		String orderBy = ' ORDER BY ' + getFieldName(HISTORY_BASE_R, TimeSettingBase__c.Code__c);
		if (sortOrder == HistorySortOrder.VALID_FROM_ASC) {
			orderBy += ', ' + getFieldName(TimeSettingHistory__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == HistorySortOrder.VALID_FROM_DESC) {
			orderBy += ', ' + getFieldName(TimeSettingHistory__c.ValidFrom__c) + ' DESC NULLS LAST';
		}

		// クエリ作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM TimeSettingHistory__c' +
				+ buildWhereString(whereList);
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += orderBy;
		}
		System.debug('TimeSettingRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<TimeSettingHistory__c> sObjs = (List<TimeSettingHistory__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(TimeSettingHistory__c.SObjectType);
		List<TimeSettingHistoryEntity> entities = new List<TimeSettingHistoryEntity>();
		for (TimeSettingHistory__c sObj : sObjs) {
			entities.add(createHistoryEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			HISTORY_CACHE.put(cacheKey, sObjs);
		}
		return entities;
	}


	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private TimeSettingBaseEntity createBaseEntity(TimeSettingBase__c baseObj) {
		return new TimeSettingBaseEntity(baseObj);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private TimeSettingHistoryEntity createHistoryEntity(TimeSettingHistory__c historyObj) {
		return new TimeSettingHistoryEntity(historyObj);
	}

	/**
	 * ベースエンティティからオブジェクトを作成する（各リポジトリで実装する）
	 */
	protected override SObject createBaseObj(ParentChildBaseEntity superBaseEntity) {
		return ((TimeSettingBaseEntity)superBaseEntity).createSObject();
	}

	/**
	 * 履歴エンティティからオブジェクトを作成する
	 * @param historyEntity 作成元の履歴エンティティ
	 * @return 作成した履歴オブジェクト
	 */
	protected override SObject createHistoryObj(ParentChildHistoryEntity superHistoryEntity) {
		return ((TimeSettingHistoryEntity)superHistoryEntity).createSObject();
	}

	/**
	 * ベースオブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone) {
		List<TimeSettingBase__c> deptList = new List<TimeSettingBase__c>();
		for (SObject obj : baseObjList) {
			deptList.add((TimeSettingBase__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * 履歴オブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone) {
		List<TimeSettingHistory__c> deptList = new List<TimeSettingHistory__c>();
		for (SObject obj : historyObjList) {
			deptList.add((TimeSettingHistory__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}
}
