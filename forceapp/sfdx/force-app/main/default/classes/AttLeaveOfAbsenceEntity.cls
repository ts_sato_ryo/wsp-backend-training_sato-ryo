/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * 休職休業マスタのエンティティ
  */
public with sharing class AttLeaveOfAbsenceEntity extends LogicalDeleteEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 休職休業名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		COMPANY_ID,
		CODE,
		UNIQUE_KEY,
		NAME_L0,
		NAME_L1,
		NAME_L2
	}

	/** 値を変更した項目のリスト */
	private Set<Field> changedFieldSet;

	/**
	 * コンストラクタ
	 */
	public AttLeaveOfAbsenceEntity() {
		changedFieldSet = new Set<Field>();
	}

	/** 休職休業名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** 休職休業コード **/
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	/** ユニークキー */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQUE_KEY);
		}
	}

	/** 休職休業名(多言語対応。参照専用) */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	/** 休職休業名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	/** 休職休業名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	/** 休職休業名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/** 会社レコードID */
	public String companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		changedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return changedFieldSet.contains(field);
	}

	/**
	 * (親クラスを含まない)クラス内の項目変更情報をリセットする
	 */
	protected override void resetChangedInSubClass() {
		this.changedFieldSet.clear();
	}

	/**
	 * ユニークキーを作成する
	 * コードを指定しておくこと。
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]休職休業のユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]休職休業のユニークキーが作成できませんでした。コードが設定されていません。');
		}
		// 会社コード + '-' + コード
		return companyCode + '-' + this.code;
	}

	/**
	 * エンティティの複製を作成する(IDはコピーされません)
	 */
	public AttLeaveOfAbsenceEntity copy() {
		AttLeaveOfAbsenceEntity copyEntity = new AttLeaveOfAbsenceEntity();
		for (AttLeaveOfAbsenceEntity.Field field : AttLeaveOfAbsenceEntity.Field.values()) {
			Object value = this.getFieldValue(field);
			if (value instanceof List<Object>) {
				List<Object> listObj = (List<Object>)value;
				copyEntity.setFieldValue(field, listObj.clone());
			} else {
				copyEntity.setFieldValue(field, value);
			}
		}
		for (LogicalDeleteEntity.Field field : LogicalDeleteEntity.Field.values()) {
			copyEntity.setFieldValue(field, this.getFieldValue(field));
		}
		return copyEntity;
	}

	/**
	 * 指定された項目の値を取得する
	 * @param targetField 対象の項目
	 * @return 項目値
	 */
	public Object getFieldValue(AttLeaveOfAbsenceEntity.Field targetField) {
		if (targetField == Field.NAME) {
			return this.name;
		}
		if (targetField == Field.CODE) {
			return this.code;
		}
		if (targetField == Field.UNIQUE_KEY) {
			return this.uniqKey;
		}
		if (targetField == Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == Field.NAME_L0) {
			return this.nameL0;
		}
		if (targetField == Field.NAME_L1) {
			return this.nameL1;
		}
		if (targetField == Field.NAME_L2) {
			return this.nameL2;
		}
		// ここにきたらバグ
		throw new App.UnsupportedException('AttLeaveOfAbsenceEntity.getFieldValue: 未対応の項目です。targetField='+ targetField);
	}

	/**
	 * 指定された項目に値を設定する
	 * @param targetField 対象の項目
	 * @pram value 設定値
	 */
	public void setFieldValue(AttLeaveOfAbsenceEntity.Field targetField, Object value) {
		if (targetField == Field.CODE) {
			this.code = (String)value;
		} else if (targetField == Field.NAME) {
			this.name = (String)value;
		} else if (targetField == Field.UNIQUE_KEY) {
			this.uniqKey = (String)value;
		} else if (targetField == Field.COMPANY_ID) {
			this.companyId = (String)value;
		} else if (targetField == Field.NAME_L0) {
			this.nameL0 = (String)value;
		} else if (targetField == Field.NAME_L1) {
			this.nameL1 = (String)value;
		} else if (targetField == Field.NAME_L2) {
			this.nameL2 = (String)value;
		} else {
			// ここにきたらバグ
			throw new App.UnsupportedException('AttLeaveOfAbsenceEntity.setFieldValue: 未対応の項目です。targetField='+ targetField);
		}
	}
}