/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Resource class providing APIs of Expense Record
 */
public with sharing class ExpRecordResource {

	/**
	 * Parameter for saving expense record
	 *   Base fields are inherited from ExpService.ExpRecord
	 */
	public class SaveExpRecordParam extends ExpService.ExpRecordParam implements RemoteApi.RequestParam {
		// Employee base ID
		public String empId;
		/** ID of Expense Report the Record Belongs To */
		public String reportId;
		/** ID of Expense Report Request the Record Belongs To */
		public String requestId;
		/** Report Type Id */
		public String reportTypeId;

		public SaveExpRecordParam(ExpRecordEntity record, AppLanguage lang) {
			super(record, lang);
		}
	}

	/** Parameter for deleting expense record */
	public class DeleteExpRecordParam implements RemoteApi.RequestParam {
		// Record ID
		public List<String> recordIds;
	}

	/**
   * Parameter for getting a list of expense records
   */
	public class GetExpRecordListParam implements RemoteApi.RequestParam {
		// Employee base ID
		public String empId;
		// Avaliable Expense Type in a report
		public String[] expTypeIdList;
		// Accounting period start date
		public String startDate;
		// Accounting period end date
		public String endDate;

		public void validate() {
			ExpCommonUtil.validateId('empId', this.empId, true);
			ExpCommonUtil.validateIdList('expTypeIdList', this.expTypeIdList, false);
			ExpCommonUtil.validateDate('startDate', this.startDate, false);
			ExpCommonUtil.validateDate('endDate', this.endDate, false);
		}
	}

	/** Parameter for Cloning Expense Record */
	public class CloneExpRecordsParam implements RemoteApi.RequestParam {
		/** Record IDs */
		public List<String> recordIds;
		/** List of Date to clone the Records to */
		public List<String> targetDates;
	}

	/**
	 * Result of saving expense record
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
	  // Saved record ID
		public String recordId;

		public SaveResult(Id recordId) {
			this.recordId = recordId;
		}
	}

	/**
	 * Result of getting expense record
	 */
	public class GetExpRecordListResult implements RemoteApi.ResponseParam {
		// List of record item
		public List<ExpService.ExpRecordParam> records;
	}

	/*
	 * Result of Cloning API
	 */
	public class CloneResult implements RemoteApi.ResponseParam {
		/** IDs of newly created records */
		public List<String> recordIds;
		/** Records Updated with new Value */
		public List<ExpParam.UpdatedClonedRecordResult> updatedRecords;
	}

	/**
	 * API to save Expense Record data
	 */
	public with sharing class SaveExpRecordApi extends RemoteApi.ResourceBase {
		private final String FINANCE_APPROVAL = 'finance-approval';

		@TestVisible
		private Boolean isRollbackTest = false;
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SaveExpRecordParam param = (SaveExpRecordParam)req.getParam(SaveExpRecordParam.class);

			Boolean isFinanceApproval = isFinanceApproval(req);

			validateParam(param, isFinanceApproval);

			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(param.empId);

			// Check if the employee can use Expense module
			ExpCommonUtil.canUseExpense(empBase);

			// Create ExpRecord entity
			// NOTE: Order for ExpRecordItems (including existing ones) are set upon creation of ExpRecord entity
			ExpRecordEntity record = param.createEntity();
			record.expReportId = param.reportId;

			Savepoint sp = Database.setSavepoint();
			Id reportTypeId = String.isNotEmpty(param.reportTypeId) ? Id.valueOf(param.reportTypeId) : null;
			Id recordId;
			try {
				recordId = new ExpRecordService().saveExpRecord(empBase, record, param.requestId, reportTypeId, isFinanceApproval);
				if (isRollbackTest) {
					throw new App.IllegalStateException('SaveExpReportApi Rollback Test');
				}
			} catch(Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return new SaveResult(recordId);
		}

		/*
		 * Validate the SaveExpRecordParam param
		 * @param param SaveExpRecordParam containing the data to save
		 * @param isFinanceApproval indicate whether it's for Finance Approval
		 */
		private void validateParam(SaveExpRecordParam param, Boolean isFinanceApproval) {
			// For Finance Approval, these 2 ID are required
			ExpCommonUtil.validateId('reportId', param.reportId, isFinanceApproval);
			ExpCommonUtil.validateId('requestId', param.requestId, isFinanceApproval);

			// Validate Job ID and Cost Center History ID (not required)
			ExpCommonUtil.validateId('jobId', param.items[0].jobId, false);
			ExpCommonUtil.validateId('costCenterHistoryId', param.items[0].costCenterHistoryId, false);

			if (param.items != null) {
				for (ExpService.ExpRecordItemParam item : param.items) {
					// Extended Item Ids
					item.verifyExtendedItemIds();
					// Extended Item Date Values (if specified, check if the date is valid)
					item.verifyExtendedItemDateValues();
				}
			}
		}

		/**
		 * Check is Finance Approval from request param
 		 * @param req Request param
 		 * @return true if it is finance approval
 		 */
		private Boolean isFinanceApproval(RemoteApi.Request req) {
			return ((req.getPath() != null) && req.getPath().contains(FINANCE_APPROVAL));
		}
	}

	/**
	 * API to get Expense Record list
	 */
	public with sharing class GetExpRecordListApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			GetExpRecordListParam param = (GetExpRecordListParam)req.getParam(GetExpRecordListParam.class);
			param.validate();

			EmployeeBaseEntity empBase = ExpCommonUtil.getEmployeeBaseEntity(param.empId);

			// Check if the employee can use Expense module
			ExpCommonUtil.canUseExpense(empBase);

			AppDate startDate = String.isEmpty(param.startDate) ? null : AppDate.parse(param.startDate);

			AppDate endDate = String.isEmpty(param.endDate) ? null : AppDate.parse(param.endDate);

			List<ExpRecordEntity> recordList = new ExpRecordService().getNoReportExpRecordList(
					empBase.id, param.expTypeIdList, startDate, endDate);

			// Set Extended Item info
			ExpService service = new ExpService();
			service.setExtendedItemInfoToRecords(recordList);
			service.setSelectedExtendedItemCustomOptionName(null, recordList);
			if (!recordList.isEmpty()) {
				ExpService.setFileAttachmentInfoToRecords(recordList);
				ExpService.setReceiptFiles(recordList);
			}

			// Get language
			AppLanguage lang = AppLanguage.getUserLang();

			return makeResponse(recordList, lang);
		}

		/**
		 * Make response from entity
		 */
		private GetExpRecordListResult makeResponse(List<ExpRecordEntity> recordList, AppLanguage lang) {
			List<ExpService.ExpRecordParam> records = new List<ExpService.ExpRecordParam>();

			for (ExpRecordEntity entity : recordList) {
				ExpService.ExpRecordParam record = new ExpService.ExpRecordParam(entity, lang);
				records.add(record);
			}

			GetExpRecordListResult response = new GetExpRecordListResult();
			response.records = records;
			return response;
		}

	}

	/**
   * API to get Expense Record list
   */
	public with sharing class DeleteExpRecordApi extends RemoteApi.ResourceBase {
		@TestVisible
		private Boolean isRollbackTest = false;
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			DeleteExpRecordParam param = (DeleteExpRecordParam)req.getParam(DeleteExpRecordParam.class);

			ExpCommonUtil.validateIdList('recordIds', param.recordIds, true);
			ExpCommonUtil.canUseExpense();

			Savepoint sp = Database.setSavepoint();
			try {
				new ExpRecordService().deleteExpRecords(param.recordIds);
				if (isRollbackTest) {
					throw new App.IllegalStateException('DeleteExpRecordApi Rollback Test');
				}
			} catch(Exception e) {
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}

	/*
	 * API to Clone Expense Records
	 */
	public with sharing class CloneExpRecordsApi extends RemoteApi.ResourceBase {
		@TestVisible
		private Boolean isRollbackTest = false;
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			CloneExpRecordsParam param = (CloneExpRecordsParam) req.getParam(CloneExpRecordsParam.class);

			ExpCommonUtil.validateIdList('recordIds', param.recordIds, true);
			if (param.targetDates == null || param.targetDates.isEmpty()) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{'targetDates'}));
			}
			for (String targetDate : param.targetDates) {
				ExpCommonUtil.validateDate('targetDates', targetDate, true);
			}

			List<AppDate> targetDateToCreateClone = new List<AppDate>();
			for (String targetDate : param.targetDates) {
				targetDateToCreateClone.add(AppDate.valueOf(targetDate));
			}

			ExpCommonUtil.canUseExpense();

			Savepoint sp = Database.setSavepoint();
			try {
				Map<Id, ExpRecordEntity> resultMap = new ExpRecordService().cloneExpenseRecords(param.recordIds, targetDateToCreateClone);
				CloneResult result = new CloneResult();
				result.recordIds = new List<String>();
				result.updatedRecords = new List<ExpParam.UpdatedClonedRecordResult>();
				for (Id resultId : resultMap.keySet()) {
					result.recordIds.add(resultId);
					ExpRecordEntity recordEntity = resultMap.get(resultId);
					if (recordEntity != null) {
						ExpParam.UpdatedClonedRecordResult updatedRecord = new ExpParam.UpdatedClonedRecordResult();
						updatedRecord.recordId = recordEntity.id;
						// Take Item 0 as the itemization data are discarded in record clone.
						updatedRecord.expenseTypeName = recordEntity.recordItemList[0].expTypeName;
						updatedRecord.recordDate = recordEntity.recordDate.format();
						updatedRecord.isForeignCurrency = recordEntity.recordItemList[0].useForeignCurrency;
						result.updatedRecords.add(updatedRecord);
					}
				}
				if (isRollbackTest) {
					throw new App.IllegalStateException('CloneExpRecordsApi Rollback Test');
				}
				return result;
			} catch(Exception e) {
				Database.rollback(sp);
				throw e;
			}
		}
	}
}