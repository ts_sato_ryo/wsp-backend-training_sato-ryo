/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ExtendedItemResourceのテスト
 */
@isTest
private class ExtendedItemResourceTest {

	/**
	 * 本クラス内の全てのテストメソッドで利用するテストデータを登録する
	 */
	@testSetup static void setup() {
		// マスタデータを登録しておく
		TestData.setupMaster();
	}

	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

	}

	/**
	 * CreateApi のテスト
	 * 新規の拡張項目が保存できることを確認する
	 */
	@isTest static void createApiTestNormal() {
		ResourceTestData testData = new ResourceTestData();

		ExtendedItemResource.CreateRequest request = new ExtendedItemResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;
		request.inputType = ExtendedItemInputType.TEXT.value;
		request.limitLength = '255';
		request.defaultValueText = 'Default Value';
		request.description_L0 = 'Description_L0';
		request.description_L1 = 'Description_L1';
		request.description_L2 = 'Description_L2';

		// API実行
		ExtendedItemResource.CreateApi api = new ExtendedItemResource.CreateApi();
		ExtendedItemResource.CreateResponse response = (ExtendedItemResource.CreateResponse)api.execute(request);

		// 保存できていることを確認
		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(response.id);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(request.name_L0, savedItem.name);
		System.assertEquals(request.name_L0, savedItem.nameL0);
		System.assertEquals(request.name_L1, savedItem.nameL1);
		System.assertEquals(request.name_L2, savedItem.nameL2);
		System.assertEquals(request.code, savedItem.code);
		System.assertEquals(request.companyId, savedItem.companyId);
		System.assertEquals(request.inputType, ExtendedItemInputType.getValue(savedItem.inputType));
		System.assertEquals(request.limitLength, String.valueOf(savedItem.limitLength));
		System.assertEquals(request.defaultValueText, savedItem.defaultValueText);
		System.assertEquals(request.description_L0, savedItem.descriptionL0);
		System.assertEquals(request.description_L1, savedItem.descriptionL1);
		System.assertEquals(request.description_L2, savedItem.descriptionL2);
	}

	/**
	 * CreateApi のテスト
	 * 新規の拡張項目が保存できることを確認する
	 */
	@isTest static void createApiTestPickList() {
		ResourceTestData testData = new ResourceTestData();

		ExtendedItemResource.CreateRequest request = new ExtendedItemResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;
		request.inputType = ExtendedItemInputType.PICKLIST.value;
		request.picklistLabel_L0 = 'Option1_L0\nOption2_L0';
		request.picklistLabel_L1 = 'Option1_L1\nOption2_L1';
		request.picklistLabel_L2 = 'Option1_L2\nOption2_L2';
		request.picklistValue = 'Value1\nValue2';
		request.description_L0 = 'Description_L0';
		request.description_L1 = 'Description_L1';
		request.description_L2 = 'Description_L2';

		// API実行
		ExtendedItemResource.CreateApi api = new ExtendedItemResource.CreateApi();
		ExtendedItemResource.CreateResponse response = (ExtendedItemResource.CreateResponse)api.execute(request);

		// 保存できていることを確認
		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(response.id);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(request.name_L0, savedItem.name);
		System.assertEquals(request.name_L0, savedItem.nameL0);
		System.assertEquals(request.name_L1, savedItem.nameL1);
		System.assertEquals(request.name_L2, savedItem.nameL2);
		System.assertEquals(request.code, savedItem.code);
		System.assertEquals(request.companyId, savedItem.companyId);
		System.assertEquals(request.inputType, ExtendedItemInputType.getValue(savedItem.inputType));
		List<ExtendedItemEntity.PicklistOption> picklist = request.createPicklist(
				request.picklistLabel_L0.split(ExtendedItemResource.LINE_BREAK),
				request.picklistLabel_L1.split(ExtendedItemResource.LINE_BREAK),
				request.picklistLabel_L2.split(ExtendedItemResource.LINE_BREAK),
				request.picklistValue.split(ExtendedItemResource.LINE_BREAK));
		System.assertEquals(JSON.serialize(picklist), JSON.serialize(savedItem.picklist));
		System.assertEquals(request.description_L0, savedItem.descriptionL0);
		System.assertEquals(request.description_L1, savedItem.descriptionL1);
		System.assertEquals(request.description_L2, savedItem.descriptionL2);
	}

	/*
	 * Test that new Extended Item can be create with Custom input type.
	 */
	@IsTest static void createApiTestExtendedItemCustom() {
		ResourceTestData testData = new ResourceTestData();
		ComExtendedItemCustom__c extendedItemCustom = ComTestDataUtility.createExtendedItemCustoms('custom', testData.company.id, 1).get(0);

		ExtendedItemResource.CreateRequest request = new ExtendedItemResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;
		request.inputType = ExtendedItemInputType.CUSTOM.value;
		request.extendedItemCustomId = extendedItemCustom.Id;
		request.description_L0 = 'Description_L0';
		request.description_L1 = 'Description_L1';
		request.description_L2 = 'Description_L2';

		// API実行
		ExtendedItemResource.CreateApi api = new ExtendedItemResource.CreateApi();
		ExtendedItemResource.CreateResponse response = (ExtendedItemResource.CreateResponse)api.execute(request);

		// 保存できていることを確認
		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(response.id);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(request.name_L0, savedItem.name);
		System.assertEquals(request.name_L0, savedItem.nameL0);
		System.assertEquals(request.name_L1, savedItem.nameL1);
		System.assertEquals(request.name_L2, savedItem.nameL2);
		System.assertEquals(request.code, savedItem.code);
		System.assertEquals(request.companyId, savedItem.companyId);
		System.assertEquals(request.inputType, ExtendedItemInputType.getValue(savedItem.inputType));
		System.assertEquals(extendedItemCustom.Id, savedItem.extendedItemCustomId);
		System.assertEquals(request.description_L0, savedItem.descriptionL0);
		System.assertEquals(request.description_L1, savedItem.descriptionL1);
		System.assertEquals(request.description_L2, savedItem.descriptionL2);
	}

	/**
	 * ExtendedItemParam.createPicklist のテスト
	 * リクエストパラメータに不正な値が設定されている場合は想定されている例外が発生することを確認する
	 */
	@isTest static void createPickListTest() {
		App.ParameterException resEx;

		// Test1: ラベル(L0)が未設定
		resEx = null;
		try {
			String[] labelList_L0 = null;
			String[] labelList_L1 = new String[]{'label1_L1', 'label2_L1', 'label3_L1'};
			String[] labelList_L2 = new String[]{'label1_L2', 'label2_L2', 'label3_L2'};
			String[] valueList = new String[]{'value1', 'value2', 'value3'};

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Admin_Lbl_PickListLabel}), resEx.getMessage());

		// Test2: 値が未設定
		resEx = null;
		try {
			String[] labelList_L0 = new String[]{'label1_L0', 'label2_L0', 'label3_L0'};
			String[] labelList_L1 = new String[]{'label1_L1', 'label2_L1', 'label3_L1'};
			String[] labelList_L2 = new String[]{'label1_L2', 'label2_L2', 'label3_L2'};
			String[] valueList = null;

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Admin_Lbl_PickListValue}), resEx.getMessage());

		// Test3: 値が重複
		resEx = null;
		try {
			String[] labelList_L0 = new String[]{'label1_L0', 'label2_L0', 'label3_L0'};
			String[] labelList_L1 = new String[]{'label1_L1', 'label2_L1', 'label3_L1'};
			String[] labelList_L2 = new String[]{'label1_L2', 'label2_L2', 'label3_L2'};
			String[] valueList = new String[]{'value', 'value', 'value'};

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test3: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_DuplicateValue(new List<String>{ComMessage.msg().Admin_Lbl_PickListValue}), resEx.getMessage());

		// Test4: ラベル、値の数が合っていない
		resEx = null;
		try {
			String[] labelList_L0 = new String[]{'label1_L0', 'label2_L0', 'label3_L0'};
			String[] labelList_L1 = new String[]{'label1_L1', 'label2_L1', 'label3_L1'};
			String[] labelList_L2 = new String[]{'label1_L2', 'label2_L2', 'label3_L2'};
			String[] valueList = new String[]{'value1', '', 'value3'};

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test4: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Admin_Err_DifferentSizePicklist, resEx.getMessage());

		// Test5: 上限よりも多く指定されている
		resEx = null;
		try {
			Integer listSize = ExtendedItemEntity.PICKLIST_MAX_SIZE + 1;
			String[] labelList_L0 = new String[listSize];
			for (Integer i = 0; i < listSize; i++) {
				labelList_L0[i] = 'label_L0' + i;
			}
			String[] labelList_L1 = new String[]{'label1_L1', 'label2_L1', 'label3_L1'};
			String[] labelList_L2 = new String[]{'label1_L2', 'label2_L2', 'label3_L2'};
			String[] valueList = new String[listSize];
			for (Integer i = 0; i < listSize; i++) {
				valueList[i] = 'value' + i;
			}

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test5: 例外が発生しませんでした');
		String message = ComMessage.msg().Com_Err_MaxSizeOver(new List<String>{ComMessage.msg().Admin_Lbl_PickListValue, ExtendedItemEntity.PICKLIST_MAX_SIZE.format()});
		System.assertEquals(message, resEx.getMessage());

		// Test6: ラベル(L0)が文字数オーバー
		resEx = null;
		try {
			String[] labelList_L0 = new String[]{createString(ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH + 1)};
			String[] labelList_L1 = new String[]{'label1_L1'};
			String[] labelList_L2 = new String[]{'label1_L2'};
			String[] valueList = new String[]{'value1'};

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test6: 例外が発生しませんでした');
		message = ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{ComMessage.msg().Admin_Lbl_PickListLabel, ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH.format()});
		System.assertEquals(message, resEx.getMessage());

		// Test7: ラベル(L1)が文字数オーバー
		resEx = null;
		try {
			String[] labelList_L0 = new String[]{'label1_L0'};
			String[] labelList_L1 = new String[]{createString(ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH + 1)};
			String[] labelList_L2 = new String[]{'label1_L2'};
			String[] valueList = new String[]{'value1'};

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test7: 例外が発生しませんでした');
		message = ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{ComMessage.msg().Admin_Lbl_PickListLabel, ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH.format()});
		System.assertEquals(message, resEx.getMessage());

		// Test8: ラベル(L2)が文字数オーバー
		resEx = null;
		try {
			String[] labelList_L0 = new String[]{'label1_L0'};
			String[] labelList_L1 = new String[]{'label1_L1'};
			String[] labelList_L2 = new String[]{createString(ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH + 1)};
			String[] valueList = new String[]{'value1'};

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test8: 例外が発生しませんでした');
		message = ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{ComMessage.msg().Admin_Lbl_PickListLabel, ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH.format()});
		System.assertEquals(message, resEx.getMessage());

		// Test9: 値が文字数オーバー
		resEx = null;
		try {
			String[] labelList_L0 = new String[]{'label1_L0'};
			String[] labelList_L1 = new String[]{'label1_L1'};
			String[] labelList_L2 = new String[]{'label1_L2'};
			String[] valueList = new String[]{createString(ExtendedItemEntity.PICKLIST_LABEL_MAX_LENGTH + 1)};

			new ExtendedItemResource.ExtendedItemParam().createPicklist(labelList_L0, labelList_L1, labelList_L2, valueList);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test9: 例外が発生しませんでした');
		message = ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{ComMessage.msg().Admin_Lbl_PickListValue, ExtendedItemEntity.PICKLIST_VALUE_MAX_LENGTH.format()});
		System.assertEquals(message, resEx.getMessage());
	}

	/*
	 * Test that new Extended Item can be create with Date input type.
	 */
	@isTest static void createApiTestDateInputType() {
		ResourceTestData testData = new ResourceTestData();

		ExtendedItemResource.CreateRequest request = new ExtendedItemResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;
		request.inputType = ExtendedItemInputType.DATE_TYPE.value;
		request.description_L0 = 'Description_L0';
		request.description_L1 = 'Description_L1';
		request.description_L2 = 'Description_L2';

		// API実行
		ExtendedItemResource.CreateApi api = new ExtendedItemResource.CreateApi();
		ExtendedItemResource.CreateResponse response = (ExtendedItemResource.CreateResponse) api.execute(request);

		// 保存できていることを確認
		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(response.id);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(request.name_L0, savedItem.name);
		System.assertEquals(request.name_L0, savedItem.nameL0);
		System.assertEquals(request.name_L1, savedItem.nameL1);
		System.assertEquals(request.name_L2, savedItem.nameL2);
		System.assertEquals(request.code, savedItem.code);
		System.assertEquals(request.companyId, savedItem.companyId);
		System.assertEquals(request.inputType, ExtendedItemInputType.getValue(savedItem.inputType));
		System.assertEquals(request.description_L0, savedItem.descriptionL0);
		System.assertEquals(request.description_L1, savedItem.descriptionL1);
		System.assertEquals(request.description_L2, savedItem.descriptionL2);
	}

	/**
	 * CreateApi のテスト
	 * リクエストパラメータに不正な値が設定されている場合は想定されている例外が発生することを確認する
	 */
	@isTest static void createApiTestInvalidParam() {
		ResourceTestData testData = new ResourceTestData();

		ExtendedItemResource.CreateRequest request = new ExtendedItemResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;

		ExtendedItemResource.CreateApi api = new ExtendedItemResource.CreateApi();
		App.ParameterException resEx;

		// 会社IDにID以外の文字列が設定されている
		resEx = null;
		try {
			ExtendedItemResource.CreateRequest testRequest = request.clone();
			testRequest.companyId = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}), resEx.getMessage());
	}

	/**
	 * UpdateApi のテスト
	 * 既存の拡張項目が更新できることを確認する
	 */
	@isTest static void updateApiTestNormal() {
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		ExtendedItemResource.UpdateRequest request = new ExtendedItemResource.UpdateRequest();
		request.id = item.id;
		request.name_L0 = item.nameL0 + '_Update';
		request.name_L1 = item.nameL1 + '_Update';
		request.name_L2 = item.nameL2 + '_Update';
		request.code = item.code + '_Update';
		request.companyId = updateCompany.id;
		request.inputType = ExtendedItemInputType.TEXT.value;

		// API実行
		ExtendedItemResource.UpdateApi api = new ExtendedItemResource.UpdateApi();
		api.execute(request);

		// 保存できていることを確認
		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(item.id);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(request.name_L0, savedItem.name);
		System.assertEquals(request.name_L0, savedItem.nameL0);
		System.assertEquals(request.name_L1, savedItem.nameL1);
		System.assertEquals(request.name_L2, savedItem.nameL2);
		System.assertEquals(request.code, savedItem.code);
		System.assertEquals(request.companyId, savedItem.companyId);
	}

	/*
	 * Test that the extended item can be updated with Custom Extended Item
	 */
	@IsTest static void updateApiTestExtendedItemCustom() {
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		ExtendedItemEntity targetExtendedItem = testData.createExtendedItem('TargetExtendedItem');
		ComExtendedItemCustom__c extendedItemCustom = ComTestDataUtility.createExtendedItemCustoms('custom', testData.company.id, 1).get(0);

		ExtendedItemResource.UpdateRequest request = new ExtendedItemResource.UpdateRequest();
		request.id = targetExtendedItem.id;
		request.name_L0 = targetExtendedItem.nameL0 + '_Update';
		request.name_L1 = targetExtendedItem.nameL1 + '_Update';
		request.name_L2 = targetExtendedItem.nameL2 + '_Update';
		request.code = targetExtendedItem.code + '_Update';
		request.companyId = updateCompany.id;
		request.inputType = ExtendedItemInputType.CUSTOM.value;
		request.extendedItemCustomId = extendedItemCustom.Id;

		 // Execute API
		ExtendedItemResource.UpdateApi api = new ExtendedItemResource.UpdateApi();
		api.execute(request);

		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(targetExtendedItem.id);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(request.name_L0, savedItem.nameL0);
		System.assertEquals(request.name_L1, savedItem.nameL1);
		System.assertEquals(request.name_L2, savedItem.nameL2);
		System.assertEquals(request.code, savedItem.code);
		System.assertEquals(request.inputType, savedItem.inputType.value);

		// Check Extended Item Custom ID
		System.assertEquals(request.extendedItemCustomId, savedItem.extendedItemCustomId);
	}

	/*
	 * Test that the extended item Date can be updated
	 */
	@IsTest static void updateApiTestExtendedItemDate() {
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		ExtendedItemEntity targetExtendedItem = testData.createExtendedItem('TargetExtendedItem');
		ComExtendedItemCustom__c extendedItemCustom = ComTestDataUtility.createExtendedItemCustoms('custom', testData.company.id, 1).get(0);

		ExtendedItemResource.UpdateRequest request = new ExtendedItemResource.UpdateRequest();
		request.id = targetExtendedItem.id;
		request.name_L0 = targetExtendedItem.nameL0 + '_Update';
		request.name_L1 = targetExtendedItem.nameL1 + '_Update';
		request.name_L2 = targetExtendedItem.nameL2 + '_Update';
		request.code = targetExtendedItem.code + '_Update';
		request.companyId = updateCompany.id;
		request.inputType = ExtendedItemInputType.DATE_TYPE.value;

		// Execute API
		ExtendedItemResource.UpdateApi api = new ExtendedItemResource.UpdateApi();
		api.execute(request);

		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(targetExtendedItem.id);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(request.name_L0, savedItem.nameL0);
		System.assertEquals(request.name_L1, savedItem.nameL1);
		System.assertEquals(request.name_L2, savedItem.nameL2);
		System.assertEquals(request.code, savedItem.code);
		System.assertEquals(request.inputType, savedItem.inputType.value);
	}

	/**
	 * UpdateApi のテスト
	 * リクエストパラメータに不正な値が設定されている場合は想定されている例外が発生することを確認する
	 */
	@isTest static void updateApiTestInvalidParam() {
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		ExtendedItemResource.UpdateRequest request = new ExtendedItemResource.UpdateRequest();
		request.id = item.id;
		request.name_L0 = item.nameL0 + '_Update';
		request.name_L1 = item.nameL1 + '_Update';
		request.name_L2 = item.nameL2 + '_Update';
		request.code = item.code + '_Update';
		request.companyId = updateCompany.id;

		ExtendedItemResource.UpdateApi api = new ExtendedItemResource.UpdateApi();
		App.ParameterException resEx;

		// Test1: 会社IDにID以外の文字列が設定されている
		resEx = null;
		try {
			ExtendedItemResource.UpdateRequest testRequest = request.clone();
			testRequest.companyId = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}),
				resEx.getMessage());

		// Test2: 拡張項目IDが設定されていない
		resEx = null;
		try {
			ExtendedItemResource.UpdateRequest testRequest = request.clone();
			testRequest.id = null;
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());

		// Test3: 拡張項目IDにID以外の文字列が設定されている
		resEx = null;
		try {
			ExtendedItemResource.UpdateRequest testRequest = request.clone();
			testRequest.id = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test3: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());
	}

	/**
	 * DeleteApi のテスト
	 * 指定した拡張項目が削除できることを確認する
	 */
	@isTest static void deleteApiTest() {
		ResourceTestData testData = new ResourceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		ExtendedItemResource.DeleteRequest request = new ExtendedItemResource.DeleteRequest();
		request.id = item.id;

		// API実行
		ExtendedItemResource.DeleteApi api = new ExtendedItemResource.DeleteApi();
		api.execute(request);

		// 削除できていることを確認
		ExtendedItemEntity resItem = new ExtendedItemRepository().getEntity(item.id);
		System.assertEquals(null, resItem);
	}

	/**
	 * DeleteApi のテスト
	 * 削除対象の拡張項目が既に削除されている場合は例外が発生しないことを確認する
	 */
	@isTest static void deleteApiTestDeletedExtendedItem() {
		ResourceTestData testData = new ResourceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		ExtendedItemResource.DeleteRequest request = new ExtendedItemResource.DeleteRequest();
		request.id = item.id;

		// 削除しておく
		new ExtendedItemRepository().deleteEntity(item.id);

		// API実行
		ExtendedItemResource.DeleteApi api = new ExtendedItemResource.DeleteApi();
		api.execute(request);

		// 削除されていることを確認
		ExtendedItemEntity resItem = new ExtendedItemRepository().getEntity(item.id);
		System.assertEquals(null, resItem);
	}

	/**
	 * DeleteApi のテスト
	 * リクエストパラメータに不正な値が設定されている場合は想定されている例外が発生することを確認する
	 */
	@isTest static void deleteApiTestInvalidParam() {
		ResourceTestData testData = new ResourceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		ExtendedItemResource.DeleteRequest request = new ExtendedItemResource.DeleteRequest();
		request.id = item.id;

		ExtendedItemResource.DeleteApi api = new ExtendedItemResource.DeleteApi();
		App.ParameterException resEx;

		// Test1: 拡張項目IDが設定されていない
		resEx = null;
		try {
			ExtendedItemResource.DeleteRequest testRequest = request.clone();
			testRequest.id = null;
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());

		// Test2: 拡張項目IDにID以外の文字列が設定されている
		resEx = null;
		try {
			ExtendedItemResource.DeleteRequest testRequest = request.clone();
			testRequest.id = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());
	}

	/**
	 * SearchApi のテスト
	 * 指定した拡張項目IDの拡張項目が取得できることを確認する
	 */
	@isTest static void searchApiTestExtendedItemId() {
		ResourceTestData testData = new ResourceTestData();
		ExtendedItemEntity targetExtendedItem = testData.createExtendedItem('TargetExtendedItem');
		ExtendedItemEntity extendedItem1 = testData.createExtendedItem('TestExtendedItem1');
		ExtendedItemEntity extendedItem2 = testData.createExtendedItem('TestExtendedItem2');

		ExtendedItemResource.SearchRequest request = new ExtendedItemResource.SearchRequest();
		request.id = targetExtendedItem.id;

		// API実行
		ExtendedItemResource.SearchApi api = new ExtendedItemResource.SearchApi();
		ExtendedItemResource.SearchResponse response = (ExtendedItemResource.SearchResponse)api.execute(request);

		// 検索結果が正しいことを確認
		System.assertEquals(1, response.records.size());
		ExtendedItemResource.ExtendedItemParam resExtendedItem = response.records[0];
		verifySearchApiResult(targetExtendedItem, resExtendedItem);

		System.assertEquals(targetExtendedItem.limitLength, AppConverter.intValue(resExtendedItem.limitLength));
		System.assertEquals(targetExtendedItem.defaultValueText, resExtendedItem.defaultValueText);
	}

	/**
	 * SearchApi のテスト
	 * 指定した会社IDの拡張項目が取得できることを確認する
	 */
	@isTest static void searchApiTestCompanyId() {
		ResourceTestData testData = new ResourceTestData();
		ExtendedItemEntity targetExtendedItem1 = testData.createExtendedItem('TargetExtendedItem1');
		ExtendedItemEntity targetExtendedItem2 = testData.createExtendedItem('TargetExtendedItem2');
		ExtendedItemEntity extendedItem1 = testData.createExtendedItem('TestExtendedItem1');
		ExtendedItemEntity extendedItem2 = testData.createExtendedItem('TestExtendedItem2');

		// 取得対象の会社
		CompanyEntity targetCompany = testData.createCompany('検索Test');
		targetExtendedItem1.companyId = targetCompany.id;
		targetExtendedItem2.companyId = targetCompany.id;
		new ExtendedItemRepository().saveEntityList(new List<ExtendedItemEntity>{targetExtendedItem1, targetExtendedItem2});

		ExtendedItemResource.SearchRequest request = new ExtendedItemResource.SearchRequest();
		request.companyId = targetCompany.id;

		// API実行
		ExtendedItemResource.SearchApi api = new ExtendedItemResource.SearchApi();
		ExtendedItemResource.SearchResponse response = (ExtendedItemResource.SearchResponse)api.execute(request);

		// 検索結果が正しいことを確認
		System.assertEquals(2, response.records.size());
		ExtendedItemResource.ExtendedItemParam resExtendedItem = response.records[0];
		verifySearchApiResult(targetExtendedItem1, resExtendedItem);

		//Verify Text input type
		System.assertEquals(targetExtendedItem1.limitLength, AppConverter.intValue(resExtendedItem.limitLength));
		System.assertEquals(targetExtendedItem1.defaultValueText, resExtendedItem.defaultValueText);
	}

	/**
	 * SearchApi のテスト
	 * 選択リスト型の拡張項目が取得できることを確認する
	 */
	@isTest static void searchApiTestPicklist() {
		ResourceTestData testData = new ResourceTestData();
		ExtendedItemEntity targetExtendedItem = testData.createExtendedItem('TargetExtendedItem');

		// 拡張項目を選択リスト型に変更
		String picklistLabel_L0 = 'Option1_L0\nOption2_L0';
		String picklistLabel_L1 = 'Option1_L1\nOption2_L1';
		String picklistLabel_L2 = 'Option1_L2\nOption2_L2';
		String picklistValue = 'Value1\nValue2';
		List<ExtendedItemEntity.PicklistOption> picklist = new ExtendedItemResource.CreateRequest().createPicklist(
				picklistLabel_L0.split(ExtendedItemResource.LINE_BREAK),
				picklistLabel_L1.split(ExtendedItemResource.LINE_BREAK),
				picklistLabel_L2.split(ExtendedItemResource.LINE_BREAK),
				picklistValue.split(ExtendedItemResource.LINE_BREAK));
		targetExtendedItem.picklist = picklist;
		new ExtendedItemRepository().saveEntity(targetExtendedItem);

		ExtendedItemResource.SearchRequest request = new ExtendedItemResource.SearchRequest();
		request.id = targetExtendedItem.id;

		// API実行
		ExtendedItemResource.SearchApi api = new ExtendedItemResource.SearchApi();
		ExtendedItemResource.SearchResponse response = (ExtendedItemResource.SearchResponse)api.execute(request);

		// 検索結果が正しいことを確認
		System.assertEquals(1, response.records.size());
		ExtendedItemResource.ExtendedItemParam resExtendedItem = response.records[0];
		verifySearchApiResult(targetExtendedItem, resExtendedItem);

		//Verify PickList
		System.assertEquals(picklistLabel_L0, resExtendedItem.picklistLabel_L0);
		System.assertEquals(picklistLabel_L1, resExtendedItem.picklistLabel_L1);
		System.assertEquals(picklistLabel_L2, resExtendedItem.picklistLabel_L2);
		System.assertEquals(picklistValue, resExtendedItem.picklistValue);
	}

	/*
	 * Test that Extended Item using Custom Extended Item can be searched correctly.
	 */
	@isTest static void searchApiTestExtendedItemCustom() {
		ResourceTestData testData = new ResourceTestData();
		ExtendedItemEntity targetExtendedItem = testData.createExtendedItem('TargetExtendedItem');
		ComExtendedItemCustom__c extendedItemCustom = ComTestDataUtility.createExtendedItemCustoms('custom', testData.company.id, 1).get(0);

		targetExtendedItem.extendedItemCustomId = extendedItemCustom.Id;
		targetExtendedItem.inputType = ExtendedItemInputType.CUSTOM;
		new ExtendedItemRepository().saveEntity(targetExtendedItem);

		ExtendedItemResource.SearchRequest request = new ExtendedItemResource.SearchRequest();
		request.id = targetExtendedItem.id;

		// API実行
		ExtendedItemResource.SearchApi api = new ExtendedItemResource.SearchApi();
		ExtendedItemResource.SearchResponse response = (ExtendedItemResource.SearchResponse)api.execute(request);

		// 検索結果が正しいことを確認
		System.assertEquals(1, response.records.size());
		ExtendedItemResource.ExtendedItemParam resExtendedItem = response.records[0];
		verifySearchApiResult(targetExtendedItem, resExtendedItem);

		// Check Extended Item Custom data
		System.assertEquals(targetExtendedItem.extendedItemCustomId, resExtendedItem.extendedItemCustomId);
		String extendedItemCustomName = new AppMultiString(extendedItemCustom.Name_L0__c, extendedItemCustom.Name_L1__c, extendedItemCustom.Name_L2__c).getValue();
		System.assertEquals(extendedItemCustomName, resExtendedItem.extendedItemCustom.name);
	}

	/**
	 * Test Extended Item Date type will be returned correctly
	 */
	@isTest static void searchApiTestExtendedItemDate() {
		ResourceTestData testData = new ResourceTestData();
		List<ComExtendedItem__c> extendedItemList = ComTestDataUtility.createExtendedItemDates('EI Date', testData.company.id, 10);

		ExtendedItemEntity targetExtendedItem = new ExtendedItemEntity();
		targetExtendedItem.setId(extendedItemList[0].id);
		targetExtendedItem.name = extendedItemList[0].Name;
		targetExtendedItem.nameL0 =  extendedItemList[0].Name_L0__c;
		targetExtendedItem.nameL1 =  extendedItemList[0].Name_L1__c;
		targetExtendedItem.nameL2 =  extendedItemList[0].Name_L2__c;
		targetExtendedItem.code =  extendedItemList[0].Code__c;
		targetExtendedItem.companyId =  extendedItemList[0].CompanyId__c;
		targetExtendedItem.inputType =  ExtendedItemInputType.valueOf(extendedItemList[0].InputType__c);
		targetExtendedItem.descriptionL0 = extendedItemList[0].Description_L0__c;
		targetExtendedItem.descriptionL1 = extendedItemList[0].Description_L1__c;
		targetExtendedItem.descriptionL2 = extendedItemList[0].Description_L2__c;

		ExtendedItemResource.SearchRequest request = new ExtendedItemResource.SearchRequest();
		request.id = targetExtendedItem.id;

		Test.startTest();
		ExtendedItemResource.SearchApi api = new ExtendedItemResource.SearchApi();
		ExtendedItemResource.SearchResponse response = (ExtendedItemResource.SearchResponse) api.execute(request);
		Test.stopTest();

		// 検索結果が正しいことを確認
		System.assertEquals(1, response.records.size());
		ExtendedItemResource.ExtendedItemParam resExtendedItem = response.records[0];
		verifySearchApiResult(targetExtendedItem, resExtendedItem);
	}

	/**
	 * SearchApi のテスト
	 * 指定した拡張項目IDの拡張項目が取得できることを確認する
	 */
	@isTest static void searchApiTestInvalidParam() {
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		ExtendedItemEntity targetExtendedItem = testData.createExtendedItem('TargetExtendedItem');
		ExtendedItemEntity extendedItem1 = testData.createExtendedItem('TestExtendedItem1');
		ExtendedItemEntity extendedItem2 = testData.createExtendedItem('TestExtendedItem2');

		ExtendedItemResource.SearchRequest request = new ExtendedItemResource.SearchRequest();
		request.companyId = testData.company.id;
		request.id = targetExtendedItem.id;

		ExtendedItemResource.SearchApi api = new ExtendedItemResource.SearchApi();
		App.ParameterException resEx;

		// Test1: 拡張項目IDにID以外の文字列が設定されている
		resEx = null;
		try {
			ExtendedItemResource.SearchRequest testRequest = request.clone();
			testRequest.id = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), resEx.getMessage());

		// Test2: 会社IDにID以外の文字列が設定されている
		resEx = null;
		try {
			ExtendedItemResource.SearchRequest testRequest = request.clone();
			testRequest.companyId = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}), resEx.getMessage());
	}

	/**
	 * 指定した文字数のテスト文字列を作成する
	 */
	private static String createString(Integer length) {
		String s = '';
		for (Integer i = 0; i < length; i++) {
			s += 'T';
		}
		return s;
	}

	private static void verifySearchApiResult(ExtendedItemEntity targetExtendedItem, ExtendedItemResource.ExtendedItemParam resExtendedItem) {
		String expExtendedItemName = new AppMultiString(resExtendedItem.name_L0, resExtendedItem.name_L1,
				resExtendedItem.name_L2).getValue();
		System.assertEquals(expExtendedItemName, resExtendedItem.name);
		System.assertEquals(targetExtendedItem.nameL0, resExtendedItem.name_L0);
		System.assertEquals(targetExtendedItem.nameL1, resExtendedItem.name_L1);
		System.assertEquals(targetExtendedItem.nameL2, resExtendedItem.name_L2);
		System.assertEquals(targetExtendedItem.code, resExtendedItem.code);
		System.assertEquals(targetExtendedItem.companyId, resExtendedItem.companyId);
		System.assertEquals(ExtendedItemInputType.getValue(targetExtendedItem.inputType), resExtendedItem.inputType);
		String expDescription = new AppMultiString(resExtendedItem.description_L0, resExtendedItem.description_L1,
				resExtendedItem.description_L2).getValue();
		System.assertEquals(expDescription, resExtendedItem.description);
		System.assertEquals(targetExtendedItem.descriptionL0, resExtendedItem.description_L0);
		System.assertEquals(targetExtendedItem.descriptionL1, resExtendedItem.description_L1);
		System.assertEquals(targetExtendedItem.descriptionL2, resExtendedItem.description_L2);
	}
}