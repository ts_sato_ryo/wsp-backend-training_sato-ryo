/**
 * @author TeamSpirit Inc.
 * @date 2017
 * 
 * @group 共通
 * 
 * @description UserResourceのテストクラス
 */
@isTest
private class UserResourceTest {

	/**
	 * ユーザレコードをID検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {

		User testUser = ComTestDataUtility.createStandardUser('Testユーザ');

		Test.startTest();

		UserResource.SearchCondition param = new UserResource.SearchCondition();
		param.id = testUser.Id;

		UserResource.SearchApi api = new UserResource.SearchApi();
		UserResource.SearchResult res = (UserResource.SearchResult)api.execute(param);

		Test.stopTest();

		// レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(param.id, res.records[0].id);
		// 項目値の検証はsearchAllPositiveTest()で行うため省略
	}

	/**
	 * ユーザレコードを全件検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		List<User> testRecords = ComTestDataUtility.createStandardUsers('Testユーザ', 3);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>();
		UserResource.SearchApi api = new UserResource.SearchApi();
		UserResource.SearchResult res = (UserResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 項目値の検証
		List<User> allUserRecords = [SELECT
				Id,
				Name,
				UserName,
				ProfileId,
				Profile.Name
			FROM User
			ORDER BY Name
		];

		// すべてのユーザレコードが取得できること
		System.assertEquals(allUserRecords.size(), res.records.size());

		for (Integer i = 0, n = allUserRecords.size(); i < n; i++) {
			User sobj = allUserRecords[i];
			System.assertEquals(sobj.Id, res.records[i].id);
			System.assertEquals(sobj.Name, res.records[i].name);
			System.assertEquals(sobj.UserName, res.records[i].userName);
			System.assertEquals(sobj.ProfileId, res.records[i].profileId);
			System.assertEquals(sobj.Profile.Name, res.records[i].profile.name);
		}
	}

}