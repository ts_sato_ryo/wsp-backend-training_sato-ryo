/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description AttPatternResourceのテストクラス
*/
@isTest
private class AttPatternResourceTest {

	/**
	 * 勤務パターンレコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		// テストデータ作成
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Date currentDate = Date.today();

		Test.startTest();

		AttPatternResource.AttPattern param = new AttPatternResource.AttPattern();
		param.name_L0 = 'Test勤務パターン_L0';
		param.name_L1 = 'Test勤務パターン_L1';
		param.name_L2 = 'Test勤務パターン_L2';
		param.code = '001';
		param.companyId = company.Id;
		param.validDateFrom = '2017-04-01';
		param.validDateTo = '2018-03-31';
		param.workSystem = 'JP:Flex';
		param.order = '1';
		param.useAMHalfDayLeave = false;
		param.usePMHalfDayLeave = true;
		param.startTime = 540;
		param.endTime = 1080;
		param.contractedWorkHours = 9*60;
		param.rest1StartTime = 720;
		param.rest1EndTime = 780;
		param.rest2StartTime = 821;
		param.rest2EndTime = 822;
		param.rest3StartTime = 831;
		param.rest3EndTime = 832;
		param.rest4StartTime = 841;
		param.rest4EndTime = 842;
		param.rest5StartTime = 851;
		param.rest5EndTime = 852;
		param.amStartTime = 540;
		param.amEndTime = 1080;
		param.amContractedWorkHours = 9*60;
		param.amRest1StartTime = 720;
		param.amRest1EndTime = 780;
		param.amRest2StartTime = 821;
		param.amRest2EndTime = 822;
		param.amRest3StartTime = 831;
		param.amRest3EndTime = 832;
		param.amRest4StartTime = 841;
		param.amRest4EndTime = 842;
		param.amRest5StartTime = 851;
		param.amRest5EndTime = 852;
		param.pmStartTime = 540;
		param.pmEndTime = 1080;
		param.pmContractedWorkHours = 9*60;
		param.pmRest1StartTime = 720;
		param.pmRest1EndTime = 780;
		param.pmRest2StartTime = 821;
		param.pmRest2EndTime = 822;
		param.pmRest3StartTime = 831;
		param.pmRest3EndTime = 832;
		param.pmRest4StartTime = 841;
		param.pmRest4EndTime = 842;
		param.pmRest5StartTime = 851;
		param.pmRest5EndTime = 852;
		param.boundaryOfStartTime = 301;
		param.boundaryOfEndTime = 501;

		// 実行
		AttPatternResource.CreateApi api = new AttPatternResource.CreateApi();
		AttPatternResource.SaveResult res = (AttPatternResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 検証
		// 勤務パターンレコードが1件ずつ作成されること
		List<AttPattern__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				UniqKey__c,
				CompanyId__c,
				ValidFrom__c,
				ValidTo__c,
				Order__c,
				WorkSystem__c,
				UseAMHalfDayLeave__c,
				UsePMHalfDayLeave__c,
				StartTime__c,
				EndTime__c,
				ContractedWorkHours__c,
				Rest1StartTime__c,
				Rest1EndTime__c,
				Rest2StartTime__c,
				Rest2EndTime__c,
				Rest3StartTime__c,
				Rest3EndTime__c,
				Rest4StartTime__c,
				Rest4EndTime__c,
				Rest5StartTime__c,
				Rest5EndTime__c,
				AMStartTime__c,
				AMEndTime__c,
				AMContractedWorkHours__c,
				AMRest1StartTime__c,
				AMRest1EndTime__c,
				AMRest2StartTime__c,
				AMRest2EndTime__c,
				AMRest3StartTime__c,
				AMRest3EndTime__c,
				AMRest4StartTime__c,
				AMRest4EndTime__c,
				AMRest5StartTime__c,
				AMRest5EndTime__c,
				PMStartTime__c,
				PMEndTime__c,
				PMContractedWorkHours__c,
				PMRest1StartTime__c,
				PMRest1EndTime__c,
				PMRest2StartTime__c,
				PMRest2EndTime__c,
				PMRest3StartTime__c,
				PMRest3EndTime__c,
				PMRest4StartTime__c,
				PMRest4EndTime__c,
				PMRest5StartTime__c,
				PMRest5EndTime__c,
				BoundaryOfStartTime__c,
				BoundaryOfEndTime__c
			FROM AttPattern__c];

		System.assertEquals(1, newRecords.size());

		// 値の検証
		AttPattern__c newPattern = newRecords[0];

		// レスポンス値の検証
		System.assertEquals(newPattern.Id, res.Id);

		// レコード値の検証
		System.assertEquals(param.name_L0, newPattern.Name);
		System.assertEquals(param.name_L0, newPattern.Name_L0__c);
		System.assertEquals(param.name_L1, newPattern.Name_L1__c);
		System.assertEquals(param.name_L2, newPattern.Name_L2__c);
		System.assertEquals(param.code, newPattern.Code__c);
		System.assertEquals(company.Code__c + '-' + param.code, newPattern.UniqKey__c);
		System.assertEquals(param.companyId, newPattern.CompanyId__c);
		System.assertEquals(param.validDateFrom, String.valueOf(newPattern.ValidFrom__c));
		System.assertEquals(param.validDateTo, String.valueOf(newPattern.ValidTo__c));
		System.assertEquals(param.order, String.valueOf(newPattern.Order__c));
		System.assertEquals(param.workSystem, newPattern.WorkSystem__c);
		System.assertEquals(param.useAMHalfDayLeave, newPattern.UseAMHalfDayLeave__c);
		System.assertEquals(param.usePMHalfDayLeave, newPattern.UsePMHalfDayLeave__c);
		System.assertEquals(param.startTime, newPattern.StartTime__c);
		System.assertEquals(param.endTime, newPattern.EndTime__c);
		System.assertEquals(param.contractedWorkHours, newPattern.ContractedWorkHours__c);
		System.assertEquals(param.rest1StartTime, newPattern.Rest1StartTime__c);
		System.assertEquals(param.rest1EndTime, newPattern.Rest1EndTime__c);
		System.assertEquals(param.rest2StartTime, newPattern.Rest2StartTime__c);
		System.assertEquals(param.rest2EndTime, newPattern.Rest2EndTime__c);
		System.assertEquals(param.rest3StartTime, newPattern.Rest3StartTime__c);
		System.assertEquals(param.rest3EndTime, newPattern.Rest3EndTime__c);
		System.assertEquals(param.rest4StartTime, newPattern.Rest4StartTime__c);
		System.assertEquals(param.rest4EndTime, newPattern.Rest4EndTime__c);
		System.assertEquals(param.rest5StartTime, newPattern.Rest5StartTime__c);
		System.assertEquals(param.rest5EndTime, newPattern.Rest5EndTime__c);
		System.assertEquals(param.amStartTime, newPattern.AMStartTime__c);
		System.assertEquals(param.amEndTime, newPattern.AMEndTime__c);
		System.assertEquals(param.amContractedWorkHours, newPattern.AMContractedWorkHours__c);
		System.assertEquals(param.amRest1StartTime, newPattern.AMRest1StartTime__c);
		System.assertEquals(param.amRest1EndTime, newPattern.AMRest1EndTime__c);
		System.assertEquals(param.amRest2StartTime, newPattern.AMRest2StartTime__c);
		System.assertEquals(param.amRest2EndTime, newPattern.AMRest2EndTime__c);
		System.assertEquals(param.amRest3StartTime, newPattern.AMRest3StartTime__c);
		System.assertEquals(param.amRest3EndTime, newPattern.AMRest3EndTime__c);
		System.assertEquals(param.amRest4StartTime, newPattern.AMRest4StartTime__c);
		System.assertEquals(param.amRest4EndTime, newPattern.AMRest4EndTime__c);
		System.assertEquals(param.amRest5StartTime, newPattern.AMRest5StartTime__c);
		System.assertEquals(param.amRest5EndTime, newPattern.AMRest5EndTime__c);
		System.assertEquals(param.pmStartTime, newPattern.PMStartTime__c);
		System.assertEquals(param.pmEndTime, newPattern.PMEndTime__c);
		System.assertEquals(param.pmContractedWorkHours, newPattern.PMContractedWorkHours__c);
		System.assertEquals(param.pmRest1StartTime, newPattern.PMRest1StartTime__c);
		System.assertEquals(param.pmRest1EndTime, newPattern.PMRest1EndTime__c);
		System.assertEquals(param.pmRest2StartTime, newPattern.PMRest2StartTime__c);
		System.assertEquals(param.pmRest2EndTime, newPattern.PMRest2EndTime__c);
		System.assertEquals(param.pmRest3StartTime, newPattern.PMRest3StartTime__c);
		System.assertEquals(param.pmRest3EndTime, newPattern.PMRest3EndTime__c);
		System.assertEquals(param.pmRest4StartTime, newPattern.PMRest4StartTime__c);
		System.assertEquals(param.pmRest4EndTime, newPattern.PMRest4EndTime__c);
		System.assertEquals(param.pmRest5StartTime, newPattern.PMRest5StartTime__c);
		System.assertEquals(param.pmRest5EndTime, newPattern.PMRest5EndTime__c);
		System.assertEquals(param.boundaryOfStartTime, newPattern.BoundaryOfStartTime__c);
		System.assertEquals(param.boundaryOfEndTime, newPattern.BoundaryOfEndTime__c);
	}

	/**
	 * 勤務パターンレコードを1件作成する(異常系:勤務パターンの管理権限なし)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		Date currentDate = Date.today();

		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 勤務パターンの管理権限を無効に設定する
		testData.permission.isManageAttPattern = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttPatternResource.AttPattern param = new AttPatternResource.AttPattern();
		param.name_L0 = 'Test勤務パターン_L0';
		param.name_L1 = 'Test勤務パターン_L1';
		param.name_L2 = 'Test勤務パターン_L2';
		param.code = '001';
		param.companyId = testData.company.Id;

		// 実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttPatternResource.CreateApi api = new AttPatternResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 勤務パターンレコードを1件作成する(異常系:Idが無効な文字列)
	 */
	@isTest
	static void createNegativeTestInvalidId() {

		// テストデータ作成
		AttPatternResource.AttPattern param = new AttPatternResource.AttPattern();
		param.Id = 'InvalidId';

		// 実行
		AttPatternResource.CreateApi api = new AttPatternResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), ex.getMessage());
	}

	/**
	 * 勤務パターンレコードを1件作成する(異常系:orderが整数に変換できない)
	 */
	@isTest
	static void createNegativeTestInvalidOrder() {

		// テストデータ作成
		AttPatternResource.AttPattern param = new AttPatternResource.AttPattern();
		param.order = 'InvalidOrder';

		// 実行
		AttPatternResource.CreateApi api = new AttPatternResource.CreateApi();
		App.TypeException ex;
		try {
			Object res = api.execute(param);
		} catch (App.TypeException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_TYPE, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValueInteger(new List<String>{ComMessage.msg().Admin_Lbl_Order}), ex.getMessage());
	}

	/**
	 * 勤務パターンレコードを1件作成する(異常系:validDateFrom、validDateToが日付に変換できない)
	 */
	@isTest
	static void createNegativeTestInvalidDate() {

		// テスト1：validDateFrom
		// テストデータ作成
		AttPatternResource.AttPattern param = new AttPatternResource.AttPattern();
		param.validDateFrom = 'InvalidFromDate';

		// 実行
		AttPatternResource.CreateApi api = new AttPatternResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'validDateFrom', String.valueOf(param.validDateFrom)}), ex.getMessage());

		// テスト2：validDateTo
		// テストデータ作成
		param = new AttPatternResource.AttPattern();
		param.validDateTo = 'InvalidToDate';

		// 実行
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'validDateTo', String.valueOf(param.validDateTo)}), ex.getMessage());
	}

	/**
	 * 勤務パターンレコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		// テストデータ作成
		ComCountry__c country = ComTestDataUtility.createCountry('Test国');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('Test会社', country.Id, 2);
		ComCompany__c oldCompany = companyList[0];
		ComCompany__c newCompany = companyList[1];
		AttPattern__c attPattern = ComTestDataUtility.createAttPattern('Test勤務パターン', oldCompany.Id);

		Test.startTest();
		Map<String, Object> param = new Map<String, Object> {
			'id' => attPattern.id,
			'code' => '002',
			'companyId' => newCompany.id
		};

		// 実行
		AttPatternResource.UpdateApi api = new AttPatternResource.UpdateApi();
		Object res = api.execute(param);
		Test.stopTest();

		// 検証
		// 勤務パターンレコードが更新されていること
		List<AttPattern__c> patternList = [SELECT
				Code__c, UniqKey__c, CompanyId__c
			FROM AttPattern__c
			WHERE Id =:(String)param.get('id')];

		System.assertEquals(null, res);
		System.assertEquals(1, patternList.size());
		System.assertEquals((String)param.get('code'), patternList[0].Code__c);
		System.assertEquals(oldCompany.Code__c + '-' + (String)param.get('code'), patternList[0].UniqKey__c);
		// 会社IDが更新されていないことを確認
		System.assertEquals(oldCompany.id, patternList[0].CompanyId__c);
	}

	/**
	 * 勤務パターンレコードを1件更新する(異常系：権限なし)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 勤務パターンの管理権限を無効に設定する
		testData.permission.isManageAttPattern = false;
		new PermissionRepository().saveEntity(testData.permission);
		AttPattern__c updateRecord = ComTestDataUtility.createAttPattern('Test勤務パターン', testData.company.Id);

		// リクエストパラメータ
		Map<String, Object> param = new Map<String, Object> {
			'id' => updateRecord.id,
			'code' => '002'
		};

		// 実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttPatternResource.UpdateApi api = new AttPatternResource.UpdateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 勤務パターンレコードを1件更新する(異常系:Idが未設定)
	 */
	@isTest
	static void updateNegativeTestNullId() {

		// Idを設定しない
		AttPatternResource.AttPattern param = new AttPatternResource.AttPattern();

		// 実行
		App.ParameterException ex;
		try {
			Object res = new AttPatternResource.UpdateApi().execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/**
	 * 勤務パターンレコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		// テストデータ作成
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		AttPattern__c delRecord = ComTestDataUtility.createAttPattern('Test勤務パターン', company.Id);
		AttPatternResource.DeleteRequest param = new AttPatternResource.DeleteRequest();
		param.id = delRecord.id;

		// 実行
		new AttPatternResource.DeleteApi().execute(param);

		// 検証
		// 勤務パターンレコードが削除されること
		List<AttPattern__c> patternList = [SELECT Id FROM AttPattern__c];
		System.assertEquals(0, patternList.size());
	}

	/**
	 * 勤務パターンレコードを1件削除する(異常系：権限なし)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 勤務パターンの管理権限を無効に設定する
		testData.permission.isManageAttPattern = false;
		new PermissionRepository().saveEntity(testData.permission);
		AttPattern__c delRecord = ComTestDataUtility.createAttPattern('Test勤務パターン', testData.company.Id);

		// リクエストパラメータ
		AttPatternResource.DeleteRequest param = new AttPatternResource.DeleteRequest();
		param.id = delRecord.id;

		// 実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttPatternResource.DeleteApi api = new AttPatternResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 勤務パターンレコードを1件削除する(異常系:Idが未設定)
	 */
	@isTest
	static void deleteNegativeTestNullId() {

		// Idを設定しない
		AttPatternResource.DeleteRequest param = new AttPatternResource.DeleteRequest();

		// 実行
		App.ParameterException ex;
		try {
			Object res = new AttPatternResource.DeleteApi().execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/**
	 * 勤務パターンレコードを検索する(正常系:Idで検索)
	 */
	@isTest
	static void searchByIdPositiveTest() {

		// テストデータ作成
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<AttPattern__c> patternList = ComTestDataUtility.createAttPattern('Test勤務パターン', company.Id, 3);

		Test.startTest();
		// Idで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => patternList[0].Id
		};

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		AttPatternResource.SearchResult res = (AttPatternResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		// 検証
		// 勤務パターンレコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 勤務パターンレコードを検索する(正常系:companyIdで検索)
	 */
	@isTest
	static void searchByCompanyIdPositiveTest() {

		// テストデータ作成
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<AttPattern__c> patternList = ComTestDataUtility.createAttPattern('Test勤務パターン', company.Id, 3);

		Test.startTest();
		// companyIdで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'companyId' => company.id
		};

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		AttPatternResource.SearchResult res = (AttPatternResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		// 検証
		// 勤務パターンレコードが取得できること
		System.assertEquals(3, res.records.size());
		System.assertEquals((String)paramMap.get('companyId'), res.records[0].companyId);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 勤務パターンレコードを検索する(正常系:targetDateで検索)
	 */
	@isTest
	static void searchByTargetDatePositiveTest() {

		// テストデータ作成
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<AttPattern__c> patternList = ComTestDataUtility.createAttPattern('Test勤務パターン', company.Id, 3);

		// 有効期間を検索対象日に合わせて設定する
		// 検索対象日が有効開始日と失効日の間
		patternList[0].ValidFrom__c = Date.valueOf('2017-12-01');
		patternList[0].ValidTo__c = Date.valueOf('2018-02-01');
		// 検索対象日が有効開始日
		patternList[1].ValidFrom__c = Date.valueOf('2018-01-01');
		patternList[1].ValidTo__c = Date.valueOf('2018-02-01');
		// 検索対象日が失効日
		patternList[2].ValidFrom__c = Date.valueOf('2017-12-01');
		patternList[2].ValidTo__c = Date.valueOf('2018-01-01');
		upsert patternList;

		Test.startTest();
		// targetDateで検索
		Map<String, Object> paramMap = new Map<String, Object> {
			'targetDate' => '2018-01-01'
		};

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		AttPatternResource.SearchResult res = (AttPatternResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		// 検証
		// 勤務パターンレコードが取得できること
		System.assertEquals(2, res.records.size());
		// 検索対象日が失効日のレコード以外を取得できること
		System.assertEquals(patternList[0].ValidFrom__c, Date.valueOf(res.records[0].validDateFrom));
		System.assertEquals(patternList[0].ValidTo__c, Date.valueOf(res.records[0].validDateTo));
		System.assertEquals(patternList[1].ValidFrom__c, Date.valueOf(res.records[1].validDateFrom));
		System.assertEquals(patternList[1].ValidTo__c, Date.valueOf(res.records[1].validDateTo));

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 勤務パターンレコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {

		// テストデータ作成
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		AttPattern__c AttPattern = ComTestDataUtility.createAttPattern('Test勤務パターン', company.Id);

		Test.startTest();
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => AttPattern.id
		};

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		AttPatternResource.SearchResult res = (AttPatternResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		// 検証
		System.assertEquals(AttPattern.Name_L1__c, res.records[0].name);
	}

	/**
	 * 勤務パターンレコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {

		// テストデータ作成
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		AttPattern__c AttPattern = ComTestDataUtility.createAttPattern('Test勤務パターン', company.Id);

		Test.startTest();
		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => AttPattern.id
		};

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		AttPatternResource.SearchResult res = (AttPatternResource.SearchResult)api.execute(paramMap);
		Test.stopTest();

		// 検証
		System.assertEquals(AttPattern.Name_L2__c, res.records[0].name);
	}

	/**
	 * 勤務パターンレコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		// テストデータ作成
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<AttPattern__c> patternList = ComTestDataUtility.createAttPattern('Test勤務パターン', company.Id, 3);

		// 実行
		Test.startTest();
		Map<String, Object> param = new Map<String, Object>();
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		AttPatternResource.SearchResult res = (AttPatternResource.SearchResult)api.execute(param);
		Test.stopTest();

		// 検証
		List<AttPattern__c> expPatternList = [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CompanyId__c,
				UniqKey__c,
				ValidFrom__c,
				ValidTo__c,
				Order__c,
				WorkSystem__c,
				UseAMHalfDayLeave__c,
				UsePMHalfDayLeave__c,
				StartTime__c,
				EndTime__c,
				ContractedWorkHours__c,
				Rest1StartTime__c,
				Rest1EndTime__c,
				Rest2StartTime__c,
				Rest2EndTime__c,
				Rest3StartTime__c,
				Rest3EndTime__c,
				Rest4StartTime__c,
				Rest4EndTime__c,
				Rest5StartTime__c,
				Rest5EndTime__c,
				AMStartTime__c,
				AMEndTime__c,
				AMContractedWorkHours__c,
				AMRest1StartTime__c,
				AMRest1EndTime__c,
				AMRest2StartTime__c,
				AMRest2EndTime__c,
				AMRest3StartTime__c,
				AMRest3EndTime__c,
				AMRest4StartTime__c,
				AMRest4EndTime__c,
				AMRest5StartTime__c,
				AMRest5EndTime__c,
				PMStartTime__c,
				PMEndTime__c,
				PMContractedWorkHours__c,
				PMRest1StartTime__c,
				PMRest1EndTime__c,
				PMRest2StartTime__c,
				PMRest2EndTime__c,
				PMRest3StartTime__c,
				PMRest3EndTime__c,
				PMRest4StartTime__c,
				PMRest4EndTime__c,
				PMRest5StartTime__c,
				PMRest5EndTime__c,
				BoundaryOfStartTime__c,
				BoundaryOfEndTime__c
			FROM AttPattern__c
			ORDER BY Code__c];

		// 勤務パターンレコードが取得できること
		System.assertEquals(expPatternList.size(), res.records.size());

		for (Integer i = 0, n = expPatternList.size(); i < n; i++) {
			AttPattern__c exp = expPatternList[i];
			AttPatternResource.AttPattern dto = res.records[i];

			// 項目値の検証
			System.assertEquals(exp.Id, dto.id);
			System.assertEquals(exp.Name_L0__c, dto.name);
			System.assertEquals(exp.Name_L0__c, dto.name_L0);
			System.assertEquals(exp.Name_L1__c, dto.name_L1);
			System.assertEquals(exp.Name_L2__c, dto.name_L2);
			System.assertEquals(exp.Code__c, dto.code);
			System.assertEquals(exp.CompanyId__c, dto.companyId);
			System.assertEquals(exp.ValidFrom__c, Date.valueOf(dto.validDateFrom));
			System.assertEquals(exp.ValidTo__c, Date.valueOf(dto.validDateTo));
			System.assertEquals(exp.Order__c, Integer.valueOf(dto.order));
			System.assertEquals(exp.WorkSystem__c, dto.workSystem);
			System.assertEquals(exp.UseAMHalfDayLeave__c, dto.useAMHalfDayLeave);
			System.assertEquals(exp.UsePMHalfDayLeave__c, dto.usePMHalfDayLeave);
			System.assertEquals(exp.StartTime__c, dto.startTime);
			System.assertEquals(exp.EndTime__c, dto.endTime);
			System.assertEquals(exp.ContractedWorkHours__c, dto.contractedWorkHours);
			System.assertEquals(exp.Rest1StartTime__c, dto.rest1StartTime);
			System.assertEquals(exp.Rest1EndTime__c, dto.rest1EndTime);
			System.assertEquals(exp.Rest2StartTime__c, dto.rest2StartTime);
			System.assertEquals(exp.Rest2EndTime__c, dto.rest2EndTime);
			System.assertEquals(exp.Rest3StartTime__c, dto.rest3StartTime);
			System.assertEquals(exp.Rest3EndTime__c, dto.rest3EndTime);
			System.assertEquals(exp.Rest4StartTime__c, dto.rest4StartTime);
			System.assertEquals(exp.Rest4EndTime__c, dto.rest4EndTime);
			System.assertEquals(exp.Rest5EndTime__c, dto.rest5EndTime);
			System.assertEquals(exp.AMStartTime__c, dto.amStartTime);
			System.assertEquals(exp.AMEndTime__c, dto.amEndTime);
			System.assertEquals(exp.AMContractedWorkHours__c, dto.amContractedWorkHours);
			System.assertEquals(exp.AMRest1StartTime__c, dto.amRest1StartTime);
			System.assertEquals(exp.AMRest1EndTime__c, dto.amRest1EndTime);
			System.assertEquals(exp.AMRest2StartTime__c, dto.amRest2StartTime);
			System.assertEquals(exp.AMRest2EndTime__c, dto.amRest2EndTime);
			System.assertEquals(exp.AMRest3StartTime__c, dto.amRest3StartTime);
			System.assertEquals(exp.AMRest3EndTime__c, dto.amRest3EndTime);
			System.assertEquals(exp.AMRest4StartTime__c, dto.amRest4StartTime);
			System.assertEquals(exp.AMRest4EndTime__c, dto.amRest4EndTime);
			System.assertEquals(exp.AMRest5StartTime__c, dto.amRest5StartTime);
			System.assertEquals(exp.AMRest5EndTime__c, dto.amRest5EndTime);
			System.assertEquals(exp.PMStartTime__c, dto.pmStartTime);
			System.assertEquals(exp.PMEndTime__c, dto.pmEndTime);
			System.assertEquals(exp.PMContractedWorkHours__c, dto.pmContractedWorkHours);
			System.assertEquals(exp.PMRest1StartTime__c, dto.pmRest1StartTime);
			System.assertEquals(exp.PMRest1EndTime__c, dto.pmRest1EndTime);
			System.assertEquals(exp.PMRest2StartTime__c, dto.pmRest2StartTime);
			System.assertEquals(exp.PMRest2EndTime__c, dto.pmRest2EndTime);
			System.assertEquals(exp.PMRest3StartTime__c, dto.pmRest3StartTime);
			System.assertEquals(exp.PMRest3EndTime__c, dto.pmRest3EndTime);
			System.assertEquals(exp.PMRest4StartTime__c, dto.pmRest4StartTime);
			System.assertEquals(exp.PMRest4EndTime__c, dto.pmRest4EndTime);
			System.assertEquals(exp.PMRest5StartTime__c, dto.pmRest5StartTime);
			System.assertEquals(exp.PMRest5EndTime__c, dto.pmRest5EndTime);
			System.assertEquals(exp.BoundaryOfStartTime__c, dto.boundaryOfStartTime);
			System.assertEquals(exp.BoundaryOfEndTime__c, dto.boundaryOfEndTime);
		}
	}

	/**
	 * 勤務パターンレコードを検索する(異常系:Idが無効な文字列)
	 */
	@isTest
	static void searchNegativeTestInvalidId() {

		// テストデータ作成
		AttPatternResource.SearchRequest param = new AttPatternResource.SearchRequest();
		param.Id = 'InvalidId';

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}), ex.getMessage());
	}

	/**
	 * 勤務パターンレコードを検索する(異常系:companyIdが無効な文字列)
	 */
	@isTest
	static void searchNegativeTestInvalidCompanyId() {

		// テストデータ作成
		AttPatternResource.SearchRequest param = new AttPatternResource.SearchRequest();
		param.companyId = 'InvalidCompanyId';

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}), ex.getMessage());
	}

	/**
	 * 勤務パターンレコードを検索する(異常系:targetDateが日付に変換できない)
	 */
	@isTest
	static void searchNegativeTestInvalidDate() {

		// テストデータ作成
		AttPatternResource.SearchRequest param = new AttPatternResource.SearchRequest();
		param.targetDate = 'InvalidDate';

		// 実行
		AttPatternResource.SearchApi api = new AttPatternResource.SearchApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		// 検証
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'targetDate', String.valueOf(param.targetDate)}), ex.getMessage());
	}
}