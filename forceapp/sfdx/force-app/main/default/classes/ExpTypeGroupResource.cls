/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Resoruce class for expense type group master mainenance
 */
public with sharing class ExpTypeGroupResource {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @description Request parameter for CreateApi, UpdateApi
	 */
	public class ExpTypeGroupParam implements RemoteApi.RequestParam {
		/** ID */
		public String id;
		/** 費目グループ名(翻訳) */
		public String name;
		/** 費目グループ名(L0) */
		public String name_L0;
		/** 費目グループ名(L1) */
		public String name_L1;
		/** 費目グループ名(L2) */
		public String name_L2;
		/** コード */
		public String code;
		/** 会社ID */
		public String companyId;
		/** 有効開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 親費目グループID */
		public String parentId;
		/** 親部グループ */
		public LookupField parent;
		/** 説明(翻訳) */
		public String description;
		/** 説明(L0) */
		public String description_L0;
		/** 説明(L1) */
		public String description_L1;
		/** 説明(L2) */
		public String description_L2;
		/** 並び順 */
		public Integer order;

		/**
		 * @description Create ExpTypeGroupEntity from the request parameter value.
		 * @param paramMap Map of the request parameter
		 * @return Created entity
		 */
		public ExpTypeGroupEntity createEntity(Map<String, Object> paramMap) {
			ExpTypeGroupEntity entity = new ExpTypeGroupEntity();

			entity.setId(this.id);
			if (paramMap.containsKey('name_L0')) {
				entity.nameL0 = this.name_L0;

				// TODO nameは仮置きでnameL0を設定
				entity.name = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('validDateFrom')){
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			// If validFrom is null, set today as default value
			if (entity.validFrom == null) {
				entity.validFrom = AppDate.today();
			}
			if (paramMap.containsKey('validDateTo')) {
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (entity.validTo == null) {
				entity.validTo = ValidPeriodEntityOld.VALID_TO_MAX;
			}
			if (paramMap.containsKey('parentId')) {
				entity.parentId = this.parentId;
			}
			if (paramMap.containsKey('description_L0')) {
				entity.descriptionL0 = this.description_L0;
			}
			if (paramMap.containsKey('description_L1')) {
				entity.descriptionL1 = this.description_L1;
			}
			if (paramMap.containsKey('description_L2')) {
				entity.descriptionL2 = this.description_L2;
			}
			if (paramMap.containsKey('order')) {
				entity.order = this.order;
			}

			return entity;
		}
	}

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		public String name;
	}

	/**
	 * @description Response parameter for CreateApi
	 */
	public class SaveResponse implements RemoteApi.ResponseParam {
		/** Id of the created record */
		public String id;
	}

	/**
	 * @description Implementation class of expense type group creation API
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_TYPE_GROUP;

		/**
		 * @description Create a new expense type group
		 * @param req Request parameter(ExpTypeGroupParam)
		 * @return Response parameter(SaveResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpTypeGroupParam param = (ExpTypeGroupParam)req.getParam(ExpTypeGroupResource.ExpTypeGroupParam.class);

			// create entity from request parameter
			ExpTypeGroupEntity entity = param.createEntity(req.getParamMap());

			// save
			Id resId = new ExpTypeGroupService().createNewExpTypeGroup(entity);

			// create response parameter
			ExpTypeGroupResource.SaveResponse res = new ExpTypeGroupResource.SaveResponse();
			res.Id = resId;

			return res;
		}
	}

	/**
	 * @description Implementation class of expense type group updating API
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_TYPE_GROUP;

		/**
		 * @description Update an expense type group
		 * @param req Request parameter(ExpTypeGroupParam)
		 * @return Response parameter(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpTypeGroupParam param = (ExpTypeGroupParam)req.getParam(ExpTypeGroupResource.ExpTypeGroupParam.class);

			// validate request parmeter
			validateParam(param);

			// create entity from request parameter
			ExpTypeGroupEntity entity = param.createEntity(req.getParamMap());

			// save
			new ExpTypeGroupService().updateExpTypeGroup(entity);

			// no response parameter
			return null;
		}

		/**
		 * Validate request parmeter
		 * @param param Target entity
		 */
		private void validateParam(ExpTypeGroupResource.ExpTypeGroupParam param) {
			// ID
			ExpCommonUtil.validateId('ID', param.id, true);
		}
	}

	/**
	 * @description Request parameter for DeleteApi
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** Record id to be deleted */
		public String id;

		/**
		 * Validate request parameter
		 */
		public void validate() {
			// ID
			ExpCommonUtil.validateId('id', this.id, true);
		}
	}

	/**
	 * @description Implementation class of expense type group deleting API
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_TYPE_GROUP;

		/**
		 * @description Delete an expense type group
		 * @param req Request parameter(DeleteRequest)
		 * @return Response parameter(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpTypeGroupResource.DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);

			// validate request parmeter
			param.validate();

			// delete
			(new ExpTypeGroupService()).deleteExpTypeGroup(param.id);

			// no response parameter
			return null;
		 }
	}

	/**
	 * @description Request parameter for SearchApi
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 関連する会社レコードID */
		public String companyId;
		/** 対象日 */
		public Date targetDate;
		/** 親費目グループ未設定 */
		public Boolean hasNoParent;

		/**
		 * Validate request parameter
		 */
		public void validate() {
			// ID
			ExpCommonUtil.validateId('id', this.id, false);

			// 会社ID
			ExpCommonUtil.validateId('companyId', this.companyId, false);
		}
	}

	/**
	 * @description Response parameter for SearchApi
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** Search result */
		public ExpTypeGroupParam[] records;
	}

	/**
	 * @description Implementation class of expense type group searching API
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search expense type group records
		 * @param req Request parameter(SearchRequest)
		 * @return Response parameter(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			// convert request parameter from Map to SearchRequest
			SearchRequest param = (ExpTypeGroupResource.SearchRequest)req.getParam(ExpTypeGroupResource.SearchRequest.class);
			Map<String, Object> paramMap = req.getParamMap();

			// validate request parmeter
			param.validate();

			// create search filter condition
			ExpTypeGroupRepository.SearchFilter filter = new ExpTypeGroupRepository.SearchFilter();
			// if parameter 'id' is set, add id to condition
			if (paramMap.containsKey('id')) {
				filter.ids = new Set<Id>{param.id};
			}
			// if parameter 'companyId' is set, add company id to condition
			if (paramMap.containsKey('companyId')) {
				filter.companyIds = new Set<Id>{param.companyId};
			}
			// if parameter 'targetDate' is set, add date to condition
			if (paramMap.containsKey('targetDate')) {
				filter.targetDate = AppDate.valueOf(param.targetDate);
			}
			// if parameter 'hasNoParent' is set, add date to condition
			if (paramMap.containsKey('hasNoParent')) {
				filter.hasNoParent = param.hasNoParent;
			}

			// search
			List<ExpTypeGroupEntity> entityList = (new ExpTypeGroupRepository()).searchEntity(filter);

			// convert search results to response format
			List<ExpTypeGroupParam> records = new List<ExpTypeGroupParam>();
			for (ExpTypeGroupEntity entity : entitylist) {
				records.add(createExpTypeGroupParam(entity));
			}

			// create response parameter
			SearchResponse res = new SearchResponse();
			res.records = records;

			return res;
		}

		/**
		 * Convert ExpTypeGroupEntity to ExpTypeGroupParam
		 * @param entity Source entity
		 * @return Converted param data
		 */
		private ExpTypeGroupParam createExpTypeGroupParam(ExpTypeGroupEntity entity) {
			ExpTypeGroupParam param = new ExpTypeGroupParam();

			param.id = entity.id;
			param.code = entity.code;
			param.name = entity.nameL.getValue();
			param.name_L0 = entity.nameL0;
			param.name_L1 = entity.nameL1;
			param.name_L2 = entity.nameL2;
			param.companyId = entity.companyId;
			param.validDateFrom = AppConverter.dateValue(entity.validFrom);
			param.validDateTo = AppConverter.dateValue(entity.validTo);
			param.parentId = entity.parentId;
			param.parentId = entity.parentId;
			param.parent = new LookupField();
			if (entity.parentGroup != null) {
				param.parent.name = entity.parentGroup.nameL.getValue();
			}
			param.description = entity.descriptionL.getValue();
			param.description_L0 = entity.descriptionL0;
			param.description_L1 = entity.descriptionL1;
			param.description_L2 = entity.descriptionL2;
			param.order = entity.order;

			return param;
		}
	}
}