/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇消費のエンティティ
 */
public with sharing class AttManagedLeaveTakeEntity extends Entity {
	/** 項目の定義(変更管理で使用する) */
	public enum Field {
			LEAVE_SUMMARY_ID, START_DATE, END_DATE, DAYS_TAKEN, LEAVE_REQUEST_ID
	}
	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;
	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}
	public AttManagedLeaveTakeEntity() {
		isChangedFieldSet = new Set<Field>();
	}
	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}
	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}
	
	/** 休暇サマリ */
	public Id leaveSummaryId {
		get;
		set {
			leaveSummaryId = value;
			setChanged(Field.LEAVE_SUMMARY_ID);
		}
	}
	/** 休暇申請 */
	public Id leaveRequestId {
		get;
		set {
			leaveRequestId = value;
			setChanged(Field.LEAVE_REQUEST_ID);
		}
	}
	/** 開始日 */
	public AppDate startDate {
		get;
		set {
			startDate = value;
			setChanged(Field.START_DATE);
		}
	}
	/**終了日 */
	public AppDate endDate {
		get;
		set {
			endDate = value;
			setChanged(Field.END_DATE);
		}
	}
	/** 取得日数 */
	public AttDays daysTaken {
		get;
		set {
			daysTaken = value;
			setChanged(Field.DAYS_TAKEN);
		}
	}
	/** 休暇Id */
	public Id leaveId {get; set;}
	/** 社員Id */
	public Id employeeId {get; set;}
}