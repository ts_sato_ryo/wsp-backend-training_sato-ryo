/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description UserSettingResourceのテストクラス
 */
@isTest
private class UserSettingResourceTest {

	/**
	 * ユーザ設定情報を取得する(正常系)
	 */
	@isTest
	static void getPositiveTest() {
		Id userId = UserInfo.getUserId();
		User testManager = ComTestDataUtility.createUser('manager', 'ja', UserInfo.getLocale());
		User testApprover01 = ComTestDataUtility.createUser('approver', 'ja', UserInfo.getLocale());
		ComTestDataUtility.createOrgSetting();
		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', null);
		company.RequireLocationAtMobileStamp__c = true;
		// Set base currency
		ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('JPY', '日本円');
		company.CurrencyId__c = currencyObj.Id;
		update company;
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);
		ComCostCenterBase__c costCenter = ComTestDataUtility.createCostCentersWithHistory('Test CC', company.Id, 1, 1)[0];
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, null, userId);
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('Test上長', company.Id, null, testManager.Id);
		ComEmpBase__c approver01 = ComTestDataUtility.createEmployeeWithHistory('Test承認者01', company.Id, null, testApprover01.Id);
		ComEmpHistory__c empHistory = [SELECT DepartmentBaseId__c FROM ComEmpHistory__c WHERE BaseId__c = :emp.Id];
		empHistory.CostCenterBaseId__c = costCenter.Id;
		empHistory.DepartmentBaseId__c = dept.Id;
		empHistory.ManagerBaseId__c = manager.Id;
		empHistory.ApproverBase01Id__c = approver01.Id;
		update empHistory;

		Test.startTest();

		UserSettingResource.GetApi api = new UserSettingResource.GetApi();
		Map<String, Object> param = new Map<String, Object>();
		UserSettingResource.UserSetting res = (UserSettingResource.UserSetting)api.execute(param);

		Test.stopTest();

		User user = [SELECT SmallPhotoUrl FROM User WHERE Id =:userId];
		ComDeptHistory__c deptHistory = [SELECT Name_L0__c, Name_L1__c, Name_L2__c FROM ComDeptHistory__c WHERE BaseId__c = :dept.Id];
		EmployeeBaseEntity empEntity = (new EmployeeRepository()).getEntity(emp.Id, AppDate.today());
		ComEmpBase__c empManager = [SELECT FirstName_L0__c, FirstName_L1__c, FirstName_L2__c, LastName_L0__c, LastName_L1__c, LastName_L2__c FROM ComEmpBase__c WHERE Id = :manager.Id];
		ComEmpBase__c empApprover01 = [SELECT FirstName_L0__c, FirstName_L1__c, FirstName_L2__c, LastName_L0__c, LastName_L1__c, LastName_L2__c FROM ComEmpBase__c WHERE Id = :approver01.Id];

		System.assertEquals(UserInfo.getUserId(), res.id);
		System.assertEquals(UserInfo.getUserName(), res.userName);
		System.assertEquals(UserInfo.getName(), res.name);
		System.assertEquals(user.SmallPhotoUrl, res.photoUrl);
		System.assertEquals(UserInfo.getLanguage(), res.language);
		System.assertEquals(company.AllowTaxAmountChange__c, res.allowTaxAmountChange);
		System.assertEquals(empEntity.companyId, res.companyId);
		System.assertEquals(empEntity.company.nameL.getValue(), res.companyName);
		System.assertEquals(empEntity.companyInfo.useAttendance, res.useAttendance);
		System.assertEquals(empEntity.companyInfo.useExpense, res.useExpense);
		System.assertEquals(empEntity.companyInfo.usePlanner, res.usePlanner);
		System.assertEquals(empEntity.companyInfo.useWorkTime, res.useWorkTime);
		System.assertEquals(empHistory.DepartmentBaseId__c, res.departmentId);
		String departmentName = new AppMultiString(deptHistory.Name_L0__c, deptHistory.Name_L1__c, deptHistory.Name_L2__c).getValue();
		System.assertEquals(departmentName, res.departmentName);
		System.assertEquals(empEntity.id, res.employeeId);
		System.assertEquals(empEntity.fullNameL.getFullName(), res.employeeName);
		String managerName = new AppPersonName(
			empManager.FirstName_L0__c, empManager.LastName_L0__c,
			empManager.FirstName_L1__c, empManager.LastName_L1__c,
			empManager.FirstName_L2__c, empManager.LastName_L2__c
			).getFullName();
		System.assertEquals(currencyObj.Id, res.currencyId);
		System.assertEquals(currencyObj.Code__c, res.currencyCode);
		System.assertEquals(currencyObj.Symbol__c, res.currencySymbol);
		System.assertEquals(currencyObj.DecimalPlaces__c, res.currencyDecimalPlaces);
		System.assertEquals(false, res.allowToChangeApproverSelf);
		String empApprover01Name = new AppPersonName(
			empApprover01.FirstName_L0__c, empApprover01.LastName_L0__c,
			empApprover01.FirstName_L1__c, empApprover01.LastName_L1__c,
			empApprover01.FirstName_L2__c, empApprover01.LastName_L2__c
			).getFullName();
		System.assertEquals(empApprover01Name, res.approver01Name);
		System.assertEquals(company.RequireLocationAtMobileStamp__c, res.requireLocationAtMobileStamp);
	}
	/**
	 * ユーザ設定情報を取得する(承認者01)
	 */
	@isTest
	static void getApprover01Test() {

		TestData.TestDataEntity testData = new TestData.TestDataEntity();

		Id userId = UserInfo.getUserId();
		User testApprover01 = ComTestDataUtility.createUser('approver1', 'ja', UserInfo.getLocale());
		User testApprover02 = ComTestDataUtility.createUser('approver2', 'ja', UserInfo.getLocale());
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('Test社員', testData.company.Id, null, userId);
		ComEmpBase__c approver01 = ComTestDataUtility.createEmployeeWithHistory('Test承認者01', testData.company.Id, null, testApprover01.Id);
		ComEmpBase__c approver02 = ComTestDataUtility.createEmployeeWithHistory('Test承認者02', testData.company.Id, null, testApprover02.Id);
		ComEmpBase__c empApprover01 = [SELECT FirstName_L0__c, FirstName_L1__c, FirstName_L2__c, LastName_L0__c, LastName_L1__c, LastName_L2__c FROM ComEmpBase__c WHERE Id = :approver01.Id];
		ComEmpBase__c empApprover02 = [SELECT FirstName_L0__c, FirstName_L1__c, FirstName_L2__c, LastName_L0__c, LastName_L1__c, LastName_L2__c FROM ComEmpBase__c WHERE Id = :approver02.Id];

		ComEmpHistory__c empHistory = [SELECT DepartmentBaseId__c FROM ComEmpHistory__c WHERE BaseId__c = :emp.Id];
		empHistory.ApproverBase01Id__c = approver01.id;
		empHistory.WorkingTypeBaseId__c = testData.workingType.id;
		update empHistory;

		ComEmpHistory__c emp2History = [SELECT DepartmentBaseId__c FROM ComEmpHistory__c WHERE BaseId__c = :empApprover02.Id];
		emp2History.ApprovalAuthority01__c = true;
		update emp2History;

		PersonalSettingEntity personalSetting = new PersonalSettingEntity();
		personalSetting.employeeBaseId = emp.id;
		personalSetting.approverBase01Id = approver02.id;
		personalSetting.setId(new PersonalSettingRepository().saveEntity(personalSetting).details[0].id);

		Test.startTest();

		// 承認者選択機能有効+個人設定承認者01あり
		testData.workingType.getHistory(0).allowToChangeApproverSelf = true;
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		UserSettingResource.GetApi api = new UserSettingResource.GetApi();
		Map<String, Object> param = new Map<String, Object>();
		UserSettingResource.UserSetting res = (UserSettingResource.UserSetting)api.execute(param);

		System.assertEquals(true, res.allowToChangeApproverSelf);
		String empApprover01Name = new AppPersonName(
			empApprover02.FirstName_L0__c, empApprover02.LastName_L0__c,
			empApprover02.FirstName_L1__c, empApprover02.LastName_L1__c,
			empApprover02.FirstName_L2__c, empApprover02.LastName_L2__c
			).getFullName();
		System.assertEquals(empApprover01Name, res.approver01Name);

		// 承認者選択機能有効+個人設定承認者01なし
		personalSetting.approverBase01Id = null;
		new PersonalSettingRepository().saveEntity(personalSetting);

		res = (UserSettingResource.UserSetting)api.execute(param);

		System.assertEquals(true, res.allowToChangeApproverSelf);
		empApprover01Name = new AppPersonName(
			empApprover01.FirstName_L0__c, empApprover01.LastName_L0__c,
			empApprover01.FirstName_L1__c, empApprover01.LastName_L1__c,
			empApprover01.FirstName_L2__c, empApprover01.LastName_L2__c
			).getFullName();
		System.assertEquals(empApprover01Name, res.approver01Name);

		// 承認者選択機能無効+個人設定承認者01あり
		personalSetting.approverBase01Id = approver02.id;
		new PersonalSettingRepository().saveEntity(personalSetting);
		testData.workingType.getHistory(0).allowToChangeApproverSelf = false;
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		res = (UserSettingResource.UserSetting)api.execute(param);

		System.assertEquals(false, res.allowToChangeApproverSelf);
		empApprover01Name = new AppPersonName(
			empApprover01.FirstName_L0__c, empApprover01.LastName_L0__c,
			empApprover01.FirstName_L1__c, empApprover01.LastName_L1__c,
			empApprover01.FirstName_L2__c, empApprover01.LastName_L2__c
			).getFullName();
		System.assertEquals(empApprover01Name, res.approver01Name);

		// // 承認者選択機能無効+社員履歴承認者01なし
		empHistory.ApproverBase01Id__c = null;
		update empHistory;
		res = (UserSettingResource.UserSetting)api.execute(param);

		System.assertEquals(false, res.allowToChangeApproverSelf);
		System.assertEquals(null, res.approver01Name);
	}

	/**
	 * ユーザ設定情報を取得する(Employee has expired)
	 */
	@isTest
	static void employeeExpiredTest() {
		Id userId = UserInfo.getUserId();
		User testManager = ComTestDataUtility.createUser('manager', 'ja', UserInfo.getLocale());
		User testApprover01 = ComTestDataUtility.createUser('approver', 'ja', UserInfo.getLocale());
		ComTestDataUtility.createOrgSetting();
		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', null);
		company.RequireLocationAtMobileStamp__c = true;
		// Set base currency
		ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('JPY', '日本円');
		company.CurrencyId__c = currencyObj.Id;
		update company;
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);
		ComCostCenterBase__c costCenter = ComTestDataUtility.createCostCentersWithHistory('Test CC', company.Id, 1, 1)[0];
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, null, userId);
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('Test上長', company.Id, null, testManager.Id);
		ComEmpBase__c approver01 = ComTestDataUtility.createEmployeeWithHistory('Test承認者01', company.Id, null, testApprover01.Id);
		ComEmpHistory__c empHistory = [SELECT DepartmentBaseId__c FROM ComEmpHistory__c WHERE BaseId__c = :emp.Id];
		empHistory.CostCenterBaseId__c = costCenter.Id;
		empHistory.DepartmentBaseId__c = dept.Id;
		empHistory.ManagerBaseId__c = manager.Id;
		empHistory.ApproverBase01Id__c = approver01.Id;
		empHistory.ValidTo__c = Date.today(); // expire
		empHistory.ValidFrom__c = Date.today().addMonths(-1);
		update empHistory;

		Test.startTest();

		UserSettingResource.GetApi api = new UserSettingResource.GetApi();
		Map<String, Object> param = new Map<String, Object>();
		UserSettingResource.UserSetting res = (UserSettingResource.UserSetting)api.execute(param);

		Test.stopTest();

		User user = [SELECT SmallPhotoUrl FROM User WHERE Id =:userId];

		System.assertEquals(UserInfo.getUserId(), res.id);
		System.assertEquals(UserInfo.getUserName(), res.userName);
		System.assertEquals(UserInfo.getName(), res.name);
		System.assertEquals(user.SmallPhotoUrl, res.photoUrl);
		System.assertEquals(UserInfo.getLanguage(), res.language);
		System.assertEquals(null, res.allowTaxAmountChange);
		System.assertEquals(null, res.companyId);
		System.assertEquals(null, res.companyName);
		System.assertEquals(null, res.useExpense);
		System.assertEquals(null, res.departmentId);
		System.assertEquals(null, res.departmentName);
		System.assertEquals(null, res.employeeId);
		System.assertEquals(null, res.employeeName);
		System.assertEquals(null, res.currencyId);
		System.assertEquals(null, res.currencyCode);
		System.assertEquals(null, res.currencySymbol);
		System.assertEquals(null, res.currencyDecimalPlaces);
		System.assertEquals(null, res.allowToChangeApproverSelf);
		System.assertEquals(null, res.approver01Name);
		System.assertEquals(null, res.requireLocationAtMobileStamp);
	}

	/*
	 * Test that Details can be selectively Retrieved without any errors.
	 * Detail Retrieval is already covered by Positive Test.
	 */
	@isTest static void getSelectiveDetailTest() {
		Id userId = UserInfo.getUserId();
		User testManager = ComTestDataUtility.createUser('manager', 'ja', UserInfo.getLocale());
		User testApprover01 = ComTestDataUtility.createUser('approver', 'ja', UserInfo.getLocale());
		ComTestDataUtility.createOrgSetting();
		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', null);
		company.RequireLocationAtMobileStamp__c = true;
		// Set base currency
		ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('JPY', '日本円');
		company.CurrencyId__c = currencyObj.Id;
		update company;
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);
		ComCostCenterBase__c costCenter = ComTestDataUtility.createCostCentersWithHistory('Test CC', company.Id, 1, 1)[0];
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, null, userId);
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('Test上長', company.Id, null, testManager.Id);
		ComEmpBase__c approver01 = ComTestDataUtility.createEmployeeWithHistory('Test承認者01', company.Id, null, testApprover01.Id);
		ComEmpHistory__c empHistory = [SELECT DepartmentBaseId__c FROM ComEmpHistory__c WHERE BaseId__c = :emp.Id];
		empHistory.CostCenterBaseId__c = costCenter.Id;
		empHistory.DepartmentBaseId__c = dept.Id;
		empHistory.ManagerBaseId__c = manager.Id;
		empHistory.ApproverBase01Id__c = approver01.Id;
		update empHistory;

		UserSettingResource.GetApi api = new UserSettingResource.GetApi();
		Test.startTest();

		testAndVerify(api, null);
		testAndVerify(api, new Set<String>());
		for (EmployeeService.EmployeeDetailSelector detailSelector : EmployeeService.EmployeeDetailSelector.values()) {
			testAndVerify(api, new Set<String> {detailSelector.name()});
		}
		Test.stopTest();
	}

	/*
	 * Verify that API is executed successfully without error.
	 */
	private static void testAndVerify(UserSettingResource.GetApi api, Set<String> detailSelectors) {
		UserSettingResource.GetApiParam param = new UserSettingResource.GetApiParam();
		param.detailSelectors = detailSelectors;
		UserSettingResource.UserSetting res = (UserSettingResource.UserSetting) api.execute(param);
		System.assertEquals(UserInfo.getUserId(), res.id);
	}
}