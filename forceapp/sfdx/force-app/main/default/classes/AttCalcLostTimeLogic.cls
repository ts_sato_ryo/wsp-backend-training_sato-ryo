/**
 * 始業終業時刻間の勤務状態から遅刻早退休憩の控除時間を計算するロジック
 *
 * 始業終業時間の間の勤務状況から、勤務時間と控除時間、所定勤務時間との過不足時間を計算する。
 * 始業終業時間外の時間外勤務と合わせて残業時間を計算するロジックの前段の処理になる。
 */
public with sharing class AttCalcLostTimeLogic {
	/**
	 * 日次控除計算ロジックの入力パラメタ
	 */
	public virtual class Input {
		// ----------
		// 実際の勤務に関する時間/時刻
		// ----------
		/**
		 * 実出社時刻を返す
		 * @return 実出社時刻
		 */
		public Integer inputStartTime = 0;

		/**
		 * 実退社時刻を返す
		 * @return 実退社時刻
		 */
		public Integer inputEndTime = 0;
		/**
		 * 取得した休憩の時間帯を返す
		 * 時間単位有休/代休はこの中に含めない。
		 *
		 * @return 取得した休憩の時間帯
		 */
		public AttCalcRangesDto inputRestRanges = new AttCalcRangesDto();
		/**
		 * 時刻を定めずに取った休憩時間を返す。取得休憩()に追加して休憩時間として扱う。
		 * @return 取得休憩時間
		 */
		public Integer inputRestTime = 0;

		// ----------
		// その日の申請から取得される時間/時刻
		// ----------
		/**
		 * 遅刻早退申請で免除された控除時間帯を返す
		 *
		 * @return 遅刻早退申請で免除された控除時間帯
		 */
		public AttCalcRangesDto permitedLostTimeRanges = new AttCalcRangesDto();
		/**
		 * 時間単位有休/代休の時間を設定する。
		 *
		 * 取得休憩には含めないくてよいが、この時間も休憩として扱うので
		 * 勤務時間から除外される。
		 * 始業就業時間外出社退社時間外の休憩も有効
		 * 勤務換算時間,無給休暇休憩,無給休暇時間との合計が所定勤務時間を越えてはならない。
		 *
		 * @return 勤務に換算する休憩時間
		 */
		public AttCalcRangesDto convertdRestRanges = new AttCalcRangesDto();

		/**
		 * 半日有休/代休の換算時間を設定する。
		 *
		 * 有給の半休などで勤務しなくても勤務したとみなす時間を設定する
		 * 勤務換算休憩と異なり、勤務から取り除く休憩時間は発生しない。
		 * 勤務換算休憩,無給休暇休憩,無給休暇時間との合計が所定勤務時間を越えてはならない。
		 * この時間を設定する場合、始業/終業時刻も調整を行うこと
		 * 例) 始業終業 9:00-18:00 所定休憩 12:00-13:00
		 *    午前半休時間帯: 9:00-13:00
		 *    午後半休時間帯: 13:00-18:00
		 * の場合、
		 * 午前半休を取ったら 始業時刻を 13:00にして 勤務換算時間 3:00 を設定する。
		 * 午後半休を取ったら 終業時刻を 13:00にして 勤務換算時間 5:00 を設定する
		 *
		 * 勤務換算休憩();と異なる点として、勤務換算休憩として午前半休を9:00-12:00として
		 * 与えると、11:30の出社ができなくなる
		 * 始業時刻を12:00に変えて、勤務換算時間に3:00を設定すると、11:30-12:00は、
		 * 通常の始業時刻前の勤務になり、勤務時間に追加して取り扱われる。
		 *
		 * @return 勤務時間に換算する時間を設定する
		 */
		public Integer convertedWorkTime = 0;

		/**
		 * 時間単位無給休暇の時間を設定する。
		 *
		 * 取得休憩には含めないくてよいが、この時間も休憩として扱うので
		 * 勤務時間から除外される。
		 * 始業就業時間外出社退社時間外の休憩も有効
		 * 勤務換算休憩,勤務換算時間,無給休暇時間との合計が所定勤務時間を越えてはならない。
		 * 勤務換算休憩の無給バージョン
		 *
		 * @return 時間単位無給休暇で申請された休憩時間
		 */
		public AttCalcRangesDto unpaidLeaveRestRanges = new AttCalcRangesDto();

		/**
		 * 半休の無給休暇の換算時間を設定する。
		 *
		 * 半休の無給休暇の時間を設定する
		 * 無給休暇休憩と異なり、勤務から取り除く休憩時間は発生しない。
		 * 勤務換算休憩,勤務換算時間,無給休暇休憩との合計が所定勤務時間を越えてはならない。
		 * 勤務換算時間の無給バージョン
		 *
		 * @return 無給休暇の時間を設定する
		 */
		public Integer unpaidLeaveTime = 0;

		// ----------
		// 勤務体系/その日の勤務パターンやシフトから取得される時間/時刻
		// ----------
		/**
		 * 始業時刻を返す
		 * @return 始業時刻
		 */
		public Integer startTime = 0;
		/**
		 * 終業時刻を返す
		 * @return 終業時刻
		 */
		public Integer endTime = 0;
		/**
		 * 時刻を定めた所定休憩時間帯返す
		 * @return 時刻を定めた所定休憩時間帯
		 */
		public AttCalcRangesDto contractRestRanges = new AttCalcRangesDto();
		/**
		 * 時刻を定めない所定休憩時間を返す
		 * 所定休憩()に追加して、所定休憩時間()だけ休めるものとする
		 * @return 時刻を定めない所定休憩時間
		 */
		public Integer contractRestTime = 0;
		/**
		 * 所定勤務時間を返す
		 *
		 * 始業終業時刻と所定休憩時間から計算される拘束時間は 所定勤務時間>=拘束時間でなくてはならない。
		 * 所定勤務時間<拘束時間 の場合はエラーになる。
		 * 所定勤務時間<拘束時間の設定がどうしても必要な場合、所定休憩時間に 拘束時間-所定勤務時間 を設定して、その分自由に休憩してもよいことを明示する。
		 *
		 * @return 所定勤務時間
		 */
		public Integer contractWorkTime = 0;
	}
	/**
	 * 日次控除計算ロジックの出力パラメタ
	 *
	 * ロジックの出力では、以下の関係が保証されている
	 * {@code 実労働時間 + 遅刻控除時間 + 遅刻補填時間 + 早退控除時間 + 早退補填時間 + 休憩控除時間 + 休憩補填時間 == 所定勤務時間 + 過不足時間}
	 * {@code !((遅刻控除時間>0 || 早退控除時間>0 || 休憩控除時間>0) && 過不足時間>0)}
	 */
	public virtual class Output {
		/**
		 * 実労働時間のSetter
		 *
		 * 始業終業時刻間で実際に働いた時間をセットする
		 * 所定休憩分は取り除かれる
		 *
		 * @param t 実労働時間
		 */
		public Integer realWorkTime = 0;
		/**
		 * 早退時間をセットする
		 *
		 * 勤怠評価用の時間で、所定休憩分は含まない。
		 * 控除免除された時間は除かれる。
		 *
		 * @param t 早退時間
		 */
		public Integer earlyLeaveTime = 0;
		/**
		 * 遅刻時間をセットする
		 *
		 * 勤怠評価用の時間で、所定休憩分は含まない。
		 * 控除免除された時間は除かれる。
		 *
		 * @param t 遅刻時間
		 */
		public Integer lateArriveTime = 0;
		/**
		 * 休憩時間をセットする
		 *
		 * 出社、退社の間で実際に取った休憩時間
		 * 控除免除された時間は除かれる。
		 *
		 * @param t 休憩時間
		 */
		public Integer breakTime = 0;
		/**
		 * 早退控除時間をセットする
		 *
		 * 早退時間のうち、給与からの控除が可能な時間を計算する
		 *
		 * @param t 早退控除時間
		 */
		public Integer earlyLeaveLostTime = 0;
		/**
		 * 遅刻控除時間をセットする
		 *
		 * 遅刻時間のうち、給与からの控除が可能な時間を計算する
		 *
		 * @param t 遅刻控除時間
		 */
		public Integer lateArriveLostTime = 0;
		/**
		 * 休憩控除時間をセットする
		 *
		 * 休憩時間のうち、給与からの控除が可能な時間を計算する
		 *
		 * @param t 休憩控除時間
		 */
		public Integer breakLostTime = 0;
		/**
		 * 早退補填時間をセットする
		 *
		 * 早退した時間のうち、控除が免除された時間をセットする
		 *
		 * @param t 早退補填時間
		 */
		public Integer authorizedEarlyLeaveTime = 0;
		/**
		 * 遅刻補填時間をセットする
		 *
		 * 遅刻した時間のうち、控除が免除された時間をセットする
		 *
		 * @param t 遅刻補填時間
		 */
		public Integer authorizedLateArriveTime = 0;
		/**
		 * 休憩補填時間をセットする
		 *
		 * 休憩した時間のうち、控除が免除された時間をセットする
		 *
		 * @param t 休憩補填時間
		 */
		public Integer authorizedBreakTime = 0;
		/**
		 * 勤務に換算した時間をセットする
		 *
		 * 勤務換算休暇と勤務換算時間の合計をセットする
		 *
		 */
		public Integer convertedWorkTime = 0;
		/**
		 * 無給休暇によって控除される時間をセットする
		 *
		 * 無給休暇休憩と無給休暇時間の合計をセットする
		 *
		 */
		public Integer unpaidLeaveLostTime = 0;
		/**
		 * 時間単位有給休暇によって勤務換算される時間をセットする
		 * この時間は休憩時間にはカウントされない
		 *
		 */
		public Integer paidLeaveTime = 0;
		/**
		 * 時間単位無給休暇によって控除される時間をセットする
		 * この時間は休憩時間にはカウントされない
		 *
		 */
		public Integer unpaidLeaveTime = 0;
		/**
		 * 始業終業の間の勤務状況と、所定勤務時間の差をセットする
		 *
		 * プラスの数値は、所定勤務時間より多く働いている状態
		 * マイナスの数値は、所定勤務時間に足りない状態を表す
		 * {遅刻、早退、休憩}控除は含まれてない。
		 *
		 * @param t 過不足時間
		 */
		public Integer gapTime = 0;

		/**
		 * 勤務換算時間を反映した実効所定勤務時間をセットする
		 *
		 * 所定勤務時間から、勤務換算休暇と勤務換算時間を引いたもの
		 *
		 * @param t 実効所定勤務時間
		 */
		public Integer realContractTime;
		/**
		 * 始業終業時刻の間で引けなかった時刻を指定しない休憩時間
		 *
		 * 時刻を指定しない休憩時間で、始業終業時刻の間に取ったとみなすのは
		 * 所定休憩時間を超えない範囲のみ。
		 * 超えた分は始業終業時刻の外でとったものとみなし、こここでカウントする
		 *
		 * @param t 未処理休憩時間
		 */
		public Integer unknowBreakTime = 0; // 未処理休憩時間;
	}

	/**
	 * 控除計算のロジックを実行する
	 *
	 * @param out ロジックの出力項目
	 * @param in ロジックの入力項目
	 */
	public void apply(Output output, Input input) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcLostTimeLogic.apply start');
		// ----------
		// 出退社時刻を始業終業時刻の間に補正
		//
		// 始業時刻以前の勤務時間、終業時刻以降の勤務時間はこの中では計算しないため、出退社時刻を始業終業の間になるように
		// 補正する。出社>退社の場合も退社時刻を出社時刻以降に補正する。
		// ----------
		Integer inputStartTime = AttCalcRangesDto.R(input.startTime, input.endTime).constrain(input.inputStartTime);
		Integer inputEndTime = AttCalcRangesDto.R(inputStartTime, input.endTime).constrain(input.inputEndTime);
		System.debug(Logginglevel.DEBUG,'---inputStartTime is ' + inputStartTime);
		System.debug(Logginglevel.DEBUG,'---inputEndTime is ' + inputEndTime);
		// ----------
		// 所定勤務時間と追加所定休憩時間の計算
		//
		// 所定勤務時間と始業終業時刻と所定休憩時間から定まる拘束時間を比較して
		// 所定勤務時間>拘束時間の場合、所定勤務時間-拘束時間分は、始業終業外で勤務が必要な時間とみなす。
		// 勤務換算休暇と勤務換算時間がある場合、その分だけ所定勤務時間が短くなったものとする。
		// ----------
		// 始業終業の間の所定休憩
		Integer contractRestTime = input.contractRestRanges.cutBetween(input.startTime,input.endTime).total();
		// 時刻を定めず取れる休憩時間
		Integer freeInputRestTime = input.contractRestTime;
		// 勤務換算時間:勤務換算休憩(時間単位・有休/代休)と勤務換算時間(半日・有休/代休)の時間を足したもの
		final Integer paidLeaveTime = input.convertdRestRanges.total();
		final Integer unpaidLeaveTime = input.unpaidLeaveRestRanges.total();
		output.paidLeaveTime = paidLeaveTime;
		output.unpaidLeaveTime = unpaidLeaveTime;
		final Integer convertedWorkTime = paidLeaveTime + input.convertedWorkTime;
		final Integer unpaidLeaveLostTime = unpaidLeaveTime + input.unpaidLeaveTime;
		output.convertedWorkTime = convertedWorkTime;
		output.unpaidLeaveLostTime = unpaidLeaveLostTime;
		// 始業終業の間で働かなければいけない時間(休憩を除く)
		Integer coreWorkTime = input.endTime - input.startTime - contractRestTime - freeInputRestTime;
		if (!(coreWorkTime >= 0)) {
			throw new ComNormalException('コアタイム時間は0以上にする必要があります');
		}
		if (!(input.contractWorkTime >= coreWorkTime)) {
			throw new ComNormalException('所定労働時間はコアタイム時間以上にする必要があります');
		}
		// 実効所定勤務時間。所定勤務時間から勤務換算休憩・無給休暇休憩があればその分を引いたもの。
		// Javaロジックと差異あり:
		// Javaロジックの in.所定労働時間 は、勤務体系で設定された所定労働時間がそのまま入っている。
		// 一方、このコードの input.contractWorkTime は、もし午前半休・午後半休・全日休があれば既に引かれている。
		Integer realContractTime = input.contractWorkTime - paidLeaveTime - unpaidLeaveTime;
		output.realContractTime = realContractTime;
		// 一日あたりの勤務換算時間は所定勤務時間を越えてはならない。
		if (!(realContractTime >= 0)) {
			throw new ComNormalException('一日あたりの勤務換算時間は所定勤務時間を越えてはなりません。');
		}
		// 所定勤務時間が拘束勤務時間より長い場合、足りない分は始業終業外で勤務しなければいけない時刻
		Integer coreWorkTimeOutContract = Math.max(0, input.contractWorkTime - coreWorkTime);

		// ----------
		// 実労働時間の計算
		//
		// 出社退社時刻の差から、取得した休憩時間を引いて実労働時間を計算する。
		// 休憩時間は、記録されている休憩時刻の中で出退社の時間の間のものと始業終業時刻間の勤務換算時間と取得休憩時間(時間帯を指定せずに申告する休憩)の合計
		// 半休の時間帯は、始業/終業時刻の外にする
		// ----------
		// 実際に取った休憩時間
		AttCalcRangesDto inputRestRanges = input.inputRestRanges.cutBetween(inputStartTime,inputEndTime); // 取得休憩
		// 始業終業の間に取った時刻とみなす時刻を決めない休憩時間
		System.debug(LoggingLevel.DEBUG, '---contractRestTime is ' + contractRestTime);
		System.debug(LoggingLevel.DEBUG, '---freeInputRestTime is ' + freeInputRestTime);
		System.debug(LoggingLevel.DEBUG, '---inputRestRanges.total() is ' + inputRestRanges.total());
		Integer inputRestTimeInContract = Math.max(0, Math.min(input.inputRestTime, contractRestTime + freeInputRestTime - inputRestRanges.total()));
		System.debug(LoggingLevel.DEBUG, '---inputRestTimeInContract is ' + inputRestTimeInContract);
		output.unknowBreakTime = input.inputRestTime - inputRestTimeInContract;
		System.debug(LoggingLevel.DEBUG, '---unknowBreakTime is ' + output.unknowBreakTime);
		Integer inputRestTime = inputRestRanges.total() + inputRestTimeInContract;
		System.debug(LoggingLevel.DEBUG, '---inputRestTime is ' + inputRestTime);
		// 始業終業の間で実際に勤務した時間(勤務時間帯から入力休憩、勤務換算休憩、無給休暇休憩を引く)
		// Javaロジックとの差異:
		// Javaロジックは、出退勤時刻の範囲外に取った時間単位休が考慮されていない(そのまま計算すると、実労働時間が本来より少なくなることがある)
		// このロジックでは、範囲外の時間単位休があっても適切に動作するよう、cutBetweenするようにした
		Integer realWorkTime = inputEndTime
				- inputStartTime
				- inputRestTime
				- input.convertdRestRanges.cutBetween(inputStartTime, inputEndTime).total()
				- input.unpaidLeaveRestRanges.cutBetween(inputStartTime, inputEndTime).total();
		System.debug(LoggingLevel.DEBUG, '---realWorkTime is ' + realWorkTime);
		if (!(realWorkTime >= 0)) {
			throw new ComNormalException('実際に勤務した時間が0以上にする必要があります。');
		}
		output.realWorkTime = realWorkTime;

		// ----------
		// 休憩時間の過不足の計算
		//
		// 取る権利のある休憩時間と実際に取った休憩時間を比較し、実際に取った休憩時間が上回った場合は控除対象にする。
		// 実際に取った休憩時間が下回った場合は、未取得休憩時間として、遅刻早退控除との相殺を行い、それでも余った場合は残業計算に
		// 時間を渡す。
		// 取る権利のある休憩時間とは、出社-退社時刻間の所定休憩と、時刻を定めず取れる休憩時間を足したもの
		// ----------
		// 実際に勤務した時間の中で取れる権利のある休憩時間
		Integer realContractRestTime = input.contractRestRanges.cutBetween(inputStartTime, inputEndTime).total() + freeInputRestTime;
		System.debug(LoggingLevel.DEBUG, '---realContractRestTime is ' + realContractRestTime);
		// 取れるはずの所定勤務時間より多く取った休憩時間
		Integer overRestTime = Math.max(0, inputRestTime - realContractRestTime);
		System.debug(LoggingLevel.DEBUG, '---overRestTime is ' + overRestTime);
		// 申請で許可された休憩は、補填の対象の時間にする
		Integer authorizedBreakTime = Math.min(overRestTime, inputRestRanges.include(input.permitedLostTimeRanges).total());
		output.authorizedBreakTime = authorizedBreakTime;
		// 補填を取り除いた分を休憩時間とする
		Integer breakTime = inputRestTime - authorizedBreakTime;
		System.debug(LoggingLevel.DEBUG, '---breakTime is ' + breakTime);
		output.breakTime = breakTime;
		// 取れるはずなのに取らなかった休憩時間(遅刻や早退などと相殺する)
		Integer unTakedRestTime = Math.max(0, realContractRestTime - inputRestTime);
		// 所定より多く取った休憩時間で許可されていない時間は控除の対象になる
		Integer breakTimeInLostTime = Math.max(0, overRestTime - authorizedBreakTime);
		System.debug(LoggingLevel.DEBUG, '---breakTimeInLostTime is ' + breakTimeInLostTime);
		// ----------
		// 休憩控除時間の計算
		//
		// 遅刻した時間帯や、早退した時間帯に所定休憩が含まれ、同時に控除対象の休憩がある場合、遅刻早退範囲の所定休憩分だけ控除対象の
		// 休憩を減らし、遅刻早退の控除を増やすようにする。
		// 例) 始業終業 9:00-18:00 で 所定休憩 12:00-13:00 で 14:00出社で 15:00-16:30に休憩を取った場合
		//   遅刻控除 = 9:00-14:00(休 12:00-13:00) = 4:00
		//   休憩控除 = 15:00-16:30 = 1:30
		//   の計算を行うが
		//   遅刻時間帯に含まれる所定休憩 1:00 の分だけ 休憩控除を減らし遅刻控除を増やすような処理を行い
		//   遅刻控除 5:00 休憩控除 0:30 にする。
		//   これは、遅刻時間帯にある所定休憩を勤務時間帯に移動して取ったように解釈したことになる。
		// 控除免除(会社都合遅刻早退)を行っている場合、控除免除の時間帯にある所定休憩は、休憩控除との相殺の対象にしない。
		//   上の例で 12:30の会社都合の遅刻申請を行っている場合は
		//   遅刻補填 = 9:00-12:30(休 12:00-12:30) = 3:00 休憩はそのまま
		//   遅刻控除 = 12:30-14:00 = 1:30
		//   休憩控除 = 15:00-16:30(休 12:30-13:00 と相殺) = 1:00
		// というように計算する。
		// ----------
		// 遅刻範囲中で許可控除時間に含まれない所定休憩時間
		Integer contractRestTimeInLateArrive = input.contractRestRanges.cutBetween(input.startTime, inputStartTime).exclude(input.permitedLostTimeRanges).total();
		// 控除対象休憩時間と、遅刻範囲の所定休憩時間を比較して、相殺する時間を計算
		Integer lostRestTimeInLateArrive = Math.min(contractRestTimeInLateArrive, breakTimeInLostTime);
		// 早退範囲中で許可控除時間に含まれない所定休憩時間
		Integer contractRestTimeInEarlyLeave = input.contractRestRanges.cutBetween(inputEndTime, input.endTime).exclude(input.permitedLostTimeRanges).total();
		// 控除するべき休憩時間と、早退範囲の所定休憩時間を比較して、相殺する時間を計算
		Integer lostRestTimeInEarlyLeave = Math.min(contractRestTimeInEarlyLeave, breakTimeInLostTime - lostRestTimeInLateArrive);
		Integer breakLostTime = breakTimeInLostTime - lostRestTimeInLateArrive - lostRestTimeInEarlyLeave;
		output.breakLostTime = breakLostTime;

		// ----------
		// 遅刻時間、遅刻補填時間、遅刻控除時間の計算
		//
		// 遅刻時間の計算をする。
		// 遅刻は始業-出社時刻の間で所定休憩を取り除いたものである。
		// 遅刻補填時間は、遅刻の時間帯で控除免除されている時間
		// 遅刻時間は、遅刻の時間帯で控除免除されていない時間であるが、含まれる所定休憩分を上の休憩控除との相殺を行っている場合はその時間を足す
		// 遅刻控除時間は、遅刻時間のうち、未取得休憩時間と相殺を行った後の時間
		// ----------
		// 遅刻の範囲を計算する(その間の所定休憩・勤務換算休憩・無給休暇休憩は取り除く)
		AttCalcRangesDto lateArriveRanges = AttCalcRangesDto.RS(input.startTime, inputStartTime)
				.exclude(input.contractRestRanges)
				.exclude(input.convertdRestRanges)
				.exclude(input.unpaidLeaveRestRanges);
		System.debug(Logginglevel.DEBUG,'---lateArriveRanges is ' + lateArriveRanges);
		// 遅刻の範囲で許可された時間を計算する
		Integer authorizedLateArriveTime = lateArriveRanges.include(input.permitedLostTimeRanges).total();
		output.authorizedLateArriveTime = authorizedLateArriveTime;
		// 許可されていない分を遅刻時間とする。休憩控除と相殺した所定休憩分も追加する。
		Integer lateArriveTime = lateArriveRanges.total() - authorizedLateArriveTime + lostRestTimeInLateArrive;
		output.lateArriveTime = lateArriveTime;
		// 遅刻時間のうち未取得休憩と相殺できる分を計算する
		Integer unTakedRestTimeInLateArrive = Math.min(unTakedRestTime, lateArriveTime);
		// 控除時間を計算する
		Integer lateArriveLostTime = lateArriveTime - unTakedRestTimeInLateArrive;
		output.lateArriveLostTime = lateArriveLostTime;

		// 遅刻時間の整合性
		if (!(lateArriveRanges.total() == lateArriveTime + authorizedLateArriveTime - lostRestTimeInLateArrive)) {
			throw new ComNormalException('遅刻時間は合いません。');
		}
		if (!(lateArriveLostTime <= lateArriveTime)) {
			throw new ComNormalException('遅刻控除時間は遅刻時間以下にする必要があります。');
		}

		// ----------
		// 早退時間、早退補填時間、早退控除時間の計算
		//
		// 早退時間の計算をする。
		// 早退は退社-終業時刻の間で所定休憩を取り除いたものである。
		// 早退補填時間は、早退の時間帯で控除免除されている時間
		// 早退時間は、早退の時間帯で控除免除されていない時間であるが、含まれる所定休憩分を上の休憩控除との相殺を行っている場合はその時間を足す
		// 早退控除時間は、早退時間のうち、未取得休憩時間と相殺を行った後の時間
		// ----------
		// 早退の範囲を計算する(その間の所定休憩・勤務換算休憩・無給休暇休憩は取り除く)
		AttCalcRangesDto earlyLeaveRages = AttCalcRangesDto.RS(inputEndTime, input.endTime)
				.exclude(input.contractRestRanges)
				.exclude(input.convertdRestRanges)
				.exclude(input.unpaidLeaveRestRanges);
		// 早退の範囲で許可された時間を計算する
		Integer authorizedEarlyLeaveTime = earlyLeaveRages.include(input.permitedLostTimeRanges).total();
		output.authorizedEarlyLeaveTime = authorizedEarlyLeaveTime;
		// 許可されていない分を早退時間とする。休憩控除と相殺した所定休憩分も追加する。
		Integer earlyLeaveTime = earlyLeaveRages.total() - authorizedEarlyLeaveTime + lostRestTimeInEarlyLeave;
		output.earlyLeaveTime = earlyLeaveTime;
		// 早退時間のうち未取得休憩と相殺できる分を計算する
		Integer unTakedRestTimeInEarlyLeave = Math.min(unTakedRestTime - unTakedRestTimeInLateArrive, earlyLeaveTime);
		// 控除時間を計算する
		Integer earlyLeaveLostTime = earlyLeaveTime - unTakedRestTimeInEarlyLeave;
		output.earlyLeaveLostTime = earlyLeaveLostTime;

		// 早退時間の整合性
		if (!(earlyLeaveRages.total() == earlyLeaveTime + authorizedEarlyLeaveTime - lostRestTimeInEarlyLeave)) {
			throw new ComNormalException('早退時間は合いません。');
		}
		if (!(earlyLeaveLostTime <= earlyLeaveTime)) {
			throw new ComNormalException('早退控除時間は早退時間以下にする必要があります。');
		}

		// ----------
		// 過不足時間の計算
		//
		// 未取得休憩時間のうち、遅刻・早退時間と相殺を行った後の時間をプラス
		// 所定勤務時間が拘束時間を上回っている分をマイナスとして
		// ２つを相殺した時間を過不足時間とする
		// ----------
		// 未取得休憩時間と、始業終業外で働かなければいけない時間を相殺して過不足時間とする
		Integer gapTime = unTakedRestTime - unTakedRestTimeInLateArrive - unTakedRestTimeInEarlyLeave - coreWorkTimeOutContract;
		output.gapTime = gapTime;

		// ----------
		// 出力の時間の間の整合性をチェック
		// ----------
		// 始業終業のあいだの{勤務時間}と{控除}と{補填}を足したものは、所定勤務時間と過不足時間を足したものに等しい

		System.debug(LoggingLevel.DEBUG, '---realWorkTime is ' + realWorkTime);
		System.debug(LoggingLevel.DEBUG, '---lateArriveLostTime is ' + lateArriveLostTime);
		System.debug(LoggingLevel.DEBUG, '---authorizedLateArriveTime is ' + authorizedLateArriveTime);
		System.debug(LoggingLevel.DEBUG, '---earlyLeaveLostTime is ' + earlyLeaveLostTime);
		System.debug(LoggingLevel.DEBUG, '---authorizedEarlyLeaveTime is ' + authorizedEarlyLeaveTime);
		System.debug(LoggingLevel.DEBUG, '---breakLostTime is ' + breakLostTime);
		System.debug(LoggingLevel.DEBUG, '---authorizedBreakTime is ' + authorizedBreakTime);
		System.debug(LoggingLevel.DEBUG, '---realContractTime is ' + realContractTime);
		System.debug(LoggingLevel.DEBUG, '---gapTime is ' + gapTime);

		if (!(realWorkTime + lateArriveLostTime + authorizedLateArriveTime + earlyLeaveLostTime + authorizedEarlyLeaveTime + breakLostTime + authorizedBreakTime
				== realContractTime + gapTime)) {
			throw new ComNormalException('勤務時間が合いません。');
		}
		// 控除が発生するときは、勤務時間は余らない
		if ((lateArriveLostTime>0 || earlyLeaveLostTime>0 || breakLostTime>0) && gapTime>0) {
			throw new ComNormalException('控除が発生するときは、勤務時間は余らない');
		}
		System.debug(LoggingLevel.DEBUG, '---AttCalcLostTimeLogic.apply end');
	}
}