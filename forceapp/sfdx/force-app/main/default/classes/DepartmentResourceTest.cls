/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description DepartmentResourceのテストクラス
*/
@isTest
private class DepartmentResourceTest {

	/**
	 * 部署レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComDeptBase__c parent = ComTestDataUtility.createDepartmentWithHistory('Test親部署', company.Id);
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, parent.Id, Userinfo.getUserId(), permission.Id);
		Date currentDate = Date.today();

		Test.startTest();

		DepartmentResource.Department param = new DepartmentResource.Department();
		param.name_L0 = 'テスト部署_L0';
		param.name_L1 = 'テスト部署_L1';
		param.name_L2 = 'テスト部署_L2';
		param.code = '001';
		param.companyId = company.Id;
		param.parentId = parent.Id;
		param.managerId = manager.Id;
		param.remarks = 'Test備考';
		param.validDateFrom = currentDate - 30;
		param.validDateTo = currentDate;
		param.comment = 'テストコメント';

		DepartmentResource.CreateApi api = new DepartmentResource.CreateApi();
		DepartmentResource.SaveResult res = (DepartmentResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 部署ベース・履歴レコードが1件ずつ作成されること
		List<ComDeptBase__c> newBaseRecords = [SELECT
				Id,
				Name,
				Code__c,
				CompanyId__c,
				CurrentHistoryId__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ParentBaseId__c,
						ManagerBaseId__c,
						Remarks__c,
						ValidFrom__c,
						ValidTo__c,
						HistoryComment__c
					FROM Histories__r
				)
			FROM ComDeptBase__c
			WHERE Id != :parent.Id];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].Histories__r.size());

		// 値の検証
		ComDeptBase__c newBase = newBaseRecords[0];
		ComDeptHistory__c newHistory = newBase.Histories__r[0];

		// レスポンス値の検証
		System.assertEquals(newBase.Id, res.Id);

		// ベースレコード値の検証
		System.assertEquals(param.name_L0, newBase.Name);
		System.assertEquals(param.code, newBase.Code__c);
		System.assertEquals(param.companyId, newBase.CompanyId__c);
		System.assertEquals(newHistory.Id, newBase.CurrentHistoryId__c);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = company.Code__c + '-' + param.code +
			'-' + DateTime.newInstance(param.validDateFrom, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd') +
			'-' + DateTime.newInstance(param.validDateTo, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd');
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.name_L0, newHistory.Name);
		System.assertEquals(param.name_L0, newHistory.Name_L0__c);
		System.assertEquals(param.name_L1, newHistory.Name_L1__c);
		System.assertEquals(param.name_L2, newHistory.Name_L2__c);
		System.assertEquals(param.parentId, newHistory.ParentBaseId__c);
		System.assertEquals(param.managerId, newHistory.ManagerBaseId__c);
		System.assertEquals(param.remarks, newHistory.Remarks__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
	}

	/**
	 * 部署レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 部署管理権限を無効に設定する
		testData.permission.isManageDepartment = false;
		new PermissionRepository().saveEntity(testData.permission);

		DepartmentResource.Department param = new DepartmentResource.Department();
		param.name_L0 = 'テスト部署_L0';
		param.code = '001';
		param.companyId = testData.company.Id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				DepartmentResource.CreateApi api = new DepartmentResource.CreateApi();
				DepartmentResource.SaveResult res = (DepartmentResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 部署レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void createNegativeTest() {
		Test.startTest();

		DepartmentResource.Department param = new DepartmentResource.Department();
		DepartmentResource.CreateApi api = new DepartmentResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException  e) {
			ex = e;
		}

		Test.stopTest();
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/**
	 * 部署レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		ComCountry__c country = ComTestDataUtility.createCountry('Test国');
		List<ComCompany__c> companyList = ComTestDataUtility.createCompanies('Test会社', country.Id, 2);
		ComCompany__c oldCompany = companyList[0];
		ComCompany__c newCompany = companyList[1];
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test親部署', oldCompany.Id);

		Test.startTest();

		Map<String, Object> param = new Map<String, Object> {
			'id' => dept.id,
			'code' => '002'
		};

		DepartmentResource.UpdateApi api = new DepartmentResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// 部署レコードが更新されていること
		List<ComDeptBase__c> deptList = [SELECT
				Code__c
			FROM ComDeptBase__c
			WHERE Id =:(String)param.get('id')];

		System.assertEquals(1, deptList.size());
		System.assertEquals((String)param.get('code'), deptList[0].Code__c);
	}

	/**
	 * 部署レコードを1件更新する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 部署管理権限を無効に設定する
		testData.permission.isManageDepartment = false;
		new PermissionRepository().saveEntity(testData.permission);

		Map<String, Object> param = new Map<String, Object> {
			'id' => testData.Department.id,
			'code' => '002'
		};

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				DepartmentResource.UpdateApi api = new DepartmentResource.UpdateApi();
				Object res = api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 部署レコードを1件更新する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		Test.startTest();

		DepartmentResource.Department param = new DepartmentResource.Department();

		App.ParameterException ex;
		try {
			Object res = (new DepartmentResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 部署レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c delRecord = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);

		Test.startTest();

		DepartmentResource.DeleteOption param = new DepartmentResource.DeleteOption();
		param.id = delRecord.id;

		Object res = (new DepartmentResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 部署レコードが削除されること
		List<ComDeptBase__c> departmentList = [SELECT Id FROM ComDeptBase__c];
		System.assertEquals(0, departmentList.size());
	}

	/**
	 * 部署レコードを1件削除する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		DepartmentBaseEntity delDepartment = testData.createDepartment('削除');

		// 部署管理権限を無効に設定する
		testData.permission.isManageDepartment = false;
		new PermissionRepository().saveEntity(testData.permission);

		DepartmentResource.DeleteOption param = new DepartmentResource.DeleteOption();
		param.id = delDepartment.id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Object res = (new DepartmentResource.DeleteApi()).execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 部署レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		DepartmentResource.DeleteOption param = new DepartmentResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new DepartmentResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 部署レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<ComDeptBase__c> departments = ComTestDataUtility.createDepartmentsWithHistory('テスト部署', company.Id, 3);
		ComDeptHistory__c history = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c =: departments[0].Id LIMIT 1];
		history.ParentBaseId__c = departments[1].Id;
		update history;

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => departments[0].id,
			'parentId' => departments[1].Id
		};

		DepartmentResource.SearchApi api = new DepartmentResource.SearchApi();
		DepartmentResource.SearchResult res = (DepartmentResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 部署レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => dept.id
		};

		DepartmentResource.SearchApi api = new DepartmentResource.SearchApi();
		DepartmentResource.SearchResult res = (DepartmentResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 多言語対応項目の検証
		ComDeptHistory__c history = [
				SELECT Name_L1__c,ManagerBaseId__c,
					ManagerBaseId__r.FirstName_L0__c, ManagerBaseId__r.LastName_L0__c,
					ManagerBaseId__r.FirstName_L1__c, ManagerBaseId__r.LastName_L1__c,
					ManagerBaseId__r.FirstName_L2__c, ManagerBaseId__r.LastName_L2__c
				FROM ComDeptHistory__c WHERE BaseId__c =:dept.Id LIMIT 1];
		// ComDeptHistory__c history = [SELECT Name_L1__c, ManagerBaseId__r.FullName_L1__c FROM ComDeptHistory__c WHERE BaseId__c =:dept.Id LIMIT 1];

		System.assertEquals(history.Name_L1__c, res.records[0].name);
		if (history.ManagerBaseId__c != null) {
			String managerName = new AppPersonName(
					history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
					history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
					history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(managerName, res.records[0].manager.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].manager.name));
		}
		// System.assertEquals(history.ManagerBaseId__r.FullName_L1__c, res.records[0].manager.name);
	}

	/**
	 * 部署レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => dept.id
		};

		DepartmentResource.SearchApi api = new DepartmentResource.SearchApi();
		DepartmentResource.SearchResult res = (DepartmentResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		ComDeptHistory__c history = [
				SELECT Name_L2__c,ManagerBaseId__c,
					ManagerBaseId__r.FirstName_L0__c, ManagerBaseId__r.LastName_L0__c,
					ManagerBaseId__r.FirstName_L1__c, ManagerBaseId__r.LastName_L1__c,
					ManagerBaseId__r.FirstName_L2__c, ManagerBaseId__r.LastName_L2__c
				FROM ComDeptHistory__c WHERE BaseId__c =:dept.Id LIMIT 1];
		System.assertEquals(history.Name_L2__c, res.records[0].name);
		if (history.ManagerBaseId__c != null) {
			String managerName = new AppPersonName(
					history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
					history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
					history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(managerName, res.records[0].manager.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].manager.name));
		}
	}

	/**
	 * 部署レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		List<ComDeptBase__c> departments = ComTestDataUtility.createDepartmentsWithHistory('Test親部署', company.Id, 3);
		ComEmpBase__c manager = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, departments[0].Id, Userinfo.getUserId(), permission.Id);
		ComDeptHistory__c testHistory = [SELECT Id FROM ComDeptHistory__c WHERE BaseId__c =:departments[0].Id LIMIT 1];
		testHistory.ParentBaseId__c = departments[1].Id;
		testHistory.ManagerBaseId__c = manager.Id;
		testHistory.Remarks__c = 'テスト備考';
		update testHistory;

		Test.startTest();

		Map<String, Object> param = new Map<String, Object>();
		DepartmentResource.SearchApi api = new DepartmentResource.SearchApi();
		DepartmentResource.SearchResult res = (DepartmentResource.SearchResult)api.execute(param);

		Test.stopTest();

		departments = [
			SELECT
				Id,
				Name,
				Code__c,
				CompanyId__c,
				CurrentHistoryId__c,
				(SELECT
						Id,
						BaseId__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ParentBaseId__c,
						ParentBaseId__r.Name,
						ManagerBaseId__c,
//						ManagerBaseId__r.FullName_L0__c,
						Remarks__c,
						ValidFrom__c,
						ValidTo__c
				FROM Histories__r),
				(SELECT
					Id
				FROM ChildHistories__r
				LIMIT 1)
			FROM ComDeptBase__c];

		// 親部署の履歴
		List<ComDeptHistory__c> parentHistories = [SELECT Name_L0__c, Name_L1__c, Name_L2__c FROM ComDeptHistory__c WHERE BaseId__c = :testHistory.ParentBaseId__c];
		ComDeptHistory__c parentHistory = parentHistories[0];

		// 部署レコードが取得できること
		System.assertEquals(departments.size(), res.records.size());

		for (Integer i = 0, n = departments.size(); i < n; i++) {
			ComDeptBase__c base = departments[i];
			ComDeptHistory__c history = base.Histories__r[0];
			DepartmentResource.Department dto = res.records[i];

			System.assertEquals(base.Id, dto.id);
			System.assertEquals(base.Code__c, dto.code);
			System.assertEquals(base.CompanyId__c, dto.companyId);
			System.assertEquals(base.ChildHistories__r.size() > 0, dto.hasChildren);
			System.assertEquals(history.Id, dto.historyId);
			System.assertEquals(history.Name_L0__c, dto.name);	// 部署名のデフォルトはL0を返す
			System.assertEquals(history.Name_L0__c, dto.name_L0);
			System.assertEquals(history.Name_L1__c, dto.name_L1);
			System.assertEquals(history.Name_L2__c, dto.name_L2);
			System.assertEquals(history.ParentBaseId__c, dto.parentId);
//			System.assertEquals(history.ParentBaseId__r.Name, dto.parent.name);
			if (dto.parentId == null) {
				System.assertEquals(null, dto.parent.name);
			} else {
				System.assertEquals(parentHistory.Name_L0__c, dto.parent.name);  // ユーザ言語がL0なのでL0
			}
			System.assertEquals(history.ManagerBaseId__c, dto.managerId);
			if (dto.managerId == null) {
				System.assertEquals(null, dto.manager.name);
			} else {
				AppPersonName managerName = new AppPersonName(
						manager.FirstName_L0__c, manager.LastName_L0__c,
						manager.FirstName_L1__c, manager.LastName_L1__c,
						manager.FirstName_L2__c, manager.LastName_L2__c);
				System.assertEquals(managerName.getFullName(), dto.manager.name);
			}
//			System.assertEquals(history.ManagerBaseId__r.FullName_L0__c, dto.manager.name);
			System.assertEquals(history.ValidFrom__c, dto.validDateFrom);
			System.assertEquals(history.ValidTo__c, dto.validDateTo);
			System.assertEquals(history.Remarks__c, dto.remarks);
		}

	}

}
