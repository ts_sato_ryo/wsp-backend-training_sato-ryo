/*
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * ISO Currency Code pick list
 */
public class ComIsoCurrencyCode extends ValueObjectType {

	/** List of the entries */
	private static final List<ComIsoCurrencyCode> TYPE_LIST;
	/** Map of the entries (Key : value of the entry) */
	private static final Map<String, ComIsoCurrencyCode> TYPE_MAP;

	/**
	 * Static Initializer
 	 */
	static {
		TYPE_LIST = new List<ComIsoCurrencyCode>();
		TYPE_MAP = new Map<String, ComIsoCurrencyCode>();

		List<Schema.PicklistEntry> pickList = ComCurrency__c.IsoCurrencyCode__c.getDescribe().getPicklistValues();

		for (Schema.PicklistEntry entry : pickList) {
			// Set only active entry
			if (entry.isActive()) {
				ComIsoCurrencyCode c = new ComIsoCurrencyCode(entry.getValue(), entry.getLabel());
				TYPE_LIST.add(c);
				TYPE_MAP.put(c.value, c);
			}
		}
	}

	/**
	 * Constructor
	 * @param value Value of the pick list entry
	 * @param label Label of the pick list entry
	 */
	private ComIsoCurrencyCode(String value, String label) {
		super(value, label);
	}

	/**
   * Return the value whose key matches with specified string
   * @param The key value to get the instance
   * @return The instance having the value specified
   */
	public static ComIsoCurrencyCode valueOf(String value) {
		return TYPE_MAP.get(value);
	}

	/**
	 * Return the type list
	 * @return TYPE_LIST
	 */
	public static List<ComIsoCurrencyCode> getTypeList() {
		return TYPE_LIST;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {
		// if null, return false.
		if (compare == null) {
			return false;
		}

		// if not a instance of ComIsoCurrencyCode, return false.
		if (!(compare instanceof ComIsoCurrencyCode)) {
			return false;
		}

		ComIsoCurrencyCode compareObj = (ComIsoCurrencyCode) compare;
		return (this.value.equals(compareObj.value));
	}
}