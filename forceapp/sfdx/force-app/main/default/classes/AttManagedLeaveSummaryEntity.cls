/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇サマリのエンティティ
 */
public with sharing class AttManagedLeaveSummaryEntity extends Entity{
	/** 項目の定義(変更管理で使用する) */
	public enum Field {
			ATT_SUMMARY_ID, LEAVE_ID, DAYS_TAKEN, DAYS_LEFT, DAYS_EXPIRED, DAYS_ADJUSTED
	}
	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;
	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}
	public AttManagedLeaveSummaryEntity() {
		isChangedFieldSet = new Set<Field>();
	}
	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}
	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}
	
	/** 勤怠サマリ */
	public Id attSummaryId {
		get;
		set {
			attSummaryId = value;
			setChanged(Field.ATT_SUMMARY_ID);
		}
	}
	/** 社員Id */
	public Id employeeId {get; set ;}
	/** 休暇 */
	public Id leaveId {
		get;
		set {
			leaveId = value;
			setChanged(Field.LEAVE_ID);
		}
	}
	/** 取得日数 */
	public AttDays daysTaken {
		get;
		set {
			daysTaken = value;
			setChanged(Field.DAYS_TAKEN);
		}
	}
	/** 残日数 */
	public AttDays daysLeft {
		get;
		set {
			daysLeft = value;
			setChanged(Field.DAYS_LEFT);
		}
	}
		/** 失効日数(確定分) */
	public AttDays daysExpired {
		get;
		set {
			daysExpired = value;
			setChanged(Field.DAYS_EXPIRED);
		}
	}
	/** 調整日数(確定分) */
	public AttDays daysAdjusted {
		get;
		set {
			daysAdjusted = value;
			setChanged(Field.DAYS_ADJUSTED);
		}
	}
	/** 開始日 */
	public AppDate startDate {get; set;}
	/** 終了日 */
	public AppDate endDate {get; set;}
	/** 付与日数 */
	public AttDays daysGranted {get; set;}
	public AppMultiString leaveNameL {get; set;}
	/** 休暇付与サマリ */
	public List<AttManagedLeaveGrantSummaryEntity> grantSummaryList {get; set;}
	/** 休暇タイプ */
	public AttLeaveType leaveType {get; set;}
	/** 最終更新日 */
	public AppDatetime lastModifiedDate {get; set;}
}