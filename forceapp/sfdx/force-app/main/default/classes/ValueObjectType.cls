/**
 * 種別を定義する値オブジェクトの基底クラス
 */
public abstract with sharing class ValueObjectType extends ValueObject {

	/** 値 */
	public String value {get; protected set;}

	/** 表示ラベル */
	public String label {get; protected set;}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	protected ValueObjectType(String value) {
		this.value = value;
	}

	/**
	 * コンストラクタ（値とラベル指定）
	 * @param value 値
	 * @param label ラベル
	 */
	protected ValueObjectType(String value, String label) {
		this.value = value;
		this.label = label;
	}

	/**
	 * valueを取得する
	 * @param type 取得対象のValueObjectType
	 * @return valueの値、ただしtypeがnullの場合はnull
	 */
	public static String getValue(ValueObjectType type) {
		if (type == null) {
			return null;
		}
		return type.value;
	}

	/** toStringのオーバーライド */
	public override String toString() {
		String outStr = this.value;
		if (this.label != null) {
			outStr += '(label:' + this.label + ')';
		}
		return outStr;
	}
}
