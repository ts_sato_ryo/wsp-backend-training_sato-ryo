/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Resource class for Accounting Period
 */
public with sharing class ExpAccountingPeriodResource  {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** Record class of ExpAccountingPeriod */
	abstract public class ExpAccountingPeriodBase {
		/** Record ID */
		public String id;
		/** Accounting Period code */
		public String code;
		/** Company ID */
		public String companyId;
		/** Currency Name */
		public String name;
		/** Currency Name(lang0) */
		public String name_L0;
		/** Currency Name(lang1) */
		public String name_L1;
		/** Currency Name(lang2) */
		public String name_L2;
		/** Start Date */
		public Date validDateFrom;
		/** End Date */
		public Date validDateTo;
		/** Recording Date */
		public Date recordingDate;
		/** Active */
		public Boolean active;
		/** IsNotMaster */
		public Boolean isNotMaster;

		/** Create entity from request parameter */
		public ExpAccountingPeriodEntity createEntity(Map<String, Object> paramMap) {
			ExpAccountingPeriodEntity entity = new ExpAccountingPeriodEntity();

			entity.setId(this.id);
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('name_L0')) {
				entity.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('validDateFrom')){
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (paramMap.containsKey('validDateTo')) {
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (paramMap.containsKey('recordingDate')) {
				entity.recordingDate = AppDate.valueOf(this.recordingDate);
			}
			if (paramMap.containsKey('active')) {
				entity.active = this.active;
			}

			return entity;
		}

		/** Set data from entity */
		public void setData(ExpAccountingPeriodEntity entity) {
			this.id = entity.id;
			this.code = entity.code;
			this.companyId = entity.companyId;
			this.name = entity.nameL.getValue();
			this.name_L0 = entity.nameL0;
			this.name_L1 = entity.nameL1;
			this.name_L2 = entity.nameL2;
			this.validDateFrom = AppConverter.dateValue(entity.validFrom);
			this.validDateTo = AppConverter.dateValue(entity.validTo);
			this.recordingDate = AppConverter.dateValue(entity.recordingDate);
			this.active = entity.active;
		}
	}

	/** Accounting Period record class */
	public class ExpAccountingPeriodRecord extends ExpAccountingPeriodBase {

	}

	/**
   * @description Parameter for new creation and update
   */
	public class UpsertParam extends ExpAccountingPeriodBase implements RemoteApi.RequestParam {
	}

	/**
 * @description Parameter for deletion
 */
	public class DeleteParam implements RemoteApi.RequestParam {
		/** Record Id */
		public String id;
	}

	/**
   * Parameter for search
   * */
	public class SearchParam implements RemoteApi.RequestParam {
		public String companyId;
		public boolean active;
		public boolean isNotMaster;
	}

	/**
   * @description Result of saving
   */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** ID of the created record */
		public String id;
	}

	/**
   * @description Result of search
   */
	public class SearchResult implements RemoteApi.ResponseParam {
		public ExpAccountingPeriodRecord[] records;
	}

	/**
	 * @description API to create a new Accounting Period record
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_ACCOUNTING_PERIOD;

		/**
		 * @description Create a new Accounting Period record
		 * @param  req Request parameter (UpsertParam)
		 * @return Response (SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			UpsertParam param = (UpsertParam) req.getParam(UpsertParam.class);

			validateParam(param);

			ExpAccountingPeriodEntity entity = param.createEntity(req.getParamMap());
			Repository.SaveResult result = ExpAccountingPeriodService.createAccountingPeriod(entity);

			SaveResult response = new SaveResult();
			response.id = result.details[0].id;
			return response;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 * */
		private void validateParam(UpsertParam param) {
			ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Com_Lbl_Code, param.code);
			ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Admin_Lbl_Name, param.name_L0);
			ExpCommonUtil.validateId('Company ID', param.companyId, true);
			ExpCommonUtil.validateDate(MESSAGE.Admin_Lbl_StartDate, String.valueOf(param.validDateFrom));
			ExpCommonUtil.validateDate(MESSAGE.Admin_Lbl_EndDate, String.valueOf(param.validDateTo));
			ExpCommonUtil.validateDate(MESSAGE.Admin_Lbl_RecordingDate, String.valueOf(param.recordingDate));
		}
	}

	/**
 * @description API to update a Accounting Period record
 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_ACCOUNTING_PERIOD;

		/**
	 * @description Update a record
	 * @param  req Request parameter (UpsertParam)
	 * @return Response null
	 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			UpsertParam param = (UpsertParam) req.getParam(UpsertParam.class);

			validateParam(param);

			ExpAccountingPeriodEntity entity = param.createEntity(req.getParamMap());
			ExpAccountingPeriodService.updateAccountingPeriod(entity);

			return null;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 **/
		private void validateParam(UpsertParam param) {
			ExpCommonUtil.validateId('ID', param.id, true);
		}

	}

	/**
 * @description API to delete a Accounting Period record
 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_ACCOUNTING_PERIOD;

		/**
	   * @description Delete a Accounting Period records
	   * @param  req Request parameter (DeleteParam)
	   * @return Response null
	   */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			DeleteParam param = (DeleteParam) req.getParam(DeleteParam.class);

			validateParam(param);

			ExpAccountingPeriodService.deleteAccountingPeriod(param.id);

			return null;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 **/
		private void validateParam(DeleteParam param) {
			ExpCommonUtil.validateId('ID', param.id, true);
		}

	}

	/**
 * @description API to search Accounting Period records
 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search Accounting Period records
		 * @param  req Request parameter (SearchParam)
	 	* @return Response (SearchResult)
	 	*/
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SearchParam param = (SearchParam)req.getParam(SearchParam.class);

			List<ExpAccountingPeriodEntity> entityList = ExpAccountingPeriodService.searchAccountingPeriod(req.getParamMap());

			Boolean isNotMaster = false;
			if(req.getParamMap().containsKey('isNotMaster')){
				isNotMaster = param.isNotMaster;
			}

			return makeResponse(entityList, isNotMaster);
		}

		/**
		 * Make response records
		 * @param entityList Records of Accounting Period entity
		 */
		private SearchResult makeResponse(List<ExpAccountingPeriodEntity> entityList, Boolean isNotMaster) {

			List<ExpAccountingPeriodRecord> records = new List<ExpAccountingPeriodRecord>();

			for (ExpAccountingPeriodEntity entity : entityList) {
				ExpAccountingPeriodRecord rec = new ExpAccountingPeriodRecord();
				rec.setData(entity);
				records.add(rec);
			}

			if (isNotMaster) {
				for (ExpAccountingPeriodRecord rec : records) {
					rec.validDateTo = rec.validDateTo.addDays(-1);
				}
			}

			SearchResult result = new SearchResult();
			result.records = records;
			return result;
		}
	}
}