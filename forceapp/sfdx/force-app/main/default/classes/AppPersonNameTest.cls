@isTest
private class AppPersonNameTest {

	/**
	 * ユーザの言語で文字列が取得できることを確認する(L0)
	 */
	@isTest private static void getFullNameTestL0() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('en_US', 'ja', null);
		User userL0 = ComTestDataUtility.createUser('TestLang0', 'en_US', UserInfo.getLocale());
//		User userL1 = ComTestDataUtility.createUser('TestLang1', 'ja', UserInfo.getLocale());
//		User userL2 = ComTestDataUtility.createUser('TestLang2', 'ja', UserInfo.getLocale());

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';

		// L0ユーザ(en_US)
		System.runAs(userL0) {
			AppPersonName name = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
			System.assertEquals(firstNameL0 + ' ' + lastNameL0, name.getFullName());
		}

	}

	/**
	 * ユーザの言語で文字列が取得できることを確認する(L1)
	 */
	@isTest private static void getFullNameTestL1() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('en_US', 'ja', null);
//		User userL0 = ComTestDataUtility.createUser('TestLang0', 'en_US', UserInfo.getLocale());
		User userL1 = ComTestDataUtility.createUser('TestLang1', 'ja', UserInfo.getLocale());
//		User userL2 = ComTestDataUtility.createUser('TestLang2', 'ja', UserInfo.getLocale());

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';

		// L1(ja)
		System.runAs(userL1) {
			AppPersonName name = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
			System.assertEquals(lastNameL1 + ' ' + firstNameL1, name.getFullName());
		}
	}

	/**
	 * ユーザの言語で文字列が取得できることを確認する(L2)
	 */
	@isTest private static void getFullNameTestL2() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('en_US', 'en_US', 'ja');
//		User userL0 = ComTestDataUtility.createUser('TestLang0', 'en_US', UserInfo.getLocale());
//		User userL1 = ComTestDataUtility.createUser('TestLang1', 'ja', UserInfo.getLocale());
		User userL2 = ComTestDataUtility.createUser('TestLang2', 'ja', UserInfo.getLocale());

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';

		System.runAs(userL2) {
			AppPersonName name = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
			System.assertEquals(lastNameL2 + ' ' + firstNameL2, name.getFullName());
		}
	}

	/**
	 * ユーザの言語がAppLanguageに存在しない言語で組織設定で英語の項目が設定されている場合、
	 * 英語の氏名が取得できることを確認する。
	 */
	@isTest private static void getFullNameTestUnsupportedLang() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('ja', 'en_US', 'ja');
		User userOther = ComTestDataUtility.createUser('TestLang', 'de', UserInfo.getLocale());

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';

		System.runAs(userOther) {
			AppPersonName name = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
			System.assertEquals(firstNameL1 + ' ' + lastNameL1, name.getFullName());
		}
	}

	/**
	 * ユーザの言語がAppLanguageに存在しない言語で組織設定で英語の項目が設定されていない場合、
	 * L0の氏名が取得できることを確認する。
	 */
	@isTest private static void getFullNameTestUnsupportedLangAndNotSetEn() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		org.Language_1__c = 'ja';
		update org;
		User userOther = ComTestDataUtility.createUser('TestLang', 'de', UserInfo.getLocale());

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';

		System.runAs(userOther) {
			AppPersonName name = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
			System.assertEquals(lastNameL0 + ' ' + firstNameL0, name.getFullName());
		}
	}

	/**
	 * 姓のみ、名のみしか設定していなくても氏名が取得できることを確認する
	 */
	@isTest private static void getFullNameTestNameBlank() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('en_US', 'ja', null);
		User userL0 = ComTestDataUtility.createUser('TestLang0', 'en_US', UserInfo.getLocale());
		User userL1 = ComTestDataUtility.createUser('TestLang1', 'ja', UserInfo.getLocale());
		User userL2 = ComTestDataUtility.createUser('TestLang2', 'ja', UserInfo.getLocale());

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';

		// 姓、名のどとらかが設定されていなくても氏名が取得できる
		System.runAs(userL0) {
			// 姓のみ設定
			AppPersonName name = new AppPersonName(null, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
			System.assertEquals(lastNameL0, name.getFullName());

			// 名のみ設定
			name = new AppPersonName(firstNameL0, ' ', firstNameL1, lastNameL1, firstNameL2, lastNameL2);
			System.assertEquals(firstNameL0, name.getFullName());
		}
	}

	/**
	 * 指定した言語で氏名が取得できることを確認する
	 */
	@isTest private static void getFullNameTestLanguage() {

		// テストデータ
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('en_US', null, 'ja');

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';

		AppPersonName name = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
		System.assertEquals(firstNameL0 + ' ' + lastNameL0, name.getFullName(AppLanguage.EN_US));
		System.assertEquals(lastNameL2 + ' ' + firstNameL2, name.getFullName(AppLanguage.JA));
	}

	/**
	 * equalsのテスト
	 */
	@isTest private static void equalsTest() {

		String firstNameL0 = 'Hanako_0';
		String lastNameL0 = 'Yamada_0';
		String firstNameL1 = 'Hanako_1';
		String lastNameL1 = 'Yamada_1';
		String firstNameL2 = 'Hanako_2';
		String lastNameL2 = 'Yamada_2';
		AppPersonName name = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
		AppPersonName compName;

		// 型が異なる場合はfalse
		System.assertEquals(false, name.equals(firstNameL0));

		// null場合はfalse
		System.assertEquals(false, name.equals(null));

		// すべて一致していればtrue
		compName = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
		System.assertEquals(true, name.equals(compName));

		// 1個の属性でも値が等しくなければfalse
		compName = new AppPersonName(firstNameL0 + 'a', lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2);
		System.assertEquals(false, name.equals(compName));

		compName = new AppPersonName(firstNameL0, lastNameL0 + 'a', firstNameL1, lastNameL1, firstNameL2, lastNameL2);
		System.assertEquals(false, name.equals(compName));

		compName = new AppPersonName(firstNameL0, lastNameL0, firstNameL1 + 'a', lastNameL1, firstNameL2, lastNameL2);
		System.assertEquals(false, name.equals(compName));

		compName = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1 + 'a', firstNameL2, lastNameL2);
		System.assertEquals(false, name.equals(compName));

		compName = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2 + 'a', lastNameL2);
		System.assertEquals(false, name.equals(compName));

		compName = new AppPersonName(firstNameL0, lastNameL0, firstNameL1, lastNameL1, firstNameL2, lastNameL2 + 'a');
		System.assertEquals(false, name.equals(compName));
	}
}
