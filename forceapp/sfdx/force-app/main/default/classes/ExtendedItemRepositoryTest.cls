/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * ExtendedItemRepositoryのテスト
 */
@isTest
private class ExtendedItemRepositoryTest {

	private class RepoTestData {
		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public List<ComCompany__c> companyObjs;
		/** 拡張項目オブジェクト */
		public List<ComExtendedItem__c> extendedItemObjs;

		/** コンストラクタ */
		public RepoTestData(Integer extendedItemSize) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObjs = new List<ComCompany__c>{
					ComTestDataUtility.createCompany('Company1', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company2', this.countryObj.Id),
					ComTestDataUtility.createCompany('Company3', this.countryObj.Id)
			};
			extendedItemObjs = ComTestDataUtility.createExtendedItems('Test Extended Item', this.companyObjs[0].Id, extendedItemSize);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityTest() {
		final Integer extendedItemSize = 3;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		ComExtendedItem__c target = testData.extendedItemObjs[1];
		ExtendedItemEntity entity = new ExtendedItemRepository().getEntity(target.Id);

		TestUtil.assertNotNull(entity);
		assertEntity(target, entity);
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのレコードが存在しない場合、nullが返却されることを確認する
	 */
	@isTest static void getEntityTestNotFound() {
		final Integer extendedItemSize = 3;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		Id targetId = UserInfo.getUserId();
		ExtendedItemEntity entity = new ExtendedItemRepository().getEntity(targetId);

		System.assertEquals(null, entity);
	}

	/**
	 * getEntityListのテスト
	 * 指定したIDで取得できることを確認する
	 */
	@isTest static void getEntityListTest() {
		final Integer extendedItemSize = 5;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		ComExtendedItem__c target1 = testData.extendedItemObjs[1];
		ComExtendedItem__c target2 = testData.extendedItemObjs[2];
		List<ExtendedItemEntity> entities =
				new ExtendedItemRepository().getEntityList(new List<Id>{target2.Id, target1.Id});

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * getEntityByCodeのテスト
	 * 指定したコードのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityByCodeTest() {
		final Integer extendedItemSize = 5;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		ComExtendedItem__c targetObj = testData.extendedItemObjs[1];
		targetObj.Code__c = 'Code Test';
		targetObj.CompanyId__c = testData.companyObjs[2].Id;
		update targetObj;
		ExtendedItemEntity entity = new ExtendedItemRepository().getEntityByCode(targetObj.CompanyId__c, targetObj.Code__c);

		TestUtil.assertNotNull(entity);
		assertEntity(targetObj, entity);
	}

	/**
	 * searchEntityListのテスト
	 * 全ての項目の値が正しく取得できていることを確認する
	 */
	@isTest static void searchEntityListTestAllField() {
		final Integer extendedItemSize = 5;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		ComExtendedItem__c targetObj = testData.extendedItemObjs[1];

		ExtendedItemRepository.SearchFilter filter = new ExtendedItemRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		List<ExtendedItemEntity> entities = new ExtendedItemRepository().searchEntityList(filter);

		System.assertEquals(1, entities.size());
		assertEntity(targetObj, entities[0]);
	}

	/**
	 * searchEntityListのテスト
	 * 指定した条件で取得できることを確認する
	 */
	@isTest static void searchEntityListTestFilter() {
		final Integer extendedItemSize = 5;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		ComExtendedItem__c targetObj = testData.extendedItemObjs[1];
		targetObj.Code__c = 'Code Test';
		targetObj.CompanyId__c = testData.companyObjs[2].Id;
		update targetObj;

		ExtendedItemRepository.SearchFilter filter;
		ExtendedItemRepository repo = new ExtendedItemRepository();
		List<ExtendedItemEntity> resEntities;

		// TEST1: IDで検索
		filter = new ExtendedItemRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

		// TEST2: 会社IDで検索
		filter = new ExtendedItemRepository.SearchFilter();
		filter.companyIds = new Set<Id>{targetObj.CompanyId__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

		// TEST3: コードで検索
		filter = new ExtendedItemRepository.SearchFilter();
		filter.codes = new Set<String>{targetObj.Code__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);

		// TEST4: 通常は論理削除済みのジョブタイプは検索されない
		targetObj.Removed__c = true;
		update targetObj;
		filter = new ExtendedItemRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(0, resEntities.size());

		// TEST5: 論理削除済みのジョブタイプも検索対象
		targetObj.Removed__c = true;
		update targetObj;
		filter = new ExtendedItemRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		filter.includeRemoved = true;
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
	}

	/**
	 * saveEntityのテスト
	 * 新規保存できることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		final Integer extendedItemSize = 0;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		ExtendedItemEntity entity = new ExtendedItemEntity();
		entity.name = 'Test';
		entity.nameL0 = 'Test_L0';
		entity.nameL1 = 'Test_L1';
		entity.nameL2 = 'Test_L2';
		entity.code = 'TestCode';
		entity.uniqKey = testData.companyObjs[0].Code__c + '-' + entity.code;
		entity.companyId = testData.companyObjs[0].Id;
		entity.inputType = ExtendedItemInputType.TEXT;
		entity.limitLength = 50;
		entity.defaultValueText = 'Default Value';
		entity.descriptionL0 = 'Description_L0';
		entity.descriptionL1 = 'Description_L1';
		entity.descriptionL2 = 'Description_L2';
		entity.isRemoved = true;

		Repository.SaveResult result = new ExtendedItemRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ComExtendedItem__c resObj = getSObject(result.details[0].id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityのテスト
	 * 既存のエンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestUpdate() {
		final Integer extendedItemSize = 1;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		ComExtendedItem__c orgObj = testData.extendedItemObjs[0];
		ExtendedItemEntity entity = new ExtendedItemEntity();
		entity.setId(orgObj.Id);
		entity.name = orgObj.Name + '_Update';
		entity.nameL0 = orgObj.Name_L0__c + '_Update';
		entity.nameL1 = orgObj.Name_L1__c + '_Update';
		entity.nameL2 = orgObj.Name_L2__c + '_Update';
		entity.code = orgObj.Code__c + '_Update';
		entity.uniqKey = testData.companyObjs[0].Code__c + '-' + entity.code + '_Update';
		entity.companyId = testData.companyObjs[1].Id;
		entity.inputType = ExtendedItemInputType.TEXT;
		entity.limitLength = 100;
		entity.defaultValueText = orgObj.DefaultValueText__c + '_Update';
		entity.descriptionL0 = orgObj.Description_L0__c + '_Update';
		entity.descriptionL1 = orgObj.Description_L1__c + '_Update';
		entity.descriptionL2 = orgObj.Description_L2__c + '_Update';
		entity.isRemoved = true;

		Repository.SaveResult result = new ExtendedItemRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ComExtendedItem__c resObj = getSObject(orgObj.Id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityListのテスト
	 * 新規、既存のエンティティが保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		final Integer extendedItemSize = 1;
		RepoTestData testData = new RepoTestData(extendedItemSize);

		// 既存
		ComExtendedItem__c orgObj = testData.extendedItemObjs[0];
		ExtendedItemEntity entityUd = new ExtendedItemEntity();
		entityUd.setId(orgObj.Id);
		entityUd.name = orgObj.Name + '_Update';
		entityUd.nameL0 = orgObj.Name_L0__c + '_Update';
		entityUd.nameL1 = orgObj.Name_L1__c + '_Update';
		entityUd.nameL2 = orgObj.Name_L2__c + '_Update';
		entityUd.code = orgObj.Code__c + '_Update';
		entityUd.uniqKey = testData.companyObjs[0].Code__c + '-' + entityUd.code + '_Update';
		entityUd.companyId = testData.companyObjs[1].Id;
		entityUd.inputType = ExtendedItemInputType.TEXT;
		entityUd.limitLength = 100;
		entityUd.defaultValueText = orgObj.DefaultValueText__c + '_Update';
		entityUd.descriptionL0 = orgObj.Description_L0__c + '_Update';
		entityUd.descriptionL1 = orgObj.Description_L1__c + '_Update';
		entityUd.descriptionL2 = orgObj.Description_L2__c + '_Update';
		entityUd.isRemoved = true;

		// 新規
		ExtendedItemEntity entityNew = new ExtendedItemEntity();
		entityNew.name = 'TestNew';
		entityNew.nameL0 = 'TestNew_L0';
		entityNew.nameL1 = 'TestNew_L1';
		entityNew.nameL2 = 'TestNew_L2';
		entityNew.code = 'TestCode';
		entityNew.uniqKey = testData.companyObjs[0].Code__c + '-' + entityNew.code;
		entityNew.companyId = testData.companyObjs[0].Id;
		entityNew.inputType = ExtendedItemInputType.TEXT;
		entityNew.limitLength = 100;
		entityNew.defaultValueText = 'Default Value';
		entityNew.descriptionL0 = 'DescriptionNew_L0';
		entityNew.descriptionL1 = 'DescriptionNew_L1';
		entityNew.descriptionL2 = 'DescriptionNew_L2';
		entityNew.isRemoved = false;

		Repository.SaveResult result = new ExtendedItemRepository().saveEntityList(new List<ExtendedItemEntity>{entityUd, entityNew});

		System.assertEquals(true, result.isSuccessAll);
		ComExtendedItem__c resObjUd = getSObject(orgObj.Id);
		assertSObject(entityUd, resObjUd);
		ComExtendedItem__c resObjNew = getSObject(result.details[1].id);
		assertSObject(entityNew, resObjNew);
	}

	/**
	 * 取得したエンティティが期待値通りであることを確認する
	 */
	private static void assertEntity(ComExtendedItem__c expObj, ExtendedItemEntity actEntity) {
		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.InputType__c, ExtendedItemInputType.getValue(actEntity.inputType));
		System.assertEquals(expObj.LimitLength__c, actEntity.limitLength);
		System.assertEquals(expObj.DefaultValueText__c, actEntity.defaultValueText);
		System.assertEquals(expObj.Description_L0__c, actEntity.descriptionL0);
		System.assertEquals(expObj.Description_L1__c, actEntity.descriptionL1);
		System.assertEquals(expObj.Description_L2__c, actEntity.descriptionL2);
	}

	/**
	 * 保存したSObjectが期待値通りであることを確認する
	 */
	private static void assertSObject(ExtendedItemEntity expEntity, ComExtendedItem__c actObj) {
		System.assertEquals(expEntity.name, actObj.name);
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.inputType, ExtendedItemInputType.valueOf(actObj.InputType__c));
		System.assertEquals(expEntity.limitLength, actObj.LimitLength__c);
		System.assertEquals(expEntity.defaultValueText, actObj.DefaultValueText__c);
		System.assertEquals(expEntity.descriptionL0, actObj.Description_L0__c);
		System.assertEquals(expEntity.descriptionL1, actObj.Description_L1__c);
		System.assertEquals(expEntity.descriptionL2, actObj.Description_L2__c);
		System.assertEquals(expEntity.isRemoved, actObj.Removed__c);
	}

	/**
	 * 指定したIDのComExtendedItemを取得する
	 */
	private static ComExtendedItem__c getSObject(Id id) {
		return [
				SELECT Id, Name, Name_L0__c, Name_L1__c, Name_L2__c, Code__c, UniqKey__c, CompanyId__c, InputType__c,
						LimitLength__c, DefaultValueText__c, Description_L0__c, Description_L1__c, Description_L2__c,
						Removed__c
				FROM ComExtendedItem__c
				WHERE Id = :id];
	}
}