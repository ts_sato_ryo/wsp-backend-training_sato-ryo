/**
 * @group 勤怠
 * @description AttBaseResourceのテストクラス
 */
@isTest
private class AttBaseResourceTest {

	/**
	 * enableMasterCaches()のテスト
	 * キャッシュが有効化されていることを確認する
	 */
	@isTest
	static void enableMasterCachesTest() {
		AttBaseResource resource = new AttResource.GetTimesheetApi();
		resource.enableMasterCaches();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		// 親子型マスタ
		System.assertEquals(true, new EmployeeRepository().isEnabledCache);
		System.assertEquals(true, new DepartmentRepository().isEnabledCache);
		System.assertEquals(true, new AttWorkingTypeRepository().isEnabledCache);
		System.assertEquals(true, new TimeSettingRepository().isEnabledCache);
		System.assertEquals(true, new AttShortTimeSettingRepository().isEnabledCache);
		// 有効期間型マスタ
		System.assertEquals(true, new AttAgreementAlertSettingRepository().isEnabledCache);
		System.assertEquals(true, new AttLeaveOfAbsenceRepository().isEnabledCache);
		System.assertEquals(true, new AttLeaveRepository().isEnabledCache);
		System.assertEquals(true, new AttPatternRepository2().isEnabledCache);
		// 論理削除型マスタ
		System.assertEquals(true, new CalendarRepository().isEnabledCache);
		System.assertEquals(true, new CompanyRepository().isEnabledCache);
	}
}
