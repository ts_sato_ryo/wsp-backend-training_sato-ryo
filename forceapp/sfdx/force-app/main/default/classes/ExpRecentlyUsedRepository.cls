/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description
 * 
 * @group Expense
 */
public with sharing class ExpRecentlyUsedRepository extends Repository.BaseRepository {

	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpRecentlyUsed__c.Id,
			ExpRecentlyUsed__c.Name,
			ExpRecentlyUsed__c.EmployeeBaseId__c,
			ExpRecentlyUsed__c.MasterType__c,
			ExpRecentlyUsed__c.TargetId__c,
			ExpRecentlyUsed__c.RecentlyUsedValues__c
		};
		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	public class SearchFilter {
		public Id employeeBaseId;
		public ExpRecentlyUsedEntity.MasterType masterType;
		public Set<String> targetIds;
	}

	public List<ExpRecentlyUsedEntity> searchEntity(SearchFilter filter) {
		Set<String> pTargetIds;
		Id pEmployeeBaseId;
		String pMasterType;

		List<String> whereList = new List<String>();
		if (filter.targetIds != null) {
			pTargetIds = filter.targetIds;
			whereList.add(getFieldName(ExpRecentlyUsed__c.TargetId__c) + ' IN :pTargetIds');
		}
		if (filter.employeeBaseId != null) {
			pEmployeeBaseId = filter.employeeBaseId;
			whereList.add(getFieldName(ExpRecentlyUsed__c.EmployeeBaseId__c) + ' = :pEmployeeBaseId');
		}
		if (filter.masterType != null) {
			pMasterType = filter.masterType.name();
			whereList.add(getFieldName(ExpRecentlyUsed__c.MasterType__c) + ' = :pMasterType');
		}
		String whereString = buildWhereString(whereList);

		List<String> selectFieldList = new List<String>();
		selectFieldList.addAll(GET_FIELD_NAME_LIST);

		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpRecentlyUsed__c.SObjectType.getDescribe().getName()
				+ whereString;

		List<ExpRecentlyUsed__c> sObjs = (List<ExpRecentlyUsed__c>) Database.query(soql);

		List<ExpRecentlyUsedEntity> entityList = new List<ExpRecentlyUsedEntity>();
		for (ExpRecentlyUsed__c sObj : sObjs) {
			entityList.add(createEntity(sObj));
		}
		return entityList;
	}

	public Repository.SaveResult saveEntity(ExpRecentlyUsedEntity entity) {
		return saveEntityList(new List<ExpRecentlyUsedEntity>{entity});
	}

	public virtual Repository.SaveResult saveEntityList(List<ExpRecentlyUsedEntity> entityList) {
		List<ExpRecentlyUsed__c> sObjList = new List<ExpRecentlyUsed__c>();
		for (ExpRecentlyUsedEntity entity : entityList) {
			sObjList.add(createObj(entity));
		}

		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, true);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/*
	 * Convert the given {@code ExpRecentlyUsed__c} to {@code ExpRecentlyUsedEntity}.
	 * @param sObject Source sObject
	 *
	 * @return ExpRecentlyUsedEntity entity instance created from the sObject
	 */
	@TestVisible
	private ExpRecentlyUsedEntity createEntity(ExpRecentlyUsed__c sObj) {
		ExpRecentlyUsedEntity entity = new ExpRecentlyUsedEntity();
		entity.setId(sObj.Id);
		entity.name = sObj.Name;
		entity.employeeBaseId = sObj.EmployeeBaseId__c;
		entity.targetId = sObj.TargetId__c;
		entity.masterType = ExpRecentlyUsedEntity.MASTER_TYPE_MAP.get(sObj.MasterType__c);
		entity.setSerializedRecentlyUsedValues(sObj.RecentlyUsedValues__c);

		entity.resetChanged();

		return entity;
	}

	@TestVisible
	private ExpRecentlyUsed__c createObj(ExpRecentlyUsedEntity entity) {
		ExpRecentlyUsed__c sObj = new ExpRecentlyUsed__c(Id = entity.Id);
		if (entity.isChanged(ExpRecentlyUsedEntity.Field.NAME)) {
			sObj.Name = entity.name;
		}

		if (entity.isChanged(ExpRecentlyUsedEntity.Field.EMPLOYEE_BASE_ID)) {
			sObj.EmployeeBaseId__c = entity.employeeBaseId;
		}

		if (entity.isChanged(ExpRecentlyUsedEntity.Field.MASTER_TYPE)) {
			sObj.MasterType__c = entity.masterType.name();
		}

		if (entity.isChanged(ExpRecentlyUsedEntity.Field.TARGET_ID)) {
			sObj.TargetId__c = entity.targetId;
		}

		if (entity.isChanged(ExpRecentlyUsedEntity.Field.RECENTLY_USED_VALUES)) {
			sObj.RecentlyUsedValues__c = entity.getSerializedRecentlyUsedValue();
		}

		return sObj;
	}
}