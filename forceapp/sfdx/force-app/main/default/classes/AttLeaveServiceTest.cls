/**
 * 休暇マスタのサービスのテストクラス
 */
@isTest
private class AttLeaveServiceTest {
	@istest static void checkAnnualAddTest() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();

		testData.createLeave('a', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		test.startTest();
		// 年次有休休暇なし
		new AttLeaveService().checkAnnualLeaveAdd(testData.company.id);

		testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		Boolean hasError = false;
		try {
			new AttLeaveService().checkAnnualLeaveAdd(testData.company.id);
		}
		catch (App.IllegalStateException e) {
			hasError = true;
			System.assertEquals(App.ERR_CODE_ANNUAL_LEAVE_EXISTED, e.getErrorCode());
		}
		finally {}
		System.assertEquals(true, hasError);

		test.stopTest();
	}

	/**
	 * saveLeaveEntityのテスト
	 * 新規の休暇が保存できることを確認する
	 */
	@istest static void saveLeaveEntityTestNew() {
		AttTestData testData = new AttTestData();

		AttLeaveEntity newLeaveData = new AttLeaveEntity();
		newLeaveData.companyId = testData.company.id;
		newLeaveData.code = 'code';
		newLeaveData.name = 'name';
		newLeaveData.nameL0 = 'namel0';
		newLeaveData.nameL1 = 'namel1';
		newLeaveData.nameL2 = 'namel2';
		newLeaveData.leaveType = AttLeaveType.PAID;
		newLeaveData.daysManaged = true;
		newLeaveData.leaveRanges = new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY, AttLeaveRange.RANGE_HALF};
		newLeaveData.validFrom = AppDate.today();
		newLeaveData.validTo = AppDate.today().addDays(10);

		Repository.SaveResult result = new AttLeaveService().saveLeaveEntity(newLeaveData);

		System.assertEquals(true, result.isSuccessAll);

		// エンティティの保存はリポジトリクラスで行うので、saveLeaveEntityメソッドで設定した項目が保存されていることを確認する
		AttLeaveEntity resLeave = new AttLeaveRepository().getLeaveById(result.details[0].Id);
		System.assertNotEquals(null, resLeave);
		System.assertEquals(newLeaveData.createUniqKey(testData.company.Code__c), resLeave.uniqkey);
	}

	/**
	 * saveLeaveEntityのテスト
	 * 休暇が更新できることを確認する
	 */
	@istest static void saveLeaveEntityTestUpdate() {
		AttTestData testData = new AttTestData();

		AttLeaveEntity leaveData = testData.leaveList[1];
		leaveData.code = leaveData.code + '_Update';
		leaveData.nameL0 = leaveData.nameL0 + '_Update';
		leaveData.validTo = leaveData.validTo.addDays(-1);

		Repository.SaveResult result = new AttLeaveService().saveLeaveEntity(leaveData);

		System.assertEquals(true, result.isSuccessAll);

		// エンティティの保存はリポジトリクラスで行うので、saveLeaveEntityメソッドで設定した項目が保存されていることを確認する
		AttLeaveEntity resLeave = new AttLeaveRepository().getLeaveById(result.details[0].Id);
		System.assertNotEquals(null, resLeave);
		System.assertEquals(leaveData.createUniqKey(testData.company.Code__c), resLeave.uniqkey);
		System.assertEquals(leaveData.nameL0, resLeave.nameL0);
		System.assertEquals(leaveData.validTo, resLeave.validTo);
	}

	/**
	 * saveLeaveEntityのテスト
	 * コードが重複している場合はアプリ例外が発生することを確認する
	 */
	@isTest static void saveLeaveEntityTestDuplicateCode() {
		AttTestData testData = new AttTestData();

		final Integer numberOfJob = 3;
		List<AttLeaveEntity> leaveEntities = testData.leaveList;
		AttLeaveEntity orgEntity = leaveEntities[1];

		// 更新情報を設定する
		AttLeaveEntity updateEntity = new AttLeaveEntity();
		updateEntity.setId(orgEntity.id);
		updateEntity.code = leaveEntities[0].code;

		try {
			new AttLeaveService().saveLeaveEntity(updateEntity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
		}
	}

	/**
	 * saveLeaveEntityのテスト
	 * 項目値に対するバリデーションが実行されていることを確認する
	 */
	@isTest static void saveLeaveEntityTestValidate() {
		AttTestData testData = new AttTestData();

		final Integer numberOfJob = 3;
		List<AttLeaveEntity> leaveEntities = testData.leaveList;
		AttLeaveEntity orgEntity = leaveEntities[1];

		// 必須項目に空文字を設定する
		AttLeaveEntity updateEntity = new AttLeaveEntity();
		updateEntity.setId(orgEntity.id);
		updateEntity.nameL0 = '';

		try {
			new AttLeaveService().saveLeaveEntity(updateEntity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
		}
	}

}
