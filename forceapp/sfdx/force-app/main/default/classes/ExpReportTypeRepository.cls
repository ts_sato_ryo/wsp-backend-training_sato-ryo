/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 経費申請タイプのリポジトリ Repository class for Expense Report Type
 */
public with sharing class ExpReportTypeRepository extends Repository.BaseRepository {

	/**
	 * リストを取得する際の並び順
	 */
	public enum SortOrder {
		CODE_ASC, CODE_DESC
	}

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** Number of Extended Items for each type */
	private static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/** String representation of Extended Item Field of the sObject */
	@TestVisible
	private static final String EXTENDED_ITEM_TEXT = ExpReportType__c.ExtendedItemText01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_PICKLIST = ExpReportType__c.ExtendedItemPicklist01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_LOOKUP = ExpReportType__c.ExtendedItemLookup01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_DATE = ExpReportType__c.ExtendedItemDate01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_ID = 'TextId__c';
	@TestVisible
	private static final String EXTENDED_ITEM_USED_IN = 'UsedIn__c';
	@TestVisible
	private static final String EXTENDED_ITEM_REQUIRED_FOR = 'RequiredFor__c';

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/**
	 * 取得対象の項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpReportType__c.Id,
			ExpReportType__c.Name,
			ExpReportType__c.Active__c,
			ExpReportType__c.Code__c,
			ExpReportType__c.CompanyId__c,
			ExpReportType__c.CostCenterRequiredFor__c,
			ExpReportType__c.CostCenterUsedIn__c,
			ExpReportType__c.Description_L0__c,
			ExpReportType__c.Description_L1__c,
			ExpReportType__c.Description_L2__c,
			ExpReportType__c.ExtendedItemLookup01TextId__c,
			ExpReportType__c.ExtendedItemLookup01UsedIn__c,
			ExpReportType__c.ExtendedItemLookup01RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup02TextId__c,
			ExpReportType__c.ExtendedItemLookup02UsedIn__c,
			ExpReportType__c.ExtendedItemLookup02RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup03TextId__c,
			ExpReportType__c.ExtendedItemLookup03UsedIn__c,
			ExpReportType__c.ExtendedItemLookup03RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup04TextId__c,
			ExpReportType__c.ExtendedItemLookup04UsedIn__c,
			ExpReportType__c.ExtendedItemLookup04RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup05TextId__c,
			ExpReportType__c.ExtendedItemLookup05UsedIn__c,
			ExpReportType__c.ExtendedItemLookup05RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup06TextId__c,
			ExpReportType__c.ExtendedItemLookup06UsedIn__c,
			ExpReportType__c.ExtendedItemLookup06RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup07TextId__c,
			ExpReportType__c.ExtendedItemLookup07UsedIn__c,
			ExpReportType__c.ExtendedItemLookup07RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup08TextId__c,
			ExpReportType__c.ExtendedItemLookup08UsedIn__c,
			ExpReportType__c.ExtendedItemLookup08RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup09TextId__c,
			ExpReportType__c.ExtendedItemLookup09UsedIn__c,
			ExpReportType__c.ExtendedItemLookup09RequiredFor__c,
			ExpReportType__c.ExtendedItemLookup10TextId__c,
			ExpReportType__c.ExtendedItemLookup10UsedIn__c,
			ExpReportType__c.ExtendedItemLookup10RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist01TextId__c,
			ExpReportType__c.ExtendedItemPicklist01UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist01RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist02TextId__c,
			ExpReportType__c.ExtendedItemPicklist02UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist02RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist03TextId__c,
			ExpReportType__c.ExtendedItemPicklist03UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist03RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist04TextId__c,
			ExpReportType__c.ExtendedItemPicklist04UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist04RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist05TextId__c,
			ExpReportType__c.ExtendedItemPicklist05UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist05RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist06TextId__c,
			ExpReportType__c.ExtendedItemPicklist06UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist06RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist07TextId__c,
			ExpReportType__c.ExtendedItemPicklist07UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist07RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist08TextId__c,
			ExpReportType__c.ExtendedItemPicklist08UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist08RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist09TextId__c,
			ExpReportType__c.ExtendedItemPicklist09UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist09RequiredFor__c,
			ExpReportType__c.ExtendedItemPicklist10TextId__c,
			ExpReportType__c.ExtendedItemPicklist10UsedIn__c,
			ExpReportType__c.ExtendedItemPicklist10RequiredFor__c,
			ExpReportType__c.ExtendedItemText01TextId__c,
			ExpReportType__c.ExtendedItemText01UsedIn__c,
			ExpReportType__c.ExtendedItemText01RequiredFor__c,
			ExpReportType__c.ExtendedItemText02TextId__c,
			ExpReportType__c.ExtendedItemText02UsedIn__c,
			ExpReportType__c.ExtendedItemText02RequiredFor__c,
			ExpReportType__c.ExtendedItemText03TextId__c,
			ExpReportType__c.ExtendedItemText03UsedIn__c,
			ExpReportType__c.ExtendedItemText03RequiredFor__c,
			ExpReportType__c.ExtendedItemText04TextId__c,
			ExpReportType__c.ExtendedItemText04UsedIn__c,
			ExpReportType__c.ExtendedItemText04RequiredFor__c,
			ExpReportType__c.ExtendedItemText05TextId__c,
			ExpReportType__c.ExtendedItemText05UsedIn__c,
			ExpReportType__c.ExtendedItemText05RequiredFor__c,
			ExpReportType__c.ExtendedItemText06TextId__c,
			ExpReportType__c.ExtendedItemText06UsedIn__c,
			ExpReportType__c.ExtendedItemText06RequiredFor__c,
			ExpReportType__c.ExtendedItemText07TextId__c,
			ExpReportType__c.ExtendedItemText07UsedIn__c,
			ExpReportType__c.ExtendedItemText07RequiredFor__c,
			ExpReportType__c.ExtendedItemText08TextId__c,
			ExpReportType__c.ExtendedItemText08UsedIn__c,
			ExpReportType__c.ExtendedItemText08RequiredFor__c,
			ExpReportType__c.ExtendedItemText09TextId__c,
			ExpReportType__c.ExtendedItemText09UsedIn__c,
			ExpReportType__c.ExtendedItemText09RequiredFor__c,
			ExpReportType__c.ExtendedItemText10TextId__c,
			ExpReportType__c.ExtendedItemText10UsedIn__c,
			ExpReportType__c.ExtendedItemText10RequiredFor__c,
			ExpReportType__c.ExtendedItemDate01TextId__c,
			ExpReportType__c.ExtendedItemDate01UsedIn__c,
			ExpReportType__c.ExtendedItemDate01RequiredFor__c,
			ExpReportType__c.ExtendedItemDate02TextId__c,
			ExpReportType__c.ExtendedItemDate02UsedIn__c,
			ExpReportType__c.ExtendedItemDate02RequiredFor__c,
			ExpReportType__c.ExtendedItemDate03TextId__c,
			ExpReportType__c.ExtendedItemDate03UsedIn__c,
			ExpReportType__c.ExtendedItemDate03RequiredFor__c,
			ExpReportType__c.ExtendedItemDate04TextId__c,
			ExpReportType__c.ExtendedItemDate04UsedIn__c,
			ExpReportType__c.ExtendedItemDate04RequiredFor__c,
			ExpReportType__c.ExtendedItemDate05TextId__c,
			ExpReportType__c.ExtendedItemDate05UsedIn__c,
			ExpReportType__c.ExtendedItemDate05RequiredFor__c,
			ExpReportType__c.ExtendedItemDate06TextId__c,
			ExpReportType__c.ExtendedItemDate06UsedIn__c,
			ExpReportType__c.ExtendedItemDate06RequiredFor__c,
			ExpReportType__c.ExtendedItemDate07TextId__c,
			ExpReportType__c.ExtendedItemDate07UsedIn__c,
			ExpReportType__c.ExtendedItemDate07RequiredFor__c,
			ExpReportType__c.ExtendedItemDate08TextId__c,
			ExpReportType__c.ExtendedItemDate08UsedIn__c,
			ExpReportType__c.ExtendedItemDate08RequiredFor__c,
			ExpReportType__c.ExtendedItemDate09TextId__c,
			ExpReportType__c.ExtendedItemDate09UsedIn__c,
			ExpReportType__c.ExtendedItemDate09RequiredFor__c,
			ExpReportType__c.ExtendedItemDate10TextId__c,
			ExpReportType__c.ExtendedItemDate10UsedIn__c,
			ExpReportType__c.ExtendedItemDate10RequiredFor__c,
			ExpReportType__c.JobRequiredFor__c,
			ExpReportType__c.JobUsedIn__c,
			ExpReportType__c.Name_L0__c,
			ExpReportType__c.Name_L1__c,
			ExpReportType__c.Name_L2__c,
			ExpReportType__c.UniqKey__c,
			ExpReportType__c.UseFileAttachment__c,
			ExpReportType__c.VendorRequiredFor__c,
			ExpReportType__c.VendorUsedIn__c
		};

		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/**
	 * 指定したIDのエンティティを取得する
	 * @param id 取得対象のID
	 * @return 指定したIDのエンティティ、ただし存在しない場合はnull
	 */
	public ExpReportTypeEntity getEntity(Id id) {
		List<ExpReportTypeEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したエンティティを取得する
	 * @param ids 取得対象のIDのリスト
	 * @return 指定したIDのエンティティのリスト、ただし存在しない場合はnull
	 */
	public List<ExpReportTypeEntity> getEntityList(List<Id> ids) {
		ExpReportTypeRepository.SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 指定したコードを持つエンティティを取得する
	 * @param code 費目コード
	 * @param companyId 会社ID
	 * @return 指定したコードを持つエンティティ、存在しない場合はnull
	 */
	public ExpReportTypeEntity getEntityByCode(String code, Id companyId) {
		ExpReportTypeRepository.SearchFilter filter = new SearchFilter();
		filter.codes = new Set<String>{code};
		filter.companyIds = new Set<Id>{companyId};

		List<ExpReportTypeEntity> entityList = searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定した拡張項目IDを持つエンティティを取得する
	 * @param extendedItemIds 拡張項目IDのリスト
	 * @return 指定した拡張項目IDを持つエンティティ、存在しない場合はnull
	 */
	public List<ExpReportTypeEntity> getEntityListByExtendedItemId(List<Id> extendedItemIds) {
		ExpReportTypeRepository.SearchFilter filter = new SearchFilter();
		filter.extendedItemIds = extendedItemIds == null ? null : new Set<Id>(extendedItemIds);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** 経費申請タイプID */
		public Set<Id> ids;
		/** 会社ID */
		public Set<Id> companyIds;
		/** コード(完全一致) */
		public Set<String> codes;
		/** 有効 */
		public Boolean active;
		/** 拡張項目ID */
		public Set<Id> extendedItemIds;
	}

	/**
	 * 費目を検索する
	 * @param filter 検索条件
	 * @return 検索結果
	 */
	public List<ExpReportTypeEntity> searchEntity(ExpReportTypeRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 経費申請タイプを検索する
	 * @param filter 検索条件
	 * @param order 並び順
	 * @return 検索結果
	 */
	public List<ExpReportTypeEntity> searchEntity(ExpReportTypeRepository.SearchFilter filter, SortOrder order) {
		return searchEntity(filter, NOT_FOR_UPDATE, order);
	}

	/**
	 * 経費申請タイプを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param order 並び順
	 * @return 検索結果
	 */
	public List<ExpReportTypeEntity> searchEntity(ExpReportTypeRepository.SearchFilter filter, Boolean forUpdate, SortOrder order) {
		// WHERE句
		List<String> whereList = new List<String>();

		// 経費申請タイプIDで検索
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ExpReportType__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ExpReportType__c.Code__c) + ' = :pCodes');
		}
		// 有効で検索
		if (filter.active != null) {
			if (filter.active) {
				whereList.add(getFieldName(ExpReportType__c.Active__c) + ' = TRUE');
			}
			else {
				whereList.add(getFieldName(ExpReportType__c.Active__c) + ' = FALSE');
			}
		}
		// 拡張項目IDで検索
		Set<Id> pExtendedItemIds;
		if (filter.extendedItemIds != null) {
			pExtendedItemIds = filter.extendedItemIds;
			whereList.add(
				'(' + getFieldName(ExpReportType__c.ExtendedItemText01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemText10TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemLookup10TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemDate10TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpReportType__c.ExtendedItemPicklist10TextId__c) + ' IN :pExtendedItemIds)');
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY句
		String orderByString = ' ORDER BY ';
		if (order == SortOrder.CODE_ASC) {
			orderByString += getFieldName(ExpReportType__c.Code__c) + ' ASC NULLS LAST';
		} else if (order == SortOrder.CODE_DESC) {
			orderByString += getFieldName(ExpReportType__c.Code__c) + ' DESC NULLS LAST';
		}

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_FIELD_NAME_LIST);

		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpReportType__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderByString
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('ExpReportTypeRepository: SOQL=' + soql);

		List<ExpReportType__c> sObjs = (List<ExpReportType__c>)Database.query(soql);

		List<ExpReportTypeEntity> entities = new List<ExpReportTypeEntity>();
		for (ExpReportType__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	private List<SObject> createObjectList(List<ExpReportTypeEntity> entityList) {
		List<ExpReportType__c> objectList = new List<ExpReportType__c>();
		for (ExpReportTypeEntity entity : entityList) {
			objectList.add(createObject((ExpReportTypeEntity)entity));
		}

		return objectList;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entity オブジェクト作成元のエンティティ
	 * @return 保存用のオブジェクト
	 */
	private ExpReportType__c createObject(ExpReportTypeEntity entity) {
		ExpReportType__c sObj = new ExpReportType__c();
		sObj.put('Id', entity.id);

		if (entity.isChanged(ExpReportTypeEntity.Field.ACTIVE)) {
			sObj.Active__c = entity.active;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.CODE)) {
			sObj.Code__c = entity.code;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.COMPANY_ID)) {
			sObj.CompanyId__c = entity.companyId;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.COST_CENTER_REQUIRED_FOR)) {
			sObj.CostCenterRequiredFor__c = entity.costCenterRequiredFor != null ? entity.costCenterRequiredFor.value : null;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.COST_CENTER_USED_IN)) {
			sObj.CostCenterUsedIn__c = entity.costCenterUsedIn != null ? entity.costCenterUsedIn.value : null;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.DESCRIPTION_L0)) {
			sObj.Description_L0__c = entity.descriptionL0;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.DESCRIPTION_L1)) {
			sObj.Description_L1__c = entity.descriptionL1;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.DESCRIPTION_L2)) {
			sObj.Description_L2__c = entity.descriptionL2;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.JOB_REQUIRED_FOR)) {
			sObj.JobRequiredFor__c = entity.jobRequiredFor != null ? entity.jobRequiredFor.value : null;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.JOB_USED_IN)) {
			sObj.JobUsedIn__c = entity.jobUsedIn != null ? entity.jobUsedIn.value : null;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.USE_FILE_ATTACHMENT)) {
			sObj.UseFileAttachment__c = entity.useFileAttachment == null ? false : entity.useFileAttachment;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.VENDOR_REQUIRED_FOR)) {
			sObj.VendorRequiredFor__c = entity.vendorRequiredFor != null ? entity.vendorRequiredFor.value : null;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.VENDOR_USED_IN)) {
			sObj.VendorUsedIn__c = entity.vendorUsedIn != null ? entity.vendorUsedIn.value : null;
		}

		Boolean extendedItemDateIdChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST);
		Boolean extendedItemDateUsedInChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST);
		Boolean extendedItemDateRequiredForChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST);

		Boolean extendedItemLookupIdChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST);
		Boolean extendedItemLookupUsedInChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST);
		Boolean extendedItemLookupRequiredForChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST);

		Boolean extendedItemPicklistIdChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST);
		Boolean extendedItemPicklistUsedInChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST);
		Boolean extendedItemPicklistRequiredForChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST);

		Boolean extendedItemTextIdChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST);
		Boolean extendedItemTextUsedInChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST);
		Boolean extendedItemTextRequiredForChanged = entity.isChanged(ExpReportTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST);

		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			if (extendedItemDateIdChanged) {
				sObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID, entity.getExtendedItemDateId(i));
			}
			if (extendedItemDateUsedInChanged) {
				sObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_USED_IN,
					ComExtendedItemUsedIn.getValue(entity.getExtendedItemDateUsedIn(i)));
			}
			if (extendedItemDateRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR,
					ComExtendedItemRequiredFor.getValue(entity.getExtendedItemDateRequiredFor(i)));
			}

			if (extendedItemLookupIdChanged) {
				sObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_ID, entity.getExtendedItemLookupId(i));
			}
			if (extendedItemLookupUsedInChanged) {
				sObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_USED_IN,
						ComExtendedItemUsedIn.getValue(entity.getExtendedItemLookupUsedIn(i)));
			}
			if (extendedItemLookupRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR,
						ComExtendedItemRequiredFor.getValue(entity.getExtendedItemLookupRequiredFor(i)));
			}

			if (extendedItemPicklistIdChanged) {
				sObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID, entity.getExtendedItemPicklistId(i));
			}
			if (extendedItemPicklistUsedInChanged) {
				sObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_USED_IN,
						ComExtendedItemUsedIn.getValue(entity.getExtendedItemPicklistUsedIn(i)));
			}
			if (extendedItemPicklistRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR,
						ComExtendedItemRequiredFor.getValue(entity.getExtendedItemPicklistRequiredFor(i)));
			}

			if (extendedItemTextIdChanged) {
				sObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID, entity.getExtendedItemTextId(i));
			}
			if (extendedItemTextUsedInChanged) {
				sObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_USED_IN,
						ComExtendedItemUsedIn.getValue(entity.getExtendedItemTextUsedIn(i)));
			}
			if (extendedItemTextRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR,
						ComExtendedItemRequiredFor.getValue(entity.getExtendedItemTextRequiredFor(i)));
			}
		}

		if (entity.isChanged(ExpReportTypeEntity.Field.NAME)) {
			sObj.Name = entity.name;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.NAME_L0)) {
			sObj.Name_L0__c = entity.nameL0;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.NAME_L1)) {
			sObj.Name_L1__c = entity.nameL1;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.NAME_L2)) {
			sObj.Name_L2__c = entity.nameL2;
		}
		if (entity.isChanged(ExpReportTypeEntity.Field.UNIQ_KEY)) {
			sObj.UniqKey__c = entity.uniqKey;
		}

		return sObj;
	}

	/**
	 * SObjectからエンティティを作成する
	 * @param obj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private ExpReportTypeEntity createEntity(ExpReportType__c sObj) {
		ExpReportTypeEntity entity = new ExpReportTypeEntity();

		entity.setId(sObj.Id);
		entity.active = sObj.Active__c;
		entity.code = sObj.Code__c;
		entity.companyId = sObj.CompanyId__c;
		entity.costCenterRequiredFor = ComCostCenterRequiredFor.ValueOf(sObj.CostCenterRequiredFor__c);
		entity.costCenterUsedIn = ComCostCenterUsedIn.ValueOf(sObj.CostCenterUsedIn__c);
		entity.descriptionL0 = sObj.Description_L0__c;
		entity.descriptionL1 = sObj.Description_L1__c;
		entity.descriptionL2 = sObj.Description_L2__c;
		entity.jobRequiredFor = ComJobRequiredFor.ValueOf(sObj.JobRequiredFor__c);
		entity.jobUsedIn = ComJobUsedIn.ValueOf(sObj.JobUsedIn__c);
		entity.useFileAttachment = sObj.UseFileAttachment__c;
		entity.vendorRequiredFor = ExpVendorRequiredFor.ValueOf(sObj.VendorRequiredFor__c);
		entity.vendorUsedIn = ExpVendorUsedIn.ValueOf(sObj.VendorUsedIn__c);

		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			entity.setExtendedItemTextId(i, (Id) sObj.get(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemTextUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
							EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
							EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR)));

			entity.setExtendedItemPicklistId(i, (Id) sObj.get(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemPicklistUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
							EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
							EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR)));

			entity.setExtendedItemDateId(i, (Id) sObj.get(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemDateUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
							EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemDateRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
							EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR)));

			entity.setExtendedItemLookupId(i, (Id) sObj.get(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemLookupUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
					EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
					EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR)));
		}

		entity.name = sObj.Name;
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.uniqKey = sObj.UniqKey__c;

		entity.resetChanged();

		return entity;
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntity(ExpReportTypeEntity entity) {
		return saveEntityList(new List<ExpReportTypeEntity>{entity}, ALL_SAVE);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public Repository.SaveResult saveEntityList(List<ExpReportTypeEntity> entityList, Boolean allOrNone) {

		List<SObject> saveObjectList = createObjectList(entityList);
		List<Database.UpsertResult> resList = AppDatabase.doUpsert(saveObjectList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}
}