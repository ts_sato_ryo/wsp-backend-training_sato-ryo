/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 権限レコード操作API(PermissionResource)のテスト
 */
@isTest
private class PermissionResourceTest {

	/** テストデータ */
	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;

		public User userObj;

		/** コンストラクタ　*/
		public TestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
			userObj = ComTestDataUtility.createUser('ExeUser', 'ja', UserInfo.getLocale());
		}

		/** 会社オブジェクトを作成する */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}

		/** 言語設定を更新する */
		public void updateOrgLang(String lang0, String lang1, String lang2) {
			ComOrgSetting__c org = [SELECT Id, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c LIMIT 1];
			org.Language_0__c = lang0;
			org.Language_1__c = lang1;
			org.Language_2__c = lang2;
			update org;
						this.orgObj = org;
		}

		/** 作成会社の権限を作成する */
		public List<ComPermission__c> createPermissions(String code, Integer size) {
			return ComTestDataUtility.createPermissions(code, this.companyObj.id, size);
		}

		/** 権限を作成する */
		public List<ComPermission__c> createPermissions(String code, Id companyId, Integer size) {
			return ComTestDataUtility.createPermissions(code, companyId, size);
		}

		/** 権限の論理削除を行う */
		public ComPermission__c logicalDelete(ComPermission__c deleteTarget) {
			deleteTarget.Removed__c = true;
			update deleteTarget;
			return deleteTarget;
		}

		/**
		 * オブジェクトを取得する
		 * @param permissionId 取得対象の権限レコードID
		 */
		public ComPermission__c getPermissionObj(Id permissionId) {
			List<ComPermission__c> permissions = [SELECT
					Id, Name, Name_L0__c, Code__c, UniqKey__c, CompanyId__c,
					ViewAttTimeSheetByDelegate__c, EditAttTimeSheetByDelegate__c, ViewTimeTrackByDelegate__c,
					ApproveAttDailyRequestByDelegate__c, ApproveAttRequestByDelegate__c, ApproveSelfAttDailyRequestByEmployee__c, ApproveSelfAttRequestByEmployee__c, CancelAttApprovalByDelegate__c, CancelAttApprovalByEmployee__c, CancelAttDailyApprovalByDelegate__c, CancelAttDailyApprovalByEmployee__c, CancelAttDailyRequestByDelegate__c, CancelAttRequestByDelegate__c, SubmitAttDailyRequestByDelegate__c, SubmitAttRequestByDelegate__c,
					ManageAttAgreementAlertSetting__c, ManageAttLeave__c, ManageAttLeaveGrant__c, ManageAttLeaveOfAbsence__c, ManageAttLeaveOfAbsenceApply__c, ManageAttShortTimeWorkSetting__c, ManageAttShortTimeWorkSettingApply__c, ManageAttWorkingType__c, ManageCalendar__c, ManageDepartment__c, ManageEmployee__c, ManageJob__c, ManageJobType__c, ManageMobileSetting__c, ManageOverallSetting__c, ManagePermission__c, ManagePlannerSetting__c,
					ManageTimeSetting__c, ManageTimeWorkCategory__c, SwitchCompany__c, ManageExpCustomHint__c

					FROM ComPermission__c WHERE Id = :permissionId];
			return permissions.isEmpty() ? null : permissions.get(0);
		}

		/**
		 * 権限を作成する(必須項目のみセット)
		 * @param code 権限のコード
		 * @param name　権限名
		 * @param company 会社
		 * @return ComPermission__c 必須項目がセットされた権限のSObject
		 */
		public ComPermission__c createDefaultPermission(String code, String name, ComCompany__c company) {
			ComPermission__c permission = new ComPermission__c(
				CompanyId__c = company.Id,
				Name = name,
				Name_L0__c = name,
				Code__c = code,
				UniqKey__c = company.Code__c + '-' + code);
			return permission;
		}

		/**
		 * 標準ユーザーを作成する
		 * @param lastName
		 */
		public EmployeeBaseEntity createEmployeeWithStandardUser(String lastName, id permissionId) {
			User standartUser = ComTestDataUtility.createStandardUser(lastName);
			ComEmpBase__c empObj = ComTestDataUtility.createEmployeeWithHistory(lastName, this.companyObj.Id, null, standartUser.id, permissionId);
			return (new EmployeeRepository()).getEntity(empObj.Id);
		}
	}

	/**
	 * @description 権限検索API(searchApi）のテスト
	 * 不正なパラメータが指定された場合に例外が発生することを確認する
	 */
	@isTest static void searchApiParamErrTest() {
		// Case1 : companyIdのパラメータ不正
		try {
			PermissionResource.SearchApi api = new PermissionResource.SearchApi();
			PermissionResource.SearchRequest req = new PermissionResource.SearchRequest();
			req.companyId = 'あああああ';
			api.execute(req);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assert(true);//OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// Case２ : idのパラメータ不正
		try {
			PermissionResource.SearchApi api = new PermissionResource.SearchApi();
			PermissionResource.SearchRequest req = new PermissionResource.SearchRequest();
			req.id = 'いいいいい';
			api.execute(req);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			System.assert(true);//OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * @description 権限検索API(searchApi）のテスト
	 * 権限検索APIが正しく権限の結果を取得できることを確認する
	 * companyIdを指定し正しくFilterできる/論理削除した権限が除外される
	 */
	@isTest static void searchApiTest() {
		//テストデータ作成
		TestData testData = new TestData();
		// - 会社の作成
		ComCompany__c anotherCompany = testData.createCompany('another');
		// - 権限の作成
		List<ComPermission__c> expPermissionList = new List<ComPermission__c>();
		expPermissionList.addAll(testData.createPermissions('Filter', 5));
		expPermissionList.addAll(testData.createPermissions('Filter', anotherCompany.id, 5));
		// - 論理削除した権限の作成
		expPermissionList.set(4, TestData.logicalDelete(expPermissionList.get(4)));

		//Case1 : 権限検索APIが正しく権限の結果を取得できることを確認する
		PermissionResource.SearchResponse companyRes;
		// - パラメータ作成
		PermissionResource.SearchApi api = new PermissionResource.SearchApi();
		PermissionResource.SearchRequest companyReq = new PermissionResource.SearchRequest();
		companyReq.companyId = testData.companyObj.id;
		// - 実行
		Test.startTest();
		try {
			companyRes = (PermissionResource.SearchResponse)api.execute(companyReq);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		Test.stopTest();
		// - 検証
		System.assertEquals(4, companyRes.records.size());
		assertPermissionRes(expPermissionList.get(0), companyRes.records[0]);
		assertPermissionRes(expPermissionList.get(1), companyRes.records[1]);
		assertPermissionRes(expPermissionList.get(2), companyRes.records[2]);
		assertPermissionRes(expPermissionList.get(3), companyRes.records[3]);
	}

	/**
	 * @description 権限検索API(searchApi）のテスト
	 * 権限検索APIが全ての項目を取得できることを確認する
	 */
	@isTest static void searchApiAllColumnTest() {
		//テストデータ作成
		TestData testData = new TestData();
		// - 権限の作成
		List<ComPermission__c> expPermissionList = new List<ComPermission__c>();
		expPermissionList.addAll(testData.createPermissions('Filter', 3));
		// 勤怠トランザクション系権限項目のテスト
		ComPermission__c expAttTransactionPermission = expPermissionList.get(0);
		expAttTransactionPermission.ViewAttTimeSheetByDelegate__c = false;
		expAttTransactionPermission.EditAttTimeSheetByDelegate__c = true;

		// 勤怠申請系権限項目のテスト
		ComPermission__c expAttRequestPermission = expPermissionList.get(1);
		expAttRequestPermission.ApproveAttDailyRequestByDelegate__c = false;
		expAttRequestPermission.ApproveAttRequestByDelegate__c = true;
		expAttRequestPermission.ApproveSelfAttDailyRequestByEmployee__c = false;
		expAttRequestPermission.ApproveSelfAttRequestByEmployee__c = true;
		expAttRequestPermission.CancelAttApprovalByDelegate__c = false;
		expAttRequestPermission.CancelAttApprovalByEmployee__c = true;
		expAttRequestPermission.CancelAttDailyApprovalByDelegate__c = false;
		expAttRequestPermission.CancelAttDailyApprovalByEmployee__c = true;
		expAttRequestPermission.CancelAttDailyRequestByDelegate__c = false;
		expAttRequestPermission.CancelAttRequestByDelegate__c = true;
		expAttRequestPermission.SubmitAttDailyRequestByDelegate__c = false;
		expAttRequestPermission.SubmitAttRequestByDelegate__c = true;

		// 管理系権限項目のテスト
		ComPermission__c expMasterPermission = expPermissionList.get(2);
		expMasterPermission.ManageAttAgreementAlertSetting__c = false;
		expMasterPermission.ManageAttLeave__c = true;
		expMasterPermission.ManageAttLeaveGrant__c = false;
		expMasterPermission.ManageAttLeaveOfAbsence__c = true;
		expMasterPermission.ManageAttLeaveOfAbsenceApply__c = false;
		expMasterPermission.ManageAttPattern__c = true;
		expMasterPermission.ManageAttPatternApply__c = false;
		expMasterPermission.ManageAttShortTimeWorkSetting__c = true;
		expMasterPermission.ManageAttShortTimeWorkSettingApply__c = false;
		expMasterPermission.ManageAttWorkingType__c = true;
		expMasterPermission.ManageCalendar__c = false;
		expMasterPermission.ManageDepartment__c = true;
		expMasterPermission.ManageEmployee__c = false;
		expMasterPermission.ManageJob__c = true;
		expMasterPermission.ManageJobType__c = false;
		expMasterPermission.ManageMobileSetting__c = true;
		expMasterPermission.ManageOverallSetting__c = false;
		expMasterPermission.ManagePermission__c = true;
		expMasterPermission.ManagePlannerSetting__c = false;
		expMasterPermission.ManageTimeSetting__c = true;
		expMasterPermission.ManageTimeWorkCategory__c = false;
		expMasterPermission.ManageExpTypeGroup__c = false;
		expMasterPermission.ManageExpType__c = false;
		expMasterPermission.ManageExpTaxType__c = false;
		expMasterPermission.ManageExpSetting__c = false;
		expMasterPermission.ManageExpExchangeRate__c = false;
		expMasterPermission.ManageExpAccountingPeriod__c = false;
		expMasterPermission.ManageExpReportType__c = false;
		expMasterPermission.ManageExpCostCenter__c = false;
		expMasterPermission.ManageExpVendor__c = false;
		expMasterPermission.ManageExpExtendedItem__c = false;
		expMasterPermission.ManageExpEmployeeGroup__c = false;
		expMasterPermission.ManageExpCustomHint__c = false;

		expMasterPermission.SwitchCompany__c = true;

		update expPermissionList;

		//Case1 : 権限検索APIが正しく権限の結果を取得できることを確認する
		PermissionResource.SearchResponse companyRes;
		// - パラメータ作成
		PermissionResource.SearchApi api = new PermissionResource.SearchApi();
		PermissionResource.SearchRequest companyReq = new PermissionResource.SearchRequest();
		companyReq.companyId = testData.companyObj.id;
		// - 実行
		Test.startTest();
		try {
			companyRes = (PermissionResource.SearchResponse)api.execute(companyReq);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		Test.stopTest();
		// - 検証
		System.assertEquals(3, companyRes.records.size());
		assertPermissionRes(expPermissionList.get(0), companyRes.records[0]);
		assertPermissionRes(expPermissionList.get(1), companyRes.records[1]);
		assertPermissionRes(expPermissionList.get(2), companyRes.records[2]);

	}

	/**
	 * @description 権限検索API(searchApi）のテスト
	 * 多言語名称が正しく取得できていることを検証する
	 */
	@isTest static void searchApiMultiLangTest() {
		// テストデータ作成
		TestData testData = new TestData();
		// - 設定の更新　多言語検証用
		testData.updateOrgLang(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		// - 権限の作成
		List<ComPermission__c> expPermissionList = new List<ComPermission__c>();
		expPermissionList.addAll(ComTestDataUtility.createPermissions('Filter', testData.companyObj.id, 1));

		// Case1 : L1の多言語名称が正しく取得できていることを確認する
		PermissionResource.SearchResponse companyRes;
		// - パラメータ作成
		PermissionResource.SearchApi api = new PermissionResource.SearchApi();
		PermissionResource.SearchRequest companyReq = new PermissionResource.SearchRequest();
		companyReq.companyId = testData.companyObj.id;
		// - 実行
		Test.startTest();
		try {
			companyRes = (PermissionResource.SearchResponse)api.execute(companyReq);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		Test.stopTest();

		// - 検証
		System.assertEquals(1, companyRes.records.size());
		assertPermissionRes(expPermissionList.get(0), companyRes.records[0], testData.orgObj);
	}

	/**
	 * @description 権限検索API(searchApi）のテスト
	 * 引数の値idで正しくFilterされることを確認する
	 */
	@isTest static void searchApiIdFilterTest() {
		// テストデータ作成
		TestData testData = new TestData();
		// - 会社の作成
		ComCompany__c anotherCompany = testData.createCompany('another');
		// - 権限の作成
		List<ComPermission__c> expPermissionList = new List<ComPermission__c>();
		expPermissionList.addAll(testData.createPermissions('Filter', 5));
		expPermissionList.addAll(testData.createPermissions('ZFilter', anotherCompany.id, 5));
		// - 論理削除した権限の作成
		expPermissionList.set(4, testData.logicalDelete(expPermissionList.get(4)));

		// Case1 : Filterを指定しない場合に論理削除した値をのぞく全ての権限が取得できる
		PermissionResource.SearchResponse companyRes;
		// - パラメータ作成
		PermissionResource.SearchApi api = new PermissionResource.SearchApi();
		PermissionResource.SearchRequest companyReq = new PermissionResource.SearchRequest();
		// - 実行
		try {
			companyRes = (PermissionResource.SearchResponse) api.execute(companyReq);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// - 検証
		System.assertEquals(9, companyRes.records.size());
		assertPermissionRes(expPermissionList.get(0), companyRes.records[0]);
		assertPermissionRes(expPermissionList.get(1), companyRes.records[1]);
		assertPermissionRes(expPermissionList.get(2), companyRes.records[2]);
		assertPermissionRes(expPermissionList.get(3), companyRes.records[3]);
		assertPermissionRes(expPermissionList.get(5), companyRes.records[4]);
		assertPermissionRes(expPermissionList.get(6), companyRes.records[5]);
		assertPermissionRes(expPermissionList.get(7), companyRes.records[6]);
		assertPermissionRes(expPermissionList.get(8), companyRes.records[7]);
		assertPermissionRes(expPermissionList.get(9), companyRes.records[8]);

		// Case2 : idを指定し正しくFilterできる
		PermissionResource.SearchResponse idRes;
		// - パラメータ作成
		PermissionResource.SearchRequest idReq = new PermissionResource.SearchRequest();
		idReq.id = expPermissionList.get(5).id;
		// - 実行
		try {
			idRes = (PermissionResource.SearchResponse) api.execute(idReq);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// - 検証
		System.assertEquals(1, idRes.records.size());
		assertPermissionRes(expPermissionList.get(5), idRes.records[0]);

		// Case3 : companyId,id両方を指定し正しくFilterできる
		PermissionResource.SearchResponse res;
		// - パラメータ作成
		PermissionResource.SearchRequest req = new PermissionResource.SearchRequest();
		req.companyId = expPermissionList.get(6).CompanyId__c;
		req.id = expPermissionList.get(6).id;
		// - 実行
		try {
			res = (PermissionResource.SearchResponse) api.execute(req);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// - 検証
		System.assertEquals(1, res.records.size());
		assertPermissionRes(expPermissionList.get(6), res.records[0]);
	}

	/**
	 * @description 権限作成API(CreateApi）のテスト
	 * 権限作成APIを実行し、正しく権限の結果を保存できることを確認する
	 */
	@isTest static void createApiTest() {
		//テストデータ作成
		TestData testData = new TestData();
		// - 権限の作成
		ComPermission__c expPermission = testData.createDefaultPermission('SaveCode', 'Save Permission Name', testData.companyObj);
		expPermission.ViewAttTimeSheetByDelegate__c = true;

		//権限検索APIが正しく権限の結果を取得できることを確認する
		PermissionResource.SaveResult saveRes;
		// - パラメータ作成
		PermissionResource.CreateApi api = new PermissionResource.CreateApi();
		PermissionResource.SaveRequest param = new PermissionResource.SaveRequest();
		param.name_L0 = expPermission.Name_L0__c;
		param.code = expPermission.Code__c;
		param.companyId = expPermission.CompanyId__c;
		param.viewAttTimesheetByDelegate = expPermission.ViewAttTimeSheetByDelegate__c;
		param.manageExpCustomHint = expPermission.ManageExpCustomHint__c;
		// - 実行
		Test.startTest();
		try {
			saveRes = (PermissionResource.SaveResult)api.execute(param);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		Test.stopTest();
		// - 検証
		ComPermission__c actPermission = testData.getPermissionObj(saveRes.id);
		System.assertEquals(expPermission.Name, actPermission.Name_L0__c);
		System.assertEquals(expPermission.Name_L0__c, actPermission.Name_L0__c);
		System.assertEquals(expPermission.Code__c, actPermission.Code__c);
		System.assertEquals(expPermission.CompanyId__c, actPermission.CompanyId__c);
		System.assertEquals(expPermission.ViewAttTimeSheetByDelegate__c, actPermission.ViewAttTimeSheetByDelegate__c);
		System.assertEquals(expPermission.ViewTimeTrackByDelegate__c, actPermission.ViewTimeTrackByDelegate__c);
		System.assertEquals(expPermission.ManageExpCustomHint__c, actPermission.ManageExpCustomHint__c);

	}

	/**
	 * @description 権限作成API(CreateApi）のテスト
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest static void createApiTestNoPermission() {
		TestData testData = new TestData();

		// アクセス権限の管理権限を無効に設定する
		ComPermission__c permission = testData.createDefaultPermission('NoPrm', 'No Permission Name', testData.companyObj);
		permission.ManagePermission__c = false;
		insert permission;
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('std-auth-User', permission.id);

		ComPermission__c expPermission = testData.createDefaultPermission('NoPrm', 'No Permission Name', testData.companyObj);

		// - パラメータ作成
		PermissionResource.CreateApi api = new PermissionResource.CreateApi();
		PermissionResource.SaveRequest param = new PermissionResource.SaveRequest();
		param.name_L0 = expPermission.Name_L0__c;
		param.code = expPermission.Code__c;
		param.companyId = expPermission.CompanyId__c;

		App.NoPermissionException actEx;
		System.runAs(new User(Id = stdEmployee.userId)) {
			try {
				PermissionResource.SaveResult res = (PermissionResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * @description 権限作成API(UpdateApi）のテスト
	 * 権限更新APIを実行し、正しく権限の結果を保存できることを確認する
	 */
	@isTest static void updateApiTest() {
		//テストデータ作成
		TestData testData = new TestData();
		// - 権限の作成
		ComPermission__c expPermission = testData.createDefaultPermission('PrmUpd', 'Update Permission Name', testData.companyObj);
		expPermission.SwitchCompany__c = true;// true -> trueに更新する場合
		expPermission.ApproveAttDailyRequestByDelegate__c = false;// false -> trueに更新する場合
		expPermission.ManageAttAgreementAlertSetting__c = true;// true -> falseに更新する場合
		expPermission.CancelAttApprovalByEmployee__c = true;
		expPermission.ManageExpCustomHint__c = true;
		insert expPermission;

		//権限更新APIが正しく権限を保存できることを確認する
		PermissionResource.SaveResult saveRes;
		// - パラメータ作成
		PermissionResource.UpdateApi api = new PermissionResource.UpdateApi();
		PermissionResource.SaveRequest param = new PermissionResource.SaveRequest();
		param.id = expPermission.Id;
		param.name_L0 = expPermission.Name_L0__c;
		param.code = expPermission.Code__c;
		param.viewAttTimesheetByDelegate = null;// nullでもfalseが入ることを保証する false -> falseに更新する場合
		param.switchCompany = expPermission.SwitchCompany__c;
		param.approveAttDailyRequestByDelegate = true;
		param.manageAttAgreementAlertSetting = false;
		param.manageExpCustomHint = false;

		// - 実行
		Test.startTest();
		try {
			saveRes = (PermissionResource.SaveResult)api.execute(param);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		Test.stopTest();
		// - 検証
		ComPermission__c actPermission = testData.getPermissionObj(saveRes.id);
		System.assertEquals(expPermission.Name_L0__c, actPermission.Name_L0__c);
		System.assertEquals(expPermission.Code__c, actPermission.Code__c);
		System.assertEquals(expPermission.CompanyId__c, actPermission.CompanyId__c);
		System.assertEquals(expPermission.ViewAttTimeSheetByDelegate__c, actPermission.ViewAttTimeSheetByDelegate__c);// false -> falseに更新されていることを確認する
		System.assertEquals(param.approveAttDailyRequestByDelegate, actPermission.ApproveAttDailyRequestByDelegate__c);// false -> trueに更新されていることを確認する
		System.assertEquals(param.switchCompany, actPermission.SwitchCompany__c);// true -> trueに更新されていることを確認する
		System.assertEquals(param.manageAttAgreementAlertSetting, actPermission.ManageAttAgreementAlertSetting__c);// true -> falseに更新されていることを確認する
		System.assertEquals(false, actPermission.CancelAttApprovalByEmployee__c);// 未設定項目が変更されていないことを確認する
		System.assertEquals(param.manageExpCustomHint, actPermission.ManageExpCustomHint__c); // Manage Expense Custom Hint set from true to false
	}

	/**
	 * @description 権限更新API(UpdateApi）のテスト
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest static void updateApiTestNoPermission() {
		TestData testData = new TestData();

		// アクセス権限の管理権限を無効に設定する
		ComPermission__c permission = testData.createDefaultPermission('NoPrmUpd', 'No Permission Name', testData.companyObj);
		permission.ManagePermission__c = false;
		insert permission;
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('std-auth-User', permission.id);

		ComPermission__c expPermission = testData.createDefaultPermission('NoUpd', 'No Permission Name', testData.companyObj);
		insert expPermission;

		// - パラメータ作成
		PermissionResource.UpdateApi api = new PermissionResource.UpdateApi();
		PermissionResource.SaveRequest param = new PermissionResource.SaveRequest();
		param.id = expPermission.Id;
		param.name_L0 = expPermission.Name_L0__c;
		param.code = expPermission.Code__c;
		param.companyId = expPermission.CompanyId__c;

		App.NoPermissionException actEx;
		System.runAs(new User(Id = stdEmployee.userId)) {
			try {
				PermissionResource.SaveResult res = (PermissionResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * @description 権限作成API(createApi）のテスト
	 * 不正なパラメータが指定された場合に例外が発生することを確認する
	 */
	@isTest static void createApiParamErrTest() {
		//テストデータ作成
		TestData testData = new TestData();

		App.ParameterException actEx;
		// Case 1: 新規作成 かつ 会社IDがない場合
		ComPermission__c expPermission1 = ComTestDataUtility.createPermission('NoCompany', testData.companyObj.id);
		PermissionResource.SaveRequest request1 = new PermissionResource.SaveRequest();
		request1.name_L0 = expPermission1.Name_L0__c;
		request1.code = expPermission1.Code__c;
		boolean isUpdate = false;
		// 実行
		try {
			request1.validate(isUpdate);
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{ComMessage.msg().Admin_Lbl_Company}), actEx.getMessage());

		// Case 2: 更新 かつ idがない場合
		ComPermission__c expPermission2 = ComTestDataUtility.createPermission('NoId', testData.companyObj.id);
		PermissionResource.SaveRequest request2 = new PermissionResource.SaveRequest();
		request2.name_L0 = expPermission2.Name_L0__c;
		request2.code = expPermission2.Code__c;
		isUpdate = true;
		// 実行
		try {
			request2.validate(isUpdate);
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'id', null}), actEx.getMessage());

		// Case 3: Idがセットされている かつ 正しく変換できない場合
		ComPermission__c expPermission3 = ComTestDataUtility.createPermission('InvalidId', testData.companyObj.id);
		PermissionResource.SaveRequest request3 = new PermissionResource.SaveRequest();
		request3.id = 'AAAA';
		request3.name_L0 = expPermission3.Name_L0__c;
		request3.code = expPermission3.Code__c;
		isUpdate = true;
		// 実行
		try {
			request3.validate(isUpdate);
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'id', 'AAAA'}), actEx.getMessage());

		// Case 4: コードがない場合
		ComPermission__c expPermission4 = ComTestDataUtility.createPermission('NoCode', testData.companyObj.id);
		PermissionResource.SaveRequest request4 = new PermissionResource.SaveRequest();
		request4.id = expPermission4.CompanyId__c;// ID変換可能か
		request4.name_L0 = expPermission4.Name_L0__c;
		isUpdate = true;
		// 実行
		try {
			request4.validate(isUpdate);
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{ComMessage.msg().Admin_Lbl_Code}), actEx.getMessage());

		// Case 5: 権限名がない場合
		ComPermission__c expPermission5 = ComTestDataUtility.createPermission('NoName', testData.companyObj.id);
		PermissionResource.SaveRequest request5 = new PermissionResource.SaveRequest();
		request5.code = expPermission5.Code__c;
		request5.companyId = expPermission5.CompanyId__c;
		isUpdate = false;
		// 実行
		try {
			request5.validate(isUpdate);
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{ComMessage.msg().Admin_Lbl_Name}), actEx.getMessage());
	}

	/**
	 * 検索で取得した権限の各項目の値が期待値に一致することを確認する
	 */
	private static void assertPermissionRes(ComPermission__c expObj, PermissionResource.Permission actRes) {
		assertPermissionRes(expObj, actRes, null);
	}
	private static void assertPermissionRes(ComPermission__c expObj, PermissionResource.Permission actRes, ComOrgSetting__c org) {
		if (actRes == null && expObj != null) {
			TestUtil.fail('該当するデータがありません');
		}
		// 検証
		System.assertEquals(expObj.id, actRes.id);
		// 多言語対応項目
		if (org != null) {
			if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_0__c)) {
				System.assertEquals(expObj.Name_L0__c, actRes.name);
			} else if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_1__c)) {
				System.assertEquals(expObj.Name_L1__c, actRes.name);
			} else if (AppLanguage.getUserLang() == AppLanguage.valueOf(org.Language_2__c)) {
				System.assertEquals(expObj.Name_L2__c, actRes.name);
			}
		} else {
			System.assertEquals(expObj.Name_L0__c, actRes.name);
		}
		System.assertEquals(expObj.CompanyId__c, actRes.companyId);
		System.assertEquals(expObj.Code__c, actRes.code);
		System.assertEquals(expObj.Name_L0__c, actRes.name_L0);
		System.assertEquals(expObj.Name_L1__c, actRes.name_L1);
		System.assertEquals(expObj.Name_L2__c, actRes.name_L2);
		// 勤怠トランザクション系権限
		System.assertEquals(expObj.ViewAttTimeSheetByDelegate__c , actRes.viewAttTimesheetByDelegate);
		System.assertEquals(expObj.EditAttTimeSheetByDelegate__c, actRes.editAttTimesheetByDelegate);
		// 工数トランザクション系権限
		System.assertEquals(expObj.ViewTimeTrackByDelegate__c , actRes.viewTimeTrackByDelegate);
		// 勤怠申請系権限
		System.assertEquals(expObj.ApproveAttDailyRequestByDelegate__c , actRes.approveAttDailyRequestByDelegate);
		System.assertEquals(expObj.ApproveAttRequestByDelegate__c, actRes.approveAttRequestByDelegate);
		System.assertEquals(expObj.ApproveSelfAttDailyRequestByEmployee__c, actRes.approveSelfAttDailyRequestByEmployee);
		System.assertEquals(expObj.ApproveSelfAttRequestByEmployee__c, actRes.approveSelfAttRequestByEmployee);
		System.assertEquals(expObj.CancelAttApprovalByDelegate__c , actRes.cancelAttApprovalByDelegate);
		System.assertEquals(expObj.CancelAttApprovalByEmployee__c, actRes.cancelAttApprovalByEmployee);
		System.assertEquals(expObj.CancelAttDailyApprovalByDelegate__c , actRes.cancelAttDailyApprovalByDelegate);
		System.assertEquals(expObj.CancelAttDailyApprovalByEmployee__c, actRes.cancelAttDailyApprovalByEmployee);
		System.assertEquals(expObj.CancelAttDailyRequestByDelegate__c , actRes.cancelAttDailyRequestByDelegate);
		System.assertEquals(expObj.CancelAttRequestByDelegate__c, actRes.cancelAttRequestByDelegate);
		System.assertEquals(expObj.SubmitAttDailyRequestByDelegate__c , actRes.submitAttDailyRequestByDelegate);
		System.assertEquals(expObj.SubmitAttRequestByDelegate__c, actRes.submitAttRequestByDelegate);
		// 管理系権限
		System.assertEquals(expObj.ManageAttAgreementAlertSetting__c , actRes.manageAttAgreementAlertSetting);
		System.assertEquals(expObj.ManageAttLeave__c, actRes.manageAttLeave);
		System.assertEquals(expObj.ManageAttLeaveGrant__c , actRes.manageAttLeaveGrant);
		System.assertEquals(expObj.ManageAttLeaveOfAbsence__c, actRes.manageAttLeaveOfAbsence);
		System.assertEquals(expObj.ManageAttLeaveOfAbsenceApply__c , actRes.manageAttLeaveOfAbsenceApply);
		System.assertEquals(expObj.ManageAttPattern__c, actRes.manageAttPattern);
		System.assertEquals(expObj.ManageAttPatternApply__c, actRes.manageAttPatternApply);
		System.assertEquals(expObj.ManageAttShortTimeWorkSetting__c, actRes.manageAttShortTimeWorkSetting);
		System.assertEquals(expObj.ManageAttShortTimeWorkSettingApply__c , actRes.manageAttShortTimeWorkSettingApply);
		System.assertEquals(expObj.ManageAttWorkingType__c, actRes.manageAttWorkingType);
		System.assertEquals(expObj.ManageCalendar__c , actRes.manageCalendar);
		System.assertEquals(expObj.ManageDepartment__c, actRes.manageDepartment);
		System.assertEquals(expObj.ManageEmployee__c , actRes.manageEmployee);
		System.assertEquals(expObj.ManageJob__c, actRes.manageJob);
		System.assertEquals(expObj.ManageJobType__c , actRes.manageJobType);
		System.assertEquals(expObj.ManageMobileSetting__c, actRes.manageMobileSetting);
		System.assertEquals(expObj.ManageOverallSetting__c , actRes.manageOverallSetting);
		System.assertEquals(expObj.ManagePermission__c, actRes.managePermission);
		System.assertEquals(expObj.ManagePlannerSetting__c , actRes.managePlannerSetting);
		System.assertEquals(expObj.ManageTimeSetting__c, actRes.manageTimeSetting);
		System.assertEquals(expObj.ManageTimeWorkCategory__c , actRes.manageTimeWorkCategory);
		System.assertEquals(expObj.SwitchCompany__c , actRes.switchCompany);
		System.assertEquals(expObj.ManageExpTypeGroup__c , actRes.manageExpTypeGroup);
		System.assertEquals(expObj.ManageExpType__c , actRes.manageExpenseType);
		System.assertEquals(expObj.ManageExpTaxType__c , actRes.manageTaxType);
		System.assertEquals(expObj.ManageExpSetting__c , actRes.manageExpSetting);
		System.assertEquals(expObj.ManageExpExchangeRate__c , actRes.manageExchangeRate);
		System.assertEquals(expObj.ManageExpAccountingPeriod__c , actRes.manageAccountingPeriod);
		System.assertEquals(expObj.ManageExpReportType__c , actRes.manageReportType);
		System.assertEquals(expObj.ManageExpCostCenter__c , actRes.manageCostCenter);
		System.assertEquals(expObj.ManageExpVendor__c , actRes.manageVendor);
		System.assertEquals(expObj.ManageExpExtendedItem__c , actRes.manageExtendedItem);
		System.assertEquals(expObj.ManageExpEmployeeGroup__c , actRes.manageEmployeeGroup);
		System.assertEquals(expObj.ManageExpCustomHint__c, actRes.manageExpCustomHint);
	}

	/**
	 * @description 権限削除API(DeleteApi）のテスト
	 * 権限削除APIを実行し、正しく権限の削除ができることを確認する
	 */
	@isTest static void deleteApiTest() {
		//テストデータ作成
		TestData testData = new TestData();
		// - 権限の作成
		List<ComPermission__c> expPermissionList = new List<ComPermission__c>();
		expPermissionList.addAll(testData.createPermissions('delete', 2));
		// - 論理削除した権限の作成
		expPermissionList.set(1, TestData.logicalDelete(expPermissionList.get(1)));

		//Case1 : 権限削除APIを実行し、正しく権限の削除ができることを確認する
		// - パラメータ作成
		PermissionResource.DeleteApi api = new PermissionResource.DeleteApi();
		PermissionResource.DeleteRequest param = new PermissionResource.DeleteRequest();
		param.id = expPermissionList.get(0).id;
		// - 実行
		Test.startTest();
		try {
			api.execute(param);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		Test.stopTest();
		// - 検証
		ComPermission__c actPermission = testData.getPermissionObj(expPermissionList.get(0).id);
		System.assertEquals(null, actPermission);

		//Case2 : 論理削除済みでも削除可能
		param = new PermissionResource.DeleteRequest();
		param.id = expPermissionList.get(1).id;
		// - 実行
		try {
			api.execute(param);
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// - 検証
		actPermission = testData.getPermissionObj(expPermissionList.get(1).id);
		System.assertEquals(null, actPermission);
	}

	/**
	 * @description 権限削除API(DeleteApi）のテスト
	 * 不正なパラメータが指定された場合に例外が発生することを確認する
	 */
	@isTest static void deleteApiParamErrTest() {
		//テストデータ作成
		TestData testData = new TestData();

		// Case 1: idがない場合
		ComPermission__c expPermission = testData.createPermissions('DeleteValid', 1).get(0);
		PermissionResource.DeleteRequest request = new PermissionResource.DeleteRequest();
		// 実行
		App.ParameterException actEx;
		try {
			request.validate();
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'id'}), actEx.getMessage());

		// Case 2: idが型不正
		request = new PermissionResource.DeleteRequest();
		request.id = 'AAAA';
		// 実行
		try {
			request.validate();
		} catch (App.ParameterException e) {
			actEx = e;
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。'+ e.getMessage() + e.getStackTraceString());
		}
		// 検証
		System.assertNotEquals(null, actEx, 'App.ParameterExceptionが発生しませんでした。');
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, actEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'id', 'AAAA'}), actEx.getMessage());
	}

	/**
	 * @description 権限削除API(DeleteApi）のテスト
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest static void deleteApiTestNoPermission() {
		TestData testData = new TestData();

		// アクセス権限の管理権限を無効に設定する
		ComPermission__c permission = testData.createDefaultPermission('perm', 'No Permission Name', testData.companyObj);
		permission.ManagePermission__c = false;
		insert permission;
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('std-auth-User', permission.id);

		ComPermission__c expPermission = testData.createPermissions('DelNoPerm', 1).get(0);

		// - パラメータ作成
		PermissionResource.DeleteApi api = new PermissionResource.DeleteApi();
		PermissionResource.DeleteRequest param = new PermissionResource.DeleteRequest();
		param.id = expPermission.id;

		App.NoPermissionException actEx;
		System.runAs(new User(Id = stdEmployee.userId)) {
			try {
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}
}