/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 * @description 短時間勤務設定の履歴切り替えバッチ
 */
public with sharing class AttShortSettingCurrentHistoryUpdateBatch extends CurrentHistoryUpdateBatchBase implements Schedulable {

	/**
	 * コンストラクタ
	 */
	public AttShortSettingCurrentHistoryUpdateBatch() {
		// ベース・履歴オブジェクトのAPI参照名を渡す
		super(AttShortTimeWorkSettingBase__c.getSObjectType(), AttShortTimeWorkSettingHistory__c.getSObjectType());
	}

	/**
	 * Apex実行スケジュールを登録する
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc){
		AttShortSettingCurrentHistoryUpdateBatch b = new AttShortSettingCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
	}

	/**
	 * バッチ開始処理
	 * @param  bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public override Database.QueryLocator start(Database.BatchableContext bc) {
		// ベースオブジェクトを全取得する
		return super.start(bc);
	}

	/**
	 * バッチ実行処理
	 * @param  bc    バッチコンテキスト
	 * @param  scope
	 */
	public override void execute(Database.BatchableContext bc, List<SObject> scope) {
		super.execute(bc, scope);
	}

	/**
	 * バッチ終了処理
	 * @param  bc    バッチコンテキスト
	 */
	public override void finish(Database.BatchableContext bc) {
		super.finish(bc);
	}
}