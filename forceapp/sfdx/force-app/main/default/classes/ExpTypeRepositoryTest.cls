/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * @description 費目マスタのリポジトリのテスト
 */
@isTest
private class ExpTypeRepositoryTest {

	/** テストデータ */
	private class RepoTestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;
		/** 税区分オブジェクト */
		public List<ExpTaxTypeBase__c> taxTypeObjs;
		/** 拡張項目オブジェクト */
		public List<ComExtendedItem__c> extendedItemObjs;
		/** 拡張項目(選択リスト)オブジェクト */
		public List<ComExtendedItem__c> extendedItemPicklistObjs;
		public List<ComExtendedItem__c> extendedItemsLookupObjs;
		/** Expense Type Group */
		public List<ExpTypeGroup__c> expTypeGroupList;

		/** コンストラクタ */
		public RepoTestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
			taxTypeObjs = ComTestDataUtility.createTaxTypesWithHistory('Tax Type', null, this.companyObj.Id, 3);
			extendedItemObjs = ComTestDataUtility.createExtendedItems('Extended Item', this.companyObj.Id, 10);
			extendedItemPicklistObjs = ComTestDataUtility.createExtendedItems('Extended Item Picklist', this.companyObj.id, ExtendedItemInputType.PICKLIST, 10);
			List<ComExtendedItemCustom__c> extendedItemCustoms = ComTestDataUtility.createExtendedItemCustoms('Custom Obj', this.companyObj.id, 20);
			extendedItemsLookupObjs = ComTestDataUtility.createExtendedItemLookups('EILookup', this.companyObj.id, extendedItemCustoms);
			expTypeGroupList = ComTestDataUtility.createExpTypeGroups('EGDEFAULT', this.companyObj.id, 2);
		}

		/**
		 * 費目オブジェクトを作成する
		 */
		public List<ExpType__c> createExpTypes(String name, Integer size) {
			return ComTestDataUtility.createExpTypes(name, this.companyObj.Id, size, true);
		}

		/**
		 * 費目エンティティを作成する(DB保存なし)
		 */
		public ExpTypeEntity createExpTypeEntity(String name) {
			ExpTypeEntity entity = new ExpTypeEntity();

			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.companyObj.Code__c + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.companyObj.Id;
			entity.parentGroupId = null;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.setUsage('Normal');
			entity.fileAttachment = ExpFileAttachment.OPTIONAL;
			entity.recordType = ExpRecordType.GENERAL;
			entity.taxTypeBase1Id = this.taxTypeObjs[0].Id;
			entity.taxTypeBase2Id = this.taxTypeObjs[1].Id;
			entity.taxTypeBase3Id = this.taxTypeObjs[2].Id;

			for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				entity.setExtendedItemTextId(i, this.extendedItemObjs[i].Id);
				entity.setExtendedItemTextUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT);
				entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);

				entity.setExtendedItemPicklistId(i, this.extendedItemPicklistObjs[i].Id);
				entity.setExtendedItemPicklistUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT);
				entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);

				entity.setExtendedItemLookupId(i, this.extendedItemsLookupObjs[i].Id);
				entity.setExtendedItemLookupUsedIn(i, ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT);
				entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.EXPENSE_REPORT);
			}

			entity.order = 1;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);
			entity.useForeignCurrency = false;

			return entity;
		}

		/**
		 * 費目グループエンティティを作成する(DB保存なし)
		 */
		public ExpTypeGroupEntity createExpTypeGroupEntity(String name) {
			ExpTypeGroupEntity entity = new ExpTypeGroupEntity();

			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.companyObj.Code__c + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.companyObj.Id;
			entity.parentId = null;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';
			entity.order = 1;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);

			return entity;
		}

		/**
		 * 会社オブジェクトを作成する
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}

		/**
		 * Create expense type group records
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Integer size) {
			return createExpTypeGroups(name, this.companyObj.id, size);
		}

		/**
		 * Create expense type group records
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpTypeGroups(name, companyId, size);
			return (new ExpTypeGroupRepository()).getEntityList(null);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したIDのエンティティが取得できることを確認する
	 * 各項目の値が取得できているかの確認はsearchEntityTestで実施
	 */
	@isTest static void getEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpType__c> testGroups = testData.createExpTypes('検索テスト', recordSize);
		ExpType__c targetObj = testGroups[1];

		ExpTypeRepository repo = new ExpTypeRepository();
		ExpTypeEntity resEntity;

		// 存在するIDを指定する
		resEntity = repo.getEntity(targetObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetObj.Id, resEntity.id);

		// 存在しないIDを指定する
		delete targetObj;
		resEntity = repo.getEntity(targetObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * 正しく検索できることを確認する
	 * 有効期間での検索はsearchEntityTestValidDateで実施
	 */
	@isTest static void searchEntityTest() {

		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ComCompany__c testCompanyObj = testData.createCompany('検索テスト会社');
		List<ExpType__c> testGroups = testData.createExpTypes('検索テスト', recordSize);
		ExpType__c targetObj = testGroups[1];

		ExpTypeRepository repo = new ExpTypeRepository();
		ExpTypeRepository.SearchFilter filter;
		List<ExpTypeEntity> resEntities;

		// 費目グループIDで検索
		filter = new ExpTypeRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj.Id, resEntities[0].id);
		// 項目値の検証
		assertFieldValue(targetObj, resEntities[0]);
	}

	/**
	 * エンティティ取得のテスト
	 * 指定した対象日に有効なエンティティが取得できることを確認する
	 */
	@isTest static void searchEntityTestValidDate() {

		// テストデータ作成
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ComCompany__c testCompanyObj = testData.createCompany('検索テスト会社');
		List<ExpType__c> targetObjs = testData.createExpTypes('検索テスト', recordSize);
		ExpType__c targetObj1 = targetObjs[0];
		ExpType__c targetObj2 = targetObjs[1];
		ExpType__c targetObj3 = targetObjs[2];
		targetObj2.ValidFrom__c = targetObj1.ValidTo__c;
		targetObj2.ValidTo__c = targetObj2.ValidFrom__c.addYears(1);
		targetObj3.ValidFrom__c = targetObj2.ValidTo__c;
		targetObj3.ValidTo__c = targetObj3.ValidFrom__c.addYears(1);
		update targetObjs;

		ExpTypeRepository repo = new ExpTypeRepository();
		ExpTypeRepository.SearchFilter filter;
		List<ExpTypeEntity> resEntities;

		// 取得対象日に履歴の有効期間開始日を指定
		filter = new ExpTypeRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj2.ValidFrom__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		filter = new ExpTypeRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c.addDays(-1));
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj3.Id, resEntities[0].id);

		// 取得対象日に最新履歴の失効日を指定
		// 取得できないはず
		filter = new ExpTypeRepository.SearchFilter();
		filter.targetDate = AppDate.valueOf(targetObj3.ValidTo__c);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());
	}

	/**
	 * Search Expense Type with EmployeeGroup Name and Code
	 * Search with normal string to give subString,partial search,
	 * Search with % string to give user defined subString,partial search,
	 * Search with Employee Group Code String subString,partial search,
	 * Search with multiple Employee Group Code Set and Single Group Code Set
	 */
	@isTest static void searchEntityEmployeeGroupTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpType__c> testGroups = testData.createExpTypes('ExpTypeA', recordSize);
		testGroups[0].ParentGroupId__c = testData.expTypeGroupList[0].Id;
		testGroups[1].ParentGroupId__c = testData.expTypeGroupList[0].Id;
		testGroups[2].ParentGroupId__c = testData.expTypeGroupList[1].Id;
		update testGroups;

		ExpTypeRepository repo = new ExpTypeRepository();
		ExpTypeRepository.SearchFilter filter = new ExpTypeRepository.SearchFilter();
		List<ExpTypeEntity> resEntities;

		// Search by Group Name
		filter.expGroupName = 'DEFAULT';
		resEntities = repo.searchEntity(filter);
		System.assertEquals(3, resEntities.size());
		System.assertEquals(testGroups[0].Id, resEntities[0].id);

		// Search by Group Name When user put %
		filter = new ExpTypeRepository.SearchFilter();
		filter.expGroupName = 'DEFAULT%';
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());

		filter.expGroupName = 'DEFAULT0';
		resEntities = repo.searchEntity(filter);
		System.assertEquals(2, resEntities.size());
		System.assertEquals(testGroups[0].Id, resEntities[0].id);

		filter.expGroupName = 'DEFAULT1';
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(testGroups[2].Id, resEntities[0].id);

		// Search by Group Code
		filter = new ExpTypeRepository.SearchFilter();
		filter.expGroupCode = testData.expTypeGroupList[0].Code__c.substring(4);
		resEntities = repo.searchEntity(filter);
		System.assertEquals(2, resEntities.size());

		// Search by Group Code Set
		filter = new ExpTypeRepository.SearchFilter();
		filter.expGroupCodeSet = new Set<String>{testData.expTypeGroupList[0].Code__c, testData.expTypeGroupList[1].Code__c };
		resEntities = repo.searchEntity(filter);
		System.assertEquals(3, resEntities.size());
		System.assertEquals(testGroups[0].Id, resEntities[0].id);

		filter.expGroupCodeSet = new Set<String>{testData.expTypeGroupList[0].Code__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(2, resEntities.size());
		System.assertEquals(testGroups[0].Id, resEntities[0].id);
	}

	/**
	 * Search Expense Type with Employee Group Param Negative Test
	 * Search unassigned group name, code
	 * Search group code with partial substring
	 */
	@isTest static void searchEntityEmployeeGroupNegativeTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		List<ExpType__c> testGroups = testData.createExpTypes('ExpTypeA', recordSize);
		testGroups[0].ParentGroupId__c = testData.expTypeGroupList[0].Id;
		testGroups[1].ParentGroupId__c = testData.expTypeGroupList[0].Id;
		testGroups[2].ParentGroupId__c = testData.expTypeGroupList[0].Id;
		update testGroups;

		ExpTypeRepository repo = new ExpTypeRepository();
		ExpTypeRepository.SearchFilter filter = new ExpTypeRepository.SearchFilter();
		List<ExpTypeEntity> resEntities;

		// Search by Group Name Not assigned
		filter.expGroupName = 'DEFAULT1';
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());

		// Search by Group Code Not assigned
		filter = new ExpTypeRepository.SearchFilter();
		filter.expGroupCodeSet = new Set<String>{testData.expTypeGroupList[1].Code__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());

		// Search by Group Code With partial Code
		filter.expGroupCodeSet = new Set<String>{testData.expTypeGroupList[0].Code__c.substring(1)};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(0, resEntities.size());
	}

	/**
	 * Search Expense Type with query Param Test
	 * Search ExpType with common code and name, also test for escape character _ . * %
	 */
	@isTest static void searchEntityQueryTest() {
		final Integer recordSize = 7;
		RepoTestData data = new RepoTestData();
		List<ExpType__c> expTypeList = data.createExpTypes('ET', recordSize);
		expTypeList[0].Code__c = 'XYZ';
		expTypeList[1].Name_L0__c = 'XYZ Expense Type';
		expTypeList[2].Name_L0__c = '_ABC Expense Type';
		expTypeList[3].Name_L0__c = 'D.DEF Expense Type';
		expTypeList[4].Name_L0__c = 'GHI* Expense Type';
		expTypeList[5].Name_L0__c = 'J%KL Expense Type';
		expTypeList[6].Name_L0__c = 'M&N Expense Type';
		update expTypeList;

		ExpTypeRepository repo = new ExpTypeRepository();
		ExpTypeRepository.SearchFilter filter = new ExpTypeRepository.SearchFilter();

		// Test both code and name is checked
		filter.query = 'XYZ';
		List<ExpTypeEntity> entity = repo.searchEntity(filter);
		System.assertEquals(2, entity.size());
		for (Integer i = 0; i < entity.size(); i++) {
			assertFieldValue(entity[i], expTypeList[entity.size() - i - 1]);
		}

		// Test _ is checked
		filter.query = '_ABC';
		entity = repo.searchEntity(filter);
		System.assertEquals(1, entity.size());
		assertFieldValue(entity[0], expTypeList[2]);

		// Test . is checked
		filter.query = '.D';
		entity = repo.searchEntity(filter);
		System.assertEquals(1, entity.size());
		assertFieldValue(entity[0], expTypeList[3]);

		// Test * is checked
		filter.query = '*';
		entity = repo.searchEntity(filter);
		System.assertEquals(1, entity.size());
		assertFieldValue(entity[0], expTypeList[4]);

		// Test % is checked
		filter.query = '%';
		entity = repo.searchEntity(filter);
		System.assertEquals(1, entity.size());
		assertFieldValue(entity[0], expTypeList[5]);

		// Test & is checked
		filter.query = '&';
		entity = repo.searchEntity(filter);
		System.assertEquals(1, entity.size());
		assertFieldValue(entity[0], expTypeList[6]);

	}

	/**
	 * saveEntityのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ExpTypeEntity expType1 = testData.createExpTypeEntity('費目1');

		// テスト実行
		ExpTypeRepository repo = new ExpTypeRepository();
		Repository.SaveResult result = repo.saveEntity(expType1);

		// 確認
		ExpType__c resObj1 = getExpTypeObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(expType1, resObj1);
	}

	/**
	 * saveEntityのテスト
	 * 親子でのエンティティ保存できることを確認する
	 */
	@isTest static void saveEntityParentTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ExpTypeGroupEntity group1 = testData.createExpTypeGroupEntity('費目グループ1');
		ExpTypeEntity expType1 = testData.createExpTypeEntity('費目1');
		expType1.parentGroupId = group1.id;

		// テスト実行
		ExpTypeRepository repo = new ExpTypeRepository();
		Repository.SaveResult result = repo.saveEntity(expType1);

		// 確認
		ExpType__c resObj1 = getExpTypeObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		System.assertEquals(expType1.parentGroupId, group1.id);
		assertFieldValue(expType1, resObj1);
	}

	/**
	 * saveEntityListのテスト
	 * エンティティ保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		final Integer recordSize = 3;
		RepoTestData testData = new RepoTestData();
		ExpTypeEntity expType1 = testData.createExpTypeEntity('費目1');
		ExpTypeEntity expType2 = testData.createExpTypeEntity('費目2');
		expType2.validFrom = AppDate.today();
		expType2.validTo = expType2.validFrom.addMonths(12);
		expType2.parentGroupId = expType1.id;

		// テスト実行
		ExpTypeRepository repo = new ExpTypeRepository();
		Repository.SaveResult result = repo.saveEntityList(new List<ExpTypeEntity>{expType1, expType2});

		// 確認
		ExpType__c resObj1 = getExpTypeObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(expType1, resObj1);
		ExpType__c resObj2 = getExpTypeObj(result.details[1].id);
		System.assertNotEquals(null, resObj2);
		assertFieldValue(expType2, resObj2);
	}

	/**
	 * 費目グループオブジェクトを取得する
	 * @param groupId 取得対象の費目グループID
	 * @return 費目グループオブジェクト
	 */
	private static ExpType__c getExpTypeObj(Id expTypeId) {
		return [
				SELECT
					Id,
					Name,
					Code__c,
					UniqKey__c,
					Name_L0__c,
					Name_L1__c,
					Name_L2__c,
					CompanyId__c,
					ParentGroupId__c,
					Description_L0__c,
					Description_L1__c,
					Description_L2__c,
					Usage__c,
					FileAttachment__c,
					RecordType__c,
					TaxTypeBase1Id__c,
					TaxTypeBase2Id__c,
					TaxTypeBase3Id__c,
					ExtendedItemText01TextId__c,
					ExtendedItemText01UsedIn__c,
					ExtendedItemText01RequiredFor__c,
					ExtendedItemText02TextId__c,
					ExtendedItemText02UsedIn__c,
					ExtendedItemText02RequiredFor__c,
					ExtendedItemText03TextId__c,
					ExtendedItemText03UsedIn__c,
					ExtendedItemText03RequiredFor__c,
					ExtendedItemText04TextId__c,
					ExtendedItemText04UsedIn__c,
					ExtendedItemText04RequiredFor__c,
					ExtendedItemText05TextId__c,
					ExtendedItemText05UsedIn__c,
					ExtendedItemText05RequiredFor__c,
					ExtendedItemText06TextId__c,
					ExtendedItemText06UsedIn__c,
					ExtendedItemText06RequiredFor__c,
					ExtendedItemText07TextId__c,
					ExtendedItemText07UsedIn__c,
					ExtendedItemText07RequiredFor__c,
					ExtendedItemText08TextId__c,
					ExtendedItemText08UsedIn__c,
					ExtendedItemText08RequiredFor__c,
					ExtendedItemText09TextId__c,
					ExtendedItemText09UsedIn__c,
					ExtendedItemText09RequiredFor__c,
					ExtendedItemText10TextId__c,
					ExtendedItemText10UsedIn__c,
					ExtendedItemText10RequiredFor__c,
					ExtendedItemPicklist01TextId__c,
					ExtendedItemPicklist01UsedIn__c,
					ExtendedItemPicklist01RequiredFor__c,
					ExtendedItemPicklist02TextId__c,
					ExtendedItemPicklist02UsedIn__c,
					ExtendedItemPicklist02RequiredFor__c,
					ExtendedItemPicklist03TextId__c,
					ExtendedItemPicklist03UsedIn__c,
					ExtendedItemPicklist03RequiredFor__c,
					ExtendedItemPicklist04TextId__c,
					ExtendedItemPicklist04UsedIn__c,
					ExtendedItemPicklist04RequiredFor__c,
					ExtendedItemPicklist05TextId__c,
					ExtendedItemPicklist05UsedIn__c,
					ExtendedItemPicklist05RequiredFor__c,
					ExtendedItemPicklist06TextId__c,
					ExtendedItemPicklist06UsedIn__c,
					ExtendedItemPicklist06RequiredFor__c,
					ExtendedItemPicklist07TextId__c,
					ExtendedItemPicklist07UsedIn__c,
					ExtendedItemPicklist07RequiredFor__c,
					ExtendedItemPicklist08TextId__c,
					ExtendedItemPicklist08UsedIn__c,
					ExtendedItemPicklist08RequiredFor__c,
					ExtendedItemPicklist09TextId__c,
					ExtendedItemPicklist09UsedIn__c,
					ExtendedItemPicklist09RequiredFor__c,
					ExtendedItemPicklist10TextId__c,
					ExtendedItemPicklist10UsedIn__c,
					ExtendedItemPicklist10RequiredFor__c,
					ExtendedItemLookup01TextId__c,
					ExtendedItemLookup01UsedIn__c,
					ExtendedItemLookup01RequiredFor__c,
					ExtendedItemLookup02TextId__c,
					ExtendedItemLookup02UsedIn__c,
					ExtendedItemLookup02RequiredFor__c,
					ExtendedItemLookup03TextId__c,
					ExtendedItemLookup03UsedIn__c,
					ExtendedItemLookup03RequiredFor__c,
					ExtendedItemLookup04TextId__c,
					ExtendedItemLookup04UsedIn__c,
					ExtendedItemLookup04RequiredFor__c,
					ExtendedItemLookup05TextId__c,
					ExtendedItemLookup05UsedIn__c,
					ExtendedItemLookup05RequiredFor__c,
					ExtendedItemLookup06TextId__c,
					ExtendedItemLookup06UsedIn__c,
					ExtendedItemLookup06RequiredFor__c,
					ExtendedItemLookup07TextId__c,
					ExtendedItemLookup07UsedIn__c,
					ExtendedItemLookup07RequiredFor__c,
					ExtendedItemLookup08TextId__c,
					ExtendedItemLookup08UsedIn__c,
					ExtendedItemLookup08RequiredFor__c,
					ExtendedItemLookup09TextId__c,
					ExtendedItemLookup09UsedIn__c,
					ExtendedItemLookup09RequiredFor__c,
					ExtendedItemLookup10TextId__c,
					ExtendedItemLookup10UsedIn__c,
					ExtendedItemLookup10RequiredFor__c,
					Order__c,
					ValidFrom__c,
					ValidTo__c,
					UseForeignCurrency__c
				FROM
					ExpType__c
				WHERE
					Id = :expTypeId
		];
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(ExpTypeEntity expEntity, ExpType__c actObj) {

		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
		System.assertEquals(expEntity.parentGroupId, actObj.ParentGroupId__c);
		System.assertEquals(expEntity.descriptionL0, actObj.Description_L0__c);
		System.assertEquals(expEntity.descriptionL1, actObj.Description_L1__c);
		System.assertEquals(expEntity.descriptionL2, actObj.Description_L2__c);
		System.assertEquals(String.valueOf(expEntity.usage), actObj.Usage__c);
		System.assertEquals(expEntity.fileAttachment.value, actObj.FileAttachment__c);
		System.assertEquals(expEntity.recordType.value, actObj.RecordType__c);
		System.assertEquals(expEntity.taxTypeBase1Id, actObj.TaxTypeBase1Id__c);
		System.assertEquals(expEntity.taxTypeBase2Id, actObj.TaxTypeBase2Id__c);
		System.assertEquals(expEntity.taxTypeBase3Id, actObj.TaxTypeBase3Id__c);
		for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			System.assertEquals(expEntity.getExtendedItemTextId(i),
				(Id) actObj.get(ExpTypeRepository.EXTENDED_ITEM_TEXT + index + ExpTypeRepository.EXTENDED_ITEM_ID));
			System.assertEquals(expEntity.getExtendedItemTextUsedIn(i).value,
				(String) actObj.get(ExpTypeRepository.EXTENDED_ITEM_TEXT + index + ExpTypeRepository.EXTENDED_ITEM_USED_IN));
			System.assertEquals(expEntity.getExtendedItemTextRequiredFor(i).value,
				(String) actObj.get(ExpTypeRepository.EXTENDED_ITEM_TEXT + index + ExpTypeRepository.EXTENDED_ITEM_REQUIRED_FOR));

			System.assertEquals(expEntity.getExtendedItemPicklistId(i),
				(Id) actObj.get(ExpTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpTypeRepository.EXTENDED_ITEM_ID));
			System.assertEquals(expEntity.getExtendedItemPicklistUsedIn(i).value,
				(String) actObj.get(ExpTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpTypeRepository.EXTENDED_ITEM_USED_IN));
			System.assertEquals(expEntity.getExtendedItemPicklistRequiredFor(i).value,
				(String) actObj.get(ExpTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpTypeRepository.EXTENDED_ITEM_REQUIRED_FOR));

			System.assertEquals(expEntity.getExtendedItemLookupId(i),
				(Id) actObj.get(ExpTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpTypeRepository.EXTENDED_ITEM_ID));
			System.assertEquals(expEntity.getExtendedItemLookupUsedIn(i).value,
				(String) actObj.get(ExpTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpTypeRepository.EXTENDED_ITEM_USED_IN));
			System.assertEquals(expEntity.getExtendedItemLookupRequiredFor(i).value,
				(String) actObj.get(ExpTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpTypeRepository.EXTENDED_ITEM_REQUIRED_FOR));
		}
		System.assertEquals(expEntity.order, Integer.valueOf(actObj.Order__c));
		System.assertEquals(expEntity.validFrom, AppDate.valueOf(actObj.ValidFrom__c));
		System.assertEquals(expEntity.validTo, AppDate.valueOf(actObj.ValidTo__c));
		System.assertEquals(expEntity.useForeignCurrency, actObj.UseForeignCurrency__c);
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(ExpType__c expObj, ExpTypeEntity actEntity) {

		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.ParentGroupId__c, actEntity.parentGroupId);
		System.assertEquals(expObj.Description_L0__c, actEntity.descriptionL0);
		System.assertEquals(expObj.Description_L1__c, actEntity.descriptionL1);
		System.assertEquals(expObj.Description_L2__c, actEntity.descriptionL2);
		System.assertEquals(expObj.Usage__c, String.valueOf(actEntity.usage));
		System.assertEquals(expObj.FileAttachment__c, actEntity.fileAttachment.value);
		System.assertEquals(expObj.RecordType__c, actEntity.recordType.value);
		System.assertEquals(expObj.TaxTypeBase1Id__c, actEntity.taxTypeBase1Id);
		System.assertEquals(expObj.TaxTypeBase2Id__c, actEntity.taxTypeBase2Id);
		System.assertEquals(expObj.TaxTypeBase3Id__c, actEntity.taxTypeBase3Id);

		for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			System.assertEquals(
				(Id) expObj.get(ExpTypeRepository.EXTENDED_ITEM_TEXT + index + ExpTypeRepository.EXTENDED_ITEM_ID), actEntity.getExtendedItemTextId(i));
			System.assertEquals(
				(String) expObj.get(ExpTypeRepository.EXTENDED_ITEM_TEXT + index + ExpTypeRepository.EXTENDED_ITEM_USED_IN), actEntity.getExtendedItemTextUsedIn(i).value);
			System.assertEquals(
				(String) expObj.get(ExpTypeRepository.EXTENDED_ITEM_TEXT + index + ExpTypeRepository.EXTENDED_ITEM_REQUIRED_FOR), actEntity.getExtendedItemTextRequiredFor(i).value);

			System.assertEquals(
				(Id) expObj.get(ExpTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpTypeRepository.EXTENDED_ITEM_ID), actEntity.getExtendedItemPicklistId(i));
			System.assertEquals(
				(String) expObj.get(ExpTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpTypeRepository.EXTENDED_ITEM_USED_IN), actEntity.getExtendedItemPicklistUsedIn(i).value);
			System.assertEquals(
				(String) expObj.get(ExpTypeRepository.EXTENDED_ITEM_PICKLIST + index + ExpTypeRepository.EXTENDED_ITEM_REQUIRED_FOR), actEntity.getExtendedItemPicklistRequiredFor(i).value);

			System.assertEquals(
				(Id) expObj.get(ExpTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpTypeRepository.EXTENDED_ITEM_ID), actEntity.getExtendedItemLookupId(i));
			System.assertEquals(
				(String) expObj.get(ExpTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpTypeRepository.EXTENDED_ITEM_USED_IN), actEntity.getExtendedItemLookupUsedIn(i).value);
			System.assertEquals(
				(String) expObj.get(ExpTypeRepository.EXTENDED_ITEM_LOOKUP + index + ExpTypeRepository.EXTENDED_ITEM_REQUIRED_FOR), actEntity.getExtendedItemLookupRequiredFor(i).value);
		}

		System.assertEquals(expObj.Order__c, actEntity.order);
		System.assertEquals(AppDate.valueOf(expObj.ValidFrom__c), actEntity.validFrom);
		System.assertEquals(AppDate.valueOf(expObj.ValidTo__c), actEntity.validTo);
		System.assertEquals(expObj.UseForeignCurrency__c, actEntity.useForeignCurrency);
	}

}