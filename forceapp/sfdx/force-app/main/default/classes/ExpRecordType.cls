/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 経費明細タイプ
 */
public with sharing class ExpRecordType extends ValueObjectType {

	/** 一般 */
	public static final ExpRecordType GENERAL = new ExpRecordType('General');
	/** 一般（領収書任意） */
	/** TODO: Remove GENERAL_RECEIPT_OPTIONAL after all references to this record type has been moved to GENERAL */
	public static final ExpRecordType GENERAL_RECEIPT_OPTIONAL = new ExpRecordType('GeneralReceiptOptional');
	/** 一般（領収書不要） */
	/** TODO: Remove GENERAL_RECEIPT_HIDDEN after all references to this record type has been moved to GENERAL */
	public static final ExpRecordType GENERAL_RECEIPT_HIDDEN = new ExpRecordType('GeneralReceiptHidden');
	/** ジョルダン（日本） */
	public static final ExpRecordType TRANSIT_JORUDAN_JP = new ExpRecordType('TransitJorudanJP');
	/** 宿泊料金 */
	public static final ExpRecordType HOTEL_FEE = new ExpRecordType('HotelFee');
	/** Fixed Allowance Single Type */
	public static final ExpRecordType FIXED_ALLOWANCE_SINGLE = new ExpRecordType('FixedAllowanceSingle');
	/** Fixed Allowance Multi Type */
	public static final ExpRecordType FIXED_ALLOWANCE_MULTI = new ExpRecordType('FixedAllowanceMulti');

	/** 経費明細タイプのリスト（明細タイプが追加されら本リストにも追加してください) */
	/** TODO: Remove GENERAL_RECEIPT_OPTIONAL and GENERAL_RECEIPT_HIDDEN after all references to these record type has been moved to GENERAL */
	public static final List<ExpRecordType> TYPE_LIST;
	static {
		TYPE_LIST = new List<ExpRecordType> {
			GENERAL,
			GENERAL_RECEIPT_OPTIONAL,
			GENERAL_RECEIPT_HIDDEN,
			TRANSIT_JORUDAN_JP,
			HOTEL_FEE,
			FIXED_ALLOWANCE_SINGLE,
			FIXED_ALLOWANCE_MULTI
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ExpRecordType(String value) {
		super(value);
	}

	/**
	 * 値からExpRecordTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つExpRecordType、ただし該当するExpRecordTypeが存在しない場合はnull
	 */
	public static ExpRecordType valueOf(String value) {
		ExpRecordType retType = null;
		if (String.isNotBlank(value)) {
			for (ExpRecordType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ExpRecordType以外の場合はfalse
		Boolean eq;
		if (compare instanceof ExpRecordType) {
			// 値が同じであればtrue
			if (this.value == ((ExpRecordType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}