/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * Entity Class for Currency Object
 */
public with sharing class CurrencyEntity extends LogicalDeleteEntity {

	/** Max length of the code field */
	public static final Integer CODE_MAX_LENGTH = 3;
	/** Max length of the name field */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** Max decimal places */
	public static final Integer MAX_DECIMAL_PLACES = 3;
	/** Max length of the symbol field */
	public static final Integer SYMBOL_MAX_LENGTH = 5;

	/**
	 * Definition of the field
	 * If field is added to the object, please add field in getFieldValue() and setFieldValue() method
	 */
	public enum Field {
		NAME,
		CODE,
		ISO_CURRENCY_CODE,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		DECIMAL_PLACES,
		SYMBOL
	}

	/** Set of fields whose value has changed */
	protected Set<CurrencyEntity.Field> isChangedFieldSet = new Set<CurrencyEntity.Field>();

	/** Currency Name */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** Key Code */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	/** ISO Currency Code */
	public ComIsoCurrencyCode isoCurrencyCode {
		get;
		set {
			isoCurrencyCode = value;
			setChanged(Field.ISO_CURRENCY_CODE);
		}
	}

	/** Currency Name(Multilingual) */
	public AppMultiString nameL {
		get {return new AppMultiString(nameL0, nameL1, nameL2);}
	}
	/** Currency Name(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}
	/** Currency Name(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}
	/** Currency Name(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/** Decimal places */
	public Integer decimalPlaces {
		get;
		set {
			decimalPlaces = value;
			setChanged(Field.DECIMAL_PLACES);
		}
	}

	/** Currency Symbol */
	public String symbol {
		get;
		set {
			symbol = value;
			setChanged(Field.SYMBOL);
		}
	}

	/**
	 * Set field as changed
	 * @param field Field to be set
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public override void resetChangedInSubClass() {
		this.isChangedFieldSet.clear();
	}

}