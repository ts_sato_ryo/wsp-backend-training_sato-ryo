/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @group Expense
 *
 * @description Test class for ExpReportRepository
 */
@IsTest
private class ExpReportRepositoryTest {

	/*
	 * Test that columns can be selectively retrieved.
	 */
	@IsTest static void selectiveColumnRetrievalInSearchTest() {
		ExpTestData testData = new ExpTestData();
		AppDate targetDate = AppDate.today();
		ExpRequestEntity preRequest = testData.createExpRequest(AppDate.convertDate(targetDate), testData.department, testData.employee, testData.job);
		// Set manager to employee
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		testData.employee.getHistory(0).managerId = testEmployeeList[0].id;
		new EmployeeRepository().saveEntity(testData.employee);

		// Application user
		User user = [SELECT Id FROM User WHERE Id = :testEmployeeList[1].userId LIMIT 1];
		System.runAs(user) {
			ExpRequestApprovalService.submitRequest(preRequest.id, 'test');
		}

		ExpReportEntity expReportEntity = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job,
				testData.costCenter.CurrentHistoryId__c,1)[0];
		ExpReport__c reportObj = [SELECT Id, ExpPreRequestId__c From ExpReport__c WHERE Id = :expReportEntity.id];
		reportObj.ExpPreRequestId__c = preRequest.id;
		UPDATE reportObj;

		ExpRequest__c requestObj = [SELECT Id, RequestNo__c, Subject__c, TotalAmount__c, Purpose__c From ExpRequest__c WHERE Id = :preRequest.id];
		expReportEntity.expPreRequestId = preRequest.id;
		expReportEntity.expPreRequest = new ExpRequestEntity();
		expReportEntity.expPreRequest.setId(preRequest.id);
		expReportEntity.expPreRequest.requestNo = requestObj.RequestNo__c;
		expReportEntity.expPreRequest.subject = requestObj.Subject__c;
		expReportEntity.expPreRequest.purpose = requestObj.Purpose__c;
		expReportEntity.expPreRequest.totalAmount = requestObj.TotalAmount__c;

		ExpReportRepository reportRepo = new ExpReportRepository();
		List<Id> employeeIdList = new List<Id>{testData.employee.id};
		Test.startTest();
		// Case 1: When null is specified, all columns are retrieved
		List<ExpReportEntity> retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, null, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, null);

		// Case 2: When empty set is specified, only ID is retrieved
		Set<ExpReportRepository.FieldSelector> fieldSelector = new Set<ExpReportRepository.FieldSelector>();
		retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, fieldSelector, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, fieldSelector);

		// Case 3: Retrieve REPORT_BASIC
		fieldSelector = new Set<ExpReportRepository.FieldSelector>{ExpReportRepository.FieldSelector.REPORT_BASIC};
		retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, fieldSelector, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, fieldSelector);

		// Case 4: Retrieve REPORT_BASIC, and REPORT_DETAIL
		fieldSelector = new Set<ExpReportRepository.FieldSelector>{
			ExpReportRepository.FieldSelector.REPORT_BASIC,
			ExpReportRepository.FieldSelector.REPORT_DETAIL
		};
		retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, fieldSelector, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, fieldSelector);

		// Case 5: Retrieve REPORT_BASIC and REPORT_DETAIL, and PRE_REQUEST_DETAIL
		fieldSelector = new Set<ExpReportRepository.FieldSelector>{
			ExpReportRepository.FieldSelector.REPORT_BASIC,
			ExpReportRepository.FieldSelector.REPORT_DETAIL,
			ExpReportRepository.FieldSelector.PRE_REQUEST_DETAIL
		};
		retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, fieldSelector, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, fieldSelector);

		// Case 6: Retrieve REPORT_BASIC and REQUEST_BASIC
		fieldSelector = new Set<ExpReportRepository.FieldSelector>{
			ExpReportRepository.FieldSelector.REPORT_BASIC,
			ExpReportRepository.FieldSelector.REQUEST_BASIC
		};
		retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, fieldSelector, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, fieldSelector);

		// Case 7: Retrieve REPORT_BASIC and REQUEST_BASIC, and REQUEST_DETAIL
		fieldSelector = new Set<ExpReportRepository.FieldSelector>{
				ExpReportRepository.FieldSelector.REPORT_BASIC,
				ExpReportRepository.FieldSelector.REQUEST_BASIC,
				ExpReportRepository.FieldSelector.REQUEST_DETAIL
				};
		retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, fieldSelector, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, fieldSelector);

		// Case 8: REPORT_BASIC, REPORT_DETAIL, PRE_REQUEST_DETAIL, REQUEST_BASIC, REQUEST_DETAIL
		fieldSelector = new Set<ExpReportRepository.FieldSelector> {
			ExpReportRepository.FieldSelector.REPORT_BASIC,
			ExpReportRepository.FieldSelector.REPORT_DETAIL,
			ExpReportRepository.FieldSelector.PRE_REQUEST_DETAIL,
			ExpReportRepository.FieldSelector.REQUEST_BASIC,
			ExpReportRepository.FieldSelector.REQUEST_DETAIL
		};
		retrievedEntityList = reportRepo.getEntityListByEmpId(employeeIdList, fieldSelector, false);
		verifySelectiveColumnRetrieval(expReportEntity, retrievedEntityList, fieldSelector);
		Test.stopTest();
	}

	/*
	 * Assert the retrieved data with expected data. The data to be compared is decided based on the fieldSelectorSet.
	 * @param expected Sourced ExpReportEntity
	 * @param actualList List of Target ExpReportEntity
	 * @param fieldSelectorSet indicates which fields are retrieved from DB.
	 */
	private static void verifySelectiveColumnRetrieval(ExpReportEntity expected, List<ExpReportEntity> actualList, Set<ExpReportRepository.FieldSelector> fieldSelectorSet) {
		System.assertEquals(1, actualList.size());
		ExpReportEntity actual = actualList[0];

		System.assertEquals(expected.id, actual.id);

		if (fieldSelectorSet == null || fieldSelectorSet.contains(ExpReportRepository.FieldSelector.REPORT_BASIC)) {
			System.assertEquals(expected.name, actual.name);
			System.assertEquals(expected.expRequestId, actual.expRequestId);
			System.assertEquals(expected.expPreRequestId, actual.expPreRequestId);
			System.assertEquals(expected.totalAmount, actual.totalAmount);
			System.assertEquals(expected.reportNo, actual.reportNo);
			System.assertEquals(expected.subject, actual.subject);
			System.assertEquals(expected.expReportTypeId, actual.expReportTypeId);
			System.assertEquals(expected.expReportTypeNameL.getValue(), actual.expReportTypeNameL.getValue());
			System.assertEquals(expected.expReportTypeCode, actual.expReportTypeCode);
			System.assertEquals(expected.vendorRequiredFor, actual.vendorRequiredFor);

			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				System.assertEquals(expected.getExtendedItemTextRequiredFor(i), actual.getExtendedItemTextRequiredFor(i));
				System.assertEquals(expected.getExtendedItemPicklistRequiredFor(i), actual.getExtendedItemPicklistRequiredFor(i));
				System.assertEquals(expected.getExtendedItemLookupRequiredFor(i), actual.getExtendedItemLookupRequiredFor(i));
			}
		}
		if (fieldSelectorSet == null || fieldSelectorSet.contains(ExpReportRepository.FieldSelector.REPORT_DETAIL)) {
			System.assertEquals(expected.accountingDate, actual.accountingDate);
			System.assertEquals(expected.accountingPeriodId, actual.accountingPeriodId);
			System.assertEquals(expected.prefix, actual.prefix);
			System.assertEquals(expected.sequenceNo, actual.sequenceNo);
			System.assertEquals(expected.departmentHistoryId, actual.departmentHistoryId);
			System.assertEquals(expected.employeeHistoryId, actual.employeeHistoryId);
			System.assertEquals(expected.employeeBaseId, actual.employeeBaseId);
			System.assertEquals(expected.employeeNameL.getFullName(), actual.employeeNameL.getFullName());
			System.assertEquals(expected.employeeCode, actual.employeeCode);
			System.assertEquals(expected.companyId, actual.companyId);
			System.assertEquals(expected.remarks, actual.remarks);
			System.assertEquals(expected.jobId, actual.jobId);
			System.assertEquals(expected.jobNameL.getValue(), actual.jobNameL.getValue());
			System.assertEquals(expected.jobCode, actual.jobCode);
			System.assertEquals(expected.vendorId, actual.vendorId);
			System.assertEquals(expected.vendorNameL.getValue(), actual.vendorNameL.getValue());
			System.assertEquals(expected.vendorCode, actual.vendorCode);
			System.assertEquals(expected.useFileAttachment, actual.useFileAttachment);
			System.assertEquals(expected.paymentDueDate, actual.paymentDueDate);

			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				System.assertEquals(expected.getExtendedItemTextId(i), actual.getExtendedItemTextId(i));
				System.assertEquals(expected.getExtendedItemTextValue(i), actual.getExtendedItemTextValue(i));

				System.assertEquals(expected.getExtendedItemPicklistId(i), actual.getExtendedItemPicklistId(i));
				System.assertEquals(expected.getExtendedItemPicklistValue(i), actual.getExtendedItemPicklistValue(i));

				System.assertEquals(expected.getExtendedItemLookupId(i), actual.getExtendedItemLookupId(i));
				System.assertEquals(expected.getExtendedItemLookupValue(i), actual.getExtendedItemLookupValue(i));
			}
			System.assertEquals(expected.costCenterHistoryId, actual.costCenterHistoryId);
			System.assertEquals(expected.costCenterNameL.getValue(), actual.costCenterNameL.getValue());
			System.assertEquals(expected.costCenterLinkageCode, actual.costCenterLinkageCode);
			System.assertEquals(expected.costCenterCode, actual.costCenterCode);
			System.assertNotEquals(null, actual.lastModifiedDate);
		}
		if (fieldSelectorSet == null || fieldSelectorSet.contains(ExpReportRepository.FieldSelector.PRE_REQUEST_DETAIL)) {
			if (String.isNotBlank(expected.expPreRequestId)) {
				System.assertEquals(expected.expPreRequest.id, actual.expPreRequest.id);
				System.assertEquals(expected.expPreRequest.requestNo, actual.expPreRequest.requestNo);
				System.assertEquals(expected.expPreRequest.subject, actual.expPreRequest.subject);
				System.assertEquals(expected.expPreRequest.purpose, actual.expPreRequest.purpose);
				System.assertEquals(expected.expPreRequest.totalAmount, actual.expPreRequest.totalAmount);
			}
		}
		if (fieldSelectorSet == null || fieldSelectorSet.contains(ExpReportRepository.FieldSelector.REQUEST_BASIC)) {
			System.assertEquals(expected.expRequest.status, actual.expRequest.status);
			System.assertEquals(expected.expRequest.cancelType, actual.expRequest.cancelType);
			System.assertEquals(expected.expRequest.isConfirmationRequired, actual.expRequest.isConfirmationRequired);
			System.assertEquals(expected.expRequest.requestTime, actual.expRequest.requestTime);
		}
		if (fieldSelectorSet == null || fieldSelectorSet.contains(ExpReportRepository.FieldSelector.REQUEST_DETAIL)) {
			System.assertEquals(expected.departmentCode, actual.departmentCode);
			System.assertEquals(expected.departmentNameL.getValue(), actual.departmentNameL.getValue());
		}
	}
}