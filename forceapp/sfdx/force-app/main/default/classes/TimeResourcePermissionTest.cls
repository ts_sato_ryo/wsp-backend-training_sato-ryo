/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 工数
 *
 * TimeResourcePermissionクラスのテスト
 */
@isTest
public with sharing class TimeResourcePermissionTest {

	private static PermissionRepository permissionRepository = new PermissionRepository();

    /** テストデータクラス */
	private class TestData extends TestData.TestDataEntity {

		public TestData() {
			super();
			// 明示的に社員データを作成しておく
			this.employee = this.employee;
			// 権限マスタを本クラスのテスト用に初期化しておく
			initPermission(this.permission);
		}

		/**
		 * 権限マスタを初期化(工数系の全ての権限をtrue)する
		 */
		public void initPermission(PermissionEntity permission) {
            permission.isViewTimeTrackByDelegate = true;
			permissionRepository.saveEntity(permission);
		}
	}

	/**
	 * 工数実績表示(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestViewTimeTrackByDelegate() {
		TestData testData = new TestData();
		User delegeteUser = ComTestDataUtility.createStandardUser('standard-user');
		EmployeeBaseEntity delegateEmployee = testData.createEmployee('Delegate', null, delegeteUser.Id);

		System.runAs(delegeteUser) {
			App.NoPermissionException actEx;
			List<TimeResourcePermission.Permission> requiredPermissionList
					= new List<TimeResourcePermission.Permission>();
			requiredPermissionList.add(TimeResourcePermission.Permission.VIEW_TIME_TRACK_BY_DELEGATE);

			// Test1: 工数実績表示(代理)の権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new TimeResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 工数実績表示(代理)の権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isViewTimeTrackByDelegate = false;
			permissionRepository.saveEntity(testData.permission);
			try {
				new TimeResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

}
