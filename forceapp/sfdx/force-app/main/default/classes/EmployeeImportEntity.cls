/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 社員インポートのエンティティ
 */
public with sharing class EmployeeImportEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		ImportBatchId,
		Status,
		Error
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ
	 */
	public EmployeeImportEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/** インポートバッチID */
	public Id importBatchId {
		get;
		set {
			ImportBatchId = value;
			setChanged(Field.ImportBatchId);
		}
	}

	/** 改訂日 */
	public AppDate revisionDate {get; set;}

	/** Salesforceユーザ名 */
	public String userName {get; set;}

	/** 社員コード */
	public String code {get; set;}

	/** 会社コード */
	public String companyCode {get; set;}

	/** 社員氏名(L0) */
	public String displayName_L0 {get; set;}

	/** 社員氏名(L1) */
	public String displayName_L1 {get; set;}

	/** 社員氏名(L2) */
	public String displayName_L2 {get; set;}

	/** Expense Employee Group Code */
	public String expEmployeeGroupCode {get; set;}

	/** 名(L0) */
	public String firstName_L0 {get; set;}

	/** 名(L1) */
	public String firstName_L1 {get; set;}

	/** 名(L2) */
	public String firstName_L2 {get; set;}

	/** 姓(L0) */
	public String lastName_L0 {get; set;}

	/** 姓(L1) */
	public String lastName_L1 {get; set;}

	/** 姓(L2) */
	public String lastName_L2 {get; set;}

	/** ミドルネーム(L0) */
	public String middleName_L0 {get; set;}

	/** ミドルネーム(L1) */
	public String middleName_L1 {get; set;}

	/** ミドルネーム(L2) */
	public String middleName_L2 {get; set;}

	/** 雇入日 */
	public AppDate hiredDate {get; set;}

	/** 入社日 */
	public AppDate validFrom {get; set;}

	/** 退職日(resignationDateに変更されたため廃止) */
	public AppDate validTo {get; set;}

	/** 退職日 */
	public AppDate resignationDate {get; set;}

	/** Cost Center Code */
	public String costCenterBaseCode {get; set;}

	/** 部署コード */
	public String departmentBaseCode {get; set;}

	/** タイトル(L0) */
	public String title_L0 {get; set;}

	/** タイトル(L1) */
	public String title_L1 {get; set;}

	/** タイトル(L2) */
	public String title_L2 {get; set;}

	/** 兼務部署コード */
	public String additionalDepartmentBaseCode {get; set;}

	/** 兼務タイトル(L0) */
	public String additionalTitle_L0 {get; set;}

	/** 兼務タイトル(L1) */
	public String additionalTitle_L1 {get; set;}

	/** 兼務タイトル(L2) */
	public String additionalTitle_L2 {get; set;}

	/** 上長コード */
	public String managerBaseCode {get; set;}

	/** 承認者コード1~10 */
	public String approverBase01Code {get; set;}
	public String approverBase02Code {get; set;}
	public String approverBase03Code {get; set;}
	public String approverBase04Code {get; set;}
	public String approverBase05Code {get; set;}
	public String approverBase06Code {get; set;}
	public String approverBase07Code {get; set;}
	public String approverBase08Code {get; set;}
	public String approverBase09Code {get; set;}
	public String approverBase10Code {get; set;}
	/** ユーザーデータ01～10 */
	public String userData01 {get; set;}
	public String userData02 {get; set;}
	public String userData03 {get; set;}
	public String userData04 {get; set;}
	public String userData05 {get; set;}
	public String userData06 {get; set;}
	public String userData07 {get; set;}
	public String userData08 {get; set;}
	public String userData09 {get; set;}
	public String userData10 {get; set;}
	/** カレンダーコード */
	public String calendarCode {get; set;}

	/** 勤務体系コード */
	public String workingTypeBaseCode {get; set;}

	/** 36協定アラート設定コード */
	public String agreementAlertSettingCode {get; set;}

	/** 工数設定コード */
	public String timeSettingBaseCode {get; set;}

	/** 権限コード */
	public String permissionCode {get; set;}

	/** 履歴コメント */
	public String historyComment {get; set;}

	/** 処理ステータス */
	public ImportStatus status {
		get;
		set {
			status = value;
			setChanged(Field.Status);
		}
	}

	/** エラー情報 */
	public String error {
		get;
		set {
			error = value;
			setChanged(Field.Error);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}