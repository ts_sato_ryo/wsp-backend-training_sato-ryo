/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group プランナー
*
* @description PlannerServiceのテストクラス
*/
@isTest
private class PlannerServiceTest {

	private static PlannerService service = new PlannerService();

	private class TestInitializer extends TestUtil.DataBuilder {}

	/**
	 * 期間指定による取得条件のテスト
	 * 予定開始、予定終了、条件開始（検索条件）、条件終了（検索条件）の時系列の組み合わせによって、取得結果を検証する
	 *
	 * 1. 予定開始 < 予定終了 < 条件開始 < 条件終了 ⇒ 取得対象外
	 * 2. 予定開始 < 条件開始 < 予定終了 < 条件終了 ⇒ 取得対象
	 * 3. 条件開始 < 予定開始 < 予定終了 < 条件終了 ⇒ 取得対象
	 * 4. 条件開始 < 予定開始 < 条件終了 < 予定終了 ⇒ 取得対象
	 * 5. 予定開始 < 予定終了 < 条件開始 < 条件終了 ⇒ 取得対象外
	 * 6. 予定開始 < 条件開始 < 条件終了 < 予定終了 ⇒ 取得対象
	 */
	@isTest private static void getEventsTestPeriod() {
		TestInitializer db = new TestInitializer();
		db.initialize();

		insert new PlanEvent__c(
			Subject__c = '予定-1',
			StartDateTime__c = Datetime.valueOf('2016-04-30 09:00:00'),
			EndDateTime__c = Datetime.valueOf('2016-04-30 22:00:00'),
			OwnerId = db.user[0].Id,
			CreatedServiceBy__c = PlanEventEntity.ServiceType.teamspirit.name()
		);
		insert new PlanEvent__c(
			Subject__c = '予定-2',
			StartDateTime__c = Datetime.valueOf('2016-04-30 09:00:00'),
			EndDateTime__c = Datetime.valueOf('2016-05-01 09:00:00'),
			OwnerId = db.user[0].Id,
			CreatedServiceBy__c = PlanEventEntity.ServiceType.teamspirit.name()
		);
		insert new PlanEvent__c(
			Subject__c = '予定-3',
			StartDateTime__c = Datetime.valueOf('2016-05-01 09:00:00'),
			EndDateTime__c = Datetime.valueOf('2016-05-01 18:00:00'),
			OwnerId = db.user[0].Id,
			CreatedServiceBy__c = PlanEventEntity.ServiceType.teamspirit.name()
		);
		insert new PlanEvent__c(
			Subject__c = '予定-4',
			StartDateTime__c = Datetime.valueOf('2016-05-01 10:00:00'),
			EndDateTime__c = Datetime.valueOf('2016-05-02 09:00:00'),
			OwnerId = db.user[0].Id,
			CreatedServiceBy__c = PlanEventEntity.ServiceType.teamspirit.name()
		);
		insert new PlanEvent__c(
			Subject__c = '予定-5',
			StartDateTime__c = Datetime.valueOf('2016-05-02 09:00:00'),
			EndDateTime__c = Datetime.valueOf('2016-05-02 18:00:00'),
			OwnerId = db.user[0].Id,
			CreatedServiceBy__c = PlanEventEntity.ServiceType.teamspirit.name()
		);
		insert new PlanEvent__c(
			Subject__c = '予定-6',
			StartDateTime__c = Datetime.valueOf('2016-04-30 10:00:00'),
			EndDateTime__c = Datetime.valueOf('2016-05-02 10:00:00'),
			OwnerId = db.user[0].Id,
			CreatedServiceBy__c = PlanEventEntity.ServiceType.teamspirit.name()
		);

		Test.startTest();
		AppDate fromDate = AppDate.parse('2016-05-01');
		AppDate toDate = AppDate.parse('2016-05-01');
		List<PlanEventEntity> entities = service.searchEvents(db.user[0].id, fromDate, toDate);
		Test.stopTest();

		System.debug(Logginglevel.INFO, entities);
		System.assertEquals(4, entities.size());
		// 開始時刻の昇順で取得
		System.assertEquals('予定-2', entities[0].subject);
		System.assertEquals('予定-6', entities[1].subject);
		System.assertEquals('予定-3', entities[2].subject);
		System.assertEquals('予定-4', entities[3].subject);
	}
}