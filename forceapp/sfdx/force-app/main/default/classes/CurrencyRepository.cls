/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Repository class for Currency
 */
public with sharing class CurrencyRepository extends Repository.BaseRepository {

	/** Do not execute query with 'FOR UPDATE' */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * Target fields to fetch data
	 * If field is added, add same field to baseFieldSet
	 */
	private static final Set<Schema.SObjectField> GET_FIELD_NAME_LIST;
	static {
		GET_FIELD_NAME_LIST = new Set<Schema.SObjectField> {
			ComCurrency__c.Name,
			ComCurrency__c.Code__c,
			ComCurrency__c.IsoCurrencyCode__c,
			ComCurrency__c.Name_L0__c,
			ComCurrency__c.Name_L1__c,
			ComCurrency__c.Name_L2__c,
			ComCurrency__c.DecimalPlaces__c,
			ComCurrency__c.Symbol__c
			};
	}

	/**
   * Save a Currency record
   * This method is used for both new creation and update.
   * @param entity Entity to be saved
   * @return Saved result
   */
	public Repository.SaveResult saveEntity(CurrencyEntity entity) {
		return saveEntityList(new List<CurrencyEntity>{ entity });
	}

	/**
	 * Save Currency records
	 * This method is used for both new creation and update.
	 * @param entity Entity list to be saved
	 * @return Saved result
	 */
	private Repository.SaveResult saveEntityList(List<CurrencyEntity> entityList) {

		//Convert entity to sObject
		List<ComCurrency__c> sObjList = new List<ComCurrency__c>();
		for (CurrencyEntity entity : entityList) {
			sObjList.add(createObject(entity));
		}

		//Save to DB
		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);

		//Make and return result
		return resultFactory.createSaveResult(resultList);
	}

	/**
	 * Get entity by specified Currency Code
	 * @param code Currency Code of target entity
	 * @return Entity of specified code. If no data is found, return null.
	 */
	public CurrencyEntity getEntity(String code) {
		List<CurrencyEntity> entityList = getEntityList(new List<String>{code});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * Get entity list by specified Currency Codes
	 * @param ids Code list of target entity
	 * @return Entity list of specified Codes.  If no data is found, return null.
	 */
	public List<CurrencyEntity> getEntityList(List<String> codes) {
		CurrencyRepository.SearchFilter filter = new SearchFilter();
		filter.codes = codes == null ? null : new List<String>(codes);

		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/** Search Filters */
	public class SearchFilter {
		/** Id */
		public Set<Id> ids;
		/** Code (exact match) */
		public List<String> codes;
		/** Company ID */
		public Id companyId;

	}

	/**
	 * Search Currency
	 * @param filter Search Filter
	 * @return Search Result
	 */
	public List<CurrencyEntity> searchEntity(CurrencyRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * Search Currency
	 * @param filter Search Filter
	 * @param forUpdate When call this method for update, specify as true.
	 * @return Search Result
	 */
	public List<CurrencyEntity> searchEntity(CurrencyRepository.SearchFilter filter, Boolean forUpdate) {

		// WHERE statement
		List<String> whereList = new List<String>();
		// Filter by IDs
		Set<Id> pIds;
		if ((filter.ids != null) && (!filter.ids.isEmpty())) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// Filter by Currency Codes
		List<String> pCodes;
		if ((filter.codes != null) && (!filter.codes.isEmpty())) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ComCurrency__c.Code__c) + ' = :pCodes');
		}

		// Exclude the company base currency
		if (filter.companyId != null) {
			CompanyEntity company = new CompanyRepository().getEntity(filter.companyId);
			if (company.currencyInfo != null) {
				String pBaseCurrency = company.currencyInfo.code;
				whereList.add(getFieldName(ComCurrency__c.Code__c) + ' <> :pBaseCurrency');
			}
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY statement
		String orderByString = ' ORDER BY ' + getFieldName(ComCurrency__c.Code__c) + ' ASC';

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_NAME_LIST) {
			selectFieldList.add(getFieldName(fld));
		}

		// Build SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ComCurrency__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderByString;
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('CurrencyRepository: SOQL=' + soql);

		// Execute query
		List<ComCurrency__c> sObjs = (List<ComCurrency__c>)Database.query(soql);

		// Create entity from SObject
		List<CurrencyEntity> entities = new List<CurrencyEntity>();
		for (ComCurrency__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	/**
	 * Create object from entity to save
	 * @param entityList Target entity list to save
	 * @return Object list to be saved
	 */
	@TestVisible
	private List<SObject> createObjectList(List<CurrencyEntity> entityList) {

		List<ComCurrency__c> objectList = new List<ComCurrency__c>();
		for (CurrencyEntity entity : entityList) {
			objectList.add(createObject((CurrencyEntity)entity));
		}

		return objectList;
	}

	/**
	 * Create object from entity to save
	 * @param entity Original entity
	 * @return Object to be saved
	 */
	@TestVisible
	private ComCurrency__c createObject(CurrencyEntity entity) {

		ComCurrency__c sObj = new ComCurrency__c();

		sObj.Id = entity.id;

		if (entity.isChanged(CurrencyEntity.Field.NAME)) {
			sObj.Name = entity.name;
		}
		if (entity.isChanged(CurrencyEntity.Field.CODE)) {
			sObj.Code__c = entity.code;
		}
		if (entity.isChanged(CurrencyEntity.Field.ISO_CURRENCY_CODE)) {
			sObj.IsoCurrencyCode__c= entity.isoCurrencyCode.value;
		}
		if (entity.isChanged(CurrencyEntity.Field.NAME_L0)) {
			sObj.Name_L0__c = entity.nameL0;
		}
		if (entity.isChanged(CurrencyEntity.Field.NAME_L1)) {
			sObj.Name_L1__c = entity.nameL1;
		}
		if (entity.isChanged(CurrencyEntity.Field.NAME_L2)) {
			sObj.Name_L2__c = entity.nameL2;
		}
		if (entity.isChanged(CurrencyEntity.Field.DECIMAL_PLACES)) {
			sObj.DecimalPlaces__c = entity.decimalPlaces;
		}
		if (entity.isChanged(CurrencyEntity.Field.SYMBOL)) {
			sObj.Symbol__c = entity.symbol;
		}

		return sObj;
	}

	/**
	 * Create entity from SObject
	 * @param obj Original SObject
	 * @return Created entity
	 */
	private static CurrencyEntity createEntity(ComCurrency__c sObj) {

		CurrencyEntity entity = new CurrencyEntity();

		entity.setId(sObj.Id);
		entity.name = sObj.Name;
		entity.code = sObj.Code__c;
		entity.isoCurrencyCode = ComIsoCurrencyCode.valueOf(sObj.IsoCurrencyCode__c);
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.decimalPlaces = (Integer)sObj.DecimalPlaces__c;
		entity.symbol = sObj.Symbol__c;

		entity.resetChanged();

		return entity;
	}
}