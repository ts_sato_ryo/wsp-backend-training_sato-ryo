/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署履歴エンティティを表すクラスのテスト
 */
@isTest
private with sharing class DepartmentHistoryEntityTest {

	/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
	private static ComOrgSetting__c orgObj;
	/** 会社オブジェクト */
	private static ComCompany__c companyObj;
	/** 権限オブジェクト */
	private static ComPermission__c permissionObj;
	/** 親部署 */
	private static ComDeptBase__c parentDeptObj;
	/** 上長 */
	private static ComEmpBase__c managerObj;

	/** テスト用の履歴エンティティを作成する */
	private static DepartmentHistoryEntity createHistoryEntity() {
		if (orgObj == null) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		}
		if (companyObj == null) {
			companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
		}
		if (permissionObj == null) {
			permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		}
		if (parentDeptObj == null) {
			parentDeptObj = ComTestDataUtility.createDepartmentWithHistory('親部署', companyObj.Id);

		}
		if (managerObj == null) {
			managerObj = ComTestDataUtility.createEmployeeWithHistory('Test社員', companyObj.Id, null, Userinfo.getUserId(), permissionObj.Id);
		}

		DepartmentHistoryEntity history = new DepartmentHistoryEntity();
		history.baseId = UserInfo.getUserId();
		history.historyComment = 'テスト履歴コメント';
		history.validFrom = AppDate.newInstance(2017, 1, 11);
		history.validTo = AppDate.newInstance(2017, 2, 11);
		history.isRemoved = true;
		history.uniqKey = history.validFrom.format() + history.validTo.format();
		history.name = 'テスト履歴';
		history.nameL0 = 'テスト履歴_L0';
		history.nameL1 = 'テスト履歴_L1';
		history.nameL2 = 'テスト履歴_L2';
		history.managerId = managerObj.Id;
		history.parentBaseId = parentDeptObj.Id;
		history.remarks = 'テスト備考';

		return history;
	}

	/**
	 * エンティティを複製できることを確認する
	 */
	@isTest static void copyTest() {
		DepartmentHistoryEntity history = createHistoryEntity();

		DepartmentHistoryEntity copyHistory = history.copy();

		System.assertEquals(history.nameL0, copyHistory.nameL0);
		System.assertEquals(history.nameL1, copyHistory.nameL1);
		System.assertEquals(history.nameL2, copyHistory.nameL2);
		System.assertEquals(history.managerId, copyHistory.managerId);
		System.assertEquals(history.parentBaseId, copyHistory.parentBaseId);
		System.assertEquals(history.remarks, copyHistory.remarks);

		System.assertEquals(history.name, copyHistory.name);
		System.assertEquals(history.baseId, copyHistory.baseId);
		System.assertEquals(history.historyComment, copyHistory.historyComment);
		System.assertEquals(history.validFrom, copyHistory.validFrom);
		System.assertEquals(history.validTo, copyHistory.validTo);
		System.assertEquals(history.isRemoved, copyHistory.isRemoved);
		System.assertEquals(history.uniqKey, copyHistory.uniqKey);
	}
}
