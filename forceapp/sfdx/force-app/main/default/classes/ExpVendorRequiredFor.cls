/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * Vendor Required For pick list
 */
public with sharing class ExpVendorRequiredFor extends ValueObjectType {

	/** Expense Report Only */
	public static final ExpVendorRequiredFor EXPENSE_REPORT = new ExpVendorRequiredFor('ExpenseReport');
	/** Expense Report and Request */
	public static final ExpVendorRequiredFor EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ExpVendorRequiredFor('ExpenseRequestAndExpenseReport');

	/** Entries */
	public static final List<ExpVendorRequiredFor> TYPE_LIST;
	static {
		TYPE_LIST = new List<ExpVendorRequiredFor> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}

	/**
	 * Constructor
	 * @param value
	 */
	private ExpVendorRequiredFor(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
	 * @param The key value to get the instance
	 * @return The instance having the value specified
	 */
	public static ExpVendorRequiredFor valueOf(String value) {
		ExpVendorRequiredFor retType = null;
		if (String.isNotBlank(value)) {
			for (ExpVendorRequiredFor type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {

		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ExpVendorRequiredFor) {
			// 値が同じであればtrue
			if (this.value == ((ExpVendorRequiredFor)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}
}