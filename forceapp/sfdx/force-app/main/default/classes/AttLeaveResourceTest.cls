/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description AttLeaveResourceのテストクラス
 */
@isTest
private class AttLeaveResourceTest {

	/**
	 * 社員ベース・履歴レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		Id userId = Userinfo.getUserId();
		ComCompany__c company = ComTestDataUtility.createTestCompany();

		Test.startTest();

		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();
		param.name_L0 = 'フレックスタイム_L0';
		param.name_L1 = 'フレックスタイム_L1';
		param.name_L2 = 'フレックスタイム_L2';
		param.code = '001';
		param.companyId = company.Id;
		param.leaveType = 'Annual';
		param.daysManaged = true;
		param.leaveRanges = new List<String>{ 'Day', 'AM', 'PM' };
		param.requireReason = true;
		param.countType = 'Attendance';
		param.summaryItemNo = '2';
		param.order = '1';
		param.validDateFrom = Date.today();
		param.validDateTo = Date.today() + 365;

		AttLeaveResource.CreateApi api = new AttLeaveResource.CreateApi();
		AttLeaveResource.SaveResult res = (AttLeaveResource.SaveResult)api.execute(param);

		Test.stopTest();

		// レコードが1件作成されること
		List<AttLeave__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CompanyId__c,
				Type__c,
				DaysManaged__c,
				Range__c,
				RequireReason__c,
				CountType__c,
				Order__c,
				SummaryItemNo__c,
				ValidFrom__c,
				ValidTo__c
			FROM AttLeave__c];

		System.assertEquals(1, newRecords.size());
		AttLeave__c base = newRecords[0];

		// レコード値検証
		System.assertEquals(base.Id, res.Id);
		System.assertEquals(param.name_L0 , base.Name);
		System.assertEquals(param.name_L0 , base.Name_L0__c);
		System.assertEquals(param.name_L1 , base.Name_L1__c);
		System.assertEquals(param.name_L2 , base.Name_L2__c);
		System.assertEquals(param.code, base.Code__c);
		System.assertEquals(param.companyId, base.CompanyId__c);
		System.assertEquals(param.leaveType, base.Type__c);
		System.assertEquals(param.daysManaged, base.DaysManaged__c);
		Set<String> paramRangeSet = new Set<String>(param.leaveRanges);
		Set<String> resRangeSet = new Set<String>(base.Range__c.split(';'));
		System.assertEquals(paramRangeSet.size(), resRangeSet.size());
		System.assert(resRangeSet.containsAll(paramRangeSet));
		System.assert(param.requireReason, base.RequireReason__c);
		System.assertEquals(Integer.valueOf(param.order), base.Order__c);
		System.assertEquals(Integer.valueOf(param.summaryItemNo), base.SummaryItemNo__c);
		System.assertEquals(param.validDateFrom, base.ValidFrom__c);
		System.assertEquals(param.validDateTo, base.ValidTo__c);
	}

	/**
	 * 社員ベース・履歴レコードを1件作成する(デフォルト値が適用される)
	 */
	@isTest
	static void createPositiveTestDefaultValue() {
		final Date defaultValidFrom = Date.today();
		final Date defaultValidTo = Date.newInstance(2101, 1, 1);

		Id userId = Userinfo.getUserId();
		ComCompany__c company = ComTestDataUtility.createTestCompany();

		Test.startTest();

		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();
		param.name_L0 = 'フレックスタイム_L0';
		param.name_L1 = 'フレックスタイム_L1';
		param.name_L2 = 'フレックスタイム_L2';
		param.code = '001';
		param.companyId = company.Id;
		param.leaveType = 'Annual';
		param.daysManaged = true;
		param.leaveRanges = new List<String>{ 'Day', 'AM', 'PM' };
		param.requireReason = true;
		param.countType = 'Attendance';
		param.summaryItemNo = '2';
		param.order = '1';
		// 有効開始日、失効日を設定しない
		param.validDateFrom = null;
		param.validDateTo = null;


		AttLeaveResource.CreateApi api = new AttLeaveResource.CreateApi();
		AttLeaveResource.SaveResult res = (AttLeaveResource.SaveResult)api.execute(param);

		Test.stopTest();

		// レコードが1件作成されること
		List<AttLeave__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				ValidFrom__c,
				ValidTo__c
			FROM AttLeave__c];

		System.assertEquals(1, newRecords.size());
		AttLeave__c base = newRecords[0];

		// レコード値検証
		System.assertEquals(base.Id, res.Id);
		System.assertEquals(defaultValidFrom, base.ValidFrom__c);
		System.assertEquals(defaultValidTo, base.ValidTo__c);
	}

	/**
	 * 休暇レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Test.startTest();

		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();
		param.companyId = company.Id;
		param.code = 'code';
		param.name_L0 = '';
		AttLeaveResource.CreateApi api = new AttLeaveResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('NOT_SET_VALUE', ex.getErrorCode());
	}

	/**
	 * 休暇レコードを1件作成する(異常系:整数値型のパラメータが正しくない)
	 */
	@isTest
	static void creativeNegativeTestInvalidInteger() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();

		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();
		param.name_L0 = 'フレックスタイム_L0';
		param.code = '001';
		param.companyId = company.Id;
		param.leaveType = 'Annual';
		param.leaveRanges = new List<String>{ 'Day', 'AM', 'PM' };
		param.requireReason = true;
		param.countType = 'Attendance';
		param.order = '1';


		AttLeaveResource.CreateApi api = new AttLeaveResource.CreateApi();
		App.TypeException ex;

		// 表示順が整数値ではない
		param.order = '1.23';
		try {
			Object res = api.execute(param);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.TypeException e) {
			ex = e;
		}
		System.assertEquals(App.ERR_CODE_INVALID_TYPE, ex.getErrorCode());

		// 集計Noが正しくない
		param.summaryItemNo = '123456789012345';
		try {
			Object res = api.execute(param);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.TypeException e) {
			ex = e;
		}
		System.assertEquals(App.ERR_CODE_INVALID_TYPE, ex.getErrorCode());
	}

	/**
	 * 休暇レコードを1件作成する(異常系:コード重複)
	 */
	@isTest
	static void creativeNegativeTestDuplicateCode() {
		AttTestData testData = new AttTestData();

		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();
		param.name_L0 = 'フレックスタイム_L0';
		param.code = testData.leaveList[0].code;
		param.companyId = testData.company.Id;
		param.leaveType = 'Annual';
		param.leaveRanges = new List<String>{ 'Day', 'AM', 'PM' };
		param.requireReason = true;
		param.countType = 'Attendance';
		param.order = '1';

		AttLeaveResource.CreateApi api = new AttLeaveResource.CreateApi();
		App.ParameterException ex;

		// 表示順が整数値ではない
		try {
			Object res = api.execute(param);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			ex = e;
		}
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, ex.getMessage());

	}

	/**
	 * 休暇レコードを1件作成する(異常系:休暇の管理権限なし)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 休暇の管理権限を無効に設定する
		testData.permission.isManageAttLeave = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();
		param.name_L0 = 'フレックスタイム_L0';
		param.code = testData.leaveList[0].code;
		param.companyId = testData.company.Id;
		param.leaveType = 'Annual';
		param.leaveRanges = new List<String>{ 'Day', 'AM', 'PM' };
		param.requireReason = true;
		param.countType = 'Attendance';
		param.order = '1';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttLeaveResource.CreateApi api = new AttLeaveResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 年次有休休暇複数件登録
	 */
	@isTest
	static void creativeMoreAnnualLeaveTest() {
		AttTestData testData = new AttTestData();
		testData.deleteLeave();

		testData.createLeave('a', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();
		AttLeaveResource.CreateApi api = new AttLeaveResource.CreateApi();
		DmlException ex;

		param.name_L0 = '年次有休2';
		param.code = '002';
		param.companyId = testData.company.Id;
		param.leaveType = 'Annual';
		param.daysManaged = true;
		param.leaveRanges = new List<String>{ 'Day', 'AM', 'PM' };
		param.requireReason = true;
		param.validDateFrom = Date.today();
		param.validDateTo = Date.today() + 365;
		Test.startTest();
		Boolean hasError = false;
		try {
			Object res = api.execute(param);
		} catch (App.IllegalStateException e) {
			hasError = true;
			System.assertEquals(App.ERR_CODE_ANNUAL_LEAVE_EXISTED, e.getErrorCode());
		}
		finally {}
		System.assertEquals(true, hasError);
		Test.stopTest();
	}
	/**
	 * 休暇レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		AttLeaveEntity ent = new AttTestData().createLeave(
			'Test01',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM }
		);


		Test.startTest();

		// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
		// 全パラメータの検証は createPositiveTest() で行っている
		Map<String, Object> param = new Map<String, Object> {
			'id' => ent.id,
			'code' => 'Test02',
			'name_L0' => 'Test02_L0',
			'leaveType' => 'Paid',
			'leaveRanges' => new List<String>{'Day'}
			// 'companyId' => null  // 会社IDは無視される -> 無視されないのでコメントアウト
		};

		AttLeaveResource.UpdateApi api = new AttLeaveResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// 社員レコードが更新されていること
		List<AttLeave__c> leaveList = [SELECT Code__c FROM AttLeave__c WHERE Id =:(String)param.get('id')];
		System.assertEquals(1, leaveList.size());
		System.assertEquals((String)param.get('code'), leaveList[0].Code__c);

	}

	/**
	 * 社員レコードを1件更新する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		Test.startTest();

		AttLeaveResource.AttLeave param = new AttLeaveResource.AttLeave();

		App.ParameterException ex;
		try {
			Object res = (new AttLeaveResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 休暇レコードを1件更新する(異常系:休暇の管理権限なし)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 休暇の管理権限を無効に設定する
		testData.permission.isManageAttLeave = false;
		new PermissionRepository().saveEntity(testData.permission);
		AttLeaveEntity ent = testData.createLeave(
			'Test01',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM }
		);

		// リクエストパラメータ
		Map<String, Object> param = new Map<String, Object> {
			'id' => ent.id,
			'code' => 'Test02',
			'name_L0' => 'Test02_L0',
			'leaveType' => 'Paid',
			'leaveRanges' => new List<String>{'Day'}
		};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttLeaveResource.UpdateApi api = new AttLeaveResource.UpdateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 休暇レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		AttLeaveEntity ent = new AttTestData().createLeave(
			'Test01',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM }
		);

		Test.startTest();

		AttLeaveResource.DeleteOption param = new AttLeaveResource.DeleteOption();
		param.id = ent.id;

		Object res = (new EmployeeResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 社員レコードが削除されること
		System.assertEquals(0, [SELECT COUNT() FROM AttLeave__c WHERE Id =:ent.id]);
	}

	/**
	 * 休暇レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		AttLeaveResource.DeleteOption param = new AttLeaveResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new AttLeaveResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 休暇レコードを1件削除する(異常系:休暇の管理権限なし)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		// テストデータ作成
		AttTestData testData = new AttTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('テスト標準ユーザ');
		// 休暇の管理権限を無効に設定する
		testData.permission.isManageAttLeave = false;
		new PermissionRepository().saveEntity(testData.permission);
		AttLeaveEntity ent = testData.createLeave(
			'Test01',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM }
		);

		// リクエストパラメータ
		AttLeaveResource.DeleteOption param = new AttLeaveResource.DeleteOption();
		param.id = ent.id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttLeaveResource.DeleteApi api = new AttLeaveResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 休暇レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		// TODO API実装後コメントアウトを外す
		AttLeaveEntity ent = new AttTestData().createLeave(
			'Test01',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM }
		);

		Test.startTest();

		AttLeaveResource.SearchCondition param = new AttLeaveResource.SearchCondition();
		Map<String, Object> paramMap = new Map<String, Object>{
			'id' => ent.Id,
			'companyId' => ent.companyId
		};

		AttLeaveResource.SearchApi api = new AttLeaveResource.SearchApi();
		AttLeaveResource.SearchResult res = (AttLeaveResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(ent.Id, res.records[0].id);
		// 項目値はsearchAllPositiveTest()で検証するため、ここでは省略*/
	}

	/**
	 * 社員レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// TODO API実装後コメントアウトを外す
		
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		AttLeaveEntity ent = new AttTestData().createLeave(
			'Test01',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM }
		);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>{
			'id' => ent.id
		};

		AttLeaveResource.SearchApi api = new AttLeaveResource.SearchApi();
		AttLeaveResource.SearchResult res = (AttLeaveResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 多言語対応項目の検証
		AttLeave__c base = [SELECT Name_L1__c FROM AttLeave__c WHERE Id =: ent.Id];
		System.assertEquals(base.Name_L1__c, res.records[0].name);
	}

	/**
	 * 社員レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// TODO API実装後コメントアウトを外す
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		AttLeaveEntity ent = new AttTestData().createLeave(
			'Test01',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM }
		);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>{
			'id' => ent.id
		};

		AttLeaveResource.SearchApi api = new AttLeaveResource.SearchApi();
		AttLeaveResource.SearchResult res = (AttLeaveResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 多言語対応項目の検証
		AttLeave__c base = [SELECT Name_L2__c FROM AttLeave__c WHERE Id =: ent.Id];
		System.assertEquals(base.Name_L2__c, res.records[0].name);
	}

	/**
	 * 社員レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {
		// TODO API実装後コメントアウトを外す
		List<AttLeaveEntity> entities = new AttTestData().createLeaves(
			'Test',
			'フレックス',
			AttLeaveType.PAID,
			true,
			new List<AttLeaveRange>{ AttLeaveRange.RANGE_AM, AttLeaveRange.RANGE_PM },
			3
		);

		Test.startTest();

		AttLeaveResource.SearchCondition param = new AttLeaveResource.SearchCondition();
		Map<String, Object> paramMap = new Map<String, Object>();

		AttLeaveResource.SearchApi api = new AttLeaveResource.SearchApi();
		AttLeaveResource.SearchResult res = (AttLeaveResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 作成したテストデータをコード順で再取得する
		List<AttLeave__c> leavelist = [SELECT
				Id,
				Code__c,
				CompanyId__c,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Order__c,
				SummaryItemNo__c,
				Type__c,
				DaysManaged__c,
				Range__c,
				RequireReason__c,
				ValidFrom__c,
				ValidTo__c
			FROM AttLeave__c
			ORDER BY Code__c];

		// レコードが取得できること
		System.assertEquals(leaveList.size(), res.records.size());

		for (Integer i = 0; i < leaveList.size(); i++) {
			AttLeaveResource.AttLeave rec = res.records[i];
			AttLeave__c sObj = leaveList[i];

			System.assertEquals(sObj.Id, rec.Id);
			System.assertEquals(sObj.Name_L0__c, rec.name);
			System.assertEquals(sObj.Name_L0__c, rec.name_L0);
			System.assertEquals(sObj.Name_L1__c, rec.name_L1);
			System.assertEquals(sObj.Name_L2__c, rec.name_L2);
			System.assertEquals(sObj.Code__c, rec.code);
			System.assertEquals(sObj.CompanyId__c, rec.companyId);
			System.assertEquals(sObj.Type__c, rec.leaveType);
			System.assertEquals(sObj.DaysManaged__c, rec.daysManaged);
			Set<String> srcRangeSet = new Set<String>(sObj.Range__c.split(';'));
			Set<String> resRangeSet = new Set<String>(rec.leaveRanges);
			System.assertEquals(srcRangeSet, resRangeSet);
			System.assertEquals(sObj.RequireReason__c, rec.requireReason);
			System.assertEquals(String.valueOf(sObj.Order__c), rec.order);
			System.assertEquals(String.valueOf(sObj.SummaryItemNo__c), rec.summaryItemNo);
			System.assertEquals(sObj.ValidFrom__c, rec.validDateFrom);
			System.assertEquals(sObj.ValidTo__c, rec.validDateTo);
		}

	}

}
