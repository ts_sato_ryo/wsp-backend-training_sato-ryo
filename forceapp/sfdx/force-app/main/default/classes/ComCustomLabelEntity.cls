/**
 * @group 共通
 *
 * @description カスタムラベルエンティティ
 * TODO 暫定対応として、ComMessage.clsに合わせてクラス名に"Com"をつけている
 */
public with sharing class ComCustomLabelEntity extends ComCustomLabelGeneratedEntity {

	/**
	 * @description コンストラクタ
	 */
	public ComCustomLabelEntity() {
		super();
	}

	/**
	 * @description コンストラクタ
	 */
	public ComCustomLabelEntity(ComCustomLabel__mdt sobj) {
		super(sobj);
	}

	/**
	 * @description 指定された言語のラベルを取得する、ユーザがラベルを変更している場合は変更後のラベルを返却する
	 * @param lang 取得対象言語
	 * @return ラベル
	 */
	public String getLabelByLang(AppLanguage lang) {
		if (lang == AppLanguage.JA) {
			// 日本語の場合
			return String.isBlank(this.jaCustom) ? jaDefault : jaCustom;
		}

		return String.isBlank(this.enUsCustom) ? enUsDefault : enUsCustom;
	}

	/**
	 * @description 指定された言語のデフォルトラベルを取得する
	 * @param lang 取得対象言語
	 * @return ラベル
	 */
	public String getDefaultLabelByLang(AppLanguage lang) {
		if (lang == AppLanguage.JA) {
			// 日本語の場合
			return jaDefault;
		}

		return enUsDefault;
	}
}