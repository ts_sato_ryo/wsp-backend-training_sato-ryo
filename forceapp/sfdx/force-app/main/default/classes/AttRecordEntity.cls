/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 勤怠
  *
 * 勤怠明細のエンティティ
 */
public with sharing class AttRecordEntity extends AttRecordGeneratedEntity {

	/**
	 * コンストラクタ
	 */
	public AttRecordEntity() {
		super(new AttRecord__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttRecordEntity(AttRecord__c sobj) {
		super(sobj);
	}

	/**
	 * @description エンティティの複製を作成する
	 */
	public AttRecordEntity copy() {
		AttRecord__c cloneObj = this.sobj.clone(false, false, false, false);
		return new AttRecordEntity(cloneObj);
	}

	// 勤怠詳細定義
	public class AttWorkTimeDetail {
		public List<AttTimePeriod> CILIPeriods {get; private set;}
		public List<AttTimePeriod> CILOPeriods {get; private set;}
		public List<AttTimePeriod> COLIPeriods {get; private set;}
		public List<AttTimePeriod> COLOPeriods {get; private set;}
		public List<AttTimePeriod> CILHPeriods {get; private set;}
		public List<AttTimePeriod> COLHPeriods {get; private set;}

		public AttWorkTimeDetail () {
			CILIPeriods = new List<AttTimePeriod>();
			CILOPeriods = new List<AttTimePeriod>();
			COLIPeriods = new List<AttTimePeriod>();
			COLOPeriods = new List<AttTimePeriod>();
			CILHPeriods = new List<AttTimePeriod>();
			COLHPeriods = new List<AttTimePeriod>();
		}
		public void appendCILIPeriod(AttTimePeriod timePeriod) {
			CILIPeriods.add(timePeriod);
		}
		public void appendCILOPeriod(AttTimePeriod timePeriod) {
			CILOPeriods.add(timePeriod);
		}
		public void appendCOLIPeriod(AttTimePeriod timePeriod) {
			COLIPeriods.add(timePeriod);
		}
		public void appendCOLOPeriod(AttTimePeriod timePeriod) {
			COLOPeriods.add(timePeriod);
		}
		public void appendCILHPeriod(AttTimePeriod timePeriod) {
			CILHPeriods.add(timePeriod);
		}
		public void appendCOLHPeriod(AttTimePeriod timePeriod) {
			COLHPeriods.add(timePeriod);
		}
		public void appendCILIPeriods(List<AttTimePeriod> timePeriods) {
			CILIPeriods.addAll(timePeriods);
		}
		public void appendCILOPeriods(List<AttTimePeriod> timePeriods) {
			CILOPeriods.addAll(timePeriods);
		}
		public void appendCOLIPeriods(List<AttTimePeriod> timePeriods) {
			COLIPeriods.addAll(timePeriods);
		}
		public void appendCOLOPeriods(List<AttTimePeriod> timePeriods) {
			COLOPeriods.addAll(timePeriods);
		}
		public void appendCILHPeriods(List<AttTimePeriod> timePeriods) {
			CILHPeriods.addAll(timePeriods);
		}
		public void appendCOLHPeriods(List<AttTimePeriod> timePeriods) {
			COLHPeriods.addAll(timePeriods);
		}
		// フレックスタイム制のグラフに調整
		public void formatToFlex(AttDailyTime contractedWorkHours) {
			AttCalcRangesDto allRanges = AttCalcRangesDto.RS();
			for (AttTimePeriod timePeriod : this.CILIPeriods) {
				allRanges.insertAndMerge(convertToRange(timePeriod));
			}
			for (AttTimePeriod timePeriod : this.CILOPeriods) {
				allRanges.insertAndMerge(convertToRange(timePeriod));
			}
			for (AttTimePeriod timePeriod : this.COLIPeriods) {
				allRanges.insertAndMerge(convertToRange(timePeriod));
			}
			for (AttTimePeriod timePeriod : this.COLOPeriods) {
				allRanges.insertAndMerge(convertToRange(timePeriod));
			}
			// 通常勤務と残業2種別分ける
			Integer contractedTime = AppConverter.intValue(contractedWorkHours, 0);
			Integer cutTime = allRanges.timeFromStart(contractedTime);
			AttCalcRangesDto ciliRanges = AttCalcRangesDto.RS();
			AttCalcRangesDto coloRanges = AttCalcRangesDto.RS();
			ciliRanges = allRanges.cutBefore(cutTime); // 所定内は切り出し
			coloRanges = allRanges.cutAfter(cutTime); // 残りは全て所定外法定外にする
			this.CILIPeriods.clear();
			for (Integer i = 0 ; i <= ciliRanges.size() - 1; i++) {
				this.CILIPeriods.add(convertToTimePeriod(ciliRanges.get(i)));
			}
			this.CILOPeriods.clear();
			this.COLIPeriods.clear();
			this.COLOPeriods.clear();
			for (Integer i = 0 ; i <= colORanges.size() - 1; i++) {
				this.COLOPeriods.add(convertToTimePeriod(coloRanges.get(i)));
			}
		}
		private AttCalcRangeDto convertToRange (AttTimePeriod timePeriod) {
			return AttCalcRangesDto.R(timePeriod.startTime.getIntValue(), timePeriod.endTime.getIntValue());
		}
		private AttTimePeriod convertToTimePeriod (AttCalcRangeDto range) {
			return new AttTimePeriod (AttTime.valueOf(range.getStartTime()), AttTime.valueOf(range.getEndTime()));
		}
	}

	/** 勤怠サマリー再計算フラグ */
	public Boolean isSummaryDirty {
		get {
			return sobj.SummaryId__r.Dirty__c != null ? sobj.SummaryId__r.Dirty__c : false;
		}
		private set;
	}

	/** 勤怠サマリーロックフラグ */
	public Boolean isSummaryLocked {
		get {
			return sobj.SummaryId__r.Locked__c != null ? sobj.SummaryId__r.Locked__c : false;
		}
		private set;
	}
	@TestVisible
	private void isSummaryLocked(Boolean value) {
		if (sobj.SummaryId__r == null) {
			sobj.SummaryId__r = new AttSummary__c();
		}
		sobj.SummaryId__r.Locked__c = value;
	}

	/** 勤務体系ベースID */
	public Id workingTypeBaseId {
		get {
			if (workingTypeBaseId == null) {
				workingTypeBaseId = sobj.SummaryId__r.WorkingTypeHistoryId__r.BaseId__c;
			}
			return workingTypeBaseId;
		}
		private set;
	}

	/** 休暇1休暇名*/
	public AppMultiString reqLeave1NameL {
		get {
			if (reqLeave1NameL != null) {
				return reqLeave1NameL;
			}
			return new AppMultiString(sobj.ReqLeave1Id__r.Name_L0__c, sobj.ReqLeave1Id__r.Name_L1__c, sobj.ReqLeave1Id__r.Name_L0__c);
		}
		private set;
	}

	/** 休暇1休暇合計時間*/
	public AttDailyTime reqLeave1LeaveTotalTime {
		get {
			return AttDailyTime.valueOf(sobj.ReqLeave1RequestId__r.LeaveTotalTime__c);
		}
		private set;
	}
	@TestVisible
	private void setReqLeave1LeaveTotalTime(AttDailyTime value) {
		if (sobj.ReqLeave1RequestId__r == null) {
			sobj.ReqLeave1RequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqLeave1RequestId__r.LeaveTotalTime__c = AppConverter.intValue(value);
	}

	/** 休暇1申請の親申請ID(振替休日の場合に設定される) */
	public Id reqLeave1ParentRequestId {
		get {
			return sobj.ReqLeave1RequestId__r.ParentRequestId__c;
		}
		private set;
	}

	/** 確定済み休暇1ステータス*/
	public AppRequestStatus reqLeave1Status {
		get {
			return AppRequestStatus.valueOf(sobj.ReqLeave1RequestId__r.Status__c);
		}
		private set;
	}
	/**
	 * FIXME privateメソッドにしたい。本メソッドはテスト用で使用する、またはメソッド自体を廃止したい
	 * AttValidator.DailyLeaveRequestSubmitValidatorで必要なためpublicにしている
	 */
	public void setReqLeave1Status(AppRequestStatus status) {
		if (sobj.ReqLeave1RequestId__r == null) {
			sobj.ReqLeave1RequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqLeave1RequestId__r.Status__c = status == null ? null : status.value;
	}

	/** 申請中休暇1ステータス*/
	public AppRequestStatus reqRequestingLeave1Status {
		get {
			return AppRequestStatus.valueOf(sobj.ReqRequestingLeave1RequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingLeave1Status(AppRequestStatus status) {
		if (sobj.ReqRequestingLeave1RequestId__r == null) {
			sobj.ReqRequestingLeave1RequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingLeave1RequestId__r.Status__c = status == null ? null : status.value;
	}

	/** 申請中休暇1範囲*/
	public AttLeaveRange reqRequestingLeave1Range {
		get {
			if (reqRequestingLeave1Range != null) {
				return reqRequestingLeave1Range;
			}
			return AttLeaveRange.valueOf(sobj.ReqRequestingLeave1RequestId__r.AttLeaveRange__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 申請中休暇1種別(参照のみ) */
	public AttLeaveType reqRequestingLeave1Type {
		get {
			if (reqRequestingLeave1Type != null) {
				return reqRequestingLeave1Type;
			}
			return AttLeaveType.valueOf(sobj.ReqRequestingLeave1RequestId__r.AttLeaveType__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 申請中休暇1始業時間*/
	public AttTime reqRequestingLeave1StartTime {
		get {
			if (reqRequestingLeave1StartTime != null) {
				return reqRequestingLeave1StartTime;
			}
			return AttTime.valueOf(sobj.ReqRequestingLeave1RequestId__r.StartTime__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 申請中休暇1終業時間*/
	public AttTime reqRequestingLeave1EndTime {
		get {
			if (reqRequestingLeave1EndTime != null) {
				return reqRequestingLeave1EndTime;
			}
			return AttTime.valueOf(sobj.ReqRequestingLeave1RequestId__r.EndTime__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 休暇2休暇名*/
	public AppMultiString reqLeave2NameL {
		get {
			return new AppMultiString(sobj.ReqLeave2Id__r.Name_L0__c, sobj.ReqLeave2Id__r.Name_L1__c, sobj.ReqLeave2Id__r.Name_L0__c);
		}
		private set;
	}

	/** 休暇2休暇合計時間*/
	public AttDailyTime reqLeave2LeaveTotalTime {
		get {
			return AttDailyTime.valueOf(sobj.ReqLeave2RequestId__r.LeaveTotalTime__c);
		}
		private set;
	}
	@TestVisible
	private void setReqLeave2LeaveTotalTime(AttDailyTime value) {
		if (sobj.ReqLeave2RequestId__r == null) {
			sobj.ReqLeave2RequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqLeave2RequestId__r.LeaveTotalTime__c = AppConverter.intValue(value);
	}

	/** 休暇2申請の親申請ID(振替休日の場合に設定される) */
	public Id reqLeave2ParentRequestId {
		get {
			return sobj.ReqLeave2RequestId__r.ParentRequestId__c;
		}
		private set;
	}

	/** 確定済み休暇2ステータス*/
	public AppRequestStatus reqLeave2Status {
		get {
			return AppRequestStatus.valueOf(sobj.ReqLeave2RequestId__r.Status__c);
		}
		private set;
	}
	/**
	 * FIXME privateメソッドにしたい。本メソッドはテスト用で使用する、またはメソッド自体を廃止したい
	 * AttValidator.DailyLeaveRequestSubmitValidatorで必要なためpublicにしている
	 */
	public void setReqLeave2Status(AppRequestStatus status) {
		if (sobj.ReqLeave2RequestId__r == null) {
			sobj.ReqLeave2RequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqLeave2RequestId__r.Status__c = status == null ? null : status.value;
	}

	/** 申請中休暇2ステータス*/
	public AppRequestStatus reqRequestingLeave2Status {
		get {
			return AppRequestStatus.valueOf(sobj.ReqRequestingLeave2RequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingLeave2Status(AppRequestStatus status) {
		if (sobj.ReqLeave2RequestId__r == null) {
			sobj.ReqLeave2RequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqLeave2RequestId__r.Status__c = status == null ? null : status.value;
	}

	/** 申請中休暇2範囲*/
	public AttLeaveRange reqRequestingLeave2Range {
		get {
			if (reqRequestingLeave2Range != null) {
				return reqRequestingLeave2Range;
			}
			return AttLeaveRange.valueOf(sobj.ReqRequestingLeave2RequestId__r.AttLeaveRange__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 申請中休暇2種別(参照のみ) */
	public AttLeaveType reqRequestingLeave2Type {
		get {
			if (reqRequestingLeave2Type != null) {
				return reqRequestingLeave2Type;
			}
			return AttLeaveType.valueOf(sobj.ReqRequestingLeave2RequestId__r.AttLeaveType__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 申請中休暇2始業時間*/
	public AttTime reqRequestingLeave2StartTime {
		get {
			if (reqRequestingLeave2StartTime != null) {
				return reqRequestingLeave2StartTime;
			}
			return AttTime.valueOf(sobj.ReqRequestingLeave2RequestId__r.StartTime__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 申請中休暇2終業時間*/
	public AttTime reqRequestingLeave2EndTime {
		get {
			if (reqRequestingLeave2EndTime != null) {
				return reqRequestingLeave2EndTime;
			}
			return AttTime.valueOf(sobj.ReqRequestingLeave2RequestId__r.EndTime__c);
		}
		// TODO AttValidatorでsetしているため、privateに出来ない。AttValidatorの処理を見直す
		set;
	}

	/** 休日出勤申請ステータス*/
	public AppRequestStatus reqHolidayWorkStatus {
		get {
			return AppRequestStatus.valueOf(sobj.ReqHolidayWorkRequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqHolidayWorkStatus(AppRequestStatus status) {
		if (sobj.ReqHolidayWorkRequestId__r == null) {
			sobj.ReqHolidayWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqHolidayWorkRequestId__r.Status__c = status == null ? null : status.value;
	}
	/** 休日出勤申請申請中ステータス*/
	public AppRequestStatus reqRequestingHolidayWorkStatus {
		get {
			return AppRequestStatus.valueOf(sobj.ReqRequestingHolidayWorkRequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingHolidayWorkStatus(AppRequestStatus status) {
		if (sobj.ReqRequestingHolidayWorkRequestId__r == null) {
			sobj.ReqRequestingHolidayWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingHolidayWorkRequestId__r.Status__c = status == null ? null : status.value;
	}


	/** 早朝勤務申請申請中ステータス*/
	public AppRequestStatus reqRequestingEarlyStartWorkStatus {
		get {
			return AppRequestStatus.valueOf(sobj.ReqRequestingEarlyStartWorkRequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingEarlyStartWorkStatus(AppRequestStatus status) {
		if (sobj.ReqRequestingEarlyStartWorkRequestId__r == null) {
			sobj.ReqRequestingEarlyStartWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingEarlyStartWorkRequestId__r.Status__c = status == null ? null : status.value;
	}
	/** 早朝勤務申請申請中開始時刻*/
	public AttTime reqRequestingEarlyStartWorkStartTime {
		get {
			return AttTime.valueOf(sobj.ReqRequestingEarlyStartWorkRequestId__r.StartTime__c);
		}
		private set;
	}
	@TestVisible
	private void setRequestingEarlyStartWorkStartTime(AttTime startTime) {
		if (sobj.ReqRequestingEarlyStartWorkRequestId__r == null) {
			sobj.ReqRequestingEarlyStartWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingEarlyStartWorkRequestId__r.EndTime__c = AttTime.intValue(startTime);
	}
	/** 早朝勤務申請申請中終了時刻*/
	public AttTime reqRequestingEarlyStartWorkEndTime {
		get {
			return AttTime.valueOf(sobj.ReqRequestingEarlyStartWorkRequestId__r.EndTime__c);
		}
		private set;
	}
	@TestVisible
	private void setRequestingEarlyStartWorkEndTime(AttTime endTime) {
		if (sobj.ReqRequestingEarlyStartWorkRequestId__r == null) {
			sobj.ReqRequestingEarlyStartWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingEarlyStartWorkRequestId__r.EndTime__c = AttTime.intValue(endTime);
	}

	/** 残業申請申請中ステータス*/
	public AppRequestStatus reqRequestingOvertimeWorkStatus {
		get {
			return AppRequestStatus.valueOf(sobj.ReqRequestingOvertimeWorkRequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingOvertimeWorkStatus(AppRequestStatus status) {
		if (sobj.ReqRequestingOvertimeWorkRequestId__r == null) {
			sobj.ReqRequestingOvertimeWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingOvertimeWorkRequestId__r.Status__c = status == null ? null : status.value;
	}
	/** 残業申請申請中開始時刻*/
	public AttTime reqRequestingOvertimeWorkStartTime {
		get {
			return AttTime.valueOf(sobj.ReqRequestingOvertimeWorkRequestId__r.StartTime__c);
		}
		private set;
	}
	@TestVisible
	private void setRequestingOvertimeWorkStartTime(AttTime startTime) {
		if (sobj.ReqRequestingOvertimeWorkRequestId__r == null) {
			sobj.ReqRequestingOvertimeWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingOvertimeWorkRequestId__r.StartTime__c = AttTime.intValue(startTime);
	}
	/** 残業申請申請中終了時刻*/
	public AttTime reqRequestingOvertimeWorkEndTime {
		get {
			return AttTime.valueOf(sobj.ReqRequestingOvertimeWorkRequestId__r.EndTime__c);
		}
		private set;
	}
	@TestVisible
	private void setRequestingOvertimeWorkEndTime(AttTime endTime) {
		if (sobj.ReqRequestingOvertimeWorkRequestId__r == null) {
			sobj.ReqRequestingOvertimeWorkRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingOvertimeWorkRequestId__r.EndTime__c = AttTime.intValue(endTime);
	}

	/** 欠勤申請ステータス*/
	public AppRequestStatus reqRequestingAbsenceStatus {
		get {
			return AppRequestStatus.valueOf(sobj.ReqRequestingAbsenceRequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingAbsenceStatus(AppRequestStatus status) {
		if (sobj.ReqRequestingAbsenceRequestId__r == null) {
			sobj.ReqRequestingAbsenceRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingAbsenceRequestId__r.Status__c = status == null ? null : status.value;
	}
	/** 直行直帰申請ステータス*/
	public AppRequestStatus inpRequestingDirectStatus {
		get {
			return AppRequestStatus.valueOf(sobj.InpRequestingDirectRequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setInpRequestingDirectStatus(AppRequestStatus status) {
		if (sobj.InpRequestingDirectRequestId__r == null) {
			sobj.InpRequestingDirectRequestId__r = new AttDailyRequest__c();
		}
		sobj.InpRequestingDirectRequestId__r.Status__c = status == null ? null : status.value;
	}
	/** 申請中直行直帰出勤時刻*/
	public AttTime inpRequestingDirectStartTime {
		get {
			return AttTime.valueOf(sobj.InpRequestingDirectRequestId__r.StartTime__c);
		}
		private set;
	}
	@TestVisible
	private void setInpRequestingDirectStartTime(AttTime startTime) {
		if (sobj.InpRequestingDirectRequestId__r == null) {
			sobj.InpRequestingDirectRequestId__r = new AttDailyRequest__c();
		}
		sobj.InpRequestingDirectRequestId__r.StartTime__c = startTime == null ? null : startTime.getIntValue();
	}
	/** 申請中直行直帰退勤時刻*/
	public AttTime inpRequestingDirectEndTime {
		get {
			return AttTime.valueOf(sobj.InpRequestingDirectRequestId__r.EndTime__c);
		}
		private set;
	}
	@TestVisible
	private void setInpRequestingDirectEndTime(AttTime endTime) {
		if (sobj.InpRequestingDirectRequestId__r == null) {
			sobj.InpRequestingDirectRequestId__r = new AttDailyRequest__c();
		}
		sobj.InpRequestingDirectRequestId__r.EndTime__c = endTime == null ? null : endTime.getIntValue();
	}
	/** 勤務時間変更申請(申請中)勤務パターンID */
	public Id reqRequestingPatternPatternId {
		get {
			return sobj.ReqRequestingPatternRequestId__r.AttPatternId__c;
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingPatternPatternId(Id patternId) {
		if (sobj.ReqRequestingPatternRequestId__r == null) {
			sobj.ReqRequestingPatternRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingPatternRequestId__r.AttPatternId__c = patternId;
	}
	/** 勤務時間変更申請(申請中)ステータス*/
	public AppRequestStatus reqRequestingPatternStatus {
		get {
			return AppRequestStatus.valueOf(sobj.ReqRequestingPatternRequestId__r.Status__c);
		}
		private set;
	}
	@TestVisible
	private void setReqRequestingPatternStatus(AppRequestStatus status) {
		if (sobj.ReqRequestingPatternRequestId__r == null) {
			sobj.ReqRequestingPatternRequestId__r = new AttDailyRequest__c();
		}
		sobj.ReqRequestingPatternRequestId__r.Status__c = status == null ? null : status.value;
	}
	/** 勤務時間変更申請(申請中)開始時刻 */
	public AttTime reqRequestingPatternStartTime {
		get {
			return AttTime.valueOf(sobj.ReqRequestingPatternRequestId__r.AttPatternId__r.StartTime__c);
			// return AttTime.valueOf(sobj.ReqRequestingPatternRequestId__r.StartTime__c);
		}
		private set;
	}
	/**
	 * FIXME! privateにしたいがAttValidator.DailyLeaveRequestSubmitValidatorで使用したいため
	 *       止むを得ずpublicメソッドにしている
	 *
	 * 勤務時間変更申請(申請中)開始時刻設定
	 */
	public void setReqRequestingPatternStartTime(AttTime startTime) {
		if (sobj.ReqRequestingPatternRequestId__r == null) {
			sobj.ReqRequestingPatternRequestId__r = new AttDailyRequest__c();
		}
		if (sobj.ReqRequestingPatternRequestId__r.AttPatternId__r == null) {
			sobj.ReqRequestingPatternRequestId__r.AttPatternId__r = new AttPattern__c();
		}
		sobj.ReqRequestingPatternRequestId__r.AttPatternId__r.StartTime__c = AttTime.intValue(startTime);
	}
	/** 勤務時間変更申請(申請中)終了時刻*/
	public AttTime reqRequestingPatternEndTime {
		get {
			return AttTime.valueOf(sobj.ReqRequestingPatternRequestId__r.AttPatternId__r.EndTime__c);
		}
		private set;
	}
	/**
	 * FIXME! privateにしたいがAttValidator.DailyLeaveRequestSubmitValidatorで使用したいため
	 *       止むを得ずpublicメソッドにしている
	 *
	 * 勤務時間変更申請(申請中)終了時刻設定
	 */
	public void setReqRequestingPatternEndTime(AttTime endTime) {
		if (sobj.ReqRequestingPatternRequestId__r == null) {
			sobj.ReqRequestingPatternRequestId__r = new AttDailyRequest__c();
		}
		if (sobj.ReqRequestingPatternRequestId__r.AttPatternId__r == null) {
			sobj.ReqRequestingPatternRequestId__r.AttPatternId__r = new AttPattern__c();
		}
		sobj.ReqRequestingPatternRequestId__r.AttPatternId__r.EndTime__c = AttTime.intValue(endTime);
	}

	/** カレンダー明細名(L0-2) */
	public AppMultiString calRecordNameL {
		get {
			return new AppMultiString(sobj.CalRecordId__r.Name_L0__c, sobj.CalRecordId__r.Name_L1__c, sobj.CalRecordId__r.Name_L2__c);
		}
		private set;
	}

	/** 休職休業名(L0-2) */
	public AppMultiString leaveOfAbsenceNameL {
		get {
			return new AppMultiString(
				sobj.LeaveOfAbsenceId__r.Name_L0__c,
				sobj.LeaveOfAbsenceId__r.Name_L1__c,
				sobj.LeaveOfAbsenceId__r.Name_L2__c);
		}
		private set;
	}

	// TODO サブクラス（勤務パターンと同じ）
	/* 将来的にオブジェクトに追加される項目
	 * 勤怠計算では参照しているため、オブジェクトに追加されたらサブクラスから削除する
	 */

	/** 短時間勤務設定名(L0-2) */
	public AppMultiString calShortTimeNameL {
		get {
			return new AppMultiString(
				sobj.CalShortTimeWorkSettingHistoryId__r.Name_L0__c,
				sobj.CalShortTimeWorkSettingHistoryId__r.Name_L1__c,
				sobj.CalShortTimeWorkSettingHistoryId__r.Name_L2__c);
		}
		private set;
	}


	/** 実効勤務パターン名(L0-2) */
	public AppMultiString outPatternNameL {
		get {
			return new AppMultiString(
				sobj.OutPatternId__r.Name_L0__c,
				sobj.OutPatternId__r.Name_L1__c,
				sobj.OutPatternId__r.Name_L2__c);
		}
		private set;
	}

	/** 実の所定勤務時間 */
	public AttDailyTime outRealContractedWorkHours {
		get {
			return AttDailyTime.valueOf(
					AppConverter.intValue(this.outContractedWorkHours, 0)
					- AppConverter.intValue(super.outVirtualWorkTime, 0)
					- AppConverter.intValue(this.outAbsenceTime, 0));
		}
		private set;
	}

	// TODO 勤怠計算で使用しているが repositoryで設定していない
	/** 無休取得時間 */
	public AttDailyTime outAbsenceTime {
		get;
		set {
			outAbsenceTime = value;
		}
	}

	// TODO 勤怠計算で使用しているが repositoryで設定していない
	/** 実効所定休憩時間 */
	public AttDailyTime outAdditionalRestTime {
		get;
		set {
			outAdditionalRestTime = value;
		}
	}

	/** 勤怠詳細項目定義(グラフ用) */
	public static final Schema.SObjectField FIELD_OUT_WORK_TIME_DETAILS = AttRecord__c.OutWorkTimeDetails__c.getDescribe().getSObjectField();
	public AttWorkTimeDetail outWorkTimeDetail {
		get {
			if (outWorkTimeDetail == null) {
				outWorkTimeDetail = createWorkTimeDetailEntity(sobj.OutWorkTimeDetails__c);
			}
			return outWorkTimeDetail;
		}
		set {
			outWorkTimeDetail = value;
		}
	}

	// TODO テストコートのみ利用されているため。
	/** 申請日タイプ */
	public AttDayType reqDayType {
		get;
		set;
	}

	/**
	 * 勤怠記録名を作成する(社員コード+社員名+対象日)
	 * @param employeeCode 社員コード
	 * @param employeeName 社員名
	 * @param targetDate 日付
	 * @return 勤怠記録名
	 */
	public static String createRecordName(
			final String employeeCode, final String employeeName, final AppDate targetDate) {
		final Integer empLength = 72; // 社員情報の最大文字数

		// パラメータチェック
		if (String.isBlank(employeeCode)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeCode is blank');
		}
		if (String.isBlank(employeeName)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeName is blank');
		}
		if (targetDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('targetDate is blank');
		}

		return (employeeCode + employeeName).left(empLength) + targetDate.formatYYYYMMDD();
	}

	/**
	 * 日タイプを取得する
	 * PreferedLegalHoliday（所定休日だが勤務してなければ法定休日になる日）は
	 * 勤務状況によりHoliday, またはLegalHolidayのどちらかに判定して返却する。
	 * @return 日タイプ。ただし、設定されていない場合はnull
	 */
	public AttDayType getDayTypeNoPref() {

		AttDayType dayType;
		if (this.outDayType == AttDayType.PREFERRED_LEGAL_HOLIDAY) {
			// 法定休日フラグisHolLegalHolidayがtrueなら法定休日、そうでなければ所定休日
			if (this.isHolLegalHoliday == true) {
				// 法定休日
				dayType = AttDayType.LEGAL_HOLIDAY;
			} else {
				// 所定休日
				dayType = AttDayType.HOLIDAY;
			}
		} else {
			// 日タイプをそのまま返却する
			dayType = outDayType;
		}
		return dayType;
	}

	/**
	 * @description 休職休業日の場合はtrue, そうでない場合はfalseを返却する
	 */
	public Boolean isLeaveOfAbsence() {
		return this.leaveOfAbsenceId != null ? true : false;
	}

	/**
	 * @description 申請中の各種勤怠申請IDを取得する
	 */
	public List<Id> getRequestingRequestIdList() {
		List<Id> requestIds = new List<Id>();
		if (this.reqRequestingLeave1RequestId != null) {
			requestIds.add(this.reqRequestingLeave1RequestId);
		}
		if (this.reqRequestingLeave2RequestId != null) {
			requestIds.add(this.reqRequestingLeave2RequestId);
		}
		if (this.reqRequestingHolidayWorkRequestId != null) {
			requestIds.add(this.reqRequestingHolidayWorkRequestId);
		}
		if (this.reqRequestingEarlyStartWorkRequestId != null) {
			requestIds.add(this.reqRequestingEarlyStartWorkRequestId);
		}
		if (this.reqRequestingOvertimeWorkRequestId != null) {
			requestIds.add(this.reqRequestingOvertimeWorkRequestId);
		}
		if (this.reqRequestingAbsenceRequestId != null) {
			requestIds.add(this.reqRequestingAbsenceRequestId);
		}
		if (this.inpRequestingDirectRequestId != null) {
			requestIds.add(this.inpRequestingDirectRequestId);
		}
		if (this.reqRequestingPatternRequestId != null) {
			requestIds.add(this.reqRequestingPatternRequestId);
		}
		return requestIds;
	}

	/**
	 * @description 承認済みの各種勤怠申請IDを取得する
	 */
	public List<Id> getApprovedRequestIdList() {
		List<Id> approvedRequestIds = new List<Id>();
		if (this.reqLeave1RequestId != null) {
			approvedRequestIds.add(this.reqLeave1RequestId);
		}
		if (this.reqLeave2RequestId != null) {
			approvedRequestIds.add(this.reqLeave2RequestId);
		}
		if (this.reqHolidayWorkRequestId != null) {
			approvedRequestIds.add(this.reqHolidayWorkRequestId);
		}
		if (this.reqEarlyStartWorkRequestId != null) {
			approvedRequestIds.add(this.reqEarlyStartWorkRequestId);
		}
		if (this.reqOvertimeWorkRequestId != null) {
			approvedRequestIds.add(this.reqOvertimeWorkRequestId);
		}
		if (this.reqAbsenceRequestId != null) {
			approvedRequestIds.add(this.reqAbsenceRequestId);
		}
		if (this.inpDirectRequestId != null) {
			approvedRequestIds.add(this.inpDirectRequestId);
		}
		if (this.reqPatternRequestId != null) {
			approvedRequestIds.add(this.reqPatternRequestId);
		}
		return approvedRequestIds;
	}

	@testVisible private static final String WORK_TIME_KEY_CILI = 'CILI';
	@testVisible private static final String WORK_TIME_KEY_CILO = 'CILO';
	@testVisible private static final String WORK_TIME_KEY_COLI = 'COLI';
	@testVisible private static final String WORK_TIME_KEY_COLO = 'COLO';
	@testVisible private static final String WORK_TIME_KEY_CILH = 'CILH';
	@testVisible private static final String WORK_TIME_KEY_COLH = 'COLH';
	@testVisible private static final String WORK_TIME_PERIOD_START = 'start';
	@testVisible private static final String WORK_TIME_PERIOD_END = 'end';

	// 勤務時間内訳JSONから変換
	@testVisible
	private AttRecordEntity.AttWorkTimeDetail createWorkTimeDetailEntity(String workTimeDetails) {
		AttRecordEntity.AttWorkTimeDetail detailData = new AttRecordEntity.AttWorkTimeDetail();
		if (String.isBlank(workTimeDetails)) {
			return detailData;
		}
		Map<String, Object> detailMap = (Map<String, Object>)JSON.deserializeUntyped(workTimeDetails);
		for (String workTimeKey : detailMap.keySet()) {
			for (Object periodObj : (List<Object>)detailMap.get(workTimeKey)) {
				Map<String, Object> periodMap = (Map<String, Object>)periodObj;
				AttTimePeriod timePeriod = new AttTimePeriod(
						AttTime.valueOf(periodMap.get('start')),
						AttTime.valueOf(periodMap.get('end')));
				if (workTimeKey == WORK_TIME_KEY_CILI) {
					detailData.appendCILIPeriod(timePeriod);
				}
				else if (workTimeKey == WORK_TIME_KEY_CILO) {
					detailData.appendCILOPeriod(timePeriod);
				}
				else if (workTimeKey == WORK_TIME_KEY_COLI) {
					detailData.appendCOLIPeriod(timePeriod);
				}
				else if (workTimeKey == WORK_TIME_KEY_COLO) {
					detailData.appendCOLOPeriod(timePeriod);
				}
				else if (workTimeKey == WORK_TIME_KEY_CILH) {
					detailData.appendCILHPeriod(timePeriod);
				}
				else if (workTimeKey == WORK_TIME_KEY_COLH) {
					detailData.appendCOLHPeriod(timePeriod);
				}
			}
		}
		return detailData;
	}

	// 勤務時間内訳エンティティからJSONへ変換
	@testVisible
	private String createWorkTimeDetailJson(AttRecordEntity.AttWorkTimeDetail workTimeDetailData) {
		Map<String, List<AttTimePeriod>> keyDetailMap = new Map<String, List<AttTimePeriod>>{
			WORK_TIME_KEY_CILI => workTimeDetailData.CILIPeriods,
			WORK_TIME_KEY_CILO => workTimeDetailData.CILOPeriods,
			WORK_TIME_KEY_COLI => workTimeDetailData.COLIPeriods,
			WORK_TIME_KEY_COLO => workTimeDetailData.COLOPeriods,
			WORK_TIME_KEY_CILH => workTimeDetailData.CILHPeriods,
			WORK_TIME_KEY_COLH => workTimeDetailData.COLHPeriods
		};
		Map<String, List<Map<String, Integer>>> detailMap = new Map<String, List<Map<String, Integer>>>();
		for (String key : keyDetailMap.keySet()) {
			detailMap.put(key, new List<Map<String, Integer>>());
			for (AttTimePeriod period : keyDetailMap.get(key)) {
				detailMap.get(key).add(new Map<String, Integer>{
					WORK_TIME_PERIOD_START => AppConverter.intValue(period.startTime),
					WORK_TIME_PERIOD_END => AppConverter.intValue(period.endTime)
				});
			}
		}

		return JSON.serialize(detailMap);
	}

	/**
	 * 更新対象のSObjectを生成する。
	 * OutWorkTimeDetails__cは親クラスで定義していないので、更新時にJson文字列へ変換する。
	 */
	public override AttRecord__c createSObject() {
		AttRecord__c sobj = super.createSObject();
		sobj.OutWorkTimeDetails__c = createWorkTimeDetailJson(outWorkTimeDetail);
		return sobj;
	}
}