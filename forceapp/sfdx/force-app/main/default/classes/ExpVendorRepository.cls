/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Repository class for Vendor
 */
public with sharing class ExpVendorRepository extends Repository.BaseRepository {

	/** Max record number to fetch */
	public static final Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** Pattern for detecting characters to unescape */
	private static final Pattern SPECIAL_CHARACTERS_PATTERN = Pattern.compile('[%\\_\\\\]');
	/** Do not execute query with 'FOR UPDATE' */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * Target fields to fetch data
	 * If field is added, add same field to baseFieldSet
	 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		// Target fields definition
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				ExpVendor__c.Id,
				ExpVendor__c.Code__c,
				ExpVendor__c.UniqKey__c,
				ExpVendor__c.Name,
				ExpVendor__c.Name_L0__c,
				ExpVendor__c.Name_L1__c,
				ExpVendor__c.Name_L2__c,
				ExpVendor__c.CompanyId__c,
				ExpVendor__c.PaymentDueDateUsage__c,
				ExpVendor__c.PaymentTerm__c,
				ExpVendor__c.PaymentTermCode__c,
				ExpVendor__c.IsWithholdingTax__c,
				ExpVendor__c.BankAccountType__c,
				ExpVendor__c.BankAccountNumber__c,
				ExpVendor__c.BankCode__c,
				ExpVendor__c.BankName__c,
				ExpVendor__c.BranchCode__c,
				ExpVendor__c.BranchName__c,
				ExpVendor__c.BranchAddress__c,
				ExpVendor__c.SwiftCode__c,
				ExpVendor__c.CorrespondentBankAddress__c,
				ExpVendor__c.CorrespondentBankName__c,
				ExpVendor__c.CorrespondentBranchName__c,
				ExpVendor__c.CorrespondentSwiftCode__c,
				ExpVendor__c.PayeeName__c,
				ExpVendor__c.Country__c,
				ExpVendor__c.CurrencyCode__c,
				ExpVendor__c.ZipCode__c,
				ExpVendor__c.Address__c,
				ExpVendor__c.Active__c
				};
	}

	/**
	 * Get entity by specified ID
	 * @param id ID of target entity
	 * @return Entity of specified ID. If no data is found, return null.
	 */
	public ExpVendorEntity getEntity(Id id) {
		List<ExpVendorEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * Get entity list by specified IDs
	 * @param ids ID list of target entity
	 * @return Entity list of specified ID.  If no data is found, return null.
	 */
	public List<ExpVendorEntity> getEntityList(List<Id> ids) {
		ExpVendorRepository.SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter, NOT_FOR_UPDATE, SEARCH_RECORDS_NUMBER_MAX);
	}

	/**
	 * Save a Accounting Period record
	 * This method is used for both new creation and update.
	 * @param entity Entity to be saved
	 * @return Saved result
	 */
	public Repository.SaveResult saveEntity(ExpVendorEntity entity) {
		return saveEntityList(new List<ExpVendorEntity>{ entity });
	}

	/**
   * Save Accounting Period records
   * This method is used for both new creation and update.
   * @param entity Entity list to be saved
   * @return Saved result
   */
	public Repository.SaveResult saveEntityList(List<ExpVendorEntity> entityList) {

		// Convert entity to sObject
		List<ExpVendor__c> sObjList = createObjectList(entityList);

		 //Save to DB
		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);

		// Make and return result
		return resultFactory.createSaveResult(resultList);
	}

	/** Search Filters */
	public class SearchFilter {
		/** ID */
		public Set<Id> ids;
		/** Code (exact match) */
		public Set<String> codes;
		/** Company ID */
		public Set<Id> companyIds;
		/** Active */
		public Integer active;
		public String nameOrCodeQuery;
		/** Active const */
		public final Integer ONLY_ACTIVE = 0;
		public final Integer ONLY_INACTIVE = 1;
		public final Integer ALL = 2;
	}

	/**
	 * Search Accounting Period
	 * @param filter Search Filter
	 * @return Search Result
	 */
	public List<ExpVendorEntity> searchEntity(ExpVendorRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE, SEARCH_RECORDS_NUMBER_MAX);
	}

	/**
	 * Search Accounting Period
	 * @param filter Search Filter
	 * @param resultLimit controls the number of returned records if specified
	 * @return Search Result
	 */
	public List<ExpVendorEntity> searchEntity(ExpVendorRepository.SearchFilter filter, Integer resultLimit) {
		return searchEntity(filter, NOT_FOR_UPDATE, resultLimit);
	}

	/**
	 * Search Accounting Period
	 * @param filter Search Filter
	 * @param forUpdate When call this method for update, specify as true.
	 * @param resultLimit controls the number of returned records if specified
	 * @return Search Result
	 */
	public List<ExpVendorEntity> searchEntity(ExpVendorRepository.SearchFilter filter, Boolean forUpdate, Integer resultLimit) {

		// WHERE statement
		List<String> whereList = new List<String>();
		// Filter by IDs
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// Filter by Company Codes
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ExpVendor__c.Code__c) + ' = :pCodes');
		}
		// Filter by Company IDs
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ExpVendor__c.CompanyId__c) + ' IN :pCompanyIds');
		}

		// Filter by Active flag
		if (filter.active == filter.ONLY_ACTIVE) {
			whereList.add(getFieldName(ExpVendor__c.Active__c) + ' = TRUE');
		} else if (filter.active == filter.ONLY_INACTIVE) {
			whereList.add(getFieldName(ExpVendor__c.Active__c) + ' = FALSE');
		}

		// Search by Name and Code
		String pNameOrCodeQuery;
		if (String.isNotBlank(filter.nameOrCodeQuery)) {
			pNameOrCodeQuery = '%'+ escapeSpecialCharacters(filter.nameOrCodeQuery) + '%';
			whereList.add(
					getFieldName(ExpVendor__c.Name_L0__c) + ' LIKE :pNameOrCodeQuery OR '
					+ getFieldName(ExpVendor__c.Name_L1__c) + ' LIKE :pNameOrCodeQuery OR '
					+ getFieldName(ExpVendor__c.Name_L2__c) + ' LIKE :pNameOrCodeQuery OR '
					+ getFieldName(ExpVendor__c.Code__c) + ' LIKE :pNameOrCodeQuery');
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY statement
		String orderByString = ' ORDER BY ' + getFieldName(ExpVendor__c.Code__c);

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		// Regular fields
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFieldList.add(getFieldName(fld));
		}

		// Build SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpVendor__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderByString
				+ ' LIMIT :resultLimit';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		//System.debug('ExpVendorRepository: SOQL=' + soql);

		// Execute query
		List<ExpVendor__c> sObjs = (List<ExpVendor__c>)Database.query(soql);

		// Create entity from SObject
		List<ExpVendorEntity> entities = new List<ExpVendorEntity>();
		for (ExpVendor__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	/**
	 * Create object from entity to save
	 * @param entityList Target entity list to save
	 * @return Object list to be saved
	 */
	private List<SObject> createObjectList(List<ExpVendorEntity> entityList) {

		List<ExpVendor__c> objectList = new List<ExpVendor__c>();
		for (ExpVendorEntity entity : entityList) {
			objectList.add(createObject(entity));
		}

		return objectList;
	}

	/**
	 * Create object from entity to save
	 * @param entity Original entity
	 * @return Object to be saved
	 */
	private ExpVendor__c createObject(ExpVendorEntity entity) {

		ExpVendor__c sObj = new ExpVendor__c();

		sObj.Id = entity.id;

		if (entity.isChanged(ExpVendorEntity.Field.ACTIVE)) {
			sObj.Active__c = entity.active;
		}
		if (entity.isChanged(ExpVendorEntity.Field.ADDRESS)) {
			sObj.Address__c = entity.address;
		}
		if (entity.isChanged(ExpVendorEntity.Field.BANK_ACCOUNT_TYPE)) {
			sObj.BankAccountType__c = entity.bankAccountType != null ? entity.bankAccountType.value : null;
		}
		if (entity.isChanged(ExpVendorEntity.Field.BANK_ACCOUNT_NUMBER)) {
			sObj.BankAccountNumber__c = entity.bankAccountNumber;
		}
		if (entity.isChanged(ExpVendorEntity.Field.BANK_CODE)) {
			sObj.BankCode__c = entity.bankCode;
		}
		if (entity.isChanged(ExpVendorEntity.Field.BANK_NAME)) {
			sObj.BankName__c = entity.bankName;
		}
		if (entity.isChanged(ExpVendorEntity.Field.BRANCH_ADDRESS)) {
			sObj.BranchAddress__c = entity.branchAddress;
		}
		if (entity.isChanged(ExpVendorEntity.Field.BRANCH_CODE)) {
			sObj.BranchCode__c = entity.branchCode;
		}
		if (entity.isChanged(ExpVendorEntity.Field.BRANCH_NAME)) {
			sObj.BranchName__c = entity.branchName;
		}
		if (entity.isChanged(ExpVendorEntity.Field.CODE)) {
			sObj.Code__c = entity.code;
		}
		if (entity.isChanged(ExpVendorEntity.Field.CORRESPONDENT_BANK_ADDRESS)) {
			sObj.CorrespondentBankAddress__c = entity.correspondentBankAddress;
		}
		if (entity.isChanged(ExpVendorEntity.Field.CORRESPONDENT_BANK_NAME)) {
			sObj.CorrespondentBankName__c = entity.correspondentBankName;
		}
		if (entity.isChanged(ExpVendorEntity.Field.CORRESPONDENT_BRANCH_NAME)) {
			sObj.CorrespondentBranchName__c = entity.correspondentBranchName;
		}
		if (entity.isChanged(ExpVendorEntity.Field.CORRESPONDENT_SWIFT_CODE)) {
			sObj.CorrespondentSwiftCode__c = entity.correspondentSwiftCode;
		}
		if (entity.isChanged(ExpVendorEntity.Field.COMPANY_ID)) {
			sObj.CompanyId__c = entity.companyId;
		}
		if (entity.isChanged(ExpVendorEntity.Field.COUNTRY)) {
			sObj.Country__c = entity.country;
		}
		if (entity.isChanged(ExpVendorEntity.Field.CURRENCY_CODE)) {
			sObj.CurrencyCode__c = entity.currencyCode != null ? entity.currencyCode.value : null;
		}
		if (entity.isChanged(ExpVendorEntity.Field.IS_WITHHOLDING_TAX)) {
			sObj.IsWithholdingTax__c = entity.isWithholdingTax;
		}
		if (entity.isChanged(ExpVendorEntity.Field.NAME_L0)) {
			sObj.Name_L0__c = entity.nameL0;
			sObj.Name = entity.nameL0;
		}
		if (entity.isChanged(ExpVendorEntity.Field.NAME_L1)) {
			sObj.Name_L1__c = entity.nameL1;
		}
		if (entity.isChanged(ExpVendorEntity.Field.NAME_L2)) {
			sObj.Name_L2__c = entity.nameL2;
		}
		if (entity.isChanged(ExpVendorEntity.Field.PAYEE_NAME)) {
			sObj.PayeeName__c = entity.payeeName;
		}
		if (entity.isChanged(ExpVendorEntity.Field.PAYMENT_DUE_DATE_USAGE)) {
			sObj.PaymentDueDateUsage__c = entity.paymentDueDateUsage.value;
		}
		if (entity.isChanged(ExpVendorEntity.Field.PAYMENT_TERM)) {
			sObj.PaymentTerm__c = entity.paymentTerm;
		}
		if (entity.isChanged(ExpVendorEntity.Field.PAYMENT_TERM_CODE)) {
			sObj.PaymentTermCode__c = entity.paymentTermCode;
		}
		if (entity.isChanged(ExpVendorEntity.Field.SWIFT_CODE)) {
			sObj.SwiftCode__c = entity.swiftCode;
		}
		if (entity.isChanged(ExpVendorEntity.Field.ZIP_CODE)) {
			sObj.ZipCode__c = entity.zipCode;
		}
		if (entity.isChanged(ExpVendorEntity.Field.UNIQUE_KEY)) {
			sObj.UniqKey__c = entity.uniqKey;
		}

		return sObj;
	}

	/**
	 * Create entity from SObject
	 * @param obj Original SObject
	 * @return Created entity
	 */
	private ExpVendorEntity createEntity(ExpVendor__c sObj) {

		ExpVendorEntity entity = new ExpVendorEntity();

		// Set common fields of 'Valid Period' type object.
		entity.setId(sObj.Id);

		entity.active = sObj.Active__c;
		entity.address = sObj.Address__c;
		entity.bankAccountType = ExpBankAccountType.valueOf(sObj.BankAccountType__c);
		entity.bankAccountNumber = sObj.BankAccountNumber__c;
		entity.bankCode = sObj.BankCode__c;
		entity.bankName = sObj.BankName__c;
		entity.branchAddress = sObj.BranchAddress__c;
		entity.branchCode = sObj.BranchCode__c;
		entity.branchName = sObj.BranchName__c;
		entity.code = sObj.Code__c;
		entity.companyId = sObj.CompanyId__c;
		entity.country = sObj.Country__c;
		entity.correspondentBankAddress = sObj.CorrespondentBankAddress__c;
		entity.correspondentBankName = sObj.CorrespondentBankName__c;
		entity.correspondentBranchName = sObj.CorrespondentBranchName__c;
		entity.correspondentSwiftCode = sObj.CorrespondentSwiftCode__c;
		entity.currencyCode = ComIsoCurrencyCode.valueOf(sObj.CurrencyCode__c);
		entity.isWithholdingTax = sObj.IsWithholdingTax__c;
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.payeeName = sObj.PayeeName__c;

		// For Existing Environment, there could be cases where the payment Due Date is not SET - hence return OPTIONAL
		ExpVendorPaymentDueDateUsage paymentDueDateUsage = ExpVendorPaymentDueDateUsage.valueOf(sObj.PaymentDueDateUsage__c);
		entity.paymentDueDateUsage = paymentDueDateUsage == null ? ExpVendorPaymentDueDateUsage.OPTIONAL: paymentDueDateUsage;

		entity.paymentTerm = sObj.PaymentTerm__c;
		entity.paymentTermCode = sObj.PaymentTermCode__c;
		entity.swiftCode = sObj.SwiftCode__c;
		entity.uniqKey = sObj.UniqKey__c;
		entity.zipCode = sObj.ZipCode__c;

		entity.resetChanged();

		return entity;
	}

	/*
	 * Return an escaped version of the input.
	 * @param unEscapedString
	 *
	 * @return escaped version of the string
	 */
	private static String escapeSpecialCharacters(String unEscapedString) {
		String[] chars = unEscapedString.split('');
		for (Integer i = 0; i < chars.size(); i++) {
			if (SPECIAL_CHARACTERS_PATTERN.matcher(chars[i]).matches()) {
				chars[i] = '\\' + chars[i];
			}
		}
		return String.join(chars, '');
	}
}