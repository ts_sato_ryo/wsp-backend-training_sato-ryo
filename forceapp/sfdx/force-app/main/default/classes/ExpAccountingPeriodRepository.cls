/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Repository class for Accounting Period
 */
public with sharing class ExpAccountingPeriodRepository extends ValidPeriodRepository {

	/** Max record number to fetch */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;
	/** Do not execute query with 'FOR UPDATE' */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * Target fields to fetch data
	 * If field is added, add same field to baseFieldSet
	 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		// Target fields definition
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				ExpAccountingPeriod__c.Id,
				ExpAccountingPeriod__c.Name,
				ExpAccountingPeriod__c.Code__c,
				ExpAccountingPeriod__c.UniqKey__c,
				ExpAccountingPeriod__c.CompanyId__c,
				ExpAccountingPeriod__c.Name_L0__c,
				ExpAccountingPeriod__c.Name_L1__c,
				ExpAccountingPeriod__c.Name_L2__c,
				ExpAccountingPeriod__c.ValidFrom__c,
				ExpAccountingPeriod__c.ValidTo__c,
				ExpAccountingPeriod__c.RecordingDate__c,
				ExpAccountingPeriod__c.Active__c
				};
	}

	/**
	 * Get entity by specified ID
	 * @param id ID of target entity
	 * @return Entity of specified ID. If no data is found, return null.
	 */
	public ExpAccountingPeriodEntity getEntity(Id id) {
		List<ExpAccountingPeriodEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * Get entity list by specified IDs
	 * @param ids ID list of target entity
	 * @return Entity list of specified ID.  If no data is found, return null.
	 */
	public List<ExpAccountingPeriodEntity> getEntityList(List<Id> ids) {
		ExpAccountingPeriodRepository.SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * Save a Accounting Period record
	 * This method is used for both new creation and update.
	 * @param entity Entity to be saved
	 * @return Saved result
	 */
	public Repository.SaveResult saveEntity(ExpAccountingPeriodEntity entity) {
		return saveEntityList(new List<ExpAccountingPeriodEntity>{ entity });
	}

	/**
   * Save Accounting Period records
   * This method is used for both new creation and update.
   * @param entity Entity list to be saved
   * @return Saved result
   */
	public Repository.SaveResult saveEntityList(List<ExpAccountingPeriodEntity> entityList) {

		// Convert entity to sObject
		List<ExpAccountingPeriod__c> sObjList = createObjectList(entityList);

		//Save to DB
		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);

		// Make and return result
		return resultFactory.createSaveResult(resultList);
	}

	/** Search Filters */
	public class SearchFilter {
		/** ID */
		public Set<Id> ids;
		/** Code (exact match) */
		public Set<String> codes;
		/** Company ID */
		public Set<Id> companyIds;
		/** Target date */
		public AppDate targetDate;
		/** Active */
		public Integer active;
		/** Active const */
		public final Integer ONLY_ACTIVE = 0;
		public final Integer ONLY_INACTIVE = 1;
		public final Integer ALL = 2;
	}

	/**
	 * Search Accounting Period
	 * @param filter Search Filter
	 * @return Search Result
	 */
	public List<ExpAccountingPeriodEntity> searchEntity(ExpAccountingPeriodRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * Search Accounting Period
	 * @param filter Search Filter
	 * @param forUpdate When call this method for update, specify as true.
	 * @order Sort order
	 * @return Search Result
	 */
	public List<ExpAccountingPeriodEntity> searchEntity(ExpAccountingPeriodRepository.SearchFilter filter, Boolean forUpdate) {

		// WHERE statement
		List<String> whereList = new List<String>();
		// Filter by IDs
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// Filter by Company Codes
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ExpAccountingPeriod__c.Code__c) + ' = :pCodes');
		}
		// Filter by Company IDs
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ExpAccountingPeriod__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// Get valid record
		// * ValidTo__c is a "expiry date". Do not contain. *
		Date pTargetDate;
		if (filter.targetDate != null) {
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ExpAccountingPeriod__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ExpAccountingPeriod__c.ValidTo__c) + ' > :pTargetDate');
		}

		// Filter by Active flag
		if (filter.active == filter.ONLY_ACTIVE) {
			whereList.add(getFieldName(ExpAccountingPeriod__c.Active__c) + ' = TRUE');
		} else if (filter.active == filter.ONLY_INACTIVE) {
			whereList.add(getFieldName(ExpAccountingPeriod__c.Active__c) + ' = FALSE');
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY statement
		String orderByString = ' ORDER BY ' + getFieldName(ExpAccountingPeriod__c.ValidFrom__c);

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		// Regular fields
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFieldList.add(getFieldName(fld));
		}

		// Build SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpAccountingPeriod__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderByString
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		//System.debug('ExpAccountingPeriodRepository: SOQL=' + soql);

		// Execute query
		List<ExpAccountingPeriod__c> sObjs = (List<ExpAccountingPeriod__c>)Database.query(soql);

		// Create entity from SObject
		List<ExpAccountingPeriodEntity> entities = new List<ExpAccountingPeriodEntity>();
		for (ExpAccountingPeriod__c sObj : sObjs) {
			entities.add(new ExpAccountingPeriodEntity(sObj));
		}

		return entities;
	}

	/**
	 * Create object from entity to save
	 * @param entityList Target entity list to save
	 * @return Object list to be saved
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntity> entityList) {

		List<ExpAccountingPeriod__c> objectList = new List<ExpAccountingPeriod__c>();
		for (ValidPeriodEntity entity : entityList) {
			objectList.add(((ExpAccountingPeriodEntity)entity).createSObject());
		}

		return objectList;
	}
}