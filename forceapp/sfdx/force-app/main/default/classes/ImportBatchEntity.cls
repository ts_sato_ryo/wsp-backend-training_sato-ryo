/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * インポートバッチのエンティティ
 */
public with sharing class ImportBatchEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		Name,
		TargetDate,
		Status,
		Type,
		ActorHistoryId,
		Comment,
		Count,
		SuccessCount,
		FailureCount
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ
	 */
	public ImportBatchEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/** インポートバッチ名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.Name);
		}
	}

	/** 対象日 */
	public AppDate targetDate {
		get;
		set {
			targetDate = value;
			setChanged(Field.TargetDate);
		}
	}

	/** ステータス */
	public ImportBatchStatus status {
		get;
		set {
			status = value;
			setChanged(Field.Status);
		}
	}

	/** 種別 */
	public ImportBatchType type {
		get;
		set {
			type = value;
			setChanged(Field.Type);
		}
	}
	/** 処理件数 */
	public Integer count {
		get;
		set {
			count = value;
			setChanged(Field.Count);
		}
	}

	/** 成功件数 */
	public Integer successCount {
		get;
		set {
			successCount = value;
			setChanged(Field.SuccessCount);
		}
	}

	/** 失敗件数 */
	public Integer failureCount {
		get;
		set {
			failureCount = value;
			setChanged(Field.FailureCount);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}