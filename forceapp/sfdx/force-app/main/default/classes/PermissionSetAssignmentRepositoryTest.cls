/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 権限セット割り当てリポジトリ(標準オブジェクト)のテスト
 */
@isTest
private class PermissionSetAssignmentRepositoryTest {

	/**
	 * searchEntityList のテスト
	 * 指定した検索条件で検索できることを確認する
	 */
	@isTest static void searchEntityListTestFilter() {
		final Integer adminUserNumber = 2;
		final Integer standardUserNumber = 1;

		List<User> admindUsers = ComTestDataUtility.createUsers('Admin', 'ja', 'ja_JP', adminUserNumber);
		List<User> standardUsers = ComTestDataUtility.createStandardUsers('Standard', standardUserNumber);


		PermissionSetAssignmentRepository repo = new PermissionSetAssignmentRepository();
		PermissionSetAssignmentRepository.SearchFilter filter;
		List<PermissionSetAssignmentEntity> resultList;

		// Test1: 任命先IDを指定して検索
		User targetAssigneeUser = admindUsers[0];
		filter = new PermissionSetAssignmentRepository.SearchFilter();
		filter.assigneeIds = new List<Id>{targetAssigneeUser.Id};
		resultList = repo.searchEntityList(filter, false);
		System.assertEquals(1, resultList.size());
		System.assertEquals(targetAssigneeUser.Id, resultList[0].assigneeId);

		// Test2: 指定したユーザの中で全てのデータの編集権限を持つユーザのみ検索
		filter = new PermissionSetAssignmentRepository.SearchFilter();
		filter.assigneeIds = new List<Id>{admindUsers[0].Id, admindUsers[1].Id, standardUsers[0].Id};
		filter.isModifyAllData = true;
		resultList = repo.searchEntityList(filter, false);
		System.assertEquals(2, resultList.size());
		System.assertEquals(targetAssigneeUser.Id, resultList[0].assigneeId);
	}

	/**
	 * searchEntityList のテスト
	 * 各項目の値が取得できていることを確認する
	 */
	@isTest static void searchEntityListTestAllField() {
		final Integer adminUserNumber = 1;

		List<User> admindUsers = ComTestDataUtility.createUsers('Admin', 'ja', 'ja_JP', adminUserNumber);

		User targetUser = admindUsers[0];
		PermissionSetAssignmentRepository.SearchFilter filter =
				new PermissionSetAssignmentRepository.SearchFilter();
		filter.assigneeIds = new List<Id>{targetUser.Id};
		filter.isModifyAllData = true;

		List<PermissionSetAssignmentEntity> resultEntityList =
				new PermissionSetAssignmentRepository().searchEntityList(filter, false);

		System.assertEquals(1, resultEntityList.size());

		// 各項目の値チェック
		PermissionSet adminPermissionSet = [SELECT Id FROM PermissionSet WHERE ProfileId = :targetUser.ProfileId];
		PermissionSetAssignmentEntity resultEntity = resultEntityList.get(0);
		System.assertEquals(targetUser.Id, resultEntity.assigneeId);
		System.assertEquals(adminPermissionSet.Id, resultEntity.permissionSetId);
	}
}