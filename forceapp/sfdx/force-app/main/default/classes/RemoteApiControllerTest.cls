/**
 * RemoteApiControllerのテスト
 */
@isTest
private class RemoteApiControllerTest {

	/** テストデータ */
	private class TestData extends TestData.TestDataEntity {
	}

	/**
	 * 言語プロパティのテスト
	 * プロパティに値が正しく設定されていることを確認する
	 */
	@isTest static void languageTest() {
		TestData testData = new RemoteApiControllerTest.TestData();
		User admindUser = ComTestDataUtility.createUser('Admin', 'en_US', 'ja_JP');
		EmployeeBaseEntity employee = testData.createEmployee('Test', null, admindUser.Id);

		RemoteApiController controller;
		System.runAs(admindUser) {
			controller = new RemoteApiController();
		}

		// "_"(アンダースコア)は"-"(ハイフン)に置換される
		System.assertEquals('en-US', controller.languageLocaleKey);
	}

	/**
	 * 社員の権限プロパティのテスト
	 * プロパティに値が正しく設定されていることを確認する
	 */
	@isTest static void employeePremissionTest() {
		TestData testData = new RemoteApiControllerTest.TestData();
		User admindUser = ComTestDataUtility.createUser('Admin', 'ja', 'ja_JP');
		EmployeeBaseEntity employee = testData.createEmployee('Test', null, admindUser.Id);

		RemoteApiController controller;
		System.runAs(admindUser) {
			controller = new RemoteApiController();
		}

		// システム管理者のため全ての権限はtrue
		// (本当は１権限ずつ確認するべきだとは思いますが。。)
		System.assertEquals(true, controller.employeePremission.viewAttTimeSheetByDelegate);
		System.assertEquals(true, controller.employeePremission.editAttTimeSheetByDelegate);
		System.assertEquals(true, controller.employeePremission.submitAttDailyRequestByDelegate);
		System.assertEquals(true, controller.employeePremission.approveAttDailyRequestByDelegate);
		System.assertEquals(true, controller.employeePremission.approveSelfAttDailyRequestByEmployee);
		System.assertEquals(true, controller.employeePremission.cancelAttDailyRequestByDelegate);
		System.assertEquals(true, controller.employeePremission.cancelAttDailyApprovalByEmployee);
		System.assertEquals(true, controller.employeePremission.cancelAttDailyApprovalByDelegate);
		System.assertEquals(true, controller.employeePremission.submitAttRequestByDelegate);
		System.assertEquals(true, controller.employeePremission.approveAttRequestByDelegate);
		System.assertEquals(true, controller.employeePremission.approveSelfAttRequestByEmployee);
		System.assertEquals(true, controller.employeePremission.cancelAttRequestByDelegate);
		System.assertEquals(true, controller.employeePremission.cancelAttApprovalByEmployee);
		System.assertEquals(true, controller.employeePremission.cancelAttApprovalByDelegate);
		System.assertEquals(true, controller.employeePremission.viewTimeTrackByDelegate);
		System.assertEquals(true, controller.employeePremission.manageOverallSetting);
		System.assertEquals(true, controller.employeePremission.switchCompany);
		System.assertEquals(true, controller.employeePremission.manageDepartment);
		System.assertEquals(true, controller.employeePremission.manageEmployee);
		System.assertEquals(true, controller.employeePremission.manageCalendar);
		System.assertEquals(true, controller.employeePremission.manageJobType);
		System.assertEquals(true, controller.employeePremission.manageJob);
		System.assertEquals(true, controller.employeePremission.manageMobileSetting);
		System.assertEquals(true, controller.employeePremission.managePlannerSetting);
		System.assertEquals(true, controller.employeePremission.managePermission);
		System.assertEquals(true, controller.employeePremission.manageAttLeave);
		System.assertEquals(true, controller.employeePremission.manageAttShortTimeWorkSetting);
		System.assertEquals(true, controller.employeePremission.manageAttLeaveOfAbsence);
		System.assertEquals(true, controller.employeePremission.manageAttWorkingType);
		System.assertEquals(true, controller.employeePremission.manageAttPattern);
		System.assertEquals(true, controller.employeePremission.manageAttAgreementAlertSetting);
		System.assertEquals(true, controller.employeePremission.manageAttLeaveGrant);
		System.assertEquals(true, controller.employeePremission.manageAttShortTimeWorkSettingApply);
		System.assertEquals(true, controller.employeePremission.manageAttLeaveOfAbsenceApply);
		System.assertEquals(true, controller.employeePremission.manageAttPatternApply);
		System.assertEquals(true, controller.employeePremission.manageTimeSetting);
		System.assertEquals(true, controller.employeePremission.manageTimeWorkCategory);
		System.assertEquals(true, controller.employeePremission.manageExpTypeGroup);
		System.assertEquals(true, controller.employeePremission.manageExpenseType);
		System.assertEquals(true, controller.employeePremission.manageTaxType);
		System.assertEquals(true, controller.employeePremission.manageExpSetting);
		System.assertEquals(true, controller.employeePremission.manageExchangeRate);
		System.assertEquals(true, controller.employeePremission.manageAccountingPeriod);
		System.assertEquals(true, controller.employeePremission.manageReportType);
		System.assertEquals(true, controller.employeePremission.manageCostCenter);
		System.assertEquals(true, controller.employeePremission.manageVendor);
		System.assertEquals(true, controller.employeePremission.manageExtendedItem);
		System.assertEquals(true, controller.employeePremission.manageEmployeeGroup);
	}

	/**
	 * pageTitleMapのテスト
	 * - Webタブの表示名がpageTitleMapから正しく取得できることを確認する
	 */
	@isTest static void pageTitleMapTest() {

		RemoteApiController controller = new RemoteApiController();

		Map<String, String> pageTitleMap = controller.pageTitleMap;
		// Common
		System.assertNotEquals(null, pageTitleMap.get('Admin'), 'Failed to get page name from Admin Tab');
		System.assertEquals(ComMessage.msg().Com_Tab_Admin, pageTitleMap.get('Admin'));
		System.assertNotEquals(null, pageTitleMap.get('Approval'), 'Failed to get page name from Approval Tab');
		System.assertEquals(ComMessage.msg().Com_Tab_Approval, pageTitleMap.get('Approval'));
		System.assertNotEquals(null, pageTitleMap.get('Planner'), 'Failed to get page name of Planner');
		System.assertEquals(ComMessage.msg().Com_Tab_Planner, pageTitleMap.get('Planner'));
		System.assertNotEquals(null, pageTitleMap.get('Team'), 'Failed to get page name from Team Tab');
		System.assertEquals(ComMessage.msg().Com_Tab_Team, pageTitleMap.get('Team'));

		// Attendance
		System.assertNotEquals(null, pageTitleMap.get('TimeAttendance'), 'Failed to get page name of Time & Attendance');
		System.assertEquals(ComMessage.msg().Com_Tab_TimeAttendance, pageTitleMap.get('TimeAttendance'));
		System.assertNotEquals(null, pageTitleMap.get('OAuthResultView'), 'Failed to get page name of OAth Result');
		System.assertEquals(ComMessage.msg().Com_Tab_OAuthResultView, pageTitleMap.get('OAuthResultView'), 'Failed to get page name of OAth Result');

		// Time Tracking
		System.assertNotEquals(null, pageTitleMap.get('Tracking'), 'Failed to get page name from Tracking Tab');
		System.assertEquals(ComMessage.msg().Com_Tab_Tracking, pageTitleMap.get('Tracking'));

		// Expenses
		System.assertNotEquals(null, pageTitleMap.get('Expenses'), 'Failed to get page name from Expenses Tab');
		System.assertEquals(ComMessage.msg().Com_Tab_Expenses, pageTitleMap.get('Expenses'));
		System.assertNotEquals(null, pageTitleMap.get('FinanceApproval'), 'Failed to get page name from Finance Approval Tab');
		System.assertEquals(ComMessage.msg().Com_Tab_FinanceApproval, pageTitleMap.get('FinanceApproval'));
		System.assertNotEquals(null, pageTitleMap.get('Requests'), 'Failed to get page name from Requests Tab');
		System.assertEquals(ComMessage.msg().Com_Tab_Requests, pageTitleMap.get('Requests'));
	}
}