/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Repository class for Expense Modification History
 */
public with sharing class ExpModificationHistoryRepository extends Repository.BaseRepository {

	/**
   * Target fields to fetch data
   * If field is added, add same field to baseFieldSet
   */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		// Target fields definition
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				ExpModificationHistory__c.Id,
				ExpModificationHistory__c.Name,
				ExpModificationHistory__c.ExpRequestId__c,
				ExpModificationHistory__c.ExpReportId__c,
				ExpModificationHistory__c.ExpRecordId__c,
				ExpModificationHistory__c.ExpRecordItemId__c,
				ExpModificationHistory__c.RecordSummary__c,
				ExpModificationHistory__c.ExtendedItemId__c,
				ExpModificationHistory__c.ExtendedItemName__c,
				ExpModificationHistory__c.Field__c,
				ExpModificationHistory__c.FieldLabel__c,
				ExpModificationHistory__c.NewValue__c,
				ExpModificationHistory__c.OldValue__c,
				ExpModificationHistory__c.Comment__c,
				ExpModificationHistory__c.ModifiedBy__c,
				ExpModificationHistory__c.ModifiedDateTime__c
			};
	}

	/** Relationship names of sObject fields */
	private static final String EXP_RECORD_R = ExpModificationHistory__c.ExpRecordId__c.getDescribe().getRelationshipName();
	private static final String EMP_BASE_R = ExpModificationHistory__c.ModifiedBy__c.getDescribe().getRelationshipName();

	/** Search Filters */
	public class SearchFilter {
		/** Request ID */
		public Id requestId;
		/** Report ID */
		public Id reportId;
		/** Record IDs */
		public Set<Id> recordIds;
		/** Fields */
		public List<String> fields;
		/** Excluded fields */
		public List<String> excludedFields;
	}

	/**
 * Save Modification History records
 * @param entity Entity list to be saved
 * @return Saved result
 */
	public Repository.SaveResult saveEntityList(List<ExpModificationHistoryEntity> entityList) {

		// Convert entity to sObject
		List<ExpModificationHistory__c> sObjList = createObjectList(entityList);

		//Save to DB
		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);

		// Make and return result
		return resultFactory.createSaveResult(resultList);
	}

	/**
	   * Search modification history
	   * @param filter Search Filter
	   * @return Search Result
	   */
	public List<ExpModificationHistoryEntity> searchEntity(SearchFilter filter) {

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFieldList.add(getFieldName(fld));
		}
		selectFieldList.add(getFieldName(EXP_RECORD_R, ExpRecord__c.Order__c));
		selectFieldList.add(getFieldName(EMP_BASE_R, ComEmpBase__c.FirstName_L0__c));
		selectFieldList.add(getFieldName(EMP_BASE_R, ComEmpBase__c.LastName_L0__c));
		selectFieldList.add(getFieldName(EMP_BASE_R, ComEmpBase__c.FirstName_L1__c));
		selectFieldList.add(getFieldName(EMP_BASE_R, ComEmpBase__c.LastName_L1__c));
		selectFieldList.add(getFieldName(EMP_BASE_R, ComEmpBase__c.FirstName_L2__c));
		selectFieldList.add(getFieldName(EMP_BASE_R, ComEmpBase__c.LastName_L2__c));

		// WHERE statement
		List<String> whereList = new List<String>();
		// Filter by IDs
		Id pRequestId;
		if (filter.requestId != null) {
			pRequestId = filter.requestId;
			whereList.add(getFieldName(ExpModificationHistory__c.ExpRequestId__c) + '= :pRequestId');
		}
		// Filter by report IDs
		Id pReportId;
		if (filter.reportId != null) {
			pReportId = filter.reportId;
			whereList.add(getFieldName(ExpModificationHistory__c.ExpReportId__c) + '= :pReportId');
		}
		// Filter by record IDs
		Set<Id> pRecordIds;
		if (filter.recordIds != null) {
			pRecordIds = filter.recordIds;
			whereList.add(getFieldName(ExpModificationHistory__c.ExpRecordId__c) + ' IN :pRecordIds');
		}
		// Filter by fields
		List<String> pFields;
		if (filter.fields != null) {
			pFields = filter.fields;
			whereList.add(getFieldName(ExpModificationHistory__c.Field__c) + ' IN :pFields');
		}
		// Filter by excluded fields
		List<String> pExcludedFields;
		if (filter.excludedFields != null) {
			pExcludedFields = filter.excludedFields;
			whereList.add(getFieldName(ExpModificationHistory__c.Field__c) + ' NOT IN :pExcludedFields');
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY statement
		String orderByString = ' ORDER BY ' + getFieldName(ExpModificationHistory__c.ModifiedDateTime__c) + ' DESC,' +
			getFieldName(EXP_RECORD_R, ExpRecord__c.Order__c) + ' ASC NULLS FIRST, ' +
			getFieldName(ExpModificationHistory__c.Field__c);

		// Build SOQL
		String soql =
			'SELECT ' + String.join(selectFieldList, ',') +
			' FROM ' + ExpModificationHistory__c.SObjectType.getDescribe().getName() +
			whereString +
			orderByString;

		// Execute query
		List<ExpModificationHistory__c> sObjs = (List<ExpModificationHistory__c>)Database.query(soql);

		// Create entity from SObject
		List<ExpModificationHistoryEntity> entities = new List<ExpModificationHistoryEntity>();
		for (ExpModificationHistory__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	/**
   * Create object from entity to save
   * @param entityList Target entity list to save
   * @return Object list to be saved
   */
	private List<ExpModificationHistory__c> createObjectList(List<ExpModificationHistoryEntity> entityList) {

		List<ExpModificationHistory__c> objectList = new List<ExpModificationHistory__c>();
		for (ExpModificationHistoryEntity entity : entityList) {
			objectList.add(createObject((ExpModificationHistoryEntity)entity));
		}

		return objectList;
	}

	/**
	 * Create object from entity to save
	 * @param entity Original entity
	 * @return Object to be saved
	 */
	private ExpModificationHistory__c createObject(ExpModificationHistoryEntity entity) {

		ExpModificationHistory__c sObj = new ExpModificationHistory__c();

		sObj.ExpRequestId__c = entity.expRequestId;
		sObj.ExpReportId__c = entity.expReportId;
		sObj.ExpRecordId__c = entity.expRecordId;
		sObj.ExpRecordItemId__c = entity.expRecordItemId;
		sObj.RecordSummary__c = entity.recordSummary;
		sObj.ExtendedItemId__c = entity.extendedItemId;
		sObj.ExtendedItemName__c = entity.extendedItemName != null ? JSON.serialize(entity.extendedItemName) : null;
		sObj.Field__c = entity.field;
		sObj.FieldLabel__c = entity.fieldLabel;
		sObj.NewValue__c = entity.newValue;
		sObj.OldValue__c = entity.oldValue;
		sObj.Comment__c = entity.comment;
		sObj.ModifiedBy__c = entity.modifiedBy;
		sObj.ModifiedDateTime__c = entity.modifiedDateTime.getDatetime();

		return sObj;
	}

	/**
   * Create entity from SObject
   * @param obj Original SObject
   * @return Created entity
   */
	private ExpModificationHistoryEntity createEntity(ExpModificationHistory__c sObj) {

		ExpModificationHistoryEntity entity = new ExpModificationHistoryEntity();

		entity.setId(sObj.Id);
		entity.expRequestId = sObj.ExpRequestId__c;
		entity.expReportId = sObj.ExpReportId__c;
		entity.expRecordId = sObj.ExpRecordId__c;
		entity.expRecordItemId = sObj.ExpRecordItemId__c;
		entity.recordSummary = sObj.RecordSummary__c;
		entity.extendedItemId = sObj.ExtendedItemId__c;
		if (entity.extendedItemId != null) {
			entity.extendedItemName = (AppMultiString)JSON.deserialize(sObj.ExtendedItemName__c, AppMultiString.class);
		}
		entity.field = sObj.Field__c;
		entity.fieldLabel = sObj.FieldLabel__c;
		entity.newValue = sObj.NewValue__c;
		entity.oldValue = sObj.OldValue__c;
		entity.comment = sObj.Comment__c;
		entity.modifiedBy = sObj.ModifiedBy__c;
		entity.modifiedByEmployeeName = new AppPersonName(
			sObj.ModifiedBy__r.FirstName_L0__c, sObj.ModifiedBy__r.LastName_L0__c,
			sObj.ModifiedBy__r.FirstName_L1__c, sObj.ModifiedBy__r.LastName_L1__c,
			sObj.ModifiedBy__r.FirstName_L2__c, sObj.ModifiedBy__r.LastName_L2__c
		);
		entity.modifiedDateTime = new AppDatetime(sObj.ModifiedDateTime__c);

		return entity;
	}
}