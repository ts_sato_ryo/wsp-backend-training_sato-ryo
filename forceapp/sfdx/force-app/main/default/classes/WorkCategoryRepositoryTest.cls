@isTest
private class WorkCategoryRepositoryTest {

	/** テストデータ */
	private class RepoTestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj;

		/** コンストラクタ */
		public RepoTestData() {
			this.orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			this.countryObj = ComTestDataUtility.createCountry('Japan');
			this.companyObj = ComTestDataUtility.createCompany('Test Company', this.countryObj.Id);
		}

		/**
		 * 作業分類オブジェクトを作成する
		 */
		public List<TimeWorkCategory__c> createWorkCategoryObjs(String name, Integer size) {
			List<TimeWorkCategory__c> workCategories = new List<TimeWorkCategory__c>();
			Date validFrom = Date.today();
			for (Integer i = 0; i < size; i++) {
				String num = String.valueOf(i + 1);
				String suffix = size > 1 ? num : '';
				TimeWorkCategory__c workCategory = new TimeWorkCategory__c(
					Name = name + suffix,
					Name_L0__c = name + suffix + ' L0',
					Name_L1__c = name + suffix + ' L1',
					Name_L2__c = name + suffix + ' L2',
					Code__c = name + suffix + '_code',
					UniqKey__c = this.companyObj.Code__c + '-' + name + suffix + '_code',
					CompanyId__c = this.companyObj.Id,
					Order__c = i + 1,
					ValidFrom__c = validFrom,
					ValidTo__c = validFrom.addYears(1)
				);
				workCategories.add(workCategory);

				validFrom = workCategory.ValidTo__c;
			}
			insert workCategories;
			return workCategories;
		}

		/**
		 * 作業分類エンティティを作成する(DB保存なし)
		 */
		public WorkCategoryEntity createWorkCategoryEntity(String name) {
			WorkCategoryEntity entity = new WorkCategoryEntity();

			entity.name = name;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.code = name + '_code';
			entity.uniqKey = this.companyObj.Code__c + '-' + name + '_code';
			entity.companyId = this.companyObj.Id;
			entity.order = 1;
			entity.validFrom = AppDate.today();
			entity.validTo = entity.validFrom.addMonths(12);

			return entity;
		}

		/**
		 * 会社オブジェクトを作成する
		 */
		public ComCompany__c createCompany(String name) {
			return ComTestDataUtility.createCompany(name, this.countryObj.Id);
		}

	}

	/**
	 * searchEntityのテスト
	 * 作業分類が検索できることを確認する
	 */
	@isTest static void getEntityTest() {
		final Integer workCategoryNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<TimeWorkCategory__c> workCategoryObjs = testData.createWorkCategoryObjs('Test', workCategoryNumber);
		TimeWorkCategory__c targetWorkCategoryObj = workCategoryObjs[1];

		// テスト実行
		WorkCategoryRepository repo = new WorkCategoryRepository();
		WorkCategoryEntity resEntity;

		// 作業分類IDで検索
		// 各項目の値が取得できているかどうかはsearchEntityで実施
		resEntity = repo.getEntity(targetWorkCategoryObj.Id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetWorkCategoryObj.Id, resEntity.id);

		// 存在しないIDを指定した場合
		// nullが返却される
		delete targetWorkCategoryObj;
		resEntity = repo.getEntity(targetWorkCategoryObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * 作業分類が検索できることを確認する
	 */
	@isTest static void getEntityByCodeTest() {
		final Integer workCategoryNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<TimeWorkCategory__c> workCategoryObjs = testData.createWorkCategoryObjs('Test', workCategoryNumber);
		TimeWorkCategory__c targetWorkCategoryObj = workCategoryObjs[1];
		targetWorkCategoryObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		update targetWorkCategoryObj;

		// テスト実行
		WorkCategoryRepository repo = new WorkCategoryRepository();
		WorkCategoryEntity resEntity;

		// 作業分類コードで検索
		// 各項目の値が取得できているかどうかはsearchEntityで実施
		resEntity = repo.getEntityByCode(targetWorkCategoryObj.Code__c, targetWorkCategoryObj.CompanyId__c);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetWorkCategoryObj.Id, resEntity.id);

		// 存在しないコードを指定した場合
		// nullが返却される
		delete targetWorkCategoryObj;
		resEntity = repo.getEntityByCode(targetWorkCategoryObj.Code__c, testData.companyObj.Id);
		System.assertEquals(null, resEntity);
	}

	/**
	 * searchEntityのテスト
	 * 作業分類が検索できることを確認する
	 */
	@isTest static void searchEntityTest() {
		final Integer workCategoryNumber = 3;
		RepoTestData testData = new RepoTestData();
		List<TimeWorkCategory__c> workCategoryObjs = testData.createWorkCategoryObjs('Test', workCategoryNumber);
		TimeWorkCategory__c targetWorkCategoryObj = workCategoryObjs[1];
		targetWorkCategoryObj.CompanyId__c = testData.createCompany('会社IDで検索').Id;
		update targetWorkCategoryObj;
		targetWorkCategoryObj = getWorkCategoryObj(targetWorkCategoryObj.Id); // 参照先の項目を取得するため

		// テスト実行
		WorkCategoryRepository repo = new WorkCategoryRepository();
		WorkCategoryRepository.SearchFilter filter;
		List<WorkCategoryEntity> resEntities;

		// 作業分類IDで検索
		filter = new WorkCategoryRepository.SearchFilter();
		filter.ids = new Set<Id>{targetWorkCategoryObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetWorkCategoryObj.Id, resEntities[0].id);

		// 全ての項目の値が取得できていることを確認
		assertFieldValue(targetWorkCategoryObj, resEntities[0]);

		// 会社IDで検索
		filter = new WorkCategoryRepository.SearchFilter();
		filter.companyIds = new Set<Id>{targetWorkCategoryObj.CompanyId__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetWorkCategoryObj.Id, resEntities[0].id);

		// コード(完全一致)で検索
		filter = new WorkCategoryRepository.SearchFilter();
		filter.codes = new Set<String>{targetWorkCategoryObj.Code__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetWorkCategoryObj.Id, resEntities[0].id);

		//---------------------------
		// 取得対象日のテストは searchEntityTestValidDateで実施
		//---------------------------
	}

	/**
	 * 作業分類が保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		RepoTestData testData = new RepoTestData();
		WorkCategoryEntity entity = testData.createWorkCategoryEntity('Test');

		// テスト実行
		WorkCategoryRepository repo = new WorkCategoryRepository();
		Repository.SaveResult result = repo.saveEntity(entity);

		// 確認
		System.assertEquals(true, result.isSuccessAll);

		TimeWorkCategory__c resObj1 = getWorkCategoryObj(result.details[0].id);
		System.assertNotEquals(null, resObj1);
		assertFieldValue(entity, resObj1);
	}


	/**
	 * ジョブオブジェクトを取得する
	 */
	private static TimeWorkCategory__c getWorkCategoryObj(Id workCategoryId) {
		return [
				SELECT
					Id, Name, Name_L0__c, Name_L1__c, Name_L2__c, Code__c, UniqKey__c,
					CompanyId__c, Order__c, ValidFrom__c, ValidTo__c
				FROM
					TimeWorkCategory__c
				WHERE
					Id = :workCategoryId];
	}

	/**
	 * 項目の値を検証する
	 * @param expObj 期待値
	 * @param actEntity 実際の値
	 */
	private static void assertFieldValue(TimeWorkCategory__c expObj, WorkCategoryEntity actEntity) {

		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(AppDate.valueOf(expObj.ValidFrom__c), actEntity.validFrom);
		System.assertEquals(AppDate.valueOf(expObj.ValidTo__c), actEntity.validTo);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
		System.assertEquals(expObj.Order__c, actEntity.order);
	}

	/**
	 * 各項目の値を検証する
	 * @param expEntity 期待値
	 * @param actObj 実際の値
	 */
	private static void assertFieldValue(WorkCategoryEntity expEntity, TimeWorkCategory__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.validFrom, AppDate.valueOf(actObj.ValidFrom__c));
		System.assertEquals(expEntity.validTo, AppDate.valueOf(actObj.ValidTo__c));
		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
		System.assertEquals(expEntity.order, actObj.Order__c);
	}

}