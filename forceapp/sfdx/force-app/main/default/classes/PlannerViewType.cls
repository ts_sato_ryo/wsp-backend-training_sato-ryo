/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group プランナー
 *
 * プランナー表示種別
 */
public with sharing class PlannerViewType extends ValueObjectType {

	/** 日表示 */
	public static final PlannerViewType DAILY = new PlannerViewType('Daily');
	/** 週表示 */
	public static final PlannerViewType WEEKLY = new PlannerViewType('Weekly');
	/** 月表示 */
	public static final PlannerViewType MONTHLY = new PlannerViewType('Monthly');

	/** プランナー表示種別のリスト（種別が追加されら本リストにも追加してください） */
	public static final List<PlannerViewType> TYPE_LIST;
	static {
		TYPE_LIST = new List<PlannerViewType> {
			DAILY,
			WEEKLY,
			MONTHLY
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private PlannerViewType(String value) {
		super(value);
	}

	/**
	 * 値からPlannerViewTypeを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つPlannerViewType、ただし該当するPlannerViewTypeが存在しない場合はnull
	 */
	public static PlannerViewType valueOf(String value) {
		PlannerViewType retType = null;
		if (String.isNotBlank(value)) {
			for (PlannerViewType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// PlannerViewType以外の場合はfalse
		Boolean eq;
		if (compare instanceof PlannerViewType) {
			// 値が同じであればtrue
			if (this.value == ((PlannerViewType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}