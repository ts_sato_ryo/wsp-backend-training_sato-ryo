/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 社員履歴切り替えバッチのテストクラス
 */
@isTest
private class EmployeeCurrentHistoryUpdateBatchTest {

	/**
	 * Apexスケジュール正常系テスト
	 */
	@isTest
	static void schedulePositiveTest() {
		Test.StartTest();
		EmployeeCurrentHistoryUpdateBatch sch = new EmployeeCurrentHistoryUpdateBatch();
		Id schId = System.schedule('社員履歴切り替えスケジュール', '0 0 0 * * ?', sch);
		Test.StopTest();
		System.assertNotEquals(null, schId);
	}

	/**
	 * バッチ正常系テスト
	 */
	@isTest
	static void batchPositiveTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		final Integer baseNum = 2;
		List<ComEmpBase__c> baseObjs = ComTestDataUtility.createEmployeesWithHistory(
			'Test', companyObj.Id, null, UserInfo.getUserId(), permissionObj.Id, baseNum);
		ComEmpBase__c base1 = baseObjs[0];
		List<ComEmpHistory__c> historyList1 =
				[SELECT Id, BaseId__c FROM ComEmpHistory__c WHERE BaseId__c = :base1.Id ORDER BY ValidFrom__c Asc];

		// CurrentHistoryId__c を空欄にしておく
		base1.CurrentHistoryId__c = null;
		update base1;

		Test.StartTest();
		EmployeeCurrentHistoryUpdateBatch b = new EmployeeCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
		Test.StopTest();
		System.assertNotEquals(null, jobId);

		ComEmpBase__c resBase = [SELECT Id, Name, CurrentHistoryId__c FROM ComEmpBase__c WHERE Id = :base1.Id];
		System.assertEquals(historyList1[0].Id, resBase.CurrentHistoryId__c);
		// Name項目は更新されない
		System.assertEquals(base1.Name, resBase.Name);
	}

}