/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 費目グループマスタのエンティティ
 */
public with sharing class ExpTypeEntity extends ValidPeriodEntityOld {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 費目グループ名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 説明の最大文字数 */
	public static final Integer DESCRIPTION_MAX_LENGTH = 1024;
	/** 表示順の最小値 */
	public static final Integer ORDER_MIN_VALUE = 1;
	/** 表示順の最大値 */
	public static final Integer ORDER_MAX_VALUE = 9999;

	/** Number of Extended Items for each type */
	public static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/** Maximum Number of Fixed Allowance Option allowed */
	public static final Integer FIXED_ALLOWANCE_OPTION_MAX_COUNT = 20;

	/** Maximum Number of Fixed Allowance Integer Digits */
	public static final Integer FIXED_ALLOWANCE_MAX_INTEGER_LENGTH = 12;
	/** Maximum Amount of Fixed Allowance allowed */
	public static final Decimal FIXED_ALLOWANCE_MAX_AMOUNT = 999999999999.0;

	/**
	 * Pattern to matches any of the following: ExtendedItem{XX}{YY}{ZZ}
	 * Where
	 *  XX: `Text`, `Picklist`, `Lookup` or `Date`
	 *  YY: 01 ~ 10
	 *  ZZ: `Id` or `Value`
	 **/
	private static final Pattern EXTENDED_ITEM_CONFIG_PATTERN = Pattern.compile('ExtendedItem((Text)|(Picklist)|(Lookup)|(Date)){1}[0-9]{2}((Id)|(UsedIn)|(RequiredFor)){1}');

	/** String templates to access the Extended Item fields - based on the Field enum names */
	private static final String EXTENDED_ITEM_ID_TEMPLATE = 'ExtendedItem{0}{1}Id';
	private static final String EXTENDED_ITEM_USED_IN_TEMPLATE = 'ExtendedItem{0}{1}UsedIn';
	private static final String EXTENDED_ITEM_REQUIRED_FOR_TEMPLATE = 'ExtendedItem{0}{1}RequiredFor';

	/**
	 * 項目の定義
	 * 項目を追加したら、getFieldValue, setFieldValueメソッドへの追加もお願いします。
	 */
	public enum Field {
		CODE,
		UNIQ_KEY,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		COMPANY_ID,
		PARENT_GROUP_ID,
		DESCRIPTION_L0,
		DESCRIPTION_L1,
		DESCRIPTION_L2,
		FILE_ATTACHMENT,
		RECORD_TYPE,
		USAGE,
		TAX_TYPE_BASE1_ID,
		TAX_TYPE_BASE2_ID,
		TAX_TYPE_BASE3_ID,
		DEBIT_ACCOUNT_CODE,
		DEBIT_ACCOUNT_NAME,
		DEBIT_SUB_ACCOUNT_CODE,
		DEBIT_SUB_ACCOUNT_NAME,
		ORDER,
		USE_FOREIGN_CURRENCY,
		FIXED_FOREIGN_CURRENCY_ID,
		FIXED_ALLOWANCE_SINGLE_AMOUNT,
		CHILD_EXP_TYPE_ID_LIST,
		FIXED_ALLOWANCE_OPTION_LIST
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** 使用用途 */
	public Enum UsageType {
		/** 通常費目 */
		Normal,
		/** 未払金用 */
		Payable,
		/** 支払方法用 */
		Payment
	}

	/**
	 * 値を指定してUsageを取得する。
	 * @param value 取得対象のMarkTypeの値
	 * @return 該当するUsage、ただしvalueが空文字の場合はnullを返却する
	 */
	private static ExpTypeEntity.UsageType getUsage(String value) {
		if (String.isBlank(value)) {
			return null;
		}

		ExpTypeEntity.UsageType retType = null;
		for (ExpTypeEntity.UsageType type : UsageType.values()) {
			if (value == type.name()) {
				retType = type;
			}
		}
		if (retType == null) {
			throw new App.ParameterException('指定された値のUsageが存在しません。(value=' + value);
		}

		return retType;
	}

	public enum ExtendedItemConfigField {
		EXTENDED_ITEM_DATE_ID_LIST,
		EXTENDED_ITEM_DATE_USED_IN_LIST,
		EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST,
		EXTENDED_ITEM_LOOKUP_ID_LIST,
		EXTENDED_ITEM_LOOKUP_USED_IN_LIST,
		EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST,
		EXTENDED_ITEM_PICKLIST_ID_LIST,
		EXTENDED_ITEM_PICKLIST_USED_IN_LIST,
		EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST,
		EXTENDED_ITEM_TEXT_ID_LIST,
		EXTENDED_ITEM_TEXT_USED_IN_LIST,
		EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, ExtendedItemConfigField> EXTENDED_ITEM_CONFIG_FIELD_MAP;
	static {
		final Map<String, ExtendedItemConfigField> fieldMap = new Map<String, ExtendedItemConfigField>();
		for (ExtendedItemConfigField f : ExtendedItemConfigField.values()) {
			fieldMap.put(f.name(), f);
		}
		EXTENDED_ITEM_CONFIG_FIELD_MAP = fieldMap;
	}

	/** Store the corresponding Accessor(Index and Extended Item Type) for the given string */
	private static final Map<String, ExtendedItemAccessor> FIELD_NAME_TO_ACCESSOR_MAP;
	static {
		FIELD_NAME_TO_ACCESSOR_MAP = new Map<String, ExtendedItemAccessor>();
		List<String> extendedItemTypeList = new List<String> {'Text', 'Picklist', 'Lookup', 'Date'};
		for (String extendedItemType : extendedItemTypeList) {
			for (Integer index = 0; index < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; index++) {
				String indexStr = String.valueOf(index + 1).leftPad(2, '0');
				FIELD_NAME_TO_ACCESSOR_MAP.put(
					String.format(EXTENDED_ITEM_ID_TEMPLATE, new List<String>{extendedItemType, indexStr}),
					new ExtendedItemAccessor(EXTENDED_ITEM_CONFIG_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'IdList'), index)
				);

				FIELD_NAME_TO_ACCESSOR_MAP.put(
					String.format(EXTENDED_ITEM_USED_IN_TEMPLATE, new List<String>{extendedItemType, indexStr}),
					new ExtendedItemAccessor(EXTENDED_ITEM_CONFIG_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'UsedInList'), index)
				);

				FIELD_NAME_TO_ACCESSOR_MAP.put(
					String.format(EXTENDED_ITEM_REQUIRED_FOR_TEMPLATE, new List<String>{extendedItemType, indexStr}),
					new ExtendedItemAccessor(EXTENDED_ITEM_CONFIG_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'RequiredForList'), index)
				);
			}
		}
	}

	/*
	 * Wrapper class for holding the Index and the field {@code ExtendedItemConfigField}
	 */
	private class ExtendedItemAccessor {
		ExtendedItemConfigField field {private set; get;}
		Integer index {private set; get;}

		public ExtendedItemAccessor(ExtendedItemConfigField field, Integer index) {
			this.field = field;
			this.index = index;
		}
	}

	public ExpTypeEntity() {
		super();
		isChangedFieldSet = new Set<ExpTypeEntity.Field>();
		isChangedExtendedItemConfigFieldSet = new Set<ExtendedItemConfigField>();

		this.expTypeChildIdList = new List<Id>();
		this.fixedAllowanceOptionEntityList = new List<ExpFixedAllowanceOptionEntity>();

		this.extendedItemDateIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextIdList = new Id[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemDateUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemDateRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemDateInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
	}

	/** 値を変更した項目のリスト */
	protected Set<ExpTypeEntity.Field> isChangedFieldSet;

	/** Set of Extended Item Config Fields which has been changed */
	private Set<ExtendedItemConfigField> isChangedExtendedItemConfigFieldSet;

	public class ParentGroup {
		public String code;
		public AppMultiString nameL;
	}

	/** 費目グループコード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	/** ユニークキー */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}

	/** 費目グループ名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	/** 費目グループ名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	/** 費目グループ名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/** 費目名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}

	/** 会社ID */
	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	/** 親費目グループID */
	public Id parentGroupId {
		get;
		set {
			parentGroupId = value;
			setChanged(Field.PARENT_GROUP_ID);
		}
	}

	/** 親費目グループ */
	public ParentGroup parentGroup;

	/** 説明(L0) */
	public String descriptionL0 {
		get;
		set {
			descriptionL0 = value;
			setChanged(Field.DESCRIPTION_L0);
		}
	}

	/** 説明(L1) */
	public String descriptionL1 {
		get;
		set {
			descriptionL1 = value;
			setChanged(Field.DESCRIPTION_L1);
		}
	}

	/** 説明(L2) */
	public String descriptionL2 {
		get;
		set {
			descriptionL2 = value;
			setChanged(Field.DESCRIPTION_L2);
		}
	}

	/** 説明(翻訳) */
	public AppMultiString descriptionL {
		get {return new AppMultiString(this.descriptionL0, this.descriptionL1, this.descriptionL2);}
	}

	/** 領収書添付 */
	public ExpFileAttachment fileAttachment {
		get;
		set {
			fileAttachment = value;
			setChanged(Field.FILE_ATTACHMENT);
		}
	}

	/** 明細タイプ */
	public ExpRecordType recordType {
		get;
		set {
			recordType = value;
			setChanged(Field.RECORD_TYPE);
		}
	}
	/** 費目用途 */
	public UsageType usage {
		get;
		set {
			usage = value;
			setChanged(Field.USAGE);
		}
	}
	public void setUsage(String value) {
		this.usage = getUsage(value);
	}

	/** 税区分1ID */
	public Id taxTypeBase1Id {
		get;
		set {
			taxTypeBase1Id = value;
			setChanged(Field.TAX_TYPE_BASE1_Id);
		}
	}

	/** 税区分2ID */
	public Id taxTypeBase2Id {
		get;
		set {
			taxTypeBase2Id = value;
			setChanged(Field.TAX_TYPE_BASE2_Id);
		}
	}

	/** 税区分3ID */
	public Id taxTypeBase3Id {
		get;
		set {
			taxTypeBase3Id = value;
			setChanged(Field.TAX_TYPE_BASE3_Id);
		}
	}

	/** 並び順 */
	public Integer order {
		get;
		set {
			order = value;
			setChanged(Field.ORDER);
		}
	}


	/** Debit Account Code */
	public String debitAccountCode {
		get;
		set {
			debitAccountCode = value;
			setChanged(Field.DEBIT_ACCOUNT_CODE);
		}
	}

	/** Debit Account Name */
	public String debitAccountName {
		get;
		set {
			debitAccountName = value;
			setChanged(Field.DEBIT_ACCOUNT_NAME);
		}
	}

	/** Debit Sub Account Code */
	public String debitSubAccountCode {
		get;
		set {
			debitSubAccountCode = value;
			setChanged(Field.DEBIT_SUB_ACCOUNT_CODE);
		}
	}

	/** Debit Sub Account Name */
	public String debitSubAccountName {
		get;
		set {
			debitSubAccountName = value;
			setChanged(Field.DEBIT_SUB_ACCOUNT_NAME);
		}
	}

	/** 外貨入力 */
	public Boolean useForeignCurrency {
		get;
		set {
			useForeignCurrency = value;
			setChanged(Field.USE_FOREIGN_CURRENCY);
		}
	}

	/** Currency Id for Fixed Foreign Currency */
	public Id fixedForeignCurrencyId {
		get;
		set {
			fixedForeignCurrencyId = value;
			setChanged(Field.FIXED_FOREIGN_CURRENCY_ID);
		}
	}

	/** Single Type Fixed Allowance Amount (only applicable for FixedAllowanceSingle Record Types) */
	public Decimal fixedAllowanceSingleAmount {
		get;
		set {
			fixedAllowanceSingleAmount = value;
			setChanged(Field.FIXED_ALLOWANCE_SINGLE_AMOUNT);
		}
	}

	/** Child ExpType Id List */
	public List<Id> expTypeChildIdList {
		get; private set;
	}

	/**
	 * Replace Child ExpType Id List
	 * @param expTypeChildIdList List of Child ExpType Ids to replace
	 */
	public void replaceExpTypeChildIdList(List<Id> expTypeChildIdList) {
		this.expTypeChildIdList = new List<Id>(expTypeChildIdList);
		setChanged(Field.CHILD_EXP_TYPE_ID_LIST);
	}

	/** Fixed Allowance Option List */
	public List<ExpFixedAllowanceOptionEntity> fixedAllowanceOptionEntityList {
		get; private set;
	}

	/**
	 * Replace Fixed Allowance Option Entity List
	 * @param fixedAllowanceOptionEntityList List of Fixed Allowance Option Entity to replace
	 */
	public void replaceFixedAllowanceOptionEntityList(List<ExpFixedAllowanceOptionEntity> fixedAllowanceOptionEntityList) {
		this.fixedAllowanceOptionEntityList = new List<ExpFixedAllowanceOptionEntity>(fixedAllowanceOptionEntityList);
		setChanged(Field.FIXED_ALLOWANCE_OPTION_LIST);
	}

	/** Date Extended Item ID */
	private List<Id> extendedItemDateIdList;
	public void setExtendedItemDateId(Integer index, Id idValue) {
		this.extendedItemDateIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST);
	}
	public Id getExtendedItemDateId(Integer index) {
		return this.extendedItemDateIdList[index];
	}

	/** Date Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemDateUsedInList;
	public void setExtendedItemDateUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemDateUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemDateUsedIn(Integer index) {
		return this.extendedItemDateUsedInList[index];
	}

	/** Date Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemDateRequiredForList;
	public void setExtendedItemDateRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemDateRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemDateRequiredFor(Integer index) {
		return this.extendedItemDateRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemDateInfoList;
	public void setExtendedItemDateInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemDateInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemDateInfo(Integer index) {
		return this.extendedItemDateInfoList[index];
	}

	/** Lookup Extended Item ID */
	private List<Id> extendedItemLookupIdList;
	public void setExtendedItemLookupId(Integer index, Id idValue) {
		this.extendedItemLookupIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST);
	}
	public Id getExtendedItemLookupId(Integer index) {
		return this.extendedItemLookupIdList[index];
	}

	/** Lookup Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemLookupUsedInList;
	public void setExtendedItemLookupUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemLookupUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemLookupUsedIn(Integer index) {
		return this.extendedItemLookupUsedInList[index];
	}

	/** Lookup Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemLookupRequiredForList;
	public void setExtendedItemLookupRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemLookupRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemLookupRequiredFor(Integer index) {
		return this.extendedItemLookupRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemLookupInfoList;
	public void setExtendedItemLookupInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemLookupInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemLookupInfo(Integer index) {
		return this.extendedItemLookupInfoList[index];
	}

	/** Picklist Extended Item ID */
	private List<Id> extendedItemPicklistIdList;
	public void setExtendedItemPicklistId(Integer index, Id idValue) {
		this.extendedItemPicklistIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST);
	}
	public Id getExtendedItemPicklistId(Integer index) {
		return this.extendedItemPicklistIdList[index];
	}

	/** Picklist Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemPicklistUsedInList;
	public void setExtendedItemPicklistUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemPicklistUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemPicklistUsedIn(Integer index) {
		return this.extendedItemPicklistUsedInList[index];
	}

	/** Picklist Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemPicklistRequiredForList;
	public void setExtendedItemPicklistRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemPicklistRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemPicklistRequiredFor(Integer index) {
		return this.extendedItemPicklistRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemPicklistInfoList;
	public void setExtendedItemPicklistInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemPicklistInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemPicklistInfo(Integer index) {
		return this.extendedItemPicklistInfoList[index];
	}

	/** Text Extended Item ID */
	private List<Id> extendedItemTextIdList;
	public void setExtendedItemTextId(Integer index, Id idValue) {
		this.extendedItemTextIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST);
	}
	public Id getExtendedItemTextId(Integer index) {
		return this.extendedItemTextIdList[index];
	}

	/** Text Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemTextUsedInList;
	public void setExtendedItemTextUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemTextUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemTextUsedIn(Integer index) {
		return this.extendedItemTextUsedInList[index];
	}

	/** Text Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemTextRequiredForList;
	public void setExtendedItemTextRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemTextRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemTextRequiredFor(Integer index) {
		return this.extendedItemTextRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemTextInfoList;
	public void setExtendedItemTextInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemTextInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemTextInfo(Integer index) {
		return this.extendedItemTextInfoList[index];
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/** Mark the specified field as changed */
	private void setChanged(ExtendedItemConfigField field) {
		this.isChangedExtendedItemConfigFieldSet.add(field);
	}

	/*
	 * Returns true is the specified field value has been changed.
	 * @param field Field to check if it has been changed
	 *
	 * @return true if the field has been changed. Otherwise, false.
	 */
	public Boolean isChanged(ExtendedItemConfigField field) {
		return isChangedExtendedItemConfigFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
		this.isChangedExtendedItemConfigFieldSet.clear();
	}

	/**
	 * ユニークキーを作成する
	 * インスタンス変数のcodeを指定しておくこと。
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]費目グループのユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]費目グループのユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + コード
		return companyCode + '-' + this.code;
	}

	/**
	 * エンティティの複製を作成する(IDはコピーされません)
	 */
	public ExpTypeEntity copy() {
		ExpTypeEntity copyEntity = new ExpTypeEntity();
		for (ValidPeriodEntityOld.Field field : ValidPeriodEntityOld.Field.values()) {
			copyEntity.setFieldValue(field, this.getFieldValue(field));
		}
		for (ExpTypeEntity.Field field : ExpTypeEntity.Field.values()) {
			copyEntity.setFieldValue(field, this.getFieldValue(field));
		}

		// Set Extended Items
		for (ExpTypeEntity.ExtendedItemConfigField field: ExpTypeEntity.ExtendedItemConfigField.values()) {
			for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				copyEntity.setExtendedItemValue(field, i, this.getExtendedItemValue(field, i));
			}
		}

		return copyEntity;
	}

	/*
	 * To allows child classes to determine if a field is a extended Item field
	 * @param fieldName the representative field name in String (e.g. extendedItemText01Id)
	 *
	 * @return true if the specified string is a extended item field. Otherwise, false.
	 */
	private Boolean isExtendedItemConfigField(String fieldName) {
		return EXTENDED_ITEM_CONFIG_PATTERN.matcher(fieldName).matches();
	}

	/*
	 * Returns extended item value using the field name in string format
	 * @param fieldName the representative field name in String (e.g. extendedItemText01Id)
	 *
	 * @return the value set to the field. If the field name is not a valid Extended Item field, {@code null} will be returned.
	 */
	private Object getExtendedItemValue(String fieldName) {
		ExtendedItemAccessor accessor = FIELD_NAME_TO_ACCESSOR_MAP.get(fieldName);
		if (accessor != null) {
			return getExtendedItemValue(accessor.field, accessor.index);
		}
		return null;
	}

	/**
	 * 指定された項目の値を取得する
	 * @param targetField 対象の項目
	 * @return 項目値
	 */
	public Object getFieldValue(ExpTypeEntity.Field targetField) {
		if (targetField == Field.CODE) {
			return this.code;
		}
		if (targetField == Field.UNIQ_KEY) {
			return this.uniqKey;
		}
		if (targetField == Field.NAME_L0) {
			return this.nameL0;
		}
		if (targetField == Field.NAME_L1) {
			return this.nameL1;
		}
		if (targetField == Field.NAME_L2) {
			return this.nameL2;
		}
		if (targetField == Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == Field.PARENT_GROUP_ID) {
			return this.parentGroupId;
		}
		if (targetField == Field.DESCRIPTION_L0) {
			return this.descriptionL0;
		}
		if (targetField == Field.DESCRIPTION_L1) {
			return this.descriptionL1;
		}
		if (targetField == Field.DESCRIPTION_L2) {
			return this.descriptionL2;
		}
		if (targetField == Field.ORDER) {
			return this.order;
		}
		if (targetField == Field.USE_FOREIGN_CURRENCY) {
			return this.useForeignCurrency;
		}
		if (targetField == Field.FIXED_FOREIGN_CURRENCY_ID) {
			return this.fixedForeignCurrencyId;
		}
		if (targetField == Field.FILE_ATTACHMENT) {
			return this.fileAttachment;
		}
		if (targetField == Field.RECORD_TYPE) {
			return this.RecordType;
		}
		if (targetField == Field.USAGE) {
			return this.usage;
		}
		if (targetField == Field.TAX_TYPE_BASE1_ID) {
			return this.taxTypeBase1Id;
		}
		if (targetField == Field.TAX_TYPE_BASE2_ID) {
			return this.taxTypeBase2Id;
		}
		if (targetField == Field.TAX_TYPE_BASE3_ID) {
			return this.taxTypeBase3Id;
		}
		if (targetField == Field.DEBIT_ACCOUNT_CODE) {
			return this.debitAccountCode;
		}
		if (targetField == Field.DEBIT_ACCOUNT_NAME) {
			return this.debitAccountName;
		}
		if (targetField == Field.DEBIT_SUB_ACCOUNT_CODE) {
			return this.debitSubAccountCode;
		}
		if (targetField == Field.DEBIT_SUB_ACCOUNT_NAME) {
			return this.debitSubAccountName;
		}
		if (targetField == Field.FIXED_ALLOWANCE_SINGLE_AMOUNT) {
			return this.fixedAllowanceSingleAmount;
		}
		if (targetField == Field.CHILD_EXP_TYPE_ID_LIST) {
			return this.expTypeChildIdList;
		}
		if (targetField == Field.FIXED_ALLOWANCE_OPTION_LIST) {
			return this.fixedAllowanceOptionEntityList;
		}

		// ここにきたらバグ
		throw new App.UnsupportedException('ExpTypeEntity.getFieldValue: 未対応の項目です。targetField=' + targetField);
	}

	/*
	 * Returns Extended Item value for the given {@code ExtendedItemConfigField} and and index.
	 *
	 * @param field Extended Item Field (Text Id, Text Value, Picklist Id, Picklist Value, etc)
	 * @param index the extended item index (starts from 0)
	 *
	 * @return the value set to the field. If the field name is not a valid Extended Item field, {@code null} will be returned.
	 */
	public Object getExtendedItemValue(ExtendedItemConfigField field, Integer index) {
		if (field == ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST) {
			return this.extendedItemDateIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST) {
			return this.extendedItemDateUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST) {
			return this.extendedItemDateRequiredForList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST) {
			return this.extendedItemLookupIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST) {
			return this.extendedItemLookupUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST) {
			return this.extendedItemLookupRequiredForList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST) {
			return this.extendedItemPicklistIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST) {
			return this.extendedItemPicklistUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST) {
			return this.extendedItemPicklistRequiredForList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST) {
			return this.extendedItemTextIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST) {
			return this.extendedItemTextUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST) {
			return this.extendedItemTextRequiredForList[index];
		}
		return null;
	}

	/**
	 * 指定された項目に値を設定する
	 * @param targetField 対象の項目
	 * @pram value 設定値
	 */
	public void setFieldValue(ExpTypeEntity.Field targetField, Object value) {
		if (targetField == Field.CODE) {
			this.code = (String)value;
		} else if (targetField == Field.UNIQ_KEY) {
			this.uniqKey = (String)value;
		} else if (targetField == Field.NAME_L0) {
			this.nameL0 = (String)value;
		} else if (targetField == Field.NAME_L1) {
			this.nameL1 = (String)value;
		} else if (targetField == Field.NAME_L2) {
			this.nameL2 = (String)value;
		} else if (targetField == Field.COMPANY_ID) {
			this.companyId = (Id)value;
		} else if (targetField == Field.PARENT_GROUP_ID) {
			this.parentGroupId = (Id)value;
		} else if (targetField == Field.DESCRIPTION_L0) {
			this.descriptionL0 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L1) {
			this.descriptionL1 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L2) {
			this.descriptionL2 = (String)value;
		} else if (targetField == Field.ORDER) {
			this.order = (Integer)value;
		} else if (targetField == Field.USE_FOREIGN_CURRENCY) {
			this.useForeignCurrency = (Boolean)value;
		} else if (targetField == Field.FIXED_FOREIGN_CURRENCY_ID) {
			this.fixedForeignCurrencyId = (Id) value;
		} else if (targetField == Field.FILE_ATTACHMENT) {
			this.fileAttachment = (ExpFileAttachment)value;
		} else if (targetField == Field.RECORD_TYPE) {
			this.RecordType = (ExpRecordType)value;
		} else if (targetField == Field.usage) {
			this.usage = (ExpTypeEntity.UsageType)value;
		} else if (targetField == Field.TAX_TYPE_BASE1_ID) {
			this.taxTypeBase1Id = (Id)value;
		} else if (targetField == Field.TAX_TYPE_BASE2_ID) {
			this.taxTypeBase2Id = (Id)value;
		} else if (targetField == Field.TAX_TYPE_BASE3_ID) {
			this.taxTypeBase3Id = (Id)value;
		} else if (targetField == Field.DEBIT_ACCOUNT_CODE) {
			this.debitAccountCode = (String) value;
		} else if (targetField == Field.DEBIT_ACCOUNT_NAME) {
			this.debitAccountName = (String) value;
		} else if (targetField == Field.DEBIT_SUB_ACCOUNT_CODE) {
			this.debitSubAccountCode = (String) value;
		} else if (targetField == Field.DEBIT_SUB_ACCOUNT_NAME) {
			this.debitSubAccountName = (String) value;
		} else if (targetField == Field.FIXED_ALLOWANCE_SINGLE_AMOUNT) {
			this.fixedAllowanceSingleAmount = (Decimal) value;
		} else if (targetField == Field.CHILD_EXP_TYPE_ID_LIST) {
			this.expTypeChildIdList = (List<Id>) value;
		} else if (targetField == Field.FIXED_ALLOWANCE_OPTION_LIST) {
			this.fixedAllowanceOptionEntityList = (List<ExpFixedAllowanceOptionEntity>) value;
		} else {
			// ここにきたらバグ
			throw new App.UnsupportedException('ExpTypeEntity.setFieldValue: 未対応の項目です。targetField=' + targetField);
		}
	}

	/*
	 * Set value to specific Extended Item
	 *
	 * @param targetField Indicate which extended item field
	 * @param index index of extended item, starting from 0
	 * @param value value to set to the specified Extended Item
	 */
	public void setExtendedItemValue(ExtendedItemConfigField targetField, Integer index, Object value) {
		if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST) {
			this.setExtendedItemDateId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST) {
			this.setExtendedItemDateUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST) {
			this.setExtendedItemDateRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST) {
			this.setExtendedItemLookupId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST) {
			this.setExtendedItemLookupUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST) {
			this.setExtendedItemLookupRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST) {
			this.setExtendedItemPicklistId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST) {
			this.setExtendedItemPicklistUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST) {
			this.setExtendedItemPicklistRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST) {
			this.setExtendedItemTextId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST) {
			this.setExtendedItemTextUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST) {
			this.setExtendedItemTextRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else {
			throw new App.UnsupportedException('ExpReportTypeEntity.setExtendedItemValue: 未対応の項目です。targetField=' + targetField);
		}
	}
}