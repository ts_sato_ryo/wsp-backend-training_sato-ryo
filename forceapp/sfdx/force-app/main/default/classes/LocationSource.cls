/**
 * @group 共通
 *
 * 位置情報の記録元
 */
public with sharing class LocationSource extends ValueObjectType {

	// Web
	public static final LocationSource WEB = new LocationSource('Web');
	// モバイルアプリ
	public static final LocationSource MOBILE = new LocationSource('Mobile');

	// 記録元のリスト
	public static final List<LocationSource> SOURCE_LIST;
	static {
		SOURCE_LIST = new List<LocationSource> {
			WEB,
			MOBILE
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private LocationSource(String value) {
		super(value);
	}


	/**
	 * 値からLocationSourceを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つLocationSource、ただし該当するLocationSourceが存在しない場合はnull
	 */
	public static LocationSource valueOf(String value) {
		LocationSource retType = null;
		if (String.isNotBlank(value)) {
			for (LocationSource type : SOURCE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// LocationSource以外の場合はfalse
		Boolean eq;
		if (compare instanceof LocationSource) {
			// 値が同じであればtrue
			if (this.value == ((LocationSource)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}
