/*
 * 準正常系Exception
 * マスタ情報不足によるエラーや設定による権限エラー等
 * 認識している範囲にて設定やデータによりエラーとなる場合に使用する
 */
public with sharing class ComSemiNormalException extends Exception {

}