/**
 * @group 勤怠
 *
 * @description 短時間勤務実績時間移行バッチスケジュールクラス
 */
public with sharing class AttShortenedWorkTimeBatchSchedule implements Schedulable {

	/**
	 * 短時間勤務実績時間移行バッチの実行スケジュールを登録する
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc) {

		AttShortenedWorkTimeMigrationBatch batch = new AttShortenedWorkTimeMigrationBatch();
		Database.executeBatch(batch);
	}
}