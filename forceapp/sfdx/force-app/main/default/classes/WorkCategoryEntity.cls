/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 工数
 *
 * 作業分類のエンティティ
 */
public with sharing class WorkCategoryEntity extends WorkCategoryGeneratedEntity {

	/** 作業分類コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** ユニークキーの最大文字数 */
	public static final Integer UNIQ_KEY_MAX_LENGTH = 31;
	/** 作業分類名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 表示順の最小値 */
	public static final Integer ORDER_MIN_VALUE = 1;
	/** 表示順の最大値 */
	public static final Integer ORDER_MAX_VALUE = 9999;

	/**
	 * コンストラクタ
	 */
	public WorkCategoryEntity() {
		super(new TimeWorkCategory__c());
	}

	/**
	 * コンストラクタ
	 */
	public WorkCategoryEntity(TimeWorkCategory__c sobj) {
		super(sobj);
	}

	/** 作業分類名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}
}