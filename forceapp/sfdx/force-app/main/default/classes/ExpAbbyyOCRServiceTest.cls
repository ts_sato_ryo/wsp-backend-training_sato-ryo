/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description description Test for OCR Service Class
 */
@IsTest
private class ExpAbbyyOCRServiceTest {

	static final String sampleTaskId = '0be6e9db-059f-430c-972f-31c343aed494';

	static AppContentVersionEntity createTestImage(){
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		FileService service = new FileService();
		String fileName = 'File.jpg';
		AppContentVersionEntity savedEntity = service.saveFile(fileName, bodyString, 'Receipt');
		return savedEntity;
	}

	/*
	 * Test access Abbyy Mock API (execute API) and execute OCR then will get accurate taskId
	 */
	@isTest
	static void executeOCRPositiveTest() {
		ExpTestData testData = new ExpTestData();
		AppContentVersionEntity entity = ExpAbbyyOCRServiceTest.createTestImage();
		Test.setMock(HttpCalloutMock.class, new ExpAbbyyOCRCalloutMock('', false));

		Test.startTest();
		String taskId = new ExpAbbyyOCRService().executeOCR(entity.id);
		Test.stopTest();

		System.assertEquals(sampleTaskId, taskId);
	}

	/*
	 * Test access Abbyy Mock API (execute API) and get Http error
	 */
	@isTest
	static void executeOCRGetHttpErrorNegativeTest() {
		ExpTestData testData = new ExpTestData();
		AppContentVersionEntity entity = ExpAbbyyOCRServiceTest.createTestImage();
		Test.setMock(HttpCalloutMock.class, new ExpAbbyyOCRCalloutMock('', true));

		Test.startTest();

		String taskId;
		try {
			taskId = new ExpAbbyyOCRService().executeOCR(entity.id);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Exp_Err_GetAbbyyAPIError + ' Error Code: 400', e.getMessage());
		}

		Test.stopTest();

		System.assertEquals(null, taskId);
	}

	/*
	 * Test access Abbyy Mock API (getStatus API) then will get InProgress status
	 */
	@isTest
	static void getOCRStatusWithStatusInProgressPositiveTest() {
		new ExpTestData();
		AppContentVersionEntity entity = ExpAbbyyOCRServiceTest.createTestImage();
		Test.setMock(HttpCalloutMock.class, new ExpAbbyyOCRCalloutMock('InProgress', false));

		Test.startTest();
		ExpAbbyyOCRService.OCRInfo ocrInfo = new ExpAbbyyOCRService().getOCRStatus(sampleTaskId);
		Test.stopTest();

		System.assertEquals('InProgress', ocrInfo.status);
	}

	/*
	 * Test access Abbyy Mock API (execute API) and get Http error
	 */
	@isTest
	static void getOCRStatusHttpErrorNegativeTest() {
		ExpTestData testData = new ExpTestData();
		AppContentVersionEntity entity = ExpAbbyyOCRServiceTest.createTestImage();
		Test.setMock(HttpCalloutMock.class, new ExpAbbyyOCRCalloutMock('InProgress', true));

		Test.startTest();
		ExpAbbyyOCRService.OCRInfo ocrInfo;
		try {
			ocrInfo = new ExpAbbyyOCRService().getOCRStatus(sampleTaskId);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Exp_Err_GetAbbyyAPIError + ' Error Code: 400', e.getMessage());
		}
		Test.stopTest();

		System.assertEquals(null, ocrInfo);
	}

	/*
	 * Test access Abbyy Mock API (getStatus API -> getResult xml API) then will get Complete status and OCR result
	 */
	@isTest
	static void getOCRStatusWithStatusCompletePositiveTest() {
		ExpTestData data = new ExpTestData();
		AppContentVersionEntity entity = ExpAbbyyOCRServiceTest.createTestImage();
		Test.setMock(HttpCalloutMock.class, new ExpAbbyyOCRCalloutMock('Completed', false));

		new ExpOCRRepository().saveEntityWithStatus(entity.id, sampleTaskId, data.company.Id, ExpOCRRepository.IN_PROGRESS);

		Test.startTest();
		ExpAbbyyOCRService.OCRInfo ocrInfo = new ExpAbbyyOCRService().getOCRStatus(sampleTaskId);
		Test.stopTest();

		System.assertEquals('Completed', ocrInfo.status);
		System.assertEquals('2018-07-27', ocrInfo.result.recordDate);
		System.assertEquals(5.27, ocrInfo.result.amount);
	}
}