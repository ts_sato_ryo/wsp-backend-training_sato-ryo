/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description 費目別費目のエンティティ Entity class for ExpTypeExpTypeLink
 */
public with sharing class ExpTypeExpTypeLinkEntity extends Entity {

	/** 項目の定義(変更管理で使用する) Field definition */
	public enum Field {
		EXP_TYPE_PARENT_ID,
		EXP_TYPE_CHILD_ID
	}

	/** 値を変更した項目のリスト Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;

	/** コンストラクタ Constructor */
	public ExpTypeExpTypeLinkEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** 費目別費目名(参照専用) ExpTypeExpTypeLink Name (reference only) */
	public String name { get; set; }
	
	/** 費目(親)ID Parent Expense Type Id */
	public Id expTypeParentId {
		get;
		set {
			expTypeParentId = value;
			setChanged(Field.EXP_TYPE_PARENT_ID);
		}
	}

	/** 費目(親)ID Child Expense Type Id */
	public Id expTypeChildId {
		get;
		set {
			expTypeChildId = value;
			setChanged(Field.EXP_TYPE_CHILD_ID);
		}
	}

	/** 費目(子)エンティティ(参照専用) Child ExpType Record Type (reference only) */
	public ExpTypeEntity expTypeChildEntity { get; set; }

	/**
	 * 項目の値を変更済みに設定する Mark field as changed
	 * @param Field 変更した項目 Field Changed field
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する Check if the value of the field has been changed
	 * @param field 取得対象の項目 Target field
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse True if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする Reset the changed status
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}
