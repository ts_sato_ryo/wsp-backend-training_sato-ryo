/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description Parent entity class for ExpRecordEntity and ExpRequestRecordEntity
 */
public abstract with sharing class ExpRecord extends Entity {

	/** Fields definition */
	public enum RecordBaseField {
		AMOUNT,
		EMPLOYEE_HISTORY_ID,
		LOCAL_AMOUNT,
		NAME,
		ORDER,
		RECEIPT_LIST,
		RECORD_DATE,
		RECORD_TYPE,
		TYPE_DATA,
		WITHOUT_TAX
	}

	/** Map of Fields with Name as the Key and the RecordBaseField enum as the value */
	public static final Map<String, RecordBaseField> RECORD_BASE_FIELD_MAP;
	static {
		final Map<String, RecordBaseField> fieldMap = new Map<String, RecordBaseField>();
		for (RecordBaseField f : RecordBaseField.values()) {
			fieldMap.put(f.name(), f);
		}
		RECORD_BASE_FIELD_MAP = fieldMap;
	}

	/** Set of fields whose value has changed */
	private Set<RecordBaseField> isChangedRecordBaseFieldSet;


	/** Constructor */
	protected ExpRecord() {
		isChangedRecordBaseFieldSet = new Set<RecordBaseField>();
	}

	/** Amount including tax */
	public Decimal amount {
		get;
		set {
			amount = value;
			setChanged(RecordBaseField.AMOUNT);
		}
	}

	/** Local Amount */
	public Decimal localAmount {
		get;
		set {
			localAmount = value;
			setChanged(RecordBaseField.LOCAL_AMOUNT);
		}
	}

	/** Employee ID */
	public Id employeeHistoryId {
		get;
		set {
			employeeHistoryId = value;
			setChanged(RecordBaseField.EMPLOYEE_HISTORY_ID);
		}
	}

	/** 領収書添付（参照のみ。値を変更してもDBに保存されない。） File Attachment (Reference only) */
	public ExpFileAttachment fileAttachment {get; set;}
	
	/** Name of record */
	public String name {
		get;
		set {
			name = value;
			setChanged(RecordBaseField.NAME);
		}
	}

	/** Order */
	public Integer order {
		get;
		set {
			order = value;
			setChanged(RecordBaseField.ORDER);
		}
	}

	/** Date expense will be used on */
	public AppDate recordDate {
		get;
		set {
			recordDate = value;
			setChanged(RecordBaseField.RECORD_DATE);
		}
	}

	/** Expense record type */
	public ExpRecordType recordType {
		get;
		set {
			recordType = value;
			setChanged(RecordBaseField.RECORD_TYPE);
		}
	}

	/** Amount excluding tax */
	public Decimal withoutTax {
		get;
		set {
			withoutTax = value;
			setChanged(RecordBaseField.WITHOUT_TAX);
		}
	}

	/** Own specific data */
	public String typeData {
		get;
		set {
			typeData = value;
			setChanged(RecordBaseField.TYPE_DATA);
		}
	}
	
	/** List of receipts */
	public List<AppContentDocumentEntity> receiptList {get; protected set;}

	
	/**
	 * Replace receipt list(duplicate list)
	 * @param receiptList
	 */
	public void replaceReceiptList(List<AppContentDocumentEntity> receiptList) {
		this.receiptList = new List<AppContentDocumentEntity>(receiptList);
		setChanged(RecordBaseField.RECEIPT_LIST);
	}

	/**
	 * Mark field as changed
	 * @param field Changed field
	 */
	private void setChanged(RecordBaseField field) {
		isChangedRecordBaseFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(RecordBaseField field) {
		return isChangedRecordBaseFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	protected virtual void resetChanged() {
		this.isChangedRecordBaseFieldSet.clear();
	}

	/*
	 * To allows child classes to determine if a field is an ExpRecord field
	 * @param fieldName the representative field name in String (e.g. expTypeId)
	 *
	 * @return true if the specified string is an ExpRecord field. Otherwise, false.
	 */
	protected Boolean isExpRecordField(String fieldName) {
		return ExpRecord.RECORD_BASE_FIELD_MAP.get(name) == null ? false : true;
	}

	/*
	 * Return the field value
	 * @param name Target field name
	 */
	public virtual Object getValue(String name) {
		RecordBaseField field = ExpRecord.RECORD_BASE_FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

        throw new App.ParameterException('Unknown field : ' + name);
	}

	/**
	 * Return the field value
	 * @param field Target field to get the value
	 * */
	public Object getValue(ExpRecord.RecordBaseField field) {
		if (field == ExpRecord.RecordBaseField.AMOUNT) {
			return amount;
		} else if (field == ExpRecord.RecordBaseField.EMPLOYEE_HISTORY_ID) {
			return employeeHistoryId;
		} else if (field == ExpRecord.RecordBaseField.LOCAL_AMOUNT) {
			return localAmount;
		} else if (field == ExpRecord.RecordBaseField.NAME) {
			return name;
		} else if (field == ExpRecord.RecordBaseField.ORDER) {
			return order;
		} else if (field == ExpRecord.RecordBaseField.RECEIPT_LIST) {
			return receiptList;
		} else if (field == ExpRecord.RecordBaseField.RECORD_DATE) {
			return recordDate;
		} else if (field == ExpRecord.RecordBaseField.RECORD_TYPE) {
			return recordType;
		} else if (field == ExpRecord.RecordBaseField.TYPE_DATA) {
			return typeData;
		} else if (field == ExpRecord.RecordBaseField.WITHOUT_TAX) {
			return withoutTax;
		}

		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}