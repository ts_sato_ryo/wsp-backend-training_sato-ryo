/**
 * 残業時間を計算するロジックテストクラス
 */
@isTest
private class AttCalcOverTimeLogicTest {
	class CalcOverInput extends AttCalcOverTimeLogic.Input {
		public CalcOverInput() {
			super();
			this.legalWorkTime = 8*60; // 法定８時間
			this.isWorkDay = true;
			this.startTime = 9*60; // 始業終業7時間
			this.endTime = 17*60;
			this.contractWorkTime = 7*60 + 30; // 所定7.5時間
			this.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		}
	}

	// 残業計算入力データを作成
	static CalcOverInput createInput(Integer inputStartTime, Integer inputEndTime) {
		CalcOverInput input = new CalcOverInput();
		input.inputStartTime = inputStartTime;
		input.inputEndTime = inputEndTime;
		input.inputRestRanges = AttCalcRangesDto.RS(input.contractRestRanges);
		return input;
	}

	// 控除と残業相殺しないテスト
	@isTest static void noOffsetLostAndOvertimeTest() {
		// 遅刻＋残業
		CalcOverInput input = createInput(10*60, 20*60);
		input.isNoOffsetOvertimeByUndertime = true;

		AttCalcOverTimeLogic.Output output = new AttCalcOverTimeLogic.Output();

		AttCalcOverTimeLogic logic = new AttCalcOverTimeLogic();
		logic.apply(output, input);
		system.assertEquals(1*60, output.lateArriveTime);
		system.assertEquals(1*60, output.lateArriveLostTime);

		AttCalcRangesDto cili = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.InContractInLegal);
		AttCalcRangesDto cilo = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.InContractOutLegal);
		AttCalcRangesDto coli = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.OutContractInLegal);
		AttCalcRangesDto colo = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.OutContractOutLegal);
		System.assertEquals(6.5*60, cili.total());
		System.assertEquals(0, cilo.total());
		System.assertEquals(1.5*60, coli.total());
		System.assertEquals(1*60, colo.total());
	}
	// 控除と残業相殺するテスト
	@isTest static void offsetLostAndOvertimeTest() {
		// 遅刻＋残業
		CalcOverInput input = createInput(10*60, 20*60);
		input.isNoOffsetOvertimeByUndertime = false;

		AttCalcOverTimeLogic.Output output = new AttCalcOverTimeLogic.Output();

		AttCalcOverTimeLogic logic = new AttCalcOverTimeLogic();
		logic.apply(output, input);
		system.assertEquals(1*60, output.lateArriveTime);
		system.assertEquals(0, output.lateArriveLostTime);

		AttCalcRangesDto cili = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.InContractInLegal);
		AttCalcRangesDto cilo = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.InContractOutLegal);
		AttCalcRangesDto coli = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.OutContractInLegal);
		AttCalcRangesDto colo = output.workTimeCategoryMap.get(AttCalcOverTimeLogic.OverTimeCategory.OutContractOutLegal);
		System.assertEquals(7*60+30, cili.total());
		System.assertEquals(0, cilo.total());
		System.assertEquals(0.5*60, coli.total());
		System.assertEquals(1*60, colo.total());
	}

	// 出退勤時間外休憩計算しない
	@isTest static void outWorkRangeRestTest() {
		CalcOverInput input = createInput(9*60, 18*60);
		// 出退勤時間外休憩追加
		input.inputRestRanges.insertAndMerge(AttCalcRangesDto.R(18*60, 18*60 + 30));
		input.inputRestRanges.insertAndMerge(AttCalcRangesDto.R(8*60, 9*60));
		AttCalcOverTimeLogic.Output output = new AttCalcOverTimeLogic.Output();

		AttCalcOverTimeLogic logic = new AttCalcOverTimeLogic();
		logic.apply(output, input);
		system.assertEquals(1*60, output.breakTime);
	}

	// 休日出勤の休憩計算追加
	@isTest static void restTimeOnHolidayTest() {
		CalcOverInput input = createInput(9*60, 18*60);
		input.isWorkDay = false;
		input.allowedWorkRanges = AttCalcRangesDto.RS(9*60, 18*60);
		input.inputRestRanges.insertAndMerge(AttCalcRangesDto.R(18*60, 18*60 + 30));
		input.inputRestRanges.insertAndMerge(AttCalcRangesDto.R(8*60, 9*60));
		AttCalcOverTimeLogic.Output output = new AttCalcOverTimeLogic.Output();

		AttCalcOverTimeLogic logic = new AttCalcOverTimeLogic();
		logic.apply(output, input);
		system.assertEquals(1*60, output.breakTime);
	}
	// 休日出勤の休憩計算追加
	@isTest static void restTimeOnLegalHolidayTest() {
		CalcOverInput input = createInput(9*60, 18*60);
		input.isWorkDay = false;
		input.isLegalHoliday = true;
		input.allowedWorkRanges = AttCalcRangesDto.RS(8*60, 12*60);
		input.inputRestRanges.insertAndMerge(AttCalcRangesDto.R(18*60, 18*60 + 30));
		input.inputRestRanges.insertAndMerge(AttCalcRangesDto.R(8*60, 9*60));
		AttCalcOverTimeLogic.Output output = new AttCalcOverTimeLogic.Output();

		AttCalcOverTimeLogic logic = new AttCalcOverTimeLogic();
		logic.apply(output, input);
		system.assertEquals(1*60, output.breakTime);
	}
	// 勤務換算休憩・無給休暇休憩が実労働時間から取り除かれること
	@isTest static void excludeRestsFromWorkTimeRangesTest() {
		CalcOverInput input = createInput(9*60, 18*60);
		input.isWorkDay = true;
		input.convertdRestRanges = AttCalcRangesDto.RS(10*60, 11*60); // 勤務換算時間(有給の時間単位休)を1時間取得
		input.unpaidLeaveRestRanges = AttCalcRangesDto.RS(16*60, 16*60 + 30); // 無給休暇休憩(無給の時間単位休)を30分取得
		AttCalcOverTimeLogic.Output output = new AttCalcOverTimeLogic.Output();

		AttCalcOverTimeLogic logic = new AttCalcOverTimeLogic();
		logic.apply(output, input);
		System.assertEquals(6*60 + 30, output.realWorkTime); // 実労働時間 = 労働時間 - 所定休憩 - 勤務換算休憩 - 無給休暇休憩
	}
}