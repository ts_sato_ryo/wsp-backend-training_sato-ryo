/**
 * @group 工数
 *
 * @description 工数内訳スナップショット作成バッチのスケジュールクラス
 */
public with sharing class TimeRecordItemSnapshotBatchSchedule implements Schedulable {
    
	/**
	 * 工数内訳スナップショット作成バッチの実行スケジュールを登録する
	 * @param sc スケジューラのコンテキスト
	 */
	public void execute(System.SchedulableContext sc) {
		TimeRecordItemSnapshotBatch batch = new TimeRecordItemSnapshotBatch();
		Id jobId = Database.executeBatch(batch, TimeRecordItemSnapshotBatch.SCOPE_LIMIT);
	}
}
