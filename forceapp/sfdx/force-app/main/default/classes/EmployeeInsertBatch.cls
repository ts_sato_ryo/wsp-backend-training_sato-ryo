/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 新規社員登録バッチ処理
 */
public with sharing class EmployeeInsertBatch implements Database.Batchable<sObject>, Database.Stateful {

	/** インポートバッチID */
	private Id importBatchId;

	/** 既存の会社データ */
	@TestVisible
	private Map<String, CompanyEntity> companyMap;
	/** 既存の社員データ */
	@TestVisible
	// キーは会社コード・社員コード
	private Map<List<String>, EmployeeBaseEntity> empMap;
	/** Expense Employee Group */
	@TestVisible
	private Map<List<String>, ExpEmployeeGroupEntity> expEmployeeGroupMap;
	/** 既存のユーザデータ */
	@TestVisible
	private Map<String, UserEntity> userMap;
	/** 既存のカレンダーデータ */
	@TestVisible
	// キーは会社コード・カレンダーコード
	private Map<List<String>, CalendarEntity> calendarMap;
	/** 既存の勤務体系データ */
	@TestVisible
	// キーは会社コード・勤務体系コード
	private Map<List<String>, AttWorkingTypeBaseEntity> workingTypeMap;
	/** 既存の36協定アラート設定データ */
	@TestVisible
	// キーは会社コード・36協定アラート設定コード
	private Map<List<String>, AttAgreementAlertSettingEntity> agreementAlertSettingMap;
	/** 既存の工数設定データ */
	@TestVisible
	// キーは会社コード・工数設定コード
	private Map<List<String>, TimeSettingBaseEntity> timeSettingMap;
	/** 既存の権限データ */
	@TestVisible
	// キーは会社コード・権限コード
	private Map<List<String>, PermissionEntity> permissionMap;

	/** Salesforceユーザ重複チェック用の社員データ */
	@TestVisible
	// キーは会社コード・社員コード、バリューはSFユーザ名
	private Map<List<String>, String> checkDuplicatedMap;

	/** Salesforceユーザ重複チェックの結果 */
	@TestVisible
	// キーは会社コード・社員コード、バリューはチェック結果(true:重複あり/false:重複なし)
	private Map<List<String>, Boolean> resultDuplicatedMap;

	/** 成功件数 */
	private Integer successCount = 0;
	/** 失敗件数 */
	private Integer errorCount = 0;
	/** 例外発生 */
	private Boolean exceptionOccurred = false;


	/**
	 * コンストラクタ
	 * @param importBatchId インポートバッチID
	 */
	public EmployeeInsertBatch(Id importBatchId) {
		this.importBatchId = importBatchId;
	}

	/**
	 * バッチ開始処理
	 * @param bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public Database.QueryLocator start(Database.BatchableContext bc) {
		// 社員インポートデータを取得
		String query = 'SELECT Id, ' +
							'Name,' +
							'ImportBatchId__c,' +
							'RevisionDate__c,' +
							'UserName__c,' +
							'Code__c,' +
							'CompanyCode__c,' +
							'DisplayName_L0__c,' +
							'DisplayName_L1__c,' +
							'DisplayName_L2__c,' +
							'ExpEmployeeGroupCode__c,' +
							'FirstName_L0__c,' +
							'FirstName_L1__c,' +
							'FirstName_L2__c,' +
							'LastName_L0__c,' +
							'LastName_L1__c,' +
							'LastName_L2__c,' +
							'MiddleName_L0__c,' +
							'MiddleName_L1__c,' +
							'MiddleName_L2__c,' +
							'HiredDate__c,' +
							'ValidFrom__c,' +
							// 'ValidTo__c,' +
							'ResignationDate__c,' +
							'CostCenterBaseCode__c,' +
							'DepartmentBaseCode__c,' +
							'Title_L0__c,' +
							'Title_L1__c,' +
							'Title_L2__c,' +
							'AdditionalTitle_L0__c,' +
							'AdditionalTitle_L1__c,' +
							'AdditionalTitle_L2__c,' +
							'ManagerBaseCode__c,' +
							'CalendarCode__c,' +
							'WorkingTypeBaseCode__c,' +
							'AgreementAlertSettingCode__c,' +
							'TimeSettingBaseCode__c,' +
							'PermissionCode__c,' +
							'HistoryComment__c,' +
							'UserData01__c,' +
							'UserData02__c,' +
							'UserData03__c,' +
							'UserData04__c,' +
							'UserData05__c,' +
							'UserData06__c,' +
							'UserData07__c,' +
							'UserData08__c,' +
							'UserData09__c,' +
							'UserData10__c,' +
							'Status__c,' +
							'Error__c ' +
						'FROM ComEmployeeImport__c ' +
						'WHERE ImportBatchId__c = :importBatchId ' +
						'AND Status__c IN (\'Waiting\', \'Error\') ' +
						'ORDER BY Code__c, RevisionDate__c, ResignationDate__c, CreatedDate';

		return Database.getQueryLocator(query);
	}

	/**
	 * バッチ実行処理
	 * @param bc バッチコンテキスト
	 * @param scope バッチ処理対象レコードのリスト
	 */
	public void execute(Database.BatchableContext bc, List<ComEmployeeImport__c> scope) {
		this.companyMap = new Map<String, CompanyEntity>();
		this.empMap = new Map<List<String>, EmployeeBaseEntity>();
		this.expEmployeeGroupMap = new Map<List<String>, ExpEmployeeGroupEntity>();
		this.userMap = new Map<String, UserEntity>();
		this.calendarMap = new Map<List<String>, CalendarEntity>();
		this.workingTypeMap = new Map<List<String>, AttWorkingTypeBaseEntity>();
		this.agreementAlertSettingMap = new Map<List<String>, AttAgreementAlertSettingEntity>();
		this.timeSettingMap = new Map<List<String>, TimeSettingBaseEntity>();
		this.permissionMap = new Map<List<String>, PermissionEntity>();
		this.checkDuplicatedMap = new Map<List<String>, String>();
		this.resultDuplicatedMap = new Map<List<String>, Boolean>();

		List<EmployeeImportEntity> empImpList = new List<EmployeeImportEntity>();
		List<EmployeeImportEntity> empImpExcludedList = new List<EmployeeImportEntity>();

		Set<String> companyCodeSet = new Set<String>();
		Set<String> empCodeSet = new Set<String>();
		Set<String> expEmpGroupCodeSet = new Set<String>();
		Set<String> userNameSet = new Set<String>();
		Set<String> calendarCodeSet = new Set<String>();
		Set<String> workingTypeCodeSet = new Set<String>();
		Set<String> agreementAlertSettingCodeSet = new Set<String>();
		Set<String> timeSettingCodeSet = new Set<String>();
		Set<String> permissionCodeSet = new Set<String>();

		for (ComEmployeeImport__c obj : scope) {

			EmployeeImportEntity entity = this.createEmpImpEntity(obj);

			// 退職日が設定されている場合は有効期間終了日の更新のみを行なうため
			// 本バッチでは処理対象外
			if (entity.resignationDate != null) {
				empImpExcludedList.add(entity);
				continue;
			}

			empImpList.add(entity);

			// 参照先のコードを取得する
			if (String.isNotEmpty(entity.userName)) {
				userNameSet.add(entity.userName);
			}
			if (String.isNotEmpty(entity.code)) {
				empCodeSet.add(entity.code);
			}
			if (String.isNotEmpty(entity.companyCode)) {
				companyCodeSet.add(entity.companyCode);
			}
			if (String.isNotEmpty(entity.expEmployeeGroupCode)) {
				expEmpGroupCodeSet.add(entity.expEmployeeGroupCode);
			}
			if (String.isNotEmpty(entity.managerBaseCode)) {
				empCodeSet.add(entity.managerBaseCode);
			}
			if (String.isNotEmpty(entity.calendarCode)) {
				calendarCodeSet.add(entity.calendarCode);
			}
			if (String.isNotEmpty(entity.workingTypeBaseCode)) {
				workingTypeCodeSet.add(entity.workingTypeBaseCode);
			}
			if (String.isNotEmpty(entity.agreementAlertSettingCode)) {
				agreementAlertSettingCodeSet.add(entity.agreementAlertSettingCode);
			}
			if (String.isNotEmpty(entity.timeSettingBaseCode)) {
				timeSettingCodeSet.add(entity.timeSettingBaseCode);
			}
			if (String.isNotEmpty(entity.permissionCode)) {
				permissionCodeSet.add(entity.permissionCode);
			}
			if (String.isNotEmpty(entity.userName) && String.isNotEmpty(entity.code) && String.isNotEmpty(entity.companyCode)) {
				this.checkDuplicatedMap.put(new List<String>{entity.companyCode, entity.code}, entity.userName);
			}
		}

		// 本バッチでの処理対象外の社員インポートデータを更新
		updateStatusOfExcludedImp(empImpExcludedList);

		// 参照先のデータを取得
		Map<Id, CompanyEntity> companyIdMap = new Map<Id, CompanyEntity>(); // キーは会社ID
		// 会社
		if (companyCodeSet.size() > 0) {
			CompanyRepository companyRepo = new CompanyRepository();
			for (CompanyEntity company : companyRepo.getEntityList(new List<String>(companyCodeSet))) {
				this.companyMap.put(company.code, company);
				companyIdMap.put(company.id, company);
			}
		}
		// 社員
		EmployeeService empService = new EmployeeService();
		if (empCodeSet.size() > 0) {
			for (ParentChildBaseEntity emp : empService.getEntityListByCode(new List<String>(empCodeSet))) {
				List<String> key = new List<String>{((EmployeeBaseEntity)emp).companyInfo.code, ((EmployeeBaseEntity)emp).code};
				this.empMap.put(key, (EmployeeBaseEntity)emp);
			}
		}
		// ユーザ
		if (userNameSet.size() > 0) {
			UserRepository userRepo = new UserRepository();
			for (UserEntity user : userRepo.getEntityListByUserName(new List<String>(userNameSet))) {
				this.userMap.put(user.userName, user);
			}
		}
		// カレンダー
		if (calendarCodeSet.size() > 0) {
			CalendarRepository calendarRepo = new CalendarRepository();
			for (CalendarEntity calendar : calendarRepo.getEntityListByCode(new List<String>(calendarCodeSet))) {
				List<String> key = new List<String>{calendar.companyCode, calendar.code};
				this.calendarMap.put(key, calendar);
			}
		}
		// 勤務体系
		if (workingTypeCodeSet.size() > 0) {
			AttWorkingTypeRepository workingTypeRepo = new AttWorkingTypeRepository();
			for (AttWorkingTypeBaseEntity workingType : workingTypeRepo.getEntityListByCode(new List<String>(workingTypeCodeSet), null)) {
				List<String> key = new List<String>{workingType.company.code, workingType.code};
				this.workingTypeMap.put(key, workingType);
			}
		}
		// 36協定アラート設定
		if (agreementAlertSettingCodeSet.size() > 0) {
			AttAgreementAlertSettingRepository agreementSettingRepo = new AttAgreementAlertSettingRepository();
			for (AttAgreementAlertSettingEntity alertSetting : agreementSettingRepo.getEntityListByCode(new List<String>(agreementAlertSettingCodeSet))) {
				List<String> key = new List<String>{alertSetting.company.code, alertSetting.code};
				this.agreementAlertSettingMap.put(key, alertSetting);
			}
		}
		// 工数設定
		if (timeSettingCodeSet.size() > 0) {
			TimeSettingRepository timeSettingRepo = new TimeSettingRepository();
			for (TimeSettingBaseEntity timeSetting : timeSettingRepo.getEntityListByCode(new List<String>(timeSettingCodeSet), null)) {
				List<String> key = new List<String>{timeSetting.company.code, timeSetting.code};
				this.timeSettingMap.put(key, timeSetting);
			}
		}

		// 権限
		if (permissionCodeSet.size() > 0) {
			PermissionRepository permissionRepo = new PermissionRepository();
			for (PermissionEntity permission : permissionRepo.getEntityListByCode(new List<String>(permissionCodeSet), null)) {
				// 更新対象の会社の権限の場合のみMapに追加
				CompanyEntity company = companyIdMap.get(permission.companyId);
				if (company != null) {
					List<String> key = new List<String>{company.code, permission.code};
					this.permissionMap.put(key, permission);
				}
			}
		}

		// employee group code map
		if (expEmpGroupCodeSet.size() > 0) {
			ExpEmployeeGroupRepository expEmpGroupRepo = new ExpEmployeeGroupRepository();
			for (ExpEmployeeGroupEntity expEmployeeGroupEntity : expEmpGroupRepo.getActiveEntityListByCodeSet(expEmpGroupCodeSet)) {
				CompanyEntity companyEntity = companyIdMap.get(expEmployeeGroupEntity.companyId);
				if (companyEntity != null) {
					List<String> key = new List<String>{companyEntity.code, expEmployeeGroupEntity.code};
					this.expEmployeeGroupMap.put(key, expEmployeeGroupEntity);
				}
			}
		}

		// Salesforceユーザの重複チェック
		this.resultDuplicatedMap = new EmployeeService().isSalesforceUserDuplicated(this.checkDuplicatedMap);

		Savepoint sp = Database.setSavepoint();

		Map<Id, EmployeeBaseEntity> empBaseMap = new Map<Id, EmployeeBaseEntity>();
		for (EmployeeImportEntity empImp : empImpList) {
			// バリデーション
			this.validateEmpImpEntity(empImp);

			// 既存データが存在する or エラーがある場合は何もしない
			List<String> empMapKey = new List<String>{empImp.companyCode, empImp.code};
			if (this.empMap.containsKey(empMapKey) || ImportStatus.ERROR.equals(empImp.status)) {
				continue;
			}

			// 登録用の社員エンティティを作成
			EmployeeBaseEntity empBase = this.createEmpEntity(empImp);
			empBaseMap.put(empImp.id, empBase);
		}

		// 社員エンティティを検証する
		validateEmployeeEntityList(empImpList, empBaseMap);

		// 新規データの登録
		for (EmployeeImportEntity empImp : empImpList) {
			try {
				// 既存データが存在する or エラーがある場合は何もしない
				List<String> empMapKey = new List<String>{empImp.companyCode, empImp.code};
				if (this.empMap.containsKey(empMapKey) || ImportStatus.ERROR.equals(empImp.status)) {
					continue;
				}

				// 登録用の社員エンティティを作成
				EmployeeBaseEntity empBase = empBaseMap.get(empImp.id);

				// 登録
				empService.saveNewEntity(empBase);

				empImp.status = ImportStatus.WAITING;
				empImp.error = null;

				// 同じコードのデータが複数件含まれているかもしれないので、Mapにも追加しておく
				this.empMap.put(empMapKey, empBase);
			} catch (App.BaseException e) {
				empImp.status = ImportStatus.ERROR;
				empImp.error = e.getMessage();
				this.errorCount++;
			} catch (Exception e) {
				Database.rollback(sp);
				empImp.status = ImportStatus.ERROR;
				empImp.error = e.getMessage();
				this.exceptionOccurred = true;
				break;
			}
		}

		// 社員インポートデータを更新
		EmployeeImportRepository empImpRepo = new EmployeeImportRepository();
		empImpRepo.saveEntityList(empImpList);
	}

	/**
	 * バッチ終了処理
	 * @param bc バッチコンテキスト
	 */
	public void finish(Database.BatchableContext bc) {
		// インポートバッチデータを更新
		ImportBatchRepository batchRepo = new ImportBatchRepository();
		ImportBatchEntity batch = batchRepo.getEntity(this.importBatchId);
		batch.status = this.exceptionOccurred ? ImportBatchStatus.FAILED : ImportBatchStatus.PROCESSING;
		batch.successCount += this.successCount;
		batch.failureCount += this.errorCount;
		batchRepo.saveEntity(batch);

		if (!this.exceptionOccurred) {
			if (!Test.isRunningTest()) {
				// 次の処理を呼び出し（既存部署更新）
				DepartmentUpdateBatch deptUpdate = new DepartmentUpdateBatch(importBatchId);
				Database.executeBatch(deptUpdate, ImportBatchService.BATCH_SIZE_DEPT_UPDATE);
			}
		}
	}

	//--------------------------------------------------------------------------------

	/**
	 * 社員インポートエンティティを作成する
	 * @param obj 社員インポートオブジェクトデータ
	 * @return 社員インポートエンティティ
	 */
	private EmployeeImportEntity createEmpImpEntity(ComEmployeeImport__c obj) {
		EmployeeImportEntity entity = new EmployeeImportEntity();

		entity.setId(obj.Id);
		entity.importBatchId = obj.ImportBatchId__c;
		entity.revisionDate = AppDate.valueOf(obj.RevisionDate__c);
		entity.userName = ImportBatchService.convertValue(obj.UserName__c);
		entity.code = ImportBatchService.convertValue(obj.Code__c);
		entity.companyCode = ImportBatchService.convertValue(obj.CompanyCode__c);
		entity.expEmployeeGroupCode = ImportBatchService.convertValue(obj.ExpEmployeeGroupCode__c);
		entity.displayName_L0 = ImportBatchService.convertValue(obj.DisplayName_L0__c);
		entity.displayName_L1 = ImportBatchService.convertValue(obj.DisplayName_L1__c);
		entity.displayName_L2 = ImportBatchService.convertValue(obj.DisplayName_L2__c);
		entity.firstName_L0 = ImportBatchService.convertValue(obj.FirstName_L0__c);
		entity.firstName_L1 = ImportBatchService.convertValue(obj.FirstName_L1__c);
		entity.firstName_L2 = ImportBatchService.convertValue(obj.FirstName_L2__c);
		entity.lastName_L0 = ImportBatchService.convertValue(obj.LastName_L0__c);
		entity.lastName_L1 = ImportBatchService.convertValue(obj.LastName_L1__c);
		entity.lastName_L2 = ImportBatchService.convertValue(obj.LastName_L2__c);
		entity.middleName_L0 = ImportBatchService.convertValue(obj.MiddleName_L0__c);
		entity.middleName_L1 = ImportBatchService.convertValue(obj.MiddleName_L1__c);
		entity.middleName_L2 = ImportBatchService.convertValue(obj.MiddleName_L2__c);
		entity.hiredDate = AppDate.valueOf(obj.HiredDate__c);
		entity.validFrom = AppDate.valueOf(obj.ValidFrom__c);
		// entity.validTo = AppDate.valueOf(obj.ValidTo__c);
		entity.resignationDate = AppDate.valueOf(obj.ResignationDate__c);
		entity.costCenterBaseCode = ImportBatchService.convertValue(obj.CostCenterBaseCode__c);
		entity.departmentBaseCode = ImportBatchService.convertValue(obj.DepartmentBaseCode__c);
		entity.title_L0 = ImportBatchService.convertValue(obj.Title_L0__c);
		entity.title_L1 = ImportBatchService.convertValue(obj.Title_L1__c);
		entity.title_L2 = ImportBatchService.convertValue(obj.Title_L2__c);
		entity.additionalTitle_L0 = ImportBatchService.convertValue(obj.AdditionalTitle_L0__c);
		entity.additionalTitle_L1 = ImportBatchService.convertValue(obj.AdditionalTitle_L1__c);
		entity.additionalTitle_L2 = ImportBatchService.convertValue(obj.AdditionalTitle_L2__c);
		entity.managerBaseCode = ImportBatchService.convertValue(obj.ManagerBaseCode__c);
		entity.calendarCode = ImportBatchService.convertValue(obj.CalendarCode__c);
		entity.workingTypeBaseCode = ImportBatchService.convertValue(obj.WorkingTypeBaseCode__c);
		entity.agreementAlertSettingCode = ImportBatchService.convertValue(obj.AgreementAlertSettingCode__c);
		entity.timeSettingBaseCode = ImportBatchService.convertValue(obj.TimeSettingBaseCode__c);
		entity.permissionCode = ImportBatchService.convertValue(obj.PermissionCode__c);
		entity.userData01 = ImportBatchService.convertValue(obj.UserData01__c);
		entity.userData02 = ImportBatchService.convertValue(obj.UserData02__c);
		entity.userData03 = ImportBatchService.convertValue(obj.UserData03__c);
		entity.userData04 = ImportBatchService.convertValue(obj.UserData04__c);
		entity.userData05 = ImportBatchService.convertValue(obj.UserData05__c);
		entity.userData06 = ImportBatchService.convertValue(obj.UserData06__c);
		entity.userData07 = ImportBatchService.convertValue(obj.UserData07__c);
		entity.userData08 = ImportBatchService.convertValue(obj.UserData08__c);
		entity.userData09 = ImportBatchService.convertValue(obj.UserData09__c);
		entity.userData10 = ImportBatchService.convertValue(obj.UserData10__c);
		entity.historyComment = ImportBatchService.convertValue(obj.HistoryComment__c);
		entity.resetChanged();
		entity.status = ImportStatus.WAITING;
		entity.error = null;

		return entity;
	}

	/**
	 * バリデーションを行う
	 * @param deptImp 社員インポートエンティティ
	 */
	 @TestVisible
	private void validateEmpImpEntity(EmployeeImportEntity empImp) {
		List<String> empMapKey = new List<String>{empImp.companyCode, empImp.code};

		// 社員コードが未設定
		if (String.isBlank(empImp.code)) {
			empImp.status = ImportStatus.Error;
			empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_EmployeeCode});
			this.errorCount++;
			return;
		}

		// 改訂日が未設定
		if (empImp.revisionDate == null) {
			empImp.status = ImportStatus.ERROR;
			empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_RevisionDate});
			this.errorCount++;
			return;
		}

		// Salesforceユーザ名が未設定（新規の場合のみ）
		if (!this.empMap.containsKey(empMapKey) && String.isBlank(empImp.userName)) {
			empImp.status = ImportStatus.Error;
			empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_UserName});
			this.errorCount++;
			return;
		}
		// Salesforceユーザ名が存在しない
		if (String.isNotBlank(empImp.userName) && !this.userMap.containsKey(empImp.userName)) {
			empImp.status = ImportStatus.Error;
			empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_UserName});
			this.errorCount++;
			return;
		}
		// Salesforceユーザ名が重複している
		if (this.resultDuplicatedMap.containsKey(empMapKey) && this.resultDuplicatedMap.get(empMapKey)) {
			empImp.status = ImportStatus.Error;
			empImp.error = ComMessage.msg().Admin_Err_SfdcUserDuplicate;
			this.errorCount++;
			return;
		}

		// 会社コードが未設定
		if (String.isBlank(empImp.companyCode)) {
			empImp.status = ImportStatus.ERROR;
			empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			this.errorCount++;
			return;
		}
		// 指定した会社が存在しない
		if (!this.companyMap.containsKey(empImp.companyCode)) {
			empImp.status = ImportStatus.ERROR;
			empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode});
			this.errorCount++;
			return;
		}
		 CompanyEntity companyEntity = this.companyMap.get(empImp.companyCode);
		 // Company use Expense module & it's not existing employee
		 if ((companyEntity.useExpense) && !this.empMap.containsKey(empMapKey)) {
			 if (String.isEmpty(empImp.expEmployeeGroupCode)) {
				 //employee group code is empty throw error
				 empImp.status = ImportStatus.ERROR;
				 empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Exp_Lbl_EmployeeGroup});
				 this.errorCount++;
				 return;
			 } else {
				 // Assigned Employee Group Not found for current company
				 if (!this.expEmployeeGroupMap.containsKey(new List<String>{empImp.companyCode, empImp.expEmployeeGroupCode})) {
					 empImp.status = ImportStatus.ERROR;
					 empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Exp_Lbl_EmployeeGroup});
					 this.errorCount++;
					 return;
				 }
			 }
		 }

		// 社員氏名(L0)が未設定（新規の場合のみ）
		if (!this.empMap.containsKey(empMapKey) && String.isBlank(empImp.displayName_L0)) {
			empImp.status = ImportStatus.Error;
			empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_DisplayName});
			this.errorCount++;
			return;
		}

		// 姓(L0)が未設定（新規の場合のみ）
		if (!this.empMap.containsKey(empMapKey) && String.isBlank(empImp.lastName_L0)) {
			empImp.status = ImportStatus.Error;
			empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_LastName});
			this.errorCount++;
			return;
		}

		// カレンダー
		if (String.isNotBlank(empImp.calendarCode)) {
			List<String> key = new List<String>{empImp.companyCode, empImp.calendarCode};

			// 存在しない
			if (!this.calendarMap.containsKey(key)) {
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CalendarCode});
				this.errorCount++;
				return;
			}
		}

		// 勤務体系
		if (String.isNotBlank(empImp.workingTypeBaseCode)) {
			List<String> key = new List<String>{empImp.companyCode, empImp.workingTypeBaseCode};

			// 存在しない
			if (!this.workingTypeMap.containsKey(key)) {
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_WorkingTypeCode});
				this.errorCount++;
				return;
			}
			// 有効開始日 > 改訂日
			if (this.workingTypeMap.containsKey(key) &&
				this.workingTypeMap.get(key).getSuperHistoryByDate(empImp.revisionDate) == null)
			{
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_WorkingTypeCode});
				return;
			}
		}

		// 36協定アラート設定
		if (String.isNotBlank(empImp.agreementAlertSettingCode)) {
			List<String> key = new List<String>{empImp.companyCode, empImp.agreementAlertSettingCode};

			// 存在しない
			if (!this.agreementAlertSettingMap.containsKey(key)) {
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_AgreementAlertSettingCode});
				this.errorCount++;
				return;
			}
			// 有効開始日 > 改訂日
			if (this.agreementAlertSettingMap.containsKey(key) &&
				this.agreementAlertSettingMap.get(key).validFrom.compareTo(empImp.revisionDate) > 0)
			{
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_AgreementAlertSettingCode});
				return;
			}
		}

		// 工数設定
		if (String.isNotBlank(empImp.timeSettingBaseCode)) {
			List<String> key = new List<String>{empImp.companyCode, empImp.timeSettingBaseCode};

			// 存在しない
			if (!this.timeSettingMap.containsKey(key)) {
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_TimeSettingCode});
				this.errorCount++;
				return;
			}
			// 有効開始日 > 改訂日
			if (this.timeSettingMap.containsKey(key) &&
				this.timeSettingMap.get(key).getSuperHistoryByDate(empImp.revisionDate) == null)
			{
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_TimeSettingCode});
				return;
			}
		}

		// 権限が未設定（新規の場合のみ）
		if (!this.empMap.containsKey(empMapKey) && String.isBlank(empImp.permissionCode)) {
			empImp.status = ImportStatus.Error;
			empImp.error = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_PermissionCode});
			this.errorCount++;
			return;
		}

		// 権限
		if (String.isNotBlank(empImp.permissionCode)) {
			List<String> key = new List<String>{empImp.companyCode, empImp.permissionCode};

			// 存在しない
			if (!this.permissionMap.containsKey(key)) {
				empImp.status = ImportStatus.Error;
				empImp.error = ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_PermissionCode});
				this.errorCount++;
				return;
			}
		}
	}

	/**
	 * 社員エンティティを作成する（ベース＋履歴）
	 * @param empImp 社員インポートエンティティ
	 * @return 社員ベースエンティティ
	 */
	private EmployeeBaseEntity createEmpEntity(EmployeeImportEntity empImp) {
		// 履歴（相互参照項目は登録しない）
		EmployeeHistoryEntity history = new EmployeeHistoryEntity();
		history.titleL0 = empImp.Title_L0;
		history.titleL1 = empImp.Title_L1;
		history.titleL2 = empImp.Title_L2;
		history.additionalTitleL0 = empImp.additionalTitle_L0;
		history.additionalTitleL1 = empImp.additionalTitle_L1;
		history.additionalTitleL2 = empImp.additionalTitle_L2;
		if (String.isNotBlank(empImp.calendarCode)) {
			List<String> calendarMapKey = new List<String>{empImp.companyCode, empImp.calendarCode};
			history.calendarId = this.calendarMap.get(calendarMapKey).id;
		}
		if (String.isNotBlank(empImp.expEmployeeGroupCode)) {
			List<String> expEmpGroupMapKey = new List<String>{empImp.companyCode, empImp.expEmployeeGroupCode};
			if (this.expEmployeeGroupMap.containsKey(expEmpGroupMapKey)) {
				history.expEmployeeGroupId = this.expEmployeeGroupMap.get(expEmpGroupMapKey).id;
			}
		}
		if (String.isNotBlank(empImp.workingTypeBaseCode)) {
			List<String> workingTypeMapKey = new List<String>{empImp.companyCode, empImp.workingTypeBaseCode};
			history.workingTypeId = this.workingTypeMap.get(workingTypeMapKey).id;
		}
		if (String.isNotBlank(empImp.agreementAlertSettingCode)) {
			List<String> agreementAlertSettingMapKey = new List<String>{empImp.companyCode, empImp.agreementAlertSettingCode};
			history.agreementAlertSettingId = this.agreementAlertSettingMap.get(agreementAlertSettingMapKey).id;
		}
		if (String.isNotBlank(empImp.timeSettingBaseCode)) {
			List<String> timeSettingMapKey = new List<String>{empImp.companyCode, empImp.timeSettingBaseCode};
			history.timeSettingId = this.timeSettingMap.get(timeSettingMapKey).id;
		}
		if (String.isNotBlank(empImp.permissionCode)) {
			List<String> permissionMapKey = new List<String>{empImp.companyCode, empImp.permissionCode};
			history.permissionId = this.permissionMap.get(permissionMapKey).id;
		}
		history.userData01 = empImp.userData01;
		history.userData02 = empImp.userData02;
		history.userData03 = empImp.userData03;
		history.userData04 = empImp.userData04;
		history.userData05 = empImp.userData05;
		history.userData06 = empImp.userData06;
		history.userData07 = empImp.userData07;
		history.userData08 = empImp.userData08;
		history.userData09 = empImp.userData09;
		history.userData10 = empImp.userData10;

		history.historyComment = empImp.historyComment;
		history.validFrom = empImp.revisionDate;
		history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;

		// ベース
		EmployeeBaseEntity base = new EmployeeBaseEntity();
		// nameはEmployeeServiceで保存する際に設定される
		// base.name = empImp.LastName_L0;
		base.userId = this.userMap.get(empImp.userName).id;
		base.code = empImp.code;
		base.uniqKey = base.createUniqKey(empImp.companyCode, base.code);
		base.companyId = this.companyMap.get(empImp.companyCode).id;
		base.displayNameL0 = empImp.DisplayName_L0;
		base.displayNameL1 = empImp.DisplayName_L1;
		base.displayNameL2 = empImp.DisplayName_L2;
		base.firstNameL0 = empImp.FirstName_L0;
		base.firstNameL1 = empImp.FirstName_L1;
		base.firstNameL2 = empImp.FirstName_L2;
		base.lastNameL0 = empImp.LastName_L0;
		base.lastNameL1 = empImp.LastName_L1;
		base.lastNameL2 = empImp.LastName_L2;
		base.middleNameL0 = empImp.MiddleName_L0;
		base.middleNameL1 = empImp.MiddleName_L1;
		base.middleNameL2 = empImp.MiddleName_L2;
		base.hiredDate = empImp.hiredDate;
		base.validTo = history.validTo;	// TODO base.validToは退職日なのであれば、-1日する必要がある・・
		// 履歴をベースに追加
		base.addHistory(history);

		return base;
	}

	/**
	 * インポートデータから作成した社員エンティティの値を検証する
	 * 検証結果はempImpListに設定する
	 *
	 * @param empImpList インポートデータ
	 * @param empBaseMap インポートデータから作成した社員ベースエンティティのマップ。キーはEmployeeImportEntity.id
	 */
	private void validateEmployeeEntityList(List<EmployeeImportEntity> empImpList,
			Map<Id, EmployeeBaseEntity> empBaseMap) {

		// 社員履歴を検証する
		List<EmployeeService.ValidateHistoryParam> empHistoryParamList = new List<EmployeeService.ValidateHistoryParam>();
		List<EmployeeImportEntity> targetEmpImpList = new List<EmployeeImportEntity>();
		for (EmployeeImportEntity empImp : empImpList) {

			// 既存データが存在する or エラーがある場合は何もしない
			List<String> empMapKey = new List<String>{empImp.companyCode, empImp.code};
			if (this.empMap.containsKey(empMapKey) || ImportStatus.ERROR.equals(empImp.status)) {
				continue;
			}

			EmployeeBaseEntity empBase = empBaseMap.get(empImp.id);
			EmployeeHistoryEntity empHistory = empBase.getHistory(0);

			empHistoryParamList.add(new EmployeeService.ValidateHistoryParam(empBase.companyId, empHistory));
			targetEmpImpList.add(empImp);
		}

		if (! targetEmpImpList.isEmpty()) {
			List<EmployeeService.ValidateResult> historyResultList =
					new EmployeeService().validateHistoryListUsingOtherData(empHistoryParamList);
			for (Integer i = 0; i < historyResultList.size(); i++) {
				EmployeeService.ValidateResult result = historyResultList[i];
				if (result.hasError) {
					EmployeeImportEntity empImp = targetEmpImpList[i];
					empImp.status = ImportStatus.ERROR;
					empImp.error = result.exceptionList[0].getMessage();
					this.errorCount++;
				}
			}
		}
	}

	/**
	 * 本バッチで処理対象外データのステータスを更新する
	 */
	private void updateStatusOfExcludedImp(List<EmployeeImportEntity> empImpExcludedList) {

		// ステータスを設定
		for (EmployeeImportEntity empImp : empImpExcludedList) {
			empImp.status = ImportStatus.WAITING;
			empImp.error = null;
		}

		// 社員インポートデータを更新
		EmployeeImportRepository empImpRepo = new EmployeeImportRepository();
		empImpRepo.saveEntityList(empImpExcludedList);
	}
}