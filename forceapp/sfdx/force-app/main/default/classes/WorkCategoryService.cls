/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 作業分類に関するサービスを提供するクラス
 */
public with sharing class WorkCategoryService {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * 作業分類を保存する
	 * @param savedEntity 作業分類エンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveWorkCategory(WorkCategoryEntity savedEntity) {
		WorkCategoryRepository rep = new WorkCategoryRepository();

		// 保存用のエンティティを作成
		WorkCategoryEntity entity;
		if (savedEntity.id == null) {
			// 新規の場合
			entity = savedEntity;
		} else {
			// 更新の場合は既存の値を取得する
			entity = rep.getEntity(savedEntity.id);
			if (entity == null) {
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
			}
			// 更新値を上書きする
			for (Schema.SObjectField field : savedEntity.getChangedFieldSet()) {
				entity.setFieldValue(field, savedEntity.getFieldValue(field));
			}
		}

		// ユニークキーを設定する
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		entity.uniqKey = entity.createUniqKey(company.code);

		// バリデーション実行
		Validator.Result validResult = (new TimeConfigValidator.WorkCategoryValidator(entity)).validate();
		if (! validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// コードの重複をチェックする
		WorkCategoryEntity duplicateCodeEntity = rep.getEntityByCode(entity.code, entity.companyId);
		if ((duplicateCodeEntity != null) && (duplicateCodeEntity.id != entity.id)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		return rep.saveEntity(entity);
	}
}