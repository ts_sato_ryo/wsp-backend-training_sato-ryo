/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description アクセス権限のリポジトリ
 */
public with sharing class PermissionRepository extends LogicalDeleteRepository {
	/** 取得対象の項目(アクセス権限) */
	private static final Set<Schema.SObjectField> GET_PERMISSION_FIELD_SET;
	static {
		GET_PERMISSION_FIELD_SET = new Set<Schema.SObjectField> {
		 ComPermission__c.Id,
		 ComPermission__c.Name,
		 ComPermission__c.Code__c,
		 ComPermission__c.UniqKey__c,
		 ComPermission__c.CompanyId__c,
		 ComPermission__c.Name_L0__c,
		 ComPermission__c.Name_L1__c,
		 ComPermission__c.Name_L2__c,
		 ComPermission__c.Removed__c,
		 // 勤怠トランザクション系権限
		 ComPermission__c.ViewAttTimeSheetByDelegate__c,
		 ComPermission__c.EditAttTimeSheetByDelegate__c,
		 // 勤怠申請系権限
		 ComPermission__c.ApproveAttDailyRequestByDelegate__c,
		 ComPermission__c.ApproveAttRequestByDelegate__c,
		 ComPermission__c.ApproveSelfAttDailyRequestByEmployee__c,
		 ComPermission__c.ApproveSelfAttRequestByEmployee__c,
		 ComPermission__c.CancelAttApprovalByDelegate__c,
		 ComPermission__c.CancelAttApprovalByEmployee__c,
		 ComPermission__c.CancelAttDailyApprovalByDelegate__c,
		 ComPermission__c.CancelAttDailyApprovalByEmployee__c,
		 ComPermission__c.CancelAttDailyRequestByDelegate__c,
		 ComPermission__c.CancelAttRequestByDelegate__c,
		 ComPermission__c.SubmitAttDailyRequestByDelegate__c,
		 ComPermission__c.SubmitAttRequestByDelegate__c,
		 // 工数トランザクション系権限
		 ComPermission__c.ViewTimeTrackByDelegate__c,
		 // 管理系権限
		 ComPermission__c.ManageAttAgreementAlertSetting__c,
		 ComPermission__c.ManageAttLeave__c,
		 ComPermission__c.ManageAttLeaveGrant__c,
		 ComPermission__c.ManageAttLeaveOfAbsence__c,
		 ComPermission__c.ManageAttLeaveOfAbsenceApply__c,
		 ComPermission__c.ManageAttPattern__c,
		 ComPermission__c.ManageAttPatternApply__c,
		 ComPermission__c.ManageAttShortTimeWorkSetting__c,
		 ComPermission__c.ManageAttShortTimeWorkSettingApply__c,
		 ComPermission__c.ManageAttWorkingType__c,
		 ComPermission__c.ManageCalendar__c,
		 ComPermission__c.ManageDepartment__c,
		 ComPermission__c.ManageEmployee__c,
		 ComPermission__c.ManageJob__c,
		 ComPermission__c.ManageJobType__c,
		 ComPermission__c.ManageMobileSetting__c,
		 ComPermission__c.ManageOverallSetting__c,
		 ComPermission__c.ManagePermission__c,
		 ComPermission__c.ManagePlannerSetting__c,
		 ComPermission__c.ManageTimeSetting__c,
		 ComPermission__c.ManageTimeWorkCategory__c,
		 // 管理系権限（経費） / admin permission (Expense)
		 ComPermission__c.ManageExpTypeGroup__c,
		 ComPermission__c.ManageExpType__c,
		 ComPermission__c.ManageExpTaxType__c,
		 ComPermission__c.ManageExpSetting__c,
		 ComPermission__c.ManageExpExchangeRate__c,
		 ComPermission__c.ManageExpAccountingPeriod__c,
		 ComPermission__c.ManageExpReportType__c,
		 ComPermission__c.ManageExpCostCenter__c,
		 ComPermission__c.ManageExpVendor__c,
		 ComPermission__c.ManageExpExtendedItem__c,
		 ComPermission__c.ManageExpEmployeeGroup__c,
		 ComPermission__c.ManageExpCustomHint__c,

		 ComPermission__c.SwitchCompany__c
		};
	}
	/** @description 検索フィルタ */
	public class SearchFilter {
		/** 会社ID */
		public Id companyId;
		/** 権限ID（複数） */
		public List<Id> permissionIds;
		/** 権限コード（複数） */
		public List<String> permissionCodes;
		/** 論理削除されている権限を取得対象にする場合はtrue */
		public Boolean includeRemoved = false;
	}
	/** 更新用でクエリを実行しない */
	private static final Boolean NOT_FOR_UPDATE = false;

	/**
	 * @description 指定した権限IDの権限を取得する
	 * @param 取得対象の権限ID
	 * @return 権限　(取得対象が存在しない場合は、null)
	 */
	public PermissionEntity getEntityById(Id id) {
		SearchFilter filter = new SearchFilter();
		filter.permissionIds = new List<Id>{id};
		List<PermissionEntity> retList = searchEntity(filter, NOT_FOR_UPDATE);
		return retList.isEmpty() ? null : retList[0];
	}

	/**
	 * @description 指定した権限コードの権限を取得する
	 * @param 取得対象の権限コード
	 * @return 権限のリスト
	 */
	public List<PermissionEntity> getEntityListByCode(List<String> codes,Id companyId) {
		SearchFilter filter = new SearchFilter();
		filter.permissionCodes = codes;
		filter.companyId = companyId;
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * @description 指定した権限コードの権限を取得する
	 * @param 検索条件
	 * @return 権限のリスト
	 */
	public List<PermissionEntity> searchEntityList(PermissionRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 権限を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue
	 * @return　権限のリスト
	 */
	@testVisible
	private List<PermissionEntity> searchEntity(PermissionRepository.SearchFilter filter, Boolean forUpdate) {
		//SOQLを作成する
		// - 取得項目
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_PERMISSION_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// - フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> permissionIds = filter.permissionIds;
		final List<String> permissionCodes = filter.permissionCodes;
		final Id companyId = filter.companyId;
		final Boolean includeRemoved = filter.includeRemoved;

		if (filter.permissionIds != null) {
			condList.add(getFieldName(ComPermission__c.Id) + ' IN :permissionIds');
		}
		if (filter.permissionCodes != null) {
			condList.add(getFieldName(ComPermission__c.Code__c) + ' IN :permissionCodes');
		}
		if (filter.companyId != null) {
			condList.add(getFieldName(ComPermission__c.CompanyId__c) + ' = :companyId');
		}
		// - 論理削除されているレコードを検索対象外にする
		if (includeRemoved != true) {
			condList.add(getFieldName(ComPermission__c.Removed__c) + ' = false');
		}
		String soql =
			'SELECT ' + String.join(selectFldList, ', ')
			+ ' FROM ' + ComPermission__c.SObjectType.getDescribe().getName()
			+ buildWhereString(condList);
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(ComPermission__c.Code__c);
		}

		// クエリ実行
		System.debug('PermissionRepository: SOQL==>' + soql);
		AppLimits.getInstance().setCurrentQueryCount();
		List< ComPermission__c> permissionList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComPermission__c.SObjectType);

		// 結果からEntityを作成する
		List<PermissionEntity> entityList = new List<PermissionEntity>();
		for (ComPermission__c permission : permissionList) {
			entityList.add(new PermissionEntity(permission));
		}
		return entityList;
	}

	/**
	 * 権限を保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntity(PermissionEntity entity) {
		return saveEntityList(new List<PermissionEntity>{entity}, true);
	}

	/**
	 * 権限を保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<PermissionEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * 権限を保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<PermissionEntity> entityList, Boolean allOrNone) {

		// オブジェクトをエンティティから作成する
		List<ComPermission__c> sObjList = new List<ComPermission__c>();
		for (PermissionEntity entity : entityList) {
			sObjList.add(entity.createSObject());
		}

		// 保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sObjList, allOrNone);

		return resultFactory.createSaveResult(recResList);
	}
}