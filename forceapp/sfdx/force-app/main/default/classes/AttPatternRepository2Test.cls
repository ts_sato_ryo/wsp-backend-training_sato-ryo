/**
 * @group 勤怠
 *
 * @description 勤務パターンリポジトリのテスト
 */
@isTest
private class AttPatternRepository2Test {

	/**
	 * @description テストデータクラス
	 */
	private class RepoTestData {

		public ComCompany__c companyObj;

		public ComCountry__c countryObj;

		public RepoTestData() {
			this.countryObj = ComTestDataUtility.createCountry('Japan');
			this.companyObj = ComTestDataUtility.createCompany('Test', this.countryObj.Id);
		}

	/**
	 * @description 会社を作成する
	 * @param name 会社名
	 * @return 作成した会社
	 */
	public CompanyEntity createCompany(String name) {
		Id companyId = ComTestDataUtility.createCompany(name, this.countryObj.Id).Id;
		return new CompanyRepository().getEntity(companyId);
	}

	/**
	 * @description 勤務パターンを作成する
	 * @param name 勤務パターン名
	 * @param companyId 会社ID
	 * @param size 作成する勤務パターン数
	 * @return 勤務パターン
	 */
	public List<AttPattern__c> createAttPatternObjList(String name, Id companyId, Integer size) {
			return ComTestDataUtility.createAttPattern(name, companyId, size);
		}
	}

	/**
	 * @description searchEntityListのテスト
	 * すべての項目が取得できていることを確認する
	 */
	@isTest static void searchEntityListTestAllField() {
		RepoTestData testData = new RepoTestData();
		final Integer patternListSize = 3;
		List<AttPattern__c> testPatternList = testData.createAttPatternObjList('Test', testData.companyObj.Id, patternListSize);
		AttPattern__c testPattern = testPatternList[0];

		AttPatternRepository2.SearchFilter filter = new AttPatternRepository2.SearchFilter();
		filter.ids = new Set<Id>{testPattern.Id};
		List<AttPatternEntity> resPatternList = new AttPatternRepository2().searchEntityList(filter);

		System.assertEquals(1, resPatternList.size());

		AttPatternEntity resPattern = resPatternList[0];
		System.assertEquals(testPattern.Id, resPattern.id);
		System.assertEquals(testPattern.CompanyId__c, resPattern.companyId);
		System.assertEquals(testPattern.Name, resPattern.name);
		System.assertEquals(testPattern.Name_L0__c, resPattern.nameL.valueL0);
		System.assertEquals(testPattern.Name_L1__c, resPattern.nameL.valueL1);
		System.assertEquals(testPattern.Name_L2__c, resPattern.nameL.valueL2);
		System.assertEquals(testPattern.Code__c, resPattern.code);
		System.assertEquals(testPattern.UniqKey__c, resPattern.uniqKey);
		System.assertEquals(testPattern.Order__c, resPattern.order);
		System.assertEquals(testPattern.ValidFrom__c, resPattern.validFrom.getDate());
		System.assertEquals(testPattern.ValidTo__c, resPattern.validTo.getDate());
		System.assertEquals(testPattern.WorkSystem__c, resPattern.workSystem.value);
		// 勤務時間系
		System.assertEquals(testPattern.StartTime__c, resPattern.startTime.getIntValue());
		System.assertEquals(testPattern.EndTime__c, resPattern.endTime.getIntValue());
		System.assertEquals(testPattern.ContractedWorkHours__c, resPattern.contractedWorkHours.getIntValue());
		System.assertEquals(testPattern.Rest1StartTime__c, resPattern.rest1StartTime.getIntValue());
		System.assertEquals(testPattern.Rest1EndTime__c, resPattern.rest1EndTime.getIntValue());
		System.assertEquals(testPattern.Rest2StartTime__c, resPattern.rest2StartTime.getIntValue());
		System.assertEquals(testPattern.Rest2EndTime__c, resPattern.rest2EndTime.getIntValue());
		System.assertEquals(testPattern.Rest3StartTime__c, resPattern.rest3StartTime.getIntValue());
		System.assertEquals(testPattern.Rest3EndTime__c, resPattern.rest3EndTime.getIntValue());
		System.assertEquals(testPattern.Rest4StartTime__c, resPattern.rest4StartTime.getIntValue());
		System.assertEquals(testPattern.Rest4EndTime__c, resPattern.rest4EndTime.getIntValue());
		System.assertEquals(testPattern.Rest5StartTime__c, resPattern.rest5StartTime.getIntValue());
		System.assertEquals(testPattern.Rest5EndTime__c, resPattern.rest5EndTime.getIntValue());
		// 午前半休項目
		System.assertEquals(testPattern.UseAMHalfDayLeave__c, resPattern.useAMHalfDayLeave);
		System.assertEquals(testPattern.AMContractedWorkHours__c, resPattern.amContractedWorkHours.getIntValue());
		System.assertEquals(testPattern.AMStartTime__c, resPattern.amStartTime.getIntValue());
		System.assertEquals(testPattern.AMEndTime__c, resPattern.amEndTime.getIntValue());
		System.assertEquals(testPattern.AMRest1StartTime__c, resPattern.amRest1StartTime.getIntValue());
		System.assertEquals(testPattern.AMRest1EndTime__c, resPattern.amRest1EndTime.getIntValue());
		System.assertEquals(testPattern.AMRest2StartTime__c, resPattern.amRest2StartTime.getIntValue());
		System.assertEquals(testPattern.AMRest2EndTime__c, resPattern.amRest2EndTime.getIntValue());
		System.assertEquals(testPattern.AMRest3StartTime__c, resPattern.amRest3StartTime.getIntValue());
		System.assertEquals(testPattern.AMRest3EndTime__c, resPattern.amRest3EndTime.getIntValue());
		System.assertEquals(testPattern.AMRest4StartTime__c, resPattern.amRest4StartTime.getIntValue());
		System.assertEquals(testPattern.AMRest4EndTime__c, resPattern.amRest4EndTime.getIntValue());
		System.assertEquals(testPattern.AMRest5StartTime__c, resPattern.amRest5StartTime.getIntValue());
		System.assertEquals(testPattern.AMRest5EndTime__c, resPattern.amRest5EndTime.getIntValue());
		// 午後半休項目
		System.assertEquals(testPattern.UseAMHalfDayLeave__c, resPattern.useAMHalfDayLeave);
		System.assertEquals(testPattern.PMContractedWorkHours__c, resPattern.pmContractedWorkHours.getIntValue());
		System.assertEquals(testPattern.PMStartTime__c, resPattern.pmStartTime.getIntValue());
		System.assertEquals(testPattern.PMEndTime__c, resPattern.pmEndTime.getIntValue());
		System.assertEquals(testPattern.PMRest1StartTime__c, resPattern.pmRest1StartTime.getIntValue());
		System.assertEquals(testPattern.PMRest1EndTime__c, resPattern.pmRest1EndTime.getIntValue());
		System.assertEquals(testPattern.PMRest2StartTime__c, resPattern.pmRest2StartTime.getIntValue());
		System.assertEquals(testPattern.PMRest2EndTime__c, resPattern.pmRest2EndTime.getIntValue());
		System.assertEquals(testPattern.PMRest3StartTime__c, resPattern.pmRest3StartTime.getIntValue());
		System.assertEquals(testPattern.PMRest3EndTime__c, resPattern.pmRest3EndTime.getIntValue());
		System.assertEquals(testPattern.PMRest4StartTime__c, resPattern.pmRest4StartTime.getIntValue());
		System.assertEquals(testPattern.PMRest4EndTime__c, resPattern.pmRest4EndTime.getIntValue());
		System.assertEquals(testPattern.PMRest5StartTime__c, resPattern.pmRest5StartTime.getIntValue());
		System.assertEquals(testPattern.PMRest5EndTime__c, resPattern.pmRest5EndTime.getIntValue());

		System.assertEquals(testPattern.HalfDayLeaveHours__c, resPattern.halfDayLeaveHours.getIntValue());
		System.assertEquals(testPattern.BoundaryOfStartTime__c, resPattern.boundaryOfStartTime.getIntValue());
		System.assertEquals(testPattern.BoundaryOfEndTime__c, resPattern.boundaryOfEndTime.getIntValue());
	}

	/**
	 * @description searchEntityListのテスト
	 * 各フィルタ条件で正しく検索できることを確認する
	 */
	@isTest static void searchEntityListTestFilter() {
		RepoTestData testData = new RepoTestData();
		final Integer patternListSize = 3;
		List<AttPattern__c> testPatternList = testData.createAttPatternObjList('Test', testData.companyObj.Id, patternListSize);
		AttPattern__c testPattern = testPatternList[1];
		AttPattern__c testFixPattern = testPatternList[2]; // 固定労働制の勤務パターン
		testFixPattern.WorkSystem__c = AttWorkSystem.JP_FIX.value;
		AttPattern__c testValidPattern = testPatternList[0]; // 有効期間を過去に設定
		testValidPattern.ValidFrom__c = Date.newInstance(2018, 1, 1);
		testValidPattern.ValidTo__c = Date.newInstance(2018, 1, 31);
		update new List<AttPattern__c>{testFixPattern, testValidPattern};
		// 別の会社
		CompanyEntity test2Company = testData.createCompany('Test2');
		List<AttPattern__c> test2PatternList = testData.createAttPatternObjList('Test2', test2Company.Id, patternListSize);

		AttPatternRepository2.SearchFilter filter;
		AttPatternRepository2 repo = new AttPatternRepository2();
		List<AttPatternEntity> resPatternList;

		// IDを指定して検索
		filter = new AttPatternRepository2.SearchFilter();
		filter.ids = new Set<Id>{testPattern.Id};
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(1, resPatternList.size());
		System.assertEquals(testPattern.Id, resPatternList[0].id);

		// コードを指定して検索
		filter = new AttPatternRepository2.SearchFilter();
		filter.codes = new List<String>{testPattern.Code__c};
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(1, resPatternList.size());
		System.assertEquals(testPattern.Id, resPatternList[0].id);

		// 会社IDを指定して検索
		filter = new AttPatternRepository2.SearchFilter();
		filter.companyId = test2Company.id;
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(patternListSize, resPatternList.size());
		for (AttPatternEntity resPattern : resPatternList) {
			System.assertEquals(test2Company.id, resPattern.companyId);
		}

		// 労働時間制を指定して検索
		filter = new AttPatternRepository2.SearchFilter();
		filter.workSystem = AttWorkSystem.JP_FIX;
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(1, resPatternList.size());
		System.assertEquals(testFixPattern.Id, resPatternList[0].id);

		// 対象日に有効開始日を指定
		filter = new AttPatternRepository2.SearchFilter();
		filter.targetDate = AppDate.valueOf(testValidPattern.ValidFrom__c);
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(1, resPatternList.size());
		System.assertEquals(testValidPattern.Id, resPatternList[0].id);

		// 対象日に失効日の前日を指定
		filter = new AttPatternRepository2.SearchFilter();
		filter.targetDate = AppDate.valueOf(testValidPattern.ValidTo__c).addDays(-1);
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(1, resPatternList.size());
		System.assertEquals(testValidPattern.Id, resPatternList[0].id);

		// 対象日に有効開始日の前日を指定した場合は、取得できない
		filter = new AttPatternRepository2.SearchFilter();
		filter.targetDate = AppDate.valueOf(testValidPattern.ValidFrom__c).addDays(-1);
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(0, resPatternList.size());

		// 対象日に失効日を指定した場合は、取得できない
		filter = new AttPatternRepository2.SearchFilter();
		filter.targetDate = AppDate.valueOf(testValidPattern.ValidTo__c);
		resPatternList = repo.searchEntityList(filter);
		System.assertEquals(0, resPatternList.size());
	}

	/**
	 * @description getPatternListByCompanyIdのテスト
	 * 指定した会社の勤務パターンが取得できることを確認する
	 */
	@isTest static void getPatternListByCompanyIdTest() {
		RepoTestData testData = new RepoTestData();
		final Integer patternListSize = 3;
		List<AttPattern__c> testPatternList = testData.createAttPatternObjList('Test', testData.companyObj.Id, patternListSize);
		AttPattern__c testPattern = testPatternList[1];
		// 別の会社
		CompanyEntity test2Company = testData.createCompany('Test2');
		List<AttPattern__c> test2PatternList = testData.createAttPatternObjList('Test2', test2Company.Id, patternListSize);

		AttPatternRepository2 repo = new AttPatternRepository2();
		List<AttPatternEntity> resPatternList;

		// 会社IDを指定して検索
		resPatternList = repo.getPatternListByCompanyId(test2Company.id);
		System.assertEquals(patternListSize, resPatternList.size());
		for (AttPatternEntity resPattern : resPatternList) {
			System.assertEquals(test2Company.id, resPattern.companyId);
		}
	}

	/**
	 * @description getPatternByIdのテスト
	 * 指定したIDの勤務パターンが取得できることを確認んする
	 */
	@isTest static void getPatternByIdTest() {
		RepoTestData testData = new RepoTestData();
		final Integer patternListSize = 3;
		List<AttPattern__c> testPatternList = testData.createAttPatternObjList('Test', testData.companyObj.Id, patternListSize);
		AttPattern__c testPattern = testPatternList[1];

		AttPatternRepository2 repo = new AttPatternRepository2();
		AttPatternEntity resPattern;

		// 存在する勤務パターンIDを指定して検索
		resPattern = repo.getPatternById(testPattern.id);
		System.assertNotEquals(null, resPattern);
		System.assertEquals(testPattern.Id, resPattern.id);

		// 存在しない勤務パターンIDを指定して検索
		delete testPattern;
		resPattern = repo.getPatternById(testPattern.id);
		System.assertEquals(null, resPattern);

		// idにnullを指定して検索した場合はnullが返却される
		Id testId = null;
		resPattern = repo.getPatternById(testId);
		System.assertEquals(null, resPattern);
	}

	/**
	 * @description getPatternByCodeのテスト
	 * 指定した会社とコードの勤務パターンが取得できることを確認する
	 */
	@isTest static void getPatternByCodeTest() {
		RepoTestData testData = new RepoTestData();
		final Integer patternListSize = 3;
		List<AttPattern__c> testPatternList = testData.createAttPatternObjList('Test', testData.companyObj.Id, patternListSize);
		AttPattern__c testPattern = testPatternList[1];
		// 別の会社
		CompanyEntity test2Company = testData.createCompany('Test2');
		List<AttPattern__c> test2PatternList = testData.createAttPatternObjList('Test2', test2Company.Id, patternListSize);

		AttPatternRepository2 repo = new AttPatternRepository2();
		AttPatternEntity resPattern;

		// 会社IDとコードを指定
		resPattern = repo.getPatternByCode(testPattern.Code__c, testPattern.CompanyId__c);
		System.assertNotEquals(null, resPattern);
		System.assertEquals(testPattern.Id, resPattern.id);

		// 存在しないコードを指定した場合はnullが返却される
		resPattern = repo.getPatternByCode(testPattern.Code__c, test2Company.id);
		System.assertEquals(null, resPattern);

		// コードにnullを指定した場合はnullが返却される
		resPattern = repo.getPatternByCode(null, test2Company.id);
		System.assertEquals(null, resPattern);
	}

	/**
	 * @description getPatternByCodeのテスト
	 * 指定した会社とコードの勤務パターンが取得できることを確認する
	 */
	@isTest static void getPatternByCodesTest() {
		RepoTestData testData = new RepoTestData();
		final Integer patternListSize = 3;
		List<AttPattern__c> testPatternList = testData.createAttPatternObjList('Test', testData.companyObj.Id, patternListSize);
		AttPattern__c testPattern = testPatternList[1];
		AttPattern__c testPattern2 = testPatternList[2];
		// 別の会社
		CompanyEntity test2Company = testData.createCompany('Test2');
		List<AttPattern__c> test2PatternList = testData.createAttPatternObjList('Test2', test2Company.Id, patternListSize);

		AttPatternRepository2 repo = new AttPatternRepository2();
		List<AttPatternEntity> resPatternList;

		// 会社IDとコードを指定
		resPatternList = repo.getPatternByCodes(new List<String>{testPattern.Code__c, testPattern2.Code__c}, testPattern.CompanyId__c);
		System.assertEquals(2, resPatternList.size());
		System.assertEquals(testPattern.Id, resPatternList[0].id);
		System.assertEquals(testPattern2.Id, resPatternList[1].id);

		// 存在しないコードを指定した場合は空のリストが返却される
		resPatternList = repo.getPatternByCodes(new List<String>{testPattern.Code__c, testPattern2.Code__c}, test2Company.id);
		System.assertEquals(true, resPatternList.isEmpty());

		// 空のコードリストを指定場合は空のリストが返却される
		resPatternList = repo.getPatternByCodes(new List<String>{}, test2Company.id);
		System.assertEquals(true, resPatternList.isEmpty());
	}

	/**
	 * @description getPatternListのテスト
	 * 指定したIDの勤務パターンが取得できることを確認する
	 */
	@isTest static void getPatternListTest() {
		RepoTestData testData = new RepoTestData();
		final Integer patternListSize = 3;
		List<AttPattern__c> testPatternList = testData.createAttPatternObjList('Test', testData.companyObj.Id, patternListSize);
		AttPattern__c testPattern = testPatternList[1];
		AttPattern__c testPattern2 = testPatternList[2];
		// 別の会社
		CompanyEntity test2Company = testData.createCompany('Test2');
		List<AttPattern__c> test2PatternList = testData.createAttPatternObjList('Test2', test2Company.Id, patternListSize);

		AttPatternRepository2 repo = new AttPatternRepository2();
		List<AttPatternEntity> resPatternList;

		// 複数のIDを指定
		resPatternList = repo.getPatternList(new Set<Id>{testPattern2.Id, testPattern.Id});
		System.assertEquals(2, resPatternList.size());
		// コード順で返却されている
		System.assertEquals(testPattern.Id, resPatternList[0].id);
		System.assertEquals(testPattern2.Id, resPatternList[1].id);

		// 存在しないコードを指定した場合は空のリストが返却される
		delete testPattern2;
		resPatternList = repo.getPatternList(new Set<Id>{testPattern2.Id});
		System.assertEquals(true, resPatternList.isEmpty());

		// 空のコードリストを指定場合は空のリストが返却される
		resPatternList = repo.getPatternList(new Set<Id>{});
		System.assertEquals(true, resPatternList.isEmpty());
	}

	/**
	 * @description saveEntityのテスト
	 * 勤務パターンが保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		RepoTestData testData = new RepoTestData();

		// エンティティからSObjectを作成するコードは自動生成されているため、
		// 本メソッドではすべての項目のチェックは行わない
		AttPatternEntity newPattern = new AttPatternEntity();
		newPattern.name = 'New Pattern';
		newPattern.companyId = testData.companyObj.Id;
		newPattern.code = 'New Code';
		newPattern.uniqKey = 'New UniqKey';
		newPattern.nameL0 = 'New Pattern L0';

		AttPatternRepository2 repo = new AttPatternRepository2();
		Repository.SaveResult result = repo.saveEntity(newPattern);
		System.assertEquals(true, result.isSuccessAll);
		Id newPatternId = result.details[0].id;
		List<AttPattern__c> resPatternObjs = [SELECT Id, Name, CompanyId__c, Code__c, UniqKey__c, Name_L0__c FROM AttPattern__c WHERE Id = :newPatternId];
		System.assertEquals(1, resPatternObjs.size());
		AttPattern__c resPattern = resPatternObjs[0];
		System.assertEquals(newPattern.name, resPattern.Name);
		System.assertEquals(newPattern.companyId, resPattern.CompanyId__c);
		System.assertEquals(newPattern.code, resPattern.Code__c);
		System.assertEquals(newPattern.uniqKey, resPattern.UniqKey__c);
		System.assertEquals(newPattern.nameL0, resPattern.Name_L0__c);
	}

	/**
	 * @description saveEntityListのテスト
	 * 勤務パターンが保存できることを確認する
	 */
	@isTest static void saveEntityListTest() {
		RepoTestData testData = new RepoTestData();

		// エンティティからSObjectを作成するコードは自動生成されているため、
		// 本メソッドではすべての項目のチェックは行わない
		AttPatternEntity newPattern1 = new AttPatternEntity();
		newPattern1.name = 'New Pattern 1';
		newPattern1.companyId = testData.companyObj.Id;
		newPattern1.code = 'New Code 1';
		newPattern1.uniqKey = 'New UniqKey 1';
		newPattern1.nameL0 = 'New Pattern 1 L0';

		AttPatternEntity newPattern2 = new AttPatternEntity();
		newPattern2.name = 'New Pattern 2';
		newPattern2.companyId = testData.companyObj.Id;
		newPattern2.code = 'New Code 2';
		newPattern2.uniqKey = 'New UniqKey 2';
		newPattern2.nameL0 = 'New Pattern 2 L0';

		AttPatternRepository2 repo = new AttPatternRepository2();
		Repository.SaveResult result = repo.saveEntityList(new List<AttPatternEntity>{newPattern1, newPattern2});
		System.assertEquals(true, result.isSuccessAll);
		List<Id> newPatternIds = new List<Id>{result.details[0].id, result.details[1].id};
		List<AttPattern__c> resPatternObjs = [
			SELECT Id, Name, CompanyId__c, Code__c, UniqKey__c, Name_L0__c FROM AttPattern__c WHERE Id IN :newPatternIds];
		System.assertEquals(2, resPatternObjs.size());
		AttPattern__c resPattern1 = resPatternObjs[0];
		AttPattern__c resPattern2 = resPatternObjs[1];
		System.assertEquals(newPattern1.name, resPattern1.Name);
		System.assertEquals(newPattern1.companyId, resPattern1.CompanyId__c);
		System.assertEquals(newPattern1.code, resPattern1.Code__c);
		System.assertEquals(newPattern1.uniqKey, resPattern1.UniqKey__c);
		System.assertEquals(newPattern1.nameL0, resPattern1.Name_L0__c);
		System.assertEquals(newPattern2.name, resPattern2.Name);
		System.assertEquals(newPattern2.companyId, resPattern2.CompanyId__c);
		System.assertEquals(newPattern2.code, resPattern2.Code__c);
		System.assertEquals(newPattern2.uniqKey, resPattern2.UniqKey__c);
		System.assertEquals(newPattern2.nameL0, resPattern2.Name_L0__c);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		RepoTestData testData = new RepoTestData();
		AttPattern__c patternObj = testData.createAttPatternObjList('Test', testData.companyObj.Id, 1)[0];

		// キャッシュをonでインスタンスを生成
		AttPatternRepository2 repository = new AttPatternRepository2(true);

		// 検索（DBから取得）
		AttPatternRepository2.SearchFilter filter = new AttPatternRepository2.SearchFilter();
		filter.ids = new Set<Id>{patternObj.Id};
		AttPatternEntity pattern = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		patternObj.Code__c = 'NotCache';
		upsert patternObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttPatternEntity cachedPattern = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(pattern.id, cachedPattern.id);
		System.assertNotEquals('NotCache', cachedPattern.code);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		RepoTestData testData = new RepoTestData();
		AttPattern__c patternObj = testData.createAttPatternObjList('Test', testData.companyObj.Id, 1)[0];

		// 検索（DBから取得）
		AttPatternRepository2.SearchFilter filter = new AttPatternRepository2.SearchFilter();
		filter.ids = new Set<Id>{patternObj.Id};
		AttPatternEntity pattern = new AttPatternRepository2(true).searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		patternObj.Code__c = 'NotCache';
		upsert patternObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		AttPatternEntity cachedPattern = new AttPatternRepository2(true).searchEntityList(filter, false)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(pattern.id, cachedPattern.id);
		System.assertNotEquals('NotCache', cachedPattern.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheDifferentCondition() {
		RepoTestData testData = new RepoTestData();
		AttPattern__c patternObj = testData.createAttPatternObjList('Test', testData.companyObj.Id, 1)[0];

		// キャッシュをonでインスタンスを生成
		AttPatternRepository2 repository = new AttPatternRepository2(true);

		// 検索（DBから取得）
		AttPatternRepository2.SearchFilter filter = new AttPatternRepository2.SearchFilter();
		filter.ids = new Set<Id>{patternObj.Id};
		AttPatternEntity pattern = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		patternObj.Code__c = 'NotCache';
		upsert patternObj;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.ids.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		AttPatternEntity cachedPattern = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(pattern.id, cachedPattern.id);
		System.assertEquals('NotCache', cachedPattern.code);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		RepoTestData testData = new RepoTestData();
		AttPattern__c patternObj = testData.createAttPatternObjList('Test', testData.companyObj.Id, 1)[0];

		// キャッシュをonでインスタンスを生成
		AttPatternRepository2 repository = new AttPatternRepository2(true);

		// 検索（DBから取得）
		AttPatternRepository2.SearchFilter filter = new AttPatternRepository2.SearchFilter();
		filter.ids = new Set<Id>{patternObj.Id};
		AttPatternEntity pattern = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		patternObj.Code__c = 'NotCache';
		upsert patternObj;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttPatternEntity cachedPattern = repository.searchEntityList(filter, true)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(pattern.id, cachedPattern.id);
		System.assertEquals('NotCache', cachedPattern.code);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		RepoTestData testData = new RepoTestData();
		AttPattern__c patternObj = testData.createAttPatternObjList('Test', testData.companyObj.Id, 1)[0];

		// キャッシュをoffでインスタンスを生成
		AttPatternRepository2 repository = new AttPatternRepository2();

		// 検索（DBから取得）
		AttPatternRepository2.SearchFilter filter = new AttPatternRepository2.SearchFilter();
		filter.ids = new Set<Id>{patternObj.Id};
		AttPatternEntity pattern = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		patternObj.Code__c = 'NotCache';
		upsert patternObj;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		AttPatternEntity cachedPattern = repository.searchEntityList(filter, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(pattern.id, cachedPattern.id);
		System.assertEquals('NotCache', cachedPattern.code);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		AttPatternRepository2.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new AttPatternRepository2().isEnabledCache);
	}
}