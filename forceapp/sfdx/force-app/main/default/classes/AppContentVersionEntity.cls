/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * コンテンツバージョンエンティティ
 */
public with sharing class AppContentVersionEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		TITLE,
		PATH_ON_CLIENT,
		VERSION_DATA,
		CREATED_DATE
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/** コンテンツドキュメントID（参照のみ。値を変更してもDBに保存されない。） */
	public Id contentDocumentId {get; set;}

	/** タイトル */
	public String title {
		get;
		set {
			title = value;
			setChanged(Field.TITLE);
		}
	}

	public Datetime createdDate {
		get;
		set;
	}

	/** ファイルタイプ（参照のみ。値を変更してもDBに保存されない。） */
	public String fileType {get; set;}

	/** パス */
	public String pathOnClient {
		get;
		set {
			pathOnClient = value;
			setChanged(Field.PATH_ON_CLIENT);
		}
	}

	/** コンテンツデータ(Blob) */
	public Blob versionData {
		get;
		set {
			versionData = value;
			setChanged(Field.VERSION_DATA);
		}
	}

	/** コンテンツデータ(文字列) */
	public String versionDataText {
		get {
			if (versionData == null) {
				return null;
			}
			return EncodingUtil.base64Encode(versionData);
		}
		set {
			if (versionDataText == null) {
				versionData = null;
			}
			versionData = EncodingUtil.base64Decode(value);
			setChanged(Field.VERSION_DATA);
		}
	}

	/** コンストラクタ */
	public AppContentVersionEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}