/**
 * 勤怠日数
 * 勤怠でカウントする日数
 */
public with sharing class AttDays extends ValueObject implements Comparable {
	/**
	 * 日数
	 */
	public Decimal value {
		private get;
		private set {
			// 必要なら値を検証する
			this.value = value;
		}
	}
	/**
	 * @desctiprion 日数と時間のセット
	 */
	public class DaysAndHours {
		public Decimal days;
		public Integer hours;
	}

	/**
	 * Integer型の値をAttTimeに変換する
	 * @param Integer型の値
	 * @return 作成したAttTime
	 */
	public static AttDays valueOf(Integer value) {
		if (value == null) {
			return null;
		}
		return new AttDays(value);
	}

	/**
	 * Decimal型の値をAttTimeに変換する
	 * @param Decimal型の値
	 * @return 作成したAttTime
	 */
	public static AttDays valueOf(Decimal value) {
		return new AttDays(value);
	}

	/**
	 * Decimal型の値をAttTimeに変換する
	 * @param Decimal型の値
	 * @return 作成したAttTime
	 */
	public static Integer convertInt(AttDays attValue) {
		if (attValue == null) {
			return null;
		}
		return Integer.valueOf(attValue.value);
	}
	/**
	 * Decimal型の値をAttTimeに変換する
	 * @param Decimal型の値
	 * @return 作成したAttTime
	 */
	public static Decimal convertDec(AttDays attValue) {
		if (attValue == null) {
			return null;
		}
		return attValue.value;
	}

	/**
	 * コンストラクタ
	 * @param value 日数
	 */
	public AttDays(Decimal value) {
		this.value = value;
	}

	/**
	 * 値をInteger型で取得する
	 * @return Intger型に変換した値
	 */
	public Integer getIntValue() {
		return (Integer)value;
	}
	/**
	 * 値をDecimal型で取得する
	 * @return Decimal型に変換した値
	 */
	public Decimal getDecValue() {
		return value;
	}

	/**
	 * AttDays型の値を現値に追加する
	 * @param 追加対象
	 */
	public void add(AttDays targetValue) {
		if (targetValue != null && targetValue != null) {
			value += targetValue.getDecValue();
		}
	}
	/**
	 * オブジェクトの値が等しいかどうか判定する
	 * @param compare 比較対象のオブジェクト
	 */
	public override Boolean equals(Object compare) {
		if (compare instanceof AttDays) {
			return this.value == ((AttDays)compare).value;
		}
		return false;
	}

	/**
	 * 比較の結果であるinteger値を返します
	 * @param 比較対象
	 * @return このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満
	 */
	public Integer compareTo(Object compareTo) {
		AttDays compareTime = (AttDays)compareTo;
		if (this.value == compareTime.value) {
			return 0;
		} else if (this.value > compareTime.value) {
			return 1;
		}
		// this.value < compareTime.value
		return -1;
	}

	/**
	 * 有休日数(x.xxx)を日と時間(x hour)に変換します
	 * 注：有休の日数計算以外には使用しないでください
	 * @param 日数(x.xxx)
	 * @param 所定労働時間
	 * @return 日数を日(x日 or X.5日)と時間(x時間)に分割したもの
	 */
	public DaysAndHours getDaysAndHours(AttDailyTime contractedWorkHours){
		Integer intContractedWorkHours = AttDailyTime.convertInt(contractedWorkHours);
		return getDaysAndHours(intContractedWorkHours);
	}
	public DaysAndHours getDaysAndHours(Integer contractedWorkHours){
		Integer roundWorkHours = AttDailyTime.getFitHourValue(contractedWorkHours);
		DaysAndHours daysAndHours = new DaysAndHours();
		// 設定値がない場合
		if (this.value == null || roundWorkHours == 0){
			daysAndHours.days = 0;
			daysAndHours.hours = 0;
		} else {
			// 日数のみを取り出し表示用に変数に格納
			Decimal realDays = this.value.round(System.RoundingMode.DOWN);
			// 日数から時間部分のみを取り出す
			Decimal hours = this.value - realDays;
			/* TODO：表示された時間数を取得できるように対応
			// 時間日数を時間単位に合わせて変換
			Integer minutes = (Integer)(hours * roundWorkHours);
			minutes = AttDailyTime.getFitHourValue(minutes);
			hours = (Decimal)minutes / roundWorkHours;
			// 一日分になる場合、日数に集計
			if (hours >= 1) {
				realDays += hours.intValue();
				hours -= hours.intValue();
			}
			*/
			// 半日の場合のみ0.5で表現する
			// 時間単位休利用しない場合や所定労働時間が2で割れる場合のみ有効
			if (hours == 0.5) {
				realDays += 0.5;
				hours = 0;
			}
			// 半日の表現が可能な条件が揃っている場合、日数をx.5+時間で表現する
			else if (hours > 0.5) {
				// 所定労働時間は2時間で割り切れるか
				boolean isDivisibleWorkHours = (math.mod(roundWorkHours , 60 * 2)) == 0;
				if (isDivisibleWorkHours) {
					hours -= 0.5;
					realDays += 0.5;
				}
			}
			// 所定労働時間から時間数を算出する
			Decimal realOurs = (hours * roundWorkHours) / 60;
			daysAndHours.days = realDays;
			daysAndHours.hours = realOurs.setScale(0, System.RoundingMode.HALF_UP).intValue();
		}
		return daysAndHours;
	}
	public static Decimal getDaysByLeaveTime(Integer leaveTime, Integer workTimeOneDay) {
		Integer realLeaveTime = leaveTime;
		// １時間未満の場合、使用する時間は１時間として切り上げる
		Integer rest = Math.mod(realLeaveTime, 60);
		if (rest > 0){
			realLeaveTime = ((realLeaveTime / 60 ) * 60) + 60;
		}
		Decimal leaveDays = Decimal.valueOf(realLeaveTime) / AttDailyTime.getFitHourValue(workTimeOneDay);
		return leaveDays.setScale(5, System.RoundingMode.DOWN);
	}
}