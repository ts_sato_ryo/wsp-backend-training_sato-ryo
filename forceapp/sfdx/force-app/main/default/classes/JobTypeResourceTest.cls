/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description JobTypeResourceのテスト
 */
@isTest
private class JobTypeResourceTest {

	/**
	 * 本クラス内の全てのテストメソッドで利用するテストデータを登録する
	 */
	@testSetup static void setup() {
		// マスタデータを登録しておく
		TestData.setupMaster();
	}


	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

		/**
		 * ジョブタイプ別作業分類を作成&保存する
		 */
		public List<JobTypeWorkCategoryEntity> createJobTypeWorkCategory(Id jobTypeId, List<Id> workCategoryIdList) {
			List<JobTypeWorkCategoryEntity> entities = new List<JobTypeWorkCategoryEntity>();
			for (Id workCategoryId : workCategoryIdList) {
				JobTypeWorkCategoryEntity entity = new JobTypeWorkCategoryEntity();
				entity.jobTypeId = jobTypeId;
				entity.workCategoryId = workCategoryId;
				entities.add(entity);
			}

			JobTypeWorkCategoryRepository repo = new JobTypeWorkCategoryRepository();
			Repository.SaveResult result = repo.saveEntityList(entities);
			List<Id> JobTypeWorkCategoryIdList = new List<Id>();
			for (Repository.ResultDetail detail : result.details) {
				JobTypeWorkCategoryIdList.add(detail.id);
			}
			return repo.getEntityList(JobTypeWorkCategoryIdList);
		}
	}

	/**
	 * CreateApi のテスト
	 * 新規のジョブタイプが保存できることを確認する
	 */
	@isTest static void createApiTestNormal() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);

		JobTypeResource.CreateRequest request = new JobTypeResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;
		request.workCategoryIdList = new List<String>{workCategories[1].id, workCategories[0].id};

		// API実行
		JobTypeResource.CreateApi api = new JobTypeResource.CreateApi();
		JobTypeResource.CreateResponse response = (JobTypeResource.CreateResponse)api.execute(request);

		// 保存できていることを確認
		JobTypeEntity savedJobType = new JobTypeRepository().getEntity(response.id);
		TestUtil.assertNotNull(savedJobType);
		System.assertEquals(request.name_L0, savedJobType.name);
		System.assertEquals(request.name_L0, savedJobType.nameL0);
		System.assertEquals(request.name_L1, savedJobType.nameL1);
		System.assertEquals(request.name_L2, savedJobType.nameL2);
		System.assertEquals(request.code, savedJobType.code);
		System.assertEquals(request.companyId, savedJobType.companyId);

		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(savedJobType.id);
		System.assertEquals(request.workCategoryIdList.size(), savedJobTypeWorkCategories.size());
		System.assertEquals(request.workCategoryIdList[0], savedJobTypeWorkCategories[1].workCategoryId);
		System.assertEquals(request.workCategoryIdList[1], savedJobTypeWorkCategories[0].workCategoryId);
	}

	/**
	 * CreateApi のテスト
	 * ジョブタイプの管理権限を持っていない管理者がジョブタイプ作成ができないことを確認する
	 */
	@isTest static void createApiTestNoPermission() {
		// テストデータ作成
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// ジョブタイプの管理権限を無効に設定する
		testData.permission.isManageJobType = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		JobTypeResource.CreateRequest request = new JobTypeResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;
		request.workCategoryIdList = new List<String>{workCategories[1].id, workCategories[0].id};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				JobTypeResource.CreateApi api = new JobTypeResource.CreateApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * CreateApi のテスト
	 * リクエストパラメータに不正な値が設定されている場合は想定されている例外が発生することを確認する
	 */
	@isTest static void createApiTestInvalidParam() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);

		JobTypeResource.CreateRequest request = new JobTypeResource.CreateRequest();
		request.name_L0 = 'Test_L0';
		request.name_L1 = 'Test_L1';
		request.name_L2 = 'Test_L2';
		request.code = 'Test001';
		request.companyId = testData.company.id;
		request.workCategoryIdList = new List<String>{workCategories[1].id, workCategories[0].id};

		JobTypeResource.CreateApi api = new JobTypeResource.CreateApi();
		App.ParameterException resEx;

		// Test1: 会社IDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.CreateRequest testRequest = request.clone();
			testRequest.companyId = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}),
				resEx.getMessage());

		// Test2: 作業分類リストIDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.CreateRequest testRequest = request.clone();
			testRequest.workCategoryIdList[1] = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'workCategoryIdList'}),
				resEx.getMessage());
	}

	/**
	 * UpdateApi のテスト
	 * 既存のジョブタイプが更新できることを確認する
	 */
	@isTest static void updateApiTestNormal() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');
		testData.createJobTypeWorkCategory(jobType.id, new List<Id>{workCategories[0].id});

		JobTypeResource.UpdateRequest request = new JobTypeResource.UpdateRequest();
		request.id = jobType.id;
		request.name_L0 = jobType.nameL0 + '_Update';
		request.name_L1 = jobType.nameL1 + '_Update';
		request.name_L2 = jobType.nameL2 + '_Update';
		request.code = jobType.code + '_Update';
		request.companyId = updateCompany.id;
		request.workCategoryIdList = new List<String>{workCategories[1].id, workCategories[2].id};

		// API実行
		JobTypeResource.UpdateApi api = new JobTypeResource.UpdateApi();
		api.execute(request);

		// 保存できていることを確認
		JobTypeEntity savedJobType = new JobTypeRepository().getEntity(jobType.id);
		TestUtil.assertNotNull(savedJobType);
		System.assertEquals(request.name_L0, savedJobType.name);
		System.assertEquals(request.name_L0, savedJobType.nameL0);
		System.assertEquals(request.name_L1, savedJobType.nameL1);
		System.assertEquals(request.name_L2, savedJobType.nameL2);
		System.assertEquals(request.code, savedJobType.code);
		System.assertEquals(request.companyId, savedJobType.companyId);

		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(savedJobType.id);
		System.assertEquals(request.workCategoryIdList.size(), savedJobTypeWorkCategories.size());
		System.assertEquals(request.workCategoryIdList[0], savedJobTypeWorkCategories[0].workCategoryId);
		System.assertEquals(request.workCategoryIdList[1], savedJobTypeWorkCategories[1].workCategoryId);
	}

	/**
	 * UpdateApi のテスト
	 * ジョブタイプの管理権限を持っていない管理者がジョブタイプ更新ができないことを確認する
	 */
	@isTest static void updateApiTestNoPermission() {
		// テストデータ作成
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');
		testData.createJobTypeWorkCategory(jobType.id, new List<Id>{workCategories[0].id});
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// ジョブタイプの管理権限を無効に設定する
		testData.permission.isManageJobType = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		JobTypeResource.UpdateRequest request = new JobTypeResource.UpdateRequest();
		request.id = jobType.id;
		request.name_L0 = jobType.nameL0 + '_Update';
		request.name_L1 = jobType.nameL1 + '_Update';
		request.name_L2 = jobType.nameL2 + '_Update';
		request.code = jobType.code + '_Update';
		request.companyId = updateCompany.id;
		request.workCategoryIdList = new List<String>{workCategories[1].id, workCategories[2].id};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				JobTypeResource.UpdateApi api = new JobTypeResource.UpdateApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * UpdateApi のテスト
	 * リクエストパラメータに不正な値が設定されている場合は想定されている例外が発生することを確認する
	 */
	@isTest static void updateApiTestInvalidParam() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');
		testData.createJobTypeWorkCategory(jobType.id, new List<Id>{workCategories[0].id});

		JobTypeResource.UpdateRequest request = new JobTypeResource.UpdateRequest();
		request.id = jobType.id;
		request.name_L0 = jobType.nameL0 + '_Update';
		request.name_L1 = jobType.nameL1 + '_Update';
		request.name_L2 = jobType.nameL2 + '_Update';
		request.code = jobType.code + '_Update';
		request.companyId = updateCompany.id;
		request.workCategoryIdList = new List<String>{workCategories[1].id, workCategories[2].id};

		JobTypeResource.UpdateApi api = new JobTypeResource.UpdateApi();
		App.ParameterException resEx;

		// Test1: 会社IDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.UpdateRequest testRequest = request.clone();
			testRequest.companyId = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}),
				resEx.getMessage());

		// Test2: 作業分類リストIDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.UpdateRequest testRequest = request.clone();
			testRequest.workCategoryIdList[1] = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'workCategoryIdList'}),
				resEx.getMessage());

		// Test3: ジョブタイプIDが設定されていない
		resEx = null;
		try {
			JobTypeResource.UpdateRequest testRequest = request.clone();
			testRequest.id = null;
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test3: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());

		// Test4: ジョブタイプIDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.UpdateRequest testRequest = request.clone();
			testRequest.id = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test4: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());
	}

	/**
	 * DeleteApi のテスト
	 * 指定したジョブタイプが削除できることを確認する
	 */
	@isTest static void deleteApiTest() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');
		testData.createJobTypeWorkCategory(jobType.id, new List<Id>{workCategories[0].id});

		JobTypeResource.DeleteRequest request = new JobTypeResource.DeleteRequest();
		request.id = jobType.id;

		// API実行
		JobTypeResource.DeleteApi api = new JobTypeResource.DeleteApi();
		api.execute(request);

		// 削除できていることを確認
		JobTypeEntity resJobType = new JobTypeRepository().getEntity(jobType.id);
		System.assertEquals(null, resJobType);
		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(jobType.id);
		System.assertEquals(true, savedJobTypeWorkCategories.isEmpty());
	}

	/**
	 * DeleteApi のテスト
	 * 削除対象のジョブタイプが既に削除されている場合は例外が発生しないことを確認する
	 */
	@isTest static void deleteApiTestDeletedJobType() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');
		testData.createJobTypeWorkCategory(jobType.id, new List<Id>{workCategories[0].id});

		JobTypeResource.DeleteRequest request = new JobTypeResource.DeleteRequest();
		request.id = jobType.id;

		// 削除しておく
		new JobTypeRepository().deleteEntity(jobType.id);

		// API実行
		JobTypeResource.DeleteApi api = new JobTypeResource.DeleteApi();
		api.execute(request);

		// 削除されていることを確認
		JobTypeEntity resJobType = new JobTypeRepository().getEntity(jobType.id);
		System.assertEquals(null, resJobType);
		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(jobType.id);
		System.assertEquals(true, savedJobTypeWorkCategories.isEmpty());
	}

	/**
	 * DeleteApi のテスト
	 * ジョブタイプの管理権限を持っていない管理者がジョブタイプ削除ができないことを確認する
	 */
	@isTest static void deleteApiTestNoPermission() {
		// テストデータ作成
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');
		testData.createJobTypeWorkCategory(jobType.id, new List<Id>{workCategories[0].id});
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// ジョブタイプの管理権限を無効に設定する
		testData.permission.isManageJobType = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		JobTypeResource.DeleteRequest request = new JobTypeResource.DeleteRequest();
		request.id = jobType.id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				JobTypeResource.DeleteApi api = new JobTypeResource.DeleteApi();
				api.execute(request);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * DeleteApi のテスト
	 * リクエストパラメータに不正な値が設定されている場合は想定されている例外が発生することを確認する
	 */
	@isTest static void deleteApiTestInvalidParam() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity jobType = testData.createJobType('TestJobType');
		testData.createJobTypeWorkCategory(jobType.id, new List<Id>{workCategories[0].id});

		JobTypeResource.DeleteRequest request = new JobTypeResource.DeleteRequest();
		request.id = jobType.id;

		JobTypeResource.DeleteApi api = new JobTypeResource.DeleteApi();
		App.ParameterException resEx;

		// Test1: ジョブタイプIDが設定されていない
		resEx = null;
		try {
			JobTypeResource.DeleteRequest testRequest = request.clone();
			testRequest.id = null;
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());

		// Test2: ジョブタイプIDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.DeleteRequest testRequest = request.clone();
			testRequest.id = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());
	}

	/**
	 * SearchApi のテスト
	 * 指定したジョブタイプIDのジョブタイプが取得できることを確認する
	 */
	@isTest static void searchApiTestJobTypeId() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity targetJobType = testData.createJobType('TestJobType');
		JobTypeEntity jobType1 = testData.createJobType('TestJobType1');
		JobTypeEntity jobType2 = testData.createJobType('TestJobType2');
		testData.createJobTypeWorkCategory(targetJobType.id, new List<Id>{workCategories[2].id, workCategories[1].id});

		JobTypeResource.SearchRequest request = new JobTypeResource.SearchRequest();
		request.id = targetJobType.id;

		// API実行
		JobTypeResource.SearchApi api = new JobTypeResource.SearchApi();
		JobTypeResource.SearchResponse response = (JobTypeResource.SearchResponse)api.execute(request);

		// 検索結果が正しいことを確認
		System.assertEquals(1, response.records.size());
		JobTypeResource.JobTypeParam resJobType = response.records[0];
		String expJobTypeName = new AppMultiString(resJobType.name_L0, resJobType.name_L1, resJobType.name_L2).getValue();
		System.assertEquals(expJobTypeName, resJobType.name);
		System.assertEquals(targetJobType.nameL0, resJobType.name_L0);
		System.assertEquals(targetJobType.nameL1, resJobType.name_L1);
		System.assertEquals(targetJobType.nameL2, resJobType.name_L2);
		System.assertEquals(targetJobType.code, resJobType.code);
		System.assertEquals(targetJobType.companyId, resJobType.companyId);
		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(targetJobType.id);
		System.assertEquals(savedJobTypeWorkCategories.size(), resJobType.workCategoryIdList.size());
		System.assertEquals(savedJobTypeWorkCategories[0].workCategoryId,
				resJobType.workCategoryIdList[0]);
		System.assertEquals(savedJobTypeWorkCategories[1].workCategoryId,
				resJobType.workCategoryIdList[1]);
	}

	/**
	 * SearchApi のテスト
	 * 指定した会社IDのジョブタイプが取得できることを確認する
	 */
	@isTest static void searchApiTestCompanyId() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity targetJobType1 = testData.createJobType('TargetJobType1');
		JobTypeEntity targetJobType2 = testData.createJobType('TargetJobType2');
		JobTypeEntity jobType1 = testData.createJobType('TestJobType1');
		JobTypeEntity jobType2 = testData.createJobType('TestJobType2');
		testData.createJobTypeWorkCategory(targetJobType1.id, new List<Id>{workCategories[2].id, workCategories[1].id});

		// 取得対象の会社
		CompanyEntity targetCompany = testData.createCompany('検索Test');
		targetJobType1.companyId = targetCompany.id;
		targetJobType2.companyId = targetCompany.id;
		new JobTypeRepository().saveEntityList(new List<JobTypeEntity>{targetJobType1, targetJobType2});

		JobTypeResource.SearchRequest request = new JobTypeResource.SearchRequest();
		request.companyId = targetCompany.id;

		// API実行
		JobTypeResource.SearchApi api = new JobTypeResource.SearchApi();
		JobTypeResource.SearchResponse response = (JobTypeResource.SearchResponse)api.execute(request);

		// 検索結果が正しいことを確認
		System.assertEquals(2, response.records.size());
		JobTypeResource.JobTypeParam resJobType = response.records[0];
		String expJobTypeName = new AppMultiString(resJobType.name_L0, resJobType.name_L1, resJobType.name_L2).getValue();
		System.assertEquals(targetJobType1.id, resJobType.id);
		System.assertEquals(expJobTypeName, resJobType.name);
		System.assertEquals(targetJobType1.nameL0, resJobType.name_L0);
		System.assertEquals(targetJobType1.nameL1, resJobType.name_L1);
		System.assertEquals(targetJobType1.nameL2, resJobType.name_L2);
		System.assertEquals(targetJobType1.code, resJobType.code);
		System.assertEquals(targetJobType1.companyId, resJobType.companyId);
		List<JobTypeWorkCategoryEntity> savedJobTypeWorkCategories =
				new JobTypeWorkCategoryRepository().getEntityListByJobType(targetJobType1.id);
		System.assertEquals(savedJobTypeWorkCategories.size(), resJobType.workCategoryIdList.size());
		System.assertEquals(savedJobTypeWorkCategories[0].workCategoryId,
				resJobType.workCategoryIdList[0]);
		System.assertEquals(savedJobTypeWorkCategories[1].workCategoryId,
				resJobType.workCategoryIdList[1]);
		// 2個目のジョブタイプは作業分類リストが空
		System.assertEquals(true, response.records[1].workCategoryIdList.isEmpty());
	}

	/**
	 * SearchApi のテスト
	 * a定したジョブタイプIDのジョブタイプが取得できることを確認する
	 */
	@isTest static void searchApiTestInvalidParam() {
		final Integer workCategorySize = 3;
		ResourceTestData testData = new ResourceTestData();
		CompanyEntity updateCompany = testData.createCompany('UpdateTest');
		List<WorkCategoryEntity> workCategories = testData.createWorkCategoryList('Test作業', workCategorySize);
		JobTypeEntity targetJobType = testData.createJobType('TestJobType');
		JobTypeEntity jobType1 = testData.createJobType('TestJobType1');
		JobTypeEntity jobType2 = testData.createJobType('TestJobType2');
		testData.createJobTypeWorkCategory(targetJobType.id, new List<Id>{workCategories[2].id, workCategories[1].id});

		JobTypeResource.SearchRequest request = new JobTypeResource.SearchRequest();
		request.companyId = testData.company.id;
		request.id = targetJobType.id;

		JobTypeResource.SearchApi api = new JobTypeResource.SearchApi();
		App.ParameterException resEx;

		// Test1: ジョブタイプIDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.SearchRequest testRequest = request.clone();
			testRequest.id = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test1: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'id'}),
				resEx.getMessage());

		// Test2: 会社IDにID以外の文字列が設定されている
		resEx = null;
		try {
			JobTypeResource.SearchRequest testRequest = request.clone();
			testRequest.companyId = 'aaa';
			api.execute(testRequest);
		} catch (App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, 'Test2: 例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'companyId'}),
				resEx.getMessage());
	}

}
