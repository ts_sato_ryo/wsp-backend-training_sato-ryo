/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description TimeSetingHistoryResourceのテストクラス
*/
@isTest
private class TimeSettingHistoryResourceTest {

	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

		public Date currentDate;

		/**
		 * コンストラクタ
		 * 標準ユーザーと設定を作成する
		 */
		public ResourceTestData(){
			super();
			currentDate = Date.today();
		}

	}

	/**
	 * 工数設定履歴レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c base = ComTestDataUtility.createTimeSettingWithHistory('工数設定ベース', company.Id);
		// 履歴の最新日を今日にする
		List<TimeSettingHistory__c> testHistoryList = [SELECT Id FROM TimeSettingHistory__c WHERE BaseId__c =:base.Id];
		Date today = System.today();
		for (Integer i = 0, n = testHistoryList.size(); i < n; i++) {
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		Test.startTest();

		TimeSettingHistoryResource.TimeSettingHistory param = new TimeSettingHistoryResource.TimeSettingHistory();
		param.baseId = base.Id;
		param.name_L0 = 'テスト工数設定_L0';
		param.name_L1 = 'テスト工数設定_L1';
		param.name_L2 = 'テスト工数設定_L2';
		param.validDateFrom = today + 1;
		param.validDateTo = today + 31;
		param.comment = 'Testコメント';

		TimeSettingHistoryResource.CreateApi api = new TimeSettingHistoryResource.CreateApi();
		TimeSettingHistoryResource.SaveResult res = (TimeSettingHistoryResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 工数設定ベース・履歴レコードが1件ずつ作成されること
		List<TimeSettingBase__c> newBaseRecords = [
			SELECT
				Id,
				Name,
				Code__c,
				UniqKey__c,
				CompanyId__c,
				CurrentHistoryId__c,
				SummaryPeriod__c,
				StartDateOfMonth__c,
				MonthMark__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ValidFrom__c,
						ValidTo__c,
						HistoryComment__c
					FROM Histories__r
					WHERE Id =: res.Id
				)
			FROM TimeSettingBase__c
			WHERE CompanyId__c = :company.Id];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].Histories__r.size());

		// 値の検証
		TimeSettingBase__c newBase = newBaseRecords[0];
		TimeSettingHistory__c newHistory = newBase.Histories__r[0];

		// レスポンス値の検証
		System.assertEquals(newHistory.Id, res.Id);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = newBase.UniqKey__c + '-' +
							new AppDate(param.validDateFrom).formatYYYYMMDD() + '-' +
							new AppDate(param.validDateTo).formatYYYYMMDD();
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.name_L0, newHistory.Name);
		System.assertEquals(param.name_L0, newHistory.Name_L0__c);
		System.assertEquals(param.name_L1, newHistory.Name_L1__c);
		System.assertEquals(param.name_L2, newHistory.Name_L2__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
	}

	/**
	 * 工数設定履歴レコードを1件作成する(異常系:工数設定の管理権限なし)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		TimeSettingBaseEntity setting = testData.createTimeSetting('Test設定');
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 工数設定の管理権限を無効に設定する
		testData.permission.isManageTimeSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		TimeSettingHistoryResource.TimeSettingHistory param = new TimeSettingHistoryResource.TimeSettingHistory();
		param.baseId = setting.id;
		param.name_L0 = 'テスト工数設定_L0';
		param.name_L1 = 'テスト工数設定_L1';
		param.name_L2 = 'テスト工数設定_L2';
		param.comment = 'Testコメント';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TimeSettingHistoryResource.CreateApi api = new TimeSettingHistoryResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg(AppLanguage.JA.value).Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 工数設定履歴レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeRequiredErrorTest() {
		Test.startTest();

		TimeSettingHistoryResource.TimeSettingHistory param = new TimeSettingHistoryResource.TimeSettingHistory();
		TimeSettingHistoryResource.CreateApi api = new TimeSettingHistoryResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 工数設定履歴レコードを1件作成する(異常系:有効開始日が不正)
	 */
	@isTest
	static void creativeInvalidValidDateErrorTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c base = ComTestDataUtility.createTimeSettingWithHistory('工数設定ベース', company.Id);

		// 履歴の最新日を今日にする
		List<TimeSettingHistory__c> testHistoryList = [SELECT Id FROM TimeSettingHistory__c WHERE BaseId__c =:base.Id];
		Date today = System.today();
		for (Integer i = 0, n = testHistoryList.size(); i < n; i++) {
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		Test.startTest();

		TimeSettingHistoryResource.TimeSettingHistory param = new TimeSettingHistoryResource.TimeSettingHistory();
		param.baseId = base.Id;
		param.name_L0 = 'テスト工数設定_L0';
		param.name_L1 = 'テスト工数設定_L1';
		param.name_L2 = 'テスト工数設定_L2';
		param.validDateFrom = today - 1;  // "最新日付以降"ではないのでエラー

		TimeSettingHistoryResource.CreateApi api = new TimeSettingHistoryResource.CreateApi();

		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
		// 改定日は最新履歴の有効開始日以降を指定してください。
		System.assertEquals(ComMessage.msg().Admin_Err_InvalidRevisionDate, ex.getMessage());
	}

	// 更新APIは一旦廃止したためコメントアウト
	// /**
	//  * 工数設定レコードを1件更新する(正常系)
	//  */
	// @isTest
	// static void updatePositiveTest() {
   //
	// 	ComCompany__c company = ComTestDataUtility.createTestCompany();
	// 	TimeSettingBase__c setting = ComTestDataUtility.createTimeSettingWithHistory('Test工数設定', company.Id);
	// 	TimeSettingHistory__c history = [SELECT Id FROM TimeSettingHistory__c LIMIT 1];
   //
	// 	Test.startTest();
   //
	// 	// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
	// 	// 全パラメータの検証は createPositiveTest() で行っている
	// 	Map<String, Object> param = new Map<String, Object> {
	// 		'id' => history.id,
	// 		'name_L0' => '工数設定履歴更新テスト'
	// 	};
   //
	// 	TimeSettingHistoryResource.UpdateApi api = new TimeSettingHistoryResource.UpdateApi();
	// 	Object res = api.execute(param);
   //
	// 	Test.stopTest();
   //
	// 	System.assertEquals(null, res);
   //
	// 	// 工数設定履歴レコードが更新されていること
	// 	List<TimeSettingHistory__c> historyList = [
	// 		SELECT
	// 			Name_L0__c
	// 		FROM TimeSettingHistory__c
	// 		WHERE Id = :(String)param.get('id')];
   //
	// 	System.assertEquals(1, historyList.size());
	// 	System.assertEquals((String)param.get('name_L0'), historyList[0].Name_L0__c);
	// }
   //
	// /**
	//  * 工数設定履歴レコードを1件更新する(異常系:必須パラメータが未設定)
	//  */
	// @isTest
	// static void updateNegativeTest() {
	// 	Test.startTest();
   //
	// 	TimeSettingHistoryResource.TimeSettingHistory param = new TimeSettingHistoryResource.TimeSettingHistory();
   //
	// 	App.ParameterException ex;
	// 	try {
	// 		Object res = (new TimeSettingHistoryResource.UpdateApi()).execute(param);
	// 	} catch (App.ParameterException e) {
	// 		ex = e;
	// 	}
   //
	// 	Test.stopTest();
   //
	// 	System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	// }

	/**
	 * 工数設定履歴レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c setting = ComTestDataUtility.createTimeSettingWithHistory('Test工数設定', company.Id);
		TimeSettingHistory__c delRecord = [SELECT Id, Name, Name_L0__c, BaseId__c, ValidFrom__c, ValidTo__c FROM TimeSettingHistory__c LIMIT 1];
		// 履歴が2件以上存在しないと削除できないため、履歴を追加する
		TimeSettingHistory__c copyRecord = delRecord.clone();
		copyRecord.ValidFrom__c = delRecord.ValidTo__c;
		copyRecord.ValidTo__c = delRecord.ValidTo__c.addMonths(1);
		copyRecord.UniqKey__c = copyRecord.Name + '-' + copyRecord.ValidFrom__c;
		insert copyRecord;

		Test.startTest();

		TimeSettingHistoryResource.DeleteOption param = new TimeSettingHistoryResource.DeleteOption();
		param.id = delRecord.id;

		Object res = (new TimeSettingHistoryResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 工数設定履歴レコードが論理削除されること
		List<TimeSettingHistory__c> recordList = [SELECT Id, Removed__c FROM TimeSettingHistory__c WHERE Id =:delRecord.Id];
		System.assertEquals(1, recordList.size());
		System.assertEquals(true, recordList[0].Removed__c);
	}

	/**
	 * 工数設定履歴レコードを1件削除する(異常系:工数設定の管理権限なし)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		TimeSettingBaseEntity setting = testData.createTimeSetting('Test設定');
		TimeSettingHistoryEntity delRecord = setting.getHistoryList().get(0);
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 工数設定の管理権限を無効に設定する
		testData.permission.isManageTimeSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		TimeSettingHistoryResource.DeleteOption param = new TimeSettingHistoryResource.DeleteOption();
		param.id = delRecord.id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				TimeSettingHistoryResource.DeleteApi api = new TimeSettingHistoryResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg(AppLanguage.JA.value).Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 工数設定履歴レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		TimeSettingHistoryResource.DeleteOption param = new TimeSettingHistoryResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new TimeSettingHistoryResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 工数設定履歴レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeSettingBase__c> settings = ComTestDataUtility.createTimeSettingsWithHistory('テスト工数設定', company.Id, 3);
		TimeSettingHistory__c history = [SELECT Id FROM TimeSettingHistory__c WHERE BaseId__c IN :settings LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		TimeSettingHistoryResource.SearchApi api = new TimeSettingHistoryResource.SearchApi();
		TimeSettingHistoryResource.SearchResult res = (TimeSettingHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 工数設定履歴レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 工数設定履歴レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c setting = ComTestDataUtility.createTimeSettingWithHistory('テスト工数設定', company.Id);
		TimeSettingHistory__c history = [SELECT Id FROM TimeSettingHistory__c WHERE BaseId__c = :setting.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		TimeSettingHistoryResource.SearchApi api = new TimeSettingHistoryResource.SearchApi();
		TimeSettingHistoryResource.SearchResult res = (TimeSettingHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 工数設定履歴レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [SELECT Name_L1__c FROM TimeSettingHistory__c WHERE Id =:history.Id LIMIT 1];
		System.assertEquals(history.Name_L1__c, res.records[0].name);
	}

	/**
	 * 部署レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeSettingBase__c setting = ComTestDataUtility.createTimeSettingWithHistory('テスト工数設定', company.Id);
		TimeSettingHistory__c history = [SELECT Id FROM TimeSettingHistory__c WHERE BaseId__c = :setting.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		TimeSettingHistoryResource.SearchApi api = new TimeSettingHistoryResource.SearchApi();
		TimeSettingHistoryResource.SearchResult res = (TimeSettingHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 工数設定履歴レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [SELECT Name_L2__c FROM TimeSettingHistory__c WHERE BaseId__c =:setting.Id LIMIT 1];
		System.assertEquals(history.Name_L2__c, res.records[0].name);
	}

	/**
	 * 工数設定履歴レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeSettingBase__c> settings = ComTestDataUtility.createTimeSettingsWithHistory('Test設定', company.Id, 3);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>();
		TimeSettingHistoryResource.SearchApi api = new TimeSettingHistoryResource.SearchApi();
		TimeSettingHistoryResource.SearchResult res = (TimeSettingHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		List<TimeSettingHistory__c> historyList = [
			SELECT
				Id,
				BaseId__c,
				UniqKey__c,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				ValidFrom__c,
				ValidTo__c,
				HistoryComment__c
			FROM TimeSettingHistory__c];

		// 工数設定履歴レコードが取得できること
		System.assertEquals(historyList.size(), res.records.size());

		for (Integer i = 0, n = historyList.size(); i < n; i++) {
			TimeSettingHistory__c history = historyList[i];
			TimeSettingHistoryResource.TimeSettingHistory dto = res.records[i];

			System.assertEquals(history.Id, dto.id);
			System.assertEquals(history.BaseId__c, dto.baseId);
			System.assertEquals(history.Name_L0__c, dto.name);	// 部署名のデフォルトはL0を返す
			System.assertEquals(history.Name_L0__c, dto.name_L0);
			System.assertEquals(history.Name_L1__c, dto.name_L1);
			System.assertEquals(history.Name_L2__c, dto.name_L2);
			System.assertEquals(history.ValidFrom__c, dto.validDateFrom);
			System.assertEquals(history.ValidTo__c, dto.validDateTo);
			System.assertEquals(history.HistoryComment__c, dto.comment);
		}
	}

}
