/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpFinanceApprovalService
 */
@isTest
private class ExpFinanceApprovalServiceTest {

	/**
	 * 精算確定対象経費申請ID一覧取得処理のテスト Test for getting request ID list
	 */
	@isTest static void getRequestIdListTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);
		List<ExpRecordEntity> recordList1 = testData.createExpRecords(reportList[0].id, Date.today(), testData.expType, testData.employee, 1);
		List<ExpRecordEntity> recordList2 = testData.createExpRecords(reportList[1].id, Date.today(), testData.expType, testData.employee, 1);

		// 社員に上長を設定 Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請を実行 Submit approval
		ExpReportRequestService service = new ExpReportRequestService();
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportList[0].id, null);
		service.submitRequest(reportList[1].id, null);
		service.submitRequest(reportList[2].id, null);

		// 申請レコードを取得 Get created ExpReportRequest list
		List<ExpReportRequest__c> requestObjList = [
				SELECT Id
				FROM ExpReportRequest__c
				ORDER BY ReportNo__c DESC];

		// ステータスを「承認済み」に更新 Update satus to approved
		requestObjList[0].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[1].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[0].CancelType__c = null;
		requestObjList[1].CancelType__c = null;
		requestObjList[0].AccountingStatus__c = 'None';
		requestObjList[1].AccountingStatus__c = 'None';
		requestObjList[0].AuthorizedTime__c = null;
		requestObjList[1].AuthorizedTime__c = null;
		requestObjList[0].ExportedTime__c = null;
		requestObjList[1].ExportedTime__c = null;

		update requestObjList;

		Test.startTest();

		ExpReportRequestRepository.SortKey sortKey = ExpReportRequestRepository.SortKey.REPORT_NO;
		ExpReportRequestRepository.SortOrder sortOrder = ExpReportRequestRepository.SortOrder.ORDER_DESC;

		List<Id> deptIds = new List<Id>();
		deptIds.add(testData.department.Id);
		List<Id> empIds = new List<Id>();
		empIds.add(testData.employee.Id);
		List<String> financeStatusList = new List<String>();
		financeStatusList.add('Approved');

		ExpFinanceApprovalService.SearchFilter filter = new ExpFinanceApprovalService.SearchFilter();
		filter.departmentBaseIds = deptIds;
		filter.empBaseIds = empIds;
		filter.financeStatusList = financeStatusList;
		ExpReportRequestRepository.DateRange todayRange = new ExpReportRequestRepository.DateRange();
		ExpReportRequestRepository.AmountRange amountRange = new ExpReportRequestRepository.AmountRange();
		todayRange.startDate = Date.today();
		todayRange.endDate = todayRange.startDate.addDays(1);
		amountRange.startAmount = 0;
		amountRange.endAmount = 3000;
		filter.requestDateRange = todayRange;
		filter.accountingDateRange = todayRange;
		filter.amountRange = amountRange;

		ExpFinanceApprovalService.RequestIdListResult result = new ExpFinanceApprovalService().getRequestIdList(testData.employee.companyId, sortKey, sortOrder, filter);

		Test.stopTest();
		System.assertEquals(2, result.totalSize);
		for (Integer i = 0; i < result.requestIdList.size(); i++) {
			System.assertEquals(requestObjList[i].Id, result.requestIdList[i]);
		}
	}


	/**
	 * 精算確定対象経費申請一覧データ取得処理のテスト Test for getting ExpReportRequest list
	 */
	@isTest static void getRequestListTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);
		List<ExpRecordEntity> recordList1 = testData.createExpRecords(reportList[0].id, Date.today(), testData.expType, testData.employee, 1);
		List<ExpRecordEntity> recordList2 = testData.createExpRecords(reportList[1].id, Date.today(), testData.expType, testData.employee, 1);

		// 領収書を作成 Create receipt
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion contentVersion = new ContentVersion(
			Title = 'Test',
			PathOnClient = 'Test.jpg',
			VersionData = Blob.valueOf(bodyString));
		insert contentVersion;
		ContentDocument contentDocument = [SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Title = :contentVersion.Title LIMIT 1];
		ContentDocumentLink link = new ContentDocumentLink(
			ContentDocumentId = contentDocument.Id,
			LinkedEntityId = recordList1[0].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		insert link;

		// Create ExpTypes
		List<ExpTypeEntity> expTypeEntity = testData.createExpTypes('OPTIONAL', 1);
		expTypeEntity[0].fileAttachment = ExpFileAttachment.OPTIONAL;
		new ExpTypeService().updateExpType(expTypeEntity[0], new List<Id>());

		// 領収書を経費明細に紐付け Link receipt to expense record
		recordList1[0].recordType = ExpRecordType.GENERAL;
		recordList1[0].recordItemList[0].expTypeId = expTypeEntity[0].id;
		List<AppContentDocumentEntity> contentEntityList = new AppContentDocumentRepository().getEntityList(new List<Id>{contentDocument.Id});
		recordList1[0].replaceReceiptList(contentEntityList);
		new ExpRecordRepository().saveEntityList(recordList1);

		reportList[0].replaceRecordList(recordList1);
		reportList[1].replaceRecordList(recordList2);

		// 社員に上長を設定 Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請を実行 Submit approval
		ExpReportRequestService service = new ExpReportRequestService();
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportList[0].id, null);
		service.submitRequest(reportList[1].id, null);
		service.submitRequest(reportList[2].id, null);

		// 申請レコードを取得 Get created ExpReportRequest list
		List<ExpReportRequest__c> requestObjList = [
				SELECT
					Id,
					ExpReportId__c,
					Status__c,
					ConfirmationRequired__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c
				FROM ExpReportRequest__c
				ORDER BY Id ASC];

		// ステータスを「承認済み」に更新 Update satus to approved
		requestObjList[0].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[1].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[0].CancelType__c = null;
		requestObjList[1].CancelType__c = null;
		requestObjList[0].AccountingStatus__c = 'None';
		requestObjList[1].AccountingStatus__c = 'None';
		requestObjList[0].AuthorizedTime__c = null;
		requestObjList[1].AuthorizedTime__c = null;
		requestObjList[0].ExportedTime__c = null;
		requestObjList[1].ExportedTime__c = null;
		update requestObjList;

		Test.startTest();

		List<String> requestIdList = new List<String>{requestObjList[0].Id, requestObjList[1].Id};
		List<ExpReportRequestEntity> requestEntityList = new ExpFinanceApprovalService().getRequestList(requestIdList);

		Test.stopTest();

		System.assertEquals(2, requestEntityList.size());
		for (Integer i = 0; i < requestEntityList.size(); i++) {
			System.assertEquals(requestObjList[i].Id, requestEntityList[i].id);
			System.assertEquals(requestObjList[i].Status__c, requestEntityList[i].status.value);
			System.assertEquals(requestObjList[i].RequestTime__c, AppDatetime.convertDatetime(requestEntityList[i].requestTime));
			System.assertEquals(requestObjList[i].ExpReportId__c, requestEntityList[i].expReportId);
			System.assertEquals(requestObjList[i].EmployeeHistoryId__c, requestEntityList[i].employeeHistoryId);
			System.assertEquals(requestObjList[i].DepartmentHistoryId__c, requestEntityList[i].departmentHistoryId);
			System.assertEquals(requestObjList[i].ExpReportId__c == reportList[0].id ? true : false, requestEntityList[i].hasReceipts);
		}
	}

	/**
	 * 経費申請詳細データ取得処理のテスト Test for get ExpReportRequest detail API
	 */
	@isTest
	static void getExpReportRequestApiTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee,
				testData.job, testData.costCenter.CurrentHistoryId__c, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType,
				testData.employee, testData.costCenter.CurrentHistoryId__c, 3);
		ExpRequestEntity expRequest = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRequestRecordEntity> requestRecordList = testData.createExpRequestRecords(expRequest.id, Date.today(), testData.expType, testData.employee, testData.job, 1);
		ExpReport__c reportObj = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id =: report.Id];
		reportObj.ExpPreRequestId__c = expRequest.Id;
		update reportObj;

		// 社員に上長を設定 Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請を実行 Submit approval
		ExpReportRequestService reqService = new ExpReportRequestService();
		reqService.isTestModeAndBypassExpReportValidation = true;
		reqService.submitRequest(report.id, null);

		// 作成された申請データを取得
		ExpReportRequest__c requestObj = [
				SELECT
					Id,
					Name,
					ExpReportId__c,
					Comment__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					Approver01Id__c
				FROM ExpReportRequest__c
				WHERE ExpReportId__c = :report.id];

		requestObj.Status__c = AppRequestStatus.APPROVED.value;
		update requestObj;

		Test.startTest();

		ExpFinanceApprovalService appService = new ExpFinanceApprovalService();

		// 申請IDを指定して実行
		try {
			ExpReportEntity result = appService.getExpReport(requestObj.Id);

			ExpReportRequestEntity request = new ExpReportRequestRepository().getEntity(requestObj.Id);

			System.assertEquals(report.id, result.id);
			System.assertEquals('exp00000001', result.reportNo);
			System.assertEquals(report.subject, result.subject);
			System.assertEquals(report.accountingDate, result.accountingDate);
			System.assertEquals(report.totalAmount, result.totalAmount);
			System.assertEquals(report.remarks, result.remarks);
			System.assertEquals(report.jobName, result.jobName);
			System.assertEquals(report.costCenterName, result.costCenterName);
			System.assertEquals(expRequest.Id, result.expPreRequestId);

			// 経費明細
			System.assertEquals(recordList.size(), result.recordList.size());
			for (Integer i = 0; i < result.recordList.size(); i++) {
				ExpRecordEntity expectedRecordEntity = recordList[i];
				ExpRecordEntity actualRecordEntity = result.recordList[i];

				System.assertEquals(expectedRecordEntity.id, actualRecordEntity.id);
				System.assertEquals(expectedRecordEntity.recordDate, actualRecordEntity.recordDate);
				System.assertEquals(expectedRecordEntity.amount, actualRecordEntity.amount);
				System.assertEquals(expectedRecordEntity.order, actualRecordEntity.order);

				for (Integer itemIndex = 0; itemIndex < expectedRecordEntity.recordItemList.size(); itemIndex++) {
					ExpRecordItemEntity recordItemEntity = expectedRecordEntity.recordItemList[itemIndex];
					ExpRecordItemEntity actualRecordItemEntity = actualRecordEntity.recordItemList[itemIndex];

					System.assertEquals(recordItemEntity.id, actualRecordItemEntity.id);
					System.assertEquals(recordItemEntity.expTypeId, actualRecordItemEntity.expTypeId);
					System.assertEquals(recordItemEntity.expTypeName, actualRecordItemEntity.expTypeName);
					System.assertEquals(recordItemEntity.amount, actualRecordItemEntity.amount);
					System.assertEquals(recordItemEntity.remarks, actualRecordItemEntity.remarks);
				}
			}
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

		// 申請IDを指定して実行(該当申請なし)
		try {
			ExpReportEntity result = appService.getExpReport(report.id);

			System.assert(false, '例外が発生しませんでした');
		} catch (App.IllegalStateException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

		Test.stopTest();
	}

	/**
	 * 精算確定承認処理のテスト Test for approving ExpReportRequest
	 */
	@isTest static void approveRequestTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);

		// 社員に上長を設定 Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請を実行 Submit approval
		ExpReportRequestService service = new ExpReportRequestService();
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportList[0].id, null);
		service.submitRequest(reportList[1].id, null);
		service.submitRequest(reportList[2].id, null);

		// 申請レコードを取得 Get created ExpReportRequest list
		List<ExpReportRequest__c> requestObjList = [
				SELECT
					Id,
					Status__c
				FROM ExpReportRequest__c
				ORDER BY Id];

		// ステータスを「承認済み」に更新 Update satus to approved
		requestObjList[0].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[1].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[2].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[0].CancelType__c = null;
		requestObjList[1].CancelType__c = null;
		requestObjList[2].CancelType__c = null;
		requestObjList[0].AccountingStatus__c = 'None';
		requestObjList[1].AccountingStatus__c = 'None';
		requestObjList[2].AccountingStatus__c = 'None';
		requestObjList[0].AuthorizedTime__c = null;
		requestObjList[1].AuthorizedTime__c = null;
		requestObjList[2].AuthorizedTime__c = null;
		requestObjList[0].ExportedTime__c = null;
		requestObjList[1].ExportedTime__c = null;
		requestObjList[2].ExportedTime__c = null;

		update requestObjList;

		Test.startTest();

		List<String> requestIdList = new List<String>{requestObjList[0].Id, requestObjList[1].Id, requestObjList[2].Id};
		new ExpFinanceApprovalService().approve(requestIdList);

		Test.stopTest();

		// 申請レコードを取得 Get created ExpReportRequest list
		List<ExpReportRequest__c> resultObjList = [
				SELECT
					Id,
					ExpReportId__c,
					AccountingStatus__c,
					AuthorizedTime__c
				FROM ExpReportRequest__c
				ORDER BY Id];

		for (Integer i = 0; i < requestObjList.size(); i++) {
			System.assertEquals(ExpAccountingStatus.AUTHORIZED.value, resultObjList[i].AccountingStatus__c);
			System.assert(resultObjList[i].AuthorizedTime__c != null);
		}

		// 経費編集履歴レコードを取得 Get ExpModificationHistory list
		List<ExpModificationHistory__c> historyObjList = [
				SELECT
					Id,
					ExpRequestId__c,
					ExpReportId__c,
					ExpRecordId__c,
					ExpRecordItemId__c,
					Field__c,
					FieldLabel__c,
					OldValue__c,
					NewValue__c,
					Comment__c,
					ModifiedBy__c,
					ModifiedDateTime__c
				FROM ExpModificationHistory__c
				ORDER BY ExpRequestId__c];

		for (Integer i = 0; i < historyObjList.size(); i++) {
			System.assertEquals(resultObjList[i].Id, historyObjList[i].ExpRequestId__c);
			System.assertEquals(resultObjList[i].ExpReportId__c, historyObjList[i].ExpReportId__c);
			System.assertEquals(null, historyObjList[i].ExpRecordId__c);
			System.assertEquals(null, historyObjList[i].ExpRecordItemId__c);
			System.assertEquals(ExpFinanceApprovalService.HISTORY_TYPE_ACCOUNTING_AUTHORIZED, historyObjList[i].Field__c);
			System.assertEquals(null, historyObjList[i].FieldLabel__c);
			System.assertEquals(null, historyObjList[i].OldValue__c);
			System.assertEquals(null, historyObjList[i].NewValue__c);
			System.assertEquals(null, historyObjList[i].Comment__c);
			System.assertEquals(testData.employee.id, historyObjList[i].ModifiedBy__c);
			System.assert(historyObjList[i].ModifiedDateTime__c != null);
		}
	}

	/**
	 * 精算確定却下処理のテスト Test for rejecting ExpReportRequest
	 */
	@isTest static void rejectRequestTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);

		// 社員に上長を設定 Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請を実行 Submit approval
		ExpReportRequestService service = new ExpReportRequestService();
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportList[0].id, null);
		service.submitRequest(reportList[1].id, null);
		service.submitRequest(reportList[2].id, null);

		// 申請レコードを取得 Get created ExpReportRequest list
		List<ExpReportRequest__c> requestObjList = [
				SELECT
					Id,
					Status__c
				FROM ExpReportRequest__c
				ORDER BY Id];

		// ステータスを「承認済み」に更新 Update satus to approved
		requestObjList[0].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[1].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[2].Status__c = AppRequestStatus.APPROVED.value;
		requestObjList[0].CancelType__c = null;
		requestObjList[1].CancelType__c = null;
		requestObjList[2].CancelType__c = null;
		requestObjList[0].AccountingStatus__c = 'None';
		requestObjList[1].AccountingStatus__c = 'None';
		requestObjList[2].AccountingStatus__c = 'None';
		requestObjList[0].AuthorizedTime__c = null;
		requestObjList[1].AuthorizedTime__c = null;
		requestObjList[2].AuthorizedTime__c = null;
		requestObjList[0].ExportedTime__c = null;
		requestObjList[1].ExportedTime__c = null;
		requestObjList[2].ExportedTime__c = null;
		update requestObjList;

		Test.startTest();

		List<String> requestIdList = new List<String>{requestObjList[0].Id, requestObjList[1].Id, requestObjList[2].Id};
		String comment = 'test';
		new ExpFinanceApprovalService().reject(requestIdList, comment);

		Test.stopTest();

		// 申請レコードを取得 Get created ExpReportRequest list
		List<ExpReportRequest__c> resultObjList = [
				SELECT
					Id,
					ExpReportId__c,
					Status__c,
					CancelType__c,
					ConfirmationRequired__c,
					ProcessComment__c,
					LastApproverId__c,
					LastApproveTime__c,
					CancelComment__c,
					AccountingStatus__c,
					AuthorizedTime__c
				FROM ExpReportRequest__c
				ORDER BY Id];

		for (Integer i = 0; i < resultObjList.size(); i++) {
			System.assertEquals(AppRequestStatus.DISABLED.value, resultObjList[i].Status__c);
			System.assertEquals(AppCancelType.CANCELED.value, resultObjList[i].CancelType__c);
			System.assertEquals(true, resultObjList[i].ConfirmationRequired__c);
			System.assertEquals(null, resultObjList[i].ProcessComment__c);
			System.assertEquals(null, resultObjList[i].LastApproverId__c);
			System.assertEquals(null, resultObjList[i].LastApproveTime__c);
			System.assertEquals(comment, resultObjList[i].CancelComment__c);
			System.assertEquals(ExpAccountingStatus.REJECTED.value, resultObjList[i].AccountingStatus__c);
			System.assertEquals(null, resultObjList[i].AuthorizedTime__c);
		}

		// 経費編集履歴レコードを取得 Get ExpModificationHistory list
		List<ExpModificationHistory__c> historyObjList = [
				SELECT
					Id,
					ExpRequestId__c,
					ExpReportId__c,
					ExpRecordId__c,
					ExpRecordItemId__c,
					Field__c,
					FieldLabel__c,
					OldValue__c,
					NewValue__c,
					Comment__c,
					ModifiedBy__c,
					ModifiedDateTime__c
				FROM ExpModificationHistory__c
				ORDER BY ExpRequestId__c];

		for (Integer i = 0; i < historyObjList.size(); i++) {
			System.assertEquals(resultObjList[i].Id, historyObjList[i].ExpRequestId__c);
			System.assertEquals(resultObjList[i].ExpReportId__c, historyObjList[i].ExpReportId__c);
			System.assertEquals(null, historyObjList[i].ExpRecordId__c);
			System.assertEquals(null, historyObjList[i].ExpRecordItemId__c);
			System.assertEquals(ExpFinanceApprovalService.HISTORY_TYPE_ACCOUNTING_REJECTED, historyObjList[i].Field__c);
			System.assertEquals(null, historyObjList[i].FieldLabel__c);
			System.assertEquals(null, historyObjList[i].OldValue__c);
			System.assertEquals(null, historyObjList[i].NewValue__c);
			System.assertEquals(comment, historyObjList[i].Comment__c);
			System.assertEquals(testData.employee.id, historyObjList[i].ModifiedBy__c);
			System.assert(historyObjList[i].ModifiedDateTime__c != null);
		}
	}
}