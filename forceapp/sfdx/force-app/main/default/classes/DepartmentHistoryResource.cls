/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 部署レコード操作APIを実装するクラス
 */
public with sharing class DepartmentHistoryResource extends Repository.BaseRepository {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 部署履歴レコードを表すクラス
	 */
	public class DepartmentHistory implements RemoteApi.RequestParam {
		/** 履歴レコードID */
		public String id;
		/** ベースレコードID */
		public String baseId;
		/** 部署名 ※取得専用。組織の言語設定に応じて言語が切り替わる */
		public String name;
		/** 部署名(言語0) */
		public String name_L0;
		/** 部署名(言語1) */
		public String name_L1;
		/** 部署名(言語2) */
		public String name_L2;
		/** 部署管理者のユーザレコードID */
		public String managerId;
		/** 部署管理者 */
		public LookupField manager;
		/** 親部署ID */
		public String parentId;
		/** 親部署 ※取得専用 */
		public LookupField parent;
		/** 備考 */
		public String remarks;
		/** 履歴コメント */
		public String comment;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;

		/**
		 * @description 部署履歴エンティティを作成する
		 * @param  paramMap リクエストパラメータのMap
		 * @param  isUpdate レコード更新時はtrue、作成時はfalse
		 * @return  作成or更新した部署履歴レコード
		 */
		public DepartmentHistoryEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate){

			DepartmentHistoryEntity entity = new DepartmentHistoryEntity();

			// 更新時にIDがブランクの場合はエラー
			if (isUpdate && String.isBlank(this.id)) {
				// メッセージ：IDを指定してください
				throw new App.ParameterException(MESSAGE.Com_Err_Specify(new List<String>{'ID'}));
			}

			// レコード更新時はIDをセット
			if(isUpdate){
				entity.setId(this.id);
			}
			// ベースIDは更新不可のため、新規作成時のみセット
			if(!isUpdate && paramMap.containsKey('baseId')){
				entity.baseId = this.baseId;
			}
			if(paramMap.containsKey('name_L0')){
				entity.name = this.name_L0;
				entity.nameL0 = this.name_L0;
			}
			if(paramMap.containsKey('name_L1')){
				entity.nameL1 = this.name_L1;
			}
			if(paramMap.containsKey('name_L2')){
				entity.nameL2 = this.name_L2;
			}
			if(paramMap.containsKey('managerId')){
				entity.managerId = this.managerId;
			}
			if(paramMap.containsKey('parentId')){
				entity.parentBaseId = this.parentId;
			}
			if(paramMap.containsKey('remarks')){
				entity.remarks = this.remarks;
			}
			if(paramMap.containsKey('comment')){
				entity.historyComment = this.comment;
			}
			if(paramMap.containsKey('validDateFrom')){
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (entity.validFrom == null) {
				// デフォルトは実行日
				entity.validFrom = AppDate.today();
			}
			if(paramMap.containsKey('validDateTo')){
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (entity.validTo == null) {
				// デフォルト値は失効日の最大日付
				entity.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}

			return entity;
		}
	}

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		public String name;
	}

	/**
	 * @description 部署レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したベースレコードID */
		public String id;
	}

	/**
	 * @description 部署レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_DEPARTMENT;

		/**
		 * @description 部署レコードを1件作成する
		 * @param  req リクエストパラメータ(Department)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			//------------------------------------------------------------------------
			// 【仮仕様】
			// 現在指定可能な改定日(有効開始日)は末尾の履歴の有効開始日以降とする。
			// 改定機能自体は実装済みでマスタ全体の有効期間内の日付で改定できるようになっているが
			// UI側が対応できていないため。
			//------------------------------------------------------------------------

			final Boolean isUpdate = false;
			DepartmentService service = new DepartmentService();
			DepartmentRepository repo = new DepartmentRepository();

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// # "DepartmentResource.Department.class"としないとコンパイルが通らないので(原因不明)、フルパスで指定している
			DepartmentHistory param = (DepartmentHistory)req.getParam(DepartmentHistoryResource.DepartmentHistory.class);
			DepartmentHistoryEntity newHistory = param.createEntity(req.getParamMap(), isUpdate);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 末尾の履歴(有効開始日が最新の履歴)を取得する
			DepartmentBaseEntity base = repo.getEntity(newHistory.baseId);
			if (base == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Department}));
			}
			DepartmentHistoryEntity latestHistory = (DepartmentHistoryEntity)base.getLatestSuperHistory();

			// 追加の履歴の有効開始日が末尾の履歴の有効開始日以降であることを確認する
			if (newHistory.validFrom.getDate() < latestHistory.validFrom.getDate()) {
				// メッセージ：改定日は最新の履歴の有効開始日以降を指定してください
				throw new App.ParameterException(MESSAGE.Admin_Err_InvalidRevisionDate);
			}

			// マスタ全体の失効日(末尾の履歴の失効日)を更新する
			if (newHistory.validTo.getDate() != latestHistory.validTo.getDate()) {
				service.updateValidTo(newHistory.baseId, newHistory.validTo);
			}
			// 履歴を改定する
			service.reviseHistory(newHistory);

			// 改定日に有効な履歴のIDを取得する
			DepartmentBaseEntity revisedBase = repo.getEntity(newHistory.baseId, newHistory.validFrom);
			SaveResult res = new SaveResult();
			res.Id = revisedBase.getHistoryList().get(0).id;
			return res;

		}

	}

	/**
	 * @desctiprion 部署レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/**
		 * @description 部署レコードを1件更新する
		 * @param  req リクエストパラメータ(Department)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 必要になったら実装してください。
			throw new App.UnsupportedException('部署履歴更新APIは利用できません。');

			// // パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// DepartmentHistory param = (DepartmentHistory)req.getParam(DepartmentHistory.class);
			// DepartmentHistoryResource.saveRecord(param, req.getParamMap(), true);
		}
	}

	/**
	 * @description 部署レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}
	}

	/**
	 * 部署レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_DEPARTMENT;

		/**
		 * @description 部署履歴レコードを1件削除する(論理削除)
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new DepartmentService().deleteHistoryListWithPropagation(new List<Id>{param.id});
			} catch (App.RecordNotFoundException e) {
				// すでに削除済みの場合は成功レスポンスを返す
			}

			// 成功レスポンスをセットする
			return null;
		}

	}

	/**
	 * @description 部署レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public DepartmentHistory[] records;
	}

	/**
	 * @description 部署履歴レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** 部署履歴レコードID */
		public String id;
		/** 関連する部署ベースレコードID */
		public String baseId;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {

			// 履歴ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// ベースID
			if (String.isNotBlank(this.baseId)) {
				try {
					System.Id.valueOf(this.baseId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'baseId'}));
				}
			}

		}

	}

	/**
	 * @description 部署レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 部署レコードを検索する
		 * @param  parameter リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);

			// パラメータのバリデーション
			param.validate();

			// 部署を検索
			DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
			filter.historyIds = String.isBlank(param.id) ? null : new List<Id>{param.id};
			filter.baseIds = String.isBlank(param.baseId) ? null : new List<Id>{param.baseId};
			List<DepartmentBaseEntity> baseList =
					(new DepartmentRepository()).searchEntityWithHistory(filter, false, DepartmentRepository.HistorySortOrder.VALID_FROM_DESC);

			// 親部署のエンティティを取得
			DepartmentService service = new DepartmentService();
			Set<Id> parentIds = new Set<Id>();
			for (DepartmentBaseEntity base : baseList) {
				for (DepartmentHistoryEntity history : base.getHistoryList()) {
					if (history.parentBaseId != null) {
						parentIds.add(history.parentBaseId);
					}
				}
			}
			Map<Id, DepartmentBaseEntity> parentBaseMap = service.getBaseMap(new List<Id>(parentIds));

			// エンティティをレスポンスオブジェクトに変換
			List<DepartmentHistory> departmentList = new List<DepartmentHistory>();
			for (DepartmentBaseEntity base : baseList) {
				for (DepartmentHistoryEntity history : base.getHistoryList()) {
					DepartmentHistory d = new DepartmentHistory();
					d.id = history.id;
					d.baseId = history.baseId;
					d.name = history.nameL.getValue();
					d.name_L0 = history.nameL.valueL0;
					d.name_L1 = history.nameL.valueL1;
					d.name_L2 = history.nameL.valueL2;
					d.validDateFrom = AppConverter.dateValue(history.validFrom);
					d.validDateTo = AppConverter.dateValue(history.validTo);
					d.remarks = history.remarks;
					d.comment = history.historyComment;

					d.parentId = history.parentBaseId;
					d.parent = new LookupField();
					d.parent.name = history.parentBaseId != null ? getHistoryName(parentBaseMap.get(history.parentBaseId), history.validFrom) : null;

					d.managerId = history.managerId;
					d.manager = new LookupField();
					d.manager.name = history.manager != null ? history.manager.fullName.getFullName() : null;

					departmentList.add(d);
				}
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = departmentList;
			return res;
		}

		/**
		 * 指定した日の部署名を取得する
		 * @return 部署名。ただし、有効な履歴が存在しない場合はnullを返却
		 */
		private String getHistoryName(DepartmentBaseEntity base, AppDate targetDate) {
			DepartmentHistoryEntity history = (DepartmentHistoryEntity)base.getSuperHistoryByDate(targetDate);
			if (history == null) {
				return null;
			}
			return history.nameL.getValue();
		}
	}

}
