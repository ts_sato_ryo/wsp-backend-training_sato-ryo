/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 承認
 *
 * 承認に関するサービスを提供するクラス
 */
public with sharing class ApprovalService {

	/**
	 * 申請ステータス
	 */
	public enum Status {
		Started,
		NoResponse,
		Approved,
		Disabled,
		Removed,
		Rejected
	}

	public enum ProcessInstanceStatus {
		Pending,
		Approved,
		Rejected
	}

	/**
	 * 取消種別
	 */
	public enum CancelType {
		Rejected,
		Removed,
		Canceled
	}

	/**
	 * 処理種別
	 */
	private enum ActionType {
		Approve,
		Reject
	}

	/**
	 * 承認履歴
	 */
	public class History {
		/** 履歴ID */
		public String id;
		/** ステップ名 */
		public String stepName;
		/** 承認日時 */
		public String approveTime;
		/** 状況 */
		public String status;
		/** 状況（表示用） */
		public String statusLabel;
		/** 割当先 */
		public String approverName; // TODO 経費の影響調査後、削除する
		/** 実行者名 */
		public String actorName;
		/** 承認者顔写真URL */
		public String actorPhotoUrl;
		/** 承認者コメント */
		public String comment;
		/** 代理操作フラグ */
		public Boolean isDelegated;
	}

	/**
	 * 承認履歴（ソート用）
	 */
	private class SortHistory implements Comparable {
		/** 履歴ID */
		public String id;
		/** 承認日時 */
		public AppDatetime approveTime;

		/**
		 * 比較の結果であるInteger値を返す
		 * 承認日時の降順に並ぶように比較を行い結果を返す
		 * @param 比較対象
		 * @return このインスタンスとcompareToが等しい場合は0、このインスタンスの方が大きい場合は-1、小さい場合は1
		 */
		public Integer compareTo(Object compareTo) {
			SortHistory compareToHistory = (SortHistory)compareTo;
			if (this.approveTime.compareTo(compareToHistory.approveTime) == 0) {
				return 0;
			}
			if (this.approveTime.compareTo(compareToHistory.approveTime) > 0) {
				return -1;
			}
			return 1;
		}
	}

	/**
	 * @description 申請の種類に関わらない共通情報を保持するクラス
	 *
	 * TODO 所有者以外にも代理承認の可否など必要なプロパティがあるため、
	 *      申請系エンティティの共通親クラス作成を検討する
	 */
	private class Request {

		/** 所有者ID */
		public Id ownerId;

		public Request(Id ownerId) {
			this.ownerId = ownerId;
		}
	}

	/**
	 * @description 承認対象の申請件数を取得する
	 * @param  userId ユーザID
	 * @param  objectName オブジェクト名のリスト
	 * @return オブジェクト名と申請件数のMap
	 */
	public Map<String, Integer> getRequestCount(final Id userId, final Set<String> objectNameSet) {
		// Mapを初期化
		Map<String, Integer> requestCountMap = new Map<String, Integer>();
		for (String objectName : objectNameSet) {
			requestCountMap.put(objectName, 0);
		}

		// 指定ユーザの承認待ちになっている申請を取得
		ApprovalProcessRepository repo = new ApprovalProcessRepository();
		List<ApprovalProcessEntity> requestList = repo.getRequestList(userId, requestCountMap.keySet());

		// オブジェクト毎の件数をカウント
		if (requestList != null && !requestList.isEmpty()) {
			for (ApprovalProcessEntity request : requestList) {
				requestCountMap.put(request.targetObjectName, requestCountMap.get(request.targetObjectName) + 1);
			}
		}

		return requestCountMap;
	}

	/**
	 * @description Get numbers of pending requests 承認対象の申請件数を取得する
	 * @param  userIdList List of target user id ユーザIDのリスト
	 * @param  objectName List of target object name オブジェクト名のリスト
	 * @return Map of user id and map of object name and number オブジェクト名と申請件数のMap
	 */
	public Map<Id ,Map<String, Integer>> getMultiUsersRequestCount(final Set<Id> userIdSet, final Set<String> objectNameSet) {
		// Mapを初期化
		Map<Id ,Map<String, Integer>> userRequestCountMap = new Map<Id, Map<String, Integer>>();

		// 指定ユーザの承認待ちになっている申請を取得
		ApprovalProcessRepository repo = new ApprovalProcessRepository();
		Map<Id, List<ApprovalProcessEntity>> requestListMap = repo.getApprovalProcessListByUsers(userIdSet, objectNameSet);

		// オブジェクト毎の件数をカウント
		if (requestListMap != null && !requestListMap.isEmpty()) {
			for (Id userId : requestListMap.keySet()) {
				List<ApprovalProcessEntity> appProcessList = requestListMap.get(userId);
				Map<String, Integer> objectCountMap = new Map<String, Integer>();
				for (ApprovalProcessEntity appProcess : appProcessList) {
					Integer count = objectCountMap.get(appProcess.targetObjectName);
					count = count == null ? 1 : ++count;
					objectCountMap.put(appProcess.targetObjectName, count);
				}
				userRequestCountMap.put(userId, objectCountMap);
			}
		}

		return userRequestCountMap;
	}


	/**
	 * @description 指定された承認待ちになっている承認プロセスを取得する
	 * @param  requestIdList 対象の申請ID
	 * @return 承認待ちの承認プロセスリスト
	 */
	public List<ApprovalProcessWorkitemEntity> getProcessWorkItemList(final List<Id> requestIdList) {
		final Integer maxRecordCount = 1000;
		ApprovalProcessWorkitemRepository processRepo = new ApprovalProcessWorkitemRepository();
		List<ApprovalProcessWorkitemEntity> processWorkitemList
				= new List<ApprovalProcessWorkitemEntity>();
		Integer startIndex = 0;
		// SOQLの文字数制限を超えないようにするため1000件ずつ取得する
		while (startIndex < requestIdList.size()) {
			Integer endIndex = Math.min(startIndex + maxRecordCount - 1, requestIdList.size() - 1);
			List<Id> taregetIds = new List<Id>();
			for (Integer i = startIndex; i <= endIndex; i++) {
				taregetIds.add(requestIdList.get(i));
			}
			processWorkitemList.addAll(processRepo.getEntityListByRequestId(taregetIds));
			startIndex = endIndex + 1;
		}

		return processWorkitemList;
	}

	/**
	 * @description 承認対象の申請IDを取得する
	 * @param  userId ユーザID
	 * @param  objectName オブジェクト名
	 * @return 申請IDのリスト
	 */
	public List<Id> getRequestIdList(final Id userId, final String objectName) {
		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		// 指定ユーザの承認待ちになっている申請レコードのIDを取得
		List<Id> requestIdList = repo.getRequestIdList(userId, objectName);

		return requestIdList;
	}

	/**
	 * @description check if filter contains status of pending
	 * @param filter search condition.
	 * @return if contain true, if not false
	 */
	private Boolean isContainPendingStatus(ApprovalProcessRepository.AdvancedSearchFilter filter) {
		return filter != null && filter.statusList != null &&
				filter.statusList.contains(ProcessInstanceStatus.Pending.name());
	}

	/**
	 * @description check if filter contains status of approval or rejected
	 * @param filter search condition.
	 * @return if contain true, if not false
	 */
	private Boolean isContainApprovedOrRejectedStatus(ApprovalProcessRepository.AdvancedSearchFilter filter) {
		return filter != null && filter.statusList != null && (
				filter.statusList.contains(ProcessInstanceStatus.Approved.name()) ||
				filter.statusList.contains(ProcessInstanceStatus.Rejected.name()));
	}

	/*
	 * @description Check if filter doesnt contains any status.
	 *
	 * @param filter Search condition.
	 *
	 * @return if doesnt contain any, true. if contains, false.
	 */
	private Boolean isNoStatusSelected(ApprovalProcessRepository.AdvancedSearchFilter filter) {
		return filter==null || filter.statusList==null || filter.statusList.isEmpty();
	}

	/**
	 * @description get Request Id List of Expense or Request
	 * @param userId user id
	 * @param objectName name of object. ExpReportRequest or ExpRequestApproval.
	 * @param filter search condition.
	 * @return List of request id
	 */
	public List<Id> getExpRequestIdList(Id userId, String objectName, ApprovalProcessRepository.AdvancedSearchFilter filter) {
		List<Id> requestIdListApprovedRejected = new List<Id>();
		List<Id> requestIdListPending = new List<Id>();
		List<Id> requestIdList = new List<Id>();
		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		// if no status is selected, all status should be selected.
		if (isNoStatusSelected(filter)) {
			Set<String> statusList = new Set<String>{ProcessInstanceStatus.Pending.name(), ProcessInstanceStatus.Approved.name(), ProcessInstanceStatus.Rejected.name()};
			if(filter!=null){
				filter.statusList = statusList;
			}
			else{
				// if filter is null (not specified by), create new filter
				ApprovalProcessRepository.AdvancedSearchFilter statusFilter =
						new ApprovalProcessRepository.AdvancedSearchFilter();
				statusFilter.statusList = statusList;
				filter = statusFilter;
			}
		}

		// if filter contains Pending Status, search ProcessInstance with ProcessInstanceWorkItem. ProcessInstanceWorkItem contains Pending process instance.
		if(isContainPendingStatus(filter)){
			requestIdListApprovedRejected = repo.getExpPendingRequestIdList(filter, objectName, userId);
		}
		// if filter contains Approval or Rejected Status, search ProcessInstance with ProcessInstanceWorkItem. ProcessInstanceStep contains kinda history information.
		if(isContainApprovedOrRejectedStatus(filter)){
			requestIdListPending = repo.getExpApprovedRejectedRequestIdList(filter, objectName, userId);
		}

		requestIdList.addAll(requestIdListPending);
		requestIdList.addAll(requestIdListApprovedRejected);
		return requestIdList;
	}

	/**
		 * @description Get delegated approver setting list having specified employee as a delegated approver
	 * @param empId Base Id of delegated employee
	 * @return List of delegated approver setting
	 */
	public Map<Id, DelegatedApproverSettingEntity> getAllOriginalApproverMap(Id empId) {
		DelegatedApproverSettingRepository repo = new DelegatedApproverSettingRepository();

		List<DelegatedApproverSettingEntity> settingList = repo.getAllOriginalApproverList(empId);

		Map<Id, DelegatedApproverSettingEntity> settingMap = new Map<Id, DelegatedApproverSettingEntity>();
		for (DelegatedApproverSettingEntity setting : settingList) {
			settingMap.put(setting.employeeBaseId, setting);
		}

		return settingMap;
	}

	/**
	 * @description 指定した社員が代理承認者に設定されている代理承認者設定を取得する（経費申請のみ）
	 * @param empId 代理承認者の社員ベースID
	 * @return 代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getExpReportOriginalApproverList(Id empId) {
		DelegatedApproverSettingRepository repo = new DelegatedApproverSettingRepository();

		List<DelegatedApproverSettingEntity> settingList = repo.getExpReportOriginalApproverList(empId);

		return settingList;
	}

	/**
	 * @description 指定した社員が代理承認者に設定されている代理承認者設定を取得する（事前申請承認のみ）
	 * @param empId 代理承認者の社員ベースID
	 * @return 代理承認者設定エンティティのリスト
	 */
	public List<DelegatedApproverSettingEntity> getExpRequestOriginalApproverList(Id empId) {
		DelegatedApproverSettingRepository repo = new DelegatedApproverSettingRepository();

		List<DelegatedApproverSettingEntity> settingList = repo.getExpRequestOriginalApproverList(empId);

		return settingList;
	}

	/**
	 * @description 対象の申請の承認履歴を取得する
	 * 対象の申請に旧申請がある場合は、旧申請の承認履歴も取得する（勤怠日次申請のみ）
	 * @param requestId 申請ID
	 * @return 対象の申請の承認履歴のリスト （新申請、旧申請の順で格納する）
	 */
	@testVisible
	private List<ApprovalProcessHistoryEntity> getApprovalProcessHistoryEntityList(Id requestId) {

		AttDailyRequestRepository requestRepo = new AttDailyRequestRepository();
		ApprovalProcessRepository approvalRepo = new ApprovalProcessRepository();

		// 承認履歴のリスト
		List<ApprovalProcessHistoryEntity> historyEntityList = new List<ApprovalProcessHistoryEntity>();

		// 勤怠日次申請以外の場合は、オリジナルIDを持っていないため、パラメータの申請IDで承認履歴を取得する
		if (!isType(requestId, AttDailyRequest__c.SObjectType)) {
			historyEntityList.addAll(approvalRepo.getHistoryList(requestId));
			return historyEntityList;
		}

		// オリジナルID（旧申請）がある場合は、旧申請の承認履歴も取得する
		AttDailyRequestEntity requestEntity = requestRepo.getEntity(requestId);
		historyEntityList.addAll(approvalRepo.getHistoryList(requestEntity.id));
		if (requestEntity.originalRequestId != null) {
			historyEntityList.addAll(approvalRepo.getHistoryList(requestEntity.originalRequestId));
		}
		return historyEntityList;
	}

	/**
	 * @description 承認履歴を取得する
	 * @param requestId 申請ID
	 * @return 承認履歴のリスト （承認日時の降順で返却する）
	 */
	public List<History> getHistoryList(final Id requestId) {
		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		// 対象申請の承認履歴を取得
		List<ApprovalProcessHistoryEntity> historyEntityList = getApprovalProcessHistoryEntityList(requestId);

		// 履歴各ステップのActorIdに紐づく社員情報を取得
		EmployeeRepository.SearchFilter empFilter = new EmployeeRepository.SearchFilter();
		empFilter.userIds = new Set<Id>();
		for (ApprovalProcessHistoryEntity entity : historyEntityList) {
			empFilter.userIds.add(entity.actorId);
		}
		List<EmployeeBaseEntity> employees = new EmployeeRepository().searchEntity(empFilter);
		Map<Id, EmployeeBaseEntity> empMapByUserId = new Map<Id, EmployeeBaseEntity>();
		for (EmployeeBaseEntity emp : employees) {
			empMapByUserId.put(emp.userId, emp);
		}

		// 申請レコードの情報を取得
		Request request = getRequest(requestId);

		List<History> historyList = new List<History>();
		for (ApprovalProcessHistoryEntity entity : historyEntityList) {
			// 実行者
			EmployeeBaseEntity actor = empMapByUserId.get(entity.actorId);
			History history = new History();
			history.id = entity.id;
			history.stepName = entity.processNodeName;
			history.approveTime = entity.createdDate.getDatetime().format('YYYY-MM-dd\' \'HH:mm');
			history.status = entity.stepStatus;
			// ステータスのラベル
			List<Schema.PicklistEntry> pickList = ProcessInstanceHistory.stepStatus.getDescribe().getPicklistValues();
			for (Schema.PicklistEntry entry : pickList) {
				if (entry.getValue() == entity.stepStatus) {
					history.statusLabel = entry.getLabel();
					break;
				}
			}
			history.actorName = actor.fullNameL.getFullName();
			history.actorPhotoUrl = actor.user.photoUrl;
			history.comment = entity.comments;
			history.isDelegated = isDelegated(request, entity);
			historyList.add(history);
		}
		return historyList;
	}

	/**
	 * @description 経費申請の承認履歴を取得する（経理承認の履歴も含む）
	 * @param requestId 申請ID
	 * @return 承認履歴のリスト （承認日時の降順で返却する）
	 */
	public List<History> getExpenseHistoryList(final Id requestId) {
		Map<Id, History> historyMap = new Map<Id, History>();
		List<SortHistory> sortHistoryList = new List<SortHistory>();

		// 通常の履歴を取得
		List<History> approvalHistoryList = getHistoryList(requestId);
		for (History history : approvalHistoryList) {
			historyMap.put(history.id, history);
		}

		for (ApprovalProcessHistoryEntity entity : getApprovalProcessHistoryEntityList(requestId)) {
			SortHistory sortHistory = new SortHistory();
			sortHistory.id = entity.id;
			sortHistory.approveTime = entity.createdDate;
			sortHistoryList.add(sortHistory);
		}

		// 経理承認の履歴を取得
		ExpModificationHistoryRepository.SearchFilter filter = new ExpModificationHistoryRepository.SearchFilter();
		filter.requestId = requestId;
		filter.fields = new List<String>{ExpFinanceApprovalService.HISTORY_TYPE_ACCOUNTING_AUTHORIZED,
				ExpFinanceApprovalService.HISTORY_TYPE_ACCOUNTING_REJECTED};
		List<ExpModificationHistoryEntity> historyEntityList = new ExpModificationHistoryRepository().searchEntity(filter);

		// 社員情報を取得
		List<Id> empBaseIds = new List<Id>();
		for (ExpModificationHistoryEntity entity : historyEntityList) {
			empBaseIds.add(entity.modifiedBy);
		}
		Map<Id, EmployeeBaseEntity> empBaseMap = new EmployeeService().getBaseMap(empBaseIds);

		// 経理承認の履歴データを作成
		for (ExpModificationHistoryEntity entity : historyEntityList) {
			// 実行者
			EmployeeBaseEntity actor = empBaseMap.get(entity.modifiedBy);
			History history = new History();
			history.id = entity.id;
			history.approveTime = entity.modifiedDateTime.getDatetime().format('YYYY-MM-dd\' \'HH:mm');
			history.status = entity.field;
			history.actorName = actor.fullNameL.getFullName();
			history.actorPhotoUrl = actor.user.photoUrl;
			history.comment = entity.comment;
			history.isDelegated = false;
			historyMap.put(history.id, history);

			SortHistory sortHistory = new SortHistory();
			sortHistory.id = entity.id;
			sortHistory.approveTime = entity.modifiedDateTime;
			sortHistoryList.add(sortHistory);
		}

		// 履歴をソート
		List<History> historyList = new List<History>();
		sortHistoryList.sort();
		for (SortHistory sortHistory : sortHistoryList) {
			History history = historyMap.get(sortHistory.id);
			historyList.add(history);
		}

		return historyList;
	}

	/**
	 * 代理申請 or 承認したか判定する
	 * @param request 申請情報
	 * @param hitsory 承認プロセス履歴
	 */
	@TestVisible
	private Boolean isDelegated(Request request, ApprovalProcessHistoryEntity history) {
		// 申請 or 申請取消
		if (history.stepStatus == Status.Started.name() || history.stepStatus == Status.Removed.name()) {
			// 実行者と所有者が異なる場合は代理とみなす
			return history.actorId != request.ownerId;
		}

		// 2018/8時点ではキューの承認に対応していないため、一律falseを返却する
		if (!isType(history.originalActorId, User.SObjectType)) {
			return false;
		}

		// 承認 or 却下
		// 実行者と割当先が異なる場合は代理とみなす
		return history.actorId != history.originalActorId;
	}

	/**
	 * @description 申請を承認する
	 * @param  requestIdList 申請IDのリスト
	 * @param  comennt 承認コメント
	 */
	public void approve(List<Id> requestIdList, String comment) {
		// 代理承認可能な申請と、その他の申請に振り分ける
		List<Id> sharingProcessIds = new List<Id>();
		List<Id> noSharingProcessIds = new List<Id>();
		for (Id requestId : requestIdList) {
			if (canDelegateProcess(requestId)) {
				noSharingProcessIds.add(requestId);
			} else {
				sharingProcessIds.add(requestId);
			}
		}

		// 共有ルールを適用した承認実行
		processRequest(ActionType.Approve.name(), sharingProcessIds, comment);

		// 共有ルールを適用しない承認実行（代理承認を含む）
		Map<Id, Id> workItemMap = getEditableRequestMap(noSharingProcessIds);
		new NoSharingApprovalProcess().processRequest(ActionType.Approve, noSharingProcessIds, workItemMap, comment);
	}

	/**
	 * @description 申請を却下する
	 * @param  requestIdList 申請IDのリスト
	 * @param  comennt 却下コメント
	 */
	public void reject(List<Id> requestIdList, String comment) {
		// 代理却下可能な申請と、その他の申請に振り分ける
		List<Id> sharingProcessIds = new List<Id>();
		List<Id> noSharingProcessIds = new List<Id>();
		for (Id requestId : requestIdList) {
			if (canDelegateProcess(requestId)) {
				noSharingProcessIds.add(requestId);
			} else {
				sharingProcessIds.add(requestId);
			}
		}

		// 共有ルールを適用した却下実行
		processRequest(ActionType.Reject.name(), sharingProcessIds, comment);

		// 共有ルールを適用しない却下実行（代理却下を含む）
		Map<Id, Id> workItemMap = getEditableRequestMap(noSharingProcessIds);
		new NoSharingApprovalProcess().processRequest(ActionType.Reject, noSharingProcessIds, workItemMap, comment);
	}

	/**
	 * 対象の申請を代理承認/代理却下出来るか判定する。
	 * このメソッドは、仕様上で代理承認/却下が可能か判定するのみで、
	 * 申請レコードに編集権限があるか判定しないため、呼出し元の後続処理で権限を判定すること。
	 *
	 * @param requestId 判定対象の申請ID（各種勤怠申請・勤務確定申請・工数確定申請・経費申請）
	 * @return 申請IDが代理承認／却下が可能な場合true
	 */
	private Boolean canDelegateProcess(Id requestId) {
		// 各種勤怠申請 or 勤務確定申請 or 経費申請 or 事前申請承認 の場合、代理可能とする
		return isType(requestId, AttDailyRequest__c.SObjectType) ||
				isType(requestId, AttRequest__c.SObjectType) ||
				isType(requestId, ExpReportRequest__c.SObjectType) ||
				isType(requestId, ExpRequestApproval__c.SObjectType);
	}

	/**
	 * 申請を承認/却下する
	 * @param type 処理種別
	 * @param requestIdList 申請IDのリスト
	 * @param comment コメント
	 */
	private void processRequest(String type, List<Id> requestIdList, String comment) {
		ApprovalProcessRepository repo = new ApprovalProcessRepository();

		// 承認/却下対象の承認申請データを取得
		Map<Id, Id> workItemIdMap = repo.getWorkItemMap(requestIdList);

		// 承認/却下実行
		List<Approval.ProcessWorkitemRequest> actionList = new List<Approval.ProcessWorkitemRequest>();
		for (Id requestId : requestIdList) {
			if (!workItemIdMap.containsKey(requestId)) {
				if (type.equals(ActionType.Approve.name())) {
					// メッセージ：該当の申請は承認できません。
					throw new App.IllegalStateException(
							App.ERR_CODE_CANNOT_APPROVE_REQUEST,
							ComMessage.msg().Appr_Err_CannotApproveRequest);
				} else if (type.equals(ActionType.Reject.name())) {
					// メッセージ：該当の申請は却下できません。
					throw new App.IllegalStateException(
							App.ERR_CODE_CANNOT_REJECT_REQUEST,
							ComMessage.msg().Appr_Err_CannotRejectRequest);
				}
			}

			Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
			request.setWorkItemId(workItemIdMap.get(requestId));
			request.setAction(type);
			request.setComments(comment);

			actionList.add(request);
		}
		if (!actionList.isEmpty()) {
			Approval.process(actionList);
		}
	}

	/**
	 * 申請レコードの情報を取得する
	 * @param requestId 申請レコードID
	 * @return 申請レコードの情報
	 * @throws App.ParameterException requestIdが申請オブジェクト以外の場合
	 */
	private Request getRequest(Id requestId) {
		// 各種勤怠申請
		if (isType(requestId, AttDailyRequest__c.SObjectType)) {
			AttDailyRequestEntity entity = new AttDailyRequestRepository().getEntity(requestId);
			return new Request(entity.ownerId);
		}
		// 勤務確定申請
		if (isType(requestId, AttRequest__c.SObjectType)) {
			AttRequestEntity entity = new AttRequestRepository().getEntity(requestId);
			return new Request(entity.ownerId);
		}
		// 工数確定申請
		if (isType(requestId, TimeRequest__c.SObjectType)) {
			TimeRequestEntity entity = new TimeRequestRepository().getTimeRequest(requestId);
			return new Request(entity.ownerId);
		}
		// 経費申請 ExpReport Request
		if (isType(requestId, ExpReportRequest__c.SObjectType)) {
			ExpReportRequestEntity entity = new ExpReportRequestRepository().getEntity(requestId);
			return new Request(entity.ownerId);
		}
		// 事前申請承認 ExpRequest Approval
		if (isType(requestId, ExpRequestApproval__c.SObjectType)) {
			ExpRequestApprovalEntity entity = new ExpRequestApprovalRepository().getEntity(requestId);
			return new Request(entity.ownerId);
		}
		// 対応外のIDを取得した場合は実行エラー
		throw new App.ParameterException('requestId', requestId);
	}

	/**
	 * IDが指定のオブジェクトのレコードか判定する
	 * @param id
	 * @return 指定のオブジェクトの場合true
	 */
	private Boolean isType(Id id, SObjectType objType) {
		return id.getSObjectType().getDescribe().getName() == objType.getDescribe().getName();
	}

	/**
	 * requestIdsのうち、申請を参照、編集出来るレコードを取得する
	 * @param requestIds 対象のWSPの申請レコードID
	 * @return key:申請レコードID、value:承認プロセスインスタンスのID
	 *         requestIdsのうち、参照・編集出来ないレコードはkeyに含まない
	 */
	private Map<Id, Id> getEditableRequestMap(List<Id> requestIds) {
		// 各種勤怠申請と勤務確定申請に振り分ける
		// TODO 申請系の共通エンティティがあれば、申請の種別を意識せずに編集権限のフラグで判定できる。
		//      また、当クラスのRequestも廃止できる。
		List<Id> attRequestIds = new List<Id>();
		List<Id> attDailyRequestIds = new List<Id>();
		List<Id> expReportRequestIds = new List<Id>();
		List<Id> expRequestApprovalIds = new List<Id>();
		for (Id requestId : requestIds) {
			// 勤務確定申請
			if (requestId.getSObjectType() == AttRequest__c.SObjectType) {
				attRequestIds.add(requestId);
				continue;
			}
			// 各種勤怠申請
			if (requestId.getSObjectType() == AttDailyRequest__c.SObjectType) {
				attDailyRequestIds.add(requestId);
				continue;
			}
			// 経費申請 ExpReportRequest
			if (requestId.getSObjectType() == ExpReportRequest__c.SObjectType) {
				expReportRequestIds.add(requestId);
				continue;
			}
			// 事前申請承認 ExpRequest Approval
			if (requestId.getSObjectType() == ExpRequestApproval__c.SObjectType) {
				expRequestApprovalIds.add(requestId);
				continue;
			}
			// 上記以外のIDを取得した場合は実行エラー
			throw new App.ParameterException('requestId', requestId);
		}

		// ログインユーザの社員情報を取得
		EmployeeBaseEntity loginEmployee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
		Boolean isSystemAdmin = new EmployeePermissionService(loginEmployee).hasModifyAllData;

		// ステータスが保留中の承認プロセスを取得
		Map<Id, ApprovalProcessWorkitemEntity> processWorkitemMap
				= new Map<Id, ApprovalProcessWorkitemEntity>(); // キーはAttRequest.id
		List<ApprovalProcessWorkitemEntity> processList
				= new ApprovalService().getProcessWorkItemList(requestIds);
		for (ApprovalProcessWorkitemEntity process : processList) {
			processWorkitemMap.put(process.targetObjectId, process);
		}

		// 参照・編集可能なレコードを取得
		List<Id> editableRecordIds = new List<Id>();
		if (!attRequestIds.isEmpty()) {
			List<AttRequestEntity> requests = new AttRequestRepository().getEntityList(attRequestIds);
			for (AttRequestEntity request : requests) {
				// レコードの編集権限がない場合はスキップ
				if (!request.hasEditAccess) {
					continue;
				}
				// システム管理者は全てのレコードを編集可能
				if (isSystemAdmin) {
					editableRecordIds.add(request.id);
					continue;
				}
				// 承認プロセスが保留中でない場合はスキップ
				ApprovalProcessWorkitemEntity workitem = processWorkitemMap.get(request.id);
				if (workitem == null) {
					continue;
				}

				// 編集権限があっても、自身が承認者以外の自身の申請は編集不可
				if (request.employeeBaseId == loginEmployee.id &&
						workitem.actorId != loginEmployee.userId) {
					continue;
				}
				editableRecordIds.add(request.id);
			}
		}
		if (!attDailyRequestIds.isEmpty()) {
			AttDailyRequestRepository dailyRequestRepo = new AttDailyRequestRepository();
			List<AttDailyRequestEntity> requests = dailyRequestRepo.getEntityListByIds(new Set<Id>(attDailyRequestIds));
			for (AttDailyRequestEntity request : requests) {
				// レコードの編集権限がない場合はスキップ
				if (!request.hasEditAccess) {
					continue;
				}
				// システム管理者は全てのレコードを編集可能
				if (isSystemAdmin) {
					editableRecordIds.add(request.id);
					continue;
				}
				// 承認プロセスが保留中でない場合はスキップ
				ApprovalProcessWorkitemEntity workitem = processWorkitemMap.get(request.id);
				if (workitem == null) {
					continue;
				}
				// 編集権限があっても、自身が承認者以外の自身の申請は編集不可
				if (request.employeeBaseId == loginEmployee.id &&
						workitem.actorId != loginEmployee.userId) {
					continue;
				}
				editableRecordIds.add(request.id);
			}
		}
		if (!expReportRequestIds.isEmpty()) {
			ExpReportRequestRepository expReportRepo = new ExpReportRequestRepository();
			List<ExpReportRequestEntity> requests = expReportRepo.getEntityList(expReportRequestIds);
			for (ExpReportRequestEntity request : requests) {
				// レコードの編集権限がない場合はスキップ
				if (!request.hasEditAccess) {
					continue;
				}
				// システム管理者は全てのレコードを編集可能
				if (isSystemAdmin) {
					editableRecordIds.add(request.id);
					continue;
				}
				// 承認プロセスが保留中でない場合はスキップ
				ApprovalProcessWorkitemEntity workitem = processWorkitemMap.get(request.id);
				if (workitem == null) {
					continue;
				}
				editableRecordIds.add(request.id);
			}
		}
		if (!expRequestApprovalIds.isEmpty()) {
			ExpRequestApprovalRepository expRequestRepo = new ExpRequestApprovalRepository();
			List<ExpRequestApprovalEntity> requests = expRequestRepo.getEntityList(expRequestApprovalIds);
			for (ExpRequestApprovalEntity request : requests) {
				// Skip if no edit permission
				if (!request.hasEditAccess) {
					continue;
				}
				// System Admin can edit all records
				if (isSystemAdmin) {
					editableRecordIds.add(request.id);
					continue;
				}
				// Skip if approval process is not pending approval
				ApprovalProcessWorkitemEntity workitem = processWorkitemMap.get(request.id);
				if (workitem == null) {
					continue;
				}
				editableRecordIds.add(request.id);
			}
		}

		// 承認プロセスIDとのマッピングを取得
		ApprovalProcessRepository repo = new ApprovalProcessRepository();
		return repo.getWorkItemMap(editableRecordIds);
	}

	/**
	 * 共有ルールを適用せずに、承認プロセスを操作するクラス。
	 * 承認プロセスで割当られた承認者以外も、承認・却下する場合に使用する。
	 * ユーザがWSPの申請レコードに対して編集権限をもつ場合のみ、承認・却下が可能。
	 *
	 * このクラスは他クラスからの呼び出しを制限するため、priveteとしている。
	 * クラス内には原則、承認却下以外の処理は実装しないこと。
	 */
	private without sharing class NoSharingApprovalProcess {

		/**
		 * 共有設定を適用せずに承認・却下を行う。
		 *
		 * @param action 承認 or　却下
		 * @param requestIds 処理対象の申請ID
		 * @param editableRequetMap 編集可能な申請のマップ key:申請ID value:承認プロセスの申請ID
		 * @param comment コメント
		 * @throws IllegalStateException 処理対象のIDがeditableRequetMapに含まれていない場合
		 */
		private void processRequest(ActionType action, List<Id> requestIds, Map<Id, Id> editableRequetMap, String comment) {

			// 承認/却下対象の承認申請データを取得
			List<Approval.ProcessWorkitemRequest> actionList = new List<Approval.ProcessWorkitemRequest>();
			for (Id requestId : requestIds) {
				if (!editableRequetMap.containsKey(requestId)) {
					if (action == ActionType.Approve) {
						// メッセージ：該当の申請は承認できません。
						throw new App.IllegalStateException(
								App.ERR_CODE_CANNOT_APPROVE_REQUEST,
								ComMessage.msg().Appr_Err_CannotApproveRequest);
					} else {
						// メッセージ：該当の申請は却下できません。
						throw new App.IllegalStateException(
								App.ERR_CODE_CANNOT_REJECT_REQUEST,
								ComMessage.msg().Appr_Err_CannotRejectRequest);
					}
				}

				Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
				request.setWorkItemId(editableRequetMap.get(requestId));
				request.setAction(action.name());
				request.setComments(comment);
				actionList.add(request);
			}

			Approval.process(actionList);
		}
	}
}