/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Repository class for Exchange Rate
 */
public with sharing class ExpExchangeRateRepository extends ValidPeriodRepository {

	/**
	 * Sort order for getting list
	 */
	public enum SortOrder {
		CODE_ASC, CODE_DESC
	}

	/** Max record number to fetch */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** Do not execute query with 'FOR UPDATE' */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * Target fields to fetch data
	 * If field is added, add same field to baseFieldSet
	 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		// Target fields definition
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				ExpExchangeRate__c.Id,
				ExpExchangeRate__c.Name,
				ExpExchangeRate__c.Code__c,
				ExpExchangeRate__c.UniqKey__c,
				ExpExchangeRate__c.CompanyId__c,
				ExpExchangeRate__c.CurrencyId__c,
				ExpExchangeRate__c.CurrencyPair__c,
				ExpExchangeRate__c.Rate__c,
				ExpExchangeRate__c.ReverseRate__c,
				ExpExchangeRate__c.ValidFrom__c,
				ExpExchangeRate__c.ValidTo__c};
	}

	/** Relationship name of Currency */
	private static final String CURRENCY_R = ExpExchangeRate__c.CurrencyId__c.getDescribe().getRelationshipName();
	/**
	 * Name fields of the Currency object
	 */
	private static final List<String> GET_CURRENCY_FIELD_NAME_LIST;
	static {

		// Definition of the target name fields
		final Set<Schema.SObjectField> currencyFieldList = new Set<Schema.SObjectField> {
			ComCurrency__c.IsoCurrencyCode__c,
			ComCurrency__c.Name_L0__c,
			ComCurrency__c.Name_L1__c,
			ComCurrency__c.Name_L2__c};

		GET_CURRENCY_FIELD_NAME_LIST = Repository.generateFieldNameList(CURRENCY_R, currencyFieldList);
	}

	/**
	 * Get entity by specified ID
	 * @param id ID of target entity
	 * @return Entity of specified ID. If no data is found, return null.
	 */
	public ExpExchangeRateEntity getEntity(Id id) {
		List<ExpExchangeRateEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * Get entity list by specified IDs
	 * @param ids ID list of target entity
	 * @return Entity list of specified ID.  If no data is found, return null.
	 */
	public List<ExpExchangeRateEntity> getEntityList(List<Id> ids) {
		ExpExchangeRateRepository.SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * Save a Exchange Rate record
	 * This method is used for both new creation and update.
	 * @param entity Entity to be saved
	 * @return Saved result
	 */
	public Repository.SaveResult saveEntity(ExpExchangeRateEntity entity) {
		return saveEntityList(new List<ExpExchangeRateEntity>{ entity });
	}

	/**
   * Save Exchange Rate records
   * This method is used for both new creation and update.
   * @param entity Entity list to be saved
   * @return Saved result
   */
	public Repository.SaveResult saveEntityList(List<ExpExchangeRateEntity> entityList) {

		// Convert entity to sObject
		List<ExpExchangeRate__c> sObjList = createObjectList(entityList);

		//Save to DB
		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);

		// Make and return result
		return resultFactory.createSaveResult(resultList);
	}

	/** Search Filters */
	public class SearchFilter {
		/** ID */
		public Set<Id> ids;
		/** Code (exact match) */
		public Set<String> codes;
		/** Company ID */
		public Set<Id> companyIds;
		/** Currency */
		public Set<Id> currencyIds;
		/** Target date */
		public AppDate targetDate;
		/** Target period */
		public List<AppDate> targetPeriod;
	}

	/**
	 * Search Exchange Rate
	 * @param filter Search Filter
	 * @return Search Result
	 */
	public List<ExpExchangeRateEntity> searchEntity(ExpExchangeRateRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * Search Exchange Rate
	 * @param filter Search Filter
	 * @order Sort order
	 * @return Search Result
	 */
	public List<ExpExchangeRateEntity> searchEntity(ExpExchangeRateRepository.SearchFilter filter, SortOrder order) {
		return searchEntity(filter, NOT_FOR_UPDATE, order);
	}

	/**
	 * Search Exchange Rate
	 * @param filter Search Filter
	 * @param forUpdate When call this method for update, specify as true.
	 * @order Sort order
	 * @return Search Result
	 */
	public List<ExpExchangeRateEntity> searchEntity(ExpExchangeRateRepository.SearchFilter filter, Boolean forUpdate, SortOrder order) {

		// WHERE statement
		List<String> whereList = new List<String>();
		// Filter by IDs
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// Filter by Company IDs
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ExpExchangeRate__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// Filter by Company IDs
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ExpExchangeRate__c.Code__c) + ' = :pCodes');
		}
		// Filter by Currencies
		Set<Id> pCurrencies;
		if (filter.currencyIds != null) {
			pCurrencies = filter.currencyIds;
			whereList.add(getFieldName(ExpExchangeRate__c.CurrencyId__c) + ' IN :pCurrencies');
		}
		// Get valid record
		// * ValidTo__c is a "expiry date". Do not contain. *
		Date pTargetDate;
		if (filter.targetDate != null) {
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ExpExchangeRate__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ExpExchangeRate__c.ValidTo__c) + ' > :pTargetDate');
		}

		// Check record existing in the period
		Date pTargetPeriodFrom;
		Date pTargetPeriodTo;
		if (filter.targetPeriod != null) {
			pTargetPeriodFrom = filter.targetPeriod.get(0).getDate();
			pTargetPeriodTo = filter.targetPeriod.get(1).getDate();
			whereList.add(getFieldName(ExpExchangeRate__c.ValidFrom__c) + ' < :pTargetPeriodTo');
			whereList.add(getFieldName(ExpExchangeRate__c.ValidTo__c) + ' > :pTargetPeriodFrom');
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY statement
		String orderByString = ' ORDER BY ';
		if (order == SortOrder.CODE_ASC) {
			orderByString += getFieldName(ExpExchangeRate__c.Code__c) + ' ASC NULLS LAST';
		} else if (order == SortOrder.CODE_DESC) {
			orderByString += getFieldName(ExpExchangeRate__c.Code__c) + ' DESC NULLS LAST';
		}

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		// Regular fields
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFieldList.add(getFieldName(fld));
		}
		// Add Currency fields
		selectFieldList.addAll(GET_CURRENCY_FIELD_NAME_LIST);

		// Build SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpExchangeRate__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderByString
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		//System.debug('ExpExchangeRateRepository: SOQL=' + soql);

		// Execute query
		List<ExpExchangeRate__c> sObjs = (List<ExpExchangeRate__c>)Database.query(soql);

		// Create entity from SObject
		List<ExpExchangeRateEntity> entities = new List<ExpExchangeRateEntity>();
		for (ExpExchangeRate__c sObj : sObjs) {
			entities.add(new ExpExchangeRateEntity(sObj));
		}

		return entities;
	}

	/**
	 * Create object from entity to save
	 * @param entityList Target entity list to save
	 * @return Object list to be saved
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntity> entityList) {

		List<ExpExchangeRate__c> objectList = new List<ExpExchangeRate__c>();
		for (ValidPeriodEntity entity : entityList) {
			objectList.add(((ExpExchangeRateEntity)entity).createSObject());
		}

		return objectList;
	}
}