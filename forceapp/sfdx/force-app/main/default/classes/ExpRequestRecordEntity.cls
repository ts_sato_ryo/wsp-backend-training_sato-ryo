/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Entity of Expense Request Record
 */
public with sharing class ExpRequestRecordEntity extends ExpRecord {

	/** Fields definition */
	public enum Field {
		EXP_REQUEST_ID,
		RECORD_ITEM_LIST
	}

	/** Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;

	/**
	 * Constructor
	 */
	public ExpRequestRecordEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** Request ID */
	public Id expRequestId {
		get;
		set {
			expRequestId = value;
			setChanged(Field.EXP_REQUEST_ID);
		}
	}

	/** List of Request record item */
	public List<ExpRequestRecordItemEntity> recordItemList {get; private set;}

	/**
	 * Replace record item list(duplicate list)
	 * @param sourceList
	 */
	public void replaceRecordItemList(List<ExpRequestRecordItemEntity> sourceList) {
		this.recordItemList = new List<ExpRequestRecordItemEntity>(sourceList);
		setChanged(Field.RECORD_ITEM_LIST);
	}

	/**
	 * Mark field as changed
	 * @param field Changed field
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
		// Reset the change status of record items as well
		if (recordItemList != null) {
			for (ExpRequestRecordItemEntity entity : recordItemList) {
				entity.resetChanged();
			}
		}
	}
}