/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠月次申請のリポジトリ
 */
public with sharing class AttRequestRepository extends Repository.BaseRepository {

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_LIMIT_MAX = 10000;

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_REQUEST_FIELD_SET;
	static {
		GET_REQUEST_FIELD_SET = new Set<Schema.SObjectField> {
				AttRequest__c.Id,
				AttRequest__c.Name,
				AttRequest__c.OwnerId,
				AttRequest__c.AttSummaryId__c,
				AttRequest__c.Comment__c,
				AttRequest__c.ProcessComment__c,
				AttRequest__c.CancelComment__c,
				AttRequest__c.Status__c,
				AttRequest__c.ConfirmationRequired__c,
				AttRequest__c.CancelType__c,
				AttRequest__c.EmployeeHistoryId__c,
				AttRequest__c.RequestTime__c,
				AttRequest__c.DepartmentHistoryId__c,
				AttRequest__c.ActorHistoryId__c,
				AttRequest__c.LastApproveTime__c,
				AttRequest__c.LastApproverId__c,
				AttRequest__c.Approver01Id__c
		};
	}

	/** 社員のリレーション名 */
	private static final String EMPLOYEE_HISTRORY_R = AttRequest__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();
	/** 申請者のリレーション名 */
	private static final String ACTOR_R = AttRequest__c.ActorHistoryId__c.getDescribe().getRelationshipName();
	/** 部署のリレーション名*/
	private static final String DEPARTMENT_R = AttRequest__c.DepartmentHistoryId__c.getDescribe().getRelationshipName();
	/** 勤怠サマリーのリレーション名*/
	private static final String SUMMARY_R = AttRequest__c.AttSummaryId__c.getDescribe().getRelationshipName();
	/** 社員履歴: ベースID項目のリレーション名 */
	private static final String EMPLOYEE_BASE_R = ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/** 社員ベース: ユーザのリレーション名 */
	private static final String USER_R = ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();

	/** UserRecordAccessの取得対象項目 */
	private static final List<String> GET_USER_RECORD_ACCESS_FIELD_NAME_LIST;
	/** UserRecordAccessの外部キー(リレーション名は取得できない) */
	private static final String USER_RECORD_ACCESS_R = 'UserRecordAccess';
	static {

		// 取得対象の項目定義(UserRecordAccess)
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			UserRecordAccess.HasEditAccess};
		GET_USER_RECORD_ACCESS_FIELD_NAME_LIST = Repository.generateFieldNameList(USER_RECORD_ACCESS_R, fieldList);
	}

	/** 検索フィルタ */
	private class SearchFilter {
		/** 申請ID */
		public List<Id> requestIds;
		/** 勤怠サマリーID */
		public List<Id> summaryIds;
		/** 申請ステータス */
		public List<AppRequestStatus> statusList;
	}

	/** FOR UPDATE でクエリしない */
	private static final Boolean NOT_FOR_UPDATE = false;

	/**
	 * 指定した申請IDの申請を取得する
	 * @param 取得対象の申請ID
	 * @return 申請、ただし見つからなかった場合はnull
	 */
	public AttRequestEntity getEntity(Id requestId) {
		List<AttRequestEntity> entities = getEntityList(new List<Id>{requestId});
		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定した申請IDの申請を取得する
	 * @param 取得対象の申請IDリスト
	 * @return 申請リスト
	 */
	public List<AttRequestEntity> getEntityList(List<Id> requestIds) {
		AttRequestRepository.SearchFilter filter = new SearchFilter();
		filter.requestIds = requestIds;

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定した勤怠サマリーの申請を取得する
	 * @param 取得対象の勤怠サマリーID
	 * @return 申請リスト
	 */
	public List<AttRequestEntity> getEntityListBySummaryId(Id summaryId) {
		AttRequestRepository.SearchFilter filter = new SearchFilter();
		filter.summaryIds = new List<Id>{summaryId};

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したステータスを持つ申請一覧を取得する
	 * @param 取得対象のステータス
	 * @return 申請リスト
	 */
	public List<AttRequestEntity> getEntityListByStatus(List<AppRequestStatus> statusList) {
		AttRequestRepository.SearchFilter filter = new SearchFilter();
		filter.statusList = statusList;

		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 申請を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した申請一覧(申請日付順)
	 */
	private List<AttRequestEntity> searchEntityList(
			AttRequestRepository.SearchFilter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_REQUEST_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		// 社員
		selectFldList.addAll(getSelectFldListEmployee(EMPLOYEE_HISTRORY_R));
		// 申請者
		selectFldList.addAll(getSelectFldListEmployee(ACTOR_R));
		// 部署
		selectFldList.addAll(getSelectFldListDepartment(DEPARTMENT_R));
		// 勤怠サマリー
		selectFldList.add(getFieldName(SUMMARY_R, AttSummary__c.SummaryName__c));
		// UserRecordAccess
		selectFldList.addAll(GET_USER_RECORD_ACCESS_FIELD_NAME_LIST);

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> requestIds = filter.requestIds;
		final List<Id> summaryIds = filter.summaryIds;
		final List<String> statusList;
		if (requestIds != null) {
			condList.add(getFieldName(AttRequest__c.Id) + ' IN :requestIds');
		}
		if (summaryIds != null) {
			condList.add(getFieldName(AttRequest__c.AttSummaryId__c) + ' IN :summaryIds');
		}
		if (filter.statusList != null) {
			statusList = new List<String>();
			for (AppRequestStatus status : filter.statusList) {
				statusList.add(status.value);
			}
			condList.add(getFieldName(AttRequest__c.Status__c) + ' IN :statusList');
		}

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + AttRequest__c.SObjectType.getDescribe().getName()
				+ ' WHERE ' + whereCond
				+ ' ORDER BY ' + getFieldName(AttRequest__c.RequestTime__c) + ' Asc'
				+ ' LIMIT :SEARCH_RECORDS_LIMIT_MAX';

		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// クエリ実行
		System.debug('AttRequestRepository.searchEntityList: SOQL ==>' + soql);
		List<AttRequest__c> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<AttRequestEntity> entityList = new List<AttRequestEntity>();
		for (AttRequest__c sObj : sObjList) {
			entityList.add(new AttRequestEntity(sObj));
		}

		return entityList;
	}

	/**
	 * 社員オブジェクトのSELECT句の取得項目を作成する
	 * @param 社員項目のリレーションシップ名
	 * @return 社員オブジェクトから取得する項目一覧
	 */
	private List<String> getSelectFldListEmployee(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.FirstName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.FirstName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.FirstName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.LastName_L0__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.LastName_L1__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, ComEmpBase__c.LastName_L2__c));
		selectFldList.add(getFieldName(relationshipName, EMPLOYEE_BASE_R, USER_R, User.SmallPhotoUrl));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.BaseId__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidFrom__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.ValidTo__c));
		selectFldList.add(getFieldName(relationshipName, ComEmpHistory__c.Removed__c));
		return selectFldList;
	}

	/**
	 * 部署オブジェクトのSELECT句の取得項目を作成する
	 * @param 社員項目のリレーションシップ名
	 * @return 社員オブジェクトから取得する項目一覧
	 */
	private List<String> getSelectFldListDepartment(String relationshipName) {
		List<String> selectFldList = new List<String>();
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L0__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L1__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Name_L2__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.ValidFrom__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.ValidTo__c));
		selectFldList.add(getFieldName(relationshipName, ComDeptHistory__c.Removed__c));
		return selectFldList;
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AttRequestEntity entity) {
		return saveEntityList(new List<AttRequestEntity>{entity});
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AttRequestEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public SaveResult saveEntityList(List<AttRequestEntity> entityList, Boolean allOrNone) {

		List<AttRequest__c> requestList = new List<AttRequest__c>();

		// エンティティからSObjectを作成する
		for (AttRequestEntity entity : entityList) {
			requestList.add(entity.createSObject());
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(requestList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}
}
