@RestResource(urlMapping='/restproxy-pc/*')
global with sharing class RestProxyPCService {
	@HttpGet
	global static void get() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		Map<String, String> params = req.params;
		String path = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
		String result = '';
		/* 共通 */
		if (path == 'getEmpInfo') {
			result = JSON.serialize(new BaseCtl.ConfigResultDto(ComLogic.getEmpByUserId(null)));
		} else if (path == 'getJobList') {
			// 指定されたジョブに紐づくジョブ一覧を取得
			String parentId = params.get('parentId');
			String targetDate = params.get('targetDate');
			BaseCtl.GetJobListReq paramDto = new BaseCtl.GetJobListReq();
			if (!String.isEmpty(parentId)) {
				paramDto.parentId = parentId;
			}
			if (!String.isEmpty(targetDate)) {
				paramDto.targetDate = targetDate;
			}
			result = JSON.serialize(BaseCtl.getJobList(paramDto));

		} else {
			res.statusCode = 404;
		}
		res.addHeader('Content-Type', 'application/json');
		RestContext.response.responseBody = Blob.valueof(result);
	}

	@HttpPost
	global static void post() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		Blob body = req.requestBody;
		System.debug(LoggingLevel.DEBUG, 'requestBody: ' + body.toString());

		String path = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
		String result = '';

		// RemoteApiController#invokeの代替処理(開発用)
		if (path == 'wsp-dev') {
			result = JSON.serialize(RemoteApiRoute.execute(req.requestBody.toString()));
		}
		else {
			res.statusCode = 404;
		}

		res.addHeader('Content-Type', 'application/json');
		RestContext.response.responseBody = Blob.valueof(result);
	}
}