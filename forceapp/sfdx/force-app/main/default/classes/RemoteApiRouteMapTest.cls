/**
 * RemoteApiRouteMapのテストクラス
 */
@isTest
private class RemoteApiRouteMapTest {

	/**
	 * RemoteApiRouteMappingに設定されたResourceクラスが
	 * すべてRemoteApi.ResourceBaseを継承したクラスになってことを検証する
	 */
	@isTest static void validateMapping() {

		for (Type resourceType:RemoteApiRouteMap.MAPPING.values()) {
			try {
				RemoteApi.ResourceBase res = (RemoteApi.ResourceBase)resourceType.newInstance();
			} catch (Exception e) {
				System.assert(false, resourceType.getName() + 'はRemoteApi.ResourceBaseクラスを継承していないため、RemoteApiRouteMapに設定できません。');
			}
		}

		System.assert(true);

	}

}