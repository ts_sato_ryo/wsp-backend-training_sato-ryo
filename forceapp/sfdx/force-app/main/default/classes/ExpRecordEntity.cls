/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * 経費明細のエンティティ
 */
public with sharing class ExpRecordEntity extends ExpRecord {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		EXP_REPORT_ID,
		RECORD_ITEM_LIST
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** 値を変更した項目のリスト Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;

	/**
	 * コンストラクタ Constructor
	 */
	public ExpRecordEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/** 経費精算ID Expense Report ID */
	public Id expReportId {
		get;
		set {
			expReportId = value;
			setChanged(Field.EXP_REPORT_ID);
		}
	}

	/** Last Modified Data (Reference Only) */
	public AppDatetime lastModifiedDate {get; set;}

	/*
	 * Remarks for Expense Record (Reference Only - currently only used to store data during journal export)
	 */
	public String remarks;

	/** 経費内訳リスト List of Record item */
	public List<ExpRecordItemEntity> recordItemList {get; private set;}


	/**
	 * 経費内訳リストをリプレースする（リストをコピーする） Replace record item list(duplicate list)
	 * @param recordItemList リプレース対象の経費内訳リスト List of recordItem to replace
	 */
	public void replaceRecordItemList(List<ExpRecordItemEntity> recordItemList) {
		this.recordItemList = new List<ExpRecordItemEntity>(recordItemList);
		setChanged(Field.RECORD_ITEM_LIST);
	}

	/**
	 * 項目の値を変更済みに設定する Mark field as changed
	 * @param field 変更した項目 Changed field
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する Check if the value of the field has been changed
	 * @param field 取得対象の項目 Target field
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする Reset the changed status
	 */
	public override void resetChanged() {
		super.resetChanged();
		this.isChangedFieldSet.clear();
		// 経費内訳リストの変更もリセットする
		if (recordItemList != null) {
			for (ExpRecordItemEntity entity : recordItemList) {
				entity.resetChanged();
			}
		}
	}

	/*
	 * Return the field value
	 * @param name Target field name
	 */
	public override Object getValue(String name) {
		Field field = ExpRecordEntity.FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

		return super.getValue(name);
	}

	/**
	 * Return the field value
	 * @param field Target field to get the value
	 */
	public Object getValue(ExpRecordEntity.Field field) {
		if (field == ExpRecordEntity.Field.EXP_REPORT_ID) {
			return expReportId;
		} else if (field == ExpRecordEntity.Field.RECORD_ITEM_LIST) {
			return recordItemList;
		}

		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}