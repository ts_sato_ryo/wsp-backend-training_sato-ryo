/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分マスタのリポジトリのテスト Test class for ExpTaxTypeRepository
 */
@isTest
private class ExpTaxTypeRepositoryTest {

	/** テストデータクラス Test data class */
	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') Org setting object */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト Country object */
		public ComCountry__c countryObj {get; private set;}
		/** 会社オブジェクト Company object */
		public ComCompany__c companyObj {get; private set;}

		/** コンストラクタ Constructor */
		public TestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Singapore');
			companyObj = ComTestDataUtility.createCompany('Test Company', ComTestDataUtility.createCountry('Japan').Id);
		}

		/**
		 * テスト用の税区分ベースエンティティを作成する Create tax type base entity(without saving to database)
		 */
		public ExpTaxTypeBaseEntity createTaxTypeBaseEntity(String name, String code) {

			ExpTaxTypeBaseEntity baseEntity = new ExpTaxTypeBaseEntity();
			baseEntity.name = name;
			baseEntity.code = code;
			baseEntity.uniqKey = baseEntity.createUniqKey(companyObj.Code__c, code);
			baseEntity.countryId = null;
			baseEntity.companyId = companyObj.id;
			baseEntity.addHistory(createTaxTypeHistoryEntity(name + '1', code, AppDate.newInstance(2017, 12, 15), AppDate.newInstance(2018, 4, 1)));
			baseEntity.addHistory(createTaxTypeHistoryEntity(name + '2', code, AppDate.newInstance(2018, 4, 1), AppDate.newInstance(2019, 1, 1)));

			return baseEntity;
		}

		/**
		 * テスト用の税区分履歴エンティティを作成する Create tax type history entity(without saving to database)
		 */
		public ExpTaxTypeHistoryEntity createTaxTypeHistoryEntity(String name, String code, AppDate validFrom, AppDate validTo) {
			ExpTaxTypeHistoryEntity historyEntity = new ExpTaxTypeHistoryEntity();
			historyEntity.name = name;
			historyEntity.nameL0 = name + '_L0';
			historyEntity.nameL1 = name + '_L1';
			historyEntity.nameL2 = name + '_L2';
			historyEntity.rate = 8.0;
			historyEntity.historyComment = '改訂コメント';
			historyEntity.validFrom = validFrom;
			historyEntity.validTo = validTo;
			historyEntity.isRemoved = true;
			historyEntity.uniqKey = historyEntity.createUniqKey(code);

			return historyEntity;
		}

		/**
		 * テスト用の税区分データを作成する Create tax type records
		 */
		public List<ExpTaxTypeBase__c> createTaxTypesWithHistory(String name, Integer baseSize, Integer historySize) {
			List<ExpTaxTypeBase__c> baseList =  ComTestDataUtility.createTaxTypesWithHistory(name, null, this.companyObj.Id, baseSize, historySize);

			return baseList;
		}
	}

	/**
	 * 指定したIDのベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest
	static void getEntityTest() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);

		Test.startTest();

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeBaseEntity resEntity = repo.getEntity(taxTypeBaseList[1].Id, null);

		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		assertBaseEntity(taxTypeBaseList[1], resEntity);
		Map<Id, ExpTaxTypeHistory__c> historyMap = getHistoryMapByBaseId(taxTypeBaseList[1].Id);
		System.assertEquals(historyMap.size(), resEntity.getHistoryList().size());
		for (ExpTaxTypeHistoryEntity historyEntity : resEntity.getHistoryList()) {
			assertHistoryEntity(historyMap.get(historyEntity.id), historyEntity);
		}
	}

	/**
	 * 存在しないベースIDを指定した場合、nullが返却されることを確認する
	 */
	@isTest
	static void getEntityTestNotExist() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);
		Id deleteId = taxTypeBaseList[1].Id;
		delete taxTypeBaseList[1];

		Test.startTest();

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeBaseEntity resEntity = repo.getEntity(deleteId, null);

		Test.stopTest();

		System.assertEquals(null, resEntity);
	}

	/**
	 * 指定したIDの指定した日付で有効なベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest
	static void getEntityTestTargetDate() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);
		final AppDate targetDate = AppDate.today();

		Test.startTest();

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeBaseEntity resEntity = repo.getEntity(taxTypeBaseList[1].Id, targetDate);

		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		assertBaseEntity(taxTypeBaseList[1], resEntity);
		Map<Id, ExpTaxTypeHistory__c> historyMap = getHistoryMapByBaseId(taxTypeBaseList[1].Id);
		System.assertEquals(1, resEntity.getHistoryList().size());
		System.assertEquals(targetDate, resEntity.getHistoryList().get(0).validFrom);
	}

	/**
	 * 指定した履歴IDのベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest
	static void getEntityByHistoryIdTest() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);
		ExpTaxTypeBase__c targetBaseObj = taxTypeBaseList[1];
		Map<Id, ExpTaxTypeHistory__c> historyMap = getHistoryMapByBaseId(targetBaseObj.Id);
		Id targetHistoryId = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c = :targetBaseObj.Id LIMIT 1].Id;

		Test.startTest();

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeBaseEntity resEntity = repo.getEntityByHistoryId(targetHistoryId);

		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetBaseObj.Id, resEntity.id);
		System.assertEquals(1, resEntity.getHistoryList().size());
		System.assertEquals(targetHistoryId, resEntity.getHistoryList().get(0).id);
	}

	/**
	 * 指定したIDのベースエンティティと履歴エンティティが取得できることを確認する
	 */
	@isTest static void searchEntityTest() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 3, 2);
		ExpTaxTypeBase__c targetTaxTypeBase = taxTypeBaseList[2];

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeRepository.SearchFilter filter;
		List<ExpTaxTypeBaseEntity> entityList;
		ExpTaxTypeRepository.HistorySortOrder sortOrder = ExpTaxTypeRepository.HistorySortOrder.VALID_FROM_DESC;

		// 対象日を指定して検索
		List<ExpTaxTypeHistory__c> historyObjs = [SELECT Id, ValidFrom__c, ValidTo__c FROM ExpTaxTypeHistory__c WHERE BaseId__c = :targetTaxTypeBase.Id LIMIT 1];
		ExpTaxTypeHistory__c tergetHistory = historyObjs[0];
		tergetHistory.ValidFrom__c = Date.newInstance(2016, 12, 19);
		tergetHistory.ValidTo__c = Date.newInstance(2016, 12, 20); // 失効日
		update tergetHistory;
		filter = new ExpTaxTypeRepository.SearchFilter();
		filter.targetDate = AppDate.newInstance(2016, 12, 19);
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		System.assertEquals(targetTaxTypeBase.Id, entityList[0].Id);
		System.assertEquals(1, entityList[0].getHistoryList().size());
		System.assertEquals(tergetHistory.Id, entityList[0].getHistoryList()[0].id);
	}

	/**
	 * 検索のテスト
	 * 履歴リストの並び順が正しく取得できることを確認する
	 */
	@isTest static void searchEntityTestSortOrder() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 3, 3);

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeRepository.SearchFilter filter;
		filter = new ExpTaxTypeRepository.SearchFilter();
		filter.baseIds = new List<Id>{taxTypeBaseList[0].Id};
		ExpTaxTypeRepository.HistorySortOrder sortOrder;
		List<ExpTaxTypeBaseEntity> entityList;
		List<ExpTaxTypeHistoryEntity> historyList;

		// 履歴リストが有効開始日の昇順
		sortOrder = ExpTaxTypeRepository.HistorySortOrder.VALID_FROM_ASC;
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		historyList = entityList[0].getHistoryList();
		System.assertEquals(3, historyList.size());
		for (Integer i = 1; i < historyList.size(); i++) {
			// 前のリストより有効開始日が後のはず
			System.assert(historyList[i - 1].validFrom.getDate() < historyList[i].validFrom.getDate(),
					'有効開始日が昇順になっていません。history[i-1].validFrom=' + historyList[i - 1].validFrom.format()
					+ ',history[i].validFrom=' + historyList[i].validFrom.format());
		}

		// 履歴リストが有効開始日の降順
		sortOrder = ExpTaxTypeRepository.HistorySortOrder.VALID_FROM_DESC;
		entityList = repo.searchEntityWithHistory(filter, false, sortOrder);
		System.assertEquals(1, entityList.size());
		historyList = entityList[0].getHistoryList();
		System.assertEquals(3, historyList.size());
		for (Integer i = 1; i < historyList.size(); i++) {
			// 前のリストより有効開始日が後のはず
			System.assert(historyList[i - 1].validFrom.getDate() > historyList[i].validFrom.getDate(),
					'有効開始日が降順になっていません。history[i-1].validFrom=' + historyList[i - 1].validFrom.format()
					+ ',history[i].validFrom=' + historyList[i].validFrom.format());
		}
	}

	/**
	 * ベースエンティティ取得のテスト
	 * 指定したベースIDのエンティティが取得できることを確認する
	 */
	@isTest
	static void getBaseEntityTest() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);
		Id targetId = taxTypeBaseList[1].Id;

		Test.startTest();

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeBaseEntity resEntity = repo.getBaseEntity(targetId);

		Test.stopTest();

		System.assertNotEquals(null, resEntity);
		System.assertEquals(targetId, resEntity.id);
	}

	/**
	 * ベースエンティティ取得のテスト
	 * 存在しないベースIDを指定した場合、nullが返却されることを確認する
	 */
	@isTest
	static void getBaseEntityTestNotExist() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);
		Id deleteId = taxTypeBaseList[1].Id;
		delete taxTypeBaseList[1];

		Test.startTest();

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeBaseEntity resEntity = repo.getBaseEntity(deleteId);

		Test.stopTest();

		System.assertEquals(null, resEntity);
	}

	/**
	 * 指定した履歴IDの履歴エンティティが取得できることを確認する
	 */
	@isTest static void getHistoryEntityTest() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);
		ExpTaxTypeBase__c targetBase = taxTypeBaseList[1];
		List<ExpTaxTypeHistory__c> historyObjs = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c = :targetBase.Id LIMIT 1];
		ExpTaxTypeHistory__c tergetHistory = historyObjs[0];

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeRepository.SearchFilter filter;
		ExpTaxTypeHistoryEntity entity;

		// 履歴エンティティが存在する場合
		entity = repo.getHistoryEntity(tergetHistory.Id);
		System.assertNotEquals(null, entity);
		System.assertEquals(tergetHistory.Id, entity.id);

		// 履歴エンティティが存在しない場合(ベースのIDで検索してみる)
		entity = repo.getHistoryEntity(targetBase.Id);
		System.assertEquals(null, entity);
	}

	/**
	 * 指定したベースIDの履歴エンティティが取得できることを確認する
	 */
	@isTest static void getHistoryEntityByBaseIdTest() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 2);
		ExpTaxTypeBase__c targetBase = taxTypeBaseList[1];
		List<ExpTaxTypeHistory__c> historyObjs = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c = :targetBase.Id];
		ExpTaxTypeHistory__c tergetHistory = historyObjs[0];

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeRepository.SearchFilter filter;
		List<ExpTaxTypeHistoryEntity> entityList;

		// 履歴エンティティが存在する場合
		entityList = repo.getHistoryEntityByBaseId(targetBase.Id);
		System.assertEquals(historyObjs.size(), entityList.size());
		for (ExpTaxTypeHistoryEntity entity : entityList) {
			System.assertEquals(targetBase.Id, entity.baseId);
		}

		// 論理削除されているレコードは対象外
		tergetHistory.Removed__c = true;
		update tergetHistory;
		entityList = repo.getHistoryEntityByBaseId(targetBase.Id);
		System.assertEquals(historyObjs.size() - 1, entityList.size());
	}

	/**
	 * 指定したベースIDと対象日の履歴エンティティが取得できることを確認する
	 */
	@isTest static void getHistoryEntityByBaseIdTestDate() {
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeBaseList = testData.createTaxTypesWithHistory('Test', 2, 3);
		ExpTaxTypeBase__c targetBase = taxTypeBaseList[1];
		List<ExpTaxTypeHistory__c> historyObjs = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c = :targetBase.Id];
		ExpTaxTypeHistory__c tergetHistory = historyObjs[0];

		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		ExpTaxTypeRepository.SearchFilter filter;
		List<ExpTaxTypeHistoryEntity> entityList;
		APpDate targetDate;

		// 対象日に履歴エンティティが存在する場合
		targetDate = AppDate.today();
		entityList = repo.getHistoryEntityByBaseId(targetBase.Id, targetDate);
		System.assertEquals(1, entityList.size());
		for (ExpTaxTypeHistoryEntity entity : entityList) {
			System.assertEquals(targetBase.Id, entity.baseId);
			System.assert(entity.validFrom.getDate() <= targetDate.getDate() && entity.validTo.getDate() > targetDate.getDate(),
					'対象日が有効期間に含まれていません');
		}

		// 対象日に履歴エンティティが存在しない
		// 空のリストが返却されるはず
		targetDate = AppDate.today().addMonths(-1);
		entityList = repo.getHistoryEntityByBaseId(targetBase.Id, targetDate);
		System.assertEquals(true, entityList.isEmpty());
	}

	/**
	 * 新規の税区分のベースと履歴エンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		// テストデータ作成
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		ExpTaxTypeBaseEntity taxTypeEntity = testData.createTaxTypeBaseEntity('Test税区分', '001');

		// テスト実行
		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		Test.startTest();

		Repository.SaveResultWithChildren result = repo.saveEntity(taxTypeEntity);

		Test.stopTest();

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		// ベース
		Id baseId = result.details[0].id;
		List<ExpTaxTypeBase__c> taxTypeBaseObjList = [
				SELECT Id, Name, Code__c, UniqKey__c, CurrentHistoryId__c, CountryId__c, CompanyId__c
				FROM ExpTaxTypeBase__c
				WHERE Id = :baseId];
		System.assertEquals(1, taxTypeBaseObjList.size());
		assertSavedBaseObj(taxTypeEntity, taxTypeBaseObjList[0]);
		// 履歴
		List<ExpTaxTypeHistory__c> taxTypeHistoryObjList = [
				SELECT Id, Name, BaseId__c, UniqKey__c, Name_L0__c, Name_L1__c, Name_L2__c,
					Rate__c, HistoryComment__c, ValidFrom__c, ValidTo__c, Removed__c
				FROM ExpTaxTypeHistory__c
				WHERE BaseId__c = :baseId
				ORDER BY ValidFrom__c];
		System.assertEquals(2, taxTypeHistoryObjList.size());
		assertSavedHistoryObj(taxTypeEntity.getHistoryList().get(0), taxTypeHistoryObjList[0], baseId);
	}

	/**
	 * 新規の税区分履歴エンティティが保存できることを確認する
	 */
	@isTest static void saveHistoryEntityTestNew() {
		// テストデータ作成
		ExpTaxTypeRepositoryTest.TestData testData = new TestData();
		List<ExpTaxTypeBase__c> taxTypeEntity = testData.createTaxTypesWithHistory('Test', 2, 2);
		ExpTaxTypeBase__c tagetBaseObj = taxTypeEntity[1];
		ExpTaxTypeHistoryEntity targetHistoryEntity =
				testData.createTaxTypeHistoryEntity(tagetBaseObj.Name, tagetBaseObj.Code__c, AppDate.newInstance(2019, 1, 1), AppDate.newInstance(2020, 12, 31));
		targetHistoryEntity.baseId = tagetBaseObj.Id;

		// テスト実行
		ExpTaxTypeRepository repo = new ExpTaxTypeRepository();
		Test.startTest();

		Repository.SaveResult result = repo.saveHistoryEntity(targetHistoryEntity);

		Test.stopTest();

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		// 履歴
		Id historyId = result.details[0].id;
		List<ExpTaxTypeHistory__c> taxTypeHistoryObjList = [
				SELECT Id, Name, BaseId__c, UniqKey__c, Name_L0__c, Name_L1__c, Name_L2__c,
					Rate__c, HistoryComment__c, ValidFrom__c, ValidTo__c, Removed__c
				FROM ExpTaxTypeHistory__c
				WHERE Id = :historyId
				ORDER BY ValidFrom__c];
		System.assertEquals(1, taxTypeHistoryObjList.size());
		assertSavedHistoryObj(targetHistoryEntity, taxTypeHistoryObjList[0], targetHistoryEntity.baseId);
	}

	/**
	 * 指定したベースIDの履歴オブジェクトを取得する
	 */
	private static Map<Id, ExpTaxTypeHistory__c> getHistoryMapByBaseId(Id baseId) {
		return new Map<Id, ExpTaxTypeHistory__c>([
				SELECT Id, Name, BaseId__c, UniqKey__c, HistoryComment__c, ValidFrom__c, ValidTo__c, Removed__c,
						Name_L0__c, Name_L1__c, Name_L2__c, Rate__c
				FROM ExpTaxTypeHistory__c
				WHERE BaseId__c = :baseId]);
	}

	/**
	 * ベースエンティティが正しく取得できているかを検証する
	 */
	private static void assertBaseEntity(ExpTaxTypeBase__c expObj, ExpTaxTypeBaseEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CurrentHistoryId__c, actEntity.currentHistoryId);
		System.assertEquals(expObj.CountryId__c, actEntity.countryId);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);
	}

	/**
	 * 履歴エンティティが正しく取得できているかを検証する
	 */
	private static void assertHistoryEntity(ExpTaxTypeHistory__c expObj, ExpTaxTypeHistoryEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.BaseId__c, actEntity.baseId);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.HistoryComment__c, actEntity.historyComment);
		System.assertEquals(expObj.ValidFrom__c, actEntity.validFrom.getDate());
		System.assertEquals(expObj.ValidTo__c, actEntity.validTo.getDate());
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);
		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
		System.assertEquals(expObj.Rate__c, actEntity.rate);
	}

	/**
	 * ExpTaxTypeBase__cに正しく保存できているかを検証する
	 */
	private static void assertSavedBaseObj(ExpTaxTypeBaseEntity expEntity, ExpTaxTypeBase__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.currentHistoryId, actObj.CurrentHistoryId__c);
		System.assertEquals(expEntity.countryId, actObj.CountryId__c);
		System.assertEquals(expEntity.companyId, actObj.CompanyId__c);
	}

	/**
	 * ExpTaxTypeHistory__cに正しく保存できているかを検証する
	 */
	private static void assertSavedHistoryObj(ExpTaxTypeHistoryEntity expEntity, ExpTaxTypeHistory__c actObj, Id baseId) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(baseId, actObj.BaseId__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.historyComment, actObj.HistoryComment__c);
		System.assertEquals(expEntity.validFrom.getDate(), actObj.ValidFrom__c);
		System.assertEquals(expEntity.validTo.getDate(), actObj.ValidTo__c);

		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
		System.assertEquals(expEntity.rate, actObj.Rate__c);
	}
}