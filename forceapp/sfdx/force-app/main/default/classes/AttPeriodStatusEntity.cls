/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 勤怠期間エンティティ
 */
public with sharing class AttPeriodStatusEntity extends Entity {

	/** コメントの最大文字数 */
	public static Integer COMMENT_MAX_LENGTH = 1000;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		OWNER_ID,
		EMPLOYEE_BASE_ID,
		VALID_FROM,
		VALID_TO,
		TYPE,
		SHORT_TIME_WORK_SETTING_BASE_ID,
		LEAVE_OF_ABSENCE_ID,
		COMMENT
	}

	/** 値を変更した項目のリスト */
	private Set<Field> changedFieldSet = new Set<AttPeriodStatusEntity.Field>();

	/**
	 * 社員情報
	 */
	public class Employee {
		public AppPersonName fullNameL;
		public Employee(AppPersonName fullNameL) {
			this.fullNameL = fullNameL;
		}
	}

	/**
	 * 休職休業情報
	 */
	public class LeaveOfAbsence {
		/** 休職休業名 */
		public AppMultiString nameL;

		/** コンストラクタ */
		public LeaveOfAbsence(AppMultiString nameL) {
			this.nameL = nameL;
		}
	}

	/** 期間名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}
	/** 所有者Id */
	public Id ownerId {
		get;
		set {
			ownerId = value;
			setChanged(Field.OWNER_ID);
		}
	}
	/** 社員ベースId */
	public Id employeeBaseId {
		get;
		set {
			employeeBaseId = value;
			setChanged(Field.EMPLOYEE_BASE_ID);
		}
	}
	/** 社員(参照専用) */
	public AttPeriodStatusEntity.Employee employee {
		get;
		set;
	}
	/** 有効開始日 */
	public AppDate validFrom {
		get;
		set {
			validFrom = value;
			setChanged(Field.VALID_FROM);
		}
	}
	/** 失効日 */
	public AppDate validTo {
		get;
		set {
			validTo = value;
			setChanged(Field.VALID_TO);
		}
	}
	/** 勤怠期間種別 */
	public AttPeriodStatusType type {
		get;
		set {
			type = value;
			setChanged(Field.TYPE);
		}
	}
	/** 短時間勤務設定ベースID */
	public Id shortTimeWorkSettingBaseId {
		get;
		set {
			shortTimeWorkSettingBaseId = value;
			setChanged(Field.SHORT_TIME_WORK_SETTING_BASE_ID);
		}
	}
	/** 休職休業ID */
	public Id leaveOfAbsenceId {
		get;
		set {
			leaveOfAbsenceId = value;
			setChanged(Field.LEAVE_OF_ABSENCE_ID);
		}
	}
	/** 休職休業情報(参照専用) */
	public LeaveOfAbsence leaveOfAbsence {
		get;
		set;
	}
	/** コメント */
	public String comment {
		get;
		set {
			comment = value;
			setChanged(Field.COMMENT);
		}
	}
	/** 最終更新日 */
	public AppDatetime lastModifiedDate {get; set;}
	/** 勤怠期間中かどうかをチェック */
	public Boolean isInPeriod(AppDate targetDate) {
		if (this.validFrom == null || this.validTo == null || targetDate == null) {
			return false;
		}
		Date dtValidFrom = this.validFrom.getDate();
		Date dtValidTo = this.validTo.getDate();
		Date dtTargetDate = targetDate.getDate();
		return dtValidFrom <= dtTargetDate && dtValidTo > dtTargetDate;
	}
	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.changedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.changedFieldSet.clear();
	}

	/**
	 * 勤怠期間名を作成する
	 */
	public String createName(String employeeCode, String employeeName) {

		// 有効期間が設定されていない場合は呼び出しタイミングが正しくない
		if (this.validFrom == null) {
			throw new App.IllegalStateException('[サーバエラー]勤怠期間の有効開始日が設定されていません');
		}
		if (this.validTo == null) {
			throw new App.IllegalStateException('[サーバエラー]勤怠期間の失効日が設定されていません');
		}

		return (employeeCode + employeeName).left(67)
				+ this.validFrom.formatYYYYMMDD() + '-' + this.validTo.formatYYYYMMDD();
	}

}
