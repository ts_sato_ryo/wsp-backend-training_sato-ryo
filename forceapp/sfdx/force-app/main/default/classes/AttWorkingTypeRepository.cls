/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 勤務体系リポジトリ
 */
public with sharing class AttWorkingTypeRepository extends ParentChildRepository {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * 履歴リストを取得する際の並び順
	 */
	public enum HistorySortOrder {
		VALID_FROM_ASC, VALID_FROM_DESC
	}

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無（デフォルトはoff） */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** 勤務体系ベースのキャッシュ */
	private static final Repository.Cache BASE_CACHE = new Repository.Cache();
	/** 勤務体系履歴のキャッシュ */
	private static final Repository.Cache HISTORY_CACHE = new Repository.Cache();

	/** 会社のリレーション名 */
	private static final String COMPANY_R = AttWorkingTypeBase__c.CompanyId__c.getDescribe().getRelationshipName();
	/** 部署ベースのリレーション名 */
	private static final String HISTORY_BASE_R = AttWorkingTypeHistory__c.BaseId__c.getDescribe().getRelationshipName();
	/**
	 * 取得対象のベースオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> baseFieldList = new Set<Schema.SObjectField> {
			AttWorkingTypeBase__c.Id,
			AttWorkingTypeBase__c.Name,
			AttWorkingTypeBase__c.CompanyId__c,
			AttWorkingTypeBase__c.Code__c,
			AttWorkingTypeBase__c.UniqKey__c,
			AttWorkingTypeBase__c.CurrentHistoryId__c,
			AttWorkingTypeBase__c.PayrollPeriod__c,
			AttWorkingTypeBase__c.StartMonthOfYear__c,
			AttWorkingTypeBase__c.StartDateOfMonth__c,
			AttWorkingTypeBase__c.StartDateOfWeek__c,
			AttWorkingTypeBase__c.YearMark__c,
			AttWorkingTypeBase__c.MonthMark__c,
			AttWorkingTypeBase__c.WorkSystem__c,
			AttWorkingTypeBase__c.WithoutCoreTime__c
		};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(baseFieldList);
	}

	/**
	 * 取得対象の履歴オブジェクトの項目名
	 * 項目が追加されたらhistoryFieldSetに追加してください
	 */
	private static final List<String> GET_HISTORY_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> historyFieldSet = new Set<Schema.SObjectField> {
			AttWorkingTypeHistory__c.Id,
			AttWorkingTypeHistory__c.Name,
			AttWorkingTypeHistory__c.BaseId__c,
			AttWorkingTypeHistory__c.UniqKey__c,
			AttWorkingTypeHistory__c.ValidFrom__c,
			AttWorkingTypeHistory__c.ValidTo__c,
			AttWorkingTypeHistory__c.Removed__c,
			AttWorkingTypeHistory__c.HistoryComment__c,
			AttWorkingTypeHistory__c.Name_L0__c,
			AttWorkingTypeHistory__c.Name_L1__c,
			AttWorkingTypeHistory__c.Name_L2__c,
			AttWorkingTypeHistory__c.WeeklyDayTypeSUN__c,
			AttWorkingTypeHistory__c.WeeklyDayTypeMON__c,
			AttWorkingTypeHistory__c.WeeklyDayTypeTUE__c,
			AttWorkingTypeHistory__c.WeeklyDayTypeWED__c,
			AttWorkingTypeHistory__c.WeeklyDayTypeTHU__c,
			AttWorkingTypeHistory__c.WeeklyDayTypeFRI__c,
			AttWorkingTypeHistory__c.WeeklyDayTypeSAT__c,
			AttWorkingTypeHistory__c.AutomaticLegalHolidayAssign__c,
			AttWorkingTypeHistory__c.BoundaryOfStartTime__c,
			AttWorkingTypeHistory__c.BoundaryOfEndTime__c,
			AttWorkingTypeHistory__c.DivideWorkHoursAtDayBoundary__c,
			AttWorkingTypeHistory__c.StartOfNightWork__c,
			AttWorkingTypeHistory__c.EndOfNightWork__c,
			AttWorkingTypeHistory__c.AllowanceOnEveryHoliday__c,
			AttWorkingTypeHistory__c.LegalHolidayAssignmentDays__c,
			AttWorkingTypeHistory__c.LegalHolidayAssignmentPeriod__c,
			AttWorkingTypeHistory__c.LegalWorkTimeADay__c,
			AttWorkingTypeHistory__c.LegalWorkTimeAWeek__c,
			AttWorkingTypeHistory__c.OffsetOvertimeAndDeductionTime__c,
			AttWorkingTypeHistory__c.AllowanceForOverContracted__c,
			AttWorkingTypeHistory__c.BasicSalaryForLegalHoliday__c,
			AttWorkingTypeHistory__c.IncludeHolidayWorkInPlainTime__c,
			AttWorkingTypeHistory__c.IncludeHolidayWorkInDeemedTime__c,
			AttWorkingTypeHistory__c.AllowWorkDuringHalfDayLeave__c,
			// 勤務時間系
			AttWorkingTypeHistory__c.StartTime__c,
			AttWorkingTypeHistory__c.EndTime__c,
			AttWorkingTypeHistory__c.FlexStartTime__c,
			AttWorkingTypeHistory__c.FlexEndTime__c,
			AttWorkingTypeHistory__c.ContractedWorkHours__c,
			AttWorkingTypeHistory__c.DeemedWorkHours__c,
			AttWorkingTypeHistory__c.Rest1StartTime__c,
			AttWorkingTypeHistory__c.Rest1EndTime__c,
			AttWorkingTypeHistory__c.Rest2StartTime__c,
			AttWorkingTypeHistory__c.Rest2EndTime__c,
			AttWorkingTypeHistory__c.Rest3StartTime__c,
			AttWorkingTypeHistory__c.Rest3EndTime__c,
			AttWorkingTypeHistory__c.Rest4StartTime__c,
			AttWorkingTypeHistory__c.Rest4EndTime__c,
			AttWorkingTypeHistory__c.Rest5StartTime__c,
			AttWorkingTypeHistory__c.Rest5EndTime__c,
			AttWorkingTypeHistory__c.AMContractedWorkHours__c,
			AttWorkingTypeHistory__c.AMStartTime__c,
			AttWorkingTypeHistory__c.AMEndTime__c,
			AttWorkingTypeHistory__c.AMRest1StartTime__c,
			AttWorkingTypeHistory__c.AMRest1EndTime__c,
			AttWorkingTypeHistory__c.AMRest2StartTime__c,
			AttWorkingTypeHistory__c.AMRest2EndTime__c,
			AttWorkingTypeHistory__c.AMRest3StartTime__c,
			AttWorkingTypeHistory__c.AMRest3EndTime__c,
			AttWorkingTypeHistory__c.AMRest4StartTime__c,
			AttWorkingTypeHistory__c.AMRest4EndTime__c,
			AttWorkingTypeHistory__c.AMRest5StartTime__c,
			AttWorkingTypeHistory__c.AMRest5EndTime__c,
			AttWorkingTypeHistory__c.UseAMHalfDayLeave__c,
			// 午後半休項目
			AttWorkingTypeHistory__c.PMContractedWorkHours__c,
			AttWorkingTypeHistory__c.PMStartTime__c,
			AttWorkingTypeHistory__c.PMEndTime__c,
			AttWorkingTypeHistory__c.PMRest1StartTime__c,
			AttWorkingTypeHistory__c.PMRest1EndTime__c,
			AttWorkingTypeHistory__c.PMRest2StartTime__c,
			AttWorkingTypeHistory__c.PMRest2EndTime__c,
			AttWorkingTypeHistory__c.PMRest3StartTime__c,
			AttWorkingTypeHistory__c.PMRest3EndTime__c,
			AttWorkingTypeHistory__c.PMRest4StartTime__c,
			AttWorkingTypeHistory__c.PMRest4EndTime__c,
			AttWorkingTypeHistory__c.PMRest5StartTime__c,
			AttWorkingTypeHistory__c.PMRest5EndTime__c,
			AttWorkingTypeHistory__c.UsePMHalfDayLeave__c,
			// 時間指定半休
			AttWorkingTypeHistory__c.HalfDayLeaveHours__c,
			AttWorkingTypeHistory__c.UseHalfDayLeave__c,
			// 申請系オプション
			AttWorkingTypeHistory__c.AddCompensationTimeToWorkHours__c,
			AttWorkingTypeHistory__c.PermitOvertimeWorkUntilContractedHours__c,
			AttWorkingTypeHistory__c.LeaveCodeList__c,
			AttWorkingTypeHistory__c.PatternCodeList__c,
			AttWorkingTypeHistory__c.AllowToChangeApproverSelf__c,
			// 利用機能系オプション
			AttWorkingTypeHistory__c.AllocatedSubstituteLeaveType__c,
			AttWorkingTypeHistory__c.UseLegalRestCheck1__c,
			AttWorkingTypeHistory__c.LegalRestCheck1WorkTimeThreshold__c,
			AttWorkingTypeHistory__c.LegalRestCheck1RequiredRestTime__c,
			AttWorkingTypeHistory__c.UseLegalRestCheck2__c,
			AttWorkingTypeHistory__c.LegalRestCheck2WorkTimeThreshold__c,
			AttWorkingTypeHistory__c.LegalRestCheck2RequiredRestTime__c,
			// 申請機能系オプション
			AttWorkingTypeHistory__c.UseEarlyStartWorkApply__c,
			AttWorkingTypeHistory__c.UseOvertimeWorkApply__c,
			AttWorkingTypeHistory__c.RequireEarlyStartWorkApply__c,
			AttWorkingTypeHistory__c.RequireOvertimeWorkApply__c,
			AttWorkingTypeHistory__c.RequireEarlyStartWorkApplyBefore__c,
			AttWorkingTypeHistory__c.RequireOvertimeWorkApplyAfter__c,
			AttWorkingTypeHistory__c.UseAbsenceApply__c,
			AttWorkingTypeHistory__c.UsePatternApply__c,
			// 直行直帰申請系
			AttWorkingTypeHistory__c.UseDirectApply__c,
			AttWorkingTypeHistory__c.DirectApplyStartTime__c,
			AttWorkingTypeHistory__c.DirectApplyEndTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest1StartTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest1EndTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest2StartTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest2EndTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest3StartTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest3EndTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest4StartTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest4EndTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest5StartTime__c,
			AttWorkingTypeHistory__c.DirectApplyRest5EndTime__c
		};

		GET_HISTORY_FIELD_NAME_LIST = Repository.generateFieldNameList(historyFieldSet);
	}

	/**
	 * コンストラクタ
	 */
	public AttWorkingTypeRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * Specify the use of cache and create instance.
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public AttWorkingTypeRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/**
	 * 指定したエンティティを取得する(ベース+全ての履歴)
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティと、ベースに紐づく全ての有効な履歴エンティティ
	 */
	public AttWorkingTypeBaseEntity getEntity(Id baseId) {
		return getEntity(baseId, null);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public AttWorkingTypeBaseEntity getEntity(Id baseId, AppDate targetDate) {

		List<AttWorkingTypeBaseEntity> entities = getEntityList(new List<Id>{baseId}, targetDate);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public List<AttWorkingTypeBaseEntity> getEntityList(List<Id> baseIds, AppDate targetDate) {
		AttWorkingTypeRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new Set<Id>(baseIds);
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param codes 取得対象の勤務体系コード
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したコードのベースエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public List<AttWorkingTypeBaseEntity> getEntityListByCode(List<String> codes, AppDate targetDate) {
		AttWorkingTypeRepository.SearchFilter filter = new SearchFilter();
		filter.codes = new Set<String>(codes);
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public AttWorkingTypeBaseEntity getBaseEntity(Id baseId) {
		return getBaseEntity(baseId, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public AttWorkingTypeBaseEntity getBaseEntity(Id baseId, Boolean forUpdate) {
		List<AttWorkingTypeBaseEntity> bases = getBaseEntityList(new List<Id>{baseId}, forUpdate);
		if (bases.isEmpty()) {
			return null;
		}
		return bases[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseIds 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティのリスト。履歴は含まない
	 */
	public List<AttWorkingTypeBaseEntity> getBaseEntityList(List<Id> baseIds, Boolean forUpdate) {
		AttWorkingTypeRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.baseIds = new Set<Id>(baseIds);
		return searchBaseEntity(filter, forUpdate);
	}

	/**
	 * 指定したコードの勤務体系基本情報を取得する(ベースのみ)
	 * @param codes 勤務体系コードのリスト
	 * @return AttWorkingTypeEntityのリスト
	 */
	public List<AttWorkingTypeBaseEntity> getBaseEntityListByCode(List<String> codes) {
		AttWorkingTypeRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.codes = new Set<String>(codes);

		return searchBaseEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyId 取得対象の履歴ID
	 * @return 履歴エンティティ。ただし、存在しない場合はnull
	 */
	public AttWorkingTypeHistoryEntity getHistoryEntity(Id historyId) {
		List<AttWorkingTypeHistoryEntity> entities = getHistoryEntityList(new List<Id>{historyId});

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyIds 取得対象の履歴ID
	 * @return 履歴エンティティのリスト。
	 */
	public List<AttWorkingTypeHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		AttWorkingTypeRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new Set<Id>(historyIds);

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する。有効開始日の昇順
	 */
	public List<AttWorkingTypeBaseEntity> searchEntity(SearchFilter filter) {
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** ベースID一覧 */
		public Set<Id> baseIds;
		/** 履歴ID一覧 */
		public Set<Id> historyIds;
		/** 会社ID */
		public Set<Id> companyIds;
		/** コード(完全一致) */
		public Set<String> codes;
		/** 有効期間の対象日 */
		public AppDate targetDate;
		/** 勤務体系名(部分一致検索) */
		public String nameL;
		/** 論理削除されている履歴を取得対象にする場合はtrue */
		public Boolean includeRemoved;
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストの並び順
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する
	 */
	public List<AttWorkingTypeBaseEntity> searchEntityWithHistory(SearchFilter filter, Boolean forUpdate,
			AttWorkingTypeRepository.HistorySortOrder sortOrder) {

		// 履歴を検索
		List<AttWorkingTypeHistoryEntity> histories = searchHistoryEntity(filter, forUpdate, sortOrder);

		// 取得対象のベースIDリストを作成
		Set<Id> targetBaseIds = new Set<Id>();
		for (AttWorkingTypeHistoryEntity history : histories) {
			targetBaseIds.add(history.baseId);
		}

		// ベースエンティティを取得する
		SearchBaseFilter baseFilter = new SearchBaseFilter();
		baseFilter.baseIds = targetBaseIds;
		List<AttWorkingTypeBaseEntity> bases = searchBaseEntity(baseFilter, forUpdate);

		// ベースに履歴を追加するためMapに変換する
		Map<Id, AttWorkingTypeBaseEntity> baseMap = new Map<Id, AttWorkingTypeBaseEntity>();
		for (AttWorkingTypeBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		// ベースエンティティに履歴を追加する
		for (AttWorkingTypeHistoryEntity history : histories) {
			AttWorkingTypeBaseEntity base = baseMap.get(history.baseId);
			base.addHistory(history);
		}

		return bases;
	}

	/** ベース用の検索フィルタ */
	public class SearchBaseFilter {
		/** 勤務体系ベースID */
		public Set<Id> baseIds;
		/** コード(完全一致) */
		public Set<String> codes;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	public List<AttWorkingTypeBaseEntity> searchBaseEntity(SearchBaseFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && BASE_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchBaseEntity ReturnCache key:' + cacheKey);
			List<AttWorkingTypeBaseEntity> entities = new List<AttWorkingTypeBaseEntity>();
			for (AttWorkingTypeBase__c sObj : (List<AttWorkingTypeBase__c>)BASE_CACHE.get(cacheKey)) {
				entities.add(createBaseEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchBaseEntity NoCache key:' + cacheKey);

		// ベース WHERE句
		List<String> baseWhereList = new List<String>();
		// ベースIDで検索
		Set<Id> pIds;
		if (filter.baseIds != null) {
			pIds = filter.baseIds;
			baseWhereList.add('Id IN :pIds');
		}
		// コードで検索(完全一致)
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			baseWhereList.add(getFieldName(AttWorkingTypeBase__c.Code__c) + ' IN :pCodes');
		}

		String whereString = buildWhereString(baseWhereList);

		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// ベース項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 関連項目（会社）
		selectFieldList.add(getFieldName(COMPANY_R, ComCompany__c.Code__c));

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + AttWorkingTypeBase__c.SObjectType.getDescribe().getName()
				+ whereString;
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(AttWorkingTypeBase__c.Code__c);
		}

		System.debug('AttWorkingTypeRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttWorkingTypeBase__c> sObjs = (List<AttWorkingTypeBase__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttWorkingTypeBase__c.SObjectType);

		// SObjectからエンティティを作成
		List<AttWorkingTypeBaseEntity> entities = new List<AttWorkingTypeBaseEntity>();
		for (AttWorkingTypeBase__c sObj : sObjs) {
			entities.add(createBaseEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			BASE_CACHE.put(cacheKey, sObjs);
		}
		return entities;
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 履歴エンティティの検索結果。有効開始日の日付の昇順
	 */
	public List<AttWorkingTypeHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate) {
		return searchHistoryEntity(filter, forUpdate, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストのソート順
	 * @return 履歴エンティティの検索結果
	 */
	public List<AttWorkingTypeHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, HistorySortOrder sortOrder) {
		// バインド変数が必要なため、クエリ作成処理をモジュール化していない

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter, sortOrder);
		if (isEnabledCache && !forUpdate && HISTORY_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity ReturnCache key:' + cacheKey);
			List<AttWorkingTypeHistoryEntity> entities = new List<AttWorkingTypeHistoryEntity>();
			for (AttWorkingTypeHistory__c sObj : (List<AttWorkingTypeHistory__c>)HISTORY_CACHE.get(cacheKey)) {
				entities.add(createHistoryEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity NoCache key:' + cacheKey);

		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 履歴項目
		selectFieldList.addAll(GET_HISTORY_FIELD_NAME_LIST);
		// 労働時間制
		selectFieldList.add(getFieldName(HISTORY_BASE_R, AttWorkingTypeBase__c.WorkSystem__c));

		// WHERE句
		List<String> whereList = new List<String>();
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(AttWorkingTypeHistory__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(AttWorkingTypeHistory__c.ValidTo__c) + ' > :pTargetDate');
		}
		// 履歴IDで検索
		Set<Id> pHistoryIds;
		if(filter.historyIds != null){
			pHistoryIds = filter.historyIds;
			whereList.add(getFieldName(AttWorkingTypeHistory__c.Id) + ' IN :pHistoryIds');
		}
		// 勤務体系名で検索
		String pNameL;
		if(String.isNotBlank(filter.nameL)){
			pNameL = '%' + filter.nameL + '%';
			String s = getFieldName(AttWorkingTypeHistory__c.Name_L0__c) + ' LIKE :pNameL OR '
					+ getFieldName(AttWorkingTypeHistory__c.Name_L1__c) + ' LIKE :pNameL OR '
					+ getFieldName(AttWorkingTypeHistory__c.Name_L2__c) + ' LIKE :pNameL';
			whereList.add(s);
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			whereList.add(getFieldName(AttWorkingTypeHistory__c.Removed__c) + ' = false');
		}
		// ベースIDで検索
		Set<Id> pBaseIds;
		if (filter.baseIds != null) {
			pBaseIds = filter.baseIds;
			whereList.add(getFieldName(HISTORY_BASE_R, AttWorkingTypeBase__c.Id) + ' IN :pBaseIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(HISTORY_BASE_R, AttWorkingTypeBase__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索(完全一致)
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(HISTORY_BASE_R, AttWorkingTypeBase__c.Code__c) + ' IN :pCodes');
		}

		// ORDER BY句
		String orderBy = ' ORDER BY ' + getFieldName(HISTORY_BASE_R, AttWorkingTypeBase__c.Code__c);
		if (sortOrder == HistorySortOrder.VALID_FROM_ASC) {
			orderBy += ', ' + getFieldName(AttWorkingTypeHistory__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == HistorySortOrder.VALID_FROM_DESC) {
			orderBy += ', ' + getFieldName(AttWorkingTypeHistory__c.ValidFrom__c) + ' DESC NULLS LAST';
		}

		// クエリ作成
		// コードごとに有効期間が新しい順
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM AttWorkingTypeHistory__c' +
				+ buildWhereString(whereList);
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += orderBy;
		}
		System.debug('AttWorkingTypeRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttWorkingTypeHistory__c> sObjs = (List<AttWorkingTypeHistory__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttWorkingTypeHistory__c.SObjectType);
		List<AttWorkingTypeHistoryEntity> entities = new List<AttWorkingTypeHistoryEntity>();
		for (AttWorkingTypeHistory__c sObj : sObjs) {
			entities.add(createHistoryEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			HISTORY_CACHE.put(cacheKey, sobjs);
		}
		return entities;
	}

	/**
	 * ベースオブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone) {
		List<AttWorkingTypeBase__c> deptList = new List<AttWorkingTypeBase__c>();
		for (SObject obj : baseObjList) {
			deptList.add((AttWorkingTypeBase__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * 履歴オブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone) {
		List<AttWorkingTypeHistory__c> deptList = new List<AttWorkingTypeHistory__c>();
		for (SObject obj : historyObjList) {
			deptList.add((AttWorkingTypeHistory__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AttWorkingTypeBaseEntity createBaseEntity(AttWorkingTypeBase__c baseObj) {
		return new AttWorkingTypeBaseEntity(baseObj);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private AttWorkingTypeHistoryEntity createHistoryEntity(AttWorkingTypeHistory__c historyObj) {
		return new AttWorkingTypeHistoryEntity(historyObj);
	}

	/**
	 * ベースエンティティからオブジェクトを作成する（各リポジトリで実装する）
	 */
	protected override SObject createBaseObj(ParentChildBaseEntity superBaseEntity) {
		return ((AttWorkingTypeBaseEntity)superBaseEntity).createSObject();
	}

	/**
	 * 履歴エンティティからオブジェクトを作成する
	 * @param historyEntity 作成元の履歴エンティティ
	 * @return 作成した履歴オブジェクト
	 */
	protected override SObject createHistoryObj(ParentChildHistoryEntity superHistoryEntity) {
		SObject ss = ((AttWorkingTypeHistoryEntity)superHistoryEntity).createSObject();
		return ((AttWorkingTypeHistoryEntity)superHistoryEntity).createSObject();
	}
}