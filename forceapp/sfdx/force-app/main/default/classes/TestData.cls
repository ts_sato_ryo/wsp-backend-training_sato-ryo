/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group テスト
  *
  * テストデータクラス
  */
@isTest
@TestVisible
private class TestData {

	/** 有効開始日の最小日付 */
	public static final Date VALID_FROM_MIN = Date.newInstance(1900, 1, 1);
	/** 有効開始日の最大日付 */
	public static final Date VALID_FROM_MAX = Date.newInstance(2100, 12, 31);
	/** 失効日の最小日付 */
	public static final Date VALID_TO_MIN = Date.newInstance(1900, 1, 1);
	/** 失効日の最大日付 */
	public static final Date VALID_TO_MAX = Date.newInstance(2101, 1, 1);

	/**
	 * 基本的なマスタデータを作成します。
	 * 本メソッドをテストクラスの@testSetupメソッドで呼び出してください
	 */
	public static void setupMaster() {
			ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			ComTestDataUtility.createCountry('Japan');
			// TODO 会社も対応したい
			// ComTestDataUtility.createTestCompany('Test Company', country.id, UserInfo.getLanguage());
	}

	/**
	 * エティティでテストデータを保持するクラス
	 * 本クラスを継承してテストデータクラスを定義してください。
	 */
	public virtual class TestDataEntity {

		/** 組織(L0='ja', L1='en_US', L2='zh_CN') */
		public OrganizationSettingEntity org;
		/** 国オブジェクト TODO エンティティ対応 */
		public CountryEntity country;
		/** 会社 */
		public CompanyEntity company {get; private set;}
		/** 通貨(currencyは予約語のため) */
		public CurrencyEntity currencyEntity {
			get {
				if (currencyEntity == null) {
					currencyEntity = createCurrency('JPY', 'Test');
				}
				return currencyEntity;
			}
			set;
		}
		/** 部署 */
		public DepartmentBaseEntity department {get; private set;}
		/** 社員(実行ユーザ) */
		public EmployeeBaseEntity employee {
			get {
				if (employee == null) {
					employee = createEmployee('Test社員', this.department.id, UserInfo.getUserId());
				}
				return employee;
			}
			set;}
		/** 勤務体系 */
		public AttWorkingTypeBaseEntity workingType {
			get;
			set;
		}
		/** 勤務パターン */
		public AttPatternEntity workPattern {
			get {
				if (workPattern == null) {
					workPattern = createWorkPattern('Test Work Pattern');
				}
				return workPattern;
			}
			set;
		}
		/** 休暇 */
		public List<AttLeaveEntity> leaveList {
			get {
				if (leaveList == null) {
					leaveList = createLeaveList();
				}
				return leaveList;
			}
			set;
		}
		/** 工数設定 */
		public TimeSettingBaseEntity timeSetting {
			get {
				if (timeSetting == null) {
					timeSetting = createTimeSetting('Test工数設定');
				}
				return timeSetting;
			}
			set;
		}
		/** カレンダー */
		public CalendarEntity calendar {
			get {
				if (calendar == null) {
					calendar = createCalendar('Testカレンダー');
				}
				return calendar;
			}
			set;
		}
		/** グレード */
		public ComGrade__c grade {
			get {
				if (grade == null) {
					grade = ComTestDataUtility.createGrade('Testグレード', 1, company.Id);
				}
				return grade;
			}
			set;

		}
		/** 36協定アラート設定 */
		public AttAgreementAlertSettingEntity agreementAlertSetting {
			get {
				if (agreementAlertSetting == null) {
					agreementAlertSetting = createAgreementAlertSetting('Testアラート設定');
				}
				return agreementAlertSetting;
			}
			set;
		}
		/** タグ */
		public TagEntity tag {
			get {
				if (tag == null) {
					tag = createTag('Test時短理由', TagType.SHORTEN_WORK_REASON);
				}
				return tag;
			}
			set;
		}
		/** 短時間勤務設定 */
		public AttShortTimeSettingBaseEntity shortTimeSetting {
			get {
				if (shortTimeSetting == null) {
					shortTimeSetting = createShortTimeSetting('Test短時間勤務設定', this.tag.id);
				}
				return shortTimeSetting;
			}
			set;
		}
		/** 休職休業 */
		public AttLeaveOfAbsenceEntity leaveOfAbsence {
			get {
				if (leaveOfAbsence == null) {
					leaveOfAbsence = createLeaveOfAbsence('Test休職休業');
				}
				return leaveOfAbsence;
			}
			set;
		}
		/** ジョブタイプ */
		public JobTypeEntity jobType {
			get {
				if (jobType == null) {
					jobType = createJobType('Testジョブタイプ');
				}
				return jobType;
			}
			set;
		}
		/** 拡張項目 */
		public ExtendedItemEntity extendedItem {
			get {
				if (extendedItem == null) {
					extendedItem = createExtendedItem('Test拡張項目');
				}
				return extendedItem;
			}
			set;
		}
		/** 権限 */
		public PermissionEntity permission {
			get {
				if (permission == null) {
					permission = createPermission('Test Permission');
				}
				return permission;
			}
			set;
		}

		/** Expense Employee Group */
		public ExpEmployeeGroupEntity expEmployeeGroup {
			get {
				if (expEmployeeGroup == null) {
					expEmployeeGroup = createExpEmployeeGroup('TestExpEmpGp');
				}
				return expEmployeeGroup;
			}
			set;
		}

		/**
		 * コンストラクタ
		 * 基本的なテスト用のマスタデータを作成する
		 */
		public TestDataEntity() {

			this.org = new OrganizationSettingRepository().getEntity();
			if (this.org == null) {
				this.org = createOrg('ja', 'en_US', null);
			}
			this.country = getCountry();
			if (this.country == null) {
				this.country = createCountry('Japan');
			}
			this.company = createCompany('Test Company', 'ja');
			this.department = createDepartment('TestData部署');
			this.workingType = createWorkingType('Test勤務体系');
		}

		/**
		 * DBに登録済みの国を1件取得する
		 * @return 国、未登録の場合はnull
		 */
		private CountryEntity getCountry() {
			List<CountryEntity> countries =
					new CountryRepository().searchEntityList(new CountryRepository.SearchFilter());
			return countries.isEmpty() ? null : countries[0];
		}

		/**
		 * DBに登録済みの国を1件取得する
		 * @return 国、未登録の場合はnull
		 */
		public virtual CountryEntity createCountry(String name) {
			ComCountry__c countryObj = ComTestDataUtility.createCountry(name);
			return new CountryRepository().searchById(countryObj.Id);
		}

		/**
		 * 組織設定を作成する
		 */
		private OrganizationSettingEntity createOrg(String l0, String l1, String l2) {
			ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting(l0, l1, l2);
			return (new OrganizationSettingRepository()).getEntity();
		}

		/**
		 * 会社を作成する
		 * @param defaultLang 会社のデフォルト言語
		 */
		public virtual CompanyEntity createCompany(String name) {
			ComCompany__c companyObj = ComTestDataUtility.createTestCompany(name, this.country.id, UserInfo.getLanguage());
			return (new CompanyRepository()).getEntity(companyObj.Id);
		}

		/**
		 * 会社を作成する
		 * @param defaultLang 会社のデフォルト言語
		 */
		protected virtual CompanyEntity createCompany(String name, String defaultLang) {
			ComCompany__c companyObj = ComTestDataUtility.createTestCompany(name, this.country.id, defaultLang);
			return (new CompanyRepository()).getEntity(companyObj.Id);
		}

		/**
		 * 部署のテストデータを作成する
		 */
		public virtual DepartmentBaseEntity createDepartment(String name) {
			ComDeptBase__c deptObj = ComTestDataUtility.createDepartmentWithHistory(
					name, this.company.Id, VALID_FROM_MIN, VALID_TO_MAX);
			return (new DepartmentRepository()).getEntity(deptObj.Id);
		}

		/**
		 * 社員のテストデータを作成する
		 */
		public virtual EmployeeBaseEntity createEmployee(String lastName, Id departmentId, Id userId) {
			ComEmpBase__c empObj = ComTestDataUtility.createEmployeeWithHistory(
					lastName, this.company.Id, departmentId, userId, this.permission.id);
			return (new EmployeeRepository()).getEntity(empObj.Id);
		}

		/**
		 * 標準ユーザの社員のテストデータを作成する
		 * @param lastName 社員の姓
		 */
		public virtual EmployeeBaseEntity createEmployeeWithStandardUser(String lastName) {
			User standardUser = ComTestDataUtility.createStandardUser(lastName);
			return createEmployee(lastName, null, standardUser.Id);
		}

		/**
		 * カレンダーを作成する
		 */
		public virtual CalendarEntity createCalendar(String name) {
			ComCalendar__c calendarObj = ComTestDataUtility.createCalendar(name, this.company.id);
			return (new CalendarRepository()).getEntityById(calendarObj.Id);
		}

		/**
		 * 36協定アラート設定を作成する
		 * @param defaultLang 会社のデフォルト言語
		 * @return アラート設定
		 */
		public virtual AttAgreementAlertSettingEntity createAgreementAlertSetting(String name) {
			AttAgreementAlertSetting__c settingObj =
					ComTestDataUtility.createAgreementAlertSettings(name, this.company.id, 1).get(0);
			return (new AttAgreementAlertSettingRepository()).getEntity(settingObj.Id);
		}

		/**
		 * タグを作成する
		 * @param name タグ名
		 * @param type タグの種別
		 * @return タグ
		 */
		public virtual TagEntity createTag(String name, TagType type) {
			final Integer order = null;
			final Boolean isRemoved = false;
			ComTag__c tagTypeObj =
					ComTestDataUtility.createTag(name, type, this.company.id, order, isRemoved);
			return (new TagRepository()).getEntity(tagTypeObj.Id);
		}
		/**
		 * 勤務体系のテストデータを作成する
		 */
		public virtual AttWorkingTypeBaseEntity createWorkingType(String name) {
			List<String> leaveCodeList = new List<String>();
			for (AttLeaveEntity leave : this.leaveList) {
				if (! String.isBlank(leave.code)) {
					leaveCodeList.add(leave.code);
				}
			}

			AttWorkingTypeBaseEntity entity = TestData.createWorkingTypeBaseEntity(name, this.company.id, this.company.code);
			entity.addHistory(TestData.createWorkingTypeHistoryEntity(entity.name, null, leaveCodeList));

			AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
			Id baseId = repo.saveEntity(entity).details[0].Id;
			entity = repo.getEntity(baseId);

			return entity;
		}

		/**
		 * 勤務体系のテストデータを作成する
		 */
		public virtual List<AttWorkingTypeBaseEntity> createWorkingTypeList(
				String name, List<AttLeaveEntity> leaves, Integer baseSize, Integer historySize) {
			return createWorkingTypeList(name, leaves, new List<AttPatternEntity>(), baseSize, historySize);
		}

		/**
		 * 勤務体系のテストデータを作成する
		 * @param name 勤務体系名
		 * @param leaves 休暇
		 * @param patterns 勤務パターン
		 * @param baseSize 登録件数（ベース）
		 * @param historySize 登録件数（履歴）
		 */
		public virtual List<AttWorkingTypeBaseEntity> createWorkingTypeList(
				String name, List<AttLeaveEntity> leaves, List<AttPatternEntity> patterns, Integer baseSize, Integer historySize) {

			List<String> leaveCodeList = new List<String>();
			for (AttLeaveEntity leave : leaves) {
				if (! String.isBlank(leave.code)) {
					leaveCodeList.add(leave.code);
				}
			}
			List<String> patternCodeList = new List<String>();
			for (AttPatternEntity pattern : patterns) {
				if (! String.isBlank(pattern.code)) {
					patternCodeList.add(pattern.code);
				}
			}

			final AppDate startDate = AppDate.today();
			List<AttWorkingTypeBaseEntity> baseList = new List<AttWorkingTypeBaseEntity>();
			for (Integer i = 0; i < baseSize; i++) {
				AttWorkingTypeBaseEntity base = TestData.createWorkingTypeBaseEntity(name + i, this.company.id, this.company.code);
				AppDate validFrom = startDate;
				for (Integer j = 0; j < historySize; j++) {
					AttWorkingTypeHistoryEntity history =
							TestData.createWorkingTypeHistoryEntity(base.name, null, leaveCodeList, patternCodeList);
					history.validFrom = validFrom;
					history.validTo = validFrom.addMonths(12);
					history.uniqKey = base.code + history.validFrom.formatYYYYMMDD() + '_' + history.validTo.formatYYYYMMDD();
					base.addHistory(history);

					validFrom = history.validTo;
				}

				baseList.add(base);
			}

			AttWorkingTypeRepository repo = new AttWorkingTypeRepository();
			List<Id> baseIds = new List<Id>();
			for (Repository.ResultDetail detail : repo.saveEntityList(baseList).details) {
				baseIds.add(detail.id);
			}
		 	return repo.getEntityList(baseIds, null);
		}

		/**
		 * 休暇マスタのテストデータを作成する
		 */
		public virtual List<AttLeaveEntity> createLeaveList() {

			List<AttLeaveEntity> leaves = new List<AttLeaveEntity>();
			leaves.add(
					createLeave('001', '有休', AttLeaveType.PAID, new List<AttLeaveRange>{
						AttLeaveRange.RANGE_DAY,
						AttLeaveRange.RANGE_AM,
						AttLeaveRange.RANGE_PM,
						AttLeaveRange.RANGE_HALF,
						AttLeaveRange.RANGE_TIME}));
			leaves.add(
					createLeave('002', '無休', AttLeaveType.UNPAID, new List<AttLeaveRange>{
						AttLeaveRange.RANGE_DAY}));
			return leaves;
		}

		/**
		 * テスト用の休暇マスタを作成する
		 * @param code コード
		 * @param name 休暇名
		 * @param leaveType 休暇タイプ
		 * @param leaveRange 休暇範囲
		 * @return 作成した休暇マスタ
		 */
		public virtual AttLeaveEntity createLeave(String code, String name, AttLeaveType leaveType, List<AttLeaveRange> leaveRanges) {
			AttLeaveEntity leave = new AttLeaveEntity();
			leave.name = name;
			leave.companyId = this.company.id;
			leave.code = code;
			leave.uniqKey = this.company.code + '-' + leave.code;
			leave.nameL0 = name + '_L0';
			leave.nameL1 = name + '_L1';
			leave.nameL2 = name + '_L2';
			leave.leaveType = leaveType;
			leave.leaveRanges = leaveRanges;

			AttLeaveRepository repo = new AttLeaveRepository();
			leave.setId(repo.saveEntity(leave).details[0].Id);

			return repo.getLeaveById(leave.id);
		}

		/**
		 * テスト用の通貨を作成する
		 * @param isoCode 通貨コード
		 * @param name 通貨名
		 * @return 通貨
		 */
		public virtual CurrencyEntity createCurrency(String isoCode, String name) {
			ComCurrency__c obj = ComTestDataUtility.createCurrency(isoCode, name);
			return (new CurrencyRepository()).getEntity(isoCode);
		}

		/**
		 * テスト用の工数設定を作成する
		 */
		public virtual TimeSettingBaseEntity createTimeSetting(String name) {
			TimeSettingBase__c timeSetting = ComTestDataUtility.createTimeSettingWithHistory(
					name, this.company.id, VALID_FROM_MIN, VALID_TO_MAX);

			return (new TimeSettingRepository()).getEntity(timeSetting.Id);
		}

		/**
		 * @desctiprion テスト用の勤務パターンを作成する
		 */
		public virtual AttPatternEntity createWorkPattern(String name) {
			AttPattern__c pattern = ComTestDataUtility.createAttPattern(name, this.company.id);
			pattern.ValidFrom__c = VALID_FROM_MIN;
			pattern.ValidTo__c = VALID_TO_MAX;
			update pattern;

			return (new AttPatternRepository2()).getPatternById(pattern.Id);
		}

		/**
		 * 短時間勤務設定を作成する
		 * @param name 短時間勤務設定名
		 * @param reasonId 理由ID(タグオブジェクトのID)
		 */
		public AttShortTimeSettingBaseEntity createShortTimeSetting(String name, Id reasonId) {
			AttShortTimeWorkSettingBase__c obj = ComTestDataUtility.createAttShortTimeSettingWithHistory(
					name, this.company.id, reasonId);

			// 有効期間を無期限に設定する
			AttShortTimeSettingRepository repo = new AttShortTimeSettingRepository();
			AttShortTimeSettingBaseEntity base = repo.getEntity(obj.Id);
			AttShortTimeSettingHistoryEntity history = base.getHistory(0);
			history.validFrom = AppDate.valueOf(VALID_FROM_MIN);
			history.validTo = AppDate.valueOf(VALID_TO_MAX);
			repo.saveHistoryEntity(history);

			return base;
		}

		/**
		 * 短時間勤務設定を作成する
		 * @param name 短時間勤務設定名
		 * @param reasonId 理由ID(タグオブジェクトのID)
		 */
		public AttLeaveOfAbsenceEntity createLeaveOfAbsence(String name) {
			ComCompany__c companyObj = new ComCompany__c(Id = this.company.id);
			AttLeaveOfAbsence__c obj = ComTestDataUtility.createLeaveOfAbsence(
					companyObj, name, false);

			return new AttLeaveOfAbsenceRepository().searchById(obj.Id);
		}

		/**
		 * 作業分類を作成する
		 * @param name 作業分類名
		 * @param size 作成する作業分類数
		 * @return 作業分類リスト
		 */
		public List<WorkCategoryEntity> createWorkCategoryList(String name, Integer size) {
			List<TimeWorkCategory__c> workCategoryObjs =
					ComTestDataUtility.createWorkCategories(name, this.company.id, size);

			// 実質期限ナシに設定する
			Set<Id> ids = new Set<Id>();
			for (TimeWorkCategory__c workCategoryObj : workCategoryObjs) {
					workCategoryObj.ValidFrom__c = VALID_FROM_MIN;
					workCategoryObj.ValidTo__c = VALID_TO_MAX;
					ids.add(workCategoryObj.Id);
			}
			update workCategoryObjs;

			WorkCategoryRepository.SearchFilter filter = new WorkCategoryRepository.SearchFilter();
			filter.ids = ids;
			return new WorkCategoryRepository().searchEntity(filter);
		}

		/**
		 * ジョブタイプを作成する
		 * @param name ジョブタイプ名
		 * @return ジョブタイプエンティティ
		 */
		public JobTypeEntity createJobType(String name) {
			ComJobType__c jobTypeObj = ComTestDataUtility.createJobType(name, this.company.id);
			return new JobTypeRepository().getEntity(jobTypeObj.Id);
		}

		/**
		 * 拡張項目を作成する
		 * @param name 拡張項目名
		 * @return 拡張項目エンティティ
		 */
		public ExtendedItemEntity createExtendedItem(String name) {
			ComExtendedItem__c extendedItemObj = ComTestDataUtility.createExtendedItem(name, this.company.id);
			return new ExtendedItemRepository().getEntity(extendedItemObj.Id);
		}

		/**
		 * 権限を作成する
		 * @param name 権限名
		 * @return 権限エンティティ
		 */
		public PermissionEntity createPermission(String name) {
			ComPermission__c permissionObj = ComTestDataUtility.createPermission(name, this.company.id);
			return new PermissionEntity(permissionObj);
		}

		/**
		 * Create Expense Employee Group
		 * @param name Expense Employee Group Name
		 * @return Expense Employee Group Entity
		 */
		public ExpEmployeeGroupEntity createExpEmployeeGroup(String name) {
			ExpEmployeeGroup__c expEmployeeGroupSObj = ComTestDataUtility.createExpEmployeeGroup(name, this.company.id);
			return new ExpEmployeeGroupRepository().createEntity(expEmployeeGroupSObj);
		}

		/**
		 * 社員からSystem.runAs(User)用のユーザを作成する
		 * @param employee 取得対象の社員
		 * @return System.runAs(User)用のユーザ
		 */
		public virtual User createRunAsUserFromEmployee(EmployeeBaseEntity employee) {
			return new User(Id = employee.userId);
		}
	}

	/**
	 * 勤務体系ベースエンティティを作成する(DB保存なし)
	 * @param name 勤務体系名
	 * @param companyId 会社ID
	 * @return 勤務体系エンティイティ(ベース)
	 */
	public static AttWorkingTypeBaseEntity createWorkingTypeBaseEntity(String name, Id companyId, String companyCode) {

		AttWorkingTypeBaseEntity entity = new AttWorkingTypeBaseEntity();
		entity.name = name;
		entity.companyId = companyId;
		entity.code = name + '_' + String.valueOf(Integer.valueOf(Math.random() * 10000));
		entity.uniqKey = companyCode + '-' + entity.code;
		entity.payrollPeriod = AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month;
		entity.startMonthOfYear = 1;
		entity.startDateOfMonth = 1;
		entity.startDateOfWeek = 0;
		entity.yearMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		entity.workSystem = AttWorkSystem.JP_Fix;
		entity.withoutCoreTime = false;

		return entity;
	}

	/**
	 * 勤務体系履歴エンティティを作成する(DB保存なし)
	 * @param name 勤務体系名
	 * @param baseId ベースID
	 * @param leaveCodeList 休暇コードリスト
	 * @return 勤務体系エンティイティ(履歴)
	 */
	public static AttWorkingTypeHistoryEntity createWorkingTypeHistoryEntity(String name, Id baseId, List<String> leaveCodeList) {
		return createWorkingTypeHistoryEntity(name, baseId, leaveCodeList, new List<String>());
	}

	/**
	 * 勤務体系履歴エンティティを作成する(DB保存なし)
	 * @param name 勤務体系名
	 * @param baseId ベースID
	 * @param leaveCodeList 休暇コードリスト
	 * @param patternCodeList 勤務パターンコードリスト
	 * @return 勤務体系エンティイティ(履歴)
	 */
	public static AttWorkingTypeHistoryEntity createWorkingTypeHistoryEntity(String name, Id baseId, List<String> leaveCodeList, List<String> patternCodeList) {
		AttWorkingTypeHistoryEntity entity = new AttWorkingTypeHistoryEntity();

		if (baseId != null) {
			entity.baseId = baseId;
		}

		entity.name = name;
		entity.validFrom = AppDate.newInstance(1900, 1, 1);
		entity.validTo = AppDate.newInstance(2100, 12, 31);
		entity.historyComment = name + '履歴コメント';
		entity.uniqKey = name + entity.validFrom.formatYYYYMMDD() + '_' + entity.validTo.formatYYYYMMDD();
		entity.isRemoved = false;

		entity.nameL0 = name + '_L0';
		entity.nameL1 = name + '_L1';
		entity.nameL2 = name + '_L2';
		entity.weeklyDayTypeSUN = AttDayType.LEGAL_HOLIDAY;
		entity.weeklyDayTypeMON = AttDayType.WORKDAY;
		entity.weeklyDayTypeTUE = AttDayType.WORKDAY;
		entity.weeklyDayTypeWED = AttDayType.WORKDAY;
		entity.weeklyDayTypeTHU = AttDayType.WORKDAY;
		entity.weeklyDayTypeFRI = AttDayType.WORKDAY;
		entity.weeklyDayTypeSAT = AttDayType.HOLIDAY;
		entity.startTime = AttTime.valueOf(9*60);
		entity.endTime = AttTime.valueOf(18*60);
		entity.flexStartTime = AttTime.valueOf(8*60);
		entity.flexEndTime = AttTime.valueOf(22*60);
		entity.contractedWorkHours = AttDailyTime.valueOf(7*60 + 30);
		entity.rest1StartTime = AttTime.valueOf(12*60);
		entity.rest1EndTime = AttTime.valueOf(13*60);
		entity.rest2StartTime = AttTime.valueOf(16*60);
		entity.rest2EndTime = AttTime.valueOf(16*60 + 30);
		entity.rest3StartTime = AttTime.valueOf(19*60);
		entity.rest3EndTime = AttTime.valueOf(19*60 + 30);
		entity.rest4StartTime = AttTime.valueOf(21*60);
		entity.rest4EndTime = AttTime.valueOf(21*60 + 30);
		entity.rest5StartTime = AttTime.valueOf(23*60);
		entity.rest5EndTime = AttTime.valueOf(23*60 + 30);
		entity.startOfNightWork = 22 * 60 + 2;
		entity.endOfNightWork = 5 * 60;
		entity.legalWorkTimeADay = AttDailyTime.valueOf(8*60);
		entity.contractedWorkHours = AttDailyTime.valueOf(7*60 + 30);
		entity.legalWorkTimeAWeek = AttDailyTime.valueOf(40*60);
		entity.automaticLegalHolidayAssign = false;
		entity.boundaryOfStartTime = AttTime.valueOf(0 * 60);
		entity.boundaryOfEndTime = AttTime.valueOf(5 * 60);
		entity.divideWorkHoursAtDayBoundary = AttCalcLogic.ClassificationNextDayWork.None.name();
		entity.allowanceOnEveryHoliday = false;
		entity.isIncludeHolidayWorkInPlainTime = true;
		entity.allowWorkDuringHalfDayLeave = false;
		entity.legalHolidayAssignmentDays = 1;
		entity.legalHolidayAssignmentPeriod = 7;
		entity.offsetOvertimeAndDeductionTime = true;
		entity.allowanceForOverContracted = false;
		entity.basicSalaryForLegalHoliday = false;
		entity.restTimeWoStartEndTime = AttDailyTime.valueOf(0*60 + 45);
		//午前半休
		entity.amStartTime = AttTime.valueOf(13*60);
		entity.amEndTime = AttTime.valueOf(18*60);
		entity.amContractedWorkHours = AttDailyTime.valueOf(4*60 + 10);
		entity.amRest1StartTime = AttTime.valueOf(14*60);
		entity.amRest1EndTime = AttTime.valueOf(14*60 + 10);
		entity.amRest2StartTime = AttTime.valueOf(15*60 + 30);
		entity.amRest2EndTime = AttTime.valueOf(15*60 + 40);
		entity.amRest3StartTime = AttTime.valueOf(16*60);
		entity.amRest3EndTime = AttTime.valueOf(16*60 + 10);
		entity.amRest4StartTime = AttTime.valueOf(16*60 + 30);
		entity.amRest4EndTime = AttTime.valueOf(16*60 + 40);
		entity.amRest5StartTime = AttTime.valueOf(17*60);
		entity.amRest5EndTime = AttTime.valueOf(17*60 + 10);
		entity.amRestTimeWoStartEndTime = AttDailyTime.valueOf(0*60 + 50);
		entity.useAMHalfDayLeave = true;
		//午後半休
		entity.pmStartTime = AttTime.valueOf(9*60);
		entity.pmEndTime = AttTime.valueOf(12*60);
		entity.pmContractedWorkHours = AttDailyTime.valueOf(2*60 + 10);
		entity.pmRest1StartTime = AttTime.valueOf(9*60);
		entity.pmRest1EndTime = AttTime.valueOf(9*60 + 10);
		entity.pmRest2StartTime = AttTime.valueOf(9*60 + 30);
		entity.pmRest2EndTime = AttTime.valueOf(9*60 + 40);
		entity.pmRest3StartTime = AttTime.valueOf(10*60);
		entity.pmRest3EndTime = AttTime.valueOf(10*60 + 10);
		entity.pmRest4StartTime = AttTime.valueOf(10*60 + 30);
		entity.pmRest4EndTime = AttTime.valueOf(10*60 + 40);
		entity.pmRest5StartTime = AttTime.valueOf(11*60);
		entity.pmRest5EndTime = AttTime.valueOf(11*60 + 10);
		entity.pmRestTimeWoStartEndTime = AttDailyTime.valueOf(0*60 + 55);
		entity.usePMHalfDayLeave = true;

		// 申請オプション
		entity.addCompensationTimeToWorkHours = false;
		entity.permitOvertimeWorkUntilContractedHours = false;
		entity.allowToChangeApproverSelf = false;
		// 休暇
		if ((leaveCodeList != null) && (! leaveCodeList.isEmpty())) {
			entity.leaveCodeList = new List<String>();
			for (String code : leaveCodeList) {
				entity.leaveCodeList.add(code);
			}
		}
		// 勤務パターン
		entity.patternCodeList = patternCodeList;

		// 利用機能系オプション
		entity.allocatedSubstituteLeaveType = new List<AttSubstituteLeaveType>{AttSubstituteLeaveType.SUBSTITUTE};

		entity.useLegalRestCheck1 = true;
		entity.legalRestCheck1WorkTimeThreshold = AttDailyTime.valueOf(6 * 60);
		entity.legalRestCheck1RequiredRestTime = AttDailyTime.valueOf(45);
		entity.useLegalRestCheck2 = true;
		entity.legalRestCheck2WorkTimeThreshold = AttDailyTime.valueOf(8 * 60);
		entity.legalRestCheck2RequiredRestTime = AttDailyTime.valueOf(60);

		//直行直帰申請
		entity.useDirectApply = true;
		entity.directApplyStartTime = AttTime.valueOf(9*60);
		entity.directApplyEndTime = AttTime.valueOf(18*60);
		entity.directApplyRest1StartTime = AttTime.valueOf(10*60);
		entity.directApplyRest1EndTime = AttTime.valueOf(10*60 + 10);
		entity.directApplyRest2StartTime = AttTime.valueOf(11*60 + 30);
		entity.directApplyRest2EndTime = AttTime.valueOf(11*60 + 40);
		entity.directApplyRest3StartTime = AttTime.valueOf(12*60);
		entity.directApplyRest3EndTime = AttTime.valueOf(12*60 + 10);
		entity.directApplyRest4StartTime = AttTime.valueOf(14*60 + 30);
		entity.directApplyRest4EndTime = AttTime.valueOf(14*60 + 40);
		entity.directApplyRest5StartTime = AttTime.valueOf(16*60);
		entity.directApplyRest5EndTime = AttTime.valueOf(16*60 + 10);

		return entity;
	}

}