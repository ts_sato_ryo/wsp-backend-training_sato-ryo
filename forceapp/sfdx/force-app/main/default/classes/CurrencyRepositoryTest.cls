/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test for repository of Currency
 */
@isTest
private class CurrencyRepositoryTest {

	/**
	 * Test for getEntity
	 */
	@isTest static void getEntityTest() {
		ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('JPY', 'Japanese Yen');
		currencyObj.DecimalPlaces__c = 0;
		currencyObj.Symbol__c = '¥';
		update currencyObj;

		CurrencyRepository repo = new CurrencyRepository();
		CurrencyEntity entity = repo.getEntity(currencyObj.Code__c);

		System.assertNotEquals(null, entity);
		System.assertEquals(currencyObj.Name, entity.name);
		System.assertEquals(currencyObj.Code__c, entity.code);
		System.assertEquals(currencyObj.IsoCurrencyCode__c, entity.isoCurrencyCode.value);
		System.assertEquals(currencyObj.Name_L0__c, entity.nameL0);
		System.assertEquals(currencyObj.Name_L1__c, entity.nameL1);
		System.assertEquals(currencyObj.Name_L2__c, entity.nameL2);
		System.assertEquals(currencyObj.DecimalPlaces__c, entity.decimalPlaces);
		System.assertEquals(currencyObj.Symbol__c, entity.symbol);
	}

	/**
	 * Test for searchEntity method
	 * Confirm enable to search data properly
	 */
	@isTest static void searchEntityTest() {
		ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('JPY', 'Japanese Yen');
		currencyObj.DecimalPlaces__c = 0;
		currencyObj.Symbol__c = '¥';
		update currencyObj;

		CurrencyRepository repo = new CurrencyRepository();
		CurrencyRepository.SearchFilter filter;
		List<CurrencyEntity> resEntities;

		filter = new CurrencyRepository.SearchFilter();
		// Search by ID
		filter.ids = new Set<Id>{currencyObj.Id};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(currencyObj.Code__c, resEntities[0].code);
		System.assertEquals(currencyObj.Name, resEntities[0].name);

		// Search by Code
		filter = new CurrencyRepository.SearchFilter();
		filter.codes = new List<String>{currencyObj.Code__c};
		resEntities = repo.searchEntity(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(currencyObj.Code__c, resEntities[0].code);
		System.assertEquals(currencyObj.Name, resEntities[0].name);
	}

}