/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description DepartmentInsertBatchのテストクラス
 */
@isTest
private class DepartmentInsertBatchTest {

	/**
	 * 新規部署登録のテスト
	 * エラーなしで処理が完了することを確認する
	 */
	@isTest
	static void executeBatchTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('部署', testCompany.Id);
		testDept.Code__c = 'batch1';
		update testDept;

		// 部署インポートデータを作成
		List<ComDepartmentImport__c> deptImpList = new List<ComDepartmentImport__c>();
		for(Integer i=0; i<10; i++) {
			Integer cnt = i + 1;

			ComDepartmentImport__c deptImp = new ComDepartmentImport__c();
			deptImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
			deptImp.Code__c = 'batch' + cnt;
			deptImp.CompanyCode__c = testCompany.Code__c;
			deptImp.Name_L0__c = 'dept' + cnt;
			deptImp.Name_L1__c = '部署' + cnt;
			deptImp.Name_L2__c = 'busyo' + cnt;
			deptImp.ManagerBaseCode__c = '000001';
			deptImp.ParentBaseCode__c = '000001';
			deptImp.HistoryComment__c = 'バッチ登録';
			deptImp.Status__c = ImportStatus.WAITING.value;

			deptImpList.add(deptImp);
		}
		insert deptImpList;

		// インポートバッチデータを作成
		ImportBatchService service = new ImportBatchService(ImportBatchType.EMPLOYEE_DEPT, null);
		Id importBatchId = service.createImportBatchData();

		Test.startTest();

		// 新規部署登録バッチ起動
		DepartmentInsertBatch deptInsert = new DepartmentInsertBatch(importBatchId);
		Database.executeBatch(deptInsert);

		Test.stopTest();

		List<ComDeptBase__c> deptList = [
				SELECT
					Id,
					Name,
					Code__c,
					CompanyId__r.Code__c,
					(SELECT
						Id,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						ManagerBaseId__r.Code__c,
						ParentBaseId__r.Code__c,
						HistoryComment__c,
						ValidFrom__c,
						ValidTo__c
					 FROM Histories__r)
				FROM ComDeptBase__c];

		Map<String, ComDeptBase__c> deptMap = new Map<String, ComDeptBase__c>();
		for (ComDeptBase__c dept : deptList) {
			deptMap.put(dept.Code__c, dept);
		}

		List<ComDepartmentImport__c> AfterDeptImpList = [
				SELECT
					Id,
					RevisionDate__c,
					Code__c,
					CompanyCode__c,
					Name_L0__c,
					Name_L1__c,
					Name_L2__c,
					ManagerBaseCode__c,
					ParentBaseCode__c,
					HistoryComment__c,
					Status__c,
					Error__c
				FROM ComDepartmentImport__c
				ORDER BY Code__c];

		// インポートデータが部署マスタに反映されていることを確認
		// 1件目は既存データで処理対象外のため、2件目以降を確認する
		for(Integer i=1; i<10; i++) {
			ComDepartmentImport__c deptImp = AfterDeptImpList[i];
			System.assertEquals(ImportStatus.WAITING.value, deptImp.Status__c);
			System.assertEquals(null, deptImp.Error__c);

			ComDeptBase__c deptBase = deptMap.get(deptImp.Code__c);
			System.assertEquals(deptImp.Name_L0__c, deptBase.Name);
			System.assertEquals(deptImp.Code__c, deptBase.Code__c);
			System.assertEquals(deptImp.CompanyCode__c, deptBase.CompanyId__r.Code__c);

			ComDeptHistory__c deptHistory = deptBase.Histories__r[0];
			System.assertEquals(deptImp.Name_L0__c, deptHistory.Name);
			System.assertEquals(deptImp.Name_L0__c, deptHistory.Name_L0__c);
			System.assertEquals(deptImp.Name_L1__c, deptHistory.Name_L1__c);
			System.assertEquals(deptImp.Name_L2__c, deptHistory.Name_L2__c);
			System.assertEquals(null, deptHistory.ManagerBaseId__r.Code__c);
			System.assertEquals(null, deptHistory.ParentBaseId__r.Code__c);
			System.assertEquals(deptImp.HistoryComment__c, deptHistory.HistoryComment__c);
			System.assertEquals(deptImp.RevisionDate__c, deptHistory.ValidFrom__c);
			System.assertEquals(Date.newInstance(2101, 1, 1), deptHistory.ValidTo__c);
		}

		ComImportBatch__c batch = [
				SELECT
					Status__c,
					Type__c,
					Count__c,
					SuccessCount__c,
					FailureCount__c
				FROM ComImportBatch__c
				WHERE Id = :importBatchId];

		// インポートバッチデータが正しく更新されていることを確認
		System.assertEquals(ImportBatchStatus.PROCESSING.value, batch.Status__c);
		System.assertEquals(ImportBatchType.EMPLOYEE_DEPT.value, batch.Type__c);
		System.assertEquals(deptImpList.size(), batch.Count__c);
		System.assertEquals(0, batch.SuccessCount__c);
		System.assertEquals(0, batch.FailureCount__c);
	}

	/**
	 * 新規部署登録のテスト
	 * エラーで登録できなかったインポートデータがステータスが「Error」に更新されることを確認する
	 */
	@isTest
	static void executeBatchErrorTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);

		// 部署インポートデータを作成
		List<ComDepartmentImport__c> deptImpList = new List<ComDepartmentImport__c>();
		// 1件目
		ComDepartmentImport__c deptImp1 = new ComDepartmentImport__c();
		deptImp1.RevisionDate__c = Date.newInstance(2018, 1, 1);
		deptImp1.Code__c = 'batch1';
		deptImp1.CompanyCode__c = testCompany.Code__c;
		deptImp1.Name_L0__c = 'dept1';
		deptImp1.HistoryComment__c = 'バッチ登録';
		deptImp1.Status__c = ImportStatus.WAITING.value;
		deptImpList.add(deptImp1);
		// 2件目 部署名(L0)未設定
		ComDepartmentImport__c deptImp2 = new ComDepartmentImport__c();
		deptImp2.RevisionDate__c = Date.newInstance(2018, 1, 1);
		deptImp2.Code__c = 'batch2';
		deptImp2.CompanyCode__c = testCompany.Code__c;
		deptImp2.Name_L0__c = null;
		deptImp2.HistoryComment__c = 'バッチ登録';
		deptImp2.Status__c = ImportStatus.WAITING.value;
		deptImpList.add(deptImp2);

		insert deptImpList;

		// インポートバッチデータを作成
		ImportBatchService service = new ImportBatchService(ImportBatchType.EMPLOYEE_DEPT, null);
		Id importBatchId = service.createImportBatchData();

		Test.startTest();

		// 新規部署登録バッチ起動
		DepartmentInsertBatch deptInsert = new DepartmentInsertBatch(importBatchId);
		Database.executeBatch(deptInsert);

		Test.stopTest();

		List<ComDepartmentImport__c> AfterDeptImpList = [
				SELECT
					Id,
					RevisionDate__c,
					Code__c,
					CompanyCode__c,
					Name_L0__c,
					Name_L1__c,
					Name_L2__c,
					ManagerBaseCode__c,
					ParentBaseCode__c,
					HistoryComment__c,
					Status__c,
					Error__c
				FROM ComDepartmentImport__c
				ORDER BY Code__c];

		// インポートデータのステータスを確認する
		// 1件目は登録成功
		System.assertEquals(ImportStatus.WAITING.value, AfterDeptImpList[0].Status__c);
		// 2件目は部署名(L0)未設定のためエラー
		System.assertEquals(ImportStatus.ERROR.value, AfterDeptImpList[1].Status__c);
	}

	/**
	 * バリデーションのテスト
	 */
	@isTest
	static void validateDeptImpEntityTest() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);

		DepartmentInsertBatch deptInsert = new DepartmentInsertBatch(null);
		deptInsert.companyMap = new Map<String, CompanyEntity>();
		deptInsert.deptMap = new Map<List<String>, DepartmentBaseEntity>();

		// 会社データをバッチクラスのMapに格納しておく
		CompanyRepository companyRepo = new CompanyRepository();
		for (CompanyEntity company : companyRepo.getEntityList(new List<String>{testCompany.Code__c})) {
			deptInsert.companyMap.put(company.code, company);
		}

		// 部署インポートエンティティを作成
		List<DepartmentImportEntity> deptImpList = new List<DepartmentImportEntity>();
		for(Integer i=0; i<5; i++) {
			Integer cnt = i + 1;

			DepartmentImportEntity deptImp = new DepartmentImportEntity();
			deptImp.revisionDate = AppDate.newInstance(2018, 1, 1);
			deptImp.code = 'batch' + cnt;
			deptImp.companyCode = testCompany.Code__c;
			deptImp.name_L0 = 'dept' + cnt;
			deptImp.name_L1 = '部署' + cnt;
			deptImp.name_L2 = 'busyo' + cnt;
			deptImp.managerBaseCode = '000001';
			deptImp.parentBaseCode = '000001';
			deptImp.historyComment = 'バッチ登録';

			deptImpList.add(deptImp);
		}

		Test.startTest();

		// 改訂日が未設定
		DepartmentImportEntity deptImp = deptImpList[0];
		deptImp.revisionDate = null;
		deptInsert.validateDeptImpEntity(deptImp);
		System.assertEquals(ImportStatus.ERROR, deptImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_RevisionDate}), deptImp.error);

		// 部署コードが未設定
		deptImp = deptImpList[1];
		deptImp.code = null;
		deptInsert.validateDeptImpEntity(deptImp);
		System.assertEquals(ImportStatus.ERROR, deptImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentCode}), deptImp.error);

		// 会社コードが未設定
		deptImp = deptImpList[2];
		deptImp.companyCode = null;
		deptInsert.validateDeptImpEntity(deptImp);
		System.assertEquals(ImportStatus.ERROR, deptImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode}), deptImp.error);

		// 指定した会社が存在しない
		deptImp = deptImpList[3];
		deptImp.companyCode = '999';
		deptInsert.validateDeptImpEntity(deptImp);
		System.assertEquals(ImportStatus.ERROR, deptImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Batch_Lbl_CompanyCode}), deptImp.error);

		// 部署名(L0)が未設定
		deptImp = deptImpList[4];
		deptImp.name_L0 = null;
		deptInsert.validateDeptImpEntity(deptImp);
		System.assertEquals(ImportStatus.ERROR, deptImp.status);
		System.assertEquals(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Batch_Lbl_DepartmentName}), deptImp.error);

		Test.stopTest();
	}

	/**
	 * インポートデータに有効期間終了日を更新するデータが混ざっている場合のテスト
	 * エラーなしで処理が完了することを確認する
	 */
	@isTest
	static void executeBatchTestUpdateValidEndDate() {
		ComTestDataUtility.createOrgSetting();
		ComCompany__c testCompany = ComTestDataUtility.createCompany('会社', null);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('部署', testCompany.Id);
		testDept.Code__c = 'batch1';
		update testDept;

		// 部署インポートデータを作成
		// 最初の2件は新規登録データ
		List<ComDepartmentImport__c> deptImpList = new List<ComDepartmentImport__c>();
		for(Integer i=0; i<2; i++) {
			Integer cnt = i + 1;

			ComDepartmentImport__c deptImp = new ComDepartmentImport__c();
			deptImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
			deptImp.Code__c = 'batch' + cnt;
			deptImp.CompanyCode__c = testCompany.Code__c;
			deptImp.Name_L0__c = 'dept' + cnt;
			deptImp.Name_L1__c = '部署' + cnt;
			deptImp.Name_L2__c = 'busyo' + cnt;
			deptImp.ManagerBaseCode__c = '000001';
			deptImp.ParentBaseCode__c = '000001';
			deptImp.HistoryComment__c = 'バッチ登録';
			deptImp.Status__c = ImportStatus.WAITING.value;

			deptImpList.add(deptImp);
		}
		// 次の2件は有効期間終了日更新データ
		for(Integer i=0; i<2; i++) {
			Integer cnt = i + 1;

			ComDepartmentImport__c deptImp = new ComDepartmentImport__c();
			deptImp.RevisionDate__c = Date.newInstance(2018, 1, 1);
			deptImp.AbolishmentDate__c = Date.newInstance(2018, 1, 1);
			deptImp.Code__c = 'batch' + cnt;
			deptImp.CompanyCode__c = testCompany.Code__c;
			deptImp.Status__c = ImportStatus.WAITING.value;

			deptImpList.add(deptImp);
		}
		insert deptImpList;

		// インポートバッチデータを作成
		ImportBatchService service = new ImportBatchService(ImportBatchType.EMPLOYEE_DEPT, null);
		Id importBatchId = service.createImportBatchData();

		Test.startTest();

		// 新規部署登録バッチ起動
		DepartmentInsertBatch deptInsert = new DepartmentInsertBatch(importBatchId);
		Database.executeBatch(deptInsert);

		Test.stopTest();

		List<ComDepartmentImport__c> AfterDeptImpList = [
				SELECT
					Id,
					RevisionDate__c,
					Code__c,
					CompanyCode__c,
					Status__c,
					Error__c
				FROM ComDepartmentImport__c
				ORDER BY Code__c];

		// インポートデータが正常に処理されたことを確認
		for (ComDepartmentImport__c empImp : AfterDeptImpList) {
			System.assertEquals(ImportStatus.WAITING.value, empImp.Status__c);
			System.assertEquals(null, empImp.Error__c);
		}

		ComImportBatch__c batch = [
				SELECT
					Status__c,
					Type__c,
					Count__c,
					SuccessCount__c,
					FailureCount__c
				FROM ComImportBatch__c
				WHERE Id = :importBatchId];

		// インポートバッチデータが正しく更新されていることを確認
		System.assertEquals(ImportBatchStatus.PROCESSING.value, batch.Status__c);
		System.assertEquals(ImportBatchType.EMPLOYEE_DEPT.value, batch.Type__c);
		System.assertEquals(deptImpList.size(), batch.Count__c);
		System.assertEquals(0, batch.SuccessCount__c);
		System.assertEquals(0, batch.FailureCount__c);
	}
}