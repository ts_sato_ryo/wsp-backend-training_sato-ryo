/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 組織リポジトリ
 */
public with sharing class OrganizationSettingRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				ComOrgSetting__c.Id,
				ComOrgSetting__c.Name,
				ComOrgSetting__c.Language_0__c,
				ComOrgSetting__c.Language_1__c,
				ComOrgSetting__c.Language_2__c};
	}

	private static final Boolean NOT_FOR_UPDATE = false;

	/**
	 * エンティティを取得する
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 組織エンティティ、1件も登録されていない場合はnull
	 */
	public OrganizationSettingEntity getEntity() {
		List<OrganizationSettingEntity> entityList = getEntityList(NOT_FOR_UPDATE);

		OrganizationSettingEntity retEntity;
		if (entityList == null || entityList.isEmpty()) {
			retEntity = null;
		} else {
			retEntity = entityList[0];
		}
		return retEntity;
	}

	/**
	 * エンティティを取得する
	 * @return 組織エンティティ
	 */
	public List<OrganizationSettingEntity> getEntityList() {
		return getEntityList(NOT_FOR_UPDATE);
	}

	/**
	 * エンティティを取得する
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 組織エンティティ
	 */
	public List<OrganizationSettingEntity> getEntityList(Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComOrgSetting__c.SObjectType.getDescribe().getName()
				+ ' LIMIT 1';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// クエリ実行
		System.debug('OrganizationSettingRepository.getEntity: SOQL ==>' + soql);
		List<ComOrgSetting__c> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<OrganizationSettingEntity> entityList = new List<OrganizationSettingEntity>();
		for (ComOrgSetting__c sObj : sObjList) {
			entityList.add(createEntity(sObj));
		}

		return entityList;
	}

	/**
	 * エンティティを保存する
	 * @param entity 作成元のエンティティ
	 * @return レコード保存結果
	 */
	public SaveResult saveEntity(OrganizationSettingEntity entity) {
		return saveEntityList(new List<OrganizationSettingEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return レコード保存結果
	 */
	public SaveResult saveEntityList(List<OrganizationSettingEntity> entityList, Boolean allOrNone) {

		// SObjectを作成する
		List<ComOrgSetting__c> sObjList = new List<ComOrgSetting__c>();
		for (OrganizationSettingEntity entity : entityList) {
			sObjList.add(createSobject(entity));
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sObjList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * CompanyEntityからComCompany__cを作成する
	 * @param entity 作成元のエンティティ
	 * @return 作成したSObject
	 */
	@TestVisible
	private ComOrgSetting__c createSobject(OrganizationSettingEntity entity) {
		ComOrgSetting__c sObj = new ComOrgSetting__c();

		sObj.Id = entity.Id;

		if (entity.isChanged(OrganizationSettingEntity.Field.LANGUAGE0)) {
			sObj.Language_0__c = AppConverter.stringValue(entity.language0);
		}
		if (entity.isChanged(OrganizationSettingEntity.Field.LANGUAGE1)) {
			sObj.Language_1__c = AppConverter.stringValue(entity.language1);
		}
		if (entity.isChanged(OrganizationSettingEntity.Field.LANGUAGE2)) {
			sObj.Language_2__c = AppConverter.stringValue(entity.language2);
		}

		return sObj;
	}

	/**
	 * SObjectからエンティティを作成する
	 * @param sObj 作成元のSObject
	 * @return sObjから作成したエンティティ
	 */
	private OrganizationSettingEntity createEntity(ComOrgSetting__c sObj) {
		OrganizationSettingEntity entity = new OrganizationSettingEntity();
		entity.setId(sObj.Id);
		entity.name = sObj.Name;
		entity.language0 = AppLanguage.valueOf(sObj.Language_0__c);
		entity.language1 = AppLanguage.valueOf(sObj.Language_1__c);
		entity.language2 = AppLanguage.valueOf(sObj.Language_2__c);

		// 変更情報はリセットしておく
		entity.resetChanged();

		return entity;
	}
}
