/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Test class for ComJobUsedIn
 */
@isTest
private class ComJobUsedInTest {

	/**
	 * Check if it compare properly
	 */
	@isTest static void equalsTest() {

		// Equal
		System.assertEquals(ComJobUsedIn.EXPENSE_REPORT, ComJobUsedIn.valueOf('ExpenseReport'));
		System.assertEquals(ComJobUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ComJobUsedIn.valueOf('ExpenseRequestAndExpenseReport'));

		// Values are different
		System.assertEquals(false, ComJobUsedIn.EXPENSE_REPORT.equals(ComJobUsedIn.valueOf('ExpenseRequestAndExpenseReport')));

		// Class type is different
		System.assertEquals(false, ComJobUsedIn.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// Value is null
		System.assertEquals(false, ComJobUsedIn.EXPENSE_REPORT.equals(null));
	}
}
