/**
 */
@isTest
private class AttCalcLostTimeLogicTest {
	enum Option {
		PermitedLateArriveTime,
		PermitedEarlyLeaveTime,
		PermitedStartBreakTime,
		PermitedEndBreakTime,
		InputRestTime,
		ContractRestTime,
		ContractWorkTime,
		ConvertdStartRestRanges,
		ConvertdEndRestRanges,
		ConvertedWorkTime,
		UnpaidLeaveRestRangesStartTime,
		UnpaidLeaveRestRangesEndTime,
		UnpaidLeaveTime
	}

	public class Input extends AttCalcLostTimeLogic.Input {
		//public AttCalcRangesDto permitedLostTimeRanges;
		public Input(Integer inputStartTime,
				Integer inputEndTime,
				AttCalcRangesDto inputRestRanges,
				Integer startTime,
				Integer endTime,
				AttCalcRangesDto contractRestRanges) {
			this.inputStartTime = inputStartTime;
			this.inputEndTime = inputEndTime;
			this.inputRestRanges = inputRestRanges;
			this.startTime = startTime;
			this.endTime = endTime;
			this.contractRestRanges = contractRestRanges;
			this.permitedLostTimeRanges = new AttCalcRangesDto();
			this.convertdRestRanges = new AttCalcRangesDto();
			this.inputRestTime = 0;
			this.contractWorkTime = endTime - startTime - contractRestRanges.cutBetween(startTime, endTime).total();
		}
		public Input(Integer inputStartTime,
				Integer inputEndTime,
				AttCalcRangesDto inputRestRanges,
				Integer startTime,
				Integer endTime,
				AttCalcRangesDto contractRestRanges,
				Map<Option,Integer> mapOpt) {
			this(inputStartTime, inputEndTime, inputRestRanges, startTime, endTime, contractRestRanges);
			if(mapOpt.containsKey(Option.permitedLateArriveTime))
				this.permitedLostTimeRanges = this.permitedLostTimeRanges.insertAndMerge(AttCalcRangesDto.RS(startTime, mapOpt.get(Option.permitedLateArriveTime)));
			if(mapOpt.containsKey(Option.permitedEarlyLeaveTime))
				this.permitedLostTimeRanges = this.permitedLostTimeRanges.insertAndMerge(AttCalcRangesDto.RS(mapOpt.get(Option.permitedEarlyLeaveTime), endTime));
			if(mapOpt.containsKey(Option.permitedStartBreakTime)) {
				Integer permitedStartBreakTime = mapOpt.get(Option.permitedStartBreakTime);
				Integer permitedEndBreakTime = mapOpt.get(Option.permitedEndBreakTime);
				System.assert(permitedStartBreakTime < permitedEndBreakTime);
				this.permitedLostTimeRanges = this.permitedLostTimeRanges.insertAndMerge(AttCalcRangesDto.RS(permitedStartBreakTime, permitedEndBreakTime));
			}
			if(mapOpt.containsKey(Option.ConvertdStartRestRanges)) {
				Integer permitedConvertedWorkStartTime = mapOpt.get(Option.ConvertdStartRestRanges);
				Integer permitedConvertedWorkEndTime = mapOpt.get(Option.ConvertdEndRestRanges);
				System.assert(permitedConvertedWorkStartTime < permitedConvertedWorkEndTime);
				this.convertdRestRanges = this.convertdRestRanges.insertAndMerge(AttCalcRangesDto.RS(permitedConvertedWorkStartTime, permitedConvertedWorkEndTime).exclude(this.contractRestRanges));
			}
			if(mapOpt.containsKey(Option.ContractRestTime))
				this.contractRestTime = mapOpt.get(Option.ContractRestTime);
			if(mapOpt.containsKey(Option.InputRestTime))
				this.inputRestTime = mapOpt.get(Option.InputRestTime);
			if(mapOpt.containsKey(Option.ContractWorkTime))
				this.contractWorkTime = mapOpt.get(Option.ContractWorkTime);
			if(mapOpt.containsKey(Option.UnpaidLeaveRestRangesStartTime)) {
				Integer unpaidLeaveRestRangesStartTime = mapOpt.get(Option.UnpaidLeaveRestRangesStartTime);
				Integer unpaidLeaveRestRangesEndTime = mapOpt.get(Option.UnpaidLeaveRestRangesEndTime);
				System.assert(unpaidLeaveRestRangesStartTime < unpaidLeaveRestRangesEndTime);
				this.unpaidLeaveRestRanges = this.unpaidLeaveRestRanges.insertAndMerge(
						AttCalcRangesDto.RS(unpaidLeaveRestRangesStartTime, unpaidLeaveRestRangesEndTime)
						.exclude(this.contractRestRanges));
			}
			if(mapOpt.containsKey(Option.UnpaidLeaveTime)) {
				this.unpaidLeaveTime = mapOpt.get(Option.UnpaidLeaveTime);
			}
		}
	}
	public class Output extends AttCalcLostTimeLogic.Output {
		public Output() {
			realWorkTime = -9999;
			earlyLeaveTime = -9999;
			lateArriveTime = -9999;
			breakTime = -9999;
			earlyLeaveLostTime = -9999;
			lateArriveLostTime = -9999;
			authorizedEarlyLeaveTime = -9999;
			authorizedLateArriveTime = -9999;
			authorizedBreakTime = -9999;
			gapTime = -9999;
		}
		public Output(Integer workTimeInContract,
				Integer lateArriveTime,
				Integer earlyLeaveTime,
				Integer breakTime,
				Integer lateArriveLostTime,
				Integer earlyLeaveLostTime,
				Integer breakLostTime,
				Integer authorizedLateArriveTime,
				Integer authorizedEarlyLeaveTime,
				Integer authorizedBreakTime,
				Integer realContractTime,
				Integer gapTime) {
			this.realWorkTime = workTimeInContract;
			this.earlyLeaveTime = earlyLeaveTime;
			this.lateArriveTime = lateArriveTime;
			this.breakTime = breakTime;
			this.earlyLeaveLostTime = earlyLeaveLostTime;
			this.lateArriveLostTime = lateArriveLostTime;
			this.breakLostTime = breakLostTime;
			this.authorizedEarlyLeaveTime = authorizedEarlyLeaveTime;
			this.authorizedLateArriveTime = authorizedLateArriveTime;
			this.authorizedBreakTime = authorizedBreakTime;
			this.gapTime = gapTime;
			this.realContractTime = realContractTime;
		}
		public void assertOut(Output o) {
			System.assertEquals(o.realWorkTime, realWorkTime);
			System.assertEquals(o.earlyLeaveTime, earlyLeaveTime);
			System.assertEquals(o.lateArriveTime, lateArriveTime);
			System.assertEquals(o.breakTime, breakTime);
			System.assertEquals(o.earlyLeaveLostTime, earlyLeaveLostTime);
			System.assertEquals(o.lateArriveLostTime, lateArriveLostTime);
			System.assertEquals(o.breakLostTime, breakLostTime);
			System.assertEquals(o.authorizedEarlyLeaveTime, authorizedEarlyLeaveTime);
			System.assertEquals(o.authorizedLateArriveTime, authorizedLateArriveTime);
			System.assertEquals(o.authorizedBreakTime, authorizedBreakTime);
			System.assertEquals(o.realContractTime, realContractTime);
			System.assertEquals(o.gapTime, gapTime);
		}
	}

	static void verify(Input input, Output tobe) {
		AttCalcLostTimeLogic logic = new AttCalcLostTimeLogic();
		Output output = new Output();
		logic.apply(output, input);
		output.assertOut(tobe);
	}
	@isTest static void testApply() {
		// 1-1 正常ケース
		verify(new Input(9*60,             // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60,			 // 所定内勤務時間
					0,               // 遅刻時間
					0,				 // 早退時間
					60,				 // 休憩時間
					0,				 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間;
					0));			 // 過不足時間

		// 1-2 正常ケース 早め出社
		verify(new Input(9*60-1,           // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60,			 // 所定内勤務時間
					0,               // 遅刻時間
					0,				 // 早退時間
					60,				 // 休憩時間
					0,				 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間;
					0));			 // 過不足時間

		// 1-3 正常ケース 遅め退社
		verify(new Input(9*60,           	 // 出社
				   18*60+1,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60,			 // 所定内勤務時間
					0,               // 遅刻時間
					0,				 // 早退時間
					60,				 // 休憩時間
					0,				 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 2 遅刻
		verify(new Input(10*60,			 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(7*60,			 // 所定内勤務時間
					1*60,			 // 遅刻時間
					0,				 // 早退時間
					60,				 // 休憩時間
					1*60,			 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 2-1 遅刻 遅め退社
		verify(new Input(10*60,			 // 出社
				   18*60+30,		 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(7*60,			 // 所定内勤務時間
					1*60,			 // 遅刻時間
					0,				 // 早退時間
					60,				 // 休憩時間
					1*60,			 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 2-2 遅刻 休憩短縮(60遅刻 30休憩短縮)
		verify(new Input(10*60,			 // 出社
				   18*60,		 	 // 退社
				   AttCalcRangesDto.RS(12*60,13*60-30),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(7*60+30,		 // 所定内勤務時間
					1*60,			 // 遅刻時間
					0,				 // 早退時間
					30,				 // 休憩時間
					30, 			 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 2-3 遅刻 休憩短縮(60遅刻 60休憩短縮)
		verify(new Input(10*60,			 // 出社
				   18*60,		 	// 退社
				   AttCalcRangesDto.RS(),	 		 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60,			 // 所定内勤務時間
					1*60,			 // 遅刻時間
					0,				 // 早退時間
					0,				 // 休憩時間
					0,	 			 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 2-4 遅刻 休憩短縮(30遅刻 60休憩短縮)
		verify(new Input(9*60+30,			 // 出社
				   18*60,		 	// 退社
				   AttCalcRangesDto.RS(),	 		 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60+30,		 // 所定内勤務時間
					30,				 // 遅刻時間
					0,				 // 早退時間
					0,				 // 休憩時間
					0,	 			 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					30));			 // 過不足時間

		// 3 早退
		verify(new Input(9*60,			 // 出社
				   16*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(6*60,			 // 所定内勤務時間
					0,			 	 // 遅刻時間
					2*60,				 // 早退時間
					60,				 // 休憩時間
					0,			 	 // 遅刻控除時間
					2*60,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 3-1 早退 早め出社
		verify(new Input(7*60,			 // 出社
				   16*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(6*60,			 // 所定内勤務時間
					0,			 	 // 遅刻時間
					2*60,			 // 早退時間
					60,				 // 休憩時間
					0,			 	 // 遅刻控除時間
					2*60,			 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 3-2 早退 120分早退 45分休憩短縮
		verify(new Input(9*60,			 // 出社
				   16*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,12*60+15),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(6*60+45,		 // 所定内勤務時間
					0,			 	 // 遅刻時間
					2*60,			 // 早退時間
					15,				 // 休憩時間
					0, 				 // 遅刻控除時間
					1*60+15,		 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 3-2 早退 60分早退 60分休憩短縮
		verify(new Input(9*60,			 // 出社
				   17*60,			 // 退社
				   AttCalcRangesDto.RS(),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60,			 // 所定内勤務時間
					0,			 	 // 遅刻時間
					60,				 // 早退時間
					0,				 // 休憩時間
					0,	 			 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 3-2 早退 15分早退 60分休憩短縮
		verify(new Input(9*60,			 // 出社
				   17*60+45,		 // 退社
				   AttCalcRangesDto.RS(),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60+45,		 // 所定内勤務時間
					0,				 // 遅刻時間
					15,				 // 早退時間
					0,				 // 休憩時間
					0,	 			 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					45));			 // 過不足時間

		// 4 休憩  30 休憩延長
		verify(new Input(9*60,			 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60+30),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(7*60+30,			 // 所定内勤務時間
					0,			 	 // 遅刻時間
					0,			 // 早退時間
					1*60+30,		 // 休憩時間
					0,			 	 // 遅刻控除時間
					0,				 // 早退控除時間
					30,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 4-1 休憩  15x2 休憩追加
		verify(new Input(9*60,			 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(10*60,10*60+15), AttCalcRangesDto.R(12*60,13*60), AttCalcRangesDto.R(15*60,15*60+15)}),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(7*60+30,			 // 所定内勤務時間
					0,			 	 // 遅刻時間
					0,				 // 早退時間
					1*60+30,			 // 休憩時間
					0,			 	 // 遅刻控除時間
					0,				 // 早退控除時間
					30,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 4-2 休憩  15休憩短縮
		verify(new Input(9*60,			 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(new List<AttCalcRangeDto>{AttCalcRangesDto.R(12*60,12*60+45)}),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60+15,		 // 所定内勤務時間
					0,			 	 // 遅刻時間
					0,				 // 早退時間
					45,				 // 休憩時間
					0,			 	 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					15));			 // 過不足時間

		// 5 遅刻-早退 45分遅刻 90分早退
		verify(new Input(9*60+45,			 // 出社
				   16*60+30,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(5*60+45,		 // 所定内勤務時間
					45,				 // 遅刻時間
					1*60+30,			 // 早退時間
					60,				 // 休憩時間
					45,				 // 遅刻控除時間
					1*60+30,			 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 5-1 遅刻-早退 45分遅刻 90分早退 30分休憩短縮
		verify(new Input(9*60+45,			 // 出社
				   16*60+30,		 // 退社
				   AttCalcRangesDto.RS(12*60,12*60+30), // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(6*60+15,		 // 所定内勤務時間
					45,				 // 遅刻時間
					1*60+30,			 // 早退時間
					30,				 // 休憩時間
					15,				 // 遅刻控除時間
					1*60+30,			 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 5-2 遅刻-早退 45分遅刻 90分早退 60分休憩短縮
		verify(new Input(9*60+45,			 // 出社
				   16*60+30,		 // 退社
				   AttCalcRangesDto.RS(),			 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(6*60+45,		 // 所定内勤務時間
					45,				 // 遅刻時間
					1*60+30,		 // 早退時間
					0,				 // 休憩時間
					0,				 // 遅刻控除時間
					1*60+15,		 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 5-3 遅刻-早退 30分遅刻 15分早退 60分休憩短縮
		verify(new Input(9*60+30,			 // 出社
				   17*60+45,		 // 退社
				   AttCalcRangesDto.RS(),			 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60)), // 所定休憩
			new Output(8*60+15,		 // 所定内勤務時間
					30,				 // 遅刻時間
					15,		 		 // 早退時間
					0,				 // 休憩時間
					0,				 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					0,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					15));			 // 過不足時間
		// 6 遅刻申請 10:00 10:00出社
		verify(new Input(10*60,			 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
				   new Map<Option, Integer>{Option.permitedLateArriveTime =>10*60}),
			new Output(7*60,		 	 // 所定内勤務時間
					0,				 // 遅刻時間
					0,			 	 // 早退時間
					60,				 // 休憩時間
					0,				 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					60,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 6-1 遅刻申請 10:00 10:30出社
		verify(new Input(10*60+30,			 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
				   new Map<Option, Integer>{Option.permitedLateArriveTime =>10*60}),
			new Output(6*60+30,	 	 // 所定内勤務時間
					30,				 // 遅刻時間
					0,			 	 // 早退時間
					60,				 // 休憩時間
					30,				 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					60,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 6-2 遅刻申請 10:00 9:30出社
		verify(new Input(9*60+30,			 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,			 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
				   new Map<Option, Integer>{Option.permitedLateArriveTime => 10*60}),
			new Output(7*60+30,	 	 // 所定内勤務時間
					0,				 // 遅刻時間
					0,			 	 // 早退時間
					60,				 // 休憩時間
					0,				 // 遅刻控除時間
					0,				 // 早退控除時間
					0,				 // 休憩控除時間
					30,				 // 遅刻補填時間
					0,				 // 早退補填時間
					0,				 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));			 // 過不足時間

		// 6-3 遅刻申請 10:00 11:30出社 所定休憩 11:00-12:00
		verify(new Input(11*60+30,		 // 出社
				   18*60,			 // 退社
				   AttCalcRangesDto.RS(12*60,13*60),	 // 取得休憩
				   9*60,				 // 始業
				   18*60,			 // 終業
				   AttCalcRangesDto.RS(11*60,12*60),  // 所定休憩
				   new Map<Option, Integer>{Option.permitedLateArriveTime => 10*60}),
			new Output(5*60+30,	 	 // 所定内勤務時間
					90,				 // 遅刻時間
					0,	 				 // 早退時間
					60,				 // 休憩時間
					90,				 // 遅刻控除時間
					0,					 // 早退控除時間
					0,					 // 休憩控除時間
					60,				 // 遅刻補填時間
					0,					 // 早退補填時間
					0,					 // 休憩補填時間
					8*60,			 // 実効所定勤務時間
					0));				 // 過不足時間

	// 6-4 遅刻申請 11:30 13:30出社 所定休憩 11:00-12:00 休憩 14:00-14:45
	verify(new Input(13*60+30,		 // 出社
			   18*60,			 // 退社
			   AttCalcRangesDto.RS(14*60,14*60+45),	// 取得休憩
			   9*60,			 // 始業
			   18*60,			 // 終業
			   AttCalcRangesDto.RS(11*60,12*60),  // 所定休憩
			   new Map<Option, Integer>{Option.permitedLateArriveTime => 11*60+30}),
		new Output(3*60+45,	 	 // 所定内勤務時間
				2*60,			 // 遅刻時間
				0,			 	 // 早退時間
				45,				 // 休憩時間
				2*60,			 // 遅刻控除時間
				0,				 // 早退控除時間
				15,				 // 休憩控除時間
				2*60,			 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				8*60,			 // 実効所定勤務時間
				0));			 // 過不足時間

	// 7-1 始業終業 9:00-18:00 所定休憩 12:00-13:00 所定勤務時間 7:45 勤務時間 9:00-18:00 取得休憩 12:00-13:00
	verify(new Input(9*60,		 // 出社
			   18*60,			 // 退社
			   AttCalcRangesDto.RS(12*60,13*60),	// 取得休憩
			   9*60,			 // 始業
			   18*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime => 7*60+45, Option.ContractRestTime => 15}),
		new Output(8*60,		 	 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				60,				 // 休憩時間
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				0,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+45,			 // 実効所定勤務時間
				15));			 // 過不足時間

	// 7-2 始業終業 9:00-18:00 所定休憩 12:00-13:00 所定勤務時間 7:45 勤務時間 9:00-18:00 取得休憩 12:00-13:30
	verify(new Input(9*60,		 // 出社
			   18*60,			 // 退社
			   AttCalcRangesDto.RS(12*60,13*60+30),	// 取得休憩
			   9*60,			 // 始業
			   18*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime =>7*60+45,Option.ContractRestTime =>15}),
		new Output(7*60+30,		 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				90,				 // 休憩時間
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				15,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+45,			 // 実効所定勤務時間
				0));			 // 過不足時間

	// 7-3 始業終業 9:00-18:00 所定休憩 12:00-13:00 所定勤務時間 7:45 勤務時間 9:00-18:00 取得休憩 12:00-12:30 取得休憩時間 1:00
	verify(new Input(9*60,		 // 出社
			   18*60,			 // 退社
			   AttCalcRangesDto.RS(12*60,12*60+30),	// 取得休憩
			   9*60,			 // 始業
			   18*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime => 7*60+45, Option.ContractRestTime =>15, Option.InputRestTime =>60}),
		new Output(7*60+45,		 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				75,				 // 休憩時間 通常30分＋45分その他休憩
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				0,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+45,			 // 実効所定勤務時間
				0));			 // 過不足時間

	// 7-4 始業終業 9:00-18:00 所定休憩 12:00-13:00 所定勤務時間 7:45 勤務時間 9:00-18:00 取得休憩 なし 取得休憩時間 1:15
	verify(new Input(9*60,		 // 出社
			   18*60,			 // 退社
			   AttCalcRangesDto.RS(),	// 取得休憩
			   9*60,			 // 始業
			   18*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime => 7*60+45, Option.ContractRestTime =>15, Option.InputRestTime => 60+15}),
		new Output(7*60+45,		 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				60+15,			 // 休憩時間
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				0,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+45,			 // 実効所定勤務時間
				0));			 // 過不足時間

	// 8-1 始業終業 9:00-17:00 所定休憩 12:00-13:00 所定勤務時間 7:15 勤務時間 9:00-18:00 取得休憩 12:00-13:00
	verify(new Input(9*60,		 // 出社
			   17*60,			 // 退社
			   AttCalcRangesDto.RS(12*60,13*60),	// 取得休憩
			   9*60,			 // 始業
			   17*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime => 7*60+15}),
		new Output(7*60,		 	 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				60,				 // 休憩時間
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				0,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+15,			 // 実効所定勤務時間
				-15));			 // 過不足時間

	// 7-2 始業終業 9:00-17:00 所定休憩 12:00-13:00 所定勤務時間 7:15 勤務時間 9:00-17:00 取得休憩 12:00-12:30
	verify(new Input(9*60,		 // 出社
			   17*60,			 // 退社
			   AttCalcRangesDto.RS(12*60,12*60+30),	// 取得休憩
			   9*60,			 // 始業
			   17*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime =>7*60+15}),
		new Output(7*60+30,		 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				30,				 // 休憩時間
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				0,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+15,			 // 実効所定勤務時間
				15));			 // 過不足時間

	// 7-3 始業終業 9:00-17:00 所定休憩 12:00-13:00 所定勤務時間 7:45 勤務時間 9:00-17:00 取得休憩 なし 取得休憩時間 1:00
	verify(new Input(9*60,		 // 出社
			   17*60,			 // 退社
			   AttCalcRangesDto.RS(),			 // 取得休憩
			   9*60,			 // 始業
			   17*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime => 7*60+15, Option.InputRestTime => 45}),
		new Output(7*60+15,		 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				45,				 // 休憩時間
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				0,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+15,			 // 実効所定勤務時間
				0));			 // 過不足時間

	// 7-4 始業終業 9:00-18:00 所定休憩 12:00-13:00 所定勤務時間 7:45 勤務時間 9:00-18:00 取得休憩 なし 取得休憩時間 1:15
	verify(new Input(9*60,		 // 出社
			   17*60,			 // 退社
			   AttCalcRangesDto.RS(),			// 取得休憩
			   9*60,			 // 始業
			   17*60,			 // 終業
			   AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
			   new Map<Option, Integer>{Option.ContractWorkTime => 7*60+15, Option.InputRestTime => 60+15}),
		new Output(7*60,		 // 所定内勤務時間
				0,				 // 遅刻時間
				0,			 	 // 早退時間
				60,			 // 休憩時間
				0,				 // 遅刻控除時間
				0,				 // 早退控除時間
				0,				 // 休憩控除時間
				0,				 // 遅刻補填時間
				0,				 // 早退補填時間
				0,				 // 休憩補填時間
				7*60+15,			 // 実効所定勤務時間
				-15));			 // 過不足時間
	}

	// 時間単位休全日分取得後出社
	@isTest static void testFullDayTimeRest() {
		// 始業終業 9:00-18:00 全日時間単位無休休暇 所定休憩 12:00-13:00 所定勤務時間 9 勤務時間 19:00-20:00
		verify(new Input(
				19*60, // 出社
				20*60, // 退社
				AttCalcRangesDto.RS(9*60, 18*60),// 取得休憩
				9*60, // 始業
				18*60, // 終業
				AttCalcRangesDto.RS(12*60,13*60),  // 所定休憩
				new Map<Option, Integer>{
					Option.ContractWorkTime => 8*60}),
		new Output(0*60, // 所定内勤務時間
				8*60, // 遅刻時間
				0, // 早退時間
				0, // 休憩時間
				8*60, // 遅刻控除時間
				0, // 早退控除時間
				0, // 休憩控除時間
				0, // 遅刻補填時間
				0, // 早退補填時間
				0, // 休憩補填時間
				8*60, // 実効所定勤務時間
				0)); // 過不足時間
	}

	/**
	 * 有給の時間単位休を取得したときの計算をテストする
	 */
	@isTest static void testPaidLeaveRestRanges() {
		AttCalcLostTimeLogic logic = new AttCalcLostTimeLogic();
		AttCalcLostTimeLogic.Input input = new AttCalcLostTimeLogic.Input();
		input.inputStartTime = 9*60;
		input.inputEndTime = 18*60;
		input.inputRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.convertdRestRanges = AttCalcRangesDto.RS(14*60, 16*60);
		input.unpaidLeaveTime = 0;
		input.startTime = 9*60;
		input.endTime = 18*60;
		input.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.contractWorkTime = 8*60;

		AttCalcLostTimeLogic.Output output = new AttCalcLostTimeLogic.Output();

		logic.apply(output, input);

		System.assertEquals(6*60, output.realWorkTime);
		System.assertEquals(2*60, output.convertedWorkTime);
		System.assertEquals(0, output.unpaidLeaveLostTime);
		System.assertEquals(2*60, output.paidLeaveTime);
		System.assertEquals(0, output.unpaidLeaveTime);
		System.assertEquals(0, output.lateArriveTime);
		System.assertEquals(0, output.lateArriveLostTime);
		System.assertEquals(0, output.earlyLeaveTime);
		System.assertEquals(0, output.earlyLeaveLostTime);
	}
	/**
	 * 無給の時間単位休を取得したときの計算をテストする
	 */
	@isTest static void testUnpaidLeaveRestRanges() {
		AttCalcLostTimeLogic logic = new AttCalcLostTimeLogic();
		AttCalcLostTimeLogic.Input input = new AttCalcLostTimeLogic.Input();
		input.inputStartTime = 9*60;
		input.inputEndTime = 18*60;
		input.inputRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.unpaidLeaveRestRanges = AttCalcRangesDto.RS(14*60, 16*60);
		input.unpaidLeaveTime = 0;
		input.startTime = 9*60;
		input.endTime = 18*60;
		input.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.contractWorkTime = 8*60;

		AttCalcLostTimeLogic.Output output = new AttCalcLostTimeLogic.Output();

		logic.apply(output, input);

		System.assertEquals(6*60, output.realWorkTime);
		System.assertEquals(0, output.convertedWorkTime);
		System.assertEquals(2*60, output.unpaidLeaveLostTime);
		System.assertEquals(0, output.paidLeaveTime);
		System.assertEquals(2*60, output.unpaidLeaveTime);
		System.assertEquals(0, output.lateArriveTime);
		System.assertEquals(0, output.lateArriveLostTime);
		System.assertEquals(0, output.earlyLeaveTime);
		System.assertEquals(0, output.earlyLeaveLostTime);
	}

	/**
	 * 無給の時間単位休を取得し、かつ遅刻したときの計算をテストする
	 */
	@isTest static void testUnpaidLeaveRestRangesInLateArrival() {
		AttCalcLostTimeLogic logic = new AttCalcLostTimeLogic();
		AttCalcLostTimeLogic.Input input = new AttCalcLostTimeLogic.Input();
		input.inputStartTime = 11*60 + 30; // 2時間30分遅刻
		input.inputEndTime = 18*60;
		input.inputRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.inputRestTime = 0;
		input.permitedLostTimeRanges = new AttCalcRangesDto();
		input.convertdRestRanges = new AttCalcRangesDto();
		input.convertedWorkTime = 0;
		input.unpaidLeaveRestRanges = AttCalcRangesDto.RS(13*60, 15*60);
		input.unpaidLeaveTime = 0;
		input.startTime = 9*60;
		input.endTime = 18*60;
		input.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.contractRestTime = 0;
		input.contractWorkTime = 8*60;

		AttCalcLostTimeLogic.Output output = new AttCalcLostTimeLogic.Output();

		logic.apply(output, input);

		System.assertEquals(3*60+30, output.realWorkTime);
		System.assertEquals(0, output.convertedWorkTime);
		System.assertEquals(2*60, output.unpaidLeaveLostTime);
		System.assertEquals(0, output.paidLeaveTime);
		System.assertEquals(2*60, output.unpaidLeaveTime);
		System.assertEquals(2*60 + 30, output.lateArriveTime);
		System.assertEquals(2*60 + 30, output.lateArriveLostTime);
		System.assertEquals(0, output.earlyLeaveTime);
		System.assertEquals(0, output.earlyLeaveLostTime);
	}

	/**
	 * 出退勤時刻の外に無給の時間単位休を取得し、かつ遅刻したときの計算をテストする
	 */
	@isTest static void testUnpaidLeaveRestRangesOutOfInputTimeInLateArrival() {
		AttCalcLostTimeLogic logic = new AttCalcLostTimeLogic();
		AttCalcLostTimeLogic.Input input = new AttCalcLostTimeLogic.Input();
		input.inputStartTime = 11*60 + 30; // 無給休暇の後に30分遅刻
		input.inputEndTime = 18*60;
		input.inputRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.inputRestTime = 0;
		input.permitedLostTimeRanges = new AttCalcRangesDto();
		input.convertdRestRanges = new AttCalcRangesDto();
		input.convertedWorkTime = 0;
		input.unpaidLeaveRestRanges = AttCalcRangesDto.RS(9*60, 11*60);
		input.unpaidLeaveTime = 0;
		input.startTime = 9*60;
		input.endTime = 18*60;
		input.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.contractRestTime = 0;
		input.contractWorkTime = 8*60;

		AttCalcLostTimeLogic.Output output = new AttCalcLostTimeLogic.Output();

		logic.apply(output, input);

		System.assertEquals(5*60+30, output.realWorkTime);
		System.assertEquals(0, output.convertedWorkTime);
		System.assertEquals(2*60, output.unpaidLeaveLostTime);
		System.assertEquals(0, output.paidLeaveTime);
		System.assertEquals(2*60, output.unpaidLeaveTime);
		System.assertEquals(30, output.lateArriveTime);
		System.assertEquals(30, output.lateArriveLostTime);
		System.assertEquals(0, output.earlyLeaveTime);
		System.assertEquals(0, output.earlyLeaveLostTime);
	}

	/**
	 * 無給の時間単位休を取得し、かつ早退したときの計算をテストする
	 */
	@isTest static void testUnpaidLeaveRestRangesInEarlyLeave() {
		AttCalcLostTimeLogic logic = new AttCalcLostTimeLogic();
		AttCalcLostTimeLogic.Input input = new AttCalcLostTimeLogic.Input();
		input.inputStartTime = 9*60;
		input.inputEndTime = 15*60 + 30; // 2時間半早退
		input.inputRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.inputRestTime = 0;
		input.permitedLostTimeRanges = new AttCalcRangesDto();
		input.convertdRestRanges = new AttCalcRangesDto();
		input.convertedWorkTime = 0;
		input.unpaidLeaveRestRanges = AttCalcRangesDto.RS(13*60, 15*60);
		input.unpaidLeaveTime = 0;
		input.startTime = 9*60;
		input.endTime = 18*60;
		input.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.contractRestTime = 0;
		input.contractWorkTime = 8*60;

		AttCalcLostTimeLogic.Output output = new AttCalcLostTimeLogic.Output();

		logic.apply(output, input);

		System.assertEquals(3*60+30, output.realWorkTime);
		System.assertEquals(0, output.convertedWorkTime);
		System.assertEquals(2*60, output.unpaidLeaveLostTime);
		System.assertEquals(0, output.paidLeaveTime);
		System.assertEquals(2*60, output.unpaidLeaveTime);
		System.assertEquals(0, output.lateArriveTime);
		System.assertEquals(0, output.lateArriveLostTime);
		System.assertEquals(2*60 + 30, output.earlyLeaveTime);
		System.assertEquals(2*60 + 30, output.earlyLeaveLostTime);
	}

	/**
	 * 出退勤時刻の外に無給の時間単位休を取得し、かつ早退したときの計算をテストする
	 */
	@isTest static void testUnpaidLeaveRestRangesOutOfInputTimeInEarlyLeave() {
		AttCalcLostTimeLogic logic = new AttCalcLostTimeLogic();
		AttCalcLostTimeLogic.Input input = new AttCalcLostTimeLogic.Input();
		input.inputStartTime = 9*60;
		input.inputEndTime = 15*60 + 30; // 無給休暇の前に30分早退
		input.inputRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.inputRestTime = 0;
		input.permitedLostTimeRanges = new AttCalcRangesDto();
		input.convertdRestRanges = new AttCalcRangesDto();
		input.convertedWorkTime = 0;
		input.unpaidLeaveRestRanges = AttCalcRangesDto.RS(16*60, 18*60);
		input.unpaidLeaveTime = 0;
		input.startTime = 9*60;
		input.endTime = 18*60;
		input.contractRestRanges = AttCalcRangesDto.RS(12*60, 13*60);
		input.contractRestTime = 0;
		input.contractWorkTime = 8*60;

		AttCalcLostTimeLogic.Output output = new AttCalcLostTimeLogic.Output();

		logic.apply(output, input);

		System.assertEquals(5*60+30, output.realWorkTime);
		System.assertEquals(0, output.convertedWorkTime);
		System.assertEquals(2*60, output.unpaidLeaveLostTime);
		System.assertEquals(0, output.paidLeaveTime);
		System.assertEquals(2*60, output.unpaidLeaveTime);
		System.assertEquals(0, output.lateArriveTime);
		System.assertEquals(0, output.lateArriveLostTime);
		System.assertEquals(30, output.earlyLeaveTime);
		System.assertEquals(30, output.earlyLeaveLostTime);
	}

}