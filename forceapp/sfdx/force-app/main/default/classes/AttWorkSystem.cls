/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 労働時間制
 */
public with sharing class AttWorkSystem extends ValueObjectType {

	/** 固定労働時間制 */
	public static final AttWorkSystem JP_FIX = new AttWorkSystem('JP:Fix');
	/** フレックスタイム制 */
	public static final AttWorkSystem JP_FLEX = new AttWorkSystem('JP:Flex');
	/** 変形労働時間制 */
	public static final AttWorkSystem JP_MODIFIED = new AttWorkSystem('JP:Modified');
	/** 管理監督者 */
	public static final AttWorkSystem JP_MANAGER = new AttWorkSystem('JP:Manager');
	/** 裁量労働性 */
	public static final AttWorkSystem JP_DISCRETION = new AttWorkSystem('JP:Discretion');

	/** 労働時間制のリスト（値が追加されら本リストにも追加してください) */
	public static final List<AttWorkSystem> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttWorkSystem> {
			JP_FIX,
			JP_FLEX,
			JP_MODIFIED,
			JP_MANAGER,
			JP_DISCRETION
		};
	}


	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttWorkSystem(String value) {
		super(value);
	}

	/**
	 * 値からAttWorkSystemを取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttWorkSystem、存在しない場合はnull
	 */
	public static AttWorkSystem valueOf(String value) {
		AttWorkSystem retType = null;
		if (String.isNotBlank(value)) {
			for (AttWorkSystem type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
					break;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttWorkSystem以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttWorkSystem) {
			// 値が同じであればtrue
			if (this.value == ((AttWorkSystem)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}

}
