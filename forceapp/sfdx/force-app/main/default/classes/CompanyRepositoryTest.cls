/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * 会社のリポジトリのテスト
 */
@isTest
private class CompanyRepositoryTest {

	/**
	 * getEntityのテスト
	 */
	@isTest static void getEntityTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCurrency__c currencyObj = ComTestDataUtility.createCurrency('JPY', 'Japanese Yen');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		ComCalendar__c defaultCalendar = ComTestDataUtility.createCalendar('Comm', companyObj.Id);
		companyObj.Language__c = 'ja';
		companyObj.CalendarId__c = defaultCalendar.Id;
		companyObj.PlannerDefaultView__c = PlannerViewType.MONTHLY.value;
		companyObj.UseCalendarAccess__c = true;
		companyObj.CalendarAccessService__c = CalendarAccessServiceName.OFFICE_365.value;
		companyObj.UseCompanyTaxMaster__c = true;
		companyObj.CurrencyId__c = currencyObj.Id;
		companyObj.JorudanFareType__c = 0;
		companyObj.JorudanAreaPreference__c = 'ALL';
		companyObj.JorudanUseChargedExpress__c = 0;
		companyObj.JorudanChargedExpressDistance__c = 0;
		companyObj.JorudanSeatPreference__c = 0;
		companyObj.JorudanRouteSort__c = 0;
		companyObj.JorudanCommuterPass__c = 0;
		companyObj.JorudanHighwayBus__c = 0;

		update companyObj;

		CompanyRepository repo = new CompanyRepository();
		CompanyEntity entity = repo.getEntity(companyObj.Id);

		System.assertNotEquals(null, entity);
		System.assertEquals(companyObj.Id, entity.id);
		System.assertEquals(companyObj.Name, entity.name);
		System.assertEquals(companyObj.Code__c, entity.code);
		System.assertEquals(companyObj.Name_L0__c, entity.nameL0);
		System.assertEquals(companyObj.Name_L1__c, entity.nameL1);
		System.assertEquals(companyObj.Name_L2__c, entity.nameL2);
		System.assertEquals(companyObj.CountryId__c, entity.countryId);
		System.assertEquals(companyObj.CurrencyId__c, entity.currencyId);
		System.assertEquals(companyObj.Language__c, entity.language.value);
		System.assertEquals(companyObj.UseAttendance__c, entity.useAttendance);
		System.assertEquals(companyObj.UseExpense__c, entity.useExpense);
		System.assertEquals(companyObj.UseExpenseRequest__c, entity.useExpenseRequest);
		System.assertEquals(companyObj.UseWorkTime__c, entity.useWorkTime);
		System.assertEquals(companyObj.UsePlanner__c, entity.usePlanner);
		System.assertEquals(companyObj.PlannerDefaultView__c, entity.plannerDefaultView.value);
		System.assertEquals(defaultCalendar.Id, entity.calendarId);
		System.assertEquals(countryObj.Name_L0__c, entity.countryNameL.valueL0);
		System.assertEquals(countryObj.Name_L1__c, entity.countryNameL.valueL1);
		System.assertEquals(countryObj.Name_L2__c, entity.countryNameL.valueL2);
		System.assertEquals(currencyObj.Name_L0__c, entity.currencyInfo.nameL.valueL0);
		System.assertEquals(currencyObj.Name_L1__c, entity.currencyInfo.nameL.valueL1);
		System.assertEquals(currencyObj.Name_L2__c, entity.currencyInfo.nameL.valueL2);
		System.assertEquals(currencyObj.Code__c, entity.currencyInfo.code);
		System.assertEquals(currencyObj.Symbol__c, entity.currencyInfo.symbol);
		System.assertEquals(currencyObj.DecimalPlaces__c, entity.currencyInfo.decimalPlaces);
		System.assertEquals(companyObj.UseCalendarAccess__c, entity.useCalendarAccess);
		System.assertEquals(companyObj.CalendarAccessService__c, entity.calendarAccessService.value);
		System.assertEquals(companyObj.UseCompanyTaxMaster__c, entity.useCompanyTaxMaster);

		System.assertEquals(companyObj.JorudanFareType__c, entity.jorudanFareType);
		System.assertEquals(companyObj.JorudanAreaPreference__c, entity.jorudanAreaPreference);
		System.assertEquals(companyObj.JorudanUseChargedExpress__c, entity.jorudanUseChargedExpress);
		System.assertEquals(companyObj.JorudanChargedExpressDistance__c, entity.jorudanChargedExpressDistance);
		System.assertEquals(companyObj.JorudanSeatPreference__c, entity.jorudanSeatPreference);
		System.assertEquals(companyObj.JorudanRouteSort__c, entity.jorudanRouteSort);
		System.assertEquals(companyObj.JorudanCommuterPass__c, entity.jorudanCommuterPass);
		System.assertEquals(companyObj.JorudanHighwayBus__c, entity.jorudanHighwayBus);
	}

	/**
	 * searchEntityListのテスト
	 */
	@isTest static void searchEntityListTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		ComCalendar__c defaultCalendar = ComTestDataUtility.createCalendar('Comm', companyObj.Id);
		companyObj.Language__c = 'ja';
		companyObj.CalendarId__c = defaultCalendar.Id;
		update companyObj;

		CompanyRepository repo = new CompanyRepository();
		CompanyRepository.SearchFilter filter;
		List<CompanyEntity> resEntities;

		// 会社IDで検索
		filter = new CompanyRepository.SearchFilter();
		filter.companyIds = new List<Id>{companyObj.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(companyObj.Id, resEntities[0].Id);

		// コードで検索
		filter = new CompanyRepository.SearchFilter();
		filter.companyCodes = new List<String>{companyObj.Code__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(companyObj.Id, resEntities[0].Id);

		// カレンダーIDで検索
		filter = new CompanyRepository.SearchFilter();
		filter.calendarIds = new List<Id>{companyObj.CalendarId__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(companyObj.Id, resEntities[0].Id);

	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', null);

		// キャッシュをonでインスタンスを生成
		CompanyRepository repository = new CompanyRepository(true);

		// 検索（DBから取得）
		CompanyRepository.SearchFilter filter = new CompanyRepository.SearchFilter();
		filter.companyIds = new List<Id>{companyObj.Id};
		CompanyEntity company = repository.searchEntityList(filter)[0];

		// 取得対象のレコードを更新する
		companyObj.Code__c = 'NotCache';
		upsert companyObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		CompanyEntity cachedCompany = repository.searchEntityList(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(company.id, cachedCompany.id);
		System.assertNotEquals('NotCache', cachedCompany.code);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', null);

		// 検索（DBから取得）
		CompanyRepository.SearchFilter filter = new CompanyRepository.SearchFilter();
		filter.companyIds = new List<Id>{companyObj.Id};
		CompanyEntity company = new CompanyRepository(true).searchEntityList(filter)[0];

		// 取得対象のレコードを更新する
		companyObj.Code__c = 'NotCache';
		upsert companyObj;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		CompanyEntity cachedCompany = new CompanyRepository(true).searchEntityList(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(company.id, cachedCompany.id);
		System.assertNotEquals('NotCache', cachedCompany.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchBaseNotFromCacheDifferentCondition() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', null);

		// キャッシュをonでインスタンスを生成
		CompanyRepository repository = new CompanyRepository(true);

		// 検索（DBから取得）
		CompanyRepository.SearchFilter filter = new CompanyRepository.SearchFilter();
		filter.companyIds = new List<Id>{companyObj.Id};
		CompanyEntity company = repository.searchEntityList(filter)[0];

		// 取得対象のレコードを更新する
		companyObj.Code__c = 'NotCache';
		upsert companyObj;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.companyCodes = new List<String>{companyObj.Code__c};
		CompanyEntity searchedCompany = repository.searchEntityList(filter)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(company.id, searchedCompany.id);
		System.assertEquals('NotCache', searchedCompany.code);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', null);

		// キャッシュをonでインスタンスを生成
		CompanyRepository repository = new CompanyRepository(true);

		// 検索（DBから取得）
		CompanyRepository.SearchFilter filter = new CompanyRepository.SearchFilter();
		filter.companyIds = new List<Id>{companyObj.Id};
		CompanyEntity company = repository.searchEntityList(filter, false)[0];

		// 取得対象のレコードを更新する
		companyObj.Code__c = 'NotCache';
		upsert companyObj;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		CompanyEntity searchedCompany = repository.searchEntityList(filter, true)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(company.id, searchedCompany.id);
		System.assertEquals('NotCache', searchedCompany.code);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', null);

		// キャッシュをoffでインスタンスを生成
		CompanyRepository repository = new CompanyRepository();

		// 検索（DBから取得）
		CompanyRepository.SearchFilter filter = new CompanyRepository.SearchFilter();
		filter.companyIds = new List<Id>{companyObj.Id};
		CompanyEntity company = repository.searchEntityList(filter)[0];

		// 取得対象のレコードを更新する
		companyObj.Code__c = 'NotCache';
		upsert companyObj;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		CompanyEntity searchedCompany = repository.searchEntityList(filter)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(company.id, searchedCompany.id);
		System.assertEquals('NotCache', searchedCompany.code);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		CompanyRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new CompanyRepository().isEnabledCache);
	}
}
