/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 経費申請タイプのエンティティ Entity class for Expense Report Type
 */
public with sharing class ExpReportTypeEntity extends Entity {
	/** コードの最大文字数 Longest possible length for Code */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 経費申請タイプ名の最大文字数 Longest possible length for Expense Report Type Name */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 説明の最大文字数 Longest possible length for Description */
	public static final Integer DESCRIPTION_MAX_LENGTH = 1024;
	/** Number of Extended Items for each type */
	public static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/**
	 * Pattern to matches any of the following: ExtendedItem{XX}{YY}{ZZ}
	 * Where
	 *  XX: `Text`, `Picklist`, `Lookup` or `Date`
	 *  YY: 01 ~ 10
	 *  ZZ: `Id` or `Value`
	 **/
	private static final Pattern EXTENDED_ITEM_CONFIG_PATTERN = Pattern.compile('ExtendedItem((Text)|(Picklist)|(Lookup)|(Date)){1}[0-9]{2}((Id)|(UsedIn)|(RequiredFor)){1}');

	/** String templates to access the Extended Item fields - based on the Field enum names */
	private static final String EXTENDED_ITEM_ID_TEMPLATE = 'ExtendedItem{0}{1}Id';
	private static final String EXTENDED_ITEM_USED_IN_TEMPLATE = 'ExtendedItem{0}{1}UsedIn';
	private static final String EXTENDED_ITEM_REQUIRED_FOR_TEMPLATE = 'ExtendedItem{0}{1}RequiredFor';

	/**
	 * @description ルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目値 */
		public AppMultiString nameL;
		/** コンストラクタ */
		public LookupField(AppMultiString nameL) {
			this.nameL = nameL;
		}
	}

	/**
	 * 項目の定義
	 * 項目を追加したら、getFieldValue, setFieldValueメソッドへの追加もお願いします。
	 */
	public enum Field {
		ACTIVE,
		CODE,
		COMPANY_ID,
		COST_CENTER_USED_IN,
		COST_CENTER_REQUIRED_FOR,
		DESCRIPTION_L0,
		DESCRIPTION_L1,
		DESCRIPTION_L2,
		JOB_USED_IN,
		JOB_REQUIRED_FOR,
		NAME,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		UNIQ_KEY,
		USE_FILE_ATTACHMENT,
		VENDOR_USED_IN,
		VENDOR_REQUIRED_FOR
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	public enum ExtendedItemConfigField {
		EXTENDED_ITEM_DATE_ID_LIST,
		EXTENDED_ITEM_DATE_USED_IN_LIST,
		EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST,
		EXTENDED_ITEM_LOOKUP_ID_LIST,
		EXTENDED_ITEM_LOOKUP_USED_IN_LIST,
		EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST,
		EXTENDED_ITEM_PICKLIST_ID_LIST,
		EXTENDED_ITEM_PICKLIST_USED_IN_LIST,
		EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST,
		EXTENDED_ITEM_TEXT_ID_LIST,
		EXTENDED_ITEM_TEXT_USED_IN_LIST,
		EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, ExtendedItemConfigField> EXTENDED_ITEM_CONFIG_FIELD_MAP;
	static {
		final Map<String, ExtendedItemConfigField> fieldMap = new Map<String, ExtendedItemConfigField>();
		for (ExtendedItemConfigField f : ExtendedItemConfigField.values()) {
			fieldMap.put(f.name(), f);
		}
		EXTENDED_ITEM_CONFIG_FIELD_MAP = fieldMap;
	}

	/** Store the corresponding Accessor(Index and Extended Item Type) for the given string */
	private static final Map<String, ExtendedItemAccessor> FIELD_NAME_TO_ACCESSOR_MAP;
	static {
		FIELD_NAME_TO_ACCESSOR_MAP = new Map<String, ExtendedItemAccessor>();
		List<String> extendedItemTypeList = new List<String> {'Text', 'Picklist', 'Lookup', 'Date'};
		for (String extendedItemType : extendedItemTypeList) {
			for (Integer index = 0; index < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; index++) {
				String indexStr = String.valueOf(index + 1).leftPad(2, '0');
				FIELD_NAME_TO_ACCESSOR_MAP.put(
					String.format(EXTENDED_ITEM_ID_TEMPLATE, new List<String>{extendedItemType, indexStr}),
					new ExtendedItemAccessor(EXTENDED_ITEM_CONFIG_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'IdList'), index)
				);

				FIELD_NAME_TO_ACCESSOR_MAP.put(
					String.format(EXTENDED_ITEM_USED_IN_TEMPLATE, new List<String>{extendedItemType, indexStr}),
					new ExtendedItemAccessor(EXTENDED_ITEM_CONFIG_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'UsedInList'), index)
				);

				FIELD_NAME_TO_ACCESSOR_MAP.put(
					String.format(EXTENDED_ITEM_REQUIRED_FOR_TEMPLATE, new List<String>{extendedItemType, indexStr}),
					new ExtendedItemAccessor(EXTENDED_ITEM_CONFIG_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'RequiredForList'), index)
				);
			}
		}
	}

	/*
	 * Wrapper class for holding the Index and the field {@code ExtendedItemConfigField}
	 */
	private class ExtendedItemAccessor {
		ExtendedItemConfigField field {private set; get;}
		Integer index {private set; get;}

		public ExtendedItemAccessor(ExtendedItemConfigField field, Integer index) {
			this.field = field;
			this.index = index;
		}
	}

	/** Set of Extended Item Config Fields which has been changed */
	private Set<ExtendedItemConfigField> isChangedExtendedItemConfigFieldSet;


	public ExpReportTypeEntity() {
		super();
		isChangedFieldSet = new Set<ExpReportTypeEntity.Field>();

		isChangedExtendedItemConfigFieldSet = new Set<ExtendedItemConfigField>();
		this.extendedItemLookupIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemDateIdList = new Id[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemLookupUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemDateUsedInList = new ComExtendedItemUsedIn[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemLookupRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemDateRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemLookupInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemDateInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
	}

	/** 値を変更した項目のリスト */
	protected Set<ExpReportTypeEntity.Field> isChangedFieldSet;

	/** 有効 */
	public boolean active {
		get;
		set {
			active = value;
			setChanged(Field.ACTIVE);
		}
	}

	/** 経費申請タイプコード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}

	/** 会社ID */
	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	/** Cost Center Used In */
	public ComCostCenterUsedIn costCenterUsedIn {
		get;
		set {
			costCenterUsedIn = value;
			setChanged(Field.COST_CENTER_USED_IN);
		}
	}

	/** Cost Center Required For */
	public ComCostCenterRequiredFor costCenterRequiredFor {
		get;
		set {
			costCenterRequiredFor = value;
			setChanged(Field.COST_CENTER_REQUIRED_FOR);
		}
	}

	/** 説明(L0) */
	public String descriptionL0 {
		get;
		set {
			descriptionL0 = value;
			setChanged(Field.DESCRIPTION_L0);
		}
	}

	/** 説明(L1) */
	public String descriptionL1 {
		get;
		set {
			descriptionL1 = value;
			setChanged(Field.DESCRIPTION_L1);
		}
	}

	/** 説明(L2) */
	public String descriptionL2 {
		get;
		set {
			descriptionL2 = value;
			setChanged(Field.DESCRIPTION_L2);
		}
	}

	/** 説明(翻訳) */
	public AppMultiString descriptionL {
		get {return new AppMultiString(this.descriptionL0, this.descriptionL1, this.descriptionL2);}
	}

	/** Job Used In */
	public ComJobUsedIn jobUsedIn {
		get;
		set {
			jobUsedIn = value;
			setChanged(Field.JOB_USED_IN);
		}
	}

	/** Job Required For */
	public ComJobRequiredFor jobRequiredFor {
		get;
		set {
			jobRequiredFor = value;
			setChanged(Field.JOB_REQUIRED_FOR);
		}
	}

	/** Use File Attachment */
	public boolean useFileAttachment {
		get;
		set {
			useFileAttachment = value;
			setChanged(Field.USE_FILE_ATTACHMENT);
		}
	}

	/** Vendor Used In */
	public ExpVendorUsedIn vendorUsedIn {
		get;
		set {
			vendorUsedIn = value;
			setChanged(Field.VENDOR_USED_IN);
		}
	}

	/** Vendor Required For */
	public ExpVendorRequiredFor vendorRequiredFor {
		get;
		set {
			vendorRequiredFor = value;
			setChanged(Field.VENDOR_REQUIRED_FOR);
		}
	}

	/** レコード名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** 経費申請タイプ名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	/** 経費申請タイプ名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	/** 経費申請タイプ名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/** 経費申請タイプ名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
	}

	/** ユニークキー */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}

	/** Date Extended Item ID */
	private List<Id> extendedItemDateIdList;
	public void setExtendedItemDateId(Integer index, Id idValue) {
		this.extendedItemDateIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST);
	}
	public Id getExtendedItemDateId(Integer index) {
		return this.extendedItemDateIdList[index];
	}

	/** Date Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemDateUsedInList;
	public void setExtendedItemDateUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemDateUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemDateUsedIn(Integer index) {
		return this.extendedItemDateUsedInList[index];
	}

	/** Date Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemDateRequiredForList;
	public void setExtendedItemDateRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemDateRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemDateRequiredFor(Integer index) {
		return this.extendedItemDateRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemDateInfoList;
	public void setExtendedItemDateInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemDateInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemDateInfo(Integer index) {
		return this.extendedItemDateInfoList[index];
	}

	/** Lookup Extended Item ID */
	private List<Id> extendedItemLookupIdList;
	public void setExtendedItemLookupId(Integer index, Id idValue) {
		this.extendedItemLookupIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST);
	}
	public Id getExtendedItemLookupId(Integer index) {
		return this.extendedItemLookupIdList[index];
	}

	/** Lookup Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemLookupUsedInList;
	public void setExtendedItemLookupUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemLookupUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemLookupUsedIn(Integer index) {
		return this.extendedItemLookupUsedInList[index];
	}

	/** Lookup Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemLookupRequiredForList;
	public void setExtendedItemLookupRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemLookupRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemLookupRequiredFor(Integer index) {
		return this.extendedItemLookupRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemLookupInfoList;
	public void setExtendedItemLookupInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemLookupInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemLookupInfo(Integer index) {
		return this.extendedItemLookupInfoList[index];
	}

	/** Picklist Extended Item ID */
	private List<Id> extendedItemPicklistIdList;
	public void setExtendedItemPicklistId(Integer index, Id idValue) {
		this.extendedItemPicklistIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST);
	}
	public Id getExtendedItemPicklistId(Integer index) {
		return this.extendedItemPicklistIdList[index];
	}

	/** Picklist Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemPicklistUsedInList;
	public void setExtendedItemPicklistUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemPicklistUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemPicklistUsedIn(Integer index) {
		return this.extendedItemPicklistUsedInList[index];
	}

	/** Picklist Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemPicklistRequiredForList;
	public void setExtendedItemPicklistRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemPicklistRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemPicklistRequiredFor(Integer index) {
		return this.extendedItemPicklistRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemPicklistInfoList;
	public void setExtendedItemPicklistInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemPicklistInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemPicklistInfo(Integer index) {
		return this.extendedItemPicklistInfoList[index];
	}

	/** Text Extended Item ID */
	private List<Id> extendedItemTextIdList;
	public void setExtendedItemTextId(Integer index, Id idValue) {
		this.extendedItemTextIdList[index] = idValue;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST);
	}
	public Id getExtendedItemTextId(Integer index) {
		return this.extendedItemTextIdList[index];
	}

	/** Text Extended Item UsedIn */
	private List<ComExtendedItemUsedIn> extendedItemTextUsedInList;
	public void setExtendedItemTextUsedIn(Integer index, ComExtendedItemUsedIn value) {
		this.extendedItemTextUsedInList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST);
	}
	public ComExtendedItemUsedIn getExtendedItemTextUsedIn(Integer index) {
		return this.extendedItemTextUsedInList[index];
	}

	/** Text Extended Item RequiredFor */
	private List<ComExtendedItemRequiredFor> extendedItemTextRequiredForList;
	public void setExtendedItemTextRequiredFor(Integer index, ComExtendedItemRequiredFor value) {
		this.extendedItemTextRequiredForList[index] = value;
		setChanged(ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST);
	}
	public ComExtendedItemRequiredFor getExtendedItemTextRequiredFor(Integer index) {
		return this.extendedItemTextRequiredForList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemTextInfoList;
	public void setExtendedItemTextInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemTextInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemTextInfo(Integer index) {
		return this.extendedItemTextInfoList[index];
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/** Mark the specified field as changed */
	private void setChanged(ExtendedItemConfigField field) {
		this.isChangedExtendedItemConfigFieldSet.add(field);
	}

	/*
	 * Returns true is the specified field value has been changed.
	 * @param field Field to check if it has been changed
	 *
	 * @return true if the field has been changed. Otherwise, false.
	 */
	public Boolean isChanged(ExtendedItemConfigField field) {
		return isChangedExtendedItemConfigFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
		this.isChangedExtendedItemConfigFieldSet.clear();
	}

	/**
	 * ユニークキーを作成する
	 * インスタンス変数のcodeを指定しておくこと。
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]経費申請タイプのユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]経費申請タイプのユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + コード
		return companyCode + '-' + this.code;
	}

	/**
	 * エンティティの複製を作成する(IDはコピーされません)
	 */
	public ExpReportTypeEntity copy() {
		ExpReportTypeEntity copyEntity = new ExpReportTypeEntity();
		for (ExpReportTypeEntity.Field field : ExpReportTypeEntity.Field.values()) {
			copyEntity.setFieldValue(field, this.getFieldValue(field));
		}

		// Set Extended Items
		for (ExpReportTypeEntity.ExtendedItemConfigField field: ExpReportTypeEntity.ExtendedItemConfigField.values()) {
			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				copyEntity.setExtendedItemValue(field, i, this.getExtendedItemValue(field, i));
			}
		}

		return copyEntity;
	}

	/*
	 * To allows child classes to determine if a field is a extended Item field
	 * @param fieldName the representative field name in String (e.g. extendedItemText01Id)
	 *
	 * @return true if the specified string is a extended item field. Otherwise, false.
	 */
	private Boolean isExtendedItemConfigField(String fieldName) {
		return EXTENDED_ITEM_CONFIG_PATTERN.matcher(fieldName).matches();
	}

	/*
	 * Returns extended item value using the field name in string format
	 * @param fieldName the representative field name in String (e.g. extendedItemText01Id)
	 *
	 * @return the value set to the field. If the field name is not a valid Extended Item field, {@code null} will be returned.
	 */
	private Object getExtendedItemValue(String fieldName) {
		ExtendedItemAccessor accessor = FIELD_NAME_TO_ACCESSOR_MAP.get(fieldName);
		if (accessor != null) {
			return getExtendedItemValue(accessor.field, accessor.index);
		}
		return null;
	}

	/**
	 * 指定された項目の値を取得する
	 * @param targetField 対象の項目
	 * @return 項目値
	 */
	public Object getFieldValue(ExpReportTypeEntity.Field targetField) {
		if (targetField == Field.ACTIVE) {
			return this.active;
		}
		if (targetField == Field.CODE) {
			return this.code;
		}
		if (targetField == Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == Field.COST_CENTER_REQUIRED_FOR) {
			return this.costCenterRequiredFor;
		}
		if (targetField == Field.COST_CENTER_USED_IN) {
			return this.costCenterUsedIn;
		}
		if (targetField == Field.DESCRIPTION_L0) {
			return this.descriptionL0;
		}
		if (targetField == Field.DESCRIPTION_L1) {
			return this.descriptionL1;
		}
		if (targetField == Field.DESCRIPTION_L2) {
			return this.descriptionL2;
		}
		if (targetField == Field.JOB_REQUIRED_FOR) {
			return this.jobRequiredFor;
		}
		if (targetField == Field.JOB_USED_IN) {
			return this.jobUsedIn;
		}
		if (targetField == Field.USE_FILE_ATTACHMENT) {
			return this.useFileAttachment;
		}
		if (targetField == Field.VENDOR_REQUIRED_FOR) {
			return this.vendorRequiredFor;
		}
		if (targetField == Field.VENDOR_USED_IN) {
			return this.vendorUsedIn;
		}
		if (targetField == Field.NAME) {
			return this.name;
		}
		if (targetField == Field.NAME_L0) {
			return this.nameL0;
		}
		if (targetField == Field.NAME_L1) {
			return this.nameL1;
		}
		if (targetField == Field.NAME_L2) {
			return this.nameL2;
		}
		if (targetField == Field.UNIQ_KEY) {
			return this.uniqKey;
		}

		// ここにきたらバグ
		throw new App.UnsupportedException('ExpReportTypeEntity.getFieldValue: 未対応の項目です。targetField=' + targetField);
	}

	/*
	 * Returns Extended Item value for the given {@code ExtendedItemConfigField} and and index.
	 *
	 * @param field Extended Item Field (Text Id, Text Value, Picklist Id, Picklist Value, etc)
	 * @param index the extended item index (starts from 0)
	 *
	 * @return the value set to the field. If the field name is not a valid Extended Item field, {@code null} will be returned.
	 */
	public Object getExtendedItemValue(ExtendedItemConfigField field, Integer index) {
		if (field == ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST) {
			return this.extendedItemDateIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST) {
			return this.extendedItemDateUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST) {
			return this.extendedItemDateRequiredForList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST) {
			return this.extendedItemLookupIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST) {
			return this.extendedItemLookupUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST) {
			return this.extendedItemLookupRequiredForList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST) {
			return this.extendedItemPicklistIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST) {
			return this.extendedItemPicklistUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST) {
			return this.extendedItemPicklistRequiredForList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST) {
			return this.extendedItemTextIdList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST) {
			return this.extendedItemTextUsedInList[index];
		} else if (field == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST) {
			return this.extendedItemTextRequiredForList[index];
		}
		return null;
	}

	/**
	 * 指定された項目に値を設定する
	 * @param targetField 対象の項目
	 * @pram value 設定値
	 */
	public void setFieldValue(ExpReportTypeEntity.Field targetField, Object value) {
		if (targetField == Field.ACTIVE) {
			this.active = (boolean)value;
		} else if (targetField == Field.CODE) {
			this.code = (String)value;
		} else if (targetField == Field.COMPANY_ID) {
			this.companyId = (Id)value;
		} else if (targetField == Field.COST_CENTER_REQUIRED_FOR) {
			this.costCenterRequiredFor = (ComCostCenterRequiredFor)value;
		} else if (targetField == Field.COST_CENTER_USED_IN) {
			this.costCenterUsedIn = (ComCostCenterUsedIn)value;
		} else if (targetField == Field.DESCRIPTION_L0) {
			this.descriptionL0 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L1) {
			this.descriptionL1 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L2) {
			this.descriptionL2 = (String)value;
		} else if (targetField == Field.JOB_REQUIRED_FOR) {
			this.jobRequiredFor = (ComJobRequiredFor)value;
		} else if (targetField == Field.JOB_USED_IN) {
			this.jobUsedIn = (ComJobUsedIn)value;
		} else if (targetField == Field.USE_FILE_ATTACHMENT) {
			this.useFileAttachment = (boolean)value;
		} else if (targetField == Field.VENDOR_REQUIRED_FOR) {
			this.vendorRequiredFor = (ExpVendorRequiredFor)value;
		} else if (targetField == Field.VENDOR_USED_IN) {
			this.vendorUsedIn = (ExpVendorUsedIn)value;
		} else if (targetField == Field.NAME) {
			this.name = (String)value;
		} else if (targetField == Field.NAME_L0) {
			this.nameL0 = (String)value;
		} else if (targetField == Field.NAME_L1) {
			this.nameL1 = (String)value;
		} else if (targetField == Field.NAME_L2) {
			this.nameL2 = (String)value;
		} else if (targetField == Field.UNIQ_KEY) {
			this.uniqKey = (String)value;
		} else {
			// ここにきたらバグ
			throw new App.UnsupportedException('ExpReportTypeEntity.setFieldValue: 未対応の項目です。targetField=' + targetField);
		}
	}

	/*
	 * Set value to specific Extended Item
	 *
	 * @param targetField Indicate which extended item field
	 * @param index index of extended item, starting from 0
	 * @param value value to set to the specified Extended Item
	 */
	public void setExtendedItemValue(ExtendedItemConfigField targetField, Integer index, Object value) {
		if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST) {
			this.setExtendedItemDateId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST) {
			this.setExtendedItemDateUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST) {
			this.setExtendedItemDateRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST) {
			this.setExtendedItemLookupId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST) {
			this.setExtendedItemLookupUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST) {
			this.setExtendedItemLookupRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST) {
			this.setExtendedItemPicklistId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST) {
			this.setExtendedItemPicklistUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST) {
			this.setExtendedItemPicklistRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST) {
			this.setExtendedItemTextId(index, (Id) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST) {
			this.setExtendedItemTextUsedIn(index, (ComExtendedItemUsedIn) value);
		} else if (targetField == ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST) {
			this.setExtendedItemTextRequiredFor(index, (ComExtendedItemRequiredFor) value);
		} else {
			throw new App.UnsupportedException('ExpReportTypeEntity.setExtendedItemValue: 未対応の項目です。targetField=' + targetField);
		}
	}
}