/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * 工数サマリーのエンティティ
 */
public with sharing class TimeSummaryEntity extends TimeSummaryGeneratedEntity {

	/**
	 * コンストラクタ
	 */
	public TimeSummaryEntity() {
		super(new TimeSummary__c());
	}

	/**
	 * コンストラクタ
	 */
	public TimeSummaryEntity(TimeSummary__c sobj) {
		super(sobj);
	}

	/** ステータス（参照のみ。値を変更してもDBに保存されない。） */
	public AppRequestStatus status {
		get {
			if (this.requestId == null) {
				return null;
			}
			if (status == null) {
				status = AppRequestStatus.valueOf(sObj.RequestId__r.Status__c);
			}
			return status;
		}
		private set;
	}

	/** 要確認フラグ（参照のみ。値を変更してもDBに保存されない。） */
	public Boolean isConfirmationRequired {
		get {
			if (this.requestId == null) {
				return false;
			}
			if (isConfirmationRequired == null) {
				isConfirmationRequired = sObj.RequestId__r.ConfirmationRequired__c;
			}
			return isConfirmationRequired;
		}
		private set;
	}

	/** 取消種別（参照のみ。値を変更してもDBに保存されない。） */
	public AppCancelType cancelType {
		get {
			if (this.requestId == null) {
				return null;
			}
			if (cancelType == null) {
				cancelType = AppCancelType.valueOf(sObj.RequestId__r.CancelType__c);
			}
			return cancelType;
		}
		private set;
	}

	/** 社員ベースID（参照のみ。値を変更してもDBに保存されない。） */
	public Id employeeBaseId {
		get {
			if (this.employeeId == null) {
				return null;
			}
			if (employeeBaseId == null) {
				employeeBaseId = sObj.EmployeeHistoryId__r.BaseId__c;
			}
			return employeeBaseId;
		}
		private set;
	}

	/** 社員名（参照のみ。値を変更してもDBに保存されない。） */
	public String employeeName {get {return employeeNameL.getFullName();}}
	public AppPersonName employeeNameL {
		get {
			if (this.employeeId == null) {
				return null;
			}
			if (employeeNameL == null) {
				employeeNameL = new AppPersonName(
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c,
						sObj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c,
						sObj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c,
						sObj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c);
			}
			return employeeNameL;
		}
		private set;
	}

	/** 顔写真URL（参照のみ。値を変更してもDBに保存されない。） */
	public String employeePhotoUrl {
		get {
			if (this.employeeId == null) {
				return null;
			}
			if (employeePhotoUrl == null) {
				employeePhotoUrl = sObj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl;
			}
			return employeePhotoUrl;
		}
		private set;
	}

	/** 部署ベースID（参照のみ。値を変更してもDBに保存されない。） */
	public Id departmentBaseId {
		get {
			if (this.employeeId == null) {
				return null;
			}
			if (departmentBaseId == null) {
				departmentBaseId = sObj.EmployeeHistoryId__r.DepartmentBaseId__c;
			}
			return departmentBaseId;
		}
		private set;
	}

	/**
	 * 工数明細リスト
	 * 明細が存在しない場合は空のリストを返す
	 */
	public List<TimeRecordEntity> recordList {
		get {
			if (recordList == null) {
				recordList = new List<TimeRecordEntity>();
			}
			return recordList;
		}
		private set;
	}

	/**
	 * 工数明細リストをリプレースする（リストをコピーする）
	 * @param recordList リプレース対象の工数明細リスト
	 */
	public void replaceRecordList(List<TimeRecordEntity> recordList) {
		this.recordList = new List<TimeRecordEntity>(recordList);
	}

	/**
	 * 作業時間
	 * Time
	 * 値がnullの場合にデフォルト値を返すために、GeneratedEntityから除外しています。
	 */
	public static final Schema.SObjectField FIELD_WORK_TIME = TimeSummary__c.Time__c;
	public Integer workTime {
		get {
			Integer value = Integer.valueOf(getFieldValue(FIELD_WORK_TIME));
			return value == null ? 0 : value;
		}
		private set;
	}

	/**
	 * ユニークキーを作成する
	 * @param employee 社員ベース
	 * @param yearMonthly 工数サマリー年月度
	 * @param subNo サブNo
	 * @return ユニークキー
	 */
	public static String createUniqKey(EmployeeBaseEntity employee, String yearMonthly, Integer subNo) {
		if (employee == null) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]工数サマリーのユニークキーが作成できませんでした。社員が未指定です。');
		}
		if (employee.id == null) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]工数サマリーのユニークキーが作成できませんでした。社員IDが空です。');
		}
		if (yearMonthly == null) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]工数サマリーのユニークキーが作成できませんでした。対象日が空です。');
		}

		// 社員ID - 工数サマリー年月度 - subNo
		return employee.id + yearMonthly + subNo;
	}

	/**
	 * 工数設定・対象日から算出した工数サマリー年月度を返します。
	 * 例：
	 * 	工数設定＝集計期間：月・起算日：5日・月度の表記：開始日
	 * 	対象日＝2019/05/01
	 * 	=> 201905
	 * @param summaryRange 工数サマリー集計期間
	 * @param setting 工数設定
	 * @return 工数設定・対象日から算出した工数サマリー年月度
	 */
	public static String getYearMonthly(AppDateRange summaryRange, TimeSettingBaseEntity setting) {
		Integer yearMonthly;

		// 工数設定情報から
		if (setting.monthMark == TimeSettingBaseGeneratedEntity.MonthMarkType.BeginBase) {
			yearMonthly = summaryRange.startDate.year() * 100 + summaryRange.startDate.month();
		} else if (setting.monthMark == TimeSettingBaseGeneratedEntity.MonthMarkType.EndBase) {
			yearMonthly = summaryRange.endDate.year() * 100 + summaryRange.endDate.month();
		}
		return String.valueOf(yearMonthly);
	}

	/**
	 * 工数設定・対象日から算出したsubNoを返します。
	 * サマリー期間が月の場合は固定で１を返します。
	 * FIXME: 改定日でサマリを分割したり、確定実績と比較して調整調整する対応は未実装です。
	 * 例：
	 * 	工数設定＝集計期間：週・起算曜日：水曜日・月度の表記：開始日
	 * 	対象期間＝2019/05/01 - 2019/05/07
	 * 	=> 1
	 * @param summaryRange 工数サマリー集計期間
	 * @param setting 工数設定
	 * @return 工数設定・対象日から算出した工数サマリー年月度
	 */
	public static Integer getSubNo(AppDateRange summaryRange, TimeSettingBaseEntity setting) {
		// サマリー期間が月の場合は固定で１を返す
		if (setting.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Month) {
			return 1;
		}
		// 工数設定情報から表記の年月度を取得する
		AppDate calcBaseStartDate;
		if (setting.monthMark == TimeSettingBaseGeneratedEntity.MonthMarkType.BeginBase) {
			calcBaseStartDate = summaryRange.startDate;
		} else if (setting.monthMark == TimeSettingBaseGeneratedEntity.MonthMarkType.EndBase) {
			calcBaseStartDate = summaryRange.endDate;
		}
		// 1日までの距離/7+1を算出する
		// 例：表記の年月度が2019/07/07の場合
		//    (7 - 1) / 7 + 1 = 1
		Integer weekNum = AppDate.newInstance(calcBaseStartDate.year(), calcBaseStartDate.month(), 1).daysBetween(
				calcBaseStartDate) / 7 + 1;
		return weekNum;
	}

	/**
	 * 工数設定・対象日から算出した工数サマリー集計期間を返します。
	 * 例：
	 * 	工数設定＝集計期間：月・起算日：5日・月度の表記：開始日
	 * 	対象日＝2019/05/10
	 * 	=> 2019/05/05 - 2019/06/04
	 * FIXME:集計期間＝月以外未実装
	 * @param targetDate 対象日
	 * @param setting 工数設定
	 * @return 工数サマリー集計期間
	 */
	public static AppDateRange getSummaryRange(AppDate targetDate, TimeSettingBaseEntity setting) {
		AppDateRange summaryRange;
		AppDate summaryRangeStartDate;
		AppDate summaryRangeEndDate;
		if (setting.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Month) {
			// 工数設定情報から月度の開始日・終了日を算出
			Integer year = targetDate.year();
			Integer month = targetDate.month();
			Integer day = targetDate.day();
			Integer startDateOfMonth = setting.startDateOfMonth;

			if (day >= startDateOfMonth) {
				summaryRangeStartDate = AppDate.newInstance(year, month, startDateOfMonth);
			} else {
				summaryRangeStartDate = AppDate.newInstance(year, month, startDateOfMonth).addMonths(-1);
			}
			summaryRangeEndDate = summaryRangeStartDate.addMonths(1).addDays(-1);
		} else if (setting.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Week) {
			// 工数設定情報から週度の開始日・終了日を算出
			// 設定の起算曜日を数値(日曜=0, 月曜=1...)に変換する
			Integer settingStartDayOfWeek = setting.convertAppDateDayOfWeek();
			// 対象日の曜日を数値に変換する
			Integer targetDayOfWeek = targetDate.dayOfWeekByInt();
			Integer settingStartDayDiff = settingStartDayOfWeek - targetDayOfWeek;
			summaryRangeStartDate = targetDate.addDays(settingStartDayDiff);
			// 対象日がサマリー開始日前の場合は次のサマリー期間を計算しているので前の週に戻す
			if (targetDate.isBefore(summaryRangeStartDate)) {
				summaryRangeStartDate = summaryRangeStartDate.minusDays(7);
			}
			// 終了日を算出
			summaryRangeEndDate = summaryRangeStartDate.addDays(6);
		}

		summaryRange = new AppDateRange(summaryRangeStartDate, summaryRangeEndDate);
		return summaryRange;
	}

	/**
	 * 工数サマリー名を作成する
	 * @param employeeCode 社員コード
	 * @param employeeName 社員名
	 * @param startDate 月度開始日
	 * @param endDate 月度終了日
	 * @return 工数サマリー名
	 */
	public static String createName(String employeeCode, String employeeName, AppDate startDate, AppDate endDate) {
		final Integer empLength = 63; // 社員情報の文字列長さ

		if (String.isBlank(employeeCode)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeCode is blank');
		}
		if (String.isBlank(employeeName)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeName is blank');
		}
		if (startDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('startDate is null');
		}
		if (endDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('endDate is null');
		}

		// 工数サマリー名作成
		return (employeeCode + employeeName).left(empLength) +
				startDate.formatYYYYMMDD() +
				'-' +
				endDate.formatYYYYMMDD();
	}
}