/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Entity class for Custom Extended Item
 * 
 * @group Expense
 */
public class ExtendedItemCustomEntity extends Entity {
	public static final Integer CODE_MAX_LENGTH = 20;
	public static final Integer NAME_MAX_LENGTH = 80;

	public ExtendedItemCustomEntity() {
		this.changedFieldSet = new Set<Field>();
		this.extendedItemCustomOptionList = new List<ExtendedItemCustomOptionEntity>();
	}

	public enum Field {
		CODE,
		COMPANY_ID,
		NAME,
		NAME_L0,
		NAME_L1,
		NAME_L2,
		UNIQ_KEY,
		CUSTOM_OPTION_LIST
	}

	public String code {
		get;
		set {
			code = value;
			setChanged(Field.Code);
		}
	}

	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}

	public AppMultiString nameL {
		get {return new AppMultiString(this.nameL0, this.nameL1, this.nameL2);}
		private set;
	}

	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}

	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}

	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}

	public List<ExtendedItemCustomOptionEntity> extendedItemCustomOptionList {
		get; private set;
	}

	/*
	 * Replace the extendedItemCustomOptionList of the entity with the provided customOptionList.
	 *
	 * @param customOptionList List of {@code ExtendedItemCustomOptionEntity} avaiable for this entity.
	 */
	public void replaceExtenedItemCustomOptionList(List<ExtendedItemCustomOptionEntity> customOptionList) {
		this.extendedItemCustomOptionList = new List<ExtendedItemCustomOptionEntity>(customOptionList);
		setChanged(Field.CUSTOM_OPTION_LIST);
	}

	private Set<Field> changedFieldSet;

	private void setChanged(Field field) {
		this.changedFieldSet.add(field);
	}

	/*
	 * Check if a field has been modified.
	 *
	 * @param field: The Field to check if it has been modified or not.
	 *
	 * @returns Boolean {@code True} if the specified field has been change. {@code False} otherwise.
	 */
	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	/*
	 * Reset the field modified flags.
	 * Also reset the ExtendedItemCustomOptionEntity linked with this entity.
	 */
	public void resetChanged() {
		this.changedFieldSet.clear();

		if (this.extendedItemCustomOptionList != null) {
			for (ExtendedItemCustomOptionEntity customOption : this.extendedItemCustomOptionList) {
				customOption.resetChanged();
			}
		}
	}

	/*
	 * Create Uniqe Key for the Extended Item Custom Object.
	 * Unique Key will be created as follow: {@code companyCode + '-' + code}
	 *
	 * COMMENTED OUT AS CURRENTLY THERE IS NO SUPPORT TO CREATE VIA API OR CODE
	 */
	/*public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			throw new App.ParameterException('Company Code is empty.');
		}
		if (String.isBlank(this.code)) {
			throw new App.IllegalStateException('Custom Extended Item Code is empty.');
		}
		return companyCode + '-' + this.code;
	}*/
}