/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠日次申請のエンティティ
 */
public with sharing class AttDailyRequestEntity extends AttDailyRequestGeneratedEntity {
	/** 備考の最大文字数 */
	private static final Integer REMARKS_MAX_LENGTH = 255;
	/** 理由の最大文字数 */
	public static final Integer REASON_MAX_LENGTH = 255;
	/** コメントの最大文字数 */
	private static final Integer COMMENT_MAX_LENGTH = 1000;
	/** 休暇申請の申請可能な最大期間(日数) */
	public static final Integer LEAVE_REQUEST_MAX_PERIOD = 31;

	/** excludingDateの区切り文字 */
	private static final String EXCLUDINGDATE_SEPARATOR = ',';

	/** 1時間あたりの分 */
	private static final Integer MINUTE_PER_HOUR = 60;

	/** 承認内容変更申請元ステータス（参照専用） */
	public AppRequestStatus originalRequestStatus {
		get {
			return AppRequestStatus.valueOf(sobj.OriginalRequestId__r.Status__c);
		}
		private set;
	}
	@testVisible
	private void setOriginalRequestStatus(AppRequestStatus status) {
		if (sobj.OriginalRequestId__r == null) {
			sobj.OriginalRequestId__r = new AttDailyRequest__c();
		}
		sobj.OriginalRequestId__r.Status__c = AppConverter.stringValue(status);
	}
	/** 承認内容変更申請元 取消種別（参照専用） */
	public AppCancelType originalRequestCancelType {
		get {
			return AppCancelType.valueOf(sobj.OriginalRequestId__r.CancelType__c);
		}
		set;
	}

	/** 休暇コード(参照専用) */
	public String leaveCode {
		get {
			return sobj.AttLeaveId__r.Code__c;
		}
		private set;
	}
	/** 休暇名(参照専用) */
	public AppMultiString leaveNameL {
		get {
			if (leaveNameL != null) {
				return leaveNameL;
			}
			return new AppMultiString(sobj.AttLeaveId__r.Name_L0__c, sobj.AttLeaveId__r.Name_L1__c, sobj.AttLeaveId__r.Name_L2__c);
		}
		private set;
	}

	/** 休暇の理由を求める(参照専用) */
	public Boolean requireReason {
		get {
			return sobj.AttLeaveId__r.RequireReason__c;
		}
		private set;
	}

	/** 除外日 */
	public List<AppDate> excludingDateList {
		get {
			if (excludingDateList == null) {
				excludingDateList = convertExcludingDateToList(sobj.ExcludingDate__c);
			}
			return excludingDateList;
		}
		set {
			excludingDateList = value;
		}
	}

	/**
	 * excludingDateListをDB保存用の文字列に変換する(YYYYMMDDのカンマ区切り)
	 */
	private String convertExcludingDateListForSObject(List<AppDate> dateList) {
		if (dateList == null) {
			return null;
		}

		List<String> strDateList = new List<String>();
		for (AppDate dt : dateList) {
			strDateList.add(dt.formatYYYYMMDD());
		}
		return String.join(strDateList, EXCLUDINGDATE_SEPARATOR);
	}

	/**
	 * excludingDate__cの値を日付リストに変換する
	 */
	public static List<AppDate> convertExcludingDateToList(String excludingDate) {
		if (excludingDate == null) {
			return null;
		}
		List<String> strDateList = excludingDate.split(EXCLUDINGDATE_SEPARATOR);
		List<AppDate> dateList = new List<AppDate>();
		for (String strDate : strDateList) {
			// TODO AppDateのメソッドに(YYYYMMDD->AppDate)
			Integer year = Integer.valueOf(strDate.subString(0, 4));
			Integer month = Integer.valueOf(strDate.subString(4, 6));
			Integer day = Integer.valueOf(strDate.subString(6, 8));
			dateList.add(AppDate.newInstance(year, month, day));
		}

		return dateList;
	}

	/** 部署の情報(参照専用) */
	public AttLookup.Department department {
		get {
			if (department == null) {
				department = new AttLookup.Department(
					new AppMultiString(
						sobj.DepartmentHistoryId__r.Name_L0__c,
						sobj.DepartmentHistoryId__r.Name_L1__c,
						sobj.DepartmentHistoryId__r.Name_L2__c),
					AppDate.valueOf(sobj.DepartmentHistoryId__r.ValidFrom__c),
					AppDate.valueOf(sobj.DepartmentHistoryId__r.ValidTo__c),
					sobj.DepartmentHistoryId__r.Removed__c);
			}
			return department;
		}
		set;
	}
	/** 社員のベースID(参照専用) */
	public Id employeeBaseId {
		get {
			return sobj.EmployeeHistoryId__r.BaseId__c;
		}
		private set;
	}
	@TestVisible
	private void setEmployeeBaseId(Id employeeBaseId) {
		if (sobj.EmployeeHistoryId__r == null) {
			sobj.EmployeeHistoryId__r = new ComEmpHistory__c();
		}
		sobj.EmployeeHistoryId__r.BaseId__c = employeeBaseId;
	}
	/** 社員の情報(参照専用) */
	public AttLookup.Employee employee {
		get {
			if (employee == null) {
				employee = new AttLookup.Employee(
						new AppPersonName(
						sobj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c,
						sobj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
						sobj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c,
						sobj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
						sobj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c,
						sobj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c),
						sobj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
						AppDate.valueOf(sobj.EmployeeHistoryId__r.ValidFrom__c),
						AppDate.valueOf(sobj.EmployeeHistoryId__r.ValidTo__c),
						sobj.EmployeeHistoryId__r.Removed__c);
			}
			return employee;
		}
		set;
	}

	/** 申請者の情報(参照専用) */
	public AttLookup.Employee actor {
		get {
			if (actor == null) {
				actor = new AttLookup.Employee(
						new AppPersonName(
							sobj.ActorHistoryId__r.BaseId__r.FirstName_L0__c,
							sobj.ActorHistoryId__r.BaseId__r.LastName_L0__c,
							sobj.ActorHistoryId__r.BaseId__r.FirstName_L1__c,
							sobj.ActorHistoryId__r.BaseId__r.LastName_L1__c,
							sobj.ActorHistoryId__r.BaseId__r.FirstName_L2__c,
							sobj.ActorHistoryId__r.BaseId__r.LastName_L2__c),
						sobj.EmployeeHistoryId__r.BaseId__r.UserId__r.SmallPhotoUrl,
						AppDate.valueOf(sobj.ActorHistoryId__r.ValidFrom__c),
						AppDate.valueOf(sobj.ActorHistoryId__r.ValidTo__c),
						sobj.ActorHistoryId__r.Removed__c);
			}
			return actor;
		}
		set;
	}

	/** ユーザにレコードに対する編集アクセス権があるかどうか(参照専用) */
	public Boolean hasEditAccess {
		get {
			return sobj.UserRecordAccess.HasEditAccess;
		}
		private set;
	}

	/**
	 * コンストラクタ
	 */
	public AttDailyRequestEntity() {
		super(new AttDailyRequest__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttDailyRequestEntity(AttDailyRequest__c sobj) {
		super(sobj);
	}

	/**
	 * 申請ステータス詳細
	 */
	public enum DetailStatus {
		APPROVAL_IN,	// 承認待ち
		APPROVED,		// 承認済み
		REJECTED,		// 却下
		REMOVED,		// 申請取り消し
		CANCELED,		// 承認取消
		REAPPLYING,		// 変更承認待ち
		REAPPLIED		// 変更承認済み
	}


	/**
	 * 申請ステータスの詳細を取得する
	 * @return ステータス。ただし、statusが未設定の場合はnullを返却する
	 */
	public DetailStatus getDetailStatus() {
		return getDetailStatus(this.status, this.cancelType);
	}

	/**
	 * 承認内容変更申請元申請のステータスの詳細を取得する
	 * @return ステータス。ただし、statusが未設定の場合はnullを返却する
	 */
	public DetailStatus getOriginalDetailStatus() {
		return getDetailStatus(this.originalRequestStatus, this.originalRequestCancelType);
	}

	/**
	 * 申請ステータスの詳細を取得する
	 * @param status ステータス
	 * @param cancelType 取消種別
	 * @return ステータス。ただし、statusが未設定の場合はnullを返却する
	 */
	private DetailStatus getDetailStatus(AppRequestStatus status, AppCancelType cancelType) {
		// statusがnullの場合はnullを返却
		if (status == null) {
			return null;
		}
		// 承認待ち
		if (status == AppRequestStatus.PENDING) {
			return AttDailyRequestEntity.DetailStatus.APPROVAL_IN;
		}
		// 承認済み
		if (status == AppRequestStatus.APPROVED) {
			return AttDailyRequestEntity.DetailStatus.APPROVED;
		}
		// 変更承認待ち
		if (status == AppRequestStatus.REAPPLYING) {
			return AttDailyRequestEntity.DetailStatus.REAPPLYING;
		}
		// 無効の場合は取消種別でステータスが決まる
		if (status == AppRequestStatus.DISABLED) {
			// 却下
			if (cancelType == AppCancelType.REJECTED) {
				return AttDailyRequestEntity.DetailStatus.REJECTED;
			}
			// 申請取り消し
			if (cancelType == AppCancelType.REMOVED) {
				return AttDailyRequestEntity.DetailStatus.REMOVED;
			}
			// 承認取消
			if (cancelType == AppCancelType.CANCELED) {
				return AttDailyRequestEntity.DetailStatus.CANCELED;
			}
			// 変更承認済み
			if (cancelType == AppCancelType.REAPPLIED) {
				return AttDailyRequestEntity.DetailStatus.REAPPLIED;
			}
			// 本来ありえないはず。直接オブジェクトを操作した場合や不具合により発生
			// メッセージ：申請のステータスが取得できませんでした。取消種別の値が正しくありません。
			throw new App.IllegalStateException(
					ComMessage.msg().Att_Err_GetStatus(new List<String>{
						ComMessage.msg().Att_Lbl_DailyRequest})
					+ ComMessage.msg().Com_Err_InvalidParameter(new List<String>{
							ComMessage.msg().Att_Lbl_CancelType, AppConverter.stringValue(cancelType)}));
		}

		// プログラムバグのため多言語化しない
		throw new App.IllegalStateException('想定外のステータスです。[' + status + ']');
	}

	/**
	 * 代理申請か判定する
	 * @return 代理申請の場合true
	 */
	public Boolean isDelegatedRequest() {
		return employeeId != actorId;
	}

	/**
	 * この申請の承認内容変更が可能か判定する。
	 * @return 承認内容変更が可能な場合true
	 */
	public Boolean canReapply() {
		return status == AppRequestStatus.APPROVED;
	}

	/**
	 * 申請取消を出来る状態か判定する。
	 * @return 申請取消可能な場合true
	 */
	public Boolean canCancelRequest() {
		return status == AppRequestStatus.PENDING;
	}

	/**
	 * 承認取消を出来る状態か判定する。
	 * @return 承認取消可能な場合true
	 */
	public Boolean canCancelApproval() {
		return status == AppRequestStatus.APPROVED;
	}

	/**
	 * 申請を削除出来る状態か判定する。
	 * @return 削除可能な場合true
	 */
	public Boolean canRemove() {
		return status == AppRequestStatus.DISABLED &&
				// 変更承認済みは削除不可
				(cancelType == AppCancelType.REJECTED || cancelType == AppCancelType.REMOVED || cancelType == AppCancelType.CANCELED) &&
				isConfirmationRequired;
	}

	/**
	 * 分を時間に切り上げた休暇合計時間(leaveTotalTime)を取得する
	 * 　例:leaveTotalTimeが150(2:30)の場合、180(3:00)が返却される
	 * @return 切り上げた値(単位は分)、ただしleaveTotalTimeがnullの場合はnullが返却される
	 */
	public Integer getRoundedUpLeaveTotalTime() {
		if (this.leaveTotalTime == null) {
			return null;
		}
		Integer minutes = Math.mod(this.leaveTotalTime, MINUTE_PER_HOUR);
		Integer roundUpValue;
		if (minutes == 0) {
			// 切り上げ処理不要
			roundUpValue = this.leaveTotalTime;
		} else {
			// 切り上げる
			roundUpValue = this.leaveTotalTime + (MINUTE_PER_HOUR - minutes);
		}

		return roundUpValue;
	}

	/**
	 * 更新対象のSObjectを生成する。
	 * OutWorkTimeDetails__cは親クラスで定義していないので、更新時にJson文字列へ変換する。
	 */
	public override AttDailyRequest__c createSObject() {
		AttDailyRequest__c sobj = super.createSObject();
		sobj.ExcludingDate__c = convertExcludingDateListForSObject(excludingDateList);
		return sobj;
	}
}