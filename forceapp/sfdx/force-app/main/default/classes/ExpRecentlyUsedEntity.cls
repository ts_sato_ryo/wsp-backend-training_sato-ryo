/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Entity to represent Recently Used Extended Item Lookup Values (ExtendedItemCustomOption__c.Code__c)
 *
 * @group Expense
 */
public with sharing class ExpRecentlyUsedEntity extends Entity {

	public enum Field {
		NAME,
		EMPLOYEE_BASE_ID,
		MASTER_TYPE,
		TARGET_ID,
		RECENTLY_USED_VALUES
	}

	public ExpRecentlyUsedEntity() {
		this.changedFieldSet = new Set<Field>();
		this.recentlyUsedValues = new List<String>();
	}

	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}

	public Id employeeBaseId {
		get;
		set {
			employeeBaseId = value;
			setChanged(Field.EMPLOYEE_BASE_ID);
		}
	}

	/** Defines which sObject the record entry is representing */
	public MasterType masterType {
		get;
		set {
			masterType = value;
			setChanged(Field.MASTER_TYPE);
		}
	}

	public String targetId {
		get;
		set {
			targetId = value;
			setChanged(Field.TARGET_ID);
		}
	}

	/** Recently Used values serialized and stored, but represented as an List */
	private List<String> recentlyUsedValues;
	public String getRecentlyUsedValue(Integer index) {
		return this.recentlyUsedValues[index];
	}
	/** Returns how many recently used Items are there */
	public Integer getRecentlyUsedValuesSize() {
		return this.recentlyUsedValues.size();
	}
	/** Replace the whole List of Recently used values */
	public void replaceRecentlyUsedValues(List<String> recordList) {
		this.recentlyUsedValues = new List<String>(recordList);
		setChanged(Field.RECENTLY_USED_VALUES);
	}
	/*
	 * Serialize Recently Used Values for storing in DB
	 * @returns String serialized representation of of the recently used value list
	 */
	public String getSerializedRecentlyUsedValue() {
		return JSON.serialize(this.recentlyUsedValues);
	}
	/** Returns a clone of of Recently Used Value list */
	public List<String> getRecentlyUsedValues() {
		return new List<String>(this.recentlyUsedValues);
	}
	/*
	 * Deserialize the provided serialized string and initialize recently Used Values list
	 * @param serializedValue String representation of the recently used value list serialized
	 */
	public void setSerializedRecentlyUsedValues(String serializedValue) {
		this.recentlyUsedValues = (List<String>) JSON.deserialize(serializedValue, List<String>.class);
		this.setChanged(Field.RECENTLY_USED_VALUES);
	}

	private transient Set<Field> changedFieldSet;

	private void setChanged(Field field) {
		this.changedFieldSet.add(field);
	}

	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	public void resetChanged() {
		this.changedFieldSet.clear();
	}

	public enum MasterType {
		CostCenter,
		Job,
		ExpenseType,
		ExtendedItemLookup,
		Vendor
	}

	/** Map of MasterType with Name as the Key and the Field enum as the value */
	public static final Map<String, MasterType> MASTER_TYPE_MAP;
	static {
		final Map<String, MasterType> masterTypeMap = new Map<String, MasterType>();
		for (MasterType f : ExpRecentlyUsedEntity.MasterType.values()) {
			masterTypeMap.put(f.name(), f);
		}
		MASTER_TYPE_MAP = masterTypeMap;
	}
}