/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description AttPeriodStatusTypeのテスト
 */
@isTest
private class AttPeriodStatusTypeTest {

	/**
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(AttPeriodStatusType.ABSENCE, AttPeriodStatusType.valueOf('Absence'));
		System.assertEquals(AttPeriodStatusType.SHORT_TIME_WORK_SETTING, AttPeriodStatusType.valueOf('ShortTimeWorkSetting'));

		// 等しくない場合(値が異なる) → false
		System.assertEquals(false, AttPeriodStatusType.ABSENCE.equals(AttPeriodStatusType.SHORT_TIME_WORK_SETTING));

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, AttPeriodStatusType.ABSENCE.equals(Integer.valueOf(123)));

		// 等しくない場合(null)
		System.assertEquals(false, AttPeriodStatusType.SHORT_TIME_WORK_SETTING.equals(null));
	}
}
