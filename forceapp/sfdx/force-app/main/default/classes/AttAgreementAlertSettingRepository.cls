/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 36協定アラート設定マスタのリポジトリ
 */
public with sharing class AttAgreementAlertSettingRepository extends ValidPeriodRepository {

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}

	/** キャッシュの利用有無 */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** キャッシュ */
	private static final Repository.Cache CACHE = new Repository.Cache();

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** 会社のリレーション名 */
	private static final String COMPANY_R = AttAgreementAlertSetting__c.CompanyId__c.getDescribe().getRelationshipName();

	/**
	 * 取得対象の項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				AttAgreementAlertSetting__c.Id,
				AttAgreementAlertSetting__c.Name,
				AttAgreementAlertSetting__c.ValidFrom__c,
				AttAgreementAlertSetting__c.ValidTo__c,
				AttAgreementAlertSetting__c.Code__c,
				AttAgreementAlertSetting__c.UniqKey__c,
				AttAgreementAlertSetting__c.CompanyId__c,
				AttAgreementAlertSetting__c.Name_L0__c,
				AttAgreementAlertSetting__c.Name_L1__c,
				AttAgreementAlertSetting__c.Name_L2__c,
				AttAgreementAlertSetting__c.MonthlyAgreementHourWarning1__c,
				AttAgreementAlertSetting__c.MonthlyAgreementHourWarning2__c,
				AttAgreementAlertSetting__c.MonthlyAgreementHourLimit__c,
				AttAgreementAlertSetting__c.MonthlyAgreementHourWarningSpecial1__c,
				AttAgreementAlertSetting__c.MonthlyAgreementHourWarningSpecial2__c,
				AttAgreementAlertSetting__c.MonthlyAgreementHourLimitSpecial__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/**
	 * コンストラクタ
	 * キャッシュを利用しないインスタンスを生成する
	 */
	public AttAgreementAlertSettingRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public AttAgreementAlertSettingRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/**
	 * 指定したIDのエンティティを取得する
	 * @param id 取得対象のID
	 * @return 指定したIDのエンティティ、ただし存在しない場合はnull
	 */
	public AttAgreementAlertSettingEntity getEntity(Id id) {
		List<AttAgreementAlertSettingEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全件が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public List<AttAgreementAlertSettingEntity> getEntityList(List<Id> ids) {
		AttAgreementAlertSettingRepository.SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したエンティティを取得する
	 * @param codes 取得対象のコード
	 * @return 指定したコードのエンティティのリスト。ただし、存在しない場合はnull
	 */
	public List<AttAgreementAlertSettingEntity> getEntityListByCode(List<String> codes) {
		AttAgreementAlertSettingRepository.SearchFilter filter = new SearchFilter();
		filter.codes = codes == null ? null : new Set<String>(codes);

		return searchEntity(filter, NOT_FOR_UPDATE);
	}


	/** 検索フィルタ */
	public class SearchFilter {
		/** 36協定アラート設定ID */
		public Set<Id> ids;
		/** 会社ID */
		public Set<Id> companyIds;
		/** コード(完全一致) */
		public Set<String> codes;
		/** 取得対象日 */
		public AppDate targetDate;
	}

	/**
	 * 36協定アラート設定を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果
	 */
	public List<AttAgreementAlertSettingEntity> searchEntity(
			AttAgreementAlertSettingRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE);
	}

	/**
	 * 36協定アラート設定を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果
	 */
	public List<AttAgreementAlertSettingEntity> searchEntity(
			AttAgreementAlertSettingRepository.SearchFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchEntity ReturnCache key:' + cacheKey);
			List<AttAgreementAlertSettingEntity> entities = new List<AttAgreementAlertSettingEntity>();
			for (AttAgreementAlertSetting__c sObj : (List<AttAgreementAlertSetting__c>)CACHE.get(cacheKey)) {
				entities.add(new AttAgreementAlertSettingEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchEntity NoCache key:' + cacheKey);

		// WHERE句
		List<String> whereList = new List<String>();
		// 36協定アラート設定IDで検索
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(AttAgreementAlertSetting__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(AttAgreementAlertSetting__c.Code__c) + ' IN :pCodes');
		}
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(AttAgreementAlertSetting__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(AttAgreementAlertSetting__c.ValidTo__c) + ' > :pTargetDate');
		}

		String whereString = buildWhereString(whereList);


		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 関連項目（会社）
		selectFieldList.add(getFieldName(COMPANY_R, ComCompany__c.Code__c));

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + AttAgreementAlertSetting__c.SObjectType.getDescribe().getName()
				+ whereString;
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(AttAgreementAlertSetting__c.Code__c) + ' Asc';
			soql += ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		}

		System.debug('AttAgreementAlertSettingRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttAgreementAlertSetting__c> sObjs = (List<AttAgreementAlertSetting__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttAgreementAlertSetting__c.SObjectType);

		// SObjectからエンティティを作成
		List<AttAgreementAlertSettingEntity> entities = new List<AttAgreementAlertSettingEntity>();
		for (AttAgreementAlertSetting__c sObj : sObjs) {
			entities.add(new AttAgreementAlertSettingEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			CACHE.put(cacheKey, sObjs);
		}
		return entities;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntity> entityList) {
		List<AttAgreementAlertSetting__c> objectList = new List<AttAgreementAlertSetting__c>();
		for (ValidPeriodEntity entity : entityList) {
			objectList.add(((AttAgreementAlertSettingEntity)entity).createSObject());
		}
		return objectList;
	}
}
