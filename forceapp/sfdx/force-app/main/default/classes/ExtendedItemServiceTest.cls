/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ExtendedItemServiceのテスト
 */
@isTest
private class ExtendedItemServiceTest {

	/**
	 * 本クラス内の全てのテストメソッドで利用するテストデータを登録する
	 */
	@testSetup static void setup() {
		// マスタデータを登録しておく
		TestData.setupMaster();
	}

	/**
	 * テストデータクラス
	 */
	private class ServiceTestData extends TestData.TestDataEntity {
		/** 費目データ */
		public List<ExpTypeEntity> expTypes = new List<ExpTypeEntity>();
		/** 経費申請タイプ */
		public List<ExpReportTypeEntity> expReportTypes = new List<ExpReportTypeEntity>();

		/**
		 * コンストラクタ
		 */
		public ServiceTestData() {
			super();
			// 費目データ作成
			ComTestDataUtility.createExpTypes('Exp Type', this.company.id, 3);
			this.expTypes = (new ExpTypeRepository()).getEntityList(null);
			
			// 経費申請タイプ作成
			ComTestDataUtility.createExpReportTypes('TRT', this.company.id, 3);
			this.expReportTypes = (new ExpReportTypeRepository()).getEntityList(null);
		}
	}

	/**
	 * saveExtendedItemテスト
	 * 新規の拡張項目が登録できることを確認する
	 */
	@isTest static void saveExtendedItemTestNew() {
		ServiceTestData testData = new ServiceTestData();

		ExtendedItemEntity item = new ExtendedItemEntity();
		item.name = 'Test';
		item.nameL0 = 'Test_L0';
		item.nameL1 = 'Test_L1';
		item.nameL2 = 'Test_L2';
		item.code = 'Test001';
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.TEXT;
		item.limitLength = 255;
		item.defaultValueText = 'Default Value';
		item.descriptionL0 = 'Description_L0';
		item.descriptionL1 = 'Description_L1';
		item.descriptionL2 = 'Description_L2';

		// 保存する
		Id savedId = new ExtendedItemService().saveExtendedItem(item);

		// 保存できていることを確認する
		ExtendedItemEntity savedItem = new ExtendedItemRepository().getEntity(savedId);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(item.nameL0, savedItem.nameL0);
		System.assertEquals(item.nameL1, savedItem.nameL1);
		System.assertEquals(item.nameL2, savedItem.nameL2);
		System.assertEquals(item.code, savedItem.code);
		System.assertEquals(item.companyId, savedItem.companyId);
		System.assertEquals(item.inputType, savedItem.inputType);
		System.assertEquals(item.limitLength, savedItem.limitLength);
		System.assertEquals(item.defaultValueText, savedItem.defaultValueText);
		System.assertEquals(item.descriptionL0, savedItem.descriptionL0);
		System.assertEquals(item.descriptionL1, savedItem.descriptionL1);
		System.assertEquals(item.descriptionL2, savedItem.descriptionL2);
		// ユニークキーが設定されている
		System.assertEquals(testData.company.code + '-' + item.code, savedItem.uniqKey);
	}

	/**
	 * saveExtendedItemテスト
	 * 既存の拡張項目が更新できることを確認する
	 */
	@isTest static void saveExtendedItemTestUpdate() {
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		item.name = item.name + '_Update';
		item.nameL0 = item.nameL0 + '_Update';
		item.nameL1 = item.nameL1 + '_Update';
		item.nameL2 = item.nameL2 + '_Update';
		item.code = item.code + '_Update';

		// 保存する
		Id savedId = new ExtendedItemService().saveExtendedItem(item);

		// 保存できていることを確認する
		ExtendedItemEntity savedItem = new extendedItemRepository().getEntity(savedId);
		TestUtil.assertNotNull(savedItem);
		System.assertEquals(item.nameL0, savedItem.nameL0);
		System.assertEquals(item.nameL1, savedItem.nameL1);
		System.assertEquals(item.nameL2, savedItem.nameL2);
		System.assertEquals(item.code, savedItem.code);
		// ユニークキーが設定されている
		System.assertEquals(testData.company.code + '-' + item.code, savedItem.uniqKey);
	}

	/**
	 * saveExtendedItemテスト
	 * コードが重複している場合に想定されている例外が発生することを確認する
	 */
	@isTest static void saveExtendedItemTestDuplicateCode() {
		ServiceTestData testData = new ServiceTestData();

		ExtendedItemEntity item1 = new ExtendedItemEntity();
		item1.name = 'Test1';
		item1.nameL0 = 'Test1_L0';
		item1.nameL1 = 'Test1_L1';
		item1.nameL2 = 'Test1_L2';
		item1.code = 'Test001';
		item1.companyId = testData.company.id;
		item1.inputType = ExtendedItemInputType.TEXT;
		new ExtendedItemService().saveExtendedItem(item1);

		// 既存の拡張項目と同じコードの拡張項目を作成
		ExtendedItemEntity item2 = new ExtendedItemEntity();
		item2.name = 'Test2';
		item2.nameL0 = 'Test2_L0';
		item2.nameL1 = 'Test2_L1';
		item2.nameL2 = 'Test2_L2';
		item2.code = 'Test001';
		item2.companyId = testData.company.id;
		item2.inputType = ExtendedItemInputType.TEXT;

		// 保存する
		App.ParameterException resEx;
		try {
			Id savedId = new ExtendedItemService().saveExtendedItem(item2);
		} catch(App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, resEx.getMessage());
	}

	/**
	 * saveExtendedItemテスト
	 * 拡張項目の情報が正しく設定されていない場合、想定されている例外が発生することを確認する
	 */
	@isTest static void saveExtendedItemTestInvalid() {
		ServiceTestData testData = new ServiceTestData();

		// コードを空文字にしておく
		ExtendedItemEntity item = new ExtendedItemEntity();
		item.name = 'Test';
		item.nameL0 = 'Test_L0';
		item.nameL1 = 'Test_L1';
		item.nameL2 = 'Test_L2';
		item.code = '';
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.TEXT;

		// 保存する
		App.ParameterException resEx;
		try {
			Id savedId = new ExtendedItemService().saveExtendedItem(item);
		} catch(App.ParameterException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		String exMessage = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code});
		System.assertEquals(exMessage, resEx.getMessage());
	}

	/**
	 * saveExtendedItemテスト
	 * 費目に紐づけられている拡張項目の場合、入力タイプが変更できないことを確認する
	 */
	@isTest static void saveExtendedItemTestInputType() {
		ServiceTestData testData = new ServiceTestData();

		// 拡張項目を新規作成
		ExtendedItemEntity item = new ExtendedItemEntity();
		item.name = 'Test';
		item.nameL0 = 'Test_L0';
		item.nameL1 = 'Test_L1';
		item.nameL2 = 'Test_L2';
		item.code = 'Test001';
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.TEXT;

		ExtendedItemService service = new ExtendedItemService();
		Id itemId = service.saveExtendedItem(item);

		// 費目に紐づけ
		testData.expTypes[0].setExtendedItemTextId(0, itemId);
		new ExpTypeRepository().saveEntity(testData.expTypes[0]);

		// 入力タイプを更新
		item.setId(itemId);
		item.inputType = ExtendedItemInputType.PICKLIST;

		// 保存する
		App.IllegalStateException resEx;
		try {
			service.saveExtendedItem(item);
		} catch(App.IllegalStateException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		String exMessage = ComMessage.msg().Com_Err_CannotChangeReference;
		System.assertEquals(exMessage, resEx.getMessage());
	}

	/**
	 * saveExtendedItemテスト
	 * 経費内訳に紐づけられている拡張項目の場合、入力タイプが変更できないことを確認する
	 */
	@isTest static void updateExpTypeTestRecordType() {
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');
		// update code of ExtendedItemEntity to prevent duplicate code during initialisation of Extended Item test data
		item.setId(item.id);
		item.code = 'ExtendedItemText1';
		new ExtendedItemService().saveExtendedItem(item);

		ExpType__c existingExpType = [SELECT Id, ExtendedItemText01TextId__c From ExpType__c WHERE Id = :testData.expTypes[0].id];
		existingExpType.ExtendedItemText01TextId__c = item.id;
		UPDATE existingExpType;

		// 経費明細レコードを作成 Create expense record
		ExpRecord__c expRecord = new ExpRecord__c(
			Date__c = System.today(),
			Amount__c = 1000,
			Order__c = 1,
			RecordType__c = testData.expTypes[0].recordType.value);
		insert expRecord;

		// 経費内訳レコードを作成 Create expense record item
		ExpRecordItem__c recordItem = new ExpRecordItem__c(
			ExpRecordId__c = expRecord.Id,
			Date__c = System.today(),
			ExpTypeId__c = testData.expTypes[0].id,
			Amount__c = 1000,
			Remarks__c = 'remarks',
			ExtendedItemText01Value__c = 'ExtendedItemText01',
			Order__c = 1);
		insert recordItem;

		// 入力タイプを更新
		item.setId(item.id);
		item.inputType = ExtendedItemInputType.PICKLIST;

		// 保存する
		App.IllegalStateException resEx;
		try {
			new ExtendedItemService().saveExtendedItem(item);
		} catch(App.IllegalStateException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		String exMessage = ComMessage.msg().Com_Err_CannotChangeReference;
		System.assertEquals(exMessage, resEx.getMessage());
	}

	/**
	 * saveExtendedItemテスト
	 * トランザクションデータに紐づけられている拡張項目の場合、選択リストが変更できないことを確認する
	 */
	@isTest static void updateExpTypeTestPicklist() {
		ServiceTestData testData = new ServiceTestData();

		// 拡張項目を新規作成
		ExtendedItemEntity item = new ExtendedItemEntity();
		item.name = 'Test';
		item.nameL0 = 'Test_L0';
		item.nameL1 = 'Test_L1';
		item.nameL2 = 'Test_L2';
		item.code = 'Test001';
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.PICKLIST;

		ExtendedItemEntity.PicklistOption option = new ExtendedItemEntity.PicklistOption();
		option.label_L0 = 'Picklist_L0';
		option.label_L1 = 'Picklist_L1';
		option.label_L2 = 'Picklist_L2';
		option.value = 'Value';
		item.picklist = new List<ExtendedItemEntity.PicklistOption>{option};

		ExtendedItemService service = new ExtendedItemService();
		Id itemId = service.saveExtendedItem(item);

		// 申請タイプに紐づけ
		testData.expReportTypes[0].setExtendedItemPicklistId(0, itemId);
		new ExpReportTypeRepository().saveEntity(testData.expReportTypes[0]);

		// 経費精算レコードを作成
		ExpReport__c expReport = new ExpReport__c(
			Name = 'Test ExpReport',
			ExpReportTypeId__c = testData.expReportTypes[0].id,
			ExtendedItemPicklist01Value__c = option.value
		);
		insert expReport;

		// 拡張項目の選択肢を更新
		item.setId(itemId);
		ExtendedItemEntity.PicklistOption updateOption = new ExtendedItemEntity.PicklistOption();
		updateOption.label_L0 = 'Picklist_L0';
		updateOption.label_L1 = 'Picklist_L1';
		updateOption.label_L2 = 'Picklist_L2';
		updateOption.value = 'UpdatedValue';
		item.picklist = new List<ExtendedItemEntity.PicklistOption>{updateOption};

		// 保存する
		App.IllegalStateException resEx;
		try {
			service.saveExtendedItem(item);
		} catch(App.IllegalStateException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		String exMessage = ComMessage.msg().Com_Err_CannotChangeReference;
		System.assertEquals(exMessage, resEx.getMessage());
	}

	/*
	 * Test that extended Item Custom reference cannot be changed if it's in used in Report
	 */
	@isTest static void updateLookupAlreadyInUsedReportTypeTest() {
		ServiceTestData testData = new ServiceTestData();

		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('custom', testData.company.id, 2);
		List<ComExtendedItem__c> extendedItemLookupList = ComTestDataUtility.createExtendedItemLookups('lookup', testData.company.id, customList);

		ExtendedItemService service = new ExtendedItemService();

		// Link Extended Item with Report Type, and Report
		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			testData.expReportTypes[0].setExtendedItemLookupId(0, extendedItemLookupList[0].Id);
		}
		new ExpReportTypeRepository().saveEntity(testData.expReportTypes[0]);

		ExpReport__c expReport = new ExpReport__c(
				Name = 'Test ExpReport',
				ExpReportTypeId__c = testData.expReportTypes[0].id,
				ExtendedItemLookup01Id__c = extendedItemLookupList[0].Id
		);
		insert expReport;

		extendedItemLookupList[0].ExtendedItemCustomId__c = customList[1].Id;
		ExtendedItemEntity item = new ExtendedItemEntity();
		item.setId(extendedItemLookupList[0].Id);
		item.name = extendedItemLookupList[0].Name;
		item.nameL0 = extendedItemLookupList[0].Name_L0__c;
		item.nameL1 = extendedItemLookupList[0].Name_L1__c;
		item.nameL2 = extendedItemLookupList[0].Name_L2__c;
		item.code = extendedItemLookupList[0].Code__c;
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.CUSTOM;
		item.extendedItemCustomId = extendedItemLookupList[0].ExtendedItemCustomId__c;

		try {
			service.saveExtendedItem(item);
			System.assert(false, 'Exception Not Thrown');
		} catch(App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Com_Err_CannotChangeReference, e.getMessage());
		}
	}

	/*
	 * Test that extended Item Custom reference cannot be changed if it's in used in Record Item
	 */
	@isTest static void updateLookupAlreadyInUsedExpTypeTest() {
		ServiceTestData testData = new ServiceTestData();

		List<ComExtendedItemCustom__c> customList = ComTestDataUtility.createExtendedItemCustoms('custom', testData.company.id, 2);
		List<ComExtendedItem__c> extendedItemLookupList = ComTestDataUtility.createExtendedItemLookups('lookup', testData.company.id, customList);

		ExtendedItemService service = new ExtendedItemService();

		// Link Extended Item with Exp Type, and ExpRecord
		testData.expTypes[0].setExtendedItemLookupId(0, extendedItemLookupList[0].Id);
		new ExpTypeRepository().saveEntity(testData.expTypes[0]);

		ExpRecord__c expRecord = new ExpRecord__c(
				Date__c = System.today(),
				Amount__c = 1000,
				Order__c = 1,
				RecordType__c = testData.expTypes[0].recordType.value);
		insert expRecord;

		ExpRecordItem__c recordItem = new ExpRecordItem__c(
				ExpRecordId__c = expRecord.Id,
				Date__c = System.today(),
				ExpTypeId__c = testData.expTypes[0].id,
				Amount__c = 1000,
				Remarks__c = 'remarks',
				ExtendedItemLookup01Id__c = extendedItemLookupList[0].Id,
				Order__c = 1
		);
		insert recordItem;

		extendedItemLookupList[0].ExtendedItemCustomId__c = customList[1].Id;
		ExtendedItemEntity item = new ExtendedItemEntity();
		item.setId(extendedItemLookupList[0].Id);
		item.name = extendedItemLookupList[0].Name;
		item.nameL0 = extendedItemLookupList[0].Name_L0__c;
		item.nameL1 = extendedItemLookupList[0].Name_L1__c;
		item.nameL2 = extendedItemLookupList[0].Name_L2__c;
		item.code = extendedItemLookupList[0].Code__c;
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.CUSTOM;
		item.extendedItemCustomId = extendedItemLookupList[0].ExtendedItemCustomId__c;

		App.IllegalStateException resEx;
		try {
			service.saveExtendedItem(item);
		} catch(App.IllegalStateException e) {
			resEx = e;
		}
		TestUtil.assertNotNull(resEx, '例外が発生しませんでした');
		String exMessage = ComMessage.msg().Com_Err_CannotChangeReference;
		System.assertEquals(exMessage, resEx.getMessage());
	}

	/**
	 * searchExtendedItems テスト
	 * 会社IDを指定した場合、正しく拡張項目が取得できることを確認する
	 */
	@isTest static void searchExtendedItemsTestCompanyId() {
		// テストデータ作成
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity testExtendedItem = testData.createExtendedItem('TestExtendedItem');
		ExtendedItemEntity testExtendedItem2 = testData.createExtendedItem('TestExtendedItem2');

		CompanyEntity targetCompany = testData.createCompany('検索テスト');
		testExtendedItem.companyId = targetCompany.id;
		new ExtendedItemRepository().saveEntity(testExtendedItem);

		// 検索する
		List<ExtendedItemEntity> resExtendedItems = new ExtendedItemService().searchExtendedItems(targetCompany.id, null, null);

		// 結果確認
		System.assertEquals(1, resExtendedItems.size());
		ExtendedItemEntity resExtendedItem = resExtendedItems[0];
		System.assertEquals(testExtendedItem.id, resExtendedItem.id);
	}

	/**
	 * searchExtendedItems テスト
	 * 拡張項目IDを指定した場合、正しく拡張項目が取得できることを確認する
	 */
	@isTest static void searchExtendedItemsTestExtendedItemId() {
		// テストデータ作成
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity testExtendedItem = testData.createExtendedItem('TestExtendedItem');
		ExtendedItemEntity testExtendedItem2 = testData.createExtendedItem('TestExtendedItem2');

		// 検索する
		List<ExtendedItemEntity> resExtendedItems = new ExtendedItemService().searchExtendedItems(null, testExtendedItem2.id, null);

		// 結果確認
		System.assertEquals(1, resExtendedItems.size());
		ExtendedItemEntity resExtendedItem = resExtendedItems[0];
		System.assertEquals(testExtendedItem2.id, resExtendedItem.id);
	}

	/**
	 * deleteExtendedItem テスト
	 * 指定した拡張項目が削除できることを確認する
	 */
	@isTest static void deleteExtendedItemTest() {
		// テストデータ作成
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity testExtendedItem = testData.createExtendedItem('TestExtendedItem');
		ExtendedItemEntity testExtendedItem2 = testData.createExtendedItem('TestExtendedItem2');

		// 削除する
		Id deleteId = testExtendedItem2.id;
		new ExtendedItemService().deleteExtendedItem(deleteId);

		// 結果確認
		ExtendedItemEntity resExtendedItem = new ExtendedItemRepository().getEntity(deleteId);
		System.assertEquals(null, resExtendedItem);
	}
	
	/**
	 * deleteExtendedItem テスト
	 * 経費申請タイプに紐づけられている拡張項目の場合、削除できないことを確認する
	 */
	@isTest static void deleteExtendedItemTestExpReportType() {
		ServiceTestData testData = new ServiceTestData();

		// 拡張項目を新規作成
		ExtendedItemEntity item = new ExtendedItemEntity();
		item.name = 'Test';
		item.nameL0 = 'Test_L0';
		item.nameL1 = 'Test_L1';
		item.nameL2 = 'Test_L2';
		item.code = 'Test001';
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.TEXT;

		ExtendedItemService service = new ExtendedItemService();
		Id itemId = service.saveExtendedItem(item);

		// 経費申請に紐づけ
		testData.expReportTypes[0].setExtendedItemTextId(0, itemId);
		new ExpReportTypeRepository().saveEntity(testData.expReportTypes[0]);

		// 削除実行
		try {
			service.deleteExtendedItem(itemId);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}
	
	/**
	 * deleteExtendedItem テスト
	 * 経費精算に紐づけられている拡張項目の場合、削除できないことを確認する
	 */
	@isTest static void deleteExtendedItemTestExpReport() {
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		ExpReportType__c existingExpReportType = [SELECT Id, ExtendedItemText01TextId__c From ExpReportType__c WHERE Id = :testData.expReportTypes[0].id];
		existingExpReportType.ExtendedItemText01TextId__c = item.id;
		UPDATE existingExpReportType;

		// 経費精算レコードを作成 Create expense report
		ExpReport__c expReport = new ExpReport__c(
			Name = 'Test ExpReport',
			ExpReportTypeId__c = testData.expReportTypes[0].id,
			ExtendedItemText01Value__c = 'ExtendedItemText01'
		);
		insert expReport;

		// 削除実行 Delete
		try {
			new ExtendedItemService().deleteExtendedItem(item.id);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}
	
	/**
	 * deleteExtendedItem テスト
	 * 事前申請に紐づけられている拡張項目の場合、削除できないことを確認する
	 */
	@isTest static void deleteExtendedItemTestExpRequest() {
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		ExpReportType__c existingExpReportType = [SELECT Id, ExtendedItemText01TextId__c From ExpReportType__c WHERE Id = :testData.expReportTypes[0].id];
		existingExpReportType.ExtendedItemText01TextId__c = item.id;
		UPDATE existingExpReportType;

		// 事前申請レコードを作成 Create expense request
		ExpRequest__c expRequest = new ExpRequest__c(
			Name = 'Test ExpRequest',
			ScheduledDate__c = System.today() + 5,
			RecordingDate__c = System.today() + 5,
			ExpReportTypeId__c = testData.expReportTypes[0].id,
			ExtendedItemText01Value__c = 'ExtendedItemText01'
		);
		insert expRequest;

		// 削除実行 Delete
		try {
			new ExtendedItemService().deleteExtendedItem(item.id);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}
	
	/**
	 * deleteExtendedItem テスト
	 * 費目に紐づけられている拡張項目の場合、削除できないことを確認する
	 */
	@isTest static void deleteExtendedItemTestExpType() {
		ServiceTestData testData = new ServiceTestData();

		// 拡張項目を新規作成
		ExtendedItemEntity item = new ExtendedItemEntity();
		item.name = 'Test';
		item.nameL0 = 'Test_L0';
		item.nameL1 = 'Test_L1';
		item.nameL2 = 'Test_L2';
		item.code = 'Test001';
		item.companyId = testData.company.id;
		item.inputType = ExtendedItemInputType.TEXT;

		ExtendedItemService service = new ExtendedItemService();
		Id itemId = service.saveExtendedItem(item);

		// 費目に紐づけ
		testData.expTypes[0].setExtendedItemTextId(0, itemId);
		new ExpTypeRepository().saveEntity(testData.expTypes[0]);

		// 削除実行
		try {
			service.deleteExtendedItem(itemId);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}

	/**
	 * deleteExtendedItem テスト
	 * 経費内訳に紐づけられている拡張項目の場合、削除できないことを確認する
	 */
	@isTest static void deleteExtendedItemTestRecordItem() {
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		// 経費明細レコードを作成 Create expense record
		ExpRecord__c expRecord = new ExpRecord__c(
			Date__c = System.today(),
			Amount__c = 1000,
			Order__c = 1,
			RecordType__c = testData.expTypes[0].recordType.value);
		insert expRecord;

		ExpType__c existingExpType = [SELECT Id, ExtendedItemText01TextId__c From ExpType__c WHERE Id = :testData.expTypes[0].id];
		existingExpType.ExtendedItemText01TextId__c = item.id;
		UPDATE existingExpType;

		// 経費内訳レコードを作成 Create expense record item
		ExpRecordItem__c recordItem = new ExpRecordItem__c(
			ExpRecordId__c = expRecord.Id,
			Date__c = System.today(),
			ExpTypeId__c = testData.expTypes[0].id,
			Amount__c = 1000,
			Remarks__c = 'remarks',
			ExtendedItemText01Value__c = 'ExtendedItemText01',
			Order__c = 1);
		insert recordItem;

		// 削除実行 Delete
		try {
			new ExtendedItemService().deleteExtendedItem(item.id);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}
	
	/**
	 * deleteExtendedItem テスト
	 * 事前申請内訳に紐づけられている拡張項目の場合、削除できないことを確認する
	 */
	@isTest static void deleteExtendedItemTestRequestRecordItem() {
		ServiceTestData testData = new ServiceTestData();
		ExtendedItemEntity item = testData.createExtendedItem('TestExtendedItem');

		// 事前申請レコードを作成 Create expense request record
		ExpRequestRecord__c expRecord = new ExpRequestRecord__c(
			Date__c = System.today(),
			Amount__c = 1000,
			Order__c = 1,
			RecordType__c = testData.expTypes[0].recordType.value);
		insert expRecord;

		ExpType__c existingExpType = [SELECT Id, ExtendedItemText01TextId__c From ExpType__c WHERE Id = :testData.expTypes[0].id];
		existingExpType.ExtendedItemText01TextId__c = item.id;
		UPDATE existingExpType;

		// 事前申請内訳レコードを作成 Create expense request record item
		ExpRequestRecordItem__c recordItem = new ExpRequestRecordItem__c(
			ExpRequestRecordId__c = expRecord.Id,
			Date__c = System.today(),
			ExpTypeId__c = testData.expTypes[0].id,
			Amount__c = 1000,
			Remarks__c = 'remarks',
			ExtendedItemText01Value__c = 'ExtendedItemText01',
			Order__c = 1);
		insert recordItem;

		// 削除実行 Delete
		try {
			new ExtendedItemService().deleteExtendedItem(item.id);
		} catch (App.IllegalStateException e) {
			String msg = ComMessage.msg().Com_Err_CannotDeleteReference;
			System.assertEquals(msg, e.getMessage());
		}
	}
}