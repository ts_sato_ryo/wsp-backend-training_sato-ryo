/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇管理系APIを提供するリソーステストクラス
 */
@isTest
private class AttManagedLeaveResourceTest {
	//  *********** 以下、対象社員検索テスト
	// 通常
	@isTest static void searchEmpAnnualTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		testData.workingType.getHistory(0).leaveCodeList = new List<String>{annualLeave.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));
		testData.employee.getHistory(0).departmentId = testData.dept.Id;
		new EmployeeRepository().saveHistoryEntity(testData.employee.getHistory(0));

		DepartmentBaseEntity deptData = new DepartmentRepository().getEntity(testData.dept.Id, AppDate.today());

		AttManagedLeaveResource.SearchEmpListRequest param = new AttManagedLeaveResource.SearchEmpListRequest();
		AttManagedLeaveResource.SearchEmpListResponse res = new AttManagedLeaveResource.SearchEmpListResponse();
		AttManagedLeaveResource.SearchEmpListApi api = new AttManagedLeaveResource.SearchEmpListApi();

		param.companyId = testData.company.id;

		Test.startTest();
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(1, res.records.size());
		System.assertEquals(testData.employee.id, res.records[0].id);
		System.assertEquals(testData.employee.code, res.records[0].code);
		System.assertEquals(testData.employee.fullNameL.getFullName(), res.records[0].name);
		System.assertEquals(deptData.getHistory(0).nameL.getValue(), res.records[0].deptName);
		System.assertEquals(testData.workingType.getHistory(0).nameL.getValue(), res.records[0].workingTypeName);
		System.assertNotEquals(null, res.records[0].photoUrl);
	}
	// 年次有休登録なし
	@isTest static void searchEmpNoAnnualTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttManagedLeaveResource.SearchEmpListRequest param = new AttManagedLeaveResource.SearchEmpListRequest();
		AttManagedLeaveResource.SearchEmpListResponse res = new AttManagedLeaveResource.SearchEmpListResponse();
		AttManagedLeaveResource.SearchEmpListApi api = new AttManagedLeaveResource.SearchEmpListApi();

		param.companyId = testData.company.id;

		Test.startTest();
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(0, res.records.size());
	}
	// 年次有休使用しない
	@isTest static void searchEmpNoUseAnnualTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		AttManagedLeaveResource.SearchEmpListRequest param = new AttManagedLeaveResource.SearchEmpListRequest();
		AttManagedLeaveResource.SearchEmpListResponse res = new AttManagedLeaveResource.SearchEmpListResponse();
		AttManagedLeaveResource.SearchEmpListApi api = new AttManagedLeaveResource.SearchEmpListApi();

		param.companyId = testData.company.id;

		Test.startTest();
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(0, res.records.size());
	}
	// 勤務体系で検索
	@isTest static void searchEmpAnnualByWorkingTypeTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		testData.workingType.getHistory(0).leaveCodeList = new List<String>{annualLeave.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		AttManagedLeaveResource.SearchEmpListRequest param = new AttManagedLeaveResource.SearchEmpListRequest();
		AttManagedLeaveResource.SearchEmpListResponse res = new AttManagedLeaveResource.SearchEmpListResponse();
		AttManagedLeaveResource.SearchEmpListApi api = new AttManagedLeaveResource.SearchEmpListApi();

		Test.startTest();
		param.companyId = testData.company.id;
		param.workingTypeName = testData.workingType.name;
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);

		System.assertEquals(1, res.records.size());

		param.workingTypeName = 'abc';
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);

		System.assertEquals(0, res.records.size());
		Test.stopTest();
	}
	// 社員で検索
	@isTest static void searchEmpAnnualByEmpTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		testData.workingType.getHistory(0).leaveCodeList = new List<String>{annualLeave.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		AttManagedLeaveResource.SearchEmpListRequest param = new AttManagedLeaveResource.SearchEmpListRequest();
		AttManagedLeaveResource.SearchEmpListResponse res = new AttManagedLeaveResource.SearchEmpListResponse();
		AttManagedLeaveResource.SearchEmpListApi api = new AttManagedLeaveResource.SearchEmpListApi();

		Test.startTest();
		param.companyId = testData.company.id;
		param.empCode = testData.employee.code;
		param.empName = testData.employee.fullNameL.getFullName();
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);

		System.assertEquals(1, res.records.size());

		param.empCode = 'a';
		param.empName = 'b';
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);

		System.assertEquals(0, res.records.size());
		Test.stopTest();
	}
	// 部署で検索
	@isTest static void searchEmpAnnualByDeptTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		testData.workingType.getHistory(0).leaveCodeList = new List<String>{annualLeave.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		testData.employee.getHistory(0).departmentId = testData.dept.Id;
		new EmployeeRepository().saveHistoryEntity(testData.employee.getHistory(0));

		DepartmentBaseEntity deptData = new DepartmentRepository().getEntity(testData.dept.Id, AppDate.today());

		AttManagedLeaveResource.SearchEmpListRequest param = new AttManagedLeaveResource.SearchEmpListRequest();
		AttManagedLeaveResource.SearchEmpListResponse res = new AttManagedLeaveResource.SearchEmpListResponse();
		AttManagedLeaveResource.SearchEmpListApi api = new AttManagedLeaveResource.SearchEmpListApi();

		Test.startTest();
		param.companyId = testData.company.id;
		param.deptName = deptData.getHistory(0).nameL.getValue();
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);

		System.assertEquals(1, res.records.size());

		param.deptName = 'abc';
		res = (AttManagedLeaveResource.SearchEmpListResponse)api.execute(param);

		System.assertEquals(0, res.records.size());
		Test.stopTest();
	}

	// *********** 以下、日数管理休暇一覧取得テスト
	@isTest static void searchManagedLeaveListApiTest() {

		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 年次有給休暇も合わせて登録し日数管理休暇一覧に出現しないことを確認する
		testData.createLeave('003', '年次有休', AttLeaveType.ANNUAL, false, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		AttLeaveEntity leave1 = testData.createLeave('004', '積み休１', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		AttLeaveEntity leave2 = testData.createLeave('005', '積み休２', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		AttManagedLeaveResource.SearchManagedLeaveListRequest param = new AttManagedLeaveResource.SearchManagedLeaveListRequest();
		AttManagedLeaveResource.SearchManagedLeaveListApi api = new AttManagedLeaveResource.SearchManagedLeaveListApi();
		AttManagedLeaveResource.SearchManagedLeaveListResponse res;
		param.companyId = testData.company.id;

		Test.startTest();
		res = (AttManagedLeaveResource.SearchManagedLeaveListResponse)api.execute(param);
		Test.stopTest();

		//　年次有給休暇がリストに引っかからないこと
		System.assertEquals(2, res.records.size());
		System.assertEquals(res.records[0].id, leave1.id);
		System.assertEquals(res.records[0].name, leave1.nameL.getValue());
		System.assertEquals(res.records[1].id, leave2.id);
		System.assertEquals(res.records[1].name, leave2.nameL.getValue());
	}

	// *********** 以下、有休付与テスト
	@isTest static void createAnnualGrantTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		testData.workingType.getHistory(0).leaveCodeList = new List<String>{annualLeave.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		AttManagedLeaveResource.CreateGrantRequest param = new AttManagedLeaveResource.CreateGrantRequest();
		AttManagedLeaveResource.CreateGrantApi api = new AttManagedLeaveResource.CreateGrantApi();

		Test.startTest();
		Date dt = date.today();
		param.empId = testData.employee.id;
		param.daysGranted = 10;
		param.validDateFrom = dt;
		param.validDateTo = dt.addYears(2);
		param.comment = 'test grant';
		api.execute(param);
		Test.stopTest();

		AttManagedLeaveGrantRepository rep = new AttManagedLeaveGrantRepository();
		AttManagedLeaveGrantRepository.SearchFilter filter = new AttManagedLeaveGrantRepository.SearchFilter();
		filter.empId = testData.employee.id;
		filter.leaveId = annualLeave.Id;
		List<AttManagedLeaveGrantEntity> grantList = rep.searchEntityList(filter, false);
		System.assertEquals(1, grantList.size());
		System.assertEquals(param.empId, grantList[0].employeeId);
		System.assertEquals(annualLeave.id, grantList[0].leaveId);
		System.assertEquals(param.daysGranted, AppConverter.decValue(grantList[0].daysGranted));
		System.assertEquals(param.daysGranted, AppConverter.decValue(grantList[0].daysLeft));
		System.assertEquals(param.validDateFrom, AppConverter.dateValue(grantList[0].validDateFrom));
		System.assertEquals(param.validDateTo, AppConverter.dateValue(grantList[0].validDateTo));
		System.assertEquals(param.comment, grantList[0].comment);
	}
	// 有休付与テスト(API実行権限がない場合に想定された例外が発生する)
	@isTest static void createAnnualGrantTestNoPermission() {
		AttTestData testData = new AttTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('Standard');

		// 休暇管理の管理権限を無効に設定する
		testData.permission.isManageAttLeaveGrant = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttManagedLeaveResource.CreateGrantRequest param = new AttManagedLeaveResource.CreateGrantRequest();
		Date dt = date.today();
		param.empId = testData.employee.id;
		param.daysGranted = 10;
		param.validDateFrom = dt;
		param.validDateTo = dt.addYears(2);

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttManagedLeaveResource.CreateGrantApi api = new AttManagedLeaveResource.CreateGrantApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	// *********** 以下、有休付与一覧取得テスト
	@isTest static void getAnnualGrantListTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		Date dt = date.today();
		AttManagedLeaveGrantRepository rep = new AttManagedLeaveGrantRepository();
		AttManagedLeaveGrantEntity grantData = new AttManagedLeaveGrantEntity();

		grantData.employeeId = testData.employee.id;
		grantData.leaveId = annualLeave.Id;
		grantData.validDateFrom = AppDate.valueOf(dt.addDays(-5)); // 2番目
		grantData.validDateTo = AppDate.valueOf(dt.addYears(1));
		grantData.daysGranted = AttDays.valueOf(3);
		grantData.daysLeft = AttDays.valueOf(1.33333);
		rep.saveEntity(grantData);

		grantData.validDateFrom = AppDate.valueOf(dt.addDays(-10)); // 1番目
		grantData.daysGranted = AttDays.valueOf(2);
		grantData.daysLeft = AttDays.valueOf(2);
		rep.saveEntity(grantData);

		AttManagedLeaveResource.GetGrantListRequest param = new AttManagedLeaveResource.GetGrantListRequest();
		AttManagedLeaveResource.GetGrantListResponse res = new AttManagedLeaveResource.GetGrantListResponse();
		AttManagedLeaveResource.GetGrantListApi api = new AttManagedLeaveResource.GetGrantListApi();

		param.empId = testData.employee.id;
		Test.startTest();
		res = (AttManagedLeaveResource.GetGrantListResponse)api.execute(param);
		Test.stopTest();

		System.assertEquals(2, res.records.size());
		AttManagedLeaveResource.GrantResponse grantRes = res.records[0];
		System.assertEquals(dt.addDays(-10), grantRes.validDateFrom);
		System.assertEquals(dt.addYears(1), grantRes.validDateTo);
		System.assertEquals(2, grantRes.daysGranted);
		System.assertEquals(2, grantRes.daysLeft);
		System.assertEquals(0, grantRes.hoursLeft);

		grantRes = res.records[1];
		System.assertEquals(dt.addDays(-5), grantRes.validDateFrom);
		System.assertEquals(dt.addYears(1), grantRes.validDateTo);
		System.assertEquals(3, grantRes.daysGranted);
		System.assertEquals(1, grantRes.daysLeft);
		System.assertEquals(3, grantRes.hoursLeft);
	}

	// *********** 以下、休暇付与調整テスト
	@istest static void adjustGrantTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		Date dt = date.today();
		AttManagedLeaveGrantRepository rep = new AttManagedLeaveGrantRepository();
		AttManagedLeaveGrantEntity grantData = new AttManagedLeaveGrantEntity();

		grantData.employeeId = testData.employee.id;
		grantData.leaveId = annualLeave.Id;
		grantData.validDateFrom = AppDate.valueOf(dt);
		grantData.validDateTo = AppDate.valueOf(dt.addYears(1));
		grantData.originalDaysGranted = AttDays.valueOf(10);
		grantData.daysGranted = AttDays.valueOf(10);
		grantData.daysLeft = AttDays.valueOf(10);
		Repository.SaveResult saveResult = rep.saveEntity(grantData);
		Id grantId = saveResult.details[0].id;

		AttManagedLeaveResource.AdjustGrantRequest param = new AttManagedLeaveResource.AdjustGrantRequest();
		AttManagedLeaveResource.AdjustGrantApi api = new AttManagedLeaveResource.AdjustGrantApi();

		Test.startTest();
		param.grantId = grantId;
		param.daysGranted = 8;
		api.execute(param);
		Test.stopTest();

		AttManagedLeaveGrantRepository.SearchFilter filter = new AttManagedLeaveGrantRepository.SearchFilter();
		filter.Id = grantId;
		List<AttManagedLeaveGrantEntity> grantList = rep.searchEntityList(filter, false);
		System.assertEquals(1, grantList.size());
		System.assertEquals(10, AppConverter.decValue(grantList[0].originalDaysGranted));
		System.assertEquals(8, AppConverter.decValue(grantList[0].daysGranted));
		System.assertEquals(8, AppConverter.decValue(grantList[0].daysLeft));

		System.assertEquals(1, grantList[0].adjustList.size());
		System.assertEquals(-2, AppConverter.decValue(grantList[0].adjustList[0].daysAdjusted));
		System.assertNotEquals(null, grantList[0].adjustList[0].adjustDate);
	}
	// 有休付与調整テスト(API実行権限がない場合に想定された例外が発生する)
	@isTest static void adjustGrantTestNoPermission() {
		AttTestData testData = new AttTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('Standard');

		// 休暇管理の管理権限を無効に設定する
		testData.permission.isManageAttLeaveGrant = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttManagedLeaveResource.AdjustGrantRequest param = new AttManagedLeaveResource.AdjustGrantRequest();
		param.grantId = UserInfo.getUserId();
		param.daysGranted = 8;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttManagedLeaveResource.AdjustGrantApi api = new AttManagedLeaveResource.AdjustGrantApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	// *********** 以下、休暇付与削除テスト
	/**
	 * DeleteGrantApiのテスト（正常系）
	 * 指定した休暇付与を論理削除できることを確認する
	 */
	@istest static void deleteGrantApiTestSuccess() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		testData.workingType.getHistory(0).leaveCodeList = new List<String>{annualLeave.code};
		new AttWorkingTypeRepository().saveHistoryEntity(testData.workingType.getHistory(0));

		AppDate dt = AppDate.valueOf('2019-05-01');

		// 休暇付与を作成
		AttManagedLeaveGrantRepository grantRepo = new AttManagedLeaveGrantRepository();
		AttManagedLeaveGrantEntity grant = new AttManagedLeaveGrantEntity();
		grant.employeeId = testData.employee.id;
		grant.leaveId = annualLeave.Id;
		grant.validDateFrom = dt;
		grant.validDateTo = dt.addMonths(1);
		grant.originalDaysGranted = AttDays.valueOf(10);
		grant.daysGranted = AttDays.valueOf(4);
		grant.daysLeft = AttDays.valueOf(4);
		Repository.SaveResult saveResult = grantRepo.saveEntity(grant);
		Id grantId = saveResult.details[0].id;

		// 実行
		AttManagedLeaveResource.DeleteGrantRequest param = new AttManagedLeaveResource.DeleteGrantRequest();
		AttManagedLeaveResource.DeleteGrantApi api = new AttManagedLeaveResource.DeleteGrantApi();
		Test.startTest();
		param.grantId = grantId;
		api.execute(param);
		Test.stopTest();

		// 論理削除されているため、対象の休暇付与を取得できないことを確認する
		AttManagedLeaveGrantRepository.SearchFilter filter = new AttManagedLeaveGrantRepository.SearchFilter();
		filter.Id = grantId;
		List<AttManagedLeaveGrantEntity> grantList = grantRepo.searchEntityList(filter, false);
		System.assertEquals(0, grantList.size());
	}

	/**
	 * DeleteGrantApiのテスト（異常系）
	 * API実行権限がない場合に想定された例外が発生することを確認する
	 */
	@isTest static void deleteGrantApiTestNoPermission() {
		AttTestData testData = new AttTestData();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeStandardUser('Standard');

		// 休暇管理の管理権限を無効に設定する
		testData.permission.isManageAttLeaveGrant = false;
		new PermissionRepository().saveEntity(testData.permission);

		AttManagedLeaveResource.AdjustGrantRequest param = new AttManagedLeaveResource.AdjustGrantRequest();
		param.grantId = ComTestDataUtility.getDummyId(AttManagedLeaveGrant__c.sObjectType); // ダミーID

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				AttManagedLeaveResource.DeleteGrantApi api = new AttManagedLeaveResource.DeleteGrantApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * DeleteGrantApiのテスト（異常系）
	 * リクエストパラメータのバリデーションが想定通り動くことを確認する
	 */
	@isTest static void deleteGrantApiTestInvalidParam() {

		AttManagedLeaveResource.DeleteGrantRequest param = new AttManagedLeaveResource.DeleteGrantRequest();
		AttManagedLeaveResource.DeleteGrantApi api = new AttManagedLeaveResource.DeleteGrantApi();

		// case1 : grantIdがnullの場合
		App.ParameterException paramEx;
		try {
			api.execute(param);
		} catch(App.ParameterException e) {
			paramEx = e;
		}
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, paramEx.getErrorCode());
		System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'grantId'}), paramEx.getMessage());

		// case2 : grantIdの型が不正な場合
		App.TypeException typeEx;
		try {
			param.grantId = 'invalidId';
			api.execute(param);
		} catch(App.TypeException e) {
			typeEx = e;
		}
		System.assertEquals(App.ERR_CODE_INVALID_TYPE, typeEx.getErrorCode());
		System.assertEquals('grantId の型が正しくありません。(grantId=invalidId)', typeEx.getMessage());
	}
}