/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group プランナー
 *
 * @description プランナー操作APIを実装するクラス
 */
public with sharing class PlannerResource {

	/**
	 * @description 予定データ取得条件を格納するクラス
	 */
	public class GetParam implements RemoteApi.RequestParam {
		public String empId;		//社員ID
		public String startDate;		// 開始日
		public String endDate;		// 終了日

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// 社員ID
			if (String.isNotBlank(empId)) {
				try {
					Id.valueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', this.empId);
				}
			}
			// 開始日
			if (String.isBlank(startDate)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'startDate'}));
			} else {
				try {
					AppDate.parse(startDate);
				} catch (Exception e) {
					throw new App.ParameterException('startDate', this.startDate);
				}
			}
			// 終了日
			if (String.isBlank(endDate)) {
				throw new App.ParameterException(ComMessage.msg().Com_Err_InvalidValue(new List<String>{'endDate'}));
			} else {
				try {
					AppDate.parse(endDate);
				} catch (Exception e) {
					throw new App.ParameterException('endDate', this.endDate);
				}
			}
		}
	}

	/**
	 * @description 予定データ取得結果を格納するクラス
	 */
	public class GetResult implements RemoteApi.ResponseParam {
		public List<Event> eventList;	// イベントのリスト
		public List<Message> messageList;	// メッセージのリスト

		public GetResult() {
			eventList = new List<Event>();
			messageList = new List<Message>();
		}
	}

	/**
	 * @description 予定データ（予定情報）取得結果を格納するクラス
	 */
	public class Event {
		public Id id;
		public Id ownerId;
		public String title;
		public Datetime startDateTime;
		public Datetime endDateTime;
		public Boolean isAllDay;
		public Boolean isOrganizer;
		public String location;
		public String description;
		public Boolean isOuting;
		public String createdServiceBy;
		public Id jobId;
		public String jobName;
		public String jobCode;
		public Id workCategoryId;
		public String workCategoryName;
		public Id contactId;
		public String contactName;
		public String externalEventId;

		/**
		 * エンティティからインスタンスを生成するコンストラクタ
		 * @param event エンティティ
		 */
		public Event(PlanEventEntity event) {
			this.id = event.Id;
			this.ownerId = event.OwnerId;
			this.title = event.subject;
			this.startDateTime = event.StartDateTime.getDatetime();
			this.endDateTime = event.EndDateTime.getDatetime();
			this.isAllDay = event.isAllDay;
			this.isOrganizer = event.IsOrganizer;
			this.location = event.Location;
			this.description = event.Description;
			this.isOuting = event.IsOuting;
			this.createdServiceBy = event.CreatedServiceBy.name();
			if (event.job != null) {
				this.jobId = event.job.id;
				this.jobName = event.job.nameL.getValue();
				this.jobCode = event.job.Code;
			}
			if (event.workCategory != null) {
				this.workCategoryId = event.workCategory.id;
				this.workCategoryName = event.workCategory.nameL.getValue();
			}
			this.contactId = event.contactId;
			this.contactName = event.contactName;
			this.externalEventId = event.externalEventId;
		}
	}

	/**
	 * @description 外部カレンダーとの連携に失敗した場合のメッセージを格納するクラス
	 */
	public class Message {

		public String message;

		/**
		 * エンティティからインスタンスを生成するコンストラクタ
		 * @param message メッセージ
		 */
		public Message(String message) {
			this.message = message;
		}
	}

	/**
	 * @description 予定データ取得APIを実装するクラス
	 */
	public with sharing class GetApi extends RemoteApi.NoRollbackResourceBase {

		/**
		 * @description 予定データを取得する
		 * @param  req リクエストパラメータ(GetParam)
		 * @return レスポンス(GetResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			EmployeeService employeeService = new EmployeeService();
			// リクエストパラメータを取得
			GetParam param = (GetParam)req.getParam(GetParam.class);
			param.validate();

			AppDate fromDate = AppDate.parse(param.startDate);
			AppDate toDate = AppDate.parse(param.endDate);

			EmployeeBaseEntity employee;
			if (String.isBlank(param.empId)) {
				employee = employeeService.getEmployeeByUserId(UserInfo.getUserId(), null);//プランナー表示時には有効期間のチェックは行わない
			} else {
				employee = employeeService.getEmployee(param.empId, null);
			}
			Id userId = employee.userId;//nullチェックはサービスでエラーがスローされる

			// レスポンス
			GetResult response = new GetResult();

			// プランナーイベントを取得
			List<PlanEventEntity> eventList = new PlannerService().searchEvents(userId, fromDate, toDate);

			// 会社の外部カレンダー連携が有効の場合、外部カレンダーの予定を取得してマージする
			if (isActiveExternalCalendar(userId)) {
				try {
					List<PlanEventEntity> office365EventList = Office365Service.getOffice365Events(userId, fromDate, toDate);
					eventList.addAll(office365EventList);
					eventList.sort();
				} catch (App.IllegalStateException e) {
					// 外部カレンダーとの連携に失敗した場合、メッセージが設定される
					response.messageList.add(new Message(e.getMessage()));
				}
			}

			// レスポンスを設定
			for (PlanEventEntity entity : eventList) {
				response.eventList.add(new Event(entity));
			}
			return response;
		}

		/*
		* 外部カレンダー連携が有効かどうか確認する
		* @param userId ユーザID
		* @return true:有効/false:無効
		*/
		@testVisible
		private Boolean isActiveExternalCalendar(Id userId) {

			EmployeeBaseEntity empEntity = new EmployeeService().getEmployeeByUserId(userId, null);
			CompanyEntity comEntity = new CompanyRepository().getEntity(empEntity.companyId);

			return comEntity.useCalendarAccess;
		}
	}

	/**
	 * @description 予定データ保存内容を格納するクラス
	 */
	public class SaveParam implements RemoteApi.RequestParam {
		public Id id;
		public String title;
		public String startDateTime;
		public String endDateTime;
		public Boolean isAllDay;
		public String location;
		public String description;
		public Boolean isOuting;
		public Id jobId;
		public Id workCategoryId;
		public Id contactId;
	}

	/**
	 * @description 予定データ保存結果レスポンス
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		public String id;
	}

	/**
	 * @description 予定データ保存APIを実装するクラス
	 */
	public with sharing class SaveApi extends RemoteApi.ResourceBase {

		/**
		 * @description 予定データを保存する
		 * @param  req リクエストパラメータ(SaveParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// リクエストパラメータを取得
			SaveParam param = (SaveParam)req.getParam(SaveParam.class);

			// 登録
			PlanEventEntity entity = createEntity(param);
			validate(entity);
			Id id = new PlannerService().saveEvent(entity);

			// 成功レスポンスをセットする
			SaveResult res = new SaveResult();
			res.id = id;
			return res;
		}

		/**
		 * パラメータからエンティティを生成する
		 * @param param パラメータ
		 * @return 生成したエンティティ
		 */
		private PlanEventEntity createEntity(SaveParam param) {
			PlanEventEntity entity = new PlanEventEntity();
			if (String.isEmpty(param.id)) {
				// insert
				entity.isOrganizer = true;
				entity.createdServiceBy = PlanEventEntity.ServiceType.teamspirit;
			} else {
				// update
				entity.setId(param.id);
			}
			entity.subject = param.title;
			entity.startDateTime = AppDatetime.parse(param.startDateTime);
			entity.endDateTime = AppDatetime.parse(param.endDateTime);
			entity.isAllDay = param.isAllDay;
			entity.location = param.location;
			entity.description = param.description;
			entity.isOuting = param.isOuting;
			entity.setJobId(param.jobId);
			entity.setWorkCategoryId(param.workCategoryId);
			entity.contactId = param.contactId;
			entity.lastModifiedService = PlanEventEntity.ServiceType.teamspirit;
			entity.lastModifiedDateTime = AppDatetime.now();
			entity.ownerId = UserInfo.getUserId();
			return entity;
		}

		/**
		 * エンティティのバリデーションを行う。
		 * ジョブが設定されている場合、以下のチェックを行う
		 * ・予定の開始日、終了日にジョブが有効ではない
		 * ・予定の開始日、終了日に作業分類が有効ではない
		 * ・作業分類がジョブタイプに紐付いていない
		 * @param entity
		 * @throw App.ParameterException
		 */
		private void validate(PlanEventEntity entity) {
			ComMsgBase MESSAGE = ComMessage.msg();
			// 社員情報のチェック
			AppDateRange masterBaseRange = new AppDateRange(
				new AppDate(entity.startDateTime.getDate()), new AppDate(entity.endDateTime.getDate()));
			EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserIdAndRange(entity.ownerId, masterBaseRange);
			// 桁あふれのチェック
			// FIXME:
			// プランナーのオブジェクトの整理がついていないのでGENIE-12266ではPlannerValidatorに含めないこととしました。
			// 整理がつき次第、Validatorクラスに以降するか、API引数のチェックとしてこのまま残すかを検討してください。
			if (String.isNotBlank(entity.subject)) {
				if (entity.subject.length() > PlanEventEntity.SUBJECT_MAX_LENGTH) {
					App.ParameterException e = new App.ParameterException();
					e.setMessage(
							MESSAGE.Com_Err_MaxLengthOver(
									new List<String>{MESSAGE.Cal_Lbl_Title,
									String.valueOf(PlanEventEntity.SUBJECT_MAX_LENGTH)}));
					throw e;
				}
			}
			if (String.isNotBlank(entity.location)) {
				if (entity.location.length() > PlanEventEntity.LOCATION_MAX_LENGTH) {
					App.ParameterException e = new App.ParameterException();
					e.setMessage(
							MESSAGE.Com_Err_MaxLengthOver(
									new List<String>{MESSAGE.Cal_Lbl_Location,
									String.valueOf(PlanEventEntity.LOCATION_MAX_LENGTH)}));
					throw e;
				}
			}
			if (String.isNotBlank(entity.description)) {
				if (entity.description.length() > PlanEventEntity.DESCRIPTION_MAX_LENGTH) {
					App.ParameterException e = new App.ParameterException();
					e.setMessage(
							MESSAGE.Com_Err_MaxLengthOver(
									new List<String>{MESSAGE.Cal_Lbl_Description,
									String.valueOf(PlanEventEntity.DESCRIPTION_MAX_LENGTH)}));
					throw e;
				}
			}
			// タスク(ジョブ+作業分類)
			if (entity.job == null) {
				return;
			}
			TimeService service = new TimeService();

			// 所属会社が工数設定を利用しない　かつ　jobがある場合エラー
			service.canUseTimeTracking(employee);
			// 所属会社が工数設定を利用する かつ 社員履歴に工数設定がない場合はエラーとする
			new TimeSettingService().validateTimeSetting(employee.id, masterBaseRange);

			AppDate fromDate = new AppDate(entity.startDateTime.getDate());
			AppDate toDate = new AppDate(entity.endDateTime.getDate());
			Id jobId = entity.job == null ? null : entity.job.id;
			Id workCategoryId = entity.workCategory == null ? null : entity.workCategory.id;
			TimeService.Task task = new TimeService.Task(jobId, workCategoryId);
			employee = new EmployeeService().getEmployeeByUserId(entity.ownerId, fromDate);

			// FIXME: validateTaskをRangeで実行可能にする
			List<String> errors = service.validateTask(employee, task, fromDate);
			if (errors.isEmpty() && !fromDate.equals(toDate)) {
				errors = service.validateTask(employee, task, toDate);
			}
			if (errors.isEmpty()) {
				return;
			}

			App.ParameterException e = new App.ParameterException();
			e.setMessage(errors[0]); // TODO 暫定的に1件のみエラーを返す
			throw e;
		}
	}


	/**
	 * @description 削除対象の予定データのIDを格納するクラス
	 */
	public class DeleteParam implements RemoteApi.RequestParam {

		public Id id;
	}

	/**
	 * @description 予定データ削除結果レスポンス
	 */
	public class DeleteResult implements RemoteApi.ResponseParam {
		public String id;
	}

	/**
	 * @description 予定データ削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/**
		 * @description 予定データを削除する
		 * @param  req リクエストパラメータ(DeleteParam)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// リクエストパラメータを取得
			DeleteParam param = (DeleteParam)req.getParam(DeleteParam.class);

			// 検証
			validate(param.id);

			// 削除
			Id id = new PlannerService().deleteEvent(param.id);
			DeleteResult res = new DeleteResult();
			res.id = id;
			return res;
		}

		/**
		 * 削除対象エンティティのバリデーションを行う。
		 * - 削除対象のイベントが存在すること
		 * - 削除対象の予定日時に削除を実行する社員が有効であること
		 * @param id 削除対象のイベントID
		 * @throw App.RecordNotFoundException イベントが見つからない場合、社員が見つからない場合
		 * @throws App.IllegalStateException 複数登録されている場合
		 */
		private void validate(Id id) {
			PlanEventEntity entity = new PlannerService().getEntity(id);
			// 削除対象のイベントが無ければエラー
			if (entity == null) {
				throw new App.RecordNotFoundException(
					App.ERR_CODE_PLANNER_NOT_FOUND_PLAN_EVENT,
					ComMessage.msg().Trac_Err_NotFound(new List<String>{ComMessage.msg().Cal_Lbl_Event}));
			}
			// 削除を実行する社員のチェック（社員がいない・有効期間外）
			AppDateRange masterBaseRange = new AppDateRange(
					new AppDate(entity.startDateTime.getDate()), new AppDate(entity.endDateTime.getDate()));
			EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserIdAndRange(UserInfo.getUserId(), masterBaseRange);
		}
	}

	/**
	 * @description アクティブジョブ一覧取得条件を格納するクラス
	 */
	public class GetActiveJobListParam implements RemoteApi.RequestParam {
		public String empId;		// 社員ID
		public String targetDate;	// 日付
	}

	/**
	 * @description アクティブジョブ一覧取得結果を格納するクラス
	 */
	public class GetActiveJobListResult implements RemoteApi.ResponseParam {
		public List<Job> activeJobList;	// ジョブのリスト

		public GetActiveJobListResult() {
			activeJobList = new List<Job>();
		}
	}

	/**
	 * @description アクティブジョブ一覧（ジョブ情報）取得結果を格納するクラス
	 */
	public class Job {
		public Id jobId;
		public String jobCode;
		public String jobName;
	}

	/**
	 * @description アクティブジョブ一覧取得APIを実装するクラス
	 */
	public with sharing class GetActiveJobListApi extends RemoteApi.ResourceBase {

		/**
		 * @description アクティブなジョブの一覧を取得する
		 * @param  req リクエストパラメータ(GetActiveJobListParam)
		 * @return レスポンス(GetActiveJobListResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			TimeResource.Util util = new TimeResource.Util();

			// リクエストパラメータを取得
			GetActiveJobListParam param = (GetActiveJobListParam)req.getParam(GetActiveJobListParam.class);
			// パラメータで取得した日付をDate型に変換
			Date targetDate = Date.valueOf(param.targetDate);

			// 社員情報を取得
			EmployeeBaseEntity employee;
			if (String.isBlank(param.empId)) {
				// 社員IDが設定されていない場合、ログインユーザから社員情報を取得する
				employee = util.getEmployeeByUserId(UserInfo.getUserId());
			} else {
				employee = util.getEmployee(param.empId);
			}

			// 個人設定から表示タスク情報を取得
			PersonalSettingRepository repo = new PersonalSettingRepository();
			PersonalSettingEntity setting = repo.getPersonalSetting(employee.id);

			List<PersonalSettingEntity.Task> taskList = new List<PersonalSettingEntity.Task>();
			if (setting != null) {
				taskList.addAll(setting.timeInputSetting.taskList);
			}

			List<Job> jobList = new List<Job>();
			Set<String> jobCodeSet = new Set<String>();

			// 表示タスク情報からレスポンス用のタスクリストを作成
			for (PersonalSettingEntity.Task task : taskList) {
				Job job = new Job();
				job.jobCode = task.jobCode;
				if (jobCodeSet.contains(job.jobCode)) {
					continue;
				}
				jobCodeSet.add(job.jobCode);
				jobList.add(job);
			}

			// 対象日の予定を取得
			for (PlanEvent__c event : [
					SELECT
						Id,
						JobId__c,
						JobId__r.Code__c,
						JobId__r.Name_L0__c,
						JobId__r.Name_L1__c,
						JobId__r.Name_L2__c
					FROM PlanEvent__c
					WHERE OwnerId = :employee.userId
					AND IsRecurrence__c = false
					AND (ResponseStatus__c = null OR ResponseStatus__c = 'Accepted')
					AND DAY_ONLY(convertTimeZone(StartDateTime__c)) = :targetDate
					ORDER BY StartDateTime__c]
			) {
				Boolean existsJob = false;
				for (Job job : jobList) {
					if (job.jobCode == event.JobId__r.Code__c) {
						existsJob = true;
						break;
					}
				}

				if (!existsJob) {
					// 既存ジョブに含まれていないので追加
					Job job = new Job();
					job.jobId = event.JobId__c;
					job.jobCode = event.JobId__r.Code__c;
					job.jobName = ComUtility.getTransFieldValue(event.JobId__r.Name_L0__c, event.JobId__r.Name_L1__c, event.JobId__r.Name_L2__c);
					jobList.add(job);
					jobCodeSet.add(job.jobCode);
				}
			}

			// 有効期限内のジョブを取得
			List<JobEntity> activeJobs = new JobService().getActiveTimeTrackJob(employee, AppDate.valueOf(targetDate));
			Map<String, JobEntity> activeJobMapByCode = new Map<String, JobEntity>();
			for (JobEntity job : activeJobs) {
				activeJobMapByCode.put(job.code, job);
			}

			// 有効期限内のジョブのみを表示対象とする
			List<Job> activeJobList = new List<Job>();
			Set<String> activeJobCodeSet = activeJobMapByCode.keySet();
			for (Job job : jobList) {
				if (activeJobCodeSet.contains(job.jobCode)) {
					JobEntity activeJob = activeJobMapByCode.get(job.jobCode);
					job.jobId = activeJob.id;
					job.jobName = activeJob.nameL.getValue();
					activeJobList.add(job);
				}
			}

			// レスポンスをセットする
			GetActiveJobListResult res = new GetActiveJobListResult();
			res.activeJobList = activeJobList;

			return res;
		}
	}
}
