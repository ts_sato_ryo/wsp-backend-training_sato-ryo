/**
 * @group 共通
 *
 * @description チーム機能で利用する勤怠サマリーのサービスを統合するサービス(TeamAttendanceSummaryService)をテストするクラス。
 */
@isTest
private class TeamAttendanceSummaryServiceTest {

	/** テストデータ */
	private class TestData {
		/** テストデータ */
		public AttTestData testData;
		/** 部署オブジェクト */
		public ComDeptBase__c dept;
		/** 勤務社員 */
		public EmployeeBaseEntity employee;
		/** 承認者(上長) */
		public EmployeeBaseEntity approver;
		/** 勤務体系 */
		public AttWorkingTypeBaseEntity workingType;

		/**
		 * コンストラクタ（社員に勤務体系を設定する)
		 * @param wkType 労働時間制
		 */
		public TestData(AttWorkSystem wkType) {
			testData = new AttTestData(wkType);
			dept = testData.dept;
			employee = testData.employee;
			workingType = testData.workingType;
			// 勤務社員の承認者01設定
			approver = testData.createTestEmployees(1).get(0);
			update new ComEmpHistory__c(
					Id = this.employee.getHistory(0).id,
					ApproverBase01Id__c = approver.id);
		}

		/**
		 * 指定した月の勤怠データ（サマリー＋明細）を作成し、DBに登録する(月度専用)
		 * @param emp 社員
		 * @param wkType 勤務体系
		 * @param startDate 開始日
		 * @param createPreNext 対象日の前の月度と次の月度も作成する場合はtrue
		 *
		 */
		public void createEmpMonthlySummary(EmployeeBaseEntity emp, AttWorkingTypeBaseEntity wkType, ComDeptBase__c dept, AppDate startDate, Boolean createPreNext) {
			Id deptId = dept == null ? null : getDeptHistId(dept.id);
			testData.createAttSummaryWithRecordsMonth(emp.getHistory(0).id,
					wkType.getHistory(0).id, deptId, startDate, createPreNext);
		}

		/**
		 * 部署データを作成する
		 * @param  部署名
		 */
		public ComDeptBase__c createDept(String name) {
			return ComTestDataUtility.createDepartmentWithHistory(name, testData.company.Id);
		}

		/**
		 * 勤務体系データを作成する
		 * @param 労働時間制
		 */
		public AttWorkingTypeBaseEntity createWorkingType(AttWorkSystem wkType) {
			return testData.createWorkingType(wkType);
		}

		/**
		 * 部署履歴IDを取得する
		 * @param 部署ベースID
		 */
		public Id getDeptHistId(Id id){
			return [SELECT BaseId__c, Id FROM ComDeptHistory__c where BaseId__c = :id][0].id;
		}
	}

	/**
	 * searchSummaryRequestRecordのテスト
	 * 他部署・他月度がfilterされることを確認する
	 * 各項目に正しい値がセットされていることを確認する
	 */
	@isTest
	static void searchSummaryRequestRecordTest() {
		TeamAttendanceSummaryService testTargetService = new TeamAttendanceSummaryService();
		// テストデータ作成
		TestData testData = new TestData(AttWorkSystem.JP_FIX);
		AttWorkingTypeBaseEntity anotherWkType = testData.createWorkingType(AttWorkSystem.JP_Discretion);
		ComDeptBase__c anotherDept = testData.createDept('他の部署');
		// 社員の作成
		List<EmployeeBaseEntity> emplist = testData.testData.createTestEmployees(10);
		for(Integer i = 5; i < 10; i++) {
			emplist.get(i).getHistory(0).workingTypeId = anotherWkType.Id;
		}
		for(Integer j = 8; j < 10; j++) {
			emplist.get(j).getHistory(0).departmentId = anotherDept.id;
		}
		new EmployeeRepository().saveEntityList(emplist);

		Map<Id, AttWorkingTypeBaseEntity> wkTypeMap = testTargetService.getWorkingTypeMap(
				new Set<Id>{testData.workingType.id, anotherWkType.id}, true);

		// 勤怠サマリの作成
		for (EmployeeBaseEntity emp : emplist) {
			ComDeptBase__c deptBase;
			if(emp.getHistory(0).departmentId == testData.dept.id) {
				deptBase = testData.dept;
			} else if(emp.getHistory(0).departmentId == anotherDept.id) {
				deptBase = anotherDept;
			}
			testData.createEmpMonthlySummary(emp, wkTypeMap.get(emp.getHistory(0).workingTypeId),
					deptBase, AppDate.newInstance(2018, 11, 1), true);
		}

		// 検索条件設定
		TeamAttendanceSummaryService.SummaryRequestSearchFilter filter
				= new TeamAttendanceSummaryService.SummaryRequestSearchFilter();
		filter.targetYear = Integer.valueOf('2018');
		filter.targetMonthly = Integer.valueOf('11');
		filter.departmentIdList = new List<Id>{ testData.dept.id };
		filter.isSearchEmployeeWithoutDepartment = false;

		//検索実行
		Test.startTest();
		List<TeamAttendanceSummaryService.SummaryRequestRecord> searchResultList = testTargetService.searchSummaryRequestRecord(filter);
		Test.stopTest();

		// 検証
		System.assertNotEquals(null, searchResultList, 'レコードが存在しない時は空のリストを返す必要があります。');
		// 他部署の人員が取得できていないことを確認する
		System.assertEquals(8, searchResultList.size());

		// 各項目検証
		for (TeamAttendanceSummaryService.SummaryRequestRecord result : searchResultList) {
			System.debug(LoggingLevel.INFO, '検索結果=' + result);

			System.assertEquals(result.employee.id, result.employeeId);
			System.assertEquals(result.employee.code, result.employeeCode);
			System.assertEquals(result.employee.fullNameL.getFullName(), result.employeeName);
			System.assertEquals(result.employee.user.photoUrl, result.photoUrl);
			System.assertEquals(wkTypeMap.get(result.employee.getHistory(0).workingTypeId).getHistory(0).nameL.getValue(), result.workingTypeName);
			System.assertEquals(AppDate.newInstance(2018,11,1), result.startDate);
			System.assertEquals(AppDate.newInstance(2018,11,30), result.endDate);
			System.assertEquals('NotRequested', result.status);
			System.assertEquals('', result.approverName);
		}
	}


	/**
	 * searchSummaryRequestRecordのテスト
	 * 部署無所属の検索が有効かどうかを検証する
	 */
	@isTest
	static void searchSummaryRequestRecordTestNoDept() {
		TeamAttendanceSummaryService testTargetService = new TeamAttendanceSummaryService();
		// テストデータ作成
		TestData testData = new TestData(AttWorkSystem.JP_FIX);
		AttWorkingTypeBaseEntity anotherWkType = testData.createWorkingType(AttWorkSystem.JP_Discretion);
		// 社員の作成
		List<EmployeeBaseEntity> emplist = testData.testData.createTestEmployees(10);
		for(Integer i = 5; i < 10; i++) {
			emplist.get(i).getHistory(0).workingTypeId = anotherWkType.Id;
		}
		for(Integer j = 8; j < 10; j++) {
			emplist.get(j).getHistory(0).departmentId = null;
		}
		new EmployeeRepository().saveEntityList(emplist);

		Map<Id, AttWorkingTypeBaseEntity> wkTypeMap = testTargetService.getWorkingTypeMap(
				new Set<Id>{testData.workingType.id, anotherWkType.id}, true);

		// 勤怠サマリの作成
		for (EmployeeBaseEntity emp : emplist) {
			ComDeptBase__c deptBase;
			if(emp.getHistory(0).departmentId == testData.dept.id) {
				deptBase = testData.dept;
			}
			testData.createEmpMonthlySummary(emp, wkTypeMap.get(emp.getHistory(0).workingTypeId),
					deptBase, AppDate.newInstance(2018, 11, 1), true);
		}

		// 検索条件設定
		TeamAttendanceSummaryService.SummaryRequestSearchFilter filter
				= new TeamAttendanceSummaryService.SummaryRequestSearchFilter();
		filter.targetYear = Integer.valueOf('2018');
		filter.targetMonthly = Integer.valueOf('11');
		// filter.departmentIdList = new List<Id>{ testData.dept.id };// あえて部署ID入れる
		filter.isSearchEmployeeWithoutDepartment = true;
		System.debug(LoggingLevel.INFO, 'filter=' + filter);

		//検索実行
		List<TeamAttendanceSummaryService.SummaryRequestRecord> searchResultList = testTargetService.searchSummaryRequestRecord(filter);

		// 検証
		System.assertNotEquals(null, searchResultList, 'レコードが存在しない時は空のリストを返す必要があります。');
		// 入れた部署が取得できてなければ2件になる
		System.assertEquals(2, searchResultList.size());

		// 各項目検証
		for (TeamAttendanceSummaryService.SummaryRequestRecord result : searchResultList) {
			System.debug(LoggingLevel.INFO, '検索結果=' + result);

			System.assertEquals(result.employee.id, result.employeeId);
			System.assertEquals(result.employee.code, result.employeeCode);
			System.assertEquals(result.employee.fullNameL.getFullName(), result.employeeName);
			System.assertEquals(result.employee.user.photoUrl, result.photoUrl);
			System.assertEquals(wkTypeMap.get(result.employee.getHistory(0).workingTypeId).getHistory(0).nameL.getValue(), result.workingTypeName);
			System.assertEquals(AppDate.newInstance(2018,11,1), result.startDate);
			System.assertEquals(AppDate.newInstance(2018,11,30), result.endDate);
			System.assertEquals('NotRequested', result.status);
			System.assertEquals('', result.approverName);
		}
	}

	/**
	 * SummaryRequestRecordの新規作成テスト
	 * 勤怠サマリー・勤務体系マスタ・社員マスタのいづれかがない場合にエラーが発生することを確認する
	 * 勤怠サマリー・勤務体系マスタ・社員マスタ全てがある場合に正常終了することを確認する
	 */
	@isTest
	static void newSummaryRequestRecordTest() {
		// テストデータ作成
		TestData testData = new TestData(AttWorkSystem.JP_FIX);
		testData.createEmpMonthlySummary(testData.employee, testData.workingType,
					null, AppDate.newInstance(2018, 11, 1), true);

		App.RecordNotFoundException notFoundEx;
		// ケース1: 勤怠サマリがnullの場合にエラーが発生することを確認する
		try {
			TeamAttendanceSummaryService.SummaryRequestRecord summaryReq =
					new TeamAttendanceSummaryService.SummaryRequestRecord(null, testData.employee, testData.workingType, new PersonalSettingEntity());
		} catch (App.RecordNotFoundException e) {
			notFoundEx = e;
			System.assertEquals('RECORD_NOT_FOUND', e.getErrorCode());
		}
		System.assertNotEquals(null, notFoundEx, 'App.RecordNotFoundExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, notFoundEx.getMessage());

		// ケース2: 社員マスタがnullの場合にエラーが発生することを確認する
		try {
			TeamAttendanceSummaryService.SummaryRequestRecord summaryReq =
					new TeamAttendanceSummaryService.SummaryRequestRecord(new AttSummaryEntity(), null, testData.workingType, new PersonalSettingEntity());
		} catch (App.RecordNotFoundException e) {
			notFoundEx = e;
			System.assertEquals('RECORD_NOT_FOUND', e.getErrorCode());
		}
		System.assertNotEquals(null, notFoundEx, 'App.RecordNotFoundExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, notFoundEx.getMessage());

		// ケース3: 勤務体系マスタがnullの場合にエラーが発生することを確認する
		try {
			TeamAttendanceSummaryService.SummaryRequestRecord summaryReq =
					new TeamAttendanceSummaryService.SummaryRequestRecord(new AttSummaryEntity(), testData.employee, null, new PersonalSettingEntity());
		} catch (App.RecordNotFoundException e) {
			notFoundEx = e;
			System.assertEquals('RECORD_NOT_FOUND', e.getErrorCode());
		}
		System.assertNotEquals(null, notFoundEx, 'App.RecordNotFoundExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, notFoundEx.getMessage());

		// ケース4: 全ての項目がある場合に正常終了することを確認する
		try {
			TeamAttendanceSummaryService.SummaryRequestRecord summaryReq =
					new TeamAttendanceSummaryService.SummaryRequestRecord(new AttSummaryEntity(), testData.employee, testData.workingType, null);
		} catch (App.RecordNotFoundException e) {
			TestUtil.fail('RecordNotFoundExceptionが発生しました'+e.getMessage()+e.getStackTraceString());
		}
	}

	/**
	 * searchSummaryRequestRecordのテスト
	 * 勤務確定申請が未申請の場合
	 * 承認者が正しく取得できることを確認する
	 */
	@isTest
	static void searchSummaryRequestRecordTestNotRequest() {
		TeamAttendanceSummaryService testTargetService = new TeamAttendanceSummaryService();
		// テストデータ作成
		TestData testData = new TestData(AttWorkSystem.JP_FIX);
		AttWorkingTypeBaseEntity anotherWkType = testData.createWorkingType(AttWorkSystem.JP_Discretion);
		List<EmployeeBaseEntity> allEmployeeList = testData.testData.createTestEmployees(10);
		// 社員の作成
		EmployeeBaseEntity emp1 = allEmployeeList[0];  // 承認者は社員の承認者01
		EmployeeBaseEntity emp2 = allEmployeeList[1];  // 承認者は個人設定の承認者01
		EmployeeBaseEntity emp3 = allEmployeeList[2];  // 個人設定の承認者01は失効している社員
		List<EmployeeBaseEntity> emplist = new List<EmployeeBaseEntity>{emp1, emp2, emp3};
		// 承認者の作成
		EmployeeBaseEntity appr1 = allEmployeeList[5];
		EmployeeBaseEntity appr2 = allEmployeeList[6];
		EmployeeBaseEntity expriredAppr = allEmployeeList[7];
		expriredAppr.getHistory(0).validTo = AppDate.newInstance(2018, 11, 30);
		expriredAppr.getHistory(0).validFrom = expriredAppr.getHistory(0).validTo.addMonths(-2);
		Map<Id, AttWorkingTypeBaseEntity> wkTypeMap = testTargetService.getWorkingTypeMap(
				new Set<Id>{testData.workingType.id, anotherWkType.id}, true);

		// 承認者01の設定
		emp1.code = 'E001';
		emp1.getHistory(0).approverBase01Id = appr1.id;
		// 2人目は個人設定を作成しておく
		emp2.code = 'E002';
		emp2.getHistory(0).approverBase01Id = appr1.id;
		PersonalSettingEntity emp2Setting = new PersonalSettingEntity();
		emp2Setting.approverBase01Id = appr2.id;
		emp2Setting.employeeBaseId = emp2.id;
		// 3人目の個人設定には失効している社員を設定する
		emp3.code = 'E003';
		emp3.getHistory(0).approverBase01Id = appr1.id;
		PersonalSettingEntity emp3Setting = new PersonalSettingEntity();
		emp3Setting.approverBase01Id = expriredAppr.id;
		emp3Setting.employeeBaseId = emp3.id;
		new PersonalSettingRepository().saveEntityList(new List<PersonalSettingEntity>{emp2Setting, emp3Setting});
		new EmployeeRepository().saveEntityList(allEmployeeList);

		// 勤怠サマリの作成
		for (EmployeeBaseEntity emp : emplist) {
			ComDeptBase__c deptBase;
			if(emp.getHistory(0).departmentId == testData.dept.id) {
				deptBase = testData.dept;
			}
			testData.createEmpMonthlySummary(emp, wkTypeMap.get(emp.getHistory(0).workingTypeId),
					deptBase, AppDate.newInstance(2018, 11, 1), true);
		}

		// 検索条件設定
		TeamAttendanceSummaryService.SummaryRequestSearchFilter filter
				= new TeamAttendanceSummaryService.SummaryRequestSearchFilter();
		filter.targetYear = Integer.valueOf('2018');
		filter.targetMonthly = Integer.valueOf('11');
		filter.departmentIdList = new List<Id>{ testData.dept.id };
		filter.isSearchEmployeeWithoutDepartment = false;

		//検索実行
		Test.startTest();
		List<TeamAttendanceSummaryService.SummaryRequestRecord> searchResultList = testTargetService.searchSummaryRequestRecord(filter);
		Test.stopTest();

		// 検証
		System.assertEquals(3, searchResultList.size());

		// 承認者の検証
		System.assertEquals(emp1.id, searchResultList[0].employeeId, searchResultList[0].employeeCode);
		System.assertEquals(appr1.fullNameL.getFullName(), searchResultList[0].approverName);
		System.assertEquals(emp2.id, searchResultList[1].employeeId, searchResultList[1].employeeCode);
		System.assertEquals(appr2.fullNameL.getFullName(), searchResultList[1].approverName);
		// emp3の個人設定の承認者01は失効しているため、社員の承認者01の名前が取得できているはず
		System.assertEquals(emp3.id, searchResultList[2].employeeId, searchResultList[2].employeeCode);
		System.assertEquals(appr1.fullNameL.getFullName(), searchResultList[2].approverName);
	}

	/**
	 * searchSummaryRequestRecordのテスト
	 * 勤怠サマリー作成後にマスタが改定されている場合、改定後のマスタを参照していることを確認する
	 */
	@isTest
	static void searchSummaryRequestRecordTestMasterRevision() {
		TeamAttendanceSummaryService testTargetService = new TeamAttendanceSummaryService();
		// テストデータ作成
		TestData testData = new TestData(AttWorkSystem.JP_FIX);
		AttWorkingTypeBaseEntity anotherWkType = testData.createWorkingType(AttWorkSystem.JP_Discretion);
		List<EmployeeBaseEntity> allEmployeeList = testData.testData.createTestEmployees(10);
		// 社員の作成
		EmployeeBaseEntity emp1 = allEmployeeList[0];
		EmployeeBaseEntity emp2 = allEmployeeList[1];
		List<EmployeeBaseEntity> emplist = new List<EmployeeBaseEntity>{emp1, emp2};
		// 承認者の作成
		EmployeeBaseEntity appr1 = allEmployeeList[5];
		EmployeeBaseEntity appr2 = allEmployeeList[6];
		EmployeeBaseEntity appr3 = allEmployeeList[6];
		Map<Id, AttWorkingTypeBaseEntity> wkTypeMap = testTargetService.getWorkingTypeMap(
				new Set<Id>{testData.workingType.id, anotherWkType.id}, true);

		// 1人目(emp1)は勤怠サマリー作成後に社員を改定して承認者を変更する
		emp1.code = 'E001';
		emp1.getHistory(0).approverBase01Id = appr1.id;

		// 2人目(emp2)は勤怠サマリー作成後に社員が参照している勤務体系を改定して、承認者選択フラグをOFFにする
		emp2.code = 'E002';
		emp2.getHistory(0).approverBase01Id = appr1.id;
		emp2.getHistory(0).workingTypeId = anotherWkType.id;
		PersonalSettingEntity emp2Setting = new PersonalSettingEntity();
		emp2Setting.approverBase01Id = appr2.id;
		emp2Setting.employeeBaseId = emp2.id;

		new PersonalSettingRepository().saveEntity(emp2Setting);
		new EmployeeRepository().saveEntityList(allEmployeeList);

		// 勤怠サマリの作成
		for (EmployeeBaseEntity emp : emplist) {
			ComDeptBase__c deptBase;
			if(emp.getHistory(0).departmentId == testData.dept.id) {
				deptBase = testData.dept;
			}
			testData.createEmpMonthlySummary(emp, wkTypeMap.get(emp.getHistory(0).workingTypeId),
					deptBase, AppDate.newInstance(2018, 11, 1), true);
		}

		// emp1の社員を改定する(同日改定)
		EmployeeHistoryEntity newEmp1History = emp1.getHistory(0).copy();
		newEmp1History.approverBase01Id = appr3.id;
		newEmp1History.uniqKey = 'revision';
		newEmp1History.isRemoved = false;
		emp1.getHistory(0).isRemoved = true;
		emp1.addHistory(newEmp1History);
		new EmployeeRepository().saveEntity(emp1);

		// emp2が参照している勤務体系を改定する
		AttWorkingTypeHistoryEntity newAnotherWkTypeHistory = anotherWkType.getHistory(0).copy();
		newAnotherWkTypeHistory.nameL0 = newAnotherWkTypeHistory.nameL0 + 'revision';
		newAnotherWkTypeHistory.nameL1 = newAnotherWkTypeHistory.nameL1 + 'revision';
		newAnotherWkTypeHistory.nameL2 = newAnotherWkTypeHistory.nameL2 + 'revision';
		newAnotherWkTypeHistory.allowToChangeApproverSelf = false;
		newAnotherWkTypeHistory.uniqKey = 'revision';
		newAnotherWkTypeHistory.isRemoved = false;
		anotherWkType.getHistory(0).isRemoved = true;
		anotherWkType.addHistory(newAnotherWkTypeHistory);
		new AttWorkingTypeRepository().saveEntity(anotherWkType);

		// 検索条件設定
		TeamAttendanceSummaryService.SummaryRequestSearchFilter filter
				= new TeamAttendanceSummaryService.SummaryRequestSearchFilter();
		filter.targetYear = Integer.valueOf('2018');
		filter.targetMonthly = Integer.valueOf('11');
		filter.departmentIdList = new List<Id>{ testData.dept.id };
		filter.isSearchEmployeeWithoutDepartment = false;

		//検索実行
		Test.startTest();
		List<TeamAttendanceSummaryService.SummaryRequestRecord> searchResultList = testTargetService.searchSummaryRequestRecord(filter);
		Test.stopTest();

		// 検証
		System.assertEquals(2, searchResultList.size());

		// 承認者の検証
		System.assertEquals(emp1.id, searchResultList[0].employeeId, searchResultList[0].employeeCode);
		System.assertEquals(appr3.fullNameL.getFullName(), searchResultList[0].approverName);
		System.assertEquals(emp2.id, searchResultList[1].employeeId, searchResultList[1].employeeCode);
		System.assertEquals(newAnotherWkTypeHistory.nameL.getValue(), searchResultList[1].workingTypeName);
		System.assertEquals(appr1.fullNameL.getFullName(), searchResultList[1].approverName);
	}

	/**
	 * searchSummaryRequestRecordのテスト
	 * 退職済みの社員のサマリーは取得できないことを確認する
	 */
	@isTest
	static void searchSummaryRequestRecordTestExpiredEmployee() {
		TeamAttendanceSummaryService testTargetService = new TeamAttendanceSummaryService();
		// テストデータ作成
		TestData testData = new TestData(AttWorkSystem.JP_FIX);
		AttWorkingTypeBaseEntity anotherWkType = testData.createWorkingType(AttWorkSystem.JP_Discretion);
		List<EmployeeBaseEntity> allEmployeeList = testData.testData.createTestEmployees(10);
		// 社員の作成
		EmployeeBaseEntity emp1 = allEmployeeList[0];
		EmployeeBaseEntity emp2 = allEmployeeList[1];
		List<EmployeeBaseEntity> emplist = new List<EmployeeBaseEntity>{emp1, emp2};
		// 承認者の作成
		EmployeeBaseEntity appr1 = allEmployeeList[5];
		EmployeeBaseEntity appr2 = allEmployeeList[6];
		EmployeeBaseEntity appr3 = allEmployeeList[6];
		Map<Id, AttWorkingTypeBaseEntity> wkTypeMap = testTargetService.getWorkingTypeMap(
				new Set<Id>{testData.workingType.id, anotherWkType.id}, true);

		// 勤怠サマリの作成
		for (EmployeeBaseEntity emp : emplist) {
			ComDeptBase__c deptBase;
			if(emp.getHistory(0).departmentId == testData.dept.id) {
				deptBase = testData.dept;
			}
			testData.createEmpMonthlySummary(emp, wkTypeMap.get(emp.getHistory(0).workingTypeId),
					deptBase, AppDate.newInstance(2018, 11, 1), true);
		}

		// 1人目(emp1)
		emp1.code = 'E001';
		emp1.getHistory(0).approverBase01Id = appr1.id;

		// 2人目(emp2)は退職済みに変更する
		emp2.code = 'E002';
		emp2.getHistory(0).approverBase01Id = appr1.id;
		emp2.getHistory(0).workingTypeId = anotherWkType.id;
		emp2.getHistory(0).isRemoved = true;
		EmployeeHistoryEntity newEmp2History = emp2.getHistory(0).copy();
		newEmp2History.validTo = AppDate.newInstance(2018, 10, 31);
		newEmp2History.validFrom = newEmp2History.validTo.addMonths(-1);
		newEmp2History.uniqKey = 'revision';
		newEmp2History.isRemoved = false;
		emp2.addHistory(newEmp2History);

		new EmployeeRepository().saveEntityList(allEmployeeList);


		// 検索条件設定
		TeamAttendanceSummaryService.SummaryRequestSearchFilter filter
				= new TeamAttendanceSummaryService.SummaryRequestSearchFilter();
		filter.targetYear = Integer.valueOf('2018');
		filter.targetMonthly = Integer.valueOf('11');
		filter.departmentIdList = new List<Id>{ testData.dept.id };
		filter.isSearchEmployeeWithoutDepartment = false;

		//検索実行
		Test.startTest();
		List<TeamAttendanceSummaryService.SummaryRequestRecord> searchResultList = testTargetService.searchSummaryRequestRecord(filter);
		Test.stopTest();

		// 検証
		// emp2のサマリーは取得できない
		System.assertEquals(1, searchResultList.size());
		System.assertEquals(emp1.id, searchResultList[0].employeeId, searchResultList[0].employeeCode);
	}
}