/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description CalendarResourceのテストクラス
*/
@isTest
private class CalendarResourceTest {

	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

		public ComOrgSetting__c setting;

		private CalendarService calService;

		/**
		 * コンストラクタ
		 * 標準ユーザーと設定を作成する
		 */
		public ResourceTestData(){
			super();
			setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			calService = new CalendarService();
		}

		/**
		 * 初期値のカレンダーを作成する
		 */
		public ComCalendar__c createDefCalendar() {
			// 会社のデフォルトカレンダーを作成
			Id calendarId = calService.createCompanyDefaultCalendar(company.Id, company.code, CalendarType.ATTENDANCE);
			// 作成したカレンダーを会社に紐づけ
			company.calendarId = calendarId;
			ComCompany__c companySobj = new CompanyRepository().createSObject(company);
			update companySobj;
			return getCalendar(calendarId);
		}

		public ComCalendar__c getCalendar(Id calendarId){
			return [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CompanyId__c,
				Type__c,
				Remarks__c
			FROM ComCalendar__c
			WHERE Id = :calendarId];
		}
	}

	/**
	 * カレンダー登録APIのテスト
	 */
	@isTest static void createApiTest() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.Calendar param = new CalendarResource.Calendar();
		param.code = 'TestCalender';
		param.name_L0 = 'テストカレンダーL0';
		param.name_L1 = 'TestCalendarL1';
		param.name_L2 = 'TestCalendarL2';
		param.companyId = company.Id;
		param.type = 'Attendance';
		param.remarks = '更新テスト';

		// API実行
		CalendarResource.CreateApi api = new CalendarResource.CreateApi();
		CalendarResource.SaveResult result = (CalendarResource.SaveResult)api.execute(param);

		Test.stopTest();

		ComCalendar__c calendar = [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CompanyId__c,
				Type__c,
				Remarks__c
			FROM ComCalendar__c
			WHERE Id = :result.id];

		System.assertNotEquals(null, calendar);
		System.assertEquals(param.code, calendar.Code__c);
		System.assertEquals(param.name_L0, calendar.Name);
		System.assertEquals(param.name_L0, calendar.Name_L0__c);
		System.assertEquals(param.name_L1, calendar.Name_L1__c);
		System.assertEquals(param.name_L2, calendar.Name_L2__c);
		System.assertEquals(param.companyId, calendar.CompanyId__c);
		System.assertEquals(param.type, calendar.Type__c);
		System.assertEquals(param.remarks, calendar.Remarks__c);
	}

	/**
	 * カレンダー作成APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void createApiTestValidation() {
		final Boolean isUpdate = false;
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 会社IDが未指定
		CalendarResource.Calendar param1 = new CalendarResource.Calendar();
		param1.companyId = null;
		param1.type = 'Attendance';
		param1.code = 'TestCode';
		param1.name_L0 = 'Test L0';
		try {
			param1.validate(isUpdate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 会社IDが不正
		CalendarResource.Calendar param2 = new CalendarResource.Calendar();
		param2.companyId = 'test';
		param2.type = 'Attendance';
		param2.code = 'TestCode';
		param2.name_L0 = 'Test L0';
		try {
			param2.validate(isUpdate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダーコードが未指定
		CalendarResource.Calendar param3 = new CalendarResource.Calendar();
		param3.companyId = 'test';
		param3.type = 'Attendance';
		param3.code = null;
		param3.name_L0 = 'Test L0';
		try {
			param3.validate(true);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダー名(L0)が不正
		CalendarResource.Calendar param4 = new CalendarResource.Calendar();
		param4.companyId = 'test';
		param4.type = 'Attendance';
		param4.code = '001';
		param4.name_L0 = null;
		try {
			param4.validate(isUpdate);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダータイプが未指定
		CalendarResource.Calendar param5= new CalendarResource.Calendar();
		param5.companyId = company.Id;
		param5.type = '';
		param5.code = 'TestCode';
		param5.name_L0 = 'Test L0';
		try {
			param5.validate(isUpdate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * カレンダー登録APIのテスト
	 * カレンダーの管理権限を持っていない管理者がカレンダー登録ができないことを確認する
	 */
	@isTest static void createApiTestNoPermission() {
		//テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// カレンダーの管理権限を無効に設定する
		testData.permission.isManageCalendar = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		CalendarResource.Calendar param = new CalendarResource.Calendar();
		param.code = 'TestCalender';
		param.name_L0 = 'テストカレンダーL0';
		param.name_L1 = 'TestCalendarL1';
		param.name_L2 = 'TestCalendarL2';
		param.companyId = testData.company.Id;
		param.type = 'Attendance';
		param.remarks = '権限テスト';

		// API実行・検証
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Test.startTest();

				CalendarResource.CreateApi api = new CalendarResource.CreateApi();
				api.execute(param);
				Test.stopTest();
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * カレンダー更新APIのテスト
	 */
	@isTest static void updateApiTest() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 会社のデフォルトカレンダーを作成
		CalendarService calService = new CalendarService();
		Id calendarId = calService.createCompanyDefaultCalendar(company.Id, company.Code__c, CalendarType.ATTENDANCE);

		// 作成したカレンダーを会社に紐づけ
		company.CalendarId__c = calendarId;
		update company;

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.Calendar param = new CalendarResource.Calendar();
		param.id = calendarId;
		param.code = 'update';
		param.name_L0 = 'テストカレンダーL0';
		param.name_L1 = 'TestCalendarL1';
		param.name_L2 = 'TestCalendarL2';
		param.remarks = '更新テスト';

		// API実行
		CalendarResource.UpdateApi api = new CalendarResource.UpdateApi();
		api.execute(param);

		Test.stopTest();

		ComCalendar__c calendar = [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				Remarks__c
			FROM ComCalendar__c
			WHERE Id = :calendarId];

		System.assertEquals(param.id, calendar.Id);
		System.assertEquals(param.code, calendar.Code__c);
		System.assertEquals(param.name_L0, calendar.Name);
		System.assertEquals(param.name_L0, calendar.Name_L0__c);
		System.assertEquals(param.name_L1, calendar.Name_L1__c);
		System.assertEquals(param.name_L2, calendar.Name_L2__c);
		System.assertEquals(param.remarks, calendar.Remarks__c);
	}

	/**
	 * カレンダー更新APIのテスト
	 * バリデーションが正しく行われることを確認すyy
	 */
	@isTest static void updateApiTestValidation() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 会社のデフォルトカレンダーを作成
		CalendarService calService = new CalendarService();
		Id calendarId = calService.createCompanyDefaultCalendar(company.Id, company.Code__c, CalendarType.ATTENDANCE);

		// 作成したカレンダーを会社に紐づけ
		company.CalendarId__c = calendarId;
		update company;

		Test.startTest();

		// カレンダーIDが未指定
		CalendarResource.Calendar param1 = new CalendarResource.Calendar();
		param1.id = null;
		try {
			param1.validate(true);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダーIDが不正
		CalendarResource.Calendar param2 = new CalendarResource.Calendar();
		param2.id = 'test';
		try {
			param2.validate(true);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダーコードが未指定
		CalendarResource.Calendar param3 = new CalendarResource.Calendar();
		param3.id = calendarId;
		param3.code = null;
		try {
			param3.validate(true);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダー名(L0)が不正
		CalendarResource.Calendar param4 = new CalendarResource.Calendar();
		param4.id = calendarId;
		param4.code = '001';
		param4.name_L0 = null;
		try {
			param4.validate(true);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー更新APIのテスト
	 * カレンダーの管理権限を持っていない管理者がカレンダー削除ができないことを確認する
	 */
	@isTest static void updateApiTestNoPermission() {
		//テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		ComCalendar__c updateCalendar = testData.createDefCalendar();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// カレンダーの管理権限を無効に設定する
		testData.permission.isManageCalendar = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		CalendarResource.Calendar param = new CalendarResource.Calendar();
		param.id = updateCalendar.Id;
		param.code = 'update';
		param.name_L0 = 'テストカレンダーL0';
		param.name_L1 = 'TestCalendarL1';
		param.name_L2 = 'TestCalendarL2';
		param.remarks = '権限テスト';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Test.startTest();
				CalendarResource.UpdateApi api = new CalendarResource.UpdateApi();
				api.execute(param);
				Test.stopTest();
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		//検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * カレンダー検索APIのテスト
	 */
	@isTest static void searchApiTest() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');

		// 会社のデフォルトカレンダーを作成
		CalendarService calService = new CalendarService();
		Id calendarId = calService.createCompanyDefaultCalendar(company.Id, company.Code__c, CalendarType.ATTENDANCE);

		// 作成したカレンダーを会社に紐づけ
		company.CalendarId__c = calendarId;
		update company;

		Test.startTest();

		CalendarResource.SearchResponse res;

		System.runAs(user) {
			// リクエストパラメータ
			CalendarResource.SearchRequest param = new CalendarResource.SearchRequest();
			param.companyId = company.Id;
			param.types = new List<String>{CalendarType.ATTENDANCE.value};

			// API実行
			CalendarResource.SearchApi api = new CalendarResource.SearchApi();
			res = (CalendarResource.SearchResponse)api.execute(param);
		}

		Test.stopTest();

		ComCalendar__c calendar = [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				Type__c,
				CompanyId__c,
				Remarks__c
			FROM ComCalendar__c
			WHERE Id = :calendarId];

		System.assertEquals(1, res.records.size());
		System.assertEquals(calendar.Id, res.records[0].id);
		System.assertEquals(calendar.Code__c, res.records[0].code);
		System.assertEquals(calendar.Name_L0__c, res.records[0].name);
		System.assertEquals(calendar.Name_L0__c, res.records[0].name_L0);
		System.assertEquals(calendar.Name_L1__c, res.records[0].name_L1);
		System.assertEquals(calendar.Name_L2__c, res.records[0].name_L2);
		System.assertEquals(calendar.CompanyId__c, res.records[0].companyId);
		System.assertEquals(calendar.Type__c, res.records[0].type);
		System.assertEquals(true, res.records[0].isDefault);
		System.assertEquals(calendar.Remarks__c, res.records[0].remarks);
	}

	/**
	 * カレンダー検索APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void searchApiTestValidation() {
		Test.startTest();

		// カレンダーIDが不正
		CalendarResource.SearchRequest param1 = new CalendarResource.SearchRequest();
		param1.companyId = 'test';
		try {
			param1.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 会社IDが不正
		CalendarResource.SearchRequest param2 = new CalendarResource.SearchRequest();
		param2.id = 'test';
		try {
			param2.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダータイプが不正
		CalendarResource.SearchRequest param3 = new CalendarResource.SearchRequest();
		param3.types = new List<String>{'test'};
		try {
			param3.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー検索APIのテスト
	 * 指定した会社が存在しない場合に例外が発生することを確認する
	 */
	@isTest static void searchApiTestCompanyNotFound() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 会社のデフォルトカレンダーを作成
		CalendarService calService = new CalendarService();
		Id calendarId = calService.createCompanyDefaultCalendar(company.Id, company.Code__c, CalendarType.ATTENDANCE);

		// 作成したカレンダーを会社に紐づけ
		company.CalendarId__c = calendarId;
		update company;

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.SearchRequest param = new CalendarResource.SearchRequest();
		param.id = company.Id;

		// API実行
		try {
			CalendarResource.SearchApi api = new CalendarResource.SearchApi();
			CalendarResource.SearchResponse res = (CalendarResource.SearchResponse)api.execute(param);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Company}), e.getMessage());

		}

		Test.stopTest();
	}

	/**
	 * カレンダー削除APIのテスト
	 * 指定したカレンダーが論理削除できることを確認する
	 */
	@isTest static void deleteApiTest() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 削除対象のカレンダーを作成
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('DeleteTest', company.Id);

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.DeleteRequest param = new CalendarResource.DeleteRequest();
		param.id = calendar.Id;

		// API実行
		CalendarResource.DeleteApi api = new CalendarResource.DeleteApi();
		api.execute(param);

		Test.stopTest();

		List<ComCalendar__c> resCalenderList = [
			SELECT
				Id,
				Removed__c
			FROM ComCalendar__c
			WHERE Id = :param.id];

		System.assertEquals(1, resCalenderList.size());
		System.assertEquals(true, resCalenderList[0].Removed__c);
	}

	/**
	 * カレンダー削除APIのテスト
	 * 指定したカレンダーが削除済みの場合、正常に処理が完了することを確認する
	 */
	@isTest static void deleteApiTestDeletedCalendar() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 削除対象のカレンダーを作成
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('DeleteTest', company.Id);

		// 対象のカレンダーを削除しておく
		delete calendar;

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.DeleteRequest param = new CalendarResource.DeleteRequest();
		param.id = calendar.Id;

		// API実行
		CalendarResource.DeleteApi api = new CalendarResource.DeleteApi();
		api.execute(param);

		Test.stopTest();

		List<ComCalendar__c> resCalenderList = [
			SELECT
				Id
			FROM ComCalendar__c
			WHERE Id = :param.id];

		System.assertEquals(true, resCalenderList.isEmpty());
	}

	/**
	 * カレンダー削除APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void deleteApiTestValidation() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 削除対象のカレンダーを作成
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('DeleteTest', company.Id);

		// カレンダーIDが設定されていない
		CalendarResource.DeleteRequest param1 = new CalendarResource.DeleteRequest();
		param1.id = null;
		try {
			param1.validate();
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダーIDにID以外の文字列が設定されている
		CalendarResource.DeleteRequest param2 = new CalendarResource.DeleteRequest();
		param2.id = 'test';
		try {
			param2.validate();
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * カレンダー削除APIのテスト
	 * カレンダーの管理権限を持っていない管理者がカレンダー削除ができないことを確認する
	 */
	@isTest static void deleteApiTestNoPermission() {
		//テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// カレンダーの管理権限を無効に設定する
		testData.permission.isManageCalendar = false;
		new PermissionRepository().saveEntity(testData.permission);
		// 削除対象のカレンダーを作成
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('DeleteTest', testData.company.Id);

		// リクエストパラメータ
		CalendarResource.DeleteRequest param = new CalendarResource.DeleteRequest();
		param.id = calendar.Id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Test.startTest();
				CalendarResource.DeleteApi api = new CalendarResource.DeleteApi();
				api.execute(param);
				Test.stopTest();
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		//検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * カレンダー明細作成APIのテスト
	 */
	@isTest static void createRecordApiTest() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.CalendarRecord param = new CalendarResource.CalendarRecord();
		param.name_L0 = '明細';
		param.name_L1 = 'record';
		param.name_L2 = 'meisai';
		param.recordDate = '2018-04-01';
		param.calendarId = calendar.Id;
		param.dayType = 'Holiday';
		param.remarks = '備考';

		// API実行
		CalendarResource.CreateRecordApi api = new CalendarResource.CreateRecordApi();
		CalendarResource.SaveResult res = (CalendarResource.SaveResult)api.execute(param);

		Test.stopTest();

		ComCalendarRecord__c record = [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Date__c,
				CalendarId__c,
				AttDayType__c,
				Remarks__c
			FROM ComCalendarRecord__c
			WHERE CalendarId__c = :calendar.Id];

		System.assertEquals(record.Id, res.id);
		System.assertEquals(param.name_L0, record.Name);
		System.assertEquals(param.name_L0, record.Name_L0__c);
		System.assertEquals(param.name_L1, record.Name_L1__c);
		System.assertEquals(param.name_L2, record.Name_L2__c);
		System.assertEquals(Date.valueOf(param.recordDate), record.Date__c);
		System.assertEquals(param.calendarId, record.CalendarId__c);
		System.assertEquals(param.dayType, record.AttDayType__c);
		System.assertEquals(param.remarks, record.Remarks__c);
	}

	/**
	 * カレンダー明細作成APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void createRecordApiTestValidation() {
		Test.startTest();

		// カレンダー明細名(L0)が未指定
		CalendarResource.CalendarRecord param1 = new CalendarResource.CalendarRecord();
		param1.name_L0 = null;
		try {
			param1.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 日付が未指定
		CalendarResource.CalendarRecord param2 = new CalendarResource.CalendarRecord();
		param2.name_L0 = '明細';
		param2.recordDate = null;
		try {
			param2.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// 日付が不正
		CalendarResource.CalendarRecord param3 = new CalendarResource.CalendarRecord();
		param3.name_L0 = '明細';
		param3.recordDate = 'test';
		try {
			param3.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダーIDが未指定
		CalendarResource.CalendarRecord param4 = new CalendarResource.CalendarRecord();
		param4.name_L0 = '明細';
		param4.recordDate = '2018-04-01';
		param4.calendarId = null;
		try {
			param4.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// カレンダーIDが不正
		CalendarResource.CalendarRecord param5 = new CalendarResource.CalendarRecord();
		param5.name_L0 = '明細';
		param5.recordDate = '2018-04-01';
		param5.calendarId = 'test';
		try {
			param5.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー明細作成APIのテスト
	 * カレンダーの管理権限を持っていない管理者がカレンダー明細作成ができないことを確認する
	 */
	@isTest static void createRecordApiTestNoPermission() {
		//テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// カレンダーの管理権限を無効に設定する
		testData.permission.isManageCalendar = false;
		new PermissionRepository().saveEntity(testData.permission);
		CalendarEntity calendar = testData.createCalendar('test');

		// リクエストパラメータ
		CalendarResource.CalendarRecord param = new CalendarResource.CalendarRecord();
		param.name_L0 = '明細';
		param.name_L1 = 'record';
		param.name_L2 = 'meisai';
		param.recordDate = '2018-04-01';
		param.calendarId = calendar.Id;
		param.dayType = 'Holiday';
		param.remarks = '備考';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Test.startTest();
				CalendarResource.CreateRecordApi api = new CalendarResource.CreateRecordApi();
				api.execute(param);
				Test.stopTest();
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		//検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * カレンダー明細作成APIのテスト
	 * 指定した日付の明細が既に存在する場合に例外が発生することを確認する
	 */
	@isTest static void searchApiTestDuplicateDate() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.CalendarRecord param = new CalendarResource.CalendarRecord();
		param.name_L0 = '明細';
		param.name_L1 = 'record';
		param.name_L2 = 'meisai';
		param.recordDate = '2018-04-01';
		param.calendarId = calendar.Id;
		param.dayType = 'Holiday';
		param.remarks = '備考';

		// API実行
		try {
			CalendarResource.CreateRecordApi api = new CalendarResource.CreateRecordApi();
			CalendarResource.SaveResult res = (CalendarResource.SaveResult)api.execute(param);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Admin_Err_EventExisted, e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー明細更新APIのテスト
	 */
	@isTest static void updateRecordApiTest() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.CalendarRecord param = new CalendarResource.CalendarRecord();
		param.id = rec1.Id;
		param.name_L0 = '明細';
		param.name_L1 = 'record';
		param.name_L2 = 'meisai';
		param.recordDate = '2018-05-01';
		param.dayType = 'Holiday';
		param.remarks = '備考';

		// API実行
		CalendarResource.UpdateRecordApi api = new CalendarResource.UpdateRecordApi();
		api.execute(param);

		Test.stopTest();

		ComCalendarRecord__c record = [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Date__c,
				CalendarId__c,
				AttDayType__c,
				Remarks__c
			FROM ComCalendarRecord__c
			WHERE CalendarId__c = :calendar.Id AND Removed__c = false];

		System.assertEquals(param.name_L0, record.Name);
		System.assertEquals(param.name_L0, record.Name_L0__c);
		System.assertEquals(param.name_L1, record.Name_L1__c);
		System.assertEquals(param.name_L2, record.Name_L2__c);
		System.assertEquals(Date.valueOf(param.recordDate), record.Date__c);
		System.assertEquals(param.dayType, record.AttDayType__c);
		System.assertEquals(param.remarks, record.Remarks__c);
	}

	/**
	 * カレンダー明細更新APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void updateRecordApiTestValidation() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		Test.startTest();

		// カレンダー明細IDが未指定
		CalendarResource.CalendarRecord param1 = new CalendarResource.CalendarRecord();
		param1.id = null;
		try {
			param1.validate(true);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// カレンダー明細IDが不正
		CalendarResource.CalendarRecord param2 = new CalendarResource.CalendarRecord();
		param2.id = 'test';
		try {
			param2.validate(true);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダー明細名(L0)が未指定
		CalendarResource.CalendarRecord param3 = new CalendarResource.CalendarRecord();
		param3.id = rec1.Id;
		param3.name_L0 = null;
		try {
			param3.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 日付が未指定
		CalendarResource.CalendarRecord param4 = new CalendarResource.CalendarRecord();
		param4.id = rec1.Id;
		param4.name_L0 = '明細';
		param4.recordDate = null;
		try {
			param4.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// 日付が不正
		CalendarResource.CalendarRecord param5 = new CalendarResource.CalendarRecord();
		param5.id = rec1.Id;
		param5.name_L0 = '明細';
		param5.recordDate = 'test';
		try {
			param5.validate(false);
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー明細更新APIのテスト
	 * カレンダーの管理権限を持っていない管理者がカレンダー明細更新ができないことを確認する
	 */
	@isTest static void updateRecordApiTestNoPermission() {
		//テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// カレンダーの管理権限を無効に設定する
		testData.permission.isManageCalendar = false;
		new PermissionRepository().saveEntity(testData.permission);
		CalendarEntity calendar = testData.createCalendar('test');
		//更新対象のカレンダーレコード作成
		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		// リクエストパラメータ
		CalendarResource.CalendarRecord param = new CalendarResource.CalendarRecord();
		param.id = rec1.Id;
		param.name_L0 = '明細';
		param.name_L1 = 'record';
		param.name_L2 = 'meisai';
		param.recordDate = '2018-05-01';
		param.dayType = 'Holiday';
		param.remarks = '備考';

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Test.startTest();
				CalendarResource.UpdateRecordApi api = new CalendarResource.UpdateRecordApi();
				api.execute(param);
				Test.stopTest();
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		//検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * カレンダー明細更新APIのテスト
	 * 指定したカレンダー明細が存在しない場合に例外が発生することを確認する
	 */
	@isTest static void updateRecordApiTestRecordNotFound() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		Id recordId = rec1.Id;

		delete rec1;

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.CalendarRecord param = new CalendarResource.CalendarRecord();
		param.id = recordId;
		param.name_L0 = '明細';
		param.name_L1 = 'record';
		param.name_L2 = 'meisai';
		param.recordDate = '2018-05-01';
		param.dayType = 'Holiday';
		param.remarks = '備考';

		// API実行
		try {
			CalendarResource.UpdateRecordApi api = new CalendarResource.UpdateRecordApi();
			CalendarResource.SaveResult res = (CalendarResource.SaveResult)api.execute(param);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Admin_Lbl_Event}), e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー明細更新APIのテスト
	 * 指定した日付の明細が既に存在する場合に例外が発生することを確認する
	 */
	@isTest static void updateRecordApiTestDuplicateDate() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		targetDate = Date.valueOf('2018-04-02');
		dayType = AttDayType.HOLIDAY;
		ComCalendarRecord__c rec2 =
				ComTestDataUtility.createCalendarRecord('rec2', calendar.Id, targetDate, dayType);

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.CalendarRecord param = new CalendarResource.CalendarRecord();
		param.id = rec1.Id;
		param.name_L0 = '明細';
		param.name_L1 = 'record';
		param.name_L2 = 'meisai';
		param.recordDate = '2018-04-02';
		param.dayType = 'Holiday';
		param.remarks = '備考';

		// API実行
		try {
			CalendarResource.UpdateRecordApi api = new CalendarResource.UpdateRecordApi();
			CalendarResource.SaveResult res = (CalendarResource.SaveResult)api.execute(param);
		} catch (App.IllegalStateException e) {
			System.assertEquals(ComMessage.msg().Admin_Err_EventExisted, e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー明細削除APIのテスト
	 */
	@isTest static void deleteRecordApiTest() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		targetDate = Date.valueOf('2018-04-02');
		dayType = AttDayType.HOLIDAY;
		ComCalendarRecord__c rec2 =
				ComTestDataUtility.createCalendarRecord('rec2', calendar.Id, targetDate, dayType);

		Test.startTest();

		// リクエストパラメータ
		CalendarResource.DeleteRecordRequest param = new CalendarResource.DeleteRecordRequest();
		param.ids = new List<String>{rec1.Id, rec2.Id};

		// API実行
		CalendarResource.DeleteRecordApi api = new CalendarResource.DeleteRecordApi();
		api.execute(param);

		Test.stopTest();

		List<ComCalendarRecord__c> recordList = [
			SELECT
				Id,
				Name,
				Removed__c
			FROM ComCalendarRecord__c
			WHERE Id IN :param.ids];

		System.assertEquals(param.ids.size(), recordList.size());
		for (ComCalendarRecord__c record : recordList) {
			System.assertEquals(true, record.Removed__c);
		}
	}

	/**
	 * カレンダー明細削除APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void deleteRecordApiTestValidation() {
		Test.startTest();

		// カレンダー明細IDが未指定
		CalendarResource.DeleteRecordRequest param1 = new CalendarResource.DeleteRecordRequest();
		param1.ids = null;
		try {
			param1.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}
		// カレンダー明細IDが不正
		CalendarResource.DeleteRecordRequest param2 = new CalendarResource.DeleteRecordRequest();
		param2.ids = new List<String>{'test'};
		try {
			param2.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * カレンダー明細削除APIのテスト
	 * カレンダーの管理権限を持っていない管理者がカレンダー明細削除ができないことを確認する
	 */
	@isTest static void deleteRecordApiTestNoPermission() {
		//テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// カレンダーの管理権限を無効に設定する
		testData.permission.isManageCalendar = false;
		new PermissionRepository().saveEntity(testData.permission);
		CalendarEntity calendar = testData.createCalendar('test');
		//更新対象のカレンダーレコード作成
		Date targetDate = Date.valueOf('2019-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		// リクエストパラメータ
		CalendarResource.DeleteRecordRequest param = new CalendarResource.DeleteRecordRequest();
		param.ids = new List<String>{rec1.Id};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Test.startTest();
				CalendarResource.DeleteRecordApi api = new CalendarResource.DeleteRecordApi();
				api.execute(param);
				Test.stopTest();
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		//検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * カレンダー明細検索APIのテスト
	 */
	@isTest static void searchRecordApiTest() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		targetDate = Date.valueOf('2018-04-02');
		dayType = AttDayType.HOLIDAY;
		ComCalendarRecord__c rec2 =
				ComTestDataUtility.createCalendarRecord('rec2', calendar.Id, targetDate, dayType);
System.debug('>>>> rec1=' + rec1);
System.debug('>>>> rec2=' + rec2);
		Test.startTest();

		CalendarResource.SearchRecordResponse res;

		System.runAs(user) {
			// リクエストパラメータ
			CalendarResource.SearchRecordRequest param = new CalendarResource.SearchRecordRequest();
			param.calendarId = calendar.Id;

			// API実行
			CalendarResource.SearchRecordApi api = new CalendarResource.SearchRecordApi();
			res = (CalendarResource.SearchRecordResponse)api.execute(param);
		}

		Test.stopTest();

		List<ComCalendarRecord__c> recordList = [
			SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Date__c,
				CalendarId__c,
				AttDayType__c,
				Remarks__c
			FROM ComCalendarRecord__c
			WHERE CalendarId__c = :calendar.Id
			ORDER BY Date__c];

		System.assertEquals(2, res.records.size());

		System.assertEquals(rec1.Id, res.records[0].id);
		System.assertEquals(rec1.Name_L0__c, res.records[0].name);
		System.assertEquals(rec1.Name_L0__c, res.records[0].name_L0);
		System.assertEquals(rec1.Name_L1__c, res.records[0].name_L1);
		System.assertEquals(rec1.Name_L2__c, res.records[0].name_L2);
		System.assertEquals(String.valueOf(rec1.Date__c), res.records[0].recordDate);
		System.assertEquals(rec1.CalendarId__c, res.records[0].calendarId);
		System.assertEquals(rec1.AttDayType__c, res.records[0].dayType);
		System.assertEquals(rec1.Remarks__c, res.records[0].remarks);

		System.assertEquals(rec2.Id, res.records[1].id);
		System.assertEquals(rec2.Name_L0__c, res.records[1].name);
		System.assertEquals(rec2.Name_L0__c, res.records[1].name_L0);
		System.assertEquals(rec2.Name_L1__c, res.records[1].name_L1);
		System.assertEquals(rec2.Name_L2__c, res.records[1].name_L2);
		System.assertEquals(String.valueOf(rec2.Date__c), res.records[1].recordDate);
		System.assertEquals(rec2.CalendarId__c, res.records[1].calendarId);
		System.assertEquals(rec2.AttDayType__c, res.records[1].dayType);
		System.assertEquals(rec2.Remarks__c, res.records[1].remarks);
	}

	/**
	 * カレンダー明細検索APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void searchRecordApiTestValidation() {
		Test.startTest();

		// カレンダー明細IDが不正
		CalendarResource.SearchRecordRequest param1 = new CalendarResource.SearchRecordRequest();
		param1.id = 'test';
		try {
			param1.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// カレンダーIDが不正
		CalendarResource.SearchRecordRequest param2 = new CalendarResource.SearchRecordRequest();
		param2.calendarId = 'test';
		try {
			param2.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}
}