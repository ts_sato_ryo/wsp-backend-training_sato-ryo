/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 短時間勤務設定のサービスクラス
 */
public with sharing class AttShortTimeSettingService extends ParentChildService {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 短時間勤務設定リポジトリ */
	private AttShortTimeSettingRepository settingRepo = new AttShortTimeSettingRepository();

	/** コンストラクタ */
	public AttShortTimeSettingService() {
		super(new AttShortTimeSettingRepository());
	}

	/**
	 * エンティティのユニークキーを更新する
	 * (コード、会社IDに値が設定されていない場合は更新されない)
	 * @param base ユニークキーを作成するエンティティ
	 */
	public void updateUniqKey(AttShortTimeSettingBaseEntity base) {
		Id companyId = base.companyId;
		String code = base.code;

		// 既存のエンティティを更新する場合はエンティティに更新対象の項目以外は値が設定されていない可能性があるため
		// リポジトリから更新前のエンティティを取得する
		if ((base.id != null) && ((companyId == null) || (String.isBlank(code)))) {
			// 既存のエンティティを取得する
			AttShortTimeSettingBaseEntity orgBase = settingRepo.getBaseEntity(base.id);
			if (orgBase == null) {
				// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_RecordNotFound);
			}
			if (companyId == null) {
				companyId = orgBase.companyId;
			}
			if (String.isBlank(code)) {
				code = orgBase.code;
			}
		}

		if (companyId == null ) {
			// メッセージ：会社が設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Company}));
		}
		if (String.isBlank(code)) {
			// メッセージ：コードが設定されていません
			throw new App.ParameterException(ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Com_Lbl_Code}));
		}
		CompanyEntity company = getCompany(companyId);
		base.uniqKey = base.createUniqKey(company.code, code);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntity(Id baseId) {
		// ベースに紐づく全ての履歴を取得する
		return settingRepo.getEntity(baseId);
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildBaseEntity> getEntityList(List<Id> baseIds) {
		// ベースに紐づく全ての履歴を取得する
		final AppDate targetAllDate = null; // 全ての履歴が対象
		return settingRepo.getEntityList(baseIds, targetAllDate);
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildBaseEntity getEntityByCode(String code) {
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.codes = new Set<String>{code};
		List<AttShortTimeSettingBaseEntity> entityList = settingRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したベースIDのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override ParentChildHistoryEntity getHistoryEntity(Id historyId) {
		// ベースに紐づく全ての履歴を取得する
		return settingRepo.getHistoryEntity(historyId);
	}

	/**
	 * 指定した履歴IDの履歴エンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	protected override List<ParentChildHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		// ベースに紐づく全ての履歴を取得する
		return settingRepo.getHistoryEntityList(historyIds);
	}

	/**
	 * ベースエンティティの値を検証する
	 * @param history 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveBaseEntity(ParentChildBaseEntity base) {
		return (new AttConfigValidator.AttShortTimeSettingBaseValidator((AttShortTimeSettingBaseEntity)base)).validate();
	}

	/**
	 * 履歴エンティティの値を検証する
	 * @param history 検証対象の履歴エンティティ
	 * @param base 検証対象のベースエンティティ
	 * @return 検証結果
	 */
	protected override Validator.Result validateSaveHistoryEntity(ParentChildBaseEntity Base, ParentChildHistoryEntity history) {
		return (new AttConfigValidator.AttShortTimeSettingHistoryValidator((AttShortTimeSettingBaseEntity)base, (AttShortTimeSettingHistoryEntity)history)).validate();
	}

	/**
	 * 履歴エンティティが持つnameL0を取得する
	 * @param history 取得対象の履歴エンティティ
	 * @return 履歴エンティティのnameL0、ただしnameL0を持っていない場合はnull
	 */
	protected override String getNameL0FromHistory(ParentChildHistoryEntity history) {
		return ((AttShortTimeSettingHistoryEntity)history).nameL0;
	}

	/**
	 * エンティティのコードが重複しているかどうかを確認する(会社単位での重複チェック)
	 * @param base チェック対象のベースエンティティ
	 * @param companyCode 会社コード
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	@TestVisible
	protected override Boolean isCodeDuplicated(ParentChildBaseEntity base) {
		AttShortTimeSettingBaseEntity empBase = (AttShortTimeSettingBaseEntity)base;
		AttShortTimeSettingBaseEntity duplicateBase = getEntityByCode(empBase.code, empBase.companyId);

		if (duplicateBase == null) {
			// 重複していない
			return false;
		}
		if (duplicateBase.id == empBase.id) {
			// 重複していない
			return false;
		}

		// ここまできたら重複している
		return true;
	}

	/**
	 * 指定したコードのベースエンティティを取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ
	 */
	private AttShortTimeSettingBaseEntity getEntityByCode(String code, Id companyId) {
		AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
		filter.codes = new Set<String>{code};
		filter.companyIds = new Set<Id>{companyId};
		List<AttShortTimeSettingBaseEntity> entityList = settingRepo.searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}
}
