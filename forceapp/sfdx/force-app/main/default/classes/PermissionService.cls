/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 権限に関するサービスを提供するクラス
 * 社員に付与されている権限はEmployeePermissionServiceクラスに実装してください
 */
public with sharing class PermissionService {
	private PermissionRepository permissionRepo = new PermissionRepository();
	private CompanyRepository companyRepo = new CompanyRepository();

	/**
	 * @description 権限を取得する
	 * @param permissionId 権限レコードID
	 * @param companyId 会社ID
	 * @return 権限のリスト
	 */
	public List<PermissionEntity> getPermissionList(List<Id> permissionIds, Id companyId) {
		//　会社IDが存在しない場合はエラーを返す
		if (companyId != null) {
			// - 会社データを取得
			CompanyEntity company = companyRepo.getEntity(companyId);
			if (company == null) {
				// メッセージ：指定された会社が見つかりません。
				throw new App.IllegalStateException(
						App.ERR_CODE_RECORD_NOT_FOUND,
						ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Company}));
			}
		}

		//　権限を取得
		// - 検索条件を作成
		PermissionRepository.SearchFilter filter = new PermissionRepository.SearchFilter();
		if (permissionIds != null && !permissionIds.isEmpty()) {
			filter.permissionIds = permissionIds;
		}
		if (String.isNotBlank(companyId)) {
			filter.companyId = companyId;
		}
		// - 検索
		List<PermissionEntity> resList = permissionRepo.searchEntityList(filter);
		return resList;
	}

	/*
	 * 権限を1件保存する。
	 * entity.idが設定されている場合はレコードを更新し、設定されていない場合は登録する。
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果のレコードID
	 * @throws App.RecordNotFoundException entityに紐づく会社が見つからない場合
	 * @throws App.ParameterException 重複したコードが設定されている場合
	 */
	public Id savePermission(PermissionEntity entity) {
		// 更新時は会社情報を取得する
		// 権限は権限コードが可変のため、ユニークキー(会社コード＋権限コード)が変更できてしまう。ユニークキー作成のため、会社情報を取得する
		if (entity.id != null) {
			PermissionEntity updateTarget =  new PermissionRepository().getEntityById(entity.id);
			entity.companyId = updateTarget == null ? null : updateTarget.companyId;
		}
		// ユニークキーの設定
		// - 会社コードを取得
		CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません
			throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_SpecifiedNotFound(new List<String>{ComMessage.msg().Com_Lbl_Company}));
		}
		entity.uniqKey = entity.createUniqKey(company.code);

		// バリデーション
		validatePermission(entity);

		// 保存処理
		Repository.SaveResult result = permissionRepo.saveEntity(entity);
		return result.details[0].id;
	}

	/**
	 * 権限エンティティのチェックを行う
	 * @param entity チェック対象のエンティティ
	 * @throws
	 */
	private void validatePermission(PermissionEntity entity) {
		Validator.Result result = new ComConfigValidator.PermissionValidator(entity).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// UniqKeyエラーを事前に防ぐため、重複したコードが設定されている場合はエラーにする
		if (isExistCode(entity)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}
	}

	/*
	 * 会社ごとにコードが重複していないことを確認する
	 * @param entity 保存対象のエンティティ
	 * @return true(重複あり) / false(重複なし)
	 */
	@TestVisible
	private boolean isExistCode(PermissionEntity entity) {
		boolean isExistCode = false;
		List<PermissionEntity> resultList = permissionRepo.getEntityListByCode(new List<String>{entity.code}, entity.companyId);
		for (PermissionEntity result : resultList) {

			if (entity.id == null) {
				// 新規の場合
				isExistCode = result != null;
			} else {
				// 更新の場合、エンティティと検索結果のidが同じであるか確認する
				isExistCode = result != null && result.Id != entity.Id;
			}

			// 同じコードが1件でもあれば重複あり
			if (isExistCode) {
				break;
			}
		}
		return isExistCode;
	}

	/*
	 * 権限を削除する。
	 * @param id 削除対象の権限レコードID
	 */
	public void deletePermission(Id id) {
		new LogicalDeleteService(permissionRepo).deleteEntity(id);
	}
}