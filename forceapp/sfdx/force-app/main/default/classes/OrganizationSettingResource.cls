/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 組織設定レコード操作APIを実装するクラス
 */
public with sharing class OrganizationSettingResource {

	/**
	 * @description 組織設定レコードを表すクラス
	 */
	public class OrganizationSetting implements RemoteApi.RequestParam, RemoteApi.ResponseParam {
		/** 使用言語1 */
		public String language0;
		/** 使用言語2 */
		public String language1;
		/** 使用言語3 */
		public String language2;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {

			// L0は必須
			if (String.isBlank(this.language0)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank(ComMessage.msg().Admin_Lbl_Language0);
				throw ex;
			}

			// 対応している言語が設定されていることを確認する
			if (isInvalidLang(this.language0)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageInvalid(ComMessage.msg().Admin_Lbl_Language0, this.language0);
				throw ex;
			}
			if (isInvalidLang(this.language1)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageInvalid(ComMessage.msg().Admin_Lbl_Language1, this.language1);
				throw ex;
			}
			if (isInvalidLang(this.language2)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageInvalid(ComMessage.msg().Admin_Lbl_Language2, this.language2);
				throw ex;
			}
		}

		/**
		 * 指定された言語が対応している言語かどうかを確認する
		 * @return 対応していない言語の場合はtrue,そうでない場合はfalse
		 */
		private Boolean isInvalidLang(String langValue) {

			// 空文字の場合は不正ではない
			if (String.isBlank(langValue)) {
				return false;
			}

			// 対応する言語がない場合は不正な言語
			if (AppLanguage.valueOf(langValue) == null) {
				return true;
			}

			return false;
		}
	}

	/**
	 * @description 組織設定レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description 組織設定レコードを1件登録する
		 * @param  req リクエストパラメータ(OrganizationSetting)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			OrganizationSetting param = (OrganizationSetting)req.getParam(OrganizationSetting.class);

			// パラメータ値を検証する
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// レコードが無ければ新規作成、あれば既存レコードを更新する
			upsertEntity(param);

			// 成功レスポンスをセットする
			return null;
		}

		/**
		 * @description 組織設定レコードが存在する場合は更新、存在しない場合は作成します<br/>
		 * @param  param 組織設定レコード値を含むパラメータ
		 * @return  作成or更新した組織設定レコード
		 */
		public void upsertEntity(OrganizationSetting param){

			OrganizationSettingRepository repo = new OrganizationSettingRepository();

			// レコードが無ければ新規作成、あれば既存レコードを更新する
			OrganizationSettingEntity entity = repo.getEntity();
			if (entity == null) {
				entity = new OrganizationSettingEntity();
			}

			entity.language0 = AppLanguage.valueOf(param.language0);
			entity.language1 = AppLanguage.valueOf(param.language1);
			entity.language2 = AppLanguage.valueOf(param.language2);

			Repository.SaveResult result = repo.saveEntity(entity);

		}

	}

	/**
	 * @description 組織設定レコード取得APIを実装するクラス
	 */
	public with sharing class GetApi extends RemoteApi.ResourceBase {

		/**
		 * @description 組織設定レコードを1件取得する
		 * @param  req リクエストパラメータ(パラメータなし)
		 * @return レスポンス(OrganizationSetting)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			List<OrganizationSettingEntity> orgList = (new OrganizationSettingRepository()).getEntityList();
			OrganizationSettingEntity org = orgList.isEmpty() ? new OrganizationSettingEntity() : orgList[0];

			// 成功レスポンスをセットする
			OrganizationSetting res = new OrganizationSetting();
			res.language0 = AppConverter.stringValue(org.language0);
			res.language1 = AppConverter.stringValue(org.language1);
			res.language2 = AppConverter.stringValue(org.language2);

			return res;
		}
	}

	/**
	 * @description 言語選択リストを格納するクラス
	 */
	public class Picklist implements RemoteApi.ResponseParam {
		/*+ 選択リスト項目 */
		public PicklistEntry[] entries;
	}

	/**
	 * @description 選択リスト項目の定義クラス
	 */
	public class PicklistEntry implements RemoteApi.ResponseParam {
		/*+ 項目のラベル */
		public String label;
		/*+ 項目の値 */
		public String value;
	}

	/**
	 * @description 言語選択リスト取得APIを実装するクラス
	 */
	public with sharing class GetLanguagePicklistApi extends RemoteApi.ResourceBase {

		/**
		 * @description 言語選択リストを取得する
		 * @param  req リクエストパラメータ(パラメータなし)
		 * @return レスポンス(Picklist)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			List<PicklistEntry> entries = new List<PicklistEntry>();

			List<Schema.PicklistEntry> pickEntries = ComOrgSetting__c.Language_0__c.getDescribe().getPicklistValues();
			for (Schema.PicklistEntry e:pickEntries) {
				if (e.isActive()) {
					PicklistEntry e2 = new PicklistEntry();
					e2.value = e.getValue();
					e2.label = e.getLabel();
					entries.add(e2);
				}
			}

			// 成功レスポンスをセットする
			Picklist res = new Picklist();
			res.entries = entries;
			return res;
		}
	}

}
