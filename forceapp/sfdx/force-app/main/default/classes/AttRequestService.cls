/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠申請サービスクラス（日次申請の処理はAttDailyRequestServiceに実装してください）
 */
public with sharing class AttRequestService {

	private static final String ACTION_TYPE_REMOVED = 'Removed';
	private static final String ACTION_TYPE_CANCELED = 'Canceled';

	/**
	 * 勤務確定申請パラメータ
	 */
	public class SubmitFixRequestParam {
		/** 申請対象のサマリーID */
		public Id summaryId;
		/** 申請コメント */
		public String comment;
	}


	/**
	 * 勤務確定可否のチェックを行う
	 * @param summaryId 勤怠サマリーID
	 * @return 確認メッセージのリスト
	 */
	public List<String> checkFixRequest(Id summaryId) {

		// 勤怠計算済みのサマリーを取得する
		AttAttendanceService service = new AttAttendanceService();
		AttSummaryEntity summary = service.getSummaryById(summaryId);
		if (summary == null) {
			// メッッセージ：該当する勤務表が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_ATT_SUMMARY,
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_TimeSheet}));
		}

		// バリデーションを実行する
		validateFixRequest(summary);

		EmployeeHistoryEntity empHistory = new EmployeeRepository().getHistoryEntity(summary.employeeId);
		AttWorkingTypeBaseEntity workTypeBase = new AttWorkingTypeRepository().getEntity(empHistory.workingTypeId, summary.endDate);
		AttWorkingTypeHistoryEntity workTypeHis = workTypeBase.getHistoryByDate(summary.endDate);


		// 欠勤日数、休憩不足日数チェック
		Integer absenceCount = 0;
		Integer insufficientRestTimeCount = 0;
		for (AttRecordEntity record : summary.attRecordList) {
			if (record.leaveOfAbsenceId != null) {
				continue;
			}
			// 欠勤した場合日数を加算
			if (!workTypeHis.useAbsenceApply) {
				if (new AttCalcService().isAbsent(workTypeHis, record)) {
					absenceCount += 1;
				}
			}
			// 法定休憩チェックを使用する場合
			if (workTypeHis.useLegalRestCheck1) {
				if (record.outInsufficientRestTime != null && record.outInsufficientRestTime.getIntValue() > 0) {
					// 不足休憩時間がある場合は休憩不足日数としてカウント
					insufficientRestTimeCount += 1;
				}
			}
		}

		// 確認事項がある場合、メッセージを返す
		List<String> confirmationList = new List<String>();
		if (absenceCount > 0 || insufficientRestTimeCount > 0) {
			// 欠勤日数>0の場合
			if (absenceCount > 0) {
				confirmationList.add(ComMessage.msg().Att_Msg_FixSummaryConfirmAbsence(new List<String>{String.valueOf(absenceCount)}));
			}
			// 休憩不足日数>0の場合
			if (insufficientRestTimeCount > 0) {
				confirmationList.add(ComMessage.msg().Att_Msg_FixSummaryConfirmInsufficientRestTime(new List<String>{String.valueOf(insufficientRestTimeCount)}));
			}
		}

		return confirmationList;
	}

	/**
	 * 勤務確定申請を申請する
	 * @param param 申請内容
	 */
	public void submitFixRequest(AttRequestService.SubmitFixRequestParam param) {

		AttRequestRepository requestRepo = new AttRequestRepository();
		AttSummaryRepository summaryRepo = new AttSummaryRepository();

		// 勤怠計算済みのサマリーを取得する
		AttAttendanceService service = new AttAttendanceService();
		AttSummaryEntity summary = service.getSummaryById(param.summaryId);
		if (summary == null) {
			// メッッセージ：該当する勤務表が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_ATT_SUMMARY,
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_TimeSheet}));
		}

		// バリデーションを実行する
		validateFixRequest(summary);

		// 欠勤日数を集計する
		Decimal absenceDays = new AttCalcService().calcAbsenceDays(summary);
		summary.outWorkAbsenceDays = AttDays.valueOf(absenceDays);
		summaryRepo.saveEntity(summary);

		// 勤怠サマリーの社員を取得する
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(summary.employeeBaseId, summary.startDate);

		// 社員の会社のデフォルト言語取得
		AppLanguage companyLang = getCompanyLanguage(employee.companyId);
		if (companyLang == null) {
			// メッセージ：会社のデフォルト言語が設定されていません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_SET_COMPANY_LANG,
					ComMessage.msg().Att_Err_FieldNotSet(new List<String>{
					ComMessage.msg().Com_Lbl_Company,
					ComMessage.msg().Com_Lbl_DefaultLanguage}));
		}

		// 申請エンティティを作成する
		AttRequestEntity request = createRequest(summary, param.comment, employee, companyLang);
		if (summary.requestId != null) {
			// 既存の申請レコードが存在する場合は、IDを設定する
			request.setId(summary.requestId);
		}

		// 申請を保存する
		Repository.SaveResult requestSaveRes = requestRepo.saveEntity(request);
		request.setId(requestSaveRes.details[0].id);

		// 勤務表データを申請オブジェクトに添付する
		// ファイル名：申請名
		// 言語：会社のデフォルト言語
		AttSummaryService summaryService = new AttSummaryService();
		AttSummaryService.TimeSheet timeSheet = summaryService.getTimeSheet(summary, companyLang);
		AppAttachmentRepository attachRepo = new AppAttachmentRepository();
		List<AppAttachmentEntity> attaches = attachRepo.getEntityListByParentId(request.id, request.name);
		AppAttachmentEntity attach;
		if ((attaches == null) || (attaches.isEmpty())) {
			attach = new AppAttachmentEntity();
			attach.name = request.name;
			attach.parentId = request.id;
		} else {
			attach = attaches[0];

			// 同じファイル名で複数存在している場合は、ログを出力しておく
			if (attaches.size() > 1) {
				System.debug(System.LoggingLevel.WARN,
					'勤怠申請レコードに同名の添付ファイルが複数存在します。(ParentId=' + request.Id + 'ファイル名=' + request.name + ')');
			}
		}
		attach.bodyText = JSON.serialize(timeSheet);
		attachRepo.saveEntity(attach);

		// 有休付与確定計算を行う(勤怠サマリのロック前)
		new AttManagedLeaveService().initLeaveSummary(summary);

		// 勤怠サマリーを更新する
		// summary.resetChanged();  // 変更情報をリセット
		if (summary.requestId == null) {
			// 申請IDを設定
			summary.requestId = request.id;
		}

		// ロックする
		summary.isLocked = true;
		summaryRepo.saveEntity(summary);

		// 勤務確定申請を申請する
		submitProcess(request.id, param.comment);
	}

	/**
	 * @description 取消処理を行う
	 * @param requestId 申請ID
	 * @param comment コメント
	 */
	public void cancelRequest(Id requestId, String comment) {
		// 申請データを取得
		AttRequestRepository repo = new AttRequestRepository();
		AttRequestEntity request = repo.getEntity(requestId);
		if (request == null) {
			// メッセージ：該当する申請が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_REQUEST,
					ComMessage.msg().Att_Err_NotFoundRequest);
		}

		if (AppRequestStatus.PENDING.equals(request.status)) {
			// 申請取消
			cancelRequestProcess(request, comment);
		} else {
			// メッセージ：該当の申請は取り消せません。
			throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_CANCEL_REQUEST,
					ComMessage.msg().Att_Err_CannotCancelRequest);
		}
	}

	/**
	 * @description 承認取消処理を行う
	 * @param requestId 申請ID
	 * @param comment コメント
	 */
	public void cancelApproval(Id requestId, String comment) {
		// 申請データを取得
		AttRequestRepository repo = new AttRequestRepository();
		AttRequestEntity request = repo.getEntity(requestId);
		if (request == null) {
			// メッセージ：該当する申請が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_REQUEST,
					ComMessage.msg().Att_Err_NotFoundRequest);
		}

		if (AppRequestStatus.APPROVED.equals(request.status)) {
			// 承認取消
			cancelApprovalProcess(request, comment);
		} else {
			// メッセージ：該当の承認は取り消せません。
			throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_CANCEL_APPROVAL,
					ComMessage.msg().Att_Err_CannotCancelApproval);
		}
	}

	/**
	 * 勤務確定申請のバリデーションを行う
	 * @param summary 申請対象のサマリー
	 */
	private void validateFixRequest(AttSummaryEntity summary) {
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttPatternRepository2 patternRepo = new AttPatternRepository2();

		// これまでに該当月で確定申請をしたことがない場合、Salesforceユーザが無効である社員が申請するとエラー
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(summary.employeeBaseId, summary.startDate);
		if (summary.requestId == null && !employee.user.isActive) {
			// メッセージ：該当社員のSalesforceユーザが無効です。
			throw new App.IllegalStateException(
					App.ERR_CODE_INACTIVE_USERS_REQUEST,
					ComMessage.msg().Com_Err_CannotRequestFromInvalidUser);
		}

		// 勤務体系を取得
		AttWorkingTypeBaseEntity workingType = new AttWorkingTypeService().getWorkingType(summary.workingTypeBaseId, summary.endDate);
		// 前後の勤怠サマリ(開始日昇順)を取得
		List<AttSummaryEntity> summaryList = summaryRepo.searchEntity(summary.employeeBaseId, summary.endDate.addDays(1), summary.startDate.addDays(-1));
		AttWorkingTypeBaseEntity workingTypeBefore;
		AttWorkingTypeBaseEntity workingTypeAfter;
		if (! summaryList.isEmpty()) {// FIXME: サマリ単位が月以外の場合を未考慮
			for (AttSummaryEntity sum : summaryList) {
				if (sum.endDate.equals(summary.startDate.addDays(-1))) {
					// 月中開始がありうるため、対象勤務表の開始日前日で前月の勤務体系を取得する
					workingTypeBefore = new AttWorkingTypeService().getWorkingType(summary.workingTypeBaseId, summary.startDate.addDays(-1));
				}
				if (sum.startDate.equals(summary.endDate.addDays(1))) {
					// 月中失効がありうるため、対象勤務表の終了日翌日で翌月の勤務体系を取得する
					workingTypeAfter = new AttWorkingTypeService().getWorkingType(summary.workingTypeBaseId, summary.endDate.addDays(1));
				}
			}
		}

		// 申請可能であることを確認する
		AttValidator.RequestSubmitValidator requestValidator =
				new AttValidator.RequestSubmitValidator(workingType, summary, summary.attRecordList);
		Validator.Result requestValidatorRes = requestValidator.validate();
		if (! requestValidatorRes.isSuccess()) {
			Validator.Error error = requestValidatorRes.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 対象サマリーの実効勤務パターンを取得する
		Map<Id, AttPatternEntity> workingPatternMap = new Map<Id, AttPatternEntity>();
		for (Integer i = 0; i < summary.attRecordList.size(); i++) {
			AttRecordEntity record = summary.attRecordList[i];
			workingPatternMap.put(record.outPatternId, null);
		}
		for (AttPatternEntity pattern : patternRepo.getPatternList(workingPatternMap.keySet())) {
			workingPatternMap.put(pattern.id, pattern);
		}

		// 勤怠明細のバリデーションを実行する
		for (Integer i = 0; i < summary.attRecordList.size(); i++) {
			AttRecordEntity record = summary.attRecordList[i];
			AttPatternEntity pattern = workingPatternMap.get(record.outPatternId);
			if (pattern == null) {
				pattern = workingType.getHistory(0).createPatternEntity();
			}

			// 前日の勤怠明細・実効勤務パターンを取得する
			AttRecordEntity recordBefore;
			AttPatternEntity patternBefore;
			if (i == 0) {
				List<AttRecordEntity> records = summaryRepo.searchRecordEntity(summary.employeeBaseId, record.rcdDate.addDays(-1));
				recordBefore = (records != null && ! records.isEmpty()) ? records[0] : null;
				if (recordBefore != null && recordBefore.outPatternId != null) {
					patternBefore = patternRepo.getPatternById(recordBefore.outPatternId);
				}
			} else {
				recordBefore = summary.attRecordList[i - 1];
				if (recordBefore != null && recordBefore.outPatternId != null) {
					patternBefore = workingPatternMap.get(recordBefore.outPatternId);
				}
			}
			// 存在しない場合は、勤怠体系から補完する
			if (patternBefore == null) {
				if (i == 0) {
					patternBefore = workingTypeBefore == null ? null : workingTypeBefore.getHistory(0).createPatternEntity();
				} else {
					patternBefore = workingType == null ? null : workingType.getHistory(0).createPatternEntity();
				}
			}

			// 後日の勤怠明細・実効勤務パターンを取得する
			AttRecordEntity recordAfter;
			AttPatternEntity patternAfter;
			if (i == (summary.attRecordList.size() - 1)) {
				List<AttRecordEntity> records = summaryRepo.searchRecordEntity(summary.employeeBaseId, record.rcdDate.addDays(1));
				recordAfter = (records != null && ! records.isEmpty()) ? records[0] : null;
				if (recordAfter != null && recordAfter.outPatternId != null) {
					patternAfter = patternRepo.getPatternById(recordAfter.outPatternId);
				}
			} else {
				recordAfter = summary.attRecordList[i + 1];
				if (recordAfter != null && recordAfter.outPatternId != null) {
					patternAfter = workingPatternMap.get(recordAfter.outPatternId);
				}
			}
			// 存在しない場合は、勤務体系が補完する
			if (patternAfter == null) {
				if (i == (summary.attRecordList.size() - 1)) {
					patternAfter = workingTypeAfter == null ? null : workingTypeAfter.getHistory(0).createPatternEntity();
				} else {
					patternAfter = workingType == null ? null : workingType.getHistory(0).createPatternEntity();
				}
			}

			AttValidator.AttTimeValidator attTimeValidator = new AttValidator.AttTimeValidator(
							record, recordBefore, recordAfter,
							pattern, patternBefore, patternAfter);
			Validator.Result attTimeValidatorRes = attTimeValidator.validate();
			if (! attTimeValidatorRes.isSuccess()) {
				// FIXME: エラーが複数検出された場合の処理
				Validator.Error error = attTimeValidatorRes.getErrors().get(0);
				throw new App.IllegalStateException(error.code, record.rcdDate.format() + ': ' + error.message);
			}
		}
		// 勤怠期間のチェック(短時間勤務)
		Set<Id> shortTimeSettingIds = new Set<Id>();
		for (AttPeriodStatusEntity periodStatus : new AttPeriodStatusService().searchPeriodStatusList(
				summary.employeeBaseId, summary.startDate, summary.endDate, AttPeriodStatusType.SHORT_TIME_WORK_SETTING)) {
			shortTimeSettingIds.add(periodStatus.shortTimeWorkSettingBaseId);
		}
		// 短時間勤務設定の労働時間制をチェックする
		if (!shortTimeSettingIds.isEmpty()) {
			// 勤怠サマリの労働時間制を取得
			AttWorkingTypeBaseEntity workingTypeBaseData = new AttWorkingTypeRepository().getEntity(summary.workingTypeBaseId);
			for (AttShortTimeSettingBaseEntity shortSetting : new AttShortTimeSettingRepository().getBaseEntityList(new List<Id>(shortTimeSettingIds), false)) {
				if (workingTypeBaseData.getWorkSystemString() != AppConverter.stringValue(shortSetting.workSystem)) {
					// メッセージ：指定された有効期間内に、社員の勤務体系の労働時間制が短時間勤務設定の労働時間制と一致していない期間が存在します
					throw new App.IllegalStateException(ComMessage.msg().Att_Err_InvalidWorkSystemShortTimeWorkSetting);
				}
			}
		}
	}

	/**
	 * 勤務確定申請エンティティを作成する
	 * @param summary 申請対象のサマリー
	 * @param comment 申請コメント
	 * @param employee 勤怠サマリーの社員
	 * @param lang 申請名などの言語
	 * @return 申請エンティティ
	 */
	private AttRequestEntity createRequest(AttSummaryEntity summary, String comment,
			EmployeeBaseEntity employee, AppLanguage lang) {

		EmployeeService empService = new EmployeeService();

		// 申請者(実行ユーザ)の社員を取得する
		EmployeeBaseEntity actor = empService.getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());

		// 勤怠サマリーの勤務体系を取得する
		AttWorkingTypeBaseEntity workingType = new AttWorkingTypeService().getWorkingType(summary.workingTypeBaseId, summary.endDate);
		if (workingType == null) {
			// メッセージ：社員の勤務体系が見つかりませんでした。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_WORKINGTYPE,
					ComMessage.msg().Att_Err_NotFoundAofB(new List<String>{
						ComMessage.msg().Com_Lbl_Employee,
						ComMessage.msg().Att_Lbl_WorkingType}));
		}

		// 申請名
		String requestName = createRequestName(employee,
				summary.startDate, summary.endDate, lang, workingType);

		// 申請エンティティを作成
		AttRequestEntity request = new AttRequestEntity();
		request.name = requestName;
		request.ownerId = employee.userId;
		request.summaryId = summary.id;
		request.comment = comment;
		request.status = AppRequestStatus.PENDING;
		request.cancelType = null;
		request.isConfirmationRequired = false;
		request.requestTime = AppDateTime.now();
		request.employeeHistoryId = summary.employeeId;
		request.departmentHistoryId = summary.deptId;
		request.actorId = actor.getHistory(0).id;

		// 承認者01の社員を取得する
		AttWorkingTypeHistoryEntity workingTypeHis = workingType.getHistoryByDate(summary.endDate);
		List<AppDate> dateList = new List<AppDate>{summary.endDate};
		Map<AppDate, EmployeeBaseEntity> approver01Map = empService.getApprover01(workingTypeHis.allowToChangeApproverSelf, employee.id, dateList);
		EmployeeBaseEntity empApprover01 = approver01Map.get(summary.endDate);
		if (empApprover01 == null) {
			// メッセージ：承認者が見つかりません。承認者が設定されていないか、有効期間外か、またはSalesforceユーザが無効です。
			throw new App.IllegalStateException(App.ERR_CODE_NOT_FOUND_APPROVER, ComMessage.msg().Att_Err_NotFoundApprover);
		}

		// 承認者01と申請者が同じ場合に、自己承認権限をチェックする
		if (empApprover01.id == employee.id) {
			List<AttResourcePermission.Permission> requiredPermissionList =
					new List<AttResourcePermission.Permission>{AttResourcePermission.Permission.APPROVE_SELF_ATT_REQUEST_BY_EMPLOYEE};
			try {
				// 権限をチェック
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, empApprover01.id);
			} catch (App.NoPermissionException e) {
				// 勤務確定申請固有のメッセージに上書き
				e.setMessage(ComMessage.msg().Att_Err_NotAllowSelfAttRequest(new List<String>{empApprover01.fullNameL.getFullName()}));
				throw e;
			}
		}
		request.approver01Id = empApprover01.userId;

		return request;
	}

	/**
	 * 勤務確定申請の申請名を作成する(月度の場合)
	 * @param employee 社員
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @return 申請名
	 */
	@testVisible
	private String createRequestName(EmployeeBaseEntity employee,
			AppDate startDate, AppDate endDate,
			AppLanguage lang, AttWorkingTypeBaseEntity workingType) {

		String employeeName = employee.displayNameL.getValue(lang);
		AppDate targetDate = getRequestNameDate(startDate, endDate, workingType);

		String requestName;
		ComMsgBase targetLang = ComMessage.msg(lang.value);
		if (lang == AppLanguage.JA) {
			// 日本語の場合
			// 申請名： {社員の表示名}さん 勤務確定申請 {YYYY/MM}
			requestName = employeeName + 'さん ' + targetLang.Att_Lbl_RequestTypeMonthlyAttendance +
			targetLang.Att_Lbl_Request + ' ' + targetDate.format(AppDate.FormatType.YYYY_MM_SLASH);
		} else {
			// 英語の場合
			// 申請名： {社員の表示名} Monthly Attendance Request {MMM YYYY}
			requestName = employeeName + ' ' + targetLang.Att_Lbl_RequestTypeMonthlyAttendance +
			' ' + targetLang.Att_Lbl_Request + ' ' + targetDate.format(AppDate.FormatType.MMMYYYY);
		}

		return requestName;
	}

	/**
	 * 勤務確定申請名に設定する日付を取得する
	 */
	private AppDate getRequestNameDate(AppDate startDate, AppDate endDate,
			AttWorkingTypeBaseEntity workingType) {

		// 現在は月度のみ対応
		AppDate nameDate;
		if (workingType.payrollPeriod == AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month) {
			if (workingType.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase) {
				nameDate = startDate;
			} else if (workingType.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase) {
				nameDate = endDate;
			}
		} else {
			// サーバエラー
			// メッセージ：勤務体系に未対応の清算期間が設定されています。（清算期間=[%1])
			throw new App.UnsupportedException(
					ComMessage.msg().Att_Err_WorkingTypePayrollPeriodNotSupported(new List<String>{workingType.payrollPeriod.name()}));
		}

		return nameDate;
	}

	/**
	 * 承認申請を行う
	 * TODO 将来的には承認申請処理はアプリ全体で共通化される予定。それまでの暫定対応。
	 * @param requestId 申請対象レコードのID
	 * @param comment コメント
	 */
	private void submitProcess(Id requestId, String comment) {
		Approval.ProcessSubmitRequest processReq = new Approval.ProcessSubmitRequest();
		processReq.setObjectId(requestId);
		processReq.setComments(comment);

		if (!test.isRunningTest()) {
			Approval.process(processReq);
		}
	}

	/**
	 * @description 申請取消を行う
	 * @param request 申請エンティティ
	 * @param comment コメント
	 */
	private void cancelRequestProcess(AttRequestEntity request, String comment) {
		if (!test.isRunningTest()) {
			// 承認/却下対象の承認申請データを取得
			ApprovalProcessRepository appRepo = new ApprovalProcessRepository();
			List<Id> workItemIdList = appRepo.getWorkItemList(new List<Id>{request.id});
			if (workItemIdList == null || workItemIdList.isEmpty()) {
				// メッセージ：該当の申請は取り消せません。
				throw new App.IllegalStateException(
						App.ERR_CODE_CANNOT_CANCEL_REQUEST,
						ComMessage.msg().Att_Err_CannotCancelRequest);
			}

			// 取消実行
			Approval.ProcessWorkitemRequest processReq = new Approval.ProcessWorkitemRequest();
			processReq.setWorkItemId(workItemIdList[0]);
			processReq.setAction(ACTION_TYPE_REMOVED);
			processReq.setComments(comment);

			try {
				Approval.process(processReq);
			} catch(DmlException e) {
				if(e.getMessage().indexOf('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY') >= 0) {
					// メッセージ：システム管理者あるいは申請者以外は申請中の取消ができません。
					throw new App.IllegalStateException(
							App.ERR_CODE_NO_PERMISSIONS_CANCEL_REQUEST,
							ComMessage.msg().Att_Err_NoPermissionsRemoveRequest);
				} else {
					throw e;
				}
			}
		}
	}

	/**
	 * @description 承認取消を行う
	 * @param request 申請エンティティ
	 * @param comment コメント
	 */
	private void cancelApprovalProcess(AttRequestEntity request, String comment) {

		AttRequestRepository repo = new AttRequestRepository();
		// 申請データを更新
		request.status = AppRequestStatus.DISABLED;
		request.cancelComment = comment;
		repo.saveEntity(request);
	}

	/**
	 * 会社のデフォルト言語を取得
	 * @param companyId 取得対象の会社ID
	 * @return 言語、設定されていない場合はnull
	 */
	private AppLanguage getCompanyLanguage(Id companyId) {
		// 社員の会社のデフォルト言語取得
		CompanyRepository repo = new CompanyRepository();
		CompanyEntity company = repo.getEntity(companyId);
		return company.language;
	}
}