/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Test class for ExpVendorUsedIn
 */
@isTest
private class ExpVendorUsedInTest {

	/**
	 * Check if it compare properly
	 */
	@isTest static void equalsTest() {

		// Equal
		System.assertEquals(ExpVendorUsedIn.EXPENSE_REPORT, ExpVendorUsedIn.valueOf('ExpenseReport'));
		System.assertEquals(ExpVendorUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ExpVendorUsedIn.valueOf('ExpenseRequestAndExpenseReport'));

		// Values are different
		System.assertEquals(false, ExpVendorUsedIn.EXPENSE_REPORT.equals(ExpVendorUsedIn.valueOf('ExpenseRequestAndExpenseReport')));

		// Class type is different
		System.assertEquals(false, ExpVendorUsedIn.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// Value is null
		System.assertEquals(false, ExpVendorUsedIn.EXPENSE_REPORT.equals(null));
	}
}