@isTest
private class AttRecordEntityTest {

	/**
	 * nameのテスト
	 * 不正な値を設定した場合に例外が発生することを確認する
	 */
	/*@isTest static void nameTest() {
		try {
			AttRecordEntity entity = new AttRecordEntity();
			entity.name = '';
			System.assert(false, '例外が発生しませんでした');
		} catch(App.ParameterException e) {
			System.assert(true);
		} catch(Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}
	}*/

	/**
	 * createNameのテスト
	 */
	@isTest static void createNameTest() {
		String empCode;
		String empName;
		AppDate targetDate;

		// 勤怠記録名が作成できることを確認する
		empCode = '0123';
		empName = 'Test Emp';
		targetDate = AppDate.newInstance(2017, 7, 1);
		System.assertEquals(
				empCode+empName+'20170701',
				AttRecordEntity.createRecordName(empCode, empName, targetDate));


		// empCodeが空の場合、例外が発生する
		empCode = '';
		empName = 'Test Emp';
		targetDate = null;
		try {
			AttRecordEntity.createRecordName(empCode, empName, targetDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}


		// empNameが空の場合、例外が発生する
		empCode = '0123';
		empName = ' ';
		targetDate = null;
		try {
			AttRecordEntity.createRecordName(empCode, empName, targetDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}


		// targetDateがnullの場合、例外が発生する
		empCode = '0123';
		empName = 'Test Emp';
		targetDate = null;
		try {
			AttRecordEntity.createRecordName(empCode, empName, targetDate);
			System.assert(false, '例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assert(true);
		} catch (Exception e) {
			System.assert(false, '想定外の例外が発生しました。' + e);
		}

	}

	/**
	 * getDayTypeNoPrefのテスト
	 * 日タイプが優先法定休日(PreferedLegalHoliday)が正しく判定されることを確認する
	 */
	@isTest static void getDayTypeNoPrefTest() {
		AttRecordEntity entity = new AttRecordEntity();

		// 日タイプが法定休日の場合、そのまま法定休日が返却される
		entity.outDayType = AttDayType.LEGAL_HOLIDAY;
		System.assertEquals(AttDayType.LEGAL_HOLIDAY, entity.getDayTypeNoPref());

		// 日タイプが所定休日の場合、そのまま所定日が返却される
		entity.outDayType = AttDayType.HOLIDAY;
		System.assertEquals(AttDayType.HOLIDAY, entity.getDayTypeNoPref());

		// 日タイプが優先法定休日(PreferedLegalHoliday)で
		// isHolLegalHolidayがtrueの場合、法定休日を返却する
		entity.outDayType = AttDayType.PREFERRED_LEGAL_HOLIDAY;
		entity.isHolLegalHoliday = true;
		System.assertEquals(AttDayType.LEGAL_HOLIDAY, entity.getDayTypeNoPref());
		// isHolLegalHolidayがfalseの場合、所定休日を返却する
		entity.isHolLegalHoliday = false;
		System.assertEquals(AttDayType.HOLIDAY, entity.getDayTypeNoPref());
	}

	// フレックスタイム形式テスト
	@isTest static void formatWorkTimeFlexTest() {
		AttRecordEntity.AttWorkTimeDetail detail = new AttRecordEntity.AttWorkTimeDetail();
		detail.appendCOLOPeriod(new AttTimePeriod(AttTime.valueOf(10), AttTime.valueOf(15))); // 5
		detail.appendCOLIPeriod(new AttTimePeriod(AttTime.valueOf(20), AttTime.valueOf(26))); // 6
		detail.appendCILOPeriod(new AttTimePeriod(AttTime.valueOf(30), AttTime.valueOf(37))); // 7
		detail.appendCILIPeriod(new AttTimePeriod(AttTime.valueOf(40), AttTime.valueOf(48))); // 8

		detail.formatToFlex(AttDailyTime.valueOf(20));
		AttTimePeriod timePeriod;

		system.assertEquals(4, detail.CILIPeriods.size()); // 20
		timePeriod = detail.CILIPeriods[0]; // 5
		system.assertEquals(10, timePeriod.startTime.getIntValue());
		system.assertEquals(15, timePeriod.endTime.getIntValue());
		timePeriod = detail.CILIPeriods[1]; // 6
		system.assertEquals(20, timePeriod.startTime.getIntValue());
		system.assertEquals(26, timePeriod.endTime.getIntValue());
		timePeriod = detail.CILIPeriods[2]; // 7
		system.assertEquals(30, timePeriod.startTime.getIntValue());
		system.assertEquals(37, timePeriod.endTime.getIntValue());
		timePeriod = detail.CILIPeriods[3]; // 2
		system.assertEquals(40, timePeriod.startTime.getIntValue());
		system.assertEquals(42, timePeriod.endTime.getIntValue());

		system.assertEquals(1, detail.COLOPeriods.size()); // 6
		timePeriod = detail.COLOPeriods[0]; // 6
		system.assertEquals(42, timePeriod.startTime.getIntValue());
		system.assertEquals(48, timePeriod.endTime.getIntValue());
	}

	/**
	 * @description
	 * getRequestingRequestIdListのテスト
	 * 申請中の各種申請IDリストが正しく取得できることを確認する
	 */
	@isTest static void getRequestingRequestIdListTest() {
		AttRecordEntity entity = new AttRecordEntity();

		entity.reqRequestingLeave1RequestId = createDummyRequest('reqLeave1RequestId');
		entity.reqRequestingLeave2RequestId = createDummyRequest('reqLeave2RequestId');
		entity.reqRequestingHolidayWorkRequestId = createDummyRequest('reqHolidayWorkRequestId');
		entity.reqRequestingEarlyStartWorkRequestId = createDummyRequest('reqEarlyStartWorkRequestId');
		entity.reqRequestingOvertimeWorkRequestId = createDummyRequest('reqOvertimeWorkRequestId');
		entity.reqRequestingAbsenceRequestId = createDummyRequest('reqAbsenceRequestId');
		entity.inpRequestingDirectRequestId = createDummyRequest('inpDirectRequestId');
		entity.reqRequestingPatternRequestId = createDummyRequest('reqPatternRequestId');

		List<Id> resRequestIdList = entity.getRequestingRequestIdList();
		System.assertEquals(8, resRequestIdList.size());
		System.assertEquals(entity.reqRequestingLeave1RequestId, resRequestIdList.get(0));
		System.assertEquals(entity.reqRequestingLeave2RequestId, resRequestIdList.get(1));
		System.assertEquals(entity.reqRequestingHolidayWorkRequestId, resRequestIdList.get(2));
		System.assertEquals(entity.reqRequestingEarlyStartWorkRequestId, resRequestIdList.get(3));
		System.assertEquals(entity.reqRequestingOvertimeWorkRequestId, resRequestIdList.get(4));
		System.assertEquals(entity.reqRequestingAbsenceRequestId, resRequestIdList.get(5));
		System.assertEquals(entity.inpRequestingDirectRequestId, resRequestIdList.get(6));
		System.assertEquals(entity.reqRequestingPatternRequestId, resRequestIdList.get(7));
	}

	/**
	 * @description
	 * getApprovedRequestIdListのテスト
	 * 承認済みの各種申請IDリストが正しく取得できることを確認する
	 */
	@isTest static void getApprovedRequestIdListTest() {
		AttRecordEntity entity = new AttRecordEntity();

		entity.reqLeave1RequestId = createDummyRequest('reqLeave1RequestId');
		entity.reqLeave2RequestId = createDummyRequest('reqLeave2RequestId');
		entity.reqHolidayWorkRequestId = createDummyRequest('reqHolidayWorkRequestId');
		entity.reqEarlyStartWorkRequestId = createDummyRequest('reqEarlyStartWorkRequestId');
		entity.reqOvertimeWorkRequestId = createDummyRequest('reqOvertimeWorkRequestId');
		entity.reqAbsenceRequestId = createDummyRequest('reqAbsenceRequestId');
		entity.inpDirectRequestId = createDummyRequest('inpDirectRequestId');
		entity.reqPatternRequestId = createDummyRequest('reqPatternRequestId');

		List<Id> resRequestIdList = entity.getApprovedRequestIdList();
		System.assertEquals(8, resRequestIdList.size());
		System.assertEquals(entity.reqLeave1RequestId, resRequestIdList.get(0));
		System.assertEquals(entity.reqLeave2RequestId, resRequestIdList.get(1));
		System.assertEquals(entity.reqHolidayWorkRequestId, resRequestIdList.get(2));
		System.assertEquals(entity.reqEarlyStartWorkRequestId, resRequestIdList.get(3));
		System.assertEquals(entity.reqOvertimeWorkRequestId, resRequestIdList.get(4));
		System.assertEquals(entity.reqAbsenceRequestId, resRequestIdList.get(5));
		System.assertEquals(entity.inpDirectRequestId, resRequestIdList.get(6));
		System.assertEquals(entity.reqPatternRequestId, resRequestIdList.get(7));
	}

	/**
	 * @description テスト用の各種勤怠申請IDを作成する
	 */
	private static Id createDummyRequest(String name) {
		AttDailyRequest__c request = new AttDailyRequest__c();
		request.Name = name;
		request.RequestType__c = AttRequestType.LEAVE.value;
		insert request;
		return request.id;
	}
}