/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description 経費申請タイプ別費目のリポジトリ Repository class for ExpReportTypeExpTypeLink
 */
public with sharing class ExpReportTypeExpTypeLinkRepository extends Repository.BaseRepository {

	/**　最大取得レコード件数 Max number of records to fetch */
	private final static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする Roll back transaction if there are any failures in saving */
	private final static Boolean ALL_SAVE = true;

	/** オブジェクト名 Object name */
	private static final String BASE_OBJECT_NAME = ExpReportTypeExpTypeLink__c.SObjectType.getDescribe().getName();

	/** クエリ実行時の項目 Fields to query */
	public enum SelectDetail {
		EXP_REPORT_TYPE_FIELD_NAME_LIST,
		EXP_TYPE_FIELD_NAME_LIST,
		EXP_TYPE_FIELD_DETAIL_LIST
	}

	/** Link Entity Order By */
	public enum ExpReportTypeExpTypeOrderBy {
		EXP_TYPE_NAME
	}

	/**
	 * 取得対象の項目名 Target fields to fetch data
	 */
	private static final List<String> GET_BASE_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpReportTypeExpTypeLink__c.Id,
			ExpReportTypeExpTypeLink__c.Name,
			ExpReportTypeExpTypeLink__c.ExpReportTypeId__c,
			ExpReportTypeExpTypeLink__c.ExpTypeId__c
		};

		GET_BASE_FIELD_LIST = Repository.generateFieldNameList(fieldList);
	}

	/**
	 * 取得対象の経費申請タイプオブジェクトの項目名 Name fields of ExpReportType object
	 */
	private static final List<String> GET_EXP_REPORT_TYPE_FIELD_NAME_LIST;
	/** 経費申請タイプのリレーション名 Relationship name of ExpReportType */
	private static final String EXP_REPORT_TYPE_R = ExpReportTypeExpTypeLink__c.ExpReportTypeId__c.getDescribe().getRelationshipName();
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpReportType__c.Name_L0__c,
			ExpReportType__c.Name_L1__c,
			ExpReportType__c.Name_L2__c};

		GET_EXP_REPORT_TYPE_FIELD_NAME_LIST = Repository.generateFieldNameList(EXP_REPORT_TYPE_R, fieldList);
	}

	/**
	 * 取得対象の費目オブジェクトの項目名 Name fields of ExpType object
	 */
	private static final List<String> GET_EXP_TYPE_FIELD_NAME_LIST;
	/** 費目のリレーション名 Relationship name of ExpType */
	private static final String EXP_TYPE_R = ExpReportTypeExpTypeLink__c.ExpTypeId__c.getDescribe().getRelationshipName();
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpType__c.Name_L0__c,
			ExpType__c.Name_L1__c,
			ExpType__c.Name_L2__c};

		GET_EXP_TYPE_FIELD_NAME_LIST = Repository.generateFieldNameList(EXP_TYPE_R, fieldList);
	}

	/**
	 * 取得対象の費目オブジェクトの項目 Fields of ExpType object
	 */
	private static final List<String> GET_EXP_TYPE_FIELD_DETAIL_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpType__c.Code__c,
			ExpType__c.RecordType__c,
			ExpType__c.ValidFrom__c,
			ExpType__c.ValidTo__c};

		GET_EXP_TYPE_FIELD_DETAIL_LIST = Repository.generateFieldNameList(EXP_TYPE_R, fieldList);
	}

	/** 検索フィルタ Search Filter */
	public class SearchFilter {
		/** Accounting Period end date */
		public AppDate endDate;
		/** 経費申請タイプIDセット Set of ExpReportType Ids */
		public Set<Id> expReportTypeIds;
		/** Expense Type Ids */
		public Set<Id> expTypeIds;
		/** 経費申請タイプ別費目IDセット Set of ExpReportTypeExpTypeLink Ids */
		public Set<Id> ids;
		/** Accounting Period start date */
		public AppDate startDate;
		/** Target Date (Record Date) */
		public AppDate targetDate;
	}

	/**
	 * 指定したIDを持つ経費申請タイプ別費目を取得する Retrieve entity by the specified Id
	 * @param id 取得対象のID ID of the target entity
	 * @return 指定したIDを持つ経費申請タイプ別費目、ただし該当する経費申請タイプ別費目が存在しない場合はnull Entity of specified ID. If no data is found, return null.
	 */
	public ExpReportTypeExpTypeLinkEntity getEntity(Id id) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>{id};
		List<ExpReportTypeExpTypeLinkEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 指定したIDを持つ経費申請タイプ別費目を取得する Retrieve entities by the specified Ids
	 * @param idList 取得対象のID List of IDs of the target entity
	 * @return 指定したIDを持つ経費申請タイプ別費目、該当する経費申請タイプ別費目が存在しない場合は空のリストを返却する List of Entities. If no data is found, return an empty list.
	 */
	public List<ExpReportTypeExpTypeLinkEntity> getEntityList(List<Id> idList) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>(idList);
		return searchEntityList(filter);
	}

	/**
	 * 指定した経費申請タイプIDを持つ経費申請タイプ別費目を取得する Retrieve entities by the specified ExpReportType Id
	 * @param idList 取得対象の経費申請タイプID ID of the target ExpReportType
	 * @return 指定したIDを持つ経費申請タイプ別費目、該当する経費申請タイプ別費目が存在しない場合は空のリストを返却する  List of Entities. If no data is found, return an empty list.
	 */
	public List<ExpReportTypeExpTypeLinkEntity> getEntityListByExpReportTypeId(Set<Id> expReportTypeIdSet) {
		SearchFilter filter = new SearchFilter();
		filter.expReportTypeIds = expReportTypeIdSet;
		return searchEntityList(filter);
	}

	/**
	 * 指定した経費申請タイプIDを持つ経費申請タイプ別費目を取得する Retrieve entities by the specified ExpReportType Id
	 * @param idList 取得対象の経費申請タイプID ID of the target ExpReportType
	 * @param selectDetails Set of SelectDetail to indicate level of detail to query
	 * @return 指定したIDを持つ経費申請タイプ別費目、該当する経費申請タイプ別費目が存在しない場合は空のリストを返却する  List of Entities. If no data is found, return an empty list.
	 */
	public List<ExpReportTypeExpTypeLinkEntity> getEntityListByExpReportTypeId(Id expReportTypeId, Set<ExpReportTypeExpTypeLinkRepository.SelectDetail> selectDetails) {
		SearchFilter filter = new SearchFilter();
		filter.expReportTypeIds = new Set<Id>{expReportTypeId};
		return searchEntityList(filter, selectDetails, null);
	}

	/**
	 * 検索フィルタの条件に一致する経費申請タイプ別費目を取得する。Search ExpReportTypeExpTypeLinks
	 * @param filter 検索フィルタ Search filter
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト Search result
	 */
	public List<ExpReportTypeExpTypeLinkEntity> searchEntityList(SearchFilter filter) {
		return searchEntityList(filter, null, null);
	}

	/**
	 * Search ExpReportTypeExpTypeLinks based on the filter conditions
	 * @param filter 検索フィルタ Search filter
	 * @param selectDetails Set of SelectDetail to indicate level of detail to query
	 * Note: If selectDetails = null, all fields are retrieved. If selectDetails is an empty set, only the base fields are retrieved
	 * @param orderBy Field to order the return result
	 * Note: If orderBy = null; default order by ReportType Code
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト Search result
	 */
	public List<ExpReportTypeExpTypeLinkEntity> searchEntityList(SearchFilter filter,
			 Set<ExpReportTypeExpTypeLinkRepository.SelectDetail> selectDetails, ExpReportTypeExpTypeOrderBy orderBy) {
		return searchEntityList(filter, selectDetails, 0,  orderBy);
	}

	/**
	 * Search ExpReportTypeExpTypeLinks based on the filter conditions
	 * @param filter 検索フィルタ Search filter
	 * @param selectDetails Set of SelectDetail to indicate level of detail to query
	 * Note: If selectDetails = null, all fields are retrieved. If selectDetails is an empty set, only the base fields are retrieved
	 * @param maxLimit Number of records to retrieve (If maxLimit is zero max number search is return)
	 * @param orderBy Field to order the return result
	 * Note: If orderBy = null; default order by ReportType Code
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト Search result
	 */
	public List<ExpReportTypeExpTypeLinkEntity> searchEntityList(SearchFilter filter,
			Set<ExpReportTypeExpTypeLinkRepository.SelectDetail> selectDetails, Integer maxLimit, ExpReportTypeExpTypeOrderBy orderBy) {

		// SELECT対象項目リスト Target fields
		List<String> selectFieldList = new List<String>();

		// 基本項目 ExpReportTypeExpTypeLink base fields must be retrieved
		selectFieldList.addAll(GET_BASE_FIELD_LIST);

		// Note the following for selectDetails set
		// null = retrieve every field
		// empty = retrieve only the base fields
		if (selectDetails == null || selectDetails.contains(ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_REPORT_TYPE_FIELD_NAME_LIST)) {
			// 経費申請タイプ項目 ExpReportType name fields
			selectFieldList.addAll(GET_EXP_REPORT_TYPE_FIELD_NAME_LIST);
		}
		if (selectDetails == null || selectDetails.contains(ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_TYPE_FIELD_NAME_LIST)) {
			// 費目項目 ExpType name fields
			selectFieldList.addAll(GET_EXP_TYPE_FIELD_NAME_LIST);
		}
		if (selectDetails == null || selectDetails.contains(ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_TYPE_FIELD_DETAIL_LIST)) {
			// 費目項目 ExpType detail fields
			selectFieldList.addAll(GET_EXP_TYPE_FIELD_DETAIL_LIST);
		}

		// WHERE句を生成 Create WHERE statement
		List<String> conditions = new List<String>();
		final Set<Id> ids = filter.ids;
		if (ids != null) {
			conditions.add(createInExpression(ExpReportTypeExpTypeLink__c.Id, 'ids'));
		}
		final Set<Id> expReportTypeIds = filter.expReportTypeIds;
		if (expReportTypeIds != null) {
			conditions.add(createInExpression(ExpReportTypeExpTypeLink__c.ExpReportTypeId__c, 'expReportTypeIds'));
		}
		final Set<Id> expTypeIds = filter.expTypeIds;
		if (expTypeIds != null) {
			conditions.add(createInExpression(ExpReportTypeExpTypeLink__c.ExpTypeId__c, 'expTypeIds'));
		}

		if (filter.targetDate != null) {
			final Date pTargetDate = filter.targetDate.getDate();
			conditions.add( getFieldName(EXP_TYPE_R, ExpType__c.ValidFrom__c) + '<= :pTargetDate');
			conditions.add( getFieldName(EXP_TYPE_R, ExpType__c.ValidTo__c) + ' > :pTargetDate' );
		}

		if (filter.startDate != null) {
			final Date pStartDate = filter.startDate.getDate();
			final Date pEndDate = filter.endDate == null ? pStartDate : filter.endDate.getDate();
			conditions.add( getFieldName(EXP_TYPE_R, ExpType__c.ValidTo__c) + ' > :pStartDate' );
			conditions.add( getFieldName(EXP_TYPE_R, ExpType__c.ValidFrom__c) + '<= :pEndDate');
		}

		String orderByString;
		if (orderBy == ExpReportTypeExpTypeOrderBy.EXP_TYPE_NAME) {
			orderByString = getFieldName(EXP_TYPE_R, ExpType__c.Name_L0__c) + ' ASC';
		} else {
			orderByString = getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.Code__c) + ' ASC';
		}

		Integer pLimit = maxLimit > 0 ? maxLimit : SEARCH_RECORDS_NUMBER_MAX;

		// SOQLを生成 Create SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + BASE_OBJECT_NAME
				+ buildWhereString(conditions)
				+ ' ORDER BY ' + orderByString
				+ ',' + getFieldName(EXP_TYPE_R, ExpType__c.Code__c) + ' ASC'
				+ ' LIMIT :pLimit';
		System.debug('ExpReportTypeExpTypeLinkRepository: SOQL=' + soql);

		// クエリ実行 Execute query
		List<ExpReportTypeExpTypeLink__c> sObjList = Database.query(soql);
		List<ExpReportTypeExpTypeLinkEntity> entityList = new List<ExpReportTypeExpTypeLinkEntity>();
		for (ExpReportTypeExpTypeLink__c sobj : sObjList) {
			entityList.add(createEntity(sobj, selectDetails));
		}
		return entityList;
	}

	/**
	 * 経費申請タイプ別費目を保存する Save ExpReportTypeExpTypeLink
	 * @param entity 保存対象の経費申請タイプ別費目 ExpReportTypeExpTypeLink entity
	 * @return 保存結果 Save Result
	 */
	 public Repository.SaveResult saveEntity(ExpReportTypeExpTypeLinkEntity entity) {
		 return saveEntityList(new List<ExpReportTypeExpTypeLinkEntity>{entity});
	 }

	/**
	 * 経費申請タイプ別費目を保存する Save ExpReportTypeExpTypeLink
	 * @param entityList 保存対象の経費申請タイプ別費目 List of ExpReportTypeExpTypeLink entities
	 * @return 保存結果 Save Result
	 */
	 public Repository.SaveResult saveEntityList(List<ExpReportTypeExpTypeLinkEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	 }

	 /**
	 * エンティティを保存する Save entity
	 * @param entity 保存対象のエンティティのリスト List of entities
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる If set to false, DML operations can complete even if a record fails
	 * @return 保存結果のリスト List of saved results
	 */
	public virtual Repository.SaveResult saveEntityList(List<ExpReportTypeExpTypeLinkEntity> entityList, Boolean allOrNone) {

		// エンティティをSObjectに変換する
		List<ExpReportTypeExpTypeLink__c> sObjList = new List<ExpReportTypeExpTypeLink__c>();
		for (ExpReportTypeExpTypeLinkEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}

		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/**
	 * Get Entity List by Expense Type Id
	 * @param expTypeId Expense Type Id
	 * @param selectDetails Set of SelectDetail to indicate level of detail to query
	 * @param requiredSize Retrieve Size required
	 * @param orderBy Field to order the return result
	 * Note: If orderBy = null; default order by ReportType Code
	 */
	public List<ExpReportTypeExpTypeLinkEntity> getEntityListByExpTypeId(Id expTypeId,  Set<ExpReportTypeExpTypeLinkRepository.SelectDetail> selectDetails,
			 Integer requiredSize, ExpReportTypeExpTypeOrderBy orderBy){
		SearchFilter filter = new SearchFilter();
		filter.expTypeIds = new Set<Id>{expTypeId};
		List<ExpReportTypeExpTypeLinkEntity> entities = searchEntityList(filter, selectDetails, requiredSize, orderBy);
		return entities.isEmpty() ? null : entities;
	}


	/**
	 * エンティティからSObjectを作成する Create object from entity
	 * @param entity 作成元のエンティティ（nullは許容しない）Target entity to create (cannot be null)
	 * @return 作成したsObject Created Object
	 */
	private ExpReportTypeExpTypeLink__c createSObject(ExpReportTypeExpTypeLinkEntity entity) {
		ExpReportTypeExpTypeLink__c sobj = new ExpReportTypeExpTypeLink__c(Id = entity.Id);

		if (entity.isChanged(ExpReportTypeExpTypeLinkEntity.Field.EXP_REPORT_TYPE_ID)) {
			sobj.ExpReportTypeId__c = entity.expReportTypeId;
		}
		if (entity.isChanged(ExpReportTypeExpTypeLinkEntity.Field.EXP_TYPE_ID)) {
			sobj.ExpTypeId__c = entity.expTypeId;
		}

		return sobj;
	}

	/**
	 * ExpReportTypeExpTypeLink__cオブジェクトからエンティティを作成する Create entity from object
	 * @param sObj 作成元のSObject Target object to create
	 * @return 作成したエンティティ Created entity
	 */
	private ExpReportTypeExpTypeLinkEntity createEntity(ExpReportTypeExpTypeLink__c sObj, Set<ExpReportTypeExpTypeLinkRepository.SelectDetail> selectDetails) {
		ExpReportTypeExpTypeLinkEntity entity = new ExpReportTypeExpTypeLinkEntity();

		entity.setId(sObj.Id);
		entity.expReportTypeId = sObj.ExpReportTypeId__c;
		entity.expTypeId = sObj.ExpTypeId__c;

		Boolean includeAllFields = (selectDetails == null) ? true : false;

		// ExpReportType names
		if (includeAllFields || selectDetails.contains(ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_REPORT_TYPE_FIELD_NAME_LIST)) {
			entity.expReportTypeNameL = new AppMultiString(
					sObj.ExpReportTypeId__r.Name_L0__c,
					sObj.ExpReportTypeId__r.Name_L1__c,
					sObj.ExpReportTypeId__r.Name_L2__c);
		}
		
		// ExpType names
		if (includeAllFields || selectDetails.contains(ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_TYPE_FIELD_NAME_LIST)) {
			entity.expTypeNameL = new AppMultiString(
						sObj.ExpTypeId__r.Name_L0__c,
						sObj.ExpTypeId__r.Name_L1__c,
						sObj.ExpTypeId__r.Name_L2__c);
		}

		// ExpType details
		if (includeAllFields || selectDetails.contains(ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_TYPE_FIELD_DETAIL_LIST)) {
			entity.expTypeCode = sObj.ExpTypeId__r.Code__c;
			entity.expTypeRecordType = ExpRecordType.valueOf(sObj.ExpTypeId__r.RecordType__c);
			entity.expTypeValidFrom = AppDate.valueOf((Date)sObj.ExpTypeId__r.ValidFrom__c);
			entity.expTypeValidTo = AppDate.valueOf((Date)sObj.ExpTypeId__r.ValidTo__c);
		}

		/** TODO: Add additional fields to set in the entity if required */

		entity.resetChanged();

		return entity;
	}
}
