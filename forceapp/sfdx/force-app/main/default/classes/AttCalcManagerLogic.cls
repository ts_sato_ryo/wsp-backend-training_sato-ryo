/**
  * @author TeamSpirit Inc.
  * @date 2018
  *
  * @group 勤怠
  *
  * 管理監督者労働制の勤怠計算ロジック
  */
public with sharing class AttCalcManagerLogic extends AttCalcLogic {
	AttCalcOverTimeLogic calcOverTimeLogic = new AttCalcOverTimeLogic();
	public AttCalcManagerLogic(iOutput output, iInput input) {
		super(output, input);
	}
	// 週次計算で実際には月次の積み上げと残業計算を行ないます
	public override Date getStartDateWeekly(Date d) {
		return input.getStartDateMonthly(d);
	}
	private class CalcAttendanceDailyInput extends AttCalcOverTimeLogic.Input {
		private Day day;
		private Config conf;
		public CalcAttendanceDailyInput(AttCalcLogic.iInput input, Day day) {
			this.day = day;
			this.conf = input.getConfig(day.getDate());
			this.inputStartTime = day.input.inputStartTime;
			this.inputEndTime = day.input.inputEndTime;
			this.inputRestRanges = day.input.inputRestRanges;
			this.startTime = day.input.startTime;
			this.endTime = day.input.endTime;
			this.contractWorkTime = day.input.contractWorkTime;
			this.unpaidLeaveRestRanges = day.input.unpaidLeaveRestRanges;
			this.unpaidLeaveTime = day.input.unpaidLeaveTime;
			this.permitedLostTimeRanges = day.input.permitedLostTimeRanges;
			this.convertdRestRanges = day.input.convertdRestRanges;
			this.convertedWorkTime = day.input.convertedWorkTime;
			this.allowedWorkRanges = day.input.allowedWorkRanges;
			this.contractRestRanges = day.input.contractRestRanges ;
			this.contractRestTime = day.input.contractRestTime;
			this.legalWorkTime = conf.LegalWorkTimeDaily;
			this.isWorkDay = day.isWorkDay();
			this.isLegalHoliday = day.input.isLegalHoliday;
			this.isNoOffsetOvertimeByUndertime = conf.isNoOffsetOvertimeByUndertime;
			this.isPermitOvertimeWorkUntilNormalHours = conf.isPermitOvertimeWorkUntilNormalHours;
			this.isIncludeCompensationTimeToWorkHours = conf.isIncludeCompensationTimeToWorkHours;
		}

		public override String toString() {
			return day.toString();
		}
	}



	// フレックスの日次勤怠計算は、
	// 所定の観点で所定内-所定外を区別しない。所定内に集計する
	// 法定の観点で法定内と法定外は区別しない。法定内に集計するが、法定休日は区別する
	// フレックで法定休日所定内がありうるか？ ない？
	public override Boolean calcAttendanceDaily(Day day, Day dayNext) {
		// 勤務日は 所定内法定内 ... 所定勤務時間まで
		CalcAttendanceDailyInput tmpInput = new CalcAttendanceDailyInput(input, day);
		AttCalcOverTimeLogic.Output tmpOutput = new AttCalcOverTimeLogic.Output();
		calcOverTimeLogic.apply(tmpOutput, tmpInput);
		day.output.realWorkTime = tmpOutput.realWorkTime;
		day.output.convertedWorkTime = tmpOutput.convertedWorkTime;
		day.output.unpaidLeaveLostTime = 0;
		day.output.paidLeaveTime = 0;
		day.output.unpaidLeaveTime = 0;
		day.output.lateArriveTime = 0;
		day.output.earlyLeaveTime = 0;
		day.output.breakTime = tmpOutput.breakTime;
		day.output.lateArriveLostTime = 0;
		day.output.earlyLeaveLostTime = 0;
		day.output.breakLostTime = 0;
		day.output.authorizedLateArriveTime = 0;
		day.output.authorizedEarlyLeaveTime = 0;
		day.output.authorizedBreakTime = 0;

		AttCalcRangesDto inContractInLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractInLegal);
		AttCalcRangesDto inContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractOutLegal);
		AttCalcRangesDto outContractInLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.outContractInLegal);
		AttCalcRangesDto outContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.outContractOutLegal);

		AttCalcRangesDto inContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto inContractOutLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractOutLegal_nextDay = AttCalcRangesDto.RS();

		if (!getCanCalcContinueNextDay(day, dayNext)) {
			Integer boundaryTime = input.getConfig(day.getDate()).boundaryTimeOfDay;
			boundaryTime += 24 * 60; // 翌日かを判定するため、24時間後にする
			inContractInLegal_nextDay = inContractInLegal.cutAfter(boundaryTime);
			inContractOutLegal_nextDay = inContractOutLegal.cutAfter(boundaryTime);
			outContractInLegal_nextDay = outContractInLegal.cutAfter(boundaryTime);
			outContractOutLegal_nextDay = outContractOutLegal.cutAfter(boundaryTime);


			inContractInLegal = inContractInLegal.cutBefore(boundaryTime);
			inContractOutLegal = inContractOutLegal.cutBefore(boundaryTime);
			outContractInLegal = outContractInLegal.cutBefore(boundaryTime);
			outContractOutLegal = outContractOutLegal.cutBefore(boundaryTime);
		}
		day.output.legalInTime = 0;
		day.output.legalOutTime = 0;

		// 代休発生時間と相殺しながら勤務時間の出力を行う
		outputWorkTime(day, day, inContractInLegal);
		outputWorkTime(day, dayNext, inContractInLegal_nextDay);
		outputWorkTime(day, day, inContractOutLegal);
		outputWorkTime(day, dayNext, inContractOutLegal_nextDay);
		outputWorkTime(day, day, outContractOutLegal);
		outputWorkTime(day, dayNext, outContractOutLegal_nextDay);
		outputWorkTime(day, day, outContractInLegal);
		outputWorkTime(day, dayNext, outContractInLegal_nextDay);

		Boolean withStartEndTime = (day.input.inputStartTime != null || day.input.inputEndTime != null);
		day.output.realWorkDay = (withStartEndTime ? 1 : 0);
		day.output.legalHolidayWorkCount = (day.isLegalHoliday() && withStartEndTime ? 1 : 0);
		day.output.holidayWorkCount = (day.isHoliday() && withStartEndTime ? 1 : 0);

		// 勤務時間内訳(翌日分とマージする)
		AttCalcRangesDto inContractInLegalRanges = inContractInLegal.insertAndMerge(inContractInLegal_nextDay);
		AttCalcRangesDto inContractOutLegalRanges = inContractOutLegal.insertAndMerge(inContractOutLegal_nextDay);
		AttCalcRangesDto outContractInLegalRanges = outContractInLegal.insertAndMerge(outContractInLegal_nextDay);
		AttCalcRangesDto outContractOutLegalRanges = outContractOutLegal.insertAndMerge(outContractOutLegal_nextDay);

		// 管理監督者は全て所定内法定内とみなしてグラフ表示をする
		day.output.inContractInLegalRanges.insertAndMerge(inContractInLegalRanges);
		day.output.inContractInLegalRanges.insertAndMerge(inContractOutLegalRanges);
		day.output.inContractInLegalRanges.insertAndMerge(outContractInLegalRanges);
		day.output.inContractInLegalRanges.insertAndMerge(outContractOutLegalRanges);

		return true;
	}

	// 週単位勤怠計算処理(フレックスタイム制)
	public override List<Date> calcAttendanceWeekly(Date d) {
		List<Date> updateDateList = new List<Date>();
		updateDateList.add(d);
		return updateDateList;
	}
	// 月次勤怠計算処理(フレックス時間制)
	public override void calcAttendanceMonthly(Date d) {
		Date startDate = input.getStartDateMonthly(d);
		Date endDate = input.getEndDateMonthly(d);
		TotalOutput totalOutput = new TotalOutput();
		for (Date dt = startDate; dt <= endDate; dt = dt.addDays(1)) {
			Day day = db.get(dt);
			if (day == null) {
				continue;
			}
			totalOutput.realWorkTime += day.output.realWorkTime;
			totalOutput.convertedWorkTime += day.output.convertedWorkTime;
			totalOutput.unpaidLeaveLostTime += day.output.unpaidLeaveLostTime;
			totalOutput.paidLeaveTime += day.output.paidLeaveTime;
			totalOutput.unpaidLeaveTime += day.output.unpaidLeaveTime;
			totalOutput.lateArriveTime += day.output.lateArriveTime;
			totalOutput.earlyLeaveTime += day.output.earlyLeaveTime;
			totalOutput.breakTime += day.output.breakTime;
			totalOutput.lateArriveLostTime += day.output.lateArriveLostTime;
			totalOutput.earlyLeaveLostTime += day.output.earlyLeaveLostTime;
			totalOutput.breakLostTime += day.output.breakLostTime;
			totalOutput.authorizedLateArriveTime += day.output.authorizedLateArriveTime;
			totalOutput.authorizedEarlyLeaveTime += day.output.authorizedEarlyLeaveTime;
			totalOutput.authorizedBreakTime += day.output.authorizedBreakTime;
			totalOutput.paidBreakTime += day.output.paidBreakTime;
			totalOutput.authorizedOffTime += day.output.authorizedOffTime;
			totalOutput.legalOutTime += day.output.legalOutTime  + day.output.regularOverTime;
			totalOutput.legalInTime += day.output.legalInTime - day.output.regularOverTime;
			totalOutput.regularRateWorkTime += day.output.regularRateWorkTime;
			totalOutput.realWorkDay += day.output.realWorkDay;
			totalOutput.workDay += day.output.workDay;
			totalOutput.legalHolidayWorkCount += day.output.legalHolidayWorkCount;
			totalOutput.holidayWorkCount += day.output.holidayWorkCount;
			for (WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
				Integer work = day.output.workTimeCategoryMap.get(c);
				Integer sum = totalOutput.workTimeCategoryMap.containsKey(c)
									? totalOutput.workTimeCategoryMap.get(c)
									: 0;
				totalOutput.workTimeCategoryMap.put(c, sum + work);
			}
			for (Integer j = 0; j < day.output.customWorkTimes.size(); j++)
				totalOutput.customWorkTimes[j] += day.output.customWorkTimes[j];
		}
		output.updateTotal(startDate, totalOutput);
	}

	public override void calcAttendanceCustom(Day day) {
		for(Integer i = 0; i < day.output.customWorkTimes.size(); i++)
			day.output.customWorkTimes[i] = 0;
		for(WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
			Integer work = day.output.workTimeCategoryMap.get(c);
			if(c.isNight)
				day.output.customWorkTimes[5] += work;
		}
	}
	// 労働時間出力
	private void outputWorkTime(Day outputDay, Day adjustmentDay, AttCalcRangesDto workTimeRanges) {
		if (workTimeRanges.total() > 0) {
			outputDay.output.legalInTime += workTimeRanges.total();
			outputDay.output.regularRateWorkTime += workTimeRanges.total();

			// ------------------
			// 日中の勤務時間を計算する
			// ------------------
			Integer dayTime = workTimeRanges.include(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			// 残りの時間を出力する
			Integer dayTimeSum = getOrDefaultTime(outputDay.output.workTimeCategoryMap, new WorkTimeCategory(false, false, adjustmentDay.getDayType(), false, false));
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(false, false, adjustmentDay.getDayType(), false, false), dayTime + dayTimeSum);

			// ------------------
			// 夜間の勤務時間を計算する
			// ------------------
			Integer nightTime = workTimeRanges.exclude(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			Integer nightTimeSum = getOrDefaultTime(outputDay.output.workTimeCategoryMap,new WorkTimeCategory(false, false, adjustmentDay.getDayType(), true, false));
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(false, false, adjustmentDay.getDayType(), true, false), nightTime + nightTimeSum);
		}
	}
	// 空の初期値を返せる処理
	private AttCalcRangesDto getOrDefaultRanges(
			Map<AttCalcOverTimeLogic.OverTimeCategory,
			AttCalcRangesDto> rangesMap, AttCalcOverTimeLogic.OverTimeCategory key) {
		if (rangesMap.containsKey(key)) {
			return rangesMap.get(key);
		}
		return AttCalcRangesDto.RS();
	}
	// 存在しない労働時間を0を返す処理
	private Integer getOrDefaultTime(
			Map<WorkTimeCategory, Integer> workTimeCategoryMap,
			WorkTimeCategory key) {
		return workTimeCategoryMap.containsKey(key)
				? workTimeCategoryMap.get(key)
				: 0;
	}
}