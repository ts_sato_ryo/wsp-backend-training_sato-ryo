/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Entity Class for ExpAccountingPeriod Object
 */
public class ExpAccountingPeriodEntity extends ExpAccountingPeriodGeneratedEntity {

	/** Max length of the code field */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** Max length of the name field */
	public static final Integer NANE_MAX_LENGTH = 80;

	/**
	 * Constructor
	 */
	public ExpAccountingPeriodEntity() {
		super(new ExpAccountingPeriod__c());
	}

	/**
	 * Constructor
	 */
	public ExpAccountingPeriodEntity(ExpAccountingPeriod__c sobj) {
		super(sobj);
	}

	/** Name(Multilingual) */
	public AppMultiString nameL {
		get {return new AppMultiString(nameL0, nameL1, nameL2);}
	}
}