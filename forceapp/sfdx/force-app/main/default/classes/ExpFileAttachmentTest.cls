/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * 経費領収書添付 Test class for Expense File Attachment Picklist
 */
@isTest
private class ExpFileAttachmentTest {

	/**
	 * Check if it compare properly
	 */
	@isTest static void equalsTest() {

		// Equal
		System.assertEquals(ExpFileAttachment.REQUIRED, ExpFileAttachment.valueOf('Required'));
		System.assertEquals(ExpFileAttachment.OPTIONAL, ExpFileAttachment.valueOf('Optional'));
		System.assertEquals(ExpFileAttachment.NOT_USED, ExpFileAttachment.valueOf('NotUsed'));

		// Values are different
		System.assertEquals(false, ExpFileAttachment.REQUIRED.equals(ExpFileAttachment.valueOf('NotUsed')));

		// Class type is different
		System.assertEquals(false, ExpFileAttachment.REQUIRED.equals(Integer.valueOf(123)));

		// Value is null
		System.assertEquals(false, ExpFileAttachment.REQUIRED.equals(null));
	}
}
