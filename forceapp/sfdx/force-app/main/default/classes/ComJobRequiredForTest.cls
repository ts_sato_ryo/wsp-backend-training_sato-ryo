/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Test class for ComJobRequiredFor
 */
@isTest
private class ComJobRequiredForTest {

    /**
	 * Check if it compare properly
	 */
	@isTest static void equalsTest() {

		// Equal
		System.assertEquals(ComJobRequiredFor.EXPENSE_REPORT, ComJobRequiredFor.valueOf('ExpenseReport'));
		System.assertEquals(ComJobRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ComJobRequiredFor.valueOf('ExpenseRequestAndExpenseReport'));

		// Values are different
		System.assertEquals(false, ComJobRequiredFor.EXPENSE_REPORT.equals(ComJobRequiredFor.valueOf('ExpenseRequestAndExpenseReport')));

		// Class type is different
		System.assertEquals(false, ComJobRequiredFor.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// Value is null
		System.assertEquals(false, ComJobRequiredFor.EXPENSE_REPORT.equals(null));
	}
}
