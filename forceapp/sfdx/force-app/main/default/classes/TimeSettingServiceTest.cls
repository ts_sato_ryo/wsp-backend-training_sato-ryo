/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 工数
 *
 * @description TimeSettingServiceのテスト
 */
@isTest
private class TimeSettingServiceTest {

	/**
	 * テストデータ
	 */
	private class TestData extends TestData.TestDataEntity {

		public TimeSettingHistoryEntity createTimSettingHistory(
				TimeSettingHistoryEntity orgEntity, AppDate validFrom, AppDate validTo) {

			TimeSettingHistoryEntity newEntity = orgEntity.copy();
			newEntity.validFrom = validFrom;
			newEntity.validTo = validTo;
			newEntity.uniqKey = newEntity.createUniqKey(newEntity.name);

			return newEntity;
		}
	}

	/**
	 * ベースエンティティが更新できることを確認する
	 */
	@isTest static void updateBaseTest() {
		TimeSettingServiceTest.TestData testData = new TimeSettingServiceTest.TestData();

		// 更新内容を設定する
		TimeSettingBaseEntity updateBase = new TimeSettingBaseEntity();
		updateBase.setId(testData.timeSetting.id);
		updateBase.startDateOfMonth = testData.timeSetting.startDateOfMonth+ 1;

		new TimeSettingService().updateBase(updateBase);

		// 更新されていることを確認する
		TimeSettingBaseEntity resBase = new TimeSettingRepository().getBaseEntity(updateBase.id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(updateBase.startDateOfMonth, resBase.startDateOfMonth);
		// 他の項目は更新されていない(更新前の値のまま)
		System.assertEquals(testData.timeSetting.summaryPeriod, resBase.summaryPeriod);
		System.assertEquals(testData.timeSetting.monthMark, resBase.monthMark);
	}

	/**
	 * 履歴が改定できることを確認する
	 * 改定内容が伝搬されることを確認する
	 */
	@isTest static void reviseHistoryTest() {
		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingServiceTest.TestData testData = new TimeSettingServiceTest.TestData();

		TimeSettingBaseEntity base = testData.createTimeSetting('伝搬テスト');
		base.getHistory(0).validFrom = AppDate.today();
		base.getHistory(0).validTo = base.getHistory(0).validFrom.addMonths(1);

 		final Integer historyNum = 5;
		AppDate validFrom = base.getHistory(0).validTo;
		AppDate validTo = validFrom.addMonths(1);
		for (Integer i = 1; i < historyNum; i++) {
			base.addHistory(testData.createTimSettingHistory(base.getHistory(0), validFrom, validTo));

			validFrom = validTo;
			validTo = validFrom.addMonths(1);
		}
		repo.saveHistoryEntityList(base.getHistoryList());
		base = repo.getEntity(base.Id);

		validFrom = base.getHistory(1).validFrom.addDays(10);
		validTo = validFrom.addDays(10);
		TimeSettingHistoryEntity targetHistory = new TimeSettingHistoryEntity();
		targetHistory.baseId = base.id;
		targetHistory.validFrom = validFrom;
		targetHistory.validTo = validTo;
		targetHistory.historyComment = '改定履歴コメント'; // 履歴管理項目は伝搬されないはず
		targetHistory.nameL0 = targetHistory.nameL0 + '_改定後';
		targetHistory.nameL2 = targetHistory.nameL2 + '_改定後';

		// 末尾の履歴のName_L2を変更しておく
		base.getHistory(historyNum - 1).nameL2 = targetHistory.nameL2 + '_改定前に変更';
		repo.saveHistoryEntity(base.getHistory(historyNum - 1));

		// 改定実行
		TimeSettingService service = new TimeSettingService();
		service.reviseHistory(targetHistory);


		List<TimeSettingHistoryEntity> resHistoryList = repo.getEntity(base.id).getHistoryList();
		System.assertEquals(historyNum + 1, resHistoryList.size());
		// 改定されていることを確認
		System.assertEquals(validFrom, resHistoryList[1].validTo); // 改定前履歴
		System.assertEquals(validFrom, resHistoryList[2].validFrom); // 改定後履歴

		// // デバッグ
		// System.debug('>>> 改定後の履歴');
		// for (DepartmentHistoryEntity history : resHistoryList) {
		// 	System.debug(history.name + '(' + history.validFrom + '-' + history.validTo + '): managerId = ' + history.managerId + ', remarks =' + history.remarks);
		// }

		// 伝搬されていることを確認
		// 末尾の履歴のnameL2はすでに変更されているので伝搬対象外
		for (Integer i = 3; i < historyNum; i++) {
			System.assertEquals(targetHistory.nameL0, resHistoryList[i].nameL0);
			System.assertEquals(targetHistory.nameL2, resHistoryList[i].nameL2);

			// 履歴管理項目は伝搬されない
			System.assertEquals(base.getHistory(i - 1).historyComment, resHistoryList[i].historyComment);

		}

		// 改定前の履歴には伝搬されていない
		for (Integer i = 0; i <= 1; i++) {
			System.assertEquals(base.getHistory(i).nameL0, resHistoryList[i].nameL0);
		}

		// 末尾の履歴の工数設定名L2は既に変更されているので伝搬されない
		TimeSettingHistoryEntity resTailHistory = resHistoryList[resHistoryList.size() - 1];
		System.assertEquals(base.getHistory(historyNum - 1).nameL2, resTailHistory.nameL2);
		// 工数設定名L0は伝搬されている
		System.assertEquals(targetHistory.nameL0, resTailHistory.nameL0);
		// 工数設定名L1の値は変更されていない
		System.assertEquals(base.getHistory(historyNum - 1).nameL1, resTailHistory.nameL1);

		// 伝搬対象の履歴の元の履歴は論理削除されている
		List<TimeSettingHistory__c> removedList = [SELECT Id, ValidFrom__c FROM TimeSettingHistory__c WHERE BaseId__c = :base.Id AND Removed__c = true];
		// for (ComDeptHistory__c history : removedList) {
		// 	System.debug('Removed = ' + history.ValidFrom__c);
		// }
		System.assertEquals(historyNum - 2, removedList.size());

	}

	/**
	 * 工数設定ベースのコードの重複チェックが正しくできることを確認する
	 */
	@isTest static void isCodeDuplicatedTest() {
		TimeSettingServiceTest.TestData testData = new TimeSettingServiceTest.TestData();

		final Integer baseNum = 2;
		final Integer historyNum = 2;
		TimeSettingBaseEntity base0 = testData.createTimeSetting('テスト0');
		TimeSettingBaseEntity base1 = testData.createTimeSetting('テスト1');
		TimeSettingBaseEntity targetBase = base1;

		TimeSettingService service = new TimeSettingService();

		// 会社内で重複している → true
		targetBase.code = base0.code;
		System.assertEquals(true, service.isCodeDuplicated(targetBase));

		// 会社をまたいで重複している → false
		ComCompany__c ompanyObj2 = ComTestDataUtility.createCompany('Test2', testData.country.id);
		targetBase.companyId = ompanyObj2.Id;
		targetBase.code = base0.code;
		System.assertEquals(false, service.isCodeDuplicated(targetBase));

		// 組織全体でも重複していない → false
		targetBase.code = targetBase.code + 'Test';
		System.assertEquals(false, service.isCodeDuplicated(targetBase));
	}

	/**
	 * updateUniqKeyのテスト
	 * 会社IDまたはコードが変更されている場合はユニークキーが更新されることを確認する
	 */
	@isTest static void updateUniqKeyTestUpdateUniqKey() {
		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingServiceTest.TestData testData = new TimeSettingServiceTest.TestData();
		CompanyEntity testCompany = testData.createCompany('更新テスト');

		TimeSettingService service = new TimeSettingService();
		TimeSettingBaseEntity targetBase;

		// Test1: 会社IDが更新されている
		targetBase = testData.createTimeSetting('UniqKeyテスト1');
		targetBase.companyId = testCompany.id;
		service.updateUniqKey(targetBase);
		System.assertEquals(testCompany.code + '-' + targetBase.code, targetBase.uniqKey);

		// Test2: コードが更新されている
		targetBase = testData.createTimeSetting('UniqKeyテスト2');
		targetBase.code = 'Changed';
		service.updateUniqKey(targetBase);
		System.assertEquals(testData.company.code + '-' + targetBase.code, targetBase.uniqKey);
	}

	/**
	 * getTimeSettingのテスト
	 * 工数設定が取得できることを確認する
	 */
	@isTest
	static void getTimeSettingTest() {
		// テストデータ作成
		TimeTestData testData = new TimeTestData(AppDate.newInstance(2016,6,1));

		// 実行
		List<TimeSettingBaseEntity> actualList;
		try {
			Test.startTest();
			actualList = new TimeSettingService().getTimeSetting(
					testData.emp.id,
					new AppDateRange(AppDate.newInstance(2016,6,1), AppDate.newInstance(2016,6,30)));
			Test.stopTest();
		} catch (Exception e){
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}

		// 検証
		System.assertEquals(1, actualList.size());
		System.assertEquals(testData.timeSetting.Id, actualList[0].id);
	}
}
