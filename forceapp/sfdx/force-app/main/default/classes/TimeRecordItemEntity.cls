/**
 *
 * @group 工数
 *
 * 工数内訳のエンティティ
 */
public with sharing class TimeRecordItemEntity extends TimeRecordItemGeneratedEntity {

	/**
	 * コンストラクタ
	 */
	public TimeRecordItemEntity() {
		super(new TimeRecordItem__c());
	}

	/**
	 * コンストラクタ
	 */
	public TimeRecordItemEntity(TimeRecordItem__c sobj) {
		super(sobj);
	}

	/**
	 * 作業時間
	 * Time
	 * 値がnullの場合にデフォルト値を返すために、GeneratedEntityから除外しています。
	 */
	public static final Schema.SObjectField FIELD_WORK_TIME = TimeRecordItem__c.Time__c;
	public Integer workTime {
		get {
			Integer value = Integer.valueOf(getFieldValue(FIELD_WORK_TIME));
			return value == null ? 0 : value;
		}
		set {
			setFieldValue(FIELD_WORK_TIME, value);
		}
	}

	/** ジョブコード（参照のみ。値を変更してもDBに保存されない。） */
	public String jobCode {
		get {
			if (this.jobId == null) {
				return null;
			}
			if (jobCode == null) {
				jobCode = sObj.JobId__r.Code__c;
			}
			return jobCode;
		}
		private set;
	}

	/** ジョブ名（参照のみ。値を変更してもDBに保存されない。） */
	public String jobName {get {return AppMultiString.stringValue(jobNameL);}}
	public AppMultiString jobNameL {
		get {
			if (this.jobId == null) {
				return null;
			}
			if (jobNameL == null) {
				jobNameL = new AppMultiString(
					sObj.JobId__r.Name_L0__c,
					sObj.JobId__r.Name_L1__c,
					sObj.JobId__r.Name_L2__c);
			}
			return jobNameL;
		}
		private set;
	}

	/**
	 * (テスト用)ジョブ項目を一括セット
	 * @param jobId ジョブID
	 * @param jobCode ジョブコード
	 * @param jobNameL0 ジョブ名(L0)
	 * @param jobNameL1 ジョブ名(L1)
	 * @param jobNameL2 ジョブ名(L2)
	 */
	@TestVisible
	private void setJob(Id jobId, String jobCode, String jobNameL0, String jobNameL1, String jobNameL2) {
		this.jobId = jobId;
		this.jobCode = jobCode;
		this.jobNameL = new AppMultiString(jobNameL0, jobNameL1, jobNameL2);
	}

	/** ジョブ直課フラグ（参照のみ。値を変更してもDBに保存されない。） */
	public Boolean isDirectChargedJob {
		get {
			if (this.jobId == null) {
				return false;
			}
			if (isDirectChargedJob == null) {
				isDirectChargedJob = sObj.JobId__r.DirectCharged__c;
			}
			return isDirectChargedJob;
		}
		private set;
	}

	/** 作業分類コード（参照のみ。値を変更してもDBに保存されない。） */
	public String workCategoryCode {
		get {
			if (this.workCategoryId == null) {
				return null;
			}
			if (workCategoryCode == null) {
				workCategoryCode = sObj.WorkCategoryId__r.Code__c;
			}
			return workCategoryCode;
		}
		private set;
	}

	/** 作業分類名（参照のみ。値を変更してもDBに保存されない。） */
	public String workCategoryName {get {return AppMultiString.stringValue(workCategoryNameL);}}
	public AppMultiString workCategoryNameL {
		get {
			if (this.workCategoryId == null) {
				return null;
			}
			if (workCategoryNameL == null) {
				workCategoryNameL = new AppMultiString(
					sObj.WorkCategoryId__r.Name_L0__c,
					sObj.WorkCategoryId__r.Name_L1__c,
					sObj.WorkCategoryId__r.Name_L2__c);
			}
			return workCategoryNameL;
		}
		private set;
	}

	/**
	 * (テスト用)作業分類項目を一括セット
	 * @param workCategoryId 作業分類ID
	 * @param workCategoryCode 作業分類コード
	 * @param workCategoryNameL0 作業分類名(L0)
	 * @param workCategoryNameL1 作業分類名(L1)
	 * @param workCategoryNameL2 作業分類名(L2)
	 */
	@TestVisible
	private void setWorkCategory(
			Id workCategoryId, String workCategoryCode,
			String workCategoryNameL0, String workCategoryNameL1, String workCategoryNameL2) {
		this.workCategoryId = workCategoryId;
		this.workCategoryCode = workCategoryCode;
		this.workCategoryNameL = new AppMultiString(workCategoryNameL0, workCategoryNameL1, workCategoryNameL2);
	}
}