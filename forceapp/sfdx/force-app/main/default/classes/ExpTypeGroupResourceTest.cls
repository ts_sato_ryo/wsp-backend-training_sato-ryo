/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpTypeGroupResource
 */
@isTest
private class ExpTypeGroupResourceTest {

	/**
	 * Test data class
	 */
	private class TestData extends TestData.TestDataEntity {

		/**
		 * Create expense type group records
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Integer size) {
			return createExpTypeGroups(name, this.company.id, size);
		}

		/**
		 * Create expense type group records
		 */
		public List<ExpTypeGroupEntity> createExpTypeGroups(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpTypeGroups(name, companyId, size);
			return (new ExpTypeGroupRepository()).getEntityList(null);
		}
	}

	/**
	 * Test for CreateApi class
	 * Check if all field value of created record is correctly set
	 */
	@isTest static void createTestAllField() {
		ExpTypeGroupResourceTest.TestData testData = new TestData();

		ExpTypeGroupResource.ExpTypeGroupParam param = new ExpTypeGroupResource.ExpTypeGroupParam();
		final String name = 'CreateTest';
		param.name_L0 = name + '_L0';
		param.name_L1 = name + '_L1';
		param.name_L2 = name + '_L2';
		param.code = name;
		param.companyId = testData.company.id;
		param.validDateFrom = Date.today().addDays(1);
		param.validDateTo = param.validDateFrom.addMonths(12);
		param.parentId = null;
		param.description_L0 = 'Description_L0';
		param.description_L1 = 'Description_L1';
		param.description_L2 = 'Description_L2';
		param.order = 1;

		// execute API
		ExpTypeGroupResource.CreateApi api = new ExpTypeGroupResource.CreateApi();
		ExpTypeGroupResource.SaveResponse res = (ExpTypeGroupResource.SaveResponse)api.execute(param);

		// assertion
		ExpTypeGroupEntity resGroup = (new ExpTypeGroupRepository()).getEntity(res.id);
		System.assertNotEquals(null, resGroup);
		assertSaveAllField(param, resGroup);
	}

	/**
	 * Test for CreateApi class
	 * Check if the default value is set to object field, when the parameter value is null
	 */
	@isTest static void createTestDefaultValue() {
		ExpTypeGroupResourceTest.TestData testData = new TestData();

		ExpTypeGroupResource.ExpTypeGroupParam param = new ExpTypeGroupResource.ExpTypeGroupParam();
		final String name = 'CreateTest';
		param.name_L0 = name + '_L0';
		param.name_L1 = name + '_L1';
		param.name_L2 = name + '_L2';
		param.code = name;
		param.companyId = testData.company.id;
		param.parentId = null;
		param.description_L0 = 'Description_L0';
		param.description_L1 = 'Description_L1';
		param.description_L2 = 'Description_L2';
		param.order = 1;
		// For the following parameter, If the value is null, the default value is set to object field
		param.validDateFrom = null;
		param.validDateTo = null;

		// execute API
		ExpTypeGroupResource.CreateApi api = new ExpTypeGroupResource.CreateApi();
		ExpTypeGroupResource.SaveResponse res = (ExpTypeGroupResource.SaveResponse)api.execute(param);

		// assertion
		ExpTypeGroupEntity resGroup = (new ExpTypeGroupRepository()).getEntity(res.id);
		System.assertNotEquals(null, resGroup);
		System.assertEquals(AppDate.today(), resGroup.validFrom);
		System.assertEquals(ValidPeriodEntityOld.VALID_TO_MAX, resGroup.validTo);
	}

	/**
	 * Test for UpdateApi class
	 * Check if all fields of the target record are correctly updated
	 */
	@isTest static void updateTestAllField() {
		final Integer testGroupSize = 3;
		ExpTypeGroupResourceTest.TestData testData = new TestData();

		List<ExpTypeGroupEntity> testGroups = testData.createExpTypeGroups('Test', testGroupSize);
		ExpTypeGroupEntity targetGroup = testGroups[1];

		ExpTypeGroupResource.ExpTypeGroupParam param = new ExpTypeGroupResource.ExpTypeGroupParam();
		param.id = targetGroup.id;
		param.name_L0 = targetGroup.nameL0 + 'Update';
		param.name_L1 = targetGroup.nameL1 + 'Update';
		param.name_L2 = targetGroup.nameL2 + 'Update';
		param.code = targetGroup.id;
		param.companyId = testData.company.id;
		param.validDateFrom = AppDate.convertDate(targetGroup.validFrom.addDays(10));
		param.validDateTo = AppDate.convertDate(targetGroup.validTo.addDays(10));
		param.parentId = testGroups[0].id;
		param.description_L0 = targetGroup.descriptionL0 + 'Update';
		param.description_L1 = targetGroup.descriptionL1 + 'Update';
		param.description_L2 = targetGroup.descriptionL2 + 'Update';
		param.order = targetGroup.order + 10;

		Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(param));

		// execute API
		ExpTypeGroupResource.UpdateApi api = new ExpTypeGroupResource.UpdateApi();
		api.execute(paramMap);

		// assertion
		ExpTypeGroupEntity resGroup = (new ExpTypeGroupRepository()).getEntity(targetGroup.id);
		System.assertEquals(param.id, resGroup.id);
		assertSaveAllField(param, resGroup);
	}

	/**
	 * Test for UpdateApi class
	 * Check if exception is thrown if the id in request parameter is null
	 */
	@isTest static void updateTestIdNull() {
		final Integer testGroupSize = 3;
		ExpTypeGroupResourceTest.TestData testData = new TestData();

		ExpTypeGroupEntity targetGroup = testData.createExpTypeGroups('Test', testGroupSize).get(0);

		ExpTypeGroupResource.ExpTypeGroupParam param = new ExpTypeGroupResource.ExpTypeGroupParam();
		param.id = null;
		param.name_L0 = targetGroup.nameL0 + 'Update';
		param.name_L1 = targetGroup.nameL1 + 'Update';
		param.name_L2 = targetGroup.nameL2 + 'Update';
		param.code = targetGroup.id;
		param.companyId = testData.company.id;
		param.validDateFrom = AppDate.convertDate(targetGroup.validFrom.addDays(10));
		param.validDateTo = AppDate.convertDate(targetGroup.validTo.addDays(10));
		param.parentId = null;
		param.description_L0 = targetGroup.descriptionL0 + 'Update';
		param.description_L1 = targetGroup.descriptionL1 + 'Update';
		param.description_L2 = targetGroup.descriptionL2 + 'Update';
		param.order = targetGroup.order + 10;

		Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(param));

		// execute API
		ExpTypeGroupResource.UpdateApi api = new ExpTypeGroupResource.UpdateApi();
		try {
			api.execute(paramMap);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/**
	 * Check if each field of the record is correctly set
	 */
	private static void assertSaveAllField(ExpTypeGroupResource.ExpTypeGroupParam expParam, ExpTypeGroupEntity actEntity) {

		System.assertEquals(expParam.name_L0, actEntity.nameL0);
		System.assertEquals(expParam.name_L1, actEntity.nameL1);
		System.assertEquals(expParam.name_L2, actEntity.nameL2);
		System.assertEquals(expParam.code, actEntity.code);
		System.assertEquals(expParam.companyId, actEntity.companyId);
		System.assertEquals(expParam.validDateFrom, AppDate.convertDate(actEntity.validFrom));
		System.assertEquals(expParam.validDateTo, APpDate.convertDate(actEntity.validTo));
		System.assertEquals(expParam.parentId, actEntity.parentId);
		System.assertEquals(expParam.description_L0, actEntity.descriptionL0);
		System.assertEquals(expParam.description_L1, actEntity.descriptionL1);
		System.assertEquals(expParam.description_L2, actEntity.descriptionL2);
		System.assertEquals(expParam.order, actEntity.order);
	}

	/**
	 * Test for DeleteApi class
	 * Check if the target record is correctly deleted
	 */
	@isTest static void deleteTest() {
		final Integer testGroupSize = 3;
		ExpTypeGroupResourceTest.TestData testData = new TestData();
		ExpTypeGroupEntity targetGroup = testData.createExpTypeGroups('Test', testGroupSize).get(0);

		ExpTypeGroupResource.DeleteRequest param = new ExpTypeGroupResource.DeleteRequest();
		param.id = targetGroup.id;

		// execute API
		ExpTypeGroupResource.DeleteApi api = new ExpTypeGroupResource.DeleteApi();
		api.execute(param);

		// check if the target record is deleted
		ExpTypeGroupEntity resGroup = (new ExpTypeGroupRepository()).getEntity(targetGroup.id);
		System.assertEquals(null, resGroup);
	}

	/**
	 * Test for DeleteApi class
	 * Check if exception is thrown if the id in request parameter is null
	 */
	@isTest static void deleteTestIdNull() {
		final Integer testGroupSize = 3;
		ExpTypeGroupResourceTest.TestData testData = new TestData();
		ExpTypeGroupEntity targetGroup = testData.createExpTypeGroups('Test', testGroupSize).get(0);

		ExpTypeGroupResource.DeleteRequest param = new ExpTypeGroupResource.DeleteRequest();
		param.id = null;

		// execute API
		ExpTypeGroupResource.DeleteApi api = new ExpTypeGroupResource.DeleteApi();
		try {
			api.execute(param);
			TestUtil.fail('Exception was not thrown.');
		} catch (App.ParameterException e) {
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
	}

	/**
	 * Test for SearchApi class
	 * Check if the all records are returned, if the search condition is not set
	 * Check if all fields of the record are acquired
	 */
	@isTest static void searchTestAll() {
		final Integer recordSize = 3;
		ExpTypeGroupResourceTest.TestData testData = new TestData();
		List<ExpTypeGroupEntity> testGroups = testData.createExpTypeGroups('Test', recordSize);

		ExpTypeGroupResource.SearchApi api = new ExpTypeGroupResource.SearchApi();
		Map<String, Object> paramMap = new Map<String, Object>{};

		Test.startTest();

		ExpTypeGroupResource.SearchResponse res = (ExpTypeGroupResource.SearchResponse)api.execute(paramMap);

		Test.stopTest();

		System.assertEquals(recordSize, res.records.size());
		for (Integer i = 0; i < recordSize; i++) {
			assertSearchAllField(testGroups[i], res.records[i]);
		}
	}

	/**
	 * Test for SearchApi class
	 * Check if the matched records are returned, if the search condition is set
	 */
	@isTest static void searchTestFilter() {
		final Integer recordSize = 3;
		ExpTypeGroupResourceTest.TestData testData = new TestData();
		List<ExpTypeGroupEntity> testGroups = testData.createExpTypeGroups('Test', recordSize);
		ExpTypeGroupEntity targetGroup = testGroups[1];

		ExpTypeGroupResource.SearchApi api = new ExpTypeGroupResource.SearchApi();
		Map<String, Object> paramMap;
		ExpTypeGroupResource.SearchResponse res;

		// add id to search condition
		paramMap = new Map<String, Object> {
			'id' => targetGroup.id
		};
		res = (ExpTypeGroupResource.SearchResponse)api.execute(paramMap);
		System.assertEquals(1, res.records.size());
		System.assertEquals(targetGroup.id, res.records[0].id);

		// add company id to search condition
		CompanyEntity targetCompany = testData.createCompany('CompanyTest');
		ExpTypeGroupEntity companyTestGroup = testData.createExpTypeGroups('CompanyTest', targetCompany.id, 1).get(0);
		paramMap = new Map<String, Object> {
			'companyId' => targetCompany.id
		};
		res = (ExpTypeGroupResource.SearchResponse)api.execute(paramMap);
		System.assertEquals(1, res.records.size());
		System.assertEquals(companyTestGroup.id, res.records[0].id);

		// add date to search condition
		paramMap = new Map<String, Object> {
			'targetDate' => targetGroup.validFrom.format()
		};
		res = (ExpTypeGroupResource.SearchResponse)api.execute(paramMap);
		System.assertEquals(1, res.records.size());
		System.assertEquals(targetGroup.id, res.records[0].id);

		// add date to search condition
		targetGroup.parentId = testGroups[0].id;
		(new ExpTypeGroupRepository()).saveEntity(targetGroup);
		paramMap = new Map<String, Object> {
			'hasNoParent' => true
		};
		res = (ExpTypeGroupResource.SearchResponse)api.execute(paramMap);
		System.assertEquals(3, res.records.size());
		for (ExpTypeGroupResource.ExpTypeGroupParam record : res.records) {
			System.assertNotEquals(targetGroup.id, record.id);
		}
	}

	/**
	 * Check if each field of the record is correctly set
	 */
	private static void assertSearchAllField(ExpTypeGroupEntity expEntity, ExpTypeGroupResource.ExpTypeGroupParam actParam) {

		System.assertEquals(expEntity.id, actParam.id);
		System.assertEquals(expEntity.code, actParam.code);
		System.assertEquals(expEntity.nameL.getValue(), actParam.name);
		System.assertEquals(expEntity.nameL0, actParam.name_L0);
		System.assertEquals(expEntity.nameL1, actParam.name_L1);
		System.assertEquals(expEntity.nameL2, actParam.name_L2);
		System.assertEquals(expEntity.companyId, actParam.companyId);
		System.assertEquals(AppDate.convertDate(expEntity.validFrom), actParam.validDateFrom);
		System.assertEquals(AppDate.convertDate(expEntity.validTo), actParam.validDateTo);
		System.assertEquals(expEntity.parentId, actParam.parentId);
		System.assertEquals(expEntity.parentGroup.nameL.getValue(), actParam.parent.name);
		System.assertEquals(expEntity.descriptionL.getValue(), actParam.description);
		System.assertEquals(expEntity.descriptionL0, actParam.description_L0);
		System.assertEquals(expEntity.descriptionL1, actParam.description_L1);
		System.assertEquals(expEntity.descriptionL2, actParam.description_L2);
		System.assertEquals(expEntity.order, actParam.order);
	}
}