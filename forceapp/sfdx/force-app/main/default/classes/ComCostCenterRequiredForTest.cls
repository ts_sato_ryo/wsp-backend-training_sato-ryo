/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Test class for ComCostCenterRequiredFor
 */
@isTest
private class ComCostCenterRequiredForTest {

    /**
	 * Check if it compare properly
	 */
	@isTest static void equalsTest() {

		// Equal
		System.assertEquals(ComCostCenterRequiredFor.EXPENSE_REPORT, ComCostCenterRequiredFor.valueOf('ExpenseReport'));
		System.assertEquals(ComCostCenterRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ComCostCenterRequiredFor.valueOf('ExpenseRequestAndExpenseReport'));

		// Values are different
		System.assertEquals(false, ComCostCenterRequiredFor.EXPENSE_REPORT.equals(ComCostCenterRequiredFor.valueOf('ExpenseRequestAndExpenseReport')));

		// Class type is different
		System.assertEquals(false, ComCostCenterRequiredFor.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// Value is null
		System.assertEquals(false, ComCostCenterRequiredFor.EXPENSE_REPORT.equals(null));
	}
}
