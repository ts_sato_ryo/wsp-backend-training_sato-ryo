/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 履歴管理方式が有効期間型のマスタのエンティティ基底クラス
 */
public with sharing abstract class ValidPeriodRepository extends Repository.BaseRepository {

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveEntity(ValidPeriodEntity entity) {
		return saveEntityList(new List<ValidPeriodEntity>{entity}, ALL_SAVE);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveEntityList(List<ValidPeriodEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public virtual Repository.SaveResult saveEntityList(List<ValidPeriodEntity> entityList, Boolean allOrNone) {

		List<SObject> saveObjectList = createObjectList(entityList);
		List<Database.UpsertResult> resList = AppDatabase.doUpsert(saveObjectList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected abstract List<SObject> createObjectList(List<ValidPeriodEntity> entityList);
}