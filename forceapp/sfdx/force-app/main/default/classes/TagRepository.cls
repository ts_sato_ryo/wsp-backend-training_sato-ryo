/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description タグリポジトリ
 */
public with sharing class TagRepository extends LogicalDeleteRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
				ComTag__c.Id,
				ComTag__c.Name,
				ComTag__c.UniqKey__c,
				ComTag__c.TagType__c,
				ComTag__c.Code__c,
				ComTag__c.CompanyId__c,
				ComTag__c.Name_L0__c,
				ComTag__c.Name_L1__c,
				ComTag__c.Name_L2__c,
				ComTag__c.Order__c,
				ComTag__c.Removed__c
		};
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** タグID */
		public Set<Id> ids;
		/** 会社ID */
		public Set<String> companyIds;
		/** コード(完全一致) */
		public Set<String> codes;
		/** タグタイプ */
		public Set<String> tagTypes;
	}

	/** FOR UPDATE でクエリしない */
	private static final Boolean NOT_FOR_UPDATE = false;

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntity(TagEntity entity) {
		return saveEntityList(new List<TagEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntityList(List<TagEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public Repository.SaveResult saveEntityList(List<TagEntity> entityList, Boolean allOrNone) {

		List<ComTag__c> takeList = new List<ComTag__c>();

		for (TagEntity entity : entityList) {
			// エンティティからSObjectを作成する
			takeList.add(createObj(entity));
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(takeList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * 指定したIDのエンティティを検索する
	 * @param tagId 取得対象のID
	 * @return エンティティ、ただし指定したIDのエンティティが存在しない場合、論理削除されている場合はnull
	 */
	public TagEntity getEntity(Id tagId) {
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.ids = new Set<Id>{tagid};
		List<TagEntity> entityList = searchEntityList(filter, NOT_FOR_UPDATE);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したコードと会社IDを持つエンティティを取得する
	 * @param code 取得対象のコード
	 * @param companyId 取得対象の会社ID
	 * @return ベースエンティティの検索結果
	 */
	public TagEntity getEntityByCode(String code, String companyId) {
		List<TagEntity> entities = getEntityListByCode(new Set<String>{code}, companyId);

		if (entities.isEmpty()) {
			return null;
		} else {
			return entities[0];
		}
	}

	/**
	 * 指定したコードと会社IDを持つエンティティを取得する
	 * @param codes 取得対象のコード
	 * @param companyId 取得対象の会社ID
	 * @return ベースエンティティの検索結果
	 */
	public List<TagEntity> getEntityListByCode(Set<String> codes, String companyId) {
		TagRepository.SearchFilter filter = new TagRepository.SearchFilter();
		filter.codes = codes;
		filter.companyIds = new Set<String>{companyId};
		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したタグ一覧（並び順、コードの昇順）
	 */
	public List<TagEntity> searchEntityList(TagRepository.SearchFilter filter) {
		return searchEntityList(filter, NOT_FOR_UPDATE);
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したタグ一覧
	 */
	private List<TagEntity> searchEntityList(TagRepository.SearchFilter filter, Boolean forUpdate) {
		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Set<Id> ids = filter.ids;
		final Set<String> companyIds = filter.companyIds;
		final Set<String> tagTypes = filter.tagTypes;
		final Set<String> codes = filter.codes;
		if (ids != null) {
			condList.add(getFieldName(ComTag__c.Id) + ' IN :ids');
		}
		if (companyIds != null) {
			condList.add(getFieldName(ComTag__c.CompanyId__c) + ' IN :companyIds');
		}
		if (codes != null) {
			condList.add(getFieldName(ComTag__c.Code__c) + ' IN :codes');
		}
		if (tagTypes != null) {
			condList.add(getFieldName(ComTag__c.TagType__c) + ' IN :tagTypes');
		}
		condList.add(getFieldName(ComTag__c.Removed__c) + ' = false');

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComTag__c.SObjectType.getDescribe().getName()
				+ buildWhereString(condList)
				+ ' ORDER BY ' + getFieldName(ComTag__c.Order__c) + ' Asc, '
							   + getFieldName(ComTag__c.Code__c) + ' Asc';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// クエリ実行
		System.debug('TagRepository.searchEntityList: SOQL ==>' + soql);
		List<ComTag__c> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<TagEntity> entityList = new List<TagEntity>();
		for (ComTag__c sObj : sObjList) {
			entityList.add(createEntity(sObj));
		}

		return entityList;
	}

	/**
	 * ComTag__cからTagEntityを作成する
	 * @param sObj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private TagEntity createEntity(ComTag__c sObj) {
		return new TagEntity(sObj);
	}

	/**
	 * TagEntity から ComTag__c を作成する
	 * @param TagEntity
	 * @return ComTag__c
	 */
	private ComTag__c createObj(TagEntity entity) {
		return entity.createSObject();
	}
}