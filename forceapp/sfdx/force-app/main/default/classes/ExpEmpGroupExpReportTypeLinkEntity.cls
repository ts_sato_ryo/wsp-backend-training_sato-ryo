/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description 経費社員グループ別申請種別のエンティティ Entity class for ExpEmpGroupExpReportTypeLink
 *
 * @group 経費 Expense
 */
public with sharing class ExpEmpGroupExpReportTypeLinkEntity extends Entity {

	/*
	 * 項目の定義(変更管理で使用する) Field definition
	 */
	public enum Field {
		EXP_EMP_GROUP_ID,
		EXP_REPORT_TYPE_ID,
		ORDER
	}

	/*
	 * 値を変更した項目のリスト Set of fields whose value has changed
	 */
	private Set<Field> isChangedFieldSet;

	/*
	 * コンストラクタ Constructor
	 */
	public ExpEmpGroupExpReportTypeLinkEntity() {
		super();
		isChangedFieldSet = new Set<Field>();
	}

	/*
	 * 社員グループID Expense Employee Group Id
	 */
	public Id expEmpGroupId {
		get;
		set {
			expEmpGroupId = value;
			setChanged(Field.EXP_EMP_GROUP_ID);
		}
	}

	/*
	 * 申請種別ID Expense Report Type Id
	 */
	public Id expReportTypeId {
		get;
		set {
			expReportTypeId = value;
			setChanged(Field.EXP_REPORT_TYPE_ID);
		}
	}

	/*
	 * オーダー Order
	 */
	public Integer order {
		get;
		set {
			order = value;
			setChanged(Field.ORDER);
		}
	}


	/*
	 * 項目の値を変更済みに設定する Mark field as changed
	 * @param Field 変更した項目 Field Changed field
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/*
	 * 項目の変更情報を取得する Check if the value of the field has been changed
	 * @param field 取得対象の項目 Target field
	 * @return 項目の値が変更されている場合はtrue、そうでない場合はfalse True if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/*
	 * 項目の変更情報をリセットする Reset the changed status
	 */
	public virtual void resetChanged() {
		this.isChangedFieldSet.clear();
	}
}