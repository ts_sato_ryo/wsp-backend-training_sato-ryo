/**
 * AppDateのテストクラス
 */
@isTest
private class AppDateTest {

	/**
	 * valueOfのテスト
	 */
	@isTest static void valueOfTestDate() {

		// Dateを設定した場合、インスタンスが作成される
		Date dt = System.today();
		AppDate appDt = AppDate.valueOf(dt);
		System.assertEquals(dt.format(), appDt.getDate().format());

		// nullを設定した場合、nullが返却される
		dt = null;
		System.assertEquals(null, AppDate.valueOf(dt));
	}

	/**
	 * valueOfのテスト(文字列)
	 */
	@isTest static void valueOfTestString() {

		// YYYY-MM-DD形式の文字列からインスタンスが作成される
		String dtString = '2017-07-02';
		AppDate appDt = AppDate.valueOf(dtString);
		System.assertEquals(Date.newInstance(2017, 7, 2), appDt.getDate());

		// nullを設定した場合、nullが返却される
		dtString = null;
		System.assertEquals(null, AppDate.valueOf(dtString));
	}

	/**
	 * parseのテスト
	 * 指定した日付文字列からインスタンスが作成されることを確認する
	 */
	@isTest static void parseTest() {

		// YYYY-MM-DD形式の文字列からインスタンスが作成される
		String dtString = '2017-07-02';
		AppDate appDt = AppDate.parse(dtString);
		System.assertEquals(Date.newInstance(2017, 7, 2), appDt.getDate());

		// nullを設定した場合、nullが返却される
		dtString = null;
		System.assertEquals(null, AppDate.parse(dtString));
	}

	/**
	 * newInstance(year, month, day)のテスト
	 * インスタンスが作成できることを確認する
	 */
	@isTest static void newInstanceYMDTest() {

		// Dateを設定した場合、インスタンスが作成される
		Date dt = System.today();
		AppDate appDt = AppDate.newInstance(dt.year(), dt.month(), dt.day());
		System.assertEquals(dt, appDt.getDate());

	}

	/**
	 * コンストラクタのテスト（文字列型）
	 */
	@isTest static void constructorTestString() {

		// 文字列から作成できることを確認する
		String dataString = '2017-07-14';
		AppDate appDt = new AppDate(dataString);
		Date dt = appDt.getDate();
		System.assertEquals(2017, dt.year());
		System.assertEquals(7, dt.month());
		System.assertEquals(14, dt.day());

		// 空文字の場合は例外が発生する
		try {
			appDt = new AppDate(' ');
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// App.ParameterException例外が発生すればOK
			System.assert(true);
		}
	}

	/**
	 * コンストラクタのテスト（Date型）
	 */
	@isTest static void constructorTestDate() {

		// Data型のオブジェクトから作成できることを確認する
		Date orgDt = Date.newInstance(2017, 7, 14);
		AppDate appDt = new AppDate(orgDt);
		Date dt = appDt.getDate();
		System.assertEquals(orgDt.year(), dt.year());
		System.assertEquals(orgDt.month(), dt.month());
		System.assertEquals(orgDt.day(), dt.day());

		// nullの場合は例外が発生する
		try {
			Date testDt = null;
			appDt = new AppDate(testDt);
			System.assert(false, '例外が発生しませんでした。');
		} catch (App.ParameterException e) {
			// App.ParameterException例外が発生すればOK
			System.assert(true);
		}
	}

	/**
	 * todayのテスト
	 * 今日の日付でインスタンスが作成できることを確認する
	 */
	@isTest static void todayTest() {
		Date expToday = Date.today();
		AppDate today = AppDate.today();
		System.assertEquals(expToday, today.getDate());
	}

	/**
	 * convertDateのテスト
	 * Integer型に変換できることを確認する
	 */
	@isTest static void convertDateTest() {

		// 値が設定されている場合
		Date testDate = Date.today();
		AppDate appValue = new AppDate(testDate);
		System.assertEquals(testDate, AppDate.convertDate(appValue));

		// Nullの場合
		System.assertEquals(null, AppDate.convertDate(null));
	}

	/**
	 * yearのテスト
	 * 値が取得できることを確認する
	 */
	@isTest static void yearTest() {
		Date orgDt = Date.newInstance(2017, 7, 14);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals(orgDt.year(), appDt.year());
	}

	/**
	 * monthのテスト
	 * 値が取得できることを確認する
	 */
	@isTest static void monthTest() {
		Date orgDt = Date.newInstance(2017, 7, 14);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals(orgDt.month(), appDt.month());
	}

	/**
	 * dayのテスト
	 * 値が取得できることを確認する
	 */
	@isTest static void dayTest() {
		Date orgDt = Date.newInstance(2017, 7, 14);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals(orgDt.day(), appDt.day());
	}

	/**
	 * formatYYYYMMDDのテスト
	 * 値が取得できることを確認する
	 */
	@isTest static void formatYYYYMMDDTest() {
		Date orgDt = Date.newInstance(2017, 7, 1);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals('20170701', appDt.formatYYYYMMDD());
	}

	/**
	 * formatのテスト
	 * yyyy-mm-dd形式で文字列が取得できることを確認する
	 */
	@isTest static void format() {
		Date orgDt = Date.newInstance(2017, 7, 1);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals('2017-07-01', appDt.format());
	}

	/**
	 * formatのテスト
	 * 値が取得できることを確認する
	 */
	@isTest static void formatTest() {
		Date orgDt = Date.newInstance(2017, 7, 1);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals('2017-07-01', appDt.format(AppDate.FormatType.YYYY_MM_DD));
		System.assertEquals('2017/07/01', appDt.format(AppDate.FormatType.YYYY_MM_DD_SLASH));
		System.assertEquals('2017/7/1', appDt.format(AppDate.FormatType.YYYY_M_D_SLASH));
		System.assertEquals('2017/07', appDt.format(AppDate.FormatType.YYYY_MM_SLASH));
		System.assertEquals('20170701', appDt.format(AppDate.FormatType.YYYYMMDD));
		System.assertEquals('201707', appDt.format(AppDate.FormatType.YYYYMM));
		System.assertEquals('Jul 2017', appDt.format(AppDate.FormatType.MMMYYYY));
		System.assertEquals('July 2017', appDt.format(AppDate.FormatType.MMMMYYYY));
		System.assertEquals('07', appDt.format(AppDate.FormatType.MM));
	}

	/**
	 * addMonthsのテスト
	 * 月数が加算できることを確認する
	 */
	@isTest static void appMonthsTest() {
		final Integer addMonths = 2;
		Date orgDt = Date.newInstance(2017, 7, 1);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals(orgDt.addMonths(addMonths), appDt.addMonths(addMonths).getDate());
	}

	/**
	 * addDaysのテスト
	 * 日数が加算できることを確認する
	 */
	@isTest static void appDaysTest() {
		final Integer addDays = 10;
		Date orgDt = Date.newInstance(2017, 7, 30);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals(orgDt.addDays(addDays), appDt.addDays(addDays).getDate());
	}

	/**
	 * minusDaysのテスト
	 * 日数が減算できることを確認する
	 */
	@isTest static void minusDaysTest() {
		AppDate appDt = AppDate.newInstance(2017, 11, 3).minusDays(3);
		System.assertEquals(2017, appDt.year());
		System.assertEquals(10, appDt.month());
		System.assertEquals(31, appDt.day());
	}

	/**
	 * daysBetweenのテスト
	 * 日数が算出できることを確認する
	 */
	@isTest static void daysBetweenTest() {
		final Integer addDays = 10;
		Date dt1 = Date.newInstance(2017, 7, 30);
		Date dt2 = dt1.addDays(addDays);
		AppDate appDt1 = new AppDate(dt1);
		AppDate appDt2 = new AppDate(dt2);
		System.assertEquals(addDays, appDt1.daysBetween(appDt2));
	}

	/**
	 * dayOfWeekのテスト
	 * 曜日が正しく取得できることを確認する
	 */
	@isTest static void dayOfWeekTest() {
		System.assertEquals(AppDate.DayOfWeek.SUN, AppDate.newInstance(2017, 8, 6).dayOfWeek());
		System.assertEquals(AppDate.DayOfWeek.MON, AppDate.newInstance(2017, 8, 7).dayOfWeek());
		System.assertEquals(AppDate.DayOfWeek.TUE, AppDate.newInstance(2017, 8, 8).dayOfWeek());
		System.assertEquals(AppDate.DayOfWeek.WED, AppDate.newInstance(2017, 8, 9).dayOfWeek());
		System.assertEquals(AppDate.DayOfWeek.THU, AppDate.newInstance(2017, 8, 10).dayOfWeek());
		System.assertEquals(AppDate.DayOfWeek.FRI, AppDate.newInstance(2017, 8, 11).dayOfWeek());
		System.assertEquals(AppDate.DayOfWeek.SAT, AppDate.newInstance(2017, 8, 12).dayOfWeek());
	}

	/**
	 * equalsのテスト
	 * 値が正しく比較できることを確認する
	 */
	@isTest static void equalsTest() {
		final Date orgDt = Date.newInstance(2017, 7, 14);
		AppDate appDt = new AppDate(orgDt);

		// 値が等しい場合 -> true
		System.assertEquals(true, appDt.equals(new AppDate(orgDt)));

		// 値が異なる場合 -> false
		System.assertEquals(false, appDt.equals(new AppDate(orgDt.addDays(1))));

		// 引数のオブジェクトの型が異なる場合 -> false
		System.assertEquals(false, appDt.equals(orgDt));

		// nullの場合 -> false
		System.assertEquals(false, appDt.equals(null));
	}

	/**
	 * hashCodeのテスト
	 * ハッシュコードが正しく取得できることを確認する
	 */
	@isTest static void hashCodeTest() {
		AppDate appDt = new AppDate(Date.newInstance(2017, 10, 03));
		System.assertEquals(20171003, appDt.hashCode());

		// Mapが使えるはず
		Map<AppDate, Integer> dateMap = new Map<AppDate, Integer>  {
			new AppDate(Date.newInstance(2017, 10, 01)) => 1,
			new AppDate(Date.newInstance(2017, 01, 02)) => 2,
			new AppDate(Date.newInstance(2019, 10, 03)) => 3};
		System.assertEquals(2, dateMap.get(new AppDate(Date.newInstance(2017, 01, 02))));
		System.assertEquals(3, dateMap.get(new AppDate(Date.newInstance(2019, 10, 03))));
	}

	/**
	 * compareToのテスト
	 * 値が正しく比較できることを確認する
	 */
	@isTest static void compareToTest() {
		final Date orgDt = Date.newInstance(2017, 7, 14);
		AppDate appDt = new AppDate(orgDt);

		// 値が等しい場合 -> 0
		System.assertEquals(0, appDt.compareTo(new AppDate(orgDt)));

		// このインスタンスの方が値が大きい場合 -> 1以上
		System.assert(1 <= appDt.compareTo(new AppDate(orgDt.addDays(-1))));

		// このインスタンスの方が値が小さい場合 -> 0より小さい
		System.assert(0 > appDt.compareTo(new AppDate(orgDt.addDays(1))));
	}

	/**
	 * toStringのテスト
	 * 文字列が正しく取得できることを確認する
	 */
	@isTest static void toStringTest() {
		final Date orgDt = Date.newInstance(2017, 9, 27);
		AppDate appDt = new AppDate(orgDt);
		System.assertEquals('2017-09-27', appDt.toString());
	}

	/**
	 * isBeforeTestのテスト
	 * 境界値を正しく判定できるを確認する
	 */
	@isTest static void isBeforeTest() {
		AppDate appDt = AppDate.newInstance(2018, 11, 1);
		System.assert(!appDt.isBefore(AppDate.newInstance(2018, 10, 31)));
		System.assert(!appDt.isBefore(AppDate.newInstance(2018, 11, 1)));
		System.assert(appDt.isBefore(AppDate.newInstance(2018, 11, 2)));
	}

	/**
	 * isAfterTestのテスト
	 * 境界値を正しく判定できるを確認する
	 */
	@isTest static void isAfterTest() {
		AppDate appDt = AppDate.newInstance(2018, 11, 1);
		System.assert(appDt.isAfter(AppDate.newInstance(2018, 10, 31)));
		System.assert(!appDt.isAfter(AppDate.newInstance(2018, 11, 1)));
		System.assert(!appDt.isAfter(AppDate.newInstance(2018, 11, 2)));
	}

	/**
	 * minのテスト
	 * 最も過去の日付からインスタンスを生成すること確認する
	 */
	@isTest static void minValueTest() {
		AppDate date1 = AppDate.newInstance(2020, 4, 1);
		AppDate date2 = AppDate.newInstance(2020, 4, 2);

		System.assertEquals(date1, AppDate.min(date1, date2));
		System.assertEquals(date1, AppDate.min(date2, date1));
		System.assertEquals(date1, AppDate.min(date1, date1));
	}

	/**
	 * maxのテスト
	 * 最も未来の日付からインスタンスを生成すること確認する
	 */
	@isTest static void maxValueTest() {
		AppDate date1 = AppDate.newInstance(2020, 4, 1);
		AppDate date2 = AppDate.newInstance(2020, 4, 2);

		System.assertEquals(date2, AppDate.max(date1, date2));
		System.assertEquals(date2, AppDate.max(date2, date1));
		System.assertEquals(date2, AppDate.max(date2, date2));
	}
	/**
	 * isBeforeEqualsTestのテスト
	 * 境界値を正しく判定できるを確認する
	 */
	@isTest static void isBeforeEqualsTest() {
		AppDate appDt = AppDate.newInstance(2018, 11, 1);
		System.assert(!appDt.isBeforeEquals(AppDate.newInstance(2018, 10, 31)));
		System.assert(appDt.isBeforeEquals(AppDate.newInstance(2018, 11, 1)));
		System.assert(appDt.isBeforeEquals(AppDate.newInstance(2018, 11, 2)));
	}

	/**
	 * isAfterEqualsTestのテスト
	 * 境界値を正しく判定できるを確認する
	 */
	@isTest static void isAfterEqualsTest() {
		AppDate appDt = AppDate.newInstance(2018, 11, 1);
		System.assert(appDt.isAfterEquals(AppDate.newInstance(2018, 10, 31)));
		System.assert(appDt.isAfterEquals(AppDate.newInstance(2018, 11, 1)));
		System.assert(!appDt.isAfterEquals(AppDate.newInstance(2018, 11, 2)));
	}

	/**
	 * customTodayのテスト
	 */
	@isTest static void customTodayTest() {
		// 値を設定しない場合は、当日を取得できることを確認
		System.assertEquals(Date.today(), AppDate.today().getDate());

		// 設定した値を取得できることを確認
		AppDate.customToday = AppDate.newInstance(2000, 1, 2);
		System.assertEquals(AppDate.newInstance(2000, 1, 2), AppDate.today());
	}
}