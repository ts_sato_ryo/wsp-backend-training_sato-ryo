/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 社員リポジトリ
 */
public with sharing class EmployeeRepository extends ParentChildRepository {

	/**
	 * 履歴リストを取得する際の並び順
	 */
	public enum HistorySortOrder {
		VALID_FROM_ASC, VALID_FROM_DESC
	}

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}

	/** キャッシュの利用有無 */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	/**
	 * キャッシュ利用が有効化されている場合はtrue
	 */
	public Boolean isEnabledCache() {
		return this.isEnabledCache;
	}

	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** 社員ベースのキャッシュ */
	private static final Repository.Cache BASE_CACHE = new Repository.Cache();
	/** 社員履歴のキャッシュ */
	private static final Repository.Cache HISTORY_CACHE = new Repository.Cache();

	/** 社員ベースのリレーション名 */
	private static final String HISTORY_BASE_R = ComEmpHistory__c.BaseId__c.getDescribe().getRelationshipName();

	/**
	 * 取得対象のベースオブジェクトの項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> baseFieldList = new Set<Schema.SObjectField> {
				ComEmpBase__c.Id,
				ComEmpBase__c.Name,
				ComEmpBase__c.CurrentHistoryId__c,
				ComEmpBase__c.Code__c,
				ComEmpBase__c.UniqKey__c,
				ComEmpBase__c.CompanyId__c,
				ComEmpBase__c.DisplayName_L0__c,
				ComEmpBase__c.DisplayName_L1__c,
				ComEmpBase__c.DisplayName_L2__c,
				ComEmpBase__c.FirstName_L0__c,
				ComEmpBase__c.FirstName_L1__c,
				ComEmpBase__c.FirstName_L2__c,
				ComEmpBase__c.MiddleName_L0__c,
				ComEmpBase__c.MiddleName_L1__c,
				ComEmpBase__c.MiddleName_L2__c,
				ComEmpBase__c.LastName_L0__c,
				ComEmpBase__c.LastName_L1__c,
				ComEmpBase__c.LastName_L2__c,
				ComEmpBase__c.HiredDate__c,
				ComEmpBase__c.ResignationDate__c,
				ComEmpBase__c.ValidFrom__c,
				ComEmpBase__c.ValidTo__c,
				ComEmpBase__c.UserId__c,
				ComEmpBase__c.OwnerId
		};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(baseFieldList);
	}

	/**
	 * 取得対象の履歴オブジェクトの項目名
	 * 項目が追加されたらhistoryFieldSetに追加してください
	 */
	private static final List<String> GET_HISTORY_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> historyFieldSet = new Set<Schema.SObjectField> {
			ComEmpHistory__c.Id,
			ComEmpHistory__c.BaseId__c,
			ComEmpHistory__c.UniqKey__c,
			ComEmpHistory__c.ValidFrom__c,
			ComEmpHistory__c.ValidTo__c,
			ComEmpHistory__c.HistoryComment__c,
			ComEmpHistory__c.Removed__c,
			ComEmpHistory__c.Name,
			ComEmpHistory__c.CostCenterBaseId__c,
			ComEmpHistory__c.DepartmentBaseId__c,
			ComEmpHistory__c.ExpEmployeeGroupId__c,
			ComEmpHistory__c.GradeId__c,
			ComEmpHistory__c.Title_L0__c,
			ComEmpHistory__c.Title_L1__c,
			ComEmpHistory__c.Title_L2__c,
			ComEmpHistory__c.WorkingTypeBaseId__c,
			ComEmpHistory__c.TimeSettingBaseId__c,
			ComEmpHistory__c.ManagerBaseId__c,
			ComEmpHistory__c.CalendarId__c,
			ComEmpHistory__c.AgreementAlertSettingId__c,
			ComEmpHistory__c.PermissionId__c,
			ComEmpHistory__c.CommuterPassAvailable__c,
			ComEmpHistory__c.JorudanRoute__c,
			ComEmpHistory__c.AdditionalDepartmentBaseId__c,
			ComEmpHistory__c.AdditionalTitle_L0__c,
			ComEmpHistory__c.AdditionalTitle_L1__c,
			ComEmpHistory__c.AdditionalTitle_L2__c,
			ComEmpHistory__c.ApproverBase01Id__c,
			ComEmpHistory__c.ApproverBase02Id__c,
			ComEmpHistory__c.ApproverBase03Id__c,
			ComEmpHistory__c.ApproverBase04Id__c,
			ComEmpHistory__c.ApproverBase05Id__c,
			ComEmpHistory__c.ApproverBase06Id__c,
			ComEmpHistory__c.ApproverBase07Id__c,
			ComEmpHistory__c.ApproverBase08Id__c,
			ComEmpHistory__c.ApproverBase09Id__c,
			ComEmpHistory__c.ApproverBase10Id__c,
			ComEmpHistory__c.ApprovalAuthority01__c,
			ComEmpHistory__c.UserData01__c,
			ComEmpHistory__c.UserData02__c,
			ComEmpHistory__c.UserData03__c,
			ComEmpHistory__c.UserData04__c,
			ComEmpHistory__c.UserData05__c,
			ComEmpHistory__c.UserData06__c,
			ComEmpHistory__c.UserData07__c,
			ComEmpHistory__c.UserData08__c,
			ComEmpHistory__c.UserData09__c,
			ComEmpHistory__c.UserData10__c
		};

		GET_HISTORY_FIELD_NAME_LIST = Repository.generateFieldNameList(historyFieldSet);
	}

	/**
	 * Field name of the Cost Center Object to be included
	 */
	private static final List<String> GET_HISTORY_COST_CENTER_FIELD_NAME_LIST;
	/** Cost Center Relationship Name */
	private static final String HISTORY_COST_CENTER_R = ComEmpHistory__c.CostcenterBaseId__c.getDescribe().getRelationshipName();
	static {
		final Set<Schema.SObjectField> costCenterFieldList = new Set<Schema.SObjectField>{ComCostCenterBase__c.Code__c};
		GET_HISTORY_COST_CENTER_FIELD_NAME_LIST = Repository.generateFieldNameList(HISTORY_COST_CENTER_R, costCenterFieldList);
	}

	/**
	 * 取得対象の上長の社員オブジェクトの項目名
	 */
	private static final List<String> GET_HISTORY_MANAGER_FIELD_NAME_LIST;
	/** 上長のリレーション名 */
	private static final String HISTORY_MANAGER_R = ComEmpHistory__c.ManagerBaseId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> employeeFieldList = new Set<Schema.SObjectField> {
			ComEmpBase__c.FirstName_L0__c,
			ComEmpBase__c.LastName_L0__c,
			ComEmpBase__c.FirstName_L1__c,
			ComEmpBase__c.LastName_L1__c,
			ComEmpBase__c.FirstName_L2__c,
			ComEmpBase__c.LastName_L2__c};

		GET_HISTORY_MANAGER_FIELD_NAME_LIST = Repository.generateFieldNameList(HISTORY_MANAGER_R, employeeFieldList);
	}

	/**
	 * 取得対象の部署のオブジェクトの項目名
	 */
	private static final List<String> GET_HISTORY_DEPARTMENT_FIELD_NAME_LIST;
	/** 部署のリレーション名 */
	private static final String HISTORY_DEPARTMENT_R = ComEmpHistory__c.DepartmentBaseId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(社員)
		final Set<Schema.SObjectField> departmentFieldList = new Set<Schema.SObjectField> {
			ComDeptBase__c.Code__c};

		GET_HISTORY_DEPARTMENT_FIELD_NAME_LIST = Repository.generateFieldNameList(HISTORY_DEPARTMENT_R, departmentFieldList);
	}

	/**
	 * 取得対象の36協定アラート設定のオブジェクトの項目名
	 */
	private static final List<String> GET_HISTORY_AGREEMENT_ALERT_SETTING_FIELD_NAME_LIST;
	/** 36協定アラート設定のリレーション名 */
	private static final String HISTORY_AGREEMENT_ALERT_SETTING_R =
			ComEmpHistory__c.AgreementAlertSettingId__c.getDescribe().getRelationshipName();
	static {
		// 取得対象の項目定義(36協定アラート設定)
		final Set<Schema.SObjectField> alertSettingFieldList = new Set<Schema.SObjectField> {
			AttAgreementAlertSetting__c.Name_L0__c,
			AttAgreementAlertSetting__c.Name_L1__c,
			AttAgreementAlertSetting__c.Name_L2__c
		};

		GET_HISTORY_AGREEMENT_ALERT_SETTING_FIELD_NAME_LIST =
				Repository.generateFieldNameList(HISTORY_AGREEMENT_ALERT_SETTING_R, alertSettingFieldList);
	}

	/**
	 * 取得対象の権限のオブジェクトの項目名
	 */
	private static final List<String> GET_HISTORY_PERMISSION_FIELD_NAME_LIST;
	/** 権限のリレーション名 */
	private static final String HISTORY_PERMISSION_R =
			ComEmpHistory__c.PermissionId__c.getDescribe().getRelationshipName();
	static {
		// 取得対象の項目定義(権限)
		final Set<Schema.SObjectField> permissionFieldList = new Set<Schema.SObjectField> {
			ComPermission__c.Name_L0__c,
			ComPermission__c.Name_L1__c,
			ComPermission__c.Name_L2__c
		};

		GET_HISTORY_PERMISSION_FIELD_NAME_LIST =
				Repository.generateFieldNameList(HISTORY_PERMISSION_R, permissionFieldList);
	}

	/** Expense Employee Group Relationship Fields */
	private static final List<String> GET_EXPNESE_EMPLOYEE_GROUP_FIELD_NAME_LIST;
	/** Relationship Field name */
	private static final String EXPENSE_EMPLOYEE_GROUP_R = ComEmpHistory__c.ExpEmployeeGroupId__c.getDescribe().getRelationshipName();
	static {
		final Set<Schema.SObjectField> expEmployeeGroupSet = new Set<Schema.SObjectField> {
			ExpEmployeeGroup__c.Code__c,
			ExpEmployeeGroup__c.Name_L0__c,
			ExpEmployeeGroup__c.Name_L1__c,
			ExpEmployeeGroup__c.Name_L2__c
			};
		GET_EXPNESE_EMPLOYEE_GROUP_FIELD_NAME_LIST = Repository.generateFieldNameList(EXPENSE_EMPLOYEE_GROUP_R, expEmployeeGroupSet);
	}

	/**
	 * 取得対象の会社のオブジェクトの項目名
	 */
	private static final List<String> GET_BASE_COMPANY_FIELD_NAME_LIST;
	/** 会社のリレーション名 */
	private static final String BASE_COMPANY_R = ComEmpBase__c.CompanyId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(会社)
		final Set<Schema.SObjectField> companyFieldList = new Set<Schema.SObjectField> {
			ComCompany__c.Code__c,
			ComCompany__c.Name_L0__c,
			ComCompany__c.Name_L1__c,
			ComCompany__c.Name_L2__c,
			ComCompany__c.CurrencyId__c,
			ComCompany__c.UseAttendance__c,
			ComCompany__c.UseExpense__c,
			ComCompany__c.UseExpenseRequest__c,
			ComCompany__c.UsePlanner__c,
			ComCompany__c.UseWorkTime__c
			};

		GET_BASE_COMPANY_FIELD_NAME_LIST = Repository.generateFieldNameList(BASE_COMPANY_R, companyFieldList);
	}

	/**
	 * 取得対象のユーザのオブジェクトの項目名
	 */
	private static final List<String> GET_BASE_USER_FIELD_NAME_LIST;
	/** ユーザのリレーション名 */
	private static final String BASE_USER_R = ComEmpBase__c.UserId__c.getDescribe().getRelationshipName();
	static {

		// 取得対象の項目定義(ユーザ)
		final Set<Schema.SObjectField> userFieldList = new Set<Schema.SObjectField> {
			User.SmallPhotoUrl,
			User.Username,
			User.LanguageLocalekey,
			User.isActive
		};

		GET_BASE_USER_FIELD_NAME_LIST = Repository.generateFieldNameList(BASE_USER_R, userFieldList);
	}

	/**
	 * コンストラクタ
	 */
	public EmployeeRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * Specify the use of cache and create instance.
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public EmployeeRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/**
	 * 指定したエンティティを取得する(ベース+全ての履歴)
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティと、ベースに紐づく全ての履歴エンティティ
	 */
	public EmployeeBaseEntity getEntity(Id baseId) {
		return getEntity(baseId, null);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public EmployeeBaseEntity getEntity(Id baseId, AppDate targetDate) {

		List<EmployeeBaseEntity> entities = getEntityList(new List<Id>{baseId}, targetDate);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public List<EmployeeBaseEntity> getEntityList(List<Id> baseIds, AppDate targetDate) {
		EmployeeRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new Set<Id>(baseIds);
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param userIds 取得対象のユーザーID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public List<EmployeeBaseEntity> getEntityListByUserIds(Set<Id> userIds, AppDate targetDate) {
		EmployeeRepository.SearchFilter filter = new SearchFilter();
		filter.userIds = userIds;
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴)
	 * @param baseId 取得対象のベースID
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @return 指定したベースIDのエンティティと、期間内に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public List<EmployeeBaseEntity> getEntityList(List<Id> baseIds, AppDate startDate, AppDate endDate) {
		EmployeeRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new Set<Id>(baseIds);
		filter.startDate = startDate;
		filter.endDate = endDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public EmployeeBaseEntity getBaseEntity(Id baseId) {
		return getBaseEntity(baseId, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseId 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public EmployeeBaseEntity getBaseEntity(Id baseId, Boolean forUpdate) {
		List<EmployeeBaseEntity> bases = getBaseEntityList(new List<Id>{baseId}, forUpdate);
		if (bases.isEmpty()) {
			return null;
		}
		return bases[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する
	 * @param baseIds 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティのリスト。履歴は含まない
	 */
	public List<EmployeeBaseEntity> getBaseEntityList(List<Id> baseIds, Boolean forUpdate) {
		EmployeeRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.baseIds = new List<Id>(baseIds);
		return searchBaseEntity(filter, forUpdate);
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyId 取得対象の履歴ID
	 * @return 履歴エンティティ。ただし、存在しない場合はnull
	 */
	public EmployeeHistoryEntity getHistoryEntity(Id historyId) {
		List<EmployeeHistoryEntity> entities = getHistoryEntityList(new List<Id>{historyId});

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ)
	 * @param historyIds 取得対象の履歴ID
	 * @return 履歴エンティティのリスト。
	 */
	public List<EmployeeHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		EmployeeRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new Set<Id>(historyIds);

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}


	/** 検索フィルタ(履歴レコードの検索時に使用します) */
	public class SearchFilter {
		/** 社員ベースID */
		public Set<Id> baseIds;
		/** 社員履歴ID */
		public Set<Id> historyIds;
		/** 会社ID */
		public Set<Id> companyIds;
		/** 取得対象日 */
		public AppDate targetDate;
		/** 取得開始日 */
		public AppDate startDate;
		/** 取得終了日 */
		public AppDate endDate;
		/** ユーザーID */
		public Set<Id> userIds;
		/** 社員コード（部分一致） */
		public String code;
		/** 社員コード（完全一致） */
		public Set<String> codes;
		/** 社員名（部分一致） */
		public String nameL;
		/** 部署ID(複数) */
		public Set<Id> departmentIds;
		/** 部署コード（部分一致） */
		public String departmentCode;
		/** 役職（部分一致） */
		public String titleL;
		/** 上長名（部分一致） */
		public String managerNameL;
		/** 勤務体系ID(複数) */
		public Set<Id> workingTypeIds;
		/** 論理削除されている履歴を取得対象にする場合はtrue */
		public Boolean includeRemoved;
		/** ユーザー名 */
		public List<String> userNames;
		/** 承認者権限01 */
		public Boolean approvalAuthority01;
		/** Expense Employee Group */
		public Set<Id> expEmployeeGroupIdSet;
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する。有効開始日の昇順
	 */
	public List<EmployeeBaseEntity> searchEntity(SearchFilter filter) {
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 検索する（ベース＋履歴）
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストの並び順
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する
	 */
	public List<EmployeeBaseEntity> searchEntityWithHistory(SearchFilter filter, Boolean forUpdate,
			EmployeeRepository.HistorySortOrder sortOrder) {

		// 履歴を検索
		List<EmployeeHistoryEntity> histories = searchHistoryEntity(filter, forUpdate, sortOrder);

		// 取得対象のベースIDリストを作成
		Set<Id> targetBaseIds = new Set<Id>();
		for (EmployeeHistoryEntity history : histories) {
			targetBaseIds.add(history.baseId);
		}

		// ベースエンティティを取得する
		SearchBaseFilter baseFilter = new SearchBaseFilter();
		baseFilter.baseIds = new List<Id>(targetBaseIds);
		List<EmployeeBaseEntity> bases = searchBaseEntity(baseFilter, forUpdate);

		// ベースに履歴を追加するためMapに変換する
		Map<Id, EmployeeBaseEntity> baseMap = new Map<Id, EmployeeBaseEntity>();
		for (EmployeeBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		// ベースエンティティに履歴を追加する
		for (EmployeeHistoryEntity history : histories) {
			EmployeeBaseEntity base = baseMap.get(history.baseId);
			base.addHistory(history);
		}

		return bases;
	}

	/** ベース用の検索フィルタ */
	public class SearchBaseFilter {
		/** 社員ベースID */
		public List<Id> baseIds;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 *         更新用で取得する場合(forUpdate)は、検索結果の取得順を指定しないため注意すること
	 */
	private List<EmployeeBaseEntity> searchBaseEntity(
			EmployeeRepository.SearchBaseFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && BASE_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchBaseEntity ReturnCache key:' + cacheKey);
			List<EmployeeBaseEntity> entities = new List<EmployeeBaseEntity>();
			for (ComEmpBase__c sObj : (List<ComEmpBase__c>)BASE_CACHE.get(cacheKey)) {
				entities.add(createBaseEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchBaseEntity NoCache key:' + cacheKey);

		// ベース WHERE句
		List<String> baseWhereList = new List<String>();
		// ベースIDで検索
		List<Id> pIds;
		if (filter.baseIds != null) {
			pIds = filter.baseIds;
			baseWhereList.add('Id IN :pIds');
		}
		String whereString = buildWhereString(baseWhereList);


		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// ベース項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 会社項目
		selectFieldList.addAll(GET_BASE_COMPANY_FIELD_NAME_LIST);
		// ユーザ項目
		selectFieldList.addAll(GET_BASE_USER_FIELD_NAME_LIST);

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ComEmpBase__c.SObjectType.getDescribe().getName()
				+ whereString;
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(ComEmpBase__c.Code__c);
		}

		System.debug('EmployeeRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComEmpBase__c> sObjs = (List<ComEmpBase__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComEmpBase__c.SObjectType);

		// SObjectからエンティティを作成
		List<EmployeeBaseEntity> entities = new List<EmployeeBaseEntity>();
		for (ComEmpBase__c sObj : sObjs) {
			entities.add(createBaseEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			BASE_CACHE.put(cacheKey, sObjs);
		}
		return entities;
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストのソート順
	 * @return 履歴エンティティの検索
	 *         更新用で取得する場合(forUpdate)は、検索結果の取得順を指定しないため注意すること
	 */
	public List<EmployeeHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, HistorySortOrder sortOrder) {
		// バインド変数が必要なため、クエリ作成処理をモジュール化していない

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter, sortOrder);
		if (isEnabledCache && !forUpdate && HISTORY_CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity ReturnCache key:' + cacheKey);
			List<EmployeeHistoryEntity> entities = new List<EmployeeHistoryEntity>();
			for (ComEmpHistory__c sObj : (List<ComEmpHistory__c>)HISTORY_CACHE.get(cacheKey)) {
				entities.add(createHistoryEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchHistoryEntity NoCache key:' + cacheKey);

		// ベース SELECT対象項目リスト
		// 勤務体系、工数設定、部署の名前(L0-L2)は履歴オブジェクトが持っているため、本メソッドでは取得対象外
		List<String> selectFieldList = new List<String>();
		// 履歴項目
		selectFieldList.addAll(GET_HISTORY_FIELD_NAME_LIST);
		// Cost Center
		selectFieldList.addAll(GET_HISTORY_COST_CENTER_FIELD_NAME_LIST);
		// 上長
		selectFieldList.addAll(GET_HISTORY_MANAGER_FIELD_NAME_LIST);
		// 部署
		selectFieldList.addAll(GET_HISTORY_DEPARTMENT_FIELD_NAME_LIST);
		// 36協定アラート設定
		selectFieldList.addAll(GET_HISTORY_AGREEMENT_ALERT_SETTING_FIELD_NAME_LIST);
		// 権限
		selectFieldList.addAll(GET_HISTORY_PERMISSION_FIELD_NAME_LIST);
		// Expense Employee Group
		selectFieldList.addAll(GET_EXPNESE_EMPLOYEE_GROUP_FIELD_NAME_LIST);

		// WHERE句
		List<String> whereList = new List<String>();
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ComEmpHistory__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ComEmpHistory__c.ValidTo__c) + ' > :pTargetDate');
		}
		Date pStartDate;
		Date pEndDate;
		if (filter.startDate != null && filter.endDate != null) {
			pStartDate = filter.startDate.getDate();
			pEndDate = filter.endDate.getDate();
			whereList.add(getFieldName(ComEmpHistory__c.ValidFrom__c) + ' <= :pEndDate');
			whereList.add(getFieldName(ComEmpHistory__c.ValidTo__c) + ' > :pStartDate');
		}
		// 履歴IDで検索
		Set<Id> pHistoryIds;
		if(filter.historyIds != null){
			pHistoryIds = filter.historyIds;
			whereList.add(getFieldName(ComEmpHistory__c.Id) + ' IN :pHistoryIds');
		}
		// 役職で検索(部分一致)
		String pTitleL;
		if(String.isNotBlank(filter.titleL)){
			pTitleL = '%' + filter.titleL + '%';
			String s = getFieldName(ComEmpHistory__c.Title_L0__c) + ' LIKE :pTitleL OR '
					+ getFieldName(ComEmpHistory__c.Title_L1__c) + ' LIKE :pTitleL OR '
					+ getFieldName(ComEmpHistory__c.Title_L2__c) + ' LIKE :pTitleL';
			whereList.add(s);
		}
		// 勤務体系IDで検索
		Set<Id> pWorkingTypeIds;
		if(filter.workingTypeIds != null){
			pWorkingTypeIds = filter.workingTypeIds;
			whereList.add(getFieldName(ComEmpHistory__c.WorkingTypeBaseId__c) + ' IN :pWorkingTypeIds');
		}

		// Expense Employee Group
		Set<Id> pExpEmployeeGroupIds;
		if (filter.expEmployeeGroupIdSet != null) {
			pExpEmployeeGroupIds = filter.expEmployeeGroupIdSet;
			whereList.add(getFieldName(ComEmpHistory__c.ExpEmployeeGroupId__c) + ' IN :pExpEmployeeGroupIds');
		}

		// 部署IDで検索
		Set<Id> pDepartmentIds;
		if(filter.departmentIds != null){
			pDepartmentIds = filter.departmentIds;
			whereList.add(getFieldName(ComEmpHistory__c.DepartmentBaseId__c) + ' IN :pDepartmentIds');
		}
		// 部署コードで検索（部分一致）
		String pDepartmentCode;
		if(String.isNotBlank(filter.departmentCode)){
			pDepartmentCode = '%' + filter.departmentCode + '%';
			whereList.add(getFieldName(HISTORY_DEPARTMENT_R, ComDeptBase__c.Code__c) + ' LIKE :pDepartmentCode');
		}
		// 上長名で検索(部分一致)
		String pManagerNameL;
		if(String.isNotBlank(filter.managerNameL)){
			pManagerNameL = '%' + filter.managerNameL + '%';
			String s = getFieldName(HISTORY_MANAGER_R, ComEmpBase__c.FirstName_L0__c) + ' LIKE :pManagerNameL OR '
					+ getFieldName(HISTORY_MANAGER_R, ComEmpBase__c.FirstName_L1__c) + ' LIKE :pManagerNameL OR '
					+ getFieldName(HISTORY_MANAGER_R, ComEmpBase__c.FirstName_L2__c) + ' LIKE :pManagerNameL OR '
					+ getFieldName(HISTORY_MANAGER_R, ComEmpBase__c.LastName_L0__c) + ' LIKE :pManagerNameL OR '
					+ getFieldName(HISTORY_MANAGER_R, ComEmpBase__c.LastName_L1__c) + ' LIKE :pManagerNameL OR '
					+ getFieldName(HISTORY_MANAGER_R, ComEmpBase__c.LastName_L2__c) + ' LIKE :pManagerNameL';
			whereList.add(s);
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			whereList.add(getFieldName(ComEmpHistory__c.Removed__c) + ' = false');
		}

		// ベースIDで検索
		Set<Id> pBaseIds;
		if (filter.baseIds != null) {
			pBaseIds = filter.baseIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ComEmpBase__c.Id) + ' IN :pBaseIds');
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ComEmpBase__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索(完全一致)
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(HISTORY_BASE_R, ComEmpBase__c.Code__c) + ' IN :pCodes');
		}
		// ユーザーIDで検索
		Set<Id> pUserIds;
		if (filter.userIds != null) {
			pUserIds = filter.userIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ComEmpBase__c.UserId__c) + ' IN :pUserIds');
		}
		// 社員コードで検索(部分一致)
		String pCode;
		if(String.isNotBlank(filter.code)){
			pCode = '%' + filter.code + '%';
			whereList.add(getFieldName(HISTORY_BASE_R, ComEmpBase__c.Code__c) + ' LIKE :pCode');
		}
		// ユーザ名で検索
		List<String> pUserNames;
		if (filter.userNames != null) {
			pUserNames = filter.userNames;
			whereList.add(getFieldName(HISTORY_BASE_R, ComEmpBase__c.UserId__c.getDescribe().getRelationshipName(), User.Username) + ' IN :pUserNames');
		}
		// 社員名で検索
		// バインド変数をリストで指定できない、2語以上指定されることも
		// 2語以上にも対応する必要があればバインド変数を使用せずSQOL文を作成する
		String pNameL1, pNameL2;
		if(String.isNotBlank(filter.nameL)){
			List<String> pNameList = filter.nameL.replace('　', ' ').split(' ');
			if (pNameList.size() >= 1) {
				pNameL1 = '%' + pNameList[0] + '%';
				String s = getFieldName(HISTORY_BASE_R, ComEmpBase__c.FirstName_L0__c) + ' LIKE :pNameL1 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.FirstName_L1__c) + ' LIKE :pNameL1 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.FirstName_L2__c) + ' LIKE :pNameL1 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.LastName_L0__c) + ' LIKE :pNameL1 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.LastName_L1__c) + ' LIKE :pNameL1 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.LastName_L2__c) + ' LIKE :pNameL1';
				whereList.add(s);
			}
			if (pNameList.size() >= 2) {
				pNameL2 = '%' + pNameList[1] + '%';
				String s = getFieldName(HISTORY_BASE_R, ComEmpBase__c.FirstName_L0__c) + ' LIKE :pNameL2 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.FirstName_L1__c) + ' LIKE :pNameL2 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.FirstName_L2__c) + ' LIKE :pNameL2 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.LastName_L0__c) + ' LIKE :pNameL2 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.LastName_L1__c) + ' LIKE :pNameL2 OR '
						+ getFieldName(HISTORY_BASE_R, ComEmpBase__c.LastName_L2__c) + ' LIKE :pNameL2';
				whereList.add(s);
			}
		}
		// 承認者権限01で検索
		if (filter.approvalAuthority01 == true) {
			whereList.add(getFieldName(ComEmpHistory__c.ApprovalAuthority01__c) + ' = true');
		}

		// ORDER BY句
		String orderBy = ' ORDER BY ' + getFieldName(HISTORY_BASE_R, ComEmpBase__c.Code__c);
		if (sortOrder == HistorySortOrder.VALID_FROM_ASC) {
			orderBy += ', ' + getFieldName(ComEmpHistory__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == HistorySortOrder.VALID_FROM_DESC) {
			orderBy += ', ' + getFieldName(ComEmpHistory__c.ValidFrom__c) + ' DESC NULLS LAST';
		}

		// クエリ作成
		// コードごとに有効期間が新しい順
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ComEmpHistory__c' +
				+ buildWhereString(whereList);
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += orderBy;
		}
		System.debug('EmployeeRepository: SOQL=' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<ComEmpHistory__c> sObjs = (List<ComEmpHistory__c>)Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(ComEmpHistory__c.SObjectType);
		List<EmployeeHistoryEntity> entities = new List<EmployeeHistoryEntity>();
		for (ComEmpHistory__c sObj : sObjs) {
			entities.add(createHistoryEntity(sObj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			HISTORY_CACHE.put(cacheKey, sobjs);
		}
		return entities;
	}

	/**
	 * ベースオブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone) {
		List<ComEmpBase__c> deptList = new List<ComEmpBase__c>();
		for (SObject obj : baseObjList) {
			deptList.add((ComEmpBase__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * 履歴オブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone) {
		List<ComEmpHistory__c> deptList = new List<ComEmpHistory__c>();
		for (SObject obj : historyObjList) {
			deptList.add((ComEmpHistory__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private EmployeeBaseEntity createBaseEntity(ComEmpBase__c baseObj) {
		return new EmployeeBaseEntity(baseObj);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private EmployeeHistoryEntity createHistoryEntity(ComEmpHistory__c historyObj) {
		return new EmployeeHistoryEntity(historyObj);
	}

	/**
	 * ベースエンティティからオブジェクトを作成する（各リポジトリで実装する）
	 */
	protected override SObject createBaseObj(ParentChildBaseEntity superBaseEntity) {
		return ((EmployeeBaseEntity)superBaseEntity).createSObject();
	}

	/**
	 * 履歴エンティティからオブジェクトを作成する
	 * @param historyEntity 作成元の履歴エンティティ
	 * @return 作成した履歴オブジェクト
	 */
	protected override SObject createHistoryObj(ParentChildHistoryEntity superHistoryEntity) {
		return ((EmployeeHistoryEntity)superHistoryEntity).createSObject();
	}

	/**
	 * 保存したキャッシュをクリアする。
	 *
	 * TODO エンティティ保存のタイミングでキャッシュをクリアする処理を実装する。
	 *      それまでは、テストケースからのみ実行できるようにしておく。
	 */
	@TestVisible
	private static void clearCache() {
		BASE_CACHE.clearAll();
		HISTORY_CACHE.clearAll();
	}
}