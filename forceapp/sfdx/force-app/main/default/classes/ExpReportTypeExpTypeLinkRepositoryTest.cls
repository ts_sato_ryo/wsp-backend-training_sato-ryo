/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description 経費申請タイプ別費目リポジトリのテスト Test class for ExpReportTypeExpTypeLinkRepository
 */
@isTest
private class ExpReportTypeExpTypeLinkRepositoryTest {
	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする Rollback transaction if there is a failure upon saving of record */
	private final static Boolean ALL_SAVE = true;

	/** テストデータ Test Data */
	private class RepoTestData {
		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') Organization Object */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト Country Object */
		public ComCountry__c countryObj;
		/** 会社オブジェクト Company Object */
		public ComCompany__c companyObj;
		/** 経費申請タイプオブジェクトリスト List of ExpReportType Objects */
		public List<ExpReportType__c> expReportTypeObjs;
		/** 費目オブジェクトリスト List of ExpType Objects */
		public List<ExpType__c> expTypeObjs;
		/** 経費申請タイプ別費目オブジェクト List of ExpReportTypeExpTypeLink Objects */
		public List<ExpReportTypeExpTypeLink__c> expReportTypeExpTypeLinkObjs;

		/** コンストラクタ Constructor */
		public RepoTestData(Integer expReportTypeSize, Integer expTypeSize) {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
			expReportTypeObjs = ComTestDataUtility.createExpReportTypes('Test ERT', companyObj.Id, expReportTypeSize);
			expTypeObjs = ComTestDataUtility.createExpTypes('Test ET', companyObj.Id, expTypeSize);

			expReportTypeExpTypeLinkObjs = new List<ExpReportTypeExpTypeLink__c>();
			for (ExpReportType__c expReportTypeObj : expReportTypeObjs) {
				for (ExpType__c expTypeObj : expTypeObjs) {
					ExpReportTypeExpTypeLink__c obj = new ExpReportTypeExpTypeLink__c(
							ExpReportTypeId__c = expReportTypeObj.Id, 
							ExpTypeId__c = expTypeObj.Id);
					expReportTypeExpTypeLinkObjs.add(obj);
				}
			}
			insert expReportTypeExpTypeLinkObjs;
		}
	}

	/**
	 * getEntityのテスト getEntity Test
	 * 指定したIDで取得できることを確認する Verify specified ID can be retrieved
	 */
	@isTest static void getEntityTest() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);
		
		ExpReportTypeExpTypeLink__c target = getSObject(testData.expReportTypeExpTypeLinkObjs[4].Id);
		ExpReportTypeExpTypeLinkEntity entity = new ExpReportTypeExpTypeLinkRepository().getEntity(target.Id);

		// 各項目の値が正しく取得できているかのチェックはsearchEntityListのテストで行う Execute searchEntityList and validate entity retrieved
		TestUtil.assertNotNull(entity);
		assertEntity(target, entity);
	}

	/**
	 * getEntityのテスト getEntity Test
	 * 指定したIDのレコードが存在しない場合、nullが返却されることを確認する Verify null is returned if specified ID does not exists
	 */
	@isTest static void getEntityTestNotFound() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		Id targetId = UserInfo.getUserId();
		ExpReportTypeExpTypeLinkEntity entity = new ExpReportTypeExpTypeLinkRepository().getEntity(targetId);

		System.assertEquals(null, entity);
	}

	/**
	 * getEntityListのテスト getEntityList Test
	 * 指定したIDで取得できることを確認する Verify specified IDs can be retrieved
	 */
	@isTest static void getEntityListTest() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		ExpReportTypeExpTypeLink__c target1 = getSObject(testData.expReportTypeExpTypeLinkObjs[1].Id);
		ExpReportTypeExpTypeLink__c target2 = getSObject(testData.expReportTypeExpTypeLinkObjs[5].Id);
		List<ExpReportTypeExpTypeLinkEntity> entities =
				new ExpReportTypeExpTypeLinkRepository().getEntityList(new List<Id>{target2.Id, target1.Id});

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * getEntityListByExpReportTypeIdのテスト getEntityListByExpReportTypeId Test
	 * 指定したIDで取得できることを確認する Verify specified ExpReportType ID can be retrieved
	 */
	@isTest static void getEntityListByExpReportTypeTest() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		ExpReportTypeExpTypeLink__c target1 = getSObject(testData.expReportTypeExpTypeLinkObjs[0].Id);
		ExpReportTypeExpTypeLink__c target2 = getSObject(testData.expReportTypeExpTypeLinkObjs[1].Id);
		List<ExpReportTypeExpTypeLinkEntity> entities =
				new ExpReportTypeExpTypeLinkRepository().getEntityListByExpReportTypeId(new Set<Id>{target1.ExpReportTypeId__c});

		System.assertEquals(2, entities.size());
		assertEntity(target1, entities[0]);
		assertEntity(target2, entities[1]);
	}

	/**
	 * getEntityListByExpTypeIdTest
	 * Verify specified ExpType ID can be retrieved
	 */
	@isTest static void getEntityListByExpTypeIdTest() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		ExpReportTypeExpTypeLink__c target1 = getSObject(testData.expReportTypeExpTypeLinkObjs[0].Id);
		ExpReportTypeExpTypeLink__c target2 = getSObject(testData.expReportTypeExpTypeLinkObjs[1].Id);
		ExpReportTypeExpTypeLink__c target3 = getSObject(testData.expReportTypeExpTypeLinkObjs[2].Id);
		target3.ExpTypeId__c = target1.ExpTypeId__c;
		update target3;
		List<ExpReportTypeExpTypeLinkEntity> entities =
				new ExpReportTypeExpTypeLinkRepository().getEntityListByExpTypeId(target1.ExpTypeId__c, null, 1, null);

		System.assertEquals(1, entities.size());
		assertEntity(target1, entities[0]);
	}

	/**
	 * searchEntityListのテスト searchEntityList Test
	 * 全ての項目の値が正しく取得できていることを確認する Verify all fields can be retrieved correctly
	 */
	@isTest static void searchEntityListTestGetAllFields() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		ExpReportTypeExpTypeLink__c targetObj = getSObject(testData.expReportTypeExpTypeLinkObjs[1].Id);

		ExpReportTypeExpTypeLinkRepository.SearchFilter filter = new ExpReportTypeExpTypeLinkRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		List<ExpReportTypeExpTypeLinkEntity> entities = new ExpReportTypeExpTypeLinkRepository().searchEntityList(filter);

		System.assertEquals(1, entities.size());
		assertEntity(targetObj, entities[0]);
	}

	/**
	 * searchEntityListのテスト searchEntityList Test
	 * 項目の値が正しく取得できていることを確認する Verify specific fields can be retrieved correctly
	 */
	@isTest static void searchEntityListTestGetSpecificFields() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		ExpReportTypeExpTypeLink__c targetObj = getSObject(testData.expReportTypeExpTypeLinkObjs[1].Id);

		// set filter condition
		ExpReportTypeExpTypeLinkRepository.SearchFilter filter = new ExpReportTypeExpTypeLinkRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj.Id};
		// set SelectDetail conditions
		Set<ExpReportTypeExpTypeLinkRepository.SelectDetail> selectDetailSet = new Set<ExpReportTypeExpTypeLinkRepository.SelectDetail>
				{ExpReportTypeExpTypeLinkRepository.SelectDetail.EXP_TYPE_FIELD_NAME_LIST};
		List<ExpReportTypeExpTypeLinkEntity> entities = new ExpReportTypeExpTypeLinkRepository().searchEntityList(filter, selectDetailSet, null);

		System.assertEquals(1, entities.size());
		
		// assert base entity details are retrieved
		System.assertEquals(targetObj.Id, entities[0].id);
		System.assertEquals(targetObj.ExpReportTypeId__c, entities[0].expReportTypeId);
		System.assertEquals(targetObj.ExpTypeId__c, entities[0].expTypeId);

		// assert ExpReportType name details are not retrieved
		System.assertEquals(null, entities[0].expReportTypeNameL);

		// assert ExpType name details are retrieved
		System.assertEquals(targetObj.ExpTypeId__r.Name_L0__c, entities[0].expTypeNameL.valueL0);
		System.assertEquals(targetObj.ExpTypeId__r.Name_L1__c, entities[0].expTypeNameL.valueL1);
		System.assertEquals(targetObj.ExpTypeId__r.Name_L2__c, entities[0].expTypeNameL.valueL2);

		// assert ExpType additional details are not retrieved
		System.assertEquals(null, entities[0].expTypeCode);
		System.assertEquals(null, entities[0].expTypeRecordType);
		System.assertEquals(null, entities[0].expTypeValidFrom);
		System.assertEquals(null, entities[0].expTypeValidTo);
	}

	/**
	 * searchEntityListのテスト searchEntityList Test
	 * 指定した条件で取得できることを確認する Verify entities can be retrieved correctly based on filter criteria
	 */
	@isTest static void searchEntityListTestFilter() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		// 同じ経費申請タイプ Same ExpReportType
		ExpReportTypeExpTypeLink__c targetObj1 = getSObject(testData.expReportTypeExpTypeLinkObjs[0].Id);
		ExpReportTypeExpTypeLink__c targetObj2 = getSObject(testData.expReportTypeExpTypeLinkObjs[1].Id);

		ExpReportTypeExpTypeLinkRepository.SearchFilter filter;
		ExpReportTypeExpTypeLinkRepository repo = new ExpReportTypeExpTypeLinkRepository();
		List<ExpReportTypeExpTypeLinkEntity> resEntities;

		// TEST1: IDで検索 Search by Id
		filter = new ExpReportTypeExpTypeLinkRepository.SearchFilter();
		filter.ids = new Set<Id>{targetObj2.Id};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(1, resEntities.size());
		System.assertEquals(targetObj2.Id, resEntities[0].id);

		// TEST2: 経費申請タイプIDで検索 Search by ExpReportType Id
		ExpType__c expType1 = testData.expTypeObjs[1];
		ExpType__c expType2 = testData.expTypeObjs[0];
		targetObj1.ExpTypeId__c = expType2.Id;
		targetObj2.ExpTypeId__c = expType1.Id;
		update new List<ExpReportTypeExpTypeLink__c>{targetObj1, targetObj2};
		targetObj1.ExpTypeId__c = expType2.Id;
		filter = new ExpReportTypeExpTypeLinkRepository.SearchFilter();
		filter.expReportTypeIds = new Set<Id>{targetObj1.ExpReportTypeId__c};
		resEntities = repo.searchEntityList(filter);
		System.assertEquals(2, resEntities.size());
		System.assertEquals(targetObj2.ExpReportTypeId__c, resEntities[0].expReportTypeId);
		System.assertEquals(targetObj1.ExpReportTypeId__c, resEntities[1].expReportTypeId);
	}

	/*
	 * Search Entity by expense type date range test
	 */
	@isTest static void searchEntityWithDateRange() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		ExpReportTypeExpTypeLinkRepository.SearchFilter filter = new ExpReportTypeExpTypeLinkRepository.SearchFilter();
		ExpReportTypeExpTypeLinkRepository repo = new ExpReportTypeExpTypeLinkRepository();
		filter.startDate = AppDate.today();
		filter.endDate = AppDate.today();

		List<ExpReportTypeExpTypeLinkEntity> resEntities = repo.searchEntityList(filter);
		System.assertEquals(3, resEntities.size());
		System.assertEquals(testData.expReportTypeObjs[0].id, resEntities[0].expReportTypeId);
		System.assertEquals(testData.expTypeObjs[0].id, resEntities[0].expTypeId);
		System.assertEquals(testData.expReportTypeObjs[1].id, resEntities[1].expReportTypeId);
		System.assertEquals(testData.expTypeObjs[0].id, resEntities[1].expTypeId);
		System.assertEquals(testData.expReportTypeObjs[2].id, resEntities[2].expReportTypeId);
		System.assertEquals(testData.expTypeObjs[0].id, resEntities[2].expTypeId);
	}

	/**
	 * saveEntityのテスト saveEntity Test
	 * 新規保存できることを確認する Verify creation of new record
	 */
	@isTest static void saveEntityTestNew() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		// テストデータの経費申請タイプ別費目を削除しておく Delete test data of ExpReportTypeExpTypeLink
		delete testData.expReportTypeExpTypeLinkObjs;

		ExpReportTypeExpTypeLinkEntity entity = new ExpReportTypeExpTypeLinkEntity();
		entity.expReportTypeId = testData.expReportTypeObjs[2].Id;
		entity.expTypeId = testData.expTypeObjs[1].Id;

		Repository.SaveResult result = new ExpReportTypeExpTypeLinkRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ExpReportTypeExpTypeLink__c resObj = getSObject(result.details[0].id);
		assertSObject(entity, resObj);
	}
	
	/**
	 * saveEntityのテスト saveEntity Test
	 * 既存のエンティティを保存できることを確認する Verify update of existing records
	 */
	@isTest static void saveEntityTestUpdate() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		ExpReportTypeExpTypeLink__c target = testData.expReportTypeExpTypeLinkObjs[0];
		ExpReportTypeExpTypeLinkEntity entity = new ExpReportTypeExpTypeLinkEntity();
		entity.setId(target.Id);
		entity.expReportTypeId = testData.expReportTypeObjs[2].Id;
		entity.expTypeId = testData.expTypeObjs[1].Id;

		Repository.SaveResult result = new ExpReportTypeExpTypeLinkRepository().saveEntity(entity);

		System.assertEquals(true, result.isSuccessAll);
		ExpReportTypeExpTypeLink__c resObj = getSObject(target.Id);
		assertSObject(entity, resObj);
	}

	/**
	 * saveEntityListのテスト saveEntityList Test
	 * 新規、既存のエンティティが保存できることを確認する Verify upsert of new and existing records
	 */
	@isTest static void saveEntityListTest() {
		final Integer expReportTypeSize = 3;
		final Integer expTypeSize = 2;
		RepoTestData testData = new RepoTestData(expReportTypeSize, expTypeSize);

		// 既存 Existing record
		ExpReportTypeExpTypeLink__c orgObj = testData.expReportTypeExpTypeLinkObjs[0];
		ExpReportTypeExpTypeLinkEntity entityUd = new ExpReportTypeExpTypeLinkEntity();
		entityUd.setId(orgObj.Id);
		entityUd.expReportTypeId = testData.expReportTypeObjs[2].Id;
		entityUd.expTypeId = testData.expTypeObjs[1].Id;

		// 新規 New record
		ExpReportTypeExpTypeLinkEntity entityNew = new ExpReportTypeExpTypeLinkEntity();
		entityNew.expReportTypeId = testData.expReportTypeObjs[2].Id;
		entityNew.expTypeId = testData.expTypeObjs[1].Id;

		Repository.SaveResult result =
				new ExpReportTypeExpTypeLinkRepository().saveEntityList(new List<ExpReportTypeExpTypeLinkEntity>{entityUd, entityNew});

		System.assertEquals(true, result.isSuccessAll);
		ExpReportTypeExpTypeLink__c resObjUd = getSObject(orgObj.Id);
		assertSObject(entityUd, resObjUd);
		ExpReportTypeExpTypeLink__c resObjNew = getSObject(result.details[1].id);
		assertSObject(entityNew, resObjNew);
	}


	/**
	 * 取得したエンティティが期待値通りであることを確認する Verify entity details
	 * @param expObj Object to compare
	 * @param actEntity エンティティ Target entity to verify
	 */
	private static void assertEntity(ExpReportTypeExpTypeLink__c expObj, ExpReportTypeExpTypeLinkEntity actEntity) {

		// assert base entity details are retrieved
		System.assertEquals(expObj.Id, actEntity.id);
		System.assertEquals(expObj.ExpReportTypeId__c, actEntity.expReportTypeId);
		System.assertEquals(expObj.ExpTypeId__c, actEntity.expTypeId);

		// assert ExpReportType name details are retrieved
		System.assertEquals(expObj.ExpReportTypeId__r.Name_L0__c, actEntity.expReportTypeNameL.valueL0);
		System.assertEquals(expObj.ExpReportTypeId__r.Name_L1__c, actEntity.expReportTypeNameL.valueL1);
		System.assertEquals(expObj.ExpReportTypeId__r.Name_L2__c, actEntity.expReportTypeNameL.valueL2);

		// assert ExpType name details are retrieved
		System.assertEquals(expObj.ExpTypeId__r.Name_L0__c, actEntity.expTypeNameL.valueL0);
		System.assertEquals(expObj.ExpTypeId__r.Name_L1__c, actEntity.expTypeNameL.valueL1);
		System.assertEquals(expObj.ExpTypeId__r.Name_L2__c, actEntity.expTypeNameL.valueL2);

		// assert ExpType additional details are retrieved
		System.assertEquals(expObj.ExpTypeId__r.Code__c, actEntity.expTypeCode);
		System.assertEquals(expObj.ExpTypeId__r.RecordType__c, actEntity.expTypeRecordType.value);
		System.assertEquals(AppDate.valueOf(expObj.ExpTypeId__r.ValidFrom__c), actEntity.expTypeValidFrom);
		System.assertEquals(AppDate.valueOf(expObj.ExpTypeId__r.ValidTo__c), actEntity.expTypeValidTo);

		/** TODO: Add additional assertions for any additional fields in ExpType details */
	}

	/**
	 * 保存したSObjectが期待値通りであることを確認する Verify saved sObject details
	 * @param expEntity エンティティ Entity to compare
	 * @param actObj Target object to verify
	 */
	private static void assertSObject(ExpReportTypeExpTypeLinkEntity expEntity, ExpReportTypeExpTypeLink__c actObj) {
		System.assertEquals(expEntity.expReportTypeId, actObj.ExpReportTypeId__c);
		System.assertEquals(expEntity.expTypeId, actObj.ExpTypeId__c);
	}
	
	/**
	 * 指定したIDのExpReportTypeExpTypeLink__cを取得する Retrieve ExpReportTypeExpTypeLink from the specified Id
	 */
	private static ExpReportTypeExpTypeLink__c getSObject(Id id) {
		/** TODO: Add additional field retrieval for any additional fields in ExpType details */
		return [
				SELECT Id, Name, ExpReportTypeId__c, ExpTypeId__c,
					ExpReportTypeId__r.Name_L0__c, ExpReportTypeId__r.Name_L1__c, ExpReportTypeId__r.Name_L2__c,
					ExpTypeId__r.Name_L0__c, ExpTypeId__r.Name_L1__c, ExpTypeId__r.Name_L2__c,
					ExpTypeId__r.Code__c, ExpTypeId__r.RecordType__c, ExpTypeId__r.ValidFrom__c, ExpTypeId__r.ValidTo__c
				FROM ExpReportTypeExpTypeLink__c
				WHERE Id = :id];
	}
}
