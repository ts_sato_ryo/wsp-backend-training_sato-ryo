// APIパスに対応するメソッドをコールするクラス
// APIを追加・削除するときはこのクラスを編集する
// ビジネスロジックの変更を理由に編集するのは禁止
public class RemoteApiRoute {

	/**
	 * APIメソッドをコールする
	 * @param  request_parameter リクエストパラメータ(JSON形式)
	 * @return APIの実行レスポンス
	 */
	public static RemoteApi.ResponseBase execute(String request_parameter) {
		RemoteApi.ResponseParam resParam;
		RemoteApi.Error err;
		Savepoint sp;
		try {
			// リクエストパラメータをJSON文字列からオブジェクトに変換
			// #パラメータが不正だと例外が発生します(メソッドコメント参照)
			RemoteApi.Request req = createRequest(request_parameter);
			String path = req.getPath();

			if (!RemoteApiRouteMap.MAPPING.containsKey(path)) {
				throw new RemoteApi.NotFoundException('リクエストパス "' + path + '"は存在しません');
			}

			Type resourceType = RemoteApiRouteMap.MAPPING.get(path);
			RemoteApi.ResourceBase resource = (RemoteApi.ResourceBase)resourceType.newInstance();
			if (resource.isAutoRollback()) {
				sp = Database.setSavepoint();
			}
			resParam = resource.execute(req);

		} catch (RemoteApi.BaseException e) {
			rollback(sp);
			err = new RemoteApi.Error();
			err.errorCode = e.getErrorCode();
			err.message = e.getMessage();
		} catch (App.BaseException e) {
			rollback(sp);
			err = new RemoteApi.Error();
			err.errorCode = e.getErrorCode();
			err.message = e.getMessage();
		} catch (DmlException e) {
			rollback(sp);
			err = new RemoteApi.Error();
			err.errorCode = e.getDmlStatusCode(0);
			err.message = e.getDmlMessage(0);
			err.stackTrace = e.getStackTraceString();
		} catch (Exception e) {
			rollback(sp);
			err = new RemoteApi.Error();
			err.errorCode = 'UNEXPECTED_ERROR';
			err.message = e.getMessage();
			err.stackTrace = e.getStackTraceString();
		}

		RemoteApi.ResponseBase res;

		if (err == null) {
			// 成功レスポンスをセット
			if (resParam instanceof RemoteApi.BatchApiResponse) {
				RemoteApi.BatchApiResponse batchRes = (RemoteApi.BatchApiResponse)resParam;
				res = new RemoteApi.SuccessBatchResponse(batchRes.results);
			} else {
				res = new RemoteApi.SuccessResponse(resParam);
			}
		} else {
			// エラーレスポンスをセット
			res = new RemoteApi.ErrorResponse(err);
		}

		// 実行結果を返す
		return res;
	}

	/**
	 * 実行パラメータ(JSON)をリクエストオブジェクトに変換する
	 * #パラメータ形式が不正な場合は、例外が発生します
	 * @param  request_parameter 実行パラメータ。次の形式のJSON文字列: { path:[String], param:[Object] }
	 * @return リクエストパラメータを格納したオブジェクト
	 */
	private static RemoteApi.Request createRequest(String request_parameter){
		String apiPath;
		Map<String, Object> apiParamMap;

		// リクエストパラメータが正しい形式になっていることをチェックする
		try {
			Map<String, Object> reqParamMap = (Map<String, Object>)JSON.deserializeUntyped(request_parameter);
			apiPath = (String)reqParamMap.get('path');
			apiParamMap = (Map<String, Object>)reqParamMap.get('param');
		} catch(Exception e) {
			throw new App.ParameterException('パラメータの形式が不正です。次の形式のみ指定可能です: { path:[String], param:[Object] }');
		}

		// エラー: pathが未指定
		if(String.isBlank(apiPath)){
			throw new App.ParameterException('pathの値を指定して下さい。');
		}

		return new RemoteApi.Request(apiPath, apiParamMap);
	}

	/**
	 * セーブポイントへロールバックする
	 * @param sp セーブポイント（nullの場合は何もしない）
	 */
	private static void rollback(Savepoint sp) {
		if (sp == null) {
			return;
		}
		Database.rollback(sp);
	}
}