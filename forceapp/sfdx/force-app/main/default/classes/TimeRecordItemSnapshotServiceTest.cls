/**
 * @group 工数
 *
 * TimeRecordItemSnapshotServiceのテストクラス
 */
@isTest
private class TimeRecordItemSnapshotServiceTest {
	
	/**
	 * 工数内訳スナップショットを新規に登録できる事を検証する
	 */
	@isTest
	static void saveTestNewSnapshot() {
		// テストデータ作成
		AppDate targetDate = AppDate.newInstance(2019, 4, 1);
		TimeTestData testData = new TimeTestData(targetDate);
		TimeSummary__c summary = testData.createMonthlySummary(targetDate);
		List<TimeRecord__c> records = testData.createRecordList(summary);

		// 工数内訳を明細につき2件ずつ作成
		ComJob__c job = testData.jobList[0];
		TimeWorkCategory__c workCategory = testData.workCategoryList[0];
		TimeRecordItem__c item1 = testData.createRecordItem(records[0], job);
		TimeRecordItem__c item2 = testData.createRecordItem(records[0], job, workCategory);
		TimeRecordItem__c item3 = testData.createRecordItem(records[records.size() - 1], job);
		TimeRecordItem__c item4 = testData.createRecordItem(records[records.size() - 1], job, workCategory);
		
		// 実行
		List<TimeRecordEntity> recordEntities = new TimeSummaryRepository().getRecordListBySummaryId(summary.Id);
		TimeRecordItemSnapshotService service = new TimeRecordItemSnapshotService(testData.company.Id);
		List<TimeRecordItemSnapshotEntity> snapshotsEntities = service.save(recordEntities);

		// スナップショットを全件取得（日付昇順、作業分類Id昇順）
		List<TimeRecordItemSnapshot__c> snapshots = [Select Id, TimeRecordItemId__c, JobIdLevel1__c, WorkCategoryId__c 
			 											From TimeRecordItemSnapshot__c 
			 											Order by TimeRecordItemId__r.TimeRecordId__r.Date__c, WorkCategoryId__c NULLS FIRST];
		// 登録結果を検証
		System.assertEquals(4, snapshots.size());
		// 1件目
		System.assertEquals(item1.Id, snapshots[0].TimeRecordItemId__c);
		System.assertEquals(job.Id, snapshots[0].JobIdLevel1__c);
		System.assertEquals(null, snapshots[0].WorkCategoryId__c);
		// 2件目
		System.assertEquals(item2.Id, snapshots[1].TimeRecordItemId__c);
		System.assertEquals(job.Id, snapshots[1].JobIdLevel1__c);
		System.assertEquals(workCategory.Id, snapshots[1].WorkCategoryId__c);
		// 3件目
		System.assertEquals(item3.Id, snapshots[2].TimeRecordItemId__c);
		System.assertEquals(job.Id, snapshots[2].JobIdLevel1__c);
		System.assertEquals(null, snapshots[2].WorkCategoryId__c);
		// 4件目
		System.assertEquals(item4.Id, snapshots[3].TimeRecordItemId__c);
		System.assertEquals(job.Id, snapshots[3].JobIdLevel1__c);
		System.assertEquals(workCategory.Id, snapshots[3].WorkCategoryId__c);
	}
	
	/**
	 * 工数内訳スナップショットが登録済みの場合、既存のレコードを削除後に新たに登録する事を検証する
	 */
	@isTest
	static void saveTestReplaceSnapshot() {
		// テストデータ作成
		AppDate targetDate = AppDate.newInstance(2019, 4, 1);
		TimeTestData testData = new TimeTestData(targetDate);
		TimeSummary__c summary = testData.createMonthlySummary(targetDate);
		List<TimeRecord__c> records = testData.createRecordList(summary);

		// 工数内訳を作成
		ComJob__c oldJob = testData.jobList[0];
		TimeWorkCategory__c oldWorkCategory = testData.workCategoryList[0];
		TimeRecordItem__c item1 = testData.createRecordItem(records[0], oldJob, oldWorkCategory);
		TimeRecordItem__c item2 = testData.createRecordItem(records[records.size() - 1], oldJob, oldWorkCategory);

		// スナップショットを作成
		TimeRecordItemSnapshot__c oldSnapshot1 = testData.createSnapshot(item1, oldJob, oldWorkCategory);
		TimeRecordItemSnapshot__c oldSnapshot2 = testData.createSnapshot(item2, oldJob, oldWorkCategory);

		// 工数内訳を修正
		ComJob__c newJob = testData.jobList[1];
		TimeWorkCategory__c newWorkCategory = testData.workCategoryList[1];
		item1.jobId__c = newJob.Id;
		item2.jobId__c = newJob.Id;
		item1.WorkCategoryId__c = newWorkCategory.Id;
		item2.WorkCategoryId__c = newWorkCategory.Id;
		update item1;
		update item2;

		// 実行
		List<TimeRecordEntity> recordEntities = new TimeSummaryRepository().getRecordListBySummaryId(summary.Id);
		TimeRecordItemSnapshotService service = new TimeRecordItemSnapshotService(testData.company.Id);
		List<TimeRecordItemSnapshotEntity> snapshotsEntities = service.save(recordEntities);

		// スナップショットを全件取得（日付昇順）
		List<TimeRecordItemSnapshot__c> snapshots = [Select Id, TimeRecordItemId__c, JobIdLevel1__c, WorkCategoryId__c 
			 											From TimeRecordItemSnapshot__c 
			 											Order by TimeRecordItemId__r.TimeRecordId__r.Date__c];
		// 登録結果を検証
		System.assertEquals(2, snapshots.size());
		// 1件目
		System.assertNotEquals(oldSnapshot1.Id, snapshots[0].Id);
		System.assertEquals(newJob.Id, snapshots[0].JobIdLevel1__c);
		System.assertEquals(newWorkCategory.Id, snapshots[0].WorkCategoryId__c);
		// 2件目
		System.assertNotEquals(oldSnapshot2.Id, snapshots[1].Id);
		System.assertEquals(newJob.Id, snapshots[1].JobIdLevel1__c);
		System.assertEquals(newWorkCategory.Id, snapshots[1].WorkCategoryId__c);
	}

	/**
	 * 工数サマリーがロック済みの場合、工数内訳スナップショットを作成しないことを検証する
	 */
	@isTest
	static void saveTestLockedSummary() {
		// テストデータ作成
		TimeTestData testData = new TimeTestData(AppDate.newInstance(2019, 4, 1));
		ComJob__c job = testData.jobList[0];

		// 確定済みの工数
		TimeSummary__c lockedSummary = testData.createSummary(AppDate.newInstance(2019, 4, 1), AppDate.newInstance(2019, 4, 3));
		lockedSummary.Locked__c = true;
		update lockedSummary;
		List<TimeRecord__c> lockedRecords = testData.createRecordList(lockedSummary);
		TimeRecordItem__c lockedItem = testData.createRecordItem(lockedRecords[1], job);

		// 未確定の工数
		TimeSummary__c unlockedSummary = testData.createSummary(AppDate.newInstance(2019, 5, 1), AppDate.newInstance(2019, 5, 3));
		List<TimeRecord__c> unlockedRecords = testData.createRecordList(unlockedSummary);
		TimeRecordItem__c unlockedItem = testData.createRecordItem(unlockedRecords[1], job);
		
		// 実行
		List<TimeRecordEntity> records = new TimeSummaryRepository().getRecordList(testData.emp.Id, AppDate.newInstance(2019, 4, 1), AppDate.newInstance(2019, 5, 3));
		TimeRecordItemSnapshotService service = new TimeRecordItemSnapshotService(testData.company.Id);
		List<TimeRecordItemSnapshotEntity> snapshotsEntities = service.save(records);

		// ロック済みの工数のスナップショットは登録されていないことを結果を検証
		List<TimeRecordItemSnapshot__c> snapshots = [Select Id, TimeRecordItemId__c From TimeRecordItemSnapshot__c];
		System.assertEquals(1, snapshots.size());
		System.assertEquals(unlockedItem.Id, snapshots[0].TimeRecordItemId__c);
	}

	/**
	 * ジョブの階層をリストに変換出来ることを検証する
	 */
	@isTest
	static void jobListTest() {
		// job1 ┬ job2 ─ job3 ─ job4
		//      └ job5 ─ job6 ─ job7
		JobEntity job1 = createJob(null);
		JobEntity job2 = createJob(job1);
		JobEntity job3 = createJob(job2);
		JobEntity job4 = createJob(job3);
		JobEntity job5 = createJob(job1);
		JobEntity job6 = createJob(job5);
		JobEntity job7 = createJob(job6);
		Map<Id, JobEntity> jobs = new Map<Id, JobEntity>{
			job1.id => job1,
			job2.id => job2,
			job3.id => job3,
			job4.id => job4,
			job5.id => job5,
			job6.id => job6,
			job7.id => job7
		};
		
		// 最上位のジョブ
		TimeRecordItemSnapshotService service = new TimeRecordItemSnapshotService(createId(ComCompany__c.sObjectType));
		List<JobEntity> top = service.createJobTree(jobs, job1.id);
		System.assertEquals(1, top.size());
		System.assertEquals(job1.id, top[0].id);

		// 中間のジョブ
		List<JobEntity> middle = service.createJobTree(jobs, job6.id);
		System.assertEquals(3, middle.size());
		System.assertEquals(job1.id, middle[0].id);
		System.assertEquals(job5.id, middle[1].id);
		System.assertEquals(job6.id, middle[2].id);
		
		// 最下位のジョブ
		List<JobEntity> bottom = service.createJobTree(jobs, job4.id);
		System.assertEquals(4, bottom.size());
		System.assertEquals(job1.id, bottom[0].id);
		System.assertEquals(job2.id, bottom[1].id);
		System.assertEquals(job3.id, bottom[2].id);
		System.assertEquals(job4.id, bottom[3].id);
	}

	/**
	 * ジョブエンティティを作成する
	 * @param parent 親ジョブ（最上位のジョブを作成する場合はnullを指定する）
	 * @return 作成したエンティティ（IdとparentIdのみ値が設定されている）
	 */
	private static JobEntity createJob(JobEntity parent) {
		JobEntity job = new JobEntity();
		job.setId(createId(ComJob__c.sObjectType));
		job.parentId = parent == null ? null : parent.id;
		return job;
	}

	/** テスト用のIDのカウンタ */
	private static Integer idSeq = 0;

	/**
	 * 指定したオブジェクトのダミーのIDを作成する
	 * テスト用にエンティティの値を設定したい場合のみ使用すること
	 * @param objType
	 * @return 生成したダミーのID
	 */
	private static Id createId(Schema.SObjectType objType) {
		String cnt = String.valueOf(idSeq++);
		String suffix = '0'.repeat(15 - cnt.length()) + cnt;
		return objType.getDescribe().getKeyPrefix() + suffix;
	}
}