/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for ExpVendorRepository
 */
@isTest
private class ExpVendorResourceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);

	/**
	 * Create a new Accounting Period record(Positive case)
	 */
	@isTest
	static void createApiTest() {

		ExpVendorResource.UpsertParam param = new ExpVendorResource.UpsertParam();
		setTestParam(param);

		Test.startTest();

		ExpVendorResource.createApi api = new ExpVendorResource.createApi();
		ExpVendorResource.SaveResult result = (ExpVendorResource.SaveResult)api.execute(param);

		Test.stopTest();

		List<ExpVendor__c> records = getVendorObjectById(result.id);

		//Confirm new record was created
		System.assertEquals(1, records.size());
		assertFields(param, records[0]);
	}

	/**
   * Create a new Accounting Period record(Negative case: Mandatory params are not set)
   */
	@isTest
	static void createApiParamErrTest() {

		ExpVendorResource.UpsertParam param = new ExpVendorResource.UpsertParam();
		setTestParam(param);

		ExpVendorResource.CreateApi api = new ExpVendorResource.CreateApi();
		App.ParameterException ex1;
		App.ParameterException ex2;
		App.ParameterException ex3;
		App.ParameterException ex4;

		Test.startTest();

		// Set code blank
		setTestParam(param);
		param.code = '';
		try {
			// Parameter error should occur
			api.execute(param);
		} catch (App.ParameterException e1) {
			ex1 = e1;
		}

		// Set name blank
		setTestParam(param);
		param.name_L0 = '';
		try {
			// Parameter error should occur
			api.execute(param);
		} catch (App.ParameterException e2) {
			ex2 = e2;
		}

		// Set company id blank
		setTestParam(param);
		param.companyId = '';
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e3) {
			ex3 = e3;
		}

		// Set Payment Due Date Usage blank
		setTestParam(param);
		param.paymentDueDateUsage = '';
		try {
			// Parameter error should cause
			api.execute(param);
		} catch (App.ParameterException e4) {
			ex4 = e4;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex1.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Com_Lbl_Code}), ex1.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex2.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{MESSAGE.Admin_Lbl_Name}), ex2.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex3.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'Company ID'}), ex3.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex4.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_InvalidParameter(
			new List<String>{'paymentDueDateUsage', param.paymentDueDateUsage}), ex4.getMessage());
	}

	/**
	* Update a Accounting Period record(Positive case)
	*/
	@isTest
	static void updateApiTest() {

		// Create a Accounting Period record first
		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		ExpVendorResource.UpsertParam param = new ExpVendorResource.UpsertParam();
		setTestParam(param);
		param.id = testRecord.Id;

		Test.startTest();

		ExpVendorResource.UpdateApi api = new ExpVendorResource.UpdateApi();
		api.execute(param);

		Test.stopTest();

		List<ExpVendor__c> records = getVendorObjectById(testRecord.Id);

		//Confirm the record was updated
		System.assertEquals(1, records.size());
		assertFields(param, records[0]);
	}

	/**
   * Update a Accounting Period record(Negative case: Mandatory param is not set)
   */
	@isTest
	static void updateApiParamErrTest() {

		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		ExpVendorResource.UpsertParam param = new ExpVendorResource.UpsertParam();
		setTestParam(param);
		// Do not set ID

		ExpVendorResource.UpdateApi api = new ExpVendorResource.UpdateApi();
		Test.startTest();

		// Id isn't specified
		App.ParameterException idNullException;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			idNullException = e;
		}

		// Payment Due Date isn't specified
		App.ParameterException paymentDueDateNullException;
		param.id = testRecord.Id;
		param.paymentDueDateUsage = null;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			paymentDueDateNullException = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, idNullException.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), idNullException.getMessage());

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, paymentDueDateNullException.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_InvalidParameter(
			new List<String>{'paymentDueDateUsage', param.paymentDueDateUsage}), paymentDueDateNullException.getMessage());
	}

	/**
   * Delete a Vendor record(Positive case)
   */
	@isTest
	static void deleteApiTest() {

		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		ExpVendorResource.DeleteParam param = new ExpVendorResource.DeleteParam();
		param.id = testRecord.Id;

		Test.startTest();

		ExpVendorResource.DeleteApi api = new ExpVendorResource.DeleteApi();
		api.execute(param);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ExpVendor__c WHERE Id =: param.id]);
	}

	/**
	 * Delete a Vendor record(Negative case)
	 */
	@isTest
	static void deleteApiParamErrTest() {

		ExpVendorResource.DeleteParam param = new ExpVendorResource.DeleteParam();

		Test.startTest();

		ExpVendorResource.DeleteApi api = new ExpVendorResource.DeleteApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_NullValue(new List<String>{'ID'}), ex.getMessage());
	}

	/**
	 * Search a Vendor record by ID(Positive case)
	 */
	@isTest
	static void searchApiTest() {

		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		Test.startTest();

		ExpVendorResource.SearchParam param = new ExpVendorResource.SearchParam();
		param.companyId = testRecord.CompanyId__c;

		ExpVendorResource.SearchApi api = new ExpVendorResource.SearchApi();
		ExpVendorResource.SearchResult res = (ExpVendorResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());

		ExpVendorResource.ExpVendorRecord result = res.records[0];
		System.assertEquals(testRecord.Id, result.id);
	}

	/**
	 * Search a Vendor record(Positive case: test for L1 name(multi lang))
	 */
	@isTest
	static void searchApiGetNameL1Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		Test.startTest();

		ExpVendorResource.SearchParam param = new ExpVendorResource.SearchParam();
		param.companyId = testRecord.CompanyId__c;

		ExpVendorResource.SearchApi api = new ExpVendorResource.SearchApi();
		ExpVendorResource.SearchResult res = (ExpVendorResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(testRecord.Name_L1__c, res.records[0].name);
	}

	/**
	 * Search a Vendor record(Positive case: test for L2 name(multi lang))
	 */
	@isTest
	static void searchApiGetNameL2Test() {

		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ExpVendor__c testRecord = ComTestDataUtility.createExpVendor('Test', companyObj.Id);

		Test.startTest();

		ExpVendorResource.SearchParam param = new ExpVendorResource.SearchParam();
		param.companyId = testRecord.CompanyId__c;

		ExpVendorResource.SearchApi api = new ExpVendorResource.SearchApi();
		ExpVendorResource.SearchResult res = (ExpVendorResource.SearchResult)api.execute(param);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		System.assertEquals(testRecord.Id, res.records[0].id);
		// Confirm multi lang name
		System.assertEquals(testRecord.Name_L2__c, res.records[0].name);
	}

	/**
	 * Set test parameter tp request parameter
	 * @param param Target object to set data
	 */
	static void setTestParam(ExpVendorResource.UpsertParam param) {
		param.active = true;
		param.address = 'address';
		param.bankAccountType = 'Savings';
		param.bankAccountNumber = 'bankAccountNumber';
		param.bankCode = 'bankCd';
		param.bankName = 'bankName';
		param.branchAddress = 'branchAddress';
		param.branchCode = 'XYZ';
		param.branchName = 'branchName';
		param.code = 'code';
		param.correspondentBankAddress = 'correspondentBankAddress';
		param.correspondentBankName = 'correspondentBankName';
		param.correspondentBranchName = 'correspondentBranchName';
		param.correspondentSwiftCode = 'cSwiftCode';
		param.companyId = companyObj.Id;
		param.country = 'JP';
		param.currencyCode = 'JPY';
		param.isWithholdingTax = true;
		param.name_L0 = 'nameL0';
		param.name_L0 = 'nameL0';
		param.name_L1 = 'nameL1';
		param.name_L2 = 'nameL2';
		param.payeeName = 'payeeName';
		param.paymentDueDateUsage = ExpVendorPaymentDueDateUsage.OPTIONAL.value;
		param.paymentTerm = 'paymentTerm';
		param.paymentTermCode = 'paymentTermCode';
		param.swiftCode = 'swiftCode';
		param.zipCode = 'zipCode';
	}

	static void assertFields(ExpVendorResource.UpsertParam param, ExpVendor__c sObj) {
		System.assertEquals(param.active, sObj.Active__c);
		System.assertEquals(param.address, sObj.Address__c);
		System.assertEquals(param.bankAccountType, sObj.BankAccountType__c);
		System.assertEquals(param.bankAccountNumber, sObj.BankAccountNumber__c);
		System.assertEquals(param.bankCode, sObj.BankCode__c);
		System.assertEquals(param.bankName, sObj.BankName__c);
		System.assertEquals(param.branchAddress, sObj.BranchAddress__c);
		System.assertEquals(param.branchCode, sObj.BranchCode__c);
		System.assertEquals(param.branchName, sObj.BranchName__c);
		System.assertEquals(param.code, sObj.Code__c);
		System.assertEquals(param.companyId, sObj.CompanyId__c);
		System.assertEquals(param.country, sObj.Country__c);
		System.assertEquals(param.currencyCode, sObj.CurrencyCode__c);
		System.assertEquals(param.correspondentBankAddress, sObj.CorrespondentBankAddress__c);
		System.assertEquals(param.correspondentBankName, sObj.CorrespondentBankName__c);
		System.assertEquals(param.correspondentBranchName, sObj.CorrespondentBranchName__c);
		System.assertEquals(param.correspondentSwiftCode, sObj.CorrespondentSwiftCode__c);
		System.assertEquals(param.isWithholdingTax, sObj.IsWithholdingTax__c);
		System.assertEquals(param.name_L0, sObj.Name_L0__c);
		System.assertEquals(param.name_L1, sObj.Name_L1__c);
		System.assertEquals(param.name_L2, sObj.Name_L2__c);
		System.assertEquals(param.payeeName, sObj.PayeeName__c);
		System.assertEquals(param.paymentDueDateUsage, sObj.PaymentDueDateUsage__c);
		System.assertEquals(param.paymentTerm, sObj.PaymentTerm__c);
		System.assertEquals(param.paymentTermCode, sObj.PaymentTermCode__c);
		System.assertEquals(param.swiftCode, sObj.SwiftCode__c);
		System.assertEquals(param.zipCode, sObj.ZipCode__c);
	}

	/*
	 * Get Vendors Object by ID
	 * @param vendorId ID of Vendor Object
	 * @return List of ExpVendor__c sObject
	 */
	private static List<ExpVendor__c> getVendorObjectById(Id vendorId) {
		List<ExpVendor__c> records = [SELECT
				Id,
				Active__c,
				Address__c,
				BankAccountType__c,
				BankAccountNumber__c,
				BankCode__c,
				BankName__c,
				BranchAddress__c,
				BranchCode__c,
				BranchName__c,
				Code__c,
				CompanyId__c,
				Country__c,
				CorrespondentBankAddress__c,
				CorrespondentBankName__c,
				CorrespondentBranchName__c,
				CorrespondentSwiftCode__c,
				CurrencyCode__c,
				IsWithholdingTax__c,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				PayeeName__c,
				PaymentDueDateUsage__c,
				PaymentTerm__c,
				PaymentTermCode__c,
				SwiftCode__c,
				UniqKey__c,
				ZipCode__c
			FROM ExpVendor__c
			WHERE Id =: vendorId
		];
		return records;
	}
}
