/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費精算 Expense
 *
 * @description ExpTaxTypeHistoryResourceのテストクラス
 */
@isTest
private class ExpTaxTypeHistoryResourceTest {

	/**
	 * 税区分履歴レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c base = ComTestDataUtility.createTaxTypeWithHistory('税区分ベース', null, company.Id);

		// 履歴の最新日を今日にする
		List<ExpTaxTypeHistory__c> testHistoryList = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c =:base.Id];
		Date today = System.today();
		for (Integer i = 0, n = testHistoryList.size(); i < n; i++) {
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		Test.startTest();

		ExpTaxTypeHistoryResource.ExpTaxTypeHistory param = new ExpTaxTypeHistoryResource.ExpTaxTypeHistory();
		param.baseId = base.Id;
		param.name_L0 = 'テスト税区分_L0';
		param.name_L1 = 'テスト税区分_L1';
		param.name_L2 = 'テスト税区分_L2';
		param.rate = 8.0;
		param.comment = 'Testコメント';
		param.validDateFrom = today + 1;
		param.validDateTo = today + 31;

		ExpTaxTypeHistoryResource.CreateApi api = new ExpTaxTypeHistoryResource.CreateApi();
		ExpTaxTypeHistoryResource.SaveResult res = (ExpTaxTypeHistoryResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 税区分ベース・履歴レコードが1件ずつ作成されること
		List<ExpTaxTypeBase__c> newBaseRecords = [
			SELECT
				Id,
				Name,
				Code__c,
				CountryId__c,
				CompanyId__c,
				CurrentHistoryId__c,
				(SELECT
						Id,
						BaseId__c,
						UniqKey__c,
						Name,
						Name_L0__c,
						Name_L1__c,
						Name_L2__c,
						Rate__c,
						HistoryComment__c,
						ValidFrom__c,
						ValidTo__c
					FROM ExpTaxTypeHistories__r
					WHERE Id = :res.Id)
			FROM ExpTaxTypeBase__c];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].ExpTaxTypeHistories__r.size());

		// 値の検証
		ExpTaxTypeBase__c newBase = newBaseRecords[0];
		ExpTaxTypeHistory__c newHistory = newBase.ExpTaxTypeHistories__r[0];

		// レスポンス値の検証
		System.assertEquals(newHistory.Id, res.Id);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = newBase.Code__c +
			'-' + DateTime.newInstance(param.validDateFrom, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd') +
			'-' + DateTime.newInstance(param.validDateTo, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd');
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.name_L0, newHistory.Name);
		System.assertEquals(param.name_L0, newHistory.Name_L0__c);
		System.assertEquals(param.name_L1, newHistory.Name_L1__c);
		System.assertEquals(param.name_L2, newHistory.Name_L2__c);
		System.assertEquals(param.rate, newHistory.Rate__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
	}

	/**
	 * 税区分履歴レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createPermissionErrorTest() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		ExpTaxTypeBase__c base = ComTestDataUtility.createTaxTypeWithHistory('税区分ベース', null, testData.company.Id);
		final Date today = System.today();

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageExpTaxType = false;
		new PermissionRepository().saveEntity(testData.permission);

		ExpTaxTypeHistoryResource.ExpTaxTypeHistory param = new ExpTaxTypeHistoryResource.ExpTaxTypeHistory();
		param.baseId = base.Id;
		param.name_L0 = 'テスト税区分_L0';
		param.name_L1 = 'テスト税区分_L1';
		param.name_L2 = 'テスト税区分_L2';
		param.rate = 8.0;
		param.comment = 'Testコメント';
		param.validDateFrom = today + 1;
		param.validDateTo = today + 31;

		App.NoPermissionException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				ExpTaxTypeHistoryResource.CreateApi api = new ExpTaxTypeHistoryResource.CreateApi();
				ExpTaxTypeHistoryResource.SaveResult res = (ExpTaxTypeHistoryResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.runAs(runAsUser) {
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 税区分履歴レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeRequiredErrorTest() {
		ExpTaxTypeHistoryResource.ExpTaxTypeHistory param = new ExpTaxTypeHistoryResource.ExpTaxTypeHistory();
		ExpTaxTypeHistoryResource.CreateApi api = new ExpTaxTypeHistoryResource.CreateApi();
		App.ParameterException ex;

		Test.startTest();

		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 税区分履歴レコードを1件作成する(異常系:有効開始日が不正)
	 */
	@isTest
	static void creativeInvalidValidDateErrorTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c base = ComTestDataUtility.createTaxTypeWithHistory('税区分ベース', null, company.Id);

		// 履歴の最新日を今日にする
		List<ExpTaxTypeHistory__c> testHistoryList = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c = :base.Id];
		Date today = System.today();
		for (Integer i = 0, n = testHistoryList.size(); i < n; i++) {
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		Test.startTest();

		ExpTaxTypeHistoryResource.ExpTaxTypeHistory param = new ExpTaxTypeHistoryResource.ExpTaxTypeHistory();
		param.baseId = base.Id;
		param.name_L0 = 'テスト税区分_L0';
		param.name_L1 = 'テスト税区分_L1';
		param.name_L2 = 'テスト税区分_L2';
		param.rate = 8.0;
		// 最新履歴の有効開始日と同日の改定を可能にしたため、日付をtoday-1に変更
		param.validDateFrom = today - 1;

		ExpTaxTypeHistoryResource.CreateApi api = new ExpTaxTypeHistoryResource.CreateApi();

		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assert(ex != null);
		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Admin_Err_InvalidRevisionDate, ex.getMessage());
	}

	// TODO: 更新APIを実装したら併せて実装する Implement test code for UpdateApi when needed.

	/**
	 * 税区分履歴レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('Test税区分', null, company.Id);
		ExpTaxTypeHistory__c delRecord = [SELECT Name_L0__c, UniqKey__c, BaseId__c, ValidFrom__c, ValidTo__c, Rate__c FROM ExpTaxTypeHistory__c LIMIT 1];
		// ベースに対して履歴が2件以上存在する場合のみ削除が可能なため、レコードを追加する
		ExpTaxTypeHistory__c history2 = delRecord.clone();
		history2.UniqKey__c += '_2';
		insert history2;

		Test.startTest();

		ExpTaxTypeHistoryResource.DeleteOption param = new ExpTaxTypeHistoryResource.DeleteOption();
		param.id = delRecord.id;

		// １回目では削除が成功し、２回目ではスキップすること
		(new ExpTaxTypeHistoryResource.DeleteApi()).execute(param);
		(new ExpTaxTypeHistoryResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 部署レコードが論理削除されること
		List<ExpTaxTypeHistory__c> recordList = [SELECT Id, Removed__c FROM ExpTaxTypeHistory__c WHERE Id =:delRecord.Id];
		System.assertEquals(1, recordList.size());
		System.assertEquals(true, recordList[0].Removed__c);
	}

	/**
	 * 税区分履歴レコードを1件削除する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		ExpTaxTypeBase__c base = ComTestDataUtility.createTaxTypeWithHistory('税区分ベース', null, testData.company.Id);
		ExpTaxTypeHistory__c delRecord = [SELECT UniqKey__c, BaseId__c FROM ExpTaxTypeHistory__c LIMIT 1];

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageExpTaxType = false;
		new PermissionRepository().saveEntity(testData.permission);

		ExpTaxTypeHistoryResource.DeleteOption param = new ExpTaxTypeHistoryResource.DeleteOption();
		param.id = delRecord.id;

		App.NoPermissionException actEx;
		User runAsUser = testData.createRunAsUserFromEmployee(stdEmployee);
		System.runAs(runAsUser) {
			try {
				(new ExpTaxTypeHistoryResource.DeleteApi()).execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.runAs(runAsUser) {
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 税区分履歴レコードを1件削除する(異常系:ベースに対して既存履歴が1件のみ)
	 */
	@isTest
	static void deleteNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('Test税区分', null, company.Id);
		ExpTaxTypeHistory__c delRecord = [SELECT UniqKey__c, BaseId__c FROM ExpTaxTypeHistory__c LIMIT 1];

		Test.startTest();

		ExpTaxTypeHistoryResource.DeleteOption param = new ExpTaxTypeHistoryResource.DeleteOption();
		param.id = delRecord.id;

		// 削除エラーが発生し、履歴削除されないこと
		App.IllegalStateException ex;

		try {
			(new ExpTaxTypeHistoryResource.DeleteApi()).execute(param);
		} catch (App.IllegalStateException e) {
			ex = e;
		}

		Test.stopTest();

		System.assert(ex != null);
		System.assertEquals(1, [SELECT COUNT() FROM ExpTaxTypeHistory__c WHERE Id =:delRecord.Id]);
	}

	/**
	 * 税区分履歴レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		ExpTaxTypeHistoryResource.DeleteOption param = new ExpTaxTypeHistoryResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new ExpTaxTypeHistoryResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 税区分履歴レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<ExpTaxTypeBase__c> taxTypes = ComTestDataUtility.createTaxTypesWithHistory('テスト税区分', null, company.Id, 3);
		ExpTaxTypeHistory__c history = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c IN :taxTypes LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		ExpTaxTypeHistoryResource.SearchApi api = new ExpTaxTypeHistoryResource.SearchApi();
		ExpTaxTypeHistoryResource.SearchResult res = (ExpTaxTypeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 税区分レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 税区分履歴レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('テスト税区分', null, company.Id);
		ExpTaxTypeHistory__c history = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c = :taxType.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		ExpTaxTypeHistoryResource.SearchApi api = new ExpTaxTypeHistoryResource.SearchApi();
		ExpTaxTypeHistoryResource.SearchResult res = (ExpTaxTypeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 税区分レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [
				SELECT Name_L1__c
				FROM ExpTaxTypeHistory__c
				WHERE Id =:history.Id LIMIT 1];
		System.assertEquals(history.Name_L1__c, res.records[0].name);
	}

	/**
	 * 税区分レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ExpTaxTypeBase__c taxType = ComTestDataUtility.createTaxTypeWithHistory('テスト税区分', null, company.Id);
		ExpTaxTypeHistory__c history = [SELECT Id FROM ExpTaxTypeHistory__c WHERE BaseId__c = :taxType.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		ExpTaxTypeHistoryResource.SearchApi api = new ExpTaxTypeHistoryResource.SearchApi();
		ExpTaxTypeHistoryResource.SearchResult res = (ExpTaxTypeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 税区分レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [
				SELECT Name_L2__c
				FROM ExpTaxTypeHistory__c
				WHERE BaseId__c =:taxType.Id LIMIT 1];
		System.assertEquals(history.Name_L2__c, res.records[0].name);
	}

	/**
	 * 税区分レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(Userinfo.getLanguage(), ComTestDataUtility.getNotUserLang(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<ExpTaxTypeBase__c> taxTypes = ComTestDataUtility.createTaxTypesWithHistory('Test税区分', null, company.Id, 3);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>();
		ExpTaxTypeHistoryResource.SearchApi api = new ExpTaxTypeHistoryResource.SearchApi();
		ExpTaxTypeHistoryResource.SearchResult res = (ExpTaxTypeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		List<ExpTaxTypeHistory__c> historyList = [
			SELECT
				Id,
				BaseId__c,
				UniqKey__c,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Rate__c,
				ValidFrom__c,
				ValidTo__c,
				HistoryComment__c
			FROM ExpTaxTypeHistory__c];

		// 税区分レコードが取得できること
		System.assertEquals(historyList.size(), res.records.size());

		for (Integer i = 0, n = historyList.size(); i < n; i++) {
			ExpTaxTypeHistory__c history = historyList[i];
			ExpTaxTypeHistoryResource.ExpTaxTypeHistory dto = res.records[i];

			System.assertEquals(history.Id, dto.id);
			System.assertEquals(history.BaseId__c, dto.baseId);
			System.assertEquals(history.Name_L0__c, dto.name);	// 税区分名のデフォルトはL0を返す
			System.assertEquals(history.Name_L0__c, dto.name_L0);
			System.assertEquals(history.Name_L1__c, dto.name_L1);
			System.assertEquals(history.Name_L2__c, dto.name_L2);
			System.assertEquals(history.Rate__c, dto.rate);
			System.assertEquals(history.HistoryComment__c, dto.comment);
		}
	}
}