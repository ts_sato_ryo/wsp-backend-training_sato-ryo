/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 勤怠期間種別
 */
public with sharing class AttPeriodStatusType extends ValueObjectType {

	/** 休職休業 */
	public static final AttPeriodStatusType ABSENCE = new AttPeriodStatusType('Absence');
	/** 短時間勤務設定 */
	public static final AttPeriodStatusType SHORT_TIME_WORK_SETTING = new AttPeriodStatusType('ShortTimeWorkSetting');

	/** 勤怠期間種別のリスト（値が追加されら本リストにも追加してください) */
	public static final List<AttPeriodStatusType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttPeriodStatusType> {
			ABSENCE,
			SHORT_TIME_WORK_SETTING
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttPeriodStatusType(String value) {
		super(value);
	}

	/**
	 * 値からAttPeriodStatusTypeを取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttWorkSystem、存在しない場合はnull
	 */
	public static AttPeriodStatusType valueOf(String value) {
		AttPeriodStatusType retType = null;
		if (String.isNotBlank(value)) {
			for (AttPeriodStatusType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
					break;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttWorkSystem以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttPeriodStatusType) {
			// 値が同じであればtrue
			if (this.value == ((AttPeriodStatusType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}

}
