/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 勤怠
  *
  * 固定労働制の勤怠計算ロジック
  */
  public with sharing class AttCalcFixLogic extends AttCalcLogic {
	AttCalcOverTimeLogic calcOverTimeLogic = new AttCalcOverTimeLogic();
	public AttCalcFixLogic(iOutput output, iInput input) {
		super(output, input);
	}

	public override Date getStartDateWeekly(Date d) {
		Integer dayOfWeek = DateUtil.dayOfWeek(d);
		return d.addDays(
				input.getConfig(d).beginDayOfWeek > dayOfWeek
					? input.getConfig(d).beginDayOfWeek - dayOfWeek - 7
					: input.getConfig(d).beginDayOfWeek - dayOfWeek
		);
	}
	private class CalcAttendanceDailyInput extends AttCalcOverTimeLogic.Input {
		private Day day;
		private Config conf;
		public CalcAttendanceDailyInput(AttCalcLogic.iInput input, Day day) {
			this.day = day;
			this.conf = input.getConfig(day.getDate());
			this.inputStartTime = day.input.inputStartTime;
			this.inputEndTime = day.input.inputEndTime;
			this.inputRestRanges = day.input.inputRestRanges;
			this.inputRestTime = day.input.inputRestTime;
			this.startTime = day.input.startTime;
			this.endTime = day.input.endTime;
			this.contractWorkTime = day.input.contractWorkTime;
			this.permitedLostTimeRanges = day.input.permitedLostTimeRanges;
			this.convertdRestRanges = day.input.convertdRestRanges;
			this.convertedWorkTime = day.input.convertedWorkTime;
			this.unpaidLeaveRestRanges = day.input.unpaidLeaveRestRanges;
			this.unpaidLeaveTime = day.input.unpaidLeaveTime;
			this.allowedWorkRanges = day.input.allowedWorkRanges;
			this.contractRestRanges = day.input.contractRestRanges;
			this.contractRestTime = day.input.contractRestTime;
			this.legalWorkTime = conf.LegalWorkTimeDaily;
			this.isWorkDay = day.isWorkDay();
			this.isLegalHoliday = day.input.isLegalHoliday;
			this.isNoOffsetOvertimeByUndertime = conf.isNoOffsetOvertimeByUndertime;
			this.isPermitOvertimeWorkUntilNormalHours = conf.isPermitOvertimeWorkUntilNormalHours;
			this.isIncludeCompensationTimeToWorkHours = conf.isIncludeCompensationTimeToWorkHours;
		}

		public override String toString() {
			return day.toString();
		}
	}

	/**
	 * 日次勤怠計算処理(固定時間制)
	 */
	public override Boolean calcAttendanceDaily(Day day, Day dayNext) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcFixLogic.calcAttendanceDaily start');
		System.debug(LoggingLevel.DEBUG, '---day is ' + day.getDate());

		CalcAttendanceDailyInput tmpInput = new CalcAttendanceDailyInput(input, day);
		AttCalcOverTimeLogic.Output tmpOutput = new AttCalcOverTimeLogic.Output();
		// 残業計算と計算結果の記録
		calcOverTimeLogic.apply(tmpOutput, tmpInput);
		day.output.realWorkTime = tmpOutput.realWorkTime;
		day.output.convertedWorkTime = tmpOutput.convertedWorkTime;
		day.output.unpaidLeaveLostTime = tmpOutput.unpaidLeaveLostTime;
		day.output.paidLeaveTime = tmpOutput.paidLeaveTime;
		day.output.unpaidLeaveTime = tmpOutput.unpaidLeaveTime;
		day.output.lateArriveTime = tmpOutput.lateArriveTime;
		day.output.earlyLeaveTime = tmpOutput.earlyLeaveTime;
		day.output.breakTime = tmpOutput.breakTime;
		day.output.lateArriveLostTime = tmpOutput.lateArriveLostTime;
		day.output.earlyLeaveLostTime = tmpOutput.earlyLeaveLostTime;
		day.output.breakLostTime = tmpOutput.breakLostTime;
		day.output.authorizedLateArriveTime = tmpOutput.authorizedLateArriveTime;
		day.output.authorizedEarlyLeaveTime = tmpOutput.authorizedEarlyLeaveTime;
		day.output.authorizedBreakTime = tmpOutput.authorizedBreakTime;
		day.output.unknowBreakTime = tmpOutput.unknowBreakTime;
		system.debug(LoggingLevel.DEBUG, '---workTimeCategoryMap is ' + tmpOutput.workTimeCategoryMap);
		AttCalcRangesDto inContractInLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractInLegal);
		AttCalcRangesDto inContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.inContractOutLegal);
		AttCalcRangesDto outContractInLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.outContractInLegal);
		AttCalcRangesDto outContractOutLegal = getOrDefaultRanges(tmpOutput.workTimeCategoryMap, AttCalcOverTimeLogic.OverTimeCategory.outContractOutLegal);
		system.debug(LoggingLevel.DEBUG, '---inContractInLegal is ' + inContractInLegal);
		system.debug(LoggingLevel.DEBUG, '---inContractOutLegal is ' + inContractOutLegal);
		system.debug(LoggingLevel.DEBUG, '---outContractInLegal is ' + outContractInLegal);
		system.debug(LoggingLevel.DEBUG, '---outContractOutLegal is ' + outContractOutLegal);
		AttCalcRangesDto inContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto inContractOutLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractInLegal_nextDay = AttCalcRangesDto.RS();
		AttCalcRangesDto outContractOutLegal_nextDay = AttCalcRangesDto.RS();
		day.output.legalOutTime = inContractOutLegal.total() + outContractOutLegal.total();
		day.output.legalInTime = inContractInLegal.total() + outContractInLegal.total();

		if (!getCanCalcContinueNextDay(day, dayNext)) {
			System.debug(LoggingLevel.DEBUG, '---can not CalcContinue to NextDay');
			Integer boundaryTime = input.getConfig(day.getDate()).boundaryTimeOfDay;
			boundaryTime += 24 * 60; // 翌日かを判定するため、24時間後にする
			Integer legalWorkTime = tmpInput.legalWorkTime;
			if(day.isLegalHoliday() && ! dayNext.isLegalHoliday()) { // 法定休日の翌日の勤務時間の取扱
				System.debug(LoggingLevel.DEBUG, '---LegalHoliday to non-LegalHoliday');
				// 法定休日には所定外法定外しか発生しないはずなのでそれだけを扱う
				final AttCalcRangesDto workRangeNextDay = outContractOutLegal.cutAfter(boundaryTime);
				System.debug(LoggingLevel.DEBUG, '---workRangeNextDay is ' + workRangeNextDay);
				outContractInLegal_nextDay = calcOverTimeLogic.getWorkRangesByStartTime(legalWorkTime, workRangeNextDay, AttCalcRangesDto.RS(), AttCalcRangesDto.RS());
				outContractOutLegal_nextDay = calcOverTimeLogic.getWorkRangesByEndTime(legalWorkTime, workRangeNextDay, AttCalcRangesDto.RS(), AttCalcRangesDto.RS());
			}
			else {
				System.debug(LoggingLevel.DEBUG, '---other day');
				outContractInLegal_nextDay = outContractInLegal.cutAfter(boundaryTime);
				outContractOutLegal_nextDay = outContractOutLegal.cutAfter(boundaryTime);
				inContractInLegal_nextDay = inContractInLegal.cutAfter(boundaryTime);
				// 翌日が法定休日で、所定内法定休日が発生する場合、法定休日の所定内勤務にも基本給を払うの設定に従う
				if (dayNext.isLegalHoliday() && input.getConfig(day.getDate()).isPayBasicSalaryLegalHolidayContractedWorkHours) {
					outContractOutLegal_nextDay = outContractOutLegal_nextDay.insertAndMerge(inContractOutLegal.cutAfter(boundaryTime));
				}
				else {
					inContractOutLegal_nextDay = inContractOutLegal.cutAfter(boundaryTime);
				}
			}
			inContractInLegal = inContractInLegal.cutBefore(boundaryTime);
			outContractInLegal = outContractInLegal.cutBefore(boundaryTime);
			outContractOutLegal = outContractOutLegal.cutBefore(boundaryTime);
			// 当日が法定休日で、所定内法定休日が発生する場合、法定休日の所定内勤務にも基本給を払うの設定に従う
			if(day.isLegalHoliday() && input.getConfig(day.getDate()).isPayBasicSalaryLegalHolidayContractedWorkHours) {
				outContractOutLegal = outContractOutLegal.insertAndMerge(outContractOutLegal.cutBefore(boundaryTime));
				inContractOutLegal = AttCalcRangesDto.RS();
			}
			else {
				inContractOutLegal = inContractOutLegal.cutBefore(boundaryTime);
			}
		}
		Integer compensatoryDayTime = day.input.compensatoryDayTime;
		day.output.regularRateWorkTime = 0;
		day.output.legalInTime = 0;
		day.output.legalOutTime = 0;

		system.debug(LoggingLevel.DEBUG, '---inContractInLegal nextDay is ' + inContractInLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---inContractOutLegal nextDay is ' + inContractOutLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---outContractInLegal nextDay is ' + outContractInLegal_nextDay);
		system.debug(LoggingLevel.DEBUG, '---outContractOutLegal nextDay is ' + outContractOutLegal_nextDay);

		// 代休発生時間と相殺しながら勤務時間の出力を行う
		OutputWorkTimeParam param = new OutputWorkTimeParam(compensatoryDayTime, day.output.unknowBreakTime);
		outputWorkTime(day, day, outContractOutLegal, true, true, param);
		outputWorkTime(day, dayNext, outContractOutLegal_nextDay, true, true, param);
		outputWorkTime(day, day, inContractOutLegal, false, true, param);
		outputWorkTime(day, dayNext, inContractOutLegal_nextDay, false, true, param);
		outputWorkTime(day, day, outContractInLegal, true, false, param);
		outputWorkTime(day, dayNext, outContractInLegal_nextDay, true, false, param);
		// 所定内法定内処理前の入力休憩時間
		Integer inputRestTimeBeforeInContract = param.inputRestTime;
		outputWorkTime(day, day, inContractInLegal, false, false, param);
		outputWorkTime(day, dayNext, inContractInLegal_nextDay, false, false, param);
		day.output.breakTime += Math.max(0, day.output.unknowBreakTime - param.inputRestTime); // 未処理休憩時間 のうち取れたものを休憩時間に追加する
		// 所定内法定内処理に使われる入力休憩時間を控除する
		day.output.breakLostTime += Math.max(0, inputRestTimeBeforeInContract - param.inputRestTime);
		// 労基内労働時間から所定内使ったその他休憩時間を引く
		day.output.legalInTime -= (day.input.inputRestTime - day.output.unknowBreakTime);
		// 出退勤有無フラグ
		Boolean withStartEndTime = (day.input.inputStartTime != null || day.input.inputEndTime != null);
		day.output.realWorkDay = (withStartEndTime ? 1 : 0);
		day.output.legalHolidayWorkCount = (day.isLegalHoliday() && withStartEndTime ? 1 : 0);
		day.output.holidayWorkCount = (day.isHoliday() && withStartEndTime ? 1 : 0);
		// 勤務時間内訳(翌日分とマージする)
		day.output.inContractInLegalRanges = inContractInLegal.insertAndMerge(inContractInLegal_nextDay);
		day.output.inContractOutLegalRanges = inContractOutLegal.insertAndMerge(inContractOutLegal_nextDay);
		day.output.outContractInLegalRanges = outContractInLegal.insertAndMerge(outContractInLegal_nextDay);
		day.output.outContractOutLegalRanges = outContractOutLegal.insertAndMerge(outContractOutLegal_nextDay);
		System.debug(LoggingLevel.DEBUG, '---day.output is ' + day.output);
		System.debug(LoggingLevel.DEBUG, '---AttCalcFixLogic.calcAttendanceDaily end');
		return true;
	}

	/**
	 * 週単位勤怠計算処理(固定時間制)
	 */
	public override List<Date> calcAttendanceWeekly(Date d) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcFixLogic.calcAttendanceWeekly start');

		Integer daysInPeriod = 7;
		List<Date> updateDateList = new List<Date>();
		Day[] dayList = new Day[daysInPeriod];
		for (Integer i = 0; i < daysInPeriod; i++) {
			dayList[i] = db.get(d.addDays(i));
		}

		Integer legalWorkTimeSum = 0;
		Integer noPaidAllowanceTimeSum = 0;

		for (Integer i = 0; i < daysInPeriod; i++) {
			if (dayList[i] == null) {
				continue;
			}
			System.debug(LoggingLevel.DEBUG, '---calc d is ' + dayList[i].getDate());
			Integer legalWorkTimeAWeek = input.getConfig(dayList[i].input.targetDate).legalWorkTimeAWeek;
			Integer legalInTimeTotalUntilPreDay = legalWorkTimeSum;
			Integer regularRateWorkTimeTotalUntilPreDay = noPaidAllowanceTimeSum;
			legalWorkTimeSum += dayList[i].output.legalInTime;
			noPaidAllowanceTimeSum += dayList[i].output.regularRateWorkTime;
			System.debug(LoggingLevel.DEBUG, '---calc legalWorkTimeSum is ' + legalWorkTimeSum);
			System.debug(LoggingLevel.DEBUG, '---calc noPaidAllowanceTimeSum is ' + noPaidAllowanceTimeSum);
			Integer regularOverTime = 0;
			if (legalWorkTimeSum > legalWorkTimeAWeek)
			{
				regularOverTime = legalWorkTimeSum - Math.max(legalInTimeTotalUntilPreDay, legalWorkTimeAWeek);
			}
			Integer additionalAllowanceTime = 0;
			if (noPaidAllowanceTimeSum > legalWorkTimeAWeek) {
				additionalAllowanceTime = noPaidAllowanceTimeSum - Math.max(regularRateWorkTimeTotalUntilPreDay, legalWorkTimeAWeek);
			}
			// 変更があったかどうかのチェック
			if (dayList[i].output.regularOverTime != regularOverTime
					|| dayList[i].output.additionalAllowanceTime != additionalAllowanceTime
					|| dayList[i].output.noPaidAllowanceTimeSum != noPaidAllowanceTimeSum) {
				dayList[i].output.regularOverTime = regularOverTime;
				dayList[i].output.noPaidAllowanceTimeSum = noPaidAllowanceTimeSum;
				dayList[i].output.additionalAllowanceTime = additionalAllowanceTime;
				updateDateList.add(dayList[i].input.targetDate);
			}
			System.debug(LoggingLevel.DEBUG, '---calc regularOverTime is ' + dayList[i].output.regularOverTime);
			System.debug(LoggingLevel.DEBUG, '---calc noPaidAllowanceTimeSum is ' + dayList[i].output.noPaidAllowanceTimeSum);
			System.debug(LoggingLevel.DEBUG, '---calc additionalAllowanceTime is ' + dayList[i].output.additionalAllowanceTime);
		}
		System.debug(LoggingLevel.DEBUG, '---AttCalcFixLogic.calcAttendanceWeekly end');

		return updateDateList;
	}

	/**
	 * 月次勤怠計算処理(固定時間制)
	 */
	public override void calcAttendanceMonthly(Date d) {
		Date startDate = input.getStartDateMonthly(d);
		Date endDate = input.getEndDateMonthly(d);
		TotalOutput totalOutput = new TotalOutput();
		for (Date dt = startDate; dt <= endDate; dt = dt.addDays(1)) {
			Day day = db.get(dt);
			if (day == null) {
				continue;
			}
			totalOutput.realWorkTime += day.output.realWorkTime;
			totalOutput.convertedWorkTime += day.output.convertedWorkTime;
			totalOutput.unpaidLeaveLostTime += day.output.unpaidLeaveLostTime;
			totalOutput.paidLeaveTime += day.output.paidLeaveTime;
			totalOutput.unpaidLeaveTime += day.output.unpaidLeaveTime;
			totalOutput.lateArriveTime += day.output.lateArriveTime;
			totalOutput.earlyLeaveTime += day.output.earlyLeaveTime;
			totalOutput.breakTime += day.output.breakTime;
			totalOutput.lateArriveLostTime += day.output.lateArriveLostTime;
			totalOutput.earlyLeaveLostTime += day.output.earlyLeaveLostTime;
			totalOutput.breakLostTime += day.output.breakLostTime;
			totalOutput.authorizedLateArriveTime += day.output.authorizedLateArriveTime;
			totalOutput.authorizedEarlyLeaveTime += day.output.authorizedEarlyLeaveTime;
			totalOutput.authorizedBreakTime += day.output.authorizedBreakTime;
			totalOutput.paidBreakTime += day.output.paidBreakTime;
			totalOutput.authorizedOffTime += day.output.authorizedOffTime;
			totalOutput.legalOutTime += day.output.legalOutTime  + day.output.regularOverTime;
			totalOutput.legalInTime += day.output.legalInTime - day.output.regularOverTime;
			totalOutput.regularRateWorkTime += day.output.regularRateWorkTime;
			totalOutput.realWorkDay += day.output.RealWorkDay;
			totalOutput.workDay += day.output.workDay;
			totalOutput.legalHolidayWorkCount += day.output.legalHolidayWorkCount;
			totalOutput.holidayWorkCount += day.output.holidayWorkCount;
			for (WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
				Integer work = day.output.workTimeCategoryMap.get(c);
				Integer sum = totalOutput.workTimeCategoryMap.containsKey(c)
									? totalOutput.workTimeCategoryMap.get(c)
									: 0;
				totalOutput.workTimeCategoryMap.put(c,sum + work);
			}
			for (Integer j = 0; j < day.output.customWorkTimes.size(); j++)
				totalOutput.customWorkTimes[j] += day.output.customWorkTimes[j];
		}
		output.updateTotal(startDate, totalOutput);
	}
	/**
	 * カスタマイズ再計算
	 */
	public override void calcAttendanceCustom(Day day) {
		System.debug(LoggingLevel.DEBUG, '---AttCalcFixLogic.calcAttendanceCustom start');
		System.debug(LoggingLevel.DEBUG, '---day is ' + day.getDate());
		for(Integer i = 0; i < day.output.customWorkTimes.size(); i++) {
			day.output.customWorkTimes[i] = 0;
		}
		for(WorkTimeCategory c: day.output.workTimeCategoryMap.keySet()) {
			Integer rate = calcAllowanceWithoutNight(day, c);
			Integer work = day.output.workTimeCategoryMap.get(c);
			if (rate == 100) {
				day.output.customWorkTimes[0] += work;
			}
			else if (rate == 125) {
				day.output.customWorkTimes[1] += work;
			}
			else if (rate == 135) {
				day.output.customWorkTimes[2] += work;
			}
			else if (rate == 25) {
				day.output.customWorkTimes[3] += work;
			}
			else if (rate == 35) {
				day.output.customWorkTimes[4] += work;
			}
			if(c.isNight)
				day.output.customWorkTimes[5] += work;
			if(c.isCompensatory)
				day.output.customWorkTimes[6] += work;
		}
		System.debug(LoggingLevel.DEBUG, '---additionalAllowanceTime is ' + day.output.additionalAllowanceTime);
		// 所定内＋時間外割増の時間を、法定外に変換する(便利上)
		Integer allowanceOffsetedWeekly = Math.min(day.output.customWorkTimes[0], day.output.additionalAllowanceTime);
		day.output.customWorkTimes[0] -= allowanceOffsetedWeekly;
		day.output.customWorkTimes[1] += allowanceOffsetedWeekly;
		day.output.customWorkTimes[3] += day.output.additionalAllowanceTime - allowanceOffsetedWeekly;

		System.debug(LoggingLevel.DEBUG, '---AttCalcFixLogic.calcAttendanceCustom end');
	}
	/**
	 * 割増率の計算
	 */
	private Integer calcAllowance(Day day, WorkTimeCategory c) {
		return calcAllowance(day, c.dayType, c.isContractOut, c.isLegalOut, c.isNight, c.isCompensatory);
	}
	/**
	 * 割増率の計算(代休と深夜は除く)
	 */
	private Integer calcAllowanceWithoutNight(Day day, WorkTimeCategory c) {
		return calcAllowance(day, c.dayType, c.isContractOut, c.isLegalOut, false, false);
	}
	/**
	 * 割増率の計算
	 */
	private Integer calcAllowance(Day day, AttCalcAutoHolidayLogic.DayType dayType, boolean isContractOut, boolean isLegalOut, boolean isNight, boolean isCompensatory) {
		Integer up = 0;
		if (isContractOut) {
			up += 100;
		}
		if (dayType == AttCalcAutoHolidayLogic.DayType.LegalHoliday) {
			up += 35;
		} else {
			if (isLegalOut) {
				up += 25;
			} else if (isContractOut) {
				if (input.getConfig(day.getDate()).isPayAllowanceWhenOverContructedWorkHours) {
					up += 25;
				} else if (dayType == AttCalcAutoHolidayLogic.DayType.Holiday
						&& input.getConfig(day.getDate()).isPayAllowanceOnEveryHoliday) {
					up += 25;
				}
			}
		}
		if (isNight) {
			up += 25;
		}
		if (isCompensatory) {
			up -= 100;
		}
		return up;
	}
	/**
	 * 労働時間出力引数
	 */
	private class OutputWorkTimeParam {
		OutputWorkTimeParam(Integer compensatorTime, Integer inputRestTime) {
			this.compensatoryTime = compensatoryTime == null ? 0 : compensatoryTime;
			this.inputRestTime = inputRestTime == null ? 0 : inputRestTime;
		}
		public Integer compensatoryTime; // 代休発生時間
		public Integer inputRestTime; // 取得休憩時間
	}
	/**
	 * 労働時間出力
	 */
	private void outputWorkTime(Day outputDay, Day adjustmentDay, AttCalcRangesDto workTimeRanges, boolean isContractOut, boolean isLegalOut, OutputWorkTimeParam param) {
		if (workTimeRanges.total() > 0) {
			// ------------------
			// 日中の勤務時間を計算する
			// ------------------
			Integer dayTime = workTimeRanges.include(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			Integer dayRestTime = Math.min(param.inputRestTime, dayTime); // 日中取得休憩
			param.inputRestTime -= dayRestTime;
			dayTime -= dayRestTime;
			// 所定外で代休発生時間がある場合は、代休ありの勤務時間を計算する
			Integer dayCompensatoryTime = isContractOut ? Math.min(dayTime, param.compensatoryTime) : 0;
			if (dayCompensatoryTime > 0) {
				outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, true), dayCompensatoryTime);
			}
			// 残りの時間を出力する
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), false, false), dayTime - dayCompensatoryTime);
			// 代休と相殺した時間を差し引く
			param.compensatoryTime -= dayCompensatoryTime;

			// ------------------
			// 夜間の勤務時間を計算する
			// ------------------
			Integer nightTime = workTimeRanges.exclude(input.getConfig(outputDay.getDate()).workTimePeriodDaily).total();
			Integer inputRestTimeNight = Math.min(param.inputRestTime, nightTime);
			param.inputRestTime -= inputRestTimeNight;
			nightTime -= inputRestTimeNight;
			// 所定外で代休発生時間がある場合は、代休ありの勤務時間を計算する
			Integer nightCompensatoryTime = isContractOut ? Math.min(nightTime, param.compensatoryTime) : 0;
			if (nightCompensatoryTime > 0) {
				outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, true), nightCompensatoryTime);
			}
			// 残りの時間を出力する
			outputDay.output.workTimeCategoryMap.put(new WorkTimeCategory(isLegalOut, isContractOut, adjustmentDay.getDayType(), true, false), nightTime - nightCompensatoryTime);
			// 代休と相殺した時間を差し引く
			param.compensatoryTime -= nightCompensatoryTime;

			if (!adjustmentDay.isLegalHoliday()) {
				// ------------------
				// 法定休日ではない日は週の法定基準オーバーの対象を計算するための勤務時間を計算する
				// ------------------
				if (isLegalOut) {
					// 法定外の労働は労基の基準外時間に計算
					outputDay.output.legalOutTime += (dayTime + nightTime); // 労基基準外時間
				} else {
					// 法定内の労働は労基の基準内時間に計算
					outputDay.output.legalInTime += (dayTime + nightTime); // 労基基準内時間

					// ------------------
					// さらに割増手当を払っていない時間を別途計算する
					// ------------------
					// 所定外法定内の場合
					if (isContractOut) {
						// 所定外で法定内の場合
						//  勤務日で"法定内残業も割増を払う"がOFFなら払っていない
						//  所定休日で"法定内残業も割増を払う"も"法定内残業も割増を払う"もOFFなら払っていない
						if (adjustmentDay.isWorkDay()) {
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) { // 法定内残業も割増を払
								outputDay.output.regularRateWorkTime += (dayTime + nightTime);
							}
						}
						else {
							if (!input.getConfig(outputDay.getDate()).isPayAllowanceOnEveryHoliday // 所定休日の勤務は法定内も割増を払う
									&& !input.getConfig(outputDay.getDate()).isPayAllowanceWhenOverContructedWorkHours) { // 法定内残業も割増を払う
								outputDay.output.regularRateWorkTime += (dayTime + nightTime);
							}
						}
					}
					// 所定内の時間は全部記録する
					else {
						outputDay.output.regularRateWorkTime += (dayTime + nightTime);
					}
				}
			}

		}
	}
	/**
	 * 空の初期値を返せる処理
	 */
	private AttCalcRangesDto getOrDefaultRanges(
			Map<AttCalcOverTimeLogic.OverTimeCategory,
			AttCalcRangesDto> rangesMap, AttCalcOverTimeLogic.OverTimeCategory key) {
		if (rangesMap.containsKey(key)) {
			return rangesMap.get(key);
		}
		return AttCalcRangesDto.RS();
	}
}