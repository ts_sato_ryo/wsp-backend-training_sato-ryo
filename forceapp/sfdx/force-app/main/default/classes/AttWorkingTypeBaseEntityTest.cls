/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 勤務体系ベースエンティティのテスト
 */
@isTest
private class AttWorkingTypeBaseEntityTest {

	/**
	 * テストデータ
	 */
	private class EntityTestData extends TestData.TestDataEntity {

	}


	/**
	 * エンティティの複製ができることを確認する
	 */
	@isTest static void copyTest() {
		EntityTestData testData = new EntityTestData();

		AttWorkingTypeBaseEntity copyEntity = testData.workingType.copy();
		System.assertEquals(testData.workingType.name, copyEntity.name);
		System.assertEquals(testData.workingType.code, copyEntity.code);
		System.assertEquals(testData.workingType.uniqKey, copyEntity.uniqKey);
		System.assertEquals(testData.workingType.companyId, copyEntity.companyId);
		System.assertEquals(testData.workingType.payrollPeriod, copyEntity.payrollPeriod);
		System.assertEquals(testData.workingType.startMonthOfYear, copyEntity.startMonthOfYear);
		System.assertEquals(testData.workingType.startDateOfMonth, copyEntity.startDateOfMonth);
		System.assertEquals(testData.workingType.startDateOfWeek, copyEntity.startDateOfWeek);
		System.assertEquals(testData.workingType.yearMark, copyEntity.yearMark);
		System.assertEquals(testData.workingType.monthMark, copyEntity.monthMark);
		System.assertEquals(testData.workingType.workSystem, copyEntity.workSystem);
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		AttWorkingTypeBaseEntity entity = new AttWorkingTypeBaseEntity();

		// 会社コードとジョブコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'MasterCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// 勤務体系コードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'MasterCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/**
	 * @description getMonthlyByDateのテスト
	 * 月度の表記、起算日に応じた月度が取得できることを確認する
	 */
	@isTest static void getMonthlyByDateTest() {
		AttWorkingTypeBaseEntity entity = new AttWorkingTypeBaseEntity();
		entity.payrollPeriod = AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month;
		AppDate targetDate;

		// TEST1 月度表記前、対象日が起算日の前の場合
		targetDate = AppDate.newInstance(2018, 12, 15);
		entity.startDateOfMonth = 16;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		System.assertEquals('201811', entity.getMonthlyByDate(targetDate));

		// TEST2 月度表記前、対象日が起算日と同日の場合
		targetDate = AppDate.newInstance(2018, 12, 16);
		entity.startDateOfMonth = 16;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		System.assertEquals('201812', entity.getMonthlyByDate(targetDate));

		// TEST3 月度表記前、対象日が起算日より後の場合
		targetDate = AppDate.newInstance(2018, 12, 17);
		entity.startDateOfMonth = 16;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		System.assertEquals('201812', entity.getMonthlyByDate(targetDate));

		// TEST4 月度表記後、対象日が起算日より前の場合
		targetDate = AppDate.newInstance(2018, 12, 15);
		entity.startDateOfMonth = 16;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		System.assertEquals('201812', entity.getMonthlyByDate(targetDate));

		// TEST5 月度表記後、対象日が起算日と同日の場合
		targetDate = AppDate.newInstance(2018, 12, 16);
		entity.startDateOfMonth = 16;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		System.assertEquals('201901', entity.getMonthlyByDate(targetDate));
	}

	/**
	 * @description getStartDateOfMonthlyのテスト
	 * 月度の表記、起算日に応じた月度が取得できることを確認する
	 */
	@isTest static void getStartDateOfMonthlyTest() {
		AttWorkingTypeBaseEntity entity = new AttWorkingTypeBaseEntity();
		entity.payrollPeriod = AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month;
		String targetYearMonthly;

		// TEST1 起算日が1日、月度表記前
		targetYearMonthly = '201901';
		entity.startDateOfMonth = 1;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		System.assertEquals(AppDate.newInstance(2019, 1, entity.startDateOfMonth), entity.getStartDateOfMonthly(targetYearMonthly));

		// TEST2 起算日が1日、月度表記後
		targetYearMonthly = '201901';
		entity.startDateOfMonth = 1;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		System.assertEquals(AppDate.newInstance(2019, 1, entity.startDateOfMonth), entity.getStartDateOfMonthly(targetYearMonthly));

		// TEST3 起算日が1日以外、月度表記前
		targetYearMonthly = '201901';
		entity.startDateOfMonth = 16;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		System.assertEquals(AppDate.newInstance(2019, 1, entity.startDateOfMonth), entity.getStartDateOfMonthly(targetYearMonthly));

		// TEST4 起算日が1日以外、月度表記後
		targetYearMonthly = '201901';
		entity.startDateOfMonth = 16;
		entity.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		System.assertEquals(AppDate.newInstance(2018, 12, entity.startDateOfMonth), entity.getStartDateOfMonthly(targetYearMonthly));
	}

	/**
	 * getyearlyRangeのテスト
	 * 指定された日付を入力した時、正しく年月度が取得できることを確認する
	 */
	@isTest static void getYearlyRangeTest() {
		AttWorkingTypeBaseEntity entity = new AttWorkingTypeBaseEntity();
		entity.startMonthOfYear = 4;
		entity.startDateOfMonth = 5;

		// Case 1: 年度の基準日が指定した日付よりも前にある場合
		AppDateRange caseBeforeTargetDate = entity.getYearlyRange(AppDate.newInstance(2019, 4, 10));
		System.assertEquals(AppDate.newInstance(2019, 4, 5), caseBeforeTargetDate.startDate);
		System.assertEquals(AppDate.newInstance(2020, 4, 4), caseBeforeTargetDate.endDate);

		// Case 2: 年度の基準日が指定した日付よりも後にある場合
		AppDateRange caseAfterTargetDate = entity.getYearlyRange(AppDate.newInstance(2019, 3, 31));
		System.assertEquals(AppDate.newInstance(2018, 4, 5), caseAfterTargetDate.startDate);
		System.assertEquals(AppDate.newInstance(2019, 4, 4), caseAfterTargetDate.endDate);
	}
}
