/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 勤務パターンマスタのサービスクラス
 */
public with sharing class AttPatternService implements Service {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * 勤務パターンマスタエンティティを検証する
	 */
	private void validatePattern(AttPatternEntity entity) {
		// 勤務パターンマスタエンティティの項目の値を検証する
		Validator.Result result = new AttConfigValidator.AttPatternValidator(entity).validate();
		if (!result.isSuccess()) {
			Validator.Error error = result.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}
	}

	/**
	 * 勤務パターンを保存する
	 */
	public Repository.SaveResult savePatternEntity(AttPatternEntity savedEntity) {
		AttPatternRepository2 patternRepo = new AttPatternRepository2();

		// 各項目をチェックする
		validatePattern(savedEntity);

		// コードの重複をチェックする
		AttPatternEntity duplicateCodeEntity = patternRepo.getPatternByCode(savedEntity.code, savedEntity.companyId);
		if ((duplicateCodeEntity != null) && (duplicateCodeEntity.id != savedEntity.id)) {
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		// ユニークキーを設定する
		CompanyEntity company = new CompanyRepository().getEntity(savedEntity.companyId);
		if (company == null) {
			// メッセージ：指定された会社が見つかりません
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
		}
		savedEntity.uniqKey = savedEntity.createUniqKey(company.code);

		// 保存する
		Repository.SaveResult result = patternRepo.saveEntity(savedEntity);
		return result;
	}

	/**
	 * 指定したidの勤務パターンを取得する
	 * @param patternId 取得対象の勤務パターンid
	 * @return 勤務パターン
	 */
	public AttPatternEntity getPatternById(Id patternId) {
		AttPatternRepository2 patternRepo = new AttPatternRepository2();
		return patternRepo.getPatternById(patternId);
	}
}
