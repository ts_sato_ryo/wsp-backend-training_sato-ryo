/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブタイプエンティティ
 */
public with sharing class JobTypeEntity extends LogicalDeleteEntity {

	/** コードの最大文字列長さ */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 名前の最大文字列長さ */
	public static final Integer NAME_MAX_LENGTH = 80;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		UNIQ_KEY,
		CODE,
		COMPANY_ID,
		NAME_L0,
		NAME_L1,
		NAME_L2
	}

	/** 値を変更した項目のリスト */
	private Set<Field> changedFieldSet;

	/** ジョブタイプ名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}
	/** ユニークキー */
	public String uniqKey {
		get;
		set {
			uniqKey = value;
			setChanged(Field.UNIQ_KEY);
		}
	}
	/** コード */
	public String code {
		get;
		set {
			code = value;
			setChanged(Field.CODE);
		}
	}
	/** 会社ID */
	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}
	/** ジョブタイプ名(翻訳) */
	public AppMultiString nameL {
		get {return new AppMultiString(nameL0, nameL1, nameL2);}
	}
	/** ジョブタイプ名(L0) */
	public String nameL0 {
		get;
		set {
			nameL0 = value;
			setChanged(Field.NAME_L0);
		}
	}
	/** ジョブタイプ名(L1) */
	public String nameL1 {
		get;
		set {
			nameL1 = value;
			setChanged(Field.NAME_L1);
		}
	}
	/** ジョブタイプ名(L2) */
	public String nameL2 {
		get;
		set {
			nameL2 = value;
			setChanged(Field.NAME_L2);
		}
	}

	/**
	 * コンストラクタ
	 */
	public JobTypeEntity() {
		changedFieldSet = new Set<Field>();
	}

	/**
	 * ユニークキーを作成する
	 * タグタイプ、タグコードを設定しておくこと。
	 * @param companyCode 会社コード
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]JobTypeのユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]JotTypeのユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + コード
		return companyCode + '-' + this.code;
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.changedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	/**
	 * (親クラスを含まない)クラス内の項目変更情報をリセットする
	 */
	protected override void resetChangedInSubClass() {
		this.changedFieldSet.clear();
	}
}