/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description ジョブ割当リポジトリのテスト
 */
 @isTest
private class JobAssignRepositoryTest {

	private static JobAssignRepository repository = new JobAssignRepository();

	/**
	 * 検索のテストに必要な共通データを作成するクラス
	 */
	private class SearchTestData {

		private ComEmpBase__c emp1;
		private ComEmpBase__c emp2;
		private ComJob__c job1;
		private ComJob__c job2;

		private SearchTestData() {
			// 社員
			ComCompany__c company = ComTestDataUtility.createTestCompany();
			User user1 = ComTestDataUtility.createUser('test1', 'ja', 'ja_JP');
			User user2 = ComTestDataUtility.createUser('test2', 'ja', 'ja_JP');
			ComPermission__c permission = ComTestDataUtility.createPermission('permission', company.id);
			emp1 = ComTestDataUtility.createEmployeeWithHistory('test1', company.Id, null, user1.Id, permission.id);
			emp1.Code__c = 'code1';
			upsert emp1;
			emp2 = ComTestDataUtility.createEmployeeWithHistory('test2', company.Id, null, user2.Id, permission.id);
			emp2.Code__c = 'code2';
			upsert emp2;

			// ジョブ
			ComJobType__c jobType = ComTestDataUtility.createJobType('JobType', company.Id);
			job1 = ComTestDataUtility.createJob('Job1', company.id, jobType.Id);
			job2 = ComTestDataUtility.createJob('Job2', company.id, jobType.Id);
		}
	}

	/**
	 * 検索したエンティティに正しく値が設定されていることを検証する
	 */
	@isTest
	static void searchEntityPropertyTest() {
		// テストデータ作成
		SearchTestData testData = new SearchTestData();
		ComJobAssign__c assign = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));

		// 検索
		JobAssignEntity entity = repository.searchEntity(new JobAssignRepository.SearchFilter())[0];
		System.assertEquals(assign.id, entity.id);
		System.assertEquals(testData.job1.id, entity.jobId);
		System.assertEquals(testData.emp1.id, entity.employeeBaseId);
		System.assertEquals(AppDate.newInstance(2018, 1, 1), entity.validFrom);
		System.assertEquals(AppDate.newInstance(2019, 1, 1), entity.validTo);
	}

	/**
	 * Idを条件に正しく検索出来ることを検証する
	 */
	@isTest
	static void searchByIdTest() {
		// テストデータ作成
		SearchTestData testData = new SearchTestData();
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));

		// 検索
		JobAssignEntity entity = repository.getEntity(assign2.id);
		System.assertEquals(assign2.id, entity.id);
	}

	/**
	 * ジョブIdを条件に正しく検索出来ることを検証する
	 */
	@isTest
	static void searchByJobIdTest() {
		// テストデータ作成
		SearchTestData testData = new SearchTestData();
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(testData.job2.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(testData.job2.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));

		// 検索
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		filter.jobIds.add(testData.job2.id);
		List<JobAssignEntity> entities = repository.searchEntity(filter);
		System.assertEquals(2, entities.size());
		for (JobAssignEntity entity : entities) {
			System.assertNotEquals(assign1.id, entity.id);
		}
	}

	/**
	 * 社員Idを条件に正しく検索出来ることを検証する
	 */
	@isTest
	static void searchByEmployeeIdTest() {
		// テストデータ作成
		SearchTestData testData = new SearchTestData();
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp2.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp2.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));

		// 検索
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		filter.employeeIds.add(testData.emp2.id);
		List<JobAssignEntity> entities = repository.searchEntity(filter);
		System.assertEquals(2, entities.size());
		for (JobAssignEntity entity : entities) {
			System.assertNotEquals(assign3.id, entity.id);
		}
	}

	/**
	 * 対象日を条件に正しく検索出来ることを検証する
	 */
	@isTest
	static void searchByTargetDateTest() {
		// テストデータ作成
		SearchTestData testData = new SearchTestData();
		Id jobId = testData.job1.id;
		Id empId = testData.emp1.id;
		AppDate minDate = ParentChildHistoryEntity.VALID_FROM_MIN;
		AppDate maxDate = ParentChildHistoryEntity.VALID_TO_MAX;
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 1, 1), maxDate);
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 1, 2), maxDate);
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 1, 3), maxDate); // 期間外
		ComJobAssign__c assign4 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 1, 1)); // 期間外
		ComJobAssign__c assign5 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 1, 2)); // 期間外（失効日と同一の場合は対象外）
		ComJobAssign__c assign6 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 1, 3));

		// 検索
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		filter.targetDate = AppDate.newInstance(2018, 1, 2);
		List<JobAssignEntity> entities = repository.searchEntity(filter);
		System.assertEquals(3, entities.size());
		for (JobAssignEntity entity : entities) {
			System.assertNotEquals(assign3.id, entity.id);
			System.assertNotEquals(assign4.id, entity.id);
			System.assertNotEquals(assign5.id, entity.id);
		}
	}

	/**
	 * 対象日の範囲に同日を指定した場合に、正しく検索出来ることを検証する
	 */
	@isTest
	static void searchByTargetRangeTest() {
		// テストデータ作成
		AppDate minDate = ParentChildHistoryEntity.VALID_FROM_MIN;
		AppDate maxDate = ParentChildHistoryEntity.VALID_TO_MAX;
		SearchTestData testData = new SearchTestData();
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), maxDate);
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 2), maxDate);
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 3), maxDate); // 期間外
		ComJobAssign__c assign4 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, minDate, AppDate.newInstance(2018, 1, 1)); // 期間外
		ComJobAssign__c assign5 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, minDate, AppDate.newInstance(2018, 1, 2)); // 期間外（失効日と同一の場合は対象外）
		ComJobAssign__c assign6 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, minDate, AppDate.newInstance(2018, 1, 3));

		// 検索
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		filter.dateRange = new AppDateRange(AppDate.newInstance(2018, 1, 2), AppDate.newInstance(2018, 1, 2));
		List<JobAssignEntity> entities = repository.searchEntity(filter);
		System.assertEquals(3, entities.size());
		for (JobAssignEntity entity : entities) {
			System.assertNotEquals(assign3.id, entity.id);
			System.assertNotEquals(assign4.id, entity.id);
			System.assertNotEquals(assign5.id, entity.id);
		}
	}

	/**
	 * 対象日の範囲を条件に正しく検索出来ることを検証する
	 */
	@isTest
	static void searchByDateRangeTest() {
		// テストデータ作成
		SearchTestData testData = new SearchTestData();
		Id jobId = testData.job1.id;
		Id empId = testData.emp1.id;
		AppDate minDate = ValidPeriodEntity.VALID_FROM_MIN;
		AppDate maxDate = ValidPeriodEntity.VALID_TO_MAX;
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2017, 12, 31)); // 対象外
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 1, 1));   // 対象外
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 1, 2));
		ComJobAssign__c assign4 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 1, 31));
		ComJobAssign__c assign5 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 2, 1));
		ComJobAssign__c assign6 = ComTestDataUtility.createJobAssign(jobId, empId, minDate, AppDate.newInstance(2018, 2, 2));
		ComJobAssign__c assign7 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2017, 12, 31), maxDate);
		ComJobAssign__c assign8 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 1, 1), maxDate);
		ComJobAssign__c assign9 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 1, 2), maxDate);
		ComJobAssign__c assign10 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 1, 31), maxDate);
		ComJobAssign__c assign11 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 2, 1), maxDate); // 対象外
		ComJobAssign__c assign12 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 2, 2), maxDate); // 対象外
		ComJobAssign__c assign13 = ComTestDataUtility.createJobAssign(jobId, empId, AppDate.newInstance(2018, 1, 10), AppDate.newInstance(2018, 1, 20));

		// 検索
		JobAssignRepository.SearchFilter filter = new JobAssignRepository.SearchFilter();
		filter.dateRange = new AppDateRange(AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2018, 1, 31));
		List<JobAssignEntity> entities = repository.searchEntity(filter);
		System.assertEquals(9, entities.size());
		for (JobAssignEntity entity : entities) {
			System.assertNotEquals(assign1.id, entity.id);
			System.assertNotEquals(assign2.id, entity.id);
			System.assertNotEquals(assign11.id, entity.id);
			System.assertNotEquals(assign12.id, entity.id);
		}
	}

	/**
	 * 社員コードの昇順、有効開始日の降順でレコードを検索していることを検証する
	 */
	@isTest
	static void searchSortOrderTest() {
		// テストデータ作成
		SearchTestData testData = new SearchTestData();
		ComJobAssign__c assign1 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 1), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign2 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp1.id, AppDate.newInstance(2018, 1, 2), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign3 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp2.id, AppDate.newInstance(2018, 1, 2), AppDate.newInstance(2019, 1, 1));
		ComJobAssign__c assign4 = ComTestDataUtility.createJobAssign(testData.job1.id, testData.emp2.id, AppDate.newInstance(2018, 1, 3), AppDate.newInstance(2019, 1, 1));

		// 検索
		List<JobAssignEntity> entities = repository.searchEntity(new JobAssignRepository.SearchFilter());
		System.assertEquals(4, entities.size());
		System.assertEquals(assign2.id, entities[0].id);
		System.assertEquals(assign1.id, entities[1].id);
		System.assertEquals(assign4.id, entities[2].id);
		System.assertEquals(assign3.id, entities[3].id);
	}

	/**
	 * エンティティを正しく登録できることを検証する
	 */
	@isTest
	static void createEntityTest() {
		// 社員作成
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComPermission__c permission = ComTestDataUtility.createPermission('permission', company.id);
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id, permission.id);

		// ジョブ作成
		ComJobType__c jobType = ComTestDataUtility.createJobType('JobType', company.Id);
		ComJob__c job = ComTestDataUtility.createJob('Job', company.id, jobType.Id);

		// エンティティ登録
		JobAssignEntity entity = new JobAssignEntity();
		entity.jobId = job.Id;
		entity.employeeBaseId = emp.Id;
		entity.validFrom = AppDate.newInstance(2018, 4, 1);
		entity.validTo = AppDate.newInstance(2019, 4, 1);
		entity.order = 1;
		Repository.SaveResult result = repository.saveEntity(entity);

		// 登録値検証
		List<ComJobAssign__c> sobjs = [SELECT Id, JobId__c, EmployeeBaseId__c, ValidFrom__c, ValidTo__c, Order__c FROM ComJobAssign__c];
		System.assertEquals(1, sobjs.size());
		System.assertEquals(result.details[0].id, sobjs[0].id);
		System.assertEquals(result.details[0].id, entity.id);
		System.assertEquals(job.id, sobjs[0].JobId__c);
		System.assertEquals(emp.id, sobjs[0].EmployeeBaseId__c);
		System.assertEquals(AppDate.newInstance(2018, 4, 1).getDate(), sobjs[0].ValidFrom__c);
		System.assertEquals(AppDate.newInstance(2019, 4, 1).getDate(), sobjs[0].ValidTo__c);
		System.assertEquals(1, sobjs[0].Order__c);
	}
}