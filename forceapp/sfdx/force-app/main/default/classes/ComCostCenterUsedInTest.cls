/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Test class for ComCostCenterUsedIn
 */
@isTest
private class ComCostCenterUsedInTest {
    
    /**
	 * Check if it compare properly
	 */
	@isTest static void equalsTest() {

		// Equal
		System.assertEquals(ComCostCenterUsedIn.EXPENSE_REPORT, ComCostCenterUsedIn.valueOf('ExpenseReport'));
		System.assertEquals(ComCostCenterUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ComCostCenterUsedIn.valueOf('ExpenseRequestAndExpenseReport'));

		// Values are different
		System.assertEquals(false, ComCostCenterUsedIn.EXPENSE_REPORT.equals(ComCostCenterUsedIn.valueOf('ExpenseRequestAndExpenseReport')));

		// Class type is different
		System.assertEquals(false, ComCostCenterUsedIn.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// Value is null
		System.assertEquals(false, ComCostCenterUsedIn.EXPENSE_REPORT.equals(null));
	}
}
