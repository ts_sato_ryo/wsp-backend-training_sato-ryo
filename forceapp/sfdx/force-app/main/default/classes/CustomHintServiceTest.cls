/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description Test class of Custom Hint Service
 */

@isTest
private class CustomHintServiceTest {
	static final ExpTestData EXP_TEST_DATA = new ExpTestData();
	/**
	 * Verify if custom hint is saved successfully
	 */
	@isTest static void saveCustomHintListSuccessfulTest() {
		// create custom hint
		CustomHintEntity entity = new CustomHintEntity();
		entity.companyId = EXP_TEST_DATA.company.id;
		entity.moduleType = CustomHintEntity.ModuleType.Expense;
		entity.fieldName = 'reportHeaderAccountingPeriod';
		entity.description_L0 = 'abc';
		entity.description_L1 = 'いいい';
		entity.description_L2 = '';
		
		Test.startTest();
		
		try {
			new CustomHintService().saveCustomHintList(new List<CustomHintEntity>{entity});
		} catch (Exception e) {
			System.assert(false);
		}

		Test.stopTest();

		// Check record
		ComCustomHint__c record = getRecordObject(entity.companyId, entity.moduleType.name(), entity.fieldName);
		System.assert(record != null);
		System.assertEquals(entity.description_L0, record.Description_L0__c);
		System.assertEquals(entity.description_L1, record.Description_L1__c);
		System.assertEquals(null, record.Description_L2__c);
	}
	
	/**
	 * Verify if no changes is saved when no description parameters are provided
	 */
	@isTest static void saveCustomHintListEmptyTest() {
		CustomHintService service = new CustomHintService();

		CustomHintEntity entity1 = new CustomHintEntity();
		entity1.companyId = EXP_TEST_DATA.company.id;
		entity1.moduleType = CustomHintEntity.ModuleType.Expense;
		entity1.fieldName = 'reportHeaderAccountingPeriod';
		entity1.description_L0 = 'abc';
		entity1.description_L1 = 'いいい';
		entity1.description_L2 = 'xxx';

		CustomHintEntity entity2 = new CustomHintEntity();
		entity2.companyId = EXP_TEST_DATA.company.id;
		entity2.moduleType = CustomHintEntity.ModuleType.Expense;
		
		Test.startTest();
		
		try {
			// save custom hints
			service.saveCustomHintList(new List<CustomHintEntity>{entity1});

			// save empty custom hint list
			service.saveCustomHintList(new List<CustomHintEntity>{});
		} catch (Exception e) {
			System.debug(e.getStackTraceString());
			System.assert(false);
		}

		Test.stopTest();

		// Check record
		ComCustomHint__c record = getRecordObject(entity1.companyId, entity1.moduleType.name(), entity1.fieldName);
		System.assert(record != null);
		System.assertEquals(entity1.description_L0, record.Description_L0__c);
		System.assertEquals(entity1.description_L1, record.Description_L1__c);
		System.assertEquals(entity1.description_L2, record.Description_L2__c);
	}

	/**
	 * Verify if App.ParameterException is thrown
	 * when description longer than 400 characters is provided
	 */
	@isTest static void saveCustomHintListNegativeTest() {
		CustomHintService service = new CustomHintService();

		CustomHintEntity entity = new CustomHintEntity();
		entity.companyId = EXP_TEST_DATA.company.id;
		entity.moduleType = CustomHintEntity.ModuleType.Expense;
		entity.fieldName = 'reportHeaderAccountingPeriod';
		entity.description_L0 = 'a'.repeat(401);
		
		Test.startTest();
		
		try {
			service.saveCustomHintList(new List<CustomHintEntity>{entity});
			System.assert(false);
		} catch (Exception e) {
			System.assert(e instanceof App.ParameterException);
		}

		Test.stopTest();
	}

	/**
	 * Verify if custom hint can be retrieved successfully
	 */
	@isTest static void getCustomHintListSuccessfulTest() {
		CustomHintService service = new CustomHintService();

		CustomHintEntity entity = new CustomHintEntity();
		entity.companyId = EXP_TEST_DATA.company.id;
		entity.moduleType = CustomHintEntity.ModuleType.Expense;
		entity.fieldName = 'reportHeaderAccountingPeriod';
		entity.description_L0 = 'abc';
		entity.description_L1 = 'いいい';
		entity.description_L2 = 'xxx';

		List<CustomHintEntity> entityList;
		
		Test.startTest();
		
		try {
			// save custom hints
			service.saveCustomHintList(new List<CustomHintEntity>{entity});
			
			// get custom hints
			entityList = service.getCustomHintList(entity.companyId, entity.moduleType.name());
		} catch (Exception e) {
			System.debug(e.getStackTraceString());
			System.assert(false);
		}

		Test.stopTest();

		System.assert(!entityList.isEmpty());

		// Check saved record
		System.assert(entityList[0] != null);
		System.assertEquals(entityList[0].companyId, entity.companyId);
		System.assertEquals(entityList[0].moduleType, entity.moduleType);
		System.assertEquals(entityList[0].fieldName, entity.fieldName);
		System.assertEquals(entityList[0].description_L0, entity.description_L0);
		System.assertEquals(entityList[0].description_L1, entity.description_L1);
		System.assertEquals(entityList[0].description_L2, entity.description_L2);
	}

	/**
	 * Verify if empty list of custom hints are returned when there is no custom hint configured yet
	 */
	@isTest static void getCustomHintListNegativeTest() {
		String companyId = EXP_TEST_DATA.company.id;
		String moduleType = CustomHintEntity.ModuleType.Expense.name();

		List<CustomHintEntity> entityList;
		
		Test.startTest();
		
		try {
			// get custom hints
			entityList = new CustomHintService().getCustomHintList(companyId, moduleType);
		} catch (Exception e) {
			System.debug(e.getStackTraceString());
			System.assert(false);
		}

		Test.stopTest();

		System.assert(entityList != null);
		System.assert(entityList.isEmpty());
	}

	static ComCustomHint__c getRecordObject(String companyId, String moduleType, String fieldName) {
		return [
			SELECT
				Description_L0__c,
				Description_L1__c,
				Description_L2__c
			FROM ComCustomHint__c
			WHERE CompanyId__c = :companyId
			AND ModuleType__c = :moduleType
			AND Field__c = :fieldName
			][0];
	}
}