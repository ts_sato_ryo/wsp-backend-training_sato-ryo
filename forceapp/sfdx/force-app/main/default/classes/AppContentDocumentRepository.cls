/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通 Common
 *
 * コンテンツドキュメントのリポジトリクラス AppContentDocument Repository class
 */
public with sharing class AppContentDocumentRepository extends Repository.BaseRepository {

	/** Viewer permission */
	@TestVisible
	private static final String LINK_SHARE_TYPE_VIEWER = 'V';

	/** 子リレーション名（コンテンツドキュメントリンク） Child Relation Name (ContentDocumentLink) */
	private static final String CHILD_RELATION_CONTENT_DOCUMENT_LINKS = 'ContentDocumentLinks';

	/** 取得対象の項目（コンテンツドキュメント） Field set (ContentDocument) */
	private static final Set<Schema.SObjectField> SELECT_CONTENT_DOCUMENT_FIELD_SET;
	static {
		SELECT_CONTENT_DOCUMENT_FIELD_SET = new Set<Schema.SObjectField> {
				ContentDocument.OwnerId,
				ContentDocument.Title,
				ContentDocument.FileType,
				ContentDocument.LatestPublishedVersionId,
				ContentDocument.ContentModifiedDate
		};
	}

	/** 取得対象の項目（コンテンツドキュメントリンク） Field set (ContentDocumentLink) */
	private static final Set<Schema.SObjectField> SELECT_CONTENT_DOCUMENT_LINK_FIELD_SET;
	static {
		SELECT_CONTENT_DOCUMENT_LINK_FIELD_SET = new Set<Schema.SObjectField> {
				ContentDocumentLink.ContentDocumentId,
				ContentDocumentLink.LinkedEntityId
		};
	}

	/**
	 * リンクを保存する Save link
	 * @param entity 保存対象のリンク AppContentLinkEntity to save
	 * @return 保存結果 Save result
	 */
	public SaveResult saveLink(AppContentDocumentLinkEntity entity) {
		return saveLinkList(new List<AppContentDocumentLinkEntity>{entity});
	}

	/** すべてのレコードを保存する(1レコードでも失敗したらロールバック) Save all records (if any record fails, rollback) */
	private static final Boolean IS_ALL_SAVE = true;
	/**
	 * リンクを保存する Save link
	 * @param entityList 保存対象のリンク List of AppContentLinkEntity to save
	 * @return 保存結果 Save result
	 */
	public SaveResult saveLinkList(List<AppContentDocumentLinkEntity> entityList) {
		List<ContentDocumentLink> saveObjList = new List<ContentDocumentLink>();

		// エンティティからSObjectを作成する Create sObject list from entity list
		for (AppContentDocumentLinkEntity entity : entityList) {
			saveObjList.add(createLinkObj(entity));
		}

		// DBに保存する Save in DB
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(saveObjList, IS_ALL_SAVE);

		// 結果作成 Return result
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * ドキュメントを取得する Retrieve List of AppContentDocumentEntity
	 * @param prefix ドキュメント名のプレフィックス Document name prefix
	 * @param ownerId 所有者ID User ID of owner
	 */
	public List<AppContentDocumentEntity> getEntityList(String prefix, Id ownerId) {
		SearchFilter filter = new SearchFilter();
		filter.ownerId = ownerId;
		filter.title = prefix;
		return searchEntityList(filter);
	}

	/**
	 * ドキュメントを取得する Retrieve List of AppContentDocumentEntity
	 * @param ids 対象IDのリスト List of ContentDocumentIds
	 */
	public List<AppContentDocumentEntity> getEntityList(List<Id> ids) {
		SearchFilter filter = new SearchFilter();
		filter.ids = ids;
		return searchEntityList(filter);
	}

	/**
	 * 検索条件 Search Filter
	 */
	private class SearchFilter {
		/** 所有者ID Owner ID */
		Id ownerId;
		/** タイトル（部分一致） Title (Partial Match) */
		String title;
		/** レコードID List of ContentDocument IDs */
		List<Id> ids;
	}

	/**
	 * ドキュメントを検索する Search ContentDocuments
	 * @param filter 検索条件 Search filter
	 * @return 条件に一致したドキュメント一覧 List of AppContentDocumentEntity that matches the criteria
	 */
	private List<AppContentDocumentEntity> searchEntityList(SearchFilter filter) {

		// SELECT対象項目リスト SELECT statement fields
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_CONTENT_DOCUMENT_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// コンテンツドキュメントリンクの項目 ContentDocumentLink fields
		List<String> linkFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_CONTENT_DOCUMENT_LINK_FIELD_SET) {
			linkFldList.add(getFieldName(fld));
		}

		selectFldList.add(
				'(SELECT ' + String.join(linkFldList, ',')
				+ ' FROM ' + CHILD_RELATION_CONTENT_DOCUMENT_LINKS + ')');

		// フィルタ条件からWHERE句の条件リストを作成する Create WHERE statement conditions from filter 
		List<String> condList = new List<String>();
		final Id pOwnerId = filter.ownerId;
		String pTitle = filter.title;
		final List<Id> pIds = filter.ids;
		if (pOwnerId != null) {
			condList.add(getFieldName(ContentDocument.OwnerId) + ' = :pOwnerId');
		}
		if (pTitle != null) {
			pTitle += '%';
			condList.add(getFieldName(ContentDocument.Title) + ' LIKE :pTitle');
		}
		if (pIds != null && !pIds.isEmpty()) {
			condList.add(getFieldName(ContentDocument.Id) + ' IN :pIds');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ Connect conditions with AND
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// SOQLを作成する Generate SOQL query statement
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ContentDocument.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		soql += ' ORDER BY ' + getFieldName(ContentDocument.ContentModifiedDate) + ' ASC';

		// クエリ実行 Execute query
		System.debug('AppContentDocumentRepository.searchEntityList: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する Create entity list from query result
		List<AppContentDocumentEntity> entityList = new List<AppContentDocumentEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ContentDocument)sobj));
		}

		return entityList;
	}

	/**
	 * ドキュメントリンクを取得する Retrieve List of AppContentDocumentLinkEntity
	 * @param linkedEntityIds 関連レコードのID List of LinkedEntity IDs
	 */
	public List<AppContentDocumentLinkEntity> getLinkEntityListByLinkedEntityId(List<Id> linkedEntityIds) {
		SearchLinkFilter filter = new SearchLinkFilter();
		filter.linkedEntityIds = linkedEntityIds;
		return searchLinkEntityList(filter);
	}

	/**
	 * 検索条件 Search Filter
	 */
	private class SearchLinkFilter {
		/** 関連レコードID List of LinkedEntity IDs */
		List<Id> linkedEntityIds;
	}

	/**
	 * ドキュメントリンクを検索する Search ContentDocumentLinks
	 * @param filter 検索条件 Search filter
	 * @return 条件に一致したリンク一覧 List of AppContentDocumentLinkEntity that matches the criteria
	 */
	private List<AppContentDocumentLinkEntity> searchLinkEntityList(SearchLinkFilter filter) {

		// SELECT対象項目リスト SELECT statement fields
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : SELECT_CONTENT_DOCUMENT_LINK_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// フィルタ条件からWHERE句の条件リストを作成する Create WHERE statement conditions from filter
		List<String> condList = new List<String>();
		final List<Id> pLinkedEntityIds = filter.linkedEntityIds;
		if (pLinkedEntityIds != null && !pLinkedEntityIds.isEmpty()) {
			condList.add(getFieldName(ContentDocumentLink.LinkedEntityId) + ' IN :pLinkedEntityIds');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ Connect conditions with AND
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// SOQLを作成する Generate SOQL query statement
		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ContentDocumentLink.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		soql += ' ORDER BY ' + getFieldName(ContentDocumentLink.Id) + ' ASC';

		// クエリ実行 Execute query
		System.debug('AppContentDocumentRepository.searchLinkEntityList: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する Create entity list from query result
		List<AppContentDocumentLinkEntity> entityList = new List<AppContentDocumentLinkEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createLinkEntity((ContentDocumentLink)sobj));
		}

		return entityList;
	}
	
	
	
	/**
	 * Retrieve Number of Links from ContentDocumentLink based on ContentDocument IDs
	 * @param contentDocumentIds List of ContentDocument IDs
	 */
	public Integer getNoOfLinksByContentDocumentId(List<Id> contentDocumentIds) {
		NoOfLinksFilter filter = new NoOfLinksFilter();
		filter.contentDocumentIds = contentDocumentIds;
		return getNoOfLinks(filter);
	}

	/**
	 * 検索条件 Search filter
	 */
	private class NoOfLinksFilter {
		/** List of ContentDocument IDs */
		List<Id> contentDocumentIds;
	}	
	
	/**
	 * Query ContentDocumentLinks
	 * @param filter 検索条件 Search filter
	 * @return Number of records that matches the criteria
	 */
	private Integer getNoOfLinks(NoOfLinksFilter filter) {

		// フィルタ条件からWHERE句の条件リストを作成する Create WHERE statement conditions from filter
		List<String> condList = new List<String>();
		final List<Id> pContentDocumentIds = filter.contentDocumentIds;
		if (pContentDocumentIds != null && !pContentDocumentIds.isEmpty()) {
			condList.add(getFieldName(ContentDocumentLink.ContentDocumentId) + ' IN :pContentDocumentIds');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ Connect conditions with AND
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		// SOQLを作成する Generate SOQL query statement
		String soql =
				'SELECT COUNT(ID) FROM ' + ContentDocumentLink.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}

		// クエリ実行 Execute query
		System.debug('AppContentDocumentRepository.getNoOfLinks: SOQL==>' + soql);
		List<AggregateResult> resultList = Database.query(soql);

		return (Integer)resultList[0].get('expr0');
	}
	
	

	/**
	 * ContentDocumentからAppContentDocumentEntityを作成する Create AppContentDocument Entity from ContentDocument sObject
	 * @param sObj 作成元のSObject ContentDocument sObject
	 * @return 作成したエンティティ AppContentDocument Entity
	 */
	private AppContentDocumentEntity createEntity(ContentDocument sObj) {
		AppContentDocumentEntity entity = new AppContentDocumentEntity();
		entity.setId(sObj.Id);
		entity.ownerId = sObj.OwnerId;
		entity.title = sObj.Title;
		entity.fileType = sObj.FileType;
		entity.latestPublishedVersionId = sObj.LatestPublishedVersionId;
		entity.contentModifiedDate = AppDatetime.valueOf(sObj.ContentModifiedDate);

		// コンテンツドキュメントリンク ContentDocumentLink
		List<ContentDocumentLink> linkObjList = sObj.ContentDocumentLinks;
		if (linkObjList != null) {
			List<AppContentDocumentLinkEntity> linkEntityList = new List<AppContentDocumentLinkEntity>();
			for (ContentDocumentLink linkObj : linkObjList) {
				// 既にレコードに紐づいている場合 If document is linked to a record
				if (linkObj.LinkedEntityId != sObj.OwnerId) {
					linkEntityList.add(createLinkEntity(linkObj));
				}
			}
			entity.replaceContentDocumentLinkList(linkEntityList);
		}

		entity.resetChanged();
		return entity;
	}

	/**
	 * ContentDocumentLinkからAppContentDocumentLinkEntityを作成する Create AppContentDocumentLink Entity from ContentDocumentLink sObject
	 * @param sObj 作成元のSObject ContentDocumentLink sObject
	 * @return 作成したエンティティ AppContentDocumentLink Entity
	 */
	private AppContentDocumentLinkEntity createLinkEntity(ContentDocumentLink sObj) {
		AppContentDocumentLinkEntity entity = new AppContentDocumentLinkEntity();
		entity.setId(sObj.Id);
		entity.contentDocumentId = sObj.ContentDocumentId;
		entity.linkedEntityId = sObj.LinkedEntityId;

		entity.resetChanged();
		return entity;
	}

	/**
	 * エンティティからSObjectを作成する Create ContentDocumentLink sObject from AppContentDocumentLink Entity
	 * @param entity 作成元のエンティティ AppContentDocumentLink Entity
	 * @return SObject ContentDocumentLink sObject
	 */
	private ContentDocumentLink createLinkObj(AppContentDocumentLinkEntity entity) {
		ContentDocumentLink sObj = new ContentDocumentLink();

		sObj.Id = entity.id;
		if (entity.isChanged(AppContentDocumentLinkEntity.Field.CONTENT_DOCUMENT_ID)) {
			sObj.ContentDocumentId = entity.contentDocumentId;
		}
		if (entity.isChanged(AppContentDocumentLinkEntity.Field.LINKED_ENTITY_ID)) {
			sObj.LinkedEntityId = entity.linkedEntityId;
		}
		sObj.ShareType = LINK_SHARE_TYPE_VIEWER;

		return sObj;
	}
}