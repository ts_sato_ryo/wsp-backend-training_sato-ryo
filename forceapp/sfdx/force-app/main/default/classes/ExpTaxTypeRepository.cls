/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分リポジトリ Repository class for ExpTaxTypeBase and ExpTaxTypeHistory object
 */
public with sharing class ExpTaxTypeRepository extends ParentChildRepository {

	/**
	 * 履歴リストを取得する際の並び順 Sort order of history list
	 */
	public enum HistorySortOrder {
		VALID_FROM_ASC, VALID_FROM_DESC
	}

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** 国のリレーション名 */
	private static final String COUNTRY_R = ExpTaxTypeBase__c.CountryId__c.getDescribe().getRelationshipName();
	/** 税区分ベースのリレーション名 Relationship name of ExpTaxTypeHistory__c.BaseId__c */
	private static final String HISTORY_BASE_R = ExpTaxTypeHistory__c.BaseId__c.getDescribe().getRelationshipName();

	/**
	 * 取得対象のベースオブジェクトの項目名 Fields of ExpTaxTypeBase object
	 * 項目が追加されたらbaseFieldSetに追加してください
	 * When add field to object, also need to add to baseFieldSet.
	 */
	private static final List<String> GET_BASE_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義 Field definision
		final Set<Schema.SObjectField> baseFieldList = new Set<Schema.SObjectField> {
			ExpTaxTypeBase__c.Id,
			ExpTaxTypeBase__c.Name,
			ExpTaxTypeBase__c.CurrentHistoryId__c,
			ExpTaxTypeBase__c.Code__c,
			ExpTaxTypeBase__c.UniqKey__c,
			ExpTaxTypeBase__c.CountryId__c,
			ExpTaxTypeBase__c.CompanyId__c};

		GET_BASE_FIELD_NAME_LIST = Repository.generateFieldNameList(baseFieldList);
	}

	/**
	 * 取得対象の履歴オブジェクトの項目名 Fields of ExpTaxTypeHistory object
	 * 項目が追加されたらhistoryFieldSetに追加してください
	 * When add field to object, also need to add to historyFieldSet.
	 */
	private static final List<String> GET_HISTORY_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義 Field definision
		final Set<Schema.SObjectField> historyFieldSet = new Set<Schema.SObjectField> {
			ExpTaxTypeHistory__c.Id,
			ExpTaxTypeHistory__c.BaseId__c,
			ExpTaxTypeHistory__c.UniqKey__c,
			ExpTaxTypeHistory__c.ValidFrom__c,
			ExpTaxTypeHistory__c.ValidTo__c,
			ExpTaxTypeHistory__c.HistoryComment__c,
			ExpTaxTypeHistory__c.Removed__c,
			ExpTaxTypeHistory__c.Name,
			ExpTaxTypeHistory__c.Name_L0__c,
			ExpTaxTypeHistory__c.Name_L1__c,
			ExpTaxTypeHistory__c.Name_L2__c,
			ExpTaxTypeHistory__c.Rate__c};

		GET_HISTORY_FIELD_NAME_LIST = Repository.generateFieldNameList(historyFieldSet);
	}

	/**
	 * 指定したエンティティを取得する(ベース+全ての履歴) Get entity of the specified ID
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティと、ベースに紐づく全ての履歴エンティティ
	 */
	public ExpTaxTypeBaseEntity getEntity(Id baseId) {
		return getEntity(baseId, null);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴) Get entity of the specified ID and date
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、存在しない場合はnull
	 */
	public ExpTaxTypeBaseEntity getEntity(Id baseId, AppDate targetDate) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};
		filter.targetDate = targetDate;

		List<ExpTaxTypeBaseEntity> entities = searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴) Get entity list
	 * @param baseIds 取得対象のベースIDのリスト、nullの場合は全てが取得対象となる
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定したベースIDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、履歴が存在しない場合はnull
	 */
	public List<ExpTaxTypeBaseEntity> getEntityList(List<Id> baseIds, AppDate targetDate) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = baseIds;
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したエンティティを取得する(ベース+履歴) Get entity list
	 * @param countryIds 取得対象の国IDのリスト、nullの場合は全てが取得対象となる
	 * @param targetDate 対象日。nullの場合は全履歴が取得対象となる
	 * @return 指定した国IDのエンティティと、対象日に有効な履歴エンティティのリスト。ただし、履歴が存在しない場合はnull
	 */
	public List<ExpTaxTypeBaseEntity> getEntityListByCountryId(List<Id> countryIds, AppDate targetDate) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.countryIds = countryIds;
		filter.targetDate = targetDate;

		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定した履歴IDの履歴エンティティとその親であるベースエンティティを取得する Get entity of the specified history ID
	 * @param historyId 履歴ID
	 * @return ベースと履歴エンティティ。ただし、対象の履歴IDが存在しない場合はnull
	 */
	public ExpTaxTypeBaseEntity getEntityByHistoryId(Id historyId) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new List<Id>{historyId};

		List<ExpTaxTypeBaseEntity> entities = searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する Get base entity of the specified ID
	 * @param baseId 取得対象のベースID
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public ExpTaxTypeBaseEntity getBaseEntity(Id baseId) {
		return getBaseEntity(baseId, NOT_FOR_UPDATE);
	}

	/**
	 * 指定したベースエンティティのみ取得する Get base entity of the specified ID
	 * @param baseId 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティ。履歴は含まない。該当するエンティティ存在しない場合はnull
	 */
	public ExpTaxTypeBaseEntity getBaseEntity(Id baseId, Boolean forUpdate) {
		List<ExpTaxTypeBaseEntity> bases = getBaseEntityList(new List<Id>{baseId}, forUpdate);
		if (bases.isEmpty()) {
			return null;
		}
		return bases[0];
	}

	/**
	 * 指定したベースエンティティのみ取得する Get base entity of the specified IDs
	 * @param baseIds 取得対象のベースID
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティのリスト。履歴は含まない
	 */
	public List<ExpTaxTypeBaseEntity> getBaseEntityList(List<Id> baseIds, Boolean forUpdate) {
		ExpTaxTypeRepository.SearchBaseFilter filter = new SearchBaseFilter();
		filter.baseIds = new List<Id>(baseIds);
		return searchBaseEntity(filter, forUpdate);
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ) Get history entity of the specified ID
	 * @param historyId 取得対象の履歴ID
	 * @return 履歴エンティティ。ただし、存在しない場合はnull
	 */
	public ExpTaxTypeHistoryEntity getHistoryEntity(Id historyId) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = new List<Id>{historyId};

		List<ExpTaxTypeHistoryEntity> entities = searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);

		if (entities.isEmpty()) {
			return null;
		}
		return entities[0];
	}

	/**
	 * 指定したエンティティを取得する(履歴エンティティのみ) Get history entity list
	 * @param historyIds 取得対象の履歴ID
	 * @return 履歴エンティティのリスト。
	 */
	public List<ExpTaxTypeHistoryEntity> getHistoryEntityList(List<Id> historyIds) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.historyIds = historyIds;

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースIDの履歴エンティティを取得する(履歴エンティティのみ) Get history entity list of the specified base ID
	 * @param baseId 取得対象のベースID
	 * @return 指定したベースIDのエンティティ
	 */
	public List<ExpTaxTypeHistoryEntity> getHistoryEntityByBaseId(Id baseId) {
		return getHistoryEntityByBaseId(baseId, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 指定したベースIDの履歴エンティティを取得する(履歴エンティティのみ)、履歴リストの取得順を指定可能 Get history entity list of the specified base ID
	 * @param baseId 取得対象のベースID
	 * @param sortOrder 履歴リストの並び順
	 * @return 指定したベースIDのエンティティ
	 */
	public List<ExpTaxTypeHistoryEntity> getHistoryEntityByBaseId(Id baseId, HistorySortOrder sortOrder) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, sortOrder);
	}

	/**
	 * 指定したベースIDの履歴エンティティを取得する(履歴エンティティのみ)、履歴リストの取得順を指定可能 Get history entity list of the specified base ID and date
	 * @param baseId 取得対象のベースID
	 * @param targetDate 対象日
	 * @return 指定したベースIDのエンティティ。有効開始日の昇順
	 */
	public List<ExpTaxTypeHistoryEntity> getHistoryEntityByBaseId(Id baseId, AppDate targetDate) {
		ExpTaxTypeRepository.SearchFilter filter = new SearchFilter();
		filter.baseIds = new List<Id>{baseId};
		filter.targetDate = targetDate;

		return searchHistoryEntity(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/**
	 * 検索する（ベース＋履歴） Search entity
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する。有効開始日の昇順
	 */
	public List<ExpTaxTypeBaseEntity> searchEntity(SearchFilter filter) {
		return searchEntityWithHistory(filter, NOT_FOR_UPDATE, HistorySortOrder.VALID_FROM_ASC);
	}

	/** 検索フィルタ(履歴の検索時に使用します) */
	public class SearchFilter {
		/** 税区分ベースID */
		public List<Id> baseIds;
		/** 税区分履歴ID */
		public List<Id> historyIds;
		/** 取得対象日 */
		public AppDate targetDate;
		/** Target period */
		public List<AppDate> targetPeriod;
		/** 国ID */
		public List<Id> countryIds;
		/** 会社ID */
		public List<Id> companyIds;
		/** コード(完全一致) */
		public List<String> codes;

		/** 論理削除されている履歴を取得対象にする場合はtrue */
		public Boolean includeRemoved;
	}

	/**
	 * 検索する（ベース＋履歴） Search entity
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストの並び順
	 * @return 検索結果のエンティティリスト。検索の結果、履歴を1件以上持つエンティティのみ返却する
	 */
	public List<ExpTaxTypeBaseEntity> searchEntityWithHistory(SearchFilter filter, Boolean forUpdate,
			ExpTaxTypeRepository.HistorySortOrder sortOrder) {

		// 履歴を検索
		List<ExpTaxTypeHistoryEntity> histories = searchHistoryEntity(filter, forUpdate, sortOrder);

		// 取得対象のベースIDリストを作成
		Set<Id> targetBaseIds = new Set<Id>();
		for (ExpTaxTypeHistoryEntity history : histories) {
			targetBaseIds.add(history.baseId);
		}

		// ベースエンティティを取得する
		SearchBaseFilter baseFilter = new SearchBaseFilter();
		baseFilter.baseIds = new List<Id>(targetBaseIds);
		List<ExpTaxTypeBaseEntity> bases = searchBaseEntity(baseFilter, forUpdate);

		// ベースに履歴を追加するためMapに変換する
		Map<Id, ExpTaxTypeBaseEntity> baseMap = new Map<Id, ExpTaxTypeBaseEntity>();
		for (ExpTaxTypeBaseEntity base : bases) {
			baseMap.put(base.id, base);
		}

		// ベースエンティティに履歴を追加する
		for (ExpTaxTypeHistoryEntity history : histories) {
			ExpTaxTypeBaseEntity base = baseMap.get(history.baseId);
			base.addHistory(history);
		}

		return bases;
	}

	/** ベース用の検索フィルタ */
	public class SearchBaseFilter {
		/** 税区分ベースID */
		public List<Id> baseIds;
	}

	/**
	 * ベースエンティティを検索する。履歴エンティティは取得しません。 Search base entity(without history entity)
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return ベースエンティティの検索結果
	 */
	private List<ExpTaxTypeBaseEntity> searchBaseEntity(SearchBaseFilter filter, Boolean forUpdate) {

		// ベース WHERE句
		List<String> baseWhereList = new List<String>();
		// ベースIDで検索
		List<Id> pIds;
		if (filter.baseIds != null) {
			pIds = filter.baseIds;
			baseWhereList.add('Id IN :pIds');
		}
		String whereString = buildWhereString(baseWhereList);

		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// ベース項目
		selectFieldList.addAll(GET_BASE_FIELD_NAME_LIST);
		// 関連項目（国）
		selectFieldList.add(getFieldName(COUNTRY_R, ComCountry__c.Name_L0__c));
		selectFieldList.add(getFieldName(COUNTRY_R, ComCountry__c.Name_L1__c));
		selectFieldList.add(getFieldName(COUNTRY_R, ComCountry__c.Name_L2__c));

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpTaxTypeBase__c.SObjectType.getDescribe().getName()
				+ whereString
				+ ' ORDER BY ' + getFieldName(ExpTaxTypeBase__c.Code__c);
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('ExpTaxTypeRepository: SOQL=' + soql);

		// クエリ実行
		List<ExpTaxTypeBase__c> sObjs = (List<ExpTaxTypeBase__c>)Database.query(soql);

		// SObjectからエンティティを作成
		List<ExpTaxTypeBaseEntity> entities = new List<ExpTaxTypeBaseEntity>();
		for (ExpTaxTypeBase__c sObj : sObjs) {
			entities.add(createBaseEntity(sObj));
		}

		return entities;
	}

	/**
	 * 履歴エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param sortOrder 履歴リストのソート順
	 * @return 履歴エンティティの検索
	 */
	public List<ExpTaxTypeHistoryEntity> searchHistoryEntity(SearchFilter filter, Boolean forUpdate, HistorySortOrder sortOrder) {
		// バインド変数が必要なため、クエリ作成処理をモジュール化していない

		// ベース SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 履歴項目
		selectFieldList.addAll(GET_HISTORY_FIELD_NAME_LIST);

		// WHERE句
		List<String> whereList = new List<String>();
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if(filter.targetDate != null){
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ExpTaxTypeHistory__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ExpTaxTypeHistory__c.ValidTo__c) + ' > :pTargetDate');
		}

		// Check record existing in the period
		Date pTargetPeriodFrom;
		Date pTargetPeriodTo;
		if (filter.targetPeriod != null) {
			pTargetPeriodFrom = filter.targetPeriod.get(0).getDate();
			pTargetPeriodTo = filter.targetPeriod.get(1).getDate();
			whereList.add(getFieldName(ExpTaxTypeHistory__c.ValidFrom__c) + ' < :pTargetPeriodTo');
			whereList.add(getFieldName(ExpTaxTypeHistory__c.ValidTo__c) + ' > :pTargetPeriodFrom');
		}

		// 履歴IDで検索
		List<Id> pHistoryIds;
		if(filter.historyIds != null){
			pHistoryIds = filter.historyIds;
			whereList.add(getFieldName(ExpTaxTypeHistory__c.Id) + ' IN :pHistoryIds');
		}
		// 論理削除されているレコードを検索対象外にする
		if (filter.includeRemoved != true) {
			whereList.add(getFieldName(ExpTaxTypeHistory__c.Removed__c) + ' = false');
		}
		// ベースIDで検索
		List<Id> pBaseIds;
		if (filter.baseIds != null) {
			pBaseIds = filter.baseIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ExpTaxTypeBase__c.Id) + ' IN :pBaseIds');
		}
		// 国IDで検索
		List<Id> pCountryIds;
		if (filter.countryIds != null) {
			pCountryIds = filter.countryIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ExpTaxTypeBase__c.CountryId__c) + ' IN :pCountryIds');
		}
		// 会社IDで検索
		List<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(HISTORY_BASE_R, ExpTaxTypeBase__c.CompanyId__c) + ' IN :pCompanyIds');
		}
		// コードで検索
		List<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(HISTORY_BASE_R, ExpTaxTypeBase__c.Code__c) + ' IN :pCodes');
		}

		// ORDER BY句
		String orderBy = ' ORDER BY ' + getFieldName(HISTORY_BASE_R, ExpTaxTypeBase__c.Code__c);
		if (sortOrder == HistorySortOrder.VALID_FROM_ASC) {
			orderBy += ', ' + getFieldName(ExpTaxTypeHistory__c.ValidFrom__c) + ' ASC NULLS LAST';
		} else if (sortOrder == HistorySortOrder.VALID_FROM_DESC) {
			orderBy += ', ' + getFieldName(ExpTaxTypeHistory__c.ValidFrom__c) + ' DESC NULLS LAST';
		}

		// クエリ作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ExpTaxTypeHistory__c' +
				+ buildWhereString(whereList)
				+ orderBy;
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		System.debug('ExpTaxTypeRepository: SOQL=' + soql);

		// クエリ実行
		List<ExpTaxTypeHistory__c> sObjs = (List<ExpTaxTypeHistory__c>)Database.query(soql);
		List<ExpTaxTypeHistoryEntity> entities = new List<ExpTaxTypeHistoryEntity>();
		for (ExpTaxTypeHistory__c sObj : sObjs) {
			entities.add(createHistoryEntity(sObj));
		}

		return entities;
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private ExpTaxTypeBaseEntity createBaseEntity(ExpTaxTypeBase__c baseObj) {
		return new ExpTaxTypeBaseEntity(baseObj);
	}

	/**
	 * クエリ実行結果のSObjectからエンティティを作成する
	 * @param 作成元のSObject
	 * @return 作成したエンティティ
	 */
	private ExpTaxTypeHistoryEntity createHistoryEntity(ExpTaxTypeHistory__c historyObj) {
		return new ExpTaxTypeHistoryEntity(historyObj);
	}

	/**
	 * ベースオブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone) {
		List<ExpTaxTypeBase__c> deptList = new List<ExpTaxTypeBase__c>();
		for (SObject obj : baseObjList) {
			deptList.add((ExpTaxTypeBase__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * 履歴オブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 */
	protected override List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone) {
		List<ExpTaxTypeHistory__c> deptList = new List<ExpTaxTypeHistory__c>();
		for (SObject obj : historyObjList) {
			deptList.add((ExpTaxTypeHistory__c)obj);
		}
		return AppDatabase.doUpsert(deptList, allOrNone);
	}

	/**
	 * ベースエンティティからオブジェクトを作成する（各リポジトリで実装する）
	 */
	protected override SObject createBaseObj(ParentChildBaseEntity baseEntity) {
		return ((ExpTaxTypeBaseEntity)baseEntity).createSObject();
	}

	/**
	 * 履歴エンティティからオブジェクトを作成する
	 * @param historyEntity 作成元の履歴エンティティ
	 * @return 作成した履歴オブジェクト
	 */
	protected override SObject createHistoryObj(ParentChildHistoryEntity historyEntity) {
		return ((ExpTaxTypeHistoryEntity)historyEntity).createSObject();
	}
}