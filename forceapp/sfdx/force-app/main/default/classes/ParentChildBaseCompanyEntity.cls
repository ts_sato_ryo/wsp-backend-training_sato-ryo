/**
 * @group Common
 *
 * @description 履歴管理方式が親子型のマスタのベースエンティティの基底クラス(会社に紐づく)
 * Base class of Base entity with parent-child type history management method
 */
public abstract with sharing class ParentChildBaseCompanyEntity extends ParentChildBaseEntity {

	/**
	 * @desctiprion 会社の情報
	 * 本クラスにはすべての親子型マスタで必要となる項目のみを追加するようにしてください
	 * 個々のマスタで必要な項目が発生した場合は、本クラスを継承して利用してください
	 */
	public virtual class Company {
		/** 会社コード */
		public String code;
		/** コンストラクタ */
		public Company(String code) {
			this.code = code;
		}
	}

	/**
	 * コンストラクタ
	 * @param sobj 参照するSObject
	 * @param sobjectFieldMap フィールド名とフィールドのMap
	 */
	public ParentChildBaseCompanyEntity(SObject sobj, Map<String, Schema.SObjectField> sobjectFieldMap) {
		super(sobj, sobjectFieldMap);
	}

	/** 会社 */
	public static final String FIELD_NAME_COMPANY_ID = createNameWithNamespace('CompanyId__c');
	public Id companyId {
		get {
			return (Id)getFieldValue(FIELD_NAME_COMPANY_ID);
		}
		set {
			setFieldValue(FIELD_NAME_COMPANY_ID, value);
		}
	}

	/** コード */
	public static final String FIELD_NAME_CODE = createNameWithNamespace('Code__c');
	public String code {
		get {
			return (String)getFieldValue(FIELD_NAME_CODE);
		}
		set {
			setFieldValue(FIELD_NAME_CODE, value);
		}
	}

	/** ユニークキー */
	public static final String FIELD_NAME_UNIQ_KEY = createNameWithNamespace('UniqKey__c');
	public String uniqKey {
		get {
			return (String)getFieldValue(FIELD_NAME_UNIQ_KEY);
		}
		set {
			setFieldValue(FIELD_NAME_UNIQ_KEY, value);
		}
	}

	/** 一意となるコードまたはユニークキーを取得する */
	public override String getUniqCode() {
		return this.uniqKey;
	}

	/**
	 * ユニークキーを作成する
	 * @param companyCode 会社コード
	 * @param code コード
	 * @return ユニークキー
	 */
	public virtual String createUniqKey(String companyCode, String code) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]ユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]ユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + コード
		return companyCode + '-' + code;
	}
}