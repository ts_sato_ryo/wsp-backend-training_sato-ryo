/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Resource class for Exchange Rate
 */
public with sharing class ExpExchangeRateResource  {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** Rate rounding mode */
	private static RoundingMode RATE_ROUNDING_MODE = RoundingMode.DOWN;

	/** Record class of ExpExchangeRate */
	abstract public class ExpExchangeRateBase {
		/** Record ID */
		public String id;
		/** Exchange Rate code */
		public String code;
		/** Company ID */
		public String companyId;
		/** Currency ID */
		public String currencyId;
		/** Currency Code */
		public String currencyCode;
		/** Currency Name */
		public String currencyName;
		/** Base Currency Code */
		public String baseCurrencyCode;
		/** Base Currency Name */
		public String baseCurrencyName;
		/** Currency Pair */
		public String currencyPair;
		/** Currency Pair label*/
		public String currencyPairLabel;
		/** Rate */
		public Decimal rate;
		/** Reverse Rate */
		public Decimal reverseRate;
		/** Rate for Calculation */
		public Decimal calculationRate;
		/** Valid Date From */
		public Date validDateFrom;
		/** Valid Date To */
		public Date validDateTo;

		/** Create entity from request parameter */
		public ExpExchangeRateEntity createEntity(Map<String, Object> paramMap) {
			ExpExchangeRateEntity entity = new ExpExchangeRateEntity();

			entity.setId(this.id);
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('currencyId')) {
				entity.currencyId = this.currencyId;
			}
			if (paramMap.containsKey('currencyCode')) {
				entity.currencyCode = this.currencyCode;
			}
			if (paramMap.containsKey('currencyPair')) {
				entity.currencyPair = ExpCurrencyPair.valueOf(this.currencyPair);
			}
			if (paramMap.containsKey('rate')) {
				entity.rate = this.rate;
				entity.reverseRate = (1 / entity.rate).setScale(ExpExchangeRateEntity.RATE_DECIMAL_MAX_LENGTH, RATE_ROUNDING_MODE);
			}
			if (paramMap.containsKey('validDateFrom')){
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			} else {
				// If the valid from date is not specified, set today as default.
				entity.validFrom = AppDate.today();
			}
			if (paramMap.containsKey('validDateTo')) {
				entity.validTo = AppDate.valueOf(this.validDateTo);
			} else {
				entity.validTo = ValidPeriodEntity.VALID_TO_MAX;
			}

			return entity;
		}

		/** Set data from entity */
		public void setData(ExpExchangeRateEntity entity) {
			this.id = entity.id;
			this.code = entity.code;
			this.companyId = entity.companyId;
			this.currencyId = entity.currencyId;
			this.currencyCode = entity.currencyCode;
			this.currencyName = entity.currencyNameL.getValue();
			this.currencyPair = entity.currencyPair.value;
			this.currencyPairLabel = entity.currencyPair.label;
			this.rate = entity.rate;
			this.reverseRate = entity.reverseRate;
			this.calculationRate = entity.calculationRate;
			this.validDateFrom = AppConverter.dateValue(entity.validFrom);
			this.validDateTo = AppConverter.dateValue(entity.validTo);
		}
	}

	/** Exchange Rate record class */
	public class ExpExchangeRateRecord extends ExpExchangeRateBase {

	}

	/**
   * @description Record class of global pick list entry
   */
	public class PicklistEntry {
		/** Label */
		public String label;
		/** Value */
		public String value;
	}

	/**
   * @description Parameter for new creation and update
   */
	public class UpsertParam extends ExpExchangeRateBase implements RemoteApi.RequestParam {
	}

	/**
   * @description Parameter for deletion
   */
	public class DeleteParam implements RemoteApi.RequestParam {
		/** Record Id */
		public String id;
	}

	/**
   * Parameter for search
   * */
	public class SearchParam implements RemoteApi.RequestParam {
		public String id;
		public String companyId;
		public String code;
		public String currencyId;
		public String targetDate;
	}

	/**
   * @description Result of saving
   */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** ID of the created record */
		public String id;
	}

	/**
	 * @description Result of search
 	*/
	public class SearchResult implements RemoteApi.ResponseParam {
		public ExpExchangeRateRecord[] records;
	}

	/**
	 * @description Result of getting
	 */
	public class GetCurrencyPairResult implements RemoteApi.ResponseParam {
		/** Get result records */
		public ExpExchangeRateResource.PicklistEntry[] records;
	}

	/**
	 * @description API to create a new Exchange Rate record
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EXCHANGE_RATE;

		/**
		 * @description Create a new Exchange Rate record
		 * @param  req Request parameter (UpsertParam)
		 * @return Response (SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			UpsertParam param = (UpsertParam) req.getParam(UpsertParam.class);

			validateParam(param);

			ExpExchangeRateEntity entity = param.createEntity(req.getParamMap());
			Repository.SaveResult result = ExpExchangeRateService.createExchangeRate(entity);

			SaveResult response = new SaveResult();
			response.id = result.details[0].id;
			return response;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 * */
		private void validateParam(UpsertParam param) {
			ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Com_Lbl_Code, param.code);
			ExpCommonUtil.validateId('Company ID', param.companyId, true);
			ExpCommonUtil.validateId('Currency ID', param.currencyId, true);
		}

	}

	/**
   * @description API to update a Exchange Rate record
   */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EXCHANGE_RATE;

		/**
	 * @description Update a Currency record
	 * @param  req Request parameter (UpsertParam)
	 * @return Response null
	 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			UpsertParam param = (UpsertParam) req.getParam(UpsertParam.class);

			validateParam(param);

			ExpExchangeRateEntity entity = param.createEntity(req.getParamMap());
			ExpExchangeRateService.updateExchangeRate(entity);

			return null;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 **/
		private void validateParam(UpsertParam param) {
			ExpCommonUtil.validateId('ID', param.id, true);
		}

	}

	/**
   * @description API to delete a Exchange Rate record
   */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_EXCHANGE_RATE;

		/**
	   * @description Delete a Exchange Rate records
	   * @param  req Request parameter (DeleteParam)
	   * @return Response null
	   */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			DeleteParam param = (DeleteParam) req.getParam(DeleteParam.class);

			validateParam(param);

			ExpExchangeRateService.deleteExchangeRate(param.id);

			return null;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 **/
		private void validateParam(DeleteParam param) {
			ExpCommonUtil.validateId('ID', param.id, true);
		}

	}

	/**
   * @description API to search Exchange Rate records
   */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search Exchange Rate records
		 * @param  req Request parameter (SearchParam)
	 	* @return Response (SearchResult)
	 	*/
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SearchParam param = (SearchParam)req.getParam(SearchParam.class);

			List<ExpExchangeRateEntity> entityList = ExpExchangeRateService.searchExchangeRate(req.getParamMap());

			return makeResponse(entityList);
		}

		/**
		 * Make response records
		 * @param entityList Records of Exchange Rate entity
		 */
		private SearchResult makeResponse(List<ExpExchangeRateEntity> entityList) {

			List<ExpExchangeRateRecord> records = new List<ExpExchangeRateRecord>();

			for (ExpExchangeRateEntity entity : entityList) {
				ExpExchangeRateRecord rec = new ExpExchangeRateRecord();
				rec.setData(entity);
				records.add(rec);
			}

			SearchResult result = new SearchResult();
			result.records = records;
			return result;
		}
	}

	/**
   * @description API to search Currency Pair
   */
	public with sharing class GetCurrencyPairApi extends RemoteApi.ResourceBase {

		/*
		 * @description Search ExpCurrencyPair picklist
		 *
		 * @param req Request parameter (SearchParam)
		 *
		 * @return Response (SearchResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			List<ExpCurrencyPair> pickList = ExpCurrencyPair.getTypeList();

			return makeResponse(pickList);
		}

		private GetCurrencyPairResult makeResponse(List<ExpCurrencyPair> pickList) {

			List<PicklistEntry> entries = new List<PicklistEntry>();

			for (ExpCurrencyPair c : pickList) {
				PicklistEntry entry = new PicklistEntry();
				entry.label = c.label;
				entry.value = c.value;
				entries.add(entry);
			}

			GetCurrencyPairResult response = new GetCurrencyPairResult();
			response.records = entries;
			return response;
		}
	}
}