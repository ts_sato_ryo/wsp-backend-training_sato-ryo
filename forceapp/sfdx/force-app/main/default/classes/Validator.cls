/**
 * バリデーション
 * バリデーションクラスを定義する場合は、BaseValidatorクラスを継承してください。
 */
public abstract class Validator {

	/**
	 * バリデーションの結果
	 */
	public class Result {

		/** エラーリスト */
		private List<Validator.Error> errors;

		/** コンストラクタ */
		public Result() {
			errors = new List<Validator.Error>();
		}

		/**
		 * バリデーションが成功したかどうか
		 * @return バリデーションエラーが存在しないかった場合はtrue、そうでない場合はfalse
		 */
		public Boolean isSuccess() {
			if (errors.isEmpty()) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * バリデーションでエラーが検知されたか
		 * @return バリデーションエラーが存在した場合はtrue, そうでない場合はfalse
		 */
		public Boolean hasError() {
			return ! isSuccess();
		}

		/**
		 * エラーのリストを返却する
		 */
		public List<Validator.Error> getErrors() {
			return this.errors;
		}

		/**
		 * エラーを返却する
		 * @param index 取得対象のエラーのインデックス
		 * @return エラー
		 */
		public Validator.Error getError(Integer index) {
			return this.errors.get(index);
		}

		/**
		 * エラーを追加する
		 * @param エラーコード
		 * @param エラーメッセージ
		 */
		public void addError(String code, String message) {
			Validator.Error error = new Validator.Error();
			error.code = code;
			error.message = message;

			this.errors.add(error);
		}

		/**
		 * エラーを追加する
		 * @param エラーコード
		 * @param エラーメッセージ
		 */
		public void addError(Validator.Error error) {
			this.errors.add(error);
		}

		/**
		 * エラーを追加する
		 * @param エラーコード
		 * @param エラーメッセージ
		 */
		public void addErrors(List<Validator.Error> errors) {
			this.errors.addAll(errors);
		}
		/**
		 * エラーをクリアする(暫定テストコード用)
		 */
		public void clearErrors() {
			this.errors.clear();
		}
	}

	/**
	 * バリデーションエラー情報
	 */
	public class Error {
		/** エラーメッセージ */
		public String message {get; set;}
		/** エラーコード */
		public String code {get; set;}
	}

	/**
	 * バリデーションの基底クラス
	 */
	public abstract class BaseValidator {

		@testVisible
		protected Validator.Result result;

		/** コンストラクタ */
		public BaseValidator() {
			this.result = new Validator.Result();
		}

		/**
		 * バリデーションを実行する
		 * バリデーション対象のエンティティはコンストラクト時にクラス変数にしておくこと
		 */
		public abstract Validator.Result validate();

	}

	//----------------------------------------------------------------------
	// エラー作成メソッド
	// コンテキスト問わず共通で利用できるメソッドを定義してください
	//----------------------------------------------------------------------

	/**
	 * 項目未設定のエラーを作成
	 * メッセージ：[fieldName]が設定されていません。
	 */
	public static Validator.Error createNullError(String fieldName) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_NOT_SET_VALUE;
		error.message = ComMessage.msg().Com_Err_NotSetValue(new List<String>{fieldName});
		return error;
	}

	/**
	 * 値が正しくない場合のエラーを作成
	 * メッセージ：[fieldName]の値が正しくありません。
	 */
	public static Validator.Error createInvalidError(String fieldName) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_InvalidValue(new List<String>{fieldName});
		return error;
	}

	/**
	 * 文字数オーバーのエラーを作成
	 * メッセージ：[fieldName]の文字数は[maxLength]以下にしてください
	 */
	public static Validator.Error createLengthOverError(String fieldName, Integer maxLength) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_MaxLengthOver(new List<String>{fieldName, maxLength.format()});
		return error;
	}

	/**
	 * 値が範囲外の場合のエラーを作成
	 * メッセージ：[objName]の[fieldName]は[minValue]から[maxValue]の間で指定してください。
	 */
	public static Validator.Error createValueRangeError(
			String fieldName, Integer minValue, Integer maxValue) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_InvalidValueRange(
				new List<String>{fieldName, minValue.format(), maxValue.format()});
		return error;
	}

	/**
	 * 値が範囲外の場合のエラーを作成(勤怠日次時間)
	 * メッセージ：[objName]の[fieldName]は[minValue]から[maxValue]の間で指定してください。
	 */
	public static Validator.Error createValueRangeError(
			String fieldName, AttDailyTime minValue, AttDailyTime maxValue) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_InvalidValueRange(
				new List<String>{fieldName, minValue.format(), maxValue.format()});
		return error;
	}


	/**
	 * 値が下限値よりも小さい場合のエラーを作成
	 * メッセージ：[fieldName]は[value]以上を指定してください。
	 */
	public static Validator.Error createValueLessError(String fieldName, Integer value) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_InvalidValueMoreEq(new List<String>{fieldName, value.format()});
		return error;
	}

	/**
	 * 値が負数の場合のエラーを作成
	 * メッセージ：[fieldName]は0以上にしてください
	 */
	public static Validator.Error createNagativeValueError(String fieldName) {
		final String zeroValue = '0';
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_InvalidValueMoreEq(new List<String>{fieldName, zeroValue});
		return error;
	}

	/**
	 * 値が上限値よりも大きい場合のエラーを作成
	 * メッセージ：[fieldName]の値が大きすぎます
	 */
	public static Validator.Error createValueTooLargeError(String fieldName) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_InvalidValueTooLarge(new List<String>{fieldName});
		return error;
	}

	/**
	 * 値が上限値よりも大きい場合のエラーを作成
	 * メッセージ：(fieldName)は(value)以下にしてください。
	 */
	public static Validator.Error createValueMoreError(String fieldName, Integer value) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Com_Err_MaxValue(new List<String>{fieldName, value.format()});
		return error;
	}

	/**
	 * 日付が最小日付よりも小さい場合のエラーを作成
	 * メセージ：[fieldName]は[minDate]以降を指定してください
	 */
	public static Validator.Error createDateLessError(String fieldName, AppDate minDate) {
		Validator.Error error = new Validator.Error();
		error.code = App.ERR_CODE_INVALID_VALUE;
		error.message = ComMessage.msg().Admin_Err_DateLess(new List<String>{fieldName, minDate.getDate().format()});
		return error;
	}

	//----------------------------------------------------------------------
	// 検証メソッド
	// コンテキスト問わず共通で利用できるメソッドを定義してください
	//----------------------------------------------------------------------

	/**
	 * 文字列が最大文字数を超えていないかをチェックする
	 * @param target 対象文字列
	 * @param maxLength 最大文字数
	 * @return 最大文字を超えている場合はtrue, そうでない場合はfalse
	 */
	public static Boolean isInvalidMaxLengthOver(String target, Integer maxLength) {
		if (target == null) {
			return false;
		}
		if (target.length() > maxLength) {
			return true;
		}
		return false;
	}

	/**
	 * 値が負の整数値でないことを確認する
	 * @param target 対象の数値
	 * @return 負の整数の場合はtrue, そうでない場合はfalse
	 */
	public static Boolean isNegativeInteger(Integer target) {
		if (target == null) {
			return false;
		}
		if (target < 0) {
			return true;
		}
		return false;
	}

	/**
	 * 値が指定した最小値以上であることを確認する Validate if the value is equal to or more than the lower limit value
	 * @param target 対象の数値 Target value
	 * @param minValue 最小値 Lower limit value
	 * @return 最小値未満の場合はtrue, そうでない場合はfalse Returns true if the target value is less than the minValue. oherwise, returns false.
	 */
	public static Boolean isInvalidMinValue(Decimal target, Decimal minValue) {
		if (target == null) {
			return false;
		}
		if (target < minValue) {
			return true;
		}
		return false;
	}

	/**
	 * 値が指定した最大値以下であることを確認する Validate if the value is equal to or less than the upper limit value
	 * @param target 対象の数値 Target value
	 * @param maxValue 最大値 Upper limit value
	 * @return 最大値より大きい場合はtrue, そうでない場合はfalse Returns true if the target value is more than the maxValue. oherwise, returns false.
	 */
	public static Boolean isInvalidMaxValue(Decimal target, Decimal maxValue) {
		if (target == null) {
			return false;
		}
		if (target > maxValue) {
			return true;
		}
		return false;
	}

}
