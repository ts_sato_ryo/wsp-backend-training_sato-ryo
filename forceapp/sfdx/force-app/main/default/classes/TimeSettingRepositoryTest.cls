/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * TimeSettingRepositoryのテスト
 */
@isTest
private class TimeSettingRepositoryTest {

	/** テストデータクラス */
	private class TestData {

		/** 組織オブジェクト(L0='ja', L1='en_US', L2='zh_CN') */
		public ComOrgSetting__c orgObj;
		/** 国オブジェクト */
		public ComCountry__c countryObj;
		/** 会社オブジェクト */
		public ComCompany__c companyObj {get; private set;}

		/** コンストラクタ */
		public TestData() {
			orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
			countryObj = ComTestDataUtility.createCountry('Japan');
			companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		}

		/**
		 * 工数設定エンティティを作成する(ベース)
		 */
		public TimeSettingBaseEntity createTimeSettingEntity(String name, String code) {
			TimeSettingBaseEntity entity = new TimeSettingBaseEntity();
			entity.name = name;
			entity.code = code;
			entity.uniqKey = this.companyObj.Code__c + '-' + code;
			entity.companyId = this.companyObj.Id;
			entity.summaryPeriod = TimeSettingBaseGeneratedEntity.SummaryPeriodType.Month;
			entity.startDateOfMonth = 12;
			entity.monthMark = TimeSettingBaseGeneratedEntity.MonthMarkType.BeginBase;
			entity.useRequest = true;

			return entity;
		}

		/**
		 * 工数設定履歴エンティティを作成する
		 */
		public TimeSettingHistoryEntity createTimeSettingEntity(String name, Id baseId, AppDate validFrom, AppDate validTo) {
			TimeSettingHistoryEntity entity = new TimeSettingHistoryEntity();
			if (baseId != null) {
				entity.baseId = baseId;
			}
			entity.name = name;
			entity.uniqKey = name + validFrom;
			entity.historyComment = '改定コメント';
			entity.validFrom = validFrom;
			entity.validTo = validTo;
			entity.isRemoved = false;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';

			return entity;
		}

		/**
		 * 工数オブジェクトを作成する
		 */
		public List<TimeSettingBase__c> createTimeSettingsWithHistory(String name, Integer baseSize) {
			return ComTestDataUtility.createTimeSettingsWithHistory(name, this.companyObj.Id, baseSize);
		}
	}

	/**
	 * getEntityのテスト
	 * 指定したベースIDのエンティティが取得できることを確認する
	 */
	@isTest static void getEntityTestAllHistory() {

		final Integer baseSize = 3;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> baseList = testData.createTimeSettingsWithHistory('Test', baseSize);
		TimeSettingBase__c targetBase = baseList[2];
		TimeSettingHistory__c targetHistory = [
				SELECT Id, Name, BaseId__c, Removed__c, Name_L0__c, ValidFrom__c, ValidTo__c FROM TimeSettingHistory__c WHERE BaseId__c = :targetBase.Id];
		TimeSettingHistory__c targetHistory2 = targetHistory.clone();
		targetHistory2.ValidFrom__c = targetHistory.ValidTo__c;
		targetHistory2.ValidTo__c = targetHistory2.ValidFrom__c.addDays(10);
		targetHistory2.UniqKey__c = targetHistory2.Name + targetHistory2.ValidFrom__c;
		insert targetHistory2;

		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter;
		TimeSettingBaseEntity resBase;

		resBase = repo.getEntity(targetBase.Id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(targetBase.Id, resBase.id);
		System.assertEquals(2, resBase.getHistoryList().size());
	}

	/**
	 * getEntityのテスト(Id, AppDate指定)
	 * 指定したベースIDと指定した日付のエンティティが取得できることを確認する
	 */
	@isTest static void getEntityTestTargetDate() {

		final Integer baseSize = 3;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> baseList = testData.createTimeSettingsWithHistory('Test', baseSize);
		TimeSettingBase__c targetBase = baseList[2];
		TimeSettingHistory__c targetHistory = [
				SELECT Id, Name, BaseId__c, Removed__c, Name_L0__c, ValidFrom__c, ValidTo__c FROM TimeSettingHistory__c WHERE BaseId__c = :targetBase.Id];
		TimeSettingHistory__c targetHistory2 = targetHistory.clone();
		targetHistory2.ValidFrom__c = targetHistory.ValidTo__c;
		targetHistory2.ValidTo__c = targetHistory2.ValidFrom__c.addDays(10);
		targetHistory2.UniqKey__c = targetHistory2.Name + targetHistory2.ValidFrom__c;
		insert targetHistory2;

		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter;
		TimeSettingBaseEntity resBase;

		resBase = repo.getEntity(targetBase.Id, AppDate.valueOf(targetHistory2.ValidFrom__c));
		System.assertNotEquals(null, resBase);
		System.assertEquals(targetBase.Id, resBase.id);
		System.assertEquals(1, resBase.getHistoryList().size());
		System.assertEquals(targetHistory2.Id, resBase.getHistory(0).id);
	}

	/**
	 * getBaseEntityのテスト
	 * 指定したベースIDのエンティティが取得できることを確認する
	 */
	@isTest static void getBaseEntityTest() {

		final Integer baseSize = 3;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> baseList = testData.createTimeSettingsWithHistory('Test', baseSize);
		TimeSettingBase__c targetBase = baseList[2];
		TimeSettingHistory__c targetHistory = [
				SELECT Id, Name, BaseId__c, Removed__c, Name_L0__c, ValidFrom__c, ValidTo__c FROM TimeSettingHistory__c WHERE BaseId__c = :targetBase.Id];

		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingBaseEntity resBase;

		resBase = repo.getBaseEntity(targetBase.Id);
		System.assertNotEquals(null, resBase);
		System.assertEquals(targetBase.Id, resBase.id);
		System.assertEquals(null, resBase.getHistoryList());

		// 存在しないIDを指定した場合はnullが返却
		delete targetBase;
		resBase = repo.getBaseEntity(targetBase.Id);
		System.assertEquals(null, resBase);
	}

	/**
	 * getBaseEntityByCodeのテスト
	 * 指定したコードのエンティティが取得できることを確認する
	 */
	@isTest static void getBaseEntityListByCodeTest() {

		final Integer baseSize = 3;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> baseList = testData.createTimeSettingsWithHistory('Test', baseSize);
		TimeSettingBase__c targetBase = baseList[2];
		TimeSettingHistory__c targetHistory = [
				SELECT Id, Name, BaseId__c, Removed__c, Name_L0__c, ValidFrom__c, ValidTo__c FROM TimeSettingHistory__c WHERE BaseId__c = :targetBase.Id];

		TimeSettingRepository repo = new TimeSettingRepository();
		List<TimeSettingBaseEntity> resBases;

		resBases = repo.getBaseEntityListByCode(new List<String>{targetBase.Code__c});
		System.assertEquals(1, resBases.size());
		System.assertEquals(targetBase.Id, resBases[0].id);
		// ベースのみ取得
		System.assertEquals(null, resBases[0].getHistoryList());

	}


	/**
	 * getHistoryEntityのテスト
	 * 指定した履歴IDのエンティティが取得できることを確認する
	 */
	@isTest static void getHistoryEntityTest() {

		final Integer baseSize = 3;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> baseList = testData.createTimeSettingsWithHistory('Test', baseSize);
		TimeSettingBase__c targetBase = baseList[2];
		TimeSettingHistory__c targetHistory = [
				SELECT Id, Name, BaseId__c, Removed__c, Name_L0__c, ValidFrom__c, ValidTo__c FROM TimeSettingHistory__c WHERE BaseId__c = :targetBase.Id];

		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter;
		TimeSettingHistoryEntity resHistory;

		resHistory = repo.getHistoryEntity(targetHistory.Id);
		System.assertNotEquals(null, resHistory);
		System.assertEquals(targetHistory.Id, resHistory.id);

		// 存在しないIDを指定した場合はnullが返却
		targetHistory.Removed__c = true;
		update targetHistory;
		resHistory = repo.getHistoryEntity(targetHistory.Id);
		System.assertEquals(null, resHistory);
	}

	/**
	 * 検索できることを確認する
	 */
	@isTest static void sarchEntityTest() {

		final Integer baseSize = 3;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> baseList = testData.createTimeSettingsWithHistory('Test', baseSize);
		TimeSettingBase__c targetBase = baseList[2];
		TimeSettingHistory__c targetHistory = [
				SELECT Id, Name, Removed__c, ValidFrom__c, ValidTo__c FROM TimeSettingHistory__c WHERE BaseId__c = :targetBase.Id];


		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter;
		List<TimeSettingBaseEntity> resBaseList;

		// 工数ベースIDで検索
		// 全ての項目の値が取得できていることを確認する
		filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{targetBase.Id};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetBase.Id, resBaseList[0].id);
		System.assertEquals(1, resBaseList[0].getHistoryList().size());
		TimeSettingBase__c expBaseObj = getBaseObj(resBaseList[0].id);
		TimeSettingHistory__c expHistoryObj = getHistoryObjList(resBaseList[0].id).get(0);
		assertBaseEntity(expBaseObj, resBaseList[0]);
		assertHistoryEntity(expHistoryObj, resBaseList[0].getHistory(0));

		// 工数履歴IDで検索
		filter = new TimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{targetHistory.Id};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetHistory.Id, resBaseList[0].getHistory(0).id);

		// 会社ID
		ComCompany__c targetCompany = ComTestDataUtility.createCompany('Tareget Company', testData.countryObj.Id);
		TimeSettingBase__c timeSettingCompany = ComTestDataUtility.createTimeSettingsWithHistory('会社検索テスト', targetCompany.Id, 1).get(0);
		filter = new TimeSettingRepository.SearchFilter();
		filter.companyIds = new Set<Id>{targetCompany.Id};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(timeSettingCompany.Id, resBaseList[0].id);

		// コード(完全一致)
		filter = new TimeSettingRepository.SearchFilter();
		filter.codes = new Set<String>{targetBase.Code__c};
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetBase.Id, resBaseList[0].id);

		// 論理削除されている履歴を取得対象にする場合はtrue
		targetHistory.Removed__c = true;
		update targetHistory;
		filter = new TimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{targetHistory.Id};
		// falseなのでtargetHistoryは取得できない
		filter.includeRemoved = false;
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(0, resBaseList.size());
		// trueなのでtargetHistoryは取得できる
		filter.includeRemoved = true;
		resBaseList = repo.searchEntity(filter);
		System.assertEquals(1, resBaseList.size());
		System.assertEquals(targetHistory.Id, resBaseList[0].getHistory(0).id);
	}

	/**
	 * エンティティ取得のテスト
	 * 指定した対象日に有効な履歴が取得できることを確認する
	 */
	@isTest static void searchEntityTestValidDate() {

		// テストデータ作成
		final Integer baseSize = 3;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> bases = testData.createTimeSettingsWithHistory('Test', baseSize);
		TimeSettingBase__c targetBase = getBaseObj(bases[0].Id);
		TimeSettingHistory__c targetHistory = getHistoryObjList(targetBase.Id).get(0);
		TimeSettingHistory__c targetHistory2 = targetHistory.clone();
		targetHistory2.ValidFrom__c = targetHistory.ValidTo__c;
		targetHistory2.ValidTo__c = targetHistory2.ValidFrom__c.addMonths(1);
		targetHistory2.UniqKey__c = targetBase.Code__c + '_' + targetHistory2.ValidFrom__c;
		TimeSettingHistory__c targetHistory3 = targetHistory.clone();
		targetHistory3.ValidFrom__c = targetHistory2.ValidTo__c;
		targetHistory3.ValidTo__c = targetHistory3.ValidFrom__c.addMonths(1);
		targetHistory3.UniqKey__c = targetBase.Code__c + '_' + targetHistory3.ValidFrom__c;
		insert new List<TimeSettingHistory__c>{targetHistory2, targetHistory3};

		TimeSettingRepository repo = new TimeSettingRepository();
		List<TimeSettingBaseEntity> resBases;
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{targetBase.Id};

		// 取得対象日に履歴の有効期間開始日を指定
		filter.targetDate = AppDate.valueOf(targetHistory2.ValidFrom__c);
		resBases = repo.searchEntity(filter);
		System.assertEquals(1, resBases.size());
		System.assertEquals(targetBase.Id, resBases[0].id);
		System.assertEquals(targetHistory2.Id, resBases[0].getHistory(0).id);

		// 取得対象日に最新履歴の失効日の前日を指定
		// 取得できるはず
		filter.targetDate = AppDate.valueOf(targetHistory3.ValidTo__c.addDays(-1));
		resBases = repo.searchEntity(filter);
		System.assertEquals(1, resBases.size());
		System.assertEquals(targetBase.Id, resBases[0].id);
		System.assertEquals(targetHistory3.Id, resBases[0].getHistory(0).id);

		// 取得対象日に最新履歴の失効日を指定
		// 取得できないはず
		filter.targetDate = AppDate.valueOf(targetHistory3.ValidTo__c);
		resBases = repo.searchEntity(filter);
		System.assertEquals(0, resBases.size());

	}

	/**
	 * 検索できることを確認する
	 */
	@isTest static void sarchEntityMapTest() {

		final Integer baseSize = 10;
		TimeSettingRepositoryTest.TestData testData = new TestData();
		List<TimeSettingBase__c> baseList = testData.createTimeSettingsWithHistory('Test', baseSize);

		// 実行
		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{baseList[0].id, baseList[3].id, baseList[5].id};
		Map<Id, TimeSettingBaseEntity> settingMap;
		try {
			Test.startTest();
			settingMap = repo.searchEntityMap(filter);
			Test.stopTest();
		} catch (Exception e){
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}

		// 検証
		System.assertEquals(3, settingMap.size());
		assertBaseEntity(baseList[0], settingMap.get(baseList[0].id));
		assertBaseEntity(baseList[3], settingMap.get(baseList[3].id));
		assertBaseEntity(baseList[5], settingMap.get(baseList[5].id));
	}

	/**
	 * 空でも正常終了して検索できることを確認する
	 */
	@isTest static void sarchEntityMapTestEmpty() {
		// 実行
		TimeSettingRepository repo = new TimeSettingRepository();
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{UserInfo.getUserId()};
		Map<Id, TimeSettingBaseEntity> settingMap;
		try {
			Test.startTest();
			settingMap = repo.searchEntityMap(filter);
			Test.stopTest();
		} catch (Exception e){
			TestUtil.fail('予期せぬ例外が発生しました' + e.getMessage());
		}

		// 検証
		System.assertEquals(0, settingMap.size());
	}

	/**
	 * 新規の工数設定履歴エンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		TimeSettingRepositoryTest.TestData testData = new TestData();
		TimeSettingBaseEntity base = testData.createTimeSettingEntity('登録テスト', 'Test001');
		base.addHistory(testData.createTimeSettingEntity(base.name, null, AppDate.today(), AppDate.today().addMonths(1)));

		Test.startTest();
			Repository.SaveResultWithChildren result = new TimeSettingRepository().saveEntity(base);
		Test.stopTest();

		// 検証
		System.assertEquals(true, result.isSuccessAll);
		Id baseId = result.details[0].id;
		TimeSettingBase__c resBaseObj = getBaseObj(baseId);
		assertSavedBaseObj(base, resBaseObj);
		List<TimeSettingHistory__c> resHistoryList = getHistoryObjList(baseId);
		System.assertEquals(1, resHistoryList.size());
		TimeSettingHistory__c resHistoryObj = resHistoryList[0];
		assertSavedHistoryObj(base.getHistory(0), resHistoryObj, baseId);
	}

	/**
	 * 同一の条件で検索済みの場合、2度目の検索はキャッシュから結果を取得することを検証する。
	 */
	@isTest static void searchFromCache() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];
		TimeSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		TimeSettingRepository repository = new TimeSettingRepository(true);

		// 検索（DBから取得）
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		filter.historyIds = new Set<Id>{history.Id};
		TimeSettingBaseEntity timeSetting = repository.searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		TimeSettingBaseEntity cachedTimeSetting = repository.searchEntity(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(timeSetting.id, cachedTimeSetting.id);
		System.assertNotEquals('NotCache', cachedTimeSetting.code);
		System.assertNotEquals('NotCache', cachedTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * Repositoryのインスタンスが異なっても、同一のキャッシュを利用することを検証する
	 */
	@isTest static void searchFromCacheMultipleInstance() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];
		TimeSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// 検索（DBから取得）
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		filter.historyIds = new Set<Id>{history.Id};
		TimeSettingBaseEntity timeSetting = new TimeSettingRepository(true).searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 再検索（キャッシュから取得）
		Integer queryCount = Limits.getQueries();
		TimeSettingBaseEntity cachedTimeSetting = new TimeSettingRepository(true).searchEntity(filter)[0];

		// 検証
		System.assertEquals(queryCount, Limits.getQueries());
		System.assertEquals(timeSetting.id, cachedTimeSetting.id);
		System.assertNotEquals('NotCache', cachedTimeSetting.code);
		System.assertNotEquals('NotCache', cachedTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。（工数設定設定ベース検索）
	 */
	@isTest static void searchBaseNotFromCacheDifferentCondition() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];

		// キャッシュをonでインスタンスを生成
		TimeSettingRepository repository = new TimeSettingRepository(true);

		// 検索（DBから取得）
		List<Id> baseIds = new List<Id>{base.Id};
		TimeSettingBaseEntity timeSetting = repository.getBaseEntityList(baseIds, false)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		baseIds.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		TimeSettingBaseEntity searchedTimeSetting = repository.getBaseEntityList(baseIds, false)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(timeSetting.id, searchedTimeSetting.id);
		System.assertEquals('NotCache', searchedTimeSetting.code);
	}

	/**
	 * 検索済みの条件と異なる場合、DBから結果を取得することを検証する。（工数設定履歴検索）
	 */
	@isTest static void searchHistoryNotFromCacheDifferentCondition() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];
		TimeSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		TimeSettingRepository repository = new TimeSettingRepository(true);

		// 検索（DBから取得）
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		TimeSettingRepository.HistorySortOrder sortOrder = TimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		TimeSettingHistoryEntity timeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 異なる条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		filter.baseIds.add(testData.companyObj.Id); // 検索結果に該当しないIDを追加する
		TimeSettingHistoryEntity searchedTimeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(timeSetting.id, searchedTimeSetting.id);
		System.assertEquals('NotCache', searchedTimeSetting.historyComment);
	}


	/**
	 * 工数設定ベースが同一で異なる履歴を検索した場合、条件に一致する履歴のみが取得結果のベースに紐づくことを検証する。（工数設定ベース + 履歴検索）
	 */
	@isTest static void searchEntityWityHistoryFromCacheSameBase() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];
		delete getHistoryObjList(base.Id).get(0);

		// 工数設定履歴を3件作成
		TimeSettingHistoryEntity history1 = testData.createTimeSettingEntity('history1', base.Id, AppDate.newInstance(2019, 4, 1), AppDate.newInstance(2019, 5, 1));
		TimeSettingHistoryEntity history2 = testData.createTimeSettingEntity('history2', base.Id, AppDate.newInstance(2019, 5, 1), AppDate.newInstance(2019, 6, 1));
		TimeSettingHistoryEntity history3 = testData.createTimeSettingEntity('history3', base.Id, AppDate.newInstance(2019, 6, 1), AppDate.newInstance(2019, 7, 1));
		new TimeSettingRepository().saveHistoryEntityList(new List<TimeSettingHistoryEntity>{history1, history2, history3});

		// キャッシュをonでインスタンスを生成
		TimeSettingRepository repository = new TimeSettingRepository(true);

		// 検索（ベース1件 + 履歴3件）
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		TimeSettingRepository.HistorySortOrder sortOrder = TimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		TimeSettingBaseEntity timeSetting = repository.searchEntityWithHistory(filter, false, sortOrder)[0];
		System.assertEquals(base.id, timeSetting.id);
		System.assertEquals(3, timeSetting.getHistoryList().size());

		// 異なる条件で同じベースを再検索（ベース1件 + 履歴1件）
		filter.targetDate = AppDate.newInstance(2019, 5, 1);
		TimeSettingBaseEntity searchedTimeSetting = repository.searchEntityWithHistory(filter, false, sortOrder)[0];
		System.assertEquals(timeSetting.id, searchedTimeSetting.id);
		System.assertEquals(1, searchedTimeSetting.getHistoryList().size());
	}

	/**
	 * ソート順が異なる場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheDifferentSortOrder() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];
		TimeSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		TimeSettingRepository repository = new TimeSettingRepository(true);

		// 検索（DBから取得）
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.historyIds = new Set<Id>{history.Id};
		TimeSettingRepository.HistorySortOrder sortOrder = TimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		TimeSettingHistoryEntity timeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 異なるソート順で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		sortOrder = TimeSettingRepository.HistorySortOrder.VALID_FROM_DESC;
		TimeSettingHistoryEntity searchedTimeSetting = repository.searchHistoryEntity(filter, false, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertEquals(timeSetting.id, searchedTimeSetting.id);
		System.assertEquals('NotCache', searchedTimeSetting.historyComment);
	}

	/**
	 * 「FOR UPDATE」で検索をする場合、DBから結果を取得することを検証する。
	 */
	@isTest static void searchNotFromCacheForUpdate() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];
		TimeSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをonでインスタンスを生成
		TimeSettingRepository repository = new TimeSettingRepository(true);

		// 検索（DBから取得）
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		TimeSettingRepository.HistorySortOrder sortOrder = TimeSettingRepository.HistorySortOrder.VALID_FROM_ASC;
		TimeSettingBaseEntity timeSetting = repository.searchEntityWithHistory(filter, false, sortOrder)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// ForUpdateで再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		TimeSettingBaseEntity searchedTimeSetting = repository.searchEntityWithHistory(filter, true, sortOrder)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertNotEquals(timeSetting, searchedTimeSetting);
		System.assertEquals('NotCache', searchedTimeSetting.code);
		System.assertEquals('NotCache', searchedTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * キャッシュを使用しない場合、常にDBから結果を取得することを検証する。
	 */
	@isTest static void searchDisableCache() {
		TestData testData = new TestData();
		TimeSettingBase__c base = testData.createTimeSettingsWithHistory('Test', 1)[0];
		TimeSettingHistory__c history = getHistoryObjList(base.Id).get(0);

		// キャッシュをoffでインスタンスを生成
		TimeSettingRepository repository = new TimeSettingRepository();

		// 検索（DBから取得）
		TimeSettingRepository.SearchFilter filter = new TimeSettingRepository.SearchFilter();
		filter.baseIds = new Set<Id>{base.Id};
		filter.historyIds = new Set<Id>{history.Id};
		TimeSettingBaseEntity timeSetting = repository.searchEntity(filter)[0];

		// 取得対象のレコードを更新する
		base.Code__c = 'NotCache';
		upsert base;
		history.HistoryComment__c = 'NotCache';
		upsert history;

		// 同一条件で再検索（DBから取得）
		Integer queryCount = Limits.getQueries();
		TimeSettingBaseEntity searchedTimeSetting = repository.searchEntity(filter)[0];

		// 検証
		System.assertNotEquals(queryCount, Limits.getQueries());
		System.assertNotEquals(timeSetting, searchedTimeSetting);
		System.assertEquals('NotCache', searchedTimeSetting.code);
		System.assertEquals('NotCache', searchedTimeSetting.getHistoryList()[0].historyComment);
	}

	/**
	 * enableCache()のテスト
	 * キャッシュが有効化されることを確認する
	 */
	@isTest static void enableCacheTest() {
		// キャッシュ有効化
		TimeSettingRepository.enableCache();

		// キャッシュ利用有無を指定せずにインスタンスを生成するとキャッシュ利用が有効になっている
		System.assertEquals(true, new TimeSettingRepository().isEnabledCache);
	}

	/**
	 * 指定したID工数設定ベースオブジェクトw取得する
	 */
	private static TimeSettingBase__c getBaseObj(Id baseId) {
		return [
				SELECT
					Id, Name, CurrentHistoryId__c, Code__c, UniqKey__c, CompanyId__c,
					SummaryPeriod__c, StartDateOfMonth__c, StartDayOfWeek__c, MonthMark__c, UseRequest__c
				FROM
					TimeSettingBase__c
				WHERE Id = :baseId
		];
	}

	/**
	 * 指定したIDの工数設定履歴オブジェクトを取得する
	 */
	private static List<TimeSettingHistory__c> getHistoryObjList(Id baseId) {
		return [
				SELECT
					Id, Name, BaseId__c, UniqKey__c, Removed__c,
					HistoryComment__c, ValidFrom__c, ValidTo__c,
					Name_L0__c, Name_L1__c, Name_L2__c
				FROM
					TimeSettingHistory__c
				WHERE
					BaseId__c = :baseId
		];
	}

	/**
	 * ベースエンティティが正しく取得できているかを検証する
	 */
	private static void assertBaseEntity(TimeSettingBase__c expObj, TimeSettingBaseEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.CurrentHistoryId__c, actEntity.currentHistoryId);
		System.assertEquals(expObj.Code__c, actEntity.code);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.CompanyId__c, actEntity.companyId);

		System.assertEquals(expObj.SummaryPeriod__c, String.valueOf(actEntity.summaryPeriod));
		System.assertEquals(expObj.StartDayOfWeek__c, String.valueOf(actEntity.startDayOfWeek));
		System.assertEquals(expObj.StartDateOfMonth__c, actEntity.startDateOfMonth);
		System.assertEquals(expObj.MonthMark__c, String.valueOf(actEntity.monthMark));
		System.assertEquals(expObj.UseRequest__c, actEntity.useRequest);
	}

	/**
	 * 履歴エンティティが正しく取得できているかを検証する
	 */
	private static void assertHistoryEntity(TimeSettingHistory__c expObj, TimeSettingHistoryEntity actEntity) {
		System.assertEquals(expObj.Name, actEntity.name);
		System.assertEquals(expObj.BaseId__c, actEntity.baseId);
		System.assertEquals(expObj.UniqKey__c, actEntity.uniqKey);
		System.assertEquals(expObj.HistoryComment__c, actEntity.historyComment);
		System.assertEquals(expObj.ValidFrom__c, actEntity.validFrom.getDate());
		System.assertEquals(expObj.ValidTo__c, actEntity.validTo.getDate());
		System.assertEquals(expObj.Removed__c, actEntity.isRemoved);

		System.assertEquals(expObj.Name_L0__c, actEntity.nameL0);
		System.assertEquals(expObj.Name_L1__c, actEntity.nameL1);
		System.assertEquals(expObj.Name_L2__c, actEntity.nameL2);
	}

	/**
	 * TimeSettingBase__cに正しく保存できているかを検証する
	 */
	private static void assertSavedBaseObj(TimeSettingBaseEntity expEntity, TimeSettingBase__c actObj) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(expEntity.code, actObj.Code__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.currentHistoryId, actObj.CurrentHistoryId__c);
		System.assertEquals(String.valueOf(expEntity.summaryPeriod), actObj.SummaryPeriod__c);
		System.assertEquals(expEntity.startDateOfMonth, actObj.StartDateOfMonth__c);
		System.assertEquals(String.valueOf(expEntity.startDayOfWeek), actObj.StartDayOfWeek__c);
		System.assertEquals(String.valueOf(expEntity.monthMark), actObj.MonthMark__c);
		System.assertEquals(expEntity.useRequest, actObj.UseRequest__c);
	}

	/**
	 * TimeSettingHistory__cに正しく保存できているかを検証する
	 */
	private static void assertSavedHistoryObj(TimeSettingHistoryEntity expEntity, TimeSettingHistory__c actObj, Id baseId) {
		System.assertEquals(expEntity.name, actObj.Name);
		System.assertEquals(baseId, actObj.BaseId__c);
		System.assertEquals(expEntity.uniqKey, actObj.UniqKey__c);
		System.assertEquals(expEntity.historyComment, actObj.HistoryComment__c);
		System.assertEquals(expEntity.validFrom.getDate(), actObj.ValidFrom__c);
		System.assertEquals(expEntity.validTo.getDate(), actObj.ValidTo__c);

		System.assertEquals(expEntity.nameL0, actObj.Name_L0__c);
		System.assertEquals(expEntity.nameL1, actObj.Name_L1__c);
		System.assertEquals(expEntity.nameL2, actObj.Name_L2__c);
	}

}
