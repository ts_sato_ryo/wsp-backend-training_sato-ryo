/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 工数設定ベースエンティティを表すクラスのテスト
 */
@isTest
private class TimeSettingBaseEntityTest {

	/**
	 * テストデータ
	 */
	private class EntityTestData extends TestData.TestDataEntity {}


	/**
	 * エンティティの複製ができることを確認する
	 */
	@isTest static void copyTest() {
		EntityTestData testData = new EntityTestData();
		TimeSettingBaseEntity orgEntity = testData.timeSetting;
		orgEntity.name = 'テスト工数設定';
		orgEntity.code = 'テスト工数設定001';
		orgEntity.uniqKey = testData.company.code + '-' + orgEntity.code;
		orgEntity.companyId = testData.company.id;
		orgEntity.summaryPeriod = TimeSettingBaseGeneratedEntity.SummaryPeriodType.Month;
		orgEntity.startDateOfMonth = 16;
		orgEntity.monthMark = TimeSettingBaseGeneratedEntity.MonthMarkType.BeginBase;

		TimeSettingBaseEntity copyEntity = orgEntity.copy();
		System.assertEquals(null, copyEntity.id);
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.summaryPeriod, copyEntity.summaryPeriod);
		System.assertEquals(orgEntity.startDateOfMonth, copyEntity.startDateOfMonth);
		System.assertEquals(orgEntity.monthMark, copyEntity.monthMark);
	}

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		TimeSettingBaseEntity entity = new TimeSettingBaseEntity();

		// 会社コードとジョブコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'MasterCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode, entity.code));

		// 勤務体系コードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'MasterCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode, entity.code);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}
}
