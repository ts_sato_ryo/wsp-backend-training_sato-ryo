/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for CurrencyService
 */
@isTest
private class CurrencyServiceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * Create a new Currency record(Positive case)
	 */
	@isTest
	static void createCurrencyTest() {
		CurrencyEntity entity = new  CurrencyEntity();
		setTestParam(entity);

		Test.startTest();

		// Create new record
		Repository.SaveResult result = CurrencyService.createCurrency(entity);

		Test.stopTest();

		System.assertEquals(1, result.details.size());

		List<ComCurrency__c> records =
		[SELECT
			Name,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			Code__c,
			IsoCurrencyCode__c,
			DecimalPlaces__c,
			Symbol__c
			FROM ComCurrency__c
			WHERE Id =: result.details[0].id];

		// Confirm new record was created
		System.assertEquals(1, records.size());

		System.assertEquals(entity.name, records[0].Name);
		System.assertEquals(entity.nameL1, records[0].Name_L1__c);
		System.assertEquals(entity.nameL2, records[0].Name_L2__c);
		System.assertEquals(entity.nameL0, records[0].Name_L0__c);
		System.assertEquals(entity.code, records[0].Code__c);
		System.assertEquals(entity.isoCurrencyCode.value, records[0].IsoCurrencyCode__c);
		System.assertEquals(entity.decimalPlaces, records[0].DecimalPlaces__c);
		System.assertEquals(entity.symbol, records[0].Symbol__c);
	}

	/**
   * Create a new Currency record(Negative case: Same code exists)
   */
	@isTest
	static void createCurrencyDuplicateErrTest() {

		// Create 'USD' currency first
		ComTestDataUtility.createCurrency('USD', 'US dollar');

		CurrencyEntity entity = new  CurrencyEntity();
		setTestParam(entity);

		Test.startTest();

		APP.ParameterException ex;
		try {
			// Try to create new 'USD' currency again
			CurrencyService.createCurrency(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
	   * Update Currency record(Positive case)
	   */
	@isTest
	static void updateCurrencyTest() {
		// Create a Currency record first
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		CurrencyEntity entity = new CurrencyEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		Test.startTest();

		CurrencyService.updateCurrency(entity);

		Test.stopTest();

		List<ComCurrency__c> records =
		[SELECT
			Name,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			Code__c,
			IsoCurrencyCode__c,
			DecimalPlaces__c,
			Symbol__c
			FROM ComCurrency__c
			WHERE Id =: testRecord.id];

		//Confirm the record was updated
		System.assertEquals(1, records.size());

		System.assertEquals(entity.name, records[0].Name);
		System.assertEquals(entity.nameL0, records[0].Name_L0__c);
		System.assertEquals(entity.namel1, records[0].Name_L1__c);
		System.assertEquals(entity.namel2, records[0].Name_L2__c);
		System.assertEquals(entity.code, records[0].Code__c);
		System.assertEquals(entity.isoCurrencyCode.value, records[0].IsoCurrencyCode__c);
		System.assertEquals(entity.decimalPlaces, records[0].DecimalPlaces__c);
		System.assertEquals(entity.symbol, records[0].Symbol__c);
	}

	/**
   * Update Currency record(Negative case: Update code is duplicate)
   */
	@isTest
	static void updateCurrencyDuplicateErrTest() {
		// Create Currency test records
		ComCurrency__c usdRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');
		ComCurrency__c jpyRecord = ComTestDataUtility.createCurrency('JPY', 'Japanese Yen');

		// Create USD Currency entity
		CurrencyEntity entity = new CurrencyEntity();
		setTestParam(entity);
		entity.setId(usdRecord.Id);

		// Set JPY code to USD Currency entity
		entity.code = jpyRecord.Code__c;
		entity.isoCurrencyCode = ComIsoCurrencyCode.valueOf(jpyRecord.IsoCurrencyCode__c);

		Test.startTest();

		APP.ParameterException ex;
		try {
			CurrencyService.updateCurrency(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
 * Update Currency record(Negative case: Target record is no found)
 */
	@isTest
	static void updateCurrencyNotFoundTest() {
		// Create a Currency record first
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		CurrencyEntity entity = new CurrencyEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		// Delete the record before updating
		delete testRecord;

		Test.startTest();

		APP.ParameterException ex;
		try {
			CurrencyService.updateCurrency(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm the expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_RecordNotFound, ex.getMessage());
	}

	/**
	 * Delete Currency record(Positive case)
	 */
	@isTest
	static void deleteCurrencyTest() {

		// Create the target data to delete
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		Id id = testRecord.Id;

		Test.startTest();

		// Delete the target
		CurrencyService.deleteCurrency(id);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ComCurrency__c WHERE Id =: id]);
	}

	/**
   * Delete Currency record(Positive case: Target has been deleted)
   */
	@isTest
	static void deleteCurrencyAlreadyDeletedTest() {

		// Create the target data to delete
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');

		Id id = testRecord.Id;

		// Delete the target record in advance
		delete testRecord;

		Test.startTest();

		DmlException ex = null;
		try {
			// Try to Delete the target
			CurrencyService.deleteCurrency(id);
		} catch (DmlException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm exception did not occur
		System.assertEquals(null, ex);
	}

	/**
 * Delete Currency record(Negative case: Target has been used as a base currency)
 */
	@isTest
	static void deleteBaseCurrencyTest() {

		// Create the target data to delete
		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('USD', 'US dollar');
		ComTestDataUtility.createCompany('test corp', null, testRecord.Id, false);

		Id id = testRecord.Id;

		Test.startTest();

		App.IllegalStateException ex = null;
		try {
			// Try to Delete the target
			CurrencyService.deleteCurrency(id);
		} catch (App.IllegalStateException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm exception did not occur
		System.assertEquals('ILLEGAL_STATE', ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_CannotDeleteReference, ex.getMessage());
	}

	/**
	 * Search Currency data(Positive case)
   */
	@isTest
	static void searchCurrencyTest() {

		ComCurrency__c testRecord = ComTestDataUtility.createCurrency('JPY', 'Japanese Yen');

		List<CurrencyEntity> result =  CurrencyService.searchCurrency(testRecord.Id, null);

		System.assertEquals(1, result.size());

		CurrencyEntity entity = result[0];
		System.assertEquals(testRecord.Id, entity.id);
		System.assertEquals(testRecord.Name, entity.name);
		System.assertEquals(testRecord.Name_L0__c, entity.nameL0);
		System.assertEquals(testRecord.Name_L1__c, entity.nameL1);
		System.assertEquals(testRecord.Name_L2__c, entity.nameL2);
		System.assertEquals(testRecord.DecimalPlaces__c, entity.decimalPlaces);
		System.assertEquals(testRecord.Symbol__c, entity.symbol);
	}

	/**
	 * Search all Currency data(Positive case)
   */
	@isTest
	static void searchCurrencyAllTest() {

		List<ComCurrency__c> testRecords = ComTestDataUtility.createCurrencies('JPY', 'Japanese Yen',3);
		// Fetch data order by code
		testRecords = [SELECT Id, Code__c, IsoCurrencyCode__c, Name_L0__c, Name_L1__c, Name_L2__c, DecimalPlaces__c, Symbol__c
			FROM ComCurrency__c
		ORDER BY Code__c NULLS LAST];

		Test.startTest();

		// When searching by nul ID, return all data
		List<CurrencyEntity> result =  CurrencyService.searchCurrency(null, null);

		Test.stopTest();

		// Confirm all data were fetched
		System.assertEquals(testRecords.size(), result.size());

		for (Integer i = 0, n = testRecords.size(); i < n; i++) {
			System.assertEquals(testRecords[i].Id, result[i].id);
			System.assertEquals(testRecords[i].IsoCurrencyCode__c, result[i].isoCurrencyCode.value);
			System.assertEquals(testRecords[i].Name_L0__c, result[i].nameL0);
			System.assertEquals(testRecords[i].Name_L1__c, result[i].nameL1);
			System.assertEquals(testRecords[i].Name_L2__c, result[i].nameL2);
			System.assertEquals(testRecords[i].DecimalPlaces__c, result[i].decimalPlaces);
			System.assertEquals(testRecords[i].Symbol__c, result[i].symbol);
		}
	}

	/**
   * Set test parameter to entity
   * @param param Target object to set data
   */
	static void setTestParam(CurrencyEntity entity) {
		entity.code = 'USD';
		entity.isoCurrencyCode = ComIsoCurrencyCode.valueOf('USD');
		entity.name = 'United States dollar';
		entity.nameL0 = 'United States dollar';
		entity.nameL1 = '米ドル';
		entity.nameL2 = '美元';
		entity.decimalPlaces = 2;
		entity.symbol = '$';
	}

}