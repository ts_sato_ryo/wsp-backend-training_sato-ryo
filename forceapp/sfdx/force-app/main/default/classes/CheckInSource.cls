/**
 * チェックイン打刻元の定義
 * この値オブジェクトは廃止になりました。LocationSource を使用してください。
 * This value object has been deprecated. Use LocationSource instead.
 */
public with sharing class CheckInSource {
}