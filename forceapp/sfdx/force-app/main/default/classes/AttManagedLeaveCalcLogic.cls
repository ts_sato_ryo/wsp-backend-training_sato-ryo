/**
 * 休暇計算ロジック
 */
public with sharing class AttManagedLeaveCalcLogic {
	public class DaysNotEnoughException extends Exception {
		public decimal daysNotEnough {get; private set;}
		public DaysNotEnoughException (Decimal daysNotEnough) {
			this.daysNotEnough = daysNotEnough;
		}
	}
	// 有休付与
	public class GrantedEntity implements Comparable {
		// 付与Id
		public Id id {get; set;}
		// 利用開始日
		public Date validDateFrom {get; set;}
		// 失効日
		public Date validDateTo {get; set;}
		// 付与日数(調整後)
		public Decimal daysGranted {get;
			set {
				this.daysGranted = value;
				this.daysLeft = value;
			}
		}
		// 付与日数(当初)
		public Decimal originalDaysGranted {get; set;}
		// 残日数
		public Decimal daysLeft {get; set;}
		// 消費日数
		public Decimal daysTaken {get; set;}
		// 失効日数
		public Decimal daysExpired {get; set;}
		// 調整日数
		public Decimal daysAdjusted {get; set;}
		// 日数調整情報一覧
		public List<AdjustedEntity> adjustedList {get; set;}

		public GrantedEntity() {
			this.daysGranted = 0;
			this.originalDaysGranted = 0;
			this.daysLeft = 0;
			this.daysExpired = 0;
			this.daysTaken = 0;
			this.daysAdjusted = 0;
			this.adjustedList = new List<AdjustedEntity>();
		}
		public GrantedEntity(AttManagedLeaveGrantEntity grantData) {
			this.id = grantData.id;
			this.daysGranted = AppConverter.decValue(grantData.daysGranted);
			this.originalDaysGranted = AppConverter.decValue(grantData.originalDaysGranted);
			this.daysLeft = AppConverter.decValue(grantData.daysLeft);
			this.validDateFrom = AppConverter.dateValue(grantData.validDateFrom);
			this.validDateTo = AppConverter.dateValue(grantData.validDateTo);
			this.daysExpired = 0;
			this.daysTaken = 0;
			this.daysAdjusted = 0;
			this.adjustedList = new List<AdjustedEntity>();
			if (grantData.adjustList != null) {
				for (AttManagedLeaveGrantEntity.GrantAdjustEntity adjustData : grantData.adjustList) {
					this.adjustedList.add(new AdjustedEntity(adjustData));
				}
			}
		}

		/**
		 * 失効日の昇順、開始日の昇順で判定する
		 */
		public Integer compareTo(Object compareTo) {
			GrantedEntity compareData = (GrantedEntity)compareTo;
			if (this.validDateTo == compareData.validDateTo) {
				if (this.validDateFrom < compareData.validDateFrom) {
					return App.COMPARE_BEFORE;
				}
				else if (this.validDateFrom > compareData.validDateFrom) {
					return App.COMPARE_AFTER;
				}
				return App.COMPARE_EQUAL;
			}
			// 失効日が早い有休付与データが先頭
			else if (this.validDateTo < compareData.validDateTo) {
				return App.COMPARE_BEFORE;
			}
			return App.COMPARE_AFTER;
		}
	}
	// 有休付与サマリ
	public class GrantedSummaryEntity {
		// 付与Id
		public Id grantedId {get; set;}
		// 残日数
		public Decimal daysLeft {get; set;}
		// 消費日数
		public Decimal daysTaken {get; set;}
		// 失効日数
		public Decimal daysExpired {get; set;}
		// 調整日数
		public Decimal daysAdjusted {get; set;}

		public GrantedSummaryEntity() {
			this.daysLeft = 0;
			this.daysTaken = 0;
			this.daysExpired = 0;
			this.daysAdjusted = 0;
		}
	}
	// 有休付与調整
	public class AdjustedEntity implements Comparable {
		// 調整日(休暇サマリ特定用)
		public Date adjustDate {get; private set;}
		// 調整日数(プラス・マイナス)
		public Decimal daysAdjusted {get; private set;}

		public AdjustedEntity(Date adjustDate, Decimal daysAdjusted) {
			this.adjustDate = adjustDate;
			this.daysAdjusted = daysAdjusted;
		}
		public AdjustedEntity(AttManagedLeaveGrantEntity.GrantAdjustEntity adjustData) {
			this.adjustDate = AppConverter.dateValue(adjustData.adjustDate);
			this.daysAdjusted = AppConverter.decValue(adjustData.daysAdjusted);
		}
		public Integer compareTo(Object compareTo) {
			AdjustedEntity compareData = (AdjustedEntity)compareTo;
			if (this.adjustDate == compareData.adjustDate) {
				return App.COMPARE_EQUAL;
			}
			else if (this.adjustDate > compareData.adjustDate) {
				return App.COMPARE_AFTER;
			}
			return App.COMPARE_BEFORE;
		}
	}
	// 有休消費
	// 開始日と終了日に除外日がある場合、2件分ける必要がある
	public class TakenEntity implements Comparable {
		// 開始日
		public Date startDate {get; set;}
		// 終了日
		public Date endDate {get; set;}
		// 消費日数
		public Decimal daysTaken {get; set;}
		public TakenEntity() {
			daysTaken = 0;
		}
		public TakenEntity(Date startDate, Date endDate, Decimal daysTaken) {
			this.startDate = startDate;
			this.endDate = endDate;
			this.daysTaken = daysTaken;
		}
		public TakenEntity(AttManagedLeaveTakeEntity takenData) {
			this.startDate = AppConverter.dateValue(takenData.startDate);
			this.endDate = AppConverter.dateValue(takenData.endDate);
			this.daysTaken = AppConverter.decValue(takenData.daysTaken);
		}
		/**
		 * 開始日の昇順を判定する
		 */
		public Integer compareTo(Object compareTo) {
			TakenEntity compareData = (TakenEntity)compareTo;
			if (this.startDate == compareData.startDate) {
				return App.COMPARE_EQUAL;
			}
			else if (this.startDate > compareData.startDate) {
				return App.COMPARE_AFTER;
			}
			return App.COMPARE_BEFORE;
		}
	}
	// 有休サマリ
	public class SummaryEntity implements Comparable {
		public Id id {get;set;}
		// サマリ期間(勤怠サマリと同じ)
		public Date startDate {get; set;}
		public Date endDate {get; set;}
		// 残日数
		public Decimal daysLeftTotal {get; set;}
		// 消費日数
		public Decimal daysTakenTotal {get; set;}
		// 失効日数
		public Decimal daysExpiredTotal {get; set;}
		// 調整日数
		public Decimal daysAdjustedTotal {get; set;}
		// 休暇付与サマリ一覧
		public List<GrantedSummaryEntity> grantedSummaryList {get; set;}

		public SummaryEntity() {}
		public SummaryEntity(AttManagedLeaveSummaryEntity summaryData) {
			this.id = summaryData.id;
			this.startDate = AppConverter.dateValue(summaryData.startDate);
			this.endDate = AppConverter.dateValue(summaryData.endDate);
		}
		public Integer compareTo(Object compareTo) {
			SummaryEntity compareData = (SummaryEntity)compareTo;
			if (this.startDate == compareData.startDate) {
				return App.COMPARE_EQUAL;
			}
			else if (this.startDate > compareData.startDate) {
				return App.COMPARE_AFTER;
			}
			return App.COMPARE_BEFORE;
		}
	}
	// 有休消費日単位(内容有休消費残日数判定用)
	private class DateTakenEntity  implements Comparable {
		// 消費日
		public Date takenDate {get; set;}
		// 消費日数
		public Decimal takenDay {get; set;}
		// 消費可能
		public Boolean isTakenOK {get; set;}
		public DateTakenEntity (Date dt, Decimal taken) {
			this.takenDate = dt;
			this.takenDay = taken;
			this.isTakenOK = false;
		}
		/**
		 * 消費日の昇順を定義する
		 */
		public Integer compareTo(Object compareTo) {
			DateTakenEntity compareData = (DateTakenEntity)compareTo;
			if (this.takenDate == compareData.takenDate) {
				return App.COMPARE_EQUAL;
			}
			else if (this.takenDate > compareData.takenDate) {
				return App.COMPARE_AFTER;
			}
			return App.COMPARE_BEFORE;
		}
	}
	/*
	 * 未確定の休暇サマリを計算する
	 * @param unfixedSummaryList 計算対象の未確定休暇サマリ一覧
	 * @param grantedList 計算対象期間中有効な休暇付与一覧
	 * @param takenList 計算対象期間中消費した休暇
	 */
	public static List<SummaryEntity> calcUnfixedSummaryList(
			List<SummaryEntity> unfixedSummaryList,
			List<GrantedEntity> grantedList,
			List<TakenEntity> takenList) {

		System.debug(LoggingLevel.DEBUG, '---AttPaidLeaveCalcLogic.calcUnfixedSummaryList start');
		// 日付順で未確定休暇サマリの計算が必要
		unfixedSummaryList.sort();
		System.debug(LoggingLevel.DEBUG, '---unfixedSummaryList is ' + unfixedSummaryList);
		System.debug(LoggingLevel.DEBUG, '---grantedList is ' + grantedList);
		System.debug(LoggingLevel.DEBUG, '---takenList is ' + takenList);
		// 既に失効した付与を除外
		for (Integer i = grantedList.size() - 1;  i>= 0; i--) {
			if (grantedList[i].daysLeft - grantedList[i].daysExpired <= 0) {
				grantedList.remove(i);
			}
		}

		Map<Date, List<TakenEntity>> summaryTakenListMap = createSummaryTakenMap(unfixedSummaryList, takenList);
		Map<Date, List<GrantedEntity>> summaryGrantedListMap = createSummaryGrantedMap(unfixedSummaryList, grantedList);
		System.debug(LoggingLevel.DEBUG, '---summaryTakenListMap is ' + summaryTakenListMap);
		System.debug(LoggingLevel.DEBUG, '---summaryGrantedListMap is ' + summaryGrantedListMap);
		// 休暇サマリごとに残日数＋消費日数を計算する
		for (SummaryEntity summaryData : unfixedSummaryList) {
			List<GrantedEntity> summaryGrantedList = summaryGrantedListMap.get(summaryData.startDate);
			summaryGrantedList.sort();
			List<TakenEntity> summaryTakenList = summaryTakenListMap.get(summaryData.startDate);

			// 休暇サマリ、休暇付与を計算する
			calcSummary(summaryData, summaryGrantedList, summaryTakenList);
			// 計算した休暇サマリに休暇付与サマリをセットする
			summaryData.grantedSummaryList = new List<GrantedSummaryEntity>();
			System.debug(LoggingLevel.DEBUG, '---summaryGrantedList after calcSummary is ' + summaryGrantedList);
			// 計算した休暇付与から休暇付与サマリを新規に作成する
			for (GrantedEntity grantedData : summaryGrantedList) {
				GrantedSummaryEntity grantedSummaryData = new GrantedSummaryEntity();
				grantedSummaryData.grantedId = grantedData.id;
				grantedSummaryData.daysTaken = grantedData.daysTaken;
				grantedSummaryData.daysLeft = grantedData.daysLeft;
				grantedSummaryData.daysExpired = grantedData.daysExpired;
				grantedSummaryData.daysAdjusted = grantedData.daysAdjusted;

				summaryData.grantedSummaryList.add(grantedSummaryData);
			}
			System.debug(LoggingLevel.DEBUG, '---summaryData.grantedSummaryListy is ' + summaryData.grantedSummaryList);
		}
		System.debug(LoggingLevel.DEBUG, '---AttPaidLeaveCalcLogic.calcUnfixedSummaryList end');
		return unfixedSummaryList;
	}

	/**
	 * 休暇付与と休暇消費から取得可能な休暇日数を計算して返却する
	 * @param targetDate 残日数計算日
	 * @param grantedList 休暇付与一覧(残日数が無くても失効していても問題ない)
	 * @param takenList 勤務確定していない全ての月の消費一覧（申請中/承認済みを区別しない）
	 * @return 残日数(取得可能な休暇日数)
	 */
	public static Decimal calcDaysLeft(
			Date targetDate,
			List<GrantedEntity> grantedList,
			List<TakenEntity> takenList) {

		System.debug(LoggingLevel.DEBUG, '---AttPaidLeaveCalcLogic.calcDaysLeft start');

		// 既存の消費と付与をマッピングする
		matchTakenToGranted(takenList, grantedList);
		// 既存の消費をマッピング後の残日数を取得
		Decimal daysLeftTotal = 0;
		for (GrantedEntity grantedData : grantedList) {
			if (grantedData.validDateFrom <= targetDate
					&& grantedData.validDateTo > targetDate) {
				daysLeftTotal += grantedData.daysLeft;
			}
		}
		return daysLeftTotal;
	}

	/**
	 * 休暇付与の残日数と取得日数を計算して更新する
	 * どの休暇付与から休暇を消費するかを判定して、休暇付与の残日数と取得日数を計算する
	 * (先に失効する休暇付与から消費する)
	 * @param takenList 休暇消費一覧
	 * @param grantedList 休暇付与一覧
	 * @throws DaysNotEnoughException 消費できる休暇付与がない場合
	 */
	public static void matchTakenToGranted(List<TakenEntity> takenList, List<GrantedEntity> grantedList) {
		System.debug(LoggingLevel.DEBUG, '---AttManagedLeaveCalcLogic.matchTakenToGranted start');
		// 休暇付与一覧のデータをマッピング用に最適化する
		for (Integer i = grantedList.size() - 1;  i>= 0; i--) {
			// 消費日数を削除
			grantedList[i].daysTaken = 0;
			// 失効済み付与日数を除外して残日数が0以下になる場合、休暇付与一覧から除外
			if (grantedList[i].daysLeft - grantedList[i].daysExpired <= 0) {
				grantedList.remove(i);
			}
		}

		grantedList.sort();
		takenList.sort();

		// 休暇消費一覧の合計消費日数
		Decimal daysNeedTake = 0;
		for (TakenEntity takenData : takenList) {
			daysNeedTake += takenData.daysTaken;
		}

		System.debug(LoggingLevel.DEBUG, '---daysNeedTake is ' + daysNeedTake);
		// 取得できない有休残日数の端数
		Decimal daysLeftFraction = 0;
		if (daysNeedTake > 0) {
			List<DateTakenEntity> takenByDateList = splitTakenByDate(takenList);
			System.debug(LoggingLevel.DEBUG, '---takenByDateList is ' + takenByDateList);
			for (GrantedEntity grantedData : grantedList) {
				System.debug(LoggingLevel.DEBUG, '---grantedData is ' + grantedData);
				// 失効日までの消費日数を計算
				Decimal takeDays = 0;
				for (DateTakenEntity takenByDate : takenByDateList) {
					System.debug(LoggingLevel.DEBUG, '---current takenByDate is ' + takenByDate);
					// 失効以前の消費に付与を取得する
					if (!takenByDate.isTakenOK
							&& takenByDate.takenDate >= grantedData.validDateFrom
							&& takenByDate.takenDate < grantedData.validDateTo) {
						System.debug(LoggingLevel.DEBUG, '--- current taken is fit granted data');
						// 残日数足りる場合
						if (takenByDate.takenDay <= grantedData.daysLeft) {
							takeDays += takenByDate.takenDay;
							takenByDate.isTakenOK = true;
							grantedData.daysLeft -= takenByDate.takenDay;
						}
						// 残日数全部足りない場合,残日数分のみ消費し、付与データのマッピングを終了
						else if (grantedData.daysLeft > 0) {
							takenByDate.takenDay -= grantedData.daysLeft;
							takeDays += grantedData.daysLeft;
							grantedData.daysLeft = 0;
							break;
						}
						// 残日数足りない場合、付与データのマッピングを終了
						else {
							break;
						}
					}
				}
				System.debug(LoggingLevel.DEBUG, '---takeDays from grandted is ' + takeDays);
				// 休暇付与の取得日数を更新
				grantedData.daysTaken += takeDays;
				System.debug(LoggingLevel.DEBUG, '---daysLeft after taken is ' + grantedData.daysLeft);
				// 取得合計日数を更新
				daysNeedTake -= takeDays;
				System.debug(LoggingLevel.DEBUG, '---daysNeedTake after match is ' + daysNeedTake);
				if (daysNeedTake == 0) {
					break;
				}
			}
		}
		// 既に消費日数がオーバーした場合、エラー
		if (daysNeedTake > 0) {
			throw new DaysNotEnoughException(daysNeedTake.stripTrailingZeros());
		}
		System.debug(LoggingLevel.DEBUG, '---AttManagedLeaveCalcLogic.matchTakenToGranted end');
	}

	/**
	 * 休暇サマリと休暇サマリの期間内に存在する休暇消費をマッピングする
	 * @param summaryList 休暇サマリ
	 * @param takenList 休暇消費
	 * @return 休暇サマリの開始日と休暇サマリの期間内に存在する休暇消費のマップ
	 */
	private static Map<Date, List<TakenEntity>> createSummaryTakenMap(List<SummaryEntity> summaryList, List<TakenEntity> takenList) {
		System.debug(LoggingLevel.DEBUG, '---AttManagedLeaveCalcLogic.createSummaryTakenMap start');
		System.debug(LoggingLevel.DEBUG, '---summaryList is ' + summaryList);
		System.debug(LoggingLevel.DEBUG, '---takenList is ' + takenList);
		// 休暇サマリの期間内に存在する休暇消費をマップにする
		Map<Date, List<TakenEntity>> retMap = new Map<Date, List<takenEntity>>();
		for (SummaryEntity summaryData : summaryList) {
			retMap.put(summaryData.startDate, new List<TakenEntity>());
			for (TakenEntity takenData : takenList) {
				if (takenData.startDate <= summaryData.endDate
						&& takenData.endDate >= summaryData.startDate) {
					retMap.get(summaryData.startDate).add(takenData);
				}
			}
		}
		System.debug(LoggingLevel.DEBUG, '---retMap is ' + retMap);
		// FIXME:サマリとマッピングできない消費がある場合、エラー
		System.debug(LoggingLevel.DEBUG, '---AttManagedLeaveCalcLogic.createSummaryTakenMap end');
		return retMap;
	}

	/**
	 * 休暇サマリと休暇サマリの期間内に存在する休暇付与をマッピングする
	 * @param summaryList 休暇サマリ
	 * @param grantedList 休暇付与
	 *
	 */
	private static Map<Date, List<GrantedEntity>> createSummaryGrantedMap(List<SummaryEntity> summaryList, List<GrantedEntity> grantedList) {
		// 休暇サマリの期間内に存在する休暇付与をマップにする
		Map<Date, List<GrantedEntity>> retMap = new Map<Date, List<GrantedEntity>>();
		for (SummaryEntity summaryData : summaryList) {
			retMap.put(summaryData.startDate, new List<GrantedEntity>());
			for (GrantedEntity grantedData : grantedList) {
				if (grantedData.validDateFrom <= summaryData.endDate
						&& grantedData.validDateTo > summaryData.startDate) {
					retMap.get(summaryData.startDate).add(grantedData);
				}
			}
		}
		return retMap;
	}
	/**
	 * 休暇消費一覧を1日単位の休暇消費に分割する
	 * @param takenList 休暇消費一覧
	 * @return 1日単位の休暇消費データ
	 */
	private static List<DateTakenEntity> splitTakenByDate(List<TakenEntity> takenList) {
		List<DateTakenEntity> retList = new List<DateTakenEntity>();
		for (TakenEntity takenData : takenList) {
			// 1日の場合、消費日数を記録(1,0.5,0.xxx)
			if (takenData.startDate == takenData.endDate) {
				retList.add(new DateTakenEntity(takenData.startDate, takenData.daysTaken));
			}
			// 2日以上の場合、毎日1日消費を固定(現仕様)
			else {
				Date dt = takenData.startDate;
				while (dt <= takenData.endDate) {
					retList.add(new DateTakenEntity(dt, 1));
					dt = dt.addDays(1);
				}
			}
		}
		retList.sort();
		return retList;
	}


	/*
	 * 休暇サマリを計算する
	 * ・休暇サマリの計算項目：合計消費日数、合計残日数、合計失効日数、合計増減日数
	 * ・副作用として休暇付与の項目も計算されている
	 * @param summaryData 計算する休暇サマリ
	 * @param summaryGrantedList 休暇サマリ期間中有効な休暇付与一覧
	 * @param summaryTakenList 休暇サマリ期間中に消費した休暇消費
	 */
	private static void calcSummary(
			SummaryEntity summaryData,
			List<GrantedEntity> summaryGrantedList,
			List<TakenEntity> summaryTakenList) {
		System.debug(LoggingLevel.DEBUG, '---AttPaidLeaveCalcLogic.calcSummary start');
		System.debug(LoggingLevel.DEBUG, '---summaryData is ' + summaryData);
		System.debug(LoggingLevel.DEBUG, '---summaryGrantedList is ' + summaryGrantedList);
		System.debug(LoggingLevel.DEBUG, '---summaryTakenList is ' + summaryTakenList);

		// 残日数合計
		Decimal daysLeftTotal = 0;
		for (GrantedEntity grantedData : summaryGrantedList) {
			daysLeftTotal += grantedData.daysLeft;
		}
		// 消費日数合計
		Decimal daysTakenTotal = 0;
		for (TakenEntity takenData : summaryTakenList) {
			daysTakenTotal += takenData.daysTaken;
		}
		// 休暇サマリの増減日数を計算する
		Decimal daysAdjustedTotal = 0;
		for (GrantedEntity grantedData : summaryGrantedList) {
			// 休暇サマリの期間中に付与された休暇付与の当初日数を集計して、増減日数に加算する
			if (grantedData.validDateFrom >= summaryData.startDate && grantedData.validDateFrom <= summaryData.endDate) {
				daysAdjustedTotal += grantedData.originalDaysGranted;
			}
			// 休暇サマリの期間中に調整された休暇付与調整の日数を集計して、増減日数に加算する
			grantedData.daysAdjusted = 0;
			for (AdjustedEntity adjustedData : grantedData.adjustedList) {
				if (adjustedData.adjustDate >= summaryData.startDate && adjustedData.adjustDate <= summaryData.endDate) {
					grantedData.daysAdjusted += adjustedData.daysAdjusted;
					daysAdjustedTotal += adjustedData.daysAdjusted;
				}
			}
		}
		// 休暇付与の残日数と取得日数を計算して更新する
		matchTakenToGranted(summaryTakenList, summaryGrantedList);

		// 休暇サマリの失効日数を計算する
		Decimal daysExpiredTotal = 0;
		for (GrantedEntity grantedData : summaryGrantedList) {
			grantedData.daysExpired = 0;
			// 休暇サマリの期間中に失効する休暇付与の残日数を集計して、失効日数に加算する
			if (grantedData.validDateTo > summaryData.startDate && grantedData.validDateTo <= summaryData.endDate.addDays(1)) {
				grantedData.daysExpired = grantedData.daysLeft;
				daysExpiredTotal += grantedData.daysLeft;
			}
		}

		// サマリに記録
		summaryData.daysTakenTotal = daysTakenTotal;
		summaryData.daysLeftTotal = daysLeftTotal - daysTakenTotal;
		summaryData.daysExpiredTotal = daysExpiredTotal;
		summaryData.daysAdjustedTotal = daysAdjustedTotal;

		System.debug(LoggingLevel.DEBUG, '---summaryData is ' + summaryData);
		System.debug(LoggingLevel.DEBUG, '---AttPaidLeaveCalcLogic.calcSummary end');
	}
}