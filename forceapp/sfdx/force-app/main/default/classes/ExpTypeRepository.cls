/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費
 *
 * @description 費目グループマスタのリポジトリ
 */
public with sharing class ExpTypeRepository extends ValidPeriodRepositoryOld {

	/**
	 * リストを取得する際の並び順
	 */
	public enum SortOrder {
		CODE_ASC, CODE_DESC, ORDER_ASC, ORDER_DESC
	}

	/** 検索で取得する最大レコード数 */
	private static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/** 更新用でクエリを実行しない */
	private final static Boolean NOT_FOR_UPDATE = false;

	/** Number of Extended Items for each type */
	private static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/** String representation of Extended Item Field of the sObject */
	@TestVisible
	private static final String EXTENDED_ITEM_TEXT = ExpType__c.ExtendedItemText01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_PICKLIST = ExpType__c.ExtendedItemPicklist01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_LOOKUP = ExpType__c.ExtendedItemLookup01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_DATE = ExpType__c.ExtendedItemDate01TextId__c.getDescribe().getName().substringBefore('01');
	@TestVisible
	private static final String EXTENDED_ITEM_ID = 'TextId__c';
	@TestVisible
	private static final String EXTENDED_ITEM_USED_IN = 'UsedIn__c';
	@TestVisible
	private static final String EXTENDED_ITEM_REQUIRED_FOR = 'RequiredFor__c';

	/** 親費目グループのリレーション名 */
	private static final String PARENT_R = ExpType__c.ParentGroupId__c.getDescribe().getRelationshipName();

	/** Pattern for detecting characters to unescape */
	private static final Pattern SPECIAL_CHARACTERS_PATTERN = Pattern.compile('[%\\_\\\\]');

	/**
	 * 取得対象の項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				ExpType__c.Id,
				ExpType__c.Name,
				ExpType__c.Code__c,
				ExpType__c.UniqKey__c,
				ExpType__c.Name_L0__c,
				ExpType__c.Name_L1__c,
				ExpType__c.Name_L2__c,
				ExpType__c.CompanyId__c,
				ExpType__c.ParentGroupId__c,
				ExpType__c.Description_L0__c,
				ExpType__c.Description_L1__c,
				ExpType__c.Description_L2__c,
				ExpType__c.FileAttachment__c,
				ExpType__c.RecordType__c,
				ExpType__c.Usage__c,
				ExpType__c.TaxTypeBase1Id__c,
				ExpType__c.TaxTypeBase2Id__c,
				ExpType__c.TaxTypeBase3Id__c,
				ExpType__c.ExtendedItemDate01TextId__c,
				ExpType__c.ExtendedItemDate01UsedIn__c,
				ExpType__c.ExtendedItemDate01RequiredFor__c,
				ExpType__c.ExtendedItemDate02TextId__c,
				ExpType__c.ExtendedItemDate02UsedIn__c,
				ExpType__c.ExtendedItemDate02RequiredFor__c,
				ExpType__c.ExtendedItemDate03TextId__c,
				ExpType__c.ExtendedItemDate03UsedIn__c,
				ExpType__c.ExtendedItemDate03RequiredFor__c,
				ExpType__c.ExtendedItemDate04TextId__c,
				ExpType__c.ExtendedItemDate04UsedIn__c,
				ExpType__c.ExtendedItemDate04RequiredFor__c,
				ExpType__c.ExtendedItemDate05TextId__c,
				ExpType__c.ExtendedItemDate05UsedIn__c,
				ExpType__c.ExtendedItemDate05RequiredFor__c,
				ExpType__c.ExtendedItemDate06TextId__c,
				ExpType__c.ExtendedItemDate06UsedIn__c,
				ExpType__c.ExtendedItemDate06RequiredFor__c,
				ExpType__c.ExtendedItemDate07TextId__c,
				ExpType__c.ExtendedItemDate07UsedIn__c,
				ExpType__c.ExtendedItemDate07RequiredFor__c,
				ExpType__c.ExtendedItemDate08TextId__c,
				ExpType__c.ExtendedItemDate08UsedIn__c,
				ExpType__c.ExtendedItemDate08RequiredFor__c,
				ExpType__c.ExtendedItemDate09TextId__c,
				ExpType__c.ExtendedItemDate09UsedIn__c,
				ExpType__c.ExtendedItemDate09RequiredFor__c,
				ExpType__c.ExtendedItemDate10TextId__c,
				ExpType__c.ExtendedItemDate10UsedIn__c,
				ExpType__c.ExtendedItemDate10RequiredFor__c,
				ExpType__c.ExtendedItemLookup01TextId__c,
				ExpType__c.ExtendedItemLookup01UsedIn__c,
				ExpType__c.ExtendedItemLookup01RequiredFor__c,
				ExpType__c.ExtendedItemLookup02TextId__c,
				ExpType__c.ExtendedItemLookup02UsedIn__c,
				ExpType__c.ExtendedItemLookup02RequiredFor__c,
				ExpType__c.ExtendedItemLookup03TextId__c,
				ExpType__c.ExtendedItemLookup03UsedIn__c,
				ExpType__c.ExtendedItemLookup03RequiredFor__c,
				ExpType__c.ExtendedItemLookup04TextId__c,
				ExpType__c.ExtendedItemLookup04UsedIn__c,
				ExpType__c.ExtendedItemLookup04RequiredFor__c,
				ExpType__c.ExtendedItemLookup05TextId__c,
				ExpType__c.ExtendedItemLookup05UsedIn__c,
				ExpType__c.ExtendedItemLookup05RequiredFor__c,
				ExpType__c.ExtendedItemLookup06TextId__c,
				ExpType__c.ExtendedItemLookup06UsedIn__c,
				ExpType__c.ExtendedItemLookup06RequiredFor__c,
				ExpType__c.ExtendedItemLookup07TextId__c,
				ExpType__c.ExtendedItemLookup07UsedIn__c,
				ExpType__c.ExtendedItemLookup07RequiredFor__c,
				ExpType__c.ExtendedItemLookup08TextId__c,
				ExpType__c.ExtendedItemLookup08UsedIn__c,
				ExpType__c.ExtendedItemLookup08RequiredFor__c,
				ExpType__c.ExtendedItemLookup09TextId__c,
				ExpType__c.ExtendedItemLookup09UsedIn__c,
				ExpType__c.ExtendedItemLookup09RequiredFor__c,
				ExpType__c.ExtendedItemLookup10TextId__c,
				ExpType__c.ExtendedItemLookup10UsedIn__c,
				ExpType__c.ExtendedItemLookup10RequiredFor__c,
				ExpType__c.ExtendedItemPicklist01TextId__c,
				ExpType__c.ExtendedItemPicklist01UsedIn__c,
				ExpType__c.ExtendedItemPicklist01RequiredFor__c,
				ExpType__c.ExtendedItemPicklist02TextId__c,
				ExpType__c.ExtendedItemPicklist02UsedIn__c,
				ExpType__c.ExtendedItemPicklist02RequiredFor__c,
				ExpType__c.ExtendedItemPicklist03TextId__c,
				ExpType__c.ExtendedItemPicklist03UsedIn__c,
				ExpType__c.ExtendedItemPicklist03RequiredFor__c,
				ExpType__c.ExtendedItemPicklist04TextId__c,
				ExpType__c.ExtendedItemPicklist04UsedIn__c,
				ExpType__c.ExtendedItemPicklist04RequiredFor__c,
				ExpType__c.ExtendedItemPicklist05TextId__c,
				ExpType__c.ExtendedItemPicklist05UsedIn__c,
				ExpType__c.ExtendedItemPicklist05RequiredFor__c,
				ExpType__c.ExtendedItemPicklist06TextId__c,
				ExpType__c.ExtendedItemPicklist06UsedIn__c,
				ExpType__c.ExtendedItemPicklist06RequiredFor__c,
				ExpType__c.ExtendedItemPicklist07TextId__c,
				ExpType__c.ExtendedItemPicklist07UsedIn__c,
				ExpType__c.ExtendedItemPicklist07RequiredFor__c,
				ExpType__c.ExtendedItemPicklist08TextId__c,
				ExpType__c.ExtendedItemPicklist08UsedIn__c,
				ExpType__c.ExtendedItemPicklist08RequiredFor__c,
				ExpType__c.ExtendedItemPicklist09TextId__c,
				ExpType__c.ExtendedItemPicklist09UsedIn__c,
				ExpType__c.ExtendedItemPicklist09RequiredFor__c,
				ExpType__c.ExtendedItemPicklist10TextId__c,
				ExpType__c.ExtendedItemPicklist10UsedIn__c,
				ExpType__c.ExtendedItemPicklist10RequiredFor__c,
				ExpType__c.ExtendedItemText01TextId__c,
				ExpType__c.ExtendedItemText01UsedIn__c,
				ExpType__c.ExtendedItemText01RequiredFor__c,
				ExpType__c.ExtendedItemText02TextId__c,
				ExpType__c.ExtendedItemText02UsedIn__c,
				ExpType__c.ExtendedItemText02RequiredFor__c,
				ExpType__c.ExtendedItemText03TextId__c,
				ExpType__c.ExtendedItemText03UsedIn__c,
				ExpType__c.ExtendedItemText03RequiredFor__c,
				ExpType__c.ExtendedItemText04TextId__c,
				ExpType__c.ExtendedItemText04UsedIn__c,
				ExpType__c.ExtendedItemText04RequiredFor__c,
				ExpType__c.ExtendedItemText05TextId__c,
				ExpType__c.ExtendedItemText05UsedIn__c,
				ExpType__c.ExtendedItemText05RequiredFor__c,
				ExpType__c.ExtendedItemText06TextId__c,
				ExpType__c.ExtendedItemText06UsedIn__c,
				ExpType__c.ExtendedItemText06RequiredFor__c,
				ExpType__c.ExtendedItemText07TextId__c,
				ExpType__c.ExtendedItemText07UsedIn__c,
				ExpType__c.ExtendedItemText07RequiredFor__c,
				ExpType__c.ExtendedItemText08TextId__c,
				ExpType__c.ExtendedItemText08UsedIn__c,
				ExpType__c.ExtendedItemText08RequiredFor__c,
				ExpType__c.ExtendedItemText09TextId__c,
				ExpType__c.ExtendedItemText09UsedIn__c,
				ExpType__c.ExtendedItemText09RequiredFor__c,
				ExpType__c.ExtendedItemText10TextId__c,
				ExpType__c.ExtendedItemText10UsedIn__c,
				ExpType__c.ExtendedItemText10RequiredFor__c,
				ExpType__c.DebitAccountCode__c,
				ExpType__c.DebitAccountName__c,
				ExpType__c.DebitSubAccountCode__c,
				ExpType__c.DebitSubAccountName__c,
				ExpType__c.Order__c,
				ExpType__c.ValidFrom__c,
				ExpType__c.ValidTo__c,
				ExpType__c.UseForeignCurrency__c,
				ExpType__c.FixedForeignCurrencyId__c,
				ExpType__c.FixedAllowanceSingleAmount__c
		};

		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}


	/**
	 * 指定したIDのエンティティを取得する
	 * @param id 取得対象のID
	 * @return 指定したIDのエンティティ、ただし存在しない場合はnull
	 */
	public ExpTypeEntity getEntity(Id id) {
		List<ExpTypeEntity> entityList = getEntityList(new List<Id>{id});
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定したエンティティを取得する
	 * @param ids 取得対象のIDのリスト
	 * @return 指定したIDのエンティティのリスト、ただし存在しない場合はnull
	 */
	public List<ExpTypeEntity> getEntityList(List<Id> ids) {
		ExpTypeRepository.SearchFilter filter = new SearchFilter();
		filter.ids = ids == null ? null : new Set<Id>(ids);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * Get entity map
	 * @param ids Target id list
	 * @return Map of entity. The key is id.
	 */
	public Map<Id, ExpTypeEntity> getEntityMap(List<Id> ids) {
		Map<Id, ExpTypeEntity> expTypeMap = new Map<Id, ExpTypeEntity>();
		for (ExpTypeEntity expType : getEntityList(ids)) {
			expTypeMap.put(expType.Id, expType);
		}
		return expTypeMap;
	}

	/**
	 * 指定したコードを持つエンティティを取得する
	 * @param code 費目コード
	 * @param companyId 会社ID
	 * @return 指定したコードを持つエンティティ、存在しない場合はnull
	 */
	public ExpTypeEntity getEntityByCode(String code, Id companyId) {
		ExpTypeRepository.SearchFilter filter = new SearchFilter();
		filter.codes = new Set<String>{code};
		filter.companyIds = new Set<Id>{companyId};

		List<ExpTypeEntity> entityList = searchEntity(filter);
		if (entityList.isEmpty()) {
			return null;
		}
		return entityList[0];
	}

	/**
	 * 指定した親費目グループIDを持つエンティティを取得する
	 * @param parentGroupIds 親費目グループIDのリスト
	 * @return 指定した親費目グループIDを持つエンティティ、存在しない場合はnull
	 */
	public List<ExpTypeEntity> getEntityListByParentGroupId(List<Id> parentGroupIds) {
		ExpTypeRepository.SearchFilter filter = new SearchFilter();
		filter.parentGroupIds = parentGroupIds == null ? null : new Set<Id>(parentGroupIds);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.ORDER_ASC);
	}

	/**
	 * 指定した拡張項目IDを持つエンティティを取得する
	 * @param extendedItemIds 拡張項目IDのリスト
	 * @return 指定した拡張項目IDを持つエンティティ、存在しない場合はnull
	 */
	public List<ExpTypeEntity> getEntityListByExtendedItemId(List<Id> extendedItemIds) {
		ExpTypeRepository.SearchFilter filter = new SearchFilter();
		filter.extendedItemIds = extendedItemIds == null ? null : new Set<Id>(extendedItemIds);

		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.ORDER_ASC);
	}

	/** 検索フィルタ */
	public class SearchFilter {
		/** 費目ID */
		public Set<Id> ids;
		/** Expense Type Name (partially match) */
		public String name;
		/** 会社ID */
		public Set<Id> companyIds;
		/** Expense Type Code (partially match) */
		public String code;
		/** コード(完全一致) */
		public Set<String> codes;
		/** Accounting Period End Date */
		public AppDate endDate;
		/** Accounting Period Start Date */
		public AppDate startDate;
		/** 取得対象日 */
		public AppDate targetDate;
		/** Expense Type Valid From Date */
		public AppDate validFrom;
		/** Expense Type Validate To Date (Excluding Last Date as at the ValidTo Date, it's considered as already "expired") */
		public AppDate validTo;
		/** 親費目グループID */
		public Set<Id> parentGroupIds;
		/** 明細タイプ */
		public ExpRecordType recordType;
		/** 領収書添付 */
		public ExpFileAttachment fileAttachment;
		/** 税区分ID */
		public Set<Id> taxTypeBaseIds;
		/** 拡張項目ID */
		public Set<Id> extendedItemIds;
		/** Expense Type Group Code (partially match) */
		public String expGroupCode;
		/** Expense Type Group Code Set (Exact Match) */
		public Set<String> expGroupCodeSet;
		/** Expense Type Group Name (partially match) */
		public String expGroupName;
		/** Expense Type Group Name or Code*/
		public String query;
		/** Foreign Currency Usage */
		public Boolean useForeignCurrency;
		/** Fixed Foreign Currency ID */
		public Set<Id> fixedForeignCurrencyIds;

	}

	/**
	 * 費目を検索する
	 * @param filter 検索条件
	 * @return 検索結果
	 */
	public List<ExpTypeEntity> searchEntity(ExpTypeRepository.SearchFilter filter) {
		return searchEntity(filter, NOT_FOR_UPDATE, SortOrder.CODE_ASC);
	}

	/**
	 * 費目を検索する
	 * @param filter 検索条件
	 * @param order 並び順
	 * @return 検索結果
	 */
	public List<ExpTypeEntity> searchEntity(ExpTypeRepository.SearchFilter filter, SortOrder order) {
		return searchEntity(filter, NOT_FOR_UPDATE, order);
	}

	/**
	 * 費目を検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @param order 並び順
	 * @return 検索結果
	 */
	public List<ExpTypeEntity> searchEntity(ExpTypeRepository.SearchFilter filter, Boolean forUpdate, SortOrder order) {

		// WHERE句
		List<String> whereList = new List<String>();
		// 費目IDで検索
		Set<Id> pIds;
		if (filter.ids != null) {
			pIds = filter.ids;
			whereList.add('Id IN :pIds');
		}
		// Search by name (partially match)
		// ToDo: Change the target name field depends on the user language
		String pName;
		if (String.isNotEmpty(filter.name)) {
			pName = '%' + escapeSpecialCharacters(filter.name) + '%';

			String s = getFieldName(ExpType__c.Name_L0__c) + ' LIKE :pName OR '
				+ getFieldName(ExpType__c.Name_L1__c) + ' LIKE :pName OR '
				+ getFieldName(ExpType__c.Name_L2__c) + ' LIKE :pName';
			whereList.add(s);
		}
		// 会社IDで検索
		Set<Id> pCompanyIds;
		if (filter.companyIds != null) {
			pCompanyIds = filter.companyIds;
			whereList.add(getFieldName(ExpType__c.CompanyId__c) + ' IN :pCompanyIds');
		}

		// Expense Type Code String (Partially Match)
		String pCode;
		if (String.isNotEmpty(filter.code)) {
			pCode = '%' + escapeSpecialCharacters(filter.code) + '%';
			whereList.add(getFieldName(ExpType__c.Code__c) + ' LIKE :pCode');
		}
		// コードで検索
		Set<String> pCodes;
		if (filter.codes != null) {
			pCodes = filter.codes;
			whereList.add(getFieldName(ExpType__c.Code__c) + ' IN :pCodes');
		}
		// 有効期間内のレコードを取得する
		// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
		Date pTargetDate;
		if (filter.targetDate != null) {
			pTargetDate = filter.targetDate.getDate();
			whereList.add(getFieldName(ExpType__c.ValidFrom__c) + ' <= :pTargetDate');
			whereList.add(getFieldName(ExpType__c.ValidTo__c) + ' > :pTargetDate');
		}

		// When accounting period is specified
		if (filter.startDate != null) {
			final Date pStartDate = filter.startDate.getDate();
			final Date pEndDate = filter.endDate == null ? pStartDate : filter.endDate.getDate();
			whereList.add( getFieldName(ExpType__c.ValidTo__c) + ' > :pStartDate' );
			whereList.add( getFieldName(ExpType__c.ValidFrom__c) + '<= :pEndDate');
		}

		Date pValidFrom;
		if (filter.validFrom != null) {
			pValidFrom = filter.validFrom.getDate();
			whereList.add(getFieldName(ExpType__c.ValidFrom__c) + ' <= :pValidFrom');
		}
		Date pValidTo;
		if (filter.validTo != null) {
			pValidTo = filter.validTo.getDate();
			whereList.add(getFieldName(ExpType__c.ValidTo__c) + ' > :pValidTo');
		}

		// 親費目グループIDで検索
		Set<Id> pParentGroupIds;
		if (filter.parentGroupIds != null) {
			pParentGroupIds = filter.parentGroupIds;
			whereList.add(getFieldName(ExpType__c.ParentGroupId__c) + ' IN :pParentGroupIds');
		}

		String pGroupCode;
		if (String.isNotEmpty(filter.expGroupCode)) {
			pGroupCode = '%' + escapeSpecialCharacters(filter.expGroupCode) + '%';
			whereList.add(getFieldName(PARENT_R, ExpType__c.ParentGroupId__r.Code__c) + ' LIKE :pGroupCode');
		}
		Set<String> pGroupCodeSet;
	 	if (filter.expGroupCodeSet != null) {
			pGroupCodeSet = filter.expGroupCodeSet;
			whereList.add(getFieldName(PARENT_R, ExpType__c.ParentGroupId__r.Code__c) + ' IN :pGroupCodeSet');
		}
		String pGroupName;
		if (String.isNotEmpty(filter.expGroupName)) {
			pGroupName = '%' + escapeSpecialCharacters(filter.expGroupName) + '%';
			String s = getFieldName(PARENT_R, ExpType__c.ParentGroupId__r.Name_L0__c) + ' LIKE :pGroupName OR '
					+ getFieldName(PARENT_R, ExpType__c.ParentGroupId__r.Name_L1__c) + ' LIKE :pGroupName OR '
					+ getFieldName(PARENT_R, ExpType__c.ParentGroupId__r.Name_L2__c) + ' LIKE :pGroupName';
			whereList.add(s);
		}

		// 明細タイプで検索
		String pRecordType;
		if (filter.recordType != null) {
			pRecordType = ExpRecordType.getValue(filter.recordType);
			whereList.add(getFieldName(ExpType__c.RecordType__c) + ' = :pRecordType');
		}

		// Foreign Currency Usage
		Boolean pUseForeignCurrency;
		// Fixed Foreign Currency Ids
		Set<Id> pFixedForeignCurrencyIds;

		if (filter.useForeignCurrency != null) {
			pUseForeignCurrency = filter.useForeignCurrency;
			whereList.add(getFieldName(ExpType__c.UseForeignCurrency__c) + ' = :pUseForeignCurrency');

			if (filter.fixedForeignCurrencyIds != null) {
				pFixedForeignCurrencyIds = filter.fixedForeignCurrencyIds;
				whereList.add(getFieldName(ExpType__c.FixedForeignCurrencyId__c) + ' IN :pFixedForeignCurrencyIds');
			} else {
				whereList.add(getFieldName(ExpType__c.FixedForeignCurrencyId__c) + ' = null');
			}
		}

		// 領収書添付で検索
		String pFileAttachment;
		if (filter.fileAttachment != null) {
			pFileAttachment = ExpFileAttachment.getValue(filter.fileAttachment);
			whereList.add(getFieldName(ExpType__c.FileAttachment__c) + ' = :pFileAttachment');
		}
		// 税区分ベースIDで検索
		Set<Id> pTaxTypeBaseIds;
		if (filter.taxTypeBaseIds != null) {
			pTaxTypeBaseIds = filter.taxTypeBaseIds;
			whereList.add(
				'(' + getFieldName(ExpType__c.TaxTypeBase1Id__c) + ' IN :pTaxTypeBaseIds OR ' +
				getFieldName(ExpType__c.TaxTypeBase2Id__c) + ' IN :pTaxTypeBaseIds OR ' +
				getFieldName(ExpType__c.TaxTypeBase3Id__c) + ' IN :pTaxTypeBaseIds)');
		}
		// 拡張項目IDで検索
		Set<Id> pExtendedItemIds;
		if (filter.extendedItemIds != null) {
			pExtendedItemIds = filter.extendedItemIds;
			whereList.add(
				'(' + getFieldName(ExpType__c.ExtendedItemText01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemText10TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemLookup10TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemDate10TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist01TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist02TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist03TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist04TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist05TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist06TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist07TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist08TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist09TextId__c) + ' IN :pExtendedItemIds OR ' +
				getFieldName(ExpType__c.ExtendedItemPicklist10TextId__c) + ' IN :pExtendedItemIds)');
		}

		// Query for the Code or Name of Expense Type
		String pQuery;
		if (String.isNotEmpty(filter.query)) {
			pQuery = '%' + escapeSpecialCharacters(filter.query) + '%';
			String s = getFieldName(ExpType__c.Name_L0__c) + ' LIKE :pQuery OR '
					+ getFieldName(ExpType__c.Name_L1__c) + ' LIKE :pQuery OR '
					+ getFieldName(ExpType__c.Name_L2__c) + ' LIKE :pQuery OR '
					+ getFieldName(ExpType__c.Code__c) + ' LIKE :pQuery ';
			whereList.add(s);
		}

		String whereString = buildWhereString(whereList);

		// ORDER BY句
		String orderByString = ' ORDER BY ';
		if (order == SortOrder.CODE_ASC) {
			orderByString += getFieldName(ExpType__c.Code__c) + ' ASC NULLS LAST';
		} else if (order == SortOrder.CODE_DESC) {
			orderByString += getFieldName(ExpType__c.Code__c) + ' DESC NULLS LAST';
		} else if (order == SortOrder.ORDER_ASC) {
			orderByString += getFieldName(ExpType__c.Order__c) + ' NULLS LAST, ' + getFieldName(ExpType__c.Code__c) + ' ASC NULLS LAST';
		} else if (order == SortOrder.ORDER_DESC) {
			orderByString += getFieldName(ExpType__c.Order__c) + ' NULLS LAST, ' + getFieldName(ExpType__c.Code__c) + ' DESC NULLS LAST';
		}

		// SELECT対象項目リスト
		List<String> selectFieldList = new List<String>();
		// 基本項目
		selectFieldList.addAll(GET_FIELD_NAME_LIST);
		// 関連項目（親費目グループ）
		selectFieldList.add(getFieldName(PARENT_R, ExpTypeGroup__c.Code__c));
		selectFieldList.add(getFieldName(PARENT_R, ExpTypeGroup__c.Name_L0__c));
		selectFieldList.add(getFieldName(PARENT_R, ExpTypeGroup__c.Name_L1__c));
		selectFieldList.add(getFieldName(PARENT_R, ExpTypeGroup__c.Name_L2__c));

		// SOQL作成
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
				+ ' FROM ' + ExpType__c.SObjectType.getDescribe().getName()
				+ whereString
				+ orderByString
				+ ' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('ExpTypeRepository: SOQL=' + soql);

		// クエリ実行
		List<ExpType__c> sObjs = (List<ExpType__c>)Database.query(soql);

		// SObjectからエンティティを作成
		List<ExpTypeEntity> entities = new List<ExpTypeEntity>();
		for (ExpType__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntityOld> entityList) {

		List<ExpType__c> objectList = new List<ExpType__c>();
		for (ValidPeriodEntityOld entity : entityList) {
			objectList.add(createObject((ExpTypeEntity)entity));
		}

		return objectList;
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entity オブジェクト作成元のエンティティ
	 * @return 保存用のオブジェクト
	 */
	@TestVisible
	private ExpType__c createObject(ExpTypeEntity entity) {

		ExpType__c sObj = new ExpType__c();

		// 有効期間型共通の項目を設定する
		setValidPeriodEntityValueToObject(entity, sObj);

		if (entity.isChanged(ExpTypeEntity.Field.CODE)) {
			sObj.Code__c = entity.code;
		}
		if (entity.isChanged(ExpTypeEntity.Field.NAME_L0)) {
			sObj.Name_L0__c = entity.nameL0;
		}
		if (entity.isChanged(ExpTypeEntity.Field.NAME_L1)) {
			sObj.Name_L1__c = entity.nameL1;
		}
		if (entity.isChanged(ExpTypeEntity.Field.NAME_L2)) {
			sObj.Name_L2__c = entity.nameL2;
		}
		if (entity.isChanged(ExpTypeEntity.Field.COMPANY_ID)) {
			sObj.CompanyId__c = entity.companyId;
		}
		if (entity.isChanged(ExpTypeEntity.Field.UNIQ_KEY)) {
			sObj.UniqKey__c = entity.uniqKey;
		}
		if (entity.isChanged(ExpTypeEntity.Field.PARENT_GROUP_ID)) {
			sObj.ParentGroupId__c = entity.parentGroupId;
		}
		if (entity.isChanged(ExpTypeEntity.Field.DESCRIPTION_L0)) {
			sObj.Description_L0__c = entity.descriptionL0;
		}
		if (entity.isChanged(ExpTypeEntity.Field.DESCRIPTION_L1)) {
			sObj.Description_L1__c = entity.descriptionL1;
		}
		if (entity.isChanged(ExpTypeEntity.Field.DESCRIPTION_L2)) {
			sObj.Description_L2__c = entity.descriptionL2;
		}
		if (entity.isChanged(ExpTypeEntity.Field.FILE_ATTACHMENT)) {
			sObj.FileAttachment__c = ExpFileAttachment.getValue(entity.fileAttachment);
		}
		if (entity.isChanged(ExpTypeEntity.Field.RECORD_TYPE)) {
			sObj.RecordType__c = ExpRecordType.getValue(entity.recordType);
		}
		if (entity.isChanged(ExpTypeEntity.Field.USAGE)) {
			sObj.Usage__c = String.valueOf(entity.usage);
		}
		if (entity.isChanged(ExpTypeEntity.Field.TAX_TYPE_BASE1_ID)) {
			sObj.TaxTypeBase1Id__c = entity.taxTypeBase1Id;
		}
		if (entity.isChanged(ExpTypeEntity.Field.TAX_TYPE_BASE2_ID)) {
			sObj.TaxTypeBase2Id__c = entity.taxTypeBase2Id;
		}
		if (entity.isChanged(ExpTypeEntity.Field.TAX_TYPE_BASE3_ID)) {
			sObj.TaxTypeBase3Id__c = entity.taxTypeBase3Id;
		}

		Boolean extendedItemDateIdChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_DATE_ID_LIST);
		Boolean extendedItemDateUsedInChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_DATE_USED_IN_LIST);
		Boolean extendedItemDateRequiredForChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_DATE_REQUIRED_FOR_LIST);

		Boolean extendedItemLookupIdChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_ID_LIST);
		Boolean extendedItemLookupUsedInChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_USED_IN_LIST);
		Boolean extendedItemLookupRequiredForChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_LOOKUP_REQUIRED_FOR_LIST);

		Boolean extendedItemPicklistIdChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_ID_LIST);
		Boolean extendedItemPicklistUsedInChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_USED_IN_LIST);
		Boolean extendedItemPicklistRequiredForChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_PICKLIST_REQUIRED_FOR_LIST);

		Boolean extendedItemTextIdChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_TEXT_ID_LIST);
		Boolean extendedItemTextUsedInChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_TEXT_USED_IN_LIST);
		Boolean extendedItemTextRequiredForChanged = entity.isChanged(ExpTypeEntity.ExtendedItemConfigField.EXTENDED_ITEM_TEXT_REQUIRED_FOR_LIST);

		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			if (extendedItemDateIdChanged) {
				sObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID, entity.getExtendedItemDateId(i));
			}
			if (extendedItemDateUsedInChanged) {
				sObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_USED_IN,
					ComExtendedItemUsedIn.getValue(entity.getExtendedItemDateUsedIn(i)));
			}
			if (extendedItemDateRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR,
					ComExtendedItemRequiredFor.getValue(entity.getExtendedItemDateRequiredFor(i)));
			}

			if (extendedItemLookupIdChanged) {
				sObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_ID, entity.getExtendedItemLookupId(i));
			}
			if (extendedItemLookupUsedInChanged) {
				sObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_USED_IN,
					ComExtendedItemUsedIn.getValue(entity.getExtendedItemLookupUsedIn(i)));
			}
			if (extendedItemLookupRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR,
					ComExtendedItemRequiredFor.getValue(entity.getExtendedItemLookupRequiredFor(i)));
			}

			if (extendedItemPicklistIdChanged) {
				sObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID, entity.getExtendedItemPicklistId(i));
			}
			if (extendedItemPicklistUsedInChanged) {
				sObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_USED_IN,
					ComExtendedItemUsedIn.getValue(entity.getExtendedItemPicklistUsedIn(i)));
			}
			if (extendedItemPicklistRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR,
					ComExtendedItemRequiredFor.getValue(entity.getExtendedItemPicklistRequiredFor(i)));
			}

			if (extendedItemTextIdChanged) {
				sObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID, entity.getExtendedItemTextId(i));
			}
			if (extendedItemTextUsedInChanged) {
				sObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_USED_IN,
					ComExtendedItemUsedIn.getValue(entity.getExtendedItemTextUsedIn(i)));
			}
			if (extendedItemTextRequiredForChanged) {
				sObj.put(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR,
					ComExtendedItemRequiredFor.getValue(entity.getExtendedItemTextRequiredFor(i)));
			}
		}

		if (entity.isChanged(ExpTypeEntity.Field.DEBIT_ACCOUNT_CODE)) {
			sObj.DebitAccountCode__c = entity.debitAccountCode;
		}
		if (entity.isChanged(ExpTypeEntity.Field.DEBIT_ACCOUNT_NAME)) {
			sObj.DebitAccountName__c = entity.debitAccountName;
		}
		if (entity.isChanged(ExpTypeEntity.Field.DEBIT_SUB_ACCOUNT_CODE)) {
			sObj.DebitSubAccountCode__c = entity.debitSubAccountCode;
		}
		if (entity.isChanged(ExpTypeEntity.Field.DEBIT_SUB_ACCOUNT_NAME)) {
			sObj.DebitSubAccountName__c = entity.debitSubAccountName;
		}
		if (entity.isChanged(ExpTypeEntity.Field.ORDER)) {
			sObj.Order__c = entity.order;
		}
		if (entity.isChanged(ExpTypeEntity.Field.USE_FOREIGN_CURRENCY)) {
			sObj.UseForeignCurrency__c = entity.useForeignCurrency;
		}
		if (entity.isChanged(ExpTypeEntity.Field.FIXED_FOREIGN_CURRENCY_ID)) {
			sObj.FixedForeignCurrencyId__c = entity.fixedForeignCurrencyId;
		}
		if (entity.isChanged(ExpTypeEntity.Field.FIXED_ALLOWANCE_SINGLE_AMOUNT)) {
			sObj.FixedAllowanceSingleAmount__c = entity.fixedAllowanceSingleAmount;
		}

		return sObj;
	}

	/**
	 * SObjectからエンティティを作成する
	 * @param obj 作成元のSObject
	 * @return 作成したエンティティ
	 */
	@TestVisible
	private ExpTypeEntity createEntity(ExpType__c sObj) {

		ExpTypeEntity entity = new ExpTypeEntity();

		// 有効期間型共通の項目を設定する
		setValidPeriodObjectValueToEntity(sObj, entity);

		entity.code = sObj.Code__c;
		entity.uniqKey = sObj.UniqKey__c;
		entity.nameL0 = sObj.Name_L0__c;
		entity.nameL1 = sObj.Name_L1__c;
		entity.nameL2 = sObj.Name_L2__c;
		entity.companyId = sObj.CompanyId__c;
		entity.parentGroupId = sObj.ParentGroupId__c;
		if(entity.ParentGroupId != null) {
			entity.parentGroup = new ExpTypeEntity.ParentGroup();
			entity.parentGroup.code = sObj.ParentGroupId__r.Code__c;
			entity.parentGroup.nameL = new AppMultiString(
					sObj.ParentGroupId__r.Name_L0__c,
					sObj.ParentGroupId__r.Name_L1__c,
					sObj.ParentGroupId__r.Name_L2__c);
		}
		entity.descriptionL0 = sObj.Description_L0__c;
		entity.descriptionL1 = sObj.Description_L1__c;
		entity.descriptionL2 = sObj.Description_L2__c;
		entity.fileAttachment = ExpFileAttachment.valueOf(sobj.FileAttachment__c);
		entity.recordType = ExpRecordType.valueOf(sobj.RecordType__c);
		entity.setUsage(sObj.Usage__c);
		entity.taxTypeBase1Id = sObj.TaxTypeBase1Id__c;
		entity.taxTypeBase2Id = sObj.TaxTypeBase2Id__c;
		entity.taxTypeBase3Id = sObj.TaxTypeBase3Id__c;

		for (Integer i = 0; i < EXTENDED_ITEM_MAX_COUNT; i++) {
			String index = String.valueOf(i + 1).leftPad(2, '0');

			entity.setExtendedItemTextId(i, (Id) sObj.get(EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemTextUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
					EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemTextRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
					EXTENDED_ITEM_TEXT + index + EXTENDED_ITEM_REQUIRED_FOR)));

			entity.setExtendedItemPicklistId(i, (Id) sObj.get(EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemPicklistUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
					EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemPicklistRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
					EXTENDED_ITEM_PICKLIST + index + EXTENDED_ITEM_REQUIRED_FOR)));

			entity.setExtendedItemDateId(i, (Id) sObj.get(EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemDateUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
					EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemDateRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
					EXTENDED_ITEM_DATE + index + EXTENDED_ITEM_REQUIRED_FOR)));

			entity.setExtendedItemLookupId(i, (Id) sObj.get(EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_ID));
			entity.setExtendedItemLookupUsedIn(i, ComExtendedItemUsedIn.valueOf((String) sObj.get(
					EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_USED_IN)));
			entity.setExtendedItemLookupRequiredFor(i, ComExtendedItemRequiredFor.valueOf((String) sObj.get(
					EXTENDED_ITEM_LOOKUP + index + EXTENDED_ITEM_REQUIRED_FOR)));
		}

		entity.debitAccountCode = sObj.DebitAccountCode__c;
		entity.debitAccountName = sObj.DebitAccountName__c;
		entity.debitSubAccountCode = sObj.DebitSubAccountCode__c;
		entity.debitSubAccountName = sObj.DebitSubAccountName__c;
		entity.order = (Integer)sObj.Order__c;
		entity.useForeignCurrency = (Boolean)sObj.UseForeignCurrency__c;
		entity.fixedForeignCurrencyId = sObj.FixedForeignCurrencyId__c;
		entity.fixedAllowanceSingleAmount = sObj.FixedAllowanceSingleAmount__c;

		entity.resetChanged();

		return entity;
	}

	/*
	 * Return an escaped version of the input.
	 * @param unEscapedString String to check
	 * @return escaped version of the string
	 */
	private static String escapeSpecialCharacters(String unEscapedString) {
		String[] chars = unEscapedString.split('');
		for (Integer i = 0; i < chars.size(); i++) {
			if (SPECIAL_CHARACTERS_PATTERN.matcher(chars[i]).matches()) {
				chars[i] = '\\' + chars[i];
			}
		}
		return String.join(chars, '');
	}
}