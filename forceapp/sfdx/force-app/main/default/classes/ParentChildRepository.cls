/**
 * @group Common 共通
 *
 * @description Base class of parent-and-child type repository
 * 履歴管理方式が親子型のリポジトリの基底クラス
 */
public with sharing abstract class ParentChildRepository extends Repository.BaseRepository {

	/**　レコードの保存時に1件でも失敗したらエラーとしてロールバックする */
	private final static Boolean ALL_SAVE = true;

	/**
	 * エンティティを保存する(ベース+履歴が保存対象)
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public virtual Repository.SaveResultWithChildren saveEntity(ParentChildBaseEntity entity) {
		return saveEntityList(new List<ParentChildBaseEntity>{entity}, ALL_SAVE);
	}

	/**
	 * エンティティを保存する(ベース+履歴が保存対象)
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public virtual Repository.SaveResultWithChildren saveEntityList(List<ParentChildBaseEntity> entityList) {
		return saveEntityList(entityList, ALL_SAVE);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public virtual Repository.SaveResultWithChildren saveEntityList(List<ParentChildBaseEntity> entityList, Boolean allOrNone) {

		List<SObject> baseObjList = new List<SObject>();

		// ベースエンティティからSObjectを作成する
		for (ParentChildBaseEntity entity : entityList) {
			SObject sObj = createBaseObj(entity);
			baseObjList.add(sObj);
		}

		// DBに保存する
		List<Database.UpsertResult> baseSaveResultList = upsertBaseObj(baseObjList, allOrNone);

		// 履歴エンティティの保存用のオブジェクトを作成する
		List<SObject> historyObjList = new List<SObject>();
		for (Integer i = 0; i < baseSaveResultList.size(); i++) {
			Database.UpsertResult res = baseSaveResultList[i];

			// ベースの保存に失敗している場合は明細も保存しない次のレコードへ
			if (! res.isSuccess()) {
				continue;
			}

			SObject baseObj = baseObjList[i];
			ParentChildBaseEntity baseEntity = entityList[i];

			// 履歴リストが空の場合は保存対象外
			if (baseEntity.isEmptyHistoryList()) {
				continue;
			}

			// 履歴オブジェクトを履歴エンティティから作成する
			Id baseId = res.getId();
			for (ParentChildHistoryEntity historyEntity : baseEntity.getSuperHistoryList()) {
				// 新規作成の場合のみbaseIDを設定する
				if (historyEntity.id == null) {
					historyEntity.baseId = baseId;
				}
				SObject historyObj = createHistoryObj(historyEntity);
				historyObjList.add(historyObj);
			}
		}

		// 履歴オブジェクトを保存する
		List<Database.UpsertResult> historyResList = upsertHistoryObj(historyObjList, allOrNone);

		// 結果作成
		Repository.SaveResult baseResult = resultFactory.createSaveResult(baseSaveResultList);
		Repository.SaveResult historyResult = resultFactory.createSaveResult(historyResList);
		SaveResultWithChildren repoRes = new SaveResultWithChildren();
		repoRes.isSuccessAll = baseResult.isSuccessAll && historyResult.isSuccessAll;
		repoRes.details = baseResult.details;
		repoRes.recordDetails = historyResult.details;

		return repoRes;
	}

	/**
	 * ベースエンティティのみ保存する(履歴は保存されません)
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveBaseEntity(ParentChildBaseEntity entity) {
		return saveBaseEntityList(new List<ParentChildBaseEntity>{entity}, ALL_SAVE);
	}

	/**
	 * ベースエンティティのみ保存する(履歴は保存されません)
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveBaseEntityList(List<ParentChildBaseEntity> entityList, Boolean allOrNone) {

		// ベースエンティティの保存用のオブジェクトを作成する
		List<SObject> baseObjList = new List<SObject>();
		for (ParentChildBaseEntity entity : entityList) {
			SObject baseObj = createBaseObj(entity);
			baseObjList.add(baseObj);
		}
		// 履歴オブジェクトを保存する
		 List<Database.UpsertResult> baseResList = upsertBaseObj(baseObjList, allOrNone);

		// 結果作成
		Repository.SaveResult baseResult = resultFactory.createSaveResult(baseResList);

		return baseResult;
	}

	/**
	 * 履歴エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public virtual Repository.SaveResult saveHistoryEntity(ParentChildHistoryEntity entity) {
		return saveHistoryEntityList(new List<ParentChildHistoryEntity>{entity}, ALL_SAVE);
	}

	/**
	 * 履歴エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public virtual Repository.SaveResult saveHistoryEntityList(List<ParentChildHistoryEntity> entityList) {
		return saveHistoryEntityList(entityList, ALL_SAVE);
	}

	/**
	 * 履歴エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public virtual Repository.SaveResult saveHistoryEntityList(List<ParentChildHistoryEntity> entityList, Boolean allOrNone) {

		// 履歴エンティティの保存用のオブジェクトを作成する
		List<SObject> historyObjList = new List<SObject>();
		for (ParentChildHistoryEntity entity : entityList) {
			SObject historyObj = createHistoryObj(entity);
			historyObjList.add(historyObj);
		}
		// 履歴オブジェクトを保存する
		 List<Database.UpsertResult> historyResList = upsertHistoryObj(historyObjList, allOrNone);

		// 結果作成
		Repository.SaveResult historyResult = resultFactory.createSaveResult(historyResList);

		return historyResult;
	}
	/**
	 * ベースエンティティからオブジェクトを作成する（各リポジトリで実装する）
	 * @param baseEntity 作成元のベースエンティティ
	 * @return 作成したベースオブジェクト
	 */
	protected abstract SObject createBaseObj(ParentChildBaseEntity baseEntity);

	/**
	 * 履歴エンティティからオブジェクトを作成する
	 * @param historyEntity 作成元の履歴エンティティ
	 * @return 作成した履歴オブジェクト
	 */
	protected abstract SObject createHistoryObj(ParentChildHistoryEntity historyEntity);

	/**
	 * ベースオブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 * @param baseObjList upsert対象のオブジェクトリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 */
	protected abstract List<Database.UpsertResult> upsertBaseObj(List<SObject> baseObjList, Boolean allOrNone);

	/**
	 * 履歴オブジェクトをUpsertする。
	 * 下記のエラーが発生するため、各リポジトリで実装すること。
	 * 「System.TypeException: DML on generic List<SObject> only allowed for insert, update or delete」対応
	 * @param historyObjList upsert対象のオブジェクトリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 */
	protected abstract List<Database.UpsertResult> upsertHistoryObj(List<SObject> historyObjList, Boolean allOrNone);
}