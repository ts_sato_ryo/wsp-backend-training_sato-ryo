/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Entity of Expense Request request
 */
public with sharing class ExpRequestApprovalEntity extends Entity {

	/** Max length of comment */
	private static final Integer COMMENT_MAX_LENGTH = 1000;

	/** Fields definition */
	public enum Field {
		NAME,
		OWNER_ID,
		EXP_REQUEST_ID,
		TOTAL_AMOUNT,
		REQUEST_NO,
		SUBJECT,
		COMMENT,
		PROCESS_COMMENT,
		STATUS,
		IS_CLAIMED,
		IS_CONFIRMATION_REQUIRED,
		CANCEL_TYPE,
		EMPLOYEE_HISTORY_ID,
		REQUEST_TIME,
		DEPARTMENT_HISTORY_ID,
		ACTOR_HISTORY_ID,
		LAST_APPROVAL_TIME,
		LAST_APPROVAL_ID,
		APPROVAL_01_ID
	}

	/** Set of fields whose value has changed */
	private Set<Field> isChangedFieldSet;

	/**
	 * Constructor
	 */
	public ExpRequestApprovalEntity() {
		isChangedFieldSet = new Set<Field>();
	}

	/** Name of record */
	public String name {
		get;
		set {
			if (String.isBlank(value)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('name');
				throw ex;
			}
			name = value;
			setChanged(Field.NAME);
		}
	}

	/** Owner ID */
	public Id ownerId {
		get;
		set {
			ownerId = value;
			setChanged(Field.OWNER_ID);
		}
	}

	/** Request ID */
	public Id expRequestId {
		get;
		set {
			expRequestId = value;
			setChanged(Field.EXP_REQUEST_ID);
		}
	}

	/** Total Amount */
	public Decimal totalAmount {
		get;
		set {
			totalAmount = value;
			setChanged(Field.TOTAL_AMOUNT);
		}
	}

	/** Request No */
	public String requestNo {
		get;
		set {
			requestNo = value;
			setChanged(Field.REQUEST_NO);
		}
	}

	/** Subject */
	public String subject {
		get;
		set {
			subject = value;
			setChanged(Field.SUBJECT);
		}
	}

	/** Application comment */
	public String comment {
		get;
		set {
			comment = value;
			setChanged(Field.COMMENT);
		}
	}

	/** Reject comment */
	public String processComment {
		get;
		set {
			processComment = value;
			setChanged(Field.PROCESS_COMMENT);
		}
	}

	/** Status */
	public AppRequestStatus status {
		get;
		set {
			status = value;
			setChanged(Field.STATUS);
		}
	}

	/** Flag whether expense has been claimed from request */
	public Boolean claimed {
		get;
		set {
			claimed = value;
			setChanged(Field.IS_CLAIMED);
		}
	}

	/** Confirmation Required */
	public Boolean isConfirmationRequired {
		get;
		set {
			isConfirmationRequired = value;
			setChanged(Field.IS_CONFIRMATION_REQUIRED);
		}
	}

	/** Cancel Type */
	public AppCancelType cancelType {
		get;
		set {
			cancelType = value;
			setChanged(Field.CANCEL_TYPE);
		}
	}

	/** Employee history Id */
	public Id employeeHistoryId {
		get;
		set {
			employeeHistoryId = value;
			setChanged(Field.EMPLOYEE_HISTORY_ID);
		}
	}

	/** Employee base Id (for reference) */
	public Id employeeBaseId {
		get;
		set;
	}

	/** Employee info */
	public AppLookup.Employee employee {
		get;
		set;
	}

	/** Application time */
	public AppDatetime requestTime {
		get;
		set {
			requestTime = value;
			setChanged(Field.REQUEST_TIME);
		}
	}

	/** Department history ID */
	public Id departmentHistoryId {
		get;
		set {
			departmentHistoryId = value;
			setChanged(Field.DEPARTMENT_HISTORY_ID);
		}
	}

	/** Department info */
	public AppLookup.Department department {
		get;
		set;
	}

	/** Actor hitory ID */
	public Id actorHistoryId {
		get;
		set {
			actorHistoryId = value;
			setChanged(Field.ACTOR_HISTORY_ID);
		}
	}

	/** Actor info */
	public AppLookup.Employee actor {
		get;
		set;
	}

	/** Last approved time */
	public AppDatetime lastApproveTime {
		get;
		set {
			lastApproveTime = value;
			setChanged(Field.LAST_APPROVAL_TIME);
		}
	}

	/** Last approver */
	public Id lastApproverId {
		get;
		set {
			lastApproverId = value;
			setChanged(Field.LAST_APPROVAL_ID);
		}
	}

	/** Approver 01 */
	public Id approver01Id {
		get;
		set {
			approver01Id = value;
			setChanged(Field.APPROVAL_01_ID);
		}
	}

	/** User has edit access to the record (for reference) */
	public Boolean hasEditAccess {
		get;
		set;
	}

	/**
	 * Mark field as changed
	 * @param Field Changed field
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}

	/**
	 * Detail of status
	 */
	public Enum DetailStatus {
		NOT_REQUESTED,		// 未申請
		PENDING,			// 承認待ち
		APPROVED,			// 承認済み
		REJECTED,			// 却下
		REMOVED,			// 申請取消
		CANCELED,		// 承認取消
		CLAIMED			// 経費申請済み
	}

	/**
	 * Get detail status
	 */
	public DetailStatus getDetailStatus() {
		DetailStatus detailStatus;

		if (this.id == null) {
			// Not applied
			detailStatus = ExpRequestApprovalEntity.DetailStatus.NOT_REQUESTED;
		} else {
			if (this.status == AppRequestStatus.PENDING) {
				// Status：Pending
				detailStatus = ExpRequestApprovalEntity.DetailStatus.PENDING;
			} else if (this.status == AppRequestStatus.APPROVED) {
				// Status：Approved
				detailStatus = ExpRequestApprovalEntity.DetailStatus.APPROVED;
				if (this.claimed) {
					// Claimed
					detailStatus = ExpRequestApprovalEntity.DetailStatus.CLAIMED;
				}
			} else if (this.status == AppRequestStatus.DISABLED) {
				// Status：Discarded
				if (this.cancelType == AppCancelType.REJECTED) {
					// Cancel Type：Rejected
					detailStatus = ExpRequestApprovalEntity.DetailStatus.REJECTED;
				} else if (this.cancelType == AppCancelType.REMOVED) {
					// Cancel Type：Cancel request
					detailStatus = ExpRequestApprovalEntity.DetailStatus.REMOVED;
				} else if (this.cancelType == AppCancelType.CANCELED) {
					// Cancel Type：Cancel approval
					detailStatus = ExpRequestApprovalEntity.DetailStatus.CANCELED;
				} else {
					// Should not reach here.
					throw new App.IllegalStateException(
							ComMessage.msg().Exp_Err_GetStatus(new List<String>{
								ComMessage.msg().Exp_Lbl_Request})
							+ ComMessage.msg().Com_Err_InvalidParameter(new List<String>{
								ComMessage.msg().Exp_Lbl_CancelType, AppConverter.stringValue(this.cancelType)}));
				}
			} else {
				// Stats hasn't been set. so set default value.
				detailStatus = ExpRequestApprovalEntity.DetailStatus.NOT_REQUESTED;
			}
		}

		return detailStatus;
	}
}