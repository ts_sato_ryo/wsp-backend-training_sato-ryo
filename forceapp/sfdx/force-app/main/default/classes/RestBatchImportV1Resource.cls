/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description マスタインポートバッチ処理起動API
 */
@RestResource(urlMapping='/v1/batch-import/exec')
global with sharing class RestBatchImportV1Resource {

	/**
	 * @description リクエストパラメータを格納するクラス
	 */
	public class RequestParam {
		/** 種別 */
		public String type;
		/** インポートバッチID */
		public String importBatchId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 種別
			if (String.isNotBlank(type)) {
				if (ImportBatchType.ValueOf(type) == null) {
					throw new App.ParameterException('type', type);
				}
			} else {
				throw new App.ParameterException('type', type);
			}

			// インポートバッチID
			if (String.isNotBlank(importBatchId)) {
				try {
					Id.ValueOf(importBatchId);
				} catch (Exception e) {
					throw new App.ParameterException('importBatchId', importBatchId);
				}
			}
		}
	}

	/**
	 * @description レスポンスを格納するクラス
	 */
	public class Response implements RemoteApi.ResponseParam {
		/** インポートバッチID */
		public String importBatchId;
	}

	/**
	 * バッチ処理を起動する
	 */
	@HttpPost
	global static void doPost() {
		RestRequest restReq = RestContext.request;
		RestResponse restRes = RestContext.response;
		Response res;
		RemoteApi.Error error;

		try {
			Blob body = restReq.requestBody;
			System.debug(LoggingLevel.DEBUG, 'requestBody: ' + body.toString());

			RequestParam param = (RequestParam)System.JSON.deserialize(body.toString(), RequestParam.class);

			// リクエストパラメータのバリデーション
			param.validate();

			ImportBatchType type = ImportBatchType.valueOf(param.type);
			Id importBatchId = String.isBlank(param.importBatchId) ? null : param.importBatchId;

			// サービスクラスを初期化
			ImportBatchService service = new ImportBatchService(type, importBatchId);

			// 新規バッチの場合、対象データがない場合はエラー
			if (String.isBlank(importBatchId) && service.processCount == 0) {
				// メッセージ：処理対象のデータが見つかりませんでした。
				throw new App.IllegalStateException(
						App.ERR_CODE_RECORD_NOT_FOUND,
						ComMessage.msg().Batch_Err_RecordNotFound);
			}
			// 処理対象の件数が上限を超える場合はエラー
			if (service.processCount > ImportBatchService.MAX_COUNT) {
				// メッセージ：インポート対象のデータ件数が上限を超えています。
				throw new App.IllegalStateException(
						App.ERR_CODE_TOO_MANY_IMPORT_RECORDS,
						ComMessage.msg().Batch_Err_TooManyImportRecords);
			}

			// インポートバッチデータを作成
			importBatchId = service.createImportBatchData();

			// バッチ処理起動
			service.executeBatch();

			res = new Response();
			res.importBatchId = importBatchId;
			restRes.statusCode = 200;

		} catch (App.BaseException e) {
			error = new RemoteApi.Error();
			error.errorCode = e.getErrorCode();
			error.message = e.getMessage();
			restRes.statusCode = 400;
		} catch (DmlException e) {
			error = new RemoteApi.Error();
			error.errorCode = e.getDmlStatusCode(0);
			error.message = e.getMessage();
			error.stackTrace = e.getStackTraceString();
			restRes.statusCode = 500;
		} catch (Exception e) {
			error = new RemoteApi.Error();
			error.errorCode = 'UNEXPECTED_ERROR';
			error.message = e.getMessage();
			error.stackTrace = e.getStackTraceString();
			restRes.statusCode = 500;
		}

		RemoteApi.ResponseBase resBody;
		if (error == null) {
			// 成功レスポンスをセット
			resBody = new RemoteApi.SuccessResponse(res);
		} else {
			// エラーレスポンスをセット
			resBody = new RemoteApi.ErrorResponse(error);
		}

		// 実行結果を返す
		restRes.addHeader('Content-Type', 'application/json');
		restRes.responseBody = Blob.valueof(JSON.serialize(resBody));
	}
}