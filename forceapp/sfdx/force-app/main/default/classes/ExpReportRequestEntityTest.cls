/*
 * @author TeamSpirit Inc.
 * 
 * @date 2018
 * 
 * @description Test class for ExpReportRequestEntity
 * 
 * @group Expense
 */
@IsTest
private class ExpReportRequestEntityTest {

	/*
	 * Test the Detail Status Retrieval Method of ExpReportRequestEntity.
	 */
	@IsTest static void testGetDetailStatus() {
		ExpReportRequestEntity noIdEntity = new ExpReportRequestEntity();

		ExpReportRequestEntity pendingEntity = new ExpReportRequestEntity();
		pendingEntity.setId(UserInfo.getUserId());
		pendingEntity.status = AppRequestStatus.PENDING;

		ExpReportRequestEntity approvedEntity = new ExpReportRequestEntity();
		approvedEntity.setId(UserInfo.getUserId());
		approvedEntity.status = AppRequestStatus.APPROVED;

		ExpReportRequestEntity rejectedEntity = new ExpReportRequestEntity();
		rejectedEntity.setId(UserInfo.getUserId());
		rejectedEntity.status = AppRequestStatus.DISABLED;
		rejectedEntity.cancelType = AppCancelType.REJECTED;

		ExpReportRequestEntity removedEntity = new ExpReportRequestEntity();
		removedEntity.setId(UserInfo.getUserId());
		removedEntity.status = AppRequestStatus.DISABLED;
		removedEntity.cancelType = AppCancelType.REMOVED;

		ExpReportRequestEntity cancelledEntity = new ExpReportRequestEntity();
		cancelledEntity.setId(UserInfo.getUserId());
		cancelledEntity.status = AppRequestStatus.DISABLED;
		cancelledEntity.cancelType = AppCancelType.CANCELED;

		ExpReportRequestEntity illegalStateEntity = new ExpReportRequestEntity();
		illegalStateEntity.setId(UserInfo.getUserId());
		illegalStateEntity.status = AppRequestStatus.DISABLED;
		illegalStateEntity.cancelType = AppCancelType.NONE;

		ExpReportRequestEntity statusNotSetEntity = new ExpReportRequestEntity();
		statusNotSetEntity.setId(UserInfo.getUserId());

		ExpReportRequestEntity accountingAuthorizedEntity = new ExpReportRequestEntity();
		accountingAuthorizedEntity.setId(UserInfo.getUserId());
		accountingAuthorizedEntity.status = AppRequestStatus.APPROVED;
		accountingAuthorizedEntity.accountingStatus = ExpAccountingStatus.AUTHORIZED;
		accountingAuthorizedEntity.exportedTime = null;

		ExpReportRequestEntity accountingRejectedEntity = new ExpReportRequestEntity();
		accountingRejectedEntity.setId(UserInfo.getUserId());
		accountingRejectedEntity.status = AppRequestStatus.DISABLED;
		accountingRejectedEntity.cancelType = AppCancelType.CANCELED;
		accountingRejectedEntity.accountingStatus = ExpAccountingStatus.REJECTED;

		ExpReportRequestEntity journalCreatedEntity = new ExpReportRequestEntity();
		journalCreatedEntity.setId(UserInfo.getUserId());
		journalCreatedEntity.status = AppRequestStatus.APPROVED;
		journalCreatedEntity.accountingStatus = ExpAccountingStatus.AUTHORIZED;
		journalCreatedEntity.exportedTime = AppDatetime.now();

		System.assertEquals(ExpReportRequestEntity.DetailStatus.NOT_REQUESTED, noIdEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.PENDING, pendingEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.APPROVED, approvedEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.REJECTED, rejectedEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.REMOVED, removedEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.CANCELED, cancelledEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.NOT_REQUESTED, statusNotSetEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.ACCOUNTING_AUTHORIZED, accountingAuthorizedEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.ACCOUNTING_REJECTED, accountingRejectedEntity.getDetailStatus());
		System.assertEquals(ExpReportRequestEntity.DetailStatus.JOURNAL_CREATED, journalCreatedEntity.getDetailStatus());

		Exception expectedException;
		try {
			illegalStateEntity.getDetailStatus();
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof App.IllegalStateException);
	}
}