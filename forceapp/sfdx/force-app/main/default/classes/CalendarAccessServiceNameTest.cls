/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * CalendarAccessServiceNameのテスト
 */
@isTest
private class CalendarAccessServiceNameTest {

	/**
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(CalendarAccessServiceName.OFFICE_365, CalendarAccessServiceName.OFFICE_365);

		// TODO 選択値が追加されたら実装
		// 等しくない場合(値が異なる) → false

		// TODO 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, CalendarAccessServiceName.OFFICE_365.equals(Date.today()));

		// 等しくない場合(null)
		System.assertEquals(false, CalendarAccessServiceName.OFFICE_365.equals(null));
	}

}