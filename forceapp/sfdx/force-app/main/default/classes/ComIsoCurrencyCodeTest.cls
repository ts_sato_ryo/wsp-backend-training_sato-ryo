/*
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * Test for ComIsoCurrencyCode class
 */
@isTest
private class ComIsoCurrencyCodeTest {

	/**
	 * Test for valueOf method
	 */
	@isTest
	static void valueOfTest() {

		// Get JPY currency
		ComIsoCurrencyCode jpyCurrency = ComIsoCurrencyCode.valueOf('JPY');
		// Confirm expected value has been got
		System.assertEquals('JPY', jpyCurrency.value);
		if (UserInfo.getLanguage() == 'ja') {
			System.assertEquals('JPY - 日本円', jpyCurrency.label);
		} else {
			// Currently, only supporting 2 language.
			System.assertEquals('JPY - Japanese yen', jpyCurrency.label);
		}

		// Get not existing currency
		ComIsoCurrencyCode nullCurrency = ComIsoCurrencyCode.valueOf('TEST');
		// Confirm null returned
		System.assertEquals(null, nullCurrency);
	}

	/**
	 * Test for getTypeListTst method
	 */
	@isTest
	static void getTypeListTst() {
		List<ComIsoCurrencyCode> typeList = ComIsoCurrencyCode.getTypeList();
		System.assertEquals(160, typeList.size());
	}

	/**
	 * Test for equals method
	 */
	@isTest
	static void equalsTest() {

		ComIsoCurrencyCode jpy1 = ComIsoCurrencyCode.valueOf('JPY');
		ComIsoCurrencyCode jpy2 = ComIsoCurrencyCode.valueOf('JPY');

		String COMPARE = 'compare';

		System.assert(jpy1.equals(jpy2));
		System.assert(!jpy1.equals(COMPARE));
		System.assert(!jpy1.equals(null));
	}
}
