/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 国レコードを操作するAPIを実装するクラス
 */
public with sharing class CountryResource {

	/**
	 * @description 国レコードを表すクラス
	 */
	public class Country implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 国名 */
		public String name;
		/** 国名(言語0) */
		public String name_L0;
		/** 国名(言語1) */
		public String name_L1;
		/** 国名(言語2) */
		public String name_L2;
		/** 国コード */
		public String code;

		/**
		 * パラメータを検証する
		 * @param isUpdate レコード更新時はtrue、作成時はfalse
		 * リクエストパラメータに関するエラーメッセージは、
		 * 本来はAPIを使用する人（開発者等）向けに、API仕様書の項目名で表示する必要があるが、
		 * 現在は画面にエラーメッセージが表示されてしまうため、多言語対応している。
		 */
		public void validate(Boolean isUpdate) {

			// 作成時のパラメータ検証
			if (!isUpdate) {
				// 国コード
				if (String.isBlank(this.code)) {
					App.ParameterException e = new App.ParameterException();
					e.setMessageIsBlank(ComMessage.msg().Com_Lbl_Code);
					throw e;
				}
				// 国名(L0)
				if (String.isBlank(this.name_L0)) {
					App.ParameterException e = new App.ParameterException();
					e.setMessageIsBlank(ComMessage.msg().Admin_Lbl_Name);
					throw e;
				}

			// 更新時のパラメータ検証
			} else {
				// レコードID
				if (String.isBlank(this.id)) {
					App.ParameterException e = new App.ParameterException();
					e.setMessageIsBlank('id');
					throw e;
				}
				try {
					System.Id.valueOf(this.id);
				} catch (StringException e) {
					throw new App.ParameterException('id', id);
				}
			}
		}
	}

	/**
	 * @description レコードのルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目 */
		public String name;
	}

	/**
	 * @description 国レコード作成結果
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成されたレコードID */
		public String id;
	}

	/**
	 * @description 国レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description 国レコードを1件作成する
		 * @param req リクエストパラメータ (CountryResource.Country)
		 * @return レスポンス (CountryResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			CountryResource.Country param = (CountryResource.Country)req.getParam(CountryResource.Country.class);

			// 入力値チェック
			param.validate(false);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// レコードを作成
			Id recordId = CountryResource.saveRecord(param, req.getParamMap(), false);

			// レスポンス作成
			SaveResult res = new SaveResult();
			res.id = recordId;
			return res;
		}
	}

	/**
	 * @description 国レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description 国レコードを1件更新する
		 * @param  req リクエストパラメータ (CountryResource.Country)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			CountryResource.Country param = (CountryResource.Country)req.getParam(CountryResource.Country.class);

			// 入力値チェック
			param.validate(true);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// レコードを更新
			CountryResource.saveRecord(param, req.getParamMap(), true);

			return null;
		}
	}

	/**
	 * @description 国レコードを1件作成または更新する<br/>
	 *    ※他のクラスから参照しないで下さい<br/>
	 *    TODO:DAOクラスに移動
	 * @param  param 国のレコード値を持つパラメータ
	 * @param  paramMap 国のレコード値を持つパラメータのマップ
	 * @param  isUpdate レコード更新時はtrue
	 * @return 作成または更新した国レコード
	 */
	public static Id saveRecord(CountryResource.Country param, Map<String, Object> paramMap, Boolean isUpdate) {
		Set<String> paramKeys = paramMap.keySet();

		// エンティティ作成
		CountryEntity entity = new CountryEntity();

		if (isUpdate) {
			entity.setId(param.Id);
		}
		if (paramKeys.contains('name_L0')) {
			entity.name = param.name_L0;
			entity.nameL0 = param.name_L0;
		}
		if (paramKeys.contains('name_L1')) {
			entity.nameL1 = param.name_L1;
		}
		if (paramKeys.contains('name_L2')) {
			entity.nameL2 = param.name_L2;
		}
		if (paramKeys.contains('code')) {
			entity.code = param.code;
		}

		// エンティティのバリデーションチェック
		Validator.Result validResult = new ComConfigValidator.CountryValidator(entity).validate();
		if (!validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// コードの重複チェック
		if (CountryResource.isExistCode(entity, isUpdate)) {
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateCode);
		}

		// 国レコードの作成または更新
		CountryRepository repo = new CountryRepository();
		Repository.SaveResult sr = repo.saveEntity(entity);
		return sr.details[0].Id;
	}

	/*
	* コードが重複していないことを確認する
	* @param entity 登録対象のエンティティ
	* @param isUpdate レコード更新時はtrue、作成時はfalse
	* @return true(重複あり) / false(重複なし)
	*/
	@testVisible
	private static boolean isExistCode(CountryEntity entity, boolean isUpdate) {

		CountryRepository repo = new CountryRepository();
		CountryEntity result = repo.searchByCode(entity.code);
		if (isUpdate) {
			// 更新の場合、エンティティと検索結果のidが同じであるか確認する
			return result != null && result.Id != entity.Id;
		} else {
			// 新規の場合
			return result != null;
		}
	}

	/**
	 * @description 国レコード削除処理のパラメータクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {
			// 削除対象のレコードID
			if (String.isBlank(this.id)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('id');
				throw e;
			}
			try {
				System.Id.valueOf(this.id);
			} catch (StringException e) {
				throw new App.ParameterException('id', id);
			}
		}
	}

	/**
	 * @description 国レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description 国レコードを1件削除する
		 * @param  req リクエストパラメータ (CountryResource.DeleteOption)
		 * @return null
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// 入力値チェック
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new CountryRepository().deleteEntity(param.Id);

			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

					// TODO 暫定的にデバッグログを出力しておきます
					System.debug(e);

					// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
					//      暫定対応として下記のようなエラーメッセージで対応します。
					// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
					e.setMessage(ComMessage.msg().Com_Err_FaildDeleteReference);
					throw e;
	 			}
			}

			return null;
		}
	}

	/**
	 * @description 検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
	}

	/**
	 * @description レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public Country[] records;
	}

	/**
	 * @description 国レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 国レコードを検索する
		 * @param  req リクエストパラメータ(SearchCondition)
		 * @return レスポンス
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);
			Map<String, Object> paramMap = req.getParamMap();

			// 検索条件を設定
			CountryRepository.SearchFilter filter =  new CountryRepository.SearchFilter();
			if (param.id != null) {
				filter.ids.add(param.id);
			}

			// レコードを検索
			List<CountryEntity> searchRecords = new CountryRepository().searchEntityList(filter);

			// レスポンスクラスに変換
			List<Country> countryList = new List<Country>();
			for(CountryEntity sr : searchRecords){
				Country c = new Country();
				c.id = sr.id;
				c.name = sr.nameL.getValue();
				c.name_L0 = sr.nameL0;
				c.name_L1 = sr.nameL1;
				c.name_L2 = sr.nameL2;
				c.code = sr.code;

				countryList.add(c);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = countryList;
			return res;
		}
	}
}