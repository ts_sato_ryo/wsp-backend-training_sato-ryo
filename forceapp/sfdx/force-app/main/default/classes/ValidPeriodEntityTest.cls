/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通
 *
 * @description 履歴管理方式が有効期間型のマスタのエンティティ基底クラスのテスト
 */
@isTest
private class ValidPeriodEntityTest {

	/**
	 * @description isValidが正しく動作することを確認する
	 */
	@isTest static void isValidTest() {
		ValidPeriodEntity entity = new AttLeaveEntity();
		AppDate targetDate;

		// Test1: 有効開始日がnull → false
		entity.validFrom = null;
		entity.validTo = AppDate.today();
		targetDate = AppDate.today();
		System.assertEquals(false, entity.isValid(targetDate));

		// Test2: 失効日がnull → false
		entity.validFrom = AppDate.today();
		entity.validTo = null;
		targetDate = AppDate.today();
		System.assertEquals(false, entity.isValid(targetDate));

		// Test3: 対象日が有効開始日前 → false
		entity.validFrom = AppDate.today();
		entity.validTo = entity.validFrom.addMonths(1);
		targetDate = entity.validFrom.addDays(-1);
		System.assertEquals(false, entity.isValid(targetDate));

		// Test4: 対象日が有効開始日 → true
		entity.validFrom = AppDate.today();
		entity.validTo = entity.validFrom.addMonths(1);
		targetDate = entity.validFrom;
		System.assertEquals(true, entity.isValid(targetDate));

		// Test5: 対象日が失効日 → false
		entity.validFrom = AppDate.today();
		entity.validTo = entity.validFrom.addMonths(1);
		targetDate = entity.validTo;
		System.assertEquals(false, entity.isValid(targetDate));

		// Test6: 対象日が失効日の前日 → true
		entity.validFrom = AppDate.today();
		entity.validTo = entity.validFrom.addMonths(1);
		targetDate = entity.validTo.addDays(-1);
		System.assertEquals(true, entity.isValid(targetDate));
	}
}