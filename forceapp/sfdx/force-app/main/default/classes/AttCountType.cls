/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤務カウント種別の値クラス
 */
public with sharing class AttCountType extends ValueObjectType {
	/** 出勤とみなす */
	public static final AttCountType ATTENDANCE = new AttCountType('Attendance');
	/** 欠勤とみなす */
	public static final AttCountType ABSENCE = new AttCountType('Absence');
	/** 勤務日として扱わない */
	public static final AttCountType NOT_WORK_DAY = new AttCountType('NotWorkDay');

	/** 休暇範囲のリスト（範囲が追加されら本リストにも追加してください) */
	public static final List<AttCountType> TYPE_LIST;
	static {
		TYPE_LIST = new List<AttCountType> {
			ATTENDANCE,
			ABSENCE,
			NOT_WORK_DAY
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private AttCountType(String value) {
		super(value);
	}

	/**
	 * AttCountType
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つAttLeaveType
	 */
	public static AttCountType valueOf(String value) {
		AttCountType retType = null;
		if (String.isNotBlank(value)) {
			for (AttCountType type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// AttDayType以外の場合はfalse
		Boolean eq;
		if (compare instanceof AttCountType) {
			// 値が同じであればtrue
			if (this.value == ((AttCountType)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}
