/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description ExpReportTypeEntityのテスト Test class for ExpReportTypeEntity
 */
@isTest
private class ExpReportTypeEntityTest {
	/**
	 * テストデータクラス Test Data Class
	 */
	private class TestData extends TestData.TestDataEntity {
		/** 拡張項目データ */
		public List<ExtendedItemEntity> extendedItems = new List<ExtendedItemEntity>();
		/** 拡張項目(選択リスト)データ Extended item data(Picklist) */
		public List<ExtendedItemEntity> extendedItemsPicklist = new List<ExtendedItemEntity>();
		/** 拡張項目使用 */
		public ComExtendedItemUsedIn extendedItemUsedIn;
		/** 拡張項目必須 */
		public ComExtendedItemRequiredFor extendedItemRequiredFor;

		/**
		 * コンストラクタ
		 */
		public TestData() {
			super();
			// 拡張項目データ作成
			List<Id> extendedItemIdList = new List<Id>();
			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItems('Extended Item', this.company.id, 10)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItems = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);

			// 選択リスト Picklist
			for (ComExtendedItem__c item : ComTestDataUtility.createExtendedItems('Extended Item Picklist', this.company.id, ExtendedItemInputType.PICKLIST, 10)) {
				extendedItemIdList.add(item.Id);
			}
			this.extendedItemsPicklist = (new ExtendedItemRepository()).getEntityList(extendedItemIdList);

			// 拡張項目使用作成
			this.extendedItemUsedIn = ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;

			// 拡張項目必須作成
			this.extendedItemRequiredFor = ComExtendedItemRequiredFor.EXPENSE_REPORT;
		}

		/**
		 * 経費申請タイプオブジェクトを作成する Create Expense Report Type records
		 */
		public List<ExpReportTypeEntity> createExpReportTypes(String name, Integer size) {
			return createExpReportTypes(name, this.company.id, size);
		}

		/**
		 * 経費申請タイプオブジェクトを作成する Create Expense Report Type records
		 */
		public List<ExpReportTypeEntity> createExpReportTypes(String name, Id companyId, Integer size) {
			ComTestDataUtility.createExpReportTypes(name, companyId, size);
			return (new ExpReportTypeRepository()).getEntityList(null);
		}

		/**
		 * 経費申請タイプエンティティを作成する(DB保存なし) Create an Expense Report Type Entity (Do not update to DB)
		 */
		public ExpReportTypeEntity createExpReportTypeEntity(String name) {
			ExpReportTypeEntity entity = new ExpReportTypeEntity();

			entity.active = true;
			entity.name = name;
			entity.code = name;
			entity.uniqKey = this.company.code + '-' + entity.code;
			entity.nameL0 = name + '_L0';
			entity.nameL1 = name + '_L1';
			entity.nameL2 = name + '_L2';
			entity.companyId = this.company.id;
			entity.costCenterRequiredFor = ComCostCenterRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			entity.costCenterUsedIn = ComCostCenterUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			entity.jobRequiredFor = ComJobRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			entity.jobUsedIn = ComJobUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			entity.vendorRequiredFor = ExpVendorRequiredFor.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			entity.vendorUsedIn = ExpVendorUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT;
			entity.descriptionL0 = 'Description_L0';
			entity.descriptionL1 = 'Description_L1';
			entity.descriptionL2 = 'Description_L2';

			for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
				entity.setExtendedItemTextId(i, this.extendedItems[i].id);
				entity.setExtendedItemTextUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemTextRequiredFor(i, this.extendedItemRequiredFor);
				entity.setExtendedItemPicklistId(i, this.extendedItemsPicklist[i].id);
				entity.setExtendedItemPicklistUsedIn(i, this.extendedItemUsedIn);
				entity.setExtendedItemPicklistRequiredFor(i, this.extendedItemRequiredFor);
			}

			return entity;
		}
	}

	/**
	 * ユニークキーが正しく作成できることを確認する Unique Key Test
	 */
	@isTest static void createUniqKeyTest() {
		ExpReportTypeEntity entity = new ExpReportTypeEntity();

		// 会社コードと費目グループコードが空でない場合
		// ユニークキーが作成される
		entity.code = 'GroupCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// 費目グループコードが空の場合
		// 例外が発生する
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}

		// 会社コードが空の場合
		// 例外が発生する
		entity.code = 'ExpReportTypeCode';
		companyCode = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/**
	 * copyのテスト Copy Test
	 * エンティティが複製できることを確認する Verify entity can be replicated
	 */
	@isTest static void copyTest() {
		ExpReportTypeEntityTest.TestData testData = new TestData();
		ExpReportTypeEntity orgEntity = testData.createExpReportTypes('Test', 2)[1];

		ExpReportTypeEntity copyEntity = orgEntity.copy();

		System.assertEquals(null, copyEntity.id);  // IDはコピーされない
		System.assertEquals(orgEntity.name, copyEntity.name);
		System.assertEquals(orgEntity.active, copyEntity.active);
		System.assertEquals(orgEntity.code, copyEntity.code);
		System.assertEquals(orgEntity.companyId, copyEntity.companyId);
		System.assertEquals(orgEntity.costCenterRequiredFor.value, copyEntity.costCenterRequiredFor.value);
		System.assertEquals(orgEntity.costCenterUsedIn.value, copyEntity.costCenterUsedIn.value);
		System.assertEquals(orgEntity.descriptionL0, copyEntity.descriptionL0);
		System.assertEquals(orgEntity.descriptionL1, copyEntity.descriptionL1);
		System.assertEquals(orgEntity.descriptionL2, copyEntity.descriptionL2);
		System.assertEquals(orgEntity.useFileAttachment, copyEntity.useFileAttachment);
		System.assertEquals(orgEntity.vendorRequiredFor.value, copyEntity.vendorRequiredFor.value);
		System.assertEquals(orgEntity.vendorUsedIn.value, copyEntity.vendorUsedIn.value);

		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			System.assertEquals(orgEntity.getExtendedItemTextId(i), copyEntity.getExtendedItemTextId(i));
			System.assertEquals(orgEntity.getExtendedItemTextUsedIn(i), copyEntity.getExtendedItemTextUsedIn(i));
			System.assertEquals(orgEntity.getExtendedItemTextRequiredFor(i), copyEntity.getExtendedItemTextRequiredFor(i));

			System.assertEquals(orgEntity.getExtendedItemPicklistId(i), copyEntity.getExtendedItemPicklistId(i));
			System.assertEquals(orgEntity.getExtendedItemPicklistUsedIn(i), copyEntity.getExtendedItemPicklistUsedIn(i));
			System.assertEquals(orgEntity.getExtendedItemPicklistRequiredFor(i), copyEntity.getExtendedItemPicklistRequiredFor(i));
		}
		System.assertEquals(orgEntity.jobRequiredFor.value, copyEntity.jobRequiredFor.value);
		System.assertEquals(orgEntity.jobUsedIn.value, copyEntity.jobUsedIn.value);
		System.assertEquals(orgEntity.nameL0, copyEntity.nameL0);
		System.assertEquals(orgEntity.nameL1, copyEntity.nameL1);
		System.assertEquals(orgEntity.nameL2, copyEntity.nameL2);
		System.assertEquals(orgEntity.uniqKey, copyEntity.uniqKey);
		System.assertEquals(orgEntity.vendorRequiredFor.value, copyEntity.vendorRequiredFor.value);
		System.assertEquals(orgEntity.vendorUsedIn.value, copyEntity.vendorUsedIn.value);
	}
}