/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for Cost Center History Resource (API Testing)
 */
@IsTest
private class CostCenterHistoryResourceTest {

	/*
	 * Normal Use Case Test: Create a Cost Center History for an Existing Cost Center
	 */
	@IsTest
	static void createCostCenterHistoryPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c parent = createCostCenter('Root Cost Center', 'LVL_0_CC', company.Id, null);
		ComCostCenterBase__c base = createCostCenter('1st Child Cost Center', 'LVL_1_CC', company.Id, null);
		Date currentDate = Date.today();

		Test.startTest();
		CostCenterHistoryResource.CostCenterHistory param = createHistoryParam(
			base.id, 'TestCostCenter Name', parent.Id, 'SAMPLE_LINKAGE_CODE',
			AppDate.valueOf(currentDate + 1).format(), AppDate.valueOf(currentDate + 5).format(),
			'Testing Cost Center History Create API'
		);
		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		CostCenterHistoryResource.SaveResult result = (CostCenterHistoryResource.SaveResult) api.execute(param);

		Test.stopTest();

		List<ComCostCenterBase__c> retrievedBaseRecords = getBaseRecords(parent.Id);

		System.assertEquals(1, retrievedBaseRecords.size());
		System.assertEquals(2, retrievedBaseRecords[0].Histories__r.size());

		ComCostCenterBase__c retrievedBase = retrievedBaseRecords[0];
		ComCostCenterHistory__c retrievedHistory = retrievedBase.Histories__r[1];

		verifyHistoryValue(param, result.id, retrievedBase, retrievedHistory, false);
	}


	/*
	 * Normal Use Case Test: Create a Cost Center History which has the same date as the original one.
	 */
	@IsTest
	static void createCostCenterHistoryOverwritePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c parent = createCostCenter('Root Cost Center', 'LVL_0_CC', company.Id, null);
		ComCostCenterBase__c base = createCostCenter('1st Child Cost Center', 'LVL_1_CC', company.Id, null);
		Date currentDate = Date.today();

		Test.startTest();
		// Use the same start date as the existing history
		CostCenterHistoryResource.CostCenterHistory param = createHistoryParam(
			base.id, 'TestCostCenter Name', parent.Id, 'SAMPLE_LINKAGE_CODE',
			AppDate.valueOf(currentDate - 60).format(), AppDate.valueOf(currentDate + 90).format(),
			'Testing Cost Center History Create API'
		);

		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		CostCenterHistoryResource.SaveResult result = (CostCenterHistoryResource.SaveResult) api.execute(param);
		Test.stopTest();

		List<ComCostCenterBase__c> retrievedBaseRecords = getBaseRecords(parent.Id);

		System.assertEquals(1, retrievedBaseRecords.size());
		System.assertEquals(2, retrievedBaseRecords[0].Histories__r.size());
		System.assertEquals(true, retrievedBaseRecords[0].Histories__r[0].Removed__c);
		System.assertEquals(false, retrievedBaseRecords[0].Histories__r[1].Removed__c);

		ComCostCenterBase__c retrievedBase = retrievedBaseRecords[0];
		ComCostCenterHistory__c retrievedHistory = retrievedBase.Histories__r[1];

		verifyHistoryValue(param, result.id, retrievedBase, retrievedHistory, false);
	}

	/*
	 * Normal Use Case Test: Create a Cost Center History which has the same ValidFrom date as the original one
	 * but ends earlier than orignal History.
	 */
	@IsTest
	static void createCostCenterHistoryOverwriteShortenPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c parent = createCostCenter('Root Cost Center', 'LVL_0_CC', company.Id, null);
		ComCostCenterBase__c base = createCostCenter('1st Child Cost Center', 'LVL_1_CC', company.Id, null);
		Date currentDate = Date.today();

		Test.startTest();
		// Use the same start date as the existing history, but ends ealier than existing
		CostCenterHistoryResource.CostCenterHistory param = createHistoryParam(
			base.id, 'TestCostCenter Name', parent.Id, 'SAMPLE_LINKAGE_CODE',
			AppDate.valueOf(currentDate - 60).format(), AppDate.valueOf(currentDate + 2).format(),
			'Testing Cost Center History Create API'
		);

		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		CostCenterHistoryResource.SaveResult result = (CostCenterHistoryResource.SaveResult) api.execute(param);
		Test.stopTest();

		List<ComCostCenterBase__c> retrievedBaseRecords = getBaseRecords(parent.Id);

		System.assertEquals(1, retrievedBaseRecords.size());
		System.assertEquals(2, retrievedBaseRecords[0].Histories__r.size());
		System.assertEquals(true, retrievedBaseRecords[0].Histories__r[0].Removed__c);
		System.assertEquals(false, retrievedBaseRecords[0].Histories__r[1].Removed__c);

		ComCostCenterBase__c retrievedBase = retrievedBaseRecords[0];
		ComCostCenterHistory__c retrievedHistory = retrievedBase.Histories__r[1];

		verifyHistoryValue(param, result.id, retrievedBase, retrievedHistory, false);
	}

	/*
	 * Invalid Param/Missing Param when Creating new CostCenter History
	 */
	@isTest
	static void createCostCenterHistoryMissingParamNegativeTest() {
		Test.startTest();

		CostCenterHistoryResource.CostCenterHistory param = new CostCenterHistoryResource.CostCenterHistory();
		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/*
	 * Test that creating history with date earlier than existing will throw exception.
	 */
	@isTest
	static void createCostCenterHistoryEarlierThanExistingHistoryNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c parent = createCostCenter('Root Cost Center', 'LVL_0_CC', company.Id, null);
		ComCostCenterBase__c base = createCostCenter('1st Child Cost Center', 'LVL_1_CC', company.Id, null);
		Date currentDate = Date.today();

		Test.startTest();
		// Use the earlier start date as the existing history
		CostCenterHistoryResource.CostCenterHistory param = createHistoryParam(
			base.id, 'TestCostCenter Name', parent.Id, 'SAMPLE_LINKAGE_CODE',
			AppDate.valueOf(currentDate - 90).format(), AppDate.valueOf(currentDate + 90).format(),
			'Testing Cost Center History Create API Earlier than Existing History'
		);

		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();

		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/*
	 * Test that if specified linkage code is already in used, exception is thrown
	 */
	/*@IsTest
	static void createCostCenterHistoryDuplicateLinkageCodeNegativeTest() {
	    // Linkage Code was changed not to be unique and there will be no checking of linkageCode

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c base = createCostCenter('Root Cost Center', 'LVL_0_CC', company.Id, null);
		Date currentDate = Date.today();

		Test.startTest();
		// Use the same start date as the existing history
		CostCenterHistoryResource.CostCenterHistory param = createHistoryParam(
			base.id, 'TestCostCenter Name', null, 'LVL_0_CCLVL_0_CC',
			AppDate.valueOf(currentDate + 60).format(), AppDate.valueOf(currentDate + 90).format(),
			'Testing Cost Center History Create API Duplicate Linkage Code'
		);

		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();

		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}*/

	/*
	 * Test to ensure a record cannot specify it's own child as parent.
	 */
	@IsTest
	static void createCostCenterHistoryParentingChildNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c A = createCostCenter('A CC', 'LVL_A_CC', company.Id, null);
		ComCostCenterBase__c AA = createCostCenter('AA CC', 'LVL_AA_CC', company.Id, A.Id);
		ComCostCenterBase__c AAA = createCostCenter('AAA CC', 'LVL_AAA_CC', company.Id, AA.Id);
		Date currentDate = Date.today();

		Test.startTest();
		CostCenterHistoryResource.CostCenterHistory param = createHistoryParam(
			A.id, 'TestCostCenter Name', AAA.Id, 'SAMPLE_LINKAGE_CODE',
			AppDate.valueOf(currentDate + 1).format(), AppDate.valueOf(currentDate + 30).format(),
			'Testing Parenting own Child'
		);

		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		String exceptionMessage;
		try {
			CostCenterHistoryResource.SaveResult result = (CostCenterHistoryResource.SaveResult) api.execute(param);
		} catch(App.ParameterException e) {
			exceptionMessage = e.getMessage();
		}
		Test.stopTest();
		System.assertEquals(ComMessage.msg().Com_Err_CannotParentItsChild, exceptionMessage);
	}

	/*
	 * Test to ensure a record cannot specify it's own Future child as parent.
	 */
	@IsTest
	static void createCostCenterHistoryParentingFutureChildNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c A = createCostCenter('A CC', 'LVL_A_CC', company.Id, null);
		ComCostCenterBase__c AA = createCostCenter('AA CC', 'LVL_AA_CC', company.Id, A.Id);
		ComCostCenterBase__c AAA = createCostCenter('AAA CC', 'LVL_AAA_CC', company.Id, null);
		Date currentDate = Date.today();

		//Make AA the parent of AAA in the future
		ComCostCenterHistory__c history = new ComCostCenterHistory__c(
			UniqKey__c = 'dummy_uniq_key',
			BaseId__c = AAA.Id,
			Name = AAA.Name,
			Name_L0__c = AAA.Name + ' L0',
			Name_L1__c = AAA.Name + ' L1',
			Name_L2__c = AAA.Name + ' L2',
			ParentBaseId__c = AA.Id,
			LinkageCode__c = 'dummy_linkage',
			ValidFrom__c = System.today().addMonths(3),
			ValidTo__c = System.today().addMonths(6),
			HistoryComment__c = 'Future Child'
		);
		insert history;


		Test.startTest();
		CostCenterHistoryResource.CostCenterHistory param = new CostCenterHistoryResource.CostCenterHistory();
		param.baseId = A.id;
		param.name_L0 = 'TestCostCenter Name_L0';
		param.name_L1 = 'TestCostCenter Name_L1';
		param.name_L2 = 'TestCostCenter Name_L2';
		param.parentId = AAA.Id;		//At this point of creating this history, AAA isn't grand child of A yet.
		param.linkageCode = 'SAMPLE_LINKAGE_CODE';
		param.validDateFrom = AppDate.valueOf(currentDate + 1).format();
		param.validDateTo = AppDate.valueOf(currentDate + 30).format();
		param.comment = 'Testing Parenting own Future Grand Child';
		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		String exceptionMessage;
		try {
			CostCenterHistoryResource.SaveResult result = (CostCenterHistoryResource.SaveResult) api.execute(param);
		} catch(App.ParameterException e) {
			exceptionMessage = e.getMessage();
		}
		Test.stopTest();
		System.assertEquals(ComMessage.msg().Com_Err_CannotParentItsChild, exceptionMessage);
	}

	/*
	 * Test that user cannot create a new History record with a Valid From date which will cause a gap btween
	 * the latest existing History record and the newly created History record.
	 * Basically, to test that the new History record ValidFrom must be the same or earlier than the
	 * latest existing History record.
	 */
	@IsTest
	static void createCostCenterHistoryGapWithExistingNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		//Shift start by 14 months earlier making the existing History Record as
		// Index 0: Past; Index 1: Current; Index 2: Future
		ComCostCenterBase__c  existingBase = ComTestDataUtility.createCostCentersWithHistory(
			'Gap Test', company.Id, 1, 3, true, -14)[0];

		List<ComCostCenterHistory__c> existingHistories = [
			SELECT Id, Name_L0__c, ValidFrom__c, ValidTo__c
			FROM ComCostCenterHistory__c
		];

		Date currentDate = Date.today();

		CostCenterHistoryResource.CostCenterHistory param = new CostCenterHistoryResource.CostCenterHistory();
		param.baseId = existingBase.id;
		param.name_L0 = 'GapTestNew_L0';
		param.name_L1 = 'GapTestNew_L0_L1';
		param.name_L2 = 'GapTestNew_L0_L2';
		param.linkageCode = 'GapTestNew_LINK_CODE';

		//Create a Cost Center History which starts later (gap) than exiting Valid To Date
		ComCostCenterHistory__c existingFutureHistory = existingHistories[2];
		param.validDateFrom = AppDate.valueOf(existingFutureHistory.ValidTo__c + 30).format();
		param.validDateTo = AppDate.valueOf(existingFutureHistory.ValidTo__c + 60).format();
		param.comment = 'Testing create a new history after the last known is expired';
		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		App.ParameterException expectedException;
		Test.startTest();
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			expectedException = e;
		}
		Test.stopTest();
		System.assertNotEquals(null, expectedException);
		System.assertEquals(ComMessage.msg().Admin_Err_InvalidRevisionGap, expectedException.getMessage());

		//Verify that existing history isn't changed
		List<ComCostCenterHistory__c> afterExecutionHistoryList = [
			SELECT Id, Name_L0__c, ValidFrom__c, ValidTo__c
			FROM ComCostCenterHistory__c
			WHERE Removed__c = false
		];

		System.assertEquals(existingHistories.size(), afterExecutionHistoryList.size());
		for (Integer i = 0; i < afterExecutionHistoryList.size(); i++) {
			System.assertEquals(existingHistories[i].Name_L0__c, afterExecutionHistoryList[i].Name_L0__c);
			System.assertEquals(existingHistories[i].ValidFrom__c, afterExecutionHistoryList[i].ValidFrom__c);
			System.assertEquals(existingHistories[i].ValidTo__c, afterExecutionHistoryList[i].ValidTo__c);
		}
	}

	/*
	 * Test that a new Cost Center History can be created on the same date as the
	 * existing latest history's Valid To date.
	 */
	@IsTest
	static void createCostCenterHistoryStartingOnSameDateWithExistingValidToDatePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		//Shift start by 14 months earlier making the existing History Record as
		// Index 0: Past; Index 1: Current; Index 2: Future
		ComCostCenterBase__c  existingBase = ComTestDataUtility.createCostCentersWithHistory(
				'Continuous', company.Id, 1, 3, true, -14)[0];

		List<ComCostCenterHistory__c> existingHistories = [
			SELECT Id, Name_L0__c, ValidFrom__c, ValidTo__c
			FROM ComCostCenterHistory__c
		];

		Date currentDate = Date.today();

		CostCenterHistoryResource.CostCenterHistory param = new CostCenterHistoryResource.CostCenterHistory();
		param.baseId = existingBase.id;
		param.name_L0 = 'ContinuousTestNew_L0';
		param.name_L1 = 'ContinuousTestNew_L0_L1';
		param.name_L2 = 'ContinuousTestNew_L0_L2';
		param.linkageCode = 'ContinuousLINK_CODE';

		//Create a Cost Center History which starts exactly on the last known histor's ValidTo Date
		ComCostCenterHistory__c existingFutureHistory = existingHistories[2];
		param.name_L0 = 'New CT';
		param.validDateFrom = AppDate.valueOf(existingFutureHistory.ValidTo__c).format();
		param.validDateTo = AppDate.valueOf(existingFutureHistory.ValidTo__c + 60).format();
		param.comment = 'Testing create a new history which starts exactly after the last known is expired';
		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		Test.startTest();
		CostCenterHistoryResource.SaveResult result = (CostCenterHistoryResource.SaveResult) api.execute(param);
		Test.stopTest();

		//Verify that existing history isn't changed
		List<ComCostCenterHistory__c> afterExecutionHistoryList = [
				SELECT Id, Name_L0__c, ValidFrom__c, ValidTo__c
				FROM ComCostCenterHistory__c
				WHERE Removed__c = false
		];

		//There should be one more history
		System.assertEquals(existingHistories.size() + 1, afterExecutionHistoryList.size());
		//Existing history shouldn't be modified
		for (Integer i = 0; i < existingHistories.size(); i++) {
			System.assertEquals(existingHistories[i].Name_L0__c, afterExecutionHistoryList[i].Name_L0__c);
			System.assertEquals(existingHistories[i].ValidFrom__c, afterExecutionHistoryList[i].ValidFrom__c);
			System.assertEquals(existingHistories[i].ValidTo__c, afterExecutionHistoryList[i].ValidTo__c);
		}
		//Verify the newly created History
		ComCostCenterHistory__c newlyCreatedHistory = afterExecutionHistoryList.get(afterExecutionHistoryList.size() - 1);
		System.assertEquals(param.name_L0, newlyCreatedHistory.Name_L0__c);
		System.assertEquals(AppDate.valueOf(param.validDateFrom).getDate(), newlyCreatedHistory.ValidFrom__c);
		System.assertEquals(AppDate.valueOf(param.validDateTo).getDate(), newlyCreatedHistory.ValidTo__c);
	}

	/*
	 * When a new History record which starts before than the latest existing record ValidTo date,
	 * the ValidTo date of the existing latest record should be shorten and set to be the date as ValidFrom date of new Record.
	 */
	@IsTest
	static void createCostCenterHistoryShortenExistingPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c base = createCostCenter('A CC', 'LVL_A_CC', company.Id, null);
		Date currentDate = Date.today();

		Test.startTest();
		CostCenterHistoryResource.CostCenterHistory param = new CostCenterHistoryResource.CostCenterHistory();
		param.baseId = base.id;
		param.name_L0 = 'TestCostCenter Name_L0';
		param.name_L1 = 'TestCostCenter Name_L1';
		param.name_L2 = 'TestCostCenter Name_L2';
		param.linkageCode = 'SAMPLE_LINKAGE_CODE';
		//Create a Cost Center History which starts before exiting latest record Valid To Date
		param.validDateFrom = AppDate.valueOf(currentDate).format();
		param.validDateTo = AppDate.valueOf(currentDate + 2).format();
		param.comment = 'Testing create a new history which shorten existing';
		CostCenterHistoryResource.CreateApi api = new CostCenterHistoryResource.CreateApi();
		CostCenterHistoryResource.SaveResult result = (CostCenterHistoryResource.SaveResult) api.execute(param);
		Test.stopTest();

		List<ComCostCenterHistory__c> retrievedHistoryList = [
				SELECT Id, Name_L0__c, ValidFrom__c, ValidTo__c
				FROM ComCostCenterHistory__c
				WHERE (BaseId__c = :base.id)
		];

		System.assertEquals(2, retrievedHistoryList.size());
		System.assertEquals(result.id, retrievedHistoryList[1].Id); //New History Created

		//Existing history's ValidTo should be shorten (becomes the same as new Record's Valid From)
		System.assertEquals(param.validDateFrom, AppDate.valueOf(retrievedHistoryList[0].ValidTo__c).format());

		//Validate the newly created history date range
		System.assertEquals(param.validDateFrom, AppDate.valueOf(retrievedHistoryList[1].ValidFrom__c).format());
		System.assertEquals(param.validDateTo, AppDate.valueOf(retrievedHistoryList[1].ValidTo__c).format());
	}

	/*
	 * Test Deleting the latest future history.
	 */
	@IsTest
	static void deleteHistoryPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer baseSize = 1;
		Integer historyPerBase = 3;
		// Shift by -20 months to make the first history a past history (it will results in "past, current, future" histories)
		ComCostCenterBase__c originalBase = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, baseSize, historyPerBase, true, -20
		)[0];

		List<ComCostCenterHistory__c> originalHistories = [
				SELECT Id, BaseId__c,
				ValidFrom__c, ValidTo__c
				FROM ComCostCenterHistory__c
		];

		CostCenterHistoryResource.DeleteParam param = new CostCenterHistoryResource.DeleteParam();
		param.id = originalHistories[historyPerBase - 1].Id;		//the last index is the future history
		Test.startTest();
		CostCenterHistoryResource.DeleteApi api = new CostCenterHistoryResource.DeleteApi();
		api.execute(param);
		Test.stopTest();

		List<ComCostCenterHistory__c> afterDeleteHistories = [
			SELECT Id, BaseId__c,
			ValidFrom__c, ValidTo__c
			FROM ComCostCenterHistory__c
			WHERE Removed__c = false
		];

		System.assertEquals(originalHistories.size() - 1, afterDeleteHistories.size());
		//Since the latest history is deleted, the 2 earlier history should be remained.
		for (Integer i = 0; i < afterDeleteHistories.size(); i++) {
			System.assertEquals(originalHistories[i].Id, afterDeleteHistories[i].Id);
			System.assertEquals(originalHistories[i].ValidFrom__c, afterDeleteHistories[i].ValidFrom__c);
			System.assertEquals(originalHistories[i].ValidTo__c, afterDeleteHistories[i].ValidTo__c);
		}
	}

	/*
	 * Test Deleting the only history left and it is a future history.
	 * The test for deleting current record is covered by another test.
	 */
	@IsTest
	static void deleteTheLastHistoryNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer baseSize = 1;
		Integer historyPerBase = 1;
		// Shift into future by 1 month. So that the Cost Center will only have 1 Record and that record is in the future.
		ComCostCenterBase__c originalBase = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, baseSize, historyPerBase, true, 1
		)[0];

		List<ComCostCenterHistory__c> originalHistories = [
				SELECT Id, BaseId__c,
				ValidFrom__c, ValidTo__c
				FROM ComCostCenterHistory__c
		];

		CostCenterHistoryResource.DeleteParam param = new CostCenterHistoryResource.DeleteParam();
		param.id = originalHistories[0].Id;
		App.IllegalStateException exceptedException;
		CostCenterHistoryResource.DeleteApi api = new CostCenterHistoryResource.DeleteApi();
		Test.startTest();
		try {
			api.execute(param);
		} catch (App.IllegalStateException e) {
			exceptedException = e;
		}

		Test.stopTest();

		System.assertNotEquals(null, exceptedException);
		System.assertEquals(ComMessage.msg().Com_Err_CannotDeletedNotExistHistory, exceptedException.getMessage());

		// Checks that no record gets deleted
		verifyExpectedNumberOfHistoriesLeft(historyPerBase);
	}

	/*
	 * Test Deleting the current history
	 */
	@IsTest
	static void deleteCurrentHistoryNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer baseSize = 1;
		Integer historyPerBase = 2;
		// Shift the start date by -14 months. It will be (index 0: past record, index 1: current record)
		ComCostCenterBase__c originalBase = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, baseSize, historyPerBase, true, -14
		)[0];

		List<ComCostCenterHistory__c> originalHistories = [
				SELECT Id, BaseId__c,
				ValidFrom__c, ValidTo__c
				FROM ComCostCenterHistory__c
		];

		CostCenterHistoryResource.DeleteParam param = new CostCenterHistoryResource.DeleteParam();
		param.id = originalHistories[1].Id;		//Select the current History record
		CostCenterHistoryResource.DeleteApi api = new CostCenterHistoryResource.DeleteApi();
		App.ParameterException exceptedException;
		Test.startTest();
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			exceptedException = e;
		}
		Test.stopTest();
		System.assertNotEquals(null, exceptedException);
		System.assertEquals(ComMessage.msg().Com_Err_CannotDeletedCurrentAndPastHistory, exceptedException.getMessage());

		// Checks that no record gets deleted
		verifyExpectedNumberOfHistoriesLeft(historyPerBase);
	}

	/*
	 * Test Deleting the a future history which is not the latest (meaning there is another future history record after that)
	 */
	@IsTest
	static void deleteNonLatestFutureHistoryNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer baseSize = 1;
		Integer historyPerBase = 3;
		// Index 0: Current, Index 1: Future, Index 2: Further Future
		ComCostCenterBase__c originalBase = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, baseSize, historyPerBase, true, 0
		)[0];

		List<ComCostCenterHistory__c> originalHistories = [
				SELECT Id, BaseId__c,
				ValidFrom__c, ValidTo__c
				FROM ComCostCenterHistory__c
		];

		CostCenterHistoryResource.DeleteParam param = new CostCenterHistoryResource.DeleteParam();
		param.id = originalHistories[1].Id;		//Select the future record (index 2 is the next future)

		CostCenterHistoryResource.DeleteApi api = new CostCenterHistoryResource.DeleteApi();
		App.ParameterException exceptedException;

		Test.startTest();
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			exceptedException = e;
		}

		Test.stopTest();

		System.assertNotEquals(null, exceptedException);
		System.assertEquals(ComMessage.msg().Com_Err_CannotDeletedInBetweenHistory, exceptedException.getMessage());

		 // Checks that no record gets deleted
		verifyExpectedNumberOfHistoriesLeft(historyPerBase);
	}


	/**
	 * Normal Search Use Case: Positive Test
	 * This test focus on the number of records (details of retrieved records are tested in another test).
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 3;
		Integer numberOfHistoryRecordPerBaseRecord = 3;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true);

		ComCostCenterHistory__c history = [SELECT Id, BaseId__c FROM ComCostCenterHistory__c WHERE BaseId__c IN :costCenters LIMIT 1];

		Test.startTest();
		Map<String, Object> idParamMap = new Map<String, Object> {
		'id' => history.Id
		};
		Map<String, Object> baseIdParamMap = new Map<String, Object> {
		'baseId' => history.BaseId__c
		};
		Map<String, Object> idAndBaseIdParamMap = new Map<String, Object> {
		'id' => history.id,
		'baseId' => history.BaseId__c
		};

		CostCenterHistoryResource.SearchApi api = new CostCenterHistoryResource.SearchApi();
		CostCenterHistoryResource.SearchResult searchResultById = (CostCenterHistoryResource.SearchResult) api.execute(idParamMap);
		CostCenterHistoryResource.SearchResult searchResultByBaseId = (CostCenterHistoryResource.SearchResult) api.execute(baseIdParamMap);
		CostCenterHistoryResource.SearchResult searchResultByIdAndBaseId = (CostCenterHistoryResource.SearchResult) api.execute(idAndBaseIdParamMap);
		Test.stopTest();

		// Verify that the correct number if records are retrieved
		System.assertEquals(1, searchResultById.records.size());
		System.assertEquals(numberOfHistoryRecordPerBaseRecord, searchResultByBaseId.records.size());
		System.assertEquals(1, searchResultByIdAndBaseId.records.size());

		// Verify that
		// Search with ID and get back the original record back
		System.assertEquals((String) idParamMap.get('id'), searchResultById.records[0].id);
		// Searched with ParentID, but the expected is the child record (since the parent has only one child for that record)
		System.assertEquals((String) baseIdParamMap.get('baseId'), searchResultByBaseId.records[0].baseId);
		// Search with both
		System.assertEquals((String) idAndBaseIdParamMap.get('id'), searchResultByIdAndBaseId.records[0].id);
	}

	/**
	 * Verify that the default "Name" field will be based on the Language Setting.
	 * Set the L0 as something which is not the user Language. When the record is retrieve,
	 * the Name__L1__c (user default language) should be in the Default "Name" field.
	 */
	@isTest
	static void searchCostCenterL0IsNotUserLanguagePositiveTest() {
		ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c costCenterBase = ComTestDataUtility.createCostCentersWithHistory(
				'Test CC', company.Id, 1, 1, true)[0];
		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c = :costCenterBase.Id LIMIT 1];

		Test.startTest();
		Map<String, Object> paramMap = new Map<String, Object> {
		'id' => history.Id
		};

		CostCenterHistoryResource.SearchApi api = new CostCenterHistoryResource.SearchApi();
		CostCenterHistoryResource.SearchResult result = (CostCenterHistoryResource.SearchResult) api.execute(paramMap);

		Test.stopTest();

		System.assertEquals(1, result.records.size());
		System.assertEquals((String) paramMap.get('id'), result.records[0].id);

		history = [
			SELECT Name_L0__c, Name_L1__c
			FROM ComCostCenterHistory__c WHERE Id =:history.Id LIMIT 1
		];
		// Since the user default language is specified as L1 before, name should be same as L1
		System.assertEquals(history.Name_L1__c, result.records[0].name);

		// This test depends on L0 and L1 having different value.
		// Ensures the previous L1 test isn't passing just because the names have the same value.
		System.assertNotEquals(history.Name_L0__c, result.records[0].name);
	}

	/**
	 * Verify the CostCenterHistory Search API without applying any filter.
	 * This test also checks that all the fields of retrieved data are correct.
	 */
	@isTest
	static void searchWithoutFilterPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer totalBases = 3;
		Integer historiesPerBase = 3;
		List<ComCostCenterBase__c> retrievedCostCenters = ComTestDataUtility.createCostCentersWithHistory(
				'Test CC', company.Id, totalBases, historiesPerBase, true);
		ComCostCenterHistory__c historyWithParentCostCenter = [
				SELECT Id FROM ComCostCenterHistory__c
				WHERE BaseId__c =:retrievedCostCenters[0].Id
		LIMIT 1
		];

		//Set another Cost Center as the Parent
		historyWithParentCostCenter.ParentBaseId__c = retrievedCostCenters[1].Id;
		update historyWithParentCostCenter;

		Test.startTest();
		Map<String, Object> paramMap = new Map<String, Object>();
		CostCenterHistoryResource.SearchApi api = new CostCenterHistoryResource.SearchApi();
		CostCenterHistoryResource.SearchResult result = (CostCenterHistoryResource.SearchResult) api.execute(paramMap);
		Test.stopTest();

		List<ComCostCenterHistory__c> retrievedHistories = [
				SELECT
				Id,
				BaseId__c,
				UniqKey__c,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				LinkageCode__c,
				ParentBaseId__c,
				ParentBaseId__r.Name,
				ValidFrom__c,
				ValidTo__c,
				HistoryComment__c
				FROM ComCostCenterHistory__c
		];

		ComCostCenterHistory__c parentHistory = [
				SELECT Id, Name_L0__c
				From ComCostCenterHistory__c
				WHERE BaseId__c = :historyWithParentCostCenter.ParentBaseId__c
		LIMIT 1
		];

		// The search using SOQL and API should return the same number of records
		System.assertEquals(retrievedHistories.size(), result.records.size());
		for (Integer baseIndex = 0; baseIndex < totalBases; baseIndex++) {
			for (Integer hIndex = 0; hIndex < historiesPerBase; hIndex++) {
				Integer baseOffset = totalBases * baseIndex;
				// Histories are created in the order of (current to future) and retrieved in order of (future to current)
				// Reverse the history to correct the order when comparing between the original created  and Search API result
				ComCostCenterHistory__c history = retrievedHistories[baseOffset + (historiesPerBase - hIndex - 1)];
				CostCenterHistoryResource.CostCenterHistory ccHistoryVo = result.records[baseOffset + hIndex];

				verifyHistoryValue(ccHistoryVo, history.Id, retrievedCostCenters.get(baseIndex), history, true);

				if (ccHistoryVo.parentId != null) {
					System.assertEquals(parentHistory.Name_L0__c, ccHistoryVo.parent.name);
				} else {
					System.assertEquals(true, String.isBlank(ccHistoryVo.parent.name));
				}
			}
		}
	}

	/*
	 * For creating the Cost Center Base + History which the test methods will depends on.
	 * As testing parent is currently only done here, this method is kept inside this class rather than ComTestDataUtil class.
	 */
	private static ComCostCenterBase__c createCostCenter(String name, String code, Id companyId, Id parentBaseId) {
		ComCostCenterBase__c base = new ComCostCenterBase__c(
			Name = name,
			Code__c = code,
			UniqKey__c = code,
			CompanyId__c = companyId
		);
		insert base;
		ComCostCenterHistory__c history = new ComCostCenterHistory__c(
			UniqKey__c = base.UniqKey__c + '1',
			BaseId__c = base.Id,
			Name = base.Name,
			Name_L0__c = base.Name + ' L0',
			Name_L1__c = base.Name + ' L1',
			Name_L2__c = base.Name + ' L2',
			LinkageCode__c = code + code,
			ValidFrom__c = AppDate.valueOf(Date.today() - 60).getDate(),
			ValidTo__c = System.today().addMonths(1),
			HistoryComment__c = 'Initial History'
		);
		if (parentBaseId != null) {
			history.ParentBaseId__c = parentBaseId;
		}
		insert history;
		return base;
	}


	private static CostCenterHistoryResource.CostCenterHistory createHistoryParam(Id baseId, String name, Id parentBaseId,
			String linkageCode, String validFrom, String validTo, String comment) {

		CostCenterHistoryResource.CostCenterHistory param = new CostCenterHistoryResource.CostCenterHistory();
		param.baseId = baseId;
		param.name_L0 = name + '_L0';
		param.name_L1 = name + '_L1';
		param.name_L2 = name + '_L2';
		if (parentBaseId != null) {
			param.parentId = parentBaseId;
		}
		param.linkageCode = linkageCode;
		param.validDateFrom = validFrom;
		param.validDateTo = validTo;
		param.comment = comment;
		return param;
	}

	/*
	 * Given the original (new data added) and retrieved data (newly created and retrieved), check that the retrieved data has the same as the orignal data.
	 */
	private static void verifyHistoryValue(CostCenterHistoryResource.CostCenterHistory param,
			Id expectedHistoryId, ComCostCenterBase__c retrievedBase,
			ComCostCenterHistory__c retrievedHistory, Boolean forSearchTest) {

		System.assertEquals(retrievedHistory.Id, expectedHistoryId);
		System.assertEquals(retrievedBase.Id, retrievedHistory.BaseId__c);
		if (!forSearchTest) {
			System.assertEquals(param.name_L0, retrievedHistory.Name);
			String uniqKey = retrievedBase.Code__c +
				'-' + DateTime.newInstance(AppDate.valueOf(param.validDateFrom).getDate(),
				Time.newInstance(0, 0, 0, 0)).format(
				'yyyyMMdd') +
				'-' + DateTime.newInstance(AppDate.valueOf(param.validDateTo).getDate(),
				Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd');
			System.assertEquals(uniqKey, retrievedHistory.UniqKey__c);
		}
		System.assertEquals(param.name_L0, retrievedHistory.Name_L0__c);
		System.assertEquals(param.name_L1, retrievedHistory.Name_L1__c);
		System.assertEquals(param.name_L2, retrievedHistory.Name_L2__c);
		System.assertEquals(param.parentId, retrievedHistory.ParentBaseId__c);
		System.assertEquals(param.linkageCode, retrievedHistory.LinkageCode__c);
		System.assertEquals(param.comment, retrievedHistory.HistoryComment__c);
		System.assertEquals(AppDate.valueOf(param.validDateFrom).getDate(), retrievedHistory.ValidFrom__c);
		System.assertEquals(AppDate.valueOf(param.validDateTo).getDate(), retrievedHistory.ValidTo__c);
	}

	/*
	 * Retrieve the CostCenterBase sObj and exclude the specified ID
	 */
	private static List<ComCostCenterBase__c> getBaseRecords(Id idToExclude) {
		return [SELECT
			Id,
			Name,
			Code__c,
			CompanyId__c,
			CurrentHistoryId__c,
			(SELECT
				Id,
				BaseId__c,
				UniqKey__c,
				Name, Name_L0__c, Name_L1__c, Name_L2__c,
				ParentBaseId__c,
				LinkageCode__c,
				HistoryComment__c,
				ValidFrom__c, ValidTo__c,
				Removed__c
				FROM Histories__r
			)
			FROM ComCostCenterBase__c
			WHERE Id != :idToExclude
		];
	}

	private static void verifyExpectedNumberOfHistoriesLeft(Integer expectedNumberOfRecordsToRemain) {
		List<ComCostCenterHistory__c> retrievedHistories = [
			SELECT Id
			FROM ComCostCenterHistory__c
			WHERE Removed__c = false
		];

		// Checks that there are expected number of records remains.
		System.assertEquals(expectedNumberOfRecordsToRemain, retrievedHistories.size());
	}
}