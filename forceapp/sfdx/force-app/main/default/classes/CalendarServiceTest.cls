/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description CalendarServiceのテストクラス
*/
@isTest
private class CalendarServiceTest {

	/**
	 * 会社デフォルトカレンダー作成処理のテスト
	 * カレンダーが正常に作成されることを確認する
	 */
	@isTest static void createCompanyDefaultCalenderTest() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		Test.startTest();

		CalendarService calService = new CalendarService();
		Id calendarId = calService.createCompanyDefaultCalendar(company.Id, company.Code__c, CalendarType.ATTENDANCE);

		Test.stopTest();

		ComCalendar__c calendar = [
			SELECT
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				UniqKey__c,
				Type__c,
				CompanyId__c,
				Remarks__c
			FROM ComCalendar__c
			WHERE Id = :calendarId];

		System.assertEquals('全社共通カレンダー', calendar.Name_L0__c);
		System.assertEquals('Company Calendar', calendar.Name_L1__c);
		System.assertEquals(null, calendar.Name_L2__c);
		System.assertEquals(calendar.Name_L0__c, calendar.Name);
		System.assertEquals(company.Code__c, calendar.Code__c);
		System.assertEquals(company.Code__c + '-' + calendar.Code__c, calendar.UniqKey__c);
		System.assertEquals(true, CalendarType.valueOf(calendar.Type__c).equals(CalendarType.ATTENDANCE));
		System.assertEquals(company.Id, calendar.CompanyId__c);
		System.assertEquals(null, calendar.Remarks__c);
	}

	/**
	 * カレンダー保存処理のテスト
	 * 既存のカレンダーが更新できることを確認する
	 */
	@isTest static void saveCalendarTestUpdate() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		CalendarRepository repo = new CalendarRepository();
		CalendarEntity calEntity = repo.getEntityById(calendar.Id);
		calEntity.code = 'update';

		Test.startTest();

		CalendarService calService = new CalendarService();
		calService.saveCalendar(calEntity);

		Test.stopTest();

		CalendarEntity resEntity = repo.getEntityById(calendar.Id);

		System.assertEquals(calEntity.nameL.valueL0, resEntity.nameL.valueL0);
		System.assertEquals(calEntity.nameL.valueL1, resEntity.nameL.valueL1);
		System.assertEquals(calEntity.nameL.valueL2, resEntity.nameL.valueL2);
		System.assertEquals(calEntity.name, resEntity.name);
		System.assertEquals(calEntity.code, resEntity.code);
		System.assertEquals(company.Code__c + '-' + calEntity.code, resEntity.uniqKey);
	}

	/**
	 * カレンダー保存処理のテスト
	 * コードが重複している場合はアプリ例外が発生することを確認する
	 */
	@isTest static void saveCalendarTestDuplicateCode() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar1 = ComTestDataUtility.createCalendar('test1', company.Id);
		ComCalendar__c calendar2 = ComTestDataUtility.createCalendar('test2', company.Id);

		CalendarRepository repo = new CalendarRepository();
		CalendarEntity calEntity = repo.getEntityById(calendar2.Id);
		calEntity.code = calendar1.Code__c;

		try {
			CalendarService calService = new CalendarService();
			calService.saveCalendar(calEntity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
		}
	}

	/**
	 * カレンダー保存処理のテスト
	 * 項目値に対するバリデーションが実行されていることを確認する
	 */
	@isTest static void saveCalendarTestValidate() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		CalendarRepository repo = new CalendarRepository();
		CalendarEntity calEntity = repo.getEntityById(calendar.Id);
		calEntity.nameL = new AppMultiString('', calEntity.nameL.valueL1, calEntity.nameL.valueL2);

		try {
			CalendarService calService = new CalendarService();
			calService.saveCalendar(calEntity);
			TestUtil.fail('例外が発生しませんでした');
		} catch (App.ParameterException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, e.getErrorCode());
		}
	}

	/**
	 * カレンダー明細保存処理のテスト
	 * 新規のカレンダー明細が保存できることを確認する
	 */
	@isTest static void saveCalendarRecordTestNew() {
		CalendarRepository repo = new CalendarRepository();
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		CalendarRecordEntity recordEntity = new CalendarRecordEntity();
		recordEntity.name = 'New Event';
		recordEntity.NameL = new AppMultiString('New Event L0', 'New Event L1', 'New Event L2');
		recordEntity.calendarId = calendar.Id;
		recordEntity.calDate = AppDate.newInstance(2018, 7, 31);
		recordEntity.dayType = AttDayType.HOLIDAY;

		Test.startTest();

		CalendarService calService = new CalendarService();
		Repository.SaveResult result = calService.saveCalendarRecord(recordEntity);

		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);

		CalendarRecordEntity resEntity = repo.getRecordEntityById(result.details[0].id);
		System.assertEquals(recordEntity.nameL.valueL0, resEntity.nameL.valueL0);
		System.assertEquals(recordEntity.nameL.valueL1, resEntity.nameL.valueL1);
		System.assertEquals(recordEntity.nameL.valueL2, resEntity.nameL.valueL2);
		System.assertEquals(recordEntity.name, resEntity.name);
		System.assertEquals(recordEntity.calDate, resEntity.calDate);
		System.assertEquals(recordEntity.calendarId, resEntity.calendarId);
		System.assertEquals(recordEntity.dayType, resEntity.dayType);
	}

	/**
	 * カレンダー明細保存処理のテスト
	 * 既存のカレンダー明細が更新できることを確認する
	 */
	@isTest static void saveCalendarRecordTestUpdate() {
		CalendarRepository repo = new CalendarRepository();
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);
		ComCalendarRecord__c record =
				ComTestDataUtility.createCalendarRecord(
						'event', calendar.Id, Date.newInstance(2018, 7, 31), AttDayType.HOLIDAY);

		CalendarRecordEntity recordEntity = new CalendarRecordEntity();
		recordEntity.setId(record.Id);
		recordEntity.name = 'Update Event';
		recordEntity.NameL = new AppMultiString('Update Event L0', 'Update Event L1', 'Update Event L2');
		recordEntity.calendarId = calendar.Id;
		recordEntity.calDate = AppDate.newInstance(2018, 7, 31);
		recordEntity.dayType = AttDayType.HOLIDAY;
		recordEntity.remarks = 'Test Remarks';
		Test.startTest();

		CalendarService calService = new CalendarService();
		Repository.SaveResult result = calService.saveCalendarRecord(recordEntity);

		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);
		System.assertNotEquals(recordEntity.id, result.details[0].id);

		// 既存のデータが論理削除されていることを確認
		Boolean includeRemoved = true;
		CalendarRecordEntity resDeleteEntity = repo.getRecordEntityById(recordEntity.id, includeRemoved);
		System.assertNotEquals(null, resDeleteEntity);
		System.assertEquals(true, resDeleteEntity.isRemoved);

		// 更新後のエンティティの各項目の値を確認
		CalendarRecordEntity resEntity = repo.getRecordEntityById(result.details[0].id);
		System.assertNotEquals(null, resEntity);
		System.assertEquals(recordEntity.nameL.valueL0, resEntity.nameL.valueL0);
		System.assertEquals(recordEntity.nameL.valueL1, resEntity.nameL.valueL1);
		System.assertEquals(recordEntity.nameL.valueL2, resEntity.nameL.valueL2);
		System.assertEquals(recordEntity.name, resEntity.name);
		System.assertEquals(recordEntity.calDate, resEntity.calDate);
		System.assertEquals(recordEntity.calendarId, resEntity.calendarId);
		System.assertEquals(recordEntity.dayType, resEntity.dayType);
		System.assertEquals(recordEntity.remarks, resEntity.remarks);
	}

	/**
	 * カレンダー明細削除のテスト
	 * カレンダー明細が論理削除できることを確認する
	 */
	@isTest static void logicalDeleteRecordTest() {
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('test', company.Id);

		Date targetDate = Date.valueOf('2018-04-01');
		AttDayType dayType = AttDayType.WORKDAY;
		ComCalendarRecord__c rec1 =
				ComTestDataUtility.createCalendarRecord('rec1', calendar.Id, targetDate, dayType);

		targetDate = Date.valueOf('2018-04-02');
		dayType = AttDayType.HOLIDAY;
		ComCalendarRecord__c rec2 =
				ComTestDataUtility.createCalendarRecord('rec2', calendar.Id, targetDate, dayType);

		Test.startTest();

		// 削除実行
		List<Id> deleteIds = new List<String>{rec1.Id, rec2.Id};
		new CalendarService().logicalDeleteCalendarRecords(deleteIds);

		Test.stopTest();

		// 論理削除されていることを確認
		List<ComCalendarRecord__c> recordList = [
			SELECT
				Id,
				Name,
				Removed__c
			FROM ComCalendarRecord__c
			WHERE Id IN :deleteIds];

		System.assertEquals(deleteIds.size(), recordList.size());
		for (ComCalendarRecord__c record : recordList) {
			System.assertEquals(true, record.Removed__c);
		}
	}

	/**
	 * カレンダー削除のテスト
	 * 指定したカレンダーが論理削除できることを確認する
	 */
	@isTest static void logicalDeleteCalendarTest() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 削除対象のカレンダーを作成
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('DeleteTest', company.Id);

		Test.startTest();

		// 削除実行
		Id deleteCalendarId = calendar.Id;
		new CalendarService().logicalDeleteCalendar(deleteCalendarId);

		Test.stopTest();

		List<ComCalendar__c> resCalenderList = [
			SELECT
				Id,
				Removed__c
			FROM ComCalendar__c
			WHERE Id = :deleteCalendarId];

		System.assertEquals(1, resCalenderList.size());
		System.assertEquals(true, resCalenderList[0].Removed__c);
	}

	/**
	 * カレンダー削除のテスト
	 * 全社共通カレンダーは削除できないことを確認する
	 */
	@isTest static void logicalDeleteCalendarTestCompanyCalendar() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCompany__c company = ComTestDataUtility.createCompany('test', null);

		// 会社のデフォルトカレンダーを作成
		// 作成したカレンダーを会社に紐づけ
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('DeleteTest', company.Id);
		company.CalendarId__c = calendar.Id;
		update company;

		// 削除実行
		App.ParameterException resEx;
		try {
			new CalendarService().logicalDeleteCalendar(calendar.Id);
		} catch (App.ParameterException e) {
			resEx = e;
		}

		TestUtil.assertNotNull(resEx);
		String expMsg = ComMessage.msg().Com_Err_CannotDelete(new List<String>{ComMessage.msg().Admin_Lbl_CompanyCalendar});
		System.assertEquals(expMsg, resEx.getMessage());
	}
}