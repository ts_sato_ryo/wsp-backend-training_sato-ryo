/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description ExpOCRRepository
 *
 * @group 経費 Expense
 */
public class ExpOCRRepository extends Repository.BaseRepository {

	public static final String IN_PROGRESS = 'InProgress';
	public static final String COMPLETED = 'Completed';

	private static final Set<Schema.SObjectField> SELECT_FIELD_SET;
	static {
		// Target fields definition
		SELECT_FIELD_SET = new Set<Schema.SObjectField> {
			ExpOCRData__c.Id,
			ExpOCRData__c.Name,
			ExpOCRData__c.CreatedDate,
			ExpOCRData__c.CompanyId__c,
			ExpOCRData__c.EmployeeHistoryId__c,
			ExpOCRData__c.EndDateTime__c,
			ExpOCRData__c.OCRAllText__c,
			ExpOCRData__c.OCRAmount__c,
			ExpOCRData__c.OCRDate__c,
			ExpOCRData__c.OCRProcessStatus__c,
			ExpOCRData__c.OCRResponse__c,
			ExpOCRData__c.ReceiptId__c,
			ExpOCRData__c.StartDateTime__c,
			ExpOCRData__c.TaskId__c
			};
	}

	/**
	 * @description save entity with status
	 * @param receiptId contentVersionId
	 * @param taskId
	 * @param companyId
	 * @param status
	 * @return auth header
	 */
	public Repository.SaveResult saveEntityWithStatus(Id receiptId, String taskId, Id companyId, String status) {
		ExpOCREntity entity = new ExpOCREntity();
		entity.ocrProcessStatus = status;
		entity.taskId = taskId;
		entity.companyId = companyId;
		entity.receiptId = receiptId;
		entity.startDateTime = AppDatetime.now();
		entity.employeeHistoryId = ExpCommonUtil.getEmployeeBaseEntity(null).currentHistoryId;
		return saveEntity(entity);
	}

	/**
	 * @description save entity with status
	 * @param receiptId contentVersionId
	 * @param entity
	 * @param status
	 * @param ocrAllText
	 * @param ocrAmount
	 * @param ocrDate
	 * @param ocrResponse
	 */
	public void saveEntityWithOCRResults(ExpOCREntity entity, String status, String ocrAllText, Decimal ocrAmount, AppDate ocrDate, String ocrResponse) {
		entity.endDateTime = AppDatetime.now();
		entity.ocrProcessStatus = status;
		entity.ocrAllText = ocrAllText;
		entity.ocrAmount = ocrAmount;
		entity.ocrDate = ocrDate;
		entity.ocrResponse = ocrResponse;
		saveEntity(entity);
	}

	/**
	 * @description save entity
	 * @param entity
	 */
	public Repository.SaveResult saveEntity(ExpOCREntity entity) {
		return saveEntityList(new List<ExpOCREntity>{ entity });
	}

	/**
	 * @description save entity list
	 * @param entityList
	 */
	public Repository.SaveResult saveEntityList(List<ExpOCREntity> entityList) {
		// Convert entity to sObject
		List<ExpOCRData__c> sObjList = createObjectList(entityList);
		// Save to DB
		List<Database.UpsertResult> resultList = AppDatabase.doUpsert(sObjList);
		// Make and return result
		return resultFactory.createSaveResult(resultList);
	}

	public class SearchFilter {
		public Set<Id> idSet;
		public Set<Id> receiptIdSet;
		public String ocrProcessStatus;
		public String taskId;
	}

	/**
	 * @description save entity list.
	 * @param contentVersionId
	 * @return search result: if the search result is empty or null, null will be returned
	 */
	public ExpOCREntity searchEntityByReceiptId(Id contentVersionId){
		ExpOCRRepository.SearchFilter filter = new ExpOCRRepository.SearchFilter();
		filter.receiptIdSet = new Set<Id>{contentVersionId};
		List<ExpOCREntity> entityList = searchEntityList(filter, false, 1);
		return entityList.isEmpty() ? null : entityList[0];
	}

	/**
	 * @description save entity list.
	 * @param contentVersionId
	 * @return search result: empty can be returned (not null)
	 */
	public ExpOCREntity searchEntityByTaskId(String taskId){
		ExpOCRRepository.SearchFilter filter = new ExpOCRRepository.SearchFilter();
		filter.taskId = taskId;
		List<ExpOCREntity> entityList = searchEntityList(filter, true, 1);
		return entityList.isEmpty() ? null : entityList[0];
	}

	private List<ExpOCREntity> searchEntityList(ExpOCRRepository.SearchFilter filter, Boolean forUpdate, Integer resultLimit) {

		// WHERE statement
		List<String> whereList = new List<String>();
		Set<Id> pIds;
		if (filter.idSet != null) {
			pIds = filter.idSet;
			whereList.add('Id IN :pIds');
		}
		Set<Id> receiptIdSet;
		if (filter.receiptIdSet != null) {
			receiptIdSet = filter.receiptIdSet;
			whereList.add(getFieldName(ExpOCRData__c.ReceiptId__c) + ' IN :receiptIdSet');
		}
		Id ocrProcessStatus;
		if (filter.ocrProcessStatus != null) {
			ocrProcessStatus = filter.ocrProcessStatus;
			whereList.add(getFieldName(ExpOCRData__c.OCRProcessStatus__c) + ' = :ocrProcessStatus');
		}
		String taskId;
		if (filter.taskId != null) {
			taskId = filter.taskId;
			whereList.add(getFieldName(ExpOCRData__c.TaskId__c) + ' = :taskId');
		}

		String whereString = buildWhereString(whereList);

		// SELECT Target fields
		List<String> selectFieldList = new List<String>();
		// Regular fields
		for (Schema.SObjectField fld : SELECT_FIELD_SET) {
			selectFieldList.add(getFieldName(fld));
		}

		// Build SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',')
			+ ' FROM ' + ExpOCRData__c.SObjectType.getDescribe().getName()
			+ whereString
			+ ' LIMIT :resultLimit';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		System.debug('ExpOCRRepository: SOQL=' + soql);

		// Execute query
		List<ExpOCRData__c> sObjs = (List<ExpOCRData__c>)Database.query(soql);

		// Create entity from SObject
		List<ExpOCREntity> entities = new List<ExpOCREntity>();
		for (ExpOCRData__c sObj : sObjs) {
			entities.add(createEntity(sObj));
		}

		return entities;
	}

	private List<SObject> createObjectList(List<ExpOCREntity> entityList) {

		List<ExpOCRData__c> objectList = new List<ExpOCRData__c>();
		for (ExpOCREntity entity : entityList) {
			objectList.add(createObject(entity));
		}

		return objectList;
	}

	private ExpOCRData__c createObject(ExpOCREntity entity) {

		ExpOCRData__c sObj = new ExpOCRData__c();

		sObj.Id = entity.id;

		if (entity.isChanged(ExpOCREntity.Field.COMPANY_ID)) {
			sObj.CompanyId__c = entity.companyId;
		}
		if (entity.isChanged(ExpOCREntity.Field.EMPLOYEE_HISTORY_ID)) {
			sObj.EmployeeHistoryId__c = entity.employeeHistoryId;
		}
		if (entity.isChanged(ExpOCREntity.Field.END_DATE_TIME)) {
			sObj.EndDateTime__c = AppDatetime.convertDatetime(entity.endDateTime);
		}
		if (entity.isChanged(ExpOCREntity.Field.OCR_ALL_TEXT)) {
			sObj.OCRAllText__c = entity.ocrAllText;
		}
		if (entity.isChanged(ExpOCREntity.Field.OCR_AMOUNT)) {
			sObj.OCRAmount__c = entity.ocrAmount;
		}
		if (entity.isChanged(ExpOCREntity.Field.OCR_DATE)) {
			sObj.OCRDate__c = AppDate.convertDate(entity.ocrDate);
		}
		if (entity.isChanged(ExpOCREntity.Field.OCR_PROCESS_STATUS)) {
			sObj.OCRProcessStatus__c = entity.ocrProcessStatus;
		}
		if (entity.isChanged(ExpOCREntity.Field.OCR_RESPONSE)) {
			sObj.OCRResponse__c = entity.ocrResponse;
		}
		if (entity.isChanged(ExpOCREntity.Field.RECEIPT_ID)) {
			sObj.ReceiptId__c = entity.receiptId;
		}
		if (entity.isChanged(ExpOCREntity.Field.START_DATE_TIME)) {
			sObj.StartDateTime__c = AppDatetime.convertDatetime(entity.startDateTime);
		}
		if (entity.isChanged(ExpOCREntity.Field.TASK_ID)) {
			sObj.TaskId__c = entity.taskId;
		}

		return sObj;
	}

	private ExpOCREntity createEntity(ExpOCRData__c sObj) {

		ExpOCREntity entity = new ExpOCREntity();

		entity.setId(sObj.Id);

		entity.startDateTime = AppDatetime.valueOf(sObj.StartDateTime__c);
		entity.receiptId = sObj.ReceiptId__c;
		entity.ocrResponse = sObj.OCRResponse__c;
		entity.ocrProcessStatus = sObj.OCRProcessStatus__c;
		entity.ocrDate = AppDate.valueOf(sObj.OCRDate__c);
		entity.ocrAmount = sObj.OCRAmount__c;
		entity.ocrAllText = sObj.OCRAllText__c;
		entity.endDateTime = AppDatetime.valueOf(sObj.EndDateTime__c);
		entity.employeeHistoryId = sObj.EmployeeHistoryId__c;
		entity.companyId = sObj.CompanyId__c;
		entity.taskId = sObj.TaskId__c;

		entity.resetChanged();

		return entity;
	}

}