/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 工数設定履歴切り替えバッチのテストクラス
 */
@isTest
private class TimeSettingCurrentHistoryUpdateBatchTest {

	/**
	 * Apexスケジュール正常系テスト
	 */
	@isTest
	static void schedulePositiveTest() {
		Test.StartTest();
		TimeSettingCurrentHistoryUpdateBatch sch = new TimeSettingCurrentHistoryUpdateBatch();
		Id schId = System.schedule('工数設定履歴切り替えスケジュール', '0 0 0 * * ?', sch);
		Test.StopTest();
		System.assertNotEquals(null, schId);
	}

	/**
	 * バッチ正常系テスト
	 */
	@isTest
	static void batchPositiveTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		final Integer baseNum = 2;
		List<TimeSettingBase__c> baseObjs = ComTestDataUtility.createTimeSettingsWithHistory('Test', companyObj.Id, baseNum);
		TimeSettingBase__c base1 = baseObjs[0];
		List<TimeSettingHistory__c> historyList1 =
				[SELECT Id, BaseId__c, Name_L0__c FROM TimeSettingHistory__c WHERE BaseId__c = :base1.Id ORDER BY ValidFrom__c Asc];

		// CurrentHistoryId__c を空欄にしておく
		base1.Name = 'AAA';
		base1.CurrentHistoryId__c = null;
		update base1;

		Test.StartTest();
		TimeSettingCurrentHistoryUpdateBatch b = new TimeSettingCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);
		Test.StopTest();
		System.assertNotEquals(null, jobId);

		TimeSettingBase__c resBase = [SELECT Id, Name, CurrentHistoryId__c FROM TimeSettingBase__c WHERE Id = :base1.Id];
		System.assertEquals(historyList1[0].Id, resBase.CurrentHistoryId__c);
		System.assertEquals(historyList1[0].Name_L0__c, resBase.Name);
	}
}