/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * コンテンツバージョンのリポジトリクラスのテスト
 */
@isTest
private class AppContentVersionRepositoryTest {

	/**
	 * テスト用のContentVersionを作成する
	 */
	private static ContentVersion createContentVersion(String title, String pathOnClient, String versionData) {
		ContentVersion content = new ContentVersion(
			Title = title,
			PathOnClient = pathOnClient,
			VersionData = Blob.valueOf(versionData),
			SharingOption = AppContentVersionRepository.SHARING_OPTION_FREEZE);

		return content;
	}

	/**
	 * saveEntityのテスト
	 * 新規の保存ができることを確認する
	 */
	@isTest static void saveEntityTestNew() {

		// ファイルボディ File body
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);

		AppContentVersionEntity content = new AppContentVersionEntity();
		content.title = 'Test';
		content.pathOnClient = 'Test.jpg';
		content.versionDataText = bodyString;

		Test.startTest();
			AppContentVersionRepository repo = new AppContentVersionRepository();
			Repository.SaveResult result = repo.saveEntity(content);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);

		// 保存できていることを確認
		Id resId = result.details[0].Id;
		List<ContentVersion> resObjs = [
				SELECT Id, Title, FileType, PathOnClient, VersionData, SharingOption
				FROM ContentVersion
				WHERE Id = :resId];

		System.assertEquals(1, resObjs.size());
		System.assertEquals(content.title, resObjs[0].Title);
		System.assertEquals('JPG', resObjs[0].FileType);
		System.assertEquals(content.pathOnClient, resObjs[0].PathOnClient);
		System.assertEquals(content.versionDataText, EncodingUtil.base64Encode(resObjs[0].VersionData));
		System.assertEquals(AppContentVersionRepository.SHARING_OPTION_FREEZE, resObjs[0].SharingOption);
	}

	/**
	 * saveEntityのテスト
	 * 既存のエンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestUpdate() {

		// ファイルボディ File body
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);

		String title = 'Test';
		String pathOnClient = 'Test.jpg';
		String versionData = bodyString;

		ContentVersion contentNew = createContentVersion(title, pathOnClient, versionData);
		insert contentNew;

		// 更新する
		AppContentVersionEntity content = new AppContentVersionEntity();
		content.setId(contentNew.Id);
		content.title = title + '_Update';

		Test.startTest();
			AppContentVersionRepository repo = new AppContentVersionRepository();
			Repository.SaveResult result = repo.saveEntity(content);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);

		// 保存できていることを確認
		Id resId = result.details[0].Id;
		List<ContentVersion> resObjs = [
				SELECT Id, Title, PathOnClient, VersionData
				FROM ContentVersion
				WHERE Id = :resId];

		System.assertEquals(1, resObjs.size());
		System.assertEquals(content.title, resObjs[0].Title);
	}
}
