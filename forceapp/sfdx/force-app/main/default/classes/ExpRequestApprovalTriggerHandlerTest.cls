/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group Expense
*
* @description Test class for ExpRequestApprovalTriggerHandlerTest
*/
@isTest
private class ExpRequestApprovalTriggerHandlerTest {

	private static ExpTestData testData = new ExpTestData();

	/**
	 * @description 申請レコード更新処理（承認済み）のテスト Test for updating r
	 */
	@isTest
	static void updateRequestApproveTest() {

		ExpRequestEntity entity = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// Submit Request
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(entity.id, null);

		// Get request ID
		Id requestId = [SELECT Id FROM ExpRequestApproval__c WHERE ExpRequestId__c = :entity.id LIMIT 1][0].Id;

		Test.startTest();

		ExpRequestApprovalEntity approvalEntity = new ExpRequestApprovalEntity();
		approvalEntity.setId(requestId);
		approvalEntity.status = AppRequestStatus.APPROVED;

		ExpRequestApprovalRepository repo = new ExpRequestApprovalRepository();
		repo.saveEntity(approvalEntity);

		Test.stopTest();

		// Get submitted date
		ExpRequestApproval__c approval = [
				SELECT
					Id,
					ConfirmationRequired__c,
					CancelType__c
				FROM ExpRequestApproval__c
				WHERE Id = :approvalEntity.id];

		System.assertEquals(false, approval.ConfirmationRequired__c);
		System.assertEquals(null, approval.CancelType__c);
	}

	/**
	 * @description 申請レコード更新処理（却下）のテスト
	 */
	@isTest
	static void updateRequestRejectTest() {
		ExpRequestEntity report = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job,1)[0];

		// Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// Submit request
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(report.id, null);

		// Get record ID
		Id requestId = [SELECT Id FROM ExpRequestApproval__c WHERE ExpRequestId__c = :report.id LIMIT 1][0].Id;

		Test.startTest();

		ExpRequestApprovalEntity approvalEntity = new ExpRequestApprovalEntity();
		approvalEntity.setId(requestId);
		approvalEntity.status = AppRequestStatus.DISABLED;
		approvalEntity.cancelType = AppCancelType.REJECTED;

		ExpRequestApprovalRepository repo = new ExpRequestApprovalRepository();
		repo.saveEntity(approvalEntity);

		Test.stopTest();

		// Get record data
		ExpRequestApproval__c request = [
				SELECT
					Id,
					ConfirmationRequired__c,
					CancelType__c
				FROM ExpRequestApproval__c
				WHERE Id = :approvalEntity.id];

		System.assertEquals(true, request.ConfirmationRequired__c);
	}
}