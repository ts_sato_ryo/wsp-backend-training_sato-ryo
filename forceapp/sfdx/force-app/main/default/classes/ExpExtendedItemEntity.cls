/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Extended Item Entity representing the Extended Items used in
 * ExpReport, ExpRequest, ExpRecordItem, and ExpRequestRecordItem.
 *
 * @group Expense
 */
public abstract with sharing class ExpExtendedItemEntity extends Entity {
	/** Number of Extended Items for each type */
	public static final Integer EXTENDED_ITEM_MAX_COUNT = 10;

	/**
	 * Pattern to matches any of the following: ExtendedItem{XX}{YY}{ZZ}
	 * Where
	 *  XX: `Text`, `Picklist`, `Lookup` or `Date`
	 *  YY: 01 ~ 10
	 *  ZZ: `Id` or `Value`
	 * */
	private static final Pattern EXTENDED_ITEM_PATTERN = Pattern.compile('ExtendedItem((Text)|(Picklist)|(Lookup)|(Date)){1}[0-9]{2}((Id)|(Value)){1}');

	/** String templates to access the Extended Item fields - based on the Field enum names */
	private static final String EXTENDED_ITEM_ID_TEMPLATE = 'ExtendedItem{0}{1}Id';
	private static final String EXTENDED_ITEM_VALUE_TEMPLATE = 'ExtendedItem{0}{1}Value';

	/** Extended Item Value Template */
	public static final String EXTENDED_ITEM_TEXT_VALUE_TEMPLATE = 'ExtendedItemText{0}Value';
	public static final String EXTENDED_ITEM_PICKLIST_VALUE_TEMPLATE = 'ExtendedItemPicklist{0}Value';
	public static final String EXTENDED_ITEM_LOOKUP_VALUE_TEMPLATE = 'ExtendedItemLookup{0}Value';
	public static final String EXTENDED_ITEM_DATE_VALUE_TEMPLATE = 'ExtendedItemDate{0}Value';

	/** Change Fields sets */
	public enum ExtendedItemField {
		ExtendedItemLookupIdList,
		ExtendedItemLookupValueList,
		ExtendedItemPicklistIdList,		//This is not used for tracking change, and only used for Modification History
		ExtendedItemPicklistValueList,
		ExtendedItemTextIdList,			//This is not used for tracking change, and only used for Modification History
		ExtendedItemTextValueList,
		ExtendedItemDateIdList,			//This is not used for tracking change, and only used for Modification History
		ExtendedItemDateValueList
	}
	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, ExtendedItemField> EXTENDED_ITEM_FIELD_MAP;
	static {
		final Map<String, ExtendedItemField> fieldMap = new Map<String, ExtendedItemField>();
		for (ExtendedItemField f : ExtendedItemField.values()) {
			fieldMap.put(f.name(), f);
		}
		EXTENDED_ITEM_FIELD_MAP = fieldMap;
	}

	/** Store the corresponding Accessor(Index and Extended Item Type) for the given string */
	private static final Map<String, ExtendedItemAccessor> FIELD_NAME_TO_ACCESSOR_MAP;
	static {
		FIELD_NAME_TO_ACCESSOR_MAP = new Map<String, ExtendedItemAccessor>();
		List<String> extendedItemTypeList = new List<String> {'Text', 'Picklist', 'Lookup', 'Date'};
		for (String extendedItemType : extendedItemTypeList) {
			for (Integer index = 0; index < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; index++) {
				String indexStr = String.valueOf(index + 1).leftPad(2, '0');
				FIELD_NAME_TO_ACCESSOR_MAP.put(
						String.format(EXTENDED_ITEM_ID_TEMPLATE, new List<String>{extendedItemType, indexStr}),
						new ExtendedItemAccessor(EXTENDED_ITEM_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'IdList'), index)
				);

				FIELD_NAME_TO_ACCESSOR_MAP.put(
						String.format(EXTENDED_ITEM_VALUE_TEMPLATE, new List<String>{extendedItemType, indexStr}),
						new ExtendedItemAccessor(EXTENDED_ITEM_FIELD_MAP.get('ExtendedItem' + extendedItemType + 'ValueList'), index)
				);
			}
		}
	}

	/*
	 * Wrapper class for holding the Index and the field {@code ExtendedItemField}
	 */
	private class ExtendedItemAccessor {
		ExtendedItemField field {private set; get;}
		Integer index {private set; get;}

		public ExtendedItemAccessor(ExtendedItemField field, Integer index) {
			this.field = field;
			this.index = index;
		}
	}

	/** Set of Extended Item Fiels which has been changed */
	private Set<ExtendedItemField> isChangedExtendedItemFieldSet;

	/**
	 * All sub class must call this parent constructor.
	 * */
	protected ExpExtendedItemEntity() {
		isChangedExtendedItemFieldSet = new Set<ExtendedItemField>();

		this.extendedItemDateIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistIdList = new Id[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextIdList = new Id[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemDateValueList = new String[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupValueList = new String[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistValueList = new String[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextValueList = new String[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemDateRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextRequiredForList = new ComExtendedItemRequiredFor[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemDateInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemLookupInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemPicklistInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];
		this.extendedItemTextInfoList = new ExtendedItemService.ExtendedItemInfo[EXTENDED_ITEM_MAX_COUNT];

		this.extendedItemCustomOptionNameList = new AppMultiString[EXTENDED_ITEM_MAX_COUNT];
	}

	/** Date type Extended Item ID (Reference Only) */
	private List<Id> extendedItemDateIdList;
	public void setExtendedItemDateId(Integer index, Id idValue) {
		this.extendedItemDateIdList[index] = idValue;
	}
	public Id getExtendedItemDateId(Integer index) {
		return this.extendedItemDateIdList[index];
	}

	/** Date type Extended Item Value */
	private List<String> extendedItemDateValueList;
	public void setExtendedItemDateValue(Integer index, String value) {
		this.extendedItemDateValueList[index] = value;
		setChanged(ExtendedItemField.ExtendedItemDateValueList);
	}
	public void setExtendedItemDateValue(Integer index, Date value) {
		AppDate convertedDate = AppDate.valueOf(value);
		this.extendedItemDateValueList[index] = convertedDate == null ? null : convertedDate.toString();
		setChanged(ExtendedItemField.ExtendedItemDateValueList);
	}
	public String getExtendedItemDateValue(Integer index) {
		return this.extendedItemDateValueList[index];
	}
	public Date getExtendedItemDateValueAsDate(Integer index) {
		AppDate convertedDate = AppDate.valueOf(this.extendedItemDateValueList[index]);
		return convertedDate == null ? null : convertedDate.getDate();
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemDateInfoList;
	public void setExtendedItemDateInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemDateInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemDateInfo(Integer index) {
		return this.extendedItemDateInfoList[index];
	}

	/** Extended Item Date Required For (Only Reference) */
	private List<ComExtendedItemRequiredFor> extendedItemDateRequiredForList;
	public void setExtendedItemDateRequiredFor(Integer index, ComExtendedItemRequiredFor requiredFor) {
		this.extendedItemDateRequiredForList[index] = requiredFor;
	}
	public ComExtendedItemRequiredFor getExtendedItemDateRequiredFor(Integer index) {
		return this.extendedItemDateRequiredForList[index];
	}


	/** Lookup Extended Item ID */
	private List<Id> extendedItemLookupIdList;
	public void setExtendedItemLookupId(Integer index, Id idValue) {
		this.extendedItemLookupIdList[index] = idValue;
		setChanged(ExtendedItemField.ExtendedItemLookupIdList);
	}
	public Id getExtendedItemLookupId(Integer index) {
		return this.extendedItemLookupIdList[index];
	}

	/** Lookup Extended Item Value */
	private List<String> extendedItemLookupValueList;
	public void setExtendedItemLookupValue(Integer index, String value) {
		this.extendedItemLookupValueList[index] = value;
		setChanged(ExtendedItemField.ExtendedItemLookupValueList);
	}
	public String getExtendedItemLookupValue(Integer index) {
		return this.extendedItemLookupValueList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemLookupInfoList;
	public void setExtendedItemLookupInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemLookupInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemLookupInfo(Integer index) {
		return this.extendedItemLookupInfoList[index];
	}

	/** Extended Item Lookup Required For (Only Reference) */
	private List<ComExtendedItemRequiredFor> extendedItemLookupRequiredForList;
	public void setExtendedItemLookupRequiredFor(Integer index, ComExtendedItemRequiredFor requiredFor) {
		this.extendedItemLookupRequiredForList[index] = requiredFor;
	}
	public ComExtendedItemRequiredFor getExtendedItemLookupRequiredFor(Integer index) {
		return this.extendedItemLookupRequiredForList[index];
	}

	/** Name representation of selected ExtendedItemCustomOptionName (ONLY REFERENCE) */
	private List<AppMultiString> extendedItemCustomOptionNameList;
	public void setExtendedItemCustomOptionName(Integer index, AppMultiString value) {
		this.extendedItemCustomOptionNameList[index] = value;
	}
	public AppMultiString getExtendedItemCustomOptionNameL(Integer index) {
		return this.extendedItemCustomOptionNameList[index];
	}
	public String getExtendedItemCustomOptionName(Integer index) {
		return AppMultiString.stringValue(this.extendedItemCustomOptionNameList[index]);
	}

	/** Picklist type Extended Item ID (Reference Only) */
	private List<Id> extendedItemPicklistIdList;
	public void setExtendedItemPicklistId(Integer index, Id idValue) {
		this.extendedItemPicklistIdList[index] = idValue;
	}
	public Id getExtendedItemPicklistId(Integer index) {
		return this.extendedItemPicklistIdList[index];
	}

	/** Picklist type Extended Item Value */
	private List<String> extendedItemPicklistValueList;
	public void setExtendedItemPicklistValue(Integer index, String value) {
		this.extendedItemPicklistValueList[index] = value;
		setChanged(ExtendedItemField.ExtendedItemPicklistValueList);
	}
	public String getExtendedItemPicklistValue(Integer index) {
		return this.extendedItemPicklistValueList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemPicklistInfoList;
	public void setExtendedItemPicklistInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemPicklistInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemPicklistInfo(Integer index) {
		return this.extendedItemPicklistInfoList[index];
	}

	/** Extended Item Picklist Required For (Only Reference) */
	private List<ComExtendedItemRequiredFor> extendedItemPicklistRequiredForList;
	public void setExtendedItemPicklistRequiredFor(Integer index, ComExtendedItemRequiredFor requiredFor) {
		this.extendedItemPicklistRequiredForList[index] = requiredFor;
	}
	public ComExtendedItemRequiredFor getExtendedItemPicklistRequiredFor(Integer index) {
		return this.extendedItemPicklistRequiredForList[index];
	}


	/** Text type Extended Item ID (Reference Only) */
	private List<Id> extendedItemTextIdList;
	public void setExtendedItemTextId(Integer index, Id idValue) {
		this.extendedItemTextIdList[index] = idValue;
	}
	public Id getExtendedItemTextId(Integer index) {
		return this.extendedItemTextIdList[index];
	}

	/** Text type Extended Item Value */
	private List<String> extendedItemTextValueList;
	public void setExtendedItemTextValue(Integer index, String value) {
		this.extendedItemTextValueList[index] = value;
		setChanged(ExtendedItemField.ExtendedItemTextValueList);
	}
	public String getExtendedItemTextValue(Integer index) {
		return this.extendedItemTextValueList[index];
	}

	/** Extended Item information (for reference) */
	private List<ExtendedItemService.ExtendedItemInfo> extendedItemTextInfoList;
	public void setExtendedItemTextInfo(Integer index, ExtendedItemService.ExtendedItemInfo info) {
		this.extendedItemTextInfoList[index] = info;
	}
	public ExtendedItemService.ExtendedItemInfo getExtendedItemTextInfo(Integer index) {
		return this.extendedItemTextInfoList[index];
	}

	/** Extended Item Text Required For (Only Reference) */
	private List<ComExtendedItemRequiredFor> extendedItemTextRequiredForList;
	public void setExtendedItemTextRequiredFor(Integer index, ComExtendedItemRequiredFor requiredFor) {
		this.extendedItemTextRequiredForList[index] = requiredFor;
	}
	public ComExtendedItemRequiredFor getExtendedItemTextRequiredFor(Integer index) {
		return this.extendedItemTextRequiredForList[index];
	}

	/** Mark the specified field as changed */
	private void setChanged(ExtendedItemField field) {
		this.isChangedExtendedItemFieldSet.add(field);
	}

	/*
	 * Returns true is the specified field value has been changed.
	 * @param field Field to check if it has been changed
	 *
	 * @return true if the field has been changed. Otherwise, false.
	 */
	public Boolean isChanged(ExtendedItemField field) {
		return isChangedExtendedItemFieldSet.contains(field);
	}

	/*
	 * Clear the {@code isChangedExtendedItemFieldSet} value, resetting the tracking of changed fields.
	 * Subclass must call this.
	 */
	public virtual void resetChanged() {
		this.isChangedExtendedItemFieldSet.clear();
	}

	/*
	 * To allows child classes to determine if a field is a extended Item field
	 * @param fieldName the representative field name in String (e.g. extendedItemText01Id)
	 *
	 * @return true if the specified string is a extended item field. Otherwise, false.
	 */
	protected Boolean isExtendedItemField(String fieldName) {
		return EXTENDED_ITEM_PATTERN.matcher(fieldName).matches();
	}

	/*
	 * Returns extended item value using the field name in string format
	 * @param fieldName the representative field name in String (e.g. extendedItemText01Id)
	 *
	 * @return the value set to the field. If the field name is not a valid Extended Item field, {@code null} will be returned.
	 */
	protected String getExtendedItemValue(String fieldName) {
		ExtendedItemAccessor accessor = FIELD_NAME_TO_ACCESSOR_MAP.get(fieldName);
		if (accessor != null) {
			return getExtendedItemValue(accessor.field, accessor.index);
		}
		return null;
	}

	/*
	 * Returns Extended Item value for the given {@code ExtendedItemField} and and index.
	 *
	 * @param field Extended Item Field (Text Id, Text Value, Picklist Id, Picklist Value, etc)
	 * @param index the extended item index (starts from 0)
	 *
	 * @return the value set to the field. If the field name is not a valid Extended Item field, {@code null} will be returned.
	 */
	private String getExtendedItemValue(ExtendedItemField field, Integer index) {
		if (field == ExtendedItemField.ExtendedItemDateIdList) {
			return this.extendedItemDateIdList[index];
		} else if (field == ExtendedItemField.ExtendedItemDateValueList) {
			return this.extendedItemDateValueList[index];
		} else if (field == ExtendedItemField.ExtendedItemLookupIdList) {
			return this.extendedItemLookupIdList[index];
		} else if (field == ExtendedItemField.ExtendedItemLookupValueList) {
			return this.extendedItemLookupValueList[index];
		} else if (field == ExtendedItemField.ExtendedItemPicklistIdList) {
			return this.extendedItemPicklistIdList[index];
		} else if (field == ExtendedItemField.ExtendedItemPicklistValueList) {
			return this.extendedItemPicklistValueList[index];
		} else if (field == ExtendedItemField.ExtendedItemTextIdList) {
			return this.extendedItemTextIdList[index];
		} else if (field == ExtendedItemField.ExtendedItemTextValueList) {
			return this.extendedItemTextValueList[index];
		}
		return null;
	}
}