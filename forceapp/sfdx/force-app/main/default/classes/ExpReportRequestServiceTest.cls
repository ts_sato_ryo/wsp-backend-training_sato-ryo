/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description ExpReportRequestServiceのテストクラス ExpReportRequestService Test Class
 */
@isTest
private class ExpReportRequestServiceTest {

	/**
	 * 経費承認申請可否チェック処理のテスト Test for checkExpReportRequest method
	 */
	@isTest static void checkExpReportRequestTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType, testData.employee, 3);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});

		Test.startTest();

		ExpReportRequestService service = new ExpReportRequestService();
		List<String> confirmationList = service.checkExpReportRequest(report.id);

		Test.stopTest();

		System.assertEquals(0, confirmationList.size());
	}

	/**
	 * 経費承認申請可否チェック処理のテスト Test for checkExpReportRequest method
	 * 不適切なデータが含まれている場合エラーが発生することを確認する Confirm that an exception is thrown if invalid data is included
	 */
	@isTest static void checkExpReportRequestTestError() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType, testData.employee, 3);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});

		recordList[0].recordType = ExpRecordType.TRANSIT_JORUDAN_JP;
		new ExpRecordRepository().saveEntityList(recordList);

		try {
			ExpReportRequestService service = new ExpReportRequestService();
			service.checkExpReportRequest(report.id);
			System.assert(false, 'Exception was not thrown.');
		} catch (App.IllegalStateException e) {
			// OK
			System.assertEquals(ComMessage.msg().Exp_Err_NoEvidence, e.getMessage());
		} catch (Exception e) {
			System.assert(false, 'An unexpected exception occurred.' + e.getMessage());
		}
	}

	/**
	 * 経費承認申請処理のテスト
	 */
	@isTest static void submitRequestTest() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		// Set correct ExpTypes
		ExpType__c expType = testData.expType;
		expType.FileAttachment__c = ExpFileAttachment.OPTIONAL.value;
		update expType;
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), expType, testData.employee, 3);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		ExpRequestEntity expRequest = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRequestRecordEntity> requestRecordList = testData.createExpRequestRecords(expRequest.id, Date.today(), testData.expType, testData.employee, testData.job, 1);
		ExpReport__c reportObj = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id =: report.Id];
		reportObj.ExpPreRequestId__c = expRequest.Id;
		update reportObj;

		// 領収書を作成 Create receipt
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion contentVersion = new ContentVersion(
			Title = 'Test',
			PathOnClient = 'Test.jpg',
			VersionData = Blob.valueOf(bodyString));
		insert contentVersion;
		ContentDocument contentDocument = [SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Title = :contentVersion.Title LIMIT 1];
		ContentDocumentLink link = new ContentDocumentLink(
			ContentDocumentId = contentDocument.Id,
			LinkedEntityId = recordList[0].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		insert link;

		// 社員に上長を設定
		testData.employee.getHistory(0).managerId = testEmployeeList[0].id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 申請実行ユーザ
		User user = [SELECT Id FROM User WHERE Id = :testEmployeeList[1].userId LIMIT 1];

		Test.startTest();

		String comment = 'コメント';

		ExpReportRequestService service = new ExpReportRequestService();
		System.runAs(user) {
			service.submitRequest(report.id, comment);
		}

		Test.stopTest();

		// 申請レコードを取得
		ExpReportRequest__c requestObj = [
				SELECT
					Id,
					Name,
					ExpReportId__c,
					TotalAmount__c,
					ReportNo__c,
					Subject__c,
					Comment__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					Approver01Id__c
				FROM ExpReportRequest__c
				WHERE ExpReportId__c = :report.id];

		String requestName = new ExpService().createRequestName('exp00000001', report.totalAmount, report.employeeNameL,
				report.subject, AppLanguage.valueOf(testData.company.Language__c));
		System.assertEquals(requestName, requestObj.Name);
		System.assertEquals(report.id, requestObj.ExpReportId__c);
		System.assertEquals(report.totalAmount, requestObj.TotalAmount__c);
		System.assertEquals('exp00000001', requestObj.ReportNo__c);
		System.assertEquals(report.subject, requestObj.Subject__c);
		System.assertEquals(comment, requestObj.Comment__c);
		System.assertEquals(AppRequestStatus.PENDING.value, requestObj.Status__c);
		System.assertEquals(false, requestObj.ConfirmationRequired__c);
		System.assertEquals(null, requestObj.CancelType__c);
		System.assertEquals(report.employeeHistoryId, requestObj.EmployeeHistoryId__c);
		System.assertEquals(report.departmentHistoryId, requestObj.DepartmentHistoryId__c);
		System.assertEquals(testEmployeeList[1].getHistory(0).id, requestObj.ActorHistoryId__c);
		System.assertEquals(testEmployeeList[0].userId, requestObj.Approver01Id__c);

		// 経費申請に添付ファイルが追加されていることを確認する
		AppAttachmentRepository attachRepo = new AppAttachmentRepository();
		List<AppAttachmentEntity> attaches = attachRepo.getEntityListByParentId(requestObj.Id);
		System.assertEquals(1, attaches.size());
		AppAttachmentEntity attach = attaches[0];
		System.assertEquals(requestObj.Name, attach.name);
		System.assertEquals(requestObj.Id, attach.parentId);
		// 添付ファイルの中身
		ExpService.ExpReportParam jsonData = (ExpService.ExpReportParam)JSON.deserialize(attach.bodyText, ExpService.ExpReportParam.class);
		System.assertEquals('exp00000001', jsonData.reportNo);
		System.assertEquals(report.subject, jsonData.subject);
		// Pre-request validation
		/** TODO: Create more comprehensive test for Expense Request values */
		System.assertEquals(expRequest.Id, jsonData.expPreRequestId);
		System.assertEquals(expRequest.expReportTypeId, jsonData.expPreRequest.expReportTypeId);
		System.assertEquals(expRequest.Id, jsonData.expPreRequest.requestId);
		System.assertEquals(expRequest.getExtendedItemTextId(0), jsonData.expPreRequest.extendedItemText01Id);
		System.assertNotEquals(null, jsonData.expPreRequest.extendedItemText01Info);
		System.assertEquals(expRequest.getExtendedItemTextValue(0), jsonData.expPreRequest.extendedItemText01Value);
		System.assertEquals(expRequest.getExtendedItemTextId(9), jsonData.expPreRequest.extendedItemText10Id);
		System.assertNotEquals(null, jsonData.expPreRequest.extendedItemText10Info);
		System.assertEquals(expRequest.getExtendedItemTextValue(9), jsonData.expPreRequest.extendedItemText10Value);
		// Pre-request Record and Record Item validation
		for (Integer i = 0; i < requestRecordList.size(); i++) {
			ExpRequestRecordEntity expectedRequestRecordEntity = requestRecordList[i];
			ExpService.ExpRequestRecord actualRequestRecord = jsonData.expPreRequest.records[i];

			System.assertEquals(expectedRequestRecordEntity.Id, actualRequestRecord.recordId);

			for (Integer itemIndex = 0; itemIndex < expectedRequestRecordEntity.recordItemList.size(); itemIndex++) {
				ExpRequestRecordItemEntity expectedRequestRecordItemEntity = expectedRequestRecordEntity.recordItemList[itemIndex];
				ExpService.ExpRequestRecordItem actualRequestRecordItem = actualRequestRecord.items[itemIndex];

				System.assertEquals(expectedRequestRecordItemEntity.Id, actualRequestRecordItem.itemId);
				System.assertEquals(expectedRequestRecordItemEntity.expTypeId, actualRequestRecordItem.expTypeId);
				System.assertEquals(expectedRequestRecordItemEntity.getExtendedItemTextId(0), actualRequestRecordItem.extendedItemText01Id);
				System.assertNotEquals(null, actualRequestRecordItem.extendedItemText01Id);
				System.assertEquals(expectedRequestRecordItemEntity.getExtendedItemTextValue(0), actualRequestRecordItem.extendedItemText01Value);
				System.assertEquals(expectedRequestRecordItemEntity.getExtendedItemTextId(9), actualRequestRecordItem.extendedItemText10Id);
				System.assertNotEquals(null, actualRequestRecordItem.extendedItemText10Id);
				System.assertEquals(expectedRequestRecordItemEntity.getExtendedItemTextValue(9), actualRequestRecordItem.extendedItemText10Value);
			}
		}

		// 領収書が申請レコードに紐づけられていることを確認する
		ContentDocumentLink linkObj = [
			SELECT Id
			FROM ContentDocumentLink
			WHERE ContentDocumentId = :contentDocument.Id
			AND LinkedEntityId = :requestObj.Id
			LIMIT 1];

		System.assertNotEquals(null, linkObj);
	}

	/**
	 * 経費承認申請処理のテスト
	 * 申請が代理承認者に共有されることを確認する
	 */
	@isTest static void submitRequestTestSharing() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(4);

		// 社員に上長を設定
		testData.employee.getHistory(0).managerId = testEmployeeList[0].id;
		new EmployeeRepository().saveEntity(testData.employee);

		// 代理承認者を設定
		List<EmployeeBaseEntity> delegatedApproverList = new List<EmployeeBaseEntity>{testEmployeeList[1], testEmployeeList[2], testEmployeeList[3]};
		for (Integer i = 0; i < delegatedApproverList.size(); i++) {
			ComDelegatedApproverSetting__c setting = ComTestDataUtility.createDelegatedApproverSetting(testData.employee.id, delegatedApproverList[i].id);
		}

		Test.startTest();

		new ExpReportRequestService().submitRequest(report.id, null);

		Test.stopTest();

		// 申請が代理承認者に共有されているかの確認
		ExpReportRequest__c requestObj = [SELECT Id FROM ExpReportRequest__c WHERE ExpReportId__c = :report.id];
		List<AppShareEntity> shareList = new ExpReportRequestRepository().getShareEntityListByRequestId(new List<Id>{requestObj.Id});
		Map<Id, AppShareEntity> shareMapByApproverId = new Map<Id, AppShareEntity>();
		for (AppShareEntity share : shareList) {
			shareMapByApproverId.put(share.userOrGroupId, share);
		}
		for (Integer i = 0; i < delegatedApproverList.size(); i++) {
			AppShareEntity share = shareMapByApproverId.get(delegatedApproverList[i].userId);
		//	System.assertEquals(requestObj.Id, share.parentId);
		}
	}

	/**
	 * 経費承認申請処理のテスト
	 * 申請対象の経費精算が見つからない場合、アプリ例外が発生することを確認する
	 */
	@isTest static void submitRequestTestReportNotFound() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// 経費精算を削除
		new ExpReportRepository().deleteEntity(report);

		Test.startTest();

		try {
			new ExpReportRequestService().submitRequest(report.id, null);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NOT_FOUND_EXP_REPORT, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 経費承認申請処理のテスト
	 * Checks if error is thrown when no manager is defined for the employee record
	 */
	@isTest static void submitRequestTestManagerNotFound() {
		ExpTestData testData = new ExpTestData();
		ExpReportEntity report = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRecordEntity> recordList = testData.createExpRecords(report.id, Date.today(), testData.expType, testData.employee, 3);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});

		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);
		ExpRequestEntity expRequest = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRequestRecordEntity> requestRecordList = testData.createExpRequestRecords(expRequest.id, Date.today(), testData.expType, testData.employee, testData.job, 1);
		ExpReport__c reportObj = [SELECT Id, ExpRequestId__c FROM ExpReport__c WHERE Id =: report.Id];
		reportObj.ExpPreRequestId__c = expRequest.Id;
		update reportObj;

		// 申請実行ユーザ
		User user = [SELECT Id FROM User WHERE Id = :testEmployeeList[1].userId LIMIT 1];

		Test.startTest();

		String comment = 'コメント';

		ExpReportRequestService service = new ExpReportRequestService();
		System.runAs(user) {
			try {
				service.submitRequest(report.id, comment);
			} catch (App.IllegalStateException e) {
				System.assertEquals(App.ERR_CODE_NOT_SET_EMP_MANAGER, e.getErrorCode());
			} catch (Exception e) {
				System.debug('例外 : ' + e.getStackTraceString());
				System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
			}
		}

		Test.stopTest();
	}

	/**
	 * 経費承認申請処理のテスト
	 * 会社のデフォルト言語で申請できていることを確認する
	 */
	@isTest static void submitRequestTestLang() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);

		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpReportRequestService service = new ExpReportRequestService();

		Test.startTest();

		// 会社のデフォルト言語が日本語
		service.submitRequest(reportList[0].id, '申請1 JA');

		// 会社のデフォルト言語が英語
		testData.company.Language__c = AppLanguage.EN_US.value;
		update testData.company;
		service.submitRequest(reportList[1].id, '申請2 EN_US');

		Test.stopTest();

		ExpReportRepository reportRepo = new ExpReportRepository();
		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();

		// 経費申請を確認する
		// 日本語
		ExpReportEntity reportEntity1 = reportRepo.getEntity(reportList[0].id);
		ExpReportRequestEntity requestEntity1 = requestRepo.getEntity(reportEntity1.expRequestId);
		System.assertNotEquals(null, requestEntity1);
		String fullName = reportList[0].employeeNameL.getFullName(AppLanguage.JA);
		String requestName = 'exp00000001 ' + reportList[0].totalAmount.setScale(0) + ' ' + fullName + 'さん ' + reportList[0].subject;
		System.assertEquals(requestName, requestEntity1.name);
		// 英語
		ExpReportEntity reportEntity2 = reportRepo.getEntity(reportList[1].id);
		ExpReportRequestEntity requestEntity2 = requestRepo.getEntity(reportEntity2.expRequestId);
		System.assertNotEquals(null, requestEntity2);
		fullName = reportList[1].employeeNameL.getFullName(AppLanguage.EN_US);
		requestName = 'exp00000002 ' + reportList[1].totalAmount.setScale(0) + ' ' + fullName + ' ' + reportList[1].subject;
		System.assertEquals(requestName, requestEntity2.name);

		// 会社のデフォルト言語が設定されていない場合
		// アプリ例外が発生する
		try {
			testData.company.Language__c = null;
			update testData.company;

			service.submitRequest(reportList[2].id, '申請3 言語未設定');
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_COMPANY_LANG, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * 承認対象経費精算一覧データ取得処理のテスト
	 */
	@isTest static void getRequestListTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpReportEntity> reportList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, 3);

		// 社員に上長を設定
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpReportRequestService service = new ExpReportRequestService();

		// 申請を実行
		service.isTestModeAndBypassExpReportValidation = true;
		service.submitRequest(reportList[0].id, null);
		service.submitRequest(reportList[1].id, null);
		service.submitRequest(reportList[2].id, null);

		// 申請レコードを取得
		List<ExpReportRequest__c> requestObjList = [
				SELECT
					Id,
					Name,
					ExpReportId__c,
					Comment__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					Approver01Id__c
				FROM ExpReportRequest__c
				ORDER BY ReportNo__c DESC];

		List<ID> requestIdList = new List<ID>();
		for (ExpReportRequest__c requestObj : requestObjList) {
			requestIdList.add(requestObj.Id);
		}

		Test.startTest();

		List<ExpReportRequestEntity> requestEntityList = service.getRequestList(requestIdList);

		Test.stopTest();

		for (Integer i=0; i<requestEntityList.size(); i++) {
			System.assertEquals(requestObjList[i].Id, requestEntityList[i].id);
			System.assertEquals(requestObjList[i].Name, requestEntityList[i].name);
			System.assertEquals(requestObjList[i].ExpReportId__c, requestEntityList[i].expReportId);
			System.assertEquals(requestObjList[i].Comment__c, requestEntityList[i].comment);
			String status = requestEntityList[i].status == null ? null : requestEntityList[i].status.value;
			System.assertEquals(requestObjList[i].Status__c, status);
			System.assertEquals(requestObjList[i].ConfirmationRequired__c, requestEntityList[i].isConfirmationRequired);
			String cancelType = requestEntityList[i].cancelType == null ? null : requestEntityList[i].cancelType.value;
			System.assertEquals(requestObjList[i].CancelType__c, cancelType);
			System.assertEquals(requestObjList[i].EmployeeHistoryId__c, requestEntityList[i].employeeHistoryId);
			System.assertEquals(requestObjList[i].RequestTime__c, AppDatetime.convertDatetime(requestEntityList[i].requestTime));
			System.assertEquals(requestObjList[i].DepartmentHistoryId__c, requestEntityList[i].departmentHistoryId);
			System.assertEquals(requestObjList[i].ActorHistoryId__c, requestEntityList[i].actorHistoryId);
			System.assertEquals(requestObjList[i].Approver01Id__c, requestEntityList[i].approver01Id);
		}
	}
}