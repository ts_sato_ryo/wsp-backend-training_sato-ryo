/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇付与サマリのリポジトリ
 */
public with sharing class AttManagedLeaveGrantSummaryRepository extends Repository.BaseRepository {
	/** 有休付与サマリ取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_GRANT_SUMMARY_FIELD_SET;
	static {
		GET_GRANT_SUMMARY_FIELD_SET = new Set<Schema.SObjectField> {
				AttManagedLeaveGrantSummary__c.Id,
				AttManagedLeaveGrantSummary__c.LeaveSummaryId__c,
				AttManagedLeaveGrantSummary__c.LeaveGrantId__c,
				AttManagedLeaveGrantSummary__c.DaysTaken__c,
				AttManagedLeaveGrantSummary__c.DaysLeft__c,
				AttManagedLeaveGrantSummary__c.DaysExpired__c,
				AttManagedLeaveGrantSummary__c.DaysAdjusted__c
		};
	}
	/** リレーション名（休暇サマリの勤怠サマリId) */
	private static final String ATT_SUMMARY_R =
			AttManagedLeaveSummary__c.AttSummaryId__c.getDescribe().getRelationshipName();
	/** リレーション名（休暇付与サマリの休暇サマリId) */
	private static final String LEAVE_SUMMARY_R =
			AttManagedLeaveGrantSummary__c.LeaveSummaryId__c.getDescribe().getRelationshipName();
	/** リレーション名（休暇付与サマリの付与Id) */
	private static final String LEAVE_GRANT_R =
			AttManagedLeaveGrantSummary__c.LeaveGrantId__c.getDescribe().getRelationshipName();
	/** リレーション名（休暇Id) */
	private static final String LEAVE_R =
			AttManagedLeaveGrant__c.LeaveId__c.getDescribe().getRelationshipName();

	/** 検索フィルタ */
	public class SearchFilter {
		public Set<Id> grantIds;
		public Set<Id> leaveSummaryIds;
		public Set<Id> empIds;
		public Set<Id> leaveIds;
		public AppDate dateFrom;
		public AppDate dateTo;
		public Boolean onlyUnAdmit;
	}
	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AttManagedLeaveGrantSummaryEntity entity) {
		return saveEntityList(new List<AttManagedLeaveGrantSummaryEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveGrantSummaryEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveGrantSummaryEntity> entityList, Boolean allOrNone) {

		List<AttManagedLeaveGrantSummary__c> grantSummaryList = new List<AttManagedLeaveGrantSummary__c>();

		for (AttManagedLeaveGrantSummaryEntity entity : entityList) {
			// エンティティからSObjectを作成する
			grantSummaryList.add(createObj(entity));
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(grantSummaryList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}
	public List<AttManagedLeaveGrantSummaryEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {
		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_GRANT_SUMMARY_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		selectFldList.add(LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.ValidFrom__c));
		selectFldList.add(LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.ValidTo__c));
		selectFldList.add(LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.DaysGranted__c));
		selectFldList.add(LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.EmployeeBaseId__c));
		selectFldList.add(LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.LeaveId__c));
		selectFldList.add(LEAVE_GRANT_R + '.' + LEAVE_R + '.' + getFieldName(AttLeave__c.Type__c));
		selectFldList.add(LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.Comment__c));

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Set<Id> grantIds = filter.grantIds;
		final Set<Id> empIds = filter.empIds;
		final Set<Id> leaveSummaryIds = filter.leaveSummaryIds;
		final Set<Id> leaveIds = filter.leaveIds;
		final Date dateFrom = AppConverter.dateValue(filter.dateFrom);
		final Date dateTo = AppConverter.dateValue(filter.dateTo);

		if (grantIds != null) {
			condList.add(getFieldName(AttManagedLeaveGrantSummary__c.LeaveGrantId__c) + ' IN :grantIds');
		}
		if (leaveSummaryIds != null) {
			condList.add(getFieldName(AttManagedLeaveGrantSummary__c.LeaveSummaryId__c) + ' IN :leaveSummaryIds');
		}
		if (leaveIds != null) {
			condList.add(LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.LeaveId__c) + ' IN :leaveIds');
		}
		if (dateFrom != null) {
			condList.add(LEAVE_SUMMARY_R + '.' + getFieldName(AttManagedLeaveGrant__c.ValidFrom__c) + ' >= :dateFrom');
		}
		if (dateTo != null) {
			condList.add(LEAVE_SUMMARY_R + '.' + getFieldName(AttManagedLeaveGrant__c.ValidTo__c) + ' < :dateTo');
		}
		if (filter.onlyUnAdmit == true) {
			condList.add(LEAVE_SUMMARY_R + '.' + ATT_SUMMARY_R + '.' + getFieldName(AttSummary__c.Locked__c) + ' = false');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ') +
				+ ' FROM ' + AttManagedLeaveGrantSummary__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (!forUpdate) {
			soql += ' ORDER BY ' + LEAVE_GRANT_R + '.' + getFieldName(AttManagedLeaveGrant__c.ValidFrom__c) + ' ASC';
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// 結果からエンティティを作成する
		List<AttManagedLeaveGrantSummaryEntity> entityList = new List<AttManagedLeaveGrantSummaryEntity>();
		AppLimits.getInstance().setCurrentQueryCount();
		for (AttManagedLeaveGrantSummary__c grantSummary : Database.query(soql)) {
			entityList.add(createEntity(grantSummary));
		}
		AppLimits.getInstance().incrementQueryCount(AttManagedLeaveGrantSummary__c.SObjectType);

		return entityList;
	}
	public DeleteResult deleteEntityList(List<AttManagedLeaveGrantSummaryEntity> entityList) {
		return super.deleteEntityList(entityList);
	}

	public AttManagedLeaveGrantSummaryEntity createEntity(AttManagedLeaveGrantSummary__c summary) {
		AttManagedLeaveGrantSummaryEntity retEntity = new AttManagedLeaveGrantSummaryEntity();
		retEntity.setId(summary.Id);

		retEntity.summaryId = summary.LeaveSummaryId__c;
		retEntity.grantId = summary.LeaveGrantId__c;
		retEntity.daysTaken = AttDays.valueOf(summary.DaysTaken__c);
		retEntity.daysLeft = AttDays.valueOf(summary.DaysLeft__c);
		retEntity.daysExpired = AttDays.valueOf(summary.DaysExpired__c);
		retEntity.daysAdjusted = AttDays.valueOf(summary.DaysAdjusted__c);
		retEntity.leaveId = summary.LeaveGrantId__r.LeaveId__c;
		retEntity.leaveType = AttLeaveType.valueOf(summary.LeaveGrantId__r.LeaveId__r.Type__c);
		retEntity.employeeId = summary.LeaveGrantId__r.EmployeeBaseId__c;
		retEntity.daysGranted = AttDays.valueOf(summary.LeaveGrantId__r.DaysGranted__c);
		retEntity.validDateFrom = AppDate.valueOf(summary.LeaveGrantId__r.ValidFrom__c);
		retEntity.validDateTo = AppDate.valueOf(summary.LeaveGrantId__r.ValidTo__c);
		retEntity.comment = summary.LeaveGrantId__r.Comment__c;
		retEntity.resetChanged();
		return retEntity;
	}
	private AttManagedLeaveGrantSummary__c createObj(AttManagedLeaveGrantSummaryEntity entity) {
		Boolean isInsert = String.isBlank(entity.id);
		AttManagedLeaveGrantSummary__c retObject = new AttManagedLeaveGrantSummary__c();
		retObject.Id = entity.Id;

		if (isInsert && entity.isChanged(AttManagedLeaveGrantSummaryEntity.Field.SUMMARY_ID)) {
			retObject.LeaveSummaryId__c = entity.summaryId;
		}
		if (isInsert && entity.isChanged(AttManagedLeaveGrantSummaryEntity.Field.GRANT_ID)) {
			retObject.LeaveGrantId__c = entity.grantId;
		}
		if (entity.isChanged(AttManagedLeaveGrantSummaryEntity.Field.DAYS_TAKEN)) {
			retObject.DaysTaken__c = AppConverter.decValue(entity.daysTaken);
		}
		if (entity.isChanged(AttManagedLeaveGrantSummaryEntity.Field.DAYS_LEFT)) {
			retObject.DaysLeft__c = AppConverter.decValue(entity.daysLeft);
		}
		if (entity.isChanged(AttManagedLeaveGrantSummaryEntity.Field.DAYS_EXPIRED)) {
			retObject.DaysExpired__c = AppConverter.decValue(entity.daysExpired);
		}
		if (entity.isChanged(AttManagedLeaveGrantSummaryEntity.Field.DAYS_ADJUSTED)) {
			retObject.DaysAdjusted__c = AppConverter.decValue(entity.daysAdjusted);
		}
		return retObject;
	}
}