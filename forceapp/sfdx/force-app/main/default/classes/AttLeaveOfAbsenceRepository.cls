/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休職休業マスタのリポジトリ
 */
public with sharing class AttLeaveOfAbsenceRepository extends LogicalDeleteRepository {

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無 */
	@testVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** キャッシュ */
	private static final Repository.Cache CACHE = new Repository.Cache();

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_LEAVE_FIELD_SET = new Set<Schema.SObjectField> {
			AttLeaveOfAbsence__c.Id,
			AttLeaveOfAbsence__c.CompanyId__c,
			AttLeaveOfAbsence__c.Code__c,
			AttLeaveOfAbsence__c.Name,
			AttLeaveOfAbsence__c.Name_L0__c,
			AttLeaveOfAbsence__c.Name_L1__c,
			AttLeaveOfAbsence__c.Name_L2__c,
			AttLeaveOfAbsence__c.UniqKey__c,
			AttLeaveOfAbsence__c.Removed__c
	};

	/**
	 * コンストラクタ
	 * キャッシュを利用しないインスタンスを生成する
	 */
	public AttLeaveOfAbsenceRepository() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public AttLeaveOfAbsenceRepository(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/** 検索フィルタ */
	public class SearchFilter {
		public Set<String> ids = new Set<String>();
		public Set<String> codes = new Set<String>();
		public String companyId;
	}

	/*
	 * IDを条件に休職休業マスタを検索する。
	 * 論理削除済みのレコードは取得対象外。
	 * @param id 検索条件のID
	 * @return 検索結果 該当するマスタが存在しない場合はnull
	 */
	public AttLeaveOfAbsenceEntity searchById(Id id) {
		SearchFilter filter = new SearchFilter();
		filter.ids.add(id);
		List<AttLeaveOfAbsenceEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 会社IDとコードを条件に休職休業マスタを検索する。
	 * 論理削除済みのレコードは取得対象外。
	 * @param companyId 会社ID
	 * @param code コード
	 * @return 検索結果 該当するマスタが存在しない場合はnull
	 */
	public AttLeaveOfAbsenceEntity searchByCode(Id companyId, String code) {
		SearchFilter filter = new SearchFilter();
		filter.companyId = companyId;
		filter.codes.add(code);
		List<AttLeaveOfAbsenceEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 検索フィルタの条件に一致する、論理削除されていない休職休業マスタを、コードの昇順で取得する。
	 * 当メソッドではレコードをロックしない。(forUpdateではない)
	 * @param filter 検索フィルタ
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<AttLeaveOfAbsenceEntity> searchEntityList(SearchFilter filter) {
		return searchEntityList(filter, false);
	}

	/**
	 * 検索フィルタの条件に一致する、論理削除されていない休職休業マスタを、コードの昇順で取得する。
	 * @param filter 検索フィルタ
	 * @param forUpdate 更新用で取得する場合はtrue(ロックする)
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<AttLeaveOfAbsenceEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchEntityList ReturnCache key:' + cacheKey);
			List<AttLeaveOfAbsenceEntity> entities = new List<AttLeaveOfAbsenceEntity>();
			for (AttLeaveOfAbsence__c sObj : (List<AttLeaveOfAbsence__c>)CACHE.get(cacheKey)) {
				entities.add(createEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchEntityList NoCache key:' + cacheKey);

		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_LEAVE_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// WHERE句を生成
		final Set<String> ids = filter.ids;
		final Id companyId = filter.companyId;
		final Set<String> codes = filter.codes;
		List<String> conditions = new List<String>();
		if (!ids.isEmpty()) {
			conditions.add(getFieldName(AttLeaveOfAbsence__c.Id) + ' IN :ids');
		}
		if (companyId != null) {
			conditions.add(getFieldName(AttLeaveOfAbsence__c.CompanyId__c) + ' = :companyId');
		}
		if (!codes.isEmpty()) {
			conditions.add(getFieldName(AttLeaveOfAbsence__c.Code__c) + ' IN :codes');
		}
		conditions.add(getFieldName(AttLeaveOfAbsence__c.Removed__c) + ' = false');

		// SOQLを生成
		String soql = 'SELECT ' + String.join(selectFldList, ', ')
					+ ' FROM ' + AttLeaveOfAbsence__c.SObjectType.getDescribe().getName()
					+ buildWhereString(conditions);
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(AttLeaveOfAbsence__c.Code__c) + ' Asc';
		}

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttLeaveOfAbsence__c> sObjList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttLeaveOfAbsence__c.SObjectType);
		List<AttLeaveOfAbsenceEntity> entityList = new List<AttLeaveOfAbsenceEntity>();
		for (AttLeaveOfAbsence__c sobj : sObjList) {
			entityList.add(createEntity(sobj));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			CACHE.put(cacheKey, sObjList);
		}
		return entityList;
	}

	/**
	 * 休職休業エンティティを休職休業マスタに登録、もしくは更新を行う。
	 * @param entity 登録対象の休職休業エンティティ（nullは許容しない）
	 * @return レコード保存結果
	 */
	 public SaveResult saveEntity(AttLeaveOfAbsenceEntity entity) {
		 return saveEntityList(new List<AttLeaveOfAbsenceEntity>{ entity });
	 }

	/**
	 * 休職休業エンティティのリストを休職休業マスタに登録、もしくは更新を行う。
	 * @param entityList 登録対象の休職休業エンティティのリスト（nullは許容しない）
	 * @return レコード保存結果
	 */
	 public SaveResult saveEntityList(List<AttLeaveOfAbsenceEntity> entityList) {

		// エンティティをsObjectに変換する
		List<AttLeaveOfAbsence__c> sObjList = new List<AttLeaveOfAbsence__c>();
		for (AttLeaveOfAbsenceEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(sObjList);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	 }

	/**
	 * AttLeaveOfAbsenceEntityからAttLeaveOfAbsence__cを作成する
	 * @param entity 作成元の休職休業エンティティ（nullは許容しない）
	 * @return 作成した休職休業のsObject
	 */
	@testVisible
	private AttLeaveOfAbsence__c createSObject(AttLeaveOfAbsenceEntity entity) {

		AttLeaveOfAbsence__c sobj = new AttLeaveOfAbsence__c(Id = entity.Id);
		if (entity.isChanged(AttLeaveOfAbsenceEntity.Field.NAME)) {
			sobj.Name = entity.name;
		}

		if (entity.isChanged(AttLeaveOfAbsenceEntity.Field.COMPANY_ID)) {
			sobj.CompanyId__c = entity.companyId;
		}

		if (entity.isChanged(AttLeaveOfAbsenceEntity.Field.CODE)) {
			sobj.Code__c = entity.code;
		}

		if (entity.isChanged(AttLeaveOfAbsenceEntity.Field.UNIQUE_KEY)) {
			sobj.UniqKey__c = entity.uniqKey;
		}

		if (entity.isChanged(AttLeaveOfAbsenceEntity.Field.NAME_L0)) {
			sobj.Name_L0__c = entity.nameL0;
		}

		if (entity.isChanged(AttLeaveOfAbsenceEntity.Field.NAME_L1)) {
			sobj.Name_L1__c = entity.nameL1;
		}

		if (entity.isChanged(AttLeaveOfAbsenceEntity.Field.NAME_L2)) {
			sobj.Name_L2__c = entity.nameL2;
		}

		if (entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED)) {
			sobj.Removed__c = entity.isRemoved;
		}
		return sobj;
	}

	@testVisible
	private AttLeaveOfAbsenceEntity createEntity(AttLeaveOfAbsence__c sobj) {
		AttLeaveOfAbsenceEntity entity = new AttLeaveOfAbsenceEntity();
		entity.setId(sobj.Id);
		entity.companyId = sobj.CompanyId__c;
		entity.code = sobj.Code__c;
		entity.uniqKey = sobj.UniqKey__c;
		entity.name = sobj.Name;
		entity.nameL0 = sobj.Name_L0__c;
		entity.nameL1 = sobj.Name_L1__c;
		entity.nameL2 = sobj.Name_L2__c;
		entity.isRemoved = sobj.Removed__c;

		// 変更情報をリセットする
		entity.resetChanged();

		return entity;
	}
}