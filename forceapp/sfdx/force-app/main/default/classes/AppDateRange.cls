/**
 * 日付範囲の値オブジェクト
 */
public with sharing class AppDateRange extends ValueObject {

	/** 範囲開始日 */
	public final AppDate startDate {
		get;
		private set;
	}

	/** 範囲終了日 */
	public final AppDate endDate {
		get;
		private set;
	}

	/**
	 * コンストラクタ
	 *
	 * NOTE:Winter'19では範囲の片方をnullとするインスタンスを許容しない。
	 * 「ある日以降」などの片方のみ指定したオブジェクトを生成したい出た場合には、
	 * 未指定の項目にnullではなく AppDate.MAX or MINを内部で保持するコンストラクタの実装を検討すること。
	 *
	 * @param startDate 範囲From
	 * @param endDate 範囲To
	 * @throws App.ParameterException startDateもしくはendDateがnullの場合
	 *                                startDateよりもendDateが前の日付の場合
	 */
	public AppDateRange(AppDate startDate, AppDate endDate) {
		if (startDate == null) {
			App.ParameterException e = new App.ParameterException();
			e.setMessageIsBlank('startDate');
			throw e;
		}
		if (endDate == null) {
			App.ParameterException e = new App.ParameterException();
			e.setMessageIsBlank('endDate');
			throw e;
		}
		// 日付の前後関係が不正な場合（同日は許容する）
		if (startDate.getDate() > endDate.getDate()) {
			App.ParameterException e = new App.ParameterException();
			e.setMessage('The endDate is set before the startDate');
			throw e;
		}
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * 範囲内の全ての日付のリストを返す。
	 * @return 範囲内の全ての日付のリスト
	 */
	public List<AppDate> values() {
		return values(true, true);
	}

	/**
	 * 範囲に含む日付のリストを返す。
	 * 開始日が2019/1/1、終了日が2019/1/4の場合、引数は以下の様に制御される。
	 * includeStartDateがfalseの場合、戻り値は開始日を含まない [2019/1/2, 2019/1/3, 2019/1/4]となる。
	 * includeEndDateがfalseの場合、戻り値は終了日を含まない [2019/1/1, 2019/1/2, 2019/1/3]となる。
	 *
	 * @param includeStartDate 開始日を含むかのフラグ
	 * @param includeEndDate 終了日を含むかのフラグ
	 * @return 範囲に含む日付のリスト
	 */
	private List<AppDate> values(Boolean includeStartDate, Boolean includeEndDate) {
		AppDate startDate = includeStartDate ? this.startDate : this.startDate.addDays(1);
		AppDate endDate = includeEndDate ? this.endDate : this.endDate.minusDays(1);

		List<AppDate> values = new List<AppDate>();
		for (AppDate d = startDate; !d.isAfter(endDate); d = d.addDays(1)) {
			values.add(d);
		}
		return values;
	}

	/**
	 * オブジェクトの値が等しいかどうか判定する
 	 * @param value 比較対象のオブジェクト
	 * @retun オブジェクトが等しい場合はtrue
	 */
	public override Boolean equals(Object value) {
		if (value instanceof AppDateRange) {
			return this.startDate.equals(((AppDateRange)value).startDate) &&
					this.endDate.equals(((AppDateRange)value).endDate);
		}
		return false;
	}

	/**
	 * オブジェクトのハッシュコードを取得する
	 * @retun
	 */
	public Integer hashCode() {
		Integer result = 17;
		result = 31 * result + startDate.hashCode();
		result = 31 * result + endDate.hashCode();
		return result ;
	}

	/** toStringのオーバーライド */
	public override String toString() {
		return startDate.formatYYYYMMDD() + '-' + endDate.formatYYYYMMDD();
	}
}