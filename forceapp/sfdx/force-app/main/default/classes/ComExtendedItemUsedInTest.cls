/**
 * ComExtendedItemUsedInのテスト
 */
@isTest
private class ComExtendedItemUsedInTest {

	/**
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(ComExtendedItemUsedIn.EXPENSE_REPORT, ComExtendedItemUsedIn.valueOf('ExpenseReport'));
		System.assertEquals(ComExtendedItemUsedIn.EXPENSE_REQUEST_AND_EXPENSE_REPORT, ComExtendedItemUsedIn.valueOf('ExpenseRequestAndExpenseReport'));

		// 等しくない場合(値が異なる) → false
		System.assertEquals(false, ComExtendedItemUsedIn.EXPENSE_REPORT.equals(ComExtendedItemUsedIn.valueOf('ExpenseRequestAndExpenseReport')));

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, ComExtendedItemUsedIn.EXPENSE_REPORT.equals(Integer.valueOf(123)));

		// 等しくない場合(null)
		System.assertEquals(false, ComExtendedItemUsedIn.EXPENSE_REPORT.equals(null));
	}
}