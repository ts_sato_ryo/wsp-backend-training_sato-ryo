/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 勤怠系APIの権限チェック機能を提供するクラス
 * 勤怠系Resourceクラス以外からは呼び出さないでください
 */
public with sharing class AttResourcePermission {

	/** API実行に必要な権限 */
	public enum Permission {
		/** 勤務表表示(代理) */
		VIEW_ATT_TIME_SHEET_BY_DELEGATE,
		/** 勤務表編集(代理) */
		EDIT_ATT_TIME_SHEET_BY_DELEGATE,
		/** 各種勤怠申請申請・申請削除(代理) */
		SUBMIT_ATT_DAILY_REQUEST_BY_DELEGATE,
		/** 各種勤怠申請承認・却下(代理) */
		APPROVE_ATT_DAILY_REQUEST_BY_DELEGATE,
		/** 各種勤怠申請自己承認(本人) */
		APPROVE_SELF_ATT_DAILY_REQUEST_BY_EMPLOYEE,
		/** 各種勤怠申請申請取消(代理) */
		CANCEL_ATT_DAILY_REQUEST_BY_DELEGATE,
		/** 各種勤怠申請承認取消(本人) */
		CANCEL_ATT_DAILY_APPROVAL_BY_EMPLOYEE,
		/** 各種勤怠申請承認取消(代理) */
		CANCEL_ATT_DAILY_APPROVAL_BY_DELEGATE,
		/** 勤務確定申請申請(代理) */
		SUBMIT_ATT_REQUEST_BY_DELEGATE,
		/** 勤務確定申請承認・却下(代理) */
		APPROVE_ATT_REQUEST_BY_DELEGATE,
		/** 勤務確定申請自己承認(本人) */
		APPROVE_SELF_ATT_REQUEST_BY_EMPLOYEE,
		/** 勤務確定申請申請取消(代理) */
		CANCEL_ATT_REQUEST_BY_DELEGATE,
		/** 勤務確定申請承認取消(本人) */
		CANCEL_ATT_APPROVAL_BY_EMPLOYEE,
		/** 勤務確定申請承認取消(代理) */
		CANCEL_ATT_APPROVAL_BY_DELEGATE
	}

	/** 社員の権限サービス */
	private EmployeePermissionService permissionService;

	/** ログインユーザの社員 */
	private EmployeeBaseEntity loginEmployee {
		get;
		set;
	}

	/**
	 * コンストラクタ
	 */
	public AttResourcePermission() {
		this.permissionService = new EmployeePermissionService();
		// ログインユーザ社員を取得しておく
		try {
			this.loginEmployee = new EmployeeService().getEmployeeByUserIdFromCache(UserInfo.getUserId(), AppDate.today());
		} catch (App.IllegalStateException e) {
			// システム管理者の場合もあるので何もしない
		} catch (App.RecordNotFoundException ex) {
			// システム管理者の場合もあるので何もしない
		}
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員ベースIDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @param targetEmployeeId 処理対象(データ取得対象、申請対象)の社員ベースID
	 *        代理で実行するか否かの判定に使用する。処理対象がログインユーザの社員の場合はnull
	 */
	public void hasExecutePermission(
			List<AttResourcePermission.Permission> requiredPermissionList, String targetEmployeeId) {

		if (isDelegateEmployee(targetEmployeeId)) {
			// 代理で実行できることをチェックする
			hasPermissionByDelegate(requiredPermissionList);
		} else {
			// 本人が実行できることをチェックする
			hasPermissionByEmployee(requiredPermissionList);
		}
	}

	/**
	 * APIの実行に必要な権限を持っていることを確認する(社員履歴IDを指定)
	 * @param requiredPermissionList 実行に必要な権限のリスト
	 * @param employeeHistoryId 処理対象(データ取得対象、申請対象)の社員履歴ID
	 *        代理で実行するか否かの判定に使用する。処理対象がログインユーザの社員の場合はnull
	 */
	public void hasExecutePermissionByEmployeeHistoryId(
			List<AttResourcePermission.Permission> requiredPermissionList, Id employeeHistoryId) {
		EmployeeRepository.SearchFilter filter = new EmployeeRepository.SearchFilter();
		filter.historyIds = new Set<Id>{employeeHistoryId};
		filter.includeRemoved = true;
		List<EmployeeBaseEntity> employeeList = new EmployeeRepository().searchEntity(filter);
		if (! employeeList.isEmpty()) {
			hasExecutePermission(requiredPermissionList, employeeList[0].id);
		}
	}

	/**
	 * 指定された権限(代理)を持っているかをチェックする
	 * @param requiredPermissionList チェック対象の代理権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	public void hasPermissionByDelegate(List<AttResourcePermission.Permission> requiredPermissionList) {
		List<AttResourcePermission.Permission> errorPermissionList =
				new List<AttResourcePermission.Permission>();
		for (Permission requiredPermission : requiredPermissionList) {
			// 勤務表表示(代理)権限
			if (requiredPermission == Permission.VIEW_ATT_TIME_SHEET_BY_DELEGATE) {
				if (! permissionService.hasViewAttTimeSheetByDelegate) {
					errorPermissionList.add(Permission.VIEW_ATT_TIME_SHEET_BY_DELEGATE);
				}
			// 勤務表編集(代理)権限
			} else if (requiredPermission == Permission.EDIT_ATT_TIME_SHEET_BY_DELEGATE) {
				if (! permissionService.hasEditAttTimeSheetByDelegate) {
					errorPermissionList.add(Permission.EDIT_ATT_TIME_SHEET_BY_DELEGATE);
				}
			// 各種勤怠申請申請・申請削除(代理)権限
			} else if (requiredPermission == Permission.SUBMIT_ATT_DAILY_REQUEST_BY_DELEGATE) {
				if (! permissionService.hasSubmitAttDailyRequestByDelegate) {
					errorPermissionList.add(Permission.SUBMIT_ATT_DAILY_REQUEST_BY_DELEGATE);
				}
			// 各種勤怠申請承認・却下(代理)権限
			} else if (requiredPermission == Permission.APPROVE_ATT_DAILY_REQUEST_BY_DELEGATE) {
				if (! permissionService.hasApproveAttDailyRequestByDelegate) {
					errorPermissionList.add(Permission.APPROVE_ATT_DAILY_REQUEST_BY_DELEGATE);
				}
			// 各種勤怠申請申請取消(代理)権限
			} else if (requiredPermission == Permission.CANCEL_ATT_DAILY_REQUEST_BY_DELEGATE) {
				if (! permissionService.hasCancelAttDailyRequestByDelegate) {
					errorPermissionList.add(Permission.CANCEL_ATT_DAILY_REQUEST_BY_DELEGATE);
				}
			// 各種勤怠申請承認取消(代理)権限
			} else if (requiredPermission == Permission.CANCEL_ATT_DAILY_APPROVAL_BY_DELEGATE) {
				if (! permissionService.hasCancelAttDailyApprovalByDelegate) {
					errorPermissionList.add(Permission.CANCEL_ATT_DAILY_APPROVAL_BY_DELEGATE);
				}
			// 勤務確定申請申請(代理)権限
			} else if (requiredPermission == Permission.SUBMIT_ATT_REQUEST_BY_DELEGATE) {
				if (! permissionService.hasSubmitAttRequestByDelegate) {
					errorPermissionList.add(Permission.SUBMIT_ATT_REQUEST_BY_DELEGATE);
				}
			// 勤務確定申請承認・却下(代理)権限
			} else if (requiredPermission == Permission.APPROVE_ATT_REQUEST_BY_DELEGATE) {
				if (! permissionService.hasApproveAttRequestByDelegate) {
					errorPermissionList.add(Permission.APPROVE_ATT_REQUEST_BY_DELEGATE);
				}
			// 勤務確定申請申請取消(代理)権限
			} else if (requiredPermission == Permission.CANCEL_ATT_REQUEST_BY_DELEGATE) {
				if (! permissionService.hasCancelAttRequestByDelegate) {
					errorPermissionList.add(Permission.CANCEL_ATT_REQUEST_BY_DELEGATE);
				}
			// 勤務確定申請承認取消(代理)権限
			} else if (requiredPermission == Permission.CANCEL_ATT_APPROVAL_BY_DELEGATE) {
				if (! permissionService.hasCancelAttApprovalByDelegate) {
					errorPermissionList.add(Permission.CANCEL_ATT_APPROVAL_BY_DELEGATE);
				}
			}
		}

		if (! errorPermissionList.isEmpty()) {
			// デバッグ用にログを出力しておく
			System.debug('--- API実行に不足している権限=' + errorPermissionList);
			throw new App.NoPermissionException(ComMessage.msg().Com_Err_NoApiPermission);
		}
	}

	/**
	 * 指定された権限(本人)を持っているかをチェックする
	 * @param requiredPermissionList チェック対象の代理権限
	 * @throws App.NoPermissionException 権限を持っていない場合
	 */
	private void hasPermissionByEmployee(List<AttResourcePermission.Permission> requiredPermissionList) {

		List<AttResourcePermission.Permission> errorPermissionList =
				new List<AttResourcePermission.Permission>();
		for (Permission requiredPermission : requiredPermissionList) {
			// 勤務確定申請承認取消(本人)権限
			if (requiredPermission == Permission.CANCEL_ATT_APPROVAL_BY_EMPLOYEE) {
				if (! permissionService.hasCancelAttApprovalByEmployee) {
					errorPermissionList.add(Permission.CANCEL_ATT_APPROVAL_BY_EMPLOYEE);
				}
			// 各種勤怠申請承認取消(本人)権限
			} else if (requiredPermission == Permission.CANCEL_ATT_DAILY_APPROVAL_BY_EMPLOYEE) {
				if (! permissionService.hasCancelAttDailyApprovalByEmployee) {
					errorPermissionList.add(Permission.CANCEL_ATT_DAILY_APPROVAL_BY_EMPLOYEE);
				}
			// 勤務確定申請自己承認(本人)権限
			} else if (requiredPermission == Permission.APPROVE_SELF_ATT_REQUEST_BY_EMPLOYEE) {
				if (! permissionService.hasApproveSelfAttRequestByEmployee) {
					errorPermissionList.add(Permission.APPROVE_SELF_ATT_REQUEST_BY_EMPLOYEE);
				}
			// 各種勤怠申請自己承認(本人)権限
			} else if (requiredPermission == Permission.APPROVE_SELF_ATT_DAILY_REQUEST_BY_EMPLOYEE) {
				if (! permissionService.hasApproveSelfAttDailyRequestByEmployee) {
					errorPermissionList.add(Permission.APPROVE_SELF_ATT_DAILY_REQUEST_BY_EMPLOYEE);
				}
			}
		}

		if (! errorPermissionList.isEmpty()) {
			// デバッグ用にログを出力しておく
			System.debug('--- API実行に不足している権限=' + errorPermissionList);
			throw new App.NoPermissionException(ComMessage.msg().Com_Err_NoApiPermission);
		}
	}

	/**
	 * ログインユーザが代理社員であるかどうかを確認する
	 * @param empBaseId 処理対象の社員ベースID、nullの場合はログインユーザの社員データが処理対象とみなす
	 * @return 代理社員の場合はtrue、そうでない場合はfalse
	 */
	private Boolean isDelegateEmployee(String empBaseId) {

		// ログインユーザに社員が設定されていない場合は(システム管理者ユーザなど)
		// 代理社員で実行するものとみなす
		if (this.loginEmployee == null) {
			return true;
		}

		// empBaseIdが空文字、またはnullの場合は代理ではないとする
		if (String.isBlank(empBaseId)) {
			return false;
		}

		if (this.loginEmployee.id != empBaseId) {
			return true;
		}
		return false;
	}

}