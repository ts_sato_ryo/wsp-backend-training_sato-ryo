/**
 * @group チーム
 *
 * @description チーム機能で利用する勤怠リソースのAPI(TeamAttendanceResource.class)を提供するクラス
 */
@isTest
private class TeamAttendanceResourceTest {

	/** テストデータ */
	private class ResourceTestData {
		/** テストデータ */
		public AttTestData testData;
		/** 部署オブジェクト */
		public ComDeptBase__c dept;
		/** 勤務社員 */
		public EmployeeBaseEntity employee;
		/** 承認者(上長) */
		public EmployeeBaseEntity approver;
		/** 勤務体系 */
		public AttWorkingTypeBaseEntity workingType;

		/**
		 * コンストラクタ（社員に勤務体系を設定する)
		 * @param wkType 労働時間制
		 */
		public ResourceTestData(AttWorkSystem wkType) {
			testData = new AttTestData(wkType);
			dept = testData.dept;
			employee = testData.employee;
			workingType = testData.workingType;
			// 勤務社員の承認者01設定
			approver = testData.createTestEmployees(1).get(0);
			update new ComEmpHistory__c(
					Id = this.employee.getHistory(0).id,
					ApproverBase01Id__c = approver.id);
		}

		/**
		 * 指定した月の勤怠データ（サマリー＋明細）を作成し、DBに登録する(月度専用)
		 * @param startDate 開始日
		 * @param createPreNext 対象日の前の月度と次の月度も作成する場合はtrue
		 *
		 */
		public void createEmpMonthlySummary(AppDate startDate, Boolean createPreNext) {
			testData.createAttSummaryWithRecordsMonth(this.employee.getHistory(0).id,
					this.workingType.getHistory(0).id, getDeptHistId(dept.id), startDate, createPreNext);
		}

		/**
		 * 部署データを作成する
		 * @param  部署名
		 */
		public Id getDeptHistId(Id id){
			return [SELECT BaseId__c, Id FROM ComDeptHistory__c where BaseId__c = :id][0].id;
		}
	}

	/**
	 * SearchApiのパラメータであるSummarySearchConditionのバリデーションテスト
	 * 全ての正常値を入力した場合、validateメソッドが正常終了することを確認する
	 */
	@isTest
	static void summarySearchConditionValid() {
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData(AttWorkSystem.JP_FIX);
		TeamAttendanceResource.SummarySearchCondition param = new TeamAttendanceResource.SummarySearchCondition();
		param.targetYear = '2018';
		param.targetMonthly = '10';
		param.departmentId = testData.dept.id;

		// 検証
		try {
			param.validate();
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました'+e.getMessage()+e.getStackTraceString());
		}
	}


	/**
	 * SearchApiのパラメータであるSummarySearchConditionのバリデーションテスト
	 * 不正値を入力した場合、validateメソッドが異常終了することを確認する
	 */
	@isTest
	static void summarySearchConditionInvalid() {
		App.ParameterException paramEx;
		// 対象年必須
		TeamAttendanceResource.SummarySearchCondition paramTargetYearRequired = new TeamAttendanceResource.SummarySearchCondition();
		try {
			paramTargetYearRequired.validate();
		} catch (App.ParameterException e) {
			paramEx = e;
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
		System.assertNotEquals(null, paramEx, 'App.ParaAttSummarmeterExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'targetYear', null}), paramEx.getMessage());

		// 対象月度必須
		TeamAttendanceResource.SummarySearchCondition paramTargetMonthlyRequired = new TeamAttendanceResource.SummarySearchCondition();
		paramTargetMonthlyRequired.targetYear = '2018';
		try {
			paramTargetMonthlyRequired.validate();
		} catch (App.ParameterException e) {
			paramEx = e;
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
		System.assertNotEquals(null, paramEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'targetMonthly', null}), paramEx.getMessage());

		// 部署IDをID変換できない
		TeamAttendanceResource.SummarySearchCondition paramDeptInvalid = new TeamAttendanceResource.SummarySearchCondition();
		paramDeptInvalid.targetYear = '2018';
		paramDeptInvalid.targetMonthly = '10';
		paramDeptInvalid.departmentId = 'aaaaa';
		try {
			paramDeptInvalid.validate();
		} catch (App.ParameterException e) {
			paramEx = e;
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}
		System.assertNotEquals(null, paramEx, 'App.ParameterExceptionが発生しませんでした');
		System.assertEquals(
				ComMessage.msg().Com_Err_InvalidParameter(new List<String>{'departmentId', String.valueOf(paramDeptInvalid.departmentId)}),
				paramEx.getMessage());
	}

	/**
	 * SearchApiのテスト(正常系)
	 * 正常値を入力し該当件数が0件の場合、空のリストが返却されることを確認する
	 */
	@isTest
	static void searchApiTestNoRecord(){
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData(AttWorkSystem.JP_FIX);

		// パラメータ
		TeamAttendanceResource.SummarySearchCondition param = new TeamAttendanceResource.SummarySearchCondition();
		param.targetYear = '2018';
		param.targetMonthly = '9';
		param.departmentId = testData.dept.id;

		// API実行
		TeamAttendanceResource.SearchApi api = new TeamAttendanceResource.SearchApi();
		TeamAttendanceResource.SummarySearchResult response;
		try {
			Test.startTest();
			response = (TeamAttendanceResource.SummarySearchResult) api.execute(param);
			Test.stopTest();
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました'+e.getMessage()+e.getStackTraceString());
		}

		// 検証
		System.assertNotEquals(null, response.records, 'レコードが存在しない時は空のリストを返す必要があります。');
		System.assertEquals(0, response.records.size());
	}

	/**
	 * SearchApiのテスト(正常系)
	 * 勤怠サマリが存在する対象部署・対象月度がある場合、データが取得できることを確認する
	 */
	@isTest
	static void searchApiTestPositive(){
		// テストデータ作成
		ResourceTestData testData = new ResourceTestData(AttWorkSystem.JP_FIX);
		testData.createEmpMonthlySummary(AppDate.newInstance(2018, 12, 1), false);
		AppDate startDate = AppDate.newInstance(2018, 12, 1);
		AppDate endDate = AppDate.newInstance(2018, 12, 1).addMonths(1).addDays(-1);

		// パラメータ
		TeamAttendanceResource.SummarySearchCondition param = new TeamAttendanceResource.SummarySearchCondition();
		param.targetYear = '2018';
		param.targetMonthly = '12';
		param.departmentId = testData.dept.id;

		// API実行
		TeamAttendanceResource.SearchApi api = new TeamAttendanceResource.SearchApi();
		TeamAttendanceResource.SummarySearchResult response;
		try {
			Test.startTest();
			response = (TeamAttendanceResource.SummarySearchResult) api.execute(param);
			Test.stopTest();
		} catch (Exception e) {
			TestUtil.fail('予期せぬエラーが発生しました'+e.getMessage()+e.getStackTraceString());
		}

		// 検証
		System.assertNotEquals(null, response.records, 'レコードが存在しない時は空のリストを返す必要があります。');
		System.assertEquals(1, response.records.size());

		// 各項目の検証
		for (TeamAttendanceResource.Summary actSummary : response.records) {
			System.assertEquals(String.valueOf(testData.employee.id), actSummary.employeeId);
			System.assertEquals(testData.employee.code, actSummary.employeeCode);
			System.assertEquals(testData.employee.fullNameL.getFullName(), actSummary.employeeName);
			System.assertEquals(testData.employee.user.photoUrl, actSummary.photoUrl);
			System.assertEquals(testData.workingType.getHistory(0).nameL.getValue(), actSummary.workingTypeName);
			System.assertEquals(startDate.format(), actSummary.startDate);
			System.assertEquals(endDate.format(), actSummary.endDate);
			System.assertEquals('NotRequested', actSummary.status);
			System.assertEquals(testData.approver.fullNameL.getFullName(), actSummary.approverName);
		}
	}

	/**
	 * @description SummaryPeriodListApiのテスト(正常系)
	 * 集計期間一覧の全てのレスポンスパラメータの値が正しく設定されていることを確認する
	 */
	@isTest static void summaryPeriodListApiTestDefaulParam() {
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		workingType.startDateOfMonth = 1;
		workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType);

		// テストユーザを設定
		testData.employee = testData.createEmployeeWithStandardUser('Test');
		testData.employee.getHistory(0).validFrom = AppDate.newInstance(1900, 1, 1);
		testData.employee.getHistory(0).validTo = AppDate.newInstance(2100, 12, 31);
		testData.employee.getHistory(0).workingTypeId = testData.workingType.id;
		new EmployeeRepository().saveHistoryEntity(testData.employee.getHistory(0));
		User testUser = testData.createRunAsUserFromEmployee(testData.employee);
		testUser.Languagelocalekey = 'en_US';
		System.runAs (testUser) {
			update testUser;
		}

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();
		api.setToday(AppDate.newInstance(2019, 1, 15));

		TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
		TeamAttendanceResource.SummaryPeriodLisResult result;
		System.runAs(testUser) {
			Test.startTest();
			result = (TeamAttendanceResource.SummaryPeriodLisResult)api.execute(param);
			Test.stopTest();
		}

		// 一覧に含まれる期間数は現在月度+過去12ヶ月分の13
		System.assertEquals(13, result.periods.size());
		List<TeamAttendanceResource.SummaryPeriod> periodList = result.periods;

		// nameが取得できていることを確認
		System.assertEquals('201901', periodList[0].name);
		System.assertEquals('201812', periodList[1].name);
		System.assertEquals('201811', periodList[2].name);
		System.assertEquals('201810', periodList[3].name);
		System.assertEquals('201809', periodList[4].name);
		System.assertEquals('201808', periodList[5].name);
		System.assertEquals('201807', periodList[6].name);
		System.assertEquals('201806', periodList[7].name);
		System.assertEquals('201805', periodList[8].name);
		System.assertEquals('201804', periodList[9].name);
		System.assertEquals('201803', periodList[10].name);
		System.assertEquals('201802', periodList[11].name);
		System.assertEquals('201801', periodList[12].name);

		// yearが取得できていることを確認
		System.assertEquals('2019', periodList[0].year);
		System.assertEquals('2018', periodList[1].year);

		// monthが取得できていることを確認
		System.assertEquals('1', periodList[0].monthly); // 0埋めはしない
		System.assertEquals('12', periodList[1].monthly);

		// labelが取得できていることを確認
		System.assertEquals('Jan 2019', periodList[0].label);
		System.assertEquals('Dec 2018', periodList[1].label);

		// currentPeriodNameが取得できていることを確認
		System.assertEquals('201901', result.currentPeriodName);
	}

	/**
	 * @description SummaryPeriodListApiのテスト(正常系)
	 * 対象期間を指定した場合、集計期間が正しく取得できることを確認する
	 */
	@isTest static void summaryPeriodListApiTestTargetPeriod() {
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		workingType.startDateOfMonth = 1;
		workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType);

		// テストユーザを設定
		testData.employee = testData.createEmployeeWithStandardUser('Test');
		User testUser = testData.createRunAsUserFromEmployee(testData.employee);
		testUser.Languagelocalekey = 'en_US';
		System.runAs (testUser) {
			update testUser;
		}

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();
		api.setToday(AppDate.newInstance(2019, 2, 15));

		TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
		param.targetPeriod = '201811';

		TeamAttendanceResource.SummaryPeriodLisResult result;
		System.runAs(testUser) {
			Test.startTest();
			result = (TeamAttendanceResource.SummaryPeriodLisResult)api.execute(param);
			Test.stopTest();
		}

		// 一覧に含まれる期間数は未来2ヶ月+現在月度+過去12ヶ月分の15
		System.assertEquals(15, result.periods.size());
		List<TeamAttendanceResource.SummaryPeriod> periodList = result.periods;

		// nameが取得できていることを確認
		System.assertEquals('201901', periodList[0].name);
		System.assertEquals('201812', periodList[1].name);
		System.assertEquals('201811', periodList[2].name);
		System.assertEquals('201810', periodList[3].name);
		System.assertEquals('201809', periodList[4].name);
		System.assertEquals('201808', periodList[5].name);
		System.assertEquals('201807', periodList[6].name);
		System.assertEquals('201806', periodList[7].name);
		System.assertEquals('201805', periodList[8].name);
		System.assertEquals('201804', periodList[9].name);
		System.assertEquals('201803', periodList[10].name);
		System.assertEquals('201802', periodList[11].name);
		System.assertEquals('201801', periodList[12].name);
		System.assertEquals('201712', periodList[13].name);
		System.assertEquals('201711', periodList[14].name);
	}

	/**
	 * @description SummaryPeriodListApiのテスト
	 * 勤務体系の起算日が1日以外の場合、集計期間が正しく取得できることを確認する
	 */
	@isTest static void summaryPeriodListApiTestWorkingTypeStartDate() {
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		workingType.startDateOfMonth = 16;
		workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType);

		// テストユーザを設定
		testData.employee = testData.createEmployeeWithStandardUser('Test');
		User testUser = testData.createRunAsUserFromEmployee(testData.employee);
		testUser.Languagelocalekey = 'en_US';
		System.runAs (testUser) {
			update testUser;
		}

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();
		api.setToday(AppDate.newInstance(2019, 1, 16));

		// 対象月度は指定しない
		TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();

		TeamAttendanceResource.SummaryPeriodLisResult result;
		System.runAs(testUser) {
			Test.startTest();
			result = (TeamAttendanceResource.SummaryPeriodLisResult)api.execute(param);
			Test.stopTest();
		}

		// 一覧に含まれる期間数は現在月度+過去12ヶ月分の13
		System.assertEquals(13, result.periods.size());
		List<TeamAttendanceResource.SummaryPeriod> periodList = result.periods;

		// nameが取得できていることを確認
		// 勤務体系のMonthMarkがEndBaseであるため、2019年2月度が含まれるはず
		System.assertEquals('201902', periodList[0].name);
		System.assertEquals('201901', periodList[1].name);
		System.assertEquals('201812', periodList[2].name);
		System.assertEquals('201811', periodList[3].name);
		System.assertEquals('201810', periodList[4].name);
		System.assertEquals('201809', periodList[5].name);
		System.assertEquals('201808', periodList[6].name);
		System.assertEquals('201807', periodList[7].name);
		System.assertEquals('201806', periodList[8].name);
		System.assertEquals('201805', periodList[9].name);
		System.assertEquals('201804', periodList[10].name);
		System.assertEquals('201803', periodList[11].name);
		System.assertEquals('201802', periodList[12].name);
	}

	/**
	 * @description SummaryPeriodListApiのテスト(正常系)
	 * 起算日の異なる勤務体系が存在する場合、各勤務体系にもとづいた集計期間リストが正しくマージされていることを確認する
	 */
	@isTest static void summaryPeriodListApiTestMergePeriod() {
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		workingType.startDateOfMonth = 1;
		workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType);

		// 起算日が異なる勤務体系
		AttWorkingTypeBaseEntity workingType2 = testData.createWorkingType('Test2');
		workingType2.startDateOfMonth = 16;
		workingType2.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType2);

		// テストユーザを設定
		testData.employee = testData.createEmployeeWithStandardUser('Test');
		User testUser = testData.createRunAsUserFromEmployee(testData.employee);

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();
		api.setToday(AppDate.newInstance(2019, 1, 16));

		// 対象月度は指定しない
		TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();

		TeamAttendanceResource.SummaryPeriodLisResult result;
		System.runAs(testUser) {
			Test.startTest();
			result = (TeamAttendanceResource.SummaryPeriodLisResult)api.execute(param);
			Test.stopTest();
		}

		// 一覧に含まれる期間数は現在月度(2つ)+過去12ヶ月分の14
		System.assertEquals(14, result.periods.size());
		List<TeamAttendanceResource.SummaryPeriod> periodList = result.periods;

		// nameが取得できていることを確認
		// 勤務体系のMonthMarkがEndBaseであるため、2019年2月度が含まれるはず
		System.assertEquals('201902', periodList[0].name);
		System.assertEquals('201901', periodList[1].name);
		System.assertEquals('201812', periodList[2].name);
		System.assertEquals('201811', periodList[3].name);
		System.assertEquals('201810', periodList[4].name);
		System.assertEquals('201809', periodList[5].name);
		System.assertEquals('201808', periodList[6].name);
		System.assertEquals('201807', periodList[7].name);
		System.assertEquals('201806', periodList[8].name);
		System.assertEquals('201805', periodList[9].name);
		System.assertEquals('201804', periodList[10].name);
		System.assertEquals('201803', periodList[11].name);
		System.assertEquals('201802', periodList[12].name);
		System.assertEquals('201801', periodList[13].name);
	}

	/**
	 * @description SummaryPeriodListApiのテスト(正常系)
	 * 現在月度をより未来の月度が取得できないことを確認する
	 */
	@isTest static void summaryPeriodListApiTestMaxPeriod() {
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		workingType.startDateOfMonth = 1;
		workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType);

		// テストユーザを設定
		testData.employee = testData.createEmployeeWithStandardUser('Test');
		User testUser = testData.createRunAsUserFromEmployee(testData.employee);

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();
		api.setToday(AppDate.newInstance(2019, 1, 15));

		TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
		param.targetPeriod = '201812';

		TeamAttendanceResource.SummaryPeriodLisResult result;
		System.runAs(testUser) {
			Test.startTest();
			result = (TeamAttendanceResource.SummaryPeriodLisResult)api.execute(param);
			Test.stopTest();
		}

		// 一覧に含まれる期間数は未来1月+対象月度+過去12ヶ月分の14
		System.assertEquals(14, result.periods.size());
		List<TeamAttendanceResource.SummaryPeriod> periodList = result.periods;

		// nameが取得できていることを確認
		System.assertEquals('201901', periodList[0].name);
		System.assertEquals('201812', periodList[1].name);
		System.assertEquals('201811', periodList[2].name);
		System.assertEquals('201810', periodList[3].name);
		System.assertEquals('201809', periodList[4].name);
		System.assertEquals('201808', periodList[5].name);
		System.assertEquals('201807', periodList[6].name);
		System.assertEquals('201806', periodList[7].name);
		System.assertEquals('201805', periodList[8].name);
		System.assertEquals('201804', periodList[9].name);
		System.assertEquals('201803', periodList[10].name);
		System.assertEquals('201802', periodList[11].name);
		System.assertEquals('201801', periodList[12].name);
		System.assertEquals('201712', periodList[13].name);
	}

	/**
	 * @description SummaryPeriodListApiのテスト(正常系)
	 * 集計期間一覧にログインユーザ社員の今月度が含まれていない場合、currentPeriodNameが設定されないことを確認する
	 */
	@isTest static void summaryPeriodListApiTestCurrentPeriodNameIsNull() {
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		workingType.startDateOfMonth = 1;
		workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType);

		// テストユーザを設定
		testData.employee = testData.createEmployeeWithStandardUser('Test');
		testData.employee.getHistory(0).validFrom = AppDate.newInstance(1900, 1, 1);
		testData.employee.getHistory(0).validTo = AppDate.newInstance(2100, 12, 31);
		testData.employee.getHistory(0).workingTypeId = testData.workingType.id;
		new EmployeeRepository().saveHistoryEntity(testData.employee.getHistory(0));
		User testUser = testData.createRunAsUserFromEmployee(testData.employee);
		testUser.Languagelocalekey = 'en_US';
		System.runAs (testUser) {
			update testUser;
		}

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();
		api.setToday(AppDate.newInstance(2019, 1, 15));

		TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
		param.targetPeriod = '201810';

		TeamAttendanceResource.SummaryPeriodLisResult result;
		System.runAs(testUser) {
			Test.startTest();
			result = (TeamAttendanceResource.SummaryPeriodLisResult)api.execute(param);
			Test.stopTest();
		}

		// 一覧に含まれる期間数は未来2ヶ月+現在月度+過去12ヶ月分の15
		System.assertEquals(15, result.periods.size());
		List<TeamAttendanceResource.SummaryPeriod> periodList = result.periods;

		// nameが取得できていることを確認
		System.assertEquals('201812', periodList[0].name);
		System.assertEquals('201811', periodList[1].name);
		System.assertEquals('201810', periodList[2].name);
		System.assertEquals('201809', periodList[3].name);
		System.assertEquals('201808', periodList[4].name);
		System.assertEquals('201807', periodList[5].name);
		System.assertEquals('201806', periodList[6].name);
		System.assertEquals('201805', periodList[7].name);
		System.assertEquals('201804', periodList[8].name);
		System.assertEquals('201803', periodList[9].name);
		System.assertEquals('201802', periodList[10].name);
		System.assertEquals('201801', periodList[11].name);
		System.assertEquals('201712', periodList[12].name);
		System.assertEquals('201711', periodList[13].name);
		System.assertEquals('201710', periodList[14].name);

		// ログインユーザの今月度は1月度のため、currentPeriodNameに値は設定されていない
		System.assertEquals(null, result.currentPeriodName);
	}

	/**
	 * @description SummaryPeriodListApiのテスト(正常系)
	 * 社員を指定した場合、
	 * 指定した社員の会社の勤務体系で集計期間一覧が作成されること、指定した社員の月在月度が取得できることを確認する
	 */
	@isTest static void summaryPeriodListApiTestEmpId() {
		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		CompanyEntity testCompany = testData.createCompany('Test');
		AttWorkingTypeBaseEntity workingType = testData.workingType;
		workingType.startDateOfMonth = 1;
		workingType.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType);

		// 起算日が異なる勤務体系
		AttWorkingTypeBaseEntity workingType2 = testData.createWorkingType('Test2');
		workingType2.companyId = testCompany.id;
		workingType2.startDateOfMonth = 16;
		workingType2.monthMark = AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase;
		new AttWorkingTypeRepository().saveBaseEntity(workingType2);

		// テストユーザを設定(起算日が16日の勤務体系を設定)
		EmployeeBaseEntity testEmployee = testData.createEmployeeWithStandardUser('Test');
		testEmployee.companyId = testCompany.id;
		testEmployee.getHistory(0).validFrom = AppDate.newInstance(1900, 1, 1);
		testEmployee.getHistory(0).validTo = AppDate.newInstance(2100, 12, 31);
		testEmployee.getHistory(0).workingTypeId = workingType2.id;
		new EmployeeRepository().saveEntity(testEmployee);
		User testUser = testData.createRunAsUserFromEmployee(testEmployee);

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();
		api.setToday(AppDate.newInstance(2019, 1, 16));

		TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
		param.empId = testEmployee.id;

		TeamAttendanceResource.SummaryPeriodLisResult result;
		System.runAs(testUser) {
			Test.startTest();
			result = (TeamAttendanceResource.SummaryPeriodLisResult)api.execute(param);
			Test.stopTest();
		}

		// 一覧に含まれる期間数は現在月度(1つ)+過去12ヶ月分の13
		System.assertEquals(13, result.periods.size());
		List<TeamAttendanceResource.SummaryPeriod> periodList = result.periods;

		// nameが取得できていることを確認
		// 勤務体系のMonthMarkがEndBaseであるため、2019年2月度が含まれるはず
		System.assertEquals('201902', periodList[0].name);
		System.assertEquals('201901', periodList[1].name);
		System.assertEquals('201812', periodList[2].name);
		System.assertEquals('201811', periodList[3].name);
		System.assertEquals('201810', periodList[4].name);
		System.assertEquals('201809', periodList[5].name);
		System.assertEquals('201808', periodList[6].name);
		System.assertEquals('201807', periodList[7].name);
		System.assertEquals('201806', periodList[8].name);
		System.assertEquals('201805', periodList[9].name);
		System.assertEquals('201804', periodList[10].name);
		System.assertEquals('201803', periodList[11].name);
		System.assertEquals('201802', periodList[12].name);

		// currentPeriodNameが取得できていることを確認
		System.assertEquals('201902', result.currentPeriodName);
	}

	/**
	 * @description SummaryPeriodListApiのテスト(異常系)
	 * 不正な社員IDを設定した場合、
	 * 想定される例外が発生することを確認する
	 */
	@isTest static void summaryPeriodListApiTestInvaliParamEmpId() {

		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity loginEmp = testData.employee;

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();

		// TEST1 ID以外の文字列を設定した場合
		App.ParameterException paramEx;
		try {
			TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
			param.empId = 'test';
			api.execute(param);
		} catch (App.ParameterException e) {
			paramEx = e;
		}
		System.assertNotEquals(null, paramEx, '例外が発生しませんでした');

		// TEST2 存在しない社員IDを設定
		App.RecordNotFoundException recordEx;
		try {
			TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
			param.empId = UserInfo.getUserId();
			api.execute(param);
		} catch (App.RecordNotFoundException e) {
			recordEx = e;
		}
		System.assertNotEquals(null, recordEx, '例外が発生しませんでした');
	}

	/**
	 * @description SummaryPeriodListApiのテスト(異常系)
	 * 不正な対象期間を設定した場合、
	 * 想定される例外が発生することを確認する
	 */
	@isTest static void summaryPeriodListApiTestInvaliParamTargetPeriod() {

		// テストデータ作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity loginEmp = testData.employee;

		TeamAttendanceResource.SummaryPeriodListApi api = new TeamAttendanceResource.SummaryPeriodListApi();

		// TEST1 年の書式が正しくない場合
		App.ParameterException paramEx;
		try {
			TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
			param.targetPeriod = 'a20101';
			api.execute(param);
		} catch (App.ParameterException e) {
			paramEx = e;
		}
		System.assertNotEquals(null, paramEx, '例外が発生しませんでした');

		// TEST2 月度の書式が正しくない場合
		paramEx = null;
		try {
			TeamAttendanceResource.SummaryPeriodListRequest param = new TeamAttendanceResource.SummaryPeriodListRequest();
			param.targetPeriod = '2010/01';
			api.execute(param);
		} catch (App.ParameterException e) {
			paramEx = e;
		}
		System.assertNotEquals(null, paramEx, '例外が発生しませんでした');
	}

}