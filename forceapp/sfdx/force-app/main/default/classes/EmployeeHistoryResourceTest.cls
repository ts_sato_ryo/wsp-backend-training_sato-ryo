/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description EmployeeHistoryResourceのテストクラス
 */
@isTest
private class EmployeeHistoryResourceTest {

	/**
	 * 社員履歴レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		AttWorkingTypeBase__c workingType = ComTestDataUtility.createAttWorkingTypeWithHistory('テスト勤務体系', company.Id);
		TimeSettingBase__c timeSetting = ComTestDataUtility.createTimeSettingWithHistory('テスト工数設定', company.Id);
		AttAgreementAlertSetting__c agreementAlertSetting = ComTestDataUtility.createAgreementAlertSetting('Testアラート設定', company.Id);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('テストカレンダー', company.Id);
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('部署ベース', company.Id);
		ComCostCenterBase__c costCenter = ComTestDataUtility.createCostCentersWithHistory('Test CC', company.Id, 1, 1)[0];
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		List<ComEmpBase__c> baseList = ComTestDataUtility.createEmployeesWithHistory('Test社員', company.Id, dept.Id, Userinfo.getUserId(), permission.Id, 3);
		ComEmpBase__c base = baseList[0];
		ComEmpBase__c manager = baseList[1];
		ComEmpBase__c approver01 = baseList[2];
		ExpEmployeeGroup__c expEmployeeGroup = ComTestDataUtility.createExpEmployeeGroup('ExpEmpGp', company.Id);

		// 履歴の最新日を今日にする
		List<ComEmpHistory__c> testHistoryList = [SELECT Id FROM ComEmpHistory__c WHERE BaseId__c =:base.Id];
		final Date today = System.today();
		for(Integer i = 0, n = testHistoryList.size(); i < n; i++){
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		// 定期情報
		ExpTransitJorudanService.CommuterRouteDetail commuterRoute = createCommuterRouteData();

		Test.startTest();
		final Date oneMonthAfterToday = today.addMonths(1);

		EmployeeHistoryResource.EmployeeHistory param = new EmployeeHistoryResource.EmployeeHistory();
		param.baseId = base.Id;
		param.title_L0 = '役職_L0';
		param.title_L1 = '役職_L1';
		param.title_L2 = '役職_L2';
		param.managerId = manager.Id;
		param.costCenterId = costCenter.Id;
		param.comment = 'Testコメント';
		param.validDateFrom = Date.newInstance(oneMonthAfterToday.year(), oneMonthAfterToday.month(), (Integer)workingType.StartDateOfMonth__c);
		param.validDateTo = param.validDateFrom + 31;
		param.workingTypeId = workingType.Id;
		param.timeSettingId = timeSetting.Id;
		param.agreementAlertSettingId = agreementAlertSetting.Id;
		param.calendarId = calendar.Id;
		param.permissionId = permission.Id;
		param.approver01Id = approver01.Id;
		param.approvalAuthority01 = true;
		param.commuterPassAvailable = true;
		param.jorudanRoute = commuterRoute;
		param.expEmployeeGroupId = expEmployeeGroup.Id;

		EmployeeHistoryResource.CreateApi api = new EmployeeHistoryResource.CreateApi();
		EmployeeHistoryResource.SaveResult res = (EmployeeHistoryResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 履歴レコードが1件作成されること
		List<ComEmpBase__c> newBaseRecords = [SELECT
				Id,
				Code__c,
				UniqKey__c,
				ValidTo__c,
				(SELECT
						Id,
						UniqKey__c,
						BaseId__c,
						Title_L0__c,
						Title_L1__c,
						Title_L2__c,
						CostCenterBaseId__c,
						DepartmentBaseId__c,
						ManagerBaseId__c,
						HistoryComment__c,
						ValidFrom__c,
						ValidTo__c,
						WorkingTypeBaseId__c,
						TimeSettingBaseId__c,
						AgreementAlertSettingId__c,
						CalendarId__c,
						PermissionId__c,
						ApproverBase01Id__c,
						ApprovalAuthority01__c,
						CommuterPassAvailable__c,
						JorudanRoute__c,
						ExpEmployeeGroupId__c
					FROM Histories__r
					WHERE Id =: res.Id
				)
			FROM ComEmpBase__c
			WHERE Id NOT IN (:manager.Id, :approver01.Id)];

		System.assertEquals(1, newBaseRecords.size());
		System.assertEquals(1, newBaseRecords[0].Histories__r.size());

		// 値の検証
		ComEmpBase__c newBase = newBaseRecords[0];
		ComEmpHistory__c newHistory = newBase.Histories__r[0];

		// レスポンス値の検証
		System.assertEquals(newHistory.Id, res.Id);

		// ベースレコード値の検証
		System.assertEquals(param.validDateTo, newBase.ValidTo__c);

		// 履歴レコード値の検証
		System.assertEquals(newBase.Id, newHistory.BaseId__c);
		String uniqKey = newBase.UniqKey__c +
			'-' + DateTime.newInstance(param.validDateFrom, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd') +
			'-' + DateTime.newInstance(param.validDateTo, Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd');
		System.assertEquals(uniqKey, newHistory.UniqKey__c);
		System.assertEquals(param.baseId, newHistory.BaseId__c);
		System.assertEquals(param.costCenterId, newHistory.CostCenterBaseId__c);
		System.assertEquals(param.departmentId, newHistory.DepartmentBaseId__c);
		System.assertEquals(param.title_L0, newHistory.Title_L0__c);
		System.assertEquals(param.title_L1, newHistory.Title_L1__c);
		System.assertEquals(param.title_L2, newHistory.Title_L2__c);
		System.assertEquals(param.managerId, newHistory.ManagerBaseId__c);
		System.assertEquals(param.comment, newHistory.HistoryComment__c);
		System.assertEquals(param.validDateFrom, newHistory.ValidFrom__c);
		System.assertEquals(param.validDateTo, newHistory.ValidTo__c);
		System.assertEquals(param.workingTypeId, newHistory.WorkingTypeBaseId__c);
		System.assertEquals(param.timeSettingId, newHistory.TimeSettingBaseId__c);
		System.assertEquals(param.agreementAlertSettingId, newHistory.AgreementAlertSettingId__c);
		System.assertEquals(param.calendarId, newHistory.CalendarId__c);
		System.assertEquals(param.permissionId, newHistory.PermissionId__c);
		System.assertEquals(param.approver01Id, newHistory.ApproverBase01Id__c);
		System.assertEquals(param.approvalAuthority01, newHistory.ApprovalAuthority01__c);
		System.assertEquals(param.commuterPassAvailable, newHistory.CommuterPassAvailable__c);
		System.assertEquals(JSON.serialize(param.jorudanRoute), newHistory.JorudanRoute__c);
		System.assertEquals(param.expEmployeeGroupId, newHistory.ExpEmployeeGroupId__c);
	}

	/**
	 * 社員レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 社員管理権限を無効に設定する
		testData.permission.isManageEmployee = false;
		new PermissionRepository().saveEntity(testData.permission);

		EmployeeHistoryResource.EmployeeHistory param = new EmployeeHistoryResource.EmployeeHistory();
		param.baseId = stdEmployee.Id;
		param.comment = 'Testコメント';
		param.validDateFrom = stdEmployee.getHistory(0).validTo.getDate();
		param.validDateTo = param.validDateFrom + 31;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				EmployeeHistoryResource.CreateApi api = new EmployeeHistoryResource.CreateApi();
				EmployeeHistoryResource.SaveResult res = (EmployeeHistoryResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 社員履歴レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeRequiredErrorTest() {
		Test.startTest();

		EmployeeHistoryResource.EmployeeHistory param = new EmployeeHistoryResource.EmployeeHistory();
		EmployeeHistoryResource.CreateApi api = new EmployeeHistoryResource.CreateApi();
		App.ParameterException ex;
		// DmlException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
		// } catch (DmlException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		// System.assertEquals('REQUIRED_FIELD_MISSING', ex.getDmlStatusCode(0));
	}

	/**
	 * 社員履歴レコードを1件作成する(異常系:勤怠利用時の必須パラメータが未設定)
	 */
	@isTest
	static void creativeRequiredErrorUsingAttendanceTest() {
		// 勤怠を利用するテストデータを作成
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		testData.company.useAttendance = true;
		new CompanyRepository().saveEntity(testData.company);

		Test.startTest();
		EmployeeHistoryResource.EmployeeHistory param = new EmployeeHistoryResource.EmployeeHistory();
		param.baseId = testData.employee.id;
		EmployeeHistoryResource.CreateApi api = new EmployeeHistoryResource.CreateApi();
		App.ParameterException ex;
		try {
			api.execute(param);
			System.assert(false);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();

		// 勤務体系が未設定の場合、エラーになることを検証する
		String expectedMessage = ComMessage.msg().Com_Err_NotSetValue(new List<String>{ComMessage.msg().Att_Lbl_WorkingType});
		System.assertEquals(expectedMessage, ex.getMessage());
	}

	/** Test Expense Employee Group Id required when Company use Expense */
	@isTest
	static void createNegativeTestNoExpEmployeeGroupIdTest() {
		ComCompany__c company = ComTestDataUtility.createCompany('TestCompany', null, null, true);
		AttWorkingTypeBase__c workingType = ComTestDataUtility.createAttWorkingTypeWithHistory('テスト勤務体系', company.Id);
		TimeSettingBase__c timeSetting = ComTestDataUtility.createTimeSettingWithHistory('テスト工数設定', company.Id);
		AttAgreementAlertSetting__c agreementAlertSetting = ComTestDataUtility.createAgreementAlertSetting('Testアラート設定', company.Id);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('テストカレンダー', company.Id);
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('部署ベース', company.Id);
		ComCostCenterBase__c costCenter = ComTestDataUtility.createCostCentersWithHistory('Test CC', company.Id, 1, 1)[0];
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		List<ComEmpBase__c> baseList = ComTestDataUtility.createEmployeesWithHistory('Test社員', company.Id, dept.Id, Userinfo.getUserId(), permission.Id, 3);
		ComEmpBase__c base = baseList[0];
		ComEmpBase__c manager = baseList[1];
		ComEmpBase__c approver01 = baseList[2];
		ExpEmployeeGroup__c expEmployeeGroup = ComTestDataUtility.createExpEmployeeGroup('ExpEmpGp', company.Id);

		// 履歴の最新日を今日にする
		List<ComEmpHistory__c> testHistoryList = [SELECT Id FROM ComEmpHistory__c WHERE BaseId__c =:base.Id];
		final Date today = System.today();
		for(Integer i = 0, n = testHistoryList.size(); i < n; i++){
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i;
		}
		update testHistoryList;

		// 定期情報
		ExpTransitJorudanService.CommuterRouteDetail commuterRoute = createCommuterRouteData();

		Test.startTest();
		final Date oneMonthAfterToday = today.addMonths(1);

		EmployeeHistoryResource.EmployeeHistory param = new EmployeeHistoryResource.EmployeeHistory();
		param.baseId = base.Id;
		param.title_L0 = '役職_L0';
		param.title_L1 = '役職_L1';
		param.title_L2 = '役職_L2';
		param.managerId = manager.Id;
		param.costCenterId = costCenter.Id;
		param.comment = 'Testコメント';
		param.validDateFrom = Date.newInstance(oneMonthAfterToday.year(), oneMonthAfterToday.month(), (Integer)workingType.StartDateOfMonth__c);
		param.validDateTo = param.validDateFrom + 31;
		param.workingTypeId = workingType.Id;
		param.timeSettingId = timeSetting.Id;
		param.agreementAlertSettingId = agreementAlertSetting.Id;
		param.calendarId = calendar.Id;
		param.permissionId = permission.Id;
		param.approver01Id = approver01.Id;
		param.approvalAuthority01 = true;
		param.commuterPassAvailable = true;
		param.jorudanRoute = commuterRoute;

		EmployeeHistoryResource.CreateApi api = new EmployeeHistoryResource.CreateApi();
		try {
			EmployeeHistoryResource.SaveResult res = (EmployeeHistoryResource.SaveResult)api.execute(param);
			TestUtil.fail('No exception is thrown for empty expEmployeeGroupId');
		} catch (Exception e) {
			System.assertEquals(ComMessage.msg().Com_Err_NullValue(new List<String>{'expEmployeeGroupId'}), e.getMessage());
		}

	}

	/**
	 * 部署履歴レコードを1件作成する(異常系:有効開始日が不正)
	 */
	@isTest
	static void creativeInvalidValidDateErrorTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('部署ベース', company.Id);
		ComEmpBase__c base = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, dept.Id, UserInfo.getUserId(), permission.Id);

		// 履歴の最新日を今日にする
		List<ComEmpHistory__c> testHistoryList = [SELECT Id FROM ComEmpHistory__c WHERE BaseId__c =:base.Id];
		Date today = System.today();
		for(Integer i = 0, n = testHistoryList.size(); i < n; i++){
			Date startDate = today - (i * 365);
			testHistoryList[i].ValidFrom__c = startDate;
			testHistoryList[i].ValidTo__c = startDate + i + 1;// validTo__cは有効期間終了日ではなく有効期間を含まない失効日なので開始日と同日にならない
		}
		update testHistoryList;

		Test.startTest();

		EmployeeHistoryResource.EmployeeHistory param = new EmployeeHistoryResource.EmployeeHistory();
		param.baseId = base.Id;
		param.validDateFrom = today.addDays(-1);  // "最新日付"以降ではないのでエラー

		EmployeeHistoryResource.CreateApi api = new EmployeeHistoryResource.CreateApi();

		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(ComMessage.msg().Admin_Err_InvalidRevisionDate, ex.getMessage());

	}

	// 履歴更新APIは一旦廃止になったため、コメントアウト
	// /**
	//  * 部署レコードを1件更新する(正常系)
	//  */
	// @isTest
	// static void updatePositiveTest() {
   //
	// 	ComCompany__c company = ComTestDataUtility.createTestCompany();
	// 	ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('部署ベース', company.Id);
	// 	ComEmpBase__c base = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, dept.Id, UserInfo.getUserId());
	// 	ComEmpHistory__c history = [SELECT Id FROM ComEmpHistory__c LIMIT 1];
   //
	// 	Test.startTest();
   //
	// 	// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
	// 	// 全パラメータの検証は createPositiveTest() で行っている
	// 	Map<String, Object> param = new Map<String, Object> {
	// 		'id' => history.id,
	// 		'comment' => 'テストコメント_update'
	// 	};
   //
	// 	EmployeeHistoryResource.UpdateApi api = new EmployeeHistoryResource.UpdateApi();
	// 	Object res = api.execute(param);
   //
	// 	Test.stopTest();
   //
	// 	System.assertEquals(null, res);
   //
	// 	// 社員履歴レコードが更新されていること
	// 	List<ComEmpHistory__c> historyList = [SELECT
	// 			HistoryComment__c
	// 		FROM ComEmpHistory__c
	// 		WHERE Id =:(String)param.get('id')];
   //
	// 	System.assertEquals(1, historyList.size());
	// 	System.assertEquals((String)param.get('comment'), historyList[0].HistoryComment__c);
	// }
   //
	// /**
	//  * 部署レコードを1件更新する(異常系:必須パラメータが未設定)
	//  */
	// @isTest
	// static void updateNegativeTest() {
	// 	Test.startTest();
   //
	// 	EmployeeHistoryResource.EmployeeHistory param = new EmployeeHistoryResource.EmployeeHistory();
   //
	// 	App.ParameterException ex;
	// 	try {
	// 		Object res = (new EmployeeHistoryResource.UpdateApi()).execute(param);
	// 	} catch (App.ParameterException e) {
	// 		ex = e;
	// 	}
   //
	// 	Test.stopTest();
   //
	// 	System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	// }

	/**
	 * 部署履歴レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);
		ComEmpBase__c base = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, dept.Id, UserInfo.getUserId(), permission.Id);
		ComEmpHistory__c delRecord = [SELECT UniqKey__c, BaseId__c, ValidFrom__c, ValidTo__c, PermissionId__c FROM ComEmpHistory__c LIMIT 1];
		// ベースに対して履歴が2件以上存在する場合のみ削除が可能なため、レコードを追加する
		ComEmpHistory__c history2 = delRecord.clone();
		history2.UniqKey__c += '_2';
		insert history2;

		Test.startTest();

		EmployeeHistoryResource.DeleteOption param = new EmployeeHistoryResource.DeleteOption();
		param.id = delRecord.id;

		// １回目では削除が成功し、２回目ではスキップすること
		(new EmployeeHistoryResource.DeleteApi()).execute(param);
		(new EmployeeHistoryResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 社員レコードが論理削除されること
		List<ComEmpHistory__c> recordList = [SELECT Id, Removed__c FROM ComEmpHistory__c WHERE Id =:delRecord.Id];
		System.assertEquals(1, recordList.size());
		System.assertEquals(true, recordList[0].Removed__c);
	}

	/**
	 * 社員レコードを1件作成する(異常系:API実行権限を持っていない)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 社員管理権限を無効に設定する
		testData.permission.isManageEmployee = false;
		new PermissionRepository().saveEntity(testData.permission);

		EmployeeHistoryResource.DeleteOption param = new EmployeeHistoryResource.DeleteOption();
		param.id = stdEmployee.getHistory(0).id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				(new EmployeeHistoryResource.DeleteApi()).execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 社員履歴レコードを1件削除する(異常系:ベースに対して既存履歴が1件のみ)
	 */
	@isTest
	static void deleteNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test部署', company.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComEmpBase__c base = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, dept.Id, UserInfo.getUserId(), permission.Id);
		ComEmpHistory__c delRecord = [SELECT UniqKey__c, BaseId__c FROM ComEmpHistory__c LIMIT 1];

		Test.startTest();

		EmployeeHistoryResource.DeleteOption param = new EmployeeHistoryResource.DeleteOption();
		param.id = delRecord.id;

		// 削除エラーが発生し、履歴削除されないこと
		App.IllegalStateException ex;

		try {
			(new EmployeeHistoryResource.DeleteApi()).execute(param);
		} catch (App.IllegalStateException e) {
			ex = e;
		}

		Test.stopTest();

		System.assert(ex != null);
		System.assertEquals(1, [SELECT COUNT() FROM ComEmpHistory__c WHERE Id =:delRecord.Id]);
	}

	/**
	 * 部署履歴レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		EmployeeHistoryResource.DeleteOption param = new EmployeeHistoryResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new EmployeeHistoryResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 社員履歴レコードを検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		List<ComEmpBase__c> bases = ComTestDataUtility.createEmployeesWithHistory('Test社員', company.Id, dept.Id, Userinfo.getUserId(), permission.Id, 3);
		ComEmpHistory__c history = [SELECT Id FROM ComEmpHistory__c WHERE BaseId__c =:bases[0].Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		EmployeeHistoryResource.SearchApi api = new EmployeeHistoryResource.SearchApi();
		EmployeeHistoryResource.SearchResult res = (EmployeeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);

		// 項目値の検証は searchAllPositiveTest() で行なうため省略
	}

	/**
	 * 社員履歴レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {
		// 組織の言語L1をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComEmpBase__c base = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, dept.Id, Userinfo.getUserId(), permission.Id);
		ComEmpHistory__c history = [SELECT Id FROM ComEmpHistory__c WHERE BaseId__c = :base.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		EmployeeHistoryResource.SearchApi api = new EmployeeHistoryResource.SearchApi();
		EmployeeHistoryResource.SearchResult res = (EmployeeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [
				SELECT Title_L1__c, ManagerBaseId__c,
					ManagerBaseId__r.FirstName_L0__c, ManagerBaseId__r.LastName_L0__c,
					ManagerBaseId__r.FirstName_L1__c, ManagerBaseId__r.LastName_L1__c,
					ManagerBaseId__r.FirstName_L2__c, ManagerBaseId__r.LastName_L2__c
				FROM ComEmpHistory__c WHERE BaseId__c =:base.Id LIMIT 1];
		// history = [SELECT Title_L1__c, ManagerBaseId__r.FullName_L1__c FROM ComEmpHistory__c WHERE BaseId__c =:base.Id LIMIT 1];
		System.assertEquals(history.Title_L1__c, res.records[0].title);
		if (history.ManagerBaseId__c != null) {
			String managerName = new AppPersonName(
					history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
					history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
					history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(managerName, res.records[0].manager.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].manager.name));
		}
		// System.assertEquals(history.ManagerBaseId__r.FullName_L1__c, res.records[0].manager.name);
	}

	/**
	 * 社員履歴レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {
		// 組織の言語L2をユーザ言語に設定する
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComEmpBase__c base = ComTestDataUtility.createEmployeeWithHistory('Test社員', company.Id, dept.Id, Userinfo.getUserId(), permission.Id);
		ComEmpHistory__c history = [SELECT Id FROM ComEmpHistory__c WHERE BaseId__c = :base.Id LIMIT 1];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object> {
			'id' => history.Id
		};

		EmployeeHistoryResource.SearchApi api = new EmployeeHistoryResource.SearchApi();
		EmployeeHistoryResource.SearchResult res = (EmployeeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 部署レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals((String)paramMap.get('id'), res.records[0].id);
		// 多言語対応項目の検証
		history = [
				SELECT Title_L2__c, ManagerBaseId__c,
					ManagerBaseId__r.FirstName_L0__c, ManagerBaseId__r.LastName_L0__c,
					ManagerBaseId__r.FirstName_L1__c, ManagerBaseId__r.LastName_L1__c,
					ManagerBaseId__r.FirstName_L2__c, ManagerBaseId__r.LastName_L2__c
				FROM ComEmpHistory__c WHERE BaseId__c =:base.Id LIMIT 1];
		System.assertEquals(history.Title_L2__c, res.records[0].title);
		if (history.ManagerBaseId__c != null) {
			String managerName = new AppPersonName(
					history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
					history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
					history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c).getFullName();
			System.assertEquals(managerName, res.records[0].manager.name);
		} else {
			System.assertEquals(true, String.isBlank(res.records[0].manager.name));
		}
	}

	/**
	 * 社員履歴レコードを検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c costCenter = ComTestDataUtility.createCostCentersWithHistory('Test CC', company.Id, 1, 1)[0];
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('テスト部署', company.Id);
		ComPermission__c permission = ComTestDataUtility.createPermission('Test Permission', company.Id);
		ComCostCenterHistory__c costCenterHistory = [SELECT Id, Name_L0__c FROM ComCostCenterHistory__c WHERE BaseId__c =:costCenter.Id LIMIT 1];
		ComDeptHistory__c deptHistory = [SELECT Id, Name_L0__c FROM ComDeptHistory__c WHERE BaseId__c =:dept.Id LIMIT 1];
		List<ComEmpBase__c> bases = ComTestDataUtility.createEmployeesWithHistory('Test社員', company.Id, dept.Id, costCenter.Id, Userinfo.getUserId(), permission.Id, 4);
		ComEmpHistory__c testHistory = [SELECT Id FROM ComEmpHistory__c WHERE BaseId__c =:bases[0].Id LIMIT 1];
		AttWorkingTypeBase__c workingType = ComTestDataUtility.createAttWorkingTypeWithHistory('テスト勤務体系', company.Id);
		AttWorkingTypeHistory__c workingTypeHistory = [SELECT Id, Name_L0__c FROM AttWorkingTypeHistory__c WHERE BaseId__c =:workingType.Id LIMIT 1];
		TimeSettingBase__c timeSetting = ComTestDataUtility.createTimeSettingWithHistory('テスト工数設定', company.Id);
		TimeSettingHistory__c timeSettingHistory = [SELECT Id, Name_L0__c FROM TimeSettingHistory__c WHERE BaseId__c =:timeSetting.Id LIMIT 1];
		AttAgreementAlertSetting__c agreementAlertSetting = ComTestDataUtility.createAgreementAlertSetting('Testアラート設定', company.Id);
		ComCalendar__c calendar = ComTestDataUtility.createCalendar('テストカレンダー', company.Id);
		ExpEmployeeGroup__c expEmployeeGroup = ComTestDataUtility.createExpEmployeeGroup('ExpEmpGp', company.Id);

		testHistory.ManagerBaseId__c = bases[1].Id;
		testHistory.HistoryComment__c = 'テストコメント';
		testHistory.WorkingTypeBaseId__c = workingType.Id;
		testHistory.TimeSettingBaseId__c = timeSetting.Id;
		testHistory.ApproverBase01Id__c = bases[2].Id;
		testHistory.ExpEmployeeGroupId__c = expEmployeeGroup.id;

		// 定期情報
		ExpTransitJorudanService.CommuterRouteDetail commuterRoute = createCommuterRouteData();
		testHistory.CommuterPassAvailable__c = true;
		testHistory.JorudanRoute__c = JSON.serialize(commuterRoute);
		update testHistory;

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>();
		EmployeeHistoryResource.SearchApi api = new EmployeeHistoryResource.SearchApi();
		EmployeeHistoryResource.SearchResult res = (EmployeeHistoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		Map<Id, ComEmpHistory__c> historyMap = new Map<Id, ComEmpHistory__c>([
			SELECT
				Id,
				BaseId__c,
				Title_L0__c,
				Title_L1__c,
				Title_L2__c,
				CostCenterBaseId__c,
				CostCenterBaseId__r.Name,
				DepartmentBaseId__c,
				DepartmentBaseId__r.Name,
				ExpEmployeeGroupId__c,
				ExpEmployeeGroupId__r.Name_L0__c,
				ExpEmployeeGroupId__r.Name_L1__c,
				ExpEmployeeGroupId__r.Name_L2__c,
				ManagerBaseId__c,
				ManagerBaseId__r.FirstName_L0__c,
				ManagerBaseId__r.FirstName_L1__c,
				ManagerBaseId__r.FirstName_L2__c,
				ManagerBaseId__r.LastName_L0__c,
				ManagerBaseId__r.LastName_L1__c,
				ManagerBaseId__r.LastName_L2__c,
				ValidFrom__c,
				ValidTo__c,
				HistoryComment__c,
				WorkingTypeBaseId__c,
				WorkingTypeBaseId__r.Name,
				TimeSettingBaseId__c,
				TimeSettingBaseId__r.Name,
				AgreementAlertSettingId__c,
				CalendarId__c,
				PermissionId__c,
				ApproverBase01Id__c,
				ApprovalAuthority01__c,
				CommuterPassAvailable__c,
				JorudanRoute__c
			FROM ComEmpHistory__c]);
		// 部署レコードが取得できること
		System.assertEquals(historyMap.size(), res.records.size());

		for (EmployeeHistoryResource.EmployeeHistory dto : res.records) {
			System.assertEquals(true, historyMap.containsKey(dto.id));
			ComEmpHistory__c history = historyMap.get(dto.id);

			// 上長の名前
			AppPersonName managerNameL = new AppPersonName(
					history.ManagerBaseId__r.FirstName_L0__c, history.ManagerBaseId__r.LastName_L0__c,
					history.ManagerBaseId__r.FirstName_L1__c, history.ManagerBaseId__r.LastName_L1__c,
					history.ManagerBaseId__r.FirstName_L2__c, history.ManagerBaseId__r.LastName_L2__c);

			System.assertEquals(history.BaseId__c, dto.baseId);
			System.assertEquals(history.Title_L0__c, dto.title); // デフォルトはL0を返す
			System.assertEquals(history.Title_L0__c, dto.title_L0);
			System.assertEquals(history.Title_L1__c, dto.title_L1);
			System.assertEquals(history.Title_L2__c, dto.title_L2);
			System.assertEquals(history.CostCenterBaseId__c, dto.costCenterId);
			System.assertEquals(costCenterHistory.Name_L0__c, dto.costCenter.name); // Returns L0 by default
			System.assertEquals(history.DepartmentBaseId__c, dto.departmentId);
			System.assertEquals(deptHistory.Name_L0__c, dto.department.name);  // デフォルトはL0を返す
			System.assertEquals(history.CalendarId__c, dto.calendarId);
			System.assertEquals(history.PermissionId__c, dto.permissionId);
			System.assertEquals(history.ApproverBase01Id__c, dto.approver01Id);
			System.assertEquals(history.ApprovalAuthority01__c, dto.approvalAuthority01);
			System.assertEquals(history.ManagerBaseId__c, dto.managerId);
			if (dto.managerId != null) {
				System.assertEquals(managerNameL.getFullName(), dto.manager.name);
			}
			System.assertEquals(history.ValidFrom__c, dto.validDateFrom);
			System.assertEquals(history.ValidTo__c, dto.validDateTo);
			System.assertEquals(history.HistoryComment__c, dto.comment);
			System.assertEquals(history.WorkingTypeBaseId__c, dto.workingTypeId);
			if (dto.workingTypeId != null) {
				System.assertEquals(workingtypeHistory.Name_L0__c, dto.workingType.name);  // デフォルトはL0を返す
			}
			System.assertEquals(history.TimeSettingBaseId__c, dto.timeSettingId);
			if (dto.timeSettingId != null) {
				System.assertEquals(timeSettingHistory.Name_L0__c, dto.timeSetting.name);  // デフォルトはL0を返す
			}
			System.assertEquals(history.AgreementAlertSettingId__c, dto.agreementAlertSettingId);
			if (dto.agreementAlertSettingId != null) {
				System.assertEquals(agreementAlertSetting.Name_L0__c, dto.agreementAlertSetting.name);  // デフォルトはL0を返す
			}
			// 定期情報
			System.assertEquals(history.CommuterPassAvailable__c, dto.commuterPassAvailable);
			if (dto.jorudanRoute != null) {
				System.assertEquals(history.JorudanRoute__c, JSON.serialize(dto.jorudanRoute));
			}
			System.assertEquals(history.ExpEmployeeGroupId__c, dto.expEmployeeGroupId);
			if(dto.expEmployeeGroup != null) {
				String currLangName = new AppMultiString(
					history.ExpEmployeeGroupId__r.Name_L0__c,
					history.ExpEmployeeGroupId__r.Name_L1__c,
					history.ExpEmployeeGroupId__r.Name_L2__c).getValue();
				System.assertEquals(history.ExpEmployeeGroupId__r.Name_L0__c, dto.expEmployeeGroup.name);
			} else {
				TestUtil.fail('expEmployeeGroup has no value so it fail.');
			}
		}

	}

	/**
	 * 定期情報のテストデータを作成する
	 */
	static private ExpTransitJorudanService.CommuterRouteDetail createCommuterRouteData() {
		ExpTransitJorudanService.CommuterRouteDetail commuterRoute = new ExpTransitJorudanService.CommuterRouteDetail();
		commuterRoute.fare1 = 10000;
		commuterRoute.fare3 = 30000;
		commuterRoute.fare6 = 60000;

		ExpTransitJorudanService.CommuterRoutePath path = new ExpTransitJorudanService.CommuterRoutePath();
		path.fromName = '京橋';
		path.toName = '日本橋';
		path.lineName = '銀座線';
		path.lineType = '2';
		commuterRoute.pathList = new List<ExpTransitJorudanService.CommuterRoutePath>{path};

		return commuterRoute;
	}

}