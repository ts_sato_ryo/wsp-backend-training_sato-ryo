/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分履歴切り替えバッチのテストクラス Test class for ExpTaxTypeCurrentHistoryUpdateBatch
 */
@isTest
private class ExpTaxTypeCurrentHistoryUpdateBatchTest {

	/**
	 * Apexスケジュール正常系テスト Positive test for Executing batch
	 */
	@isTest
	static void schedulePositiveTest() {
		Test.StartTest();

		ExpTaxTypeCurrentHistoryUpdateBatch sch = new ExpTaxTypeCurrentHistoryUpdateBatch();
		Id schId = System.schedule('税区分履歴切り替えスケジュール', '0 0 0 * * ?', sch);

		Test.StopTest();

		System.assertNotEquals(null, schId);
	}

	/**
	 * バッチ正常系テスト Positive test for batch process
	 */
	@isTest
	static void batchPositiveTest() {
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Company', countryObj.Id);
		final Integer baseNum = 2;
		final Integer historyNum = 2;
		List<ExpTaxTypeBase__c> baseObjs = ComTestDataUtility.createTaxTypesWithHistory('Test', null, companyObj.Id, baseNum, historyNum);
		ExpTaxTypeBase__c base1 = baseObjs[0];
		List<ExpTaxTypeHistory__c> historyList1 =
				[SELECT Id, BaseId__c, Name_L0__c FROM ExpTaxTypeHistory__c WHERE BaseId__c = :base1.Id ORDER BY ValidFrom__c Asc];

		// CurrentHistoryId__c を空欄にしておく Set null to CurrentHistoryId__c
		base1.Name = 'AAA';
		base1.CurrentHistoryId__c = null;
		update base1;

		Test.StartTest();

		ExpTaxTypeCurrentHistoryUpdateBatch b = new ExpTaxTypeCurrentHistoryUpdateBatch();
		Id jobId = Database.executeBatch(b);

		Test.StopTest();

		System.assertNotEquals(null, jobId);

		ExpTaxTypeBase__c resBase = [SELECT Id, Name, CurrentHistoryId__c FROM ExpTaxTypeBase__c WHERE Id = :base1.Id];
		System.assertEquals(historyList1[0].Id, resBase.CurrentHistoryId__c);
		System.assertEquals(historyList1[0].Name_L0__c, resBase.Name);
	}
}