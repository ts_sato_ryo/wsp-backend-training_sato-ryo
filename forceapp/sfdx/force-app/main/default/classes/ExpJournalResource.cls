/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Resource class for Exp Journal
 * 
 * @group Expense
 */
public class ExpJournalResource {

	/*
	 * Request Param for updating the Extract Date of the Journal
	 */
	public class JournalExtractDateParam implements RemoteApi.RequestParam {
		public Set<String> journalNos;
		public String extractDate;

		public void validate() {
			ExpCommonUtil.validateDate('extractDate', extractDate);
		}
	}

	/*
	 * Api Response for updating the Extract Date of the Journal
	 */
	public class ExtractDateUpdateResult implements RemoteApi.ResponseParam {
		public List<String> journalNos;
	}

	/*
	 * API for Updating the Extract Date of the API
	 */
	public with sharing class UpdateExtractDateApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			JournalExtractDateParam param = (JournalExtractDateParam) req.getParam(ExpJournalResource.JournalExtractDateParam.class);
			param.validate();

			ExpCommonUtil.canUseExpense();

			List<String> journalNos = new ExpJournalService().updateExtractedDate(
					param.journalNos, AppDate.valueOf(param.extractDate));

			ExpJournalResource.ExtractDateUpdateResult result = new ExpJournalResource.ExtractDateUpdateResult();
			result.journalNos = journalNos;

			return result;
		}
	}

	/*
	 * Request Param for setting/updating the Payment Date of the Journal
	 */
	public class JournalPaymentDateParam implements RemoteApi.RequestParam {
		public List<ExpReport> reports;

		public void validate() {
			for (ExpReport report : reports) {
				ExpCommonUtil.validateId('reportId', report.reportId, true);
				if(String.isNotBlank(report.paymentDate)) {
					ExpCommonUtil.validateDate('paymentDate', report.paymentDate);
				}
			}
		}
	}

	/*
	 * Representing a report in the Request Param for setting/updating the Payment Date of the Journal
	 */
	public class ExpReport {
		public String reportId;
		public String paymentDate;
	}


	/*
	 * Api Response for setting/updating the Payment Date of the Journal
	 */
	public class PaymentDateUpdateResult implements RemoteApi.ResponseParam {
		public List<String> reportIds;
	}

	/*
	 * API for updating the Payment Date in the Journal
	 */
	public with sharing class UpdatePaymentDateApi extends RemoteApi.ResourceBase {
		
		/** isRollbackTest will be set as true during rollback test */
		@TestVisible
		private Boolean isRollbackTest = false;
		
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			JournalPaymentDateParam param = (JournalPaymentDateParam) req.getParam(ExpJournalResource.JournalPaymentDateParam.class);
			param.validate();

			ExpCommonUtil.canUseExpense();
			
			Savepoint sp = Database.setSavepoint();

			try {
				List<String> reportIds = new ExpJournalService().updatePaymentDate(param.reports, false);

				ExpJournalResource.PaymentDateUpdateResult result = new ExpJournalResource.PaymentDateUpdateResult();
				result.reportIds = reportIds;
				
				if (isRollbackTest) {
					throw new App.IllegalStateException('UpdatePaymentDateApi Rollback Test');
				}
	
				return result;
			} catch (Exception e) {
				System.debug('*** ERROR e=' + e);
				System.debug('*** ERROR e=' + e.getStackTraceString());
				Database.rollback(sp);
				throw e;
			}
		}
	}

	/*
	 * API for setting the Payment Date (for the first time) in the Journal
	 */
	public with sharing class CreatePaymentDateApi extends RemoteApi.ResourceBase {
		
		/** isRollbackTest will be set as true during rollback test */
		@TestVisible
		private Boolean isRollbackTest = false;
		
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			JournalPaymentDateParam param = (JournalPaymentDateParam) req.getParam(ExpJournalResource.JournalPaymentDateParam.class);
			param.validate();

			ExpCommonUtil.canUseExpense();
			
			Savepoint sp = Database.setSavepoint();

			try {
				List<String> reportIds = new ExpJournalService().updatePaymentDate(param.reports, true);

				ExpJournalResource.PaymentDateUpdateResult result = new ExpJournalResource.PaymentDateUpdateResult();
				result.reportIds = reportIds;
	
				if (isRollbackTest) {
					throw new App.IllegalStateException('CreatePaymentDateApi Rollback Test');
				}
				
				return result;
			} catch (Exception e) {
				System.debug('*** ERROR e=' + e);
				System.debug('*** ERROR e=' + e.getStackTraceString());
				Database.rollback(sp);
				throw e;
			}
		}
	}
}