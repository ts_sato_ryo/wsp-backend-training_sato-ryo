/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 履歴管理方式が有効期間型のマスタのエンティティ基底クラス
 */
public abstract with sharing class ValidPeriodEntityOld extends Entity {

	/** 有効開始日の最小日付 */
	public static final AppDate VALID_FROM_MIN = AppDate.newInstance(1900, 1, 1);
	/** 有効開始日の最大日付 */
	public static final AppDate VALID_FROM_MAX = AppDate.newInstance(2100, 12, 31);
	/** 失効日の最小日付 */
	public static final AppDate VALID_TO_MIN = AppDate.newInstance(1900, 1, 1);
	/** 失効日の最大日付 */
	public static final AppDate VALID_TO_MAX = AppDate.newInstance(2101, 1, 1);

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目値 */
		public AppMultiString nameL;
		/** コンストラクタ */
		public LookupField(AppMultiString nameL) {
			this.nameL = nameL;
		}
	}

	/**
	 * @desctiprion 会社の情報
	 */
	public class Company {
		/** 会社コード */
		public String code;
		/** コンストラクタ */
		public Company(String code) {
			this.code = code;
		}
	}

	/**
	 * 項目の定義
	 * 項目を追加したら、getFieldValue, setFieldValueメソッドへの追加もお願いします。
	 */
	public enum Field {
		NAME,
		VALID_FROM,
		VALID_TO}

	/** 値を変更した項目のリスト */
	protected Set<ValidPeriodEntityOld.Field> isChangedFieldSet = new Set<ValidPeriodEntityOld.Field>();


	/** レコード名 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.NAME);
		}
	}
	/** 有効開始日 */
	public AppDate validFrom {
		get;
		set {
			validFrom = value;
			setChanged(Field.VALID_FROM);
		}
	}
	/** 失効日 */
	public AppDate validTo {
		get;
		set {
			validTo = value;
			setChanged(Field.VALID_TO);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	protected void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public virtual void resetChanged() {
		this.isChangedFieldSet.clear();
	}

	/**
	 * 指定された項目の値を取得する
	 * @param targetField 対象の項目
	 * @return 項目値
	 */
	public Object getFieldValue(ValidPeriodEntityOld.Field targetField) {

		if (targetField == Field.NAME) {
			return this.name;
		}
		if (targetField == Field.VALID_FROM) {
			return this.validFrom;
		}
		if (targetField == Field.VALID_TO) {
			return this.validTo;
		}

		// ここにきたらバグ
		throw new App.UnsupportedException('ValidPeriodEntityOld.getFieldValue: 未対応の項目です。targetField='+ targetField);
	}

	/**
	 * 指定された項目に値を設定する
	 * @param targetField 対象の項目
	 * @pram value 設定値
	 */
	public void setFieldValue(ValidPeriodEntityOld.Field targetField, Object value) {
		if (targetField == Field.NAME) {
			this.name = (String)value;
		} else if (targetField == Field.VALID_FROM) {
			this.validFrom = (AppDate)value;
		} else if (targetField == Field.VALID_TO) {
			this.validTo = (AppDate)value;

		} else {
			// ここにきたらバグ
			throw new App.UnsupportedException('ValidPeriodEntityOld.setFieldValue: 未対応の項目です。targetField='+ targetField);
		}
	}
	// 指定日付が期間範囲内かどうかをチェックする
	public Boolean isInPeriod(AppDate targetDate) {
		if (targetDate == null) {
			return false;
		}
		Date targetDt = targetDate.getDate();
		Date fromDt = validFrom.getDate();
		Date toDt = validTo.getDate();
		return fromDt <= targetDt && targetDt < toDt;
	}
}
