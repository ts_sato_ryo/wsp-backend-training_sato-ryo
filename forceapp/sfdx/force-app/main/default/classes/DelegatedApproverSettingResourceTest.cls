/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description DelegatedApproverSettingResourceのテストクラス
 */
@isTest
private class DelegatedApproverSettingResourceTest {

	/**
	 * 代理承認者設定一覧データ取得APIのテスト
	 */
	@isTest static void getSettingListApiTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Japan');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComDeptBase__c departmentObj = ComTestDataUtility.createDepartmentsWithHistory('Department', companyObj.Id, 1)[0];
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, departmentObj.Id, UserInfo.getUserId(), permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, departmentObj.Id, UserInfo.getUserId(), permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		List<ComDelegatedApproverSetting__c> settings = new List<ComDelegatedApproverSetting__c>();
		for (Integer i = 0; i < delegatedApproverObjs.size(); i++) {
			ComDelegatedApproverSetting__c setting = ComTestDataUtility.createDelegatedApproverSetting(employeeObj.Id, delegatedApproverObjs[i].Id);
			settings.add(setting);
		}

		Test.startTest();

		// リクエストパラメータ
		DelegatedApproverSettingResource.GetSettingListParam param = new DelegatedApproverSettingResource.GetSettingListParam();
		param.empId = null;

		// API実行
		DelegatedApproverSettingResource.GetSettingListApi api = new DelegatedApproverSettingResource.GetSettingListApi();
		DelegatedApproverSettingResource.GetSettingListResult res = (DelegatedApproverSettingResource.GetSettingListResult)api.execute(param);

		Test.stopTest();

		for (Integer i = 0; i < res.settingList.size(); i++) {
			System.assertEquals(settings[i].Id, res.settingList[i].settingId);
			System.assertEquals(settings[i].DelegatedApproverBaseId__c, res.settingList[i].delegatedApproverId);
			System.assertEquals(delegatedApproverUserObj[i].IsActive, res.settingList[i].isActiveSFUserAcc);
			System.assertEquals(delegatedApproverObjs[i].Code__c, res.settingList[i].delegatedApproverCode);
			AppPersonName approverName = new AppPersonName(
					delegatedApproverObjs[i].FirstName_L0__c,
					delegatedApproverObjs[i].LastName_L0__c,
					delegatedApproverObjs[i].FirstName_L1__c,
					delegatedApproverObjs[i].LastName_L1__c,
					delegatedApproverObjs[i].FirstName_L2__c,
					delegatedApproverObjs[i].LastName_L2__c);
			System.assertEquals(approverName.getFullName(), res.settingList[i].delegatedApproverName);
			User user = [SELECT Id, SmallPhotoUrl FROM User WHERE Id = :delegatedApproverObjs[i].UserId__c LIMIT 1];
			System.assertEquals(user.SmallPhotoUrl, res.settingList[i].delegatedApproverPhotoUrl);
			System.assertEquals(settings[i].ApproveExpenseRequestByDelegate__c, res.settingList[i].canApproveExpenseRequestByDelegate);
			System.assertEquals(settings[i].ApproveExpenseReportByDelegate__c, res.settingList[i].canApproveExpenseReportByDelegate);
		}
	}

	/**
	 * 代理承認者設定一覧データ取得APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void getSettingListApiTestValidation() {

		Test.startTest();

		// 社員IDが不正
		DelegatedApproverSettingResource.GetSettingListParam param = new DelegatedApproverSettingResource.GetSettingListParam();
		param.empId = 'test';
		try {
			param.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 代理承認者設定データ保存APIのテスト
	 */
	@isTest static void saveSettingListApiTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		Test.startTest();

		// リクエストパラメータ
		DelegatedApproverSettingResource.SaveSettingParam param = new DelegatedApproverSettingResource.SaveSettingParam();
		param.empId = employeeObj.Id;
		param.settings = new List<DelegatedApproverSettingResource.DelegatedApproverSetting>();
		param.settings.add(createParam(delegatedApproverObjs[0].Id));
		param.settings.add(createParam(delegatedApproverObjs[1].Id));
		param.settings.add(createParam(delegatedApproverObjs[2].Id));

		// API実行
		DelegatedApproverSettingResource.SaveSettingApi api = new DelegatedApproverSettingResource.SaveSettingApi();
		api.execute(param);

		Test.stopTest();

		// 代理承認者設定が保存されているかの確認
		List<DelegatedApproverSettingEntity> result = new DelegatedApproverSettingService().getSettingList(employeeObj.Id);
		System.assertEquals(param.settings.size(), result.size());
		for (Integer i = 0; i < result.size(); i++) {
			System.assertEquals(param.empId, result[i].employeeBaseId);
			System.assertEquals(param.settings[i].delegatedApproverId, result[i].delegatedApproverBaseId);
			System.assertEquals(param.settings[i].canApproveExpenseRequestByDelegate, result[i].canApproveExpenseRequestByDelegate);
			System.assertEquals(param.settings[i].canApproveExpenseReportByDelegate, result[i].canApproveExpenseReportByDelegate);
		}
	}

	/**
	 * 代理承認者設定データ保存APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void saveSettingListApiTestValidation() {

		Test.startTest();

		// 社員IDが不正
		DelegatedApproverSettingResource.SaveSettingParam param = new DelegatedApproverSettingResource.SaveSettingParam();
		param.empId = 'test';
		try {
			param.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 代理承認者IDが未設定
		param = new DelegatedApproverSettingResource.SaveSettingParam();
		param.settings = new List<DelegatedApproverSettingResource.DelegatedApproverSetting>();
		param.settings.add(createParam(null));
		try {
			param.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// Invalid settingId
		param.settings[0].settingId = 'Test';
		try {
			param.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 代理承認者IDが不正
		param.settings[0].settingId = null;
		param.settings[0].delegatedApproverId = 'Test';
		try {
			param.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 代理承認者設定データ保存APIのテスト
	 * エラーが発生した場合はロールバックされることを確認する
	 */
	@isTest static void saveSettingListApiTestRollback() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		Test.startTest();

		// リクエストパラメータ
		DelegatedApproverSettingResource.SaveSettingParam param = new DelegatedApproverSettingResource.SaveSettingParam();
		param.empId = employeeObj.Id;
		param.settings = new List<DelegatedApproverSettingResource.DelegatedApproverSetting>();
		param.settings.add(createParam(delegatedApproverObjs[0].Id));
		param.settings.add(createParam(delegatedApproverObjs[1].Id));
		param.settings.add(createParam(delegatedApproverObjs[2].Id));

		// API実行
		try {
			DelegatedApproverSettingResource.SaveSettingApi api = new DelegatedApproverSettingResource.SaveSettingApi();
			api.isRollbackTest = true;
			api.execute(param);

			System.assert(false, '例外が発生しませんでした。');
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());

			// ロールバックされていることを確認する
			// 1件も代理承認者設定データが保存されていないはず
			List<ComDelegatedApproverSetting__c> resSettingList = [SELECT Id FROM ComDelegatedApproverSetting__c];
			System.assert(resSettingList.isEmpty());
		}

		Test.stopTest();
	}

	/**
	 * 代理承認者設定データ削除APIのテスト
	 */
	@isTest static void deleteSettingListApiTest() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		DelegatedApproverSettingService service = new DelegatedApproverSettingService();

		// 代理承認者設定
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(employeeObj.Id, AppDate.today());
		List<DelegatedApproverSettingEntity> settings = new List<DelegatedApproverSettingEntity>();
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[0].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[1].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[2].Id));
		service.saveSettingList(employee, settings);

		List<Id> deleteSettingIdList = new List<Id>();
		for (DelegatedApproverSettingEntity setting : service.getSettingList(employeeObj.Id)) {
			deleteSettingIdList.add(setting.id);
		}

		Test.startTest();

		// リクエストパラメータ
		DelegatedApproverSettingResource.DeleteSettingParam param = new DelegatedApproverSettingResource.DeleteSettingParam();
		param.settingIds = deleteSettingIdList;

		// API実行
		DelegatedApproverSettingResource.DeleteSettingApi api = new DelegatedApproverSettingResource.DeleteSettingApi();
		api.execute(param);

		Test.stopTest();

		// 代理承認者設定が削除されているかの確認
		List<DelegatedApproverSettingEntity> result = service.getSettingList(employeeObj.Id);
		System.assertEquals(null, result);
	}

	/**
	 * 代理承認者設定データ削除APIのテスト
	 * バリデーションが正しく行われることを確認する
	 */
	@isTest static void deleteSettingListApiTestValidation() {

		Test.startTest();

		// 代理承認者設定IDのリストが未設定
		DelegatedApproverSettingResource.DeleteSettingParam param = new DelegatedApproverSettingResource.DeleteSettingParam();
		param.settingIds = null;
		try {
			param.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		// 代理承認者設定IDが不正
		param = new DelegatedApproverSettingResource.DeleteSettingParam();
		param.settingIds = new List<String>{'Test'};
		try {
			param.validate();
		} catch (App.ParameterException e) {
			// OK
		} catch (Exception e) {
			System.assert(false, '予期せぬ例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * 代理承認者設定データ削除APIのテスト
	 * エラーが発生した場合はロールバックされることを確認する
	 */
	@isTest static void deleteSettingListApiTestRollback() {
		// データ登録
		ComOrgSetting__c orgObj = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c countryObj = ComTestDataUtility.createCountry('Test');
		ComCompany__c companyObj = ComTestDataUtility.createCompany('Company', countryObj.Id);
		ComPermission__c permissionObj = ComTestDataUtility.createPermission('Test Permission', companyObj.Id);
		User employeeUserObj = ComTestDataUtility.createUser('EmployeeUser', UserInfo.getLanguage(), UserInfo.getLocale());
		ComEmpBase__c employeeObj = ComTestDataUtility.createEmployeeWithHistory('Employee', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id);
		List<User> delegatedApproverUserObj = ComTestDataUtility.createUsers('ApproverUser', UserInfo.getLanguage(), UserInfo.getLocale(), 3);
		List<ComEmpBase__c> delegatedApproverObjs = ComTestDataUtility.createEmployeesWithHistory('Approver', companyObj.Id, null, employeeUserObj.Id, permissionObj.Id, 3);
		// 社員コードが昇順になるように更新
		delegatedApproverObjs[0].Code__c = '001';
		delegatedApproverObjs[0].UserId__c = delegatedApproverUserObj[0].Id;
		delegatedApproverObjs[1].Code__c = '002';
		delegatedApproverObjs[1].UserId__c = delegatedApproverUserObj[1].Id;
		delegatedApproverObjs[2].Code__c = '003';
		delegatedApproverObjs[2].UserId__c = delegatedApproverUserObj[2].Id;
		update delegatedApproverObjs;

		DelegatedApproverSettingService service = new DelegatedApproverSettingService();

		// 代理承認者設定
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(employeeObj.Id, AppDate.today());
		List<DelegatedApproverSettingEntity> settings = new List<DelegatedApproverSettingEntity>();
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[0].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[1].Id));
		settings.add(createEntity(employeeObj.Id, delegatedApproverObjs[2].Id));
		service.saveSettingList(employee, settings);

		List<Id> deleteSettingIdList = new List<Id>();
		for (DelegatedApproverSettingEntity setting : service.getSettingList(employeeObj.Id)) {
			deleteSettingIdList.add(setting.id);
		}

		Test.startTest();

		// リクエストパラメータ
		DelegatedApproverSettingResource.DeleteSettingParam param = new DelegatedApproverSettingResource.DeleteSettingParam();
		param.settingIds = deleteSettingIdList;

		// API実行
		try {
			DelegatedApproverSettingResource.DeleteSettingApi api = new DelegatedApproverSettingResource.DeleteSettingApi();
			api.isRollbackTest = true;
			api.execute(param);

			System.assert(false, '例外が発生しませんでした。');
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());

			// ロールバックされていることを確認する
			// 1件も代理承認者設定データが削除されていないはず
			List<ComDelegatedApproverSetting__c> resSettingList = [SELECT Id FROM ComDelegatedApproverSetting__c];
			System.assertEquals(settings.size(), resSettingList.size());
		}

		Test.stopTest();
	}

	/**
	 * 代理承認者設定パラメータを作成する Create delegated approver setting parameter
	 * @param delegatedApproverId 代理承認者ベースID
	 * @return 作成したパラメータ
	 */
	private static DelegatedApproverSettingResource.DelegatedApproverSetting createParam(Id delegatedApproverId) {
		DelegatedApproverSettingEntity setting = new DelegatedApproverSettingEntity();
		setting.delegatedApproverBaseId = delegatedApproverId;
		setting.canApproveExpenseRequestByDelegate = true;
		setting.canApproveExpenseReportByDelegate = true;

		return new DelegatedApproverSettingResource.DelegatedApproverSetting(setting);
	}

	/**
	 * 代理承認者設定エンティティを作成する Create delegated approver setting entity
	 * @param empId 社員ベースID
	 * @param delegatedApproverId 代理承認者ベースID
	 * @return 作成したエンティティ
	 */
	private static DelegatedApproverSettingEntity createEntity(Id empId, Id delegatedApproverId) {
		DelegatedApproverSettingEntity setting = new DelegatedApproverSettingEntity();
		setting.employeeBaseId = empId;
		setting.delegatedApproverBaseId = delegatedApproverId;
		setting.canApproveExpenseRequestByDelegate = true;
		setting.canApproveExpenseReportByDelegate = true;

		return setting;
	}
}