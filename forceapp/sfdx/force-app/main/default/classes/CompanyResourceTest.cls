/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description CompanyResourceのテストクラス
*/
@isTest
private class CompanyResourceTest {

	/**
	 * 会社レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting();

		Test.startTest();

		CompanyResource.Company param = new CompanyResource.Company();
		param.name_L0 = 'Test会社_L0';
		param.name_L1 = 'Test会社_L1';
		param.name_L2 = 'Test会社_L2';
		param.code = '001';
		param.countryId = ComTestDataUtility.createCountry('Japan').id;
		param.currencyId = ComTestDataUtility.createCurrency('JPY', 'Japanese Yen').id;
		param.language = 'ja';
		param.useAttendance = true;
		param.useExpense = true;
		param.useExpenseRequest = true;
		param.useCompanyTaxMaster = true;
		param.useWorkTime = true;
		param.usePlanner = true;
		param.plannerDefaultView = 'Monthly';

		param.jorudanFareType = '0';
		param.jorudanAreaPreference = 'ALL';
		param.jorudanUseChargedExpress = '0';
		param.jorudanChargedExpressDistance = '0';
		param.jorudanSeatPreference = '0';
		param.jorudanRouteSort = '0';
		param.jorudanCommuterPass = '0';
		param.jorudanHighwayBus = '0';

		param.allowTaxAmountChange = true;

		CompanyResource.CreateApi api = new CompanyResource.CreateApi();
		CompanyResource.SaveResult res = (CompanyResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 会社レコードが1件作成されること
		List<ComCompany__c> companyList = [SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CountryId__c,
				CurrencyId__c,
				Language__c,
				UseAttendance__c,
				UseExpense__c,
				UseExpenseRequest__c,
				UseCompanyTaxMaster__c,
				UseWorkTime__c,
				UsePlanner__c,
				PlannerDefaultView__c,
				JorudanFareType__c,
				JorudanAreaPreference__c,
				JorudanUseChargedExpress__c,
				JorudanChargedExpressDistance__c,
				JorudanSeatPreference__c,
				JorudanRouteSort__c,
				JorudanCommuterPass__c,
				JorudanHighwayBus__c,
				AllowTaxAmountChange__c
			FROM ComCompany__c];

		System.assertEquals(1, companyList.size());

		ComCompany__c c = companyList[0];
		System.assertEquals(c.Id, res.Id);
		System.assertEquals(param.name_L0, c.Name);
		System.assertEquals(param.name_L0, c.Name_L0__c);
		System.assertEquals(param.name_L1, c.Name_L1__c);
		System.assertEquals(param.name_L2, c.Name_L2__c);
		System.assertEquals(param.code, c.Code__c);
		System.assertEquals(param.countryId, c.CountryId__c);
		System.assertEquals(param.currencyId, c.CurrencyId__c);
		System.assertEquals(param.language, c.Language__c);
		System.assertEquals(param.useAttendance, c.UseAttendance__c);
		System.assertEquals(param.useExpense, c.UseExpense__c);
		System.assertEquals(param.useExpenseRequest, c.UseExpenseRequest__c);
		System.assertEquals(param.useCompanyTaxMaster, c.UseCompanyTaxMaster__c);
		System.assertEquals(param.useWorkTime, c.UseWorkTime__c);
		System.assertEquals(param.usePlanner, c.UsePlanner__c);
		System.assertEquals(param.jorudanFareType, String.valueOf(c.JorudanFareType__c));
		System.assertEquals(param.jorudanAreaPreference, c.JorudanAreaPreference__c);
		System.assertEquals(param.jorudanUseChargedExpress, String.valueOf(c.JorudanUseChargedExpress__c));
		System.assertEquals(param.jorudanChargedExpressDistance, String.valueOf(c.JorudanChargedExpressDistance__c));
		System.assertEquals(param.jorudanSeatPreference, String.valueOf(c.JorudanSeatPreference__c));
		System.assertEquals(param.jorudanRouteSort, String.valueOf(c.JorudanRouteSort__c));
		System.assertEquals(param.jorudanCommuterPass, String.valueOf(c.JorudanCommuterPass__c));
		System.assertEquals(param.jorudanHighwayBus, String.valueOf(c.JorudanHighwayBus__c));
		System.assertEquals('Monthly', c.PlannerDefaultView__c);
		System.assertEquals(param.allowTaxAmountChange, c.AllowTaxAmountChange__c);
	}

	/**
	 * 会社レコードを1件作成する(正常系)
	 * 会社のデフォルトカレンダーが作成されることを確認する
	 */
	@isTest
	static void createPositiveTestCalendar() {
		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting();

		Test.startTest();

		CompanyResource.Company param = new CompanyResource.Company();
		param.name_L0 = 'Test会社_L0';
		param.name_L1 = 'Test会社_L1';
		param.name_L2 = 'Test会社_L2';
		param.code = '001';
		param.countryId = ComTestDataUtility.createCountry('Japan').id;
		param.language = 'ja';
		param.useAttendance = true;
		param.useExpense = true;
		param.useExpenseRequest = true;
		param.useCompanyTaxMaster = true;
		param.useWorkTime = true;
		param.usePlanner = true;
		param.plannerDefaultView = 'Daily';

		param.jorudanFareType = '0';
		param.jorudanAreaPreference = 'ALL';
		param.jorudanUseChargedExpress = '0';
		param.jorudanChargedExpressDistance = '0';
		param.jorudanSeatPreference = '0';
		param.jorudanRouteSort = '0';
		param.jorudanCommuterPass = '0';
		param.jorudanHighwayBus = '0';

		CompanyResource.CreateApi api = new CompanyResource.CreateApi();
		CompanyResource.SaveResult res = (CompanyResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 会社レコードが1件作成されること
		List<ComCompany__c> companyList = [SELECT
				Id,
				CalendarId__c
			FROM ComCompany__c];

		System.assertEquals(1, companyList.size());

		// 会社に紐づく勤怠用カレンダーが1件作成されること
		ComCalendar__c calendar = [
			SELECT
				Id,
				Type__c
			FROM ComCalendar__c
			WHERE CompanyId__c = :res.id];

		System.assertEquals(companyList[0].CalendarId__c, calendar.Id);
		System.assertEquals(CalendarType.ATTENDANCE.value, calendar.Type__c);
	}

	/**
	 * 会社レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeNegativeTest() {
		Test.startTest();

		Map<String, Object> param = new Map<String, Object> {};

		CompanyResource.CreateApi api = new CompanyResource.CreateApi();
		App.ParameterException ex;

		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals(App.ERR_CODE_NOT_SET_VALUE, ex.getErrorCode());

	}

	/**
	 * 会社レコードを1件作成する(異常系:コードが重複している)
	 */
	@isTest
	static void createNegativeTestDuplicateCode() {

		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting();
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c testRecord = ComTestDataUtility.createCompany('Test会社', country.Id);
		CompanyResource.CreateApi api = new CompanyResource.CreateApi();

		// テスト1 コードが重複しているためエラー
		CompanyResource.Company param1 = new CompanyResource.Company();
		param1.name_L0 = 'Test会社_L0';
		param1.code = testRecord.Code__c;
		param1.countryId = country.id;
		param1.language = 'ja';
		param1.plannerDefaultView = 'Daily';
		param1.useAttendance = true;
		param1.useExpense = true;
		param1.useExpenseRequest = true;
		param1.useCompanyTaxMaster = true;
		param1.useWorkTime = true;
		param1.usePlanner = true;

		param1.jorudanFareType = '0';
		param1.jorudanAreaPreference = 'ALL';
		param1.jorudanUseChargedExpress = '0';
		param1.jorudanChargedExpressDistance = '0';
		param1.jorudanSeatPreference = '0';
		param1.jorudanRouteSort = '0';
		param1.jorudanCommuterPass = '0';
		param1.jorudanHighwayBus = '0';
		param1.allowTaxAmountChange = true;

		// API実行
		try {
			api.execute(param1);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}

		// テスト2 コードが重複しておらず正常終了
		CompanyResource.Company param2 = new CompanyResource.Company();
		param2.name_L0 = 'Test会社_L0';
		param2.code = '001';
		param2.countryId = country.id;
		param2.language = 'ja';
		param2.plannerDefaultView = 'Daily';
		param2.useAttendance = true;
		param2.useExpense = true;
		param2.useExpenseRequest = true;
		param2.useCompanyTaxMaster = true;
		param2.useWorkTime = true;
		param2.usePlanner = true;

		param2.jorudanFareType = '0';
		param2.jorudanAreaPreference = 'ALL';
		param2.jorudanUseChargedExpress = '0';
		param2.jorudanChargedExpressDistance = '0';
		param2.jorudanSeatPreference = '0';
		param2.jorudanRouteSort = '0';
		param2.jorudanCommuterPass = '0';
		param2.jorudanHighwayBus = '0';
		param2.allowTaxAmountChange = true;

		// API実行
		api.execute(param2);
	}

	/**
	 * 会社レコードを1件作成する(異常系:App.NoPermissionException発生)
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void createNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		CompanyResource.Company param = new CompanyResource.Company();
		param.name_L0 = 'Test会社_L0';
		param.name_L1 = 'Test会社_L1';
		param.name_L2 = 'Test会社_L2';
		param.code = '001';
		param.countryId = testData.country.id;
		param.currencyId = ComTestDataUtility.createCurrency('JPY', 'Japanese Yen').id;
		param.language = 'ja';
		param.useAttendance = true;
		param.useExpense = true;
		param.useExpenseRequest = true;
		param.useCompanyTaxMaster = true;
		param.useWorkTime = true;
		param.usePlanner = true;
		param.plannerDefaultView = 'Monthly';
		param.allowTaxAmountChange = true;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				CompanyResource.CreateApi api = new CompanyResource.CreateApi();
				CompanyResource.SaveResult res = (CompanyResource.SaveResult)api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 会社レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', null, null, true);

		Test.startTest();

		// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
		// 全パラメータの検証は createPositiveTest() で行っている
		Map<String, Object> param = new Map<String, Object> {
			'id' => company.id,
			'code' => '002'
		};

		CompanyResource.UpdateApi api = new CompanyResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// 会社レコードが更新されていること
		List<ComCompany__c> companyList = [SELECT Code__c FROM ComCompany__c WHERE Id =:(String)param.get('id')];
		System.assertEquals(1, companyList.size());
		System.assertEquals((String)param.get('code'), companyList[0].Code__c);
	}

	/**
	 * 会社レコードを1件更新する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		Test.startTest();

		CompanyResource.Company param = new CompanyResource.Company();
		CompanyResource.UpdateApi api = new CompanyResource.UpdateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}


	/**
	 * 会社レコードを1件更新する(正常系:経費精算レポートが存在しない場合基準通貨をアップデート→正常にアップデート)
	 */
	@isTest
	static void updateBaseCurrencyPositiveReportExistTest() {

		ExpTestData expTestData = new ExpTestData();
		// ExpReportEntity expReportEntity = expTestData.createExpReport(Date.today(), expTestData.department, expTestData.employee);
		ComCurrency__c newCurrency = ComTestDataUtility.createCurrency('SGD', 'Singapore dollar');

		Test.startTest();

		Map<String, Object> param = new Map<String, Object> {
			'id' => expTestData.company.id,
			'code' => '002',
			'currencyId' => newCurrency.id
		};

		CompanyResource.UpdateApi api = new CompanyResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// 会社レコードが更新されていること
		List<ComCompany__c> companyList = [SELECT Code__c, CurrencyId__c FROM ComCompany__c WHERE Id =:(String)param.get('id')];
		System.assertEquals(1, companyList.size());
		System.assertEquals((String)param.get('code'), companyList[0].Code__c);
		System.assertEquals((String)param.get('currencyId'), companyList[0].CurrencyId__c);
	}

	/**
	 * 会社レコードを1件更新する(異常系:経費精算レポートが存在するにも関わらず基準通貨をアップデート→エラー発生)
	 */
	@isTest
	static void updateBaseCurrencyNegativeReportExistTest() {

		ExpTestData expTestData = new ExpTestData();
		ExpReportEntity expReportEntity = expTestData.createExpReport(Date.today(), expTestData.department, expTestData.employee, expTestData.job);
		ComCurrency__c newCurrency = ComTestDataUtility.createCurrency('SGD', 'Singapore dollar');

		Test.startTest();

		Map<String, Object> param = new Map<String, Object> {
			'id' => expTestData.company.id,
			'code' => '002',
			'currencyId' => newCurrency.id
		};

		CompanyResource.UpdateApi api = new CompanyResource.UpdateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 会社レコードを1件更新する(異常系:コードが重複している)
	 */
	@isTest
	static void updateNegativeTestDuplicateCode() {

		ComOrgSetting__c setting = ComTestDataUtility.createOrgSetting();
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c testRecord = ComTestDataUtility.createCompany('Test会社1', country.Id);
		ComCompany__c testRecordDuplicate = ComTestDataUtility.createCompany('Test会社2', country.Id);

		CompanyResource.UpdateApi api = new CompanyResource.UpdateApi();

		// テスト1 コードが重複しているためエラー
		Map<String, Object> param1 = new Map<String, Object> {
			'id' => testRecord.id,
			'code' => testRecordDuplicate.Code__c
		};

		// API実行
		try {
			api.execute(param1);
			System.assert(false);
		} catch (App.ParameterException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
			System.assertEquals('INVALID_PARAMETER', e.getErrorCode());
		}

		// テスト2 コードが重複しておらず正常終了
		Map<String, Object> param2 = new Map<String, Object> {
			'id' => testRecord.id,
			'code' => '001'
		};

		// API実行
		api.execute(param2);

		// テスト3 コードが重複しているが、同じIdのため正常終了
		Map<String, Object> param3 = new Map<String, Object> {
			'id' => testRecord.id,
			'code' => testRecord.Code__c
		};

		// API実行
		api.execute(param3);
	}

	/**
	 * 会社レコードを1件更新する(異常系:App.NoPermissionException発生)
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void updateNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
		// 全パラメータの検証は createPositiveTest() で行っている
		Map<String, Object> param = new Map<String, Object> {
			'id' => testData.company.id,
			'code' => '002'
		};

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				CompanyResource.UpdateApi api = new CompanyResource.UpdateApi();
				Object res = api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 会社レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', null, null, true);

		Test.startTest();

		CompanyResource.DeleteOption param = new CompanyResource.DeleteOption();
		param.id = company.id;

		Object res = (new CompanyResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 会社レコードが削除されること
		List<ComCompany__c> companyList = [SELECT Id FROM ComCompany__c];
		System.assertEquals(0, companyList.size());
	}

	/**
	 * 会社レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		CompanyResource.DeleteOption param = new CompanyResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new CompanyResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());

	}

	/**
	 * 会社レコードを1件削除する(異常系:DMLException発生)
	 */
	@isTest
	static void deleteNegativeTest() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		Test.startTest();

		CompanyResource.DeleteOption param = new CompanyResource.DeleteOption();
		param.Id = testData.company.id;

		DmlException ex;

		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)){
			try {
				Object res = (new CompanyResource.DeleteApi()).execute(param);
			} catch (DmlException e) {
				ex = e;
			}
		}

		Test.stopTest();

		System.assertEquals('INSUFFICIENT_ACCESS_OR_READONLY', ex.getDmlStatusCode(0));
	}

	/**
	 * 会社レコードを1件削除する(異常系:App.NoPermissionException発生)
	 * API実行に必要なアクセス権限を持っていない場合は想定されるアプリ例外が発生することを確認する
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {
		TestData.TestDataEntity testData = new TestData.TestDataEntity();
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');

		// 全体設定管理権限を無効に設定する
		testData.permission.isManageOverallSetting = false;
		new PermissionRepository().saveEntity(testData.permission);

		CompanyResource.DeleteOption param = new CompanyResource.DeleteOption();
		param.Id = testData.company.id;

		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				Object res = (new CompanyResource.DeleteApi()).execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 会社レコードをID検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', null);

		Test.startTest();

		CompanyResource.SearchCondition param = new CompanyResource.SearchCondition();
		param.id = company.id;

		CompanyResource.SearchApi api = new CompanyResource.SearchApi();
		CompanyResource.SearchResult res = (CompanyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 会社レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(company.Id, res.records[0].id);
		// 項目値の検証はsearchAllPositiveTest()で行うため省略
	}

	/**
	 * 会社レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', country.Id);

		Test.startTest();

		CompanyResource.SearchCondition param = new CompanyResource.SearchCondition();
		param.id = company.id;

		CompanyResource.SearchApi api = new CompanyResource.SearchApi();
		CompanyResource.SearchResult res = (CompanyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 会社レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(company.Id, res.records[0].id);
		// 多言語対応項目の検証
		company = [SELECT Name_L1__c, CountryId__r.Name_L1__c, CurrencyId__r.Name_L1__c FROM ComCompany__c WHERE Id =: company.Id];
		System.assertEquals(company.Name_L1__c, res.records[0].name);
		System.assertEquals(company.CountryId__r.Name_L1__c, res.records[0].country.name);
		System.assertEquals(company.CurrencyId__r.Name_L1__c, res.records[0].currencyField.name);
	}

	/**
	 * 会社レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', country.Id);

		Test.startTest();

		CompanyResource.SearchCondition param = new CompanyResource.SearchCondition();
		param.id = company.id;

		CompanyResource.SearchApi api = new CompanyResource.SearchApi();
		CompanyResource.SearchResult res = (CompanyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 会社レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(company.Id, res.records[0].id);
		// 多言語対応項目の検証
		company = [SELECT Name_L2__c, CountryId__r.Name_L2__c, CurrencyId__r.Name_L2__c FROM ComCompany__c WHERE Id =: company.Id];
		System.assertEquals(company.Name_L2__c, res.records[0].name);
		System.assertEquals(company.CountryId__r.Name_L2__c, res.records[0].country.name);
		System.assertEquals(company.CurrencyId__r.Name_L2__c, res.records[0].currencyField.name);
	}

	/**
	 * 会社レコードを全件検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		List<ComCompany__c> testRecords = ComTestDataUtility.createCompanies('Test会社', country.Id, 3);

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>();
		CompanyResource.SearchApi api = new CompanyResource.SearchApi();
		CompanyResource.SearchResult res = (CompanyResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 会社レコードが取得できること
		System.assertEquals(testRecords.size(), res.records.size());

		testRecords = [SELECT
				Id,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CountryId__c,
				CountryId__r.Name_L0__c,
				CurrencyId__c,
				CurrencyId__r.Code__c,
				CurrencyId__r.Name_L0__c,
				Language__c,
				UseAttendance__c,
				UseExpense__c,
				UseExpenseRequest__c,
				UseWorkTime__c,
				UsePlanner__c,
				PlannerDefaultView__c,
				UseCompanyTaxMaster__c,
				JorudanFareType__c,
				JorudanAreaPreference__c,
				JorudanUseChargedExpress__c,
				JorudanChargedExpressDistance__c,
				JorudanSeatPreference__c,
				JorudanRouteSort__c,
				JorudanCommuterPass__c,
				JorudanHighwayBus__c,
				AllowTaxAmountChange__c
			FROM ComCompany__c
			WHERE Id IN :testRecords
			ORDER BY Code__c];

		for (Integer i = 0, n = testRecords.size(); i < n; i++) {
			ComCompany__c sobj = testRecords[i];
			CompanyResource.Company dto = res.records[i];
			System.assertEquals(sobj.Id, dto.id);
			System.assertEquals(sobj.Name_L0__c, dto.name);	// 会社名のデフォルトはL0を返す
			System.assertEquals(sobj.Name_L0__c, dto.name_L0);
			System.assertEquals(sobj.Name_L1__c, dto.name_L1);
			System.assertEquals(sobj.Name_L2__c, dto.name_L2);
			System.assertEquals(sobj.Code__c, dto.code);
			System.assertEquals(sobj.CountryId__c, dto.countryId);
			System.assertEquals(sobj.CountryId__r.Name_L0__c, dto.country.name);
			System.assertEquals(sobj.CurrencyId__c, dto.currencyId);
			System.assertEquals(sobj.CurrencyId__r.Name_L0__c, dto.currencyField.name);
			System.assertEquals(sobj.CurrencyId__r.Code__c, dto.currencyField.code);
			System.assertEquals(sobj.Language__c, dto.language);
			System.assertEquals(sobj.UseAttendance__c, dto.useAttendance);
			System.assertEquals(sobj.UseExpense__c, dto.useExpense);
			System.assertEquals(sobj.UseExpenseRequest__c, dto.useExpenseRequest);
			System.assertEquals(sobj.UseWorkTime__c, dto.useWorkTime);
			System.assertEquals(sobj.UsePlanner__c, dto.usePlanner);
			System.assertEquals(sobj.PlannerDefaultView__c, dto.plannerDefaultView);
			System.assertEquals(sobj.UseCompanyTaxMaster__c, dto.useCompanyTaxMaster);

			System.assertEquals(String.valueOf(sobj.JorudanFareType__c), dto.jorudanFareType);
			System.assertEquals(String.valueOf(sobj.JorudanAreaPreference__c), dto.jorudanAreaPreference);
			System.assertEquals(String.valueOf(sobj.JorudanUseChargedExpress__c), dto.jorudanUseChargedExpress);
			System.assertEquals(String.valueOf(sobj.JorudanChargedExpressDistance__c), dto.jorudanChargedExpressDistance);
			System.assertEquals(String.valueOf(sobj.JorudanSeatPreference__c), dto.jorudanSeatPreference);
			System.assertEquals(String.valueOf(sobj.JorudanRouteSort__c), dto.jorudanRouteSort);
			System.assertEquals(String.valueOf(sobj.JorudanCommuterPass__c), dto.jorudanCommuterPass);
			System.assertEquals(String.valueOf(sobj.JorudanHighwayBus__c), dto.jorudanHighwayBus);
			System.assertEquals(sobj.AllowTaxAmountChange__c, dto.allowTaxAmountChange);
		}
	}

	/**
	 * 会社レコードを検索する(正常系:デフォルト値を検証するテスト)
	 */
	@isTest
	static void searchDefaultValueTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createCompany('Test会社', country.Id);
		company.PlannerDefaultView__c = null;
		upsert company;

		Test.startTest();

		CompanyResource.SearchCondition param = new CompanyResource.SearchCondition();
		param.id = company.id;

		CompanyResource.SearchApi api = new CompanyResource.SearchApi();
		CompanyResource.SearchResult res = (CompanyResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 会社レコードが取得できること
		System.assertEquals(1, res.records.size());
		System.assertEquals(company.Id, res.records[0].id);

		// デフォルト値が正しいこと
		System.assertEquals(PlannerViewType.WEEKLY.value, res.records[0].plannerDefaultView);
		System.assertEquals(true, res.records[0].useCompanyTaxMaster);
	}


}