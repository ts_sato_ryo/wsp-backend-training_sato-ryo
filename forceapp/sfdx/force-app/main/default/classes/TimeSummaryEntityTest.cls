/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 工数
 *
 * @description TimeSummaryEntityのテストクラス
 */
@isTest
private class TimeSummaryEntityTest {

	/**
	 * 参照項目テスト
	 * 参照項目がない場合にエラーを発生しないことを確認する
	 * 参照項目がない場合にnull または 初期値が返ってくることを確認する
	 */
	 @isTest static void noReferenceItemTest() {
		// requestIdでひもづくもの
		// status・isConfirmationRequired・cancelType
		 {
			 try {
				 TimeSummaryEntity entity = new TimeSummaryEntity();
				 // Case-1 : requestIdの紐付けがない
				 System.assertEquals(null, entity.status);
				 System.assertEquals(null, entity.cancelType);
				 System.assertEquals(false, entity.isConfirmationRequired);
				 // Case-2 : requestIdの紐付けがある
				 entity.requestId = UserInfo.getUserId();// UserInfo.getUserId()はダミーコード
				 System.assertEquals(null, entity.status);
				 System.assertEquals(null, entity.cancelType);
				 System.assertEquals(false, entity.isConfirmationRequired);
			 } catch (Exception e) {
				 TestUtil.fail('Unexpected Error occured ！' + e.getCause());
			 }
		 }
		// employeeIdで紐付くもの
		// employeeBaseId・employeeNameL・employeePhotoUrl・departmentBaseId
		 {
			 try {
				 TimeSummaryEntity entity = new TimeSummaryEntity();
				 // Case-1 : employeeIdの紐付けがない
				 System.assertEquals(null, entity.employeeBaseId);
				 System.assertEquals(null, entity.employeeNameL);
				 System.assertEquals(null, entity.employeePhotoUrl);
				 System.assertEquals(null, entity.departmentBaseId);

				 // Case-2 : employeeIdの紐付けがある
				 entity.employeeId = UserInfo.getUserId();// UserInfo.getUserId()はダミーコード
				 System.assertEquals(null, entity.employeeBaseId);
				 System.assertEquals(null, entity.employeeNameL.firstNameL0);
				 System.assertEquals(null, entity.employeeNameL.firstNameL1);
				 System.assertEquals(null, entity.employeeNameL.firstNameL2);
				 System.assertEquals(null, entity.employeeNameL.lastNameL0);
				 System.assertEquals(null, entity.employeeNameL.lastNameL1);
				 System.assertEquals(null, entity.employeeNameL.lastNameL2);
				 System.assertEquals(null, entity.employeePhotoUrl);
				 System.assertEquals(null, entity.departmentBaseId);

			 } catch (Exception e) {
				 TestUtil.fail('Unexpected Error occured ！' + e.getCause());
			 }
		 }
	 }

	/**
	 * createName のテスト
	 * 適当な値を設定した場合にemployeeCode+employeeName+targetDateとなっていることを確認する
	 */
	@isTest static void createNameTest() {
		String actName = null;
		try {
			actName = TimeSummaryEntity.createName('E001', '京橋太郎', AppDate.newInstance(2019, 5, 10), AppDate.newInstance(2019, 6, 9));
		} catch(Exception e) {
			TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
		}
		System.assertEquals('E001京橋太郎20190510-20190609', actName);
	}

	/**
	 * createName のテスト
	 * 不正な値を設定した場合に例外が発生することを確認する
	 */
	@isTest static void createNameErrorTest() {
		{
			App.ParameterException actEx;
			try {
				TimeSummaryEntity.createName(null, '京橋太郎', AppDate.newInstance(2019, 5, 10), AppDate.newInstance(2019, 6, 9));
			} catch(App.ParameterException e) {
				actEx = e;
			} catch(Exception e) {
				TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals('employeeCode is blank', actEx.getMessage());
		}
		{
			App.ParameterException actEx;
			try {
				TimeSummaryEntity.createName('E001', null, AppDate.newInstance(2019, 5, 10), AppDate.newInstance(2019, 6, 9));
			} catch(App.ParameterException e) {
				actEx = e;
			} catch(Exception e) {
				TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals('employeeName is blank', actEx.getMessage());
		}
		{
			App.ParameterException actEx;
			try {
				TimeSummaryEntity.createName('E001', '京橋太郎', null, AppDate.newInstance(2019, 6, 9));
			} catch(App.ParameterException e) {
				actEx = e;
			} catch(Exception e) {
				TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals('startDate is null', actEx.getMessage());
		}
		{
			App.ParameterException actEx;
			try {
				TimeSummaryEntity.createName('E001', '京橋太郎', AppDate.newInstance(2019, 5, 10), null);
			} catch(App.ParameterException e) {
				actEx = e;
			} catch(Exception e) {
				TestUtil.fail('想定外の例外が発生しました。' + e.getCause());
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals('endDate is null', actEx.getMessage());
		}
	}

	/**
	 * createUniqKeyのテスト
	 * 生成される値が正しいことを検証する
	 */
	@isTest static void createUniqKeyTest() {
		EmployeeBaseEntity employee = new EmployeeBaseEntity();
		employee.setId(ComTestDataUtility.getDummyId(ComEmpBase__c.sObjectType));
		String expected = employee.id + '202001' + '1';
		System.assertEquals(expected, TimeSummaryEntity.createUniqKey(employee, '202001', 1));
	}

	/**
	 * getSubNoのテスト
	 * 生成される値が正しいことを検証する
	 */
	@isTest static void getSubNoTest() {
		// Case1 : 集計期間が月
		// 全ての起算日でsubNo=1を取得できることを確認する
		{
			for (Integer i = 1; i < 29; i++) {
				// テストデータ作成
				AppDateRange summaryRange = new AppDateRange(
						AppDate.newInstance(2019, 5, i),
						AppDate.newInstance(2019, 5, i).addMonths(1).addDays(-1));
				TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
				setting.setSummaryPeriod('Month');
				// 実行
				Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
				// 検証
				System.assertEquals(1, subNo);
			}
		}
		// Case2 : 集計期間が週(月度の表記：開始日)
		// 集計期間が週(月度の表記：開始日)を設定して全てのSubNoのパターンを取得できることを確認する
		// Case2-1 : 週(月度の表記：開始日)　対象期間開始日が月初7日以下 => 1
		{
			// テストデータ作成
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 9, 2),
					AppDate.newInstance(2019, 9, 8));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('BeginBase');
			setting.setStartDayOfWeek('Monday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(1, subNo);
		}
		// Case2-2 : 週(月度の表記：開始日)　対象期間開始日が月初14日以下 => 2
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 7, 9),
					AppDate.newInstance(2019, 7, 15));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('BeginBase');
			setting.setStartDayOfWeek('Tuesday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(2, subNo);
		}
		// Case2-3 : 週(月度の表記：開始日)　対象期間開始日が月初21日以下 => 3
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 2, 20),
					AppDate.newInstance(2019, 2, 26));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('BeginBase');
			setting.setStartDayOfWeek('Wednesday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(3, subNo);
		}
		// Case2-4 : 週(月度の表記：開始日)　対象期間開始日が月初28日以下 => 4
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 2, 28),
					AppDate.newInstance(2019, 3, 6));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('BeginBase');
			setting.setStartDayOfWeek('Thursday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(4, subNo);
		}
		// Case2-5 : 週(月度の表記：開始日)　対象期間開始日が月初28日以上 => 5
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 5, 31),
					AppDate.newInstance(2019, 6, 6));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('BeginBase');
			setting.setStartDayOfWeek('Friday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(5, subNo);
		}
		// Case3 : 集計期間が週(月度の表記：終了日)
		// 集計期間が週(月度の表記：終了日)を設定して全てのSubNoのパターンを取得できることを確認する
		// Case3-1 : 週(月度の表記：終了日)　対象期間終了日が月初7日未満 => 1
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 6, 29),
					AppDate.newInstance(2019, 7, 5));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('EndBase');
			setting.setStartDayOfWeek('Saturday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(1, subNo);
		}
		// Case3-2 : 週(月度の表記：終了日)　対象期間終了日が月初14日未満 => 2
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 7, 7),
					AppDate.newInstance(2019, 7, 13));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('EndBase');
			setting.setStartDayOfWeek('Sunday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(2, subNo);
		}
		// Case3-3 : 週(月度の表記：終了日)　対象期間終了日が月初21日未満 => 3
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 7, 14),
					AppDate.newInstance(2019, 7, 20));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('EndBase');
			setting.setStartDayOfWeek('Sunday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(3, subNo);
		}
		// Case3-4 : 週(月度の表記：終了日)　対象期間終了日が月初28日未満 => 4
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 7, 21),
					AppDate.newInstance(2019, 7, 27));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('EndBase');
			setting.setStartDayOfWeek('Sunday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(4, subNo);
		}
		// Case3-5 : 週(月度の表記：終了日)　対象期間終了日が月初28日以上 => 5
		{
			AppDateRange summaryRange = new AppDateRange(
					AppDate.newInstance(2019, 8, 23),
					AppDate.newInstance(2019, 8, 29));
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setMonthMark('EndBase');
			setting.setStartDayOfWeek('Thursday');
			// 実行
			Integer subNo = TimeSummaryEntity.getSubNo(summaryRange, setting);
			// 検証
			System.assertEquals(5, subNo);
		}
	}

	/**
	 * getSummaryRangeのテスト
	 * 与えた対象日を含むサマリー期間を返すことを確認する
	 */
	@isTest static void getSummaryRangeTest() {
		// Case1: 集計期間が月
		{
			// テストデータ作成
			AppDate targetDate = AppDate.newInstance(2019, 9, 2);
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Month');
			setting.startDateOfMonth = 1;
			// 実行
			AppDateRange actSummaryRange = TimeSummaryEntity.getSummaryRange(targetDate, setting);
			// 検証
			System.assertEquals(30, actSummaryRange.startDate.addDays(-1).daysBetween(actSummaryRange.endDate));
			System.assertEquals(AppDate.newInstance(2019, 9, 1), actSummaryRange.startDate);
			System.assertEquals(AppDate.newInstance(2019, 9, 30), actSummaryRange.endDate);
		}
		// Case2: サマリー単位が週 かつ 対象日＝起算曜日
		{
			// テストデータ作成
			AppDate targetDate = AppDate.newInstance(2019, 8, 7);
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setStartDayOfWeek('Wednesday');
			// 実行
			AppDateRange actSummaryRange = TimeSummaryEntity.getSummaryRange(targetDate, setting);
			// 検証
			System.assertEquals(7, actSummaryRange.startDate.addDays(-1).daysBetween(actSummaryRange.endDate));
			System.assertEquals(AppDate.newInstance(2019, 8, 7), actSummaryRange.startDate);
			System.assertEquals(AppDate.newInstance(2019, 8, 13), actSummaryRange.endDate);
		}
		// Case3: サマリー単位が週 かつ 対象日より起算曜日が前
		{
			// テストデータ作成
			AppDate targetDate = AppDate.newInstance(2019, 8, 7);
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setStartDayOfWeek('Tuesday');
			// 実行
			AppDateRange actSummaryRange = TimeSummaryEntity.getSummaryRange(targetDate, setting);
			// 検証
			System.assertEquals(7, actSummaryRange.startDate.addDays(-1).daysBetween(actSummaryRange.endDate));
			System.assertEquals(AppDate.newInstance(2019, 8, 6), actSummaryRange.startDate);
			System.assertEquals(AppDate.newInstance(2019, 8, 12), actSummaryRange.endDate);
		}
		// Case4: サマリー単位が週 かつ 対象日より起算曜日が後
		{
			// テストデータ作成
			AppDate targetDate = AppDate.newInstance(2019, 8, 7);
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setStartDayOfWeek('Saturday');
			// 実行
			AppDateRange actSummaryRange = TimeSummaryEntity.getSummaryRange(targetDate, setting);
			// 検証
			System.assertEquals(7, actSummaryRange.startDate.addDays(-1).daysBetween(actSummaryRange.endDate));
			System.assertEquals(AppDate.newInstance(2019, 8, 3), actSummaryRange.startDate);
			System.assertEquals(AppDate.newInstance(2019, 8, 9), actSummaryRange.endDate);
		}
		// Case4: サマリー単位が週 かつ 起算曜日が日曜 かつ 対象日が月曜
		{
			// テストデータ作成
			AppDate targetDate = AppDate.newInstance(2019, 7, 6);
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setStartDayOfWeek('Sunday');
			// 実行
			AppDateRange actSummaryRange = TimeSummaryEntity.getSummaryRange(targetDate, setting);
			// 検証
			System.assertEquals(7, actSummaryRange.startDate.addDays(-1).daysBetween(actSummaryRange.endDate));
			System.assertEquals(AppDate.newInstance(2019, 6, 30), actSummaryRange.startDate);
			System.assertEquals(AppDate.newInstance(2019, 7, 6), actSummaryRange.endDate);
		}
		// Case4: サマリー単位が週 かつ 起算曜日が土曜 かつ 対象日が日曜
		{
			// テストデータ作成
			AppDate targetDate = AppDate.newInstance(2019, 7, 14);
			TimeSettingBaseEntity setting = new TimeSettingBaseEntity();
			setting.setSummaryPeriod('Week');
			setting.setStartDayOfWeek('Saturday');
			// 実行
			AppDateRange actSummaryRange = TimeSummaryEntity.getSummaryRange(targetDate, setting);
			// 検証
			System.assertEquals(7, actSummaryRange.startDate.addDays(-1).daysBetween(actSummaryRange.endDate));
			System.assertEquals(AppDate.newInstance(2019, 7, 13), actSummaryRange.startDate);
			System.assertEquals(AppDate.newInstance(2019, 7, 19), actSummaryRange.endDate);
		}
	}

	/**
	 * 工数明細取得のテスト
	 * 値の有無に関わらず、nullが返らないことを検証する
	 */
	@isTest static void recordItemListTest() {

		// インスタンス生成
		TimeSummaryEntity summary = new TimeSummaryEntity();
		System.assertEquals(true, summary.recordList.isEmpty());

		// nullを設定
		summary.replaceRecordList(null);
		System.assertEquals(true, summary.recordList.isEmpty());

		// 工数明細を設定
		summary.replaceRecordList(new List<TimeRecordEntity> {new TimeRecordEntity()});
		System.assertEquals(1, summary.recordList.size());
	}
}