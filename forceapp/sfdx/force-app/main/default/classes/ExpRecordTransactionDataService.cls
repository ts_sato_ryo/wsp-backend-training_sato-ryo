/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Common service class for Records and Record items of both Expense Report and Expense Request.
 * 
 * @group Expense
 */
public with sharing virtual class ExpRecordTransactionDataService {
	/*
	 * Retrieve and return the Exchange Rate map for Record Cloning processing
	 * @param currencyIdSet Set of ID of Currency Used by Records
	 * @param clonedRecordTargetDateList List of Dates to clone the Records to
	 * @param earliestDate Earlier date where the Record would be cloned from the List of Target Dates
	 * @param latestDate Latest date where the Record would be cloned from the List of Target Dates
	 *
	 * @return Map contains the the following Structure Map<K: Currency ID, V: Map<K: Target Date, V: Exchange Rate>>
	 */
	protected Map<Id, Map<AppDate, Decimal>> getExchangeRateMapForRecordCloning(Set<Id> currencyIdSet, List<AppDate> clonedRecordTargetDateList, AppDate earliestDate, AppDate latestDate) {
		Map<Id, Map<AppDate, Decimal>> currencyTargetExchangeRateEntityMap = new Map<Id, Map<AppDate, Decimal>>();
		if (!currencyIdSet.isEmpty()) {
			ExpExchangeRateRepository.SearchFilter filter = new ExpExchangeRateRepository.SearchFilter();
			filter.currencyIds = currencyIdSet;
			filter.targetPeriod = new List<AppDate>{earliestDate, latestDate};
			List<ExpExchangeRateEntity> exchangeRateEntityList = new ExpExchangeRateRepository().searchEntity(filter);

			// Group Exchange Rate by Currency ID
			Map<Id, List<ExpExchangeRateEntity>> currencyExchangeRateMap = new Map<Id, List<ExpExchangeRateEntity>>();
			for (ExpExchangeRateEntity entity : exchangeRateEntityList) {
				List<ExpExchangeRateEntity> exRateList = currencyExchangeRateMap.get(entity.currencyId);
				if (exRateList == null) {
					exRateList = new List<ExpExchangeRateEntity>();
					currencyExchangeRateMap.put(entity.currencyId, exRateList);
				}
				exRateList.add(entity);
			}

			for (Id currencyId : currencyExchangeRateMap.keySet()) {
				Map<AppDate, Decimal> targetDateExchangeRateMap = currencyTargetExchangeRateEntityMap.get(currencyId);
				if (targetDateExchangeRateMap == null) {
					targetDateExchangeRateMap = new Map<AppDate, Decimal>();
					currencyTargetExchangeRateEntityMap.put(currencyId, targetDateExchangeRateMap);
				}
				for (AppDate targetDate : clonedRecordTargetDateList) {
					if (targetDateExchangeRateMap.get(targetDate) == null) {
						// Will be null for given date if no data is found for the given date.
						for (ExpExchangeRateEntity exchangeRateEntity : currencyExchangeRateMap.get(currencyId)) {
							if (exchangeRateEntity.validFrom.isBeforeEquals(targetDate) && exchangeRateEntity.validTo.isAfter(targetDate)) {
								targetDateExchangeRateMap.put(targetDate, exchangeRateEntity.calculationRate);
								break;
							}
						}
					}
				}
			}
		}
		return currencyTargetExchangeRateEntityMap;
	}

	/*
	 * Retrieve and return the Tax Type map for Record Cloning processing
	 * @param taxTypeBaseIdSet Set of ID of Tax Type Used by Records
	 * @param clonedRecordTargetDateList List of Dates to clone the Records to
	 * @param earliestDate Earlier date where the Record would be cloned from the List of Target Dates
	 * @param latestDate Latest date where the Record would be cloned from the List of Target Dates
	 *
	 * @return Map contains the the following Structure Map<K: Tax Base ID, V: Map<K: Target Date, V: Tax History Entity>>
	 */
	protected Map<Id, Map<AppDate, ExpTaxTypeHistoryEntity>> getTaxTypeMapForRecordCloning(Set<Id> taxTypeBaseIdSet, List<AppDate> clonedRecordTargetDateList, AppDate earliestDate, AppDate latestDate) {
		Map<Id, Map<AppDate, ExpTaxTypeHistoryEntity>> baseTargetHistoryMap = new Map<Id, Map<AppDate, ExpTaxTypeHistoryEntity>>();
		if (!taxTypeBaseIdSet.isEmpty()) {
			ExpTaxTypeRepository.SearchFilter filter = new ExpTaxTypeRepository.SearchFilter();
			filter.baseIds = new List<Id> (taxTypeBaseIdSet);
			filter.targetPeriod = new List<AppDate> {earliestDate, latestDate};
			List<ExpTaxTypeBaseEntity> taxTypeBaseEntityList = new ExpTaxTypeRepository() .searchEntityWithHistory(filter, false, ExpTaxTypeRepository.HistorySortOrder.VALID_FROM_ASC);

			if (taxTypeBaseEntityList.isEmpty() || taxTypeBaseEntityList.size() != taxTypeBaseIdSet.size()) {
				throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Exp_Lbl_TaxType}));
			}

			for (ExpTaxTypeBaseEntity base : taxTypeBaseEntityList) {
				Map<AppDate, ExpTaxTypeHistoryEntity> targetHistoryMap = baseTargetHistoryMap.get(base.id);
				if (targetHistoryMap == null) {
					targetHistoryMap = new Map<AppDate, ExpTaxTypeHistoryEntity>();
					baseTargetHistoryMap.put(base.id, targetHistoryMap);
				}
				for (AppDate targetDate : clonedRecordTargetDateList) {
					if (targetHistoryMap.get(targetDate) == null) {
						// need to find the corresponding history -> O(nm), where n is the number of Dates, and m is number of histories
						Boolean isTaxTypeFound = false; // Assuming the number of History found won't be too big - as it's already somewhat limited by date range.
						// But if the number of history can be significantly big, then this can be improved by keep a last found index.
						for (ExpTaxTypeHistoryEntity historyEntity : base.getHistoryList()) {
							if (historyEntity.validFrom.isBeforeEquals(targetDate) && historyEntity.validTo.isAfter(targetDate)) {
								targetHistoryMap.put(targetDate, historyEntity);
								isTaxTypeFound = true;
								break;
							}
						}
						if (isTaxTypeFound == false) {
							throw new App.RecordNotFoundException(ComMessage.msg().Com_Err_OutOfValidPeriod(new List<String>{ComMessage.msg().Exp_Lbl_TaxType}));
						}
					}
				}
			}
		}
		return baseTargetHistoryMap;
	}

	/*
	 * Process the Exchange Rate related data of the newly cloned Record
	 *
	 * @param clonedRecordItem Cloned Record Item
	 * @param currencyTargetExchangeRateEntityMap Map containing Exchange for a Given Currency at a given date
	 * @param decimalPlaces Number of Decimal places to use
	 *
	 * @return true if the Cloned Record has been marked as updated
	 */
	protected Boolean processExchangeRateDataForRecordCloning(ExpRecordItem clonedRecordItem,
			Map<Id, Map<AppDate, Decimal>> currencyTargetExchangeRateEntityMap, Integer decimalPlaces) {
		Boolean autoUpdated = false;

		Map<AppDate, Decimal> dateExchangeRateMap = currencyTargetExchangeRateEntityMap.get(clonedRecordItem.currencyId);
		if (dateExchangeRateMap != null) {
			Decimal exchangeRate = dateExchangeRateMap.get(clonedRecordItem.itemDate);
			if (exchangeRate != null) {
				// Regardless of auto or manual, original exchange should always reflect the rate on that day
				clonedRecordItem.originalExchangeRate = exchangeRate;
				// If there is exchange rate, and the rate is different, use the new exchange rate.
				// If it's the same as current rate, then do nothing.
				if (clonedRecordItem.exchangeRateManual != True && clonedRecordItem.exchangeRate != exchangeRate) {
					autoUpdated = true;
					clonedRecordItem.exchangeRate = exchangeRate;
					clonedRecordItem.amount = (clonedRecordItem.localAmount * clonedRecordItem.exchangeRate)
						.setScale(decimalPlaces, RoundingMode.FLOOR);
				}
			} else {
				// When Exchange Rate is not found, use the exchange rate from source record item, but set Manual flag to true
				if (clonedRecordItem.exchangeRateManual != True) {
					// If record is originally not Manual, then change it to mark as manual, and also as updated record
					autoUpdated = true;
					clonedRecordItem.exchangeRateManual = true;
				}
				clonedRecordItem.originalExchangeRate = 0;		// Set to 0 since no original rate found
			}
		} else {
			// When Exchange Rate is not found, use the exchange rate from source record item, but set Manual flag to true
			if (clonedRecordItem.exchangeRateManual != True) {
				// If record is originally not Manual, then change it to mark as manual, and also as updated record
				autoUpdated = true;
				clonedRecordItem.exchangeRateManual = true;
			}
			clonedRecordItem.originalExchangeRate = 0;	// Set to 0 since no original rate found
		}

		return autoUpdated;
	}

	/*
	 * Process the Tax Rate related data of the newly cloned Record
	 *
	 * @param clonedRecordItem Cloned Record Item
	 * @param baseTargetHistoryMap Map containing Tax History for a given Tax base at a given date
	 * @param decimalPlaces Number of Decimal places to use
	 *
	 * @return true if the Cloned Record has been marked as updated
	 */
	public Boolean processTaxRateDataForRecordCloning(ExpRecordItem clonedRecordItem,
			Map<Id, Map<AppDate, ExpTaxTypeHistoryEntity>> baseTargetHistoryMap, Integer decimalPlaces) {
		Boolean autoUpdated = false;
		ExpTaxTypeHistoryEntity taxTypeHistoryEntity = baseTargetHistoryMap.get(clonedRecordItem.taxTypeBaseId).get(clonedRecordItem.itemDate);
 		// If the current Tax History Id is different from the Tax History to use for the given date, update. Otherwise, do nothing.
		if (clonedRecordItem.taxTypeHistoryId != taxTypeHistoryEntity.id) {
			clonedRecordItem.taxTypeHistoryId = taxTypeHistoryEntity.id;

			// If Tax amount isn't set manually by user, then recalculate automatically (Only if rate is different).
			if (clonedRecordItem.taxManual != true && clonedRecordItem.taxRate != taxTypeHistoryEntity.rate) {
				autoUpdated = true;
				clonedRecordItem.taxRate = taxTypeHistoryEntity.rate;
				final Decimal calTaxRate = clonedRecordItem.taxRate + 100;

				clonedRecordItem.gstVat = ((clonedRecordItem.amount / calTaxRate) * clonedRecordItem.taxRate) .setScale(decimalPlaces, RoundingMode.FLOOR);

				clonedRecordItem.withoutTax = clonedRecordItem.amount - clonedRecordItem.gstVat;
			}
		}
		return autoUpdated;
	}
}