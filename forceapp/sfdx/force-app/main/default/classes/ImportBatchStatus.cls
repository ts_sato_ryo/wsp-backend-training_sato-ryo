/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * インポートバッチステータス
 */
public with sharing class ImportBatchStatus extends ValueObjectType {

	/** 未処理 */
	public static final ImportBatchStatus WAITING = new ImportBatchStatus('Waiting');
	/** 実行中 */
	public static final ImportBatchStatus PROCESSING = new ImportBatchStatus('Processing');
	/** 完了 */
	public static final ImportBatchStatus COMPLETED = new ImportBatchStatus('Completed');
	/** 失敗 */
	public static final ImportBatchStatus FAILED = new ImportBatchStatus('Failed');

	/** インポートバッチステータスのリスト */
	public static final List<ImportBatchStatus> STATUS_LIST;
	static {
		STATUS_LIST = new List<ImportBatchStatus> {
			WAITING,
			PROCESSING,
			COMPLETED,
			FAILED
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private ImportBatchStatus(String value) {
		super(value);
	}

	/**
	 * 値からImportBatchStatusを取得する
	 * @param value 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つImportBatchStatus、ただし該当するImportBatchStatusが存在しない場合はnull
	 */
	public static ImportBatchStatus valueOf(String value) {
		ImportBatchStatus retType = null;
		if (String.isNotBlank(value)) {
			for (ImportBatchStatus type : STATUS_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// ImportBatchStatus以外の場合はfalse
		Boolean eq;
		if (compare instanceof ImportBatchStatus) {
			// 値が同じであればtrue
			if (this.value == ((ImportBatchStatus)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}