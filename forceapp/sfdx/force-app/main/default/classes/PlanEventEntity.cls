/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group プランナー
 *
 * プランイベントのエンティティ
 * 現状使用していない項目は、一旦非公開プロパティとしている
 */
public with sharing class PlanEventEntity extends Entity implements Comparable {
	/** 件名の最大文字数 Longest possible length for Subject */
	public static final Integer SUBJECT_MAX_LENGTH = 255;
	/** 場所の最大文字数 Longest possible length for Place */
	public static final Integer LOCATION_MAX_LENGTH = 255;
	/** 説明の最大文字数 Longest possible length for Description */
	public static final Integer DESCRIPTION_MAX_LENGTH = 4096;

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		OwnerId,
		Name,
		Subject,
		StartDateTime,
		EndDateTime,
		IsAllDay,
		IsOuting,
		Location,
		Job,
		WorkCategory,
		ContactId,
		ContactName,
		Description,
		IsReadonly,
		IsOrganizer,
		OrganizerEventId,
		ResponseStatus,
		ResponseComment,
		IsRecurrence,
		RecurrenceEventId,
		OriginalStartDateTime,
		RecurrenceType,
		RecurrenceInterval,
		RecurrenceMonthOfYear,
		RecurrenceDayOfMonth,
		RecurrenceInstance,
		RecurrenceDayOfWeekMask,
		RecurrenceStartDateTime,
		RecurrenceEndDateOnly,
		RecurrenceCount,
		SfEventId,
		SfLastModifiedDate,
		SfRecurrenceEventId,
		SfRecurrenceHash,
		CreatedServiceBy,
		LastModifiedService,
		LastModifiedDateTime,
		ExternalEventId
	}

	// TODO 以下は項目定義以外で使用していないため、とりあえずenumとしているが、
	// 使用するケースに合わせて、VOへの変更を検討する

	/** 返答ステータス */
	public enum ResponseStatus {
		// New : 未返答 予約後のため使用する場合は、VO型に変更する
		Declined,	// 辞退
		Tentative,	// 仮の予定
		Accepted	// 承認
	}

	/** 繰り返し種別 */
	public enum RecurrenceType {
		RecursDaily,		// 毎日
		RecursEveryWeekday,	// 平日
		RecursWeekly,		// 毎週N曜日
		RecursMonthlyNth,	// 毎月第M N曜日
		RecursMonthly,		// 毎月N日
		RecursYearlyNth,	// 毎年L月第M N曜日
		RecursYearly		// 毎年M月N日
	}

	/** サービス種別 */
	public enum ServiceType {
		teamspirit,
		google,
		salesforce,
		office365
	}
	public ServiceType valueOfServiceType(String name) {
		for (ServiceType type : ServiceType.values()) {
			if (name.equals(type.name())) {
				return type;
			}
		}
		throw new App.IllegalStateException(name);
	}

	/**
	 * コンストラクタ
	 */
	public PlanEventEntity() {
		changedFieldSet = new Set<Field>();
	}

	/** 所有者 */
	public Id ownerId {
		get;
		set {
			ownerId = value;
			setChanged(Field.OwnerId);
		}
	}

	/** 予定番号 */
	public String name {
		get;
		set {
			name = value;
			setChanged(Field.Name);
		}
	}

	/** 件名 */
	public String subject {
		get;
		set {
			subject = value;
			setChanged(Field.Subject);
		}
	}

	/** 開始日時 */
	public AppDatetime startDateTime {
		get;
		set {
			startDateTime = value;
			setChanged(Field.StartDateTime);
		}
	}

	/** 終了日時 */
	public AppDatetime endDateTime {
		get;
		set {
			endDateTime = value;
			setChanged(Field.EndDateTime);
		}
	}

	/** 終日予定 */
	public Boolean isAllDay {
		get;
		set {
			isAllDay = value;
			setChanged(Field.IsAllDay);
		}
	}

	/** 外出 */
	public Boolean isOuting {
		get;
		set {
			isOuting = value;
			setChanged(Field.IsOuting);
		}
	}

	/** 場所 */
	public String location {
		get;
		set {
			location = value;
			setChanged(Field.Location);
		}
	}

	/** ジョブ */
	public JobEntity job {
		get;
		set {
			job = value;
			setChanged(Field.Job);
		}
	}
	public void setJobId(Id id) {
		setChanged(Field.Job);
		if (id == null) {
			return;
		}
		if (job == null) {
			job = new JobEntity();
		}
		job.setId(id);
	}

	/** 作業分類 */
	public WorkCategoryEntity workCategory {
		get;
		set {
			workCategory = value;
			setChanged(Field.WorkCategory);
		}
	}
	public void setWorkCategoryId(Id id) {
		setChanged(Field.WorkCategory);
		if (id == null) {
			return;
		}
		if (workCategory == null) {
			workCategory = new WorkCategoryEntity();
		}
		workCategory.setId(id);
	}

	/** 取引先責任者ID */
	public Id contactId {
		get;
		set {
			contactId = value;
			setChanged(Field.ContactId);
		}
	}

	/** 取引先責任者名 */
	public String contactName {
		get;
		set {
			contactName = value;
			setChanged(Field.ContactName);
		}
	}

	/** 説明 */
	public String description {
		get;
		set {
			description = value;
			setChanged(Field.Description);
		}
	}

	/** 参照のみ */
	public Boolean isReadOnly {
		get;
		set {
			isReadOnly = value;
			setChanged(Field.IsReadOnly);
		}
	}

	/** 主催者 */
	public Boolean isOrganizer {
		get;
		set {
			isOrganizer = value;
			setChanged(Field.IsOrganizer);
		}
	}

	/** 主催者予定 */
	public Id organizerEventId {
		get;
		set {
			organizerEventId = value;
			setChanged(Field.OrganizerEventId);
		}
	}

	/** 返答ステータス */
	public ResponseStatus responseStatus {
		get;
		set {
			responseStatus = value;
			setChanged(Field.ResponseStatus);
		}
	}

	/** 返答コメント */
	public String responseComment {
		get;
		set {
			responseComment = value;
			setChanged(Field.ResponseComment);
		}
	}

	/** 定期的な予定 */
	public Boolean isRecurrence {
		get;
		set {
			isRecurrence = value;
			setChanged(Field.IsRecurrence);
		}
	}

	/** 繰り返し予定 */
	private Id recurrenceEventId {
		get;
		set {
			recurrenceEventId = value;
			setChanged(Field.RecurrenceEventId);
		}
	}

	/** 元の開始日時 */
	private String originalStartDateTime {
		get;
		set {
			originalStartDateTime = value;
			setChanged(Field.OriginalStartDateTime);
		}
	}

	/** 繰り返し種別 */
	private RecurrenceType recurrenceType {
		get;
		set {
			recurrenceType = value;
			setChanged(Field.RecurrenceType);
		}
	}

	/** 繰り返し間隔 */
	private Integer recurrenceInterval {
		get;
		set {
			recurrenceInterval = value;
			setChanged(Field.RecurrenceInterval);
		}
	}

	/** 繰り返し月 */
	private String recurrenceMonthOfYear {
		get;
		set {
			recurrenceMonthOfYear = value;
			setChanged(Field.RecurrenceMonthOfYear);
		}
	}

	/** 繰り返し日 */
	private Integer recurrenceDayOfMonth {
		get;
		set {
			recurrenceDayOfMonth = value;
			setChanged(Field.RecurrenceDayOfMonth);
		}
	}

	/** 繰り返し週 */
	private String recurrenceInstance {
		get;
		set {
			recurrenceInstance = value;
			setChanged(Field.RecurrenceInstance);
		}
	}

	/** 繰り返し曜日 */
	private Integer RecurrenceDayOfWeekMask {
		get;
		set {
			recurrenceDayOfWeekMask = value;
			setChanged(Field.RecurrenceDayOfWeekMask);
		}
	}

	/** 繰り返し開始日時 */
	private AppDatetime recurrenceStartDateTime {
		get;
		set {
			recurrenceStartDateTime = value;
			setChanged(Field.RecurrenceStartDateTime);
		}
	}

	/** 繰り返し終了日 */
	private AppDate recurrenceEndDateOnly {
		get;
		set {
			recurrenceEndDateOnly = value;
			setChanged(Field.RecurrenceEndDateOnly);
		}
	}

	/** 繰り返し回数 */
	private Integer recurrenceCount {
		get;
		set {
			recurrenceCount = value;
			setChanged(Field.RecurrenceCount);
		}
	}

	/** SalesforceイベントID */
	private String sfEventId {
		get;
		set {
			sfEventId = value;
			setChanged(Field.SfEventId);
		}
	}

	/** Salesforceイベント最終更新日時 */
	private String sfLastModifiedDate {
		get;
		set {
			sfLastModifiedDate = value;
			setChanged(Field.SfLastModifiedDate);
		}
	}

	/** Salesforce繰り返しイベントID */
	private String sfRecurrenceEventId {
		get;
		set {
			sfRecurrenceEventId = value;
			setChanged(Field.SfRecurrenceEventId);
		}
	}

	/** Salesforce繰り返しイベントハッシュ */
	private String sfRecurrenceHash {
		get;
		set {
			sfRecurrenceHash = value;
			setChanged(Field.SfRecurrenceHash);
		}
	}

	/** 作成元サービス */
	public ServiceType createdServiceBy {
		get;
		set {
			createdServiceBy = value;
			setChanged(Field.CreatedServiceBy);
		}
	}

	/** 最終更新サービス */
	public ServiceType lastModifiedService {
		get;
		set {
			lastModifiedService = value;
			setChanged(Field.LastModifiedService);
		}
	}

	/** 最終更新日時 */
	public AppDatetime lastModifiedDateTime {
		get;
		set {
			lastModifiedDateTime = value;
			setChanged(Field.LastModifiedDateTime);
		}
	}

	/** 外部カレンダーのイベントID */
	public String externalEventId {
		get;
		set {
			externalEventId = value;
			setChanged(Field.ExternalEventId);
		}
	}

	/** 値を変更した項目のリスト */
	private Set<Field> changedFieldSet;

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		changedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	/**
	 * インスタンスとパラメータの比較結果をIntegerで返却する
	 * @param 比較対象
	 * @return 比較結果（このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満）
	 */
	public Integer compareTo(Object compareTo) {

		Integer cpStartResult = this.startDateTime.compareTo(((PlanEventEntity)compareTo).startDateTime);
		Integer cpEndResult = this.endDateTime.compareTo(((PlanEventEntity)compareTo).endDateTime);

		// インスタンスの方が開始時刻が早い場合
		if (cpStartResult == -1) {
			return App.COMPARE_BEFORE;
		}
		// 開始時刻が同じ場合
		if (cpStartResult == 0) {
			// インスタンスの方が終了時刻が遅い場合
			if (cpEndResult == 1) {
				return App.COMPARE_BEFORE;
			// 開始時刻も終了時刻も同じ場合
			} else if (cpEndResult == 0) {
				return App.COMPARE_EQUAL;
			}
		}
		// インスタンスの方が開始時刻が遅い場合、終了時刻が早い場合
		return App.COMPARE_AFTER;
	}
}