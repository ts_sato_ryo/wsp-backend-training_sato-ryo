/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * AttResourcePermissionクラスのテスト
 */
@isTest
private class AttResourcePermissionTest {

	/** テストデータクラス */
	private class TestData extends TestData.TestDataEntity {
		private PermissionRepository permissionRepo = new PermissionRepository();

		public TestData() {
			super();
			// 明示的に社員データを作成しておく
			this.employee = this.employee;
			// 権限マスタを本クラスのテスト用に初期化しておく
			initPermission(this.permission);
		}

		/**
		 * 権限マスタを初期化(勤怠系の全ての権限をtrue)する
		 */
		public void initPermission(PermissionEntity permission) {
			permission.isViewAttTimeSheetByDelegate = true;
			permission.isEditAttTimeSheetByDelegate = true;
			permission.isSubmitAttDailyRequestByDelegate = true;
			permission.isCancelAttDailyRequestByDelegate = true;
			permission.isCancelAttDailyApprovalByDelegate = true;
			permission.isSubmitAttRequestByDelegate = true;
			permission.isCancelAttRequestByDelegate = true;
			permission.isCancelAttApprovalByDelegate = true;
			permission.isCancelAttApprovalByEmployee = true;
			permission.isCancelAttDailyApprovalByEmployee = true;

			permissionRepo.saveEntity(permission);
		}
	}

	/**
	 * 勤務表表示(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestViewAttTimeSheetByDelegate() {
		TestData testData = new TestData();
		User delegeteUser = ComTestDataUtility.createStandardUser('standard-user');
		EmployeeBaseEntity delegateEmployee = testData.createEmployee('Delegate', null, delegeteUser.Id);

		System.runAs(delegeteUser) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.VIEW_ATT_TIME_SHEET_BY_DELEGATE);

			// Test1: 勤務表表示(代理)の権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 勤務表表示(代理)の権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isViewAttTimeSheetByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務表編集(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestEditAttTimeSheetByDelegate() {
		TestData testData = new TestData();
		User delegeteUser = ComTestDataUtility.createStandardUser('standard-user');
		EmployeeBaseEntity delegateEmployee = testData.createEmployee('Delegate', null, delegeteUser.Id);

		System.runAs(delegeteUser) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.EDIT_ATT_TIME_SHEET_BY_DELEGATE);

			// Test1: 勤務表表示(代理)の権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 勤務表表示(代理)の権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isEditAttTimeSheetByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 各種勤怠申請申請・申請削除(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestSubmitAttDailyRequestByDelegate() {
		TestData testData = new TestData();
		EmployeeBaseEntity delegateEmployee = testData.createEmployeeWithStandardUser('Delegate');

		System.runAs(testData.createRunAsUserFromEmployee(delegateEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.SUBMIT_ATT_DAILY_REQUEST_BY_DELEGATE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isSubmitAttDailyRequestByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 各種勤怠申請申請取消(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestCancelAttDailyRequestByDelegate() {
		TestData testData = new TestData();
		EmployeeBaseEntity delegateEmployee = testData.createEmployeeWithStandardUser('Delegate');

		System.runAs(testData.createRunAsUserFromEmployee(delegateEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.CANCEL_ATT_DAILY_REQUEST_BY_DELEGATE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isCancelAttDailyRequestByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 各種勤怠申請承認取消(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestCancelAttDailyApprovalByDelegate() {
		TestData testData = new TestData();
		EmployeeBaseEntity delegateEmployee = testData.createEmployeeWithStandardUser('Delegate');

		System.runAs(testData.createRunAsUserFromEmployee(delegateEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.CANCEL_ATT_DAILY_APPROVAL_BY_DELEGATE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isCancelAttDailyApprovalByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務確定申請申請(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestSubmitAttRequestByDelegate() {
		TestData testData = new TestData();
		EmployeeBaseEntity delegateEmployee = testData.createEmployeeWithStandardUser('Delegate');

		System.runAs(testData.createRunAsUserFromEmployee(delegateEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.SUBMIT_ATT_REQUEST_BY_DELEGATE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isSubmitAttRequestByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務確定申請申請取消(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestCancelAttRequestByDelegate() {
		TestData testData = new TestData();
		EmployeeBaseEntity delegateEmployee = testData.createEmployeeWithStandardUser('Delegate');

		System.runAs(testData.createRunAsUserFromEmployee(delegateEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.CANCEL_ATT_REQUEST_BY_DELEGATE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isCancelAttRequestByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務確定申請承認取消(代理)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestCancelAttApprovalByDelegate() {
		TestData testData = new TestData();
		EmployeeBaseEntity delegateEmployee = testData.createEmployeeWithStandardUser('Delegate');

		System.runAs(testData.createRunAsUserFromEmployee(delegateEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.CANCEL_ATT_APPROVAL_BY_DELEGATE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isCancelAttApprovalByDelegate = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, testData.employee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務確定申請承認取消(本人)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestCancelAttApprovalByEmployee() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard-user');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.CANCEL_ATT_APPROVAL_BY_EMPLOYEE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isCancelAttApprovalByEmployee = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 各種勤怠申請承認取消(本人)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestCancelAttDailyApprovalByEmployee() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard-user');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.CANCEL_ATT_DAILY_APPROVAL_BY_EMPLOYEE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isCancelAttDailyApprovalByEmployee = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 勤務確定申請自己承認(本人)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestApproveSelfAttRequestByEmployee() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard-user');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.APPROVE_SELF_ATT_REQUEST_BY_EMPLOYEE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isApproveSelfAttRequestByEmployee = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}

	/**
	 * 各種勤怠申請自己承認(本人)の権限チェックが正しく行うことができることを確認する
	 */
	@isTest static void hasExecutePermissionTestApproveSelfAttDailyRequestByEmployee() {
		TestData testData = new TestData();
		EmployeeBaseEntity standardEmployee = testData.createEmployeeWithStandardUser('standard-user');

		System.runAs(testData.createRunAsUserFromEmployee(standardEmployee)) {
			App.NoPermissionException actEx;
			List<AttResourcePermission.Permission> requiredPermissionList
					= new List<AttResourcePermission.Permission>();
			requiredPermissionList.add(AttResourcePermission.Permission.APPROVE_SELF_ATT_DAILY_REQUEST_BY_EMPLOYEE);

			// Test1: 権限を持っている場合、例外は発生しない
			actEx = null;
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertEquals(null, actEx);

			// Test2: 権限を持っていない場合、例外が発生する
			actEx = null;
			EmployeePermissionService.clearPermissionMap();
			testData.permission.isApproveSelfAttDailyRequestByEmployee = false;
			new PermissionRepository().saveEntity(testData.permission);
			try {
				new AttResourcePermission().hasExecutePermission(requiredPermissionList, standardEmployee.id);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			System.assertNotEquals(null, actEx);
			System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
		}
	}
}