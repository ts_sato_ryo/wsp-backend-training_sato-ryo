/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * 会社非公開設定
 */
public with sharing class CompanyPrivateSettingEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		COMPANY_ID,
		CRYPTO_KEY,
		OFFICE365_AUTH_CODE
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;
	
	/**
	 * コンストラクタ
	 */
	public CompanyPrivateSettingEntity() {
		isChangedFieldSet = new Set<Field>();
	}
	
	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}

	/** 会社ID */
	public String companyId {
		get;
		set {
			companyId = value;
			setChanged(Field.COMPANY_ID);
		}
	}
	
	/** 暗号化キー */
	public String cryptoKey {
		get;
		set {
			cryptoKey = value;
			setChanged(Field.CRYPTO_KEY);
		}
	}
	
	/** Office365 API認証キー */
	public String office365AuthCode {
		get;
		set {
			office365AuthCode = value;
			setChanged(Field.OFFICE365_AUTH_CODE);
		}
	}
}