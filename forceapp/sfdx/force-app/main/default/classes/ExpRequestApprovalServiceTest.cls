/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Test class for ExpRequestApprovalService
 */
@isTest
private class ExpRequestApprovalServiceTest {
	
	/**
	 * Test for submitting expense request
	 */
	@isTest static void submitRequestTest() {
		ExpTestData testData = new ExpTestData();
		ExpRequestEntity report = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRequestRecordEntity> recordList = testData.createExpRequestRecords(report.id, Date.today(), testData.expType, testData.employee, testData.job, 3);
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(2);

		// Create reportType and expType Link
		testData.linkExpReportTypeExpTypeLinkEntity(report.expReportTypeId, new Set<String>{testData.expType.Id});

		// Create receipt
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		ContentVersion contentVersion = new ContentVersion(
			Title = 'Test',
			PathOnClient = 'Test.jpg',
			VersionData = Blob.valueOf(bodyString));
		insert contentVersion;
		ContentDocument contentDocument = [SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Title = :contentVersion.Title LIMIT 1];
		ContentDocumentLink link = new ContentDocumentLink(
			ContentDocumentId = contentDocument.Id,
			LinkedEntityId = recordList[0].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		insert link;

		// Set manager to employee
		testData.employee.getHistory(0).managerId = testEmployeeList[0].id;
		new EmployeeRepository().saveEntity(testData.employee);

		// Application user
		User user = [SELECT Id FROM User WHERE Id = :testEmployeeList[1].userId LIMIT 1];

		Test.startTest();

		String comment = 'Comment';

		System.runAs(user) {
			ExpRequestApprovalService.submitRequest(report.id, comment);
		}

		Test.stopTest();

		// Get request approval record
		ExpRequestApproval__c requestObj = [
				SELECT
					Id,
					Name,
					ExpRequestId__c,
					TotalAmount__c,
					RequestNo__c,
					Subject__c,
					Comment__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					Approver01Id__c
				FROM ExpRequestApproval__c
				WHERE ExpRequestId__c = :report.id];

		String requestName = new ExpService().createRequestName(report.requestNo, report.totalAmount, report.employeeNameL,
				report.subject, AppLanguage.valueOf(testData.company.Language__c));
		System.assertEquals(requestName, requestObj.Name);
		System.assertEquals(report.id, requestObj.ExpRequestId__c);
		System.assertEquals(report.totalAmount, requestObj.TotalAmount__c);
		System.assertEquals(report.subject, requestObj.Subject__c);
		System.assertEquals(comment, requestObj.Comment__c);
		System.assertEquals(AppRequestStatus.PENDING.value, requestObj.Status__c);
		System.assertEquals(false, requestObj.ConfirmationRequired__c);
		System.assertEquals(null, requestObj.CancelType__c);
		System.assertEquals(report.employeeHistoryId, requestObj.EmployeeHistoryId__c);
		System.assertEquals(report.departmentHistoryId, requestObj.DepartmentHistoryId__c);
		System.assertEquals(testEmployeeList[1].getHistory(0).id, requestObj.ActorHistoryId__c);
		System.assertEquals(testEmployeeList[0].userId, requestObj.Approver01Id__c);

		// Confirm if a file is attached to expense request
		AppAttachmentRepository attachRepo = new AppAttachmentRepository();
		List<AppAttachmentEntity> attaches = attachRepo.getEntityListByParentId(requestObj.Id);
		System.assertEquals(1, attaches.size());
		AppAttachmentEntity attach = attaches[0];
		System.assertEquals(requestObj.Name, attach.name);
		System.assertEquals(requestObj.Id, attach.parentId);
		// Content of attchment
		ExpRequestService.ExpRequest jsonData = (ExpRequestService.ExpRequest)JSON.deserialize(attach.bodyText, ExpRequestService.ExpRequest.class);
		/** TODO: Create more comprehensive test for Expense Request values */
		// ExpRequest validation
		System.assertEquals(report.requestNo, jsonData.reportNo);
		System.assertEquals(report.subject, jsonData.subject);
		System.assertEquals(report.expReportTypeName, jsonData.expReportTypeName);
		System.assertEquals(report.getExtendedItemTextId(0), jsonData.extendedItemText01Id);
		System.assertNotEquals(null, jsonData.extendedItemText01Info);
		System.assertEquals(report.getExtendedItemTextValue(0), jsonData.extendedItemText01Value);
		System.assertEquals(report.getExtendedItemTextId(9), jsonData.extendedItemText10Id);
		System.assertNotEquals(null, jsonData.extendedItemText10Info);
		System.assertEquals(report.getExtendedItemTextValue(9), jsonData.extendedItemText10Value);
		// ExpRequest Record and Record Item validation
		for (Integer i = 0; i < recordList.size(); i++) {
			ExpRequestRecordEntity expectedRecord = recordList[i];
			ExpRequestService.ExpRequestRecord actualRecord = jsonData.records[i];

			System.assertEquals(expectedRecord.Id, actualRecord.recordId);

			for (Integer itemIndex = 0; itemIndex < expectedRecord.recordItemList.size(); itemIndex++) {
				ExpRequestRecordItemEntity expectedItem = expectedRecord.recordItemList[itemIndex];
				ExpRequestService.ExpRequestRecordItem actualItem = actualRecord.items[itemIndex];

				System.assertEquals(expectedItem.Id, actualItem.itemId);
				System.assertEquals(expectedItem.expTypeId, actualItem.expTypeId);
				System.assertEquals(expectedItem.getExtendedItemTextId(0), actualItem.extendedItemText01Id);
				System.assertNotEquals(null, actualItem.extendedItemText01Id);
				System.assertEquals(expectedItem.getExtendedItemTextValue(0), actualItem.extendedItemText01Value);
				System.assertEquals(expectedItem.getExtendedItemTextId(9), actualItem.extendedItemText10Id);
				System.assertNotEquals(null, actualItem.extendedItemText10Id);
				System.assertEquals(expectedItem.getExtendedItemTextValue(9), actualItem.extendedItemText10Value);
			}
		}

		// get request record
		ExpRequest__c request = [
				SELECT
					Id,
					Name,
					RequestNo__c
				FROM ExpRequest__c
				WHERE Id =: report.id];
			
		System.assertEquals('pre-req00000001', request.requestNo__c);
		System.assertEquals(request.RequestNo__c, requestObj.RequestNo__c);

		// Confirm if receipt is related to request approval record
		ContentDocumentLink linkObj = [
			SELECT Id
			FROM ContentDocumentLink
			WHERE ContentDocumentId = :contentDocument.Id
			AND LinkedEntityId = :requestObj.Id
			LIMIT 1];

		System.assertNotEquals(null, linkObj);
	}

	/**
	 * Test for submitting expense request
	 * Ensures ExpRequestApproval is shared with delegate approvers
	 */
	@isTest static void submitRequestTestSharing() {
		ExpTestData testData = new ExpTestData();
		ExpRequestEntity request = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<EmployeeBaseEntity> testEmployeeList = testData.createTestEmployees(4);

		// Set managers for employees
		testData.employee.getHistory(0).managerId = testEmployeeList[0].id;
		new EmployeeRepository().saveEntity(testData.employee);

		// Set delegated approvers
		List<EmployeeBaseEntity> delegatedApproverList = new List<EmployeeBaseEntity>{testEmployeeList[1], testEmployeeList[2], testEmployeeList[3]};
		for (Integer i = 0; i < delegatedApproverList.size(); i++) {
			ComDelegatedApproverSetting__c setting = ComTestDataUtility.createDelegatedApproverSetting(testData.employee.id, delegatedApproverList[i].id);
		}

		Test.startTest();

		ExpRequestApprovalService.submitRequest(request.id, null);

		Test.stopTest();
		// Check approval is shared with delegated approvers
		ExpRequestApproval__c requestObj = [SELECT Id FROM ExpRequestApproval__c WHERE ExpRequestId__c = :request.id];
		List<AppShareEntity> shareList = new ExpRequestApprovalRepository().getShareEntityListByApprovalId(new List<Id>{requestObj.Id});
		Map<Id, AppShareEntity> shareMapByApproverId = new Map<Id, AppShareEntity>();
		for (AppShareEntity share : shareList) {
			shareMapByApproverId.put(share.userOrGroupId, share);
		}
		for (Integer i = 0; i < delegatedApproverList.size(); i++) {
			AppShareEntity share = shareMapByApproverId.get(delegatedApproverList[i].userId);
			//System.assertEquals(requestObj.Id, share.parentId);
		}
	}
	
	/**
	 * Test for submitting expense request
	 * Confirm error occurs when expense request to be submitted is not found
	 */
	@isTest static void submitRequestTestRequestNotFound() {
		ExpTestData testData = new ExpTestData();
		ExpRequestEntity report = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];

		// Delete expense request
		new ExpRequestRepository().deleteEntity(report);

		Test.startTest();

		try {
			ExpRequestApprovalService.submitRequest(report.id, null);
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NOT_FOUND_EXP_REPORT, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Test for submitting expense request
	 * Confirm expense request is submiited with company default language
	 */
	@isTest static void submitRequestTestLang() {
		ExpTestData testData = new ExpTestData();
		List<ExpRequestEntity> reportList = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 3);

		// Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		Test.startTest();

		// Company default language is Japanese
		ExpRequestApprovalService.submitRequest(reportList[0].id, 'Submission 1 JA');

		// Company default language is English
		testData.company.Language__c = AppLanguage.EN_US.value;
		update testData.company;
		ExpRequestApprovalService.submitRequest(reportList[1].id, 'Submission  EN_US');

		Test.stopTest();

		ExpRequestRepository reportRepo = new ExpRequestRepository();
		ExpRequestApprovalRepository requestRepo = new ExpRequestApprovalRepository();

		// Confirm expense requests
		// Japanese
		ExpRequestEntity reportEntity1 = reportRepo.getEntity(reportList[0].id);
		ExpRequestApprovalEntity requestEntity1 = requestRepo.getEntity(reportEntity1.expRequestApprovalId);
		System.assertNotEquals(null, requestEntity1);
		String fullName = reportList[0].employeeNameL.getFullName(AppLanguage.JA);
		String requestName = reportList[0].requestNo + ' ' + reportList[0].totalAmount.setScale(0) + ' ' + fullName + 'さん ' + reportList[0].subject;
		System.assertEquals(requestName, requestEntity1.name);
		// English
		ExpRequestEntity reportEntity2 = reportRepo.getEntity(reportList[1].id);
		ExpRequestApprovalEntity requestEntity2 = requestRepo.getEntity(reportEntity2.expRequestApprovalId);
		System.assertNotEquals(null, requestEntity2);
		fullName = reportList[1].employeeNameL.getFullName(AppLanguage.EN_US);
		requestName = reportList[1].requestNo + ' ' + reportList[1].totalAmount.setScale(0) + ' ' + fullName + ' ' + reportList[1].subject;
		System.assertEquals(requestName, requestEntity2.name);

		// In case company default language is not set
		// Throw exception
		try {
			testData.company.Language__c = null;
			update testData.company;

			ExpRequestApprovalService.submitRequest(reportList[2].id, 'Submission 3 language is not set');
		} catch (App.IllegalStateException e) {
			System.assertEquals(App.ERR_CODE_NOT_SET_COMPANY_LANG, e.getErrorCode());
		} catch (Exception e) {
			System.debug('例外 : ' + e.getStackTraceString());
			System.assert(false, '想定外の例外が発生しました。' + e.getMessage());
		}
	}

	/**
	 * Test for createRequestName
	 */
	@isTest static void createRequestNameTest() {
		ExpTestData testData = new ExpTestData();
		ExpRequestEntity report = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 1)[0];
		List<ExpRequestRecordEntity> recordList = testData.createExpRequestRecords(report.id, Date.today(), testData.expType, testData.employee, testData.job, 3);

		ExpRequestService service = new ExpRequestService();
		String requestName;

		// In case of Japanese
		requestName = new ExpService().createRequestName(report.requestNo, report.totalAmount, report.employeeNameL, report.subject, AppLanguage.JA);
		String fullName = report.employeeNameL.getFullName(AppLanguage.JA);
		System.assertEquals(report.requestNo + ' ' + report.totalAmount.setScale(0) + ' ' + fullName + 'さん ' + report.subject, requestName);

		// In case of English
		requestName = new ExpService().createRequestName(report.requestNo, report.totalAmount, report.employeeNameL, report.subject, AppLanguage.EN_US);
		fullName = report.employeeNameL.getFullName(AppLanguage.EN_US);
		System.assertEquals(report.requestNo + ' ' + report.totalAmount.setScale(0) + ' ' + fullName + ' ' + report.subject, requestName);
	}

	/**
	 * Test for getting a list of requests need to be approved
	 */
	@isTest static void getRequestListTest() {
		ExpTestData testData = new ExpTestData();
		List<ExpRequestEntity> reportList = testData.createExpRequests(Date.today(), testData.department, testData.employee, testData.job, 3);

		// Set manager to employee
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		ExpRequestService service = new ExpRequestService();

		// Submit request
		ExpRequestApprovalService.isTestModeAndBypassExpRequestValidation = true;
		ExpRequestApprovalService.submitRequest(reportList[0].id, null);
		ExpRequestApprovalService.submitRequest(reportList[1].id, null);
		ExpRequestApprovalService.submitRequest(reportList[2].id, null);

		// Get request list
		List<ExpRequestApproval__c> requestObjList = [
				SELECT
					Id,
					Name,
					ExpRequestId__c,
					Comment__c,
					Status__c,
					ConfirmationRequired__c,
					CancelType__c,
					EmployeeHistoryId__c,
					RequestTime__c,
					DepartmentHistoryId__c,
					ActorHistoryId__c,
					Approver01Id__c
				FROM ExpRequestApproval__c
				ORDER BY RequestNo__c DESC];

		List<ID> requestIdList = new List<ID>();
		for (ExpRequestApproval__c requestObj : requestObjList) {
			requestIdList.add(requestObj.Id);
		}

		Test.startTest();

		List<ExpRequestApprovalEntity> requestEntityList = ExpRequestApprovalService.getRequestApprovalList(requestIdList);

		Test.stopTest();

		for (Integer i=0; i<requestEntityList.size(); i++) {
			System.assertEquals(requestObjList[i].Id, requestEntityList[i].id);
			System.assertEquals(requestObjList[i].Name, requestEntityList[i].name);
			System.assertEquals(requestObjList[i].ExpRequestId__c, requestEntityList[i].expRequestId);
			System.assertEquals(requestObjList[i].Comment__c, requestEntityList[i].comment);
			String status = requestEntityList[i].status == null ? null : requestEntityList[i].status.value;
			System.assertEquals(requestObjList[i].Status__c, status);
			System.assertEquals(requestObjList[i].ConfirmationRequired__c, requestEntityList[i].isConfirmationRequired);
			String cancelType = requestEntityList[i].cancelType == null ? null : requestEntityList[i].cancelType.value;
			System.assertEquals(requestObjList[i].CancelType__c, cancelType);
			System.assertEquals(requestObjList[i].EmployeeHistoryId__c, requestEntityList[i].employeeHistoryId);
			System.assertEquals(requestObjList[i].RequestTime__c, AppDatetime.convertDatetime(requestEntityList[i].requestTime));
			System.assertEquals(requestObjList[i].DepartmentHistoryId__c, requestEntityList[i].departmentHistoryId);
			System.assertEquals(requestObjList[i].ActorHistoryId__c, requestEntityList[i].actorHistoryId);
			System.assertEquals(requestObjList[i].Approver01Id__c, requestEntityList[i].approver01Id);
		}
	}
	
}