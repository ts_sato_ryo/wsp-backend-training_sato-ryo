/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠申請のトリガーハンドラー
 */
public with sharing class AttRequestTriggerHandler extends RequestTriggerHandler {
	public override Set<Id> getApprovedRequestIds() {
		return approvedRequestMap.keySet();
	}
	public override Set<Id> getDisabledRequestIds() {
		return disabledRequestMap.keySet();
	}
	public List<AttSummary__c> updatedAttSummaryList {
		get {
			List<AttSummary__c> retList = new List<AttSummary__c>();
			for (AttRequest__c disabledRequest : disabledRequestMap.values()) {
				if (disabledRequest.CancelType__c == AppCancelType.REJECTED.value ||
					disabledRequest.CancelType__c == AppCancelType.REMOVED.value ||
					disabledRequest.CancelType__c == AppCancelType.CANCELED.value){
					retList.add(new AttSummary__c(Id = disabledRequest.AttSummaryId__c, Locked__c = false, OutWorkAbsenceDays__c = null, Dirty__c = true));
				}
				else{
					retList.add(new AttSummary__c(Id = disabledRequest.AttSummaryId__c, Locked__c = false, OutWorkAbsenceDays__c = null));
				}
			}
			return retList;
		}
	}
	public List<AttRecord__c> updatedAttRecordList {get; private set;}
	private Map<Id, AttRequest__c> approvedRequestMap = new Map<id, AttRequest__c>();
	private Map<Id, AttRequest__c> disabledRequestMap = new Map<id, AttRequest__c>();

	public AttRequestTriggerHandler(Map<Id, AttRequest__c> oldMap, Map<Id, AttRequest__c> newMap) {
		super();
		this.updatedAttRecordList = new List<AttRecord__c>();
		this.init(oldMap, newMap);
	}
	// 承認/却下時の承認情報のセット処理(承認者、承認日時、コメントなど)
	public override void applyProcessInfo() {
		Set<Id> targetObjectIds = new Set<Id>();
		targetObjectIds.addAll(approvedRequestIds);
		targetObjectIds.addAll(disabledRequestIds);
		Map<Id, ProcessInstanceStep> processInfoMap = getProcessInfo(targetObjectIds);

		for(AttRequest__c request : approvedRequestMap.values()) {
			if (!processInfoMap.containsKey(request.id)) {
				continue;
			}
			ProcessInstanceStep pis = processInfoMap.get(request.id);
			request.LastApproveTime__c = pis.CreatedDate;
			request.ProcessComment__c = pis.Comments;
		}
		for(AttRequest__c request : disabledRequestMap.values()) {
			request.ConfirmationRequired__c = true;
			if (!processInfoMap.containsKey(request.id)) {
				continue;
			}
			ProcessInstanceStep pis = processInfoMap.get(request.id);
			// 却下
			if (pis.StepStatus == STEP_STATUS_REJECTED) {
				request.CancelType__c = AppCancelType.REJECTED.value;
				request.ProcessComment__c = pis.Comments;
			}
			// 申請取消
			else if (pis.StepStatus == STEP_STATUS_REMOVED) {
				request.CancelType__c = AppCancelType.REMOVED.value;
				request.CancelComment__c = pis.Comments;
			}
			// 承認取消
			else if (pis.StepStatus == STEP_STATUS_APPROVED ) {
				request.CancelType__c = AppCancelType.CANCELED.value;
				request.LastApproveTime__c = null;
				request.LastApproverId__c = null;
				request.ProcessComment__c = null;
			}
		}
	}
	// 却下/申請取消、承認取消時の勤怠明細のセット処理
	public void applyAttRecordInfo(final List<AttRecord__c> attRecordList) {
	}
	// 承認済み・無効になった勤怠申請Idを取得
	private void init(Map<Id, AttRequest__c> oldMap, Map<Id, AttRequest__c> newMap) {
		for (Id requestId : newMap.keySet()) {
			AttRequest__c attRequestNew = newMap.get(requestId);
			AttRequest__c attRequestOld = oldMap.get(requestId);
			// // 承認判定：ステータス→承認済み
			if (AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(attRequestNew.Status__c))
						&& !AppRequestStatus.APPROVED.equals(AppRequestStatus.valueOf(attRequestOld.Status__c))) {
				approvedRequestMap.put(attRequestNew.Id, attRequestNew);
			}
			else if (AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(attRequestNew.Status__c))
						&& !AppRequestStatus.DISABLED.equals(AppRequestStatus.valueOf(attRequestOld.Status__c))) {
				disabledRequestMap.put(attRequestNew.Id, attRequestNew);
			}
		}
	}
}