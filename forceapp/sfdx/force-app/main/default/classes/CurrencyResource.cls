/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Class having APIs for Currency object
 */
public with sharing class CurrencyResource {

	/**
	 * Currency record class
	 */
	abstract public class CurrencyAbs {
		/** Record ID */
		public String id;
		/** Currency Name */
		public String name;
		/** ISO Currency Code */
		public String isoCurrencyCode;
		/** Currency Name(lang0) */
		public String name_L0;
		/** Currency Name(lang1) */
		public String name_L1;
		/** Currency Name(lang2) */
		public String name_L2;
		/** Decimal Places */
		public Integer decimalPlaces;
		/** Currency Symbol */
		public String symbol;

		public CurrencyEntity createEntity(Map<String, Object> paramMap) {
			CurrencyEntity entity = new CurrencyEntity();

			entity.setId(this.id);
			if(paramMap.containsKey('name_L0')) {
				entity.nameL0 = this.name_L0;
				entity.name = this.name_L0;
			}
			if(paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;

			}
			if(paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;

			}
			if(paramMap.containsKey('isoCurrencyCode')) {
				entity.isoCurrencyCode = ComIsoCurrencyCode.valueOf(this.isoCurrencyCode);
				entity.code = this.isoCurrencyCode;
			}
			if(paramMap.containsKey('decimalPlaces')) {
				entity.decimalPlaces = this.decimalPlaces;

			}
			if(paramMap.containsKey('symbol')) {
				entity.symbol = this.symbol;
			}

			return entity;
		}

		public void setData(CurrencyEntity entity) {
			this.id = entity.id;
			this.name = entity.nameL.getValue();
			this.isoCurrencyCode = entity.isoCurrencyCode.value;
			this.name_L0 = entity.nameL0;
			this.name_L1 = entity.nameL1;
			this.name_L2 = entity.nameL2;
			this.decimalPlaces = entity.decimalPlaces;
			this.symbol = entity.symbol;
		}
	}

	/**
	 * @description Currency record class to create instance
	 */
	public class CurrencyRecord extends CurrencyAbs {

	}

	/**
	 * @description Record class of global pick list entry
	 */
	public class PicklistEntry {
		/** Label */
		public String label;
		/** Value */
		public String value;
	}

	/**
	 * @description Parameter for new creation and update
	 */
	public class UpsertParam extends CurrencyAbs implements RemoteApi.RequestParam {

	}

	/**
 	* @description Parameter for deletion
	 */
	public class DeleteParam implements RemoteApi.RequestParam {
		/** Record Id */
		public String id;
	}

	/**
   * @description Parameter for search
   */
	public class SearchParam implements RemoteApi.RequestParam {
		/** Record Id */
		public String id;
		/**
	   * Company ID
		 * If this company ID is specified in parameter, exclude the company base currency from result.
	   * */
		public String companyId;
	}

	/**
	 * @description Parameter for searching ISO Currency code
	 */
	public class SearchIsoCodeParam implements RemoteApi.RequestParam {
		/** Search key word of Currency Name Label */
		public String label;
	}

	/**
	 * @description Result of saving
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** ID of the created record */
		public String id;
	}

	/**
   * @description Result of search
   */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** Search result records */
		public CurrencyResource.CurrencyRecord[] records;
	}

	/**
	 * @description Result of search
	 */
	public class SearchIsoCodeResult implements RemoteApi.ResponseParam {
		/** Search result records */
		public CurrencyResource.PicklistEntry[] records;
	}

	/**
	 * @description API to create a new Currency record
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** Permission to execute api */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description Create a new Currency records
		 * @param  req Request parameter (CurrencyResource.UpsertParam)
		 * @return Response (CurrencyResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			UpsertParam param = (UpsertParam)req.getParam(UpsertParam.class ) ;

			validateParam(param);

			new ComResourcePermission().hasExecutePermission(requriedPermission);

			CurrencyEntity entity = param.createEntity(req.getParamMap());
			Repository.SaveResult result = CurrencyService.createCurrency(entity);

			SaveResult response = new SaveResult();
			response.id = result.details[0].id;

			return response;
		}

		// Validate params
		private void validateParam(UpsertParam param) {
			//ISO code is mandatory
			if (String.isEmpty(param.isoCurrencyCode)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('ISO Currency Code');
				throw ex;
			}
			// Name
			if (String.isEmpty(param.name_L0)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('Name');
				throw ex;
			}
		}

	}

	/**
	 * @description API to update a Currency record
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** Permission to execute api */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description Update a Currency records
		 * @param  req Request parameter (CurrencyResource.UpsertParam)
		 * @return Response null
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			UpsertParam param = (UpsertParam)req.getParam(UpsertParam.class ) ;

			validateParam(param);

			new ComResourcePermission().hasExecutePermission(requriedPermission);

			CurrencyEntity entity = param.createEntity(req.getParamMap());
			CurrencyService.updateCurrency(entity);

			return null;
		}

		// Validate params
		private void validateParam(UpsertParam param) {
			//ID is mandatory
			if (String.isEmpty(param.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('ID');
				throw ex;
			}
		}
	}

	/**
   * @description API to delte a Currency record
   */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** Permission to execute api */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_OVERALL_SETTING;

		/**
		 * @description Delete a Currency records
		 * @param  req Request parameter (CurrencyResource.DeleteParam)
		 * @return Response null
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			DeleteParam param = (DeleteParam)req.getParam(DeleteParam.class);

			validateParam(param);

			new ComResourcePermission().hasExecutePermission(requriedPermission);

			CurrencyService.deleteCurrency(param.id);

			return null;
		}

		// Validate params
		private void validateParam(DeleteParam param) {
			//ID is mandatory
			if (String.isEmpty(param.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('ID');
				throw ex;
			}
		}
	}

	/**
	 * @description API to search Currency records
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search Currency records
		 * @param  req Request parameter (CurrencyResource.SearchParam)
		 * @return Response (CurrencyResource.SearchResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SearchParam param = (SearchParam)req.getParam(SearchParam.class);

			if (String.isBlank(param.companyId)) {
				param.companyId = null;
			}

			List<CurrencyEntity> entityList = CurrencyService.searchCurrency(param.id, param.companyId);

			return makeResponse(entityList);
		}

		private SearchResult makeResponse(List<CurrencyEntity> entityList) {

			List<CurrencyResource.CurrencyRecord> records = new List<CurrencyResource.CurrencyRecord>();

			for (CurrencyEntity entity : entityList) {
				CurrencyResource.CurrencyRecord rec = new CurrencyResource.CurrencyRecord();
				rec.setData(entity);
				records.add(rec);
			}

			SearchResult result = new SearchResult();
			result.records = records;
			return result;
		}
	}

	/**
	 * @description API to search Currency records
	 */
	public with sharing class SearchIsoCurrencyCodeApi extends RemoteApi.ResourceBase {

		/**
		 * @description API to search ComIsoCurrencyCode pick list for IsoCurrencyCode field
		 * @param req Request parameter (CurrencyResource.SearchIsoCurrencyCodeApi)
		 * @return Response (CurrencyResource.SearchIsoCodeResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			String arg;
			if (!req.getParamMap().isEmpty()) {
				SearchIsoCodeParam param = (SearchIsoCodeParam)req.getParam(SearchIsoCodeParam.class);
				arg = param.label;
			} else {
				arg = '';
			}

			List<ComIsoCurrencyCode> pickList = ComIsoCurrencyCode.getTypeList();

			pickList = filterListByLabel(pickList, arg);

			return makeResponse(pickList);
		}

		/**
		 * Filter list by search parameter
		 */
		private List<ComIsoCurrencyCode> filterListByLabel(List<ComIsoCurrencyCode> pickList, String labelParam) {
			List<ComIsoCurrencyCode> result = new List<ComIsoCurrencyCode>();

			for (ComIsoCurrencyCode c : pickList) {
				// Set only contains parameter string
				if (c.label.contains(labelParam)) {
					result.add(c);
				}
			}
			return result;
		}

		private SearchIsoCodeResult makeResponse(List<ComIsoCurrencyCode> pickList) {

			List<PicklistEntry> records = new List<PicklistEntry>();

			for (ComIsoCurrencyCode c : pickList) {
				PicklistEntry entry = new PicklistEntry();
				entry.label = c.label;
				entry.value = c.value;
				records.add(entry);
			}

			SearchIsoCodeResult response = new SearchIsoCodeResult();
			response.records = records;
			return response;
		}
	}

}