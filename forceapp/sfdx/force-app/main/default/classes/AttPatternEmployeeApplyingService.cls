/**
 * @group 勤怠
 *
 * @description 勤務パターン社員適用サービス
 * 社員の勤怠明細に勤務パターンと日タイプを設定します。
 */
public with sharing class AttPatternEmployeeApplyingService implements Service {

	private static final ComMsgBase MESSAGE_L = ComMessage.msg();

	/**
	 * @description エラー情報
	 */
	public class Error {
		/** @description エラーメッセージ */
		public String message;
		public Error(String message) {
			this.message = message;
		}
	}

	/**
	 * @description 処理結果
	 */
	public class Result {
		/** @description エラー情報 */
		public List<Error> errorList {get; private set;}

		/**
		 * @description エラーを追加する
		 */
		public Result() {
			this.errorList = new List<Error>();
		}

		/**
		 * @description エラーを追加する
		 */
		public void addError(Error error) {
			errorList.add(error);
		}

		/**
		 * @description 成功している場合はtrue、そうでない場合はfalse
		 */
		public Boolean isSuccess {
			get {
				return this.errorList.isEmpty();
			}
		}
	}

	/**
	 * @description 適用可能な日タイプ(値を取得する時はparseDayType(String)を利用してください)
	 */
	private static final Map<String, AttDayType> ATT_APPLYING_DAY_TYPE_MAP;
	static {
		ATT_APPLYING_DAY_TYPE_MAP = new Map<String, AttDayType> {
				'W' => AttDayType.WORKDAY,			// 勤務日
				'H' => AttDayType.HOLIDAY,			// 休日
				'L' => AttDayType.LEGAL_HOLIDAY	// 法定休日
		};
	}

	/**
	 * @description 日タイプを取得する(大文字小文字を区別しない)
	 * @param importDayType インポートの日タイプ
	 * @return 該当する日タイプ、存在しない場合はnull
	 */
	@testVisible
	private AttDayType parseDayType(String importDayType) {
		String key = String.isBlank(importDayType) ? null : importDayType.toUpperCase();
		return ATT_APPLYING_DAY_TYPE_MAP.get(key);
	}

	/**
	 * 文字列からDate型のインスタンスを生成する(YYYY-MM-DD, ユーザのロケールの日付書式)
	 * @param 対象文字列
	 * @return 文字列から生成したDate
	 */
	@testVisible
	private AppDate parseDateForApplying(String stringValue) {
		if (stringValue == null) {
			return null;
		}

		Date dateValue;
		try {
			// YYYY-MM-DD
			dateValue = Date.valueOf(stringValue);
		} catch (Exception valudOfEx) {
			// ユーザのロケール
			dateValue = Date.parse(stringValue);
		}
		return AppDate.valueOf(dateValue);
	}

	/**
	 * @description 社員に勤務パターンを設定する
	 * (複数のエラーを返却したいため、エラーは例外ではなく結果オブジェクトで返却しています)
	 * @param pattarnApplyImport 適用情報
	 * @param companyId 適用対象の社員の会社ID
	 * @return 適用結果
	 */
	public Result applyPatternToEmployee(AttPatternApplyImportEntity pattarnApplyImport, Id companyId) {
		Result result = new Result();

		// 登録された適用データが正しいことを確認する
		result = validateImport(pattarnApplyImport);
		if (! result.isSuccess) {
			return result;
		}

		try {
			AttDayType dayType = parseDayType(pattarnApplyImport.dayType);
			AppDate targetDate = parseDateForApplying(pattarnApplyImport.shiftDate);

			// 社員を取得する
			EmployeeBaseEntity employee = getEmployee(pattarnApplyImport.employeeCode, companyId, targetDate);

			// 社員の勤務体系を取得する
			AttWorkingTypeBaseEntity workingType = getEmployeeWorkingType(employee, targetDate);

			// 勤務パターンを取得する
			AttPatternEntity pattern;
			if (String.isNotBlank(pattarnApplyImport.patternCode)) {
				pattern = getPattern(pattarnApplyImport.patternCode, companyId, targetDate);
				// patternId = pattern.id;

				// 社員の勤務体系で許可されている勤務パターンであることをチェック
				isAvaliablePattern(pattarnApplyImport.patternCode, workingType, targetDate);
			}

			// 勤怠サマリー取得(必要に応じて勤怠計算を実行)
			AttSummaryEntity summary = new AttAttendanceService().getSummary(employee.id, targetDate);
			// 勤怠データに勤務パターンと日タイプを設定する
			setPatternToAttendance(summary, pattern, dayType, targetDate);

		} catch (App.BaseException e) {
			result.addError(new Error(e.getMessage()));
		}

		return result;
	}

	/**
	 * @description 適用するデータが正しいことを確認する(個別の項目)
	 * @param pattarnApplyImport 適用情報
	 * @return 検証結果
	 */
	private Result validateImport(AttPatternApplyImportEntity pattarnApplyImport) {
		Result result = validateImportSimple(pattarnApplyImport);
		if (! result.isSuccess) {
			return result;
		}
		return validateImportComplex(pattarnApplyImport);
	}

	/**
	 * @description 適用するデータが正しいことを確認する(個別の項目)
	 * @param pattarnApplyImport 適用情報
	 * @return 検証結果
	 */
	private Result validateImportSimple(AttPatternApplyImportEntity pattarnApplyImport) {
		Result result = new Result();

		// 社員コードが設定されている
		if (String.isBlank(pattarnApplyImport.employeeCode)) {
			// メッセージ：社員コードが指定されていません
			result.addError(new Error(MESSAGE_L.Com_Err_NotSpecified(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyEmployeeCode})));
		}

		// 適用対象日が設定されてる
		if (String.isBlank(pattarnApplyImport.shiftDate)) {
			// メッセージ：日付が指定されていません
			result.addError(new Error(MESSAGE_L.Com_Err_NotSpecified(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyDate})));
		}

		// 適用対象日書式チェック
		if (String.isNotBlank(pattarnApplyImport.shiftDate)) {
			// YYYY-MM-DDまたは、ユーザのロケールの形式であればOK
			try {
				parseDateForApplying(pattarnApplyImport.shiftDate);
			} catch (Exception e) {
				// メッセージ：日付のフォーマットが正しくありません。
				result.addError(new Error(MESSAGE_L.Com_Err_InvalidFormat(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyDate})));
			}
		}

		// 日タイプチェック
		if (String.isNotBlank(pattarnApplyImport.dayType)) {
			if (parseDayType(pattarnApplyImport.dayType) == null) {
				// メッセージ：指定された日タイプが正しくありません。
				result.addError(new Error(MESSAGE_L.Com_Err_SpecifiedInvalid(new List<String>{MESSAGE_L.Att_Lbl_PatternApplyDayType})));
			}
		}

		return result;
	}

	/**
	 * @description 適用するデータが正しいことを確認する(複数項目)
	 * @param pattarnApplyImport 適用情報
	 * @return 検証結果
	 */
	private Result validateImportComplex(AttPatternApplyImportEntity pattarnApplyImport) {
		Result result = new Result();

		// 勤務パターンコードが指定されている場合は、日タイプが勤務日であることをチェックする
		if (String.isNotBlank(pattarnApplyImport.patternCode)) {
			if (parseDayType(pattarnApplyImport.dayType) != AttDayType.WORKDAY) {
				// メッセージ：勤務パターンを指定する場合は日タイプに勤務日を指定する必要があります。
				result.addError(new Error(MESSAGE_L.Att_Err_PatternApplyRequiedWorkDay));
			}
		}
		return result;
	}

	/**
	 * @description 指定された社員コードの社員を取得する
	 * @param employeeCode 社員コード
	 * @param companyId 会社ID
	 * @param targetDate 対象日
	 * @return 社員(すべての有効な履歴を持つ)
	 * @throws App.ParameterException 指定された社員が見つからない場合、社員が無効な場合
	 */
	private EmployeeBaseEntity getEmployee(String employeeCode, Id companyId, AppDate targetDate) {
		EmployeeRepository.SearchFilter filter = new EmployeeRepository.SearchFilter();
		filter.companyIds = new Set<Id>{companyId};
		filter.codes = new Set<String>{employeeCode};
		List<EmployeeBaseEntity> employeeList = new EmployeeRepository().searchEntity(filter);

		// 社員の存在チェック
		if (employeeList.isEmpty()) {
			// メッセージ：指定された社員が見つかりません。
			throw new App.ParameterException(MESSAGE_L.Att_Err_SpecifiedNotFound(new List<String>{MESSAGE_L.Com_Lbl_Employee}));
		}

		// 社員が有効であることをチェック
		EmployeeBaseEntity employee = employeeList[0];
		if (employee.getHistoryByDate(targetDate) == null) {
			// メッセージ：指定された社員は有効期間外です。
			throw new App.ParameterException(MESSAGE_L.Com_Err_SpecifiedOutOfValidPeirod(new List<String>{MESSAGE_L.Com_Lbl_Employee}));
		}

		return employee;
	}

	/**
	 * @description 指定された勤務パターンコードの勤務パターンを取得する
	 * @param patternCode 勤務パターンコード
	 * @param companyId 会社ID
	 * @param targetDate 対象日
	 * @return 勤務パターン
	 * @throws App.ParameterException 指定された勤務パターンが見つからない場合、勤務パターンが無効な場合
	 */
	private AttPatternEntity getPattern(String patternCode, Id companyId, AppDate targetDate) {
		AttPatternEntity pattern = new AttPatternRepository2().getPatternByCode(patternCode, companyId);

		// 勤務パターンの存在チェック
		if (pattern == null) {
			// メッセージ：指定された勤務パターンが見つかりません。
			throw new App.ParameterException(MESSAGE_L.Att_Err_SpecifiedNotFound(new List<String>{MESSAGE_L.Att_Lbl_WorkingPattern}));
		}

		// 勤務パターンが有効であることのチェック
		if (! pattern.isValid(targetDate)) {
			// メッセージ：指定された勤務パターンは有効期間外です。
			throw new App.ParameterException(MESSAGE_L.Com_Err_SpecifiedOutOfValidPeirod(new List<String>{MESSAGE_L.Att_Lbl_WorkingPattern}));
		}

		return pattern;
	}

	/**
	 * @description 社員の勤務体系を取得する
	 * @param employee 対象社員
	 * @param targetDate 対象日
	 * @return 勤務体系(すべての有効な履歴を持つ)
	 * @throws App.ParameterException 対象日に社員に勤務体系が指定されていない場合、勤務体系が無効な場合
	 */
	private AttWorkingTypeBaseEntity getEmployeeWorkingType(EmployeeBaseEntity employee, AppDate targetDate) {

		Id workingTypeId = employee.getHistoryByDate(targetDate).workingTypeId;
		if (workingTypeId == null) {
			// メッセージ：指定された社員に勤務体系が設定されていません。
			throw new App.ParameterException(MESSAGE_L.Com_Err_SpecifiedNotSet(
					new List<String>{MESSAGE_L.Com_Lbl_Employee, MESSAGE_L.Att_Lbl_WorkingType}));
		}

		AttWorkingTypeBaseEntity workingType = new AttWorkingTypeRepository().getEntity(workingTypeId);

		// 勤務体系の存在チェック
		if (workingType == null) {
			// メッセージ：指定された社員の勤務体系が見つかりませんでした。
			throw new App.ParameterException(MESSAGE_L.Com_Err_SpecifiedNotFoundAofB(
					new List<String>{MESSAGE_L.Com_Lbl_Employee, MESSAGE_L.Att_Lbl_WorkingType}));
		}

		// 勤務体系が有効であることのチェック
		if (workingType.getHistoryByDate(targetDate) == null) {
			// メッセージ：指定された社員の勤務体系は有効期間外です。
			throw new App.ParameterException(MESSAGE_L.Com_Err_SpecifiedOutOfValidPeirodAofB(
					new List<String>{MESSAGE_L.Com_Lbl_Employee, MESSAGE_L.Att_Lbl_WorkingType}));
		}

		return workingType;
	}

	/**
	 * @description 勤務体系で利用可能な勤務パターンであることをチェックする
	 * @param patternCode 対象の勤務パターン
	 * @param workingType 勤務体系
	 * @param targetDate 対象日
	 * @throws App.ParameterException 利用不可の勤務パターンの場合
	 */
	private void isAvaliablePattern(String patternCode, AttWorkingTypeBaseEntity workingType, AppDate targetDate) {
		Boolean isAvailable = workingType.getHistoryByDate(targetDate).isAvailablePattern(patternCode);
		if (! isAvailable) {
			// メッセージ：指定された勤務パターンは社員の勤務体系で許可されていません。
			throw new App.ParameterException(MESSAGE_L.Att_Err_SpecifiedPatternNotPermitted);
		}
	}

	/**
	 * @description 勤怠データに勤務パターンと日タイプを設定する
	 * @param pattern 適用する勤務パターン
	 * @param dayType 適用する日タイプ
	 * @param targetDate 対象日
	 */
	private void setPatternToAttendance(AttSummaryEntity summaryWithRecords, AttPatternEntity pattern, AttDayType dayType, AppDate targetDate) {
		AttSummaryRepository summaryRepo = new AttSummaryRepository();

		Savepoint sp = Database.setSavepoint();
		try {
			// 勤怠明細に設定して保存
			AttRecordEntity record = summaryWithRecords.getAttRecord(targetDate);
			record.patternId = pattern == null ? null : pattern.Id;
			record.dayType = dayType;
			summaryRepo.saveRecordEntity(record);

			// 強制的に勤怠計算を実行
			new AttCalcService().calcAttendance(summaryWithRecords.id);

			// 勤怠計算後の勤怠サマリーと明細を取得する
			AttSummaryEntity calcSummary = summaryRepo.getEntity(summaryWithRecords.id, false, false);
			AttRecordEntity calcRecord = summaryRepo.getRecordEntity(record.id);

			// 勤務体系
			AttWorkingTypeBaseEntity workingType = getWorkingTypeByHistoryId(calcSummary.workingTypeId);

			// 勤怠計算後の状態が正しいかどうかを検証する
			Validator.Result result = new AttValidator.ApplyingPatternValidator(calcSummary, calcRecord, pattern, workingType).validate();
			if (result.hasError()) {
				throw new App.IllegalStateException(result.getError(0).message);
			}
		} catch (Exception e) {
			System.debug(e.getMessage());
			System.debug(e.getStackTraceString());
			// 適用できなかった場合はロールバックする
			Database.rollback(sp);
			throw e;
		}
	}

	/**
	 * @description 勤務体系履歴IDを指定して勤務体系(ベース+履歴)を取得する
	 * @param historyId 取得対象の履歴ID
	 * @return 履歴が存在する場合は履歴とベース、存在しない場合はnull
	 */
	private AttWorkingTypeBaseEntity getWorkingTypeByHistoryId(Id historyId) {
		AttWorkingTypeRepository.SearchFilter filter = new AttWorkingTypeRepository.SearchFilter();
		filter.historyIds = new Set<Id>{historyId};
		List<AttWorkingTypeBaseEntity> workingTypes = new AttWorkingTypeRepository().searchEntity(filter);
		return workingTypes.isEmpty() ? null : workingTypes.get(0);
	}
}