// APIリクエストに関するクラスを定義する
// APIリクエストの共通仕様の定義クラスなので、ビジネスロジックの変更を理由に編集するのは禁止
public class RemoteApi {

	// API実装クラスの基本クラス
	public abstract class ResourceBase {
		// API実行メソッド
		public abstract RemoteApi.ResponseParam execute(Request req);
		// テストメソッドでは、パラメータを直接指定して実行できるようにする
		@TestVisible
		private RemoteApi.ResponseParam execute(RequestParam req_param){
			return execute(new RemoteApi.Request(null, req_param));
		}
		@TestVisible
		private RemoteApi.ResponseParam execute(Map<String, Object> rep_param_map){
			return execute(new RemoteApi.Request(null, rep_param_map));
		}
		// execute実行時に、自動でロールバックするかを返す
		// デフォルトはtrueとし、ロールバックしない場合は当メソッドをオーバーライドする
		public virtual Boolean isAutoRollBack() {
			return true;
		}
	}

	// 自動ロールバックをしないAPIの基底クラス
	// 手動でトランザクションを制御する場合は、当クラスのサブクラスとして実装する
	public abstract class NoRollbackResourceBase extends ResourceBase {
		public override Boolean isAutoRollBack() {
			return false;
		}
	}

	// リクエストパラメータを格納するクラス
	public class Request {
		// APIのパス
		private String path;
		// API実行パラメータ格納Map
		private Map<String, Object> paramMap;

		public Request(String path, Map<String, Object> param_map) {
			this.path = path;
			// NullPointerException防止のためnullチェックする
			this.paramMap = param_map != null ? param_map : new Map<String, Object>();
		}

		// 開発用 (開発者コンソール等から叩くときに使用)
		public Request(String path, RequestParam req) {
			this(path, (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(req)));
		}

		public String getPath() {
			return path;
		}

		public RequestParam getParam(Type classType) {
			try {
				return (RequestParam)JSON.deserialize(JSON.serialize(paramMap), classType);
			} catch (System.JSONException e) {
				System.debug('Request Parameter Class Name: ' + classType.getName());
				System.debug('Request Paremeter: ' + paramMap);
				System.debug('JSONException: ' + e);
				String errMsg = 'パラメータに不正な値が設定されています。';
				throw new App.TypeException(errMsg);
			} catch (Exception e) {
				String errMsg = 'リクエストパラメータの型が' + classType.getName() + 'クラスとマッチしていないか、' +
					classType.getName() + 'クラスが' + RequestParam.class.getName() + 'インターフェースを実装していません。';
				throw new App.TypeException(errMsg);
			}
		}

		public Map<String, Object> getParamMap() {
			return paramMap;
		}

	}

	// リクエストパラメータのインターフェース
	public interface RequestParam {}

	// レスポンスパラメータのインターフェース
	public interface ResponseParam {}


	/**
	 * APIレスポンス定義クラス
	 */

	// RemoteActionレスポンスの基本クラス
	public abstract class ResponseBase {
		public Boolean isSuccess;
	}

	// 成功レスポンスの形式を定義するクラス
	public class SuccessResponse extends ResponseBase {
		public RemoteApi.ResponseParam result;
		public SuccessResponse(RemoteApi.ResponseParam paramter){
			isSuccess = true;
			result = paramter;
		}
	}

	// 成功レスポンスの形式を定義するクラス (一括処理)
	public class SuccessBatchResponse extends ResponseBase {
		public RemoteApi.ResponseParam[] results;
		public SuccessBatchResponse(RemoteApi.ResponseParam[] paramters){
			isSuccess = true;
			results = paramters;
		}
	}

	// エラーレスポンスの形式を定義するクラス
	public class ErrorResponse extends ResponseBase {
		public Error error;
		public ErrorResponse(RemoteApi.Error error_paramter){
			isSuccess = false;
			error = error_paramter;
		}
	}

	// エラー定義クラス
	// エラーの詳細情報を格納する
	public class Error {
		public String errorCode;
		public String message;
		public String stackTrace;
	}

	/**
	 * APIのカスタム例外クラス
	 * 例外の種類を増やす場合は、ここでクラスを宣言＆追加していく
	 * #アプリケーション共通の例外はApp.clsに定義してください
	 * ・BaseExceptionを継承する必要がある
	 * ・名前は 〜Exception にする必要がある (Apexのルールより)
	 */

	// 基本クラス
	public abstract class BaseException extends Exception {
		// エラーコードを返すメソッド
		public abstract String getErrorCode();
	}

	// (サンプル) APIパスが存在しないエラーのクラス
	public class NotFoundException extends BaseException {
		// 固定のエラーコードを返す
		// エラーコードはユニークにすること
		public override String getErrorCode(){ return 'API_NOT_FOUND'; }
	}

	// (サンプル) BatchApi失敗エラーのクラス
	public class BatchException extends BaseException {
		public override String getErrorCode(){ return 'BATCH_FAILED'; }
	}

	/**
	 * API一括処理実装クラス
	 */

	/**
	 * API一括処理のリクエストパラメータを定義するクラス
	 */
	//@TestVisible
	public class BatchApiRequest implements RemoteApi.RequestParam {
		public RemoteApi.Request[] requests;  // サブリクエストの配列
	}

	/**
	 * API一括処理のレスポンスパラメータを定義するクラス
	 */
	//@TestVisible
	public class BatchApiResponse implements RemoteApi.ResponseParam {
		public RemoteApi.ResponseParam[] results;  // レスポンスの配列
	}

	public with sharing class BatchApi extends RemoteApi.ResourceBase {

		/**
		 * API一括処理を実行する。実行結果をリクエストの配列順に返す
		 * @param  parameter リクエストパラメータ
		 * @return レスポンスパラメータ
		 */
		public override ResponseParam execute (Request parameter) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			BatchApiRequest param = (BatchApiRequest)parameter.getParam(BatchApiRequest.class);
			Map<String, Object> paramMap = parameter.getParamMap();

			// 必須パラメータ未指定の場合はエラー
			if(!paramMap.containsKey('requests')){
				throw new App.TypeException('パラメータ "requests" を指定して下さい');
			}

			List<RemoteApi.ResponseParam> responses = new List<RemoteApi.ResponseParam>();
			for(RemoteApi.Request reqEntry:param.requests){
				String reqJSON = JSON.serialize(reqEntry);
				RemoteApi.ResponseBase res = RemoteApiRoute.execute(reqJSON);
				if (res.isSuccess) {
					responses.add( ((RemoteApi.SuccessResponse)res).result );
				} else {
					throw new BatchException( ((RemoteApi.ErrorResponse)res).error.message );
				}
			}

			// 成功レスポンスをセットする
			BatchApiResponse res = new BatchApiResponse();
			res.results = responses;
			return res;
		}

	}

}
