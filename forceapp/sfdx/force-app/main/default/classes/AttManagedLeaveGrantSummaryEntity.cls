/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇付与サマリのエンティティ
 */
public with sharing class AttManagedLeaveGrantSummaryEntity extends Entity{
	/** 項目の定義(変更管理で使用する) */
	public enum Field {
			SUMMARY_ID, GRANT_ID, DAYS_TAKEN, DAYS_LEFT, LEAVE_TYPE, DAYS_GRANTED, DAYS_EXPIRED, DAYS_ADJUSTED, COMMENT
	}
	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;
	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}
	public AttManagedLeaveGrantSummaryEntity() {
		isChangedFieldSet = new Set<Field>();
	}
	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}
	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}
	
	/** 休暇サマリ */
	public Id summaryId {
		get;
		set {
			summaryId = value;
			setChanged(Field.SUMMARY_ID);
		}
	}
	/** 付与Id */
	public Id grantId {
		get;
		set {
			grantId = value;
			setChanged(Field.GRANT_ID);
		}
	}
	/** 取得日数 */
	public AttDays daysTaken {
		get;
		set {
			daysTaken = value;
			setChanged(Field.DAYS_TAKEN);
		}
	}
	/** 残日数 */
	public AttDays daysLeft {
		get;
		set {
			daysLeft = value;
			setChanged(Field.DAYS_LEFT);
		}
	}
	/** 失効日数(確定分) */
	public AttDays daysExpired {
		get;
		set {
			daysExpired = value;
			setChanged(Field.DAYS_EXPIRED);
		}
	}
	/** 調整日数(確定分) */
	public AttDays daysAdjusted {
		get;
		set {
			daysAdjusted = value;
			setChanged(Field.DAYS_ADJUSTED);
		}
	}
	/** 休暇Id */
	public Id leaveId {get; set;}
	/** 休暇種別 */
	public AttLeaveType leaveType {
		get;
		set {
			leaveType = value;
			setChanged(Field.LEAVE_TYPE);
		}
	}
	/** 社員Id */
	public Id employeeId {get; set;}
	/** 付与数 */
	public AttDays daysGranted {
		get;
		set {
			daysGranted = value;
			setChanged(Field.DAYS_GRANTED);
		}
	}
	/** 付与日 */
	public AppDate validDateFrom {get; set;}
	/** 失効日 */
	public AppDate validDateTo {get; set;}
	/** コメント */
	public String comment {
		get;
		set {
			comment = value;
			setChanged(Field.COMMENT);
		}
	}
}