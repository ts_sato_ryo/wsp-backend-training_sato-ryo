/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇付与のエンティティ
 */
public with sharing class AttManagedLeaveGrantEntity extends Entity {
	/** 項目の定義(変更管理で使用する) */
	public enum Field {
			NAME, OWNER_ID, EMPLOYEE_ID, LEAVE_ID, VALID_FROM, VALID_TO,
			ORIGINAL_DAYS_GRANTED, DAYS_GRANTED, DAYS_LEFT, DAYS_EXPIRED, DAYS_ADJUSTED, COMMENT, IS_REMOVED
	}
	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;
	/**
	 * 項目の値を変更済みに設定する
	 * @param field 変更した項目
	 */
	private void setChanged(Field field) {
		isChangedFieldSet.add(field);
	}
	public AttManagedLeaveGrantEntity() {
		isChangedFieldSet = new Set<Field>();
	}
	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return isChangedFieldSet.contains(field);
	}
	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		isChangedFieldSet.clear();
	}
	/** 所有者ID */
	public Id ownerId {
		get;
		set {
			ownerId = value;
			setChanged(Field.OWNER_ID);
		}
	}
	/** 社員 */
	public Id employeeId {
		get;
		set {
			employeeId = value;
			setChanged(Field.EMPLOYEE_ID);
		}
	}
	/** 休暇 */
	public Id leaveId {
		get;
		set {
			leaveId = value;
			setChanged(Field.LEAVE_ID);
		}
	}
	/** 有効開始日 */
	public AppDate validDateFrom {
		get;
		set {
			validDateFrom = value;
			setChanged(Field.VALID_FROM);
		}
	}
	/** 失効日 */
	public AppDate validDateTo {
		get;
		set {
			validDateTo = value;
			setChanged(Field.VALID_TO);
		}
	}
	/** 付与日数 */
	public AttDays originalDaysGranted {
		get;
		set {
			originalDaysGranted = value;
			setChanged(Field.ORIGINAL_DAYS_GRANTED);
		}
	}
	/** 付与日数(調整後) */
	public AttDays daysGranted {
		get;
		set {
			daysGranted = value;
			setChanged(Field.DAYS_GRANTED);
		}
	}
	/** 残日数(確定分) */
	public AttDays daysLeft {
		get;
		set {
			daysLeft = value;
			setChanged(Field.DAYS_LEFT);
		}
	}
	/** 残時間(確定分) */
	public Integer hoursLeft { get; set;}
	/** 失効日数(確定分) */
	public AttDays daysExpired {
		get;
		set {
			daysExpired = value;
			setChanged(Field.DAYS_EXPIRED);
		}
	}
	/** コメント */
	public String comment {
		get;
		set {
			comment = value;
			setChanged(Field.COMMENT);
		}
	}
	/** 削除フラグ */
	public Boolean isRemoved {
		get {
			return isRemoved == null ? false : isRemoved;
		}
		set {
			isRemoved = value;
			setChanged(Field.IS_REMOVED);
		}
	}
	/** 調整一覧 */
	public List<GrantAdjustEntity> adjustList {get; set;}
	/** 休暇サマリ一覧 */
	public List<AttManagedLeaveGrantSummaryEntity> summaryList {get; set;}

	public AppMultiString leaveNameL {get; set;}
	public AttLeaveType leaveType {get; set;}
	public AppDatetime lastModifiedDate {get; set;}

	// 有休付与調整エンティティ
	public class GrantAdjustEntity extends Entity {
		public Id grantId {get; set;}
		public AppDate adjustDate {get; set;}
		public AttDays daysAdjusted {get; set;}
		public Boolean isRemoved {
			get {
				return isRemoved == null ? false : isRemoved;
			}
			set;
		}
	}
}