/**
 * コントローラクラスの基本クラス
 */
public virtual with sharing class BaseCtl {
	/**
	 * システム設定情報Dto
	 */
	public class ConfigResultDto {
		public String empId; // 社員Id
		public String userId; // ユーザーId
		public String userName; // ユーザー名
		public String userPhotoUrl; // ユーザー画像Url
		public String companyId; // 所属会社Id
		public String language; // 言語
		public String empName; // 社員名(多言語対応後)
		public Boolean useAttendance;
		public Boolean useExpense;
		public Boolean useWorkTime;

		public ConfigResultDto(ComDto.EmpDto empInfo) {
			this.empId = empInfo.Id;
			this.userId = empInfo.userInfo.Id;
			this.userName = empInfo.userInfo.name;
			this.userPhotoUrl = empInfo.userInfo.photoUrl;
			this.language = empInfo.userInfo.language;
			this.companyId = empInfo.companyInfo.Id;
			this.empName = empInfo.name;
			this.useAttendance = empInfo.companyInfo.useAttendance;
			this.useExpense = empInfo.companyInfo.useExpense;
			this.useWorkTime = empInfo.companyInfo.useWorkTime;
		}
		public override String toString() {
			return JSON.serialize(this);
		}
	}

	// ログインユーザーにより初期情報
	public ConfigResultDto loginEmpInfo {get; private set;}

	public BaseCtl() {
		this.loginEmpInfo = new ConfigResultDto(ComLogic.getEmpByUserId(null));
	}

	/**
	 * ジョブ一覧取得APIのリクエストパラメータ
	 */
	public class GetJobListReq {
		public Id parentId;
		public String targetDate;
	}

	/**
	 * ジョブ(レスポンス用)
	 */
	public class JobRes {
		public Id id;
		public String name;
		public String code;
		public Id parentId;
		public Boolean hasChildren; // 子ジョブが存在するか
		public Boolean isDirectCharged; // 直課かどうか
		public Boolean selectabledExpense; // 経費精算機能で選択可能かどうか
		public Boolean selectabledTimeTrack; // 工数管理機能で選択可能かどうか

		public JobRes(ComDto.JobDto dto) {
			this.id = dto.id;
			this.name = dto.name;
			this.code = dto.code;
			this.parentId = dto.parentId;
			this.hasChildren = dto.childrenCount > 0;
			this.isDirectCharged = dto.isDirectCharged;
			this.selectabledExpense = dto.selectabledExpense;
			this.selectabledTimeTrack = dto.selectabledTimeTrack;
		}
	}

	/**
	 * ジョブの一覧取得API
	 * @param req 親ジョブId情報
	 * @return ジョブ一覧
	 */
	@RemoteAction
	public static List<JobRes> getJobList(GetJobListReq req) {
		System.debug(LoggingLevel.DEBUG, '[START] BaseCtl#getJobList:' + req);

		List<JobRes> jobList = new List<JobRes>();
		// ジョブ一覧を取得
		for (ComDto.JobDto jobDto : ComLogic.getJobList(req.parentId, req.targetDate)) {
			jobList.add(new JobRes(jobDto));
		}

		System.debug(LoggingLevel.DEBUG, '[END] BaseCtl#getJobList:' + jobList);
		return jobList;
	}

	/**
	 * 作業分類一覧取得APIのリクエストパラメータ
	 */
	public class GetWorkCategoryListReq {
		public Id jobId;
		public String dateStr;
	}

	/**
	 * 作業分類レスポンス
	 */
	public class WorkCategoryRes {
		public Id id ;
		public String name;

		public WorkCategoryRes(ComDto.WorkCategoryDto dto) {
			this.id = dto.id;
			this.name = dto.name;
		}
	}

	/**
	 * 作業分類一覧レスポンス
	 */
	public class WorkCategoryListRes extends BaseResultDto {
		public List<WorkCategoryRes> workCategoryList = new List<WorkCategoryRes>();

		public void add(ComDto.WorkCategoryDto dto) {
			workCategoryList.add(new WorkCategoryRes(dto));
		}
	}

	/**
	 * 作業分類一覧取得API
	 * @param req リクエストパラメータ
	 * @return 作業分類一覧
	 */
	@RemoteAction
	public static WorkCategoryListRes getWorkCategoryList(GetWorkCategoryListReq req) {
		WorkCategoryListRes res = new WorkCategoryListRes();

		ComCtlLogic.executeControllerStart();
		try {
			// 作業分類一覧を取得
			for (ComDto.WorkCategoryDto workCategory : ComLogic.getWorkCategoryList(req.jobId, Date.valueOf(req.dateStr))) {
				res.add(workCategory);
			}
		} catch (Exception ex) {
			ComCtlLogic.executeControllerThrow(ex, res);
		} finally {
			ComCtlLogic.executeControllerEnd();
		}

		return res;
	}
}