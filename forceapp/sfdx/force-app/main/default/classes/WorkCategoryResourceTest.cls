/**
* @author TeamSpirit Inc.
* @date 2017
*
* @group 共通
*
* @description WorkCategoryResourceのテストクラス
*/
@isTest
private class WorkCategoryResourceTest {
	/**
	 * テストデータクラス
	 */
	private class ResourceTestData extends TestData.TestDataEntity {

		public Date currentDate;

		/**
		 * コンストラクタ
		 * 標準ユーザーと設定を作成する
		 */
		public ResourceTestData(){
			super();
			currentDate = Date.today();
		}

	}

	/**
	 * 作業分類レコードを1件作成する(正常系)
	 */
	@isTest
	static void createPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();

		Test.startTest();

		WorkCategoryResource.WorkCategory param = new WorkCategoryResource.WorkCategory();
		param.name_L0 = 'テスト作業分類_L0';
		param.name_L1 = 'テスト作業分類_L1';
		param.name_L2 = 'テスト作業分類_L2';
		param.code = '001';
		param.companyId = company.Id;
		param.validDateFrom = Date.today();
		param.validDateTo = Date.today().addDays(60);
		param.order = 1;

		WorkCategoryResource.CreateApi api = new WorkCategoryResource.CreateApi();
		WorkCategoryResource.SaveResult res = (WorkCategoryResource.SaveResult)api.execute(param);

		Test.stopTest();

		// 作業分類レコードが1件作成されること
		List<TimeWorkCategory__c> newRecords = [SELECT
				Id,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				Code__c,
				CompanyId__c,
				ValidFrom__c,
				ValidTo__c,
				Order__c
			FROM TimeWorkCategory__c];
		System.assertEquals(1, newRecords.size());

		TimeWorkCategory__c newRecord = newRecords[0];
		System.assertEquals(newRecord.Id, res.Id);
		System.assertEquals(param.name_L0, newRecord.Name);
		System.assertEquals(param.name_L0, newRecord.Name_L0__c);
		System.assertEquals(param.name_L1, newRecord.Name_L1__c);
		System.assertEquals(param.name_L2, newRecord.Name_L2__c);
		System.assertEquals(param.code, newRecord.Code__c);
		System.assertEquals(param.companyId, newRecord.CompanyId__c);
		System.assertEquals(param.validDateFrom, newRecord.ValidFrom__c);
		System.assertEquals(param.validDateTo, newRecord.ValidTo__c);
		System.assertEquals(param.order, newRecord.Order__c);

	}

	/**
	 * 作業分類レコードを1件作成する(異常系：作業分類の管理権限なし)
	 */
	@isTest
	static void createNegativeTestNoPermission() {

		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();

		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 権限を無効に設定する
		testData.permission.isManageTimeWorkCategory = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		WorkCategoryResource.WorkCategory param = new WorkCategoryResource.WorkCategory();
		param.name_L0 = 'テスト作業分類_L0';
		param.name_L1 = 'テスト作業分類_L1';
		param.name_L2 = 'テスト作業分類_L2';
		param.code = '001';
		param.companyId = testData.company.Id;
		param.validDateFrom = testData.currentDate;
		param.validDateTo = testData.currentDate.addDays(60);
		param.order = 1;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				WorkCategoryResource.CreateApi api = new WorkCategoryResource.CreateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());

	}

	/**
	 * 作業分類レコードを1件作成する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void creativeNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();

		Test.startTest();

		WorkCategoryResource.WorkCategory param = new WorkCategoryResource.WorkCategory();
		param.code = '001';
		param.companyId = company.Id;
		WorkCategoryResource.CreateApi api = new WorkCategoryResource.CreateApi();
		App.ParameterException ex;
		try {
			Object res = api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('NOT_SET_VALUE', ex.getErrorCode());
	}

	/**
	 * 作業分類レコードを1件更新する(正常系)
	 */
	@isTest
	static void updatePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeWorkCategory__c workCategory = ComTestDataUtility.createWorkCategory('Test作業分類', company.Id);

		Test.startTest();

		// レコード更新ロジックはレコード作成APIと共通のため、本テストは最低限必要なパラメータのみで行う
		// 全パラメータの検証は createPositiveTest() で行っている
		Map<String, Object> param = new Map<String, Object> {
			'id' => workCategory.id,
			'code' => '002',
			'companyId' => company.Id,
			'name_L0' => 'Test'
		};

		WorkCategoryResource.UpdateApi api = new WorkCategoryResource.UpdateApi();
		Object res = api.execute(param);

		Test.stopTest();

		System.assertEquals(null, res);

		// レコードが更新されていること
		List<TimeWorkCategory__c> wcList = [SELECT Code__c FROM TimeWorkCategory__c WHERE Id =:(String)param.get('id')];
		System.assertEquals(1, wcList.size());
		System.assertEquals((String)param.get('code'), wcList[0].Code__c);
	}

	/**
	 * 作業分類レコードを1件更新する(異常系：作業分類の管理権限なし)
	 */
	@isTest
	static void updateNegativeTestNoPermission() {

		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		WorkCategoryEntity workCategory = testData.createWorkCategoryList('Test作業分類',1).get(0);

		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 権限を無効に設定する
		testData.permission.isManageTimeWorkCategory = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		Map<String, Object> param = new Map<String, Object> {
			'id' => workCategory.id,
			'code' => '002',
			'companyId' => testData.company.Id,
			'name_L0' => 'Test'
		};

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				WorkCategoryResource.UpdateApi api = new WorkCategoryResource.UpdateApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 作業分類レコードを1件更新する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void updateNegativeTest() {
		Test.startTest();

		WorkCategoryResource.WorkCategory param = new WorkCategoryResource.WorkCategory();

		App.ParameterException ex;
		try {
			Object res = (new WorkCategoryResource.UpdateApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 作業分類レコードを1件削除する(正常系)
	 */
	@isTest
	static void deletePositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		TimeWorkCategory__c delRecord = ComTestDataUtility.createWorkCategory('Test作業分類', company.Id);

		Test.startTest();

		WorkCategoryResource.DeleteOption param = new WorkCategoryResource.DeleteOption();
		param.id = delRecord.id;

		Object res = (new WorkCategoryResource.DeleteApi()).execute(param);

		Test.stopTest();

		// 作業分類レコードが削除されること
		List<TimeWorkCategory__c> WorkCategoryList = [SELECT Id FROM TimeWorkCategory__c];
		System.assertEquals(0, WorkCategoryList.size());
	}

	/**
	 * 作業分類レコードを1件削除する(異常系：作業分類の管理権限なし)
	 */
	@isTest
	static void deleteNegativeTestNoPermission() {

		// テストデータ作成
		ResourceTestData testData = new ResourceTestData();
		WorkCategoryEntity delRecord = testData.createWorkCategoryList('Test作業分類',1).get(0);

		// システム管理者だと権限設定が有効にならないため標準ユーザーでテストする
		EmployeeBaseEntity stdEmployee = testData.createEmployeeWithStandardUser('テスト標準ユーザ');
		// 権限を無効に設定する
		testData.permission.isManageTimeWorkCategory = false;
		new PermissionRepository().saveEntity(testData.permission);

		// リクエストパラメータ
		WorkCategoryResource.DeleteOption param = new WorkCategoryResource.DeleteOption();
		param.id = delRecord.id;

		// API実行
		App.NoPermissionException actEx;
		System.runAs(testData.createRunAsUserFromEmployee(stdEmployee)) {
			try {
				WorkCategoryResource.DeleteApi api = new WorkCategoryResource.DeleteApi();
				api.execute(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
		}

		// 検証
		System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
		System.assertEquals(ComMessage.msg().Com_Err_NoApiPermission, actEx.getMessage());
	}

	/**
	 * 作業分類レコードを1件削除する(異常系:必須パラメータが未設定)
	 */
	@isTest
	static void deleteRequiredIdTest() {

		Test.startTest();

		WorkCategoryResource.DeleteOption param = new WorkCategoryResource.DeleteOption();

		App.ParameterException ex;
		try {
			Object res = (new WorkCategoryResource.DeleteApi()).execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		System.assertEquals('INVALID_PARAMETER', ex.getErrorCode());
	}

	/**
	 * 作業分類レコードを1件削除する(異常系:DMLException発生)
	 */
	@isTest
	static void deleteNegativeTest() {

		ResourceTestData testData = new ResourceTestData();
		User stdUser = testData.createRunAsUserFromEmployee(testData.createEmployeeWithStandardUser('テスト標準ユーザ'));
		WorkCategoryEntity delRecord = testData.createWorkCategoryList('Test作業分類',1).get(0);
		Test.startTest();

		WorkCategoryResource.DeleteOption param = new WorkCategoryResource.DeleteOption();
		param.Id = testData.company.Id;

		DmlException ex;

		System.runAs(stdUser){
			try {
				Object res = (new WorkCategoryResource.DeleteApi()).execute(param);
			} catch (DmlException e) {
				ex = e;
			}
		}

		Test.stopTest();

		System.assertEquals('INSUFFICIENT_ACCESS_OR_READONLY', ex.getDmlStatusCode(0));
	}

	/**
	 * 作業分類レコードをIDで検索する(正常系)
	 */
	@isTest
	static void searchByIdPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeWorkCategory__c> workCategories = ComTestDataUtility.createWorkCategories('テスト作業分類', company.Id, 3);
		TimeWorkCategory__c testRecord = workCategories[0];

		Test.startTest();

		WorkCategoryResource.SearchCondition param = new WorkCategoryResource.SearchCondition();
		param.id = testRecord.id;
		param.companyId = testRecord.CompanyId__c;

		WorkCategoryResource.SearchApi api = new WorkCategoryResource.SearchApi();
		WorkCategoryResource.SearchResult res = (WorkCategoryResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 作業分類レコードが取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		// 項目値の検証はsearchAllPositiveTestで行うため省略
	}

	/**
	 * 作業分類レコードを全件検索する(正常系:ソート順指定)
	 */
	@isTest
	static void searchSortPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeWorkCategory__c> workCategories = ComTestDataUtility.createWorkCategories('テスト作業分類', company.Id, 3);
		workCategories[1].Order__c = null;
		update workCategories[1];

		// テスト実行結果と同じ並びになるように再取得
		workCategories = [SELECT Id FROM TimeWorkCategory__c ORDER BY Order__c DESC NULLS LAST, Name ASC NULLS LAST];

		Test.startTest();

		// 表示順の降順で並び替え
		WorkCategoryResource.SortOrder order1 = new WorkCategoryResource.SortOrder();
		order1.field = 'Order__c';
		order1.orderBy = 'desc';

		WorkCategoryResource.SortOrder order2 = new WorkCategoryResource.SortOrder();
		order2.field = 'Name';
		order2.orderBy = 'asc';

		WorkCategoryResource.SearchCondition param = new WorkCategoryResource.SearchCondition();
		param.companyId = company.Id;
		param.sortOrder = new List<WorkCategoryResource.SortOrder>{ order1, order2 };

		WorkCategoryResource.SearchApi api = new WorkCategoryResource.SearchApi();
		WorkCategoryResource.SearchResult res = (WorkCategoryResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 作業分類レコードが全件取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(workCategories.size(), res.records.size());

		// レコードが表示順の降順になっていること
		for (Integer i = 0, n = workCategories.size(); i < n; i++) {
			System.assertEquals(workCategories[i].Id, res.records[i].id);
		}
		// 項目値の検証はsearchAllPositiveTestで行うため省略

	}

	/**
	 * 作業分類レコードを検索する(正常系:多言語対応L1のテスト)
	 */
	@isTest
	static void searchL1PositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeWorkCategory__c> workCategories = ComTestDataUtility.createWorkCategories('テスト作業分類', company.Id, 3);
		TimeWorkCategory__c testRecord = workCategories[0];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>{
			'id' => testRecord.id
		};

		WorkCategoryResource.SearchApi api = new WorkCategoryResource.SearchApi();
		WorkCategoryResource.SearchResult res = (WorkCategoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 作業分類レコードが取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		// 多言語対応項目の検証
		System.assertEquals(testRecord.Name_L1__c, res.records[0].name);
	}

	/**
	 * 作業分類レコードを検索する(正常系:多言語対応L2のテスト)
	 */
	@isTest
	static void searchL2PositiveTest() {

		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), null, Userinfo.getLanguage());
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeWorkCategory__c> workCategories = ComTestDataUtility.createWorkCategories('テスト作業分類', company.Id, 3);
		TimeWorkCategory__c testRecord = workCategories[0];

		Test.startTest();

		Map<String, Object> paramMap = new Map<String, Object>{
			'id' => testRecord.id
		};

		WorkCategoryResource.SearchApi api = new WorkCategoryResource.SearchApi();
		WorkCategoryResource.SearchResult res = (WorkCategoryResource.SearchResult)api.execute(paramMap);

		Test.stopTest();

		// 作業分類レコードが取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(1, res.records.size());
		// 多言語対応項目の検証
		System.assertEquals(testRecord.Name_L2__c, res.records[0].name);
	}

	/**
	 * 作業分類レコードを全件検索する(正常系)
	 */
	@isTest
	static void searchAllPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<TimeWorkCategory__c> workCategories = ComTestDataUtility.createWorkCategories('テスト作業分類', company.Id, 3);
		// 作業分類コード順で再取得
		workCategories = [SELECT
				Id, Name_L0__c, Name_L1__c, Name_L2__c, Code__c, CompanyId__c, ValidFrom__c, ValidTo__c, Order__c
			FROM TimeWorkCategory__c
			ORDER BY Code__c NULLS LAST];

		Test.startTest();

		Map<String, Object> param = new Map<String, Object>();
		WorkCategoryResource.SearchApi api = new WorkCategoryResource.SearchApi();
		WorkCategoryResource.SearchResult res = (WorkCategoryResource.SearchResult)api.execute(param);

		Test.stopTest();

		// 作業分類レコードが全件取得できること
		System.assertNotEquals(null, res.records);
		System.assertEquals(workCategories.size(), res.records.size());

		// レコードが作業分類コードの昇順になっていること
		for (Integer i = 0, n = workCategories.size(); i < n; i++) {
			System.assertEquals(workCategories[i].Id, res.records[i].id);
			System.assertEquals(workCategories[i].Name_L0__c, res.records[i].name);	// デフォルトでL0の値を返す
			System.assertEquals(workCategories[i].Name_L0__c, res.records[i].name_L0);
			System.assertEquals(workCategories[i].Name_L1__c, res.records[i].name_L1);
			System.assertEquals(workCategories[i].Name_L2__c, res.records[i].name_L2);
			System.assertEquals(workCategories[i].Code__c, res.records[i].code);
			System.assertEquals(workCategories[i].CompanyId__c, res.records[i].companyId);
			System.assertEquals(workCategories[i].ValidFrom__c, res.records[i].validDateFrom);
			System.assertEquals(workCategories[i].ValidTo__c, res.records[i].validDateTo);
			System.assertEquals(workCategories[i].Order__c, res.records[i].order);
		}

	}

}
