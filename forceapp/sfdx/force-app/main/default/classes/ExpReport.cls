/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description Parent entity class for ExpReportEntity and ExpRequestEntity
 */
public abstract with sharing class ExpReport extends ExpExtendedItemEntity {

	/** Object of approval request info */
	public virtual class Request extends ValueObject {
		/** Status */
		public AppRequestStatus status {public get; protected set;}
		/** Cancel Type */
		public AppCancelType cancelType {public get; protected set;}
		/** Confirmation required */
		public Boolean isConfirmationRequired {public get; protected set;}
		/** Application time */
		public AppDatetime requestTime {public get; protected set;}

		/** Constructor for ExpReport Request */
		protected Request() {}

		/**
 		 * Comparator
 		 * @param compare target object to compare
 		 */
 		public virtual override Boolean equals(Object compare) {
			Boolean ret;
			if (compare instanceof Request) {
				Request compareRequest = (Request)compare;
				if ((this.status != compareRequest.status)
						|| (this.cancelType != compareRequest.cancelType)
						|| (this.isConfirmationRequired != compareRequest.isConfirmationRequired)
						|| (this.requestTime != compareRequest.requestTime)) {
					ret = false;
				} else {
					ret = true;
				}
			} else {
				ret = false;
			}
			return ret;
		}
	}

	/** Fields definition */
	public enum ReportBaseField {
		ATTACHED_FILE_LIST,
		COST_CENTER_HISTORY_ID,
		DEPARTMENT_HISTORY_ID,
		EMPLOYEE_HISTORY_ID,
		EXP_REPORT_TYPE_ID,
		JOB_ID,
		NAME,
		PAYMENT_DUE_DATE,
		PREFIX,
		SEQUENCE_NO,
		SUBJECT,
		TOTAL_AMOUNT,
		VENDOR_ID
	}

	/** Map of Fields with Name as the Key and the ReportBaseField enum as the value */
	public static final Map<String, ReportBaseField> REPORT_BASE_FIELD_MAP;
	static {
		final Map<String, ReportBaseField> fieldMap = new Map<String, ReportBaseField>();
		for (ReportBaseField f : ReportBaseField.values()) {
			fieldMap.put(f.name(), f);
		}
		REPORT_BASE_FIELD_MAP = fieldMap;
	}

	/** Set of fields whose value has changed */
	private Set<ReportBaseField> isChangedReportBaseFieldSet;


	/** Constructor */
	protected ExpReport() {
		super();
		isChangedReportBaseFieldSet = new Set<ReportBaseField>();
	}

	/** List of attached files */
	public List<AppContentDocumentEntity> attchedFileList {get; protected set;}

	/**
	 * Replace attached files list
	 * @param attachedFileList
	 */
	public void replaceAttachedFileList(List<AppContentDocumentEntity> attachedFileList) {
		this.attchedFileList = new List<AppContentDocumentEntity>(attachedFileList);
		setChanged(ReportBaseField.ATTACHED_FILE_LIST);
	}

	/** ID of Cost Center History */
	public String costCenterHistoryId {
		get;
		set {
			costCenterHistoryId = value;
			setChanged(ReportBaseField.COST_CENTER_HISTORY_ID);
		}
	}
	/** Cost Center Name (history specific) */
	public String costCenterName {get {return AppMultiString.stringValue(costCenterNameL);}}
	public AppMultiString costCenterNameL {get; set;}

	/** Cost Center Code (for reference only) */
	public String costCenterCode {get; set;}

	/** CostCenterLinkage Code (for reference only) */
	public String costCenterLinkageCode {get; set;}

	/** Department history ID */
	public Id departmentHistoryId {
		get;
		set {
			departmentHistoryId = value;
			setChanged(ReportBaseField.DEPARTMENT_HISTORY_ID);
		}
	}

	/** Employee history ID */
	public Id employeeHistoryId {
		get;
		set {
			employeeHistoryId = value;
			setChanged(ReportBaseField.EMPLOYEE_HISTORY_ID);
		}
	}

	/** Employee base ID */
	public Id employeeBaseId {get; set;}

	/** Employee Name */
	public String employeeName {get {return employeeNameL != null ? employeeNameL.getFullName(): null;}}
	public AppPersonName employeeNameL {get; set;}

	/** Employee Code (for reference only) */
	public String employeeCode {get; set;}

	/** Expense Report Type ID */
	public Id expReportTypeId {
		get;
		set {
			expReportTypeId = value;
			setChanged(ReportBaseField.EXP_REPORT_TYPE_ID);
		}
	}

	/** Report Type Name (for reference only) */
	public String expReportTypeName {get {return AppMultiString.stringValue(expReportTypeNameL);}}
	public AppMultiString expReportTypeNameL {get; set;}

	/** Report Type Code (for reference only) */
	public String expReportTypeCode {get; set;}

	/** Job ID */
	public Id jobId {
		get;
		set {
			jobId = value;
			setChanged(ReportBaseField.JOB_ID);
		}
	}

	/** Job Name (for reference only) */
	public String jobName {get {return AppMultiString.stringValue(jobNameL);}}
	public AppMultiString jobNameL {get; set;}

	/** Job Code (for reference only) */
	public String jobCode {get; set;}

	/** Name of record */
	public String name {
		get;
		set {
			if (String.isBlank(value)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('name');
				throw ex;
			}
			name = value;
			setChanged(ReportBaseField.NAME);
		}
	}

	/** Payment Due Date */
	public AppDate paymentDueDate {
		get;
		set {
			paymentDueDate = value;
			setChanged(ReportBaseField.PAYMENT_DUE_DATE);
		}
	}

	/** Prefix */
	public String prefix {
		get;
		set {
			prefix = value;
			setChanged(ReportBaseField.PREFIX);
		}
	}

	/** Sequence Number */
	public Integer sequenceNo {
		get;
		set {
			sequenceNo = value;
			setChanged(ReportBaseField.SEQUENCE_NO);
		}
	}

	/** Subject */
	public String subject {
		get;
		set {
			subject = value;
			setChanged(ReportBaseField.SUBJECT);
		}
	}

	/** Total amount */
	public Decimal totalAmount {
		get;
		set {
			totalAmount = value;
			setChanged(ReportBaseField.TOTAL_AMOUNT);
		}
	}

	/*
	 * Report Type useFileAttachment (Reference Only)
	 */
	public Boolean useFileAttachment{
		get;
		set {
			useFileAttachment = value;
		}
	}

	/** Vendor ID */
	public Id vendorId {
		get;
		set {
			vendorId = value;
			setChanged(ReportBaseField.VENDOR_ID);
		}
	}

	/** Vendor Name (For reference) */
	public String vendorName {get {return AppMultiString.stringValue(vendorNameL);}}
	public AppMultiString vendorNameL {get; set;}
	public String vendorCode {get; set;}
	/** Vendor Payment Due Date Usage (Reference ONLY) */
	public ExpVendorPaymentDueDateUsage vendorPaymentDueDateUsage {get; set;}


	/**
	 * Mark field as changed
	 * @param field Changed field
	 */
	private void setChanged(ReportBaseField field) {
		this.isChangedReportBaseFieldSet.add(field);
	}

	/**
	 * Check if the value of the field has been changed
	 * @param field Target field
	 * @param Returns true if the value has been changed, otherwise, returns false
	 */
	public Boolean isChanged(ReportBaseField field) {
		return isChangedReportBaseFieldSet.contains(field);
	}

	public virtual override void resetChanged() {
		super.resetChanged();
		this.isChangedReportBaseFieldSet.clear();
	}

	/*
	 * To allows child classes to determine if a field is an ExpReport field
	 * @param fieldName the representative field name in String (e.g. expReportTypeId)
	 *
	 * @return true if the specified string is an ExpReport field. Otherwise, false.
	 */
	protected Boolean isExpReportField(String fieldName) {
		return ExpReport.REPORT_BASE_FIELD_MAP.get(name) == null ? false : true;
	}

	/**
	 * Create name(Emp code + Emp name)
	 * @param employeeCode Employee Code
	 * @param employeeName Employee Name
	 * @param num Report or Request Number
	 * @return Name
	 */
	public static String createName(String employeeCode, String employeeName, String num) {
		final Integer empLength = 64; // Limit of employee string

		if (String.isBlank(employeeCode)) {
			// Only for debugging, no need to translate
			throw new App.ParameterException('employeeCode is blank');
		}
		if (String.isBlank(employeeName)) {
			// Only for debugging, no need to translate
			throw new App.ParameterException('employeeName is blank');
		}

		return (employeeCode + employeeName).left(empLength) + (String.isBlank(num) ? '' : num);
	}

	/*
	 * Return the field value
	 * @param name Target field name
	 */
	public virtual Object getValue(String name) {
		ReportBaseField field = ExpReport.REPORT_BASE_FIELD_MAP.get(name);
		if (field != null) {
			return getValue(field);
		}

		if (super.isExtendedItemField(name)) {
			return getExtendedItemValue(name);
		}

		throw new App.ParameterException('Unknown field : ' + name);
	}

	/**
	 * Return the field value
	 * @param field Target field to get the value
	 */
	private Object getValue(ExpReport.ReportBaseField field) {
		if (field == ExpReport.ReportBaseField.ATTACHED_FILE_LIST) {
			return this.attchedFileList;
		} else if (field == ExpReport.ReportBaseField.COST_CENTER_HISTORY_ID) {
			return this.costCenterHistoryId;
		} else if (field == ExpReport.ReportBaseField.DEPARTMENT_HISTORY_ID) {
			return this.departmentHistoryId;
		} else if (field == ExpReport.ReportBaseField.EMPLOYEE_HISTORY_ID) {
			return this.employeeHistoryId;
		} else if (field == ExpReport.ReportBaseField.EXP_REPORT_TYPE_ID) {
			return this.expReportTypeId;
		} else if (field == ExpReport.ReportBaseField.JOB_ID) {
			return this.jobId;
		} else if (field == ExpReport.ReportBaseField.NAME) {
			return this.name;
		} else if (field == ExpReport.ReportBaseField.PAYMENT_DUE_DATE) {
			return this.paymentDueDate;
		} else if (field == ExpReport.ReportBaseField.PREFIX) {
			return this.prefix;
		} else if (field == ExpReport.ReportBaseField.SEQUENCE_NO) {
			return this.sequenceNo;
		} else if (field == ExpReport.ReportBaseField.SUBJECT) {
			return this.subject;
		} else if (field == ExpReport.ReportBaseField.TOTAL_AMOUNT) {
			return this.totalAmount;
		} else if (field == ExpReport.ReportBaseField.VENDOR_ID) {
			return this.vendorId;
		}

		throw new App.ParameterException('Cannot retrieve value from field : ' + field.name());
	}
}
