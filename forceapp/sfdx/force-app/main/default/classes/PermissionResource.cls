/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 権限レコード操作API
 */
public with sharing class PermissionResource {

	/**
	 * @description Permissionレコードを表すクラス
	 */
	public virtual class Permission {
		/** 権限レコードID */
		public String id;
		/** 権限コード */
		public String code;
		/** 権限名 */
		public String name;
		/** 権限名 (L0) */
		public string name_L0;
		/** 権限名 (L1) */
		public String name_L1;
		/** 権限名 (L2) */
		public String name_L2;
		/** 会社レコードID */
		public String companyId;

		//勤怠トランザクション系権限
		/* 勤務表表示(代理) */
		public Boolean viewAttTimesheetByDelegate;
		/* 勤務表編集(代理) */
		public Boolean editAttTimesheetByDelegate;

		// 工数トランザクション系権限
		/* 工数実績表示(代理) */
		public Boolean viewTimeTrackByDelegate;

		//勤怠申請系権限
		/* 各種勤怠申請申請・申請削除(代理) */
		public Boolean submitAttDailyRequestByDelegate;
		/* 各種勤怠申請申請取消(代理) */
		public Boolean cancelAttDailyRequestByDelegate;
		/* 各種勤怠申請承認却下(代理) */
		public Boolean approveAttDailyRequestByDelegate;
		/* 各種勤怠申請承認取消(本人) */
		public Boolean cancelAttDailyApprovalByEmployee;
		/* 各種勤怠申請承認取消(代理) */
		public Boolean cancelAttDailyApprovalByDelegate;
		/* 各種勤怠申請自己承認(本人) */
		public Boolean approveSelfAttDailyRequestByEmployee;
		/* 勤務確定申請申請(代理) */
		public Boolean submitAttRequestByDelegate;
		/* 勤務確定申請申請取消(代理) */
		public Boolean cancelAttRequestByDelegate;
		/* 勤務確定申請承認却下(代理) */
		public Boolean approveAttRequestByDelegate;
		/* 勤務確定申請承認取消(本人) */
		public Boolean cancelAttApprovalByEmployee;
		/* 勤務確定申請承認取消(代理) */
		public Boolean cancelAttApprovalByDelegate;
		/* 勤務確定申請自己承認(本人) */
		public Boolean approveSelfAttRequestByEmployee;

		//管理系権限
		/* 全体設定の管理 */
		public Boolean manageOverallSetting;
		/* 会社の切り替え */
		public Boolean switchCompany;
		/* 部署の管理 */
		public Boolean manageDepartment;
		/* 社員の管理 */
		public Boolean manageEmployee;
		/* カレンダーの管理 */
		public Boolean manageCalendar;
		/* ジョブタイプの管理 */
		public Boolean manageJobType;
		/* ジョブの管理 */
		public Boolean manageJob;
		/* モバイル機能設定の管理 */
		public Boolean manageMobileSetting;
		/* プランナー機能設定の管理 */
		public Boolean managePlannerSetting;
		/* アクセス権限設定の管理 */
		public Boolean managePermission;
		/* 休暇の管理 */
		public Boolean manageAttLeave;
		/* 短時間勤務設定の管理 */
		public Boolean manageAttShortTimeWorkSetting;
		/* 休職・休業の管理 */
		public Boolean manageAttLeaveOfAbsence;
		/* 勤務体系の管理 */
		public Boolean manageAttWorkingType;
		/* 勤務パターンの管理 */
		public Boolean manageAttPattern;
		/* 残業警告設定の管理 */
		public Boolean manageAttAgreementAlertSetting;
		/* 休暇管理の管理 */
		public Boolean manageAttLeaveGrant;
		/* 短時間勤務設定適用の管理 */
		public Boolean manageAttShortTimeWorkSettingApply;
		/* 休職・休業適用の管理 */
		public Boolean manageAttLeaveOfAbsenceApply;
		/* 勤務パターン適用の管理 */
		public Boolean manageAttPatternApply;
		/* 工数設定の管理 */
		public Boolean manageTimeSetting;
		/* 作業分類の管理 */
		public Boolean manageTimeWorkCategory;
		/* manage of expense type group */
		public Boolean manageExpTypeGroup;
		/* manage of expense type */
		public Boolean manageExpenseType;
		/* manage of tax type */
		public Boolean manageTaxType;
		/* manage of expense setting */
		public Boolean manageExpSetting;
		/* manage of exchenge rate */
		public Boolean manageExchangeRate;
		/* manage of accounting period */
		public Boolean manageAccountingPeriod;
		/* manage of report type */
		public Boolean manageReportType;
		/* manage of cost center */
		public Boolean manageCostCenter;
		/* manage of vendor */
		public Boolean manageVendor;
		/* manage of extended item */
		public Boolean manageExtendedItem;
		/* manage of group */
		public Boolean manageEmployeeGroup;
		/** manage expense custom hint */
		public Boolean manageExpCustomHint;

		/**
		 * デフォルトコンストラクタ
		 */
		public Permission() {
		}

		/**
		 * エンティティからインスタンスを生成するコンストラクタ
		 * @param entity アクセス権限エンティティ
		 */
		public Permission(PermissionEntity entity) {
			this.id = entity.id;
			this.code = entity.code;
			this.name = entity.nameL.getValue();
			this.name_L0 = entity.nameL0;
			this.name_L1 = entity.nameL1;
			this.name_L2 = entity.nameL2;
			this.companyId = entity.companyId;

			this.viewAttTimesheetByDelegate = entity.isViewAttTimeSheetByDelegate;
			this.editAttTimesheetByDelegate = entity.isEditAttTimeSheetByDelegate;

			this.viewTimeTrackByDelegate = entity.isViewTimeTrackByDelegate;

			this.submitAttDailyRequestByDelegate = entity.isSubmitAttDailyRequestByDelegate;
			this.cancelAttDailyRequestByDelegate = entity.isCancelAttDailyRequestByDelegate;
			this.approveAttDailyRequestByDelegate = entity.isApproveAttDailyRequestByDelegate;
			this.cancelAttDailyApprovalByEmployee = entity.isCancelAttDailyApprovalByEmployee;
			this.cancelAttDailyApprovalByDelegate = entity.isCancelAttDailyApprovalByDelegate;
			this.approveSelfAttDailyRequestByEmployee = entity.isApproveSelfAttDailyRequestByEmployee;
			this.submitAttRequestByDelegate = entity.isSubmitAttRequestByDelegate;
			this.cancelAttRequestByDelegate = entity.isCancelAttRequestByDelegate;
			this.approveAttRequestByDelegate = entity.isApproveAttRequestByDelegate;
			this.cancelAttApprovalByEmployee = entity.isCancelAttApprovalByEmployee;
			this.cancelAttApprovalByDelegate = entity.isCancelAttApprovalByDelegate;
			this.approveSelfAttRequestByEmployee = entity.isApproveSelfAttRequestByEmployee;

			this.manageOverallSetting = entity.isManageOverallSetting;
			this.switchCompany = entity.isSwitchCompany;
			this.manageDepartment = entity.isManageDepartment;
			this.manageEmployee = entity.isManageEmployee;
			this.manageCalendar = entity.isManageCalendar;
			this.manageJobType = entity.isManageJobType;
			this.manageJob = entity.isManageJob;
			this.manageMobileSetting = entity.isManageMobileSetting;
			this.managePlannerSetting = entity.isManagePlannerSetting;
			this.managePermission = entity.isManagePermission;
			this.manageAttLeave = entity.isManageAttLeave;
			this.manageAttShortTimeWorkSetting = entity.isManageAttShortTimeWorkSetting;
			this.manageAttLeaveOfAbsence = entity.isManageAttLeaveOfAbsence;
			this.manageAttWorkingType = entity.isManageAttWorkingType;
			this.manageAttPattern = entity.isManageAttPattern;
			this.manageAttAgreementAlertSetting = entity.isManageAttAgreementAlertSetting;
			this.manageAttLeaveGrant = entity.isManageAttLeaveGrant;
			this.manageAttShortTimeWorkSettingApply = entity.isManageAttShortTimeWorkSettingApply;
			this.manageAttLeaveOfAbsenceApply = entity.isManageAttLeaveOfAbsenceApply;
			this.manageAttPatternApply = entity.isManageAttPatternApply;
			this.manageTimeSetting = entity.isManageTimeSetting;
			this.manageTimeWorkCategory = entity.isManageTimeWorkCategory;
			this.manageExpTypeGroup = entity.isManageExpTypeGroup;
			this.manageExpenseType = entity.isManageExpType;
			this.manageTaxType = entity.isManageExpTaxType;
			this.manageExpSetting = entity.isManageExpSetting;
			this.manageExchangeRate = entity.isManageExpExchangeRate;
			this.manageAccountingPeriod = entity.isManageExpAccountingPeriod;
			this.manageReportType = entity.isManageExpReportType;
			this.manageCostCenter = entity.isManageExpCostCenter;
			this.manageVendor = entity.isManageExpVendor;
			this.manageExtendedItem = entity.isManageExpExtendedItem;
			this.manageEmployeeGroup = entity.isManageExpEmployeeGroup;
			this.manageExpCustomHint = entity.isManageExpCustomHint;
		}
	}

	/**
	 * @description 権限レコードの検索条件を格納するクラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** 権限レコードID */
		public String id;
		/** 会社レコードID */
		public String companyId;
		public void validate() {
			// 権限レコードID
			if (String.isNotBlank(id)) {
				try {
					Id val = (Id)id;
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}
			// 会社ID
			if (String.isNotBlank(companyId)) {
				try {
					Id val = (Id)companyId;
				} catch (Exception e) {
					throw new App.ParameterException('companyId', companyId);
				}
			}
		}
	}

	/**
	 * @description 権限レコード検索APIの実行結果を格納するクラス
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** 権限レコード検索結果 ソートはcode昇順*/
		public Permission[] records;
	}

	/**
	 * @description 権限レコード検索API
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {
		/**
		 * @description 権限レコードを検索する
		 * @param req リクエストパラメータ
		 * @return レスポンス(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchRequest param = (SearchRequest)req.getParam(SearchRequest.class);
			// パラメータ値の検証
			param.validate();

			// 権限データを取得する
			List<Id> filterIdList = new List<Id>();
			if (param.id != null) {
				filterIdList.add(param.id);
			}
			List<PermissionEntity> searchedPermissionList = new PermissionService().getPermissionList(filterIdList, param.companyId);

			// 成功レスポンスをセットする
			SearchResponse res = new SearchResponse();
			List<Permission> resPermissionList = new List<Permission>();
			for (PermissionEntity entity : searchedPermissionList) {
				resPermissionList.add(new Permission(entity));
			}
			res.records = resPermissionList;
			return res;
		}
	}

	/**
	 * @description 作成・更新APIのリクエストパラメータ。更新対象権限オブジェクトを表す。
	 * NOTE:モデルとしては不適切だが、項目追加のメンテナンスコストを抑えるためにPermissionを継承している。
	 */
	public class SaveRequest extends Permission implements RemoteApi.RequestParam {

		/**
		 * パラメータを検証する
		 * @param isUpdate trueの場合、更新
		 */
		public void validate(Boolean isUpDate) {
			// 会社コード
			if (!isUpdate && String.isBlank(this.companyId)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank(ComMessage.msg().Admin_Lbl_Company);
				throw e;
			}

			// ID
			if (isUpdate && String.isBlank(this.id)) {
					throw new App.ParameterException('id', id);
			}
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException('id', id);
				}
			}

			// コード
			if (String.isBlank(this.code)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank(ComMessage.msg().Admin_Lbl_Code);
				throw e;
			}

			// 権限名(L0)
			if (String.isBlank(this.name_L0)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank(ComMessage.msg().Admin_Lbl_Name);
				throw e;
			}
		}

		/**
		 * @description Entityに置換する
		 * @param  paramMap レスポンスパラメータ
		 * @param  isUpdate trueの場合、更新。falseの場合、新規作成。
		 */
		private PermissionEntity createPermissionEntity(Map<String, Object> paramMap, Boolean isUpdate) {
			PermissionEntity entity = new PermissionEntity();

			// 先に新規作成か更新かでセットするかどうかで変わる項目をセット
			if (isUpdate) {
				// レコード更新時はIDをセット
				entity.setId(this.id);
			} else {
				// 会社IDは新規作成時のみセット
				if (paramMap.containsKey('companyId')) {
					entity.companyId = this.companyId;
				}
			}

			// 基本項目
			if (paramMap.containsKey('name_L0')) {
				entity.name = this.name_L0;
				entity.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if(paramMap.containsKey('code')){
				entity.code = this.code;
			}

			// 勤怠トランザクション系権限
			if (paramMap.containsKey('viewAttTimesheetByDelegate')) {
				entity.isViewAttTimeSheetByDelegate = this.viewAttTimesheetByDelegate == null ? false : this.viewAttTimesheetByDelegate;
			}
			if (paramMap.containsKey('editAttTimesheetByDelegate')) {
				entity.isEditAttTimeSheetByDelegate = this.editAttTimesheetByDelegate == null ? false : this.editAttTimesheetByDelegate;
			}

			// 工数トランザクション系権限
			if (paramMap.containsKey('viewTimeTrackByDelegate')) {
				entity.isViewTimeTrackByDelegate = this.viewTimeTrackByDelegate == null ? false : this.viewTimeTrackByDelegate;
			}

			// 勤怠申請系権限
			if (paramMap.containsKey('submitAttDailyRequestByDelegate')) {
				entity.isSubmitAttDailyRequestByDelegate = this.submitAttDailyRequestByDelegate == null ? false : this.submitAttDailyRequestByDelegate;
			}
			if (paramMap.containsKey('cancelAttDailyRequestByDelegate')) {
				entity.isCancelAttDailyRequestByDelegate = this.cancelAttDailyRequestByDelegate == null ? false : this.cancelAttDailyRequestByDelegate;
			}
			if (paramMap.containsKey('approveAttDailyRequestByDelegate')) {
				entity.isApproveAttDailyRequestByDelegate = this.approveAttDailyRequestByDelegate == null ? false : this.approveAttDailyRequestByDelegate;
			}
			if (paramMap.containsKey('cancelAttDailyApprovalByEmployee')) {
				entity.isCancelAttDailyApprovalByEmployee = this.cancelAttDailyApprovalByEmployee == null ? false : this.cancelAttDailyApprovalByEmployee;
			}
			if (paramMap.containsKey('cancelAttDailyApprovalByDelegate')) {
				entity.isCancelAttDailyApprovalByDelegate = this.cancelAttDailyApprovalByDelegate == null ? false : this.cancelAttDailyApprovalByDelegate;
			}
			if (paramMap.containsKey('approveSelfAttDailyRequestByEmployee')) {
				entity.isApproveSelfAttDailyRequestByEmployee = this.approveSelfAttDailyRequestByEmployee == null ? false : this.approveSelfAttDailyRequestByEmployee;
			}
			if (paramMap.containsKey('submitAttRequestByDelegate')) {
				entity.isSubmitAttRequestByDelegate = this.submitAttRequestByDelegate == null ? false : this.submitAttRequestByDelegate;
			}
			if (paramMap.containsKey('cancelAttRequestByDelegate')) {
				entity.isCancelAttRequestByDelegate = this.cancelAttRequestByDelegate == null ? false : this.cancelAttRequestByDelegate;
			}
			if (paramMap.containsKey('approveAttRequestByDelegate')) {
				entity.isApproveAttRequestByDelegate = this.approveAttRequestByDelegate == null ? false : this.approveAttRequestByDelegate;
			}
			if (paramMap.containsKey('cancelAttApprovalByEmployee')) {
				entity.isCancelAttApprovalByEmployee = this.cancelAttApprovalByEmployee == null ? false : this.cancelAttApprovalByEmployee;
			}
			if (paramMap.containsKey('cancelAttApprovalByDelegate')) {
				entity.isCancelAttApprovalByDelegate = this.cancelAttApprovalByDelegate == null ? false : this.cancelAttApprovalByDelegate;
			}
			if (paramMap.containsKey('approveSelfAttRequestByEmployee')) {
				entity.isApproveSelfAttRequestByEmployee = this.approveSelfAttRequestByEmployee == null ? false : this.approveSelfAttRequestByEmployee;
			}

			// 管理系権限
			if (paramMap.containsKey('manageOverallSetting')) {
				entity.isManageOverallSetting = this.manageOverallSetting == null ? false : this.manageOverallSetting;
			}
			if (paramMap.containsKey('switchCompany')) {
				entity.isSwitchCompany = this.switchCompany == null ? false : this.switchCompany;
			}
			if (paramMap.containsKey('manageDepartment')) {
				entity.isManageDepartment = this.manageDepartment == null ? false : this.manageDepartment;
			}
			if (paramMap.containsKey('manageEmployee')) {
				entity.isManageEmployee = this.manageEmployee == null ? false : this.manageEmployee;
			}
			if (paramMap.containsKey('manageCalendar')) {
				entity.isManageCalendar = this.manageCalendar == null ? false : this.manageCalendar;
			}
			if (paramMap.containsKey('manageJobType')) {
				entity.isManageJobType = this.manageJobType == null ? false : this.manageJobType;
			}
			if (paramMap.containsKey('manageJob')) {
				entity.isManageJob = this.manageJob == null ? false : this.manageJob;
			}
			if (paramMap.containsKey('manageMobileSetting')) {
				entity.isManageMobileSetting = this.manageMobileSetting == null ? false : this.manageMobileSetting;
			}
			if (paramMap.containsKey('managePlannerSetting')) {
				entity.isManagePlannerSetting = this.managePlannerSetting == null ? false : this.managePlannerSetting;
			}
			if (paramMap.containsKey('managePermission')) {
				entity.isManagePermission = this.managePermission == null ? false : this.managePermission;
			}
			if (paramMap.containsKey('manageAttLeave')) {
				entity.isManageAttLeave = this.manageAttLeave == null ? false : this.manageAttLeave;
			}
			if (paramMap.containsKey('manageAttShortTimeWorkSetting')) {
				entity.isManageAttShortTimeWorkSetting = this.manageAttShortTimeWorkSetting == null ? false : this.manageAttShortTimeWorkSetting;
			}
			if (paramMap.containsKey('manageAttLeaveOfAbsence')) {
				entity.isManageAttLeaveOfAbsence = this.manageAttLeaveOfAbsence == null ? false : this.manageAttLeaveOfAbsence;
			}
			if (paramMap.containsKey('manageAttWorkingType')) {
				entity.isManageAttWorkingType = this.manageAttWorkingType == null ? false : this.manageAttWorkingType;
			}
			if (paramMap.containsKey('manageAttPattern')) {
				entity.isManageAttPattern = this.manageAttPattern == null ? false : this.manageAttPattern;
			}
			if (paramMap.containsKey('manageAttAgreementAlertSetting')) {
				entity.isManageAttAgreementAlertSetting = this.manageAttAgreementAlertSetting == null ? false : this.manageAttAgreementAlertSetting;
			}
			if (paramMap.containsKey('manageAttLeaveGrant')) {
				entity.isManageAttLeaveGrant = this.manageAttLeaveGrant == null ? false : this.manageAttLeaveGrant;
			}
			if (paramMap.containsKey('manageAttShortTimeWorkSettingApply')) {
				entity.isManageAttShortTimeWorkSettingApply = this.manageAttShortTimeWorkSettingApply == null ? false : this.manageAttShortTimeWorkSettingApply;
			}
			if (paramMap.containsKey('manageAttPatternApply')) {
				entity.isManageAttPatternApply = this.manageAttPatternApply == null ? false : this.manageAttPatternApply;
			}
			if (paramMap.containsKey('manageAttLeaveOfAbsenceApply')) {
				entity.isManageAttLeaveOfAbsenceApply = this.manageAttLeaveOfAbsenceApply == null ? false : this.manageAttLeaveOfAbsenceApply;
			}
			if (paramMap.containsKey('manageTimeSetting')) {
				entity.isManageTimeSetting = this.manageTimeSetting == null ? false : this.manageTimeSetting;
			}
			if (paramMap.containsKey('manageTimeWorkCategory')) {
				entity.isManageTimeWorkCategory = this.manageTimeWorkCategory == null ? false : this.manageTimeWorkCategory;
			}
			if (paramMap.containsKey('manageExpTypeGroup')) {
				entity.isManageExpTypeGroup = this.manageExpTypeGroup == null ? false : this.manageExpTypeGroup;
			}
			if (paramMap.containsKey('manageExpenseType')) {
				entity.isManageExpType = this.manageExpenseType == null ? false : this.manageExpenseType;
			}
			if (paramMap.containsKey('manageTaxType')) {
				entity.isManageExpTaxType = this.manageTaxType == null ? false : this.manageTaxType;
			}
			if (paramMap.containsKey('manageExpSetting')) {
				entity.isManageExpSetting = this.manageExpSetting == null ? false : this.manageExpSetting;
			}
			if (paramMap.containsKey('manageExchangeRate')) {
				entity.isManageExpExchangeRate = this.manageExchangeRate == null ? false : this.manageExchangeRate;
			}
			if (paramMap.containsKey('manageAccountingPeriod')) {
				entity.isManageExpAccountingPeriod = this.manageAccountingPeriod == null ? false : this.manageAccountingPeriod;
			}
			if (paramMap.containsKey('manageReportType')) {
				entity.isManageExpReportType = this.manageReportType == null ? false : this.manageReportType;
			}
			if (paramMap.containsKey('manageCostCenter')) {
				entity.isManageExpCostCenter = this.manageCostCenter == null ? false : this.manageCostCenter;
			}
			if (paramMap.containsKey('manageVendor')) {
				entity.isManageExpVendor = this.manageVendor == null ? false : this.manageVendor;
			}
			if (paramMap.containsKey('manageExtendedItem')) {
				entity.isManageExpExtendedItem = this.manageExtendedItem == null ? false : this.manageExtendedItem;
			}
			if (paramMap.containsKey('manageEmployeeGroup')) {
				entity.isManageExpEmployeeGroup = this.manageEmployeeGroup == null ? false : this.manageEmployeeGroup;
			}
			if (paramMap.containsKey('manageExpCustomHint')) {
				entity.isManageExpCustomHint = this.manageExpCustomHint == null ? false : this.manageExpCustomHint;
			}
			return entity;
		}
	}

	/*
	 * @description 作成・更新APIのレスポンスパラメータ。
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成・更新した権限レコードID */
		public String id;
	}

	/**
	 * @description 権限レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_PERMISSION;

		/**
		 * @description 権限レコードを1件作成する
		 * @param  req リクエストパラメータ(PermissionResource.SaveRequest)
		 * @return レスポンス(PermissionResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SaveRequest param = (SaveRequest)req.getParam(SaveRequest.class);
			Map<String, Object> paramMap = req.getParamMap();
			Boolean isUpdate = false;
			param.validate(isUpdate);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 保存処理
			Id resultId = new PermissionService().savePermission(param.createPermissionEntity(paramMap, isUpdate));

			// 成功レスポンスをセットする
			SaveResult res = new SaveResult();
			res.id = resultId;
			return res;
		}
	}

	/**
	 * @description 権限レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_PERMISSION;

		/**
		 * @description 権限レコードを1件作成する
		 * @param  req リクエストパラメータ(PermissionResource.SaveRequest)
		 * @return レスポンス(PermissionResource.SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SaveRequest param = (SaveRequest)req.getParam(SaveRequest.class);
			Map<String, Object> paramMap = req.getParamMap();
			Boolean isUpdate = true;
			param.validate(isUpdate);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 保存処理
			Id resultId = new PermissionService().savePermission(param.createPermissionEntity(paramMap, isUpdate));

			// 成功レスポンスをセットする
			SaveResult res = new SaveResult();
			res.id = resultId;
			return res;
		}
	}

		/**
	 * @description 削除対象の権限レコードIDを格納するクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象の権限レコードID */
		public String id;

		/** パラメータを検証する */
		public void validate() {
			// ID
			if (String.isBlank(this.id)) {
				App.ParameterException e = new App.ParameterException();
				e.setMessageIsBlank('id');
				throw e;
			}
			try {
				System.Id.valueOf(this.id);
			} catch (Exception e) {
				throw new App.ParameterException('id', id);
			}
		}
	}

	/**
	 * @description 権限レコード削除API
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_PERMISSION;

		/**
		 * @description 権限レコードを削除する
		 * @param  req リクエストパラメータ(DeleteRequest)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteRequest param = (DeleteRequest)req.getParam(DeleteRequest.class);
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new PermissionService().deletePermission(System.Id.valueOf(param.id));
			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

					// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
					//      暫定対応として下記のようなエラーメッセージで対応します。
					// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
					e.setMessage(ComMessage.msg().Com_Err_FaildDeleteReference);
					throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		}
	}

}