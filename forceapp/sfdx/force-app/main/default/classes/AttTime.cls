/**
 * 勤務時刻の値オブジェクト
 * その日の0:00を0とした分で表す時刻
 * 1440 以降は翌時 0未満は前日の時刻を表す
 */
public with sharing class AttTime extends ValueObject implements Comparable {

	/** 1時間当たりの分 */
	private static final Integer MINUTES_PER_HOUR = 60;

	/**
	 * その日の0:00を0とした分で表す時刻
	 * 1440 以降は翌時 0未満は前日の時刻を表す
	 */
	public Integer value {
		private get;
		private set {
			// 必要なら値を検証する
			this.value = value;
		}
	}

	/**
	 * コンストラクタ（分を指定）
	 * @param value その日の0:00を0とした分で表す時刻
	 */
	public AttTime(Integer value) {
		this.value = value;
	}

	/**
	 * 時、分の情報からインスタンスを作成する
	 * @param hour 時
	 * @param minute 分
	 * @return 作成したAttTime
	 */
	public static AttTime newInstance(Integer hour, Integer minute) {

		// TODO パラメータの検証
		if (hour < 0) {
			return new AttTime(hour * 60 - minute);
		}

		return new AttTime(hour * 60 + minute);
	}

	/**
	 * AttTimeをInteger型に変換する
	 * @param 変換対象の値
	 * @return 変換後のInteger値, valueがnullの場合はnullを返却する
	 */
	public static Integer convertInt(AttTime attValue) {
		if (attValue == null) {
			return null;
		}
		return attValue.value;
	}

	/**
	 * AttTimeをInteger型に変換する
	 * @param 変換対象の値
	 * @return 変換後のInteger値, valueがnullの場合はnullを返却する
	 */
	public static Integer intValue(AttTime attValue) {
		return convertInt(attValue);
	}

	/**
	 * 現在値に分を追加
	 * FIXME:現在値を更新しない方針
	 * @param 追加分
	 * @return 追加後の対象
	 */
	public AttTime addMinutes(Integer minutes) {
		this.value += minutes;
		return this;
	}

	/**
	 * 現在値に時を追加
	 * @param 追加時間
	 * @return 追加後の対象
	 */
	public AttTime addHours(Integer hours) {
		return AttTime.valueOf(this.value + hours * MINUTES_PER_HOUR);
	}


	/**
	 * Integer型の値をAttTimeに変換する
	 * @param Integer型の値
	 * @return 作成したAttTime
	 */
	public static AttTime valueOf(Integer value) {
		if (value == null) {
			return null;
		}
		return new AttTime(value);
	}

	/**
	 * Decimal型の値をAttTimeに変換する
	 * @param Decimal型の値
	 * @return 作成したAttTime
	 */
	public static AttTime valueOf(Decimal value) {
		return valueOf(Integer.valueOf(value));
	}
	/**
	 * Time型の値をAttTimeに変換する
	 * @param Time型の値
	 * @return 作成したAttTime
	 */
	public static AttTime valueOf(Time value) {
		return valueOf(value.hour() * 60 + value.minute());
	}
	/**
	 * Object型の値をAttTimeに変換する
	 * @param Object型の値
	 * @return 作成したAttTime
	 */
	public static AttTime valueOf(Object value) {
		if (value == null) {
			return null;
		}
		return new AttTime(Integer.valueOf(value));
	}
	/**
	 * 値をInteger型で取得する
	 * @return Intger型に変換した値
	 */
	public Integer getIntValue() {
		return value;
	}

	/**
	 * 値をDecimal型で取得する
	 * @return Decimal型に変換した値
	 */
	public Decimal getDecimalValue() {
		return Decimal.valueOf(value);
	}

	/**
	 * Hourを返す
	 * @return Hour
	 */
	public Integer getHour() {
		if (this.value == null) {
			return null;
		}

		Integer hour = this.value / MINUTES_PER_HOUR;
		return hour;
	}

	/**
	 * Minuteを返す
	 * @return Minute
	 */
	public Integer getMinute() {
		if (this.value == null) {
			return null;
		}

		Integer minute = this.value - (this.value / MINUTES_PER_HOUR) * MINUTES_PER_HOUR;
		return minute;
	}

	/**
	 * HH:mm 形式の文字列を返却する
	 */
	public String format() {
		if (this.value == null) {
			return null;
		}
		Integer hourVal = getHour();
		String hour;
		Integer absHourVal = Math.abs(hourVal);
		if (absHourVal < 10) {
			if (hourVal < 0) {
				hour = '-0' + String.valueOf(absHourVal);
			} else {
				hour = '0' + String.valueOf(absHourVal);
			}
		} else {
			hour = String.valueOf(absHourVal);
		}

		String minute = String.valueOf(getMinute()).replace('-', '');
		if (minute.length() <= 1) {
			// 0埋め
			minute = '0' + minute;
		}
		String str = hour + ':' + minute;
		return str;
	}

	/**
	 * オブジェクトの値が等しいかどうか判定する
	 * @param compare 比較対象のオブジェクト
	 */
	public override Boolean equals(Object compare) {
		if (compare instanceof AttTime) {
			return this.value == ((AttTime)compare).value;
		}
		return false;
	}

	/**
	 * 比較の結果であるinteger値を返します
	 * @param 比較対象
	 * @return このインスタンスとcompareToが等しい場合は0、より大きい場合は1以上、より小さい場合は0未満
	 */
	public Integer compareTo(Object compareTo) {
		AttTime compareTime = (AttTime)compareTo;
		if (this.value == compareTime.value) {
			return 0;
		} else if (this.value > compareTime.value) {
			return 1;
		}
		// this.value < compareTime.value
		return -1;
	}

	/** toStringのオーバーライド */
	public override String toString() {
		String outStr;
		if (this.value != null) {
			outStr = this.value.format();
		} else {
			// 本来ありえないはずですが・・
			outStr = null;
		}
		return outStr;
	}

}
