/**
 * @group 工数
 *
 * 工数明細のエンティティ
 */
public with sharing class TimeRecordEntity extends TimeRecordGeneratedEntity {

	/** 作業報告の最大文字数 */
	public static final Integer NOTE_MAX_LENGTH = 4096;//　文字数はSFの最大桁ではなく、業務運用上想定される最大桁4096とする

	/**
	 * コンストラクタ
	 */
	public TimeRecordEntity() {
		super(new TimeRecord__c());
	}

	/**
	 * コンストラクタ
	 */
	public TimeRecordEntity(TimeRecord__c sobj) {
		super(sobj);
	}

	/**
	 * 工数内訳リスト
	 * 内訳が存在しない場合は空のリストを返す
	 */
	public List<TimeRecordItemEntity> recordItemList {
		get {
			if (recordItemList == null) {
				recordItemList = new List<TimeRecordItemEntity>();
			}
			return recordItemList;
		}
		private set;
	}

	/**
	 * 作業時間
	 * Time
	 * 値がnullの場合にデフォルト値を返すために、GeneratedEntityから除外しています。
	 */
	public static final Schema.SObjectField FIELD_WORK_TIME = TimeRecord__c.Time__c;
	public Integer workTime {
		get {
			Integer value = Integer.valueOf(getFieldValue(FIELD_WORK_TIME));
			return value == null ? 0 : value;
		}
		private set;
	}

	/**
	 * 工数サマリーのロックフラグ
	 * 工数確定申請が「承認待ち」or「承認済み」の場合に、trueを返す
	 */
	public Boolean isSummaryLocked {
		get {
			if (sobj.TimeSummaryId__r == null) {
				return false;
			}
			if (isSummaryLocked == null) {
				isSummaryLocked = sobj.TimeSummaryId__r.Locked__c;
			}
			return isSummaryLocked;
		}
		private set;
	}

	/**
	 * 工数内訳リストをリプレースする（リストをコピーする）
	 * @param recordItemList リプレース対象の工数内訳リスト
	 */
	public void replaceRecordItemList(List<TimeRecordItemEntity> recordItemList) {
		this.recordItemList = new List<TimeRecordItemEntity>(recordItemList);
	}

	/**
	 * 工数明細名を作成する
	 * @param employeeCode 社員コード
	 * @param employeeName 社員名
	 * @param targetDate 日付
	 * @return 工数明細名
	 */
	public static String createName(String employeeCode, String employeeName, AppDate targetDate) {
		final Integer empLength = 72; // 社員情報の文字列長さ

		if (String.isBlank(employeeCode)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeCode is blank');
		}
		if (String.isBlank(employeeName)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeName is blank');
		}
		if (targetDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('targetDate is null');
		}

		// 工数明細名作成
		return (employeeCode + employeeName).left(empLength) + targetDate.formatYYYYMMDD();
	}

	/**
	 * ユニークキーを作成する
	 * @param summary サマリー
	 * @param targetDate 作成対象日
	 * @return ユニークキー
	 */
	public static String createUniqKey(TimeSummaryEntity summary, AppDate targetDate) {
		if (summary == null) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]工数明細のユニークキーが作成できませんでした。工数サマリーが空です。');
		}
		if (targetDate == null) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]工数明細のユニークキーが作成できませんでした。対象日が空です。');
		}

		// 社員コード - 工数サマリー年月度 - subNo
		return summary.id + targetDate.formatYYYYMMDD();
	}
}