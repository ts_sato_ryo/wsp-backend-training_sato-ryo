/**
 * @group 承認
 *
 * @description プロセスインスタンス履歴のエンティティ
 */
public with sharing class ApprovalProcessWorkitemEntity extends ApprovalProcessWorkitemGeneratedEntity {

	/**
	 * @description コンストラクタ
	 */
	public ApprovalProcessWorkitemEntity() {
		super(new ProcessInstanceWorkitem());
	}

	/**
	 * @description コンストラクタ
	 * @param sobj エンティティのデータを保持するSObject
	 */
	public ApprovalProcessWorkitemEntity(ProcessInstanceWorkitem sobj) {
		super(sobj);
	}

	/** 現在承認を担当しているユーザ,キューの名前 */
	public String actorName {
		get {
			return sobj.Actor.Name;
		}
	}

	/** 承認申請対象のオブジェクトのID */
	public String targetObjectId {
		get {
			return sobj.ProcessInstance.TargetObjectId;
		}
	}
}