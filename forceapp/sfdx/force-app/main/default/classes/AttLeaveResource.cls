/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 休暇レコード操作APIを実装するクラス
 */
public with sharing class AttLeaveResource {

	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 休暇レコードを表すクラス
	 */
	public class AttLeave implements RemoteApi.RequestParam {
		/** ベースレコードID */
		public String id;
		/** 休暇名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 休暇名(言語0) */
		public String name_L0;
		/** 休暇名(言語1) */
		public String name_L1;
		/** 休暇名(言語2) */
		public String name_L2;
		/** 休暇コード */
		public String code;
		/** 会社レコードID */
		public String companyId;
		/** 休暇種別 */
		public String leaveType;
		/** 日数管理 */
		public boolean daysManaged;
		/** 休暇範囲 */
		public List<String> leaveRanges;
		/** 勤務カウント種別 */
		public String countType;
		/** 理由を求める */
		public Boolean requireReason;
		/** 集計No */
		public String summaryItemNo;
		/** 並び順 */
		public String order;
		/** 有効開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;

		/**
		 * パラメータの値を設定したTeamSpirit 休暇エンティティを作成する。
		 * @param  paramMap リクエストパラメエータのMap
		 * @param  isUpdate true:更新を行う場合に設定, false:新規作成を行う場合に設定
		 * @return 更新情報を設定したエンティティ
		 */
		public AttLeaveEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {
			
			/* 空エンティティを定義 */
			AttLeaveEntity entity = new AttLeaveEntity();

			/* レコード更新時はIDをセット */
			if (isUpdate) {
				entity.setId(this.id);
			}

			/* 休暇名(言語0) */
			if (paramMap.containsKey('name_L0')) {
				entity.name = this.name_L0;
				entity.nameL0 = this.name_L0;
			}

			/* 休暇名(言語1) */
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}

			/* 休暇名(言語2) */
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}

			/* 休暇コード */
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}

			/* 会社レコードID */
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}

			/* 休暇種別 */
			if (paramMap.containsKey('leaveType')) {
				entity.leaveType = AttLeaveType.valueOf(this.leaveType);
			}

			/* 日数管理 */
			if (paramMap.containsKey('daysManaged')) {
				entity.daysManaged = this.daysManaged;
			}
			if (entity.daysManaged == null){
				entity.daysManaged = false;
			}

			/* 休暇範囲 */
			if (paramMap.containsKey('leaveRanges') && this.leaveRanges != null) {
				for (String range : this.leaveRanges) {
					AttLeaveRange attLeaveRange = AttLeaveRange.valueOf(range);
					entity.leaveRanges.add(attLeaveRange);
				}
			}

			/* 勤務カウント種別 */
			if (paramMap.containsKey('countType')) {
				entity.countType = AttCountType.valueOf(this.countType);
			}

			/* 理由を求める */
			if (paramMap.containsKey('requireReason')) {
				entity.requireReason = this.requireReason;
			}

			/* 集計No */
			if (paramMap.containsKey('summaryItemNo') && this.summaryItemNo != null) {
				entity.summaryItemNo = Integer.valueOf(this.summaryItemNo);
			}

			/* 並び順 */
			if (paramMap.containsKey('order') && this.order != null) {
				entity.order = Integer.valueOf(this.order);
			}

			/* 有効開始日 */
			if (paramMap.containsKey('validDateFrom')) {
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (entity.validFrom == null) {
				entity.validFrom = AppDate.today(); /* nullの場合は本日の日付をセットする。*/
			}

			/* 失効日 */
			if (paramMap.containsKey('validDateTo')) {
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (entity.validTo == null) {
				entity.validTo = ValidPeriodEntity.VALID_TO_MAX; /* nullの場合は2101/1/1をセットする */
			}

			return entity;
		}

		/**
		 * パラメータを検証する
		 */
		public void validate(Boolean isUpdate) {
			// TODO 入力チェックを実装して下さい
			// ここで判定すべきエラーは、フロントから送られてきた情報の型があっているかを確認する。
			// やること：必須チェック・型チェック
			// やらないこと：桁数チェック・必須チェック・ValidToなどの有効チェック
			// IDがブランクの場合はエラー
			if (isUpdate && String.isBlank(this.id)) {
				throw new App.ParameterException('パラメータ "id" を指定して下さい');
			}

			//型チェック
			if (String.isNotBlank(this.companyId)) {
				try {
					System.Id.ValueOf(this.companyId);
				} catch (Exception e) {
					throw new App.ParameterException('companyId', this.companyId);
				}
			}
			if (String.isNotBlank(this.summaryItemNo)) {
				AppParser.validateParseInteger('summaryItemNo', this.summaryItemNo);
			}

			if (String.isNotBlank(this.order)) {
				AppParser.validateParseInteger('order', this.order);
			}

		}
	}

	/**
	 * @description 社員ベースレコード作成結果レスポンス
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したレコードID */
		public String id;
	}

	/**
	 * @description 社員レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE;

		/**
		 * @description 社員レコードを1件作成する
		 * @param  req リクエストパラメータ(Leave)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			final Boolean isUpdate = false;

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			AttLeave param = (AttLeave)req.getParam(AttLeave.class);

			// パラメータのバリデーション
			param.validate(isUpdate);

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			//エンティティの作成
			AttLeaveEntity entity = param.createEntity(req.getParamMap(), isUpdate);

			//サービスを定義し、保存する。
			AttLeaveService attLeaveService = new AttLeaveService();
			Repository.SaveResult saveResult = attLeaveService.saveLeaveEntity(entity);
			
			//返却用オブジェクトに保存結果を格納する。
			SaveResult response = new SaveResult();
			response.id = saveResult.details[0].id;
			
			return response;
		}
	}

	/**
	 * @desctiprion 休暇レコード更新処理を実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE;

		/**
		 * @description 社員レコードを1件更新する
		 * @param  req リクエストパラメータ(Employee)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			final Boolean isUpdate = true;

			AttLeave param = (AttLeave)req.getParam(AttLeave.class);

			// パラメータのバリデーション
			param.validate(isUpdate);

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// TODO 更新処理を実装してください
			//エンティティの作成
			AttLeaveEntity entity = param.createEntity(req.getParamMap(), isUpdate);

			//サービスを定義し、保存する。
			AttLeaveService attLeaveService = new AttLeaveService();
			Repository.SaveResult saveResult = attLeaveService.saveLeaveEntity(entity);

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 休暇レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;
	}

	/**
	 * 休暇レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_LEAVE;

		/**
		 * @description 休暇レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// IDがブランクの場合はエラー
			if (String.isBlank(param.id)) {
				throw new App.ParameterException('パラメータ "id" を指定して下さい');
			}

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				(new AttLeaveRepository()).deleteEntity(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {

					// TODO 暫定的にデバッグログを出力しておきます
					System.debug(e);

					// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
					//      現状では工数の問題で実装ができないため下記のようなエラーメッセージで対応します。
					// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
					e.setMessage(MESSAGE.Com_Err_FaildDeleteReference);
					throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		}

	}

	/**
	 * @description 休暇レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 関連する会社レコードID */
		public String companyId;
		/** 対象日付 */
		public Date targetDate; //使用しない
	}

	/**
	 * @description 休暇レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public AttLeave[] records;
	}

	public with sharing class SearchApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {
	
			//リクエストパラメータのキャスト
			AttLeaveResource.SearchCondition param = 
					(AttLeaveResource.SearchCondition)req.getParam(AttLeaveResource.SearchCondition.class);
	
			//リクエストからフィルターを作成する。
			AttLeaveRepository.SearchFilter filter = new AttLeaveRepository.SearchFilter();

			if (param.id != null) {
				filter.id = param.id;
			}
			if (param.companyId != null) {
				filter.companyId = param.companyId;
			}

			// フィルターを利用して検索
			AttLeaveRepository repo = new AttLeaveRepository();
			List<AttLeaveEntity> entityList = repo.searchEntityList(filter);

			//レスポンスの設定
			AttLeaveResource.SearchResult res = new AttLeaveResource.SearchResult();
			res.records = new List<AttLeave>();

			for (AttLeaveEntity e : entityList) {
				AttLeave record = new AttLeave();

				record.id = e.id;
				record.code = e.code;
				record.name = e.nameL.getValue();
				record.name_L0 = e.nameL0;
				record.name_L1 = e.nameL1;
				record.name_L2 = e.nameL2;
				record.companyId = e.companyId;
				record.daysManaged = e.daysManaged;
				record.requireReason = e.requireReason;

				//ValueObjectType -> String
				record.leaveType = AppConverter.stringValue(e.leaveType);
				record.countType = AppConverter.stringValue(e.countType);

				//List<AttLeaveRange> -> String[]
				record.leaveRanges = new List<String>();
				for (AttLeaveRange range : e.leaveRanges) {
					record.leaveRanges.add(AppConverter.stringValue(range));
				}
				
				//Integer -> String
				record.order = String.valueOf(e.order);				
				record.summaryItemNo = String.valueOf(e.summaryItemNo);
				
				//AppDate -> Date
				record.validDateFrom = AppDate.convertDate(e.validFrom);
				record.validDateTo = AppDate.convertDate(e.validTo);

				res.records.add(record);
			}

			return res;
	
		}
	}

}
