/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Test class for ExpRecentlyUsedService
 * 
 * @group Expense
 */
@isTest
private class ExpRecentlyUsedServiceTest {

	/*
	 * Test that the method will returns the recently used values in the correct order,
	 * and if there is no recently used, then it will return empty list
	 */
	@IsTest static void getRecentlyUsedExtendedItemCustomOptionListPositiveTest() {
		ExpTestData testData = new ExpTestData();

		// Test 1: When there is no recently used exist, empty list is returned -> OK
		List<ExtendedItemCustomOptionEntity> recentlyUsedOptionList = new ExpRecentlyUsedService().getRecentlyUsedExtendedItemCustomOptionList(
				testData.employee.id, testData.extendedItemLookupList.get(0).Id,
				testData.extendedItemLookupList.get(5).ExtendedItemCustomId__c);
		System.assertEquals(true, recentlyUsedOptionList.isEmpty());

		// Test 2: When there are recently used items, check that they are returned in the correct order
		List<String> recentlyUsedCodes = new List<String>();
		for (Integer i = 10; i > 0; i--) {
			recentlyUsedCodes.add(testData.extendedItemCustomOptionList.get(i).Code__c);
		}
		createRecentlyUsedDataForExtendedItemLookup(testData.employee.id, testData.extendedItemLookupList.get(0).Id, recentlyUsedCodes);
		recentlyUsedOptionList = new ExpRecentlyUsedService().getRecentlyUsedExtendedItemCustomOptionList(
				testData.employee.id, testData.extendedItemLookupList.get(0).Id,
				testData.extendedItemLookupList.get(5).ExtendedItemCustomId__c);

		System.assertEquals(10, recentlyUsedOptionList.size());
		for (Integer i = 0; i < 10; i++) {
			System.assertEquals(recentlyUsedCodes.get(i), recentlyUsedOptionList.get(i).code);
		}
	}

	private static void createRecentlyUsedDataForExtendedItemLookup(Id employeeBaseId, Id extendedItemLookupId, List<String> codes) {
		ExpRecentlyUsed__c recentlyUsed = New ExpRecentlyUsed__c(
				EmployeeBaseId__c = employeeBaseId,
				MasterType__c = ExpRecentlyUsedEntity.MasterType.ExtendedItemLookup.name(),
				TargetId__c = extendedItemLookupId,
				RecentlyUsedValues__c = JSON.serialize(codes)
		);
		INSERT recentlyUsed;
	}

	/*
	 * Test the extendedItemLookup recently used values will get saved correctly.
	 * Tests:
	 *  1. When there is no previously used
	 *  2. When some of the older recently used are used again
	 *  3. When there are aleady max recently used, and new values are used
	 */
	@IsTest static void saveRecentlyUsedExtendedItemLookupValuesPositiveTest() {
		ExpTestData testData = new ExpTestData();
		ExpRecentlyUsedService service = new ExpRecentlyUsedService();
		Id extendedItemLookupId = testData.extendedItemLookupList.get(0).Id;

		Map<Id, Set<String>> extendedItemIdOptionCodeMap = new Map<Id, Set<String>>();
		Set<String> codes = new Set<String>();
		Integer maxRecentlyUsedCount = ExpRecentlyUsedService.MAX_EXTENDED_ITEM_LOOKUP_RECENTLY_USED;

		// Test 1: Recently Use can be stored successfully when there are no existing recently used -> OK
		for (Integer i = 0; i < maxRecentlyUsedCount; i++) {
			codes.add(String.valueOf(i));
		}
		extendedItemIdOptionCodeMap.put(extendedItemLookupId, codes);
		List<String> recentlyUsedCodeList = saveAndRetrieveRecentlyUsedCodeList(testData.employee.id,
				extendedItemLookupId, extendedItemIdOptionCodeMap);

		for (Integer i = 0; i < maxRecentlyUsedCount; i++) {
			System.assertEquals(String.valueOf(i), recentlyUsedCodeList.get(i));
		}

		// Test 2: When an existing Recently used is used again, it will be bring forward
		// Initially, it's 0, 1, 2, ..., 9 -> and should becomes 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5
		codes = new Set<String>();
		Integer offset = maxRecentlyUsedCount / 2;
		for (Integer i = offset; i < maxRecentlyUsedCount; i++) {
			codes.add(String.valueOf(i));
		}
		extendedItemIdOptionCodeMap.put(extendedItemLookupId, codes);
		recentlyUsedCodeList = saveAndRetrieveRecentlyUsedCodeList(testData.employee.id,
				extendedItemLookupId, extendedItemIdOptionCodeMap);
		for (Integer i = offset; i < maxRecentlyUsedCount; i++) {
			System.assertEquals(String.valueOf(i), recentlyUsedCodeList.get(i - offset));
		}
		for (Integer i = 0; i < offset; i++) {
			System.assertEquals(String.valueOf(i), recentlyUsedCodeList.get(i + offset));
		}

		// Test 3; When there are more than max allows, oldest recently used will be discarded
		// Initially: 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5 -> should becomes 10, 11, 12, 13, 14, 5, 6, 7, 8, 9
		codes = new Set<String>();
		for (Integer i = maxRecentlyUsedCount; i < (maxRecentlyUsedCount + offset); i++) {
			codes.add(String.valueOf(i));
		}
		extendedItemIdOptionCodeMap.put(extendedItemLookupId, codes);
		recentlyUsedCodeList = saveAndRetrieveRecentlyUsedCodeList(testData.employee.id,
				extendedItemLookupId, extendedItemIdOptionCodeMap);
		for (Integer i = maxRecentlyUsedCount; i < (maxRecentlyUsedCount + offset); i++) {
			System.assertEquals(String.valueOf(i), recentlyUsedCodeList.get(i - maxRecentlyUsedCount));
		}
		for (Integer i = offset; i < maxRecentlyUsedCount; i++) {
			System.assertEquals(String.valueOf(i), recentlyUsedCodeList.get(i));
		}
	}

	/*
	 * Called the {@code ExpRecentlyUsedService}
	 */
	private static List<String> saveAndRetrieveRecentlyUsedCodeList(Id employeeBaseId, Id extendedItemLookupId, Map<Id, Set<String>> extendedItemIdOptionCodeMap) {
		new ExpRecentlyUsedService().saveRecentlyUsedExtendedItemLookupValues(extendedItemIdOptionCodeMap, employeeBaseId);
		List<ExpRecentlyUsed__c> recentlyUsedSObjList = [SELECT ID, MasterType__c, TargetId__c, RecentlyUsedValues__c
			FROM ExpRecentlyUsed__c
			WHERE TargetId__c = :extendedItemLookupId
		];
		System.assertEquals(1, recentlyUsedSObjList.size());
		ExpRecentlyUsed__c recentlyUsedSObj = recentlyUsedSObjList[0];
		System.assertEquals(ExpRecentlyUsedEntity.MasterType.ExtendedItemLookup.name(), recentlyUsedSObj.MasterType__c);
		List<String> retCodes = (List<String>) Json.deserialize(recentlyUsedSObj.RecentlyUsedValues__c, List<String>.class);
		System.assertEquals(ExpRecentlyUsedService.MAX_EXTENDED_ITEM_LOOKUP_RECENTLY_USED, retCodes.size());
		return retCodes;
	}

	/*
	 * Test the costcenter recently used values will get saved correctly.
	 * Tests:
	 *  1. When there is no previously used
	 *  2. When some of the older recently used are used again
	 *  3. When there are already max recently used, and new values are used
	 *  4. When there are already max recently used and order changed
	 */
	@IsTest static void saveRecentlyUsedCostCenterPositiveTest() {
		ExpTestData testData = new ExpTestData();
		ExpRecentlyUsedService service = new ExpRecentlyUsedService();
		List<CostCenterBaseEntity> costCenterBaseEntityList = testData.createCostCenters(12);

		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c][0];

		// 1. When there is no previously used cost center
		service.saveRecentlyUsedCostCenterValues(testData.employee.id, null, costCenterBaseEntityList[0].getHistory(0).id);
		ExpRecentlyUsedEntity recentlyUsedEntity = retrieveRecentlyUsedCostCenter();

		System.assertEquals(1, recentlyUsedEntity.getRecentlyUsedValues().size());
		String idValue = costCenterBaseEntityList[0].getHistory(0).id;
		System.assertEquals(idValue , recentlyUsedEntity.getRecentlyUsedValues()[0]);

		// Data preparation for Case 2 - Current position is CC6 to CC0
		List<String> newRecentlyUsedValues = new List<String>();
		Integer sizeToInit = 7;
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(costCenterBaseEntityList[sizeToInit-i-1].getHistory(0).id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		// 2. When some of the older recently used are used again
		// Current position is CC2 CC6 CC5 CC4 CC3 CC1 CC0
		service.saveRecentlyUsedCostCenterValues(testData.employee.id, costCenterBaseEntityList[3].getHistory(0).id, costCenterBaseEntityList[2].getHistory(0).id);
		recentlyUsedEntity = retrieveRecentlyUsedCostCenter();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(costCenterBaseEntityList[2].getHistory(0).id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(costCenterBaseEntityList[6].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(costCenterBaseEntityList[5].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(costCenterBaseEntityList[4].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(costCenterBaseEntityList[3].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(costCenterBaseEntityList[1].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(costCenterBaseEntityList[0].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(6));

		// Data preparation for Case 3 - Current position is CC9 to CC0
		Integer MaxRecentlyUsedCC = 10;
		sizeToInit = MaxRecentlyUsedCC;
		newRecentlyUsedValues.clear();
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(costCenterBaseEntityList[sizeToInit-i-1].getHistory(0).id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		recentlyUsedEntity = retrieveRecentlyUsedCostCenter();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		// Current position is CC9 to CC0
		for (Integer i = 0; i < sizeToInit; i++ ) {
			String value = recentlyUsedEntity.getRecentlyUsedValues()[sizeToInit-i-1];
			System.assertEquals(costCenterBaseEntityList[i].getHistory(0).id, value);
		}

		// 3. When there are max recently used, and new values are used
		// Current position is CC11 CC9 CC8 CC7 CC6 CC5 CC4 CC3 CC2 CC1
		service.saveRecentlyUsedCostCenterValues(testData.employee.id, costCenterBaseEntityList[3].getHistory(0).id, costCenterBaseEntityList[11].getHistory(0).id);
		recentlyUsedEntity = retrieveRecentlyUsedCostCenter();
		System.assertEquals(MaxRecentlyUsedCC, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(costCenterBaseEntityList[11].getHistory(0).id, recentlyUsedEntity.getRecentlyUsedValue(0));
		for (Integer i = 1; i < MaxRecentlyUsedCC; i++) {
			System.assertEquals(costCenterBaseEntityList[MaxRecentlyUsedCC-i].getHistory(0).id, recentlyUsedEntity.getRecentlyUsedValue(i));
		}

		// 4. When there are already max recently used and order changed
		// Current position is CC5 CC11 CC9 CC8 CC7 CC6 CC4 CC3 CC2 CC1
		service.saveRecentlyUsedCostCenterValues(testData.employee.id, costCenterBaseEntityList[1].getHistory(0).id, costCenterBaseEntityList[5].getHistory(0).id);
		recentlyUsedEntity = retrieveRecentlyUsedCostCenter();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(costCenterBaseEntityList[5].getHistory(0).id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(costCenterBaseEntityList[11].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(costCenterBaseEntityList[9].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(costCenterBaseEntityList[8].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(costCenterBaseEntityList[7].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(costCenterBaseEntityList[6].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(costCenterBaseEntityList[4].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(6));
		System.assertEquals(costCenterBaseEntityList[3].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(7));
		System.assertEquals(costCenterBaseEntityList[2].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(8));
		System.assertEquals(costCenterBaseEntityList[1].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(9));
	}

	/**
	 * Get recently used cost center entity
	 * @return ExpRecentlyUsedEntity
	 */
	private static ExpRecentlyUsedEntity retrieveRecentlyUsedCostCenter() {
		String masterType = ExpRecentlyUsedEntity.MasterType.CostCenter.name();
		List<ExpRecentlyUsed__c> recentlyUsedSObjList = [SELECT ID, EmployeeBaseId__c, MasterType__c, Name, TargetId__c, RecentlyUsedValues__c
			FROM ExpRecentlyUsed__c
		]; //WHERE MasterType__c = :masterType
		return new ExpRecentlyUsedRepository().createEntity(recentlyUsedSObjList[0]);
	}

	/**
	 * Test getExistingExpRecentlyUsedEntity for Cost Center
	 */
	@IsTest static void getExistingExpRecentlyUsedEntityTest() {
		ExpTestData testData = new ExpTestData();
		ExpRecentlyUsedService service = new ExpRecentlyUsedService();
		List<CostCenterBaseEntity> costCenterBaseEntityList = testData.createCostCenters(12);

		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c][0];

		// 1. When there is no previously used cost center
		service.saveRecentlyUsedCostCenterValues(testData.employee.id, null, costCenterBaseEntityList[0].getHistory(0).id);
		ExpRecentlyUsedEntity recentlyUsedEntity = service.getExistingExpRecentlyUsedEntity(testData.employee.id, ExpRecentlyUsedEntity.MasterType.CostCenter, null);

		System.assertNotEquals(null , recentlyUsedEntity);
		System.assertEquals(1 , recentlyUsedEntity.getRecentlyUsedValues().size());
		String idValue = costCenterBaseEntityList[0].getHistory(0).id;
		System.assertEquals(idValue , recentlyUsedEntity.getRecentlyUsedValues()[0]);

		// Data preparation for Case 2 - Current position is CC6 to CC0
		List<String> newRecentlyUsedValues = new List<String>();
		Integer sizeToInit = 7;
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(costCenterBaseEntityList[sizeToInit-i-1].getHistory(0).id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		// 2. When some of the older recently used are used again
		// Current position is CC2 CC6 CC5 CC4 CC3 CC1 CC0
		service.saveRecentlyUsedCostCenterValues(testData.employee.id, costCenterBaseEntityList[3].getHistory(0).id, costCenterBaseEntityList[2].getHistory(0).id);
		recentlyUsedEntity = service.getExistingExpRecentlyUsedEntity(testData.employee.id, ExpRecentlyUsedEntity.MasterType.CostCenter, null);
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(costCenterBaseEntityList[2].getHistory(0).id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(costCenterBaseEntityList[6].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(costCenterBaseEntityList[5].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(costCenterBaseEntityList[4].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(costCenterBaseEntityList[3].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(costCenterBaseEntityList[1].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(costCenterBaseEntityList[0].getHistory(0).id,recentlyUsedEntity.getRecentlyUsedValue(6));
	}
	/**
	 * Test the job recently used values will get saved correctly.
	 * Tests:
	 *  1. When there is no previously used
	 *  2. When some of the older recently used are used again
	 *  3. When there are already max recently used, and new values are used
	 *  4. When there are already max recently used and order changed
	 */
	@IsTest static void saveRecentlyUsedJobPositiveTest() {
		ExpTestData testData = new ExpTestData();
		ExpRecentlyUsedService service = new ExpRecentlyUsedService();
		List<JobEntity> jobEntityList = testData.createJobList(12);

		// 1. When there is no previously used job
		service.saveRecentlyUsedJobValues(testData.employee.id, null, jobEntityList[0].id);
		ExpRecentlyUsedEntity recentlyUsedEntity = retrieveRecentlyUsedJob();

		System.assertEquals(1, recentlyUsedEntity.getRecentlyUsedValues().size());
		String idValue = jobEntityList[0].id;
		System.assertEquals(idValue , recentlyUsedEntity.getRecentlyUsedValues()[0]);

		// Data preparation for Case 2 - Current position is JJ6 to JJ0
		List<String> newRecentlyUsedValues = new List<String>();
		Integer sizeToInit = 7;
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(jobEntityList[sizeToInit-i-1].id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		// 2. When some of the older recently used are used again
		// Current position is JJ2 JJ6 JJ5 JJ4 JJ3 JJ1 JJ0
		service.saveRecentlyUsedJobValues(testData.employee.id, jobEntityList[3].id, jobEntityList[2].id);
		recentlyUsedEntity = retrieveRecentlyUsedJob();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(jobEntityList[2].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(jobEntityList[6].id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(jobEntityList[5].id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(jobEntityList[4].id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(jobEntityList[3].id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(jobEntityList[1].id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(jobEntityList[0].id,recentlyUsedEntity.getRecentlyUsedValue(6));

		// Data preparation for Case 3 - Current position is JJ9 to JJ0
		Integer MaxRecentlyUsedJJ = 10;
		sizeToInit = MaxRecentlyUsedJJ;
		newRecentlyUsedValues.clear();
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(jobEntityList[sizeToInit-i-1].id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		recentlyUsedEntity = retrieveRecentlyUsedJob();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		// Current position is JJ9 to JJ0
		for (Integer i = 0; i < sizeToInit; i++ ) {
			String value = recentlyUsedEntity.getRecentlyUsedValues()[sizeToInit-i-1];
			System.assertEquals(jobEntityList[i].id, value);
		}

		// 3. When there are max recently used, and new values are used
		// Current position is JJ11 JJ9 JJ8 JJ7 JJ6 JJ5 JJ4 JJ3 JJ2 JJ1
		service.saveRecentlyUsedJobValues(testData.employee.id, jobEntityList[3].id, jobEntityList[11].id);
		recentlyUsedEntity = retrieveRecentlyUsedJob();
		System.assertEquals(MaxRecentlyUsedJJ, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(jobEntityList[11].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		for (Integer i = 1; i < MaxRecentlyUsedJJ; i++) {
			System.assertEquals(jobEntityList[MaxRecentlyUsedJJ-i].id, recentlyUsedEntity.getRecentlyUsedValue(i));
		}

		// 4. When there are already max recently used and order changed
		// Current position is JJ5 JJ11 JJ9 JJ8 JJ7 JJ6 JJ4 JJ3 JJ2 JJ1
		service.saveRecentlyUsedJobValues(testData.employee.id, jobEntityList[1].id, jobEntityList[5].id);
		recentlyUsedEntity = retrieveRecentlyUsedJob();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(jobEntityList[5].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(jobEntityList[11].id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(jobEntityList[9].id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(jobEntityList[8].id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(jobEntityList[7].id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(jobEntityList[6].id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(jobEntityList[4].id,recentlyUsedEntity.getRecentlyUsedValue(6));
		System.assertEquals(jobEntityList[3].id,recentlyUsedEntity.getRecentlyUsedValue(7));
		System.assertEquals(jobEntityList[2].id,recentlyUsedEntity.getRecentlyUsedValue(8));
		System.assertEquals(jobEntityList[1].id,recentlyUsedEntity.getRecentlyUsedValue(9));
	}

	/**
	 * Get recently used job entity
	 * @return ExpRecentlyUsedEntity
	 */
	private static ExpRecentlyUsedEntity retrieveRecentlyUsedJob() {
		String masterType = ExpRecentlyUsedEntity.MasterType.Job.name();
		List<ExpRecentlyUsed__c> recentlyUsedSObjList = [SELECT ID, EmployeeBaseId__c, MasterType__c, Name, TargetId__c, RecentlyUsedValues__c
		FROM ExpRecentlyUsed__c
		]; //WHERE MasterType__c = :masterType
		return new ExpRecentlyUsedRepository().createEntity(recentlyUsedSObjList[0]);
	}

	/**
	 * Test getExistingExpRecentlyUsedEntity with MasterType.Job
	 */
	@IsTest static void getExistingExpRecentlyUsedEntityJobTest() {
		ExpTestData testData = new ExpTestData();
		ExpRecentlyUsedService service = new ExpRecentlyUsedService();
		List<JobEntity> jobEntityList = testData.createJobList(12);

		// 1. When there is no previously used job
		service.saveRecentlyUsedJobValues(testData.employee.id, null, jobEntityList[0].id);
		ExpRecentlyUsedEntity recentlyUsedEntity = service.getExistingExpRecentlyUsedEntity(testData.employee.id, ExpRecentlyUsedEntity.MasterType.Job, null);

		System.assertNotEquals(null , recentlyUsedEntity);
		System.assertEquals(1 , recentlyUsedEntity.getRecentlyUsedValues().size());
		String idValue = jobEntityList[0].id;
		System.assertEquals(idValue , recentlyUsedEntity.getRecentlyUsedValues()[0]);

		// Data preparation for Case 2 - Current position is JJ6 to JJ0
		List<String> newRecentlyUsedValues = new List<String>();
		Integer sizeToInit = 7;
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(jobEntityList[sizeToInit-i-1].id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		// 2. When some of the older recently used are used again
		// Current position is JJ2 JJ6 JJ5 JJ4 JJ3 JJ1 JJ0
		service.saveRecentlyUsedJobValues(testData.employee.id, jobEntityList[3].id, jobEntityList[2].id);
		recentlyUsedEntity = service.getExistingExpRecentlyUsedEntity(testData.employee.id, ExpRecentlyUsedEntity.MasterType.Job, null);
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(jobEntityList[2].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(jobEntityList[6].id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(jobEntityList[5].id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(jobEntityList[4].id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(jobEntityList[3].id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(jobEntityList[1].id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(jobEntityList[0].id,recentlyUsedEntity.getRecentlyUsedValue(6));
	}

	/**
	 * Test the ExpType recently used values will get saved correctly.
	 * Tests:
	 *  1. When there is no previously used
	 *  2. When some of the older recently used are used again
	 *  3. When there are already max recently used, and new values are used
	 *  4. When there are already max recently used and order changed
	 */
	@IsTest static void saveRecentlyUsedExpTypePositiveTest() {
		ExpTestData testData = new ExpTestData();
		Integer recordSize = 12;
		ExpRecentlyUsedService service = new ExpRecentlyUsedService();
		List<ExpTypeEntity> expTypeEntityList = testData.createExpTypes('ET', recordSize);

		List<ExpType__c> expTypeObjList = new List<ExpType__c>();
		List<Id> expenseTypeIdList = new List<Id>();
		for (Integer i = 0; i < recordSize; i++) {
			expTypeEntityList[i].validFrom = AppDate.today();
			expTypeObjList.add(new ExpTypeRepository().createObject(expTypeEntityList[i]));
			expenseTypeIdList.add(expTypeEntityList[i].Id);
		}
		update expTypeObjList;
		ExpReportType__c reportType = ComTestDataUtility.createExpReportType('RT', testData.company.Id);

		ComTestDataUtility.createExpReportTypeExpTypeLinkList(reportType.Id, expenseTypeIdList);

		// 1. When there is no previously used ExpType
		service.saveRecentlyUsedExpenseTypeValues(testData.employee.id, null, expTypeEntityList[0].id, reportType.Id);
		ExpRecentlyUsedEntity recentlyUsedEntity = retrieveRecentlyUsedExpType();

		System.assertEquals(1, recentlyUsedEntity.getRecentlyUsedValues().size());
		String idValue = expTypeEntityList[0].id;
		System.assertEquals(idValue , recentlyUsedEntity.getRecentlyUsedValues()[0]);

		// Data preparation for Case 2 - Current position is ET6 to ET0
		List<String> newRecentlyUsedValues = new List<String>();
		Integer sizeToInit = 7;
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(expTypeEntityList[sizeToInit-i-1].id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		// 2. When some of the older recently used are used again
		// Current position is ET2 ET6 ET5 ET4 ET3 ET1 ET0
		service.saveRecentlyUsedExpenseTypeValues(testData.employee.id, expTypeEntityList[3].id, expTypeEntityList[2].id, reportType.Id);
		recentlyUsedEntity = retrieveRecentlyUsedExpType();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(expTypeEntityList[2].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(expTypeEntityList[6].id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(expTypeEntityList[5].id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(expTypeEntityList[4].id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(expTypeEntityList[3].id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(expTypeEntityList[1].id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(expTypeEntityList[0].id,recentlyUsedEntity.getRecentlyUsedValue(6));

		// Data preparation for Case 3 - Current position is ET9 to ET0
		Integer MaxRecentlyUsedET = 10;
		sizeToInit = MaxRecentlyUsedET;
		newRecentlyUsedValues.clear();
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(expTypeEntityList[sizeToInit-i-1].id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		recentlyUsedEntity = retrieveRecentlyUsedExpType();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		// Current position is ET9 to ET0
		for (Integer i = 0; i < sizeToInit; i++ ) {
			String value = recentlyUsedEntity.getRecentlyUsedValues()[sizeToInit-i-1];
			System.assertEquals(expTypeEntityList[i].id, value);
		}

		// 3. When there are max recently used, and new values are used
		// Current position is ET11 ET9 ET8 ET7 ET6 ET5 ET4 ET3 ET2 ET1
		service.saveRecentlyUsedExpenseTypeValues(testData.employee.id, expTypeEntityList[3].id, expTypeEntityList[11].id, reportType.Id);
		recentlyUsedEntity = retrieveRecentlyUsedExpType();
		System.assertEquals(MaxRecentlyUsedET, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(expTypeEntityList[11].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		for (Integer i = 1; i < MaxRecentlyUsedET; i++) {
			System.assertEquals(expTypeEntityList[MaxRecentlyUsedET-i].id, recentlyUsedEntity.getRecentlyUsedValue(i));
		}

		// 4. When there are already max recently used and order changed
		// Current position is ET5 ET11 ET9 ET8 ET7 ET6 ET4 ET3 ET2 ET1
		service.saveRecentlyUsedExpenseTypeValues(testData.employee.id, expTypeEntityList[1].id, expTypeEntityList[5].id, reportType.Id);
		recentlyUsedEntity = retrieveRecentlyUsedExpType();
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(expTypeEntityList[5].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(expTypeEntityList[11].id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(expTypeEntityList[9].id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(expTypeEntityList[8].id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(expTypeEntityList[7].id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(expTypeEntityList[6].id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(expTypeEntityList[4].id,recentlyUsedEntity.getRecentlyUsedValue(6));
		System.assertEquals(expTypeEntityList[3].id,recentlyUsedEntity.getRecentlyUsedValue(7));
		System.assertEquals(expTypeEntityList[2].id,recentlyUsedEntity.getRecentlyUsedValue(8));
		System.assertEquals(expTypeEntityList[1].id,recentlyUsedEntity.getRecentlyUsedValue(9));
	}

	/**
	 * Get recently used ExpType entity
	 * @return ExpRecentlyUsedEntity
	 */
	private static ExpRecentlyUsedEntity retrieveRecentlyUsedExpType() {
		String masterType = ExpRecentlyUsedEntity.MasterType.ExpenseType.name();
		List<ExpRecentlyUsed__c> recentlyUsedSObjList = [SELECT ID, EmployeeBaseId__c, MasterType__c, Name, TargetId__c, RecentlyUsedValues__c
		FROM ExpRecentlyUsed__c
		WHERE MasterType__c = :masterType
		];
		return new ExpRecentlyUsedRepository().createEntity(recentlyUsedSObjList[0]);
	}

	/**
	 * Test getExistingExpRecentlyUsedEntity with MasterType.ExpenseType
	 * 1. When there is no previously used ExpType
	 * 2. When some of the older recently used are used again
	 */
	@IsTest static void getExistingExpRecentlyUsedEntityExpTypeTest() {
		ExpTestData testData = new ExpTestData();
		Integer recordSize = 12;
		ExpRecentlyUsedService service = new ExpRecentlyUsedService();
		List<ExpTypeEntity> expTypeEntityList = testData.createExpTypes('ET', recordSize);

		List<ExpType__c> expTypeObjList = new List<ExpType__c>();
		List<Id> expenseTypeIdList = new List<Id>();
		for (Integer i = 0; i < recordSize; i++) {
			expTypeEntityList[i].validFrom = AppDate.today();
			expTypeObjList.add(new ExpTypeRepository().createObject(expTypeEntityList[i]));
			expenseTypeIdList.add(expTypeEntityList[i].Id);
		}
		update expTypeObjList;
		ExpReportType__c reportType = ComTestDataUtility.createExpReportType('RT', testData.company.Id);

		ComTestDataUtility.createExpReportTypeExpTypeLinkList(reportType.Id, expenseTypeIdList);

		// 1. When there is no previously used ExpType
		service.saveRecentlyUsedExpenseTypeValues(testData.employee.id, null, expTypeEntityList[0].id, null);
		ExpRecentlyUsedEntity recentlyUsedEntity = service.getExistingExpRecentlyUsedEntity(testData.employee.id, ExpRecentlyUsedEntity.MasterType.ExpenseType, null);

		System.assertNotEquals(null , recentlyUsedEntity);
		System.assertEquals(1 , recentlyUsedEntity.getRecentlyUsedValues().size());
		String idValue = expTypeEntityList[0].id;
		System.assertEquals(idValue , recentlyUsedEntity.getRecentlyUsedValues()[0]);

		// Data preparation for Case 2 - Current position is ET6 to ET0
		List<String> newRecentlyUsedValues = new List<String>();
		Integer sizeToInit = 7;
		for (Integer i = 0; i < sizeToInit; i++ ) {
			newRecentlyUsedValues.add(expTypeEntityList[sizeToInit-i-1].id);
		}
		recentlyUsedEntity.replaceRecentlyUsedValues(newRecentlyUsedValues);
		upsert new ExpRecentlyUsedRepository().createObj(recentlyUsedEntity);

		// 2. When some of the older recently used are used again
		// Current position is ET2 ET6 ET5 ET4 ET3 ET1 ET0
		service.saveRecentlyUsedExpenseTypeValues(testData.employee.id, expTypeEntityList[3].id, expTypeEntityList[2].id, null);
		recentlyUsedEntity = service.getExistingExpRecentlyUsedEntity(testData.employee.id, ExpRecentlyUsedEntity.MasterType.ExpenseType, null);
		System.assertEquals(sizeToInit, recentlyUsedEntity.getRecentlyUsedValues().size());
		System.assertEquals(expTypeEntityList[2].id, recentlyUsedEntity.getRecentlyUsedValue(0));
		System.assertEquals(expTypeEntityList[6].id,recentlyUsedEntity.getRecentlyUsedValue(1));
		System.assertEquals(expTypeEntityList[5].id,recentlyUsedEntity.getRecentlyUsedValue(2));
		System.assertEquals(expTypeEntityList[4].id,recentlyUsedEntity.getRecentlyUsedValue(3));
		System.assertEquals(expTypeEntityList[3].id,recentlyUsedEntity.getRecentlyUsedValue(4));
		System.assertEquals(expTypeEntityList[1].id,recentlyUsedEntity.getRecentlyUsedValue(5));
		System.assertEquals(expTypeEntityList[0].id,recentlyUsedEntity.getRecentlyUsedValue(6));
	}
}