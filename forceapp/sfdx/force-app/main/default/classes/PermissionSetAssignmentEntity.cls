/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 権限セット割り当てエンティティ
 */
public with sharing class PermissionSetAssignmentEntity extends PermissionSetAssignmentGeneratedEntity {

	/**
	 * コンストラクタ
	 */
	public PermissionSetAssignmentEntity() {
		super(new PermissionSetAssignment());
	}

	/**
	 * コンストラクタ
	 */
	public PermissionSetAssignmentEntity(PermissionSetAssignment sobj) {
		super(sobj);
	}
}