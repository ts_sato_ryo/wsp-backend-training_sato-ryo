/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 勤怠申請のトリガーハンドラーテスト
 */
@isTest
private class AttRequestTriggerHandlerTest {
	static private Id createSummary(Id empHistoryId, Id workingTypeHistoryId, AppDate startDate, AppDate endDate) {
		AttSummary__c summary = new AttSummary__c(
				Name = 'Test' + startDate.format(),
				EmployeeHistoryId__c = empHistoryId,
				UniqKey__c = 'Test' + empHistoryId + startDate.formatYYYYMMDD(),
				StartDate__c = startDate.getDate(),
				EndDate__c = endDate.getDate(),
				SummaryName__c = startDate.format(AppDate.FormatType.YYYYMM),
				WorkingTypeHistoryId__c = workingTypeHistoryId,
				Locked__c = true);

		insert summary;
		return summary.Id;
	}
	@isTest static void getApprovedTargetTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		Id summaryId = createSummary(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, endDate);

		AttRequestEntity attRequest = testData.createRequest(
				testData.employee.getHistory(0).id,
				summaryId,
				AppRequestStatus.APPROVED,
				null);

		Map<Id, AttRequest__c> oldMap = new Map<Id, AttRequest__c>();
		Map<Id, AttRequest__c> newMap = new Map<Id, AttRequest__c>();

		AttRequestRepository requestRep = new AttRequestRepository();
		AttRequest__c attRequestOld = attRequest.createSObject();
		attRequestOld.Status__c = AppConverter.stringValue(AppRequestStatus.PENDING);

		AttRequest__c attRequestNew = attRequest.createSObject();
		attRequestNew.Status__c = AppConverter.stringValue(AppRequestStatus.APPROVED);
		oldMap.put(attRequest.id, attRequestOld);
		newMap.put(attRequest.id, attRequestNew);

		AttRequestTriggerHandler handler = new AttRequestTriggerHandler(oldMap, newMap);
		System.assertEquals(1, handler.approvedRequestIds.size());

		handler.applyProcessInfo();
		System.assertEquals(false, attRequestNew.ConfirmationRequired__c);
	}
	@isTest static void getRejectedTargetTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		Id summaryId = createSummary(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, endDate);

		AttRequestEntity attRequest = testData.createRequest(
				testData.employee.getHistory(0).id,
				summaryId,
				AppRequestStatus.APPROVED,
				null);

		Map<Id, AttRequest__c> oldMap = new Map<Id, AttRequest__c>();
		Map<Id, AttRequest__c> newMap = new Map<Id, AttRequest__c>();

		AttRequestRepository requestRep = new AttRequestRepository();
		AttRequest__c attRequestOld = attRequest.createSObject();
		attRequestOld.Status__c = AppConverter.stringValue(AppRequestStatus.PENDING);

		AttRequest__c attRequestNew = attRequest.createSObject();
		attRequestNew.Status__c = AppConverter.stringValue(AppRequestStatus.DISABLED);
		attRequestNew.CancelType__c = AppCancelType.REJECTED.value;
		oldMap.put(attRequest.id, attRequestOld);
		newMap.put(attRequest.id, attRequestNew);

		AttRequestTriggerHandler handler = new AttRequestTriggerHandler(oldMap, newMap);
		System.assertEquals(1, handler.disabledRequestIds.size());

		handler.applyProcessInfo();
		System.assertEquals(true, attRequestNew.ConfirmationRequired__c);
	}

	@isTest static void getRemovedTargetTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		Id summaryId = createSummary(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, endDate);

		AttRequestEntity attRequest = testData.createRequest(
				testData.employee.getHistory(0).id,
				summaryId,
				AppRequestStatus.APPROVED,
				null);

		Map<Id, AttRequest__c> oldMap = new Map<Id, AttRequest__c>();
		Map<Id, AttRequest__c> newMap = new Map<Id, AttRequest__c>();

		AttRequestRepository requestRep = new AttRequestRepository();
		AttRequest__c attRequestOld = attRequest.createSObject();
		attRequestOld.Status__c = AppConverter.stringValue(AppRequestStatus.PENDING);

		AttRequest__c attRequestNew = attRequest.createSObject();
		attRequestNew.Status__c = AppConverter.stringValue(AppRequestStatus.DISABLED);
		attRequestNew.CancelType__c = AppCancelType.REMOVED.value;
		oldMap.put(attRequest.id, attRequestOld);
		newMap.put(attRequest.id, attRequestNew);

		AttRequestTriggerHandler handler = new AttRequestTriggerHandler(oldMap, newMap);
		System.assertEquals(1, handler.disabledRequestIds.size());

		handler.applyProcessInfo();
		System.assertEquals(true, attRequestNew.ConfirmationRequired__c);
	}

	@isTest static void afterApprovedTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		Id summaryId = createSummary(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, endDate);

		AttRequestEntity attRequest = testData.createRequest(
				testData.employee.getHistory(0).id,
				summaryId,
				AppRequestStatus.PENDING,
				null);

		attRequest.status = AppRequestStatus.APPROVED;
		AttRequestRepository requestRep = new AttRequestRepository();
		test.startTest();
		requestRep.saveEntity(attRequest);
		test.stopTest();

		AttSummaryRepository summaryRep = new AttSummaryRepository();
		AttSummaryEntity summaryData = summaryRep.getEntity(summaryId);
		System.assertEquals(true, summaryData.isLocked);
	}

	@isTest static void afterRejectedTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		Id summaryId = createSummary(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, endDate);

		AttRequestEntity attRequest = testData.createRequest(
				testData.employee.getHistory(0).id,
				summaryId,
				AppRequestStatus.PENDING,
				null);

		attRequest.status = AppRequestStatus.DISABLED;
		attRequest.cancelType = AppCancelType.REJECTED;
		AttRequestRepository requestRep = new AttRequestRepository();
		test.startTest();
		requestRep.saveEntity(attRequest);
		test.stopTest();

		AttSummaryRepository summaryRep = new AttSummaryRepository();
		AttSummaryEntity summaryData = summaryRep.getEntity(summaryId);
		System.assertEquals(false, summaryData.isLocked);
		System.assertEquals(true, summaryData.isDirty);
	}

	@isTest static void afterRemovedTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		Id summaryId = createSummary(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, endDate);

		AttRequestEntity attRequest = testData.createRequest(
				testData.employee.getHistory(0).id,
				summaryId,
				AppRequestStatus.PENDING,
				null);

		attRequest.status = AppRequestStatus.DISABLED;
		attRequest.cancelType = AppCancelType.REMOVED;
		AttRequestRepository requestRep = new AttRequestRepository();
		test.startTest();
		requestRep.saveEntity(attRequest);
		test.stopTest();

		AttSummaryRepository summaryRep = new AttSummaryRepository();
		AttSummaryEntity summaryData = summaryRep.getEntity(summaryId);
		System.assertEquals(false, summaryData.isLocked);
		System.assertEquals(true, summaryData.isDirty);
	}

	@isTest static void afterCanceledTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 勤務表データ
		AppDate startDate = AppDate.newInstance(2017, 9, 1);
		AppDate endDate = startDate.addMonths(1).addDays(-1);
		Id summaryId = createSummary(testData.employee.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, endDate);

		AttRequestEntity attRequest = testData.createRequest(
				testData.employee.getHistory(0).id,
				summaryId,
				AppRequestStatus.PENDING,
				null);

		attRequest.status = AppRequestStatus.DISABLED;
		attRequest.cancelType = AppCancelType.CANCELED;
		AttRequestRepository requestRep = new AttRequestRepository();
		test.startTest();
		requestRep.saveEntity(attRequest);
		test.stopTest();

		AttSummaryRepository summaryRep = new AttSummaryRepository();
		AttSummaryEntity summaryData = summaryRep.getEntity(summaryId);
		System.assertEquals(false, summaryData.isLocked);
		System.assertEquals(true, summaryData.isDirty);
	}
}