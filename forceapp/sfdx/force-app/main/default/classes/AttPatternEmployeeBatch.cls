/**
 * @group 勤怠
 *
 * @description 勤務パターン社員一括適用バッチ
 * 本バッチを実行する時はscopeにBATCH_SCOPEを指定してください。
 * Database.executeBatch(new AttPatternEmployeeBatch(id), AttPatternEmployeeBatch.BATCH_SCOPE)
 */
public class AttPatternEmployeeBatch implements Database.Batchable<sObject>, Database.Stateful {

	/** @description executeメソッドに渡すレコードの数 */
	public static final Integer BATCH_SCOPE = 1;

	/** @description 1回の実行で処理可能なレコード数(このレコード数を超えると、場合によってはガバナ制限エラーが発生します) */
	public static final Integer LIMIT_RECORDS_COUNT = 5000;

	/** @description 処理対象の勤怠インポートバッチID */
	private Id importBatchId;

	/** @description 成功処理件数 */
	@testVisible
	private Integer successCount;

	/** @description エラー処理件数 */
	@testVisible
	private Integer failureCount;

	/** @description 処理件数 */
	@testVisible
	private Integer processCount;

	/**
	 * @description コンストラクタ
	 * @param importBatchId 処理対象の勤怠インポートバッチID
	 * @return バッチ処理対象レコード
	 */
	public AttPatternEmployeeBatch(Id importBatchId) {
		AttImportBatchEntity batch = new AttImportBatchRepository().getEntity(importBatchId);
		if (batch == null) {
			// メッセージ：処理対象のデータが見つかりませんでした。
			System.debug(LoggingLevel.ERROR, 'Batch record not found. (importBatchId=' + this.importBatchId + ')');
			throw new App.RecordNotFoundException(ComMessage.msg().Att_Err_BatchRecordNotFound);
		}

		// 対象のバッチの処理種別が勤務パターン適用であることを確認
		// 正しくない場合はバグであるため多言語化不要
		if (batch.importType != AttImportBatchType.ATT_PATTERN_APPLY) {
			String msg = 'Invalid importType. (importType=' + batch.importType + ')';
			System.debug(LoggingLevel.ERROR, msg);
			throw new App.ParameterException(msg);
		}

		this.importBatchId = importBatchId;
		this.processCount = 0;
		this.successCount = 0;
		this.failureCount = 0;
	}

	/**
	 * @description バッチ開始処理
	 * @param bc バッチコンテキスト
	 * @return バッチ処理対象レコード
	 */
	public Database.QueryLocator start(Database.BatchableContext BC) {
		AttImportBatchRepository batchRepo = new AttImportBatchRepository();

		// バッチを取得する
		AttImportBatchEntity batch = batchRepo.getEntity(this.importBatchId);
		if (batch == null) {
			// メッセージ：処理対象のデータが見つかりませんでした。
			System.debug(LoggingLevel.ERROR, 'AttPatternEmployeeBatch.start: Batch record not found. (importBatchId=' + this.importBatchId + ')');
			throw new App.RecordNotFoundException(ComMessage.msg().Att_Err_BatchRecordNotFound);
		}

		// ステータスを処理中に設定
		// 保存時にDML例外が発生する可能性があるが、発生してもバッチのステータスを更新することはできないため例外はキャッチしない
		batch.status = ImportBatchStatus.PROCESSING;
		batchRepo.saveEntity(batch);

		// クエリを作成する
		String targetImportType = AttImportBatchType.ATT_PATTERN_APPLY.value;
		String query = 'SELECT Id, ImportBatchId__c, ImportBatchId__r.CompanyId__c,'
				+ ' No__c, EmployeeCode__c, Date__c, DayType__c, PatternCode__c, Status__c, Error__c'
				+ ' FROM AttPatternApplyImport__c'
				+ ' WHERE ImportBatchId__c = :importBatchId'
				+ ' ORDER BY No__c Asc';

		return Database.getQueryLocator(query);
	}

	/**
	 * @description バッチ実行処理
	 * @param bc バッチコンテキスト
	 * @param scope バッチ処理対象レコードのリスト
	 */
	public void execute(Database.BatchableContext BC, List<AttPatternApplyImport__c> scope) {
		// リポジトリのキャッシュ有効化
		enableMasterCaches();

		this.processCount++;

		// 勤務パターンを適用
		// scopeのサイズは1であることを前提としている
		AttPatternApplyImport__c importObj = scope[0];

		AttPatternApplyImportRepository rep = new AttPatternApplyImportRepository();
		AttPatternApplyImportEntity entity = new AttPatternApplyImportEntity(importObj);
		// ガバナーエラーなど処理中断の場合、対象データを特定用
		entity.status = ImportStatus.PROCESSING; // 実行中
		rep.saveEntity(entity);

		Id companyId = importObj.ImportBatchId__r.CompanyId__c;

		Savepoint sp = Database.setSavepoint();
		try {
			AttPatternEmployeeApplyingService service = new AttPatternEmployeeApplyingService();
			AttPatternEmployeeApplyingService.Result result = service.applyPatternToEmployee(entity, companyId);

			if (result.isSuccess) {
				// 処理が成功した場合
				entity.status = ImportStatus.SUCCESS;
				this.successCount++;
			} else {
				// 処理が失敗した場合
				List<String> messageList = new List<String>();
				for (AttPatternEmployeeApplyingService.Error error : result.errorList) {
					messageList.add(error.message);
				}
				entity.setError(messageList);
				this.failureCount++;
			}
		} catch (Exception e) {
			String message = ComMessage.msg().Att_Err_BatchUnexpectedError + '(' + e.getMessage() + ')';
			entity.setError(new List<String>{message});
		}

		// 処理に失敗した場合はロールバックする
		if (entity.status == ImportStatus.ERROR) {
			Database.rollback(sp);
		}

		rep.saveEntity(entity);

		// バッチの処理件数を更新する
		AttImportBatchEntity batch = new AttImportBatchEntity();
		batch.setId(this.importBatchId);
		batch.successCount = this.successCount;
		batch.failureCount = this.failureCount;
		new AttImportBatchRepository().saveEntity(batch);
	}

	/**
	 * @description バッチ終了処理
	 * @param bc バッチコンテキスト
	 */
	public void finish(Database.BatchableContext BC) {
		AttImportBatchRepository batchRepo = new AttImportBatchRepository();

		// バッチを取得する
		AttImportBatchEntity batch = batchRepo.getEntity(this.importBatchId);
		if (batch == null) {
			// メッセージ：処理対象のデータが見つかりませんでした。
			System.debug(LoggingLevel.ERROR, 'AttPatternEmployeeBatch.start: Batch record not found. (importBatchId=' + this.importBatchId + ')');
			throw new App.RecordNotFoundException(ComMessage.msg().Att_Err_BatchRecordNotFound);
		}

		// ステータスが「成功」,「失敗」以外のレコードが存在している場合は、システムエラーが発生したものとしてステータスを失敗に変更する
		if (this.processCount > (this.successCount + this.failureCount)) {
			Integer toErrorCount = updateWaitingImportToError();
			this.failureCount += toErrorCount;
		}

		// バッチのステータスと処理件数の更新
		// 保存時にDML例外が発生する可能性があるが、発生してもバッチのステータスを更新することはできないため例外はキャッチしない
		batch.status = ImportBatchStatus.COMPLETED;
		batch.count = this.processCount;
		batch.successCount = this.successCount;
		batch.failureCount = this.failureCount;
		batchRepo.saveEntity(batch);
	}

	/**
	 * @description
	 * 未処理のインポートデータのステータスをエラーに更新する
	 * 未処理レコードの件数が処理可能な件数を超えている場合は更新しません。
	 * @return 未処理からエラーに変更したインポートデータ件数
	 */
	private Integer updateWaitingImportToError() {
		final String waitingStatus = ImportStatus.WAITING.value;
		final Integer limitRecord = LIMIT_RECORDS_COUNT + 1;
		Integer retCount = 0;

		List<AttPatternApplyImport__c> waitingList =
				[SELECT Id, Status__c
				FROM AttPatternApplyImport__c
				WHERE Status__c = :waitingStatus
				LIMIT :limitRecord];

		if (waitingList.isEmpty()) {
			return retCount;
		}
		if (waitingList.size() == limitRecord) {
			// 処理可能件数を超えているので何もしない
			return retCount;
		}

		// ステータスを失敗に設定する
		String message = ComMessage.msg().Att_Err_BatchUnexpectedError;
		for (AttPatternApplyImport__c waiting : waitingList) {
			waiting.Status__c = ImportStatus.ERROR.value;
			waiting.Error__c = message;
			retCount++;
		}
		update waitingList;
		return retCount;
	}

	/**
	 * 勤怠処理で利用するマスタのキャッシュ機能を有効化する
	 */
	private void enableMasterCaches() {
		// 親子型マスタ
		EmployeeRepository.enableCache();
		DepartmentRepository.enableCache();
		AttWorkingTypeRepository.enableCache();
		TimeSettingRepository.enableCache();
		AttShortTimeSettingRepository.enableCache();
		// 有効期間型マスタ
		AttAgreementAlertSettingRepository.enableCache();
		AttLeaveOfAbsenceRepository.enableCache();
		AttLeaveRepository.enableCache();
		AttPatternRepository2.enableCache();
		// 論理削除型マスタ
		CalendarRepository.enableCache();
		CompanyRepository.enableCache();
	}
}