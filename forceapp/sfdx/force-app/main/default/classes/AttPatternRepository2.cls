/**
 * @group 勤怠
 *
 * @description 勤務パターンリポジトリ
 * 過去にAttPatternRepositoryクラスが存在しており、再度同名のクラスをパッケージに追加することができないためクラス名に2を付けている
 */
public with sharing class AttPatternRepository2 extends ValidPeriodRepository {

	/**
	 * キャッシュ利用有無のデフォルト値
	 * Default value for cache usage
	 */
	private static Boolean isDefaultEnabledCache = false;
	/**
	 * キャッシュ利用を有効化する
	 * 本メソッドを実行した後に一時的にキャッシュを利用しない場合はコンストラト時に設定することができる。
	 * Enable cache usage
	 * If you do not use cache, please specify in the parameter of the constructor.
	 */
	public static void enableCache() {
		isDefaultEnabledCache = true;
	}
	/** キャッシュの利用有無（デフォルトはoff） */
	@TestVisible
	private Boolean isEnabledCache = isDefaultEnabledCache;
	// 検索条件をキーに検索結果をキャッシュし、同一検索条件の2回目以降の検索ではキャシュした値を返す
	// staticフィールドなので、Repositoryのインスタンスが異なってもキャッシュを共有する
	/** キャッシュ */
	private static final Repository.Cache CACHE = new Repository.Cache();

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_PATTERN_FIELD_SET;
	static {
		GET_PATTERN_FIELD_SET = new Set<Schema.SObjectField> {
				AttPattern__c.Id,
				AttPattern__c.CompanyId__c,
				AttPattern__c.Name,
				AttPattern__c.Name_L0__c,
				AttPattern__c.Name_L1__c,
				AttPattern__c.Name_L2__c,
				AttPattern__c.Code__c,
				AttPattern__c.UniqKey__c,
				AttPattern__c.Order__c,
				AttPattern__c.ValidFrom__c,
				AttPattern__c.ValidTo__c,
				AttPattern__c.WorkSystem__c,
				// 勤務時間系
				AttPattern__c.StartTime__c,
				AttPattern__c.EndTime__c,
				AttPattern__c.ContractedWorkHours__c,
				AttPattern__c.Rest1StartTime__c,
				AttPattern__c.Rest1EndTime__c,
				AttPattern__c.Rest2StartTime__c,
				AttPattern__c.Rest2EndTime__c,
				AttPattern__c.Rest3StartTime__c,
				AttPattern__c.Rest3EndTime__c,
				AttPattern__c.Rest4StartTime__c,
				AttPattern__c.Rest4EndTime__c,
				AttPattern__c.Rest5StartTime__c,
				AttPattern__c.Rest5EndTime__c,
				// 午前半休項目
				AttPattern__c.UseAMHalfDayLeave__c,
				AttPattern__c.AMContractedWorkHours__c,
				AttPattern__c.AMStartTime__c,
				AttPattern__c.AMEndTime__c,
				AttPattern__c.AMRest1StartTime__c,
				AttPattern__c.AMRest1EndTime__c,
				AttPattern__c.AMRest2StartTime__c,
				AttPattern__c.AMRest2EndTime__c,
				AttPattern__c.AMRest3StartTime__c,
				AttPattern__c.AMRest3EndTime__c,
				AttPattern__c.AMRest4StartTime__c,
				AttPattern__c.AMRest4EndTime__c,
				AttPattern__c.AMRest5StartTime__c,
				AttPattern__c.AMRest5EndTime__c,
				// 午後半休項目
				AttPattern__c.UsePMHalfDayLeave__c,
				AttPattern__c.PMContractedWorkHours__c,
				AttPattern__c.PMStartTime__c,
				AttPattern__c.PMEndTime__c,
				AttPattern__c.PMRest1StartTime__c,
				AttPattern__c.PMRest1EndTime__c,
				AttPattern__c.PMRest2StartTime__c,
				AttPattern__c.PMRest2EndTime__c,
				AttPattern__c.PMRest3StartTime__c,
				AttPattern__c.PMRest3EndTime__c,
				AttPattern__c.PMRest4StartTime__c,
				AttPattern__c.PMRest4EndTime__c,
				AttPattern__c.PMRest5StartTime__c,
				AttPattern__c.PMRest5EndTime__c,

				AttPattern__c.HalfDayLeaveHours__c,
				AttPattern__c.BoundaryOfStartTime__c,
				AttPattern__c.BoundaryOfEndTime__c
		};
	}

	/**
	 * コンストラクタ
	 * キャッシュを利用しないインスタンスを生成する
	 */
	public AttPatternRepository2() {
	}

	/**
	 * キャッシュの利用有無を指定して、インスタンスを生成する
	 * @param isEnabledCache キャッシュの利用有無
	 */
	public AttPatternRepository2(Boolean isEnabledCache) {
		this.isEnabledCache = isEnabledCache;
	}

	/** 検索フィルタ (検索API実行用) */
	public class SearchFilter {
		public Set<Id> ids;
		public List<String> codes;
		public String companyId;
		public AttWorkSystem workSystem;
		public AppDate targetDate;
	}

	/**
	 * 指定会社の勤務パターンを取得する(管理画面)
	 * @param companyId 指定する会社Id
	 * @return 指定会社の全勤務パターン一覧(指定順)
	 */
	public List<AttPatternEntity> getPatternListByCompanyId(Id companyId) {
		SearchFilter filter = new SearchFilter();
		filter.companyId = companyId;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定したidの勤務パターンを取得する
	 * @param patternId 取得対象の勤務パターンid
	 * @return 勤務パターン
	 */
	public AttPatternEntity getPatternById(Id patternId) {
		SearchFilter filter = new SearchFilter();
		filter.ids = new Set<Id>{patternId};

		for (AttPatternEntity patternData : searchEntityList(filter, false)) {
			return patternData;
		}
		return null;
	}

	/**
	 * 指定したcodeの勤務パターンを取得する
	 * @param code 取得対象の勤務パターンコード
	 * @param companyId 会社ID
	 * @return 勤務パターン
	 */
	public AttPatternEntity getPatternByCode(String code, Id companyId) {
		SearchFilter filter = new SearchFilter();
		filter.codes = new List<String>{code};
		filter.companyId = companyId;

		for (AttPatternEntity patternData : searchEntityList(filter, false)) {
			return patternData;
		}
		return null;
	}

	/**
	 * 指定したcodeの勤務パターンを取得する
	 * @param codes 取得対象の勤務パターンコードのリスト
	 * @param companyId 会社ID
	 * @return 勤務パターンマスタ一覧
	 */
	public List<AttPatternEntity> getPatternByCodes(List<String> codes, Id companyId) {
		SearchFilter filter = new SearchFilter();
		filter.codes = codes;
		filter.companyId = companyId;

		return searchEntityList(filter, false);
	}

	/**
	 * 指定Idの勤務パターン一覧を取得する(勤怠計算)
	 * @param patternIds 指定する勤務パターンId一覧
	 * @return 勤務パターン一覧
	 */
	public List<AttPatternEntity> getPatternList(Set<Id> patternIds) {
		SearchFilter filter = new SearchFilter();
		filter.ids = patternIds;

		return searchEntityList(filter, false);
	}
	/**
	 * 勤務パターンを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した勤務パターン一覧(指定順)
	 */
	@TestVisible
	private List<AttPatternEntity> searchEntityList(
			SearchFilter filter, Boolean forUpdate) {

		// キャッシュ判定
		CacheKey cacheKey = new CacheKey(filter);
		if (isEnabledCache && !forUpdate && CACHE.hasCache(cacheKey)) {
			System.debug(LoggingLevel.DEBUG, 'searchEntityList ReturnCache key:' + cacheKey);
			List<AttPatternEntity> entities = new List<AttPatternEntity>();
			for (AttPattern__c sObj : (List<AttPattern__c>)CACHE.get(cacheKey)) {
				entities.add(new AttPatternEntity(sObj));
			}
			return entities;
		}
		System.debug(LoggingLevel.DEBUG, 'searchEntityList NoCache key:' + cacheKey);

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_PATTERN_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Set<Id> patternIds = filter.ids;
		final Id companyId = filter.companyId;
		final List<String> codes = filter.codes;
		final String workSystem = AppConverter.stringValue(filter.workSystem);
		final Date targetDate = filter.targetDate == null ? null : filter.targetDate.getDate();

		if (patternIds != null) {
			condList.add(getFieldName(AttPattern__c.Id) + ' in :patternIds');
		}
		if (companyId != null) {
			condList.add(getFieldName(AttPattern__c.CompanyId__c) + ' = :companyId');
		}
		if (codes != null) {
			condList.add(getFieldName(AttPattern__c.Code__c) + ' IN :codes');
		}
		if (workSystem != null) {
			condList.add(getFieldName(AttPattern__c.WorkSystem__c) + ' = :workSystem');
		}
		if(targetDate != null){
			// ValidTo__cは失効日のため有効期間に含めない。扱い注意。
			condList.add(getFieldName(AttPattern__c.ValidFrom__c) + ' <= :targetDate');
			condList.add(getFieldName(AttPattern__c.ValidTo__c) + ' > :targetDate');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + AttPattern__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		// ForUpdateとOrderByは併用出来ないため、どちらかを設定する
		if (forUpdate) {
			soql += ' FOR UPDATE';
		} else {
			soql += ' ORDER BY ' + getFieldName(AttPattern__c.Order__c) + ' Asc' + ' NULLS LAST,'
				+ getFieldName(AttPattern__c.Code__c) + ' Asc';
		}

		System.debug('AttPatternRepository2.searchEntityList: SOQL==>' + soql);

		// クエリ実行
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttPattern__c> sObjList = Database.query(soql);
		AppLimits.getInstance().incrementQueryCount(AttPattern__c.SObjectType);

		// 結果からエンティティを作成する
		List<AttPatternEntity> entityList = new List<AttPatternEntity>();
		for (AttPattern__c attPattern : sObjList) {
			entityList.add(new AttPatternEntity(attPattern));
		}

		// キャッシュを利用する場合は、検索結果を退避する
		if (isEnabledCache) {
			CACHE.put(cacheKey, sObjList);
		}
		return entityList;
	}

	/**
	 * 勤務パターンを検索する (検索API用)
	 * @param filter 検索条件
	 * @return 条件に一致した勤務パターン一覧(指定順)
	 */
	public List<AttPatternEntity> searchEntityList(SearchFilter filter){
		return searchEntityList(filter, false);
	}

	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	protected override List<SObject> createObjectList(List<ValidPeriodEntity> entityList) {
		List<AttPattern__c> objectList = new List<AttPattern__c>();
		for (ValidPeriodEntity entity : entityList) {
			objectList.add(((AttPatternEntity)entity).createSObject());
		}
		return objectList;
	}
}