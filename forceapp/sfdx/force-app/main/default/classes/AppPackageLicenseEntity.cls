/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group アプリ
 *
 * パッケージライセンスエンティティ
 */
public with sharing class AppPackageLicenseEntity extends Entity {

	/** WSPの名前空間プレフィクス */
	public static final String NAMESPACE_PREFIX_WSP = 'tswsp';

	/** 名前空間プレフィックス */
	public String namespacePrefix {
		get;
		set {
			namespacePrefix = value;
		}
	}

	/** ステータス */
	public AppPackageLicenseStatus status {
		get;
		set {
			status = value;
		}
	}
}