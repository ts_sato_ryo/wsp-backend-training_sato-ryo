/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * インポートバッチのリポジトリ
 */
public with sharing class ImportBatchRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_FIELD_SET;
	static {
		GET_FIELD_SET = new Set<Schema.SObjectField> {
			ComImportBatch__c.Id,
			ComImportBatch__c.Name,
			ComImportBatch__c.TargetDate__c,
			ComImportBatch__c.Status__c,
			ComImportBatch__c.Type__c,
			ComImportBatch__c.Count__c,
			ComImportBatch__c.SuccessCount__c,
			ComImportBatch__c.FailureCount__c
		};
	}

	/** 検索フィルタ */
	 private class Filter {
		/** インポートバッチID */
		public List<Id> batchIdList;
	 }

	/**
	 * インポートバッチエンティティを取得する
	 * @param batchId インポートバッチID
	 * @return インポートバッチエンティティ
	 */
	public ImportBatchEntity getEntity(Id batchId) {
		// 検索実行
		List<ImportBatchEntity> entityList = getEntityList(new List<Id>{batchId});

		ImportBatchEntity entity = null;
		if (entityList != null && !entityList.isEmpty()) {
			entity = entityList[0];
		}

		return entity;
	}

	/**
	 * インポートバッチエンティティのリストを取得する
	 * @param batchIdList インポートバッチIDのリスト
	 * @return 条件に一致したインポートバッチエンティティのリスト
	 */
	public List<ImportBatchEntity> getEntityList(List<Id> batchIdList) {
		// 検索条件を作成する
		Filter filter = new Filter();
		filter.batchIdList = batchIdList;

		// 検索を実行する
		final Boolean forUpdate = false;

		return searchEntity(filter, forUpdate);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(ImportBatchEntity entity) {
		return saveEntityList(new List<ImportBatchEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entityList 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<ImportBatchEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト
	 */
	public SaveResult saveEntityList(List<ImportBatchEntity> entityList, Boolean allOrNone) {
		List<ComImportBatch__c> objList = new List<ComImportBatch__c>();

		// エンティティからSObjectを作成する
		for (ImportBatchEntity entity : entityList) {
			ComImportBatch__c obj = createObj(entity);
			objList.add(obj);
		}

		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(objList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}

	/**
	 * インポートバッチを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したインポートバッチエンティティのリスト
	 */
	 private List<ImportBatchEntity> searchEntity(Filter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_FIELD_SET) {
			selectFldList.add(fld.getDescribe().getName());
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> batchIds = filter.batchIdList;
		if ((batchIds != null) && (!batchIds.isEmpty())) {
			condList.add(getFieldName(ComImportBatch__c.Id) + ' IN :batchIds');
		}

		String whereCond;
		if (!condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + ComImportBatch__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		System.debug('ImportBatchRepository.searchEntity: SOQL==>' + soql);
		List<SObject> sobjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<ImportBatchEntity> entityList = new List<ImportBatchEntity>();
		for (SObject sobj : sobjList) {
			entityList.add(createEntity((ComImportBatch__c)sobj));
		}

		return entityList;
	 }

	/**
	 * ComImportBatch__cオブジェクトからImportBatchEntityを作成する
	 * @param obj 作成元のComImportBatch__cオブジェクト
	 * @return 作成したImportBatchEntity
	 */
	private ImportBatchEntity createEntity(ComImportBatch__c obj) {
		ImportBatchEntity entity = new ImportBatchEntity();

		entity.setId(obj.Id);
		entity.name = obj.Name;
		entity.targetDate = AppDate.valueOf(obj.TargetDate__c);
		entity.status = ImportBatchStatus.valueOf(obj.Status__c);
		entity.type = ImportBatchType.valueOf(obj.Type__c);
		entity.count = Integer.valueOf(obj.Count__c);
		entity.successCount = Integer.valueOf(obj.SuccessCount__c);
		entity.failureCount = Integer.valueOf(obj.FailureCount__c);

		entity.resetChanged();
		return entity;
	}

	/**
	 * ImportBatchEntityからComImportBatch__cオブジェクトを作成する
	 * @param entity 作成元のImportBatchEntity
	 * @return 作成したComImportBatch__cオブジェクト
	 */
	private ComImportBatch__c createObj(ImportBatchEntity entity) {
		ComImportBatch__c retObj = new ComImportBatch__c();

		retObj.Id = entity.id;
		if (entity.isChanged(ImportBatchEntity.Field.Name)) {
			retObj.Name = entity.name;
		}
		if (entity.isChanged(ImportBatchEntity.Field.TargetDate)) {
			retObj.TargetDate__c = AppDate.convertDate(entity.targetDate);
		}
		if (entity.isChanged(ImportBatchEntity.Field.Status)) {
			retObj.Status__c = entity.status.value;
		}
		if (entity.isChanged(ImportBatchEntity.Field.Type)) {
			retObj.Type__c = entity.type.value;
		}
		if (entity.isChanged(ImportBatchEntity.Field.Count)) {
			retObj.Count__c = entity.count;
		}
		if (entity.isChanged(ImportBatchEntity.Field.SuccessCount)) {
			retObj.SuccessCount__c = entity.successCount;
		}
		if (entity.isChanged(ImportBatchEntity.Field.FailureCount)) {
			retObj.FailureCount__c = entity.failureCount;
		}

		return retObj;
	}
}