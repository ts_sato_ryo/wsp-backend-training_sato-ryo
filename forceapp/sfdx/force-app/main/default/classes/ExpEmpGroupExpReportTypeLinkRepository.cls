/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description 経費申請タイプ別費目のリポジトリ Repository class for ExpReportTypeExpTypeLink
 *
 * @group 経費 Expense
 */
public with sharing class ExpEmpGroupExpReportTypeLinkRepository extends Repository.BaseRepository {

	/*
	 *  Max number of records to fetch
	 */
	private final static Integer SEARCH_RECORDS_NUMBER_MAX = 10000;

	/*
	 *  Roll back transaction if there are any failures in saving
	 */
	private final static Boolean ALL_SAVE = true;

	/*
	 * Object name
	 */
	private static final String BASE_OBJECT_NAME = ExpEmpGroupExpReportTypeLink__c.SObjectType.getDescribe().getName();
	/*
	 * 取得対象の項目名 Target fields to fetch data
	 */
	private static final List<String> GET_BASE_FIELD_LIST;
	static {
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
			ExpEmpGroupExpReportTypeLink__c.Id,
			ExpEmpGroupExpReportTypeLink__c.Name,
			ExpEmpGroupExpReportTypeLink__c.ExpEmpGroupId__c,
			ExpEmpGroupExpReportTypeLink__c.ExpReportTypeId__c,
			ExpEmpGroupExpReportTypeLink__c.Order__c
		};

		GET_BASE_FIELD_LIST = Repository.generateFieldNameList(fieldList);
	}

	/*
	 * Relationship name of expense employee group
	 */
	private static final String EXP_EMP_GROUP_R = ExpEmpGroupExpReportTypeLink__c.ExpEmpGroupId__c.getDescribe().getRelationshipName();

	private static final String EXP_REPORT_TYPE_R = ExpEmpGroupExpReportTypeLink__c.ExpReportTypeId__c.getDescribe().getRelationshipName();

	private static final List<String> SELECT_EXP_REPORT_TYPE_FIELD_SET;
	static {
		final Set<Schema.SObjectField> fields = new Set<Schema.SObjectField> {
			ExpReportType__c.Active__c,
			ExpReportType__c.VendorUsedIn__c
		};
		SELECT_EXP_REPORT_TYPE_FIELD_SET = Repository.generateFieldNameList(EXP_REPORT_TYPE_R , fields);
	}

	/*
	 * Search Filter
	 */
	public class SearchFilter {
		/*
		 *  Set of employee group ids
		 */
		public Set<Id> empGroupIdSet;
		/** if set to True or False, it will filter Report Types based on Active flag of Report Type */
		public Boolean reportTypeActive;
		/** For Mobile: set to True to exclude Report Types with Vendor Used In field value is NOT null */
		public Boolean reportTypeWithoutVendorOnly;
	}

	/*
	 * Search ExpReportTypeExpTypeLinks based on the filter conditions
	 *
	 * @param empGroupId employee group id
	 * @return ExpEmpGroupExpReportTypeLinkEntity
	 */
	public List<ExpEmpGroupExpReportTypeLinkEntity> searchEntityByEmpGroupId(Id empGroupId) {
		SearchFilter filter = new SearchFilter();
		filter.empGroupIdSet = new Set<Id>{ empGroupId };
		return searchEntityList(filter);
	}

	/*
	 * Search ExpReportTypeExpTypeLinks based on the filter conditions
	 *
	 * @param filter Search filter
	 *
	 * @return Search result
	 */
	public List<ExpEmpGroupExpReportTypeLinkEntity> searchEntityList(SearchFilter filter) {

		//  Target fields
		List<String> selectFieldList = new List<String>();
		selectFieldList.addAll(GET_BASE_FIELD_LIST);

		//  Create WHERE statement
		List<String> conditions = new List<String>();
		final Set<Id> empGroupIdSet = filter.empGroupIdSet;
		if (empGroupIdSet != null) {
			conditions.add(createInExpression(ExpEmpGroupExpReportTypeLink__c.ExpEmpGroupId__c, 'empGroupIdSet'));
		}

		Boolean reportTypeActive;
		if (filter.reportTypeActive != null) {
			reportTypeActive = filter.reportTypeActive;
			conditions.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.Active__c) + ' = :reportTypeActive');
		}

		if (filter.reportTypeWithoutVendorOnly == true) {
			conditions.add(getFieldName(EXP_REPORT_TYPE_R, ExpReportType__c.VendorUsedIn__c) + ' = null');
		}

		//  Create SOQL
		String soql = 'SELECT ' + String.join(selectFieldList, ',') +
			' FROM ' + BASE_OBJECT_NAME +
			buildWhereString(conditions) +
			' ORDER BY ' + getFieldName(EXP_EMP_GROUP_R, ExpEmployeeGroup__c.Id) + ' ASC' +
			',' + getFieldName(ExpEmpGroupExpReportTypeLink__c.Order__c) + ' ASC' +
			' LIMIT :SEARCH_RECORDS_NUMBER_MAX';
		System.debug('ExpEmpGrpExpReportTypeLinkRepository: SOQL=' + soql);

		//  Execute query
		List<ExpEmpGroupExpReportTypeLink__c> sObjList = Database.query(soql);
		List<ExpEmpGroupExpReportTypeLinkEntity> entityList = new List<ExpEmpGroupExpReportTypeLinkEntity>();
		for (ExpEmpGroupExpReportTypeLink__c sobj : sObjList) {
			entityList.add(createEntity(sobj));
		}

		return entityList;
	}

	/*
	 * get entity map from search filter. SELECT from DB then convert from entity to map
	 *
	 * @param filter SearchFilter
	 * @return Map {expEmpGroupId, reportTypeIdList}
	 */
	public Map<Id, List<Id>> getEntityListMap(SearchFilter filter) {
		List<ExpEmpGroupExpReportTypeLinkEntity> entityList = searchEntityList(filter);
		return createMap(entityList);
	}

	/*
	 * createMap from ExpEmpGroupExpReportTypeLinkEntity
	 *
	 * @param entityList ExpEmpGroupExpReportTypeLinkEntity
	 * @return Map {expEmpGroupId, reportTypeIdList}
	 */
	@testVisible
	private Map<Id, List<Id>> createMap(List<ExpEmpGroupExpReportTypeLinkEntity> entityList) {
		Map<Id, List<Id>> reportTypeIdMap = new Map<Id, List<Id>>();
		if(entityList.isEmpty()){
			return reportTypeIdMap;
		}
		Set<Id> empGroupIdSet = new Set<Id>();
		for (ExpEmpGroupExpReportTypeLinkEntity entity : entityList) {
			empGroupIdSet.add(entity.expEmpGroupId);
		}
		for (Id empGroupId : empGroupIdSet) {
			List<Id> reportTypeIdList = searchReportTypeIdList(empGroupId, entityList);
			reportTypeIdMap.put(empGroupId, reportTypeIdList);
		}
		return reportTypeIdMap;
	}

	/*
	 * search ReportTypeIdList corresponding to expEmpGroupId from List<ExpEmpGroupExpReportTypeLinkEntity>.
	 *
	 * @param expEmpGroupId
	 * @param entityList list of ExpEmpGroupExpReportTypeLinkEntity
	 * @return Map {expEmpGroupId, reportTypeIdList}
	 */
	private List<Id> searchReportTypeIdList(Id expEmpGroupId, List<ExpEmpGroupExpReportTypeLinkEntity> entityList) {
		List<Id> reportTypeIdList = new List<Id>();
		for (ExpEmpGroupExpReportTypeLinkEntity entity : entityList) {
			if (entity.expEmpGroupId == expEmpGroupId) {
				reportTypeIdList.add(entity.expReportTypeId);
			}
		}
		return reportTypeIdList;
	}

	/*
	 * save ExpEmpGroupExpReportTypeLinkEntity
	 *
	 * @param entityList
	 * @return void
	 */
	public void saveEntityList(List<ExpEmpGroupExpReportTypeLinkEntity> entityList) {
		List<ExpEmpGroupExpReportTypeLink__c> sObjList = new List<ExpEmpGroupExpReportTypeLink__c>();
		for (ExpEmpGroupExpReportTypeLinkEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}
		List<Database.UpsertResult> resList = AppDatabase.doUpsert(sObjList, ALL_SAVE);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);
	}

	/*
	 * create object from entity
	 *
	 * @param entity
	 * @return object
	 */
	private ExpEmpGroupExpReportTypeLink__c createSObject(ExpEmpGroupExpReportTypeLinkEntity entity) {
		ExpEmpGroupExpReportTypeLink__c sobj = new ExpEmpGroupExpReportTypeLink__c(ID = entity.Id);

		if (entity.isChanged(ExpEmpGroupExpReportTypeLinkEntity.Field.EXP_EMP_GROUP_ID)) {
			sobj.ExpEmpGroupId__c = entity.expEmpGroupId;
		}
		if (entity.isChanged(ExpEmpGroupExpReportTypeLinkEntity.Field.EXP_REPORT_TYPE_ID)) {
			sobj.ExpReportTypeId__c = entity.expReportTypeId;
		}
		if (entity.isChanged(ExpEmpGroupExpReportTypeLinkEntity.Field.ORDER)) {
			sobj.Order__c = entity.order;
		}

		return sobj;
	}

	/*
	 * create entity from object
	 *
	 * @param sObj
	 * @return entity
	 */
	@testVisible
	private ExpEmpGroupExpReportTypeLinkEntity createEntity(ExpEmpGroupExpReportTypeLink__c sObj) {
		ExpEmpGroupExpReportTypeLinkEntity entity = new ExpEmpGroupExpReportTypeLinkEntity();

		entity.setId(sObj.Id);
		entity.expEmpGroupId = sObj.ExpEmpGroupId__c;
		entity.expReportTypeId = sObj.ExpReportTypeId__c;
		entity.order = (Integer)sObj.Order__c;

		entity.resetChanged();

		return entity;
	}
}