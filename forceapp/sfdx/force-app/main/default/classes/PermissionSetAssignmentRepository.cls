/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 権限セット割り当てリポジトリ(標準オブジェクト)
 */
public with sharing class PermissionSetAssignmentRepository extends Repository.BaseRepository {

	/**
	 * 取得対象の項目名
	 * 項目が追加されたらbaseFieldSetに追加してください
	 */
	private static final List<String> GET_FIELD_NAME_LIST;
	static {
		// 取得対象の項目定義
		final Set<Schema.SObjectField> fieldList = new Set<Schema.SObjectField> {
				PermissionSetAssignment.Id,
				PermissionSetAssignment.AssigneeId,
				PermissionSetAssignment.PermissionSetId};

		GET_FIELD_NAME_LIST = Repository.generateFieldNameList(fieldList);
	}

	/** 権限セットのリレーション名 */
	private static final String PERMISSION_SET_R = PermissionSetAssignment.PermissionSetId.getDescribe().getRelationshipName();


	/** 検索フィルタ */
	public class SearchFilter {
		/** アサイン先のSalesforceユーザID */
		public List<Id> assigneeIds;
		/** 全てのデータの編集権限を持つ場合はtrue */
		public Boolean isModifyAllData = false;
	}

	/**
	 * エンティティを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した申請一覧
	 */
	public List<PermissionSetAssignmentEntity> searchEntityList(
			PermissionSetAssignmentRepository.SearchFilter filter, Boolean forUpdate) {

		// 取得対象項目
		List<String> selectFieldList = new List<String>();
		selectFieldList.addAll(GET_FIELD_NAME_LIST);

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final List<Id> assigneeIds = filter.assigneeIds;
		Boolean isModifyAllData = filter.isModifyAllData;
		if (assigneeIds != null) {
			condList.add(getFieldName(PermissionSetAssignment.AssigneeId) + ' IN :assigneeIds');
		}
		if (isModifyAllData == true) {
			condList.add(getFieldName(PERMISSION_SET_R, PermissionSet.PermissionsModifyAllData) + ' = true');
		}

		// Where句作成
		String whereClause = buildWhereString(condList);

		String soql = 'SELECT ' + String.join(selectFieldList, ', ')
				+ ' FROM ' + PermissionSetAssignment.SObjectType.getDescribe().getName()
				+ whereClause;
		if (forUpdate != true) {
			// 並び順は暫定対応
			soql += ' ORDER BY ' + getFieldName(PermissionSetAssignment.AssigneeId);
		}

		// クエリ実行
		System.debug('>>> PermissionSetAssignmentRepository.searchEntityList: SOQL=' + soql);
		List<PermissionSetAssignment> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<PermissionSetAssignmentEntity> entityList = new List<PermissionSetAssignmentEntity>();
		for (PermissionSetAssignment sObj : sObjList) {
			entityList.add(new PermissionSetAssignmentEntity(sObj));
		}

		return entityList;
	}
}