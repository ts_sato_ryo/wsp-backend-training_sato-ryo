/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 社員ベースレコード操作APIを実装するクラス
 */
public with sharing class EmployeeResource {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 検索APIの取得対象日のデフォルト値 */
	private static AppDate SEARCH_DEFAULT_TARGET_DATE = AppDate.today();
	/**
	 * @desctiprion 社員ベースレコードを表すクラス
	 */
	public class Employee implements RemoteApi.RequestParam {
		/** ベースレコードID */
		public String id;
		/** 社員名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 履歴レコードID */
		public String historyId;
		/** 姓(言語0) */
		public String firstName_L0;
		/** 姓(言語1) */
		public String firstName_L1;
		/** 姓(言語2) */
		public String firstName_L2;
		/** 名(言語0) */
		public String lastName_L0;
		/** 名(言語1) */
		public String lastName_L1;
		/** 名(言語2) */
		public String lastName_L2;
		/** ミドルネーム(言語0) */
		public String middleName_L0;
		/** ミドルネーム(言語1) */
		public String middleName_L1;
		/** ミドルネーム(言語2) */
		public String middleName_L2;
		/** 表示名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String displayName;
		/** 表示名(言語0) */
		public String displayName_L0;
		/** 表示名(言語1) */
		public String displayName_L1;
		/** 表示名(言語2) */
		public String displayName_L2;
		/** 社員コード */
		public String code;
		/** 会社レコードID */
		public String companyId;
		/** 部署レコードID */
		public String departmentId;
		/** 部署 */
		public Department department;
		/** 上長(ユーザレコード)ID */
		public String managerId;
		/** 上長 */
		public LookupField manager;
		/** Cost Center ID */
		public String costCenterId;
		/** Cost Center */
		public CostCenter costCenter;
		/** 役職(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String title;
		/** 役職(言語0) */
		public String title_L0;
		/** 役職(言語1) */
		public String title_L1;
		/** 役職(言語2) */
		public String title_L2;
		/** 入社日 */
		public Date startDate;
		/** 社員に紐付くユーザID */
		public String userId;
		/** ユーザ */
		public User user;
		/** Indicates active salesforce user */
		public Boolean isActiveSFUserAcc;
		/** 有効開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** コメント */
		public String comment;
		/** 勤務体系ベースID */
		public String workingTypeId;
		/** 勤務体系 */
		public LookupField workingType;
		/** 工数設定ベースID */
		public String timeSettingId;
		/** 工数設定 */
		public LookupField timeSetting;
		/** 36協定アラート設定ID */
		public String agreementAlertSettingId;
		/** 36協定アラート設定 */
		public LookupField agreementAlertSetting;
		/** カレンダーID */
		public String calendarId;
		/** 権限ID */
		public String permissionId;
		/** 承認者01ID */
		public String approver01Id;
		/** 承認者権限01フラグ */
		public Boolean approvalAuthority01;

		/** 通勤定期券有 */
		public Boolean commuterPassAvailable;
		/** ジョルダン経路 */
		public ExpTransitJorudanService.CommuterRouteDetail jorudanRoute;
		/** Expense Employee Group */
		public String expEmployeeGroupId;
		/** Expense Employee Group */
		public ExpEmployeeGroup expEmployeeGroup;

		/**
		 * パラメータの値を設定したベースエンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public EmployeeBaseEntity createBaseEntity(Map<String, Object> paramMap) {

			EmployeeBaseEntity base = new EmployeeBaseEntity();

			base.setId(this.id);

			if(paramMap.containsKey('lastName_L0')){
				base.lastNameL0 = this.lastName_L0;
			}
			if(paramMap.containsKey('lastName_L1')){
				base.lastNameL1 = this.lastName_L1;
			}
			if(paramMap.containsKey('lastName_L2')){
				base.lastNameL2 = this.lastName_L2;
			}
			if(paramMap.containsKey('firstName_L0')){
				base.firstNameL0 = this.firstName_L0;
			}
			if(paramMap.containsKey('firstName_L1')){
				base.firstNameL1 = this.firstName_L1;
			}
			if(paramMap.containsKey('firstName_L2')){
				base.firstNameL2 = this.firstName_L2;
			}
			if(paramMap.containsKey('middleName_L0')){
				base.middleNameL0 = this.middleName_L0;
			}
			if(paramMap.containsKey('middleName_L1')){
				base.middleNameL1 = this.middleName_L1;
			}
			if(paramMap.containsKey('middleName_L2')){
				base.middleNameL2 = this.middleName_L2;
			}
			if(paramMap.containsKey('displayName_L0')){
				base.displayNameL0 = this.displayName_L0;
			}
			if(paramMap.containsKey('displayName_L1')){
				base.displayNameL1 = this.displayName_L1;
			}
			if(paramMap.containsKey('displayName_L2')){
				base.displayNameL2 = this.displayName_L2;
			}
			if(paramMap.containsKey('code')){
				base.code = this.code;
			}
			if(paramMap.containsKey('companyId')){
				base.companyId = this.companyId;
			}
			if(paramMap.containsKey('startDate')){
				base.validFrom = AppDate.valueOf(this.startDate);
			}
			if(paramMap.containsKey('userId')){
				base.userId = this.userId;
			}

			// 新規作成時のみvalidToを設定
			// TODO 更新時に対応する際は履歴の有効期間も調整するようにすること
			if (base.id == null) {
				if(paramMap.containsKey('validDateTo')){
					base.validTo = AppDate.valueOf(this.validDateTo);
				}
				if (base.validTo == null) {
					base.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
				}
			}

			// 表示名のデフォルト値設定
			setDefaultDisplayName(base);

			return base;
		}

		/**
		 * DisplayNameにデフォルト値を設定する(姓、名が設定されている場合のみ)
		 * TODO 画面からDisplayNameが登録できるようになるまでの暫定対応
		 *      本メソッドの処理をServiceクラスに保存メソッドに実装すれば良さそうですが、
		 *      保存メソッドは社員インポートの機能からも参照されており、
		 *      インポート機能ではDisplayNameを登録できるようになっているため実装できませんでした。
		 *      一時的な機能であるということで、Resourceクラスに実装しております。
		 */
		private void setDefaultDisplayName(EmployeeBaseEntity base) {
			// 組織設定の言語(L0, L1)が何かを取得する
			AppLanguage langL0 = AppLanguage.getLangByLangKey(AppLanguage.LangKey.L0);
			AppLanguage langL1 = AppLanguage.getLangByLangKey(AppLanguage.LangKey.L1);
			AppLanguage langL2 = AppLanguage.getLangByLangKey(AppLanguage.LangKey.L2);

			EmployeeBaseEntity mergeBase;
			if (base.id == null) {
				mergeBase = base;
			} else {
				mergeBase = new EmployeeRepository().getEntity(base.id);
				if (mergeBase == null) {
					// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
					throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
				}

				// 値が変更された項目のみを設定する
				for (Schema.SObjectField field : base.getChangedFieldSet()) {
						mergeBase.setFieldValue(field, base.getFieldValue(field));
				}
			}

			if (String.isBlank(base.displayNameL0)
					&& (base.isChanged(EmployeeBaseGeneratedEntity.FIELD_FIRST_NAME_L0)
						|| (base.isChanged(EmployeeBaseGeneratedEntity.FIELD_LAST_NAME_L0)))) {
				base.displayNameL0 = mergeBase.getDefaultDisplayNameL0(langL0);
			}
			if (String.isBlank(base.displayNameL1)
					&& (base.isChanged(EmployeeBaseGeneratedEntity.FIELD_FIRST_NAME_L1)
						|| (base.isChanged(EmployeeBaseGeneratedEntity.FIELD_LAST_NAME_L1)))) {
				base.displayNameL1 = mergeBase.getDefaultDisplayNameL1(langL1);
			}
			if (String.isBlank(base.displayNameL2)
					&& (base.isChanged(EmployeeBaseGeneratedEntity.FIELD_FIRST_NAME_L2)
						|| (base.isChanged(EmployeeBaseGeneratedEntity.FIELD_LAST_NAME_L2)))) {
				base.displayNameL2 = mergeBase.getDefaultDisplayNameL2(langL2);
			}

		}

		/**
		 * パラメータの値を設定したベースエンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public EmployeeHistoryEntity createHistoryEntity(Map<String, Object> paramMap) {

			EmployeeHistoryEntity history = new EmployeeHistoryEntity();

			// Name項目はServiceクラスで保存するときに設定される
			if(paramMap.containsKey('costCenterId')){
				history.costCenterId = this.costCenterId;
			}
			if(paramMap.containsKey('departmentId')){
				history.departmentId = this.departmentId;
			}
			if(paramMap.containsKey('title_L0')){
				history.titleL0 = this.title_L0;
			}
			if(paramMap.containsKey('title_L1')){
				history.titleL1 = this.title_L1;
			}
			if(paramMap.containsKey('title_L2')){
				history.titleL2 = this.title_L2;
			}
			if(paramMap.containsKey('managerId')){
				history.managerId = this.managerId;
			}
			if(paramMap.containsKey('validDateFrom')){
				history.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			// 有効開始日のデフォルトは実行日
			if (history.validFrom == null) {
				history.validFrom = AppDate.today();
			}
			if(paramMap.containsKey('validDateTo')){
				history.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (history.validTo == null) {
				history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}
			if(paramMap.containsKey('comment')){
				history.historyComment = this.comment;
			}
			if(paramMap.containsKey('workingTypeId')){
				history.workingTypeId = this.workingTypeId;
			}
			if(paramMap.containsKey('timeSettingId')){
				history.timeSettingId = this.timeSettingId;
			}
			if(paramMap.containsKey('agreementAlertSettingId')){
				history.agreementAlertSettingId = this.agreementAlertSettingId;
			}
			if (paramMap.containsKey('permissionId')) {
				history.permissionId = this.permissionId;
			}
			if(paramMap.containsKey('calendarId')){
				history.calendarId = this.calendarId;
			}
			if (paramMap.containskey('approver01Id')) {
				history.approverBase01Id = this.approver01Id;
			}
			if (paramMap.containskey('approvalAuthority01')) {
				history.approvalAuthority01 = this.approvalAuthority01;
			}

			if (paramMap.containsKey('commuterPassAvailable')) {
				history.commuterPassAvailable = this.commuterPassAvailable;
			}
			if (paramMap.containsKey('jorudanRoute')) {
				history.jorudanRoute = this.jorudanRoute == null ? null : JSON.serialize(this.jorudanRoute);
			}
			if (paramMap.containsKey('expEmployeeGroupId')) {
				history.expEmployeeGroupId = this.expEmployeeGroupId;
			}

			return history;
		}

	}

	/**
	 * @description ルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目 */
		public String name;
	}

	/**
	 * @desctiprion ユーザを表すクラス
	 */
	public class User {
		/** 顔写真URL */
		public String photoUrl;
	}

	/**
	 * @description Class representing Cost Center
	 */
	public class CostCenter {
		/** Cost Center Name */
		public String name;
		/** Cost Center Code */
		public String code;
	}

	/**
	 * @description 部署を表すクラス
	 */
	public class Department {
		/** レコードのName項目 */
		public String name;
		/** レコードのName項目 */
		public String code;
	}

	/**
	 * @description Expense Employee Group
	 */
	public class ExpEmployeeGroup {
		/** Expense Employee Group Name */
		public String name;
		/** Expense Employee Group Code */
		public String code;
	}

	/**
	 * @description 社員ベースレコード作成結果レスポンス
	 */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したレコードID */
		public String id;
	}

	/**
	 * @description 社員レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EMPLOYEE;

		/**
		 * @description 社員レコードを1件作成する
		 * @param  req リクエストパラメータ(Employee)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			Employee param = (Employee)req.getParam(EmployeeResource.Employee.class);

			EmployeeBaseEntity base = param.createBaseEntity(req.getParamMap());
			EmployeeHistoryEntity history = param.createHistoryEntity(req.getParamMap());

			if (String.isNotBlank(param.companyId) && ExpCommonUtil.getCompany(param.companyId).useExpense) {
				ExpCommonUtil.validateId(MESSAGE.Exp_Lbl_EmployeeGroup, param.expEmployeeGroupId, true);
			}

			base.addHistory(history);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// SFユーザの重複チェック
			EmployeeResource.validateSFUserDuplicated(base, false);

			// 保存する
			EmployeeService service = new EmployeeService();
			Id resId = service.saveNewEntityWithValidation(base);

			EmployeeResource.SaveResult res = new EmployeeResource.SaveResult();
			res.Id = resId;
			return res;
		}
	}

	/**
	 * @desctiprion 社員レコード更新処理を実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EMPLOYEE;

		/**
		 * @description 社員レコードを1件更新する
		 * @param  req リクエストパラメータ(Employee)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			Employee param = (Employee)req.getParam(EmployeeResource.Employee.class);

			// パラメータのバリデーション
			validateParam(param);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			EmployeeBaseEntity base = param.createBaseEntity(req.getParamMap());

			// SFユーザの重複チェック
			EmployeeResource.validateSFUserDuplicated(base, true);

			EmployeeService service = new EmployeeService();
			service.updateBaseWithValidation(base);

			// 成功レスポンスをセットする
			return null;
		}

		/**
		 * ベース更新APIのバリデーションを実行
		 */
		private void validateParam(EmployeeResource.Employee param) {
			// ID
			if (String.isBlank(param.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('ID');
				throw ex;
			}
		}
	}

	/**
	 * @description 社員ベースレコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象ベースレコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}

	}

	/**
	 * @description 社員ベースレコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EMPLOYEE;

		/**
		 * @description 社員レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				(new EmployeeService()).deleteEntity(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合は成功レスポンスを返す
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					 throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		 }

	}

	/**
	 * @description 社員レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 関連する会社レコードID */
		public String companyId;
		/** 対象日付 */
		public Date targetDate;

		/** 社員コード */
		public String code;
		/** 社員名 */
		public String name;
		/** 部署ID */
		public String departmentId;
		/** 部署コード */
		public String departmentCode;
		/** 部署名 */
		public String departmentName;
		/** 役職 */
		public String title;
		/** 上長名 */
		public String managerName;
		/** 勤務体系名 */
		public String workingTypeName;
		/** 承認者権限01 */
		public Boolean approvalAuthority01;
		/** Expense Employee Group Id */
		public String expEmployeeGroupId;
	}

	/**
	 * @description 社員レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public Employee[] records;
	}

	/**
	 * @description 社員レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description 社員レコードを1件登録する
		 * @param  parameter リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);
			Map<String, Object> paramMap = req.getParamMap();

			// 社員を検索する
			EmployeeRepository.SearchFilter filter = createEmployeeFilter(param, paramMap);
			List<EmployeeBaseEntity> entitylist = (new EmployeeService()).searchEntity(filter);

			// パラメータ departmentName が指定されている場合は、社員検索結果のうち所属部署名がマッチする社員のみを抽出
			if (paramMap.containsKey('departmentName') && String.isNotBlank(param.departmentName)) {
				if (entitylist != null && entityList.size() > 0) {
					// 所属部署が指定条件に該当する社員を抽出する
					DepartmentRepository.SearchFilter deptFilter = createDepartmentFilter(param, paramMap);
					entitylist = (new EmployeeService()).filterEmployeeByDepartment(entitylist, deptFilter);
				}
			}

			// パラメータ workingTypeName が指定されている場合は、社員検索結果のうち勤務体系名がマッチする社員のみを抽出
			if (paramMap.containsKey('workingTypeName') && String.isNotBlank(param.workingTypeName)) {
				if (entitylist != null && entityList.size() > 0) {
					// 所属部署が指定条件に該当する社員を抽出する
					AttWorkingTypeRepository.SearchFilter workingTypeFilter = createWorkingTypeFilter(param, paramMap);
					entitylist = (new EmployeeService()).filterEmployeeByWorkingType(entitylist, workingTypeFilter);
				}
			}

			// レスポンスクラスに変換
			List<Employee> empList = new List<Employee>();
			for(EmployeeBaseEntity base : entitylist) {
				EmployeeHistoryEntity history = base.getHistory(0);
				Employee e = new Employee();
				e.id = base.id;
				e.historyId = history.id;
				e.name = base.fullNameL.getFullName();
				e.firstName_L0 = base.firstNameL0;
				e.firstName_L1 = base.firstNameL1;
				e.firstName_L2 = base.firstNameL2;
				e.lastName_L0 = base.lastNameL0;
				e.lastName_L1 = base.lastNameL1;
				e.lastName_L2 = base.lastNameL2;
				e.middleName_L0 = base.middleNameL0;
				e.middleName_L1 = base.middleNameL1;
				e.middleName_L2 = base.middleNameL2;
				e.displayName = base.displayNameL.getValue();
				e.displayName_L0 = base.displayNameL0;
				e.displayName_L1 = base.displayNameL1;
				e.displayName_L2 = base.displayNameL2;
				e.code = base.code;
				e.companyId = base.companyId;
				e.title = history.titleL.getValue();
				e.title_L0 = history.titleL0;
				e.title_L1 = history.titleL1;
				e.title_L2 = history.titleL2;
				e.startDate = AppConverter.dateValue(base.validFrom);
				e.validDateFrom = AppConverter.dateValue(history.validFrom);
				e.validDateTo = AppConverter.dateValue(history.validTo);
				e.comment = history.historyComment;
				e.calendarId = history.calendarId;
				e.permissionId = history.permissionId;
				e.approver01Id = history.approverBase01Id;
				e.approvalAuthority01 = history.approvalAuthority01;

				// ユーザ
				e.userId = base.userId;
				e.user = new User();
				if (base.user != null) {
					e.user.photoUrl = base.user.photoUrl;
				}
				e.isActiveSFUserAcc = base.user.isActive;

				// 上長
				e.managerId = history.managerId;
				e.manager = new LookupField();
				if (history.manager != null) {
					e.manager.name = history.manager.fullNameL.getFullName();
				}

				// The associated Cost Center may be expired (invalid)
				e.costCenterId = history.costCenterId;
				e.costCenter = new CostCenter();
				if (history.costCenter != null) {
					e.costCenter.name = history.costCenter.nameL.getValue();
					e.costCenter.code = history.costCenter.code;
				}

				// 部署
				// departmentId が設定されていても有効期間外の可能性もあるため
				// department がnullでないことを確認すること
				e.departmentId = history.departmentId;
				e.department = new Department();
				if (history.department != null) {
					e.department.name = history.department.nameL.getValue();
					e.department.code = history.department.code;
				}

				// 勤務体系
				e.workingTypeId = history.workingTypeId;
				e.workingType = new LookupField();
				if (history.workingType != null) {
					e.workingType.name = history.workingType.nameL.getValue();
				}

				// 工数設定
				e.timeSettingId = history.timeSettingId;
				e.timeSetting = new LookupField();
				if (history.timeSetting != null) {
					e.timeSetting.name = history.timeSetting.nameL.getValue();
				}

				// 36協定アラート設定
				e.agreementAlertSettingId = history.agreementAlertSettingId;
				e.agreementAlertSetting = new LookupField();
				if (history.agreementAlertSetting != null) {
					e.agreementAlertSetting.name = history.agreementAlertSetting.nameL.getValue();
				}

				// 定期情報
				e.commuterPassAvailable = history.commuterPassAvailable;
				if (String.isNotBlank(history.jorudanRoute)) {
					e.jorudanRoute = (ExpTransitJorudanService.CommuterRouteDetail)JSON.deserialize(
							history.jorudanRoute, ExpTransitJorudanService.CommuterRouteDetail.class);
				}

				e.expEmployeeGroupId = history.expEmployeeGroupId;
				e.expEmployeeGroup = new ExpEmployeeGroup();
				if (history.expEmployeeGroup != null) {
					e.expEmployeeGroup.code = history.expEmployeeGroup.code;
					e.expEmployeeGroup.name = history.expEmployeeGroup.nameL.getValue();
				}
				empList.add(e);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = empList;
			return res;
		}

		/**
		 * 検索パラメータから社員の検索フィルタを作成する
		 */
		public EmployeeRepository.SearchFilter createEmployeeFilter(
				SearchCondition param, Map<String, Object> paramMap) {

			EmployeeRepository.SearchFilter filter = new EmployeeRepository.SearchFilter();
			final AppDate defaultTargetDate = AppDate.today(); // 履歴取得対象日(デフォルトは今日の日付)

			// パラメータ id が指定されている場合は、idで検索
			if(paramMap.containsKey('id')){
				filter.baseIds= new Set<Id>{param.id};
			}
			// パラメータ companyId が指定されている場合は、会社IDで検索
			if(paramMap.containsKey('companyId')){
				filter.companyIds = new Set<Id>{param.companyId};
			}
			// パラメータ targetDate が指定されている場合は、その日付時点の履歴から取得
			if(paramMap.containsKey('targetDate')){
				filter.targetDate = AppDate.valueOf(param.targetDate);
			}
			if (filter.targetDate == null) {
				filter.targetDate = EmployeeResource.SEARCH_DEFAULT_TARGET_DATE;
			}

			// パラメータ code が指定されている場合は、社員コードで検索（部分一致）
			if(paramMap.containsKey('code')){
				filter.code = param.code;
			}
			// パラメータ name が指定されている場合は、社員名で検索（部分一致）
			if(paramMap.containsKey('name')){
				filter.nameL = param.name;
			}
			// パラメータ departmentId が指定されている場合は、部署ベースIDで検索
			if(paramMap.containsKey('departmentId')){
				filter.departmentIds = new Set<Id>{param.departmentId};
			}
			// パラメータ departmentCode が指定されている場合は、部署コードで検索（部分一致）
			if(paramMap.containsKey('departmentCode')){
				filter.departmentCode = param.departmentCode;
			}
			// パラメータ title が指定されている場合は、役職で検索（部分一致）
			if(paramMap.containsKey('title')){
				filter.titleL = param.title;
			}
			// パラメータ managerName が指定されている場合は、上長名で検索（部分一致）
			if(paramMap.containsKey('managerName')){
				filter.managerNameL = param.managerName;
			}
			// パラメータ approvalAuthority01 が指定されている場合は、承認者権限01で検索
			if(paramMap.containsKey('approvalAuthority01')
					&& param.approvalAuthority01 == true){
				filter.approvalAuthority01 = true;
			}
			// Set Expense Employee Group Id to Search Filter
			if (paramMap.containsKey('expEmployeeGroupId')) {
				filter.expEmployeeGroupIdSet = new Set<Id>{param.expEmployeeGroupId};
			}
			return filter;
		}

		/**
		 * 検索パラメータから部署の検索フィルタを作成する
		 */
		public DepartmentRepository.SearchFilter createDepartmentFilter(
				SearchCondition param, Map<String, Object> paramMap) {

			DepartmentRepository.SearchFilter filter = new DepartmentRepository.SearchFilter();
			// 会社ID
			if (paramMap.containsKey('companyId') && (param.companyId != null)) {
				filter.companyIds = new List<Id>{param.companyId};
			}
			// 対象日
			if (paramMap.containsKey('targetDate')) {
				filter.targetDate = AppDate.valueOf(param.targetDate);
			}
			if (filter.targetDate == null) {
				filter.targetDate = EmployeeResource.SEARCH_DEFAULT_TARGET_DATE;
			}
			// 部署名（部分一致）
			if (paramMap.containsKey('departmentName')) {
				filter.nameL = param.departmentName;
			}

			return filter;
		}

		/**
		 * 検索パラメータから部署の検索フィルタを作成する
		 */
		public AttWorkingTypeRepository.SearchFilter createWorkingTypeFilter(
				SearchCondition param, Map<String, Object> paramMap) {

			AttWorkingTypeRepository.SearchFilter filter = new AttWorkingTypeRepository.SearchFilter();
			// 会社ID
			if (paramMap.containsKey('companyId') && (param.companyId != null)) {
				filter.companyIds = new Set<Id>{param.companyId};
			}
			// 対象日
			if (paramMap.containsKey('targetDate')) {
				filter.targetDate = AppDate.valueOf(param.targetDate);
			}
			if (filter.targetDate == null) {
				filter.targetDate = EmployeeResource.SEARCH_DEFAULT_TARGET_DATE;
			}
			// 勤務体系名（部分一致）
			if (paramMap.containsKey('workingTypeName')) {
				filter.nameL = param.workingTypeName;
			}

			return filter;
		}

	}

	/**
	 * Salesforceユーザの重複チェックを行う
	 * @param 登録、更新対象の社員エンティティ
	 */
	private static void validateSFUserDuplicated(EmployeeBaseEntity base, Boolean forUpdate) {

		List<String> keyList = new List<String>();
		UserEntity user = new UserEntity();
		EmployeeRepository empRepo = new EmployeeRepository();
		CompanyRepository comRepo = new CompanyRepository();
		UserRepository userRepo = new UserRepository();

		// 更新時
		if (forUpdate) {
			EmployeeBaseEntity orgBase = empRepo.getEntity(base.id);
			CompanyEntity company = comRepo.getEntity(orgBase.companyId);

			if (String.isBlank(base.userId)) {
				user = userRepo.getEntity(orgBase.userId);
			} else {
				user = userRepo.getEntity(base.userId);
			}
			keyList.add(company.code);
			keyList.add(orgBase.code);

		// 登録時
		} else {
			CompanyEntity company = comRepo.getEntity(base.companyId);
			user = userRepo.getEntity(base.userId);
			keyList.add(company.code);
			keyList.add(base.code);
		}

		// 重複チェック
		Map<List<String>, String> paramMap = new Map<List<String>, String>();
		paramMap.put(keyList, user.userName);
		Map<List<String>, Boolean> resultMap = new EmployeeService().isSalesforceUserDuplicated(paramMap);

		// 重複がある場合、例外を発生する
		if (resultMap.get(keyList)) {
			throw new App.ParameterException(MESSAGE.Admin_Err_SfdcUserDuplicate);
		}
	}
}