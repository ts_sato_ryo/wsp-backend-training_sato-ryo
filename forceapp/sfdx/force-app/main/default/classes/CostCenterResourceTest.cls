/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for Cost Center Resource (API Testing)
 */
@isTest
private class CostCenterResourceTest {

	/*
	 * Normal Use Case Test: Create a Cost Center
	 */
	@isTest
	static void createCostCenterPositiveTest() {

		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c parent = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, 1, 1
		)[0];

		Date currentDate = Date.today();

		Test.startTest();

		CostCenterResource.CostCenter param = new CostCenterResource.CostCenter();
		param.name_L0 = 'TestCostCenter Name_L0';
		param.name_L1 = 'TestCostCenter Name_L1';
		param.name_L2 = 'TestCostCenter Name_L2';

		param.code = 'TEST_COST_CENTER_001';
		param.companyId = company.Id;
		param.parentId = parent.Id;
		param.linkageCode = 'SAMPLE_LINKAGE_CODE';
		param.validDateFrom = AppDate.valueOf(currentDate - 30).format();
		param.validDateTo = AppDate.valueOf(currentDate).format();
		param.comment = 'Testing Cost Center Create API';

		CostCenterResource.CreateApi api = new CostCenterResource.CreateApi();
		CostCenterResource.SaveResult res = (CostCenterResource.SaveResult)api.execute(param);

		Test.stopTest();

		// Retrieve the newly created Cost Center
		List<ComCostCenterBase__c> retrievedRecords = [
			SELECT
			Id,
			Name,
			Code__c,
			CompanyId__c,
			CurrentHistoryId__c,
			(SELECT
				Id,
				BaseId__c,
				UniqKey__c,
				Name, Name_L0__c, Name_L1__c, Name_L2__c,
				ParentBaseId__c,
				LinkageCode__c,
				ValidFrom__c, ValidTo__c,
				HistoryComment__c
				FROM Histories__r
			)
			FROM ComCostCenterBase__c
			WHERE Id != :parent.Id
		];

		System.assertEquals(1, retrievedRecords.size());
		System.assertEquals(1, retrievedRecords[0].Histories__r.size());

		ComCostCenterBase__c retrievedBase = retrievedRecords[0];
		ComCostCenterHistory__c retrievedHistory = retrievedBase.Histories__r[0];

		//Set the returned ID from API to the param and it will be test in the verifyCostCenter method.
		param.id = res.Id;
		verifyCostCenter(company, param, retrievedBase, retrievedHistory, false);
	}

	/*
	 * Invalid Param/Missing Param when Creating new CostCenter
	 */
	@isTest
	static void createCostCenterMissingParamNegativeTest() {
		Test.startTest();

		CostCenterResource.CostCenter param = new CostCenterResource.CostCenter();
		CostCenterResource.CreateApi api = new CostCenterResource.CreateApi();
		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}

	/*
	 * Verify that exception is thrown when the same LinkageCode is used.
	 */
	/*@IsTest
	static void createCostCenterDuplicateLinkageCodeNegativeTest() {
	    // Currently, linkageCode is changed not to be Unique
	    // and there will be no checking performed for now.
		Test.startTest();
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c parent = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, 1, 1, true
		)[0];

		Date currentDate = Date.today();

		CostCenterResource.CostCenter param = new CostCenterResource.CostCenter();
		param.name_L0 = 'TestCostCenter Name_L0';
		param.name_L1 = 'TestCostCenter Name_L1';
		param.name_L2 = 'TestCostCenter Name_L2';
		param.code = 'TEST_COST_CENTER_001';
		param.companyId = company.Id;
		param.parentId = parent.Id;
		param.linkageCode = 'lc_0_1';
		param.validDateFrom = AppDate.valueOf(currentDate + 90).format();
		param.validDateTo = AppDate.valueOf(currentDate + 100).format();
		param.comment = 'Testing Duplicate Linkage';
		CostCenterResource.CreateApi api = new CostCenterResource.CreateApi();

		App.ParameterException ex;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			ex = e;
		}
		Test.stopTest();
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
	}*/

	/*
	 * Test the Normal Use case of Updating Existing Cost Center Code
	 */
	@IsTest
	static void updateCostCenterPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		// Since the histories are generated with 1 year period, -20 months from current date to make the first one a past history.
		List<ComCostCenterBase__c> originalCostCenters = ComTestDataUtility.createCostCentersWithHistory(
				'Root Cost Center', company.Id, 3, 3, true, -20
		);

		CostCenterResource.CostCenter param = new CostCenterResource.CostCenter();
		param.id = originalCostCenters[1].Id;
		param.code = 'Updated_Code';
		Test.startTest();
		CostCenterResource.UpdateApi api = new CostCenterResource.UpdateApi();
		api.execute(param);
		Test.stopTest();


		ComCostCenterBase__c retrievedRecords = [
			SELECT
			Id,
			Code__c,
			UniqKey__c,
			(SELECT
				Id,
				UniqKey__c,
				ValidFrom__c, ValidTo__c
				FROM Histories__r
			)
			FROM ComCostCenterBase__c
			WHERE Id = :param.Id
		];
		System.assertEquals(param.id, retrievedRecords.Id);
		System.assertEquals(param.code, retrievedRecords.Code__c);
		String updatedBaseUniqKey = company.Code__c + '-' + param.code;
		System.assertEquals(updatedBaseUniqKey, retrievedRecords.UniqKey__c);

		// For the histories, check that the uniqKey are updated accordingly
		for (Integer i = 0; i < 3; i++) {
			String fromDate = AppDate.valueOf(retrievedRecords.Histories__r[i].ValidFrom__c).formatYYYYMMDD();
			String toDate = AppDate.valueOf(retrievedRecords.Histories__r[i].ValidTo__c).formatYYYYMMDD();
			System.assertEquals(updatedBaseUniqKey + '-' + fromDate + '-' + toDate,
					retrievedRecords.Histories__r[i].UniqKey__c);
		}
	}

	/*
	 * Checks that when the Code specified is the same as current, no exception is thrown.
	 */
	@IsTest
	static void updateCostCenterCodeNoChangePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		// Since the histories are generated with 1 year period, -20 months from current date to make the first one a past history.
		List<ComCostCenterBase__c> originalCostCenters = ComTestDataUtility.createCostCentersWithHistory(
				'Root Cost Center', company.Id, 3, 3, true, -20
		);

		CostCenterResource.CostCenter param = new CostCenterResource.CostCenter();
		param.id = originalCostCenters[1].Id;
		param.code = originalCostCenters[1].Code__c;        //Assign the same Code as the current code
		Test.startTest();
		CostCenterResource.UpdateApi api = new CostCenterResource.UpdateApi();
		api.execute(param);
		Test.stopTest();

		ComCostCenterBase__c updatedBase = [
			SELECT Id, Code__c, UniqKey__c
			FROM ComCostCenterBase__c
			WHERE id = :param.id
		];

		System.assertEquals(param.code, updatedBase.Code__c);
		String updatedBaseUniqKey = company.Code__c + '-' + param.code;
		// Originally, the uniqKey wasn't constructed based on standard and it was just Name + Num
		// But this is not really a normal use case since users can't specify UniqKey manually.
		System.assertEquals(updatedBaseUniqKey, updatedBase.UniqKey__c);
	}

	/*
	 * Checks that when the Code specified is already in used, exception is thrown.
	 */
	@IsTest
	static void updateCostCenterCodeAlreadyInUsedNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		// Since the histories are generated with 1 year period, -20 months from current date to make the first one a past history.
		List<ComCostCenterBase__c> originalCostCenters = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, 3, 3, true, -20
		);

		CostCenterResource.CostCenter param = new CostCenterResource.CostCenter();
		param.id = originalCostCenters[1].Id;
		param.code = originalCostCenters[2].Code__c;
		Test.startTest();
		CostCenterResource.UpdateApi api = new CostCenterResource.UpdateApi();
		Exception thrownException;
		try {
			api.execute(param);
		} catch (App.ParameterException e) {
			thrownException = e;
		}
		Test.stopTest();

		System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, thrownException.getMessage());
	}

	/*
	 * Test for deleting a Cost Center which isn't used by any other Cost Center
	 */
	@IsTest
	static void deleteCostCenterPositiveTest () {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer baseSize = 3;
		Integer historyPerBase = 3;
		// Shift by -20 months to make the first history a past history
		ComCostCenterBase__c originalCostCenter = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, baseSize, historyPerBase, true, -20
		)[1];

		CostCenterResource.DeleteParam param = new CostCenterResource.DeleteParam();
		param.id = originalCostCenter.Id;
		Test.startTest();
		CostCenterResource.DeleteApi api = new CostCenterResource.DeleteApi();
		api.execute(param);
		Test.stopTest();

		// Check that it doesn't delete the other Base or History
		List<ComCostCenterBase__c> retrievedBases = [SELECT Id FROM ComCostCenterBase__c];
		List<ComCostCenterHistory__c> retrievedHistories = [SELECT Id, ParentBaseId__c FROM ComCostCenterHistory__c];
		System.assertEquals(baseSize - 1, retrievedBases.size());
		System.assertEquals((baseSize * historyPerBase) - historyPerBase, retrievedHistories.size());

		// Check that those deleted are indeed the specified Base and its histories
		Boolean deleteTargetExist = false;
		for (ComCostCenterBase__c base: retrievedBases) {
			if (base.Id == param.id) {
				deleteTargetExist = true;
				break;
			}
		}
		System.assertEquals(false, deleteTargetExist);
		for (ComCostCenterHistory__c history: retrievedHistories) {
			if (history.ParentBaseId__c == param.id) {
				deleteTargetExist = true;
				break;
			}
		}
		System.assertEquals(false, deleteTargetExist);
	}

	/*
	 * Users can overwrite the latest using the same date and the history will be deleted logically.
	 * In such cases, users should be able to Delete the Cost Center successfully because
	 * the History which is parenting the Delete Target has already been deleted logically.
	 */
	@IsTest
	static void deleteCostCenterWithLogicallyDeletedChildPositiveTest () {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer baseSize = 3;
		Integer historyPerBase = 3;
		// Shift by -20 months to make the first history a past history
		List<ComCostCenterBase__c> originalCostCenters = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, baseSize, historyPerBase, false, -20
		);

		List<ComCostCenterHistory__c> withParents = [
				SELECT Id, ParentBaseId__c,
				ValidFrom__c, ValidTo__c
				FROM ComCostCenterHistory__c
				WHERE BaseId__c = :originalCostCenters[0].Id
		];

		// Set the Base 1, Last History's Parent as Base 2
		withParents[2].ParentBaseId__c = originalCostCenters[1].Id; //Parent of Past History
		update withParents[2];

		//Overwrite the above history by using the same day, and change the Parent to Base 3
		// Here, instead of just manually Removed__c, use the service so that it will actually mimic the actual data state
		// Prepare the new History Record
		CostCenterHistoryEntity historyEntity = new CostCenterHistoryEntity();
		historyEntity.baseId = originalCostCenters[0].Id;
		historyEntity.nameL0 = 'New History';
		historyEntity.parentBaseId = originalCostCenters[2].Id;

		 // The Valid From must be the same in order to overwrite
		historyEntity.validFrom = AppDate.valueOf(withParents[2].ValidFrom__c);
		historyEntity.validTo = AppDate.valueOf(withParents[2].ValidTo__c);
		historyEntity.historyComment = 'Overwrite Existing Last History and Changed Parent';

		// Overwrite existing history
		CostCenterService service = new CostCenterService();
		service.saveCostCenterHistoryRecord(historyEntity);

		//Since the correctness depends on pre-data, add a checking to be sure
		List<ComCostCenterHistory__c> beforeDelete = [SELECT Id, ParentBaseId__c, Removed__c
			FROM ComCostCenterHistory__c
			WHERE BaseId__c = :originalCostCenters[0].Id
		];
		System.assertEquals(baseSize + 1, beforeDelete.size());

		 //Delete the Cost Center
		CostCenterResource.DeleteParam param = new CostCenterResource.DeleteParam();
		param.id = originalCostCenters[0].Id;
		Test.startTest();
		CostCenterResource.DeleteApi api = new CostCenterResource.DeleteApi();
		api.execute(param);
		Test.stopTest();

		// Check that it doesn't delete the other Base or History
		List<ComCostCenterBase__c> retrievedBases = [SELECT Id FROM ComCostCenterBase__c];
		List<ComCostCenterHistory__c> retrievedHistories = [SELECT Id, ParentBaseId__c FROM ComCostCenterHistory__c];
		System.debug(retrievedBases);
		System.debug(retrievedHistories);
		System.assertEquals(baseSize - 1, retrievedBases.size());
		System.assertEquals((baseSize - 1) * historyPerBase, retrievedHistories.size());

		// Check that those deleted are indeed the specified Base and its histories
		Boolean deleteTargetExist = false;
		for (ComCostCenterBase__c base: retrievedBases) {
			if (base.Id == param.id) {
				deleteTargetExist = true;
				break;
			}
		}
		System.assertEquals(false, deleteTargetExist);
		for (ComCostCenterHistory__c history: retrievedHistories) {
			if (history.ParentBaseId__c == param.id) {
				deleteTargetExist = true;
				break;
			}
		}
		System.assertEquals(false, deleteTargetExist);
	}

	/*
	 * Checks that if the cost center has a child (sub-cost center), it cannot be deleted.
	 */
	@IsTest
	static void deleteCostCenterWithChildNegativeTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer baseSize = 4;
		Integer historyPerBase = 3;
		// Shift by -20 months to make the first history a past history
		List<ComCostCenterBase__c> originalCostCenters = ComTestDataUtility.createCostCentersWithHistory(
			'Root Cost Center', company.Id, baseSize, historyPerBase, true, -20
		);

		List<ComCostCenterHistory__c> withParents = [
			SELECT Id, ParentBaseId__c
			FROM ComCostCenterHistory__c
			WHERE BaseId__c = :originalCostCenters[0].Id
		];

		// Set First Base Cost Center Histories to different parents
		// Past History to 2nd Cost Center, Current History to 3rd Cost Center, Future to 4th Cost Center
		withParents[0].ParentBaseId__c = originalCostCenters[1].Id; //Parent of Past History
		withParents[1].ParentBaseId__c = originalCostCenters[2].Id; //Parent of Current History
		withParents[2].ParentBaseId__c = originalCostCenters[3].Id; //Parent of Future History
		update withParents;

		CostCenterResource.DeleteParam param = new CostCenterResource.DeleteParam();
		CostCenterResource.DeleteApi api = new CostCenterResource.DeleteApi();
		Test.startTest();

		List<App.ParameterException> exceptions = new List<App.ParameterException>();
		for (Integer i = 1; i < originalCostCenters.size(); i++) {
			try {
				param.id = originalCostCenters[i].Id;
				api.execute(param);
			} catch (App.ParameterException e) {
				exceptions.add(e);
			}
		}
		Test.stopTest();
		System.assertEquals(3, exceptions.size());
		for (App.ParameterException e: exceptions) {
			System.assertEquals(ComMessage.msg().Com_Err_CannotDeleteReference, e.getMessage());
		}
	}

	/**
	 * Normal Search Use Case: Positive Test
	 * This test focus on the number of records (details of retrieved records are tested in another test).
	 */
	@isTest
	static void searchCostCenterByIdPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 3;
		Integer numberOfHistoryRecordPerBaseRecord = 3;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true);
		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c
			WHERE BaseId__c =: costCenters[0].Id
			LIMIT 1
		];
		history.ParentBaseId__c = costCenters[1].Id;
		update history;

		Test.startTest();

		Map<String, Object> idParamMap = new Map<String, Object> {
		'id' => costCenters[0].id
		};
		Map<String, Object> parentParamMap = new Map<String, Object> {
		'parentId' => costCenters[1].Id
		};
		Map<String, Object> companyParamMap = new Map<String, Object> {
		'companyId' => costCenters[0].CompanyId__c
		};
		Map<String, Object> idAndParentIdParamMap = new Map<String, Object> {
		'id' => costCenters[0].id,
		'parentId' => costCenters[1].Id
		};

		CostCenterResource.SearchApi api = new CostCenterResource.SearchApi();
		CostCenterResource.SearchResult searchResultById = (CostCenterResource.SearchResult) api.execute(idParamMap);
		CostCenterResource.SearchResult searchResultByParentId = (CostCenterResource.SearchResult) api.execute(parentParamMap);
		CostCenterResource.SearchResult searchResultByCompanyId = (CostCenterResource.SearchResult) api.execute(companyParamMap);
		CostCenterResource.SearchResult searchResultByIdAndParentId = (CostCenterResource.SearchResult) api.execute(idAndParentIdParamMap);
		Test.stopTest();

		// Verify that the correct number if records are retrieved
		System.assertEquals(1, searchResultById.records.size());
		System.assertEquals(1, searchResultByParentId.records.size());
		System.assertEquals(numberOfBaseRecords, searchResultByCompanyId.records.size());
		System.assertEquals(1, searchResultByIdAndParentId.records.size());

		// Verify that
		// Search with ID and get back the original record back
		System.assertEquals((String) idParamMap.get('id'), searchResultById.records[0].id);
		// Searched with ParentID, but the expected is the child record (since the parent has only one child for that record)
		System.assertEquals((String) parentParamMap.get('parentId'), searchResultByParentId.records[0].parentId);
		// Search with both
		System.assertEquals((String) idAndParentIdParamMap.get('id'), searchResultById.records[0].id);
	}

	/**
	 * Verify that the default "Name" field will be based on the Language Setting
	 * Set the L0 as something which is not the user Language. When the record is retrieve,
	 * the Name__L1__c (user default language) should be in the Default "Name" field.
	 */
	@isTest
	static void searchCostCenterL0IsNotUserLanguagePositiveTest() {
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting(ComTestDataUtility.getNotUserLang(), Userinfo.getLanguage(), null);
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComCostCenterBase__c costCenter = ComTestDataUtility.createCostCentersWithHistory('Test Cost Center',
				company.Id, 1, 1).get(0);

		Test.startTest();
		Map<String, Object> paramMap = new Map<String, Object> {
		'id' => costCenter.id
		};
		CostCenterResource.SearchApi api = new CostCenterResource.SearchApi();
		CostCenterResource.SearchResult result = (CostCenterResource.SearchResult) api.execute(paramMap);
		Test.stopTest();

		System.assertEquals(1, result.records.size());
		System.assertEquals((String) paramMap.get('id'), result.records[0].id);

		// Multi-language assertion
		ComCostCenterHistory__c history = [
			SELECT Name_L0__c, Name_L1__c
			FROM ComCostCenterHistory__c WHERE BaseId__c =:costCenter.Id LIMIT 1
		];

		// Since the user default language is specified as L1 before, name should be same as L1
		System.assertEquals(history.Name_L1__c, result.records[0].name);

		// This test depends on L0 and L1 having different value.
		// Ensures the previous L1 test isn't passing just because the names have the same value.
		System.assertNotEquals(history.Name_L0__c, result.records[0].name);
	}

	/**
	 * Verify the CostCenter Search API without applying any filter.
	 * This test also checks that all the fields of retrieved data are correct.
	 */
	@isTest
	static void searchWithoutFilterPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		List<ComCostCenterBase__c> retrievedCostCenters = ComTestDataUtility.createCostCentersWithHistory('Test Cost Center',
				company.Id, 3, 3, true);
		ComCostCenterHistory__c historyWithParentCostCenter = [SELECT Id FROM ComCostCenterHistory__c
			WHERE BaseId__c =:retrievedCostCenters[0].Id
			LIMIT 1
		];
		historyWithParentCostCenter.ParentBaseId__c = retrievedCostCenters[1].Id;
		update historyWithParentCostCenter;

		Test.startTest();
		Map<String, Object> param = new Map<String, Object>();
		CostCenterResource.SearchApi api = new CostCenterResource.SearchApi();
		CostCenterResource.SearchResult searchApiResult = (CostCenterResource.SearchResult) api.execute(param);
		Test.stopTest();

		retrievedCostCenters = [
			SELECT
			Id,
			Name,
			Code__c,
			CompanyId__c,
			CurrentHistoryId__c,
			(SELECT
				Id,
				BaseId__c,
				Name,
				Name_L0__c,
				Name_L1__c,
				Name_L2__c,
				LinkageCode__c,
				ParentBaseId__c,
				ParentBaseId__r.Name,
				HistoryComment__c,
				ValidFrom__c,
				ValidTo__c
			FROM Histories__r),
			(SELECT
				Id
				FROM ChildHistories__r
				LIMIT 1)
			FROM ComCostCenterBase__c
		];

		ComCostCenterHistory__c parentHistory = [SELECT Name_L0__c, Name_L1__c, Name_L2__c
			FROM ComCostCenterHistory__c
			WHERE BaseId__c = :historyWithParentCostCenter.ParentBaseId__c
		][0];

		// The search using SOQL and API should return the same number of records
		System.assertEquals(retrievedCostCenters.size(), searchApiResult.records.size());
		for (Integer i = 0, n = retrievedCostCenters.size(); i < n; i++) {
			ComCostCenterBase__c base = retrievedCostCenters[i];
			//history are query with latest first
			//So take the last one (it's the current history) since searchAPI auto assume current date when not specified.
			ComCostCenterHistory__c history = base.Histories__r[2];
			CostCenterResource.CostCenter costCenterVo = searchApiResult.records[i];

			//NOTE: The ACTUAL and EXPECTED fields are actually in REVERSED order for the verifyCostCenter method
			verifyCostCenter(company, costCenterVo, base, history, true);
			System.assertEquals(base.ChildHistories__r.size() > 0, costCenterVo.hasChildren);
			if (costCenterVo.parentId == null) {
				System.assertEquals(null, costCenterVo.parent.name);
			} else {
				System.assertEquals(parentHistory.Name_L0__c, costCenterVo.parent.name);
			}
		}
	}

	/*
	 * Normal Search Use Case: Positive Test with query name
	 */
	@isTest
	static void searchCostCenterByNamePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 3;
		Integer numberOfHistoryRecordPerBaseRecord = 3;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true);
		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c
			WHERE BaseId__c =: costCenters[0].Id
			LIMIT 1];
		history.ParentBaseId__c = costCenters[1].Id;
		update history;

		Map<String, Object> queryParamMap = new Map<String, Object> {
		'query' => 'Test'
		};

		CostCenterResource.SearchApi api = new CostCenterResource.SearchApi();
		CostCenterResource.SearchResult searchResultByName =
				(CostCenterResource.SearchResult) api.execute(queryParamMap);

		// Verify that the correct number if records are retrieved
		System.assertEquals(3, searchResultByName.records.size());

		// Verify that the id are correct
		System.assertEquals(costCenters[0].id, searchResultByName.records[0].id);
		System.assertEquals(costCenters[1].id, searchResultByName.records[1].id);
		System.assertEquals(costCenters[2].id, searchResultByName.records[2].id);

		queryParamMap = new Map<String, Object> {
		'query' => 'Test Search1'
		};

		api = new CostCenterResource.SearchApi();
		searchResultByName = (CostCenterResource.SearchResult) api.execute(queryParamMap);

		// Verify that the correct number if records are retrieved
		System.assertEquals(1, searchResultByName.records.size());

		// Verify that the id are correct
		System.assertEquals(costCenters[0].id, searchResultByName.records[0].id);

	}

	/*
	 * Normal Search Use Case: Positive Test with query code
	 */
	@isTest
	static void searchCostCenterByCodePositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 3;
		Integer numberOfHistoryRecordPerBaseRecord = 1;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true);
		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c
			WHERE BaseId__c =: costCenters[0].Id
			LIMIT 1];
		history.ParentBaseId__c = costCenters[1].Id;
		update history;
		costCenters[1].Code__c = 'CenterCode';
		update costCenters[1];

		Map<String, Object> queryParamMap = new Map<String, Object> {
		'query' => 'Center'
		};

		CostCenterResource.SearchApi api = new CostCenterResource.SearchApi();
		CostCenterResource.SearchResult searchResultByCode =
				(CostCenterResource.SearchResult) api.execute(queryParamMap);

		// Verify that the correct number if records are retrieved
		System.assertEquals(1, searchResultByCode.records.size());

		// Verify that the id are correct
		System.assertEquals(costCenters[1].id, searchResultByCode.records[0].id);

		queryParamMap = new Map<String, Object> {
		'query' => 'Test Search1'
		};

		api = new CostCenterResource.SearchApi();
		searchResultByCode = (CostCenterResource.SearchResult) api.execute(queryParamMap);

		// Verify that the correct number if records are retrieved
		System.assertEquals(1, searchResultByCode.records.size());

		// Verify that the id are correct
		System.assertEquals(costCenters[0].id, searchResultByCode.records[0].id);

	}

	/**
	 * Search Use Case: Positive Test with Hierarchy Parent names
	 */
	@isTest
	static void searchCostCenterByNameWithHierarchyPositiveTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 14;
		Integer numberOfHistoryRecordPerBaseRecord = 1;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true, -1);
		ComCostCenterHistory__c history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[10].Id];
		history.ParentBaseId__c = costCenters[9].Id;
		update history;

		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[8].Id];
		history.ParentBaseId__c = costCenters[9].Id;
		update history;

		// CCB8 --> CCB11
		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[7].Id];
		history.ParentBaseId__c = costCenters[10].Id;
		update history;

		// CCB7 --> CCB8
		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[6].Id];
		history.ParentBaseId__c = costCenters[7].Id;
		update history;
		// CCB1 --> CCV7
		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[0].Id];
		history.ParentBaseId__c = costCenters[6].Id;
		update history;

		// CCB3 --> CCB1
		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[2].Id];
		history.ParentBaseId__c = costCenters[0].Id;
		update history;

		// Middle 2 relationship
		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[1].Id];
		history.ParentBaseId__c = costCenters[13].Id;
		update history;

		// Last 3 relationship
		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[4].Id];
		history.ParentBaseId__c = costCenters[5].Id;
		update history;

		history = [SELECT Id FROM ComCostCenterHistory__c WHERE BaseId__c =: costCenters[5].Id];
		history.ParentBaseId__c = costCenters[11].Id;
		update history;


		Map<String, Object> queryParamMap = new Map<String, Object> {
		'query' => 'Test Search3',
		'usedIn' => 'REPORT'
		};

		CostCenterResource.SearchApi api = new CostCenterResource.SearchApi();
		CostCenterResource.SearchResult searchResultByName = (CostCenterResource.SearchResult) api.execute(queryParamMap);

		// Verify that the correct number if records are retrieved
		System.assertEquals(1, searchResultByName.records.size());
		System.assertNotEquals(null, searchResultByName.records[0].hierarchyParentNameList);

		// Verify that the parent names are correct
		System.assertEquals('Test Search1 L0', searchResultByName.records[0].hierarchyParentNameList[0]);
		System.assertEquals('Test Search7 L0', searchResultByName.records[0].hierarchyParentNameList[1]);
		System.assertEquals('Test Search8 L0', searchResultByName.records[0].hierarchyParentNameList[2]);
		System.assertEquals('Test Search11 L0', searchResultByName.records[0].hierarchyParentNameList[3]);
		System.assertEquals('Test Search10 L0', searchResultByName.records[0].hierarchyParentNameList[4]);
	}


	/**
	 * Search Use Case: Moderate Stress Test with query name
	 * This test focus on the number of records created gand list of parents name.
	 */
	@isTest
	static void searchCostCenterByNameWithHierarchyLargeNumberTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		Integer numberOfBaseRecords = 1500;
		Integer numberOfHistoryRecordPerBaseRecord = 1;
		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('TB1_',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true, -1);
		List<ComCostCenterHistory__c> historyList = [SELECT Id FROM ComCostCenterHistory__c];

		for (Integer i = 999; i > 0; i--) {
			historyList[i].ParentBaseId__c = costCenters[i - 1].Id;
		}

		update historyList;

		Map<String, Object> queryParamMap = new Map<String, Object> {
		'query' => 'TB1_999',
		'usedIn' => 'REPORT'
		};

		CostCenterResource.SearchApi api = new CostCenterResource.SearchApi();
		CostCenterResource.SearchResult searchResultByName =
				(CostCenterResource.SearchResult) api.execute(queryParamMap);

		// Verify that the correct number if records are retrieved
		System.assertEquals(1, searchResultByName.records.size());
		System.assertEquals(998, searchResultByName.records[0].hierarchyParentNameList.size());

		// Verify that the parent names are correct
		Integer start = 1;
		for (Integer i = 997; i >= 0; i--) {
			String name = 'TB1_' + start + ' L0';
			System.assertEquals(name, searchResultByName.records[0].hierarchyParentNameList[i]);
			start++;
		}

	}

	/**
	 * CostCenter Recently Used Api Test Without Hierarchy
	 */
	@IsTest static void getRecentlyUpdatedCostCenterTest() {
		// Create CostCenters
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComEmpBase__c employee = ComTestDataUtility.createEmployeeWithHistory('testEmp', company.Id, null, UserInfo.getUserId());
		Integer numberOfBaseRecords = 12;
		Integer numberOfHistoryRecordPerBaseRecord = 3;

		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true, -1);
		List<Id> costCenterBaseIdList = new List<Id>();
		for (ComCostCenterBase__c base : costCenters) {
			costCenterBaseIdList.add(base.Id);
		}
		AppDate targetDate = AppDate.today();
		List<CostCenterBaseEntity> costCenterBaseEntityList = new CostCenterRepository().getEntityList(costCenterBaseIdList, targetDate);

		ExpRecentlyUsedService expRecUsedService = new ExpRecentlyUsedService();
		// Add to recently used CostCenter
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, costCenterBaseEntityList[3].getHistory(0).id, costCenterBaseEntityList[0].getHistory(0).id);

		// Retrieve CostCenters Check everything is correct
		CostCenterResource.GetRecentlyUsedListParam param = new CostCenterResource.GetRecentlyUsedListParam();
		param.employeeBaseId = employee.id;

		CostCenterResource.GetRecentlyUsedListApi api = new CostCenterResource.GetRecentlyUsedListApi();
		CostCenterResource.SearchResult res = (CostCenterResource.SearchResult)api.execute(param);

		CostCenterBaseEntity baseEntity = costCenterBaseEntityList[0];
		CostCenterHistoryEntity historyEntity = costCenterBaseEntityList[0].getHistory(0);

		// Retrieve 1
		System.assertEquals(1, res.records.size());
		CostCenterResource.CostCenter resultCostCenter = res.records[0];
		verifyCCResult(baseEntity, historyEntity, resultCostCenter);

	}

	/**
	 * CostCenter Recently Used Api Test Without Hierarchy
	 */
	@IsTest static void getRecentlyUpdatedCostCenterMaxTest() {
		// Create CostCenters
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		ComEmpBase__c employee = ComTestDataUtility.createEmployeeWithHistory('testEmp', company.Id, null, UserInfo.getUserId());
		Integer numberOfBaseRecords = 12;
		Integer numberOfHistoryRecordPerBaseRecord = 3;

		List<ComCostCenterBase__c> costCenters = ComTestDataUtility.createCostCentersWithHistory('Test Search',
				company.Id, numberOfBaseRecords, numberOfHistoryRecordPerBaseRecord, true, -1);
		List<Id> costCenterBaseIdList = new List<Id>();
		for (ComCostCenterBase__c base : costCenters) {
			costCenterBaseIdList.add(base.Id);
		}
		AppDate targetDate = AppDate.today();
		List<CostCenterBaseEntity> costCenterBaseEntityList = new CostCenterRepository().getEntityList(costCenterBaseIdList, targetDate);

		test.startTest();

		ExpRecentlyUsedService expRecUsedService = new ExpRecentlyUsedService();
		// Add to recently used CostCenter
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, costCenterBaseEntityList[3].getHistory(0).id, costCenterBaseEntityList[0].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[1].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[2].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[3].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[4].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[5].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[6].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[7].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[8].getHistory(0).id);
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[9].getHistory(0).id);

		Integer maxLimit = 10;
		// Retrieve CostCenters Check everything is correct
		CostCenterResource.GetRecentlyUsedListParam param = new CostCenterResource.GetRecentlyUsedListParam();
		param.employeeBaseId = employee.id;

		CostCenterResource.GetRecentlyUsedListApi api = new CostCenterResource.GetRecentlyUsedListApi();
		CostCenterResource.SearchResult res = (CostCenterResource.SearchResult)api.execute(param);

		System.assertEquals(10, res.records.size());

		for (Integer i = 0; i < maxLimit; i++) {
			CostCenterBaseEntity baseEntity = costCenterBaseEntityList[maxLimit-i-1];
			CostCenterHistoryEntity historyEntity = costCenterBaseEntityList[maxLimit-i-1].getHistory(0);

			// Retrieve CostCenter
			CostCenterResource.CostCenter resultCostCenter = res.records[i];
			verifyCCResult(baseEntity, historyEntity, resultCostCenter);
		}

		// Add New one and test it only contain latest 10
		expRecUsedService.saveRecentlyUsedCostCenterValues(employee.id, null, costCenterBaseEntityList[11].getHistory(0).id);
		res = (CostCenterResource.SearchResult)api.execute(param);

		System.assertEquals(10, res.records.size());

		verifyCCResult(costCenterBaseEntityList[11], costCenterBaseEntityList[11].getHistory(0), res.records[0]);
		verifyCCResult(costCenterBaseEntityList[9], costCenterBaseEntityList[9].getHistory(0), res.records[1]);
		verifyCCResult(costCenterBaseEntityList[8], costCenterBaseEntityList[8].getHistory(0), res.records[2]);
		verifyCCResult(costCenterBaseEntityList[7], costCenterBaseEntityList[7].getHistory(0), res.records[3]);
		verifyCCResult(costCenterBaseEntityList[6], costCenterBaseEntityList[6].getHistory(0), res.records[4]);
		verifyCCResult(costCenterBaseEntityList[5], costCenterBaseEntityList[5].getHistory(0), res.records[5]);
		verifyCCResult(costCenterBaseEntityList[4], costCenterBaseEntityList[4].getHistory(0), res.records[6]);
		verifyCCResult(costCenterBaseEntityList[3], costCenterBaseEntityList[3].getHistory(0), res.records[7]);
		verifyCCResult(costCenterBaseEntityList[2], costCenterBaseEntityList[2].getHistory(0), res.records[8]);
		verifyCCResult(costCenterBaseEntityList[1], costCenterBaseEntityList[1].getHistory(0), res.records[9]);

		test.stopTest();
	}

	/*
	 * Verify that the Cost Center created using API has the expected values.
	 */
	private static void verifyCostCenter(ComCompany__c company,
			CostCenterResource.CostCenter param,
			ComCostCenterBase__c retrievedBase,
			ComCostCenterHistory__c retrievedHistory,
			Boolean forSearchTest) {

		System.assertEquals(param.Id, retrievedBase.id);
		System.assertEquals(param.code, retrievedBase.Code__c);
		System.assertEquals(param.companyId, retrievedBase.CompanyId__c);
		System.assertEquals(retrievedBase.Id, retrievedHistory.BaseId__c);
		if (forSearchTest) {
			// Because the normal CreateCost Center doesn't return the History ID,
			// AND the testing of HistoryID is tested in the CostCenterHistoryResourceTest
			System.assertEquals(retrievedHistory.Id, param.historyId);
			System.assertEquals(retrievedHistory.ParentBaseId__c, param.parentId);
		} else {
			System.assertEquals(param.name_L0, retrievedBase.Name);
			System.assertEquals(param.name_L0, retrievedHistory.Name);
			System.assertEquals(retrievedHistory.Id, retrievedBase.CurrentHistoryId__c);
			String uniqKey = company.Code__c + '-' + param.code +
				'-' + DateTime.newInstance(AppDate.valueOf(param.validDateFrom).getDate(),
				Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd') +
				'-' + DateTime.newInstance(AppDate.valueOf(param.validDateTo).getDate(),
				Time.newInstance(0, 0, 0, 0)).format('yyyyMMdd');
			System.assertEquals(uniqKey, retrievedHistory.UniqKey__c);
			System.assertEquals(param.parentId, retrievedHistory.ParentBaseId__c);
		}
		System.assertEquals(param.name_L0, retrievedHistory.Name_L0__c);
		System.assertEquals(param.name_L1, retrievedHistory.Name_L1__c);
		System.assertEquals(param.name_L2, retrievedHistory.Name_L2__c);
		System.assertEquals(param.linkageCode, retrievedHistory.LinkageCode__c);
		System.assertEquals(AppDate.valueOf(param.validDateFrom).getDate(), retrievedHistory.ValidFrom__c);
		System.assertEquals(AppDate.valueOf(param.validDateTo).getDate(), retrievedHistory.ValidTo__c);
		System.assertEquals(param.comment, retrievedHistory.HistoryComment__c);
	}

	private static void verifyCCResult(CostCenterBaseEntity baseEntity, CostCenterHistoryEntity historyEntity, CostCenterResource.CostCenter resultCostCenter) {
		System.assertEquals(baseEntity.code, resultCostCenter.code);
		System.assertEquals(baseEntity.id, resultCostCenter.id);
		System.assertEquals(baseEntity.companyId, resultCostCenter.companyId);
		System.assertEquals(historyEntity.id, resultCostCenter.historyId);
		System.assertEquals(historyEntity.nameL.getValue(), resultCostCenter.name);
		System.assertEquals(historyEntity.nameL.valueL0, resultCostCenter.name_L0);
		System.assertEquals(historyEntity.nameL.valueL1, resultCostCenter.name_L1);
		System.assertEquals(historyEntity.nameL.valueL2, resultCostCenter.name_L2);
		System.assertEquals(historyEntity.linkageCode, resultCostCenter.linkageCode);
		System.assertEquals(historyEntity.validFrom.format(), resultCostCenter.validDateFrom);
		System.assertEquals(historyEntity.validTo.format(), resultCostCenter.validDateTo);
		System.assertEquals(historyEntity.parentBaseId, resultCostCenter.parentId);
	}
}