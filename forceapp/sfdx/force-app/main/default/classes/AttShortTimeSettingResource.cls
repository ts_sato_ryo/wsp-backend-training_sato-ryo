/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 短時間勤務設定レコード操作APIを実装するクラス
 */
public with sharing class AttShortTimeSettingResource {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 短時間勤務パターンレコードを表すクラス
	 */
	public class Setting implements RemoteApi.RequestParam {
		/** 短時間勤務レコードベースID */
		public String id;
		/** コード */
		public String code;
		/** 会社レコードID */
		public String companyId;
		/** 理由ID */
		public String reasonId;
		/** 労働時間制 */
		public String workSystem;
		/** 短時間勤務履歴レコードID */
		public String historyId;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 履歴コメント */
		public String comment;
		/** 短時間勤務名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 短時間勤務名(言語0) */
		public String name_L0;
		/** 短時間勤務名(言語1) */
		public String name_L1;
		/** 短時間勤務名(言語2) */
		public String name_L2;
		/** 許可する短時間勤務時間[分] */
		public Integer allowableTimeOfShortWork;
		/** 許可する遅刻時間の上限[分] */
		public Integer allowableTimeOfLateArrival;
		/** 許可する早退時間の上限[分] */
		public Integer allowableTimeOfEarlyLeave;
		/** 許可する私用外出の上限[分] */
		public Integer allowableTimeOfIrregularRest;

		/**
		 * パラメータの値を設定したベースエンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public AttShortTimeSettingBaseEntity createBaseEntity( Map<String, Object> paramMap, Boolean isUpdate) {

			AttShortTimeSettingBaseEntity setting = new AttShortTimeSettingBaseEntity();

			// レコード更新時はIDをセット
			if (isUpdate) {
				setting.setId(this.id);
			}
			if (!isUpdate && paramMap.containsKey('name_L0')) {
				// TODO Name項目対応(name_L0を設定しておく)
				setting.name = this.name_L0;
			}
			if (paramMap.containsKey('code')) {
				setting.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				setting.companyId = this.companyId;
			}
			if (paramMap.containsKey('reasonId')) {
				setting.reasonId = this.reasonId;
			}
			if (paramMap.containsKey('workSystem')) {
				setting.workSystem = AttWorkSystem.valueOf(this.workSystem);
			}

			return setting;
		}

		/**
		 * パラメータの値を設定した履歴エンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public AttShortTimeSettingHistoryEntity createHistoryEntity( Map<String, Object> paramMap) {
			AttShortTimeSettingHistoryEntity setting = new AttShortTimeSettingHistoryEntity();

			if (paramMap.containsKey('name_L0')) {
				// TODO Name項目対応(name_L0を設定しておく)
				setting.name = this.name_L0;
				setting.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				setting.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				setting.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('validDateFrom') && this.validDateFrom != null) {
				setting.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (paramMap.containsKey('validDateTo')) {
				setting.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (paramMap.containsKey('comment')) {
				setting.historyComment = this.comment;
			}
			if (paramMap.containsKey('allowableTimeOfShortWork')) {
				setting.allowableTimeOfShortWork = this.allowableTimeOfShortWork;
			}
			if (paramMap.containsKey('allowableTimeOfLateArrival')) {
				setting.allowableTimeOfLateArrival = this.allowableTimeOfLateArrival;
			}
			if (paramMap.containsKey('allowableTimeOfEarlyLeave')) {
				setting.allowableTimeOfEarlyLeave = this.allowableTimeOfEarlyLeave;
			}
			if (paramMap.containsKey('allowableTimeOfIrregularRest')) {
				setting.allowableTimeOfIrregularRest = this.allowableTimeOfIrregularRest;
			}
			// フロント側の問題でフレックスの私用外出は実装されるまでは `0` で初期化する
			// https://teamspiritdev.atlassian.net/browse/GENIE-11333
			if (this.workSystem == AttWorkSystem.JP_FLEX.value) {
				setting.allowableTimeOfIrregularRest = 0;
			}

			// 有効開始日のデフォルトは実行日
			if (setting.validFrom == null) {
				setting.validFrom = AppDate.today();
			}
			// 失効日のデフォルト
			if (setting.validTo == null) {
				setting.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}

			return setting;
		}

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}
		}

		/**
		 * パラメータを検証する(更新API用)
		 */
		private void validateForUpdate() {

			validate();

			// ID
			if (String.isBlank(this.id)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('id');
				throw ex;
			}
		}
	}

	/**
	 * @description 短時間勤務設定レコード作成結果レスポンス
	 */
	public virtual class CreateResponse implements RemoteApi.ResponseParam {
		/** 作成したベースレコードID */
		public String id;
	}

	/**
	 * @description 短時間勤務設定レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING;

		/**
		 * @description 短時間勤務設定レコードを1件作成する
		 * @param req リクエストパラメータ(Setting)
		 * @return レスポンス(CreateResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			Setting param = (Setting)req.getParam(AttShortTimeSettingResource.Setting.class);

			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			final Boolean isUpdate = false;
			AttShortTimeSettingBaseEntity base = param.createBaseEntity(req.getParamMap(), isUpdate);
			base.addHistory(param.createHistoryEntity(req.getParamMap()));

			// 保存する
			AttShortTimeSettingService service = new AttShortTimeSettingService();
			service.updateUniqKey(base);
			Id resId = service.saveNewEntity(base);

			AttShortTimeSettingResource.CreateResponse res = new AttShortTimeSettingResource.CreateResponse();
			res.Id = resId;
			return res;
		}
	}

	/**
	 * @desctiprion 短時間勤務設定レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING;

		/**
		 * @description 短時間勤務設定レコードを1件更新する
		 * @param req リクエストパラメータ(Setting)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			Setting param = (Setting)req.getParam(AttShortTimeSettingResource.Setting.class);
			param.validateForUpdate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			// 更新する
			final Boolean isUpdate = true;
			AttShortTimeSettingBaseEntity base = param.createBaseEntity(req.getParamMap(), isUpdate);
			AttShortTimeSettingService service = new AttShortTimeSettingService();
			service.updateUniqKey(base);
			service.updateBase(base);

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 短時間勤務設定レコードの削除パラメータを定義するクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}

	}

	/**
	 * 短時間勤務設定レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING;

		/**
		 * @description  短時間勤務設定レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteRequest param = (DeleteRequest)req.getParam(AttShortTimeSettingResource.DeleteRequest.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				(new AttShortTimeSettingService()).deleteEntity(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合は成功レスポンスを返す
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 短時間勤務設定レコード検索条件を格納するクラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 会社レコードID */
		public String companyId;
		/** 有効期間の対象日 */
		public Date targetDate;
		/** 社員ベースID */
		public String employeeId;
	}

	/**
	 * @description 短時間勤務設定レコード検索結果を格納するクラス
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public AttShortTimeSettingResource.Setting[] records;
	}

	/**
	 * @description 短時間勤務設定レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 短時間勤務設定レコードを検索する
		 * @param requ リクエストパラメータ(SearchCondition)
		 * @return レスポンス(SearchResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SearchRequest param = (SearchRequest)req.getParam(AttShortTimeSettingResource.SearchRequest.class);

			// 対象日（デフォルト値は今日の日付）
			AppDate targetDate = param.targetDate != null ? AppDate.valueOf(param.targetDate) : AppDate.today();
			// 社員に紐づく勤務体系を取得する
			AttWorkingTypeBaseEntity workingTypeBase = getWorkingType(param.employeeId, targetDate);

			// 検索フィルタ
			AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
			if (param.id != null) {
				filter.baseIds = new Set<Id>{param.id};
			}
			if (param.companyId != null) {
				filter.companyIds = new Set<Id>{param.companyId};
			}
			if (workingTypeBase != null) {
				filter.workSystem = workingTypeBase.workSystem;
			}
			filter.targetDate = targetDate;

			List<AttShortTimeSettingBaseEntity> entityList = (new AttShortTimeSettingRepository()).searchEntity(filter);

			// レスポンスクラスに変換
			List<Setting> recordList = new List<Setting>();
			for (AttShortTimeSettingBaseEntity entity : entityList) {
				AttShortTimeSettingHistoryEntity history = entity.getHistory(0);
				Setting record = new Setting();
				record.id = entity.id;
				record.code = entity.code;
				record.companyId = entity.companyId;
				record.reasonId = entity.reasonId;
				record.workSystem = AppConverter.stringValue(entity.workSystem);
				record.historyId = history.id;
				record.comment = history.historyComment;
				record.validDateFrom = AppDate.convertDate(history.validFrom);
				record.validDateTo = AppDate.convertDate(history.validTo);
				record.name = history.nameL.getValue();
				record.name_L0 = history.nameL0;
				record.name_L1 = history.nameL1;
				record.name_L2 = history.nameL2;
				record.allowableTimeOfShortWork = history.allowableTimeOfShortWork;
				record.allowableTimeOfLateArrival = history.allowableTimeOfLateArrival;
				record.allowableTimeOfEarlyLeave = history.allowableTimeOfEarlyLeave;
				record.allowableTimeOfIrregularRest = history.allowableTimeOfIrregularRest;

				recordList.add(record);
			}

			// 成功レスポンスをセットする
			SearchResponse res = new SearchResponse();
			res.records = recordList;
			return res;
		}

		/**
		 * 社員IDと対象日から社員履歴に紐づく勤務体系を取得する
		 * 社員IDと対象日が存在しない場合は、nullを返却する
		 * @param employeeId
		 * @param targetDate
		 * @return 勤務体系ベース
		 * @throw App.RecordNotFoundException 社員または勤務体系が見つからない場合、有効期間外の場合
		 */
		private AttWorkingTypeBaseEntity getWorkingType(Id employeeId, AppDate targetDate) {
			if (employeeId == null || targetDate == null) {
				return null;
			}
			EmployeeBaseEntity employeeBase = new EmployeeService().getEmployee(employeeId, targetDate);
			EmployeeHistoryEntity employeeHis = employeeBase.getHistoryByDate(targetDate);
			return new AttWorkingTypeService().getWorkingType(employeeHis.workingTypeId);
		}
	}
}
