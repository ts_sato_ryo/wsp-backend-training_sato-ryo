/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * Job RequiredFor picklist
 */
public with sharing class ComJobRequiredFor extends ValueObjectType {
    
    /** Expense Report Only */
	public static final ComJobRequiredFor EXPENSE_REPORT = new ComJobRequiredFor('ExpenseReport');
	/** Expense Report and Request */
	public static final ComJobRequiredFor EXPENSE_REQUEST_AND_EXPENSE_REPORT = new ComJobRequiredFor('ExpenseRequestAndExpenseReport');

	/** Entries */
	public static final List<ComJobRequiredFor> TYPE_LIST;
	static {
		TYPE_LIST = new List<ComJobRequiredFor> {
			EXPENSE_REPORT,
			EXPENSE_REQUEST_AND_EXPENSE_REPORT
		};
	}

	/**
	 * Constructor
	 * @param value
	 */
	private ComJobRequiredFor(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
	 * @param The key value to get the instance
	 * @return The instance having the value specified
	 */
	public static ComJobRequiredFor valueOf(String value) {
		ComJobRequiredFor retType = null;
		if (String.isNotBlank(value)) {
			for (ComJobRequiredFor type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {

		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ComJobRequiredFor) {
			// 値が同じであればtrue
			if (this.value == ((ComJobRequiredFor)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}
}
