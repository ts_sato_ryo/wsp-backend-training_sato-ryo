/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 経費申請のサービス Service class for Expense Report Request
 */
 public with sharing class ExpReportRequestService{

	private static final String ACTION_TYPE_REMOVED = 'Removed';

	/** 申請番号プレフィックス */
	private static String REQUEST_NO_PREFIX = 'exp';

	/** Report Type Validation Bypassing */
	@TestVisible
	private Boolean isTestModeAndBypassExpReportValidation = false;

	/**
	 * 経費承認申請可否のチェックを行う Checks the validity of Expense Report for Approval
	 * @param reportId 経費精算ID ExpReport ID
	 * @return 確認メッセージのリスト Confirmation Message List
	 */
	public List<String> checkExpReportRequest(Id reportId) {
		// 申請可能であることを確認する
		ExpReportEntity report = getExpReport(reportId);
		validateExpReport(report);

		// 確認事項がある場合、メッセージを返す
		List<String> confirmationList = new List<String>();

		return confirmationList;
	}

	/**
	 * 承認申請を実行する Submits Expense Report for Approval
	 * @param reportId 経費精算ID ExpReport ID
	 * @param comment 申請コメント Expense Report Request Comment
	 */
	public void submitRequest(Id reportId, String comment) {
		// 申請可能であることを確認する
		ExpReportEntity report = getExpReport(reportId);

		// 経費精算に紐づく社員を取得する
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity employee = empService.getEmployee(report.employeeBaseId, report.accountingDate);

		if (!isTestModeAndBypassExpReportValidation) {
			validateExpReport(report, employee.companyId);
		}

		// 社員の会社のデフォルト言語取得
		AppLanguage companyLang = getCompanyLanguage(employee.companyId);
		if (companyLang == null) {
			// メッセージ：会社のデフォルト言語が設定されていません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_SET_COMPANY_LANG,
					ComMessage.msg().Exp_Err_FieldNotSet(new List<String>{
					ComMessage.msg().Com_Lbl_Company,
					ComMessage.msg().Com_Lbl_DefaultLanguage}));
		}

		// 新規申請の場合は申請番号を採番する
		ExpReportRepository reportRepo = new ExpReportRepository();
		if (report.expRequestId == null) {
			report.resetChanged();
			report.prefix = REQUEST_NO_PREFIX;
			report.sequenceNo = Integer.valueOf(reportRepo.getNewSequenceNo());
			report.reportNo = report.prefix + String.valueOf(report.sequenceNo).leftPad(8, '0');
			// 経費精算名を設定
			report.name = ExpReportEntity.createName(employee.code, employee.fullNameL.getFullName(), report.reportNo);
		}

		// 経費申請エンティティを作成する
		ExpReportRequestEntity request = createRequest(report, comment, employee, companyLang);

		// 申請を保存する
		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();
		Repository.SaveResult requestSaveRes = requestRepo.saveEntity(request);
		request.setId(requestSaveRes.details[0].id);

		// 経費精算データを申請オブジェクトに添付する
		// ファイル名：申請名
		// 言語：会社のデフォルト言語
		ExpService.ExpReportParam attachBody = new ExpService.ExpReportParam(report, companyLang);
		if (report.expPreRequestId != null) {
			ExpRequestEntity expRequestEntity = ExpRequestService.getExpRequest(report.expPreRequestId);
			ExpService.ExpRequest expRequest = new ExpService.ExpRequest(expRequestEntity, companyLang);
			attachBody.expPreRequest = expRequest;
		}

		AppAttachmentRepository attachRepo = new AppAttachmentRepository();
		List<AppAttachmentEntity> attaches = attachRepo.getEntityListByParentId(request.id, request.name);
		AppAttachmentEntity attach;
		if ((attaches == null) || (attaches.isEmpty())) {
			attach = new AppAttachmentEntity();
			attach.name = request.name;
			attach.parentId = request.id;
		} else {
			attach = attaches[0];

			// 同じファイル名で複数存在している場合は、ログを出力しておく
			if (attaches.size() > 1) {
				System.debug(System.LoggingLevel.WARN,
					'経費申請レコードに同名の添付ファイルが複数存在します。(ParentId=' + request.id + 'ファイル名=' + request.name + ')');
			}
		}
		attach.bodyText = JSON.serialize(attachBody);
		attachRepo.saveEntity(attach);

		// 新規申請の場合は経費精算を更新する
		if (report.expRequestId == null) {
			report.expRequestId = request.id;
			reportRepo.saveEntity(report);
		}

		// 明細に紐づいている領収書を申請レコードに共有する
		createLinkToRequest(attachBody, request.id);

		// 経費申請を承認申請する
		submitProcess(request.id, comment);
	}

	/**
	 * @description 取消処理を行う
	 * @param requestId 申請ID
	 * @param comment コメント
	 */
	public void cancelRequest(Id requestId, String comment) {
		// 申請データを取得
		ExpReportRequestRepository repo = new ExpReportRequestRepository();
		ExpReportRequestEntity request = repo.getEntity(requestId);
		if (request == null) {
			// メッセージ：該当する申請が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_REQUEST,
					ComMessage.msg().Exp_Err_NotFoundRequest);
		}

		if (AppRequestStatus.PENDING.equals(request.status)) {
			// 申請取消
			cancelRequestProcess(request, comment);
		} else {
			// メッセージ：該当の申請は取り消せません。
			throw new App.IllegalStateException(
					App.ERR_CODE_CANNOT_CANCEL_REQUEST,
					ComMessage.msg().Exp_Err_CannotCancelRequest);
		}
	}

	/**
	 * @description 経費申請一覧データを取得する
	 * @param  requestIdList 経費申請IDのリスト
	 * @return 経費申請エンティティのリスト
	 */
	public List<ExpReportRequestEntity> getRequestList(final List<Id> requestIdList) {
		ExpReportRequestRepository repo = new ExpReportRequestRepository();

		// 経費申請データを取得
		List<ExpReportRequestEntity> requestList = repo.getEntityList(requestIdList);

		return requestList;
	}

	 /**
	  * 会社のデフォルト言語を取得
	  * @param companyId 取得対象の会社ID
	  * @return 言語、設定されていない場合はnull
	  */
	public static AppLanguage getCompanyLanguage(Id companyId) {
		// 社員の会社のデフォルト言語取得
		CompanyRepository repo = new CompanyRepository();
		CompanyEntity company = repo.getEntity(companyId);
		return company.language;
	}

	/**
	 * 経費申請エンティティを作成する
	 * @param report 申請対象の経費精算エンティティ
	 * @param comment 申請コメント
	 * @param employee 経費精算に紐づく社員のベースエンティティ
	 * @param lang 申請名などの言語
	 * @return 経費申請エンティティ
	 */
	private ExpReportRequestEntity createRequest(ExpReportEntity report, String comment,
			EmployeeBaseEntity employee, AppLanguage lang) {

		// 申請者(実行ユーザ)の社員を取得する
		EmployeeService empService = new EmployeeService();
		EmployeeBaseEntity actor = empService.getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());

		// 申請名
		String requestName = new ExpService().createRequestName(report.reportNo, report.totalAmount, report.employeeNameL, report.subject, lang);

		// 経費申請エンティティを作成
		ExpReportRequestEntity request = new ExpReportRequestEntity();
		if (report.expRequestId != null) {
			// 既存の申請レコードが存在する場合は、IDを設定する
			request.setId(report.expRequestId);
		}
		request.name = requestName;
		request.expReportId = report.id;
		request.totalAmount = report.totalAmount;
		request.reportNo = report.reportNo;
		request.subject = report.subject;
		request.comment = comment;
		request.status = AppRequestStatus.PENDING;
		request.accountingStatus = ExpAccountingStatus.NONE;
		request.cancelType = null;
		request.isConfirmationRequired = false;
		request.requestTime = AppDateTime.now();
		request.employeeHistoryId = report.employeeHistoryId;
		request.departmentHistoryId = report.departmentHistoryId;
		request.actorHistoryId = actor.getHistory(0).id;

		// TODO: 承認者は暫定対応。経費精算の社員の上長で固定
		EmployeeHistoryEntity empHistory = employee.getHistory(0);
		if (empHistory.managerId == null) {
			// メッセージ：社員の上長が設定されていません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_SET_EMP_MANAGER,
					ComMessage.msg().Exp_Err_FieldNotSet(new List<String>{
						ComMessage.msg().Com_Lbl_Employee,
						ComMessage.msg().Com_Lbl_Manager}));
		}
		EmployeeBaseEntity manager = empService.getEmployee(empHistory.managerId, report.accountingDate);
		if (manager == null) {
			// メッセージ：社員の上長が見つかりませんでした。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_MANAGER,
					ComMessage.msg().Exp_Err_NotFoundAofB(new List<String>{
						ComMessage.msg().Com_Lbl_Employee,
						ComMessage.msg().Com_Lbl_Manager}));
		}
		if (manager.userId == null) {
			// メッセージ：上長のユーザが設定されていません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_SET_EMP_MANAGER,
					ComMessage.msg().Exp_Err_FieldNotSet(new List<String>{
					ComMessage.msg().Com_Lbl_Manager,
					ComMessage.msg().Att_Lbl_User}));
		}
		request.approver01Id =  manager.userId;

		return request;
	}

	/**
	 * 明細に紐づいている領収書を申請レコードに共有する
	 * Create Link for report and record file/receipt attachment
	 * @param reportParam ExpReportParam list that contains report and records info
	 * @param requestId 経費申請ID
	 */
	private void createLinkToRequest(ExpService.ExpReportParam reportParam, Id requestId) {
		// 明細に紐づいている領収書のうち、申請に紐づいている領収書のIDを取得する
		List<Id> entityIdList = new List<Id>();
		for (ExpService.ExpRecordParam record : reportParam.records) {
			if (String.isNotBlank(record.receiptId)) {
				entityIdList.add(record.recordId);
			}
		}

		// File attachment to the Id list to find the relevant ContentDocument
		if (reportParam.attachedFileId != null) {
			entityIdList.add(reportParam.reportId);
		}

		Set<Id> linkedReceiptIdSet = new Set<Id>();
		if (!entityIdList.isEmpty()) {
			for (AppContentDocumentEntity content : new FileService().getContentDocumentEntityList(entityIdList)) {
				for (AppContentDocumentLinkEntity link : content.contentDocumentLinkList) {
					if (link.linkedEntityId == requestId) {
						linkedReceiptIdSet.add(link.contentDocumentId);
					}
				}
			}
		}

		// 申請へのリンクがまだない場合は作成
		List<AppContentDocumentLinkEntity> linkList = new List<AppContentDocumentLinkEntity>();
		for (ExpService.ExpRecordParam record : reportParam.records) {
			if (String.isNotBlank(record.receiptId) && !linkedReceiptIdSet.contains(record.receiptId)) {
				AppContentDocumentLinkEntity link = new AppContentDocumentLinkEntity();
				link.contentDocumentId = record.receiptId;
				link.linkedEntityId = requestId;
				linkList.add(link);
			}
		}

		// Create File attachment link if haven't created
		if (reportParam.attachedFileId != null && !linkedReceiptIdSet.contains(reportParam.attachedFileId)) {
			AppContentDocumentLinkEntity link = new AppContentDocumentLinkEntity();
			link.contentDocumentId = reportParam.attachedFileId;
			link.linkedEntityId = requestId;
			linkList.add(link);
		}
		if (!linkList.isEmpty()) {
			new AppContentDocumentRepository().saveLinkList(linkList);
		}
	}

	/**
	 * 承認申請を行う
	 * TODO 将来的には承認申請処理はアプリ全体で共通化される予定。それまでの暫定対応。
	 * @param requestId 申請対象レコードのID
	 * @param comment コメント
	 */
	private void submitProcess(Id requestId, String comment) {
		Approval.ProcessSubmitRequest processReq = new Approval.ProcessSubmitRequest();
		processReq.setObjectId(requestId);
		processReq.setComments(comment);

		Approval.process(processReq);
	}

	/**
	 * @description 申請取消を行う
	 * @param request 申請エンティティ
	 * @param comment コメント
	 */
	private void cancelRequestProcess(ExpReportRequestEntity request, String comment) {
		if (!test.isRunningTest()) {
			// 承認/却下対象の承認申請データを取得
			ApprovalProcessRepository appRepo = new ApprovalProcessRepository();
			List<Id> workItemIdList = appRepo.getWorkItemList(new List<Id>{request.id});
			if (workItemIdList == null || workItemIdList.isEmpty()) {
				// メッセージ：経費精算の申請は取り消せません。
				throw new App.IllegalStateException(
						App.ERR_CODE_CANNOT_CANCEL_REQUEST,
						ComMessage.msg().Exp_Err_CannotCancelRequest);
			}

			// 取消実行
			Approval.ProcessWorkitemRequest processReq = new Approval.ProcessWorkitemRequest();
			processReq.setWorkItemId(workItemIdList[0]);
			processReq.setAction(ACTION_TYPE_REMOVED);
			processReq.setComments(comment);

			try {
				Approval.process(processReq);
			} catch(DmlException e) {
				if(e.getMessage().indexOf('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY') >= 0) {
					// メッセージ：システム管理者あるいは申請者以外は申請中の取消ができません。
					throw new App.IllegalStateException(
							App.ERR_CODE_NO_PERMISSIONS_CANCEL_REQUEST,
							ComMessage.msg().Exp_Err_NoPermissionsRemoveRequest);
				} else {
					throw e;
				}
			}
		}
	}

	/**
	 * Retrieves Expense Report Entity via the Expense Report ID
	 * @param reportId 経費精算ID ExpReport ID
	 * @return ExpReportEntity
	 */
	private ExpReportEntity getExpReport(Id reportId) {
		// 経費精算データを取得
		ExpReportEntity report = new ExpService().getExpReport(reportId, ExpService.CallType.REPORT);
		if (report == null) {
			// メッッセージ：該当する申請が見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_EXP_REPORT,
					ComMessage.msg().Exp_Err_NotFoundRequest);
		}
		return report;
	}

	 /*
	  * Verify that the Expense Report data are valid for Approval Submission
	  * @param report Expense Report to Validate
	  */
	 private void validateExpReport(ExpReportEntity report) {
		 EmployeeService empService = new EmployeeService();
		 EmployeeBaseEntity employee = empService.getEmployee(report.employeeBaseId, report.accountingDate);
		 validateExpReport(report, employee.companyId);
	 }

	 /**
	  * 経費承認申請が可能であることを確認する
	  * @param report 経費精算エンティティ
	  * @param companyId The ID of the company
	  */
	private void validateExpReport(ExpReportEntity report, Id companyId) {
		ExpValidator.RequestSubmitValidator requestValidator = new ExpValidator.RequestSubmitValidator(report, report.recordList, companyId);
		Validator.Result requestValidatorRes = requestValidator.validate();
		if (!requestValidatorRes.isSuccess()) {
			List<String> codes = new List<String>();
			List<String> messages = new List<String>();

			List<Validator.Error> errors = requestValidatorRes.getErrors();
			for (Validator.Error error : errors) {
				codes.add(error.code);
				messages.add(error.message);
			}

			throw new App.IllegalStateException(String.join(codes, ','), String.join(messages, '\n'));
		}
	}
 }