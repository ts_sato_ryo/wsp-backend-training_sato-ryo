/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 国マスタのリポジトリ
 */
public with sharing class CountryRepository extends Repository.BaseRepository {

	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_COUNTRY_FIELD_SET = new Set<Schema.SObjectField> {
		ComCountry__c.Id,
		ComCountry__c.Name,
		ComCountry__c.Name_L0__C,
		ComCountry__c.Name_L1__C,
		ComCountry__c.Name_L2__C,
		ComCountry__c.Code__C
	};

	/** 検索フィルタ */
	public class SearchFilter {
		public Set<Id> ids = new Set<Id>();
		public Set<String> codes = new Set<String>();
 	}

	/**
	 * IDを条件に国マスタを検索する。
	 * @param id 検索条件のID
	 * @return 検索結果 該当するマスタが存在しない場合はnull
	 */
	public CountryEntity searchById(Id id) {
		SearchFilter filter = new SearchFilter();
		filter.ids.add(id);
		List<CountryEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * コードを条件に国マスタを検索する。
	 * @param id 検索条件のコード
	 * @return 検索結果 該当するマスタが存在しない場合はnull
	 */
	public CountryEntity searchByCode(String code) {
		SearchFilter filter = new SearchFilter();
		filter.codes.add(code);
		List<CountryEntity> entities = searchEntityList(filter);
		return entities.isEmpty() ? null : entities[0];
	}

	/**
	 * 検索フィルタの条件に一致する国マスタを取得する。
	 * 当メソッドではレコードをロックしない。(forUpdateではない)
	 * @param filter 検索フィルタ
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
  public List<CountryEntity> searchEntityList(SearchFilter filter) {
		return searchEntityList(filter, false);
	}

	/**
	 * 検索フィルタの条件に一致する国マスタを取得する。
	 * @param filter 検索フィルタ
	 * @param forUpdate 更新用で取得する場合はtrue(ロックする)
	 * @return 検索結果 該当するマスタが存在しない場合は空のリスト
	 */
	public List<CountryEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_COUNTRY_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// WHERE句を生成
		final Set<Id> ids = filter.ids;
		final Set<String> codes = filter.codes;
		List<String> conditions = new List<String>();
		if (!ids.isEmpty()) {
			conditions.add(getFieldName(ComCountry__c.Id) + ' IN :ids');
		}
		if (!codes.isEmpty()) {
			conditions.add(getFieldName(ComCountry__c.Code__c) + ' IN :codes');
		}

		// SOQLを生成
		String soql = 'SELECT ' + String.join(selectFldList, ', ')
					+ ' FROM ' + ComCountry__c.SObjectType.getDescribe().getName()
					+ buildWhereString(conditions)
					+ ' ORDER BY Code__c ';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}

		// クエリ実行
		List<ComCountry__c> sObjList = Database.query(soql);
		List<CountryEntity> entityList = new List<CountryEntity>();
		for (ComCountry__c sobj : sObjList) {
			entityList.add(createEntity(sobj));
		}
		return entityList;
	}

	/**
	 * ComCountry__cからCountryEntityを作成する
	 * @param entity 作成元の国オブジェクト（nullは許容しない）
	 * @return 作成した国エンティティ
	 */
	@testVisible
	private CountryEntity createEntity(ComCountry__c sobj) {
		CountryEntity entity = new CountryEntity();
		entity.setId(sobj.Id);
		entity.name = sobj.Name;
		entity.nameL0 = sobj.Name_L0__c;
		entity.nameL1 = sobj.Name_L1__c;
		entity.nameL2 = sobj.Name_L2__c;
		entity.code = sobj.Code__c;
		return entity;
	}

	/**
	 * 国エンティティを国マスタに登録、または更新する。
	 * @param entity 登録対象の国エンティティ(nullは許容しない)
	 * @return レコード保存結果
	 */
	 public SaveResult saveEntity(CountryEntity entity) {
		return saveEntityList(new List<CountryEntity>{ entity });
	 }

	/**
	 * 国エンティティのリストを国マスタに登録、または更新する。
	 * @param entityList 登録対象の国エンティティのリスト(nullは許容しない)
	 * @return レコード保存結果
	 */
	 public SaveResult saveEntityList(List<CountryEntity> entityList) {

		// エンティティをsObjectに変換する
		List<ComCountry__c> sObjList = new List<ComCountry__c>();
		for (CountryEntity entity : entityList) {
			sObjList.add(createSObject(entity));
		}

		// DBに保存する
		List<Database.UpsertResult> saveResultList = AppDatabase.doUpsert(sObjList);

		// 結果作成
		return resultFactory.createSaveResult(saveResultList);
	 }

	/**
	 * CountryEntityからComCountry__cを作成する
	 * @param entity 作成元の国エンティティ(nullは許容しない)
	 * @return 作成した国オブジェクト
	 */
	@testVisible
	private ComCountry__c createSObject(CountryEntity entity) {

	  ComCountry__c sobj = new ComCountry__c(Id = entity.Id);
		if (entity.isChanged(CountryEntity.Field.NAME)) {
			sobj.Name = entity.name;
		}

		if (entity.isChanged(CountryEntity.Field.NAME_L0)) {
			sobj.NAME_L0__C = entity.nameL0;
		}

		if (entity.isChanged(CountryEntity.Field.NAME_L1)) {
			sobj.NAME_L1__C = entity.nameL1;
		}

		if (entity.isChanged(CountryEntity.Field.NAME_L2)) {
			sobj.NAME_L2__c = entity.nameL2;
		}

		if (entity.isChanged(CountryEntity.Field.CODE)) {
			sobj.CODE__c = entity.code;
		}
		return sobj;
	}
}