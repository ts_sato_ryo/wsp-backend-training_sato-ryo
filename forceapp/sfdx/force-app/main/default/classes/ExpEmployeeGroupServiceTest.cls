/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Expense Employee Group Service Test
 */
@IsTest
private class ExpEmployeeGroupServiceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();
	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);

	/*
	 * Test Data Class
	 */
	private class TestData  {
		/** Expense Employee Group Data */
		public List<ExpEmployeeGroupEntity> expEmployeeGroupList;

		/** Constructor */
		public TestData() {
			expEmployeeGroupList = createExpEmployeeGroupList('Testcode', companyObj.Id, 3);
		}

		/**
		 * Create and Insert Employee Group List
		 * @param code Test Data Prefix Code
		 * @param companyId Company ID
		 * @param size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpEmployeeGroupEntity> createExpEmployeeGroupList(String code, Id companyId, Integer size) {
			expEmployeeGroupList = new List<ExpEmployeeGroupEntity>();
			ExpEmployeeGroupRepository repo = new ExpEmployeeGroupRepository();
			ExpEmpGroupExpReportTypeLinkRepository linkRepo = new ExpEmpGroupExpReportTypeLinkRepository();

			List<ExpEmployeeGroup__c> expEmpGroupList =
				ComTestDataUtility.createExpEmployeeGroupList(code, companyId, size);
			List<Id> expEmpGroupIdList = new List<Id>();
			for (ExpEmployeeGroup__c expEmpGroup : expEmpGroupList){
				expEmpGroupIdList.add(expEmpGroup.Id);
			}
			List<Id> reportTypeIdList = createReportTypeIdList('test');
			List<ExpEmpGroupExpReportTypeLink__c> ExpEmpGroupExpReportTypeLinkList =
					ComTestDataUtility.createExpEmployeeGroupReportTypeLinkList(expEmpGroupIdList, reportTypeIdList);

			List<ExpEmpGroupExpReportTypeLinkEntity> entityList = new List<ExpEmpGroupExpReportTypeLinkEntity>();
			for (ExpEmpGroupExpReportTypeLink__c expEmpGroupSObj : ExpEmpGroupExpReportTypeLinkList) {
				entityList.add(linkRepo.createEntity(expEmpGroupSObj));
			}
			Map<Id, List<Id>> linkMap = linkRepo.createMap(entityList);

			for (ExpEmployeeGroup__c expEmpGroupSObj : expEmpGroupList) {
				ExpEmployeeGroupEntity entity = repo.createEntity(expEmpGroupSObj);
				entity.reportTypeIdList = linkMap.get(entity.id);
				expEmployeeGroupList.add(entity);
			}
			return expEmployeeGroupList;
		}
	}

	/**
	 * Create and Insert report type list
	 * @param name Name of report type
	 * @return List of report type ids created
	 */
	static List<Id> createReportTypeIdList(String name) {
		List<ExpReportType__c> expReportTypeList =
				ComTestDataUtility.createExpReportTypes(name, companyObj.Id, 3);
		List<Id> expReportTypeIdList = new List<Id>();
		for (ExpReportType__c expReportType : expReportTypeList){
			expReportTypeIdList.add(expReportType.Id);
		}
		return expReportTypeIdList;
	}

	/**
	 * Create Expense Employee Group Positive Test (without report type link)
	 */
	@isTest
	static void createExpEmployeeGroup() {
		ExpEmployeeGroupEntity entity = setTestEntity(null);

		Test.startTest();

		Id result = ExpEmployeeGroupService.createExpenseEmployeeGroup(entity, null);

		Test.stopTest();

		List<ExpEmployeeGroup__c> records = getExpenseEmployeeGroupSObjectFROMSOQL(result);

		//Confirm new record was created
		System.assertEquals(1, records.size());
		ExpEmployeeGroupServiceTest.assertFields(entity, records[0]);
	}

	/**
	* Create Expense Employee Group Positive Test (with report type link)
	*/
	@isTest
	static void createExpEmployeeGroupWithReportTypeIdList() {

		List<Id> expReportTypeIdList = createReportTypeIdList('test');

		ExpEmployeeGroupEntity entity = setTestEntity(expReportTypeIdList);

		Test.startTest();

		Id resultEmpGroupId = ExpEmployeeGroupService.createExpenseEmployeeGroup(entity, expReportTypeIdList);

		Test.stopTest();

		List<ExpEmployeeGroup__c> empGroupObjList = getExpenseEmployeeGroupSObjectFROMSOQL(resultEmpGroupId);
		List<ExpEmpGroupExpReportTypeLink__c> linkObjList = getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(empGroupObjList[0].Id);

		//Confirm new record was created
		System.assertEquals(1, empGroupObjList.size());
		ExpEmployeeGroupServiceTest.assertFieldsWithReportTypeIdList(entity,empGroupObjList[0], linkObjList);
	}


	/**
	 * Create Duplicate Expense Employee Group using identical entity
	 */
	@isTest
	static void creatDuplicateExpenseEmployeeGroupTest() {

		List<Id> expReportTypeIdList = createReportTypeIdList('test');

		ExpEmployeeGroupEntity entity = setTestEntity(null);

		Test.startTest();

		ExpEmployeeGroupService.createExpenseEmployeeGroup(entity, expReportTypeIdList);
		try{
			ExpEmployeeGroupService.createExpenseEmployeeGroup(entity, expReportTypeIdList);
		} catch (Exception e) {
			System.assertEquals(MESSAGE.Com_Err_DuplicateCode, e.getMessage());
		}

		Test.stopTest();

	}

	/**
	 * Search Active Expense Employee Group: Create 3 test data find one of it
	 */
	@isTest
	static void searchExpenseEmployeeGroupTest() {

		TestData testData = new TestData();

		Test.startTest();

		List<ExpEmployeeGroupEntity> responseEntity = ExpEmployeeGroupService.searchExpenseEmployeeGroupList(companyObj.Id, testData.expEmployeeGroupList[0].id, true, true);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, responseEntity);
		System.assertEquals(1, responseEntity.size());

		ExpEmployeeGroupServiceTest.assertFieldsForSearch(testData.expEmployeeGroupList[0], responseEntity[0]);
	}

	/**
	 * Search Expense Employee Group without report type
	 */
	@isTest
	static void searchExpenseEmployeeGroupWithoutReportTypeTest() {

		TestData testData = new TestData();

		Test.startTest();

		List<ExpEmployeeGroupEntity> responseEntity = ExpEmployeeGroupService.searchExpenseEmployeeGroupList(companyObj.Id, testData.expEmployeeGroupList[0].id, false, true);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, responseEntity);
		System.assertEquals(1, responseEntity.size());
		System.assertEquals(null, responseEntity[0].reportTypeIdList);
	}

	/**
	 * Search Active Expense Employee Group no record found: Create 3 records delete one data and search the data
	 */
	@isTest
	static void searchExpenseEmployeeGroupNotFoundTest() {

		TestData testData = new TestData();

		delete getExpenseEmployeeGroupSObjectFROMSOQL(testData.expEmployeeGroupList[1].id);
		delete getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(testData.expEmployeeGroupList[1].id);

		Test.startTest();

		List<ExpEmployeeGroupEntity> responseEntity = ExpEmployeeGroupService.searchExpenseEmployeeGroupList(companyObj.Id, testData.expEmployeeGroupList[1].id, true, true);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, responseEntity);
		System.assertEquals(0, responseEntity.size());
	}

	/**
	 * Search Expense Employee Group with id param null to retrieve all the available ids
	 */
	@isTest
	static void searchExpenseEmployeeGroupParamMapRetrieveAllTest() {
		TestData testData = new TestData();

		Test.startTest();

		List<ExpEmployeeGroupEntity> responseEntity = ExpEmployeeGroupService.searchActiveExpenseEmployeeGroupList(companyObj.Id, '', true);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, responseEntity);
		System.assertEquals(3, responseEntity.size());

		for (Integer i = 0; i < responseEntity.size(); i++) {
			ExpEmployeeGroupServiceTest.assertFieldsForSearch(testData.expEmployeeGroupList[i], responseEntity[i]);
		}

	}

	/**
	 * Search Expense Employee Group with Active param null to retrieve all Group
	 */
	@isTest
	static void searchExpenseEmployeeGroupActiveParamNull() {
		List<ExpEmployeeGroup__c> testRecord = ComTestDataUtility.createExpEmployeeGroupList('Test', companyObj.Id, 3);
		testRecord[0].Active__c = false;
		update testRecord[0];

		Test.startTest();

		List<ExpEmployeeGroupEntity> responseActiveEntity = ExpEmployeeGroupService.searchActiveExpenseEmployeeGroupList(companyObj.Id, '', false);
		List<ExpEmployeeGroupEntity> responseAllEntity = ExpEmployeeGroupService.searchExpenseEmployeeGroupList(companyObj.Id, '', false,null);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, responseActiveEntity);
		System.assertEquals(2, responseActiveEntity.size());
		System.assertNotEquals(null, responseAllEntity);
		System.assertEquals(3, responseAllEntity.size());
	}

	/**
	 * Search Expense Employee Group with different Company
	 */
	@isTest
	static void searchExpenseEmployeeGroupInDiffCompany() {
		ComCompany__c XYZCompanyObj = ComTestDataUtility.createCompany('XYZCom', countryObj.Id);
		List<ExpEmployeeGroup__c> testRecord = ComTestDataUtility.createExpEmployeeGroupList('Test', companyObj.Id, 3);
		List<ExpEmployeeGroup__c> XYZTestRecord = ComTestDataUtility.createExpEmployeeGroupList('XYZTest', XYZCompanyObj.Id, 5);
		ExpEmployeeGroupRepository repo = new ExpEmployeeGroupRepository();

		Test.startTest();

		List<ExpEmployeeGroupEntity> responseEntity = ExpEmployeeGroupService.searchActiveExpenseEmployeeGroupList(companyObj.Id, '', true);
		List<ExpEmployeeGroupEntity> XYZResponseEntity = ExpEmployeeGroupService.searchActiveExpenseEmployeeGroupList(XYZCompanyObj.Id, '', true);

		Test.stopTest();

		// Confirm record is fetched
		System.assertNotEquals(null, responseEntity);
		System.assertEquals(3, responseEntity.size());

		for (Integer i = 0; i < responseEntity.size(); i++) {
			ExpEmployeeGroupServiceTest.assertFieldsForSearch(repo.createEntity(testRecord[i]), responseEntity[i]);
		}

		// Confirm record is fetched from XYZ
		System.assertNotEquals(null, XYZresponseEntity);
		System.assertEquals(5, XYZresponseEntity.size());

		for (Integer i = 0; i < XYZresponseEntity.size(); i++) {
			ExpEmployeeGroupServiceTest.assertFieldsForSearch(repo.createEntity(XYZtestRecord[i]), XYZResponseEntity[i]);
		}
	}

	/**
	 * Update Expense Employee Group Positive Test
	 */
	@isTest
	static void updateExpenseEmployeeGroupTest() {
		TestData testData = new TestData(); // to be updated value

		List<Id> reportTypeIdList = createReportTypeIdList('test2'); // updated to this value
		ExpEmployeeGroupEntity entity = setTestEntity(reportTypeIdList); // updated to this value
		entity.setId(testData.expEmployeeGroupList[0].id);

		Test.startTest();

		Id updatedExpEmployeeGroupId = ExpEmployeeGroupService.updateExpenseEmployeeGroup(entity, entity.reportTypeIdList);

		Test.stopTest();

		List<ExpEmployeeGroup__c> empGroupObjList = getExpenseEmployeeGroupSObjectFROMSOQL(testData.expEmployeeGroupList[0].id);

		System.assertEquals(testData.expEmployeeGroupList[0].id, updatedExpEmployeeGroupId);
		//Confirm updated record was created
		System.assertEquals(1, empGroupObjList.size());
		ExpEmployeeGroupServiceTest.assertFields(entity, empGroupObjList[0]);

		List<ExpEmpGroupExpReportTypeLink__c> linkObjList = getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(updatedExpEmployeeGroupId);

		ExpEmployeeGroupServiceTest.assertFieldsWithReportTypeIdList(entity,empGroupObjList[0], linkObjList);
	}


	/**
	 * Update Expense Employee Group Record Not found to update Negative Test
	 */
	@isTest
	static void updateExpenseEmployeeGroupNotFoundTest() {
		TestData testData = new TestData();
		List<Id> reportTypeIdList = createReportTypeIdList('test2'); // updated to this value
		ExpEmployeeGroupEntity entity = setTestEntity(reportTypeIdList); // updated to this value
		entity.setId(testData.expEmployeeGroupList[0].id);
		delete getExpenseEmployeeGroupSObjectFROMSOQL(testData.expEmployeeGroupList[0].id);
		delete getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(testData.expEmployeeGroupList[1].id);

		Test.startTest();

		try {
			ExpEmployeeGroupService.updateExpenseEmployeeGroup(entity, entity.reportTypeIdList);
			TestUtil.fail('No exception thrown for invalid update.');
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_RecordNotFound, e.getMessage());
		}

		Test.stopTest();
	}

	/**
	 * Update Expense Employee Group Record Updating value duplicate with existing value Negative Test
	 */
	@isTest
	static void updateExpenseEmployeeGroupDuplicateErrorTest() {
		TestData testData = new TestData();
		List<Id> reportTypeIdList = createReportTypeIdList('test2');
		ExpEmployeeGroupEntity entity = setTestEntity(reportTypeIdList);
		entity.setId(testData.expEmployeeGroupList[1].id);
		entity.companyId = testData.expEmployeeGroupList[2].companyId;
		entity.code = testData.expEmployeeGroupList[2].code;

		Test.startTest();

		try {
			ExpEmployeeGroupService.updateExpenseEmployeeGroup(entity, entity.reportTypeIdList);
			TestUtil.fail('No exception thrown for invalid update.');
		} catch (App.ParameterException e) {
			System.assertEquals(ComMessage.msg().Com_Err_DuplicateCode, e.getMessage());
		}

		Test.stopTest();
	}


	/**
	 * Delete Expense Employee Group Positive Test
	 */
	@isTest
	static void deleteExpEmployeeGroupTest() {
		TestData testData = new TestData();

		Test.startTest();

		ExpEmployeeGroupService.deleteExpEmployeeGroup(testData.expEmployeeGroupList[0].Id);

		Test.stopTest();

		List<ExpEmployeeGroup__c> records = getExpenseEmployeeGroupSObjectFROMSOQL(testData.expEmployeeGroupList[0].Id);
		List<ExpEmpGroupExpReportTypeLink__c> linkObjList = getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(testData.expEmployeeGroupList[0].Id);

		// Confirm record is deleted
		System.assertNotEquals(null, records);
		System.assertEquals(0, records.size());

		System.assertNotEquals(null, linkObjList);
		System.assertEquals(0, linkObjList.size());
	}

	/**
	 * Delete Expense Employee Group Negative test
	 * error is shown if no employee group record exist
	 */
	@isTest
	static void deleteExpEmployeeGroupNoEmpGroupRecordExistTest() {
		TestData testData = new TestData();

		delete getExpenseEmployeeGroupSObjectFROMSOQL(testData.expEmployeeGroupList[0].id);

		Test.startTest();

		try {
			ExpEmployeeGroupService.deleteExpEmployeeGroup(testData.expEmployeeGroupList[0].id);
			TestUtil.fail('No exception thrown for invalid delete.');
		} catch (DmlException e) {
			System.assertEquals(System.StatusCode.ENTITY_IS_DELETED, e.getDmlType(0));
		}
		Test.stopTest();
	}

	/**
	 * Assert expected Entity field with test Entity field
	 * @param originalEntity Original Expense Employee Group Entity
	 * @param entity Expense Employee Group Entity to be compared
	 */
	private static void assertFieldsForSearch(ExpEmployeeGroupEntity originalEntity, ExpEmployeeGroupEntity entity) {
		System.assertEquals(originalEntity.active, entity.active);
		System.assertEquals(originalEntity.code, entity.code);
		System.assertEquals(originalEntity.companyId, entity.companyId);
		System.assertEquals(originalEntity.descriptionL0, entity.descriptionL0);
		System.assertEquals(originalEntity.descriptionL1, entity.descriptionL1);
		System.assertEquals(originalEntity.descriptionL2, entity.descriptionL2);
		System.assertEquals(originalEntity.NameL0, entity.NameL0);
		System.assertEquals(originalEntity.nameL1, entity.nameL1);
		System.assertEquals(originalEntity.nameL2, entity.nameL2);
		System.assertEquals(originalEntity.uniqKey, entity.uniqKey);
		if (entity.reportTypeIdList != null) {
			for(Integer i=0; i<entity.reportTypeIdList.size(); i++){
				System.assertEquals(entity.reportTypeIdList[i], originalEntity.reportTypeIdList[i]);
			}
		}
	}

	private static void assertFields(ExpEmployeeGroupEntity entity, ExpEmployeeGroup__c sObjTest) {
		System.assertEquals(entity.active, sObjTest.Active__c);
		System.assertEquals(entity.code, sObjTest.Code__c);
		System.assertEquals(entity.companyId, sObjTest.CompanyId__c);
		System.assertEquals(entity.descriptionL0, sObjTest.Description_L0__c);
		System.assertEquals(entity.descriptionL1, sObjTest.Description_L1__c);
		System.assertEquals(entity.descriptionL2, sObjTest.Description_L2__c);
		System.assertEquals(entity.nameL0, sObjTest.Name_L0__c);
		System.assertEquals(entity.nameL1, sObjTest.Name_L1__c);
		System.assertEquals(entity.nameL2, sObjTest.Name_L2__c);
		System.assertEquals(companyObj.Code__c+'-'+entity.code, sObjTest.UniqKey__c);
	}

	/**
	 * Assert expected entity field with test sObject field
	 * @param entity Expense Employee Group Entity
	 * @param sObjTest Expense Employee Group S Object
	 * @param linkObjList Expense Employee Group Expense Report Type Link S Object
	 */
	private static void assertFieldsWithReportTypeIdList(ExpEmployeeGroupEntity entity, ExpEmployeeGroup__c sObjTest, List<ExpEmpGroupExpReportTypeLink__c> linkObjList) {
		System.assertEquals(entity.active, sObjTest.Active__c);
		System.assertEquals(entity.code, sObjTest.Code__c);
		System.assertEquals(entity.companyId, sObjTest.CompanyId__c);
		System.assertEquals(entity.descriptionL0, sObjTest.Description_L0__c);
		System.assertEquals(entity.descriptionL1, sObjTest.Description_L1__c);
		System.assertEquals(entity.descriptionL2, sObjTest.Description_L2__c);
		System.assertEquals(entity.nameL0, sObjTest.Name_L0__c);
		System.assertEquals(entity.nameL1, sObjTest.Name_L1__c);
		System.assertEquals(entity.nameL2, sObjTest.Name_L2__c);
		System.assertEquals(companyObj.Code__c+'-'+entity.code, sObjTest.UniqKey__c);
		for(Integer i=0; i<entity.reportTypeIdList.size(); i++){
			System.assertEquals(entity.reportTypeIdList[i], linkObjList[i].ExpReportTypeId__c);
		}
	}

	/**
	 * Create Expense Employee Group Entity
	 */
	private static ExpEmployeeGroupEntity setTestEntity(List<Id> expReportTypeIdList) {
		ExpEmployeeGroupEntity param = new ExpEmployeeGroupEntity();
		param.active = true;
		param.code = 'code';
		param.companyId = companyObj.id;
		param.descriptionL0 = 'DescriptionL0';
		param.descriptionL1 = 'DescriptionL1';
		param.descriptionL2 = 'DescriptionL2';
		param.nameL0 = 'nameL0';
		param.nameL1 = 'nameL1';
		param.nameL2 = 'nameL2';
		if(expReportTypeIdList!=null){
			param.reportTypeIdList = expReportTypeIdList;
		}
		return param;
	}

	/**
	 * Create Expense Employee Group Entity
	 */
	private static ExpEmployeeGroupEntity setTestEntityWithoutReportType() {
		ExpEmployeeGroupEntity param = new ExpEmployeeGroupEntity();
		param.active = true;
		param.code = 'code';
		param.companyId = companyObj.id;
		param.descriptionL0 = 'DescriptionL0';
		param.descriptionL1 = 'DescriptionL1';
		param.descriptionL2 = 'DescriptionL2';
		param.nameL0 = 'nameL0';
		param.nameL1 = 'nameL1';
		param.nameL2 = 'nameL2';
		return param;
	}

	/**
	 * SOQL statement to get ExpEmployeeGroup__c list
	 * @param id Expense Employee Group Id
	 * @return List of Expense Employee Group
	 */
	private static List<ExpEmployeeGroup__c> getExpenseEmployeeGroupSObjectFROMSOQL(Id id) {
		return [SELECT
			Id,
			Active__c,
			Code__c,
			CompanyId__c,
			Description_L0__c,
			Description_L1__c,
			Description_L2__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			UniqKey__c

		FROM ExpEmployeeGroup__c
		WHERE Id =: id];
	}

	/**
	 * SOQL statement to get ExpEmpGroupExpReportTypeLink__c list
	 * @param id Expense Employee Group Id
	 * @return List of ExpEmpGroupExpReportTypeLink object
	 */
	private static List<ExpEmpGroupExpReportTypeLink__c> getExpEmpGroupExpReportTypeLinkSObjectFROMSOQL(Id empGroupId) {
		return [SELECT
			Id,
			ExpEmpGroupId__c,
			ExpReportTypeId__c,
			Order__c
			FROM ExpEmpGroupExpReportTypeLink__c
			WHERE ExpEmpGroupId__c =: empGroupId
			ORDER BY Order__c
		];
	}

}