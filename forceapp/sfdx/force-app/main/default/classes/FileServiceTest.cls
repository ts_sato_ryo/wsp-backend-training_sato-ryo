/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description FileServiceのテストクラス Test class for FileService
 */
@isTest
private class FileServiceTest {
	
	/**
	 * ファイル保存処理のテスト Test for saveFile method
	 */
	@isTest static void saveFileTest() {

		// ファイルボディ File body
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);

		Test.startTest();

		FileService service = new FileService();
		String fileName = 'File.jpg';
		AppContentVersionEntity savedEntity = service.saveFile(fileName, bodyString, 'Receipt');

		Test.stopTest();

		ContentVersion savedFile = [
			SELECT
				Id,
				Title,
				PathOnClient,
				VersionData
			FROM ContentVersion
			WHERE Id = :savedEntity.id];

		System.assert(savedFile.Title.startsWith(FileService.EXP_RECEIPT_TITLE_PREFIX));
		System.assertEquals(fileName, savedFile.PathOnClient);
	}

	/**
	 * ファイル一覧取得処理のテスト Test for getFileList method
	 */
	@isTest static void getFileListTest() {

		// ファイルボディ File body
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);

		// ファイルを作成する Create files
		List<ContentVersion> contentList = new List<ContentVersion>();
		ContentVersion content1 = new ContentVersion(
			Title = FileService.EXP_RECEIPT_TITLE_PREFIX + 'Test1',
			PathOnClient = 'Test1.jpg',
			VersionData = Blob.valueOf(bodyString));
		contentList.add(content1);
		ContentVersion content2 = new ContentVersion(
			Title = FileService.EXP_RECEIPT_TITLE_PREFIX + 'Test2',
			PathOnClient = 'Test2.jpg',
			VersionData = Blob.valueOf(bodyString));
		contentList.add(content2);
		ContentVersion content3 = new ContentVersion(
			Title = FileService.EXP_RECEIPT_TITLE_PREFIX + 'Test3',
			PathOnClient = 'Test3.jpg',
			VersionData = Blob.valueOf(bodyString));
		contentList.add(content3);
		insert contentList;

		Test.startTest();

		// ファイルを取得する Get files
		FileService service = new FileService();
		List<AppContentDocumentEntity> resList = service.getFileList('Receipt', UserInfo.getUserId());

		Test.stopTest();

		List<ContentDocument> documentList = [
			SELECT
				Id,
				OwnerId,
				Title,
				LatestPublishedVersionId,
				ContentModifiedDate
			FROM ContentDocument
			ORDER BY Title];

		System.assertEquals(3, resList.size());
		for (Integer i = 0; i < resList.size(); i++) {
			System.assertEquals(documentList[i].Id, resList[i].id);
			System.assertEquals(documentList[i].OwnerId, resList[i].ownerId);
			System.assertEquals(documentList[i].Title, resList[i].title);
			System.assertEquals('JPG', resList[i].fileType);
			System.assertEquals(documentList[i].LatestPublishedVersionId, resList[i].latestPublishedVersionId);
			System.assertEquals(documentList[i].ContentModifiedDate, resList[i].contentModifiedDate.getDatetime());
		}
	}

	/**
	 * ファイル取得処理のテスト Test for getFile method
	 */
	@isTest static void getFileTest() {
		FileService service = new FileService();

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File.jpg';
		Id contentVersionId = service.saveFile(fileName, bodyString, 'Receipt').id;

		Test.startTest();

		// ファイルを取得する Get files
		AppContentVersionEntity res = service.getFile(contentVersionId);

		Test.stopTest();

		ContentVersion contentVersion = [
			SELECT
				Id,
				ContentDocumentId,
				PathOnClient,
				Title,
				FileType,
				VersionData
			FROM ContentVersion
			WHERE Id = :contentVersionId];

		System.assertEquals(contentVersion.Id, res.id);
		System.assertEquals(contentVersion.ContentDocumentId, res.contentDocumentId);
		System.assertEquals(contentVersion.PathOnClient, res.pathOnClient);
		System.assertEquals(contentVersion.Title, res.title);
		System.assertEquals('JPG', res.fileType);
		System.assertEquals(EncodingUtil.base64Encode(contentVersion.VersionData), res.versionDataText);
	}

	/**
	 * ファイル削除処理のテスト Test for deleteFile method
	 */
	@isTest static void deleteFileTest() {
		FileService service = new FileService();

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File.jpg';
		Id contentDocumentId = service.saveFile(fileName, bodyString, 'Receipt').contentDocumentId;

		Test.startTest();

		// ファイルを削除する Delete files
		service.deleteFile(new List<Id>{contentDocumentId});

		Test.stopTest();

		List<ContentDocument> contentList = [SELECT Id FROM ContentDocument];

		System.assertEquals(true, contentList.isEmpty());
	}
	
	/**
	 * ファイル削除処理のテスト Test for deleteFile method
	 * Verify file cannot be deleted if associated with other records
	 */
	@isTest static void deleteFileWithAssociationTest() {
		FileService service = new FileService();

		// ファイル1を作成する Create file 1
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File1.jpg';
		Id contentDocumentId1 = service.saveFile(fileName, bodyString, 'Receipt').contentDocumentId;
		// ファイル2を作成する Create file 2
		byteSize = (Integer)Math.pow(10, 3); // 1KB
		bodyString = ('0').repeat(byteSize);
		fileName = 'File2.jpg';
		Id contentDocumentId2 = service.saveFile(fileName, bodyString, 'Receipt').contentDocumentId;
		
		// Associate 1 file with 1 other record
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ContentDocumentLink link = new ContentDocumentLink(
			ContentDocumentId = contentDocumentId2,
			LinkedEntityId = org.id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		insert link;
		
		Exception expectedException;

		Test.startTest();

		try {
			service.deleteFile(new List<Id>{contentDocumentId1, contentDocumentId2});
		} catch (Exception e) {
			expectedException = e;
		}

		Test.stopTest();

		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceOf App.ParameterException);

		List<ContentDocument> contentList = [SELECT Id FROM ContentDocument];
		System.assertEquals(2, contentList.size());
	}
	
	/**
	 * Test for getContentDocumentEntityList method
	 */
	@isTest static void getContentDocumentEntityListTest() {
		FileService service = new FileService();

		// Create file 1
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File1.jpg';
		Id contentDocumentId1 = service.saveFile(fileName, bodyString, 'Receipt').contentDocumentId;
		// Create file 2
		byteSize = (Integer)Math.pow(10, 3); // 1KB
		bodyString = ('0').repeat(byteSize);
		fileName = 'File2.jpg';
		Id contentDocumentId2 = service.saveFile(fileName, bodyString, 'Receipt').contentDocumentId;
		// Create file 3
		byteSize = (Integer)Math.pow(10, 3); // 1KB
		bodyString = ('0').repeat(byteSize);
		fileName = 'File3.jpg';
		Id contentDocumentId3 = service.saveFile(fileName, bodyString, 'Receipt').contentDocumentId;
		
		// Associate 2 file with 1 other record
		// For this test, the files will be associated with an ExpRecord
		ExpTestData testData = new ExpTestData();
		ExpReportEntity testReport = testData.createExpReport(Date.today(), testData.department, testData.employee,
				testData.job);
		List<ExpRecordEntity> testRecords = testData.createExpRecords(testReport.id, Date.today(), testData.expType,
				testData.employee, 1);

		List<ContentDocumentLink> linkList = new List<ContentDocumentLink>();
		
		ContentDocumentLink link1 = new ContentDocumentLink(
			ContentDocumentId = contentDocumentId2,
			LinkedEntityId = testRecords[0].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		linkList.add(link1);
		
		ContentDocumentLink link2 = new ContentDocumentLink(
			ContentDocumentId = contentDocumentId3,
			LinkedEntityId = testRecords[0].id,
			ShareType = AppContentDocumentRepository.LINK_SHARE_TYPE_VIEWER);
		linkList.add(link2);
		
		insert linkList;
		
		// start test
		Test.startTest();
		
		List<AppContentDocumentEntity> result = service.getContentDocumentEntityList(new List<Id>{testRecords[0].id});
		
		Test.stopTest();
		
		System.assertEquals(2, result.size());

		Boolean isFile2Retrieved = false;
		Boolean isFile3Retrieved = false;

		for (AppContentDocumentEntity resultEntity : result) {
			if (resultEntity.id == contentDocumentId2) {
				isFile2Retrieved = true;
			}
			if (resultEntity.id == contentDocumentId3) {
				isFile3Retrieved = true;
			}
		}

		System.assertEquals(true, isFile2Retrieved);
		System.assertEquals(true, isFile3Retrieved);
	}
}