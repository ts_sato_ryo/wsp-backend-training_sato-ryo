/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇付与のリポジトリ
 */
public with sharing class AttManagedLeaveGrantRepository extends Repository.BaseRepository {
	/** 有休付与取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_GRANT_FIELD_SET;
	static {
		GET_GRANT_FIELD_SET = new Set<Schema.SObjectField> {
				AttManagedLeaveGrant__c.Id,
				AttManagedLeaveGrant__c.EmployeeBaseId__c,
				AttManagedLeaveGrant__c.LeaveId__c,
				AttManagedLeaveGrant__c.OriginalDaysGranted__c,
				AttManagedLeaveGrant__c.DaysGranted__c,
				AttManagedLeaveGrant__c.DaysLeft__c,
				AttManagedLeaveGrant__c.DaysExpired__c,
				AttManagedLeaveGrant__c.ValidFrom__c,
				AttManagedLeaveGrant__c.ValidTo__c,
				AttManagedLeaveGrant__c.Comment__c,
				AttManagedLeaveGrant__c.OwnerId,
				AttManagedLeaveGrant__c.LastModifiedDate,
				AttManagedLeaveGrant__c.Removed__c
		};
	}
	/** 有休付与調整取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_GRANT_ADJUST_FIELD_SET;
	static {
		GET_GRANT_ADJUST_FIELD_SET = new Set<Schema.SObjectField> {
				AttManagedLeaveGrantAdjust__c.Id,
				AttManagedLeaveGrantAdjust__c.LeaveGrantId__c,
				AttManagedLeaveGrantAdjust__c.DaysAdjusted__c,
				AttManagedLeaveGrantAdjust__c.Date__c,
				AttManagedLeaveGrantAdjust__c.Removed__c
		};
	}
	/** 有休付与サマリ取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_GRANT_SUMMARY_FIELD_SET;
	static {
		GET_GRANT_SUMMARY_FIELD_SET = new Set<Schema.SObjectField> {
				AttManagedLeaveGrantSummary__c.Id,
				AttManagedLeaveGrantSummary__c.LeaveSummaryId__c,
				AttManagedLeaveGrantSummary__c.LeaveGrantId__c,
				AttManagedLeaveGrantSummary__c.DaysTaken__c
		};
	}
	/** 休暇リレーション名（休暇）*/
	private static final String LEAVE_R= AttManagedLeaveGrant__c.LeaveId__c.getDescribe().getRelationshipName();
	/** 休暇リレーション名（休暇サマリ）*/
	private static final String LEAVE_SUMMARY_R= AttManagedLeaveGrantSummary__c.LeaveSummaryId__c.getDescribe().getRelationshipName();
	/** 休暇リレーション名（勤怠サマリ）*/
	private static final String ATT_SUMMARY_R= AttManagedLeaveSummary__c.AttSummaryId__c.getDescribe().getRelationshipName();

	/** 子リレーション名（休暇付与調整）*/
	private static final String CHILD_RELATION_ADJUSTS = 'LeaveGrantAdjusts__r';
	/** 子リレーション名（休暇付与サマリ）*/
	private static final String CHILD_RELATION_SUMMARIES = 'LeaveGrantSummaries__r';
	/** 検索フィルタ (検索API実行用) */
	public class SearchFilter {
		public Id Id;
		public Id empId;
		public Set<Id> empIds;
		public Id leaveId;
		public Set<Id> leaveIds;
		public Boolean includeAdmitSummary;
		public AppDate dateFrom;
		public AppDate dateTo;
	}
	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AttManagedLeaveGrantEntity entity) {
		return saveEntityList(new List<AttManagedLeaveGrantEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveGrantEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveGrantEntity> entityList, Boolean allOrNone) {

		List<AttManagedLeaveGrant__c> grantList = new List<AttManagedLeaveGrant__c>();

		for (AttManagedLeaveGrantEntity entity : entityList) {
			// エンティティからSObjectを作成する
			grantList.add(createObj(entity));
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(grantList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}
	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveAdjustEntity(AttManagedLeaveGrantEntity.GrantAdjustEntity entity) {
		return saveAdjustEntityList(new List<AttManagedLeaveGrantEntity.GrantAdjustEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveAdjustEntityList(List<AttManagedLeaveGrantEntity.GrantAdjustEntity> entityList) {
		return saveAdjustEntityList(entityList, true);
	}

	/**
	 * 調整エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public SaveResult saveAdjustEntityList(List<AttManagedLeaveGrantEntity.GrantAdjustEntity> entityList, Boolean allOrNone) {

		List<AttManagedLeaveGrantAdjust__c> adjustList = new List<AttManagedLeaveGrantAdjust__c>();

		for (AttManagedLeaveGrantEntity.GrantAdjustEntity entity : entityList) {
			adjustList.add(new AttManagedLeaveGrantAdjust__c(
					Id = entity.id,
					LeaveGrantId__c = entity.grantId,
					Date__c = AppConverter.dateValue(entity.adjustDate),
					DaysAdjusted__c = AppConverter.decValue(entity.daysAdjusted),
					Removed__c = entity.isRemoved));
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(adjustList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}
	public List<AttManagedLeaveGrantEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {
		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_GRANT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Type__c));
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L0__c));
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L1__c));
		selectFldList.add(LEAVE_R + '.' + getFieldName(AttLeave__c.Name_L2__c));

		List<String> selectAdjustFldList = new List<String>();
		for (Schema.SObjectField fld : GET_GRANT_ADJUST_FIELD_SET) {
			selectAdjustFldList.add(getFieldName(fld));
		}
		List<String> selectSummaryFldList = new List<String>();
		for (Schema.SObjectField fld : GET_GRANT_SUMMARY_FIELD_SET) {
			selectSummaryFldList.add(getFieldName(fld));
		}
		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id grantId = filter.id;
		final Id empId = filter.empId;
		final Set<Id> empIds = filter.empIds;
		final Id leaveId = filter.leaveId;
		final Set<Id> leaveIds = filter.leaveIds;
		final Boolean includeAdmitSummary = filter.includeAdmitSummary;
		final Date dateFrom = AppConverter.dateValue(filter.dateFrom);
		final Date dateTo = AppConverter.dateValue(filter.dateTo);

		if (!String.isBlank(grantId)) {
			condList.add(getFieldName(AttManagedLeaveGrant__c.Id) + ' = :grantId');
		}
		if (!String.isBlank(empId)) {
			condList.add(getFieldName(AttManagedLeaveGrant__c.EmployeeBaseId__c) + ' = :empId');
		}
		if (empIds != null) {
			condList.add(getFieldName(AttManagedLeaveGrant__c.EmployeeBaseId__c) + ' IN :empIds');
		}
		if (!String.isBlank(leaveId)) {
			condList.add(getFieldName(AttManagedLeaveGrant__c.LeaveId__c) + ' = :leaveId');
		}
		if (leaveIds != null) {
			condList.add(getFieldName(AttManagedLeaveGrant__c.LeaveId__c) + ' IN :leaveIds');
		}
		if (dateFrom != null) {
			condList.add(getFieldName(AttManagedLeaveGrant__c.ValidTo__c) + ' > :dateFrom');
		}
		if (dateTo != null) {
			condList.add(getFieldName(AttManagedLeaveGrant__c.ValidFrom__c) + ' <= :dateTo');
		}
		// 論理削除されているレコードを検索対象外にする
		condList.add(getFieldName(AttManagedLeaveGrant__c.Removed__c) + ' = false');

		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ') +
					' ,(SELECT ' + String.join(selectAdjustFldList, ', ') +
						' FROM ' + CHILD_RELATION_ADJUSTS +
						// 論理削除されているレコードを検索対象外にする
						' WHERE ' + getFieldName(AttManagedLeaveGrantAdjust__c.Removed__c) + ' = false' +
						' ORDER BY ' + getFieldName(AttManagedLeaveGrantAdjust__c.Date__c) + ' Asc)';
		if (includeAdmitSummary == true) {
			soql += ',(SELECT ' + String.join(selectSummaryFldList, ',') +
					' FROM ' + CHILD_RELATION_SUMMARIES +
					' WHERE ' + LEAVE_SUMMARY_R + '.' + ATT_SUMMARY_R + '.' + getFieldName(AttSummary__c.Locked__c) + ' = true) ';
		}
		soql += ' FROM ' + AttManagedLeaveGrant__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (!forUpdate) {
			soql += ' ORDER BY ' + getFieldName(AttManagedLeaveGrant__c.ValidFrom__c) + ' ASC,'
				+ getFieldName(AttManagedLeaveGrant__c.ValidTo__c) + ' ASC';
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// 結果からエンティティを作成する
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttManagedLeaveGrantEntity> entityList = new List<AttManagedLeaveGrantEntity>();
		for (AttManagedLeaveGrant__c grant : Database.query(soql)) {
			entityList.add(createEntity(grant));
		}
		AppLimits.getInstance().incrementQueryCount(AttManagedLeaveGrant__c.SObjectType);

		return entityList;
	}

	public AttManagedLeaveGrantEntity createEntity(AttManagedLeaveGrant__c grant) {
		AttManagedLeaveGrantEntity retEntity = new AttManagedLeaveGrantEntity();
		retEntity.setId(grant.Id);

		retEntity.employeeId = grant.EmployeeBaseId__c;
		retEntity.ownerId = grant.OwnerId;
		retEntity.leaveId = grant.LeaveId__c;
		retEntity.leaveType = AttLeaveType.valueOf(grant.LeaveId__r.Type__c);
		retEntity.leaveNameL = new AppMultiString(
			grant.LeaveId__r.Name_L0__c,
			grant.LeaveId__r.Name_L1__c,
			grant.LeaveId__r.Name_L2__c);
		retEntity.originalDaysGranted = AttDays.valueOf(grant.OriginalDaysGranted__c);
		retEntity.daysGranted = AttDays.valueOf(grant.DaysGranted__c);
		retEntity.daysLeft = AttDays.valueOf(grant.DaysLeft__c);
		retEntity.daysExpired = AttDays.valueOf(grant.DaysExpired__c);
		retEntity.validDateFrom = AppDate.valueOf(grant.ValidFrom__c);
		retEntity.validDateTo = AppDate.valueOf(grant.ValidTo__c);
		retEntity.comment = grant.Comment__c;
		retEntity.lastModifiedDate = AppDateTime.valueOf(grant.LastModifiedDate);
		retEntity.isRemoved = grant.Removed__c;
		retEntity.adjustList = new List<AttManagedLeaveGrantEntity.GrantAdjustEntity>();
		for (AttManagedLeaveGrantAdjust__c adjust : grant.LeaveGrantAdjusts__r) {
			AttManagedLeaveGrantEntity.GrantAdjustEntity adjustData = new AttManagedLeaveGrantEntity.GrantAdjustEntity();
			adjustData.setId(adjust.Id);
			adjustData.grantId = adjust.LeaveGrantId__c;
			adjustData.adjustDate = AppDate.valueOf(adjust.Date__c);
			adjustData.daysAdjusted = AttDays.valueOf(adjust.DaysAdjusted__c);
			adjustData.isRemoved = adjust.Removed__c;
			retEntity.adjustList.add(adjustData);
		}
		retEntity.summaryList = new List<AttManagedLeaveGrantSummaryEntity>();
		if (grant.LeaveGrantSummaries__r != null) {
			for (AttManagedLeaveGrantSummary__c grantSummary : grant.LeaveGrantSummaries__r) {
				AttManagedLeaveGrantSummaryEntity summaryData = new AttManagedLeaveGrantSummaryEntity();
				summaryData.daysTaken = AttDays.valueOf(grantSummary.DaysTaken__c);
				retEntity.summaryList.add(summaryData);
			}
		}
		retEntity.resetChanged();
		return retEntity;
	}

	private AttManagedLeaveGrant__c createObj(AttManagedLeaveGrantEntity entity) {
		Boolean isInsert = String.isBlank(entity.id);
		AttManagedLeaveGrant__c retObject = new AttManagedLeaveGrant__c();
		retObject.Id = entity.Id;

		if (isInsert && entity.isChanged(AttManagedLeaveGrantEntity.Field.EMPLOYEE_ID)) {
			retObject.EmployeeBaseId__c = entity.employeeId;
		}
		if (isInsert && entity.isChanged(AttManagedLeaveGrantEntity.Field.OWNER_ID)) {
			retObject.OwnerId = entity.ownerId;
		}
		if (isInsert && entity.isChanged(AttManagedLeaveGrantEntity.Field.LEAVE_ID)) {
			retObject.LeaveId__c = entity.leaveId;
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.ORIGINAL_DAYS_GRANTED)) {
			retObject.OriginalDaysGranted__c = AppConverter.decValue(entity.originalDaysGranted);
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.DAYS_GRANTED)) {
			retObject.DaysGranted__c = AppConverter.decValue(entity.daysGranted);
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.DAYS_LEFT)) {
			retObject.DaysLeft__c = AppConverter.decValue(entity.daysLeft);
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.DAYS_EXPIRED)) {
			retObject.DaysExpired__c = AppConverter.decValue(entity.daysExpired);
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.VALID_FROM)) {
			retObject.ValidFrom__c = AppConverter.dateValue(entity.validDateFrom);
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.VALID_TO)) {
			retObject.ValidTo__c = AppConverter.dateValue(entity.validDateTo);
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.COMMENT)) {
			retObject.Comment__c = entity.comment;
		}
		if (entity.isChanged(AttManagedLeaveGrantEntity.Field.IS_REMOVED)) {
			retObject.Removed__c = entity.isRemoved;
		}
		return retObject;
	}
}