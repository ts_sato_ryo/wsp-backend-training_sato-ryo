/**
  * @group 勤怠
  *
  * @desctiprion 勤務パターン適用インポートのリポジトリ
  */
public with sharing class AttPatternApplyImportRepository extends Repository.BaseRepository {
	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_IMPORT_FIELD_SET;
	static {
		GET_IMPORT_FIELD_SET = new Set<Schema.SObjectField> {
				AttPatternApplyImport__c.Id,
				AttPatternApplyImport__c.ImportBatchId__c,
				AttPatternApplyImport__c.No__c,
				AttPatternApplyImport__c.EmployeeCode__c,
				AttPatternApplyImport__c.Date__c,
				AttPatternApplyImport__c.DayType__c,
				AttPatternApplyImport__c.PatternCode__c,
				AttPatternApplyImport__c.Status__c,
				AttPatternApplyImport__c.Error__c
		};
	}

	/** 検索フィルタ (検索API実行用) */
	public class SearchFilter {
		/** インポートバッチID */
		public Id importBatchId;
		/** ステータス */
		public List<ImportStatus> statusList;
	}

	/**
	 * 指定バッチの勤務パターン適用インポートを取得する
	 * @param importBatchId 指定するバッチId
	 * @return 指定バッチの全シフトインポート一覧
	 */
	public List<AttPatternApplyImportEntity> getPatternList(Id importBatchId) {
		SearchFilter filter = new SearchFilter();
		filter.importBatchId = importBatchId;

		return searchEntityList(filter, false);
	}

	/**
	 * 勤務パターン適用インポートを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致した勤務パターン一覧(指定順)
	 */
	private List<AttPatternApplyImportEntity> searchEntityList(
			SearchFilter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_IMPORT_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id importBatchId = filter.importBatchId;
		final List<String> statusList;
		if (importBatchId != null) {
			condList.add(getFieldName(AttPatternApplyImport__c.ImportBatchId__c) + ' = :importBatchId');
		}
		if (filter.statusList != null) {
			for (ImportStatus status : filter.statusList) {
				statusList.add(status.value);
			}
			condList.add(getFieldName(AttPatternApplyImport__c.Status__c) + ' IN :statusList');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + AttPatternApplyImport__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		soql += ' ORDER BY ' + getFieldName(AttPatternApplyImport__c.ImportBatchId__c) + ' Asc,'
				+ getFieldName(AttPatternApplyImport__c.No__c) + ' Asc';
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// クエリ実行
		//System.debug('AttShiftImportRepository.searchEntityList: SOQL==>' + soql);
		List<AttPatternApplyImport__c> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<AttPatternApplyImportEntity> entityList = new List<AttPatternApplyImportEntity>();
		for (AttPatternApplyImport__c attShiftImport : sObjList) {
			entityList.add(new AttPatternApplyImportEntity(attShiftImport));
		}

		return entityList;
	}

	/**
	 * 勤務パターン適用インポートを検索する (検索API用)
	 * @param filter 検索条件
	 * @return 条件に一致した勤務パターン適用インポート一覧
	 */
	public List<AttPatternApplyImportEntity> searchEntityList(SearchFilter filter){
		return searchEntityList(filter, false);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntity(AttPatternApplyImportEntity entity) {
		return saveEntityList(new List<AttPatternApplyImportEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntityList(List<AttPatternApplyImportEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public Repository.SaveResult saveEntityList(List<AttPatternApplyImportEntity> entityList, Boolean allOrNone) {

		List<SObject> saveObjectList = createObjectList(entityList);
		List<Database.UpsertResult> resList = AppDatabase.doUpsert(saveObjectList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}
	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	private List<SObject> createObjectList(List<AttPatternApplyImportEntity> entityList) {
		List<AttPatternApplyImport__c> objectList = new List<AttPatternApplyImport__c>();
		for (AttPatternApplyImportEntity entity : entityList) {
			objectList.add(entity.createSObject());
		}
		return objectList;
	}
}