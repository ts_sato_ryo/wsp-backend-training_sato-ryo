@isTest
private class CountryRepositoryTest {

	private static CountryRepository repository = new CountryRepository();

	/**
	 * createEntityのテスト
	 * sObjectからエンティティに想定通りに項目を詰め替えていることを検証する。
	 */
	@isTest static void createEntityTest() {

		// オブジェクト作成
		ComCountry__c sobj = new ComCountry__c(
			Name = 'countryName',
			Name_L0__c = 'countryL0',
			Name_L1__c = 'countryL1',
			Name_L2__c = 'countryL2',
			Code__c = 'code'
		);

		// 実行
		CountryEntity resultEntity = repository.createEntity(sobj);

		// 検証
		System.assertEquals(sobj.Id, resultEntity.id);
		System.assertEquals('countryName', resultEntity.name);
		System.assertEquals('countryL0', resultEntity.nameL0);
		System.assertEquals('countryL1', resultEntity.nameL1);
		System.assertEquals('countryL2', resultEntity.nameL2);
		System.assertEquals('code', resultEntity.code);
	}

	/**
	 * searchByIdのテスト
	 * 指定したIDで取得できること、取得した値をエンティティに正しく設定することを検証する
	 */
	@isTest static void searchByIdTestExist() {

		// データ登録
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country', 4);

		// 検索
		CountryEntity entity = repository.searchById(createCountryList[2].Id);

		// 検証
		System.assertEquals(createCountryList[2].Id, entity.id);
		System.assertEquals('country3', entity.code);
		System.assertEquals('country3', entity.name);
		System.assertEquals('country3 L0', entity.nameL0);
		System.assertEquals('country3 L1', entity.nameL1);
		System.assertEquals('country3 L2', entity.nameL2);
	}

	/**
	 * searchByIdのテスト
	 * 指定したIDのレコードが存在しない場合、nullを返すことを検証する
	 */
	@isTest static void searchByIdTestNotExist() {

		// データ登録
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country', 4);

		// 検索
		CountryEntity entity = repository.searchById('a0J7F000005Pj8FUAS');

		// 検証
		System.assert(entity == null);
	}

	/**
	 * searchByCodeのテスト
	 * 指定したコードで取得できること、取得した値をエンティティに正しく設定することを検証する
	 */
	@isTest static void searchByCodeTestExist() {

		// データ登録
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country', 4);

		// 検索
		CountryEntity entity = repository.searchByCode(createCountryList[2].Code__c);

		// 検証
		System.assertEquals(createCountryList[2].Id, entity.id);
		System.assertEquals('country3', entity.code);
		System.assertEquals('country3', entity.name);
		System.assertEquals('country3 L0', entity.nameL0);
		System.assertEquals('country3 L1', entity.nameL1);
		System.assertEquals('country3 L2', entity.nameL2);
	}

	/**
	 * searchByCodeのテスト
	 * 指定したコードのレコードが存在しない場合、nullを返すことを検証する
	 */
	@isTest static void searchByCodeTestNotExist() {

		// データ登録
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country', 4);

		// 検索
		CountryEntity entity = repository.searchByCode('NOT_EXIST');

		// 検証
		System.assert(entity == null);
	}

	/**
	 * searchEntityListのテスト
	 * 指定したIDのレコードを取得できることを検証する。
	 */
	@isTest static void searchEntityListTest() {

		// データ登録
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country', 4);

		// 検索
		CountryRepository.SearchFilter filter = new CountryRepository.SearchFilter();
		filter.ids.add(createCountryList[1].id);
		List<CountryEntity> entityList = repository.searchEntityList(filter);

		// 検証
		System.assertEquals(createCountryList[1].Id, entityList[0].id);
		System.assertEquals('country2', entityList[0].code);
		System.assertEquals('country2', entityList[0].name);
		System.assertEquals('country2 L0', entityList[0].nameL0);
		System.assertEquals('country2 L1', entityList[0].nameL1);
		System.assertEquals('country2 L2', entityList[0].nameL2);
	}

	/**
	 * createSObjectのテスト
	 * エンティティからsObjectに想定通りに項目を詰め替えていることを検証する。
	 */
	@isTest static void createSObjectTest() {

		// エンティティ作成
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country', 1);
		CountryEntity entity = new CountryEntity();
		entity.setId(createCountryList[0].Id);
		entity.name = 'country_name1';
		entity.nameL0 = 'country_nameL0_1';
		entity.nameL1 = 'country_nameL1_1';
		entity.nameL2 = 'country_nameL2_1';
		entity.code = 'code_1';

		// 実行
		ComCountry__c resultSObj = repository.createSObject(entity);

		// 検証
		System.assertEquals(entity.id, resultSObj.Id);
		System.assertEquals('country_name1', resultSObj.Name);
		System.assertEquals('country_nameL0_1', resultSObj.Name_L0__c);
		System.assertEquals('country_nameL1_1', resultSObj.Name_L1__c);
		System.assertEquals('country_nameL2_1', resultSObj.Name_L2__c);
		System.assertEquals('code_1', resultSObj.Code__c);
	}

	/**
	 * saveEntityのテスト
	 * 新規の国エンティティが1件登録されることを検証する。
	 */
	@isTest static void saveEntityTestInsert() {

		// エンティティ作成
		CountryEntity entity = new CountryEntity();
		entity.name = 'country_name1';
		entity.nameL0 = 'country_nameL0_1';
		entity.nameL1 = 'country_nameL1_1';
		entity.nameL2 = 'country_nameL2_1';
		entity.code = 'code_1';

		// 実行
		Repository.SaveResult result = new Repository.SaveResult();
		result = repository.saveEntity(entity);

		// 検証
		List<ComCountry__c> countryList = [SELECT Id, Code__c, Name_L0__c, Name_L1__c, Name_L2__c, Name FROM ComCountry__c];
		System.assertEquals(true, result.isSuccessAll);
		System.assertEquals(1, countryList.size());
		System.assertEquals('country_name1', countryList[0].Name);
		System.assertEquals('country_nameL0_1', countryList[0].Name_L0__c);
		System.assertEquals('country_nameL1_1', countryList[0].Name_L1__c);
		System.assertEquals('country_nameL2_1', countryList[0].Name_L2__c);
		System.assertEquals('code_1', countryList[0].Code__c);

	}

	/**
	 * saveEntityのテスト
	 * 国エンティティと既存レコードのIDが一致する場合、新規に登録されずに既存レコードが更新されることを検証する。
	 */
	@isTest static void saveEntityTestUpdate() {

		// データ登録
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country_before', 4);
		List<ComCountry__c> countryListBefore = [SELECT Id, Code__c FROM ComCountry__c];
		System.assertEquals(4, countryListBefore.size());

		// エンティティ作成
		CountryEntity entity = new CountryEntity();
		entity.setId(createCountryList[1].Id);
		entity.name = 'country_name_after';
		entity.nameL0 = 'country_nameL0_after';
		entity.nameL1 = 'country_nameL1_after';
		entity.nameL2 = 'country_nameL2_after';
		entity.code = 'code_after';

		// 実行
		Repository.SaveResult result = new Repository.SaveResult();
		result = repository.saveEntity(entity);

		// 検証
		List<ComCountry__c> countryListAfter = [SELECT Id, Code__c, Name_L0__c, Name_L1__c, Name_L2__c, Name FROM ComCountry__c];
		System.assertEquals(true, result.isSuccessAll);
		System.assertEquals(result.details[0].Id, countryListAfter[1].Id);
		System.assertEquals(4, countryListAfter.size());
		System.assertEquals('country_name_after', countryListAfter[1].Name);
		System.assertEquals('country_nameL0_after', countryListAfter[1].Name_L0__c);
		System.assertEquals('country_nameL1_after', countryListAfter[1].Name_L1__c);
		System.assertEquals('country_nameL2_after', countryListAfter[1].Name_L2__c);
		System.assertEquals('code_after', countryListAfter[1].Code__c);

		System.assertEquals('country_before3', countryListAfter[2].Name);
		System.assertEquals('country_before3 L0', countryListAfter[2].Name_L0__c);
		System.assertEquals('country_before3 L1', countryListAfter[2].Name_L1__c);
		System.assertEquals('country_before3 L2', countryListAfter[2].Name_L2__c);
		System.assertEquals('country3', countryListAfter[2].Code__c);
	}

	/**
	 * saveEntityのテスト
	 * 新規の国エンティティがNUllの場合、例外が発生して登録されずに終了することを検証する。
	 */
	@isTest static void saveEntityTestNullArgs() {

		List<ComCountry__c> entityListBefore = [SELECT Id FROM ComCountry__c];
		System.assert(entityListBefore.isEmpty());

		try {
			repository.saveEntity(null);
			System.assert(false);
		} catch (Exception e) {
			List<ComCountry__c> entityListAfter = [SELECT Id FROM ComCountry__c];
			System.assert(entityListAfter.isEmpty());
		}
	}

	/**
	 * saveEntityListのテスト
	 * リストの中に、登録と更新のレコードが混在する場合の結果を検証する。
	 */
	@isTest static void saveEntityListTestInsertUpdate() {

		// データ登録
		List<ComCountry__c> createCountryList = ComTestDataUtility.createCountries('country_before', 3);
		List<ComCountry__c> countryListBefore = [SELECT Id, Code__c FROM ComCountry__c];
		System.assertEquals(3, countryListBefore.size());

		// エンティティ作成
		List<CountryEntity> entityList = new List<CountryEntity>();

		// 新規登録（必須項目のみ）
		CountryEntity entity1 = new CountryEntity();
		entity1.nameL0 = 'country_nameL0_after_1';
		entity1.code = 'code_after_1';
		entityList.add(entity1);

		// 更新（必須項目のみ）
		CountryEntity entity2 = new CountryEntity();
		entity2.setId(createCountryList[1].Id);
		entity2.nameL0 = 'country_nameL0_after_2';
		entity2.code = 'code_after_2';
		entityList.add(entity2);

		// 実行
		Repository.SaveResult result = new Repository.SaveResult();
		result = repository.saveEntityList(entityList);
		Id entityId1 = result.details[0].id;
		Id entityId2 = result.details[1].id;

		// 検証
		Map<Id ,ComCountry__c> countryListAfter = new Map<Id ,ComCountry__c>([SELECT Id, Code__c, Name_L0__c, Name_L1__c, Name_L2__c, Name FROM ComCountry__c]);
		System.assertEquals(true, result.isSuccessAll);
		System.assertEquals(4, countryListAfter.size());
		System.assertEquals(result.details[0].Id, countryListAfter.get(entityId1).Id);
		System.assertEquals(result.details[1].Id, countryListAfter.get(entityId2).Id);
		System.assertEquals('country1', countryListAfter.get(createCountryList[0].Id).Code__C);
		System.assertEquals('code_after_2', countryListAfter.get(entityId2).Code__C);
		System.assertEquals('country3', countryListAfter.get(createCountryList[2].Id).Code__C);
		System.assertEquals('code_after_1', countryListAfter.get(entityId1).Code__C);

		// 新規登録の検証（必須項目が登録されていること、必須項目以外がデフォルト値であること）
		System.assert(countryListAfter.get(entityId1).Name != null);
		System.assertEquals('country_nameL0_after_1', countryListAfter.get(entityId1).Name_L0__c);
		System.assert(countryListAfter.get(entityId1).Name_L1__c == null);
		System.assert(countryListAfter.get(entityId1).Name_L2__c == null);
		System.assertEquals('code_after_1', countryListAfter.get(entityId1).Code__c);

		// 更新の検証（必須項目が更新されていること、必須項目以外が更新されていないこと）
		System.assertEquals('country_before2', countryListAfter.get(entityId2).Name);
		System.assertEquals('country_nameL0_after_2', countryListAfter.get(entityId2).Name_L0__c);
		System.assertEquals('country_before2 L1', countryListAfter.get(entityId2).Name_L1__c);
		System.assertEquals('country_before2 L2', countryListAfter.get(entityId2).Name_L2__c);
		System.assertEquals('code_after_2', countryListAfter.get(entityId2).Code__c);
	}
}