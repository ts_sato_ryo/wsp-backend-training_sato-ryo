/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description 代理承認者設定のAPIを提供するリソースクラス Resource class for delegated approver setting function
 */
public with sharing class DelegatedApproverSettingResource {

	/**
	 * @description 代理承認者設定一覧データ取得条件を格納するクラス Parameter for delegated approver setting list
	 */
	public class GetSettingListParam implements RemoteApi.RequestParam {
		/** 社員ベースID */
		public String empId;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 社員ID
			if (String.isNotBlank(empId)) {
				try {
					Id.ValueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', empId);
				}
			}
		}
	}

	/**
	 * @description 代理承認者設定一覧データ取得結果を格納するクラス Result of getting delegated approver setting list
	 */
	public class GetSettingListResult implements RemoteApi.ResponseParam {
		/** 代理承認者設定のリスト */
		public List<DelegatedApproverSetting> settingList;

		public GetSettingListResult() {
			settingList = new List<DelegatedApproverSetting>();
		}
	}

	/**
	 * @description 代理承認者設定情報を格納するクラス Delegated approver setting info
	 */
	public class DelegatedApproverSetting {
		/** 代理承認者設定ID */
		public String settingId;
		/** 代理承認者ベースID */
		public String delegatedApproverId;
		/** 代理承認者社員コード */
		public String delegatedApproverCode;
		/** 代理承認者社員名 */
		public String delegatedApproverName;
		/** 代理承認者顔写真URL */
		public String delegatedApproverPhotoUrl;
		/** 代理承認者部署コード */
		public String departmentCode;
		/** 代理承認者部署名 */
		public String departmentName;
		/** 事前申請承認却下(代理) */
		public Boolean canApproveExpenseRequestByDelegate;
		/** 経費精算申請承認却下(代理) */
		public Boolean canApproveExpenseReportByDelegate;
		/** Is Active Salesforce User Account for delegated Approver */
		public Boolean isActiveSFUserAcc;

		/**
		 * コンストラクタ（DelegatedApproverSettingEntityから作成) Constructor(create from entity)
		 */
		public DelegatedApproverSetting(DelegatedApproverSettingEntity entity) {
			settingId = entity.id;
			delegatedApproverId = entity.delegatedApproverBaseId;
			if (entity.delegatedApprover != null) {
				delegatedApproverCode = entity.delegatedApprover.code;
				delegatedApproverName = entity.delegatedApprover.fullName == null ? null : entity.delegatedApprover.fullName.getFullName();
				delegatedApproverPhotoUrl = entity.delegatedApprover.photoUrl;
				isActiveSFUserAcc = entity.delegatedApprover.isActiveSFUserAcc;
			}
			if (entity.delegatedApproverDepartment != null) {
				departmentCode = entity.delegatedApproverDepartment.code;
				departmentName = entity.delegatedApproverDepartment.name == null ? null : entity.delegatedApproverDepartment.name.getValue();
			}
			canApproveExpenseRequestByDelegate = entity.canApproveExpenseRequestByDelegate;
			canApproveExpenseReportByDelegate = entity.canApproveExpenseReportByDelegate;
		}
	}

	/**
	 * @description 代理承認者設定一覧データ取得APIを実装するクラス API to get delegated approver setting list
	 */
	public with sharing class GetSettingListApi extends RemoteApi.ResourceBase {

		/**
		 * @description 代理承認者設定一覧データを取得する Get delegated approver setting list
		 * @param req リクエストパラメータ(GetSettingListParam)
		 * @return レスポンス(GetSettingListResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			GetSettingListParam param = (GetSettingListParam)req.getParam(GetSettingListParam.class);

			// パラメータ値の検証
			param.validate();

			Id empId = param.empId;
			if (String.isBlank(empId)) {
				// 社員情報を取得 Get employee
				EmployeeBaseEntity employee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
				empId = employee.id;
			}

			// 代理承認者設定一覧を取得 Get delegated approver setting list
			List<DelegatedApproverSettingEntity> settingList = new DelegatedApproverSettingService().getSettingList(empId);

			// レスポンスをセットする Create response
			GetSettingListResult res = new GetSettingListResult();
			if (settingList != null && !settingList.isEmpty()) {
				for (DelegatedApproverSettingEntity setting : settingList) {
					res.settingList.add(new DelegatedApproverSetting(setting));
				}
			}

			return res;
		}
	}


	/**
	 * @description 代理承認者設定データ保存内容を格納するクラス
	 */
	public class SaveSettingParam implements RemoteApi.RequestParam {
		/** 社員ベースID */
		public String empId;
		/** 代理承認者設定のリスト */
		public List<DelegatedApproverSetting> settings;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 社員ID
			if (String.isNotBlank(empId)) {
				try {
					Id.ValueOf(empId);
				} catch (Exception e) {
					throw new App.ParameterException('empId', empId);
				}
			}
			// 代理承認者設定のリスト
			if (settings != null && !settings.isEmpty()) {
				for (DelegatedApproverSetting setting : settings) {
					// settingId
					if (String.isNotBlank(setting.settingId)) {
						try {
							Id.ValueOf(setting.settingId);
						} catch (Exception e) {
							throw new App.ParameterException('settingId', setting.settingId);
						}
					}
					
					// delegatedApproverId
					if (String.isNotBlank(setting.delegatedApproverId)) {
						try {
							Id.ValueOf(setting.delegatedApproverId);
						} catch (Exception e) {
							throw new App.ParameterException('delegatedApproverId', setting.delegatedApproverId);
						}
					} else {
						throw new App.ParameterException('delegatedApproverId', setting.delegatedApproverId);
					}
				}
			}
		}

		/**
		 * パラメータの値を設定した代理承認者設定エンティティのリストを作成する
		 * @return 値を設定したエンティティのリスト
		 */
		public List<DelegatedApproverSettingEntity> createSettingEntityList() {
			List<DelegatedApproverSettingEntity> settingEntityList = new List<DelegatedApproverSettingEntity>();

			for (DelegatedApproverSetting setting : this.settings) {
				DelegatedApproverSettingEntity entity = new DelegatedApproverSettingEntity();
				entity.setId(setting.settingId);
				entity.employeeBaseId = this.empId;
				entity.delegatedApproverBaseId = setting.delegatedApproverId;
				entity.canApproveExpenseRequestByDelegate = setting.canApproveExpenseRequestByDelegate;
				entity.canApproveExpenseReportByDelegate = setting.canApproveExpenseReportByDelegate;

				settingEntityList.add(entity);
			}

			return settingEntityList;
		}
	}

	/**
	 * @description 代理承認者設定データ保存APIを実装するクラス
	 */
	public with sharing class SaveSettingApi extends RemoteApi.ResourceBase {

		/** ロールバックテストをする場合にtrue */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * @description 代理承認者設定データを保存する
		 * @param req リクエストパラメータ(SaveSettingParam)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SaveSettingParam param = (SaveSettingParam)req.getParam(SaveSettingParam.class);

			// パラメータ値の検証
			param.validate();

			// 社員情報を取得 Get employee
			EmployeeBaseEntity employee;
			if (String.isBlank(param.empId)) {
				employee = new EmployeeService().getEmployeeByUserId(UserInfo.getUserId(), AppDate.today());
				param.empId = employee.id;
			} else {
				employee = new EmployeeService().getEmployee(param.empId, AppDate.today());
			}

			// パラメータからエンティティを作成
			List<DelegatedApproverSettingEntity> settingList = param.createSettingEntityList();

			// 失敗時はDBをロールバックする
			Savepoint sp = Database.setSavepoint();

			try {
				// 代理承認者設定データを保存
				new DelegatedApproverSettingService().saveSettingList(employee, settingList);

				// ロールバックテスト
				// 強制的に例外を発生させてロールバックする
				if (isRollbackTest) {
					throw new App.IllegalStateException('SaveSettingApi Rollback Test');
				}
			} catch (Exception e) {
				// ロールバック
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}


	/**
	 * @description 代理承認者設定データ削除内容を格納するクラス Parameter for delete
	 */
	public class DeleteSettingParam implements RemoteApi.RequestParam {
		/** 代理承認者設定IDのリスト */
		public List<String> settingIds;

		/**
		 * パラメータを検証する
		 * @throws App.ParameterException 不正な値が設定されている場合
		 */
		public void validate() {
			// 代理承認者設定IDのリスト
			if (settingIds == null || settingIds.isEmpty()) {
				throw new App.ParameterException('settingIds', settingIds);
			} else {
				for (String settingId : settingIds) {
					if (String.isNotBlank(settingId)) {
						try {
							Id.ValueOf(settingId);
						} catch (Exception e) {
							throw new App.ParameterException('settingId', settingId);
						}
					}
				}
			}
		}
	}

	/**
	 * @description 代理承認者設定データ削除APIを実装するクラス API to delete delegated approver settings
	 */
	public with sharing class DeleteSettingApi extends RemoteApi.ResourceBase {

		/** ロールバックテストをする場合にtrue */
		@TestVisible
		private Boolean isRollbackTest = false;

		/**
		 * @description 代理承認者設定データを削除する
		 * @param req リクエストパラメータ(DeleteSettingParam)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			DeleteSettingParam param = (DeleteSettingParam)req.getParam(DeleteSettingParam.class);

			// パラメータ値の検証
			param.validate();

			// 失敗時はDBをロールバックする
			Savepoint sp = Database.setSavepoint();

			try {
				// 代理承認者設定データを削除
				new DelegatedApproverSettingService().deleteSettingList(param.settingIds);

				// ロールバックテスト
				// 強制的に例外を発生させてロールバックする
				if (isRollbackTest) {
					throw new App.IllegalStateException('DeleteSettingApi Rollback Test');
				}
			} catch (Exception e) {
				// ロールバック
				Database.rollback(sp);
				throw e;
			}

			return null;
		}
	}
}