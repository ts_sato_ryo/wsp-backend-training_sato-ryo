/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 組織リポジトリのテスト
 */
@isTest
private class OrganizationSettingRepositoryTest {

	/**
	 * getEntityのテスト
	 */
	@isTest static void getEntityTest() {
		ComOrgSetting__c orgObj = new ComOrgSetting__c(
				Language_0__c = 'ja',
				Language_1__c = 'en_US',
				Language_2__c = 'ja');
		insert orgObj;
		orgObj = [SELECT Id, Name, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];

		OrganizationSettingRepository repo = new OrganizationSettingRepository();
		OrganizationSettingEntity entity = repo.getEntity();
		System.assertEquals(orgObj.Id, entity.id);
		System.assertEquals(orgObj.Name, entity.name);
		System.assertEquals(orgObj.Language_0__c, entity.language0.value);
		System.assertEquals(orgObj.Language_1__c, entity.language1.value);
		System.assertEquals(orgObj.Language_2__c, entity.language2.value);
	}

	/**
	 * saveEntityのテスト
	 * 保存できることを確認する
	 */
	@isTest static void saveEntityTest() {
		OrganizationSettingEntity entity = new OrganizationSettingEntity();
		entity.language0 = AppLanguage.JA;
		entity.language1 = AppLanguage.EN_US;
		entity.language2 = null;

		OrganizationSettingRepository repo = new OrganizationSettingRepository();

		// 新規作成
		Repository.SaveResult resultNew = repo.saveEntity(entity);
		System.assertEquals(true, resultNew.isSuccessAll);
		List<ComOrgSetting__c> objList = [SELECT Id, Name, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];
		System.assertEquals(1, objList.size());
		ComOrgSetting__c obj = objList[0];
		System.assertEquals(AppConverter.stringValue(entity.language0), obj.Language_0__c);
		System.assertEquals(AppConverter.stringValue(entity.language1), obj.Language_1__c);
		System.assertEquals(AppConverter.stringValue(entity.language2), obj.Language_2__c);

		// 更新
		entity.setId(obj.Id);
		entity.language0 = AppLanguage.EN_US;
		entity.language1 = null;
		entity.language2 = AppLanguage.JA;
		Repository.SaveResult resultUpdate = repo.saveEntity(entity);
		System.assertEquals(true, resultUpdate.isSuccessAll);
		objList = [SELECT Id, Name, Language_0__c, Language_1__c, Language_2__c FROM ComOrgSetting__c];
		obj = objList[0];
		System.assertEquals(AppConverter.stringValue(entity.language0), obj.Language_0__c);
		System.assertEquals(AppConverter.stringValue(entity.language1), obj.Language_1__c);
		System.assertEquals(AppConverter.stringValue(entity.language2), obj.Language_2__c);

	}

}
