/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 論理削除型マスタのエンティティ基底クラス
 */
public abstract with sharing class LogicalDeleteEntity extends Entity {

	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		IS_REMOVED
	}

	/** 値を変更した項目のリスト */
	private Set<Field> changedFieldSet = new Set<Field>();

	/** 削除フラグ */
	public Boolean isRemoved {
		get;
		set {
			isRemoved = value;
			setChanged(Field.IS_REMOVED);
		}
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		changedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.changedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public virtual void resetChanged() {
		this.changedFieldSet.clear();
		resetChangedInSubClass();
	}

	/** (親クラスを含まない)クラス内の項目変更情報をリセットする */
	protected abstract void resetChangedInSubClass();

	/**
	 * 指定された項目の値を取得する
	 * @param targetField 対象の項目
	 * @return 項目値
	 */
	public Object getFieldValue(LogicalDeleteEntity.Field targetField) {
		if (targetField == Field.IS_REMOVED) {
			return this.isRemoved;
		}
		// ここにきたらバグ
		throw new App.UnsupportedException('LogicalDeleteEntity.getFieldValue: 未対応の項目です。targetField='+ targetField);
	}

	/**
	 * 指定された項目に値を設定する
	 * @param targetField 対象の項目
	 * @pram value 設定値
	 */
	public void setFieldValue(LogicalDeleteEntity.Field targetField, Object value) {
		if (targetField == Field.IS_REMOVED) {
			this.isRemoved = (Boolean)value;
		} else {
			// ここにきたらバグ
			throw new App.UnsupportedException('LogicalDeleteEntity.setFieldValue: 未対応の項目です。targetField='+ targetField);
		}
	}
}