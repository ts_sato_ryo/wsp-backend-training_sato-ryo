/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 勤怠
 *
 * 勤務時間変更申請のサービスクラス
 * 当クラスはAttAttDailyRequestServiceからのみ実行する事を前提としています。
 * Resourceクラスなどから直接実行しないで下さい。
 */
public with sharing class AttPatternRequestService extends AttDailyRequestService.AbstractRequestProcessor {

	/** 申請可能な最大日数 */
	private static final Integer REQUEST_PERIOD_MAX = 31;

	private final AttDailyRequestRepository dailyRequestRepository = new AttDailyRequestRepository();

	/**
	 * 申請・再申請パラメータ
	 */
	public class SubmitParam extends AttDailyRequestService.SubmitParam {
		/** 勤務パターンコード */
		public String patternCode;
		/** 開始日 */
		public AppDate startDate;
		/** 終了日 */
		public AppDate endDate;
		/** 備考 */
		public String remarks;
	}

	/**
	 * 勤務時間変更申請の申請/再申請を行う。
	 *
	 * @param param 申請情報
	 * @return 作成した申請情報
	 */
	public override AttDailyRequestEntity submit(AttDailyRequestService.SubmitParam submitParam) {
		SubmitParam param = (SubmitParam)submitParam;

		AttDailyRequestService requestService = new AttDailyRequestService();

		// 申請期間のチェック
		// 申請期間が長すぎるとSOQLガバナ例外が発生してしまう可能性があるため
		if (param.startDate != null && param.endDate != null) {
			if (param.startDate.daysBetween(param.endDate) >= REQUEST_PERIOD_MAX) {
				// メッセージ：申請期間が長すぎます。申請期間は31日以内にしてください。
				throw new App.ParameterException(
						ComMessage.msg().Att_Err_RequesePeriodTooLong(new List<String>{REQUEST_PERIOD_MAX.format()}));
			}
		}

		// 再申請の場合は取下げを行う
		if (param.requestId != null) {
			super.remove(dailyRequestRepository.getEntity(param.requestId));
		}

		// 申請対象となる勤怠明細レコードとサマリーを取得する
		AttDailyRequestService.RecordsWithSummary recordsWithSummary = getRecordEntityList(param.empId, param.startDate, param.endDate);
		Map<Id, AttSummaryEntity> summaryMap = recordsWithSummary.summaryMap;
		List<AttRecordEntity> records = recordsWithSummary.records;

		// 勤務時間変更申請を使用可能か判定
		if (!isValidPatternRequest(summaryMap, records)) {
			// メッセージ：勤務体系により勤務時間変更申請が許可されていません。
			throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_PATTERN_REQUEST, ComMessage.msg().Att_Err_NotPermittedPatternRequest);
		}

		// 勤務日以外の日タイプを含む場合はエラー
		checkWorkDay(records, param);

		// 勤務パターンが申請期間中に有効であることを確認する
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(param.empId, param.startDate);
		AttPatternEntity pattern = new AttPatternRepository2().getPatternByCode(param.patternCode, employee.companyId);
		if (pattern == null) {
			// メッセージ：指定された勤務パターンが見つかりません。
			throw new App.IllegalStateException(
					App.ERR_CODE_NOT_FOUND_PATTERN,
					ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_WorkingPattern}));
		}
		if (!pattern.isValid(param.startDate) || (!pattern.isValid(param.endDate))) {
			// メッセージ：申請期間が勤務パターンの有効期間外です。
			throw new App.ParameterException(ComMessage.msg().Att_Err_OutOfPatternPeriod);
		}

		// 各日の勤務体系で許可されている勤務パターンであることを確認する
		Set<Id> isCheckedWokingTypeSet = new Set<Id>(); // チェック済みの勤務体系ID

		for (AttRecordEntity record : records) {
			AttSummaryEntity summary = summaryMap.get(record.summaryId);
			// 対象の勤務体系IDが既にチェック済みの場合はスキップする
			if (!isCheckedWokingTypeSet.contains(summary.workingTypeId)) {
				isCheckedWokingTypeSet.add(summary.workingTypeId);
				AttWorkingTypeBaseEntity workingType = requestService.getWorkingType(summary.workingTypeBaseId, summary.endDate);
				if (workingType == null) {
					// メッセージ：該当する勤務体系が見つかりません。
					throw new App.IllegalStateException(
						ComMessage.msg().Att_Err_NotFound(new List<String>{ComMessage.msg().Att_Lbl_WorkingType}));
				}
				List<AttDailyRequestService.AvailablePattern> avPatterns = requestService.getPatternList(workingType);
				if (!canSubmitPatternRequest(avPatterns, param.patternCode)) {
					// メッセージ：許可されていない申請です。
					throw new App.IllegalStateException(App.ERR_CODE_NOT_PERMITTID_REQUEST,
							ComMessage.msg().Att_Err_NotPermittedRequest);
				}
			}
		}

		// 申請エンティティを作成する
		AttDailyRequestEntity request = createPatternRequest(param, pattern);

		// 申請エンティティのチェック
		AttValidator.DailyRequestValidator requestValidator = new AttValidator.DailyRequestValidator(request);
		Validator.Result requestValidatorResult = requestValidator.validate();
		if (! requestValidatorResult.isSuccess()) {
			Validator.Error error = requestValidatorResult.getErrors()[0];
			throw new App.IllegalStateException(error.code, error.message);
		}

		// 申請を保存する
		Repository.SaveResult saveRes = new AttDailyRequestRepository().saveEntity(request);
		request.setId(saveRes.details[0].id);

		// 勤怠明細の勤務時間変更申請IDを更新する
		for (AttRecordEntity record : records) {
			// 勤怠明細のバリデーション
			Validator.Result validatorResult = validateSubmitPatternRequest(request, record, pattern, param.empId);
			if (!validatorResult.isSuccess()) {
				Validator.Error error = validatorResult.getErrors()[0];
				throw new App.IllegalStateException(error.code, error.message);
			}
			record.reqRequestingPatternRequestId = request.id;
		}

		new AttSummaryRepository().saveRecordEntityList(records);

		// 申請する(申請コメントなし)
		submitProcess(request, null);
		return request;
	}

	/**
	 * 勤務時間変更申請可能かどうかを確認する
	 * 申請開始日の場合、前日の勤務体系（勤務パターン）もチェック対象とする
	 * 申請終了日の場合、翌日の勤務体系（勤務パターン）もチェック対象とする
	 * @param request 申請対象の各種勤怠申請
	 * @param record 対象日の勤怠明細
	 * @param pattern 申請対象の勤務パターン
	 * @param empBaseId 申請対象の社員ベースID
	 * @return チェック結果
	 */
	private Validator.Result validateSubmitPatternRequest(
				AttDailyRequestEntity request, AttRecordEntity record, AttPatternEntity pattern, Id empBaseId) {

		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		EmployeeRepository empRepo = new EmployeeRepository();
		AttValidator.PatternSubmitValidator validator;
		AttPatternEntity patternBefore;
		AttPatternEntity patternAfter;

		// 申請開始日の場合、前日の勤務体系（勤務パターン）とのチェックを行う
		if (record.rcdDate == request.startDate) {
			// 前日の社員履歴を取得（社員履歴がない場合はチェックをしない）
			AppDate targetDate = record.rcdDate.addDays(-1);
			EmployeeBaseEntity employee = empRepo.getEntity(empBaseId, targetDate);
			EmployeeHistoryEntity empHistory = employee == null ? null : employee.getHistoryByDate(targetDate);
			if (empHistory != null) {
				// 前日の勤怠明細を取得
				List<AttRecordEntity> recordBeforeList = summaryRepo.searchRecordEntity(employee.id, targetDate);
				AttRecordEntity recordBefore = recordBeforeList.isEmpty() ? null : recordBeforeList[0];
				// 前日の勤務パターンを取得
				patternBefore = getPattern(recordBefore, empHistory.workingTypeId);
				// バリデーション
				validator = new AttValidator.PatternSubmitValidator(record, patternBefore, pattern, patternAfter);
				Validator.Result validatorResult = validator.validate();
				if (!validatorResult.isSuccess()) {
					return validatorResult;
				}
			}
		}

		// 申請終了日の場合、翌日の勤務体系（勤務パターン）とのチェックを行う
		if (record.rcdDate == request.endDate) {
			// 翌日の社員履歴を取得（社員履歴がない場合はチェックをしない）
			AppDate targetDate = record.rcdDate.addDays(1);
			EmployeeBaseEntity employee = empRepo.getEntity(empBaseId, targetDate);
			EmployeeHistoryEntity empHistory = employee == null ? null : employee.getHistoryByDate(targetDate);
			if (empHistory != null) {
				// 翌日の勤怠明細を取得
				List<AttRecordEntity> recordAfterList = summaryRepo.searchRecordEntity(employee.id, targetDate);
				AttRecordEntity recordAfter = recordAfterList.isEmpty() ? null : recordAfterList[0];
				// 翌日の勤務パターンを取得
				patternAfter = getPattern(recordAfter, empHistory.workingTypeId);
				// バリデーション
				validator = new AttValidator.PatternSubmitValidator(record, patternBefore, pattern, patternAfter);
				return validator.validate();
			}
		}

		// バリデーション
		validator = new AttValidator.PatternSubmitValidator(record, patternBefore, pattern, patternAfter);
		return validator.validate();
	}

	/**
	 * 対象日の勤務パターンを取得する
	 * @param record 対象日の勤怠明細
	 * @param workingTypeBaseId 対象日の勤務体系ベースID
	 * @return 勤務パターン（nullあり）
	 */
	@TestVisible
	private AttPatternEntity getPattern(AttRecordEntity record, Id workingTypeBaseId) {
		AttPatternRepository2 patternRepo = new AttPatternRepository2();

		if (record == null) {
			return null;
		}
		// 申請中の勤務パターンを取得する
		if (record.reqRequestingPatternRequestId != null) {
			AttDailyRequestEntity patternReqeust = new AttDailyRequestRepository().getEntity(record.reqRequestingPatternRequestId);
			return patternRepo.getPatternById(patternReqeust.attPatternId);
		}
		// 勤怠明細に適用されている勤務パターンを取得する
		if (record.outPatternId != null) {
			return patternRepo.getPatternById(record.outPatternId);
		}
		// 勤務体系を取得して勤務パターンに変換する
		AttWorkingTypeBaseEntity workingTypeBase = new AttWorkingTypeRepository().getEntity(workingTypeBaseId, record.rcdDate);
		return workingTypeBase == null ? null : workingTypeBase.getHistory(0).createPatternEntity();
	}

	/**
	 * 指定された勤務パターンコードの勤務パターンが申請可能かどうかをチェックする
	 * @param avLeaves 申請可能な勤務パターン一覧
	 * @param leaveCode 申請対象の勤務パターンコード
	 * @return 申請可能な場合はtrue, そうでない場合はfalse
	 */
	private Boolean canSubmitPatternRequest(
			List<AttDailyRequestService.AvailablePattern> avPatterns, String patternCode) {

		// 勤務パターンコードが含まれていなければ申請不可
		for (AttDailyRequestService.AvailablePattern avPattern : avPatterns) {
			if (avPattern.pattern.code == patternCode) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 勤務時間変更申請用の勤怠日次申請エンティティを作成する
	 * @param param 勤務時間変更申請パラメータ
	 * @param pattern 申請対象の勤務パターン
	 * @return 作成した日時申請エンティティ
	 */
	private AttDailyRequestEntity createPatternRequest(SubmitParam param, AttPatternEntity pattern) {

		// 社員を取得
		EmployeeBaseEntity employee = new EmployeeService().getEmployee(param.empId, param.startDate);
		AttDailyRequestEntity entity = new AttDailyRequestEntity();
		entity.setId(param.requestId);
		entity.name = createRequestName(param, employee, getCompanyLanguage(employee.companyId));
		entity.requestType = AttRequestType.PATTERN;
		entity.attPatternId = pattern.id;
		entity.startDate = param.startDate;
		entity.endDate = param.endDate;
		entity.remarks = param.remarks;
		entity.status = AppRequestStatus.PENDING;
		entity.cancelType = AppCancelType.NONE;
		entity.isConfirmationRequired = false;
		entity.requestTime = AppDatetime.now();
		setRequestCommInfo(entity, employee);
		return entity;
	}

	/**
	 * 勤務時間変更申請の申請名を作成する
	 * @param employee 社員
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @param lang 作成言語
	 * @return 申請名
	 */
	@testVisible
	private String createRequestName(SubmitParam param, EmployeeBaseEntity employee, AppLanguage lang) {
		// 日付フォーマット
		final AppDate.FormatType dateFormat = AppDate.FormatType.YYYYMMDD;

		// バインド文字列
		List<String> args = new List<String>{
			employee.displayNameL.getValue(lang),
			param.startDate.format(dateFormat),
			param.endDate.format(dateFormat)};

		// 申請種類名
		ComMsgBase targetLang = ComMessage.msg(lang.value);
		return lang == AppLanguage.JA ?
			String.format('{0}さん ' + targetLang.Att_Lbl_RequestTypePattern + targetLang.Att_Lbl_Request + ' {1}-{2}', args) :
			String.format('{0} ' + targetLang.Att_Lbl_RequestTypePattern + ' ' + targetLang.Att_Lbl_Request + ' {1}-{2}', args);
	}

	/**
	 * 勤務日以外の日タイプが含まれていないか判定する
	 * @param 判定対象の勤怠明細リスト
	 * @param 申請パラメータ
	 * @return 勤務日以外の場合はエラー
	 */
	@TestVisible
	private void checkWorkDay(List<AttRecordEntity> records, SubmitParam param) {
		for (AttRecordEntity record : records) {
			// 日タイプが勤務日(Workday)以外の場合はエラー
			if (record.outDayType != AttDayType.WORKDAY) {
				throw new App.IllegalStateException(App.ERR_CODE_ATT_INVALID_PATTERN_REQUEST,
						ComMessage.msg().Att_Err_TheDayIsNotWorkDay(new List<String>{record.rcdDate.format(AppDate.FormatType.YYYY_MM_DD_SLASH)}));
			}
		}
	}

	/**
	 * 指定の期間で勤務時間変更申請を使用可能か、勤務体系マスタから判定する。
	 * @param summaryMap 勤怠サマリのマップ（keyは勤怠サマリId）
	 * @param records 勤務明細のリスト
	 * @return 判定結果 期間全てにおいて、勤務時間変更が可能な場合true
	 */
	private Boolean isValidPatternRequest(Map<Id, AttSummaryEntity> summaryMap, List<AttRecordEntity> records) {
		// 勤務体系を取得
		AttWorkingTypeRepository.SearchFilter filter = new AttWorkingTypeRepository.SearchFilter();
		filter.baseIds = new Set<Id>();
		for (AttSummaryEntity summary : summaryMap.values()) {
			filter.baseIds.add(summary.workingTypeBaseId);
		}
		List<AttWorkingTypeBaseEntity> workingTypes = new AttWorkingTypeRepository().searchEntity(filter);
		// 申請日に紐づく勤務体系履歴を判定
		for (AttRecordEntity attRecord : records) {
			AttWorkingTypeHistoryEntity history = getWorkingTypeHistory(workingTypes, attRecord.rcdDate);
			if (history == null || !history.usePatternApply) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 指定した日付の勤務体系履歴を取得する。
	 * @param workingTypes 勤務体系ベース
	 * @param targetDate 対象日
	 * @param 指定日に対応する勤務体系履歴（存在しない場合はnull）
	 */
	private AttWorkingTypeHistoryEntity getWorkingTypeHistory(List<AttWorkingTypeBaseEntity> workingTypes, AppDate targetDate) {
		for (AttWorkingTypeBaseEntity base : workingTypes) {
			AttWorkingTypeHistoryEntity history = base.getHistoryByDate(targetDate);
			if (history != null) {
				return history;
			}
		}
		return null;
	}

	/**
	 * @description 承認取消時のメールを作成する
	 * @param request 申請
	 * @param applicant 申請者
	 * @param empLastModified 最終更新者
	 * @param targetLanguage 送信先ユーザの言語
	 * @return mailMessage メール内容
	 */
	public override MailService.MailParam createCancelMailMessage(AttDailyRequestEntity request, EmployeeBaseEntity applicant, EmployeeBaseEntity empLastModified, AppLanguage targetLanguage) {

		MailService.MailParam mailMessage = new MailService.MailParam();
		String language = targetLanguage.value;

		// 送信者名
		String senderDisplayName = empLastModified.fullNameL.getFullName(targetLanguage);
		mailMessage.senderDisplayName = senderDisplayName;

		// メールタイトル
		String mailSubject;
		// 申請種別を文字列に変換する
		String requestType = ComMessage.msg(language).Att_Lbl_RequestTypePattern;
		mailSubject = ComMessage.msg(language).Att_Msg_CancelMailSubject(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType});
		mailMessage.subject = mailSubject;

		// メール本文
		String mailBody;
		// 申請日付
		String requestDate = request.requestTime.formatLocal().substring(0, 10).replace('-', '/');
		// 勤務パターン名
		AttPatternEntity pattern = new AttPatternRepository2().getPatternById(request.attPatternId);
		String patternName = pattern.nameL.getValue(targetLanguage);
		// 開始日
		String startDate = String.valueOf(AppDate.convertDate(request.startDate)).replace('-', '/');
		// 終了日
		String endDate = String.valueOf(AppDate.convertDate(request.endDate)).replace('-', '/');
		// 始業終業時刻
		String startTime = pattern.startTime.format();
		String endTime = pattern.endTime.format();
		// 所定休憩
		String rest1StartTime = pattern.rest1StartTime == null ? '' : pattern.rest1StartTime.format();
		String rest1EndTime = pattern.rest1EndTime == null ? '' : pattern.rest1EndTime.format();
		String rest2StartTime = pattern.rest2StartTime == null ? '' : pattern.rest2StartTime.format();
		String rest2EndTime = pattern.rest2EndTime == null ? '' : pattern.rest2EndTime.format();
		String rest3StartTime = pattern.rest3StartTime == null ? '' : pattern.rest3StartTime.format();
		String rest3EndTime = pattern.rest3EndTime == null ? '' : pattern.rest3EndTime.format();
		String rest4StartTime = pattern.rest4StartTime == null ? '' : pattern.rest4StartTime.format();
		String rest4EndTime = pattern.rest4EndTime == null ? '' : pattern.rest4EndTime.format();
		String rest5StartTime = pattern.rest5StartTime == null ? '' : pattern.rest5StartTime.format();
		String rest5EndTime = pattern.rest5EndTime == null ? '' : pattern.rest5EndTime.format();
		// 備考
		String remarks = request.remarks;
		// メール本文を作成
		mailBody = ComMessage.msg(language).Att_Msg_CancelMailMessage(
					new List<String>{applicant.fullNameL.getFullName(targetLanguage), requestType, empLastModified.fullNameL.getFullName(targetLanguage)});
		mailBody += '\r\n\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_RequestContents + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestDate + '：' + requestDate + '\r\n';
		mailBody += ComMessage.msg(language).Appr_Lbl_RequestType + '：' + requestType + '\r\n';
		// 期間での申請の場合は、開始日と終了日を追加
		if (startDate.equals(endDate)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate;
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Period + '：' + startDate + '－' + endDate;
		}
		mailBody += '\r\n';
		mailBody += ComMessage.msg(language).Att_Lbl_WorkingPattern + '：' + patternName + '\r\n';
		mailBody += ComMessage.msg(language).Admin_Lbl_WorkingHours + '：' + startTime + '－' + endTime + '\r\n';
		if (String.isNotBlank(rest1StartTime)) {// マスタ管理画面でrest1EndTimeのみを保存できないので、開始日だけでチェックする
			mailBody += ComMessage.msg(language).Admin_Lbl_WorkingTypeRest1 + '：' + rest1StartTime + '－' + rest1EndTime + '\r\n';
		}
		if (String.isNotBlank(rest2StartTime)) {// マスタ管理画面でrest2EndTimeのみを保存できないので、開始日だけでチェックする
			mailBody += ComMessage.msg(language).Admin_Lbl_WorkingTypeRest2 + '：' + rest2StartTime + '－' + rest2EndTime + '\r\n';
		}
		if (String.isNotBlank(rest3StartTime)) {// マスタ管理画面でrest3EndTimeのみを保存できないので、開始日だけでチェックする
			mailBody += ComMessage.msg(language).Admin_Lbl_WorkingTypeRest3 + '：' + rest3StartTime + '－' + rest3EndTime + '\r\n';
		}
		if (String.isNotBlank(rest4StartTime)) {// マスタ管理画面でrest4EndTimeのみを保存できないので、開始日だけでチェックする
			mailBody += ComMessage.msg(language).Admin_Lbl_WorkingTypeRest4 + '：' + rest4StartTime + '－' + rest4EndTime + '\r\n';
		}
		if (String.isNotBlank(rest5StartTime)) {// マスタ管理画面でrest5EndTimeのみを保存できないので、開始日だけでチェックする
			mailBody += ComMessage.msg(language).Admin_Lbl_WorkingTypeRest5 + '：' + rest5StartTime + '－' + rest5EndTime + '\r\n';
		}
		if (String.isNotBlank(remarks)) {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + remarks + '\r\n';
		} else {
			mailBody += ComMessage.msg(language).Att_Lbl_Remarks + '：' + '\r\n';
		}
		// 署名
		mailBody += '\r\n---\r\n';
		mailBody += ComMessage.msg(language).Att_Msg_TeamSpirit;
		mailMessage.body = mailBody;

		return mailMessage;
	}
}