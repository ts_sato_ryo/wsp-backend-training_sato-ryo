/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Test for ExpJournalBatch and ExpJournalBatchSchedule
 *
 * @group Expense
 */
@isTest
private class ExpJournalBatchAndScheduleTest {

	/*
	 * Test that it can be scheduled
	 */
	@isTest static void schedulePositiveTest() {
		Test.StartTest();
		ExpJournalBatchSchedule schedule = new ExpJournalBatchSchedule();
		Id scheduleId = System.schedule('Export To Journal Schedule Test', '0 0 0 * * ?', schedule);
		Test.StopTest();

		System.assertNotEquals(null, scheduleId);
	}

	/*
	 * Test that the Batch can be started correctly and the Journal No is correct.
	 * The verifying of each field is already tested in {@code ExpJournalService} class.
	 */
	@isTest static void batchPositiveTest() {
		ExpTestData testData = new ExpTestData();
		// Set Manager
		EmployeeBaseEntity manager = testData.createTestEmployees(1)[0];
		testData.employee.getHistory(0).managerId = manager.id;
		new EmployeeRepository().saveEntity(testData.employee);

		// Create Report and Record, and Record Items
		List<ExpReportEntity> reportEntityList = testData.createExpReports(Date.today(), testData.department, testData.employee, testData.job, testData.costCenter.CurrentHistoryId__c, 2);
		for (ExpReportEntity reportEntity : reportEntityList) {
			testData.createExpRecords(reportEntity.id, Date.today(), testData.expType,
					testData.employee, testData.costCenter.CurrentHistoryId__c,1);
		}

		//Submit Report
		ExpReportRequestService service = new ExpReportRequestService();
		service.isTestModeAndBypassExpReportValidation = true;
		List<Id> reportIdList = new List<Id>();
		for (ExpReportEntity reportEntity : reportEntityList) {
			service.submitRequest(reportEntity.id, '');
			reportIdList.add(reportEntity.id);
		}

		ExpReportRequestRepository requestRepo = new ExpReportRequestRepository();

		reportEntityList = new ExpReportRepository().getEntityList(reportIdList);
		List<Id> expReportRequestIds = new List<Id>();
		for (ExpReportEntity reportEntity: reportEntityList) {
			expReportRequestIds.add(reportEntity.expRequestId);
		}

		// Set the Authorized Date and Last Approved Time
		List<ExpReportRequestEntity> rReportRequestEntityList = requestRepo.getEntityList(expReportRequestIds);
		for (ExpReportRequestEntity reportRequestEntity : rReportRequestEntityList) {
			reportRequestEntity.authorizedTime = AppDatetime.now();
			reportRequestEntity.lastApproveTime = AppDatetime.now();
		}
		requestRepo.saveEntityList(rReportRequestEntityList);

		Test.StartTest();
		ExpJournalBatch batch = new ExpJournalBatch();
		Id jobId = Database.executeBatch(batch);
		Test.StopTest();

		System.assertNotEquals(null, jobId);
		List<ExpJournal__c> retJournalList = [SELECT Id, JournalNo__c FROM ExpJournal__c ORDER BY JournalNo__c ASC NULLS LAST ];
		System.assertEquals(2, retJournalList.size());
	}
}