/**
 * 勤務インポートバッチリポジトリ
 */
public with sharing class AttImportBatchRepository extends Repository.BaseRepository {
	/** 取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_BATCH_FIELD_SET;
	static {
		GET_BATCH_FIELD_SET = new Set<Schema.SObjectField> {
				AttImportBatch__c.Id,
				AttImportBatch__c.Name,
				AttImportBatch__c.CompanyId__c,
				AttImportBatch__c.ActorHistoryId__c,
				AttImportBatch__c.DepartmentHistoryId__c,
				AttImportBatch__c.Comment__c,
				AttImportBatch__c.ImportTime__c,
				AttImportBatch__c.Status__c,
				AttImportBatch__c.Type__c,
				AttImportBatch__c.Count__c,
				AttImportBatch__c.SuccessCount__c,
				AttImportBatch__c.FailureCount__c
		};
	}

	/** 検索フィルタ (検索API実行用) */
	public class SearchFilter {
		public Id companyId;
		public List<Id> batchIdList;
	}

	/**
	 * @description 指定したバッチIDのインポートバッチを取得する
	 * @param batchId 指定する会社Id
	 * @return 指定会社のインポートバッチ一覧
	 */
	public AttImportBatchEntity getEntity(Id batchId) {
		SearchFilter filter = new SearchFilter();
		filter.batchIdList = new List<Id>{batchId};
		List<AttImportBatchEntity> entityList = searchEntityList(filter, false);
		return entityList.isEmpty() ? null : entityList[0];
	}
	/**
	 * @description 指定したバッチIDのインポートバッチを取得する
	 * @param batchId 指定する会社Id
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 指定会社のインポートバッチ一覧
	 */
	public AttImportBatchEntity getEntity(Id batchId, Boolean forUpdate) {
		SearchFilter filter = new SearchFilter();
		filter.batchIdList = new List<Id>{batchId};
		List<AttImportBatchEntity> entityList = searchEntityList(filter, forUpdate);
		return entityList.isEmpty() ? null : entityList[0];
	}
	/**
	 * 指定会社のインポートバッチを取得する
	 * @param companyId 指定する会社Id
	 * @return 指定会社のインポートバッチ一覧
	 */
	public List<AttImportBatchEntity> getImportBatchList(Id companyId) {
		SearchFilter filter = new SearchFilter();
		filter.companyId = companyId;

		return searchEntityList(filter, false);
	}

	/**
	 * 勤怠インポートバッチを検索する
	 * @param filter 検索条件
	 * @param forUpdate 更新用で取得する場合はtrue(ロックされる)
	 * @return 条件に一致したンポートバッチ一覧
	 */
	private List<AttImportBatchEntity> searchEntityList(
			SearchFilter filter, Boolean forUpdate) {

		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_BATCH_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}

		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id companyId = filter.companyId;
		final List<Id> batchIdList = filter.batchIdList;

		if (companyId != null) {
			condList.add(getFieldName(AttImportBatch__c.CompanyId__c) + ' = :companyId');
		}
		if (batchIdList != null) {
			condList.add(getFieldName(AttImportBatch__c.Id) + ' IN :batchIdList');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ')
				+ ' FROM ' + AttImportBatch__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (!forUpdate) {
			soql += ' ORDER BY ' + getFieldName(AttImportBatch__c.ImportTime__c) + ' Desc ';
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// クエリ実行
		//System.debug('AttImportBatchRepository.searchEntityList: SOQL==>' + soql);
		List<AttImportBatch__c> sObjList = Database.query(soql);

		// 結果からエンティティを作成する
		List<AttImportBatchEntity> entityList = new List<AttImportBatchEntity>();
		for (AttImportBatch__c attPattern : sObjList) {
			entityList.add(new AttImportBatchEntity(attPattern));
		}

		return entityList;
	}

	/**
	 * 勤怠インポートバッチを検索する (検索API用)
	 * @param filter 検索条件
	 * @return 条件に一致した勤怠インポートバッチ一覧(指定順)
	 */
	public List<AttImportBatchEntity> searchEntityList(SearchFilter filter){
		return searchEntityList(filter, false);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntity(AttImportBatchEntity entity) {
		return saveEntityList(new List<AttImportBatchEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public Repository.SaveResult saveEntityList(List<AttImportBatchEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(ベースと履歴レコードの結果)
	 */
	public Repository.SaveResult saveEntityList(List<AttImportBatchEntity> entityList, Boolean allOrNone) {

		List<SObject> saveObjectList = createObjectList(entityList);
		List<Database.UpsertResult> resList = AppDatabase.doUpsert(saveObjectList, allOrNone);
		Repository.SaveResult result = resultFactory.createSaveResult(resList);

		return result;
	}
	/**
	 * エンティティから保存用のオブジェクトを作成する
	 * @param entityList 保存対象のエンティティリスト
	 * @return 保存用のオブジェクトリスト
	 */
	private List<SObject> createObjectList(List<AttImportBatchEntity> entityList) {
		List<AttImportBatch__c> objectList = new List<AttImportBatch__c>();
		for (AttImportBatchEntity entity : entityList) {
			objectList.add(entity.createSObject());
		}
		return objectList;
	}
}