/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 勤怠
  *
  * 勤怠サマリーのエンティティ
  */
public with sharing class AttSummaryEntity extends AttSummaryGeneratedEntity {

	/** 申請情報の値オブジェクト*/
	public class Request extends ValueObject {
		/** ステータス */
		public AppRequestStatus status {public get; private set;}
		/** 取消種別 */
		public AppCancelType cancelType {public get; private set;}
		/** 要確認フラグ */
		public Boolean isConfirmationRequired {public get; private set;}

		/** 承認者01Id */
		public Id approver01Id {public get; private set;}

		public Request(AppRequestStatus status, AppCancelType cancelType,
				Boolean isConfirmationRequired, Id approver01Id) {
			this.status = status;
			this.cancelType = cancelType;
			this.isConfirmationRequired = isConfirmationRequired;
			this.approver01Id = approver01Id;
		}
		@testVisible
		private Request(AppRequestStatus status, AppCancelType cancelType,
				Boolean isConfirmationRequired) {
			this.status = status;
			this.cancelType = cancelType;
			this.isConfirmationRequired = isConfirmationRequired;
		}
		/**
 		 * オブジェクトの値が等しいかどうか判定する
 		 * @param compare 比較対象のオブジェクト
 		 */
 		public override Boolean equals(Object compare) {
			Boolean ret;
			if (compare instanceof Request) {
				Request compareRequest = (Request)compare;
				if ((this.status != compareRequest.status)
						|| (this.cancelType != compareRequest.cancelType)
						|| (this.isConfirmationRequired != compareRequest.isConfirmationRequired)) {
					ret = false;
				} else {
					ret = true;
				}
			} else {
				ret = false;
			}
			return ret;
		}

	}

	/** 社員ベースID （参照のみ)*/
	public Id employeeBaseId {
		get {
			if (employeeBaseId == null) {
				employeeBaseId = sObj.EmployeeHistoryId__r.BaseId__c;
			}
			return employeeBaseId;
		}
		set;
	}
	/** 社員コード（参照のみ。値を変更してもDBに保存されない。） */
	public String employeeCode {
		get {
			if (employeeCode == null) {
				employeeCode = sObj.EmployeeHistoryId__r.BaseId__r.Code__c;
			}
			return employeeCode;
		}
		set;
	}
	/** 社員名（参照のみ。値を変更してもDBに保存されない。） */
	public String employeeName {get {return employeeNameL.getFullName();}}
	public AppPersonName employeeNameL {
		get {
			if (employeeNameL == null) {
				employeeNameL = new AppPersonName(
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L0__c, sObj.EmployeeHistoryId__r.BaseId__r.LastName_L0__c,
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L1__c, sObj.EmployeeHistoryId__r.BaseId__r.LastName_L1__c,
						sObj.EmployeeHistoryId__r.BaseId__r.FirstName_L2__c, sObj.EmployeeHistoryId__r.BaseId__r.LastName_L2__c);
			}
			return employeeNameL;
		}
		set;
	}

	/** 勤務確定申請情報 */
	public AttSummaryEntity.Request request {
		get {
			if (request != null) {
				return request;
			}
			return new AttSummaryEntity.Request(
				AppRequestStatus.valueOf(sObj.RequestId__r.Status__c),
				AppCancelType.valueOf(sObj.RequestId__r.CancelType__c),
				sObj.RequestId__r.ConfirmationRequired__c,
				sObj.RequestId__r.Approver01Id__c);
		}
		set;
	}

	/** 勤怠明細リスト */
	public List<AttRecordEntity> attRecordList {get; private set;}

	/**
	 * 勤怠明細リストをリプレースする
	 * （リストをコピーします）
	 * @param attRecordList リプレース対象の勤怠明細リスト
	 */
	public void replaceAttRecordList(List<AttRecordEntity> attRecordList) {
		this.attRecordList = new List<AttRecordEntity>(attRecordList);
	}

	/** 無休取得時間 */
	public AttDailyTime outAbsenceTime { get; set; }

	/** 勤務体系ベースID （参照のみ)*/
	public Id workingTypeBaseId {
		get {
			if (workingTypeBaseId == null) {
				workingTypeBaseId = sObj.WorkingTypeHistoryId__r.BaseId__c;
			}
			return workingTypeBaseId;
		}
		set;
	}

	/** 勤務体系名（参照のみ。保存時に本項目は保存されない。） */
	public String workingTypeName {get {return AppMultiString.stringValue(workingTypeNameL);}}
	public AppMultiString workingTypeNameL {
		get {
			if (workingTypeNameL == null) {
				workingTypeNameL = new AppMultiString(
						sObj.WorkingTypeHistoryId__r.Name_L0__c,
						sObj.WorkingTypeHistoryId__r.Name_L1__c,
						sObj.WorkingTypeHistoryId__r.Name_L2__c);
			}
			return workingTypeNameL;
		}
		set;
	}

	/** 勤務帯系所定勤務時間 (参照のみ)*/
	public AttDailyTime contractedWorkHours {
		get {
			if (contractedWorkHours != null) {
				return contractedWorkHours;
			}
			return AttDailyTime.valueOf(sobj.WorkingTypeHistoryId__r.ContractedWorkHours__c);
		}
		set;
	}


	/** 所定勤務時間 (参照のみ)*/
	/** 部署名（参照のみ。値の設定はリポジトリのみで行う） */
	public String deptName {get {return AppMultiString.stringValue(deptNameL);}}
	public AppMultiString deptNameL {
		get {
			if (deptNameL != null) {
				return deptNameL;
			}
			return new AppMultiString(sobj.DeptHistoryId__r.Name_L0__c, sobj.DeptHistoryId__r.Name_L1__c, sobj.DeptHistoryId__r.Name_L2__c);
		}
		set;
	}

	/**
	 * コンストラクタ
	 */
	public AttSummaryEntity() {
		super(new AttSummary__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttSummaryEntity(AttSummary__c sobj) {
		super(sobj);
	}

	/**
	 * 勤怠記録名を作成する(社員コード+社員名+開始日(YYYYMMDD)-終了日(YYYYMMDD))
	 * @param employeeCode 社員コード
	 * @param employeeName 社員名
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @return 勤怠記録名
	 */
	public static String createName(
			String employeeCode, String employeeName, AppDate startDate, AppDate endDate) {
		final Integer empLength = 63; // 社員情報の文字列長さ

		if (String.isBlank(employeeCode)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeCode is blank');
		}
		if (String.isBlank(employeeName)) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('employeeName is blank');
		}
		if (startDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('startDate is null');
		}
		if (endDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('endDate is null');
		}

		return (employeeCode + employeeName).left(empLength)
				+ startDate.formatYYYYMMDD() + '-' + endDate.formatYYYYMMDD();
	}

	/**
	 * 期間名を作成する(月度の場合はYYYYMM)
	 * @param monthMark 月度の表記のフラグ
	 * @param startDate サマリーの開始日
	 * @param endDate サマリーの終了日
	 * @return 期間名
	 */
	public static String createSummaryName(
			AttWorkingTypeBaseGeneratedEntity.MarkType monthMark, AppDate startDate, AppDate endDate) {

		if (startDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('startDate is null');
		}
		if (endDate == null) {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('endDate is null');
		}

		// 期間名作成
		String sumName;
		if (monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase) {
			// 開始日の月で作成
			sumName = startDate.format(AppDate.FormatType.YYYYMM);
		} else if (monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.EndBase) {
			// 終了日の月で作成
			sumName = endDate.format(AppDate.FormatType.YYYYMM);
		} else {
			// サーバ側の実装ミスのためエラーメッセージの多言語不要
			throw new App.ParameterException('monthMark', monthMark);
		}

		return sumName;
	}

	/**
	 * 申請の詳細ステータスを取得する
	 */
	public AttRequestEntity.DetailStatus getRequestDetailStatus() {

		AttRequestEntity reqEntity = new AttRequestEntity();
		reqEntity.setId(this.requestId);
		if (this.request != null) {
			reqEntity.status = this.request.status;
			reqEntity.cancelType = this.request.cancelType;
		}

		return reqEntity.getDetailStatus();
	}

	/**
	 * 指定した日付の勤怠明細を、サマリー内に保持しているか判定する。
	 * 勤怠明細を日付の昇順で、且つ、隙間なく連続して保持している場合のみ、このメソッドは使用できる。
	 * @param targetDate 判定対象日
	 * @return 指定した日付の勤怠明細が存在する場合true
	 */
	public Boolean hasAttRecord(AppDate targetDate) {
		if (attRecordList == null || attRecordList.isEmpty()) {
			return false;
		}
		AppDate min = attRecordList[0].rcdDate;
		AppDate max = attRecordList[attRecordList.size() - 1].rcdDate;
		return !targetDate.isBefore(min) && !targetDate.isAfter(max);
	}

	/**
	 * 日付を指定して、サマリー内の勤怠明細を取得する
	 * @param targetDate 取得対象日
	 * @return 指定した日付の勤怠明細（存在しない場合はnull）
	 */
	public AttRecordEntity getAttRecord(AppDate targetDate) {
		for (AttRecordEntity record : attRecordList) {
			if (record.rcdDate.equals(targetDate)) {
				return record;
			}
		}
		return null;
	}

	/**
	 * 勤怠明細の全ての期間が休職休業中か判定する。
	 * @return 全ての期間が休職休業中の場合true
	 */
	public Boolean isAllAbsent() {
		for (AttRecordEntity record : attRecordList) {
			if (record.leaveOfAbsenceId == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 勤怠明細で短時間勤務が適用されている日があるかを判定する。
	 * @return 1日でも短時間勤務が適用されている場合true
	 */
	public Boolean isShortWorkTimeEvenOneDay() {
		for (AttRecordEntity record : attRecordList) {
			if (record.calShortTimeWorkSettingHistoryId != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 勤怠明細の日付のリストを返却する
	 * @return 勤怠明細の日付のリスト
	 */
	public List<AppDate> getDateList() {
		List<AppDate> dateList = new List<AppDate>();
		for (AttRecordEntity record : attRecordList) {
			dateList.add(record.rcdDate);
		}
		return dateList;
	}
}