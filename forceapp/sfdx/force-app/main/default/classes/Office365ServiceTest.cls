/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description Office365Serviceのテストクラス
*/
@isTest
private class Office365ServiceTest {

	/**
	 * テストデータ
	 */
	private class TestData {

		public ComCompany__c createTestData() {
			CompanyPrivateSettingRepository repository = new CompanyPrivateSettingRepository();
			ComCompany__c testCompany = ComTestDataUtility.createCompany('testCompany', ComTestDataUtility.createCountry('ja').Id);
			CompanyPrivateSettingEntity entity = new CompanyPrivateSettingEntity();
			entity.companyId = testCompany.Id;
			entity.office365AuthCode = 'AuthCode';
			repository.saveEntity(entity);
			return testCompany;
		}
	}

	/**
	 * getOAuthEndpointUrlのテスト
	 * OAuth認証URLが正常に作成されることを検証する
	 */
	@isTest
	static void getOAuthEndpointUrlTest() {

		// 実行
		String resultUrl = Office365Service.getOAuthEndpointUrl();

		// 検証
		System.assertEquals('https://login.microsoftonline.com/common/adminconsent?client_id=eee03ef3-dbef-44e8-8357-006a5e7b07f0&redirect_uri=https%3A%2F%2Foauth.teamspirit.co.jp%2Fwsp%2FOffice365OAuthRedirectPage.html', resultUrl);
	}

	/**
	 * generateDateTimeStringのテスト
	 * 時刻を正常に変換できることを検証する
	 */
	@isTest
	static void generateDateTimeStringTest() {

		Datetime targetDatetime = Datetime.newInstance(2018, 1, 1);

		// 実行
		String dateTimeStr = Office365Service.generateDateTimeString(targetDatetime);
		dateTimeStr = dateTimeStr.substring(0, 19).replace('T', ' ');
		Datetime convertedDateTime = Datetime.valueOfGmt(dateTimeStr);

		// 検証
		System.assertEquals(targetDateTime, convertedDateTime);
	}

	/**
	 * getCalendarApiUrlのテスト
	 * 予定取得API実行URLを正常に作成できることを検証する
	 */
	@isTest
	static void getCalendarApiUrlTest() {

		// タイムゾーンがJSTのユーザで実行する
		ComCompany__c testCompany = new TestData().createTestData();
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		User user = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');

		String resultUrl;
		String office365Account = 'testAccount@test.com';
		System.runAs(user) {
			Datetime targetDateFrom = Datetime.newInstance(2019, 1, 1, 0, 0, 0);
			Datetime targetDateTo = Datetime.newInstance(2019, 1, 2, 0, 0, 0);

			// 実行
			resultUrl = Office365Service.getCalendarApiUrl(office365Account, targetDateFrom, targetDateTo);
		}

		// 検証
		String expectedUrl = 'https://graph.microsoft.com/v1.0/users/'
												+ EncodingUtil.urlEncode(office365Account, 'UTF-8')
												+ '/calendarview'
												+ '?StartDateTime=' + EncodingUtil.urlEncode('2018-12-31T15:00:00Z', 'UTF-8')
												+ '&EndDateTime=' + EncodingUtil.urlEncode('2019-01-01T15:00:00Z', 'UTF-8')
												+ '&$select=' + EncodingUtil.urlEncode('id,Subject,Start,End,IsAllDay', 'UTF-8')
												+ '&$orderby=' + EncodingUtil.urlEncode('Start/Datetime,End/Datetime', 'UTF-8')
												+ '&$top=250'
												+ '&$filter=' + EncodingUtil.urlEncode('Sensitivity ne \'Private\'', 'UTF-8');

		System.assertEquals(expectedUrl, resultUrl);
	}

	/**
	 * checkAuthのテスト
	 * 認証結果がAUTHORIZEDであることを検証する
	 */
	@isTest
	static void checkAuthTestAuthrized() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'body' => '{"value":[]}',
				'status' => 'OK',
				'statuscode' => '200'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		String result = Office365Service.checkAuth(testCompany.Id);
		Test.stopTest();

		// 検証
		System.assertEquals('AUTHORIZED', result);
	}

	/**
	 * checkAuthのテスト
	 * 認証結果がUNAUTHORIZEDであることを検証する
	 */
	@isTest
	static void checkAuthTestUnAuthrized() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'body' => '{"value":[]}',
				'status' => 'Unauthorized',
				'statuscode' => '401'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		String result = Office365Service.checkAuth(testCompany.Id);
		Test.stopTest();

		// 検証
		System.assertEquals('UNAUTHORIZED', result);
	}

	/**
	 * checkAuthのテスト
	 * 認証結果がREMOTE_SITE_SETTINGS_INACTIVEであることを検証する
	 */
	@isTest
	static void checkAuthTestRemoteSiteSettingInactive() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'exception' => 'Unauthorized endpoint, please check Setup->Security->Remote site settings.'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		String result = Office365Service.checkAuth(testCompany.Id);
		Test.stopTest();

		// 検証
		System.assertEquals('REMOTE_SITE_SETTINGS_INACTIVE', result);
	}

	/**
	 * checkAuthのテスト
	 * 認証結果がAPI_CONNECTION_FAILEDであることを検証する
	 */
	@isTest
	static void checkAuthTestApiConnectionFailed() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'exception' => 'Read Timed out'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		String result = Office365Service.checkAuth(testCompany.Id);
		Test.stopTest();

		// 検証
		System.assertEquals('API_CONNECTION_FAILED', result);
	}

	/**
	 * getOffice365Eventsのテスト
	 * 正常にイベントを取得できることを検証する
	 */
	@isTest
	static void getOffice365EventsTestGetEvents() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);

		// タイムゾーンがJSTのユーザで実行する
		System.runAs(testUser) {
			AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
			AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

			// レスポンス作成
			List<Map<String, String>> responseList = new List<Map<String, String>> {
				new Map<String, String> {
					'urlprefix' => 'https://login.microsoftonline.com/',
					'method' => 'POST',
					'body' => '{"access_token":"ACCESSTOKEN"}',
					'status' => 'OK',
					'statuscode' => '200'
				},
				new Map<String, String> {
					'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
					'method' => 'GET',
					'body' => '{' +
						'    "value": [' +
						'        {' +
						'            "id": "AAMkAGNhNzczYzEwLTlhN2ItNGFkZi05YWE4LThjZDFhMmI2Y2YxOABGAAAAAABJvYfEGFnQQYBxE2PXzGdiBwDnEuU-bGAgTLtcyL-C8lyMAAAAAAENAADnEuU-bGAgTLtcyL-C8lyMAACi6tLhAAA=",' +
						'            "subject": "テスト1",' +
						'            "isAllDay": false,' +
						'            "start": {' +
						'                "dateTime": "2018-10-05T10:25:00.0000000",' +
						'                "timeZone": "UTC"' +
						'            },' +
						'            "end": {' +
						'                "dateTime": "2018-10-05T11:35:00.0000000",' +
						'                "timeZone": "UTC"' +
						'            }' +
						'        },' +
						'        {'  +
						'            "id": "AAMkAGNhNzczYzEwLTlhN2ItNGFkZi05YWE4LThjZDFhMmI2Y2YxOABGAAAAAABJvYfEGFnQQYBxE2PXzGdiBwDnEuU-bGAgTLtcyL-C8lyMAAAAAAENAADnEuU-bGAgTLtcyL-C8lyMAACi6tLhAAA=",' +
						'            "subject": "テスト2",' +
						'            "isAllDay": false,' +
						'            "start": {' +
						'                "dateTime": "2018-10-05T13:30:00.0000000",' +
						'                "timeZone": "UTC"' +
						'            },' +
						'            "end": {' +
						'                "dateTime": "2018-10-05T14:30:00.0000000",' +
						'                "timeZone": "UTC"' +
						'            }' +
						'        },' +
						'        {'  +
						'            "id": "AAMkAGNhNzczYzEwLTlhN2ItNGFkZi05YWE4LThjZDFhMmI2Y2YxOABGAAAAAABJvYfEGFnQQYBxE2PXzGdiBwDnEuU-bGAgTLtcyL-C8lyMAAAAAAENAADnEuU-bGAgTLtcyL-C8lyMAACi6tLhAAA=",' +
						'            "subject": "テスト3",' +
						'            "isAllDay": true,' +
						'            "start": {' +
						'                "dateTime": "2018-10-05T00:00:00.0000000",' +
						'                "timeZone": "UTC"' +
						'            },' +
						'            "end": {' +
						'                "dateTime": "2018-10-06T0:00:00.0000000",' +
						'                "timeZone": "UTC"' +
						'            }' +
						'        }' +
						'    ]' +
						'}',
						'status' => 'OK',
						'statuscode' => '200'
				}
			};

			// コールアウトモックを設定
			Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

			// 実行
			Test.startTest();
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			Test.stopTest();

			// 検証
			System.assertEquals(3, resultList.size());

			// 1件目
			System.assertEquals('テスト1', resultList[0].subject);
			System.assert(!resultList[0].isAllDay);
			System.assertEquals(new AppDatetime(DateTime.newInstance(2018, 10, 05, 19, 25, 0)), resultList[0].startDateTime);
			System.assertEquals(new AppDatetime(DateTime.newInstance(2018, 10, 05, 20, 35, 0)), resultList[0].endDateTime);
			System.assertEquals(PlanEventEntity.ServiceType.office365, resultList[0].createdServiceBy);

			// 2件目
			System.assertEquals('テスト2', resultList[1].subject);
			System.assert(!resultList[1].isAllDay);
			System.assertEquals(new AppDatetime(DateTime.newInstance(2018, 10, 05, 22, 30, 0)), resultList[1].startDateTime);
			System.assertEquals(new AppDatetime(DateTime.newInstance(2018, 10, 05, 23, 30, 0)), resultList[1].endDateTime);
			System.assertEquals(PlanEventEntity.ServiceType.office365, resultList[1].createdServiceBy);

			// 3件目
			System.assertEquals('テスト3', resultList[2].subject);
			System.assert(resultList[2].isAllDay);
			System.assertEquals(new AppDatetime(DateTime.newInstance(2018, 10, 05, 09, 00, 0)), resultList[2].startDateTime);
			System.assertEquals(new AppDatetime(DateTime.newInstance(2018, 10, 05, 09, 00, 0)), resultList[2].endDateTime);
			System.assertEquals(PlanEventEntity.ServiceType.office365, resultList[2].createdServiceBy);
		}
	}

	/**
	 * getOffice365Eventsのテスト
	 * 認証コードが存在しない場合にエラーになることを検証する
	 */
	@isTest
	static void getOffice365EventsTestNoAuthSetting() {

		CompanyPrivateSettingRepository repo = new CompanyPrivateSettingRepository();

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);
		CompanyPrivateSettingEntity cpsEntity = repo.getEntity(testCompany.Id);
		cpsEntity.office365AuthCode = null;
		repo.saveEntity(cpsEntity);

		AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
		AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

		// 実行
		Test.startTest();
		try {
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Cal_Msg_NoAuthSetting, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * getOffice365Eventsのテスト
	 * リモートサイトが有効化されていない場合にエラーになることを検証する
	 */
	@isTest
	static void getOffice365EventsTestRemoteSiteSettingsInActive() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);
		AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
		AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'exception' => 'Unauthorized endpoint, please check Setup->Security->Remote site settings.'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		try {
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Cal_Msg_RemoteSiteSettingsInActive, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * getOffice365Eventsのテスト
	 * 外部カレンダーへの接続に失敗した場合にエラーになることを検証する
	 */
	@isTest
	static void getOffice365EventsTestApiConnectionFailed() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);
		AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
		AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'exception' => 'Read Timed out'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		try {
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Cal_Msg_ApiConnectionFailed, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * getOffice365Eventsのテスト
	 * 認証を失敗した場合にエラーになることを検証する
	 */
	@isTest
	static void getOffice365EventsTestUnAuthorized() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);
		AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
		AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'body' => '{"value":[]}',
				'status' => 'Unauthorized',
				'statuscode' => '401'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		try {
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Cal_Msg_UnAuthorized, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * getOffice365Eventsのテスト
	 * 応答ステータスがその他の場合にエラーになることを検証する
	 */
	@isTest
	static void getOffice365EventsTestOtherStatus() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);
		AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
		AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'body' => '{"value":[]}',
				'status' => 'Bad Request',
				'statuscode' => '400'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		try {
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Cal_Msg_ApiConnectionFailed, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * getOffice365Eventsのテスト
	 * Bodyが想定外の値だった場合にエラーになることを検証する
	 */
	@isTest
	static void getOffice365EventsTestInvalidBody() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);
		AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
		AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'body' => '{}',
				'status' => 'OK',
				'statuscode' => '200'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		try {
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Cal_Msg_ApiConnectionFailed, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * getOffice365Eventsのテスト
	 * Valueの値がNULLの場合にエラーになることを検証する
	 */
	@isTest
	static void getOffice365EventsTestValueNull() {

		// データ登録
		ComCompany__c testCompany = new TestData().createTestData();
		User testUser = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		ComPermission__c testPermission = ComTestDataUtility.createPermission('Test Permission', testCompany.Id);
		ComDeptBase__c testDept = ComTestDataUtility.createDepartmentWithHistory('testDept', testCompany.Id);
		ComEmpBase__c testEmp = ComTestDataUtility.createEmployeeWithHistory('testEmp', testCompany.Id, testDept.Id, testUser.Id, testPermission.Id);
		AppDate targetDateFrom = AppDate.newInstance(2018, 10, 5);
		AppDate targetDateTo = AppDate.newInstance(2018, 10, 5);

		// レスポンス作成
		List<Map<String, String>> responseList = new List<Map<String, String>> {
			new Map<String, String> {
				'urlprefix' => 'https://login.microsoftonline.com/',
				'method' => 'POST',
				'body' => '{"access_token":"ACCESSTOKEN"}',
				'status' => 'OK',
				'statuscode' => '200'
			},
			new Map<String, String> {
				'urlprefix' => 'https://graph.microsoft.com/v1.0/users/',
				'method' => 'GET',
				'body' => '{"value":}',
				'status' => 'OK',
				'statuscode' => '200'
			}
		};

		// コールアウトモックを設定
		Test.setMock(HttpCalloutMock.class, new Office365CalloutMock(responseList));

		// 実行
		Test.startTest();
		try {
			List<PlanEventEntity> resultList = Office365Service.getOffice365Events(testUser.Id, targetDateFrom, targetDateTo);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Cal_Msg_ApiConnectionFailed, e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * isEffeciveEventのテスト
	 * Office365から取得した予定のうち、表示対象外とする終日予定を正しく判定出来ることを検証する
	 */
	@isTest
	static void isEffectiveAllDayEventTest() {
		// 前日の終日予定 → 表示対象外
		Office365Service.Office365Event event1 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T00:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-01T00:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => true
		});
		// 前日 + 当日の終日予定 → 表示対象
		Office365Service.Office365Event event2 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T00:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-02T00:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => true
		});
		// 当日の終日予定 → 表示対象
		Office365Service.Office365Event event3 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-04-01T00:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-02T00:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => true
		});
		// 当日 + 翌日の終日予定 → 表示対象
		Office365Service.Office365Event event4 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-04-01T00:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-03T00:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => true
		});

		// タイムゾーンがJSTのユーザで実行する
		ComCompany__c testCompany = new TestData().createTestData();
		User user = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		System.runAs(user) {
			Datetime searchFrom = Datetime.newInstance(2019, 4, 1, 00, 00, 00);
			Datetime searchTo = Datetime.newInstance(2019, 4, 1, 23, 59, 00);
			System.assertEquals(false, Office365Service.isEffectiveEvent(searchFrom, searchTo, event1));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event2));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event3));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event4));
		}
	}

	/**
	 * isEffeciveEventのテスト
	 * JSTの境界値の予定を正しく判定できることを検証する
	 */
	@isTest
	static void isEffectiveJstBoundaryTest() {
		// 開始時刻の境界値
		// 前日23:00 〜 当日0:00の予定 → 表示対象外
		Office365Service.Office365Event event1 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T14:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-03-31T15:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});
		// 前日23:30 〜 当日0:30の予定 → 表示対象
		Office365Service.Office365Event event2 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T14:30:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-03-31T15:30:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});
		// 当日0:00 〜 当日1:00の予定 → 表示対象
		Office365Service.Office365Event event3 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T15:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-03-31T16:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});

		// 終了時刻の境界値
		// 当日23:30 〜 翌日0:00の予定 → 表示対象
		Office365Service.Office365Event event4 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-04-01T14:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-01T15:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});
		// 当日23:30 〜 翌日0:30の予定 → 表示対象
		Office365Service.Office365Event event5 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-04-01T14:30:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-01T15:30:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});

		// 開始時刻と終了時刻が同一の場合
		// 当日0:00 〜 当日0:00の予定 → 表示対象
		Office365Service.Office365Event event6 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T15:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-03-31T15:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});

		// タイムゾーンがJSTのユーザで実行する
		ComCompany__c testCompany = new TestData().createTestData();
		User user = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		System.runAs(user) {
			Datetime searchFrom = Datetime.newInstance(2019, 4, 1, 00, 00, 00);
			Datetime searchTo = Datetime.newInstance(2019, 4, 1, 23, 59, 00);
			System.assertEquals(false, Office365Service.isEffectiveEvent(searchFrom, searchTo, event1));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event2));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event3));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event4));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event5));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event6));
		}
	}

	/**
	 * isEffeciveEventのテスト
	 * UTCの境界値の予定を正しく判定し、表示対象の予定を除外しないことを検証する
	 */
	@isTest
	static void isEffectiveUtcBoundaryTest() {
		// 当日8:00 〜 当日9:00の予定 → 表示対象
		Office365Service.Office365Event event1 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T23:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-01T00:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});
		// 当日8:30 〜 当日9:30の予定 → 表示対象
		Office365Service.Office365Event event2 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-03-31T23:30:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-01T00:30:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});
		// 当日9:00 〜 当日10:00の予定 → 表示対象
		Office365Service.Office365Event event3 = new Office365Service.Office365Event(new Map<String, Object>{
			'start' => new Map<String, String>{'dateTime' => '2019-04-01T00:00:00.0000000', 'timeZone' => 'UTC'},
			'end' => new Map<String, String>{'dateTime' => '2019-04-01T01:00:00.0000000', 'timeZone' => 'UTC'},
			'isAllDay' => false
		});

		// タイムゾーンがJSTのユーザで実行する
		ComCompany__c testCompany = new TestData().createTestData();
		User user = ComTestDataUtility.createUser('testUser', 'ja', 'ja_JP');
		System.runAs(user) {
			Datetime searchFrom = Datetime.newInstance(2019, 4, 1, 00, 00, 00);
			Datetime searchTo = Datetime.newInstance(2019, 4, 1, 23, 59, 00);
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event1));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event2));
			System.assertEquals(true, Office365Service.isEffectiveEvent(searchFrom, searchTo, event3));
		}
	}
}