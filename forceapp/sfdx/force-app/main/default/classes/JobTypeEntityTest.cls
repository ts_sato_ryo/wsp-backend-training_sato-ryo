/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description JobTypeEntityのテストクラス
 */
@isTest
private class JobTypeEntityTest {

	/**
	 * ユニークキーが正しく作成できることを確認する
	 */
	@isTest static void createUniqKeyTest() {
		JobTypeEntity entity = new JobTypeEntity();

		// 会社コード、コードが空でない場合
		// ユニークキーが作成される
		String companyCode = 'CompanyCode';
		entity.code = 'JobTypeCode';
		System.assertEquals('CompanyCode-JobTypeCode', entity.createUniqKey(companyCode));
	}

	/*
	 * ユニークキー作成テスト
	 * 会社コードが空の場合、例外が発生することを確認する
	 */
	@isTest static void createUniqKeyTestWhenCompanyCodeIsBrankThenThrough() {
		JobTypeEntity entity = new JobTypeEntity();
		String companyCode = '';
		entity.code = 'JobTypeCode';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.ParameterException e) {
			// OK
		}
	}

	/*
	 * ユニークキー作成テスト
	 * タグコードが空の場合、例外が発生することを確認する
	 */
	@isTest static void createUniqKeyTestWhenTagCodeIsBrankThenThrough() {
		JobTypeEntity entity = new JobTypeEntity();
		String companyCode = 'CompanyCode';
		entity.code = '';
		try {
			entity.createUniqKey(companyCode);
			TestUtil.fail('例外が発生しませんでした。');
		} catch(App.IllegalStateException e) {
			// OK
		}
	}

	/*
	 * プロパティの変更判定を確認する
	 */
	@isTest static void setChangedTest() {
		JobTypeEntity entity = new JobTypeEntity();
		entity.nameL0 = 'ジョブタイプ L0';
		entity.code = 'Test';

		// 更新した項目を確認
		System.assertEquals(true, entity.isChanged(JobTypeEntity.Field.NAME_L0));
		System.assertEquals(true, entity.isChanged(JobTypeEntity.Field.CODE));

		// 更新していない項目を確認
		System.assertEquals(false, entity.isChanged(JobTypeEntity.Field.NAME_L1));
		System.assertEquals(false, entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED));
	}

	/*
	 * resetChangedのテスト
	 * 変更情報がリセットされていることを確認する
	 */
	@isTest static void resetChangedTest() {
		JobTypeEntity entity = new JobTypeEntity();
		entity.nameL0 = 'Test';
		entity.isRemoved = true;

		// 更新した項目を確認
		System.assertEquals(true, entity.isChanged(JobTypeEntity.Field.NAME_L0));
		System.assertEquals(true, entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED));

		entity.resetChanged();

		// 変更情報がリセットされていることを確認する
		System.assertEquals(false, entity.isChanged(JobTypeEntity.Field.NAME_L1));
		System.assertEquals(false, entity.isChanged(LogicalDeleteEntity.Field.IS_REMOVED));
	}
}