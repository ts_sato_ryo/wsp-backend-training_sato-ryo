/*
 * @author TeamSpirit Inc.
 * 
 * @date 2018
 * 
 * @description Test class for ExpRequestApprovalEntity
 * 
 * @group Expense
 */
@IsTest
private class ExpRequestApprovalEntityTest {

	/*
	 * Test the Detail Status Retrieval Method of ExpRequestApprovalEntity.
	 */
	@IsTest static void testGetDetailStatus() {
		ExpRequestApprovalEntity noIdEntity = new ExpRequestApprovalEntity();

		ExpRequestApprovalEntity pendingEntity = new ExpRequestApprovalEntity();
		pendingEntity.setId(UserInfo.getUserId());
		pendingEntity.status = AppRequestStatus.PENDING;

		ExpRequestApprovalEntity approvedEntity = new ExpRequestApprovalEntity();
		approvedEntity.setId(UserInfo.getUserId());
		approvedEntity.status = AppRequestStatus.APPROVED;
		approvedEntity.claimed = false;

		ExpRequestApprovalEntity approvedAndClaimedEntity = new ExpRequestApprovalEntity();
		approvedAndClaimedEntity.setId(UserInfo.getUserId());
		approvedAndClaimedEntity.status = AppRequestStatus.APPROVED;
		approvedAndClaimedEntity.claimed = true;

		ExpRequestApprovalEntity rejectedEntity = new ExpRequestApprovalEntity();
		rejectedEntity.setId(UserInfo.getUserId());
		rejectedEntity.status = AppRequestStatus.DISABLED;
		rejectedEntity.cancelType = AppCancelType.REJECTED;

		ExpRequestApprovalEntity removedEntity = new ExpRequestApprovalEntity();
		removedEntity.setId(UserInfo.getUserId());
		removedEntity.status = AppRequestStatus.DISABLED;
		removedEntity.cancelType = AppCancelType.REMOVED;

		ExpRequestApprovalEntity cancelledEntity = new ExpRequestApprovalEntity();
		cancelledEntity.setId(UserInfo.getUserId());
		cancelledEntity.status = AppRequestStatus.DISABLED;
		cancelledEntity.cancelType = AppCancelType.CANCELED;

		ExpRequestApprovalEntity illegalStateEntity = new ExpRequestApprovalEntity();
		illegalStateEntity.setId(UserInfo.getUserId());
		illegalStateEntity.status = AppRequestStatus.DISABLED;
		illegalStateEntity.cancelType = AppCancelType.NONE;

		ExpRequestApprovalEntity statusNotSetEntity = new ExpRequestApprovalEntity();
		statusNotSetEntity.setId(UserInfo.getUserId());

		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.NOT_REQUESTED, noIdEntity.getDetailStatus());
		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.PENDING, pendingEntity.getDetailStatus());
		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.APPROVED, approvedEntity.getDetailStatus());
		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.CLAIMED, approvedAndClaimedEntity.getDetailStatus());
		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.REJECTED, rejectedEntity.getDetailStatus());
		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.REMOVED, removedEntity.getDetailStatus());
		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.CANCELED, cancelledEntity.getDetailStatus());
		System.assertEquals(ExpRequestApprovalEntity.DetailStatus.NOT_REQUESTED, statusNotSetEntity.getDetailStatus());

		Exception expectedException;
		try {
			illegalStateEntity.getDetailStatus();
		} catch (Exception e) {
			expectedException = e;
		}
		System.assertNotEquals(null, expectedException);
		System.assertEquals(true, expectedException instanceof App.IllegalStateException);
	}
}