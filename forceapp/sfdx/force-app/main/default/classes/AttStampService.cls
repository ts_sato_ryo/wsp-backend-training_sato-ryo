/**
 * @group 勤怠
 *
 * @description 勤怠打刻サービス
 */
public with sharing class AttStampService implements Service {

	public static final Integer MINUTES_OF_DAY = 24 * 60;
	// 打刻種別
	public static final String CLOCK_TYPE_IN = 'in'; // 出勤打刻
	public static final String CLOCK_TYPE_OUT = 'out'; // 退勤打刻
	public static final String CLOCK_TYPE_REIN = 'rein'; // 再出勤打刻

	// 出退勤状態種別
	private static final String STAMP_TYPE_NONE = 'none';
	private static final String STAMP_TYPE_INPUT = 'input';
	private static final String STAMP_TYPE_STAMP = 'stamp';

	/**
	 * @description 指定時刻に打刻状況情報
	 */
	public class StampableStatus {
		/** 出勤打刻可能対象日 */
		public AppDate stampInDate {get; set;}
		/** 退勤打刻可能対象日 */
		public AppDate stampOutDate {get; set;}
		/** 再出勤打刻可能対象日 */
		public AppDate stampReInDate {get; set;}
		/** 出勤打刻可能フラグ */
		public Boolean isStampInOk {get; set;}
		/** 退勤打刻可能フラグ */
		public Boolean isStampOutOk {get; set;}
		/** 再出勤打刻可能フラグ */
		public Boolean isStampReInOk {get; set;}

		public StampableStatus() {
			this.isStampInOk = false;
			this.isStampOutOk = false;
			this.isStampReInOk = false;
		}
	}

	/** 社員サービス */
	private EmployeeService empService = new EmployeeService();

	/**
	 * @description 指定時刻の打刻状態を取得する
	 * @param emp 対象社員
	 * @param clockDatetime 打刻時刻
	 * @return チェック結果
	 */
	public StampableStatus getStampableStatus(Id empId, AppDateTime clockDateTime) {
		AttSummaryRepository summaryRep = new AttSummaryRepository();
		// 打刻日時を基準日として、前後二日間の勤怠明細を取得
		AppDate targetDate = AppDate.valueOf(clockDateTime.getDate());
		AppDate preDate = targetDate.addDays(-1);
		AppDate nextDate = targetDate.addDays(1);
		AttRecordEntity preRecordData = null;
		AttRecordEntity targetRecordData = null;
		AttRecordEntity nextRecordData = null;
		Id prePatternId = null;
		Id targetPatternId = null;
		Id nextPatternId = null;
		Map<AppDate, AttRecordEntity> dateRecordMap = new Map<AppDate, AttRecordEntity>();
		for (AttRecordEntity recordData : summaryRep.searchRecordEntityList(empId, preDate, nextDate)) {
			dateRecordMap.put(recordData.rcdDate, recordData);
			if (recordData.rcdDate.equals(preDate)) {
				preRecordData = recordData;
				prePatternId = recordData.outPatternId;
			}
			else if (recordData.rcdDate.equals(nextDate)) {
				nextRecordData = recordData;
				nextPatternId = recordData.outPatternId;
			}
			else if (recordData.rcdDate.equals(targetDate)) {
				targetRecordData = recordData;
				targetPatternId = recordData.outPatternId;
			}
		}
		// 勤務パターンを取得する
		AttPatternEntity prePattern = null;
		AttPatternEntity targetPattern = null;
		AttPatternEntity nextPattern = null;
		Set<Id> patternIds = new Set<Id>{prePatternId, targetPatternId, nextPatternId};
		patternIds.remove(null);
		if (!patternIds.isEmpty()) {
			for (AttPatternEntity patternData :
					new AttPatternRepository2().getPatternList(new Set<Id>{prePatternId, targetPatternId, nextPatternId})) {
				if (patternData.id == prePatternId) {
					prePattern = patternData;
				}
				if (patternData.id == targetPatternId) {
					targetPattern = patternData;
				}
				if (patternData.id == nextPatternId) {
					nextPattern = patternData;
				}
			}
		}
		// 勤務パターンがない場合、勤務体系を取得して、勤務パターンに変換
		EmployeeService employeeService = new EmployeeService();
		AttWorkingTypeService workingTypeService = new AttWorkingTypeService();
		if (prePatternId == null) {
			try {
				EmployeeBaseEntity empBase = employeeService.getEmployee(empId, preDate);
				Id preWorkingTypeId = empBase.getHistory(0).workingTypeId;
				AttWorkingTypeBaseEntity workingTypeBase = workingTypeService.getWorkingType(preWorkingTypeId, preDate);
				prePattern = workingTypeBase.getHistory(0).createPatternEntity();
			} catch (Exception e) {}
		}
		if (targetPatternId == null) {
			try {
				EmployeeBaseEntity empBase = employeeService.getEmployee(empId, targetDate);
				Id targetWorkingTypeId = empBase.getHistory(0).workingTypeId;
				AttWorkingTypeBaseEntity workingTypeBase = workingTypeService.getWorkingType(targetWorkingTypeId, targetDate);
				targetPattern = workingTypeBase.getHistory(0).createPatternEntity();
			} catch (Exception e) {}
		}
		if (nextPatternId == null) {
			try {
				EmployeeBaseEntity empBase = employeeService.getEmployee(empId, nextDate);
				Id nextWorkingTypeId = empBase.getHistory(0).workingTypeId;
				AttWorkingTypeBaseEntity workingTypeBase = workingTypeService.getWorkingType(nextWorkingTypeId, nextDate);
				nextPattern = workingTypeBase.getHistory(0).createPatternEntity();
			} catch (Exception e) {}
		}
		// 勤務パターンにより、打刻対象日を計算する
		AttTime stampTime = AttTime.valueOf(clockDateTime.getTime());
		AppDate stampInDate = calcStampInDate(targetDate, stampTime, prePattern, targetPattern);
		AppDate stampOutDate = calcStampOutDate(targetDate, stampTime, prePattern, targetPattern, targetRecordData);
		AppDate stampReInDate = stampOutDate; // 再出勤対象日=退勤対象日

		// 打刻時刻
		AttTime stampInTime = AttTime.valueOf(clockDateTime.getTime());
		AttTime stampOutTime = AttTime.valueOf(clockDateTime.getTime());
		AttTime stampReInTime = AttTime.valueOf(clockDateTime.getTime());

		StampableStatus retResult = new StampableStatus();
		retResult.stampInDate = stampInDate;
		retResult.stampOutDate = stampOutDate;
		retResult.stampReInDate = stampReInDate;
		// 出勤打刻のチェック
		try {
			if (clockDateTime.getDate() > stampInDate.getDate()) {
				stampInTime.addMinutes(24 * 60);
			}
			else if (clockDateTime.getDate() < stampInDate.getDate()) {
				stampInTime.addMinutes(-24 * 60);
			}
			preRecordData = dateRecordMap.get(stampInDate.addDays(-1));
			targetRecordData = dateRecordMap.get(stampInDate);
			nextRecordData = dateRecordMap.get(stampInDate.addDays(1));
			validateClock(preRecordData, targetRecordData, nextRecordData, stampInTime, CLOCK_TYPE_IN);
			retResult.isStampInOk = true;
		}
		catch (Exception e) {}

		// 退勤打刻のチェック
		try {
			if (clockDateTime.getDate() > stampOutDate.getDate()) {
				stampOutTime.addMinutes(24 * 60);
			}
			else if (clockDateTime.getDate() < stampOutDate.getDate()) {
				stampOutTime.addMinutes(-24 * 60);
			}
			preRecordData = dateRecordMap.get(stampOutDate.addDays(-1));
			targetRecordData = dateRecordMap.get(stampOutDate);
			nextRecordData = dateRecordMap.get(stampOutDate.addDays(1));
			validateClock(preRecordData, targetRecordData, nextRecordData, stampOutTime, CLOCK_TYPE_OUT);
			retResult.isStampOutOk = true;
		}
		catch (Exception e) {}

		// 再出勤打刻のチェック
		try {
			if (clockDateTime.getDate() > stampReInDate.getDate()) {
				stampReInTime.addMinutes(24 * 60);
			}
			else if (clockDateTime.getDate() < stampReInDate.getDate()) {
				stampReInTime.addMinutes(-24 * 60);
			}
			preRecordData = dateRecordMap.get(stampReInDate.addDays(-1));
			targetRecordData = dateRecordMap.get(stampReInDate);
			nextRecordData = dateRecordMap.get(stampReInDate.addDays(1));
			validateClock(preRecordData, targetRecordData, nextRecordData, stampReInTime, CLOCK_TYPE_REIN);
			retResult.isStampReInOK = true;
		}
		catch (Exception e) {}

		return retResult;
	}

	/**
	 * @description 打刻対象日を取得
	 * @param empId 社員Id
	 * @param clockDateTime 打刻日時
	 * @param clockType 打刻区分(CLOCK_TYPE_XX)
	 * @return 打刻対象日付
	 */
	public AppDate getStampTargetDate(Id empId, AppDateTime clockDateTime, String clockType) {
		StampableStatus stampStatus = getStampableStatus(empId, clockDateTime);

		return clockType == CLOCK_TYPE_IN
				? stampStatus.stampInDate
				: (clockType == CLOCK_TYPE_OUT
					? stampStatus.stampOutDate
					: stampStatus.stampReInDate);
	}

	/**
	 * @description 出勤打刻対象日判定
	 */
	private AppDate calcStampInDate(AppDate targetDate, AttTime stampAttTime, AttPatternEntity prePattern, AttPatternEntity targetPattern) {
		// 今日の出勤境界時刻
		Integer targetBoundaryStartTime = ensureBoundaryTime(adjustTimeIn24(targetPattern.boundaryOfStartTime));
		// 実境界時刻
		Integer realBoundaryTime;
		if (prePattern != null) {
			// 前日出社境界時刻
			Integer preBoundaryStartTime = ensureBoundaryTime(adjustTimeIn24(prePattern.boundaryOfStartTime));
			// 本日と全日の境界時刻が早い時刻を境界時刻にする
			realBoundaryTime = Math.min(targetBoundaryStartTime, preBoundaryStartTime);
			// 前日の終業時刻がある場合
			if (prePattern.endTime != null) {
				Integer preEndTime = AppConverter.intValue(prePattern.endTime) - MINUTES_OF_DAY;
				// 前日の終業時間より後の時刻にする
				realBoundaryTime = Math.max(preEndTime, realBoundaryTime);
			}
		} else {
			realBoundaryTime = targetBoundaryStartTime;
		}
		Integer stampTime = AppConverter.intValue(stampAttTime);
		// 境界時刻より前(本日の出勤が優先)は前日とする
		return stampTime < realBoundaryTime ? targetDate.addDays(-1) : targetDate;
	}

	/**
	 * @description 退勤打刻対象日判定
	 */
	private AppDate calcStampOutDate(AppDate targetDate, AttTime stampAttTime, AttPatternEntity prePattern, AttPatternEntity targetPattern, AttRecordEntity targetRecord) {
		// 今日の退勤境界時刻
		Integer targetBoundaryEndTime = ensureBoundaryTime(adjustTimeIn24(targetPattern.boundaryOfEndTime));
		// 実境界時刻
		Integer realBoundaryTime;
		if (prePattern != null) {
			// 前日の退勤境界時刻
			Integer preBoundaryEndTime = ensureBoundaryTime(adjustTimeIn24(prePattern.boundaryOfEndTime));
			// 本日と前日の境界時刻が遅い時刻を実境界時刻とする
			realBoundaryTime = Math.max(targetBoundaryEndTime, preBoundaryEndTime);
		} else {
			realBoundaryTime = targetBoundaryEndTime;
		}

		// 本日の始業時刻がある場合
		if (targetPattern.startTime != null) {
			Integer targetStartTime = adjustTimeIn24(targetPattern.startTime);
			// 本日の始業時間より前の時刻にする
			realBoundaryTime = Math.min(targetStartTime, realBoundaryTime);
		}
		// 本日が出勤した場合、出勤時刻を境界時刻とする
		if (targetRecord != null && targetRecord.inpStartTime != null) {
			Integer targetInputStartTime = adjustTimeIn24(targetRecord.inpStartTime);
			realBoundaryTime = Math.min(realBoundaryTime, targetInputStartTime);
		}
		Integer stampTime = AppConverter.intValue(stampAttTime);
		// 境界時刻以前(前日の退勤が優先)は前日とする
		return stampTime <= realBoundaryTime ? targetDate.addDays(-1) : targetDate;
	}

	/**
	 * @description 打刻情報保存用のパラメータ
	 */
	public class StampAttTimeParam {
		/** 社員ベース */
		public EmployeeBaseEntity empBaseEntity;
		/** 打刻対象日 */
		public AppDate targetDate;
		/** 打刻時間 */
		public AppDateTime clockDateTime;
		/** 打刻種別 */
		public String clockType;
		/** コメント */
		public String comment;
		/** 位置情報 */
		public Location location;
		/** 打刻元 */
		public LocationSource source;
	}

	/**
	 * @description 指定日の出退勤打刻を行う
	 * @param employeeId 社員ID
	 * @param targetDate 打刻対象日
	 * @param clockTime 打刻時刻
	 * @param clockType 打刻種別
	 * @param comment コメント
	 * @return 当日
	 */
	public void stampAttTime(AttStampService.StampAttTimeParam param) {
		system.debug(LoggingLevel.DEBUG, '--- AttAttendanceService.stampAttTime start');
		system.debug(LoggingLevel.DEBUG, '--- targetDate is ' + param.targetDate.getDate());
		system.debug(LoggingLevel.DEBUG, '--- clockDateTime is ' + param.clockDateTime.getDate() + ' ' + param.clockDateTime.getTime());

		// 打刻時刻
		AttTime clockTime = AttTime.valueOf(param.clockDateTime.getTime());
		if (param.clockDateTime.getDate() > param.targetDate.getDate()) {
			clockTime.addMinutes(MINUTES_OF_DAY);
		}
		// 前日、対象日、翌日の勤怠明細を取得(ない場合、新規作成)
		AttSummaryRepository summaryRep = new AttSummaryRepository();

		AttRecordEntity preRecordData = null;
		AttRecordEntity targetRecordData = null;
		AttRecordEntity nextRecordData = null;

		for (AttRecordEntity recordData : summaryRep.searchRecordEntityList(param.empBaseEntity.id, param.targetDate.addDays(-1), param.targetDate.addDays(1))) {
			if (recordData.rcdDate.equals(param.targetDate.addDays(-1))) {
				preRecordData = recordData;
			}
			else if (recordData.rcdDate.equals(param.targetDate.addDays(1))) {
				nextRecordData = recordData;
			}
			else {
				targetRecordData = recordData;
			}
		}

		// 打刻対象がない場合、サマリーを新規作成する
		if (targetRecordData == null) {
			AttSummaryEntity newSummaryData = new AttAttendanceService().getSummary(param.empBaseEntity.id, param.targetDate);
			for (AttRecordEntity recordData : summaryRep.searchRecordEntity(param.empBaseEntity.id, param.targetDate)) {
				targetRecordData = recordData;
			}
		}
		// 打刻対象のサマリーは再計算必要な場合、再計算する
		else if (targetRecordData.isSummaryDirty) {
			new AttCalcService().calcAttendance(targetRecordData.summaryId);
			targetRecordData = summaryRep.getRecordEntity(targetRecordData.id);
		}

		// 位置情報必須でコメントがない場合、打刻不可
		CompanyEntity company = new CompanyRepository().getEntity(param.empBaseEntity.companyId);
		if (LocationSource.Mobile.equals(param.source)
				&& company.requireLocationAtMobileStamp
				&& param.location == null
				&& String.isBlank(param.comment)) {
			throw new App.IllegalStateException(
						App.ERR_CODE_ATT_STAMP_FAILED,
						ComMessage.msg().Att_Err_RequireCommentWithoutLocation);
		}
		validateClock(preRecordData, targetRecordData, nextRecordData, clockTime, param.clockType);
		// 出勤打刻の場合
		if (param.clockType == CLOCK_TYPE_IN) {
			targetRecordData.inpStartTime = clockTime;
			targetRecordData.inpStartStampTime = clockTime;
			Id locationId = saveLocation(param, targetRecordData);
			targetRecordData.inpClockInDetailId = locationId;
		}
		// 退勤打刻の場合
		else if (param.clockType == CLOCK_TYPE_OUT) {
			targetRecordData.inpEndTime = clockTime;
			targetRecordData.inpEndStampTime = clockTime;
			Id locationId = saveLocation(param, targetRecordData);
			targetRecordData.inpClockOutDetailId = locationId;
		}
		// 再出勤打刻の場合
		else {
			// 休憩時間をマージ
			AttCalcRangeDto newRest = AttCalcRangesDto.R(targetRecordData.inpEndTime.getIntValue(), clockTime.getIntValue()); // 退勤時間から再出勤打刻時間まで
			AttCalcRangesDto rests = toRestRanges(targetRecordData);
			rests.insertAndMerge(newRest);
			if (5 < rests.size()) {
				throw new App.IllegalStateException(
						App.ERR_CODE_ATT_RESTART_STAMP_OVER_REST_LIMIT,
						ComMessage.msg().Att_Err_RestartStampOverRestLimit);
			}
			setRestsToEntity(rests, targetRecordData);
			saveLocation(param, targetRecordData);
			// 退勤情報をクリア
			targetRecordData.inpEndTime = null;
			targetRecordData.inpEndStampTime = null;
			targetRecordData.inpClockOutDetailId = null;
		}
		targetRecordData.inpLastUpdateTime = AppDateTime.now();

		summaryRep.saveRecordEntity(targetRecordData);

		// 打刻後の勤怠計算を行う
		new AttCalcService().calcAttendance(targetRecordData.summaryId);
		system.debug(LoggingLevel.DEBUG, '--- AttAttendanceService.stampAttTime end');
	}

	/*
	 * 位置情報を1件作成して保存する
	 * 再出勤打刻の場合、退勤打刻時の位置情報(打刻情報)を修正または作成する
	 * @param param 位置情報
	 * @param targetRecordData 勤怠明細レコード
	 * @return 保存した位置情報ID
	 */
	private Id saveLocation(AttStampService.StampAttTimeParam param, AttRecordEntity targetRecordData) {

		// 位置情報を作成する
		ComLocationRepository locationRepo = new ComLocationRepository();
		ComLocationEntity locationEntity = new ComLocationEntity();
		locationEntity.name = createLocationName(param.empBaseEntity.name, param.clockDateTime);
		locationEntity.employeeBaseId = param.empBaseEntity.id;
		locationEntity.rcdType = LocationType.ATT_STAMP;
		locationEntity.attRecordId = targetRecordData.id;
		locationEntity.rcdDateTime = param.clockDateTime;
		locationEntity.rcdLocation = param.location;
		locationEntity.comment = param.comment;
		locationEntity.source = param.source;

		// 出勤打刻, 退勤打刻
		if (param.clockType == CLOCK_TYPE_IN || param.clockType == CLOCK_TYPE_OUT) {
			locationEntity.rcdAttStampType = param.clockType == CLOCK_TYPE_IN
					? AttStampType.CLOCK_IN
					: AttStampType.CLOCK_OUT;
			// 位置情報を保存する
			Repository.SaveResult saveRes = locationRepo.saveEntity(locationEntity);
			return saveRes.details[0].Id;
		}
		// 再出勤打刻
		else {
			locationEntity.rcdAttStampType = AttStampType.REST_END;
			ComLocationEntity clockOutLocationEntity = new ComLocationEntity();
			clockOutLocationEntity.rcdType = LocationType.ATT_STAMP;

			// 退勤位置情報がある場合、種別を変更する
			if (targetRecordData.inpClockOutDetailId != null) {
				clockOutLocationEntity = locationRepo.getEntity(targetRecordData.inpClockOutDetailId);
				clockOutLocationEntity.rcdAttStampType = AttStampType.REST_START;
			// 退勤位置情報がない場合、ダミーの位置情報を作成する
			} else {
				AppDateTime convertedClockDateTime = convertClockDateTime(targetRecordData.inpEndTime, param.targetDate);
				clockOutLocationEntity.name = createLocationName(param.empBaseEntity.name, convertedClockDateTime);
				clockOutLocationEntity.employeeBaseId = param.empBaseEntity.id;
				clockOutLocationEntity.attRecordId = targetRecordData.id;
				clockOutLocationEntity.rcdDateTime = convertedClockDateTime;
				clockOutLocationEntity.rcdLocation = null;
				clockOutLocationEntity.comment = null;
				clockOutLocationEntity.source = null;
				clockOutLocationEntity.rcdAttStampType = AttStampType.REST_START;
			}
			// 位置情報を保存する
			Repository.SaveResult saveRes = locationRepo.saveEntityList(
					new List<ComLocationEntity>{ locationEntity, clockOutLocationEntity });
			return saveRes.details[0].Id;
		}
	}

	/**
	 * 位置情報名称を作成する
	 * @param empName 社員名
	 * @param clockDateTime 日付
	 */
	@TestVisible
	private static String createLocationName(String empName, AppDateTime clockDateTime) {
		String locationName = empName + ' ' + clockDateTime.formatLocal();
		if (locationName.length() > 80) {
			Integer i = locationName.length() - 80;
			locationName = empName.left(empName.length() - i) + ' ' + clockDateTime.formatLocal();
		}
		return locationName;
	}

	/**
	 * 勤怠時刻と勤怠日付から勤怠日時に変換する
	 * @param inpAttTime 勤怠時刻
	 * @param inpAppDate 勤怠日付
	 * @return 日時
	 */
	@TestVisible
	private static AppDateTime convertClockDateTime(AttTime inpAttTime, AppDate inpAppDate) {
		Integer hour = inpAttTime.getHour();
		Integer minute = inpAttTime.getminute();
		Date dt = AppDate.convertDate(inpAppDate);
		// 翌日の場合
		if (hour >= 24) {
			dt = dt.addDays(1);
			hour -= 24;
		}
		return new AppDateTime(DateTime.newInstance(dt, Time.newInstance(hour, minute, 0, 0)));
	}

	// 打刻可否バリデーションエラー入口
	private void validateClock(AttRecordEntity preRecordData, AttRecordEntity targetRecordData, AttRecordEntity nextRecordData, AttTime clockTime, String clockType) {
		if (targetRecordData != null) {
			if (targetRecordData.isLocked) {
				throw new ComNormalException(ComMessage.msg().Att_Err_Locked(new List<String>{ComMessage.msg().Att_Lbl_Record}));
			}
			if (targetRecordData.isSummaryLocked) {
				throw new ComNormalException(ComMessage.msg().Att_Err_Locked(new List<String>{ComMessage.msg().Att_Lbl_TimeSheet}));
			}
			// 直行直帰申請承認後、打刻不可
			if (targetRecordData.inpDirectRequestId != null) {
				throw new App.IllegalStateException(
							App.ERR_CODE_ATT_STAMP_FAILED,
							ComMessage.msg().Att_Err_StampOnDirectApproved);
			}
		}
		if (clockType == CLOCK_TYPE_IN) {
			validateClockIn(preRecordData, targetRecordData, clockTime);
		}
		else if (clockType == CLOCK_TYPE_OUT) {
			validateClockOut(nextRecordData, targetRecordData, clockTime);
		}
		else if (clockType == CLOCK_TYPE_REIN) {
			validateClockReIn(nextRecordData, targetRecordData, clockTime);
		}
	}

	/**
	 * 出勤打刻チェック
	 * @param preRecordData 前日明細
	 * @param targetRecordData 打刻明細
	 * @param clockTime 打刻時刻
	 */
	@testVisible
	private void validateClockIn(AttRecordEntity preRecordData, AttRecordEntity targetRecordData, AttTime clockTime) {
		if (targetRecordData != null) {
			String startStampType = getStampType(targetRecordData.inpStartTime, targetRecordData.inpStartStampTime);
			String endStampType = getStampType(targetRecordData.inpEndTime, targetRecordData.inpEndStampTime);

			if (targetRecordData.isLocked || targetRecordData.isSummaryLocked) {
				throw new ComNormalException(ComMessage.msg().Att_Err_Locked(
						new List<String>{ComMessage.msg().Att_Lbl_Record}));
			}
			// 出勤打刻済み
			if (startStampType == STAMP_TYPE_STAMP) {
				throw new ComNormalException(ComMessage.msg().Att_Err_HasStampIn);
			}
			// 退勤打刻済み
			if (endStampType == STAMP_TYPE_STAMP) {
				throw new ComNormalException(ComMessage.msg().Att_Err_InvalidAttStartEndTime);
			}
			// 入力なし＋退勤時間より後
			if (startStampType == STAMP_TYPE_NONE) {
					if (targetRecordData.inpEndTime != null
							&& targetRecordData.inpEndTime.getIntValue() <= clockTime.getIntValue()) {
						throw new ComNormalException(ComMessage.msg().Att_Err_InvalidAttStartEndTime);
				}
			}
			// 手入力＋手入力
			if (startStampType == STAMP_TYPE_INPUT &&
					endStampType == STAMP_TYPE_INPUT) {
				throw new ComNormalException(ComMessage.msg().Att_Err_WithStartEndTime);
			}
		}
		// 前日チェック
		if (preRecordData != null) {
			// 退勤時間より前
			if (preRecordData.inpEndTime != null && preRecordData.inpEndTime.getIntValue() > (clockTime.getIntValue() + 24 * 60)) {
				throw new ComNormalException(ComMessage.msg().Att_Err_OverlapWithPreDay);
			}
			// 申請中の直行直帰申請の退勤時刻より前
			if (AppRequestStatus.PENDING.equals(preRecordData.inpRequestingDirectStatus)
					&& preRecordData.inpRequestingDirectEndTime != null
					&& preRecordData.inpRequestingDirectEndTime.getIntValue() > (clockTime.getIntValue() + 24 * 60)) {
				throw new ComNormalException(ComMessage.msg().Att_Err_OverlapWithPreDayDirectRequest);
			}
		}
	}

	/**
	 * 退勤打刻チェック
	 * @param nextRecordData 翌日明細
	 * @param targetRecordData 打刻明細
	 * @param clockTime 打刻時刻
	 */
	@testVisible
	private void validateClockOut(AttRecordEntity nextRecordData, AttRecordEntity targetRecordData, AttTime clockTime) {
		if (targetRecordData != null) {
			String startStampType = getStampType(targetRecordData.inpStartTime, targetRecordData.inpStartStampTime);
			String endStampType = getStampType(targetRecordData.inpEndTime, targetRecordData.inpEndStampTime);

		if (targetRecordData.isLocked || targetRecordData.isSummaryLocked) {
				throw new ComNormalException(ComMessage.msg().Att_Err_Locked(
						new List<String>{ComMessage.msg().Att_Lbl_Record}));
			}
			// 打刻済み
			if (endStampType == STAMP_TYPE_STAMP) {
				throw new ComNormalException(ComMessage.msg().Att_Err_HasStampOut);
			}
			// 手入力＋(手入力Or打刻)
			if (endStampType == STAMP_TYPE_INPUT
					&& startStampType != STAMP_TYPE_NONE) {
				throw new ComNormalException(ComMessage.msg().Att_Err_WithStartEndTime);
			}
			// 入力なし＋出勤時間前
			if (endStampType == STAMP_TYPE_NONE
					&& startStampType != STAMP_TYPE_NONE) {
				if (targetRecordData.inpStartTime.getIntValue() > clockTime.getIntValue()) {
					throw new ComNormalException(ComMessage.msg().Att_Err_InvalidAttStartEndTime);
				}
			}
		}
		// 翌日の出勤時間より後
		if (nextRecordData != null) {
			if (nextRecordData.inpStartTime != null
					&& (nextRecordData.inpStartTime.getIntValue() + 24 * 60) < clockTime.getIntValue()) {
				throw new ComNormalException(ComMessage.msg().Att_Err_OverlapWithNextDay);
			}
			// 申請中の直行直帰申請の退勤時刻より前
			if (AppRequestStatus.PENDING.equals(nextRecordData.inpRequestingDirectStatus)
					&& nextRecordData.inpRequestingDirectStartTime != null
					&& (nextRecordData.inpRequestingDirectStartTime.getIntValue() + 24 * 60) < clockTime.getIntValue()) {
				throw new ComNormalException(ComMessage.msg().Att_Err_OverlapWithNextDayDirectRequest);
			}
		}
	}

	/**
	 * 再出勤打刻チェック
	 * @param nextRecordData 翌日明細
	 * @param targetRecordData 打刻明細
	 * @param clockTime 打刻時刻
	 */
	@testVisible
	private void validateClockReIn(AttRecordEntity nextRecordData, AttRecordEntity targetRecordData, AttTime clockTime) {
		if (targetRecordData == null) {
			throw new ComNormalException(ComMessage.msg().Att_Err_NotStampOutYet);
		}
		if (targetRecordData.isLocked || targetRecordData.isSummaryLocked) {
			throw new ComNormalException(ComMessage.msg().Att_Err_Locked(
					new List<String>{ComMessage.msg().Att_Lbl_Record}));
		}
		String startStampType = getStampType(targetRecordData.inpStartTime, targetRecordData.inpStartStampTime);
		String endStampType = getStampType(targetRecordData.inpEndTime, targetRecordData.inpEndStampTime);

		// 未退勤
		if (endStampType == STAMP_TYPE_NONE) {
			throw new ComNormalException(ComMessage.msg().Att_Err_NotStampOutYet);
		}
		// 退勤時間より前
		else if (targetRecordData.inpEndTime.getIntValue() > clockTime.getIntValue()) {
			throw new ComNormalException(ComMessage.msg().Att_Err_InvalidAttReStartBeforeEndTime);
		}

		// 翌日の出勤時間より後
		if (nextRecordData != null) {
			if (nextRecordData.inpStartTime != null
					&& (nextRecordData.inpStartTime.getIntValue() + 24 * 60) < clockTime.getIntValue()) {
				throw new ComNormalException(ComMessage.msg().Att_Err_OverlapWithNextDay);
			}
			// 申請中の直行直帰申請の退勤時刻より前
			if (AppRequestStatus.PENDING.equals(nextRecordData.inpRequestingDirectStatus)
					&& nextRecordData.inpRequestingDirectStartTime != null
					&& (nextRecordData.inpRequestingDirectStartTime.getIntValue() + 24 * 60) < clockTime.getIntValue()) {
				throw new ComNormalException(ComMessage.msg().Att_Err_OverlapWithNextDayDirectRequest);
			}
		}
	}

	// 出退勤状態を取得する(打刻バリデーション用)
	private String getStampType(AttTime inpTime, AttTime inpStampTime) {
		if (inpTime == null) {
			return STAMP_TYPE_NONE;
		}
		else {
			return inpTime == inpStampTime ? STAMP_TYPE_STAMP : STAMP_TYPE_INPUT;
		}
	}

	/*
	 * エンティティの休憩時間から AttCalcRangesDto を生成する
	 */
	private AttCalcRangesDto toRestRanges(AttRecordEntity attRecord) {
		List<AttCalcRangeDto> rests = new List<AttCalcRangeDto>();
		if (attRecord.inpRest1StartTime != null || attRecord.inpRest1EndTime != null) {
			rests.add(AttCalcRangesDto.R(AttTime.intValue(attRecord.inpRest1StartTime), AttTime.intValue(attRecord.inpRest1EndTime)));
		}
		if (attRecord.inpRest2StartTime != null || attRecord.inpRest2EndTime != null) {
			rests.add(AttCalcRangesDto.R(AttTime.intValue(attRecord.inpRest2StartTime), AttTime.intValue(attRecord.inpRest2EndTime)));
		}
		if (attRecord.inpRest3StartTime != null || attRecord.inpRest3EndTime != null) {
			rests.add(AttCalcRangesDto.R(AttTime.intValue(attRecord.inpRest3StartTime), AttTime.intValue(attRecord.inpRest3EndTime)));
		}
		if (attRecord.inpRest4StartTime != null || attRecord.inpRest4EndTime != null) {
			rests.add(AttCalcRangesDto.R(AttTime.intValue(attRecord.inpRest4StartTime), AttTime.intValue(attRecord.inpRest4EndTime)));
		}
		if (attRecord.inpRest5StartTime != null || attRecord.inpRest5EndTime != null) {
			rests.add(AttCalcRangesDto.R(AttTime.intValue(attRecord.inpRest5StartTime), AttTime.intValue(attRecord.inpRest5EndTime)));
	}
		return AttCalcRangesDto.RS(rests);
	}

	/*
	 * restsに保持した時刻をソートし、エンティティに設定する
	 */
	private void setRestsToEntity(AttCalcRangesDto rests, AttRecordEntity attRecord) {
		// 休憩1~5の開始時刻・終了時刻を順に設定する
		List<AttTime> unsort = new List<AttTime>();
		for (Integer i = 0; i < 5; i++) {
			if (i < rests.size()) {
				unsort.add(new AttTime(rests.get(i).getStartTime()));
				unsort.add(new AttTime(rests.get(i).getEndTime()));
				continue;
			}
			unsort.add(null);
			unsort.add(null);
		}

		// 休憩時刻をソート
		List<AttTimePeriod> sorted = AttTimePeriod.sortByStartTime(
				unsort[0],unsort[1],
				unsort[2],unsort[3],
				unsort[4],unsort[5],
				unsort[6],unsort[7],
				unsort[8],unsort[9]
			);

		// 休憩時間を設定
		attRecord.inpRest1StartTime = sorted.get(0).startTime;
		attRecord.inpRest1EndTime = sorted.get(0).endTime;
		attRecord.inpRest2StartTime = sorted.get(1).startTime;
		attRecord.inpRest2EndTime = sorted.get(1).endTime;
		attRecord.inpRest3StartTime = sorted.get(2).startTime;
		attRecord.inpRest3EndTime = sorted.get(2).endTime;
		attRecord.inpRest4StartTime = sorted.get(3).startTime;
		attRecord.inpRest4EndTime = sorted.get(3).endTime;
		attRecord.inpRest5StartTime = sorted.get(4).startTime;
		attRecord.inpRest5EndTime = sorted.get(4).endTime;
	}

	/*
	 * 境界時刻がnullの場合、デフォルト値（5:00）を返す。
	 * 境界時刻がマスタに設定される仕様になったら廃止する
	 */
	private Integer ensureBoundaryTime(Integer boundaryTime) {
		return boundaryTime == null ? 5 * 60 : boundaryTime;
	}

	/*
	 * 時刻を24時間以内に調整
	 */
	private Integer adjustTimeIn24(AttTime targetAttTime) {
		Integer targetTime = AppConverter.intValue(targetAttTime);
		if (targetTime != null) {
			targetTime = Math.mod(targetTime, MINUTES_OF_DAY);
		}
		return targetTime;
	}
}