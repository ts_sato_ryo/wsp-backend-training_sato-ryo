/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 工数
 *
 * @description 工数系のバリデータ
 */
public with sharing class TimeConfigValidator {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 工数設定のラベル */
	private static final String TIME_SETTING_LABEL = ComMessage.msg().Admin_Lbl_TimeSetting;

	private static final String WORK_CATEGORY_NAME = ComMessage.msg().Admin_Lbl_WorkCategory;

	/**
	 * 値が下限値以上であることを確認する
	 * @param target 対象文字列
	 * @return 下限値より小さい場合はtrue、そうでない場合はfalse
	 */
	private static Boolean isInvalidMinValue(Integer target, Integer minValue) {
		if (target == null) {
			return false;
		}
		if (target < minValue) {
			return true;
		}
		return false;
	}

	/**
	 * 値が上限値以上であることを確認する
	 * @param target 対象文字列
	 * @return 上限値より大きい場合はtrue、そうでない場合はfalse
	 */
	private static Boolean isInvalidMaxValue(Integer target, Integer maxValue) {
		if (target == null) {
			return false;
		}
		if (target > maxValue) {
			return true;
		}
		return false;
	}


	/**
	 * 工数設定ベースエンティティのバリデーション
	 */
	public virtual class TimeSettingBaseValidator extends ComConfigValidator.ParentChildBaseValidator {

		/** バリデーション対象の履歴エンティティ */
		protected TimeSettingBaseEntity targetEntity;

		/**
		 * コンストラクタ
		 * @param targetEntity 検証対象の履歴エンティティ
		 * @param objectName 検証対象のオブジェクト名(マスタ名)
		 */
		public TimeSettingBaseValidator(TimeSettingBaseEntity targetEntity) {
			super(targetEntity);
			this.targetEntity = targetEntity;
		}

		/**
		 * バリデーションを行う
		 */
		public virtual override Validator.Result validate() {

			// ベースエンティティ共通の検証を行う
			super.validate();
			if (! result.isSuccess()) {
				return result;
			}

			// コードが設定されている
			if (String.isBlank(this.targetEntity.code)) {
				result.addError(createNullError(MESSAGE.Com_Lbl_Code));
			}
			// コードの文字数が最大文字数以下である
			if (isInvalidMaxLengthOver(this.targetEntity.code, TimeSettingBaseEntity.CODE_MAX_LENGTH)) {
				result.addError(createLengthOverError(MESSAGE.Com_Lbl_Code, TimeSettingBaseEntity.CODE_MAX_LENGTH));
			}

			// 会社IDが設定されている
			if (this.targetEntity.companyId == null) {
				result.addError(createNullError(MESSAGE.Com_Lbl_Company));
			}

			// サマリー期間の必須チェック
			if (this.targetEntity.summaryPeriod == null) {
				result.addError(createNullError(
						ComMessage.msg().Admin_Lbl_SummaryPeriod));
			}

			// サマリー期間別の必須項目
			if (this.targetEntity.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Month) {
				// 月度の開始日の必須チェック
				if (this.targetEntity.startDateOfMonth == null) {
					result.addError(createNullError(MESSAGE.Admin_Lbl_StartDateOfMonth));
				}
				// 月度の開始日の値のチェック
				if (this.targetEntity.startDateOfMonth < TimeSettingBaseEntity.START_DATE_OF_MONTH_MIN
						|| this.targetEntity.startDateOfMonth > TimeSettingBaseEntity.START_DATE_OF_MONTH_MAX) {
					result.addError(createValueRangeError(
							MESSAGE.Admin_Lbl_StartDateOfMonth,
							TimeSettingBaseEntity.START_DATE_OF_MONTH_MIN,
							TimeSettingBaseEntity.START_DATE_OF_MONTH_MAX));
				}
			} else if (this.targetEntity.summaryPeriod == TimeSettingBaseGeneratedEntity.SummaryPeriodType.Week) {
				// 週の必須チェック
				// 週の起算曜日の必須チェック
				if (this.targetEntity.startDayOfWeek == null) {
					result.addError(createNullError(MESSAGE.Admin_Lbl_StartDayOfWeek));
				}
			}

			// 月度表記の必須チェック
			if (this.targetEntity.monthMark == null) {
				result.addError(createNullError(ComMessage.msg().Admin_Lbl_MonthMark));
			}

			return result;
		}
	}

	/**
	 * 工数設定の履歴エンティティのバリデーション
	 */
	public class TimeSettingHistoryValidator extends ComConfigValidator.ParentChildHistoryValidator {

		/** 工数設定履歴エンティティ */
		private TimeSettingHistoryEntity targetEntity;
		/** 工数設定ベースエンティティ */
		private TimeSettingBaseEntity baseEntity;

		/**
		 * コンストラクタ
		 */
		public TimeSettingHistoryValidator(TimeSettingBaseEntity baseEntity, TimeSettingHistoryEntity targetEntity) {
			super(targetEntity, MESSAGE.Admin_Lbl_TimeSetting);
			this.targetEntity = targetEntity;
			this.baseEntity = baseEntity;
		}

		/**
		 * バリデーションを行う
		 */
		public override Validator.Result validate() {

			// 履歴の検証を行う
			super.validate();
			if (! result.isSuccess()) {
				return result;
			}

			// 工数設定名が設定されている
			if (String.isBlank(this.targetEntity.nameL0)) {
						result.addError(createNullError(createNameLangLabal(MESSAGE.Admin_Lbl_Language0)));
			}
			// 工数設定名の文字数が最大文字数以下である
			if (isInvalidMaxLengthOver(this.targetEntity.nameL0, TimeSettingHistoryEntity.NAME_MAX_LENGTH)) {
						result.addError(createLengthOverError(createNameLangLabal(MESSAGE.Admin_Lbl_Language0), TimeSettingHistoryEntity.NAME_MAX_LENGTH));
			}
			if (isInvalidMaxLengthOver(this.targetEntity.nameL1, TimeSettingHistoryEntity.NAME_MAX_LENGTH)) {
						result.addError(createLengthOverError(createNameLangLabal(MESSAGE.Admin_Lbl_Language1), TimeSettingHistoryEntity.NAME_MAX_LENGTH));
			}
			if (isInvalidMaxLengthOver(this.targetEntity.nameL2, TimeSettingHistoryEntity.NAME_MAX_LENGTH)) {
						result.addError(createLengthOverError(createNameLangLabal(MESSAGE.Admin_Lbl_Language2), TimeSettingHistoryEntity.NAME_MAX_LENGTH));
			}

			return this.result;
		}

		/**
		 * 名前項目のラベルを作成する
		 * @return 設定名(langLabel)
		 */
		private String createNameLangLabal(String langLabel) {
			return MESSAGE.Admin_Lbl_Name + '(' + langLabel + ')';
		}
	}


	/**
	 * 作業分類エンティティのバリデーション
	 */
	public class WorkCategoryValidator extends ComConfigValidator.ValidPeriodValidator {
		private WorkCategoryEntity targetEntity;

		/**
		 * コンストラクタ
		 * @param targetEntity 検証対象のアラート設定エンティティ
		 */
		public WorkCategoryValidator(WorkCategoryEntity targetEntity) {
			super(targetEntity, WORK_CATEGORY_NAME);
			this.targetEntity = targetEntity;
		}

		/**
		 * バリデーションを行う
		 */
		public override Validator.Result validate() {

			// 基本項目のバリデーション
			validateBasic();

			// 有効期間型マスタ共通項目のバリデーション
			super.validate();

			return this.result;
		}

		/**
		 * 基本項目のバリデーションを実行する
		 */
		public void validateBasic() {

			// コードが設定されている
			if (String.isBlank(this.targetEntity.code)) {
				result.addError(createNullError(MESSAGE.Com_Lbl_Code));
			}
			// コードの文字数が最大文字数以下である
			if (isInvalidMaxLengthOver(this.targetEntity.code, WorkCategoryEntity.CODE_MAX_LENGTH)) {
				result.addError(createLengthOverError(MESSAGE.Com_Lbl_Code, WorkCategoryEntity.CODE_MAX_LENGTH));
			}

			// 会社IDが設定されている
			if (this.targetEntity.companyId == null) {
				result.addError(createNullError(MESSAGE.Com_Lbl_Company));
			}

			// 作業分類名(L0)が設定されている
			if (String.isBlank(this.targetEntity.nameL0)) {
				result.addError(createNullError(MESSAGE.Admin_Lbl_Name));
			}
			// 作業分類名の文字数が最大文字数以下である
			if (isInvalidMaxLengthOver(this.targetEntity.nameL0, WorkCategoryEntity.NAME_MAX_LENGTH)) {
				result.addError(createLengthOverError(MESSAGE.Admin_Lbl_Name, WorkCategoryEntity.NAME_MAX_LENGTH));
			}
			if (isInvalidMaxLengthOver(this.targetEntity.nameL1, WorkCategoryEntity.NAME_MAX_LENGTH)) {
				result.addError(createLengthOverError(MESSAGE.Admin_Lbl_Name, WorkCategoryEntity.NAME_MAX_LENGTH));
			}
			if (isInvalidMaxLengthOver(this.targetEntity.nameL2, WorkCategoryEntity.NAME_MAX_LENGTH)) {
				result.addError(createLengthOverError(MESSAGE.Admin_Lbl_Name, WorkCategoryEntity.NAME_MAX_LENGTH));
			}

			// 表示順が下限値以上である
			if (isInvalidMinValue(this.targetEntity.order, WorkCategoryEntity.ORDER_MIN_VALUE)) {
				result.addError(createValueLessError(MESSAGE.Admin_Lbl_Order, WorkCategoryEntity.ORDER_MIN_VALUE));
			}
			// 表示順が上限値以下である
			if (isInvalidMaxValue(this.targetEntity.order, WorkCategoryEntity.ORDER_MAX_VALUE)) {
				result.addError(createValueTooLargeError(MESSAGE.Admin_Lbl_Order));
			}
		}
	}
}
