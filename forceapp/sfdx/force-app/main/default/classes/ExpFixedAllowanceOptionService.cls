/*
 * @author TeamSpirit Inc.
 *
 * @date 2019
 *
 * @description Service class for Expense Fixed Allowance Options
 *
 * @group Expense
 */
public with sharing class ExpFixedAllowanceOptionService implements Service {
	private static ComMsgBase MESSAGE = ComMessage.msg();

	/*
	 * Update the Fixed Allowance Options associated with the Expense Type.
	 * The options order are also updated to reflect the new order.
	 * The existing Options which are not include in the Option will be deleted.
	 * Option's Allowance Amount cannot be updated if already in used.
	 * Option's cannot be delete if already in used.
	 *
	 * @param expenseTypeId Id of Expense Type the Fixed Allowance Options belong to
	 * @param fixedAllowanceOptionEntityList list of {@link ExpFixedAllowanceOptionEntity} to associate with
	 */
	public void saveFixedAllowanceOptions(Id expenseTypeId, List<ExpFixedAllowanceOptionEntity> fixedAllowanceOptionEntityList, Boolean isNewExpType) {
		if (fixedAllowanceOptionEntityList != null) {
			for (ExpFixedAllowanceOptionEntity optionEntity : fixedAllowanceOptionEntityList) {
				optionEntity.expTypeId = expenseTypeId;
			}
		}

		if (isNewExpType) {
			ExpFixedAllowanceOptionRepository repo = new ExpFixedAllowanceOptionRepository();
			repo.saveEntityList(fixedAllowanceOptionEntityList);
		} else {
			List<ExpFixedAllowanceOptionEntity> existingEntityList = getFixedAllowanceOptions(expenseTypeId);
			// if it's null or empty, delete all the existing Options linked to the Fixed Allowance
			if (fixedAllowanceOptionEntityList == null || fixedAllowanceOptionEntityList.isEmpty()) {
				// Since referenced using Lookup, Restrict, no need to check if already in used or not.
				deleteFixedAllowanceOptions(existingEntityList);
			} else {
				deleteExistingFloatingOptions(existingEntityList, fixedAllowanceOptionEntityList);

				// Check Amount Change
				Map<Id, ExpFixedAllowanceOptionEntity> existingOptionMap = new Map<Id, ExpFixedAllowanceOptionEntity>();
				for (ExpFixedAllowanceOptionEntity entity : existingEntityList) {
					existingOptionMap.put(entity.id, entity);
				}

				Set<Id> updatedOptionIdSet = new Set<Id>();
				for (ExpFixedAllowanceOptionEntity entity : fixedAllowanceOptionEntityList) {
					// Only Process if the Option Entity is NOT new
					if (entity.id != null) {
						ExpFixedAllowanceOptionEntity existingEntity = existingOptionMap.get(entity.id);
						if (entity.allowanceAmount != existingEntity.allowanceAmount ||
								entity.currencyId != existingEntity.currencyId) {
							updatedOptionIdSet.add(entity.id);
						}
					}
				}

				if (!updatedOptionIdSet.isEmpty()) {
					if (existsRelatedTransactionRecords(updatedOptionIdSet)) {
						throw new App.IllegalStateException(MESSAGE.Com_Err_CannotChangeReference);
					}
				}

				ExpFixedAllowanceOptionRepository repo = new ExpFixedAllowanceOptionRepository();
				repo.saveEntityList(fixedAllowanceOptionEntityList);
			}
		}
	}

	/*
	 * Delete Existing Fixed Allowance Options which are no longer linked to the Expense Type
	 *
	 * @param oldEntityList List of Fixed Allowance Option Entity currently exists in the system
	 * @param newEntityList List of Fixed Allowance Option Entity which should be linked to the Expense Type
	 */
	private void deleteExistingFloatingOptions(List<ExpFixedAllowanceOptionEntity> oldEntityList, List<ExpFixedAllowanceOptionEntity> newEntityList) {
		Set<Id> existingOptionsIdSet = new Set<Id>();
		for (ExpFixedAllowanceOptionEntity entity : oldEntityList) {
			existingOptionsIdSet.add(entity.id);
		}

		Set<Id> toKeepIdSet = new Set<Id>();
		for (ExpFixedAllowanceOptionEntity entity : newEntityList) {
			toKeepIdSet.add(entity.id);
		}

		// Remove the existing Options which are no longer linked to the Expense Type from the Set
		existingOptionsIdSet.removeAll(toKeepIdSet);
		List<ExpFixedAllowanceOptionEntity> toDeleteEntityList = new List<ExpFixedAllowanceOptionEntity>();
		for (Id toDeleteOptionId : existingOptionsIdSet) {
			ExpFixedAllowanceOptionEntity entity = new ExpFixedAllowanceOptionEntity();
			entity.setId(toDeleteOptionId);
			toDeleteEntityList.add(entity);
		}
		deleteFixedAllowanceOptions(toDeleteEntityList);
	}

	/*
	 * Check if there is any existing Records which uses the Fixed Allowance Option
	 */
	private Boolean existsRelatedTransactionRecords(Set<Id> fixedAllowanceOptionIdSet) {
		// Search for expense record item which uses any of the Fixed Allowance Options specified
		List<ExpRecordItemEntity> recordItemList = new ExpRecordRepository().getRecordItemEntityListByFixedAllowanceId(fixedAllowanceOptionIdSet, 1);

		// If related records exist, return true
		if (recordItemList != null && !recordItemList.isEmpty()) {
			return true;
		}

		// Search for expense request record item whose target tax type is associated
		List<ExpRequestRecordItemEntity> requestRecordItemList = new ExpRequestRecordRepository().getRecordItemEntityListByFixedAllowanceId(fixedAllowanceOptionIdSet, 1);

		// If related records exist, return true
		if (requestRecordItemList != null && !requestRecordItemList.isEmpty()) {
			return true;
		}

		return false;
	}

	/*
	 * Get List of Fixed Allowance Options for the given Expense Type
	 *
	 * @param expenseTypeId ID of Expense Type to retrieve the Fixed Allowance Option
	 * @return List<ExpFixedAllowanceOptionEntity> List of Fixed Allowance Option Entity for the give expenseTypeId
	 */
	public List<ExpFixedAllowanceOptionEntity> getFixedAllowanceOptions(Id expenseTypeId) {
		ExpFixedAllowanceOptionRepository.SearchFilter filter = new ExpFixedAllowanceOptionRepository.SearchFilter();
		filter.expTypeIds = new Set<Id> {expenseTypeId};

		ExpFixedAllowanceOptionRepository repo = new ExpFixedAllowanceOptionRepository();
		return repo.searchEntityList(filter);
	}

	/*
	 * Retrieve and Set Fixed Allowance Options to the Expense Type Entity List
	 */
	public void setFixedAllowanceOptions(List<ExpTypeEntity> expTypeEntityList) {
		ExpFixedAllowanceOptionRepository.SearchFilter filter = new ExpFixedAllowanceOptionRepository.SearchFilter();
		Set<Id> expTypeIdSet = new Set<Id>();
		for (ExpTypeEntity entity : expTypeEntityList) {
			// Only need to process if it's of Multi Fixed Allowance type
			if (entity.recordType == ExpRecordType.FIXED_ALLOWANCE_MULTI) {
				expTypeIdSet.add(entity.id);
			}
		}
		// No need to do additional processing if there is no Multi Fixed Allowance Expense Type
		if (expTypeIdSet.isEmpty()) {
			return;
		}

		filter.expTypeIds = expTypeIdSet;

		ExpFixedAllowanceOptionRepository repo = new ExpFixedAllowanceOptionRepository();
		List<ExpFixedAllowanceOptionEntity> optionEntityList = repo.searchEntityList(filter);
		if (optionEntityList.isEmpty()) {
			// No Option, hence no need to do further processing
			return;
		}
		Map<Id, ExpTypeEntity> expTypeEntityMap = new Map<Id, ExpTypeEntity>();
		for (ExpTypeEntity expTypeEntity : expTypeEntityList) {
			if (expTypeEntity.recordType == ExpRecordType.FIXED_ALLOWANCE_MULTI) {
				expTypeEntityMap.put(expTypeEntity.id, expTypeEntity);
			}
		}

		for (ExpFixedAllowanceOptionEntity optionEntity : optionEntityList) {
			expTypeEntityMap.get(optionEntity.expTypeId).fixedAllowanceOptionEntityList.add(optionEntity);
		}
	}

	/*
	 * Delete Fixed Allowance Options
	 * @param toDeleteEntityList List of ExpFixedAllowanceOptionEntity to delete
	 */
	private static void deleteFixedAllowanceOptions(List<ExpFixedAllowanceOptionEntity> toDeleteEntityList) {
		if (!toDeleteEntityList.isEmpty()) {
			ExpFixedAllowanceOptionRepository repo = new ExpFixedAllowanceOptionRepository();
			try {
				repo.deleteEntityList(toDeleteEntityList);
			} catch (DmlException e) {
				if (e.getDmlStatusCode(0) == StatusCode.DELETE_FAILED.name()) {
					throw new App.IllegalStateException(MESSAGE.Com_Err_CannotDeleteReference);
				}
				throw e;
			}
		}
	}

	/*
	 * Set Currency Id to The Fixed Allowance Options
	 * @param expTypeEntity ExpTypeEntity containing Fixed Allowance Entity
	 * @param currencyEntity Currency Entity to set to Fixed Allowance Option
	 */
	public static void setCurrencyToAllowanceOptions(ExpTypeEntity expTypeEntity, CurrencyEntity currencyEntity) {
		if (!expTypeEntity.fixedAllowanceOptionEntityList.isEmpty()) {
			for (ExpFixedAllowanceOptionEntity optionEntity : expTypeEntity.fixedAllowanceOptionEntityList) {
				optionEntity.currencyId = currencyEntity.id;
			}
		}
	}
}