/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * @description 短時間勤務履歴レコード操作APIを実装するクラス
 */
public with sharing class AttShortTimeSettingHistoryResource {
	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 短時間勤務設定レコードを表すクラス
	 */
	public class SettingHistory implements RemoteApi.RequestParam {
		/** 短時間勤務レコードベースID */
		public String id;
		/** 短時間勤務ベースレコードID */
		public String baseId;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 履歴コメント */
		public String comment;
		/** 短時間勤務名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 短時間勤務名(言語0) */
		public String name_L0;
		/** 短時間勤務名(言語1) */
		public String name_L1;
		/** 短時間勤務名(言語2) */
		public String name_L2;
		/** 許可する短時間勤務時間[分] */
		public Integer allowableTimeOfShortWork;
		/** 許可する遅刻時間の上限[分] */
		public Integer allowableTimeOfLateArrival;
		/** 許可する早退時間の上限[分] */
		public Integer allowableTimeOfEarlyLeave;
		/** 許可する私用外出の上限[分] */
		public Integer allowableTimeOfIrregularRest;

		/**
		 * パラメータの値を設定した履歴エンティティを作成する
		 * @param paramMap リクエストパラメエータのMap
		 * @return 更新情報を設定したエンティティ
		 */
		public AttShortTimeSettingHistoryEntity createHistoryEntity( Map<String, Object> paramMap) {
			AttShortTimeSettingHistoryEntity setting = new AttShortTimeSettingHistoryEntity();

			if (paramMap.containsKey('baseId')) {
				setting.baseId = this.baseId;
			}
			if (paramMap.containsKey('name_L0')) {
				// TODO Name項目対応(name_L0を設定しておく)
				setting.name = this.name_L0;
				setting.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				setting.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				setting.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('validDateFrom') && this.validDateFrom != null) {
				setting.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (paramMap.containsKey('validDateTo')) {
				setting.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (paramMap.containsKey('comment')) {
				setting.historyComment = this.comment;
			}
			if (paramMap.containsKey('allowableTimeOfShortWork')) {
				setting.allowableTimeOfShortWork = this.allowableTimeOfShortWork;
			}
			if (paramMap.containsKey('allowableTimeOfLateArrival')) {
				setting.allowableTimeOfLateArrival = this.allowableTimeOfLateArrival;
			}
			if (paramMap.containsKey('allowableTimeOfEarlyLeave')) {
				setting.allowableTimeOfEarlyLeave = this.allowableTimeOfEarlyLeave;
			}
			if (paramMap.containsKey('allowableTimeOfIrregularRest')) {
				setting.allowableTimeOfIrregularRest = this.allowableTimeOfIrregularRest;
			}

			// 有効開始日のデフォルトは実行日
			if (setting.validFrom == null) {
				setting.validFrom = AppDate.today();
			}
			// 失効日のデフォルト
			if (setting.validTo == null) {
				setting.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}

			return setting;
		}

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// ベースID
			if (String.isBlank(this.baseId)) {
				App.ParameterException ex = new App.ParameterException();
				ex.setMessageIsBlank('baseId');
				throw ex;
			}
			try {
				System.Id.valueOf(this.baseId);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'baseId'}));
			}

		}
	}

	/**
	 * @description 短時間勤務設定履歴レコード作成結果レスポンス
	 */
	public virtual class CreateResponse implements RemoteApi.ResponseParam {
		/** 作成した履歴レコードID */
		public String id;
	}

	/**
	 * @description 短時間勤務設定履歴レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING;

		/**
		 * @description 短時間勤務設定履歴レコードを1件作成する
		 * @param req リクエストパラメータ(TimeSettingHistory)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			//------------------------------------------------------------------------
			// 【仮仕様】
			// 現在指定可能な改定日(有効開始日)は末尾の履歴の有効開始日以降とする。
			// 改定機能自体は実装済みでマスタ全体の有効期間内の日付で改定できるようになっているが
			// UI側が対応できていないため。
			//------------------------------------------------------------------------

			AttShortTimeSettingService service = new AttShortTimeSettingService();
			AttShortTimeSettingRepository repo = new AttShortTimeSettingRepository();

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			SettingHistory param = (SettingHistory)req.getParam(AttShortTimeSettingHistoryResource.SettingHistory.class);
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			AttShortTimeSettingHistoryEntity newHistory = param.createHistoryEntity(req.getParamMap());

			// 末尾の履歴(有効開始日が最新の履歴)を取得する
			AttShortTimeSettingBaseEntity base = repo.getEntity(newHistory.baseId);
			if (base == null) {
				throw new App.ParameterException(
						MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Admin_Lbl_ShortTimeWorkSetting}));
			}
			AttShortTimeSettingHistoryEntity latestHistory = (AttShortTimeSettingHistoryEntity)base.getLatestSuperHistory();

			// 追加の履歴の有効開始日が末尾の履歴の有効開始日以降であることを確認する
			if (newHistory.validFrom.getDate() < latestHistory.validFrom.getDate()) {
				// メッセージ：改定日は最新の履歴の有効開始日以降を指定してください
				throw new App.ParameterException(MESSAGE.Admin_Err_InvalidRevisionDate);
			}
			// 改定対象が最新履歴の場合は、マスタ全体の失効日(末尾の履歴の失効日)を更新する
			if (newHistory.validTo.getDate() != latestHistory.validTo.getDate()) {
				service.updateValidTo(newHistory.baseId, newHistory.validTo);
			}
			// 履歴を改定する
			service.reviseHistory(newHistory);

			// 改定日に有効な履歴のIDを取得する
			AttShortTimeSettingBaseEntity revisedBase = repo.getEntity(newHistory.baseId, newHistory.validFrom);
			CreateResponse res = new CreateResponse();
			res.Id = revisedBase.getHistoryList().get(0).id;
			return res;
		}
	}

	/**
	 * @description 短時間勤務設定履歴レコードの削除パラメータを定義するクラス
	 */
	public class DeleteRequest implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}

	}

	/**
	 * 短時間勤務パターン履歴レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final AttConfigResourcePermission.Permission requriedPermission =
				AttConfigResourcePermission.Permission.MANAGE_ATT_SHORT_TIME_WORK_SETTING;

		/**
		 * @description 短時間勤務設定履歴レコードを1件削除する
		 * @param req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// #以下の場合は例外 (RemoteApi.TypeException) が発生する
			//   ・データ型が一致しないパラメータがある
			DeleteRequest param = (DeleteRequest)req.getParam(AttShortTimeSettingHistoryResource.DeleteRequest.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new AttConfigResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new AttShortTimeSettingService().deleteHistoryListWithPropagation(new List<Id>{param.id});
			} catch (App.RecordNotFoundException e) {
				// すでに削除済みの場合は成功レスポンスを返す
			}

			// 成功レスポンスをセットする
			return null;

		}
	}

	/**
	 * @description 短時間勤務設定履歴レコード検索結果を格納するクラス
	 */
	public class SearchResponse implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public SettingHistory[] records;
	}

	/**
	 * @description 短時間勤務設定履歴レコード検索条件を格納するクラス
	 */
	public class SearchRequest implements RemoteApi.RequestParam {

		/** 短時間勤務設定履歴レコードID */
		public String id;
		/** 関連するベースレコードID */
		public String baseId;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {

			// 履歴ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// ベースID
			if (String.isNotBlank(this.baseId)) {
				try {
					System.Id.valueOf(this.baseId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'baseId'}));
				}
			}
		}

	}

	/**
	 * @description 短時間勤務設定履歴レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 短時間勤務設定履歴レコードを検索する
		 * @param requ リクエストパラメータ(SearchRequest)
		 * @return レスポンス(SearchResponse)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SearchRequest param = (SearchRequest)req.getParam(AttShortTimeSettingHistoryResource.SearchRequest.class);

			// パラメータのバリデーション
			param.validate();

			AttShortTimeSettingRepository.SearchFilter filter = new AttShortTimeSettingRepository.SearchFilter();
			if (param.id != null) {
				filter.historyIds = new Set<Id>{param.id};
			}
			if (param.baseId != null) {
				filter.baseIds = new Set<Id>{param.baseId};
			}

			List<AttShortTimeSettingHistoryEntity> entityList =
					(new AttShortTimeSettingRepository()).searchHistoryEntity(filter, false,
					AttShortTimeSettingRepository.HistorySortOrder.VALID_FROM_DESC);


			// エンティティをレスポンスオブジェクトに変換
			List<SettingHistory> historyList = new List<SettingHistory>();
			for (AttShortTimeSettingHistoryEntity entity : entityList) {
				SettingHistory history = new SettingHistory();
				history.id = entity.id;
				history.baseId = entity.baseId;
				history.validDateFrom = AppDate.convertDate(entity.validFrom);
				history.validDateTo = AppDate.convertDate(entity.validTo);
				history.comment = entity.historyComment;
				history.name = entity.nameL.getValue();
				history.name_L0 = entity.nameL0;
				history.name_L1 = entity.nameL1;
				history.name_L2 = entity.nameL2;
				history.allowableTimeOfShortWork = entity.allowableTimeOfShortWork;
				history.allowableTimeOfLateArrival = entity.allowableTimeOfLateArrival;
				history.allowableTimeOfEarlyLeave = entity.allowableTimeOfEarlyLeave;
				history.allowableTimeOfIrregularRest = entity.allowableTimeOfIrregularRest;

				historyList.add(history);
			}

			// 成功レスポンスをセットする
			SearchResponse res = new SearchResponse();
			res.records = historyList;
			return res;
		}
	}
}
