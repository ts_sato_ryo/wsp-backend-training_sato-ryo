/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * Vendor Payment Due Date Usage pick list
 */
public with sharing class ExpVendorPaymentDueDateUsage extends ValueObjectType {

	public static final ExpVendorPaymentDueDateUsage REQUIRED = new ExpVendorPaymentDueDateUsage('Required');
	public static final ExpVendorPaymentDueDateUsage OPTIONAL = new ExpVendorPaymentDueDateUsage('Optional');
	public static final ExpVendorPaymentDueDateUsage NOT_USED = new ExpVendorPaymentDueDateUsage('NotUsed');

	/** Entries */
	public static final List<ExpVendorPaymentDueDateUsage> TYPE_LIST;
	static {
		TYPE_LIST = new List<ExpVendorPaymentDueDateUsage> {
			REQUIRED,
			OPTIONAL,
			NOT_USED
		};
	}

	/**
	 * Constructor
	 * @param value
	 */
	private ExpVendorPaymentDueDateUsage(String value) {
		super(value);
	}

	/**
	 * Return the value whose key matches with specified string
	 * @param The key value to get the instance
	 * @return The instance having the value specified
	 */
	public static ExpVendorPaymentDueDateUsage valueOf(String value) {
		ExpVendorPaymentDueDateUsage retType = null;
		if (String.isNotBlank(value)) {
			for (ExpVendorPaymentDueDateUsage type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * Return whether the object is same
	 * @param Target object to compare with
	 * @return If the objects are same, return true. otherwise return false.
	 */
	public override Boolean equals(Object compare) {

		// if null, return false.
		if (compare == null) {
			return false;
		}

		Boolean eq;
		if (compare instanceof ExpVendorPaymentDueDateUsage) {
			// 値が同じであればtrue
			if (this.value == ((ExpVendorPaymentDueDateUsage) compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		return eq;
	}
}