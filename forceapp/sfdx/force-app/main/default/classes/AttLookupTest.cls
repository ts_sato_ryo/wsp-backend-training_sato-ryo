/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 勤怠
 *
 * 参照の項目の情報を持つ値オブジェクトのテスト
 */
@isTest
private class AttLookupTest {

	/**
	 * Employee値オブジェクトの比較が正しくできていることを確認する
	 */
	@isTest static void employeeEqualsTest() {

 		// 等しい場合
		AttLookup.Employee employee = new AttLookup.Employee(
				new AppPersonName('Hanako_L0', 'Yamada_L0', 'Hanako_L1', 'Yamada_L1', 'Hanako_L2', 'Yamada_L2'),
				null,
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), false);
		AttLookup.Employee compare = new AttLookup.Employee(
				new AppPersonName('Hanako_L0', 'Yamada_L0', 'Hanako_L1', 'Yamada_L1', 'Hanako_L2', 'Yamada_L2'),
				null,
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), false);
		System.assertEquals(employee, compare);

		// 等しくない場合(値が異なる) → false
		// isRmovedが異なる
		compare = new AttLookup.Employee(
				new AppPersonName('Hanako_L0', 'Yamada_L0', 'Hanako_L1', 'Yamada_L1', 'Hanako_L2', 'Yamada_L2'),
				null,
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), true);
		System.assertNotEquals(employee, compare);
		// 氏名が異なる
		compare = new AttLookup.Employee(
				new AppPersonName('DIFF_Hanako_L0', 'Yamada_L0', 'Hanako_L1', 'Yamada_L1', 'Hanako_L2', 'Yamada_L2'),
				null,
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), false);
		System.assertNotEquals(employee, compare);
		// 有効開始日が異なる
		compare = new AttLookup.Employee(
				new AppPersonName('Hanako_L0', 'Yamada_L0', 'Hanako_L1', 'Yamada_L1', 'Hanako_L2', 'Yamada_L2'),
				null,
				AppDate.newInstance(2017, 10, 05), AppDate.newInstance(2030, 12, 31), false);
		System.assertNotEquals(employee, compare);
		// 有効終了日が異なる
		compare = new AttLookup.Employee(
				new AppPersonName('Hanako_L0', 'Yamada_L0', 'Hanako_L1', 'Yamada_L1', 'Hanako_L2', 'Yamada_L2'),
				null,
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2031, 1, 1), false);
		System.assertNotEquals(employee, compare);

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, employee.equals(AppDate.today()));

		// 等しくない場合(null)
		System.assertEquals(false, employee.equals(null));

	}

	/**
	 * Department値オブジェクトの比較が正しくできていることを確認する
	 */
	@isTest static void departmentEqualsTest() {

		// 等しい場合
		AttLookup.Department dept = new AttLookup.Department(
				new AppMultiString('Dept_L0', 'Dept_L1', 'Dept_L2'),
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), false);
		AttLookup.Department compare = new AttLookup.Department(
				new AppMultiString('Dept_L0', 'Dept_L1', 'Dept_L2'),
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), false);
		System.assertEquals(dept, compare);

		// 等しくない場合(値が異なる) → false
		// isRmovedが異なる
		compare = new AttLookup.Department(
				new AppMultiString('Dept_L0', 'Dept_L1', 'Dept_L2'),
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), true);
		System.assertNotEquals(dept, compare);
		// 部署名が異なる
		compare = new AttLookup.Department(
				new AppMultiString('DIFF_Dept_L0', 'Dept_L1', 'Dept_L2'),
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2030, 12, 31), false);
		System.assertNotEquals(dept, compare);
		// 有効開始日が異なる
		compare = new AttLookup.Department(
				new AppMultiString('Dept_L0', 'Dept_L1', 'Dept_L2'),
				AppDate.newInstance(2017, 10, 05), AppDate.newInstance(2030, 12, 31), false);
		System.assertNotEquals(dept, compare);
		// 有効終了日が異なる
		compare = new AttLookup.Department(
				new AppMultiString('Dept_L0', 'Dept_L1', 'Dept_L2'),
				AppDate.newInstance(2017, 10, 04), AppDate.newInstance(2031, 1, 1), false);
		System.assertNotEquals(dept, compare);

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, dept.equals(AppDate.today()));

		// 等しくない場合(null)
		System.assertEquals(false, dept.equals(null));

	}
}
