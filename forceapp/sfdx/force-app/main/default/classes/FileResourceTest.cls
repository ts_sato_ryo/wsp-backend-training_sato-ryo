/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 経費 Expense
 *
 * @description FileResourceのテストクラス Test class for FileResource
 */
@isTest
private class FileResourceTest {
	
	/** Message */
	private final static ComMsgBase MESSAGE = ComMessage.msg();
	
	/**
	 * ファイル保存APIのテスト Test for SaveFileApi class
	 */
	@isTest static void saveFileApiTest() {
		// ファイルボディ File body
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);

		Test.startTest();

		// リクエストパラメータ Request parameter
		FileResource.SaveFileParam param = new FileResource.SaveFileParam();
		param.fileName = 'File.jpg';
		param.fileBody = bodyString;
		param.type = FileService.Type.Receipt.name();

		// API実行 Execute API
		FileResource.SaveFileApi api = new FileResource.SaveFileApi();
		FileResource.SaveFileResult res = (FileResource.SaveFileResult)api.execute(param);

		Test.stopTest();

		ContentVersion savedFile = [
			SELECT
				Id,
				PathOnClient,
				VersionData
			FROM ContentVersion
			WHERE Id = :res.contentVersionId];

		System.assertEquals(param.fileName, savedFile.PathOnClient);
	}

	/**
	 * ファイル保存APIのテスト Test for SaveFileApi class
	 * バリデーションが正しく行われることを確認する Parameter validation test
	 */
	@isTest static void saveFileApiTestValidation() {
		List<Exception> expectedExceptionList = new List<Exception>();
		String testType = 'test';
		
		Test.startTest();

		// ファイル名が未指定 File name not specified => NG
		FileResource.SaveFileParam param1 = new FileResource.SaveFileParam();
		param1.fileName = null;
		try {
			param1.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		// ファイルボディが未指定 File body not specified => NG
		FileResource.SaveFileParam param2 = new FileResource.SaveFileParam();
		param2.fileName = 'File.jpg';
		param2.fileBody = null;
		try {
			param2.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}
		
		// タイプが未指定 Type not specified => NG
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		FileResource.SaveFileParam param3 = new FileResource.SaveFileParam();
		param3.fileName = 'File.jpg';
		param3.fileBody = bodyString;
		param3.type = null;
		try {
			param3.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}
		
		// タイプが不正 Invalid Type => NG
		FileResource.SaveFileParam param4 = new FileResource.SaveFileParam();
		param4.fileName = 'File.jpg';
		param4.fileBody = bodyString;
		param4.type = testType;
		try {
			param4.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		Test.stopTest();
		
		System.assertEquals(4, expectedExceptionList.size());
		for (Exception expectedException : expectedExceptionList) {
			System.assertNotEquals(null, expectedException);
			System.assertEquals(true, expectedException instanceOf App.ParameterException);
		}
		
		String msg1 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'fileName', null});
		String msg2 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'fileBody', null});
		String msg3 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'type', null});
		String msg4 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'type', testType});
		System.assertEquals(msg1, expectedExceptionList[0].getMessage());
		System.assertEquals(msg2, expectedExceptionList[1].getMessage());
		System.assertEquals(msg3, expectedExceptionList[2].getMessage());
		System.assertEquals(msg4, expectedExceptionList[3].getMessage());
	}

	/**
	 * ファイル保存APIのテスト Test for SaveFileApi class
	 * エラーが発生した場合はロールバックされることを確認する Rollback test
	 */
	@isTest static void saveFileApiTestRollback() {
		// ファイルボディ File body
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);

		Test.startTest();

		// リクエストパラメータ Request parameter
		FileResource.SaveFileParam param = new FileResource.SaveFileParam();
		param.fileName = 'File.jpg';
		param.fileBody = bodyString;
		param.type = FileService.Type.Receipt.name();

		// API実行 Execute API
		try {
			FileResource.SaveFileApi api = new FileResource.SaveFileApi();
			api.isRollbackTest = true;
			api.execute(param);

			System.assert(false, '例外が発生しませんでした。');
		} catch (Exception e) {
			// ロールバックされていることを確認する Verify rollback
			// 1件もデータが作成されていないはず Ensure no data is created
			List<ContentVersion> contentVersionList = [SELECT Id FROM ContentVersion];
			System.assertEquals(true, contentVersionList.isEmpty());
		}

		Test.stopTest();
	}
	
	/**
	 * ファイル一覧取得APIのテスト Test for GetFileListApi class
	 */
	@isTest static void getFileListApiTest() {
		// Initialize test data
		ComOrgSetting__c org = ComTestDataUtility.createOrgSetting('ja', 'en_US', null);
		ComCountry__c country = ComTestDataUtility.createCountry('Japan');
		ComCompany__c company = ComTestDataUtility.createTestCompany('Test', country.Id, 'en_US');
		ComDeptBase__c dept = ComTestDataUtility.createDepartmentWithHistory('Test', company.Id);
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('Test', company.Id, dept.Id, userInfo.getUserId());
		
		// ファイルボディ File body
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);

		// ファイルを作成する Create files
		// File 1 => Quotation file
		// File 2 => Quotation file
		// File 3 => Receipt file
		// File 4 => Receipt file
		List<ContentVersion> contentList = new List<ContentVersion>();
		ContentVersion content1 = new ContentVersion(
			Title = FileService.EXP_QUOTATION_TITLE_PREFIX + 'Test1',
			PathOnClient = 'Test1.jpg',
			VersionData = Blob.valueOf(bodyString));
		contentList.add(content1);
		ContentVersion content2 = new ContentVersion(
			Title = FileService.EXP_QUOTATION_TITLE_PREFIX + 'Test2',
			PathOnClient = 'Test2.jpg',
			VersionData = Blob.valueOf(bodyString));
		contentList.add(content2);
		ContentVersion content3 = new ContentVersion(
			Title = FileService.EXP_RECEIPT_TITLE_PREFIX + 'Test3',
			PathOnClient = 'Test3.jpg',
			VersionData = Blob.valueOf(bodyString));
		contentList.add(content3);
		ContentVersion content4 = new ContentVersion(
			Title = FileService.EXP_RECEIPT_TITLE_PREFIX + 'Test4',
			PathOnClient = 'Test4.jpg',
			VersionData = Blob.valueOf(bodyString));
		contentList.add(content4);
		insert contentList;

		Test.startTest();

		// リクエストパラメータ Request parameter 1 => Get Quotation Files
		FileResource.GetFileListParam param1 = new FileResource.GetFileListParam();
		param1.type = FileService.Type.Quotation.name();
		param1.empId = null;
		
		// リクエストパラメータ Request parameter 2 => Get Receipt Files
		FileResource.GetFileListParam param2 = new FileResource.GetFileListParam();
		param2.type = FileService.Type.Receipt.name();
		param2.empId = null;
		
		// リクエストパラメータ Request parameter 3 => Get All Expense Files
		FileResource.GetFileListParam param3 = new FileResource.GetFileListParam();
		param3.type = FileService.Type.Expense.name();
		param3.empId = null;

		// API実行 Execute APIs
		FileResource.GetFileListApi api = new FileResource.GetFileListApi();
		FileResource.GetFileListResult res1 = (FileResource.GetFileListResult)api.execute(param1);
		FileResource.GetFileListResult res2 = (FileResource.GetFileListResult)api.execute(param2);
		FileResource.GetFileListResult res3 = (FileResource.GetFileListResult)api.execute(param3);

		Test.stopTest();

		List<ContentDocument> documentList = [
			SELECT
				Id,
				OwnerId,
				Title,
				LatestPublishedVersionId,
				ContentModifiedDate
			FROM ContentDocument
			ORDER BY Title];
		
		// Verify results
		System.assertEquals(2, res1.files.size());
		System.assertEquals(2, res2.files.size());
		System.assertEquals(4, res3.files.size());
		
		// Verify Quotation Files
		for (Integer i = 0; i < res1.files.size(); i++) {
			System.assertEquals(documentList[i].Id, res1.files[i].contentDocumentId);
			System.assertEquals(documentList[i].LatestPublishedVersionId, res1.files[i].contentVersionId);
			System.assertEquals(documentList[i].Title, res1.files[i].title);
			System.assertEquals(String.valueOf(documentList[i].ContentModifiedDate.date()), res1.files[i].createdDate);
		}
		
		// Verify Receipt Files
		Integer j = 0;
		for (Integer i = 2; i < documentList.size(); i++) {
			System.assertEquals(documentList[i].Id, res2.files[j].contentDocumentId);
			System.assertEquals(documentList[i].LatestPublishedVersionId, res2.files[j].contentVersionId);
			System.assertEquals(documentList[i].Title, res2.files[j].title);
			System.assertEquals(String.valueOf(documentList[i].ContentModifiedDate.date()), res2.files[j].createdDate);
			j++;
		}
		
		// Verify All Expense Files
		for (Integer i = 0; i < res3.files.size(); i++) {
			System.assertEquals(documentList[i].Id, res3.files[i].contentDocumentId);
			System.assertEquals(documentList[i].LatestPublishedVersionId, res3.files[i].contentVersionId);
			System.assertEquals(documentList[i].Title, res3.files[i].title);
			System.assertEquals(String.valueOf(documentList[i].ContentModifiedDate.date()), res3.files[i].createdDate);
		}
	}

	/**
	 * ファイル一覧取得APIのテスト Test for GetFileListApi class
	 * バリデーションが正しく行われることを確認する Parameter validation test
	 */
	@isTest static void getFileListApiTestValidation() {
		List<Exception> expectedExceptionList = new List<Exception> ();
		String testValue = 'test';
		
		Test.startTest();
		
		// タイプが未指定 Type not specified => NG
		FileResource.GetFileListParam param1 = new FileResource.GetFileListParam();
		param1.type = null;
		try {
			param1.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}
		
		// タイプが不正 Invalid Type => NG
		FileResource.GetFileListParam param2 = new FileResource.GetFileListParam();
		param2.type = testValue;
		try {
			param2.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		// ファイル名が不正 Invalid Employee Base ID => NG
		FileResource.GetFileListParam param3 = new FileResource.GetFileListParam();
		param3.type = FileService.Type.Receipt.name();
		param3.empId = testValue;
		try {
			param3.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		Test.stopTest();
		
		System.assertEquals(3, expectedExceptionList.size());
		for (Exception expectedException : expectedExceptionList) {
			System.assertNotEquals(null, expectedException);
			System.assertEquals(true, expectedException instanceOf App.ParameterException);
		}
		
		String msg1 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'type', null});
		System.assertEquals(msg1, expectedExceptionList[0].getMessage());
		String msg2 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'type', testValue});
		System.assertEquals(msg2, expectedExceptionList[1].getMessage());
		String msg3 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'empId', testValue});
		System.assertEquals(msg3, expectedExceptionList[2].getMessage());
	}

	/**
	 * ファイル取得APIのテスト Test for GetFileApi class
	 */
	@isTest static void getFileApiTest() {
		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File.jpg';
		Id contentVersionId = new FileService().saveFile(fileName, bodyString, FileService.Type.Receipt.name()).id;

		Test.startTest();

		// リクエストパラメータ Request parameter
		FileResource.GetFileParam param = new FileResource.GetFileParam();
		param.contentVersionId = contentVersionId;

		// API実行 Execute API
		FileResource.GetFileApi api = new FileResource.GetFileApi();
		FileResource.GetFileResult res = (FileResource.GetFileResult)api.execute(param);

		Test.stopTest();

		ContentVersion contentVersion = [
			SELECT
				ContentDocumentId,
				Title,
				VersionData
			FROM ContentVersion
			WHERE Id = :contentVersionId];

		System.assertEquals(contentVersion.ContentDocumentId, res.contentDocumentId);
		System.assertEquals(contentVersion.Title, res.title);
		System.assertEquals(EncodingUtil.base64Encode(contentVersion.VersionData), res.fileBody);
	}

	/*
	 *  Test for GetFileApi class - with OCR status in progress
	 */
	@isTest static void getFileApiWithOCRStatusInProgressTest() {
		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File.jpg';
		Id contentVersionId = new FileService().saveFile(fileName, bodyString, FileService.Type.Receipt.name()).id;

		ExpTestData testData = new ExpTestData();
		String sampleTaskId = '0be6e9db-059f-430c-972f-31c343aed494';
		new ExpOCRRepository().saveEntityWithStatus(contentVersionId, sampleTaskId, testData.company.Id, ExpOCRRepository.IN_PROGRESS);

		Test.startTest();

		// リクエストパラメータ Request parameter
		FileResource.GetFileParam param = new FileResource.GetFileParam();
		param.contentVersionId = contentVersionId;

		// API実行 Execute API
		FileResource.GetFileApi api = new FileResource.GetFileApi();
		FileResource.GetFileResult res = (FileResource.GetFileResult)api.execute(param);

		Test.stopTest();

		ContentVersion contentVersion = [
			SELECT
				ContentDocumentId,
				Title,
				VersionData
			FROM ContentVersion
			WHERE Id = :contentVersionId];

		System.assertEquals(contentVersion.ContentDocumentId, res.contentDocumentId);
		System.assertEquals(contentVersion.Title, res.title);
		System.assertEquals(EncodingUtil.base64Encode(contentVersion.VersionData), res.fileBody);
		System.assertEquals('InProgress', res.ocrInfo.status);
		System.assertEquals(sampleTaskId, res.ocrInfo.taskId);
		System.assertEquals(null, res.ocrInfo.result.amount);
		System.assertEquals(null, res.ocrInfo.result.recordDate);
	}

	/*
	 *  Test for GetFileApi class - with OCR status completed
	 */
	@isTest static void getFileApiWithOCRStatusCompletedTest() {
		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File.jpg';
		Id contentVersionId = new FileService().saveFile(fileName, bodyString, FileService.Type.Receipt.name()).id;

		ExpTestData testData = new ExpTestData();
		String sampleTaskId = '0be6e9db-059f-430c-972f-31c343aed494';
		ExpOCRRepository repo = new ExpOCRRepository();
		repo.saveEntityWithStatus(contentVersionId, sampleTaskId, testData.company.Id, ExpOCRRepository.COMPLETED);
		ExpOCREntity ocrEntity = repo.searchEntityByTaskId(sampleTaskId);
		repo.saveEntityWithOCRResults(ocrEntity, ExpOCRRepository.COMPLETED, 'testAllText', 777, AppDate.valueOf('1994-06-18'), 'testJson');

		Test.startTest();

		// リクエストパラメータ Request parameter
		FileResource.GetFileParam param = new FileResource.GetFileParam();
		param.contentVersionId = contentVersionId;

		// API実行 Execute API
		FileResource.GetFileApi api = new FileResource.GetFileApi();
		FileResource.GetFileResult res = (FileResource.GetFileResult)api.execute(param);

		Test.stopTest();

		ContentVersion contentVersion = [
			SELECT
				ContentDocumentId,
				Title,
				VersionData
			FROM ContentVersion
			WHERE Id = :contentVersionId];

		System.assertEquals(contentVersion.ContentDocumentId, res.contentDocumentId);
		System.assertEquals(contentVersion.Title, res.title);
		System.assertEquals(EncodingUtil.base64Encode(contentVersion.VersionData), res.fileBody);
		System.assertEquals('Completed', res.ocrInfo.status);
		System.assertEquals(sampleTaskId, res.ocrInfo.taskId);
		System.assertEquals(777, res.ocrInfo.result.amount);
		System.assertEquals('1994-06-18', res.ocrInfo.result.recordDate);
	}

	/**
	 * ファイル取得APIのテスト Test for GetFileApi class
	 * バリデーションが正しく行われることを確認する Parameter validation test
	 */
	@isTest static void getFileApiTestValidation() {
		List<Exception> expectedExceptionList = new List<Exception>();
		String testContentVersionId = 'test';
		
		Test.startTest();

		// ファイルIDが未指定 ContentVersion ID not specified => NG
		FileResource.GetFileParam param1 = new FileResource.GetFileParam();
		param1.contentVersionId = null;
		try {
			param1.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		// ファイルIDが不正 Invalid ContentVersion ID => NG
		FileResource.GetFileParam param2 = new FileResource.GetFileParam();
		param2.contentVersionId = testContentVersionId;
		try {
			param2.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		Test.stopTest();
		
		System.assertEquals(2, expectedExceptionList.size());
		for (Exception expectedException : expectedExceptionList) {
			System.assertNotEquals(null, expectedException);
			System.assertEquals(true, expectedException instanceOf App.ParameterException);
		}
		
		String msg1 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'contentVersionId', null});
		String msg2 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'contentVersionId', testContentVersionId});
		System.assertEquals(msg1, expectedExceptionList[0].getMessage());
		System.assertEquals(msg2, expectedExceptionList[1].getMessage());
	}
	
	/**
	 * ファイル削除APIのテスト Test for DeleteFileApi class
	 */
	@isTest static void deleteFileApiTest() {
		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File.jpg';
		Id contentDocumentId = new FileService().saveFile(fileName, bodyString, FileService.Type.Receipt.name()).contentDocumentId;

		Test.startTest();

		// リクエストパラメータ Request parameter
		FileResource.DeleteFileParam param = new FileResource.DeleteFileParam();
		param.contentDocumentIds = new List<String>{contentDocumentId};

		// API実行 Execute API
		FileResource.DeleteFileApi api = new FileResource.DeleteFileApi();
		api.execute(param);

		Test.stopTest();

		List<ContentDocument> contentList = [SELECT Id FROM ContentDocument];

		System.assertEquals(true, contentList.isEmpty());
	}

	/**
	 * ファイル削除APIのテスト Test for DeleteFileApi class
	 * バリデーションが正しく行われることを確認する Parameter validation test
	 */
	@isTest static void deleteFileApiTestValidation() {
		List<Exception> expectedExceptionList = new List<Exception>();
		String testContentDocumentId = 'test';

		Test.startTest();

		// IDリストが未指定 ContentDocument ID not specified => NG
		FileResource.DeleteFileParam param1 = new FileResource.DeleteFileParam();
		param1.contentDocumentIds = null;
		try {
			param1.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		// IDが不正 Invalid ContentDocument ID => NG
		FileResource.DeleteFileParam param2 = new FileResource.DeleteFileParam();
		param2.contentDocumentIds = new List<String>{testContentDocumentId};
		try {
			param2.validate();
		} catch (Exception e) {
			expectedExceptionList.add(e);
		}

		Test.stopTest();
		
		System.assertEquals(2, expectedExceptionList.size());
		for (Exception expectedException : expectedExceptionList) {
			System.assertNotEquals(null, expectedException);
			System.assertEquals(true, expectedException instanceOf App.ParameterException);
		}
		
		String msg1 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'contentDocumentIds', null});
		String msg2 = MESSAGE.Com_Err_InvalidParameter(new List<String>{'contentDocumentId', testContentDocumentId});
		System.assertEquals(msg1, expectedExceptionList[0].getMessage());
		System.assertEquals(msg2, expectedExceptionList[1].getMessage());
	}

	/**
	 * ファイル削除APIのテスト Test for DeleteFileApi class
	 * エラーが発生した場合はロールバックされることを確認する Rollback test
	 */
	@isTest static void deleteFileApiTestRollback() {

		// ファイルを作成する Create files
		Integer byteSize = (Integer)Math.pow(10, 3); // 1KB
		String bodyString = ('0').repeat(byteSize);
		String fileName = 'File.jpg';
		Id contentDocumentId = new FileService().saveFile(fileName, bodyString, FileService.Type.Receipt.name()).contentDocumentId;

		Test.startTest();

		// リクエストパラメータ Request parameter
		FileResource.DeleteFileParam param = new FileResource.DeleteFileParam();
		param.contentDocumentIds = new List<String>{contentDocumentId};

		// API実行 Execute API
		try {
			FileResource.DeleteFileApi api = new FileResource.DeleteFileApi();
			api.isRollbackTest = true;
			api.execute(param);

			System.assert(false, '例外が発生しませんでした。');
		} catch (Exception e) {
			// ロールバックされていることを確認する Verify rollback
			// 1件もデータが削除されていないはず Ensure no data is deleted
			List<ContentDocument> contentList = [SELECT Id FROM ContentDocument];
			System.assertEquals(false, contentList.isEmpty());
		}

		Test.stopTest();
	}
}