/**
 * AttDayTypeのテスト
 */
@isTest
private class AttDayTypeTest {

	/**
	 * 比較が正しくできていることを確認する
	 */
	@isTest static void equalsTest() {

		// 等しい場合
		System.assertEquals(AttDayType.WORKDAY, AttDayType.getType('Workday'));
		System.assertEquals(AttDayType.HOLIDAY, AttDayType.getType('Holiday'));
		System.assertEquals(AttDayType.PREFERRED_LEGAL_HOLIDAY, AttDayType.getType('PreferredLegalHoliday'));
		System.assertEquals(AttDayType.LEGAL_HOLIDAY, AttDayType.getType('LegalHoliday'));

		// 等しくない場合(値が異なる) → false
		System.assertEquals(false, AttDayType.WORKDAY.equals(AttDayType.getType('Holiday')));

		// 等しくない場合(クラスの型が異なる) → false
		System.assertEquals(false, AttDayType.WORKDAY.equals(Integer.valueOf(123)));

		// 等しくない場合(null)
		System.assertEquals(false, AttDayType.WORKDAY.equals(null));
	}
}
