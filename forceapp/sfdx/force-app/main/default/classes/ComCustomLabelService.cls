/**
 * @group 共通
 *
 * @description カスタムラベルサービス
 */
public with sharing class ComCustomLabelService {

	/** カスタムラベルMap(Key:language) */
	private static Map<String, Map<String, String>> customMsgMap;

	/**
	 * @description ユーザによって変更されたカスタムラベルを取得する
	 * @return カスタムラベル(Mak<言語, Map<メッセージID, メッセージ>>)
	 */
	public Map<String, Map<String, String>> getCustomLabelMap() {
		if (customMsgMap != null) {
			return customMsgMap;
		}

		Map<String, String> jaMap = new Map<String, String>();
		Map<String, String> enUsMap = new Map<String, String>();
		for (ComCustomLabelEntity label : getCustomMessageListAll()) {
			if (String.isNotEmpty(label.enUsCustom)) {
				enUsMap.put(label.developerName, label.enUsCustom);
			}
			if (String.isNotEmpty(label.jaCustom)) {
				jaMap.put(label.developerName, label.JaCustom);
			}
		}
		customMsgMap = new Map<String, Map<String, String>>();
		customMsgMap.put(AppLanguage.JA.value, jaMap);
		customMsgMap.put(AppLanguage.EN_US.value, enUsMap);
		return customMsgMap;
	}

	/**
	 * @description カスタムラベルを取得する。
	 * ユーザが変更している場合は変更後のラベル、変更していない場合はデフォルトラベルを取得する。
	 * @param messageId 取得対象のメッセージID
	 * @param lang 取得対象の言語
	 * @return カスタムラベル、ただしラベルが存在しない場合はnull
	 */
	public String getCustomLabel(String messageId, AppLanguage lang) {
		ComCustomLabelEntity label = getCustomMessageListByDeveloperName(messageId);
		if (label == null) {
			return null;
		}
		return label.getLabelByLang(lang);
	}

	/**
	 * @description デフォルトラベルを取得する。
	 * @param messageId 取得対象のメッセージID
	 * @param lang 取得対象の言語
	 * @return カスタムラベル、ただしラベルが存在しない場合はnull
	 */
	public String getDefaultLabel(String messageId, AppLanguage lang) {
		ComCustomLabelEntity label = getCustomMessageListByDeveloperName(messageId);
		if (label == null) {
			return null;
		}
		return label.getDefaultLabelByLang(lang);
	}

	// TODO 要リファクタリング。可能であれば動的SOQLにしてクエリ実行処理を共通化する。
	/**
	 * @description 全てのカスタムラベルを取得する
	 * @return カスタムラベルのリスト
	 */
	private List<ComCustomLabelEntity> getCustomMessageListAll() {
		List<ComCustomLabel__mdt> labelObjList = [SELECT DeveloperName, QualifiedApiName, MasterLabel,
				En_US_Default__c, En_US_Custom__c, Ja_Default__c, Ja_Custom__c
				FROM ComCustomLabel__mdt];
		List<ComCustomLabelEntity> labelList = new List<ComCustomLabelEntity>();
		for (ComCustomLabel__mdt obj : labelObjList) {
			labelList.add(new ComCustomLabelEntity(obj));
		}
		return labelList;
	}

	// TODO 要リファクタリング。可能であれば動的SOQLにしてクエリ実行処理を共通化する
	/**
	 * @description 指定したDeveloperName(メッセージID)のカスタムラベルを取得する
	 * @return カスタムラベル、ただし存在しない場合はnull
	 */
	private ComCustomLabelEntity getCustomMessageListByDeveloperName(String developerName) {
		List<ComCustomLabel__mdt> labelList = [SELECT DeveloperName, QualifiedApiName, MasterLabel,
				En_US_Default__c, En_US_Custom__c, Ja_Default__c, Ja_Custom__c
				FROM ComCustomLabel__mdt
				WHERE DeveloperName = :developerName];

		return labelList.isEmpty() ? null : new ComCustomLabelEntity(labelList[0]);
	}
}