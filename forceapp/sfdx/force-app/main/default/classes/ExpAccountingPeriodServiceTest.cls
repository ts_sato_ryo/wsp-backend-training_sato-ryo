/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Common
 *
 * @description Test class for ExpAccountingPeriodService
 */
@isTest
private class ExpAccountingPeriodServiceTest {

	static final ComMsgBase MESSAGE = ComMessage.msg();

	static final ComCountry__c countryObj = ComTestDataUtility.createCountry('JPN');
	static final ComCompany__c companyObj = ComTestDataUtility.createCompany('Test Co. Ltd.', countryObj.Id);

	/**
	 * Create a new Accounting Period record(Positive case)
	 */
	@isTest
	static void createAccountingPeriodTest() {
		ExpAccountingPeriodEntity entity = new  ExpAccountingPeriodEntity();
		setTestParam(entity);

		Test.startTest();

		// Create new record
		Repository.SaveResult result = ExpAccountingPeriodService.createAccountingPeriod(entity);

		Test.stopTest();

		System.assertEquals(1, result.details.size());

		List<ExpAccountingPeriod__c> records =
		[SELECT
			Code__c,
			UniqKey__c,
			CompanyId__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			ValidFrom__c,
			ValidTo__c,
			RecordingDate__c,
			Active__c
			FROM ExpAccountingPeriod__c
			WHERE Id =: result.details[0].id];

		// Confirm new record was created
		System.assertEquals(1, records.size());

		assertFields(entity, records[0]);
	}

	/**
	 * Create a new Accounting Period record(Positive case : same code in different companies can be saved)
	 */
	@isTest
	static void createAccountingPeriodUniqTest() {
		ExpAccountingPeriodEntity entity = new  ExpAccountingPeriodEntity();
		setTestParam(entity);

		Test.startTest();

		// Create one record
		ExpAccountingPeriodService.createAccountingPeriod(entity);

		ComCompany__c newCompany = ComTestDataUtility.createCompany('New Test Co. Ltd.', countryObj.Id);
		ExpAccountingPeriodEntity newEntity = new  ExpAccountingPeriodEntity();
		setTestParam(newEntity);
		newEntity.companyId = newCompany.Id;

		// Create new record with same code in different company
		Repository.SaveResult result = ExpAccountingPeriodService.createAccountingPeriod(newEntity);

		Test.stopTest();

		List<ExpAccountingPeriod__c> records =
		[SELECT
			Code__c,
			CompanyId__c
			FROM ExpAccountingPeriod__c];

		// Confirm new record was created
		System.assertEquals(2, records.size());

		System.assertEquals(records[0].Code__c, records[1].Code__c);
		System.assertNotEquals(records[0].CompanyId__c, records[1].CompanyId__c);
	}

	/**
   * Create a new Accounting Period record(Negative case: Same code exists)
   */
	@isTest
	static void createAccountingPeriodDuplicateErrTest() {

		// Create 'TEST1' code record first
		ComTestDataUtility.createExpAccountingPeriod('TEST', companyObj.Id);

		ExpAccountingPeriodEntity entity = new  ExpAccountingPeriodEntity();
		setTestParam(entity);
		entity.code = 'TEST1';

		Test.startTest();

		APP.ParameterException ex;
		try {
			// Try to create new 'TEST1' code record again
			ExpAccountingPeriodService.createAccountingPeriod(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
	   * Update Accounting Period record(Positive case)
	   */
	@isTest
	static void updateAccountingPeriodTest() {
		// Create a Accounting Period record first
		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		ExpAccountingPeriodEntity entity = new ExpAccountingPeriodEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		Test.startTest();

		ExpAccountingPeriodService.updateAccountingPeriod(entity);

		Test.stopTest();

		List<ExpAccountingPeriod__c> records =
		[SELECT
			Code__c,
			UniqKey__c,
			CompanyId__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			ValidFrom__c,
			ValidTo__c,
			RecordingDate__c,
			Active__c
			FROM ExpAccountingPeriod__c
			WHERE Id =: testRecord.id];

		//Confirm the record was updated
		System.assertEquals(1, records.size());

		assertFields(entity, records[0]);
	}

	/**
   * Update Accounting Period record(Negative case: Update code is duplicate)
   */
	@isTest
	static void updateAccountingPeriodDuplicateErrTest() {
		// Create test records
		ExpAccountingPeriod__c test1Record = ComTestDataUtility.createExpAccountingPeriod('Test1', companyObj.Id);
		ExpAccountingPeriod__c test2Record = ComTestDataUtility.createExpAccountingPeriod('Test2', companyObj.Id);

		// Create Test1 entity
		ExpAccountingPeriodEntity entity = new ExpAccountingPeriodEntity();
		setTestParam(entity);
		entity.setId(test1Record.Id);

		// Set Test2 code to Test1 entity
		entity.code = test2Record.Code__c;

		Test.startTest();

		APP.ParameterException ex;
		try {
			ExpAccountingPeriodService.updateAccountingPeriod(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		// Confirm duplicate error occurred
		System.assertEquals(APP.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_DuplicateCode, ex.getMessage());
	}

	/**
 * Update Accounting Period record(Negative case: Target record is no found)
 */
	@isTest
	static void updateAccountingPeriodNotFoundTest() {
		// Create a Accounting Period record first
		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		ExpAccountingPeriodEntity entity = new ExpAccountingPeriodEntity();
		setTestParam(entity);
		entity.setId(testRecord.Id);

		// Delete the record before updating
		delete testRecord;

		Test.startTest();

		APP.ParameterException ex;
		try {
			ExpAccountingPeriodService.updateAccountingPeriod(entity);
		} catch (APP.ParameterException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm the expected error occurred
		System.assertEquals(App.ERR_CODE_INVALID_PARAMETER, ex.getErrorCode());
		System.assertEquals(MESSAGE.Com_Err_RecordNotFound, ex.getMessage());
	}

	/*
	 * Test that Recording Date can be changed only if it's not used in Transactions
	 */
	@isTest
	static void updateAccountingPeriodAlreadyInUsedTest() {
		List<ExpAccountingPeriod__c> accPeriodList = ComTestDataUtility.createExpAccountingPeriods('Test', companyObj.Id, 2);
		ExpAccountingPeriod__c alreadyInUsedAccPeriodSObj = accPeriodList[0];
		ExpAccountingPeriod__c floatingAccPeriodSObj = accPeriodList[1];
		ExpReportType__c reportType = ComTestDataUtility.createExpReportType('RT', companyObj.Id);

		// Link the Accounting Period with Report
		ExpReport__c expReport = new ExpReport__c(
			AccountingPeriodId__c = alreadyInUsedAccPeriodSObj.Id,
			ExpReportTypeId__c = reportType.Id,
			AccountingDate__c =  alreadyInUsedAccPeriodSObj.RecordingDate__c
		);
		INSERT expReport;

		ExpAccountingPeriodEntity inUsedEntity = convertToEntity(alreadyInUsedAccPeriodSObj);

		Test.startTest();
		// Update Recording Date of Accounting Period already used by Report -> NG
		inUsedEntity.recordingDate = AppDate.valueOf(alreadyInUsedAccPeriodSObj.RecordingDate__c).addDays(1);
		try {
			ExpAccountingPeriodService.updateAccountingPeriod(inUsedEntity);
			System.assert(false, 'Exception Not Thrown!');
		} catch (Exception e) {
			System.assertEquals(true, e instanceof App.ParameterException);
			System.assertEquals(MESSAGE.Com_Err_CannotChangeReference, e.getMessage());
		}

		// Update Recording Date of Accounting Period NOT used by any Report -> OK
		ExpAccountingPeriodEntity floatingEntity = convertToEntity(floatingAccPeriodSObj);
		floatingEntity.recordingDate = floatingEntity.recordingDate.addDays(1);
		ExpAccountingPeriodService.updateAccountingPeriod(floatingEntity);
		ExpAccountingPeriod__c retSObj = [SELECT ID, RecordingDate__c FROM ExpAccountingPeriod__c WHERE ID = :floatingEntity.id];
		System.assertEquals(floatingEntity.recordingDate, AppDate.valueOf(retSObj.RecordingDate__c));
		Test.stopTest();
	}

	/**
	 * Delete Accounting Period record(Positive case)
	 */
	@isTest
	static void deleteAccountingPeriodTest() {

		// Create the target data to delete
		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		Id id = testRecord.Id;

		Test.startTest();

		// Delete the target
		ExpAccountingPeriodService.deleteAccountingPeriod(id);

		Test.stopTest();

		//Confirm the target record was deleted
		System.assertEquals(0, [SELECT COUNT() FROM ExpAccountingPeriod__c WHERE Id =: id]);
	}

	/**
   * Delete Accounting Period record(Positive case: Target has been deleted)
   */
	@isTest
	static void deleteAccountingPeriodAlreadyDeletedTest() {

		// Create the target data to delete
		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		Id id = testRecord.Id;

		// Delete the target record in advance
		delete testRecord;

		Test.startTest();

		DmlException ex = null;
		try {
			// Try to Delete the target
			ExpAccountingPeriodService.deleteAccountingPeriod(id);
		} catch (DmlException e) {
			ex = e;
		}

		Test.stopTest();

		//Confirm exception did not occur
		System.assertEquals(null, ex);
	}

	/**
   * Search AccountingPeriod data(Positive case)
 	 */
	@isTest
	static void searchAccountingPeriodTest() {

		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		Map<String, Object> param = new Map<String, Object>();
		// All conditions
		param.put('companyId', testRecord.CompanyId__c);
		param.put('active', testRecord.Active__c);

		List<ExpAccountingPeriodEntity> result =  ExpAccountingPeriodService.searchAccountingPeriod(param);
		System.assertEquals(1, result.size());

		System.assertEquals(testRecord.Id, result[0].id);
		assertFields(result[0], testRecord);

		// Company ID
		param.clear();
		param.put('companyId', testRecord.CompanyId__c);
		result = ExpAccountingPeriodService.searchAccountingPeriod(param);
		System.assertEquals(1, result.size());

		// Active
		param.clear();
		param.put('active', testRecord.Active__c);
		result = ExpAccountingPeriodService.searchAccountingPeriod(param);
		System.assertEquals(1, result.size());
	}

	/**
	 * Search AccountingPeriod data(Negative case)
	 */
	@isTest
	static void searchAccountingPeriodNoResultTest() {

		ExpAccountingPeriod__c testRecord = ComTestDataUtility.createExpAccountingPeriod('Test', companyObj.Id);

		Map<String, Object> param = new Map<String, Object>();
		List<ExpAccountingPeriodEntity> result;

		// Invalid Company ID
		param.clear();
		param.put('companyId', testRecord.Id);
		result = ExpAccountingPeriodService.searchAccountingPeriod(param);
		System.assertEquals(0, result.size());

		// Invalid Active
		param.clear();
		param.put('active', !testRecord.Active__c);
		result = ExpAccountingPeriodService.searchAccountingPeriod(param);
		System.assertEquals(0, result.size());
	}

	/**
	 * Search all AccountingPeriod data(Positive case)
   */
	@isTest
	static void searchAccountingPeriodAllTest() {

		ComTestDataUtility.createExpAccountingPeriods('Test', companyObj.Id, 3);
		// Fetch data order by code
		List<ExpAccountingPeriod__c> testRecords = [SELECT
			Id,
			Code__c,
			UniqKey__c,
			CompanyId__c,
			Name_L0__c,
			Name_L1__c,
			Name_L2__c,
			ValidFrom__c,
			ValidTo__c,
			RecordingDate__c,
			Active__c
			FROM ExpAccountingPeriod__c
		ORDER BY Code__c NULLS LAST];

		Test.startTest();

		// When searching by nul ID, return all data
		List<ExpAccountingPeriodEntity> result =  ExpAccountingPeriodService.searchAccountingPeriod(null);

		Test.stopTest();

		// Confirm all data were fetched
		System.assertEquals(testRecords.size(), result.size());

		for (Integer i = 0, n = testRecords.size(); i < n; i++) {
			System.assertEquals(testRecords[i].Id, result[i].id);
			assertFields(result[i], testRecords[i]);
		}
	}

	/**
   * Set test parameter to entity
   * @param param Target object to set data
   */
	static void setTestParam(ExpAccountingPeriodEntity entity) {
		entity.code = 'TEST';
		entity.companyId = companyObj.Id;
		entity.nameL0 = 'Period L0';
		entity.nameL1 = 'Period L1';
		entity.nameL2 = 'Period L2';
		entity.validFrom = AppDate.today();
		entity.validTo = AppDate.today().addMonths(1);
		entity.recordingDate = entity.validTo.addDays(-1);
		entity.active = true;
	}

	static void assertFields(ExpAccountingPeriodEntity entity, ExpAccountingPeriod__c record) {
		System.assertEquals(entity.code, record.Code__c);
		System.assertEquals(entity.uniqKey, record.UniqKey__c);
		System.assertEquals(entity.companyId, record.CompanyId__c);
		System.assertEquals(entity.nameL0, record.Name_L0__c);
		System.assertEquals(entity.nameL1, record.Name_L1__c);
		System.assertEquals(entity.nameL2, record.Name_L2__c);
		System.assertEquals(entity.validFrom, AppDate.valueOf(record.ValidFrom__c));
		System.assertEquals(entity.validTo, AppDate.valueOf(record.ValidTo__c));
		System.assertEquals(entity.recordingDate, AppDate.valueOf(record.RecordingDate__c));
		System.assertEquals(entity.active, record.Active__c);
	}

	/*
	 * Converts and return an Entity for the provided sObject.
	 */
	private static ExpAccountingPeriodEntity convertToEntity(ExpAccountingPeriod__c accPeriodSObj) {
		ExpAccountingPeriodEntity accPeriodEntity = new ExpAccountingPeriodEntity();
		accPeriodEntity.setId(accPeriodSObj.Id);
		accPeriodEntity.code = accPeriodSObj.Code__c;
		accPeriodEntity.companyId = accPeriodSObj.CompanyId__c;
		accPeriodEntity.nameL0 = accPeriodSObj.Name_L0__c;
		accPeriodEntity.nameL1 = accPeriodSObj.Name_L1__c;
		accPeriodEntity.nameL2 = accPeriodSObj.Name_L2__c;
		accPeriodEntity.validFrom = AppDate.valueOf(accPeriodSObj.ValidFrom__c);
		accPeriodEntity.validTo = AppDate.valueOf(accPeriodSObj.ValidTo__c);
		accPeriodEntity.recordingDate = AppDate.valueOf(accPeriodSObj.RecordingDate__c);
		accPeriodEntity.active = true;
		return accPeriodEntity;
	}
}