/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 履歴管理方式が親子型マスタのサービスクラス
 */
public abstract with sharing class ParentChildService implements Service {
	/** メッセージ */
	public static final ComMsgBase MESSAGE = ComMessage.msg();

	/** 履歴マスタのリポジトリ */
	private ParentChildRepository mainRepo;

	/** 指定したベースIDのベースエンティティを取得する */
	protected abstract ParentChildBaseEntity getEntity(Id baseId);

	/** 指定したベースIDのベースエンティティを取得する */
	protected abstract List<ParentChildBaseEntity> getEntityList(List<Id> baseIds);

	/** 指定したベースIDのベースエンティティを取得する */
	protected abstract ParentChildBaseEntity getEntityByCode(String code);

	/**
	 * エンティティのコードが重複しているかどうかを確認する(会社単位での重複チェック)
	 * @param base チェック対象のベースエンティティ
	 * @param companyCode 会社コード
	 * @return 重複している場合はtrue, そうでない場合はfalse
	 */
	protected abstract Boolean isCodeDuplicated(ParentChildBaseEntity base);

	/** 指定した履歴IDの履歴エンティティを取得する */
	protected abstract ParentChildHistoryEntity getHistoryEntity(Id historyId);

	/** 指定した履歴IDの履歴エンティティを取得する */
	protected abstract List<ParentChildHistoryEntity> getHistoryEntityList(List<Id> historyIds);
	/** 指定履歴エンティティにより回帰チェックを行う */
	protected virtual void validateCircular(ParentChildBaseEntity base, List<ParentChildHistoryEntity> historyList) {return;}
	/** 改定内容を伝搬する */
	public class PropagationResult {
		/** 保存する履歴のリスト */
		public List<ParentChildHistoryEntity> saveHistories;
		/** 論理削除する履歴のリスト */
		public List<ParentChildHistoryEntity> deleteHistories;
	}

	/**
	 * 検証結果クラス
	 */
	public virtual class ValidateResult {
		public Boolean hasError {get {return this.exceptionList == null || this.exceptionList.isEmpty() ? false : true;}}
		public List<Exception> exceptionList;
		public ValidateResult() {
			exceptionList = new List<Exception>();
		}
	}

	/**
	 * コンストラクタ(メインで扱うマスタのリポジトリを設定)
	 */
	public ParentChildService(ParentChildRepository mainRepo) {
		this.mainRepo = mainRepo;
	}

	/**
	 * 履歴エンティティの値を検証する
	 * @param history 検証対象の履歴エンティティ
	 * @param base 参照用のベースエンティティ（検証はしない）
	 * @return 検証結果
	 */
	protected abstract Validator.Result validateSaveHistoryEntity(ParentChildBaseEntity base, ParentChildHistoryEntity history);

	/**
	 * ベースエンティティの値を検証する
	 * @param base 検証対象のエンティティ
	 * @return 検証結果
	 */
	protected abstract Validator.Result validateSaveBaseEntity(ParentChildBaseEntity base);

	/**
	 * 更新対象項目の値をorgBaseに設定する。updateBaseの変更した項目の値のみ設定する
	 * @param 更新した値を設定するエンティティ
	 * @param 更新値が設定されているエンティティ
	 */
	protected void setUpdateValueToEntity(ParentChildBaseEntity orgBase, ParentChildBaseEntity updateBase) {
		for (Schema.SObjectField field : updateBase.getChangedFieldSet()) {
				orgBase.setFieldValue(field, updateBase.getFieldValue(field));
		}
	}


	/**
	 * マスタを新規に登録する(ベース+履歴)
	 * 会社に紐づくマスタを新規登録する
	 * @param base 新規登録するマスタのベース(uniqKey項目にも値を設定しておくこと)
	 */
	public virtual Id saveNewEntity(ParentChildBaseEntity base) {

		// ベースエンティティの項目の値を検証する
		Validator.Result baseResult = validateSaveBaseEntity(base);
		if (! baseResult.isSuccess()) {
			Validator.Error error = baseResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// コードの重複チェック
		if (isCodeDuplicated(base)) {
			// メッセージ：コードが重複しています
			throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
		}

		// 履歴が存在することをチェック
		if (base.getSuperHistoryList().isEmpty()) {
			// TODO エラーメッセージ多言語化(不具合以外でここにくることはなさそうですが。。)
			throw new App.ParameterException('新規登録時に履歴データがありません。');
		}

		// 履歴エンティティのバリデーション
		for (ParentChildHistoryEntity history : base.getSuperHistoryList()) {
			history.uniqKey = history.createUniqKey(base.getUniqCode());
			Validator.Result historyResult = validateSaveHistoryEntity(base, history);
			if (! historyResult.isSuccess()) {
				Validator.Error error = historyResult.getErrors().get(0);
				App.ParameterException e = new App.ParameterException(error.message);
				e.setCode(error.code);
				throw e;
			}
		}

		//------------------------
		// TODO 履歴間の有効期間のチェック
		//------------------------
		// 保存する
		Repository.SaveResultWithChildren baseRes = mainRepo.saveEntity(base);
		Id baseId = baseRes.details[0].id;

		// 階層チェックを行う
		ParentChildBaseEntity parentEntity = getEntity(baseId);
		validateCircular(base, parentEntity.getSuperHistoryList());

		// CurrentHistoryIdを更新する(保存後でないとbaseIdがわからない)
		updateCurrentHistory(baseId);

		return baseId;
	}

	/**
	 * ベースを更新する。更新対象外の項目にも既に値が設定されている
	 * @poram updateBase 更新値が設定されているエンティティ(uniqKey項目にも値を設定しておくこと)
	 */
	public virtual void updateBase(ParentChildBaseEntity updateBase) {

		// 更新対象のレコード存在確認
		ParentChildBaseEntity base = getEntity(updateBase.id);
		if (base == null) {
			// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
		}

		String orgUniqCode = base.getUniqCode();

		// baseに更新する値を設定する
		setUpdateValueToEntity(base, updateBase);

		// コードの重複チェック
		Boolean isChangedCode = false;
		if (updateBase.getUniqCode() != null && orgUniqCode != updateBase.getUniqCode()) {
			if (isCodeDuplicated(base)) {
				// メッセージ：コードが重複しています
				throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
			}
			isChangedCode = true;
		}

		// 保存対象のエンティティを検証する
		Validator.Result baseResult = validateSaveBaseEntity(base);
		if (! baseResult.isSuccess()) {
			Validator.Error error = baseResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// ベースを保存する
		mainRepo.saveBaseEntity(base);

		// ベース項目の値に連動している履歴項目を更新する
		updateHistoryList(base, isChangedCode);

	}

	/**
	 * ベースの情報に基づき履歴エンティティを更新する
	 */
	protected virtual void updateHistoryList(ParentChildBaseEntity base, Boolean isChangedCode) {

		Boolean hasChanged = false;
		List<ParentChildHistoryEntity> updateHistoryList = base.getSuperHistoryList();

		// コードが変更されているのであれば履歴のユニークキーも変更する
		if (isChangedCode) {
			hasChanged = true;
			for (ParentChildHistoryEntity history : updateHistoryList) {
				history.uniqKey = history.createUniqKey(base.getUniqCode());
			}
		}

		// 社員の場合、Name項目を更新する
		// 本メソッドをoverrideした方が良いかもしれない。。。
		if (base instanceof EmployeeBaseEntity) {
			EmployeeBaseEntity empBase = (EmployeeBaseEntity)base;
			if (empBase.isChanged(EmployeeBaseGeneratedEntity.FIELD_DISPLAY_NAME_L0)) {
				hasChanged = true;
				for (ParentChildHistoryEntity history : updateHistoryList) {
					history.name = empBase.displayNameL0;
				}
			}
		}

		if (hasChanged) {
			mainRepo.saveHistoryEntityList(updateHistoryList);
		}
	}


	/**
	 * 履歴を改定する
	 * @param revisionHistory 改定後履歴エンティティの情報。validFromには改定日を設定しておくこと。validToは設定不要。
	 */
	public virtual void reviseHistory(ParentChildHistoryEntity revisionHistory) {

		//-------------------------------------------------------------
		// 各マスタ独自の処理を実装したい場合は本メソッドをoverrideしてください
		//-------------------------------------------------------------

		// ベースに紐づく全ての履歴を取得する
		ParentChildBaseEntity base = getEntity(revisionHistory.baseId);
		if (base == null) {
			// メッセージ：対象のデータが見つかりませんでした。削除されている可能性があります。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_DeleteRecordNotFound);
		}

		// 改定する
		reviseHistory(base, revisionHistory);
	}

	/**
	 * 履歴を改定する
	 * @param base 全ての有効な履歴を持っているベースエンティティ
	 * @param revisionHistory 改定後履歴エンティティの情報。validFromには改定日を設定しておくこと。validToは設定不要。
	 */
	protected void reviseHistory(ParentChildBaseEntity base, ParentChildHistoryEntity revisionHistory) {

		// 改定対象の履歴を取得する
		AppDate revisionDate = revisionHistory.validFrom;
		ParentChildHistoryEntity prevHistory = base.getSuperHistoryByDate(revisionDate); // 改定前履歴
		if (prevHistory == null) {
			// メッセージ：改定日がデータの有効期間外です。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_HistoryNotFound);
		}

		// 改定後履歴を作成する(下記は6/1で改定した場合)
		// 改定前：|-------------------------------------|
		//      4/1           改定前履歴                8/1
		// 改定後：|----------------|--------------------|
		//      4/1    改定前履歴  6/1     改定後履歴     8/1
		ParentChildHistoryEntity afterHistory = buildAfterRevisionHistory(prevHistory, revisionHistory);

		// 伝搬対象の履歴を取得する
		// 改定前履歴の失効日以降の履歴が伝搬対象
		List<ParentChildHistoryEntity> propagationList =
				base.getSuperHistoryIncludedPeriod(prevHistory.validTo, base.getLatestSuperHistory().validTo);

		// 伝搬を実行する
		ParentChildService.PropagationResult propagationResult =
				propagate(prevHistory, afterHistory, propagationList, base.getUniqCode());


		// 改定前履歴と改定後履歴の有効開始日と失効日を更新する
		// 改定後履歴の改定日 = 改定日
		// 改定後履歴の失効日 = 改定前履歴の元の失効日
		// 改定前履歴の失効日 = 改定日
		afterHistory.validFrom = revisionHistory.validFrom;
		afterHistory.validTo = prevHistory.validTo;
		prevHistory.validTo = revisionHistory.validFrom;

		String code = base.getUniqCode();
		prevHistory.uniqKey = prevHistory.createUniqKey(code);
		afterHistory.uniqKey = afterHistory.createUniqKey(code);
		propagationResult.saveHistories.add(afterHistory);

		// 同日改定の場合(改定前履歴と改定後履歴の有効開始日が等しい場合)は、
		// 改定前履歴を論理削除する
		if (prevHistory.validFrom.getDate() == afterHistory.validFrom.getDate()) {
			// 論理削除
			propagationResult.deleteHistories.add(prevHistory);
		} else {
			// 改定前履歴の失効日が変更されているので保存する
			propagationResult.saveHistories.add(prevHistory);
		}

		// 改定後履歴の値を検証する
		// revisionHistoryには改定内容しか設定されていない場合があるため(必須項目が設定されていない場合がある)
		// 改定処理が完了した時点の保存前の改定後履歴が正しいかどうかを検証することにする。
		Validator.Result historyResult = validateSaveHistoryEntity(base, afterHistory);
		if (! historyResult.isSuccess()) {
			Validator.Error error = historyResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// 履歴を論理削除する
		// TODO 参照されていなければ物理削除
		if (! propagationResult.deleteHistories.isEmpty()) {
			logicallyDeleteHistoryList(propagationResult.deleteHistories);
		}

		// 伝搬内容を保存する
		mainRepo.saveHistoryEntityList(propagationResult.saveHistories);

		// 階層関係チェックを行う
		validateCircular(base, propagationResult.saveHistories);

		// ベースのCurrentIdを更新する
		updateCurrentHistory(base.id);

	}

	/**
	 * 改定前履歴と改定内容から改定後履歴を作成する
	 * @param superPrevHistory 改定前履歴
	 * @param superRevisionHistory 改定内容
	 * @return 改定後履歴
	 */
	protected ParentChildHistoryEntity buildAfterRevisionHistory(
			ParentChildHistoryEntity prevHistory, ParentChildHistoryEntity revisionHistory) {

		// 値が変更された項目のみを設定する
		ParentChildHistoryEntity afterHistory = prevHistory.copySuperEntity();
		for (Schema.SObjectField field : revisionHistory.getChangedFieldSet()) {
				afterHistory.setFieldValue(field, revisionHistory.getFieldValue(field));
		}
		return afterHistory;
	}

	/**
	 * 伝搬処理を実行する
	 * @param superPrevHistory 改定前履歴
	 * @param superAfterHistory 改定後履歴
	 * @param propagetionList 伝搬対象の履歴リスト
	 * @param code コード
	 * @return 伝搬処理結果
	 */
	protected ParentChildService.PropagationResult propagate(
			ParentChildHistoryEntity prevHistory, ParentChildHistoryEntity afterHistory,
			List<ParentChildHistoryEntity> propagetionList, String code) {

		// 伝搬対象が存在しないのであれば、ここで終了
		if (propagetionList.isEmpty()) {
			ParentChildService.PropagationResult result;
			result = new ParentChildService.PropagationResult();
			result.saveHistories = new List<ParentChildHistoryEntity>();
			result.deleteHistories = new List<ParentChildHistoryEntity>();
			return result;
		}

		// 保存対象の履歴リスト
		List<ParentChildHistoryEntity> saveHistories = new List<ParentChildHistoryEntity>();
		// 論理削除対象の履歴リスト
		List<ParentChildHistoryEntity> deleteHistories = new List<ParentChildHistoryEntity>();

		// 伝搬対象項目リストを作成する
		Set<Schema.SObjectField> propaFieldSet =
				createPropagationField(prevHistory, afterHistory, propagetionList[0]);
		for (ParentChildHistoryEntity history : propagetionList) {

			// 伝搬対象項目がなくなったら終了
			if (propaFieldSet.isEmpty()) {
				break;
			}

			ParentChildHistoryEntity cloneHistory = null;
			for (Schema.SObjectField field : propaFieldSet) {

				// 改定前の履歴と値が異なる場合は伝搬対象リストから削除
				Object fieldValue = history.getFieldValue(field);
				Object prevFieldValue = prevHistory.getFieldValue(field);
				if (prevFieldValue != fieldValue) {
					propaFieldSet.remove(field);
					continue;
				}

				// 伝搬対象のエンティティの複製を作成する
				if (cloneHistory == null) {
					cloneHistory = history.copySuperEntity();
					saveHistories.add(cloneHistory);

					// 複製した履歴に改定後の値を設定するため、
					// 複製元の履歴は論理削除
					deleteHistories.add(history);
				}

				// 複製した履歴に改定値を設定する
				Object afterFieldValue = afterHistory.getFieldValue(field);
				cloneHistory.setFieldValue(field, afterFieldValue);
			}
		}

		ParentChildService.PropagationResult result = new ParentChildService.PropagationResult();
		result.saveHistories = saveHistories;
		result.deleteHistories = deleteHistories;

		return result;
	}

	/**
	 * 伝搬対象項目リストを作成する
	 * @param prevHistory 改定前履歴
	 * @param afterHistory 改定後履歴
	 * @param headHistory 伝搬対象リストの先頭の履歴
	 * @return 伝搬対象項目リスト
	 */
	private Set<Schema.SObjectField> createPropagationField(ParentChildHistoryEntity prevHistory,
			ParentChildHistoryEntity afterHistory, ParentChildHistoryEntity headHistory) {

		Set<String> nonPropagationFieldNameSet = headHistory.getNonPropagationFieldNameSet();
		Set<Schema.SObjectField> propagationFieldSet = new Set<Schema.SObjectField>();
		for (Schema.SObjectField field : afterHistory.getChangedFieldSet()) {
			if (nonPropagationFieldNameSet.contains(field.getDescribe().getName())) {
				continue;
			}
			Object prevValue = prevHistory.getFieldValue(field);
			Object afterValue = afterHistory.getFieldValue(field);
			Object headValue = headHistory.getFieldValue(field);

			// 改定前履歴と改定後履歴の値が異なっている
			// かつ、改定前履歴と伝搬対象の先頭の履歴の値が等しい場合に伝搬対象項目とする
			if (prevValue != afterValue && prevValue == headValue) {
				propagationFieldSet.add(field);
			}
		}

		return propagationFieldSet;
	}

	/**
	 * マスタ全体の失効日を更新する Update the expiration date of the entire maste
	 *  - 履歴を失効日に合わせて調整する Adjust Master's History to Expiration Date
	 *    - 有効期間を延長する場合：最新の履歴の失効日を延長する
	 *    - 有効期間を短縮する場合：失効日以降の履歴は論理削除する
	 * @param baseId 更新対象のマスタのベースID
	 * @param newValidTo 更新後のマスタ全体の失効日
	 */
	public virtual void updateBaseValidToDate(Id baseId, AppDate newValidTo) {

		//-------------------------------------------------------------
		// マスタ独自の処理を実装したい場合は本メソッドをoverrideしてください
		// Please override this method if you want to implement the master's own processing
		//------------------------------------------------------------

		ParentChildBaseEntity base = getEntity(baseId);

		// 失効日のチェック
		// マスタ全体の有効開始日 < 失効日 <= 2101/01/01
		if (newValidTo.isBeforeEquals(base.getValidStartDate())) {
			// エラーメッセージ [%1]は有効期間開始日以降を指定してください
			throw new App.ParameterException(
					MESSAGE.Admin_Err_InvalidValidEndDateAfterStart(new List<String>{base.getValidEndDateLabel()}));
		}
		if (newValidTo.isAfter(ParentChildHistoryEntity.VALID_TO_MAX)) {
			// エラーメッセージ [%1]は[%2]より前に指定してください。
			throw new App.ParameterException(
					MESSAGE.Admin_Err_InvalidValidEndDateMax(
						new List<String>{base.getValidEndDateLabel(),
						ParentChildHistoryEntity.VALID_TO_MAX.getDate().format()}));
		}

		// 履歴をマスタ全体の失効日に合わせる
		List<ParentChildHistoryEntity> updateHistoryList = new List<ParentChildHistoryEntity>();
		ParentChildHistoryEntity latestHistory = base.getLatestSuperHistory();
		if (newValidTo.isAfter(latestHistory.validTo)) {
			// マスタ全体の有効期間を延長する場合
			// 最新の履歴の有効期間終了日を延長する
			latestHistory.validTo = newValidTo;
			updateHistoryList.add(latestHistory);
		} else {
			// マスタ全体の有効期間を短縮する場合
			// 有効期間終了日に有効な履歴の失効日を更新し、失効日以降の履歴を論理削除する
			for (ParentChildHistoryEntity history : base.getSuperHistoryList()) {
				Boolean isUpdated = false;

				if (history.isValid(newValidTo.addDays(-1))) {
					// マスタ全体の有効期間終了日(失効日-1)に有効な履歴の場合
					// 本履歴が最新の履歴となる
					history.validTo = newValidTo;
					isUpdated = true;
				} else if (history.validFrom.isAfterEquals(newValidTo)) {
					// マスタ全体の失効日以降の履歴の場合
					// 論理削除する
					history.isRemoved = true;
					isUpdated = true;
				}

				if (isUpdated) {
					updateHistoryList.add(history);
				}
			}
		}

		// 履歴を保存する
		if (! updateHistoryList.isEmpty()) {
			mainRepo.saveHistoryEntityList(updateHistoryList);
		}

		// ベースのCurrentIdを更新する
		updateCurrentHistory(base.id);
	}

	/**
	 * TODO 管理画面でもマスタ全体の失効日の設定ができるようになったら updateBaseValidTo(Id, AppDate)に移行すること
	 *
	 * マスタ全体の失効日を更新する(管理画面用)
	 * 本メソッドの処理は暫定対応のため更新後の失効日は最新の履歴の有効開始日より後にしておく必要がある。
	 * @param baseId 更新対象のマスタのベースID
	 * @param validTo 更新後の失効日
	 */
	public void updateValidTo(Id baseId, AppDate validTo) {

		ParentChildBaseEntity base = getEntity(baseId);
		ParentChildHistoryEntity latestHistory = base.getLatestSuperHistory();

		// 現在は、更新後の失効日は最新の履歴の有効開始日より後にしておく必要がある
		if (latestHistory.validFrom.getDate() >= validTo.getDate()) {
			// メッッセージ：失効日は最新履歴の有効開始日より後に設定してください。
			throw new App.ParameterException(MESSAGE.Admin_Err_InvalidValidTo);
		}

		// 最新履歴の失効日を更新する
		latestHistory.validTo = validTo;
		mainRepo.saveHistoryEntity(latestHistory);

		// 最新履歴の失効日を更新しても、ベースのCurrentHistoryIdが変更されることはないはず
		// よってCurrentHistoryIdの更新は行わない
	}

	/**
	 * 履歴を論理削除する
	 * @param histories 論理削除対象の履歴リスト
	 */
	public void logicallyDeleteHistoryList(List<ParentChildHistoryEntity> histories) {

		for (ParentChildHistoryEntity history : histories) {
			history.setLogicallyDelete();
		}

		mainRepo.saveHistoryEntityList(histories);
	}

	/**
	 * 履歴を削除する(削除対象の前バージョンの履歴の失効日が更新される)
	 * 削除対象履歴の前バージョンの履歴の失効日 = 削除対象の履歴の失効日
	 * TODO 将来的には削除対象の履歴で変更した内容を後続の履歴で取り消す伝搬処理を実装する予定
	 * @param histories 論理削除対象の履歴IDリスト
	 */
	public void deleteHistoryListWithPropagation(List<Id> historyIds) {

		// 削除対象の履歴が存在するかチェック
		List<ParentChildHistoryEntity> histories = getHistoryEntityList(historyIds);
		if (histories.size() != historyIds.size()) {
			// メッセージ：対象データが見つかりませんでした。削除されている可能性があります。
			throw new App.RecordNotFoundException(MESSAGE.Com_Err_DeleteRecordNotFound);
		}

		// 履歴のベースIDからベースエンティティを取得する
		Set<Id> baseIds = new Set<Id>();
		for (ParentChildHistoryEntity history : histories) {
			baseIds.add(history.baseId);
		}
		List<ParentChildBaseEntity> bases = getEntityList(new List<Id>(baseIds));

		// ベースIDをキーにしたMapを作成
		Map<Id, ParentChildBaseEntity> baseMap = new Map<Id, ParentChildBaseEntity>(); // キーはベースID
		Set<Id> baseIdSet = new Set<Id>();
		for (ParentChildBaseEntity base : bases) {
			baseMap.put(base.id, base);
			baseIdSet.add(base.id);
		}

		// 対象の履歴を削除
		Map<Id, ParentChildHistoryEntity> updateHistoryMap = new Map<Id, ParentChildHistoryEntity>(); // キーは履歴ID
		List<ParentChildHistoryEntity> validToUpdatedHistories = new List<ParentChildHistoryEntity>();
		for (ParentChildHistoryEntity history : histories) {

			// 削除対象履歴のベースエンティティを取得
			ParentChildBaseEntity base = baseMap.get(history.baseId);
			if (base == null) {
				// メッセージ：対象データが見つかりませんでした。削除されている可能性があります。
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_DeleteRecordNotFound);
			}

			// 履歴が1件のみの場合は削除不可
			if (base.getSuperHistoryList().size() <= 1) {
				throw new App.IllegalStateException(MESSAGE.Com_Err_CannotDeletedNotExistHistory);
			}

			// 削除対象履歴の前バージョンの履歴の失効日 = 削除対象の履歴の失効日
			ParentChildHistoryEntity prevHistory = null;
			for (ParentChildHistoryEntity baseHistory : base.getSuperHistoryList()) {
				if (baseHistory.id == history.id) {
					break;
				}
				prevHistory = baseHistory;
			}
			if (prevHistory != null) {
				isDeletableHistory(base, history, prevHistory);
				prevHistory.validTo = history.validTo;
				validToUpdatedHistories.add(prevHistory);
				updateHistoryMap.put(prevHistory.id, prevHistory);
			}

			// 対象の履歴を論理削除
			history.setLogicallyDelete();
			updateHistoryMap.put(history.id, history);
		}

		if (! updateHistoryMap.isEmpty()) {
			mainRepo.saveHistoryEntityList(updateHistoryMap.values());
		}
		// 終了日延長した履歴の回帰チェック
		if (!validToUpdatedHistories.isEmpty()) {
			validateCircular(bases[0], validToUpdatedHistories);
		}
		// ベースのCurrentIdを更新する
		updateCurrentHistory(new List<Id>(baseIdSet));
	}

	/**
	 * 履歴が削除可能かどうかを判定する
	 * @param base 削除対象の履歴エンティティの親エンティティ
	 * @param deletedHistory 削除対象の履歴エンティティ
	 * @param prevHistory 削除対象の履歴エンティティの直前のエンティティ
	 * @throws IllegalStateException 削除不可の場合
	 */
	protected virtual void isDeletableHistory(ParentChildBaseEntity base,
			ParentChildHistoryEntity deletedHistory, ParentChildHistoryEntity prevHistory) {
	}

	/**
	 * ベースエンティティを物理削除する(ひも付く履歴も物理削除される)
	 * 既に削除されている場合はエラーコード「System.StatusCode.ENTITY_IS_DELETED」でDML例外が発生します。
	 * 上記以外の理由で削除に失敗した場合もDML例外が発生しますが、エラーメッセージをカスタマイズしています。
	 */
	public void deleteEntity(Id baseId) {
		try {
			// レコードを削除
			mainRepo.deleteEntity(baseId);
		} catch (DmlException e) {
			// すでに削除済みの場合(System.StatusCode.ENTITY_IS_DELETED)は処理成功とみなす(API仕様)
			if (e.getDmlType(0) == System.StatusCode.DELETE_FAILED) {
				// TODO 暫定的にデバッグログを出力しておきます
				System.debug(e);
				// TODO 本当はレコードを検索して参照しているレコードが存在するのか確認したいのですが、
				//      現状では工数の問題で実装ができないため下記のようなエラーメッセージで対応します。
				// メッセージ：削除できませんでした。他のデータから参照されている可能性があります。
 				e.setMessage(MESSAGE.Com_Err_FaildDeleteReference);
			}
			throw e;
		}
	}

	/**
	 * 現在の履歴IDを更新する
	 */
	@testVisible
	private void updateCurrentHistory(Id baseId) {
		updateCurrentHistory(new List<Id>{baseId});
	}

	/**
	 * 現在の履歴IDを更新する
	 */
	@testVisible
	private void updateCurrentHistory(List<Id> baseIdList) {
		List<ParentChildBaseEntity> baseList = getEntityList(baseIdList);

		List<ParentChildBaseEntity> saveBaseList = new List<ParentChildBaseEntity>();
		for (ParentChildBaseEntity base : baseList) {
			ParentChildHistoryEntity currentHistory = base.getSuperHistoryByDate(AppDate.today());
			if (currentHistory == null) {
				// マスタ全体の有効期間前ならもっとも古い履歴、失効日以降(失効日含む)であれば最新のエンティティとする
				Date today = Date.today();
				ParentChildHistoryEntity oldestHistory = base.getOldestSuperHistory();
				ParentChildHistoryEntity latestHistory = base.getLatestSuperHistory();
				if (oldestHistory.validFrom != null && today < oldestHistory.validFrom.getDate()) {
					currentHistory = oldestHistory;
				} else if (oldestHistory.validTo != null && today >= latestHistory.validTo.getDate()) {
					currentHistory = latestHistory;
				} else {
					if (oldestHistory != null) {
						currentHistory = oldestHistory;
					} else {
						// ここにくるはずはないけど、もし来たら不具合かな？
						throw new App.IllegalStateException('サーバエラー：CurrentHistoryが見つかりませんでした。');
					}
				}
			}

			// IDが変更されていたらDBに更新する
			if (base.currentHistoryId != currentHistory.id) {

				base.currentHistoryId = currentHistory.id;

				// 履歴エンティティがマスタの名前を持っている場合はベースのName項目に設定する
				String nameL0 = getNameL0FromHistory(currentHistory);
				if (String.isNotBlank(nameL0)) {
					base.Name = nameL0;
				}

				// 履歴は更新しないため、エンティティから削除しておく
				base.getSuperHistoryList().clear();

				saveBaseList.add(base);
			}

			// 保存
			if (! saveBaseList.isEmpty()) {
				mainRepo.saveBaseEntityList(saveBaseList, true);
			}
		}
	}

	/**
	 * 履歴エンティティが持つnameL0を取得する
	 * @param history 取得対象の履歴エンティティ
	 * @return 履歴エンティティのnameL0、ただしnameL0を持っていない場合はnull
	 */
	protected abstract String getNameL0FromHistory(ParentChildHistoryEntity history);

	/** 会社のキャッシュ */
	protected Map<Id, CompanyEntity> companyCache = new Map<Id, CompanyEntity>();
	/**
	 * 会社を取得する(キャッシュ機能あり)
	 * @param companyId 取得対象の会社ID
	 * @return 会社
	 */
	protected CompanyEntity getCompany(Id companyId) {
		CompanyEntity company = companyCache.get(companyId);
		if (company == null) {
			company = new CompanyRepository().getEntity(companyId);
			if (company == null) {
				// TODO エラーメッセージ
				throw new App.RecordNotFoundException('会社が見つかりませんでした。会社ID=' + companyId);
			}
			companyCache.put(companyId, company);
		}
		return company;
	}
}