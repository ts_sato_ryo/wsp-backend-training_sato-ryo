/**
* @author TeamSpirit Inc.
* @date 2018
*
* @group 共通
*
* @description CheckInInfoRepositoryの廃止に伴い、テストを削除しました。
* The tests inside are removed Because CheckInInfoRepository has been deprecated.
*/
@isTest
private class CheckInInfoRepositoryTest {
}