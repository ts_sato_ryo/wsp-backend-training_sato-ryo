/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * カレンダーに関するサービスを提供するクラス
 */
public with sharing class CalendarService {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * カレンダーを取得する
	 * @param calendarId カレンダーID
	 * @param companyId 会社ID
	 * @param types カレンダータイプ
	 * @return カレンダーのリスト
	 */
	public List<CalendarEntity> getCalendarList(Id calendarId, Id companyId, List<CalendarType> types) {
		CalendarRepository calRep = new CalendarRepository();

		return calRep.getEntityList(calendarId, companyId, types, false);
	}

	/**
	 * 会社のデフォルトカレンダーを作成する
	 * @param companyId 会社ID
	 * @param code カレンダーに設定するコード
	 * @param type カレンダータイプ
	 * @return 作成したカレンダーのID
	 */
	public Id createCompanyDefaultCalendar(Id companyId, String code, CalendarType type) {
		// カレンダー名定義
		Map<AppLanguage.LangKey, String> nameMap = new Map<AppLanguage.LangKey, String>();
		nameMap.put(AppLanguage.JA.getLangkey(), '全社共通カレンダー');
		nameMap.put(AppLanguage.EN_US.getLangkey(), 'Company Calendar');

		// カレンダーエンティティを作成
		CalendarEntity calendar = new CalendarEntity();
		calendar.code = code;
		calendar.name = nameMap.get(AppLanguage.LangKey.L0);
		calendar.nameL = new AppMultiString(nameMap.get(AppLanguage.LangKey.L0), nameMap.get(AppLanguage.LangKey.L1), nameMap.get(AppLanguage.LangKey.L2));
		calendar.calType = type;
		calendar.companyId = companyId;

		CalendarRepository.SaveCalendarResult res = this.saveCalendar(calendar);

		return res.details[0].Id;
	}

	/**
	 * カレンダーを保存する
	 * @param entity カレンダーエンティティ
	 * @return 保存結果
	 */
	public CalendarRepository.SaveCalendarResult saveCalendar(CalendarEntity savedEntity) {
		CalendarRepository calRep = new CalendarRepository();

		// 保存用のエンティティを作成
		CalendarEntity entity;
		if (savedEntity.id == null) {
			// 新規の場合
			entity = savedEntity;
		} else {
			// 更新の場合は既存の値を取得する
			entity = calRep.getEntityById(savedEntity.id);
			if (entity == null) {
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_RecordNotFound);
			}
			// 更新値を上書きする
			for (CalendarEntity.Field field : CalendarEntity.Field.values()) {
				if (savedEntity.isChanged(field)) {
					entity.setFieldValue(field, savedEntity.getFieldValue(field));
				}
			}
		}

		// カレンダーコード、または会社IDが変更されていたらユニークキーを設定する
		if (entity.isChanged(CalendarEntity.Field.CODE) || entity.isChanged(CalendarEntity.Field.COMPANY_ID)) {
			// 会社コードを取得
			CompanyEntity company = new CompanyRepository().getEntity(entity.companyId);
			if (company == null) {
				// メッセージ：指定された会社が見つかりません
				throw new App.RecordNotFoundException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
			}
			entity.uniqKey = entity.createUniqKey(company.code);
		}

		// バリデーション実行
		Validator.Result validResult = (new ComConfigValidator.CalendarValidator(entity)).validate();
		if (! validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		// カレンダーコードが変更されている場合は重複チェック
		if (entity.isChanged(CalendarEntity.Field.CODE)) {
			CalendarEntity duplicateCodeEntity = calRep.getEntityByCode(entity.code, entity.companyId);
			if ((duplicateCodeEntity != null) && (duplicateCodeEntity.id != entity.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_DuplicateCode);
			}
		}

		return calRep.saveCalendarEntity(entity, false);
	}

	/**
	 * カレンダー明細を取得する
	 * @param recordId カレンダー明細ID
	 * @return カレンダー明細
	 */
	public CalendarRecordEntity getCalendarRecord(Id recordId) {
		CalendarRepository calRep = new CalendarRepository();

		return calRep.getRecordEntityById(recordId);
	}

	/**
	 * カレンダー明細を取得する
	 * @param calendarId カレンダーID
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @return カレンダー明細のリスト
	 */
	public List<CalendarRecordEntity> getCalendarRecordList(
			Id calendarId, AppDate startDate, AppDate endDate) {
		return getCalendarRecordList(new Set<Id> {calendarId}, startDate, endDate);
	}

	/**
	 * カレンダー明細を取得する
	 * @param calendarIds カレンダーIDのセット
	 * @param startDate 開始日
	 * @param endDate 終了日
	 * @return カレンダー明細のリスト
	 */
	public List<CalendarRecordEntity> getCalendarRecordList(
			Set<Id> calendarIds, AppDate startDate, AppDate endDate) {

		CalendarRepository calRep = new CalendarRepository();

		return calRep.searchRecordList(calendarIds, startDate, endDate);
	}

	/**
	 * カレンダー明細を取得する
	 * @param recordId カレンダー明細ID
	 * @param calendarId カレンダーID
	 * @return カレンダー明細のリスト
	 */
	public List<CalendarRecordEntity> getCalendarRecordList(Id recordId, Id calendarId) {
		CalendarRepository calRep = new CalendarRepository();

		return calRep.searchRecordList(recordId, calendarId);
	}

	/**
	 * カレンダー明細を保存する
	 * 既存の明細を更新する場合は、既存の明細データを論理削除して更新内容で新規にレコードを作成する
	 * @param entity カレンダー明細エンティティ
	 * @return 保存結果
	 */
	public Repository.SaveResult saveCalendarRecord(CalendarRecordEntity entity) {
		CalendarRepository calRep = new CalendarRepository();
		CalendarRecordEntity newEntity;

		// バリデーション実行
		CalendarEntity calendar = calRep.getEntityById(entity.calendarId);
		Validator.Result validResult = (new ComConfigValidator.CalendarRecordValidator(calendar, entity)).validate();
		if (! validResult.isSuccess()) {
			Validator.Error error = validResult.getErrors().get(0);
			App.ParameterException e = new App.ParameterException(error.message);
			e.setCode(error.code);
			throw e;
		}

		if (entity.id == null) {
			newEntity = entity;
		} else {
			// 既存の明細を更新する場合は
			// 既存の明細を論理削除し、更新後の明細は新規データとして登録する
			logicalDeleteCalendarRecords(new List<Id>{entity.Id});
			newEntity = entity.copy();
			newEntity.setId((Id)null);
		}

		return calRep.saveRecordEntity(newEntity);
	}

	/**
	 * カレンダー明細を論理削除する
	 * @param recordIdList カレンダー明細IDのリスト
	 */
	public void logicalDeleteCalendarRecords(List<Id> recordIdList) {

		// 論理削除する
		List<CalendarRecordEntity> recordList = new CalendarRepository().getRecordEntityListById(recordIdList);
		logicalDeleteCalendarRecords(recordList);
	}

	/**
	 * カレンダーを論理削除する
	 * ただし、全社共通カレンダーは削除できません
	 * @param calendarId 削除対象のカレンダーID
	 */
	public void logicalDeleteCalendar(Id calendarId) {

		// 全社のデフォルトカレンダーでないことを確認
		CompanyRepository.SearchFilter filter = new CompanyRepository.SearchFilter();
		filter.calendarIds = new List<Id>{calendarId};
		List<CompanyEntity> companyList = new CompanyRepository().searchEntityList(filter);
		if (! companyList.isEmpty()) {
			// メッセージ：全社カレンダーは削除できません
			throw new App.ParameterException(
					ComMessage.msg().Com_Err_CannotDelete(new List<String>{ComMessage.msg().Admin_Lbl_CompanyCalendar}));
		}

		// 論理削除する
		CalendarEntity calendar = new CalendarRepository().getEntityById(calendarId);

		if (calendar != null) {
			logicalDeleteCalendars(new List<CalendarEntity>{calendar});
		}
	}

	/**
	 * カレンダーとカレンダー明細を論理削除する
	 * @param calendarList 削除対象のカレンダー
	 */
	private void logicalDeleteCalendars(List<CalendarEntity> calendarList) {
		Boolean withRecords = false;
		for (CalendarEntity calendar : calendarList) {
			calendar.isRemoved = true;
			calendar.uniqKey = calendar.id; // 重複を避けるため論理削除の場合はIDを設定する
			if (calendar.recordList != null && ! calendar.recordList.isEmpty()) {
				withRecords = true;
				for (CalendarRecordEntity record : calendar.recordList) {
					record.isRemoved = true;
				}
			}
		}

		new CalendarRepository().saveCalendarEntityList(calendarList, withRecords);
	}

	/**
	 * カレンダー明細を論理削除する
	 */
	private void logicalDeleteCalendarRecords(List<CalendarRecordEntity> recordList) {
		for (CalendarRecordEntity record : recordList) {
			record.isRemoved = true;
		}
		new CalendarRepository().saveRecordEntityList(recordList);
	}
}