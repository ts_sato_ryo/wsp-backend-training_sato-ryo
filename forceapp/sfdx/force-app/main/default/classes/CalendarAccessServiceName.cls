/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * カレンダー連携対象サービス名
 */
public with sharing class CalendarAccessServiceName extends ValueObjectType {

	/** Office365 */
	public static final CalendarAccessServiceName OFFICE_365 = new CalendarAccessServiceName('Office365');


	/** カレンダー連携対象サービスのリスト（種別が追加されら本リストにも追加してください) */
	public static final List<CalendarAccessServiceName> TYPE_LIST;
	static {
		TYPE_LIST = new List<CalendarAccessServiceName> {
			OFFICE_365
		};
	}

	/**
	 * コンストラクタ（値のみ）
	 * @param value 値
	 */
	private CalendarAccessServiceName(String value) {
		super(value);
	}

	/**
	 * 値からCalendarAccessServiceNameを取得する
	 * @param 取得対象のインスタンスが持つ値
	 * @return パラメータで指定された値を持つCalendarAccessServiceName、ただし該当するCalendarAccessServiceNameが存在しない場合はnull
	 */
	public static CalendarAccessServiceName valueOf(String value) {
		CalendarAccessServiceName retType = null;
		if (String.isNotBlank(value)) {
			for (CalendarAccessServiceName type : TYPE_LIST) {
				if (value == type.value) {
					retType = type;
				}
			}
		}
		return retType;
	}

	/**
	 * 値が等しいかどうかを判定する
	 * @param compare 比較対象のオブジェクト
	 * @retun 等しい場合はtrue、そうでない場合はfalse
	 */
	public override Boolean equals(Object compare) {

		// nullの場合はfalse
		if (compare == null) {
			return false;
		}

		// CalendarAccessServiceName以外の場合はfalse
		Boolean eq;
		if (compare instanceof CalendarAccessServiceName) {
			// 値が同じであればtrue
			if (this.value == ((CalendarAccessServiceName)compare).value) {
				eq = true;
			} else {
				eq = false;
			}
		} else {
			eq = false;
		}

		// 値が異なる
		return eq;
	}
}