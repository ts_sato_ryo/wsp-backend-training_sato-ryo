 /**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group アプリ
 *
 * 添付ファイルのリポジトリクラスのテスト
 */
@isTest
private class AppAttachmentRepositoryTest {

	/**
	 * テスト用の勤怠申請オブジェクトを作成する
	 */
	private static AttDailyRequest__c createRequest(String name) {
		AttDailyRequest__c request = new AttDailyRequest__c(
				Name = name,
				RequestType__c = 'Leave');

		return request;
	}

	/**
	 * テスト用のAttachmentを作成する
	 */
	private static Attachment createAttachment(String name, Id parentId, String body) {
		Attachment attach = new Attachment(
			Name = name,
			ParentId = parentId,
			Body = Blob.valueOf(body));

		return attach;
	}

	/**
	 * saveEntityのテスト
	 * 新規の保存ができることを確認する
	 */
	@isTest static void saveEntityTestNew() {
		AttDailyRequest__c request = createRequest('テスト申請');
		insert request;

		AppAttachmentEntity attach = new AppAttachmentEntity();
		attach.name = request.Name;
		attach.parentId = request.Id;
		attach.bodyText = 'ファイルの中身';

		Test.startTest();
			AppAttachmentRepository repo = new AppAttachmentRepository();
			Repository.SaveResult result = repo.saveEntity(attach);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);

		// 保存できていることを確認
		Id resId = result.details[0].Id;
		List<Attachment> resObjs = [
				SELECT Id, Name, Body, ParentId
				FROM Attachment
				WHERE Id = :resId];
		System.assertEquals(1, resObjs.size());
		System.assertEquals(attach.name, resObjs[0].Name);
		System.assertEquals(attach.bodyText, resObjs[0].Body.toString());
		System.assertEquals(attach.parentId, resObjs[0].ParentId);

	}

	/**
	 * saveEntityのテスト
	 * 既存のエンティティが保存できることを確認する
	 */
	@isTest static void saveEntityTestUpdate() {
		AttDailyRequest__c request = createRequest('テスト申請');
		insert request;
		Attachment attachNew = createAttachment(request.Name, request.Id, 'ファイルの中身');
		insert attachNew;

		// 更新する
		AppAttachmentEntity attach = new AppAttachmentEntity();
		attach.setId(attachNew.Id);
		attach.name = request.Name + '_Update';
		attach.bodyText = 'ファイルの中身_Update';

		Test.startTest();
			AppAttachmentRepository repo = new AppAttachmentRepository();
			Repository.SaveResult result = repo.saveEntity(attach);
		Test.stopTest();

		System.assertEquals(true, result.isSuccessAll);

		// 保存できていることを確認
		Id resId = result.details[0].Id;
		List<Attachment> resObjs = [
				SELECT Id, Name, Body, ParentId
				FROM Attachment
				WHERE Id = :resId];
		System.assertEquals(1, resObjs.size());
		System.assertEquals(attach.name, resObjs[0].Name);
		System.assertEquals(attach.bodyText, resObjs[0].Body.toString());
		System.assertEquals(attachNew.ParentId, resObjs[0].ParentId);

	}

	/**
	 * getEntityByParentIdのテスト
	 * 親IDを指定して取得できることを確認する
	 */
	@isTest static void getEntityListByParentIdTest() {
		List<AttDailyRequest__c> requests = new List<AttDailyRequest__c> {
			createRequest('テスト申請1'),
			createRequest('テスト申請2')};
		insert requests;
		List<Attachment> attaches = new List<Attachment> {
			createAttachment(requests[0].Name, requests[0].Id, 'ファイルの中身1'),
			createAttachment(requests[0].Name + '2', requests[0].Id, 'ファイルの中身1-2'),
			createAttachment(requests[1].Name, requests[1].Id, 'ファイルの中身2')};
		insert attaches;

		// 取得する
		Test.startTest();
			AppAttachmentRepository repo = new AppAttachmentRepository();
			List<AppAttachmentEntity> resList = repo.getEntityListByParentId(requests[0].Id);
		Test.stopTest();

		// 各項目の確認はgetEntityListByParentIdTestWithName()で実施
		System.assertEquals(2, resList.size());
		System.assertEquals(requests[0].Id, resList[0].parentId);
		System.assertEquals(attaches[0].Name, resList[0].name);
		System.assertEquals(requests[0].Id, resList[1].parentId);
		System.assertEquals(attaches[1].Name, resList[1].name);

	}

	/**
	 * getEntityByParentIdのテスト
	 * 親IDと添付ファイル名を指定して取得できることを確認する
	 */
	@isTest static void getEntityListByParentIdTestWithName() {
		List<AttDailyRequest__c> requests = new List<AttDailyRequest__c> {
			createRequest('テスト申請1'),
			createRequest('テスト申請2')};
		insert requests;
		List<Attachment> attaches = new List<Attachment> {
			createAttachment(requests[0].Name, requests[0].Id, 'ファイルの中身1'),
			createAttachment(requests[0].Name + '2', requests[0].Id, 'ファイルの中身1-2'),
			createAttachment(requests[1].Name, requests[1].Id, 'ファイルの中身2')};
		insert attaches;

		// 取得する
		Test.startTest();
			AppAttachmentRepository repo = new AppAttachmentRepository();
			List<AppAttachmentEntity> resList = repo.getEntityListByParentId(requests[0].Id, requests[0].Name);
		Test.stopTest();

		System.assertEquals(1, resList.size());
		System.assertEquals(attaches[0].Name, resList[0].name);
		System.assertEquals(attaches[0].Body.toString(), resList[0].bodyText);
		System.assertEquals(attaches[0].ParentId, resList[0].parentId);

	}
}
