/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 組織エンティティ
 */
public with sharing class OrganizationSettingEntity extends Entity {
	/** 項目の定義(変更管理で使用する) */
	public enum Field {
		NAME,
		LANGUAGE0,
		LANGUAGE1,
		LANGUAGE2
	}

	/** 値を変更した項目のリスト */
	private Set<Field> isChangedFieldSet;

	/** 設定番号(自動採番項目のため参照のみ) */
	public String name {
		get;
		set;
	}

	/** 言語L0 */
	public AppLanguage language0 {
		get;
		set {
			language0 = value;
			setChanged(Field.LANGUAGE0);
		}
	}
	/** 言語L1 */
	public AppLanguage language1 {
		get;
		set {
			language1 = value;
			setChanged(Field.LANGUAGE1);
		}
	}
	/** 言語L2 */
	public AppLanguage language2 {
		get;
		set {
			language2 = value;
			setChanged(Field.LANGUAGE2);
		}
	}

	/**
	 * コンストラクタ
	 */
	public OrganizationSettingEntity() {
		this.isChangedFieldSet = new Set<Field>();
	}

	/**
	 * 項目の値を変更済みに設定する
	 * @param Field 変更した項目
	 */
	private void setChanged(Field field) {
		this.isChangedFieldSet.add(field);
	}

	/**
	 * 項目の変更情報を取得する
	 * @param field 取得対象の項目
	 * @param 項目の値が変更されている場合はtrue、そうでない場合はfalse
	 */
	public Boolean isChanged(Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * 項目の変更情報をリセットする
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}

}
