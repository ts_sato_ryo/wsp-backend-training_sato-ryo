/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 共通
 *
 * @description 勤務体系ベースエンティティ
 */
public with sharing class AttWorkingTypeBaseEntity extends AttWorkingTypeBaseGeneratedEntity {
	/** コードの最大文字列長さ */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 年度の開始月の最小値 */
	public static final Integer START_MONTH_OF_YEAR_MIN = 1;
	/** 年度の開始月の最大値 */
	public static final Integer START_MONTH_OF_YEAR_MAX = 12;
	/** 月度の開始日の最小値 */
	public static final Integer START_DAY_OF_MONTH_MIN = 1;
	/** 月度の開始日の最大値 */
	public static final Integer START_DAY_OF_MONTH_MAX = 28;
	/** 週の開始曜日の最小値 */
	public static final Integer START_DAY_OF_WEEK_MIN = 0;
	/** 週の開始曜日の最大値 */
	public static final Integer START_DAY_OF_WEEK_MAX = 6;

	/**
	 * デフォルトコンストラクタ
	 */
	public AttWorkingTypeBaseEntity() {
		super();
	}

	/**
	 * コンストラクタ
	 */
	public AttWorkingTypeBaseEntity(AttWorkingTypeBase__c sobj) {
		super(sobj);
	}

	/** 履歴エンティティのリスト */
	protected List<AttWorkingTypeHistoryEntity> historyList;

	/**
	 * エンティティを複製する(IDは複製されません)
	 */
	public AttWorkingTypeBaseEntity copy() {
		AttWorkingTypeBaseEntity copyEntity = new AttWorkingTypeBaseEntity();
		setAllFieldValues(copyEntity);
		return copyEntity;
	}

	/**
	 * @description Copy the entity and return instance of ParentChildHistoryEntity type.
	 * エンティティを複製し、ParentChildHistoryEntity型で返却する
	 */
	public override ParentChildBaseEntity copySuperEntity() {
		return copy();
	}

	/**
	 * 値を指定してMarkTypeを取得する。
	 * @param value 取得対象のMarkTypeの値
	 * @return 該当するMarkType、ただしvalueが空文字の場合はnullを返却する
	 */
	private AttWorkingTypeBaseGeneratedEntity.MarkType getMarkType(String value) {
		AttWorkingTypeBaseGeneratedEntity.MarkType enumValue = toMarkType(value);
		if (enumValue == null) {
			throw new App.ParameterException('指定された値のMarkTypeが存在しません。(value=' + value);
		}

		return enumValue;
	}

	/**
	 * 値を指定してPayrollPeriodTypeを取得する。
	 * @param value 取得対象のPayrollPeriodTypeの値
	 * @return 該当するPayrollPeriodType、ただしvalueが空文字の場合はnullを返却する
	 */
	private static AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType getPayrollPeriodType(String value) {
		if (String.isBlank(value)) {
			return null;
		}

		AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType retType = null;
		for (AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType type :
				AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.values()) {
			if (value == type.name()) {
				retType = type;
			}
		}
		if (retType == null) {
			throw new App.ParameterException('指定された値のMarkTypeが存在しません。(value=' + value);
		}

		return retType;
	}

	/** 清算期間を設定する */
	public void setPayrollPeriod(String value) {
		this.payrollPeriod = getPayrollPeriodType(value);
	}

	/**
	 * 履歴の基底クラスに変換する
	 */
	protected override List<ParentChildHistoryEntity> convertSuperHistoryList() {
		return (List<ParentChildHistoryEntity>)historyList;
	}

	/**
	 * ユニークキーを作成する
	 * ジョブコードを指定しておくこと。
	 * @return ユニークキー
	 */
	public String createUniqKey(String companyCode) {
		if (String.isBlank(companyCode)) {
			// 実装ミスのため多言語化不要
			throw new App.ParameterException('[サーバエラー]勤務体系(ベース)のユニークキーが作成できませんでした。会社コードが空です。');
		}
		if (String.isBlank(this.code)) {
			// 実装ミスのため多言語化不要
			throw new App.IllegalStateException('[サーバエラー]勤務体系(ベース)のユニークキーが作成できませんでした。コードが設定されていません。');
		}

		// 会社コード + '-' + ジョブコード
		return companyCode + '-' + this.code;
	}

	/**
	 * 履歴エンティティリストを取得する
	 * @return 履歴エンティティリスト
	 */
	public List<AttWorkingTypeHistoryEntity> getHistoryList() {
		return historyList;
	}

	/**
	 * 履歴エンティティをリストに追加する
	 * @param history 追加する履歴エンティティ
	 */
	 public void addHistory(AttWorkingTypeHistoryEntity history) {
		 if (historyList == null) {
			historyList = new List<AttWorkingTypeHistoryEntity>();
		 }
		 historyList.add(history);
	 }

	 /**
	  * 勤務体系履歴リストを取得する
	  * @return 勤務体系名。履歴が存在しない場合はnull
	  */
	 public AttWorkingTypeHistoryEntity getHistory(Integer index) {
		 if (historyList == null) {
			 return null;
		 }
		 return historyList[index];
	 }

	/**
	 * 勤務体系履歴エンティティリストから指定したIDの履歴を取得する
	 * @return 履歴エンティティ、ただし存在しない場合はnull
	 */
	public AttWorkingTypeHistoryEntity getHistoryById(Id historyId) {
		return (AttWorkingTypeHistoryEntity)super.getSuperHistoryById(historyId);
	}

	/**
	 * 指定した日に有効な履歴を取得する
	 * 履歴リストに論理削除されている履歴が含まれている場合は利用しないでください
	 * @param targetDate 対象日
	 * @return 有効な履歴、ただし有効な履歴が存在しない場合はnull
	 */
	public AttWorkingTypeHistoryEntity getHistoryByDate(AppDate targetDate) {
		return (AttWorkingTypeHistoryEntity)getSuperHistoryByDate(targetDate);
	}

	/** 会社(参照専用) */
	public ParentChildBaseCompanyEntity.Company company {
		get {
			if (company == null) {
				company = new ParentChildBaseCompanyEntity.Company(sobj.CompanyId__r.Code__c);
			}
			return company;
		}
		private set;
	}

	/**
	 * @description 年度の表記を設定する
	 */
	public void setYearMark(String value) {
		this.yearMark = getMarkType(value);
	}

	/**
	 * @description 月度の表記を設定する
	 */
	public void setMonthMark(String value) {
		this.monthMark = getMarkType(value);
	}

	/** 労働時間制を設定する */
	public void setWorkSystem(String value) {
		this.workSystem = AttWorkSystem.valueOf(value);
	}

	/**
	 * WorkSystemTypeに対応する選択リスト値を取得する
	 * @param value 取得対象のWorkSystemTypeの値
	 * @return 該当するWorkSystemType、ただしvalueが空文字の場合はnullを返却する
	 */
	public String getWorkSystemString() {
		return this.workSystem == null ? null : this.workSystem.value;
	}

	/**
	 * @description 対象日の月度を取得する
	 * @param targetDate 対象日
	 * @return 対象月度(yyyymm)、yyyyは年度ではなく年である
	 */
	public String getMonthlyByDate(AppDate targetDate) {
		// 清算期間が月度以外はエラーとしておく
		if (this.payrollPeriod != AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month) {
			// 実装ミスであるため多言語化不要
			throw new App.IllegalStateException('清算期間が月度ではありません');
		}

		AppDate startDate;
		if (targetDate.day() >= this.startDateOfMonth) {
			startDate = AppDate.newInstance(targetDate.year(), targetDate.month(), this.startDateOfMonth);
		} else {
			// 起算日以前の場合、対象月の前月の開始日が月度の開始日
			AppDate beforeMonth = targetDate.addMonths(-1);
			startDate = AppDate.newInstance(beforeMonth.year(), beforeMonth.month(), this.startDateOfMonth);
		}

		String monthly;
		if (this.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase) {
			// 月度の開始日の月が月度
			monthly = formatMonthly(startDate.year(), startDate.month());
		} else {
			// 月度の末日の月が月度
			AppDate endDate = startDate.addMonths(1).addDays(-1);
			monthly = formatMonthly(endDate.year(), endDate.month());
		}

		return monthly;
	}

	/**
	 * @description 指定された月度の表示名を作成する
	 * @param yearMonthly 月度(yyyymm) yyyyは年度ではなく年である
	 * @return 月度の表示ラベル
	 */
	public String getMonthlyLabel(String yearMonthly, String lang) {
		String year = yearMonthly.subString(0, 4);
		String monthly = yearMonthly.subString(4, 6);

		if (lang == AppLanguage.JA.value) {
			// 日本語(例: xxxx年xx月)
			return year + '年' + monthly + '月';
		}

		// 英語(例: Jul 2017)
		AppDate dt = AppDate.newInstance(Integer.valueOf(year), Integer.valueOf(monthly), 1);
		return dt.format(AppDate.FormatType.MMMYYYY);
	}

	/**
	 * @description 指定された月度の開始日を取得する
	 * @param yearMonthly 月度(yyyymm) yyyyは年度ではなく年である
	 * @return 月度の開始日
	 */
	public AppDate getStartDateOfMonthly(String yearMonthly) {
		// 清算期間が月度以外はエラーとしておく
		if (this.payrollPeriod != AttWorkingTypeBaseGeneratedEntity.PayrollPeriodType.Month) {
			// 実装ミスであるため多言語化不要
			throw new App.IllegalStateException('清算期間が月度ではありません');
		}

		Integer year = Integer.valueOf(yearMonthly.subString(0, 4));
		Integer monthly = Integer.valueOf(yearMonthly.subString(4, 6));

		// 月度の開始日が1日の場合は月度の表記が前でも後ろでも同じ起算日になる
		if (this.startDateOfMonth == 1) {
			return AppDate.newInstance(year, monthly, this.startDateOfMonth);
		}

		// 月度の開始日が1日以外は開始日と終了日で月が異なる
		AppDate startDate;
		if (this.monthMark == AttWorkingTypeBaseGeneratedEntity.MarkType.BeginBase) {
			// 月度が起算日の月になる
			startDate = AppDate.newInstance(year, monthly, this.startDateOfMonth);
		} else {
			// 月度の前月が起算日の月になる
			startDate = AppDate.newInstance(year, monthly, this.startDateOfMonth).addMonths(-1);
		}
		return startDate;
	}

	/**
	 * @description 指定された日付を含む年月度の期間を取得する
	 * 勤務体系の「年度の開始月」が4月、「月度の開始日」が5日の場合、「2018/1/1」を渡すと「2017/4/5〜2018/4/4」を返す。
	 * このメソッドは「年度の表記」「月度の表記」の値は考慮しないことに注意すること。
	 * @param 指定された日付
	 * @return 年月度の期間
	 */
	public AppDateRange getYearlyRange(AppDate targetDate) {
		if (targetDate == null) {
			return null;
		}
		AppDate yearlyStartDate;
		AppDate yearlyEndDate;

		AppDate thisYearStartStandardDate = AppDate.newInstance(targetDate.year(), this.startMonthOfYear, this.startDateOfMonth);

		if (thisYearStartStandardDate.isAfter(targetDate)) {
			yearlyStartDate = thisYearStartStandardDate.addMonths(-12);
			yearlyEndDate = thisYearStartStandardDate.addDays(-1);
		} else {
			yearlyStartDate = thisYearStartStandardDate;
			yearlyEndDate = thisYearStartStandardDate.addMonths(12).addDays(-1);
		}
		return new AppDateRange(yearlyStartDate, yearlyEndDate);
	}

	/**
	 * @description yyyymm の形式の文字列を返却する
	 * @param year 年
	 * @param month 月
	 * @return yyyymmフォーマットの文字列
	 */
	public String formatMonthly(Integer yaer, Integer month) {
		// yyyymm
		return String.valueOf(yaer).leftPad(4, '0') + String.valueOf(month).leftPad(2, '0');
	}
}