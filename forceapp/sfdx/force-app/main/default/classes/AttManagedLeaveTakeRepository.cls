/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇消費のリポジトリ
 */
public with sharing class AttManagedLeaveTakeRepository extends Repository.BaseRepository {
	/** 有休消費取得対象の項目 */
	private static final Set<Schema.SObjectField> GET_TAKE_FIELD_SET;
	static {
		GET_TAKE_FIELD_SET = new Set<Schema.SObjectField> {
				AttManagedLeaveTake__c.Id,
				AttManagedLeaveTake__c.LeaveSummaryId__c,
				AttManagedLeaveTake__c.LeaveRequestId__c,
				AttManagedLeaveTake__c.StartDate__c,
				AttManagedLeaveTake__c.EndDate__c,
				AttManagedLeaveTake__c.DaysTaken__c
		};
	}
	/** リレーション名（休暇消費の申請Id) */
	private static final String REQUEST_R =
			AttManagedLeaveTake__c.LeaveRequestId__c.getDescribe().getRelationshipName();
	/** リレーション名（休暇消費の休暇サマリId) */
	private static final String SUMMARY_R =
			AttManagedLeaveTake__c.LeaveSummaryId__c.getDescribe().getRelationshipName();
	/** リレーション名（休暇サマリの勤怠サマリId) */
	private static final String ATT_SUMMARY_R =
			AttManagedLeaveSummary__c.AttSummaryId__c.getDescribe().getRelationshipName();
	/** リレーション名（勤怠サマリの社員履歴Id) */
	private static final String ATT_EMP_HIST_R =
			AttSummary__c.EmployeeHistoryId__c.getDescribe().getRelationshipName();

	/** 検索フィルタ (検索API実行用) */
	public class SearchFilter {
		public Id empId;
		public Set<Id> leaveIds;
		public Set<Id> leaveRequestIds;
		public Set<Id> leaveSummryIds;
		public Boolean onlyAdmit; // 確定のみ（onlyUnAdmitと同時に指定不可）
		public Boolean onlyUnAdmit; // 未確定のみ（onlyAdmitと同時に指定不可）
	}
	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティ
	 * @return 保存結果
	 */
	public SaveResult saveEntity(AttManagedLeaveTakeEntity entity) {
		return saveEntityList(new List<AttManagedLeaveTakeEntity>{entity}, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @return 保存結果
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveTakeEntity> entityList) {
		return saveEntityList(entityList, true);
	}

	/**
	 * エンティティを保存する
	 * @param entity 保存対象のエンティティのリスト
	 * @param allOrNone falseの場合、レコードが失敗しても残りのDML操作を正常に完了できる
	 * @return 保存結果のリスト(勤怠サマリーと明細の結果)
	 */
	public SaveResult saveEntityList(List<AttManagedLeaveTakeEntity> entityList, Boolean allOrNone) {

		List<AttManagedLeaveTake__c> takeList = new List<AttManagedLeaveTake__c>();

		for (AttManagedLeaveTakeEntity entity : entityList) {
			// エンティティからSObjectを作成する
			takeList.add(createObj(entity));
		}
		// DBに保存する
		List<Database.UpsertResult> recResList = AppDatabase.doUpsert(takeList, allOrNone);

		// 結果作成
		return resultFactory.createSaveResult(recResList);
	}
	public List<AttManagedLeaveTakeEntity> searchEntityList(SearchFilter filter, Boolean forUpdate) {
		// SOQLを作成する
		List<String> selectFldList = new List<String>();
		for (Schema.SObjectField fld : GET_TAKE_FIELD_SET) {
			selectFldList.add(getFieldName(fld));
		}
		selectFldList.add(SUMMARY_R + '.' + Att_SUMMARY_R + '.' + ATT_EMP_HIST_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c));
		selectFldList.add(REQUEST_R + '.' + getFieldName(AttDailyRequest__c.AttLeaveId__c));
		// フィルタ条件からWHERE句の条件リストを作成する
		List<String> condList = new List<String>();
		final Id empId = filter.empId;
		final Set<Id> leaveIds = filter.leaveIds;
		final Set<Id> leaveRequestIds = filter.leaveRequestIds;
		final Set<Id> leaveSummryIds = filter.leaveSummryIds;
		if (!String.isBlank(empId)) {
			condList.add(SUMMARY_R + '.' + Att_SUMMARY_R + '.' + ATT_EMP_HIST_R + '.' + getFieldName(ComEmpHistory__c.BaseId__c) + ' = :empId');
		}
		if (leaveIds != null) {
			condList.add(REQUEST_R + '.' + getFieldName(AttDailyRequest__c.AttLeaveId__c) + ' IN :leaveIds');
		}
		if (leaveRequestIds != null) {
			condList.add(getFieldName(AttManagedLeaveTake__c.LeaveRequestId__c) + ' IN :leaveRequestIds');
		}
		if (leaveSummryIds != null) {
			condList.add(getFieldName(AttManagedLeaveTake__c.LeaveSummaryId__c) + ' IN :leaveSummryIds');
		}
		if (filter.onlyAdmit == true) {
			condList.add(SUMMARY_R + '.' + Att_SUMMARY_R + '.' + getFieldName(AttSummary__c.Locked__c) + ' = true');
		} else if (filter.onlyUnAdmit == true) {
			condList.add(SUMMARY_R + '.' + Att_SUMMARY_R + '.' + getFieldName(AttSummary__c.Locked__c) + ' = false');
		}
		String whereCond;
		if (! condList.isEmpty()) {
			// ANDで条件をつなぐ
			whereCond = '(' + String.join(condList, ') AND (') + ')';
		} else {
			whereCond = '';
		}

		String soql =
				'SELECT ' + String.join(selectFldList, ', ') +
				+ ' FROM ' + AttManagedLeaveTake__c.SObjectType.getDescribe().getName();
		if (String.isNotBlank(whereCond)) {
			soql += ' WHERE ' + whereCond;
		}
		if (!forUpdate) {
			soql += ' ORDER BY ' + getFieldName(AttManagedLeaveTake__c.StartDate__c) + ' ASC';
		}
		if (forUpdate) {
			soql += ' FOR UPDATE';
		}
		// 結果からエンティティを作成する
		AppLimits.getInstance().setCurrentQueryCount();
		List<AttManagedLeaveTakeEntity> entityList = new List<AttManagedLeaveTakeEntity>();
		for (AttManagedLeaveTake__c grant : Database.query(soql)) {
			entityList.add(createEntity(grant));
		}
		AppLimits.getInstance().incrementQueryCount(AttManagedLeaveTake__c.SObjectType);

		return entityList;
	}
	public DeleteResult deleteEntityList(List<AttManagedLeaveTakeEntity> entityList) {
		return super.deleteEntityList(entityList);
	}
	private AttManagedLeaveTake__c createObj(AttManagedLeaveTakeEntity entity) {
		Boolean isInsert = String.isBlank(entity.id);
		AttManagedLeaveTake__c retObject = new AttManagedLeaveTake__c();
		retObject.Id = entity.Id;

		if (isInsert && entity.isChanged(AttManagedLeaveTakeEntity.Field.LEAVE_SUMMARY_ID)) {
			retObject.LeaveSummaryId__c = entity.leaveSummaryId;
		}
		if (isInsert && entity.isChanged(AttManagedLeaveTakeEntity.Field.LEAVE_REQUEST_ID)) {
			retObject.LeaveRequestId__c = entity.leaveRequestId;
		}
		if (entity.isChanged(AttManagedLeaveTakeEntity.Field.DAYS_TAKEN)) {
			retObject.DaysTaken__c = AppConverter.decValue(entity.daysTaken);
		}
		if (entity.isChanged(AttManagedLeaveTakeEntity.Field.START_DATE)) {
			retObject.StartDate__c = AppConverter.dateValue(entity.startDate);
		}
		if (entity.isChanged(AttManagedLeaveTakeEntity.Field.END_DATE)) {
			retObject.EndDate__c = AppConverter.dateValue(entity.endDate);
		}
		return retObject;
	}
	private AttManagedLeaveTakeEntity createEntity(AttManagedLeaveTake__c take) {
		AttManagedLeaveTakeEntity retEntity = new AttManagedLeaveTakeEntity();
		retEntity.setId(take.Id);
		retEntity.leaveSummaryId = take.LeaveSummaryId__c;
		retEntity.leaveRequestId = take.LeaveRequestId__c;
		retEntity.leaveId = take.LeaveRequestId__r.AttLeaveId__c;
		retEntity.employeeId = take.LeaveSummaryId__r.AttSummaryId__r.EmployeeHistoryId__r.BaseId__c;
		retEntity.startDate = AppDate.valueOf(take.StartDate__c);
		retEntity.endDate = AppDate.valueOf(take.EndDate__c);
		retEntity.daysTaken = AttDays.valueOf(take.DaysTaken__c);
		retEntity.resetChanged();
		return retEntity;
	}
}