/*
 * @author TeamSpirit Inc.
 * 
 * @date 2019
 * 
 * @description Scheduableclass for ExpJournalBatch
 * 
 * @group Expense
 */
public with sharing class ExpJournalBatchSchedule implements Schedulable {

	// To avoid governor limit, process 20 Reports per batch
	private static final Integer BATCH_SCOPE = 20;

	public void execute(System.SchedulableContext sc) {
		ExpJournalBatch batch = new ExpJournalBatch();
		Id jobId = Database.executeBatch(batch, BATCH_SCOPE);
	}
}