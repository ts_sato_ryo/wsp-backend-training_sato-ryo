/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group Expense
 *
 * @description Resource class for Vendor
 */
public with sharing class ExpVendorResource  {

	/** Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/** Total number of records to returns */
	private static final Integer SEARCH_RESULT_LIMIT = 100;

	/** Record class of ExpVendor */
	abstract public class ExpVendorBase {
		/** Record ID */
		public String id;
		/** Vendor code */
		public String code;
		/** Company ID */
		public String companyId;
		/** Currency Name */
		public String name;
		/** Currency Name(lang0) */
		public String name_L0;
		/** Currency Name(lang1) */
		public String name_L1;
		/** Currency Name(lang2) */
		public String name_L2;
		/** Payment Term */
		public String paymentTerm;
		/** Payment Term Code */
		public String paymentTermCode;
		/** Bank Account Type */
		public String bankAccountType;
		/** Bank Account Number */
		public String bankAccountNumber;
		/** Bank Code */
		public String bankCode;
		/** Bank Name */
		public String bankName;
		/** Branch Code */
		public String branchCode;
		/** Branch Name */
		public String branchName;
		/** Branch Address */
		public String branchAddress;
		/** Swift Code */
		public String swiftCode;
		/** Correspondent Bank Address */
		public String correspondentBankAddress;
		/** Correspondent Bank Name */
		public String correspondentBankName;
		/** Correspondent Branch Name */
		public String correspondentBranchName;
		/** Correspondent Swift Code */
		public String correspondentSwiftCode;
		/** Is Withholding Tax */
		public Boolean isWithholdingTax;
		/** Currency Code */
		public String currencyCode;
		/** Country */
		public String country;
		/** Payee Name */
		public String payeeName;
		/** Zip Code */
		public String zipCode;
		/** Address */
		public String address;
		/** Active */
		public Boolean active;
		/** Specify the Payment Due Date field Usage  */
		public String paymentDueDateUsage;

		/** Create entity from request parameter */
		public ExpVendorEntity createEntity(Map<String, Object> paramMap) {
			ExpVendorEntity entity = new ExpVendorEntity();

			entity.setId(this.id);
			if (paramMap.containsKey('code')) {
				entity.code = this.code;
			}
			if (paramMap.containsKey('companyId')) {
				entity.companyId = this.companyId;
			}
			if (paramMap.containsKey('name_L0')) {
				entity.nameL0 = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				entity.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				entity.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('paymentTerm')) {
				entity.paymentTerm = this.paymentTerm;
			}
			if (paramMap.containsKey('paymentTermCode')) {
				entity.paymentTermCode = this.paymentTermCode;
			}
			if (paramMap.containsKey('bankAccountType')) {
				entity.bankAccountType = ExpBankAccountType.valueOf(this.bankAccountType);
			}
			if (paramMap.containsKey('bankAccountNumber')) {
				entity.bankAccountNumber = this.bankAccountNumber;
			}
			if (paramMap.containsKey('bankCode')) {
				entity.bankCode = this.bankCode;
			}
			if (paramMap.containsKey('bankName')) {
				entity.bankName = this.bankName;
			}
			if (paramMap.containsKey('branchCode')) {
				entity.branchCode = this.branchCode;
			}
			if (paramMap.containsKey('branchName')) {
				entity.branchName = this.branchName;
			}
			if (paramMap.containsKey('branchAddress')) {
				entity.branchAddress = this.branchAddress;
			}
			if (paramMap.containsKey('swiftCode')) {
				entity.swiftCode = this.swiftCode;
			}
			if (paramMap.containsKey('correspondentBankAddress')) {
				entity.correspondentBankAddress = this.correspondentBankAddress;
			}
			if (paramMap.containsKey('correspondentBankName')) {
				entity.correspondentBankName = this.correspondentBankName;
			}
			if (paramMap.containsKey('correspondentBranchName')) {
				entity.correspondentBranchName = this.correspondentBranchName;
			}
			if (paramMap.containsKey('correspondentSwiftCode')) {
				entity.correspondentSwiftCode = this.correspondentSwiftCode;
			}
			if (paramMap.containsKey('isWithholdingTax')) {
				entity.isWithholdingTax = this.isWithholdingTax;
			}
			if (paramMap.containsKey('currencyCode')) {
				entity.currencyCode = ComIsoCurrencyCode.valueOf(this.currencyCode);
			}
			if (paramMap.containsKey('country')) {
				entity.country = this.country;
			}
			if (paramMap.containsKey('payeeName')) {
				entity.payeeName = this.payeeName;
			}
			if (paramMap.containsKey('zipCode')) {
				entity.zipCode = this.zipCode;
			}
			if (paramMap.containsKey('address')) {
				entity.address = this.address;
			}
			if (paramMap.containsKey('active')) {
				entity.active = this.active;
			}
			if (paramMap.containsKey('paymentDueDateUsage')) {
				entity.paymentDueDateUsage = ExpVendorPaymentDueDateUsage.valueOf(this.paymentDueDateUsage);
			}

			return entity;
		}

		/** Set data from entity */
		public void setData(ExpVendorEntity entity) {
			this.id = entity.id;
			this.code = entity.code;
			this.companyId = entity.companyId;
			this.name = entity.nameL.getValue();
			this.name_L0 = entity.nameL0;
			this.name_L1 = entity.nameL1;
			this.name_L2 = entity.nameL2;
			this.paymentTerm = entity.paymentTerm;
			this.paymentTermCode = entity.paymentTermCode;
			if (entity.bankAccountType != null) {
				this.bankAccountType = entity.bankAccountType.value;
			}
			this.bankAccountNumber = entity.bankAccountNumber;
			this.bankCode = entity.bankCode;
			this.bankName = entity.bankName;
			this.branchCode = entity.branchCode;
			this.branchName = entity.branchName;
			this.branchAddress = entity.branchAddress;
			this.correspondentBankAddress = entity.correspondentBankAddress;
			this.correspondentBankName = entity.correspondentBankName;
			this.correspondentBranchName = entity.correspondentBranchName;
			this.correspondentSwiftCode = entity.correspondentSwiftCode;
			this.isWithholdingTax = entity.isWithholdingTax;
			this.swiftCode = entity.swiftCode;
			if (entity.currencyCode != null) {
				this.currencyCode = entity.currencyCode.value;
			}
			this.country = entity.country;
			this.payeeName = entity.payeeName;
			this.zipCode = entity.zipCode;
			this.address = entity.address;
			this.active = entity.active;
			this.paymentDueDateUsage = entity.paymentDueDateUsage.value;
		}
	}

	/** Vendor record class */
	public class ExpVendorRecord extends ExpVendorBase {

	}

	/**
   * @description Parameter for new creation and update
   */
	public class UpsertParam extends ExpVendorBase implements RemoteApi.RequestParam {
	}

	/**
 * @description Parameter for deletion
 */
	public class DeleteParam implements RemoteApi.RequestParam {
		/** Record Id */
		public String id;
	}

	/**
   * Parameter for search
   * */
	public class SearchParam implements RemoteApi.RequestParam {
		public String Id;
		public String companyId;
		public boolean active;
		public String query;
	}

	public class GetRecentlyUsedListParam implements RemoteApi.RequestParam {
		public String employeeBaseId;
	}

	/**
   * @description Result of saving
   */
	public class SaveResult implements RemoteApi.ResponseParam {
		/** ID of the created record */
		public String id;
	}

	/**
   * @description Result of search
   */
	public class SearchResult implements RemoteApi.ResponseParam {
		public ExpVendorRecord[] records;
		public Boolean hasMore;
	}

	/**
	 * @description API to create a new Vendor record
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_VENDOR;

		/**
		 * @description Create a new Vendor record
		 * @param  req Request parameter (UpsertParam)
		 * @return Response (SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			UpsertParam param = (UpsertParam) req.getParam(UpsertParam.class);

			validateParam(param);

			ExpVendorEntity entity = param.createEntity(req.getParamMap());
			Repository.SaveResult result = ExpVendorService.createVendor(entity);

			SaveResult response = new SaveResult();
			response.id = result.details[0].id;
			return response;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 * */
		private void validateParam(UpsertParam param) {
			ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Com_Lbl_Code, param.code);
			ExpCommonUtil.validateStringIsNotBlank(MESSAGE.Admin_Lbl_Name, param.name_L0);
			ExpCommonUtil.validateId('Company ID', param.companyId, true);
			if (ExpVendorPaymentDueDateUsage.valueOf(param.paymentDueDateUsage) == null) {
				throw new App.ParameterException('paymentDueDateUsage', param.paymentDueDateUsage);
			}
		}
	}

	/**
 * @description API to update a Vendor record
 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_VENDOR;

		/**
	 * @description Update a record
	 * @param  req Request parameter (UpsertParam)
	 * @return Response null
	 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			UpsertParam param = (UpsertParam) req.getParam(UpsertParam.class);

			validateParam(param);

			ExpVendorEntity entity = param.createEntity(req.getParamMap());
			ExpVendorService.updateVendor(entity);

			return null;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 **/
		private void validateParam(UpsertParam param) {
			ExpCommonUtil.validateId('ID', param.id, true);
			if (ExpVendorPaymentDueDateUsage.valueOf(param.paymentDueDateUsage) == null) {
				throw new App.ParameterException('paymentDueDateUsage', param.paymentDueDateUsage);
			}
		}

	}

	/**
 * @description API to delete a Vendor record
 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		private final ComResourcePermission.Permission requriedPermission =
			ComResourcePermission.Permission.MANAGE_EXP_VENDOR;

		/**
	   * @description Delete a Vendor records
	   * @param  req Request parameter (DeleteParam)
	   * @return Response null
	   */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// check permission
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			DeleteParam param = (DeleteParam) req.getParam(DeleteParam.class);

			validateParam(param);

			ExpVendorService.deleteVendor(param.id);

			return null;
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 **/
		private void validateParam(DeleteParam param) {
			ExpCommonUtil.validateId('ID', param.id, true);
		}

	}

	/**
 * @description API to search Vendor records
 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * @description Search Vendor records
		 * @param  req Request parameter (SearchParam)
	 	* @return Response (SearchResult)
	 	*/
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			SearchParam param = (SearchParam)req.getParam(SearchParam.class);

			List<ExpVendorEntity> entityList = ExpVendorService.searchVendor(req.getParamMap(), SEARCH_RESULT_LIMIT + 1);

			return makeResponse(entityList);
		}
	}

	/**
	 * Make response records
	 * @param entityList Records of Vendor entity
	 */
	private static SearchResult makeResponse(List<ExpVendorEntity> entityList) {

		List<ExpVendorRecord> records = new List<ExpVendorRecord>();

		Integer numberOfRecordsToReturn = entityList.size() > SEARCH_RESULT_LIMIT ? SEARCH_RESULT_LIMIT: entityList.size();
		for (Integer i = 0; i < numberOfRecordsToReturn; i++) {
			ExpVendorRecord rec = new ExpVendorRecord();
			rec.setData(entityList.get(i));
			records.add(rec);
		}

		SearchResult result = new SearchResult();
		result.records = records;
		result.hasMore = entityList.size() > SEARCH_RESULT_LIMIT; // If entity list is the same, that means it has more data.
		return result;
	}

	public with sharing class GetRecentlyUsedListApi extends RemoteApi.ResourceBase {
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			GetRecentlyUsedListParam param = (GetRecentlyUsedListParam) req.getParam(GetRecentlyUsedListParam.class );
			validateParam(param);

			List<ExpVendorEntity> entityList = ExpVendorService.getRecentlyUsedVendorList(param.employeeBaseId);

			return makeResponse(entityList);
		}

		/**
		 * Validate params
		 * @param param Request parameter
		 **/
		private void validateParam(GetRecentlyUsedListParam param) {
			ExpCommonUtil.validateId('employeeBaseId', param.employeeBaseId, true);
		}
	}
}