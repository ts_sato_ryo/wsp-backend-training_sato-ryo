/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Expense
 *
 * @description Test class for ExpEmployeeGroupEntity
 */
@IsTest
private class ExpEmployeeGroupEntityTest {

	/*
	 * Test Data Class
	 */
	private class TestData extends TestData.TestDataEntity {
		/**
		 * Expense Employee Group Data
		 */
		public List<ExpEmployeeGroupEntity> expEmployeeGroupList = new List<ExpEmployeeGroupEntity>();

		/**
		 * Constructor
		 */
		public TestData() {
			super();
			expEmployeeGroupList = createExpEmployeeGroupList('Testcode', company.Id, 3);
		}

		/**
		 * Create and Insert Employee Group List
		 * @param code Test Data Prefix Code
		 * @param companyId Company ID
		 * @param size Number of record to create
		 * @return List of entities created
		 */
		public List<ExpEmployeeGroupEntity> createExpEmployeeGroupList(String code, Id companyId, Integer size) {
			List<ExpEmployeeGroup__c> expEmpGroupList =
				ComTestDataUtility.createExpEmployeeGroupList(code, companyId, size);

			ExpEmployeeGroupRepository repo = new ExpEmployeeGroupRepository();
			for (ExpEmployeeGroup__c expEmpGroupSObj : expEmpGroupList) {
				expEmployeeGroupList.add(repo.createEntity(expEmpGroupSObj));
			}
			return expEmployeeGroupList;
		}
	}

	/**
	 * Confirm unique key is created properly
	 */
	@isTest static void createUniqKeyTest() {
		ExpEmployeeGroupEntity entity = new ExpEmployeeGroupEntity();

		// When expense employee group code and company code are not empty, key would be created.
		entity.code = 'TestCode';
		String companyCode = 'CompanyCode';
		System.assertEquals(companyCode + '-' + entity.code, entity.createUniqKey(companyCode));

		// When expense employee group code is empty, exception would occur.
		entity.code = '';
		try {
			entity.uniqKey = entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occured');
		} catch(App.IllegalStateException e) {
			System.assertEquals('[Server Error]Failed to create unique key of "Expense Employee Group". code is empty.', e.getMessage());
		}

		// When company code is empty, exception would occur.
		entity.code = 'TestCode';
		companyCode = '';
		try {
			entity.uniqKey = entity.createUniqKey(companyCode);
			TestUtil.fail('Expected exception has not occurred');
		} catch(App.ParameterException e) {
			System.assertEquals('[Server Error]Failed to create unique key of "Expense Employee Group". companyCode is empty.', e.getMessage());
		}
	}

	/*
	 * Field Change Status Test
	 */
	@isTest static void isChangedTest() {
		ExpEmployeeGroupEntityTest.TestData testData = new ExpEmployeeGroupEntityTest.TestData();
		ExpEmployeeGroupEntity entity = testData.expEmployeeGroupList[0];
		ExpEmployeeGroupEntity entity1 = testData.expEmployeeGroupList[1];

		entity.resetChanged();
		for (ExpEmployeeGroupEntity.Field field : ExpEmployeeGroupEntity.Field.values()) {
			System.assertEquals(false, entity.isChanged(field));
		}

		entity1.resetChanged();
		ExpEmployeeGroupEntity.Field targetField = ExpEmployeeGroupEntity.Field.DESCRIPTION_L0;
		System.assertEquals(false, entity1.isChanged(targetField));
		entity1.descriptionL0 = 'Description Changed';
		entity1.descriptionL1 = 'Description Changed';
		System.assertEquals(true, entity1.isChanged(targetField));
		targetField = ExpEmployeeGroupEntity.Field.DESCRIPTION_L1;
		System.assertEquals(true, entity1.isChanged(targetField));
		targetField = ExpEmployeeGroupEntity.Field.DESCRIPTION_L2;
		System.assertEquals(false, entity1.isChanged(targetField));

	}

	/**
	 * Get field value Test
	 */
	@isTest static void getFieldValueTest() {
		ExpEmployeeGroupEntityTest.TestData testData = new ExpEmployeeGroupEntityTest.TestData();
		ExpEmployeeGroupEntity entity = testData.expEmployeeGroupList[1];

		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.ACTIVE.name()), entity.active);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.CODE.name()), entity.code);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.COMPANY_ID.name()), entity.companyId);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.DESCRIPTION_L0.name()), entity.descriptionL0);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.DESCRIPTION_L1.name()), entity.descriptionL1);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.DESCRIPTION_L2.name()), entity.descriptionL2);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.NAME_L0.name()), entity.nameL0);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.NAME_L1.name()), entity.nameL1);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.NAME_L2.name()), entity.nameL2);
		System.assertEquals(entity.getFieldValue(ExpEmployeeGroupEntity.Field.UNIQ_KEY.name()), entity.uniqKey);
	}

	/**
	 * Set field value Test
	 */
	@isTest static void setFieldValueTest() {
		ExpEmployeeGroupEntityTest.TestData testData = new ExpEmployeeGroupEntityTest.TestData();
		ExpEmployeeGroupEntity entity = testData.expEmployeeGroupList[2];

		for (ExpEmployeeGroupEntity.Field field : ExpEmployeeGroupEntity.Field.values()) {
			ExpEmployeeGroupEntity testEntity = new ExpEmployeeGroupEntity();

			Object value = entity.getFieldValue(field.name());
			testEntity.setFieldValue(field.name(), value);
			System.assertEquals(value, testEntity.getFieldValue(field.name()));
		}
	}

}