/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * 休暇付与のリポジトリのテストクラス
 */
@isTest
private class AttManagedLeaveGrantRepositoryTest {
	@isTest static void saveEntityTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('b', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		Date dt = Date.today();
		AttManagedLeaveGrantEntity grantData = new AttManagedLeaveGrantEntity();
		grantData.employeeId = testData.employee.id;
		grantData.leaveId = annualLeave.id;
		grantData.daysGranted = AttDays.valueOf(1);
		grantData.daysLeft = AttDays.valueOf(1);
		grantData.validDateFrom = AppDate.valueOf(dt);
		grantData.validDateTo = AppDate.valueOf(dt.addYears(2));
		grantData.comment = 'abc';

		System.assertEquals(true, grantData.isChanged(AttManagedLeaveGrantEntity.Field.EMPLOYEE_ID));
		System.assertEquals(true, grantData.isChanged(AttManagedLeaveGrantEntity.Field.LEAVE_ID));
		System.assertEquals(true, grantData.isChanged(AttManagedLeaveGrantEntity.Field.DAYS_GRANTED));
		System.assertEquals(true, grantData.isChanged(AttManagedLeaveGrantEntity.Field.DAYS_LEFT));
		System.assertEquals(true, grantData.isChanged(AttManagedLeaveGrantEntity.Field.VALID_FROM));
		System.assertEquals(true, grantData.isChanged(AttManagedLeaveGrantEntity.Field.VALID_TO));
		System.assertEquals(true, grantData.isChanged(AttManagedLeaveGrantEntity.Field.COMMENT));

		Test.startTest();
		AttManagedLeaveGrantRepository rep = new AttManagedLeaveGrantRepository();
		Repository.SaveResult saveResult = rep.saveEntity(grantData);
		Id grantId = saveResult.details[0].id;
		Test.stopTest();

		List<AttManagedLeaveGrant__c> grantList =
				[SELECT Id, EmployeeBaseId__c, LeaveId__c, DaysGranted__c, DaysLeft__c, ValidFrom__c, ValidTo__c, Comment__c
				FROM AttManagedLeaveGrant__c
				WHERE Id = :grantId];
		System.assertEquals(1, grantList.size());

		AttManagedLeaveGrant__c grant = grantList[0];
		System.assertEquals(grantData.employeeId, grant.EmployeeBaseId__c);
		System.assertEquals(grantData.leaveId, grant.LeaveId__c);
		System.assertEquals(AppConverter.decValue(grantData.daysGranted), grant.DaysGranted__c);
		System.assertEquals(AppConverter.decValue(grantData.daysLeft), grant.DaysLeft__c);
		System.assertEquals(AppConverter.dateValue(grantData.validDateFrom), grant.ValidFrom__c);
		System.assertEquals(AppConverter.dateValue(grantData.validDateTo), grant.ValidTo__c);
		System.assertEquals(grantData.comment, grant.Comment__c);
	}

	@isTest static void seachEntityTest() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('a', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});
		AttLeaveEntity otherLeave = testData.createLeave('b', '有休', AttLeaveType.PAID, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		Date dt = date.today();
		insert new AttManagedLeaveGrant__c(
				EmployeeBaseId__c = testData.employee.id,
				LeaveId__c = annualLeave.id,
				DaysGranted__c = 10,
				DaysLeft__c = 10,
				ValidFrom__c = dt.addMonths(-1),
				ValidTo__c = dt.addYears(1),
				Comment__c = 'a');
		insert new AttManagedLeaveGrant__c(
				EmployeeBaseId__c = testData.employee.id,
				LeaveId__c = annualLeave.id,
				DaysGranted__c = 11,
				DaysLeft__c = 11,
				ValidFrom__c = dt.addMonths(-2),
				ValidTo__c = dt.addYears(2),
				Comment__c = 'b');
		insert new AttManagedLeaveGrant__c(
				EmployeeBaseId__c = testData.employee.id,
				LeaveId__c = otherLeave.id,
				DaysGranted__c = 3,
				DaysLeft__c = 3,
				ValidFrom__c = dt.addMonths(-2),
				ValidTo__c = dt.addYears(2),
				Comment__c = 'b');

		Test.startTest();
		AttManagedLeaveGrantRepository.SearchFilter filter = new AttManagedLeaveGrantRepository.SearchFilter();
		filter.empId = testData.employee.id;
		filter.leaveId = annualLeave.id;

		List<AttManagedLeaveGrantEntity> grantDataList = new AttManagedLeaveGrantRepository().searchEntityList(filter, false);
		Test.stopTest();

		System.assertEquals(2, grantDataList.size());
		AttManagedLeaveGrantEntity grantData = grantDataList[0];
		System.assertEquals(dt.addMonths(-2), AppConverter.dateValue(grantData.validDateFrom));
		System.assertEquals(dt.addYears(2), AppConverter.dateValue(grantData.validDateTo));
		System.assertEquals(11, AppConverter.decValue(grantData.daysGranted));
		System.assertEquals(11, AppConverter.decValue(grantData.daysLeft));
		System.assertEquals('b', grantData.comment);

		grantData = grantDataList[1];
		System.assertEquals(dt.addMonths(-1), AppConverter.dateValue(grantData.validDateFrom));
		System.assertEquals(dt.addYears(1), AppConverter.dateValue(grantData.validDateTo));
		System.assertEquals(10, AppConverter.decValue(grantData.daysGranted));
		System.assertEquals(10, AppConverter.decValue(grantData.daysLeft));
		System.assertEquals('a', grantData.comment);
	}

	/**
	 * searchEntityListのテスト
	 * 論理削除済みの休暇付与のレコードが取得されないことを確認する
	 */
	@isTest static void searchEntityTestGrantRemoved() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('a', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		// 論理削除済みの休暇付与
		AttManagedLeaveGrant__c grant1 = new AttManagedLeaveGrant__c(
				EmployeeBaseId__c = testData.employee.id,
				LeaveId__c = annualLeave.id,
				Removed__c = true);
		// 論理削除をしていない休暇付与
		AttManagedLeaveGrant__c grant2 = new AttManagedLeaveGrant__c(
				EmployeeBaseId__c = testData.employee.id,
				LeaveId__c = annualLeave.id,
				Removed__c = false);
		insert new List<AttManagedLeaveGrant__c>{grant1, grant2};

		// 実行
		Test.startTest();
		AttManagedLeaveGrantRepository.SearchFilter filter = new AttManagedLeaveGrantRepository.SearchFilter();
		filter.empId = testData.employee.id;
		filter.leaveId = annualLeave.id;
		List<AttManagedLeaveGrantEntity> grantDataList = new AttManagedLeaveGrantRepository().searchEntityList(filter, false);
		Test.stopTest();

		// 検証（論理削除済みの休暇付与が取得されないこと）
		System.assertEquals(1, grantDataList.size());
		AttManagedLeaveGrantEntity grantData = grantDataList[0];
		System.assertEquals(grant2.Id, grantData.id);
		System.assertEquals(grant2.Removed__c, grantData.isRemoved);
	}

	/**
	 * searchEntityListのテスト
	 * 論理削除済みの休暇付与調整のレコードが取得されないことを確認する
	 */
	@isTest static void searchEntityTestAdjustRemoved() {
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		AttLeaveEntity annualLeave = testData.createLeave('a', '年次有休', AttLeaveType.ANNUAL, true, new List<AttLeaveRange>{AttLeaveRange.RANGE_DAY});

		Date dt = date.newInstance(2019, 5, 1);
		// 論理削除をしていない休暇付与
		AttManagedLeaveGrant__c grant = new AttManagedLeaveGrant__c(
				EmployeeBaseId__c = testData.employee.id,
				LeaveId__c = annualLeave.id,
				Removed__c = false);
		insert grant;

		// 論理削除済みの休暇付与調整
		AttManagedLeaveGrantAdjust__c grantAdjust1 = new AttManagedLeaveGrantAdjust__c(
				LeaveGrantId__c = grant.Id,
				Removed__c = true);
		// 論理削除をしていない休暇付与調整
		AttManagedLeaveGrantAdjust__c grantAdjust2 = new AttManagedLeaveGrantAdjust__c(
				LeaveGrantId__c = grant.Id,
				Removed__c = false);
		insert new List<AttManagedLeaveGrantAdjust__c>{grantAdjust1, grantAdjust2};

		// 実行
		Test.startTest();
		AttManagedLeaveGrantRepository.SearchFilter filter = new AttManagedLeaveGrantRepository.SearchFilter();
		filter.empId = testData.employee.id;
		filter.leaveId = annualLeave.id;
		List<AttManagedLeaveGrantEntity> grantDataList = new AttManagedLeaveGrantRepository().searchEntityList(filter, false);
		Test.stopTest();

		// 検証（論理削除済みの休暇付与調整が取得されないこと）
		AttManagedLeaveGrantEntity grantData = grantDataList[0];
		System.assertEquals(1, grantData.adjustList.size());
		AttManagedLeaveGrantEntity.GrantAdjustEntity adjustData = grantData.adjustList[0];
		System.assertEquals(grantAdjust2.Id, adjustData.id);
		System.assertEquals(grantAdjust2.Removed__c, adjustData.isRemoved);
	}
}