/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 経費 Expense
 *
 * @description 税区分ベースレコード操作APIを実装するクラス Resource class for ExpTaxTypeBase object
 */
public with sharing class ExpTaxTypeResource {

	/** メッセージ Message */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 税区分レコードを表すクラス Tax type data class
	 */
	public class ExpTaxType implements RemoteApi.RequestParam {
		/** 税区分ベースレコードID */
		public String id;
		/** 税区分履歴レコードID */
		public String historyId;
		/** 税区分名(取得専用。組織の言語設定に応じて言語が切り替わる) */
		public String name;
		/** 税区分名(言語0) */
		public String name_L0;
		/** 税区分名(言語1) */
		public String name_L1;
		/** 税区分名(言語2) */
		public String name_L2;
		/** 税区分コード */
		public String code;
		/** 国レコードID */
		public String countryId;
		/** 国ルックアップ */
		public LookupField country;
		/** 会社レコードID */
		public String companyId;
		/** 税率 */
		public Decimal rate;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** コメント */
		public String comment;

		/**
		 * パラメータの値を設定した税区分ベースエンティティを作成する Create base entity
		 * @param paramMap リクエストパラメエータのMap(キーが存在するパラメータのみ更新する)
		 * @return 更新情報を設定したエンティティ
		 */
		public ExpTaxTypeBaseEntity createBaseEntity(Map<String, Object> paramMap) {
			ExpTaxTypeBaseEntity base = new ExpTaxTypeBaseEntity();

			base.setId(this.id);
			if (paramMap.containsKey('code')) {
				base.code = this.code;
			}
			// 国ID
			if (paramMap.containsKey('countryId')) {
				base.countryId = this.countryId;
			}
			// 会社ID
			if (paramMap.containsKey('companyId')) {
				base.companyId = this.companyId;
			}

			// TODO 名前項目は暫定対応。とりあえず現状維持
			if (paramMap.containsKey('name_L0')) {
				base.name = this.name_L0;
			}

			return base;
		}

		/**
		 * パラメータの値を設定した税区分履歴エンティティを作成する Create history entity
		 * @param paramMap リクエストパラメエータのMap(キーが存在するパラメータのみ更新する)
		 * @return 更新情報を設定したエンティティ
		 */
		public ExpTaxTypeHistoryEntity createHistoryEntity(Map<String, Object> paramMap) {
			ExpTaxTypeHistoryEntity history = new ExpTaxTypeHistoryEntity();

			// 以下、レコード作成時のみ値をセット
			if (paramMap.containsKey('name_L0')) {
				history.nameL0 = this.name_L0;

				// TODO 名前項目は暫定対応。とりあえず現状維持
				history.name = this.name_L0;
			}
			if (paramMap.containsKey('name_L1')) {
				history.nameL1 = this.name_L1;
			}
			if (paramMap.containsKey('name_L2')) {
				history.nameL2 = this.name_L2;
			}
			if (paramMap.containsKey('rate')) {
				history.rate = this.rate;
			}
			if (paramMap.containsKey('validDateFrom')) {
				history.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (history.validFrom == null) {
				// デフォルトは実行日
				history.validFrom = AppDate.today();
			}
			// レコード更新時は失効日を無視する (ExpTaxTypeRepository参照)
			if (paramMap.containsKey('validDateTo')) {
				history.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (history.validTo == null) {
				history.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}
			if (paramMap.containsKey('comment')) {
				history.historyComment = this.comment;
			}

			return history;
		}
	}

	/**
	 * @description レコードのルックアップ項目を表すクラス
	 */
	public class LookupField {
		/** レコードのName項目 */
		public String name;
	}

	/**
	 * @description 税区分レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したベースレコードID */
		public String id;
	}

	/**
	 * @description 税区分レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EXP_TAX_TYPE;

		/**
		 * @description 税区分レコードを1件作成する
		 * @param  req リクエストパラメータ(ExpTaxType)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			ExpTaxType param = (ExpTaxType)req.getParam(ExpTaxTypeResource.ExpTaxType.class);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			ExpTaxTypeBaseEntity base = param.createBaseEntity(req.getParamMap());
			ExpTaxTypeHistoryEntity history = param.createHistoryEntity(req.getParamMap());
			base.addHistory(history);

			// 保存する
			ExpTaxTypeService service = new ExpTaxTypeService();
			service.updateUniqKey(base);
			Id resId = service.saveNewEntity(base);
			// 国の税区分の場合はユニークキーにレコードIDを設定する
			if (base.countryId != null) {
				base.setId(resId);
				service.updateUniqKey(base);
				service.updateBase(base);
			}

			ExpTaxTypeResource.SaveResult res = new ExpTaxTypeResource.SaveResult();
			res.Id = resId;
			return res;
		}
	}

	/**
	 * @desctiprion 税区分レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EXP_TAX_TYPE;

		/**
		 * @description 税区分レコードを1件更新する(ベースの項目のみ)
		 * @param  req リクエストパラメータ(ExpTaxType)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			// 更新情報を設定したエンティティを作成する
			ExpTaxType param = (ExpTaxType)req.getParam(ExpTaxType.class);

			// パラメータのバリデーション
			validateParam(param);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 更新する
			ExpTaxTypeBaseEntity base = param.createBaseEntity(req.getParamMap());
			ExpTaxTypeService service = new ExpTaxTypeService();
			service.updateUniqKey(base);
			service.updateBase(base);

			// 成功レスポンスをセットする
			return null;
		}

		/**
		 * ベース更新APIのバリデーションを実行
		 */
		private void validateParam(ExpTaxTypeResource.ExpTaxType param) {
			// ID
			ExpCommonUtil.validateId('ID', param.id, true);
		}
	}

	/**
	 * @description 税区分レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			ExpCommonUtil.validateId('id', this.id, true);
		}
	}

	/**
	 * 税区分レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EXP_TAX_TYPE;

		/**
		 * @description 税区分レコードを1件削除する
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 対象税区分が紐づいている費目を検索 / search for expense type whose target tax type is associated
			ExpTypeRepository.SearchFilter filter = new ExpTypeRepository.SearchFilter();
			filter.taxTypeBaseIds = new Set<Id>{param.id};
			List<ExpTypeEntity> expTypeList = new ExpTypeRepository().searchEntity(filter);

			// 費目が存在する場合はエラー / if exists expense type, exception is thrown
			if (expTypeList != null && !expTypeList.isEmpty()) {
				throw new App.IllegalStateException(MESSAGE.Com_Err_CannotDeleteReference);
			}

			// 対象税区分が紐づいている内訳を取得 / get a expense record item list whose target tax type is associated with
			List<ExpRecordItemEntity> expRecordItemEntityList = new ExpRecordRepository().getRecordItemEntityListByExpTaxTypeId(param.id);

			// 内訳が存在する場合はエラー / if expense record item　exists, exception is thrown
			if (expRecordItemEntityList != null && !expRecordItemEntityList.isEmpty()) {
				throw new App.IllegalStateException(MESSAGE.Com_Err_CannotDeleteReference);
			}

			try {
				// レコードを削除
				(new ExpTaxTypeService()).deleteEntity(param.id);
			} catch (DmlException e) {
				// すでに削除済みの場合は成功レスポンスを返す
				if (e.getDmlType(0) != System.StatusCode.ENTITY_IS_DELETED) {
					throw e;
				}
			}

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 税区分レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** レコードID */
		public String id;
		/** 関連する国レコードID */
		public String countryId;
		/** 関連する会社レコードID */
		public String companyId;
		/** 有効期間の対象日 */
		public Date targetDate;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {
			// ID
			ExpCommonUtil.validateId('id', this.id, false);

			// countryId
			ExpCommonUtil.validateId('countryId', this.countryId, false);

			// companyId
			ExpCommonUtil.validateId('companyId', this.companyId, false);

		}
	}

	/**
	 * @description 税区分レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public ExpTaxType[] records;
	}

	/**
	 * @description 税区分レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 税区分レコードを1件登録する
		 * @param  req リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);

			// パラメータのバリデーション
			param.validate();

			// 税区分を検索
			AppDate targetDate = param.targetDate == null ? AppDate.today() : AppDate.valueOf(param.targetDate);
			ExpTaxTypeRepository.SearchFilter filter = new ExpTaxTypeRepository.SearchFilter();
			filter.baseIds = String.isBlank(param.id) ? null : new List<Id>{param.id};
			filter.countryIds = String.isBlank(param.countryId) ? null : new List<Id>{param.countryId};
			filter.companyIds = String.isBlank(param.companyId) ? null : new List<Id>{param.companyId};
			filter.targetDate = targetDate;
			List<ExpTaxTypeBaseEntity> baseList = (new ExpTaxTypeRepository()).searchEntity(filter);

			// レスポンスクラスに変換
			List<ExpTaxType> taxTypeList = new List<ExpTaxType>();
			for (ExpTaxTypeBaseEntity base : baseList) {
				// パラメータ未指定の場合は、国の税区分のみを返す
				if (String.isBlank(param.id) && String.isBlank(param.countryId) && String.isBlank(param.companyId)) {
					if (base.companyId != null) {
						continue;
					}
				}

				ExpTaxTypeHistoryEntity history = base.getHistoryList().get(0);
				ExpTaxType taxType = new ExpTaxType();
				taxType.id = base.id;
				taxType.code = base.code;
				taxType.countryId = base.countryId;
				taxType.country = new LookupField();
				if (base.country != null) {
					taxType.country.name = base.country.nameL.getValue();
				}
				taxType.companyId = base.companyId;
				taxType.historyId = history.id;
				taxType.name = history.nameL.getValue(); // ユーザ言語での税区分名
				taxType.name_L0 = history.nameL.valueL0;
				taxType.name_L1 = history.nameL.valueL1;
				taxType.name_L2 = history.nameL.valueL2;
				taxType.validDateFrom = AppConverter.dateValue(history.validFrom);
				taxType.validDateTo = AppConverter.dateValue(history.validTo);
				taxType.rate = history.rate;

				taxTypeList.add(taxType);
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = taxTypeList;
			return res;
		}
	}
}