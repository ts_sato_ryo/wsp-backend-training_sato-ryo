/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group Common
 *
 * @description Custom Hint Entity
 */
public with sharing class CustomHintEntity extends Entity {

	/** Custom Hint Field Name Max Length */
	public static final Integer FIELD_NAME_MAX_LENGTH = 80;

	/** Custom Hint Description Max Length */
	public static final Integer DESCRIPTION_MAX_LENGTH = 400;

	/**
	 * Constructor
	 */
	public CustomHintEntity() {
	}

	/** Custom Hint Field */
	public enum Field {
		COMPANY_ID,
		MODULE_TYPE,
		FIELD_NAME,
		DESCRIPTION_L0,
		DESCRIPTION_L1,
		DESCRIPTION_L2
	}

	public enum ModuleType {
		Expense
	}

	/** Map of Fields with Name as the Key and the Field enum as the value */
	public static final Map<String, Field> FIELD_MAP;
	static {
		final Map<String, Field> fieldMap = new Map<String, Field>();
		for (Field f : Field.values()) {
			fieldMap.put(f.name(), f);
		}
		FIELD_MAP = fieldMap;
	}

	/** Map of ModuleType with Name as the Key and the ModuleType enum as the value */
	public static final Map<String, ModuleType> MODULE_TYPE_MAP;
	static {
		final Map<String, ModuleType> moduleTypeMap = new Map<String, ModuleType>();
		for (ModuleType f : CustomHintEntity.ModuleType.values()) {
			moduleTypeMap.put(f.name(), f);
		}
		MODULE_TYPE_MAP = moduleTypeMap;
	}

	/** Changed Field Set */
	private Set<CustomHintEntity.Field> isChangedFieldSet = new Set<CustomHintEntity.Field>();

	/** Custom Hint id */
	public String id;

	/** Custom Hint Name (Reference only) */
	public String name;

	/** Company Id */
	public Id companyId {
		get;
		set {
			companyId = value;
			setChanged(CustomHintEntity.Field.COMPANY_ID);
		}
	}

	/** Module Type */
	public ModuleType moduleType {
		get;
		set {
			moduleType = value;
			setChanged(CustomHintEntity.Field.MODULE_TYPE);
		}
	}

	/** Field */
	public String fieldName {
		get;
		set {
			fieldName = value;
			setChanged(CustomHintEntity.Field.FIELD_NAME);
		}
	}

	/** Description(L0) */
	public String description_L0 {
		get;
		set {
			description_L0 = value;
			setChanged(CustomHintEntity.Field.DESCRIPTION_L0);
		}
	}

	/** Description(L1) */
	public String description_L1 {
		get;
		set {
			description_L1 = value;
			setChanged(CustomHintEntity.Field.DESCRIPTION_L1);
		}
	}

	/** Description(L2) */
	public String description_L2 {
		get;
		set {
			description_L2 = value;
			setChanged(CustomHintEntity.Field.DESCRIPTION_L2);
		}
	}

	/** Description (Reference Only) */
	public AppMultiString description {
		get {
			return new AppMultiString(description_L0, description_L1, description_L2);
		}
	}

	/**
	 * Check field is changed
	 * @param field Custom Hint field
	 * @return true if the field have been changed
	 */
	public Boolean isChanged(CustomHintEntity.Field field) {
		return this.isChangedFieldSet.contains(field);
	}

	/**
	 * Reset the changed status
	 */
	public void resetChanged() {
		this.isChangedFieldSet.clear();
	}

	/**
	 * Return the value of the specified field name
	 * @param targetField Target Field name
	 * @return Field enum value matched the specified field name
	 */
	public Object getFieldValue(String fieldName) {
		Field targetField = FIELD_MAP.get(fieldName);
		if (targetField == null) {
			throw new App.ParameterException('Unknown field : ' + fieldName);
		}
		return getFieldValue(targetField);
	}

	public Object getFieldValue(CustomHintEntity.Field targetField) {
		if (targetField == CustomHintEntity.Field.COMPANY_ID) {
			return this.companyId;
		}
		if (targetField == CustomHintEntity.Field.MODULE_TYPE) {
			return this.moduleType.name();
		}
		if (targetField == CustomHintEntity.Field.FIELD_NAME) {
			return this.fieldName;
		}
		if (targetField == CustomHintEntity.Field.DESCRIPTION_L0) {
			return this.description_L0;
		}
		if (targetField == CustomHintEntity.Field.DESCRIPTION_L1) {
			return this.description_L1;
		}
		if (targetField == CustomHintEntity.Field.DESCRIPTION_L2) {
			return this.description_L2;
		}

		throw new App.UnsupportedException('CustomHintEntity.getFieldValue: Unexpected field. targetField='+ targetField);
	}

	/**
	 * Set the value to the specified field
	 * @param targetField Target field to set the value
	 * @pram value Value to be set
	 */
	public void setFieldValue(String fieldName, Object value) {
		Field targetField = FIELD_MAP.get(fieldName);
		if (targetField == null) {
			throw new App.ParameterException('Unknown field : ' + fieldName);
		}
		setFieldValue(targetField, value);
	}

	public void setFieldValue(Field targetField, Object value) {
		if (targetField == Field.COMPANY_ID) {
			this.companyId = (Id)value;
		} else if (targetField == Field.MODULE_TYPE) {
			this.moduleType = (ModuleType)value;
		} else if (targetField == Field.FIELD_NAME) {
			this.fieldName = (String)value;
		} else if (targetField == Field.DESCRIPTION_L0) {
			this.description_L0 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L1) {
			this.description_L1 = (String)value;
		} else if (targetField == Field.DESCRIPTION_L2) {
			this.description_L2 = (String)value;
		} else {
			throw new App.UnsupportedException('CustomHintEntity.setFieldValue: Unexpected field. targetField=' + targetField);
		}
	}

	/**
	 *  Record changed field
	 *  @param field Custom Hint field
	 */
	private void setChanged(CustomHintEntity.Field field) {
		this.isChangedFieldSet.add(field);
	}
}