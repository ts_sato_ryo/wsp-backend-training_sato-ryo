/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 工数設定履歴エンティティを表すクラスのテスト
 */
@isTest
private class TimeSettingHistoryEntityTest {

	/** テストデータクラス */
	private class EntityTestData extends TestData.TestDataEntity {}

	/**
	 * エンティティを複製できることを確認する
	 */
	@isTest static void copyTest() {
		EntityTestData testData = new EntityTestData();

		TimeSettingHistoryEntity history = testData.timeSetting.getHistory(0);
		history.name = 'テスト工数設定';
		history.baseId = testData.timeSetting.id;
		history.historyComment = '改定コメント';
		history.validFrom = AppDate.today();
		history.validTo = history.validFrom.addDays(10);
		history.isRemoved = false;
		history.uniqKey = history.name + history.validFrom.formatYYYYMMDD();
		history.nameL0 = history.name + '_L0';
		history.nameL1 = history.name + '_L1';
		history.nameL2 = history.name + '_L2';

		// コピー実行
		TimeSettingHistoryEntity copyHistory = history.copy();

		// 検証
		System.assertEquals(history.nameL0, copyHistory.nameL0);
		System.assertEquals(history.nameL1, copyHistory.nameL1);
		System.assertEquals(history.nameL2, copyHistory.nameL2);

		System.assertEquals(null, copyHistory.id); // IDはコピーされない
		System.assertEquals(history.name, copyHistory.name);
		System.assertEquals(history.baseId, copyHistory.baseId);
		System.assertEquals(history.historyComment, copyHistory.historyComment);
		System.assertEquals(history.validFrom, copyHistory.validFrom);
		System.assertEquals(history.validTo, copyHistory.validTo);
		System.assertEquals(history.isRemoved, copyHistory.isRemoved);
		System.assertEquals(history.uniqKey, copyHistory.uniqKey);
	}

}
