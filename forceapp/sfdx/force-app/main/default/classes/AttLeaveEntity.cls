/**
  * @author TeamSpirit Inc.
  * @date 2017
  *
  * @group 勤怠
  *
  * 休暇マスタのエンティティ
  */
public with sharing class AttLeaveEntity extends AttLeaveGeneratedEntity {

	/** コードの最大文字数 */
	public static final Integer CODE_MAX_LENGTH = 20;
	/** 休暇名の最大文字数 */
	public static final Integer NAME_MAX_LENGTH = 80;
	/** 表示順の最小値 */
	public static final Integer ORDER_MIN_VALUE = 1;
	/** 表示順の最大値 */
	public static final Integer ORDER_MAX_VALUE = 9999;
	/** 集計Noの最大値 */
	public static final Integer SUMMARY_NO_MAX_VALUE = 99;

	/**
	 * コンストラクタ
	 */
	public AttLeaveEntity() {
		super(new AttLeave__c());
	}

	/**
	 * コンストラクタ
	 */
	public AttLeaveEntity(AttLeave__c sobj) {
		super(sobj);
	}

	/** 休暇名(多言語対応。参照専用) */
	public AppMultiString nameL {
		get {
			return new AppMultiString(nameL0, nameL1, nameL2);
		}
	}

	/** 休暇範囲 */
  public static final Schema.SObjectField FIELD_LEAVE_RANGES = AttLeave__c.Range__c;
  public List<AttLeaveRange> leaveRanges {
		get {
			if (isChanged(FIELD_LEAVE_RANGES)) {
					return (List<AttLeaveRange>)getFieldValue(FIELD_LEAVE_RANGES);
			}
			setFieldValue(FIELD_LEAVE_RANGES, convertLeaveRangesValueToList(sobj.Range__c));
			return (List<AttLeaveRange>)getFieldValue(FIELD_LEAVE_RANGES);
		}
		set {
			setFieldValue(FIELD_LEAVE_RANGES, value);
		}
	}

	/**
	 * @description SObjectに保存されている休暇範囲の値をList型に変換する
	 * @param leaveRanges SObjectの休暇範囲の値
	 * @return 変換後の休暇範囲のリスト。項目値がnullの場合は空のリストが返却される
	 */
	private List<AttLeaveRange> convertLeaveRangesValueToList(String leaveRanges) {
		return AttLeaveRange.valuesOf(leaveRanges);
	}

	/**
	 * @description SObjectに保存されている休暇範囲リストの値をSObjectの項目値に変換する
	 * @param leaveRanges 休暇範囲のリスト
	 * @return 変換後のリスト 項目値がnullの場合はnullが返却される
	 */
	private String convertLeaveRangesValueToSObject(List<AttLeaveRange> leaveRanges) {
		return AppConverter.stringValue(leaveRanges);
	}

	/**
	 * エンティティの複製を作成する(IDはコピーされません)
	 */
	public AttLeaveEntity copy() {
		AttLeaveEntity copyEntity = new AttLeaveEntity();
		setAllFieldValues(copyEntity);
		// List型の項目をコピー
		copyEntity.leaveRanges = this.leaveRanges == null ? null : this.leaveRanges.clone();
		return copyEntity;
	}

	public override AttLeave__c createSObject() {
		AttLeave__c sobj = new AttLeave__c(Id = super.id);
		for (Schema.SObjectField field : changedValues.keySet()) {
			if (field == FIELD_LEAVE_RANGES) {
				continue;
			}
			sobj.put(field, changedValues.get(field));
		}
		sobj.Range__c = convertLeaveRangesValueToSObject(this.leaveRanges);
		return sobj;
	}
}