/**
 * @author TeamSpirit Inc.
 * @date 2019
 *
 * @group 共通
 *
 * DelegatedApproverSettingRepositoryのテスト
 */
@isTest
public class DelegatedApproverSettingRepositoryTest {

	/**
	 * getDelegatedApproverSettingListをテストする
	 */
	@isTest static void getEntityListTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id);
		User delegatedApproverUser = ComTestDataUtility.createUser('approver', 'ja', 'ja_JP');
		ComEmpBase__c delegatedApprover = ComTestDataUtility.createEmployeeWithHistory('approver', company.Id, null, delegatedApproverUser.Id);

		DelegatedApproverSettingRepository repo = new DelegatedApproverSettingRepository();

		// 代理承認者設定データを作成
		DelegatedApproverSettingEntity newSetting = new DelegatedApproverSettingEntity();
		newSetting.name = 'test';
		newSetting.employeeBaseId = emp.Id;
		newSetting.delegatedApproverBaseId = delegatedApprover.Id;
		newSetting.canApproveExpenseRequestByDelegate = true;
		newSetting.canApproveExpenseReportByDelegate = true;
		repo.saveEntityList(new List<DelegatedApproverSettingEntity>{newSetting});

		Test.startTest();
		List<DelegatedApproverSettingEntity> settingList = repo.getEntityListByEmpId(emp.Id);
		Test.stopTest();

		System.assertEquals(1, settingList.size());
		DelegatedApproverSettingEntity setting = settingList[0];
		System.assertEquals(newSetting.name, setting.Name);
		System.assertEquals(newSetting.employeeBaseId, setting.employeeBaseId);
		System.assertEquals(newSetting.delegatedApproverBaseId, setting.delegatedApproverBaseId);
		System.assertEquals(newSetting.canApproveExpenseRequestByDelegate, setting.canApproveExpenseRequestByDelegate);
		System.assertEquals(newSetting.canApproveExpenseReportByDelegate, setting.canApproveExpenseReportByDelegate);
	}

	/**
	 * saveEntityをテストする
	 */
	@isTest static void SaveEntityTest() {
		ComCompany__c company = ComTestDataUtility.createTestCompany();
		User user = ComTestDataUtility.createUser('test', 'ja', 'ja_JP');
		ComEmpBase__c emp = ComTestDataUtility.createEmployeeWithHistory('test', company.Id, null, user.Id);
		User delegatedApproverUser = ComTestDataUtility.createUser('approver', 'ja', 'ja_JP');
		ComEmpBase__c delegatedApprover = ComTestDataUtility.createEmployeeWithHistory('approver', company.Id, null, delegatedApproverUser.Id);

		DelegatedApproverSettingEntity newSetting = new DelegatedApproverSettingEntity();
		newSetting.name = 'test';
		newSetting.employeeBaseId = emp.Id;
		newSetting.delegatedApproverBaseId = delegatedApprover.Id;
		newSetting.canApproveExpenseRequestByDelegate = true;
		newSetting.canApproveExpenseReportByDelegate = true;

		DelegatedApproverSettingRepository repo = new DelegatedApproverSettingRepository();
		Test.startTest();
		repo.saveEntity(newSetting);
		Test.stopTest();

		// 作成された代理承認者設定データを取得
		ComDelegatedApproverSetting__c setting = [
				SELECT
					Id,
					Name,
					EmployeeBaseId__c,
					DelegatedApproverBaseId__c,
					ApproveExpenseRequestByDelegate__c,
					ApproveExpenseReportByDelegate__c
				FROM ComDelegatedApproverSetting__c
				WHERE EmployeeBaseId__c = :emp.Id
				LIMIT 1];

		System.assertEquals(newSetting.name, setting.Name);
		System.assertEquals(newSetting.employeeBaseId, setting.EmployeeBaseId__c);
		System.assertEquals(newSetting.delegatedApproverBaseId, setting.DelegatedApproverBaseId__c);
		System.assertEquals(newSetting.canApproveExpenseRequestByDelegate, setting.ApproveExpenseRequestByDelegate__c);
		System.assertEquals(newSetting.canApproveExpenseReportByDelegate, setting.ApproveExpenseReportByDelegate__c);
	}
}