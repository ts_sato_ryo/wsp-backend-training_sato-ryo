/**
 * @author TeamSpirit Inc.
 * @date 2017
 *
 * @group 共通
 *
 * @description 社員レコード操作APIを実装するクラス
 */
public with sharing class EmployeeHistoryResource extends Repository.BaseRepository {

	/** メッセージ */
	private static final ComMsgBase MESSAGE = ComMessage.msg();

	/**
	 * @desctiprion 社員履歴レコードを表すクラス
	 */
	public class EmployeeHistory implements RemoteApi.RequestParam {
		/** 履歴レコードID */
		public String id;
		/** ベースレコードID */
		public String baseId;
		/** Cost Center Base ID */
		public String costCenterId;
		/** Cost Center Base */
		public LookupField costCenter;
		/** 部署ベースID */
		public String departmentId;
		/** 部署ベース ※取得専用 */
		public LookupField department;
		/** 役職 ※取得専用 */
		public String title;
		/** 役職(言語0) */
		public String title_L0;
		/** 役職(言語1) */
		public String title_L1;
		/** 役職(言語2) */
		public String title_L2;
		/** マネージャ (社員ベース) のレコードID */
		public String managerId;
		/** マネージャ (社員ベース)  */
		public LookupField manager;
		/** 履歴コメント */
		public String comment;
		/** 有効期間開始日 */
		public Date validDateFrom;
		/** 失効日 */
		public Date validDateTo;
		/** 勤務体系ID */
		public String workingTypeId;
		/** 勤務体系 */
		public LookupField workingType;
		/** 工数設定ID */
		public String timeSettingId;
		/** 工数設定 */
		public LookupField timeSetting;
		/** 36協定アラート設定ID */
		public String agreementAlertSettingId;
		/** 36協定アラート設定 */
		public LookupField agreementAlertSetting;
		/** カレンダーID */
		public String calendarId;
		/** 権限ID */
		public String permissionId;
		/** 承認者01ID */
		public ID approver01Id;
		/** 承認者権限01フラグ */
		public Boolean approvalAuthority01;

		/** 通勤定期券有 */
		public Boolean commuterPassAvailable;
		/** ジョルダン経路 */
		public ExpTransitJorudanService.CommuterRouteDetail jorudanRoute;
		/** Expense Employee Group */
		public String expEmployeeGroupId;
		/** Expense Employee Group Name */
		public LookupField expEmployeeGroup;

		/**
		 * エンティティを作成する
		 */
		public EmployeeHistoryEntity createEntity(Map<String, Object> paramMap, Boolean isUpdate) {

			// 更新時にIDがブランクの場合はエラー
			if (isUpdate && String.isBlank(this.id)) {
				throw new App.ParameterException('パラメータ "id" を指定して下さい');
			}

			EmployeeHistoryEntity entity = new EmployeeHistoryEntity();

			// レコード更新時はIDをセット
			if(isUpdate){
				entity.setId(this.id);
			}
			// ベースIDは更新不可のため、新規作成時のみセット
			if(!isUpdate && paramMap.containsKey('baseId')){
				entity.baseId = this.baseId;
			}
			if(paramMap.containsKey('costCenterId')){
				entity.costCenterId = this.costCenterId;
			} else {
				// The only time this will happen is when the useExpense is set to false.
				// In that case, explicitly set it to null to avoid cost-center propagation in emp history.
				entity.costCenterId = null;
			}
			if(paramMap.containsKey('departmentId')){
				entity.departmentId = this.departmentId;
			}
			if(paramMap.containsKey('title_L0')){
				entity.titleL0 = this.title_L0;
			}
			if(paramMap.containsKey('title_L1')){
				entity.titleL1 = this.title_L1;
			}
			if(paramMap.containsKey('title_L2')){
				entity.titleL2 = this.title_L2;
			}
			if(paramMap.containsKey('managerId')){
				entity.managerId = this.managerId;
			}
			if(paramMap.containsKey('validDateFrom')){
				entity.validFrom = AppDate.valueOf(this.validDateFrom);
			}
			if (entity.validFrom == null) {
				// デフォルトは実行日
				entity.validFrom = AppDate.today();
			}
			if(paramMap.containsKey('validDateTo')){
				entity.validTo = AppDate.valueOf(this.validDateTo);
			}
			if (entity.validTo == null) {
				// デフォルト値は失効日の最大日付
				entity.validTo = ParentChildHistoryEntity.VALID_TO_MAX;
			}
			if(paramMap.containsKey('comment')){
				entity.historyComment = this.comment;
			}
			if(paramMap.containsKey('workingTypeId')){
				entity.workingTypeId = this.workingTypeId;
			}
			if(paramMap.containsKey('timeSettingId')){
				entity.timeSettingId = this.timeSettingId;
			}
			if(paramMap.containsKey('agreementAlertSettingId')){
				entity.agreementAlertSettingId = this.agreementAlertSettingId;
			}
			if(paramMap.containsKey('calendarId')){
				entity.calendarId = this.calendarId;
			}
			if (paramMap.containsKey('permissionId')) {
				entity.permissionId = this.permissionId;
			}
			if (paramMap.containskey('approvalAuthority01')) {
				entity.approvalAuthority01 = this.approvalAuthority01;
			}
			if (paramMap.containskey('approver01Id')) {
				entity.approverBase01Id = this.approver01Id;
			}

			if (paramMap.containsKey('commuterPassAvailable')) {
				entity.commuterPassAvailable = this.commuterPassAvailable;
			}
			if (paramMap.containsKey('jorudanRoute')) {
				entity.jorudanRoute = this.jorudanRoute == null ? null : JSON.serialize(this.jorudanRoute);
			}

			if (paramMap.containsKey('expEmployeeGroupId')) {
				entity.expEmployeeGroupId = this.expEmployeeGroupId;
			}

			return entity;
		}
	}

	/**
	 * @desctiprion ルックアップ項目を表すクラス
	 */
	public class LookupField {
		public String name;
	}

	/**
	 * @description 社員履歴レコード作成結果レスポンス
	 */
	public virtual class SaveResult implements RemoteApi.ResponseParam {
		/** 作成したレコードID */
		public String id;
	}

	/**
	 * @description 社員履歴レコード作成APIを実装するクラス
	 */
	public with sharing class CreateApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EMPLOYEE;

		/**
		 * @description 社員履歴レコードを1件作成する
		 * @param  req リクエストパラメータ(EmployeeHistory)
		 * @return レスポンス(SaveResult)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			//------------------------------------------------------------------------
			// 【仮仕様】
			// 現在指定可能な改定日(有効開始日)は末尾の履歴の有効開始日以降とする。
			// 改定機能自体は実装済みでマスタ全体の有効期間内の日付で改定できるようになっているが
			// UI側が対応できていないため。
			//------------------------------------------------------------------------

			final Boolean isUpdate = false;
			EmployeeService service = new EmployeeService();
			EmployeeRepository repo = new EmployeeRepository();

			// パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			EmployeeHistory param = (EmployeeHistory)req.getParam(EmployeeHistoryResource.EmployeeHistory.class);
			EmployeeHistoryEntity newHistory = param.createEntity(req.getParamMap(), isUpdate);

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			// 末尾の履歴(有効開始日が最新の履歴)を取得する
			EmployeeBaseEntity base = repo.getEntity(newHistory.baseId);
			if (base == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Employee}));
			}

			// 勤務体系チェック（勤怠利用時は勤務体系を必須とする）
			CompanyEntity company = new CompanyRepository().getEntity(base.companyId);
			if (company == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_SpecifiedNotFound(new List<String>{MESSAGE.Com_Lbl_Company}));
			}
			if (company.useAttendance && newHistory.workingTypeId == null) {
				throw new App.ParameterException(MESSAGE.Com_Err_NotSetValue(new List<String>{MESSAGE.Att_Lbl_WorkingType}));
			}

			if (company.useExpense) {
				ExpCommonUtil.validateId('expEmployeeGroupId', newHistory.expEmployeeGroupId, true);
			}

			EmployeeHistoryEntity latestHistory = (EmployeeHistoryEntity)base.getLatestSuperHistory();

			// 追加の履歴の有効開始日が末尾の履歴の有効開始日以降であることを確認する
			if (newHistory.validFrom.getDate() < latestHistory.validFrom.getDate()) {
				// メッセージ：改定日は最新の履歴の有効開始日以降を指定してください
				throw new App.ParameterException(MESSAGE.Admin_Err_InvalidRevisionDate);
			}

			// 改定対象が最新履歴の場合は、マスタ全体の失効日(末尾の履歴の失効日)を更新する
			if (newHistory.validTo.getDate() != latestHistory.validTo.getDate()) {
				service.updateBaseValidToDate(newHistory.baseId, newHistory.validTo);
			}

			// 履歴を改定する
			service.reviseHistoryWithValidation(base, newHistory);

			// 改定日に有効な履歴のIDを取得する
			EmployeeBaseEntity revisedBase = repo.getEntity(newHistory.baseId, newHistory.validFrom);
			SaveResult res = new SaveResult();
			res.Id = revisedBase.getHistoryList().get(0).id;
			return res;
		}
	}

	/**
	 * @desctiprion 社員レコード更新APIを実装するクラス
	 */
	public with sharing class UpdateApi extends RemoteApi.ResourceBase {

		/**
		 * @description 社員レコードを1件更新する
		 * @param  req リクエストパラメータ(Department)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {
			// 一旦廃止します。必要になったら実装してください。
			throw new App.UnsupportedException('社員履歴更新APIは利用できません。');

			// // パラメータをMapからリクエストパラメータ格納オブジェクトに変換する
			// EmployeeHistory param = (EmployeeHistory)req.getParam(EmployeeHistory.class);
			// EmployeeHistoryResource.saveRecord(param, req.getParamMap(), true);
		}
	}

	/**
	 * @description 社員レコードの削除パラメータを定義するクラス
	 */
	public class DeleteOption implements RemoteApi.RequestParam {
		/** 削除対象レコードID */
		public String id;

		/**
		 * パラメータを検証する
		 */
		public void validate() {

			// ID
			if (String.isBlank(this.id)) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
			try {
				System.Id.valueOf(this.Id);
			} catch (Exception e) {
				throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
			}
		}
	}

	/**
	 * 社員レコード削除APIを実装するクラス
	 */
	public with sharing class DeleteApi extends RemoteApi.ResourceBase {

		/** API実行に必要な権限 */
		private final ComResourcePermission.Permission requriedPermission =
				ComResourcePermission.Permission.MANAGE_EMPLOYEE;

		/**
		 * @description 社員履歴レコードを1件削除する(論理削除)
		 * @param  req リクエストパラメータ(DeleteOption)
		 * @return レスポンス(null)
		 */
		public override RemoteApi.ResponseParam execute(RemoteApi.Request req) {

			DeleteOption param = (DeleteOption)req.getParam(DeleteOption.class);

			// パラメータを検証する
			param.validate();

			// 権限チェック
			new ComResourcePermission().hasExecutePermission(requriedPermission);

			try {
				// レコードを削除
				new EmployeeService().deleteHistoryListWithPropagation(new List<Id>{param.id});
			} catch (App.RecordNotFoundException e) {
				// すでに削除済みの場合は成功レスポンスを返す
			}

			// 成功レスポンスをセットする
			return null;
		}
	}

	/**
	 * @description 社員レコード検索結果を格納するクラス
	 */
	public class SearchResult implements RemoteApi.ResponseParam {
		/** レコード検索結果 */
		public EmployeeHistory[] records;
	}

	/**
	 * @description 社員履歴レコード検索条件を格納するクラス
	 */
	public class SearchCondition implements RemoteApi.RequestParam {
		/** 社員履歴レコードID */
		public String id;
		/** 関連する社員ベースレコードID */
		public String baseId;

		/**
		 * パラメータ値を検証する
		 */
		public void validate() {

			// 履歴ID
			if (String.isNotBlank(this.id)) {
				try {
					System.Id.valueOf(this.id);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'id'}));
				}
			}

			// ベースID
			if (String.isNotBlank(this.baseId)) {
				try {
					System.Id.valueOf(this.baseId);
				} catch (Exception e) {
					throw new App.ParameterException(MESSAGE.Com_Err_InvalidValue(new List<String>{'baseId'}));
				}
			}
		}
	}

	/**
	 * @description 社員履歴レコード検索APIを実装するクラス
	 */
	public with sharing class SearchApi extends RemoteApi.ResourceBase {

		/**
		 * 社員履歴レコードを検索する
		 * @param  parameter リクエストパラメータを格納したオブジェクト
		 * @return レスポンスパラメータを格納したオブジェクト
		 */
		public override RemoteApi.ResponseParam execute (RemoteApi.Request req) {

			SearchCondition param = (SearchCondition)req.getParam(SearchCondition.class);

			// パラメータのバリデーション
			param.validate();

			EmployeeRepository.SearchFilter filter = new EmployeeRepository.SearchFilter();
			filter.historyIds = String.isBlank(param.id) ? null : new Set<Id>{param.id};
			filter.baseIds = String.isBlank(param.baseId) ? null : new Set<Id>{param.baseId};
			List<EmployeeBaseEntity> baseList =
					(new EmployeeRepository()).searchEntityWithHistory(filter, false, EmployeeRepository.HistorySortOrder.VALID_FROM_DESC);

			// 参照先のマスタの情報を取得
			Set<Id> costCenterIds = new Set<Id>();
			Set<Id> departmentIds = new Set<Id>();
			Set<Id> managerIds = new Set<Id>();
			Set<Id> workingTypeIds = new Set<Id>();
			Set<Id> timeSettingIds = new Set<Id>();
			for (EmployeeBaseEntity base : baseList) {
				for (EmployeeHistoryEntity history : base.getHistoryList()) {
					if (history.costCenterId != null) {
						costCenterIds.add(history.costCenterId);
					}
					if (history.departmentId != null) {
						departmentIds.add(history.departmentId);
					}
					if (history.managerId != null) {
						managerIds.add(history.managerId);
					}
					if (history.workingTypeId != null) {
						workingTypeIds.add(history.workingTypeId);
					}
					if (history.timeSettingId != null) {
						timeSettingIds.add(history.timeSettingId);
					}
				}
			}
			Map<Id, CostCenterBaseEntity> costCenterMap = (new CostCenterService()).getBaseMap(new List<Id>(costCenterIds));
			Map<Id, DepartmentBaseEntity> deptMap = (new DepartmentService()).getBaseMap(new List<Id>(departmentIds));
			Map<Id, AttWorkingTypeBaseEntity> workingTypeMap = (new AttWorkingTypeService()).getBaseMap(new List<Id>(workingTypeIds));
			Map<Id, TimeSettingBaseEntity> timeSettingMap = (new TimeSettingService()).getBaseMap(new List<Id>(timeSettingIds));

			// エンティティをレスポンスオブジェクトに変換
			List<EmployeeHistory> resList = new List<EmployeeHistory>();
			for(EmployeeBaseEntity base : baseList) {
				for (EmployeeHistoryEntity history : base.getHistoryList()) {
					EmployeeHistory res = new EmployeeHistory();
					res.id = history.id;
					res.baseId = history.baseId;
					res.costCenterId = history.costCenterId;
					res.costCenter = new LookupField();
					res.costCenter.name = getCostCenterName(costCenterMap, res.costCenterId, history.validFrom);
					res.departmentId = history.departmentId;
					res.department = new LookupField();
					res.department.name = getDeptName(deptMap, res.departmentId, history.validFrom);
					res.title = history.titleL.getValue();
					res.title_L0 = history.titleL0;
					res.title_L1 = history.titleL1;
					res.title_L2 = history.titleL2;
					res.calendarId = history.calendarId;
					res.permissionId = history.permissionId;
					res.approver01Id = history.approverBase01Id;
					res.approvalAuthority01 = history.approvalAuthority01;
					res.managerId = history.managerId;
					res.manager = new LookupField();
					res.manager.name = history.manager == null ? null : history.manager.fullNameL.getFullName();
					res.validDateFrom = AppConverter.dateValue(history.validFrom);
					res.validDateTo = AppConverter.dateValue(history.validTo);
					res.comment = history.historyComment;
					res.workingTypeId = history.workingTypeId;
					res.workingType = new LookupField();
					res.workingType.name = getWorkingTypeName(workingTypeMap, history.workingTypeId, history.validFrom);
					res.timeSettingId = history.timeSettingId;
					res.timeSetting = new LookupField();
					res.timeSetting.name = getTimeSettingName(timeSettingMap, history.timeSettingId, history.validFrom);
					res.expEmployeeGroupId = history.expEmployeeGroupId;
					res.expEmployeeGroup = new LookupField();
					res.expEmployeeGroup.name = history.expEmployeeGroup != null ? history.expEmployeeGroup.nameL.getValue() : null ;

					// 36協定アラート設定
					res.agreementAlertSettingId = history.agreementAlertSettingId;
					res.agreementAlertSetting = new LookupField();
					if (history.agreementAlertSetting != null) {
						res.agreementAlertSetting.name = history.agreementAlertSetting.nameL.getValue();
					}

					// 定期情報
					res.commuterPassAvailable = history.commuterPassAvailable;
					if (String.isNotBlank(history.jorudanRoute)) {
						res.jorudanRoute = (ExpTransitJorudanService.CommuterRouteDetail)JSON.deserialize(
								history.jorudanRoute, ExpTransitJorudanService.CommuterRouteDetail.class);
					}

					resList.add(res);
				}
			}

			// 成功レスポンスをセットする
			SearchResult res = new SearchResult();
			res.records = resList;
			return res;
		}

		/**
		 * Retrieve the Name of the Cost Center for the given targetDate.
		 * @param costCenterMap Cost Center Base map with Cost Center Base ID as the Key
		 * @param CostCenterBaseId Cost Center Base Record ID
		 * @param targetDate The Target Date for History Record
		 * @return 部署名。ただし、有効な履歴が存在しない場合はnullを返却
		 */
		private String getCostCenterName(Map<Id, CostCenterBaseEntity> costCenterMap, Id CostCenterBaseId, AppDate targetDate) {
			if (CostCenterBaseId == null) {
				return null;
			}

			if (! costCenterMap.containsKey(CostCenterBaseId)) {
				return null;
			}

			CostCenterBaseEntity costCenterBase = costCenterMap.get(CostCenterBaseId);
			CostCenterHistoryEntity history = (CostCenterHistoryEntity) costCenterBase.getSuperHistoryByDate(targetDate);
			if (history == null) {
				return null;
			}
			return history.nameL.getValue();
		}

		/**
		 * 指定した日の部署名を取得する。ユーザ言語で翻訳済み
		 * @param deptMap キーを部署ベースIDにした部署ベースのマップ
		 * @param deptBaseId 部署ベースID
		 * @param targetDate 取得対象日
		 * @return 部署名。ただし、有効な履歴が存在しない場合はnullを返却
		 */
		private String getDeptName(Map<Id, DepartmentBaseEntity> deptMap, Id deptBaseId, AppDate targetDate) {
			if (deptBaseId == null) {
				return null;
			}

			if (! deptMap.containsKey(deptBaseId)) {
				return null;
			}

			DepartmentBaseEntity deptBase = deptMap.get(deptBaseId);
			DepartmentHistoryEntity history = (DepartmentHistoryEntity)deptBase.getSuperHistoryByDate(targetDate);
			if (history == null) {
				return null;
			}
			return history.nameL.getValue();
		}

		/**
		 * 指定した日の勤務体系名を取得する。ユーザ言語で翻訳済み
		 * @param workingTypeMap キーを勤務体系ベースIDにした工数設定ベースのマップ
		 * @param workingTypeBaseId 勤務体系ベースID
		 * @param targetDate 取得対象日
		 * @return 部署名。ただし、有効な履歴が存在しない場合はnullを返却
		 */
		private String getWorkingTypeName(Map<Id, AttWorkingTypeBaseEntity> workingTypeMap, Id workingTypeBaseId, AppDate targetDate) {
			if (workingTypeBaseId == null) {
				return null;
			}

			if (! workingTypeMap.containsKey(workingTypeBaseId)) {
				return null;
			}

			AttWorkingTypeBaseEntity deptBase = workingTypeMap.get(workingTypeBaseId);
			AttWorkingTypeHistoryEntity history = (AttWorkingTypeHistoryEntity)deptBase.getSuperHistoryByDate(targetDate);
			if (history == null) {
				return null;
			}
			return history.nameL.getValue();
		}

		/**
		 * 指定した日の工数設定名を取得する。ユーザ言語で翻訳済み
		 * @param workingTypeMap キーを工数設定ベースIDにした工数設定ベースのマップ
		 * @param workingTypeBaseId 工数設定ベースID
		 * @param targetDate 取得対象日
		 * @return 部署名。ただし、有効な履歴が存在しない場合はnullを返却
		 */
		private String getTimeSettingName(Map<Id, TimeSettingBaseEntity> timeSettingMap,
				Id timeSettingBaseId, AppDate targetDate) {
			if (timeSettingBaseId == null) {
				return null;
			}

			if (! timeSettingMap.containsKey(timeSettingBaseId)) {
				return null;
			}

			TimeSettingBaseEntity deptBase = timeSettingMap.get(timeSettingBaseId);
			TimeSettingHistoryEntity history = (TimeSettingHistoryEntity)deptBase.getSuperHistoryByDate(targetDate);
			if (history == null) {
				return null;
			}
			return history.nameL.getValue();
		}
	}
}