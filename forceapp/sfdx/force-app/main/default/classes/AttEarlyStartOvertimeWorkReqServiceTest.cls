/**
 * @author TeamSpirit Inc.
 * @date 2018
 *
 * @group 勤怠
 *
 * AttEarlyStartOverTimeWorkReqServiceのテストクラス
 */
@isTest
private class AttEarlyStartOvertimeWorkReqServiceTest{


	/**
	 * submitのテスト
	 * 早朝勤務申請ができることを確認する
	 */
	@isTest static void submitTest() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Flex);
		EmployeeBaseEntity testEmp1 = testData.employee; // 実行ユーザの社員
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// テスト社員1の承認者01設定
		update new ComEmpHistory__c(
				Id = testEmp1.getHistory(0).id,
				ApproverBase01Id__c = testEmp2.id);

		// 勤務表データ作成
		AppDate startDate = AppDate.newInstance(2018, 6, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, false);

		// 早朝勤務申請を作成
		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp1.id;
		param.targetDate = AppDate.newInstance(2018, 6, 5);
		param.startTIme = AttTime.newInstance(6, 0);
		param.endTIme = AttTime.newInstance(7, 0);
		param.remarks = 'test備考';
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 申請実行
		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		service.submit(param);

		// 検証
		// 勤怠明細を確認する
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		List<AttRecordEntity> resultRecords = new AttSummaryRepository().searchRecordEntityList(testEmp1.Id, param.targetDate, null);
		System.assert(resultRecords[0].reqRequestingEarlyStartWorkRequestId != null);

		// 勤怠日次申請を確認する
		AttDailyRequestEntity resultEntity = new AttDailyRequestRepository().getEntity(resultRecords[0].reqRequestingEarlyStartWorkRequestId);
		System.assertEquals(AttRequestType.EARLY_START_WORK, resultEntity.requestType);
		System.assertEquals(AppRequestStatus.PENDING, resultEntity.status);
		System.assertNotEquals(null, resultEntity.requestTime);
		System.assertEquals(testEmp1.getHistory(0).id, resultEntity.employeeId);
		System.assertEquals(testData.dept.CurrenthistoryId__c, resultEntity.departmentId);
		System.assertEquals(testData.employee.getHistory(0).id, resultEntity.actorId);
		System.assertEquals(param.targetDate, resultEntity.startDate);
		System.assertEquals(param.targetDate, resultEntity.endDate);
		System.assertEquals(param.startTime, resultEntity.startTime);
		System.assertEquals(param.endTime, resultEntity.endTime);
		System.assertEquals(param.remarks, resultEntity.remarks);
		System.assertEquals('001 太郎1_L0さん 早朝勤務申請 20180605 06:00–07:00', resultEntity.name);
	}

	/**
	 * submitのテスト
	 * 早朝勤務申請バリデーションが正常に実行されることを確認する
	 */
	@isTest static void submitTestInvalidRecord() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp1 = testData.employee; // 実行ユーザの社員
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// テスト社員1の承認者01設定
		update new ComEmpHistory__c(
				Id = testEmp1.getHistory(0).id,
				ApproverBase01Id__c = testEmp2.id);

		// 勤務表データ作成
		AppDate startDate = AppDate.newInstance(2018, 6, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, false);

		// 早朝勤務申請を作成
		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp1.id;
		param.targetDate = AppDate.newInstance(2018, 6, 5);
		param.startTIme = AttTime.newInstance(8, 0);
		param.endTIme = AttTime.newInstance(9, 1);
		param.remarks = 'test備考';
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 申請実行
		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		try {
			service.submit(param);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Admin_Err_DateOver(new List<String>{ComMessage.msg().Att_Lbl_RequestTypeEarlyStartWorkEndTime, ComMessage.msg().Admin_Lbl_WorkingTypeStartTime}), e.getMessage());
		}
	}

	/**
	 * submitのテスト
	 * 申請対象日の勤怠明細に勤務パターンが適用されている場合、勤務パターンを参照することを確認する
	 * 早朝勤務申請の申請終了時刻が勤務パターンの始業時刻以前の場合、申請できることを確認する
	 */
	@isTest static void submitTestPatternSuccess() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp1 = testData.employee; // 実行ユーザの社員
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// テスト社員1の承認者01設定
		update new ComEmpHistory__c(
				Id = testEmp1.getHistory(0).id,
				ApproverBase01Id__c = testEmp2.id);

		// 勤務表データ作成
		AppDate startDate = AppDate.newInstance(2018, 6, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, false);

		// 始業時刻が11:00の勤務パターンを作成する
		AttPatternEntity patternEntity = testData.createPattern(AttWorkSystem.JP_FIX, 'testPattern');
		patternEntity.startTime = AttTime.newInstance(11, 0);
		patternEntity.endTime = AttTime.newInstance(18, 0);
		new AttPatternRepository2().saveEntity(patternEntity);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRecordEntity targetRecord = summaryRepo.searchRecordEntityList(testEmp1.id, startDate, startDate)[0];
		targetRecord.patternId = patternEntity.id;
		summaryRepo.saveRecordEntity(targetRecord);

		// 早朝勤務申請を作成
		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp1.id;
		param.targetDate = AppDate.newInstance(2018, 6, 1);
		param.startTIme = AttTime.newInstance(8, 0);
		param.endTIme = AttTime.newInstance(11, 0);
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 申請実行
		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		service.submit(param);
		// 検証
		// 勤怠明細を確認する
		List<AttRecordEntity> resultRecords = summaryRepo.searchRecordEntityList(testEmp1.Id, param.targetDate, null);
		System.assert(resultRecords[0].reqRequestingEarlyStartWorkRequestId != null);

		// 勤怠日次申請を確認する
		AttDailyRequestEntity resultEntity = new AttDailyRequestRepository().getEntity(resultRecords[0].reqRequestingEarlyStartWorkRequestId);
		System.assertEquals(AttRequestType.EARLY_START_WORK, resultEntity.requestType);
		System.assertEquals(AppRequestStatus.PENDING, resultEntity.status);
		System.assertEquals(param.targetDate, resultEntity.startDate);
		System.assertEquals(param.targetDate, resultEntity.endDate);
		System.assertEquals(param.startTime, resultEntity.startTime);
		System.assertEquals(param.endTime, resultEntity.endTime);
	}

	/**
	 * submitのテスト
	 * 申請対象日の勤怠明細に勤務パターンが適用されている場合、勤務パターンを参照することを確認する
	 * 早朝勤務申請の申請終了時刻が勤務パターンの始業時刻以降の場合、申請できないことを確認する
	 */
	@isTest static void submitTestPatternFailure() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp1 = testData.employee; // 実行ユーザの社員
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// テスト社員1の承認者01設定
		update new ComEmpHistory__c(
				Id = testEmp1.getHistory(0).id,
				ApproverBase01Id__c = testEmp2.id);

		// 勤務表データ作成
		AppDate startDate = AppDate.newInstance(2018, 6, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, false);

		// 始業時刻が11:00の勤務パターンを作成する
		AttPatternEntity patternEntity = testData.createPattern(AttWorkSystem.JP_FIX, 'testPattern');
		patternEntity.startTime = AttTime.newInstance(11, 0);
		patternEntity.endTime = AttTime.newInstance(18, 0);
		new AttPatternRepository2().saveEntity(patternEntity);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRecordEntity targetRecord = summaryRepo.searchRecordEntityList(testEmp1.id, startDate, startDate)[0];
		targetRecord.patternId = patternEntity.id;
		summaryRepo.saveRecordEntity(targetRecord);

		// 早朝勤務申請を作成
		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp1.id;
		param.targetDate = AppDate.newInstance(2018, 6, 1);
		param.startTIme = AttTime.newInstance(8, 0);
		param.endTIme = AttTime.newInstance(11, 1);
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 申請実行
		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		try {
			service.submit(param);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Admin_Err_DateOver(new List<String>{ComMessage.msg().Att_Lbl_RequestTypeEarlyStartWorkEndTime, ComMessage.msg().Admin_Lbl_WorkingTypeStartTime}), e.getMessage());
		}
	}

	/**
	 * submitのテスト
	 * 申請対象日の勤怠明細に申請中の勤務時間変更申請が存在する場合、申請中の勤務パターンを参照することを確認する
	 */
	@isTest static void submitTestRequestingPattern() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp1 = testData.employee; // 実行ユーザの社員
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// テスト社員1の承認者01設定
		update new ComEmpHistory__c(
				Id = testEmp1.getHistory(0).id,
				ApproverBase01Id__c = testEmp2.id);

		// 勤務表データ作成
		AppDate startDate = AppDate.newInstance(2018, 6, 1);
		testData.createAttSummaryWithRecordsMonth(testEmp1.getHistory(0).id, testData.workingType.getHistory(0).id, startDate, false);

		// 始業時刻が11:00の勤務パターンを作成する
		AttPatternEntity patternEntity = testData.createPattern(AttWorkSystem.JP_FIX, 'testPattern');
		patternEntity.startTime = AttTime.newInstance(11, 0);
		patternEntity.endTime = AttTime.newInstance(18, 0);
		Id patternId = new AttPatternRepository2().saveEntity(patternEntity).details[0].id;

		// 申請中の勤務時間変更申請を作成
		AttDailyRequestEntity patternRequest = testData.createPatternRequest(patternId, startDate, startDate);
		AttSummaryRepository summaryRepo = new AttSummaryRepository();
		AttRecordEntity targetRecord = summaryRepo.searchRecordEntityList(testEmp1.id, startDate, startDate)[0];
		targetRecord.reqRequestingPatternRequestId = patternRequest.id;
		summaryRepo.saveRecordEntity(targetRecord);

		// 早朝勤務申請を作成
		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp1.id;
		param.targetDate = AppDate.newInstance(2018, 6, 1);
		param.startTIme = AttTime.newInstance(8, 0);
		param.endTIme = AttTime.newInstance(11, 1);
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 申請実行
		Test.startTest();
		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		try {
			service.submit(param);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Admin_Err_DateOver(new List<String>{ComMessage.msg().Att_Lbl_RequestTypeEarlyStartWorkEndTime, ComMessage.msg().Admin_Lbl_WorkingTypeStartTime}), e.getMessage());
		}
		Test.stopTest();
	}

	/**
	 * submitのテスト
	 * 勤務体系で早朝勤務申請を使用するオプションがOffの場合、早朝勤務申請ができないことを検証する。
	 */
	@isTest static void submitUnuseEarlyStartWorkOption() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp = testData.employee;

		// 早朝勤務申請を使用しない
		AttWorkingTypeHistoryEntity history = testData.workingType.getHistory(0);
		history.useEarlyStartWorkApply = false;
		new AttWorkingTypeRepository().saveHistoryEntity(history);

		// 早朝勤務申請を作成
		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp.id;
		param.targetDate = AppDate.newInstance(2018, 6, 5);
		param.startTIme = AttTime.newInstance(8, 0);
		param.endTIme = AttTime.newInstance(9, 0);
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 申請実行
		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		try {
			service.submit(param);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Att_Err_NotPermittedEarlyStartWorkRequest, e.getMessage());
		}
	}

	/**
	 * submitのテスト
	 * 勤務体系で残業申請を使用するオプションがOffの場合、残業申請ができないことを検証する。
	 */
	@isTest static void submitUnuseOvertimeWorkOption() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp = testData.employee;

		// 残業申請を使用しない
		AttWorkingTypeHistoryEntity history = testData.workingType.getHistory(0);
		history.useOvertimeWorkApply = false;
		new AttWorkingTypeRepository().saveHistoryEntity(history);

		// 残業申請を作成
		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp.id;
		param.targetDate = AppDate.newInstance(2018, 6, 5);
		param.startTIme = AttTime.newInstance(18, 0);
		param.endTIme = AttTime.newInstance(20, 0);
		param.requestType = AttRequestType.OVERTIME_WORK;

		// 申請実行
		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		try {
			service.submit(param);
			System.assert(false);
		} catch (App.IllegalStateException e) {
			// 検証
			System.assertEquals(ComMessage.msg().Att_Err_NotPermittedOvertimeWorkRequest, e.getMessage());
		}
	}

	/**
	 * createRequestNameのテスト
	 * 申請名が正しく作成できることを確認する
	 */
	@isTest static void createRequestNameTest() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		EmployeeBaseEntity testEmp = testData.employee;

		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();

		param.targetDate = AppDate.newInstance(2018, 6, 5);
		param.startTime = AttTime.newInstance(8, 30);
		param.endTime = AttTime.newInstance(9, 0);

		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();
		String requestName, employeeName;
		AppLanguage lang;

		// 早朝勤務申請
		// 日本語の場合
		param.requestType = AttRequestType.EARLY_START_WORK;
		lang = AppLanguage.JA;
		employeeName = testEmp.displayNameL.getValue(lang);
		// 実行
		requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		System.assertEquals(employeeName + 'さん 早朝勤務申請 20180605 08:30–09:00', requestName);

		// 英語の場合
		lang = AppLanguage.EN_US;
		employeeName = testEmp.displayNameL.getValue(lang);
		// 実行
		requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		System.assertEquals(employeeName + ' Early Start Work Request 20180605 08:30–09:00', requestName);

		// 残業申請
		// 日本語の場合
		param.requestType = AttRequestType.OVERTIME_WORK;
		lang = AppLanguage.JA;
		employeeName = testEmp.displayNameL.getValue(lang);
		// 実行
		requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		System.assertEquals(employeeName + 'さん 残業申請 20180605 08:30–09:00', requestName);

		// 英語の場合
		lang = AppLanguage.EN_US;
		employeeName = testEmp.displayNameL.getValue(lang);
		// 実行
		requestName = service.createRequestName(param, testEmp, lang);
		// 検証
		System.assertEquals(employeeName + ' Overtime Work Request 20180605 08:30–09:00', requestName);

	}

	/**
	 * createCancelMailMessageのテスト
	 * 承認取消メールが正常に作成できることを確認する
	 */
	@isTest static void createCancelMailMessageTest() {

		AttEarlyStartOvertimeWorkReqService service = new AttEarlyStartOvertimeWorkReqService();

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Flex);
		EmployeeBaseEntity testEmp1 = testData.employee;
		EmployeeBaseEntity testEmp2 = testData.createTestEmployees(1)[0];

		// 早朝勤務申請のテスト
		AttDailyRequestEntity request1 = testData.createEarlyStartWorkOrOvertimeRequest(
				AttRequestType.EARLY_START_WORK, AppDate.newInstance(2018, 10, 1), AttTime.newInstance(7, 0), AttTime.newInstance(9, 0));
		request1.requestTime = AppDatetime.valueOf(Datetime.newInstance(2018, 10, 1));
		// 実行
		MailService.MailParam result1 = service.createCancelMailMessage(request1, testEmp1, testEmp2, AppLanguage.JA);
		// 検証
		// 送信者名
		System.assertEquals('adminTest_L0 太郎_L0', result1.senderDisplayName);
		// メールタイトル
		System.assertEquals('001_L0 太郎_L0さんの早朝勤務の承認が取消されました', result1.subject);
		// メール本文
		System.assertEquals('001_L0 太郎_L0さんの早朝勤務の承認がadminTest_L0 太郎_L0さんによって取消されました'
				+ '\r\n\r\n'
				+ '【申請内容】\r\n'
				+ '申請日付：2018/10/01\r\n'
				+ '申請種別：早朝勤務\r\n'
				+ '期間：2018/10/01 07:00－09:00\r\n'
				+ '備考：備考\r\n'
				+ '\r\n---\r\n'
				+ 'チームスピリット', result1.body);

		// 残業申請のテスト
		AttDailyRequestEntity request2 = testData.createEarlyStartWorkOrOvertimeRequest(
				AttRequestType.OVERTIME_WORK, AppDate.newInstance(2018, 10, 1), AttTime.newInstance(18, 15), AttTime.newInstance(20, 30));
		request2.requestTime = AppDatetime.valueOf(Datetime.newInstance(2018, 10, 1));
		// 実行
		MailService.MailParam result2 = service.createCancelMailMessage(request2, testEmp1, testEmp2, AppLanguage.JA);
		// 検証
		// メールタイトル
		System.assertEquals('001_L0 太郎_L0さんの残業の承認が取消されました', result2.subject);
		// メール本文
		System.assertEquals('001_L0 太郎_L0さんの残業の承認がadminTest_L0 太郎_L0さんによって取消されました'
				+ '\r\n\r\n'
				+ '【申請内容】\r\n'
				+ '申請日付：2018/10/01\r\n'
				+ '申請種別：残業\r\n'
				+ '期間：2018/10/01 18:15－20:30\r\n'
				+ '備考：備考\r\n'
				+ '\r\n---\r\n'
				+ 'チームスピリット', result2.body);
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が無効の場合、アプリ例外が発生することを確認する
	 */
	@isTest
	static void selfApprovePermissionTestFailure() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 各種勤怠申請自己承認(本人)の権限を無効に設定する
		testData.permission.isApproveSelfAttDailyRequestByEmployee = false;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp1.id;
		param.targetDate = AppDate.newInstance(2018, 6, 5);
		param.startTIme = AttTime.newInstance(6, 0);
		param.endTIme = AttTime.newInstance(7, 0);
		param.remarks = 'test備考';
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 実行
		System.runAs(AttTestData.getUser(testEmp1.userId)) {
			App.NoPermissionException actEx;
			try {
				AttDailyRequestEntity request = new AttEarlyStartOvertimeWorkReqService().submit(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertNotEquals(null, actEx, 'App.NoPermissionExceptionが発生しませんでした');
			String expectMessage = ComMessage.msg().Att_Err_NotAllowSelfAttDailyRequest(new List<String>{testEmp1.fullNameL.getFullName()});
			System.assertEquals(expectMessage, actEx.getMessage());
		}
	}

	/**
	 * 自己承認権限のテスト
	 * 承認者01と申請者の社員が同一で自己承認権限が有効の場合、アプリ例外が発生しないことを確認する
	 */
	@isTest
	static void selfApprovePermissionTestSuccess() {

		// テストデータ作成
		AttTestData testData = new AttTestData(AttWorkSystem.JP_Fix);
		// 各種勤怠申請自己承認(本人)の権限を有効に設定する
		testData.permission.isApproveSelfAttDailyRequestByEmployee = true;
		new PermissionRepository().saveEntity(testData.permission);

		// 標準ユーザの社員を作成
		EmployeeBaseEntity testEmp1 = testData.createTestEmployeesWithStandardProf(1)[0];
		// 承認者01を申請者と同一の社員に設定する
		testEmp1.getHistory(0).approverBase01Id = testEmp1.id;
		new EmployeeRepository().saveHistoryEntity(testEmp1.getHistory(0));

		AttEarlyStartOvertimeWorkReqService.SubmitParam param = new AttEarlyStartOvertimeWorkReqService.SubmitParam();
		param.empId = testEmp1.id;
		param.targetDate = AppDate.newInstance(2018, 6, 5);
		param.startTIme = AttTime.newInstance(6, 0);
		param.endTIme = AttTime.newInstance(7, 0);
		param.remarks = 'test備考';
		param.requestType = AttRequestType.EARLY_START_WORK;

		// 実行
		System.runAs(AttTestData.getUser(testEmp1.userId)) {
			App.NoPermissionException actEx;
			try {
				AttDailyRequestEntity request = new AttEarlyStartOvertimeWorkReqService().submit(param);
			} catch (App.NoPermissionException e) {
				actEx = e;
			}
			// 検証
			System.assertEquals(null, actEx);
		}
	}
}