/*
 * @author TeamSpirit Inc.
 * 
 * @date 2018
 * 
 * @description Service class for Exp Journal
 * 
 * @group Expense
 */
public with sharing class ExpJournalService {

	/*
	 * Update the Extracted Date of the Journals
	 * Validation Criteria:
	 *   - Specified Journal Number Exists
	 *   - Extract Date is same or later than the ExportedTime (doesn't make sense to extract before export)
	 *
	 *   @param journalNos Set of journal Numbers
	 *   @param extractDate Date which the Journal data has been extracted from the system
	 *
	 *   @return List<String> list of Journal Numbers which has been updated
	 */
	public List<String> updateExtractedDate(Set<String> journalNos, AppDate extractDate) {
		if (journalNos == null || journalNos.isEmpty()) {
			return null;
		}
		if (extractDate == null) {
			throw new App.ParameterException('extractDate', extractDate);
		}
		ExpJournalRepository repo = new ExpJournalRepository();
		ExpJournalRepository.SearchFilter searchFilter = new ExpJournalRepository.SearchFilter();
		searchFilter.journalNos = journalNos;
		List<ExpJournalEntity> journalEntityList = repo.searchEntity(searchFilter, false);
		if (journalNos.size() != journalEntityList.size()) {
			throw new App.RecordNotFoundException(ComMessage.msg().Exp_Err_NotFoundRecord);
		}
		for (ExpJournalEntity journalEntity : journalEntityList) {
			// It's not possible to extract the Journal before it is Exported to the Journal
			if (journalEntity.exportedTime.getDate() > extractDate.getDate()) {
				throw new App.IllegalStateException(App.ERR_CODE_INVALID_VALUE,
					ComMessage.msg().Com_Err_InvalidValueLater(new List<String>{
						ComMessage.msg().Exp_Lbl_ExtractDate,
						ComMessage.msg().Exp_Lbl_ExportedTime
					})
				);
			}
			journalEntity.extractDate = extractDate;
		}
		repo.saveEntityList(journalEntityList, true);
		return new List<String>(journalNos);
	}

	/*
	 * Update the Payment Date of the Journals
	 * Validation Criteria:
	 *   - Specified Report ID Exists
	 *   - If for updating existing Payment Date:
	 *      - Payment Date is already set previously in the Journal (Payment DATE is NOT NULL)
	 *   - If for setting Payment Date for the first time:
	 *      - Payment Date MUST be null
	 *   - Payment Date is same or later than the Authorized Time (doesn't make sense to pay before Authorized)
	 *
	 *   @param reports List of ExpReport contains the reportId and PaymentDate
	 *   @param isCreate whether this operation is for updating existing Payment Date or for setting for the first time.
	 *
	 *   @return List<Id> list of ReportIds whose Journal's payment date has been changed
	 */
	public List<Id> updatePaymentDate(List<ExpJournalResource.ExpReport> reports, Boolean isCreate) {
		if (reports == null || reports.isEmpty()) {
			return null;
		}
		ExpJournalRepository repo = new ExpJournalRepository();
		ExpJournalRepository.SearchFilter searchFilter = new ExpJournalRepository.SearchFilter();
		Map<Id, AppDate> paymentDateMap = new Map<Id, AppDate>();
		for (ExpJournalResource.ExpReport report : reports) {
			paymentDateMap.put(report.reportId, AppDate.valueOf(report.paymentDate));
		}

		if (reports.size() != paymentDateMap.keySet().size()) {
			// Duplicate Reports are not supported as it might payment date data inconsistency
			throw new App.ParameterException(ComMessage.msg().Com_Err_DuplicateValue(new List<String>{'reportId'}));
		}

		/** Retrieve ExpJournal entities with the report IDs */ 
		searchFilter.reportIds = paymentDateMap.keySet();
		List<ExpJournalEntity> journalEntityList = repo.searchEntity(searchFilter, false);
		
		/** Retrieve ExpReportRequest entities with the report IDs */
		ExpReportRequestRepository expReportReqRepo = new ExpReportRequestRepository();
		List<Id> reportIds = new List<Id>();
		reportIds.addAll(paymentDateMap.keySet());
		List<ExpReportRequestEntity> reportReqEntityList = expReportReqRepo.getEntityListFromReportId(reportIds);

		/** Update Payment Date in ExpJournal */
		Set<Id> processedReportIds = new Set<Id>();
		// If isCreate is True and the Payment Date already exists, throws exception
		for (ExpJournalEntity journalEntity : journalEntityList) {
			AppDate newPaymentDate = paymentDateMap.get(journalEntity.expReportId);

			verifyPaymentDate(journalEntity, newPaymentDate, isCreate);

			journalEntity.paymentDate = newPaymentDate;
			processedReportIds.add(journalEntity.expReportId);
		}

		if (reports.size() != processedReportIds.size()) {
			throw new App.RecordNotFoundException(ComMessage.msg().Exp_Err_NotFoundRecord);
		}
		
		/** Update Payment Date and Accounting Status values in ExpReportRequest */
		processedReportIds = new Set<Id>();
		// If isCreate is True and the Payment Date already exists, throws exception
		for (ExpReportRequestEntity entity : reportReqEntityList) {
			AppDate newPaymentDate = paymentDateMap.get(entity.expReportId);

			verifyPaymentDate(entity, newPaymentDate, isCreate);

			entity.paymentDate = newPaymentDate;
			entity.accountingStatus = ExpAccountingStatus.FULLY_PAID;
			processedReportIds.add(entity.expReportId);
		}

		if (reports.size() != processedReportIds.size()) {
			throw new App.RecordNotFoundException(ComMessage.msg().Exp_Err_NotFoundRecord);
		}
		
		repo.saveEntityList(journalEntityList, true);
		expReportReqRepo.saveEntityList(reportReqEntityList, true);
		
		return new List<Id>(paymentDateMap.keySet());
	}

	/*
	 * Verify new Payment Date against the Journal Record
	 * Validation Criteria:
	 *   - If for updating existing Payment Date:
	 *      - Payment Date is already set previously in the Journal (Payment DATE is NOT NULL)
	 *   - If for setting Payment Date for the first time:
	 *      - Payment Date MUST be null
	 *   - Payment Date is same or later than the Authorized Time (doesn't make sense to pay before Authorized)
	 *
	 *   @param journalEntity Journal Entity to validate the Payment Date against
	 *   @param newPaymentDate New PaymentDate to set to the Journal Record
	 *   @param isCreate if True, then there should NOT be existng Payment Date. If False, there MUSt be existing payment date.
	 *
	 *   @throws App.IllegalStateException
	 */
	private void verifyPaymentDate(ExpJournalEntity journalEntity, AppDate newPaymentDate, Boolean isCreate) {
		if (isCreate && journalEntity.paymentDate != null) {
			//If it's create, then paymentDate must NOT exists => throw exception
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_AlreadySetPaymentDate);
		} else if (isCreate != true && journalEntity.paymentDate == null) {
			//If it's NOT create, then paymentDate must exists => throw exception
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_NoPreviousPaymentDate);
		}

		if (journalEntity.authorizedTime.getDate() > newPaymentDate.getDate()) {
			// Payment date shouldn't be before the Authorized Time
			throw new App.IllegalStateException(App.ERR_CODE_INVALID_VALUE,
					ComMessage.msg().Com_Err_InvalidValueLater(new List<String>{
							ComMessage.msg().Exp_Lbl_PaymentDate,
							ComMessage.msg().Exp_Lbl_AuthorizedTime
							})
			);
		}
	}
	
	/*
	 * Verify new Payment Date against the ExpReportRequest Record
	 * Validation Criteria:
	 *   - If for updating existing Payment Date:
	 *      - Payment Date is already set previously in the ExpReportRequest (Payment DATE is NOT NULL)
	 *   - If for setting Payment Date for the first time:
	 *      - Payment Date MUST be null
	 *   - Payment Date is same or later than the Authorized Time (doesn't make sense to pay before Authorized)
	 *
	 *   @param entity ExpReportRequest Entity to validate the Payment Date against
	 *   @param newPaymentDate New PaymentDate to set to the ExpReportRequest Record
	 *   @param isCreate if True, then there should NOT be existng Payment Date. If False, there MUSt be existing payment date.
	 *
	 *   @throws App.IllegalStateException
	 */
	private void verifyPaymentDate(ExpReportRequestEntity entity, AppDate newPaymentDate, Boolean isCreate) {
		if (isCreate && entity.paymentDate != null) {
			//If it's create, then paymentDate must NOT exists => throw exception
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_AlreadySetPaymentDate);
		} else if (isCreate != true && entity.paymentDate == null) {
			//If it's NOT create, then paymentDate must exists => throw exception
			throw new App.IllegalStateException(ComMessage.msg().Exp_Err_NoPreviousPaymentDate);
		}

		if (entity.authorizedTime.getDate() > newPaymentDate.getDate()) {
			// Payment date shouldn't be before the Authorized Time
			throw new App.IllegalStateException(App.ERR_CODE_INVALID_VALUE,
					ComMessage.msg().Com_Err_InvalidValueLater(new List<String>{
							ComMessage.msg().Exp_Lbl_PaymentDate,
							ComMessage.msg().Exp_Lbl_AuthorizedTime
							})
			);
		}
	}

	/*
	 * Export the confirmed by Accounting clerk but not yet exported to Journal Reports to the Journal object {@code ExpJournal__c}
	 * and also update the corresponding {@code ExpReportRequest} object's {@code ExportedTime__c} field.
	 *
	 * Required Precondition
	 * 1. ExpReport has been authorized by the Accounting Clerk (check via {@code AuthorizedTime__c}
	 *      since the AuthorizedTime will only be set if it has been Authorized)
	 * 2. There isn't an Exported Time {@code ExportedTime__c} set in the ExpReportRequest yet (it should only be exported once to the journal)
	 *
	 * @param confirmedExpReportIds IDs of Expense Reports for exporting to Journal and for updating the Exported Time
	 *
	 * @throws IllegalStateException if the above specified requirement are not met
	 * @throws RecordNotFoundException if any of the specified reports are not found
	 */
	public void exportConfirmedReportsToJournal(List<Id> confirmedExpReportIds) {
		// If it's empty, don't need to process anything.
		if (confirmedExpReportIds.isEmpty()) {
			return;
		}

		ExpReportRequestRepository reportRequestRepo = new ExpReportRequestRepository();

		ExpJournalRepository journalRepo = new ExpJournalRepository();

		// 1. Retrieve all the Reports to be exported to Journal
		List<ExpReportEntity> reportList = new ExpReportRepository().getEntityList(confirmedExpReportIds);

		// 2. Verifies that the total number of Reports retrieved is the same as total number of confirmed Report IDs
		if (reportList.size() != confirmedExpReportIds.size()) {
			throw new App.RecordNotFoundException(ComMessage.msg().Exp_Err_NotFoundRecord);
		}

		// 3. Retrieve and set the Record and Record Item to each Report
		setRecordsAndRecordItems(reportList);

		// 4. Retrieve the ExpReportRequest which are associated with confirmed Reports
		List<ExpReportRequestEntity> reportRequestList = reportRequestRepo.getEntityListFromReportId(confirmedExpReportIds);

		// 5. Retrieve the necessary data for the Journal which are used by the confirmed Reports
		//  - Instead of retrieving each one, collect all the data which are used by the Reports and Requests
		//    because in most cases, the reports are likely to be using the same "Master" data (not much memory usage),
		//    and multiple DB query can be avoided this way.
		Map<Id, ExpReportRequestEntity> reportIdReportRequestMap = buildReportRequestEntityMap(reportRequestList);
		Map<Id, ExpRequestEntity> expRequestMap = buildExpRequestMap(reportList);
		Map<Id, ExpRequestApprovalEntity> requestApprovalMap = buildRequestApprovalMap(expRequestMap.keySet());
		Map<Id, CompanyEntity> companyMap = buildCompaniesMap(reportList);
		Map<Id, DepartmentBaseEntity> departmentHistoryIdBaseMap = buildDepartmentsMap(reportList, expRequestMap.values());
		Map<Id, ExpTypeEntity> expTypeMap = buildExpTypesMap(reportList);

		List<ExpJournalEntity> journalList = new List<ExpJournalEntity>();

		// 6. Retrieve the Journal Number to use
		//  - Instead of retrieving for each Journal, it's retrieve once, and then only get increased in this method.
		//    The Journal Number is persisted for all Journals. As this is intended to be use by batch, race conditions
		//    shouldn't happens - no risk or Journal Number being duplicated.
		Decimal journalNo = journalRepo.getNewJournalNo();
		Integer recordNo;
		Integer recordItemNo;

		// 7. Each Journal entry is created at the most detail level - meaning ExpRecordItem
		for (ExpReportEntity report : reportList) {
			recordNo = 1; // When it is a new report recordNo is set back
			for (ExpRecordEntity record : report.recordList) {
				// Record Items are order by Order Asc. Hence, Item [0] will always be `order = 0` and can be taken as record remarks.
				// Only itemized records will have the data on the RecordItemRemark field
				record.remarks = record.recordItemList[0].remarks;
				record.recordItemList[0].remarks = null;
				Boolean isHotelFeeRecord = record.recordType.equals(ExpRecordType.HOTEL_FEE);
				recordItemNo = 1;
				for (ExpRecordItemEntity recordItem : record.recordItemList) {
					// If it's a Hotel Fee record, don't export the first item.
					if (isHotelFeeRecord && (recordItem.order == 0)) {
						continue;
					}
					// Retrieve the data used the Report, Record and RecordItem from the Maps
					ExpReportRequestEntity expReportRequest = reportIdReportRequestMap.get(report.id);
					ExpRequestEntity expRequest = expRequestMap.get(report.expPreRequestId);
					ExpRequestApprovalEntity expRequestApproval = requestApprovalMap.get(report.expPreRequestId);
					ExpTypeEntity expType = expTypeMap.get(recordItem.expTypeId);
					CompanyEntity company = companyMap.get(report.companyId);

					ExpJournalEntity journal =
							createJournalFromReport(journalNo, report, record, recordItem, expReportRequest,
									expRequest, expRequestApproval, expType, company, departmentHistoryIdBaseMap, recordNo, recordItemNo);
					journalList.add(journal);

					journalNo = journalNo + 1;
					recordItemNo = recordItemNo + 1;
				}
				recordNo = recordNo +1;
			}
		}
		// 8. Persist the Journal List
		journalRepo.saveEntityList(journalList, true);
		// 9. Corresponding ReportRequest's ExportedTime are updated during the Journal Entity creation and persist the ExportedTime
		reportRequestRepo.saveEntityList(reportRequestList);
	}

	/*
	 * Retrieve the Record and Record Item used by the Report and set it to the respective ExpReportEntity
	 * @param reportList List of ExpReporEntity to be populated with Record and Record Item
	 */
	private void setRecordsAndRecordItems(List<ExpReportEntity> reportList) {
		ExpRecordRepository recordRepo = new ExpRecordRepository();
		for (ExpReportEntity reportEntity : reportList) {
			List<ExpRecordEntity> recordList = recordRepo.getEntityListByReportId(reportEntity.id);
			reportEntity.replaceRecordList(recordList);
		}
	}

	/*
	 * Retrieve the Company Entity used in the ExpReport and return <ID, Entity> map.
	 * @param List of Exp Reports
	 * @return Map<Id, CompanyEntity> companyId as key and Company Entity as the Value
	 */
	private Map<Id, CompanyEntity> buildCompaniesMap(List<ExpReportEntity> reportList) {
		Set<Id> companyIds = new Set<Id>();
		for (ExpReportEntity reportEntity : reportList) {
			companyIds.add(reportEntity.companyId);
		}

		CompanyRepository repo = new CompanyRepository();
		CompanyRepository.SearchFilter searchFilter = new CompanyRepository.SearchFilter();
		searchFilter.companyIds = new List<Id>(companyIds);
		List<CompanyEntity> entityList = repo.searchEntityList(searchFilter);
		Map<Id, CompanyEntity> companyMap = new Map<Id, CompanyEntity>();
		for (CompanyEntity companyEntity : entityList) {
			companyMap.put(companyEntity.Id, companyEntity);
		}
		return companyMap;
	}

	/*
	 * Retrieve the DepartmentBase Entity used in ExpReport and ExpRequest and return <ID, Entity> map.
	 * @param reportList list of Exp Reports
	 * @param expRequestList list of Exp Request
	 * @return Map<Id, DepartmentBaseEntity> Department History Id as key and Department Base Entity as the Value
	 */
	private Map<Id, DepartmentBaseEntity> buildDepartmentsMap(List<ExpReportEntity> reportList, List<ExpRequestEntity> expRequestList) {
		Set<Id> departmentHistoryIds = new Set<Id>();
		for (ExpReportEntity reportEntity : reportList) {
			departmentHistoryIds.add(reportEntity.departmentHistoryId);
		}
		// Add the Department used in the ExpRequest to the Department History Set
		for (ExpRequestEntity reqEntity: expRequestList) {
			departmentHistoryIds.add(reqEntity.departmentHistoryId);
		}

		DepartmentRepository repo = new DepartmentRepository();
		DepartmentRepository.SearchFilter searchFilter = new DepartmentRepository.SearchFilter();
		searchFilter.historyIds = new List<Id>(departmentHistoryIds);
		List<DepartmentBaseEntity> entityList = repo.searchEntity(searchFilter);
		Map<Id, DepartmentBaseEntity> departmentMap = new Map<Id, DepartmentBaseEntity>();
		for (DepartmentBaseEntity departmentBaseEntity : entityList) {
			// Department repo aggregate the Histories of same Base. Link each History with corresponding BaseEntity.
			for (DepartmentHistoryEntity departmentHistory: departmentBaseEntity.getHistoryList()) {
				departmentMap.put(departmentHistory.id, departmentBaseEntity);
			}
		}
		return departmentMap;
	}

	/*
	 * Retrieve the Expense Type Entity used in ExpReports and return <ID, Entity> map.
	 * @param reportList list of Exp Reports
	 * @return Map<Id, ExpTypeEntity> expTypeId as key and ExpTypeEntity Entity as the Value
	 */
	private Map<Id, ExpTypeEntity> buildExpTypesMap(List<ExpReportEntity> reportList) {
		Set<Id> expTypeIds = new Set<Id>();
		for (ExpReportEntity reportEntity : reportList) {
			for (ExpRecordEntity record : reportEntity.recordList) {
				for (ExpRecordItemEntity recordItem : record.recordItemList) {
					expTypeIds.add(recordItem.expTypeId);
				}
			}
		}

		ExpTypeRepository repo = new ExpTypeRepository();
		ExpTypeRepository.SearchFilter searchFilter = new ExpTypeRepository.SearchFilter();
		searchFilter.ids = expTypeIds;
		List<ExpTypeEntity> entityList = repo.searchEntity(searchFilter);
		Map<Id, ExpTypeEntity> expTypeMap = new Map<Id, ExpTypeEntity>();
		for (ExpTypeEntity expTypeEntity : entityList) {
			expTypeMap.put(expTypeEntity.Id, expTypeEntity);
		}
		return expTypeMap;
	}

	/*
	 * Retrieve the RequestApproval (Pre-Application) Entity used in ExpReports and return <ID, Entity> map.
	 * @param reportList List of ExpReports
	 * @return Map<Id, ExpRequestEntity> ExpRequestId as key and ExpRequestEntity as the Value
	 */
	private Map<Id, ExpRequestEntity> buildExpRequestMap(List<ExpReportEntity> reportList) {
		Set<Id> preRequestIds = new Set<Id>();
		for (ExpReportEntity reportEntity : reportList) {
			if (reportEntity.expPreRequestId != null) {
				preRequestIds.add(reportEntity.expPreRequestId);
			}
		}

		ExpRequestRepository repo = new ExpRequestRepository();
		List<ExpRequestEntity> entityList = repo.getEntityList(new List<Id>(preRequestIds));
		Map<Id, ExpRequestEntity> expRequestMap = new Map<Id, ExpRequestEntity>();
		for (ExpRequestEntity expRequestEntity : entityList) {
			expRequestMap.put(expRequestEntity.Id, expRequestEntity);
		}
		return expRequestMap;
	}

	/*
	 * Retrieve the ExpReportRequest Entity used in ExpReports and return <ID, Entity> map.
	 * @param reportRequestList List of ExpReportRequestIds
	 * @return Map<Id, ExpReportRequestEntity> expReportId as key and ExpReportRequestEntity as the Value
	 * @throws App.IllegalStateException if AuthorizedTime is not yet set or ExportedTime is already set
	 */
	private Map<Id, ExpReportRequestEntity> buildReportRequestEntityMap(List<ExpReportRequestEntity> reportRequestList) {
		Map<Id, ExpReportRequestEntity> reportRequestEntityMap = new Map<Id, ExpReportRequestEntity>();
		for (ExpReportRequestEntity reportRequestEntity : reportRequestList) {
			if (reportRequestEntity.authorizedTime == null) {
				throw new App.IllegalStateException(ComMessage.msg().Exp_Err_NotAuthorized);
			} else if (reportRequestEntity.exportedTime != null) {
				throw new App.IllegalStateException(ComMessage.msg().Exp_Err_AlreadyExportedReport);
			}
			reportRequestEntityMap.put(reportRequestEntity.expReportId, reportRequestEntity);
		}
		return reportRequestEntityMap;
	}

	/*
	 * Retrieve the ExpRequestApproval Entity used in ExpReports and return <ID, Entity> map.
	 * @param preRequestIds Set of ExpRequestIds
	 * @return Map<Id, ExpRequestApprovalEntity> ExpRequestApprovalId as key and ExpRequestApprovalEntity as the Value
	 */
	private Map<Id, ExpRequestApprovalEntity> buildRequestApprovalMap(Set<Id> preRequestIds) {
		ExpRequestApprovalRepository repo = new ExpRequestApprovalRepository();
		List<ExpRequestApprovalEntity> requestApprovalList = repo.getEntityListFromRequestId(preRequestIds);
		Map<Id, ExpRequestApprovalEntity> requestApprovalEntityMap = new Map<Id, ExpRequestApprovalEntity>();
		for (ExpRequestApprovalEntity requestApprovalEntity : requestApprovalList) {
			requestApprovalEntityMap.put(requestApprovalEntity.expRequestId, requestApprovalEntity);
		}
		return requestApprovalEntityMap;
	}

	/*
	 * Create Journal Entity based on the Report-Record-RecordItem.
	 * The necessary data are passed to this method in order to avoid DB query per recordItem.
	 * @param journalNo journal number to use
	 * @param journal Journal Entity to be populate (target)
	 * @param report Expense Report Entity (source)
	 * @param record Expense Report Record Entity (source)
	 * @param recordItem Expense Report Record Item Entity (source)
	 * @param reportRequest Expense Report Request Entity (source)
	 * @param expRequest Exp Request Entity (source)
	 * @param requestApproval Exp Request Approval Entity (source)
	 * @param expType Expense Type Entity (source)
	 * @param company Company Entity (source)
	 * @param departmentHistoryIdBaseMap Map containing (HistoryID: BaseEntity) used in the Report
	 * @param recordRowNo Row Number of record in the report type
	 * @param recordItemRowNo Row Number of record item in the record
	 * @return ExpJournalEntity Expense Journal Entity populated with the provided information
	 */
	private ExpJournalEntity createJournalFromReport(Decimal journalNo, ExpReportEntity report, ExpRecordEntity record, ExpRecordItemEntity recordItem,
			ExpReportRequestEntity reportRequest, ExpRequestEntity expRequest, ExpRequestApprovalEntity requestApproval,
			ExpTypeEntity expType, CompanyEntity company, Map<Id, DepartmentBaseEntity> departmentHistoryIdBaseMap,
			Integer recordRowNo, Integer recordItemRowNo) {
		ExpJournalEntity journal = new ExpJournalEntity();

		journal.expReportId = report.id;
		journal.companyCode = company.code;
		AppLanguage companyLanguage = company.language;
		journal.name = journalNo.toPlainString();
		journal.journalNo = String.valueOf(journalNo).leftPad(12, '0');

		AppDatetime exportedTime = AppDatetime.now();
		journal.exportedTime = exportedTime;
		reportRequest.exportedTime = exportedTime;      // Update the ReportRequest Entity's Exported Time
		journal.reportNo = report.reportNo;
		journal.reportPaymentDueDate = report.paymentDueDate;
		journal.reportSubject = report.subject;
		journal.reportType = report.expReportTypeCode;
		journal.reportVendorCode = report.vendorCode;
		journal.recordingDate = report.accountingDate;
		journal.reportDate = AppDate.valueOf(reportRequest.requestTime.getDate());
		journal.reportAmount = report.totalAmount;
		journal.reportCostCenterCode = report.costCenterLinkageCode;
		journal.reportCostCenterName = report.costCenterNameL.getValue(companyLanguage);
		journal.reportJobCode = report.jobCode;
		journal.reportJobName = report.jobNameL.getValue(companyLanguage);
		setReportExtendedItemsToJournal(journal, report);

		journal.reportEmployeeName = report.employeeNameL.getFullName(companyLanguage);
		journal.reportEmployeeCode = report.employeeCode;

		DepartmentBaseEntity deptBase = departmentHistoryIdBaseMap.get(report.departmentHistoryId);
		if (deptBase != null) {
			// Department in Employee is optional. Set only if specified.
			journal.reportEmployeeDepartmentName = deptBase.getHistory(0).nameL.getValue(companyLanguage);
			journal.reportEmployeeDepartmentCode = deptBase.code;
		}

		journal.remark = report.remarks;
		journal.authorizedTime = AppDatetime.valueOf(reportRequest.authorizedTime.getDatetime());
		journal.reportApprovedDate = AppDate.valueOf(reportRequest.lastApproveTime.getDate());
		setRecordAndRecordItemToJournal(journal, report, record, recordItem, expType, companyLanguage, recordRowNo, recordItemRowNo);

		// If converted from the Request, include the Request Information
		if (report.expPreRequestId != null) {
			DepartmentBaseEntity departmentBase = departmentHistoryIdBaseMap.get(expRequest.departmentHistoryId);
			setPreRequestToJournal(journal, expRequest, requestApproval, departmentBase, companyLanguage);
		}
		return journal;
	}

	/*
	 * Set Data from the ExpRecord and ExpRecordItem to the Journal Entity.
	 * @param journal Journal Entity to be populate (target)
	 * @param report Expense Report Entity (source)
	 * @param record ExpRecord Entity (source)
	 * @param recordItem ExpRecordItem Entity (source)
	 * @param expType ExpType Entity used by the ExpRecordItem (source)
	 * @param companyLanguage Language to used for exporting multi-language strings
	 * @param recordRowNo Row Number of record in the report type
	 * @param recordItemRowNo Row Number of record item in the record
	 */
	private void setRecordAndRecordItemToJournal(ExpJournalEntity journal, ExpReportEntity report, ExpRecordEntity record, ExpRecordItemEntity recordItem,
			ExpTypeEntity expType, AppLanguage companyLanguage, Integer recordRowNo, Integer recordItemRowNo) {
		journal.recordId = record.id;
		journal.recordAmount = record.amount;
		journal.recordDate = record.recordDate;
		journal.recordRemarks = record.remarks; 
		journal.recordRowNo = recordRowNo;
		journal.recordItemRowNo = recordItemRowNo;
		// Record Item Fields
		journal.recordItemId = recordItem.id;
		journal.recordItemDate = recordItem.itemDate;
		// Expense Type
		journal.recordItemExpenseTypeId = recordItem.expTypeId;
		journal.recordItemExpenseTypeCode = expType.code;
		journal.expenseTypeDebitAccountName = expType.debitAccountName;
		journal.expenseTypeDebitAccountCode = expType.debitAccountCode;
		journal.expenseTypeDebitSubAccountName = expType.debitSubAccountName;
		journal.expenseTypeDebitSubAccountCode = expType.debitSubAccountCode;
		// Expense Type - Credit Information
		AppMultiString creditAcct = new AppMultiString('現金', 'CASH', null);
		journal.creditAccountCode = creditAcct.getValue(companyLanguage);
		journal.creditAccountName = creditAcct.getValue(companyLanguage);
		journal.creditAmount = recordItem.amount;
		//Tax Type
		journal.taxTypeId = recordItem.taxTypeHistoryId;
		journal.taxTypeCode = recordItem.taxTypeCode;
		journal.taxTypeName = recordItem.taxTypeNameL.getValue(companyLanguage);
		// Amount
		journal.recordItemAmount = recordItem.amount;
		journal.recordItemAmountWithoutTax = recordItem.withoutTax;
		journal.recordItemTaxAmount = recordItem.gstVat;
		// Cost Center
		// NOTE: Cost Center info is retrieved from Report level if info at Record level is null
		if (recordItem.costCenterHistoryId == null) {
			if (report.costCenterHistoryId != null) {
				journal.recordItemCostCenterName = report.costCenterNameL.getValue(companyLanguage);
				journal.recordItemCostCenterCode = report.costCenterLinkageCode;
			}
		} else {
			journal.recordItemCostCenterName = recordItem.costCenterNameL.getValue(companyLanguage);
			journal.recordItemCostCenterCode = recordItem.costCenterLinkageCode;
		}
		// Job
		// NOTE: Job info is retrieved from Report level if info at Record level is null
		if (recordItem.jobId == null) {
			if (report.jobId != null) {
				journal.recordItemJobName = report.jobNameL.getValue(companyLanguage);
				journal.recordItemJobCode = report.jobCode;
			}
		} else {
			journal.recordItemJobName = recordItem.jobNameL.getValue(companyLanguage);
			journal.recordItemJobCode = recordItem.jobCode;
		}
		//If there is no currency Info, then it's not using the Foreign Currency.
		if (recordItem.currencyInfo != null) {
			journal.currencyCode = recordItem.currencyInfo.code;
			journal.localAmount = recordItem.localAmount;
			journal.exchangeRate = recordItem.exchangeRate;
		}

		setRecordExtendedItemsToJournal(journal, recordItem);

		journal.recordItemRemarks = recordItem.remarks;
	}

	/*
	 * Set Extended Items from the Report Entity to the Journal Entity.
	 * @param journal Journal Entity to be populate (target)
	 * @param report Expense Report Entity (source)
	 */
	private void setReportExtendedItemsToJournal(ExpJournalEntity journal, ExpReportEntity report) {
		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			journal.setReportExtendedItemDate(i, report.getExtendedItemDateValue(i));
			journal.setReportExtendedItemText(i, report.getExtendedItemTextValue(i));
			journal.setReportExtendedItemPicklist(i, report.getExtendedItemPicklistValue(i));
			journal.setReportExtendedItemLookup(i, report.getExtendedItemLookupValue(i));
		}
	}

	/*
	 * Set Extended Items from the RecordItem Entity to the Journal Entity.
	 * @param journal Journal Entity to be populate (target)
	 * @param recordItem Expense Record Item Entity (source)
	 */
	private void setRecordExtendedItemsToJournal(ExpJournalEntity journal, ExpRecordItemEntity recordItem) {
		for (Integer i = 0; i < ExpTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			journal.setRecordItemExtendedItemDate(i, recordItem.getExtendedItemDateValue(i));
			journal.setRecordItemExtendedItemText(i, recordItem.getExtendedItemTextValue(i));
			journal.setRecordItemExtendedItemPicklist(i, recordItem.getExtendedItemPicklistValue(i));
			journal.setRecordItemExtendedItemLookup(i, recordItem.getExtendedItemLookupValue(i));
		}
	}

	/*
	 * Set Data from the Exp Request Entity to the Journal Entity.
	 * @param journal Journal Entity to be populate (target)
	 * @param requestEntity ExpRequestEntity to copy data from (source)
	 * @param requestApprovalEntity ExpRequestApprovalEntity to copy data from (source)
	 * @param departmentHistoryIdBaseMap Map containing (HistoryID: BaseEntity) used in the Report
	 * @param companyLanguage Language to used for exporting multi-language strings
	 */
	private void setPreRequestToJournal(ExpJournalEntity journal,
			ExpRequestEntity expRequest, ExpRequestApprovalEntity expRequestApproval,
			DepartmentBaseEntity deptBase, AppLanguage companyLanguage) {
		journal.requestNo = expRequest.requestNo;
		journal.requestSubject = expRequest.subject;
		journal.requestReportType = expRequest.expReportTypeCode;
		journal.scheduleDate = expRequest.scheduledDate;
		journal.requestDate = AppDate.valueOf(expRequestApproval.requestTime.getDate());
		journal.requestAmount = expRequest.totalAmount;
		// Cost Center
		journal.requestCostCenterCode = expRequest.costCenterLinkageCode;
		journal.requestCostCenterName = expRequest.costCenterNameL.getValue(companyLanguage);
		// Job
		journal.requestJobCode = expRequest.jobCode;
		journal.requestJobName = expRequest.jobNameL.getValue(companyLanguage);

		setRequestExtendedItemsToJournal(journal, expRequest);

		// Employee Code
		journal.requestEmployeeCode = expRequest.employeeCode;
		journal.requestEmployeeName = expRequest.employeeNameL.getFullName(companyLanguage);

		if (deptBase != null) {
			// Department in Employee is optional. Set only if specified.
			journal.requestEmployeeDepartmentCode = deptBase.code;
			journal.requestEmployeeDepartmentName = deptBase.getHistory(0).nameL.getValue(companyLanguage);
		}

		journal.purpose = expRequest.purpose;
		journal.approvedDate = AppDate.valueOf(expRequestApproval.lastApproveTime.getDate());
	}

	/*
	 * Set Extended Items from the Exp Request Entity to the Journal Entity.
	 * @param journal Journal Entity to be populate (target)
	 * @param report Expense Request Entity (source)
	 */
	private void setRequestExtendedItemsToJournal(ExpJournalEntity journal, ExpRequestEntity request) {
		for (Integer i = 0; i < ExpReportTypeEntity.EXTENDED_ITEM_MAX_COUNT; i++) {
			journal.setRequestExtendedItemDate(i, request.getExtendedItemDateValue(i));
			journal.setRequestExtendedItemText(i, request.getExtendedItemTextValue(i));
			journal.setRequestExtendedItemPicklist(i, request.getExtendedItemPicklistValue(i));
			journal.setRequestExtendedItemLookup(i, request.getExtendedItemLookupValue(i));
		}
	}
}