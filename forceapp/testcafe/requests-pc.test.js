import { Selector, Role } from 'testcafe';

import { registeredUser } from './selectors/Common';
import { RequestPC } from './selectors/RequestPC';
import { ApprovalPC } from './selectors/ApprovalPC';

// Homepage constants
const REQUEST_PC_PAGE = 'https://c.ap15.visual.force.com/apex/Requests';
const APPROVAL_PC_PAGE = 'https://c.ap15.visual.force.com/apex/Approval';

// initialise selectors
const REQUEST_PC_SELECTORS = new RequestPC();
const APPROVAL_PC_SELECTORS = new ApprovalPC();

fixture('Requests PC Test');

test('Create and submit new request', async (t) => {
  await t.useRole(registeredUser).navigateTo(REQUEST_PC_PAGE);

  // Clicks the New Report Button
  await t.click(REQUEST_PC_SELECTORS.newReportBtn);

  // Input new request title, "New Test Request"
  await t
    .typeText(REQUEST_PC_SELECTORS.reportTitle, 'New Test Request', {
      replace: true,
    })
    .expect(REQUEST_PC_SELECTORS.reportTitle.value)
    .eql('New Test Request');

  // Selects the final reportType
  await t
    .click(REQUEST_PC_SELECTORS.reportTypeSelect)
    .click(REQUEST_PC_SELECTORS.reportTypeOption.withText('Minimum Report'));

  // Type purpose
  await t
    .typeText(REQUEST_PC_SELECTORS.purposeInput, 'TestCafe Test Request', {
      replace: true,
    })
    .expect(REQUEST_PC_SELECTORS.purposeInput.value)
    .eql('TestCafe Test Request');

  // Types the date
  await t
    .typeText(REQUEST_PC_SELECTORS.scheduledDateInput, '06/11/2019', {
      replace: true,
    })
    .expect(REQUEST_PC_SELECTORS.scheduledDateInput.value)
    .eql('06/11/2019');

  // Clicks the save button
  await t.click(REQUEST_PC_SELECTORS.saveReportBtn);
  // Submits the report
  await t.click(REQUEST_PC_SELECTORS.submitReportBtn);

  await REQUEST_PC_SELECTORS.dialogConfirmSubmitBtn;
  // Clicks request to submit
  await t.click(REQUEST_PC_SELECTORS.dialogConfirmSubmitBtn);

  // wait 5 seconds for the api to return successfully
  await t.wait(5000);

  await t.expect(REQUEST_PC_SELECTORS.statusLabel.innerText).eql('Pending');
});

test('Check submitted report in approval page', async (t) => {
  await t.useRole(registeredUser).navigateTo(APPROVAL_PC_PAGE);

  await APPROVAL_PC_SELECTORS.requestTab;
  // Click request tab
  await t.click(APPROVAL_PC_SELECTORS.requestTab);

  // check new request purpose
  await t
    .expect(APPROVAL_PC_SELECTORS.purposeLabel.innerText)
    .eql('TestCafe Test Request');
});
