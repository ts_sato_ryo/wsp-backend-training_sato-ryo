import { Selector } from 'testcafe';

export class ApprovalPC {
  constructor() {
    this.requestTab = Selector('.ts-approval__header__tab > div:last-child > a.tab__anchor');
    this.purposeLabel = Selector('.approvals-pc-expenses-pre-approval-pane-detail__container > ul > li:nth-child(4) .ts-horizontal-layout__body');
  }
}
