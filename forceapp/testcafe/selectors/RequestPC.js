import { Selector } from 'testcafe';

export class RequestPC {
  constructor() {
    // Report Summary
    this.newReportBtn = Selector('.ts-expenses__header__btn');
    this.reportTitle = Selector('.ts-expenses__form-report-summary__header-input');
    this.accountingPeriodSelect = Selector('.ts-expenses__form-report-summary__form__ap__select');
    this.accountingPeriodOption = this.accountingPeriodSelect.find('option');
    this.reportTypeSelect = Selector('.ts-expenses__form-report-summary__form__report-type-input .ts-select-input');
    this.reportTypeOption = this.reportTypeSelect.find('option');
    this.saveReportBtn = Selector('.ts-expenses__form-report-summary__actions__save');
    this.submitReportBtn = Selector('.ts-expenses__form-report-summary__actions__submit');
    // After Pagination is merged, backtoReportListBtn will change
    // this.backToReportListBtn = Selector('.ts-finance-approval-sub-header-pager__back-btn');
    this.backToReportListBtn = Selector('.tab__anchor');
    this.firstReportItem = Selector('.ts-expenses__reports-list-items:first-child .ts-expenses__reports-list-items-text__subject');
    this.summaryActionToggleBtn = Selector('.ts-expenses__form-report-summary__actions__toggle');
    this.deleteReportBtn = Selector('.ts-expenses__form-report-summary__actions__delete');
    this.dialogConfirmSubmitBtn = Selector('.commons-dialog-frame-footer .ts-button--primary');
    this.dialogConfirmBtn = Selector('#commons-dialogs-confirm-dialog__ok-button');

    this.purposeInput = Selector('.ts-expenses__form-report-summary__form__purpose .slds-input');
    this.scheduledDateInput = Selector('.react-datepicker__input-container > .slds-input');
    this.statusLabel = Selector('.ts-expenses__form-report-status__label');

    // Record Summary
    this.newRecordBtn = Selector('.ts-expenses__form-records-button-area__btn-new > button');
  }
}
