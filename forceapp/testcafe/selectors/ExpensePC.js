import { Selector } from 'testcafe';

export class ExpensePC {
  constructor() {
    // Report Summary
    this.newReportBtn = Selector('.ts-expenses__header__btn');
    this.reportTitle = Selector('.ts-expenses__form-report-summary__header-input');
    this.accountingPeriodSelect = Selector('.ts-expenses__form-report-summary__form__ap__select');
    this.accountingPeriodOption = this.accountingPeriodSelect.find('option');
    this.reportTypeSelect = Selector('.ts-expenses__form-report-summary__form__report-type-input .ts-select-input');
    this.reportTypeOption = this.reportTypeSelect.find('option');
    this.saveReportBtn = Selector('.ts-expenses__form-report-summary__actions__save');
    // After Pagination is merged, backtoReportListBtn will change
    // this.backToReportListBtn = Selector('.ts-finance-approval-sub-header-pager__back-btn');
    this.backToReportListBtn = Selector('.tab__anchor');
    this.firstReportItem = Selector('.ts-expenses__reports-list-items:first-child .ts-expenses__reports-list-items-text__subject');
    this.summaryActionToggleBtn = Selector('.ts-expenses__form-report-summary__actions__toggle');
    this.deleteReportBtn = Selector('.ts-expenses__form-report-summary__actions__delete');
    this.dialogConfirmBtn = Selector('#commons-dialogs-confirm-dialog__ok-button');

    // Record Summary
    this.newRecordBtn = Selector('.ts-expenses__form-records-button-area__btn-new > button');
    this.selectExpenseTypeBtn = Selector('.ts-multi-column-finder.slds .ts-multi-column-finder__btn-grp button:last-child');
    this.firstExpenseTypeGroup = Selector('.ts-multi-column-finder__pointer:first-child .ts-multi-column-finder__show-child-btn');
    this.generalExpenseType = Selector('.ts-multi-column-finder__item:nth-child(10)');
    this.amountInputField = Selector('.ts-expenses-requests__contents__amount__amount  .slds-input');
    this.selectReceiptLibraryBtn = Selector('.record-receipt__button--file');
    this.firstReceiptInput = Selector('.ts-expenses-modal-receipt-library-image-display__img-area:first-child .ts-expenses-modal-receipt-library-image-display__input');
    this.receiptLibraryAttachBtn = Selector('.commons-dialog-frame-footer__inner > .ts-button--primary');
    this.recordItemContent = Selector('.ts-expenses-requests__contents');
    this.amountWithoutTax = Selector('.amount-without-tax');
    this.taxAmount = Selector('.gst-vat');
    this.saveRecordBtn = Selector('.ts-expenses__form-record-item__actions__save');
    this.firstRecordItem = Selector('.ts-expenses__form-records__list__item');
    this.firstRecordItemAmount = Selector('.ts-expenses__form-records__list__item  .ts-expenses__form-records__list__item__amount');
    this.firstRecordItemInput = Selector('.ts-expenses__form-records__list__item  .ts-expenses__form-records__list__item__wrapbox__checkbox__input');
    this.editRecordItemBtn = Selector('.ts-expenses__form-records-button-area__btn-edit > button');
    this.deleteRecordItemBtn = Selector('.ts-edit-menu__button');
  }
}
