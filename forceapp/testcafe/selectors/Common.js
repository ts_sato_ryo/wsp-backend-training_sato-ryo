import { Role } from 'testcafe';

require('dotenv').config();

const HOMEPAGE = 'https://login.salesforce.com';

export const registeredUser = Role(HOMEPAGE, async (t) => {
  // Logins to Salesforce
  if (!process.env.TEST_ORG_USERNAME) {
    throw Error('TEST_ORG_USERNAME environment variable not set in .env file');
  }

  if (!process.env.TEST_ORG_PASSWORD) {
    throw Error('TEST_ORG_PASSWORD environment variable not set in .env file');
  }

  await t
    .typeText('#username', process.env.TEST_ORG_USERNAME)
    .typeText('#password', process.env.TEST_ORG_PASSWORD)
    .click('#Login');
});
