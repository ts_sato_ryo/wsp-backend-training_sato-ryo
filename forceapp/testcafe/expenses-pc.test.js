import { Selector, Role } from 'testcafe';

import { registeredUser } from './selectors/Common';
import { ExpensePC } from './selectors/ExpensePC';

// Homepage constants
const EXPENSE_PC = 'https://c.ap15.visual.force.com/apex/Expenses';

// initialise selectors
const EXPENSE_PC_SELECTORS = new ExpensePC();

fixture('Expenses PC Test');

test('Creates New Report', async (t) => {
  await t.useRole(registeredUser).navigateTo(EXPENSE_PC);

  // Clicks the New Report Button
  await t.click(EXPENSE_PC_SELECTORS.newReportBtn);

  // Input new title, "New Test Report"
  await t
    .typeText(EXPENSE_PC_SELECTORS.reportTitle, 'New Test Report', {
      replace: true,
    })
    .expect(EXPENSE_PC_SELECTORS.reportTitle.value)
    .eql('New Test Report');

  // Selects the final reportType
  await t
    .click(EXPENSE_PC_SELECTORS.reportTypeSelect)
    .click(EXPENSE_PC_SELECTORS.reportTypeOption.withText('Minimum Report'))
    .expect(EXPENSE_PC_SELECTORS.reportTypeSelect.value)
    .eql('a102v00000LlbhpAAB');

  // Selects the first accountingPeriod
  await t
    .click(EXPENSE_PC_SELECTORS.accountingPeriodSelect)
    .click(EXPENSE_PC_SELECTORS.accountingPeriodOption.nth(-1));

  // Click the save button
  await t.click(EXPENSE_PC_SELECTORS.saveReportBtn);

  // Clicks the report button to go back to report list
  await t.click(EXPENSE_PC_SELECTORS.backToReportListBtn);
  await t
    .expect(EXPENSE_PC_SELECTORS.firstReportItem.innerText)
    .eql('New Test Report');
});

test('Create New Record', async (t) => {
  await t.useRole(registeredUser).navigateTo(EXPENSE_PC);

  // Clicks the first report item
  await t.click(EXPENSE_PC_SELECTORS.firstReportItem);

  // Clicks new record button
  await t.click(EXPENSE_PC_SELECTORS.newRecordBtn);

  // Clicks Select Expense Type from category button
  await t.click(EXPENSE_PC_SELECTORS.selectExpenseTypeBtn);

  // Clicks the first expense type group
  await t.click(EXPENSE_PC_SELECTORS.firstExpenseTypeGroup);

  // Clicks the general expense type
  await t.click(EXPENSE_PC_SELECTORS.generalExpenseType);

  // Change amount input
  await t
    .typeText(EXPENSE_PC_SELECTORS.amountInputField, '100', {
      replace: true,
    })
    .expect(EXPENSE_PC_SELECTORS.amountInputField.value)
    .eql('100');

  // Click outside to recalculate result
  await t.click(EXPENSE_PC_SELECTORS.recordItemContent);

  // Check that the amountWithoutTax is correct
  await t.expect(EXPENSE_PC_SELECTORS.amountWithoutTax.value).eql('S$ 92.60');

  // Check that the tax amount is correct
  await t.expect(EXPENSE_PC_SELECTORS.taxAmount.value).eql('S$ 7.40');

  // Click the save button
  await t.click(EXPENSE_PC_SELECTORS.saveRecordBtn);

  // expects first record item to exist
  await t.expect(EXPENSE_PC_SELECTORS.firstRecordItem.exists).ok();
});

test('Update New Record', async (t) => {
  await t.useRole(registeredUser).navigateTo(EXPENSE_PC);

  // Clicks the first report item
  await t.click(EXPENSE_PC_SELECTORS.firstReportItem);

  // Clicks first record Item
  await t.click(EXPENSE_PC_SELECTORS.firstRecordItem);

  // Change amount input
  await t
    .typeText(EXPENSE_PC_SELECTORS.amountInputField, '120', {
      replace: true,
    })
    .expect(EXPENSE_PC_SELECTORS.amountInputField.value)
    .eql('120');

  // Click outside to recalculate result
  await t.click(EXPENSE_PC_SELECTORS.recordItemContent);

  // Check that the amountWithoutTax is correct
  await t.expect(EXPENSE_PC_SELECTORS.amountWithoutTax.value).eql('S$ 111.12');

  // Check that the tax amount is correct
  await t.expect(EXPENSE_PC_SELECTORS.taxAmount.value).eql('S$ 8.88');

  // Click the save button
  await t.click(EXPENSE_PC_SELECTORS.saveRecordBtn);

  // Checks first record item amount is updated
  await t
    .expect(EXPENSE_PC_SELECTORS.firstRecordItemAmount.textContent)
    .eql('S$ 120.00');
});

test('Delete New Record', async (t) => {
  await t.useRole(registeredUser).navigateTo(EXPENSE_PC);

  // Clicks the first report item
  await t.click(EXPENSE_PC_SELECTORS.firstReportItem);

  // Clicks first record Item
  await t.click(EXPENSE_PC_SELECTORS.firstRecordItemInput);

  // Click edit button
  await t.click(EXPENSE_PC_SELECTORS.editRecordItemBtn);

  // Click delete record item button
  await t.click(EXPENSE_PC_SELECTORS.deleteRecordItemBtn);

  // expects first record item to not exist
  await t.expect(EXPENSE_PC_SELECTORS.firstRecordItem.exists).notOk();
});

test('Edit/Updates Report', async (t) => {
  await t.useRole(registeredUser).navigateTo(EXPENSE_PC);

  // Clicks the first report item
  await t.click(EXPENSE_PC_SELECTORS.firstReportItem);

  // Input new title, "New Test Report"
  await t
    .typeText(EXPENSE_PC_SELECTORS.reportTitle, 'Updated New Test Report', {
      replace: true,
    })
    .expect(EXPENSE_PC_SELECTORS.reportTitle.value)
    .eql('Updated New Test Report');

  // wait for the report type api to return before continue
  await t.wait(50000);

  // Click the save button
  await t.click(EXPENSE_PC_SELECTORS.saveReportBtn);

  await EXPENSE_PC_SELECTORS.backToReportListBtn;

  // Clicks the report button to go back to report list
  await t.click(EXPENSE_PC_SELECTORS.backToReportListBtn);
  await t
    .expect(EXPENSE_PC_SELECTORS.firstReportItem.innerText)
    .eql('Updated New Test Report');
});

test('Delete Report', async (t) => {
  await t.useRole(registeredUser).navigateTo(EXPENSE_PC);

  await EXPENSE_PC_SELECTORS.firstReportItem;

  // Clicks the first report item
  await t.click(EXPENSE_PC_SELECTORS.firstReportItem);

  // Toggle the action buttons πanel
  await t.click(EXPENSE_PC_SELECTORS.summaryActionToggleBtn);

  // Clicks the delete button
  await t.click(EXPENSE_PC_SELECTORS.deleteReportBtn);

  // Clicks the dialog OK button
  await t.click(EXPENSE_PC_SELECTORS.dialogConfirmBtn);

  await t
    .expect(EXPENSE_PC_SELECTORS.firstReportItem.innerText)
    .notEql('Updated New Test Report');
});
