// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import onDevelopment from '../commons/config/development';

import TrackSummary from '../time-tracking/TrackSummary';

import './index.scss';

const ROOT = 'subapps-debug';

const SubAppMenu = () => (
  <>
    <section className={`${ROOT}__menu__section`}>
      <a className={`${ROOT}__menu__item`} href="/index.html">
        Top
      </a>
    </section>

    {/*
      Add Link to sub app
      <Link className="menu__item" to="path to sub app">
        Sub App Name
      </Link>
    */}

    <section className={`${ROOT}__menu__section`}>
      <h2 className="title">Expense</h2>
      <div />
    </section>

    <section className={`${ROOT}__menu__section`}>
      <h2 className="title">Attendance</h2>
      <div />
    </section>

    <section className={`${ROOT}__menu__section`}>
      <h2 className="title">Time Tracking</h2>
      <div className={`${ROOT}__menu__items`}>
        <Link className={`${ROOT}__menu__item`} to="/track-summary/approval">
          TrackSummary.Approval
        </Link>
        <Link className={`${ROOT}__menu__item`} to="/track-summary/request">
          TrackSummary.Request
        </Link>
      </div>
    </section>
  </>
);

const Index = () => {
  return (
    <Router basename="/subapps-debug">
      <div className={ROOT}>
        <h1 className="title">Sub Apps</h1>
        <div className={`${ROOT}__section`}>
          <div className={`${ROOT}__menu`}>
            <Route path="/" component={SubAppMenu} />
          </div>
          <div className={`${ROOT}__content`}>
            {/*
              Write the following code to add your sub app to routes.
              <Rotue exact path="..." component={...} />
            */}

            <Route
              exact
              path="/track-summary/approval"
              component={TrackSummary.Approval}
            />
            <Route
              exact
              path="/track-summary/request"
              component={TrackSummary.Request}
            />
          </div>
        </div>
      </div>
    </Router>
  );
};

function renderApp(Component): void {
  const container = document.getElementById('container');
  if (container) {
    onDevelopment(() => {
      ReactDOM.render(<Component />, container);
    });
  }
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = (_params: Object) => renderApp(Index);
