/* @flow */
import { connect } from 'react-redux';

import App from '../components';

import { execute } from '../action-dispatchers/app';

const mapStateToProps = (state) => ({
  response: state.response,
});

const mapDispatchToProps = {
  executeAPI: execute,
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(App): React.ComponentType<Object>);
