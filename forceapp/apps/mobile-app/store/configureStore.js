// @flow

/* eslint-disable global-require, import/newline-after-import */
import logger from 'redux-logger';

import { type Dispatch, createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer, { type State } from '../modules';

const PRODUCTION = process.env.NODE_ENV === 'production';

/* eslint-disable no-underscore-dangle */
const composeEnhancers: any =
  !PRODUCTION && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;

export default function configureStore(initialState?: State) {
  const middlewares = [thunkMiddleware];
  if (!PRODUCTION) {
    middlewares.push(logger);
  }

  const store = createStore<Object, Object, Dispatch<Object>>(
    (rootReducer: any),
    initialState,
    // Redux DevTools Extension
    composeEnhancers(applyMiddleware(...middlewares))
  );

  if (!PRODUCTION && module.hot) {
    module.hot.accept('../modules', () => {
      const newRootReducer = require('../modules').default;
      store.replaceReducer((newRootReducer: any));
    });
  }

  return store;
}
