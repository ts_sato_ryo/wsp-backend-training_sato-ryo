// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import Toast from '../../components/atoms/Toast';

storiesOf('Components/atoms/Toast', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    () => {
      return (
        <Toast
          message={text('message', '申請しました')}
          isShow={boolean('isShow', true)}
        />
      );
    },
    {
      info: `
        # Description

        申請完了や、保存完了時に表示するトーストです
      `,
    }
  );
