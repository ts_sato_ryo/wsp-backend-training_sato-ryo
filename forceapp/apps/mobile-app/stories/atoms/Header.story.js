// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, number } from '@storybook/addon-knobs';

import Header from '../../components/atoms/Header';

storiesOf('Components/atoms/Header', module)
  .addDecorator(withKnobs)
  .add('Basic', () => <Header level={number('level', 1)}>HEADER</Header>, {
    info: `
      # Description

      ナビゲーションや見出しを表示するためのヘッダーコンポーネント。
      levelが1の場合には画面の最上部に固定されます。
    `,
  });
