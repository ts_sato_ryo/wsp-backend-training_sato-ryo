// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import ListHeader from '../../components/atoms/ListHeader';

import './ListHeader.story.scss';

storiesOf('Components/atoms/ListHeader', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        ListHeader コンポーネント

        # Props

        \`onClick\` コンポーネント全体をクリックできるようになっております。
      `
    )(() => (
      <div>
        <div className="list-header-body">
          <ListHeader onClick={action('onClick only header')}>
            LIST HEADER
          </ListHeader>
        </div>
        <div className="list-header-body">
          <ListHeader onClick={action('onClick 1st header')}>
            LIST HEADER 1
          </ListHeader>
          <ListHeader onClick={action('onClick 2nd header')}>
            LIST HEADER 2
          </ListHeader>
          <ListHeader onClick={action('onClick 3rd header')}>
            LIST HEADER 3
          </ListHeader>
          <ListHeader onClick={action('onClick 4th header')}>
            LIST HEADER 4
          </ListHeader>
        </div>
      </div>
    ))
  );
