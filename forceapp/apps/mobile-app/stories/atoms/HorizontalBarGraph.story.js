// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, array } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import HorizontalBarGraph from '../../components/atoms/HorizontalBarGraph';

storiesOf('Components/atoms/HorizontalBarGraph', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(`
    # Description

  `)(() => (
      <HorizontalBarGraph
        data={array('data', [
          {
            color: '#0083b6',
            value: 510,
            label: 'TEST',
          },
          {
            color: '#00a3df',
            value: 370,
            label: 'TEST',
          },
          {
            color: '#ddd',
            value: 900,
            label: 'TEST',
            labelColor: 'black',
            labelAlign: 'right',
          },
        ])}
      />
    ))
  );
