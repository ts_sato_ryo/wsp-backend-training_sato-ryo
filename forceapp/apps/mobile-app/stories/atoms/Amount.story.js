// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Amount from '../../components/atoms/Amount';

storiesOf('Components/atoms/Amount', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        金額コンポーネント
        自動的に表示内容を整形します

        # Propsについて

        amount: 整形前の金額
        DecimalPlaces: 小数点以下の桁数 
        Symbol: 通貨記号,
      `
    )(() => (
      <Amount
        amount={text('amount', '199.99')}
        decimalPlaces={text('decimalPlaces', 2)}
        symbol={text('symbol', '$')}
      />
    ))
  );
