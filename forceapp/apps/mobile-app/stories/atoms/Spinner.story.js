// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';

import Spinner from '../../components/atoms/Spinner';

import colors from '../../styles/variables/_colors.scss';

storiesOf('Components/atoms/Spinner', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    () => (
      <Spinner
        assistiveText={text('assistiveText', null)}
        color={text('color', null)}
      />
    ),
    {
      info: {
        text: `
      ローディングなどを表現するSpinnerになります。
    `,
      },
    }
  )
  .add(
    'Colors',
    () => (
      <div>
        <Spinner
          assistiveText={text('assistiveText', null)}
          color={text('color', null)}
        />

        <div
          style={{
            display: 'flex',
            flexFlow: 'row wrap',
            justifyContent: 'flex-start',
          }}
        >
          {Object.keys(colors).map((key) => (
            <div
              style={{
                textAlign: 'center',
              }}
            >
              <Spinner color={colors[key]} />
              {key}
            </div>
          ))}
        </div>
      </div>
    ),
    {
      info: {
        text: `
      ローディングなどを表現するSpinnerになります。
    `,
      },
    }
  );
