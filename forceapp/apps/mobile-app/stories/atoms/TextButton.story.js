// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean } from '@storybook/addon-knobs';

import TextButton from '../../components/atoms/TextButton';

storiesOf('Components/atoms/TextButton', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    `
      # Normal

      ~~~js
      <TextButton>TEXT</TextButton>
      ~~~

      # Disabled

      \`disabled\`を\`true\`にすると\`TextButton\`が非活性になります。

      ~~~js
      <TextButton disabled />
      ~~~

      # E2E テスト

      E2Eテストのために\`testId\` prop に一意なIDを渡してください。
      これにより\`TextButton\`が常に一意に特定できる事をE2Eテストに保障すること出来ます。
    `,
    () => (
      <TextButton
        disabled={boolean('disabled', null)}
        onClick={action('onClick')}
        testId="unique-id"
      >
        +休暇を追加
      </TextButton>
    )
  );
