// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import LinkListItem from '../../components/atoms/LinkListItem';

import './LinkListItem.story.scss';

storiesOf('Components/atoms/LinkListItem', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        LinkListItem コンポーネント

        # Props

        \`onClick\` コンポーネント全体をクリックできるようになっております。
      `
    )(() => (
      <div>
        <div className="list-item-body">
          <LinkListItem onClick={action('onClick only item')}>
            {text('children', 'LIST ITEM')}
          </LinkListItem>
        </div>
        <div className="list-item-body">
          <LinkListItem onClick={action('onClick 1st item')}>
            LIST ITEM 1
          </LinkListItem>
          <LinkListItem onClick={action('onClick 2nd item')}>
            LIST ITEM 2
          </LinkListItem>
          <LinkListItem onClick={action('onClick 3rd item')}>
            LIST ITEM 3
          </LinkListItem>
          <LinkListItem onClick={action('onClick 4th item')}>
            LIST ITEM 4
          </LinkListItem>
        </div>
      </div>
    ))
  );
