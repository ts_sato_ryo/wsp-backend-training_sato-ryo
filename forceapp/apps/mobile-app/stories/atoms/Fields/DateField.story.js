// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import DateField from '../../../components/atoms/Fields/DateField';

storiesOf('Components/atoms/Fields/DateField', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        Date Field
      `
    )(() => (
      <DateField
        testId={text('testId', 'unique-id')}
        required={boolean('required', false)}
        error={boolean('error', false)}
        value={text('value', '2019-01-01')}
        disabled={boolean('disabled', false)}
        readOnly={boolean('readOnly', false)}
        onChange={action('onChange')}
      />
    ))
  );
