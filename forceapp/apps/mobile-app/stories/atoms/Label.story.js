// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean, text } from '@storybook/addon-knobs';

import Label from '../../components/atoms/Label';

storiesOf('Components/atoms/Label', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    () => (
      <Label
        text={text(
          'text',
          'LABEL LABEL LABEL LABEL ラベル　ラベル　ラベル　ラベル ラベル'
        )}
        marked={boolean('marked', null)}
        emphasis={boolean('emphasis', null)}
      />
    ),
    {
      info: `
      # Description

      フォーム等に使われるラベルです。

      # Marked label

      \`marked\` props を\`true\`にするとラベルにマークをつけることができます。

      ~~~js
      <Label text="LABEL" marked />
      ~~~
    `,
    }
  )
  .add(
    'Form',
    () => (
      <div>
        <div>
          <Label text="Clickable!" htmlFor="id" />
          <input type="checkbox" id="id" />
        </div>
        <hr />
        <Label text="Clickable!">
          <input type="checkbox" />
        </Label>
      </div>
    ),
    {
      info: `
      HTML5のlabel要素と同様にformと紐付けて、ラベルをクリック可能な領域とし扱うことができます。

      # htmlForを使う

      ~~~js
      <Label text="Clickable!" htmlFor="id" />
      <input type="checkbox" id="id" />
      ~~~

      # form controlをLabelの子要素にする

      ~~~js
      <Label text="Clickable!">
        <input type="checkbox" />
      </Label>
      ~~~

    `,
    }
  );
