// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, array } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Errors from '../../components/atoms/Errors';

storiesOf('Components/atoms/Errors', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(`
      # Description

      エラーメッセージを表示します。
    `)(() => (
      <Errors
        messages={array('messages', ['Errror Message #1', 'Errror Message #2'])}
      />
    ))
  );
