// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs } from '@storybook/addon-knobs';

import Person from '../../components/atoms/Person';

import ImgSample from '../images/sample.png';

storiesOf('Components/atoms/Person', module)
  .addDecorator(withKnobs)
  .add('Basic', () => <Person src={ImgSample} alt="sample" />, {
    info: `
      # Description

      承認者、申請者のアイコンデザインです。
    `,
  });
