// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean, text } from '@storybook/addon-knobs';

import IconButton from '../../components/atoms/IconButton';

storiesOf('Components/atoms/IconButton', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    () => (
      <IconButton
        icon={text('icon', 'adduser')}
        disabled={boolean('disabled', null)}
        onClick={action('onClick')}
        testId="unique-id"
      />
    ),
    {
      info: `
      # Normal

      ~~~js
      <IconButton icon="adduser"/>
      ~~~

      # Disabled

      \`disabled\`を\`true\`にすると\`IconButton\`が非活性になります。

      ~~~js
      <IconButton disabled icon="adduser" />
      ~~~

      # E2E テスト

      E2Eテストのために\`testId\` prop に一意なIDを渡してください。
      これにより\`IconButton\`が常に一意に特定できる事をE2Eテストに保障すること出来ます。
    `,
    }
  );
