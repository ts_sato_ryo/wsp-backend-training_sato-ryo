// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Comment from '../../components/atoms/Comment';

storiesOf('Components/atoms/Comment', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        Comment コンポーネント

        # Propsについて

        Positionで吹き出しの位置を変更できます。
        現在、leftとrightのみ
      `
    )(() => (
      <Comment
        position={text('position', 'left')}
        value={text('value', 'Value Value Value Value')}
      />
    ))
  );
