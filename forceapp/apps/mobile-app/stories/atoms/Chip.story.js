// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Chip from '../../components/atoms/Chip';

storiesOf('Components/atoms/Chip', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(`
     # Description

      Show supplementary information
    `)(() => (
      <Chip
        text={text('text', 'TEXT TEXT TEXT TEXT TEXT text text text text.')}
      />
    ))
  );
