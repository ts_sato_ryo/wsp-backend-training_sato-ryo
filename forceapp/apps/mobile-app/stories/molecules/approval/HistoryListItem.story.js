// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import HistoryListItemItem from '../../../components/molecules/approval/HistoryListItem';

import ImgSample from '../../images/sample.png';

storiesOf('Components/molecules/approval', module)
  .addDecorator(withKnobs)
  .addDecorator((story) => <div>{story()}</div>)
  .add(
    'HistoryListItemItem',
    withInfo({
      text: `
            # Description

            承認履歴に使われれる行です。
          `,
    })(() => (
      <HistoryListItemItem
        history={{
          id: text('id', 'ID'),
          stepName: text('stepName', 'STEP NAME'),
          approveTime: text('approveTime', '2019-01-01 00:00'),
          status: '',
          statusLabel: text('statusLabel', 'STATUS LABEL'),
          approverName: text('approverName', 'APPROVER NAME'),
          actorName: text('actorName', 'ACTOR NAME'),
          actorPhotoUrl: ImgSample,
          comment: text('comment', 'COMMENT'),
          isDelegated: false,
        }}
      />
    ))
  );
