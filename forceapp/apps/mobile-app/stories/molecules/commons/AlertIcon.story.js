// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import AlertIcon from '../../../components/molecules/commons/AlertIcon';

storiesOf('Components/molecules/commons/AlertIcon', module)
  .addDecorator(withKnobs)
  .add(
    'Warning',
    withInfo(
      `
        # Description

        Display AlertIcon
      `
    )(() => <AlertIcon variant={text('variant', 'warning')} />)
  )
  .add(
    'Attention',
    withInfo(
      `
        # Description

        Display AlertIcon
      `
    )(() => <AlertIcon variant={text('variant', 'attention')} />)
  );
