// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import PrevButton from '../../../../components/molecules/commons/Buttons/PrevButton';

storiesOf('Components/molecules/commons/Buttons', module)
  .addDecorator(withKnobs)
  .add(
    'PrevButton',
    withInfo(
      `
        # Description

        A Button to go back previous content.
      `
    )(() => (
      <PrevButton
        testId={text('testId', 'unique-id')}
        text={text('text', 'PrevButton')}
        onClick={action('onClick')}
        disabled={boolean('disabled', false)}
      />
    ))
  );
