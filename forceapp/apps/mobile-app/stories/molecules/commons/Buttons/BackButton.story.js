// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import BackButton from '../../../../components/molecules/commons/Buttons/BackButton';

storiesOf('Components/molecules/commons/Buttons', module)
  .addDecorator(withKnobs)
  .add(
    'BackButton',
    withInfo(
      `
        # Description

        A Button to go back previous page.
      `
    )(() => (
      <BackButton
        testId={text('testId', 'unique-id')}
        text={text('text', 'BackButton')}
        onClick={action('onClick')}
        disabled={boolean('disabled', false)}
      />
    ))
  );
