// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import NextButton from '../../../../components/molecules/commons/Buttons/NextButton';

storiesOf('Components/molecules/commons/Buttons', module)
  .addDecorator(withKnobs)
  .add(
    'NextButton',
    withInfo(
      `
        # Description

        A Button to go next content.
      `
    )(() => (
      <NextButton
        testId={text('testId', 'unique-id')}
        text={text('text', 'NextButton')}
        onClick={action('onClick')}
        disabled={boolean('disabled', false)}
      />
    ))
  );
