// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import RefreshButton from '../../../../components/molecules/commons/Buttons/RefreshButton';

storiesOf('Components/molecules/commons/Buttons', module)
  .addDecorator(withKnobs)
  .add(
    'RefreshButton',
    withInfo(
      `
        # Description

        A Button to refresh data.
      `
    )(() => (
      <RefreshButton
        testId={text('testId', 'unique-id')}
        className={text('className', 'className')}
        onClick={action('onClick')}
        type={text('type', 'submit')}
        disabled={boolean('disabled', false)}
      />
    ))
  );
