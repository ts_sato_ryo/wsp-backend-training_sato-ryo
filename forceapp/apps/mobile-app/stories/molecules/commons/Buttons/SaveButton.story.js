// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react'; /* eslint-disable import/no-extraneous-dependencies */
import { action } from '@storybook/addon-actions';

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import SaveButton from '../../../../components/molecules/commons/Buttons/SaveButton';

storiesOf('Components/molecules/commons/Buttons', module)
  .addDecorator(withKnobs)
  .add(
    'SaveButton',
    withInfo(
      `
        # Description

        A Button to save something.
      `
    )(() => (
      <SaveButton
        testId={text('testId', 'unique-id')}
        onClick={action('onClick')}
        disabled={boolean('disabled', false)}
      />
    ))
  );
