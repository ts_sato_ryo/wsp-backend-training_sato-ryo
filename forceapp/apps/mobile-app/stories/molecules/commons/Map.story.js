// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, number } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Map from '../../../components/molecules/commons/Map';

storiesOf('Components/molecules/commons/Map', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      text: `
        # Description

        Show map
      `,
    })(() => (
      <Map
        latitude={number('latitude', 35.658581)}
        longitude={number('longitude', 139.745433)}
      />
    ))
  );
