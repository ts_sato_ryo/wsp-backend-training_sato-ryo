// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import ApprovalStatus from '../../../components/molecules/commons/ApprovalStatus';

storiesOf('Components/molecules/commons/ApprovalStatus', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        Display approval status
      `
    )(() => (
      <ApprovalStatus
        status={text('status', 'Pending')}
        iconOnly={boolean('iconOnly', false)}
        size={text('size', 'large')}
        isForReapply={boolean('isForReapply', false)}
      />
    ))
  );
