// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import DailyHeader from '../../../../../components/molecules/commons/Headers/DailyHeader';

storiesOf('Components/molecules/commons/Headers', module)
  .addDecorator(withKnobs)
  .add(
    'DailyHeader',
    withInfo({
      inline: false,
      text: `
      日付ページングがついたヘッダー
    `,
    })(() => (
      <DailyHeader
        currentDate={text('currentDate', '2018-10-01')}
        title={text('title', 'TITLE TITLE')}
        backButtonLabel={text('backButtonLabel', 'Oct 2018')}
        onClickBackMonth={action('onClickBackMonth')}
        onChangeDate={action('onChangeDate')}
        onClickPrevDate={action('onClickPrevDate')}
        onClickNextDate={action('onClickNextDate')}
      />
    ))
  );
