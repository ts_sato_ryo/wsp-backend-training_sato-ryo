// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import MonthlyHeader from '../../../../../components/molecules/commons/Headers/MonthlyHeader';

storiesOf('Components/molecules/commons/Headers', module)
  .addDecorator(withKnobs)
  .add(
    'MonthlyHeader',
    withInfo({
      inline: false,
      text: `
      月のページングがついたヘッダー
    `,
    })(() => (
      <MonthlyHeader
        currentYearMonth={text('currentYearMonth', '2018-10')}
        yearMonthOptions={[
          {
            label: '2018-09',
            value: '2018-09',
          },
          {
            label: '2018-10',
            value: '2018-10',
          },
          {
            label: '2018-11',
            value: '2018-11',
          },
        ]}
        title={text('title', 'TITLE TITLE')}
        onChangeMonth={action('onChangeMonth')}
        onClickRefresh={action('onClickRefresh')}
        onClickPrevMonth={action('onClickPrevMonth')}
        onClickNextMonth={action('onClickNextMonth')}
      />
    ))
  );
