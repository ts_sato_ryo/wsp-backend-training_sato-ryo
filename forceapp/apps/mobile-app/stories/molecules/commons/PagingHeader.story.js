// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import PagingHeader from '../../../components/molecules/commons/PagingHeader';

storiesOf('Components/molecules/commons/PagingHeader', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: false,
      text: `
      # Description

      アプリケーションのヘッダーに使われるコンポーネントです。

      # onClickPrev, onClickNext

      \`onClickPrev\` handler を使って前のページに移動する操作を制御できます。

      \`onClickPrev\` handler を使って次のページに移動する操作を制御できます。

    `,
    })(() => (
      <PagingHeader
        prevButtonLabel={text('prevButtonLabel', 'Prev')}
        nextButtonLabel={text('nextButtonLabel', 'Next')}
        disabledPrevButton={boolean('disabledPrevButton', false)}
        disabledNextButton={boolean('disabledNextButton', false)}
        onClickPrev={action('onClickPrev')}
        onClickNext={action('onClickNext')}
      >
        {text(
          'chilren',
          'title title title title title title title title title title'
        )}
      </PagingHeader>
    ))
  );
