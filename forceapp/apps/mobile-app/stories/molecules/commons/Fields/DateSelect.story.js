// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import DateSelect from '../../../../components/molecules/commons/Fields/DateSelect';

storiesOf('Components/molecules/commons/Fields/DateSelect', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        date select コンポーネント

        # Propsについて

        HTML5 非標準のinputコンポーネント。Navigationなどに使う日付選択できるコンポーネントです。
      `
    )(() => (
      <DateSelect
        testId="unique-id"
        value={text('value', '2018-10-01')}
        onChange={action('onChange')}
      />
    ))
  );
