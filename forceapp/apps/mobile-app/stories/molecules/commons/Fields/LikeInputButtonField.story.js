// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import LikeInputButtonField from '../../../../components/molecules/commons/Fields/LikeInputButtonField';

storiesOf('Components/molecules/commons/Fields/LikeInputButtonField', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        This component is used as like input button field for mobile app。
      `
    )(() => (
      <LikeInputButtonField
        placeholder={text('placeholder', 'Search')}
        onClick={action('onClick')}
        label={text('label', 'Label')}
        value={text('value', 'value')}
        disabled={boolean('disabled', false)}
        required={boolean('required', false)}
        readOnly={boolean('readOnly', false)}
      />
    ))
  );
