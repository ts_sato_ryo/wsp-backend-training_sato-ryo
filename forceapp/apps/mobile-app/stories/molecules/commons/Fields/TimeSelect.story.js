// @flow
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import TimeSelect from '../../../../components/molecules/commons/Fields/TimeSelect';

storiesOf('Components/molecules/commons/Fields/TimeSelect', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    () => (
      <TimeSelect
        value={text('value', '')}
        minValue={text('minValue', '')}
        maxValue={text('maxValue', '')}
        defaultValue={text('defaultValue', '00:00')}
        placeholder={text('placeholder', '(00:00)')}
        disabled={boolean('disabled', false)}
        readOnly={boolean('readOnly', false)}
        error={boolean('error', false)}
        icon={boolean('icon', false)}
        useTitle={boolean('useTitle', false)}
        onChange={action('onChange')}
      />
    ),
    {
      info: `Time select`,
    }
  );
