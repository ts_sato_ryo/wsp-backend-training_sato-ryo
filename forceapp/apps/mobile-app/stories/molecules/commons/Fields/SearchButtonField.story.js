// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import SearchButtonField from '../../../../components/molecules/commons/Fields/SearchButtonField';

storiesOf('Components/molecules/commons/Fields/SearchButtonField', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        This component is used like a button for searching in next page with delete function in mobile app.
      `
    )(() => (
      <SearchButtonField
        placeholder={text('placeholder', 'Search')}
        onClick={action('onClick')}
        onClickDeleteButton={action('onClickDeleteButton')}
        label={text('label', 'Label')}
        value={text('value', 'value')}
        disabled={boolean('disabled', false)}
        required={boolean('required', false)}
        readOnly={boolean('readOnly', false)}
      />
    ))
  );
