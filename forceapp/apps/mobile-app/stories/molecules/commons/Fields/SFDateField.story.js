// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import SFDateField from '../../../../components/molecules/commons/Fields/SFDateField';

storiesOf('Components/molecules/commons/Fields/DateField', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        Date Field
      `
    )(() => (
      <SFDateField
        testId={text('testId', 'unique-id')}
        required={boolean('required', false)}
        errors={[text('errors[0]', 'error')]}
        value={text('value', '2019-01-01')}
        emphasis={boolean('emphasis', false)}
        label={text('label', 'label')}
        placeholder={text('placeholder', 'placeholder')}
        disabled={boolean('disabled', false)}
        onChange={action('onChange')}
        onBlur={action('onBlur')}
        useRemoveValueButton={boolean('useRemoveValueButton', true)}
        onClickRemoveValueButton={action('onClickRemoveValueButton', true)}
      />
    ))
  );
