// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import SearchField from '../../../../components/molecules/commons/Fields/SearchField';

storiesOf('Components/molecules/commons/Fields/SearchField', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        This component is used as search field for mobile app。
      `
    )(() => (
      <SearchField
        placeHolder="Search"
        iconClick={action('onClick')}
        onChange={action('onChange')}
        label="Label"
        value="value"
      />
    ))
  );
