// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import LikeInputButton from '../../../components/molecules/commons/LikeInputButton';

storiesOf('Components/molecules/commons/LikeInputButton', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(`
          # Description

          インプットのようなボタンです。
        `)(() => (
      <LikeInputButton
        placeholder={text('placeholder', 'Like input atom.')}
        error={boolean('error', false)}
        disabled={boolean('disabled', false)}
        readOnly={boolean('readOnly', false)}
        onClick={action('onClick')}
        value={text('value', 'Like input atom.')}
      />
    ))
  );
