// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import ViewItem from '../../../components/molecules/commons/ViewItem';

storiesOf('Components/molecules/commons/ViewItem', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        要素表示用のコンポーネントです
        childrenで渡された値をそのまま表示します

        # ラベル

        \`label\` props で必ずラベルを指定して下さい。

        ## 強調ラベル

        \`emphasis\` を \`true\` にするとラベルの見出しが強調されます。
        \`required\` と併用することもできます。

        ~~~js
        <ViewItem
          label="label"
          emphasis
        />

        ~~~

      `
    )(() => (
      <ViewItem
        label={text('label', 'LABEL LABEL LABEL LABEL LABEL LABEL LABEL')}
        emphasis={boolean('emphasis', null)}
      >
        表示用
      </ViewItem>
    ))
  );
