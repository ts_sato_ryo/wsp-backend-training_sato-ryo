// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import CommentArea from '../../../components/molecules/commons/CommentArea';

import ImgSample from '../../images/sample.png';

storiesOf('Components/molecules/commons/CommentArea', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        CommentArea コンポーネント
        顔アイコンと吹き出しのセットです。

        ~~~js
        <TextArea
          error
        />
        ~~~
      `
    )(() => (
      <CommentArea
        src={ImgSample}
        alt="sample"
        value={text('value', 'Value Value Value Value')}
      />
    ))
  );
