// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Dialog from '../../../components/molecules/commons/Dialog';

storiesOf('Components/molecules/commons/Dialog', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: false,
      text: `
            # Description

            Basic Dialog
          `,
    })(() => (
      <Dialog
        title={text('title', '申請してよろしいですか')}
        content={
          <div>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit.
            Praesentium pariatur iure fuga autem iusto doloremque totam fugiat
            illo architecto nemo omnis cum veritatis, neque maiores. Atque autem
            neque molestias distinctio!
          </div>
        }
        leftButtonLabel={text('leftButtonLabel', 'キャンセル')}
        onClickLeftButton={() => {}}
        rightButtonLabel={text('rightButtonLabel', '申請する')}
        onClickRightButton={() => {}}
        onClickCloseButton={action('onClickCloseButton')}
      />
    ))
  );
