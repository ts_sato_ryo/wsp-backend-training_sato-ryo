// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Loading from '../../../components/molecules/commons/Loading';

storiesOf('Components/molecules/commons/Loading', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: false,
      text: `
    ページを開いている間・データを更新している間などに表示されるローディングです。
  `,
    })(() => (
      <Loading
        theme={text('theme', 'light')}
        text={text('text', '読み込み中')}
      />
    ))
  )
  .add(
    'Themes',
    withInfo({
      inline: false,
      text: `
    light・dark theme が選べます。
  `,
    })(() => (
      <Loading theme={text('theme', 'dark')} text={text('text', '申請中')} />
    ))
  );
