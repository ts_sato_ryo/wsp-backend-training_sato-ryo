// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Headline from '../../../components/molecules/commons/Headline';

storiesOf('Components/molecules/commons/Headline', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: false,
      text: `
            # Description

            ナビゲーションが不要なページのタイトルに使われるコンポーネントです。
            画面の最上部に固定されます。
          `,
    })(() => (
      <Headline
        title={text(
          'title',
          'title title title title title title title title title title title title title'
        )}
      />
    ))
  );
