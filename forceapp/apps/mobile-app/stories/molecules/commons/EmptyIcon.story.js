// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import EmptyIcon from '../../../components/molecules/commons/EmptyIcon';

storiesOf('Components/molecules/commons/EmptyIcon', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: false,
      text: `
      # Description
      リストが空の際に表示するでっかいアイコンです。

      # message 

      アイコンの直下に出すメッセージです。
  `,
    })(() => <EmptyIcon message={text('message', 'リストは空です')} />)
  );
