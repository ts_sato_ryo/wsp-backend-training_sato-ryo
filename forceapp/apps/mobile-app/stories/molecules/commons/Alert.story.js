// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Alert from '../../../components/molecules/commons/Alert';

storiesOf('Components/molecules/commons/Alert', module)
  .addDecorator(withKnobs)
  .add(
    'Warning',
    withInfo(
      `
        # Description

        Display Alert
      `
    )(() => (
      <Alert
        variant={text('variant', 'warning')}
        message={[text('message', 'text text text text')]}
      />
    ))
  )
  .add(
    'Attention',
    withInfo(
      `
        # Description

        Display Alert
      `
    )(() => (
      <Alert
        variant={text('variant', 'attention')}
        message={[text('message', 'text text text text')]}
      />
    ))
  )
  .add(
    'MultipleWarning',
    withInfo(
      `
        # Description

        Display Alert
      `
    )(() => (
      <Alert
        variant={text('variant', 'warning')}
        message={[
          text('message1', 'text text text text'),
          text('message2', 'text text text text'),
        ]}
      />
    ))
  )
  .add(
    'MultipleAttention',
    withInfo(
      `
        # Description

        Display Alert
      `
    )(() => (
      <Alert
        variant={text('variant', 'attention')}
        message={[
          text('message1', 'text text text text'),
          text('message2', 'text text text text'),
        ]}
      />
    ))
  );
