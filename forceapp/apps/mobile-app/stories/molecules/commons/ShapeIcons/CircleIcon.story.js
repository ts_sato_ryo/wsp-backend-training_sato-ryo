// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import CircleShapeIcon from '../../../../components/molecules/commons/ShapeIcons/CircleShapeIcon';

storiesOf('Components/molecules/commons/ShapeIcons/CircleShapeIcon', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      text: `
      # Description

      背景に円のあるアイコンです。

      # backgroundColor

      背景色です
    `,
    })(() => {
      const backgroundColor = text('backgroundColor', 'red');
      const type = text('type', 'adduser');

      return (
        <React.Fragment>
          <CircleShapeIcon
            backgroundColor={backgroundColor}
            type={type}
            size="x-large"
          />
          <CircleShapeIcon
            backgroundColor={backgroundColor}
            type={type}
            size="large"
          />
          <CircleShapeIcon
            backgroundColor={backgroundColor}
            type={type}
            size="medium"
          />
          <CircleShapeIcon
            backgroundColor={backgroundColor}
            type={type}
            size="small"
          />
          <CircleShapeIcon
            backgroundColor={backgroundColor}
            type={type}
            size="x-small"
          />
        </React.Fragment>
      );
    })
  );
