// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import CircleOutlineShapeIcon from '../../../../components/molecules/commons/ShapeIcons/CircleOutlineShapeIcon';

storiesOf(
  'Components/molecules/commons/ShapeIcons/CircleOutlineShapeIcon',
  module
)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      text: `
      # Description

      円ので囲まれたアイコンです。

      # borderColor

      線の色です。指定がなければ color を使用します。
    `,
    })(() => {
      const color = text('color', 'red');
      const borderColor = text('borderColor', 'red');
      const type = text('type', 'adduser');

      return (
        <React.Fragment>
          <CircleOutlineShapeIcon
            color={color}
            borderColor={borderColor}
            type={type}
            size="x-large"
          />
          <CircleOutlineShapeIcon
            color={color}
            borderColor={borderColor}
            type={type}
            size="large"
          />
          <CircleOutlineShapeIcon
            color={color}
            borderColor={borderColor}
            type={type}
            size="medium"
          />
          <CircleOutlineShapeIcon
            color={color}
            borderColor={borderColor}
            type={type}
            size="small"
          />
          <CircleOutlineShapeIcon
            color={color}
            borderColor={borderColor}
            type={type}
            size="x-small"
          />
        </React.Fragment>
      );
    })
  );
