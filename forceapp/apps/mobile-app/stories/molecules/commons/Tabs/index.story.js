// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, boolean, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import { Tabs, Tab } from '../../../../components/molecules/commons/Tabs';

storiesOf('Components/molecules/commons/Tabs', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        Tabs component

        ~~~js
        <Tabs>
          <Tab ... label="FOOBAR" />
          <Tab ... label="HOGE" />
          <Tab ... label="FUGA" />
        </Tabs>
        ~~~
      `
    )(() => (
      <Tabs
        position={text('position', undefined)}
        className={text('className', undefined)}
      >
        <Tab
          onClick={action('[FOOBAR] onClick')}
          label={text('[FOOBAR] label', 'FOOBAR')}
          active={boolean('[FOOBAR] active', false)}
        />
        <Tab
          onClick={action('[FUGA] onClick')}
          label={text('[FUGA] label', 'FUGA')}
          active={boolean('[FUGA] active', true)}
        />
        <Tab
          onClick={action('[HOGE] onClick')}
          label={text('[HOGE] label', 'HOGE')}
          active={boolean('[HOGE] active', false)}
        />
      </Tabs>
    ))
  );
