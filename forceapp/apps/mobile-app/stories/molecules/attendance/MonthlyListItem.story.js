// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import MonthlyListItem from '../../../components/molecules/attendance/MonthlyListItem';

import { LEAVE_TYPE } from '../../../../domain/models/attendance/LeaveType';
import { LEAVE_RANGE } from '../../../../domain/models/attendance/LeaveRange';
import { generateLeaveRequest } from './mock-data/LeaveRequest';

storiesOf('Components/molecules/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'MonthlyListItem',
    withInfo({
      text: `
            # Description

            勤務表のリストアイテムに使われるコンポーネントです。

            # Props

            - \`dayType\`: \`Workday\` | \`Holiday\` | \`LegalHoliday\`
            - \`date\`: string (YYYY-MM-DD)
            - \`startTime\`: string
            - \`endTime\`: string
            - \`workingTypeStartTime\`: string
            - \`workingTypeEndTime\`: string
            - \`onClick\`: Function
            - \`leaveRequests\`: AttDailyRequest[]
            - \`isLeaveOfAbsence\`: boolean
            - \`hasAbsenceRequest\`: boolean

            # Status and Color

            以下の条件で行の色が変化します(上に行くほど優先)

            ※注意※デザインガイドラインにない色を使用しています

            - 休職休業中(isLeaveOfAbsence)
              -> 灰縞

            - 欠勤申請がある(hasAbsenceRequest)
              - [欠勤申請]有給の午後半休がある
                -> 前半が灰縞、後半が橙縞
              - [欠勤申請]有給の午前半休、半日休、時間単位休がある
                -> 前半が橙縞、後半が灰縞
              - [欠勤申請]それ以外
                -> 灰縞

            - 所定休日、法定休日である
              -> 赤(法定休日), 青(所定休日)
              ※振替休暇の場合もここ

            - 全日休がある
            - 有給・無給が同じ午前半休と午後半休がある
            - 有給・無給が同じ半日休が2つある
              -> 橙縞または灰縞
              ※年次有給休暇は有給休暇と同じ、代休は無給休暇と同じ表示とする。以下同様

            - 全日休ではない休暇が1つある
              -> 行の半分が橙縞または灰縞になる(午前半休・半日休・時間単位休は前半部、午後半休は後半部)

            - 午前半休と午後半休があり有給・無給が異なる
              -> 橙縞と灰縞が半分ずつ

            - 午前半休または午後半休と、時間単位休がある
              -> 午前半休または午後半休を優先的に表示

            - 半日休がある
              -> 午前半休が1つある場合と同じ

            - 時間単位休がある
              -> 午前半休が1つある場合と同じ

            - 上記すべてにあてはまらない
              -> 白
          `,
    })(() => (
      <React.Fragment>
        <MonthlyListItem
          dayType={text('dayType', 'Workday')}
          date={text('date', '2018-10-10')}
          startTime={text('startTime', '9:00')}
          endTime={text('endTime', '18:00')}
          workingTypeStartTime={text('workingTypeStartTime', '9:00')}
          workingTypeEndTime={text('workingTypeEndTime', '18:00')}
          onClick={action('click')}
          requestStatus={text('requestStatus', 'Approval In')}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-11"
          startTime="9:00"
          endTime="18:00"
          workingTypeStartTime="9:00"
          workingTypeEndTime="18:00"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-12"
          workingTypeStartTime="9:00"
          workingTypeEndTime="18:00"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Holiday"
          date="2010-10-13"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="LegalHoliday"
          date="2010-10-14"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-15"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Paid, LEAVE_RANGE.Day),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-16"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Unpaid, LEAVE_RANGE.Day),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-17"
          onClick={action('click')}
          startTime="14:00"
          endTime="18:00"
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Paid, LEAVE_RANGE.AM),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-18"
          onClick={action('click')}
          workingTypeStartTime="10:00"
          workingTypeEndTime="14:00"
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Paid, LEAVE_RANGE.PM),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-19"
          onClick={action('click')}
          startTime="14:00"
          endTime="18:00"
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Unpaid, LEAVE_RANGE.AM),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-20"
          onClick={action('click')}
          workingTypeStartTime="10:00"
          workingTypeEndTime="14:00"
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Unpaid, LEAVE_RANGE.PM),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-21"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Paid, LEAVE_RANGE.AM),
            generateLeaveRequest(LEAVE_TYPE.Unpaid, LEAVE_RANGE.PM),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-22"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Unpaid, LEAVE_RANGE.AM),
            generateLeaveRequest(LEAVE_TYPE.Paid, LEAVE_RANGE.PM),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-23"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest={false}
          leaveRequests={[
            generateLeaveRequest(LEAVE_TYPE.Paid, LEAVE_RANGE.AM),
            generateLeaveRequest(LEAVE_TYPE.Paid, LEAVE_RANGE.Time),
          ]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-24"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence
          hasAbsenceRequest={false}
          leaveRequests={[]}
          attentionMessages={['test']}
        />
        <MonthlyListItem
          dayType="Workday"
          date="2010-10-25"
          onClick={action('click')}
          requestStatus={null}
          isLeaveOfAbsence={false}
          hasAbsenceRequest
          leaveRequests={[]}
          attentionMessages={['test']}
        />
      </React.Fragment>
    ))
  );
