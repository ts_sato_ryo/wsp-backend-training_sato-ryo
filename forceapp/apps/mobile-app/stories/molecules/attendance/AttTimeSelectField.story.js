// @flow
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import {
  withKnobs,
  text,
  boolean,
  number,
  array,
} from '@storybook/addon-knobs';

import AttTimeSelectField from '../../../components/molecules/attendance/AttTimeSelectField';

storiesOf('Components/molecules/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'AttTimeSelectField',
    () => (
      <AttTimeSelectField
        label={text('label', 'LABEL LABEL LABEL')}
        errors={array('errors', ['error message.'])}
        emphasis={boolean('emphasis', false)}
        value={number('value')}
        defaultValue={number('defaultValue', 0)}
        placeholder={text('placeholder', '(00:00)')}
        disabled={boolean('disabled', false)}
        readOnly={boolean('readOnly', false)}
        icon={boolean('icon', false)}
        onChange={action('onChange')}
      />
    ),
    {
      info: `Time select`,
    }
  );
