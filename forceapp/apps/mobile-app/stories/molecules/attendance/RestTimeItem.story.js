// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import RestTimeItem from '../../../components/molecules/attendance/RestTimeItem';

storiesOf('Components/molecules/attendance', module)
  .addDecorator(withKnobs)
  .addDecorator((story) => <div>{story()}</div>)
  .add(
    'RestTimeItem',
    withInfo({
      text: `
            # Description

            勤務詳細の休憩時間リストアイテムのコンポーネントです。
          `,
    })(() => (
      <RestTimeItem
        label={text('label', '休憩１')}
        startTime={{
          value: number('startTime', 9 * 60),
          onChangeValue: action('onChangeStartTime'),
        }}
        endTime={{
          value: number('endTime', 18 * 60),
          onChangeValue: action('onChangeEndTime'),
        }}
        isDisabledRemove={boolean('isDisabledRemove', false)}
        readOnly={boolean('readOnly', false)}
        onClickRemove={action('onRemove')}
      />
    ))
  );
