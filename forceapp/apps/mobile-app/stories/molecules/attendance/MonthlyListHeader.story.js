// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';

import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import MonthlyListHeader from '../../../components/molecules/attendance/MonthlyListHeader';

storiesOf('Components/molecules/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'MonthlyListHeader',
    withInfo({
      text: `
            # Description

            勤務表のリストヘッダーとコンポーネントです。
          `,
    })(() => <MonthlyListHeader />)
  );
