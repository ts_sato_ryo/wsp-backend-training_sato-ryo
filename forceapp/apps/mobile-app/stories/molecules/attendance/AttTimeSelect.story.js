// @flow
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';

import AttTimeSelect from '../../../components/molecules/attendance/AttTimeSelect';

storiesOf('Components/molecules/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'AttTimeSelect',
    () => (
      <AttTimeSelect
        value={number('value')}
        defaultValue={number('defaultValue', 0)}
        placeholder={text('placeholder', '(00:00)')}
        disabled={boolean('disabled', false)}
        readOnly={boolean('readOnly', false)}
        error={boolean('error', false)}
        icon={boolean('icon', false)}
        onChange={action('onChange')}
      />
    ),
    {
      info: `Time select`,
    }
  );
