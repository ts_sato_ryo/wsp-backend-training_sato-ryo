// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, array, boolean, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import { CODE } from '../../../../domain/models/attendance/AttDailyRequestType';
import { type LeaveRange } from '../../../../domain/models/attendance/LeaveRange';
import LeaveStartEndField from '../../../components/molecules/attendance/LeaveStartEndField';

const request = (leaveRange: LeaveRange) => ({
  type: CODE.Leave,
  leaveRange,
  isDaysLeftManaged: false,
  startDate: '2019-01-01',
  endDate: '2019-01-05',
  leaveName: '',
  leaveCode: '',
  leaveType: null,
  availableLeaveTypes: [],
  daysLeft: 10,
  hoursLeft: 100,
  id: '',
  requestTypeCode: CODE.Leave,
  requestTypeName: '休暇',
  status: 'NotRequested',
  startTime: null,
  endTime: null,
  remarks: '',
  reason: '',
  substituteLeaveType: null,
  substituteDate: '',
  directApplyRestTimes: [],
  patternCode: '',
  patternName: '',
  patternRestTimes: [],
  requireReason: false,
  originalRequestId: '',
  isForReapply: false,
  approver01Name: '',
  // FIXME: 各種申請ごとのモデルになったら削除されます。
  targetDate: '',
});

storiesOf('Components/molecules/attendance/LeaveStartEndField', module)
  .addDecorator(withKnobs)
  .add(
    'Day',
    withInfo(
      `
        # Description

        休暇範囲ごとの入力フィールド

      `
    )(() => (
      <LeaveStartEndField
        testId={text('testId', 'unique-id')}
        required={boolean('required', false)}
        readOnly={boolean('readOnly', false)}
        onChange={action('onChange')}
        request={request('Day')}
        isDaysLeftManaged={false}
      />
    ))
  )
  .add(
    'Half',
    withInfo(
      `
          # Description

          休暇範囲ごとの入力フィールド

        `
    )(() => (
      <LeaveStartEndField
        testId={text('testId', 'unique-id')}
        required={boolean('required', false)}
        readOnly={boolean('readOnly', false)}
        onChange={action('onChange')}
        request={request('Half')}
        isDaysLeftManaged={false}
      />
    ))
  )
  .add(
    'Time',
    withInfo(
      `
          # Description

          休暇範囲ごとの入力フィールド

        `
    )(() => (
      <LeaveStartEndField
        testId={text('testId', 'unique-id')}
        required={boolean('required', false)}
        readOnly={boolean('readOnly', false)}
        onChange={action('onChange')}
        request={request('Time')}
        isDaysLeftManaged={false}
      />
    ))
  )
  .add(
    'Time - isDaysLeftManaged: true',
    withInfo(
      `
            # Description

            休暇範囲ごとの入力フィールド

          `
    )(() => (
      <LeaveStartEndField
        testId={text('testId', 'unique-id')}
        required={boolean('required', false)}
        readOnly={boolean('readOnly', false)}
        onChange={action('onChange')}
        request={request('Time')}
        isDaysLeftManaged
        errors={array('errors', ['ERROR'])}
      />
    ))
  );
