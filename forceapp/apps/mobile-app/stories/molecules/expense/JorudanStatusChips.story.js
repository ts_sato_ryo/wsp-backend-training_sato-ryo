// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';

import { withKnobs, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import JorudanStatusChips from '../../../components/molecules/expense/JorudanStatusChips';

storiesOf('Components/molecules/expense/JorudanStatusChips', module)
  .addDecorator(withKnobs)
  .addDecorator((story) => <div>{story()}</div>)
  .add(
    'Basic',
    withInfo({
      text: `
            # Description

            ジョルダンで各条件を満たした際に表示されるアイコンを簡単に表示するためのコンポーネントです。
          `,
    })(() => (
      <JorudanStatusChips
        isCheapest={boolean('isCheapest', true)}
        isEarliest={boolean('isEarliest', true)}
        isMinTransfer={boolean('isMinTransfer', true)}
        verticalRow={boolean('verticalRow', false)}
      />
    ))
  );
