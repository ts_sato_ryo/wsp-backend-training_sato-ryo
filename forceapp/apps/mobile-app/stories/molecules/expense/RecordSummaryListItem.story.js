// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, number } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import records from './meta/records';

import RecordSummaryListItem from '../../../components/molecules/expense/RecordSummaryListItem';

storiesOf('Components/molecules/expense/RecordSummaryListItem', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: true,
      text: `
            # Description
            List Item for Expense mobile apps.
          `,
    })(() =>
      records.map((record) => (
        <RecordSummaryListItem
          onClick={action('onClick')}
          record={record}
          currencySymbol={text('currencySymbol', '￥')}
          currencyDecimalPlaces={number('currencyDecimalPlaces', 2)}
        />
      ))
    )
  );
