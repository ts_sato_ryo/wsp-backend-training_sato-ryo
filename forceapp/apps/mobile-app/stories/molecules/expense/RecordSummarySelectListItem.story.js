// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, number, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import records from './meta/records';

import RecordSummarySelectListItem from '../../../components/molecules/expense/RecordSummarySelectListItem';

storiesOf('Components/molecules/expense/RecordSummarySelectListItem', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: true,
      text: `
            # Description
            List Item for Expense mobile apps.
          `,
    })(() =>
      records.map((record) => (
        <RecordSummarySelectListItem
          onClick={action('onClick')}
          isSelected={boolean('isSelected', false)}
          record={record}
          currencySymbol={text('currencySymbol', '￥')}
          currencyDecimalPlaces={number('currencyDecimalPlaces', 2)}
        />
      ))
    )
  );
