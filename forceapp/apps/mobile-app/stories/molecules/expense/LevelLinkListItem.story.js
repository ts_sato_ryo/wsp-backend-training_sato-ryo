// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import LevelLinkListItem from '../../../components/molecules/expense/LevelLinkListItem';

storiesOf('Components/molecules/expense/LevelLinkListItem', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo(
      `
        # Description

        LinkListItem コンポーネント

        # Props

        \`onClick\` コンポーネント全体をクリックできるようになっております。
      `
    )(() => (
      <div>
        <div className="list-item-body">
          <LevelLinkListItem onClickBody={action('onClickBody only item')}>
            {text('children', 'LIST ITEM')}
          </LevelLinkListItem>
        </div>
        <div className="list-item-body">
          <LevelLinkListItem
            onClickBody={action('onClickBody 1st item')}
            onClickIcon={action('onClickIcon 1st item')}
          >
            LIST ITEM 1
          </LevelLinkListItem>
          <LevelLinkListItem
            onClickBody={action('onClickBody 2nd item')}
            onClickIcon={action('onClickIcon 2nd item')}
          >
            LIST ITEM 2
          </LevelLinkListItem>
          <LevelLinkListItem onClickBody={action('onClickBody 3rd item')}>
            LIST ITEM 3
          </LevelLinkListItem>
          <LevelLinkListItem onClickBody={action('onClickBody 4th item')}>
            LIST ITEM 4
          </LevelLinkListItem>
        </div>
      </div>
    ))
  );
