// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import JorudanListItem from '../../../components/molecules/expense/JorudanListItem';

storiesOf('Components/molecules/expense/JorudanListItem', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: true,
      text: `
            # Description
            List Item for Jorudan mobile apps.
          `,
    })(() => (
      <JorudanListItem
        onClick={action('onClick only item')}
        amount={text('amount', '￥11,604')}
        returnAmount={text('returnAmount', '(往復:￥23,208)')}
        routeInfo={text('routeInfo', 'JR / 私鉄 / 新幹線')}
        lineNames={['Line Name 1', 'Line Name 2']}
        fast
        cheap
        easy
      />
    ))
  );
