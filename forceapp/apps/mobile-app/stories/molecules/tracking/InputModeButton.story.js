// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, text } from '@storybook/addon-knobs';

import InputModeButton from '../../../components/molecules/tracking/InputModeButton';

storiesOf('Components/molecules/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'InputModeButton',
    withInfo(
      `
        # Description

        Switch input mode for trakcing t
    `
    )(() => (
      <InputModeButton
        value={text('value', 'ratio')}
        onClick={action('onClick')}
      />
    ))
  );
