// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import AddJobButton from '../../../components/molecules/tracking/AddJobButton';

storiesOf('Components/molecules/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'AddJobButton',
    withInfo({
      inline: false,
      style: {},
      text: `
              # Description

              Button to add a job to task list
          `,
    })(() => (
      <AddJobButton
        onClick={action('onClick')}
        floating={boolean('floating', true)}
      />
    ))
  );
