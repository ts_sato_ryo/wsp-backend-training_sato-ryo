// @flow
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import TaskTimeSelect from '../../../components/molecules/tracking/TaskTimeSelect';

storiesOf('Components/molecules/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'TaskTimeSelect',
    () => (
      <TaskTimeSelect
        value={text('value', '')}
        defaultValue={text('defaultValue', '00:00')}
        placeholder={text('placeholder', '(00:00)')}
        disabled={boolean('disabled', false)}
        readOnly={boolean('readOnly', false)}
        error={boolean('error', false)}
        icon={boolean('icon', false)}
        onChange={action('onChange')}
      />
    ),
    {
      info: `Time select`,
    }
  );
