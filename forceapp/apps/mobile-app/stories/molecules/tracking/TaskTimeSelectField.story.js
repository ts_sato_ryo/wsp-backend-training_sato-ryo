// @flow
import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, boolean, array } from '@storybook/addon-knobs';

import TaskTimeSelectField from '../../../components/molecules/tracking/TaskTimeSelectField';

storiesOf('Components/molecules/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'TaskTimeSelectField',
    () => (
      <TaskTimeSelectField
        label={text('label', 'LABEL LABEL LABEL')}
        errors={array('errors', ['error message.'])}
        emphasis={boolean('emphasis', false)}
        value={text('value')}
        defaultValue={text('defaultValue', '00:00')}
        placeholder={text('placeholder', '(00:00)')}
        disabled={boolean('disabled', false)}
        readOnly={boolean('readOnly', false)}
        icon={boolean('icon', false)}
        onChange={action('onChange')}
      />
    ),
    {
      info: `Time select`,
    }
  );
