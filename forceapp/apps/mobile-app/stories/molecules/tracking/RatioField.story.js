// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, number } from '@storybook/addon-knobs';

import RatioField from '../../../components/molecules/tracking/RatioField';

storiesOf('Components/molecules/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'RadioField',
    withInfo(
      `
        # Description

        Input field for entering trakcing time as ratio.
    `
    )(() => (
      <RatioField value={number('value', 10)} onBlur={action('onBlur')} />
    ))
  );
