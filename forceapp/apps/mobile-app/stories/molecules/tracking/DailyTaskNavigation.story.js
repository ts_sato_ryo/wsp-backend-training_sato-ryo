// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import DailyTaskNavigation from '../../../components/molecules/tracking/DailyTaskNavigation';

storiesOf('Components/molecules/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'DailyTaskNavigation',
    withInfo({
      inline: false,
      text: `
        AAAA
    `,
    })(() => (
      <DailyTaskNavigation
        today={text('today', '2018-10-02')}
        listEditing={false}
        onClickMonthlySummary={action('onClickMonthlySummary')}
        onChangeDate={action('onChangeDate')}
        onClickPrevDate={action('onChangePrevDate')}
        onClickNextDate={action('onChangeNextDate')}
        onToggleEditing={action('onToggleEditing')}
        onClickRefresh={action('onClickRefresh')}
      />
    ))
  );
