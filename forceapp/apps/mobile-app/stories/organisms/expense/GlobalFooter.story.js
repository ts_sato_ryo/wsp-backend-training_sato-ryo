// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs } from '@storybook/addon-knobs';

import GlobalFooter from '../../../components/organisms/expense/GlobalFooter';

storiesOf('Components/organisms/expense/GlobalFooter', module)
  .addDecorator(withKnobs)
  .add(
    'Basic',
    withInfo({
      inline: false,
      text: `
            expenseのglobal footerです。
    `,
    })(() => (
      <GlobalFooter
        onClickReportList={action('onClick report list')}
        onClickRecordList={action('onClick record list')}
      />
    ))
  );
