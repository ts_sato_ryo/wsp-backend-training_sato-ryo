// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import DailyLayout, {
  TABS,
} from '../../../components/organisms/attendance/DailyLayout';

storiesOf('Components/organisms/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'DailyLayout',
    withInfo({
      inline: false,
      text: `
        # Description

        Common layout for working input and daily request page.
      `,
    })(() => (
      <DailyLayout
        tab={TABS.request}
        title={text('title', 'TITLE')}
        currentDate={text('currentDate', '2019-01-10')}
        startDate={text('currentDate', '2019-01-01')}
        onChangeDate={action('onChangeDate')}
        onClickBackMonth={action('onClickBackMonth')}
        onClickPrevDate={action('onClickPrevDate')}
        onClickNextDate={action('onClickNextDate')}
        onClickTimesheetDaily={action('onClickTimesheetDaily')}
        onClickDailyRequest={action('onClickDailyRequest')}
        onClickRefresh={action('onClickRefresh')}
      >
        <div>STORYBOOK</div>
      </DailyLayout>
    ))
  );
