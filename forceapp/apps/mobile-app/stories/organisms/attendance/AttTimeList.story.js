// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean, number } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import AttTimeList from '../../../components/organisms/attendance/AttTimeList';

storiesOf('Components/organisms/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'AttTimeList',
    withInfo({
      inline: false,
      text: `
      勤務時間一覧
    `,
    })(() => (
      <AttTimeList
        workingTime={{
          from: {
            value: number('startTime'),
            onChangeValue: action('onChangeStartTime'),
          },
          to: {
            value: number('endTime'),
            onChangeValue: action('onChangeEndTime'),
          },
        }}
        restTimes={{
          value: [
            {
              startTime: number('restStartTime'),
              endTime: number('restEndTime'),
            },
          ],
          min: number('minRestTimesCount'),
          max: number('maxRestTimesCount'),
          onChangeValueStartTime: action('onChangeRestTimeStartTime'),
          onChangeValueEndTime: action('onChangeRestTimeEndTime'),
          onClickRemove: action('onClickRemoveRestTime'),
          onClickAdd: action('onClickAddRestTime'),
        }}
        otherRestTime={
          boolean('isShowOtherRestTime', true)
            ? {
                value: number('otherRestTime'),
                onChange: action('onChangeOtherRestTime'),
              }
            : undefined
        }
        readOnly={boolean('readOnly', false)}
      />
    ))
  );
