// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean, number, text } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import * as props from './meta';
import DailyDetailList from '../../../../components/organisms/attendance/DailyDetailList';

storiesOf('Components/organisms/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'DailyDetailList',
    withInfo({
      inline: false,
      text: `
      勤務詳細
    `,
    })(() => (
      <DailyDetailList
        startTime={number('startTime', props.startTime)}
        endTime={number('endTime', props.endTime)}
        otherRestTime={number('otherRestTime', props.restHours)}
        restTimes={props.restTimes}
        remarks={text('remarks', props.remarks)}
        contractedDetail={props.contractedDetail}
        readOnly={boolean('readOnly', false)}
        isShowOtherRestTime={boolean('isShowOtherRestTime', false)}
        onChangeStartTime={action('onChangeStartTime')}
        onChangeEndTime={action('onChangeEndTime')}
        onChangeRestTimeStartTime={action('onChangeRestTimeStartTime')}
        onChangeRestTimeEndTime={action('onChangeRestTimeEndTime')}
        onClickRemoveRestTime={action('onClickRemoveRestTime')}
        onClickAddRestTime={action('onClickAddRestTime')}
        onChangeOtherRestTime={action('onChangeOtherRestTime')}
        onChangeRemarks={action('onChangeRemarks')}
      />
    ))
  );
