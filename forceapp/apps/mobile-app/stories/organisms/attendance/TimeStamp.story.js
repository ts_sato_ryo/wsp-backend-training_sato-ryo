// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import TimeStamp from '../../../components/organisms/attendance/TimeStamp';

storiesOf('Components/organisms/attendance', module)
  .addDecorator(withKnobs)
  .add(
    'TimeStamp',
    withInfo({
      inline: false,
      text: `
        # Description
        勤怠打刻コンポーネント

        # Props
        - fetchStatus: 'Success' | 'Failure' | 'None'
        - defaultAction: 'in' | 'out' | 'rein'
      `,
    })(() => (
      <TimeStamp
        timeLocale={text('locale', 'ja')}
        onClickStartStampButton={action('onClickStartStampButton')}
        onClickEndStampButton={action('onClickEndStampButton')}
        isEnableStartStamp={boolean('isEnableStartStamp', true)}
        isEnableEndStamp={boolean('isEnableEndStamp', true)}
        isEnableRestartStamp={boolean('isEnableRestartStamp', false)}
        defaultAction={text('defaultAction', 'in')}
        showLocationToggleButton={boolean('showLocationToggleButton', true)}
        onClickToggleButton={action('onClickToggleButton')}
        willSendLocation={boolean('willSendLocation', true)}
        fetchStatus={text('fetchStatus', 'Success')}
        locationFetchTime={number('locationFetchTime', 1541760572975)}
        latitude={number('latitude', 35.658581)}
        longitude={number('longitude', 139.745433)}
        message={text('message', 'Good morning!')}
        onChangeMessageField={action('onChangeMessageField')}
      />
    ))
  );
