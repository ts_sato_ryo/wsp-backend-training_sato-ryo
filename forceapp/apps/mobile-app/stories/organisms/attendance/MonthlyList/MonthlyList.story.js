// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';

import * as props from './meta';
import MonthlyList from '../../../../components/organisms/attendance/MonthlyList';

storiesOf('Components/organisms/attendance', module).add(
  'MonthlyList',
  withInfo({
    inline: false,
    text: `
      勤務表の内容
    `,
  })(() => <MonthlyList {...props} onClickItem={action('onClickListItem')} />)
);
