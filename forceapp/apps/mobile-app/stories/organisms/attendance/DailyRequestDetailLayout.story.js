// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import store from './store.mock';

import { defaultValue } from '../../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';

import DailyRequestDetailLayout from '../../../components/organisms/attendance/DailyRequestDetailLayout';

import ImgSample from '../../images/sample.png';

storiesOf('Components/organisms/attendance', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'DailyReuqestDetailLayout',
    withInfo({
      inline: false,
      text: `
        # Description

        各種勤怠申請詳細画面
    `,
    })(() => {
      return (
        <DailyRequestDetailLayout
          isLocked={boolean('isLocked', false)}
          isEditing={boolean('isEditing', false)}
          target={{
            ...defaultValue,
            requestTypeName: text('requestTypeName', 'NONE'),
            requestTypeCode: text('requestTypeCode', 'NONE'),
          }}
          editAction={text('editAction', 'None')}
          disableAction={text('disableAction', 'None')}
          approvalHistories={[
            {
              id: 'ID001',
              stepName: 'STEP NAME',
              approveTime: '2019-01-01 00:00',
              status: '',
              statusLabel: 'STATUS LABEL',
              approverName: 'APPROVER NAME',
              actorName: 'ACTOR NAME',
              actorPhotoUrl: ImgSample,
              comment: 'COMMENT',
              isDelegated: false,
            },
            {
              id: 'ID002',
              stepName: 'STEP NAME',
              approveTime: '2019-01-01 00:00',
              status: '',
              statusLabel: 'STATUS LABEL',
              approverName: 'APPROVER NAME',
              actorName: 'ACTOR NAME',
              actorPhotoUrl: ImgSample,
              comment: 'COMMENT',
              isDelegated: false,
            },
            {
              id: 'ID003',
              stepName: 'STEP NAME',
              approveTime: '2019-01-01 00:00',
              status: '',
              statusLabel: 'STATUS LABEL',
              approverName: 'APPROVER NAME',
              actorName: 'ACTOR NAME',
              actorPhotoUrl: ImgSample,
              comment: 'COMMENT',
              isDelegated: false,
            },
          ]}
          onClickBack={action('onClickBack')}
          onClickStartEditing={action('onClickStartEditing')}
          onClickCancelEditing={action('onClickCancelEditing')}
          onClickCreate={action('onClickCreate')}
          onClickModify={action('onClickModify')}
          onClickReapply={action('onClickReapply')}
          onClickCancelRequest={action('onClickCancelRequest')}
          onClickCancelApproval={action('onClickCancelApproval')}
          onClickRemove={action('onClickRemove')}
        >
          <div>STORYBOOK</div>
        </DailyRequestDetailLayout>
      );
    })
  );
