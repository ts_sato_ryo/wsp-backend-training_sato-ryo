// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, array } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import DailyTaskGraphCard from '../../../components/organisms/tracking/DailyTaskGraphCard';

storiesOf('Components/organisms/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'DailyTaskGraphCard',
    withInfo({
      text: `
      A graph showing tracked time and real work time.
    `,
    })(() => (
      <DailyTaskGraphCard
        realWorkTime={540}
        taskTimes={array('taskTimes', [
          {
            color: '#0083b6',
            value: 200,
          },
          {
            color: '#00a3df',
            value: 200,
          },
          {
            color: '#00b3df',
            value: 130,
          },
        ])}
      />
    ))
  )
  .add(
    'DailyTaskGraphCard - No gap',
    withInfo({
      text: `
      A graph showing tracked time and real work time.
    `,
    })(() => (
      <DailyTaskGraphCard
        realWorkTime={540}
        taskTimes={array('taskTimes', [
          {
            color: '#0083b6',
            value: 200,
          },
          {
            color: '#00a3df',
            value: 200,
          },
          {
            color: '#00b3df',
            value: 140,
          },
        ])}
      />
    ))
  )
  .add(
    'DailyTaskGraphCard - Overwork',
    withInfo({
      text: `
        A graph showing tracked time and real work time.
      `,
    })(() => (
      <DailyTaskGraphCard
        realWorkTime={1200}
        taskTimes={array('taskTimes', [
          {
            color: '#0083b6',
            value: 510,
          },
          {
            color: '#00a3df',
            value: 370,
          },
          {
            color: '#00b3df',
            value: 900,
          },
        ])}
      />
    ))
  );
