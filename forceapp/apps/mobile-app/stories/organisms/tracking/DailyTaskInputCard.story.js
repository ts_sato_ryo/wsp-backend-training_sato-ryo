// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, text, number, boolean } from '@storybook/addon-knobs';

import DailyTaskInputCard from '../../../components/organisms/tracking/DailyTaskInputCard';

storiesOf('Components/organisms/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'InputTrackingCard',
    withInfo(
      `
      # Description

      工数を入力するカード
      `
    )(() => (
      <DailyTaskInputCard
        listEditing={false}
        workCategoryName={text('job', 'V5勤怠')}
        job={text('jobType', '開発')}
        code={text('code', 'WSP-00000002')}
        timeOrRatioValue={text('timeOrRatioValue', 'ratio')}
        onClickDeleteButton={action('onClickDeleteButton')}
        onClickInputModeButton={action('onClickInputModeButton')}
        onChangeTaskTime={action('onChangeTaskTime')}
        onChangeRatio={action('onChangeRatio')}
        ratio={number('ratio', 30)}
        taskTime={number('taskTime', 150)}
        isDefaultJob={boolean('isDefaultJob', true)}
        defaultJobExists={boolean('defaultJobExists', true)}
        multipleRatiosExist={boolean('multipleRatiosExist', true)}
        realWorkTimeExists={boolean('realWorkTimeExists', true)}
      />
    ))
  )
  .add(
    'InputTrackingCard - List Editing',
    withInfo(
      `
      # Description

      In Edit mode
    `
    )(() => (
      <DailyTaskInputCard
        listEditing
        workCategoryName={text('job', 'V5勤怠')}
        job={text('jobType', '開発')}
        code={text('code', 'WSP-00000002')}
        timeOrRatioValue={text('timeOrRatioValue', 'ratio')}
        onClickDeleteButton={action('onClickDeleteButton')}
        onClickInputModeButton={action('onClickInputModeButton')}
        onChangeTaskTime={action('onChangeTaskTime')}
        onChangeRatio={action('onChangeRatio')}
        ratio={number('ratio', 30)}
        taskTime={number('taskTime', 150)}
        isDefaultJob={boolean('isDefaultJob', true)}
        defaultJobExists={boolean('defaultJobExists', true)}
        multipleRatiosExist={boolean('multipleRatiosExist', true)}
        realWorkTimeExists={boolean('realWorkTimeExists', false)}
      />
    ))
  );
