// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';

import { withKnobs, boolean } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import HistoryList from '../../../components/organisms/approval/HistoryList';

import ImgSample from '../../images/sample.png';

const historyList = [
  {
    id: 'ID001',
    stepName: 'STEP NAME',
    approveTime: '2019-01-01 00:00',
    status: '',
    statusLabel: 'STATUS LABEL',
    approverName: 'APPROVER NAME',
    actorName: 'ACTOR NAME',
    actorPhotoUrl: ImgSample,
    comment: 'COMMENT',
    isDelegated: false,
  },
  {
    id: 'ID002',
    stepName: 'STEP NAME',
    approveTime: '2019-01-01 00:00',
    status: '',
    statusLabel: 'STATUS LABEL',
    approverName: 'APPROVER NAME',
    actorName: 'ACTOR NAME',
    actorPhotoUrl: ImgSample,
    comment: 'COMMENT',
    isDelegated: false,
  },
  {
    id: 'ID003',
    stepName: 'STEP NAME',
    approveTime: '2019-01-01 00:00',
    status: '',
    statusLabel: 'STATUS LABEL',
    approverName: 'APPROVER NAME',
    actorName: 'ACTOR NAME',
    actorPhotoUrl: ImgSample,
    comment: 'COMMENT',
    isDelegated: false,
  },
];

storiesOf('Components/organisms/approval', module)
  .addDecorator(withKnobs)
  .addDecorator((story) => <div>{story()}</div>)
  .add(
    'HistoryList',
    withInfo({
      text: `
            # Description

            承認履歴に使われれる行です。
          `,
    })(() => (
      <HistoryList
        historyList={
          boolean('isShowSeeMore', true) ? historyList : [historyList[0]]
        }
      />
    ))
  );
