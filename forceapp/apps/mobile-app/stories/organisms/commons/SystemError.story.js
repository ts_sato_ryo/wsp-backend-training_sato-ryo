// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, boolean, text } from '@storybook/addon-knobs';

import SystemError from '../../../components/organisms/commons/SystemError';

storiesOf('Components/organisms', module)
  .addDecorator(withKnobs)
  .add(
    'SystemError',
    withInfo({
      inline: false,
      text: `
      Display system error
    `,
    })(() => (
      <SystemError
        continue={boolean('continue', false)}
        message={text(
          'message',
          'MESSAGE MESSAGE MESSAGE MESSAGE MESSAGE \n (THIS IS FATAL ERROR. THIS IS FATAL ERROR.)'
        )}
        solution={text(
          'solution',
          'SOLUTION SOLUTION SOLUTION SOLUTION SOLUTION SOLUTION SOLUTION'
        )}
        showError={boolean('showError', true)}
        resetError={action('resetError')}
      />
    ))
  );
