// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, text } from '@storybook/addon-knobs';

import SystemAlert from '../../../components/organisms/commons/SystemAlert';

storiesOf('Components/organisms', module)
  .addDecorator(withKnobs)
  .add(
    'SystemAlert',
    () => {
      let fire = boolean('fire', false);
      const message = text(
        'message',
        'MESSAGE MESSAGE MESSAGE MESSAGE MESSAGE'
      );

      return (
        <SystemAlert
          message={fire ? message : ''}
          callback={() => {
            fire = false;
          }}
        />
      );
    },
    {
      info: `Display system alert `,
    }
  );
