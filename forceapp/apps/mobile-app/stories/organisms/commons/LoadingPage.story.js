// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import LoadingPage from '../../../components/organisms/commons/LoadingPage';

storiesOf('Components/organisms', module)
  .addDecorator(withKnobs)
  .add(
    'LoadingPage',
    withInfo({
      inline: false,
      text: `
      ページの表示を待っている間に表示されるコンテンツ
    `,
    })(() => <LoadingPage isShowing={boolean('isShowing', true)} />)
  );
