// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, boolean, text } from '@storybook/addon-knobs';

import SystemConfirm from '../../../components/organisms/commons/SystemConfirm';

storiesOf('Components/organisms', module)
  .addDecorator(withKnobs)
  .add(
    'SystemConfirm',
    () => {
      let fire = boolean('fire', false);
      const message = text(
        'message',
        'MESSAGE MESSAGE MESSAGE MESSAGE MESSAGE'
      );

      return (
        <SystemConfirm
          message={fire ? message : ''}
          callback={(_: boolean) => {
            fire = false;
          }}
        />
      );
    },
    {
      info: `Display system alert `,
    }
  );
