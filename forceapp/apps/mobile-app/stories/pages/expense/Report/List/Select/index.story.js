// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';

import { withKnobs, boolean } from '@storybook/addon-knobs';

import records from '../../../../../molecules/expense/meta/records';

import ReportListSelectPage from '../../../../../../components/pages/expense/Record/List/Select';

const userSetting = { currencySymbol: '￥', currencyDecimalPlaces: 2 };

storiesOf('Components/pages/expense/Report/List/Select', module)
  .addDecorator(withKnobs)
  .add(
    '0',
    withInfo({
      inline: false,
      text: `
      No items in report.
    `,
    })(() => (
      <ReportListSelectPage
        userSetting={userSetting}
        canCreateReport={false}
        records={[]}
        flagsById={{}}
        onClickItem={action('onClickItem')}
        onClickCancel={action('onClickCancel')}
        onClickCreateReport={action('onClickCreateReport')}
      />
    ))
  )
  .add(
    'all',
    withInfo({
      inline: false,
      text: `
      All items in report.
    `,
    })(() => (
      <ReportListSelectPage
        userSetting={userSetting}
        flagsById={{}}
        canCreateReport={boolean('canCreateReport', true)}
        records={records}
        onClickItem={action('onClickItem')}
        onClickCancel={action('onClickCancel')}
        onClickCreateReport={action('onClickCreateReport')}
      />
    ))
  );
