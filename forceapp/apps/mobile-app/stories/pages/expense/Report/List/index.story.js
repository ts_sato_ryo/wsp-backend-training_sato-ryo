// @flow

import React from 'react';
import { MemoryRouter } from 'react-router';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';

import records from '../../../../molecules/expense/meta/records';
import ReportListIndexPage from '../../../../../components/pages/expense/Record/List';

const store = createStore(() => {});

const userSetting = { currencySymbol: '￥', currencyDecimalPlaces: 2 };

storiesOf('Components/pages/expense/Report/List/index', module)
  .addDecorator((story) => (
    <Provider store={store}>
      <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
    </Provider>
  ))
  .add(
    '0',
    withInfo({
      inline: false,
      text: `
      No items in report.
    `,
    })(() => (
      <ReportListIndexPage
        userSetting={userSetting}
        records={[]}
        isShowReportTypeSelection={false}
        accountingPeriodList={[]}
        accountingDate="date"
        expReportTypeList={[]}
        onClickRecord={action('onClickItem')}
        onClickCreateReport={action('onClickCreateReport')}
        onChangeAccountingPeriod={action('onChangeAccountingPeriod')}
        onChangeAccountingDate={action('onChangeAccountingDate')}
        onClickCloseReportTypeSelection={action('onClickCloseDialog')}
        onClickNext={action('onClickCloseDialog')}
      />
    ))
  )
  .add(
    'all',
    withInfo({
      inline: false,
      text: `
      All items in report.
    `,
    })(() => (
      <ReportListIndexPage
        userSetting={userSetting}
        records={records}
        isShowReportTypeSelection={false}
        accountingPeriodList={[]}
        accountingDate="date"
        expReportTypeList={[]}
        onClickRecord={action('onClickItem')}
        onClickCreateReport={action('onClickCreateReport')}
        onChangeAccountingPeriod={action('onChangeAccountingPeriod')}
        onChangeAccountingDate={action('onChangeAccountingDate')}
        onClickCloseReportTypeSelection={action('onClickCloseDialog')}
        onClickNext={action('onClickCloseDialog')}
      />
    ))
  );
