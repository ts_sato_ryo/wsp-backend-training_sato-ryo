// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import {
  withKnobs,
  array,
  boolean,
  number,
  text,
} from '@storybook/addon-knobs';

import ReportDetail from '../../../../../components/pages/expense/Report/Detail';

storiesOf('Components/pages/expense', module)
  .addDecorator(withKnobs)
  .add(
    'ReportDetail',
    withInfo({
      inline: false,
      styles: (stylesheet) => ({
        ...stylesheet,
        button: {
          ...stylesheet.button,
          topRight: {
            display: 'none',
          },
        },
      }),
      text: `
            # Description

            Detail of Expense Report
          `,
    })(() => (
      <ReportDetail
        onClickRecord={action('onClickRecord')}
        onClickSubmit={action('onClickSubmit')}
        onClickReportList={action('onClickReportList')}
        onClickDelete={action('onClickDelete')}
        onClickEdit={action('onClickEdit')}
        subject={text('subject', 'Report at Nov.')}
        status={text('status', 'Pending')}
        reportNo={text('reportNo', 'exp00000')}
        reportTypeName={text('reportTypeName', 'One-day business trip')}
        accountingPeriod={text('accountingPeriod', '2018-01-01 - 2018-12-31')}
        accountingDate={text('accountingDate', '2018-12-06')}
        useFileAttachment={boolean('useFileAttachment', false)}
        costCenterCode={text('costCenterCode', 'PD-1')}
        costCenterName={text('costCenterName', 'Product & Developer')}
        jobCode={text('jobCode', 'PRJ-A')}
        jobName={text('jobName', 'Project A')}
        remarks={text(
          'remarks',
          'This is remarks. this is remarks. this is remarks.'
        )}
        currencySymbol="￥"
        currencyDecimalPlaces={0}
        isEditable={boolean('isEditable', true)}
        extendedItemTexts={array('extendedItemTexts', [
          {
            name: 'Extended Item Text 1',
            value: 'Value 1',
          },
          {
            name: 'Extended Item Text 2',
            value: 'Value 2',
          },
          {
            name: 'Extended Item Text 3',
            value: 'Value 3',
          },
        ])}
        extendedItemPicklists={array('extendedItemPicklists', [
          {
            name: 'Extended Item Picklist 1',
            value: 'Value 1',
          },
          {
            name: 'Extended Item Picklist 2',
            value: 'Value 2',
          },
          {
            name: 'Extended Item Picklist 3',
            value: 'Value 3',
          },
        ])}
        extendedItemLookup={array('extendedItemLookup', [
          {
            name: 'Extended Item Lookup 1',
            value: 'Value 1',
          },
          {
            name: 'Extended Item Lookup 2',
            value: 'Value 2',
          },
          {
            name: 'Extended Item Lookup 3',
            value: 'Value 3',
          },
        ])}
        extendedItemDate={array('extendedItemDate', [
          {
            name: 'Extended Item Date 1',
            value: 'Value 1',
          },
          {
            name: 'Extended Item Date 2',
            value: 'Value 2',
          },
          {
            name: 'Extended Item Date 3',
            value: 'Value 3',
          },
        ])}
        totalAmountOfRecords={number('amountOfRecords', '6000')}
        records={array('records', [
          {
            recordDate: '2017-08-24',
            currencyName: 'jp',
            recordType: 'transition',
            amount: '2000',
            items: [
              {
                expTypeName: 'AAAA',
                currencyInfo: {
                  symbol: '￥',
                  decimalPlaces: 0,
                },
              },
            ],
          },
          {
            recordDate: '2017-08-24',
            currencyName: 'jp',
            recordType: 'transition',
            amount: '2000',
            items: [
              {
                expTypeName: 'AAAA',
                currencyInfo: {
                  symbol: '￥',
                  decimalPlaces: 0,
                },
              },
            ],
          },
          {
            recordDate: '2017-08-24',
            currencyName: 'jp',
            recordType: 'transition',
            amount: '2000',
            items: [
              {
                expTypeName: 'AAAA',
                currencyInfo: {
                  symbol: '￥',
                  decimalPlaces: 0,
                },
              },
            ],
          },
        ])}
      />
    ))
  );
