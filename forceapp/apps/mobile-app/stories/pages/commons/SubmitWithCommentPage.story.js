// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, text } from '@storybook/addon-knobs';

import SubmitWithCommentPage from '../../../components/pages/commons/SubmitWithCommentPage';

storiesOf('Components/pages/commons', module)
  .addDecorator(withKnobs)
  .add(
    'SubmitWithCommentPage',
    withInfo({
      inline: false,
      styles: (stylesheet) => ({
        ...stylesheet,
        button: {
          ...stylesheet.button,
          topRight: {
            display: 'none',
          },
        },
      }),
      text: `
            # Description

            Common page for submitting request/report/approval with comment.
          `,
    })(() => (
      <SubmitWithCommentPage
        title={text('title', 'TITLE TITLE TITLE TITLE TITLE')}
        avatarUrl={text(
          'avatarUrl',
          'https://images.pexels.com/photos/89775/dog-hovawart-black-pet-89775.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'
        )}
        submitLabel={text('submitLabel', 'Submit')}
        getBackLabel={action('getBackLabel')}
        onClickBack={action('onClickBack')}
        onClickSubmit={action('onClickSubmit')}
      />
    ))
  );
