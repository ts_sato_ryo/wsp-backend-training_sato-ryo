// @flow

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';

import { items as records } from '../../organisms/attendance/MonthlyList/meta';
import TimesheetMonthlyPage from '../../../components/pages/attendance/TimesheetMonthlyPage';

storiesOf('Components/pages/attendance', module).add(
  'TimesheetMonthlyPage',
  withInfo({
    inline: false,
    text: `
      勤務表
    `,
  })(() => (
    <TimesheetMonthlyPage
      currentDate="2018-10-10"
      yearMonthOptions={[
        { value: '2018-09-01', label: '2018-09' },
        { value: '2018-10-01', label: '2018-10' },
        { value: '2018-11-01', label: '2018-11' },
      ]}
      records={records}
      onChangeMonth={action('onChageMonth')}
      onClickMonthlyListItem={action('onClickMonthlyListItem')}
      onClickRefresh={action('onClickRefresh')}
      onClickPrevMonth={action('onClickPrevMonth')}
      onClickNextMonth={action('onClickNextMonth')}
    />
  ))
);
