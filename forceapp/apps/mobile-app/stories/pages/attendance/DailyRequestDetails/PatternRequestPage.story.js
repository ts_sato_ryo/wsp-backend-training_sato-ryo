// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';

import store from '../store.mock';

import STATUS from '../../../../../domain/models/approval/request/Status';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';
import { defaultValue } from '../../../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';

import PatternRequestPage from '../../../../components/pages/attendance/DailyRequestDetails/PatternRequestPage';

const patterns = [
  {
    code: 'A',
    name: 'Pattern A',
    startTime: 9 * 60,
    endTime: 18 * 60,
    restTimes: [
      {
        startTime: 12 * 60,
        endTime: 13 * 60,
      },
      {
        startTime: 15 * 60,
        endTime: 15 * 60 + 15,
      },
    ],
  },
  {
    code: 'B',
    name: 'Pattern B',
    startTime: 9 * 60,
    endTime: 12 * 60,
    restTimes: [
      {
        startTime: 10 * 60,
        endTime: 10 * 60 + 15,
      },
      {
        startTime: 11 * 60,
        endTime: 11 * 60 + 15,
      },
    ],
  },
  {
    code: 'C',
    name: 'Pattern C',
    startTime: 12 * 60,
    endTime: 15 * 60,
    restTimes: [
      {
        startTime: 13 * 60,
        endTime: 13 * 60 + 15,
      },
      {
        startTime: 14 * 60,
        endTime: 14 * 60 + 15,
      },
    ],
  },
];

storiesOf('Components/pages/attendance/DailyRequestDatails', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'PatternRequestPage',
    withInfo({
      inline: false,
      text: `
        # Description

        勤務時間変更申請詳細画面
    `,
    })(() => {
      return (
        <PatternRequestPage
          readOnly={boolean('readOnly', false)}
          request={{
            ...defaultValue,
            type: CODE.Pattern,
            status: text('state', STATUS.NotRequested),
            requestTypeCode: CODE.Pattern,
            requestTypeName: text('requestTypeName', 'Pattern'),
            startDate: text('startDate', '2018-01-01'),
            patternName: 'A',
            patternCode: text('patternCode', 'A'),
            startTime: 9 * 60,
            endTime: 18 * 60,
            patternRestTimes: patterns[0].restTimes,
            availablePatterns: patterns,
            remarks: text('remarks', ''),
          }}
          validation={{}}
          patternOptions={patterns.map(({ code, name }) => ({
            label: name,
            value: code,
          }))}
          onChangeStartDate={action('onChangeStartDate')}
          onChangeEndDate={action('onChangeEndDate')}
          onChangePatternCode={action('onChagePatternCode')}
          onChangeRemarks={action('onChangeRemarks')}
        />
      );
    })
  );
