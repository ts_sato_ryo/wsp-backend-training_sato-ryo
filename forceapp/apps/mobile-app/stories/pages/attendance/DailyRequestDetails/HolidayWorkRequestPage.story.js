// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, number, text, boolean } from '@storybook/addon-knobs';

import store from '../store.mock';

import msg from '../../../../../commons/languages';

import STATUS from '../../../../../domain/models/approval/request/Status';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';
import { defaultValue } from '../../../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';
import { SUBSTITUTE_LEAVE_TYPE } from '../../../../../domain/models/attendance/SubstituteLeaveType';

import HolidayWorkRequestPage from '../../../../components/pages/attendance/DailyRequestDetails/HolidayWorkRequestPage';

storiesOf('Components/pages/attendance/DailyRequestDatails', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'HolidayWorkRequestPage',
    withInfo({
      inline: false,
      text: `
        # Description

        休日出勤申請詳細画面
    `,
    })(() => {
      return (
        <HolidayWorkRequestPage
          readOnly={boolean('readOnly', false)}
          request={{
            ...defaultValue,
            type: CODE.HolidayWork,
            status: text('state', STATUS.NotRequested),
            requestTypeCode: CODE.HolidayWork,
            requestTypeName: text('requestTypeName', 'HolidayWork'),
            startDate: text('startDate', '2018-01-01'),
            startTime: number('startTime', 0),
            endTime: number('endTime', 0),
            availableSubstituteLeaveTypeList: Object.keys(
              SUBSTITUTE_LEAVE_TYPE
            ),
            substituteLeaveType: text(
              'substituteLeaveType',
              SUBSTITUTE_LEAVE_TYPE.Substitute
            ),
            substituteDate: text('substituteDate', '2018-01-01'),
            remarks: text('remarks', ''),
          }}
          typeOptions={[
            {
              label: msg().Att_Lbl_DoNotUseReplacementDayOff,
              value: SUBSTITUTE_LEAVE_TYPE.None,
            },
            {
              label: msg().Att_Lbl_Substitute,
              value: SUBSTITUTE_LEAVE_TYPE.Substitute,
            },
          ]}
          onChangeStartDate={action('onChangeStartDate')}
          onChangeStartTime={action('onChangeStartTime')}
          onChangeEndTime={action('onChangeEndTime')}
          onChangeSubstituteLeaveType={action('onChangeSubstituteLeaveType')}
          onChangeSubstituteDate={action('onChangeSubstituteDate')}
          onChangeRemarks={action('onChangeRemarks')}
          validation={{}}
        />
      );
    })
  );
