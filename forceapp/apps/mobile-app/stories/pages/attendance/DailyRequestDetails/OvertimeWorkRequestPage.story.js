// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, number, text, boolean } from '@storybook/addon-knobs';

import store from '../store.mock';

import STATUS from '../../../../../domain/models/approval/request/Status';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';
import { defaultValue } from '../../../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';

import OvertimeWorkRequestPage from '../../../../components/pages/attendance/DailyRequestDetails/OvertimeWorkRequestPage';

storiesOf('Components/pages/attendance/DailyRequestDatails', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'OvertimeWorkRequestPage',
    withInfo({
      inline: false,
      text: `
        # Description

        残業申請詳細画面
    `,
    })(() => {
      return (
        <OvertimeWorkRequestPage
          readOnly={boolean('readOnly', false)}
          request={{
            ...defaultValue,
            type: CODE.OvertimeWork,
            status: text('state', STATUS.NotRequested),
            requestTypeCode: CODE.OvertimeWork,
            requestTypeName: text('requestTypeName', 'OvertimeWork'),
            startDate: text('startDate', '2018-01-01'),
            startTime: number('startTime', 0),
            endTime: number('endTime', 0),
            remarks: text('remarks', ''),
          }}
          validation={{}}
          onChangeStartTime={action('onChangeStartTime')}
          onChangeEndTime={action('onChangeEndTime')}
          onChangeRemarks={action('onChangeRemarks')}
        />
      );
    })
  );
