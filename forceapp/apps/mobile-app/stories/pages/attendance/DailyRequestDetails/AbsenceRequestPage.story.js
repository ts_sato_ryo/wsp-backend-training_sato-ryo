// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, boolean, text } from '@storybook/addon-knobs';

import store from '../store.mock';

import AbsenceRequestPage from '../../../../components/pages/attendance/DailyRequestDetails/AbsenceRequestPage';

storiesOf('Components/pages/attendance/DailyRequestDatails', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'AbsenceRequestPage',
    withInfo({
      inline: false,
      text: `
      Absence Request
      欠勤申請
    `,
    })(() => (
      <AbsenceRequestPage
        onChangeEndDate={action('onChangeEndDate')}
        onChangeReason={action('onChangeReason')}
        readOnly={boolean('readOnly', false)}
        request={{
          type: 'Absence',
          id: 'ABC',
          daysLeft: null,
          hoursLeft: null,
          isDaysLeftManaged: true,
          availableLeaveTypes: [],
          requestTypeCode: 'Absence',
          requestTypeName: text('requestTypeName', '欠勤'),
          status: text('status', 'NotRequested'),
          startDate: text('startDate', '2019-01-01'),
          endDate: text('endDate', '2019-12-31'),
          startTime: null,
          endTime: null,
          remarks: '',
          reason: text('reason', 'Awesome reason of 2-month absence'),
          leaveCode: null,
          leaveName: null,
          leaveType: null,
          leaveRange: null,
          substituteLeaveType: null,
          substituteDate: '',
          directApplyRestTimes: [],
          patternCode: '',
          patternName: '',
          patternRestTimes: [],
          originalRequestId: '',
          requireReason: false,
          isForReapply: boolean('isForReapply', false),
          approver01Name: '',
          targetDate: text('targetDate', '2019-01-01'),
        }}
        validation={{}}
      />
    ))
  );
