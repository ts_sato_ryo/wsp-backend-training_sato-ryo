// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, number, text, boolean } from '@storybook/addon-knobs';

import store from '../store.mock';

import STATUS from '../../../../../domain/models/approval/request/Status';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';
import { defaultValue } from '../../../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';

import EarlyStartWorkRequestPage from '../../../../components/pages/attendance/DailyRequestDetails/EarlyStartWorkRequestPage';

storiesOf('Components/pages/attendance/DailyRequestDatails', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'EarlyStartWorkRequestPage',
    withInfo({
      inline: false,
      text: `
        # Description

        早朝勤務申請詳細画面
    `,
    })(() => {
      return (
        <EarlyStartWorkRequestPage
          readOnly={boolean('readOnly', false)}
          request={{
            ...defaultValue,
            type: CODE.EarlyStartWork,
            status: text('state', STATUS.NotRequested),
            requestTypeCode: CODE.EarlyStartWork,
            requestTypeName: text('requestTypeName', 'EarlyStartWork'),
            startDate: text('startDate', '2018-01-01'),
            startTime: number('startTime', 0),
            endTime: number('endTime', 0),
            remarks: text('remarks', ''),
          }}
          validation={{}}
          onChangeStartTime={action('onChangeStartTime')}
          onChangeEndTime={action('onChangeEndTime')}
          onChangeRemarks={action('onChangeRemarks')}
        />
      );
    })
  );
