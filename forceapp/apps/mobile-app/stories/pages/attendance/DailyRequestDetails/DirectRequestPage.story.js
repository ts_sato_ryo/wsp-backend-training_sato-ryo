// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, number, text, boolean } from '@storybook/addon-knobs';

import store from '../store.mock';

import STATUS from '../../../../../domain/models/approval/request/Status';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';
import { defaultValue } from '../../../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';

import DirectRequestPage from '../../../../components/pages/attendance/DailyRequestDetails/DirectRequestPage';

storiesOf('Components/pages/attendance/DailyRequestDatails', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'DirectRequestPage',
    withInfo({
      inline: false,
      text: `
        # Description

        直行直帰申請詳細画面
    `,
    })(() => {
      return (
        <DirectRequestPage
          readOnly={boolean('readOnly', false)}
          request={{
            ...defaultValue,
            type: CODE.Direct,
            status: text('state', STATUS.NotRequested),
            requestTypeCode: CODE.Direct,
            requestTypeName: text('requestTypeName', 'Direct'),
            startDate: text('startDate', '2018-01-01'),
            endDate: text('endDate', '2018-01-01'),
            startTime: number('startTime', 0),
            endTime: number('endTime', 0),
            directApplyRestTimes: [
              {
                startTime: number('restStartTime', 0),
                endTime: number('restEndTime', 0),
              },
            ],
            remarks: text('remarks', ''),
          }}
          validation={{}}
          onChangeStartDate={action('onChangeStartDate')}
          onChangeEndDate={action('onChangeEndDate')}
          onChangeStartTime={action('onChangeStartTime')}
          onChangeEndTime={action('onChangeEndTime')}
          onChangeRestTime={action('onChangeRestTime')}
          onClickRemoveRestTime={action('onClickRemoveRestTime')}
          onClickAddRestTime={action('onClickAddRestTime')}
          onChangeRemarks={action('onChangeRemarks')}
        />
      );
    })
  );
