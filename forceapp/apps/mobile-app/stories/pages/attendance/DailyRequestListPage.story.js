// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, array, boolean } from '@storybook/addon-knobs';

import store from './store.mock';

import DailyRequestListPage from '../../../components/pages/attendance/DailyRequestListPage';

storiesOf('Components/pages/attendance', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'DailyReuqestListPage',
    withInfo({
      inline: false,
      text: `
      日次申請一覧
    `,
    })(() => (
      <DailyRequestListPage
        availableRequests={array('availableRequests', [
          {
            requestTypeCode: 'Code1',
            requestTypeName: '申請1',
            status: 'Pending',
            isForReapply: true,
          },
        ])}
        latestRequests={array('latestRequests', [
          {
            requestTypeCode: 'Code1',
            requestTypeName: '申請1',
            status: 'Pending',
            isForReapply: true,
          },
          {
            requestTypeCode: 'Code2',
            requestTypeName: '申請2',
            status: 'Rejected',
            isForReapply: true,
          },
          {
            requestTypeCode: 'Code3',
            requestTypeName: '申請3',
            status: 'Approved',
            isForReapply: true,
          },
          {
            requestTypeCode: 'Code4',
            requestTypeName: '申請4',
            status: 'Approval In',
            isForReapply: false,
          },
          {
            requestTypeCode: 'Code5',
            requestTypeName: '申請5',
            status: 'Rejected',
            isForReapply: false,
          },
        ])}
        onClickRequest={action('onClickRequest')}
        isLocked={boolean('isLocked', false)}
      />
    ))
  );
