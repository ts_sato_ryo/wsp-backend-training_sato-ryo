// @flow

import React from 'react';

import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import store from './store.mock';

import * as record from '../../organisms/attendance/DailyDetailList/meta';
import TimesheetDailyPage from '../../../components/pages/attendance/TimesheetDailyPage';

storiesOf('Components/pages/attendance', module)
  .addDecorator((story) => <Provider store={store}>{story()}</Provider>)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(withKnobs)
  .add(
    'TimesheetDailyPage',
    withInfo({
      inline: false,
      text: `
      勤務表
    `,
    })(() => (
      <TimesheetDailyPage
        currentDate="2018-10-10"
        isEditable={boolean('isEditable', true)}
        record={record}
        onChangeStartTime={action('onChangeStartTime')}
        onChangeEndTime={action('onChangeEndTime')}
        onChangeRestTimeStartTime={action('onChangeRestTimeStartTime')}
        onChangeRestTimeEndTime={action('onChangeRestTimeEndTime')}
        onClickRemoveRestTime={action('onClickRemoveRestTime')}
        onClickAddRestTime={action('onClickAddRestTime')}
        onChangeOtherRestTime={action('onChangeOtherRestTime')}
        onChangeRemarks={action('onChangeRemarks')}
        onClickSave={action('onClickSave')}
      />
    ))
  );
