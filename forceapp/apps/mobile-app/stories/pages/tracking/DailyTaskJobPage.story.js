// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import { withKnobs, array, text } from '@storybook/addon-knobs';

import DailyTaskJobPage from '../../../components/pages/tracking/DailyTaskJobPage';

storiesOf('Components/pages/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'DailyTaskJobPage',
    withInfo({
      inline: false,
      styles: (stylesheet) => ({
        ...stylesheet,
        button: {
          ...stylesheet.button,
          topRight: {
            bottom: 0,
            right: 0,
            borderRadius: '5px 0 0 0',
          },
        },
      }),
      text: `
            # Description

            ジョブを追加登録するページ
          `,
    })(() => (
      <DailyTaskJobPage
        jobs={array('jobs', [
          {
            label: 'JOB 1',
            value: 'a',
          },
          {
            label: 'JOB 2',
            value: 'b',
          },
          {
            label: 'JOB 3',
            value: 'c',
          },
        ])}
        selectedJobId={text('selectedJobId', 'b')}
        workCategories={array('workCategories', [
          {
            label: 'Work Category A',
            value: 'a',
          },
          {
            label: 'Work Category B',
            value: 'b',
          },
          {
            label: 'Work Category C',
            value: 'c',
          },
        ])}
        selectedWorkCategoryId={text('selectedWorkCategoryId', 'c')}
        onSelectJob={action('onSelectJob')}
        onSelectWorkCategory={action('onSelectWorkCategory')}
        onChangeTaskTime={action('onChangeTaskTime')}
        onClickDiscard={action('onClickDiscard')}
        onClickSave={action('onClickSave')}
        taskTime="11:00"
      />
    ))
  );
