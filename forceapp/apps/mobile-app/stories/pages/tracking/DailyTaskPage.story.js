// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

/* eslint-disable import/no-extraneous-dependencies */

import { withInfo } from '@storybook/addon-info';
import {
  withKnobs,
  array,
  boolean,
  text,
  number,
} from '@storybook/addon-knobs';

import DailyTaskPage from '../../../components/pages/tracking/DailyTaskPage';

storiesOf('Components/pages/tracking', module)
  .addDecorator(withKnobs)
  .add(
    'DailyTrackPage',
    withInfo({
      inline: false,
      text: `
            # Description

            工数を入力するカード
          `,
    })(() => (
      <DailyTaskPage
        listEditing={false}
        today={text('today', '2018-10-01')}
        taskList={[
          {
            id: 'a',
            jobId: 'aa',
            jobCode: 'XXXXX',
            jobName: 'FOOBAR',
            workCategoryName: 'V5 勤怠',
            workCategoryCode: 'ASSS',
            workCategoryId: 'aaa',
            isDirectInput: true,
            graphRatio: 100,
            taskTime: 100,
            taskNote: 'FFBAR',
            ratio: 100,
            order: 1,
            isDirectCharged: false,
            isDefaultJob: false,
            isRecorded: false,
            color: {},
            eventTaskTime: 0,
          },
          {
            id: 'b',
            jobId: 'aa',
            jobCode: 'XXXXX',
            jobName: 'FOOBAR',
            workCategoryName: 'V5 勤怠',
            workCategoryCode: 'ASSS',
            workCategoryId: 'aaa',
            isDirectInput: true,
            graphRatio: 100,
            taskTime: 15,
            taskNote: 'FFBAR',
            ratio: 100,
            order: 1,
            isDirectCharged: false,
            isDefaultJob: false,
            isRecorded: false,
            color: {},
            eventTaskTime: 0,
          },
          {
            id: 'c',
            jobId: 'aa',
            jobCode: 'XXXXX',
            jobName: 'FOOBAR',
            workCategoryName: 'V5 勤怠',
            workCategoryCode: 'ASSS',
            workCategoryId: 'aaa',
            isDirectInput: false,
            graphRatio: 100,
            taskTime: 35,
            taskNote: 'FFBAR',
            ratio: 100,
            order: 1,
            isDirectCharged: false,
            isDefaultJob: true,
            isRecorded: false,
            color: {},
            eventTaskTime: 0,
          },
          {
            id: 'd',
            jobId: 'aa',
            jobCode: 'XXXXX',
            jobName: 'FOOBAR',
            workCategoryName: 'V5 勤怠',
            workCategoryCode: 'ASSS',
            workCategoryId: 'aaa',
            isDirectInput: true,
            graphRatio: 100,
            taskTime: 35,
            taskNote: 'FFBAR',
            ratio: 100,
            order: 1,
            isDirectCharged: false,
            isDefaultJob: false,
            isRecorded: false,
            color: {},
            eventTaskTime: 0,
          },
          {
            id: 'e',
            jobId: 'aa',
            jobCode: 'XXXXX',
            jobName: 'FOOBAR',
            workCategoryName: 'V5 勤怠',
            workCategoryCode: 'ASSS',
            workCategoryId: 'aaa',
            isDirectInput: true,
            graphRatio: 100,
            taskTime: 35,
            taskNote: 'FFBAR',
            ratio: 100,
            order: 1,
            isDirectCharged: false,
            isDefaultJob: false,
            isRecorded: false,
            color: {},
            eventTaskTime: 0,
          },
        ]}
        taskTimes={array('taskTimes', [
          {
            color: '#0083b6',
            value: 200,
          },
        ])}
        realWorkTime={number('realWorkTime', 200)}
        isTemporaryWorkTime={boolean('isTemporaryWorkTime', false)}
        save={action('save')}
        onChangeDate={action('onChangeDate')}
        onClickAddJob={action('onClickAddJob')}
        onClickPrevDate={action('onClickPrevDate')}
        onClickNextDate={action('onClickNextDate')}
        onToggleEditing={action('onToggleEditing')}
        onClickRefresh={action('onClickRefresh')}
        editRatio={action('editRatio')}
        editTaskTime={action('editTaskTime')}
        deleteTask={action('deleteTask')}
        toggleDirectInput={action('toggleDirectInput')}
        hasDefaultJob={boolean('hasDefaultJob', true)}
        hasMultipleRatios={boolean('hasMultipleRatios', true)}
      />
    ))
  );
