// @flow

import '../commons/config/public-path';

/* eslint-disable import/imports-first */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/* eslint-enable */

import { type Permission } from '../domain/models/access-control/Permission';
import { setUserPermission } from '../commons/modules/accessControl/permission';
import configureStore from './store/configureStore';

import App from './App';

import './styles/base.scss';

const onTouchStart = () => {
  if (document.body) {
    document.body.setAttribute('ontouchstart', '');
  }
};

const renderApp = ({ store, ...props }: *) => (
  Component: React.ComponentType<*>
) => {
  const container = document.getElementById('container');
  if (container !== null) {
    ReactDOM.render(
      <Provider store={store}>
        <Component {...props} />
      </Provider>,
      container
    );
  }
};

// eslint-disable-next-line import/prefer-default-export
export const startApp = ({
  userPermission,
  pathname,
  search,
}: {
  userPermission: Permission,
  pathname?: string,
  search?: string,
}) => {
  const configuredStore = configureStore();
  onTouchStart();
  configuredStore.dispatch(setUserPermission(userPermission));
  renderApp({ pathname, search, store: configuredStore })(App);
};
