// @flow

import type { RouteConfig } from '../../commons/router';

import Navigation from '../components/molecules/commons/SampleNavigation';
import SamplePage from '../components/pages/sample/SamplePage';
import SamplePageContainer from '../containers/pages/sample/SamplePageContainer';
import PageTransition from '../components/atoms/animations/PageTransition';

/*
 * [route]                [react component]
 * sample/approval/:color SamplePage
 * sample/attendance      SamplePageContainer
 * sample/expence/:color  SamplaPage
 */
const routes: RouteConfig = [
  {
    path: 'sample',
    transition: PageTransition, // animation on page transition
    component: Navigation, // layout
    children: [
      {
        path: 'approval/:color',
        component: SamplePage,
        /*
         * URLパラメーターをpropsへマップします。
         * Map URL parameter to Props.
         */
        mapParamsToProps: ({ match }) => ({
          color: match.params.color,
          title: 'approval',
        }),
      },
      {
        path: 'attendance',
        component: SamplePageContainer,
        mapParamsToProps: () => ({
          color: 'red',
          title: 'attendance',
        }),
      },
      {
        path: 'expense/:color',
        component: SamplePage,
        mapParamsToProps: ({ match }) => ({
          color: match.params.color,
          title: 'expense',
        }),
      },
    ],
  },
];

export default routes;
