// @flow

import * as React from 'react';

import type { RouteConfig } from '../../commons/router';

import LayoutContainer from '../containers/organisms/approval/LayoutContainer';
import PageTransition from '../components/atoms/animations/PageTransition';

type Component = Promise<{| default: React.ComponentType<any> |}>;

const ApprovalListContainer = (): Component =>
  import('../containers/pages/approval/ListContainer');
const ReportContainer = (): Component =>
  import('../containers/pages/approval/expense/ReportContainer');
const RecordContainer = (): Component =>
  import('../containers/pages/approval/expense/RecordContainer');
const ApproveAndRejectContainer = (): Component =>
  import('../containers/pages/approval/ApproveAndRejectContainer');
const AttendanceDetailContainer = (): Component =>
  import('../containers/pages/approval/attendance/AttendanceDetailContainer');

/*
 * [route]                [react component]
 * sample/approval/:color ExpensePage
 * sample/attendance      ExpensePageContainer
 * sample/expence/:color  SamplaPage
 */
const routes: RouteConfig = [
  {
    path: 'approval',
    transition: PageTransition, // animation on page transition
    component: LayoutContainer, // layout
    children: [
      {
        // type = attendance or expense or ...
        path: 'list/search-result/:type',
        component: ApprovalListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        // kind narrow
        path: 'list/kind-select',
        component: ApprovalListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'list/select/att-daily',
        component: ApprovalListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'list/select/att-month',
        component: ApprovalListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'list/select/time-request',
        component: ApprovalListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      // Common approve screen.
      {
        path: '/approve/:type/:requestId/',
        component: ApproveAndRejectContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          approve: true,
          type: match.params.type,
          requestId: match.params.requestId,
        }),
      },
      // Common reject screen.
      {
        path: '/reject/:type/:requestId/',
        component: ApproveAndRejectContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          approve: false,
          type: match.params.type,
          requestId: match.params.requestId,
        }),
      },
      {
        path: 'list/select/expense/:requestId/detail/:recordId',
        component: RecordContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          requestId: match.params.requestId,
          recordId: match.params.recordId,
        }),
      },
      {
        path: 'list/select/expense/:requestId',
        component: ReportContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          requestId: match.params.requestId,
        }),
      },
      {
        path: 'list/type/:requestType',
        component: ApprovalListContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          requestType: match.params.requestType,
        }),
      },
      {
        path: 'list/select/attendance/:requestId',
        component: AttendanceDetailContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          requestId: match.params.requestId,
        }),
      },
      {
        path: 'list',
        component: ApprovalListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
    ],
  },
];

export default routes;
