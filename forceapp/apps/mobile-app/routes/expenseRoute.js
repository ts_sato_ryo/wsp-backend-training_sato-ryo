// @flow

import type { RouteConfig } from '../../commons/router';

import Layout from '../containers/organisms/expense/LayoutContainer';
import PageTransition from '../components/atoms/animations/PageTransition';

type Component = Promise<{| default: React.ComponentType<any> |}>;

const ExpenseTypeContainer = (): Component =>
  import('../containers/pages/expense/ExpenseTypeContainer');
const CostCenterContainer = (): Component =>
  import('../containers/pages/expense/CostCenterContainer');
const JobContainer = (): Component =>
  import('../containers/pages/expense/JobContainer');
const CustomExtendedItemContainer = (): Component =>
  import('../containers/pages/expense/CustomExtendedItemContainer');

// record pages
const RecordNewContainer = (): Component =>
  import('../containers/pages/expense/RecordNewContainer');
const RecordNewTranjitJorudanJP = (): Component =>
  import('../components/pages/expense/Record/New/TranjitJorudanJP');
const ReportListContainer = (): Component =>
  import('../containers/pages/expense/ReportListContainer');
const RecordListContainer = (): Component =>
  import('../containers/pages/expense/RecordListContainer');
const RecordListSelectContainer = (): Component =>
  import('../containers/pages/expense/RecordListSelectContainer');
const RecordListItemContainer = (): Component =>
  import('../containers/pages/expense/RecordListItemContainer');

// route pages
const RouteFormContainer = (): Component =>
  import('../containers/pages/expense/RouteFormContainer');
const RouteListContainer = (): Component =>
  import('../containers/pages/expense/RouteListContainer');
const RouteListItemContainer = (): Component =>
  import('../containers/pages/expense/RouteListItemContainer');

// report pages
const ReportDetailContainer = (): Component =>
  import('../containers/pages/expense/ReportDetailContainer');
const ReportSubmitContainer = (): Component =>
  import('../containers/pages/expense/ReportSubmitContainer');
const ReportEditContainer = (): Component =>
  import('../containers/pages/expense/ReportEditContainer');

const ReceiptUploadContainer = (): Component =>
  import('../containers/pages/expense/ReceiptUploadContainer');

/*
 * [route]                [react component]
 * sample/approval/:color ExpensePage
 * sample/attendance      ExpensePageContainer
 * sample/expence/:color  SamplaPage
 */
const routes: RouteConfig = [
  {
    path: 'expense',
    transition: PageTransition, // animation on page transition
    component: Layout, // layout
    children: [
      /**
       * Expense Type
       */
      {
        path:
          'expense-type/list/:recordType/:parentParentGroupId/:parentGroupId',
        component: ExpenseTypeContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          recordType: match.params.recordType,
          parentGroupId: match.params.parentGroupId,
          parentParentGroupId: match.params.parentParentGroupId,
        }),
      },
      {
        path: 'expense-type/list/:recordType/:parentGroupId',
        component: ExpenseTypeContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          recordType: match.params.recordType,
          parentGroupId: match.params.parentGroupId,
        }),
      },
      {
        path: 'expense-type/list/:recordType',
        component: ExpenseTypeContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          recordType: match.params.recordType,
        }),
      },
      {
        path: `expense-type/search/keyword=:keyword?/:level`,
        component: ExpenseTypeContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'search',
          keyword: match.params.keyword,
          level: match.params.level,
        }),
      },

      /**
       * Custom Extended Item
       */
      {
        path:
          'customExtendedItem/list/backType=:backType/reportId=:reportId/recordId=:recordId/index=:index/customExtendedItemLookupId=:customExtendedItemLookupId/customExtendedItemId=:customExtendedItemId/customExtendedItemName=:customExtendedItemName',
        component: CustomExtendedItemContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          customExtendedItemId: match.params.customExtendedItemId,
          customExtendedItemName: match.params.customExtendedItemName,
          customExtendedItemLookupId: match.params.customExtendedItemLookupId,
          index: match.params.index,
          reportId: match.params.reportId,
          recordId: match.params.recordId,
          backType: match.params.backType,
        }),
      },

      /**
       * Cost center
       */
      {
        path:
          'cost-center/list/backType=:backType/targetDate=:targetDate/reportId=:reportId/parentId=:parentId',
        component: CostCenterContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          targetDate: match.params.targetDate,
          reportId: match.params.reportId,
          parentId: match.params.parentId,
          backType: match.params.backType,
        }),
      },
      {
        path:
          'cost-center/list/backType=:backType/targetDate=:targetDate/reportId=:reportId',
        component: CostCenterContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          targetDate: match.params.targetDate,
          reportId: match.params.reportId,
          parentId: match.params.parentId,
          backType: match.params.backType,
        }),
      },
      // Records Flow //
      {
        path: 'cost-center/list/targetDate=:targetDate',
        component: CostCenterContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          targetDate: match.params.targetDate,
          reportId: match.params.reportId,
          parentId: match.params.parentId,
        }),
      },
      {
        path:
          'job/list/backType=:backType/targetDate=:targetDate/reportId=:reportId/parentId=:parentId',
        component: JobContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          targetDate: match.params.targetDate,
          reportId: match.params.reportId,
          parentId: match.params.parentId,
          backType: match.params.backType,
        }),
      },

      /**
       * Record
       */
      {
        path:
          'job/list/backType=:backType/targetDate=:targetDate/reportId=:reportId',
        component: JobContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          targetDate: match.params.targetDate,
          reportId: match.params.reportId,
          parentId: match.params.parentId,
          backType: match.params.backType,
        }),
      },
      {
        path: 'job/list/targetDate=:targetDate',
        component: JobContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'list',
          targetDate: match.params.targetDate,
          reportId: match.params.reportId,
          parentId: match.params.parentId,
        }),
      },
      {
        path: 'record/detail/:reportId/:recordId',
        component: RecordNewContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'reportList',
          reportId: match.params.reportId,
          recordId: match.params.recordId,
        }),
      },
      {
        path: 'record/detail/:recordId',
        component: RecordNewContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'recordList',
          recordId: match.params.recordId,
        }),
      },
      {
        path: 'record/new/general',
        component: RecordNewContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({
          type: 'new',
        }),
      },
      {
        path: 'record/jorudan-detail/:recordId/:reportId',
        component: RouteListItemContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'report',
          recordId: match.params.recordId,
          reportId: match.params.reportId,
        }),
      },
      {
        path: 'record/jorudan-detail/:recordId',
        component: RouteListItemContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'record',
          recordId: match.params.recordId,
        }),
      },
      {
        path: 'record/new/transitjorudanjp',
        component: RecordNewTranjitJorudanJP,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'record/list/item/:recordno',
        component: RecordListItemContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          recordNo: match.params.recordno,
        }),
      },
      {
        path: 'record/list/select',
        component: RecordListSelectContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'record/list/reselect',
        component: RecordListSelectContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({
          isReselect: true,
        }),
      },
      {
        path: 'record/list',
        component: RecordListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'receipt/upload',
        component: ReceiptUploadContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      /**
       * Route
       */
      // Jorudan Record
      // Create New Jorudan Route Flow //
      {
        path: 'route/new',
        component: RouteFormContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'route/list/item/:routeno',
        component: RouteListItemContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          type: 'new',
          routeNo: match.params.routeno,
        }),
      },
      {
        path: 'route/list',
        component: RouteListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      // Report Flow //
      {
        path: 'report/list',
        component: ReportListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },

      /**
       * Report
       */
      {
        path: 'report/submit',
        component: ReportSubmitContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'report/list',
        component: ReportListContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
      {
        path: 'report/detail/:id',
        component: ReportDetailContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          reportId: match.params.id,
        }),
      },
      {
        path: 'report/edit/:reportId',
        component: ReportEditContainer,
        dynamicImport: true,
        mapParamsToProps: ({ match }) => ({
          reportId: match.params.reportId,
        }),
      },
      {
        path: 'report/new',
        component: ReportEditContainer,
        dynamicImport: true,
        mapParamsToProps: () => ({}),
      },
    ],
  },
];

export default routes;
