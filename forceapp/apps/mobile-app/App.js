// @flow

import * as React from 'react';

import { Redirect, BrowserRouter as Router } from 'react-router-dom';
import LoadingPageContainer from './containers/organisms/commons/LoadingPageContainer';
import SystemError from './containers/organisms/commons/SystemErrorContainer';
import SystemAlert from './containers/organisms/commons/SystemAlertContainer';
import SystemConfirm from './containers/organisms/commons/SystemConfirmContainer';
import Toast from './containers/organisms/commons/ToastContainer';

import routes from './routes';
import renderRoutes from '../commons/router';

type Props = {|
  pathname?: string,
  search?: string,
|};

const renderLoaders = () => (
  <React.Fragment>
    <LoadingPageContainer />
  </React.Fragment>
);

const renderSystemError = () => <SystemError />;

const renderSystemAlert = () => <SystemAlert />;

const renderSystemConfirm = () => <SystemConfirm />;

const renderToast = () => <Toast />;

export default ({ pathname, search }: Props) => {
  const app = renderRoutes(routes);
  return (
    <React.Fragment>
      {renderToast()}
      {renderLoaders()}
      {renderSystemError()}
      {renderSystemAlert()}
      {renderSystemConfirm()}
      <Router
        basename={
          process.env.NODE_ENV === 'production' ? '/apex' : '/mobile-app'
        }
      >
        <React.Fragment>
          {pathname ? (
            <Redirect
              to={{
                pathname,
                search,
              }}
            />
          ) : null}
          {app}
        </React.Fragment>
      </Router>
    </React.Fragment>
  );
};
