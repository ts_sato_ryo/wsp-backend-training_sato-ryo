// @flow

import * as React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { type RouterHistory, withRouter } from 'react-router-dom';

import msg from '../../../../commons/languages';
import Component, {
  type Props,
  TABS,
} from '../../../components/organisms/attendance/DailyLayout';
import { initializeDaily } from '../../../action-dispatchers/attendance/timesheet';

import { type $ExtractReturn } from '../../../../commons/utils/TypeUtil';
import { type State } from '../../../modules';

type OwnProps = $ReadOnly<{|
  children: React.Node,
  history: RouterHistory,
|}>;

const goTimesheetDaily = (history: RouterHistory, targetDate: string): void => {
  history.replace(`/attendance/timesheet-daily/${targetDate}`);
};

const goTimesheetMonthly = (
  history: RouterHistory,
  targetDate: string
): void => {
  history.replace(`/attendance/timesheet-monthly/${targetDate}`);
};

const mapStateToProps = (state: State) => ({
  currentDate: (state.attendance.timesheet.ui.daily.paging.current: string),
  prevDate: (state.attendance.timesheet.ui.daily.paging.prev: string),
  nextDate: (state.attendance.timesheet.ui.daily.paging.next: string),
  startDate: (state.attendance.timesheet.entities.startDate: string),
});

const mapDispatchToProps = {
  onClickRefresh: initializeDaily,
};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps,
  ownProps: OwnProps
): Props => ({
  children: ownProps.children,
  currentDate: stateProps.currentDate,
  startDate: stateProps.startDate,
  title: msg().Att_Lbl_WorkTimeInput,
  tab: TABS.workTimeInput,
  disabledPrevDate: !stateProps.prevDate,
  disabledNextDate: !stateProps.nextDate,
  onChangeDate: (date: string) => goTimesheetDaily(ownProps.history, date),
  onClickBackMonth: () =>
    goTimesheetMonthly(ownProps.history, stateProps.currentDate),
  onClickPrevDate: () =>
    goTimesheetDaily(ownProps.history, stateProps.prevDate),
  onClickNextDate: () =>
    goTimesheetDaily(ownProps.history, stateProps.nextDate),
  onClickTimesheetDaily: () =>
    goTimesheetDaily(ownProps.history, stateProps.currentDate),
  onClickDailyRequest: () => {
    ownProps.history.replace(
      `/attendance/daily-requests/${stateProps.currentDate}`
    );
  },
  onClickRefresh: () => dispatchProps.onClickRefresh(stateProps.currentDate),
});

export default ((compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
)(Component): React.ComponentType<{|
  children: $PropertyType<Props, 'children'>,
|}>): React.ComponentType<Object>);
