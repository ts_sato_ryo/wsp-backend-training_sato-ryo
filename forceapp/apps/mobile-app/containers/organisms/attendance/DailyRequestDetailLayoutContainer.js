// @flow

import * as React from 'react';
import { compose, bindActionCreators, type Dispatch } from 'redux';
import { connect } from 'react-redux';
import { type RouterHistory, type Match, withRouter } from 'react-router-dom';

import lifecycle from '../../../concerns/lifecycle';

import ApprovalRequestHistoryRepository from '../../../../repositories/ApprovalRequestHistoryRepository';

import Component, {
  type Props,
} from '../../../components/organisms/attendance/DailyRequestDetailLayout';

import { actions as EntitiesApprovalHistoriesActions } from '../../../modules/attendance/dailyRequest/entities/approvalHistories';
import * as selector from '../../../modules/attendance/dailyRequest/ui/selector';

import { type $ExtractReturn } from '../../../../commons/utils/TypeUtil';
import { type State } from '../../../modules';

import { withLoading } from '../../../modules/commons/loading';
import * as actions from '../../../action-dispatchers/attendance/dailyRequest';

const goBack = (history: RouterHistory, targetDate: ?string): void => {
  history.replace(`/attendance/daily-requests/${targetDate || ''}`);
};

const reload = (history: RouterHistory, match: Match): void => {
  history.replace(match.url);
};

const mapStateToProps = (state: State) => {
  const timesheet = state.attendance.timesheet.entities;
  const detail = state.attendance.dailyRequest.ui.detail;
  const approvalHistories =
    state.attendance.dailyRequest.entities.approvalHistories;
  return {
    isLocked: timesheet.isLocked,
    isEditing: detail.isEditing,
    target: selector.targetRequest(state.attendance.dailyRequest.ui),
    editAction: detail.editAction,
    disableAction: detail.disableAction,
    approvalHistories,
  };
};

const mapDispatchToProps = {
  startEditingHandler: actions.startEditing,
  createHandler: actions.create,
  modifyHandler: actions.modify,
  reapplyHandler: actions.reapply,
  cancelRequestHandler: actions.cancelRequest,
  cancelApprovalHandler: actions.cancelApproval,
  removeHandler: actions.remove,
};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps,
  ownProps: {
    children: React.Node,
    history: RouterHistory,
    match: Match,
  }
) => ({
  id: ownProps.match.params.id || null,
  isLocked: stateProps.isLocked,
  isEditing: stateProps.isEditing,
  editAction: stateProps.editAction,
  disableAction: stateProps.disableAction,
  approvalHistories: stateProps.approvalHistories,
  target: stateProps.target,
  children: ownProps.children,
  onClickBack: () => goBack(ownProps.history, ownProps.match.params.targetDate),
  onClickCancelEditing: () => {
    reload(ownProps.history, ownProps.match);
  },
  onClickStartEditing: () => {
    if (!stateProps.target) {
      return;
    }
    dispatchProps.startEditingHandler(stateProps.target);
  },
  onClickCreate: () => {
    if (!stateProps.target) {
      return;
    }
    dispatchProps
      .createHandler(stateProps.target, ownProps.match.params.targetDate)
      .then((result) => {
        if (result) {
          goBack(ownProps.history, ownProps.match.params.targetDate);
        }
      });
  },
  onClickModify: () => {
    if (!stateProps.target) {
      return;
    }
    dispatchProps
      .modifyHandler(stateProps.target, ownProps.match.params.targetDate)
      .then((result) => {
        if (result) {
          reload(ownProps.history, ownProps.match);
        }
      });
  },
  onClickReapply: () => {
    if (!stateProps.target) {
      return;
    }
    dispatchProps
      .reapplyHandler(stateProps.target, ownProps.match.params.targetDate)
      .then((result) => {
        if (result) {
          goBack(ownProps.history, ownProps.match.params.targetDate);
        }
      });
  },
  onClickCancelRequest: () => {
    if (!stateProps.target) {
      return;
    }
    dispatchProps
      .cancelRequestHandler(
        stateProps.target.id,
        ownProps.match.params.targetDate
      )
      .then((result) => {
        if (result) {
          reload(ownProps.history, ownProps.match);
        }
      });
  },
  onClickCancelApproval: () => {
    if (!stateProps.target) {
      return;
    }
    dispatchProps
      .cancelApprovalHandler(
        stateProps.target.id,
        ownProps.match.params.targetDate
      )
      .then((result) => {
        if (result) {
          reload(ownProps.history, ownProps.match);
        }
      });
  },
  onClickRemove: () => {
    if (!stateProps.target) {
      return;
    }
    dispatchProps
      .removeHandler(stateProps.target.id, ownProps.match.params.targetDate)
      .then((result) => {
        if (result) {
          goBack(ownProps.history, ownProps.match.params.targetDate);
        }
      });
  },
});

export default ((compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: $ExtractReturn<typeof mergeProps>
    ) => {
      const appService = bindActionCreators(
        {
          withLoading,
        },
        dispatch
      );
      const approvalHistoriesService = bindActionCreators(
        EntitiesApprovalHistoriesActions,
        dispatch
      );
      const { id } = props;

      approvalHistoriesService.initialize();

      if (id === null) {
        return;
      }

      appService.withLoading(() =>
        ApprovalRequestHistoryRepository.fetch(id).then((result) =>
          approvalHistoriesService.initialize(result)
        )
      );
    },
  })
)(Component): React.ComponentType<{|
  children: $PropertyType<Props, 'children'>,
|}>): React.ComponentType<Object>);
