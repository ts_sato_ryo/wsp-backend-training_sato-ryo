// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import { compose, type Dispatch } from 'redux';

import { getUserSetting } from '../../../../commons/actions/userSetting';
import { withLoading } from '../../../modules/commons/loading';
import lifecycle from '../../../concerns/lifecycle';

const Identity = ({ children }) => {
  return children;
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default (props: Object) => {
  const ComposedComponent = (compose(
    connect(
      (state: Object) => state,
      (_dispatch: any) => ({}),
      mergeProps
    ),
    lifecycle({
      componentDidMount: (dispatch: Dispatch<any>, _props) => {
        dispatch(withLoading(getUserSetting()));
      },
    })
  )(Identity): React.ComponentType<Object>);

  const MemoizedComponent = React.useMemo(
    () => () => <ComposedComponent {...props} />,
    [props]
  );

  return <MemoizedComponent />;
};
