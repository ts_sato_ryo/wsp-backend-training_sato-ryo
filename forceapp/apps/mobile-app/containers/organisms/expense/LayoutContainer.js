// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';
import Layout from '../../../components/organisms/expense/Layout';

import { getUserSetting } from '../../../../commons/actions/userSetting';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    loading: state.mobileCommons.loading,
    isShowingBody: !!(state.userSetting && state.userSetting.companyId),
  };
};

const mapDispatchToProps = (_dispatch: Dispatch<*>) => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      if (!props.isShowingBody) {
        // When an empty Array is sent to the API, it will only return the basic information and will not retrieve and populate the details.
        dispatch(getUserSetting({ detailSelectors: [] }));
      }
    },
  })
)(Layout): React.ComponentType<Object>);
