// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import lifecycle from '../../../concerns/lifecycle';
import GlobalFooterPage from '../../../components/organisms/expense/GlobalFooter';

const mapStateToProps = (_state, _ownProps) => ({});

const mapDispatchToProps = (_dispatch: Dispatch<*>) => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickRecordList: () => {
    ownProps.history.replace(`/expense/record/list/`);
  },
  onClickReportList: () => {
    ownProps.history.replace(`/expense/report/list/`);
  },
});

// You should not normally use withRoute.
// However, since this Footer is called at various places, it is exceptionally used.
export default (compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({})
)(GlobalFooterPage): React.ComponentType<Object>);
