// @flow

import { connect } from 'react-redux';

import Component from '../../../components/organisms/commons/LoadingPage';

import { isShowing } from '../../../modules/commons/loading';
import { type State } from '../../../modules';

type Props = $PropertyType<Component, 'props'>;

const mapStateToProps = (state: State, ownProps): Props => ({
  ...ownProps,
  isShowing: isShowing(state.mobileCommons.loading),
});

export default (connect(mapStateToProps)(
  Component
): React.ComponentType<Object>);
