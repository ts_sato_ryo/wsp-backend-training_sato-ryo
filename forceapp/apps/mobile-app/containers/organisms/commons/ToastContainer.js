// @flow

import { connect } from 'react-redux';

import Component, { type Props } from '../../../components/atoms/Toast';

import { type State } from '../../../modules';

const mapStateToProps = (state: State): Props => ({
  isShow: state.common.toast.isShow,
  message: state.common.toast.message,
});

export default (connect(mapStateToProps)(
  Component
): React.ComponentType<Object>);
