// @flow

import { connect } from 'react-redux';

import { type State } from '../../../modules';

import Component, {
  type Props,
} from '../../../components/organisms/commons/SystemConfirm';

const mapStateToProps = (state: State, _ownProps): Props => ({
  message: state.mobileCommons.confirm.message,
  callback: state.mobileCommons.confirm.callback,
});

export default (connect(mapStateToProps)(
  Component
): React.ComponentType<Object>);
