// @flow

import { connect } from 'react-redux';

import { type State } from '../../../modules';

import Component, {
  type Props,
} from '../../../components/organisms/commons/SystemAlert';

const mapStateToProps = (state: State, _ownProps): Props => ({
  message: state.mobileCommons.alert.message,
  callback: state.mobileCommons.alert.callback,
});

export default (connect(mapStateToProps)(
  Component
): React.ComponentType<Object>);
