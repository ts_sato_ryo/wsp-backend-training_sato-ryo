// @flow

import { type Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import mapValues from 'lodash/mapValues';

import Component, {
  type Props,
} from '../../../components/organisms/commons/SystemError';

import AppPermissionUtil from '../../../../commons/utils/AppPermissionUtil';

import { reset, errorExists } from '../../../modules/commons/error';
import { type State } from '../../../modules';

type Action = { resetError: () => void };

const mapStateToProps = (state: State, _ownProps): $Diff<Props, Action> => {
  const { useExpense, employeeId, currencyId } = state.userSetting;
  const hasPermissionError = AppPermissionUtil.checkPermissionError(
    useExpense,
    employeeId,
    currencyId,
    true
  );
  return {
    continue: state.mobileCommons.error.isContinuable,
    message: state.mobileCommons.error.message,
    solution: undefined, // TODO Display solution for error.
    showError: errorExists(state.mobileCommons.error) || !!hasPermissionError,
    hasPermissionError,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<any>): Action => ({
  ...mapValues(
    bindActionCreators(
      {
        resetError: reset,
      },
      dispatch
    ),
    (action) => () => {
      action();
    }
  ),
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(Component): React.ComponentType<Object>);
