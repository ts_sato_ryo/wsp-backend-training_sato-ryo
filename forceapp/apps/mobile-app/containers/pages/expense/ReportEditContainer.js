// @flow
// import { isEmpty } from 'lodash';

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import { find, isEmpty } from 'lodash';

import lifecycle from '../../../concerns/lifecycle';
import schema from '../../../schema/expenses/ExpensesReport';
import ReportEditPage from '../../../components/pages/expense/Report/Edit';
import { getAccountingPeriodList } from '../../../action-dispatchers/expense/AccountingPeriod';
import { getExpReportTypeList } from '../../../action-dispatchers/expense/ExpReportType';
import { getCustomHints } from '../../../action-dispatchers/expense/CustomHint';
import { actions as formValueAction } from '../../../modules/expense/ui/report/formValues';
import { actions as reportTypeSelectionAction } from '../../../modules/expense/ui/report/reportTypeSelection';
import { actions as customHintUIActions } from '../../../modules/expense/ui/customHint/list';
import { type State } from '../../../modules';
import { actions as reportActions } from '../../../modules/expense/entities/report';
import { type Report } from '../../../../domain/models/exp/Report';
import {
  uploadReceipts,
  getBase64files,
} from '../../../action-dispatchers/expense/Receipt';

import { selectedRecords } from '../../../modules/expense/selector';
import {
  initialize,
  createNewReport,
  save,
} from '../../../action-dispatchers/expense/ReportDetail';

const mapStateToProps = (state: State) => ({
  targetExpReport: state.expense.entities.report,
  records: selectedRecords(state.expense),
  formValues: state.expense.ui.report.formValues,
  reportTypeSelection: state.expense.ui.report.reportTypeSelection,
  accountingPeriodList: state.expense.entities.accountingPeriod.filter(
    (ap) => ap.active
  ),
  expReportTypeList: state.expense.entities.expReportType,
  userSetting: state.userSetting,
  activeHints: state.expense.ui.customHint.list,
  customHints: state.expense.entities.customHint,
});

const mapDispatchToProps = {
  saveReportFormValues: formValueAction.save,
  clearFormValues: formValueAction.clear,
  getAccountingPeriodList,
  getExpReportTypeList,
  save,
  clearReportTypeSelection: reportTypeSelectionAction.clear,
  onClickHint: customHintUIActions.set,
  resetCustomHint: customHintUIActions.clear,
  getCustomHints,
  getBase64files,
  uploadReceipts,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickCancelButton: (report: Report) => {
    if (ownProps.reportId && ownProps.reportId !== 'null') {
      ownProps.history.push(
        `../../../expense/report/detail/${ownProps.reportId}`
      );
    } else {
      dispatchProps.saveReportFormValues(report);
      ownProps.history.push('../../expense/record/list/reselect');
    }
  },
  onClickSearchCostCenter: (accountingDate: string) => {
    const reportId = ownProps.reportId || 'null';
    ownProps.history.push(
      `/expense/cost-center/list/backType=report/targetDate=${accountingDate}/reportId=${reportId}`
    );
  },
  onClickSearchJob: (accountingDate: string) => {
    const reportId = ownProps.reportId || 'null';
    ownProps.history.push(
      `/expense/job/list/backType=report/targetDate=${accountingDate}/reportId=${reportId}`
    );
  },
  onClickSearchCustomEI: (
    customExtendedItemLookupId: string,
    customExtendedItemId: string,
    customExtendedItemName: string,
    index: string
  ) => {
    const reportId = ownProps.reportId || 'null';
    ownProps.history.push(
      `/expense/customExtendedItem/list/backType=report/reportId=${reportId}/recordId=null/index=${index}/customExtendedItemLookupId=${customExtendedItemLookupId}/customExtendedItemId=${customExtendedItemId}/customExtendedItemName=${customExtendedItemName}`
    );
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withFormik({
    enableReinitialize: true,
    validationSchema: schema,

    mapPropsToValues: (props) => {
      const selectedReportType = find(props.expReportTypeList, {
        id: props.targetExpReport.expReportTypeId,
      });
      let isCostCenterRequired = false;
      let isJobRequired = false;
      if (selectedReportType) {
        isCostCenterRequired =
          selectedReportType.isCostCenterRequired === 'REQUIRED';
        isJobRequired = selectedReportType.isJobRequired === 'REQUIRED';
      }
      return {
        ui: {
          checkboxes: [],
          recordIdx: -1,
          recalc: false,
          saveMode: false,
        },
        report: {
          ...props.targetExpReport,
          isCostCenterRequired,
          isJobRequired,
        },
      };
    },
    handleSubmit: (values, { props }) => {
      props.save(values.report).then(([{ reportId }, ..._rest]) => {
        props.clearFormValues();
        props.clearReportTypeSelection();
        props.resetCustomHint();
        props.history.replace(`/expense/report/detail/${reportId}`);
      });
    },
  }),
  lifecycle({
    componentDidMount(dispatch: Dispatch<any>, props) {
      if (isEmpty(props.customHint)) {
        props.getCustomHints(props.userSetting.companyId);
      }
      // in case the url is 'report/edit'
      if (props.reportId) {
        Promise.all([
          props.getAccountingPeriodList(props.userSetting.companyId),
          props.getExpReportTypeList(
            props.userSetting.companyId,
            true,
            props.userSetting.employeeId
          ),
        ]).then(([aplRes]) => {
          const accountingPeriodList =
            aplRes[0].payload.filter((ap) => ap.active) || [];
          if (isEmpty(props.formValues)) {
            dispatch(initialize(props.reportId, accountingPeriodList));
          } else {
            dispatch(reportActions.setReport(props.formValues));
          }
        });
      }
      // in case the url is 'report/new'
      else {
        Promise.all([
          props.getAccountingPeriodList(props.userSetting.companyId),
          props.getExpReportTypeList(
            props.userSetting.companyId,
            true,
            props.userSetting.employeeId
          ),
        ]).then(([aplRes, ertl]) => {
          const accountingPeriodList = aplRes[0].payload.filter(
            (ap) => ap.active
          );
          const expReportTypeList = ertl[0].payload;
          const accountingPeriodId =
            props.reportTypeSelection.accountingPeriodId;
          const reportTypeId = props.reportTypeSelection.reportTypeId;
          const accountingDate = props.reportTypeSelection.accountingDate;
          if (isEmpty(props.formValues)) {
            dispatch(
              createNewReport(
                props.records,
                props.userSetting,
                accountingPeriodList,
                expReportTypeList,
                accountingPeriodId,
                reportTypeId,
                accountingDate
              )
            );
          } else {
            dispatch(reportActions.setReport(props.formValues));
            dispatch(reportActions.setRecords(props.records));
          }
        });
      }
    },
  })
)(ReportEditPage): React.ComponentType<Object>);
