// @flow
import _ from 'lodash';
import { compose } from 'redux';
import { withFormik } from 'formik';
import { connect } from 'react-redux';
import schema from '../../../schema/expenses/JorudanRecordNewSchema';

import { newRecord, RECORD_TYPE } from '../../../../domain/models/exp/Record';
import { actions as routeActions } from '../../../../domain/modules/exp/jorudan/route';
import { actions as routeFormActions } from '../../../modules/expense/pages/routeFormPage';
import {
  createRecord,
  deleteRecord,
} from '../../../action-dispatchers/expense/Record';
import { save as saveReport } from '../../../action-dispatchers/expense/ReportDetail';
import RouteListItemPage from '../../../components/pages/expense/Route/List/Item';
import STATUS from '../../../../domain/models/approval/request/Status';
import { actions as formValueAction } from '../../../modules/expense/ui/general/formValues';
import { calculateTax } from '../../../../domain/models/exp/TaxType';
import { actions as isShowDialogAction } from '../../../modules/expense/ui/general/isShowDialog';
import {
  mapStateToProps as mapReportTypeSelectionStateToProps,
  mapDispatchToProps as mapReportTypeSelectionDispatchToProps,
  mergeProps as mergeReportTypeSelectionProps,
} from './ReportTypeSelectionContainer';
import { actions as customHintUIActions } from '../../../modules/expense/ui/customHint/list';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  ...mapReportTypeSelectionStateToProps(state),
  report: state.expense.entities.report,
  records: state.expense.entities.recordList,
  reportRecords: state.expense.entities.report.records,
  routeFormParams: state.expense.pages.routeFormPage,
  defaultRouteOptions: state.expense.pages.defaultRouteOptions,
  routeResults: state.expense.entities.routeResults,
  isNotEditable: [STATUS.Pending, STATUS.Approved].includes(
    state.expense.entities.report.status
  ),
  expenseTypeList: state.expense.entities.expenseTypeList,
  formValues: state.expense.ui.general.formValues,
  taxList: state.expense.entities.taxType,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  isShowDialog: state.expense.ui.general.isShowDialog,
  activeHints: state.expense.ui.customHint.list,
  customHints: state.expense.entities.customHint,
});

const mapDispatchToProps = {
  createRecord,
  deleteRecord,
  saveReport,
  clearRouteFormValues: routeFormActions.clear,
  saveRouteFormValues: routeFormActions.save,
  clearRouteResults: routeActions.clear,
  saveFormValues: formValueAction.save,
  clearFormValues: formValueAction.clear,
  showSaveDialog: isShowDialogAction.visible,
  hideSaveDialog: isShowDialogAction.unvisible,
  ...mapReportTypeSelectionDispatchToProps,
  onClickHint: customHintUIActions.set,
  resetCustomHint: customHintUIActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickNewRouteRecordButton: () => {
    dispatchProps.clearRouteFormValues();
    dispatchProps.resetCustomHint();
    ownProps.history.push(`/expense/route/new`);
    dispatchProps.clearRouteResults();
    dispatchProps.clearFormValues();
  },
  onClickRecordListButton: () => {
    dispatchProps
      .getAccountingPeriodList(stateProps.userSetting.companyId)
      .then(() => {
        dispatchProps.hideSaveDialog();
        dispatchProps.showReportTypeSelectionDialog(true);
        dispatchProps.clearRouteFormValues();
      });
  },
  onClickBackButton: () => {
    if (ownProps.type === 'new') {
      ownProps.history.push('/expense/route/list');
    } else if (ownProps.type === 'record') {
      ownProps.history.push('/expense/record/list');
    } else if (ownProps.type === 'report') {
      ownProps.history.push(`/expense/report/detail/${ownProps.reportId}`);
    }
  },
  onClickCloseButton: () => {
    ownProps.history.push('/expense/record/list');
    dispatchProps.clearRouteFormValues();
    dispatchProps.clearRouteResults();
  },

  onClickEditButton: () => {
    // go to edit page
  },
  onClickDeleteButton: () => {
    if (ownProps.type === 'record') {
      dispatchProps.deleteRecord(ownProps.recordId).then(() => {
        ownProps.history.goBack();
      });
    } else if (ownProps.type === 'report') {
      dispatchProps.deleteRecord(ownProps.recordId).then(() => {
        dispatchProps.clearRouteFormValues();
        dispatchProps.resetCustomHint();
        ownProps.history.goBack();
      });
    }
  },

  getValuesFromRecords: () => {
    const { recordId, records, reportRecords } = stateProps;
    const record = _.find(records, { recordId });
    const values = _.isEmpty(record)
      ? _.find(reportRecords, { recordId })
      : record;

    if (!values) {
      return false;
    }

    if (typeof values.routeInfo.roundTrip !== 'string') {
      values.routeInfo.roundTrip = values.routeInfo.roundTrip ? '1' : '0';
    }

    return values;
  },
  newRouteRecords: () => {
    if (!_.isEmpty(stateProps.formValues)) {
      return stateProps.formValues;
    }

    const { routeResults, routeFormParams, expenseTypeList } = stateProps;
    const selectedExpType = _.find(expenseTypeList, {
      id: routeFormParams.expenseTypeId,
    });
    const record: any = newRecord(
      routeFormParams.expenseTypeId,
      routeFormParams.expenseType,
      RECORD_TYPE.TransitJorudanJP,
      false,
      selectedExpType,
      true
    );
    const routeLists = _.get(routeResults, 'route.routeList');
    const selectedRoute = routeLists[ownProps.routeNo];
    const routeInfo = {
      arrival: routeFormParams.arrival,
      origin: routeFormParams.origin,
      viaList: routeFormParams.viaList,
      roundTrip: '0',
      selectedRoute,
    };

    record.routeInfo = routeInfo;
    record.recordDate = routeFormParams.targetDate;
    // init amount as one way cost
    record.items[0].amount = selectedRoute.cost;

    const tax = stateProps.taxList[0];

    const taxRes = calculateTax(
      tax.rate,
      selectedRoute.cost,
      stateProps.baseCurrencyDecimal
    );

    record.withoutTax = taxRes.amountWithoutTax;
    record.items[0].withoutTax = taxRes.amountWithoutTax;
    record.items[0].gstVat = taxRes.gstVat;
    record.items[0].taxTypeHistoryId = stateProps.taxList[1].historyId; // taxList[1] has the first tax. taxList[0] is for Auto Select one.

    return record;
  },
  onClickSearchCustomEI: (
    customExtendedItemLookupId: string,
    customExtendedItemId: string,
    customExtendedItemName: string,
    index: string
  ) => {
    const reportId = ownProps.reportId || 'null';

    // dispatchProps.saveRouteFormValues();

    ownProps.history.push(
      `/expense/customExtendedItem/list/backType=${ownProps.routeNo}/reportId=${reportId}/recordId=null/index=${index}/customExtendedItemLookupId=${customExtendedItemLookupId}/customExtendedItemId=${customExtendedItemId}/customExtendedItemName=${customExtendedItemName}`
    );
  },
  onClickSearchCostCenter: (recordDate: string) => {
    const reportId = ownProps.reportId;
    ownProps.history.push(
      `/expense/cost-center/list/backType=record/targetDate=${recordDate}/reportId=${reportId}`
    );
  },
  onClickSearchJob: (recordDate: string) => {
    const reportId = ownProps.reportId;
    ownProps.history.push(
      `/expense/job/list/backType=record/targetDate=${recordDate}/reportId=${reportId}`
    );
  },
  ...mergeReportTypeSelectionProps(stateProps, dispatchProps, ownProps),
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withFormik({
    enableReinitialize: true,
    validationSchema: schema,
    mapPropsToValues: (props) => {
      const { recordId } = props;
      return recordId ? props.getValuesFromRecords() : props.newRouteRecords();
    },
    handleSubmit: (values, { props, setStatus }) => {
      const result = _.cloneDeep(values);
      if (typeof result.routeInfo.roundTrip === 'string') {
        result.routeInfo.roundTrip = result.routeInfo.roundTrip !== '0';
      }
      props.createRecord(result).then(() => {
        setStatus({
          isRecordSaved: true,
        });
        props.showSaveDialog();
        props.resetCustomHint();
      });
    },
  })
)(RouteListItemPage): React.ComponentType<Object>);
