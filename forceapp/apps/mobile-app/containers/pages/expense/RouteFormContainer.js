// @flow
import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import { withFormik } from 'formik';
import { startLoading, endLoading } from '../../../modules/commons/loading';
import lifecycle from '../../../concerns/lifecycle';
import schema from '../../../schema/expenses/RouteFormSchema';

import NewRoutePage from '../../../components/pages/expense/Route/New';

import { actions as routeFormPageActions } from '../../../modules/expense/pages/routeFormPage';
import { actions as routeOptionActions } from '../../../modules/expense/pages/defaultRouteOptions';
import {
  getStationSuggestion,
  searchStations,
} from '../../../action-dispatchers/expense/StationSearch';
import { getJorudanExpenseType } from '../../../action-dispatchers/expense/ExpenseType';
import { clearRouteResults } from '../../../action-dispatchers/expense/RouteSearch';
import { getUserSetting } from '../../../../commons/actions/userSetting';
import { getTaxTypeList } from '../../../action-dispatchers/expense/TaxType';
import { getCustomHints } from '../../../action-dispatchers/expense/CustomHint';
import { actions as customHintUIActions } from '../../../modules/expense/ui/customHint/list';
import AppPermissionUtil from '../../../../commons/utils/AppPermissionUtil';

const mapStateToProps = (state) => {
  const { useExpense, employeeId, currencyId } = state.userSetting;
  return {
    userSetting: state.userSetting,
    activeHints: state.expense.ui.customHint.list,
    customHints: state.expense.entities.customHint,
    routeFormParams: state.expense.pages.routeFormPage,
    defaultRouteOptions: state.expense.pages.defaultRouteOptions,
    expenseTypeList: state.expense.entities.expenseTypeList,
    hasPermissionError: AppPermissionUtil.checkPermissionError(
      useExpense,
      employeeId,
      currencyId
    ),
    language: state.userSetting.language,
  };
};

const mapDispatchToProps = {
  searchStations,
  clearRouteResults,
  getStationSuggestion,
  getJorudanExpenseType,
  saveParams: (routeFormParams: any) =>
    routeFormPageActions.save(routeFormParams),
  getTaxTypeList,
  onClickHint: customHintUIActions.set,
  resetCustomHint: customHintUIActions.clear,
  getCustomHints,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickPushHistory: () => {
    ownProps.history.push('/expense/route/list');
  },
  getJorudanExpenseType: (targetDate: string) => {
    dispatchProps.getJorudanExpenseType(
      stateProps.userSetting.companyId,
      targetDate
    );
  },
});

const newRouteForm = withFormik({
  enableReinitialize: true,
  mapPropsToValues: (props) => ({
    empId: props.userSetting.employeeId,
    arrival: props.routeFormParams.arrival || {
      category: '',
      company: '',
      name: '',
    },
    targetDate: props.routeFormParams.targetDate || '',
    expenseType: props.routeFormParams.expenseType || '',
    expenseTypeId: props.routeFormParams.expenseTypeId || '',
    option: props.routeFormParams.option || {
      routeSort: props.defaultRouteOptions.jorudanRouteSort,
      highwayBus: props.defaultRouteOptions.jorudanHighwayBus,
      seatPreference: props.defaultRouteOptions.jorudanSeatPreference,
      useChargedExpress: props.defaultRouteOptions.jorudanUseChargedExpress,
      useExReservation: props.defaultRouteOptions.jorudanUseExReservation,
      excludeCommuterRoute: true,
    },
    origin: props.routeFormParams.origin || {
      category: '',
      company: '',
      name: '',
    },
    viaList: props.routeFormParams.viaList || [],
  }),
  validationSchema: schema,
  handleSubmit: (values, { props, setValues, setStatus }) => {
    // reset custom errors for search result
    setStatus({});

    // make sure empty via is excluded.
    const filteredViaList = values.viaList.filter((via) =>
      via ? via.name : via
    );
    values.viaList = filteredViaList;

    // search stations with new param
    props.searchStations(values).then((result) => {
      setValues(result.param);
      props.saveParams(result.param);

      if (isEmpty(result.errors)) {
        props.clearRouteResults();
        props.onClickPushHistory();
      } else {
        setStatus(result.errors);
      }
    });
  },
})(NewRoutePage);

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      if (isEmpty(props.customHint)) {
        props.getCustomHints(props.userSetting.companyId);
      }
      if (!props.userSetting || !props.userSetting.id) {
        const loadingId = dispatch(startLoading());
        // When an empty Array is sent to the API, it will only return the basic information and will not retrieve and populate the details.
        dispatch(getUserSetting({ detailSelectors: [] })).then((res) => {
          dispatch(routeOptionActions.search(res.payload.companyId)).then(
            () => {
              dispatch(endLoading(loadingId));
            }
          );
        });
      } else {
        dispatch(routeOptionActions.search(props.userSetting.companyId));
      }
    },
    componentWillUnmount: (_dispatch: Dispatch<any>, props) => {
      props.getTaxTypeList(
        props.routeFormParams.expenseTypeId,
        props.routeFormParams.targetDate
      );
    },
  })
)(newRouteForm): React.ComponentType<Object>);
