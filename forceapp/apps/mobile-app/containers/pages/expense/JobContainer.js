// @flow

import { cloneDeep } from 'lodash';
import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';

import msg from '../../../../commons/languages';
import LevelList from '../../../components/pages/expense/commons/LevelList';

import { getJobList, searchJob } from '../../../action-dispatchers/expense/Job';

import { actions as jobActions } from '../../../../domain/modules/exp/job/list';
import { actions as formValueReportAction } from '../../../modules/expense/ui/report/formValues';
import { actions as formValueRecordAction } from '../../../modules/expense/ui/general/formValues';

const mapStateToProps = (state) => {
  return {
    companyId: state.userSetting.companyId,
    formValuesReport: state.expense.ui.report.formValues,
    formValuesRecord: state.expense.ui.general.formValues,
    searchingList: state.expense.entities.job,
    searchingAllList: state.expense.entities.jobAll,
    title: msg().Exp_Lbl_Job,
    userSetting: state.userSetting,
  };
};

const mapDispatchToProps = {
  saveFormValuesReport: formValueReportAction.save,
  saveFormValuesRecord: formValueRecordAction.save,
  getJobList: jobActions.list,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onSearchButtonClick: () => {},
  onClickRow: (selectedJob) => {
    let newFormValues;
    if (ownProps.backType === 'report') {
      newFormValues = cloneDeep(stateProps.formValuesReport);
      newFormValues.jobName = selectedJob.name;
      newFormValues.jobId = selectedJob.id;
      dispatchProps.saveFormValuesReport(newFormValues);
    } else if (ownProps.backType === 'record') {
      newFormValues = cloneDeep(stateProps.formValuesRecord);
      newFormValues.items[0].jobName = selectedJob.name;
      newFormValues.items[0].jobId = selectedJob.id;
      dispatchProps.saveFormValuesRecord(newFormValues);
    }
    // redirect
    let url;
    if (ownProps.backType === 'report') {
      if (ownProps.reportId && ownProps.reportId !== 'null') {
        url = `/expense/report/edit/${ownProps.reportId}`;
      } else {
        url = '/expense/report/new';
      }
      ownProps.history.push(url);
    } else if (ownProps.backType === 'record') {
      url = `/expense/record/detail/${ownProps.reportId}/${stateProps.formValuesRecord.recordId}`;
    }
    ownProps.history.push(url);
  },
  onClickIcon: (selectedJob) => {
    const url = `/expense/job/list/backType=${ownProps.backType}/targetDate=${
      ownProps.targetDate
    }/reportId=${
      ownProps.reportId || ownProps.reportId === 'null'
        ? ownProps.reportId
        : 'null'
    }/parentId=${selectedJob.id}`;
    ownProps.history.push(url);
  },
  onClickBack: () => {
    ownProps.history.goBack();
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      dispatch(
        getJobList(
          props.userSetting.employeeId,
          props.parentId ? props.parentId : null,
          props.targetDate
        )
      );
      dispatch(searchJob(props.userSetting.employeeId, props.targetDate));
    },
  })
)(LevelList): React.ComponentType<Object>);
