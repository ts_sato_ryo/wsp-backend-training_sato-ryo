// @flow

import _ from 'lodash';
import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';

import ExpenseType from '../../../components/pages/expense/Record/New/General/ExpenseType';
import { copyEIsFromSource } from '../../../../domain/models/exp/ExtendedItem';
import { newRecord, RECEIPT_TYPE } from '../../../../domain/models/exp/Record';
import {
  getExpenseTypeList,
  searchExpenseType,
} from '../../../action-dispatchers/expense/ExpenseType';

import { actions as formValueAction } from '../../../modules/expense/ui/general/formValues';

const mapStateToProps = (state) => {
  return {
    companyId: state.userSetting.companyId,
    formValues: state.expense.ui.general.formValues,
    expenseTypeList: state.expense.entities.expenseTypeList,
  };
};

const mapDispatchToProps = {
  saveFormValues: formValueAction.save,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onSearchClick: (keyword) => {
    if (keyword === null) {
      return;
    }
    let level = 2;
    if (ownProps.parentParentGroupId) {
      level = 4;
    } else if (ownProps.parentGroupId) {
      level = 3;
    }
    // Special handling of % because it could not be sanitized with react router.
    const sanitizedKeyword = encodeURIComponent(keyword.replace(/%/g, '%25'));
    const urlPath = `/expense/expense-type/search/keyword=${sanitizedKeyword}/${level}`;
    ownProps.history.push(urlPath);
  },

  onRowClick: (expenseType) => {
    if (expenseType.isGroup) {
      ownProps.history.push(
        `${ownProps.history.location.pathname}/${expenseType.id}`
      );
    } else {
      const record = newRecord(
        expenseType.id,
        expenseType.name,
        expenseType.recordType,
        expenseType.useForeignCurrency,
        expenseType,
        true,
        expenseType.fileAttachment
      );
      const formValues = _.cloneDeep(stateProps.formValues);
      record.recordDate = formValues.recordDate;
      record.items[0].amount = formValues.items[0].amount;
      record.items[0].remarks = formValues.items[0].remarks;
      record.items[0].taxTypeBaseId = formValues.items[0].taxTypeBaseId;
      record.items[0].taxTypeHistoryId = formValues.items[0].taxTypeHistoryId;
      record.items[0].taxTypeName = formValues.items[0].taxTypeName;
      record.recordId = formValues.recordId;
      record.items[0].itemId = formValues.items[0].itemId; // Temporary fix, change RecordItem for multiple items

      record.items[0] = copyEIsFromSource(record.items[0], formValues.items[0]);
      if (expenseType.fileAttachment !== RECEIPT_TYPE.NotUsed) {
        record.receiptId = formValues.receiptId;
        record.receiptFileId = formValues.receiptFileId;
        record.receiptData = formValues.receiptData;
      }

      dispatchProps.saveFormValues(record);
      if (record.recordId) {
        if (ownProps.keyword && ownProps.level) {
          ownProps.history.go(-1 * ownProps.level);
        } else if (ownProps.parentParentGroupId) {
          ownProps.history.go(-3);
        } else if (ownProps.parentGroupId) {
          ownProps.history.go(-2);
        } else {
          ownProps.history.go(-1);
        }
      } else {
        ownProps.history.push('/expense/record/new/general');
      }
    }
  },
  onBackClick: () => {
    ownProps.history.goBack();
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      if (props.type === 'list') {
        const parentGroupId = props.parentGroupId || null;
        dispatch(
          getExpenseTypeList(
            null,
            parentGroupId,
            props.formValues.recordDate,
            props.recordDate
          )
        );
      } else {
        const keyword = props.keyword || '';
        dispatch(
          searchExpenseType(
            props.companyId,
            decodeURIComponent(keyword),
            props.formValues.recordDate
          )
        );
      }
    },
  })
)(ExpenseType): React.ComponentType<Object>);
