// @flow

import _ from 'lodash';
import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';

import EILookup from '../../../components/pages/expense/commons/EILookup';
import {
  getCustomExtendItemOptions,
  getRecentlyUsed,
} from '../../../action-dispatchers/expense/CustomExtendedItem';

import { actions as formValueReportAction } from '../../../modules/expense/ui/report/formValues';
import { actions as formValueRecordAction } from '../../../modules/expense/ui/general/formValues';

const mapStateToProps = (state) => {
  return {
    companyId: state.userSetting.companyId,
    employeeId: state.userSetting.employeeId,
    formValuesReprot: state.expense.ui.report.formValues,
    formValuesRecord: state.expense.ui.general.formValues,
    eiLookup: state.expense.entities.customEIOption,
    notSearchingRealTime: true,
    hasMore: state.expense.entities.customEIOption.hasMore,
  };
};

const mapDispatchToProps = {
  saveFormValuesReport: formValueReportAction.save,
  saveFormValuesRecord: formValueRecordAction.save,
  getRecentlyUsed,
  getCustomExtendItemOptions,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickSearchButton: (keyword) => {
    dispatchProps.getCustomExtendItemOptions(
      ownProps.customExtendedItemId,
      keyword
    );
  },
  onClickRow: (selectedCustomExtendedItem) => {
    let newFormValues;
    if (ownProps.backType === 'report') {
      newFormValues = _.cloneDeep(stateProps.formValuesReprot);
      newFormValues[`extendedItemLookup${ownProps.index}Value`] =
        selectedCustomExtendedItem.code;
      newFormValues[`extendedItemLookup${ownProps.index}SelectedOptionName`] =
        selectedCustomExtendedItem.name;
      dispatchProps.saveFormValuesReport(newFormValues);
    } else if (ownProps.backType === 'record') {
      newFormValues = _.cloneDeep(stateProps.formValuesRecord);
      newFormValues.items[0][`extendedItemLookup${ownProps.index}Value`] =
        selectedCustomExtendedItem.code;
      newFormValues.items[0][
        `extendedItemLookup${ownProps.index}SelectedOptionName`
      ] = selectedCustomExtendedItem.name;
      dispatchProps.saveFormValuesRecord(newFormValues);
    } else {
      newFormValues = _.cloneDeep(stateProps.formValuesRecord);
      newFormValues.items[0][`extendedItemLookup${ownProps.index}Value`] =
        selectedCustomExtendedItem.code;
      newFormValues.items[0][
        `extendedItemLookup${ownProps.index}SelectedOptionName`
      ] = selectedCustomExtendedItem.name;
      dispatchProps.saveFormValuesRecord(newFormValues);
    }

    let url = '';
    if (ownProps.backType === 'record') {
      if (ownProps.recordId && ownProps.recordId !== 'null') {
        url = `/expense/record/detail/${ownProps.recordId}`;
      } else {
        url = `/expense/record/new/general`;
      }
    } else if (ownProps.backType === 'report') {
      if (ownProps.reportId && ownProps.reportId !== 'null') {
        url = `/expense/report/edit/${ownProps.reportId}`;
      } else {
        url = '/expense/report/new';
      }
    } else {
      url = `/expense/route/list/item/${ownProps.backType}`;
    }
    ownProps.history.push(url);
  },
  onClickBack: () => {
    ownProps.history.goBack();
  },
  getRecentlyUsed: (eiCustomId, eiLookupId) =>
    dispatchProps.getRecentlyUsed(
      eiCustomId,
      eiLookupId,
      stateProps.employeeId
    ),
  title: ownProps.customExtendedItemName,
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      props.getRecentlyUsed(
        props.customExtendedItemId,
        props.customExtendedItemLookupId
      );
    },
  })
)(EILookup): React.ComponentType<Object>);
