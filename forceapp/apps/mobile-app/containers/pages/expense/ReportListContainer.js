// @flow

import { compose } from 'redux';
import { connect } from 'react-redux';
import lifecycle from '../../../concerns/lifecycle';

import ReportListPage from '../../../components/pages/expense/Report/List';
import { getReportList } from '../../../action-dispatchers/expense/ReportList';
import AppPermissionUtil from '../../../../commons/utils/AppPermissionUtil';

const mapStateToProps = (state) => {
  const { useExpense, employeeId, currencyId } = state.userSetting;
  return {
    reportList: state.expense.entities.reportList,
    userSetting: state.userSetting,
    hasPermissionError: AppPermissionUtil.checkPermissionError(
      useExpense,
      employeeId,
      currencyId
    ),
  };
};

const mapDispatchToProps = {
  getReportList,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  openDetail: (reportId: string) => {
    ownProps.history.push(`/expense/report/detail/${reportId}`);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, _props) => {
      dispatch(getReportList());
    },
  })
)(ReportListPage): React.ComponentType<Object>);
