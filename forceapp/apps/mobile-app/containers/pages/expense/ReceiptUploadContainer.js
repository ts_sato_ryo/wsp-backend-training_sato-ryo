// @flow

import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';

import UploadPage from '../../../components/pages/expense/Report/Upload';
import {
  uploadReceipts,
  getBase64files,
} from '../../../action-dispatchers/expense/Record';
import AppPermissionUtil from '../../../../commons/utils/AppPermissionUtil';

const mapStateToProps = (state) => {
  const { useExpense, employeeId, currencyId } = state.userSetting;
  return {
    hasPermissionError: AppPermissionUtil.checkPermissionError(
      useExpense,
      employeeId,
      currencyId
    ),
  };
};

const mapDispatchToProps = {
  uploadReceipts,
  getBase64files,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withFormik({
    mapPropsToValues: (_props) => ({
      files: [],
    }),
    handleSubmit: (values, { props, setValues }) => {
      props.uploadReceipts(values.files).then(() => setValues({ files: [] }));
    },
  })
)(UploadPage): React.ComponentType<Object>);
