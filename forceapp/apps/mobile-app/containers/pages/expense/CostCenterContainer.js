// @flow

import { cloneDeep } from 'lodash';
import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';

import msg from '../../../../commons/languages';
import LevelList from '../../../components/pages/expense/commons/LevelList';

import {
  getCostCenterList,
  searchCostCenter,
} from '../../../action-dispatchers/expense/CostCenter';

import { actions as costCenterActions } from '../../../../domain/modules/exp/cost-center/list';
import { actions as formValueReportAction } from '../../../modules/expense/ui/report/formValues';
import { actions as formValueRecordAction } from '../../../modules/expense/ui/general/formValues';

const mapStateToProps = (state) => {
  return {
    companyId: state.userSetting.companyId,
    formValuesReport: state.expense.ui.report.formValues,
    formValuesRecord: state.expense.ui.general.formValues,
    searchingList: state.expense.entities.costCenterList,
    searchingAllList: state.expense.entities.costCenterAllList,
    title: msg().Exp_Clbl_CostCenter,
    userSetting: state.userSetting,
  };
};

const mapDispatchToProps = {
  saveFormValuesReport: formValueReportAction.save,
  saveFormValuesRecord: formValueRecordAction.save,
  getCostCenterList: costCenterActions.list,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onSearchButtonClick: () => {},
  onClickRow: (selectedCostCenter) => {
    let newFormValues;
    if (ownProps.backType === 'report') {
      newFormValues = cloneDeep(stateProps.formValuesReport);
      newFormValues.costCenterName = selectedCostCenter.name;
      newFormValues.costCenterHistoryId = selectedCostCenter.historyId
        ? selectedCostCenter.historyId
        : selectedCostCenter.id;
      dispatchProps.saveFormValuesReport(newFormValues);
    } else if (ownProps.backType === 'record') {
      newFormValues = cloneDeep(stateProps.formValuesRecord);
      newFormValues.items[0].costCenterName = selectedCostCenter.name;
      newFormValues.items[0].costCenterHistoryId = selectedCostCenter.historyId
        ? selectedCostCenter.historyId
        : selectedCostCenter.id;
      dispatchProps.saveFormValuesRecord(newFormValues);
    }
    // redirect
    let url;
    if (ownProps.backType === 'report') {
      if (ownProps.reportId && ownProps.reportId !== 'null') {
        url = `/expense/report/edit/${ownProps.reportId}`;
      } else {
        url = '/expense/report/new';
      }
    } else if (ownProps.backType === 'record') {
      url = `/expense/record/detail/${ownProps.reportId}/${stateProps.formValuesRecord.recordId}`;
    }
    ownProps.history.push(url);
  },
  onClickIcon: (selectedCostCenter) => {
    const url = `/expense/cost-center/list/backType=${
      ownProps.backType
    }/targetDate=${ownProps.targetDate}/reportId=${
      ownProps.reportId || ownProps.reportId === 'null'
        ? ownProps.reportId
        : 'null'
    }/parentId=${selectedCostCenter.baseId}`;
    ownProps.history.push(url);
  },
  onClickBack: () => {
    ownProps.history.goBack();
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      dispatch(
        getCostCenterList(
          props.userSetting.companyId,
          props.parentId,
          props.targetDate
        )
      );
      dispatch(searchCostCenter(props.userSetting.companyId, props.targetDate));
    },
  })
)(LevelList): React.ComponentType<Object>);
