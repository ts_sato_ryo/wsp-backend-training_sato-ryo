// @flow
import { isEmpty, cloneDeep, find } from 'lodash';

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';
import { withFormik } from 'formik';

import lifecycle from '../../../concerns/lifecycle';
import schema from '../../../schema/RecordNewSchema';
import RecordNewPage from '../../../components/pages/expense/Record/New/General';
import {
  newRecord,
  RECORD_TYPE,
  RECEIPT_TYPE,
} from '../../../../domain/models/exp/Record';
import {
  type FilePreview,
  getFilePreview,
} from '../../../../domain/models/exp/receipt-library/list';
import {
  createRecord,
  deleteRecord,
} from '../../../action-dispatchers/expense/Record';
import { save as saveReport } from '../../../action-dispatchers/expense/ReportDetail';
import { getTaxTypeList } from '../../../action-dispatchers/expense/TaxType';
import {
  uploadReceipts,
  getBase64files,
} from '../../../action-dispatchers/expense/Receipt';
import { getCustomHints } from '../../../action-dispatchers/expense/CustomHint';
import { actions as formValueAction } from '../../../modules/expense/ui/general/formValues';
import { actions as rateAction } from '../../../modules/expense/ui/general/rate';
import { actions as isShowDialogAction } from '../../../modules/expense/ui/general/isShowDialog';
import { actions as readOnlyAction } from '../../../modules/expense/ui/general/readOnly';
import { actions as customHintUIActions } from '../../../modules/expense/ui/customHint/list';
import { calculateTax } from '../../../../domain/models/exp/TaxType';
import AppPermissionUtil from '../../../../commons/utils/AppPermissionUtil';
import {
  mapStateToProps as mapReportTypeSelectionStateToProps,
  mapDispatchToProps as mapReportTypeSelectionDispatchToProps,
  mergeProps as mergeReportTypeSelectionProps,
} from './ReportTypeSelectionContainer';
import FileUtil from '../../../../commons/utils/FileUtil';

import STATUS from '../../../../domain/models/approval/request/Status';
import { searchExpenseType } from '../../../action-dispatchers/expense/ExpenseType';

const mapStateToProps = (state) => {
  const { useExpense, employeeId, currencyId, companyId } = state.userSetting;
  return {
    companyId,
    activeHints: state.expense.ui.customHint.list,
    customHints: state.expense.entities.customHint,
    formValues: state.expense.ui.general.formValues,
    taxTypeList: state.expense.entities.taxType,
    records: state.expense.entities.recordList,
    report: state.expense.entities.report,
    currencyDecimalPlace: state.userSetting.currencyDecimalPlaces,
    currencySymbol: state.userSetting.currencySymbol,
    rate: state.expense.ui.general.rate,
    isShowSaveDialog: state.expense.ui.general.isShowDialog,
    readOnly: state.expense.ui.general.readOnly,
    hasPermissionError: AppPermissionUtil.checkPermissionError(
      useExpense,
      employeeId,
      currencyId
    ),
    expenseTypeList: state.expense.entities.expenseTypeList,
    ...mapReportTypeSelectionStateToProps(state),
  };
};

const mapDispatchToProps = {
  saveFormValues: formValueAction.save,
  clearFormValues: formValueAction.clear,
  setRate: rateAction.set,
  showSaveDialog: isShowDialogAction.visible,
  hideSaveDialog: isShowDialogAction.unvisible,
  isReadOnly: readOnlyAction.set,
  createRecord,
  deleteRecord,
  getBase64files,
  uploadReceipts,
  getTaxTypeList,
  saveReport,
  onClickHint: customHintUIActions.set,
  resetCustomHint: customHintUIActions.clear,
  getCustomHints,
  searchExpenseType,
  ...mapReportTypeSelectionDispatchToProps,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  isNotEditable:
    ownProps.type === 'reportList' &&
    [STATUS.Pending, STATUS.Approved].includes(stateProps.report.status),
  onSearchClick: () => {
    ownProps.history.push(`/expense/expense-type/list/${RECORD_TYPE.General}`);
  },
  onClickEditButton: () => {
    dispatchProps.isReadOnly(false);
  },

  onDeleteClick: () => {
    if (ownProps.type === 'recordList') {
      dispatchProps.deleteRecord(ownProps.recordId).then(() => {
        ownProps.history.push(`/expense/record/list`);
      });
    } else if (ownProps.type === 'reportList') {
      // change save report into just deleteRecord
      dispatchProps.deleteRecord(ownProps.recordId).then(() => {
        dispatchProps.clearFormValues();
        dispatchProps.resetCustomHint();
        ownProps.history.push(`/expense/report/detail/${ownProps.reportId}`);
      });
    }
  },
  onDialogLeftClick: () => {
    dispatchProps
      .getAccountingPeriodList(stateProps.userSetting.companyId)
      .then(() => {
        dispatchProps.hideSaveDialog();
        dispatchProps.showReportTypeSelectionDialog(true);
      });
  },

  onDialogRightClick: () => {
    ownProps.history.push('/expense/record/new/general');
    dispatchProps.hideSaveDialog();
  },

  onDialogCloseClick: () => {
    ownProps.history.push('/expense/record/list');
    dispatchProps.hideSaveDialog();
  },
  onBackClick: () => {
    dispatchProps.clearFormValues();
    dispatchProps.resetCustomHint();
    dispatchProps.isReadOnly(true);
    if (ownProps.type === 'recordList') {
      ownProps.history.push(`/expense/record/list`);
    } else if (ownProps.type === 'reportList') {
      ownProps.history.push(`/expense/report/detail/${ownProps.reportId}`);
    }
  },
  onClickSearchCustomEI: (
    customExtendedLookupId: string,
    customExtendedItemId: string,
    customExtendedItemName: string,
    index: string
  ) => {
    const recordId = ownProps.recordId || 'null';
    ownProps.history.push(
      `/expense/customExtendedItem/list/backType=record/reportId=null/recordId=${recordId}/index=${index}/customExtendedItemLookupId=${customExtendedLookupId}/customExtendedItemId=${customExtendedItemId}/customExtendedItemName=${customExtendedItemName}`
    );
  },
  onClickSearchCostCenter: (recordDate: string) => {
    const reportId = ownProps.reportId;
    ownProps.history.push(
      `/expense/cost-center/list/backType=record/targetDate=${recordDate}/reportId=${reportId}`
    );
  },
  onClickSearchJob: (recordDate: string) => {
    const reportId = ownProps.reportId;
    ownProps.history.push(
      `/expense/job/list/backType=record/targetDate=${recordDate}/reportId=${reportId}`
    );
  },
  ...mergeReportTypeSelectionProps(stateProps, dispatchProps, ownProps),
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  withFormik({
    validationSchema: schema,
    mapPropsToValues: (props) => {
      const { type, formValues } = props;

      // if exist formValues, always use formValues.
      if (!isEmpty(formValues)) {
        return formValues;
      }

      if (type === 'recordList') {
        // from RecordList
        return find(props.records, { recordId: props.recordId });
      } else if (props.type === 'reportList') {
        // from ReportList
        return find(props.report.records, { recordId: props.recordId });
      }
      // new
      const record = newRecord('', '', RECORD_TYPE.General);
      record.items[0].taxTypeBaseId = 'default';
      record.items[0].taxTypeHistoryId = 'default';
      return record;
    },

    handleSubmit: (values, { props }) => {
      const saveValues = cloneDeep(values);
      if (saveValues.items[0].taxTypeBaseId === 'default') {
        const taxType = props.taxTypeList[1];
        saveValues.items[0].taxTypeBaseId = taxType.baseId;
        saveValues.items[0].taxTypeHistoryId = taxType.historyId;
        saveValues.items[0].taxTypeName = taxType.name;
      }
      if (props.type === 'new' || props.type === 'recordList') {
        props.createRecord(saveValues).then(() => {
          props.clearFormValues();
          props.resetCustomHint();
          if (props.type === 'new') {
            props.showSaveDialog();
          }
          props.isReadOnly(true);
        });
      } else if (props.type === 'reportList') {
        saveValues.reportId = props.report.reportId;
        props.createRecord(saveValues).then(() => {
          props.clearFormValues();
          props.resetCustomHint();
          props.isReadOnly(true);
        });
      }
    },
  }),
  lifecycle({
    componentDidMount: (_dispatch: Dispatch<any>, props) => {
      if (isEmpty(props.customHint)) {
        props.getCustomHints(props.companyId);
      }
      if (props.type === 'new') {
        props.isReadOnly(false);
        props.clearReportTypeSelection();
      }
      // This screen was opened by recordList or reportList.
      // Or you came back from the expense type screen.

      let getImage = new Promise((resolve) => resolve({}));
      // get receipt preview if any
      const needImageData =
        props.values.fileAttachment !== RECEIPT_TYPE.NotUsed &&
        props.values.receiptFileId &&
        !props.values.receiptData;

      if (needImageData) {
        getImage = getFilePreview(props.values.receiptFileId).then(
          (result: FilePreview) => {
            const fileBody = result.fileBody;
            const dataType = FileUtil.getMIMEType(result.fileType);
            const url = `data:${dataType};base64,${fileBody}`;
            const fileName = FileUtil.getOriginalFileNameWithoutPrefix(
              result.title
            );
            return { receiptData: url, fileName };
          }
        );
      }
      if (
        !isEmpty(props.values) &&
        props.values.recordDate &&
        props.values.items[0].expTypeId
      ) {
        Promise.all([
          props.getTaxTypeList(
            props.values.items[0].expTypeId,
            props.values.recordDate
          ),
          props.searchExpenseType(null, null, props.values.recordDate),
          getImage,
        ]).then(([res, expTypeRes, imageData]) => {
          if (res[0].payload.length === 0) {
            return;
          }
          // Make sure that you can use the item you are currently setting for the specified date.
          // Also recalculate the amount

          // tax selection
          let taxType;
          let rate = res[0].payload[0].rate;
          let taxTypeBaseId = 'default';
          let taxTypeHistoryId = 'default';
          let taxTypeName = 'default';
          if (props.values.items[0].taxTypeBaseId) {
            taxType = find(res[0].payload, {
              baseId: props.values.items[0].taxTypeBaseId,
            });
            if (taxType) {
              rate = taxType ? taxType.rate : 0;
              taxTypeBaseId = taxType.baseId;
              taxTypeHistoryId = taxType.historyId;
              taxTypeName = taxType.name;
            }
          }
          props.setRate(rate);

          let amount = props.values.items[0].amount;
          const expenseTypeList = expTypeRes[0].payload;
          const selectedExpenseType = find(expenseTypeList, {
            id: props.values.items[0].expTypeId,
          });

          let values = cloneDeep(props.values);
          // fixed allowance part
          if (
            selectedExpenseType &&
            selectedExpenseType.recordType === RECORD_TYPE.FixedAllowanceSingle
          ) {
            amount = selectedExpenseType.fixedAllowanceSingleAmount;
          }
          else if (
            selectedExpenseType &&
            selectedExpenseType.recordType === RECORD_TYPE.FixedAllowanceMulti &&
            !props.values.items[0].fixedAllowanceOptionId
          ) {
            if (!props.readOnly) {
              amount = 0;
            }

            values.amount = amount;
            values.items[0].amount = amount;
          }

          // tax calculation part
          if (amount !== 0) {
            const taxRes = calculateTax(
              rate,
              amount,
              props.currencyDecimalPlace
            );

            values.amount = amount;
            values.withoutTax = taxRes.amountWithoutTax;
            values.items[0].amount = amount;
            values.items[0].withoutTax = taxRes.amountWithoutTax;
            values.items[0].gstVat = taxRes.gstVat;
            values.items[0].taxTypeBaseId = taxTypeBaseId;
            values.items[0].taxTypeHistoryId = taxTypeHistoryId;
            values.items[0].taxTypeName = taxTypeName;
          }

          values = Object.assign(values, imageData);
          props.setValues(values);
        });
      }
    },
  })
)(RecordNewPage): React.ComponentType<Object>);
