// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';

import { type State } from '../../../modules';
import { selectors } from '../../../modules/expense/entities/report';
import { status } from '../../../modules/selector';
import { RECORD_TYPE } from '../../../../domain/models/exp/Record';

import STATUS from '../../../../domain/models/approval/request/Status';

import {
  initialize,
  remove,
} from '../../../action-dispatchers/expense/ReportDetail';
import Component, {
  type Props,
} from '../../../components/pages/expense/Report/Detail';

const mapStateToProps = (state: State) => ({
  reportNo: state.expense.entities.report.reportNo,
  subject: state.expense.entities.report.subject,
  status: status(state.expense.entities.report.status),
  accountingPeriod: state.expense.entities.report.accountingPeriod,
  accountingDate: state.expense.entities.report.accountingDate,
  reportId: state.expense.entities.report.reportId,
  reportTypeName: state.expense.entities.report.expReportTypeName,
  costCenterCode: state.expense.entities.report.costCenterCode,
  costCenterName: state.expense.entities.report.costCenterName,
  jobCode: state.expense.entities.report.jobCode,
  jobName: state.expense.entities.report.jobName,
  remarks: state.expense.entities.report.remarks,
  useFileAttachment: state.expense.entities.report.useFileAttachment,
  attachedFileData: state.expense.entities.report.attachedFileData,
  fileName: state.expense.entities.report.fileName,
  attachedFileVerId: state.expense.entities.report.attachedFileVerId,
  attachedFileId: state.expense.entities.report.attachedFileId,
  extendedItemTexts: selectors.extendedItemTexts(state.expense.entities.report),
  extendedItemPicklists: selectors.extendedItemPicklists(
    state.expense.entities.report
  ),
  extendedItemLookup: selectors.extendedItemLookup(
    state.expense.entities.report
  ),
  extendedItemDate: selectors.extendedItemDate(state.expense.entities.report),
  totalAmountOfRecords: selectors.totalAmount(state.expense.entities.report),
  records: state.expense.entities.report.records,
  currencySymbol: state.userSetting.currencySymbol,
  currencyDecimalPlaces: state.userSetting.currencyDecimalPlaces,
  isEditable: [
    STATUS.NotRequested,
    STATUS.Canceled,
    STATUS.Removed,
    STATUS.Rejected,
  ].includes(state.expense.entities.report.status),
});

const mapDispatchToProps = {
  remove,
};

const mergeProps = (stateProps, dispatchProps, ownProps): Props => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickRecord: (recordId: string, recordType: string) => {
    if (recordType === RECORD_TYPE.TransitJorudanJP) {
      // push to jorudan item container
      ownProps.history.push(
        `/expense/record/jorudan-detail/${recordId}/${stateProps.reportId}`
      );
    } else {
      // push to normal item container
      ownProps.history.push(
        `/expense/record/detail/${ownProps.reportId}/${recordId}`
      );
    }
  },
  onClickSubmit: () => {
    ownProps.history.replace(`/expense/report/submit`);
  },
  onClickReportList: () => {
    ownProps.history.push(`/expense/report/list`);
  },
  onClickDelete: () => {
    dispatchProps.remove(ownProps.reportId).then(() => {
      ownProps.history.replace(`/expense/report/list`);
    });
  },
  onClickEdit: () => {
    ownProps.history.replace(`/expense/report/edit/${ownProps.reportId}`);
  },
  onBackClick: () => {
    ownProps.history.goBack();
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount(dispatch: Dispatch<any>, props) {
      dispatch(initialize(props.reportId));
    },
  })
)(Component): React.ComponentType<Object>);
