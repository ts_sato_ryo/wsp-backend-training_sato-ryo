// @flow
import _ from 'lodash';
import { compose } from 'redux';
import { connect } from 'react-redux';
import lifecycle from '../../../concerns/lifecycle';

import ReportTypeSelectDialog from '../../../components/pages/commons/ReportTypeSelect';
import { actions as isShowReportTypeSelectionAction } from '../../../modules/expense/ui/reportTypeSelectDialog';
import { convertAccountingDateId } from '../../../../domain/models/exp/AccountingPeriod';
import { getAccountingPeriodList } from '../../../action-dispatchers/expense/AccountingPeriod';
import { getExpReportTypeList } from '../../../action-dispatchers/expense/ExpReportType';
import { actions as reportTypeSelectionAction } from '../../../modules/expense/ui/report/reportTypeSelection';
import * as RecordListActions from '../../../action-dispatchers/expense/RecordList';

export const mapStateToProps = (state: any) => ({
  userSetting: state.userSetting,
  isShowReportTypeSelection: state.expense.ui.reportTypeSelectDialog.isShow,
  accountingPeriodList: state.expense.entities.accountingPeriod.filter(
    (ap) => ap.active
  ),
  accountingDate: state.expense.ui.report.reportTypeSelection.accountingDate,
  expReportTypeList: state.expense.entities.expReportType,
});

export const mapDispatchToProps = {
  showReportTypeSelectionDialog: isShowReportTypeSelectionAction.show,
  getAccountingPeriodList,
  getExpReportTypeList,
  getFilteredRecordList: RecordListActions.filterRecordList,
  saveReportTypeSelection: reportTypeSelectionAction.save,
  clearReportTypeSelection: reportTypeSelectionAction.clear,
};

export const mergeProps = (
  stateProps: any,
  dispatchProps: any,
  ownProps: any
) => ({
  ...stateProps,
  ...dispatchProps,
  onClickCloseReportTypeSelection: () => {
    ownProps.history.push('/expense/record/list');
    dispatchProps.showReportTypeSelectionDialog(false);
  },

  onChangeAccountingPeriod: (accountingPeriodId) => {
    const dates = convertAccountingDateId(
      accountingPeriodId,
      stateProps.accountingPeriodList
    );
    const { startDate, endDate } = dates;
    dispatchProps.getExpReportTypeList(
      stateProps.userSetting.companyId,
      true,
      stateProps.userSetting.employeeId,
      true,
      startDate,
      endDate
    );
  },

  onChangeAccountingDate: (accountingDate) => {
    dispatchProps.saveReportTypeSelection(undefined, undefined, accountingDate);
    dispatchProps.getExpReportTypeList(
      stateProps.userSetting.companyId,
      true,
      stateProps.userSetting.employeeId,
      true,
      accountingDate
    );
  },

  onClickNext: (accountingPeriodId, reportTypeId) => {
    const selectedReportType = stateProps.expReportTypeList.find(
      (rt) => rt.id === reportTypeId
    );
    const expTypeListIds = selectedReportType.expTypeList
      ? selectedReportType.expTypeList.map((exp) => exp.expTypeId)
      : [];
    const dates =
      accountingPeriodId &&
      convertAccountingDateId(
        accountingPeriodId,
        stateProps.accountingPeriodList
      );
    const { startDate, endDate } = dates;
    Promise.all([
      dispatchProps.getFilteredRecordList(
        stateProps.userSetting.employeeId,
        expTypeListIds,
        startDate,
        endDate
      ),
      dispatchProps.saveReportTypeSelection(accountingPeriodId, reportTypeId),
    ]).then(() => {
      ownProps.history.push(`/expense/record/list/select`);
      dispatchProps.showReportTypeSelectionDialog(false);
    });
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({})
)(ReportTypeSelectDialog): React.ComponentType<Object>);
