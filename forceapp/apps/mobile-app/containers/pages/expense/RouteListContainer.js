// @flow
import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';
import lifecycle from '../../../concerns/lifecycle';

import RouteListPage from '../../../components/pages/expense/Route/List';
import { searchRouteWithParam } from '../../../action-dispatchers/expense/RouteSearch';

const mapStateToProps = (state) => ({
  routeFormParams: state.expense.pages.routeFormPage,
  routeResults: state.expense.entities.routeResults,
  defaultRouteOptions: state.expense.pages.defaultRouteOptions,
  apiErrors: state.mobileCommons.error,
});

const mapDispatchToProps = {
  searchRouteWithParam,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  searchRouteWithParam: () => {
    dispatchProps
      .searchRouteWithParam(stateProps.routeFormParams)
      .then((hasResults) => {
        if (!hasResults) {
          ownProps.history.goBack();
        }
      });
  },
  onClickBackButton: () => {
    ownProps.history.push(`/expense/route/new`);
  },
  onClickListItem: (routeNo: number) => {
    ownProps.history.push(`/expense/route/list/item/${routeNo}`);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      // if cache results exist, do not call api again.
      if (!props.routeResults.route.storeData) {
        props.searchRouteWithParam();
      }
    },
  })
)(RouteListPage): React.ComponentType<Object>);
