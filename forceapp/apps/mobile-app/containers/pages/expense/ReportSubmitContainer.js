// @flow

import { compose } from 'redux';
import { connect } from 'react-redux';
import { type ContextRouter } from 'react-router-dom';

import msg from '../../../../commons/languages';
import lifecycle from '../../../concerns/lifecycle';
import { type State } from '../../../modules';
import { submit } from '../../../action-dispatchers/expense/ReportDetail';

import Component, {
  type Props,
} from '../../../components/pages/commons/SubmitWithCommentPage';

type OwnProps = { ...ContextRouter };

const mapStateToProps = (state: State, ownProps: OwnProps) => ({
  ...ownProps,
  title: msg().Appr_Lbl_SubmitComment,
  submitLabel: msg().Appr_Lbl_Submit,
  backLabel: msg().Exp_Lbl_Report,
  avatarUrl: state.userSetting.photoUrl,
  reportId: state.expense.entities.report.reportId,
});

const mapDispatchToProps = {
  submit,
};

const mergeProps = (stateProps, dispatchProps, ownProps: OwnProps): Props => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickBack: () => {
    ownProps.history.replace(`/expense/report/detail/${stateProps.reportId}`);
  },
  onClickSubmit: (comment: string) => {
    dispatchProps
      .submit(stateProps.reportId, comment)
      .then(() => {
        ownProps.history.replace('/expense/report/list');
      })
      .catch(() => {
        ownProps.history.replace(
          `/expense/report/detail/${stateProps.reportId}`
        );
      });
  },
  getBackLabel: () => {
    return msg().Exp_Lbl_Report;
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({})
)(Component): React.ComponentType<Object>);
