// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';
import lifecycle from '../../../concerns/lifecycle';
import { RECORD_TYPE } from '../../../../domain/models/exp/Record';
import RecordListPage from '../../../components/pages/expense/Record/List';
import * as RecordListActions from '../../../action-dispatchers/expense/RecordList';
import { actions as formValueAction } from '../../../modules/expense/ui/report/formValues';
import { actions as reportTypeSelectionAction } from '../../../modules/expense/ui/report/reportTypeSelection';

import {
  mapStateToProps as mapReportTypeSelectionStateToProps,
  mapDispatchToProps as mapReportTypeSelectionDispatchToProps,
  mergeProps as mergeReportTypeSelectionProps,
} from './ReportTypeSelectionContainer';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    records: state.expense.entities.recordList,
    onClickRecord: (recordId: string, recordType: string) => {
      if (recordType === RECORD_TYPE.TransitJorudanJP) {
        // push to jorudan item container
        ownProps.history.push(`/expense/record/jorudan-detail/${recordId}`);
      } else {
        // push to normal item container
        ownProps.history.push(`/expense/record/detail/${recordId}`);
      }
    },
    formValues: state.expense.ui.report.formValues,
    report: state.expense.entities.report,
    reportTypeSelection: state.expense.ui.report.reportTypeSelection,
    ...mapReportTypeSelectionStateToProps(state),
  };
};

const mapDispatchToProps = {
  clearFormValues: formValueAction.clear,
  ...mapReportTypeSelectionDispatchToProps,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickCreateReport: () => {
    dispatchProps
      .getAccountingPeriodList(stateProps.userSetting.companyId)
      .then(dispatchProps.showReportTypeSelectionDialog(true));
  },
  ...mergeReportTypeSelectionProps(stateProps, dispatchProps, ownProps),
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props: any) => {
      dispatch(RecordListActions.initRecordList(props.userSetting.employeeId));
      dispatch(formValueAction.clear()); // for not taking over the report form information.
      dispatch(reportTypeSelectionAction.clear());
    },
  })
)(RecordListPage): React.ComponentType<Object>);
