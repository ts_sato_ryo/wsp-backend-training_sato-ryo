// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';
import RecordListItemPage from '../../../components/pages/expense/Record/List/Item';

import { actions as recordItemActions } from '../../../modules/expense/entities/recordItem';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    recordItem: state.expense.entities.recordItem,
  };
};

const mapDispatchToProps = (_dispatch: Dispatch<*>) => ({});

const mergeProps = (stateProps, _dispatchProps, ownProps) => ({
  ...stateProps,
  ...ownProps,
});

export default (compose(
  lifecycle({
    componentDidMount: (dispatch, props) => {
      recordItemActions.get(props.recordNo);
    },
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
)(RecordListItemPage): React.ComponentType<Object>);
