// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';
import RecordListSelectPage from '../../../components/pages/expense/Record/List/Select';

import { actions as RecordListSelectAction } from '../../../modules/expense/ui/record/list/select';
import { actions as ReportActions } from '../../../modules/expense/entities/report';
import { actions as reportTypeSelectionAction } from '../../../modules/expense/ui/report/reportTypeSelection';
import * as RecordListActions from '../../../action-dispatchers/expense/RecordList';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    userSetting: state.userSetting,
    canCreateReport: state.expense.ui.record.list.select.selectedIds.length > 0,
    records: state.expense.entities.recordList,
    flagsById: state.expense.ui.record.list.select.flagsById,
    reportTypeSelection: state.expense.ui.report.reportTypeSelection,
  };
};

const mapDispatchToProps = {
  clearReport: ReportActions.clear,
  onClickItem: RecordListSelectAction.toggle,
  clearRecordListSelection: RecordListSelectAction.clear,
  clearReportTypeSelection: reportTypeSelectionAction.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickCancel: () => {
    dispatchProps.clearRecordListSelection();
    dispatchProps.clearReportTypeSelection();
    ownProps.history.push(`/expense/record/list`);
  },
  onClickCreateReport: () => {
    dispatchProps.clearReport();
    ownProps.history.push(`/expense/report/new`);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props: any) => {
      // Do not initialize record list when it's redirected from report type selection dialog
      const reportTypeId =
        props.reportTypeSelection && props.reportTypeSelection.reportTypeId;
      if (!reportTypeId) {
        dispatch(
          RecordListActions.initRecordListSelect(
            props.isReselect,
            props.userSetting.employeeId
          )
        );
      }
    },
  })
)(RecordListSelectPage): React.ComponentType<Object>);
