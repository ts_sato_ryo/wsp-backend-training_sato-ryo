// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../../concerns/lifecycle';
import RecordPage from '../../../../components/pages/approval/expense/Report/Record';

import { setImageData } from '../../../../action-dispatchers/approval/ExpenseReport';

const filterRecord = (report, recordId) => {
  if (!report || !report.records || !recordId) {
    return null;
  }
  const selectRecords = report.records.filter(
    (record) => record.recordId === recordId
  );
  if (selectRecords.length > 0) {
    return selectRecords[0];
  }
  return null;
};

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    currencySymbol: state.userSetting.currencySymbol,
    currencyDecimalPlaces: state.userSetting.currencyDecimalPlaces,
    report: state.approval.entities.expense.report,
    record: filterRecord(
      state.approval.entities.expense.report,
      ownProps.recordId
    ),
    onClickBack: () => {
      ownProps.history.push(
        `/approval/list/select/expense/${ownProps.requestId}`
      );
    },
  };
};

const mapDispatchToProps = {
  setImageData,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      if (props.record.receiptFileId) {
        props.setImageData(props.record.receiptFileId, props.recordId);
      }
    },
  })
)(RecordPage): React.ComponentType<Object>);
