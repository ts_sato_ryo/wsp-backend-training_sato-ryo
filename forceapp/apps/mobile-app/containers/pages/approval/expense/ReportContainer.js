// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../../concerns/lifecycle';
import ReportPage from '../../../../components/pages/approval/expense/Report';

import {
  getExpRequest,
  setImageData,
} from '../../../../action-dispatchers/approval/ExpenseReport';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    currencySymbol: state.userSetting.currencySymbol,
    currencyDecimalPlaces: state.userSetting.currencyDecimalPlaces,
    report: state.approval.entities.expense.report,
    selection: state.approval.entities.list.select,
    onClickRecord: (recordId: string) => {
      ownProps.history.push(
        `/approval/list/select/expense/${ownProps.requestId}/detail/${recordId}`
      );
    },
    onClickApproveButton: () => {
      ownProps.history.push(`/approval/approve/expense/${ownProps.requestId}`);
    },
    onClickRejectButton: () => {
      ownProps.history.push(`/approval/reject/expense/${ownProps.requestId}`);
    },
  };
};

const mapDispatchToProps = (_dispatch: Dispatch<*>) => ({});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickBack: () => {
    ownProps.history.push(`/approval/list/type/${stateProps.selection}`);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      dispatch(getExpRequest(props.requestId)).then((report) => {
        if (report.attachedFileVerId) {
          // set report attachment for preview if any
          dispatch(
            setImageData(report.attachedFileVerId, report.requestId, true)
          );
        }
      });
    },
  })
)(ReportPage): React.ComponentType<Object>);
