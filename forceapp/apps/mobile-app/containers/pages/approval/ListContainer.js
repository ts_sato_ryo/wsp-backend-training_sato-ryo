// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';
import ListPage from '../../../components/pages/approval/ListPage';
import AppPermissionUtil from '../../../../commons/utils/AppPermissionUtil';

import { fetchApprRequestList } from '../../../action-dispatchers/approval/List';
import { actions as listActions } from '../../../modules/approval/entities/list';

const mapStateToProps = (state, ownProps) => {
  const { useExpense, employeeId, currencyId } = state.userSetting;
  return {
    ...ownProps,
    // TODO
    approvalList: state.approval.entities.list.approvalList,
    userSetting: state.userSetting,
    hasPermissionError: AppPermissionUtil.checkPermissionError(
      useExpense,
      employeeId,
      currencyId,
      true
    ),
    onClickRefresh: (requestType: string) => {
      ownProps.history.push(`/approval/list/type/${requestType}`);
    },
  };
};

const mapDispatchToProps = {
  setSelect: listActions.setSelect,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickPushHisotry: (
    requestId: string,
    requestType: string,
    selection?: string
  ) => {
    dispatchProps.setSelect(selection);
    const target = requestType === 'expense' ? 'expense' : 'attendance';
    ownProps.history.push(`/approval/list/select/${target}/${requestId}`);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, _props) => {
      dispatch(fetchApprRequestList());
    },
  })
)(ListPage): React.ComponentType<Object>);
