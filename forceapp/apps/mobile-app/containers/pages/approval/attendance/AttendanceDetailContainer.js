// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../../concerns/lifecycle';
import AttDetailPage from '../../../../components/pages/approval/attendance/Request';
import * as detailSelectors from '../../../../../approvals-pc/modules/entities/att/detail/selectors';

import { getAttRequest } from '../../../../action-dispatchers/approval/AttendanceRequest';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  request: state.approval.entities.attendance.request,
  selection: state.approval.entities.list.select,
  detailList: detailSelectors.detailListSelector(
    state.approval.entities.attendance.request
  ),
  onClickApproveButton: () => {
    ownProps.history.push(`/approval/approve/attendance/${ownProps.requestId}`);
  },
  onClickRejectButton: () => {
    ownProps.history.push(`/approval/reject/attendance/${ownProps.requestId}`);
  },
});

const mapDispatchToProps = {
  getAttRequest,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickBack: () => {
    ownProps.history.push(`/approval/list/type/${stateProps.selection}`);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      props.getAttRequest(props.requestId);
    },
  })
)(AttDetailPage): React.ComponentType<Object>);
