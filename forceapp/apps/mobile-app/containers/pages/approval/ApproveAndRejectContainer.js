// @flow

import { compose } from 'redux';
import { connect } from 'react-redux';
import { type ContextRouter } from 'react-router-dom';

import msg from '../../../../commons/languages';
import lifecycle from '../../../concerns/lifecycle';
import { approve } from '../../../action-dispatchers/approval/Approve';
import { reject } from '../../../action-dispatchers/approval/Reject';
import { type State } from '../../../modules';

import Component, {
  type Props,
} from '../../../components/pages/commons/SubmitWithCommentPage';

type OwnProps = {
  approve: boolean,
  requestId: string,
  type: string,
  ...ContextRouter,
};

const mapStateToProps = (state: State, ownProps: OwnProps) => ({
  ...ownProps,
  title: ownProps.approve
    ? msg().Appr_Lbl_ApproveComment
    : msg().Appr_Lbl_RejectComment,
  submitLabel: ownProps.approve
    ? msg().Appr_Btn_Approve
    : msg().Appr_Btn_Reject,
  avatarUrl: state.userSetting.photoUrl,
});

const mapDispatchToProps = {
  approve,
  reject,
};

const mergeProps = (stateProps, dispatchProps, ownProps: OwnProps): Props => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickBack: () => {
    ownProps.history.goBack();
  },
  onClickSubmit: (comment: string) => {
    if (ownProps.approve) {
      dispatchProps.approve([ownProps.requestId], comment).then(() => {
        ownProps.history.push('/approval/list');
      });
    } else {
      dispatchProps.reject([ownProps.requestId], comment).then(() => {
        ownProps.history.push('/approval/list');
      });
    }
  },
  getBackLabel: () => {
    if (stateProps.type === 'attendance') {
      return msg().Att_Lbl_Request;
    }
    return msg().Exp_Lbl_Report;
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({})
)(Component): React.ComponentType<Object>);
