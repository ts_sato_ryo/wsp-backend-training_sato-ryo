// @flow
import { bindActionCreators, compose, type Dispatch } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';

import { CLOCK_TYPE } from '../../../../domain/models/attendance/DailyStampTime';
import { LOCATION_FETCH_STATUS } from '../../../../domain/models/Location';
import { type State } from '../../../modules';
import {
  initialize,
  stamp,
  onToggleSendLocation,
} from '../../../action-dispatchers/attendance/timeStamp';
import { actions as uiActions } from '../../../modules/attendance/timeStamp/ui';

import TimeStampPage from '../../../components/pages/attendance/TimeStampPage';
import onResume from '../../../concerns/onResume';

const mapStateToProps = (state: State) => ({
  timeLocale: state.userSetting.language,

  showLocationToggleButton: state.userSetting.requireLocationAtMobileStamp,
  willSendLocation: state.attendance.timeStamp.ui.willSendLocation,

  fetchStatus: state.mobileCommons.location.fetchStatus,
  locationFetchTime: state.mobileCommons.location.fetchTime,
  latitude: state.mobileCommons.location.latitude,
  longitude: state.mobileCommons.location.longitude,

  message: state.attendance.timeStamp.ui.message,

  isEnableStartStamp:
    state.attendance.timeStamp.entities.dailyStampTime.isEnableStartStamp,
  isEnableEndStamp:
    state.attendance.timeStamp.entities.dailyStampTime.isEnableEndStamp,
  isEnableRestartStamp:
    state.attendance.timeStamp.entities.dailyStampTime.isEnableRestartStamp,
  defaultAction:
    state.attendance.timeStamp.entities.dailyStampTime.defaultAction,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  ...bindActionCreators(
    {
      onClickStartStampButton: stamp,
      onClickEndStampButton: stamp,
      onChangeMessageField: uiActions.editMessage,
      onClickToggleButton: onToggleSendLocation,
      onClickRefresh: initialize,
    },
    dispatch
  ),
});

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickRefresh: () => {
    dispatchProps.onClickRefresh(stateProps.willSendLocation);
  },
  onClickStartStampButton: () =>
    dispatchProps.onClickStartStampButton(
      {
        mode: stateProps.isEnableRestartStamp
          ? CLOCK_TYPE.CLOCK_REIN
          : CLOCK_TYPE.CLOCK_IN,
        message: stateProps.message,
        latitude:
          stateProps.showLocationToggleButton && stateProps.willSendLocation
            ? stateProps.latitude
            : // $FlowFixMe
              null,
        longitude:
          stateProps.showLocationToggleButton && stateProps.willSendLocation
            ? stateProps.longitude
            : // $FlowFixMe
              null,
      },
      stateProps.showLocationToggleButton,
      stateProps.willSendLocation,
      stateProps.showLocationToggleButton &&
        stateProps.fetchStatus === LOCATION_FETCH_STATUS.Success
    ),
  onClickEndStampButton: () =>
    dispatchProps.onClickEndStampButton(
      {
        mode: CLOCK_TYPE.CLOCK_OUT,
        message: stateProps.message,
        latitude:
          stateProps.showLocationToggleButton && stateProps.willSendLocation
            ? stateProps.latitude
            : // $FlowFixMe v0.85
              null,
        longitude:
          stateProps.showLocationToggleButton && stateProps.willSendLocation
            ? stateProps.longitude
            : // $FlowFixMe v0.85
              null,
      },
      stateProps.showLocationToggleButton,
      stateProps.willSendLocation,
      stateProps.showLocationToggleButton &&
        stateProps.fetchStatus === LOCATION_FETCH_STATUS.Success
    ),
  onClickToggleButton: (isChecked: boolean) =>
    dispatchProps.onClickToggleButton(isChecked),
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  onResume((dispatch: Dispatch<any>, props: { willSendLocation: boolean }) => {
    dispatch(initialize(props.willSendLocation));
  }),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>) => {
      dispatch(initialize(true));
    },
  })
)(TimeStampPage): React.ComponentType<Object>);
