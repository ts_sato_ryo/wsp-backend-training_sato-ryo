// @flow

import { bindActionCreators, compose, type Dispatch } from 'redux';
import { connect } from 'react-redux';

import { type Timesheet } from '../../../../domain/models/attendance/Timesheet';
import { MAX_STANDARD_REST_TIME_COUNT } from '../../../../domain/models/attendance/RestTime';

import lifecycle from '../../../concerns/lifecycle';
import TimesheetDailyPage from '../../../components/pages/attendance/TimesheetDailyPage';

import * as TimesheetActions from '../../../action-dispatchers/attendance/timesheet';
import { actions as UiDailyEditingActions } from '../../../modules/attendance/timesheet/ui/daily/editing';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  currentDate: state.attendance.timesheet.ui.daily.paging.current,
  prevDate: state.attendance.timesheet.ui.daily.paging.prev,
  nextDate: state.attendance.timesheet.ui.daily.paging.next,
  record: state.attendance.timesheet.ui.daily.editing,
  isEditable: state.attendance.timesheet.ui.daily.editing.canEdit,
  startDate: state.attendance.timesheet.entities.startDate,
  endDate: state.attendance.timesheet.entities.endDate,
  timesheet: state.attendance.timesheet.entities,
  minRestTimesCount: 1,
  maxRestTimesCount: MAX_STANDARD_REST_TIME_COUNT,
});

const mapDispatchToProps = (_dispatch: Dispatch<any>) =>
  bindActionCreators(
    {
      onChangeStartTime: UiDailyEditingActions.updateStartTime,
      onChangeEndTime: UiDailyEditingActions.updateEndTime,
      onChangeRestTime: UiDailyEditingActions.updateRestTime,
      onClickRemoveRestTime: UiDailyEditingActions.deleteRestTime,
      onClickAddRestTime: UiDailyEditingActions.addRestTime,
      onChangeOtherRestTime: UiDailyEditingActions.updateRestHours,
      onChangeRemarks: UiDailyEditingActions.updateRemarks,
      onClickSave: TimesheetActions.saveAttDailyRecord,
      fetchTimesheet: (currentDate: string) => (
        thunkDispatch: Dispatch<any>
      ) => {
        thunkDispatch(TimesheetActions.initializeMonthly(currentDate));
      },
    },
    _dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onChangeRestTimeStartTime: (
    idx: number,
    startTime: number | null,
    endTime: number | null
  ) => dispatchProps.onChangeRestTime(idx, { startTime, endTime }),
  onChangeRestTimeEndTime: (
    idx: number,
    startTime: number | null,
    endTime: number | null
  ) =>
    dispatchProps.onChangeRestTime(idx, {
      startTime,
      endTime,
    }),
  onClickSave: () => dispatchProps.onClickSave(stateProps.record),
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: {
        targetDate: string,
        timesheet: Timesheet,
      }
    ) => {
      dispatch(
        TimesheetActions.initializeDaily(props.targetDate, props.timesheet)
      );
    },
    componentWillUnmount: () => {},
  })
)(TimesheetDailyPage): React.ComponentType<Object>);
