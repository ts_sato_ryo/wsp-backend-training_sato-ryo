// @flow

import { compose, type Dispatch } from 'redux';
import { connect } from 'react-redux';
import { type RouterHistory } from 'react-router';

import lifecycle from '../../../concerns/lifecycle';
import DailyRequestListPage, {
  type Props,
} from '../../../components/pages/attendance/DailyRequestListPage';

import { type State as Timesheet } from '../../../modules/attendance/timesheet/entities';
import { type State as LatestRequests } from '../../../modules/attendance/dailyRequest/entities/latestRequests';
import { type State as AvailableRequests } from '../../../modules/attendance/dailyRequest/entities/availableRequests';
import { type State } from '../../../modules';
import { type $ExtractReturn } from '../../../../commons/utils/TypeUtil';
import { type AttDailyRequest } from '../../../../domain/models/attendance/AttDailyRequest';
import { CODE } from '../../../../domain/models/attendance/AttDailyRequestType';
import * as actions from '../../../action-dispatchers/attendance/dailyRequest';

type OwnProps = {|
  targetDate: string,
  timesheet: Timesheet,
  history: RouterHistory,
|};

const mapStateToProps = (state: State) => ({
  availableRequests: (state.attendance.dailyRequest.entities
    .availableRequests: AvailableRequests),
  latestRequests: (state.attendance.dailyRequest.entities
    .latestRequests: LatestRequests),
  isLocked: (state.attendance.timesheet.entities.isLocked: boolean),
});

const mapDispatchToProps = {};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps: typeof mapDispatchToProps,
  ownProps: OwnProps
): Props => ({
  ...stateProps,
  ...dispatchProps,
  onClickRequest: (dailyRequest: AttDailyRequest) => {
    const path = dailyRequest.id ? dailyRequest.id : 'new';
    switch (dailyRequest.requestTypeCode) {
      case CODE.Absence:
        ownProps.history.replace(
          `/attendance/daily-requests/${ownProps.targetDate}/absence/${path}`
        );
        break;
      case CODE.Leave:
        ownProps.history.replace(
          `/attendance/daily-requests/${ownProps.targetDate}/leave/${path}`
        );
        break;
      case CODE.HolidayWork:
        ownProps.history.replace(
          `/attendance/daily-requests/${
            ownProps.targetDate
          }/holiday-work/${path}`
        );
        break;
      case CODE.EarlyStartWork:
        ownProps.history.replace(
          `/attendance/daily-requests/${
            ownProps.targetDate
          }/early-start-work/${path}`
        );
        break;
      case CODE.OvertimeWork:
        ownProps.history.replace(
          `/attendance/daily-requests/${
            ownProps.targetDate
          }/overtime-work/${path}`
        );
        break;
      case CODE.Direct:
        ownProps.history.replace(
          `/attendance/daily-requests/${ownProps.targetDate}/direct/${path}`
        );
        break;
      case CODE.Pattern:
        ownProps.history.replace(
          `/attendance/daily-requests/${ownProps.targetDate}/pattern/${path}`
        );
        break;
      default:
        break;
    }
  },
});

export default (compose(
  connect(
    (state: State, ownProps: OwnProps): OwnProps => ({
      ...ownProps,
      timesheet: (state.attendance.timesheet.entities: Timesheet),
    })
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props: OwnProps) => {
      dispatch(actions.initialize(props.targetDate, props.timesheet));
    },
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
)(DailyRequestListPage): React.ComponentType<Object>);
