// @flow

import { compose } from 'redux';
import { connect } from 'react-redux';

import { create as createEarlyStartWorkRequest } from '../../../../../domain/models/attendance/AttDailyRequest/EarlyStartWorkRequest';

import { type $ExtractReturn } from '../../../../../commons/utils/TypeUtil';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';

import lifecycle from '../../../../concerns/lifecycle';
import guard from '../../../../../commons/concerns/guard';
import Component from '../../../../components/pages/attendance/DailyRequestDetails/EarlyStartWorkRequestPage';

import { type State } from '../../../../modules';
import * as selectors from '../../../../modules/attendance/selector';
import { actions } from '../../../../modules/attendance/dailyRequest/ui/detail';
import { actions as requestActions } from '../../../../modules/attendance/dailyRequest/ui/requests/earlyStartWorkRequest';

type OwnProps = $ReadOnly<{|
  id?: string,
  targetDate: string,
|}>;

const mapStateToProps = (state: State, ownProps: OwnProps) => {
  return {
    ...ownProps,
    readOnly: !state.attendance.dailyRequest.ui.detail.isEditing,
    request:
      state.attendance.dailyRequest.ui.requests.earlyStartWorkRequest.request,
    validation: state.attendance.dailyRequest.ui.validation,
    timesheet: state.attendance.timesheet.entities,
    originalRequest: ownProps.id
      ? selectors.selectLatestRequest(state.attendance, ownProps.id)
      : selectors.selectAvailableRequest(state.attendance, CODE.EarlyStartWork),
  };
};

const mapDispatchToProps = {
  updateHandler: requestActions.update,
};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps: typeof mapDispatchToProps
) => ({
  ...stateProps,
  onChangeStartTime: (val: number | null) => {
    dispatchProps.updateHandler('startTime', val);
  },
  onChangeEndTime: (_: number | null, val: number | null) => {
    dispatchProps.updateHandler('endTime', val);
  },
  onChangeRemarks: (val: string) => {
    dispatchProps.updateHandler('remarks', val);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: $ExtractReturn<typeof mergeProps>
    ) => {
      const { originalRequest, timesheet, targetDate } = props;
      if (!originalRequest) {
        return;
      }
      const request = createEarlyStartWorkRequest(
        originalRequest,
        timesheet.workingType,
        targetDate
      );
      dispatch(actions.initialize(request));
      dispatch(requestActions.initialize(request));
    },
  }),
  guard(
    (props: $ExtractReturn<typeof mapStateToProps>) => props.request !== null
  )
)(Component): React.ComponentType<Object>);
