// @flow

import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';

import STATUS from '../../../../../domain/models/approval/request/Status';
import { create as createPatternRequest } from '../../../../../domain/models/attendance/AttDailyRequest/PatternRequest';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';

import { type $ExtractReturn } from '../../../../../commons/utils/TypeUtil';

import lifecycle from '../../../../concerns/lifecycle';
import guard from '../../../../../commons/concerns/guard';
import Component from '../../../../components/pages/attendance/DailyRequestDetails/PatternRequestPage';

import { type State } from '../../../../modules';
import { catchApiError } from '../../../../modules/commons/error';
import { withLoading } from '../../../../modules/commons/loading';
import * as selectors from '../../../../modules/attendance/selector';
import { patternOptions } from '../../../../modules/attendance/dailyRequest/ui/requests/selector';
import { actions } from '../../../../modules/attendance/dailyRequest/ui/detail';
import { actions as requestActions } from '../../../../modules/attendance/dailyRequest/ui/requests/patternRequest';

import AttDailyPatternRepository from '../../../../../repositories/AttDailyPatternRepository';

type OwnProps = $ReadOnly<{|
  id?: string,
  targetDate: string,
|}>;

const mapStateToProps = (state: State, ownProps: OwnProps) => {
  return {
    ...ownProps,
    readOnly: !state.attendance.dailyRequest.ui.detail.isEditing,
    request: state.attendance.dailyRequest.ui.requests.patternRequest.request,
    validation: state.attendance.dailyRequest.ui.validation,
    patternOptions: patternOptions(state.attendance.dailyRequest.ui.requests),
    selectedAttPattern:
      state.attendance.dailyRequest.ui.requests.patternRequest
        .selectedAttPattern,
    originalRequest: ownProps.id
      ? selectors.selectLatestRequest(state.attendance, ownProps.id)
      : selectors.selectAvailableRequest(state.attendance, CODE.Pattern),
  };
};

const mapDispatchToProps = {
  updateHandler: requestActions.update,
};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps: typeof mapDispatchToProps
) => ({
  ...stateProps,
  onChangeStartDate: () => {},
  onChangeEndDate: (val: string) => {
    dispatchProps.updateHandler('endDate', val);
  },
  onChangePatternCode: (val: string | null) => {
    dispatchProps.updateHandler('patternCode', val);
  },
  onChangeRemarks: (val: string) => {
    dispatchProps.updateHandler('remarks', val);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: $ExtractReturn<typeof mergeProps>
    ) => {
      const { originalRequest, targetDate } = props;

      if (!originalRequest) {
        return;
      }

      const appService = bindActionCreators(
        {
          withLoading,
          catchApiError,
        },
        dispatch
      );

      const dailyRequestService = bindActionCreators(actions, dispatch);
      const requestService = bindActionCreators(requestActions, dispatch);

      if (originalRequest.status === STATUS.NotRequested) {
        appService.withLoading(() =>
          AttDailyPatternRepository.search({ targetDate })
            .then((attPatterns) => {
              const request = createPatternRequest(
                originalRequest,
                attPatterns,
                targetDate
              );
              dailyRequestService.initialize(request);
              requestService.initialize(request, attPatterns);
            })
            .catch((err) => {
              appService.catchApiError(err, { isContinuable: true });
            })
        );
      } else {
        const request = createPatternRequest(originalRequest, null, targetDate);
        dailyRequestService.initialize(request);
        requestService.initialize(request);
      }
    },
  }),
  guard(
    (props: $ExtractReturn<typeof mapStateToProps>) => props.request !== null
  )
)(Component): React.ComponentType<Object>);
