// @flow

import { compose } from 'redux';
import { connect } from 'react-redux';

import { create as createDirectRequest } from '../../../../../domain/models/attendance/AttDailyRequest/DirectRequest';
import * as RestTime from '../../../../../domain/models/attendance/RestTime';

import { type $ExtractReturn } from '../../../../../commons/utils/TypeUtil';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';

import lifecycle from '../../../../concerns/lifecycle';
import guard from '../../../../../commons/concerns/guard';
import Component from '../../../../components/pages/attendance/DailyRequestDetails/DirectRequestPage';

import { type State } from '../../../../modules';
import * as selectors from '../../../../modules/attendance/selector';
import { actions } from '../../../../modules/attendance/dailyRequest/ui/detail';
import { actions as requestActions } from '../../../../modules/attendance/dailyRequest/ui/requests/directRequest';

type OwnProps = $ReadOnly<{|
  id?: string,
  targetDate: string,
|}>;

const mapStateToProps = (state: State, ownProps: OwnProps) => {
  return {
    ...ownProps,
    readOnly: !state.attendance.dailyRequest.ui.detail.isEditing,
    request: state.attendance.dailyRequest.ui.requests.directRequest.request,
    validation: state.attendance.dailyRequest.ui.validation,
    timesheet: state.attendance.timesheet.entities,
    originalRequest: ownProps.id
      ? selectors.selectLatestRequest(state.attendance, ownProps.id)
      : selectors.selectAvailableRequest(state.attendance, CODE.Direct),
  };
};

const mapDispatchToProps = {
  updateHandler: requestActions.update,
};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps: typeof mapDispatchToProps
) => ({
  ...stateProps,
  minRestTimesCount: 1,
  maxRestTimesCount: RestTime.MAX_STANDARD_REST_TIME_COUNT,
  onChangeStartDate: () => {},
  onChangeEndDate: (val: string) => {
    dispatchProps.updateHandler('endDate', val);
  },
  onChangeStartTime: (val: number | null) => {
    dispatchProps.updateHandler('startTime', val);
  },
  onChangeEndTime: (val: number | null) => {
    dispatchProps.updateHandler('endTime', val);
  },
  onChangeRestTime: (
    idx: number,
    startTime: number | null,
    endTime: number | null
  ) => {
    dispatchProps.updateHandler(
      'directApplyRestTimes',
      RestTime.update(stateProps.request.directApplyRestTimes, idx, {
        startTime,
        endTime,
      })
    );
  },
  onClickRemoveRestTime: (idx: number) => {
    dispatchProps.updateHandler(
      'directApplyRestTimes',
      RestTime.remove(stateProps.request.directApplyRestTimes, idx)
    );
  },
  onClickAddRestTime: () => {
    dispatchProps.updateHandler(
      'directApplyRestTimes',
      RestTime.push(stateProps.request.directApplyRestTimes)
    );
  },
  onChangeRemarks: (val: string) => {
    dispatchProps.updateHandler('remarks', val);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: $ExtractReturn<typeof mergeProps>
    ) => {
      const { originalRequest, timesheet, targetDate } = props;
      if (!originalRequest) {
        return;
      }
      const request = createDirectRequest(
        originalRequest,
        timesheet.workingType,
        targetDate
      );
      dispatch(actions.initialize(request));
      dispatch(requestActions.initialize(request));
    },
  }),
  guard(
    (props: $ExtractReturn<typeof mapStateToProps>) => props.request !== null
  )
)(Component): React.ComponentType<Object>);
