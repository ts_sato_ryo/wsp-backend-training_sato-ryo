// @flow

import { compose } from 'redux';
import { connect } from 'react-redux';

import { create as createHolidayWorkRequest } from '../../../../../domain/models/attendance/AttDailyRequest/HolidayWorkRequest';
import { create as createSubstituteLeaveType } from '../../../../../domain/models/attendance/SubstituteLeaveType';

import { type $ExtractReturn } from '../../../../../commons/utils/TypeUtil';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';

import lifecycle from '../../../../concerns/lifecycle';
import guard from '../../../../../commons/concerns/guard';
import Component from '../../../../components/pages/attendance/DailyRequestDetails/HolidayWorkRequestPage';

import { type State } from '../../../../modules';
import * as selectors from '../../../../modules/attendance/selector';
import { substituteLeaveTypeOptions } from '../../../../modules/attendance/dailyRequest/ui/requests/selector';
import { actions } from '../../../../modules/attendance/dailyRequest/ui/detail';
import { actions as requestActions } from '../../../../modules/attendance/dailyRequest/ui/requests/holidayWorkRequest';

type OwnProps = $ReadOnly<{|
  id?: string,
  targetDate: string,
|}>;

const mapStateToProps = (state: State, ownProps: OwnProps) => {
  return {
    ...ownProps,
    readOnly: !state.attendance.dailyRequest.ui.detail.isEditing,
    request:
      state.attendance.dailyRequest.ui.requests.holidayWorkRequest.request,
    substituteLeaveTypeList:
      state.attendance.dailyRequest.ui.requests.holidayWorkRequest
        .substituteLeaveTypeList,
    timesheet: state.attendance.timesheet.entities,
    typeOptions: substituteLeaveTypeOptions(
      state.attendance.dailyRequest.ui.requests
    ),
    originalRequest: ownProps.id
      ? selectors.selectLatestRequest(state.attendance, ownProps.id)
      : selectors.selectAvailableRequest(state.attendance, CODE.HolidayWork),
    validation: state.attendance.dailyRequest.ui.validation,
  };
};

const mapDispatchToProps = {
  updateHandler: requestActions.update,
};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps: typeof mapDispatchToProps
) => ({
  ...stateProps,
  onChangeStartDate: (val: string) => {
    dispatchProps.updateHandler('startDate', val);
  },
  onChangeStartTime: (val: number | null) => {
    dispatchProps.updateHandler('startTime', val);
  },
  onChangeEndTime: (_: number | null, val: number | null) => {
    dispatchProps.updateHandler('endTime', val);
  },
  onChangeSubstituteLeaveType: (val: string) => {
    dispatchProps.updateHandler('substituteLeaveType', val);
  },
  onChangeSubstituteDate: (val: string) => {
    dispatchProps.updateHandler('substituteDate', val);
  },
  onChangeRemarks: (val: string) => {
    dispatchProps.updateHandler('remarks', val);
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: $ExtractReturn<typeof mergeProps>
    ) => {
      const { originalRequest, timesheet, targetDate } = props;
      if (!originalRequest) {
        return;
      }
      const request = createHolidayWorkRequest(
        originalRequest,
        timesheet.workingType,
        targetDate
      );
      const substituteLeaveTypeList = createSubstituteLeaveType(
        request,
        timesheet.workingType
      );
      dispatch(actions.initialize(request));
      dispatch(requestActions.initialize(request, substituteLeaveTypeList));
    },
  }),
  guard(
    (props: $ExtractReturn<typeof mapStateToProps>) => props.request !== null
  )
)(Component): React.ComponentType<Object>);
