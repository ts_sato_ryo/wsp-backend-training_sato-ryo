// @flow
import { bindActionCreators, compose, type Dispatch } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../../concerns/lifecycle';
import guard from '../../../../../commons/concerns/guard';

import STATUS from '../../../../../domain/models/approval/request/Status';
import { type AttDailyRequest } from '../../../../../domain/models/attendance/AttDailyRequest';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';
import { create } from '../../../../../domain/models/attendance/AttDailyRequest/LeaveRequest';
import { catchApiError } from '../../../../modules/commons/error';
import { withLoading } from '../../../../modules/commons/loading';
import { actions } from '../../../../modules/attendance/dailyRequest/ui/detail';
import { actions as requestActions } from '../../../../modules/attendance/dailyRequest/ui/requests/leaveRequest';
import Repository from '../../../../../repositories/AttDailyLeaveRepository';

import { type $ExtractReturn } from '../../../../../commons/utils/TypeUtil';
import { type State } from '../../../../modules';
import {
  rangeOptions,
  leaveTypeOptions,
} from '../../../../modules/attendance/dailyRequest/ui/requests/selector';

import Component from '../../../../components/pages/attendance/DailyRequestDetails/LeaveRequestPage';

type OwnProps = {|
  id: string,
  targetDate: string,
|};

const mapStateToProps = (state: State, ownProps: OwnProps) => ({
  latestRequests: (state.attendance.dailyRequest.entities
    .latestRequests: AttDailyRequest[]),
  availableRequests: (state.attendance.dailyRequest.entities
    .availableRequests: AttDailyRequest[]),
  request: state.attendance.dailyRequest.ui.requests.leaveRequest.request,
  leaveTypeOptions: leaveTypeOptions(state.attendance.dailyRequest.ui.requests),
  rangeOptions: rangeOptions(state.attendance.dailyRequest.ui.requests),
  selectedAttLeave:
    state.attendance.dailyRequest.ui.requests.leaveRequest.selectedAttLeave,
  readOnly: !state.attendance.dailyRequest.ui.detail.isEditing,
  validation: state.attendance.dailyRequest.ui.validation,
  ownProps,
});

const mapDispatchToProps = {
  onChange: requestActions.update,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: $ExtractReturn<typeof mapStateToProps>
    ) => {
      const appService = bindActionCreators({ catchApiError }, dispatch);
      const dailyRequestService = bindActionCreators(actions, dispatch);
      const requestService = bindActionCreators(requestActions, dispatch);

      const request = props.ownProps.id
        ? props.latestRequests.find((r) => r.id === props.ownProps.id)
        : props.availableRequests.find((r) => r.requestTypeCode === CODE.Leave);
      if (!request) {
        return;
      }

      if (request.status === STATUS.NotRequested) {
        const initialize = () =>
          Repository.search({
            targetDate: props.ownProps.targetDate,
          }).then((attLeaveList) => {
            const leave = create(
              request,
              attLeaveList,
              props.ownProps.targetDate
            );
            dailyRequestService.initialize(leave);
            requestService.initialize(leave, attLeaveList);
          });

        dispatch(withLoading(initialize)).catch((e) =>
          appService.catchApiError(e)
        );
      } else {
        const leave = create(request, null, props.ownProps.targetDate);
        dailyRequestService.initialize(leave);
        requestService.initialize(leave);
      }
    },
  }),
  guard(
    (props: $ExtractReturn<typeof mapStateToProps>) => props.request !== null
  )
)(Component): React.ComponentType<Object>);
