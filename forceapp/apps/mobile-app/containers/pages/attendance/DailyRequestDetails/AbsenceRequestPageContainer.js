// @flow
import { compose, type Dispatch } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../../concerns/lifecycle';
import guard from '../../../../../commons/concerns/guard';

import { type AttDailyRequest } from '../../../../../domain/models/attendance/AttDailyRequest';
import { CODE } from '../../../../../domain/models/attendance/AttDailyRequestType';
import { create } from '../../../../../domain/models/attendance/AttDailyRequest/AbsenceRequest';
import { actions } from '../../../../modules/attendance/dailyRequest/ui/detail';
import { actions as requestActions } from '../../../../modules/attendance/dailyRequest/ui/requests/absenceRequest';

import { type $ExtractReturn } from '../../../../../commons/utils/TypeUtil';
import { type State } from '../../../../modules';

import Component from '../../../../components/pages/attendance/DailyRequestDetails/AbsenceRequestPage';

type OwnProps = $ReadOnly<{|
  id: string,
  targetDate: string,
|}>;

const mapStateToProps = (state: State, ownProps: OwnProps) => {
  return {
    ...ownProps,
    latestRequests: (state.attendance.dailyRequest.entities
      .latestRequests: AttDailyRequest[]),
    availableRequests: (state.attendance.dailyRequest.entities
      .availableRequests: AttDailyRequest[]),
    request: state.attendance.dailyRequest.ui.requests.absenceRequest.request,
    readOnly: !state.attendance.dailyRequest.ui.detail.isEditing,
    validation: state.attendance.dailyRequest.ui.validation,
  };
};

const mapDispatchToProps = {
  onChange: requestActions.update,
};

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps: typeof mapDispatchToProps
) => ({
  ...stateProps,
  onChangeEndDate: (value: string) => dispatchProps.onChange('endDate', value),
  onChangeReason: (value: string) => dispatchProps.onChange('reason', value),
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: * // FIXME: $ExtractReturn<typeof mergeProps> causes type error on mobile-app/concerns/lifecycle/index.js
    ) => {
      const request = props.id
        ? props.latestRequests.find((r) => r.id === props.id)
        : props.availableRequests.find(
            (r) => r.requestTypeCode === CODE.Absence
          );
      if (!request) {
        return;
      }

      const absence = create(request, props.targetDate);

      dispatch(actions.initialize(absence));
      dispatch(requestActions.initialize(absence));
    },
  }),
  guard((props: $ExtractReturn<typeof mergeProps>) => props.request !== null)
)(Component): React.ComponentType<Object>);
