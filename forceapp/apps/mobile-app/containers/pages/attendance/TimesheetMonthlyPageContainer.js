// @flow

import { type Dispatch, compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { type Timesheet } from '../../../../domain/models/attendance/Timesheet';

import lifecycle from '../../../concerns/lifecycle';
import TimesheetMonthlyPage from '../../../components/pages/attendance/TimesheetMonthlyPage';

import * as TimesheetActions from '../../../action-dispatchers/attendance/timesheet';
import onResume from '../../../concerns/onResume';

const mapStateToProps = (state, ownProps) => {
  const timesheet = state.attendance.timesheet.entities;
  const { recordAllRecordDates, recordsByRecordDate } = timesheet;
  return {
    ...ownProps,
    currentDate: state.attendance.timesheet.ui.monthly.paging.current,
    prevDate: state.attendance.timesheet.ui.monthly.paging.prev,
    nextDate: state.attendance.timesheet.ui.monthly.paging.next,
    yearMonthOptions: state.attendance.timesheet.ui.monthly.paging.pages,
    timesheet: state.attendance.timesheet.entities,
    records: recordAllRecordDates.map((k) => recordsByRecordDate[k]),
    changeMonthHandler: (value: string) => {
      ownProps.history.push(`/attendance/timesheet-monthly/${value}`);
    },
    changeDateHandler: (value: string) => {
      ownProps.history.push(`/attendance/timesheet-daily/${value}`);
    },
  };
};

const mapDispatchToProps = (dispatch: Dispatch<any>) =>
  bindActionCreators(
    {
      onClickRefresh: (currentDate: string) => (thunkDispatch: Dispatch<any>) =>
        thunkDispatch(TimesheetActions.initializeMonthly(currentDate)),
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  disabledPrevDate: !stateProps.prevDate,
  disabledNextDate: !stateProps.nextDate,
  onChangeMonth: stateProps.changeMonthHandler,
  onClickRefresh: () => dispatchProps.onClickRefresh(stateProps.currentDate),
  onClickPrevMonth: () => stateProps.changeMonthHandler(stateProps.prevDate),
  onClickNextMonth: () => stateProps.changeMonthHandler(stateProps.nextDate),
  onClickMonthlyListItem: stateProps.changeDateHandler,
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  onResume((dispatch: Dispatch<any>, props: { targetDate: string }) =>
    dispatch(TimesheetActions.initializeMonthly(props.targetDate))
  ),
  lifecycle({
    componentDidMount: (
      dispatch: Dispatch<any>,
      props: {
        targetDate: string,
        timesheet: Timesheet,
      }
    ) => {
      dispatch(
        TimesheetActions.initializeMonthly(props.targetDate, props.timesheet)
      );
    },
    componentWillUnmount: () => {},
  })
)(TimesheetMonthlyPage): React.ComponentType<Object>);
