// @flow

import { type Dispatch, compose } from 'redux';
import { connect } from 'react-redux';

import lifecycle from '../../../concerns/lifecycle';
import SampleaPage from '../../../components/pages/sample/SamplePage';

const mapStateToProps = (_state, ownProps) => ({ ...ownProps });

const mapDispatchToProps = (_dispatch: Dispatch<*>) => ({});

const mergeProps = (_stateProps, _dispatchProps, ownProps) => ({ ...ownProps });

export default (compose(
  lifecycle({
    componentDidMount: (_dispatch, props: { color: 'string' }) => {
      console.log(props);
      window.alert('Hello!');
    },
    componentWillUnmount: () => window.alert('By!'),
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
)(SampleaPage): React.ComponentType<Object>);
