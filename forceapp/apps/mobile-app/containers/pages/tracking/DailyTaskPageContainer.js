// @flow

import { connect } from 'react-redux';
import { type Dispatch, compose, bindActionCreators } from 'redux';
import mapValues from 'lodash/mapValues';
import isNil from 'lodash/isNil';

import { type State } from '../../../modules';
import lifecycle from '../../../concerns/lifecycle';

import DateUtil from '../../../../commons/utils/DateUtil';

import {
  initialize,
  editTask,
  saveTask,
} from '../../../action-dispatchers/tracking/dailyTask';
import {
  taskTimes,
  toggleInputMode,
  type State as DailyTaskState,
} from '../../../modules/tracking/entity/dailyTask';
import {
  toggleEditing,
  deleteTask,
} from '../../../modules/tracking/ui/dailyTask';

import Component, {
  type Props,
} from '../../../components/pages/tracking/DailyTaskPage';

const returnVoid = (f: Function) => {
  return (...args) => {
    f(...args);
  };
};

type DispatchProps = {|
  dispatch: Dispatch<any>,
  bindActions: (Object) => Object,
|};

const mapStateToProps = (state: State): State => state;
const mapDispatchToProps = (dispatch: Dispatch<any>): DispatchProps => ({
  dispatch,
  bindActions: (actions) =>
    mapValues(bindActionCreators(actions, dispatch), returnVoid),
});

const mergeProps = (
  state: State,
  { bindActions }: DispatchProps,
  ownProps
): Props => {
  const listEditing = state.tracking.ui.dailyTask.isEditing;
  const dailyTask: DailyTaskState = listEditing
    ? state.tracking.ui.dailyTask.dailyTask
    : state.tracking.entity.dailyTask;

  return {
    // State
    listEditing,
    today: ownProps.date,
    taskList: dailyTask.taskList,
    isTemporaryWorkTime: dailyTask.isTemporaryWorkTime,
    realWorkTime: dailyTask.realWorkTime,
    taskTimes: taskTimes(dailyTask),
    hasDefaultJob: dailyTask.hasDefaultJob,
    hasMultipleRatios:
      state.tracking.entity.dailyTask.taskList.filter(
        (task) => !task.isDirectInput
      ).length > 1,
    disabled: isNil(dailyTask.targetDate) || dailyTask.targetDate === '',

    // Actions
    ...bindActions({
      toggleDirectInput: (id, _isDirectInput) => toggleInputMode(id),
      editTaskTime: (id, taskTime) => editTask(id, { taskTime }),
      editRatio: (id, ratio) => editTask(id, { ratio }),
      deleteTask: (id) => deleteTask(id),
      save: () => saveTask(dailyTask),
      onToggleEditing: () => toggleEditing(dailyTask),
    }),

    // Routing actions
    onChangeDate: (date: string) => {
      ownProps.history.replace(
        `/tracking/tracking-daily/${DateUtil.formatISO8601Date(date)}`
      );
    },
    onClickAddJob: () => {
      ownProps.history.replace(
        `/tracking/tracking-daily/${DateUtil.formatISO8601Date(
          ownProps.date
        )}/jobs`
      );
    },
    onClickPrevDate: () => {
      const prevDate = DateUtil.addDays(ownProps.date, -1);
      ownProps.history.replace(
        `/tracking/tracking-daily/${DateUtil.formatISO8601Date(prevDate)}`
      );
    },
    onClickNextDate: () => {
      const nextDate = DateUtil.addDays(ownProps.date, 1);
      ownProps.history.replace(
        `/tracking/tracking-daily/${DateUtil.formatISO8601Date(nextDate)}`
      );
    },
    onClickRefresh: () => {
      ownProps.history.replace(
        `/tracking/tracking-daily/${DateUtil.formatISO8601Date(ownProps.date)}`
      );
    },
  };
};

export default (compose(
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      if (props.willFetchData) {
        dispatch(initialize({ targetDate: props.date }));
      }
    },
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )
)(Component): React.ComponentType<Object>);
