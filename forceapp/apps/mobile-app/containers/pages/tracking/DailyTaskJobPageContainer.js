// @flow

import { connect } from 'react-redux';
import { type Dispatch, compose, bindActionCreators } from 'redux';
import mapValues from 'lodash/mapValues';

import DateUtil from '../../../../commons/utils/DateUtil';
import { type State } from '../../../modules';
import lifecycle from '../../../concerns/lifecycle';

import {
  jobOptionList,
  workCategoryOptionList,
} from '../../../modules/tracking/selector';

import {
  initialize,
  save,
  discard,
  selectJob,
  selectWorkCategory,
  editTaskTime,
} from '../../../action-dispatchers/tracking/dailyTaskJob';

import Component, {
  type Props,
} from '../../../components/pages/tracking/DailyTaskJobPage';

const mergeProps = (stateProps, dispatchProps, ownProps): Props => ({
  // State
  jobs: jobOptionList(stateProps.tracking),
  workCategories: workCategoryOptionList(stateProps.tracking),
  selectedJobId: stateProps.tracking.ui.dailyTaskJob.jobId || '',
  selectedWorkCategoryId:
    stateProps.tracking.ui.dailyTaskJob.workCategoryId || '',
  taskTime: stateProps.tracking.ui.dailyTaskJob.taskTime || '',

  // Actions
  ...dispatchProps.bindActionCreators({
    onSelectJob: (e: SyntheticEvent<HTMLSelectElement>) =>
      selectJob(e.currentTarget.value),
    onSelectWorkCategory: (e: SyntheticEvent<HTMLSelectElement>) =>
      selectWorkCategory(e.currentTarget.value),
    onChangeTaskTime: (taskTime: ?number) => editTaskTime(taskTime),
  }),
  onClickDiscard: () => {
    dispatchProps.dispatch(discard());
    ownProps.history.replace(
      `/tracking/tracking-daily/${DateUtil.formatISO8601Date(
        ownProps.date
      )}?mode=1`
    );
  },
  onClickSave: () => {
    dispatchProps
      .dispatch(
        save(
          stateProps.tracking.ui.dailyTaskJob,
          stateProps.tracking.entity.dailyTask
        )
      )
      .then(() => {
        ownProps.history.replace(
          `/tracking/tracking-daily/${DateUtil.formatISO8601Date(
            ownProps.date
          )}`
        );
      });
  },
});

export default (compose(
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, props) => {
      dispatch(initialize(props.date));
    },
  }),
  connect(
    (state: State) => state,
    (dispatch: Dispatch<any>) => ({
      dispatch,
      bindActionCreators: (actions) => {
        type ReturnVoid = <V, T>((args: T) => V) => (T) => void;
        type Actions = $ObjMap<typeof actions, ReturnVoid>;

        return (mapValues(
          bindActionCreators(actions, dispatch),
          (f: Function) => (...args) => {
            f(...args);
          }
        ): Actions);
      },
    }),
    mergeProps
  )
)(Component): React.ComponentType<Object>);
