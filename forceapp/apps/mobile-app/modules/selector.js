// @flow

import { createSelector } from 'reselect';

import msg from '../../commons/languages';
import { type Status } from '../../domain/models/approval/request/Status';
import { type State } from './index';

/**
 * Selectors depending on the root state.
 *
 * Note that you should NOT put selectors depending on a module.
 * If you want to write selectors depending on a module, then you should
 * put the selectors into a module.
 */

/* Status */

// $FlowFixMe
export const status = createSelector(
  (s) => s,
  (s: Status) => {
    const key = `Com_Status_${s}`;
    return msg()[key];
  }
);

/* Default Job  */

const selectDefaultJobId = (state: State): ?string =>
  (state.personalSetting.defaultJob || {}).id;

// $FlowFixMe
export const isDefaultJob = createSelector(
  selectDefaultJobId,
  (defaultJobId: ?string) => (jobId: string): boolean =>
    defaultJobId !== null &&
    defaultJobId !== undefined &&
    defaultJobId === jobId
);
