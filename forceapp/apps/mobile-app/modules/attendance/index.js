/* @flow */

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';

import dailyRequest from './dailyRequest';
import timesheet from './timesheet';
import timeStamp from './timeStamp';

const rootReducer = { timesheet, timeStamp, dailyRequest };

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>({
  dailyRequest,
  timesheet,
  timeStamp,
});
