// @flow

import { type AttDailyRequest } from '../../../domain/models/attendance/AttDailyRequest';
import { type Code } from '../../../domain/models/attendance/AttDailyRequestType';

import { type State } from './index';

export const selectAvailableRequest = (
  state: State,
  requestTypeCode: Code
): ?AttDailyRequest =>
  state.dailyRequest.entities.availableRequests.find(
    (request) => request.requestTypeCode === requestTypeCode
  ) || null;

export const selectLatestRequest = (
  state: State,
  id: string
): ?AttDailyRequest =>
  state.dailyRequest.entities.latestRequests.find(
    (request) => request.id === id
  ) || null;
