// @flow
import STATUS, {
  type Status,
} from '../../../../domain/models/approval/request/Status';
import {
  type Timesheet,
  getAttDailyRecordByDate,
  getAttDailyRequestByRecordDate,
} from '../../../../domain/models/attendance/Timesheet';
import { defaultValue as workingTypeDefaultValue } from '../../../../domain/models/attendance/WorkingType';
import {
  shouldInput as shouldInputAttDailyRecord,
  canEdit as canEditAttDailyRecord,
} from '../../../../domain/models/attendance/AttDailyRecord';
import {
  createAttDailyAttentions,
  CODE,
} from '../../../../domain/models/attendance/AttDailyAttention';
import TextUtil from '../../../../commons/utils/TextUtil';
import TimeUtil from '../../../../commons/utils/TimeUtil';
import msg from '../../../../commons/languages';
import * as AttDailyRequest from '../../../../domain/models/attendance/AttDailyRequest';
import * as LeaveRequest from '../../../../domain/models/attendance/AttDailyRequest/LeaveRequest';

type AttDailyRecord = {|
  ...$ElementType<$PropertyType<Timesheet, 'recordsByRecordDate'>, string>,
  shouldInput: boolean,
  canEdit: boolean,
  effectualLeaveRequests: LeaveRequest.LeaveRequest[],
  hasAbsenceRequest: boolean,
  remarkableRequestStatus: {
    count: number,
    status: Status,
  } | null,
|};

export type State = {|
  ...Timesheet,
  recordsByRecordDate: { [string]: AttDailyRecord },
|};

const ACTIONS = {
  FETCH_SUCCESS:
    'MOBILE_APP/MODULES/ATTENDANCE/TIMESHEET/ENTITIES/FETCH_SUCCESS',
  CLEAR: 'MOBILE_APP/MODULES/ATTENDANCE/TIMESHEET/ENTITIES/CLEAR',
};

export const actions = {
  fetchSuccess: (timesheet: Timesheet) => ({
    type: ACTIONS.FETCH_SUCCESS,
    payload: timesheet,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

const initialState: State = {
  id: '',
  recordAllRecordDates: [],
  recordsByRecordDate: {},
  periods: [],
  requestTypes: {},
  requestAllIds: [],
  requestsById: {},
  employeeName: '',
  departmentName: '',
  workingTypeName: '',
  startDate: '',
  endDate: '',
  requestId: '',
  status: STATUS.NotRequested,
  approver01Name: '',
  isLocked: true,
  workingType: {
    ...workingTypeDefaultValue,
  },
  isAllAbsent: false,
};

export default (state: State = initialState, action: *): State => {
  const { type, payload } = action;
  switch (type) {
    case ACTIONS.FETCH_SUCCESS:
      const timesheet = payload;
      const recordsByRecordDate = timesheet.recordAllRecordDates.reduce(
        (obj, key) => {
          const shouldInput = shouldInputAttDailyRecord(key, timesheet);
          const canEdit = canEditAttDailyRecord(key, timesheet, shouldInput);
          const requests = getAttDailyRequestByRecordDate(key, timesheet);
          const effectualLeaveRequests = AttDailyRequest.getEffectualLeaveRequests(
            requests
          );
          const hasAbsenceRequest = AttDailyRequest.hasEffectualAbsenceRequest(
            requests
          );
          const remarkableRequestStatus = AttDailyRequest.getRemarkableRequestStatus(
            key,
            timesheet
          );
          const record = getAttDailyRecordByDate(key, timesheet);
          let attentionMessages = [''];
          if (record) {
            attentionMessages = createAttDailyAttentions(record).map((item) => {
              switch (item.code) {
                case CODE.InsufficientRestTime:
                  return TextUtil.template(
                    msg().Att_Msg_InsufficientRestTime,
                    item.value
                  );
                case CODE.IneffectiveWorkingTime:
                  return TextUtil.template(
                    msg().Att_Msg_NotIncludeWorkingTime,
                    TimeUtil.toHHmm(item.value.fromTime),
                    TimeUtil.toHHmm(item.value.toTime)
                  );
                default:
                  return '';
              }
            });
          }
          obj[key] = {
            ...getAttDailyRecordByDate(key, timesheet),
            shouldInput,
            canEdit,
            effectualLeaveRequests,
            hasAbsenceRequest,
            remarkableRequestStatus,
            attentionMessages,
          };
          return obj;
        },
        {}
      );
      return {
        ...payload,
        recordsByRecordDate,
      };
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
};
