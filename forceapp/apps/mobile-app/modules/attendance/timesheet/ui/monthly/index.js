/* @flow */

import { combineReducers } from 'redux';

import paging from './paging';

export default combineReducers<Object, Object>({
  paging,
});
