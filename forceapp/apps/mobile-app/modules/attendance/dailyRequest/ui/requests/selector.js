// @flow

import { createSelector } from 'reselect';

import msg from '../../../../../../commons/languages';
import { ORDER_OF_RANGE_TYPES } from '../../../../../../domain/models/attendance/LeaveRange';
import {
  SUBSTITUTE_LEAVE_TYPE,
  ORDER_OF_SUBSTITUTE_LEAVE_TYPES,
} from '../../../../../../domain/models/attendance/SubstituteLeaveType';
import { type State } from './index';

// $FlowFixMe
export const rangeOptions = createSelector(
  (s: State) => s.leaveRequest.request,
  (s: State) => s.leaveRequest.selectedAttLeave,
  (request, selectedAttLeave) => {
    if (request === null) {
      return [];
    }
    if (selectedAttLeave === null) {
      return [];
    }

    const RANGE_LABEL_MAP = {
      Day: () => msg().Att_Lbl_FullDayLeave,
      AM: () => msg().Att_Lbl_FirstHalfOfDayLeave,
      PM: () => msg().Att_Lbl_SecondHalfOfDayLeave,
      Half: () => msg().Att_Lbl_HalfDayLeave,
      Time: () => msg().Att_Lbl_HourlyLeave,
    };

    return ORDER_OF_RANGE_TYPES.filter((rangeType) =>
      selectedAttLeave.ranges.includes(rangeType)
    ).map((rangeKey) => ({
      label: RANGE_LABEL_MAP[rangeKey](),
      value: rangeKey,
    }));
  }
);

// $FlowFixMe
export const leaveTypeOptions = createSelector(
  (s: State) => s.leaveRequest.attLeaveList,
  (attLeaveList) => {
    return (attLeaveList || []).map((leave) => ({
      label: leave.name,
      value: leave.code,
    }));
  }
);

// $FlowFixMe
export const substituteLeaveTypeOptions = createSelector(
  (s: State) => s.holidayWorkRequest.substituteLeaveTypeList,
  (substituteLeaveTypeList) => {
    if (substituteLeaveTypeList === null) {
      return [];
    }

    const LABEL_MAP = {
      [SUBSTITUTE_LEAVE_TYPE.None]: () =>
        msg().Att_Lbl_DoNotUseReplacementDayOff,
      [SUBSTITUTE_LEAVE_TYPE.Substitute]: () => msg().Att_Lbl_Substitute,
    };

    return ORDER_OF_SUBSTITUTE_LEAVE_TYPES.filter((type) =>
      substituteLeaveTypeList.includes(type)
    ).map((type) => ({
      label: LABEL_MAP[type](),
      value: type,
    }));
  }
);

// $FlowFixMe
export const patternOptions = createSelector(
  (s: State) => s.patternRequest.attPatternList,
  (attPatternList) => {
    return (attPatternList || []).map((pattern) => ({
      label: pattern.name,
      value: pattern.code,
    }));
  }
);
