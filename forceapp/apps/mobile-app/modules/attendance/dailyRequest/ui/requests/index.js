// @flow

import { combineReducers } from 'redux';

import absenceRequest from './absenceRequest';
import directRequest from './directRequest';
import earlyStartWorkRequest from './earlyStartWorkRequest';
import holidayWorkRequest from './holidayWorkRequest';
import leaveRequest from './leaveRequest';
import overtimeWorkRequest from './overtimeWorkRequest';
import patternRequest from './patternRequest';

import { type $State } from '../../../../../../commons/utils/TypeUtil';

const reducer = {
  absenceRequest,
  directRequest,
  earlyStartWorkRequest,
  holidayWorkRequest,
  leaveRequest,
  overtimeWorkRequest,
  patternRequest,
};

export type State = $State<typeof reducer>;

export default combineReducers<Object, Object>(reducer);
