/* @flow */

import { combineReducers } from 'redux';

import detail from './detail';
import validation from './validation';
import requests from './requests';

import { type $State } from '../../../../../commons/utils/TypeUtil';

const reducer = {
  detail,
  validation,
  requests,
};

export type State = $State<typeof reducer>;

export default combineReducers<Object, Object>(reducer);
