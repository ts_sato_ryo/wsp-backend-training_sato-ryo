// @flow

import { type AttDailyRecord } from '../../../../../domain/models/attendance/AttDailyRecord';
import { type Timesheet } from '../../../../../domain/models/attendance/Timesheet';
import {
  type AttDailyRequest,
  createFromDefaultValue,
  getAvailableRequestTypesAt,
} from '../../../../../domain/models/attendance/AttDailyRequest';
import { DisplayOrder } from '../../../../../domain/models/attendance/AttDailyRequestType';

// State

export type State = AttDailyRequest[];

const initialState: State = [];

// Actions

type Initialize = {|
  type: 'MOBILE-APP/MODULES/ATTENDANCE/DAILYREQUEST/ENTITIES/AVAILABLEREQUESTS/INITIALIZE',
  payload: {|
    timesheet: Timesheet,
    dailyRecord: AttDailyRecord,
  |},
|};

type Action = Initialize;

const INITIALIZE: $PropertyType<Initialize, 'type'> =
  'MOBILE-APP/MODULES/ATTENDANCE/DAILYREQUEST/ENTITIES/AVAILABLEREQUESTS/INITIALIZE';

export const actions = {
  initialize: <+TAttDailyRecord: AttDailyRecord, +TTimesheet: Timesheet>(
    dailyRecord: TAttDailyRecord,
    timesheet: TTimesheet
  ) => ({
    type: INITIALIZE,
    payload: {
      dailyRecord,
      timesheet,
    },
  }),
};

// Reducer

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case INITIALIZE: {
      const availableRequestTypes = getAvailableRequestTypesAt(
        action.payload.dailyRecord,
        action.payload.timesheet
      );
      return DisplayOrder.filter(
        (requestTypeCode) => availableRequestTypes[requestTypeCode]
      ).map((requestTypeCode) =>
        createFromDefaultValue(
          action.payload.timesheet.requestTypes,
          availableRequestTypes[requestTypeCode].code
        )
      );
    }

    default:
      return state;
  }
};
