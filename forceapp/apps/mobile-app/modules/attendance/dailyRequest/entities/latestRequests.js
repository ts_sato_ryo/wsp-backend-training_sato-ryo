// @flow

import { type AttDailyRecord } from '../../../../../domain/models/attendance/AttDailyRecord';
import { type Timesheet } from '../../../../../domain/models/attendance/Timesheet';
import {
  type AttDailyRequest,
  getLatestRequestsAt,
} from '../../../../../domain/models/attendance/AttDailyRequest';

// State

export type State = Array<AttDailyRequest>;

const initialState: State = [];

// Actions

type Initialize = {|
  type: 'MOBILE_APP/MODULES/ATTENDANCE/DAILYREQUEST/ENTITIES/LATESTREQUEST/INITIALIZE',
  payload: {
    timesheet: Timesheet,
    dailyRecord: AttDailyRecord,
  },
|};

type Action = Initialize;

const INITIALIZE: $PropertyType<Initialize, 'type'> =
  'MOBILE_APP/MODULES/ATTENDANCE/DAILYREQUEST/ENTITIES/LATESTREQUEST/INITIALIZE';

export const actions = {
  initialize: <+TAttDailyRecord: AttDailyRecord, +TTimesheet: Timesheet>(
    dailyRecord: TAttDailyRecord,
    timesheet: TTimesheet
  ): Initialize => ({
    type: INITIALIZE,
    payload: {
      timesheet,
      dailyRecord,
    },
  }),
};

// Reducer

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case INITIALIZE: {
      return [
        ...getLatestRequestsAt(
          action.payload.dailyRecord,
          action.payload.timesheet
        ),
      ];
    }

    default:
      return state;
  }
};
