// @flow

export type State = {|
  // 位置情報を送信するか
  willSendLocation: boolean,
  // 打刻メッセージ
  message: string,
|};

type Action = {
  type: string,
  payload?: any,
};

type SetWillSendLocationAction = {
  type: 'ATTENDANCE/TIME_STAMP/UI/SET_WILL_SEND_LOCATION',
  payload: boolean,
};

type EditMessageAction = {
  type: 'ATTENDANCE/TIME_STAMP/UI/EDIT_MESSAGE',
  payload: string,
};

type ClearMessageAction = {
  type: 'ATTENDANCE/TIME_STAMP/UI/CLEAR_MESSAGE',
};

const ACTIONS = {
  SET_WILL_SEND_LOCATION: 'ATTENDANCE/TIME_STAMP/UI/SET_WILL_SEND_LOCATION',
  EDIT_MESSAGE: 'ATTENDANCE/TIME_STAMP/UI/EDIT_MESSAGE',
  CLEAR_MESSAGE: 'ATTENDANCE/TIME_STAMP/UI/CLEAR_MESSAGE',
};

export const actions = {
  setWillSendLocation: (
    willSendLocation: boolean
  ): SetWillSendLocationAction => ({
    type: ACTIONS.SET_WILL_SEND_LOCATION,
    payload: willSendLocation,
  }),
  editMessage: (message: string): EditMessageAction => ({
    type: ACTIONS.EDIT_MESSAGE,
    payload: message,
  }),
  clearMessage: (): ClearMessageAction => ({
    type: ACTIONS.CLEAR_MESSAGE,
  }),
};

const initialState: State = {
  willSendLocation: false,
  message: '',
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case ACTIONS.SET_WILL_SEND_LOCATION:
      return {
        ...state,
        willSendLocation: action.payload,
      };
    case ACTIONS.EDIT_MESSAGE:
      return {
        ...state,
        message: action.payload,
      };
    case ACTIONS.CLEAR_MESSAGE:
      return {
        ...state,
        message: '',
      };
    default:
      return state;
  }
};
