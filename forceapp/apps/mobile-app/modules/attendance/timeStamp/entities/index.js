// @flow
import { combineReducers } from 'redux';

import dailyStampTime from './dailyStampTime';

export default combineReducers<Object, Object>({
  dailyStampTime,
});
