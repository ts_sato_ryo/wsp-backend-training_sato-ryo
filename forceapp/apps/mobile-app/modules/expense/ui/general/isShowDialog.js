// @flow
import { type Reducer } from 'redux';

export const ACTIONS = {
  VISIBLE: 'MODULES/EXPENSE/UI/GENERAL/IS_SHOW_DIALOG/VISIBLE',
  UNVISIBLE: 'MODULES/EXPENSE/UI/GENERAL/IS_SHOW_DIALOG/UNVISIBLE',
};

export const actions = {
  visible: () => ({
    type: ACTIONS.VISIBLE,
  }),
  unvisible: () => ({
    type: ACTIONS.UNVISIBLE,
  }),
};

const initialState = false;

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.VISIBLE:
      return true;
    case ACTIONS.UNVISIBLE:
      return false;
    default:
      return state;
  }
}: Reducer<boolean, any>);
