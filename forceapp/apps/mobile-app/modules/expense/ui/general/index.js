// @flow

import { combineReducers } from 'redux';

import formValues from './formValues';
import rate from './rate';
import isShowDialog from './isShowDialog';
import readOnly from './readOnly';

const rootReducer = {
  formValues,
  rate,
  isShowDialog,
  readOnly,
};

export default combineReducers<Object, Object>(rootReducer);
