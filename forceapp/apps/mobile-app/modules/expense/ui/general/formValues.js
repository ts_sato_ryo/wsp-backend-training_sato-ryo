// @flow
import { type Reducer, type Dispatch } from 'redux';

import { type Record } from '../../../../../domain/models/exp/Record';

export const ACTIONS = {
  SAVE_VALUES: 'MODULES/EXPENSE/UI/GENERAL/FORM_VALUES/SAVE_VALUES',
  CLEAR_FORM: 'MODULES/EXPENSE/UI/GENERAL/FORM_VALUES/CLEAR_FORM',
};

const formValues = (body: Record) => ({
  type: ACTIONS.SAVE_VALUES,
  payload: body,
});

const clearForm = () => ({
  type: ACTIONS.CLEAR_FORM,
  payload: {},
});

export const actions = {
  save: (values: Record) => (dispatch: Dispatch<any>): boolean | any => {
    return dispatch(formValues(values));
  },
  clear: () => (dispatch: Dispatch<any>): void | any => {
    return dispatch(clearForm());
  },
};

const initialState = {};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SAVE_VALUES:
      return action.payload;
    case ACTIONS.CLEAR_FORM:
      return action.payload;
    default:
      return state;
  }
}: Reducer<Record | {}, any>);
