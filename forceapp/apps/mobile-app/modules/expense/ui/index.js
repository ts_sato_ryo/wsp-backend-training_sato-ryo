// @flow

import { combineReducers } from 'redux';

import reportTypeSelectDialog from './reportTypeSelectDialog';
import customHint from './customHint';
import general from './general';
import report from './report';
import record from './record';

const rootReducer = {
  customHint,
  general,
  report,
  record,
  reportTypeSelectDialog,
};

export default combineReducers<Object, Object>(rootReducer);
