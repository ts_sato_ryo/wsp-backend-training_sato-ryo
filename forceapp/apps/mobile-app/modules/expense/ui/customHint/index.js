// @flow
import { combineReducers } from 'redux';
import list from './list';

const rootReducer = {
  list,
};

export default combineReducers<Object, Object>(rootReducer);
