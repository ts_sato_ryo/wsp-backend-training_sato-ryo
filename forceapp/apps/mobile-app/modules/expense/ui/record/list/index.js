// @flow

import { combineReducers } from 'redux';

import select from './select';

const rootReducer = {
  select,
};

export default combineReducers<Object, Object>(rootReducer);
