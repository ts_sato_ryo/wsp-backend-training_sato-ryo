// @flow
import { type Reducer, type Dispatch } from 'redux';

import { type Report } from '../../../../../domain/models/exp/Report';

export const ACTIONS = {
  SAVE_VALUES: 'MODULES/EXPENSE/UI/REPORT/FORM_VALUES/SAVE_VALUES',
  CLEAR_FORM: 'MODULES/EXPENSE/UI/REPORT/FORM_VALUES/CLEAR_FORM',
};

const formValues = (body: Report) => ({
  type: ACTIONS.SAVE_VALUES,
  payload: body,
});

const clearForm = () => ({
  type: ACTIONS.CLEAR_FORM,
  payload: {},
});

export const actions = {
  save: (values: Report) => (dispatch: Dispatch<any>): boolean | any => {
    return dispatch(formValues(values));
  },
  clear: () => (dispatch: Dispatch<any>): void | any => {
    return dispatch(clearForm());
  },
};

const initialState = {};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SAVE_VALUES:
      return action.payload;
    case ACTIONS.CLEAR_FORM:
      return action.payload;
    default:
      return state;
  }
}: Reducer<Report | {}, any>);
