// @flow

import { combineReducers } from 'redux';

import formValues from './formValues';
import reportTypeSelection from './reportTypeSelection';

const rootReducer = {
  formValues,
  reportTypeSelection,
};

export default combineReducers<Object, Object>(rootReducer);
