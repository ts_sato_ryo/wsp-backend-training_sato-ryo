// @flow
import { type Reducer, type Dispatch } from 'redux';
import { cloneDeep, merge } from 'lodash';

import { type Report } from '../../../../../domain/models/exp/Report';

export const ACTIONS = {
  SAVE_VALUES: 'MODULES/EXPENSE/UI/REPORT/SELECTED_REPORT_TYPE/SAVE_VALUES',
  CLEAR_VALUES: 'MODULES/EXPENSE/UI/REPORT/SELECTED_REPORT_TYPE/CLEAR_VALUES',
};

const saveValues = (
  accountingPeriodId: string,
  reportTypeId: string,
  accountingDate?: string
) => ({
  type: ACTIONS.SAVE_VALUES,
  payload: { accountingPeriodId, reportTypeId, accountingDate },
});

const clearValues = () => ({
  type: ACTIONS.CLEAR_VALUES,
});

export const actions = {
  save: (
    accountingPeriodId: string,
    reportTypeId: string,
    accountingDate?: string
  ) => (dispatch: Dispatch<any>): any => {
    return dispatch(
      saveValues(accountingPeriodId, reportTypeId, accountingDate)
    );
  },
  clear: () => (dispatch: Dispatch<any>): any => {
    return dispatch(clearValues());
  },
};

const initialState = {};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SAVE_VALUES:
      const prevState = cloneDeep(state);
      return merge(prevState, action.payload);
    case ACTIONS.CLEAR_VALUES:
      return {};
    default:
      return state;
  }
}: Reducer<Report | {}, any>);
