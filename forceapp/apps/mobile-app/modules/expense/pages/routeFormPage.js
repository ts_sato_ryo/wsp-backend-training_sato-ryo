// @flow
import { type Reducer, type Dispatch } from 'redux';

export const ACTIONS = {
  SAVE_SUCCESS: 'MODULES/PAGES/EXP/ROUTE_FORM_PAGE/SAVE_SUCCESS',
  CLEAR_SUCCESS: 'MODULES/PAGES/EXP/ROUTE_FORM_PAGE/CLEAR_SUCCESS',
};

const saveSuccess = (routeFormParams: any) => ({
  type: ACTIONS.SAVE_SUCCESS,
  payload: routeFormParams,
});

const clearSuccess = () => ({
  type: ACTIONS.CLEAR_SUCCESS,
});

export const actions = {
  save: (routeFormParams: any) => (dispatch: Dispatch<any>) =>
    dispatch(saveSuccess(routeFormParams)),
  clear: () => (dispatch: Dispatch<any>) => dispatch(clearSuccess()),
};

const initialState = {};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SAVE_SUCCESS:
      return action.payload;
    case ACTIONS.CLEAR_SUCCESS:
      return initialState;
    default:
      return state;
  }
}: Reducer<any, any>);
