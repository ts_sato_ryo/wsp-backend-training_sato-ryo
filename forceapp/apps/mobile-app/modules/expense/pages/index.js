// @flow

import { combineReducers } from 'redux';

import routeFormPage from './routeFormPage';
import defaultRouteOptions from './defaultRouteOptions';

const rootReducer = {
  routeFormPage,
  defaultRouteOptions,
};

export default combineReducers<Object, Object>(rootReducer);
