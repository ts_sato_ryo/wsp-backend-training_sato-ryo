// @flow
import type { Reducer } from 'redux';

import {
  type ReceiptList,
  type FilePreview,
  getReceipts,
  getFilePreview,
  deleteReceipt,
} from '../../../../domain/models/exp/receipt-library/list';

import {
  type Base64FileList,
  uploadReceipt,
} from '../../../../domain/models/exp/Receipt';

export const ACTIONS = {
  GET: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/GET',
  LIST: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/LIST',
  DELETE: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/DELETE',
  SAVE: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/SAVE',
};

const listSuccess = (res: ReceiptList) => ({
  type: ACTIONS.LIST,
  payload: res,
});

const getSuccess = (res: FilePreview) => ({
  type: ACTIONS.GET,
  payload: res,
});

const saveSuccess = (res: string) => {
  return {
    type: ACTIONS.SAVE,
    payload: res,
  };
};
const deleteSuccess = (res: string) => {
  return {
    type: ACTIONS.DELETE,
    payload: res,
  };
};

export const actions = {
  list: () => (dispatch: Dispatch<any>): void | any => {
    return getReceipts()
      .then((res: ReceiptList) => {
        dispatch(listSuccess(res));
      })
      .catch((err) => {
        throw err;
      });
  },
  get: (receiptFileId: string) => (dispatch: Dispatch<any>): void | any => {
    return getFilePreview(receiptFileId)
      .then((res: FilePreview) => dispatch(getSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  delete: (receiptId: string) => (dispatch: Dispatch<any>): void | any => {
    return deleteReceipt(receiptId)
      .then(() => dispatch(deleteSuccess(receiptId)))
      .catch((err) => {
        throw err;
      });
  },
  uploadReceipt: (base64FileList: Base64FileList) => (
    dispatch: Dispatch<any>
  ): void | any => {
    return uploadReceipt(base64FileList)
      .then((res) => dispatch(saveSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
};

type State = {
  receipts: ReceiptList,
  totalSize: number,
  receiptPreview: Array<FilePreview>,
};
const initialState = {
  receipts: [],
  totalSize: 0,
  receiptPreview: [],
};

/* Reducer */
export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST:
      return { ...state, receipts: action.payload };
    case ACTIONS.GET:
      return {
        ...state,
        receiptPreview: action.payload,
      };
    case ACTIONS.DELETE:
      const receipts = state.receipts;
      receipts.splice(
        receipts.findIndex((item) => item.receiptId === action.payload),
        1
      );
      return { ...state, receipts };
    default:
      return state;
  }
}: Reducer<State, any>);
