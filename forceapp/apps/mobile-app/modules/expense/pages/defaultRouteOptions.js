// @flow
import { type Reducer, type Dispatch } from 'redux';

import searchRouteOption from '../../../../domain/models/exp/jorudan/JorudanOption';

export const ACTIONS = {
  SEARCH_SUCCESS: 'MODULES/EXP/JORUDAN/ROUTEOPTION/GET',
};

const searchSuccess = (body: any) => {
  return {
    type: ACTIONS.SEARCH_SUCCESS,
    payload: body,
  };
};

export const actions = {
  search: (companyId: string) => (dispatch: Dispatch<any>): void | any => {
    return searchRouteOption(companyId)
      .then((res: any) => {
        dispatch(searchSuccess(res));
        return true;
      })
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = {
  jorudanAreaPreference: null,
  jorudanChargedExpressDistance: null,
  jorudanCommuterPass: null,
  jorudanFareType: '0',
  jorudanHighwayBus: '0',
  jorudanRouteSort: '0',
  jorudanSeatPreference: '0',
  jorudanUseChargedExpress: '0',
  jorudanUseExReservation: '0',
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_SUCCESS:
      // return action.payload;
      return {
        ...action.payload,
        jorudanUseExReservation: state.jorudanUseExReservation,
      };
    default:
      return state;
  }
}: Reducer<any, any>);
