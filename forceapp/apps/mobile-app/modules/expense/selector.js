// @flow

import { createSelector } from 'reselect';

import { type Record } from '../../../domain/models/exp/Record';
import { type State } from './index';

/**
 * Selectors depending on the expense state.
 *
 * Note that you should NOT put selectors depending on a module.
 * If you want to write selectors depending on a module, then you should
 * put the selectors into a module.
 */

/* Records  */

const selectedRecordIdMap = (state: State): { [string]: boolean } =>
  state.ui.record.list.select.flagsById;

const selectRecordList = (state: State): Record[] => state.entities.recordList;

// eslint-disable-next-line import/prefer-default-export
export const selectedRecords: (State) => Record[] = createSelector(
  selectedRecordIdMap,
  selectRecordList,
  (recordIdMap: { [string]: boolean }, recordList: Record[] = []): Record[] => {
    return (
      recordList &&
      recordList.filter(
        (record) => record.recordId && recordIdMap[record.recordId]
      )
    );
  }
);
