// @flow

import { combineReducers } from 'redux';

import expenseTypeList from './expenseTypeList';
import reportList from './reportList';
import recordList from './recordList';
import recordItem from './recordItem';
import taxType from './taxType';
import routeResults from '../../../../domain/modules/exp/jorudan/route';
import report from './report';
import accountingPeriod from './accountingPeriod';
import costCenterList from './costCenterList';
import costCenterAllList from './costCenterAllList';
import expReportType from './expReportType';
import job from './jobList';
import jobAll from './jobAllList';
import customEIOption from './customEIOption';
import customHint from './customHint';

const rootReducer = {
  reportList,
  taxType,
  expenseTypeList,
  recordList,
  recordItem,
  routeResults,
  report,
  accountingPeriod,
  costCenterList,
  costCenterAllList,
  expReportType,
  job,
  jobAll,
  customEIOption,
  customHint,
};

export default combineReducers<Object, Object>(rootReducer);
