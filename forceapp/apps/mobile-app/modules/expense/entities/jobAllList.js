// @flow
import { type Reducer, type Dispatch } from 'redux';

import { type JobList, searchJob } from '../../../../domain/models/exp/Job';

export const ACTIONS = {
  SEARCH_JOB_SUCCESS: 'MODULES/EXPENSE/ENTITIES/JOB/SEARCH_ALL_SUCESS',
};

const searchSuccess = (body: JobList) => ({
  type: ACTIONS.SEARCH_JOB_SUCCESS,
  payload: body,
});

export const actions = {
  search: (empId?: string, targetDate: string) => (
    dispatch: Dispatch<any>
  ): Promise<JobList> => {
    return searchJob(empId, null, targetDate)
      .then((res: JobList) => dispatch(searchSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_JOB_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<JobList, any>);
