// @flow
import { type Reducer, type Dispatch } from 'redux';
import { includes } from 'lodash';

import {
  type ExpenseTypeList,
  getExpenseTypeList,
  searchExpenseType,
  searchExpenseTypeWithRecord,
} from '../../../../domain/models/exp/ExpenseType';
import { RECORD_TYPE } from '../../../../domain/models/exp/Record';

const MOBILE_SUPPORTED_RECORD_TYPES = [
  RECORD_TYPE.General,
  RECORD_TYPE.FixedAllowanceSingle,
  RECORD_TYPE.FixedAllowanceMulti,
];

export const ACTIONS = {
  GET_SUCCESS: 'MODULES/EXPENSE/ENTITIES/EXPENSES_TYPE/GET_SUCESS',
  SEARCH_SUCCESS: 'MODULES/EXPENSE/ENTITIES/EXPENSES_TYPE/SEARCH_SUCESS',
  SEARCH_WITH_TYPE_SUCCESS:
    'MODULES/EXPENSE/ENTITIES/EXPENSES_TYPE/SEARCH_WITH_TYPE_SUCCESS',
  CLEAR_SUCCESS: 'MODULES/EXPENSE/ENTITIES/EXPENSES_TYPE/CLEAR_SUCESS',
};

const getSuccess = (body: ExpenseTypeList) => ({
  type: ACTIONS.GET_SUCCESS,
  payload: body,
});

const searchSuccess = (body: ExpenseTypeList) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: body,
});

const searchWithTypeSuccess = (body: ExpenseTypeList) => ({
  type: ACTIONS.SEARCH_WITH_TYPE_SUCCESS,
  payload: body,
});

const clearExpenseTypeList = () => ({
  type: ACTIONS.CLEAR_SUCCESS,
});

// For now, only base currency & Generan record type is available for mobile app
// Once those functions are implemented, remove this method.
const filterExpenseTypeForMobile = (res: ExpenseTypeList) => {
  return res.filter(
    (e) =>
      e.hasChildren ||
      (!e.useForeignCurrency &&
        includes(MOBILE_SUPPORTED_RECORD_TYPES, e.recordType))
  );
};

export const actions = {
  get: (
    empId: ?string,
    parentGroupId: ?string,
    targetDate: ?string,
    recordType: ?string
  ) => (dispatch: Dispatch<any>): Promise<ExpenseTypeList> => {
    return getExpenseTypeList(
      empId,
      parentGroupId,
      targetDate,
      recordType,
      'REPORT',
      undefined,
      true
    )
      .then((res: ExpenseTypeList) =>
        dispatch(getSuccess(filterExpenseTypeForMobile(res)))
      )
      .catch((err) => {
        dispatch(clearExpenseTypeList());
        throw err;
      });
  },
  search: (companyId?: string, name: ?string, targetDate: ?string) => (
    dispatch: Dispatch<any>
  ): Promise<ExpenseTypeList> => {
    return searchExpenseType(
      companyId,
      name,
      targetDate,
      'REPORT',
      undefined,
      undefined,
      undefined,
      true
    )
      .then((res: ExpenseTypeList) =>
        dispatch(searchSuccess(filterExpenseTypeForMobile(res)))
      )
      .catch((err) => {
        dispatch(clearExpenseTypeList());
        throw err;
      });
  },
  searchWithType: (
    companyId: string,
    targetDate: ?string,
    recordType: ?string
  ) => (dispatch: Dispatch<any>): Promise<ExpenseTypeList> => {
    return searchExpenseTypeWithRecord(
      companyId,
      targetDate,
      recordType,
      'REPORT',
      true
    )
      .then((res: ExpenseTypeList) => dispatch(searchWithTypeSuccess(res)))
      .catch((err) => {
        dispatch(clearExpenseTypeList());
        throw err;
      });
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.GET_SUCCESS:
    case ACTIONS.SEARCH_SUCCESS:
      return action.payload;
    case ACTIONS.SEARCH_WITH_TYPE_SUCCESS:
      return action.payload;
    case ACTIONS.CLEAR_SUCCESS:
      return initialState;
    default:
      return state;
  }
}: Reducer<ExpenseTypeList, any>);
