// @flow
import { type Reducer, type Dispatch } from 'redux';
import { concat } from 'lodash';
import msg from '../../../../commons/languages';

import {
  type ExpTaxTypeList,
  searchTaxTypeList,
} from '../../../../domain/models/exp/TaxType';

export const ACTIONS = {
  SEARCH_TAX_TYPE_SUCCESS:
    'MODULES/ENTITIES/EXP/TAX_TYPE/SEARCH_TAX_TYPE_SUCCESS',
};

const searchTaxTypeSuccess = (body: ExpTaxTypeList) => ({
  type: ACTIONS.SEARCH_TAX_TYPE_SUCCESS,
  payload: body,
});

const addDefaultValue = (expTaxTypeList: ExpTaxTypeList) => {
  if (expTaxTypeList.length > 0) {
    return concat(
      [
        {
          historyId: 'default',
          baseId: 'default',
          name: msg().Exp_Sel_Auto_Select_From_Exp_Type,
          rate: expTaxTypeList[0].rate,
        },
      ],
      expTaxTypeList
    );
  }
  return [];
};

export const actions = {
  list: (expTypeId: string, targetDate: string) => (
    dispatch: Dispatch<any>
  ): Promise<{ taxTypes: ExpTaxTypeList }> => {
    return searchTaxTypeList(expTypeId, targetDate).then(
      (res: { taxTypes: ExpTaxTypeList }) =>
        dispatch(searchTaxTypeSuccess(addDefaultValue(res.taxTypes)))
    );
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_TAX_TYPE_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<ExpTaxTypeList, any>);
