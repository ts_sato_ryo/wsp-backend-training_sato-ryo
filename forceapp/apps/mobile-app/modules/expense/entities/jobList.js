// @flow
import { type Reducer, type Dispatch } from 'redux';

import {
  type JobList,
  getJobList,
  searchJob,
} from '../../../../domain/models/exp/Job';

export const ACTIONS = {
  GET_SUCCESS: 'MODULES/EXPENSE/ENTITIES/JOB/GET_SUCESS',
  SEARCH_SUCCESS: 'MODULES/EXPENSE/ENTITIES/JOB/SEARCH_SUCESS',
  SEARCH_WITH_TYPE_SUCCESS:
    'MODULES/EXPENSE/ENTITIES/JOB/SEARCH_WITH_TYPE_SUCCESS',
};

const getSuccess = (body: JobList) => ({
  type: ACTIONS.GET_SUCCESS,
  payload: body,
});

const searchSuccess = (body: JobList) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: body,
});

export const actions = {
  get: (empId: string, parentId?: ?string, targetDate: string) => (
    dispatch: Dispatch<any>
  ): Promise<JobList> => {
    return getJobList(empId, parentId, targetDate)
      .then((res) => dispatch(getSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  search: (empId?: string, name: ?string, targetDate: string) => (
    dispatch: Dispatch<any>
  ): Promise<JobList> => {
    return searchJob(empId, name, targetDate)
      .then((res: JobList) => dispatch(searchSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.GET_SUCCESS:
    case ACTIONS.SEARCH_SUCCESS:
      return action.payload;
    case ACTIONS.SEARCH_WITH_TYPE_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<JobList, any>);
