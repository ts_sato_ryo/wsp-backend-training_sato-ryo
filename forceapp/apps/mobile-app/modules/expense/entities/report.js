// @flow

import { type Dispatch } from 'redux';
import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';

import {
  type Report,
  getReport,
  saveReportMobile,
  deleteReport,
  initialStateReport,
  calcTotalAmount,
} from '../../../../domain/models/exp/Report';
import { type Record } from '../../../../domain/models/exp/Record';
import { submitExpRequestReport } from '../../../../domain/models/exp/request/Report';
import {
  type FilePreview,
  getFilePreview,
} from '../../../../domain/models/exp/receipt-library/list';

import FileUtil from '../../../../commons/utils/FileUtil';

// State

type State = Report;

const initialState: State = initialStateReport;

// Selector

const range: string[] = Array.from(Array(10), (_, index) =>
  `${index + 1}`.padStart(2, '0')
);

export const selectors = {
  // $FlowFixMe
  extendedItemTexts: createSelector(
    (state) => state,
    (state: State) => {
      return range.reduce(
        (acc, index) =>
          isNil(state[`extendedItemText${index}Id`])
            ? acc
            : [
                ...acc,
                {
                  name: state[`extendedItemText${index}Info`].name,
                  value: state[`extendedItemText${index}Value`],
                },
              ],
        []
      );
    }
  ),
  // $FlowFixMe
  extendedItemPicklists: createSelector(
    (state) => state,
    (state: State) => {
      return range.reduce(
        (acc, index) =>
          isNil(state[`extendedItemPicklist${index}Id`])
            ? acc
            : [
                ...acc,
                {
                  name: state[`extendedItemPicklist${index}Info`].name,
                  value: state[`extendedItemPicklist${index}Value`],
                  picklist: state[`extendedItemPicklist${index}Info`].picklist,
                },
              ],
        []
      );
    }
  ),
  // $FlowFixMe
  extendedItemLookup: createSelector(
    (state) => state,
    (state: State) => {
      return range.reduce(
        (acc, index) =>
          isNil(state[`extendedItemLookup${index}Id`])
            ? acc
            : [
                ...acc,
                {
                  name: state[`extendedItemLookup${index}Info`].name,
                  value: state[`extendedItemLookup${index}Value`],
                  selectedOptionName:
                    state[`extendedItemLookup${index}SelectedOptionName`],
                },
              ],
        []
      );
    }
  ),
  // $FlowFixMe
  extendedItemDate: createSelector(
    (state) => state,
    (state: State) => {
      return range.reduce(
        (acc, index) =>
          isNil(state[`extendedItemDate${index}Id`])
            ? acc
            : [
                ...acc,
                {
                  name: state[`extendedItemDate${index}Info`].name,
                  value: state[`extendedItemDate${index}Value`],
                },
              ],
        []
      );
    }
  ),
  // $FlowFixMe
  totalAmount: createSelector(
    (state) => state,
    (state: State) => {
      return isNil(state.records) ? 0 : calcTotalAmount(state);
    }
  ),
};

// Actions
type FetchSuccess = {|
  type: 'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/FETCH_SUCCES',
  payload: Report,
|};

type SetImageSuccess = {|
  type: 'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_IMAGE_SUCCESS',
  payload: { imageData: string, reportId: string, fileName: string },
|};

type SetRecords = {|
  type: 'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_RECORDS',
  payload: Record[],
|};

type UpdateValue = {|
  type: 'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/UPDATE_VALUE',
  payload: {
    key: $Keys<Report>,
    value: any,
  },
|};

type SetReport = {|
  type: 'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_REPORT',
  payload: Report,
|};

type SetEI = {|
  type: 'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_EI',
  payload: any,
|};

type Clear = {|
  type: 'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/CLEAR',
  payload: Report,
|};

type Action =
  | FetchSuccess
  | SetImageSuccess
  | SetRecords
  | UpdateValue
  | SetReport
  | SetEI
  | Clear;

const FETCH_SUCCES: $PropertyType<FetchSuccess, 'type'> =
  'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/FETCH_SUCCES';
const SET_IMAGE_SUCCESS: $PropertyType<SetImageSuccess, 'type'> =
  'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_IMAGE_SUCCESS';
const SET_RECORDS: $PropertyType<SetRecords, 'type'> =
  'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_RECORDS';
const UPDATE_VALUE: $PropertyType<UpdateValue, 'type'> =
  'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/UPDATE_VALUE';
const SET_REPORT: $PropertyType<SetReport, 'type'> =
  'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_REPORT';
const SET_EI: $PropertyType<any, 'type'> =
  'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/SET_EI';
const CLEAR: $PropertyType<Clear, 'type'> =
  'MOBILE-APP/MODULES/EXPENSE/ENTITIES/REPORT/CLEAR';

export const fetchSuccess = (report: Report): FetchSuccess => ({
  type: FETCH_SUCCES,
  payload: report,
});

const setImageSuccess = (
  imageData: string,
  reportId: string,
  fileName: string
) => {
  return {
    type: SET_IMAGE_SUCCESS,
    payload: { imageData, reportId, fileName },
  };
};

export const actions = {
  setRecords: (records: Record[]): SetRecords => ({
    type: SET_RECORDS,
    payload: records,
  }),

  updateValue: (key: $Keys<Report>, value: any): UpdateValue => ({
    type: UPDATE_VALUE,
    payload: { key, value },
  }),

  setReport: (report: Report): SetReport => ({
    type: SET_REPORT,
    payload: report,
  }),

  setEI: (eis: any): any => ({
    type: SET_EI,
    payload: eis,
  }),

  setImage: (receiptFileId: string, id: string) => (
    dispatch: Dispatch<any>
  ) => {
    getFilePreview(receiptFileId).then((res: FilePreview) => {
      const dataType = FileUtil.getMIMEType(res.fileType);
      const fileUrl = `data:${dataType};base64,${res.fileBody}`;
      const fileName = FileUtil.getOriginalFileNameWithoutPrefix(res.title);
      dispatch(setImageSuccess(fileUrl, id, fileName));
    });
  },

  fetch: (reportId: string) => (dispatch: Dispatch<any>): Promise<void> => {
    return getReport(reportId, 'REPORT').then((result) =>
      dispatch(fetchSuccess(result))
    );
  },

  delete: (reportId: string) => (_dispatch: Dispatch<any>): Promise<void> => {
    return deleteReport(reportId);
  },

  submit: (reportId: string, comment: string) => (
    _dispatch: Dispatch<any>
  ): Promise<void> => {
    return submitExpRequestReport(reportId, comment);
  },

  save: (report: Report) => (_dispatch: Dispatch<any>): Promise<void> => {
    return saveReportMobile(report);
  },

  clear: () => ({
    type: CLEAR,
    payload: initialState,
  }),
};

// Reducer

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case FETCH_SUCCES: {
      return {
        ...action.payload,
      };
    }

    case SET_IMAGE_SUCCESS: {
      let attachedFileData = null;
      let fileName = null;
      if (action.payload.reportId === state.reportId) {
        attachedFileData = action.payload.imageData;
        fileName = action.payload.fileName;
      }
      return { ...state, attachedFileData, fileName };
    }
    case SET_RECORDS: {
      const records = (action: SetRecords).payload;
      const totalAmount = calcTotalAmount({
        ...state,
        records,
      });
      return {
        ...state,
        totalAmount,
        records,
      };
    }

    case UPDATE_VALUE: {
      const payload = (action: UpdateValue).payload;
      return {
        ...state,
        [payload.key]: payload.value,
      };
    }

    case SET_REPORT: {
      return {
        ...action.payload,
      };
    }

    case SET_EI: {
      const payload = action.payload;
      return {
        ...state,
        extendedItemText01Id: payload.extendedItemText01Id,
        extendedItemText01Info: payload.extendedItemText01Info,
        extendedItemText01Value:
          payload.extendedItemText01Info &&
          payload.extendedItemText01Info.defaultValueText,
        extendedItemText02Id: payload.extendedItemText02Id,
        extendedItemText02Info: payload.extendedItemText02Info,
        extendedItemText02Value:
          payload.extendedItemText02Info &&
          payload.extendedItemText02Info.defaultValueText,
        extendedItemText03Id: payload.extendedItemText03Id,
        extendedItemText03Info: payload.extendedItemText03Info,
        extendedItemText03Value:
          payload.extendedItemText03Info &&
          payload.extendedItemText03Info.defaultValueText,
        extendedItemText04Id: payload.extendedItemText04Id,
        extendedItemText04Info: payload.extendedItemText04Info,
        extendedItemText04Value:
          payload.extendedItemText04Info &&
          payload.extendedItemText04Info.defaultValueText,
        extendedItemText05Id: payload.extendedItemText05Id,
        extendedItemText05Info: payload.extendedItemText05Info,
        extendedItemText05Value:
          payload.extendedItemText05Info &&
          payload.extendedItemText05Info.defaultValueText,
        extendedItemText06Id: payload.extendedItemText06Id,
        extendedItemText06Info: payload.extendedItemText06Info,
        extendedItemText06Value:
          payload.extendedItemText06Info &&
          payload.extendedItemText06Info.defaultValueText,
        extendedItemText07Id: payload.extendedItemText07Id,
        extendedItemText07Info: payload.extendedItemText07Info,
        extendedItemText07Value:
          payload.extendedItemText07Info &&
          payload.extendedItemText07Info.defaultValueText,
        extendedItemText08Id: payload.extendedItemText08Id,
        extendedItemText08Info: payload.extendedItemText08Info,
        extendedItemText08Value:
          payload.extendedItemText08Info &&
          payload.extendedItemText08Info.defaultValueText,
        extendedItemText09Id: payload.extendedItemText09Id,
        extendedItemText09Info: payload.extendedItemText09Info,
        extendedItemText09Value:
          payload.extendedItemText09Info &&
          payload.extendedItemText09Info.defaultValueText,
        extendedItemText10Id: payload.extendedItemText10Id,
        extendedItemText10Info: payload.extendedItemText10Info,
        extendedItemText10Value:
          payload.extendedItemText10Info &&
          payload.extendedItemText10Info.defaultValueText,
        extendedItemPicklist01Id: payload.extendedItemPicklist01Id,
        extendedItemPicklist01Info: payload.extendedItemPicklist01Info,
        extendedItemPicklist02Id: payload.extendedItemPicklist02Id,
        extendedItemPicklist02Info: payload.extendedItemPicklist02Info,
        extendedItemPicklist03Id: payload.extendedItemPicklist03Id,
        extendedItemPicklist03Info: payload.extendedItemPicklist03Info,
        extendedItemPicklist04Id: payload.extendedItemPicklist04Id,
        extendedItemPicklist04Info: payload.extendedItemPicklist04Info,
        extendedItemPicklist05Id: payload.extendedItemPicklist05Id,
        extendedItemPicklist05Info: payload.extendedItemPicklist05Info,
        extendedItemPicklist06Id: payload.extendedItemPicklist06Id,
        extendedItemPicklist06Info: payload.extendedItemPicklist06Info,
        extendedItemPicklist07Id: payload.extendedItemPicklist07Id,
        extendedItemPicklist07Info: payload.extendedItemPicklist07Info,
        extendedItemPicklist08Id: payload.extendedItemPicklist08Id,
        extendedItemPicklist08Info: payload.extendedItemPicklist08Info,
        extendedItemPicklist09Id: payload.extendedItemPicklist09Id,
        extendedItemPicklist09Info: payload.extendedItemPicklist09Info,
        extendedItemPicklist10Id: payload.extendedItemPicklist10Id,
        extendedItemPicklist10Info: payload.extendedItemPicklist10Info,
        extendedItemLookup01Id: payload.extendedItemLookup01Id,
        extendedItemLookup01Info: payload.extendedItemLookup01Info,
        extendedItemLookup02Id: payload.extendedItemLookup02Id,
        extendedItemLookup02Info: payload.extendedItemLookup02Info,
        extendedItemLookup03Id: payload.extendedItemLookup03Id,
        extendedItemLookup03Info: payload.extendedItemLookup03Info,
        extendedItemLookup04Id: payload.extendedItemLookup04Id,
        extendedItemLookup04Info: payload.extendedItemLookup04Info,
        extendedItemLookup05Id: payload.extendedItemLookup05Id,
        extendedItemLookup05Info: payload.extendedItemLookup05Info,
        extendedItemLookup06Id: payload.extendedItemLookup06Id,
        extendedItemLookup06Info: payload.extendedItemLookup06Info,
        extendedItemLookup07Id: payload.extendedItemLookup07Id,
        extendedItemLookup07Info: payload.extendedItemLookup07Info,
        extendedItemLookup08Id: payload.extendedItemLookup08Id,
        extendedItemLookup08Info: payload.extendedItemLookup08Info,
        extendedItemLookup09Id: payload.extendedItemLookup09Id,
        extendedItemLookup09Info: payload.extendedItemLookup09Info,
        extendedItemLookup10Id: payload.extendedItemLookup10Id,
        extendedItemLookup10Info: payload.extendedItemLookup10Info,
        extendedItemDate01Id: payload.extendedItemDate01Id,
        extendedItemDate01Info: payload.extendedItemDate01Info,
        extendedItemDate02Id: payload.extendedItemDate02Id,
        extendedItemDate02Info: payload.extendedItemDate02Info,
        extendedItemDate03Id: payload.extendedItemDate03Id,
        extendedItemDate03Info: payload.extendedItemDate03Info,
        extendedItemDate04Id: payload.extendedItemDate04Id,
        extendedItemDate04Info: payload.extendedItemDate04Info,
        extendedItemDate05Id: payload.extendedItemDate05Id,
        extendedItemDate05Info: payload.extendedItemDate05Info,
        extendedItemDate06Id: payload.extendedItemDate06Id,
        extendedItemDate06Info: payload.extendedItemDate06Info,
        extendedItemDate07Id: payload.extendedItemDate07Id,
        extendedItemDate07Info: payload.extendedItemDate07Info,
        extendedItemDate08Id: payload.extendedItemDate08Id,
        extendedItemDate08Info: payload.extendedItemDate08Info,
        extendedItemDate09Id: payload.extendedItemDate09Id,
        extendedItemDate09Info: payload.extendedItemDate09Info,
        extendedItemDate10Id: payload.extendedItemDate10Id,
        extendedItemDate10Info: payload.extendedItemDate10Info,
      };
    }
    case CLEAR: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
