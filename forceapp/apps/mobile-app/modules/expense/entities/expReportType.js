// @flow
import { type Reducer, type Dispatch } from 'redux';

import {
  type ExpenseReportTypeList,
  getReportTypeList,
} from '../../../../domain/models/exp/expense-report-type/list';

export const ACTIONS = {
  SEARCH_REPORT_TYPE_SUCCESS:
    'MODULES/ENTITIES/EXP/REPORT_TYPE/SEARCH_REPORT_TYPE_SUCCESS',
};

const searchExpReportTypeSuccess = (body: any) => ({
  type: ACTIONS.SEARCH_REPORT_TYPE_SUCCESS,
  payload: body,
});

// For now, using vendor master in mobile app is not supported.
const filterOutVendorReportType = (res: ExpenseReportTypeList) => {
  return res.filter((x) => x.vendorUsedIn === null);
};

export const actions = {
  list: (
    companyId: string,
    active: ?boolean,
    employeeId?: string,
    withExpTypeName: ?boolean,
    startDate: ?string,
    endDate: ?string
  ) => (dispatch: Dispatch<any>): Promise<any> => {
    return getReportTypeList(
      companyId,
      'REPORT',
      active,
      employeeId,
      withExpTypeName,
      startDate,
      endDate
    ).then((res: any) =>
      dispatch(
        searchExpReportTypeSuccess(filterOutVendorReportType(res.records))
      )
    );
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_REPORT_TYPE_SUCCESS:
      return action.payload.filter((rt) => rt.active);
    default:
      return state;
  }
}: Reducer<ExpenseReportTypeList, any>);
