// @flow
import { type Reducer, type Dispatch } from 'redux';
import {
  type ReportList,
  getReportListMobile,
  deleteReport,
} from '../../../../domain/models/exp/Report';

import { statuses } from '../../../../commons/constants/requestStatus';

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/EXPENSE/ENTITIES/REPORT/LIST_SUCCESS',
  DELETE_SUCCESS: 'MODULES/EXPENSE/ENTITIES/REPORT/DELETE_SUCCESS',
};

const listSuccess = (reportList: ReportList) => {
  return {
    type: ACTIONS.LIST_SUCCESS,
    payload: reportList,
  };
};

const deleteSuccess = (reportId: string) => {
  return {
    type: ACTIONS.DELETE_SUCCESS,
    payload: reportId,
  };
};

// For now, The below function are not supported on mobile app.
// So don't display them on the report list to let user not to process them on mobile app.
// - Create report from request (-> Approved requests are hidden)
// - Edit vendor fields (-> Reports requires vendor are hidden)
const filterReportListForMobile = (res: ReportList) => {
  return res
    .filter(
      (rep) =>
        rep.status !== statuses.APPROVED && rep.status !== 'ApprovedPreRequest'
    )
    .filter((rep) => rep.vendorId === null && !rep.vendorRequiredFor);
};

export const actions = {
  list: (empId?: ?string) => (dispatch: Dispatch<any>): Promise<ReportList> => {
    return getReportListMobile(empId).then((res: ReportList) =>
      dispatch(listSuccess(filterReportListForMobile(res)))
    );
  },

  delete: (reportId: string) => (dispatch: Dispatch<any>): Promise<any> => {
    return deleteReport(reportId).then(() => dispatch(deleteSuccess(reportId)));
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return action.payload;
    case ACTIONS.DELETE_SUCCESS:
      return state.filter((report) => report.reportId !== action.payload);
    default:
      return state;
  }
}: Reducer<ReportList, any>);
