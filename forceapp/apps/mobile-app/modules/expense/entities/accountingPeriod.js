// @flow
import { type Reducer, type Dispatch } from 'redux';
import _ from 'lodash';

import DateUtil from '../../../../commons/utils/DateUtil';

import {
  type AccountingPeriodList,
  searchAccountingPeriod,
} from '../../../../domain/models/exp/AccountingPeriod';

export const ACTIONS = {
  SEARCH_ACCOUNT_PERIOD_SUCCESS:
    'MODULES/ENTITIES/EXP/ACCOUNT_PERIOD/SEARCH_ACCOUNT_PERIOD_SUCCESS',
};

const searchAccountingPeriodSuccess = (body: any) => ({
  type: ACTIONS.SEARCH_ACCOUNT_PERIOD_SUCCESS,
  payload: body,
});

const makeAccountingPeriodList = (res: AccountingPeriodList) => {
  const t = _.map(res, (o: any) => {
    return {
      value: o.id,
      label: `${DateUtil.format(
        o.validDateFrom,
        'YYYY/MM/DD'
      )} - ${DateUtil.format(o.validDateTo, 'YYYY/MM/DD')}`,
      recordDate: o.recordingDate,
      active: o.active,
      id: o.id,
    };
  });
  return t;
};

export const actions = {
  list: (companyId: string) => (dispatch: Dispatch<any>): Promise<any> => {
    return searchAccountingPeriod(companyId).then((res: any) =>
      dispatch(
        searchAccountingPeriodSuccess(makeAccountingPeriodList(res.records))
      )
    );
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_ACCOUNT_PERIOD_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<AccountingPeriodList, any>);
