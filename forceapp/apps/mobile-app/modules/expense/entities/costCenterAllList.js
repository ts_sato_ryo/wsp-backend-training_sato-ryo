// @flow
import { type Reducer, type Dispatch } from 'redux';

import {
  type CostCenterList,
  searchCostCenter,
} from '../../../../domain/models/exp/CostCenter';

export const ACTIONS = {
  SEARCH_SUCCESS: 'MODULES/EXPENSE/ENTITIES/COST_CENTER/SEARCH_ALL_SUCESS',
};

const searchSuccess = (body: CostCenterList) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: body,
});

export const actions = {
  search: (companyId?: string, targetDate: string) => (
    dispatch: Dispatch<any>
  ): Promise<CostCenterList> => {
    return searchCostCenter(companyId, null, targetDate, null)
      .then((res: CostCenterList) => dispatch(searchSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<CostCenterList, any>);
