// @flow
import { type Reducer, type Dispatch } from 'redux';

import {
  saveRecord,
  deleteRecord,
  getRecordList,
  type Record,
} from '../../../../domain/models/exp/Record';

import {
  uploadReceipt,
  type Base64FileList,
} from '../../../../domain/models/exp/Receipt';

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/EXPENSE/ENTITIES/RECORD/LIST_SUCCESS',
  SAVE_SUCCESS: 'MODULES/EXPENSE/ENTITIES/RECORD/SAVE_SUCCESS',
  DELETE_SUCCESS: 'MODULES/EXPENSE/ENTITIES/RECORD/DELETE_SUCCESS',
  UPLOAD_RECEIPT_SUCCESS:
    'MODULES/EXPENSE/ENTITIES/RECORD/UPLOAD_RECEIPT_SUCCESS',
};

const listSuccess = (recordList: Record[]) => {
  return {
    type: ACTIONS.LIST_SUCCESS,
    payload: recordList,
  };
};

const saveSuccess = (respones: any) => {
  return {
    type: ACTIONS.SAVE_SUCCESS,
    payload: respones,
  };
};

const deleteSuccess = (response: any) => {
  return {
    type: ACTIONS.DELETE_SUCCESS,
    payload: response,
  };
};

const uploadReceiptSuccess = () => {
  return {
    type: ACTIONS.UPLOAD_RECEIPT_SUCCESS,
  };
};

export const actions = {
  list: (
    empId: string,
    expTypeList: ?Array<string>,
    startDate: ?string,
    endDate: ?string
  ) => (dispatch: Dispatch<any>): Promise<Record[]> => {
    return getRecordList(empId, expTypeList, startDate, endDate).then(
      ({ records }: { records: Record[] }) => dispatch(listSuccess(records))
    );
  },
  save: (values: Record) => (dispatch: Dispatch<any>): Promise<any> => {
    return saveRecord(values).then(() => dispatch(saveSuccess(values)));
  },
  delete: (recordId: string) => (dispatch: Dispatch<any>): Promise<any> => {
    const recordIds = [recordId];

    return deleteRecord(recordIds).then((res: any) =>
      dispatch(deleteSuccess(res))
    );
  },
  uploadReceipt: (files: Base64FileList) => (
    dispatch: Dispatch<any>
  ): Promise<any> => {
    return uploadReceipt(files).then(() => dispatch(uploadReceiptSuccess()));
  },
};

type State = Record[];

const initialState: State = [];

export default ((state: State = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return action.payload || [];
    case ACTIONS.SAVE_SUCCESS:
      return [...state, action.payload] || [];
    case ACTIONS.DELETE_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<Record[], any>);
