/* @flow */

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';

import entities from './entities';
import ui from './ui';
import pages from './pages';

const rootReducer = {
  entities,
  ui,
  pages,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
