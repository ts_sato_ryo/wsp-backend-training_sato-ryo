/* @flow */

import { combineReducers } from 'redux';

import { type $State } from '../../../../commons/utils/TypeUtil';

import dailyTask from './dailyTask';
import dailyTaskJob from './dailyTaskJob';

const rootReducer = {
  dailyTask,
  dailyTaskJob,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
