// @flow

import { createSelector } from 'reselect';

import { type State } from './index';

const selectJob = (state: State) => state.entity.job;

const selectSelectedJobId = (state: State) => state.ui.dailyTaskJob.jobId;

// $FlowFixMe
export const jobOptionList = createSelector(
  selectJob,
  (jobs: *) => [
    { label: '', value: '' },
    ...jobs.map((job) => ({
      label: job.name,
      value: job.id,
    })),
  ]
);

// $FlowFixMe
export const workCategoryOptionList = createSelector(
  selectJob,
  selectSelectedJobId,
  (jobs: *, jobId: null | string) => {
    const defaultOption = {
      label: '',
      value: '',
    };
    const found = jobs.find((job) => job.id === jobId);
    return found
      ? [
          defaultOption,
          ...found.workCategories.map((wc) => ({
            label: `${wc.code} ${wc.name}`,
            value: wc.id,
          })),
        ]
      : [];
  }
);
