// @flow

import { type Dispatch } from 'redux';
import { createSelector } from 'reselect';

import Repository from '../../../../repositories/TimeTaskDailyRepository';
import defaultValue, {
  type DailyTask,
  getDefaultJob,
} from '../../../../domain/models/time-tracking/DailyTask';
import defaultTaskValue, {
  type Task,
  isDefaultJob,
  calculateTaskTimeFromRatio,
  toggleDirectInput,
  updateTaskList,
} from '../../../../domain/models/time-tracking/Task';
import colors from '../../../styles/variables/_colors.scss';

// State

type ViewState = {|
  hasDefaultJob: boolean,
|};

export type State = {| ...DailyTask, ...ViewState |};

const initialState: State = {
  ...defaultValue,
  hasDefaultJob: false,
};

// Selectors

// $FlowFixMe
export const taskTimes = createSelector(
  (state: State) => state,
  (state: State) => {
    return [
      state.taskList.reduce(
        (acc, task) => ({
          color: colors.blue600,
          value:
            acc.value +
            (state.realWorkTime !== null || task.isDirectInput
              ? task.taskTime
              : 0),
        }),
        { color: '', value: 0 }
      ),
    ];
  }
);

// Actions

type ClearState = {
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/CLEAR_STATE',
};

const clearState = (): ClearState => ({
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/CLEAR_STATE',
});

type FetchSuccess = {|
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/FETCH_SUCCESS',
  payload: DailyTask,
|};

const fetchSuccess = (payload: DailyTask): FetchSuccess => ({
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/FETCH_SUCCESS',
  payload,
});

type PushDefaultJob = {|
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/PUSH_DEFAULT_JOB',
  payload: Task,
|};

const pushDefaultJob = (defaultJob: Task): PushDefaultJob => ({
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/PUSH_DEFAULT_JOB',
  payload: defaultJob,
});

type MarkDefaultJob = {|
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/MARK_DEFAULT_JOB',
  payload: Task,
|};

const markDefaultJob = (defaultJob: Task): MarkDefaultJob => ({
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/MARK_DEFAULT_JOB',
  payload: defaultJob,
});

type UpdateTask = {|
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/UPDATE_TASK',
  payload: {|
    id: string,
    task: $Shape<Task>,
  |},
|};

export const updateTask = (id: string, task: $Shape<Task>): UpdateTask => ({
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/UPDATE_TASK',
  payload: { id, task },
});

type ToggleInputMode = {|
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/TOGGLE_INPUT_MODE',
  payload: { id: string },
|};

export const toggleInputMode = (id: string): ToggleInputMode => ({
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/TOGGLE_INPUT_MODE',
  payload: { id },
});

export const fetch = (param: {| empId?: string, targetDate: string |}) => (
  dispatch: Dispatch<any>
): Promise<DailyTask> => {
  dispatch(clearState());
  return Repository.fetch(param).then((result) => {
    dispatch(fetchSuccess(result));
    return result;
  });
};

export const loadDefaultJob = (
  targetDate: string,
  defaultJob: Object,
  dailyTask: DailyTask
) => (dispatch: Dispatch<any>): void => {
  const defaultTask = getDefaultJob(defaultJob, dailyTask);

  if (dailyTask.taskList.some((t) => isDefaultJob(defaultJob.id, t))) {
    dispatch(markDefaultJob(defaultTask));
  } else {
    dispatch(pushDefaultJob(defaultTask));
  }
};

type Action =
  | ClearState
  | FetchSuccess
  | PushDefaultJob
  | MarkDefaultJob
  | UpdateTask
  | ToggleInputMode;

// Reducer

export default (state: State = initialState, action: Action) => {
  const getRealWorkTimeOrDefault = ($state: State): number => {
    return $state.realWorkTime === null
      ? 100 // 割合計算のために暫定で実労働時間を100分にする
      : $state.realWorkTime;
  };

  switch (action.type) {
    case '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/CLEAR_STATE': {
      return {
        ...initialState,
      };
    }

    case '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/FETCH_SUCCESS': {
      return {
        ...action.payload,
      };
    }

    case '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/PUSH_DEFAULT_JOB': {
      const payload = (action: PushDefaultJob).payload;
      const realWorkTime = getRealWorkTimeOrDefault(state);
      return {
        ...state,
        taskList: calculateTaskTimeFromRatio(realWorkTime)([
          ...state.taskList,
          payload,
        ]),
        hasDefaultJob: true,
      };
    }

    case '/MOBILE-APP/MODULES/TRACKING/ENTITY/DAILYTASK/MARK_DEFAULT_JOB': {
      const payload = (action: MarkDefaultJob).payload;
      const realWorkTime = getRealWorkTimeOrDefault(state);
      const taskList = state.taskList.map((task) => {
        return isDefaultJob(payload.jobId, task) ? { ...payload } : { ...task };
      });
      return {
        ...state,
        taskList: calculateTaskTimeFromRatio(realWorkTime)(taskList),
        hasDefaultJob: true,
      };
    }

    case '/MOBILE-APP/MODULES/TRACKING/ENTITY/UPDATE_TASK': {
      const payload = (action: UpdateTask).payload;
      const realWorkTime = getRealWorkTimeOrDefault(state);
      const targetTask = state.taskList.find((task) => task.id === payload.id);
      const modifiedTask = { ...targetTask, ...payload.task };
      const taskList = updateTaskList(
        realWorkTime,
        modifiedTask,
        state.taskList
      );

      return {
        ...state,
        taskList: [...taskList],
      };
    }

    case '/MOBILE-APP/MODULES/TRACKING/ENTITY/TOGGLE_INPUT_MODE': {
      const payload = (action: ToggleInputMode).payload;
      const realWorkTime = getRealWorkTimeOrDefault(state);
      const task =
        state.taskList.find((t) => t.id === payload.id) || defaultTaskValue;

      // 実労働時間がない場合には割合も時間クリアする
      if (state.realWorkTime === null) {
        task.taskTime = 0;
        task.ratio = 0;
      }

      return {
        ...state,
        taskList: toggleDirectInput(realWorkTime, task, state.taskList),
      };
    }

    default:
      return state;
  }
};
