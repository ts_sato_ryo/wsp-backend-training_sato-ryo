// @flow

import { type Job } from '../../../../domain/models/time-tracking/Job';

// State

type State = Job[];

// Action

type FetchSuccess = {
  type: '/MOBILE-APP/MODULES/TRACKING/ENTITY/JOB/FETCH_SUCCESS',
  payload: Job[],
};

const FETCH_SUCCESS: $PropertyType<FetchSuccess, 'type'> =
  '/MOBILE-APP/MODULES/TRACKING/ENTITY/JOB/FETCH_SUCCESS';

export const actions = {
  fetchSuccess: (jobs: Job[]): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: jobs,
  }),
};

type Action = FetchSuccess;

// Reducer

export default (state: State = [], action: Action): State => {
  switch (action.type) {
    case FETCH_SUCCESS: {
      return [...action.payload];
    }

    default:
      return state;
  }
};
