/* @flow */

import { combineReducers } from 'redux';

import { type $State } from '../../../../commons/utils/TypeUtil';

import dailyTask from './dailyTask';
import job from './job';

const rootReducer = {
  dailyTask,
  job,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
