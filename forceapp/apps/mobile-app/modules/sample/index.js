/* @flow */

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';

const rootReducer = {};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
