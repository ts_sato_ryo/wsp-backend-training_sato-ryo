// @flow

import { createSelector } from 'reselect';

import ApiError from '../../../commons/errors/ApiError';
import {
  catchApiError as $catchApiError,
  type CatchApiErrorAction,
} from '../../../commons/actions/app';

// State

type State = $Shape<{|
  isContinuable: boolean,
  errorCode: string,
  problem: string,
  message: string,
  stackTrace: string,
|}>;

const initialState: State = {};

// Selector

// $FlowFixMe
export const errorExists = createSelector(
  (state: State) => state,
  (state: State): boolean => {
    // Test object is empty or not.
    return !(Object.keys(state).length === 0 && state.constructor === Object);
  }
);

// Actions

type ResetError = {|
  type: 'RESET_ERROR',
|};

/**
 * Reset state
 */
export const reset = () => ({
  type: 'RESET_ERROR',
});

/**
 * Dispatch error handling action
 */
export const catchApiError = $catchApiError;

type Action = ResetError | CatchApiErrorAction;

// Reducer

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case 'RESET_ERROR': {
      return {};
    }

    case 'CATCH_API_ERROR': {
      const error: ApiError = (action: CatchApiErrorAction).payload;
      return {
        isContinuable: error.isContinuable,
        errorCode: error.errorCode,
        message: error.problem,
        stackTrace: error.stackTrace,
      };
    }

    default: {
      return state;
    }
  }
};
