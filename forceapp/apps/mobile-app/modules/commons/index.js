/* @flow */

import { combineReducers } from 'redux';

import error from './error';
import $alert from './alert';
import $confirm from './confirm';
import loading from './loading';
import location from './location';

import { type $State } from '../../../commons/utils/TypeUtil';

const rootReducer = {
  error,
  alert: $alert,
  confirm: $confirm,
  loading,
  location,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
