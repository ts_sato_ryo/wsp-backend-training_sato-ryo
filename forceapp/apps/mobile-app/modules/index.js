/* @flow */

import { combineReducers } from 'redux';

import { type $State } from '../../commons/utils/TypeUtil';

import personalSetting from '../../commons/modules/personalSetting';
import common from '../../commons/modules';
import userSetting from '../../commons/reducers/userSetting';
import approval from './approval';
import attendance from './attendance';
import expense from './expense';
import { default as mobileCommons } from './commons';
import tracking from './tracking';

const rootReducer = {
  common,
  userSetting,
  attendance,
  approval,
  expense,
  mobileCommons,
  tracking,
  personalSetting,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
