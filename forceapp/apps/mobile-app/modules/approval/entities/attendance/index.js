// @flow

import { combineReducers } from 'redux';

import request from './request';

const rootReducer = {
  request,
};

export default combineReducers<Object, Object>(rootReducer);
