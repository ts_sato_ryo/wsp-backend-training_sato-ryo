// @flow
import { type Reducer, type Dispatch } from 'redux';

import { getAttRequest } from '../../../../../domain/models/attendance/AttDailyRequest';

import { type AttDailyDetailBaseFromApi } from '../../../../../domain/models/approval/AttDailyDetail/Base';
import { type None } from '../../../../../domain/models/approval/AttDailyDetail/None';

export const ACTIONS = {
  GET_SUCCESS: 'MODULES/ENTITIES/APPROVAL/ATT/GET_SUCCESS',
};

const getSuccess = (attRequest: AttDailyDetailBaseFromApi<None>) => {
  return {
    type: ACTIONS.GET_SUCCESS,
    payload: attRequest,
  };
};

export const actions = {
  get: (requestId: string) => (
    dispatch: Dispatch<any>
  ): Promise<AttDailyDetailBaseFromApi<None>> => {
    return getAttRequest(requestId).then(
      (res: AttDailyDetailBaseFromApi<None>) => dispatch(getSuccess(res))
    );
  },
};

const initialState = null;

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.GET_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<?AttDailyDetailBaseFromApi<None>, any>);
