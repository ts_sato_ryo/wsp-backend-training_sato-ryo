// @flow
import { type Dispatch } from 'redux';

import msg from '../../../../commons/languages';
import { reject } from '../../../../domain/models/approval/request/Reject';
import { showToast } from '../../../../commons/modules/toast';

export const ACTIONS = {
  REJECT_SUCCESS: 'MODULES/ENTITIES/APPROVAL/REJECT_SUCCESS',
};

const rejectSuccess = () => {
  return {
    type: ACTIONS.REJECT_SUCCESS,
  };
};

export const actions = {
  reject: (requestIdList: Array<string>, comment: string) => async (
    dispatch: Dispatch<any>
  ): Promise<void> => {
    await reject(requestIdList, comment);
    dispatch(showToast(msg().Appr_Lbl_Rejected));
    dispatch(rejectSuccess());
  },
};
