// @flow

import { combineReducers } from 'redux';

import list from './list';
import expense from './expense';
import attendance from './attendance';

const rootReducer = {
  list,
  expense,
  attendance,
};

export default combineReducers<Object, Object>(rootReducer);
