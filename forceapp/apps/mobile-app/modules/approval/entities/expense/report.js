// @flow
import { type Reducer, type Dispatch } from 'redux';

import FileUtil from '../../../../../commons/utils/FileUtil';

import {
  type ExpRequest,
  getExpRequestReport,
} from '../../../../../domain/models/exp/request/Report';

import {
  type FilePreview,
  getFilePreview,
} from '../../../../../domain/models/exp/receipt-library/list';

export const ACTIONS = {
  GET_SUCCESS: 'MODULES/ENTITIES/APPROVAL/EXPENSE/GET_SUCCESS',
  SET_IMAGE_SUCCESS: 'MODULES/ENTITIES/APPROVAL/EXPENSE/SET_IMAGE_SUCCESS',
  SET_REPORT_IMAGE_SUCCESS:
    'MODULES/ENTITIES/APPROVAL/EXPENSE/SET_REPORT_IMAGE_SUCCESS',
};

const getSuccess = (expRequest: ExpRequest) => {
  return {
    type: ACTIONS.GET_SUCCESS,
    payload: expRequest,
  };
};

const setImageSuccess = (
  imageData: string,
  recordId: string,
  fileName: string
) => {
  return {
    type: ACTIONS.SET_IMAGE_SUCCESS,
    payload: { imageData, recordId, fileName },
  };
};

const setReportImageSuccess = (
  imageData: string,
  requestId: string,
  fileName: string
) => {
  return {
    type: ACTIONS.SET_REPORT_IMAGE_SUCCESS,
    payload: { imageData, requestId, fileName },
  };
};

export const actions = {
  get: (requestId: string) => (
    dispatch: Dispatch<any>
  ): Promise<ExpRequest> => {
    return getExpRequestReport(requestId).then((res: ExpRequest) => {
      dispatch(getSuccess(res));
      return res;
    });
  },
  setImage: (receiptFileId: string, id: string, isReport?: boolean) => (
    dispatch: Dispatch<any>
  ): Promise<any> => {
    return getFilePreview(receiptFileId).then((res: FilePreview) => {
      const dataType = FileUtil.getMIMEType(res.fileType);
      const fileUrl = `data:${dataType};base64,${res.fileBody}`;
      const fileName = FileUtil.getOriginalFileNameWithoutPrefix(res.title);

      if (isReport) {
        return dispatch(setReportImageSuccess(fileUrl, id, fileName));
      } else {
        return dispatch(setImageSuccess(fileUrl, id, fileName));
      }
    });
  },
};

const initialState = null;

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.GET_SUCCESS:
      return action.payload;
    case ACTIONS.SET_REPORT_IMAGE_SUCCESS:
      return {
        ...state,
        attachedFileData: action.payload.imageData,
        fileName: action.payload.fileName,
      };
    case ACTIONS.SET_IMAGE_SUCCESS:
      if (state) {
        const records = state.records;
        const idx = records.findIndex(
          (item) => item.recordId === action.payload.recordId
        );
        records[idx].receiptData = action.payload.imageData;
        records[idx].fileName = action.payload.fileName;
        state.records = records;
      }
      return state;
    default:
      return state;
  }
}: Reducer<?ExpRequest, any>);
