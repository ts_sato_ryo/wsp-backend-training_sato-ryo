// @flow

import { combineReducers } from 'redux';

import report from './report';

const rootReducer = {
  report,
};

export default combineReducers<Object, Object>(rootReducer);
