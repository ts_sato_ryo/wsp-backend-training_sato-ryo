/* @flow */

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';

import entities from './entities';

const rootReducer = {
  entities,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
