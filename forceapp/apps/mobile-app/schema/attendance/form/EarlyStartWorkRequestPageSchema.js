// @flow

import * as yup from 'yup'; // for everything

import msg from '../../../../commons/languages';

export default () =>
  yup.object().shape({
    startDate: yup.string().required(msg().Common_Err_Required),
    startTime: yup
      .number()
      .nullable()
      .required(msg().Common_Err_Required),
    endTime: yup
      .number()
      .nullable()
      .required(msg().Common_Err_Required),
    remarks: yup.string().nullable(),
  });
