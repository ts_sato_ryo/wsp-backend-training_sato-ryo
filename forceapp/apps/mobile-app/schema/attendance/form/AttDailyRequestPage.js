// @flow

import * as yup from 'yup'; // for everything

import { type AttDailyRequest } from '../../../../domain/models/attendance/AttDailyRequest';
import { CODE } from '../../../../domain/models/attendance/AttDailyRequestType';

import leave from './LeaveRequestPageSchema';
import absence from './AbsenceRequestPageSchema';
import direct from './DirectRequestPageSchema';
import earlyStartWork from './EarlyStartWorkRequestPageSchema';
import overtimeWork from './OvertimeWorkRequestPageSchema';
import holidayWork from './HolidayWorkRequestPageSchema';
import pattern from './PatternRequestPageSchema';

export default yup.lazy((form: AttDailyRequest) => {
  switch (form.requestTypeCode) {
    case CODE.Leave:
      return leave();
    case CODE.Absence:
      return absence();
    case CODE.Direct:
      return direct();
    case CODE.EarlyStartWork:
      return earlyStartWork();
    case CODE.OvertimeWork:
      return overtimeWork();
    case CODE.HolidayWork:
      return holidayWork();
    case CODE.Pattern:
      return pattern();
    default:
      return yup.mixed();
  }
});
