// @flow

import * as yup from 'yup'; // for everything

import msg from '../../../../commons/languages';
import { SUBSTITUTE_LEAVE_TYPE } from '../../../../domain/models/attendance/SubstituteLeaveType';

export default () =>
  yup.object().shape({
    startDate: yup.string().required(msg().Common_Err_Required),
    startTime: yup.mixed().required(msg().Common_Err_Required),
    endTime: yup.mixed().required(msg().Common_Err_Required),
    substituteLeaveType: yup.string(),
    substituteDate: yup
      .string()
      .nullable()
      .when('substituteLeaveType', {
        is: SUBSTITUTE_LEAVE_TYPE.Substitute,
        then: yup.string().required(msg().Common_Err_Required),
      }),
    remarks: yup.string().nullable(),
  });
