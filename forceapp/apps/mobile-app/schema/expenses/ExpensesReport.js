import * as Yup from 'yup';
import _ from 'lodash';

import msg from '../../../commons/languages';
import getExtendedItemsValidators from './ExtendedItemSchema';

export default Yup.lazy(() => {
  return Yup.object().shape({
    report: Yup.object().shape({
      accountingDate: Yup.string()
        .nullable()
        .required(msg().Common_Err_Required),
      subject: Yup.string()
        .required(msg().Common_Err_Required)
        .max(80, msg().Common_Err_Max)
        .trim(msg().Common_Err_Required),
      isJobRequired: Yup.boolean(),
      jobId: Yup.string()
        .when(`isJobRequired`, {
          is: (isJobRequired) => !!isJobRequired,
          then: Yup.string().required(msg().Common_Err_Required),
        })
        .nullable(),
      isCostCenterRequired: Yup.boolean(),
      costCenterName: Yup.string()
        .when(`isCostCenterRequired`, {
          is: (isCostCenterRequired) => !!isCostCenterRequired,
          then: Yup.string().required(msg().Common_Err_Required),
        })
        .nullable(),
      ...getExtendedItemsValidators(),
    }),
  });
});
