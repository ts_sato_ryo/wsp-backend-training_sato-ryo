import * as Yup from 'yup';
import _ from 'lodash';
import getExtendedItemsValidators from './ExtendedItemSchema';

export default Yup.lazy(() => {
  return Yup.object().shape({
    items: Yup.array().of(
      Yup.object().shape({
        ...getExtendedItemsValidators(),
      })
    ),
  });
});
