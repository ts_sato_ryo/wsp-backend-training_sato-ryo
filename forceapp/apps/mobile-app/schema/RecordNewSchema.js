import * as Yup from 'yup';
import { RECORD_TYPE, RECEIPT_TYPE } from '../../domain/models/exp/Record';
import msg from '../../commons/languages';
import getExtendedItemsValidators from './expenses/ExtendedItemSchema';

export default Yup.lazy((values) => {
  return Yup.object().shape({
    recordDate: Yup.string()
      .required(msg().Common_Err_Required)
      .nullable(),
    fileAttachment: Yup.string().nullable(),
    receiptId: Yup.string()
      .nullable()
      .when('fileAttachment', {
        is: RECEIPT_TYPE.Required,
        then: Yup.string().required(msg().Common_Err_Required),
      })
      .when('fileAttachment', {
        is: RECEIPT_TYPE.Optional,
        then: Yup.string().test(
          'GeneralReceiptOptional',
          msg().Exp_Err_NoReceiptAndReceipt,
          function validateAttachmentBody(value) {
            const hasRemarks =
              this.parent.items[0].remarks &&
              this.parent.items[0].remarks.trim();
            if (hasRemarks || value) {
              return true;
            }
            return false;
          }
        ),
      }),
    items: Yup.array().of(
      Yup.object().shape({
        amount: Yup.number()
          .typeError(msg().Common_Err_TypeNumber)
          .required(msg().Common_Err_Required)
          .max(999999999999, msg().Common_Err_Max)
          .nullable(),
        expTypeName: Yup.string()
          .required(msg().Common_Err_Required)
          .nullable(),
        taxTypeBaseId: Yup.string()
          .required(msg().Common_Err_Required)
          .nullable(),
        fixedAllowanceOptionId: Yup.string()
          .when({
            is: () =>
              values && values.recordType === RECORD_TYPE.FixedAllowanceMulti,
            then: Yup.string().required(msg().Common_Err_Required),
          })
          .nullable(),
        remarks: Yup.string()
          .max(1024, msg().Common_Err_Max)
          .nullable(),
        ...getExtendedItemsValidators(),
      })
    ),
  });
});
