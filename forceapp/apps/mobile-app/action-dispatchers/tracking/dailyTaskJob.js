// @flow

import { type Dispatch } from 'redux';

import DailyTaskRepository from '../../../repositories/TimeTaskDailyRepository';
import Repository from '../../../repositories/JobWorkCategoryRepository';
import { actions as job } from '../../modules/tracking/entity/job';
import {
  type State as DailyTaskJobState,
  actions as dailyTaskJob,
} from '../../modules/tracking/ui/dailyTaskJob';
import { type State as DailyTaskState } from '../../modules/tracking/entity/dailyTask';
import { catchApiError } from '../../modules/commons/error';
import { withLoading } from '../../modules/commons/loading';
import TimeUtil from '../../../commons/utils/TimeUtil';

export const initialize = (
  targetDate: string,
  jobId?: string,
  empId?: string
) => {
  const fetchData = (dispatch: Dispatch<any>) => {
    return Repository.fetch({
      targetDate,
      jobId: jobId || null,
      empId: empId || null,
    })
      .then((result) => dispatch(job.fetchSuccess(result)))
      .catch((err) => dispatch(catchApiError(err)));
  };

  return withLoading(fetchData);
};

export const selectJob = (jobId: string) => (dispatch: Dispatch<any>): void => {
  dispatch(dailyTaskJob.update('jobId', jobId));
};

export const selectWorkCategory = (workCategoryId: string) => (
  dispatch: Dispatch<any>
): void => {
  dispatch(dailyTaskJob.update('workCategoryId', workCategoryId));
};

export const editTaskTime = (taskTime: ?number) => (
  dispatch: Dispatch<any>
) => {
  dispatch(dailyTaskJob.update('taskTime', TimeUtil.toHHmm(taskTime)));
};

export const discard = () => (dispatch: Dispatch<any>) => {
  dispatch(dailyTaskJob.reset());
};

export const save = (
  dailyTaskJobState: DailyTaskJobState,
  dailyTaskState: DailyTaskState
) => {
  const newTask = {
    jobId: dailyTaskJobState.jobId,
    workCategoryId: dailyTaskJobState.workCategoryId,
    taskTime: TimeUtil.toMinutes(dailyTaskJobState.taskTime || 0),
    isDirectInput: true,
    ratio: null,
    volume: dailyTaskJobState.taskTime,
    taskNote: null,
  };

  const saveData = (dispatch: Dispatch<any>): Promise<any> =>
    // $FlowFixMe v0.98
    DailyTaskRepository.update({
      ...dailyTaskState,
      taskList: [...dailyTaskState.taskList, newTask],
    })
      .then(() => {
        dispatch(dailyTaskJob.reset());
      })
      .catch((err) => {
        dispatch(catchApiError(err));
        return Promise.reject(err);
      });

  return withLoading(saveData);
};
