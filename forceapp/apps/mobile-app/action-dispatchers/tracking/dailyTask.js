// @flow

import { type Dispatch } from 'redux';

import * as personalSetting from '../../../commons/modules/personalSetting';
import * as dailyTask from '../../modules/tracking/entity/dailyTask';
import * as dailyTaskUIActions from '../../modules/tracking/ui/dailyTask';
import { catchApiError } from '../../modules/commons/error';
import { withLoading } from '../../modules/commons/loading';

import {
  isDefaultJobAvailable,
  type DailyTask,
} from '../../../domain/models/time-tracking/DailyTask';
import {
  isDefaultJob,
  type Task,
} from '../../../domain/models/time-tracking/Task';

import Repository from '../../../repositories/TimeTaskDailyRepository';

// eslint-disable-next-line import/prefer-default-export
export const initialize = (param: {| empId?: string, targetDate: string |}) => {
  const fetchData = (dispatch: Dispatch<any>) =>
    Promise.all([
      dispatch(personalSetting.fetch()),
      dispatch(dailyTask.fetch(param)),
    ])
      .then(([{ defaultJob }, result: DailyTask]) => {
        // TODO
        // 残工数ジョブが設定されてない場合にはnullが返ってくるべきだが、
        // 現在はフィールドにnullが設定されたジョブが返ってくる。
        // 残工数ジョブをnullで返してもらう対応はすぐにできなかったので、
        // ここでは暫定対応でidをチェックしている。
        if (
          defaultJob !== null &&
          defaultJob.id !== null &&
          defaultJob.id !== undefined &&
          isDefaultJobAvailable(param.targetDate, defaultJob)
        ) {
          dispatch(
            dailyTask.loadDefaultJob(param.targetDate, defaultJob, result)
          );

          for (const task: Task of result.taskList) {
            if (!isDefaultJob(defaultJob.id, task)) {
              dispatch(
                dailyTask.updateTask(task.id, {
                  ...task,
                  isDirectInput: true,
                })
              );
            }
          }
        }
      })
      .catch((err) => dispatch(catchApiError(err)));

  return withLoading(fetchData);
};

export const editTask = (id: string, task: $Shape<Task>) => (
  dispatch: Dispatch<any>
) => {
  dispatch(dailyTask.updateTask(id, task));
};

export const saveTask = (task: dailyTask.State, empId?: string) => {
  const update = (dispatch: Dispatch<any>) =>
    Repository.update(task, empId)
      .then(() => dispatch(dailyTaskUIActions.reset()))
      .then(() =>
        dispatch(dailyTask.fetch({ targetDate: task.targetDate, empId }))
      )
      .catch((err) => dispatch(catchApiError(err)));

  return withLoading(update);
};
