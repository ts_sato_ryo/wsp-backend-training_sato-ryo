// @flow
import moment from 'moment';

import TimesheetRepository from '../../../repositories/TimesheetRepository';
import AttDailyTimeRepository from '../../../repositories/AttDailyTimeRepository';
import AttDailyRemarksRepository from '../../../repositories/AttDailyRemarksRepository';

import {
  type Timesheet,
  isTargetDateInTimesheet,
} from '../../../domain/models/attendance/Timesheet';
import { convertToUpdateRequestParam } from '../../../domain/models/attendance/AttDailyTime';
import * as DailyRestTimeFill from '../../../domain/models/attendance/DailyRestTimeFill';

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';
import { showConfirm } from '../../modules/commons/confirm';
import { showToast } from '../../../commons/modules/toast';
import { getUserSetting } from '../../../commons/actions/userSetting';
import { actions as EntitiesActions } from '../../modules/attendance/timesheet/entities';
import { actions as UiMonthlyPagingActions } from '../../modules/attendance/timesheet/ui/monthly/paging';
import {
  type State as UiDailyEditingState,
  actions as UiDailyEditingActions,
} from '../../modules/attendance/timesheet/ui/daily/editing';
import { actions as UiDailyPagingActions } from '../../modules/attendance/timesheet/ui/daily/paging';

import TextUtil from '../../../commons/utils/TextUtil';
import msg from '../../../commons/languages';

const isTimesheetSomeMonth = (targetDate?: string, timesheet?: Timesheet) => {
  if (timesheet !== null && timesheet !== undefined) {
    return isTargetDateInTimesheet(targetDate, timesheet);
  }
  return false;
};

const loadTimesheet = (targetDate: string) => {
  return TimesheetRepository.fetch(targetDate);
};

const clearTimesheetMonthly = () => (dispatch: Dispatch<*>) => {
  dispatch(UiMonthlyPagingActions.clear());
  dispatch(EntitiesActions.clear());
};

const clearTimesheetDaily = () => (dispatch: Dispatch<*>) => {
  dispatch(UiDailyPagingActions.clear());
  dispatch(UiDailyEditingActions.clear());
  dispatch(EntitiesActions.clear());
};

const setTimesheetMonthly = (timesheet?: Timesheet) => (
  dispatch: Dispatch<any>
) => {
  if (timesheet) {
    dispatch(UiMonthlyPagingActions.fetchSuccess(timesheet));
    dispatch(EntitiesActions.fetchSuccess(timesheet));
  } else {
    dispatch(clearTimesheetMonthly());
  }
};

const setTimesheetDaily = (targetDate: string, timesheet?: Timesheet) => (
  dispatch: Dispatch<any>
) => {
  if (timesheet) {
    dispatch(UiDailyEditingActions.fetchSuccess(targetDate, timesheet));
    dispatch(UiDailyPagingActions.fetchSuccess(targetDate, timesheet));
    dispatch(EntitiesActions.fetchSuccess(timesheet));
  } else {
    dispatch(clearTimesheetDaily());
    dispatch(UiDailyPagingActions.setCurrent(targetDate));
  }
};

const resetTimesheetDaily = (targetDate: string) => (dispatch: Dispatch<*>) => {
  return loadTimesheet(targetDate)
    .catch((error) => {
      dispatch(
        catchApiError(error, {
          isContinuable: true,
        })
      );
    })
    .then((timesheet) => dispatch(setTimesheetDaily(targetDate, timesheet)));
};

const resetTimesheetMonthly = (targetDate?: string) => (
  dispatch: Dispatch<*>
) => {
  return loadTimesheet(targetDate || '')
    .catch((error) => {
      dispatch(
        catchApiError(error, {
          isContinuable: false,
        })
      );
    })
    .then((timesheet) => dispatch(setTimesheetMonthly(timesheet)));
};

export const initializeMonthly = (
  targetDate?: string,
  timesheet?: Timesheet
) => (dispatch: Dispatch<any>) => {
  dispatch(clearTimesheetMonthly());
  dispatch(
    withLoading(
      (_dispatch: Dispatch<any>) => _dispatch(getUserSetting()),
      (_dispatch: Dispatch<any>) => {
        if (isTimesheetSomeMonth(targetDate, timesheet)) {
          return _dispatch(setTimesheetMonthly(timesheet));
        } else {
          return _dispatch(resetTimesheetMonthly(targetDate));
        }
      }
    )
  );
};

export const initializeDaily = (
  _targetDate?: string,
  timesheet?: Timesheet
) => (dispatch: Dispatch<any>) => {
  const targetDate = _targetDate || moment().format('YYYY-MM-DD');
  dispatch(clearTimesheetDaily());
  dispatch(
    withLoading(
      (_dispatch: Dispatch<any>) => _dispatch(getUserSetting()),
      (_dispatch: Dispatch<any>) => {
        if (isTimesheetSomeMonth(targetDate, timesheet)) {
          return _dispatch(setTimesheetDaily(targetDate, timesheet));
        } else {
          return _dispatch(resetTimesheetDaily(targetDate));
        }
      }
    )
  );
};

export const saveAttDailyRecord = (editing: UiDailyEditingState) => (
  dispatch: Dispatch<*>
) => {
  const { targetDate } = editing;
  dispatch(
    withLoading(async () => {
      try {
        const result = await AttDailyTimeRepository.update(
          targetDate,
          convertToUpdateRequestParam({
            targetDate,
            startTime: editing.startTime,
            endTime: editing.endTime,
            restTimes: editing.restTimes,
            restHours: editing.restHours,
          })
        );
        await AttDailyRemarksRepository.update(
          editing.id,
          editing.remarks || ''
        );

        if (Number(result.insufficientRestTime)) {
          const confirmText = TextUtil.template(
            msg().Com_Msg_InsufficientRestTime,
            Number(result.insufficientRestTime)
          );
          if (await dispatch(showConfirm(confirmText))) {
            await DailyRestTimeFill.post({ targetDate });
          }
        }
      } catch (error) {
        dispatch(catchApiError(error));
        throw error;
      }
      dispatch(showToast(msg().Att_Lbl_SaveWorkTime));
      await dispatch(resetTimesheetDaily(targetDate));
    })
  );
};
