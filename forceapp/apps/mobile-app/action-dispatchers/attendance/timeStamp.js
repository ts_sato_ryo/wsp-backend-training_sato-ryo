// @flow
import { type Dispatch } from 'redux';

import msg from '../../../commons/languages';

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';
import { showToast } from '../../../commons/modules/toast';
import { showConfirm } from '../../modules/commons/confirm';
import * as locationActions from '../../modules/commons/location';
import { getUserSetting } from '../../../commons/actions/userSetting';
import BaseWSPError from '../../../commons/errors/BaseWSPError';
import TextUtil from '../../../commons/utils/TextUtil';
import * as DailyRestTimeFill from '../../../domain/models/attendance/DailyRestTimeFill';

import {
  type PostStampRequest,
  type DailyStampTime,
  STAMP_SOURCE,
  CLOCK_TYPE,
  fetchDailyStampTime,
  postStamp as apiPostStamp,
} from '../../../domain/models/attendance/DailyStampTime';

import { actions as dailyStampTimeActions } from '../../modules/attendance/timeStamp/entities/dailyStampTime';
import { actions as uiActions } from '../../modules/attendance/timeStamp/ui';

export const onToggleSendLocation = (willSend: boolean) => (
  dispatch: Dispatch<any>
) => {
  dispatch(uiActions.setWillSendLocation(willSend));
  if (willSend) {
    dispatch(locationActions.fetchLocation());
  } else {
    // 手動でチェックを外した場合は、現在地情報をクリアする
    dispatch(locationActions.unset());
  }
};

const getStampSettings = () => (dispatch: Dispatch<any>): Promise<any> =>
  fetchDailyStampTime()
    .then((result: DailyStampTime) =>
      dispatch(dailyStampTimeActions.set(result))
    )
    .catch((error) => dispatch(catchApiError(error, { isContinuable: false })));

// 画面初期化
export const initialize = (willSend: boolean) =>
  withLoading(
    async (_dispatch: Dispatch<any>) => {
      const userSetting = await _dispatch(getUserSetting());
      const { requireLocationAtMobileStamp } = userSetting;
      _dispatch(onToggleSendLocation(requireLocationAtMobileStamp && willSend));
    },
    (_dispatch: Dispatch<any>) => _dispatch(getStampSettings())
  );

// 打刻
const postStamp = (
  param: PostStampRequest,
  shouldSendLocation: boolean,
  willSendLocation: boolean,
  hasLocation: boolean
) => async (dispatch: Dispatch<any>) => {
  try {
    if (shouldSendLocation && !hasLocation && !param.message) {
      throw new BaseWSPError(
        msg().Com_Lbl_Error,
        msg().Att_Err_RequireCommentWithoutLocation
      );
    }
    const result = await apiPostStamp(param, STAMP_SOURCE.MOBILE);
    if (Number(result.insufficientRestTime)) {
      const confirmText = TextUtil.template(
        msg().Com_Msg_InsufficientRestTime,
        Number(result.insufficientRestTime)
      );

      if (await dispatch(showConfirm(confirmText))) {
        await DailyRestTimeFill.post();
      }
    }

    let stampDoneMsg: string = '';
    switch (param.mode) {
      case CLOCK_TYPE.CLOCK_IN:
        stampDoneMsg = msg().Att_Msg_TimeStampDoneIn;
        break;
      case CLOCK_TYPE.CLOCK_REIN:
        stampDoneMsg = msg().Att_Msg_TimeStampDoneRein;
        break;
      case CLOCK_TYPE.CLOCK_OUT:
        stampDoneMsg = msg().Att_Msg_TimeStampDoneOut;
        break;
      default:
        return;
    }
    dispatch(showToast(stampDoneMsg));

    dispatch(uiActions.clearMessage());
  } catch (error) {
    dispatch(catchApiError(error, { isContinuable: true }));
  }
  dispatch(initialize(willSendLocation));
};

// 打刻+ローディング
export const stamp = (
  param: PostStampRequest,
  shouldSendLocation: boolean,
  willSendLocation: boolean,
  hasLocation: boolean
) =>
  withLoading((dispatch: Dispatch<any>) =>
    dispatch(
      postStamp(param, shouldSendLocation, willSendLocation, hasLocation)
    )
  );
