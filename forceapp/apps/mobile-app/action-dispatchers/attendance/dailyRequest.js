// @flow

import isNil from 'lodash/isNil';
import { bindActionCreators, type Dispatch } from 'redux';

import {
  type AttDailyRequest,
  EDIT_ACTION,
  DISABLE_ACTION,
  isForReapply,
} from '../../../domain/models/attendance/AttDailyRequest';
import { doesAttLeaveExist } from '../../../domain/models/attendance/LeaveType';
import * as LeaveRequest from '../../../domain/models/attendance/AttDailyRequest/LeaveRequest';
import * as PatternRequest from '../../../domain/models/attendance/AttDailyRequest/PatternRequest';
import { CODE } from '../../../domain/models/attendance/AttDailyRequestType';
import {
  type Timesheet,
  isTargetDateInTimesheet,
  getAttDailyRecordByDate,
} from '../../../domain/models/attendance/Timesheet';

import msg from '../../../commons/languages';

import Repository from '../../../repositories/AttDailyRequestRepository';
import TimesheetRepository from '../../../repositories/TimesheetRepository';
import AttDailyLeaveRepository from '../../../repositories/AttDailyLeaveRepository';
import AttDailyPatternRepository from '../../../repositories/AttDailyPatternRepository';

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';
import { showConfirm } from '../../modules/commons/confirm';
import { getUserSetting } from '../../../commons/actions/userSetting';
import { showToast } from '../../../commons/modules/toast';
import { actions } from '../../modules/attendance/dailyRequest/ui/detail';
import { actions as leaveRequestActions } from '../../modules/attendance/dailyRequest/ui/requests/leaveRequest';
import { actions as patternRequestActions } from '../../modules/attendance/dailyRequest/ui/requests/patternRequest';
import { actions as ValidationActions } from '../../modules/attendance/dailyRequest/ui/validation';
import {
  type State as TimesheetState,
  actions as TimesheetEntitiesActions,
} from '../../modules/attendance/timesheet/entities';
import { actions as TimesheetDailyUiPagingActions } from '../../modules/attendance/timesheet/ui/daily/paging';
import { actions as EntitiesLatestRequestsActions } from '../../modules/attendance/dailyRequest/entities/latestRequests';
import { actions as EntitiesAvailableRequestsActions } from '../../modules/attendance/dailyRequest/entities/availableRequests';

const loadTimesheet = (targetDate: string) => {
  return TimesheetRepository.fetch(targetDate);
};

const setDailyRequest = (targetDate: string, timesheet: Timesheet) => (
  dispatch: Dispatch<any>
) => {
  const attDailyRecord = getAttDailyRecordByDate(targetDate, timesheet);
  dispatch(TimesheetDailyUiPagingActions.fetchSuccess(targetDate, timesheet));
  dispatch(TimesheetEntitiesActions.fetchSuccess(timesheet));
  if (attDailyRecord) {
    dispatch(
      EntitiesLatestRequestsActions.initialize(attDailyRecord, timesheet)
    );
    dispatch(
      EntitiesAvailableRequestsActions.initialize(attDailyRecord, timesheet)
    );
  } else {
    throw new Error('Not find AttDailyRecord.');
  }
};

const resetDailyRequest = (targetDate: string) => (dispatch: Dispatch<any>) => {
  return loadTimesheet(targetDate).then((timesheet) =>
    dispatch(setDailyRequest(targetDate, timesheet))
  );
};

const initializeDailyRequest = (
  targetDate: string,
  timesheet?: TimesheetState
) => (dispatch: Dispatch<any>) => {
  if (!isNil(timesheet) && isTargetDateInTimesheet(targetDate, timesheet)) {
    return dispatch(setDailyRequest(targetDate, timesheet));
  } else {
    return dispatch(resetDailyRequest(targetDate));
  }
};

export const initialize = (targetDate: string, timesheet?: TimesheetState) => (
  dispatch: Dispatch<any>
) =>
  dispatch(
    withLoading(async () => {
      dispatch(ValidationActions.clear());
      await dispatch(getUserSetting());
      await dispatch(initializeDailyRequest(targetDate, timesheet));
    })
  );

export const startEditing = (target: AttDailyRequest) => async (
  dispatch: Dispatch<any>
) => {
  dispatch(ValidationActions.clear());
  dispatch(actions.startEditing());
  if (target.requestTypeCode === CODE.Leave) {
    try {
      await dispatch(
        withLoading(async () => {
          const attLeaveList = await AttDailyLeaveRepository.search({
            targetDate: target.startDate,
            ignoredId: target.id,
          });
          if (isForReapply(target)) {
            if (doesAttLeaveExist(attLeaveList, target)) {
              const leave = LeaveRequest.create(target, attLeaveList);
              dispatch(leaveRequestActions.initialize(leave, attLeaveList));
            } else {
              const leave = LeaveRequest.create(target);
              dispatch(leaveRequestActions.initialize(leave));
            }
          } else {
            const leave = LeaveRequest.create(target, attLeaveList);
            dispatch(leaveRequestActions.initialize(leave, attLeaveList));
          }
        })
      );
    } catch (e) {
      dispatch(catchApiError(e));
    }
  } else if (target.requestTypeCode === CODE.Pattern) {
    try {
      dispatch(
        withLoading(async () => {
          const attPatternList = await AttDailyPatternRepository.search({
            targetDate: target.startDate,
            ignoredId: target.id,
          });
          const pattern = PatternRequest.create(
            target,
            attPatternList,
            target.startDate
          );
          dispatch(patternRequestActions.initialize(pattern, attPatternList));
        })
      );
    } catch (e) {
      dispatch(catchApiError(e));
    }
  }
};

const validate = (target: AttDailyRequest) => (
  dispatch: Dispatch<any>
): Promise<boolean> => dispatch(ValidationActions.validate(target));

export const create = (target: AttDailyRequest, targetDate: string) => async (
  dispatch: Dispatch<any>
): Promise<boolean> => {
  const appService = bindActionCreators(
    {
      withLoading,
      catchApiError,
    },
    dispatch
  );
  const dailyRequestService = bindActionCreators(
    { initializeDailyRequest, validate },
    dispatch
  );

  const isValid = await dailyRequestService.validate(target);
  if (!isValid) {
    return false;
  }

  try {
    await appService.withLoading(async () => {
      // submit
      await Repository.create(target);
      await dailyRequestService.initializeDailyRequest(targetDate);
    });
    dispatch(showToast(msg().Att_Lbl_Requested));
    return true;
  } catch (err) {
    appService.catchApiError(err, { isContinuable: true });
    return false;
  }
};

export const modify = (target: AttDailyRequest, targetDate: string) => async (
  dispatch: Dispatch<any>
): Promise<boolean> => {
  const appService = bindActionCreators(
    {
      withLoading,
      catchApiError,
    },
    dispatch
  );
  const dailyRequestService = bindActionCreators(
    { initializeDailyRequest, validate },
    dispatch
  );

  const isValid = await dailyRequestService.validate(target);
  if (!isValid) {
    return false;
  }

  try {
    await appService.withLoading(async () => {
      // submit
      await Repository.update(target, EDIT_ACTION.Modify);
      await dailyRequestService.initializeDailyRequest(targetDate);
    });
    dispatch(showToast(msg().Att_Lbl_Requested));
    return true;
  } catch (err) {
    appService.catchApiError(err, { isContinuable: true });
    return false;
  }
};

export const reapply = (target: AttDailyRequest, targetDate: string) => async (
  dispatch: Dispatch<any>
): Promise<boolean> => {
  const appService = bindActionCreators(
    {
      withLoading,
      catchApiError,
    },
    dispatch
  );
  const dailyRequestService = bindActionCreators(
    { initializeDailyRequest, validate },
    dispatch
  );

  const isValid = await dailyRequestService.validate(target);
  if (!isValid) {
    return false;
  }

  try {
    await appService.withLoading(async () => {
      await Repository.update(target, EDIT_ACTION.Reapply);
      await dailyRequestService.initializeDailyRequest(targetDate);
    });
    dispatch(showToast(msg().Att_Lbl_Requested));
    return true;
  } catch (err) {
    appService.catchApiError(err, { isContinuable: true });
    return false;
  }
};

export const cancelRequest = (id: string, targetDate: string) => async (
  dispatch: Dispatch<any>
): Promise<boolean> => {
  try {
    await dispatch(
      withLoading(async () => {
        await Repository.delete(id, DISABLE_ACTION.CancelRequest);
        await dispatch(initializeDailyRequest(targetDate));
      })
    );
    dispatch(showToast(msg().Att_Lbl_CancelRequest));
    return true;
  } catch (err) {
    dispatch(catchApiError(err, { isContinuable: true }));
    return false;
  }
};

export const cancelApproval = (id: string, targetDate: string) => async (
  dispatch: Dispatch<any>
): Promise<boolean> => {
  try {
    await dispatch(
      withLoading(async () => {
        await Repository.delete(id, DISABLE_ACTION.CancelApproval);
        await dispatch(initializeDailyRequest(targetDate));
      })
    );
    dispatch(showToast(msg().Att_Lbl_CancelRequest));
    return true;
  } catch (err) {
    dispatch(catchApiError(err, { isContinuable: true }));
    return false;
  }
};

export const remove = (id: string, targetDate: string) => async (
  dispatch: Dispatch<any>
): Promise<boolean> => {
  if (!(await dispatch(showConfirm(msg().Att_Msg_DailyReqConfirmRemove)))) {
    return false;
  }
  try {
    await dispatch(
      withLoading(async () => {
        await Repository.delete(id, DISABLE_ACTION.Remove);
        await dispatch(initializeDailyRequest(targetDate));
      })
    );
    dispatch(showToast(msg().Att_Lbl_RemoveRequest));
    return true;
  } catch (err) {
    dispatch(catchApiError(err, { isContinuable: true }));
    return false;
  }
};
