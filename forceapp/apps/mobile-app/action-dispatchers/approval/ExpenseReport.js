// @flow

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';

import type { ExpRequest } from '../../../domain/models/exp/request/Report';
import { actions as expRequestActions } from '../../modules/approval/entities/expense/report';

/* eslint-disable import/prefer-default-export */
export const getExpRequest = (requestId: string) => (
  dispatch: Dispatch<any>
) => {
  return dispatch(withLoading(expRequestActions.get(requestId)))
    .then((res: Array<ExpRequest>) => {
      const report = res[0];
      return report;
    })
    .catch((err) => {
      dispatch(catchApiError(err));
      throw err;
    });
};

export const setImageData = (
  receiptFileId: string,
  id: string,
  isReport?: boolean
) => (dispatch: Dispatch<any>) => {
  dispatch(
    withLoading(expRequestActions.setImage(receiptFileId, id, isReport))
  ).catch((err) => {
    dispatch(catchApiError(err));
    throw err;
  });
};
