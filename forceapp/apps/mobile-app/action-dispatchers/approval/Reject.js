// @flow

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';

import { actions as rejectActions } from '../../modules/approval/entities/reject';

/* eslint-disable import/prefer-default-export */
export const reject = (requestIdList: Array<string>, comment: string) => {
  return (dispatch: Dispatch<any>) => {
    return dispatch(
      withLoading(rejectActions.reject(requestIdList, comment))
    ).catch((err) => {
      dispatch(catchApiError(err));
      throw err;
    });
  };
};
