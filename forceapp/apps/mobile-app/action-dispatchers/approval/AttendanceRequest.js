// @flow

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';
import { actions } from '../../modules/approval/entities/attendance/request';

/* eslint-disable import/prefer-default-export */
export const getAttRequest = (requestId: string) => {
  return (dispatch: Dispatch<any>) => {
    return dispatch(withLoading(actions.get(requestId))).catch((err) => {
      dispatch(catchApiError(err));
      throw err;
    });
  };
};
