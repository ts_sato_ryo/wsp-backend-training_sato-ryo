// @flow

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';

import { actions as approveActions } from '../../modules/approval/entities/approve';

/* eslint-disable import/prefer-default-export */
export const approve = (requestIdList: Array<string>, comment: string) => {
  return (dispatch: Dispatch<any>) => {
    return dispatch(
      withLoading(approveActions.approve(requestIdList, comment))
    ).catch((err) => {
      dispatch(catchApiError(err));
      throw err;
    });
  };
};
