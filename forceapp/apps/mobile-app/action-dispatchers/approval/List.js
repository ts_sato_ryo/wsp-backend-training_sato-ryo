// @flow

import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';

import { actions as listActions } from '../../modules/approval/entities/list';

/* eslint-disable import/prefer-default-export */
export const fetchApprRequestList = () => {
  return (dispatch: Dispatch<any>) => {
    return dispatch(withLoading(listActions.list())).catch((err) => {
      dispatch(catchApiError(err));
      throw err;
    });
  };
};
