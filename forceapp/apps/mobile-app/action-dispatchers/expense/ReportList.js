// @flow

import { withLoading } from '../../modules/commons/loading';

import { actions as reportListActions } from '../../modules/expense/entities/reportList';

export const getReportList = (empId?: ?string) => (dispatch: Dispatch<any>) =>
  dispatch(withLoading(reportListActions.list(empId)));

export const deleteReport = (reportId: string) => (dispatch: Dispatch<any>) =>
  dispatch(withLoading(reportListActions.delete(reportId)));
