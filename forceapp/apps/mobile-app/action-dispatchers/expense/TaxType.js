// @flow

import { withLoading } from '../../modules/commons/loading';

import { actions as taxTypeActions } from '../../modules/expense/entities/taxType';

/* eslint-disable import/prefer-default-export */
export const getTaxTypeList = (expTypeId: string, targetDate: string) => (
  dispatch: Dispatch<any>
) => dispatch(withLoading(taxTypeActions.list(expTypeId, targetDate)));
