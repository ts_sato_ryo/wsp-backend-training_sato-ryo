// @flow

import { type Dispatch } from 'redux';
import _ from 'lodash';

import { actions } from '../../modules/expense/entities/report';
import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';
import { type Report } from '../../../domain/models/exp/Report';
import { type Record } from '../../../domain/models/exp/Record';

export const initialize = (reportId: string, accountingPeriodList: any) => {
  const fetchData = (dispatch: Dispatch<any>): Promise<void> =>
    dispatch(actions.fetch(reportId))
      .then((res) => {
        const fileData = res.payload.attachedFileData;
        const fileVerId = res.payload.attachedFileVerId;
        if (fileVerId && !fileData) {
          dispatch(actions.setImage(fileVerId, reportId));
        }
        const selectedAccountingPeriod = res.payload.accountingPeriodId;
        const isExistAccountingPeriodinList = _.find(accountingPeriodList, {
          id: selectedAccountingPeriod,
        });
        // if selected accounting period is inactive, reselect first one of active accounting period list
        if (!isExistAccountingPeriodinList && accountingPeriodList.length > 0) {
          dispatch(
            actions.updateValue(
              'accountingPeriodId',
              accountingPeriodList[0].value
            )
          );
          dispatch(
            actions.updateValue(
              'accountingDate',
              accountingPeriodList[0].recordDate
            )
          );
        }
      })
      .catch((err) => dispatch(catchApiError(err, { isContinuable: false })));

  return withLoading(fetchData);
};

export const createNewReport = (
  records: Record[],
  userSetting: Object,
  accountingPeriodList: any,
  expReportTypeList: any,
  accountingPeriodId: ?string,
  reportTypeId: ?string,
  accountingDate: ?string
) => (dispatch: Dispatch<any>) => {
  const selectedReportType = expReportTypeList.find(
    (report) => report.id === reportTypeId
  );
  dispatch(actions.clear());
  dispatch(actions.setRecords(records));
  dispatch(actions.updateValue('costCenterName', userSetting.costCenterName));
  dispatch(
    actions.updateValue('costCenterHistoryId', userSetting.costCenterHistoryId)
  );

  if (accountingPeriodId) {
    const selectedAccountingPeriod = accountingPeriodList.find(
      (ap) => ap.id === accountingPeriodId
    );
    dispatch(actions.updateValue('accountingPeriodId', accountingPeriodId));
    dispatch(
      actions.updateValue('accountingDate', selectedAccountingPeriod.recordDate)
    );
  } else {
    dispatch(actions.updateValue('accountingDate', accountingDate));
  }

  if (selectedReportType) {
    dispatch(actions.updateValue('expReportTypeId', reportTypeId));
    dispatch(actions.setEI(selectedReportType));
  } else {
    dispatch(actions.updateValue('expReportTypeId', expReportTypeList[0].id));
    dispatch(
      actions.updateValue(
        'useFileAttachment',
        expReportTypeList[0].useFileAttachment
      )
    );
    dispatch(actions.setEI(expReportTypeList[0]));
  }
};

export const remove = (reportId: string) => (dispatch: Dispatch<any>) =>
  dispatch(withLoading(actions.delete(reportId))).catch((err) => {
    dispatch(catchApiError(err));
    throw err;
  });

export const submit = (reportId: string, comment: string) => (
  dispatch: Dispatch<any>
) =>
  dispatch(withLoading(actions.submit(reportId, comment))).catch((err) => {
    dispatch(catchApiError(err));
    throw err;
  });

export const save = (report: Report) => {
  const saveData = (dispatch: Dispatch<any>) =>
    dispatch(actions.save(report)).catch((err) => {
      dispatch(catchApiError(err));
      throw err;
    });

  return withLoading(saveData);
};
