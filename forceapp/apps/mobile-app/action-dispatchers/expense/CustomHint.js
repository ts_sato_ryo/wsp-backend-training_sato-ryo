// @flow
import { withLoading } from '../../modules/commons/loading';
import { actions as customHintActions } from '../../modules/expense/entities/customHint';

/* eslint-disable import/prefer-default-export */
export const getCustomHints = (companyId: string) => (
  dispatch: Dispatch<any>
) => dispatch(withLoading(customHintActions.get(companyId)));
