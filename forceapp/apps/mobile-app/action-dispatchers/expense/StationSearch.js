// @flow
import _ from 'lodash';

import msg from '../../../commons/languages';
import { startLoading, endLoading } from '../../modules/commons/loading';
import {
  type Station,
  searchStation,
  getSuggestion,
} from '../../../domain/models/exp/jorudan/Station';
import { type SearchRouteParam } from '../../../domain/models/exp/jorudan/Route';

export const getStationSuggestion = (
  searchString: string,
  targetDate: string,
  category: ?string = null
) => (dispatch: Dispatch<any>) => {
  const loadingId = dispatch(startLoading());

  return getSuggestion(searchString, targetDate, category).then((result) => {
    dispatch(endLoading(loadingId));
    return result;
  });
};

export const searchStations = (searchRouteParam: SearchRouteParam) => (
  dispatch: Dispatch<any>
) => {
  const param: any = { ...searchRouteParam };
  const errors = {};
  const searchStationList = [];

  // helper functions
  const searchStationWithKey = (key: string) =>
    searchStation(_.get(param, `${key}.name`), param.targetDate, null)
      .then((result: Station) => {
        if (result.num > 0) {
          const station = result.stationList[0];
          _.set(param, key, station);
        } else {
          _.set(errors, `${key}.name`, msg().Cmn_Lbl_SuggestNoResult);
        }
      })
      .catch((err) => {
        throw err;
      });

  if (!param.origin.company && param.origin.name) {
    searchStationList.push(searchStationWithKey('origin'));
  }

  if (!param.arrival.company && param.arrival.name) {
    searchStationList.push(searchStationWithKey('arrival'));
  }

  if (param.viaList.length > 0) {
    param.viaList.forEach((viaList, id) => {
      if (!viaList.company && viaList.name) {
        searchStationList.push(searchStationWithKey(`viaList.${id}`));
      }
    });
  }

  const loadingId = dispatch(startLoading());
  return Promise.all(searchStationList).then(() => {
    dispatch(endLoading(loadingId));
    const result = {
      param,
      errors,
    };

    return result;
  });
};
