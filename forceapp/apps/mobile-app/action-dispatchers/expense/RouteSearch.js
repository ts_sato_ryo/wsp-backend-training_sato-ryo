// @flow
import { withLoading } from '../../modules/commons/loading';
import { catchApiError } from '../../modules/commons/error';

import {
  searchSuccess,
  actions as routeActions,
} from '../../../domain/modules/exp/jorudan/route';
import {
  type SearchRouteParam,
  searchRoute,
} from '../../../domain/models/exp/jorudan/Route';

export const searchRouteWithParam = (param: SearchRouteParam) => (
  dispatch: Dispatch<any>
): Promise<any> =>
  dispatch(
    withLoading(() =>
      searchRoute(param).then((res: any) => {
        dispatch(searchSuccess(res));
        return true;
      })
    )
  ).catch((err) => {
    dispatch(catchApiError(err));
    return false;
  });

export const clearRouteResults = () => (dispatch: Dispatch<any>) => {
  dispatch(routeActions.clear());
};
