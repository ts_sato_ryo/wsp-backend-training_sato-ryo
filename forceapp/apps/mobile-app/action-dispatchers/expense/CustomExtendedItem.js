// @flow
import { withLoading } from '../../modules/commons/loading';
import { actions as costomEIOptionActions } from '../../modules/expense/entities/customEIOption';

export const getCustomExtendItemOptions = (
  extendedItemCustomId: string,
  query?: string
) => (dispatch: Dispatch<any>) =>
  dispatch(withLoading(costomEIOptionActions.get(extendedItemCustomId, query)));

export const getRecentlyUsed = (
  eiCustomId: string,
  eiLookupId: string,
  empId: string
) => (dispatch: Dispatch<any>) =>
  dispatch(
    withLoading(
      costomEIOptionActions.getRecentlyUsed(eiCustomId, eiLookupId, empId)
    )
  );
