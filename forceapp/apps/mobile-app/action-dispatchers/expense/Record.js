// @flow

import {
  withLoading,
  startLoading,
  endLoading,
} from '../../modules/commons/loading';
import { showAlert } from '../../modules/commons/alert';

import msg from '../../../commons/languages';
import FileUtil from '../../../commons/utils/FileUtil';

import { type Record } from '../../../domain/models/exp/Record';
import { type Base64FileList } from '../../../domain/models/exp/Receipt';
import { actions as recordActions } from '../../modules/expense/entities/recordList';

export const createRecord = (values: Record) => (dispatch: Dispatch<any>) =>
  dispatch(withLoading(recordActions.save(values)));

export const deleteRecord = (recordId: string) => (dispatch: Dispatch<any>) =>
  dispatch(withLoading(recordActions.delete(recordId)));

export const getBase64files = (e: SyntheticInputEvent<HTMLInputElement>) => (
  dispatch: Dispatch<Base64FileList>
) => {
  const newFiles = e.currentTarget.files;
  const loadingId = dispatch(startLoading());
  const promise = Array.from(newFiles).map((file) =>
    FileUtil.getBase64(file).then((data) => ({
      name: file.name,
      data,
      size: file.size,
      type: file.type,
    }))
  );
  return Promise.all(promise).then((files) => {
    dispatch(endLoading(loadingId));
    return files;
  });
};

export const uploadReceipts = (files: Base64FileList) => (
  dispatch: Dispatch<any>
) =>
  dispatch(withLoading(recordActions.uploadReceipt(files)))
    .then(async () => {
      await dispatch(showAlert(msg().Exp_Lbl_ReceiptUploadSuccess));
    })
    .catch(async (err) => {
      const errMsg =
        (err.message && ` (${err.message})`) ||
        (err.event && ` (${err.event.message})`) ||
        '';
      await dispatch(
        showAlert(`${msg().Exp_Lbl_ReceiptUploadFailed}${errMsg}`)
      );
    });
