// @flow

import { withLoading } from '../../modules/commons/loading';
import { actions as jobListActions } from '../../modules/expense/entities/jobList';
import { actions as jobAllListActions } from '../../modules/expense/entities/jobAllList';

export const getJobList = (
  empId: string,
  parentId?: ?string,
  targetDate: string
) => (dispatch: Dispatch<any>) =>
  dispatch(withLoading(jobListActions.get(empId, parentId, targetDate)));

export const searchJob = (empId: string, targetDate: string) => (
  dispatch: Dispatch<any>
) => dispatch(withLoading(jobAllListActions.search(empId, targetDate)));
