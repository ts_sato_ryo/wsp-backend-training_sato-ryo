// @flow

import { withLoading } from '../../modules/commons/loading';

import { actions as expReportTypeActions } from '../../modules/expense/entities/expReportType';

/* eslint-disable import/prefer-default-export */
export const getExpReportTypeList = (
  companyId: string,
  active: ?boolean,
  employeeId?: string,
  withExpTypeName: ?boolean,
  startDate: ?string,
  endDate: ?string
) => (dispatch: Dispatch<any>) =>
  dispatch(
    withLoading(
      expReportTypeActions.list(
        companyId,
        active,
        employeeId,
        withExpTypeName,
        startDate,
        endDate
      )
    )
  );
