// @flow

import { withLoading } from '../../modules/commons/loading';

import { actions as accountingPeriodActions } from '../../modules/expense/entities/accountingPeriod';

/* eslint-disable import/prefer-default-export */
export const getAccountingPeriodList = (companyId: string) => (
  dispatch: Dispatch<any>
) => dispatch(withLoading(accountingPeriodActions.list(companyId)));
