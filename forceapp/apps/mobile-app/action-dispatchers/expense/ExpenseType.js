// @flow

import { withLoading } from '../../modules/commons/loading';
import { actions as expenseTypeListActions } from '../../modules/expense/entities/expenseTypeList';

export const getExpenseTypeList = (
  empId: ?string,
  parentGroupId: ?string,
  targetDate: ?string,
  recordType: ?string
) => (dispatch: Dispatch<any>) =>
  dispatch(
    withLoading(
      expenseTypeListActions.get(empId, parentGroupId, targetDate, recordType)
    )
  );

export const searchExpenseType = (
  companyId: string,
  name: ?string,
  targetDate: ?string
) => (dispatch: Dispatch<any>) =>
  dispatch(
    withLoading(expenseTypeListActions.search(companyId, name, targetDate))
  );

export const getJorudanExpenseType = (
  companyId: string,
  targetDate: string
) => (dispatch: Dispatch<any>) => {
  dispatch(
    withLoading(
      expenseTypeListActions.searchWithType(
        companyId,
        targetDate,
        'TransitJorudanJP'
      )
    )
  );
};
