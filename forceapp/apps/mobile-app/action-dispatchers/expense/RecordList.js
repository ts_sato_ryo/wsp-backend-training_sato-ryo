// @flow

import { withLoading } from '../../modules/commons/loading';
import { actions as recordListAction } from '../../modules/expense/entities/recordList';
import { actions as recordListSelectAction } from '../../modules/expense/ui/record/list/select';

export const initRecordList = (employeeId: string) => (
  dispatch: Dispatch<*>
) => {
  dispatch(withLoading(recordListAction.list(employeeId)));
};

export const initRecordListSelect = (
  isReselect: boolean = false,
  employeeId: string
) => (dispatch: Dispatch<*>) => {
  if (!isReselect) {
    dispatch(recordListSelectAction.clear());
  }
  dispatch(withLoading(recordListAction.list(employeeId)));
};

export const filterRecordList = (
  employeeId: string,
  expTypeList: Array<string>,
  startDate: string,
  endDate: string
) => (dispatch: Dispatch<*>) => {
  return dispatch(
    withLoading(
      recordListAction.list(employeeId, expTypeList, startDate, endDate)
    )
  );
};
