// @flow

import { startLoading, endLoading } from '../../modules/commons/loading';
import { showToast } from '../../../commons/modules/toast';
import FileUtil from '../../../commons/utils/FileUtil';
import type { Base64FileList } from '../../../domain/models/exp/Receipt';
import { actions as receiptLibraryAction } from '../../../domain/modules/exp/receiptLibrary/list';

export const getBase64files = (e: SyntheticInputEvent<HTMLInputElement>) => (
  dispatch: Dispatch<Base64FileList>
) => {
  const newFiles = e.currentTarget.files;
  const loadingId = dispatch(startLoading());
  const promise = Array.from(newFiles).map((file) =>
    FileUtil.getBase64(file).then((data) => ({
      name: file.name,
      data,
      size: file.size,
      type: file.type,
    }))
  );
  return Promise.all(promise).then((files) => {
    dispatch(endLoading(loadingId));
    return files;
  });
};

export const uploadReceipts = (base64FileList: Base64FileList) => async (
  dispatch: Dispatch<any>
) => {
  const { uploadReceipt, getDocumentId } = receiptLibraryAction;
  const loadingId = dispatch(startLoading());
  let res;
  try {
    const resUploadReceipt = await dispatch(uploadReceipt(base64FileList));
    const contentVersionId = resUploadReceipt.payload[0];
    const resGetDocumentId = await dispatch(getDocumentId([contentVersionId]));
    const contentDocumentId = resGetDocumentId.payload[0];
    res = { contentVersionId, contentDocumentId };
  } catch (err) {
    const errMsg =
      (err.message && ` (${err.message})`) ||
      (err.event && ` (${err.event.message})`) ||
      '';
    dispatch(showToast(errMsg));
  } finally {
    dispatch(endLoading(loadingId));
  }
  return res;
};
