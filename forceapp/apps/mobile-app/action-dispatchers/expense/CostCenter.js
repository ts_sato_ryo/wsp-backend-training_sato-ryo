// @flow

import { withLoading } from '../../modules/commons/loading';
import { actions as costCenterListActions } from '../../modules/expense/entities/costCenterList';
import { actions as costCenterAllListActions } from '../../modules/expense/entities/costCenterAllList';

export const getCostCenterList = (
  companyId: string,
  parentId?: ?string,
  targetDate: string
) => (dispatch: Dispatch<any>) =>
  dispatch(
    withLoading(costCenterListActions.get(companyId, parentId, targetDate))
  );

export const searchCostCenter = (companyId: string, targetDate: string) => (
  dispatch: Dispatch<any>
) =>
  dispatch(withLoading(costCenterAllListActions.search(companyId, targetDate)));
