// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'redux';

const mapDispatchToProps = <T>(dispatch: Dispatch<T>) => ({
  dispatch,
});

const withDispatch = <TProps>(
  WrappedComponent: React.ComponentType<TProps>
): React.ComponentType<*> => {
  return (connect(mapDispatchToProps)(
    WrappedComponent
  ): React.ComponentType<Object>);
};

export default withDispatch;
