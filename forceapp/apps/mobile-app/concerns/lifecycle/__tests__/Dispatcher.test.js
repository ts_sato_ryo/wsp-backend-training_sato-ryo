// @flow

import * as React from 'react';

import { render, cleanup } from 'react-testing-library';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import withDispatch from '../Dispatcher';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore({});

describe('withDispatch', () => {
  afterEach(cleanup);

  test('Pass `dispatch` to downstream', () => {
    const mock = jest.fn(() => <div />);
    const Component = withDispatch(mock);
    render(
      <Provider store={store}>
        <Component />
      </Provider>
    );

    const { dispatch } = mock.mock.calls[0][0];
    dispatch({
      type: 'TEST',
    });

    expect(dispatch).not.toBeNull();
    expect(store.getActions()).toEqual([{ type: 'TEST' }]);
  });
});
