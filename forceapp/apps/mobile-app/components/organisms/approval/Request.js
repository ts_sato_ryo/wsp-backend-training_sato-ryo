// @flow

import React from 'react';
import classNames from 'classnames';

import ViewItem from '../../molecules/commons/ViewItem';
// FIXME: Do't use PC's components.
import Comparison from '../../../../approvals-pc/components/DetailParts/Comparison';

import type { ArrayLabel } from '../../../../domain/models/approval/AttDailyDetail/Base';

import './Request.scss';

const ROOT = 'mobile-app-organisms-approval-request';

export type Props = {
  className?: string,
  detailList: ArrayLabel,
};

export default class Request extends React.PureComponent<Props> {
  render() {
    return (
      <div className={classNames(ROOT, this.props.className)}>
        {this.props.detailList.map((item, idx) => (
          <ViewItem label={item.label} key={idx}>
            {item.valueType ? (
              <Comparison
                new={item.value.toString()}
                old={
                  item.originalValue !== undefined &&
                  item.originalValue !== null
                    ? item.originalValue.toString()
                    : ''
                }
                type={item.valueType || 'date'}
              />
            ) : (
              item.value
            )}
          </ViewItem>
        ))}
      </div>
    );
  }
}
