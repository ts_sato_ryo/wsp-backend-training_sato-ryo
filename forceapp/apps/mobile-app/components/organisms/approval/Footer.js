// @flow

import React from 'react';

import msg from '../../../../commons/languages';

import Button from '../../atoms/Button';

import './Footer.scss';

const ROOT = 'mobile-app-organisms-approval-footer';

export type Props = {
  onClickApproveButton: () => void,
  onClickRejectButton: () => void,
};

export default (props: Props) => (
  <section className={ROOT}>
    <div className={`${ROOT}__item`}>
      <Button
        priority="primary"
        variant="alert"
        onClick={props.onClickRejectButton}
      >
        {msg().Appr_Btn_Reject}
      </Button>
    </div>
    <div className={`${ROOT}__item`}>
      <Button
        priority="primary"
        variant="neutral"
        onClick={props.onClickApproveButton}
      >
        {msg().Appr_Btn_Approve}
      </Button>
    </div>
  </section>
);
