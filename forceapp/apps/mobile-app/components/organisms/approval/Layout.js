// @flow

import { type Node } from 'react';

type Props = $ReadOnly<{|
  isShowingBody: boolean,
  children: Node,
|}>;

export default (props: Props) => {
  return props.isShowingBody ? props.children : null;
};
