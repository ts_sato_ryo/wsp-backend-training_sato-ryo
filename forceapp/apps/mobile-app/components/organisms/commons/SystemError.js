// @flow

import * as React from 'react';
import { get } from 'lodash';

import msg from '../../../../commons/languages';

import Icon from '../../atoms/Icon';

import colors from '../../../styles/variables/_colors.scss';

import PermissionError from './PermissionError';

import { type ErrorInfo } from '../../../../commons/utils/AppPermissionUtil';

import './SystemError.scss';

const ROOT = 'mobile-app-commons-system-error';

export type Props = $ReadOnly<{|
  message: string,
  solution?: string,
  continue: boolean,
  showError: boolean,
  hasPermissionError?: ErrorInfo | null,
  resetError: () => void,
|}>;

export default class SystemError extends React.PureComponent<Props> {
  componentDidUpdate() {
    if (this.props.continue) {
      window.alert(`${msg().Com_Lbl_Error}\n${this.props.message}`);
      this.props.resetError();
    }
  }

  render() {
    const RecoverableError = () => {
      // TODO
      // 1. Remove window.alert on componentDidUpdate.
      // 2. Render custom view for recoverable error.
      return null;
    };

    const fatalError =
      get(this.props, 'hasPermissionError.message') !== undefined ? (
        <PermissionError errorInfo={get(this.props, 'hasPermissionError')} />
      ) : (
        <div className={`${ROOT}__container`}>
          <div className={`${ROOT}__icon`}>
            <Icon
              className={`${ROOT}__error-icon`}
              type="error"
              color={colors.alert}
            />
          </div>
          <div className={`${ROOT}__message`}>
            <div className={`${ROOT}__message-body`}>{this.props.message}</div>
          </div>
          {this.props.solution && (
            <div className={`${ROOT}__solution`}>{this.props.solution}</div>
          )}
        </div>
      );

    return this.props.showError ? (
      <div className={ROOT}>
        {this.props.continue ? <RecoverableError /> : fatalError}
      </div>
    ) : null;
  }
}
