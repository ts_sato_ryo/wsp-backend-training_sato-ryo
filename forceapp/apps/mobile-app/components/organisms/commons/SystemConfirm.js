// @flow

export type Props = {|
  message: string,
  callback: (boolean) => void,
|};

export default (props: Props) => {
  const { message, callback } = props;
  if (message) {
    // eslint-disable-next-line no-alert
    callback(window.confirm(message));
  }

  return '';
};
