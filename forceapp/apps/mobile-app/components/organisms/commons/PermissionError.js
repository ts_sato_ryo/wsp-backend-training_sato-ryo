// @flow

import React from 'react';
import Icon from '../../atoms/Icon';
import { type ErrorInfo } from '../../../../commons/utils/AppPermissionUtil';
import colors from '../../../styles/variables/_colors.scss';
import './PermissionError.scss';

const ROOT = 'mobile-app-commons-error-page';

type Props = {
  errorInfo: ErrorInfo,
};

export default class PermissionError extends React.Component<Props> {
  render() {
    const {
      errorInfo: { message, description },
    } = this.props;

    return (
      <div className={ROOT}>
        <div className={`${ROOT}__container`}>
          <div className={`${ROOT}__icon`}>
            <Icon
              className={`${ROOT}__lock`}
              type="block"
              color={colors.blue600}
            />
          </div>
          <div className={`${ROOT}__message`}>
            <div className={`${ROOT}__message-body`}>{message}</div>
          </div>
          {description && (
            <div className={`${ROOT}__solution`}>{description}</div>
          )}
        </div>
      </div>
    );
  }
}
