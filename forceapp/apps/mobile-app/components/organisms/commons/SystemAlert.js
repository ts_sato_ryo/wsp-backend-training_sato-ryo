// @flow

export type Props = {|
  message: string,
  callback: () => void,
|};

export default (props: Props) => {
  const { message, callback } = props;
  if (message) {
    // eslint-disable-next-line no-alert
    window.alert(message);
    callback();
  }

  return '';
};
