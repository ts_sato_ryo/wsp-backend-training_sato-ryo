// @flow

import * as React from 'react';

import classNames from 'classnames';

import PermissionError from './PermissionError';
import { type ErrorInfo } from '../../../../commons/utils/AppPermissionUtil';

import './WrapperWithPermission.scss';

const ROOT = 'mobile-app-organisms-wrapper-with-permission';

export type Props = $ReadOnly<{
  className?: string,
  children: React.Node,
  hasPermissionError: ?ErrorInfo,
}>;

export default class WrapperWithPermission extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);
    const { hasPermissionError } = this.props;
    return (
      <div className={className}>
        {hasPermissionError ? (
          <PermissionError errorInfo={hasPermissionError} />
        ) : (
          this.props.children
        )}
      </div>
    );
  }
}
