// @flow

import React, { type Node } from 'react';

import LoadingPage from '../commons/LoadingPage';

type Props = $ReadOnly<{|
  isShowingBody: boolean,
  isShowing: boolean,
  children: Node,
|}>;

export default class Layout extends React.PureComponent<Props> {
  render() {
    return (
      <div>
        <LoadingPage isShowing={this.props.isShowing} />
        {this.props.isShowingBody && this.props.children}
      </div>
    );
  }
}
