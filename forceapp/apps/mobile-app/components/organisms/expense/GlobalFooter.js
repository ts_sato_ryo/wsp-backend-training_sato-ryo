// @flow

import React from 'react';

import msg from '../../../../commons/languages';
import { Tabs, Tab } from '../../molecules/commons/Tabs';

type Props = $ReadOnly<{|
  active?: 'record' | 'report',
  onClickRecordList: () => void,
  onClickReportList: () => void,
|}>;

export default (props: Props) => {
  const isActive = (type: string) => {
    return props.active === type;
  };

  return (
    <Tabs position="bottom">
      <Tab
        label={msg().Exp_Lbl_RecordList}
        onClick={props.onClickRecordList}
        active={isActive('record')}
      />
      <Tab
        label={msg().Exp_Lbl_ReportList}
        onClick={props.onClickReportList}
        active={isActive('report')}
      />
    </Tabs>
  );
};
