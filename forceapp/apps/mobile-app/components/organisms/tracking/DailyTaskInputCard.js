// @flow

import * as React from 'react';

import msg from '../../../../commons/languages';
import TimeUtil from '../../../../commons/utils/TimeUtil';

import Card from '../../atoms/Card';
import Chip from '../../atoms/Chip';
import IconButton from '../../atoms/IconButton';
import TaskTimeSelect from '../../molecules/tracking/TaskTimeSelect';
import RatioField from '../../molecules/tracking/RatioField';
import InputModeButton from '../../molecules/tracking/InputModeButton';

import './DailyTaskInputCard.scss';

const ROOT = 'mobile-app-organisms-daily-task-input-card';

type Props = $ReadOnly<{|
  listEditing: boolean,
  testId?: string,
  code: string,
  job: string,
  isDefaultJob: boolean,
  defaultJobExists: boolean,
  multipleRatiosExist: boolean,
  realWorkTimeExists: boolean,
  workCategoryName: string,
  ratio: number,
  taskTime: number,
  timeOrRatioValue: 'time' | 'ratio',
  onChangeTaskTime: (value: number) => void,
  onChangeRatio: (value: number) => void,
  onClickInputModeButton: (value: 'time' | 'ratio') => void,
  onClickDeleteButton: () => void,
|}>;

export default class DailyTaskInputCard extends React.Component<Props> {
  render() {
    const { listEditing } = this.props;
    const testId = (prefix: string): typeof undefined | string => {
      return this.props.testId ? `${this.props.testId}__${prefix}` : undefined;
    };

    return (
      <Card className={ROOT}>
        <div className={`${ROOT}__left`}>
          {this.props.isDefaultJob ? (
            <Chip
              className={`${ROOT}__default-job`}
              text={msg().Com_Lbl_DefaultJobShort}
            />
          ) : null}

          <div className={`${ROOT}__heading heading-3`}>
            <span className={`${ROOT}__code`}>{this.props.code}</span>
            <span className={`${ROOT}__break`}>/</span>
            <span className={`${ROOT}__job`}>{this.props.job}</span>
          </div>
          <div className={`${ROOT}__work-category`}>
            {this.props.workCategoryName}
          </div>
        </div>

        <div className={`${ROOT}__right horizontal`}>
          <div className={`${ROOT}__left`}>
            {this.props.timeOrRatioValue === 'time' ? (
              <TaskTimeSelect
                testId={testId('att-time')}
                onChange={(value) =>
                  this.props.onChangeTaskTime(TimeUtil.toMinutes(value) || 0)
                }
                className={`${ROOT}__input`}
                placeholder="00:00"
                value={TimeUtil.toHHmm(this.props.taskTime)}
                readOnly={this.props.listEditing}
              />
            ) : null}
            {this.props.timeOrRatioValue === 'ratio' ? (
              <React.Fragment>
                <RatioField
                  testId={testId('att-time')}
                  onBlur={(value) =>
                    value !== this.props.ratio &&
                    this.props.onChangeRatio(value)
                  }
                  className={`${ROOT}__input`}
                  placeholder="0"
                  readOnly={
                    this.props.isDefaultJob ||
                    !this.props.multipleRatiosExist ||
                    this.props.listEditing
                  }
                  value={this.props.ratio}
                />
                <div className={`${ROOT}__calculated-task-time`}>
                  {this.props.realWorkTimeExists
                    ? TimeUtil.toHHmm(this.props.taskTime)
                    : '* *'}
                </div>
              </React.Fragment>
            ) : null}
          </div>
          <div className={`${ROOT}__right`}>
            <InputModeButton
              testId={testId('input-mode-button')}
              value={this.props.timeOrRatioValue}
              onClick={this.props.onClickInputModeButton}
              disabled={
                this.props.isDefaultJob ||
                this.props.defaultJobExists ||
                this.props.listEditing
              }
            />
          </div>
          {listEditing ? (
            <div className={`${ROOT}__additional`}>
              <IconButton
                icon="delete-copy"
                size="large"
                onClick={this.props.onClickDeleteButton}
                disabled={this.props.isDefaultJob}
              />
            </div>
          ) : null}
        </div>
      </Card>
    );
  }
}
