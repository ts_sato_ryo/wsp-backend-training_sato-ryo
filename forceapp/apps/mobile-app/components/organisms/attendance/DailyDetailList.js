// @flow
import * as React from 'react';
import classNames from 'classnames';

import type { AttDailyRecordContractedDetail } from '../../../../domain/models/attendance/AttDailyRecord';
import type { RestTimes } from '../../../../domain/models/attendance/RestTime';

import msg from '../../../../commons/languages';

import TimeUtil from '../../../../commons/utils/TimeUtil';

import TextAreaField from '../../molecules/commons/Fields/TextAreaField';
import AttTimeList from './AttTimeList';

import './DailyDetailList.scss';

const ROOT = 'mobile-app-components-organisms-attendance-daily-detail-list';

type Props = $ReadOnly<{|
  className?: string,
  readOnly: boolean,
  startTime: number | null,
  endTime: number | null,
  restTimes: RestTimes,
  remarks: string,
  otherRestTime: number | null,
  contractedDetail: AttDailyRecordContractedDetail,
  isShowOtherRestTime: boolean,
  minRestTimesCount?: number,
  maxRestTimesCount?: number,
  onChangeStartTime: (number | null) => void,
  onChangeEndTime: (number | null) => void,
  onChangeRestTimeStartTime: (number, number | null, number | null) => void,
  onChangeRestTimeEndTime: (number, number | null, number | null) => void,
  onClickRemoveRestTime: (number) => void,
  onClickAddRestTime: () => void,
  onChangeOtherRestTime: (number | null) => void,
  onChangeRemarks: (string) => void,
|}>;

export default class DailyDetailList extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);
    const {
      readOnly,
      startTime,
      endTime,
      restTimes,
      otherRestTime,
      remarks,
      contractedDetail,
      isShowOtherRestTime,
      minRestTimesCount,
      maxRestTimesCount,
      onChangeStartTime,
      onChangeEndTime,
      onChangeRestTimeStartTime,
      onChangeRestTimeEndTime,
      onClickRemoveRestTime,
      onClickAddRestTime,
      onChangeOtherRestTime,
      onChangeRemarks,
    } = this.props;

    return (
      <div className={className}>
        <AttTimeList
          readOnly={readOnly}
          workingTime={{
            from: {
              placeholder: !readOnly
                ? `(${TimeUtil.toHHmm(contractedDetail.startTime)})`
                : undefined,
              defaultValue: contractedDetail.startTime,
              value: startTime,
              onChangeValue: (from, _to) => {
                if (onChangeStartTime) {
                  onChangeStartTime(from);
                }
              },
            },
            to: {
              placeholder: !readOnly
                ? `(${TimeUtil.toHHmm(contractedDetail.endTime)})`
                : undefined,
              defaultValue: contractedDetail.endTime,
              value: endTime,
              onChangeValue: (_from, to) => {
                if (onChangeEndTime) {
                  onChangeEndTime(to);
                }
              },
            },
          }}
          restTimes={{
            placeholder: !readOnly
              ? (contractedDetail.restTimes.map((o) => ({
                  startTime: `(${TimeUtil.toHHmm(o.startTime)})`,
                  endTime: `(${TimeUtil.toHHmm(o.endTime)})`,
                })): {
                  startTime: string,
                  endTime: string,
                }[])
              : undefined,
            defaultValue: (contractedDetail.restTimes.map((o) => ({
              startTime: o.startTime,
              endTime: o.endTime,
            })): { startTime: number | null, endTime: number | null }[]),
            value: restTimes,
            min: minRestTimesCount,
            max: maxRestTimesCount,
            onChangeValueStartTime: onChangeRestTimeStartTime,
            onChangeValueEndTime: onChangeRestTimeEndTime,
            onClickRemove: onClickRemoveRestTime,
            onClickAdd: onClickAddRestTime,
          }}
          otherRestTime={
            isShowOtherRestTime
              ? {
                  value: otherRestTime,
                  onChange: onChangeOtherRestTime,
                }
              : undefined
          }
        />
        <div className={`${ROOT}__item`}>
          <TextAreaField
            className={`${ROOT}__remarks`}
            label={msg().Att_Lbl_Remarks}
            rows={3}
            value={remarks}
            onChange={(event: SyntheticEvent<HTMLTextAreaElement>) =>
              onChangeRemarks(event.currentTarget.value)
            }
            readOnly={readOnly}
          />
        </div>
      </div>
    );
  }
}
