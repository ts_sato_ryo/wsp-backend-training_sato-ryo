/* @flow */
import * as React from 'react';
import classNames from 'classnames';
import isNil from 'lodash/isNil';

import {
  type LocationFetchStatus,
  LOCATION_FETCH_STATUS,
} from '../../../../domain/models/Location';

import msg from '../../../../commons/languages';

import TextAreaField from '../../molecules/commons/Fields/TextAreaField';
import Map from '../../molecules/commons/Map';
import StampClock from '../../molecules/attendance/TimeStamp/StampClock';
import SendLocationToggle from '../../molecules/attendance/TimeStamp/SendLocationToggle';
import StampButtons from '../../molecules/attendance/TimeStamp/StampButtons';
import './TimeStamp.scss';

type Props = {|
  className?: string,
  timeLocale: string,
  onClickStartStampButton: () => void,
  onClickEndStampButton: () => void,
  isEnableStartStamp: boolean,
  isEnableEndStamp: boolean,
  isEnableRestartStamp: boolean,
  defaultAction: 'in' | 'out' | 'rein',

  showLocationToggleButton: boolean,
  onClickToggleButton: (boolean) => void,
  willSendLocation: boolean,
  fetchStatus: LocationFetchStatus,
  locationFetchTime: number | null,
  latitude: number | null,
  longitude: number | null,

  message: string,
  onChangeMessageField: (message: string) => void,
|};

const ROOT = 'mobile-app-components-organisms-attendance-time-stamp';

export default class TimeStamp extends React.PureComponent<Props> {
  render() {
    const {
      className: $className,
      isEnableStartStamp,
      isEnableEndStamp,
      isEnableRestartStamp,
      showLocationToggleButton,
      willSendLocation,
      fetchStatus,
      timeLocale,
      latitude,
      longitude,
      locationFetchTime,
      message,
      defaultAction,
      onChangeMessageField,
      onClickToggleButton,
      onClickEndStampButton,
      onClickStartStampButton,
    } = this.props;
    const className = classNames(ROOT, $className);
    const canStamp =
      isEnableStartStamp || isEnableEndStamp || isEnableRestartStamp;
    const isMessageRequired =
      showLocationToggleButton &&
      (!willSendLocation || fetchStatus !== LOCATION_FETCH_STATUS.Success);
    const isShowMap =
      showLocationToggleButton &&
      willSendLocation &&
      !isNil(latitude) &&
      !isNil(longitude);

    return (
      <div className={className}>
        <StampClock className={`${ROOT}__stamp-clock`} locale={timeLocale} />
        {showLocationToggleButton ? (
          <SendLocationToggle
            className={`${ROOT}__send-location-toggle`}
            willSendLocation={willSendLocation}
            onClick={onClickToggleButton}
            fetchStatus={fetchStatus}
            locationFetchTime={locationFetchTime}
          />
        ) : null}
        {isShowMap && (
          <Map
            className={`${ROOT}__map`}
            latitude={latitude || 0}
            longitude={longitude || 0}
          />
        )}
        <TextAreaField
          className={`${ROOT}__message-field`}
          label={msg().Att_Lbl_StampMessage}
          errors={
            canStamp && isMessageRequired && !message
              ? [msg().Att_Err_RequireCommentWithoutLocation]
              : undefined
          }
          disabled={!canStamp}
          required={isMessageRequired}
          onChange={(e: SyntheticEvent<HTMLTextAreaElement>) => {
            onChangeMessageField(e.currentTarget.value);
          }}
          value={message}
        />
        <StampButtons
          className={`${ROOT}__stamp-buttons`}
          onClickStartStampButton={onClickStartStampButton}
          onClickEndStampButton={onClickEndStampButton}
          isEnableStartStamp={isEnableStartStamp}
          isEnableEndStamp={isEnableEndStamp}
          isEnableRestartStamp={isEnableRestartStamp}
          defaultAction={defaultAction}
          disabled={isMessageRequired && !message}
        />
      </div>
    );
  }
}
