// @flow

import * as React from 'react';

const Navigation = (props: *) => <div key="nav">{props.children}</div>;

export default Navigation;
