// @flow

import * as React from 'react';
import classNames from 'classnames';

import DateUtil from '../../../../commons/utils/DateUtil';

import { type LeaveRequest } from '../../../../domain/models/attendance/AttDailyRequest/LeaveRequest';
import { type Status } from '../../../../domain/models/approval/request/Status';
import { LEAVE_TYPE } from '../../../../domain/models/attendance/LeaveType';
import { DAY_TYPE } from '../../../../domain/models/attendance/AttDailyRecord';
import { LEAVE_RANGE } from '../../../../domain/models/attendance/LeaveRange';

import LinkListItem from '../../atoms/LinkListItem';
import ApprovalStatus from '../commons/ApprovalStatus';
import AlertIcon from '../commons/AlertIcon';

import './MonthlyListItem.scss';

const ROOT = 'mobile-app-molecules-attendance-monthly-list-item';

type Props = $ReadOnly<{|
  className?: string,
  onClick?: (SyntheticEvent<Element>) => void,
  dayType: string,
  date: string,
  startTime?: string,
  endTime?: string,
  workingTypeStartTime?: string,
  workingTypeEndTime?: string,
  requestStatus: Status | null,
  leaveRequests: LeaveRequest[],
  isLeaveOfAbsence: boolean,
  hasAbsenceRequest: boolean,
  attentionMessages: string[],
|}>;

export default class MonthlyListItem extends React.PureComponent<Props> {
  getDayStatus() {
    const { leaveRequests } = this.props;

    // 休職・休業
    if (this.props.isLeaveOfAbsence) {
      return `all-absent`;
    }
    // 欠勤申請済
    if (this.props.hasAbsenceRequest) {
      // 有給の半日休・時間単位休申請後に欠勤があった場合
      if (leaveRequests.length > 0) {
        const paidPMLeave = leaveRequests.find(
          (request) =>
            (request.leaveType === LEAVE_TYPE.Annual ||
              request.leaveType === LEAVE_TYPE.Paid) &&
            request.leaveRange === LEAVE_RANGE.PM
        );
        const paidLeave = leaveRequests.find(
          (request) =>
            request.leaveType === LEAVE_TYPE.Annual ||
            request.leaveType === LEAVE_TYPE.Paid
        );
        if (paidPMLeave || paidLeave) {
          return paidPMLeave
            ? `leave-am-unpaid-pm-paid`
            : `leave-am-paid-pm-unpaid`;
        }
      }
      return `absent`;
    }
    // 所定休日・法定休日
    if (this.props.dayType !== DAY_TYPE.Workday) {
      return `${this.props.dayType.toLowerCase()}`;
    }
    // 休暇申請 (優先度: 全日休 > 午前半休 = 午後半休 > 半休 > 時間休)
    if (this.props.leaveRequests.length > 0) {
      // 年次有給休暇 = 有給, 代休 = 無給 と同等に扱う
      const actualLeaveRequests = leaveRequests.map((request) => {
        request.leaveType =
          request.leaveType === LEAVE_TYPE.Annual ||
          request.leaveType === LEAVE_TYPE.Paid
            ? LEAVE_TYPE.Paid
            : LEAVE_TYPE.Unpaid;
        return request;
      });

      // 全日休(同種別の半日休x2を含む)を表示
      const hasAllDayLeave =
        // 全日休がある
        actualLeaveRequests.some(
          (request) => request.leaveRange === LEAVE_RANGE.Day
        ) ||
        // 同種別の午前半休&午後半休がある
        (actualLeaveRequests.length === 2 &&
          actualLeaveRequests[0].leaveType ===
            actualLeaveRequests[1].leaveType &&
          ((actualLeaveRequests[0].leaveRange === LEAVE_RANGE.AM &&
            actualLeaveRequests[1].leaveRange === LEAVE_RANGE.PM) ||
            (actualLeaveRequests[0].leaveRange === LEAVE_RANGE.PM &&
              actualLeaveRequests[1].leaveRange === LEAVE_RANGE.AM)));
      if (hasAllDayLeave) {
        return `leave-day-${(
          actualLeaveRequests[0].leaveType || ''
        ).toLowerCase()}`;
      }

      // 種別にかかわらず、半日休x2がある
      if (
        actualLeaveRequests.length === 2 &&
        (actualLeaveRequests[0].leaveRange === LEAVE_RANGE.Half &&
          actualLeaveRequests[1].leaveRange === LEAVE_RANGE.Half)
      ) {
        return `leave-day-${(
          actualLeaveRequests[0].leaveType || ''
        ).toLowerCase()}`;
      }

      // 休暇が1つのみであればそれを表示(半休・時間単位休は午前半休と同じ)
      if (actualLeaveRequests.length === 1) {
        const leaveRangeName =
          actualLeaveRequests[0].leaveRange === LEAVE_RANGE.PM ? 'pm' : 'am';
        return `leave-${leaveRangeName}-${(
          actualLeaveRequests[0].leaveType || ''
        ).toLowerCase()}`;
      }

      // 午前と午後が両方あるが種別が異なる場合
      const amLeave = actualLeaveRequests.find(
        (request) => request.leaveRange === LEAVE_RANGE.AM
      );
      const pmLeave = actualLeaveRequests.find(
        (request) => request.leaveRange === LEAVE_RANGE.PM
      );
      if (amLeave && pmLeave) {
        return `leave-am-${(amLeave.leaveType || '').toLowerCase()}-pm-${(
          pmLeave.leaveType || ''
        ).toLowerCase()}`;
      }
      // 午前半休と午後半休のいずれかがあれば、優先的に表示
      if (amLeave || pmLeave) {
        const majorLeave = amLeave || pmLeave;
        return `leave-am-${
          majorLeave ? (majorLeave.leaveType || '').toLowerCase() : ''
        }`;
      }

      // 半休があれば、優先的に表示(午前半休と同じ表示とする)
      const halfLeave = actualLeaveRequests.find(
        (request) => request.leaveRange === LEAVE_RANGE.Half
      );
      if (halfLeave) {
        return `leave-am-${(halfLeave.leaveType || '').toLowerCase()}`;
      }

      // 時間単位休を1つだけ表示(午前半休と同じ表示とする)
      return `leave-am-${(
        actualLeaveRequests[0].leaveType || ''
      ).toLowerCase()}`;
    }
    return 'workday';
  }

  render() {
    const modifier: string = this.getDayStatus();
    const className = classNames(
      ROOT,
      this.props.className,
      `${ROOT}--${modifier}`
    );

    return (
      <div className={className}>
        <LinkListItem
          className={`${ROOT}__link-list-item`}
          onClick={this.props.onClick}
        >
          <div className={`${ROOT}__status`}>
            <div className={`${ROOT}__status-top`}>
              <ApprovalStatus
                size="medium"
                status={this.props.requestStatus || undefined}
                className={`${ROOT}__request-approval-status`}
                iconOnly
              />
              {this.props.attentionMessages.length >= 1 && (
                <AlertIcon variant="attention" />
              )}
            </div>
          </div>
          <div className={`${ROOT}__date`}>
            <div className={`${ROOT}__month-day`}>
              {DateUtil.formatMD(this.props.date)}
            </div>
            <div className={`${ROOT}__weekday`}>
              {DateUtil.formatW(this.props.date)}
            </div>
          </div>
          <div
            className={classNames(`${ROOT}__startTime`, {
              [`${ROOT}__startTime--placeholder`]: !this.props.startTime,
            })}
          >
            {this.props.startTime ||
              (this.props.workingTypeStartTime &&
                `(${this.props.workingTypeStartTime})`) ||
              '-'}
          </div>
          <div
            className={classNames(`${ROOT}__endTime`, {
              [`${ROOT}__endTime--placeholder`]: !this.props.endTime,
            })}
          >
            {this.props.endTime ||
              (this.props.workingTypeEndTime &&
                `(${this.props.workingTypeEndTime})`) ||
              '-'}
          </div>
        </LinkListItem>
      </div>
    );
  }
}
