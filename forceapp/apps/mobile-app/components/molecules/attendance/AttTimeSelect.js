// @flow

import * as React from 'react';

import TimeSelect from '../commons/Fields/TimeSelect';

import msg from '../../../../commons/languages';
import TimeUtil from '../../../../commons/utils/TimeUtil';
import { parseIntOrNull } from '../../../../commons/utils/NumberUtil';

export type Props = $ReadOnly<{|
  className?: string,
  value?: number | null,
  defaultValue?: number | null,
  placeholder?: string,
  disabled?: boolean,
  readOnly?: boolean,
  error?: boolean,
  icon?: boolean,
  testId?: string,
  onChange?: (number | null) => void,
|}>;

export default (props: Props) => {
  const { value, defaultValue, onChange, ...timeSelectProps } = props;

  return (
    <TimeSelect
      {...timeSelectProps}
      value={TimeUtil.toHHmm(value)}
      defaultValue={TimeUtil.toHHmm(defaultValue)}
      onChange={(v: string) =>
        onChange && onChange(parseIntOrNull(TimeUtil.toMinutes(v)))
      }
      maxValue="47:59"
      formatHour={(v: number) =>
        `${String(v).padStart(2, '0')}${msg().Com_Lbl_UnitHours}`
      }
      formatMinute={(v: number) =>
        `${String(v).padStart(2, '0')}${msg().Com_Lbl_UnitMins}`
      }
      useTitle
    />
  );
};
