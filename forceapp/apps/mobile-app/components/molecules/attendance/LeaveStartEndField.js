// @flow

import * as React from 'react';

import { LEAVE_RANGE } from '../../../../domain/models/attendance/LeaveRange';
import { type LeaveRequest } from '../../../../domain/models/attendance/AttDailyRequest/LeaveRequest';

import msg from '../../../../commons/languages';
import DateUtil from '../../../../commons/utils/DateUtil';
import AttTimeSelectRangeField from './AttTimeSelectRangeField';
import DateRangeField from '../commons/Fields/DateRangeField';

import './LeaveStartEndField.scss';

const ROOT = 'mobile-app-molecules-attendance-leave-start-end-field';

export type Props = {|
  testId?: string,
  request: LeaveRequest,
  isDaysLeftManaged: boolean,
  onChange: (
    key: $Keys<LeaveRequest>,
    value: $Values<LeaveRequest> | null
  ) => void,
  readOnly?: boolean,
  required?: boolean,
  errors?: string[],
|};

export default class LeaveStartEndField extends React.Component<Props> {
  render() {
    switch (this.props.request.leaveRange) {
      case LEAVE_RANGE.Day:
        return (
          <div>
            <DateRangeField
              testId={`${this.props.testId || ROOT}__date-range-field`}
              label={msg().Att_Lbl_Period}
              required={this.props.required}
              start={{
                readOnly: this.props.readOnly,
                disabled: !this.props.readOnly,
                value: this.props.request.startDate,
                errors: this.props.errors,
                onChange: (_e: SyntheticInputEvent<HTMLElement>, { date }) =>
                  this.props.onChange('startDate', DateUtil.fromDate(date)),
              }}
              end={{
                readOnly: this.props.readOnly,
                value: this.props.request.endDate,
                errors: this.props.errors,
                onChange: (_e: SyntheticInputEvent<HTMLElement>, { date }) =>
                  this.props.onChange('endDate', DateUtil.fromDate(date)),
              }}
            />
          </div>
        );
      case LEAVE_RANGE.Half:
        return null;
      case LEAVE_RANGE.Time:
        return (
          <div>
            <AttTimeSelectRangeField
              required={this.props.required}
              readOnly={this.props.readOnly}
              testId={`${this.props.testId || ROOT}__att-time-range-field`}
              placeholder="(00:00)"
              errors={this.props.errors}
              from={{
                label: msg().Att_Lbl_StartTime,
                value: this.props.request.startTime,
                onChangeValue: (startTime, _endTime) =>
                  this.props.onChange('startTime', startTime),
              }}
              to={{
                label: msg().Att_Lbl_EndTime,
                value: this.props.request.endTime,
                onChangeValue: (_startTiem, endTime) =>
                  this.props.onChange('endTime', endTime),
              }}
            />
            {this.props.isDaysLeftManaged && (
              <p className={`${ROOT}__rounding-up-notice`}>
                {msg().Att_Msg_RoundingUpNotice}
              </p>
            )}
          </div>
        );
      default:
        return null;
    }
  }
}
