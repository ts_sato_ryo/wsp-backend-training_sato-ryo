/* @flow */
import * as React from 'react';
import classNames from 'classnames';

import msg from '../../../../../commons/languages';

import Button from '../../../atoms/Button';

import './StampButtons.scss';

type Props = {
  className?: string,
  onClickStartStampButton: () => void,
  onClickEndStampButton: () => void,
  isEnableStartStamp: boolean,
  isEnableEndStamp: boolean,
  isEnableRestartStamp: boolean,
  disabled?: boolean,
  defaultAction: 'in' | 'out' | 'rein',
};

const ROOT =
  'mobile-app-components-molecules-attendance-time-stamp-stamp-buttons';

export default class StampButtons extends React.Component<Props> {
  renderStartStampButton() {
    return (
      <Button
        testId={`${ROOT}__stamp_button`}
        className={`${ROOT}__stamp_button`}
        variant="neutral"
        priority={this.props.defaultAction !== 'out' ? 'primary' : 'secondary'}
        disabled={
          this.props.disabled ||
          !(this.props.isEnableStartStamp || this.props.isEnableRestartStamp)
        }
        onClick={this.props.onClickStartStampButton}
      >
        {this.props.isEnableRestartStamp
          ? msg().Com_Btn_ClockRein
          : msg().Com_Btn_ClockIn}
      </Button>
    );
  }

  renderEndStampButton() {
    return (
      <Button
        testId={`${ROOT}__stamp_button`}
        className={`${ROOT}__stamp_button`}
        variant="neutral"
        priority={this.props.defaultAction === 'out' ? 'primary' : 'secondary'}
        disabled={this.props.disabled || !this.props.isEnableEndStamp}
        onClick={this.props.onClickEndStampButton}
      >
        {msg().Com_Btn_ClockOut}
      </Button>
    );
  }

  render() {
    const className = classNames(ROOT, this.props.className);

    return (
      <div className={className}>
        {this.renderStartStampButton()}
        {this.renderEndStampButton()}
      </div>
    );
  }
}
