// @flow

import * as React from 'react';
import Autosuggest from 'react-autosuggest';

import Errors from '../../atoms/Errors';
import IconButton from '../../atoms/IconButton';
import Label from '../../atoms/Label';
import msg from '../../../../commons/languages';

// Prop types
import { type InputProps } from '../../atoms/Fields/InputProps';
import {
  type Suggestions,
  type SuggestionsItem,
  type StationInfo,
} from '../../../../domain/models/exp/jorudan/Station';

// images
import ImgIconTransportationBus from '../../../../commons/images/iconTransportationBus.png';
import ImgIconTransportationTrain from '../../../../commons/images/iconTransportationTrain.png';
import ImgIconTransportationShip from '../../../../commons/images/iconTransportationShip.png';

import './SearchStationField.scss';

const ROOT = 'mobile-app-molecules-search-station-field';

type RouteOriginArrivalValues = {
  category: string,
  company: string,
  name: string,
};

type Props = $ReadOnly<{|
  emphasis?: boolean,
  errors?: string[],
  label: string,
  testId?: string,
  disabled?: boolean,
  initialValue: RouteOriginArrivalValues,
  searchRoute: (value: string) => any,
  setFormikFieldValue: (value: any) => void,
  ...InputProps,
|}>;

type State = {
  suggestions: Suggestions,
  error: string,
  value: string,
};

export default class SearchStationField extends React.PureComponent<
  Props,
  State
> {
  input: any;
  focus: () => void;
  getSuggestionValue: (StationInfo) => void;
  onClickSearchStationButton: () => void;
  onChangeInput: (e: SyntheticInputEvent<HTMLInputElement>, {}) => void;
  onSuggestionSelected: (
    e: SyntheticInputEvent<HTMLInputElement>,
    { method: string }
  ) => void;

  renderSearchButton: () => void;
  renderSectionTitle: (StationInfo) => void;
  renderSuggestion: (StationInfo) => void;
  storeInputReference: (any) => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      suggestions: [],
      value: '',
      error: '',
    };

    this.input = null;
  }

  onChangeInput = (
    e: SyntheticInputEvent<HTMLInputElement>,
    { newValue }: { newValue: string }
  ) => {
    if (newValue !== this.state.value) {
      this.setState({
        value: newValue,
      });
      this.props.setFormikFieldValue({
        category: '',
        company: '',
        name: newValue,
      });
    }
  };

  onClickSearchStationButton = (value: string) => {
    if (value.length > 0) {
      this.props.searchRoute(value).then((result: Suggestions) => {
        this.setState({
          suggestions: result,
          error: result.length === 0 ? msg().Cmn_Lbl_SuggestNoResult : '',
        });
        this.focus();
      });
    }
  };

  onSuggestionSelected = (
    e: SyntheticInputEvent<HTMLInputElement>,
    { suggestion }: { suggestion: StationInfo }
  ) => {
    this.props.setFormikFieldValue(suggestion);
  };

  focus = () => {
    if (!this.input) {
      return;
    }
    this.input.focus();
  };

  getSectionSuggestions = (section: SuggestionsItem) => {
    return section.suggestions;
  };

  getSuggestionValue = (suggestionItem: StationInfo) => {
    return suggestionItem.name;
  };

  storeInputReference = (autosuggest: any) => {
    if (autosuggest) {
      this.input = autosuggest.input;
    }
  };

  renderSectionTitle = (section: StationInfo) => {
    if (
      section.category === 'B' ||
      section.category === 'H' ||
      section.category === 'P'
    ) {
      return <img src={ImgIconTransportationBus} alt="バス" />;
    } else if (section.category === 'R') {
      return <img src={ImgIconTransportationTrain} alt="電車" />;
    } else if (section.category === 'F') {
      return <img src={ImgIconTransportationShip} alt="船" />;
    }
    return null;
  };

  renderSuggestion = (suggestionItem: StationInfo) => {
    return (
      <p>
        {suggestionItem.name}
        {suggestionItem.company !== '-' ? `[${suggestionItem.company}]` : ''}
      </p>
    );
  };

  render() {
    const { value } = this.state;
    const errors = this.props.errors || [];
    const hasErrorClass =
      errors.length > 0 ? 'mobile-app-atoms-input--error' : '';
    const hasDisabledClass = this.props.disabled
      ? 'mobile-app-atoms-input--disabled'
      : '';

    const inputProps = {
      value: (this.props.initialValue && this.props.initialValue.name) || value,
      placeholder: msg().Exp_Lbl_RoutePlaceholder,
      onChange: this.onChangeInput,
      disabled: this.props.disabled,
    };

    const suggestTheme = {
      container: `${ROOT}__suggest mobile-app-atoms-input ${hasErrorClass} ${hasDisabledClass}`,
      input: 'mobile-app-atoms-input__input',
      suggestionsContainerOpen: 'ts-suggest-result',
      sectionContainer: 'ts-suggest-result__container',
      suggestionsList: 'ts-suggest-result__list',
      sectionTitle: 'ts-suggest-result__title',
      suggestion: 'ts-suggest-result__value',
      suggestionHighlighted: 'ts-suggest-result__value-highlight',
    };

    return (
      <div className={ROOT}>
        <Label
          emphasis
          marked={this.props.required}
          className={`${ROOT}__label`}
          text={this.props.label}
        >
          <Autosuggest
            getSectionSuggestions={this.getSectionSuggestions}
            getSuggestionValue={this.getSuggestionValue}
            inputProps={inputProps}
            multiSection
            onSuggestionsClearRequested={() => {}}
            onSuggestionSelected={this.onSuggestionSelected}
            onSuggestionsFetchRequested={() => {}}
            ref={this.storeInputReference}
            renderSectionTitle={this.renderSectionTitle}
            renderSuggestion={this.renderSuggestion}
            suggestions={this.state.suggestions}
            theme={suggestTheme}
          />
          <IconButton
            icon="search"
            onClick={() => this.onClickSearchStationButton(inputProps.value)}
          />
        </Label>
        {this.state.error && <Errors messages={[this.state.error]} />}
        {!this.state.error && this.props.errors && (
          <Errors messages={this.props.errors} />
        )}
      </div>
    );
  }
}
