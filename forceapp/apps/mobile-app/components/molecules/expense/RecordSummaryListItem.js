// @flow

import * as React from 'react';
import classNames from 'classnames';

import LinkListItem from '../../atoms/LinkListItem';
import RecordSummary, {
  type Props as RecordSummaryProps,
} from './RecordSummary';

const ROOT = 'mobile-app-molecules-expense-record-summary-list-item';

export type Props = $ReadOnly<{
  className?: string,
  onClick: () => void,
  ...RecordSummaryProps,
}>;

export default class RecordSummaryListItem extends React.PureComponent<Props> {
  render() {
    const { className: _className, onClick, ...props } = this.props;
    const className = classNames(ROOT, _className);

    return (
      <LinkListItem className={className} onClick={onClick}>
        <RecordSummary {...props} />
      </LinkListItem>
    );
  }
}
