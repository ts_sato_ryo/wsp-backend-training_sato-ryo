// @flow

import * as React from 'react';
import classNames from 'classnames';

import msg from '../../../../commons/languages';

import { PENDING } from '../../../../commons/constants/requestStatus';

import { type Report } from '../../../../domain/models/exp/Report';

import DateUtil from '../../../../commons/utils/DateUtil';

import Amount from '../../atoms/Amount';
import Icon from '../../atoms/Icon';
import ApprovalStatus from '../commons/ApprovalStatus';
import STATUS from '../../../../domain/models/approval/request/Status';

import './ReportSummary.scss';

const ROOT = 'mobile-app-molecules-expense-report-summary';

export type Props = $ReadOnly<{|
  className?: string,
  report: Report,
  decimalPlaces: number,
  symbol: string,
|}>;

export default class ReportSummary extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);
    const { report, decimalPlaces, symbol } = this.props;

    return (
      <div className={className}>
        <div className={`${ROOT}__subject`}>{report.subject}</div>
        <div className={`${ROOT}__content`}>
          {report.status && (
            <ApprovalStatus
              status={
                report.status === STATUS.Canceled
                  ? STATUS.Rejected
                  : report.status
              } // overwriting is temporal. BE should change 'accounting rejected' status.
              className={`${ROOT}__status`}
            />
          )}
        </div>
        <div className={`${ROOT}__bottom`}>
          <div className={`${ROOT}__bottom__date`}>
            <Icon
              className={`${ROOT}__bottom__date-icon`}
              type="monthlyview"
              size="small"
            />
            <div className={`${ROOT}__bottom__date-title`}>
              {report.status === PENDING
                ? msg().Appr_Lbl_DateSubmitted
                : msg().Exp_Lbl_ReportDate}
              :
            </div>
            {DateUtil.formatYMD(report.requestDate || report.accountingDate)}
          </div>
          <Amount
            className={`${ROOT}__bottom__amount`}
            amount={report.totalAmount}
            decimalPlaces={decimalPlaces}
            symbol={symbol}
          />
        </div>
      </div>
    );
  }
}
