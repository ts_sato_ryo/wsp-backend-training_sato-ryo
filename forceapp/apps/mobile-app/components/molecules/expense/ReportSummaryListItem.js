// @flow

import * as React from 'react';
import classNames from 'classnames';

import LinkListItem from '../../atoms/LinkListItem';
import ReportSummary, {
  type Props as ReportSummaryProps,
} from './ReportSummary';

const ROOT = 'mobile-app-molecules-expense-report-summary-list-item';

export type Props = $ReadOnly<{
  className?: string,
  onClick: () => void,
  ...ReportSummaryProps,
}>;

export default class ReportSummaryListItem extends React.PureComponent<Props> {
  render() {
    const { className: _className, onClick, ...props } = this.props;
    const className = classNames(ROOT, _className);

    return (
      <LinkListItem className={className} onClick={onClick}>
        <ReportSummary {...props} />
      </LinkListItem>
    );
  }
}
