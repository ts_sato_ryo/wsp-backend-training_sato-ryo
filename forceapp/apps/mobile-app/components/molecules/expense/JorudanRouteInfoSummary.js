// @flow

import * as React from 'react';
import isNil from 'lodash/isNil';
import classNames from 'classnames';

import { type RouteInfo } from '../../../../domain/models/exp/Record';

import JorudanStatusChips from './JorudanStatusChips';

import './JorudanRouteInfoSummary.scss';
import { includeEXReservation } from '../../../../domain/models/exp/jorudan/Route';

const ROOT = 'mobile-app-molecules-expense-jorudan-route-info-summary';

type Props = $ReadOnly<{|
  className?: string,
  routeInfo: RouteInfo,
|}>;

export default class JorudanRouteInfoSummary extends React.Component<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);
    const { origin, arrival, selectedRoute } = this.props.routeInfo;

    return (
      <div className={className}>
        <div className={`${ROOT}__from-to`}>
          {(!isNil(origin) && origin.name) || ''}-
          {(!isNil(arrival) && arrival.name) || ''}
        </div>
        <div className={`${ROOT}__status`}>
          {!isNil(selectedRoute) && !isNil(selectedRoute.status) && (
            <JorudanStatusChips
              isEarliest={selectedRoute.status.isEarliest || false}
              isCheapest={selectedRoute.status.isCheapest || false}
              isMinTransfer={selectedRoute.status.isMinTransfer || false}
              isIncludeEx={includeEXReservation(selectedRoute) || false}
            />
          )}
        </div>
      </div>
    );
  }
}
