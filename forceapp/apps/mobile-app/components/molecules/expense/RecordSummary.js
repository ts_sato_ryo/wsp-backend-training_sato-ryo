// @flow

import * as React from 'react';
import isNil from 'lodash/isNil';
import classNames from 'classnames';

import { type Record, RECORD_TYPE } from '../../../../domain/models/exp/Record';
import { type ExpRequestRecord } from '../../../../domain/models/exp/request/Report';

import DateUtil from '../../../../commons/utils/DateUtil';

import Amount from '../../atoms/Amount';
import Icon from '../../atoms/Icon';
import JorudanRouteInfoSummary from './JorudanRouteInfoSummary';
import transitIcon from '../../../../commons/images/transit-icon.png';

import './RecordSummary.scss';

const ROOT = 'mobile-app-molecules-expense-record-summary';

export type Props = $ReadOnly<{|
  className?: string,
  record: Record | ExpRequestRecord,
  currencyDecimalPlaces: number,
  currencySymbol: string,
|}>;

export default class RecordSummary extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);
    const { record, currencyDecimalPlaces, currencySymbol } = this.props;
    const firstItem =
      !isNil(record.items) && !isNil(record.items[0]) ? record.items[0] : null;

    return (
      <div className={className}>
        <div className={`${ROOT}__subject`}>
          {firstItem && firstItem.expTypeName}
          {record.recordType === RECORD_TYPE.TransitJorudanJP && (
            <div className={`${ROOT}__subject__transit-proof`}>
              <img src={transitIcon} alt="transit" />
            </div>
          )}
          {record.recordType === RECORD_TYPE.FixedAllowanceMulti &&
            ` : ${record.items[0].fixedAllowanceOptionLabel || ''}`}
        </div>
        <div className={`${ROOT}__content`}>
          {record.routeInfo ? (
            <JorudanRouteInfoSummary routeInfo={record.routeInfo} />
          ) : (
            record.remarks
          )}
        </div>
        <div className={`${ROOT}__bottom`}>
          <div className={`${ROOT}__bottom__date`}>
            <div className={`${ROOT}__bottom__date-icon`}>
              <Icon type="event-copy" size="small" />
            </div>
            {DateUtil.formatYMD(record.recordDate)}
          </div>
          <Amount
            className={`${ROOT}__bottom__amount`}
            amount={record.amount}
            decimalPlaces={currencyDecimalPlaces}
            symbol={currencySymbol}
          />
        </div>
      </div>
    );
  }
}
