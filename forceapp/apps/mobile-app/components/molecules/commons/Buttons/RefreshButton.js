// @flow

import * as React from 'react';
import classNames from 'classnames';

import IconButton from '../../../atoms/IconButton';
import displayName from '../../../../../commons/concerns/displayName';

const ROOT = 'mobile-app-components-molecules-commons-buttons-refresh-button';

type Props = $ReadOnly<{|
  className?: string,
  testId?: string,
  type?: 'submit',
  disabled?: boolean,
  onClick: (SyntheticEvent<Element>) => void,
|}>;

export default (displayName('RefreshButton')((props: Props) => (
  <div className={classNames(ROOT, props.className)}>
    <IconButton
      testId={props.testId}
      onClick={props.onClick}
      type={props.type}
      disabled={props.disabled}
      icon="refresh-copy"
      size="small"
    />
  </div>
)): React.ComponentType<Object>);
