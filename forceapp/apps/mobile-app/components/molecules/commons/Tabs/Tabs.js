// @flow

import * as React from 'react';
import classNames from 'classnames';

import Tab from './Tab';

import displayName from '../../../../../commons/concerns/displayName';

import './Tabs.scss';

const ROOT = 'mobile-app-molecules-commons-tabs-tabs';

export type Props = $ReadOnly<{|
  className?: string,
  children: React.Element<typeof Tab>[],
  position?: 'top' | 'bottom',
|}>;

export default (displayName('Tabs')((props: Props) => {
  return (
    <div
      className={classNames(ROOT, props.className, {
        [`${ROOT}--top`]: props.position === 'top',
        [`${ROOT}--bottom`]: props.position === 'bottom',
      })}
    >
      <div className={`${ROOT}__link-wrapper`}>
        {props.children.map((child, index) => (
          <React.Fragment key={index}>{child}</React.Fragment>
        ))}
      </div>
    </div>
  );
}): React.ComponentType<Object>);
