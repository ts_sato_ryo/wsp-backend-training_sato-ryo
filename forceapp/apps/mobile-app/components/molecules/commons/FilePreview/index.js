// @flow
import React from 'react';

import msg from '../../../../../commons/languages';
import FileUtil from '../../../../../commons/utils/FileUtil';
import ViewItem from '../ViewItem';
import Button from '../../../../../commons/components/buttons/Button';

import pdfIcon from '../../../../../commons/images/pdfIcon.png';

import './index.scss';

type Props = {
  url: ?string,
  label?: string,
  rootClassName?: string,
  fileName?: string,
  fileId?: ?string,
};

const FilePreview = (props: Props) => {
  const { url, fileId } = props;

  const ROOT =
    props.rootClassName || 'mobile-app-molecules-commons-file-preview';

  let attachmentContainer = null;
  if (url) {
    const isPDF = FileUtil.getB64FileExtension(url, 'pdf');
    attachmentContainer = (
      <ViewItem label={props.label || ''}>
        {isPDF ? (
          <Button
            onClick={() =>
              FileUtil.downloadFile(
                url,
                fileId,
                `${props.fileName || 'receipt'}.pdf`,
                true
              )
            }
            className={`${ROOT}__pdf-download-link`}
          >
            <img alt="pdf-icon" src={pdfIcon} className={`${ROOT}__pdf-icon`} />
          </Button>
        ) : (
          <img
            alt={msg().Exp_Lbl_Receipt}
            className={`lightbox-img-thumbnail ${ROOT}__image`}
            src={url}
          />
        )}
      </ViewItem>
    );
  }
  return attachmentContainer;
};

export default FilePreview;
