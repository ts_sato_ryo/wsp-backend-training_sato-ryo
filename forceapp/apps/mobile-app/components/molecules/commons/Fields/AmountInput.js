// @flow

import * as React from 'react';
import classNames from 'classnames';

import StringUtil from '../../../../../commons/utils/StringUtil';
import FormatUtil from '../../../../../commons/utils/FormatUtil';

import {
  type InputProps,
  mapPropsToInputProps,
} from '../../../atoms/Fields/InputProps';
import { type IconSetType } from '../../../atoms/Icon/IconSet';

import Input from '../../../atoms/Fields/Input';

const ROOT = 'mobile-app-molecules-commons-amount-input';

type State = {|
  isFocus: boolean,
  value: number | '',
|};

type Props = $ReadOnly<{|
  ...InputProps,
  className?: string,
  testId?: string,
  error?: boolean,
  icon?: IconSetType,
  value: number,
  decimalPlaces?: number,
  onBlur: (number) => void,
|}>;

export default class AmountInput extends React.Component<Props, State> {
  state = {
    isFocus: false,
    value: this.props.value,
  };

  // eslint-disable-next-line camelcase,react/sort-comp
  UNSAFE_componentWillReceiveProps(nextProps: Props) {
    if (
      nextProps.value !== this.props.value ||
      String(nextProps.value) === ''
    ) {
      this.setState({
        value: nextProps.value,
      });
    }
  }

  onBlur = () => {
    this.setState({
      isFocus: false,
    });
    if (this.props.onBlur) {
      this.props.onBlur(this.state.value || 0);
    }
  };

  onFocus = () => {
    this.setState({
      isFocus: true,
    });
  };

  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const decimalPlaces = this.props.decimalPlaces || 0;
    const value = e.target.value;
    if (value === '') {
      this.setState({ value });
    } else if (value.match(/^[0-9|０-９]+$/)) {
      const hanValue = StringUtil.convertToHankaku(value);
      if (hanValue < 1000000000000) {
        this.setState({
          value: hanValue,
        });
      }
    } else if (
      decimalPlaces > 0 &&
      value.match(`^[0-9|０-９]+[\\\\.]?([0-9|０-９]{1,${decimalPlaces}})?$`)
    ) {
      const hanValue = StringUtil.convertToHankaku(value);
      if (hanValue < 1000000000000) {
        this.setState({
          value: hanValue,
        });
      }
    }
  };

  render() {
    const className = classNames(ROOT, this.props.className);

    const value = !this.state.isFocus
      ? FormatUtil.formatNumber(this.state.value, this.props.decimalPlaces || 0)
      : this.state.value;

    return (
      <Input
        {...mapPropsToInputProps(this.props)}
        className={className}
        testId={this.props.testId}
        type="text"
        icon={this.props.icon}
        error={this.props.error}
        value={String(value)}
        onBlur={this.onBlur}
        onFocus={this.onFocus}
        onChange={this.onChange}
        maxLength={13}
      />
    );
  }
}
