// @flow

import * as React from 'react';
import classNames from 'classnames';

import Errors from '../../../atoms/Errors';
import {
  mapPropsToTextAreaProps,
  type TextAreaProps,
} from '../../../atoms/Fields/TextAreaProps';
import TextArea from '../../../atoms/Fields/TextArea';
import Label from '../../../atoms/Label';

const ROOT = 'mobile-app-molecules-commons-text-field';

type Props = $ReadOnly<{|
  className?: string,
  emphasis?: boolean,
  errors?: string[],
  label: string,
  testId?: string,
  ...TextAreaProps,
|}>;

export default class TextField extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);

    const errors = this.props.errors || [];
    const hasErrors = errors.length > 0;
    return (
      <div className={className}>
        <Label
          className={`${ROOT}__label`}
          text={this.props.label}
          marked={this.props.required}
          emphasis={this.props.emphasis}
        >
          <TextArea
            {...mapPropsToTextAreaProps(this.props)}
            error={hasErrors}
            testId={this.props.testId}
          />
        </Label>
        {hasErrors ? <Errors messages={errors} /> : null}
      </div>
    );
  }
}
