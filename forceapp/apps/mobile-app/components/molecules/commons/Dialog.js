// @flow

import * as React from 'react';

import Modal from '../../atoms/Modal';
import Button from '../../atoms/Button';

import './Dialog.scss';

const ROOT = 'mobile-app-molecules-commons-dialog';

type Props = $ReadOnly<{|
  title: string,
  persistent?: boolean,
  isOpened?: boolean,
  content: React.Node,
  leftButtonLabel?: string,
  onClickLeftButton?: (event: SyntheticEvent<Element>) => void,
  rightButtonLabel?: string,
  onClickRightButton?: (event: SyntheticEvent<Element>) => void,
  centerButtonLabel?: string,
  centerButtonDisabled?: boolean,
  onClickCenterButton?: (event: SyntheticEvent<Element>) => void,
  onClickCloseButton?: () => void,
|}>;

export default class Dialog extends React.Component<Props> {
  render() {
    return (
      <Modal
        className={`${ROOT} dark`}
        onClickCloseButton={this.props.onClickCloseButton}
        persistent={this.props.persistent}
        isOpened={this.props.isOpened}
      >
        <div className={`${ROOT}__title`}>{this.props.title}</div>
        <div className={`${ROOT}__content`}>{this.props.content}</div>
        <div className={`${ROOT}__buttons`}>
          {this.props.centerButtonLabel ? (
            <Button
              className={`${ROOT}__button-center`}
              priority="primary"
              variant="neutral"
              onClick={this.props.onClickCenterButton}
              disabled={this.props.centerButtonDisabled}
            >
              {this.props.centerButtonLabel}
            </Button>
          ) : (
            <>
              <button
                className={`${ROOT}__button-left`}
                onClick={this.props.onClickLeftButton}
              >
                {this.props.leftButtonLabel}
              </button>
              <button
                className={`${ROOT}__button-right`}
                onClick={this.props.onClickRightButton}
              >
                {this.props.rightButtonLabel}
              </button>
            </>
          )}
        </div>
      </Modal>
    );
  }
}
