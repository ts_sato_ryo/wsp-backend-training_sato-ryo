// @flow

import * as React from 'react';

import TimeSelect from '../commons/Fields/TimeSelect';

import msg from '../../../../commons/languages';

export type Props = $ReadOnly<{|
  className?: string,
  value?: string,
  defaultValue?: string,
  placeholder?: string,
  disabled?: boolean,
  readOnly?: boolean,
  error?: boolean,
  icon?: boolean,
  testId?: string,
  onChange?: (string) => void,
|}>;

export default (props: Props) => {
  return (
    <TimeSelect
      {...props}
      formatHour={(v: number) => `${String(v)}${msg().Com_Lbl_UnitTimeHours}`}
      formatMinute={(v: number) => `${String(v)}${msg().Com_Lbl_UnitMins}`}
      useTitle
    />
  );
};
