// @flow

import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import { Form } from 'formik';

import msg from '../../../../../../commons/languages';
import Button from '../../../../atoms/Button';
import TextButton from '../../../../atoms/TextButton';
import IconButton from '../../../../atoms/IconButton';

import Navigation from '../../../../molecules/commons/Navigation';
import SFDateField from '../../../../molecules/commons/Fields/SFDateField';

import SelectField from '../../../../molecules/commons/Fields/SelectField';
import SearchStationField from '../../../../molecules/expense/SearchStationField';
import WrapperWithPermission from '../../../../organisms/commons/WrapperWithPermission';
import { MAX_LENGTH_VIA_LIST } from '../../../../../../domain/models/exp/Record';
import { type ExpenseTypeList } from '../../../../../../domain/models/exp/ExpenseType';
import { type CustomHint } from '../../../../../../domain/models/exp/CustomHint';
import { type ErrorInfo } from '../../../../../../commons/utils/AppPermissionUtil';

import RouteOptions from './RouteOptions';

import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-route-form';

type RouteOriginArrivalValues = {
  category: string,
  company: string,
  name: string,
};

export type RouteFormValues = {
  arrival: RouteOriginArrivalValues,
  targetDate: string,
  expenseType: string,
  expenseTypeId: string,
  origin: RouteOriginArrivalValues,
  viaList: Array<any>,
  option: {
    routeSort: string,
    highwayBus: string,
    seatPreference: string,
    useChargedExpress: string,
    useExReservation: string,
  },
};

type RouteFormErrors = {
  targetDate: string,
  arrival: string,
  origin: string,
};

type Props = {
  status: any,
  values: RouteFormValues,
  errors: RouteFormErrors,
  touched: RouteFormValues,
  expenseTypeList: ExpenseTypeList,
  hasPermissionError: ?ErrorInfo,
  setStatus: (any) => void,
  setFieldValue: (key: string, value: any) => void,
  getJorudanExpenseType: (targetDate: string) => void,
  setFieldTouched: (string, {} | boolean, ?boolean) => void,
  getStationSuggestion: (value: string, targetDate?: string) => void,
  handleSubmit: () => void,
  language: string,
  // Custom Hint
  activeHints: Array<string>,
  customHints: CustomHint,
  onClickHint: (string) => void,
};

export default class RoutePage extends React.Component<Props> {
  onDeleteVia = (index: number) => {
    const viaList = [...this.props.values.viaList];
    const status = this.props.status || {};
    const viaListStatus = status.viaList || [];
    viaList.splice(index, 1);
    viaListStatus.splice(index, 1);
    this.props.setFieldValue('viaList', viaList);
    this.props.setStatus(status);
  };

  getCustomHintProps = (fieldName: string, disabled?: boolean) => ({
    hintMsg: !disabled ? this.props.customHints[fieldName] : '',
    isShowHint: this.props.activeHints.includes(fieldName),
    onClickHint: () => this.props.onClickHint(fieldName),
  });

  handleDateChange = (e: any, data: any) => {
    const formattedDate = moment(data.date).format('YYYY-MM-DD');
    this.props.getJorudanExpenseType(formattedDate);
    this.props.setFieldValue('targetDate', formattedDate);
    this.props.setFieldTouched('targetDate', true, false);
  };

  handleSelectChange = (e: any) => {
    const expenseTypeName = e.target.options[e.target.selectedIndex].text;

    this.props.setFieldValue('expenseType', expenseTypeName);
    this.props.setFieldValue('expenseTypeId', e.target.value);
    this.props.setFieldTouched('expenseTypeId', true, false);
  };

  setFormikFieldValue(key: string) {
    return (value: any) => {
      this.props.setFieldValue(key, value);
    };
  }

  setValueForKey = (key: string) => {
    return (e: any) => {
      this.props.setFieldValue(key, e.target.value);
      this.props.setFieldTouched(key, true, false);
    };
  };

  addViaList = () => {
    const viaList = [...this.props.values.viaList, ''];
    this.props.setFieldValue('viaList', viaList);
  };

  searchRouteWithDate = (value: string) => {
    return this.props.getStationSuggestion(value, this.props.values.targetDate);
  };

  renderError = (key: string) => {
    const error = _.get(this.props.errors, key);
    const touched = _.get(this.props.touched, key);
    const status = _.get(this.props.status, key);

    if (error && touched) {
      return [msg()[error]];
    } else if (status) {
      return [status];
    } else {
      return [];
    }
  };

  render() {
    const isDisabled = this.props.values.targetDate === '';
    const selectFieldOptions = this.props.expenseTypeList.map((item) => {
      return {
        label: item.name,
        value: item.id,
      };
    });

    return (
      <WrapperWithPermission
        className={ROOT}
        hasPermissionError={this.props.hasPermissionError}
      >
        <Navigation title={msg().Exp_Btn_Transit} />
        <Form className="main-content">
          <section className={`${ROOT}__date`}>
            <SFDateField
              required
              label={msg().Exp_Clbl_Date}
              value={this.props.values.targetDate}
              onChange={this.handleDateChange}
              errors={this.renderError('targetDate')}
              {...this.getCustomHintProps('recordDate')}
            />
          </section>

          <section className={`${ROOT}__expense-type`}>
            <SelectField
              required
              disabled={isDisabled}
              label={msg().Exp_Clbl_ExpenseType}
              onChange={this.handleSelectChange}
              placeholder={msg().Exp_Lbl_ExpenseTypeSelect}
              errors={this.renderError('expenseType')}
              value={this.props.values.expenseTypeId}
              options={selectFieldOptions}
              {...this.getCustomHintProps('recordExpenseType')}
            />
          </section>

          <section className={`${ROOT}__origin`}>
            <SearchStationField
              disabled={isDisabled}
              label={msg().Exp_Lbl_DepartFrom}
              initialValue={this.props.values.origin}
              errors={this.renderError('origin.name')}
              setFormikFieldValue={this.setFormikFieldValue('origin')}
              searchRoute={this.searchRouteWithDate}
            />
          </section>

          <section className={`${ROOT}__via`}>
            {this.props.values.viaList &&
              this.props.values.viaList.map((viaItem, index) => {
                return (
                  <div
                    key={`via${this.props.values.viaList.length}${index}`}
                    className={`${ROOT}__via-station`}
                  >
                    <SearchStationField
                      disabled={isDisabled}
                      label={msg().Exp_Lbl_Via}
                      initialValue={_.get(
                        this.props.values,
                        `viaList.${index}`
                      )}
                      errors={this.renderError(`viaList.${index}.name`)}
                      setFormikFieldValue={this.setFormikFieldValue(
                        `viaList.${index}`
                      )}
                      searchRoute={this.searchRouteWithDate}
                    />
                    <IconButton
                      className={`${ROOT}__via-delete`}
                      icon="close-copy"
                      onClick={() => this.onDeleteVia(index)}
                    />
                  </div>
                );
              })}
            {this.props.values.viaList.length < MAX_LENGTH_VIA_LIST && (
              <TextButton disabled={isDisabled} onClick={this.addViaList}>
                {msg().Exp_Lbl_AddViaButton}
              </TextButton>
            )}
          </section>

          <section className={`${ROOT}__arrival`}>
            <SearchStationField
              disabled={isDisabled}
              label={msg().Exp_Lbl_Destination}
              initialValue={this.props.values.arrival}
              errors={this.renderError('arrival.name')}
              setFormikFieldValue={this.setFormikFieldValue('arrival')}
              searchRoute={this.searchRouteWithDate}
            />
          </section>

          <section className={`${ROOT}__search-button`}>
            <Button
              priority="primary"
              variant="neutral"
              type="submit"
              onClick={this.props.handleSubmit}
            >
              {msg().Exp_Btn_RouteSearch}
            </Button>
          </section>

          <RouteOptions
            values={this.props.values}
            setValueForKey={this.setValueForKey}
            language={this.props.language}
          />
        </Form>
      </WrapperWithPermission>
    );
  }
}
