// @flow

import React from 'react';
import { get } from 'lodash';
import { Form } from 'formik';

import msg from '../../../../../../../commons/languages';
import FormatUtil from '../../../../../../../commons/utils/FormatUtil';
import DateUtil from '../../../../../../../commons/utils/DateUtil';

import {
  calculateTax,
  type ExpTaxTypeList,
} from '../../../../../../../domain/models/exp/TaxType';

import initFareTypeConfig from './fareConfig';
import ReportTypeSelectDialog from '../../../../commons/ReportTypeSelect';
import Label from '../../../../../atoms/Label';
import TextButton from '../../../../../atoms/TextButton';
import Navigation from '../../../../../molecules/commons/Navigation';
import Dialog from '../../../../../molecules/commons/Dialog';
import TextField from '../../../../../molecules/commons/Fields/TextField';
import SelectField from '../../../../../molecules/commons/Fields/SelectField';
import SFDateField from '../../../../../molecules/commons/Fields/SFDateField';
import LikeInputButtonField from '../../../../../molecules/commons/Fields/LikeInputButtonField';
import RadioButtonGroup from '../../../../../atoms/RadioButtonGroup';
import JorudanAmountSummary from '../../../../../molecules/expense/JorudanAmountSummary';
import RouteMap from '../../../../../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteMap';
import Amount from '../../../../../atoms/Amount';
import Wrapper from '../../../../../atoms/Wrapper';
import { updateValues } from '../../../../../../../commons/utils/FormikUtils';

import { type Record } from '../../../../../../../domain/models/exp/Record';
import { type AccountingPeriodOption } from '../../../../../../../domain/models/exp/AccountingPeriod';
import { type ExpenseReportTypeList } from '../../../../../../../domain/models/exp/expense-report-type/list';
import { type CustomHint } from '../../../../../../../domain/models/exp/CustomHint';
import {
  getExtendedItemArray,
  type ExtendItemInfo,
} from '../../../../../../../domain/models/exp/ExtendedItem';
import { type Report } from '../../../../../../../domain/models/exp/Report';

import './index.scss';
import SearchButtonField from '../../../../../molecules/commons/Fields/SearchButtonField';

const ROOT = 'mobile-app-pages-expense-page-route-list-item';
const DIALOG_ROOT = 'mobile-app-molecules-commons-dialog';

type Props = {
  report: Report,
  recordId?: string,
  isNotEditable: boolean,
  values: Record,
  errors: Record,
  touched: Record,
  status: any,
  isShowDialog: boolean,
  isShowReportTypeSelection: boolean,
  accountingPeriodList: Array<AccountingPeriodOption>,
  accountingDate: string,
  expReportTypeList: ExpenseReportTypeList,
  setFieldValue: (key: string, value: any) => void,
  setFieldTouched: (string, {} | boolean, ?boolean) => void,
  setValues: (value: any) => void,
  setTouched: (value: any) => void,
  handleSubmit: () => void,
  onClickBackButton: () => void,
  onClickCloseButton: () => void,
  onClickDeleteButton: () => void,
  // onClickEditButton: () => void,
  onClickNewRouteRecordButton: () => void,
  onClickRecordListButton: () => void,
  taxList: ExpTaxTypeList,
  baseCurrencyDecimal: number,
  saveFormValues: (Record) => void,
  onClickSearchCustomEI: (string, string, string, string) => void,
  onChangeAccountingDate: (string) => void,
  onChangeAccountingPeriod: (string) => void,
  onClickNext: () => void,
  onClickCloseReportTypeSelection: () => void,
  // Custom Hint
  activeHints: Array<string>,
  customHints: CustomHint,
  onClickHint: (string) => void,
  onClickSearchCostCenter: (string) => void,
  onClickSearchJob: (string) => void,
};

export default class RouteListItem extends React.Component<Props> {
  onChangeUpdateValues = (updateObj: any) => {
    const { values, touched } = updateValues(
      this.props.values,
      this.props.touched,
      updateObj
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  };

  onSearchJob = () => {
    this.props.setFieldValue(
      'items.0.jobName',
      this.props.values.items[0].jobName
    );
    this.props.saveFormValues(this.props.values);
    this.props.onClickSearchJob(this.props.values.recordDate);
  };

  onSearchCostCenter = () => {
    this.props.setFieldValue(
      'items.0.costCenterName',
      this.props.values.items[0].costCenterName
    );
    this.props.saveFormValues(this.props.values);
    this.props.onClickSearchCostCenter(this.props.values.recordDate);
  };

  getCustomHintProps = (fieldName: string, disabled?: boolean) => ({
    hintMsg: !disabled ? this.props.customHints[fieldName] : '',
    isShowHint: this.props.activeHints.includes(fieldName),
    onClickHint: () => this.props.onClickHint(fieldName),
  });

  getCustomHintforEI = (id: string, info: ExtendItemInfo) => ({
    hintMsg: !this.props.recordId ? info.description : '',
    isShowHint: this.props.activeHints.includes(id),
    onClickHint: () => this.props.onClickHint(id),
  });

  getPickListValue(idx: number) {
    const value = this.props.values.items[0];
    const selectedVal = value[`extendedItemPicklist${this.getIdx(idx)}Value`];
    return selectedVal;
  }

  buildOptionList(picklist: any) {
    const optionList = picklist.map((pick) => ({
      value: pick.value || '',
      label: pick.label,
    }));
    return [{ value: '', label: msg().Exp_Lbl_PleaseSelect }, ...optionList];
  }

  getIdx(idx: number) {
    return `0${idx + 1}`.slice(-2);
  }

  setError = (field: string) => {
    const errors = get(this.props.errors, field);
    const isFieldTouched = get(this.props.touched, field);
    return errors && isFieldTouched ? [errors] : [];
  };

  // helper method to improve readability
  format = (value: string | number) => {
    return `￥${FormatUtil.convertToIntegerString(value)}`;
  };

  setValueForKey = (key: string) => {
    return (e: any) => {
      this.props.setFieldValue(key, e.target.value);
      this.props.setFieldTouched(key, true, false);
    };
  };

  handleRoundTripChange = (e: any) => {
    const route = get(this.props.values, 'routeInfo.selectedRoute');
    const amount = e.target.value === '0' ? route.cost : route.roundTripCost;

    this.props.setFieldValue('routeInfo.roundTrip', e.target.value);
    this.props.setFieldTouched('routeInfo.roundTrip', true, false);
    this.props.setFieldValue('items[0].amount', amount);

    const tax = this.props.taxList[0];
    const taxRes = calculateTax(
      tax.rate,
      amount,
      this.props.baseCurrencyDecimal
    );
    this.props.setFieldValue('withoutTax', taxRes.amountWithoutTax);
    this.props.setFieldValue('items[0].withoutTax', taxRes.amountWithoutTax);
    this.props.setFieldValue('items[0].gstVat', taxRes.gstVat);
  };

  renderRecordSaveSummaryDialog() {
    const { values, status, isShowDialog } = this.props;

    return (
      status &&
      status.isRecordSaved &&
      isShowDialog && (
        <Dialog
          title={msg().Exp_Lbl_ItemIsSaved}
          content={
            <div className={`${DIALOG_ROOT}__content-container`}>
              <p className={`${DIALOG_ROOT}__item`}>{msg().Com_Lbl_Date}:</p>
              <p className={`${DIALOG_ROOT}__sub-item`}>
                {FormatUtil.formatDateWithLocale(values.recordDate)}
              </p>
              <p className={`${DIALOG_ROOT}__item`}>
                {msg().Exp_Lbl_ExpenseType}:
              </p>
              <p className={`${DIALOG_ROOT}__sub-item`}>
                {values.items[0].expTypeName}
              </p>
              <p className={`${DIALOG_ROOT}__item`}>{msg().Appr_Lbl_Detail}:</p>
              <p className={`${DIALOG_ROOT}__sub-item`}>
                {get(values, 'routeInfo.origin.name')} -
                {get(values, 'routeInfo.arrival.name')}
              </p>
              <Amount
                className={`${ROOT}__item`}
                amount={values.items[0].amount}
                decimalPlaces={0}
                symbol="￥"
              />
            </div>
          }
          leftButtonLabel={msg().Appr_Lbl_Submit}
          rightButtonLabel={msg().Appr_Lbl_ContinueToRegister}
          onClickLeftButton={this.props.onClickRecordListButton}
          onClickRightButton={this.props.onClickNewRouteRecordButton}
          onClickCloseButton={this.props.onClickCloseButton}
        />
      )
    );
  }

  renderReportTypeSelectionDialog() {
    return (
      this.props.isShowReportTypeSelection && (
        <ReportTypeSelectDialog
          isShowDialog={this.props.isShowReportTypeSelection}
          accountingPeriodList={this.props.accountingPeriodList}
          accountingDate={this.props.accountingDate}
          expReportTypeList={this.props.expReportTypeList}
          onChangeAccountingPeriod={this.props.onChangeAccountingPeriod}
          onChangeAccountingDate={this.props.onChangeAccountingDate}
          onClickNext={this.props.onClickNext}
          onClickCloseDialog={this.props.onClickCloseReportTypeSelection}
        />
      )
    );
  }

  render() {
    const { values, recordId, report } = this.props;

    if (!values) {
      return null;
    }

    const isReadOnly = !!recordId;
    const fareType = initFareTypeConfig();
    const route = values.routeInfo && values.routeInfo.selectedRoute;
    const navigationActions = recordId
      ? [
          <TextButton
            disabled={this.props.isNotEditable}
            onClick={this.props.onClickDeleteButton}
          >
            {msg().Com_Btn_Delete}
          </TextButton>,
          // TODO: Hide until policy is decided
          /*
        <TextButton disabled={this.props.isNotEditable} onClick={this.props.onClickEditButton}>
          {msg().Com_Btn_Edit}
        </TextButton>,
        */
        ]
      : [
          <TextButton type="submit" onClick={this.props.handleSubmit}>
            {msg().Com_Btn_Save}
          </TextButton>,
        ];

    const { jobId, costCenterHistoryId } = report;
    let { jobName, costCenterName } = report;
    if (values.items[0].jobId) {
      jobName = values.items[0].jobName;
    }
    if (values.items[0].costCenterHistoryId) {
      costCenterName = values.items[0].costCenterName;
    }

    return (
      <Wrapper className={ROOT}>
        <Navigation
          title={msg().Exp_Lbl_Records}
          backButtonLabel={msg().Com_Lbl_Back}
          onClickBack={this.props.onClickBackButton}
          actions={navigationActions}
        />
        <Form className="main-content">
          <section className={`${ROOT}__date`}>
            <TextField
              required
              disabled
              label={msg().Exp_Lbl_Date}
              value={FormatUtil.formatDateWithLocale(values.recordDate)}
            />
          </section>

          <section className={`${ROOT}__fare-type`}>
            <RadioButtonGroup
              disabled={isReadOnly}
              required
              label={fareType.labels}
              options={fareType.options}
              value={get(values, 'routeInfo.roundTrip')}
              onChange={this.handleRoundTripChange}
            />
          </section>

          <section className={`${ROOT}__amount`}>
            <Label text={msg().Exp_Lbl_Amount} />
            <p className={`${ROOT}__amount-text`}>
              {this.format(values.items[0].amount)}
            </p>
          </section>

          <section className={`${ROOT}__route-map`}>
            <Label text={msg().Com_Lbl_Route} />
            <JorudanAmountSummary route={route} />
            <RouteMap routeInfo={values.routeInfo} mobile />
          </section>

          {costCenterHistoryId && (
            <section>
              <LikeInputButtonField
                required
                disabled={isReadOnly}
                errors={this.setError('items.0.costCenterName')}
                onClick={() => this.onSearchCostCenter()}
                value={costCenterName || ''}
                label={msg().Exp_Clbl_CostCenter}
                {...this.getCustomHintProps('recordCostCenter', isReadOnly)}
              />
            </section>
          )}

          {jobId && (
            <section>
              <LikeInputButtonField
                required
                disabled={isReadOnly}
                errors={this.setError('items.0.jobName')}
                onClick={() => this.onSearchJob()}
                value={jobName || ''}
                label={msg().Exp_Lbl_Job}
                {...this.getCustomHintProps('recordJob', isReadOnly)}
              />
            </section>
          )}

          {getExtendedItemArray(this.props.values.items[0])
            .filter((i) => i.id)
            .map(({ id, info, index }) => {
              if (!info) {
                return null;
              }

              let $field;
              switch (info.inputType) {
                case 'Text':
                  $field = (
                    <section className={`${ROOT}__input`} key={id}>
                      <TextField
                        required={info.isRequired}
                        disabled={isReadOnly}
                        label={info.name}
                        errors={this.setError(
                          `items.0.extendedItemText${index}Value`
                        )}
                        onChange={(e) => {
                          const val = e.target.value;
                          this.onChangeUpdateValues({
                            [`items.0.extendedItemText${index}Value`]: val,
                            [`items.0.extendedItemText${index}Id`]: id,
                          });
                        }}
                        value={
                          values.items[0][`extendedItemText${index}Value`] || ''
                        }
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;

                case 'Picklist':
                  $field = (
                    <section key={id} className={`${ROOT}__input`}>
                      <SelectField
                        disabled={isReadOnly}
                        errors={this.setError(
                          `items.0.extendedItemPicklist${index}Value`
                        )}
                        required={info.isRequired}
                        label={info.name}
                        options={this.buildOptionList(info.picklist)}
                        onChange={this.setValueForKey(
                          `items.0.extendedItemPicklist${index}Value`
                        )}
                        value={
                          values.items[0][
                            `extendedItemPicklist${index}Value`
                          ] || ''
                        }
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;

                case 'Lookup':
                  $field = (
                    <section key={id} className={`${ROOT}__input`}>
                      <SearchButtonField
                        placeholder={msg().Admin_Lbl_Search}
                        required={info.isRequired}
                        errors={this.setError(
                          `items.0.extendedItemLookup${index}Value`
                        )}
                        disabled={isReadOnly}
                        onClick={() => {
                          this.props.saveFormValues(this.props.values);
                          this.props.onClickSearchCustomEI(
                            id,
                            info.extendedItemCustomId,
                            info.name,
                            index
                          );
                        }}
                        onClickDeleteButton={() => {
                          this.onChangeUpdateValues({
                            [`items.0.extendedItemLookup${index}Value`]: null,
                            [`items.0.extendedItemLookup${index}SelectedOptionName`]: null,
                          });
                        }}
                        value={
                          values.items[0][
                            `extendedItemLookup${index}SelectedOptionName`
                          ] || ''
                        }
                        label={info.name}
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;

                case 'Date':
                  const onChangeDateFieldValue = (date?: Date) => {
                    const updateValue = (date && DateUtil.fromDate(date)) || '';
                    this.onChangeUpdateValues({
                      [`items.0.extendedItemDate${index}Value`]: updateValue,
                      [`items.0.extendedItemDate${index}Id`]: id,
                    });
                  };
                  $field = (
                    <section key={id} className={`${ROOT}__input`}>
                      <SFDateField
                        key={id}
                        required={info.isRequired}
                        disabled={isReadOnly}
                        label={info.name}
                        errors={this.setError(
                          `items.0.extendedItemDate${index}Value`
                        )}
                        onChange={(e, { date }) => {
                          onChangeDateFieldValue(date);
                        }}
                        value={
                          values.items[0][`extendedItemDate${index}Value`] || ''
                        }
                        useRemoveValueButton
                        onClickRemoveValueButton={() =>
                          onChangeDateFieldValue()
                        }
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;

                default:
                  $field = null;
              }

              return $field;
            })}

          <section className={`${ROOT}__summary`}>
            <TextField
              label={msg().Exp_Lbl_Summary}
              value={values.items[0].remarks}
              disabled={isReadOnly}
              onChange={this.setValueForKey('items[0].remarks')}
              {...this.getCustomHintProps('recordSummary', isReadOnly)}
            />
          </section>
        </Form>
        {this.renderRecordSaveSummaryDialog()}
        {this.renderReportTypeSelectionDialog()}
      </Wrapper>
    );
  }
}
