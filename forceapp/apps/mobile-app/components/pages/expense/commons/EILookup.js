// @flow

import React from 'react';

import type { CustomEIOptionList } from '../../../../../domain/models/exp/ExtendedItem';

import msg from '../../../../../commons/languages';
import Navigation from '../../../molecules/commons/Navigation';
import LinkListItem from '../../../atoms/LinkListItem';
import Wrapper from '../../../atoms/Wrapper';
import SearchField from '../../../molecules/commons/Fields/SearchField';

import './EILookup.scss';

const ROOT = 'mobile-app-pages-ei-lookup';

type State = {
  keyword: string,
  label: string,
};

type Props = {
  title: string,
  eiLookup: CustomEIOptionList,
  onClickBack: () => void,
  onClickSearchButton: (keyword: string) => void,
  onClickRow: (item: any) => void,
};

export default class EILookup extends React.Component<Props, State> {
  state = {
    keyword: '',
    label: msg().Exp_Lbl_RecentlyUsed,
  };

  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({ keyword: e.target.value });
  };

  onClickSearch = () => {
    this.setState({ label: msg().Exp_Lbl_SearchResult });
    this.props.onClickSearchButton(this.state.keyword);
  };

  render() {
    return (
      <Wrapper className={ROOT}>
        <Navigation
          title={this.props.title}
          onClickBack={this.props.onClickBack}
        />
        <SearchField
          placeHolder={msg().Com_Lbl_Search}
          iconClick={() => this.onClickSearch()}
          onChange={this.onChange}
          value={this.state.keyword}
        />
        <div className={`${ROOT}__label`}>{this.state.label}</div>
        <div className="main-content">
          <div className={`${ROOT}__result`}>
            {this.props.eiLookup.records.map((item, idx) => (
              <div key={idx} className={`${ROOT}__row`}>
                <LinkListItem
                  key={item.id}
                  className={`${ROOT}__item`}
                  onClick={() => this.props.onClickRow(item)}
                  noIcon
                >
                  {`${item.code} - ${item.name}`}
                </LinkListItem>
              </div>
            ))}
            <div className={`${ROOT}__has-more`}>
              {this.props.eiLookup.records.length === 0 &&
                `${msg().Com_Lbl_ZeroSearchResult}`}

              {this.props.eiLookup.hasMore &&
                msg().Com_Lbl_TooManySearchResults}
            </div>
          </div>
        </div>
      </Wrapper>
    );
  }
}
