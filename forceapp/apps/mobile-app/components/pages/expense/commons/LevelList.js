// @flow

import React from 'react';

import msg from '../../../../../commons/languages';
import Navigation from '../../../molecules/commons/Navigation';
import LevelLinkListItem from '../../../molecules/expense/LevelLinkListItem';
import SearchField from '../../../molecules/commons/Fields/SearchField';

import './LevelList.scss';

const ROOT = 'mobile-app-pages-level-list';

type State = {
  keyword: string,
};

type Props = {
  searchingList: [],
  searchingAllList: [],
  title: string,
  label?: string,
  onClickBack: () => void,
  onClickSearchButton: (keyword: string) => void,
  onClickRow: (item: any) => void,
  onClickIcon: (item: any) => void,
  notSearchingRealTime?: boolean,
  hasMore?: boolean,
};

export default class LevelList extends React.Component<Props, State> {
  state = {
    keyword: '',
  };

  onChange = (value: string) => {
    this.setState({ keyword: value });
  };

  render() {
    const displayList = this.state.keyword
      ? this.props.searchingAllList
      : this.props.searchingList;

    return (
      <article>
        <Navigation
          title={this.props.title}
          onClickBack={this.props.onClickBack}
        />
        <div className={ROOT}>
          <section className={`${ROOT}__input`}>
            <SearchField
              placeHolder={msg().Com_Lbl_Search}
              onChange={(e) => this.onChange(e.target.value)}
              value={this.state.keyword}
              iconClick={() => {
                if (this.state.keyword) {
                  this.props.onClickSearchButton(this.state.keyword);
                }
              }}
            />
          </section>
          <section className={`${ROOT}__label`}>{this.props.label}</section>
          <section className={`${ROOT}__result`}>
            {displayList
              .filter(
                (item) =>
                  item.name.indexOf(this.state.keyword) >= 0 ||
                  item.code.indexOf(this.state.keyword) >= 0 ||
                  this.props.notSearchingRealTime
              )
              .map((item, idx) => (
                <section key={idx} className={`${ROOT}__row`}>
                  <LevelLinkListItem
                    key={item.requestId}
                    className={`${ROOT}__item`}
                    onClickBody={() => this.props.onClickRow(item)}
                    onClickIcon={
                      !this.state.keyword && item.hasChildren
                        ? () => this.props.onClickIcon(item)
                        : null
                    }
                  >
                    {item.code} - {item.name}
                  </LevelLinkListItem>
                </section>
              ))}
            {this.props.hasMore && (
              <section key="hasMore" className={`${ROOT}__hasMore`}>
                <LevelLinkListItem key="hasMore" className={`${ROOT}__item`}>
                  {msg().Com_Lbl_TooManySearchResults}
                </LevelLinkListItem>
              </section>
            )}
          </section>
        </div>
      </article>
    );
  }
}
