// @flow

import React from 'react';

import {
  type Record,
  RECORD_TYPE,
} from '../../../../../../../domain/models/exp/Record';
import msg from '../../../../../../../commons/languages';
import AmountInputField from '../../../../../molecules/commons/Fields/AmountInputField';
import ViewItem from '../../../../../molecules/commons/ViewItem';
import Amount from '../../../../../atoms/Amount';
import { calculateTax } from '../../../../../../../domain/models/exp/TaxType';

import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-record-new-general-amount-area';

type Props = {
  values: Record,
  rate: number,
  currencyDecimalPlace: number,
  currencySymbol: string,
  readOnly: boolean,
  onChangeUpdateValues: ({}) => void,
  setError: (string) => string[],
};

export default class AmountArea extends React.Component<Props> {
  onChangeAmount = (value: any) => {
    const amount = value || 0;

    const taxRes = calculateTax(
      this.props.rate,
      amount,
      this.props.currencyDecimalPlace
    );
    this.props.onChangeUpdateValues({
      amount,
      withoutTax: taxRes.amountWithoutTax,
      'items.0.amount': amount,
      'items.0.withoutTax': taxRes.amountWithoutTax,
      'items.0.gstVat': taxRes.gstVat,
    });
  };

  render() {
    const {
      values,
      setError,
      readOnly,
      currencyDecimalPlace,
      currencySymbol,
    } = this.props;
    return (
      <div className={ROOT}>
        <AmountInputField
          required
          disabled={
            readOnly ||
            this.props.values.recordType === RECORD_TYPE.FixedAllowanceSingle ||
            this.props.values.recordType === RECORD_TYPE.FixedAllowanceMulti
          }
          label={msg().Exp_Clbl_IncludeTax}
          errors={setError('items.0.amount')}
          onBlur={(value) => this.onChangeAmount(value)}
          value={values.items[0].amount}
          decimalPlaces={currencyDecimalPlace}
        />
        <ViewItem label={msg().Exp_Clbl_WithoutTax} align="right">
          <Amount
            amount={values.items[0].withoutTax}
            decimalPlaces={currencyDecimalPlace}
            symbol={currencySymbol}
          />
        </ViewItem>
        <ViewItem label={msg().Exp_Clbl_GstAmount} align="right">
          <Amount
            amount={values.items[0].gstVat}
            decimalPlaces={currencyDecimalPlace}
            symbol={currencySymbol}
          />
        </ViewItem>
      </div>
    );
  }
}
