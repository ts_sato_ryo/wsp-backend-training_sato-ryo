// @flow

import React from 'react';

import { isEmpty, find } from 'lodash';

import { type Record } from '../../../../../../../domain/models/exp/Record';
import msg from '../../../../../../../commons/languages';
import SelectField from '../../../../../molecules/commons/Fields/SelectField';
import {
  type ExpTaxTypeList,
  calculateTax,
  //  calcTaxFromGstVat,
} from '../../../../../../../domain/models/exp/TaxType';

import './index.scss';

type Props = {
  taxTypeList: ExpTaxTypeList,
  values: Record,
  currencyDecimalPlace: number,
  readOnly: boolean,
  onChangeUpdateValues: ({}) => void,
  setRate: (number) => any,
  setError: (string) => string[],
};

export default class TaxTypeArea extends React.Component<Props> {
  onChangeTaxType = (value: any) => {
    let historyId;
    let rate = 0;
    let baseId;
    let name;
    if (this.props.taxTypeList.length > 0) {
      const taxType = find(this.props.taxTypeList, { baseId: value });
      if (taxType) {
        historyId = taxType.historyId;
        rate = taxType.rate;
        baseId = taxType.baseId;
        name = taxType.name;
      } else {
        historyId = 'default';
        rate = this.props.taxTypeList[0].rate;
        baseId = 'default';
        name = 'default';
      }
    } else {
      historyId = 'default';
      rate = 0;
      baseId = 'default';
      name = 'default';
    }
    const { amount } = this.props.values.items[0];
    const taxRes = calculateTax(rate, amount, this.props.currencyDecimalPlace);
    this.props.setRate(rate);
    this.props.onChangeUpdateValues({
      amount,
      withoutTax: taxRes.amountWithoutTax,
      'items.0.amount': amount,
      'items.0.withoutTax': taxRes.amountWithoutTax,
      'items.0.gstVat': taxRes.gstVat,
      'items.0.taxTypeBaseId': baseId,
      'items.0.taxTypeHistoryId': historyId,
      'items.0.taxTypeName': name,
    });
  };

  render() {
    const { values, taxTypeList } = this.props;
    const isDisabledTaxType =
      isEmpty(values.items[0].expTypeId) || this.props.readOnly;
    const taxTypeListOptions = taxTypeList.map((taxType) => {
      return {
        label: taxType.name,
        value: taxType.baseId,
      };
    });

    taxTypeList
      .map((taxType) => {
        return {
          label: taxType.name,
          value: taxType.baseId,
        };
      })
      .push();
    return (
      <SelectField
        required
        disabled={isDisabledTaxType}
        label={msg().Exp_Clbl_Gst}
        errors={this.props.setError('items.0.taxTypeBaseId')}
        options={taxTypeListOptions}
        onChange={(e: any) => {
          this.onChangeTaxType(e.target.value);
        }}
        value={values.items[0].taxTypeBaseId}
      />
    );
  }
}
