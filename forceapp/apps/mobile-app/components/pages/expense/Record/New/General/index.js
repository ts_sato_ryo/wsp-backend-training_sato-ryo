// @flow

import React from 'react';

import { Form } from 'formik';
import moment from 'moment';
import { isEmpty, get, concat, cloneDeep, find } from 'lodash';
import { updateValues } from '../../../../../../../commons/utils/FormikUtils';

import { type ExpTaxTypeList } from '../../../../../../../domain/models/exp/TaxType';
import { type Base64FileList } from '../../../../../../../domain/models/exp/Receipt';
import {
  type Record,
  RECORD_TYPE,
} from '../../../../../../../domain/models/exp/Record';
import { type CustomHint } from '../../../../../../../domain/models/exp/CustomHint';
import {
  type AmountOption,
  type ExpenseType,
} from '../../../../../../../domain/models/exp/ExpenseType';
import { type Report } from '../../../../../../../domain/models/exp/Report';
import { type ErrorInfo } from '../../../../../../../commons/utils/AppPermissionUtil';
import msg from '../../../../../../../commons/languages';
import LikeInputButtonField from '../../../../../molecules/commons/Fields/LikeInputButtonField';
import ReportTypeSelectDialog from '../../../../commons/ReportTypeSelect';
import TextField from '../../../../../molecules/commons/Fields/TextField';
import SFDateField from '../../../../../molecules/commons/Fields/SFDateField';
import Navigation from '../../../../../molecules/commons/Navigation';
import TextButton from '../../../../../atoms/TextButton';
import WrapperWithPermission from '../../../../../organisms/commons/WrapperWithPermission';
import SaveDialog from './SaveDialog';
import AmountArea from './AmountArea';
import ExtendedItem from './ExtendedItem';
import TaxTypeArea from './TaxTypeArea';
import { type AccountingPeriodOption } from '../../../../../../../domain/models/exp/AccountingPeriod';
import { type ExpenseReportTypeList } from '../../../../../../../domain/models/exp/expense-report-type/list';
import Attachment from './Attachment';
import FixedAmountSelectionArea from './FixedAmountSelectionArea';

import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-record-new-general';

type Props = {
  report: Report,
  taxTypeList: ExpTaxTypeList,
  isNotEditable: boolean,
  isShowSaveDialog: boolean,
  isShowReportTypeSelection: boolean,
  values: Record,
  rate: number,
  type: string,
  errors: Record,
  touched: Record,
  currencyDecimalPlace: number,
  currencySymbol: string,
  readOnly: boolean,
  hasPermissionError: ?ErrorInfo,
  accountingPeriodList: Array<AccountingPeriodOption>,
  expReportTypeList: ExpenseReportTypeList,
  accountingDate: string,
  setFieldTouched: (string, {} | boolean, ?boolean) => void,
  setFieldValue: (key: string, value: any, boolean) => void,
  setValues: (value: any) => void,
  setTouched: (value: any) => void,
  setRate: (number) => void,
  saveFormValues: (Record) => boolean,
  handleSubmit: () => void,
  onDeleteClick: () => void,
  onClickEditButton: () => void,
  onBackClick: () => void,
  onSearchClick: () => void,
  onDialogLeftClick: () => void,
  onDialogRightClick: () => void,
  onDialogCloseClick: () => void,
  onClickSearchCustomEI: (string, string, string) => void,
  onChangeAccountingDate: (string) => void,
  onChangeAccountingPeriod: (string) => void,
  onClickNext: () => void,
  onClickCloseReportTypeSelection: () => void,
  getBase64files: (
    e: SyntheticInputEvent<HTMLInputElement>
  ) => Promise<Base64FileList>,
  uploadReceipts: (
    list: Base64FileList
  ) => Promise<{
    contentVersionId: string,
    contentDocumentId: string,
  }>,
  expenseTypeList: Array<ExpenseType>,
  // Custom Hint
  activeHints: Array<string>,
  customHints: CustomHint,
  onClickHint: (string) => void,
  onClickSearchCostCenter: (string) => void,
  onClickSearchJob: (string) => void,
};

export default class Recordform extends React.Component<Props> {
  onSearchClick = () => {
    return () => {
      this.props.setFieldValue('recordId', this.props.values.recordId, false);
      this.props.saveFormValues(this.props.values);
      this.props.onSearchClick();
    };
  };

  onSearchJob = () => {
    this.props.setFieldValue(
      'items.0.jobName',
      this.props.values.items[0].jobName,
      false
    );
    this.props.saveFormValues(this.props.values);
    this.props.onClickSearchJob(this.props.values.recordDate);
  };

  onSearchCostCenter = () => {
    this.props.setFieldValue(
      'items.0.costCenterName',
      this.props.values.items[0].costCenterName,
      false
    );
    this.props.saveFormValues(this.props.values);
    this.props.onClickSearchCostCenter(this.props.values.recordDate);
  };

  onChangeValue = (field: string) => {
    return (e: any) => {
      this.props.setFieldValue(field, e.target.value, false);
      this.props.setFieldTouched(field, true);
    };
  };

  onChangeUpdateValues = (updateObj: any) => {
    const { values, touched } = updateValues(
      this.props.values,
      this.props.touched,
      updateObj
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  };

  handleDateChange = (e: any, data: any) => {
    const formattedDate = moment(data.date).format('YYYY-MM-DD');
    const values = cloneDeep(this.props.values);

    values.recordDate = formattedDate;
    values.withoutTax = 0;
    values.items[0].withoutTax = 0;
    values.items[0].gstVat = 0;
    values.items[0].expTypeId = '';
    values.items[0].expTypeName = '';
    values.items[0].taxTypeBaseId = 'default';
    values.items[0].taxTypeHistoryId = 'default';
    values.items[0].taxTypeName = '';
    this.props.setValues(values);
    this.props.setTouched({ recordDate: true });
  };

  setError = (field: string) => {
    const errors = get(this.props.errors, field);
    const isFieldTouched = get(this.props.touched, field);
    return errors && isFieldTouched ? [errors] : [];
  };

  getCustomHintProps = (fieldName: string, disabled?: boolean) => ({
    hintMsg: !disabled ? this.props.customHints[fieldName] : '',
    isShowHint: this.props.activeHints.includes(fieldName),
    onClickHint: () => this.props.onClickHint(fieldName),
  });

  renderBackButton = () => {
    return this.props.type !== 'new' ? this.props.onBackClick : undefined;
  };

  renderDeleteAndEditButton = () =>
    this.props.readOnly
      ? [
          <TextButton
            type="submit"
            disabled={this.props.isNotEditable}
            onClick={this.props.onDeleteClick}
          >
            {msg().Com_Btn_Delete}
          </TextButton>,
          <TextButton
            type="submit"
            disabled={this.props.isNotEditable}
            onClick={this.props.onClickEditButton}
          >
            {msg().Com_Btn_Edit}
          </TextButton>,
        ]
      : [];

  renderSaveButton = () =>
    !this.props.readOnly
      ? [
          <TextButton type="submit" onClick={this.props.handleSubmit}>
            {msg().Com_Btn_Save}
          </TextButton>,
        ]
      : [];

  render() {
    const { values, type, readOnly, report } = this.props;
    const isDisabledExpenseType = isEmpty(values.recordDate) || readOnly;
    const { jobId, costCenterHistoryId } = report;
    let { jobName, costCenterName } = report;
    if (values.items[0].jobId) {
      jobName = values.items[0].jobName;
    }
    if (values.items[0].costCenterHistoryId) {
      costCenterName = values.items[0].costCenterName;
    }

    const selectedExpType = find(this.props.expenseTypeList, {
      id: this.props.values.items[0].expTypeId,
    });
    const fixedAllowanceOptionList: ?Array<AmountOption> =
      selectedExpType && selectedExpType.fixedAllowanceOptionList;
    return (
      <WrapperWithPermission
        className={ROOT}
        hasPermissionError={this.props.hasPermissionError}
      >
        <Navigation
          backButtonLabel={msg().Com_Lbl_Back}
          onClickBack={this.renderBackButton()}
          title={
            type !== 'new' ? msg().Exp_Lbl_Records : msg().Exp_Lbl_NewRecord
          }
          actions={concat(
            this.renderDeleteAndEditButton(),
            this.renderSaveButton()
          )}
        />
        <Form className="main-content">
          <SFDateField
            required
            disabled={this.props.readOnly}
            label={msg().Exp_Clbl_Date}
            errors={this.setError('recordDate')}
            onChange={this.handleDateChange}
            value={values.recordDate}
            {...this.getCustomHintProps('recordDate', readOnly)}
          />
          <LikeInputButtonField
            required
            disabled={isDisabledExpenseType}
            errors={this.setError('items.0.expTypeName')}
            onClick={this.onSearchClick()}
            value={values.items[0].expTypeName || ''}
            label={msg().Exp_Clbl_ExpenseType}
            {...this.getCustomHintProps('recordExpenseType', readOnly)}
          />
          <TaxTypeArea
            values={values}
            currencyDecimalPlace={this.props.currencyDecimalPlace}
            currencySymbol={this.props.currencySymbol}
            readOnly={this.props.readOnly}
            taxTypeList={this.props.taxTypeList}
            setRate={this.props.setRate}
            setError={this.setError}
            onChangeUpdateValues={this.onChangeUpdateValues}
          />

          {this.props.values.recordType === RECORD_TYPE.FixedAllowanceMulti && (
            <FixedAmountSelectionArea
              rate={this.props.rate}
              currencyDecimalPlace={this.props.currencyDecimalPlace}
              values={values}
              fixedAllowanceOptionList={fixedAllowanceOptionList}
              currencySymbol={this.props.currencySymbol}
              readOnly={this.props.readOnly}
              taxTypeList={this.props.taxTypeList}
              errors={this.setError('items.0.fixedAllowanceOptionId')}
              onChangeUpdateValues={this.onChangeUpdateValues}
              decimalPlaces={this.props.currencyDecimalPlace}
            />
          )}

          <AmountArea
            rate={this.props.rate}
            setError={this.setError}
            values={values}
            onChangeUpdateValues={this.onChangeUpdateValues}
            currencyDecimalPlace={this.props.currencyDecimalPlace}
            currencySymbol={this.props.currencySymbol}
            readOnly={this.props.readOnly}
          />
          {costCenterHistoryId && (
            <LikeInputButtonField
              required
              disabled={isDisabledExpenseType}
              errors={this.setError('items.0.costCenterName')}
              onClick={() => this.onSearchCostCenter()}
              value={costCenterName || ''}
              label={msg().Exp_Clbl_CostCenter}
              {...this.getCustomHintProps('recordCostCenter', readOnly)}
            />
          )}
          {jobId && (
            <LikeInputButtonField
              required
              disabled={isDisabledExpenseType}
              errors={this.setError('items.0.jobName')}
              onClick={() => this.onSearchJob()}
              value={jobName || ''}
              label={msg().Exp_Lbl_Job}
              {...this.getCustomHintProps('recordJob', readOnly)}
            />
          )}
          <Attachment
            receiptFileId={values.receiptFileId}
            receiptId={values.receiptId}
            receiptData={values.receiptData}
            fileName={values.fileName}
            readOnly={this.props.readOnly}
            getBase64files={this.props.getBase64files}
            uploadReceipts={this.props.uploadReceipts}
            onChangeUpdateValues={this.onChangeUpdateValues}
            receiptConfig={values.fileAttachment}
            errors={this.setError('receiptId')}
          />

          <ExtendedItem
            setError={this.setError}
            onChangeUpdateValues={this.onChangeUpdateValues}
            onChangeValue={this.onChangeValue}
            item={values.items[0]}
            readOnly={this.props.readOnly}
            onClickSearchCustomEI={this.props.onClickSearchCustomEI}
            saveFormValues={this.props.saveFormValues}
            record={this.props.values}
            activeHints={this.props.activeHints}
            onClickHint={this.props.onClickHint}
          />
          <TextField
            disabled={readOnly}
            label={msg().Exp_Clbl_Summary}
            onChange={this.onChangeValue('items.0.remarks')}
            errors={this.setError('items.0.remarks')}
            value={values.items[0].remarks || ''}
            {...this.getCustomHintProps('recordSummary', readOnly)}
          />
        </Form>
        <SaveDialog
          currencyDecimalPlace={this.props.currencyDecimalPlace}
          currencySymbol={this.props.currencySymbol}
          isShowDialog={this.props.isShowSaveDialog}
          values={values}
          onDialogLeftClick={this.props.onDialogLeftClick}
          onDialogRightClick={this.props.onDialogRightClick}
          onDialogCloseClick={this.props.onDialogCloseClick}
        />
        <ReportTypeSelectDialog
          isShowDialog={this.props.isShowReportTypeSelection}
          accountingPeriodList={this.props.accountingPeriodList}
          accountingDate={this.props.accountingDate}
          expReportTypeList={this.props.expReportTypeList}
          onChangeAccountingPeriod={this.props.onChangeAccountingPeriod}
          onChangeAccountingDate={this.props.onChangeAccountingDate}
          onClickNext={this.props.onClickNext}
          onClickCloseDialog={this.props.onClickCloseReportTypeSelection}
        />
      </WrapperWithPermission>
    );
  }
}
