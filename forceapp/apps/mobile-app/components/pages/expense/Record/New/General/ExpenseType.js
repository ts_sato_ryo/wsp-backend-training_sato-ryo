// @flow

import React from 'react';

import msg from '../../../../../../../commons/languages';
import Navigation from '../../../../../molecules/commons/Navigation';
import LinkListItem from '../../../../../atoms/LinkListItem';
import Wrapper from '../../../../../atoms/Wrapper';
import SearchField from '../../../../../molecules/commons/Fields/SearchField';

import './ExpenseType.scss';

const ROOT = 'mobile-app-pages-expense-type';

type State = {
  keyword: string,
};

type Props = {
  keyword: string,
  expenseTypeList: [],
  onBackClick: () => void,
  onSearchClick: (keyword: string) => void,
  onRowClick: (item: any) => void,
};

export default class SearchExpenseType extends React.Component<Props, State> {
  state = {
    keyword:
      (this.props.keyword && decodeURIComponent(this.props.keyword)) || '',
  };

  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({ keyword: e.target.value });
  };

  render() {
    return (
      <Wrapper className={ROOT}>
        <Navigation
          title={msg().Exp_Lbl_ExpenseType}
          onClickBack={this.props.onBackClick}
        />
        <div className="main-content">
          <SearchField
            placeHolder={msg().Com_Lbl_Search}
            iconClick={() => this.props.onSearchClick(this.state.keyword)}
            onChange={this.onChange}
            value={this.state.keyword}
          />

          <div className={`${ROOT}__result`}>
            {this.props.expenseTypeList.map((item, idx) => (
              <div key={idx} className={`${ROOT}__row`}>
                <LinkListItem
                  key={item.requestId}
                  className={`${ROOT}__item`}
                  onClick={() => this.props.onRowClick(item)}
                  noIcon={!item.isGroup}
                >
                  {item.code} - {item.name}
                </LinkListItem>
              </div>
            ))}
          </div>
        </div>
      </Wrapper>
    );
  }
}
