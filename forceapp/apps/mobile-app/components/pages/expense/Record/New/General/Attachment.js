// @flow
import React, { useState } from 'react';
import isEmpty from 'lodash/isEmpty';

import msg from '../../../../../../../commons/languages';
import TextButton from '../../../../../atoms/TextButton';
import Input from '../../../../../atoms/Fields/Input';
import Label from '../../../../../atoms/Label';
import Errors from '../../../../../atoms/Errors';
import FilePreview from '../../../../../molecules/commons/FilePreview';
import {
  MAX_FILE_SIZE,
  VALID_FILE_TYPES,
  type Base64FileList,
} from '../../../../../../../domain/models/exp/Receipt';
import { RECEIPT_TYPE } from '../../../../../../../domain/models/exp/Record';

import './Attachment.scss';

type Props = {
  receiptFileId: ?string,
  receiptId?: ?string,
  receiptData: ?string,
  fileName?: string,
  readOnly?: boolean,
  receiptConfig: string,
  errors: string[],
  getBase64files: (
    e: SyntheticInputEvent<HTMLInputElement>
  ) => Promise<Base64FileList>,
  uploadReceipts: (
    list: Base64FileList
  ) => Promise<{
    contentVersionId: string,
    contentDocumentId: string,
  }>,
  onChangeUpdateValues: ({
    receiptId: ?string,
    receiptFileId: ?string,
    receiptData: ?string,
  }) => void,
};

const ROOT = 'mobile-app-pages-expense-page-record-new-general-attachment';

const Attachment = (props: Props) => {
  const [isLargeFile, setIsLargeFile] = useState(false);
  const [isInvalidFileType, setIsInvalidFileType] = useState(false);
  const {
    receiptFileId,
    fileName,
    receiptId,
    receiptData,
    readOnly,
    receiptConfig,
    errors,
  } = props;

  const receiptOptional = receiptConfig === RECEIPT_TYPE.Optional;
  const receiptRequired = receiptConfig === RECEIPT_TYPE.Required;

  const handleAttachFile = (e: SyntheticInputEvent<HTMLInputElement>) => {
    props.getBase64files(e).then((base64Files) => {
      const isLarge = base64Files[0].size > MAX_FILE_SIZE;
      const isInvalidType = !VALID_FILE_TYPES.includes(base64Files[0].type);
      setIsLargeFile(isLarge);
      setIsInvalidFileType(isInvalidType);
      if (isLarge || isInvalidType) {
        return;
      }
      props.uploadReceipts(base64Files).then((res) => {
        let updateFileInfo;
        if (res) {
          updateFileInfo = {
            receiptId: res.contentDocumentId,
            receiptFileId: res.contentVersionId,
            receiptData: base64Files[0].data,
          };
        } else {
          updateFileInfo = {
            receiptId: null,
            receiptFileId: null,
            receiptData: null,
          };
        }
        props.onChangeUpdateValues(updateFileInfo);
      });
    });
  };

  const handleDeleteFile = () => {
    setIsLargeFile(false);
    setIsInvalidFileType(false);
    const resetFileInfo = {
      receiptId: null,
      receiptFileId: null,
      receiptData: null,
    };
    props.onChangeUpdateValues(resetFileInfo);
  };

  if (!receiptRequired && !receiptOptional) {
    return null;
  }

  const title = (
    <Label
      className={`${ROOT}__title`}
      text={msg().Exp_Lbl_Receipt}
      marked={receiptRequired}
    />
  );

  const errorMsg = isEmpty(errors) ? null : <Errors messages={errors} />;

  let attachmentContainer = null;
  if (readOnly) {
    attachmentContainer = (
      <FilePreview
        url={receiptData}
        rootClassName={ROOT}
        fileName={fileName}
        fileId={receiptId}
      />
    );
  } else if (!receiptFileId) {
    attachmentContainer = (
      <div className={ROOT}>
        <div className={`${ROOT}__wrapper`}>
          {isLargeFile && (
            <div className={`${ROOT}__error`}>
              {msg().Common_Err_MaxFileSize}
            </div>
          )}
          {isInvalidFileType && (
            <div className={`${ROOT}__error`}>
              {msg().Common_Err_InvalidType}
            </div>
          )}
          <label className={`${ROOT}__label`}>
            <Input
              className={`${ROOT}__input`}
              type="file"
              accept="image/*"
              capture="camera"
              onChange={(e) => {
                handleAttachFile(e);
                e.target.value = '';
              }}
              multiple={false}
            />
            {msg().Exp_Btn_AddReceipt}
          </label>
        </div>
      </div>
    );
  } else {
    attachmentContainer = (
      <div className={ROOT}>
        <FilePreview
          url={receiptData}
          rootClassName={`${ROOT}`}
          fileName={fileName}
          fileId={receiptId}
        />
        <TextButton onClick={handleDeleteFile} className={`${ROOT}__delete`}>
          {msg().Exp_Lbl_DeleteReceipt}
        </TextButton>
      </div>
    );
  }

  return (
    <>
      {title}
      {attachmentContainer}
      {errorMsg}
    </>
  );
};

export default Attachment;
