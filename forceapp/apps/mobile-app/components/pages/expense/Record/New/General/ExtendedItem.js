// @flow

import React from 'react';

import { isEmpty } from 'lodash';

import {
  type RecordItem,
  type Record,
} from '../../../../../../../domain/models/exp/Record';
import {
  getExtendedItemArray,
  type ExtendItemInfo,
} from '../../../../../../../domain/models/exp/ExtendedItem';
import msg from '../../../../../../../commons/languages';
import TextField from '../../../../../molecules/commons/Fields/TextField';
import SelectField from '../../../../../molecules/commons/Fields/SelectField';
import SFDateField from '../../../../../molecules/commons/Fields/SFDateField';
import DateUtil from '../../../../../../../commons/utils/DateUtil';

import './index.scss';
import SearchButtonField from '../../../../../molecules/commons/Fields/SearchButtonField';

const ROOT = 'mobile-app-pages-expense-page-record-new-general-extended-item';

type Props = {
  readOnly: boolean,
  item: RecordItem,
  setError: (string) => any,
  onChangeUpdateValues: ({}) => void,
  onChangeValue: (any) => any,
  onClickSearchCustomEI: (string, string, string, string) => void,
  saveFormValues: (Record) => boolean,
  record: Record,
  // Custom Hint
  activeHints: Array<string>,
  onClickHint: (string) => void,
};

export default class Recordform extends React.Component<Props> {
  showExtendedItem = (fieldStart: string) => {
    const valueItems = this.props.item;
    const extVals = [];
    for (let i = 1; i <= 10; i++) {
      const idx = `0${i}`.slice(-2);
      const id = valueItems[`${fieldStart}${idx}Id`];
      const info = valueItems[`${fieldStart}${idx}Info`];
      const selectedOptionName =
        valueItems[`${fieldStart}${idx}SelectedOptionName`];
      if (!isEmpty(id)) {
        extVals.push({ idx, id, info, selectedOptionName });
      }
    }
    return extVals;
  };

  buildOptionList(picklist: any) {
    const optionList = picklist.map((pick) => ({
      value: pick.value || '',
      label: pick.label,
    }));
    return [{ value: '', label: msg().Exp_Lbl_PleaseSelect }, ...optionList];
  }

  getCustomHintforEI = (id: string, info: ExtendItemInfo) => ({
    hintMsg: !this.props.readOnly ? info.description : '',
    isShowHint: this.props.activeHints.includes(id),
    onClickHint: () => this.props.onClickHint(id),
  });

  render() {
    const {
      setError,
      readOnly,
      onChangeValue,
      onChangeUpdateValues,
      item,
    } = this.props;

    return (
      <div className={ROOT}>
        {getExtendedItemArray(this.props.item)
          .filter((i) => i.id)
          .map(({ id, info, index }) => {
            if (!info) {
              return null;
            }

            let $field;
            switch (info.inputType) {
              case 'Text':
                $field = (
                  <TextField
                    key={id}
                    required={info.isRequired}
                    disabled={readOnly}
                    label={info.name}
                    errors={setError(`items.0.extendedItemText${index}Value`)}
                    onChange={(e) => {
                      const val = e.target.value;
                      onChangeUpdateValues({
                        [`items.0.extendedItemText${index}Value`]: val,
                        [`items.0.extendedItemText${index}Id`]: id,
                      });
                    }}
                    value={item[`extendedItemText${index}Value`] || ''}
                    {...this.getCustomHintforEI(id, info)}
                  />
                );
                break;

              case 'Picklist':
                $field = (
                  <SelectField
                    key={id}
                    disabled={readOnly}
                    errors={setError(
                      `items.0.extendedItemPicklist${index}Value`
                    )}
                    required={info.isRequired}
                    label={info.name}
                    options={this.buildOptionList(info.picklist)}
                    onChange={onChangeValue(
                      `items.0.extendedItemPicklist${index}Value`
                    )}
                    value={item[`extendedItemPicklist${index}Value`] || ''}
                    {...this.getCustomHintforEI(id, info)}
                  />
                );
                break;

              case 'Lookup':
                $field = (
                  <SearchButtonField
                    placeholder={msg().Admin_Lbl_Search}
                    disabled={readOnly}
                    required={info.isRequired}
                    errors={setError(`items.0.extendedItemLookup${index}Value`)}
                    onClick={() => {
                      this.props.saveFormValues(this.props.record);
                      this.props.onClickSearchCustomEI(
                        id,
                        info.extendedItemCustomId,
                        info.name,
                        index
                      );
                    }}
                    onClickDeleteButton={() => {
                      onChangeUpdateValues({
                        [`items.0.extendedItemLookup${index}Value`]: null,
                        [`items.0.extendedItemLookup${index}SelectedOptionName`]: null,
                      });
                    }}
                    value={
                      item[`extendedItemLookup${index}SelectedOptionName`] || ''
                    }
                    label={info.name}
                    {...this.getCustomHintforEI(id, info)}
                  />
                );
                break;

              case 'Date':
                const onChangeDateFieldValue = (date?: Date) => {
                  const updateValue = (date && DateUtil.fromDate(date)) || '';
                  onChangeUpdateValues({
                    [`items.0.extendedItemDate${index}Value`]: updateValue,
                    [`items.0.extendedItemDate${index}Id`]: id,
                  });
                };
                $field = (
                  <SFDateField
                    key={id}
                    required={info.isRequired}
                    disabled={readOnly}
                    label={info.name}
                    errors={setError(`items.0.extendedItemDate${index}Value`)}
                    onChange={(e, { date }) => {
                      onChangeDateFieldValue(date);
                    }}
                    value={item[`extendedItemDate${index}Value`] || ''}
                    useRemoveValueButton
                    onClickRemoveValueButton={() => onChangeDateFieldValue()}
                    {...this.getCustomHintforEI(id, info)}
                  />
                );
                break;

              default:
                $field = null;
            }

            return $field;
          })}
      </div>
    );
  }
}
