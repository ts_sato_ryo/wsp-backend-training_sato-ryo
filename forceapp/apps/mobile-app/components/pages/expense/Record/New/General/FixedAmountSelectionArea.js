// @flow

import React from 'react';

import { isEmpty, find } from 'lodash';

import { type Record } from '../../../../../../../domain/models/exp/Record';
import msg from '../../../../../../../commons/languages';
import SelectField from '../../../../../molecules/commons/Fields/SelectField';

import './index.scss';
import { calculateTax } from '../../../../../../../domain/models/exp/TaxType';
import FormatUtil from '../../../../../../../commons/utils/FormatUtil';

type Props = {
  values: Record,
  currencySymbol: string,
  readOnly: boolean,
  onChangeUpdateValues: ({}) => void,
  errors?: string[],
  fixedAllowanceOptionList: ?Array<Object>,
  rate: number,
  currencyDecimalPlace: number,
  decimalPlaces: number,
};

const ROOT =
  'mobile-app-pages-expense-page-record-new-general-fixed-amount-selection';

export default class FixedAmountSelectionArea extends React.Component<Props> {
  onSelectAmount = (value: number | string) => {
    const selectedFixedAllowanceOption = find(
      this.props.fixedAllowanceOptionList,
      { id: value }
    );
    const amount = selectedFixedAllowanceOption
      ? selectedFixedAllowanceOption.allowanceAmount
      : 0;
    const fixedAllowanceOptionId = selectedFixedAllowanceOption
      ? selectedFixedAllowanceOption.id
      : '';
    const fixedAllowanceOptionLabel = selectedFixedAllowanceOption
      ? selectedFixedAllowanceOption.label
      : '';

    const taxRes = calculateTax(
      this.props.rate,
      amount,
      this.props.currencyDecimalPlace
    );

    this.props.onChangeUpdateValues({
      amount,
      withoutTax: taxRes.amountWithoutTax,
      'items.0.amount': amount,
      'items.0.withoutTax': taxRes.amountWithoutTax,
      'items.0.gstVat': taxRes.gstVat,
      'items.0.fixedAllowanceOptionId': fixedAllowanceOptionId,
      'items.0.fixedAllowanceOptionLabel': fixedAllowanceOptionLabel,
    });
  };

  render() {
    const { values, fixedAllowanceOptionList } = this.props;
    const errors = this.props.errors || [];
    const isDisabledExpType =
      isEmpty(values.items[0].expTypeId) || this.props.readOnly;
    const optionList: Array<{
      label: string,
      value: string,
    }> = fixedAllowanceOptionList
      ? fixedAllowanceOptionList.map((amountOption) => {
          return {
            label: `${amountOption.label} ${
              this.props.currencySymbol
            }${FormatUtil.formatNumber(
              amountOption.allowanceAmount,
              this.props.decimalPlaces
            )}`,
            value: amountOption.id,
          };
        })
      : [{ label: '', value: '' }];

    optionList.unshift({
      label: '',
      value: '',
    });

    return (
      <div className={ROOT}>
        <SelectField
          required
          disabled={isDisabledExpType}
          label={msg().Exp_Lbl_AmountSelection}
          errors={errors}
          options={optionList}
          onChange={(e: any) => {
            this.onSelectAmount(e.target.value);
          }}
          value={values.items[0].fixedAllowanceOptionId}
        />
      </div>
    );
  }
}
