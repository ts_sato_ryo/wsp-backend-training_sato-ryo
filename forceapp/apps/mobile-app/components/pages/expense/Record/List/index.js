// @flow

import * as React from 'react';

import { type Record } from '../../../../../../domain/models/exp/Record';

import msg from '../../../../../../commons/languages';

import Wrapper from '../../../../atoms/Wrapper';
import TextButton from '../../../../atoms/TextButton';
import Navigation from '../../../../molecules/commons/Navigation';
import EmptyIcon from '../../../../molecules/commons/EmptyIcon';
import RecordSummaryListItem from '../../../../molecules/expense/RecordSummaryListItem';
import GlobalFooter from '../../../../../containers/organisms/expense/GlobalFooterContainer';
import ReportTypeSelectDialog from '../../../commons/ReportTypeSelect';
import { type AccountingPeriodOption } from '../../../../../../domain/models/exp/AccountingPeriod';
import { type ExpenseReportTypeList } from '../../../../../../domain/models/exp/expense-report-type/list';

import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-record-list';

type Props = $ReadOnly<{|
  userSetting: { currencySymbol: string, currencyDecimalPlaces: number },
  onClickRecord: (recordId?: ?string, recordType: string) => void,
  onClickCreateReport: () => void,
  onChangeAccountingPeriod: (string) => void,
  onChangeAccountingDate: (string) => void,
  onClickCloseReportTypeSelection: () => void,
  onClickNext: () => void,
  records: Record[],
  isShowReportTypeSelection: boolean,
  accountingPeriodList: Array<AccountingPeriodOption>,
  accountingDate: string,
  expReportTypeList: ExpenseReportTypeList,
|}>;

const RecordListIndex = (props: Props) => {
  const { userSetting, records, isShowReportTypeSelection } = props;

  return (
    <div className={ROOT}>
      <Wrapper>
        <Navigation
          title={msg().Exp_Lbl_RecordsList}
          actions={[
            <TextButton onClick={() => props.onClickCreateReport()}>
              {msg().Exp_Btn_SelectRecord}
            </TextButton>,
          ]}
        />
        <div className="main-content">
          <section>
            {records && records.length ? (
              records.map((record, idx) => (
                <RecordSummaryListItem
                  key={idx}
                  onClick={() =>
                    props.onClickRecord(record.recordId, record.recordType)
                  }
                  record={record}
                  currencyDecimalPlaces={userSetting.currencyDecimalPlaces}
                  currencySymbol={userSetting.currencySymbol}
                />
              ))
            ) : (
              <EmptyIcon
                className={`${ROOT}__empty-icon`}
                message={msg().Exp_Msg_NoRecordItem}
              />
            )}
          </section>
        </div>
        <GlobalFooter active="record" />
        <ReportTypeSelectDialog
          isShowDialog={isShowReportTypeSelection}
          accountingPeriodList={props.accountingPeriodList}
          accountingDate={props.accountingDate}
          expReportTypeList={props.expReportTypeList}
          onChangeAccountingPeriod={props.onChangeAccountingPeriod}
          onChangeAccountingDate={props.onChangeAccountingDate}
          onClickNext={props.onClickNext}
          onClickCloseDialog={props.onClickCloseReportTypeSelection}
        />
      </Wrapper>
    </div>
  );
};

export default RecordListIndex;
