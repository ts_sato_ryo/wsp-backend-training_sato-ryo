// @flow

import * as React from 'react';

import { type Record } from '../../../../../../../domain/models/exp/Record';

import msg from '../../../../../../../commons/languages';

import Button from '../../../../../atoms/Button';
import Wrapper from '../../../../../atoms/Wrapper';
import TextButton from '../../../../../atoms/TextButton';
import Navigation from '../../../../../molecules/commons/Navigation';
import EmptyIcon from '../../../../../molecules/commons/EmptyIcon';
import RecordSummarySelectListItem from '../../../../../molecules/expense/RecordSummarySelectListItem';

import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-record-list-select';

type Props = $ReadOnly<{|
  userSetting: { currencySymbol: string, currencyDecimalPlaces: number },
  canCreateReport: boolean,
  onClickItem: (?string) => void,
  onClickCancel: () => void,
  onClickCreateReport: () => void,
  flagsById: { [string]: boolean },
  records: Record[],
|}>;

const RecordListSelect = (props: Props) => {
  const { userSetting } = props;
  const isEmptyRecord = props.records.length === 0;

  return (
    <Wrapper className={ROOT}>
      <Navigation
        className={`${ROOT}__navigation`}
        title={msg().Exp_Lbl_RecordsListSelect}
        actions={[
          <TextButton
            className={`${ROOT}__cancel-button`}
            onClick={props.onClickCancel}
          >
            {msg().Com_Btn_Cancel}
          </TextButton>,
        ]}
      />
      <div className="main-content">
        <div>
          {isEmptyRecord ? (
            <EmptyIcon
              className={`${ROOT}__empty-icon`}
              message={msg().Exp_Msg_NoRecordItem}
            />
          ) : (
            props.records.map((record, idx) => (
              <RecordSummarySelectListItem
                key={idx}
                onClick={() => props.onClickItem(record.recordId)}
                isSelected={props.flagsById[record.recordId || ''] || false}
                record={record}
                currencyDecimalPlaces={userSetting.currencyDecimalPlaces}
                currencySymbol={userSetting.currencySymbol}
              />
            ))
          )}
        </div>
      </div>
      {!isEmptyRecord && (
        <div className={`${ROOT}__footer`}>
          <Button
            disabled={!props.canCreateReport}
            onClick={props.onClickCreateReport}
            variant="add"
            priority="secondary"
          >
            {msg().Exp_Lbl_CreateReport}
          </Button>
        </div>
      )}
    </Wrapper>
  );
};

export default RecordListSelect;
