// @flow

import * as React from 'react';

import './index.scss';

const ROOT = 'mobile-app-pages-report-detail-submit';

export type Props = $ReadOnly<{||}>;

const ReportSubmit = () => {
  return <div className={ROOT} />;
};

export default ReportSubmit;
