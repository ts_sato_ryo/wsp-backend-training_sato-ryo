// @flow

import React from 'react';

import { type Base64FileList } from '../../../../../../domain/models/exp/Receipt';

import FileUtil from '../../../../../../commons/utils/FileUtil';

import pdfIcon from '../../../../../../commons/images/pdfIcon.png';
import IconButton from '../../../../atoms/IconButton';
import colors from '../../../../../styles/variables/_colors.scss';
import './Preview.scss';

const ROOT = 'mobile-app-pages-expense-page-receipt-upload-preview';

type State = {
  index: number,
  isLandscape: boolean,
};

export type Props = {
  images: Base64FileList,
  onClickRemoveButton: (number) => void,
};

export default class Preview extends React.Component<Props, State> {
  previewImage: ?HTMLImageElement;

  state = {
    index: 0,
    isLandscape: true,
  };

  onClickRemoveButton = () => {
    this.props.onClickRemoveButton(this.state.index);
    if (this.state.index !== 0) {
      this.setState((prevState) => ({
        index: prevState.index - 1,
      }));
    }
  };

  onClickCarouselButton = (add: 1 | -1) => {
    const { index } = this.state;
    const { images } = this.props;

    let idx;
    if (index + add === -1) {
      idx = images.length - 1;
    } else if (index + add === images.length) {
      idx = 0;
    } else {
      idx = index + add;
    }
    this.setState({ index: idx });
  };

  adjustSize = () => {
    if (this.previewImage) {
      this.setState({
        isLandscape:
          this.previewImage.naturalWidth >= this.previewImage.naturalHeight,
      });
    }
  };

  render() {
    const { index, isLandscape } = this.state;
    const { images } = this.props;
    const b64Data = images[index].data;
    const isPDF = FileUtil.getB64FileExtension(b64Data, 'pdf');
    const imageWrapperClass = isLandscape
      ? `${ROOT}__image-landscape`
      : `${ROOT}__image-portrait`;

    return (
      <div className={ROOT}>
        <div className={`${ROOT}__main`}>
          <IconButton
            className={`${ROOT}__remove`}
            color={colors.textReverse}
            size="small"
            icon="close-copy"
            onClick={this.onClickRemoveButton}
          />
          {images.length > 1 && (
            <IconButton
              className={`${ROOT}__nav-left`}
              icon="chevronleft"
              color={colors.textReverse}
              size="middle"
              disabled={images.length === 1 || index === undefined}
              onClick={() => this.onClickCarouselButton(-1)}
            />
          )}
          <div className={imageWrapperClass}>
            <img
              ref={(c) => {
                this.previewImage = c;
              }}
              alt="file"
              src={isPDF ? pdfIcon : b64Data}
              onLoad={this.adjustSize}
            />
          </div>
          {images.length > 1 && (
            <IconButton
              className={`${ROOT}__nav-right`}
              icon="chevronright"
              size="middle"
              color={colors.textReverse}
              onClick={() => this.onClickCarouselButton(1)}
              disabled={images.length === 1 || index === undefined}
            />
          )}
        </div>
        <div className={`${ROOT}__index`}>
          {index + 1} / {images.length}
        </div>
      </div>
    );
  }
}
