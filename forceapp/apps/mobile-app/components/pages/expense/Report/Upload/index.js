// @flow
import React from 'react';
import { cloneDeep, some } from 'lodash';
import { Form } from 'formik';

import { type Base64FileList } from '../../../../../../domain/models/exp/Receipt';
import msg from '../../../../../../commons/languages';
import Button from '../../../../atoms/Button';
import Input from '../../../../atoms/Fields/Input';
import WrapperWithPermission from '../../../../organisms/commons/WrapperWithPermission';
import EmptyImageIcon from '../../../../molecules/commons/EmptyImageIcon';
import Navigation from '../../../../molecules/commons/Navigation';
import { type ErrorInfo } from '../../../../../../commons/utils/AppPermissionUtil';
import Preview from './Preview';
import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-receipt-upload';
const MAX_FILE_SIZE = 5242880;
const VALID_FILE_TYPES = [
  'image/jpeg',
  'image/png',
  'application/pdf',
  'image/gif',
];

type Props = {
  values: { files: Base64FileList },
  hasPermissionError: ?ErrorInfo,
  setFieldValue: (key: string, value: any) => void,
  handleSubmit: () => void,
  getBase64files: (
    e: SyntheticInputEvent<HTMLInputElement>
  ) => Promise<Array<File>>,
};

const ReceiptUploadPage = (props: Props) => {
  const { values, handleSubmit } = props;
  const hasImage = values.files[0];
  // methods
  const handleChange = () => (e: SyntheticInputEvent<HTMLInputElement>) => {
    const { files } = props.values;
    props.getBase64files(e).then((addFiles) => {
      Array.prototype.push.apply(addFiles, files);
      props.setFieldValue('files', addFiles);
    });
    e.target.value = '';
  };

  const remove = (index: number) => {
    const files = cloneDeep(props.values.files);
    files.splice(index, 1);
    props.setFieldValue('files', files);
  };

  const renderFileSizeMsg = (isLargeFile: boolean) =>
    isLargeFile ? (
      <div className={`${ROOT}__error`}>{msg().Common_Err_MaxFileSize}</div>
    ) : (
      <div>{msg().Exp_Lbl_MaxFileSize}</div>
    );

  // This validation will be removed when accept=".pdf,image/*" works fine for mobile
  const isInvalidType = some(
    values.files,
    (file) => VALID_FILE_TYPES.indexOf(file.type) === -1
  );
  const isLargeFile = some(values.files, (file) => {
    console.log(file.size);
    return file.size > MAX_FILE_SIZE;
  });
  const isCantSubmitButton = !hasImage || isInvalidType || isLargeFile;

  return (
    <WrapperWithPermission
      className={ROOT}
      hasPermissionError={props.hasPermissionError}
    >
      <Navigation title={msg().Exp_Lbl_AddReceipt} />
      <Form className="main-content">
        <div className={`${ROOT}-preview`}>
          {hasImage ? (
            <Preview images={values.files} onClickRemoveButton={remove} />
          ) : (
            <EmptyImageIcon className={`${ROOT}__empty`} />
          )}
        </div>
        {msg().Exp_Lbl_UploadReceiptImage}
        {renderFileSizeMsg(isLargeFile)}
        {isInvalidType && (
          <div className={`${ROOT}__error`}>{msg().Common_Err_InvalidType}</div>
        )}
        <div className={`${ROOT}__buttons`}>
          <label className={`${ROOT}__label`}>
            <Input
              className={`${ROOT}__input`}
              type="file"
              accept="image/*"
              capture="camera"
              onChange={handleChange()}
              multiple
            />
            {msg().Exp_Btn_AddReceipt}
          </label>
          <Button
            priority="primary"
            variant="neutral"
            type="submit"
            className={`${ROOT}__submit`}
            disabled={isCantSubmitButton}
            onClick={handleSubmit}
          >
            {msg().Exp_Btn_SubmitReceipt}
          </Button>
        </div>
      </Form>
    </WrapperWithPermission>
  );
};

export default ReceiptUploadPage;
