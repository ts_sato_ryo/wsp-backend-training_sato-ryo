// @flow
import React, { useState } from 'react';
import { find } from 'lodash';

import msg from '../../../../../../commons/languages';
import DateUtil from '../../../../../../commons/utils/DateUtil';

import Amount from '../../../../atoms/Amount';
import Wrapper from '../../../../atoms/Wrapper';
import Button from '../../../../atoms/Button';
import TextButton from '../../../../atoms/TextButton';
import Alert from '../../../../molecules/commons/Alert';
import Dialog from '../../../../molecules/commons/Dialog';
import Navigation from '../../../../molecules/commons/Navigation';
import STATUS from '../../../../../../domain/models/approval/request/Status';
import ViewItem from '../../../../molecules/commons/ViewItem';
import RecordSummaryListItem from '../../../../molecules/expense/RecordSummaryListItem';
import FilePreview from '../../../../molecules/commons/FilePreview/index';

import { type Record } from '../../../../../../domain/models/exp/Record';

import './index.scss';

const ROOT = 'mobile-app-pages-report-detail';

export type Props = $ReadOnly<{|
  reportNo: string,
  subject: string,
  status: string,
  accountingPeriod: string,
  accountingDate: string,
  useFileAttachment: boolean,
  attachedFileData?: string,
  fileName?: string,
  fileType?: string,
  attachedFileId?: ?string,
  reportTypeName: string,
  costCenterCode: string,
  costCenterName: string,
  jobCode: string,
  jobName: string,
  remarks: string,
  extendedItemTexts: Array<{|
    name: string,
    value: string,
  |}>,
  extendedItemPicklists: Array<{|
    name: string,
    value: string,
    picklist: Array<{ label: string, value: string }>,
  |}>,
  extendedItemLookup: Array<{|
    name: string,
    value: string,
    selectedOptionName: string,
  |}>,
  extendedItemDate: Array<{|
    name: string,
    value: string,
  |}>,
  totalAmountOfRecords: number,
  records: Record[],
  currencySymbol: string,
  currencyDecimalPlaces: number,
  isEditable: boolean,

  // Navigation
  onClickRecord: (recordId?: ?string, recordType: string) => void,
  onClickSubmit: () => void,
  onClickReportList: () => void,
  onClickDelete: () => void,
  onClickEdit: () => void,
|}>;

const renderAlert = (records: Record[]) =>
  records.length !== 0 ? null : (
    <Alert variant="warning" message={[msg().Exp_Err_SubmitReportNoRecords]} />
  );

const renderExtendedTextItems = (
  items: Array<{|
    name: string,
    value: string,
  |}>
) => items.map((item) => <ViewItem label={item.name}>{item.value}</ViewItem>);

const renderExtendedPicklistItems = (
  items: Array<{|
    name: string,
    value: string,
    picklist: Array<{ label: string, value: string }>,
  |}>
) =>
  items.map((item) => {
    const val = item.value;
    const pickListItem = val ? find(item.picklist, { value: val }) : null;
    return (
      <ViewItem label={item.name}>
        {pickListItem ? pickListItem.label : ''}
      </ViewItem>
    );
  });

const renderExtendedLookupItems = (
  items: Array<{|
    name: string,
    value: string,
    selectedOptionName: string,
  |}>
) =>
  items.map((item) => (
    <ViewItem label={item.name}>
      {item.value ? `${item.value} - ${item.selectedOptionName}` : ''}
    </ViewItem>
  ));

const renderExtendedDateItems = (
  items: Array<{|
    name: string,
    value: string,
  |}>
) =>
  items.map((item) => (
    <ViewItem label={item.name}>{DateUtil.dateFormat(item.value)}</ViewItem>
  ));

const ReportDetail = (props: Props) => {
  const [isOpenedDeleteDialog, setIsOpenedDeleteDialog] = useState(false);
  const openedDeleteDialog = () => setIsOpenedDeleteDialog(true);
  const closeDeleteDialog = () => setIsOpenedDeleteDialog(false);
  const { records = [], isEditable } = props;
  const isDisableSubmitButton = !isEditable || records.length === 0;

  return (
    <Wrapper>
      <div className={ROOT}>
        <div className={`${ROOT}__heading`}>
          <Navigation
            title={msg().Exp_Lbl_Report}
            backButtonLabel={msg().Exp_Btn_ReportList}
            onClickBack={props.onClickReportList}
            actions={[
              <TextButton
                onClick={openedDeleteDialog}
                disabled={!props.isEditable}
              >
                {msg().Com_Btn_Delete}
              </TextButton>,
              <TextButton
                onClick={props.onClickEdit}
                disabled={!props.isEditable}
              >
                {msg().Com_Btn_Edit}
              </TextButton>,
            ]}
          />
        </div>
        <div className="main-content">
          {renderAlert(records)}
          <div className={`${ROOT}__report`}>
            <div className={`${ROOT}__headline`}>
              <div className={`${ROOT}__title heading-2`}>
                {msg().Exp_Lbl_Report}
              </div>
              <span className={`${ROOT}__report-no`}>{props.reportNo}</span>
            </div>
            <ViewItem label={msg().Exp_Clbl_ReportTitle}>
              {props.subject}
            </ViewItem>
            <ViewItem label={msg().Exp_Lbl_Status}>
              {props.status === STATUS.Canceled
                ? STATUS.Rejected
                : props.status}
            </ViewItem>
            {props.accountingPeriod && (
              <ViewItem label={msg().Exp_Clbl_AccountingPeriod}>
                {props.accountingPeriod}
              </ViewItem>
            )}
            <ViewItem label={msg().Exp_Clbl_RecordDate}>
              {DateUtil.formatYMD(props.accountingDate)}
            </ViewItem>
            <ViewItem label={msg().Exp_Clbl_ReportType}>
              {props.reportTypeName}
            </ViewItem>
            <ViewItem label={msg().Exp_Clbl_CostCenter}>
              {props.costCenterCode
                ? `${props.costCenterCode} - ${props.costCenterName}`
                : ''}
            </ViewItem>
            <ViewItem label={msg().Exp_Lbl_Job}>
              {props.jobCode ? `${props.jobCode} - ${props.jobName}` : ''}
            </ViewItem>
            {renderExtendedTextItems(props.extendedItemTexts)}
            {renderExtendedPicklistItems(props.extendedItemPicklists)}
            {renderExtendedLookupItems(props.extendedItemLookup)}
            {renderExtendedDateItems(props.extendedItemDate)}
            <ViewItem label={msg().Exp_Clbl_ReportRemarks}>
              {props.remarks}
            </ViewItem>
            {props.useFileAttachment ? (
              <FilePreview
                url={props.attachedFileData}
                fileId={props.attachedFileId}
                label={msg().Exp_Lbl_AttachedFile}
                fileName={props.fileName}
              />
            ) : null}
          </div>
          <div className={`${ROOT}__records`}>
            <div className={`${ROOT}__headline`}>
              <div className={`${ROOT}__title heading-2`}>
                {msg().Exp_Lbl_Records}
              </div>
              <div className={`${ROOT}__subtitle heading-2`}>
                <div className={`${ROOT}__total`}>
                  {msg().Exp_Lbl_TotalLong}
                </div>
                <Amount
                  amount={props.totalAmountOfRecords}
                  symbol={props.currencySymbol}
                  decimalPlaces={props.currencyDecimalPlaces}
                />
              </div>
            </div>
            {records.map((record) => (
              <RecordSummaryListItem
                key={record.recordId}
                onClick={() =>
                  props.onClickRecord(record.recordId, record.recordType)
                }
                record={record}
                currencySymbol={props.currencySymbol}
                currencyDecimalPlaces={props.currencyDecimalPlaces}
              />
            ))}
          </div>
          <div className={`${ROOT}__actions`}>
            <Button
              className={`${ROOT}__actions-button`}
              onClick={props.onClickSubmit}
              priority="primary"
              variant="neutral"
              disabled={isDisableSubmitButton}
            >
              {msg().Exp_Btn_Submit}
            </Button>
          </div>
        </div>
        <Dialog
          isOpened={isOpenedDeleteDialog}
          title={msg().Exp_Msg_ConfirmDelete}
          content={
            <div className={`${ROOT}__dialog`}>
              <p className={`${ROOT}__item`}>{msg().Exp_Lbl_Title}:</p>
              <p className={`${ROOT}__sub-item`}>{props.subject}</p>
              <p className={`${ROOT}__item`}>{msg().Exp_Lbl_TotalAmount}:</p>
              <Amount
                className={`${ROOT}__amount`}
                amount={props.totalAmountOfRecords}
                symbol={props.currencySymbol}
                decimalPlaces={props.currencyDecimalPlaces}
              />
            </div>
          }
          leftButtonLabel={msg().Com_Btn_Cancel}
          rightButtonLabel={msg().Com_Btn_Delete}
          onClickLeftButton={closeDeleteDialog}
          onClickRightButton={props.onClickDelete}
          onClickCloseButton={closeDeleteDialog}
        />
      </div>
    </Wrapper>
  );
};

export default ReportDetail;
