// @flow

import * as React from 'react';

import { Form } from 'formik';
import _ from 'lodash';
import moment from 'moment';
import { updateValues } from '../../../../../../commons/utils/FormikUtils';
import msg from '../../../../../../commons/languages';

import { type Base64FileList } from '../../../../../../domain/models/exp/Receipt';
import { type Report } from '../../../../../../domain/models/exp/Report';
import { type CustomHint } from '../../../../../../domain/models/exp/CustomHint';
import Navigation from '../../../../molecules/commons/Navigation';
import Alert from '../../../../molecules/commons/Alert';

import TextButton from '../../../../atoms/TextButton';
import Wrapper from '../../../../atoms/Wrapper';
import SFDateField from '../../../../molecules/commons/Fields/SFDateField';
import SelectField from '../../../../molecules/commons/Fields/SelectField';
import TextField from '../../../../molecules/commons/Fields/TextField';
import SearchButtonField from '../../../../molecules/commons/Fields/SearchButtonField';
import ViewItem from '../../../../molecules/commons/ViewItem';
import {
  getEIsOnly,
  getExtendedItemArray,
  type ExtendItemInfo,
} from '../../../../../../domain/models/exp/ExtendedItem';
import FormatUtil from '../../../../../../commons/utils/FormatUtil';
import DateUtil from '../../../../../../commons/utils/DateUtil';
import Attachment from './Attachment';

import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-report-edit';

type Props = {
  accountingPeriodList: Array<Object>,
  userSetting: Object,
  expReportTypeList: Object,
  values: {
    ui: Object,
    report: Report,
  },
  errors: string[],
  touched: Object,
  setValues: (values: $PropertyType<Props, 'values'>) => void,
  setTouched: (value: Object) => void,
  saveReportFormValues: (Report) => boolean,
  onClickCancelButton: (Report) => void,
  handleSubmit: () => void,
  onClickSearchCostCenter: (string) => void,
  onClickSearchJob: (string) => void,
  reportId: string,
  onClickSearchCustomEI: (string, string, string, string) => void,
  getBase64files: (
    e: SyntheticInputEvent<HTMLInputElement>
  ) => Promise<Base64FileList>,
  uploadReceipts: (
    list: Base64FileList
  ) => Promise<{
    contentVersionId: string,
    contentDocumentId: string,
  }>,
  // Custom Hint
  activeHints: Array<string>,
  customHints: CustomHint,
  onClickHint: (string) => void,
};

type State = {
  errorMessages: string[],
  isLargeFile: boolean,
  isInvalidFileType: boolean,
};
export default class ReportEditPage extends React.Component<Props, State> {
  onChangeExpReportType: Function;
  onChangeReportUnTypeSafely: Function;
  onChangeReport: Function;
  onClickDeleteCCButton: Function;
  onClickDeleteJobButton: Function;

  constructor(props: Props) {
    super(props);
    this.state = {
      errorMessages: [],
      isLargeFile: false,
      isInvalidFileType: false,
    };
    this.onChangeExpReportType = this.onChangeExpReportType.bind(this);
    this.onChangeReportUnTypeSafely = this.onChangeReportUnTypeSafely.bind(
      this
    );
    this.onChangeReport = this.onChangeReport.bind(this);
    this.onClickDeleteCCButton = this.onClickDeleteCCButton.bind(this);
    this.onClickDeleteJobButton = this.onClickDeleteJobButton.bind(this);
  }

  componentDidUpdate(prevProps: Props) {
    const { expReportTypeList, values } = this.props;
    if (
      prevProps.expReportTypeList !== expReportTypeList ||
      prevProps.values.report.records !== values.report.records ||
      prevProps.values.report.expReportTypeId !== values.report.expReportTypeId
    ) {
      const id = values.report.expReportTypeId || '';
      const nextId = !values.report.reportId
        ? _.get(expReportTypeList, '0.id')
        : '';
      const reportTypeId =
        expReportTypeList.findIndex((x) => x.id === id) > -1 ? id : nextId;
      this.setExpenseTypeError(reportTypeId);
    }
  }

  onChangeExpReportType(e: SyntheticEvent<*>) {
    const selectedReportType = (_.find(this.props.expReportTypeList, {
      id: e.currentTarget.value,
    }): Object);
    const originalReportValues = this.props.values.report;
    // if cost center/job is unused, remove the existing value
    const isJobRequired = selectedReportType.isJobRequired === 'REQUIRED';
    const isCostCenterRequired =
      selectedReportType.isCostCenterRequired === 'REQUIRED';
    let initialJobData = {};
    if (selectedReportType.isJobRequired === 'UNUSED') {
      initialJobData = {
        jobId: null,
        jobCode: '',
        jobName: null,
      };
    }

    let initialCostCenterData = {};
    if (selectedReportType.isCostCenterRequired === 'UNUSED') {
      initialCostCenterData = {
        costCenterName: null,
        costCenterCode: '',
        costCenterHistoryId: null,
      };
    }

    const updatedReceiptData = {
      useFileAttachment: selectedReportType.useFileAttachment,
    };
    if (!selectedReportType.useFileAttachment) {
      _.assign(updatedReceiptData, {
        attachedFileVerId: null,
        attachedFileId: null,
        attachedFileData: null,
      });
    }

    this.setExpenseTypeError(selectedReportType.id);
    const updatedExtendedData = getEIsOnly(
      selectedReportType,
      originalReportValues
    );
    const newReportOfValues = [...Array(10).keys()]
      .map((index) => `${index + 1}`.padStart(2, '0'))
      .reduce(
        (report: Object) => ({
          ...report,
          expReportTypeId: e.currentTarget.value,
          ...initialJobData,
          ...initialCostCenterData,
          ...updatedExtendedData,
          ...updatedReceiptData,
          isCostCenterRequired,
          isJobRequired,
        }),
        this.props.values.report
      );
    const { touched, values } = updateValues(
      this.props.values,
      this.props.touched,
      {
        report: newReportOfValues,
      }
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  }

  onChangeReportUnTypeSafely(key: string) {
    return (e: SyntheticEvent<*>) => {
      const { values, touched } = updateValues(
        this.props.values,
        this.props.touched,
        {
          report: {
            ...this.props.values.report,
            [key]: e.currentTarget.value,
          },
        }
      );
      this.props.setValues(values);
      this.props.setTouched(touched);
    };
  }

  onChangeReport(key: $Keys<Report>) {
    return (e: SyntheticEvent<*>) => {
      this.onChangeReportUnTypeSafely(key)(e);

      if (key === 'accountingPeriodId') {
        const selectedAccountingPeriod =
          _.find(this.props.accountingPeriodList, {
            value: e.currentTarget.value,
          }) || {};
        const { touched, values } = updateValues(
          this.props.values,
          this.props.touched,
          {
            report: {
              ...this.props.values.report,
              accountingPeriodId: selectedAccountingPeriod.value,
              accountingDate: selectedAccountingPeriod.recordDate,
            },
          }
        );
        this.props.setValues(values);
        this.props.setTouched(touched);
      }
    };
  }

  onClickDeleteCCButton() {
    const { touched, values } = updateValues(
      this.props.values,
      this.props.touched,
      {
        report: {
          ...this.props.values.report,
          costCenterName: null,
          costCenterHistoryId: null,
        },
      }
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  }

  onClickDeleteJobButton() {
    const { touched, values } = updateValues(
      this.props.values,
      this.props.touched,
      {
        report: {
          ...this.props.values.report,
          jobName: null,
          jobId: null,
        },
      }
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  }

  onClickDeleteCustomEIButton(index: string) {
    const { touched, values } = updateValues(
      this.props.values,
      this.props.touched,
      {
        report: {
          ...this.props.values.report,
          [`extendedItemLookup${index}Value`]: null,
          [`extendedItemLookup${index}SelectedOptionName`]: null,
        },
      }
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  }

  updateReportValues = (updateObj: Object) => {
    const { values, touched } = updateValues(
      this.props.values,
      this.props.touched,
      {
        report: {
          ...this.props.values.report,
          ...updateObj,
        },
      }
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  };

  handleAttachFile = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const MAX_FILE_SIZE = 5242880;
    const VALID_FILE_TYPES = [
      'image/jpeg',
      'image/png',
      'application/pdf',
      'image/gif',
    ];
    this.props.getBase64files(e).then((base64Files) => {
      const isLargeFile = base64Files[0].size > MAX_FILE_SIZE;
      const isInvalidFileType = !VALID_FILE_TYPES.includes(base64Files[0].type);
      this.setState({ isLargeFile, isInvalidFileType });

      if (isLargeFile || isInvalidFileType) {
        return;
      }

      this.props.uploadReceipts(base64Files).then((res) => {
        let updateFileInfo;
        if (res) {
          updateFileInfo = {
            attachedFileId: res.contentDocumentId,
            attachedFileVerId: res.contentVersionId,
            attachedFileData: base64Files[0].data,
          };
        } else {
          updateFileInfo = {
            attachedFileId: null,
            attachedFileVerId: null,
            attachedFileData: null,
          };
        }
        this.updateReportValues(updateFileInfo);
      });
    });
  };

  handleDeleteFile = () => {
    this.setState({ isLargeFile: false, isInvalidFileType: false });
    const resetFileInfo = {
      attachedFileId: null,
      attachedFileVerId: null,
      attachedFileData: null,
    };
    this.updateReportValues(resetFileInfo);
  };

  setExpenseTypeError(id: string) {
    const {
      expReportTypeList,
      values: {
        report: { records },
      },
    } = this.props;

    const selectedReportType = _.find(expReportTypeList, { id });
    const availableExpTypes =
      (selectedReportType && selectedReportType.expTypeIds) || [];
    const recordList = records ? records.map((x) => x.items[0]) : [];

    const usedExpType = recordList.map(({ expTypeId, expTypeName }) => ({
      expTypeId,
      expTypeName,
    }));

    const invalidExpTypes = usedExpType
      .filter((x) => availableExpTypes.indexOf(x.expTypeId) < 0)
      .map((x) => x.expTypeName);

    const expenseTypeError = `${
      msg().Exp_Warn_InvalidRecordExpenseTypeForReportTypeMobile
    } ${invalidExpTypes.join(', ')}`;

    this.setState({
      errorMessages: invalidExpTypes.length > 0 ? [expenseTypeError] : [],
    });
  }

  handleDateChange = (date: Date | '', key: string) => {
    const formattedDate = (date && moment(date).format('YYYY-MM-DD')) || date;
    const { touched, values } = updateValues(
      this.props.values,
      this.props.touched,
      {
        report: {
          ...this.props.values.report,
          [key]: formattedDate,
        },
      }
    );
    this.props.setValues(values);
    this.props.setTouched(touched);
  };

  setError(field: string): string[] {
    const error: string = _.get(this.props.errors, field);
    const isFieldTouched: boolean = _.get(this.props.touched, field);
    return error && isFieldTouched ? [error] : [];
  }

  buildOptionList(
    picklist: Array<{ value: string, label: string }>
  ): Array<{ value: string | number | null, label: string }> {
    const optionList = picklist.map((pick) => ({
      value: pick.value || '',
      label: pick.label,
    }));
    return [{ value: '', label: msg().Exp_Lbl_PleaseSelect }, ...optionList];
  }

  getExtendedItemInfo(range: string[], source: Object, fieldPrefix: string) {
    return range.reduce(
      (acc, index) =>
        _.isNil(source[`${fieldPrefix}${index}Id`])
          ? acc
          : [
              ...acc,
              {
                id: source[`${fieldPrefix}${index}Id`],
                info: source[`${fieldPrefix}${index}Info`],
                value: source[`${fieldPrefix}${index}Value`],
                idx: index,
              },
            ],
      []
    );
  }

  getReportTypeList() {
    let rtl = this.props.expReportTypeList;
    const selectedReportTypeId = this.props.values.report.expReportTypeId;
    const reportId = this.props.values.report.reportId;
    if (reportId && rtl.findIndex((x) => x.id === selectedReportTypeId) < 0) {
      rtl = [{ id: '', name: '' }].concat(rtl);
    }
    const t = (_.map(rtl, (o: any) => {
      return {
        value: o.id,
        label: o.name,
      };
    }): Object[]);
    return t;
  }

  getCustomHintProps = (fieldName: string, disabled?: boolean) => ({
    hintMsg: !disabled ? this.props.customHints[fieldName] : '',
    isShowHint: this.props.activeHints.includes(fieldName),
    onClickHint: () => this.props.onClickHint(fieldName),
  });

  getCustomHintforEI = (id: string, info: ExtendItemInfo) => ({
    hintMsg: info.description,
    isShowHint: this.props.activeHints.includes(id),
    onClickHint: () => this.props.onClickHint(id),
  });

  render() {
    const report = this.props.values.report;

    const selectedAccountingPeriod = report.accountingPeriodId
      ? _.find(this.props.accountingPeriodList, {
          value: report.accountingPeriodId,
        }) || {}
      : {};

    const selectedReportType = (_.find(this.props.expReportTypeList, {
      id: report.expReportTypeId,
    }): Object);

    const isCostCenterVisible =
      selectedReportType &&
      selectedReportType.isCostCenterRequired !== 'UNUSED';
    const isCostCenterRequired =
      isCostCenterVisible &&
      selectedReportType.isCostCenterRequired === 'REQUIRED';

    const isJobVisible =
      selectedReportType && selectedReportType.isJobRequired !== 'UNUSED';
    const isJobRequired =
      isJobVisible && selectedReportType.isJobRequired === 'REQUIRED';

    const isFileAttachmentVisible =
      selectedReportType && selectedReportType.useFileAttachment;
    const hasError = this.state.errorMessages.length > 0;
    const optionsList = _.cloneDeep(this.props.accountingPeriodList);
    optionsList.unshift(
      <option key="default-blank-option" value="" className="is-hidden" />
    );

    return (
      <Wrapper className={ROOT}>
        <Navigation
          className={`${ROOT}__navigation`}
          title={
            this.props.reportId && this.props.reportId !== 'null'
              ? msg().Exp_Lbl_ReportEdit
              : msg().Exp_Lbl_CreateReport
          }
          backButtonLabel={
            this.props.reportId && this.props.reportId !== 'null'
              ? msg().Com_Btn_Cancel
              : msg().Exp_Lbl_BackToRecordSelection
          }
          onClickBack={() => {
            this.props.onClickCancelButton(report);
          }}
          actions={[
            <TextButton
              type="submit"
              onClick={this.props.handleSubmit}
              disabled={hasError}
            >
              {msg().Com_Btn_Save}
            </TextButton>,
          ]}
        />
        <Form className="main-content">
          {this.state.errorMessages.length > 0 && (
            <Alert
              className={`${ROOT}__alert`}
              variant="warning"
              message={this.state.errorMessages}
            />
          )}

          <section className={`${ROOT}__input`}>
            <TextField
              required
              label={msg().Exp_Clbl_ReportTitle}
              onChange={this.onChangeReport('subject')}
              errors={this.setError('report.subject')}
              value={report.subject}
            />
          </section>

          {this.props.accountingPeriodList.length > 0 && (
            <div>
              <section className={`${ROOT}__input option-hidden`}>
                <SelectField
                  required
                  label={msg().Exp_Clbl_AccountingPeriod}
                  errors={this.setError('report.accountingPeriodId')}
                  onChange={this.onChangeReport('accountingPeriodId')}
                  options={optionsList}
                  value={selectedAccountingPeriod.value}
                  {...this.getCustomHintProps('reportHeaderAccountingPeriod')}
                />
              </section>

              <section className={`${ROOT}__input`}>
                <ViewItem label={msg().Exp_Clbl_RecordDate}>
                  {DateUtil.format(
                    selectedAccountingPeriod.recordDate,
                    'YYYY/MM/DD'
                  )}
                </ViewItem>
              </section>
            </div>
          )}

          {
            <section className={`${ROOT}__input`}>
              <SelectField
                required
                label={msg().Exp_Clbl_ReportType}
                errors={this.setError('report.expReportTypeId')}
                onChange={this.onChangeExpReportType}
                options={this.getReportTypeList()}
                value={report.expReportTypeId}
                {...this.getCustomHintProps('reportHeaderReportType')}
              />
            </section>
          }

          {this.props.accountingPeriodList.length < 1 && (
            <section className={`${ROOT}__input`}>
              <SFDateField
                required
                label={msg().Exp_Clbl_RecordDate}
                errors={this.setError('report.accountingDate')}
                onChange={(e, { date }) =>
                  this.handleDateChange(date, 'accountingDate')
                }
                value={report.accountingDate}
                {...this.getCustomHintProps('reportHeaderRecordDate')}
              />
            </section>
          )}

          <section className={`${ROOT}__input`}>
            <ViewItem label={msg().Exp_Lbl_TotalAmount}>
              <p className={`${ROOT}__amount-text`}>
                {this.props.userSetting.currencySymbol}{' '}
                {FormatUtil.formatNumber(report.totalAmount)}
              </p>
            </ViewItem>
          </section>

          {/* display cost center based on admin setting */}
          {isCostCenterVisible && (
            <section className={`${ROOT}__input`}>
              <SearchButtonField
                required={isCostCenterRequired}
                placeholder={msg().Admin_Lbl_Search}
                disabled={_.isEmpty(report.accountingDate)}
                onClick={() => {
                  this.props.saveReportFormValues(report);
                  this.props.onClickSearchCostCenter(report.accountingDate);
                }}
                onClickDeleteButton={this.onClickDeleteCCButton}
                value={report.costCenterName || ''}
                label={msg().Exp_Clbl_CostCenter}
                errors={this.setError('report.costCenterName')}
                {...this.getCustomHintProps('reportHeaderCostCenter')}
              />
            </section>
          )}

          {/* display job based on admin setting */}
          {isJobVisible && (
            <section className={`${ROOT}__input`}>
              <SearchButtonField
                required={isJobRequired}
                disabled={_.isEmpty(report.accountingDate)}
                placeholder={msg().Admin_Lbl_Search}
                onClick={() => {
                  this.props.saveReportFormValues(report);
                  this.props.onClickSearchJob(report.accountingDate);
                }}
                onClickDeleteButton={this.onClickDeleteJobButton}
                value={report.jobName || ''}
                label={msg().Exp_Lbl_Job}
                errors={this.setError('report.jobId')}
                {...this.getCustomHintProps('reportHeaderJob')}
              />
            </section>
          )}

          {getExtendedItemArray(this.props.values.report)
            .filter((i) => i.id)
            .map(({ id, info, value, index, name }) => {
              if (!info) {
                return null;
              }

              let $field;
              switch (info.inputType) {
                case 'Text':
                  $field = (
                    <section className={`${ROOT}__input`} key={id}>
                      <TextField
                        required={info.isRequired}
                        label={info.name}
                        errors={this.setError(
                          `report.extendedItemText${index}Value`
                        )}
                        onChange={this.onChangeReportUnTypeSafely(
                          `extendedItemText${index}Value`
                        )}
                        value={value}
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;
                case 'Picklist':
                  $field = (
                    <section className={`${ROOT}__input`} key={id}>
                      <SelectField
                        errors={this.setError(
                          `report.extendedItemPicklist${index}Value`
                        )}
                        required={info.isRequired}
                        label={info.name}
                        options={this.buildOptionList(info.picklist)}
                        onChange={this.onChangeReportUnTypeSafely(
                          `extendedItemPicklist${index}Value`
                        )}
                        value={value || ''}
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;
                case 'Lookup':
                  $field = (
                    <section className={`${ROOT}__input`} key={id}>
                      <SearchButtonField
                        placeholder={msg().Admin_Lbl_Search}
                        required={info.isRequired}
                        errors={this.setError(
                          `report.extendedItemLookup${index}Value`
                        )}
                        onClick={() => {
                          this.props.saveReportFormValues(report);
                          this.props.onClickSearchCustomEI(
                            id,
                            info.extendedItemCustomId,
                            info.name,
                            index
                          );
                        }}
                        onClickDeleteButton={() =>
                          this.onClickDeleteCustomEIButton(index)
                        }
                        value={name || ''}
                        label={info.name}
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;
                case 'Date':
                  const onChangeDateValue = (date) => {
                    this.handleDateChange(
                      date,
                      `extendedItemDate${index}Value`
                    );
                  };
                  $field = (
                    <section className={`${ROOT}__input`} key={id}>
                      <SFDateField
                        key={id}
                        required={info.isRequired}
                        label={info.name}
                        errors={this.setError(
                          `report.extendedItemDate${index}Value`
                        )}
                        onChange={(e, { date }) => onChangeDateValue(date)}
                        value={value || ''}
                        useRemoveValueButton
                        onClickRemoveValueButton={() => onChangeDateValue('')}
                        {...this.getCustomHintforEI(id, info)}
                      />
                    </section>
                  );
                  break;
                default:
                  $field = null;
              }

              return $field;
            })}

          <section className={`${ROOT}__input`}>
            <TextField
              label={msg().Exp_Clbl_ReportRemarks}
              onChange={this.onChangeReport('remarks')}
              errors={this.setError('report.remarks')}
              value={report.remarks}
              {...this.getCustomHintProps('reportHeaderRemarks')}
            />
          </section>

          {isFileAttachmentVisible && (
            <section className={`${ROOT}__input`}>
              <Attachment
                attachedFileId={report.attachedFileId}
                attachedFileData={report.attachedFileData}
                fileName={report.fileName}
                isLargeFile={this.state.isLargeFile}
                isInvalidFileType={this.state.isInvalidFileType}
                handleAttachFile={this.handleAttachFile}
                handleDeleteFile={this.handleDeleteFile}
              />
            </section>
          )}
        </Form>
      </Wrapper>
    );
  }
}
