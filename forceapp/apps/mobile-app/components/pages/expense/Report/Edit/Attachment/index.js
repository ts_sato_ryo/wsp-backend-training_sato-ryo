// @flow
import React from 'react';

import msg from '../../../../../../../commons/languages';
import TextButton from '../../../../../atoms/TextButton';
import Input from '../../../../../atoms/Fields/Input';
import FilePreview from '../../../../../molecules/commons/FilePreview/index';

import './index.scss';

type Props = {
  attachedFileId: ?string,
  attachedFileData: ?string,
  fileName?: string,
  isLargeFile: boolean,
  isInvalidFileType: boolean,
  handleAttachFile: (SyntheticInputEvent<HTMLInputElement>) => void,
  handleDeleteFile: () => void,
};

const ROOT = 'mobile-app-pages-expense-page-report-edit-attachment';

const Attachment = (props: Props) => {
  const {
    attachedFileId,
    attachedFileData,
    fileName,
    isLargeFile,
    isInvalidFileType,
  } = props;

  let attachmentContainer = null;

  if (!attachedFileId) {
    attachmentContainer = (
      <div className={ROOT}>
        <span className={`${ROOT}__title`}>{msg().Exp_Lbl_AttachedFile}</span>
        <div className={`${ROOT}__wrapper`}>
          {isLargeFile && (
            <div className={`${ROOT}__error`}>
              {msg().Common_Err_MaxFileSize}
            </div>
          )}

          {isInvalidFileType && (
            <div className={`${ROOT}__error`}>
              {msg().Common_Err_InvalidType}
            </div>
          )}

          <label className={`${ROOT}__label`}>
            <Input
              className={`${ROOT}__input`}
              type="file"
              accept="image/*"
              capture="camera"
              onChange={(e) => {
                props.handleAttachFile(e);
                e.target.value = '';
              }}
              multiple={false}
            />
            {msg().Exp_Btn_UploadFile}
          </label>
        </div>
      </div>
    );
  } else {
    attachmentContainer = (
      <div className={ROOT}>
        <FilePreview
          url={attachedFileData}
          fileId={attachedFileId}
          label={msg().Exp_Lbl_AttachedFile}
          rootClassName={`${ROOT}`}
          fileName={fileName}
        ></FilePreview>

        <TextButton
          onClick={props.handleDeleteFile}
          className={`${ROOT}__delete`}
        >
          {msg().Com_Btn_Delete}
        </TextButton>
      </div>
    );
  }

  return attachmentContainer;
};

export default Attachment;
