// @flow
import React, { useState } from 'react';

import msg from '../../../../../../commons/languages';
import IconButton from '../../../../atoms/IconButton';
import EmptyIcon from '../../../../molecules/commons/EmptyIcon';
import Navigation from '../../../../molecules/commons/Navigation';
import { type State as UserSetting } from '../../../../../../commons/reducers/userSetting';
import { type ErrorInfo } from '../../../../../../commons/utils/AppPermissionUtil';
import ReportSummaryListItem from '../../../../molecules/expense/ReportSummaryListItem';
import GlobalFooter from '../../../../../containers/organisms/expense/GlobalFooterContainer';
import WrapperWithPermission from '../../../../organisms/commons/WrapperWithPermission';
import InfoDialog from './infoDialog';
import './index.scss';

const ROOT = 'mobile-app-pages-expense-page-report-list';

type Props = {
  openDetail: (id: string) => void,
  userSetting: UserSetting,
  reportList: [],
  hasPermissionError: ?ErrorInfo,
};
const ReportListPage = (props: Props) => {
  const [isShowInfo, setShowInfo] = useState(false);
  const { reportList, userSetting } = props;
  const openDetail = (id: string) => {
    return () => {
      props.openDetail(id);
    };
  };

  return (
    <WrapperWithPermission
      className={ROOT}
      hasPermissionError={props.hasPermissionError}
    >
      <Navigation title={msg().Exp_Lbl_Reports} />
      <div className="main-content">
        <IconButton
          className={`${ROOT}__info`}
          icon="info_alt"
          size="medium"
          onClick={() => setShowInfo(true)}
        />
        {reportList.map((item, idx) => {
          return (
            <div key={idx} className={`${ROOT}__row`}>
              <ReportSummaryListItem
                onClick={openDetail(item.reportId)}
                report={item}
                decimalPlaces={userSetting.currencyDecimalPlaces}
                symbol={userSetting.currencySymbol}
              />
            </div>
          );
        })}
        {!reportList.length && (
          <EmptyIcon
            className={`${ROOT}__empty`}
            message={msg().Exp_Msg_NoReportFound}
          />
        )}
      </div>
      <InfoDialog isShowInfo={isShowInfo} setShowInfo={setShowInfo} />
      <GlobalFooter active="report" />
    </WrapperWithPermission>
  );
};

export default ReportListPage;
