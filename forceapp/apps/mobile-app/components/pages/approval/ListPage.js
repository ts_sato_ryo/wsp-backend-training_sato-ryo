// @flow

import React from 'react';
import _ from 'lodash';

import msg from '../../../../commons/languages';

import { type State as UserSetting } from '../../../../commons/reducers/userSetting';
import {
  type ApprRequestList,
  type ApprRequest,
} from '../../../../domain/models/approval/request/Request';
import IconButton from '../../atoms/IconButton';
import Select from '../../atoms/Fields/Select';

import WrapperWithPermission from '../../organisms/commons/WrapperWithPermission';
import Navigation from '../../molecules/commons/Navigation';
import EmptyIcon from '../../molecules/commons/EmptyIcon';

import ReportSummaryListItem, {
  type Props as ReportSummaryListItemProps,
} from '../../molecules/approval/ReportSummaryListItem';

import { type ErrorInfo } from '../../../../commons/utils/AppPermissionUtil';

import './ListPage.scss';

const ROOT = 'mobile-app-pages-approval-page-list';

type State = {
  selection: string,
};

type Props = {
  approvalList: ApprRequestList,
  userSetting: UserSetting,
  hasPermissionError: ?ErrorInfo,
  onClickPushHisotry: (
    requestId: string,
    requestType: string,
    selection: string
  ) => void,
  onClickRefresh: (requestType: string) => void,
  requestType: string,
} & ReportSummaryListItemProps;

export default class ListPage extends React.PureComponent<Props, State> {
  state = {
    selection: this.props.requestType || 'all',
  };

  onChangeSelect = () => {
    return (e: SyntheticEvent<HTMLSelectElement>) => {
      this.setState({ selection: e.currentTarget.value });
    };
  };

  getFilteredList = (list: ApprRequestList) => {
    return this.state.selection === 'all'
      ? list
      : (list.filter(
          (item) => item.requestType === this.state.selection
        ): ApprRequestList);
  };

  render() {
    const { approvalList = [] } = this.props;
    const filteredList = this.getFilteredList(approvalList);

    const requestTypeGroups = [
      {
        available: true,
        item: { label: msg().Com_Sel_All, value: 'all' },
      },
      {
        available: this.props.userSetting.useAttendance,
        item: { label: msg().Appr_Lbl_AttendanceRequest, value: 'attendance' },
      },
      {
        available: this.props.userSetting.useExpense,
        item: { label: msg().Appr_Btn_ExpensesRequest, value: 'expense' },
      },
    ];
    const requestTypes = _(requestTypeGroups)
      .filter((group) => group.available)
      .map((group) => group.item)
      .value();

    const count = filteredList.length;
    const num = (
      <div key="0" className={`${ROOT}__num`}>
        {`${count} ${msg().Appr_Lbl_RecordCount}`}
      </div>
    );

    const refresh = (
      <IconButton
        className={`${ROOT}__refresh`}
        type="submit"
        onClick={() => this.props.onClickRefresh(this.state.selection)}
        icon="refresh-copy"
        size="small"
      />
    );

    return (
      <WrapperWithPermission
        className={ROOT}
        hasPermissionError={this.props.hasPermissionError}
      >
        <Navigation title={msg().Appr_Lbl_Approval} actions={[num, refresh]} />
        <div className="main-content-appr">
          <Select
            onChange={this.onChangeSelect()}
            className={`${ROOT}__select`}
            options={requestTypes}
            value={this.state.selection}
          />
          <div className={`${ROOT}__list`}>
            {filteredList.map((item: ApprRequest) => (
              <ReportSummaryListItem
                key={item.requestId}
                requestType={item.requestType}
                report={item}
                className={`${ROOT}__item`}
                onClick={() =>
                  this.props.onClickPushHisotry(
                    item.requestId,
                    item.requestType,
                    this.state.selection || 'all'
                  )
                }
                decimalPlaces={this.props.userSetting.currencyDecimalPlaces}
                symbol={this.props.userSetting.currencySymbol}
              />
            ))}
            {!filteredList.length && (
              <EmptyIcon
                className={`${ROOT}__empty`}
                message={msg().Appr_Msg_EmptyRequestList}
              />
            )}
          </div>
        </div>
      </WrapperWithPermission>
    );
  }
}
