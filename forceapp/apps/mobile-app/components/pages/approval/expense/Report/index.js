// @flow

import React from 'react';

import msg from '../../../../../../commons/languages';

import { type ExpRequest } from '../../../../../../domain/models/exp/request/Report';

import Wrapper from '../../../../atoms/Wrapper';
import Navigation, {
  type Props as NavigationProps,
} from '../../../../molecules/commons/Navigation';
import Report, { type Props as ReportProps } from './Report';
import RecordSummaryListItem from '../../../../molecules/expense/RecordSummaryListItem';
import HistoryList from '../../../../organisms/approval/HistoryList';
import Footer, {
  type Props as FooterProps,
} from '../../../../organisms/approval/Footer';

import './index.scss';

const ROOT = 'mobile-app-pages-approval-page-expense-report';

type Props = {
  report: ExpRequest,
  onClickRecord: (string) => void,
} & ReportProps &
  FooterProps &
  NavigationProps;

export default (props: Props) => {
  if (!props.report) {
    return null;
  }

  return (
    <Wrapper className={ROOT}>
      <Navigation
        title={msg().Appr_Lbl_ApprovalDetail}
        onClickBack={props.onClickBack}
        backButtonLabel={msg().Exp_Lbl_ReportList}
      />
      <div className="main-content">
        <Report
          report={props.report}
          currencySymbol={props.currencySymbol}
          currencyDecimalPlaces={props.currencyDecimalPlaces}
        />
        <section className={`${ROOT}__section`}>
          <div className={`${ROOT}__title`}>{msg().Exp_Lbl_Records}</div>
          {props.report.records.map((record) => (
            <RecordSummaryListItem
              key={record.recordId}
              onClick={() => props.onClickRecord(record.recordId || '')}
              record={record}
              currencySymbol={props.currencySymbol}
              currencyDecimalPlaces={props.currencyDecimalPlaces}
            />
          ))}
        </section>
        <section className={`${ROOT}__section`}>
          <div className={`${ROOT}__title`}>
            {msg().Com_Lbl_ApprovalHistory}
          </div>
          <HistoryList historyList={props.report.historyList} />
        </section>
        <Footer
          onClickApproveButton={props.onClickApproveButton}
          onClickRejectButton={props.onClickRejectButton}
        />
      </div>
    </Wrapper>
  );
};
