// @flow

import React from 'react';

import msg from '../../../../../../../commons/languages';

import { type ExpRequestRecord } from '../../../../../../../domain/models/exp/request/Report';

import Amount from '../../../../../atoms/Amount';
import ViewItem from '../../../../../molecules/commons/ViewItem';

const ROOT = 'mobile-app-pages-approval-page-expense-record-general';

export type Props = {
  record: ExpRequestRecord,
  currencySymbol: string,
  currencyDecimalPlaces: number,
};

export default (props: Props) => {
  const { record, currencyDecimalPlaces, currencySymbol } = props;

  return (
    <div className={ROOT}>
      <ViewItem label={msg().Exp_Clbl_WithoutTax} align="right">
        <Amount
          amount={record.items[0].withoutTax}
          decimalPlaces={currencyDecimalPlaces}
          symbol={currencySymbol}
        />
      </ViewItem>
      <ViewItem label={msg().Exp_Clbl_GstAmount} align="right">
        <Amount
          amount={record.items[0].gstVat}
          decimalPlaces={currencyDecimalPlaces}
          symbol={currencySymbol}
        />
      </ViewItem>
    </div>
  );
};
