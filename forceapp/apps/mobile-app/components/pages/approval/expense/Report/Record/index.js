// @flow

import _ from 'lodash';
import React from 'react';

import msg from '../../../../../../../commons/languages';

import Wrapper from '../../../../../atoms/Wrapper';
import Amount from '../../../../../atoms/Amount';
import Navigation from '../../../../../molecules/commons/Navigation';
import ViewItem from '../../../../../molecules/commons/ViewItem';
import FilePreview from '../../../../../molecules/commons/FilePreview';

import Route from './Route';
import General from './General';

import './index.scss';

import { getLabelValueFromEIs } from '../../../../../../../domain/models/exp/ExtendedItem';
import { RECORD_TYPE } from '../../../../../../../domain/models/exp/Record';
import {
  type ExpRequestRecord,
  type ExpRequest,
} from '../../../../../../../domain/models/exp/request/Report';

export type Props = {
  report: ExpRequest,
  record: ExpRequestRecord,
  currencySymbol: string,
  currencyDecimalPlaces: number,
  onClickBack: () => void,
};

const ROOT = 'mobile-app-pages-approval-page-expense-record';

export default (props: Props) => {
  const { record, currencyDecimalPlaces, currencySymbol, report } = props;

  if (!record) {
    return null;
  }

  let recordBody = null;
  if (record.recordType === RECORD_TYPE.TransitJorudanJP) {
    recordBody = <Route record={record} />;
  } else {
    recordBody = (
      <General
        record={record}
        currencyDecimalPlaces={currencyDecimalPlaces}
        currencySymbol={currencySymbol}
      />
    );
  }

  const extendedItems = getLabelValueFromEIs(record.items[0]);

  let { jobCode, jobName, costCenterCode, costCenterName } = report;
  const recordItem = record.items[0];
  if (jobCode && recordItem.jobId) {
    jobCode = recordItem.jobCode;
    jobName = recordItem.jobName;
  }

  if (costCenterCode && recordItem.costCenterHistoryId) {
    costCenterCode = recordItem.costCenterCode;
    costCenterName = recordItem.costCenterName;
  }

  return (
    <Wrapper className={ROOT}>
      <Navigation
        title={msg().Exp_Lbl_Records}
        onClickBack={props.onClickBack}
      />
      <div className="main-content">
        <ViewItem label={msg().Exp_Clbl_Date}>
          {record.recordDate.replace(/-/g, '/')}
        </ViewItem>
        <ViewItem label={msg().Exp_Clbl_ExpenseType}>
          {record.items[0].expTypeName}
          {record.recordType === RECORD_TYPE.FixedAllowanceMulti &&
            ` : ${record.items[0].fixedAllowanceOptionLabel || ''}`}
        </ViewItem>
        <ViewItem label={msg().Exp_Clbl_Gst}>
          {record.items[0].taxTypeName}
        </ViewItem>
        <ViewItem label={msg().Exp_Clbl_IncludeTax} align="right">
          <Amount
            amount={record.amount}
            decimalPlaces={currencyDecimalPlaces}
            symbol={currencySymbol}
          />
        </ViewItem>
        {costCenterCode && (
          <ViewItem label={msg().Exp_Clbl_CostCenter}>
            {`${costCenterCode} - ${costCenterName}`}
          </ViewItem>
        )}
        {jobCode && (
          <ViewItem label={msg().Exp_Lbl_Job}>
            {`${jobCode} - ${jobName}`}
          </ViewItem>
        )}
        {recordBody}
        {extendedItems.map((item, i) => (
          <ViewItem key={i} label={item.label}>
            {item.value}
          </ViewItem>
        ))}
        <ViewItem label={msg().Exp_Clbl_Summary}>
          {record.items[0].remarks}
        </ViewItem>
        <FilePreview
          url={record.receiptData}
          fileId={record.receiptId}
          label={msg().Exp_Lbl_Receipt}
          rootClassName={ROOT}
          fileName={record.fileName}
        />
      </div>
    </Wrapper>
  );
};
