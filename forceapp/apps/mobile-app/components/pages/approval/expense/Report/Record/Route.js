// @flow

import React from 'react';
import msg from '../../../../../../../commons/languages';

import { type ExpRequestRecord } from '../../../../../../../domain/models/exp/request/Report';

import ViewItem from '../../../../../molecules/commons/ViewItem';
import JorudanAmountSummary from '../../../../../molecules/expense/JorudanAmountSummary';

import RouteMap from '../../../../../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteMap';

import './Route.scss';

const ROOT = 'mobile-app-pages-approval-page-expense-record-route';

export type Props = {
  record: ExpRequestRecord,
};

export default (props: Props) => {
  const { record } = props;
  if (!record.routeInfo || !record.routeInfo.selectedRoute) {
    return null;
  }

  return (
    <div className={ROOT}>
      <ViewItem
        label={`${msg().Exp_Lbl_RouteOptionOneWay} / ${
          msg().Exp_Lbl_RouteOptionRoundTrip
        }`}
      >
        {record.routeInfo.roundTrip
          ? msg().Exp_Lbl_RouteOptionRoundTrip
          : msg().Exp_Lbl_RouteOptionOneWay}
      </ViewItem>
      <ViewItem className={`${ROOT}__route-map`} label={msg().Com_Lbl_Route}>
        <JorudanAmountSummary route={record.routeInfo.selectedRoute} />
        <RouteMap routeInfo={record.routeInfo} mobile />
      </ViewItem>
    </div>
  );
};
