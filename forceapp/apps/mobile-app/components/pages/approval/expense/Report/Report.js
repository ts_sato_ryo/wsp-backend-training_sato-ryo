// @flow

import React from 'react';

import msg from '../../../../../../commons/languages';

import { getLabelValueFromEIs } from '../../../../../../domain/models/exp/ExtendedItem';
import { type ExpRequest } from '../../../../../../domain/models/exp/request/Report';

import CommentArea from '../../../../molecules/commons/CommentArea';
import Amount from '../../../../atoms/Amount';
import ViewItem from '../../../../molecules/commons/ViewItem';
import FilePreview from '../../../../molecules/commons/FilePreview';

const ROOT = 'mobile-app-pages-approval-page-expense-report';

export type Props = {
  report: ExpRequest,
  currencySymbol: string,
  currencyDecimalPlaces: number,
};

export default (props: Props) => {
  const { report } = props;

  const extendedItems = getLabelValueFromEIs(report);

  return (
    <section className={`${ROOT}__section`}>
      <div className={`${ROOT}__title`}>{msg().Exp_Lbl_Report}</div>
      <CommentArea src={report.employeePhotoUrl} value={report.comment} />
      <ViewItem label={msg().Exp_Clbl_Type}>{msg().Appr_Lbl_Expense}</ViewItem>
      <ViewItem label={msg().Appr_Lbl_ApplicantName}>
        {report.employeeName}
      </ViewItem>
      <ViewItem label={msg().Exp_Clbl_ReportTitle}>{report.subject}</ViewItem>
      <ViewItem label={msg().Exp_Clbl_RecordDate}>
        {report.accountingDate.replace(/-/g, '/')}
      </ViewItem>
      <ViewItem label={msg().Appr_Lbl_TotalAmount}>
        <Amount
          amount={report.totalAmount}
          symbol={props.currencySymbol}
          decimalPlaces={props.currencyDecimalPlaces}
        />
      </ViewItem>
      <ViewItem label={msg().Exp_Clbl_ReportType}>
        {report.expReportTypeName}
      </ViewItem>
      {report.delegatedEmployeeName && (
        <ViewItem label={msg().Appr_Lbl_DelegatedApplicantName}>
          {report.delegatedEmployeeName}
        </ViewItem>
      )}
      {report.vendorId && (
        <ViewItem
          label={msg().Exp_Lbl_Vendor}
        >{`${report.vendorCode} - ${report.vendorName}`}</ViewItem>
      )}
      {report.paymentDueDate && (
        <ViewItem label={msg().Exp_Lbl_PaymentDate}>
          {report.paymentDueDate
            ? report.paymentDueDate.replace(/-/g, '/')
            : ''}
        </ViewItem>
      )}
      {report.costCenterCode && (
        <ViewItem label={msg().Exp_Clbl_CostCenter}>
          {`${report.costCenterCode} - ${report.costCenterName}`}
        </ViewItem>
      )}
      {report.jobCode && (
        <ViewItem label={msg().Appr_Lbl_Job}>
          {`${report.jobCode} - ${report.jobName}`}
        </ViewItem>
      )}
      {extendedItems.map((item, i) => (
        <ViewItem key={i} label={item.label}>
          {item.value}
        </ViewItem>
      ))}
      <ViewItem label={msg().Exp_Clbl_ReportRemarks}>{report.remarks}</ViewItem>
      <FilePreview
        url={report.attachedFileData}
        fileId={report.attachedFileId}
        label={msg().Exp_Lbl_AttachedFile}
        rootClassName={ROOT}
        fileName={report.fileName}
      />
    </section>
  );
};
