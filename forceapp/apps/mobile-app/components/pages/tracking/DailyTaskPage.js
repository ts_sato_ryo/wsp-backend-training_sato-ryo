// @flow

import * as React from 'react';
import classNames from 'classnames';

import DetectOverflow from '../../atoms/Utils/DetectOverflow';
import AddJobButton from '../../molecules/tracking/AddJobButton';
import SaveButton from '../../molecules/commons/Buttons/SaveButton';
import DailyTaskNavigation from '../../molecules/tracking/DailyTaskNavigation';
import DailyTaskGraphCard from '../../organisms/tracking/DailyTaskGraphCard';
import DailyTaskInputCard from '../../organisms/tracking/DailyTaskInputCard';

import { type Task } from '../../../../domain/models/time-tracking/Task';

import './DailyTaskPage.scss';

const ROOT = 'mobile-app-pages-tracking-daily-task-page';

export type Props = $ReadOnly<{|
  taskList: Array<Task>,
  toggleDirectInput: (id: string, isDirectInput: boolean) => void,
  editTaskTime: (id: string, taskTime: number) => void,
  editRatio: (id: string, ratio: number) => void,
  deleteTask: (id: string) => void,
  save: () => void,
  hasDefaultJob: boolean,
  hasMultipleRatios: boolean,
  realWorkTime: null | number,
  disabled?: boolean,

  /* Graph */
  isTemporaryWorkTime: boolean,
  taskTimes: Array<*>,

  /* Navigation */
  today: string,
  listEditing: boolean,
  onChangeDate: (string) => void,
  onClickPrevDate: () => void,
  onClickNextDate: () => void,
  onToggleEditing: () => void,
  onClickAddJob: () => void,
  onClickRefresh: () => void,
|}>;

export default class DailyTaskPage extends React.Component<Props> {
  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__heading`}>
          <DailyTaskNavigation
            today={this.props.today}
            listEditing={this.props.listEditing}
            onClickMonthlySummary={() => {}}
            onChangeDate={this.props.onChangeDate}
            onClickPrevDate={this.props.onClickPrevDate}
            onClickNextDate={this.props.onClickNextDate}
            onToggleEditing={this.props.onToggleEditing}
            onClickRefresh={this.props.onClickRefresh}
          />
          <div className={`${ROOT}__heading--info`}>
            <DailyTaskGraphCard
              isTemporaryWorkTime={this.props.isTemporaryWorkTime}
              realWorkTime={this.props.realWorkTime}
              taskTimes={this.props.taskTimes}
            />
          </div>
        </div>
        <DetectOverflow className={`${ROOT}__container`}>
          {({ isOverflowed }) => (
            <React.Fragment>
              <div className={`${ROOT}__content`}>
                <div>
                  {this.props.taskList.map((task) => (
                    <div className={`${ROOT}__input-field`} key={task.id}>
                      <DailyTaskInputCard
                        listEditing={this.props.listEditing}
                        code={task.jobCode}
                        job={task.jobName}
                        workCategoryName={task.workCategoryName || ''}
                        timeOrRatioValue={task.isDirectInput ? 'time' : 'ratio'}
                        onClickInputModeButton={() =>
                          this.props.toggleDirectInput(
                            task.id,
                            task.isDirectInput
                          )
                        }
                        onChangeTaskTime={(value) =>
                          this.props.editTaskTime(task.id, value)
                        }
                        onChangeRatio={(value) =>
                          this.props.editRatio(task.id, value)
                        }
                        onClickDeleteButton={() =>
                          this.props.deleteTask(task.id)
                        }
                        taskTime={task.taskTime || 0}
                        ratio={task.ratio || 0}
                        isDefaultJob={task.isDefaultJob}
                        defaultJobExists={this.props.hasDefaultJob}
                        multipleRatiosExist={this.props.hasMultipleRatios}
                        realWorkTimeExists={this.props.realWorkTime !== null}
                      />
                    </div>
                  ))}
                </div>
              </div>
              <div
                className={classNames(`${ROOT}__actions`, {
                  [`${ROOT}__actions--overflowed`]: isOverflowed,
                })}
              >
                {!this.props.listEditing && (
                  <AddJobButton
                    className={`${ROOT}__add-job-button`}
                    testId={`${ROOT}__add-job-button`}
                    onClick={this.props.onClickAddJob}
                    floating={isOverflowed}
                    disabled={this.props.disabled}
                  />
                )}
                <SaveButton
                  onClick={this.props.save}
                  disabled={this.props.disabled}
                />
              </div>
            </React.Fragment>
          )}
        </DetectOverflow>
      </div>
    );
  }
}
