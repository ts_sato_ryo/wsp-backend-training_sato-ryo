// @flow

import * as React from 'react';

import msg from '../../../../commons/languages';

import TimeUtil from '../../../../commons/utils/TimeUtil';
import { parseIntOrNull } from '../../../../commons/utils/NumberUtil';

import Card from '../../atoms/Card';
import TextButton from '../../atoms/TextButton';
import TaskTimeSelectField from '../../molecules/tracking/TaskTimeSelectField';
import SelectField, {
  type Props as SelectFieldProps,
} from '../../molecules/commons/Fields/SelectField';
import SaveButton from '../../molecules/commons/Buttons/SaveButton';
import Navigation from '../../molecules/commons/Navigation';

import './DailyTaskJobPage.scss';

const ROOT = 'mobile-app-tracking-daily-task-job-page';

type Options = $PropertyType<SelectFieldProps, 'options'>;

export type Props = $ReadOnly<{|
  jobs: Options,
  selectedJobId: string,
  workCategories: Options,
  selectedWorkCategoryId: string,
  taskTime: string,
  onSelectJob: (SyntheticEvent<HTMLSelectElement>) => void,
  onSelectWorkCategory: (SyntheticEvent<HTMLSelectElement>) => void,
  onChangeTaskTime: (?number) => void,
  onClickDiscard: () => void,
  onClickSave: () => void,
|}>;

export default class DailyTaskJobPage extends React.Component<Props> {
  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__heading`}>
          <Navigation
            title={msg().Trac_Lbl_AddJob}
            actions={[
              <TextButton onClick={this.props.onClickDiscard}>
                {msg().Com_Btn_Cancel}
              </TextButton>,
            ]}
          />
        </div>
        <div className={`${ROOT}__container`}>
          <Card flat className={`${ROOT}__content`}>
            <SelectField
              className={`${ROOT}__control`}
              required
              label={msg().Trac_Lbl_Job}
              options={this.props.jobs}
              value={this.props.selectedJobId}
              onChange={(e) => this.props.onSelectJob(e)}
            />
            <SelectField
              className={`${ROOT}__control`}
              label={msg().Trac_Lbl_WorkCategory}
              options={this.props.workCategories}
              value={this.props.selectedWorkCategoryId}
              onChange={(e) => this.props.onSelectWorkCategory(e)}
            />
            <TaskTimeSelectField
              className={`${ROOT}__control`}
              label={msg().Trac_Lbl_WorkTime}
              value={this.props.taskTime}
              onChange={(value) =>
                this.props.onChangeTaskTime(
                  parseIntOrNull(TimeUtil.toMinutes(value))
                )
              }
              placeholder="00:00"
            />
            <div className={`${ROOT}__action`}>
              <SaveButton
                onClick={this.props.onClickSave}
                disabled={
                  this.props.selectedJobId === '' ||
                  this.props.selectedJobId === null ||
                  this.props.selectedJobId === undefined
                }
              />
            </div>
          </Card>
        </div>
      </div>
    );
  }
}
