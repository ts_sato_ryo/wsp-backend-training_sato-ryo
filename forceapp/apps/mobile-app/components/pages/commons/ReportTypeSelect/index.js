// @flow

import React, { useState } from 'react';

import { get, cloneDeep, isEmpty } from 'lodash';
import Dialog from '../../../molecules/commons/Dialog';
import SelectField from '../../../molecules/commons/Fields/SelectField';
import SFDateField from '../../../molecules/commons/Fields/SFDateField';
import ViewItem from '../../../molecules/commons/ViewItem';
import msg from '../../../../../commons/languages';
import DateUtil from '../../../../../commons/utils/DateUtil';

import './index.scss';

const ROOT = 'mobile-app-molecules-report-type-select-dialog';

type Props = $ReadOnly<{|
  isShowDialog?: boolean,
  accountingPeriodList: Array<Object>,
  expReportTypeList: Array<Object>,
  accountingDate: string,
  onChangeAccountingPeriod: (value: string) => void,
  onChangeAccountingDate: (value: string) => void,
  onClickCloseDialog: () => void,
  onClickNext: (accountingPeriodId: ?string, expReportTypeId: string) => void,
|}>;

const ReportTypeSelectDialog = (props: Props) => {
  const {
    isShowDialog,
    accountingPeriodList,
    expReportTypeList,
    accountingDate,
  } = props;
  const [accountingPeriodId, setAccountingPeriodId] = useState('');
  const [expReportTypeId, setExpReportTypeId] = useState('');
  const [availableExpType, setAvailableExpType] = useState([]);

  // when accountingPeriodList is empty, if coming from record creation flow, take record date instead
  // if cominng from record list, take today as record date
  const isNoAccountingPeriod = isEmpty(accountingPeriodList);
  const accountingOptionsList = cloneDeep(accountingPeriodList) || [];
  if (!accountingPeriodId) {
    accountingOptionsList.unshift(
      <option key="default-blank-option" value="" className="is-hidden" />
    );
  }

  const getReportTypeList = (): Array<Object> => {
    const reportTypeList = props.expReportTypeList;
    const optionList = reportTypeList.map((obj) => ({
      value: obj.id,
      label: obj.name,
    }));
    if (!expReportTypeId) {
      return [{ value: '', label: '' }, ...optionList];
    }
    return optionList;
  };

  const onChangeAccountingPeriod = (value: string) => {
    props.onChangeAccountingPeriod(value);
    setAccountingPeriodId(value);
  };

  const onChangeAccountingDate = (date: Date) => {
    const updateValue = date && DateUtil.fromDate(date);
    props.onChangeAccountingDate(updateValue);
  };

  const onChangeReportType = (value: string) => {
    setExpReportTypeId(value);
    const selectedReportType = expReportTypeList.find((rt) => rt.id === value);
    const expTypeList = get(selectedReportType, 'expTypeList');
    const expTypeViewList = expTypeList
      ? expTypeList.map((obj) => {
          return (
            <ViewItem key={obj.expTypeId} label="">
              {obj.expTypeName}
            </ViewItem>
          );
        })
      : [];
    setAvailableExpType(expTypeViewList);
  };

  const onClickNext = () => {
    props.onClickNext(accountingPeriodId, expReportTypeId);
  };

  const onClickCloseDialog = () => {
    setAccountingPeriodId('');
    setExpReportTypeId('');
    setAvailableExpType([]);
    props.onClickCloseDialog();
  };

  const isReportTypeDisable = isNoAccountingPeriod
    ? !accountingDate
    : !accountingPeriodId;
  return isShowDialog ? (
    <div className={ROOT}>
      <Dialog
        title={msg().Exp_Lbl_ReportTypeSelection}
        content={
          <div className={`${ROOT}__content-container`}>
            <section>
              {isNoAccountingPeriod ? (
                <SFDateField
                  label={msg().Exp_Lbl_RecordDate}
                  onChange={(e, { date }) => onChangeAccountingDate(date)}
                  value={accountingDate}
                />
              ) : (
                <SelectField
                  label={msg().Exp_Clbl_AccountingPeriod}
                  onChange={(e) =>
                    onChangeAccountingPeriod((e.target: any).value)
                  }
                  options={accountingOptionsList}
                  value={accountingPeriodId}
                />
              )}
            </section>
            <section>
              <SelectField
                label={msg().Exp_Lbl_ReportType}
                disabled={isReportTypeDisable}
                onChange={(e) => onChangeReportType((e.target: any).value)}
                options={getReportTypeList()}
                value={expReportTypeId}
              />
            </section>
            {expReportTypeId !== '' && (
              <section className={`${ROOT}__section`}>
                <div className={`${ROOT}__section-label`}>
                  {msg().Exp_Lbl_ApplicableExpenseType}
                </div>
                <div className={`${ROOT}__section-viewList`}>
                  {availableExpType}
                </div>
              </section>
            )}
          </div>
        }
        centerButtonLabel={msg().Com_Lbl_NextButton}
        centerButtonDisabled={
          !expReportTypeId || (!accountingPeriodId && !isNoAccountingPeriod)
        }
        onClickCenterButton={() => onClickNext()}
        onClickCloseButton={() => onClickCloseDialog()}
      />
    </div>
  ) : null;
};

export default ReportTypeSelectDialog;
