/* @flow */
import * as React from 'react';

import msg from '../../../../commons/languages';

import Navigation from '../../molecules/commons/Navigation';
import RefreshButton from '../../molecules/commons/Buttons/RefreshButton';
import TimeStamp from '../../organisms/attendance/TimeStamp';

import './TimeStampPage.scss';

type TimeStampProps = $PropertyType<TimeStamp, 'props'>;

const ROOT = 'mobile-app-components-pages-attendance-time-stamp-page';

type Props = {|
  ...TimeStampProps,
  onClickRefresh: () => void,
|};

export default class TimeStampPage extends React.PureComponent<Props> {
  render() {
    const { onClickRefresh, ...props } = this.props;

    const navActions = [
      <RefreshButton className={`${ROOT}__refresh`} onClick={onClickRefresh} />,
    ];

    return (
      <div className={ROOT}>
        <Navigation title={msg().Att_Title_TimeStamp} actions={navActions} />
        <TimeStamp className={`${ROOT}__time-stamp`} {...props} />
      </div>
    );
  }
}
