// @flow
import React from 'react';

import type { AttDailyRecord } from '../../../../domain/models/attendance/AttDailyRecord';
import type { RestTimes } from '../../../../domain/models/attendance/RestTime';

import msg from '../../../../commons/languages';

import Layout from '../../../containers/organisms/attendance/TimesheetDailyLayoutContainer';

import Button from '../../atoms/Button';
import DailyDetailList from '../../organisms/attendance/DailyDetailList';
import Alert from '../../molecules/commons/Alert';

import './TimesheetDailyPage.scss';

const ROOT = 'mobile-app-pages-attendance-timesheet-daily-page';

export type Props = $ReadOnly<{|
  currentDate: string,
  isEditable: boolean,
  record: {
    startTime: number | null,
    endTime: number | null,
    restTimes: RestTimes,
    restHours: number | null,
    remarks: string,
    contractedDetail: $PropertyType<AttDailyRecord, 'contractedDetail'>,
    hasOtherRestTime: boolean,
    attentionMessages: string[],
  },
  minRestTimesCount?: number,
  maxRestTimesCount?: number,
  onChangeStartTime: (number | null) => void,
  onChangeEndTime: (number | null) => void,
  onChangeRestTimeStartTime: (number, number | null, number | null) => void,
  onChangeRestTimeEndTime: (number, number | null, number | null) => void,
  onClickRemoveRestTime: (number | null) => void,
  onClickAddRestTime: () => void,
  onChangeOtherRestTime: (number | null) => void,
  onChangeRemarks: (string) => void,
  onClickSave: () => void,
|}>;

export default class TimesheetDailyPage extends React.Component<Props> {
  render() {
    const { props } = this;
    return (
      <div className={`${ROOT}`} key={props.currentDate}>
        <Layout>
          <div className={`${ROOT}__package`}>
            {props.record.attentionMessages.length >= 1 && (
              <Alert
                className={`${ROOT}__alert`}
                variant="attention"
                message={props.record.attentionMessages}
              />
            )}
            <DailyDetailList
              key={props.currentDate}
              className={`${ROOT}__detail-list`}
              readOnly={!props.isEditable}
              startTime={props.record.startTime}
              endTime={props.record.endTime}
              restTimes={props.record.restTimes}
              otherRestTime={props.record.restHours}
              remarks={props.record.remarks}
              contractedDetail={props.record.contractedDetail}
              isShowOtherRestTime={props.record.hasOtherRestTime}
              minRestTimesCount={props.minRestTimesCount}
              maxRestTimesCount={props.maxRestTimesCount}
              onChangeStartTime={props.onChangeStartTime}
              onChangeEndTime={props.onChangeEndTime}
              onChangeRestTimeStartTime={props.onChangeRestTimeStartTime}
              onChangeRestTimeEndTime={props.onChangeRestTimeEndTime}
              onClickRemoveRestTime={props.onClickRemoveRestTime}
              onClickAddRestTime={props.onClickAddRestTime}
              onChangeOtherRestTime={props.onChangeOtherRestTime}
              onChangeRemarks={props.onChangeRemarks}
            />
            <div className={`${ROOT}__save-button-area`}>
              <Button
                priority="primary"
                variant="neutral"
                onClick={props.onClickSave}
                disabled={!props.isEditable}
              >
                {msg().Com_Btn_Save}
              </Button>
            </div>
          </div>
        </Layout>
      </div>
    );
  }
}
