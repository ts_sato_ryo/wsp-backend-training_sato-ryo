// @flow

import * as React from 'react';

import { type DirectRequest } from '../../../../../domain/models/attendance/AttDailyRequest/DirectRequest';
import * as RestTime from '../../../../../domain/models/attendance/RestTime';

import msg from '../../../../../commons/languages';
import DateUtil from '../../../../../commons/utils/DateUtil';

import Layout from '../../../../containers/organisms/attendance/DailyRequestDetailLayoutContainer';

import DateRangeField from '../../../molecules/commons/Fields/DateRangeField';
import AttTimeList from '../../../organisms/attendance/AttTimeList';
import TextAreaField from '../../../molecules/commons/Fields/TextAreaField';

import './DirectRequestPage.scss';

const ROOT =
  'mobile-app-pages-attendance-daily-request-details-direct-request-page';

export type Props = $ReadOnly<{|
  readOnly: boolean,
  request: DirectRequest,
  validation: { [$Keys<DirectRequest>]: string[] },
  minRestTimesCount?: number,
  maxRestTimesCount?: number,
  onChangeStartDate: (string) => void,
  onChangeEndDate: (string) => void,
  onChangeStartTime: (number | null) => void,
  onChangeEndTime: (number | null) => void,
  onChangeRestTime: (number, number | null, number | null) => void,
  onClickRemoveRestTime: (number) => void,
  onClickAddRestTime: () => void,
  onChangeRemarks: (string) => void,
|}>;

export default class DirectRequestPage extends React.Component<Props> {
  render() {
    const {
      readOnly,
      request,
      validation,
      minRestTimesCount,
      maxRestTimesCount,
      onChangeStartDate,
      onChangeEndDate,
      onChangeStartTime,
      onChangeEndTime,
      onChangeRestTime,
      onChangeRemarks,
      onClickRemoveRestTime,
      onClickAddRestTime,
    } = this.props;

    return (
      <div className={ROOT}>
        <Layout>
          <div className={`${ROOT}__item`}>
            <DateRangeField
              label={msg().Att_Lbl_Period}
              start={{
                readOnly,
                disabled: !readOnly,
                value: request.startDate,
                onChange: (e: SyntheticInputEvent<HTMLElement>, { date }) =>
                  onChangeStartDate(DateUtil.fromDate(date)),
              }}
              end={{
                readOnly,
                value: request.endDate,
                onChange: (e: SyntheticInputEvent<HTMLElement>, { date }) =>
                  onChangeEndDate(DateUtil.fromDate(date)),
              }}
              required
            />
          </div>
          <AttTimeList
            className={`${ROOT}__att-time-list`}
            readOnly={readOnly}
            workingTime={{
              placeholder: '(00:00)',
              from: {
                value: request.startTime,
                onChangeValue: (from, _to) => onChangeStartTime(from),
                required: true,
              },
              to: {
                value: request.endTime,
                onChangeValue: (_from, to) => onChangeEndTime(to),
                required: true,
              },
              errors: validation.startTime || validation.endTime,
            }}
            restTimes={{
              placeholder: '(00:00)',
              value:
                readOnly && request.directApplyRestTimes.length < 1
                  ? RestTime.filter(request.directApplyRestTimes)
                  : request.directApplyRestTimes,
              min: minRestTimesCount,
              max: maxRestTimesCount,
              onChangeValueStartTime: onChangeRestTime,
              onChangeValueEndTime: onChangeRestTime,
              onClickRemove: onClickRemoveRestTime,
              onClickAdd: onClickAddRestTime,
            }}
          />
          <div className={`${ROOT}__item`}>
            <TextAreaField
              className={`${ROOT}__remarks`}
              label={msg().Att_Lbl_Remarks}
              rows={3}
              value={request.remarks}
              onChange={(event: SyntheticEvent<HTMLTextAreaElement>) =>
                onChangeRemarks(event.currentTarget.value)
              }
              readOnly={readOnly}
            />
          </div>
        </Layout>
      </div>
    );
  }
}
