// @flow

import * as React from 'react';
import classNames from 'classnames';

import clickable, {
  type ClickableProps,
} from '../../../../commons/concerns/clickable';
import floatable, { type FloatableProps } from './floatable';
import { compose } from '../../../../commons/utils/FnUtil';
import displayName from '../../../../commons/concerns/displayName';

import './index.scss';

const ROOT = 'mobile-app-atoms-button';

type Props = $ReadOnly<{|
  variant: 'neutral' | 'add' | 'alert',
  priority: 'primary' | 'secondary',
  disabled?: boolean,
  children?: React.Node,
  type?: string,
  testId?: string,
  ...$Exact<ClickableProps>,
  ...$Exact<FloatableProps>,
|}>;

class ButtonPresentation extends React.PureComponent<Props> {
  props: Props;

  render() {
    const className = classNames(
      ROOT,
      this.props.className,
      `${ROOT}__container`
    );
    const classNameInner = classNames(
      ROOT,
      `${ROOT}__${this.props.variant}--${this.props.priority}`,
      { [`${ROOT}--disabled`]: this.props.disabled }
    );
    return (
      <div className={className}>
        <button
          data-test-id={this.props.testId}
          type={this.props.type}
          className={classNameInner}
          disabled={this.props.disabled}
          onClick={this.props.onClick}
        >
          {this.props.children}
        </button>
      </div>
    );
  }
}

export default (compose(
  displayName('Button'),
  floatable,
  clickable
)(ButtonPresentation): React.ComponentType<Object>);
