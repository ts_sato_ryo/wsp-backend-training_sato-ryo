// @flow

import * as React from 'react';
import classNames from 'classnames';

import FormatUtil from '../../../commons/utils/FormatUtil';

import './Amount.scss';

const ROOT = 'mobile-app-atoms-amount';

export type Props = $ReadOnly<{|
  amount: number,
  decimalPlaces: number,
  symbol: string,
  className?: string,
|}>;

export default (props: Props) => {
  const amount = FormatUtil.formatNumber(props.amount, props.decimalPlaces);

  const className = classNames(ROOT, props.className);
  return (
    <div className={className}>
      <div className={`${ROOT}__symbol`}>{props.symbol}</div>
      <div className={`${ROOT}__text`}>{amount}</div>
    </div>
  );
};
