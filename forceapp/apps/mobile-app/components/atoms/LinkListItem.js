// @flow

import * as React from 'react';
import classNames from 'classnames';

import clickable, {
  type ClickableProps,
} from '../../../commons/concerns/clickable';
import { compose } from '../../../commons/utils/FnUtil';
import displayName from '../../../commons/concerns/displayName';
import Icon from './Icon';

import './LinkListItem.scss';

const ROOT = 'mobile-app-atoms-link-list-item';

type Props = $ReadOnly<{|
  className?: string,
  children?: React.Node,
  noIcon?: boolean,
  ...$Exact<ClickableProps>,
|}>;

class LinkListItem extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);
    return (
      <div className={className} onClick={this.props.onClick}>
        <div className={`${ROOT}__item`}>{this.props.children}</div>
        {!this.props.noIcon && (
          <div className={`${ROOT}__icon`}>
            <Icon type="chevronright" size="medium" />
          </div>
        )}
      </div>
    );
  }
}

export default (compose(
  displayName('LinkListItem'),
  clickable
)(LinkListItem): React.ComponentType<Object>);
