// @flow

import * as React from 'react';
import classNames from 'classnames';
import IconButton from './IconButton';

import './Label.scss';

const ROOT = 'mobile-app-atoms-label';

type Props = $ReadOnly<{|
  className?: string,
  htmlFor?: string,
  text: string,
  marked?: boolean,
  emphasis?: boolean,
  children?: React.Node,
  hintMsg?: string,
  onClickHint?: () => void,
|}>;

export default class Label extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className, {
      [`${ROOT}--emphasis`]: this.props.emphasis,
    });

    return (
      <label className={className} htmlFor={this.props.htmlFor}>
        <div className={`${ROOT}__text`}>
          {this.props.marked ? (
            <div className={`${ROOT}__marker`}>*</div>
          ) : null}
          {this.props.text}
          {this.props.hintMsg && (
            <IconButton icon="info-copy" onClick={this.props.onClickHint} />
          )}
        </div>
        <div className={`${ROOT}__control`}>{this.props.children}</div>
      </label>
    );
  }
}
