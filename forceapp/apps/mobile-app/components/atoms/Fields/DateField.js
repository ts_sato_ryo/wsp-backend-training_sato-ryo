// @flow
// Reference url: https://react.lightningdesignsystem.com/components/date-pickers/

import * as React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import {
  Datepicker,
  IconSettings,
  Input,
} from '@salesforce/design-system-react';

import msg from '../../../../commons/languages';

import './DateField.scss';
import './Input.scss';

// date field localizations
import initDateFieldLabels from './DateFieldLabels';

const ROOT = 'mobile-app-atoms-date-field';

export type Props = $ReadOnly<{|
  className?: string,
  error?: boolean,
  emphasis?: boolean,
  testId?: string,
  placeholder?: string,
  required?: boolean,
  value: string,
  disabled?: boolean,
  readOnly?: boolean,
  onChange?: (
    SyntheticInputEvent<*>,
    { date: Date, formattedDate: string, timezoneOffset: number }
  ) => void,
  onBlur?: (SyntheticInputEvent<*>) => void,
|}>;

export default class DateField extends React.PureComponent<Props> {
  // date formatter for jp and english
  formatDate(date: any) {
    if (window.empInfo && window.empInfo.language === 'ja') {
      return moment(date).format('YYYY/MM/DD');
    } else {
      return moment(date).format('DD/MM/YYYY');
    }
  }

  render() {
    const { value, placeholder } = this.props;
    const formattedDate = value ? this.formatDate(value) : '';
    const datepickerValue = value ? new Date(value) : null;
    const dateFieldLabels = {
      ...initDateFieldLabels(),
      placeholder: placeholder || msg().Com_Lbl_MobilePleaseTapAndSelectDate,
    };
    const rootCss = classNames(ROOT, this.props.className);

    return (
      <div className={rootCss}>
        <IconSettings iconPath={window.__icon_path__}>
          <Datepicker
            className={ROOT}
            triggerClassName={this.props.error ? 'is-error' : null}
            labels={dateFieldLabels}
            onChange={this.props.onChange}
            menuPosition="overflowBoundaryElement"
            value={datepickerValue}
          >
            <Input
              disabled={this.props.disabled || this.props.readOnly}
              className={classNames(`${ROOT}__input`, {
                'is-read-only': this.props.readOnly,
              })}
              readOnly
              value={formattedDate}
            />
          </Datepicker>
        </IconSettings>
      </div>
    );
  }
}
