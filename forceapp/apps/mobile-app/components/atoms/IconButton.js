// @flow

import * as React from 'react';
import classNames from 'classnames';

// NOTE 通常はatoms -> atomsへの依存はよくないのだけど、Iconに限り例外的にそうしている
import type { IconSetType } from './Icon/IconSet';
import Icon from './Icon';
import clickable, {
  type ClickableProps,
} from '../../../commons/concerns/clickable';
import { compose } from '../../../commons/utils/FnUtil';
import displayName from '../../../commons/concerns/displayName';

import colors from '../../styles/variables/_colors.scss';

import './IconButton.scss';

const ROOT = 'mobile-app-atoms-icon-button';

type Props = $ReadOnly<{|
  className?: string,
  icon: IconSetType,
  size?: 'x-small' | 'small' | 'medium' | 'large' | 'x-large',
  disabled?: boolean,
  testId?: string,
  color?: ?string,
  disableColor?: ?string,
  ...$Exact<ClickableProps>,
|}>;

class IconButtonPresentation extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className, {
      [`${ROOT}--disabled`]: this.props.disabled,
      [`${ROOT}--size-${this.props.size || ''}`]: this.props.size,
    });

    let color = colors.blue600;
    if (this.props.disabled && this.props.disableColor) {
      color = this.props.disableColor;
    } else if (this.props.disabled) {
      color = colors.blue300;
    } else if (this.props.color) {
      color = this.props.color;
    }

    return (
      <button
        data-test-id={this.props.testId}
        className={className}
        disabled={this.props.disabled}
        onClick={this.props.onClick}
        type="button"
      >
        <Icon type={this.props.icon} size={this.props.size} color={color} />
      </button>
    );
  }
}

export default (compose(
  displayName('IconButton'),
  clickable
)(IconButtonPresentation): React.ComponentType<Object>);
