// @flow

import * as React from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import './PageTransition.scss';

export type Props = $Shape<{
  +transitionKey: string,
  +children: React.Node,
}>;

export default class PageTransition extends React.Component<Props> {
  render() {
    const { transitionKey, children } = this.props;
    return (
      <TransitionGroup>
        <CSSTransition
          key={transitionKey}
          classNames="page-transition"
          timeout={1000}
          mountOnEnter
          unmountOnExit
          appear
        >
          <div>{children}</div>
        </CSSTransition>
      </TransitionGroup>
    );
  }
}
