// @flow

import * as React from 'react';
import classNames from 'classnames';

import clickable, {
  type ClickableProps,
} from '../../../commons/concerns/clickable';
import { compose } from '../../../commons/utils/FnUtil';
import displayName from '../../../commons/concerns/displayName';

import './TextButton.scss';

const ROOT = 'mobile-app-atoms-text-button';

type Props = $ReadOnly<{|
  className?: string,
  disabled?: boolean,
  testId?: string,
  type?: string,
  children: string,
  ...$Exact<ClickableProps>,
|}>;

class TextButtonPresentation extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);
    const buttonClassName = classNames(`${ROOT}__button`, {
      [`${ROOT}__button--disabled`]: this.props.disabled,
    });

    return (
      <div className={className}>
        <button
          data-test-id={this.props.testId}
          className={buttonClassName}
          type={this.props.type}
          disabled={this.props.disabled}
          onClick={this.props.onClick}
        >
          {this.props.children}
        </button>
      </div>
    );
  }
}

export default (compose(
  displayName('TextButton'),
  clickable
)(TextButtonPresentation): React.ComponentType<Object>);
