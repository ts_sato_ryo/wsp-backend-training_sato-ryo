// @flow

import * as React from 'react';
import classNames from 'classnames';

import clickable, {
  type ClickableProps,
} from '../../../commons/concerns/clickable';
import { compose } from '../../../commons/utils/FnUtil';
import displayName from '../../../commons/concerns/displayName';

import './ListHeader.scss';

const ROOT = 'mobile-app-atoms-list-header';

type Props = $ReadOnly<{|
  className?: string,
  children?: React.Node,
  ...$Exact<ClickableProps>,
|}>;

class ListHeader extends React.PureComponent<Props> {
  render() {
    const className = classNames(ROOT, this.props.className);

    return (
      <div className={className} onClick={this.props.onClick}>
        {this.props.children}
      </div>
    );
  }
}

export default (compose(
  displayName('ListHeader'),
  clickable
)(ListHeader): React.ComponentType<Object>);
