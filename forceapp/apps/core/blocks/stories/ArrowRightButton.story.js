// @flow

/* eslint-disable import/no-extraneous-dependencies */

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ArrowRightButton from '../buttons/ArrowRightButton';

storiesOf('core/blocks/buttons', module).add('ArrowRightButton', () => (
  <ArrowRightButton onClick={action('onClick')} />
));
