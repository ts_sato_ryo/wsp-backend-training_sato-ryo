// @flow

/* eslint-disable import/no-extraneous-dependencies */

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ArrowLeftButton from '../buttons/ArrowLeftButton';

storiesOf('core/blocks/buttons', module).add('ArrowLeftButton', () => (
  <ArrowLeftButton onClick={action('onClick')} />
));
