// @flow

/* eslint-disable import/no-extraneous-dependencies */

import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import CloseButton from '../buttons/CloseButton';

storiesOf('core/blocks/buttons', module).add('CloseButton', () => (
  <CloseButton onClick={action('onClick')} />
));
