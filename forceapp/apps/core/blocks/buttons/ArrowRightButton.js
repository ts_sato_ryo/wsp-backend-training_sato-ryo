// @flow

import * as React from 'react';
import IconButton from '../../elements/IconButton';
import ArrowRight from '../../assets/icons/arrow-right.svg';

type Props = $ReadOnly<{
  ...React.ElementConfig<typeof IconButton>,
}>;

const CloseButton = (props: Props) => (
  <IconButton icon={ArrowRight} {...props} />
);

export default CloseButton;
