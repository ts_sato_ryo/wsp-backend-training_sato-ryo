// @flow

import * as React from 'react';
import IconButton from '../../elements/IconButton';
import ArrowLeft from '../../assets/icons/arrow-left.svg';

type Props = $ReadOnly<{
  ...React.ElementConfig<typeof IconButton>,
}>;

const CloseButton = (props: Props) => (
  <IconButton icon={ArrowLeft} {...props} />
);

export default CloseButton;
