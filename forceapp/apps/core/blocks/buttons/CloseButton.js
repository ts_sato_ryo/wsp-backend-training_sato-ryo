// @flow

import * as React from 'react';
import IconButton from '../../elements/IconButton';
import Close from '../../assets/icons/close.svg';

type Props = $ReadOnly<{
  ...React.ElementConfig<typeof IconButton>,
}>;

const CloseButton = (props: Props) => (
  <IconButton icon={Close} {...props} color="#999" />
);

export default CloseButton;
