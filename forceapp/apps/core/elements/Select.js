// @flow

import React, { useState, useCallback, useEffect, useRef } from 'react';
import type { ElementRef, ElementConfig, Node } from 'react';
import styled from 'styled-components';

import Check from '../assets/icons/check.svg';
import variables from '../../commons/styles/wsp.scss';

export type Option = {
  id?: string,
  value: any,
  label?: any,
};

type Props = $Diff<ElementConfig<'select'>, { children: ?Node }> &
  $ReadOnly<{
    className?: string,
    value?: any,
    options: $ReadOnlyArray<Option>,
    onSelect: (value: Option) => void,
  }>;

const Combobox = styled.div.attrs(({ isOpen, disabled, readOnly }: Props) => ({
  'aria-haspopup': true,
  'aria-expanded': isOpen,
  'aria-disabled': disabled,
  'aria-readonly': readOnly,
  role: 'combobox',
}))`
  position: relative;
  display: inline-block;
  background: #fff;
  border: 1px solid ${variables['color-border-1']};
  box-sizing: border-box;
  border-radius: 4px;
  appearance: none;
  width: 200px;
  height: 32px;
  color: ${variables['color-text-1']};
  font-size: ${variables['text-body-1-font-size']};
  line-height: ${variables['text-body-1-line-height']};
  padding: 7px 13px 8px 17px;
  text-align: left;
  white-space: normal;
  cursor: pointer;

  &:hover {
    background: #d3d2d2;
  }
`;

const Mark = styled.div`
  content: ' ';
  width: 8px;
  height: 5px;
  border: 5px solid transparent;
  border-color: #666 transparent transparent transparent;
`;

const OptionListItem = styled.ul.attrs(({ selectedId }) => ({
  'aria-activedescendant': selectedId,
  role: 'listbox',
}))`
  position: absolute;
  right: 0;
  left: 0;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  box-sizing: border-box;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.2);
  border-radius: 4px;
  max-height: 408px;
  margin: 8px 0 0 0;
  padding: 16px 0 16px 0;
  overflow-x: none;
  overflow-y: auto;
  z-index: 2;
`;

const OptionItem = styled.li.attrs(({ selected }) => ({
  'aria-selected': selected,
  role: 'option',
}))`
  min-height: 30px;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  align-items: center;
  font-size: 13px;
  line-height: 17px;
  color: #000;
  white-space: normal;
  font-weight: ${(props: Object) => (props.selected ? 'bold' : 'normal')};
  padding: 0 0 0 ${(props: Object) => (props.selected ? 0 : '29px')};

  :hover {
    background: ${variables['color-hover']};
  }
`;

const SelectedMark = styled.svg`
  fill: #008eb6;
  min-width: 29px;
`;

const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const useOutsideClick = (
  callback: () => void
): {| current: ElementRef<any> |} => {
  const ref = useRef();

  const handleClick = (e: MouseEvent) => {
    if (ref.current && !ref.current.contains(e.target)) {
      callback();
    }
  };
  useEffect(() => {
    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    };
  });

  return ref;
};

const Select = (props: Props) => {
  const [isOpen, setIsOpen] = useState(false);
  const ref = useOutsideClick(() => setIsOpen(false));
  const onClick = useCallback(() => {
    if (!props.readOnly && !props.disabled) {
      setIsOpen(!isOpen);
    }
  }, [isOpen, props.readOnly, props.disabled]);
  const onSelect = useCallback(
    (value: Option) => {
      if (!props.readOnly && !props.disabled) {
        props.onSelect(value);
        setIsOpen(false);
      }
    },
    [props.readOnly, props.disabled]
  );

  const selectedOption =
    props.options.find((option) => option.value === props.value) || {};

  return (
    <Combobox {...props} onClick={onClick} ref={ref} isOpen={isOpen}>
      <Content>
        <span>{selectedOption.label || selectedOption.value}</span>
        {!props.readOnly && !props.disabled && <Mark />}
      </Content>
      {isOpen && (
        <OptionListItem selectedId={selectedOption.id || selectedOption.value}>
          {props.options.map((option) => (
            <OptionItem
              id={option.id || option.value}
              selected={option.value === selectedOption.value}
              onClick={() => onSelect(option)}
            >
              {option.value === selectedOption.value && (
                <SelectedMark as={Check} />
              )}
              {option.label || option.value}
            </OptionItem>
          ))}
        </OptionListItem>
      )}
    </Combobox>
  );
};

export default Select;
