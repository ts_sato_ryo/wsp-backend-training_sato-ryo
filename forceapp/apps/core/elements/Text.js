// @flow

import * as React from 'react';
import styled from 'styled-components';

const Size = {
  xxxl: '24px',
  xxl: '20px',
  xl: '16px',
  large: '13px',
  medium: '12px',
  small: '10px',
};

const Color = {
  primary: '#333333',
  secondary: '#666666',
  disable: '#999999',
  action: '#2782ed',
  error: '#c23934',
};

type Props = {
  color?: $Keys<typeof Color>,
  size?: $Keys<typeof Size>,
  bold?: boolean,
  children: string,
};

const Text: React.ComponentType<Props> = styled.span`
  font-family: 'Salesforce Sans';
  font-weight: ${({ bold }) => (bold ? 'bold' : 'normal')};
  font-size: ${({ size = 'medium' }) => Size[size]};
  color: ${({ color = 'primary' }) => Color[color]};
  line-height: 1.5;
  overflow: inherit;
  whtie-space: inherit;
  text-overflow: inherit;
`;

export default Text;
