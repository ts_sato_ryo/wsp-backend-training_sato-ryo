// @flow

import * as React from 'react';
import styled from 'styled-components';

import { Color } from '../styles';

type ButtonProps = $Diff<
  React.ElementConfig<'button'>,
  { children?: React.Node }
>;

type Props = {
  ...ButtonProps,
  icon: string | React.ComponentType<{}>,
  color?: string,
};

const Icon = styled.svg`
  width: 12px;
  outline: none;
  appearance: none;
  border: none;

  fill: ${({ disabled, color }) => (disabled ? Color.disabled : color)};
`;

const Button = styled.button`
  outline: none;
  appearance: none;
  border: none;
  background: none;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 28px;
  width: 28px;

  :hover {
    background: ${Color.hover};
  }
  :active {
    background: ${Color.click};
  }
  :disabled,
  :disabled:active,
  :disabled:hover {
    color: ${Color.disabled};
    background: none;
  }
`;

const IconButton = ({ icon, color = '#000', ...props }: Props) => {
  return (
    <Button color={color} {...props}>
      <Icon as={icon} color={color} {...props} />
    </Button>
  );
};

export default IconButton;
