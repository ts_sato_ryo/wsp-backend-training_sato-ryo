// @flow
/* eslint-disable import/no-extraneous-dependencies */

import React from 'react';
import styled from 'styled-components';

import { storiesOf } from '@storybook/react';

import Button from '../Button';

const Center = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: space-evenly;
  height: 200px;
`;

storiesOf('core/Button', module)
  .addDecorator((story) => <Center>{story()}</Center>)
  .add('default', () => <Button color="default">Tester</Button>)
  .add('default/disabled', () => (
    <Button color="default" disabled>
      Disabled
    </Button>
  ))
  .add('primary', () => <Button color="primary">Tester</Button>)
  .add('primary/disabled', () => (
    <Button color="primary" disabled>
      Disabled
    </Button>
  ))
  .add('secondary', () => <Button color="secondary">Tester</Button>)
  .add('secondary/disabled', () => (
    <Button color="secondary" disabled>
      Disabled
    </Button>
  ))
  .add('danger', () => <Button color="danger">Tester</Button>)
  .add('danger/disabled', () => (
    <Button color="danger" disabled>
      Disabled
    </Button>
  ))
  .add('long text', () => (
    <Button color="default">
      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
      ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
      dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
      nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
      Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
      enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
      felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
      elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo
      ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem
      ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla
      ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies
      nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam
      rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper
      libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit
      vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante
      tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam
      quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed
      fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed
      consequat, leo eget bibendum sodales, augue velit cursus nunc,
    </Button>
  ));
