// @flow

/* eslint-disable import/no-extraneous-dependencies */

import React from 'react';

import { storiesOf } from '@storybook/react';

import Close from '../../assets/icons/close.svg';
import IconButton from '../IconButton';

storiesOf('core/elements/IconButton', module)
  .add('default', () => <IconButton icon={Close} />)
  .add('disabled', () => <IconButton icon={Close} color="red" disabled />)
  .add('colors', () => (
    <>
      <IconButton icon={Close} color="red" />
      <IconButton icon={Close} color="blue" />
      <IconButton icon={Close} color="green" />
    </>
  ));
