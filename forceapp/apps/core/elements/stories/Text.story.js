// @flow

/* eslint-disable import/no-extraneous-dependencies */

import React from 'react';
import styled from 'styled-components';

import { storiesOf } from '@storybook/react';

import Text from '../Text';

const Decorator = styled.div`
  display: flex;
  flex-flow: column nowrap;
`;

storiesOf('core/elements/Text', module)
  .addDecorator((story) => <Decorator>{story()}</Decorator>)
  .add('default', () => <Text>TEXT</Text>)
  .add('XXXL', () => (
    <>
      <Text size="xxxl">Font Size: 24px</Text>
      <Text size="xxxl" bold>
        Font Size: 24px
      </Text>
    </>
  ))
  .add('XXL', () => (
    <>
      <Text size="xxl">Font Size: 20px</Text>
      <Text size="xxl" bold>
        Font Size: 20px
      </Text>
    </>
  ))
  .add('XL', () => (
    <>
      <Text size="xl">Font Size: 16px</Text>
      <Text size="xl" bold>
        Font Size: 16px
      </Text>
    </>
  ))
  .add('L', () => (
    <>
      <Text size="large">Font Size: 13px</Text>
      <Text size="large" bold>
        Font Size: 13px
      </Text>
    </>
  ))
  .add('M', () => (
    <>
      <Text size="medium">Font Size: 12px</Text>
      <Text size="medium" bold>
        Font Size: 12px
      </Text>
    </>
  ))
  .add('S', () => (
    <>
      <Text size="small">Font Size: 10px</Text>
      <Text size="small" bold>
        Font Size: 10px
      </Text>
    </>
  ));
