// @flow
import { type Reducer, type Dispatch } from 'redux';

import {
  type JobList,
  getJobList,
  getRecentlyUsed,
  searchJobByKeyword,
} from '../../../models/exp/Job';

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/ENTITIES/EXP/JOB/LIST_SUCCESS',
};

const listSuccess = (jobList: JobList) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: jobList,
});

export const actions = {
  list: (parentId: ?string, targetDate: ?string, empId: ?string) => (
    dispatch: Dispatch<any>
  ): void | Promise<JobList> => {
    return getJobList(empId, parentId, targetDate)
      .then((res: JobList) => dispatch(listSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  getRecentlyUsed: (targetDate: ?string, empId: string) => (
    dispatch: Dispatch<any>
  ): void | Promise<JobList> => {
    return getRecentlyUsed(targetDate, empId)
      .then((res: JobList) => dispatch(listSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  getJobSearchResult: (
    keyword: ?string,
    targetDate: string,
    employeeId: string,
    usedIn?: string
  ) => (dispatch: Dispatch<any>): void | Promise<JobList> => {
    return searchJobByKeyword(
      employeeId,
      undefined,
      targetDate,
      keyword,
      usedIn
    )
      .then((res: JobList) => dispatch(listSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<any, any>);
