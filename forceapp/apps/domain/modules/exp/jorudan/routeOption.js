// @flow
import { type Reducer, type Dispatch } from 'redux';

import { initialStateConpany } from '../../../models/common/Company';

import searchRouteOption from '../../../models/exp/jorudan/JorudanOption';

import { actions as highwayBusActions } from '../../../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm/option/highwayBus';
import { actions as routeSortActions } from '../../../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm/option/routeSort';
import { actions as seatPreferenceActions } from '../../../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm/option/seatPreference';
import { actions as useChargedExpressActions } from '../../../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm/option/useChargedExpress';
import { actions as useExReservationActions } from '../../../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm/option/useExReservation';

export const ACTIONS = {
  SEARCH_SUCCESS: 'MODULES/EXP/JORUDAN/ROUTEOPTION/GET',
};

const searchSuccess = (body: any) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: body,
});

export const actions = {
  search: (companyId: string) => (dispatch: Dispatch<any>): void | any => {
    return searchRouteOption(companyId)
      .then((res: any) => {
        dispatch(searchSuccess(res));
        dispatch(highwayBusActions.set(res.jorudanHighwayBus || '0'));
        dispatch(routeSortActions.set(res.jorudanRouteSort || '0'));
        dispatch(seatPreferenceActions.set(res.jorudanSeatPreference || '0'));
        dispatch(
          useChargedExpressActions.set(res.jorudanUseChargedExpress || '0')
        );
        dispatch(
          useExReservationActions.set(res.jorudanUseExReservation || '0')
        );
      })
      .catch((err) => {
        throw err;
      });
  },
};

export default ((state = initialStateConpany, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<any, any>);
