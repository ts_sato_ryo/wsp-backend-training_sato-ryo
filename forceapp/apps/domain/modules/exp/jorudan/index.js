/* @flow */
import { combineReducers } from 'redux';

import route from './route';
import routeOption from './routeOption';

export default combineReducers<Object, Object>({
  route,
  routeOption,
});
