// @flow
import { type Reducer, type Dispatch } from 'redux';

import {
  type SearchRouteParam,
  type Route,
  initialStateRoute,
  searchRoute,
} from '../../../models/exp/jorudan/Route';

export const ACTIONS = {
  SEARCH_SUCCESS: 'MODULES/UI/EXP/JORUDAN/STATION/SEARCH',
  CLEAR: 'MODULES/UI/EXP/JORUDAN/STATION/CLEAR',
};

export const searchSuccess = (body: any) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: body,
});

const clear = () => ({
  type: ACTIONS.CLEAR,
});

export const actions = {
  search: (param: SearchRouteParam) => (
    dispatch: Dispatch<any>
  ): Promise<any> => {
    return searchRoute(param)
      .then((res: any) => dispatch(searchSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  clear: () => (dispatch: Dispatch<any>): void | any => {
    dispatch(clear());
  },
};

const initialState = initialStateRoute;

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_SUCCESS:
      return action.payload;
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<Route, any>);
