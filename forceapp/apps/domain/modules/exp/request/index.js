/* @flow */
import { combineReducers } from 'redux';

import report from './report';

export default combineReducers<Object, Object>({
  report,
});
