// @flow
import { type Reducer, type Dispatch } from 'redux';
import { find, cloneDeep, set } from 'lodash';

import {
  type ExpRequestList,
  type ExpRequest,
  type ExpRequestIdsInfo,
  type SearchConditions,
  initialStateExpRequestList,
  initialStateExpRequest,
  canExpRequestApproval,
  submitExpPreRequestReport,
  fetchExpPreRequestReportList,
  fetchExpPreRequestReportIds,
  getExpPreRequestReport,
} from '../../../models/exp/request/Report';

export const ACTIONS = {
  SUBMIT: 'MODULES/UI/EXP/REQUEST/PRE_REQUEST/SUBMIT',
  GET_IDS_SUCCESS: 'MODULES/UI/EXP/REQUEST/PRE_REQUEST/GET_IDS_SUCCESS',
  LIST_SUCCESS: 'MODULES/UI/EXP/REQUEST/PRE_REQUEST/LIST_SUCCESS',
  GET_SUCCESS: 'MODULES/UI/EXP/REQUEST/PRE_REQUEST/GET_SUCCESS',
  SET_STATUS: 'MODULES/UI/EXP/REQUEST/PRE_REQUEST/SET_STATUS',
  SET_IMAGE_DATA: 'MODULES/UI/EXP/REQUEST/PRE_REQUEST/SET_IMAGE_DATA',
  CLEAR: 'MODULES/UI/EXP/REQUEST/PRE_REQUEST/CLEAR',
};

const getIdsSuccess = (res: ExpRequestIdsInfo) => {
  return {
    type: ACTIONS.GET_IDS_SUCCESS,
    payload: res,
  };
};

const listSuccess = (body: ExpRequestList) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: body,
});

const getSuccess = (res: ExpRequest) => ({
  type: ACTIONS.GET_SUCCESS,
  payload: res,
});

const setImageData = (requestId, imageData, fileName, dataType) => {
  return {
    type: ACTIONS.SET_IMAGE_DATA,
    payload: { requestId, imageData, fileName, dataType },
  };
};

const setStatus = (requestList, requestId, status) => {
  const updatedList = cloneDeep(requestList);
  const request = find(updatedList, { requestId });
  if (request) {
    set(request, 'status', status);
  }
  return { type: ACTIONS.SET_STATUS, payload: updatedList };
};

const clear = () => ({
  type: ACTIONS.CLEAR,
});

export const actions = {
  submit: (reportId: string, comment: string) => () => {
    return submitExpPreRequestReport(reportId, comment);
  },
  cancel: (requestId: string, comment: string) => () => {
    return canExpRequestApproval(requestId, comment);
  },
  listIds: (
    searchCondition: SearchConditions,
    empId?: string,
    isEmpty?: boolean
  ) => (dispatch: Dispatch<any>): void | any => {
    return fetchExpPreRequestReportIds(searchCondition, empId).then(
      (res: ExpRequestIdsInfo) => {
        const list = isEmpty ? { totalSize: 0, requestIdList: [] } : res;
        return dispatch(getIdsSuccess(list));
      }
    );
  },
  list: (requestIds: Array<string>, empId?: string, isEmpty?: boolean) => (
    dispatch: Dispatch<any>
  ): void | any => {
    return fetchExpPreRequestReportList(requestIds).then(
      (requestList: ExpRequestList) => {
        const list = isEmpty ? [] : requestList;
        return dispatch(listSuccess(list));
      }
    );
  },
  get: (requestId: ?string) => (dispatch: Dispatch<any>): void | any => {
    return getExpPreRequestReport(requestId).then((res: any) =>
      dispatch(getSuccess(res))
    );
  },
  clear: () => (dispatch: Dispatch<any>): void | any => {
    dispatch(clear());
  },
  setImageData: (
    requestId: string,
    imageData: string,
    fileName: string,
    dataType: string
  ) => (dispatch: Dispatch<any>): void | any => {
    dispatch(setImageData(requestId, imageData, fileName, dataType));
  },
  setStatus: (
    requestList: ExpRequestList,
    requestId: string,
    status: string
  ) => (dispatch: Dispatch<any>): void | any => {
    dispatch(setStatus(requestList, requestId, status));
  },
};

const initialState = {
  expRequestList: initialStateExpRequestList,
  expRequest: initialStateExpRequest,
};

type State = {
  expRequestList: ExpRequestList,
  expRequest: ExpRequest,
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.GET_IDS_SUCCESS:
      return {
        ...state,
        expIdsInfo: action.payload,
      };
    case ACTIONS.LIST_SUCCESS:
      return {
        ...state,
        expRequestList: action.payload,
      };
    case ACTIONS.GET_SUCCESS:
      return {
        ...state,
        expRequest: action.payload,
      };
    case ACTIONS.SET_IMAGE_DATA:
      const stateCopy = cloneDeep(state);
      if (state.expRequest.requestId === action.payload.requestId) {
        set(stateCopy, 'expRequest.attachedFileData', action.payload.imageData);
        set(stateCopy, 'expRequest.fileName', action.payload.fileName);
        set(stateCopy, 'expRequest.dataType', action.payload.dataType);
      }
      return stateCopy;
    case ACTIONS.SET_STATUS:
      return {
        ...state,
        expRequestList: action.payload,
      };
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<State, any>);
