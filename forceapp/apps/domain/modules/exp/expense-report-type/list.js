// @flow
import { type Reducer, type Dispatch } from 'redux';

import { getReportTypeList } from '../../../models/exp/expense-report-type/list';

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/ENTITIES/EXP/EXPENSES_REPORT_TYPE/LIST_SUCCESS',
};

const listSuccess = (body: any) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: body.records,
});

export const actions = {
  list: (
    companyId: string,
    usedIn?: string,
    active: ?boolean,
    employeeId?: string
  ) => (dispatch: Dispatch<any>): void | any => {
    return getReportTypeList(companyId, usedIn, active, employeeId)
      .then((res: any) => {
        dispatch(listSuccess(res));
      })
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = { active: [], inactive: [] };

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      const activeReportType = action.payload.filter((rt) => rt.active);
      const inactiveReportType = action.payload.filter((rt) => !rt.active);
      return { active: activeReportType, inactive: inactiveReportType };
    default:
      return state;
  }
}: Reducer<Object, any>);
