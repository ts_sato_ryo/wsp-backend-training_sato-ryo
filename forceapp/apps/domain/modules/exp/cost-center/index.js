/* @flow */
import { combineReducers } from 'redux';

import list from './list';

export default combineReducers<Object, Object>({
  list,
});
