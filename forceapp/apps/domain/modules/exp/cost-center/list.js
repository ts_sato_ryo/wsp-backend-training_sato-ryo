// @flow
import { type Reducer, type Dispatch } from 'redux';

import {
  type CostCenterList,
  getCostCenterList,
  getRecentlyUsed,
  searchCostCenter,
} from '../../../models/exp/CostCenter';

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/ENTITIES/EXP/COST_CENTER/LIST_SUCCESS',
};

const listSuccess = (costCenterList: CostCenterList) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: costCenterList,
});

export const actions = {
  list: (parentId: ?string, targetDate: ?string) => (
    dispatch: Dispatch<any>
  ): void | any => {
    return getCostCenterList(null, parentId, targetDate)
      .then((res: CostCenterList) => dispatch(listSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  getRecentlyUsed: (employeeId: string, targetDate: string) => (
    dispatch: Dispatch<any>
  ): void | Promise<CostCenterList> => {
    return getRecentlyUsed(employeeId, targetDate)
      .then((res: CostCenterList) => dispatch(listSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  searchCostCenter: (
    companyId: ?string,
    keyword: ?string,
    targetDate: string,
    usedIn: string
  ) => (dispatch: Dispatch<any>): void | Promise<CostCenterList> => {
    return searchCostCenter(companyId, undefined, targetDate, keyword, usedIn)
      .then((res: CostCenterList) => dispatch(listSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<any, any>);
