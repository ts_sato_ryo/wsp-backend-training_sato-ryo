/* @flow */
import { combineReducers } from 'redux';

import report from './report';
import request from './request';
import approval from './approval';
import financeApproval from './finance-approval';
import preRequest from './pre-request';
import jorudan from './jorudan';
import expenseReportType from './expense-report-type';
import expenseType from './expense-type';
import customHint from './customHint';
import taxType from './taxType';
import receiptLibrary from './receiptLibrary';

export default combineReducers<Object, Object>({
  report,
  request,
  approval,
  customHint,
  preRequest,
  jorudan,
  expenseReportType,
  expenseType,
  taxType,
  receiptLibrary,
  financeApproval,
});
