/* @flow */
import { combineReducers } from 'redux';

import list from './list';
import childList from './childList';
import availableExpType from './availableExpType';

export default combineReducers<Object, Object>({
  availableExpType,
  list,
  childList,
});
