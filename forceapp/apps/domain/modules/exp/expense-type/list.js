// @flow
import { type Reducer, type Dispatch } from 'redux';
import { loadingStart, loadingEnd } from '../../../../commons/actions/app';

import {
  type ExpenseTypeList,
  getExpenseTypeList,
  searchExpenseType,
  getRecentlyUsed,
} from '../../../models/exp/ExpenseType';

export const ACTIONS = {
  GET_SUCCESS: 'MODULES/ENTITIES/EXP/EXPENSES_TYPE/GET_SUCCESS',
};

const getSuccess = (res: ExpenseTypeList) => ({
  type: ACTIONS.GET_SUCCESS,
  payload: res,
});

export const actions = {
  list: (
    empId: ?string,
    parentGroupId: ?string,
    targetDate: ?string,
    recordType: ?string,
    usedIn?: string,
    expReportTypeId?: string
  ) => (dispatch: Dispatch<any>): void | any => {
    dispatch(loadingStart());
    return getExpenseTypeList(
      empId,
      parentGroupId,
      targetDate,
      recordType,
      usedIn,
      expReportTypeId
    )
      .then((res: ExpenseTypeList) => dispatch(getSuccess(res)))
      .catch((err) => {
        throw err;
      })
      .finally(() => dispatch(loadingEnd()));
  },

  getRecentlyUsed: (
    employeeBaseId: string,
    companyId: string,
    targetDate?: string,
    recordType?: string,
    reportTypeId?: string,
    usedIn?: string
  ) => (dispatch: Dispatch<any>): void | Promise<ExpenseTypeList> => {
    return getRecentlyUsed(
      employeeBaseId,
      companyId,
      targetDate,
      recordType,
      reportTypeId,
      usedIn,
      true
    )
      .then((res: ExpenseTypeList) => dispatch(getSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },

  searchExpenseType: (
    companyId: ?string,
    keyword: ?string,
    targetDate: ?string,
    usedIn?: string,
    expReportTypeId: string,
    recordType?: string
  ) => (dispatch: Dispatch<any>): void | Promise<ExpenseTypeList> => {
    return searchExpenseType(
      companyId,
      keyword,
      targetDate,
      usedIn,
      expReportTypeId,
      true,
      recordType
    )
      .then((res: ExpenseTypeList) => dispatch(getSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.GET_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<ExpenseTypeList, any>);
