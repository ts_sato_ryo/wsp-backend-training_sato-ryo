// @flow
import { type Reducer, type Dispatch } from 'redux';
import { set } from 'lodash';

import { loadingStart, loadingEnd } from '../../../../commons/actions/app';

import {
  searchExpTypesByParentRecord,
  type ExpenseTypeList,
} from '../../../models/exp/ExpenseType';

type ExpTypesInfo = { recordType?: { targetDate: ExpenseTypeList } };

export const ACTIONS = {
  SEARCH_SUCCESS: 'MODULES/ENTITIES/EXP/EXPENSES_TYPE/SEARCH_SUCCESS',
};

const searchByParentSuccess = (resObj: ExpTypesInfo) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: resObj,
});

export const actions = {
  searchByParent: (
    targetDate: string,
    parentTypeId: string,
    usedIn: string
  ) => (dispatch: Dispatch<any>): void | any => {
    dispatch(loadingStart());
    return searchExpTypesByParentRecord(targetDate, parentTypeId, usedIn)
      .then((res: any) => {
        const resObj = {};
        set(resObj, `${parentTypeId}.${targetDate}`, res);
        dispatch(searchByParentSuccess(resObj));
        return res;
      })
      .catch((err) => {
        throw err;
      })
      .finally(() => dispatch(loadingEnd()));
  },
};

const initialState = {};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<ExpTypesInfo, any>);
