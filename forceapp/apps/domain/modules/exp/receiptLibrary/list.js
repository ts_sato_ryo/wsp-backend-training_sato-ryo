// @flow
import type { Reducer } from 'redux';

import {
  type ReceiptList,
  type FilePreview,
  getReceipts,
  getFilePreview,
  getOCRStatus,
  executeOcr,
  deleteReceipt,
} from '../../../models/exp/receipt-library/list';

import {
  uploadReceipt,
  type Base64FileList,
  getDocumentId,
} from '../../../models/exp/Receipt';

export const ACTIONS = {
  GET: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/GET',
  GET_OCR: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/GET_OCR',
  GET_DOCUMENT_ID: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/GET_DOCUMENT_ID',
  LIST: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/LIST',
  DELETE: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/DELETE',
  SAVE: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/SAVE',
  OCR_SUCCESS: 'MODULES/ENTITIES/EXP/RECEIPT_LIBRARY/OCR_SUCCESS',
  CLEAR: 'MODULES/EXPENSES/DIALOG/CLEAR',
};

const listSuccess = (res: ReceiptList) => ({
  type: ACTIONS.LIST,
  payload: res,
});

const getSuccess = (res: FilePreview) => ({
  type: ACTIONS.GET,
  payload: res,
});

const getOCRSuccess = (res: FilePreview) => ({
  type: ACTIONS.GET_OCR,
  payload: res,
});
const getDocumentIdSuccess = (res: string[]) => ({
  type: ACTIONS.GET_DOCUMENT_ID,
  payload: res,
});

const saveSuccess = (res: string) => {
  return {
    type: ACTIONS.SAVE,
    payload: res,
  };
};
const deleteSuccess = (res: string) => {
  return {
    type: ACTIONS.DELETE,
    payload: res,
  };
};
const ocrSuccess = (res: string) => {
  return {
    type: ACTIONS.OCR_SUCCESS,
    payload: res,
  };
};

export const actions = {
  list: () => (dispatch: Dispatch<any>): void | any => {
    return getReceipts()
      .then((res: ReceiptList) => {
        dispatch(listSuccess(res));
      })
      .catch((err) => {
        throw err;
      });
  },
  get: (receiptFileId: string) => (dispatch: Dispatch<any>): void | any => {
    return getFilePreview(receiptFileId)
      .then((res: FilePreview) => dispatch(getSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  getOcrStatus: (taskId: string) => (dispatch: Dispatch<any>): void | any =>
    getOCRStatus(taskId)
      .then((res: FilePreview) => {
        return dispatch(getOCRSuccess(res));
      })
      .catch((err) => {
        throw err;
      }),
  executeOcr: (receiptId: string) => (dispatch: Dispatch<any>): void | any => {
    return executeOcr(receiptId)
      .then((res) => dispatch(ocrSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  delete: (receiptId: string) => (dispatch: Dispatch<any>): void | any => {
    return deleteReceipt(receiptId)
      .then(() => dispatch(deleteSuccess(receiptId)))
      .catch((err) => {
        throw err;
      });
  },
  uploadReceipt: (base64FileList: Base64FileList) => (
    dispatch: Dispatch<any>
  ): void | any => {
    return uploadReceipt(base64FileList)
      .then((res) => dispatch(saveSuccess(res)))
      .catch((err) => {
        throw err;
      });
  },
  getDocumentId: (ids: string[]) => (
    dispatch: Dispatch<any>
  ): void | Promise<string[]> =>
    getDocumentId(ids)
      .then((res) => dispatch(getDocumentIdSuccess(res)))
      .catch((err) => {
        throw err;
      }),
};

type State = {
  receipts: ReceiptList,
  totalSize: number,
  receiptPreview: Array<FilePreview>,
};
const initialState = {
  receipts: [],
  totalSize: 0,
  receiptPreview: [],
};

/* Reducer */

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST:
      return { ...state, receipts: action.payload.reverse() };
    case ACTIONS.GET:
    case ACTIONS.GET_OCR:
      return {
        ...state,
        receiptPreview: action.payload,
      };
    case ACTIONS.DELETE:
      const receipts = state.receipts;
      receipts.splice(
        receipts.findIndex((item) => item.receiptId === action.payload),
        1
      );
      return { ...state, receipts };
    case ACTIONS.OCR_SUCCESS:
      return { ...state, ...action.payload };
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<State, any>);
