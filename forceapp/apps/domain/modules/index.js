/* @flow */
import { combineReducers } from 'redux';

import exp from './exp';
import approval from './approval';

export default combineReducers<Object, Object>({
  exp,
  approval,
});
