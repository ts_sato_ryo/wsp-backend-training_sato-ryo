/* @flow */
import { combineReducers } from 'redux';

import history from './history';

export default combineReducers<Object, Object>({
  history,
});
