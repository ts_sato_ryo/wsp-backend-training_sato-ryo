/* @flow */
import { combineReducers } from 'redux';

import request from './request';

export default combineReducers<Object, Object>({
  request,
});
