// @flow

export type Country = {
  name: string,
};

export type Company = {
  id: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
  code: string,
  countryId: string,
  language: string,
  useAttendance: boolean,
  useWorkTime: boolean,
  useExpense: boolean,
  useCompanyTaxMaster: boolean,
  useCompanyTaxMaster: boolean,
  plannerDefaultView: string,
  country: Country,
};

export type CompanyList = {
  records: Array<Company>,
};

export const initialStateCountry = {
  name: '',
};

export const initialStateConpany = {
  id: '',
  name: '',
  name_L0: '',
  name_L1: '',
  name_L2: '',
  code: '',
  countryId: '',
  language: '',
  useAttendance: false,
  useWorkTime: false,
  useExpense: false,
  useCompanyTaxMaster: false,
  plannerDefaultView: '',
  country: initialStateCountry,
};
