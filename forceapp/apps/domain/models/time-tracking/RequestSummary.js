// @flow
import type { TaskSummaryRecord } from './TaskSummaryRecord';

export type RequestSummary = $ReadOnly<{|
  summaryId: ?string,
  startDate: string,
  endDate: string,
  workTime: number,
  taskSummaryRecords: $ReadOnlyArray<TaskSummaryRecord>,
|}>;

const defaultSummary: RequestSummary = {
  summaryId: null,
  startDate: '',
  endDate: '',
  workTime: 0,
  taskSummaryRecords: [],
};

export default defaultSummary;
