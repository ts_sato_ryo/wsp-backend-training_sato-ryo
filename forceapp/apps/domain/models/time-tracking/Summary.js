// @flow
import type { TaskSummaryRecord } from './TaskSummaryRecord';
import defaultRequest, { type Request } from './Request';

export type SummaryFromRemote = $ReadOnly<{|
  id: ?string,
  startDate: string,
  endDate: string,
  workTime: number,
  taskSummaryRecords: $ReadOnlyArray<TaskSummaryRecord>,
  isLocked: boolean,
|}>;

export type SummaryFromRemoteWithRequest = $ReadOnly<{|
  request: ?Request,
  summary: SummaryFromRemote,
  useRequest: boolean,
|}>;

export type Summary = $ReadOnly<{|
  request: Request,
  useRequest: boolean,
  ...SummaryFromRemote,
|}>;

const defaultSummary: Summary = {
  request: defaultRequest,
  id: null,
  startDate: '',
  endDate: '',
  workTime: 0,
  taskSummaryRecords: [],
  isLocked: false,
  useRequest: false,
};

export default defaultSummary;
