/* @flow */

import { type WorkCategory } from './WorkCategory';

export type Job = {|
  /**
   * ジョブID
   */
  id: string,

  /**
   * ジョブコード
   */
  code: string,

  /**
   * ジョブ名
   */
  name: string,

  /**
   * 親ジョブID
   */
  parentId: ?string,

  /**
   * ジョブに紐付く作業分類のリスト
   */
  workCategories: WorkCategory[],

  /**
   * Whether jot has child level or not.
   */
  hasChildren: boolean,

  /**
   * 直課か否か
   */
  isDirectCharged: boolean,
|};
