// @flow

import _ from 'lodash';

import Task from '../../../time-tracking/tracking-pc/components/List/Item/Task';

/**
 * 作業分類
 */
export type WorkCategory = {|
  /**
   * 作業分類ID
   */
  id: string,

  /**
   * 作業分類コード
   */
  code: string,

  /**
   * 作業分類名
   */
  name: string,
|};

// eslint-disable-next-line import/prefer-default-export
export const createWorkCategoryMap = (
  tasks: Task[]
): { [jobId: string]: WorkCategory[] } => {
  if (_.isEmpty(tasks)) {
    return {};
  }

  const workCategoryLists = _(tasks)
    .map((task) => ({
      jobId: task.jobId,
      list: task.workCategoryList,
    }))
    .uniqBy('jobId')
    .values();

  const workCategoryMap = workCategoryLists.reduce(
    (acc, workCategoryList) => ({
      ...acc,
      [workCategoryList.jobId]: workCategoryList.list,
    }),
    {}
  );

  // 有効期間外のため現在は使用できない作業分類も選択肢に加える
  tasks.forEach((task) => {
    const workCategoryList = workCategoryMap[task.jobId];
    if (Array.isArray(workCategoryList) && task.workCategoryId !== null) {
      const notExistsInList = workCategoryList.every((workCategory) => {
        return workCategory.id !== task.workCategoryId;
      });
      if (notExistsInList) {
        workCategoryList.push({
          id: task.workCategoryId,
          code: task.workCategoryCode,
          name: task.workCategoryName,
        });
      }
    }
  });

  return workCategoryMap;
};
