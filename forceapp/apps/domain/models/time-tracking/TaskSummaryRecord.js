// @flow

export type TaskSummaryRecord = {|
  jobId: string,
  jobCode: string,
  jobName: string,
  isDefaultJob: boolean,
  workCategoryId: ?string,
  workCategoryCode: ?string,
  workCategoryName: ?string,
  workTimeRatio: number,
  workTime: number,
|};
