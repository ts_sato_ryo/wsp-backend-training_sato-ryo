/* @flow */

import _ from 'lodash';
import isNil from 'lodash/isNil';
import defaultTo from 'lodash/defaultTo';

import { pipe } from '../../../commons/utils/FnUtil';

export type TaskFromRemote = {
  jobId: string,
  jobCode: string,
  jobName: string,
  workCategoryId: ?string,
  workCategoryCode: ?string,
  workCategoryName: ?string,
  ratio: ?number,
  taskTime: ?number,
  taskNote: ?string,
  order: number,
  isDirectInput: boolean,

  /**
   * Actual performance or the other performances.
   * true: actual performance
   * false: the other performances
   */
  isRecorded: boolean,
};

/**
 * Actual performance for a job and a work category or
 * last performance automatically entered or
 * performance for events
 */
export type Task = TaskFromRemote & {
  id: string,
  graphRatio: number,
  volume?: number,
  isDefaultJob: boolean,
  isDirectCharged: boolean,
  eventTaskTime: number,
  color: $Shape<{
    base: string,
    linked: string,
  }>,
};

export const isDefaultJob = (defaultJobId: string, task: Task): boolean =>
  task.jobId === defaultJobId &&
  task.workCategoryId === null &&
  (!task.isDirectInput || task.taskTime === 0);

const updateIn = <T: { id: string }>(
  items: T[],
  item: T,
  identifier?: (T, T) => boolean
): T[] => {
  const identify = identifier || ((x, y) => x.id === y.id);
  return items.map((x) => (identify(x, item) ? { ...x, ...item } : x));
};

const mapWithOrder = <T: { id: string }>(
  items: T[],
  orderSelector: (T) => number,
  mapSelector: (T) => T
) => {
  const orderTable = _(
    items.map((item, index) => ({
      id: item.id,
      order: index,
    }))
  )
    .keyBy((task) => task.id)
    .value();
  const orderByTable = (item) => orderTable[item.id].order;

  return _(items)
    .orderBy([orderSelector, orderByTable], ['desc', 'asc'])
    .map(mapSelector)
    .orderBy(orderByTable)
    .value();
};

const fillInGapOfTaskTime = (realWorkTime: number, modifiedTask?: Task) => (
  taskList: Task[]
): Task[] => {
  const task = _(taskList)
    .filter(
      (t) =>
        !t.isDirectInput &&
        defaultTo(t.ratio, 0) > 0 &&
        (isNil(modifiedTask) || t.id !== modifiedTask.id)
    )
    .minBy((t) => t.taskTime);
  if (task === undefined || task.taskTime <= 0) {
    return taskList;
  }

  const totalTaskTime = _(taskList).sumBy((t) => t.taskTime);
  const gapOfTaskTime = realWorkTime - totalTaskTime;
  return updateIn(taskList, {
    ...task,
    taskTime: defaultTo(task.taskTime, 0) + gapOfTaskTime,
  });
};

const fillInGapOfRatio = (modifiedTask?: Task) => (
  taskList: Task[]
): Task[] => {
  const totalRatio = _(taskList)
    .filter((task) => !task.isDirectInput)
    .sumBy((task) => task.ratio);
  let restOfRatio = 100 - totalRatio;
  const getRestOfRatio = (): number => {
    restOfRatio -= 1;
    return restOfRatio > -1 ? 1 : 0;
  };

  return mapWithOrder(
    taskList,
    (task) => defaultTo(task.ratio, 0),
    (task) => {
      if (
        !task.isDirectInput &&
        defaultTo(task.ratio, 0) > 0 &&
        (isNil(modifiedTask) || task.id !== modifiedTask.id)
      ) {
        return {
          ...task,
          ratio: task.ratio + getRestOfRatio(),
        };
      } else {
        return task;
      }
    }
  );
};

const getRatioCalculator = (modifiedTask?: Task, taskList: Task[]) => {
  const totalRatio = _(taskList)
    .filter((task) => !task.isDirectInput)
    .sumBy((task) => defaultTo(task.ratio, 0));
  const diffRatio = 100 - totalRatio;
  const length = modifiedTask
    ? _(taskList)
        .filter((task) => modifiedTask && task.id !== modifiedTask.id)
        .filter((task) => !task.isDirectInput)
        .value().length
    : _(taskList)
        .filter((task) => !task.isDirectInput)
        .value().length;
  const values = _(_.range(length))
    .map((x) => Math.floor((diffRatio + x) / length))
    .orderBy((x) => x, 'desc')
    .value();

  return (task: Task): number => {
    const ratio = defaultTo(task.ratio, 0);

    if (values.length < 0) {
      return ratio;
    }

    const value = values.shift();
    return ratio + value;
  };
};

const balanceRatio = (modifiedTask?: Task) => (taskList: Task[]): Task[] => {
  const calculateRatio = getRatioCalculator(modifiedTask, taskList);

  const doesExcludeModifiedTaskIfExists = modifiedTask
    ? (task) => modifiedTask && task.id !== modifiedTask.id
    : (_task) => true;

  return mapWithOrder(
    taskList,
    (task) => defaultTo(task.ratio, 0),
    (task) => {
      if (!task.isDirectInput && doesExcludeModifiedTaskIfExists(task)) {
        return {
          ...task,
          ratio: calculateRatio(task),
        };
      } else {
        return task;
      }
    }
  );
};

const updateRatio = (taskList: Task[]) => {
  const nonDirectInputTotalTaskTime = _(taskList)
    .filter((task) => !task.isDirectInput)
    .sumBy((task) => defaultTo(task.taskTime, 0));

  return _(taskList)
    .map((task) => {
      return !task.isDirectInput
        ? {
            ...task,
            ratio:
              nonDirectInputTotalTaskTime === 0
                ? 0
                : Math.floor(
                    (defaultTo(task.taskTime, 0) /
                      nonDirectInputTotalTaskTime) *
                      100
                  ),
          }
        : task;
    })
    .value();
};

export const calculateTaskTimeFromRatio = (realWorkTime: number) => (
  taskList: Task[]
): Task[] => {
  const directInputTotalTaskTime = _(taskList)
    .filter((task) => task.isDirectInput)
    .sumBy((task) => defaultTo(task.taskTime, 0));
  const restTaskTime = realWorkTime - defaultTo(directInputTotalTaskTime, 0);

  return _(taskList)
    .map((task) => {
      return !task.isDirectInput
        ? {
            ...task,
            taskTime:
              task.ratio === 0
                ? 0
                : Math.max(
                    0,
                    // $FlowFixMe v0.89
                    Math.floor(restTaskTime * (defaultTo(task.ratio) / 100))
                  ),
          }
        : task;
    })
    .value();
};

export const updateInTaskList = (taskList: Task[], task: Task): Task[] => {
  return updateIn(taskList, {
    ...task,
    ratio: defaultTo(task.ratio, 0),
    taskTime: defaultTo(task.taskTime, 0),
  });
};

export const toggleDirectInput = (
  realWorkTime: number,
  modifiedTask: Task,
  taskList: Task[]
): Task[] => {
  return pipe(
    (ts) =>
      updateInTaskList(ts, {
        ...modifiedTask,
        isDirectInput: !modifiedTask.isDirectInput,
      }),
    updateRatio,
    balanceRatio(),
    defaultTo(modifiedTask.taskTime, 0) !== 0 // taskTimeが0なら再計算してtaskTimeを求める必要がある
      ? calculateTaskTimeFromRatio(realWorkTime)
      : (xs) => xs,
    fillInGapOfRatio(modifiedTask),
    fillInGapOfTaskTime(realWorkTime, modifiedTask)
  )(taskList);
};

export const updateTaskList = (
  realWorkTime: number,
  modifiedTask: Task,
  taskList: Task[]
): Task[] => {
  const update = pipe(
    (ts) => updateInTaskList(ts, modifiedTask),
    balanceRatio(modifiedTask),
    calculateTaskTimeFromRatio(realWorkTime),
    fillInGapOfRatio(modifiedTask),
    fillInGapOfTaskTime(realWorkTime, modifiedTask),

    /**
     * 割合を更新すると、マイナスのratioと時間のデータが出来るので、
     * 再度割合を再計算してあげる必要がある
     */
    updateRatio,
    balanceRatio(modifiedTask)
  );
  return update(taskList);
};

export const deleteTaskInTaskList = (
  realWorkTime: number,
  taskId: string,
  taskList: Task[]
): Task[] => {
  const newTaskList = taskList.filter((t) => t.id !== taskId);
  const recalculateTaskTime = pipe(
    balanceRatio(),
    calculateTaskTimeFromRatio(realWorkTime),
    fillInGapOfRatio(),
    fillInGapOfTaskTime(realWorkTime)
  );
  return recalculateTaskTime(newTaskList);
};

const defaultTask: Task = {
  id: '',
  graphRatio: 0,
  jobId: '',
  jobCode: '',
  jobName: '',
  workCategoryId: null,
  workCategoryCode: null,
  workCategoryName: null,
  ratio: 0,
  taskTime: 0,
  taskNote: null,
  order: -1,
  isDirectInput: false,
  isDirectCharged: false,
  isDefaultJob: false,
  isRecorded: true,
  color: {},
  eventTaskTime: 0,
};

export default defaultTask;
