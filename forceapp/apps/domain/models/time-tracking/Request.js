// @flow
import STATUS, { type Status } from '../approval/request/Status';

export type Request = $ReadOnly<{
  requestId: ?string,
  status: Status,
  approvalId: ?string,
  approverCode: ?string,
  approverName: ?string,
  actorId: ?string,
  actorCode: ?string,
  actorName: ?string,
}>;

const defaultRequest: Request = {
  requestId: null,
  status: STATUS.NotRequested,
  approvalId: null,
  approverCode: null,
  approverName: null,
  actorId: null,
  actorCode: null,
  actorName: null,
};
export default defaultRequest;
