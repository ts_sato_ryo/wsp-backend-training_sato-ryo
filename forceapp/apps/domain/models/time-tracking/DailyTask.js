// @flow

import isNil from 'lodash/isNil';
import uuid from 'uuid/v4';
import moment from 'moment';

import defaultTask, { type Task, isDefaultJob } from './Task';
import type { PersonalSetting } from '../../../commons/modules/personalSetting';

type Status =
  | 'NotRequested'
  | 'Pending'
  | 'Approved'
  | 'Rejected'
  | 'Removed'
  | 'Canceled';

export type DailyTask = {|
  targetDate: string,
  status: Status,
  note: null | string,
  output: null | string,
  realWorkTime: null | number,
  isTemporaryWorkTime: boolean,
  taskList: Task[],
|};

export const getDefaultJob = (
  defaultJob: Object,
  dailyTask: DailyTask
): Task => {
  const task = dailyTask.taskList.find((t: Task) =>
    isDefaultJob(defaultJob.id, t)
  );
  if (task !== null && task !== undefined) {
    return {
      ...task,
      isDirectInput: false,
      volume:
        task.isDirectInput || (!isNil(task.volume) && task.volume > 0)
          ? 600
          : 0,
      ratio: 100,
      taskTime: null,
      eventTaskTime: 0,
      workCategoryList: [],
      isDefaultJob: true,
    };
  } else {
    return {
      ...defaultTask,
      id: uuid(),
      jobId: defaultJob.id,
      jobCode: defaultJob.code,
      jobName: defaultJob.name,
      isDirectCharged: defaultJob.isDirectCharged,
      workCategoryId: null,
      color: { base: '', linked: '' },
      isDirectInput: false,
      volume: 600,
      ratio: 100,
      taskTime: null,
      eventTaskTime: 0,
      isDefaultJob: true,
      workCategoryList: [],
    };
  }
};

/**
 * Check if the Default Job is available on the specified date.
 *
 * criteria:
 * - The ID is not null
 * - The "Valid From" is null or before today (inclusive)
 * - The "Valid To" is null or after today (exclusive)
 *
 * @param {?string} todayDate The date represents today. Must be UTC time and formatted as ISO8601.
 * @param {Object} defaultJob The Default Job object in the Personal Setting state
 */
export const isDefaultJobAvailable = (
  targetDate: string,
  defaultJob: $PropertyType<PersonalSetting, 'defaultJob'>
): boolean =>
  defaultJob !== null &&
  defaultJob !== undefined &&
  defaultJob.id !== null &&
  defaultJob.id !== undefined &&
  (defaultJob.validDateFrom === null ||
    defaultJob.validDateFrom === undefined ||
    (moment(defaultJob.validDateFrom).isSameOrBefore(targetDate, 'day') &&
      (defaultJob.validDateTo === null ||
        defaultJob.validDateTo === undefined ||
        moment(defaultJob.validDateTo).isAfter(targetDate, 'day'))));

const defaultValue: DailyTask = {
  targetDate: '',
  status: 'NotRequested',
  note: null,
  output: null,
  realWorkTime: null,
  isTemporaryWorkTime: false,
  taskList: [],
};

export default defaultValue;
