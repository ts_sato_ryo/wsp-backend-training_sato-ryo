// @flow

import { isPermissionSatisfied } from '../Permission';

describe('isPermissionSatisfied(conditions)', () => {
  describe('By Delegate (isByDelegate: true)', () => {
    describe('Always Allowed (allowIfByDelegate: true)', () => {
      test('Returns true', () => {
        const conditions = {
          isByDelegate: true,
          userPermission: {},
          allowIfByDelegate: true,
        };
        expect(isPermissionSatisfied(conditions)).toBe(true);
      });
    });

    describe('Permission Required  (requireIfByEmployee: permissionKey[])', () => {
      describe('With Proper User Permission', () => {
        test('Returns true', () => {
          const conditions = {
            isByDelegate: true,
            requireIfByDelegate: ['cancelAttDailyApprovalByDelegate'],
            userPermission: { cancelAttDailyApprovalByDelegate: true },
          };
          expect(isPermissionSatisfied(conditions)).toBe(true);
        });
      });

      describe('Without Proper User Permission', () => {
        test('Returns false', () => {
          const conditions = {
            isByDelegate: true,
            requireIfByDelegate: ['cancelAttDailyApprovalByDelegate'],
            userPermission: {},
          };
          expect(isPermissionSatisfied(conditions)).toBe(false);
        });
      });
    });
  });

  describe('By Employee (isByDelegate: false)', () => {
    describe('Always Allowed (allowIfByEmployee: true)', () => {
      test('Returns true', () => {
        const conditions = {
          isByDelegate: false,
          userPermission: {},
          allowIfByEmployee: true,
        };
        expect(isPermissionSatisfied(conditions)).toBe(true);
      });
    });

    describe('Permission Required  (requireIfByEmployee: permissionKey[])', () => {
      describe('With Proper User Permission', () => {
        test('Returns true', () => {
          const conditions = {
            isByDelegate: false,
            requireIfByEmployee: ['cancelAttDailyApprovalByEmployee'],
            userPermission: { cancelAttDailyApprovalByEmployee: true },
          };
          expect(isPermissionSatisfied(conditions)).toBe(true);
        });
      });

      describe('Without Proper User Permission', () => {
        test('Returns false', () => {
          const conditions = {
            isByDelegate: false,
            requireIfByEmployee: ['cancelAttDailyApprovalByEmployee'],
            userPermission: {},
          };
          expect(isPermissionSatisfied(conditions)).toBe(false);
        });
      });
    });
  });
});
