// @flow

import type { UserSetting } from '../UserSetting';

/**
 * 勤怠トランザクション系権限
 */
type AttTransactionPermission = {|
  /**
   * 勤務表表示(代理)
   */
  viewAttTimeSheetByDelegate: boolean,

  /**
   * 勤務表編集(代理)
   */
  editAttTimeSheetByDelegate: boolean,
|};

/**
 * 工数トランザクション系権限
 */
type TimeTrackTransactionPermission = {|
  /**
   * 工数実績表示(代理)
   */
  viewTimeTrackByDelegate: boolean,
|};

/**
 * 各種申請系権限
 */
type AttDailyRequestPermission = {|
  /**
   * 各種勤怠申請申請・申請削除(代理)
   */
  submitAttDailyRequestByDelegate: boolean,

  /**
   * 各種勤怠申請申請取消(代理)
   */
  cancelAttDailyRequestByDelegate: boolean,

  /**
   * 各種勤怠申請承認却下(代理)
   */
  approveAttDailyRequestByDelegate: boolean,

  /**
   * 各種勤怠申請承認取消(本人)
   */
  cancelAttDailyApprovalByEmployee: boolean,

  /**
   * 各種勤怠申請承認取消(代理)
   */
  cancelAttDailyApprovalByDelegate: boolean,

  /**
   * 各種勤怠申請自己承認（本人）
   */
  approveSelfAttDailyRequestByEmployee: boolean,

  /**
   * 勤務確定申請申請(代理)
   */
  submitAttRequestByDelegate: boolean,

  /**
   * 勤務確定申請申請取消(代理)
   */
  cancelAttRequestByDelegate: boolean,

  /**
   * 勤務確定申請承認却下(代理)
   */
  approveAttRequestByDelegate: boolean,

  /**
   * 勤務確定申請承認取消(本人)
   */
  cancelAttApprovalByEmployee: boolean,

  /**
   * 勤務確定申請承認取消(代理)
   */
  cancelAttApprovalByDelegate: boolean,

  /**
   * 勤務確定申請自己承認（本人）
   */
  approveSelfAttRequestByEmployee: boolean,
|};

/**
 * 管理系権限
 */
type ManagementPermission = {|
  /**
   * 全体設定の管理
   */
  manageOverallSetting: boolean,

  /**
   * 会社の切り替え
   */
  switchCompany: boolean,

  /**
   * 部署の管理
   */
  manageDepartment: boolean,

  /**
   * 社員の管理
   */
  manageEmployee: boolean,

  /**
   * カレンダーの管理
   */
  manageCalendar: boolean,

  /**
   * ジョブタイプの管理
   */
  manageJobType: boolean,

  /**
   * ジョブの管理
   */
  manageJob: boolean,

  /**
   * モバイル機能設定の管理
   */
  manageMobileSetting: boolean,

  /**
   * アクセス権限設定の管理
   */
  managePermission: boolean,

  /**
   * プランナー機能設定の管理
   */
  managePlannerSetting: boolean,

  /**
   * 休暇の管理
   */
  manageAttLeave: boolean,

  /**
   * 短時間勤務設定の管理
   */
  manageAttShortTimeWorkSetting: boolean,

  /**
   * 休職・休業の管理
   */
  manageAttLeaveOfAbsence: boolean,

  /**
   * 勤務体系の管理
   */
  manageAttWorkingType: boolean,

  /**
   * 勤務パターンの管理
   */
  manageAttPattern: boolean,

  /**
   * 勤務パターン適用の管理
   */
  manageAttPatternApply: boolean,

  /**
   * 残業警告設定の管理
   */
  manageAttAgreementAlertSetting: boolean,

  /**
   * 休暇管理の管理
   */
  manageAttLeaveGrant: boolean,

  /**
   * 短時間勤務設定適用の管理
   */
  manageAttShortTimeWorkSettingApply: boolean,

  /**
   * 休職・休業適用の管理
   */
  manageAttLeaveOfAbsenceApply: boolean,

  /**
   * 勤務パターン適用
   */
  manageAttPatternApply: boolean,

  /**
   * 工数設定の管理
   */
  manageTimeSetting: boolean,

  /**
   * 作業分類の管理
   */
  manageTimeWorkCategory: boolean,

  manageExpTypeGroup: boolean,
  manageExpenseType: boolean,
  manageTaxType: boolean,
  manageExpSetting: boolean,
  manageExchangeRate: boolean,
  manageAccountingPeriod: boolean,
  manageReportType: boolean,
  manageCostCenter: boolean,
  manageVendor: boolean,
  manageExtendedItem: boolean,
  manageEmployeeGroup: boolean,
  manageExpCustomHint: boolean,
|};

/**
 * 権限
 */
export type Permission = {|
  // 勤怠トランザクション系権限
  ...AttTransactionPermission,

  // 工数トランザクション系権限
  ...TimeTrackTransactionPermission,

  // 各種申請系権限
  ...AttDailyRequestPermission,

  // 管理系権限
  ...ManagementPermission,
|};

type TestConditionsForDelegate =
  | {||}
  | {| allowIfByDelegate: true |}
  | {|
      allowIfByDelegate?: ?false,
      requireIfByDelegate: $Keys<Permission>[],
    |};

type TestConditionsForEmployee =
  | {||}
  | {| allowIfByEmployee: true |}
  | {|
      allowIfByEmployee?: ?false,
      requireIfByEmployee: $Keys<Permission>[],
    |};

export type DynamicTestConditions = {|
  ...TestConditionsForDelegate,
  ...TestConditionsForEmployee,
|};

export type TotalTestConditions = {|
  ...TestConditionsForDelegate,
  ...TestConditionsForEmployee,
  userPermission: Permission,
  isByDelegate: boolean,
|};

const defaultPermission: Permission = {
  viewAttTimeSheetByDelegate: false,
  editAttTimeSheetByDelegate: false,

  viewTimeTrackByDelegate: false,

  submitAttDailyRequestByDelegate: false,
  cancelAttDailyRequestByDelegate: false,
  approveSelfAttDailyRequestByEmployee: false,
  approveAttDailyRequestByDelegate: false,
  cancelAttDailyApprovalByEmployee: false,
  cancelAttDailyApprovalByDelegate: false,
  approveSelfAttRequestByEmployee: false,

  submitAttRequestByDelegate: false,
  cancelAttRequestByDelegate: false,
  approveAttRequestByDelegate: false,
  cancelAttApprovalByEmployee: false,
  cancelAttApprovalByDelegate: false,

  manageOverallSetting: false,
  switchCompany: false,
  manageDepartment: false,
  manageEmployee: false,
  manageCalendar: false,
  manageJobType: false,
  manageJob: false,
  manageMobileSetting: false,
  managePermission: false,
  managePlannerSetting: false,
  manageAttLeave: false,
  manageAttShortTimeWorkSetting: false,
  manageAttLeaveOfAbsence: false,
  manageAttWorkingType: false,
  manageAttPattern: false,
  manageAttAgreementAlertSetting: false,
  manageAttLeaveGrant: false,
  manageAttShortTimeWorkSettingApply: false,
  manageAttLeaveOfAbsenceApply: false,
  manageAttPatternApply: false,
  manageTimeSetting: false,
  manageTimeWorkCategory: false,

  manageExpTypeGroup: false,
  manageExpenseType: false,
  manageTaxType: false,
  manageExpSetting: false,
  manageExchangeRate: false,
  manageAccountingPeriod: false,
  manageReportType: false,
  manageCostCenter: false,
  manageVendor: false,
  manageExtendedItem: false,
  manageEmployeeGroup: false,
  manageExpCustomHint: false,
};

const hasAllowedPermission = (
  userPermission: Permission,
  allowedPermissions: $Keys<Permission>[]
) => allowedPermissions.some((permission) => userPermission[permission]);

export const buildCheckerByUserPermission = (userPermission: Permission) => (
  allowedPermissions: $Keys<Permission>[]
) => hasAllowedPermission(userPermission, allowedPermissions);

export const isPermissionSatisfied = (conditions: TotalTestConditions) => {
  const hasPermission = (required: $Keys<Permission>[]) =>
    hasAllowedPermission(conditions.userPermission, required);

  return conditions.isByDelegate
    ? conditions.allowIfByDelegate ||
        (conditions.requireIfByDelegate &&
          hasPermission(conditions.requireIfByDelegate)) ||
        false
    : conditions.allowIfByEmployee ||
        (conditions.requireIfByEmployee &&
          hasPermission(conditions.requireIfByEmployee)) ||
        false;
};

export const isAvailableTimeTrack = (
  userPermission: Permission,
  userSetting: UserSetting,
  isDelegated: boolean = false
): boolean => {
  if (isDelegated) {
    const hasDelegatePermission = isPermissionSatisfied({
      userPermission,
      requireIfByDelegate: ['viewTimeTrackByDelegate'],
      allowIfByEmployee: true,
      isByDelegate: true,
    });

    return userSetting.useWorkTime && hasDelegatePermission;
  } else {
    return userSetting.useWorkTime;
  }
};

export default defaultPermission;
