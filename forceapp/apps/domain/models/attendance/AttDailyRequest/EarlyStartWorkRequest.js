// @flow

import { type BaseAttDailyRequest } from './BaseAttDailyRequest';
import { type WorkingType, getEarlyStartWorkTimeRange } from '../WorkingType';
import { type CodeMap, CODE } from '../AttDailyRequestType';

import { compose } from '../../../../commons/utils/FnUtil';

export type EarlyStartWorkRequest = BaseAttDailyRequest & {
  type: $PropertyType<CodeMap, 'EarlyStartWork'>,

  /**
   * 開始日
   */
  startDate: string,

  /**
   * 開始時間
   */
  startTime: number | null,

  /**
   * 終了時間
   */
  endTime: number | null,
};

/**
 *  開始日のデフォルト値の設定します。
 */
const defaultStartDate = (targetDate: string | null) => (
  request: EarlyStartWorkRequest
) => {
  if (!targetDate || request.startDate) {
    return request;
  }
  return { ...request, startDate: targetDate };
};

/**
 * 勤務体系から基本設定を作成します。
 */
const defaultValue = (workingType: WorkingType | null = null) => (
  request: EarlyStartWorkRequest
): EarlyStartWorkRequest => {
  if (request.id || !workingType) {
    return request;
  }
  return {
    ...request,
    ...getEarlyStartWorkTimeRange(workingType),
  };
};

/**
 * AttDailyRequest から EarlyStartWorkRequest を作成します。
 */
const formatAttDailyRequest = (
  request: BaseAttDailyRequest
): EarlyStartWorkRequest => ({
  ...request,
  type: CODE.EarlyStartWork,
});

export const update = (
  target: EarlyStartWorkRequest,
  key: string,
  value: $Values<EarlyStartWorkRequest>
): EarlyStartWorkRequest => ({
  ...target,
  [key]: value,
});

export const create = (
  request: BaseAttDailyRequest,
  workingType: WorkingType | null = null,
  targetDate: string | null = null
): EarlyStartWorkRequest =>
  (compose(
    defaultStartDate(targetDate),
    defaultValue(workingType),
    formatAttDailyRequest
  )(request): React.ComponentType<Object>);
