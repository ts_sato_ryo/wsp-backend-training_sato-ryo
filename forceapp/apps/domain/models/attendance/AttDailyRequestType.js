// @flow

/**
 * Leave - 休暇申請
 * HolidayWork - 休日出勤申請
 * OvertimeWork - 残業申請
 * EarlyStartWork - 早朝勤務申請
 * Direct - 直行・直帰申請
 * Pattern - 勤務時間変更
 * Absence - 欠勤申請
 */
export type CodeMap = {|
  None: 'None',
  Leave: 'Leave',
  HolidayWork: 'HolidayWork',
  OvertimeWork: 'OvertimeWork',
  EarlyStartWork: 'EarlyStartWork',
  Direct: 'Direct',
  Pattern: 'Pattern',
  Absence: 'Absence',
|};
export type Code = $Values<CodeMap>;

export const CODE: CodeMap = {
  None: 'None',
  Leave: 'Leave',
  HolidayWork: 'HolidayWork',
  OvertimeWork: 'OvertimeWork',
  EarlyStartWork: 'EarlyStartWork',
  Direct: 'Direct',
  Pattern: 'Pattern',
  Absence: 'Absence',
};

export type AttDailyRequestType = $ReadOnly<{|
  // 申請タイプコード
  code: Code,
  // 申請タイプ名
  name: string,
|}>;

export type AttDailyRequestTypeMap = {
  [Code]: AttDailyRequestType,
};

export const DisplayOrder: Code[] = [
  'Leave',
  'HolidayWork',
  'OvertimeWork',
  'EarlyStartWork',
  'Direct',
  'Pattern',
  'Absence',
];

export const ReapplyableTypes: Code[] = ['HolidayWork', 'Leave'];
