// @flow
import { uniq } from 'lodash';
import { type HolidayWorkRequest } from './AttDailyRequest/HolidayWorkRequest';
import { type WorkingType } from './WorkingType';

export type SubstituteLeaveType = 'None' | 'Substitute';

export const SUBSTITUTE_LEAVE_TYPE: {
  [SubstituteLeaveType]: SubstituteLeaveType,
} = {
  None: 'None',
  Substitute: 'Substitute',
};

export const ORDER_OF_SUBSTITUTE_LEAVE_TYPES = [
  SUBSTITUTE_LEAVE_TYPE.None,
  SUBSTITUTE_LEAVE_TYPE.Substitute,
];

export const create = (
  request: HolidayWorkRequest | null = null,
  workingType: WorkingType | null = null
) => {
  const substituteLeaveTypeList = [SUBSTITUTE_LEAVE_TYPE.None];

  if (
    workingType &&
    workingType.holidayWorkConfig &&
    workingType.holidayWorkConfig.substituteLeaveTypeList
  ) {
    substituteLeaveTypeList.push(
      ...workingType.holidayWorkConfig.substituteLeaveTypeList
    );
  }

  if (request && request.substituteLeaveType) {
    substituteLeaveTypeList.push(request.substituteLeaveType);
  }

  return (uniq(substituteLeaveTypeList): SubstituteLeaveType[]);
};
