// @flow
import { type AttDailyRequest } from './AttDailyRequest';
import { type AttLeave } from './AttLeave';
/**
 * Annual - 年次有給休暇
 * Paid - 有休
 * Unpaid - 無休
 * Substitute - 振替
 * Compensatory - 代休
 */
export type LeaveType =
  | 'Annual'
  | 'Paid'
  | 'Unpaid'
  | 'Substitute'
  | 'Compensatory';

// eslint-disable-next-line import/prefer-default-export
export const LEAVE_TYPE: { [LeaveType]: LeaveType } = {
  Annual: 'Annual',
  Paid: 'Paid',
  Unpaid: 'Unpaid',
  Substitute: 'Substitute',
  Compensatory: 'Compensatory',
};

export const doesAttLeaveExist = (
  attLeaveList: AttLeave[],
  target: AttDailyRequest
): boolean => {
  const found: ?AttLeave = attLeaveList.find(
    ({ code }) => code === target.leaveCode
  );
  return typeof found !== 'undefined';
};
