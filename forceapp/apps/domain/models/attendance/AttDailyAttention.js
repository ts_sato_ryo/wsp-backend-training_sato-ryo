// @flow
/**
 * 注意喚起メッセージを生成するのに必要な情報を保持するモデルです。
 * 各画面で表示するメッセージが異なるため、ここでは各注意喚起のコードと値のみを生成しております。
 *
 * メッセージを BE で生成するのか FE で生成するのかが現在で揺れております。
 * また、元々、BE でメッセージを生成していたので、このようなメッセージ生成モデルの置き場所が決まっておりません。
 * 規制が整いましたら、このファイルを然るべき場所へ移動、もしくは修正、削除をお願いいたします。
 */
import {
  isRealWorkingTimeOnEffectiveWorkingTime,
  isRealStartTimeOnEffectiveWorkingTime,
  isRealEndTimeOnEffectiveWorkingTime,
  hasInsufficientRestTime,
} from './AttDailyRecord';

export type CodeMap = {
  IneffectiveWorkingTime: 'IneffectiveWorkingTime',
  InsufficientRestTime: 'InsufficientRestTime',
};

export type Code = $Values<CodeMap>;

export const CODE: CodeMap = {
  IneffectiveWorkingTime: 'IneffectiveWorkingTime',
  InsufficientRestTime: 'InsufficientRestTime',
};

type IneffectiveWorkingTime = {
  code: $PropertyType<CodeMap, 'IneffectiveWorkingTime'>,
  value: {
    fromTime: number,
    toTime: number,
  },
};

type InsufficientRestTime = {
  code: $PropertyType<CodeMap, 'InsufficientRestTime'>,
  value: number,
};

export type AttDailyAttention = IneffectiveWorkingTime | InsufficientRestTime;

const createIneffectiveWorkingTimeAttention = (
  fromTime: number,
  toTime: number
): IneffectiveWorkingTime => ({
  code: CODE.IneffectiveWorkingTime,
  value: {
    fromTime,
    toTime,
  },
});

const createInsufficientRestTime = (
  insufficientRestTime: number
): InsufficientRestTime => ({
  code: CODE.InsufficientRestTime,
  value: insufficientRestTime,
});

export function createAttDailyAttentions(record: {
  startTime: number | null,
  endTime: number | null,
  outStartTime: number | null,
  outEndTime: number | null,
  insufficientRestTime: number | null,
}): AttDailyAttention[] {
  const attention = [];

  if (!isRealWorkingTimeOnEffectiveWorkingTime(record)) {
    attention.push(
      createIneffectiveWorkingTimeAttention(
        record.startTime + 0,
        record.endTime + 0
      )
    );
  } else {
    if (!isRealStartTimeOnEffectiveWorkingTime(record)) {
      attention.push(
        createIneffectiveWorkingTimeAttention(
          record.startTime + 0,
          record.outStartTime + 0
        )
      );
    }
    if (!isRealEndTimeOnEffectiveWorkingTime(record)) {
      attention.push(
        createIneffectiveWorkingTimeAttention(
          record.outEndTime + 0,
          record.endTime + 0
        )
      );
    }
  }
  if (hasInsufficientRestTime(record)) {
    attention.push(createInsufficientRestTime(record.insufficientRestTime + 0));
  }

  return attention;
}
