// @flow

import { type Timesheet } from '../../Timesheet';
import { CODE } from '../../AttDailyRequestType';

export const defaultTimesheet = {
  id: 'A0101bbbddd',
  recordAllRecordDates: [
    '2019-01-04',
    '2019-01-05',
    '2019-01-06',
    '2019-01-07',
    '2019-01-08',
  ],
  recordsByRecordDate: {
    '2019-01-04': {},
    '2019-01-05': {},
    '2019-01-06': {},
    '2019-01-07': {},
    '2019-01-08': {},
  },
  periods: [
    {
      name: '2019年1月',
      startDate: '2019-01-01',
      endDate: '2019-01-31',
    },
  ],
  requestTypes: {
    [CODE.Absence]: {
      code: CODE.Absence,
      name: '欠勤',
    },
    [CODE.Leave]: {
      code: CODE.Leave,
      name: '休暇',
    },
    [CODE.HolidayWork]: {
      code: CODE.HolidayWork,
      name: '休日出勤',
    },
    [CODE.OvertimeWork]: {
      code: CODE.OvertimeWork,
      name: '残業',
    },
    [CODE.EarlyStartWork]: {
      code: CODE.EarlyStartWork,
      name: '早朝勤務',
    },
    [CODE.LateArrival]: {
      code: CODE.LateArrival,
      name: '遅刻',
    },
    [CODE.EarlyLeave]: {
      code: CODE.EarlyLeave,
      name: '早退',
    },
    [CODE.ScheduleChange]: {
      code: CODE.ScheduleChange,
      name: '勤務時間変更',
    },
    [CODE.WorkTimeModify]: {
      code: CODE.WorkTimeModify,
      name: '勤務時刻修正',
    },
    [CODE.Direct]: {
      code: CODE.Direct,
      name: '直行直帰',
    },
  },
  requestAllIds: ['A0', 'B0', 'C0', 'D0', 'E0', 'F0', 'G0', 'H0', 'I0', 'J0'],
  requestsById: {
    A0: {},
    B0: {},
    C0: {},
    D0: {},
    E0: {},
    F0: {},
    G0: {},
    H0: {},
    I0: {},
    J0: {},
  },
  employeeName: 'UNIT TEST employee name',
  departmentName: 'UNIT TEST department name',
  workingTypeName: 'UNIT TEST working type',
  startDate: '2019-01-01',
  endDate: '2019-01-31',
  requestId: 'ABC00',
  status: 'Not Requested',
  approver01Name: 'UNIT TEST BOSS',
  isLocked: false,
  workingType: {},
  isAllAbsent: false,
};

export const createTimesheet = <+T: Timesheet>(customValue: T): Timesheet => ({
  ...defaultTimesheet,
  ...customValue,
});
