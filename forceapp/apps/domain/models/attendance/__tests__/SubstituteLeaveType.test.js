// @flow

import {
  type SubstituteLeaveType,
  SUBSTITUTE_LEAVE_TYPE,
  create,
} from '../SubstituteLeaveType';
import { create as createHolidayWorkRequest } from '../AttDailyRequest/HolidayWorkRequest';
import { defaultValue as attDailyRequestDefaultValue } from '../AttDailyRequest/BaseAttDailyRequest';
import {
  type WorkingType,
  defaultValue as workingTypeDefaultValue,
} from '../WorkingType';

describe('domain/models/attendance/SubstituteLeaveType', () => {
  describe('create', () => {
    test('No parameter', () => {
      const list = create();
      expect(list).toStrictEqual([SUBSTITUTE_LEAVE_TYPE.None]);
    });

    describe('With HolidayWorkRequest', () => {
      test.each(
        ([
          [null, [SUBSTITUTE_LEAVE_TYPE.None]],
          [SUBSTITUTE_LEAVE_TYPE.None, [SUBSTITUTE_LEAVE_TYPE.None]],
          [
            SUBSTITUTE_LEAVE_TYPE.Substitute,
            [SUBSTITUTE_LEAVE_TYPE.None, SUBSTITUTE_LEAVE_TYPE.Substitute],
          ],
        ]: [SubstituteLeaveType | null, SubstituteLeaveType[]][])
      )('substituteLeaveType: %s = [%s]', (substituteLeaveType, expected) => {
        const request = createHolidayWorkRequest({
          ...attDailyRequestDefaultValue,
          substituteLeaveType,
        });
        const list = create(request);
        expect(list).toStrictEqual(expected);
      });
    });

    describe('With WorkingType', () => {
      test.each(
        ([
          [[], [SUBSTITUTE_LEAVE_TYPE.None]],
          [[SUBSTITUTE_LEAVE_TYPE.None], [SUBSTITUTE_LEAVE_TYPE.None]],
          [
            [SUBSTITUTE_LEAVE_TYPE.Substitute],
            [SUBSTITUTE_LEAVE_TYPE.None, SUBSTITUTE_LEAVE_TYPE.Substitute],
          ],
          [['abc'], [SUBSTITUTE_LEAVE_TYPE.None, 'abc']],
          [['abc', 'def'], [SUBSTITUTE_LEAVE_TYPE.None, 'abc', 'def']],
        ]: [string[], string[]][])
      )(
        'hokidayWorkConfig.substituteLeaveType: [%s] = [%s]',
        (substituteLeaveTypeList, expected) => {
          const workingType = ({
            ...workingTypeDefaultValue,
            holidayWorkConfig: {
              substituteLeaveTypeList,
            },
          }: WorkingType);
          const list = create(undefined, workingType);
          expect(list).toStrictEqual(expected);
        }
      );
    });

    describe('With HolidayWorkRequest and WorkingType', () => {
      test.each(
        ([
          [null, [], [SUBSTITUTE_LEAVE_TYPE.None]],
          [null, [SUBSTITUTE_LEAVE_TYPE.None], [SUBSTITUTE_LEAVE_TYPE.None]],
          [
            SUBSTITUTE_LEAVE_TYPE.None,
            [SUBSTITUTE_LEAVE_TYPE.None],
            [SUBSTITUTE_LEAVE_TYPE.None],
          ],
          [
            SUBSTITUTE_LEAVE_TYPE.None,
            [SUBSTITUTE_LEAVE_TYPE.Substitute],
            [SUBSTITUTE_LEAVE_TYPE.None, SUBSTITUTE_LEAVE_TYPE.Substitute],
          ],
          [
            SUBSTITUTE_LEAVE_TYPE.Substitute,
            [SUBSTITUTE_LEAVE_TYPE.None],
            [SUBSTITUTE_LEAVE_TYPE.None, SUBSTITUTE_LEAVE_TYPE.Substitute],
          ],
          [
            SUBSTITUTE_LEAVE_TYPE.Substitute,
            [SUBSTITUTE_LEAVE_TYPE.Substitute],
            [SUBSTITUTE_LEAVE_TYPE.None, SUBSTITUTE_LEAVE_TYPE.Substitute],
          ],
        ]: [SubstituteLeaveType | null, string[], string[]][])
      )(
        '\n substituteLeaveType: %s \n hokidayWorkConfig.substituteLeaveType: [%s] \n   = [%s]',
        (substituteLeaveType, substituteLeaveTypeList, expected) => {
          const request = createHolidayWorkRequest({
            ...attDailyRequestDefaultValue,
            substituteLeaveType,
          });
          const workingType = ({
            ...workingTypeDefaultValue,
            holidayWorkConfig: {
              substituteLeaveTypeList,
            },
          }: WorkingType);
          const list = create(request, workingType);
          expect(list).toStrictEqual(expected);
        }
      );
    });
  });
});
