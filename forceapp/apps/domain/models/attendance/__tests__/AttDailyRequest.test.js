// @flow

import {
  EDIT_ACTION,
  DISABLE_ACTION,
  createFromDefaultValue,
  isForReapply,
  getAvailableRequestTypesAt,
  getLatestRequestsAt,
  getPerformableEditAction,
  getPerformableDisableAction,
} from '../AttDailyRequest';
import { defaultValue } from '../AttDailyRequest/BaseAttDailyRequest';
import { LEAVE_RANGE } from '../LeaveRange';
import { type AttDailyRequestTypeCode, CODE } from '../AttDailyRequestType';
import STATUS from '../../approval/request/Status';

import { createTimesheet } from './mocks/timesheet';

const createRequest = (request: { requestTypeCode: AttDailyRequestTypeCode }) =>
  createFromDefaultValue(
    {
      [request.requestTypeCode]: {
        code: request.requestTypeCode,
        name: 'Request',
      },
    },
    request.requestTypeCode,
    {
      ...defaultValue,
      ...request,
    }
  );

describe('domain/models/attendance/AttDailyRequest', () => {
  describe('getAvaiableRequestTypesAt', () => {
    test('should return avaiable request types', () => {
      // Arrange
      const dailyRecord = {
        requestTypeCodes: [CODE.HolidayWork, CODE.OvertimeWork],
      };
      const requestTypes = {
        [CODE.HolidayWork]: {
          code: CODE.HolidayWork,
          name: '休日出勤',
        },
        [CODE.OvertimeWork]: {
          code: CODE.OvertimeWork,
          name: '残業',
        },
        [CODE.EarlyStartWork]: {
          code: CODE.EarlyStartWork,
          name: '早朝勤務',
        },
        [CODE.LateArrival]: {
          code: CODE.LateArrival,
          name: '遅刻',
        },
      };
      const timesheet = createTimesheet({
        requestTypes,
      });
      const expected = {
        [CODE.HolidayWork]: {
          code: CODE.HolidayWork,
          name: '休日出勤',
        },
        [CODE.OvertimeWork]: {
          code: CODE.OvertimeWork,
          name: '残業',
        },
      };

      // Execute
      const actual = getAvailableRequestTypesAt(dailyRecord, timesheet);

      // Assert
      expect(actual).toEqual(expected);
    });

    test('when timesheet is locked, this method is return the empty object', () => {
      // Arrange
      const dailyRecord = {
        requestTypeCodes: [CODE.HolidayWork, CODE.OvertimeWork],
      };
      const requestTypes = {
        [CODE.HolidayWork]: {
          code: CODE.HolidayWork,
          name: '休日出勤',
        },
        [CODE.OvertimeWork]: {
          code: CODE.OvertimeWork,
          name: '残業',
        },
        [CODE.EarlyStartWork]: {
          code: CODE.EarlyStartWork,
          name: '早朝勤務',
        },
        [CODE.LateArrival]: {
          code: CODE.LateArrival,
          name: '遅刻',
        },
      };
      const timesheet = createTimesheet({
        requestTypes,
        isLocked: true,
      });

      const expected = {};

      // Execute
      const actual = getAvailableRequestTypesAt(dailyRecord, timesheet);

      // Assert
      expect(actual).toEqual(expected);
    });
  });
  describe('getLatestRequestsAt', () => {
    describe('when timesheet is locked', () => {
      test('should return submitted requests excluding reapplying requests', () => {
        // Arrange
        const requestsById = {
          A: {
            id: 'A',
            originalRequestId: 'E',
            isForReapply: false,
          },
          B: {
            id: 'B',
            isForReapply: false,
          },
          C: {
            id: 'C',
            isForReapply: false,
          },
          D: {
            id: 'D',
            isForReapply: false,
          },
          E: {
            id: 'E',
            isForReapply: true,
          },
        };
        const timesheet = createTimesheet({
          requestsById,
          isLocked: true,
        });
        const expected = [
          {
            id: 'D',
            isForReapply: false,
          },
        ];

        // Execute
        const actual = getLatestRequestsAt(
          {
            requestIds: ['D', 'E'],
          },
          timesheet
        );

        // Assert
        expect(actual).toEqual(expected);
      });
    });
    describe('when timesheet is not locked', () => {
      test('should return submitted requests including reapplying requests', () => {
        // Arrange
        const requestsById = {
          A: {
            id: 'A',
            originalRequestId: 'E',
            isForReapply: true,
          },
          B: {
            id: 'B',
            isForReapply: false,
          },
          C: {
            id: 'C',
            isForReapply: false,
          },
          D: {
            id: 'D',
            isForReapply: true,
          },
          E: {
            id: 'E',
            isForReapply: false,
          },
        };
        const timesheet = createTimesheet({
          requestsById,
          isLocked: false,
        });
        const expected = [
          {
            id: 'D',
            isForReapply: true,
          },
          {
            id: 'A',
            originalRequestId: 'E',
            isForReapply: true,
          },
        ];

        // Execute
        const actual = getLatestRequestsAt(
          {
            requestIds: ['D', 'A', 'E'],
          },
          timesheet
        );

        // Assert
        expect(actual).toEqual(expected);
      });
    });
  });

  describe('getPerformableEditAction', () => {
    test.each([
      [
        {
          status: STATUS.NotRequested,
        },
        EDIT_ACTION.Create,
      ],
      [
        {
          status: STATUS.ApprovalIn,
        },
        EDIT_ACTION.None,
      ],
      [
        {
          status: STATUS.Approved,
          requestTypeCode: CODE.HolidayWork,
        },
        EDIT_ACTION.Reapply,
      ],
      [
        {
          status: STATUS.Approved,
        },
        EDIT_ACTION.None,
      ],
      [
        {
          status: STATUS.Rejected,
        },
        EDIT_ACTION.Modify,
      ],
      [
        {
          status: STATUS.Removed,
        },
        EDIT_ACTION.Modify,
      ],
      [
        {
          status: STATUS.Canceled,
        },
        EDIT_ACTION.Modify,
      ],
    ])('%o\n%s', (request, result) => {
      expect(
        getPerformableEditAction({
          ...defaultValue,
          ...request,
        })
      ).toBe(result);
    });
  });

  describe('getPerfomableDisableAction', () => {
    test.each([
      [
        {
          status: STATUS.NotRequested,
        },
        DISABLE_ACTION.None,
      ],
      [
        {
          status: STATUS.ApprovalIn,
        },
        DISABLE_ACTION.CancelRequest,
      ],
      [
        {
          status: STATUS.Approved,
        },
        DISABLE_ACTION.CancelApproval,
      ],
      [
        {
          status: STATUS.Rejected,
        },
        DISABLE_ACTION.Remove,
      ],
      [
        {
          status: STATUS.Removed,
        },
        DISABLE_ACTION.Remove,
      ],
      [
        {
          status: STATUS.Canceled,
        },
        DISABLE_ACTION.Remove,
      ],
    ])('%o\n%s', (request, result) => {
      expect(
        getPerformableDisableAction({
          ...defaultValue,
          ...request,
        })
      ).toBe(result);
    });
  });

  describe('isForReapply', () => {
    const reapply = [CODE.HolidayWork, CODE.Leave];
    const notReapply = Object.keys(CODE).filter(
      (code) => code !== CODE.None && !reapply.includes(code)
    );
    const modify = [STATUS.Rejected, STATUS.Removed, STATUS.Canceled];

    describe.each(notReapply)('%s', (code) => {
      test.each(Object.keys(STATUS))('status: %s', (status) => {
        const request = createRequest({
          requestTypeCode: code,
          status,
        });
        expect(isForReapply(request)).toBe(false);
      });
    });

    describe('HolidayWork', () => {
      test.each(Object.keys(STATUS))('status: %s', (status) => {
        const request = createRequest({
          requestTypeCode: CODE.HolidayWork,
          status,
        });
        if (status === STATUS.Approved) {
          expect(isForReapply(request)).toBe(true);
        } else {
          expect(isForReapply(request)).toBe(false);
        }
      });
      describe('isForReapply', () => {
        test.each(Object.keys(STATUS))('status: %s', (status) => {
          if (STATUS.Approved === status) {
            return;
          }
          const request = createRequest({
            requestTypeCode: CODE.HolidayWork,
            isForReapply: true,
            status,
          });
          if (modify.includes(status)) {
            expect(isForReapply(request)).toBe(true);
          } else {
            expect(isForReapply(request)).toBe(false);
          }
        });
      });
    });

    describe('LeaveRequest', () => {
      test.each(Object.keys(LEAVE_RANGE))('leaveRange: %s', (leaveRange) => {
        const request = createRequest({
          requestTypeCode: CODE.Leave,
          status: STATUS.Approved,
          leaveRange,
        });
        if (LEAVE_RANGE.Day === leaveRange) {
          expect(isForReapply(request)).toBe(true);
        } else {
          expect(isForReapply(request)).toBe(false);
        }
      });

      test.each(Object.keys(STATUS))('status: %s', (status) => {
        const request = createRequest({
          requestTypeCode: CODE.Leave,
          leaveRange: LEAVE_RANGE.Day,
          status,
        });
        if (status === STATUS.Approved) {
          expect(isForReapply(request)).toBe(true);
        } else {
          expect(isForReapply(request)).toBe(false);
        }
      });

      describe('isForReapply', () => {
        test.each(Object.keys(STATUS))('status: %s', (status) => {
          if (STATUS.Approved === status) {
            return;
          }
          const request = createRequest({
            requestTypeCode: CODE.Leave,
            leaveRange: LEAVE_RANGE.Day,
            isForReapply: true,
            status,
          });
          if (modify.includes(status)) {
            expect(isForReapply(request)).toBe(true);
          } else {
            expect(isForReapply(request)).toBe(false);
          }
        });
      });
    });
  });
});
