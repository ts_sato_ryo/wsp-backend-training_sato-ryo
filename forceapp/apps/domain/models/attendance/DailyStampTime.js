/* @flow */

import Api from '../../../commons/api';

export type ClockType = 'CLOCK_IN' | 'CLOCK_OUT' | 'CLOCK_REIN';

export const CLOCK_TYPE = {
  CLOCK_IN: 'CLOCK_IN',
  CLOCK_OUT: 'CLOCK_OUT',
  CLOCK_REIN: 'CLOCK_REIN',
};

export type StampAction = 'in' | 'out' | 'rein';

export const STAMP_ACTION = {
  IN: 'in',
  OUT: 'out',
  REIN: 'rein',
};

export type StampSource = 'web' | 'mobile';
export const STAMP_SOURCE = {
  WEB: 'web',
  MOBILE: 'mobile',
};

export type DailyStampTime = {
  isEnableStartStamp: boolean,
  isEnableEndStamp: boolean,
  isEnableRestartStamp: boolean,
  defaultAction: ?StampAction,
};

export type EditingDailyStampTime = {
  isEnableStartStamp: boolean,
  isEnableEndStamp: boolean,
  isEnableRestartStamp: boolean,
  mode: ?ClockType,
  message: string,
};

export type DailyStampTimeResult = {
  insufficientRestTime: ?number,
};

type FromRemote = {
  isEnableStartStamp: boolean,
  isEnableEndStamp: boolean,
  isEnableRestartStamp: boolean,
  defaultAction: ?StampAction,
};

const convertFromRemote = (fromRemote: FromRemote) => ({
  isEnableStartStamp: fromRemote.isEnableStartStamp,
  isEnableEndStamp: fromRemote.isEnableEndStamp,
  isEnableRestartStamp: fromRemote.isEnableRestartStamp,
  defaultAction: fromRemote.defaultAction,
});

export const fetchDailyStampTime = (
  employeeId: ?string = null
): Promise<DailyStampTime> =>
  Api.invoke({
    path: '/att/daily-time/get',
    param: {
      empId: employeeId,
    },
  }).then((response: FromRemote) => convertFromRemote(response));

export type PostStampRequest = {
  mode: ClockType,
  message?: ?string,
  latitude?: number,
  longitude?: number,
};

export const postStamp = (
  stampWidget: PostStampRequest,
  source: StampSource
): Promise<DailyStampTimeResult> => {
  const CLOCK_TYPE_MAP = {
    [CLOCK_TYPE.CLOCK_IN]: STAMP_ACTION.IN,
    [CLOCK_TYPE.CLOCK_OUT]: STAMP_ACTION.OUT,
    [CLOCK_TYPE.CLOCK_REIN]: STAMP_ACTION.REIN,
  };
  const req = {
    path: '/att/daily-time/stamp',
    param: {
      clockType: CLOCK_TYPE_MAP[stampWidget.mode],
      comment: stampWidget.message,
      latitude:
        stampWidget.latitude !== null && stampWidget.latitude !== undefined
          ? stampWidget.latitude
          : null,
      longitude:
        stampWidget.longitude !== null && stampWidget.longitude !== undefined
          ? stampWidget.longitude
          : null,
      source,
    },
  };
  return Api.invoke(req);
};
