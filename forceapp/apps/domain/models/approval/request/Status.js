/* @flow */
export type ApprovalStatus = $ReadOnly<{|
  NotRequested: 'NotRequested',
  /**
   * 申請中
   */
  Pending: 'Pending',
  /**
   * 申請取消
   */
  Removed: 'Removed',
  /**
   * 却下
   */
  Rejected: 'Rejected',
  /**
   * 承認待ち
   */
  ApprovalIn: 'Approval In',
  /**
   * 承認済み
   */
  Approved: 'Approved',
  /**
   * 承認取消
   */
  Canceled: 'Canceled',
  /**
   * 承認内容変更承認待ち
   */
  Reapplying: 'Reapplying',
|}>;

export type Status = $Values<ApprovalStatus>;

const STATUS: ApprovalStatus = {
  NotRequested: 'NotRequested',
  /**
   * 申請中
   */
  Pending: 'Pending',
  /**
   * 申請取消
   */
  Removed: 'Removed',
  /**
   * 却下
   */
  Rejected: 'Rejected',
  /**
   * 承認待ち
   */
  ApprovalIn: 'Approval In',
  /**
   * 承認済み
   */
  Approved: 'Approved',
  /**
   * 承認取消
   */
  Canceled: 'Canceled',
  /**
   * 承認内容変更承認待ち
   */
  Reapplying: 'Reapplying',
};

export const ORDER_OF_STATUS = [
  STATUS.Canceled,
  STATUS.Rejected,
  STATUS.Removed,
  STATUS.ApprovalIn,
  STATUS.Approved,
];

export default STATUS;
