// @flow

import {
  type AttDailyDetailBaseFromApi,
  type AttDailyDetailBaseForStore,
  type Request,
} from './Base';

/**
 * Holiday Work
 *
 * 休日出勤申請にのみ存在する項目の type
 */
export type HolidayWork = {
  type: 'HolidayWork',
  startDate: string, // 開始日
  endDate: string, // 終了日
  startTime: ?number, // 開始時刻
  endTime: ?number, // 終了時刻
  substituteLeaveType: ?string, // 休日出勤取得休日タイプ
  substituteDate: ?string, // 振替休日取得日
};

// TODO
// Merge the following types into one type,
// becuase those two types have same structure.

export type HolidayWorkApi = AttDailyDetailBaseFromApi<HolidayWork>;

export type HolidayWorkStore = AttDailyDetailBaseForStore<HolidayWork>;

/**
 * The body of request
 */
export type HolidayWorkRequest = Request<HolidayWorkStore>;
