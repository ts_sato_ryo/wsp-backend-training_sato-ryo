/* @flow */

import type { Status as ApprovalStatus } from '../request/Status';
import type { ApprovalHistoryList } from '../request/History';

/**
 * The base type of daily requests
 *
 * API で返却される申請の共通部分
 */
export type BaseAttDailyDetail = {
  id: string,
  status: ApprovalStatus | '',
  employeeName: string,
  employeePhotoUrl: string,
  delegatedEmployeeName: ?string,
  comment: ?string, // NOTE unimplementend and unused.
  // type: string, // request type
  typeLabel: string, // label of r request type
  remarks: ?string,
};

/**
 * The base type returned by API
 *
 * API で返却されるデータのベース type
 */
export type AttDailyDetailBaseFromApi<TRequest> = {
  request: BaseAttDailyDetail & TRequest,
  originalRequest?: BaseAttDailyDetail & TRequest,
} & ApprovalHistoryList;

/**
 * The base type saved into Redux Store
 *
 * Store に保存する申請の共通部分
 */
export type BaseAttDailyDetailForStore = {
  id: string, // 申請ID
  status: ApprovalStatus | '', // 申請ステータス
  employeeName: string, // 申請者名
  employeePhotoUrl: string, // 申請者顔写真URL
  delegatedEmployeeName: ?string, // 代理申請者名
  comment?: ?string, // 申請時コメント（未実装、未使用）
  // type: string, // 申請種別 // 各タイプで定義
  typeLabel: string, // 申請種別（表示用）
  remarks: ?string, // 備考
};

/**
 * The base type saved into Redux Store
 *
 * Store に保存するデータのベース type
 */
export type AttDailyDetailBaseForStore<TRequest> = {
  request: BaseAttDailyDetailForStore & TRequest,
  originalRequest?: BaseAttDailyDetailForStore & TRequest,
} & ApprovalHistoryList;

/**
 * The latest request
 */
export type Request<T> = $PropertyType<T, 'request'>;

/**
 * The orignal request
 */
export type OriginalRequest<T> = $PropertyType<T, 'originalRequest'>;

/**
 * ラベルタイプ
 */
export type Label = {
  label: string,
  value: number | string,
  valueType?: 'date' | 'datetime' | 'text' | 'longtext',
  originalValue?: number | string,
};

/**
 * ラベルタイプの配列
 */
export type ArrayLabel = Array<Label>;
