// @flow
import _ from 'lodash';
import moment from 'moment';
import Decimal from 'decimal.js';

import Api from '../../../commons/api';
import { type ApprovalStatus } from '../approval/request/Status';

import { type Record } from './Record';
import { type ExtendedItemExpectedList, initialEmptyEIs } from './ExtendedItem';
import {
  type SortBy,
  type OrderBy,
  type SearchConditions,
  financeStatusListAllParam,
  financeStatusListInitParam,
  requestDateInitVal,
} from './FinanceApproval';

export type Report = {
  subject: string,
  accountingDate: string,
  accountingPeriodId?: ?string,
  attachedFileVerId: ?string,
  attachedFileId: ?string,
  attachedFileData: ?string,
  fileName?: string,
  useFileAttachment: boolean,
  totalAmount: number,
  reportNo?: string,
  reportId?: ?string,
  requestId?: string,
  requestDate?: string,
  preRequestId?: string,
  preRequest?: Report,
  employeeHistoryId?: string,
  status?: $Values<ApprovalStatus>,
  remarks?: string,
  records: Array<Record>,
  selectedAccountingPeriod?: string,
  jobId: ?string,
  jobName: string,
  jobCode: string,
  vendorId: ?string,
  vendorCode: string,
  vendorName: string,
  paymentDueDate: string,
  paymentDueDateUsage: ?string,
  costCenterHistoryId: ?string,
  costCenterName: string,
  costCenterCode: string,
  scheduledDate?: string,
  purpose?: string,
  expReportTypeId?: ?string,
  expReportTypeName?: ?string,
  departmentCode?: string,
  departmentName?: string,
  employeeName?: string,
  ...$Exact<ExtendedItemExpectedList>,
};

export type ReportListItem = {
  subject: string,
  reportId: string,
  reportNo: string,
  requestDate: string,
  status: string,
  totalAmount: number,
  vendorRequiredFor: ?string,
  vendorId: ?string,
  chosen: boolean,
};

export type ReportList = Array<ReportListItem>;

// Pagination
export type ReportIdList = Array<string>;
export type ExpReportIds = {
  totalSize: number,
  reportIdList: ReportIdList,
};

// Other status are not to be displayed in report summary screen
export const headerStatusLabels = [
  'NotRequested',
  'Pending',
  'Removed',
  'Rejected',
  'Canceled',
  'Approved',
  'Claimed',
  'AccountingAuthorized',
  'AccountingRejected',
  'JournalCreated',
  'Fully Paid',
];

export const initialStateReport = {
  subject: '',
  accountingDate: '',
  accountingPeriodId: null,
  attachedFileVerId: null,
  attachedFileId: null,
  attachedFileData: null,
  fileName: 'receipt',
  useFileAttachment: false,
  totalAmount: 0,
  jobId: null,
  jobName: '',
  jobCode: '',
  costCenterHistoryId: null,
  reportId: null,
  costCenterName: '',
  costCenterCode: '',
  remarks: '',
  records: [],
  vendorId: null,
  vendorCode: '',
  vendorName: '',
  paymentDueDate: '',
  paymentDueDateUsage: null,
  expReportTypeId: null,
  ...initialEmptyEIs(),
};

export const initialStatePreRequest = {
  subject: '',
  accountingDate: moment(new Date()).format('YYYY-MM-DD'),
  attachedFileVerId: null,
  attachedFileId: null,
  attachedFileData: null,
  fileName: 'receipt',
  useFileAttachment: false,
  scheduledDate: '',
  totalAmount: 0,
  jobId: null,
  jobName: '',
  jobCode: '',
  costCenterHistoryId: null,
  costCenterName: '',
  costCenterCode: '',
  purpose: '',
  records: [],
  vendorId: null,
  vendorCode: '',
  vendorName: '',
  paymentDueDate: '',
  paymentDueDateUsage: null,
  expReportTypeId: null,
  ...initialEmptyEIs(),
};

export const updateFiledValue = (state: Report, key: string, value: any) => {
  const cloneState = _.cloneDeep(state);
  cloneState[key] = value;
  return cloneState;
};

export const calcTotalAmount = (
  report: Report,
  decimalPlaces: number = 6
): number => {
  const totalAmount: Decimal = report.records.reduce(
    (total: Decimal, record): Decimal =>
      Decimal.add(total, record.items[0].amount),
    new Decimal(0)
  );
  return totalAmount.toFixed(decimalPlaces, Decimal.ROUND_DOWN);
};

// Pagination
export const getReportIdList = (
  isApproved: boolean = true,
  sortBy?: ?SortBy,
  order?: ?OrderBy,
  advSearchConditions?: ?SearchConditions
): Promise<ExpReportIds> => {
  let financeStatusList;
  if (advSearchConditions) {
    if (
      advSearchConditions.financeStatusList &&
      advSearchConditions.financeStatusList.length > 0
    ) {
      financeStatusList = advSearchConditions.financeStatusList;
    } else {
      financeStatusList = financeStatusListAllParam;
    }
  } else {
    financeStatusList = financeStatusListInitParam;
  }
  return Api.invoke({
    path: '/exp/report/id-list',
    param: {
      isApproved,
      sortBy,
      order,
      searchFilter: {
        financeStatusList,
        departmentBaseIds: advSearchConditions
          ? advSearchConditions.departmentBaseIds
          : null,
        empBaseIds: advSearchConditions ? advSearchConditions.empBaseIds : null,
        requestDateRange: advSearchConditions
          ? advSearchConditions.requestDateRange
          : requestDateInitVal(),
        accountingDateRange: advSearchConditions
          ? advSearchConditions.accountingDateRange
          : null,
        amountRange: advSearchConditions
          ? advSearchConditions.amountRange
          : null,
        reportNo: advSearchConditions ? advSearchConditions.reportNo : null,
      },
    },
  }).then((response: ExpReportIds) => response);
};

export const getReportList = (
  reportIds?: ReportIdList
): Promise<ReportList> => {
  return Api.invoke({
    path: '/exp/report/list',
    param: {
      reportIds,
      isMobile: false,
    },
  }).then((response: { reports: ReportList }) => response.reports);
};

export const getReportListMobile = (empId?: ?string): Promise<ReportList> => {
  return Api.invoke({
    path: '/exp/report/list',
    param: {
      empId,
      isMobile: true,
    },
  }).then((response: { reports: ReportList }) => response.reports);
};

export const getReport = (
  reportId: ?string,
  usedIn?: string
): Promise<Report> => {
  return Api.invoke({
    path: '/exp/report/get',
    param: {
      reportId,
      usedIn,
    },
  }).then((response: Report) => response);
};

export const deleteReport = (reportId: string): Promise<any> => {
  return Api.invoke({
    path: '/exp/report/delete',
    param: {
      reportId,
    },
  }).then((response: any) => response);
};

export const saveReport = (report: Report): Promise<Object> => {
  // remove records for better API performance.
  const reportCopy = Object.assign({}, report);
  delete reportCopy.records;
  // remove image data
  delete reportCopy.attachedFileData;

  return (
    Api.invoke({
      path: '/exp/report/save',
      param: reportCopy,
    })
      // response is save reportId
      .then((response: string) => response)
      .catch((err) => {
        throw err;
      })
  );
};

// Temporary Solution. To be removed once mobile api is updated.
export const saveReportMobile = (report: Report): Promise<void> => {
  const reportCopy = Object.assign({}, report);

  // get recordIds from records
  const recordIds = reportCopy.records.map((record) => record.recordId);
  reportCopy.recordIds = recordIds;

  // remove records for better API performance.
  delete reportCopy.records;
  // remove image data
  delete reportCopy.attachedFileData;

  return (
    Api.invoke({
      path: '/exp/report/save',
      param: reportCopy,
    })
      // response is save reportId
      .then((response: string) => response)
      .catch((err) => {
        throw err;
      })
  );
};

export const createReportFromRequest = (report: Report): Promise<void> => {
  return (
    Api.invoke({
      path: '/exp/pre-request/create-report',
      param: {
        reportId: report.preRequestId,
      },
    })
      // response is save reportId
      .then((response: string) => response)
      .catch((err) => {
        throw err;
      })
  );
};

export const cloneRequest = (reportId: string): Promise<void> => {
  return Api.invoke({
    path: '/exp/pre-request/clone',
    param: {
      reportId,
    },
  })
    .then((response: string) => response)
    .catch((err) => {
      throw err;
    });
};

export const cloneReport = (reportId: string): Promise<void> => {
  return Api.invoke({
    path: '/exp/report/clone',
    param: {
      reportId,
    },
  })
    .then((response: string) => response)
    .catch((err) => {
      throw err;
    });
};

export const getApprovedRequestReport = (reportId: ?string) => {
  return Api.invoke({
    path: '/exp/report/approved-request/get',
    param: {
      reportId,
    },
  })
    .then((res: Report) => res)
    .catch((err) => {
      throw err;
    });
};

export type PreRequestList = Array<Report>;

export const getPreRequestIdList = (
  isApproved: boolean = true
): Promise<ExpReportIds> => {
  return Api.invoke({
    path: '/exp/pre-request/id-list',
    param: {
      isApproved,
    },
  }).then((response: ExpReportIds) => response);
};

export const getPreRequestList = (
  reportIds?: ReportIdList
): Promise<PreRequestList> => {
  return Api.invoke({
    path: '/exp/pre-request/list',
    param: {
      reportIds,
    },
  }).then((response: { reports: ReportList }) => response.reports);
};

export const getPreRequest = (
  reportId: ?string,
  usedIn?: string
): Promise<any> => {
  return Api.invoke({
    path: '/exp/pre-request/get',
    param: {
      reportId,
      usedIn,
    },
  })
    .then((res: Report) => res)
    .catch((err) => {
      throw err;
    });
};

export const savePreRequest = (report: Report): Promise<any> => {
  return Api.invoke({
    path: '/exp/pre-request/save',
    param: report,
  })
    .then((response: any) => response)
    .catch((err) => {
      throw err;
    });
};

export const deletePreRequest = (reportId: ?string): Promise<void> => {
  return Api.invoke({
    path: '/exp/pre-request/delete',
    param: {
      reportId,
    },
  }).catch((err) => {
    throw err;
  });
};
