// @flow
import Api from '../../../commons/api';

import { type Employee } from '../organization/Employee';

export type EmployeeList = Array<Employee>;

// eslint-disable-next-line import/prefer-default-export
export const getEmployeeList = (
  companyId: ?string,
  targetDate: ?string
): Promise<EmployeeList> => {
  return Api.invoke({
    path: '/employee/search',
    param: {
      companyId,
      targetDate,
    },
  }).then((result: { records: EmployeeList }) => {
    return result.records;
  });
};
