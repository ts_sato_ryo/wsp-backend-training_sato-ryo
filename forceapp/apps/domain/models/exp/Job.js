// @flow
import Api from '../../../commons/api';

export type Job = {
  id: string,
  code: string,
  name: string,
  hierarchyParentNameList: string[],
};

export type JobList = Array<Job>;

export const getJobList = (
  empId: ?string,
  parentId: ?string,
  targetDate: ?string
): Promise<JobList> => {
  return Api.invoke({
    path: '/exp/job/get',
    param: {
      empId,
      parentId,
      targetDate,
    },
  }).then((response: { jobList: JobList }) => {
    return response.jobList;
  });
};

export const searchJob = (
  empId: ?string,
  name?: ?string,
  targetDate: string,
  query: ?string
): Promise<JobList> => {
  return Api.invoke({
    path: '/exp/job/get',
    param: {
      empId,
      targetDate,
      query,
    },
  }).then((response: { jobList: JobList }) => {
    return response.jobList;
  });
};

export const searchJobByKeyword = (
  empId: ?string,
  name?: ?string,
  targetDate: string,
  query: ?string,
  usedIn: ?string
): Promise<JobList> => {
  return Api.invoke({
    path: '/job/search',
    param: {
      empId,
      targetDate,
      query,
      usedIn,
    },
  }).then((response: { records: JobList }) => {
    return response.records;
  });
};

export const getRecentlyUsed = (
  targetDate: ?string,
  employeeBaseId: string
): Promise<JobList> => {
  return Api.invoke({
    path: '/job/recently-used/list',
    param: {
      targetDate,
      employeeBaseId,
    },
  }).then((response: { records: JobList }) => {
    return response.records;
  });
};
