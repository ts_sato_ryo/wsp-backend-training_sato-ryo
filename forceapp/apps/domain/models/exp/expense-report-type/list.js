// @flow
import Api from '../../../../commons/api';

import type { ExtendedItemExpectedList } from '../ExtendedItem';

// Export Type Definition
export type ExpenseReportType = {
  name?: string,
  id: string,
  description?: string,
  companyId?: string,
  code?: string,
  active?: boolean,
  isVendorRequired?: string,
  isCostCenterRequired?: string,
  isJobRequired?: string,
  vendorUsedIn?: boolean,
  expTypeIds?: Array<string>,
  expTypeList?: Array<Object>,
  ...$Exact<ExtendedItemExpectedList>,
};

export type ExpenseReportTypeList = Array<ExpenseReportType>;

// eslint-disable-next-line
export const getReportTypeList = (
  companyId: string,
  usedIn?: string,
  active: ?boolean,
  empId?: string,
  withExpTypeName: ?boolean,
  startDate: ?string,
  endDate: ?string
) => {
  return Api.invoke({
    path: '/exp/report-type/search',
    param: {
      companyId,
      usedIn,
      active,
      withExpTypeName,
      startDate,
      endDate,
      empId,
    },
  });
};
