import { calculateTax } from '../TaxType';

describe('calculateTax(testParam)', () => {
  const TEST_DATA = [
    // decimalPlaces : 0
    //   tax rate : 0% (tax free)
    {
      decimalPlaces: 0,
      rate: 0,
      amount: 100,
      expectedAmountExclTax: 100,
      expectedTax: 0,
    },
    //   tax rate : 8% (Japan)
    {
      decimalPlaces: 0,
      rate: 8,
      amount: 107,
      expectedAmountExclTax: 100,
      expectedTax: 7,
    },
    {
      decimalPlaces: 0,
      rate: 8,
      amount: 108,
      expectedAmountExclTax: 100,
      expectedTax: 8,
    },
    {
      decimalPlaces: 0,
      rate: 8,
      amount: 109,
      expectedAmountExclTax: 101,
      expectedTax: 8,
    },
    {
      decimalPlaces: 0,
      rate: 8,
      amount: 1012,
      expectedAmountExclTax: 938,
      expectedTax: 74,
    },
    {
      decimalPlaces: 0,
      rate: 8,
      amount: 1013,
      expectedAmountExclTax: 938,
      expectedTax: 75,
    },
    // decimalPlaces : 2
    //   tax rate : 0% (tax free)
    {
      decimalPlaces: 2,
      rate: 0,
      amount: 10,
      expectedAmountExclTax: 10,
      expectedTax: 0,
    },
    //   tax rate : 7% (Singapore)
    // Singapore GST should be rounded off(四捨五入).
    // But now only rounding down is supported. so the results differ from the expectations.
    // Once we support the rounding setting, test below with a 'round off' setting.
    //  { decimalPlaces: 2, rate: 7, amount: 10.7, expectedAmountExclTax: 10, expectedTax: 0.7},
    //  { decimalPlaces: 2, rate: 7, amount: 10.8, expectedAmountExclTax: 10.09, expectedTax: 0.71},
  ];

  test('Execute all the tax calculation with TEST_DATA', () => {
    TEST_DATA.forEach((param) => {
      const ans = calculateTax(param.rate, param.amount, param.decimalPlaces);

      expect(parseFloat(ans.amountWithoutTax)).toBe(
        param.expectedAmountExclTax
      );
      expect(parseFloat(ans.gstVat)).toBe(param.expectedTax);
    });
  });
});
