// @flow
import Api from '../../../../../commons/api';

import { type ApprovalHistory } from '../../../approval/request/History';

export type ExpApprovalHistory = ApprovalHistory;

// Exp Approval Hitory
export type ExpApprovalHistoryList = { historyList: Array<ExpApprovalHistory> };

// eslint-disable-next-line import/prefer-default-export
export const getExpApprovalHistoryLit = (
  requestId: string
): Promise<ExpApprovalHistoryList> => {
  return Api.invoke({
    path: '/exp/request/history/get',
    param: {
      requestId,
    },
  }).then((response: ExpApprovalHistoryList) => response);
};
