// @flow
import { cloneDeep } from 'lodash';
import Decimal from 'decimal.js';
import Api from '../../../commons/api';

import type { StationInfo } from './jorudan/Station';
import type { RouteItem } from './jorudan/Route';
import { type ExtendedItemExpectedList, getEIsOnly } from './ExtendedItem';
import { FOREIGN_CURRENCY_USAGE } from './foreign-currency/Currency';

export type currencyInfo = {
  code: string,
  decimalPlaces: number,
  name: string,
  symbol: string,
};

export const NEW_RECORD_OPTIONS = {
  MANUAL: 'manualRecord',
  OCR_RECORD: 'ocrRecord',
};

// 内訳
export type RecordItem = {
  itemId?: string | null,
  recordDate: string,
  taxRate?: number,
  expTypeId: string,
  expTypeName: string,
  amount: number,
  remarks: string,
  taxTypeBaseId: string,
  taxTypeHistoryId: string,
  taxTypeName: ?string,
  withoutTax: number,
  gstVat: number,
  taxManual: boolean,
  currencyId: ?string,
  localAmount: number,
  exchangeRate: number,
  currencyInfo: currencyInfo,
  exchangeRateManual: boolean,
  originalExchangeRate: number,
  useForeignCurrency: boolean,
  fixedForeignCurrencyId: string,
  useFixedForeignCurrency: boolean,
  costCenterHistoryId: ?string,
  costCenterName: string,
  costCenterCode: string,
  jobId: ?string,
  jobName: string,
  jobCode: string,
  fixedAllowanceOptionId: string | null,
  fixedAllowanceOptionLabel: string | null,
  ...$Exact<ExtendedItemExpectedList>,
}; // 経費内訳ID // 費目ID // 費目名 // 税込金額 // 摘要

export type ViaList = Array<StationInfo | null>;
// export type ViaList = Array<any>;

export const MAX_LENGTH_VIA_LIST = 4;

export type RouteInfo = {
  roundTrip: boolean,
  origin: ?StationInfo,
  viaList: ViaList,
  arrival: ?StationInfo,
  selectedRoute: ?RouteItem,
};

export const RECORD_TYPE = {
  General: 'General',
  TransitJorudanJP: 'TransitJorudanJP',
  HotelFee: 'HotelFee',
  FixedAllowanceSingle: 'FixedAllowanceSingle',
  FixedAllowanceMulti: 'FixedAllowanceMulti',
};

export const RECEIPT_TYPE = {
  Optional: 'Optional',
  Required: 'Required',
  NotUsed: 'NotUsed',
};

export type RecordType =
  | 'General'
  | 'TransitJorudanJP'
  | 'HotelFee'
  | 'FixedAllowanceSingle'
  | 'FixedAllowanceMulti';

export type ReceiptType = 'Optional' | 'Required' | 'NotUsed';

export type Record = {
  recordId?: ?string, // 明細ID
  recordDate: string, // 利用日
  currencyName: string, // 通貨名
  items: Array<RecordItem>, // 内訳
  recordType: string,
  fileAttachment: string,
  routeInfo?: ?RouteInfo,
  amount: number, // 税込金額
  remarks?: string, // 摘要
  order?: number, // 並び順
  withoutTax: number,
  receiptId?: ?string,
  receiptFileId?: ?string,
  receiptData?: ?string,
  fileName: string,
  dataType: ?string,
  // for saving single record, we need report/requestId to link it with its parent Report
  reportId?: string,
  requestId?: string,
}; // 明細ID // 利用日 // 通貨名 // 内訳 // 税込金額 // 摘要 // 並び順

export type RecordUpdateInfo = {
  expenseTypeName: string,
  recordDate: string,
  isForeignCurrency: string,
};

export type RecordUpdateInfoList = Array<RecordUpdateInfo>;

export const newRouteInfo = {
  roundTrip: false,
  origin: null,
  viaList: [],
  arrival: null,
  selectedRoute: null,
};

export const newRecordItem = (
  expTypeId: string,
  expTypeName: string,
  useForeignCurrency: boolean,
  expType: any,
  initTaxTypeBaseIdIsBlank: boolean,
  fixedForeignCurrencyId?: string,
  foreignCurrencyUsage?: string,
  amount?: number = 0
) => ({
  itemId: null,
  expTypeName, // 費目名
  expTypeId, // 費目ID
  useForeignCurrency,
  amount, // 税込金額
  remarks: '', // 摘要
  withoutTax: 0,
  taxManual: false,
  gstVat: 0,
  taxTypeBaseId: initTaxTypeBaseIdIsBlank ? '' : 'noIdSelected',
  taxTypeHistoryId: '',
  taxTypeName: '',
  currencyId: null,
  localAmount: 0,
  exchangeRate: 0,
  originalExchangeRate: 0,
  exchangeRateManual: false,
  fixedAllowanceOptionId: null,
  fixedAllowanceOptionLabel: null,
  currencyInfo: {
    code: '',
    decimalPlaces: 0,
    name: '',
    symbol: '',
  },
  useFixedForeignCurrency:
    foreignCurrencyUsage === FOREIGN_CURRENCY_USAGE.Fixed,
  fixedForeignCurrencyId,
  ...getEIsOnly(expType),
});

export const newRecord = (
  expTypeId: string = '',
  expTypeName: string = '',
  recordType: string = '',
  useForeignCurrency: boolean = false,
  item: any = null,
  initTaxTypeBaseIdIsBlank: boolean = false,
  fileAttachment: string = '',
  fixedForeignCurrencyId: string = '',
  foreignCurrencyUsage: string = FOREIGN_CURRENCY_USAGE.NotUsed,
  amount: number = 0,
  recordDate: string = '',
  receiptId: string | null = null,
  receiptFileId: string | null = null,
  receiptData: string | null = null,
  dataType: string | null = null
) => ({
  recordId: null,
  recordDate,
  currencyName: 'JPY', // 通貨名
  items: [
    newRecordItem(
      expTypeId,
      expTypeName,
      useForeignCurrency,
      item,
      initTaxTypeBaseIdIsBlank,
      fixedForeignCurrencyId,
      foreignCurrencyUsage,
      amount
    ),
  ], // 内訳
  remarks: '', // 摘要
  routeInfo: recordType === 'TransitJorudanJP' ? newRouteInfo : null,
  recordType, // 明細種別
  fileAttachment,
  amount: 0, // 税込金額
  withoutTax: 0,
  receiptId,
  receiptFileId,
  receiptData,
  fileName: 'receipt',
  dataType,
});

export const newRecordTouched = {
  recordId: false, // 明細ID
  recordDate: false,
  expTypeId: false, // 費目ID
  amount: false, // 税込金額
  remarks: false, // 摘要
  taxTypeBaseId: false,
  taxTypeHistoryId: false,
  withoutTax: false,
  gstVat: false,
  taxManual: false,
  receiptData: null,
};

export const newRecordItemTouched = {
  amount: false,
  remarks: false,
  withoutTax: false,
};

export const isRecordItemized = (
  recordType: string,
  isChildRecord?: boolean
) => {
  return recordType === 'HotelFee' && !isChildRecord;
};

export const isFixedAllowanceSingle = (recordType: string) =>
  recordType === RECORD_TYPE.FixedAllowanceSingle;

export const isFixedAllowanceMulti = (recordType: string) =>
  recordType === RECORD_TYPE.FixedAllowanceMulti;

export const calcItemsTotalAmount = (
  items: Array<RecordItem>,
  key: string,
  decimalPlaces: number = 6
): number => {
  const totalAmount: Decimal = items.reduce(
    (total: Decimal, item): Decimal => Decimal.add(total, item[key] || 0),
    new Decimal(0)
  );
  return totalAmount.toFixed(decimalPlaces, Decimal.ROUND_DOWN);
};

export const isAmountMatch = (
  amountA: string | number,
  amountB: string | number
) => {
  return new Decimal(amountA).equals(amountB);
};

export const saveRecord = (
  values: Record,
  reportId?: string,
  reportTypeId?: string
): Promise<any> => {
  const record = cloneDeep(values);
  if (!record.reportId) {
    record.reportId = reportId;
  }
  // remove image data
  delete record.receiptData;

  return Api.invoke({
    path: '/exp/report/record/save',
    param: { ...record, reportTypeId },
  }).then((response: any) => response);
};

export const savePreRequestRecord = (
  values: Record,
  reportId?: string,
  reportTypeId: string
): Promise<any> => {
  const record = cloneDeep(values);
  record.requestId = reportId;
  return Api.invoke({
    path: '/exp/pre-request/record/save',
    param: { ...record, reportTypeId },
  }).then((response: any) => response);
};

export const saveFARecord = (
  values: Record,
  reportId?: string,
  requestId?: string,
  reportTypeId: string
): Promise<any> => {
  const record = cloneDeep(values);
  record.reportId = reportId;
  record.requestId = requestId;

  return Api.invoke({
    path: '/exp/finance-approval/record/save',
    param: { ...record, reportTypeId },
  }).then((response: any) => response);
};

export const deleteRecord = (recordIds: Array<string>): Promise<any> => {
  return Api.invoke({
    path: '/exp/report/record/delete',
    param: {
      recordIds,
    },
  }).then((response: any) => response);
};

export const deletePreRequestRecord = (
  recordIds: Array<string>
): Promise<any> => {
  return Api.invoke({
    path: '/exp/pre-request/record/delete',
    param: {
      recordIds,
    },
  }).then((response: any) => response);
};

export const getRecordList = (
  empId: string = '',
  expTypeIdList: ?Array<string>,
  startDate: ?string,
  endDate: ?string
): Promise<Object> => {
  return Api.invoke({
    path: '/exp/report/record/list',
    param: {
      empId,
      expTypeIdList,
      startDate,
      endDate,
    },
  });
};

export const uploadReceipt = (
  fileName: string,
  fileBody: ArrayBuffer
): Promise<any> => {
  return Api.invoke({
    path: '/exp/receipt/save',
    param: {
      fileName,
      fileBody,
      type: 'Receipt',
    },
  });
};

export const cloneRecord = (
  recordIds: Array<string>,
  targetDates: Array<string>
): Promise<any> => {
  return Api.invoke({
    path: '/exp/report/record/clone',
    param: {
      recordIds,
      targetDates,
    },
  });
};

export const clonePreRequestRecord = (
  recordIds: Array<string>,
  targetDates: Array<string>
): Promise<any> => {
  return Api.invoke({
    path: '/exp/pre-request/record/clone',
    param: {
      recordIds,
      targetDates,
    },
  });
};

export const warnings = ['Exp_Warn_AttRecordNotFound'];
