// @flow
import { some } from 'lodash';

import Api from '../../../commons/api';

import SALESFORCE_API_VERSION from '../../../commons/config/salesforceApiVersion';
import FileUtil from '../../../commons/utils/FileUtil';

export type SelectedReceipt = {
  receiptId: string,
  receiptFileId: string,
  receiptData: string,
  ocrInfo: Object,
};

export const OCR_STATUS = {
  COMPLETED: 'Completed',
  IN_PROGRESS: 'InProgress',
  NOT_PROCESSED: 'NotProcessed',
  QUEUED: 'Queued',
};

export type Base64File = {
  name: string,
  data: string,
  size: number,
  type: string,
};

export type Base64FileList = Array<Base64File>;

// SF Rest API Response
type ErrorResponseBody = Array<{
  message: string,
  errorCode: string,
  fields?: Array<string>,
}>;

type SuccessResponseBody = {
  id: string,
  success: boolean,
  errors: Array<string>,
};

type CompositeResponseContent = {
  body: SuccessResponseBody | ErrorResponseBody,
  httpHeaders: {
    Location?: string,
  },
  httpStatusCode: number,
  referenceId: string,
};
export type CompositeResponsResponse = {
  compositeResponse: Array<CompositeResponseContent>,
};

export const SERVICE_API_PATH = `/services/data/v${SALESFORCE_API_VERSION}`;

export const MAX_FILE_SIZE = 5242880;

export const VALID_FILE_TYPES = [
  'image/jpeg',
  'image/png',
  'application/pdf',
  'image/gif',
];

const requestComposite = async (compositeRequest) => {
  const res = await (Api.requestSFApi(`${SERVICE_API_PATH}/composite/`, {
    allOrNone: true,
    compositeRequest,
  }).catch((err) => {
    const error = {
      errorCode: err.errorCode,
      message: err.message,
      stackTrace: null,
    };
    throw error;
  }): CompositeResponsResponse);
  const { compositeResponse } = res;
  if (!compositeResponse) {
    throw new Error('Some Unknown Error Happened.');
  }
  return compositeResponse;
};

export const uploadReceipt = async (files: Base64FileList): Promise<any> => {
  const compositeRequest = files.map((file, idx) => ({
    method: 'POST',
    url: `${SERVICE_API_PATH}/sobjects/ContentVersion`,
    referenceId: `receipt_${idx}`,
    body: {
      Title: `Receipt_${FileUtil.getFileNameWithoutExtension(file.name)}`,
      PathOnClient: file.name,
      VersionData: file.data.substr(file.data.indexOf(',') + 1),
    },
  }));

  const compositeResponse = await requestComposite(compositeRequest).catch(
    (err) => {
      throw err;
    }
  );
  if (
    // upload is rollbacked when some error happened in any one of receipt.
    some(
      compositeResponse,
      (comRes) => comRes.httpStatusCode !== 200 && comRes.httpStatusCode !== 201
    )
  ) {
    const errorMessage = compositeResponse
      .map((comRes, idx) => `${files[idx].name} : ${comRes.body[0].message}`)
      .join('\n');

    const error = {
      errorCode: 'UPLOAD_FAILED',
      message: errorMessage,
      stackTrace: null,
    };
    throw error;
  }
  return compositeResponse.map((comRes) => comRes.body.id);
};

// sample
//
// SF Api success response
//
// compositeResponse: [
//   {
//     body: { id: '0687F00000U5dHOQAZ', success: true, errors: [] },
//     httpHeaders: {
//       Location:
//         '/services/data/v40.0/sobjects/ContentVersion/0687F00000U5dHOQAZ',
//     },
//     httpStatusCode: 201,
//     referenceId: 'receipt_0',
//   },
//   {
//     body: { id: '0687F00000U5dHPQAZ', success: true, errors: [] },
//     httpHeaders: {
//       Location:
//         '/services/data/v40.0/sobjects/ContentVersion/0687F00000U5dHPQAZ',
//     },
//     httpStatusCode: 201,
//     referenceId: 'receipt_1',
//   },
// ],

// SF Api error response
//
//  compositeResponse: [
//    {
//      body: [
//        {
//          message: 'Please input values: [VersionData]',
//          errorCode: 'REQUIRED_FIELD_MISSING',
//          fields: ['VersionData'],
//        },
//      ],
//      httpHeaders: {},
//      httpStatusCode: 400,
//      referenceId: 'receipt_0',
//    },
//    {
//      body: [
//        {
//          errorCode: 'PROCESSING_HALTED',
//          message:
//            'The transaction was rolled back since another operation in the same transaction failed.',
//        },
//      ],
//      httpHeaders: {},
//      httpStatusCode: 400,
//      referenceId: 'receipt_1',
//    },
//  ]

export const getDocumentId = async (ids: string[]): Promise<string[]> => {
  const compositeRequest = ids.map((id) => ({
    method: 'GET',
    url: `${SERVICE_API_PATH}/sobjects/ContentVersion/${id}`,
    referenceId: id,
  }));
  const compositeResponse = await requestComposite(compositeRequest).catch(
    (err) => {
      throw err;
    }
  );
  if (
    some(
      compositeResponse,
      (comRes) => comRes.httpStatusCode !== 200 && comRes.httpStatusCode !== 201
    )
  ) {
    const errorMessage = compositeResponse
      .map((comRes, idx) => `${ids[idx]} : ${comRes.body[0].message}`)
      .join('\n');

    const error = {
      errorCode: 'UPLOAD_FAILED',
      message: errorMessage,
      stackTrace: null,
    };
    throw error;
  }
  return compositeResponse.map((comRes) => comRes.body.ContentDocumentId);
};
