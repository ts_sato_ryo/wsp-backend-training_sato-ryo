// @flow
import Api from '../../../commons/api';

// Custom Hint
export type CustomHint = {
  moduleType: string,
  reportHeaderAccountingPeriod?: string,
  reportHeaderReportType?: string,
  reportHeaderRecordDate?: string,
  reportHeaderJob?: string,
  reportHeaderCostCenter?: string,
  reportHeaderVendor?: string,
  reportHeaderRemarks?: string,
  reportHeaderScheduledDate?: string,
  reportHeaderPurpose?: string,
  recordDate?: string,
  requestRecordDate?: string,
  recordExpenseType?: string,
  recordJob?: string,
  recordCostCenter?: string,
  recordSummary?: string,
  recordReceipt?: string,
  recordQuotation?: string,
};

// eslint-disable-next-line import/prefer-default-export
export const getCustomHint = (
  companyId: string,
  moduleType: string
): Promise<CustomHint> => {
  return Api.invoke({
    path: '/custom-hint/get',
    param: { companyId, moduleType },
  });
};
