// @flow
import Api from '../../../commons/api';

import { type Department } from '../organization/Department';

export type Manager = {
  name: string,
};

export type Parent = {
  name: string,
};

export type DepartmentList = Array<Department>;

// eslint-disable-next-line import/prefer-default-export
export const getDepartmentList = (
  companyId: ?string,
  targetDate: ?string
): Promise<DepartmentList> => {
  return Api.invoke({
    path: '/department/search',
    param: {
      companyId,
      targetDate,
    },
  }).then((result: { records: DepartmentList }) => {
    return result.records;
  });
};
