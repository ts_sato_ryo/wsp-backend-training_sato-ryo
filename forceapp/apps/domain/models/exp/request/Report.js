// @flow
import moment from 'moment';
import Api from '../../../../commons/api';

import { type RouteInfo, type RecordItem } from '../Record';
import {
  type ExtendedItemExpectedList,
  initialEmptyEIs,
} from '../ExtendedItem';
import { type ApprovalHistory } from '../../approval/request/History';

export type ExtendItemInfo = {
  name: string,
  inputType: string,
  limitLength: string,
  isRequired: string,
  defaultValueText: string,
  extendedItemCustomId: string,
  description: string,
  picklist: Array<{ value: string, label: string }>,
};

// 経費明細
export type ExpRequestRecord = {
  recordId: string, // 経費明細ID
  recordDate: string, // 利用日	YYYY-MM-DD形式の文字列
  currencyName: string, // 通貨名
  amount: number, // 合計金額
  order: number, // 明細の並び順
  items: Array<RecordItem>, // 経費内訳
  withoutTax: number,
  recordType: string,
  routeInfo: RouteInfo,
  receiptId: ?string,
  receiptFileId: ?string,
  receiptData: ?string,
  fileName?: string,
  ...$Exact<ExtendedItemExpectedList>,
};

// 承認履歴
export type ExpRequestHistoryList = ApprovalHistory;

// 経費申請
export type ExpRequest = {
  requestId: string, // 申請ID
  status: string, // 申請ステータス
  employeeName: string, // 申請者名
  employeePhotoUrl: string, // 申請者顔写真URL
  comment: string, // 申請者コメント
  reportNo: string, // 申請番号
  subject: string, // 件名
  accountingDate: string, // 処理日 YYYY-MM-DD形式の文字列
  accountingPeriodId: string, // ID of Accounting Period
  attachedFileVerId: ?string,
  attachedFileId: ?string,
  attachedFileData?: ?string,
  fileName: string,
  dataType: ?string,
  delegatedEmployeeName?: string, // 代理承認者
  totalAmount: number, // 合計金額
  remarks: string, // 備考
  records: Array<ExpRequestRecord>,
  jobName: string,
  jobCode: string,
  isJobRequired?: Boolean,
  costCenterName: string,
  costCenterCode: string,
  isCostCenterRequired?: Boolean,
  historyList: Array<ExpRequestHistoryList>,
  recordingDate?: string,
  scheduledDate?: string,
  purpose?: string,
  expReportTypeName?: ?string,
  expPreRequestId: ?string,
  expPreRequest: ?ExpRequest,
  requestNo: ?string,
  ...$Exact<ExtendedItemExpectedList>,
  vendorId: ?string,
  vendorCode: string,
  vendorName: string,
  paymentDueDate: string,
  paymentDueDateUsage: ?string,
};

export type ExpRequestIdsInfo = {
  totalSize: number,
  requestIdList: Array<string>,
};

// 経費一覧
export type ExpRequestListItem = {
  requestId: string, // 申請ID
  employeeName: string, // 社員名
  photoUrl: string, // 社員の顔写真URL
  departmentName: string, // 部署名
  requestDate: string, // 申請日付
  reportNo: string, // 申請番号
  subject: string, // 件名
  totalAmount: number, // 合計金額
};

export type DateRangeOption = {
  startDate: ?string,
  endDate: ?string,
};

export type EmployeeOption = {
  label: string,
  value: string,
};

export type SearchConditions = {
  name?: string,
  statusList: Array<string>,
  empBaseIdList: Array<string>,
  requestDateRange: DateRangeOption,
};

export type ExpRequestList = Array<ExpRequestListItem>;

export const submitDateInitVal = (): DateRangeOption => {
  const firstDayOfLastMonth = moment(new Date())
    .add(-1, 'months')
    .startOf('months')
    .format('YYYY-MM-DD');
  const today = moment().format('YYYY-MM-DD');
  const resDateRange: DateRangeOption = {
    startDate: firstDayOfLastMonth,
    endDate: today,
  };
  return resDateRange;
};

// 申請一覧
export const initialStateExpRequestList = [];

export const initialSearchCondition = {
  statusList: ['Pending'],
  empBaseIdList: [],
  requestDateRange: submitDateInitVal(),
};

// 経費申請
export const initialStateExpRequest = {
  requestId: '', // 申請ID
  status: '', // 申請ステータス
  employeeName: '', // 申請者名
  employeePhotoUrl: '', // 申請者顔写真URL
  comment: '', // 申請者コメント
  reportNo: '', // 申請番号
  subject: '', // 件名
  accountingDate: '', // 処理日 YYYY-MM-DD形式の文字列
  accountingPeriodId: '', // ID of Accounting Period
  attachedFileVerId: null,
  attachedFileId: null,
  fileName: 'receipt',
  dataType: null,
  attachedFileData: null,
  totalAmount: 0, // 合計金額
  remarks: '', // 備考
  jobName: '', // Job
  jobCode: '',
  costCenterName: '',
  costCenterCode: '',
  records: [],
  historyList: [],
  expReportTypeName: null,
  expPreRequestId: null,
  expPreRequest: null,
  requestNo: null,
  ...initialEmptyEIs(),
  vendorId: null,
  vendorCode: '',
  vendorName: '',
  paymentDueDate: '',
  paymentDueDateUsage: null,
};

export const submitExpRequestReport = (reportId: string, comment: string) => {
  return Api.invoke({
    path: '/exp/request/report/submit',
    param: { reportId, comment },
  });
};

export const cancelExpRequestReport = (requestId: string, comment: string) => {
  return Api.invoke({
    path: '/exp/request/report/cancel-request',
    param: { requestId, comment },
  });
};

export const fetchExpRequestReportIds = (
  searchCondition: SearchConditions,
  empId?: string
) => {
  return Api.invoke({
    path: '/exp/request/report/id/list',
    param: {
      searchCondition,
      empId,
    },
  });
};

export const fetchExpRequestReportList = (
  requestIdList: Array<string>,
  empId?: string
) => {
  return Api.invoke({
    path: '/exp/request/report/list',
    param: { requestIdList, empId },
  }).then((res: { requestList: ExpRequestList }) => res.requestList);
};

export const getExpRequestReport = (requestId: ?string) => {
  return Api.invoke({
    path: '/exp/request/report/get',
    param: {
      requestId,
    },
  }).then((response: ExpRequest) => response);
};

export const submitExpPreRequestReport = (
  reportId: string,
  comment: string
): Promise<void> => {
  return Api.invoke({
    path: '/exp/request/pre-request/submit',
    param: { reportId, comment },
  }).catch((err) => {
    throw err;
  });
};

export const canExpRequestApproval = (requestId: string, comment: string) => {
  return Api.invoke({
    path: '/exp/request/pre-request/cancel-request',
    param: { requestId, comment },
  });
};

export const fetchExpPreRequestReportIds = (
  searchCondition: SearchConditions,
  empId?: string
): Promise<ExpRequestIdsInfo> => {
  return Api.invoke({
    path: '/exp/request/pre-request/id/list',
    param: { searchCondition, empId },
  });
};

export const fetchExpPreRequestReportList = (
  requestIdList: Array<string>,
  empId?: string
): Promise<ExpRequestList> => {
  return Api.invoke({
    path: '/exp/request/pre-request/list',
    param: { requestIdList, empId },
  })
    .then((res: any) => res.requestList)
    .catch((err) => {
      throw err;
    });
};

export const getExpPreRequestReport = (
  requestId: ?string
): Promise<ExpRequest> => {
  return Api.invoke({
    path: '/exp/request/pre-request/get',
    param: {
      requestId,
    },
  })
    .then((res: ExpRequest) => res)
    .catch((err) => {
      throw err;
    });
};
