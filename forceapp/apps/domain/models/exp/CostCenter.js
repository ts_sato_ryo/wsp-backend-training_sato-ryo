// @flow
import Api from '../../../commons/api';

export type CostCenter = {
  id: string,
  code: string,
  name: string,
  baseId: string,
  hasChildren: boolean,
  historyId: ?string,
  hierarchyParentNameList: string[],
};

export type DefaultCostCenter = {
  costCenterCode: string,
  costCenterName: string,
  costCenterHistoryId: string,
};

export type CostCenterList = Array<CostCenter>;

export const getCostCenterList = (
  companyId: ?string,
  parentId?: ?string,
  targetDate: ?string
): Promise<CostCenterList> => {
  return Api.invoke({
    path: '/exp/cost-center/get',
    param: {
      companyId,
      parentId,
      targetDate,
    },
  }).then((response: { costCenterList: CostCenterList }) => {
    return response.costCenterList;
  });
};

export const searchCostCenter = (
  companyId: ?string,
  parentId?: ?string,
  targetDate: string,
  query: ?string,
  usedIn: ?string
): Promise<CostCenterList> => {
  return Api.invoke({
    path: '/cost-center/search',
    param: {
      companyId,
      targetDate,
      query,
      usedIn,
    },
  }).then((response: { records: CostCenterList }) => {
    return response.records;
  });
};

// Correct API path
export const getRecentlyUsed = (
  employeeBaseId: string,
  targetDate: string
): Promise<CostCenterList> => {
  return Api.invoke({
    path: '/cost-center/recently-used/list',
    param: { employeeBaseId, targetDate },
  }).then((response: { records: CostCenterList }) => {
    return response.records;
  });
};
