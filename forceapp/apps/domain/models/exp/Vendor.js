// @flow
import Api from '../../../commons/api';

// follow ExtendedItem.js apps/domain/models/exp/ExtendedItem.js

export const VENDOR_PAYMENT_DUE_DATE_USAGE = {
  Optional: 'Optional',
  Required: 'Required',
  NotUsed: 'NotUsed',
};

export type VendorItem = {
  active: boolean,
  address: string,
  bankAccountNumber: string,
  bankAccountType: string,
  bankCode: string,
  bankName: string,
  branchAddress: string,
  branchCode: string,
  branchName: string,
  code: string,
  companyId: string,
  correspondentBankAddress: string,
  correspondentBankName: string,
  correspondentBranchName: string,
  correspondentSwiftCode: string,
  country: string,
  currencyCode: string,
  id: string,
  isWithholdingTax: boolean,
  name: string,
  payeeName: string,
  paymentTerm: string,
  paymentTermCode: ?string,
  swiftCode: string,
  zipCode: string,
};

export type VendorItemList = {
  records: Array<VendorItem>,
  hasMore: boolean,
};

export const getVendorList = (
  companyId: ?string,
  query: ?string
): Promise<VendorItemList> => {
  return Api.invoke({
    path: '/exp/vendor/search',
    param: {
      companyId,
      active: true,
      query,
    },
  }).then((response: VendorItemList) => {
    return response;
  });
};

export const getVendorDetail = (id: ?string): Promise<VendorItemList> => {
  return Api.invoke({
    path: '/exp/vendor/search',
    param: {
      id,
    },
  }).then((response: VendorItemList) => {
    return response;
  });
};

export const getRecentlyUsedVendor = (
  employeeBaseId: string
): Promise<VendorItemList> =>
  Api.invoke({
    path: '/exp/vendor/recently-used/list',
    param: { employeeBaseId },
  }).then((response: VendorItemList) => response);
