// @flow
import { cloneDeep, invert, find, pickBy, get } from 'lodash';
import Api from '../../../commons/api';
import DateUtil from '../../../commons/utils/DateUtil';

export type ExtendedItemId = string | null;
export type ExtendedItemValue = string | null;

export type ExtendedItemSelectedOptionName = string | null;

export type ExtendedItemInfoInputType = {
  value: string,
  label: ?string,
};

export type ExtendItemInfo = {
  name: string,
  inputType: string,
  limitLength: string,
  isRequired: boolean,
  defaultValueText: string,
  description: string,
  extendedItemCustomId: string,
  picklist: Array<{ value: string, label: string }>,
};

export type ExtendedItem = {
  index: string,
  id: string,
  info: ?ExtendItemInfo,
  name?: ?string,
  value: string,
};

export type CustomEIOption = {
  id: string,
  extendedItemCustomId: string,
  code: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
};

export type CustomEIOptionList = {
  records: Array<CustomEIOption>,
  hasMore: boolean,
};

export type ExtendedItemExpectedList = {
  extendedItemText01Id?: ?ExtendedItemId,
  extendedItemText01Value?: ?ExtendedItemValue,
  extendedItemText01Info?: ?ExtendItemInfo,
  extendedItemText02Id?: ?ExtendedItemId,
  extendedItemText02Value?: ?ExtendedItemValue,
  extendedItemText02Info?: ?ExtendItemInfo,
  extendedItemText03Id?: ?ExtendedItemId,
  extendedItemText03Value?: ?ExtendedItemValue,
  extendedItemText03Info?: ?ExtendItemInfo,
  extendedItemText04Id?: ?ExtendedItemId,
  extendedItemText04Value?: ?ExtendedItemValue,
  extendedItemText04Info?: ?ExtendItemInfo,
  extendedItemText05Id?: ?ExtendedItemId,
  extendedItemText05Value?: ?ExtendedItemValue,
  extendedItemText05Info?: ?ExtendItemInfo,
  extendedItemText06Id?: ?ExtendedItemId,
  extendedItemText06Value?: ?ExtendedItemValue,
  extendedItemText06Info?: ?ExtendItemInfo,
  extendedItemText07Id?: ?ExtendedItemId,
  extendedItemText07Value?: ?ExtendedItemValue,
  extendedItemText07Info?: ?ExtendItemInfo,
  extendedItemText08Id?: ?ExtendedItemId,
  extendedItemText08Value?: ?ExtendedItemValue,
  extendedItemText08Info?: ?ExtendItemInfo,
  extendedItemText09Id?: ?ExtendedItemId,
  extendedItemText09Value?: ?ExtendedItemValue,
  extendedItemText09Info?: ?ExtendItemInfo,
  extendedItemText10Id?: ?ExtendedItemId,
  extendedItemText10Value?: ?ExtendedItemValue,
  extendedItemText10Info?: ?ExtendItemInfo,

  extendedItemPicklist01Id?: ?ExtendedItemId,
  extendedItemPicklist01Value?: ?ExtendedItemValue,
  extendedItemPicklist01Info?: ?ExtendItemInfo,
  extendedItemPicklist02Id?: ?ExtendedItemId,
  extendedItemPicklist02Value?: ?ExtendedItemValue,
  extendedItemPicklist02Info?: ?ExtendItemInfo,
  extendedItemPicklist03Id?: ?ExtendedItemId,
  extendedItemPicklist03Value?: ?ExtendedItemValue,
  extendedItemPicklist03Info?: ?ExtendItemInfo,
  extendedItemPicklist04Id?: ?ExtendedItemId,
  extendedItemPicklist04Value?: ?ExtendedItemValue,
  extendedItemPicklist04Info?: ?ExtendItemInfo,
  extendedItemPicklist05Id?: ?ExtendedItemId,
  extendedItemPicklist05Value?: ?ExtendedItemValue,
  extendedItemPicklist05Info?: ?ExtendItemInfo,
  extendedItemPicklist06Id?: ?ExtendedItemId,
  extendedItemPicklist06Value?: ?ExtendedItemValue,
  extendedItemPicklist06Info?: ?ExtendItemInfo,
  extendedItemPicklist07Id?: ?ExtendedItemId,
  extendedItemPicklist07Value?: ?ExtendedItemValue,
  extendedItemPicklist07Info?: ?ExtendItemInfo,
  extendedItemPicklist08Id?: ?ExtendedItemId,
  extendedItemPicklist08Value?: ?ExtendedItemValue,
  extendedItemPicklist08Info?: ?ExtendItemInfo,
  extendedItemPicklist09Id?: ?ExtendedItemId,
  extendedItemPicklist09Value?: ?ExtendedItemValue,
  extendedItemPicklist09Info?: ?ExtendItemInfo,
  extendedItemPicklist10Id?: ?ExtendedItemId,
  extendedItemPicklist10Value?: ?ExtendedItemValue,
  extendedItemPicklist10Info?: ?ExtendItemInfo,

  extendedItemLookup01Id?: ?ExtendedItemId,
  extendedItemLookup01Value?: ?ExtendedItemValue,
  extendedItemLookup01SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup01Info?: ?ExtendItemInfo,
  extendedItemLookup02Id?: ?ExtendedItemId,
  extendedItemLookup02Value?: ?ExtendedItemValue,
  extendedItemLookup02SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup02Info?: ?ExtendItemInfo,
  extendedItemLookup03Id?: ?ExtendedItemId,
  extendedItemLookup03Value?: ?ExtendedItemValue,
  extendedItemLookup03SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup03Info?: ?ExtendItemInfo,
  extendedItemLookup04Id?: ?ExtendedItemId,
  extendedItemLookup04Value?: ?ExtendedItemValue,
  extendedItemLookup04SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup04Info?: ?ExtendItemInfo,
  extendedItemLookup05Id?: ?ExtendedItemId,
  extendedItemLookup05Value?: ?ExtendedItemValue,
  extendedItemLookup05SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup05Info?: ?ExtendItemInfo,
  extendedItemLookup06Id?: ?ExtendedItemId,
  extendedItemLookup06Value?: ?ExtendedItemValue,
  extendedItemLookup06SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup06Info?: ?ExtendItemInfo,
  extendedItemLookup07Id?: ?ExtendedItemId,
  extendedItemLookup07Value?: ?ExtendedItemValue,
  extendedItemLookup07SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup07Info?: ?ExtendItemInfo,
  extendedItemLookup08Id?: ?ExtendedItemId,
  extendedItemLookup08Value?: ?ExtendedItemValue,
  extendedItemLookup08SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup08Info?: ?ExtendItemInfo,
  extendedItemLookup09Id?: ?ExtendedItemId,
  extendedItemLookup09Value?: ?ExtendedItemValue,
  extendedItemLookup09SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup09Info?: ?ExtendItemInfo,
  extendedItemLookup10Id?: ?ExtendedItemId,
  extendedItemLookup10Value?: ?ExtendedItemValue,
  extendedItemLookup10SelectedOptionName?: ?ExtendedItemSelectedOptionName,
  extendedItemLookup10Info?: ?ExtendItemInfo,

  extendedItemDate01Id?: ?ExtendedItemId,
  extendedItemDate01Value?: ?ExtendedItemValue,
  extendedItemDate01Info?: ?ExtendItemInfo,
  extendedItemDate02Id?: ?ExtendedItemId,
  extendedItemDate02Value?: ?ExtendedItemValue,
  extendedItemDate02Info?: ?ExtendItemInfo,
  extendedItemDate03Id?: ?ExtendedItemId,
  extendedItemDate03Value?: ?ExtendedItemValue,
  extendedItemDate03Info?: ?ExtendItemInfo,
  extendedItemDate04Id?: ?ExtendedItemId,
  extendedItemDate04Value?: ?ExtendedItemValue,
  extendedItemDate04Info?: ?ExtendItemInfo,
  extendedItemDate05Id?: ?ExtendedItemId,
  extendedItemDate05Value?: ?ExtendedItemValue,
  extendedItemDate05Info?: ?ExtendItemInfo,
  extendedItemDate06Id?: ?ExtendedItemId,
  extendedItemDate06Value?: ?ExtendedItemValue,
  extendedItemDate06Info?: ?ExtendItemInfo,
  extendedItemDate07Id?: ?ExtendedItemId,
  extendedItemDate07Value?: ?ExtendedItemValue,
  extendedItemDate07Info?: ?ExtendItemInfo,
  extendedItemDate08Id?: ?ExtendedItemId,
  extendedItemDate08Value?: ?ExtendedItemValue,
  extendedItemDate08Info?: ?ExtendItemInfo,
  extendedItemDate09Id?: ?ExtendedItemId,
  extendedItemDate09Value?: ?ExtendedItemValue,
  extendedItemDate09Info?: ?ExtendItemInfo,
  extendedItemDate10Id?: ?ExtendedItemId,
  extendedItemDate10Value?: ?ExtendedItemValue,
  extendedItemDate10Info?: ?ExtendItemInfo,
};

const totalNumExtendedItems = 10;
export const totalNumTextExtendedItems = 10;
export const totalNumPicklistExtendedItems = 10;
export const totalNumLookupExtendedItems = 10;
export const totalNumDateExtendedItems = 10;

// initialize object with EIs with empty value
export const initialEmptyEIs = (): ExtendedItemExpectedList => {
  const eIValues = {};
  for (let i = 0; i < totalNumTextExtendedItems; i++) {
    const index = `0${i + 1}`.slice(-2);
    eIValues[`extendedItemText${index}Id`] = null;
    eIValues[`extendedItemText${index}Value`] = null;
    eIValues[`extendedItemText${index}Info`] = null;
  }
  for (let i = 0; i < totalNumPicklistExtendedItems; i++) {
    const index = `0${i + 1}`.slice(-2);
    eIValues[`extendedItemPicklist${index}Id`] = null;
    eIValues[`extendedItemPicklist${index}Value`] = null;
    eIValues[`extendedItemPicklist${index}Info`] = null;
  }
  for (let i = 0; i < totalNumLookupExtendedItems; i++) {
    const index = `0${i + 1}`.slice(-2);
    eIValues[`extendedItemLookup${index}Id`] = null;
    eIValues[`extendedItemLookup${index}Value`] = null;
    eIValues[`extendedItemLookup${index}Info`] = null;
    eIValues[`extendedItemLookup${index}SelectedOptionName`] = null;
  }
  for (let i = 0; i < totalNumDateExtendedItems; i++) {
    const index = `0${i + 1}`.slice(-2);
    eIValues[`extendedItemDate${index}Id`] = null;
    eIValues[`extendedItemDate${index}Value`] = null;
    eIValues[`extendedItemDate${index}Info`] = null;
  }
  return eIValues;
};

const sortEIByType = (extendedItems: Array<ExtendedItem>) => {
  const sortingOrder = ['Text', 'Picklist', 'Date', 'Lookup'];
  extendedItems.sort((a, b) => {
    const keyA = get(a, 'info.inputType', '');
    const keyB = get(b, 'info.inputType', '');
    return sortingOrder.indexOf(keyA) - sortingOrder.indexOf(keyB);
  });
  return extendedItems;
};

export const getExtendedItemArray = (
  item: ExtendedItemExpectedList
): Array<ExtendedItem> => {
  const extendedItems = [];
  for (let i = 1; i <= totalNumExtendedItems; i++) {
    const num = `0${i}`.slice(-2);
    extendedItems.push({
      index: num,
      id: item[`extendedItemText${num}Id`],
      info: item[`extendedItemText${num}Info`],
      value: item[`extendedItemText${num}Value`],
    });
    extendedItems.push({
      index: num,
      id: item[`extendedItemPicklist${num}Id`],
      info: item[`extendedItemPicklist${num}Info`],
      value: item[`extendedItemPicklist${num}Value`],
    });
    extendedItems.push({
      index: num,
      id: item[`extendedItemLookup${num}Id`],
      info: item[`extendedItemLookup${num}Info`],
      value: item[`extendedItemLookup${num}Value`],
      name: item[`extendedItemLookup${num}SelectedOptionName`],
    });
    extendedItems.push({
      index: num,
      id: item[`extendedItemDate${num}Id`],
      info: item[`extendedItemDate${num}Info`],
      value: item[`extendedItemDate${num}Value`],
    });
  }
  return sortEIByType(extendedItems);
};

// e.g. when changing record/expense type, use this method to copy extended item values from old item
// uses the salesforce ID (example: "a116F00000BOEZ0QAP") to identify the correct extended item
export const copyEIsFromSource = (
  target: ExtendedItemExpectedList,
  source?: ExtendedItemExpectedList
) => {
  // if no target: return empty; if no source, still need to handle default value
  if (!target) {
    return {};
  }

  const sourceItems = source || {};
  const targetItems = cloneDeep(target);
  const invertedSourceRecord = invert(sourceItems);

  Object.keys(targetItems).forEach((key) => {
    const EIId = targetItems[key]; // e.g "a116F00000BOEZ0QAP"
    const isEI = /^extendedItem.*Id$/.test(key);

    // if key is extendedItem
    if (isEI) {
      const targetEI = key.replace('Id', ''); // e.g extendedItemText01
      const isEILookup = /^extendedItemLookup.*Id$/.test(key);

      // if is in target
      if (EIId) {
        // e.g check whether the target EI is in source
        const sourceEIID = invertedSourceRecord[EIId]; // e.g "extendedItemText01Id"

        // if source contains the target EI
        if (sourceEIID) {
          const sourceEI = sourceEIID.replace('Id', ''); // e.g extendedItemText01
          // update newRecordItem with source EI Value & Info
          targetItems[`${targetEI}Value`] = sourceItems[`${sourceEI}Value`];
          if (isEILookup) {
            targetItems[`${targetEI}SelectedOptionName`] =
              sourceItems[`${sourceEI}SelectedOptionName`];
          }
        } else {
          // if exist only in target: use target with default value
          targetItems[`${targetEI}Value`] =
            targetItems[`${targetEI}Info`].defaultValueText;
          if (isEILookup) {
            targetItems[`${targetEI}SelectedOptionName`] = null;
          }
        }
      } else {
        // if not in target, reset value
        targetItems[`${targetEI}Value`] = null;
        if (isEILookup) {
          targetItems[`${targetEI}SelectedOptionName`] = null;
        }
      }
    }
  });

  return targetItems;
};

// get ONLY the EI part, handle default value as well
export const getEIsOnly = (
  target: ExtendedItemExpectedList,
  source?: ExtendedItemExpectedList
) => {
  const wholeObject: Object = copyEIsFromSource(target, source) || {};
  const onlyExtendedItems = pickBy(wholeObject, (v: any, key: string) =>
    key.includes('extendedItem')
  );
  return onlyExtendedItems;
};

const existExtendedItems = (
  items: ExtendedItemExpectedList
): Array<ExtendedItem> =>
  getExtendedItemArray(items).filter((i: ExtendedItem) => i.id);

// get label-value pairs from EIs
export const getLabelValueFromEIs = (
  items: ExtendedItemExpectedList
): Array<{ label: string, value: string }> =>
  sortEIByType(existExtendedItems(items)).map((extendedItem: ExtendedItem) => {
    if (!extendedItem.info) {
      return { label: '', value: '' };
    }
    let displayValue = '';
    switch (extendedItem.info.inputType) {
      case 'Text':
        displayValue = extendedItem.value;
        break;
      case 'Picklist':
        const targetEI = find(extendedItem.info.picklist, {
          value: extendedItem.value,
        });
        displayValue = targetEI ? targetEI.label : '';
        break;
      case 'Lookup':
        displayValue = extendedItem.value
          ? `${extendedItem.value} - ${extendedItem.name || ''}`
          : '';
        break;
      case 'Date':
        displayValue = DateUtil.formatYMD(extendedItem.value) || '';
        break;
      default:
        displayValue = '';
    }
    const extendedItemName = extendedItem.info ? extendedItem.info.name : '';
    return { label: extendedItemName, value: displayValue };
  });

export type EISearchObj = {
  extendedItemCustomId: string | null,
  extendedItemLookupId: string | null,
  name: string | null,
  idx: string | null,
  target: string | null,
  hintMsg: string | null,
};

export const getCustomEIOptionList = (
  extendedItemCustomId: ?string,
  query: ?string
): Promise<CustomEIOptionList> =>
  Api.invoke({
    path: '/extended-item-custom-option/search',
    param: {
      extendedItemCustomId,
      query,
    },
  }).then((response: CustomEIOptionList) => response);

export const getRecentlyUsedCustomEI = (
  employeeBaseId: string,
  extendedItemLookupId: ?string,
  extendedItemCustomId: ?string
): Promise<CustomEIOptionList> =>
  Api.invoke({
    path: '/extended-item-custom-option/recently-used/list',
    param: { employeeBaseId, extendedItemCustomId, extendedItemLookupId },
  }).then((response: CustomEIOptionList) => response);
