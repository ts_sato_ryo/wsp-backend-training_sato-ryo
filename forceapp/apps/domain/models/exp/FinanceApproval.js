// @flow
import moment from 'moment';
import Api from '../../../commons/api';
import type { ApprovalStatus } from '../approval/request/Status';
import type { Report } from './Report';

export type RequestIdList = Array<string>;
export type RequestIds = Array<string>;

export type RequestItem = {
  totalAmount: number,
  subject: string,
  status: ApprovalStatus | string,
  requestId: string,
  requestDate: string,
  reportNo: string,
  photoUrl: string,
  hasReceipts: boolean,
  employeeName: string,
  employeeCode: string,
  departmentName: string,
  departmentCode: string,
};

export type RequestList = Array<RequestItem>;

export type EditHistoryItem = {|
  isHeader: boolean,
  rowNo: number,
  fieldName: string,
  oldValue: string,
  newValue: string,
  recordSummary: string,
  modifiedByEmployeeName: string,
  modifiedDateTime: string,
|};

export type EditHistoryList = Array<EditHistoryItem>;

// eslint-disable-next-line import/prefer-default-export
export const initialStateRequestItem = {
  totalAmount: 0,
  subject: '',
  status: '',
  requestId: '',
  requestDate: '',
  reportNo: '',
  photoUrl: '',
  hasReceipts: false,
  employeeName: '',
  employeeCode: '',
  departmentName: '',
  departmentCode: '',
};

export type SortBy =
  | ''
  | 'ReportNo' // 申請番号
  | 'Status' // ステータス
  | 'RequestDate' // 申請日付
  | 'Subject' // 件名
  | 'TotalAmount' // 金額
  | 'EmployeeCode' // 社員コード
  | 'EmployeeName' // 社員名
  | 'DepartmentCode' // 部署コード
  | 'DepartmentName'; // 部署名

export const SORT_BY = {
  ReportNo: 'ReportNo', // 申請番号
  Status: 'Status', // /ステータス
  RequestDate: 'RequestDate', // 申請日付
  Subject: 'Subject', // 件名
  TotalAmount: 'TotalAmount', // 金額
  EmployeeCode: 'EmployeeCode', // 社員コード
  EmployeeName: 'EmployeeName', // 社員名
  DepartmentCode: 'DepartmentCode', // 部署コード
  DepartmentName: 'DepartmentName', // 部署名
};

export type OrderBy = '' | 'Asc' | 'Desc';

export const ORDER_BY = {
  Asc: 'Asc',
  Desc: 'Desc',
};

// const labelObject = () => ({
//   reports: msg().Exp_Lbl_Reports,
//   newReport: msg().Exp_Lbl_NewReportCreateExp,
//   accountingDate: msg().Exp_Clbl_RecordDate,
// });

export const detailValue = {
  ACCOUNTING_DATE: 'accountingDate',
  REPORT_ID: 'reportNo',
  AMOUNT: 'amount',
};

// export const detailOption = () => [
//   // { label: 'report date', value: 'reportDate' },
//   {
//     label: msg().Exp_Clbl_RecordDate,
//     value: detailValue.ACCOUNTING_DATE,
//   },
//   // { label: 'has receipt', value: 'receipt' },
//   // { label: 'exp type', value: 'expType' },
//   {
//     label: msg().Exp_Btn_SearchConditionReportNo,
//     value: detailValue.REPORT_ID,
//   },
//   { label: msg().Exp_Clbl_Amount, value: detailValue.AMOUNT },
// ];

export const DETAILS = {
  accountingDate: 'accountingDate',
  requestDate: 'requestDate',
  receipt: 'receipt',
  expType: 'expType',
  reportNo: 'reportNo',
  amount: 'amount',
};

export const receiptOption = [{ label: 'hasReceipt', value: 'hasReceipt' }];

export type Option = {
  label: string,
  value: string,
};

export type selectedOption = {
  item: string,
  value: string,
};

export type DateRangeOption = {
  startDate: ?string,
  endDate: ?string,
};

export type AmountRangeOption = {
  startAmount: ?number,
  endAmount: ?number,
};

export const AmountRangeOptionInitVal: AmountRangeOption = {
  startAmount: null,
  endAmount: null,
};

export const requestDateInitVal = (): DateRangeOption => {
  const firstDayOfLastMonth = moment(new Date())
    .add(-1, 'months')
    .startOf('months')
    .format('YYYY-MM-DD');
  const today = moment().format('YYYY-MM-DD');
  const resDateRange: DateRangeOption = {
    startDate: firstDayOfLastMonth,
    endDate: today,
  };
  return resDateRange;
};

export const accountingDateInitVal: DateRangeOption = {
  startDate: null,
  endDate: null,
};

export type StatusOption = {
  label: string,
  value: string,
};

// export const statusOptions = () => [
//   { label: msg().Exp_Btn_StatusOptionStatusApproved, value: 'Approved' },
//   {
//     label: msg().Exp_Btn_StatusOptionAccountingAuthorized,
//     value: 'AccountingAuthorized',
//   },
//   {
//     label: msg().Exp_Btn_StatusOptionAccountingRejected,
//     value: 'AccountingRejected',
//   },
//   { label: msg().Exp_Btn_StatusOptionJournalCreated, value: 'JournalCreated' },
// ];

export type SearchConditions = {
  name?: string,
  financeStatusList: Array<string>,
  departmentBaseIds: Array<string>,
  empBaseIds: Array<string>,
  // expTypeIds: Array<string>,
  reportNo: ?string,
  amountRange: AmountRangeOption,
  requestDateRange: DateRangeOption,
  accountingDateRange: DateRangeOption,
  detail: Array<string>,
};

export const financeStatusListInitParam = ['Approved'];

export const financeStatusListAllParam = [
  'Approved',
  'AccountingAuthorized',
  'AccountingRejected',
  'JournalCreated',
  'Fully Paid',
];

export function searchConditionInitValue() {
  return {
    // name: msg().Exp_Lbl_SearchConditionApprovelreRuestList,
    name: '',
    financeStatusList: financeStatusListInitParam,
    departmentBaseIds: [],
    empBaseIds: [],
    // expTypeIds: Array<?string>,
    reportNo: null,
    amountRange: AmountRangeOptionInitVal,
    requestDateRange: requestDateInitVal(),
    accountingDateRange: accountingDateInitVal,
    detail: [],
  };
}

export type FinanceApprovalRequestIds = {
  totalSize: number,
  requestIdList: RequestIdList,
};

export const getRequestIdList = (
  sortBy?: ?SortBy,
  order?: ?OrderBy,
  advSearchConditions?: ?SearchConditions
): Promise<FinanceApprovalRequestIds> => {
  let financeStatusList;
  if (advSearchConditions) {
    if (
      advSearchConditions.financeStatusList &&
      advSearchConditions.financeStatusList.length > 0
    ) {
      financeStatusList = advSearchConditions.financeStatusList;
    } else {
      financeStatusList = financeStatusListAllParam;
    }
  } else {
    financeStatusList = financeStatusListInitParam;
  }
  return Api.invoke({
    path: '/exp/finance-approval/request-id/list',
    param: {
      sortBy,
      order,
      searchFilter: {
        financeStatusList,
        departmentBaseIds: advSearchConditions
          ? advSearchConditions.departmentBaseIds
          : null,
        empBaseIds: advSearchConditions ? advSearchConditions.empBaseIds : null,
        requestDateRange: advSearchConditions
          ? advSearchConditions.requestDateRange
          : requestDateInitVal(),
        accountingDateRange: advSearchConditions
          ? advSearchConditions.accountingDateRange
          : null,
        amountRange: advSearchConditions
          ? advSearchConditions.amountRange
          : null,
        reportNo: advSearchConditions ? advSearchConditions.reportNo : null,
      },
    },
  }).then((response: FinanceApprovalRequestIds) => response);
};

export const getRequestList = (
  requestIds: RequestIds
): Promise<RequestList> => {
  return Api.invoke({
    path: '/exp/finance-approval/report/list',
    param: {
      requestIds,
    },
  }).then((response: { requestList: RequestList }) => response.requestList);
};

export const getRequestItem = (requestId?: ?string): Promise<RequestItem> => {
  return Api.invoke({
    path: '/exp/finance-approval/report/get',
    param: {
      requestId,
      usedIn: 'REPORT',
    },
  }).then((response: RequestItem) => response);
};

export const getEditHistory = (requestId: string): Promise<EditHistoryList> => {
  return Api.invoke({
    path: '/exp/finance-approval/report/history/get',
    param: {
      requestId,
    },
  }).then(
    (response: { modificationList: EditHistoryList }) =>
      response.modificationList
  );
};

export const reject = (requestIds: string[], comment: string): Promise<any> => {
  return Api.invoke({
    path: '/exp/finance-approval/report/reject',
    param: {
      requestIds,
      comment,
    },
  }).then((response: any) => response);
};

export const approve = (
  requestIds: string[],
  comment: string
): Promise<any> => {
  return Api.invoke({
    path: '/exp/finance-approval/report/approve',
    param: {
      requestIds,
      comment,
    },
  }).then((response: any) => response);
};

export const save = (report: Report): Promise<Report> => {
  // remove records for better API performance.
  const reportCopy = Object.assign({}, report);
  delete reportCopy.records;
  // remove image data
  delete reportCopy.attachedFileData;

  return Api.invoke({
    path: '/exp/finance-approval/report/save',
    param: reportCopy,
  }).then((response: any) => response);
};
