// @flow
import get from 'lodash/get';
import DateUtil from '../../../commons/utils/DateUtil';
import Api from '../../../commons/api';

// 期間検索 / expense accounting period
export type AccountingPeriod = {
  id: string,
  code: string,
  name: string,
  name_L0: string,
  name_L1: ?string,
  name_L2: ?string,
  companyId: string,
  validDateFrom: string,
  validDateTo: string,
  recordingDate: string,
  active: boolean,
};

export type AccountingPeriodList = Array<AccountingPeriod>;

export type AccountingPeriodOption = {
  id: string,
  value: string,
  label: string,
  recordDate: string,
  active: boolean,
};
export const convertAccountingDateId = (
  accountingPeriodId: string,
  accountingPeriodList: Array<AccountingPeriodOption>
) => {
  const selectedAccountingPeriod = accountingPeriodList.find(
    (ap) => ap.id === accountingPeriodId
  );
  const label = get(selectedAccountingPeriod, 'label');
  const dates = label.split(' - ');
  const startDate = DateUtil.format(dates[0], 'YYYY-MM-DD');
  const endDate = DateUtil.format(dates[1], 'YYYY-MM-DD');
  return {
    startDate,
    endDate,
  };
};

// eslint-disable-next-line import/prefer-default-export
export const searchAccountingPeriod = (
  companyId: string
): Promise<AccountingPeriodList> => {
  return Api.invoke({
    path: '/exp/accounting-period/search',
    param: { companyId, isNotMaster: true },
  });
};
