// @flow
import Decimal from 'decimal.js';

import Api from '../../../../commons/api';

export const FOREIGN_CURRENCY_USAGE = {
  NotUsed: 'NOT_USED',
  Fixed: 'FIXED',
  Flexible: 'FLEXIBLE',
};

export type Currency = {
  id: string,
  isoCurrencyCode: string,
  name_L0: string,
  name_L1: string,
  decimalPlaces: string,
  symbol: string,
};

export type CurrencyList = Array<Currency>;

// Business Logic
export const calcAmountFromRate = (
  rate: number,
  localAmount: number,
  decimal: number
) => {
  return Decimal.mul(rate, localAmount).toFixed(decimal, Decimal.ROUND_DOWN);
};

// eslint-disable-next-line import/prefer-default-export
export const searchCurrency = (companyId: string): Promise<CurrencyList> => {
  return Api.invoke({
    path: '/currency/search',
    param: {
      companyId,
    },
  });
};
