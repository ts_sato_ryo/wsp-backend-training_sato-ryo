// @flow
import Api from '../../../../commons/api';

export type Receipt = {
  title?: string,
  contentVersionId: string,
  receiptId: string,
  receiptFileId: string,
  createDate?: string,
};
export type ReceiptList = Array<Receipt>;

export type FilePreview = {
  receiptId: string,
  title: string,
  fileBody: string,
  fileType: string,
};

export const getFilePreview = (receiptFileId: string): Promise<FilePreview> => {
  return Api.invoke({
    path: '/exp/receipt/get',
    param: {
      contentVersionId: receiptFileId,
    },
  });
};

export const getOCRStatus = (taskId: string): Promise<FilePreview> => {
  return Api.invoke({
    path: '/exp/receipt/ocr/get-status',
    param: {
      taskId,
    },
  });
};

export const getReceipts = (): Promise<ReceiptList> => {
  return Api.invoke({
    path: '/exp/receipt/list',
    param: {
      type: 'Receipt',
    },
  }).then((res) => {
    return res.files;
  });
};

export const deleteReceipt = (receiptId: string): Promise<ReceiptList> => {
  return Api.invoke({
    path: '/exp/receipt/delete',
    param: { contentDocumentIds: [receiptId] },
  });
};

export const executeOcr = (receiptId: string): Promise<string> => {
  return Api.invoke({
    path: '/exp/receipt/ocr/execute',
    param: { receiptId },
  });
};
