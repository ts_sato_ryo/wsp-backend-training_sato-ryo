// @flow
import Decimal from 'decimal.js';
import Api from '../../../commons/api';

// 経費税区分 / expense tax type
export type ExpenseTaxType = {
  id: string,
  code: string,
  name: string,
  name_L0: string,
  name_L1: ?string,
  name_L2: ?string,
  companyId: string,
  validDateFrom: string,
  validDateTo: string,
  rate: number,
};

export type ExpTaxTypeList = Array<{
  historyId: string,
  baseId: string,
  name: string,
  rate: number,
}>;

export type ExpTaxTypeListApiReturn = {
  payload: ExpTaxTypeList,
};

export type ExpenseTaxTypeList = Array<ExpenseTaxType>;

export const calculateTax = (
  rateNum: number,
  amountNum: number,
  decimalPlaces: number
) => {
  const taxRate = new Decimal(rateNum);
  const calcRate = new Decimal(rateNum + 100);
  const amount = new Decimal(amountNum);
  const gstVat = amount
    .div(calcRate)
    .mul(taxRate)
    .toDecimalPlaces(decimalPlaces, Decimal.ROUND_DOWN);
  const amountWithoutTax = amount.minus(gstVat);

  // e.g) amount: 108, rate: 8, decimalPlaces: 0
  // const gstVat = amount / (100 + rate) * rate;
  // const gstVat = 108 / (100 + 8) * 8;
  //       gstVat = 8
  // const amountWithoutTax = amount - gstVat;
  // const amountWithoutTax = 108 - 8;
  //       amountWithoutTax = 100;
  return {
    gstVat: gstVat.toFixed(decimalPlaces),
    amountWithoutTax: amountWithoutTax.toFixed(decimalPlaces),
  };
};

export const calcTaxFromGstVat = (
  gstVat: number,
  amount: number,
  decimalPlaces: number
) => {
  const amountWithoutTax = Decimal.sub(amount, gstVat).toFixed(
    decimalPlaces,
    Decimal.ROUND_DOWN
  );
  return {
    amountWithoutTax,
  };
};

// eslint-disable-next-line import/prefer-default-export
export const searchTaxType = (
  targetDate: string
): Promise<{ taxTypes: ExpenseTaxTypeList }> => {
  return Api.invoke({
    path: '/exp/tax-type/search',
    param: { targetDate },
  });
};

export const searchTaxTypeList = (
  expTypeId: string,
  targetDate: string
): Promise<{ taxTypes: ExpTaxTypeList }> => {
  return Api.invoke({
    path: '/exp/tax-type/list',
    param: { expTypeId, targetDate },
  });
};
