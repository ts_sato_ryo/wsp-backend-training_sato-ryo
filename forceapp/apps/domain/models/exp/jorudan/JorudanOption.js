// @flow

import _ from 'lodash';

import Api from '../../../../commons/api';

import { type CompanyList } from '../../common/Company';

const searchRouteOption = (companyId: string): Promise<any> =>
  Api.invoke({
    path: '/company/search',
  }).then((companies: CompanyList) =>
    _.find(companies.records, { id: companyId })
  );

export default searchRouteOption;
