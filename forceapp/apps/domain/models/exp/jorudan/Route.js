// @flow
import Api from '../../../../commons/api';
import { type Station } from './Station';
// import { type ViaList } from '../Record';

export type SearchRouteParam = {
  roundTrip: boolean,
  targetDate: string,
  origin: Station,
  //  viaList: ViaList,
  viaList: any,
  arrival: Station,
  option: {
    fareType: number,
    useChargedExpress: number,
    seatPreference: number,
    routeSort: number,
    useExReservation: boolean,
  },
};

export type Path = {
  key: string,
  lineName: string,
  lineType: number,
  airLine: string,
  fromName: string,
  toName: string,
  distance: number,
  requiredTime: number,
  transfer: boolean,
  travelTime: number,
  seatCode: string,
  seatName: string,
  seatList: Array<{
    code: string,
    name: string,
    airLine: string,
    airLineFare: string,
  }>,
  fareKey: string,
  expressKey: string,
};
export type RouteItemStatus = {
  isEarliest: boolean,
  isCheapest: boolean,
  isMinTransfer: boolean,
};

export type RouteItem = {
  key: string,
  cost: number,
  roundTripCost: number,
  existsIcCost: boolean,
  requiredTime: number,
  transferNumber: number,
  pathNumber: number,
  distance: number,
  status: RouteItemStatus,
  fareMap: {
    key: string,
    fare: number,
    roundTripFare: number,
    isRoundDiscount: string,
    isIcFare: boolean,
    exceptCommuterRoute: boolean,
  },
  expressMap: {
    key: string,
    fee: number,
    green: number,
    sleeping: number,
    roundTripFee: number,
    roundTripGreen: number,
    roundTripSleeping: number,
    season: number,
    isConnectionDiscount: number,
  },
  pathList: Array<Path>,
  fare1: ?number,
  fare3: ?number,
  fare6: ?number,
};

export type RouteList = Array<RouteItem>;

export type Route = {
  route: {
    routeNumber: number,
    routeList: RouteList,
  },
  commuter: {
    routeNumber: number,
    routeList: any,
  },
};

export const initialStateRoute = {
  route: {
    routeNumber: 0,
    routeList: [],
  },
  commuter: {
    routeNumber: 0,
    routeList: [],
  },
};

export const searchRoute = (param: SearchRouteParam): Promise<Route> =>
  Api.invoke({
    path: '/exp/jorudan/route/search',
    param,
  }).then((res) => res.data);

export const isEXReservation = (seatCode: string): boolean =>
  ['ICS', 'ICF', 'ICG'].includes(seatCode);

export const includeEXReservation = (item: RouteItem): boolean =>
  item.pathList.map((p) => isEXReservation(p.seatCode)).includes(true);
