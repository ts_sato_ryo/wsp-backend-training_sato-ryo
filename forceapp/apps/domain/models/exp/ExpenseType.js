// @flow
import Api from '../../../commons/api';
import { type ExtendedItemExpectedList } from './ExtendedItem';

export type FixedAllowanceOption = {
  id: string,
  label: string,
  allowanceAmount: number,
};

// 経費一覧
export type AmountOption = {
  id: ?string,
  label?: string,
  label_L0: string,
  label_L1: string,
  label_L2?: string,
  allowanceAmount: number,
  label_L0_error?: boolean,
  allowanceAmount_error?: boolean,
};

export type ExpenseType = {
  id: string,
  code: string,
  name: string,
  description: string,
  parentGroupId: ?string,
  isGroup: boolean,
  hasChildren: boolean,
  recordType: string,
  fileAttachment: string,
  useForeignCurrency: boolean,
  order: number,
  parentGroupCode: string,
  parentGroup: { name: string },
  ...$Exact<ExtendedItemExpectedList>,
  foreignCurrencyUsage?: string,
  hierarchyParentNameList?: Array<string>,
  fixedAllowanceSingleAmount: ?number,
  fixedAllowanceOptionList?: Array<AmountOption>,
};

export type ExpenseTypeList = Array<ExpenseType>;

// eslint-disable-next-line import/prefer-default-export
export const getExpenseTypeList = (
  empId: ?string,
  parentGroupId: ?string,
  targetDate: ?string,
  recordType: ?string,
  usedIn?: string,
  expReportTypeId: ?string,
  filterByEmployeeGroup: ?boolean
): Promise<ExpenseTypeList> => {
  return Api.invoke({
    path: '/exp/expense-type/list',
    param: {
      empId,
      parentGroupId,
      targetDate,
      recordType,
      usedIn,
      expReportTypeId,
      filterByEmployeeGroup,
    },
  }).then(
    (response: { expenseTypes: ExpenseTypeList }) => response.expenseTypes
  );
};

export const searchExpenseType = (
  companyId: ?string,
  query: ?string,
  targetDate: ?string,
  usedIn?: string,
  expReportTypeId: ?string,
  withParentHierarchy?: boolean,
  recordType?: string,
  filterByEmployeeGroup: ?boolean
): Promise<ExpenseTypeList> => {
  return Api.invoke({
    path: '/exp/expense-type/search',
    param: {
      companyId,
      query,
      targetDate,
      withExtendedItems: true,
      usedIn,
      expReportTypeId,
      withParentHierarchy,
      recordType,
      filterByEmployeeGroup,
    },
  }).then((response: { records: ExpenseTypeList }) => response.records);
};

export const searchExpTypesByParentRecord = (
  targetDate: string,
  parentId: string,
  usedIn: string
): Promise<ExpenseTypeList> => {
  return Api.invoke({
    path: '/exp/expense-type/list-child',
    param: {
      targetDate,
      parentId,
      usedIn,
      withExtendedItems: true,
    },
  }).then(
    (response: { childExpTypeList: ExpenseTypeList }) =>
      response.childExpTypeList
  );
};

export const searchExpenseTypeWithRecord = (
  companyId: string,
  targetDate: ?string,
  recordType: ?string,
  usedIn?: string,
  filterByEmployeeGroup: ?boolean
): Promise<ExpenseTypeList> => {
  return Api.invoke({
    path: '/exp/expense-type/search',
    param: {
      companyId,
      targetDate,
      recordType,
      withExtendedItems: true,
      usedIn,
      filterByEmployeeGroup,
    },
  }).then((response: { records: ExpenseTypeList }) => response.records);
};

export const searchExpenseTypeByReportType = (
  empId: ?string,
  companyId: ?string,
  targetDate: ?string,
  expReportTypeId: ?string,
  usedIn?: string
): Promise<ExpenseTypeList> => {
  return Api.invoke({
    path: '/exp/expense-type/search',
    param: {
      empId,
      companyId,
      targetDate,
      expReportTypeId,
      usedIn,
    },
  }).then((response: { records: ExpenseTypeList }) => response.records);
};
export type ExpTypeSearchQuery = {
  companyId: ?string,
  targetDate: ?string,
  name: ?string,
  code: ?string,
  expGroupName: ?string,
  expGroupCode: ?string,
  recordType: ?string,
  foreignCurrencyUsage: ?string,
  fixedForeignCurrencyId: ?string,
};

const queryToParam = (query: ExpTypeSearchQuery) => ({
  ...query,
});

export const searchExpenseTypeByGroup = (
  query: ExpTypeSearchQuery
): Promise<ExpenseTypeList> => {
  return Api.invoke({
    path: '/exp/expense-type/search',
    param: queryToParam(query),
  }).then((response: { records: ExpenseTypeList }) => response.records);
};

export const getRecentlyUsed = (
  employeeBaseId: string,
  companyId: string,
  targetDate?: string,
  recordType?: string,
  reportTypeId?: string,
  usedIn?: string,
  withParentHierarchy?: boolean
): Promise<ExpenseTypeList> => {
  return Api.invoke({
    path: '/exp/expense-type/recently-used/list',
    param: {
      employeeBaseId,
      companyId,
      targetDate,
      recordType,
      reportTypeId,
      usedIn,
      withParentHierarchy,
    },
  }).then((response: { records: ExpenseTypeList }) => response.records);
};

export const getExpenseTypeById = (id: string): Promise<ExpenseType> => {
  return Api.invoke({
    path: '/exp/expense-type/search',
    param: {
      id,
    },
  }).then((response: { records: ExpenseType }) => response.records);
};
