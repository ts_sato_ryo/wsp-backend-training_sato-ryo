// @flow

export type EventMessage = {
  message: string,
};
