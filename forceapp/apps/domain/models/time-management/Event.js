/* @flow */

export type CalendarServiceId =
  | 'teamspirit'
  | 'google'
  | 'office365'
  | 'salesforce';

export type Event = {
  id: string,
  ownerId: string,
  title: string,
  startDateTime: string,
  endDateTime: string,
  isAllDay: boolean,
  isOrganizer: boolean,
  location: string,
  description: string,
  isOuting: boolean,
  createdServiceBy: CalendarServiceId,
  externalEventId: null | string,
  jobId: ?string,
  jobCode: ?string,
  jobName: ?string,
  workCategoryId: ?string,
  workCategoryName: ?string,
  contactId: string,
  contactName: string,
};
