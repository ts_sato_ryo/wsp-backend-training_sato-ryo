/* @flow */
import moment from 'moment';
import defaultTo from 'lodash/defaultTo';
import isNil from 'lodash/isNil';
import sumBy from 'lodash/sumBy';
import uuidV4 from 'uuid/v4';

import Api from '../../../commons/api';
import TimeUtil from '../../../commons/utils/TimeUtil';

import { type Job } from '../time-tracking/Job';
import { type WorkCategory } from '../time-tracking/WorkCategory';
import STATUS, { type Status } from '../approval/request/Status';
import { type DailySummaryTask, colorizeTasks } from './DailySummaryTask';

export type { DailySummaryTask };

export type DailySummaryEvent = {
  id: string,
  subject: ?string,
  startDateTime: ?string,
  endDateTime: ?string,
};

export type DailySummary = {
  targetDate: string,
  status: Status,
  note: ?string,
  output: ?string,
  realWorkTime: ?number,
  isTemporaryWorkTime: ?boolean,
  taskList: DailySummaryTask[],
};

export type DailySummaryParam = {
  empId?: string,
  targetDate: string,
  note?: string,
  output?: string,
  taskList: Array<{
    jobId: string,
    workCategoryId: ?string,
    isDirectInput: boolean,
    taskTime?: number,
    ratio?: number,
    volume?: number,
    taskNote?: string,
  }>,
};

export type EventFromRemote = {
  id: string,
  subject: ?string,
  startDateTime: ?string,
  endDateTime: ?string,
};

export type TaskFromRemote = {
  jobId: string,
  jobCode: string,
  jobName: string,
  isDirectCharged: boolean,
  workCategoryId: ?string,
  workCategoryCode: ?string,
  workCategoryName: ?string,
  isDirectInput: boolean,
  volume: ?number,
  ratio: ?number,
  taskTime: ?number,
  taskNote: ?string,
  workCategoryList: WorkCategory[],
  eventList: DailySummaryEvent[],
};

export type ContentsFromRemote = {
  targetDate: string,
  status:
    | 'NotRequested'
    | 'Pending'
    | 'Approved'
    | 'Rejected'
    | 'Removed'
    | 'Canceled',
  note: ?string,
  output: ?string,
  realWorkTime: ?number,
  isTemporaryWorkTime: ?boolean,
  taskList: TaskFromRemote[],
};

const convertWorkCategoryFromRemote = (
  workCategoryFromRemote: WorkCategory
): WorkCategory => ({
  id: workCategoryFromRemote.id,
  code: workCategoryFromRemote.code,
  name: workCategoryFromRemote.name,
});

const convertEventTaskTimeFromRemoteEvents = (
  eventsFromRemote: EventFromRemote[]
): number => {
  const minutes = eventsFromRemote.reduce(
    (eventTaskTime: number, eventFromRemote: EventFromRemote) =>
      eventTaskTime +
      TimeUtil.calcDurationMinutes(
        eventFromRemote.startDateTime,
        eventFromRemote.endDateTime
      ),
    0
  );
  return minutes >= 24 /* hours */ * 60 /* minutes */ ? 0 : minutes;
};

export const convertTaskFromRemote = (
  taskFromRemote: TaskFromRemote
): DailySummaryTask => ({
  // FIXME this makes a function mutable
  id: uuidV4(),
  jobId: taskFromRemote.jobId,
  jobCode: taskFromRemote.jobCode,
  jobName: taskFromRemote.jobName,
  isDirectCharged: taskFromRemote.isDirectCharged,
  workCategoryId: taskFromRemote.workCategoryId,
  workCategoryName: taskFromRemote.workCategoryName,
  workCategoryCode: taskFromRemote.workCategoryCode,
  color: { base: '', linked: '' },
  isDirectInput: taskFromRemote.isDirectInput,
  volume: taskFromRemote.volume,
  ratio: taskFromRemote.ratio,
  // complete task-hours by total duration of related events
  taskTime: taskFromRemote.taskTime,
  eventTaskTime: convertEventTaskTimeFromRemoteEvents(taskFromRemote.eventList),
  workCategoryList: taskFromRemote.workCategoryList.map(
    convertWorkCategoryFromRemote
  ),
});

const convertContentsFromRemote = (
  contentsFromRemote: ContentsFromRemote
): DailySummary => ({
  targetDate: contentsFromRemote.targetDate,
  status: contentsFromRemote.status,
  note: contentsFromRemote.note,
  output: contentsFromRemote.output,
  realWorkTime: contentsFromRemote.realWorkTime,
  isTemporaryWorkTime: contentsFromRemote.isTemporaryWorkTime,
  taskList: contentsFromRemote.taskList.map(convertTaskFromRemote),
});

export const fetchDailySummaryContents = (
  targetDate: string
): Promise<DailySummary> =>
  Api.invoke({
    path: '/daily-summary/get',
    param: {
      targetDate,
    },
  }).then((response) => convertContentsFromRemote(response));

export const calculateEachRatioOfNonDirectInputTasks = (
  taskList: DailySummaryTask[]
) => {
  const nonDirectInputTaskVolumeSum = sumBy(taskList, (task) =>
    !task.isDirectInput ? task.volume : 0
  );

  const newTaskList: DailySummaryTask[] = taskList.map((task) =>
    !task.isDirectInput && !isNil(task.volume)
      ? {
          ...task,
          /* assert(task.volume !== null) */
          ratio:
            nonDirectInputTaskVolumeSum > 0
              ? Math.floor((task.volume / nonDirectInputTaskVolumeSum) * 100)
              : 0,
        }
      : { ...task, ratio: null }
  );

  // handle round-off errors
  const firstNonDirectInputTask = newTaskList.find(
    (task) => !task.isDirectInput && !isNil(task.volume) && task.volume > 0
  );
  if (!isNil(firstNonDirectInputTask)) {
    firstNonDirectInputTask.ratio +=
      100 -
      sumBy(
        newTaskList.filter((task) => !task.isDirectInput),
        (task) => task.ratio
      );
  }

  return newTaskList;
};

export const calculateEachTaskTimeOfNonDirectInputTasks = (
  realWorkTime: ?number,
  taskList: DailySummaryTask[]
) => {
  const directInputTaskTimeSum = sumBy(
    taskList.filter((task) => task.isDirectInput),
    (task) => defaultTo(task.taskTime, 0)
  );

  /* assert(realWorkTime !== 0) */
  const nonDirectInputTaskTimeSum = Math.max(
    0,
    defaultTo(realWorkTime, 0) - directInputTaskTimeSum
  );

  const newTaskList: DailySummaryTask[] = taskList.map((task) =>
    !task.isDirectInput && !isNil(task.ratio)
      ? {
          ...task,
          taskTime:
            nonDirectInputTaskTimeSum > 0 &&
            !isNil(task.volume) &&
            task.volume > 0
              ? Math.floor((nonDirectInputTaskTimeSum * task.ratio) / 100)
              : null,
        }
      : task
  );

  const firstNonDirectInputTask = newTaskList.find(
    (task) => !task.isDirectInput && !isNil(task.taskTime)
  );
  if (!isNil(firstNonDirectInputTask)) {
    firstNonDirectInputTask.taskTime +=
      nonDirectInputTaskTimeSum -
      sumBy(newTaskList.filter((task) => !task.isDirectInput), (task) =>
        defaultTo(task.taskTime, 0)
      );
  }

  return newTaskList;
};

/**
 * Given actual work hours in the day and tasks in Daily Summary on the date,
 * calculates each ratio and task-hours for non-direct-input tasks.
 * @param {?number} realWorkTime Actual work hours in the day. Cannot be null.
 * @param {Object[]} tasks Tasks in Daily Summary on the date. Can contain direct-input tasks.
 * @returns {Object[]} Given direct-input tasks, and non-direct-input tasks with ratio and volume calculated by actual work hours in the day.
 */
export const calculateEachRatioAndTaskTimeOfNonDirectInputTasks = (
  realWorkTime: ?number,
  taskList: DailySummaryTask[]
): DailySummaryTask[] => {
  return calculateEachTaskTimeOfNonDirectInputTasks(
    realWorkTime,
    calculateEachRatioOfNonDirectInputTasks(taskList)
  );
};

/**
 * Ensure a task of Default Job exists in the list.
 * @param {Object[]} tasks List of Daily Summary tasks
 * @param {Object} defaultJob Default Job object
 * @return {Object[]} List of Daily Summary tasks with a task of Default Job
 */
export const ensureTaskOfDefaultJobExists = (
  realWorkTime: ?number,
  defaultJob: Job,
  taskList: DailySummaryTask[]
): DailySummaryTask[] => {
  // calculate each ratio and task-hours of non-direct-input tasks once
  // to complete task-hours when making them direct-input.
  let taskListWithRatioAndTaskTime = calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
    realWorkTime,
    taskList
  );

  // find a task of Default Job in the list
  let taskOfDefaultJob = taskListWithRatioAndTaskTime.find(
    (task) =>
      task.jobId === defaultJob.id &&
      task.workCategoryId === null &&
      (!task.isDirectInput || task.taskTime === 0)
  );
  if (!isNil(taskOfDefaultJob)) {
    // if there's a task of job same as Default Job and meets other criteria
    Object.assign(taskOfDefaultJob, {
      isDirectInput: false,
      volume:
        taskOfDefaultJob.isDirectInput ||
        (!isNil(taskOfDefaultJob.volume) && taskOfDefaultJob.volume > 0)
          ? 600
          : 0,
      ratio: null,
      taskTime: null,
      eventTaskTime: 0,
      workCategoryList: [],
    });
  } else {
    // if there's no task of job same as Default Job or didn't meet other criteria
    taskListWithRatioAndTaskTime.push(
      (taskOfDefaultJob = {
        // FIXME stop using uuid
        id: uuidV4(),
        jobId: defaultJob.id,
        jobCode: defaultJob.code,
        jobName: defaultJob.name,
        isDirectCharged: defaultJob.isDirectCharged,
        workCategoryId: null,
        workCategoryName: null,
        workCategoryCode: null,
        color: { base: '', linked: '' },
        isDirectInput: false,
        volume: 600,
        ratio: null,
        taskTime: null,
        eventTaskTime: 0,
        workCategoryList: [],
      })
    );
  }

  // make other tasks are direct-input tasks
  taskListWithRatioAndTaskTime = taskListWithRatioAndTaskTime.map((task) =>
    !task.isDirectInput && task !== taskOfDefaultJob
      ? {
          ...task,
          isDirectInput: true,
          volume: null,
          ratio: null,
        }
      : task
  );

  // clear calculation results in the list and return
  return taskListWithRatioAndTaskTime.map((task) =>
    !task.isDirectInput
      ? {
          ...task,
          ratio: null,
          taskTime: null,
        }
      : task
  );
};

/**
 * Complete each task-hours of event-related tasks.
 * @param {Object[]} taskList List of Daily Summary tasks
 * @return {Object[]}
 */
export const completeTaskTimeOfEventRelatedTasks = (
  taskList: DailySummaryTask[]
): DailySummaryTask[] =>
  taskList.map((task) =>
    task.eventTaskTime > 0 && task.taskTime === 0
      ? {
          ...task,
          taskTime: task.eventTaskTime,
        }
      : task
  );

const buildPostDate = (
  editing: { taskList: DailySummaryTask[], note: string, output: string },
  targetDate: string
): DailySummaryParam => {
  return {
    targetDate,
    taskList: calculateEachRatioOfNonDirectInputTasks(editing.taskList).map(
      (task) => ({
        jobId: task.jobId,
        workCategoryId: task.workCategoryId || null, // 空文字がきた場合にはnullを送信する
        isDirectInput: task.isDirectInput,
        ...(task.isDirectInput
          ? {
              taskTime: task.taskTime || 0,
            }
          : {
              volume: task.volume || 0,
              ratio: task.ratio || 0,
            }),
      })
    ),
    note: editing.note,
    output: editing.output,
  };
};

/**
 * サマリーデータ保存
 * @param {Object} dailySummary
 */
export const postDailySummary = (dailySummary: {
  editing: { taskList: DailySummaryTask[], note: string, output: string },
  selectedDay: moment,
}) => {
  const { editing, selectedDay } = dailySummary;

  const postData = buildPostDate(
    editing,
    selectedDay.clone().format('YYYY-MM-DD')
  );

  const req = {
    path: '/daily-summary/save',
    param: postData,
  };

  return Api.invoke(req);
};

export const isLocked = (dailySummary: DailySummary): boolean => {
  const lockedStatus = [STATUS.Pending, STATUS.Approved];
  return lockedStatus.includes(dailySummary.status);
};

export const convertFromRemote = (
  remote: ContentsFromRemote,
  defaultJob: null | Job = null
): DailySummary => {
  const dailySummary = convertContentsFromRemote(remote);

  let taskList = dailySummary.taskList;
  if (!isLocked(dailySummary) && defaultJob) {
    // If Default Job is set, and Time Tracking Summary on the date is not locked,
    // append a task of Default Job.
    taskList = ensureTaskOfDefaultJobExists(
      dailySummary.realWorkTime,
      defaultJob,
      taskList
    );
  }

  // Complete each task-hours of event-related tasks
  taskList = completeTaskTimeOfEventRelatedTasks(taskList);

  // Set a color for each bar of tasks
  taskList = colorizeTasks(0, taskList);

  // this value is used on DailySummary's textarea value
  // so have to convert null to Empty in order to avoid make React.js update view properly.
  const note = dailySummary.note || '';

  return {
    ...dailySummary,
    taskList,
    note,
  };
};
