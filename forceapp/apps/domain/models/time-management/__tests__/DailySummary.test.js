import {
  calculateEachRatioOfNonDirectInputTasks,
  calculateEachTaskTimeOfNonDirectInputTasks,
  ensureTaskOfDefaultJobExists,
  completeTaskTimeOfEventRelatedTasks,
} from '../DailySummary';

describe('calculateEachRatioOfNonDirectInputTasks(taskList)', () => {
  test('calculate each ratio of non-direct-input tasks correctly', () => {
    const taskList = [
      { isDirectInput: false, volume: 100 },
      { isDirectInput: false, volume: 200 },
      { isDirectInput: true, taskTime: 300 },
      { isDirectInput: false, volume: 0 },
    ];

    const newTaskList = calculateEachRatioOfNonDirectInputTasks(taskList);

    expect(newTaskList[0].ratio).toBe(34);
    expect(newTaskList[1].ratio).toBe(66);
  });

  test('should add the round-off errors of ratio to the first non-direct-input task in the list', () => {
    const taskList = [
      { isDirectInput: false, volume: 48 },
      { isDirectInput: false, volume: 102 },
      { isDirectInput: false, volume: 165 },
      { isDirectInput: true, taskTime: 300 },
    ];

    const newTaskList = calculateEachRatioOfNonDirectInputTasks(taskList);

    expect(newTaskList[1].ratio).toBe(32);
    expect(newTaskList[2].ratio).toBe(52);
    // should contain the round-off errors
    expect(newTaskList[0].ratio).toBe(16);
  });

  test('should set ratio to 0 when the sum of volumes of non-direct-input tasks is 0', () => {
    const taskList = [{ isDirectInput: false, volume: 0 }];

    const newTaskList = calculateEachRatioOfNonDirectInputTasks(taskList);

    // should avoid this: 0/0 -> NaN
    expect(newTaskList[0].ratio).toBe(0);
  });
});

describe('calculateEachTaskTimeOfNonDirectInputTasks(realWorkTime, taskList)', () => {
  test('calculate each task-hours of non-direct-input tasks correctly', () => {
    const realWorkTime = 600;
    const taskList = [
      { isDirectInput: false, volume: 100, ratio: 34 },
      { isDirectInput: false, volume: 200, ratio: 66 },
      { isDirectInput: true, taskTime: 300 },
      { isDirectInput: false, volume: 0, ratio: 0 },
    ];

    const newTaskList = calculateEachTaskTimeOfNonDirectInputTasks(
      realWorkTime,
      taskList
    );

    expect(newTaskList[0].taskTime).toBe(102);
    expect(newTaskList[1].taskTime).toBe(198);
  });

  test('should add the round-off errors of task-hours to the first non-direct-input task', () => {
    const realWorkTime = 303;
    const taskList = [
      { isDirectInput: false, volume: 48, ratio: 16 },
      { isDirectInput: false, volume: 102, ratio: 32 },
      { isDirectInput: false, volume: 165, ratio: 52 },
      { isDirectInput: true, taskTime: 300 },
    ];

    const newTaskList = calculateEachTaskTimeOfNonDirectInputTasks(
      realWorkTime,
      taskList
    );

    expect(newTaskList[1].taskTime).toBe(0);
    expect(newTaskList[2].taskTime).toBe(1);
    // should contain the round-off errors
    expect(newTaskList[0].taskTime).toBe(2);
  });

  test('should set task-hours to null if the volume is 0', () => {
    const realWorkTime = 500;
    const taskList = [
      {
        isDirectInput: false,
        volume: 0,
        taskTime: null,
        ratio: 0,
      },
    ];

    const newTaskList = calculateEachTaskTimeOfNonDirectInputTasks(
      realWorkTime,
      taskList
    );

    expect(newTaskList[0].taskTime).toBe(null);
  });

  test('should set each task-hours of non-direct-input tasks to null when the actual work hours is null', () => {
    const realWorkTime = null;
    const taskList = [{ isDirectInput: false, volume: 10, ratio: 100 }];

    const newTaskList = calculateEachTaskTimeOfNonDirectInputTasks(
      realWorkTime,
      taskList
    );

    expect(newTaskList[0].taskTime).toBe(null);
  });

  test('should set each task-hours of non-direct-input tasks to null when the total task-hours of direct-input tasks is longer than actual work hours', () => {
    const realWorkTime = 200;
    const taskList = [
      { isDirectInput: false, volume: 48, ratio: 16 },
      { isDirectInput: false, volume: 102, ratio: 32 },
      { isDirectInput: false, volume: 165, ratio: 52 },
      { isDirectInput: true, taskTime: 300 },
    ];

    const newTaskList = calculateEachTaskTimeOfNonDirectInputTasks(
      realWorkTime,
      taskList
    );

    expect(newTaskList[1].taskTime).toBe(null);
    expect(newTaskList[2].taskTime).toBe(null);
  });
});

describe('ensureTaskOfDefaultJobExists(realWorkTime, defaultJob, taskList)', () => {
  const dummyDefaultJob = {
    id: 'a0N7F000002dQdTUAU',
    code: 'default001',
    name: 'Default Job',
    isDirectCharged: true,
  };

  const dummyNonDirectInputTask = {
    jobId: 'a0N7F000000SjqSUAS',
    workCategoryId: null,
    isDirectInput: false,
    volume: 600,
    ratio: 100,
  };

  test('should replace an non-direct-input task which has same job and no work category and some volume', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [
      {
        jobId: dummyDefaultJob.id,
        workCategoryId: null,
        isDirectInput: false,
        volume: 300,
        taskTime: null,
        eventTaskTime: 0,
      },
      {
        ...dummyNonDirectInputTask,
        volume: 300,
      },
    ];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList).toHaveLength(2);
    expect(newTaskList[0].volume).toBe(600);
  });

  test('should replace an non-direct-input task which has same job and no work category and no volume', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [
      {
        jobId: dummyDefaultJob.id,
        workCategoryId: null,
        isDirectInput: false,
        volume: 0,
        eventTaskTime: 0,
      },
    ];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList).toHaveLength(1);
    expect(newTaskList[0].volume).toBe(0);
  });

  test('should not replace an non-direct-input task which has same job and a different work category', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [
      {
        jobId: dummyDefaultJob.id,
        workCategoryId: 'a0X7F000000T0F6UAK',
        isDirectInput: false,
        volume: 600,
        eventTaskTime: 0,
      },
    ];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList).toHaveLength(2);
  });

  test('should replace an direct-input task which has same job and no work category when its task-hours is 0', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [
      {
        jobId: dummyDefaultJob.id,
        workCategoryId: null,
        isDirectInput: true,
        taskTime: 0,
        eventTaskTime: 0,
      },
    ];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList).toHaveLength(1);
    expect(newTaskList[0].isDirectInput).toBe(false);
  });

  test('should not replace an direct-input task which has same job and no work category when its task-hours is not 0', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [
      {
        jobId: dummyDefaultJob.id,
        workCategoryId: null,
        isDirectInput: true,
        taskTime: 60,
        eventTaskTime: 0,
      },
    ];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList).toHaveLength(2);
  });

  test('should replace an event-related task which has same job when its task-hours is 0', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [
      {
        jobId: dummyDefaultJob.id,
        workCategoryId: null,
        isDirectInput: true,
        taskTime: 0,
        eventTaskTime: 60,
      },
    ];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList).toHaveLength(1);
    expect(newTaskList[0]).toEqual(
      expect.objectContaining({
        isDirectInput: false,
        taskTime: null,
        // should set the sum of event duration to 0
        eventTaskTime: 0,
      })
    );
  });

  test("should append a task of Default Job when it doesn't exist in the list", () => {
    const realWorkTime = null;
    const taskList = [];
    const defaultJob = dummyDefaultJob;

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList).toHaveLength(1);

    expect(newTaskList[0].id).toBeTruthy();
    expect(newTaskList[0]).toEqual(
      expect.objectContaining({
        jobId: 'a0N7F000002dQdTUAU',
        jobCode: 'default001',
        jobName: 'Default Job',
        isDirectCharged: true,
        workCategoryId: null,
        color: { base: '', linked: '' },
        isDirectInput: false,
        volume: 600,
        ratio: null,
        taskTime: null,
        eventTaskTime: 0,
        workCategoryList: [],
      })
    );
  });

  test('should make other non-direct-input tasks direct-input', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [dummyNonDirectInputTask];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList[0].isDirectInput).toBe(true);
  });

  test('should keep each task-hours of other non-direct-input tasks when actual work hours is given', () => {
    const realWorkTime = 480;
    const defaultJob = dummyDefaultJob;
    const taskList = [dummyNonDirectInputTask];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList[0].taskTime).toBe(480);
  });

  test('should clear each ratio of other non-direct-input tasks', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [dummyNonDirectInputTask];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList[0].ratio).toBeNull();
  });

  test('should clear each task-time of other non-direct-input tasks', () => {
    const realWorkTime = null;
    const defaultJob = dummyDefaultJob;
    const taskList = [dummyNonDirectInputTask];

    const newTaskList = ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );

    expect(newTaskList[0].taskTime).toBeNull();
  });
});

describe('completeTaskTimeOfEventRelatedTasks(taskList)', () => {
  test('should complete each task-hours of event-related tasks whose task-hours is 0', () => {
    const taskList = [
      {
        // if the task-hours is 0, it means the task was added automatically
        // (not added by an user)
        taskTime: 0,
        eventTaskTime: 180,
      },
    ];

    const newTaskList = completeTaskTimeOfEventRelatedTasks(taskList);

    expect(newTaskList[0].taskTime).toBe(180);
  });

  test('should not overwrite task-hours of event-related tasks whose task-hours is not 0', () => {
    const taskList = [
      {
        // if the task-hours is not 0, it was set by an user
        taskTime: 100,
        eventTaskTime: 180,
      },
    ];

    const newTaskList = completeTaskTimeOfEventRelatedTasks(taskList);

    expect(newTaskList[0].taskTime).toBe(100);
  });
});
