// @flow

import uuid from 'uuid/v4';

import type { WorkCategory } from '../time-tracking/WorkCategory';
import type { Job } from '../time-tracking/Job';
import TaskColorUtil from '../../../commons/utils/TaskColorUtil';

export type DailySummaryTaskBarColor = {
  base: string,
  linked: string,
};

export type DailySummaryTask = {
  id: string,
  jobId: string,
  jobCode: string,
  jobName: string,
  isDirectCharged: boolean,
  workCategoryId: ?string,
  workCategoryName: ?string,
  workCategoryCode: ?string,
  color: DailySummaryTaskBarColor,
  isDirectInput: boolean,
  volume: ?number,
  ratio: ?number,
  taskTime: ?number,
  eventTaskTime: number,
  workCategoryList: WorkCategory[],
};

export const colorizeTask = (
  colorIndex: number,
  task: DailySummaryTask
): DailySummaryTask => ({
  ...task,
  color: TaskColorUtil.getNextColor(colorIndex),
});

export const colorizeTasks = (
  startColorIndex: number,
  nonColorizedTasks: DailySummaryTask[]
): DailySummaryTask[] =>
  nonColorizedTasks.map((task, i) => colorizeTask(startColorIndex + i, task));

export const create = (colorIndex: number, job: Job) =>
  colorizeTask(colorIndex, {
    id: uuid(),
    jobId: job.id,
    jobCode: job.code,
    jobName: job.name,
    isDirectCharged: job.isDirectCharged,
    workCategoryId: null,
    workCategoryName: null,
    workCategoryCode: null,
    color: { base: '', linked: '' },
    isDirectInput: true,
    volume: null,
    ratio: null,
    taskTime: 0,
    eventTaskTime: 0,
    // will be updated
    workCategoryList: [],
  });
