// @flow
import Api from '../../commons/api';

type UpdateRequestType = {
  defaultJobId?: ?string,
  approverBase01Id?: ?string,
};

// eslint-disable-next-line import/prefer-default-export
export const update = (param: UpdateRequestType) =>
  Api.invoke({
    path: '/personal-setting/update',
    param,
  });
