// @flow
import { type Reducer } from 'redux';

import { dropRight } from 'lodash';

//
// constants
//
export const ACTIONS = {
  HIDE: 'MODULES/DIALOG/ACTIVE_DIALOG/HIDE',
  HIDE_ALL: 'MODULES/DIALOG/ACTIVE_DIALOG/HIDE_ALL',
  SEARCH_CONDITION: 'MODULES/DIALOG/ACTIVE_DIALOG/SEARCH_CONDITION',
  DELETE_SEARCH_CONDITION:
    'MODULES/DIALOG/ACTIVE_DIALOG/DELETE_SEARCH_CONDITION',
};

export const dialogTypes = {
  HIDE: 'HIDE',
  SEARCH_CONDITION: 'SEARCH_CONDITION',
  DELETE_SEARCH_CONDITION: 'DELETE_SEARCH_CONDITION',
};

//
// actions
//
export const actions = {
  hide: () => ({
    type: ACTIONS.HIDE,
    payload: dialogTypes.HIDE,
  }),
  hideAll: () => ({
    type: ACTIONS.HIDE_ALL,
  }),
  searchCondition: () => ({
    type: ACTIONS.SEARCH_CONDITION,
    payload: dialogTypes.SEARCH_CONDITION,
  }),
  deleteSearchCondition: () => ({
    type: ACTIONS.DELETE_SEARCH_CONDITION,
    payload: dialogTypes.DELETE_SEARCH_CONDITION,
  }),
};

//
// Reducer
//
const initialState = [];
export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.HIDE:
      return dropRight(state);
    case ACTIONS.HIDE_ALL:
      return initialState;
    case ACTIONS.SEARCH_CONDITION:
    case ACTIONS.DELETE_SEARCH_CONDITION:
      return [...state, action.payload];
    default:
      return state;
  }
}: Reducer<Array<string>, any>);
