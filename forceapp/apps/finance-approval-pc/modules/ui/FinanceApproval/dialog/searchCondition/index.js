// @flow

import { combineReducers } from 'redux';

import name from './name';
import inputError from './inputError';

export default combineReducers<Object, Object>({
  name,
  inputError,
});
