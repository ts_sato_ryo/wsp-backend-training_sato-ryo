// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/RECEIPT/SET',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/RECEIPT/CLEAR',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/RECEIPT/REPLACE',
};

// type Details = Array<string>;

export const actions = {
  set: (hasReceipt: Array<boolean>) => ({
    type: ACTIONS.SET,
    payload: hasReceipt,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = false;

export default ((state: any = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return !state;
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<boolean, any>);
