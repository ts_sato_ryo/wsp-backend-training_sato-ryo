// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/REPORTNO/SET',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/REPORTNO/REPLACE',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/REPORTNO/CLEAR',
};

export const actions = {
  set: (reportNo: string) => ({
    type: ACTIONS.SET,
    payload: reportNo,
  }),
  replace: (reportNo: string) => ({
    type: ACTIONS.REPLACE,
    payload: reportNo,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = null;

export default ((state: ?string = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const setValue = action.payload === '' ? null : action.payload;
      return setValue;
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      const replaceValue = action.payload === '' ? null : action.payload;
      return replaceValue;
    default:
      return state;
  }
}: Reducer<?string, any>);
