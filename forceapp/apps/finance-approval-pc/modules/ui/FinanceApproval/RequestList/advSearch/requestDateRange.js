// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

import {
  type DateRangeOption,
  requestDateInitVal,
} from '../../../../../../domain/models/exp/FinanceApproval';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/REQUESTDATEDATE/SET',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/REQUESTDATEDATE/REPLACE',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/REQUESTDATEDATE/CLEAR',
};

export const actions = {
  set: (requestDate: DateRangeOption) => ({
    type: ACTIONS.SET,
    payload: requestDate,
  }),
  replace: (requestDate: DateRangeOption) => ({
    type: ACTIONS.REPLACE,
    payload: requestDate,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = requestDateInitVal();

export default ((state: DateRangeOption = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const startDate =
        action.payload.startDate === '' ? null : action.payload.startDate;
      const endDate =
        action.payload.endDate === '' ? null : action.payload.endDate;
      return {
        startDate,
        endDate,
      };
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<DateRangeOption, any>);
