// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/EXPTYPE_HISTORY_IDS/SET',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/EXPTYPE_HISTORY_IDS/CLEAR',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/EXPTYPE_HISTORY_IDS/REPLACE',
};

type ExpTypeIds = Array<string>;

export const actions = {
  set: (expTypeId: string) => ({
    type: ACTIONS.SET,
    payload: expTypeId,
  }),
  replace: (expTypeIds: Array<string>) => ({
    type: ACTIONS.REPLACE,
    payload: expTypeIds,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = [];

export default ((state: ExpTypeIds = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const newExpTypeList = _.cloneDeep(state);
      const newItem = action.payload;
      if (!_.includes(state, newItem)) {
        newExpTypeList.push(newItem);
      } else {
        _.pull(newExpTypeList, newItem);
      }
      return newExpTypeList;
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<ExpTypeIds, any>);
