/* @flow */
import { combineReducers } from 'redux';

import departmentBaseIds from './departmentBaseIds';
import empBaseIds from './empBaseIds';
import detail from './detail';
import requestDateRange from './requestDateRange';
import accountingDateRange from './accountingDateRange';
// import receipt from './receipt';
// import expTypeIds from './expTypeIds';
import amountRange from './amountRange';
import reportNo from './reportNo';
import financeStatusList from './financeStatusList';

export default combineReducers<Object, Object>({
  departmentBaseIds,
  empBaseIds,
  detail,
  requestDateRange,
  accountingDateRange,
  // receipt,
  // expTypeIds,
  amountRange,
  reportNo,
  financeStatusList,
});
