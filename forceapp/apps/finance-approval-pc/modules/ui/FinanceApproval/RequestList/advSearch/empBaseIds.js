// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/EMPLOYEE_HISTORY_IDS/SET',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/EMPLOYEE_HISTORY_IDS/CLEAR',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/EMPLOYEE_HISTORY_IDS/REPLACE',
};

type EmployeeHistoryIds = Array<string>;

export const actions = {
  set: (employeeBaseId: string) => ({
    type: ACTIONS.SET,
    payload: employeeBaseId,
  }),
  replace: (employeeBaseIds: Array<string>) => ({
    type: ACTIONS.REPLACE,
    payload: employeeBaseIds,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = [];

export default ((state: EmployeeHistoryIds = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const newDepList = _.cloneDeep(state);
      const newItem = action.payload;
      if (!_.includes(state, newItem)) {
        newDepList.push(newItem);
      } else {
        _.pull(newDepList, newItem);
      }
      return newDepList;
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<EmployeeHistoryIds, any>);
