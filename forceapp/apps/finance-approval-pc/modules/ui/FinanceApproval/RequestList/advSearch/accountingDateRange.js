// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

import {
  type DateRangeOption,
  accountingDateInitVal,
} from '../../../../../../domain/models/exp/FinanceApproval';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/ACCOUNTINGDATE/SET',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/ACCOUNTINGDATE/REPLACE',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/ACCOUNTINGDATE/CLEAR',
};

export const actions = {
  set: (accountingDate: DateRangeOption) => ({
    type: ACTIONS.SET,
    payload: accountingDate,
  }),
  replace: (recordDate: DateRangeOption) => ({
    type: ACTIONS.REPLACE,
    payload: recordDate,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = accountingDateInitVal;

export default ((state: DateRangeOption = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const startDate =
        action.payload.startDate === '' ? null : action.payload.startDate;
      const endDate =
        action.payload.endDate === '' ? null : action.payload.endDate;
      return {
        startDate,
        endDate,
      };
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<DateRangeOption, any>);
