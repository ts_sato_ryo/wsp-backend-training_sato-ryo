// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

import { financeStatusListInitParam } from '../../../../../../domain/models/exp/FinanceApproval';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/FINANCIAL_STATUS_LIST/SET',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/FINANCIAL_STATUS_LIST/CLEAR',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/FINANCIAL_STATUS_LIST/REPLACE',
};

type FinancialStatuses = Array<string>;

export const actions = {
  set: (financialStatus: string) => ({
    type: ACTIONS.SET,
    payload: financialStatus,
  }),
  replace: (financialStatuses: Array<string>) => ({
    type: ACTIONS.REPLACE,
    payload: financialStatuses,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//

export default ((
  state: FinancialStatuses = financeStatusListInitParam,
  action
) => {
  switch (action.type) {
    case ACTIONS.SET:
      const newDepList = _.cloneDeep(state);
      const newItem = action.payload;
      if (!_.includes(state, newItem)) {
        newDepList.push(newItem);
      } else {
        _.pull(newDepList, newItem);
      }
      return newDepList;
    case ACTIONS.CLEAR:
      return financeStatusListInitParam;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<FinancialStatuses, any>);
