// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/DETAIL/SET',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/DETAIL/CLEAR',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/DETAIL/REPLACE',
};

type Details = Array<string>;

export const actions = {
  set: (detail: string) => ({
    type: ACTIONS.SET,
    payload: detail,
  }),
  replace: (details: Details) => ({
    type: ACTIONS.REPLACE,
    payload: details,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = [];

export default ((state: Details = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const newDetailList = _.cloneDeep(state);
      const newItem = action.payload;
      if (!_.includes(state, newItem)) {
        newDetailList.push(newItem);
      } else {
        _.pull(newDetailList, newItem);
      }
      return newDetailList;
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<Details, any>);
