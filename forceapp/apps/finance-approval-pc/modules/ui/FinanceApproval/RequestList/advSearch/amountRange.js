// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

import { type AmountRangeOption } from '../../../../../../domain/models/exp/FinanceApproval';

export const ACTIONS = {
  SET: 'UI/FINANCE_APPROVAL/REQUESTLIST/AMOUNT/SET',
  REPLACE: 'UI/FINANCE_APPROVAL/REQUESTLIST/AMOUNT/REPLACE',
  CLEAR: 'UI/FINANCE_APPROVAL/REQUESTLIST/AMOUNT/CLEAR',
};

export const actions = {
  set: (amount: AmountRangeOption) => ({
    type: ACTIONS.SET,
    payload: amount,
  }),
  replace: (amount: AmountRangeOption) => ({
    type: ACTIONS.REPLACE,
    payload: amount,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = { startAmount: null, endAmount: null };

export default ((state: AmountRangeOption = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const amountRange = {
        startAmount: action.payload.startAmount,
        endAmount: action.payload.endAmount,
      };
      return amountRange;
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<AmountRangeOption, any>);
