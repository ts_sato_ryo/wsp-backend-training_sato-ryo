/* @flow */
import { combineReducers } from 'redux';

import page from './page';
import advSearch from './advSearch';
import sortBy from './sortBy';
import orderBy from './orderBy';
import selectedSearchCondition from './selectedSearchCondition';

export default combineReducers<Object, Object>({
  page,
  sortBy,
  orderBy,
  advSearch,
  selectedSearchCondition,
});
