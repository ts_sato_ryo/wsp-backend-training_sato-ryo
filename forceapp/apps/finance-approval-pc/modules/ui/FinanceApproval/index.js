/* @flow */
import { combineReducers } from 'redux';

import RequestList from './RequestList';
import dialog from './dialog';
import DropdownValues from './DropdownValues';

export default combineReducers<Object, Object>({
  RequestList,
  dialog,
  DropdownValues,
});
