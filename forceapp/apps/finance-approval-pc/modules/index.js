/* @flow */
import { combineReducers } from 'redux';

import ui from './ui';
import common from '../../commons/modules';
import userSetting from '../../commons/reducers/userSetting';
import entities from './entities';

export default combineReducers<Object, Object>({
  common,
  entities,
  userSetting,
  ui,
});
