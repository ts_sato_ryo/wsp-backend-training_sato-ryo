// @flow
import { type Reducer, type Dispatch } from 'redux';

import _ from 'lodash';

import msg from '../../../commons/languages';

import Api from '../../../commons/api';

import {
  type SearchConditions,
  searchConditionInitValue,
} from '../../../domain/models/exp/FinanceApproval';
import {
  getPersonalSetting,
  updatePersonalSetting,
} from '../../models/advSearchCondition';

export const ACTIONS = {
  GET_SUCCESS: 'MODULES/ENTITIES/ADVSEARCHCONDITION/GET_SUCCESS',
  SAVE: 'MODULES/UI/EXP/ADVSEARCHCONDITION/SAVE',
  DELETE: 'MODULES/UI/EXP/ADVSEARCHCONDITION/DELETE',
};

// eslint-disable-next-line import/prefer-default-export
export const update = (param: SearchConditions) =>
  Api.invoke({
    path: '/personal-setting/update',
    param,
  });

const getSuccess = (result: Array<SearchConditions>) => ({
  type: ACTIONS.GET_SUCCESS,
  payload: result,
});

const addInitialValue = (res: Array<SearchConditions>) => {
  const resWithInitVal = _.cloneDeep(res);
  const initValue = _.cloneDeep(searchConditionInitValue());
  initValue.name = msg().Exp_Lbl_SearchConditionApprovelreRuestList;
  resWithInitVal.unshift(initValue);
  return resWithInitVal;
};

export const actions = {
  get: () => (dispatch: Dispatch<any>): Promise<Array<SearchConditions>> => {
    return getPersonalSetting().then((res: Array<SearchConditions>) =>
      dispatch(getSuccess(addInitialValue(res)))
    );
  },
  save: (searchConditionList: Array<SearchConditions>) => () => {
    return updatePersonalSetting(searchConditionList);
  },
  delete: () => () => {
    return updatePersonalSetting(null);
  },
};

function initialState(): Array<SearchConditions> {
  return [searchConditionInitValue()];
}

export default ((state = initialState(), action) => {
  switch (action.type) {
    case ACTIONS.GET_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<Array<SearchConditions>, any>);
