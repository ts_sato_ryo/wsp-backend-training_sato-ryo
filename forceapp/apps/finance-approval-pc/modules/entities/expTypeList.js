// @flow
import { type Reducer, type Dispatch } from 'redux';
import {
  type ExpenseTypeList,
  getExpenseTypeList,
} from '../../../domain/models/exp/ExpenseType';

export type ExpTypeOption = {
  label: string,
  value: string,
};

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/ENTITIES/EXPTYPE_LIST/LIST_SUCCESS',
};

const listSuccess = (expTypeList: ExpenseTypeList) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: expTypeList,
});

export const actions = {
  list: (companyId?: ?string, targetDate?: ?string) => (
    dispatch: Dispatch<any>
  ): Promise<ExpenseTypeList> => {
    return getExpenseTypeList(companyId, targetDate).then(
      (res: ExpenseTypeList) => dispatch(listSuccess(res))
    );
  },
};

const convertStyle = (expTypeList: ExpenseTypeList) => {
  const options = expTypeList.map((expType) => {
    const expTypeOption: ExpTypeOption = {
      label: expType.name,
      value: expType.id,
    };
    return expTypeOption;
  });
  return options;
};

const initialState: Array<ExpTypeOption> = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return convertStyle(action.payload);
    default:
      return state;
  }
}: Reducer<Array<ExpTypeOption>, any>);
