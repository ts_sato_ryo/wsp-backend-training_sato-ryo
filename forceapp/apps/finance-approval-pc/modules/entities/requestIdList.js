// @flow
import { type Reducer, type Dispatch } from 'redux';
import {
  type FinanceApprovalRequestIds,
  getRequestIdList,
  type SortBy,
  type OrderBy,
  type SearchConditions,
} from '../../../domain/models/exp/FinanceApproval';

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/ENTITIES/REQUEST_ID_LIST/LIST_SUCCESS',
};

const listSuccess = (financeApprovalRequestIds: FinanceApprovalRequestIds) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: financeApprovalRequestIds,
});

export const actions = {
  list: (
    sortBy?: ?SortBy,
    orderBy?: ?OrderBy,
    advSearchConditions?: ?SearchConditions
  ) => (dispatch: Dispatch<any>): Promise<FinanceApprovalRequestIds> => {
    return getRequestIdList(sortBy, orderBy, advSearchConditions).then(
      (res: FinanceApprovalRequestIds) => dispatch(listSuccess(res))
    );
  },
};

// NOTE: it's not really good to a employee has multiple value.
// Since the API returns multiple values at the same time, we are daringly doing this.
const initialState = {
  totalSize: 0,
  requestIdList: [],
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}: Reducer<FinanceApprovalRequestIds, any>);
