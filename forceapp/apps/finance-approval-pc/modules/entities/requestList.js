// @flow
import { type Reducer, type Dispatch } from 'redux';
import {
  type RequestList,
  type RequestIds,
  getRequestList,
} from '../../../domain/models/exp/FinanceApproval';

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/ENTITIES/REQUEST_LIST/LIST_SUCCESS',
  INITIALIZE: 'MODULES/ENTITIES/REQUEST_LIST/INITIALIZE',
};

const listSuccess = (requestList: RequestList) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: requestList,
});

const initialize = () => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: [],
});

export const actions = {
  list: (requestIds: RequestIds) => (
    dispatch: Dispatch<any>
  ): Promise<RequestList> => {
    return getRequestList(requestIds).then((res: RequestList) =>
      dispatch(listSuccess(res))
    );
  },
  initialize: () => (dispatch: Dispatch<any>): Promise<RequestList> => {
    return dispatch(initialize());
  },
};

const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return action.payload;
    case ACTIONS.INITIALIZE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<RequestList, any>);
