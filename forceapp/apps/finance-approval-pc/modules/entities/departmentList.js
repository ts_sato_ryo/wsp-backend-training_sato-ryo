// @flow
import { type Reducer, type Dispatch } from 'redux';
import {
  type DepartmentList,
  getDepartmentList,
} from '../../../domain/models/exp/Department';

export type DepartmentOption = {
  label: string,
  value: string,
};

export const ACTIONS = {
  LIST_SUCCESS: 'MODULES/ENTITIES/DEPARTMENT_LIST/LIST_SUCCESS',
};

const listSuccess = (departmentList: DepartmentList) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: departmentList,
});

export const actions = {
  list: (companyId?: ?string, targetDate?: ?string) => (
    dispatch: Dispatch<any>
  ): Promise<DepartmentList> => {
    return getDepartmentList(companyId, targetDate).then(
      (res: DepartmentList) => dispatch(listSuccess(res))
    );
  },
};

const convertStyle = (departmentList: DepartmentList) => {
  const options = departmentList.map((department) => {
    const departmentOption: DepartmentOption = {
      label: department.name,
      value: department.id,
    };
    return departmentOption;
  });
  return options;
};

const initialState: Array<DepartmentOption> = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return convertStyle(action.payload);
    default:
      return state;
  }
}: Reducer<Array<DepartmentOption>, any>);
