/* @flow */
import { combineReducers } from 'redux';

import exp from '../../../domain/modules/exp';
import approval from '../../../domain/modules/approval';
import financeApproval from '../../../domain/modules/exp/finance-approval';
import requestIdList from './requestIdList';
import requestList from './requestList';
import departmentList from './departmentList';
import expTypeList from './expTypeList';
import employeeList from './employeeList';
import advSearchConditionList from './advSearchConditionList';

export default combineReducers<Object, Object>({
  exp,
  approval,
  financeApproval,
  requestIdList,
  requestList,
  departmentList,
  employeeList,
  expTypeList,
  advSearchConditionList,
});
