/* @flow */
import { compose } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';
import msg from '../../../commons/languages';

import { actions as overlapActions } from '../../../expenses-pc/modules/ui/expenses/overlap';
import { actions as departmentActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/departmentBaseIds';
import { actions as employeeActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/empBaseIds';
// import { actions as expTypeActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/expTypeIds';
import { actions as detailActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/detail';
import { actions as accountingDateActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/accountingDateRange';
import { actions as requestDateActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/requestDateRange';
// import { actions as receiptActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/receipt';
import { actions as activeDialogActions } from '../../../expenses-pc/modules/ui/expenses/dialog/activeDialog';
import { actions as advSearchCondition } from '../../modules/entities/advSearchConditionList';
import { actions as selectedSearchConditionActions } from '../../modules/ui/FinanceApproval/RequestList/selectedSearchCondition';
import { actions as amountActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/amountRange';
import { actions as reportNoActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/reportNo';
import { actions as statusActions } from '../../modules/ui/FinanceApproval/RequestList/advSearch/financeStatusList';
import { actions as departmentListActions } from '../../modules/entities/departmentList';
import { actions as employeeListActions } from '../../modules/entities/employeeList';
// import { actions as dialogNameActions } from '../../modules/ui/FinanceApproval/dialog/searchCondition/name';
import { actions as dialogNameActions } from '../../../expenses-pc/modules/ui/expenses/dialog/approval/comment';

import {
  fetchFinanceApprovalIdList,
  fetchFinanceApprovalList,
  fetchExpRequest,
  backFromDetailToList,
  fetchinitialSetting,
  saveAdvSearchCondition,
  deteleAdvSearchCondition,
  saveSearchCondition,
} from '../../action-dispatchers/FinanceApproval';

import { confirm } from '../../../commons/actions/app';
import { modes } from '../../../expenses-pc/modules/ui/expenses/mode';
import { actions as dropdownActions } from '../../modules/ui/FinanceApproval/DropdownValues';
import {
  detailValue,
  type DateRangeOption,
} from '../../../domain/models/exp/FinanceApproval';
import lifecycle from '../../../mobile-app/concerns/lifecycle';
import FinanceApproval from '../../components/FinanceApproval';

// put function call outside so that it will only initialize once and not everytime when state is updated
const labelObject = () => ({
  reports: msg().Exp_Lbl_Reports,
  newReport: msg().Exp_Lbl_NewReportCreateExp,
  accountingDate: msg().Exp_Clbl_RecordDate,
  isFinanceApproval: true,
});

const mapStateToProps = (state) => {
  const requestIdList = state.entities.requestIdList.requestIdList;
  const selectedRequestId = state.ui.expenses.selectedExpReport.requestId;
  // The idx of the screen being displayed.
  // if no report is selected, values is -1
  const currentRequestIdx = selectedRequestId
    ? requestIdList.indexOf(selectedRequestId)
    : -1;

  return {
    labelObject,
    requestIdList,
    currentRequestIdx,
    mode: state.ui.expenses.mode,
    overlap: state.ui.expenses.overlap,
    currentPage: state.ui.FinanceApproval.RequestList.page,
    requestTotalNum: state.entities.requestIdList.totalSize,
    orderBy: state.ui.FinanceApproval.RequestList.orderBy,
    sortBy: state.ui.FinanceApproval.RequestList.sortBy,
    advSearchDepartment: state.ui.FinanceApproval.RequestList.department,
    advSearchEmployee: state.ui.FinanceApproval.RequestList.employee,
    departmentOptions: state.entities.departmentList,
    employeeOptions: state.entities.employeeList,
    expTypeOptions: state.entities.expTypeList,
    advSearchCondition: state.ui.FinanceApproval.RequestList.advSearch,
    fetchedAdvSearchConditionList: state.entities.advSearchConditionList,
    selectedSearchConditionName:
      state.ui.FinanceApproval.RequestList.selectedSearchCondition,
    selectedDetail: state.ui.FinanceApproval.RequestList.advSearch.detail,
    selectedAccountingDate:
      state.ui.FinanceApproval.RequestList.advSearch.accountingDate,
    selectedRequestDate:
      state.ui.FinanceApproval.RequestList.advSearch.requestDate,
    hasReceipt: state.ui.FinanceApproval.RequestList.advSearch.receipt,
    amount: state.ui.FinanceApproval.RequestList.advSearch.amount,
    dropdownValues: state.ui.FinanceApproval.DropdownValues,
    userSetting: state.userSetting,
    currencyDecimalPlaces: state.userSetting.currencyDecimalPlaces,
    approvalHistory: state.entities.exp.approval.request.history,
  };
};

const mapDispatchToProps = {
  advSearchCondition,
  backFromDetailToList,
  backToHome: overlapActions.nonOverlapReport,
  clearAccountingDate: accountingDateActions.clear,
  clearAmount: amountActions.clear,
  clearReportNo: reportNoActions.clear,
  clearRequestDate: requestDateActions.clear,
  confirm,
  deteleAdvSearchCondition,
  dialog: activeDialogActions.searchCondition,
  fetchDepartmentList: departmentListActions.list,
  fetchEmployeeList: employeeListActions.list,
  fetchExpRequest,
  fetchFinanceApprovalIdList,
  fetchFinanceApprovalList,
  fetchinitialSetting,
  onClickDeleteConditionButton: activeDialogActions.deleteSearchCondition,
  onClickDetail: detailActions.set,
  onClickInputValueAccountingDate: accountingDateActions.set,
  onClickInputValueAmount: amountActions.set,
  onClickInputValueDepartment: departmentActions.set,
  onClickInputValueEmployee: employeeActions.set,
  onClickInputValueReportNo: reportNoActions.set,
  onClickInputValueStatus: statusActions.set,
  resetDepartmentInitialValue: departmentActions.clear,
  resetDetailInitialValue: detailActions.clear,
  resetEmployeeInitialValue: employeeActions.clear,
  resetStatusInitialValue: statusActions.clear,
  saveAdvSearchCondition,
  saveSearchCondition,
  setAccountingDateInitialValue: accountingDateActions.replace,
  setAmountInitialValue: amountActions.replace,
  setComment: dialogNameActions.set,
  setDepartmentInitialValue: departmentActions.replace,
  setDetailInitialValue: detailActions.replace,
  setDropdownValue: dropdownActions.set,
  setEmployeeInitialValue: employeeActions.replace,
  setReportNoInitialValue: reportNoActions.replace,
  setRequestDate: requestDateActions.set,
  setRequestDateInitialValue: requestDateActions.replace,
  setSearchCondition: selectedSearchConditionActions.set,
  setStatusInitialValue: statusActions.replace,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickRefreshButton: () => {
    // To search using same condition for last search
    dispatchProps.fetchFinanceApprovalIdList(
      '',
      '',
      stateProps.advSearchCondition
    );
  },

  onClickAdvSearchButton: () => {
    dispatchProps.fetchFinanceApprovalIdList(
      null,
      null,
      stateProps.advSearchCondition
    );
  },
  // Move to next to report. it's either +1 or -1
  onClickNextToRequestButton: (moveNum: number) => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.fetchExpRequest(
            stateProps.requestIdList[stateProps.currentRequestIdx + moveNum]
          );
        }
      });
    } else {
      dispatchProps.fetchExpRequest(
        stateProps.requestIdList[stateProps.currentRequestIdx + moveNum]
      );
    }
  },
  // Take a list according to the idx of display screen
  // when come back from detail screen again.
  onClickBackButton: () => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.backFromDetailToList(
            stateProps.requestIdList,
            stateProps.currentRequestIdx
          );
        }
      });
    } else {
      dispatchProps.backFromDetailToList(
        stateProps.requestIdList,
        stateProps.currentRequestIdx
      );
    }
  },
  onClickSaveConditionButton: () => {
    dispatchProps.dialog();
    dispatchProps.setComment(stateProps.selectedSearchConditionName);
  },
  onClickCondititon: (e: SyntheticInputEvent<HTMLInputElement>) => {
    const searchConditionName = e.target.value;
    const selectedSearchCondition = _.find(
      stateProps.fetchedAdvSearchConditionList,
      { name: searchConditionName }
    );
    dispatchProps.setSearchCondition(searchConditionName);
    if (
      searchConditionName === stateProps.selectedSearchConditionName ||
      !searchConditionName
    ) {
      dispatchProps.resetDepartmentInitialValue();
      dispatchProps.resetEmployeeInitialValue();
      dispatchProps.resetStatusInitialValue();
      dispatchProps.clearRequestDate();
      dispatchProps.clearAccountingDate();
      dispatchProps.clearReportNo();
      dispatchProps.clearAmount();
      dispatchProps.resetDetailInitialValue();
      dispatchProps.fetchFinanceApprovalIdList(null, null, null);
    } else {
      const targetDate = selectedSearchCondition.requestDateRange.startDate;
      Promise.all([
        dispatchProps.fetchDepartmentList(
          stateProps.userSetting.companyId,
          targetDate
        ),
        dispatchProps.fetchEmployeeList(
          stateProps.userSetting.companyId,
          targetDate
        ),
      ]).then(() => {
        dispatchProps.setDepartmentInitialValue(
          selectedSearchCondition.departmentBaseIds
        );
        dispatchProps.setEmployeeInitialValue(
          selectedSearchCondition.empBaseIds
        );

        dispatchProps.setStatusInitialValue(
          selectedSearchCondition.financeStatusList
        );
        dispatchProps.setAccountingDateInitialValue(
          selectedSearchCondition.accountingDateRange
        );
        dispatchProps.setRequestDateInitialValue(
          selectedSearchCondition.requestDateRange
        );
        dispatchProps.setReportNoInitialValue(selectedSearchCondition.reportNo);
        dispatchProps.setAmountInitialValue(
          selectedSearchCondition.amountRange
        );

        dispatchProps.setDetailInitialValue(selectedSearchCondition.detail);

        dispatchProps.fetchFinanceApprovalIdList(
          null,
          null,
          selectedSearchCondition
        );
      });
    }
  },
  // onClickDeleteConditionButton: () => {
  //   dispatchProps.deleteSearchConditionDialog();
  // },
  onClickDeteleAccountingDate: () => {
    dispatchProps.clearAccountingDate();
    dispatchProps.onClickDetail(detailValue.ACCOUNTING_DATE);
  },
  onClickDeteleReportNo: () => {
    dispatchProps.clearReportNo();
    dispatchProps.onClickDetail(detailValue.REPORT_ID);
  },
  onClickDeteleAmount: () => {
    dispatchProps.clearAmount();
    dispatchProps.onClickDetail(detailValue.AMOUNT);
  },
  onClickInputValueRequestDate: (
    date: DateRangeOption,
    needUpdateList: boolean
  ) => {
    dispatchProps.setRequestDate(date);
    if (needUpdateList) {
      dispatchProps.fetchDepartmentList(
        stateProps.userSetting.companyId,
        date.startDate
      );
      dispatchProps.fetchEmployeeList(
        stateProps.userSetting.companyId,
        date.startDate
      );
    }
  },
});

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  ),
  lifecycle({
    componentDidMount(_dispatch: Dispatch<any>, props) {
      props.fetchFinanceApprovalIdList();
      props.fetchinitialSetting(props.userSetting.companyId);
    },
  })
)(FinanceApproval): React.ComponentType<Object>);
