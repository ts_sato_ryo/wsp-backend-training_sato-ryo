// @flow
import { connect } from 'react-redux';
import { cloneDeep, get } from 'lodash';
import { withFormik } from 'formik';

import msg from '../../../commons/languages';

import schema from '../../../expenses-pc/schema/ExpensesRequest';

import { confirm } from '../../../commons/actions/app';
import { actions as overlapActions } from '../../../expenses-pc/modules/ui/expenses/overlap';
import {
  actions as modeActions,
  modes,
} from '../../../expenses-pc/modules/ui/expenses/mode';

import { actions as activeDialogActions } from '../../../expenses-pc/modules/ui/expenses/dialog/activeDialog';
import { actions as commentActions } from '../../../expenses-pc/modules/ui/expenses/dialog/approval/comment';
import { actions as nameActions } from '../../modules/ui/FinanceApproval/dialog/searchCondition/name';

import {
  deleteExpReport,
  openApprovalHistoryDialog,
  openEditHistoryDialog,
} from '../../../expenses-pc/action-dispatchers/Expenses';
import {
  saveExpReport,
  saveExpRecord,
  fetchExpRequest,
  approve as approveFA,
  reject as rejectFA,
} from '../../action-dispatchers/FinanceApproval';

import { MAX_SEARCH_RESULT_NUM } from '../../modules/ui/FinanceApproval/RequestList/page';
import RequestFormView from '../../../commons/components/exp/Form';

const mapStateToProps = (state) => ({
  isFinanceApproval: true,
  mode: state.ui.expenses.mode,
  overlap: state.ui.expenses.overlap,
  requestList: state.entities.requestList,
  requestIdList: state.entities.requestIdList,
  selectedExpReport: state.ui.expenses.selectedExpReport,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  taxTypeListForSaving: state.ui.expenses.recordItemPane.tax,
  orderBy: state.ui.FinanceApproval.RequestList.orderBy,
  sortBy: state.ui.FinanceApproval.RequestList.sortBy,
  apActive: state.ui.expenses.recordListPane.accountingPeriod.filter(
    (ap) => ap.active
  ),
});

const mapDispatchToProps = {
  approveFA,
  changeModetoSelect: modeActions.reportSelect,
  clearComments: commentActions.clear,
  confirm,
  fetchExpRequest,
  moveBackToReport: overlapActions.nonOverlapReport,
  onChangeComment: nameActions.set,
  onClickDeleteButton: deleteExpReport,
  onClickSubmitButton: activeDialogActions.approval,
  openApprovalHistoryDialog,
  openCancelDialog: activeDialogActions.cancelRequest,
  openEditHistoryDialog,
  openRejectFADialog: activeDialogActions.rejectFADialog,
  rejectFA,
  reportEdit: modeActions.reportEdit,
  saveExpReport,
  saveExpRecord,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickRejectButton: () => {
    dispatchProps.openRejectFADialog();
  },
  onClickApproveButton: () => {
    const { requestList, requestIdList, selectedExpReport } = stateProps;
    const requestId = selectedExpReport.requestId;
    const requestIds = requestIdList && requestIdList.requestIdList;
    const index = requestIds.indexOf(requestId);
    const totalRequests = requestIdList.totalSize;
    const maxIndex =
      totalRequests < MAX_SEARCH_RESULT_NUM
        ? totalRequests - 1
        : MAX_SEARCH_RESULT_NUM - 1;
    const nextIndex = index === maxIndex ? index : index + 1;
    dispatchProps.approveFA([requestId], requestIds[nextIndex], requestList);
  },
  onClickEditHistoryButton: () =>
    dispatchProps.openEditHistoryDialog(stateProps.selectedExpReport.requestId),
  onClickDeleteButton: () => {
    dispatchProps.confirm(msg().Att_Msg_DailyReqConfirmRemove, (yes) => {
      if (yes) {
        dispatchProps.onClickDeleteButton(
          stateProps.selectedExpReport.reportId
        );
      }
    });
  },
  onClickCancelRequestButton: () => {
    dispatchProps.clearComments();
    dispatchProps.openCancelDialog();
  },
  onClickApprovalHistoryButton: () =>
    dispatchProps.openApprovalHistoryDialog(
      stateProps.selectedExpReport.requestId
    ),
  onClickBackButton: () => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.fetchExpRequest(stateProps.selectedExpReport.requestId);
          dispatchProps.changeModetoSelect();
        }
      });
    }
  },
  onClickSaveButton: (expReport, reportTypeList, defaultTaxType) => {
    expReport.attachedFileData = null;
    expReport.records.forEach((record) => {
      if (record.items[0].taxTypeBaseId === 'noIdSelected') {
        const selectedTaxType =
          defaultTaxType[record.items[0].expTypeId][record.recordDate][0];
        record.items[0].taxTypeBaseId = selectedTaxType.baseId;
        record.items[0].taxTypeHistoryId = selectedTaxType.historyId;
        record.items[0].taxTypeName = selectedTaxType.name;
      }
    });
    dispatchProps.saveExpReport(
      expReport,
      stateProps.sortBy,
      stateProps.orderBy
    );
  },
  saveRecord: (
    selectedRecord,
    reportTypeList,
    defaultTaxType,
    reportId,
    requestId,
    reportTypeId
  ) => {
    const record = cloneDeep(selectedRecord);
    record.receiptData = null;
    if (record.items[0].taxTypeBaseId === 'noIdSelected') {
      const selectedTaxType = get(
        defaultTaxType,
        `${record.items[0].expTypeId}.${record.recordDate}.0`,
        {}
      );
      record.items[0].taxTypeBaseId = selectedTaxType.baseId;
      record.items[0].taxTypeHistoryId = selectedTaxType.historyId;
      record.items[0].taxTypeName = selectedTaxType.name;
    }

    dispatchProps.saveExpRecord(
      record,
      stateProps.sortBy,
      stateProps.orderBy,
      reportId,
      requestId,
      reportTypeId
    );
  },
});

const expensesRequestForm = withFormik({
  // permission for change by props update (when initialised by reducer)
  enableReinitialize: true,
  mapPropsToValues: (props) => ({
    ui: {
      checkboxes: [],
      recordIdx: -1,
      recalc: false,
      saveMode: false,
      isRecordSave: false,
      submitMode: false,
    },
    report: props.selectedExpReport,
  }),
  validationSchema: schema,
  handleSubmit: (values, { props, setFieldValue }) => {
    const { ui, report } = values;
    const { saveMode, isRecordSave, recordIdx } = ui;

    if (saveMode) {
      if (isRecordSave) {
        const currentRecord = report.records[recordIdx];

        // save single Record
        props.saveRecord(
          currentRecord,
          props.reportTypeList,
          props.taxTypeListForSaving,
          report.reportId,
          report.requestId,
          report.expReportTypeId
        );
      } else {
        // save whole report without records
        props.onClickSaveButton(
          values.report,
          props.reportTypeList,
          props.taxTypeListForSaving
        );
      }
    } else {
      props.onClickSubmitButton();
    }
    setFieldValue('ui.saveMode', false);
    setFieldValue('ui.isRecordSave', false);
    setFieldValue('ui.submitMode', false);
  },
})(RequestFormView);

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(expensesRequestForm): React.ComponentType<*>): React.ComponentType<Object>);
