/* @flow */
import { connect } from 'react-redux';

import { actions as overlapActions } from '../../../expenses-pc/modules/ui/expenses/overlap';
import {
  fetchExpRequest,
  fetchFinanceApprovalIdList,
  fetchFinanceApprovalList,
} from '../../action-dispatchers/FinanceApproval';

import { actions as accountingPeriodActions } from '../../../expenses-pc/modules/ui/expenses/recordListPane/accountingPeriod';

import {
  ORDER_BY,
  type SortBy,
} from '../../../domain/models/exp/FinanceApproval';

import RequestListView from '../../components/FinanceApproval/RequestList';

const mapStateToProps = (state) => ({
  mode: state.ui.expenses.mode,
  companyId: state.userSetting.companyId,
  currentPage: state.ui.FinanceApproval.RequestList.page,
  requestList: state.entities.requestList,
  requestIdList: state.entities.requestIdList.requestIdList,
  requestTotalNum: state.entities.requestIdList.totalSize,
  baseCurrencySymbol: state.userSetting.currencySymbol,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  orderBy: state.ui.FinanceApproval.RequestList.orderBy,
  sortBy: state.ui.FinanceApproval.RequestList.sortBy,
  advSearchCondition: state.ui.FinanceApproval.RequestList.advSearch,
  detail: state.ui.FinanceApproval.RequestList.advSearch.detail,
});

const mapDispatchToProps = {
  fetchExpRequest,
  fetchFinanceApprovalIdList,
  fetchFinanceApprovalList,
  moveToExpensesForm: overlapActions.overlapReport,
  searchAccountPeriod: accountingPeriodActions.search,
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickRequestItem: (requestId: string) => {
    const { companyId, requestList } = stateProps;
    dispatchProps.fetchExpRequest(requestId, requestList);
    dispatchProps.moveToExpensesForm();
    dispatchProps.searchAccountPeriod(companyId);
  },
  onClickPagerLink: (pageNum: number) => {
    dispatchProps.fetchFinanceApprovalList(stateProps.requestIdList, pageNum);
  },
  onClickSortKey: (sortKey: SortBy) => {
    // List is sorted by Desc when sortKey is same last time and current order is asc
    const orderBy =
      stateProps.sortBy === sortKey && stateProps.orderBy === ORDER_BY.Asc
        ? ORDER_BY.Desc
        : ORDER_BY.Asc;
    dispatchProps.fetchFinanceApprovalIdList(
      sortKey,
      orderBy,
      stateProps.advSearchCondition
    );
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RequestListView): React.ComponentType<Object>);
