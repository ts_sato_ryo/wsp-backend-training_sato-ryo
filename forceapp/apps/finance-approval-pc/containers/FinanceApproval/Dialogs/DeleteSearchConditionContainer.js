// @flow
import { connect } from 'react-redux';
import { cloneDeep, remove } from 'lodash';
import msg from '../../../../commons/languages';

import {
  fetchinitialSetting,
  fetchFinanceApprovalIdList,
  deteleAdvSearchCondition,
} from '../../../action-dispatchers/FinanceApproval';
import { actions as commentActions } from '../../../../expenses-pc/modules/ui/expenses/dialog/approval/comment';
import { actions as departmentActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/departmentBaseIds';
import { actions as employeeActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/empBaseIds';
import { actions as statusActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/financeStatusList';
import { actions as accountingDateActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/accountingDateRange';
import { actions as requestDateActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/requestDateRange';
import { actions as reportNoActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/reportNo';
import { actions as amountActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/amountRange';
import { actions as detailActions } from '../../../modules/ui/FinanceApproval/RequestList/advSearch/detail';
import { actions as selectedSearchConditionActions } from '../../../modules/ui/FinanceApproval/RequestList/selectedSearchCondition';
import DeleteSearchCondition from '../../../../commons/components/exp/Form/Dialog/DeleteSearchCondition';

const mapStateToProps = (state) => ({
  title: msg().Exp_Btn_DeleteSearchCondition,
  mainButtonTitle: msg().Exp_Lbl_Reject,
  photoUrl: state.userSetting.photoUrl,
  comment: state.ui.expenses.dialog.approval.comment,
  inputError: state.ui.FinanceApproval.dialog.searchCondition.inputError,
  selectedConditionName:
    state.ui.FinanceApproval.RequestList.selectedSearchCondition,
  fetchedAdvSearchConditionList: state.entities.advSearchConditionList,
  selectedSearchConditionName:
    state.ui.FinanceApproval.RequestList.selectedSearchCondition,
});

const mapDispatchToProps = {
  fetchinitialSetting,
  fetchFinanceApprovalIdList,
  deteleAdvSearchCondition,
  setSearchCondition: selectedSearchConditionActions.set,
  onChangeName: commentActions.set,
  resetDepartmentInitialValue: departmentActions.clear,
  resetDetailInitialValue: detailActions.clear,
  resetEmployeeInitialValue: employeeActions.clear,
  resetStatusInitialValue: statusActions.clear,
  resetComment: commentActions.clear,
  clearRequestDate: requestDateActions.clear,
  clearAccountingDate: accountingDateActions.clear,
  clearAmount: amountActions.clear,
  clearReportNo: reportNoActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickDeleteSearchCondition: () => {
    dispatchProps.resetComment();
    const cFetchedAdvSearchConditionList = cloneDeep(
      stateProps.fetchedAdvSearchConditionList
    );
    cFetchedAdvSearchConditionList.shift();
    remove(cFetchedAdvSearchConditionList, (o) => {
      return o.name === stateProps.selectedSearchConditionName;
    });
    dispatchProps.deteleAdvSearchCondition(cFetchedAdvSearchConditionList);
    dispatchProps.setSearchCondition('');
    dispatchProps.resetDepartmentInitialValue();
    dispatchProps.resetEmployeeInitialValue();
    dispatchProps.resetStatusInitialValue();
    dispatchProps.clearRequestDate();
    dispatchProps.clearAccountingDate();
    dispatchProps.clearReportNo();
    dispatchProps.clearAmount();
    dispatchProps.resetDetailInitialValue();
    dispatchProps.fetchFinanceApprovalIdList(null, null, null);
    dispatchProps.fetchinitialSetting();
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DeleteSearchCondition): React.ComponentType<*>): React.ComponentType<Object>);
