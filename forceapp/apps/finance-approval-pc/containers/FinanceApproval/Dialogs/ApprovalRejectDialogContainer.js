// @flow
import { connect } from 'react-redux';
import msg from '../../../../commons/languages';
import { actions as commentActions } from '../../../../expenses-pc/modules/ui/expenses/dialog/approval/comment';
import { MAX_SEARCH_RESULT_NUM } from '../../../modules/ui/FinanceApproval/RequestList/page';
import { reject as rejectFA } from '../../../action-dispatchers/FinanceApproval';
import ApprovalReject from '../../../../commons/components/exp/Form/Dialog/Approval';

const mapStateToProps = (state) => ({
  title: msg().Exp_Lbl_Reject,
  mainButtonTitle: msg().Exp_Lbl_Reject,
  requestList: state.entities.requestList,
  requestIdList: state.entities.requestIdList,
  comment: state.ui.expenses.dialog.approval.comment,
});

const mapDispatchToProps = {
  rejectFA,
  onChangeComment: commentActions.set,
  resetComment: commentActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickMainButton: () => {
    dispatchProps.resetComment();
    const { requestList, requestIdList, comment } = stateProps;
    const requestId = ownProps.expReport.requestId;
    const requestIds = requestIdList && requestIdList.requestIdList;
    const index = requestIds.indexOf(requestId);
    const totalRequests = requestIdList.totalSize;
    const maxIndex =
      totalRequests < MAX_SEARCH_RESULT_NUM
        ? totalRequests - 1
        : MAX_SEARCH_RESULT_NUM - 1;
    const nextIndex = index === maxIndex ? index : index + 1;
    dispatchProps.rejectFA(
      [requestId],
      requestIds[nextIndex],
      requestList,
      comment
    );
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ApprovalReject): React.ComponentType<*>): React.ComponentType<Object>);
