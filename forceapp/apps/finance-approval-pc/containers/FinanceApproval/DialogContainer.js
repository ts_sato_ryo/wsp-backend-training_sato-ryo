// @flow
import * as React from 'react';
import { connect } from 'react-redux';

import { actions as activeDialogActions } from '../../../expenses-pc/modules/ui/expenses/dialog/activeDialog';

import FADialog from '../../components/FinanceApproval/Dialog';

const mapStateToProps = (state) => ({
  activeDialog: state.ui.expenses.dialog.activeDialog,
});

const mapDispatchToProps = {
  hideDialog: activeDialogActions.hide,
  clearDialog: activeDialogActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickHideDialogButton: () => {
    dispatchProps.hideDialog();
    dispatchProps.clearDialog();
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(FADialog): React.ComponentType<*>): React.ComponentType<Object>);
