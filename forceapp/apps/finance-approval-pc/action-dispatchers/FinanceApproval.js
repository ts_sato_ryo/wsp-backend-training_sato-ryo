// @flow

import _ from 'lodash';

import msg from '../../commons/languages';

import {
  withLoading,
  catchApiError,
  loadingStart,
  loadingEnd,
} from '../../commons/actions/app';

import {
  type RequestIdList,
  type SearchConditions,
  type SortBy,
  type OrderBy,
  requestDateInitVal,
  // searchConditionInitValue,
} from '../../domain/models/exp/FinanceApproval';

import type { Record } from '../../domain/models/exp/Record';

import { actions as financeApprovalActions } from '../../domain/modules/exp/finance-approval';
import { actions as requestIdListActions } from '../modules/entities/requestIdList';
import { actions as requestListActions } from '../modules/entities/requestList';
import { actions as departmentListActions } from '../modules/entities/departmentList';
import { actions as employeeListActions } from '../modules/entities/employeeList';
// import { actions as expTypeListActions } from '../modules/entities/expTypeList';
import { actions as advSearchCondition } from '../modules/entities/advSearchConditionList';
import { actions as overlapActions } from '../../expenses-pc/modules/ui/expenses/overlap';
import {
  actions as pageActions,
  PAGE_SIZE,
} from '../modules/ui/FinanceApproval/RequestList/page';
import { actions as orderByActions } from '../modules/ui/FinanceApproval/RequestList/orderBy';
import { actions as sortByActions } from '../modules/ui/FinanceApproval/RequestList/sortBy';
import { actions as modeActions } from '../../expenses-pc/modules/ui/expenses/mode';
import { actions as selectedExpReportActions } from '../../expenses-pc/modules/ui/expenses/selectedExpReport';
import { actions as approvalHistoryActions } from '../../domain/modules/exp/approval/request/history';
import { actions as activeDialogActions } from '../../expenses-pc/modules/ui/expenses/dialog/activeDialog';
import { actions as selectedSearchConditionActions } from '../modules/ui/FinanceApproval/RequestList/selectedSearchCondition';

export const fetchFinanceApprovalIdList = (
  sortBy?: ?SortBy,
  orderBy?: ?OrderBy,
  advSearchConditions?: ?SearchConditions
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(requestIdListActions.list(sortBy, orderBy, advSearchConditions))
    .then((ret) => {
      if (ret.payload.requestIdList.length > 0) {
        dispatch(loadingStart());
        dispatch(
          requestListActions.list(ret.payload.requestIdList.slice(0, PAGE_SIZE))
        ).then(() => {
          dispatch(loadingEnd());
          dispatch(pageActions.set(1));
          if (sortBy && orderBy) {
            dispatch(sortByActions.set(sortBy));
            dispatch(orderByActions.set(orderBy));
          }
        });
      } else if (ret.payload.requestIdList.length === 0) {
        dispatch(requestListActions.initialize());
      }
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const fetchFinanceApprovalList = (
  requestIdList: RequestIdList,
  pageNum: number
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    requestListActions.list(
      requestIdList.slice(PAGE_SIZE * (pageNum - 1), PAGE_SIZE * pageNum)
    )
  )
    .then(() => dispatch(pageActions.set(pageNum)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const fetchExpRequest = (requestId: string, requestTypeList: any) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(financeApprovalActions.get(requestId))
    .then((result) => {
      dispatch(
        selectedExpReportActions.select(result.payload, requestTypeList)
      );
      dispatch(modeActions.reportSelect());
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

// fetch search items' lists + personal setting
export const fetchinitialSetting = (companyId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(
    selectedSearchConditionActions.set(
      msg().Exp_Lbl_SearchConditionApprovelreRuestList
    )
  );
  Promise.all([
    dispatch(
      departmentListActions.list(companyId, requestDateInitVal().startDate)
    ),
    dispatch(
      employeeListActions.list(companyId, requestDateInitVal().startDate)
    ),
    // dispatch(expTypeListActions.list()),
    dispatch(advSearchCondition.get()),
  ])
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

// save advanced search conditions
export const saveAdvSearchCondition = (condition: Array<SearchConditions>) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(advSearchCondition.save(condition))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

// delete advanced search conditions
export const deteleAdvSearchCondition = (
  condition: Array<SearchConditions>
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(activeDialogActions.hide());
  dispatch(advSearchCondition.save(condition))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => {
      dispatch(loadingEnd());
      dispatch(advSearchCondition.get());
    });
};
export const fetchFinanceApprovalHistory = (requestId: string) => (
  dispatch: Dispatch<any>
) =>
  dispatch(
    withLoading(() => dispatch(approvalHistoryActions.get(requestId)))
  ).catch((err) => dispatch(catchApiError(err, { isContinuable: true })));

export const fetchFinanceApprovalEditHistory = (requestId: string) => (
  dispatch: Dispatch<any>
) =>
  dispatch(
    withLoading(() => dispatch(financeApprovalActions.getHistory(requestId)))
  ).catch((err) => dispatch(catchApiError(err, { isContinuable: true })));

export const reject = (
  requestIds: string[],
  requestId: string,
  requestList: any,
  comment: string
) => (dispatch: Dispatch<any>) => {
  dispatch(activeDialogActions.hide());
  dispatch(loadingStart());
  dispatch(financeApprovalActions.reject(requestIds, comment))
    .then(() => dispatch(fetchExpRequest(requestId, requestList)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const approve = (
  requestIds: string[],
  requestId: string,
  requestList: any,
  comment: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(financeApprovalActions.approve(requestIds, comment))
    .then(() => dispatch(fetchExpRequest(requestId, requestList)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const saveExpReport = (
  expReport: any,
  sortBy: SortBy,
  orderBy: OrderBy
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(financeApprovalActions.save(expReport))
    .then(() => {
      dispatch(fetchExpRequest(expReport.requestId));
      dispatch(overlapActions.nonOverlapRecord());
      dispatch(fetchFinanceApprovalIdList(sortBy, orderBy, null));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const saveExpRecord = (
  recordItem: Record,
  sortBy: SortBy,
  orderBy: OrderBy,
  reportId: string,
  requestId: string,
  reportTypeId: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    financeApprovalActions.saveRecord(
      recordItem,
      reportId,
      requestId,
      reportTypeId
    )
  )
    .then(() => {
      dispatch(fetchExpRequest(requestId));
      dispatch(overlapActions.nonOverlapRecord());
      dispatch(fetchFinanceApprovalIdList(sortBy, orderBy, null));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const backFromDetailToList = (
  requestIdList: RequestIdList,
  currentRequestIdx: number
) => (dispatch: Dispatch<any>) => {
  const pageNum = Math.ceil((currentRequestIdx + 1) / PAGE_SIZE);

  dispatch(overlapActions.nonOverlapReport());
  dispatch(fetchFinanceApprovalList(requestIdList, pageNum));
};

// 申請
export const saveSearchCondition = (
  searchConditionList: Array<SearchConditions>
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(activeDialogActions.hide());
  dispatch(advSearchCondition.save(searchConditionList))
    .then(() =>
      dispatch(advSearchCondition.get())
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
        .then(() => dispatch(loadingEnd()))
    )
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      dispatch(loadingEnd());
    });
};
