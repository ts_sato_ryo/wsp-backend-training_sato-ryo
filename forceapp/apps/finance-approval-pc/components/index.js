/* @flow */
import React from 'react';

import GlobalContainer from '../../commons/containers/GlobalContainer';
import GlobalHeader from '../../commons/components/GlobalHeader';
import msg from '../../commons/languages';

import FinanceApproval from '../containers/FinanceApproval/FinanceApprovalContainer';
import iconHeaderFinanceApproval from '../../approvals-pc/images/Approval.svg';

type Props = {
  userSetting: {
    id: string,
    employeeId: string,
    useExpense: boolean,
  },
  initialize: () => void,
};

export default class App extends React.Component<Props> {
  // Don't move to container yet. There are bugs at Redux compose
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    const { userSetting } = this.props;

    if (!userSetting.id) {
      return null;
    }

    return (
      <GlobalContainer>
        <GlobalHeader
          content={<div />}
          iconAssistiveText={msg().Com_Lbl_FinanceApproval}
          iconSrc={iconHeaderFinanceApproval}
          iconSrcType="svg"
          showPersonalMenuPopoverButton={false}
          showProxyIndicator={false}
        />
        <FinanceApproval />
      </GlobalContainer>
    );
  }
}
