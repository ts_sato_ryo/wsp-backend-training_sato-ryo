// @flow
import React from 'react';
import _ from 'lodash';

import CustomDropdown from '../../../../commons/components/fields/CustomDropdown';
import FilterableCheckbox from '../../../../commons/components/fields/FilterableCheckbox';
import DropdownDateRange from '../../../../commons/components/fields/DropdownDateRange';
import DropdownAmountRange from '../../../../commons/components/fields/DropdownAmountRange';
import DropDownTextField from '../../../../commons/components/fields/DropdownTextField';

import Button from '../../../../commons/components/buttons/Button';
import msg from '../../../../commons/languages';

import { type DepartmentOption } from '../../../modules/entities/departmentList';
import { type EmployeeOption } from '../../../modules/entities/employeeList';
import {
  type SearchConditions,
  detailValue,
  // receiptOption,
  DETAILS,
  type DateRangeOption,
  // statusOptions,
  // searchConditionInitValue,
} from '../../../../domain/models/exp/FinanceApproval';

import './index.scss';

const ROOT = 'ts-finance-approval__search-area';

type Props = {
  onClickAdvSearchButton: () => void,
  onClickSaveConditionButton: () => void,
  onClickDeleteConditionButton: () => void,
  departmentOptions: Array<DepartmentOption>,
  employeeOptions: Array<EmployeeOption>,
  // expTypeOptions: Array<ExpTypeOption>,
  advSearchCondition: SearchConditions,
  onClickDetail: (string) => void,
  selectedDetail: Array<string>,
  // hasReceipt: Array<boolean>,
  // onClickHasReceipt: (hasReceipt: Array<boolean>) => void,
  onClickDeteleAmount: () => void,
  onClickDeteleAccountingDate: () => void,
  onClickDeteleReportNo: () => void,

  onClickInputValueStatus: () => void,

  onClickInputValueDepartment: () => void,

  onClickInputValueEmployee: () => void,

  onClickInputValueAccountingDate: () => void,

  onClickInputValueRequestDate: (
    dateRangeOption: DateRangeOption,
    needUpdate: boolean
  ) => void,

  onClickInputValueReportNo: () => void,

  onClickInputValueAmount: () => void,

  fetchedAdvSearchConditionList: Array<SearchConditions>,
  selectedSearchConditionName: string,
  currencyDecimalPlaces: number,
};

class SearchArea extends React.Component<Props> {
  render() {
    const selectedSearchCondition = _.cloneDeep(
      _.find(this.props.fetchedAdvSearchConditionList, {
        name: this.props.selectedSearchConditionName,
      })
    );
    _.unset(selectedSearchCondition, ['name']);

    const isDisabledSaveButton = _.isEqual(
      selectedSearchCondition,
      this.props.advSearchCondition
    );

    const isDisabledDeleteButton =
      this.props.selectedSearchConditionName ===
      msg().Exp_Lbl_SearchConditionApprovelreRuestList;

    const detailOption = [
      // { label: 'report date', value: 'reportDate' },
      {
        label: msg().Exp_Clbl_RecordDate,
        value: detailValue.ACCOUNTING_DATE,
      },
      // { label: 'has receipt', value: 'receipt' },
      // { label: 'exp type', value: 'expType' },
      {
        label: msg().Exp_Btn_SearchConditionReportNo,
        value: detailValue.REPORT_ID,
      },
      { label: msg().Exp_Clbl_Amount, value: detailValue.AMOUNT },
    ];

    const statusOptions = [
      { label: msg().Exp_Btn_StatusOptionStatusApproved, value: 'Approved' },
      {
        label: msg().Exp_Btn_StatusOptionAccountingAuthorized,
        value: 'AccountingAuthorized',
      },
      {
        label: msg().Exp_Btn_StatusOptionAccountingRejected,
        value: 'AccountingRejected',
      },
      {
        label: msg().Exp_Btn_StatusOptionJournalCreated,
        value: 'JournalCreated',
      },
      {
        label: msg().Exp_Btn_StatusOptionPaid,
        value: 'Fully Paid',
      },
    ];

    return (
      <div className={`${ROOT}`}>
        <div>
          <CustomDropdown
            valuePlaceholder={msg().Exp_Btn_SearchConditionStatus}
            selectedStringValues={
              this.props.advSearchCondition.financeStatusList
            }
            data={statusOptions}
          >
            {(values, updateParentState) => (
              <FilterableCheckbox
                data={statusOptions}
                selectedStringValues={
                  this.props.advSearchCondition.financeStatusList
                }
                values={values}
                searchPlaceholder={
                  msg().Exp_Lbl_SearchConditionPlaceholderStatus
                }
                updateParentState={updateParentState}
                onSelectInput={this.props.onClickInputValueStatus}
                hasDisplayValue
              />
            )}
          </CustomDropdown>
          <CustomDropdown
            valuePlaceholder={msg().Exp_Btn_SearchConditionEmployee}
            selectedStringValues={this.props.advSearchCondition.empBaseIds}
            data={this.props.employeeOptions}
          >
            {(values, updateParentState) => (
              <FilterableCheckbox
                data={this.props.employeeOptions}
                selectedStringValues={this.props.advSearchCondition.empBaseIds}
                values={values}
                searchPlaceholder={
                  msg().Exp_Lbl_SearchConditionPlaceholderEmployee
                }
                updateParentState={updateParentState}
                onSelectInput={this.props.onClickInputValueEmployee}
                hasDisplayValue
                maxLength={8}
              />
            )}
          </CustomDropdown>
          <CustomDropdown
            valuePlaceholder={msg().Exp_Btn_SearchConditionDepartment}
            selectedStringValues={
              this.props.advSearchCondition.departmentBaseIds
            }
            data={this.props.departmentOptions}
          >
            {() => (
              <FilterableCheckbox
                data={this.props.departmentOptions}
                selectedStringValues={
                  this.props.advSearchCondition.departmentBaseIds
                }
                searchPlaceholder={
                  msg().Exp_Lbl_SearchConditionPlaceholderDepartment
                }
                onSelectInput={this.props.onClickInputValueDepartment}
                hasDisplayValue
                maxLength={8}
              />
            )}
          </CustomDropdown>
          <CustomDropdown
            valuePlaceholder={msg().Exp_Btn_SearchConditionRequestDate}
            selectedDateRangeValues={
              this.props.advSearchCondition.requestDateRange
            }
          >
            {(values, updateParentState) => (
              <DropdownDateRange
                dateRange={this.props.advSearchCondition.requestDateRange}
                onClickUpdateDate={this.props.onClickInputValueRequestDate}
                updateParentState={updateParentState}
                values={values}
                needStartDate
              />
            )}
          </CustomDropdown>
          <CustomDropdown
            valuePlaceholder={msg().Exp_Btn_SearchConditionDetail}
          >
            {(values, updateParentState) => (
              <FilterableCheckbox
                data={detailOption}
                selectedStringValues={this.props.advSearchCondition.detail}
                values={values}
                searchPlaceholder={
                  msg().Exp_Lbl_SearchConditionPlaceholderDetail
                }
                onSelectInput={this.props.onClickDetail}
                updateParentState={updateParentState}
              />
            )}
          </CustomDropdown>
          <Button
            onClick={this.props.onClickAdvSearchButton}
            className={`${ROOT}-search-button`}
          >
            {msg().Exp_Btn_Search}
          </Button>
          <Button
            onClick={this.props.onClickSaveConditionButton}
            className={`${ROOT}-condition-save-button`}
            disabled={isDisabledSaveButton}
          >
            {msg().Exp_Btn_SaveSearchCondition}
          </Button>
          <Button
            onClick={this.props.onClickDeleteConditionButton}
            className={`${ROOT}-condition-delete-button`}
            disabled={isDisabledDeleteButton}
          >
            {msg().Exp_Btn_DeleteSearchCondition}
          </Button>
        </div>
        <div>
          {_.includes(this.props.selectedDetail, DETAILS.accountingDate) && (
            <CustomDropdown
              valuePlaceholder={msg().Exp_Clbl_RecordDate}
              onClickDeteleButton={this.props.onClickDeteleAccountingDate}
              selectedDateRangeValues={
                this.props.advSearchCondition.accountingDateRange
              }
              isClearable
            >
              {(updateParentState) => (
                <DropdownDateRange
                  dateRange={this.props.advSearchCondition.accountingDateRange}
                  onClickUpdateDate={this.props.onClickInputValueAccountingDate}
                  updateParentState={updateParentState}
                />
              )}
            </CustomDropdown>
          )}
          {/* {_.includes(this.props.selectedDetail, DETAILS.receipt) && (
            <CustomDropdown valuePlaceholder="receipt">
              {() => (
                <FilterableCheckbox
                  data={receiptOption}
                  values={this.props.hasReceipt ? ['hasReceipt'] : []}
                  searchPlaceholder="receipt"
                  updateParentState={this.props.onClickHasReceipt}
                />
              )}
            </CustomDropdown>
          )} */}
          {/* {_.includes(this.props.selectedDetail, DETAILS.expType) && (
            <CustomDropdown valuePlaceholder="expenseType">
              {() => (
                <FilterableCheckbox
                  data={this.props.expTypeOptions}
                  values={this.props.advSearchCondition.expTypeIds}
                  searchPlaceholder="expenseType"
                  updateParentState={this.props.onClickInputValueExpType}
                />
              )}
            </CustomDropdown>
          )} */}
          {_.includes(this.props.selectedDetail, DETAILS.reportNo) && (
            <CustomDropdown
              valuePlaceholder={msg().Exp_Btn_SearchConditionReportNo}
              isClearable
              onClickDeteleButton={this.props.onClickDeteleReportNo}
              selectedStringValue={this.props.advSearchCondition.reportNo}
            >
              {() => (
                <DropDownTextField
                  selectedStringValues={this.props.advSearchCondition.reportNo}
                  onClickUpdateText={this.props.onClickInputValueReportNo}
                />
              )}
            </CustomDropdown>
          )}
          {_.includes(this.props.selectedDetail, DETAILS.amount) && (
            <CustomDropdown
              valuePlaceholder={msg().Exp_Clbl_Amount}
              displayColor
              isClearable
              onClickDeteleButton={this.props.onClickDeteleAmount}
              selectedAmountRangeValues={
                this.props.advSearchCondition.amountRange
              }
            >
              {(toggleDropdown) => (
                <DropdownAmountRange
                  amountRange={this.props.advSearchCondition.amountRange}
                  onClickUpdateAmount={this.props.onClickInputValueAmount}
                  currencyDecimalPlaces={this.props.currencyDecimalPlaces}
                  toggleDropdown={toggleDropdown}
                />
              )}
            </CustomDropdown>
          )}
        </div>
      </div>
    );
  }
}
export default SearchArea;
