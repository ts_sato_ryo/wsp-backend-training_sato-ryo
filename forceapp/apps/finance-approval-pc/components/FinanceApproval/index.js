// @flow
import React from 'react';

// re-use expense components
import {
  ReportSummaryContainer,
  RecordListContainer,
  RecordItemContainer,
  BaseCurrencyContainer,
  ForeignCurrencyContainer,
  RouteFormContainer,
  SuggestContainer,
} from '../../../expenses-pc/containers/Expenses';

import Overlap from '../../../commons/components/Overlap';
import DialogContainer from '../../containers/FinanceApproval/DialogContainer';
import FormContainer from '../../containers/FinanceApproval/FormContainer';
import PagerInfo, {
  type Props as PagerInfoProps,
} from '../../../commons/components/PagerInfo';
import RequestListContainer from '../../containers/FinanceApproval/RequestListContainer';

import Icon from '../../../mobile-app/components/atoms/Icon';
import { PAGE_SIZE } from '../../modules/ui/FinanceApproval/RequestList/page';
import { modes } from '../../../expenses-pc/modules/ui/expenses/mode';

import SearchArea from './SearchArea';

import SelectField from '../../../commons/components/fields/SelectField';

import {
  type SearchConditions,
  type DateRangeOption,
  type AmountRangeOption,
} from '../../../domain/models/exp/FinanceApproval';

import { type DepartmentOption } from '../../modules/entities/departmentList';
import { type EmployeeOption } from '../../modules/entities/employeeList';
import { type ExpTypeOption } from '../../modules/entities/expTypeList';
import SubHeaderPager, {
  type Props as subHeaderPageProps,
} from './SubHeaderPager';

import './index.scss';

const ROOT = 'ts-finance-approval';

type State = {
  values: [],
};

type Props = {
  mode: string,
  overlap: { report: boolean, record: boolean },
  labelObject: () => any,
  // event handlers
  onClickRefreshButton: () => void,
  requestTotalNum: number,
  onSelectDepartment: () => void,
  onSelectEmployee: () => void,
  onClickAdvSearchButton: () => void,
  onClickSaveConditionButton: () => void,
  onClickDeleteConditionButton: () => void,
  advSearchCondition: SearchConditions,
  departmentOptions: Array<DepartmentOption>,
  employeeOptions: Array<EmployeeOption>,
  expTypeOptions: Array<ExpTypeOption>,
  fetchedAdvSearchConditionList: Array<SearchConditions>,
  onClickCondititon: (SyntheticInputEvent<HTMLInputElement>) => void,
  onClickInputValueEmployee: (string) => void,
  onClickInputValueDepartment: (string) => void,
  onClickInputValueStatus: (string) => void,
  selectedSearchConditionName: string,
  onClickDetail: (string) => void,
  selectedDetail: Array<string>,
  onClickUpdateRequestDate: (dateRangeOption: DateRangeOption) => void,
  selectedRequestDate: DateRangeOption,
  hasReceipt: Array<boolean>,
  onClickHasReceipt: (hasReceipt: Array<boolean>) => void,
  amount: AmountRangeOption,
  onClickUpdateAmount: (amountRangeOption: AmountRangeOption) => void,
  onClickUpdateReportNo: any,
  onClickDeteleAmount: () => void,
  onClickDeteleReportNo: () => void,

  onClickInputValueStatus: () => void,
  resetStatusInitialValue: () => void,
  setEmployeeInitialStatus: () => void,

  onClickInputValueDepartment: () => void,
  resetDepartmentInitialValue: () => void,
  setDepartmentInitialValue: () => void,

  onClickInputValueEmployee: () => void,
  resetEmployeeInitialValue: () => void,
  setEmployeeInitialValue: () => void,

  onClickInputValueAccountingDate: () => void,
  resetAccountingDateInitialValue: () => void,
  setAccountingDateInitialValue: () => void,

  onClickInputValueRequestDate: () => void,
  resetRequestDateInitialValue: () => void,
  setRequestDateInitialValue: () => void,

  onClickInputValueReportNo: () => void,
  resetReportNoInitialValue: () => void,
  setReportNoInitialValue: () => void,

  onClickInputValueAmount: () => void,
  resetAmountInitialValue: () => void,
  setAmountInitialValue: () => void,

  onClickInputValueDetail: () => void,
  resetDetailInitialValue: () => void,
  setDetailInitialValue: () => void,

  onClickDeleteAccountingDate: () => void,
  onClickDeteleAccountingDate: () => void,
  currencyDecimalPlaces: number,
  fetchFinanceApprovalIdList: () => void,
} & PagerInfoProps &
  subHeaderPageProps;

class FinanceApproval extends React.Component<Props, State> {
  component: any;

  // Don't move to container yet. There are bugs at Redux compose
  componentDidMount() {
    this.props.fetchFinanceApprovalIdList();
  }

  isMode(mode: string) {
    return this.props.mode === modes[mode];
  }

  createOptions(fetchedCondition: Array<SearchConditions>): Object[] {
    const conditionList = fetchedCondition.map((c) => {
      return { value: c.name, text: c.name };
    });
    return conditionList;
  }

  render() {
    const disabled = !this.isMode('REPORT_EDIT');

    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}-sub-header`}>
          <SelectField
            options={this.createOptions(
              this.props.fetchedAdvSearchConditionList
            )}
            onChange={this.props.onClickCondititon}
            value={this.props.selectedSearchConditionName}
          />
          <SubHeaderPager
            overlap={this.props.overlap}
            requestTotalNum={this.props.requestTotalNum}
            currentRequestIdx={this.props.currentRequestIdx}
            onClickBackButton={this.props.onClickBackButton}
            onClickNextToRequestButton={this.props.onClickNextToRequestButton}
          />
        </div>
        <div className={`${ROOT}-search-area`}>
          <SearchArea
            onClickDeleteConditionButton={
              this.props.onClickDeleteConditionButton
            }
            onClickDeteleAccountingDate={this.props.onClickDeteleAccountingDate}
            onClickSaveConditionButton={this.props.onClickSaveConditionButton}
            advSearchCondition={this.props.advSearchCondition}
            departmentOptions={this.props.departmentOptions}
            employeeOptions={this.props.employeeOptions}
            expTypeOptions={this.props.expTypeOptions}
            onSelectDepartment={this.props.onSelectDepartment}
            onSelectEmployee={this.props.onSelectEmployee}
            onClickAdvSearchButton={this.props.onClickAdvSearchButton}
            onClickDetail={this.props.onClickDetail}
            selectedDetail={this.props.selectedDetail}
            selectedRequestDate={this.props.selectedRequestDate}
            onClickUpdateRequestDate={this.props.onClickUpdateRequestDate}
            onClickDeleteAccountingDate={this.props.onClickDeleteAccountingDate}
            hasReceipt={this.props.hasReceipt}
            onClickHasReceipt={this.props.onClickHasReceipt}
            onClickUpdateAmount={this.props.onClickUpdateAmount}
            amount={this.props.amount}
            onClickUpdateReportNo={this.props.onClickUpdateReportNo}
            onClickDeteleReportNo={this.props.onClickDeteleReportNo}
            onClickDeteleAmount={this.props.onClickDeteleAmount}
            onClickInputValueStatus={this.props.onClickInputValueStatus}
            resetStatusInitialValue={this.props.resetStatusInitialValue}
            setEmployeeInitialStatus={this.props.setEmployeeInitialStatus}
            onClickInputValueDepartment={this.props.onClickInputValueDepartment}
            resetDepartmentInitialValue={this.props.resetDepartmentInitialValue}
            setDepartmentInitialValue={this.props.setDepartmentInitialValue}
            onClickInputValueEmployee={this.props.onClickInputValueEmployee}
            resetEmployeeInitialValue={this.props.resetEmployeeInitialValue}
            setEmployeeInitialValue={this.props.setEmployeeInitialValue}
            onClickInputValueAccountingDate={
              this.props.onClickInputValueAccountingDate
            }
            resetAccountingDateInitialValue={
              this.props.resetAccountingDateInitialValue
            }
            setAccountingDateInitialValue={
              this.props.setAccountingDateInitialValue
            }
            onClickInputValueRequestDate={
              this.props.onClickInputValueRequestDate
            }
            resetRequestDateInitialValue={
              this.props.resetRequestDateInitialValue
            }
            setRequestDateInitialValue={this.props.setRequestDateInitialValue}
            onClickInputValueReportNo={this.props.onClickInputValueReportNo}
            resetReportNoInitialValue={this.props.resetReportNoInitialValue}
            setReportNoInitialValue={this.props.setReportNoInitialValue}
            onClickInputValueAmount={this.props.onClickInputValueAmount}
            resetAmountInitialValue={this.props.resetAmountInitialValue}
            setAmountInitialValue={this.props.setAmountInitialValue}
            onClickInputValueDetail={this.props.onClickInputValueDetail}
            resetDetailInitialValue={this.props.resetDetailInitialValue}
            setDetailInitialValue={this.props.setDetailInitialValue}
            labelObject={this.props.labelObject}
            selectedSearchConditionName={this.props.selectedSearchConditionName}
            fetchedAdvSearchConditionList={
              this.props.fetchedAdvSearchConditionList
            }
            currencyDecimalPlaces={this.props.currencyDecimalPlaces}
          />
        </div>
        <div className={`${ROOT}__request-header`}>
          <PagerInfo
            className={`${ROOT}__page-info`}
            currentPage={this.props.currentPage}
            totalNum={this.props.requestTotalNum}
            pageSize={PAGE_SIZE}
          />
          <button
            className={`${ROOT}__refresh-btn`}
            onClick={this.props.onClickRefreshButton}
          >
            <Icon type="refresh-copy" size="small" />
          </button>
        </div>
        <RequestListContainer />
        <Overlap isVisible={this.props.overlap.report}>
          <div className={`${ROOT}__tab-area`}>
            <FormContainer
              recordList={RecordListContainer}
              recordItem={RecordItemContainer}
              reportSummary={ReportSummaryContainer}
              baseCurrency={BaseCurrencyContainer}
              foreignCurrency={ForeignCurrencyContainer}
              routeForm={RouteFormContainer}
              dialog={DialogContainer}
              suggest={SuggestContainer}
              disabled={disabled}
              mode={this.props.mode}
              labelObject={this.props.labelObject}
            />
          </div>
        </Overlap>
      </div>
    );
  }
}

export default FinanceApproval;
