// @flow
import React from 'react';

import msg from '../../../commons/languages';

import { MAX_SEARCH_RESULT_NUM } from '../../modules/ui/FinanceApproval/RequestList/page';

import TextUtil from '../../../commons/utils/TextUtil';
import Button from '../../../commons/components/buttons/Button';

import './SubHeaderPager.scss';

const ROOT = 'ts-finance-approval-sub-header-pager';

export type Props = {
  overlap: { report: boolean },
  currentRequestIdx: number,
  requestTotalNum: number,
  // event handlers
  onClickBackButton: () => void,
  onClickNextToRequestButton: (number) => void,
};

const SubHeaderPager = ({
  overlap,
  currentRequestIdx,
  requestTotalNum,
  onClickNextToRequestButton,
  onClickBackButton,
}: Props) => {
  if (!overlap.report) {
    return null;
  }

  const displayNum = TextUtil.template(
    msg().Exp_Lbl_NumberDisplay,
    currentRequestIdx + 1,
    requestTotalNum
  );

  const isVisible = currentRequestIdx !== -1;
  const isNotFirstPage = currentRequestIdx !== 0;
  const isNotLastPage =
    currentRequestIdx < requestTotalNum - 1 &&
    currentRequestIdx + 1 < MAX_SEARCH_RESULT_NUM;

  return (
    <div className={ROOT}>
      <Button
        type="text"
        className={`${ROOT}__back-btn`}
        onClick={onClickBackButton}
      >
        {msg().Com_Lbl_BackToList}
      </Button>
      {isVisible && (
        <div className={`${ROOT}__contents`}>
          {isNotFirstPage && (
            <Button
              type="text"
              className={`${ROOT}__previous-request-btn`}
              onClick={() => onClickNextToRequestButton(-1)}
            >
              &lt;
            </Button>
          )}
          <div className={`${ROOT}__current-page`}>{displayNum}</div>
          {isNotLastPage && (
            <Button
              type="text"
              className={`${ROOT}__next-request-btn`}
              onClick={() => onClickNextToRequestButton(+1)}
            >
              &gt;
            </Button>
          )}
        </div>
      )}
    </div>
  );
};

export default SubHeaderPager;
