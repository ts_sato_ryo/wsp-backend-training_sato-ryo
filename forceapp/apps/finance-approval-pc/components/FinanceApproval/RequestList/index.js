// @flow
import React from 'react';

import msg from '../../../../commons/languages';

import { type RequestList } from '../../../../domain/models/exp/FinanceApproval';
import {
  PAGE_SIZE,
  MAX_PAGE_NUM,
  MAX_SEARCH_RESULT_NUM,
} from '../../../modules/ui/FinanceApproval/RequestList/page';

import ListHeader, { type Props as ListHeaderProps } from './Header';
import ListItem from './Item';
import PagerInfo, {
  type Props as PagerInfoProps,
} from '../../../../commons/components/PagerInfo';
import Pagenation, {
  type Props as PagerProps,
} from '../../../../commons/components/Pagination';

import './index.scss';

const ROOT = 'ts-finance-approval__requests';
const ROOT_LONG = 'ts-finance-approval__requests-long';

export type CommonProps = $ReadOnly<{|
  baseCurrencySymbol: string,
  baseCurrencyDecimal: string,
  onClickRequestItem: (requestId: string) => void,
|}>;

type Props = {
  ...CommonProps,
  requestList: RequestList,
  requestTotalNum: number,
  detail: Array<string>,
} & PagerProps &
  PagerInfoProps &
  ListHeaderProps;

// export default class FinanceApprovalRequestList extends React.Component<Props> {

const FinanceApprovalRequestList = ({
  onClickSortKey,
  sortBy,
  orderBy,
  requestList,
  baseCurrencySymbol,
  baseCurrencyDecimal,
  onClickRequestItem,
  currentPage,
  requestTotalNum,
  onClickPagerLink,
  detail,
}: Props) => (
  <div className={detail && detail.length > 0 ? ROOT_LONG : ROOT}>
    <ListHeader
      onClickSortKey={onClickSortKey}
      sortBy={sortBy}
      orderBy={orderBy}
    />
    {requestList.length !== 0 && (
      <div className={`${ROOT}-list`}>
        {requestList.map((item) => (
          <ListItem
            item={item}
            key={item.requestId}
            baseCurrencySymbol={baseCurrencySymbol}
            baseCurrencyDecimal={baseCurrencyDecimal}
            onClickRequestItem={onClickRequestItem}
          />
        ))}
        {currentPage === MAX_PAGE_NUM &&
          requestTotalNum > MAX_SEARCH_RESULT_NUM && (
            <div className={`${ROOT}-too-many-results`}>
              {msg().Com_Lbl_TooManySearchResults}
            </div>
          )}
        <div className={`${ROOT}-list-footer`}>
          <PagerInfo
            className={`${ROOT}-page-info`}
            currentPage={currentPage}
            totalNum={requestTotalNum}
            pageSize={PAGE_SIZE}
          />
          <Pagenation
            className={`${ROOT}-list-pager`}
            currentPage={currentPage}
            totalNum={requestTotalNum}
            displayNum={5}
            pageSize={PAGE_SIZE}
            onClickPagerLink={(num) => onClickPagerLink(num)}
            maxPageNum={MAX_PAGE_NUM}
          />
        </div>
      </div>
    )}
  </div>
);
export default FinanceApprovalRequestList;
