// @flow
import React from 'react';
import classNames from 'classnames';

import DateUtil from '../../../../../commons/utils/DateUtil';
import FormatUtil from '../../../../../commons/utils/FormatUtil';
import MultiColumnsGrid from '../../../../../commons/components/MultiColumnsGrid';

import { type RequestItem } from '../../../../../domain/models/exp/FinanceApproval';
import { type CommonProps } from '..';

import { getStatusText } from '../../../../../domain/modules/exp/report';
import iconVoucherReceipt from '../../../../../commons/components/exp/images/iconVoucherReceipt.png';

import './index.scss';

const ROOT = 'ts-finance-approval__requests-item';

export type Props = {
  ...CommonProps,
  item: RequestItem,
};

export default class RequestListItem extends React.Component<Props> {
  render() {
    const { item } = this.props;
    const totalAmount = FormatUtil.formatNumber(
      item.totalAmount || 0,
      this.props.baseCurrencyDecimal
    );

    const hasReceipt = item.hasReceipts ? (
      <img src={iconVoucherReceipt} alt="receipt" />
    ) : null;

    const handleListItemClick = () => {
      // to be implemented after get API is completed
      this.props.onClickRequestItem(item.requestId);
    };

    const formattedLocalAmount = `${this.props.baseCurrencySymbol} ${totalAmount}`;
    // const isSelected =
    //   (this.props.selectedExpReportId &&
    //     this.props.selectedExpReportId === item.reportId) ||
    //   false;

    const listItemClassNames = classNames({
      // [`${ROOT}--active`]: isSelected,
      [`${ROOT}`]: true,
    });

    const CELL_CLASS = `${ROOT}-cell ${ROOT}`;

    return (
      <div onClick={handleListItemClick} className={listItemClassNames}>
        <MultiColumnsGrid sizeList={[1, 1, 1, 1, 2, 1, 1, 1, 1, 2]}>
          <div className={`${ROOT}-icon`}>{hasReceipt}</div>

          <div className={`${CELL_CLASS}-reportId`}>{item.reportNo}</div>

          <div className={`${ROOT}-status`}>
            <div className={`${CELL_CLASS}-status-text`}>
              {getStatusText(item.status)}
            </div>
          </div>

          <div className={`${CELL_CLASS}-date`}>
            {DateUtil.format(item.requestDate)}
          </div>

          <div className={`${ROOT}-main`}>
            <div className={`${ROOT}-text`}>
              <div className={`${CELL_CLASS}-text__subject`}>
                {item.subject}
              </div>
            </div>
          </div>

          <div className={`${CELL_CLASS}-amount`}>{formattedLocalAmount}</div>

          <div className={`${CELL_CLASS}-emp-code`}>{item.employeeCode}</div>

          <div className={`${CELL_CLASS}-emp-name`}>{item.employeeName}</div>

          <div className={`${CELL_CLASS}-dep-code`}>{item.departmentCode}</div>

          <div className={`${CELL_CLASS}-dep-name`}>{item.departmentName}</div>
        </MultiColumnsGrid>
      </div>
    );
  }
}
