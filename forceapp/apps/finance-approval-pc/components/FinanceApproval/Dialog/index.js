// @flow
import React from 'react';
import last from 'lodash/last';

import { dialogTypes } from '../../../../expenses-pc/modules/ui/expenses/dialog/activeDialog';
import { getSelectedExpDialogComponent } from '../../../../expenses-pc/components/Expenses/Dialog';

import SearchCondition from '../../../containers/FinanceApproval/Dialogs/SearchConditionDialogContainer';
import DeleteSearchCondition from '../../../containers/FinanceApproval/Dialogs/DeleteSearchConditionContainer';
import ApprovalReject from '../../../containers/FinanceApproval/Dialogs/ApprovalRejectDialogContainer';

type Props = {
  activeDialog: Array<string>,
  hideDialog: () => void,
  clearDialog: () => void,
  onClickHideDialogButton: () => void,
};

const getSelectedFADialogComponent = (currentDialog: string, props: Props) => {
  switch (currentDialog) {
    case dialogTypes.REJECT_FINANCE_APPROVAL:
      return <ApprovalReject {...props} />;
    case dialogTypes.DELETE_SEARCH_CONDITION:
      return <DeleteSearchCondition {...props} />;
    case dialogTypes.SEARCH_CONDITION:
      return <SearchCondition {...props} />;
    default:
      return null;
  }
};

const FADialog = (props: Props) => {
  const currentDialog = last(props.activeDialog);
  const expDialog = getSelectedExpDialogComponent(currentDialog, props);
  return expDialog || getSelectedFADialogComponent(currentDialog, props);
};

export default FADialog;
