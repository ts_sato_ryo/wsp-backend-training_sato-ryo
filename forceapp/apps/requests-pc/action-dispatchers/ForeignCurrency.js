// @flow
import { actions as currencyActions } from '../modules/ui/expenses/recordItemPane/foreignCurrency/currency';
import { actions as exchangeRateActions } from '../modules/ui/expenses/recordItemPane/foreignCurrency/exchangeRate';

export const getRateFromId = (
  companyId: string,
  currencyId: string,
  recordDate?: string
): any => exchangeRateActions.search(companyId, currencyId, recordDate);

export const searchCurrencyList = (companyId: string) =>
  currencyActions.search(companyId);
