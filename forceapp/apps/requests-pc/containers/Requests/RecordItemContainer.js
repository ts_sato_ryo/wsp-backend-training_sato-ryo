// @flow
import { cloneDeep, get, isEqual, assign, isEmpty } from 'lodash';
import { connect } from 'react-redux';
import FileUtil from '../../../commons/utils/FileUtil';
import DateUtil from '../../../commons/utils/DateUtil';
import msg from '../../../commons/languages';

import {
  newRouteInfo,
  isRecordItemized,
} from '../../../domain/models/exp/Record';

import { mapStateToProps as mapExpenseStateToProps } from '../../../expenses-pc/containers/Expenses/RecordItemContainer';
import { confirm } from '../../../commons/actions/app';
import { actions as overlapActions } from '../../modules/ui/expenses/overlap';
import { actions as modeActions, modes } from '../../modules/ui/expenses/mode';
import {
  openRecordItemsCreateDialog,
  openRecordItemsConfirmDialog,
  openRecordItemsDeleteDialog,
  searchTaxTypeList,
  searchExpTypesByParentRecord,
  openReceiptLibraryDialog,
  getBase64File,
  uploadReceipts,
  getFilePreview,
  openEILookupDialog,
  openCostCenterDialog,
  openJobDialog,
} from '../../action-dispatchers/Requests';
import { getRateFromId } from '../../action-dispatchers/ForeignCurrency';
import { calcAmountFromRate } from '../../../domain/models/exp/foreign-currency/Currency';
import {
  type EISearchObj,
  getEIsOnly,
} from '../../../domain/models/exp/ExtendedItem';
import { resetRouteForm } from '../../action-dispatchers/Route';

import {
  type ExpTaxTypeListApiReturn,
  calculateTax,
} from '../../../domain/models/exp/TaxType';

import RecordItemView from '../../../commons/components/exp/Form/RecordItem';

const mapStateToProps = (state, ownProps) => {
  const expenseStateToProps = mapExpenseStateToProps(state, ownProps);

  return {
    ...expenseStateToProps,
    isExpenseRequest: true,
  };
};

const mapDispatchToProps = {
  confirm,
  getFilePreview,
  hideRecord: overlapActions.nonOverlapRecord,
  openEILookupDialog,
  getRateFromId,
  openReceiptLibraryDialog,
  resetRouteForm,
  reportSelectMode: modeActions.reportSelect,
  searchTaxTypeList,
  searchExpTypesByParentRecord,
  getBase64File,
  uploadReceipts,
  openJobDialog,
  openCostCenterDialog,
  onClickRecordItemsCreateButton: () => openRecordItemsCreateDialog(),
  onClickRecordItemsConfirmButton: () => openRecordItemsConfirmDialog(),
  onClickRecordItemsDeleteButton: () => openRecordItemsDeleteDialog(),
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickOpenLibraryButton: () => {
    dispatchProps.openReceiptLibraryDialog();
  },
  onClickResetRouteInfoButton: () => {
    const updateObj = {
      routeInfo: cloneDeep(newRouteInfo),
      'items[0].amount': 0,
    };
    stateProps.updateRecord(updateObj, true);
    dispatchProps.resetRouteForm(null, false);
  },
  onClickHideRecordButton: () => {
    const { expReport, recordIdx, selectedRecord } = ownProps;
    const isNewRecord = !get(expReport, `records[${recordIdx}].recordId`);
    const isReportEditMode = stateProps.mode === modes.REPORT_EDIT;
    const tmpRecord = expReport.records[recordIdx];

    const isRecordEdited = !isEqual(tmpRecord, selectedRecord);

    if (isReportEditMode && isRecordEdited) {
      // if confirm discard changes
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.hideRecord();

          if (isNewRecord) {
            // delete record from redux store if it's new record
            stateProps.deleteRecord();
          } else {
            // restore from tmpRecord
            stateProps.restoreOldRecord();
          }
          dispatchProps.reportSelectMode();
        }
      });
    } else {
      dispatchProps.hideRecord();
      dispatchProps.reportSelectMode();
      // unselect the recordItem
      ownProps.onChangeEditingExpReport(`ui.recordIdx`, -1);
      ownProps.onChangeEditingExpReport(`ui.tempSavedRecordItems`, null);
    }
  },
  onChangeRecordDate: (recordDate) => {
    if (
      ownProps.expReport.records[ownProps.recordIdx].recordDate !== recordDate
    ) {
      const target = ownProps.expReport.records[ownProps.recordIdx];
      const isItemized = isRecordItemized(target.recordType, false);
      if (isItemized) {
        stateProps.updateRecord({ recordDate }, true);
      } else if (target.items[0].useForeignCurrency) {
        let exchangeRateManual = recordDate === '';
        const today = DateUtil.getToday();
        const rateDate = recordDate || today;
        const rateInfo = get(
          stateProps.exchangeRateMap,
          `${target.items[0].currencyId}.${rateDate}`
        );

        let getExchangeRate;
        if (rateInfo) {
          getExchangeRate = new Promise((resolve) =>
            resolve(rateInfo.calculationRate)
          );
        } else {
          getExchangeRate = dispatchProps
            .getRateFromId(
              stateProps.companyId,
              target.items[0].currencyId,
              rateDate
            )
            .then((exchangeRate) => {
              if (exchangeRate > 0) {
                return exchangeRate;
              } else if (rateDate === today) {
                exchangeRateManual = true;
                return exchangeRate;
              } else {
                // if no rate on recordDate, get rate on today
                return dispatchProps
                  .getRateFromId(
                    stateProps.companyId,
                    target.items[0].currencyId,
                    today
                  )
                  .then((todayRate) => {
                    exchangeRateManual = true;
                    return todayRate;
                  });
              }
            });
        }

        getExchangeRate.then((exchangeRate) => {
          const originalExchangeRate = exchangeRateManual ? 0 : exchangeRate;
          const amount = calcAmountFromRate(
            exchangeRate,
            target.items[0].localAmount,
            stateProps.baseCurrencyDecimal
          );

          stateProps.updateRecord({
            recordDate,
            amount,
            'items.0.amount': amount,
            'items.0.recordDate': recordDate,
            'items.0.exchangeRate': exchangeRate,
            'items.0.originalExchangeRate': originalExchangeRate,
            'items.0.exchangeRateManual': exchangeRateManual,
          });
        });
      } else {
        // when base currency
        const expTypeId =
          ownProps.expReport.records[ownProps.recordIdx].items[0].expTypeId;

        dispatchProps
          .searchTaxTypeList(expTypeId, recordDate)
          .then((result: ExpTaxTypeListApiReturn) => {
            const initTax = result.payload[expTypeId][recordDate][0];

            const amount =
              ownProps.expReport.records[ownProps.recordIdx].items[0].amount;
            const rate = initTax ? initTax.rate : 0;
            const baseId = 'noIdSelected';
            const historyId = initTax ? initTax.historyId : null;
            const name = initTax ? initTax.name : null;

            const taxRes = calculateTax(
              rate,
              amount || 0,
              stateProps.baseCurrencyDecimal
            );
            const updateObj = {
              recordDate,
              amount: amount || 0,
              withoutTax: taxRes.amountWithoutTax,
              'items.0.amount': amount || 0,
              'items.0.recordDate': recordDate,
              'items.0.withoutTax': taxRes.amountWithoutTax,
              'items.0.gstVat': taxRes.gstVat,
              'items.0.taxTypeBaseId': baseId,
              'items.0.taxTypeHistoryId': historyId,
              'items.0.taxTypeName': name,
            };
            stateProps.updateRecord(updateObj, true);
          });
      }
    }
  },
  onChangeChildDateOrTypeForBC: (selectedValues) => {
    const targetRecordItem =
      ownProps.expReport.records[ownProps.recordIdx].items[
        ownProps.recordItemIdx
      ];
    const recordDate = selectedValues.recordDate;
    const expTypeId = selectedValues.expTypeId;
    const isRecordDateChange = targetRecordItem.recordDate !== recordDate;
    const isExpTypeChange = targetRecordItem.expTypeId !== expTypeId;
    const targetPath = `items.${ownProps.recordItemIdx}`;
    const updateObj = {};

    // if change expense type, need to update EIs
    if (expTypeId && isExpTypeChange) {
      const expTypeInfo = ownProps.expTypeList.find(
        (expType) => expType.id === expTypeId
      );
      const EIs = getEIsOnly(expTypeInfo);
      Object.keys(EIs).forEach((key) => {
        updateObj[`${targetPath}.${key}`] = EIs[key];
      });
      updateObj[`${targetPath}.expTypeName`] = expTypeInfo.name;
    }

    // if input empty recordDate or expType, only update value
    if (!recordDate || !expTypeId) {
      assign(updateObj, {
        [`${targetPath}.recordDate`]: recordDate,
        [`${targetPath}.expTypeId`]: expTypeId,
      });
      stateProps.updateRecord(updateObj);
    }
    // else when recordDate or expType change, update tax info
    else if (isRecordDateChange || isExpTypeChange) {
      const taxTypeList = get(stateProps.tax, `${expTypeId}.${recordDate}`);
      const amount = targetRecordItem.amount;

      let getTaxInfo;
      if (taxTypeList) {
        const initTax = taxTypeList[0];
        getTaxInfo = new Promise((resolve) => resolve(initTax));
      } else {
        getTaxInfo = dispatchProps
          .searchTaxTypeList(expTypeId, recordDate)
          .then((result: ExpTaxTypeListApiReturn) => {
            const initTax = result.payload[expTypeId][recordDate][0];
            return initTax;
          });
      }
      getTaxInfo.then((initTax) => {
        const rate = initTax ? initTax.rate : 0;
        const baseId = 'noIdSelected';
        const historyId = initTax ? initTax.historyId : null;
        const name = initTax ? initTax.name : null;

        const taxRes = calculateTax(
          rate,
          amount || 0,
          stateProps.baseCurrencyDecimal
        );
        assign(updateObj, {
          [`${targetPath}.recordDate`]: recordDate,
          [`${targetPath}.withoutTax`]: taxRes.amountWithoutTax,
          [`${targetPath}.gstVat`]: taxRes.gstVat,
          [`${targetPath}.taxTypeBaseId`]: baseId,
          [`${targetPath}.taxTypeHistoryId`]: historyId,
          [`${targetPath}.taxTypeName`]: name,
          [`${targetPath}.taxRate`]: rate,
          [`${targetPath}.expTypeId`]: expTypeId,
        });
        stateProps.updateRecord(updateObj, true);
      });
    }
  },
  onChangeChildExpTypeForFC: (expTypeId) => {
    const { expReport, recordIdx, recordItemIdx, expTypeList } = ownProps;
    const targetRecordItem = expReport.records[recordIdx].items[recordItemIdx];
    const isExpTypeChange = targetRecordItem.expTypeId !== expTypeId;
    // if change expense type, need to update EIs
    if (expTypeId && isExpTypeChange) {
      const updateObj = {};
      const targetPath = `items.${recordItemIdx}`;
      const expTypeInfo = expTypeList.find(
        (expType) => expType.id === expTypeId
      );
      const EIs = getEIsOnly(expTypeInfo);
      Object.keys(EIs).forEach((key) => {
        updateObj[`${targetPath}.${key}`] = EIs[key];
      });
      updateObj[`${targetPath}.expTypeName`] = expTypeInfo.name;
      updateObj[`${targetPath}.expTypeId`] = expTypeId;
      stateProps.updateRecord(updateObj);
    }
  },
  // when change date for child item which use Foreign currency
  onChangeChildDateForFC: (recordDate) => {
    const { expReport, recordIdx, recordItemIdx } = ownProps;
    const targetItem = expReport.records[recordIdx].items[recordItemIdx];
    if (targetItem.recordDate === recordDate) {
      return;
    }

    let exchangeRateManual = recordDate === '';
    const today = DateUtil.getToday();
    const rateDate = recordDate || today;
    const rateInfo = get(
      stateProps.exchangeRateMap,
      `${targetItem.currencyId}.${rateDate}`
    );
    let getExchangeRate;

    if (rateInfo) {
      getExchangeRate = new Promise((resolve) =>
        resolve(rateInfo.calculationRate)
      );
    } else {
      getExchangeRate = dispatchProps
        .getRateFromId(stateProps.companyId, targetItem.currencyId, rateDate)
        .then((exchangeRate) => {
          if (exchangeRate > 0) {
            return exchangeRate;
          } else if (rateDate === today) {
            exchangeRateManual = true;
            return exchangeRate;
          } else {
            // if no rate on recordDate, get rate on today
            return dispatchProps
              .getRateFromId(stateProps.companyId, targetItem.currencyId, today)
              .then((todayRate) => {
                exchangeRateManual = true;
                return todayRate;
              });
          }
        });
    }

    const path = `items.${recordItemIdx}`;
    getExchangeRate.then((exchangeRate) => {
      const originalExchangeRate = exchangeRateManual ? 0 : exchangeRate;
      const amount = calcAmountFromRate(
        exchangeRate,
        targetItem.localAmount,
        stateProps.baseCurrencyDecimal
      );

      stateProps.updateRecord({
        [`${path}.recordDate`]: recordDate,
        [`${path}.amount`]: amount,
        [`${path}.exchangeRate`]: exchangeRate,
        [`${path}.originalExchangeRate`]: originalExchangeRate,
        [`${path}.exchangeRateManual`]: exchangeRateManual,
      });
    });
  },
  onImageDrop: (file: File) =>
    dispatchProps.getBase64File(file).then((base64File) => {
      dispatchProps.uploadReceipts([base64File]).then((res) => {
        if (res) {
          stateProps.updateRecord({
            receiptId: res.contentDocumentId,
            receiptFileId: res.contentVersionId,
            receiptData: base64File.data,
            fileName: FileUtil.getFileNameWithoutExtension(base64File.name),
            dataType: base64File.type,
          });
        }
      });
    }),
  getFilePreview: (receiptFileId: string) => {
    dispatchProps.getFilePreview(receiptFileId).then((res) => {
      const dataType = FileUtil.getMIMEType(res.payload.fileType);

      return stateProps.updateRecord(
        {
          receiptData: `data:${dataType};base64,${res.payload.fileBody}`,
          fileName: FileUtil.getOriginalFileNameWithoutPrefix(
            res.payload.title
          ),
          dataType,
        },
        false
      );
    });
  },
  onClickJobBtn: (recordDate) => {
    dispatchProps.openJobDialog(recordDate, stateProps.employeeId);
  },
  onClickCostCenterBtn: (recordDate) => {
    dispatchProps.openCostCenterDialog(recordDate, stateProps.employeeId);
  },
  onClickLookupEISearch: (item: EISearchObj) =>
    dispatchProps.openEILookupDialog(item, stateProps.employeeId),
  onChangeAmountSelection: (amountId: string) => {
    const { expReport, recordIdx } = ownProps;
    const record = expReport.records[recordIdx];
    const expTypeId = get(record, 'items[0].expTypeId');
    const useFixedForeignCurrency = get(
      record,
      'items[0].useFixedForeignCurrency'
    );
    const amountOption = get(stateProps.fixedAmountOptionList, `${expTypeId}`);
    const selectedAmount = amountOption.find((item) => item.id === amountId);
    const fixedAmount = isEmpty(selectedAmount)
      ? 0
      : selectedAmount.allowanceAmount;
    const updateObj = {};
    const targetPath = 'items.0';
    updateObj[`${targetPath}.fixedAllowanceOptionId`] = amountId;

    if (useFixedForeignCurrency) {
      updateObj[`${targetPath}.localAmount`] = fixedAmount;
    } else {
      updateObj.amount = fixedAmount;
      updateObj[`${targetPath}.amount`] = fixedAmount;
    }
    stateProps.updateRecord(updateObj);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordItemView): React.ComponentType<Object>);
