// @flow
import { connect } from 'react-redux';

import { confirm } from '../../../commons/actions/app';
import { actions as overlapActions } from '../../modules/ui/expenses/overlap';
import {
  fetchExpReport,
  // pagination
  fetchExpReportList,
  fetchExpReportIdList,
} from '../../action-dispatchers/Requests';

// Pagination
import {
  PAGE_SIZE,
  MAX_PAGE_NUM,
  MAX_SEARCH_RESULT_NUM,
} from '../../modules/ui/expenses/reportList/page';
import { actions as selectedExpReportActions } from '../../modules/ui/expenses/selectedExpReport';

import ReportListView from '../../../commons/components/exp/ReportList';

const mapStateToProps = (state, ownProps) => ({
  // pagination props
  currentPage: state.ui.expenses.reportList.page,
  requestTotalNum: state.entities.reportIdList.totalSize,
  orderBy: state.ui.expenses.reportList.orderBy,
  sortBy: state.ui.expenses.reportList.sortBy,
  pageSize: PAGE_SIZE,
  maxPageNo: MAX_PAGE_NUM,
  maxSearchNo: MAX_SEARCH_RESULT_NUM,
  reportIdList: state.entities.reportIdList.reportIdList,
  // end of pagination props
  expReportList: state.entities.exp.preRequest.expReportList,
  mode: state.ui.expenses.mode,
  selectedExpReport: state.ui.expenses.selectedExpReport,
  baseCurrencySymbol: state.userSetting.currencySymbol,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  ...ownProps,
});

const mapDispatchToProps = {
  confirm,
  fetchExpReport,
  fetchExpReportList,
  fetchExpReportIdList,
  clearSelectedReport: selectedExpReportActions.clear,
  moveToExpensesForm: overlapActions.overlapReport,
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickRefreshButton: (isApproved: boolean) => {
    dispatchProps.fetchExpReportIdList(isApproved);
    dispatchProps.clearSelectedReport();
  },
  onClickPagerLink: (pageNum: number) => {
    dispatchProps.fetchExpReportList(stateProps.reportIdList, pageNum);
  },
  onClickReportItem: (status, reportNo) => {
    dispatchProps.fetchExpReport(reportNo, stateProps.reportTypeList);
    dispatchProps.moveToExpensesForm();
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ReportListView): React.ComponentType<Object>);
