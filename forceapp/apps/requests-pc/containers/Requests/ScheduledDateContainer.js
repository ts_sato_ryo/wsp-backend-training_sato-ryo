// @flow
import { connect } from 'react-redux';

import ScheduledDate from '../../../commons/components/exp/Form/ReportSummary/Form/DateSelector/ScheduledDate';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
});

const mapDispatchToProps = {};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onChangeScheduledDate: (date: string) => {
    ownProps.onChangeEditingExpReport('report.scheduledDate', date, true);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ScheduledDate): React.ComponentType<Object>);
