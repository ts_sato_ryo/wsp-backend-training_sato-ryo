// @flow
import { connect } from 'react-redux';
import { withFormik } from 'formik';
import { cloneDeep, get } from 'lodash';

import msg from '../../../commons/languages';

import schema from '../../schema/ExpensesRequest';

import { confirm } from '../../../commons/actions/app';
import { actions as overlapActions } from '../../modules/ui/expenses/overlap';
import { actions as modeActions, modes } from '../../modules/ui/expenses/mode';
import { actions as activeDialogActions } from '../../modules/ui/expenses/dialog/activeDialog';
import { actions as commentActions } from '../../modules/ui/expenses/dialog/approval/comment';

import {
  saveExpReport,
  saveExpRecord,
  deleteExpReport,
  openApprovalHistoryDialog,
} from '../../action-dispatchers/Requests';

import RequestFormView from '../../../commons/components/exp/Form';

const mapStateToProps = (state) => ({
  overlap: state.ui.expenses.overlap,
  mode: state.ui.expenses.mode,
  selectedExpReport: state.ui.expenses.selectedExpReport,
  taxTypeListForSaving: state.ui.expenses.recordItemPane.tax,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  isExpense: false,
  isRequest: true,
  availableExpType: state.entities.exp.expenseType.availableExpType,
  expReportList: state.entities.exp.preRequest.expReportList,
  reportIdList: state.entities.reportIdList.reportIdList,
});

const mapDispatchToProps = {
  clearComments: commentActions.clear,
  confirm,
  moveBackToReport: overlapActions.nonOverlapReport,
  onClickDeleteButton: deleteExpReport,
  onClickSubmitButton: activeDialogActions.approval,
  openApprovalHistoryDialog,
  openCancelDialog: activeDialogActions.cancelRequest,
  reportEdit: modeActions.reportEdit,
  saveExpReport,
  saveExpRecord,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickDeleteButton: () => {
    dispatchProps.confirm(msg().Att_Msg_DailyReqConfirmRemove, (yes) => {
      if (yes) {
        dispatchProps.onClickDeleteButton(
          stateProps.selectedExpReport.reportId
        );
      }
    });
  },
  onClickCancelRequestButton: () => {
    dispatchProps.clearComments();
    dispatchProps.openCancelDialog();
  },
  onClickApprovalHistoryButton: () =>
    dispatchProps.openApprovalHistoryDialog(
      stateProps.selectedExpReport.requestId
    ),
  onClickBackButton: () => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.moveBackToReport();
        }
      });
    } else {
      dispatchProps.moveBackToReport();
    }
  },
  onClickSaveButton: (expReport, reportTypeList, defaultTaxType) => {
    // if cost center is unsed, remove the default cost center
    const isCostCenterUsed = reportTypeList.filter(
      (rt) => rt.id === expReport.expReportTypeId
    )[0].isCostCenterRequired;
    if (isCostCenterUsed === 'UNUSED') {
      expReport.costCenterName = null;
      expReport.costCenterCode = '';
      expReport.costCenterHistoryId = null;
    }
    expReport.attachedFileData = null;
    expReport.records.forEach((record) => {
      record.receiptData = null;
      if (record.items[0].taxTypeBaseId === 'noIdSelected') {
        const selectedTaxType =
          defaultTaxType[record.items[0].expTypeId][record.recordDate][0];
        record.items[0].taxTypeBaseId = selectedTaxType.baseId;
        record.items[0].taxTypeHistoryId = selectedTaxType.historyId;
        record.items[0].taxTypeName = selectedTaxType.name;
      }
    });
    dispatchProps.saveExpReport(
      expReport,
      reportTypeList,
      stateProps.expReportList,
      stateProps.reportIdList
    );
  },
  saveRecord: (
    selectedRecord,
    reportTypeList,
    defaultTaxType,
    reportId,
    reportTypeId
  ) => {
    const record = cloneDeep(selectedRecord);
    record.receiptData = null;
    if (record.items[0].taxTypeBaseId === 'noIdSelected') {
      const selectedTaxType = get(
        defaultTaxType,
        `${record.items[0].expTypeId}.${record.recordDate}.0`,
        {}
      );
      record.items[0].taxTypeBaseId = selectedTaxType.baseId;
      record.items[0].taxTypeHistoryId = selectedTaxType.historyId;
      record.items[0].taxTypeName = selectedTaxType.name;
    }

    dispatchProps.saveExpRecord(record, reportTypeList, reportId, reportTypeId);
  },
});

const expensesRequestForm = withFormik({
  // permission for change by props update (when initialised by reducer)
  enableReinitialize: true,
  mapPropsToValues: (props) => ({
    ui: {
      checkboxes: [],
      recordIdx: -1,
      recalc: false,
      saveMode: false,
      isRecordSave: false,
      submitMode: false,
    },
    report: props.selectedExpReport,
  }),
  validationSchema: schema,
  handleSubmit: (values, { props, setFieldValue }) => {
    const { ui, report } = values;
    const { saveMode, isRecordSave, recordIdx } = ui;

    if (saveMode) {
      if (isRecordSave) {
        const currentRecord = report.records[recordIdx];

        // save single Record
        props.saveRecord(
          currentRecord,
          props.reportTypeList,
          props.taxTypeListForSaving,
          report.reportId,
          report.expReportTypeId
        );
      } else {
        // save whole report without records
        props.onClickSaveButton(
          values.report,
          props.reportTypeList,
          props.taxTypeListForSaving
        );
      }
    } else {
      props.onClickSubmitButton();
    }
    setFieldValue('ui.saveMode', false);
    setFieldValue('ui.isRecordSave', false);
    setFieldValue('ui.submitMode', false);
  },
})(RequestFormView);

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(expensesRequestForm): React.ComponentType<*>): React.ComponentType<Object>);
