// @flow
import { cloneDeep, get, isEmpty } from 'lodash';
import { connect } from 'react-redux';
import * as React from 'react';

import DateUtil from '../../../commons/utils/DateUtil';
import msg from '../../../commons/languages';
import { confirm } from '../../../commons/actions/app';

import { mapStateToProps as mapExpenseStateToProps } from '../../../expenses-pc/containers/Expenses/RecordListContainer';
import { actions as overlapActions } from '../../modules/ui/expenses/overlap';
// import { actions as openEditMenuActions } from '../../modules/ui/expenses/recordListPane/recordList/openEditMenu';
import { actions as openTitleActions } from '../../modules/ui/expenses/recordListPane/summary/openTitle';
import { actions as selectedExpReportActions } from '../../modules/ui/expenses/selectedExpReport';
import {
  deleteExpRecord,
  openExpenseTypeDialog,
} from '../../action-dispatchers/Requests';

import RecordListView from '../../../commons/components/exp/Form/RecordList';

import RecordIcon from '../../../commons/components/exp/Form/RecordList/Icon';
import { isFixedAllowanceMulti } from '../../../domain/models/exp/Record';
import { actions as fixedAmountOptionActions } from '../../modules/ui/expenses/recordItemPane/fixedAmountOption';

import iconVoucherReceipt from '../../images/iconVoucherReceipt.png';
import iconVoucherTransit from '../../images/iconVoucherTransit.png';

import { actions as activeDialogActions } from '../../modules/ui/expenses/dialog/activeDialog';
import { actions as recordCloneActions } from '../../modules/ui/expenses/dialog/recordClone/dialog';

function getImage() {
  return {
    receipt: () => {
      return <img src={iconVoucherReceipt} alt="receipt" />;
    },
    routeSelected: () => {
      return <img src={iconVoucherTransit} alt="receipt" />;
    },
    warningReceipt: () => {
      return null;
    },
    warningRoute: (idx, ROOT, errors, touched) => {
      return (
        <RecordIcon
          idx={idx}
          errors={errors}
          touched={touched}
          tooltip={msg().Exp_Lbl_RouteNeeded}
          className={`${ROOT}__receipt__record__icon`}
        />
      );
    },
  };
}

const mapStateToProps = (state, ownProps) => {
  const expenseStateProps = mapExpenseStateToProps(state, ownProps);

  // override Expense's getImage with Request's getImage
  return {
    ...expenseStateProps,
    isExpenseRequest: true,
    getImage,
  };
};

const mapDispatchToProps = {
  closeRecord: overlapActions.nonOverlapRecord,
  closeTitle: openTitleActions.close,
  deleteExpRecord,
  confirm,
  nonOverlapReport: overlapActions.nonOverlapReport,
  openExpenseTypeDialog,
  overlapRecord: overlapActions.overlapRecord,
  searchOptionList: fixedAmountOptionActions.search,
  openRecordCloneDialog: activeDialogActions.recordCloneDate,
  setCloneReport: recordCloneActions.setRecord,
  setReport: selectedExpReportActions.select,
  // Edit dropdown has been removed, can be used in future
  // onClickCloseEditMenu: openEditMenuActions.close,
  // onClickOpenEditMenu: openEditMenuActions.open,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickNewRecordButton() {
    dispatchProps.closeTitle();
    dispatchProps.openExpenseTypeDialog(
      stateProps.employeeId,
      stateProps.companyId,
      ownProps.expReport.scheduledDate,
      '',
      ownProps.expReport.expReportTypeId,
      false
    );
  },
  onClickRecord: (idx: number) => {
    dispatchProps.closeTitle();
    ownProps.onChangeEditingExpReport('ui.recordIdx', idx);
    // store old state into temp ui state to restore later
    ownProps.onChangeEditingExpReport(
      'ui.selectedRecord',
      ownProps.expReport.records[idx]
    );

    /* temporary saved copy of record items for following use:
     1. serve as a temp save copy when user temporarily "save" record items
     2. restore formik to the temp saved items when user cancel editing */
    ownProps.onChangeEditingExpReport(
      'ui.tempSavedRecordItems',
      ownProps.expReport.records[idx].items
    );

    // fetch amount option list if record type is Multiple Fixed Allowance and redux doesn't have data
    const recordType = ownProps.expReport.records[idx].recordType;
    const expTypeId = ownProps.expReport.records[idx].items[0].expTypeId;
    const optionList = get(stateProps.fixedAmountOptionList, expTypeId);
    if (isFixedAllowanceMulti(recordType) && isEmpty(optionList)) {
      dispatchProps.searchOptionList(expTypeId);
    }

    dispatchProps.overlapRecord();
  },
  onChangeCheckBox: (idx: number) => {
    const target = ownProps.checkboxes.indexOf(idx);
    const checkboxes = cloneDeep(ownProps.checkboxes);
    if (target > -1) {
      checkboxes.splice(target, 1);
    } else {
      checkboxes.push(idx);
    }
    ownProps.onChangeEditingExpReport('ui.checkboxes', checkboxes);
  },
  onClickDeleteRecordItem: () => {
    dispatchProps.confirm(msg().Exp_Msg_ConfirmDeleteSelectedRecords, (yes) => {
      if (yes) {
        const checkboxes = ownProps.checkboxes.sort((a, b) => (a > b ? -1 : 1));
        const records = cloneDeep(ownProps.expReport.records);
        let recordsTouched = [];
        const selectedRecordIds = [];
        checkboxes.forEach((index) => {
          selectedRecordIds.push(records[index].recordId);
          records.splice(index, 1);
        });
        if (ownProps.touched && ownProps.touched.records) {
          recordsTouched = cloneDeep(ownProps.touched.records);
          checkboxes.forEach((index) => {
            recordsTouched.splice(index, 1);
          });
        }

        // Change touched into false so that report remains in Select mode.
        ownProps.onChangeEditingExpReport(`report.records`, records, false);
        ownProps.onChangeEditingExpReport('ui', {
          checkboxes: [],
          recordIdx: -1,
          recalc: true,
          editMode: true,
        });
        const updateReport = cloneDeep(ownProps.expReport);
        updateReport.records = records;
        dispatchProps.setReport(updateReport, stateProps.reportTypeList);
        dispatchProps.deleteExpRecord(selectedRecordIds);
      }
    });
  },
  onClickCloneRecordButton() {
    const checkboxes = ownProps.checkboxes;
    const records = cloneDeep(ownProps.expReport.records);
    const selectedRecordIds = [];
    let latestDate = records[`${checkboxes[0]}`].recordDate;
    checkboxes.forEach((index) => {
      selectedRecordIds.push(records[index].recordId);
      if (DateUtil.isBefore(latestDate, records[index].recordDate)) {
        latestDate = records[index].recordDate;
      }
    });
    dispatchProps.setCloneReport(
      selectedRecordIds,
      DateUtil.addDays(latestDate, 1)
    );
    dispatchProps.openRecordCloneDialog();
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordListView): React.ComponentType<*>): React.ComponentType<Object>);
