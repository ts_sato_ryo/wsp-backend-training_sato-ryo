// @flow
import { connect } from 'react-redux';

import msg from '../../../commons/languages';

import {
  fetchExpReportList,
  // pagination
  fetchExpReport,
  backFromDetailToList,
  fetchExpReportIdList,
} from '../../action-dispatchers/Requests';

import {
  PAGE_SIZE,
  MAX_PAGE_NUM,
  MAX_SEARCH_RESULT_NUM,
} from '../../modules/ui/expenses/reportList/page';
import { actions as tabAction } from '../../modules/ui/expenses/tab';
import { modes } from '../../modules/ui/expenses/mode';
import { confirm } from '../../../commons/actions/app';

import withRequestsHOC from '../../components/Requests';
import ExpView from '../../../commons/components/exp';

function labelObject() {
  return {
    reports: msg().Exp_Lbl_ReportsRequest,
    newReport: msg().Exp_Lbl_NewReportCreateReq,
  };
}

function mapStateToProps(state) {
  const reportIdList = state.entities.reportIdList.reportIdList;
  const selectedRequestId =
    state.ui.expenses.selectedExpReport.reportId ||
    state.ui.expenses.selectedExpReport.preRequestId;
  // The idx of the screen being displayed.
  // if no report is selected, values is -1
  const currentRequestIdx = selectedRequestId
    ? reportIdList.indexOf(selectedRequestId)
    : -1;

  return {
    // pagination props
    currentRequestIdx,
    currentPage: state.ui.expenses.reportList.page,
    requestTotalNum: state.entities.reportIdList.totalSize,
    orderBy: state.ui.expenses.reportList.orderBy,
    sortBy: state.ui.expenses.reportList.sortBy,
    pageSize: PAGE_SIZE,
    maxPageNo: MAX_PAGE_NUM,
    maxSearchNo: MAX_SEARCH_RESULT_NUM,
    reportIdList,
    // end of pagination props
    labelObject,
    mode: state.ui.expenses.mode,
    selectedTab: state.ui.expenses.tab,
    overlap: state.ui.expenses.overlap,
    selectedExpReport: state.ui.expenses.selectedExpReport,
    userSetting: state.userSetting,
    reportTypeList: state.entities.exp.expenseReportType.list.active,
    expReportList: state.entities.exp.preRequest.expReportList,
  };
}

const mapDispatchToProps = {
  confirm,
  fetchExpReportList,
  // pagination
  backFromDetailToList,
  fetchExpReportIdList,
  fetchExpReport,
  onChangeTab: tabAction.changeTab,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  // Move to next to report. it's either +1 or -1
  onClickNextToRequestButton: (moveNum: number) => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.fetchExpReport(
            stateProps.reportIdList[stateProps.currentRequestIdx + moveNum],
            stateProps.reportTypeList
          );
        }
      });
    } else {
      dispatchProps.fetchExpReport(
        stateProps.reportIdList[stateProps.currentRequestIdx + moveNum],
        stateProps.reportTypeList
      );
    }
  },
  // selects the correct index when going back to list view.
  onClickBackButton: () => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.backFromDetailToList(
            stateProps.reportIdList,
            stateProps.currentRequestIdx,
            stateProps.expReportList
          );
        }
      });
    } else {
      dispatchProps.backFromDetailToList(
        stateProps.reportIdList,
        stateProps.currentRequestIdx,
        stateProps.expReportList
      );
    }
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(withRequestsHOC(ExpView)): React.ComponentType<Object>);
