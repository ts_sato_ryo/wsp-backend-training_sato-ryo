// @flow
import { connect } from 'react-redux';
import { cloneDeep, set, last } from 'lodash';
import msg from '../../../../commons/languages';
import FileUtil from '../../../../commons/utils/FileUtil';
import { actions as selectedReceiptActions } from '../../../modules/ui/expenses/receiptLibrary/selectedReceipt';
import { actions as commentActions } from '../../../modules/ui/expenses/dialog/approval/comment';
import {
  getBase64File,
  getFilePreview,
  deleteReceipt,
  uploadReceipts,
  openExpenseTypeDialog,
} from '../../../action-dispatchers/Requests';

import ReceiptLibraryDialog from '../../../../commons/components/exp/Form/Dialog/ReceiptLibrary';

const mapStateToProps = (state) => {
  // Receipts with no OCR info
  const isReportReceipt = true;
  const isMultiStep = false;

  const activeDialog = state.ui.expenses.dialog.activeDialog;
  const currentDialog = last(activeDialog);
  const mainButtonTitle = msg().Exp_Lbl_Attach;
  return {
    mainButtonTitle,
    isReportReceipt,
    isMultiStep,
    currentDialog,
    title: msg().Exp_Lbl_ReceiptLibrary,
    receiptList: state.entities.exp.receiptLibrary.list.receipts,
    photoUrl: state.userSetting.photoUrl,
    comment: state.ui.expenses.dialog.approval.comment,
    customHint: state.entities.exp.customHint.recordReceipt,
    receiptStatus: state.ui.expenses.receiptLibrary.status,
    selectedReceipt: state.ui.expenses.receiptLibrary.selectedReceipt,
    companyId: state.userSetting.companyId,
    employeeId: state.userSetting.employeeId,
    reportTypeId: state.ui.expenses.selectedExpReport.expReportTypeId,
  };
};

const mapDispatchToProps = {
  getBase64File,
  getFilePreview,
  deleteReceipt,
  uploadReceipts,
  openExpenseTypeDialog,
  onChangeComment: commentActions.set,
  setSelectedReceipt: selectedReceiptActions.set,
  resetSelectedReceipt: selectedReceiptActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  deleteReceipt: (receiptId) => dispatchProps.deleteReceipt(receiptId),
  onClickReceiptLibrayCloseButton: () => {
    dispatchProps.resetSelectedReceipt();
    ownProps.onClickHideDialogButton();
  },
  onImageDrop: (file) => {
    return dispatchProps
      .getBase64File(file)
      .then((base64File) => {
        return dispatchProps.uploadReceipts([base64File]).then((res) => {
          if (res) {
            return {
              receiptId: res.contentDocumentId,
              receiptFileId: res.contentVersionId,
              receiptData: base64File.data,
            };
          }
          return {
            receiptId: null,
            receiptFileId: null,
            receiptData: null,
          };
        });
      })
      .catch(() => {});
  },
  onClickMainButton: () => {
    ownProps.onClickCancelButton();
  },
  getFilePreview: (receiptFileId) =>
    dispatchProps.getFilePreview(receiptFileId).then((res) => {
      const dataType = FileUtil.getMIMEType(res.payload.fileType);

      return {
        receiptFileId,
        receiptId: res.payload.contentDocumentId,
        fileBody: `data:${dataType};base64,${res.payload.fileBody}`,
        title: res.payload.title,
        uploadedDate: res.payload.uploadedDate,
      };
    }),
  onClickSelectReceipt: (receiptFileId, receiptId, receiptData) => {
    ownProps.hideDialog();
    dispatchProps.resetSelectedReceipt();
    const expReport = cloneDeep(ownProps.expReport);
    const touched = cloneDeep(ownProps.touched);
    if (ownProps.recordIdx < 0) {
      // for report receipt
      expReport.attachedFileId = receiptId;
      expReport.attachedFileVerId = receiptFileId;
      expReport.attachedFileData = receiptData;
      set(touched, 'report.attachedFileId', true);
      set(touched, 'report.attachedFileVerId', true);
      ownProps.onChangeEditingExpReport('report', expReport, touched);
      return;
    }
    expReport.records[ownProps.recordIdx].receiptId = receiptId;
    expReport.records[ownProps.recordIdx].receiptFileId = receiptFileId;
    expReport.records[ownProps.recordIdx].receiptData = receiptData;

    if (!touched.records) {
      touched.records = {};
    }
    if (!touched.records[ownProps.recordIdx]) {
      touched.records[ownProps.recordIdx] = {};
    }
    const touchedRecord = touched.records[ownProps.recordIdx];
    touchedRecord.receiptId = true;
    ownProps.onChangeEditingExpReport(`report`, expReport, touched);
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ReceiptLibraryDialog): React.ComponentType<*>): React.ComponentType<Object>);
