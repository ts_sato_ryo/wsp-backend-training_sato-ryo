// @flow
import { connect } from 'react-redux';
import isEmpty from 'lodash/isEmpty';
import msg from '../../../../commons/languages';
import DateUtil from '../../../../commons/utils/DateUtil';
import { showToast } from '../../../../commons/modules/toast';
import { cloneRecords } from '../../../action-dispatchers/Requests';
import { actions as activeDialogActions } from '../../../modules/ui/expenses/dialog/activeDialog';
import { actions as recordCloneActions } from '../../../modules/ui/expenses/dialog/recordClone/dialog';
import RecordCloneDate from '../../../../commons/components/exp/Form/Dialog/RecordClone/CloneDateSelection';

const mapStateToProps = (state) => ({
  recordClone: state.ui.expenses.dialog.recordClone.dialog,
  language: state.userSetting.language,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
});

const mapDispatchToProps = {
  cloneRecords,
  showToast,
  showRecordUpdateDialog: activeDialogActions.recordUpdated,
  onChangeCloneDate: recordCloneActions.setDate,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickRecordCloneButton: () => {
    const { dates, records } = stateProps.recordClone;
    const targetDates = dates.map((item) =>
      DateUtil.format(item, 'YYYY-MM-DD')
    );
    dispatchProps
      .cloneRecords(
        targetDates,
        records,
        ownProps.expReport.reportId,
        stateProps.reportTypeList
      )
      .then((cloneRes) => {
        const { recordIds, updatedRecords } = cloneRes;
        if (!isEmpty(recordIds)) {
          dispatchProps.showToast(msg().Exp_Msg_CloneRecords, 4000);
          if (!isEmpty(updatedRecords)) {
            dispatchProps.showRecordUpdateDialog();
          }
        }
      });
  },
  onClickChangeDate: (selectedDates) => {
    dispatchProps.onChangeCloneDate(selectedDates);
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordCloneDate): React.ComponentType<*>): React.ComponentType<Object>);
