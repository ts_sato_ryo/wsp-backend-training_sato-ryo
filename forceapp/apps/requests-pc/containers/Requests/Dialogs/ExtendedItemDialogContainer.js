// @flow
import { connect } from 'react-redux';
import cloneDeep from 'lodash/cloneDeep';
import { actions as eiSearchAction } from '../../../modules/ui/expenses/dialog/extendedItem/search';
import { actions as eiRecentlyUsedAction } from '../../../modules/ui/expenses/dialog/extendedItem/recentlyUsed';
import { searchEILookup } from '../../../action-dispatchers/Requests';
import ExtendedItem from '../../../../commons/components/exp/Form/Dialog/ExtendedItem';

const mapStateToProps = (state) => ({
  eiRecentlyUsed: state.ui.expenses.dialog.extendedItem.recentlyUsed,
  extendedItemLookup: state.ui.expenses.dialog.extendedItem.search,
});

const mapDispatchToProps = {
  searchEILookup,
  clearEIRecentlyUsedDialog: eiRecentlyUsedAction.clear,
  clearEISearchDialog: eiSearchAction.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickSearchLookup: (id, query) =>
    dispatchProps.searchEILookup(id, query).then((res) => res),
  onClickCustomObjectOption: (item) => {
    const idx = stateProps.extendedItemLookup.idx;
    const target = stateProps.extendedItemLookup.target;
    const expReport = cloneDeep(ownProps.expReport);
    const touched = cloneDeep(ownProps.touched);
    const recordItemIdx = ownProps.recordItemIdx || 0;

    if (target === 'REPORT') {
      expReport[`extendedItemLookup${idx}SelectedOptionName`] = item.name;
      expReport[`extendedItemLookup${idx}Value`] = item.code;
    } else {
      const targetRecord = expReport.records[ownProps.recordIdx];
      targetRecord.items[recordItemIdx][
        `extendedItemLookup${idx}SelectedOptionName`
      ] = item.name;
      targetRecord.items[recordItemIdx][`extendedItemLookup${idx}Value`] =
        item.code;
    }
    touched[`extendedItemLookup${idx}Value`] = true;
    ownProps.onChangeEditingExpReport('report', expReport, touched);
    dispatchProps.clearEISearchDialog();
    dispatchProps.clearEIRecentlyUsedDialog();
    ownProps.hideDialog();
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ExtendedItem): React.ComponentType<*>): React.ComponentType<Object>);
