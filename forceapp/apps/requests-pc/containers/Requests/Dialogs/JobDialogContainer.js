// @flow
import { connect } from 'react-redux';
import {
  getJobList,
  getJobSearchResult,
  getNextJobList,
} from '../../../action-dispatchers/Requests';
import JobSelect from '../../../../commons/components/exp/Form/Dialog/JobSelect';

const mapStateToProps = (state) => ({
  jobList: state.ui.expenses.dialog.jobSelect.list.selectionList,
  jobSearchList: state.ui.expenses.dialog.jobSelect.list.searchList,
  jobRecentItems: state.ui.expenses.dialog.jobSelect.list.recentItems,
  hintMsg: state.entities.exp.customHint.reportHeaderJob,
  employeeId: state.userSetting.employeeId,
});

const mapDispatchToProps = {
  getJobList,
  getJobSearchResult,
  getNextJobList,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickJobSelectByCategory: () => {
    dispatchProps.getJobList(null, ownProps.expReport.scheduledDate);
  },
  onClickJobSearch: (keyword) => {
    dispatchProps.getJobSearchResult(
      keyword,
      ownProps.expReport.scheduledDate,
      stateProps.employeeId
    );
  },
  onClickJobListItem: (item, items) => {
    if (item.hasChildren && items !== undefined) {
      dispatchProps.getNextJobList(
        item,
        items,
        item.id,
        ownProps.expReport.scheduledDate
      );
    } else {
      const recordIdx = ownProps.recordIdx;
      // if no record be selected, set job info to report header, otherwise set to selected record
      if (recordIdx === -1) {
        ownProps.onChangeEditingExpReport(`report.jobId`, item.id, true);
        ownProps.onChangeEditingExpReport(`report.jobName`, item.name, true);
        ownProps.onChangeEditingExpReport(`report.jobCode`, item.code, true);
      } else {
        ownProps.onChangeEditingExpReport(
          `report.records[${recordIdx}].items[0].jobId`,
          item.id,
          true
        );
        ownProps.onChangeEditingExpReport(
          `report.records[${recordIdx}].items[0].jobName`,
          item.name,
          true
        );
        ownProps.onChangeEditingExpReport(
          `report.records[${recordIdx}].items[0].jobCode`,
          item.code,
          true
        );
      }
      ownProps.hideDialog();
      ownProps.clearDialog();
    }
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(JobSelect): React.ComponentType<*>): React.ComponentType<Object>);
