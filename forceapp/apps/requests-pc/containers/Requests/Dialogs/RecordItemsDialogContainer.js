// @flow
import { connect } from 'react-redux';
import { cloneDeep, get, isEmpty, last } from 'lodash';
import DateUtil from '../../../../commons/utils/DateUtil';
import { newRecordItem } from '../../../../domain/models/exp/Record';
import { calculateTax } from '../../../../domain/models/exp/TaxType';
import { calcAmountFromRate } from '../../../../domain/models/exp/foreign-currency/Currency';
import { actions as activeDialogActions } from '../../../modules/ui/expenses/dialog/activeDialog';
import {
  openRecordItemsConfirmDialog,
  searchExpTypesByParentRecord,
  searchTaxTypeList,
} from '../../../action-dispatchers/Requests';
import { getRateFromId } from '../../../action-dispatchers/ForeignCurrency';
import recordItemContainer from '../RecordItemContainer';

import RecordItems from '../../../../commons/components/exp/Form/Dialog/RecordItems';

const mapStateToProps = (state) => {
  const activeDialog = state.ui.expenses.dialog.activeDialog;
  const currentDialog = last(activeDialog);

  return {
    currentDialog,
    customHint: state.entities.exp.customHint,
    tax: state.ui.expenses.recordItemPane.tax,
    companyId: state.userSetting.companyId,
    baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
    baseCurrencySymbol: state.userSetting.currencySymbol,
    childExpTypeLists: state.entities.exp.expenseType.childList,
    exchangeRateMap:
      state.ui.expenses.recordItemPane.foreignCurrency.exchangeRate,
  };
};

const mapDispatchToProps = {
  openRecordItemsConfirmDialog,
  searchExpTypesByParentRecord,
  searchTaxTypeList,
  getRateFromId,
  hideAllDialog: activeDialogActions.hideAll,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  recordItemContainer,
  // initialize details (tax, EIs, etc) for child record items
  initTaxAndEIsForRecordItems: (expTypeList) => {
    const expReport = cloneDeep(ownProps.expReport);

    // method to get tax detail based on tax & amount
    const getInitTaxInfo = (initTax, item) => {
      const rate = initTax ? initTax.rate : 0;
      const baseId = item.taxTypeBaseId || 'noIdSelected';
      const historyId = initTax ? initTax.historyId : null;
      const name = initTax ? initTax.name : '';
      const taxRes = calculateTax(
        rate,
        item.amount || 0,
        stateProps.baseCurrencyDecimal
      );

      const taxInfo = {
        withoutTax: taxRes.amountWithoutTax,
        gstVat: taxRes.gstVat,
        taxTypeBaseId: baseId,
        taxTypeHistoryId: historyId,
        taxTypeName: name,
        taxRate: rate,
      };

      if (item.taxManual) {
        taxInfo.withoutTax = item.withoutTax;
        taxInfo.gstVat = item.gstVat;
      }

      return taxInfo;
    };

    // array of promise which then return record item info including tax
    const promises = expReport.records[ownProps.recordIdx].items.map(
      async (item, index) => {
        if (index === 0) {
          // keep item 0 (parent record) as it is
          return item;
        }

        const { recordDate, expTypeId, expTypeName } = item;
        const expTypeInfo = expTypeList.find((ele) => ele.id === expTypeId);
        const initItem = newRecordItem(
          expTypeId,
          expTypeName,
          false,
          expTypeInfo,
          false
        );
        const getInitTaxType = (list) => {
          if (!Array.isArray(list)) {
            return null;
          }
          const initialTaxType =
            list.find((taxType) => {
              return (
                taxType.baseId === item.taxTypeBaseId &&
                taxType.historyId === item.taxTypeHistoryId
              );
            }) || list[0];
          return initialTaxType;
        };

        const taxTypeList = get(stateProps.tax, `${expTypeId}.${recordDate}`);
        if (!isEmpty(taxTypeList)) {
          const selectedTax = getInitTaxType(taxTypeList);
          const taxInfo = getInitTaxInfo(selectedTax, item);
          return { ...initItem, ...item, ...taxInfo };
        } else {
          return dispatchProps
            .searchTaxTypeList(expTypeId, recordDate)
            .then((result) => {
              const typeList = result.payload[expTypeId][recordDate];
              const selectedTax = getInitTaxType(typeList);
              const taxInfo = getInitTaxInfo(selectedTax, item);
              return { ...initItem, ...item, ...taxInfo };
            });
        }
      }
    );

    Promise.all(promises).then((recordItems) => {
      return ownProps.onChangeEditingExpReport(
        `report.records.${ownProps.recordIdx}.items`,
        recordItems
      );
    });
  },
  initForeignCurrencyAndEIs: (expTypeList) => {
    const items = cloneDeep(
      ownProps.expReport.records[ownProps.recordIdx].items
    );
    const currencyId = items[0].currencyId;
    const promises = items.map(async (item, index) => {
      if (index === 0) {
        // keep item 0 (parent record) as it is
        return item;
      }

      const {
        expTypeId,
        expTypeName,
        localAmount,
        exchangeRateManual,
        recordDate,
      } = item;

      const currencyInfo = {
        useForeignCurrency: true,
        currencyId,
        currencyInfo: items[0].currencyInfo,
      };
      const itemWithCurrency = { ...item, ...currencyInfo };

      // if exchangeRateManual true, means it was saved once, no need to init again
      if (exchangeRateManual) {
        return itemWithCurrency;
      }

      const getRateAndAmount = (rate, isManual) => {
        const exchangeRate = rate || 0;
        const rateManual = isManual || exchangeRate === 0;
        const originalExchangeRate = isManual ? 0 : exchangeRate;
        return {
          exchangeRate,
          originalExchangeRate,
          exchangeRateManual: rateManual,
          amount: calcAmountFromRate(
            exchangeRate,
            localAmount,
            stateProps.baseCurrencyDecimal
          ),
        };
      };

      const expTypeInfo = expTypeList.find((ele) => ele.id === expTypeId);
      const initItem = newRecordItem(
        expTypeId,
        expTypeName,
        true,
        expTypeInfo,
        false
      );
      const updatedItem = { ...initItem, ...itemWithCurrency };
      const today = DateUtil.getToday();
      const exchangeRateInfo = get(
        stateProps.exchangeRateMap,
        `${currencyId}.${recordDate}`
      );

      if (exchangeRateInfo) {
        return {
          ...updatedItem,
          ...getRateAndAmount(exchangeRateInfo.calculationRate),
        };
      } else {
        return dispatchProps
          .getRateFromId(stateProps.companyId, currencyId, recordDate)
          .then((exchangeRate) => {
            if (exchangeRate > 0 || recordDate === today) {
              return {
                ...updatedItem,
                ...getRateAndAmount(exchangeRate),
              };
            } else {
              return dispatchProps
                .getRateFromId(stateProps.companyId, currencyId, today)
                .then((todayRate) => {
                  return {
                    ...updatedItem,
                    ...getRateAndAmount(todayRate, true),
                  };
                });
            }
          });
      }
    });

    Promise.all(promises).then((recordItems) => {
      return ownProps.onChangeEditingExpReport(
        `report.records.${ownProps.recordIdx}.items`,
        recordItems
      );
    });
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordItems): React.ComponentType<*>): React.ComponentType<Object>);
