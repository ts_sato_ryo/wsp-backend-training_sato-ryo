// @flow
import { connect } from 'react-redux';
import last from 'lodash/last';
import msg from '../../../../commons/languages';
import {
  cancelExpRequestApproval,
  submitExpReport,
} from '../../../action-dispatchers/Requests';
import { dialogTypes } from '../../../modules/ui/expenses/dialog/activeDialog';
import { actions as commentActions } from '../../../modules/ui/expenses/dialog/approval/comment';

import ApprovalDialogView from '../../../../commons/components/exp/Form/Dialog/Approval';

const mapStateToProps = (state) => {
  const activeDialog = state.ui.expenses.dialog.activeDialog;
  const currentDialog = last(activeDialog);
  let title = msg().Exp_Lbl_Request;
  let mainButtonTitle = msg().Com_Btn_Request;
  if (currentDialog === dialogTypes.CANCEL_REQUEST) {
    title = msg().Exp_Lbl_Recall;
    mainButtonTitle = msg().Exp_Lbl_Recall;
  }
  return {
    title,
    mainButtonTitle,
    currentDialog,
    photoUrl: state.userSetting.photoUrl,
    comment: state.ui.expenses.dialog.approval.comment,
  };
};

const mapDispatchToProps = {
  submitExpReport,
  cancelExpRequestApproval,
  onChangeComment: commentActions.set,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickCancelButton: () => {
    dispatchProps.cancelExpRequestApproval(
      ownProps.expReport.requestId,
      stateProps.comment
    );
  },
  onClickApprovalButton: () => {
    dispatchProps.submitExpReport(
      ownProps.expReport.reportId,
      stateProps.comment
    );
  },
  onClickMainButton: () => {
    if (stateProps.currentDialog === dialogTypes.APPROVAL) {
      dispatchProps.submitExpReport(
        ownProps.expReport.reportId,
        stateProps.comment
      );
    } else if (stateProps.currentDialog === dialogTypes.CANCEL_REQUEST) {
      dispatchProps.cancelExpRequestApproval(
        ownProps.expReport.requestId,
        stateProps.comment
      );
    }
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ApprovalDialogView): React.ComponentType<*>): React.ComponentType<Object>);
