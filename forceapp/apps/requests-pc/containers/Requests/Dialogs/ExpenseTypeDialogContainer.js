// @flow
import { connect } from 'react-redux';
import get from 'lodash/get';

import {
  newRecord,
  isRecordItemized,
  isFixedAllowanceSingle,
  isFixedAllowanceMulti,
} from '../../../../domain/models/exp/Record';
import { calculateTax } from '../../../../domain/models/exp/TaxType';

import {
  getExpenseTypeList,
  getExpenseTypeSearchResult,
  getNextExpenseTypeList,
  searchTaxTypeList,
} from '../../../action-dispatchers/Requests';
import { resetRouteForm } from '../../../action-dispatchers/Route';

import { actions as fixedAmountOptionActions } from '../../../modules/ui/expenses/recordItemPane/fixedAmountOption';
import { actions as overlapActions } from '../../../modules/ui/expenses/overlap';
import { actions as selectedReceiptActions } from '../../../modules/ui/expenses/receiptLibrary/selectedReceipt';
import { actions as searchRouteActions } from '../../../../domain/modules/exp/jorudan/routeOption';

import ExpenseTypeSelect from '../../../../commons/components/exp/Form/Dialog/ExpenseTypeSelect';

const mapStateToProps = (state) => ({
  expenseTypeList:
    state.ui.expenses.dialog.expenseTypeSelect.list.selectionList,
  expenseTypeSearchList:
    state.ui.expenses.dialog.expenseTypeSelect.list.searchList,
  expenseTypeRecentItems:
    state.ui.expenses.dialog.expenseTypeSelect.list.recentItems,
  selectedReceipt: state.ui.expenses.receiptLibrary.selectedReceipt,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  companyId: state.userSetting.companyId,
  recordType: state.ui.expenses.dialog.expenseTypeSelect.recordType,
  hintMsg: state.entities.exp.customHint.recordExpenseType,
});

const mapDispatchToProps = {
  resetRouteForm,
  getExpenseTypeList,
  getExpenseTypeSearchResult,
  getNextExpenseTypeList,
  searchTaxTypeList,
  searchRouteOption: searchRouteActions.search,
  setFixedAmountOption: fixedAmountOptionActions.set,
  overlap: overlapActions.overlapRecord,
  resetSelectedReceipt: selectedReceiptActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickExpenseTypeSelectByCategory: () => {
    dispatchProps.getExpenseTypeList(
      stateProps.recordType,
      ownProps.expReport.scheduledDate,
      ownProps.expReport.expReportTypeId
    );
  },
  onClickExpenseTypeCloseButton: () => {
    dispatchProps.resetSelectedReceipt();
    ownProps.onClickHideDialogButton();
  },
  onClickExpenseTypeSearch: (keyword) => {
    dispatchProps.getExpenseTypeSearchResult(
      stateProps.companyId,
      keyword,
      ownProps.expReport.scheduledDate,
      ownProps.expReport.expReportTypeId,
      stateProps.recordType
    );
  },
  onClickExpenseTypeItem: (selectedExpType, hierarExpTypes) => {
    /**
     * 新規明細ボタンを押されて呼ばれた費目選択ダイアログ内のアイテム（費目or費目グループ）が押された時の挙動を定義する関数
     * define behavior when item (===expense type or expType group) in expense type dialog is selected
     * only about when new record button clicked
     * @param selectedExpType 選択された費目or費目グループ / selected expense type or expense type group
     * @param hierarExpTypes 費目or費目グループの階層構造 / hierarchal expense type or expense type group
     */

    // 選択されたものが費目グループの場合 / if expense group is selected
    if (selectedExpType.isGroup) {
      dispatchProps.getNextExpenseTypeList(
        selectedExpType,
        hierarExpTypes,
        stateProps.recordType,
        ownProps.expReport.expReportTypeId
      );
    }
    // 選択されたものが費目の場合 / if expense type is selected
    else {
      const idx = ownProps.expReport.records.length;
      const newRec = newRecord(
        selectedExpType.id,
        selectedExpType.name,
        selectedExpType.recordType,
        selectedExpType.useForeignCurrency,
        selectedExpType,
        false,
        selectedExpType.fileAttachment,
        selectedExpType.fixedForeignCurrencyId,
        selectedExpType.foreignCurrencyUsage
      );
      const expTypeId = selectedExpType.id;
      const recordDate = ownProps.expReport.scheduledDate;

      newRec.recordDate = recordDate;
      dispatchProps.searchTaxTypeList(expTypeId, recordDate).then((result) => {
        const initTax = result.payload[expTypeId][recordDate][0];
        // 選択された費目の明細タイプがジョルダンの場合 / if record type of selected expense type is Jorudan
        if (selectedExpType.recordType === 'TransitJorudanJP') {
          dispatchProps.resetRouteForm(null);
          dispatchProps.searchRouteOption(stateProps.companyId);
        }
        newRec.items[0].taxTypeBaseId = 'noIdSelected';
        newRec.items[0].taxTypeHistoryId = initTax ? initTax.historyId : null;

        if (isRecordItemized(selectedExpType.recordType, false)) {
          newRec.items[0].taxTypeBaseId = null;
          newRec.items[0].taxTypeHistoryId = null;
          newRec.items[0].taxTypeName = null;
        }

        if (isFixedAllowanceSingle(selectedExpType.recordType)) {
          const singleFixedAmout =
            selectedExpType.fixedAllowanceSingleAmount || 0;
          newRec.amount = singleFixedAmout;
          newRec.items[0].amount = singleFixedAmout;
          if (selectedExpType.fixedForeignCurrencyId) {
            newRec.items[0].localAmount = singleFixedAmout;
          } else {
            const taxRes = calculateTax(
              initTax.rate,
              singleFixedAmout,
              stateProps.baseCurrencyDecimal
            );
            newRec.items[0].gstVat = taxRes.gstVat;
            newRec.items[0].withoutTax = taxRes.amountWithoutTax;
          }
        }

        if (isFixedAllowanceMulti(selectedExpType.recordType)) {
          const lists = get(selectedExpType, 'fixedAllowanceOptionList') || [];
          dispatchProps.setFixedAmountOption(selectedExpType.id, lists);
        }

        ownProps.onChangeEditingExpReport(`report.records[${idx}]`, newRec, {});
        ownProps.onChangeEditingExpReport('ui.recordIdx', idx);
      });
      dispatchProps.overlap();
      ownProps.hideDialog();
      ownProps.clearDialog();
    }
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ExpenseTypeSelect): React.ComponentType<*>): React.ComponentType<Object>);
