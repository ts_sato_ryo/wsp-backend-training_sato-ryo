// @flow
import { connect } from 'react-redux';
import {
  getCostCenterList,
  getNextCostCenterList,
  getCostCenterSearchResult,
} from '../../../action-dispatchers/Requests';
import CostCenterSelect from '../../../../commons/components/exp/Form/Dialog/CostCenterSelect';

const mapStateToProps = (state) => ({
  companyId: state.userSetting.companyId,
  hintMsg: state.entities.exp.customHint.reportHeaderCostCenter,
  costCenterList: state.ui.expenses.dialog.costCenterSelect.list.selectionList,
  costCenterSearchList:
    state.ui.expenses.dialog.costCenterSelect.list.searchList,
  costCenterRecentItems:
    state.ui.expenses.dialog.costCenterSelect.list.recentItems,
});

const mapDispatchToProps = {
  getCostCenterList,
  getNextCostCenterList,
  getCostCenterSearchResult,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickCostCenterSelectByCategory: () => {
    dispatchProps.getCostCenterList(null, ownProps.expReport.scheduledDate);
  },
  onClickCostCenterSearch: (keyword) => {
    dispatchProps.getCostCenterSearchResult(
      stateProps.companyId,
      keyword,
      ownProps.expReport.scheduledDate
    );
  },
  onClickCostCenterListItem: (item, items) => {
    if (item.hasChildren && items !== undefined) {
      dispatchProps.getNextCostCenterList(
        item,
        items,
        item.baseId,
        ownProps.expReport.scheduledDate
      );
    } else {
      const recordIdx = ownProps.recordIdx;
      // if no record be selected, set cost center info to report header, otherwise set to selected record
      if (recordIdx === -1) {
        ownProps.onChangeEditingExpReport(
          `report.costCenterHistoryId`,
          item.id,
          true
        );
        ownProps.onChangeEditingExpReport(
          `report.costCenterName`,
          item.name,
          true
        );
        ownProps.onChangeEditingExpReport(
          `report.costCenterCode`,
          item.code,
          true
        );
      } else {
        ownProps.onChangeEditingExpReport(
          `report.records[${recordIdx}].items[0].costCenterHistoryId`,
          item.id,
          true
        );
        ownProps.onChangeEditingExpReport(
          `report.records[${recordIdx}].items[0].costCenterName`,
          item.name,
          true
        );
        ownProps.onChangeEditingExpReport(
          `report.records[${recordIdx}].items[0].costCenterCode`,
          item.code,
          true
        );
      }
      ownProps.hideDialog();
      ownProps.clearDialog();
    }
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CostCenterSelect): React.ComponentType<*>): React.ComponentType<Object>);
