// @flow
import { connect } from 'react-redux';
import * as React from 'react';
import _ from 'lodash';

import msg from '../../../commons/languages';
import DateUtil from '../../../commons/utils/DateUtil';
import DateField from '../../../commons/components/fields/DateField';

import { confirm } from '../../../commons/actions/app';
import { actions as openTitleActions } from '../../modules/ui/expenses/recordListPane/summary/openTitle';
import { actions as modeActions, modes } from '../../modules/ui/expenses/mode';

import {
  type EISearchObj,
  getEIsOnly,
} from '../../../domain/models/exp/ExtendedItem';
import {
  createNewExpReport,
  fetchExpReport,
  openCostCenterDialog,
  openJobDialog,
  openEILookupDialog,
  openVendorLookupDialog,
  cloneReport,
  searchVendorDetail,
  openReceiptLibraryDialog,
  getFilePreview,
} from '../../action-dispatchers/Requests';
import { setAvailableExpType } from '../../../domain/modules/exp/expense-type/availableExpType';

import ReportSummaryView from '../../../commons/components/exp/Form/ReportSummary';

const mapStateToProps = (state, ownProps) => ({
  companyId: state.userSetting.companyId,
  mode: state.ui.expenses.mode,
  expReportList: state.entities.exp.preRequest.expReportList,
  selectedExpReport: state.ui.expenses.selectedExpReport,
  baseCurrencySymbol: state.userSetting.currencySymbol,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  employeeId: state.userSetting.employeeId,
  openTitle: state.ui.expenses.recordListPane.summary.openTitle,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  customHint: state.entities.exp.customHint,
  inactiveReportTypeList: state.entities.exp.expenseReportType.list.inactive,
  activeVendor: state.ui.expenses.dialog.vendor.search,
  updateReport: (updateObj) => {
    const tmpReport = _.cloneDeep(ownProps.expReport);
    const tmpTouched = _.cloneDeep(ownProps.touched);

    Object.keys((updateObj: any)).forEach((key) => {
      _.set(tmpReport, key, updateObj[key]);
      _.set(tmpTouched, key, true);
    });
    ownProps.onChangeEditingExpReport('report', tmpReport, tmpTouched);
  },
  remarksLabel: msg().Exp_Clbl_Purpose,
  isExpenseRequest: true,
  ...ownProps,
});

const mapDispatchToProps = {
  confirm,
  createNewExpReport,
  fetchExpReport,
  openTitleAction: openTitleActions.open,
  onClickTitleToggleButton: openTitleActions.toggle,
  reportEdit: modeActions.reportEdit,
  onClickCostCenterButton: (targetDate: string, employeeId: string) =>
    openCostCenterDialog(targetDate, employeeId),
  onClickJobButton: (targetDate: string, employeeId: string) =>
    openJobDialog(targetDate, employeeId),
  openVendorLookupDialog,
  openEILookupDialog,
  setAvailableExpType,
  cloneReport,
  searchVendorDetail,
  openReceiptLibraryDialog,
  getFilePreview,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickNewReportButton: () => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.createNewExpReport();
        }
      });
    } else {
      dispatchProps.createNewExpReport();
    }
  },
  onClickBackBtn: () => {
    dispatchProps.moveBackToReport();
  },
  handleChangeExpenseReportType: (e: any) => {
    const reportTypeIndex = stateProps.reportTypeList.findIndex(
      (reportType) => reportType.id === e.target.value
    );
    const newReportType = stateProps.reportTypeList[reportTypeIndex];
    let resetVendorData = {};
    if (newReportType.isVendorRequired === 'UNUSED') {
      resetVendorData = {
        vendorId: null,
        vendorCode: '',
        vendorName: '',
        paymentDueDate: null,
        paymentDueDateUsage: null,
      };
    }
    let resetJobData = {};
    if (newReportType.isJobRequired === 'UNUSED') {
      resetJobData = {
        jobId: null,
        jobCode: '',
        jobName: '',
      };
    }
    let resetCostCenterData = {};
    if (newReportType.isCostCenterRequired === 'UNUSED') {
      resetCostCenterData = {
        costCenterName: null,
        costCenterCode: '',
        costCenterHistoryId: null,
      };
    }
    const originalReport = ownProps.expReport;
    const updateReceiptData = {
      useFileAttachment: newReportType.useFileAttachment,
    };
    if (!newReportType.useFileAttachment) {
      _.assign(updateReceiptData, {
        attachedFileVerId: null,
        attachedFileId: null,
        attachedFileData: null,
      });
    }
    const updateExtendedData = getEIsOnly(newReportType, originalReport);
    updateExtendedData.expReportTypeId = e.target.value;
    stateProps.updateReport({
      ...resetVendorData,
      ...resetCostCenterData,
      ...resetJobData,
      ...updateReceiptData,
      ...updateExtendedData,
    });
  },
  handleChangeCostCenter: () => {
    ownProps.onChangeEditingExpReport('report.costCenterHistoryId', null, true);
    ownProps.onChangeEditingExpReport('report.costCenterName', '', true);
  },
  handleChangeSubject: (e: any) => {
    ownProps.onChangeEditingExpReport('report.subject', e.target.value, true);
  },
  handleChangeRemarks: (e: any) => {
    ownProps.onChangeEditingExpReport('report.purpose', e.target.value, true);
  },
  handleChangeJob: () => {
    ownProps.onChangeEditingExpReport('report.jobId', null, true);
    ownProps.onChangeEditingExpReport('report.jobName', '', true);
  },
  handleClickCostCenterBtn: () => {
    dispatchProps.onClickCostCenterButton(
      ownProps.expReport.scheduledDate,
      stateProps.employeeId
    );
  },
  handleClickJobBtn: () => {
    dispatchProps.onClickJobButton(
      ownProps.expReport.scheduledDate,
      stateProps.employeeId
    );
  },
  renderScheduledDate: (disabled: boolean, errors: any, touched: any) => {
    return (
      <div className="ts-expenses__form-report-summary__form__scheduled-date">
        <div className="ts-text-field-container">
          <p className="key">
            <span className="is-required">*</span>
            &nbsp;{msg().Exp_Clbl_ScheduledDate}
          </p>
          <DateField
            value={DateUtil.format(
              ownProps.expReport.scheduledDate,
              'YYYY-MM-DD'
            )}
            onChange={(value: string) => {
              ownProps.onChangeEditingExpReport(
                'report.scheduledDate',
                value,
                true
              );
            }}
            disabled={disabled}
          />
          {errors.scheduledDate && touched.scheduledDate && (
            <div className="input-feedback">{msg()[errors.scheduledDate]}</div>
          )}
        </div>
      </div>
    );
  },
  onClickVendorSearch: () => {
    dispatchProps.openVendorLookupDialog(stateProps.employeeId);
  },
  searchVendorDetail: (vendorId) => {
    const isCached =
      stateProps.activeVendor && stateProps.activeVendor.id === vendorId;
    if (isCached) {
      return new Promise((resolve) =>
        resolve({ records: [stateProps.activeVendor] })
      );
    } else {
      return dispatchProps.searchVendorDetail(vendorId);
    }
  },
  onClickLookupEISearch: (item: EISearchObj) => {
    dispatchProps.openEILookupDialog(item, stateProps.employeeId);
  },
  setLinkedExpType: async (reportTypeId) => {
    const linkedExpType =
      (stateProps.reportTypeList.find((rt) => rt.id === reportTypeId) || {})
        .expTypeIds || [];
    await dispatchProps.setAvailableExpType(linkedExpType);
    ownProps.onChangeEditingExpReport('ui.availableExpType', linkedExpType);
  },
  onClickCloneButton: () => {
    dispatchProps.cloneReport(
      stateProps.selectedExpReport.reportId,
      stateProps.reportTypeList
    );
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ReportSummaryView): React.ComponentType<Object>);
