// @flow
import { connect } from 'react-redux';

import msg from '../../../commons/languages';

import { confirm } from '../../../commons/actions/app';
import { modes } from '../../modules/ui/expenses/mode';
import { actions as openTitleActions } from '../../modules/ui/expenses/recordListPane/summary/openTitle';
import { actions as overlapActions } from '../../modules/ui/expenses/overlap';
import { createNewExpReport } from '../../action-dispatchers/Requests';

import HeaderView from '../../../commons/components/exp/Header';

const mapStateToProps = (state) => ({
  isExpenseRequest: true,
  mode: state.ui.expenses.mode,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  defaultCostCenter: {
    costCenterCode: state.userSetting.costCenterCode,
    costCenterName: state.userSetting.costCenterName,
    costCenterHistoryId: state.userSetting.costCenterHistoryId,
  },
});

const mapDispatchToProps = {
  confirm,
  createNewExpReport,
  openTitle: openTitleActions.open,
  closeTitle: openTitleActions.close,
  backToHome: overlapActions.nonOverlapReport,
  moveToReport: overlapActions.overlapReport,
  moveToRecord: overlapActions.overlapRecord,
  closeRecord: overlapActions.nonOverlapRecord,
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickNewReportButton: () => {
    const callback = (yes: boolean) => {
      if (yes) {
        dispatchProps.openTitle();
        dispatchProps.createNewExpReport(
          stateProps.reportTypeList,
          stateProps.defaultCostCenter
        );
        dispatchProps.closeRecord();
        dispatchProps.moveToReport();
      }
    };

    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, callback);
    } else {
      callback(true);
    }
  },
  onClickNewRecordButton: () => {
    const callback = (yes: boolean) => {
      if (yes) {
        dispatchProps.createNewExpReport(
          stateProps.reportTypeList,
          stateProps.defaultCostCenter
        );
        dispatchProps.closeTitle();
        dispatchProps.closeRecord();
        dispatchProps.moveToReport();
      }
    };

    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, callback);
    } else {
      callback(true);
    }
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(HeaderView): React.ComponentType<Object>);
