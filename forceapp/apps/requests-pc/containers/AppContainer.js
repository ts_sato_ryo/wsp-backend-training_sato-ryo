/* @flow */
import { connect } from 'react-redux';

import App from '../components';

import { initialize } from '../action-dispatchers/app';

const mapStateToProps = (state) => ({
  userSetting: state.userSetting,
});

const mapDispatchToProps = {
  initialize,
};

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(App): React.ComponentType<Object>);
