/* @flow */
import { combineReducers } from 'redux';

import common from '../../commons/modules';
import userSetting from '../../commons/reducers/userSetting';
import ui from './ui';
import entities from './entities';

export default combineReducers<Object, Object>({
  common,
  userSetting,
  ui,
  entities,
});
