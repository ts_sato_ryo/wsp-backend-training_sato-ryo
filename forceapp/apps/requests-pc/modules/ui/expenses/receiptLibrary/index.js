/* @flow */
import { combineReducers } from 'redux';

import selectedReceipt from './selectedReceipt';

export default combineReducers<Object, Object>({
  selectedReceipt,
});
