// @flow
import { type Reducer } from 'redux';
import { type SelectedReceipt } from '../../../../../domain/models/exp/Receipt';

export const ACTIONS = {
  SET: 'MODULES/UI/EXP/RECEIPT_LIBRARY/SELECTED_RECEIPT/SET',
  CLEAR: 'MODULES/UI/EXP/RECEIPT_LIBRARY/SELECTED_RECEIPT/CLEAR',
};

export const actions = {
  set: (selectedReceipt: SelectedReceipt) => ({
    type: ACTIONS.SET,
    payload: selectedReceipt,
  }),
  clear: () => ({ type: ACTIONS.CLEAR }),
};

const initialState = {
  receiptId: '',
  receiptFileId: '',
  receiptData: '',
  ocrInfo: {},
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<Object, any>);
