// @flow

import { combineReducers } from 'redux';

import origin from './origin';
import viaList from './viaList';
import arrival from './arrival';

export default combineReducers<Object, Object>({
  origin,
  viaList,
  arrival,
});
