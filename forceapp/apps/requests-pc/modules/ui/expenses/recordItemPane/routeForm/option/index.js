// @flow

import { combineReducers } from 'redux';

import routeSort from './routeSort';
import seatPreference from './seatPreference';
import highwayBus from './highwayBus';
import useChargedExpress from './useChargedExpress';
import useExReservation from './useExReservation';

export default combineReducers<Object, Object>({
  routeSort,
  seatPreference,
  useChargedExpress,
  useExReservation,
  highwayBus,
});
