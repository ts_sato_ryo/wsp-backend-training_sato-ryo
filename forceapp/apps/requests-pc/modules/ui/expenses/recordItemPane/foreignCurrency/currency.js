// @flow
import type { Reducer } from 'redux';
import {
  searchCurrency,
  type CurrencyList,
} from '../../../../../../domain/models/exp/foreign-currency/Currency';
import {
  catchApiError,
  loadingEnd,
  loadingStart,
} from '../../../../../../commons/actions/app';

export const ACTIONS = {
  SEARCH_SUCCESS:
    'MODULES/EXPENSES/RECORD_ITEM_PANE/FOREIGN_CURRENCY/CURRENCY/SEARCH_SUCCESS',
};

const searchSuccess = (result: any) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: result.records,
});

export const actions = {
  search: (companyId: string) => (dispatch: Dispatch<any>): void | any => {
    let currencyRecords = [];
    dispatch(loadingStart());
    return searchCurrency(companyId)
      .then((res: any) => {
        currencyRecords = res.records;
        dispatch(loadingEnd());
        dispatch(searchSuccess(res));
        return currencyRecords;
      })
      .catch((err) => {
        dispatch(loadingEnd());
        dispatch(catchApiError(err, { isContinuable: true }));
        throw err;
      });
  },
};

// Reducer
const initialState = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_SUCCESS:
      return [...action.payload];
    default:
      return state;
  }
}: Reducer<CurrencyList, any>);
