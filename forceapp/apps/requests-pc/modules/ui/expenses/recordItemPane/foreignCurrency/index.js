// @flow

import { combineReducers } from 'redux';

import exchangeRate from './exchangeRate';
import currency from './currency';

export default combineReducers<Object, Object>({
  exchangeRate,
  currency,
});
