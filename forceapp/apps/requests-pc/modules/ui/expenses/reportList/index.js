/* @flow */
import { combineReducers } from 'redux';

import page from './page';
import sortBy from './sortBy';
import orderBy from './orderBy';

export default combineReducers<Object, Object>({
  page,
  sortBy,
  orderBy,
});
