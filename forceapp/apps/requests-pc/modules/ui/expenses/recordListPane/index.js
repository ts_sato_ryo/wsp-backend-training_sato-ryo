// @flow

import { combineReducers } from 'redux';

import summary from './summary';
import recordList from './recordList';

export default combineReducers<Object, Object>({
  summary,
  recordList,
});
