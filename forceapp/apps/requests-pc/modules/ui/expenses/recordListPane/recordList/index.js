// @flow

import { combineReducers } from 'redux';

import openEditMenu from './openEditMenu';

export default combineReducers<Object, Object>({
  openEditMenu,
});
