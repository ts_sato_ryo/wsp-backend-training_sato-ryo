// @flow
import type { Reducer } from 'redux';

import { type ExpenseTypeList } from '../../../../../../domain/models/exp/ExpenseType';

export const ACTIONS = {
  SET: 'MODULES/REQUESTS/DIALOG/EXPENSE_TYPE_SELECT/LIST/SET',
  SET_SEARCH: 'MODULES/REQUESTS/DIALOG/EXPENSE_TYPE_SELECT/LIST/SET_SEARCH',
  SET_RECENT: 'MODULES/REQUESTS/DIALOG/EXPENSE_TYPE_SELECT/LIST/SET_RECENT',
  CLEAR: 'MODULES/REQUEST/DIALOG/CLEAR',
};

export const actions = {
  set: (expenseTypeSelectList: ExpenseTypeList) => ({
    type: ACTIONS.SET,
    payload: expenseTypeSelectList,
  }),
  setSearchResult: (expenseTypeSelectList: ExpenseTypeList) => ({
    type: ACTIONS.SET_SEARCH,
    payload: expenseTypeSelectList,
  }),
  setRecentResult: (expenseTypeSelectList: ExpenseTypeList) => ({
    type: ACTIONS.SET_RECENT,
    payload: expenseTypeSelectList,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
type State = {
  selectionList: ExpenseTypeList,
  searchList: ExpenseTypeList,
  recentItems: ExpenseTypeList,
};
const initialState = { selectionList: [], searchList: [], recentItems: [] };

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return { ...state, selectionList: action.payload };
    case ACTIONS.SET_SEARCH:
      return { ...state, searchList: action.payload };
    case ACTIONS.SET_RECENT:
      return { ...state, recentItems: action.payload };
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<State, any>);
