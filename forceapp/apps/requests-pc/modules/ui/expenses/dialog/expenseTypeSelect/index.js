// @flow

import { combineReducers } from 'redux';

import list from './list';
import recordType from './recordType';

export default combineReducers<Object, Object>({
  list,
  recordType,
});
