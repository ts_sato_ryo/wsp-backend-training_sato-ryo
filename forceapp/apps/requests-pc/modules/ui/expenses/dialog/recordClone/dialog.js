// @flow
import type { Reducer } from 'redux';
import type { RecordClone } from '../../../../../../commons/components/exp/Form/Dialog/RecordClone/CloneDateSelection';

export const ACTIONS = {
  SET_RECORD: 'MODULES/EXPENSES/DIALOG/RECORD_CLONE/SET_RECORD',
  SET_DATE: 'MODULES/EXPENSES/DIALOG/RECORD_CLONE/SET_DATE',
};

export const actions = {
  setDate: (date: Array<string>) => ({
    type: ACTIONS.SET_DATE,
    payload: date,
  }),
  setRecord: (recordIds: Array<string>, latestDay: string) => ({
    type: ACTIONS.SET_RECORD,
    payload: { recordIds, latestDay },
  }),
};

//
// Reducer
//
const initialState: RecordClone = {
  dates: [],
  records: [],
  defaultDate: '',
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_DATE:
      return { ...state, dates: [...action.payload] };
    case ACTIONS.SET_RECORD:
      const { recordIds, latestDay } = action.payload;
      return { ...initialState, records: recordIds, defaultDate: latestDay };
    default:
      return state;
  }
}: Reducer<*, any>);
