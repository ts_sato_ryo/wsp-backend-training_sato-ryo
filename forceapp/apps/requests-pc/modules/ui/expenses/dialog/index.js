// @flow

import { combineReducers } from 'redux';

import activeDialog from './activeDialog';
import approval from './approval';
import costCenterSelect from './costCenterSelect';
import expenseTypeSelect from './expenseTypeSelect';
import extendedItem from './extendedItem';
import jobSelect from './jobSelect';
import vendor from './vendor';
import recordClone from './recordClone';
import recordUpdated from './recordUpdated';

export default combineReducers<Object, Object>({
  activeDialog,
  approval,
  costCenterSelect,
  expenseTypeSelect,
  extendedItem,
  jobSelect,
  vendor,
  recordClone,
  recordUpdated,
});
