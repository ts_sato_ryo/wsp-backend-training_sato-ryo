// @flow
import type { Reducer } from 'redux';

import { type VendorItem } from '../../../../../../domain/models/exp/Vendor';

export const ACTIONS = {
  SET: 'MODULES/EXPENSES/DIALOG/VENDOR/LIST/SET',
  CLEAR: 'MODULES/EXPENSES/DIALOG/VENDOR/LIST/CLEAR',
};

export const actions = {
  set: (item: VendorItem) => ({
    type: ACTIONS.SET,
    payload: item,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

//
// Reducer
//
const initialState = {
  active: false,
  address: '',
  bankAccountNumber: '',
  bankAccountType: '',
  bankCode: '',
  bankName: '',
  branchAddress: '',
  branchCode: '',
  branchName: '',
  code: '',
  companyId: '',
  correspondentBankAddress: '',
  correspondentBankName: '',
  correspondentBranchName: '',
  correspondentSwiftCode: '',
  country: '',
  currencyCode: '',
  id: '',
  isWithholdingTax: false,
  name: '',
  payeeName: '',
  paymentTerm: '',
  paymentTermCode: '',
  swiftCode: '',
  zipCode: '',
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<VendorItem, any>);
