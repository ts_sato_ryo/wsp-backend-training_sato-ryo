// @flow
import type { Reducer } from 'redux';

import { type JobList } from '../../../../../../domain/models/exp/Job';

export const ACTIONS = {
  SET: 'MODULES/REQUESTS/DIALOG/JOB_SELECT/LIST/SET',
  SET_SEARCH_RESULT: 'MODULES/REQUESTS/DIALOG/JOB_SELECT/LIST/SET_SEARCH',
  SET_RECENT: 'MODULES/REQUESTS/DIALOG/JOB_SELECT/LIST/SET_RECENT',
  CLEAR: 'MODULES/REQUEST/DIALOG/CLEAR',
};

type JobSelectList = Array<JobList>;

export const actions = {
  set: (jobSelectList: JobList) => ({
    type: ACTIONS.SET,
    payload: jobSelectList,
  }),
  setSearchResult: (jobSearchList: JobList) => ({
    type: ACTIONS.SET_SEARCH_RESULT,
    payload: jobSearchList,
  }),
  setRecentResult: (costCenterRecentItems: JobList) => ({
    type: ACTIONS.SET_RECENT,
    payload: costCenterRecentItems,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

type State = {
  selectionList: JobSelectList,
  searchList: JobSelectList,
  recentItems: JobSelectList,
};

const initialState = { selectionList: [], searchList: [], recentItems: [] };

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return { ...state, selectionList: action.payload };
    case ACTIONS.SET_SEARCH_RESULT:
      return { ...state, searchList: action.payload };
    case ACTIONS.SET_RECENT:
      return { ...state, recentItems: action.payload };
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<State, any>);
