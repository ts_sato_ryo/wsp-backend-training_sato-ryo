// @flow

import { combineReducers } from 'redux';

import mode from './mode';
import overlap from './overlap';
import tab from './tab';
import selectedExpReport from './selectedExpReport';
import recordListPane from './recordListPane';
import recordItemPane from './recordItemPane';
import receiptLibrary from './receiptLibrary';
import dialog from './dialog';
import reportList from './reportList';

export default combineReducers<Object, Object>({
  dialog,
  mode,
  tab,
  overlap,
  recordItemPane,
  recordListPane,
  reportList,
  selectedExpReport,
  receiptLibrary,
});
