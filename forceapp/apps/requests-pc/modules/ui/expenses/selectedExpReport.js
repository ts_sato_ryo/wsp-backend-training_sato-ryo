// @flow
import { type Reducer } from 'redux';

import {
  type Report,
  initialStatePreRequest,
} from '../../../../domain/models/exp/Report';
import type {
  ExpenseReportType,
  ExpenseReportTypeList,
} from '../../../../domain/models/exp/expense-report-type/list';
import { getEIsOnly } from '../../../../domain/models/exp/ExtendedItem';
import type { DefaultCostCenter } from '../../../../domain/models/exp/CostCenter';

type DefaultData = {
  reportType: ?ExpenseReportType,
  defaultCostCenter: ?DefaultCostCenter,
};

export const ACTIONS = {
  NEW_REPORT: 'MODULES/UI/EXPENSES/SELECTED_EXP_REPORT/NEW_REPORT',
  SELECT: 'MODULES/UI/EXPENSES/SELECTED_EXP_REPORT/SELECT',
  CLEAR: 'MODULES/UI/EXPENSES/SELECTED_EXP_REPORT/CLEAR',
};

export const actions = {
  newReport: (defaultData: DefaultData) => ({
    type: ACTIONS.NEW_REPORT,
    payload: defaultData,
  }),
  select: (target: Report, reportTypeList: ?ExpenseReportTypeList) => ({
    type: ACTIONS.SELECT,
    payload: { target, reportTypeList },
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

const getExtendedItemInfo = (reportType: ExpenseReportType) => {
  const extendedItemData = getEIsOnly(reportType);
  extendedItemData.expReportTypeId = reportType.id;
  return extendedItemData;
};

const reportIsReadOnly = (reportStatus) =>
  reportStatus !== 'Removed' && reportStatus !== 'NotRequested';

export default ((state = initialStatePreRequest, action) => {
  switch (action.type) {
    case ACTIONS.SELECT:
      const reportStatus = action.payload.target.status;
      // Updating Report is only performed for Editable Reports
      if (!reportIsReadOnly(reportStatus)) {
        // Check if need to use reportType. Only active one are passed to this
        if (
          action.payload.reportTypeList &&
          action.payload.reportTypeList.length > 0
        ) {
          // Check if existing report is already using ReportType
          if (action.payload.target.expReportTypeId) {
            // Find the Updated ReportType (if no longer active, then use the default (index 0)
            const reportTypeToUse = action.payload.reportTypeList.find(
              (reportType) =>
                reportType.id === action.payload.target.expReportTypeId
            );
            if (reportTypeToUse) {
              return {
                ...action.payload.target,
              };
            } else {
              // Report Type is no longer active. set it to false.
              return {
                ...action.payload.target,
                expReportTypeId: null,
              };
            }
          } else {
            return action.payload.target;
          }
        } else {
          // Previously using Report Type and now no more report type in the system (and it's non-readonly).
          return {
            ...action.payload.target,
            expReportTypeId: null,
          };
        }
      }
      return action.payload.target;
    case ACTIONS.CLEAR:
      return initialStatePreRequest;
    case ACTIONS.NEW_REPORT:
      if (action.payload.reportType) {
        const reportType = action.payload.reportType;
        return {
          ...initialStatePreRequest,
          ...getExtendedItemInfo(reportType),
          ...action.payload.defaultCostCenter,
          useFileAttachment: reportType.useFileAttachment,
        };
      } else {
        return {
          ...initialStatePreRequest,
          ...action.payload.defaultCostCenter,
        };
      }
    default:
      return state;
  }
}: Reducer<Report, any>);
