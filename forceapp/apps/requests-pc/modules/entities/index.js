/* @flow */
import { combineReducers } from 'redux';

import exp from '../../../domain/modules/exp';
import approval from '../../../domain/modules/approval';
import reportIdList from './reportIdList';

export default combineReducers<Object, Object>({
  exp,
  approval,
  reportIdList,
});
