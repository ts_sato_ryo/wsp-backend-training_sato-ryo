/* @flow */
import React from 'react';

import GlobalContainer from '../../commons/containers/GlobalContainer';
import GlobalHeader from '../../commons/components/GlobalHeader';
import msg from '../../commons/languages';

import RequestsContainer from '../containers/Requests/RequestsContainer';
import RequestsHeaderContainer from '../containers/Requests/HeaderContainer';
import ToastContainer from '../../commons/containers/ToastContainer';

import iconHeaderExp from '../images/Request.svg';

type Props = {
  userSetting: {
    id: string,
    employeeId: string,
    useExpense: boolean,
  },
  initialize: () => void,
};

export default class App extends React.Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    const { userSetting } = this.props;

    if (!userSetting.id) {
      return null;
    }

    return (
      <GlobalContainer>
        <GlobalHeader
          content={<RequestsHeaderContainer />}
          iconAssistiveText={msg().Exp_Lbl_Request}
          iconSrc={iconHeaderExp}
          iconSrcType="svg"
          showPersonalMenuPopoverButton={false}
          showProxyIndicator={false}
        />
        <RequestsContainer />
        <ToastContainer />
      </GlobalContainer>
    );
  }
}
