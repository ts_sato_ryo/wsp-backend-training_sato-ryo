import React from 'react';

import ReportListContainer from '../../containers/Requests/ReportListContainer';
import FormContainer from '../../containers/Requests/FormContainer';
import ReportSummaryContainer from '../../containers/Requests/ReportSummaryContainer';
import RecordListContainer from '../../containers/Requests/RecordListContainer';
import RecordItemContainer from '../../containers/Requests/RecordItemContainer';
import BaseCurrencyContainer from '../../containers/Requests/BaseCurrencyContainer';
import ForeignCurrencyContainer from '../../containers/Requests/ForeignCurrencyContainer';
import RouteFormContainer from '../../containers/Requests/RouteFormContainer';
import SuggestContainer from '../../containers/Requests/SuggestContainer';
import DialogContainer from '../../containers/Requests/DialogContainer';

import './index.scss';

export default function expenseRequestHOC(WrappedComponent) {
  return class withRequestHOC extends React.Component {
    render() {
      return (
        <WrappedComponent
          {...this.props}
          baseCurrency={BaseCurrencyContainer}
          dialog={DialogContainer}
          fetchExpReportList={this.props.fetchExpReportList}
          foreignCurrency={ForeignCurrencyContainer}
          form={FormContainer}
          recordItem={RecordItemContainer}
          recordList={RecordListContainer}
          reportList={ReportListContainer}
          reportSummary={ReportSummaryContainer}
          routeForm={RouteFormContainer}
          suggest={SuggestContainer}
        />
      );
    }
  };
}
