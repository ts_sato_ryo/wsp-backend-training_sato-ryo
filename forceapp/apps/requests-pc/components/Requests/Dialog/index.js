// @flow
import React from 'react';
import last from 'lodash/last';

import { dialogTypes } from '../../../modules/ui/expenses/dialog/activeDialog';

import Approval from '../../../containers/Requests/Dialogs/ApprovalDialogContainer';
import ApprovalHistory from '../../../containers/Requests/Dialogs/ApprovalHistoryDialogContainer';
import CostCenterSelect from '../../../containers/Requests/Dialogs/CostCenterDialogContainer';
import ExpenseTypeSelect from '../../../containers/Requests/Dialogs/ExpenseTypeDialogContainer';
import RecordItemsDialog from '../../../containers/Requests/Dialogs/RecordItemsDialogContainer';
import JobSelect from '../../../containers/Requests/Dialogs/JobDialogContainer';
import RouteSelect from '../../../containers/Requests/Dialogs/RouteSelectDialogContainer';
import ReceiptLibrary from '../../../containers/Requests/Dialogs/ReceiptLibraryDialogContainer';
import EILookupDialog from '../../../containers/Requests/Dialogs/ExtendedItemDialogContainer';
import VendorLookupDialog from '../../../containers/Requests/Dialogs/VendorDialogContainer';
import RecordCloneDateDialog from '../../../containers/Requests/Dialogs/RecordCloneDialogContainer';
import RecordUpdatedDialog from '../../../containers/Requests/Dialogs/RecordUpdateInfoDialogContainer';

type Props = {
  activeDialog: Array<string>,
  hideDialog: () => void,
  clearDialog: () => void,
  onClickHideDialogButton: () => void,
};

export const getSelectedExpDialogComponent = (
  currentDialog: string,
  props: Props
) => {
  switch (currentDialog) {
    case dialogTypes.APPROVAL:
    case dialogTypes.CANCEL_REQUEST:
      return <Approval {...props} />;
    case dialogTypes.APPROVAL_HISTORY:
      return <ApprovalHistory {...props} />;
    case dialogTypes.COST_CENTER:
      return <CostCenterSelect {...props} />;
    case dialogTypes.EI_LOOKUP:
      return <EILookupDialog {...props} />;
    case dialogTypes.EXPENSE_TYPE:
    case dialogTypes.EXPENSE_TYPE_CHANGE:
      return <ExpenseTypeSelect {...props} />;
    case dialogTypes.JOB:
      return <JobSelect {...props} />;
    case dialogTypes.RECORD_ITEMS_CREATE:
    case dialogTypes.RECORD_ITEMS_CONFIRM:
    case dialogTypes.RECORD_ITEMS_DELETE:
      return <RecordItemsDialog {...props} />;
    case dialogTypes.RECORD_CLONE_DATE:
      return <RecordCloneDateDialog {...props} />;
    case dialogTypes.RECORD_UPDATED:
      return <RecordUpdatedDialog {...props} />;
    case dialogTypes.RECEIPTS:
      return <ReceiptLibrary {...props} />;
    case dialogTypes.ROUTE_SELECT:
      return <RouteSelect {...props} />;
    case dialogTypes.VENDOR_LOOKUP:
      return <VendorLookupDialog {...props} />;
    default:
      return null;
  }
};

const ExpDialog = (props: Props) => {
  const currentDialog = last(props.activeDialog);
  return getSelectedExpDialogComponent(currentDialog, props);
};

export default ExpDialog;
