// @flow

import { combineReducers } from 'redux';
import summary from './summary';

export default combineReducers<Object, Object>({
  summary,
});
