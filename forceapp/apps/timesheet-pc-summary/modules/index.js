// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../commons/utils/TypeUtil';
import common from '../../commons/reducers';
import userSetting from '../../commons/reducers/userSetting';
import entities from './entities';

const reducers = {
  common,
  userSetting,
  entities,
};

export type State = $State<typeof reducers>;

export default combineReducers<Object, Object>(reducers);
