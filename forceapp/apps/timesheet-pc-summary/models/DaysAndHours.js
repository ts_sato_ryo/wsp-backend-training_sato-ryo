// @flow

export type DaysAndHours = {|
  /**
   * 日数
   */
  days: number,

  /**
   * 時間
   */
  hours: ?number,

  /**
   * 単位
   */
  unit: ?string,
|};
