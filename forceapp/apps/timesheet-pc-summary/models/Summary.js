// @flow

import type { Status } from './Status';
import type { SummaryBlock } from './SummaryBlock';
import type { Record } from './Record';

export type Summary = {|
  summaryName: string,
  status: Status,
  hasCalculatedAbsence: boolean,
  departmentName: string,
  workingTypeName: string,
  employeeCode: string,
  employeeName: string,
  records: Record[],
  summaries: SummaryBlock[],
|};
