// @flow

export type StatusType = {|
  Approved: 'Approved',
  Pending: 'Pending',
|};

export type Status = $Values<StatusType>;
