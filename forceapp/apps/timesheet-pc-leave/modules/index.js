import { combineReducers } from 'redux';

import common from '../../commons/reducers';
import userSetting from '../../commons/reducers/userSetting';
import entities from './entities';

export default combineReducers({
  common,
  userSetting,
  entities,
});
