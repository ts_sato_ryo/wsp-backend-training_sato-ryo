import Api, { ErrorResponse } from '../../../../../../__tests__/mocks/ApiMock';
import DispatcherMock from '../../../../../../__tests__/mocks/DispatcherMock';

import reducer, { constants, actions, selectors } from '../index';

import dummyResponse from '../../../../../repositories/__tests__/mocks/response/att-leave-info-get';

describe('actions', () => {
  describe('.fetch(targetDate, targetEmployeeId)', () => {
    describe('Success', () => {
      const dispatchMock = new DispatcherMock();

      beforeAll(() => {
        Api.setDummyResponse(
          '/att/leave-info/get',
          {
            targetDate: '2018-01-01',
            empId: null,
          },
          dummyResponse
        );

        return actions.fetch('2018-01-01')(dispatchMock.dispatch);
      });

      test('1. LOADING_START', () => {
        expect(dispatchMock.logged[0].type).toBe('LOADING_START');
      });

      test('2. MODULES/ENTITIES/LEAVE_INFO/FETCH_SUCCESS', () => {
        expect(dispatchMock.logged[1].type).toBe(
          'MODULES/ENTITIES/LEAVE_INFO/FETCH_SUCCESS'
        );
      });

      test('3. LOADING_END', () => {
        expect(dispatchMock.logged[2].type).toBe('LOADING_END');
      });
    });

    describe('Error', () => {
      const dispatchMock = new DispatcherMock();

      beforeAll(() => {
        Api.setDummyResponse(
          '/att/leave-info/get',
          {
            targetDate: '2018-01-01-Error',
            empId: null,
          },
          new ErrorResponse({ message: 'Dummy Message' })
        );

        return actions.fetch('2018-01-01-Error')(dispatchMock.dispatch);
      });

      test('1. LOADING_START', () => {
        expect(dispatchMock.logged[0].type).toBe('LOADING_START');
      });

      test('2. CATCH_API_ERROR', () => {
        expect(dispatchMock.logged[1].type).toBe('CATCH_API_ERROR');
      });

      test('3. LOADING_END', () => {
        expect(dispatchMock.logged[2].type).toBe('LOADING_END');
      });
    });
  });
});

describe('selectors', () => {
  describe('.leaveDetailsSelector(state)', () => {
    const normalizedState = {
      entities: {
        leaveInfo: {
          leaveDetails: {
            allIds: ['002', '001'],
            byId: {
              '001': {
                name: '年次有給休暇',
                startDate: '2018-01-01',
              },
              '002': {
                name: '年次有給休暇',
                startDate: '2018-01-02',
              },
            },
          },
        },
      },
    };

    test('returns denormalized list', () => {
      expect(selectors.leaveDetailsSelector(normalizedState)).toEqual([
        { name: '年次有給休暇', startDate: '2018-01-02' },
        { name: '年次有給休暇', startDate: '2018-01-01' },
      ]);
    });
  });
});

describe('reducer', () => {
  const stateKeys = [
    'annualLeave',
    'departmentName',
    'employeeCode',
    'employeeName',
    'leaveDetails',
    'paidManagedLeave',
    'period',
    'unpaidManagedLeave',
    'workingTypeName',
  ];

  describe('(initial)', () => {
    test('returns correct shape', () => {
      const initialSate = reducer(undefined, {});
      expect(Object.keys(initialSate).sort()).toEqual(stateKeys);
    });
  });

  describe('action.type === FETCH_SUCCESS', () => {
    test('replaces all field', () => {
      const payload = {};
      stateKeys.forEach((key) => {
        payload[key] = 'dummy';
      });

      const action = {
        type: constants.FETCH_SUCCESS,
        payload,
      };
      const nextState = reducer({}, action);

      expect(nextState).not.toBe(payload);
      expect(nextState).toEqual(payload);
    });
  });
});
