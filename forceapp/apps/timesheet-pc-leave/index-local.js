// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import AppContainer from './containers/AppContainer';
import { actions as appActions } from './modules/app';

import onDevelopment from '../commons/config/development';

import '../commons/styles/base.scss';
import '../commons/config/moment';

function renderApp(store, Component): void {
  const container = document.getElementById('container');
  if (container) {
    onDevelopment(() => {
      ReactDOM.render(
        <Provider store={store}>
          <Component />
        </Provider>,
        container
      );
    });
  }
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = () => {
  const configuredStore = configureStore();
  renderApp(configuredStore, AppContainer);
  configuredStore.dispatch(appActions.initialize());
};
