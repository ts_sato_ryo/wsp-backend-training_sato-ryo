/* @flow */

export type LeaveRange = 'Day' | 'AM' | 'PM' | 'Half' | 'Time';

// Express an entity as a type alias unless it has some methods
export type LeaveDetail = {
  requestId: string,
  startDate: string,
  endDate: string,
  name: string,
  range: LeaveRange,
  days: ?number,
  leaveTime: ?number,
  remarks: ?string,
};

export type GrantedAndTaken = {
  validDateFrom: string,
  validDateTo: string,
  daysGranted: number,
  daysTaken: number,
  hoursTaken: number,
  daysLeft: number,
  hoursLeft: number,
  comment: ?string,
};

// export type TotalGrantedAndTaken = {
// };

export type DaysManagedLeave = {
  leaveName: ?string,
  leaveType: 'Paid' | 'Unpaid',
  daysGrantedTotal: number,
  daysTakenTotal: number,
  hoursTakenTotal: number,
  daysLeftTotal: number,
  hoursLeftTotal: number,
  grants: GrantedAndTaken[],
};
