/* @flow */
import React from 'react';
import typeof SyntheticEvent from 'react-dom';

import GlobalContainer from '../../commons/containers/GlobalContainer';
import PopupWindowNavbar from '../../commons/components/PopupWindowNavbar';
import PopupWindowPage from '../../commons/components/PopupWindowPage';
import Button from '../../commons/components/buttons/Button';

import Header, { type Props as HeaderProps } from './Header';
import DetailList, { type Props as DetailListProps } from './DetailList';
import DaysManagedSection from './DaysManagedSection';

import msg from '../../commons/languages';
import type { DaysManagedLeave } from '../models/types';

// Use union type
export type Props = HeaderProps &
  DetailListProps & {
    annualLeave: DaysManagedLeave,
    paidManagedLeave: DaysManagedLeave[],
    unpaidManagedLeave: DaysManagedLeave[],

    // Use SyntheticEvent to express React events
    onClickPrintButton: SyntheticEvent => void,
  };

export default class App extends React.Component<Props> {
  render() {
    return (
      <GlobalContainer>
        <PopupWindowNavbar title={msg().Att_Lbl_LeaveDetails}>
          <Button type="text" onClick={this.props.onClickPrintButton}>
            {msg().Com_Btn_Print}
          </Button>
        </PopupWindowNavbar>
        <PopupWindowPage>
          <Header
            period={this.props.period}
            departmentName={this.props.departmentName}
            workingTypeName={this.props.workingTypeName}
            employeeCode={this.props.employeeCode}
            employeeName={this.props.employeeName}
          />

          <DaysManagedSection
            annualLeave={this.props.annualLeave}
            paidManagedLeave={this.props.paidManagedLeave}
            unpaidManagedLeave={this.props.unpaidManagedLeave}
          />

          <DetailList leaveDetails={this.props.leaveDetails} />
        </PopupWindowPage>
      </GlobalContainer>
    );
  }
}
