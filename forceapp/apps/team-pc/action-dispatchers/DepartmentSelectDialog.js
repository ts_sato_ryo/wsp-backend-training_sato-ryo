// @flow

import { type Dispatch } from 'redux';
import isNil from 'lodash/isNil';

import {
  defaultValue as emptyDepartment,
  type Department,
} from '../../domain/models/organization/Department';
import msg from '../../commons/languages';
import { withLoading } from '../../commons/actions/app';
import {
  actions,
  type Item,
  type ItemList,
} from '../modules/ui/attRequestStatus/departmentSelectDialog';
import Repository from '../../repositories/DepartmentRepository';

const search = (
  targetDate: string,
  companyId: string,
  parent: null | Item
): Promise<Department[]> =>
  Repository.search({
    targetDate,
    companyId,
    parentId: parent !== null ? parent.id : undefined,
  });

export const initialize = (targetDate: string, companyId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(
    withLoading(() =>
      search(targetDate, companyId, null).then((records) => {
        if (records.length > 0) {
          const departments = records
            .filter((record) => isNil(record.parentId))
            .map((record) => ({
              ...record,
              isGroup: false,
            }));
          dispatch(
            actions.searchSuccess([
              [
                ...departments,
                {
                  ...emptyDepartment,
                  isGroup: false,
                  name: msg().Team_Lbl_EmptyDepartment,
                },
              ],
            ])
          );
        }
      })
    )
  );
};

export const openDialog = (targetDate: string, companyId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(initialize(targetDate, companyId));
  dispatch(actions.openDialog());
};

/**
 * Retrieve departments and build list item for select dialog
 */
export const searchDepartmentItemLists = (
  targetDate: string,
  companyId: string,
  parent: null | Item,
  ancestors: ItemList[] = []
) => (dispatch: Dispatch<any>) => {
  dispatch(
    withLoading(() =>
      search(targetDate, companyId, parent).then((records) => {
        if (records.length > 0) {
          const parentId = parent !== null ? parent.id : null;
          const children = records
            .filter((record) => record.parentId === parentId)
            .map((record) => ({
              ...record,
              isGroup: false,
            }));
          const departments = [...ancestors, [...children]];

          dispatch(actions.searchSuccess(departments));
        }
      })
    )
  );
};
