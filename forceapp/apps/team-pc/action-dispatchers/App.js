// @flow

import type { Permission } from '../../domain/models/access-control/Permission';

import { withLoading } from '../../commons/actions/app';
import { getUserSetting } from '../../commons/actions/userSetting';
import { setUserPermission } from '../../commons/modules/accessControl/permission';
import { load } from './AttRequestStatus';

/**
 * チーム画面を初期化する
 */
// eslint-disable-next-line import/prefer-default-export
export const initialize = (param?: { userPermission: Permission }) => (
  dispatch: Dispatch<any>
) => {
  if (param) {
    dispatch(setUserPermission(param.userPermission));
  }

  return dispatch(
    withLoading(
      () => dispatch(getUserSetting()),
      (userSetting) => {
        if (!userSetting) {
          return Promise.reject();
        }
        return dispatch(load(userSetting.departmentId, null));
      }
    )
  );
};
