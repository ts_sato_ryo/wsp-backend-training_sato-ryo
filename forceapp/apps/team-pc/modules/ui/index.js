// @flow

import { combineReducers } from 'redux';

import attRequestStatus from './attRequestStatus';

import { type $State } from '../../../commons/utils/TypeUtil';

const rootReducer = {
  attRequestStatus,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
