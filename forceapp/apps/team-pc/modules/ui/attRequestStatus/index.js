// @flow

import { combineReducers } from 'redux';

import periods from './periods';
import table from './table';
import departmentSelectDialog from './departmentSelectDialog';

export default combineReducers<Object, Object>({
  periods,
  table,
  departmentSelectDialog,
});
