// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../commons/utils/TypeUtil';

import common from '../../commons/reducers';
import entities from './entities';
import ui from './ui';

const rootReducer = {
  common,
  entities,
  ui,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
