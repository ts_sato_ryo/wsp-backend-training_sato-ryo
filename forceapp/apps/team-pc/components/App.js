// @flow
import React from 'react';

import GlobalContainer from '../../commons/containers/GlobalContainer';
import AttRequestStatusContainer from '../containers/AttRequestStatusContainer';
import GlobalHeader from '../../commons/components/GlobalHeader';

import imgIconHeader from '../images/Team.svg';

import msg from '../../commons/languages';

type Props = $ReadOnly<{||}>;

export default class App extends React.Component<Props> {
  render() {
    return (
      <GlobalContainer>
        <GlobalHeader
          iconSrc={imgIconHeader}
          iconSrcType="svg"
          iconAssistiveText={msg().Att_Lbl_TimeAttendance}
          showPersonalMenuPopoverButton={false}
          showProxyIndicator={false}
        />
        <AttRequestStatusContainer />
      </GlobalContainer>
    );
  }
}
