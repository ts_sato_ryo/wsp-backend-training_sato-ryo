// @flow

import * as React from 'react';

import msg from '../../../commons/languages';
import { type Department } from '../../../domain/models/organization/Department';
import { type ItemList } from '../../modules/ui/attRequestStatus/departmentSelectDialog';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import MultiColumnFinder from '../../../commons/components/dialogs/MultiColumnFinder';

import './DepartmentSelectDialog.scss';

const ROOT = 'team-pc-att-request-status-department-select-dialog';

export type Props = $ReadOnly<{
  isOpened: boolean,
  items: ItemList[],

  onClickItem: (item: Department) => void,
  onClickCloseButton: () => void,
}>;

export default class DepartmentSelectDialog extends React.Component<Props> {
  render() {
    return (
      <React.Fragment>
        {this.props.isOpened && (
          <DialogFrame className={ROOT} hide={this.props.onClickCloseButton}>
            <MultiColumnFinder
              items={this.props.items}
              typeName={msg().Team_Lbl_Department}
              showHistory={false}
              showFavorites={false}
              parentSelectable
              onClickItem={this.props.onClickItem}
              onClickCloseButton={this.props.onClickCloseButton}
            />
          </DialogFrame>
        )}
      </React.Fragment>
    );
  }
}
