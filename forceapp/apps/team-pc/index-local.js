// @flow
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { type Store } from 'redux';

import type { Permission } from '../domain/models/access-control/Permission';

import configureStore from './store/configureStore';

import onDevelopment from '../commons/config/development';

import AppContainer from './containers/AppContainer';
import * as AppActions from './action-dispatchers/App';

import '../commons/styles/base.scss';
import '../commons/config/moment';

const renderApp = (store, Component: React.ComponentType<*>) => {
  const container = document.getElementById('container');
  if (container !== null) {
    onDevelopment(() => {
      ReactDOM.render(
        <Provider store={store}>
          <Component />
        </Provider>,
        container
      );
    });
  }
};

// eslint-disable-next-line import/prefer-default-export
export const startApp = (param?: { userPermission: Permission }) => {
  const configuredStore: Store<Object, Object> = configureStore();
  renderApp(configuredStore, AppContainer);
  configuredStore.dispatch(AppActions.initialize(param));
};
