// @flow

import * as React from 'react';
import { Provider } from 'react-redux';
import styled from 'styled-components';
import configureStore from './store/configureStore';

import App from './containers/TrackSummary';
import AppActions from './action-dispatchers/App';
import Notification from './containers/NotificationContainer';
import Loading from './containers/LoadingContainer';
import BlockScreen from './containers/BlockScreenContainer';

const Container = styled.div`
  position: relative;
  margin: ${({ margin }) => margin || '0'};
`;

const configuredStore = configureStore();

const initialize = (Component: React.ComponentType<*>, displayName: string) => {
  const Com = (props: Object) => {
    React.useEffect(() => {
      AppActions(configuredStore.dispatch).initialize();
    }, []);

    return (
      <Provider store={configuredStore}>
        <Container {...props}>
          <Loading />
          <Notification />
          <BlockScreen />
          <Component {...props} />
        </Container>
      </Provider>
    );
  };
  Com.displayName = displayName;

  return Com;
};

export default {
  Approval: initialize(App.Approval, 'TrackSummary.Approval'),
  Request: initialize(App.Request, 'TrackSummary.Request'),
};
