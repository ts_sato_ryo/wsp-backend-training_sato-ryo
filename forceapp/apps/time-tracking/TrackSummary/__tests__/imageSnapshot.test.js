// @flow

import path from 'path';
import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';

initStoryshots({
  suite: 'TrackSummary: Image storyshots',
  configPath: path.resolve(__dirname, 'config.js'),
  test: imageSnapshot({
    storybookUrl: `file://${path.resolve('./storybook-static')}`,
  }),
});
