// @flow

import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { actions as requestActions } from '../modules/ui/request';
import { selectors as historyListSelectors } from '../../../../widgets/dialogs/ApprovalHistoryDialog/modules/entities/historyList';
import HistoryDialog from '../../../../widgets/dialogs/ApprovalHistoryDialog/components/Dialog';

const HistoryDialogContainer = () => {
  const historyList = useSelector(historyListSelectors.historyListSelector);
  const isOpen = useSelector((state) => state.ui.request.isOpenHistoryDialog);
  const dispatch = useDispatch();
  const onHide = useCallback(
    () => dispatch(requestActions.update({ isOpenHistoryDialog: false })),
    []
  );

  return (
    <>{isOpen && <HistoryDialog historyList={historyList} onHide={onHide} />}</>
  );
};

export default HistoryDialogContainer;
