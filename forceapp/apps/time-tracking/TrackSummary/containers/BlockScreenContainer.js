// @flow

import React from 'react';
import { useSelector } from 'react-redux';

import BlockScreen from '../components/BlockScreen';

import type { State } from '../modules';

const mapStateToProps = (state: State) => ({
  active: state.ui.blocking.enabled,
});

const BlockScreenContainer = () => {
  const props = useSelector(mapStateToProps);

  return <BlockScreen {...props} />;
};

export default BlockScreenContainer;
