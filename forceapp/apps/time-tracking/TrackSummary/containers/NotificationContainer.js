// @flow

import React, { useCallback, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import App from '../action-dispatchers/App';
import Toast from '../../../commons/components/Toast';
import type { State } from '../modules';

const useUnhandledRejectionCallback = (
  callback: (event: PromiseRejectionEvent) => void,
  dependencyList: $ReadOnlyArray<any>
) => {
  useEffect(() => {
    window.addEventListener('unhandledrejection', callback);
    return () => window.removeEventListener('unhandledrejection', callback);
  }, dependencyList);
};

const mapStateToProps = (state: State) => ({
  isShow: state.toast.isShow,
  message: state.toast.message,
  variant: state.toast.variant,
});

const NotificationContainer = () => {
  const props = useSelector(mapStateToProps);
  const dispatch = useDispatch();
  const app = useMemo(() => App(dispatch), [dispatch]);

  useUnhandledRejectionCallback((event: PromiseRejectionEvent) => {
    if (event.reason.type === 'WSP_PRODUCTION_APEX_ERROR') {
      app.showErrorNotification(event.reason.event.message);
    } else {
      app.showErrorNotification(event.reason.message);
    }

    event.preventDefault();
    event.stopPropagation();
  }, []);
  const onClick = useCallback(() => app.hideErrorNotification(), []);
  const onExit = useCallback(() => app.resetErrorNotification(), []);

  return <Toast {...props} onClick={onClick} onExit={onExit} />;
};

export default NotificationContainer;
