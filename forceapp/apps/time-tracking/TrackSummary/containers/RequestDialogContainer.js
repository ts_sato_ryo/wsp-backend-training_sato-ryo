// @flow

import React, { useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import type { State } from '../modules';
import type { UserSetting } from '../../../domain/models/UserSetting';

import Request from '../action-dispatchers/Request';

import RequestDialog from '../components/RequestDialog';

const mapStateToProps = (state: State) => ({
  approverName: state.entities.summary.request.approverName,
  status: state.entities.summary.request.status,
  userPhotoUrl: (state.userSetting: UserSetting).photoUrl,
  comment: state.ui.request.comment,
});

const RequestDialogContainer = () => {
  const props = useSelector(mapStateToProps);
  const isOpen = useSelector((state) => state.ui.request.isOpen);
  const targetDate = useSelector((state) => state.entities.request.targetDate);
  const requestId = useSelector(
    (state) => state.entities.summary.request.requestId
  );
  const dispatch = useDispatch();
  const request = useMemo(() => Request(dispatch), [dispatch]);

  const onChangeComment = useCallback(
    (value: string) => request.editComment(value),
    [props.comment]
  );
  const onClickClose = useCallback(() => request.close(), [isOpen]);
  const onClickSubmit = useCallback(async () => {
    await request.applyForTimeTrackReport({
      status: props.status,
      comment: props.comment,
      targetDate,
      requestId,
    });
  }, [props.status, props.comment, targetDate, requestId]);

  return (
    <>
      {isOpen && (
        <RequestDialog
          {...props}
          onChangeComment={onChangeComment}
          onClickClose={onClickClose}
          onClickSubmit={onClickSubmit}
        />
      )}
    </>
  );
};

export default RequestDialogContainer;
