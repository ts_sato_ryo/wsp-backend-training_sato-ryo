// @flow

import React, { useEffect, useMemo, useCallback, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Request from '../../action-dispatchers/Request';
import TrackSummary from '../../components/TrackSummary';
import {
  TIME_TRACK_UPDATED,
  DEFAULT_JOB_UPDATED,
} from '../../../../commons/constants/customEventName';

import type { State } from '../../modules';

type OwnProps = $ReadOnly<{||}>;

const mapStateToProps = (state: State) => ({
  data: state.entities.summary.taskSummaryRecords,
  startDate: state.entities.summary.startDate,
  endDate: state.entities.summary.endDate,
  targetDate: state.entities.request.targetDate,
  status: state.entities.summary.request.status,
  useRequest: state.entities.summary.useRequest,
});

const RequestContainer = (_ownProps: OwnProps) => {
  const { targetDate, ...props } = useSelector(mapStateToProps);
  const requestId = useSelector(
    (state) => state.entities.summary.request.requestId
  );

  const dispatch = useDispatch();
  const request = useMemo(() => Request(dispatch), [dispatch]);

  const openHistoryDialog = useCallback(() => {
    request.openHistoryDialog(requestId);
  }, [requestId]);

  useEffect(() => {
    const initialize = () => request.initialize(targetDate);
    window.addEventListener(TIME_TRACK_UPDATED, initialize);
    return () => {
      window.removeEventListener(TIME_TRACK_UPDATED, initialize);
    };
  }, [targetDate]);

  useEffect(() => {
    const initialize = () => request.initialize(targetDate);
    window.addEventListener(DEFAULT_JOB_UPDATED, initialize);
    return () => {
      window.removeEventListener(DEFAULT_JOB_UPDATED, initialize);
    };
  }, [targetDate]);

  useEffect(() => {
    request.initialize();
  }, []);

  const [isOpen, setIsOpen] = useState(false);

  return (
    <TrackSummary.Request
      {...props}
      isOpen={isOpen}
      onClickOpen={() => setIsOpen(!isOpen)}
      openHistoryDialog={openHistoryDialog}
    />
  );
};

export default RequestContainer;
