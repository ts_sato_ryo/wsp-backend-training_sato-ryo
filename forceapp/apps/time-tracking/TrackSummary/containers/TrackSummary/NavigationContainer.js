// @flow

import React, { useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Navigation from '../../components/TrackSummary/Navigation';
import Request from '../../action-dispatchers/Request';

import type { State } from '../../modules';

const mapStateToProps = (state: State) => ({
  startDate: state.entities.summary.startDate,
  endDate: state.entities.summary.endDate,
});

const NavigationContainer = () => {
  const props = useSelector(mapStateToProps);
  const dispatch = useDispatch();

  const request = useMemo(() => Request(dispatch), [dispatch]);
  const onClickCurrentPeriod = useCallback(() => {
    request.loadCurrentPeriodSummary();
  }, []);
  const onClickNext = useCallback(() => {
    request.loadNextPeriodSummary({
      startDate: props.startDate,
      endDate: props.endDate,
    });
  }, [props.startDate, props.endDate]);
  const onClickPrev = useCallback(() => {
    request.loadPrevPeriodSummary({
      startDate: props.startDate,
      endDate: props.endDate,
    });
  }, [props.startDate, props.endDate]);

  return (
    <Navigation
      {...props}
      onClickCurrent={onClickCurrentPeriod}
      onClickNext={onClickNext}
      onClickPrev={onClickPrev}
    />
  );
};

export default NavigationContainer;
