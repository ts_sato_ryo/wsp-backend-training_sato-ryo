// @flow

import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import TrackSummary from '../../components/TrackSummary';
import Approval from '../../action-dispatchers/Approval';
import type { State } from '../../modules/index';

type OwnProps = $ReadOnly<{|
  id: string,
|}>;

const mapStateToProps = (state: State) => ({
  data: state.entities.requestSummary.taskSummaryRecords,
  startDate: state.entities.requestSummary.startDate,
  endDate: state.entities.requestSummary.endDate,
});

const ApprovalContainer = (ownProps: OwnProps) => {
  const props = useSelector(mapStateToProps);

  const dispatch = useDispatch();
  useEffect(() => {
    Approval(dispatch).initialize(ownProps.id);
  }, [ownProps.id]);

  return <TrackSummary.Approval {...props} />;
};

export default ApprovalContainer;
