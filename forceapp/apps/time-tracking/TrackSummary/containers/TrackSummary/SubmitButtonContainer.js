// @flow

import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import type { State } from '../../modules';

import RequestDispatcher from '../../action-dispatchers/Request';

import SubmitButton from '../../components/TrackSummary/SubmitButton';

const mapStateToProps = (state: State) => ({
  disabled: false,
  status: state.entities.summary.request.status,
});

const SubmitButtonContainer = () => {
  const props = useSelector(mapStateToProps);
  const dispatch = useDispatch();
  const Request = RequestDispatcher(dispatch);

  const onClick = useCallback(() => {
    Request.open();
  });

  return <SubmitButton {...props} onClick={onClick} />;
};

export default SubmitButtonContainer;
