// @flow

import Approval from './ApprovalContainer';
import Request from './RequestContainer';

const TrackSummaryContainer = {
  Approval,
  Request,
};

export default TrackSummaryContainer;
