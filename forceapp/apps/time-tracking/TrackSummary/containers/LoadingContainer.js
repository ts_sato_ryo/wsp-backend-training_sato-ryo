// @flow

import React from 'react';
import { useSelector } from 'react-redux';

import Spinner from '../../../commons/components/Spinner';

import type { State } from '../modules';

const mapStateToProps = (state: State) => ({
  loading: state.app.loadingDepth > 0,
});

const LoadingContainer = () => {
  const props = useSelector(mapStateToProps);

  return <Spinner {...props} priority="low" />;
};

export default LoadingContainer;
