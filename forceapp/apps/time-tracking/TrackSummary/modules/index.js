// @flow

import { combineReducers } from 'redux';

import app from '../../../commons/modules/app';
import entities from './entities';
import ui from './ui';
import userSetting from '../../../commons/reducers/userSetting';
import toast from '../../../commons/modules/toast';
import widgets from './widgets';

import { type $State } from '../../../commons/utils/TypeUtil';

const rootReducer = {
  app,
  entities,
  userSetting,
  ui,
  toast,
  widgets,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, typeof rootReducer>(rootReducer);
