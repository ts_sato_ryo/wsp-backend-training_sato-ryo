// @flow
import defaultSummary, {
  type Summary,
} from '../../../../domain/models/time-tracking/Summary';

type State = Summary;

const initialState: State = defaultSummary;

export const FETCH_SUCCESS = 'TRACK_SUMMARY/ENTITIES/SUMMARY/FETCH_SUCCESS';

type FetchSuccess = {
  type: 'TRACK_SUMMARY/ENTITIES/SUMMARY/FETCH_SUCCESS',
  payload: Summary,
};

export const actions = {
  fetchSuccess: (summary: Summary): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: summary,
  }),
};

type Action = FetchSuccess;

export default function reducer(
  state: State = initialState,
  action: Action
): State {
  switch (action.type) {
    case FETCH_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}
