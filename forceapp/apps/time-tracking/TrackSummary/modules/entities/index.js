// @flow

import { combineReducers } from 'redux';

import summary from './summary';
import requestSummary from './requestSummary';
import request from './request';

import { type $State } from '../../../../commons/utils/TypeUtil';

const rootReducer = {
  summary,
  requestSummary,
  request,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
