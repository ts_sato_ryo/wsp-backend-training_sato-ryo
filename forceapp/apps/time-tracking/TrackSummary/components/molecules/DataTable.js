// @flow

import * as React from 'react';
import styled from 'styled-components';
import isNil from 'lodash/isNil';

import { Table, Header, HeaderCol, Body, Row, Cell } from '../atoms/Table';

type ColumnSelector<TRow: {}> = {
  [header: string]: React.ComponentType<{ ...TRow, color: ?string }>,
};

type Props<TRow: {}> = $ReadOnly<{|
  className?: string,
  data: $ReadOnlyArray<TRow>,
  column: ColumnSelector<TRow>,
  colors?: $ReadOnlyArray<string>,
|}>;

const EmptyRow = styled(Row)`
  min-height: 100px;
  height: 100px;
`;

const Circle = styled.div`
  border-radius: 50%;
  background-color: ${(props: Object) => props.color};
  height: 12px;
  width: 12px;
`;

const DataTable = <TRow: {}>(props: Props<TRow>) => {
  const colors = props.colors || [];
  const colorLength = colors.length;
  const headers = Object.keys(props.column);

  return (
    <Table className={props.className}>
      <Header>
        <Row>
          {props.colors && <HeaderCol />}
          {headers.map((header: string, index: number) => {
            return <HeaderCol colIndex={index}>{header}</HeaderCol>;
          })}
        </Row>
      </Header>
      <Body>
        {(isNil(props.data) || props.data.length < 1) && <EmptyRow />}
        {props.data.map((datum, rowIndex: number) => {
          const colorIndex = rowIndex % colorLength;
          const color = props.colors ? colors[colorIndex] : undefined;
          return (
            <Row>
              {props.colors && (
                <Cell>
                  <Circle color={color} />
                </Cell>
              )}
              {headers.map((header, index) => {
                const Content = props.column[header];
                return (
                  <Cell colIndex={index}>
                    <Content {...datum} color={color} />
                  </Cell>
                );
              })}
            </Row>
          );
        })}
      </Body>
    </Table>
  );
};

export default DataTable;
