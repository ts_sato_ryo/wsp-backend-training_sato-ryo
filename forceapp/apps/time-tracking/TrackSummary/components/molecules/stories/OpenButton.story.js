// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean } from '@storybook/addon-knobs';
/* eslint-disable import/no-extraneous-dependencies */

import OpenButton from '../OpenButton';

storiesOf('time-tracking/TrackSummary/Buttons', module)
  .addDecorator(withKnobs)
  .add(
    'OpenButton',
    () => (
      <OpenButton
        disableMotion
        isOpen={boolean('isOpen', false)}
        onClick={action('onClick')}
      >
        Open
      </OpenButton>
    ),
    { info: { propTables: false, inline: false, source: true } }
  );
