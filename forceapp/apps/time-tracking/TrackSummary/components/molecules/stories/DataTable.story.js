// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import numeral from 'numeral';
import range from 'lodash/range';
import styled from 'styled-components';

import TimeUtil from '../../../../../commons/utils/TimeUtil';
import variables from '../../../../../commons/styles/wsp.scss';

import DataTable from '../DataTable';

const DefaultJob = styled.div`
  display: inline;
  border-radius: 20px;
  border: 1px solid ${(props: Object) => props.color};
  color: ${(props: Object) => props.color};
  margin: 0 0 0 8px;
  padding: 0 4px;
  font-size: 10px;
  line-height: 13px;
`;

const data = [
  {
    jobName: 'R06(19/07)_WSP_Entire/Design',
    workTimeRatio: 62.0,
    workTime: 5829,
  },
  {
    jobName: 'R06(19/07)_WSP_Entire／Meeting&Others',
    workTimeRatio: 23.0,
    workTime: 2160,
  },
  {
    jobName: '（PD-Common）Others',
    workTimeRatio: 12.3,
    workTime: 990,
    isDefaultJob: true,
  },
  {
    jobName: '（PD-Common）Recruting',
    workTimeRatio: 2.1,
    workTime: 210,
  },
  {
    jobName: '（PD-Common）Meeting/ミーティング他',
    workTimeRatio: 0.1,
    workTime: 30,
  },
];

storiesOf('time-tracking/TrackSummary', module).add('DataTable', () => (
  <DataTable
    data={data}
    colors={range(1, 13).map((i) => variables[`graph-color-${i}`])}
    column={{
      'Job/Work Category': ({ color, ...row }) =>
        row.isDefaultJob ? (
          <>
            {row.jobName}
            <DefaultJob color={color}>Default Job</DefaultJob>
          </>
        ) : (
          row.jobName
        ),
      'Working Percentage': (row) =>
        `${numeral(row.workTimeRatio).format('0.0')}%`,
      'Working Hours': (row) => TimeUtil.toHHmm(row.workTime),
    }}
  />
));
