// @flow

import * as React from 'react';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';

import msg from '../../../../commons/languages';
import OpenButtonIcon from '../../icons/open-button.svg';
import variables from '../../../../commons/styles/wsp.scss';

import Button from '../atoms/Button';

type Props = $ReadOnly<{
  disableMotion?: boolean,
  isOpen: boolean,
  onClick: () => void,
}>;

const Block = styled.button`
  border-radius: unset;
  background: transparent;
  border: none;
  outline: none;
  appearance: none;
  padding: 0;
  margin: 0;
  width: 60px;
`;

const Row = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  width: 100%;
  font-size: ${variables['text-button-font-size']};
`;

const Icon = styled.div`
  width: 16px;
  height: 16px;
  fill: ${(props: Object) => variables[`color-${props.color}`]};
`;

const Text = styled.div`
  font-size: ${variables['text-button-font-size']};
  line-height: ${variables['text-button-line-height']};
  color: ${(props: Object) => variables[`color-${props.color}`]};
`;

const Rotate = (props: {
  trigger: boolean,
  rotate: string,
  children: React.Node,
}) => {
  const { transform } = useSpring({
    from: {
      transform: props.trigger ? `rotate(${props.rotate})` : 'rotate(0deg)',
    },
    to: {
      transform: props.trigger ? 'rotate(0deg)' : `rotate(${props.rotate})`,
    },
  });

  return (
    <animated.div style={{ transform, display: 'inherit' }}>
      {props.children}
    </animated.div>
  );
};

const IconBlock = styled.div`
  margin: 0 4px 0 0;
  display: inherit;
  height: 100%;
`;

const _OpenButton = (props: Props) => (
  <Block onClick={props.onClick}>
    <Row>
      <IconBlock>
        {props.disableMotion ? (
          <Icon as={OpenButtonIcon} color="primary" />
        ) : (
          <Rotate trigger={!props.isOpen} rotate="180deg">
            <Icon as={OpenButtonIcon} color="primary" />
          </Rotate>
        )}
      </IconBlock>
      <Text color="primary">
        {props.isOpen ? msg().Com_Btn_Close : msg().Com_Btn_Open}
      </Text>
    </Row>
  </Block>
);

const OpenButton = (props: Props) => <Button {...props} as={_OpenButton} />;

export default OpenButton;
