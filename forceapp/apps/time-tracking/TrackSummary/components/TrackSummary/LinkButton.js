// @flow

import * as React from 'react';
import styled from 'styled-components';

type Props = $ReadOnly<{|
  children: React.Node,
  onClick: () => void,
|}>;

const LinkButton: React.ComponentType<Props> = styled.button`
  background: none;
  color: #2782ed;
  border: none;
`;

export default LinkButton;
