// @flow

import React, { type Node, useEffect, useRef, useState } from 'react';
import { animated, useSpring } from 'react-spring';
import ResizeObserver from 'resize-observer-polyfill';

import {
  Body,
  Card,
  Divider,
  Header,
  HeaderGroup,
  HeaderItem,
  Title,
} from './Card';
import OpenButton from '../molecules/OpenButton';
import StatusLabel from './StatusLabel';
import WorkHours from './WorkHours';
import Summary, { Column } from './Summary';
import SubmitButton from '../../containers/TrackSummary/SubmitButtonContainer';
import LinkButton from './LinkButton';
import RequestDialog from '../../containers/RequestDialogContainer';
import HistoryDialog from '../../containers/HistoryDialogContainer';
import Navigation from '../../containers/TrackSummary/NavigationContainer';

import msg from '../../../../commons/languages';

import type { RequestProps } from './Props';
import { Row } from './Layout';
import STATUS from '../../../../domain/models/approval/request/Status';

const useMeasureHeight = () => {
  const ref = useRef();
  const [height, setHeight] = useState(0);
  const [observer] = useState(
    () => new ResizeObserver(([dom]) => setHeight(dom.contentRect.height))
  );
  useEffect(() => {
    if (ref.current) {
      observer.observe(ref.current);
    }
    return () => observer.disconnect();
  });
  return [ref, height];
};

const Expand = (props: { trigger: boolean, children: Node }) => {
  const [ref, viewHeight] = useMeasureHeight();

  const frame = {
    from: {
      height: 0,
      opacity: 0,
      transform: 'translate3d(0, 0, 0)',
    },
    to: {
      height: viewHeight,
      opacity: 1,
      transform: `translate3d(0, 50px, 0)`,
    },
  };
  const style = useSpring(
    props.trigger
      ? {
          from: frame.from,
          to: frame.to,
        }
      : {
          from: frame.to,
          to: frame.from,
        }
  );

  return (
    <animated.div style={{ ...style, display: 'inherit' }}>
      <div style={{ transform: `translate3d(0, -50px, 0)` }} ref={ref}>
        {props.children}
      </div>
    </animated.div>
  );
};

const Request = (props: {
  ...RequestProps,
  disableMotion?: boolean,
  isOpen: boolean,
  onClickOpen: () => void,
}) => {
  const ExpandMotion = props.disableMotion
    ? ({ children }: Object) => <div>{children}</div>
    : Expand;

  return (
    <Card>
      <Header>
        <Title>{msg().Time_Lbl_TrackSummaryTitle}</Title>
        <HeaderGroup>
          <HeaderItem>
            {props.useRequest && <StatusLabel status={props.status} />}
          </HeaderItem>
          <HeaderItem>
            {props.useRequest && props.status !== STATUS.NotRequested && (
              <LinkButton onClick={props.openHistoryDialog}>
                {msg().Com_Lbl_ApprovalHistory}
              </LinkButton>
            )}
          </HeaderItem>
        </HeaderGroup>
        <HeaderGroup right>
          <HeaderItem>
            <WorkHours data={props.data} headline />
          </HeaderItem>
          <HeaderItem>
            <OpenButton
              disableMotion={props.disableMotion}
              isOpen={props.isOpen}
              onClick={props.onClickOpen}
            />
          </HeaderItem>
        </HeaderGroup>
      </Header>
      <RequestDialog />
      <HistoryDialog />
      {props.isOpen && <Divider />}
      <ExpandMotion trigger={props.isOpen}>
        {props.isOpen && (
          <Body>
            <Row height={props.isOpen ? 'auto' : 0}>
              <Row noMargin>
                <Navigation />
              </Row>

              {props.useRequest && <SubmitButton />}
            </Row>
            <Row noMargin align="start" height={props.isOpen ? 'auto' : 0}>
              <Summary data={props.data} column={Column.Request} />
            </Row>
          </Body>
        )}
      </ExpandMotion>
    </Card>
  );
};

export default Request;
