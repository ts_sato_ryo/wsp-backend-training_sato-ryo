// @flow

import styled from 'styled-components';
import * as Base from '../atoms/Card';

export const Card = styled(Base.Card)`
  min-height: ${(props: Object) => (props.isOpen ? '290px' : 'auto')};
`;
export const Header = Base.Header;
export const HeaderGroup = Base.HeaderGroup;
export const HeaderItem = Base.HeaderItem;
export const Divider = Base.Divider;
export const Body = Base.Body;
export const Title = Base.Title;
