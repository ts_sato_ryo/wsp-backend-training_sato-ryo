// @flow

import React from 'react';
import styled from 'styled-components';

import Button from '../atoms/Button';
import ChevronLeft from '../../icons/chevronleft.svg';
import ChevronRight from '../../icons/chevronright.svg';
import Period from './Period';

import msg from '../../../../commons/languages';

type Props = $ReadOnly<{|
  startDate: string,
  endDate: string,
  onClickNext: () => void,
  onClickPrev: () => void,
  onClickCurrent: () => void,
|}>;

const CurrentPeriodButton = styled(Button)`
  margin: 0 16px 0 20px;
`;

const Navigation = ({
  startDate,
  endDate,
  onClickNext,
  onClickPrev,
  onClickCurrent,
}: Props) => {
  return (
    <>
      <Period startDate={startDate} endDate={endDate} />
      <CurrentPeriodButton default onClick={onClickCurrent}>
        {msg().Time_Btn_CurrentPeriod}
      </CurrentPeriodButton>
      <Button icon primary onClick={onClickPrev}>
        <ChevronLeft />
      </Button>
      <Button icon primary onClick={onClickNext}>
        <ChevronRight />
      </Button>
    </>
  );
};

export default Navigation;
