// @flow

import React from 'react';
import styled from 'styled-components';

import { Color } from '../../../../core/styles';
import msg from '../../../../commons/languages';

import STATUS, {
  type Status,
} from '../../../../domain/models/approval/request/Status';

const colors = {
  [STATUS.NotRequested]: Color.notRequested,
  [STATUS.Pending]: Color.pending,
  [STATUS.Removed]: Color.removed,
  [STATUS.Rejected]: Color.rejected,
  [STATUS.Approved]: Color.approved,
  [STATUS.Canceled]: Color.canceled,
};

const Block = styled.div`
  color: ${(props: Object) =>
    props.status === STATUS.Canceled ? '#333' : '#fff'}
  background: ${(props: Object) => colors[props.status]};
  font-size: 12px;
  width: 120px;
  height: 20px;
  border-radius: 2px;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: center;
`;

const StatusLabel = (props: { status: Status }) => (
  <Block status={props.status}>{msg()[`Time_Status_${props.status}`]}</Block>
);

export default StatusLabel;
