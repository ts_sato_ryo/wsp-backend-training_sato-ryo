// @flow

import * as React from 'react';
import numeral from 'numeral';
import styled from 'styled-components';
import range from 'lodash/range';

import Pie from '../atoms/graphs/Pie';
import DataTable from '../molecules/DataTable';
import DefaultJobChip from './DefaultJobChip';
import WorkHours from './WorkHours';

import variables from '../../../../commons/styles/wsp.scss';
import msg from '../../../../commons/languages';
import TimeUtil from '../../../../commons/utils/TimeUtil';

import type { Data, Datum } from './Props';

type Row = {
  ...Datum,
  color: ?string,
};

type ColumnDef = { [header: string]: React.ComponentType<Row> };

type Props = $ReadOnly<{ data: Data, column: () => ColumnDef }>;

const PreLine = styled.span`
  white-space: pre-line;
`;

const Text = styled.span`
  display: inline-block;
`;

const JobAndWorkCategory = ({ jobName, workCategoryName }) => (
  <>
    <PreLine>
      <Text>{jobName}</Text>
      {workCategoryName && <Text> /{workCategoryName}</Text>}
    </PreLine>
  </>
);

export const Column: { Request: () => ColumnDef, Approval: () => ColumnDef } = {
  Request: () => ({
    [msg().Time_Lbl_JobName]: ({ isDefaultJob, color, ...props }) => (
      <>
        <JobAndWorkCategory {...props} />
        {isDefaultJob && <DefaultJobChip color={color || ''} />}
      </>
    ),
    [msg().Time_Lbl_WorkingPercentage]: ({ workTimeRatio }: Row) =>
      `${numeral(workTimeRatio).format('0.0')}%`,
    [msg().Time_Lbl_WorkingHours]: ({ workTime }: Row) =>
      TimeUtil.toHHmm(workTime),
  }),
  Approval: () => ({
    [msg().Time_Lbl_JobName]: JobAndWorkCategory,
    [msg().Time_Lbl_WorkingHours]: ({ workTime }: Row) =>
      TimeUtil.toHHmm(workTime),
  }),
};

const ChartBlock = styled.div`
  width: 140px;
  height: 140px;
  margin: 0 20px 0 0;
`;

const DataBlock = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  align-items: flex-end;
  width: 100%;
  min-height: 140px;
`;

const colors = range(1, 13).map((i) => variables[`graph-color-${i}`]);

export default ({ data, column }: Props) => (
  <>
    <ChartBlock>
      <Pie
        data={data.map(
          ({ jobName, workCategoryName, workTimeRatio }, index) => ({
            id: `${index}`,
            value: workTimeRatio,
            label: workCategoryName
              ? `${jobName}/${workCategoryName}`
              : jobName,
          })
        )}
      />
    </ChartBlock>
    <DataBlock>
      <DataTable data={data} colors={colors} column={column()} />
      <WorkHours data={data} />
    </DataBlock>
  </>
);
