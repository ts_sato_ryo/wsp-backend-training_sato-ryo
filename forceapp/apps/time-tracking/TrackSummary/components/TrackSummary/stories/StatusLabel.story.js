// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-enable import/no-extraneous-dependencies */

import StatusLabel from '../StatusLabel';

storiesOf('time-tracking/TrackSummary', module).add('StatusLabel', () => (
  <>
    <StatusLabel status="NotRequested" />
    <StatusLabel status="Pending" />
    <StatusLabel status="Removed" />
    <StatusLabel status="Rejected" />
    <StatusLabel status="Approved" />
  </>
));
