// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
/* eslint-enable import/no-extraneous-dependencies */

import { withProvider } from '../../../../../../.storybook/decorator/Provider';
import configureStore from '../../../store/configureStore';
import TrackSummary from '../index';

const store = configureStore({
  entities: {
    summary: {
      startDate: '2019-7-16',
      endDate: '2019-8-15',
      request: {
        status: 'NotRequested',
        approverName: 'TeamSpirit',
      },
    },
  },
  userSetting: {
    photoUrl:
      'https://www.guidedogs.org/wp-content/uploads/2015/05/Dog-Im-Not.jpg',
  },
});

const data = [
  {
    jobName: 'R06(19/07)_WSP_Entire',
    workCategoryName: 'Design',
    workTimeRatio: 62.0,
    workTime: 5829,
    isDefaultJob: false,
  },
  {
    jobName: 'R06(19/07)_WSP_Entire',
    workCategoryName: 'Meeting&Others',
    workTimeRatio: 23.0,
    workTime: 2160,
    isDefaultJob: false,
  },
  {
    jobName: '（PD-Common）Others',
    workCategoryName: undefined,
    workTimeRatio: 12.3,
    workTime: 990,
    isDefaultJob: true,
  },
  {
    jobName: '（PD-Common）Recruting',
    workCategoryName: undefined,
    workTimeRatio: 2.1,
    workTime: 210,
    isDefaultJob: false,
  },
  {
    jobName: '（PD-Common）Meeting',
    workCategoryName: 'ミーティング他',
    workTimeRatio: 0.1,
    workTime: 30,
    isDefaultJob: false,
  },
];

const longData = [
  {
    jobName:
      'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure? On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee',
    workCategoryName: 'Design',
    workTimeRatio: 50.0,
    workTime: 5829,
    isDefaultJob: false,
  },
  {
    jobName: 'R06(19/07)_WSP_Entire',
    workCategoryName: 'Meeting&Others',
    workTimeRatio: 50.0,
    workTime: 2160,
    isDefaultJob: false,
  },
];

storiesOf('time-tracking/TrackSummary', module)
  .addDecorator(withProvider(store))
  .add('Approval', () => (
    <TrackSummary.Approval
      data={data.slice(0, 2)}
      startDate="2019-7-16"
      endDate="2019-8-15"
    />
  ))
  .add('Approval/Many Data', () => (
    <TrackSummary.Approval
      data={[...data, ...data, ...data, ...data]}
      startDate="2019-7-16"
      endDate="2019-8-15"
    />
  ))
  .add('Approval/Long Data', () => (
    <TrackSummary.Approval
      data={longData}
      startDate="2019-7-16"
      endDate="2019-8-15"
    />
  ))
  .add('Request', () => (
    <TrackSummary.Request
      data={data}
      startDate="2019-07-15"
      endDate="2019-08-14"
      status="Pending"
      useRequest
      isOpen
      disableMotion
      openHistoryDialog={action('open history dialog')}
      onClickOpen={action('onClickOpen')}
    />
  ))
  .add('Request/Empty', () => (
    <TrackSummary.Request
      data={[]}
      startDate="2019-07-15"
      endDate="2019-08-14"
      status="NotRequested"
      useRequest
      isOpen
      disableMotion
      openHistoryDialog={action('open history dialog')}
      onClickOpen={action('onClickOpen')}
    />
  ))
  .add('Request/Many Data', () => (
    <TrackSummary.Request
      data={[...data, ...data, ...data, ...data]}
      startDate="2019-7-16"
      endDate="2019-8-15"
      status="Approved"
      useRequest
      isOpen
      disableMotion
      openHistoryDialog={action('open history dialog')}
      onClickOpen={action('onClickOpen')}
    />
  ))
  .add('Request/Long Data', () => (
    <TrackSummary.Request
      data={longData}
      startDate="2019-7-16"
      endDate="2019-8-15"
      status="Approved"
      useRequest
      isOpen
      disableMotion
      openHistoryDialog={action('open history dialog')}
      onClickOpen={action('onClickOpen')}
    />
  ));
