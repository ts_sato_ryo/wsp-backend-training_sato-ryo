// @flow

import type { Status } from '../../../../domain/models/approval/request/Status';

export type Datum = {
  jobName: string,
  workCategoryName: ?string,
  workTime: number,
  workTimeRatio: number,
  isDefaultJob: ?boolean,
};

export type Data = $ReadOnlyArray<Datum>;

export type ApprovalProps = $ReadOnly<{|
  /**
   * Records of Time Track Summary
   */
  data: Data,

  /**
   * Start date of Time Track Summary
   */
  startDate: string,

  /**
   * End date of Time Track Summary
   */
  endDate: string,
|}>;

export type RequestProps = $ReadOnly<{|
  ...ApprovalProps,
  status: Status,
  useRequest: boolean,
  openHistoryDialog: () => void,
|}>;
