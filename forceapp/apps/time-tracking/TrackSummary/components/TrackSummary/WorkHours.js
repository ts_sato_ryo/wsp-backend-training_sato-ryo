// @flow

import React from 'react';
import styled from 'styled-components';
import isNil from 'lodash/isNil';

import { Headline, Text } from '../atoms/Text';
import msg from '../../../../commons/languages';
import variables from '../../../../commons/styles/wsp.scss';
import TimeUtil from '../../../../commons/utils/TimeUtil';

type Data = $ReadOnlyArray<{
  workTime: number,
}>;

type Props = $ReadOnly<{|
  headline?: boolean,
  data: Data,
|}>;

const Heading = styled(Text)`
  color: ${variables['color-text-2']};
  margin-right: 16px;
`;

const Emphasis = styled(Headline)`
  color: ${(props: Object) =>
    props.isEmpty ? variables['color-text-3'] : variables['color-text-1']};
`;

export default ({ headline, data }: Props) => {
  const aggregatedValue = data.reduce((acc, datum) => acc + datum.workTime, 0);

  return (
    <>
      {headline && <Heading body1>{msg().Time_Lbl_WorkingHours}</Heading>}
      <Emphasis isEmpty={isNil(data) || data.length <= 0}>
        {TimeUtil.toHHmm(aggregatedValue)}
      </Emphasis>
    </>
  );
};
