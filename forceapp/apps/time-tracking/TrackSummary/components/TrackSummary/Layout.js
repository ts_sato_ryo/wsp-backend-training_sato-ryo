// @flow

import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const Row = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: ${({ align = 'center' }) => align};
  margin: 0 0 ${(props: Object) => (props.noMargin ? '0' : '20px')} 0;
  height: ${(props: Object) => props.height || 'auto'};
`;
