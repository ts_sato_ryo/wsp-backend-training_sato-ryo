// @flow

import Approval from './Approval';
import Request from './Request';

const TrackSummary = {
  Approval,
  Request,
};

export default TrackSummary;
