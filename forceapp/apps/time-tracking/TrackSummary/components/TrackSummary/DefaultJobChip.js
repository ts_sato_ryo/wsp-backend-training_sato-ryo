// @flow

import React from 'react';
import styled from 'styled-components';

import msg from '../../../../commons/languages';

const Chip = styled.div`
  display: inline;
  border-radius: 20px;
  border: 1px solid ${(props: Object) => props.color};
  color: ${(props: Object) => props.color};
  margin: 0 0 0 8px;
  padding: 0 4px;
  font-size: 10px;
  line-height: 13px;
`;

type Props = $ReadOnly<{|
  color: string,
|}>;

const DefaultJobChip = (props: Props) => (
  <Chip {...props}>{msg().Com_Lbl_DefaultJobShort}</Chip>
);

export default DefaultJobChip;
