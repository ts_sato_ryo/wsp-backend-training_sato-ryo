// @flow

import React from 'react';

import { Body, Card, Divider, Header, Title } from './Card';
import Summary, { Column } from './Summary';

import msg from '../../../../commons/languages';

import type { ApprovalProps } from './Props';
import { Row } from './Layout';
import Period from './Period';

const Approval = (props: ApprovalProps) => {
  return (
    <Card>
      <Header>
        <Title>{msg().Time_Lbl_TrackSummaryTitle}</Title>
      </Header>
      <Divider />
      <Body>
        <Row>
          <Row noMargin>
            <Period startDate={props.startDate} endDate={props.endDate} />
          </Row>
        </Row>
        <Row noMargin align="start">
          <Summary data={props.data} column={Column.Approval} />
        </Row>
      </Body>
    </Card>
  );
};

export default Approval;
