// @flow

import React from 'react';

import type { Status } from '../../../../domain/models/approval/request/Status';
import STATUS from '../../../../domain/models/approval/request/Status';
import Button from '../atoms/Button';
import msg from '../../../../commons/languages';

type Props = $ReadOnly<{
  status: Status,
  disabled?: boolean,
  onClick: () => void | Promise<void>,
}>;

const Submit = ({ disabled, onClick }: Props) => (
  <Button primary disabled={disabled} onClick={onClick}>
    {msg().Time_Action_Submit}
  </Button>
);

const Recall = ({ disabled, onClick }: Props) => (
  <Button error disabled={disabled} onClick={onClick}>
    {msg().Time_Action_Recall}
  </Button>
);

const CancelApproval = ({ disabled, onClick }: Props) => (
  <Button error disabled={disabled} onClick={onClick}>
    {msg().Time_Action_CancelApproval}
  </Button>
);

const ButtonSet = {
  // Status => Action
  [STATUS.NotRequested]: Submit,
  [STATUS.Rejected]: Submit,
  [STATUS.Removed]: Submit,
  [STATUS.Canceled]: Submit,
  [STATUS.Pending]: Recall,
  [STATUS.Approved]: CancelApproval,
  // Never used here
  [STATUS.ApprovalIn]: null,
  [STATUS.Reapplying]: null,
  // fallback
  '': null,
};

const SubmitButton = ({ status, ...props }: Props) => {
  const ActionButton = ButtonSet[status];
  return <>{ActionButton && <ActionButton status={status} {...props} />}</>;
};

export default SubmitButton;
