// @flow

import React from 'react';
import styled from 'styled-components';

import { Text } from '../atoms/Text';

import variables from '../../../../commons/styles/wsp.scss';
import DateUtil from '../../../../commons/utils/DateUtil';

type Props = $ReadOnly<{|
  className?: string,
  startDate: string,
  endDate: string,
|}>;

const StyledText = styled(Text)`
  min-width: 260px;
  font-size: 13px;
  font-weight: bold;
  line-height: 17px;
  color: ${variables['color-text-2']};
`;

const Period = (props: Props) => {
  const period =
    props.startDate && props.endDate
      ? `${DateUtil.formatLongDate(
          props.startDate
        )} - ${DateUtil.formatLongDate(props.endDate)}`
      : '';
  return <StyledText body1>{period}</StyledText>;
};

export default Period;
