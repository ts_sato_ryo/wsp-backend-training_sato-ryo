// @flow
import * as React from 'react';
import styled from 'styled-components';

import msg from '../../../commons/languages';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import Label from '../../../commons/components/fields/Label';
import CommentNarrowField from '../../../commons/components/fields/CommentNarrowField';
import SubmitButton from './TrackSummary/SubmitButton';
import Button from './atoms/Button';
import type { Status } from '../../../domain/models/approval/request/Status';

import './RequestDialog.scss';

const { useCallback } = React;
const ROOT = 'tracking-pc-request-dialog';

type Props = $ReadOnly<{|
  comment: string,
  userPhotoUrl: string,
  approverName: ?string,
  status: Status,
  onChangeComment: (comment: string) => void,
  onClickClose: () => void,
  onClickSubmit: () => Promise<void>,
|}>;

const StyledButton = styled(Button)`
  margin: 0 8px 0 0;
`;

export default function RequestDialog({
  comment,
  userPhotoUrl,
  approverName,
  status,
  onChangeComment,
  onClickClose,
  onClickSubmit,
}: Props) {
  const onChangeCommentHandler = useCallback(
    (value: string) => onChangeComment(value),
    []
  );
  return (
    <div className={`${ROOT}`}>
      <DialogFrame
        className={`${ROOT}__dialog-frame`}
        title={msg().Trac_Lbl_Request}
        hide={onClickClose}
        footer={
          <DialogFrame.Footer
            sub={
              <Label text={msg().Trac_Lbl_NextApproverEmployee}>
                <div className={`${ROOT}__approver-employee`}>
                  <div className={`${ROOT}__approver-employee-name`}>
                    {approverName || msg().Com_Lbl_Unspecified}
                  </div>
                </div>
              </Label>
            }
          >
            <StyledButton onClick={onClickClose}>
              {msg().Com_Btn_Cancel}
            </StyledButton>
            <SubmitButton status={status} onClick={onClickSubmit}>
              {msg().Com_Btn_Request}
            </SubmitButton>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__inner`}>
          <CommentNarrowField
            onChange={onChangeCommentHandler}
            value={comment}
            icon={userPhotoUrl}
          />
        </div>
      </DialogFrame>
    </div>
  );
}
