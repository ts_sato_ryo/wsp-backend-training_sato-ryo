// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import styled from 'styled-components';

import {
  Body,
  Card,
  Divider,
  Header,
  HeaderGroup,
  HeaderItem,
  Title,
} from '../Card';

const Container = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  width: 50%;
  height: 100vh;
`;

storiesOf('time-tracking/TrackSummary/Cards', module)
  .addDecorator((story) => <Container>{story()}</Container>)
  .add(
    'Card',
    () => (
      <Card>
        <Header>
          <Title>Title</Title>
          <HeaderGroup>
            <HeaderItem>item</HeaderItem>
          </HeaderGroup>
          <HeaderGroup>
            <HeaderItem right>right 1</HeaderItem>
            <HeaderItem right>right 2</HeaderItem>
          </HeaderGroup>
        </Header>
        <Divider />
        <Body>Body</Body>
      </Card>
    ),
    {
      info: { propTables: false, inline: false, source: true },
    }
  );
