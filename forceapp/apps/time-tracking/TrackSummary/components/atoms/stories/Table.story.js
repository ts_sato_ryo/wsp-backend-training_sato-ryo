// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
/* eslint-disable import/no-extraneous-dependencies */

import styled from 'styled-components';

import { Table, Header, HeaderCol, Body, Row, Cell } from '../Table';

const Container = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  width: 50%;
  height: 100vh;
`;

storiesOf('time-tracking/TrackSummary', module)
  .addDecorator((story) => <Container>{story()}</Container>)
  .add('Table', () => (
    <Table>
      <Header>
        <HeaderCol>A</HeaderCol>
        <HeaderCol>B</HeaderCol>
        <HeaderCol>C</HeaderCol>
      </Header>
      <Body>
        <Row>
          <Cell>1</Cell>
          <Cell>2</Cell>
          <Cell>3</Cell>
        </Row>
        <Row>
          <Cell>4</Cell>
          <Cell>5</Cell>
          <Cell>6</Cell>
        </Row>
      </Body>
    </Table>
  ));
