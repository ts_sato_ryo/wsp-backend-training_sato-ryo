// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
/* eslint-disable import/no-extraneous-dependencies */

import Button from '../Button';

storiesOf('time-tracking/TrackSummary', module).add(
  'Buttons',
  () => (
    <>
      <Button onClick={action('onClick')}>Button</Button>
      <Button onClick={action('onClick')} default>
        Default
      </Button>
      <Button onClick={action('onClick')} primary>
        Primary
      </Button>
      <Button onClick={action('onClick')} secondary>
        Secondary
      </Button>
      <Button onClick={action('onClick')} error>
        Error
      </Button>
      <Button onClick={action('onClick')} text>
        Text
      </Button>
    </>
  ),
  { info: { propTables: false, inline: false, source: true } }
);
