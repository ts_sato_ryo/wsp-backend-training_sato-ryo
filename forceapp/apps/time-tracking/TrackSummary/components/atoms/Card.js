// @flow

import classnames from 'classnames';
import styled from 'styled-components';

import '../../../../commons/styles/wsp.scss';

export const Card = styled.div.attrs({
  className: ('wsp-card': ?string),
})``;

export const Title = styled.div.attrs({
  className: ('wsp-card__title': ?string),
})``;

export const Header = styled.div.attrs({
  className: ('wsp-card__header': ?string),
})``;

export const HeaderGroup = styled.div.attrs((props: { right: boolean }) => ({
  className: (classnames('wsp-card__header-group', {
    'wsp-card__header-group--right': props.right,
  }): ?string),
}))`
  margin: ${(props: Object) => (props.right ? '0' : '0 0 0 20px')};
`;

export const HeaderItem = styled.div.attrs({
  className: (classnames('wsp-card__header-group__item'): ?string),
})``;

export const Divider = styled.div.attrs({
  className: (classnames('wsp-card__divider'): ?string),
})``;

export const Body = styled.div.attrs({
  className: (classnames('wsp-card__body'): ?string),
})``;
