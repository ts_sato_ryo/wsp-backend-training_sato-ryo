// @flow

import styled from 'styled-components';
import classnames from 'classnames';

export const Headline = styled.span.attrs({
  className: 'wsp-header-1',
})``;

export const Text = styled.span.attrs((props: Object) => ({
  className: (classnames({
    'wsp-body-1': (!props.body1 && !props.body2) || props.body1,
    'wsp-body-2': props.body2,
  }): ?string),
}))``;
