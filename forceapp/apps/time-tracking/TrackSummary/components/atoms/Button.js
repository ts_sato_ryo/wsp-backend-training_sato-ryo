// @flow

import classnames from 'classnames';
import styled from 'styled-components';

import '../../../../commons/styles/wsp.scss';

type Props = $ReadOnly<{
  default?: boolean,
  primary?: boolean,
  secondary?: boolean,
  error?: boolean,
  text?: boolean,
  icon?: boolean,
}>;

const Button = styled.button.attrs((props: Props) => ({
  className: (classnames('wsp-button', {
    '': props.default,
    'wsp-button--primary': props.primary,
    'wsp-button--secondary': props.secondary,
    'wsp-button--error': props.error,
    'wsp-button--text': props.text,
    'wsp-button--icon': props.icon,
  }): any),
}))``;

export default Button;
