/* @flow */

import React from 'react';

import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { withProvider } from '../../../../../.storybook/decorator/Provider';

import RequestDialog from '../RequestDialog';
import configureStore from '../../store/configureStore';
import '../RequestDialog.scss';

const store = configureStore();

storiesOf('time-tracking/TrackSummary/RequestDialog', module)
  .addDecorator(withProvider(store))
  .addDecorator(withKnobs)
  .addParameters({
    readme: {
      content: '',
    },
  })
  .add(
    'RequestDialog',
    withInfo({
      propTables: [RequestDialog],
      inline: true,
      source: true,
    })(() => (
      <RequestDialog
        status="NotRequested"
        userPhotoUrl={text(
          'photoUrl',
          'https://cdn.onlinewebfonts.com/svg/img_415067.png'
        )}
        comment={text('comment', 'comment')}
        approverName={text('approverName', 'Manager')}
        onChangeComment={action('onChangeComment')}
        onClickClose={action('onClickClose')}
        onClickSubmit={action('onClickSubmit')}
      />
    ))
  );
