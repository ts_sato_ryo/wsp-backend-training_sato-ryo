// @flow
import { type Dispatch, bindActionCreators } from 'redux';

import { actions as appActions } from '../../../commons/modules/app';
import { actions as summaryActions } from '../modules/entities/requestSummary';
import RequestRepository from '../../../repositories/time-tracking/RequestRepository';

const App = (dispatch: Dispatch<Object>) =>
  bindActionCreators(appActions, dispatch);

export default (dispatch: Dispatch<Object>) => {
  const app = App(dispatch);
  return {
    initialize: async (requestId: string): Promise<void> => {
      dispatch(summaryActions.reset());
      try {
        app.loadingStart();

        const summary = await RequestRepository.fetchSummary({
          requestId,
        });
        dispatch(summaryActions.fetchSuccess(summary));
      } finally {
        app.loadingEnd();
      }
    },
  };
};
