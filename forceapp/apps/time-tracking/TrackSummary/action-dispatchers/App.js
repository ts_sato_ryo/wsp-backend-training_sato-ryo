// @flow

import type { Dispatch } from 'redux';
import { getUserSetting } from '../../../commons/actions/userSetting';

import { actions as blocking } from '../modules/ui/blocking';
import { actions as toast } from '../../../commons/modules/toast';

export default (dispatch: Dispatch<Object>) => ({
  initialize: async (): Promise<void> => {
    await dispatch(getUserSetting());
  },

  showErrorNotification: (message: string) => {
    dispatch(blocking.enable());
    dispatch(toast.show(message, 'error'));
  },

  hideErrorNotification: () => {
    dispatch(toast.hide());
    dispatch(blocking.disable());
  },

  resetErrorNotification: () => {
    dispatch(toast.reset());
  },
});
