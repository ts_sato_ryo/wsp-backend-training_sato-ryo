// @flow
import { bindActionCreators, type Dispatch } from 'redux';

import { actions as appActions } from '../../../commons/modules/app';
import { showToast } from '../../../commons/modules/toast';
import { actions } from '../modules/ui/request';
import { actions as summaryActions } from '../modules/entities/summary';
import { actions as requestActions } from '../modules/entities/request';
import { actions as historyListActions } from '../../../../widgets/dialogs/ApprovalHistoryDialog/modules/entities/historyList';
import { getRequestApprovalHistory } from '../../../domain/models/approval/request/History';
import SummaryRepository from '../../../repositories/time-tracking/SummaryRepository';
import RequestRepository from '../../../repositories/time-tracking/RequestRepository';
import DateUtil from '../../../commons/utils/DateUtil';
import msg from '../../../commons/languages';
import type { Status } from '../../../domain/models/approval/request/Status';
import STATUS from '../../../domain/models/approval/request/Status';

const App = (dispatch: Dispatch<Object>) =>
  bindActionCreators({ ...appActions, showToast }, dispatch);

const Summary = (dispatch: Dispatch<Object>) => ({
  load: async (targetDate: string): Promise<void> => {
    const summary = await SummaryRepository.fetchSummary({
      targetDate,
    });
    dispatch(summaryActions.fetchSuccess(summary));
    dispatch(requestActions.update(targetDate));
  },
});

const UI = (dispatch: Dispatch<Object>) => ({
  editComment: (value: string) => {
    dispatch(actions.update({ comment: value }));
  },

  closeRequestDialog: () => {
    dispatch(actions.reset());
  },

  openRequestDialog: () => {
    dispatch(actions.update({ isOpen: true }));
  },
});

const Request = (_dispatch: Dispatch<Object>) => ({
  submit: async (comment: string, targetDate: string) => {
    await RequestRepository.submit({
      comment,
      targetDate,
    });
  },

  recall: async (requestId: string, comment: string) => {
    await RequestRepository.recall({ requestId, comment });
  },
});

export default (dispatch: Dispatch<Object>) => {
  const today = DateUtil.getToday();
  const summary = Summary(dispatch);
  const request = Request(dispatch);
  const ui = UI(dispatch);
  const app = App(dispatch);

  // Use cases of time track report request
  //
  // NOTE
  // Consider whether to separate objects for each actor
  // e.g.
  // const Application = (dispatch) => ({
  //   initialize: () => {...},
  // )};
  //
  // Application.initialize();
  //
  //
  // const Employee = (dispatch) => ({
  //   seeTimeTrackSummaryOfNextPeriod: () => {...},
  //   applyForTimeTrackReport: () => {...}
  // });
  //
  // Employee.seeTimeTrackSummaryOfNextPeriod();
  // Employee.applyForTimeTrackReport();
  //
  return {
    /**
     * Application "initialize"s its first view.
     */
    initialize: async (targetDate: string = today): Promise<void> => {
      try {
        app.loadingStart();

        await summary.load(targetDate);
      } finally {
        app.loadingEnd();
      }
    },

    /**
     *  Application loads time track summary of next period by Employee operation
     */
    loadNextPeriodSummary: async (currentPeriod: {
      endDate: string,
    }): Promise<void> => {
      try {
        app.loadingStart();

        const nextDate = DateUtil.addDays(currentPeriod.endDate, 1);
        await summary.load(nextDate);
      } finally {
        app.loadingEnd();
      }
    },

    /**
     *  Application loads time track summary of previous period by Employee operation
     */
    loadPrevPeriodSummary: async (currentPeriod: {
      startDate: string,
    }): Promise<void> => {
      try {
        app.loadingStart();

        const prevDate = DateUtil.addDays(currentPeriod.startDate, -1);
        await summary.load(prevDate);
      } finally {
        app.loadingEnd();
      }
    },

    /**
     * Application loads time track summary of today by Employee operation
     */
    loadCurrentPeriodSummary: async () => {
      try {
        app.loadingStart();

        await summary.load(today);
      } finally {
        app.loadingEnd();
      }
    },

    /**
     * Employee edits their comment on request dialog
     */
    editComment: (value: string) => {
      ui.editComment(value);
    },

    /**
     * Employee closes request dialog
     */
    close: () => {
      ui.closeRequestDialog();
    },

    /**
     * Employee opens request dialog
     */
    open: () => {
      ui.openRequestDialog();
    },

    /**
     * Employee submits a time track report
     */
    submit: async (comment: string, targetDate: string) => {
      try {
        app.loadingStart();
        ui.closeRequestDialog();

        await request.submit(comment, targetDate);
        await summary.load(targetDate);

        app.showToast(msg().Time_Lbl_SubmitSucceeded);
      } finally {
        app.loadingEnd();
      }
    },

    /**
     * Employee recalls a submitted time track report
     */
    recall: async (requestId: string, comment: string, targetDate: string) => {
      try {
        app.loadingStart();
        ui.closeRequestDialog();

        await request.recall(requestId, comment);
        await summary.load(targetDate);

        app.showToast(msg().Time_Lbl_RecallSucceeded);
      } finally {
        app.loadingEnd();
      }
    },

    /**
     * Employee cancels an approved time track report
     */
    cancelApproval: async (
      requestId: string,
      comment: string,
      targetDate: string
    ) => {
      try {
        app.loadingStart();
        ui.closeRequestDialog();

        // NOTE
        // `recall` performs also `cancel approval` action internally.
        // In the future, an additional API for `cancel approval` will be added
        // most possibly.
        await request.recall(requestId, comment);
        await summary.load(targetDate);

        app.showToast(msg().Time_Lbl_CancelApprovalSucceeded);
      } finally {
        app.loadingEnd();
      }
    },

    openHistoryDialog: async (requestId: string): Promise<void> => {
      dispatch(historyListActions.unset());

      try {
        app.loadingStart();

        const res = await getRequestApprovalHistory(requestId);
        dispatch(historyListActions.set(res.historyList));
        dispatch(actions.update({ isOpenHistoryDialog: true }));
      } finally {
        app.loadingEnd();
      }
    },

    /**
     * Employee applies for a time track report according to the application status
     */
    get applyForTimeTrackReport() {
      type Props = {
        status: Status,
        requestId: ?string,
        comment: string,
        targetDate: string,
      };

      return ({ status, requestId, comment, targetDate }: Props) => {
        const unexpectedStatus = () =>
          new Error(`Unexpected status: ${status}`);
        const action = {
          [STATUS.NotRequested]: () => this.submit(comment, targetDate),
          [STATUS.Rejected]: () => this.submit(comment, targetDate),
          [STATUS.Canceled]: () => this.submit(comment, targetDate),
          [STATUS.Removed]: () => this.submit(comment, targetDate),
          [STATUS.Pending]: () => this.recall(requestId, comment, targetDate),
          [STATUS.Approved]: () =>
            this.cancelApproval(requestId, comment, targetDate),
          [STATUS.Reapplying]: unexpectedStatus,
          [STATUS.ApprovalIn]: unexpectedStatus,
        }[status];

        return action();
      };
    },
  };
};
