import React from 'react';
import { storiesOf } from '@storybook/react';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import RequestDialog from '../components/RequestDialog';

storiesOf('tracking-pc', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add('RequestDialog', () => <RequestDialog />, {
    propTables: [RequestDialog],
    inline: true,
    source: true,
  });
