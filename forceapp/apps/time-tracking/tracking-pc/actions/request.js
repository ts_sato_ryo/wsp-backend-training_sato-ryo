import Api from '../../../commons/api';
import * as appActions from '../../../commons/actions/app';
import * as dialogsTypes from '../constants/dialogTypes';
import * as dialogConstants from '../../../commons/constants/dialog';
import * as constants from '../constants';
import { selectMonth } from './timeTrackMonthly';

const showDialog = () => {
  return {
    type: dialogConstants.DIALOG_SHOW,
    payload: dialogsTypes.REQUEST,
  };
};

const hideDialog = () => {
  return {
    type: dialogConstants.DIALOG_HIDE,
    payload: dialogsTypes.REQUEST,
  };
};

// State Getter
// TODO: implement
const getUiSelectedMonth = (state) => {
  return state.ui.request.selectedMonth;
};
const getUiComment = (state) => {
  return state.ui.request.comment;
};

const postRequest = (targetDate, comment) => {
  const param = { targetDate, comment };
  const req = { path: '/time-track/monthly/apply', param };

  return Api.invoke(req);
};

export const editComment = (payload = '') => {
  return {
    type: constants.REQUEST_EDIT_COMMENT,
    payload,
  };
};

export const cancel = () => (dispatch) => {
  dispatch(hideDialog());
  dispatch({ type: constants.REQUEST_CANCEL });
};

export const prepare = () => (dispatch, getState) => {
  const state = getState();

  dispatch(showDialog());
  dispatch({
    type: constants.REQUEST_PREPARE,
    payload: state.timeTrack.selectedMonth,
  });
};

export const submit = () => (dispatch, getState) => {
  const state = getState();

  const targetDate = getUiSelectedMonth(state);
  const comment = getUiComment(state);

  dispatch(appActions.loadingStart());

  postRequest(targetDate, comment)
    .then(() => {
      dispatch(hideDialog());
      dispatch({ type: constants.REQUEST_SUBMIT_SUCCESS });
      dispatch(selectMonth(targetDate));
    })
    .catch((err) =>
      dispatch(appActions.catchApiError(err, { isContinuable: true }))
    )
    .then(() => dispatch(appActions.loadingEnd()));
};
