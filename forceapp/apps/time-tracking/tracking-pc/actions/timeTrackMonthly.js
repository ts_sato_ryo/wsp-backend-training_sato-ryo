/* @flow */
import _ from 'lodash';
import moment from 'moment';

import type { Dispatch } from 'redux';

import {
  fetch as fetchMonthlyTrack,
  type MonthlyTrack,
} from '../../../domain/models/time-tracking/MonthlyTrack';
import { type Period } from '../../../domain/models/time-tracking/Period';

import * as appActions from '../../../commons/actions/app';
import * as taskListActions from './taskList';
import {
  set as setDailyList,
  clear as clearDailyList,
} from '../modules/timeTrack/dailyTrackList';
import { set as setOverview } from '../modules/timeTrack/overview';
import { set as setRequest } from '../modules/timeTrack/request';
import { set as setPeriods } from '../modules/timeTrack/periods';
import { set as setSelectedMonth } from '../modules/timeTrack/selectedMonth';

// 選択可能な期間の中から、指定した日付が含まれる期間を取り出す
const pickDayFromPeriods = (
  targetDate: string,
  periods: Period[] // 降順
): string | null => {
  const targetPeriod = _.find(periods, (period) =>
    moment(period.startDate).isSameOrBefore(targetDate)
  );

  return targetPeriod !== undefined ? targetPeriod.startDate : null;
};

const fetchSuccess = (res: MonthlyTrack, targetDate: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(taskListActions.setSummaryTask(res.dailyTrackList));

  dispatch(setDailyList(res.dailyTrackList));
  dispatch(setOverview(res.overview));
  dispatch(setPeriods(res.periods));
  dispatch(setRequest(res.requestId));

  // selectThisMonth() によってfetchされた場合、targetDateがperiods[]の中にない日付になっている可能性があるため、
  // periodsの中から適切な日付を取り出してセットする
  const selectedMonth = pickDayFromPeriods(targetDate, res.periods);
  if (selectedMonth !== null) {
    dispatch(setSelectedMonth(selectedMonth));
  }
};

// -- public
/**
 * 月次工数データの取得
 * @param {string} targetDate
 */
export const fetch = (targetDate: string) => (dispatch: Dispatch<any>) => {
  dispatch(appActions.loadingStart());

  return fetchMonthlyTrack(targetDate)
    .then((res) => dispatch(fetchSuccess(res, targetDate)))
    .catch((err) =>
      dispatch(appActions.catchApiError(err, { isContinuable: false }))
    )
    .then(() => dispatch(appActions.loadingEnd()));
};

/**
 * 月選択
 * @param {string} targetDate ISO形式の日付、起算日計算があるため日を投げる
 */
export const selectMonth = (targetDate: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(clearDailyList());
  dispatch(fetch(targetDate));
};

/**
 * 次月を選択
 * @param {string} selectedDate 現在の指定日 YYYY-MM-DD
 * @param {array} periods 月度リスト(降順)
 */
export const selectNextMonth = (selectedDate: string, periods: Period[]) => (
  dispatch: Dispatch<any>
): void => {
  // 期間リストを昇順にした後で、指定日よりも後となる最初の期間を取得する
  const nextPeriod = _.cloneDeep(periods)
    .reverse()
    .find((period) => moment(period.startDate).isAfter(selectedDate));

  if (nextPeriod !== undefined) {
    dispatch(selectMonth(nextPeriod.startDate));
  }
};

/**
 * 今月を選択
 */
export const selectThisMonth = () => (dispatch: Dispatch<any>): void => {
  const today = moment().format('YYYY-MM-DD');
  dispatch(selectMonth(today));
};

/**
 * 前月を選択
 * @param {string} selectedDate 現在の指定日 YYYY-MM-DD
 * @param {array} periods 月度リスト(降順)
 */
export const selectPrevMonth = (selectedDate: string, periods: Period[]) => (
  dispatch: Dispatch<any>
): void => {
  // 指定日よりも前となる最初の期間を取得する
  const prevPeriod = _.find(periods, (period) =>
    moment(period.startDate).isBefore(selectedDate)
  );

  if (prevPeriod !== undefined) {
    dispatch(selectMonth(prevPeriod.startDate));
  }
};

/**
 * 現在選択されている月の情報を更新する
 * FIXME: stateに依存させないようにする
 */
export const reload = () => (dispatch: Dispatch<any>, getState: () => any) => {
  const state = getState();
  const { selectedMonth } = state.timeTrack;

  // 月が選択されていない場合は何もしない
  if (selectedMonth === '') {
    return;
  }

  dispatch(selectMonth(selectedMonth));
};
