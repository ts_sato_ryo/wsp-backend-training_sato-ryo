/* @flow */
import { type Dispatch } from 'redux';

import { getUserSetting } from '../../../commons/actions/userSetting';
import { selectThisMonth } from './timeTrackMonthly';

/* eslint-disable import/prefer-default-export */
export const init = () => (dispatch: Dispatch<any>) => {
  dispatch(getUserSetting()).then(() => {
    dispatch(selectThisMonth());
  });
};
