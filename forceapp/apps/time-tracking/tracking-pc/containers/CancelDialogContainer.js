import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import CancelDialog from '../components/CancelDialog';
import { actions as cancelActions } from '../modules/ui/cancel';

class RequestDialogContainer extends React.Component {
  static propTypes = {
    comment: PropTypes.string.isRequired,
    userPhotoUrl: PropTypes.string,
    cancelSubmit: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
    status: PropTypes.string.isRequired,
  };

  static defaultProps = {
    userPhotoUrl: '',
  };

  render() {
    return (
      <CancelDialog
        comment={this.props.comment}
        userPhotoUrl={this.props.userPhotoUrl}
        cancelSubmit={this.props.cancelSubmit}
        editComment={this.props.editComment}
        submit={this.props.submit}
        status={this.props.status}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    comment: state.ui.cancel.comment,
    userPhotoUrl: state.userSetting.photoUrl,
    status: state.timeTrack.overview.status,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cancelSubmit: bindActionCreators(cancelActions.cancelSubmit, dispatch),
    editComment: bindActionCreators(cancelActions.editComment, dispatch),
    submit: bindActionCreators(cancelActions.submit, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestDialogContainer);
