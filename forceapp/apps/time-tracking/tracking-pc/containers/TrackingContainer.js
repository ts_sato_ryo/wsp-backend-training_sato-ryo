/* @flow */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Tracking from '../components';

import { init } from '../actions/app';
import * as timeTrackMonthlyActions from '../actions/timeTrackMonthly';

const mapStateToProps = (state) => ({
  selectedMonth: state.timeTrack.selectedMonth,
  startDate: state.timeTrack.overview.startDate,
  periods: state.timeTrack.periods,
  dialogs: state.ui.dialogs,
  requestId: state.timeTrack.request.id,
});

const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
  ...bindActionCreators(
    {
      init,
      selectMonth: timeTrackMonthlyActions.selectMonth,
      onClickPeriodNextButton: timeTrackMonthlyActions.selectNextMonth,
      onClickPeriodCurrentButton: timeTrackMonthlyActions.selectThisMonth,
      onClickPeriodPrevButton: timeTrackMonthlyActions.selectPrevMonth,
    },
    dispatch
  ),
});

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickPeriodNextButton: () =>
    dispatchProps.onClickPeriodNextButton(
      stateProps.selectedMonth,
      stateProps.periods
    ),
  onClickPeriodPrevButton: () =>
    dispatchProps.onClickPeriodPrevButton(
      stateProps.selectedMonth,
      stateProps.periods
    ),
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Tracking): React.ComponentType<Object>);
