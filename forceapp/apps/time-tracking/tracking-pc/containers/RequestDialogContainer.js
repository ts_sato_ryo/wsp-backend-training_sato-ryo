import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import RequestDialog from '../components/RequestDialog';
import * as requestActions from '../actions/request';

class RequestDialogContainer extends React.Component {
  static propTypes = {
    comment: PropTypes.string.isRequired,
    userPhotoUrl: PropTypes.string,
    cancel: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    userPhotoUrl: '',
  };

  render() {
    return (
      <RequestDialog
        comment={this.props.comment}
        userPhotoUrl={this.props.userPhotoUrl}
        cancel={this.props.cancel}
        editComment={this.props.editComment}
        submit={this.props.submit}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    comment: state.ui.request.comment,
    userPhotoUrl: state.userSetting.photoUrl,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cancel: bindActionCreators(requestActions.cancel, dispatch),
    editComment: bindActionCreators(requestActions.editComment, dispatch),
    submit: bindActionCreators(requestActions.submit, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestDialogContainer);
