// @flow

import * as React from 'react';
import { bindActionCreators, type Dispatch } from 'redux';
import { connect } from 'react-redux';

import type { ApprovalHistory } from '../../../domain/models/approval/request/History';
import type { State } from '../modules';
import { actions as approvalHistoryActions } from '../modules/ui/approvalHistory';
import ApprovalHistoryDialog from '../../../../widgets/dialogs/ApprovalHistoryDialog';
import { selectors as historyListSelectors } from '../../../../widgets/dialogs/ApprovalHistoryDialog/modules/entities/historyList';

type Props = {
  historyList: ApprovalHistory[],
  close: Function,
};

class ApprovalHistoryDialogContainer extends React.Component<Props> {
  render() {
    return (
      <ApprovalHistoryDialog
        historyList={this.props.historyList}
        onHide={this.props.close}
      />
    );
  }
}

const mapStateToProps = (state: State) => {
  return {
    historyList: historyListSelectors.historyListSelector(state),
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => {
  return {
    close: bindActionCreators(approvalHistoryActions.close, dispatch),
  };
};

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(ApprovalHistoryDialogContainer): React.ComponentType<Object>);
