import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import List from '../components/List';

class ListContainer extends React.Component {
  static propTypes = {
    dailyTrackList: PropTypes.object.isRequired,
    taskList: PropTypes.object.isRequired,
    taskIdList: PropTypes.array.isRequired,
    allTaskSum: PropTypes.number.isRequired,
    startDate: PropTypes.string,
    endDate: PropTypes.string,
  };

  static defaultProps = {
    startDate: '',
    endDate: '',
  };

  render() {
    return (
      <List
        dailyTrackList={this.props.dailyTrackList}
        taskList={this.props.taskList}
        taskIdList={this.props.taskIdList}
        allTaskSum={this.props.allTaskSum}
        startDate={this.props.startDate}
        endDate={this.props.endDate}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dailyTrackList: state.timeTrack.dailyTrackList,
    taskList: state.timeTrack.taskList.byId,
    taskIdList: state.timeTrack.taskList.allIds,
    allTaskSum: state.timeTrack.allTaskSum,
    startDate: state.timeTrack.overview.startDate,
    endDate: state.timeTrack.overview.endDate,
  };
};

export default connect(mapStateToProps)(ListContainer);
