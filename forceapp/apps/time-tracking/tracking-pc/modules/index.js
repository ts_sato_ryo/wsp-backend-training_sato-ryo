// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';

import common from '../../../commons/reducers';
import userSetting from '../../../commons/reducers/userSetting';
import timeTrack from './timeTrack';
import ui from './ui';
import widgets from './widgets';

// eslint-disable-next-line no-unused-vars
function env(state: Object = {}, action: Object) {
  return state;
}

const rootReducer = {
  env,
  common,
  userSetting,
  timeTrack,
  ui,
  widgets,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
