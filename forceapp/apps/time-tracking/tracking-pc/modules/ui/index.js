import { combineReducers } from 'redux';

import dialogs from '../../../../commons/reducers/ui/dialogs';
import request from './request';
import cancel from './cancel';
import approvalHistory from './approvalHistory';

const rootReducer = combineReducers({
  dialogs,
  request,
  cancel,
  approvalHistory,
});

export default rootReducer;
