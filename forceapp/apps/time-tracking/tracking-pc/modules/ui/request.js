import * as constants from '../../constants';

const initialState = {
  comment: '',
  selectedMonth: '',
};

export default function requestReducer(state = initialState, action) {
  switch (action.type) {
    case constants.REQUEST_PREPARE:
      return Object.assign({}, state, { selectedMonth: action.payload });
    case constants.REQUEST_CANCEL:
    case constants.REQUEST_SUBMIT_SUCCESS:
      return initialState;
    case constants.REQUEST_EDIT_COMMENT:
      return Object.assign({}, state, { comment: action.payload });
    default:
      return state;
  }
}
