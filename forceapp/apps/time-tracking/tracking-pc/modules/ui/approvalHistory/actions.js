import Api from '../../../../../commons/api';
import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../../../../commons/actions/app';
import { actions as historyListActions } from '../../../../../../widgets/dialogs/ApprovalHistoryDialog/modules/entities/historyList';
import * as dialogsTypes from '../../../constants/dialogTypes';
import * as dialogConstants from '../../../../../commons/constants/dialog';

const open = (requestId) => (dispatch) => {
  dispatch({
    type: dialogConstants.DIALOG_SHOW,
    payload: dialogsTypes.APPROVAL_HISTORY,
  });

  const req = {
    path: '/approval/request/history/get',
    param: {
      requestId,
    },
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then((res) => {
      dispatch(historyListActions.set(res.historyList));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => {
      dispatch(loadingEnd());
    });
};

const close = () => (dispatch) => {
  dispatch(historyListActions.unset());
  dispatch({
    type: dialogConstants.DIALOG_HIDE,
    payload: dialogsTypes.APPROVAL_HISTORY,
  });
};

export { open, close };
