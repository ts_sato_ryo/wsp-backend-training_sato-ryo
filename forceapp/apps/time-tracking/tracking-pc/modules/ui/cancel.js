import Api from '../../../../commons/api';
import * as appActions from '../../../../commons/actions/app';
import * as dialogsTypes from '../../constants/dialogTypes';
import * as dialogConstants from '../../../../commons/constants/dialog';
import { reload } from '../../actions/timeTrackMonthly';

//
// -- constants
//
const PREPARE = 'MODULES/UI/CANCEL/PREPARE';
const CANCEL = 'MODULES/UI/CANCEL/CANCEL';
const SUBMIT_SUCCESS = 'MODULES/UI/CANCEL/REQUEST_SUBMIT_SUCCESS';
const EDIT_COMMENT = 'MODULES/UI/CANCEL/REQUEST_EDIT_COMMENT';

export const constants = {
  PREPARE,
  CANCEL,
  SUBMIT_SUCCESS,
  EDIT_COMMENT,
};

const showDialog = () => {
  return {
    type: dialogConstants.DIALOG_SHOW,
    payload: dialogsTypes.CANCEL,
  };
};

const hideDialog = () => {
  return {
    type: dialogConstants.DIALOG_HIDE,
    payload: dialogsTypes.CANCEL,
  };
};

//
// -- action
//
const postRequset = (requestId, comment) => {
  const param = { requestId, comment };
  const req = { path: '/time/request/cancel', param };

  return Api.invoke(req);
};

export const editComment = (payload = '') => {
  return {
    type: EDIT_COMMENT,
    payload,
  };
};

export const cancelSubmit = () => (dispatch) => {
  dispatch(hideDialog());
  dispatch({ type: constants.REQUEST_CANCEL });
};

export const prepare = () => (dispatch, getState) => {
  const state = getState();

  dispatch(showDialog());
  dispatch({
    type: PREPARE,
    payload: state.timeTrack.request.id,
  });
};

export const submit = () => (dispatch, getState) => {
  const state = getState();

  const { id, comment } = state.ui.cancel;

  dispatch(appActions.loadingStart());

  postRequset(id, comment)
    .then(() => {
      dispatch(hideDialog());
      dispatch({ type: SUBMIT_SUCCESS });

      dispatch(reload());
    })
    .catch((err) => {
      dispatch(appActions.catchApiError(err, { isContinuable: true }));
    })
    .then(() => dispatch(appActions.loadingEnd()));
};

export const actions = {
  editComment,
  cancelSubmit,
  prepare,
  submit,
};

//
// -- reducer
//
const initialState = {
  comment: '',
  id: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PREPARE:
      console.log(action.payload);
      return Object.assign({}, initialState, { id: action.payload });
    case CANCEL:
    case SUBMIT_SUCCESS:
      return initialState;
    case EDIT_COMMENT:
      return Object.assign({}, state, { comment: action.payload });
    default:
      return state;
  }
};
