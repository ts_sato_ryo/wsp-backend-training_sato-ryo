import * as constants from '../../constants';

const initialState = 0;

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.TIME_TRACK_MONTHLY_CLEAR:
      return initialState;
    case constants.TIME_TRACK_MONTHLY_SET_SUMMARY_SUM_TASK_TIME:
      return action.payload;
    default:
      return state;
  }
};
