import * as constants from '../../constants';
import {
  labelMapping,
  LABEL_UNAPPROVED,
} from '../../../../commons/constants/requestStatus';

export const set = (overview) => {
  return {
    type: constants.TIME_TRACK_MONTHLY_SET_OVERVIEW,
    payload: overview,
  };
};

export const statusApprovalLabelSelector = (state) => {
  const key = state.timeTrack.overview.status
    ? state.timeTrack.overview.status
    : '';
  return labelMapping[key] ? labelMapping[key] : LABEL_UNAPPROVED;
};

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.TIME_TRACK_MONTHLY_CLEAR:
      return initialState;
    case constants.TIME_TRACK_MONTHLY_SET_OVERVIEW:
      return action.payload;
    default:
      return state;
  }
};
