import * as constants from '../../../constants';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.TIME_TRACK_MONTHLY_CLEAR:
      return initialState;
    case constants.TIME_TRACK_MONTHLY_SET_SUMMARY_TASK:
      return action.payload.byId;
    default:
      return state;
  }
};
