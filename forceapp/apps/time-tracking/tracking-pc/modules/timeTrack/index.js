import { combineReducers } from 'redux';

import selectedMonth from './selectedMonth';
import periods from './periods';
import overview from './overview';
import dailyTrackList from './dailyTrackList';
import allTaskSum from './allTaskSum';
import taskList from './taskList';
import request from './request';

const timeTrackReducer = combineReducers({
  selectedMonth,
  periods,
  overview,
  dailyTrackList,
  allTaskSum,
  taskList,
  request,
});

export default timeTrackReducer;
