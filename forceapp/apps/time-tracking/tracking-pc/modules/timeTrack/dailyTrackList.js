import * as constants from '../../constants';

export const set = (dailyList) => ({
  type: constants.TIME_TRACK_MONTHLY_SET_DAILY_LIST,
  payload: dailyList,
});

export const clear = () => ({
  type: constants.TIME_TRACK_MONTHLY_CLEAR,
});

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.TIME_TRACK_MONTHLY_CLEAR:
      return initialState;
    case constants.TIME_TRACK_MONTHLY_SET_DAILY_LIST:
      return action.payload;
    default:
      return state;
  }
};
