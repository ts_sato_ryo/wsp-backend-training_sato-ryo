import * as constants from '../../constants';

export const set = (periods) => {
  return {
    type: constants.TIME_TRACK_MONTHLY_SET_PERIODS,
    payload: periods,
  };
};

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.TIME_TRACK_MONTHLY_CLEAR:
      return initialState;
    case constants.TIME_TRACK_MONTHLY_SET_PERIODS:
      return action.payload;
    default:
      return state;
  }
};
