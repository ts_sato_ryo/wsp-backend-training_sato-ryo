/* @flow */
import * as constants from '../../constants';

type State = string;

type Action = {
  type: string,
  payload: string,
};

export const set = (selectedMonth: string) => {
  return {
    type: constants.TIME_TRACK_MONTHLY_SELECT_MONTH,
    payload: selectedMonth,
  };
};

const initialState = '';

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case constants.TIME_TRACK_MONTHLY_SELECT_MONTH:
      return action.payload;
    default:
      return state;
  }
};
