// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import onDevelopment from '../../commons/config/development';

import Api from './api/local';
import TrackingContainer from './containers/TrackingContainer';

import '../../commons/styles/base.scss';
import '../../commons/config/moment';

function createLocalReactBridge(Component) {
  return {
    render() {
      const store = configureStore({
        env: {
          api: new Api(),
        },
      });
      const container = document.getElementById('container');
      if (container) {
        onDevelopment(() => {
          ReactDOM.render(
            <Provider store={store}>
              <Component />
            </Provider>,
            container
          );
        });
      }
    },
  };
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = () => {
  createLocalReactBridge(TrackingContainer).render();
};
