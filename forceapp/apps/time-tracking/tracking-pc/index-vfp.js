import '../../commons/config/public-path';

/* eslint-disable import/imports-first */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/* eslint-enable */

import configureStore from './store/configureStore';

import Api from './api/vfp';
import TrackingContainer from './containers/TrackingContainer';

import '../../commons/styles/base.scss';
import '../../commons/config/moment';

function createVfpReactBridge(Component) {
  return {
    init() {},
    render() {
      const store = configureStore({
        env: {
          api: new Api(),
        },
      });
      ReactDOM.render(
        <Provider store={store}>
          <Component />
        </Provider>,
        // eslint-disable-next-line no-undef
        document.getElementById('container')
      );
    },
  };
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = () => {
  createVfpReactBridge(TrackingContainer).render();
};
