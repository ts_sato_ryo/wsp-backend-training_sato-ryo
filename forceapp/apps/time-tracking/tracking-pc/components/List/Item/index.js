import React from 'react';
import PropTypes from 'prop-types';

import Summary from './Summary';
import Task from './Task';

import '../column.scss';
import './index.scss';

const ROOT = 'tracking-pc-list-item';
const COLUMN_ROOT = 'tracking-pc-list-column';

export default class Item extends React.Component {
  static propTypes = {
    dailyTrack: PropTypes.object.isRequired,
    taskList: PropTypes.object.isRequired,
    dateStr: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.renderTask = this.renderTask.bind(this);
  }

  renderTask(task, i) {
    return (
      <Task key={i} index={i} task={task} taskList={this.props.taskList} />
    );
  }

  render() {
    return (
      <section className={`${ROOT}`}>
        <header className={`${ROOT}__item ${COLUMN_ROOT}--date`}>
          <div className={`${ROOT}__item-date-inner`}>{this.props.dateStr}</div>
        </header>
        <div className={`${ROOT}__content`}>
          <div className={`${ROOT}__task-wrapper`}>
            {this.props.dailyTrack.recordItemList.map(this.renderTask)}
          </div>
          <Summary
            sumTaskTime={this.props.dailyTrack.sumTaskTime}
            note={this.props.dailyTrack.note}
          />
        </div>
      </section>
    );
  }
}
