import React from 'react';
import PropTypes from 'prop-types';

import TimeUtil from '../../../../../commons/utils/TimeUtil';
import TaskUtil from '../../../utils/TaskUtil';

import '../column.scss';
import './Task.scss';

const ROOT = 'tracking-pc-list-item-task';
const COLUMN_ROOT = 'tracking-pc-list-column';

export default class Task extends React.Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    taskList: PropTypes.object.isRequired,
    task: PropTypes.object.isRequired,
  };

  render() {
    const taskMeta = this.props.taskList[this.props.task.id];

    const barStyle = {
      width: `${this.props.task.graphRatio}%`,
      backgroundColor: taskMeta.barColor,
    };

    return (
      <div className={`${ROOT}`}>
        <div
          className={`${ROOT}__item ${ROOT}__item--name ${COLUMN_ROOT}--name`}
        >
          <div className={`${ROOT}__task-index`}>{this.props.index + 1}</div>
          <div className={`${ROOT}__task-name`}>
            {TaskUtil.createTaskName(this.props.task)}
          </div>
        </div>
        <div className={`${ROOT}__item ${COLUMN_ROOT}--tracking-graph`}>
          <div
            className={`${ROOT}__item ${ROOT}__tracking-graph`}
            style={barStyle}
          />
        </div>
        <div
          className={`${ROOT}__item ${ROOT}__item--work-time ${COLUMN_ROOT}--work-time`}
        >
          {TimeUtil.toHHmm(this.props.task.taskTime)}
        </div>
        <div className={`${ROOT}__item ${COLUMN_ROOT}--comment`} />
      </div>
    );
  }
}
