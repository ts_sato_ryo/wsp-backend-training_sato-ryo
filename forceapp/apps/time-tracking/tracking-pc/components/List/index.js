import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Header from './Header';
import Item from './Item';
import EmptyItem from './EmptyItem';
import Summary from './Summary';
import DateUtil from '../../../../commons/utils/DateUtil';

import './index.scss';

const ROOT = 'tracking-pc-list';

export default class List extends React.Component {
  static propTypes = {
    dailyTrackList: PropTypes.object.isRequired,
    taskList: PropTypes.object.isRequired,
    taskIdList: PropTypes.array.isRequired,
    allTaskSum: PropTypes.number.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string.isRequired,
  };

  renderItem(dailyTrack, i) {
    return <Item key={i} dailyTrack={dailyTrack} />;
  }

  renderCalendar() {
    if (this.props.startDate === '' || this.props.endDate === '') {
      return null;
    }

    const startDate = moment(this.props.startDate).startOf('day');
    const endDate = moment(this.props.endDate).endOf('day');

    const itemList = [];
    let monthCheck = null;
    while (startDate.isBefore(endDate)) {
      const nowMonth = startDate.month();
      let dateStr;
      if (nowMonth !== monthCheck) {
        monthCheck = nowMonth;
        dateStr = DateUtil.format(startDate.valueOf(), 'M/D dd');
      } else {
        dateStr = DateUtil.format(startDate.valueOf(), 'D dd');
      }

      const keyDateStr = DateUtil.formatISO8601Date(startDate.valueOf());
      const dailyTrack = this.props.dailyTrackList[keyDateStr];

      if (dailyTrack && dailyTrack.recordItemList.length > 0) {
        itemList.push(
          <Item
            key={dateStr}
            dateStr={dateStr}
            dailyTrack={dailyTrack}
            taskList={this.props.taskList}
          />
        );
      } else {
        itemList.push(
          <EmptyItem dateStr={dateStr} key={dateStr} note={dailyTrack.note} />
        );
      }

      startDate.add(1, 'days');
    }

    return itemList;
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        <Header />
        <div className={`${ROOT}__item-container`}>
          {this.renderCalendar()}
          <div className={`${ROOT}__summary-wrapper`}>
            <Summary
              taskList={this.props.taskList}
              taskIdList={this.props.taskIdList}
              allTaskSum={this.props.allTaskSum}
            />
          </div>
        </div>
      </div>
    );
  }
}
