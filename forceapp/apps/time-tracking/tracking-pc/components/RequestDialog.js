import React from 'react';
import PropTypes from 'prop-types';

import msg from '../../../commons/languages';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import Button from '../../../commons/components/buttons/Button';
import CommentNarrowField from '../../../commons/components/fields/CommentNarrowField';

import './RequestDialog.scss';

const ROOT = 'tracking-pc-request-dialog';

export default class RequestDialog extends React.Component {
  static propTypes = {
    comment: PropTypes.string.isRequired,
    userPhotoUrl: PropTypes.string.isRequired,
    cancel: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.onChangeComment = this.onChangeComment.bind(this);
  }

  onChangeComment(value) {
    this.props.editComment(value);
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        <DialogFrame
          className={`${ROOT}__dailog-frame`}
          title={msg().Trac_Lbl_Request}
          hide={this.props.cancel}
          footer={
            <DialogFrame.Footer>
              <Button type="default" onClick={this.props.cancel}>
                {msg().Com_Btn_Cancel}
              </Button>
              <Button type="primary" onClick={this.props.submit}>
                {msg().Com_Btn_Request}
              </Button>
            </DialogFrame.Footer>
          }
        >
          <div className={`${ROOT}__inner`}>
            <CommentNarrowField
              onChange={this.onChangeComment}
              value={this.props.comment}
              icon={this.props.userPhotoUrl}
            />
          </div>
        </DialogFrame>
      </div>
    );
  }
}
