import React from 'react';

import msg from '../../../commons/languages';

import './PCHeaderSub.scss';

const ROOT = 'tracking-pc-pc-header-sub';

export default class PCHeaderSub extends React.Component {
  render() {
    return (
      <div className={`${ROOT}`}>
        <h1 className={`${ROOT}__heading`}>{msg().Trac_Lbl_TimeTrack}</h1>
      </div>
    );
  }
}
