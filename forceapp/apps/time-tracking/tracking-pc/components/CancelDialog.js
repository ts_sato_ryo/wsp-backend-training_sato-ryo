import React from 'react';
import PropTypes from 'prop-types';

import msg from '../../../commons/languages';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import Button from '../../../commons/components/buttons/Button';
import CommentNarrowField from '../../../commons/components/fields/CommentNarrowField';
import { PENDING, APPROVED } from '../../../commons/constants/requestStatus';

import './CancelDialog.scss';

const ROOT = 'tracking-pc-cancel-dialog';

export default class CancelDialog extends React.Component {
  static propTypes = {
    comment: PropTypes.string.isRequired,
    userPhotoUrl: PropTypes.string.isRequired,
    cancelSubmit: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
    status: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.onChangeComment = this.onChangeComment.bind(this);
  }

  onChangeComment(value) {
    this.props.editComment(value);
  }

  render() {
    let buttonLabel = '';

    if (this.props.status === PENDING) {
      buttonLabel = msg().Com_Btn_RequestCancel;
    } else if (this.props.status === APPROVED) {
      buttonLabel = msg().Com_Btn_ApprovalCancel;
    }

    return (
      <div className={`${ROOT}`}>
        <DialogFrame
          className={`${ROOT}__dailog-frame`}
          title={msg().Trac_Lbl_Cancel}
          hide={this.props.cancelSubmit}
          footer={
            <DialogFrame.Footer>
              <Button type="default" onClick={this.props.cancelSubmit}>
                {msg().Com_Btn_Cancel}
              </Button>
              <Button type="primary" onClick={this.props.submit}>
                {buttonLabel}
              </Button>
            </DialogFrame.Footer>
          }
        >
          <div className={`${ROOT}__inner`}>
            <CommentNarrowField
              onChange={this.onChangeComment}
              value={this.props.comment}
              icon={this.props.userPhotoUrl}
            />
          </div>
        </DialogFrame>
      </div>
    );
  }
}
