import React from 'react';
import PropTypes from 'prop-types';

import * as dialogTypes from '../constants/dialogTypes';
import RequestDialogContainer from '../containers/RequestDialogContainer';
import CancelDialogContainer from '../containers/CancelDialogContainer';
import ApprovalHistoryDialogContainer from '../containers/ApprovalHistoryDialogContainer';

export default class DialogConductor extends React.Component {
  static get propTypes() {
    return {
      dialogs: PropTypes.object.isRequired,
    };
  }

  constructor(props) {
    super(props);

    this.renderDialog = this.renderDialog.bind(this);
  }

  renderDialog(type) {
    switch (type) {
      case dialogTypes.REQUEST:
        return (
          <RequestDialogContainer
            key={type}
            zIndex={this.props.dialogs[type]}
          />
        );
      case dialogTypes.CANCEL:
        return (
          <CancelDialogContainer key={type} zIndex={this.props.dialogs[type]} />
        );
      case dialogTypes.APPROVAL_HISTORY:
        return (
          <ApprovalHistoryDialogContainer
            key={type}
            zIndex={this.props.dialogs[type]}
          />
        );
      default:
        return null;
    }
  }

  render() {
    return <div>{Object.keys(this.props.dialogs).map(this.renderDialog)}</div>;
  }
}
