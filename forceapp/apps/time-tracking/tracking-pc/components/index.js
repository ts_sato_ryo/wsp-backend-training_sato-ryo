/* @flow */
import React from 'react';
import moment from 'moment';

import msg from '../../../commons/languages';

import GlobalContainer from '../../../commons/containers/GlobalContainer';
import GlobalHeader from '../../../commons/components/GlobalHeader';
import PeriodPicker from '../../../commons/components/fields/PeriodPicker';
import ListContainer from '../containers/ListContainer';
import PCHeaderSub from './PCHeaderSub';
import DialogConductor from './DialogConductor';

import ImgIconHeader from '../images/Track.svg';

import './index.scss';

const ROOT = 'tracking-pc';

type Props = {
  selectedMonth: string,
  periods: Array<{ name: string, startDate: string }>,
  init: () => {},
  selectMonth: (date: string) => void,
  onClickPeriodNextButton: () => void,
  onClickPeriodCurrentButton: () => void,
  onClickPeriodPrevButton: () => void,
  dialogs: { [dialogKey: string]: number },
};

export default class Tracking extends React.Component<Props> {
  static defaultProps = {
    selectedMonth: '',
  };

  constructor(props: Props) {
    super(props);

    this.onChangePeriodSelect = this.onChangePeriodSelect.bind(this);
  }

  componentWillMount() {
    this.props.init();
  }

  onChangePeriodSelect = (value: string): void => {
    this.props.selectMonth(value);
  };

  renderHeaderContent() {
    const isDisabledToClickPrev = this.props.periods.every((period) => {
      return moment(this.props.selectedMonth).isSameOrBefore(period.startDate);
    });

    const isDisabledToClickNext = this.props.periods.every((period) => {
      return moment(this.props.selectedMonth).isSameOrAfter(period.startDate);
    });

    return (
      <div className={`${ROOT}__month-picker-wrapper`}>
        <PeriodPicker
          selectOptions={this.props.periods.map((period) => {
            return { text: period.name, value: period.startDate };
          })}
          selectValue={this.props.selectedMonth}
          currentButtonLabel={msg().Time_Btn_CurrentPeriod}
          onClickCurrentButton={this.props.onClickPeriodCurrentButton}
          onClickPrevButton={this.props.onClickPeriodPrevButton}
          onClickNextButton={this.props.onClickPeriodNextButton}
          disabledNextButton={isDisabledToClickNext}
          disabledPrevButton={isDisabledToClickPrev}
          onChangeSelect={this.onChangePeriodSelect}
        />
      </div>
    );
  }

  render() {
    return (
      <GlobalContainer>
        <GlobalHeader
          content={this.renderHeaderContent()}
          iconAssistiveText={msg().Trac_Lbl_TimeTrack}
          iconSrc={ImgIconHeader}
          iconSrcType="svg"
          showPersonalMenuPopoverButton={false}
          showProxyIndicator={false}
        />
        <PCHeaderSub />
        <ListContainer />
        <DialogConductor dialogs={this.props.dialogs} />
      </GlobalContainer>
    );
  }
}
