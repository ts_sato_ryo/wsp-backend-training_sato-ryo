import React from 'react';

import {
  ReportListContainer,
  FormContainer,
  ReportSummaryContainer,
  RecordListContainer,
  RecordItemContainer,
  BaseCurrencyContainer,
  ForeignCurrencyContainer,
  RouteFormContainer,
  SuggestContainer,
  DialogContainer,
} from '../../containers/Expenses';

export default function expenseRequestHOC(WrappedComponent) {
  return class withRequestHOC extends React.Component {
    render() {
      return (
        <WrappedComponent
          {...this.props}
          baseCurrency={BaseCurrencyContainer}
          dialog={DialogContainer}
          fetchExpReportList={this.props.fetchExpReportList}
          foreignCurrency={ForeignCurrencyContainer}
          form={FormContainer}
          recordItem={RecordItemContainer}
          recordList={RecordListContainer}
          reportList={ReportListContainer}
          reportSummary={ReportSummaryContainer}
          routeForm={RouteFormContainer}
          suggest={SuggestContainer}
        />
      );
    }
  };
}
