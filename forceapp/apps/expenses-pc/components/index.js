/* @flow */
import React from 'react';

import GlobalContainer from '../../commons/containers/GlobalContainer';
import GlobalHeader from '../../commons/components/GlobalHeader';
import msg from '../../commons/languages';

import ExpensesContainer from '../containers/Expenses/ExpensesContainer';
import ExpensesHeaderContainer from '../containers/Expenses/HeaderContainer';
import ToastContainer from '../../commons/containers/ToastContainer';

import iconHeaderExp from '../images/Expense.svg';

type Props = {
  userSetting: {
    id: string,
    employeeId: string,
    useExpense: boolean,
  },
  initialize: () => void,
};

export default class App extends React.Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    const { userSetting } = this.props;

    if (!userSetting.id) {
      return null;
    }

    return (
      <GlobalContainer>
        <GlobalHeader
          content={<ExpensesHeaderContainer />}
          iconAssistiveText={msg().Appr_Lbl_Expense}
          iconSrc={iconHeaderExp}
          iconSrcType="svg"
          showPersonalMenuPopoverButton={false}
          showProxyIndicator={false}
        />
        <ExpensesContainer />
        <ToastContainer />
      </GlobalContainer>
    );
  }
}
