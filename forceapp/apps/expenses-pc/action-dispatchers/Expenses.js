// @flow
import isEmpty from 'lodash/isEmpty';
import UrlUtil from '../../commons/utils/UrlUtil';
import FileUtil from '../../commons/utils/FileUtil';

import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';

import { actions as modeActions } from '../modules/ui/expenses/mode';
import { actions as tabActions } from '../modules/ui/expenses/tab';
import { actions as selectedExpReportActions } from '../modules/ui/expenses/selectedExpReport';
import { actions as receiptStatusActions } from '../modules/ui/expenses/receiptLibrary/status';

import {
  actions as expReportActions,
  status as appStatus,
} from '../../domain/modules/exp/report';
import { actions as requestActions } from '../../domain/modules/exp/request/report';
import { actions as costCenterActions } from '../../domain/modules/exp/cost-center/list';
import { actions as expenseTypeActions } from '../../domain/modules/exp/expense-type/list';
import { actions as childExpenseTypeActions } from '../../domain/modules/exp/expense-type/childList';
import { actions as jobActions } from '../../domain/modules/exp/job/list';
import { actions as receiptLibraryAction } from '../../domain/modules/exp/receiptLibrary/list';
import { actions as taxActions } from '../modules/ui/expenses/recordItemPane/tax';
import { actions as overlapActions } from '../modules/ui/expenses/overlap';
import { actions as activeDialogActions } from '../modules/ui/expenses/dialog/activeDialog';
import { actions as expHistoryActions } from '../../domain/modules/exp/finance-approval';
import { actions as approvalHistoryActions } from '../../domain/modules/exp/approval/request/history';
import { actions as eiSelectActions } from '../modules/ui/expenses/dialog/extendedItem/search';
import { actions as eiRecentlyUsedActions } from '../modules/ui/expenses/dialog/extendedItem/recentlyUsed';
import { actions as vendorSelectActions } from '../modules/ui/expenses/dialog/vendor/search';
import { actions as vendorRecentlyUsedActions } from '../modules/ui/expenses/dialog/vendor/recentlyUsed';
import { actions as costCenterSelectListActions } from '../modules/ui/expenses/dialog/costCenterSelect/list';
import { actions as expenseTypeSelectListActions } from '../modules/ui/expenses/dialog/expenseTypeSelect/list';
import { actions as expenseTypeSelectRecordTypeActions } from '../modules/ui/expenses/dialog/expenseTypeSelect/recordType';
import { actions as jobSelectListActions } from '../modules/ui/expenses/dialog/jobSelect/list';
import { actions as openTitleActions } from '../modules/ui/expenses/recordListPane/summary/openTitle';
import { actions as recordUpdatedActions } from '../modules/ui/expenses/dialog/recordUpdated/dialog';
// Pagination
// temporarily import from FA because we haven't start work on Sort, Order and AdvSearch for Expense and Request yet. Remove it when we start
import {
  type SearchConditions,
  type SortBy,
  type OrderBy,
} from '../../domain/models/exp/FinanceApproval';
import { type ReportIdList } from '../../domain/models/exp/Report';
import { actions as reportIdListActions } from '../modules/entities/reportIdList';
import {
  actions as pageActions,
  PAGE_SIZE,
} from '../modules/ui/expenses/reportList/page';
import { actions as orderByActions } from '../modules/ui/expenses/reportList/orderBy';
import { actions as sortByActions } from '../modules/ui/expenses/reportList/sortBy';

// Prop Types
import type { Report, ReportListItem } from '../../domain/models/exp/Report';
import { OCR_STATUS } from '../../domain/models/exp/Receipt';
import { type Record, cloneRecord } from '../../domain/models/exp/Record';
import type { Base64FileList } from '../../domain/models/exp/Receipt';
import type {
  ExpenseType,
  ExpenseTypeList,
} from '../../domain/models/exp/ExpenseType';
import type {
  CostCenter,
  CostCenterList,
  DefaultCostCenter,
} from '../../domain/models/exp/CostCenter';
import type { ExpenseReportType } from '../../domain/models/exp/expense-report-type/list';
import {
  type CustomEIOptionList,
  type EISearchObj,
  getCustomEIOptionList,
  getRecentlyUsedCustomEI,
} from '../../domain/models/exp/ExtendedItem';
import {
  getVendorList,
  getVendorDetail,
  getRecentlyUsedVendor,
  type VendorItemList,
} from '../../domain/models/exp/Vendor';

const getActiveReportTypes = (reportTypeList: ?Array<ExpenseReportType>) =>
  reportTypeList ? reportTypeList.filter((rt) => rt.active) || null : null;

export const createNewExpReport = (
  reportTypeList: ?Array<ExpenseReportType>,
  defaultCostCenter: DefaultCostCenter
) => (dispatch: Dispatch<any>) => {
  const reportType = reportTypeList
    ? reportTypeList.find((rt) => rt.active) || null
    : null;
  dispatch(modeActions.reportSelect());
  dispatch(
    selectedExpReportActions.newReport({
      defaultCostCenter,
      reportType,
    })
  );
};

// Pagination
export const fetchExpReportIdList = (
  isApproved: boolean,
  sortBy?: ?SortBy,
  orderBy?: ?OrderBy,
  advSearchConditions?: ?SearchConditions
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    reportIdListActions.list(isApproved, sortBy, orderBy, advSearchConditions)
  )
    .then((res) => {
      // if there are reports, list up to page size and set page
      if (res.payload.reportIdList.length > 0) {
        dispatch(loadingStart());
        dispatch(
          expReportActions.list(res.payload.reportIdList.slice(0, PAGE_SIZE))
        ).then(() => {
          dispatch(loadingEnd());
          dispatch(pageActions.set(1));
          if (sortBy && orderBy) {
            dispatch(sortByActions.set(sortBy));
            dispatch(orderByActions.set(orderBy));
          }
        });
      } else if (res.payload.reportIdList.length === 0) {
        dispatch(expReportActions.initialize());
      }
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const fetchExpReportList = (
  reportIdList: ReportIdList,
  pageNum: number
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    expReportActions.list(
      reportIdList.slice(PAGE_SIZE * (pageNum - 1), PAGE_SIZE * pageNum)
    )
  )
    .then(() => dispatch(pageActions.set(pageNum)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(() => dispatch(loadingEnd()));
};

export const backFromDetailToList = (
  reportIdList: ReportIdList,
  currentRequestIdx: number,
  expReportList: Array<ReportListItem>
) => (dispatch: Dispatch<any>) => {
  const isNewReport = currentRequestIdx === -1;
  const pageNum = Math.ceil((currentRequestIdx + 1) / PAGE_SIZE);

  dispatch(overlapActions.nonOverlapReport());

  if (isNewReport) {
    dispatch(modeActions.reportSelect());
  } else {
    // only fetch id-list API when reportId is not inside expReportList
    const isCurrentReportOnList = expReportList.find(
      (exp) => exp.reportId === reportIdList[currentRequestIdx]
    );
    if (!isCurrentReportOnList) {
      dispatch(fetchExpReportList(reportIdList, pageNum));
    }
  }
};

const getReport = (status: ?string, reportId: ?string): void | any =>
  status === appStatus.APPROVED_PRE_REQUEST
    ? expReportActions.getApprovedRequest(reportId)
    : expReportActions.get(reportId, 'REPORT');

export const cloneReport = (
  reportId: string,
  reportTypeList: Array<ExpenseReportType>
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(expReportActions.clone(reportId))
    .then((result) =>
      Promise.all([
        dispatch(fetchExpReportIdList(false)),
        dispatch(expReportActions.get(result.reportId, 'REPORT')).then(
          (saveExpReportResult) => {
            dispatch(
              selectedExpReportActions.select(
                saveExpReportResult.payload,
                getActiveReportTypes(reportTypeList)
              )
            );
            dispatch(modeActions.reportSelect());
            dispatch(openTitleActions.open());
          }
        ),
      ])
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
        .then(() => {
          dispatch(overlapActions.nonOverlapRecord());
          dispatch(loadingEnd());
          dispatch(tabActions.setActive());
        })
    )
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const fetchExpReport = (
  status: string,
  reportId: string,
  reportTypeList: ?Array<ExpenseReportType>
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(getReport(status, reportId))
    .then((result) => {
      dispatch(
        selectedExpReportActions.select(
          result.payload,
          getActiveReportTypes(reportTypeList)
        )
      );
      dispatch(modeActions.reportSelect());
      // if (status === appStatus.APPROVED_PRE_REQUEST) {
      //   dispatch(modeActions.reportEdit());
      // }
    })
    .catch((err) => {
      // if we couldn't get report, try using preApproved instead
      if (!status && err.errorCode === 'EXP_NOT_FOUND_REPORT') {
        dispatch(
          fetchExpReport(
            appStatus.APPROVED_PRE_REQUEST,
            reportId,
            reportTypeList
          )
        );
      } else {
        dispatch(catchApiError(err, { isContinuable: true }));
      }
    })
    .then(() => dispatch(loadingEnd()));
};

// 保存
export const saveExpReport = (
  expReport: Report,
  reportTypeList: ?Array<ExpenseReportType>,
  expReportList: Array<ReportListItem>,
  reportIdList: Array<string>
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  const isNewReport = !expReport.reportId;
  return dispatch(expReportActions.save(expReport))
    .then(() => {
      // for new created report, it's going to be the first one in the report list
      // for the excisting report, it remains the same place as ordered
      if (isNewReport) {
        // update reportIdList
        dispatch(reportIdListActions.addNewId(expReport.reportId));
        // update expReportList for pagination
        // if current page list is not the first page, fetch the first page list and update
        const isListInFirstPage =
          reportIdList.length === 0 ||
          reportIdList[0] === expReportList[0].reportId;
        if (isListInFirstPage) {
          dispatch(expReportActions.updateList(expReport, true));
        } else {
          dispatch(
            expReportActions.list(reportIdList.slice(0, PAGE_SIZE))
          ).then(() => dispatch(expReportActions.updateList(expReport, true)));
        }
        dispatch(pageActions.set(1));
      } else {
        dispatch(expReportActions.updateList(expReport));
      }
      dispatch(
        selectedExpReportActions.select(
          expReport,
          getActiveReportTypes(reportTypeList)
        )
      );
      dispatch(modeActions.reportSelect());
      dispatch(overlapActions.nonOverlapRecord());
      dispatch(loadingEnd());
    })
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      dispatch(loadingEnd());
    });
};

export const saveExpRecord = (
  recordItem: Record,
  reportTypeList: ?Array<ExpenseReportType>,
  reportId: string,
  reportTypeId: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  return dispatch(
    expReportActions.saveRecord(recordItem, reportId, reportTypeId)
  )
    .then(() =>
      dispatch(expReportActions.get(reportId, 'REPORT'))
        .then((saveExpReportResult) => {
          dispatch(
            selectedExpReportActions.select(
              saveExpReportResult.payload,
              getActiveReportTypes(reportTypeList)
            )
          );
          dispatch(expReportActions.updateList(saveExpReportResult.payload));
          dispatch(modeActions.reportSelect());
        })
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
        .then(() => {
          dispatch(overlapActions.nonOverlapRecord());
          dispatch(loadingEnd());
        })
    )
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      dispatch(loadingEnd());
    });
};

// Create Expense Report from Approved Pre-request
export const createReportFromRequest = (
  expReport: Report,
  reportTypeList: ?Array<ExpenseReportType>
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  return dispatch(expReportActions.createReport(expReport))
    .then((result) => {
      const { updatedRecords, reportId } = result;
      if (updatedRecords.length > 0) {
        dispatch(recordUpdatedActions.setCloneUpdate(updatedRecords));
      }
      Promise.all([
        dispatch(fetchExpReportIdList(false)),
        dispatch(expReportActions.get(reportId, 'REPORT')).then(
          (saveExpReportResult) => {
            dispatch(
              selectedExpReportActions.select(
                saveExpReportResult.payload,
                getActiveReportTypes(reportTypeList)
              )
            );
            dispatch(modeActions.reportSelect());
          }
        ),
      ])
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
        .then(() => {
          dispatch(overlapActions.nonOverlapRecord());
          dispatch(loadingEnd());
          if (!isEmpty(updatedRecords)) {
            dispatch(activeDialogActions.recordUpdated());
          }
        });
    })
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      dispatch(loadingEnd());
    });
};

// 申請削除
export const deleteExpReport = (reportId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(expReportActions.delete(reportId))
    .then(() => {
      Promise.all([
        dispatch(fetchExpReportIdList(false)),
        dispatch(modeActions.initialize()),
        dispatch(selectedExpReportActions.clear()),
        dispatch(overlapActions.nonOverlapReport()),
      ]);
    })
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
    })
    .finally(() => dispatch(loadingEnd()));
};

export const deleteExpRecord = (recordIds: Array<string>) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  return dispatch(expReportActions.deleteRecord(recordIds))
    .then(() => {
      dispatch(fetchExpReportIdList(false));
    })
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
    })
    .finally(() => dispatch(loadingEnd()));
};

// 申請
export const submitExpReport = (reportId: string, comment: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(activeDialogActions.hide());
  return dispatch(requestActions.submit(reportId, comment))
    .then(() =>
      Promise.all([
        dispatch(fetchExpReportIdList(false)),
        dispatch(expReportActions.get(reportId)).then(
          (submitExpReportResult) => {
            dispatch(
              selectedExpReportActions.select(submitExpReportResult.payload)
            );
            dispatch(modeActions.reportSelect());
          }
        ),
      ])
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
        .then(() => dispatch(loadingEnd()))
    )
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      dispatch(loadingEnd());
    });
};

// 申請
export const cancelExpReportRequest = (requestId: string, comment: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(activeDialogActions.hide());
  dispatch(requestActions.cancel(requestId, comment))
    .then(() => {
      Promise.all([
        dispatch(fetchExpReportIdList(false)),
        dispatch(modeActions.initialize()),
        dispatch(selectedExpReportActions.clear()),
        dispatch(overlapActions.nonOverlapReport()),
      ])
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
        .then(() => dispatch(loadingEnd()));
    })
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      dispatch(loadingEnd());
    });
};

export const openApprovalHistoryDialog = (requestId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(approvalHistoryActions.get(requestId))
    .then(() => dispatch(activeDialogActions.approvalHistory()))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const openEditHistoryDialog = (reportId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(expHistoryActions.getHistory(reportId))
    .then(() => dispatch(activeDialogActions.editHistory()))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const openCostCenterDialog = (
  targetDate: string,
  employeeId: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(costCenterActions.getRecentlyUsed(employeeId, targetDate))
    .then((result) => {
      const payload = result.payload.map((x) => {
        return {
          ...x,
          id: x.historyId,
        };
      });
      dispatch(costCenterSelectListActions.setRecentResult(payload));
      dispatch(activeDialogActions.costCenter());
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getCostCenterList = (parentId: string, targetDate: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(costCenterActions.list(parentId, targetDate))
    .then((result) => {
      dispatch(costCenterSelectListActions.set([result.payload]));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getCostCenterSearchResult = (
  companyId: ?string,
  keyword: string,
  targetDate: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    costCenterActions.searchCostCenter(companyId, keyword, targetDate, 'REPORT')
  )
    .then((result) => {
      const payload = result.payload.map((x) => {
        return {
          ...x,
          id: x.historyId,
        };
      });
      dispatch(costCenterSelectListActions.setSearchResult(payload));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const openExpenseTypeDialog = (
  employeeId: string,
  companyId: string,
  targetDate?: string,
  recordType: string,
  reportTypeId?: string,
  isChange: boolean // isChange: true->change expense type by search button in item pane. : false->select expense type by new record button
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(expenseTypeSelectRecordTypeActions.set(recordType));
  dispatch(
    expenseTypeActions.getRecentlyUsed(
      employeeId,
      companyId,
      targetDate,
      recordType,
      reportTypeId,
      'REPORT'
    )
  )
    .then((result) => {
      dispatch(expenseTypeSelectListActions.setRecentResult(result.payload));
      if (isChange) {
        dispatch(activeDialogActions.expenseTypeChange());
      } else {
        dispatch(activeDialogActions.expenseType());
      }
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getExpenseTypeList = (
  recordType: string = '',
  targetDate: string,
  reportTypeId?: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    expenseTypeActions.list(
      null,
      null,
      targetDate,
      recordType,
      'REPORT',
      reportTypeId
    )
  )
    .then((result) => {
      dispatch(expenseTypeSelectListActions.set([result.payload]));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getExpenseTypeSearchResult = (
  companyId: string,
  keyword: string,
  targetDate: string,
  expReportTypeId: string,
  recordtype?: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    expenseTypeActions.searchExpenseType(
      companyId,
      keyword,
      targetDate,
      'REPORT',
      expReportTypeId,
      recordtype
    )
  )
    .then((result) => {
      dispatch(expenseTypeSelectListActions.setSearchResult(result.payload));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getNextExpenseTypeList = (
  item: ExpenseType,
  items: ExpenseTypeList,
  recordType: ?string,
  reportTypeId?: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    expenseTypeActions.list(
      null,
      item.id,
      null,
      recordType,
      'REPORT',
      reportTypeId
    )
  )
    .then((result) => {
      items.push(result.payload);
      dispatch(expenseTypeSelectListActions.set(items));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const openRecordItemsCreateDialog = () => (dispatch: Dispatch<any>) => {
  dispatch(activeDialogActions.recordItemsCreate());
};

export const openRecordItemsConfirmDialog = () => (dispatch: Dispatch<any>) => {
  dispatch(activeDialogActions.recordItemsConfirm());
};

export const openRecordItemsDeleteDialog = () => (dispatch: Dispatch<any>) => {
  dispatch(activeDialogActions.recordItemsDelete());
};

export const openJobDialog = (targetDate: string, empId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  dispatch(jobActions.getRecentlyUsed(targetDate, empId))
    .then((result) => {
      dispatch(jobSelectListActions.setRecentResult(result.payload));
      dispatch(activeDialogActions.job());
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getJobList = (
  parentId: string,
  targetDate: string,
  empId?: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(jobActions.list(parentId, targetDate, empId))
    .then((result) => {
      dispatch(jobSelectListActions.set([result.payload]));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getJobSearchResult = (
  keyword: string,
  targetDate: string,
  employeeId: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(
    jobActions.getJobSearchResult(keyword, targetDate, employeeId, 'REPORT')
  )
    .then((result) => {
      dispatch(jobSelectListActions.setSearchResult(result.payload));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const searchTaxTypeList = (expTypeId: string, targetDate: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(taxActions.set(expTypeId, targetDate, []));
  dispatch(loadingStart());
  let taxTypeList;
  return dispatch(taxActions.list(expTypeId, targetDate))
    .then((result) => {
      taxTypeList = result;
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => {
      dispatch(loadingEnd());
      return taxTypeList;
    });
};

export const searchExpTypesByParentRecord = (
  targetDate: string,
  parentExpTypeId: string
) => (dispatch: Dispatch<any>) => {
  return dispatch(
    childExpenseTypeActions.searchByParent(
      targetDate,
      parentExpTypeId,
      'REPORT'
    )
  );
};

export const getNextCostCenterList = (
  item: CostCenter,
  items: CostCenterList,
  baseId: string,
  targetDate: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(costCenterActions.list(baseId, targetDate))
    .then((result) => {
      items.push(result.payload);
      dispatch(costCenterSelectListActions.set(items));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getNextJobList = (
  item: any,
  items: any,
  parentId: string,
  targetDate: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(jobActions.list(parentId, targetDate))
    .then((result) => {
      items.push(result.payload);
      dispatch(jobSelectListActions.set(items));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

/**
 * @param {String} periodStartDate The start date of target month
 * @param {String} [targetEmployeeId=null] The ID of target employee
 */
export const openPrintWindow = (reportId: string) => () =>
  UrlUtil.openApp('expenses-pc-print', {
    reportId,
  });

export const openReceiptLibraryDialog = () => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  return dispatch(receiptLibraryAction.list())
    .then(() => dispatch(activeDialogActions.receipts()))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const openOCRReceiptLibraryDialog = () => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  return dispatch(receiptLibraryAction.list())
    .then(() => dispatch(activeDialogActions.ocrReceipts()))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const getBase64File = (file: File) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  return FileUtil.getBase64(file)
    .then((data) => ({
      name: file.name,
      data: String(data),
      size: file.size,
      type: file.type,
    }))
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      throw err;
    })
    .finally(dispatch(loadingEnd()));
};

export const uploadReceipts = (base64FileList: Base64FileList) => async (
  // export const uploadReceipts = (base64FileList: Base64FileList) => (
  dispatch: Dispatch<any>
) => {
  const { uploadReceipt, getDocumentId } = receiptLibraryAction;
  dispatch(loadingStart());
  let res;
  try {
    const resUploadReceipt = await dispatch(uploadReceipt(base64FileList));
    const contentVersionId = resUploadReceipt.payload[0];
    const resGetDocumentId = await dispatch(getDocumentId([contentVersionId]));
    const contentDocumentId = resGetDocumentId.payload[0];
    res = { contentVersionId, contentDocumentId };
  } catch (err) {
    dispatch(catchApiError(err, { isContinuable: true }));
  } finally {
    dispatch(loadingEnd());
  }
  return res;
};

export const getFilePreview = (receiptFileId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  return dispatch(receiptLibraryAction.get(receiptFileId))
    .then((res) => res)
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(dispatch(loadingEnd()));
};

export const executeOcr = (receiptFileId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  return dispatch(receiptLibraryAction.executeOcr(receiptFileId))
    .then((res) => res)
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally(dispatch(loadingEnd()));
};

export const keepGettingStatus = (taskId: string, receiptFileId: string) => (
  dispatch: Dispatch<any>
) => {
  return dispatch(receiptLibraryAction.getOcrStatus(taskId)).then((res) => {
    dispatch(
      receiptStatusActions.set(receiptFileId, res.payload.ocrInfo.status)
    );
    if (res.payload.ocrInfo.status !== OCR_STATUS.COMPLETED) {
      setTimeout(
        () => dispatch(keepGettingStatus(taskId, receiptFileId)),
        3000
      );
    }
  });
};

export const deleteReceipt = (receiptId: string) => (dispatch: Dispatch<any>) =>
  dispatch(receiptLibraryAction.delete(receiptId)).catch((err) =>
    dispatch(catchApiError(err, { isContinuable: true }))
  );

export const openEILookupDialog = (item: EISearchObj, empId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(eiSelectActions.set(item));
  dispatch(loadingStart());
  getRecentlyUsedCustomEI(
    empId,
    item.extendedItemLookupId,
    item.extendedItemCustomId
  )
    .then((res) => dispatch(eiRecentlyUsedActions.set(res)))
    .then(() => dispatch(activeDialogActions.eiLookup()))
    .finally(() => dispatch(loadingEnd()));
};

export const searchEILookup = (id: string, query: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  return getCustomEIOptionList(id, query)
    .then((result: CustomEIOptionList) => {
      dispatch(loadingEnd());
      return result;
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
};

// Vendor Search
export const openVendorLookupDialog = (empId: string) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(vendorSelectActions.clear());
    dispatch(loadingStart());
    getRecentlyUsedVendor(empId)
      .then((res) => dispatch(vendorRecentlyUsedActions.set(res)))
      .then(() => dispatch(activeDialogActions.vendorLookup()))
      .finally(() => dispatch(loadingEnd()));
  };
};

export const searchVendorDetail = (vendorId: ?string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  return getVendorDetail(vendorId)
    .then((result: VendorItemList) => {
      dispatch(vendorSelectActions.set(result.records[0]));
      dispatch(loadingEnd());
      return result;
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
};

export const searchVendorLookup = (id: string, query: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());
  return getVendorList(id, query)
    .then((result: VendorItemList) => {
      dispatch(loadingEnd());
      return result;
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
};

export const cloneRecords = (
  targetDates: Array<string>,
  recordIds: Array<string>,
  reportId: string,
  reportTypeList: Array<ExpenseReportType>
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  dispatch(activeDialogActions.hide());
  return cloneRecord(recordIds, targetDates)
    .then((cloneRes) => {
      if (cloneRes.updatedRecords.length > 0) {
        dispatch(recordUpdatedActions.setCloneUpdate(cloneRes.updatedRecords));
      }
      return dispatch(expReportActions.get(reportId, 'REPORT')).then((res) => {
        dispatch(selectedExpReportActions.select(res.payload, reportTypeList));
        dispatch(modeActions.reportSelect());
        return cloneRes;
      });
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .finally((cloneRes) => {
      dispatch(loadingEnd());
      return cloneRes;
    });
};
