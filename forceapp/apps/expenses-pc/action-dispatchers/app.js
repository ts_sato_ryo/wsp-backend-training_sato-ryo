// @flow

import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';

import msg from '../../commons/languages';

import { getUserSetting } from '../../commons/actions/userSetting';
import { actions as expenseReportTypeActions } from '../../domain/modules/exp/expense-report-type/list';
import { actions as customHintActions } from '../../domain/modules/exp/customHint';

/** Define actions */

// eslint-disable-next-line import/prefer-default-export
export const initialize = () => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  // When an empty Array is sent to the API, it will only return the basic information and will not retrieve and populate the details.
  dispatch(getUserSetting({ detailSelectors: [] }))
    .then((result) => {
      if (result.employeeId === '') {
        dispatch(
          catchApiError(
            {
              errorCode: '',
              message: msg().Exp_Lbl_InitEmpSettingNotCompleted,
              statckTrace: null,
            },
            { isContinuable: false }
          )
        );
      }
      return result;
    })
    .then((result) => {
      dispatch(
        expenseReportTypeActions.list(
          result.companyId,
          'REPORT',
          true,
          result.employeeId
        )
      );
      dispatch(customHintActions.get(result.companyId, 'Expense'));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: false })))
    .then(() => {
      dispatch(loadingEnd());
    });
};

export default initialize;
