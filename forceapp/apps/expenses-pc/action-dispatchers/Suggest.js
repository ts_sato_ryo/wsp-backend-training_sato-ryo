// @flow
import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';

import { getSuggestion } from '../../domain/models/exp/jorudan/Station';

// eslint-disable-next-line import/prefer-default-export
export const getStation = (
  searchString: string,
  targetDate: string,
  category: ?string = null
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());
  let station = [];
  return getSuggestion(searchString, targetDate, category)
    .then((result) => {
      station = result;
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => {
      dispatch(loadingEnd());
      return station;
    });
};
