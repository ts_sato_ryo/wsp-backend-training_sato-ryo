// flow
import * as Yup from 'yup';
import { get } from 'lodash';

import {
  accountingDateSchema,
  amountSchema,
  fixedAmountSelectionSchema,
  expReportTypeIdSchema,
  taxTypeHistoryIdSchema,
  getExtendedItemsValidators,
  paymentDueDateSchema,
  receiptIdSchema,
  recordDateSchema,
  recordDateWithCheckWorkingDaysSchema,
  routeInfoSchema,
  subjectSchema,
  vendorIdSchema,
  expTypeIdSchema,
  jobIdSchema,
  costCenterNameSchema,
  recordItemsEditSchema,
  recordItemsCreateSchema,
} from '../../commons/schema/Expense';

import { status } from '../../domain/modules/exp/report';

//
// Report save schema
//
const saveSchema = (values) => {
  return Yup.object().shape({
    report: Yup.object().shape({
      subject: subjectSchema(),
      accountingDate: accountingDateSchema(),
      costCenterName: costCenterNameSchema(values),
      jobId: jobIdSchema(values),
      vendorId: vendorIdSchema(values),
      paymentDueDate: paymentDueDateSchema(values, false),
      expReportTypeId: expReportTypeIdSchema(values),
      ...getExtendedItemsValidators(),
    }),
  });
};

//
// submit
//
const submitSchema = (values) => {
  return Yup.object().shape({
    report: Yup.object().shape({
      subject: subjectSchema(),
      accountingDate: accountingDateSchema(),
      expReportTypeId: expReportTypeIdSchema(values),
      ...getExtendedItemsValidators(),
      records: Yup.array().of(
        Yup.object().shape({
          recordType: Yup.string(),
          items: Yup.array().when('recordType', {
            is: 'HotelFee',
            then: Yup.array().of(
              Yup.object().shape({
                amount: amountSchema(),
                expTypeId: expTypeIdSchema(values),
              })
            ),
            otherwise: Yup.array().of(
              Yup.object().shape({
                amount: amountSchema(),
                expTypeId: expTypeIdSchema(values),
                ...getExtendedItemsValidators(),
              })
            ),
          }),
          receiptId: receiptIdSchema(values),
          routeInfo: routeInfoSchema(),
          recordDate: recordDateSchema(values),
        })
      ),
    }),
  });
};

//
// input
//
const inputSchema = (values) => {
  return Yup.object().shape({
    report: Yup.object().shape({
      subject: subjectSchema(),
      accountingDate: accountingDateSchema(),
      expReportTypeId: expReportTypeIdSchema(values),
      ...getExtendedItemsValidators(),
      records: Yup.array().of(
        Yup.object().shape({
          recordType: Yup.string(),
          items: Yup.array().when('recordType', {
            is: 'HotelFee',
            then: Yup.array()
              .of(
                Yup.object().shape({
                  amount: amountSchema(),
                  expTypeId: expTypeIdSchema(values),
                })
              )
              .min(2, 'Exp_Err_RecordItemsMandatory'),
            otherwise: Yup.array().of(
              Yup.object().shape({
                amount: amountSchema(),
                ...getExtendedItemsValidators(),
                expTypeId: expTypeIdSchema(values),
              })
            ),
          }),
          receiptId: receiptIdSchema(values),
          routeInfo: routeInfoSchema(),
          recordDate: recordDateWithCheckWorkingDaysSchema(values),
        })
      ),
    }),
  });
};

//
// recordSave
//
const recordSaveSchema = (values) => {
  const { recordIdx } = values.ui;
  const selectedRecordId = get(values, `report.records[${recordIdx}].recordId`);

  return Yup.object().shape({
    report: Yup.object().shape({
      records: Yup.array().of(
        Yup.object().when('.', {
          // validate only current selected record
          is: (self) => self.recordId === selectedRecordId,
          then: Yup.object().shape({
            recordType: Yup.string(),
            items: Yup.array()
              .when('recordType', {
                is: 'HotelFee',
                then: Yup.array().of(
                  Yup.object().shape({
                    amount: amountSchema(),
                  })
                ),
                otherwise: Yup.array().of(
                  Yup.object().shape({
                    amount: amountSchema(),
                    taxTypeHistoryId: taxTypeHistoryIdSchema(),
                  })
                ),
              })
              .when('recordType', {
                is: 'FixedAllowanceMulti',
                then: Yup.array().of(
                  Yup.object().shape({
                    fixedAllowanceOptionId: fixedAmountSelectionSchema(),
                  })
                ),
              }),
            recordDate: recordDateSchema(values),
          }),
          otherwise: Yup.object(),
        })
      ),
    }),
  });
};

//
// submit schema for FA, ignore the records' expense type check
//
const submitFASchema = (values) => {
  return Yup.object().shape({
    report: Yup.object().shape({
      subject: subjectSchema(),
      accountingDate: accountingDateSchema(),
      expReportTypeId: expReportTypeIdSchema(values),
      ...getExtendedItemsValidators(),
      records: Yup.array().of(
        Yup.object().shape({
          recordType: Yup.string(),
          items: Yup.array().when('recordType', {
            is: 'HotelFee',
            then: Yup.array().of(
              Yup.object().shape({
                amount: amountSchema(),
              })
            ),
            otherwise: Yup.array().of(
              Yup.object().shape({
                amount: amountSchema(),
                ...getExtendedItemsValidators(),
              })
            ),
          }),
          receiptId: receiptIdSchema(values),
          routeInfo: routeInfoSchema(),
          recordDate: recordDateSchema(values),
        })
      ),
    }),
  });
};

//
// input schema for FA, ignore the records' expense type check
//
const inputFASchema = (values) => {
  return Yup.object().shape({
    report: Yup.object().shape({
      subject: subjectSchema(),
      accountingDate: accountingDateSchema(),
      expReportTypeId: expReportTypeIdSchema(values),
      ...getExtendedItemsValidators(),
      records: Yup.array().of(
        Yup.object().shape({
          recordType: Yup.string(),
          items: Yup.array().when('recordType', {
            is: 'HotelFee',
            then: Yup.array().of(
              Yup.object().shape({
                amount: amountSchema(),
              })
            ),
            otherwise: Yup.array().of(
              Yup.object().shape({
                amount: amountSchema(),
                ...getExtendedItemsValidators(),
              })
            ),
          }),
          receiptId: receiptIdSchema(values),
          routeInfo: routeInfoSchema(),
          recordDate: recordDateWithCheckWorkingDaysSchema(values),
        })
      ),
    }),
  });
};

export default Yup.lazy((values) => {
  // child record items create / edit
  if (values.ui.isRecordItemsEdit) {
    return recordItemsEditSchema(values);
  } else if (values.ui.isRecordItemsCreate) {
    return recordItemsCreateSchema(values);
  }
  const { report } = values;
  // if it has preRequestId and no reportId, it's a new approved preRequest
  const isNewPreRequest = report.preRequestId && !report.reportId;
  // For pending, claimed or any status after financial approval approved stage, no validation
  // ???????check status of approved record/report
  const isNonEditableReport =
    report.status === status.PENDING ||
    report.status === status.CLAIMED ||
    report.status === status.ACCOUNTING_AUTHORIZED ||
    report.status === status.ACCOUNTING_REJECTED ||
    report.status === status.JOURNAL_CREATED ||
    report.status === status.FULLY_PAID;

  // no validation if it's new preRequest or non-editable report or financial page
  // canot return null as lazy requires a valid schema. Hence, we return empty schema
  if (isNewPreRequest || isNonEditableReport) {
    return Yup.object().shape({});
  }

  // separates report & record save schema
  if (values.ui.isRecordSave) {
    // record save
    return recordSaveSchema(values);
  } else if (values.ui.saveMode) {
    // save
    return saveSchema(values);
  } else if (values.ui.submitMode) {
    // submit
    return values.report.status === status.APPROVED
      ? submitFASchema(values)
      : submitSchema(values);
  } else {
    // // Check when you are typing
    return values.report.status === status.APPROVED
      ? inputFASchema(values)
      : inputSchema(values);
  }
});
