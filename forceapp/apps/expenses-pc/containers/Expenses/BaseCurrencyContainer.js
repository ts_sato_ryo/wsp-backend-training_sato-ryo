// @flow
import { connect } from 'react-redux';
import { find, get, assign } from 'lodash';

import { mergeValues } from '../../models/expenses/ExpensesRequestForm';
import BaseCurrencyView from '../../../commons/components/exp/Form/RecordItem/General/BaseCurrency';

// calulating function for tax
import {
  calculateTax,
  calcTaxFromGstVat,
} from '../../../domain/models/exp/TaxType';
import { searchTaxTypeList } from '../../action-dispatchers/Expenses';

const mapStateToProps = (state, ownProps) => ({
  tax: state.ui.expenses.recordItemPane.tax,
  allowTaxAmountChange: state.userSetting.allowTaxAmountChange,
  onChangeEditingExpReport: (key, value) => {
    ownProps.onChangeEditingExpReport(`report.${key}`, value);
  },
});

const mapDispatchToProps = {
  searchTaxTypeList,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...ownProps,
  searchTaxTypeList: () => {
    if (ownProps.expRecord.items[0].expTypeId) {
      dispatchProps.searchTaxTypeList(
        ownProps.expRecord.items[0].expTypeId,
        ownProps.expRecord.recordDate
      );
    }
  },
  onChangeAmountOrTaxType: (
    amount: number,
    baseId: ?string = null,
    taxName: ?string = null
  ) => {
    const isParentItem = ownProps.recordItemIdx === 0;
    const recordItem = ownProps.expRecord.items[ownProps.recordItemIdx];
    const expTypeId = recordItem.expTypeId;

    // if no expType selected OR is itemized, only update value
    if (!expTypeId || ownProps.isItemized) {
      ownProps.onChangeEditingExpReport(
        `${ownProps.targetRecord}.items.${ownProps.recordItemIdx}.amount`,
        amount,
        true
      );
      if (isParentItem) {
        ownProps.onChangeEditingExpReport(
          `${ownProps.targetRecord}.amount`,
          amount,
          true
        );
      }
      return;
    }

    const taxTypeName = taxName || recordItem.taxTypeName;
    const taxTypeBaseId = baseId || recordItem.taxTypeBaseId;

    const recordDate = recordItem.recordDate || ownProps.expRecord.recordDate;
    let taxTypeList =
      stateProps.tax &&
      stateProps.tax[expTypeId] &&
      stateProps.tax[expTypeId][recordDate] &&
      stateProps.tax[expTypeId][recordDate];

    let taxRes = {};
    let historyId = '';

    // if tax state has not already existed, fetch it from API
    if (!taxTypeList) {
      dispatchProps.searchTaxTypeList(expTypeId, recordDate).then((result) => {
        taxTypeList = result.payload[expTypeId][recordDate];
        const taxType =
          find(taxTypeList, { baseId: taxTypeBaseId }) || taxTypeList[0];
        historyId = taxType ? taxType.historyId : null;
        const rate = taxType ? taxType.rate : 0;

        taxRes = calculateTax(rate, amount || 0, ownProps.baseCurrencyDecimal);
      });
    } else {
      const taxType =
        find(taxTypeList, { baseId: taxTypeBaseId }) || taxTypeList[0];
      historyId = taxType ? taxType.historyId : null;
      const rate = taxType ? taxType.rate : 0;

      taxRes = calculateTax(rate, amount || 0, ownProps.baseCurrencyDecimal);
    }
    const targetPath = `items.${ownProps.recordItemIdx}`;

    const updateObj = {
      [`${targetPath}.amount`]: amount || 0,
      [`${targetPath}.withoutTax`]: taxRes.amountWithoutTax,
      [`${targetPath}.gstVat`]: taxRes.gstVat,
      [`${targetPath}.taxTypeBaseId`]: taxTypeBaseId,
      [`${targetPath}.taxTypeHistoryId`]: historyId,
      [`${targetPath}.taxTypeName`]: taxTypeName,
    };
    if (isParentItem) {
      assign(updateObj, {
        amount: amount || 0,
        withoutTax: taxRes.amountWithoutTax,
      });
    }

    const mergedExpRecord = mergeValues(
      ownProps.expRecord,
      ownProps.touched,
      updateObj
    );

    ownProps.onChangeEditingExpReport(
      ownProps.targetRecord,
      mergedExpRecord.values,
      true,
      mergedExpRecord.touched
    );
  },
  onClickEditButton: () => {
    const idx = ownProps.recordItemIdx;
    const isParentItem = idx === 0;
    const taxManual = !ownProps.expRecord.items[idx].taxManual;
    if (!taxManual) {
      const recordDate =
        ownProps.expRecord.items[idx].recordDate ||
        ownProps.expRecord.recordDate;
      const expTypeId = ownProps.expRecord.items[idx].expTypeId;

      const taxTypeList = get(stateProps, `tax.${expTypeId}.${recordDate}`, []);
      const taxType = find(taxTypeList, {
        historyId: ownProps.expRecord.items[idx].taxTypeHistoryId,
      });
      const rate = taxType ? taxType.rate : 0;
      const taxRes = calculateTax(
        rate,
        ownProps.expRecord.items[idx].amount,
        ownProps.baseCurrencyDecimal
      );
      const targetPath = `items.${idx}`;
      const updateObj = {
        [`${targetPath}.withoutTax`]: taxRes.amountWithoutTax,
        [`${targetPath}.gstVat`]: taxRes.gstVat,
        [`${targetPath}.taxManual`]: taxManual,
      };
      if (isParentItem) {
        assign(updateObj, { withoutTax: taxRes.amountWithoutTax });
      }
      const mergedExpRecord = mergeValues(
        ownProps.expRecord,
        ownProps.touched,
        updateObj
      );

      ownProps.onChangeEditingExpReport(
        ownProps.targetRecord,
        mergedExpRecord.values,
        false,
        mergedExpRecord.touched
      );
    } else {
      const targetPath = `${ownProps.targetRecord}.items.${idx}.taxManual`;
      ownProps.onChangeEditingExpReport(targetPath, taxManual);
    }
  },
  calcTaxFromGstVat: (gstVat: number) => {
    const taxRes = calcTaxFromGstVat(
      gstVat,
      ownProps.expRecord.items[ownProps.recordItemIdx].amount,
      ownProps.baseCurrencyDecimal
    );

    const isParentItem = ownProps.recordItemIdx === 0;
    const targetPath = `items.${ownProps.recordItemIdx}`;
    const updateObj = {
      [`${targetPath}.withoutTax`]: taxRes.amountWithoutTax,
      [`${targetPath}.gstVat`]: gstVat,
    };
    if (isParentItem) {
      assign(updateObj, { withoutTax: taxRes.amountWithoutTax });
    }

    const mergedExpRecord = mergeValues(
      ownProps.expRecord,
      ownProps.touched,
      updateObj
    );

    ownProps.onChangeEditingExpReport(
      ownProps.targetRecord,
      mergedExpRecord.values,
      false,
      mergedExpRecord.touched
    );
  },
});
export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(BaseCurrencyView): React.ComponentType<*>): React.ComponentType<Object>);
