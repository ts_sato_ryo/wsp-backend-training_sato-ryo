// @flow
import { connect } from 'react-redux';
import _ from 'lodash';

import msg from '../../../commons/languages';

import { confirm } from '../../../commons/actions/app';
import { actions as openTitleActions } from '../../modules/ui/expenses/recordListPane/summary/openTitle';
import { actions as modeActions } from '../../modules/ui/expenses/mode';
import {
  type EISearchObj,
  getEIsOnly,
} from '../../../domain/models/exp/ExtendedItem';

import {
  createNewExpReport,
  fetchExpReport,
  openCostCenterDialog,
  openJobDialog,
  openPrintWindow,
  openEILookupDialog,
  openVendorLookupDialog,
  cloneReport,
  searchVendorDetail,
  openReceiptLibraryDialog,
  getFilePreview,
} from '../../action-dispatchers/Expenses';

import ReportSummaryView from '../../../commons/components/exp/Form/ReportSummary';
import { setAvailableExpType } from '../../../domain/modules/exp/expense-type/availableExpType';

const mapStateToProps = (state, ownProps) => ({
  mode: state.ui.expenses.mode,
  expReportList: state.entities.exp.report.expReportList,
  selectedExpReport: state.ui.expenses.selectedExpReport,
  baseCurrencySymbol: state.userSetting.currencySymbol,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  employeeId: state.userSetting.employeeId,
  companyId: state.userSetting.companyId,
  openTitle: state.ui.expenses.recordListPane.summary.openTitle,
  overlap: state.ui.expenses.overlap,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  customHint: state.entities.exp.customHint,
  inactiveReportTypeList: state.entities.exp.expenseReportType.list.inactive,
  availableExpType: state.entities.availableExpType,
  activeVendor: state.ui.expenses.dialog.vendor.search,
  updateReport: (updateObj) => {
    const tmpReport = _.cloneDeep(ownProps.expReport);
    const tmpTouched = _.cloneDeep(ownProps.touched);

    Object.keys((updateObj: any)).forEach((key) => {
      _.set(tmpReport, key, updateObj[key]);
      _.set(tmpTouched, key, true);
    });
    ownProps.onChangeEditingExpReport('report', tmpReport, tmpTouched);
  },
  remarksLabel: msg().Exp_Clbl_ReportRemarks,
  ...ownProps,
});

const mapDispatchToProps = {
  confirm,
  createNewExpReport,
  fetchExpReport,
  openTitleAction: openTitleActions.open,
  onClickTitleToggleButton: openTitleActions.toggle,
  reportEdit: modeActions.reportEdit,
  onClickCostCenterButton: (targetDate: string, employeeId: string) =>
    openCostCenterDialog(targetDate, employeeId),
  onClickJobButton: (targetDate: string, employeeId: string) =>
    openJobDialog(targetDate, employeeId),
  openVendorLookupDialog,
  openPrintWindow,
  openEILookupDialog,
  cloneReport,
  setAvailableExpType,
  searchVendorDetail,
  openReceiptLibraryDialog,
  getFilePreview,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickBackBtn: () => {
    dispatchProps.moveBackToReport();
  },
  handleChangeExpenseReportType: (e: any) => {
    const reportTypeIndex = stateProps.reportTypeList.findIndex(
      (reportType) => reportType.id === e.target.value
    );
    const newReportType = stateProps.reportTypeList[reportTypeIndex];
    let resetVendorData = {};
    if (newReportType.isVendorRequired === 'UNUSED') {
      resetVendorData = {
        vendorId: null,
        vendorCode: '',
        vendorName: '',
        paymentDueDate: null,
        paymentDueDateUsage: null,
      };
    }
    let resetJobData = {};
    if (newReportType.isJobRequired === 'UNUSED') {
      resetJobData = {
        jobId: null,
        jobCode: '',
        jobName: '',
      };
    }
    let resetCostCenterData = {};
    if (newReportType.isCostCenterRequired === 'UNUSED') {
      resetCostCenterData = {
        costCenterName: null,
        costCenterCode: '',
        costCenterHistoryId: null,
      };
    }
    const originalReport = ownProps.expReport;
    const updateReceiptData = {
      useFileAttachment: newReportType.useFileAttachment,
    };
    if (!newReportType.useFileAttachment) {
      _.assign(updateReceiptData, {
        attachedFileVerId: null,
        attachedFileId: null,
        attachedFileData: null,
      });
    }
    const updateExtendedData = getEIsOnly(newReportType, originalReport);
    updateExtendedData.expReportTypeId = e.target.value;
    stateProps.updateReport({
      ...resetVendorData,
      ...resetJobData,
      ...resetCostCenterData,
      ...updateReceiptData,
      ...updateExtendedData,
    });
  },
  handleChangeCostCenter: () => {
    ownProps.onChangeEditingExpReport('report.costCenterHistoryId', null, true);
    ownProps.onChangeEditingExpReport('report.costCenterName', '', true);
  },
  handleChangeSubject: (e: any) => {
    ownProps.onChangeEditingExpReport('report.subject', e.target.value, true);
  },
  handleChangeRemarks: (e: any) => {
    ownProps.onChangeEditingExpReport('report.remarks', e.target.value, true);
  },
  handleChangeJob: () => {
    ownProps.onChangeEditingExpReport('report.jobId', null, true);
    ownProps.onChangeEditingExpReport('report.jobName', '', true);
  },
  handleClickCostCenterBtn: () => {
    dispatchProps.onClickCostCenterButton(
      ownProps.expReport.accountingDate,
      stateProps.employeeId
    );
  },
  handleClickJobBtn: () => {
    const employeeId =
      ownProps.expReport.employeeBaseId || stateProps.employeeId;
    dispatchProps.onClickJobButton(
      ownProps.expReport.accountingDate,
      employeeId
    );
  },
  onClickVendorSearch: () => {
    dispatchProps.openVendorLookupDialog(stateProps.employeeId);
  },
  searchVendorDetail: (vendorId) => {
    const isCached =
      stateProps.activeVendor && stateProps.activeVendor.id === vendorId;
    if (isCached) {
      return new Promise((resolve) =>
        resolve({ records: [stateProps.activeVendor] })
      );
    } else {
      return dispatchProps.searchVendorDetail(vendorId);
    }
  },
  onClickLookupEISearch: (item: EISearchObj) => {
    dispatchProps.openEILookupDialog(item, stateProps.employeeId);
  },
  onClickPrintPageButton: () => {
    dispatchProps.openPrintWindow(stateProps.selectedExpReport.reportId);
  },
  onClickCloneButton: () => {
    dispatchProps.cloneReport(
      stateProps.selectedExpReport.reportId,
      stateProps.reportTypeList
    );
  },
  setLinkedExpType: async (reportTypeId) => {
    const linkedExpType =
      (stateProps.reportTypeList.find((rt) => rt.id === reportTypeId) || {})
        .expTypeIds || [];
    await dispatchProps.setAvailableExpType(linkedExpType);
    ownProps.onChangeEditingExpReport('ui.availableExpType', linkedExpType);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ReportSummaryView): React.ComponentType<Object>);
