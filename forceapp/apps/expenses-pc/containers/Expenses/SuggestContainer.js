// @flow
import { connect } from 'react-redux';

import { getStation } from '../../action-dispatchers/Suggest';

import Suggest from '../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteForm/Suggest';

const mapStateToProps = (state, ownProps) => ({
  ...state,
  ...ownProps,
});

const mapDispatchToProps = {
  onClickSearchStationButton: getStation,
};

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(Suggest): React.ComponentType<Object>);
