// @flow
import { connect } from 'react-redux';

import RecordDate from '../../../commons/components/exp/Form/ReportSummary/Form/DateSelector/RecordDate';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
});

const mapDispatchToProps = {};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onChangeRecordDate: (date: string) => {
    ownProps.onChangeEditingExpReport('report.accountingDate', date, true);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordDate): React.ComponentType<Object>);
