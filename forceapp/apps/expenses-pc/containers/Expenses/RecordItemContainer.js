// @flow
import { connect } from 'react-redux';
import { cloneDeep, get, set, find, isEqual, assign, isEmpty } from 'lodash';
import FileUtil from '../../../commons/utils/FileUtil';
import msg from '../../../commons/languages';

import {
  newRouteInfo,
  isRecordItemized,
} from '../../../domain/models/exp/Record';

import { confirm } from '../../../commons/actions/app';
import { actions as overlapActions } from '../../modules/ui/expenses/overlap';
import { actions as modeActions, modes } from '../../modules/ui/expenses/mode';
import {
  openRecordItemsCreateDialog,
  openRecordItemsConfirmDialog,
  openRecordItemsDeleteDialog,
  searchTaxTypeList,
  searchExpTypesByParentRecord,
  openReceiptLibraryDialog,
  getBase64File,
  uploadReceipts,
  getFilePreview,
  openEILookupDialog,
  openJobDialog,
  openCostCenterDialog,
} from '../../action-dispatchers/Expenses';
import { resetRouteForm } from '../../action-dispatchers/Route';
import { getRateFromId } from '../../action-dispatchers/ForeignCurrency';
import { actions as workingDaysActions } from '../../modules/ui/expenses/recordItemPane/workingDays';
import { actions as selectedReceiptActions } from '../../modules/ui/expenses/receiptLibrary/selectedReceipt';

import {
  type ExpTaxTypeListApiReturn,
  calculateTax,
} from '../../../domain/models/exp/TaxType';
import { calcAmountFromRate } from '../../../domain/models/exp/foreign-currency/Currency';
import {
  type EISearchObj,
  getEIsOnly,
} from '../../../domain/models/exp/ExtendedItem';
import RecordItemView from '../../../commons/components/exp/Form/RecordItem';

export const mapStateToProps = (state: any, ownProps: any) => {
  const {
    onChangeEditingExpReport,
    errors,
    touched,
    expReport,
    recordIdx,
    selectedRecord,
    recordItemIdx,
  } = ownProps;

  const deleteRecord = () => {
    const tmpReport = cloneDeep(expReport);
    tmpReport.records.splice(recordIdx, 1);
    onChangeEditingExpReport(`report`, tmpReport);
    onChangeEditingExpReport(`ui.recalc`, true);
  };

  const restoreOldRecord = () => {
    const tmpReport = cloneDeep(expReport);
    tmpReport.records.splice(recordIdx, 1, selectedRecord);
    onChangeEditingExpReport(`report`, tmpReport);
    onChangeEditingExpReport(`ui.recalc`, true);
  };

  const updateReport = (
    key: string,
    value: any,
    recalc: boolean = false,
    isTouched: boolean = true
  ) => {
    onChangeEditingExpReport(`report.${key}`, value, isTouched);
    if (recalc) {
      onChangeEditingExpReport(`ui.recalc`, true);
    }
  };

  const updateRecord = (
    updateObj: { [string]: any },
    recalc: boolean = false
  ) => {
    const tmpRecord = cloneDeep(expReport.records[recordIdx]);

    const touchedRecords = get(touched, `records[${recordIdx}]`);
    const tmpTouched = touchedRecords ? cloneDeep(touchedRecords) : {};

    Object.keys(updateObj).forEach((key) => {
      set(tmpRecord, key, updateObj[key]);
      if (
        key !== 'routeInfo' &&
        key !== 'receiptData' &&
        key !== 'fileName' &&
        key !== 'dataType'
      ) {
        set(tmpTouched, key, true);
      }
    });

    const targetRecord = `report.records[${recordIdx}]`;
    onChangeEditingExpReport(targetRecord, tmpRecord, tmpTouched);

    if (recalc) {
      onChangeEditingExpReport(`ui.recalc`, true);
    }
  };

  return {
    touched,
    errors,
    recordItemIdx,
    mode: state.ui.expenses.mode,
    overlap: state.ui.expenses.overlap,
    baseCurrencyCode: state.userSetting.currencyCode,
    baseCurrencySymbol: state.userSetting.currencySymbol,
    baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
    companyId: state.userSetting.companyId,
    customHint: state.entities.exp.customHint,
    employeeId: state.userSetting.employeeId,
    expenseTypeList: state.entities.exp.expenseType.list,
    expenseTaxTypeList: state.entities.exp.taxType,
    accountingPeriodAll: state.ui.expenses.recordListPane.accountingPeriod,
    isUseAttendance: state.userSetting.useAttendance,
    deleteRecord,
    updateRecord,
    restoreOldRecord,
    updateReport,
    tax: state.ui.expenses.recordItemPane.tax,
    exchangeRateMap:
      state.ui.expenses.recordItemPane.foreignCurrency.exchangeRate,
    fixedAmountOptionList: state.ui.expenses.recordItemPane.fixedAmountOption,
  };
};

const mapDispatchToProps = {
  confirm,
  resetRouteForm,
  searchTaxTypeList,
  searchExpTypesByParentRecord,
  getRateFromId,
  openReceiptLibraryDialog,
  getBase64File,
  uploadReceipts,
  getFilePreview,
  hideRecord: overlapActions.nonOverlapRecord,
  reportEdit: modeActions.reportEdit,
  reportSelectMode: modeActions.reportSelect,
  checkWorkingDays: workingDaysActions.check,
  onClickRecordItemsCreateButton: () => openRecordItemsCreateDialog(),
  onClickRecordItemsConfirmButton: () => openRecordItemsConfirmDialog(),
  onClickRecordItemsDeleteButton: () => openRecordItemsDeleteDialog(),
  openJobDialog,
  openCostCenterDialog,
  openEILookupDialog,
  resetSelectedReceipt: selectedReceiptActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickOpenLibraryButton: () => {
    dispatchProps.openReceiptLibraryDialog();
  },
  onClickResetRouteInfoButton: () => {
    const updateObj = {
      routeInfo: cloneDeep(newRouteInfo),
      'items[0].amount': 0,
    };
    stateProps.updateRecord(updateObj, true);
    dispatchProps.resetRouteForm(null, false);
  },
  onClickHideRecordButton: () => {
    const { expReport, recordIdx, selectedRecord } = ownProps;
    const hasReportId = expReport.reportId;
    const hasPreRequestId = expReport.preRequestId;
    const isNewPreRequest = hasPreRequestId && !hasReportId;
    const isNewRecord = !get(expReport, `records[${recordIdx}].recordId`);
    const isReportEditMode = stateProps.mode === modes.REPORT_EDIT;
    const tmpRecord = cloneDeep(expReport.records[recordIdx]);

    // Remove any ocr data
    dispatchProps.resetSelectedReceipt();

    // remove receiptData for comparison.
    // receiptData is only available after we call the api to get data from receiptId
    if (tmpRecord.receiptData) {
      delete tmpRecord.receiptData;
    }

    const isRecordEdited = !isEqual(tmpRecord, selectedRecord);

    if (isReportEditMode && isRecordEdited) {
      // if confirm discard changes
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.hideRecord();

          if (isNewRecord) {
            // delete record from redux store if it's new record
            stateProps.deleteRecord();
          } else {
            // restore from tmpRecord
            stateProps.restoreOldRecord();
          }
          dispatchProps.reportSelectMode();
        }
      });
    } else {
      dispatchProps.hideRecord();
      // unselect the recordItem
      ownProps.onChangeEditingExpReport(`ui.recordIdx`, -1);
      ownProps.onChangeEditingExpReport(`ui.tempSavedRecordItems`, null);
      if (isNewRecord && !isNewPreRequest) {
        // delete record from redux store if it's new record
        stateProps.deleteRecord();
      }
      dispatchProps.reportSelectMode();
    }
  },
  onChangeRecordDate: (recordDate) => {
    const target = ownProps.expReport.records[ownProps.recordIdx];
    if (target.recordDate !== recordDate) {
      // check recordDate was working day.
      const recordDates = ownProps.expReport.records.map(
        (record) => record.recordDate
      );
      recordDates[ownProps.recordIdx] = recordDate;
      const workingDays = recordDates.filter(
        (x, i, self) => self.indexOf(x) === i
      );
      if (stateProps.isUseAttendance && ownProps.isExpense) {
        dispatchProps.checkWorkingDays(workingDays).then((result) => {
          ownProps.onChangeEditingExpReport('ui.workingDays', result.payload);
        });
      }

      const isItemized = isRecordItemized(target.recordType, false);
      if (isItemized || !recordDate) {
        stateProps.updateRecord({ recordDate }, true);
      } else if (target.items[0].useForeignCurrency) {
        let exchangeRateManual = false;
        const rateInfo = get(
          stateProps.exchangeRateMap,
          `${target.items[0].currencyId}.${recordDate}`
        );
        let getExchangeRate;
        if (rateInfo) {
          getExchangeRate = new Promise((resolve) =>
            resolve(rateInfo.calculationRate)
          );
        } else {
          getExchangeRate = dispatchProps
            .getRateFromId(
              stateProps.companyId,
              target.items[0].currencyId,
              recordDate
            )
            .then((exchangeRate) => {
              exchangeRateManual = exchangeRate === 0;
              return exchangeRate;
            });
        }
        getExchangeRate.then((exchangeRate) => {
          const amount = calcAmountFromRate(
            exchangeRate,
            target.items[0].localAmount,
            stateProps.baseCurrencyDecimal
          );

          stateProps.updateRecord({
            recordDate,
            amount,
            'items.0.amount': amount,
            'items.0.recordDate': recordDate,
            'items.0.exchangeRate': exchangeRate,
            'items.0.originalExchangeRate': exchangeRate,
            'items.0.exchangeRateManual': exchangeRateManual,
          });
        });
      } else {
        // this 2 lines are for giving formik information of accounting period and displaying error if the recordDate is out of accounting period range
        const selectedAccountingPeriod = find(stateProps.accountingPeriodAll, {
          id: ownProps.expReport.accountingPeriodId,
        });
        ownProps.onChangeEditingExpReport(
          'ui.selectedAccountingPeriod',
          selectedAccountingPeriod,
          false
        );

        const expTypeId =
          ownProps.expReport.records[ownProps.recordIdx].items[0].expTypeId;

        dispatchProps
          .searchTaxTypeList(expTypeId, recordDate)
          .then((result: ExpTaxTypeListApiReturn) => {
            const initTax = result.payload[expTypeId][recordDate][0];

            const amount =
              ownProps.expReport.records[ownProps.recordIdx].items[0].amount;
            const rate = initTax ? initTax.rate : 0;
            const baseId = 'noIdSelected';
            const historyId = initTax ? initTax.historyId : null;
            const name = initTax ? initTax.name : null;

            const taxRes = calculateTax(
              rate,
              amount || 0,
              stateProps.baseCurrencyDecimal
            );
            const updateObj = {
              recordDate,
              amount: amount || 0,
              withoutTax: taxRes.amountWithoutTax,
              'items.0.amount': amount || 0,
              'items.0.recordDate': recordDate,
              'items.0.withoutTax': taxRes.amountWithoutTax,
              'items.0.gstVat': taxRes.gstVat,
              'items.0.taxTypeBaseId': baseId,
              'items.0.taxTypeHistoryId': historyId,
              'items.0.taxTypeName': name,
            };
            stateProps.updateRecord(updateObj, true);
          });
      }
    }
  },
  // when change date or exp type for child item which use base currency
  onChangeChildDateOrTypeForBC: (selectedValues) => {
    const targetRecordItem =
      ownProps.expReport.records[ownProps.recordIdx].items[
        ownProps.recordItemIdx
      ];
    const recordDate = selectedValues.recordDate;
    const expTypeId = selectedValues.expTypeId;
    const isRecordDateChange = targetRecordItem.recordDate !== recordDate;
    const isExpTypeChange = targetRecordItem.expTypeId !== expTypeId;
    const targetPath = `items.${ownProps.recordItemIdx}`;
    const updateObj = {};

    // if change expense type, need to update EIs
    if (expTypeId && isExpTypeChange) {
      const expTypeInfo = ownProps.expTypeList.find(
        (expType) => expType.id === expTypeId
      );
      const EIs = getEIsOnly(expTypeInfo);
      Object.keys(EIs).forEach((key) => {
        updateObj[`${targetPath}.${key}`] = EIs[key];
      });
      updateObj[`${targetPath}.expTypeName`] = expTypeInfo.name;
    }
    // if input empty recordDate or expType, only update value
    if (!recordDate || !expTypeId) {
      assign(updateObj, {
        [`${targetPath}.recordDate`]: recordDate,
        [`${targetPath}.expTypeId`]: expTypeId,
      });
      stateProps.updateRecord(updateObj);
    }

    // when recordDate or expType change, update tax info
    else if (isRecordDateChange || isExpTypeChange) {
      const taxTypeList = get(stateProps.tax, `${expTypeId}.${recordDate}`);
      const amount = targetRecordItem.amount;

      let getTaxInfo;
      if (taxTypeList) {
        const initTax = taxTypeList[0];
        getTaxInfo = new Promise((resolve) => resolve(initTax));
      } else {
        getTaxInfo = dispatchProps
          .searchTaxTypeList(expTypeId, recordDate)
          .then((result: ExpTaxTypeListApiReturn) => {
            const initTax = result.payload[expTypeId][recordDate][0];
            return initTax;
          });
      }
      getTaxInfo.then((initTax) => {
        const rate = initTax ? initTax.rate : 0;
        const baseId = 'noIdSelected';
        const historyId = initTax ? initTax.historyId : null;
        const name = initTax ? initTax.name : null;

        const taxRes = calculateTax(
          rate,
          amount || 0,
          stateProps.baseCurrencyDecimal
        );
        assign(updateObj, {
          [`${targetPath}.recordDate`]: recordDate,
          [`${targetPath}.withoutTax`]: taxRes.amountWithoutTax,
          [`${targetPath}.gstVat`]: taxRes.gstVat,
          [`${targetPath}.taxTypeBaseId`]: baseId,
          [`${targetPath}.taxTypeHistoryId`]: historyId,
          [`${targetPath}.taxTypeName`]: name,
          [`${targetPath}.taxRate`]: rate,
          [`${targetPath}.expTypeId`]: expTypeId,
        });
        stateProps.updateRecord(updateObj, true);
      });
    }
  },
  // when change expense type for child item which use Foreign currency
  onChangeChildExpTypeForFC: (expTypeId) => {
    const { expReport, recordIdx, recordItemIdx, expTypeList } = ownProps;
    const targetRecordItem = expReport.records[recordIdx].items[recordItemIdx];
    const isExpTypeChange = targetRecordItem.expTypeId !== expTypeId;
    // if change expense type, need to update EIs
    if (expTypeId && isExpTypeChange) {
      const updateObj = {};
      const targetPath = `items.${recordItemIdx}`;
      const expTypeInfo = expTypeList.find(
        (expType) => expType.id === expTypeId
      );
      const EIs = getEIsOnly(expTypeInfo);
      Object.keys(EIs).forEach((key) => {
        updateObj[`${targetPath}.${key}`] = EIs[key];
      });
      updateObj[`${targetPath}.expTypeName`] = expTypeInfo.name;
      updateObj[`${targetPath}.expTypeId`] = expTypeId;
      stateProps.updateRecord(updateObj);
    }
  },
  // when change date for child item which use Foreign currency
  onChangeChildDateForFC: (recordDate) => {
    const { expReport, recordIdx, recordItemIdx } = ownProps;
    const targetItem = expReport.records[recordIdx].items[recordItemIdx];
    const path = `items.${recordItemIdx}`;
    if (!recordDate || targetItem.recordDate === recordDate) {
      stateProps.updateRecord({ [`${path}.recordDate`]: recordDate }, true);
      return;
    }
    let exchangeRateManual = false;
    const rateInfo = get(
      stateProps.exchangeRateMap,
      `${targetItem.currencyId}.${recordDate}`
    );
    let getExchangeRate;
    if (rateInfo) {
      getExchangeRate = new Promise((resolve) =>
        resolve(rateInfo.calculationRate)
      );
    } else {
      getExchangeRate = dispatchProps
        .getRateFromId(stateProps.companyId, targetItem.currencyId, recordDate)
        .then((exchangeRate) => {
          exchangeRateManual = exchangeRate === 0;
          return exchangeRate;
        });
    }
    getExchangeRate.then((exchangeRate) => {
      const amount = calcAmountFromRate(
        exchangeRate,
        targetItem.localAmount,
        stateProps.baseCurrencyDecimal
      );

      stateProps.updateRecord({
        [`${path}.recordDate`]: recordDate,
        [`${path}.amount`]: amount,
        [`${path}.exchangeRate`]: exchangeRate,
        [`${path}.originalExchangeRate`]: exchangeRate,
        [`${path}.exchangeRateManual`]: exchangeRateManual,
      });
    });
  },
  onImageDrop: (file: File) => {
    dispatchProps.getBase64File(file).then((base64File) => {
      dispatchProps.uploadReceipts([base64File]).then((res) => {
        if (res) {
          stateProps.updateRecord({
            receiptId: res.contentDocumentId,
            receiptFileId: res.contentVersionId,
            receiptData: base64File.data,
            fileName: FileUtil.getFileNameWithoutExtension(base64File.name),
            dataType: base64File.type,
          });
        }
      });
    });
  },
  getFilePreview: (receiptFileId: string) => {
    dispatchProps.getFilePreview(receiptFileId).then((res) => {
      const dataType = FileUtil.getMIMEType(res.payload.fileType);

      return stateProps.updateRecord(
        {
          receiptData: `data:${dataType};base64,${res.payload.fileBody}`,
          fileName: FileUtil.getOriginalFileNameWithoutPrefix(
            res.payload.title
          ),
          dataType,
        },
        false
      );
    });
  },
  onClickJobBtn: (recordDate) => {
    dispatchProps.openJobDialog(recordDate, stateProps.employeeId);
  },
  onClickCostCenterBtn: (recordDate) => {
    dispatchProps.openCostCenterDialog(recordDate, stateProps.employeeId);
  },
  onClickLookupEISearch: (item: EISearchObj) =>
    dispatchProps.openEILookupDialog(item, stateProps.employeeId),
  onChangeAmountSelection: (amountId: string) => {
    const { expReport, recordIdx } = ownProps;
    const record = expReport.records[recordIdx];
    const expTypeId = get(record, 'items[0].expTypeId');
    const useFixedForeignCurrency = get(
      record,
      'items[0].useFixedForeignCurrency'
    );
    const amountOption = get(stateProps.fixedAmountOptionList, `${expTypeId}`);
    const selectedAmount = amountOption.find((item) => item.id === amountId);
    const fixedAmount = isEmpty(selectedAmount)
      ? 0
      : selectedAmount.allowanceAmount;
    const updateObj = {};
    const targetPath = 'items.0';
    updateObj[`${targetPath}.fixedAllowanceOptionId`] = amountId;

    if (useFixedForeignCurrency) {
      updateObj[`${targetPath}.localAmount`] = fixedAmount;
    } else {
      updateObj.amount = fixedAmount;
      updateObj[`${targetPath}.amount`] = fixedAmount;
    }
    stateProps.updateRecord(updateObj);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordItemView): React.ComponentType<Object>);
