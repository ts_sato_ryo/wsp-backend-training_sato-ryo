// @flow
import { connect } from 'react-redux';
import { cloneDeep, set, last } from 'lodash';
import msg from '../../../../commons/languages';
import FileUtil from '../../../../commons/utils/FileUtil';
import { OCR_STATUS } from '../../../../domain/models/exp/Receipt';
import { actions as progressBarActions } from '../../../modules/ui/expenses/dialog/progressBar';
import { actions as selectedReceiptActions } from '../../../modules/ui/expenses/receiptLibrary/selectedReceipt';
import { actions as commentActions } from '../../../modules/ui/expenses/dialog/approval/comment';
import { actions as receiptStatusActions } from '../../../modules/ui/expenses/receiptLibrary/status';
import { dialogTypes } from '../../../modules/ui/expenses/dialog/activeDialog';
import {
  getBase64File,
  getFilePreview,
  deleteReceipt,
  uploadReceipts,
  keepGettingStatus,
  executeOcr,
  openExpenseTypeDialog,
} from '../../../action-dispatchers/Expenses';

import ReceiptLibraryDialog from '../../../../commons/components/exp/Form/Dialog/ReceiptLibrary';

const mapStateToProps = (state) => {
  // Receipts with no OCR info
  let isReportReceipt = true;
  let isMultiStep = false;

  const activeDialog = state.ui.expenses.dialog.activeDialog;
  const currentDialog = last(activeDialog);
  let mainButtonTitle = msg().Exp_Lbl_Attach;
  if (currentDialog === dialogTypes.OCR_RECEIPTS) {
    mainButtonTitle = msg().Com_Lbl_NextButton;
    isReportReceipt = false;
    isMultiStep = true;
  }
  return {
    mainButtonTitle,
    isReportReceipt,
    isMultiStep,
    currentDialog,
    title: msg().Exp_Lbl_ReceiptLibrary,
    receiptList: state.entities.exp.receiptLibrary.list.receipts,
    photoUrl: state.userSetting.photoUrl,
    comment: state.ui.expenses.dialog.approval.comment,
    customHint: state.entities.exp.customHint.recordReceipt,
    receiptStatus: state.ui.expenses.receiptLibrary.status,
    progressBar: state.ui.expenses.dialog.progressBar,
    selectedReceipt: state.ui.expenses.receiptLibrary.selectedReceipt,
    companyId: state.userSetting.companyId,
    employeeId: state.userSetting.employeeId,
    reportTypeId: state.ui.expenses.selectedExpReport.expReportTypeId,
  };
};

const mapDispatchToProps = {
  getBase64File,
  getFilePreview,
  deleteReceipt,
  uploadReceipts,
  keepGettingStatus,
  executeOcr,
  openExpenseTypeDialog,
  onChangeComment: commentActions.set,
  setProgressBar: progressBarActions.set,
  resetProgressBar: progressBarActions.clear,
  setReceiptStatus: receiptStatusActions.set,
  setSelectedReceipt: selectedReceiptActions.set,
  removeReceiptStatus: receiptStatusActions.remove,
  resetSelectedReceipt: selectedReceiptActions.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  deleteReceipt: (receiptId, receiptFileId) => {
    dispatchProps.deleteReceipt(receiptId);
    dispatchProps.removeReceiptStatus(receiptFileId);
  },
  onClickReceiptLibrayCloseButton: () => {
    dispatchProps.resetSelectedReceipt();
    dispatchProps.resetProgressBar();
    ownProps.onClickHideDialogButton();
  },
  onImageDrop: (file, runOCR) => {
    return dispatchProps.getBase64File(file).then((base64File) => {
      return dispatchProps.uploadReceipts([base64File]).then((res) => {
        if (runOCR) {
          dispatchProps
            .executeOcr(res.contentVersionId)
            .then((ocrExecuteRes) => {
              dispatchProps.keepGettingStatus(
                ocrExecuteRes.payload.taskId,
                res.contentVersionId
              );
            });
        }
        if (res) {
          if (runOCR) {
            dispatchProps.setReceiptStatus(
              res.contentVersionId,
              OCR_STATUS.IN_PROGRESS
            );
          }
          return {
            receiptId: res.contentDocumentId,
            receiptFileId: res.contentVersionId,
            receiptData: base64File.data,
          };
        }
        return {
          receiptId: null,
          receiptFileId: null,
          receiptData: null,
          ocrInfo: null,
        };
      });
    });
  },

  onClickMainButton: () => {
    ownProps.onClickCancelButton();
  },
  getFilePreview: (receiptFileId, withOcrInfo) => {
    return dispatchProps.getFilePreview(receiptFileId).then((res) => {
      const dataType = FileUtil.getMIMEType(res.payload.fileType);
      if (withOcrInfo && res.payload.ocrInfo) {
        const status = res.payload.ocrInfo.status;
        dispatchProps.setReceiptStatus(receiptFileId, status);
      }
      return {
        receiptFileId,
        receiptId: res.payload.contentDocumentId,
        fileBody: `data:${dataType};base64,${res.payload.fileBody}`,
        title: res.payload.title,
        ocrInfo: res.payload.ocrInfo,
        uploadedDate: res.payload.uploadedDate,
        dataType,
      };
    });
  },
  executeOcr: (receiptFileId) => {
    dispatchProps.setReceiptStatus(receiptFileId, OCR_STATUS.IN_PROGRESS);
    dispatchProps.executeOcr(receiptFileId).then((ocrExecuteRes) => {
      dispatchProps.keepGettingStatus(
        ocrExecuteRes.payload.taskId,
        receiptFileId
      );
    });
  },
  onClickSelectReceipt: (param, receiptId, receiptData, fileName, dataType) => {
    if (stateProps.currentDialog === dialogTypes.OCR_RECEIPTS) {
      const progressBar = param;
      ownProps.hideDialog();
      dispatchProps.setProgressBar(progressBar);
      const ocrInfo = stateProps.selectedReceipt.ocrInfo;
      const recordDate =
        (ocrInfo && ocrInfo.result && ocrInfo.result.recordDate) ||
        ownProps.expReport.accountingDate;

      dispatchProps.openExpenseTypeDialog(
        stateProps.employeeId,
        stateProps.companyId,
        recordDate,
        'General',
        stateProps.reportTypeId,
        false
      );
    } else {
      const receiptFileId = param;
      ownProps.hideDialog();
      dispatchProps.resetSelectedReceipt();
      const expReport = cloneDeep(ownProps.expReport);
      const touched = cloneDeep(ownProps.touched);
      if (ownProps.recordIdx < 0) {
        // for report receipt
        expReport.attachedFileId = receiptId;
        expReport.attachedFileVerId = receiptFileId;
        expReport.attachedFileData = receiptData;
        set(touched, 'report.attachedFileId', true);
        set(touched, 'report.attachedFileVerId', true);
        ownProps.onChangeEditingExpReport('report', expReport, touched);
        return;
      }
      expReport.records[ownProps.recordIdx].receiptId = receiptId;
      expReport.records[ownProps.recordIdx].receiptFileId = receiptFileId;
      expReport.records[ownProps.recordIdx].receiptData = receiptData;
      expReport.records[ownProps.recordIdx].fileName = fileName;
      expReport.records[ownProps.recordIdx].dataType = dataType;

      set(touched, `records.${ownProps.recordIdx}.receiptId`, true);
      ownProps.onChangeEditingExpReport(`report`, expReport, touched);
    }
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ReceiptLibraryDialog): React.ComponentType<*>): React.ComponentType<Object>);
