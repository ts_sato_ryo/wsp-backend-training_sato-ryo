// @flow
import { connect } from 'react-redux';

import RecordUpdatedInfo from '../../../../commons/components/exp/Form/Dialog/RecordUpdatedInfo';

const mapStateToProps = (state) => ({
  language: state.userSetting.language,
  updateInfo: state.ui.expenses.dialog.recordUpdated.dialog.updateInfo,
});

const mapDispatchToProps = {};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordUpdatedInfo): React.ComponentType<*>): React.ComponentType<Object>);
