// @flow
import { connect } from 'react-redux';
import { isEmpty, difference, find } from 'lodash';
import msg from '../../../../commons/languages';
import DateUtil from '../../../../commons/utils/DateUtil';
import { showToast } from '../../../../commons/modules/toast';
import { cloneRecords } from '../../../action-dispatchers/Expenses';
import { actions as activeDialogActions } from '../../../modules/ui/expenses/dialog/activeDialog';
import { actions as recordCloneActions } from '../../../modules/ui/expenses/dialog/recordClone/dialog';
import { actions as workingDaysActions } from '../../../modules/ui/expenses/recordItemPane/workingDays';
import RecordCloneDate from '../../../../commons/components/exp/Form/Dialog/RecordClone/CloneDateSelection';

const mapStateToProps = (state) => ({
  recordClone: state.ui.expenses.dialog.recordClone.dialog,
  language: state.userSetting.language,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  workingDays: state.ui.expenses.recordItemPane.workingDays,
  isUseAttendance: state.userSetting.useAttendance,
  accountingPeriodAll: state.ui.expenses.recordListPane.accountingPeriod,
});

const mapDispatchToProps = {
  cloneRecords,
  showToast,
  showRecordUpdateDialog: activeDialogActions.recordUpdated,
  onChangeCloneDate: recordCloneActions.setDate,
  checkWorkingDays: workingDaysActions.check,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onClickRecordCloneButton: () => {
    const { dates, records } = stateProps.recordClone;
    const targetDates = dates.map((item) =>
      DateUtil.format(item, 'YYYY-MM-DD')
    );
    dispatchProps
      .cloneRecords(
        targetDates,
        records,
        ownProps.expReport.reportId,
        stateProps.reportTypeList
      )
      .then((cloneRes) => {
        const { recordIds, updatedRecords } = cloneRes;
        if (!isEmpty(recordIds)) {
          dispatchProps.showToast(msg().Exp_Msg_CloneRecords, 4000);
          if (!isEmpty(updatedRecords)) {
            dispatchProps.showRecordUpdateDialog();
          }
        }
        // if clone dates are not inside current working days, call working days API to check
        const recordDates = ownProps.expReport.records.map(
          (record) => record.recordDate
        );
        const workingDaysList = Object.keys(stateProps.workingDays);
        const workingDaysDiff = difference(
          [...recordDates, ...targetDates],
          workingDaysList
        );
        const newWorkingDays = [...workingDaysList, ...workingDaysDiff];
        if (workingDaysDiff.length) {
          if (stateProps.isUseAttendance && ownProps.isExpense) {
            dispatchProps.checkWorkingDays(newWorkingDays).then((result) => {
              ownProps.onChangeEditingExpReport(
                'ui.workingDays',
                result.payload
              );
            });
          }
        } else {
          ownProps.onChangeEditingExpReport(
            'ui.workingDays',
            stateProps.workingDays
          );
        }
        // giving formik information of accounting period and displaying error if the recordDate is out of accounting period range
        const selectedAccountingPeriod = find(stateProps.accountingPeriodAll, {
          id: ownProps.expReport.accountingPeriodId,
        });
        ownProps.onChangeEditingExpReport(
          'ui.selectedAccountingPeriod',
          selectedAccountingPeriod,
          false
        );
      });
  },
  onClickChangeDate: (selectedDates) => {
    dispatchProps.onChangeCloneDate(selectedDates);
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordCloneDate): React.ComponentType<*>): React.ComponentType<Object>);
