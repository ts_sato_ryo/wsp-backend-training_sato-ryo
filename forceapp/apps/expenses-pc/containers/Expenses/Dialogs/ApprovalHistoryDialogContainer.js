// @flow
import { connect } from 'react-redux';
import ApprovalHistoryDialog from '../../../../../widgets/dialogs/ApprovalHistoryDialog';

const mapStateToProps = (state) => ({
  historyList: state.entities.exp.approval.request.history.historyList,
});

const mapDispatchToProps = {};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  onHide: () => {
    ownProps.onClickHideDialogButton();
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ApprovalHistoryDialog): React.ComponentType<*>): React.ComponentType<Object>);
