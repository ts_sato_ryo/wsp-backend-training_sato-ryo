// @flow
import { connect } from 'react-redux';
import cloneDeep from 'lodash/cloneDeep';
import { actions as eiSearchAction } from '../../../modules/ui/expenses/dialog/extendedItem/search';
import { actions as vendorRecentlyUsedAction } from '../../../modules/ui/expenses/dialog/vendor/recentlyUsed';
import { searchVendorLookup } from '../../../action-dispatchers/Expenses';
import Vendor from '../../../../commons/components/exp/Form/Dialog/Vendor';

const mapStateToProps = (state) => ({
  companyId: state.userSetting.companyId,
  hintMsg: state.entities.exp.reportHeaderVendor,
  vendorRecentlyUsed: state.ui.expenses.dialog.vendor.recentlyUsed,
});

const mapDispatchToProps = {
  searchVendorLookup,
  clearVendorRecentlyUsedDialog: vendorRecentlyUsedAction.clear,
  clearVendorSearchDialog: eiSearchAction.clear,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  ...dispatchProps,
  onVendorSearch: (id, query) =>
    dispatchProps.searchVendorLookup(id, query).then((res) => res),

  onClickVendorItem: (item) => {
    const expReport = cloneDeep(ownProps.expReport);
    const touched = cloneDeep(ownProps.touched);

    expReport.vendorId = item.id;
    expReport.vendorName = item.name;
    expReport.vendorCode = item.code;
    expReport.paymentDueDateUsage = item.paymentDueDateUsage;
    touched.vendorId = true;
    touched.vendorName = true;
    touched.vendorCode = true;
    ownProps.onChangeEditingExpReport('report', expReport, touched);

    dispatchProps.clearVendorSearchDialog();
    dispatchProps.clearVendorRecentlyUsedDialog();
    ownProps.hideDialog();
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Vendor): React.ComponentType<*>): React.ComponentType<Object>);
