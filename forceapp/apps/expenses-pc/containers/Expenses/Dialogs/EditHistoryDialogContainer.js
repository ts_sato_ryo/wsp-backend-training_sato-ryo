// @flow
import { connect } from 'react-redux';
import EditHistoryDialog from '../../../../../widgets/dialogs/EditHistoryDialog';

const mapStateToProps = (state) => ({
  modificationList: state.entities.exp.financeApproval.modificationList,
});

const mapDispatchToProps = {};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...ownProps,
  ...stateProps,
  onHide: () => {
    ownProps.onClickHideDialogButton();
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(EditHistoryDialog): React.ComponentType<*>): React.ComponentType<Object>);
