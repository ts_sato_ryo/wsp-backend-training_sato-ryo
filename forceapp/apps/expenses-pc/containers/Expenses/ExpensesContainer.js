// @flow
import { connect } from 'react-redux';
import { get } from 'lodash';

import msg from '../../../commons/languages';

import {
  fetchExpReportList,
  // pagination
  fetchExpReport,
  backFromDetailToList,
  fetchExpReportIdList,
} from '../../action-dispatchers/Expenses';

import {
  PAGE_SIZE,
  MAX_PAGE_NUM,
  MAX_SEARCH_RESULT_NUM,
} from '../../modules/ui/expenses/reportList/page';
import { actions as tabAction } from '../../modules/ui/expenses/tab';
import { modes } from '../../modules/ui/expenses/mode';
import { confirm } from '../../../commons/actions/app';

import withExpensesHOC from '../../components/Expenses';
import ExpView from '../../../commons/components/exp';

// this function contains labels which are different in Expense and Request. if this label edited, change same object in Request
// 経費申請と事前申請で表示するラベルが異なるものを格納。この関数を編集したら、事前申請の同じ関数も編集してください

function labelObject() {
  return {
    reports: msg().Exp_Lbl_Reports,
    newReport: msg().Exp_Lbl_NewReportCreateExp,
  };
}

function mapStateToProps(state) {
  const reportIdList = state.entities.reportIdList.reportIdList;
  const selectedRequestId =
    state.ui.expenses.selectedExpReport.reportId ||
    state.ui.expenses.selectedExpReport.preRequestId;
  // The idx of the screen being displayed.
  // if no report is selected, values is -1
  const currentRequestIdx = selectedRequestId
    ? reportIdList.indexOf(selectedRequestId)
    : -1;

  return {
    // pagination props
    currentRequestIdx,
    currentPage: state.ui.expenses.reportList.page,
    requestTotalNum: state.entities.reportIdList.totalSize,
    orderBy: state.ui.expenses.reportList.orderBy,
    sortBy: state.ui.expenses.reportList.sortBy,
    pageSize: PAGE_SIZE,
    maxPageNo: MAX_PAGE_NUM,
    maxSearchNo: MAX_SEARCH_RESULT_NUM,
    reportIdList,
    // end of pagination props
    labelObject,
    mode: state.ui.expenses.mode,
    overlap: state.ui.expenses.overlap,
    selectedTab: state.ui.expenses.tab,
    selectedExpReport: state.ui.expenses.selectedExpReport,
    userSetting: state.userSetting,
    reportTypeList: state.entities.exp.expenseReportType.list.active,
    expReportList: state.entities.exp.report.expReportList,
  };
}

const mapDispatchToProps = {
  confirm,
  fetchExpReportList,
  // pagination
  backFromDetailToList,
  fetchExpReportIdList,
  fetchExpReport,
  onChangeTab: tabAction.changeTab,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  // Move to next to report. it's either +1 or -1
  onClickNextToRequestButton: (moveNum: number) => {
    const nextReportIndex = stateProps.currentRequestIdx + moveNum;
    const nextReportId = stateProps.reportIdList[nextReportIndex];
    const nextReport = stateProps.expReportList.find(
      (i) => i.reportId === nextReportId
    );
    const reportStatus = nextReport ? get(nextReport, 'status', null) : null;

    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.fetchExpReport(
            reportStatus,
            nextReportId,
            stateProps.reportTypeList
          );
        }
      });
    } else {
      dispatchProps.fetchExpReport(
        reportStatus,
        nextReportId,
        stateProps.reportTypeList
      );
    }
  },
  // selects the correct index when going back to list view.
  onClickBackButton: () => {
    if (stateProps.mode === modes.REPORT_EDIT) {
      dispatchProps.confirm(msg().Common_Confirm_DiscardEdits, (yes) => {
        if (yes) {
          dispatchProps.backFromDetailToList(
            stateProps.reportIdList,
            stateProps.currentRequestIdx,
            stateProps.expReportList
          );
        }
      });
    } else {
      dispatchProps.backFromDetailToList(
        stateProps.reportIdList,
        stateProps.currentRequestIdx,
        stateProps.expReportList
      );
    }
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(withExpensesHOC(ExpView)): React.ComponentType<Object>);
