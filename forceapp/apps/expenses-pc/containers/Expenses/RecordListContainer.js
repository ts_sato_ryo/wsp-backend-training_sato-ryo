// @flow
import { cloneDeep, find, get, isEmpty } from 'lodash';
import { connect } from 'react-redux';
import * as React from 'react';
import DateUtil from '../../../commons/utils/DateUtil';

import msg from '../../../commons/languages';
import { confirm } from '../../../commons/actions/app';

import { actions as overlapActions } from '../../modules/ui/expenses/overlap';
import { actions as selectedReceiptActions } from '../../modules/ui/expenses/receiptLibrary/selectedReceipt';
// import { actions as openEditMenuActions } from '../../modules/ui/expenses/recordListPane/recordList/openEditMenu';
import { actions as openTitleActions } from '../../modules/ui/expenses/recordListPane/summary/openTitle';
import { actions as progressBarActions } from '../../modules/ui/expenses/dialog/progressBar';

import { actions as activeDialogActions } from '../../modules/ui/expenses/dialog/activeDialog';
import { actions as recordCloneActions } from '../../modules/ui/expenses/dialog/recordClone/dialog';
import { actions as selectedExpReportActions } from '../../modules/ui/expenses/selectedExpReport';

import {
  deleteExpRecord,
  openExpenseTypeDialog,
  openReceiptLibraryDialog,
  openOCRReceiptLibraryDialog,
} from '../../action-dispatchers/Expenses';

import RecordListView from '../../../commons/components/exp/Form/RecordList';

import RecordIcon from '../../../commons/components/exp/Form/RecordList/Icon';
import {
  isFixedAllowanceMulti,
  warnings,
} from '../../../domain/models/exp/Record';
import { actions as fixedAmountOptionActions } from '../../modules/ui/expenses/recordItemPane/fixedAmountOption';

import iconVoucherReceipt from '../../images/iconVoucherReceipt.png';
import iconVoucherTransit from '../../images/iconVoucherTransit.png';

function getImage() {
  return {
    receipt: () => {
      return <img src={iconVoucherReceipt} alt="receipt" />;
    },
    routeSelected: () => {
      return <img src={iconVoucherTransit} alt="receipt" />;
    },
    warningReceipt: (idx, ROOT, errors, touched) => {
      const currentRecordErrors = get(errors, `records.${idx}`, {});
      // Temp Fix, remove when Refactor is done (Do Not Add Warning in Error Msg)
      // Remove "There's no work information on the day." Warning from Error Msg
      const displayErrors = Object.values(currentRecordErrors).filter(
        (x) => !warnings.includes(x)
      );

      let tooltip = msg().Exp_Lbl_ReceiptNeeded;

      if (displayErrors) {
        tooltip = displayErrors.reduce(
          (errorMsg, i: any) => `${errorMsg} ${msg()[i]}`,
          ''
        );
      }

      return (
        <RecordIcon
          idx={idx}
          errors={errors}
          touched={touched}
          tooltip={tooltip}
          className={`${ROOT}__receipt__record__icon`}
        />
      );
    },
    warningRoute: (idx, ROOT, errors, touched) => {
      return (
        <RecordIcon
          idx={idx}
          errors={errors}
          touched={touched}
          tooltip={msg().Exp_Lbl_RouteNeeded}
          className={`${ROOT}__receipt__record__icon`}
        />
      );
    },
  };
}

// export this common stateProps to reuse in Request and FA Containers
export const mapStateToProps = (state: any, ownProps: any) => ({
  ...ownProps,
  accountingPeriodAll: state.ui.expenses.recordListPane.accountingPeriod,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  baseCurrencySymbol: state.userSetting.currencySymbol,
  companyId: state.userSetting.companyId,
  employeeId: state.userSetting.employeeId,
  getImage,
  mode: state.ui.expenses.mode,
  openEditMenu: state.ui.expenses.recordListPane.recordList.openEditMenu,
  fixedAmountOptionList: state.ui.expenses.recordItemPane.fixedAmountOption,
  reportTypeList: state.entities.exp.expenseReportType.list.active,
  workingDays: state.ui.expenses.recordItemPane.workingDays,
});

const mapDispatchToProps = {
  overlapRecord: overlapActions.overlapRecord,
  closeRecord: overlapActions.nonOverlapRecord,
  nonOverlapReport: overlapActions.nonOverlapReport,
  closeTitle: openTitleActions.close,
  openExpenseTypeDialog,
  deleteExpRecord,
  confirm,
  openRecordCloneDialog: activeDialogActions.recordCloneDate,
  setCloneReport: recordCloneActions.setRecord,
  searchOptionList: fixedAmountOptionActions.search,
  openReceiptLibraryDialog,
  openOCRReceiptLibraryDialog,
  resetSelectedReceipt: selectedReceiptActions.clear,
  resetProgressBar: progressBarActions.clear,
  setReport: selectedExpReportActions.select,

  // Edit dropdown has been removed, can be used in future
  // onClickOpenEditMenu: openEditMenuActions.open,
  // onClickCloseEditMenu: openEditMenuActions.close,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickOpenLibraryButton: () => {
    dispatchProps.resetSelectedReceipt();
    dispatchProps.openOCRReceiptLibraryDialog();
  },
  onClickNewRecordButton() {
    dispatchProps.closeTitle();
    dispatchProps.resetProgressBar();
    dispatchProps.resetSelectedReceipt();
    dispatchProps.openExpenseTypeDialog(
      stateProps.employeeId,
      stateProps.companyId,
      ownProps.expReport.accountingDate,
      '',
      ownProps.expReport.expReportTypeId,
      false
    );
    const selectedAccountingPeriod = find(stateProps.accountingPeriodAll, {
      id: ownProps.expReport.accountingPeriodId,
    });
    ownProps.onChangeEditingExpReport(
      'ui.selectedAccountingPeriod',
      selectedAccountingPeriod,
      false
    );
  },
  onClickCloneRecordButton() {
    const checkboxes = ownProps.checkboxes;
    const records = cloneDeep(ownProps.expReport.records);
    const selectedRecordIds = [];
    let latestDate = records[`${checkboxes[0]}`].recordDate;
    checkboxes.forEach((index) => {
      selectedRecordIds.push(records[index].recordId);
      if (DateUtil.isBefore(latestDate, records[index].recordDate)) {
        latestDate = records[index].recordDate;
      }
    });
    dispatchProps.setCloneReport(
      selectedRecordIds,
      DateUtil.addDays(latestDate, 1)
    );
    dispatchProps.openRecordCloneDialog();
  },
  onClickRecord: (idx: number) => {
    dispatchProps.closeTitle();
    ownProps.onChangeEditingExpReport('ui.recordIdx', idx);

    // store old state into temp ui state to restore later
    ownProps.onChangeEditingExpReport(
      'ui.selectedRecord',
      ownProps.expReport.records[idx]
    );

    /* temporary saved copy of record items for following use:
     1. serve as a temp save copy when user temporarily "save" record items
     2. restore formik to the temp saved items when user cancel editing */
    ownProps.onChangeEditingExpReport(
      'ui.tempSavedRecordItems',
      ownProps.expReport.records[idx].items
    );

    // this 2 lines are for giving formik information of accounting period and displaying error if the recordDate is out of accounting period range
    const selectedAccountingPeriod = find(stateProps.accountingPeriodAll, {
      id: ownProps.expReport.accountingPeriodId,
    });
    ownProps.onChangeEditingExpReport(
      'ui.selectedAccountingPeriod',
      selectedAccountingPeriod,
      false
    );

    // fetch amount option list if record type is Multiple Fixed Allowance and redux doesn't have data
    const recordType = ownProps.expReport.records[idx].recordType;
    const expTypeId = ownProps.expReport.records[idx].items[0].expTypeId;
    const optionList = get(stateProps.fixedAmountOptionList, expTypeId);
    if (isFixedAllowanceMulti(recordType) && isEmpty(optionList)) {
      dispatchProps.searchOptionList(expTypeId);
    }

    dispatchProps.overlapRecord();
  },
  onChangeCheckBox: (idx: number) => {
    const target = ownProps.checkboxes.indexOf(idx);
    const checkboxes = cloneDeep(ownProps.checkboxes);
    if (target > -1) {
      checkboxes.splice(target, 1);
    } else {
      checkboxes.push(idx);
    }
    ownProps.onChangeEditingExpReport('ui.checkboxes', checkboxes);
  },
  onClickDeleteRecordItem: () => {
    dispatchProps.confirm(msg().Exp_Msg_ConfirmDeleteSelectedRecords, (yes) => {
      if (yes) {
        const checkboxes = ownProps.checkboxes.sort((a, b) => (a > b ? -1 : 1));
        const records = cloneDeep(ownProps.expReport.records);
        let recordsTouched = [];
        const selectedRecordIds = [];
        checkboxes.forEach((index) => {
          selectedRecordIds.push(records[index].recordId);
          records.splice(index, 1);
        });
        if (ownProps.touched && ownProps.touched.records) {
          recordsTouched = cloneDeep(ownProps.touched.records);
          checkboxes.forEach((index) => {
            recordsTouched.splice(index, 1);
          });
        }

        // Change touched into false so that report remains in Select mode.
        ownProps.onChangeEditingExpReport(`report.records`, records, false);
        ownProps.onChangeEditingExpReport('ui', {
          checkboxes: [],
          recordIdx: -1,
          recalc: true,
          editMode: true,
        });

        const updateReport = cloneDeep(ownProps.expReport);
        updateReport.records = records;
        dispatchProps.setReport(updateReport, stateProps.reportTypeList);
        dispatchProps.deleteExpRecord(selectedRecordIds).then(() => {
          // pass formik information of workdays to check record date warning
          ownProps.onChangeEditingExpReport(
            'ui.workingDays',
            stateProps.workingDays
          );
          // pass formik information of accounting period and displaying error if the recordDate is out of accounting period range
          const selectedAccountingPeriod = find(
            stateProps.accountingPeriodAll,
            {
              id: ownProps.expReport.accountingPeriodId,
            }
          );
          ownProps.onChangeEditingExpReport(
            'ui.selectedAccountingPeriod',
            selectedAccountingPeriod,
            false
          );
        });
      }
    });
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordListView): React.ComponentType<*>): React.ComponentType<Object>);
