// @flow
import { connect } from 'react-redux';
import { find } from 'lodash';

import AccountingPeriodAndRecordDate from '../../../commons/components/exp/Form/ReportSummary/Form/DateSelector/AccountingPeriodAndRecordDate';

const mapStateToProps = (state, ownProps) => ({
  accountingPeriodAll: state.ui.expenses.recordListPane.accountingPeriod,
  accountingPeriodActive: state.ui.expenses.recordListPane.accountingPeriod.filter(
    (ap) => ap.active
  ),
  accountingPeriodInactive: state.ui.expenses.recordListPane.accountingPeriod.filter(
    (ap) => !ap.active
  ),
  accountingPeriodIdOriginallySelected:
    state.ui.expenses.selectedExpReport.accountingPeriodId,
  employeeId: state.userSetting.employeeId,
  companyId: state.userSetting.companyId,
  ...ownProps,
});

const mapDispatchToProps = {};

const changeReportonAccountingReport = (
  ownProps,
  selectedAccountingPeriod,
  makeSaveButtonActive
) => {
  ownProps.onChangeEditingExpReport(
    'report.accountingPeriodId',
    selectedAccountingPeriod.id,
    makeSaveButtonActive
  );
  ownProps.onChangeEditingExpReport(
    'report.accountingDate',
    selectedAccountingPeriod.recordingDate,
    makeSaveButtonActive
  );
  ownProps.onChangeEditingExpReport(
    'ui.selectedAccountingPeriod',
    selectedAccountingPeriod,
    makeSaveButtonActive
  );
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  onChangeAccountingPeriod: (e: SyntheticEvent<HTMLSelectElement>) => {
    const selectedAccountingPeriodId = e.currentTarget.value;
    const selectedAccountingPeriod = find(stateProps.accountingPeriodAll, {
      id: selectedAccountingPeriodId,
    });
    changeReportonAccountingReport(ownProps, selectedAccountingPeriod, true);
  },
  resetAccountingPeriod: () => {
    ownProps.onChangeEditingExpReport('report.accountingDate', '', false);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(AccountingPeriodAndRecordDate): React.ComponentType<Object>);
