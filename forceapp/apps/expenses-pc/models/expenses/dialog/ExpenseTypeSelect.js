// @flow
import { type ExpenseTypeList } from '../../../../domain/models/exp/ExpenseType';

export type ExpenseTypeSelectList = Array<ExpenseTypeList>;
