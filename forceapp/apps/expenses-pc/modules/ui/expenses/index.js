// @flow

import { combineReducers } from 'redux';

import mode from './mode';
import overlap from './overlap';
import selectedExpReport from './selectedExpReport';
import tab from './tab';
import recordListPane from './recordListPane';
import recordItemPane from './recordItemPane';
import dialog from './dialog';
import reportList from './reportList';
import receiptLibrary from './receiptLibrary';

export default combineReducers<Object, Object>({
  dialog,
  mode,
  overlap,
  recordItemPane,
  recordListPane,
  reportList,
  selectedExpReport,
  tab,
  receiptLibrary,
});
