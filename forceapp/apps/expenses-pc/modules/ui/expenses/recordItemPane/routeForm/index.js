// @flow

import { combineReducers } from 'redux';

import roundTrip from './roundTrip';
import origin from './origin';
import viaList from './viaList';
import arrival from './arrival';
import option from './option';
import errors from './errors';
import edits from './edits';

export default combineReducers<Object, Object>({
  roundTrip,
  origin,
  viaList,
  arrival,
  option,
  errors,
  edits,
});
