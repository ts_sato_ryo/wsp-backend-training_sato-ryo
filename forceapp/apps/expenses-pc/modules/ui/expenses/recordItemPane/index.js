// @flow

import { combineReducers } from 'redux';

import routeForm from './routeForm';
import tax from './tax';
import foreignCurrency from './foreignCurrency';
import workingDays from './workingDays';
import fixedAmountOption from './fixedAmountOption';

export default combineReducers<Object, Object>({
  routeForm,
  tax,
  foreignCurrency,
  workingDays,
  fixedAmountOption,
});
