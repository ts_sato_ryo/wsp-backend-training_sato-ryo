// @flow
import type { Reducer } from 'redux';

import { merge } from 'lodash';

import {
  searchExchangeRate,
  type ExchangeRateList,
} from '../../../../../../domain/models/exp/foreign-currency/ExchangeRate';
import {
  catchApiError,
  loadingEnd,
  loadingStart,
} from '../../../../../../commons/actions/app';

export const ACTIONS = {
  SEARCH_SUCCESS:
    'MODULES/EXPENSES/RECORD_ITEM_PANE/FOREIGN_CURRENCY/EXCHANGE_RATE/SEARCH_SUCCESS',
};

const searchSuccess = (currencyId, recordDate, result: any) => ({
  type: ACTIONS.SEARCH_SUCCESS,
  payload: { [currencyId]: { [recordDate]: result.records[0] } },
});

export const actions = {
  search: (companyId: string, currencyId: string, recordDate: string) => {
    return (dispatch: Dispatch<any>) => {
      let rate = 0;
      dispatch(loadingStart());
      return searchExchangeRate(companyId, currencyId, recordDate)
        .then((res: any) => {
          // get rate from api if exist, else default to 0.
          rate = res.records[0] ? res.records[0].calculationRate : 0;
          dispatch(loadingEnd());
          dispatch(searchSuccess(currencyId, recordDate, res));
          return rate;
        })
        .catch((err) => {
          dispatch(loadingEnd());
          dispatch(catchApiError(err, { isContinuable: true }));
          throw err;
        });
    };
  },
};

// Reducer
const initialState = {};

type Props = { [string]: ExchangeRateList };

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SEARCH_SUCCESS:
      return merge(state, action.payload);
    default:
      return state;
  }
}: Reducer<Props, any>);
