// @flow
import type { Reducer } from 'redux';

import type { CostCenterList } from '../../../../../../domain/models/exp/CostCenter';

export const ACTIONS = {
  SET: 'MODULES/EXPENSES/DIALOG/COST_CENTER_SELECT/LIST/SET',
  SET_SEARCH: 'MODULES/EXPENSES/DIALOG/COST_CENTER_SELECT/LIST/SET_SEARCH',
  SET_RECENT: 'MODULES/EXPENSES/DIALOG/COST_CENTER_SELECT/LIST/SET_RECENT',
  CLEAR: 'MODULES/EXPENSES/DIALOG/CLEAR',
};

export const actions = {
  set: (costCenterSelectList: CostCenterList) => ({
    type: ACTIONS.SET,
    payload: costCenterSelectList,
  }),
  setSearchResult: (costCenterSearchList: CostCenterList) => ({
    type: ACTIONS.SET_SEARCH,
    payload: costCenterSearchList,
  }),
  setRecentResult: (costCenterRecentItems: CostCenterList) => ({
    type: ACTIONS.SET_RECENT,
    payload: costCenterRecentItems,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

type State = {
  selectionList: CostCenterList,
  searchList: CostCenterList,
  recentItems: CostCenterList,
};

const initialState = { selectionList: [], searchList: [], recentItems: [] };

/* Reducer */
export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return { ...state, selectionList: action.payload };
    case ACTIONS.SET_SEARCH:
      return { ...state, searchList: action.payload };
    case ACTIONS.SET_RECENT:
      return { ...state, recentItems: action.payload };
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
}: Reducer<State, any>);
