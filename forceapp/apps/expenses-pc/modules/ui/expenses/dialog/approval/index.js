// @flow

import { combineReducers } from 'redux';

import comment from './comment';

export default combineReducers<Object, Object>({
  comment,
});
