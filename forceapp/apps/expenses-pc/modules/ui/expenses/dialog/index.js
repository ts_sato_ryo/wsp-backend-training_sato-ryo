// @flow

import { combineReducers } from 'redux';

import activeDialog from './activeDialog';
import approval from './approval';
import costCenterSelect from './costCenterSelect';
import expenseTypeSelect from './expenseTypeSelect';
import jobSelect from './jobSelect';
import extendedItem from './extendedItem';
import vendor from './vendor';
import progressBar from './progressBar';
import recordClone from './recordClone';
import recordUpdated from './recordUpdated';

export default combineReducers<Object, Object>({
  activeDialog,
  approval,
  costCenterSelect,
  expenseTypeSelect,
  jobSelect,
  extendedItem,
  vendor,
  progressBar,
  recordClone,
  recordUpdated,
});
