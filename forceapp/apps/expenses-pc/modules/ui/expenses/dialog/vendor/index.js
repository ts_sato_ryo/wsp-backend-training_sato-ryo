// @flow

import { combineReducers } from 'redux';

import search from './search';
import recentlyUsed from './recentlyUsed';

export default combineReducers<Object, Object>({
  search,
  recentlyUsed,
});
