// @flow
import { type Reducer } from 'redux';
import _ from 'lodash';

export const ACTIONS = {
  SET: 'MODULES/UI/EXP/RECEIPT_LIBRARY/STATUS/SET',
  REMOVE: 'MODULES/UI/EXP/RECEIPT_LIBRARY/STATUS/REMOVE',
  CLEAR: 'MODULES/UI/EXP/RECEIPT_LIBRARY/STATUS/CLEAR',
};

export const actions = {
  set: (receiptFileId: string, status: string) => ({
    type: ACTIONS.SET,
    payload: { [receiptFileId]: status },
  }),
  remove: (receiptFileId: string) => ({
    type: ACTIONS.REMOVE,
    payload: receiptFileId,
  }),
  clear: () => ({ type: ACTIONS.CLEAR }),
};

const initialState = {};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const tmpState = _.cloneDeep(state);
      const obj = _.merge(tmpState, action.payload);
      return obj;
    case ACTIONS.REMOVE:
      const newState = _.cloneDeep(state);
      delete newState[action.payload];
      return newState;
    default:
      return state;
  }
}: Reducer<Object, any>);
