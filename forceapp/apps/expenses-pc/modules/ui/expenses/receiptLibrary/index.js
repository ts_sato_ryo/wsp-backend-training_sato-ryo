/* @flow */
import { combineReducers } from 'redux';

import status from './status';
import selectedReceipt from './selectedReceipt';

export default combineReducers<Object, Object>({
  selectedReceipt,
  status,
});
