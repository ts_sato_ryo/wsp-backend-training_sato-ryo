// @flow

import { combineReducers } from 'redux';

import summary from './summary';
import recordList from './recordList';
import accountingPeriod from './accountingPeriod';

export default combineReducers<Object, Object>({
  summary,
  recordList,
  accountingPeriod,
});
