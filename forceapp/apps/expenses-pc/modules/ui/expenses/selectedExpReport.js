// @flow
import { type Reducer } from 'redux';

import {
  type Report,
  initialStateReport,
} from '../../../../domain/models/exp/Report';

import { getEIsOnly } from '../../../../domain/models/exp/ExtendedItem';

import type {
  ExpenseReportType,
  ExpenseReportTypeList,
} from '../../../../domain/models/exp/expense-report-type/list';
import type { DefaultCostCenter } from '../../../../domain/models/exp/CostCenter';

type DefaultData = {
  reportType: ?ExpenseReportType,
  defaultCostCenter: ?DefaultCostCenter,
};

export const ACTIONS = {
  NEW_REPORT: 'MODULES/UI/EXPENSES/SELECTED_EXP_REPORT/NEW_REPORT',
  SELECT: 'MODULES/UI/EXPENSES/SELECTED_EXP_REPORT/SELECT',
  CLEAR: 'MODULES/UI/EXPENSES/SELECTED_EXP_REPORT/CLEAR',
};

export const actions = {
  newReport: (defaultData: DefaultData) => ({
    type: ACTIONS.NEW_REPORT,
    payload: defaultData,
  }),
  select: (target: Report, reportTypeList: ?ExpenseReportTypeList) => ({
    type: ACTIONS.SELECT,
    payload: { target, reportTypeList },
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

const getExtendedItemInfo = (reportType: ExpenseReportType) => {
  const extendedItemData = getEIsOnly(reportType);
  extendedItemData.expReportTypeId = reportType.id;
  return extendedItemData;
};

// Currently, when an Admin change the Report Type's Extended Items, nothing is being done (part of EXP-1003)
// The following method performs merging of data for existing Report with the current Extended Items from the Report TYpe
/*
const mergeExistingDataWithNewReportType = (
  originalReport: Report,
  reportType: ExpenseReportType
) => {
  const currentExtendedItemsValueMap = new Map();
  for (let i = 1; i <= totalNumExtendedItems; i++) {
    const index = `0${i}`.slice(-2);
    currentExtendedItemsValueMap.set(
      originalReport[`extendedItemText${index}Id`],
      originalReport[`extendedItemText${index}Value`]
    );
  }
  const updateExtendedData = {};
  for (let i = 1; i <= totalNumExtendedItems; i++) {
    const index = `0${i}`.slice(-2);
    updateExtendedData[`extendedItemText${index}Id`] =
      reportType[`extendedItemText${index}Id`];
    updateExtendedData[`extendedItemText${index}Info`] =
      reportType[`extendedItemText${index}Info`];
    const currentValue = currentExtendedItemsValueMap.get(
      reportType[`extendedItemText${index}Id`]
    );
    if (currentValue) {
      updateExtendedData[`extendedItemText${index}Value`] = currentValue;
    } else if (reportType[`extendedItemText${index}Info`]) {
      updateExtendedData[`extendedItemText${index}Value`] =
        reportType[`extendedItemText${index}Info`].defaultValueText;
    } else {
      updateExtendedData[`extendedItemText${index}Value`] = null;
    }
  }
  updateExtendedData.expReportTypeId = reportType.id;
  return updateExtendedData;
};
*/

const reportIsReadOnly = (reportStatus) =>
  reportStatus !== 'Removed' && reportStatus !== 'NotRequested';

export default ((state = initialStateReport, action) => {
  switch (action.type) {
    case ACTIONS.SELECT:
      const reportStatus = (action.payload.target || {}).status;
      // Updating Report is only performed for Editable Reports
      if (
        !reportIsReadOnly(reportStatus) &&
        !(action.payload.target || {}).preRequestId
      ) {
        // Check if need to use reportType. Only active one are passed to this
        if (
          action.payload.reportTypeList &&
          action.payload.reportTypeList.length > 0
        ) {
          // Check if existing report is already using ReportType
          if (action.payload.target.expReportTypeId) {
            // Find the Updated ReportType (if no longer active, then use the default (index 0)
            const reportTypeToUse = action.payload.reportTypeList.find(
              (reportType) =>
                reportType.id === action.payload.target.expReportTypeId
            );
            if (reportTypeToUse) {
              // TODO: When extended item of existing report is changed, the existing report isn't updated
              return {
                ...action.payload.target,
                // Doesn't perform the merging (part of EXP-1003): see comment on mergeExistingDataWithNewReportType method
                /*
                ...mergeExistingDataWithNewReportType(
                  action.payload.target,
                  reportTypeToUse
                ),
                */
              };
            } else {
              // Report Type is no longer active. set it to false.
              return {
                ...action.payload.target,
                expReportTypeId: null,
              };
            }
          } else {
            return action.payload.target;
          }
        } else {
          // Previously using Report Type and now no more report type in the system (and it's non-readonly).
          return {
            ...action.payload.target,
            expReportTypeId: null,
          };
        }
      }
      return action.payload.target;
    case ACTIONS.CLEAR:
      return initialStateReport;
    case ACTIONS.NEW_REPORT:
      if (action.payload.reportType) {
        const reportType = action.payload.reportType;
        return {
          ...initialStateReport,
          ...getExtendedItemInfo(reportType),
          ...action.payload.defaultCostCenter,
          useFileAttachment: reportType.useFileAttachment,
        };
      } else {
        return {
          ...initialStateReport,
          ...action.payload.defaultCostCenter,
        };
      }
    default:
      return state;
  }
}: Reducer<Report, any>);
