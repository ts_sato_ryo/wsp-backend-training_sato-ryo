/* @flow */
import { combineReducers } from 'redux';

import expenses from './expenses';

export default combineReducers<Object, Object>({
  expenses,
});
