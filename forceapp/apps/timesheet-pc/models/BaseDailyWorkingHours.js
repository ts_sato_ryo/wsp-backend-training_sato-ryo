/**
 * [Value Object] 日次の勤務時刻の基底クラス
 * - AttRecordの抜粋
 */
export default class BaseDailyWorkingHours {
  /**
   * @param {Object} param
   */
  constructor(param = {}) {
    /**
     * 出勤時刻 WSP勤怠時刻形式
     * @type {Number}
     */
    this.startTime = param.startTime;

    /**
     * 退勤時刻 WSP勤怠時刻形式
     * @type {String}
     */
    this.endTime = param.endTime;

    /**
     * 休憩時刻
     * @type {Array<{start: Number, end: Number}>}
     */
    this.restTimes = param.restTimes || [];
  }

  /**
   * 時間の種別
   * - WORK: 労働時間
   * - REST: 休憩時間
   * @type {Object<String, String>}
   */
  static TYPE = {
    WORK: 'WORK',
    REST: 'REST',
  };
}
