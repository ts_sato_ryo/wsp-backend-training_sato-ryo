import BaseDailyWorkingHours from './BaseDailyWorkingHours';

/**
 * [Value Object] 日次の所定時刻
 * - AttRecordの抜粋
 */
export default class DailyContractedDetail extends BaseDailyWorkingHours {
  /**
   * @param {Number} startTime
   * @param {Number} endTime
   * @param {Number} [rest1StartTime]
   * @param {Number} [rest1EndTime]
   * @param {Number} [rest2StartTime]
   * @param {Number} [rest2EndTime]
   * @param {Number} [rest3StartTime]
   * @param {Number} [rest3EndTime]
   * @param {Number} [rest4StartTime]
   * @param {Number} [rest4EndTime]
   * @param {Number} [rest5StartTime]
   * @param {Number} [rest5EndTime]
   * @returns {DailyContractedDetail}
   */
  static createFromParam({
    startTime,
    endTime,
    rest1StartTime,
    rest1EndTime,
    rest2StartTime,
    rest2EndTime,
    rest3StartTime,
    rest3EndTime,
    rest4StartTime,
    rest4EndTime,
    rest5StartTime,
    rest5EndTime,
  } = {}) {
    const restTimes = [];

    if (rest1StartTime && rest1EndTime) {
      restTimes.push({ start: rest1StartTime, end: rest1EndTime });
    }

    if (rest2StartTime && rest2EndTime) {
      restTimes.push({ start: rest2StartTime, end: rest2EndTime });
    }

    if (rest3StartTime && rest3EndTime) {
      restTimes.push({ start: rest3StartTime, end: rest3EndTime });
    }

    if (rest4StartTime && rest4EndTime) {
      restTimes.push({ start: rest4StartTime, end: rest4EndTime });
    }

    if (rest5StartTime && rest5EndTime) {
      restTimes.push({ start: rest5StartTime, end: rest5EndTime });
    }

    if (!startTime && !endTime && restTimes.length === 0) {
      return undefined;
    }

    return new DailyContractedDetail({
      startTime,
      endTime,
      restTimes,
    });
  }
}
