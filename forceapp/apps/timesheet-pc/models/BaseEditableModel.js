/**
 * [Base] 編集可能モデルの基底クラス
 */
export default class BaseEditableModel {
  /**
   * @param {Object} param
   */
  constructor(param) {
    /**
     * 読み込み専用モードを識別するためのフラグ
     * @type {Boolean}
     */
    this.isReadOnly = param.isReadOnly;
  }

  /**
   * イミュータブルなフィールドの更新。モデルの新しいインスタンスを返却する
   * @param key
   * @param value
   * @return {BaseEditableModel}
   */
  update(key, value) {
    return new this.constructor({
      ...this,
      [key]: value,
    });
  }

  /**
   * @return {BaseEditableModel}
   */
  copyAsReadOnly() {
    return this.update('isReadOnly', true);
  }

  /**
   * @return {BaseEditableModel}
   */
  copyAsEditable() {
    return this.update('isReadOnly', false);
  }

  /**
   * @abstract
   */
  convertToPostRequestParam() {
    throw new Error(
      `'convertToPostRequestParam()' is not implemented in [${
        this.constructor.name
      }]`
    );
  }
}
