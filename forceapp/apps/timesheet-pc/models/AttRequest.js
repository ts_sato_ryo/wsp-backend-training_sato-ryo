// @flow

import type { Code } from '../../domain/models/attendance/AttDailyRequestType';
import type { Status } from '../../domain/models/approval/request/Status';

/**
 * 勤務表データ中の各種申請を表す型
 * @type {AttRequest}
 */
export type AttRequest = {
  /**
   * 申請ID
   * @type {string}
   */
  +id: string,

  /**
   * 申請タイプコード
   * @type {string}
   */
  +requestTypeCode: Code,

  /**
   * 申請ステータス
   * @type {RequestStatusType}
   */
  +status: Status,

  /**
   * 開始日(YYYY-MM-DD)
   * @type {string}
   */
  +startDate: string,

  /**
   * 終了日(YYYY-MM-DD)
   * @type {string}
   */
  +endDate: string,

  /**
   * 開始時刻
   * 00:00から経過した分 e.g. 900
   * @type {number}
   */
  +startTime: number,

  /**
   * 終了時刻
   * 00:00から経過した分 e.g. 3600
   * @type {number}
   */
  +endTime: number,

  /**
   * 備考
   * 最大255文字
   * @type {string}
   */
  +remarks: string,

  /**
   * 休暇名コード
   * @type {string|null}
   */
  +leaveCode: string | null,

  /**
   * 休暇名
   * @type {string|null}
   */
  +leaveName: string | null,

  /**
   * 休暇タイプ
   * Paid	有休
   * Unpaid	無休
   * Substitute	振替
   * Compensatory	代休
   * @type {string|null}
   */
  +leaveType: 'Paid' | 'Unpaid' | 'Substitute' | 'Compensatory' | null,

  /**
   * 休暇範囲
   * @type {string|null}
   */
  +leaveRange: string | null,

  /**
   * 休日出勤取得休日タイプ
   * ※ HolidayWorkの場合のみ有効
   * @type {string|null}
   */
  +substituteLeaveRange: string | null,

  /**
   * 振替休日取得日
   * ※ HolidayWorkの場合のみ有効
   * @type {string|null}
   */
  +substituteDate: string | null,

  /**
   * 直行直帰申請の出勤時間のデフォルト値
   */
  +directApplyStartTime: number | null,

  /**
   * 直行直帰申請の退勤時間のデフォルト値
   */
  +directApplyEndTime: number | null,

  /**
   * 直行直帰申請の休憩開始時間1のデフォルト値
   */
  +directApplyRest1StartTime: number | null,

  /**
   * 直行直帰申請の休憩終了時間1のデフォルト値
   */
  +directApplyRest1EndTime: number | null,

  /**
   * 直行直帰申請の休憩開始時間2のデフォルト値
   */
  +directApplyRest2StartTime: number | null,

  /**
   * 直行直帰申請の休憩終了時間2のデフォルト値
   */
  +directApplyRest2EndTime: number | null,

  /**
   * 直行直帰申請の休憩開始時間3のデフォルト値
   */
  +directApplyRest3StartTime: number | null,

  /**
   * 直行直帰申請の休憩終了時間3のデフォルト値
   */
  +directApplyRest3EndTime: number | null,

  /**
   * 直行直帰申請の休憩開始時間4のデフォルト値
   */
  +directApplyRest4StartTime: number | null,

  /**
   * 直行直帰申請の休憩終了時間4のデフォルト値
   */
  +directApplyRest4EndTime: number | null,

  /**
   * 直行直帰申請の休憩開始時間5のデフォルト値
   */
  +directApplyRest5StartTime: number | null,

  /**
   * 直行直帰申請の休憩終了時間2のデフォルト値
   */
  +directApplyRest5EndTime: number | null,

  /**
   * 勤務時間変更申請の休憩開始時間1のデフォルト値
   */
  +patternApplyRest1StartTime: number | null,

  /**
   * 勤務時間変更申請の休憩終了時間1のデフォルト値
   */
  +patternApplyRest1EndTime: number | null,

  /**
   * 勤務時間変更申請の休憩開始時間2のデフォルト値
   */
  +patternApplyRest2StartTime: number | null,

  /**
   * 勤務時間変更申請の休憩終了時間2のデフォルト値
   */
  +patternApplyRest2EndTime: number | null,

  /**
   * 勤務時間変更申請の休憩開始時間3のデフォルト値
   */
  +patternApplyRest3StartTime: number | null,

  /**
   * 勤務時間変更申請の休憩終了時間3のデフォルト値
   */
  +patternApplyRest3EndTime: number | null,

  /**
   * 勤務時間変更申請の休憩開始時間4のデフォルト値
   */
  +patternApplyRest4StartTime: number | null,

  /**
   * 勤務時間変更申請の休憩終了時間4のデフォルト値
   */
  +patternApplyRest4EndTime: number | null,

  /**
   * 勤務時間変更申請の休憩開始時間5のデフォルト値
   */
  +patternApplyRest5StartTime: number | null,

  /**
   * 勤務時間変更申請の休憩終了時間2のデフォルト値
   */
  +patternApplyRest5EndTime: number | null,

  /**
   * 勤務パターンコード
   */
  +patternCode: string | null,

  /**
   * 勤務パターン名
   */
  +patternName: string | null,
};
