import BaseEditableModel from './BaseEditableModel';

/**
 * [Value Object] 日次備考
 * - AttRecordの抜粋
 */
export default class DailyRemarks extends BaseEditableModel {
  /**
   * @param {Object} param
   */
  constructor(param = {}) {
    super(param);

    /**
     * 勤怠明細ID
     * @type {String}
     */
    this.recordId = param.recordId;

    /**
     * 日付(YYYY-MM-DD形式の文字列)
     * @type {String}
     */
    this.recordDate = param.recordDate;

    /**
     * 備考内容
     * @type {String}
     */
    this.remarks = param.remarks;

    /**
     * 勤怠日次申請に含まれる備考内容
     * - TODO: モデルの本質からやや外れた実装なので、別のstateフィールドに含める方針を検討したい
     * @type {Array<{requestTypeName: String, remarks: String}>}
     */
    this.otherRemarksFromRequest = param.otherRemarksFromRequest;
  }

  /**
   * @override
   * @return {{recordId: String, remarks: String}}
   */
  convertToPostRequestParam() {
    return {
      recordId: this.recordId,
      remarks: this.remarks,
    };
  }

  /**
   * @param {String} id 勤怠日次明細ID
   * @param {String} recordDate
   * @param {Boolean} isTotallyRocked
   * @param {String} [remarks]
   * @param {Array<{requestTypeName: String, remarks: String}>} otherRemarksFromRequest
   * @returns {DailyRemarks}
   */
  static createFromParam(
    { id, recordDate, isTotallyLocked, remarks },
    otherRemarksFromRequest
  ) {
    return new DailyRemarks({
      recordId: id,
      isReadOnly: isTotallyLocked,
      recordDate,
      remarks: remarks || '',
      otherRemarksFromRequest,
    });
  }
}
