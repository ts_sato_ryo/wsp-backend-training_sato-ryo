import BaseEditableModel from './BaseEditableModel';
import AttSummaryModel from './AttSummary';

/**
 * 勤怠確定申請
 */
export default class AttFixSummaryRequest extends BaseEditableModel {
  /**
   * @param {Object} param
   */
  constructor(param) {
    super(param);

    /**
     * 勤怠サマリーID
     * @type {String}
     */
    this.summaryId = param.summaryId;

    /**
     * 勤務確定申請ID
     * @type {String}
     */
    this.requestId = param.requestId;

    /**
     * 申請コメント
     * @type {String}
     */
    this.comment = param.comment;

    /**
     * 確定申請に関して実行可能な操作
     * @type {String}
     */
    this.performableActionForFix = param.performableActionForFix;
  }

  /**
   * @return {Object}
   */
  convertToPostRequestParam() {
    const {
      Submit,
      CancelRequest,
      CancelApproval,
    } = AttSummaryModel.ACTIONS_FOR_FIX;

    switch (this.performableActionForFix) {
      case Submit:
        return {
          summaryId: this.summaryId,
          comment: this.comment,
        };

      case CancelRequest:
      case CancelApproval:
        return {
          requestId: this.requestId,
          comment: this.comment,
        };

      default:
        return undefined;
    }
  }

  /**
   * @param {String} id 勤怠サマリーID
   * @param {String} requestId 勤務確定申請ID
   * @param {String} performableActionForFix 確定申請に関して実行可能な操作
   * @return {AttFixSummaryRequest}
   */
  static createFromParam({ id, requestId, performableActionForFix }) {
    return new AttFixSummaryRequest({
      summaryId: id,
      requestId,
      performableActionForFix,
    });
  }
}
