import { create } from '../DailyAttentionMessages';
import AttRecord from '../AttRecord';
import msg from '../../../commons/languages';
import TextUtil from '../../../commons/utils/TextUtil';

describe('timecheet-pc/models/DailyAttentionMessages', () => {
  describe('法定休憩不足', () => {
    const attRecord = AttRecord.createFromParam(
      {
        dayType: AttRecord.DAY_TYPE.WORKDAY,
        insufficientRestTime: 5,
        startTime: null,
        endTime: null,
        outStartTime: null,
        outEndTime: null,
        requestIds: [],
      },
      false
    );
    const attentions = create(attRecord, {});
    expect(attentions).toHaveLength(1);
    expect(attentions[0]).toEqual(
      TextUtil.template(msg().Att_Msg_InsufficientRestTime, 5)
    );
  });

  describe('勤務時間外', () => {
    test('時間外で出退勤', () => {
      const attRecord = AttRecord.createFromParam(
        {
          dayType: AttRecord.DAY_TYPE.WORKDAY,
          startTime: 0,
          endTime: 1 * 60,
          outStartTime: 0,
          outEndTime: 0,
          requestIds: [],
        },
        false
      );
      const attentions = create(attRecord, {});
      expect(attentions).toHaveLength(1);
      expect(attentions[0]).toEqual(
        TextUtil.template(msg().Att_Msg_NotIncludeWorkingTime, '00:00', '01:00')
      );
    });

    test('出勤が時間外', () => {
      const attRecord = AttRecord.createFromParam(
        {
          dayType: AttRecord.DAY_TYPE.WORKDAY,
          startTime: 0,
          endTime: 2 * 60,
          outStartTime: 1 * 60,
          outEndTime: 2 * 60,
          requestIds: [],
        },
        false
      );
      const attentions = create(attRecord, {});
      expect(attentions).toHaveLength(1);
      expect(attentions[0]).toEqual(
        TextUtil.template(msg().Att_Msg_NotIncludeWorkingTime, '00:00', '01:00')
      );
    });

    test('退勤が時間外', () => {
      const attRecord = AttRecord.createFromParam(
        {
          dayType: AttRecord.DAY_TYPE.WORKDAY,
          startTime: 0,
          endTime: 2 * 60,
          outStartTime: 0,
          outEndTime: 1 * 60,
          requestIds: [],
        },
        false
      );
      const attentions = create(attRecord, {});
      expect(attentions).toHaveLength(1);
      expect(attentions[0]).toEqual(
        TextUtil.template(msg().Att_Msg_NotIncludeWorkingTime, '01:00', '02:00')
      );
    });

    test('時間内で働いた時間が有るが出退勤時間が時間外', () => {
      const attRecord = AttRecord.createFromParam(
        {
          dayType: AttRecord.DAY_TYPE.WORKDAY,
          startTime: 0,
          endTime: 3 * 60,
          outStartTime: 1 * 60,
          outEndTime: 2 * 60,
          requestIds: [],
        },
        false
      );
      const attentions = create(attRecord, {});
      expect(attentions).toHaveLength(2);
      expect(attentions[0]).toEqual(
        TextUtil.template(msg().Att_Msg_NotIncludeWorkingTime, '00:00', '01:00')
      );
      expect(attentions[1]).toEqual(
        TextUtil.template(msg().Att_Msg_NotIncludeWorkingTime, '02:00', '03:00')
      );
    });

    test('時間内', () => {
      const attRecord = AttRecord.createFromParam(
        {
          dayType: AttRecord.DAY_TYPE.WORKDAY,
          startTime: 0,
          endTime: 1 * 60,
          outStartTime: 0,
          outEndTime: 1 * 60,
          requestIds: [],
        },
        false
      );
      const attentions = create(attRecord, {});
      expect(attentions).toHaveLength(0);
    });
  });

  test('複数メッセージ', () => {
    const attRecord = AttRecord.createFromParam(
      {
        dayType: AttRecord.DAY_TYPE.WORKDAY,
        insufficientRestTime: 5,
        startTime: 0,
        endTime: 3 * 60,
        outStartTime: 1 * 60,
        outEndTime: 2 * 60,
        requestIds: [],
      },
      false
    );
    const attentions = create(attRecord, {});
    expect(attentions).toHaveLength(3);
  });
});
