import AttSummary from '../AttSummary';

describe('timesheet-pc/models/AttSummary', () => {
  describe('setPerformableActionForFix()', () => {
    const BASE_ATT_SUMMARY = AttSummary.createFromParam({
      id: 'aaaaa',
      requestId: 'bbbbb',
      status: AttSummary.STATUS.NotRequested,
      isLocked: false,
      isLeaveOfAbsence: false,
    });

    test('ステータス[未申請]: 承認申請を実行可能', () => {
      const anAttSummary = AttSummary.createFromParam({
        ...BASE_ATT_SUMMARY,
        status: AttSummary.STATUS.NotRequested,
      });
      expect(anAttSummary.performableActionForFix).toEqual(
        AttSummary.ACTIONS_FOR_FIX.Submit
      );
    });

    test('ステータス[申請取消]: 承認申請を実行可能', () => {
      const anAttSummary = AttSummary.createFromParam({
        ...BASE_ATT_SUMMARY,
        status: AttSummary.STATUS.Removed,
      });
      expect(anAttSummary.performableActionForFix).toEqual(
        AttSummary.ACTIONS_FOR_FIX.Submit
      );
    });

    test('ステータス[承認取消]: 承認申請を実行可能', () => {
      const anAttSummary = AttSummary.createFromParam({
        ...BASE_ATT_SUMMARY,
        status: AttSummary.STATUS.Canceled,
      });
      expect(anAttSummary.performableActionForFix).toEqual(
        AttSummary.ACTIONS_FOR_FIX.Submit
      );
    });

    test('ステータス[却下]: 承認申請を実行可能', () => {
      const anAttSummary = AttSummary.createFromParam({
        ...BASE_ATT_SUMMARY,
        status: AttSummary.STATUS.Rejected,
      });
      expect(anAttSummary.performableActionForFix).toEqual(
        AttSummary.ACTIONS_FOR_FIX.Submit
      );
    });

    test('ステータス[承認待ち]: 申請取消を実行可能', () => {
      const anAttSummary = AttSummary.createFromParam({
        ...BASE_ATT_SUMMARY,
        status: AttSummary.STATUS.Pending,
      });
      expect(anAttSummary.performableActionForFix).toEqual(
        AttSummary.ACTIONS_FOR_FIX.CancelRequest
      );
    });

    test('ステータス[承認済み]: 承認取消を実行可能', () => {
      const anAttSummary = AttSummary.createFromParam({
        ...BASE_ATT_SUMMARY,
        status: AttSummary.STATUS.Approved,
      });
      expect(anAttSummary.performableActionForFix).toEqual(
        AttSummary.ACTIONS_FOR_FIX.CancelApproval
      );
    });

    test('ステータス[null](休職休業中): 承認申請などの操作は不可', () => {
      const anAttSummary = AttSummary.createFromParam({
        ...BASE_ATT_SUMMARY,
        status: null,
        isLeaveOfAbsence: true,
      });
      expect(anAttSummary.performableActionForFix).toEqual(
        AttSummary.ACTIONS_FOR_FIX.None
      );
    });
  });
});
