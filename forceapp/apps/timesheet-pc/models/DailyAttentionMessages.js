/* @flow */
import {
  type AttDailyAttention,
  CODE as ATT_DAILY_ATTENTION_CODE,
  createAttDailyAttentions,
} from '../../domain/models/attendance/AttDailyAttention';
import { type AttDailyRecord } from '../../domain/models/attendance/AttDailyRecord';
import msg from '../../commons/languages';
import TextUtil from '../../commons/utils/TextUtil';
import TimeUtil from '../../commons/utils/TimeUtil';

export type DailyAttentionMessages = Array<string>;

/**
 * 指定の時間が勤務時間に含まれない旨を伝えるメッセージを返します。
 * @param fromTime 基準となる時間。計算対象の開始時間。
 * @param toTime 計算対象の終了時間。
 * @return string
 */
function makeNotIncludeWorkingTimeMessage(
  fromTime: number,
  toTime: number
): string {
  return TextUtil.template(
    msg().Att_Msg_NotIncludeWorkingTime,
    TimeUtil.toHHmm(fromTime),
    TimeUtil.toHHmm(toTime)
  );
}

/**
 * 不足休憩時間のメッセージを返します。
 * @param insufficientRestTime
 * @return message
 */
function makeInsufficientRestTimeMessage(insufficientRestTime: number): string {
  return TextUtil.template(
    msg().Att_Msg_InsufficientRestTime,
    insufficientRestTime
  );
}

// eslint-disable-next-line import/prefer-default-export
export function create(record: AttDailyRecord): DailyAttentionMessages {
  return createAttDailyAttentions(record)
    .map((attention: AttDailyAttention) => {
      switch (attention.code) {
        case ATT_DAILY_ATTENTION_CODE.IneffectiveWorkingTime:
          return makeNotIncludeWorkingTimeMessage(
            attention.value.fromTime,
            attention.value.toTime
          );
        case ATT_DAILY_ATTENTION_CODE.InsufficientRestTime:
          return makeInsufficientRestTimeMessage(attention.value);
        default:
          return '';
      }
    })
    .filter((v) => v);
}
