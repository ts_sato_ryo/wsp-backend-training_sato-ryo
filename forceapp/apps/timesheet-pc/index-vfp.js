import '../commons/config/public-path';

/* eslint-disable import/imports-first */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/* eslint-enable */

import configureStore from './store/configureStore';

import AppContainer from './containers/AppContainer';
import * as AppActions from './action-dispatchers/App';

import '../commons/styles/base.scss';
import '../commons/config/moment';

function renderApp(store, Component) {
  ReactDOM.render(
    <Provider store={store}>
      <Component />
    </Provider>,
    document.getElementById('container')
  );
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = (params) => {
  const configuredStore = configureStore();
  renderApp(configuredStore, AppContainer);
  configuredStore.dispatch(AppActions.initialize(params));
};
