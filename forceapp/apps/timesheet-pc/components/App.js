// @flow

import React from 'react';

import GlobalContainer from '../../commons/containers/GlobalContainer';
import MainContentContainer from '../containers/MainContentContainer';
import PersonalMenuPopoverContainer from '../../commons/containers/PersonalMenuPopoverContainer';
import DailyAttTimeDialogContainer from '../containers/dialogs/DailyAttTimeDialogContainer';
import DailyAttRequestDialogContainer from '../containers/dialogs/DailyAttRequestDialogContainer';
import DailyRemarksDialogContainer from '../containers/dialogs/DailyRemarksDialogContainer';
import DailyAttentionsDialogContainer from '../containers/dialogs/DailyAttentionsDialogContainer';
import FixSummaryRequestDialogContainer from '../containers/dialogs/FixSummaryRequestDialogContainer';
import ApproverEmployeeSettingDialogContainer from '../../commons/containers/dialogs/ApproverEmployeeSettingDialogContanier';
import ApproverEmployeeSearchDialogContainer from '../../commons/containers/dialogs/ApproverEmployeeSearchDialogContanier';
import ApprovalHistoryDialogContainer from '../containers/dialogs/ApprovalHistoryDialogContainer';
import ProxyEmployeeSelectDialogContainer from '../../../widgets/dialogs/ProxyEmployeeSelectDialog/containers/ProxyEmployeeSelectDialogContainer';

import {
  isPermissionSatisfied,
  type Permission,
} from '../../domain/models/access-control/Permission';
import DailySummaryDialogContainer from '../containers/dialogs/DailySummaryDialogContainer';

type Props = {|
  isProxyMode: boolean,
  userPermission: Permission,
  onDecideProxyEmployee: () => void,
  onChangeApproverEmployee: () => void,
|};

export default class App extends React.Component<Props> {
  render() {
    const { onDecideProxyEmployee, userPermission, isProxyMode } = this.props;
    const isEnableViewAttTimeSheetByDelegate = isPermissionSatisfied({
      userPermission,
      isByDelegate: isProxyMode,
      requireIfByEmployee: ['viewAttTimeSheetByDelegate'],
      requireIfByDelegate: ['viewAttTimeSheetByDelegate'],
    });

    return (
      <GlobalContainer>
        <MainContentContainer />

        {/* Dialogs */}
        <PersonalMenuPopoverContainer
          showProxyEmployeeSelectButton={isEnableViewAttTimeSheetByDelegate}
        />
        <ProxyEmployeeSelectDialogContainer onDecide={onDecideProxyEmployee} />
        <DailyAttTimeDialogContainer />
        <DailyAttRequestDialogContainer />
        <DailyRemarksDialogContainer />
        <DailyAttentionsDialogContainer />
        <DailySummaryDialogContainer />
        <FixSummaryRequestDialogContainer />
        <ApprovalHistoryDialogContainer />
        <ApproverEmployeeSettingDialogContainer
          handleSave={this.props.onChangeApproverEmployee}
        />
        <ApproverEmployeeSearchDialogContainer />
      </GlobalContainer>
    );
  }
}
