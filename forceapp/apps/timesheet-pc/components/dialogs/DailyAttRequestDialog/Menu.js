// @flow

import React from 'react';
import classNames from 'classnames';

import msg from '../../../../commons/languages';

import { type AttDailyRequest } from '../../../../domain/models/attendance/AttDailyRequest';
import {
  type Code as RequestTypeCode,
  DisplayOrder as DisplayOrderOfRequest,
} from '../../../../domain/models/attendance/AttDailyRequestType';
import STATUS from '../../../../domain/models/approval/request/Status';

import { type DailyRequestConditionsType } from '../../../models/DailyRequestConditions';

import iconStatusApproved from '../../../../commons/images/iconStatusApprovedNoPhoto.png';
import iconStatusApprovalIn from '../../../../commons/images/iconStatusSubmit.png';
import iconStatusReject from '../../../../commons/images/iconStatusReject.png';
import iconStatusApprovalInIsForReapply from '../../../../commons/images/iconStatusSubmitIsForReapply.png';
import iconStatusRejectIsForReapply from '../../../../commons/images/iconStatusRejectIsForReapply.png';

import './Menu.scss';

const ROOT = 'timesheet-pc-dialogs-daily-att-request-dialog-menu';

const MAP_STATE_TO_ICON = {
  [STATUS.Approved]: iconStatusApproved,
  [STATUS.ApprovalIn]: iconStatusApprovalIn,
  [STATUS.Rejected]: iconStatusReject,
  [STATUS.Removed]: iconStatusReject,
  [STATUS.Canceled]: iconStatusReject,
};

const MAP_STATE_TO_ICON_IS_FOR_REAPPLY = {
  [STATUS.Approved]: iconStatusApproved, // 勤務表がロックされてる場合
  [STATUS.ApprovalIn]: iconStatusApprovalInIsForReapply,
  [STATUS.Rejected]: iconStatusRejectIsForReapply,
  [STATUS.Removed]: iconStatusRejectIsForReapply,
  [STATUS.Canceled]: iconStatusRejectIsForReapply,
};

type MenuItemProps = {
  onClick: Function,
  children: string | null,
  isSelected: ?boolean,
  status?: ?string,
  isForReapply?: ?boolean,
};

const MenuItem = (props: MenuItemProps) => {
  const iconSrc = props.isForReapply
    ? props.status && MAP_STATE_TO_ICON_IS_FOR_REAPPLY[props.status]
    : props.status && MAP_STATE_TO_ICON[props.status];

  return (
    <li className={`${ROOT}__item`}>
      <button
        className={classNames(`${ROOT}__item-button`, {
          [`${ROOT}__item-button--selected`]: props.isSelected,
        })}
        type="text"
        onClick={props.onClick}
      >
        <div
          className={classNames(`${ROOT}__item-button-icon`, {
            [`${ROOT}__item-button-icon--with-image`]: !!iconSrc,
            [`${ROOT}__item-button-icon--is-for-reapply`]: props.isForReapply,
          })}
        >
          {iconSrc && <img src={iconSrc} role="presentation" />}
        </div>
        <div className={`${ROOT}__item-button-content`}>{props.children}</div>
      </button>
    </li>
  );
};

MenuItem.defaultProps = {
  status: null,
  isSelected: false,
};

type Props = {
  requestConditions: DailyRequestConditionsType,
  onClickRequestDetailButton: (AttDailyRequest) => void,
  onClickRequestEntryButton: (string, string) => void,
  selectedRequestId: string | null,
  selectedRequestTypeCode: RequestTypeCode | null,
};

export default class Menu extends React.Component<Props> {
  static defaultProps = {
    selectedRequestId: null,
  };

  // 申請済み
  renderSubmittedRequestsBlock() {
    const {
      selectedRequestId,
      requestConditions,
      onClickRequestDetailButton,
    } = this.props;
    const { latestRequests, isLocked } = requestConditions || {};

    if (!latestRequests || !latestRequests.length) {
      return null;
    }

    return (
      <div className={`${ROOT}__section`}>
        <div className={`${ROOT}__title`}>{msg().Att_Lbl_ApprovelIn}</div>
        <ul className={`${ROOT}__items`}>
          {latestRequests.map((request, i) => {
            const isSelected = request.id === selectedRequestId;
            return (
              <MenuItem
                key={`menu-item-for-${request.id || i}`}
                status={isLocked ? STATUS.Approved : request.status}
                onClick={() => {
                  onClickRequestDetailButton(request);
                }}
                isForReapply={request.isForReapply}
                isSelected={isSelected}
              >
                {request.requestTypeName || null}
              </MenuItem>
            );
          })}
        </ul>
      </div>
    );
  }

  // 新規申請
  renderRequestEntryBlock() {
    const {
      requestConditions,
      onClickRequestEntryButton,
      selectedRequestId,
      selectedRequestTypeCode,
    } = this.props;
    const { isAvailableToEntryNewRequest, availableRequestTypes } =
      requestConditions || {};

    if (!isAvailableToEntryNewRequest) {
      return null;
    }

    const items = DisplayOrderOfRequest.filter(
      (orderedTypeCode) => availableRequestTypes[orderedTypeCode]
    ).map((orderedTypeCode) => {
      const requestType = availableRequestTypes[orderedTypeCode];

      const isSelected =
        !selectedRequestId && selectedRequestTypeCode === requestType.code;

      return (
        <MenuItem
          key={`menu-item-for-${requestType.code}`}
          isSelected={isSelected}
          onClick={() => {
            onClickRequestEntryButton(
              requestType.code,
              requestConditions.recordDate
            );
          }}
        >
          {requestType.name}
        </MenuItem>
      );
    });

    return (
      <div className={`${ROOT}__section`}>
        <div className={`${ROOT}__title`}>{msg().Att_Lbl_NewRequest}</div>
        <ul className={`${ROOT}__items`}>{items}</ul>
      </div>
    );
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        {this.renderSubmittedRequestsBlock()}
        {this.renderRequestEntryBlock()}
      </div>
    );
  }
}
