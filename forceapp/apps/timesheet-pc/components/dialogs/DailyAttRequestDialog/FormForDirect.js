// @flow

import * as React from 'react';

import { type DirectRequest } from '../../../../domain/models/attendance/AttDailyRequest/DirectRequest';
import * as RestTime from '../../../../domain/models/attendance/RestTime';
import DailyAttTime from '../../../models/DailyAttTime';

import DateUtil from '../../../../commons/utils/DateUtil';
import msg from '../../../../commons/languages';

import DateField from '../../../../commons/components/fields/DateField';
import DateRangeField from '../../../../commons/components/fields/DateRangeField';
import AttTimeRangeField from '../../../../commons/components/fields/AttTimeRangeField';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';
import IconButton from '../../../../commons/components/buttons/IconButton';
import Label from '../../../../commons/components/fields/Label';
import TimeUtil from '../../../../commons/utils/TimeUtil';

import ImgBtnPlusField from '../../../images/btnPlusField.png';
import ImgBtnMinusField from '../../../images/btnMinusField.png';

import './FormForDirect.scss';

const ROOT = 'timesheet-pc-dialogs-daily-att-request-dialog-form-for-direct';

export type Props = {
  // state
  isReadOnly: boolean,
  targetRequest: DirectRequest,
  hasRange: boolean,

  // actions
  onUpdateHasRange: (boolean) => void,
  onUpdateValue: ($Keys<DirectRequest>, $Values<DirectRequest>) => void,
};

export default (props: Props) => {
  const {
    isReadOnly,
    targetRequest,
    hasRange,
    onUpdateHasRange,
    onUpdateValue,
  } = props;
  const minOfEndDate = DateUtil.addDays(targetRequest.startDate, 1);
  const directApplyRestTimes =
    isReadOnly && targetRequest.directApplyRestTimes.length > 1
      ? RestTime.filter(targetRequest.directApplyRestTimes)
      : targetRequest.directApplyRestTimes;

  return (
    <div className={ROOT}>
      <Label text={msg().Att_Lbl_Period}>
        {hasRange ? (
          <DateRangeField
            startDateFieldProps={{
              disabled: true,
              value: targetRequest.startDate,
              onChange: (value) => onUpdateValue('startDate', value),
            }}
            endDateFieldProps={{
              disabled: isReadOnly,
              value: targetRequest.endDate,
              minDate: minOfEndDate,
              onChange: (value) => onUpdateValue('endDate', value),
            }}
            required
          />
        ) : (
          <DateField
            value={targetRequest.startDate}
            selected={targetRequest.startDate}
            onChange={(value) => onUpdateValue('startDate', value)}
            disabled
            required
          />
        )}

        <div className="slds-form-element">
          <div className="slds-form-element__control">
            <div className="slds-checkbox">
              <input
                type="checkbox"
                id="range"
                checked={hasRange}
                onChange={(e) => onUpdateHasRange(e.target.checked)}
                disabled={isReadOnly}
              />
              <label className="slds-checkbox__label" htmlFor="range">
                <span className="slds-checkbox_faux" />
                <span className="slds-form-element__label">
                  {msg().Att_Lbl_UsePeriod}
                </span>
              </label>
            </div>
          </div>
        </div>
      </Label>

      <Label text={msg().Att_Lbl_WorkTime}>
        <AttTimeRangeField
          className={`${ROOT}__date-range`}
          startTime={TimeUtil.toHHmm(targetRequest.startTime)}
          endTime={TimeUtil.toHHmm(targetRequest.endTime)}
          onBlurAtStart={(value) =>
            onUpdateValue('startTime', TimeUtil.toMinutes(value))
          }
          onBlurAtEnd={(value) =>
            onUpdateValue('endTime', TimeUtil.toMinutes(value))
          }
          disabled={isReadOnly}
          required
        />
      </Label>

      {directApplyRestTimes.map(
        (restTime, idx, restTimes: RestTime.RestTimes) => (
          <Label key={idx} text={`${msg().Att_Lbl_Rest}${idx + 1}`}>
            <AttTimeRangeField
              className={`${ROOT}__date-range`}
              startTime={TimeUtil.toHHmm(restTime.startTime)}
              endTime={TimeUtil.toHHmm(restTime.endTime)}
              onBlurAtStart={(value) =>
                onUpdateValue(
                  'directApplyRestTimes',
                  RestTime.update(restTimes, idx, {
                    ...restTime,
                    startTime: TimeUtil.toMinutes(value),
                  })
                )
              }
              onBlurAtEnd={(value) =>
                onUpdateValue(
                  'directApplyRestTimes',
                  RestTime.update(restTimes, idx, {
                    ...restTime,
                    endTime: TimeUtil.toMinutes(value),
                  })
                )
              }
              disabled={isReadOnly}
            />
            {!isReadOnly &&
              idx === restTimes.length - 1 &&
              restTimes.length < DailyAttTime.MAX_STANDARD_REST_TIME_COUNT && (
                <IconButton
                  className={`${ROOT}__date-range-button`}
                  src={ImgBtnPlusField}
                  alt={msg().Att_Btn_AddItem}
                  onClick={() =>
                    onUpdateValue(
                      'directApplyRestTimes',
                      RestTime.push(restTimes)
                    )
                  }
                  key="icon-add"
                />
              )}
            {!isReadOnly && restTimes.length > 1 && (
              <IconButton
                className={`${ROOT}__date-range-button--minus`}
                src={ImgBtnMinusField}
                alt={msg().Att_Btn_RemoveItem}
                onClick={() =>
                  onUpdateValue(
                    'directApplyRestTimes',
                    RestTime.remove(restTimes, idx)
                  )
                }
                key="icon-remove"
              />
            )}
          </Label>
        )
      )}

      <Label text={msg().Att_Lbl_Remarks}>
        <TextAreaField
          maxLength={255}
          value={targetRequest.remarks || ''}
          onChange={(e) => onUpdateValue('remarks', e.target.value)}
          disabled={isReadOnly}
        />
      </Label>
    </div>
  );
};
