// @flow

import React from 'react';

import { type OvertimeWorkRequest } from '../../../../domain/models/attendance/AttDailyRequest/OvertimeWorkRequest';

import msg from '../../../../commons/languages';
import DateUtil from '../../../../commons/utils/DateUtil';
import TimeUtil from '../../../../commons/utils/TimeUtil';

import AttTimeRangeField from '../../../../commons/components/fields/AttTimeRangeField';
import Label from '../../../../commons/components/fields/Label';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';

const ROOT =
  'timesheet-pc-dialogs-daily-att-request-dialog-form-for-overtime-work';

type Props = {
  isReadOnly: boolean,
  targetRequest: OvertimeWorkRequest,
  onUpdateValue: (
    $Keys<OvertimeWorkRequest>,
    $Values<OvertimeWorkRequest>
  ) => void,
};

export default class FormForOvertimeWork extends React.Component<Props> {
  render() {
    const { isReadOnly, targetRequest, onUpdateValue } = this.props;

    return (
      <div className={ROOT}>
        <Label text={msg().Att_Lbl_Date}>
          <p>{DateUtil.formatYMD(targetRequest.startDate)}</p>
        </Label>

        <Label text={msg().Att_Lbl_Duration}>
          <AttTimeRangeField
            startTime={TimeUtil.toHHmm(targetRequest.startTime)}
            endTime={TimeUtil.toHHmm(targetRequest.endTime)}
            onBlurAtStart={(value) =>
              onUpdateValue('startTime', TimeUtil.toMinutes(value))
            }
            onBlurAtEnd={(value) =>
              onUpdateValue('endTime', TimeUtil.toMinutes(value))
            }
            disabled={isReadOnly}
            required
          />
        </Label>

        <Label text={msg().Att_Lbl_Remarks}>
          <TextAreaField
            maxLength={255}
            value={targetRequest.remarks || ''}
            onChange={(e) => onUpdateValue('remarks', e.target.value)}
            disabled={isReadOnly}
          />
        </Label>
      </div>
    );
  }
}
