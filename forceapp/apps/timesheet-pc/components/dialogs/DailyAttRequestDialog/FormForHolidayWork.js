// @flow

import React from 'react';

import { isForReapply } from '../../../../domain/models/attendance/AttDailyRequest';
import { type HolidayWorkRequest } from '../../../../domain/models/attendance/AttDailyRequest/HolidayWorkRequest';
import {
  type SubstituteLeaveType,
  SUBSTITUTE_LEAVE_TYPE,
  ORDER_OF_SUBSTITUTE_LEAVE_TYPES,
} from '../../../../domain/models/attendance/SubstituteLeaveType';

import msg from '../../../../commons/languages';
import TimeUtil from '../../../../commons/utils/TimeUtil';

import Label from '../../../../commons/components/fields/Label';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';
import DateField from '../../../../commons/components/fields/DateField';
import RadioGroupField, {
  LAYOUT_TYPE as RADIO_GROUP_LAYOUT_TYPE,
} from '../../../../commons/components/fields/RadioGroupField';
import AttTimeRangeField from '../../../../commons/components/fields/AttTimeRangeField';

type Props = {
  isReadOnly: boolean,
  targetRequest: HolidayWorkRequest,
  substituteLeaveTypeList: SubstituteLeaveType[],
  onUpdateValue: (
    $Keys<HolidayWorkRequest>,
    $Values<HolidayWorkRequest>
  ) => void,
};

const LEAVE_TYPE_LABEL_MAP = {
  [SUBSTITUTE_LEAVE_TYPE.None]: () => msg().Att_Lbl_DoNotUseReplacementDayOff,
  [SUBSTITUTE_LEAVE_TYPE.Substitute]: () => msg().Att_Lbl_Substitute,
};

export default class FormForHolidayWork extends React.Component<Props> {
  renderOriginalTargetDateField() {
    const { isReadOnly, targetRequest, onUpdateValue } = this.props;

    const enabled = isForReapply(targetRequest);

    return (
      <Label text={msg().Att_Lbl_HolidayWorkDate}>
        <DateField
          className="ts-text-field slds-input"
          value={targetRequest.startDate}
          selected={targetRequest.startDate}
          onChange={(value) => onUpdateValue('startDate', value)}
          disabled={isReadOnly || !enabled}
          required
        />
      </Label>
    );
  }

  renderSubstituteLeaveDateField() {
    const { isReadOnly, targetRequest, onUpdateValue } = this.props;
    const { substituteDate, startDate } = targetRequest;

    return (
      <Label
        text={msg().Att_Lbl_ScheduledDateOfSubstitute}
        key="substituteDate"
      >
        <DateField
          className="ts-text-field slds-input"
          value={substituteDate}
          selected={startDate}
          onChange={(value) => onUpdateValue('substituteDate', value)}
          disabled={isReadOnly}
          required
        />
      </Label>
    );
  }

  renderSubstituteLeaveFields() {
    const {
      isReadOnly,
      targetRequest,
      substituteLeaveTypeList,
      onUpdateValue,
    } = this.props;
    const { substituteLeaveType } = targetRequest;

    // 「利用しない」しか選択肢がない場合は、関連フィールドを出力しない
    if (substituteLeaveTypeList.length === 1) {
      return null;
    }

    const fieldsDOMList = [];

    const leaveTypeOptions = ORDER_OF_SUBSTITUTE_LEAVE_TYPES.filter(
      (leaveType) => substituteLeaveTypeList.includes(leaveType)
    ).map((leaveType) => ({
      text: LEAVE_TYPE_LABEL_MAP[leaveType](),
      value: leaveType,
    }));

    fieldsDOMList.push(
      <Label text={msg().Att_Lbl_ReplacementDayOff} key="substituteLeaveType">
        <RadioGroupField
          layout={RADIO_GROUP_LAYOUT_TYPE.vertical}
          options={leaveTypeOptions}
          value={substituteLeaveType}
          onChange={(value) => onUpdateValue('substituteLeaveType', value)}
          showSelectedTextOnly={isReadOnly}
        />
      </Label>
    );

    switch (substituteLeaveType) {
      case SUBSTITUTE_LEAVE_TYPE.Substitute:
        fieldsDOMList.push(this.renderSubstituteLeaveDateField());
        break;

      default:
    }

    return fieldsDOMList;
  }

  render() {
    const { isReadOnly, targetRequest, onUpdateValue } = this.props;

    return (
      <div>
        {this.renderOriginalTargetDateField()}

        <Label text={msg().Att_Lbl_Duration}>
          <AttTimeRangeField
            startTime={TimeUtil.toHHmm(targetRequest.startTime)}
            endTime={TimeUtil.toHHmm(targetRequest.endTime)}
            onBlurAtStart={(value) =>
              onUpdateValue('startTime', TimeUtil.toMinutes(value))
            }
            onBlurAtEnd={(value) =>
              onUpdateValue('endTime', TimeUtil.toMinutes(value))
            }
            disabled={isReadOnly}
            required
          />
        </Label>

        {this.renderSubstituteLeaveFields()}

        <Label text={msg().Att_Lbl_Remarks}>
          <TextAreaField
            maxLength={255}
            value={targetRequest.remarks || ''}
            onChange={(e) => onUpdateValue('remarks', e.target.value)}
            disabled={isReadOnly}
          />
        </Label>
      </div>
    );
  }
}
