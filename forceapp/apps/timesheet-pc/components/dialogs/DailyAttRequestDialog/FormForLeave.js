// @flow
import React from 'react';

import msg from '../../../../commons/languages';
import Label from '../../../../commons/components/fields/Label';
import SelectField from '../../../../commons/components/fields/SelectField';
import RadioGroupField, {
  LAYOUT_TYPE as RADIO_GROUP_LAYOUT_TYPE,
} from '../../../../commons/components/fields/RadioGroupField';
import DateRangeField from '../../../../commons/components/fields/DateRangeField';
import DateField from '../../../../commons/components/fields/DateField';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';
import AttTimeRangeField from '../../../../commons/components/fields/AttTimeRangeField';

import { ORDER_OF_RANGE_TYPES } from '../../../../domain/models/attendance/LeaveRange';
import { type LeaveRequest } from '../../../../domain/models/attendance/AttDailyRequest/LeaveRequest';
import * as AttLeave from '../../../../domain/models/attendance/AttLeave';
import { isForReapply } from '../../../../domain/models/attendance/AttDailyRequest';

import DurationUtil from '../../../../commons/utils/DurationUtil';
import DateUtil from '../../../../commons/utils/DateUtil';
import TimeUtil from '../../../../commons/utils/TimeUtil';

import './FormForLeave.scss';

const ROOT = 'timesheet-pc-dialogs-daily-att-request-dialog-form-for-leave';

const RANGE_LABEL_MAP = {
  Day: () => msg().Att_Lbl_FullDayLeave,
  AM: () => msg().Att_Lbl_FirstHalfOfDayLeave,
  PM: () => msg().Att_Lbl_SecondHalfOfDayLeave,
  Half: () => msg().Att_Lbl_HalfDayLeave,
  Time: () => msg().Att_Lbl_HourlyLeave,
};

type Props = {|
  isReadOnly: boolean,
  hasRange: boolean,
  targetRequest: LeaveRequest,
  attLeaveList: AttLeave.AttLeave[],
  selectedAttLeave: AttLeave.AttLeave | null,
  onUpdateValue: ($Keys<LeaveRequest>, $Values<LeaveRequest>) => void,
  onUpdateHasRange: (boolean) => void,
|};

export default class FormForLeave extends React.Component<Props> {
  renderDaysLeftField() {
    const { selectedAttLeave } = this.props;

    if (!selectedAttLeave || !AttLeave.isDaysLeftManaged(selectedAttLeave)) {
      return null;
    }

    return (
      <Label text={msg().Att_Lbl_DaysLeft}>
        {DurationUtil.formatDaysAndHoursWithUnit(
          Number(selectedAttLeave.daysLeft),
          Number(selectedAttLeave.hoursLeft)
        )}
      </Label>
    );
  }

  // NOTE: 所定休憩時間の除外が実装されるまで機能提供しない
  // renderRoundedUpHoursToTake() {
  //   const MASK_STR = '* *';
  //
  //   const { targetRequest } = this.props;
  //   const { isDaysLeftManaged } = targetRequest;
  //
  //   if (!isDaysLeftManaged) {
  //     return null;
  //   }
  //
  //   const hoursToTake = targetRequest.roundedUpHoursToTake || MASK_STR;
  //
  //   return (
  //     <span className={`${ROOT}__hours-to-take`}>
  //       {`${msg().Att_Lbl_Take}: ${hoursToTake} ${msg().Com_Lbl_Hours}`}
  //     </span>
  //   );
  // }

  renderRoundingUpWarning() {
    const { selectedAttLeave } = this.props;

    if (!selectedAttLeave || !AttLeave.isDaysLeftManaged(selectedAttLeave)) {
      return null;
    }

    return (
      <p className={`${ROOT}__rounding-up-notice`}>
        {msg().Att_Msg_RoundingUpNotice}
      </p>
    );
  }

  renderStartEndField() {
    const {
      isReadOnly,
      hasRange,
      targetRequest,
      onUpdateValue,
      onUpdateHasRange,
    } = this.props;

    switch (targetRequest.leaveRange) {
      case 'Day':
        const minOfEndDate = DateUtil.addDays(targetRequest.startDate, 1);
        return (
          <Label text={msg().Att_Lbl_Period}>
            {hasRange ? (
              <DateRangeField
                startDateFieldProps={{
                  disabled: true,
                  value: targetRequest.startDate,
                  onChange: (value) => onUpdateValue('startDate', value),
                }}
                endDateFieldProps={{
                  disabled: isReadOnly,
                  value: targetRequest.endDate,
                  minDate: minOfEndDate,
                  onChange: (value) => onUpdateValue('endDate', value),
                }}
                required
              />
            ) : (
              <DateField
                value={targetRequest.startDate}
                selected={targetRequest.startDate}
                onChange={(value) => onUpdateValue('startDate', value)}
                disabled
                required
              />
            )}

            <div className="slds-form-element">
              <div className="slds-form-element__control">
                <div className="slds-checkbox">
                  <input
                    type="checkbox"
                    id="range"
                    checked={hasRange}
                    onChange={(e) => onUpdateHasRange(e.target.checked)}
                    disabled={isReadOnly}
                  />
                  <label className="slds-checkbox__label" htmlFor="range">
                    <span className="slds-checkbox_faux" />
                    <span className="slds-form-element__label">
                      {msg().Att_Lbl_UsePeriod}
                    </span>
                  </label>
                </div>
              </div>
            </div>
          </Label>
        );
      case 'Half':
        return null;
      case 'Time': {
        const aboutRoundingUp =
          targetRequest.leaveRange === 'Time'
            ? [
                // this.renderRoundedUpHoursToTake(),
                this.renderRoundingUpWarning(),
              ]
            : null;

        return (
          <Label text={msg().Att_Lbl_Duration}>
            <AttTimeRangeField
              startTime={TimeUtil.toHHmm(targetRequest.startTime)}
              endTime={TimeUtil.toHHmm(targetRequest.endTime)}
              onBlurAtStart={(value) =>
                onUpdateValue('startTime', TimeUtil.toMinutes(value))
              }
              onBlurAtEnd={(value) =>
                onUpdateValue('endTime', TimeUtil.toMinutes(value))
              }
              disabled={isReadOnly}
              required
            />
            {aboutRoundingUp}
          </Label>
        );
      }

      default:
        return null;
    }
  }

  render() {
    const {
      isReadOnly,
      targetRequest,
      attLeaveList,
      selectedAttLeave,
      onUpdateValue,
    } = this.props;

    const LeaveTypeOptions = attLeaveList.map((leaveType) => ({
      text: leaveType.name,
      value: leaveType.code,
    }));

    const rangeOptions = ORDER_OF_RANGE_TYPES.filter(
      (rangeType) =>
        // FIXME: 「selectedAttLeave &&」は既存編集のエラー回避で、本来は不要。勤務表データ取得APIでLeaveCodeを貰えれば解決するかも？？
        selectedAttLeave && selectedAttLeave.ranges.includes(rangeType)
      // selectedAttLeave.ranges.includes(rangeType)
    ).map((rangeKey) => ({
      text: RANGE_LABEL_MAP[rangeKey](),
      value: rangeKey,
    }));

    // 承認内容変更申請の場合には読み取り専用になるフィールドかどうか
    const isReadOnlyForReapply = isForReapply(targetRequest);

    return (
      <div>
        <Label text={msg().Att_Lbl_LeaveType}>
          <SelectField
            options={LeaveTypeOptions}
            value={targetRequest.leaveCode}
            onChange={(e) => onUpdateValue('leaveCode', e.target.value)}
            disabled={isReadOnly || isReadOnlyForReapply}
          />
        </Label>

        {this.renderDaysLeftField()}

        <Label text={msg().Att_Lbl_Range}>
          <RadioGroupField
            layout={RADIO_GROUP_LAYOUT_TYPE.vertical}
            options={rangeOptions}
            value={targetRequest.leaveRange}
            onChange={(value) => onUpdateValue('leaveRange', value)}
            showSelectedTextOnly={isReadOnly || isReadOnlyForReapply}
          />
        </Label>

        {this.renderStartEndField()}

        {selectedAttLeave && selectedAttLeave.requireReason ? (
          <Label text={msg().Att_Lbl_Reason}>
            <TextAreaField
              maxLength={255}
              value={targetRequest.reason || ''}
              onChange={(e) => onUpdateValue('reason', e.target.value)}
              disabled={isReadOnly}
              required
            />
          </Label>
        ) : (
          <Label text={msg().Att_Lbl_Remarks}>
            <TextAreaField
              maxLength={255}
              value={targetRequest.remarks || ''}
              onChange={(e) => onUpdateValue('remarks', e.target.value)}
              disabled={isReadOnly}
            />
          </Label>
        )}
      </div>
    );
  }
}
