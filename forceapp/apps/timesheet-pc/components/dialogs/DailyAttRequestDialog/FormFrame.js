// @flow

import * as React from 'react';

import msg from '../../../../commons/languages';
import Button from '../../../../commons/components/buttons/Button';
import Label from '../../../../commons/components/fields/Label';
import AccessControl from '../../../../commons/containers/AccessControlContainer';

import {
  type EditAction,
  type DisableAction,
  EDIT_ACTION,
  DISABLE_ACTION,
} from '../../../../domain/models/attendance/AttDailyRequest';

import { type ApproverEmployee } from '../../../../domain/models/approval/ApproverEmployee';

import './FormFrame.scss';

const ROOT = 'timesheet-pc-dialogs-daily-att-request-dialog-form-frame';

type Props = {|
  // State
  isEditing: boolean,
  editAction: EditAction,
  disableAction: DisableAction,
  isAvailableToModify: boolean,
  children: React.Node,
  approverEmployee: ApproverEmployee,

  // Actions
  onSubmit: Function,
  onDisable: Function,
  onStartEditing: Function,
  onCancelEditing: Function,
  onClickOpenApprovalHistoryDialog: Function,
  onClickOpenApproverEmployeeSettingDialog: Function,
|};

export default class FormFrame extends React.Component<Props> {
  onSubmit: (SyntheticEvent<HTMLButtonElement>) => void;

  constructor(props: Props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e: SyntheticEvent<HTMLButtonElement>) {
    e.preventDefault();
    if (e.currentTarget.form && e.currentTarget.form.reportValidity()) {
      this.props.onSubmit();
    }
  }

  renderApprovalHistoryButton() {
    return (
      <Button
        key="to-show-approval-history"
        type="text"
        onClick={this.props.onClickOpenApprovalHistoryDialog}
      >
        {msg().Com_Btn_ApprovalHistory}
      </Button>
    );
  }

  renderDisableButton() {
    const { isEditing, disableAction } = this.props;

    if (disableAction === DISABLE_ACTION.None) {
      return null;
    }

    const label = {
      [DISABLE_ACTION.CancelRequest]: msg().Att_Btn_CancelRequest,
      [DISABLE_ACTION.CancelApproval]: msg().Att_Btn_CancelApproval,
      [DISABLE_ACTION.Remove]: msg().Att_Btn_RemoveRequest,
    }[disableAction];

    const buttonType =
      disableAction === DISABLE_ACTION.Remove ? 'destructive' : 'default';

    const disabled = isEditing;

    const permissionTestConditions = {
      [DISABLE_ACTION.CancelRequest]: {
        allowIfByEmployee: true,
        requireIfByDelegate: ['cancelAttDailyRequestByDelegate'],
      },
      [DISABLE_ACTION.CancelApproval]: {
        requireIfByEmployee: ['cancelAttDailyApprovalByEmployee'],
        requireIfByDelegate: ['cancelAttDailyApprovalByDelegate'],
      },
      [DISABLE_ACTION.Remove]: {
        allowIfByEmployee: true,
        requireIfByDelegate: ['submitAttDailyRequestByDelegate'],
      },
    }[disableAction];

    return (
      <AccessControl
        conditions={permissionTestConditions}
        key="to-disable-request"
      >
        <Button
          type={buttonType}
          disabled={disabled}
          className={`${ROOT}__button`}
          onClick={this.props.onDisable}
        >
          {label}
        </Button>
      </AccessControl>
    );
  }

  renderStartEditingButton() {
    const { editAction, onStartEditing } = this.props;

    // 実行できる編集系操作が無い場合は、非表示
    if (editAction === EDIT_ACTION.None) {
      return null;
    }

    const label = {
      [EDIT_ACTION.Modify]: msg().Att_Btn_ModifyRequest,
      [EDIT_ACTION.Reapply]: msg().Att_Btn_ChangeRequest,
    }[editAction];

    return (
      <AccessControl
        allowIfByEmployee
        requireIfByDelegate={['submitAttDailyRequestByDelegate']}
        key="to-start-editing"
      >
        <Button className={`${ROOT}__button`} onClick={onStartEditing}>
          {label}
        </Button>
      </AccessControl>
    );
  }

  renderCancelEditingButton() {
    return (
      <Button
        key="to-cancel-editing"
        className={`${ROOT}__button`}
        onClick={this.props.onCancelEditing}
      >
        {msg().Com_Btn_Cancel}
      </Button>
    );
  }

  renderModifierButtons() {
    const { isEditing, editAction, isAvailableToModify } = this.props;

    // 新規申請の場合は、非表示
    if (editAction === EDIT_ACTION.Create) {
      return null;
    }

    // 「申請履歴」ボタンは全ての状態で表示する
    const buttons = [this.renderApprovalHistoryButton()];

    // 操作・操作開始トリガー系のボタンは、勤務確定後は表示しない
    if (isAvailableToModify) {
      buttons.push(this.renderDisableButton());

      // 実行できる編集系操作があれば、表示する
      if (editAction !== EDIT_ACTION.None) {
        // 表示モード／編集モードによる出し分け
        if (isEditing) {
          buttons.push(this.renderCancelEditingButton());
        } else {
          buttons.push(this.renderStartEditingButton());
        }
      }
    }

    return buttons;
  }

  renderSubmitButton() {
    const { isEditing, editAction } = this.props;

    // 表示モードの場合は、または実行できる編集系操作が無い場合は、非表示
    if (!isEditing || editAction === EDIT_ACTION.None) {
      return null;
    }

    const label = {
      [EDIT_ACTION.Create]: msg().Att_Btn_Request,
      [EDIT_ACTION.Modify]: msg().Att_Btn_RequestAgain,
      [EDIT_ACTION.Reapply]: msg().Att_Btn_Reapply,
    }[editAction];

    return (
      <Button
        type="primary"
        className={`${ROOT}__button`}
        onClick={(e) => this.onSubmit(e)}
      >
        {label}
      </Button>
    );
  }

  render() {
    return (
      <div className={ROOT}>
        <form action="/#">
          <div className={`${ROOT}__content`}>
            {this.props.children}
            <Label text={msg().Att_Lbl_NextApproverEmployee}>
              <div className={`${ROOT}__approver-employee`}>
                <div className={`${ROOT}__approver-employee-name`}>
                  {(this.props.approverEmployee &&
                    this.props.approverEmployee.employeeName) ||
                    msg().Com_Lbl_Unspecified}
                </div>
                <Button
                  onClick={this.props.onClickOpenApproverEmployeeSettingDialog}
                >
                  {msg().Att_Btn_DisplayApprovalSteps}
                </Button>
              </div>
            </Label>
          </div>
          <div className={`${ROOT}__operators`}>
            <div className={`${ROOT}__modify`}>
              {this.renderModifierButtons()}
            </div>

            <div className={`${ROOT}__submit`}>{this.renderSubmitButton()}</div>
          </div>
        </form>
      </div>
    );
  }
}
