// @flow

import * as React from 'react';

import type { AbsenceRequest } from '../../../../domain/models/attendance/AttDailyRequest/AbsenceRequest';

import DateUtil from '../../../../commons/utils/DateUtil';
import msg from '../../../../commons/languages';

import DateField from '../../../../commons/components/fields/DateField';
import DateRangeField from '../../../../commons/components/fields/DateRangeField';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';
import Label from '../../../../commons/components/fields/Label';

import './FormForAbsence.scss';

const ROOT = 'timesheet-pc-dialogs-daily-att-request-dialog-form-for-absence';

export type Props = {
  // state
  isReadOnly: boolean,
  targetRequest: AbsenceRequest,
  hasRange: boolean,

  // actions
  onUpdateHasRange: (boolean) => void,
  onUpdateValue: ($Keys<AbsenceRequest>, $Values<AbsenceRequest>) => void,
};

export default (props: Props) => {
  const {
    isReadOnly,
    targetRequest,
    hasRange,
    onUpdateValue,
    onUpdateHasRange,
  } = props;
  const minOfEndDate = DateUtil.addDays(targetRequest.startDate, 1);
  return (
    <div className={ROOT}>
      <Label text={msg().Att_Lbl_Period}>
        {hasRange ? (
          <DateRangeField
            startDateFieldProps={{
              disabled: true,
              value: targetRequest.startDate,
              onChange: (value) => onUpdateValue('startDate', value),
            }}
            endDateFieldProps={{
              disabled: isReadOnly,
              value: targetRequest.endDate,
              minDate: minOfEndDate,
              onChange: (value) => onUpdateValue('endDate', value),
            }}
            required
          />
        ) : (
          <DateField
            value={targetRequest.startDate}
            selected={targetRequest.startDate}
            onChange={(value) => onUpdateValue('startDate', value)}
            disabled
            required
          />
        )}

        <div className="slds-form-element">
          <div className="slds-form-element__control">
            <div className="slds-checkbox">
              <input
                type="checkbox"
                id="range"
                checked={hasRange}
                onChange={(e) => onUpdateHasRange(e.target.checked)}
                disabled={isReadOnly}
              />
              <label className="slds-checkbox__label" htmlFor="range">
                <span className="slds-checkbox_faux" />
                <span className="slds-form-element__label">
                  {msg().Att_Lbl_UsePeriod}
                </span>
              </label>
            </div>
          </div>
        </div>
      </Label>

      <Label text={msg().Att_Lbl_Reason}>
        <TextAreaField
          maxLength={255}
          value={targetRequest.reason || ''}
          onChange={(e) => onUpdateValue('reason', e.target.value)}
          disabled={isReadOnly}
          required
        />
      </Label>
    </div>
  );
};
