// @flow

import * as React from 'react';

import type { AttPattern } from '../../../../domain/models/attendance/AttPattern';
import type { PatternRequest } from '../../../../domain/models/attendance/AttDailyRequest/PatternRequest';

import DateUtil from '../../../../commons/utils/DateUtil';
import TimeUtil from '../../../../commons/utils/TimeUtil';
import msg from '../../../../commons/languages';

import DateField from '../../../../commons/components/fields/DateField';
import DateRangeField from '../../../../commons/components/fields/DateRangeField';
import AttTimeRangeField from '../../../../commons/components/fields/AttTimeRangeField';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';
import Label from '../../../../commons/components/fields/Label';
import SelectField from '../../../../commons/components/fields/SelectField';

import './FormForPattern.scss';

const ROOT = 'timesheet-pc-dialogs-daily-att-request-dialog-form-for-pattern';

export type Props = {
  // state
  isReadOnly: boolean,
  targetRequest: PatternRequest,
  attPatternList: AttPattern[],
  selectedAttPattern: AttPattern | null,
  hasRange: boolean,

  // actions
  onUpdateHasRange: (boolean) => void,
  onUpdateValue: ($Keys<PatternRequest>, string | number) => void,
};

export default (props: Props) => {
  const {
    isReadOnly,
    targetRequest,
    attPatternList,
    hasRange,
    onUpdateHasRange,
    onUpdateValue,
  } = props;
  const selectedAttPattern: AttPattern = props.selectedAttPattern || {
    name: '',
    code: '',
    startTime: null,
    endTime: null,
    restTimes: [],
  };
  const minOfEndDate = DateUtil.addDays(targetRequest.startDate, 1);

  return (
    <div className={ROOT}>
      <Label text={msg().Att_Lbl_Period}>
        {hasRange ? (
          <DateRangeField
            startDateFieldProps={{
              disabled: true,
              value: targetRequest.startDate,
              onChange: (value) => onUpdateValue('startDate', value),
            }}
            endDateFieldProps={{
              disabled: isReadOnly,
              value: targetRequest.endDate,
              minDate: minOfEndDate,
              onChange: (value) => onUpdateValue('endDate', value),
            }}
            required
          />
        ) : (
          <DateField
            value={targetRequest.startDate}
            selected={targetRequest.startDate}
            onChange={(value) => onUpdateValue('startDate', value)}
            disabled
            required
          />
        )}

        <div className="slds-form-element">
          <div className="slds-form-element__control">
            <div className="slds-checkbox">
              <input
                type="checkbox"
                id="range"
                checked={hasRange}
                onChange={(e) => onUpdateHasRange(e.target.checked)}
                disabled={isReadOnly}
              />
              <label className="slds-checkbox__label" htmlFor="range">
                <span className="slds-checkbox_faux" />
                <span className="slds-form-element__label">
                  {msg().Att_Lbl_UsePeriod}
                </span>
              </label>
            </div>
          </div>
        </div>
      </Label>

      <Label text={msg().Att_Lbl_AttPattern}>
        <SelectField
          options={attPatternList.map(({ name, code }) => ({
            text: name,
            value: code,
          }))}
          value={targetRequest.patternCode}
          onChange={(e) => onUpdateValue('patternCode', e.target.value)}
          disabled={isReadOnly}
        />
      </Label>

      <Label text={msg().Admin_Lbl_WorkingHours}>
        <AttTimeRangeField
          className={`${ROOT}__date-range`}
          startTime={TimeUtil.toHHmm(selectedAttPattern.startTime)}
          endTime={TimeUtil.toHHmm(selectedAttPattern.endTime)}
          onBlurAtStart={() => {}}
          onBlurAtEnd={() => {}}
          disabled
        />
      </Label>

      {selectedAttPattern.restTimes.map((restTime, idx) => (
        <Label key={idx} text={`${msg().Admin_Lbl_WorkingTypeRest}${idx + 1}`}>
          <AttTimeRangeField
            className={`${ROOT}__date-range`}
            startTime={TimeUtil.toHHmm(restTime.startTime)}
            endTime={TimeUtil.toHHmm(restTime.endTime)}
            onBlurAtStart={() => {}}
            onBlurAtEnd={() => {}}
            disabled
          />
        </Label>
      ))}

      <Label text={msg().Att_Lbl_Remarks}>
        <TextAreaField
          maxLength={255}
          value={targetRequest.remarks || ''}
          onChange={(e) => onUpdateValue('remarks', e.target.value)}
          disabled={isReadOnly}
        />
      </Label>
    </div>
  );
};
