// @flow
import * as React from 'react';

import msg from '../../../commons/languages';

import DailyAttTime from '../../models/DailyAttTime';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import DialogListLayout from '../../../commons/components/dialogs/layouts/DialogListLayout';
import Label from '../../../commons/components/fields/Label';
import Button from '../../../commons/components/buttons/Button';
import IconButton from '../../../commons/components/buttons/IconButton';
import TextField from '../../../commons/components/fields/TextField';
import AttTimeField from '../../../commons/components/fields/AttTimeField';
import AttTimeRangeField from '../../../commons/components/fields/AttTimeRangeField';
import DateUtil from '../../../commons/utils/DateUtil';
import { parseIntOrStringNull } from '../../../commons/utils/NumberUtil';

import ImgBtnPlusField from '../../images/btnPlusField.png';
import ImgBtnMinusField from '../../images/btnMinusField.png';

import './DailyAttTimeDialog.scss';

const ROOT = 'timesheet-pc-dialogs-daily-att-time-dialog';

const FieldItem = (props: {| label: string, children: React.Node |}) => {
  const { label, children } = props;

  return (
    <DialogListLayout.Item>
      <Label labelCols={4} childCols={8} text={label}>
        {children}
      </Label>
    </DialogListLayout.Item>
  );
};

type Duration = {|
  start: string,
  end: string,
|};

type Props = {|
  isReadOnly: boolean,
  onUpdateClockTime: (string, *) => void,
  onUpdateRestTime: (number, string, *) => void,
  onAddRestTime: () => void,
  onDeleteRestTime: (number) => void,
  onSubmit: () => void,
  onCancel: () => void,
  dailyAttTime: ?DailyAttTime,
|};

export default class DailyAttTimeDialog extends React.Component<Props> {
  onSubmit: () => void;
  renderRestItem: (Duration, number, *[]) => React.Node;
  renderOtherRestTimeInput: () => React.Node;

  static defaultProps = {
    dailyAttTime: null,
  };

  constructor(props: Props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.renderRestItem = this.renderRestItem.bind(this);
    this.renderOtherRestTimeInput = this.renderOtherRestTimeInput.bind(this);
  }

  onSubmit(e: SyntheticEvent<HTMLButtonElement>) {
    e.preventDefault();
    this.props.onSubmit();
  }

  renderHeaderSub() {
    const { dailyAttTime } = this.props;

    if (!dailyAttTime) {
      return null;
    }

    const displayYMDd = [
      DateUtil.formatYMD(dailyAttTime.recordDate),
      `(${DateUtil.formatWeekday(dailyAttTime.recordDate)})`,
    ].join(' ');

    return (
      <time
        className={`${ROOT}__date`}
        dateTime={new Date(dailyAttTime.recordDate).toISOString()}
      >
        {displayYMDd}
      </time>
    );
  }

  renderFooter() {
    const { isReadOnly } = this.props;

    // TODO: ロック時（リード・オンリー）の場合に、「キャンセル」ボタンのラベル文言を変更する必要があるか検討する
    const buttons = [
      <Button type="default" onClick={this.props.onCancel} key="button-hide">
        {msg().Com_Btn_Cancel}
      </Button>,
    ];

    if (!isReadOnly) {
      buttons.push(
        <Button submit type="primary" key="button-exec">
          {msg().Com_Btn_Submit}
        </Button>
      );
    }

    return <DialogFrame.Footer>{buttons}</DialogFrame.Footer>;
  }

  renderRestItem(
    restItem: Duration,
    index: number,
    restItemList: *[]
  ): React.Node {
    const {
      isReadOnly,
      onUpdateRestTime,
      onAddRestTime,
      onDeleteRestTime,
    } = this.props;

    const plusButton =
      index === restItemList.length - 1 &&
      index + 1 < DailyAttTime.MAX_STANDARD_REST_TIME_COUNT &&
      !isReadOnly ? (
        <IconButton
          className={`${ROOT}__date-range-button`}
          src={ImgBtnPlusField}
          alt={msg().Att_Btn_AddItem}
          onClick={() => onAddRestTime()}
          key="icon-add"
        />
      ) : null;

    const minusButton =
      restItemList.length > 1 && !isReadOnly ? (
        <IconButton
          className={`${ROOT}__date-range-button ${ROOT}__date-range-button--minus`}
          src={ImgBtnMinusField}
          alt={msg().Att_Btn_RemoveItem}
          onClick={() => onDeleteRestTime(index)}
          key="icon-remove"
        />
      ) : null;

    return (
      <FieldItem
        label={`${msg().Att_Lbl_Rest} ${index + 1}`}
        key={`rest-${index}`}
      >
        <AttTimeRangeField
          className={`${ROOT}__date-range`}
          startTime={restItem.start}
          endTime={restItem.end}
          onBlurAtStart={(value) => onUpdateRestTime(index, 'start', value)}
          onBlurAtEnd={(value) => onUpdateRestTime(index, 'end', value)}
          disabled={isReadOnly}
        />
        {[plusButton, minusButton]}
      </FieldItem>
    );
  }

  renderOtherRestTimeInput() {
    const { isReadOnly, dailyAttTime, onUpdateClockTime } = this.props;

    if (!dailyAttTime || !dailyAttTime.hasRestTime) {
      return null;
    }

    return (
      <FieldItem label={`${msg().Att_Lbl_OtherRestTime}`} key="other-rest">
        <TextField
          className={`${ROOT}__other-rest-time`}
          type="number"
          min={0}
          max={2880}
          step={1}
          value={parseIntOrStringNull(dailyAttTime.restHours)}
          onChange={(e) => {
            onUpdateClockTime(
              'restHours',
              parseIntOrStringNull(e.target.value)
            );
          }}
          disabled={isReadOnly}
        />{' '}
        {msg().Com_Lbl_Mins}
      </FieldItem>
    );
  }

  render() {
    const { dailyAttTime, isReadOnly, onUpdateClockTime } = this.props;

    if (!dailyAttTime) {
      return null;
    }

    // TODO: ロック時（リード・オンリー）の場合に、ダイアログのタイトルを変更する必要があるか検討する
    return (
      <form onSubmit={this.onSubmit} action="/#">
        <DialogFrame
          title={msg().Att_Lbl_WorkTimeInput}
          className={ROOT}
          headerSub={this.renderHeaderSub()}
          footer={this.renderFooter()}
          hide={this.props.onCancel}
        >
          <div className={`${ROOT}__list`}>
            <DialogListLayout>
              <FieldItem label={msg().Att_Lbl_TimeIn} key="clock-in">
                <AttTimeField
                  value={dailyAttTime.startTime}
                  onBlur={(value) => onUpdateClockTime('startTime', value)}
                  disabled={isReadOnly}
                />
              </FieldItem>

              <FieldItem label={msg().Att_Lbl_TimeOut} key="clock-out">
                <AttTimeField
                  value={dailyAttTime.endTime}
                  onBlur={(value) => onUpdateClockTime('endTime', value)}
                  disabled={isReadOnly}
                />
              </FieldItem>

              {dailyAttTime.restTimes.map(this.renderRestItem)}

              {this.renderOtherRestTimeInput()}
            </DialogListLayout>
          </div>
        </DialogFrame>
      </form>
    );
  }
}
