import React from 'react';
import PropTypes from 'prop-types';

import msg from '../../../commons/languages';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import Button from '../../../commons/components/buttons/Button';
import Label from '../../../commons/components/fields/Label';
import CommentNarrowField from '../../../commons/components/fields/CommentNarrowField';
import AttSummaryModel from '../../models/AttSummary';
import AttFixSummaryRequestModel from '../../models/AttFixSummaryRequest';

import './FixRequestDialog.scss';

const ROOT = 'timesheet-pc-fix-request-dialog';

export default class FixRequestDialog extends React.Component {
  static propTypes = {
    userPhotoUrl: PropTypes.string.isRequired,
    onUpdateValue: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    fixSummaryRequest: PropTypes.instanceOf(AttFixSummaryRequestModel),
  };

  static defaultProps = {
    fixSummaryRequest: null,
  };

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit();
  }

  render() {
    const {
      fixSummaryRequest,
      userPhotoUrl,
      onUpdateValue,
      onCancel,
    } = this.props;

    if (!fixSummaryRequest) {
      return null;
    }

    let titleLabel;
    let submitLabel;

    switch (fixSummaryRequest.performableActionForFix) {
      case AttSummaryModel.ACTIONS_FOR_FIX.Submit:
        titleLabel = msg().Att_Lbl_RequestForApproval;
        submitLabel = msg().Com_Btn_Request;
        break;

      case AttSummaryModel.ACTIONS_FOR_FIX.CancelRequest:
        titleLabel = msg().Att_Lbl_ReqStatRemoved;
        submitLabel = msg().Com_Btn_RequestCancel;
        break;

      case AttSummaryModel.ACTIONS_FOR_FIX.CancelApproval:
        titleLabel = msg().Att_Lbl_ReqStatCanceled;
        submitLabel = msg().Com_Btn_ApprovalCancel;
        break;

      default:
    }

    return (
      <div className={`${ROOT}`}>
        <form onSubmit={this.onSubmit} action="/#">
          <DialogFrame
            className={`${ROOT}__dialog-frame`}
            title={titleLabel}
            hide={this.props.onCancel}
            footer={
              <DialogFrame.Footer
                sub={
                  <Label text={msg().Att_Lbl_NextApproverEmployee}>
                    <div className={`${ROOT}__approver-employee`}>
                      <div className={`${ROOT}__approver-employee-name`}>
                        {(this.props.approverEmployee &&
                          this.props.approverEmployee.employeeName) ||
                          msg().Com_Lbl_Unspecified}
                      </div>
                      <Button
                        onClick={
                          this.props.onClickOpenApproverEmployeeSettingDialog
                        }
                      >
                        {msg().Att_Btn_DisplayApprovalSteps}
                      </Button>
                    </div>
                  </Label>
                }
              >
                <Button type="default" onClick={onCancel}>
                  {msg().Com_Btn_Cancel}
                </Button>
                <Button type="primary" submit>
                  {submitLabel}
                </Button>
              </DialogFrame.Footer>
            }
          >
            <div className={`${ROOT}__inner`}>
              <CommentNarrowField
                onChange={(value) => onUpdateValue('comment', value)}
                value={fixSummaryRequest.comment}
                maxLength={1000}
                icon={userPhotoUrl}
              />
            </div>
          </DialogFrame>
        </form>
      </div>
    );
  }
}
