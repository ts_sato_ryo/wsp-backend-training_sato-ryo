// @flow
import React from 'react';
import classNames from 'classnames';

import { type TimeRange } from '../../../domain/models/attendance/TimeRange';
import { type Period } from '../../../domain/models/attendance/Timesheet';
import { type State as DailyTimeTrackState } from '../../modules/entities/dailyTimeTrack';
import { type UserSetting } from '../../../domain/models/UserSetting';

import AttRecordModel from '../../models/AttRecord';
import AttSummaryModel from '../../models/AttSummary';
import DailyActualWorkingTimePeriodModel from '../../models/DailyActualWorkingTimePeriod';
import DailyContractedDetailModel from '../../models/DailyContractedDetail';
import DailyRequestConditionsModel from '../../models/DailyRequestConditions';
import Employee from './Employee';
import EmployeeModel from '../../models/Employee';
import GlobalHeader from '../../../commons/components/GlobalHeader';

import ImgIconHeader from '../../images/Time&Attendance.svg';
import ImgPartsIconPunch from '../../images/partsIconPunch.png';
import Request from './Request';
import StampWidgetContainer from '../../../commons/containers/widgets/StampWidgetContainer';
import SummaryPeriodNav from './SummaryPeriodNav';
import Timesheet from './Timesheet';
import Utilities from './Utilities';
import msg from '../../../commons/languages';

import './index.scss';

const ROOT = 'timesheet-pc-main-content';

type Props = {
  attRecordList: AttRecordModel[],
  attSummary: AttSummaryModel,
  dailyActualWorkingPeriodListMap: DailyActualWorkingTimePeriodModel[],
  dailyAttentionMessagesMap: Object,
  dailyContractedDetailMap: DailyContractedDetailModel,
  dailyRequestConditionsMap: DailyRequestConditionsModel,
  dailyRequestedWorkingHoursMap: TimeRange,
  dailyTimeTrackMap: DailyTimeTrackState,
  employee: EmployeeModel,
  isManHoursGraphOpened: boolean,
  isProxyMode: boolean,
  standalone: boolean,
  isStampWidgetOpened: boolean,
  onClickFixSummaryRequestButton: Function,
  onClickOpenAttRequestHistoryButton: Function,
  onClickOpenDailySummaryButton: (targetDate: string) => void,
  onClickStampWidgetToggle: Function,
  onClickTimesheetAttentionsButton: (Array<string>) => void,
  onClickTimesheetRemarksButton: (
    AttRecordModel,
    DailyRequestConditionsModel
  ) => void,
  onClickTimesheetRequestButton: (DailyRequestConditionsModel) => void,
  onClickTimesheetTimeButton: (AttRecordModel) => void,
  onExitProxyMode: Function,
  onPeriodSelected: Function,
  onRequestOpenLeaveWindow: Function,
  onRequestOpenSummaryWindow: Function,
  onStampSuccess: Function, // FIXME: These Function types should be more detailed
  selectedPeriodStartDate: string,
  summaryPeriodList: Period[],
  userSetting: UserSetting,
};

export default class MainContent extends React.Component<Props> {
  renderHeaderContent() {
    return (
      <div>
        {/* $FlowFixMe v0.89 */}
        <SummaryPeriodNav
          summaryPeriodList={this.props.summaryPeriodList}
          selectedPeriodStartDate={this.props.selectedPeriodStartDate}
          onPeriodSelected={this.props.onPeriodSelected}
        />
        {!this.props.isProxyMode && !this.props.standalone ? (
          <button
            type="button"
            className={classNames({
              'slds-button': true,
              'slds-button--neutral': true,
              [`${ROOT}__toggle-stamp-widget`]: true,
            })}
            onClick={this.props.onClickStampWidgetToggle}
          >
            <img src={ImgPartsIconPunch} alt={msg().Att_Lbl_InputAttendance} />
          </button>
        ) : null}
      </div>
    );
  }

  renderGlobalHeader() {
    return (
      <GlobalHeader
        iconSrc={ImgIconHeader}
        iconSrcType="svg"
        iconAssistiveText={msg().Att_Lbl_TimeAttendance}
        content={this.renderHeaderContent()}
        showPersonalMenuPopoverButton={!this.props.isProxyMode}
        showProxyIndicator={this.props.isProxyMode}
        onClickProxyExitButton={this.props.onExitProxyMode}
      />
    );
  }

  renderMainContent() {
    if (!this.props.attRecordList || !this.props.selectedPeriodStartDate) {
      return null;
    }

    return (
      <main className={`${ROOT}__main`}>
        <header>
          <div className={`${ROOT}__header-operation`}>
            <h1 className={`${ROOT}__header-heading`}>
              {msg().Att_Lbl_TimeAttendance}
            </h1>

            <Employee employee={this.props.employee} />

            <div className={`${ROOT}__header-tools`}>
              <Utilities
                onClickOpenSummaryWindowButton={
                  this.props.onRequestOpenSummaryWindow
                }
                onClickOpenLeaveWindowButton={
                  this.props.onRequestOpenLeaveWindow
                }
              />
              <Request
                attSummary={this.props.attSummary}
                isProxyMode={this.props.isProxyMode}
                onClickRequestButton={this.props.onClickFixSummaryRequestButton}
                onClickOpenRequestHistoryButton={
                  this.props.onClickOpenAttRequestHistoryButton
                }
              />
            </div>
          </div>
        </header>
        {/* $FlowFixMe v0.89 */}
        <Timesheet
          attRecordList={this.props.attRecordList}
          dailyContractedDetailMap={this.props.dailyContractedDetailMap}
          dailyRequestedWorkingHoursMap={
            this.props.dailyRequestedWorkingHoursMap
          }
          dailyActualWorkingPeriodListMap={
            this.props.dailyActualWorkingPeriodListMap
          }
          dailyRequestConditionsMap={this.props.dailyRequestConditionsMap}
          dailyAttentionMessagesMap={this.props.dailyAttentionMessagesMap}
          dailyTimeTrackMap={this.props.dailyTimeTrackMap}
          isManHoursGraphOpened={this.props.isManHoursGraphOpened}
          onClickRequestButton={this.props.onClickTimesheetRequestButton}
          onClickTimeButton={this.props.onClickTimesheetTimeButton}
          onClickRemarksButton={this.props.onClickTimesheetRemarksButton}
          onClickAttentionsButton={this.props.onClickTimesheetAttentionsButton}
          onClickDailySummaryButton={this.props.onClickOpenDailySummaryButton}
          userSetting={this.props.userSetting}
        />
      </main>
    );
  }

  render() {
    const { isStampWidgetOpened, onStampSuccess } = this.props;

    const className = classNames(ROOT, {
      [`${ROOT}--stamp-widget-opened`]: isStampWidgetOpened,
    });

    return (
      <div className={className}>
        {this.renderGlobalHeader()}
        {/*
         * NOTE: 打刻ウィジェットの初期化・同期は、勤務表の更新アクションの内部で実行するため
         * onDidMountを無効化している
         */}
        {!this.props.isProxyMode && !this.props.standalone ? (
          <StampWidgetContainer
            className={`${ROOT}__stamp-widget`}
            withGlobalLoading
            onDidMount={null}
            onStampSuccess={onStampSuccess}
          />
        ) : null}

        {this.renderMainContent()}
      </div>
    );
  }
}
