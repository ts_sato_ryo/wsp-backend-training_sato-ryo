import React from 'react';

import './AttendanceSummary.scss';

const ROOT = 'timesheet-pc-attendance-summary';

export default class AttendanceSummary extends React.Component {
  render() {
    return (
      <table className={ROOT}>
        <tbody>
          <tr>
            <th>所定出勤日数</th>
            <td>20</td>
            <th>所定出勤時間</th>
            <td>07:31</td>
            <th>法定時間内残業</th>
            <td>07:31</td>
            <th>法定休日労働</th>
            <td>07:31</td>
          </tr>
          <tr>
            <th>実出動日数</th>
            <td>5</td>
            <th>総労働時間</th>
            <td>07:31</td>
            <th>法定時間外残業</th>
            <td>07:31</td>
          </tr>
        </tbody>
      </table>
    );
  }
}
