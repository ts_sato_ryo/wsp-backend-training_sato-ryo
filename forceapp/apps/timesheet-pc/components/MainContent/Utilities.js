import React from 'react';
import PropTypes from 'prop-types';

import './Utilities.scss';
import Button from '../../../commons/components/buttons/Button';
import msg from '../../../commons/languages';

const ROOT = 'timesheet-pc-utilities';

// FIXME: 適した名前を検討して適用する
export default class Utilities extends React.Component {
  static propTypes = {
    onClickOpenSummaryWindowButton: PropTypes.func.isRequired,
    onClickOpenLeaveWindowButton: PropTypes.func.isRequired,
  };

  render() {
    return (
      <ul className={ROOT}>
        <li className={`${ROOT}__item`}>
          <Button
            type="text"
            onClick={this.props.onClickOpenSummaryWindowButton}
          >
            {msg().Att_Lbl_MonthlySummary}
          </Button>
        </li>
        <li className={`${ROOT}__item`}>
          <Button type="text" onClick={this.props.onClickOpenLeaveWindowButton}>
            {msg().Att_Lbl_LeaveDetails}
          </Button>
        </li>
      </ul>
    );
  }
}
