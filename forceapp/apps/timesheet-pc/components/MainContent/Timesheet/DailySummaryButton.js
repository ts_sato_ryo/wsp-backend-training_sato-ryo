// @flow

import * as React from 'react';
// import classNames from 'classnames';
// import isNil from 'lodash/isNil';
import imgBtnPlus from '../../../../commons/images/btnPlus.png';
// import TimeUtil from '../../../../commons/utils/TimeUtil';
import Tooltip from '../../../../commons/components/Tooltip';
import msg from '../../../../commons/languages';

import './DailySummaryButton.scss';

const ROOT = 'timesheet-pc-main-content-timesheet-daily-summary-button';

type Props = $ReadOnly<{|
  workTime: ?number,
  totalTaskTime: ?number,
  onClick: () => void,
|}>;

const HintTooltip = (props: { hasAlert: boolean, children: React.Node }) => {
  return props.hasAlert ? (
    <Tooltip content={msg().Att_TimeTracking_Hint} align="top left">
      <div className={`${ROOT}__tooltip-container`}>{props.children}</div>
    </Tooltip>
  ) : (
    props.children
  );
};

const DailySummaryButton = (props: Props): React.Element<'div'> => {
  /* GENIE-11780
   * Do no show the alert of mismatched time track and work hours.
   * This is a temporary workaround.
   * We found that showing alert is not useful for the part of early access users.
   * In the future, we will implement the alert about time track
   * in the way our customers really want.

  const hasLeft = !isNil(props.workTime) && isNil(props.totalTaskTime);

  const invalidTotalTaskTime =
    !isNil(props.workTime) &&
    !isNil(props.totalTaskTime) &&
    props.workTime !== props.totalTaskTime;

  const invalidWorkTime = isNil(props.workTime) && !isNil(props.totalTaskTime);

  const hasAlert = invalidTotalTaskTime || invalidWorkTime;
  */

  return (
    <div className={ROOT} onClick={props.onClick}>
      <HintTooltip
        hasAlert={
          /* GENIE-11780
           * Do no show the alert of mismatched time track and work hours.
           * This is a temporary workaround.
           * We found that showing alert is not useful for the part of early access users.
           * In the future, we will implement the alert about time track
           * in the way our customers really want.

          hasAlert
           */
          false
        }
      >
        <div className={`${ROOT}__mark`}>
          {/* GENIE-11780
           * Do no show the alert of mismatched time track and work hours.
           * This is a temporary workaround.
           * We found that showing alert is not useful for the part of early access users.
           * In the future, we will implement the alert about time track
           * in the way our customers really want.
          <div
            className={classNames({
              [`${ROOT}__alert`]: hasAlert || hasLeft,
              [`${ROOT}__ok`]: !hasAlert && !hasLeft,
            })}
          />
          */}
        </div>

        <div className={`${ROOT}__content`}>
          {/* GENIE-11780
           * Do no show the alert of mismatched time track and work hours.
           * This is a temporary workaround.
           * We found that showing alert is not useful for the part of early access users.
           * In the future, we will implement the alert about time track
           * in the way our customers really want.
      {!isNil(props.totalTaskTime) ? (
        <div
          className={classNames(`${ROOT}__total-task-time`, {
            [`${ROOT}__alert`]: hasAlert,
            [`${ROOT}__ok`]: !hasAlert,
          })}
        >
          {TimeUtil.toHHmm(props.totalTaskTime)}
        </div>
      ) : ( */}
          <button className={`${ROOT}__button`}>
            <img className={`${ROOT}__icon`} src={imgBtnPlus} />
          </button>
          {/* )} */}
        </div>
      </HintTooltip>
    </div>
  );
};

DailySummaryButton.displayName = 'DailySummaryButton';

export default DailySummaryButton;
