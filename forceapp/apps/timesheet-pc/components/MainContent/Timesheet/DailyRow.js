/* @flow */
import React from 'react';
import classNames from 'classnames';

import AccessControl from '../../../../commons/containers/AccessControlContainer';
import Tooltip from '../../../../commons/components/Tooltip';
import AttRecordModel from '../../../models/AttRecord';
import DailyContractedDetailModel from '../../../models/DailyContractedDetail';
import { type TimeRange } from '../../../../domain/models/attendance/TimeRange';
import DailyActualWorkingTimePeriodModel from '../../../models/DailyActualWorkingTimePeriod';
import DailyRequestConditionsModel from '../../../models/DailyRequestConditions';
import type { UserSetting } from '../../../../domain/models/UserSetting';

import Button from '../../../../commons/components/buttons/Button';
import msg from '../../../../commons/languages';
import RequestButtonWithStatus from './RequestButtonWithStatus';
import DailySummaryButton from './DailySummaryButton';
import AttChart from './AttChart';

import './DailyRow.scss';
import iconStatusAttention from '../../../../commons/images/iconAttention_small.png';

const ROOT = 'timesheet-pc-main-content-timesheet-daily-row';

type Props = {
  attRecord: AttRecordModel,
  requestConditions: DailyRequestConditionsModel,
  dailyTimeTrack?: {|
    workTime: ?number,
    totalTaskTime: ?number,
  |},
  onClickRequestButton: (DailyRequestConditionsModel) => void,
  onClickTimeButton: (AttRecordModel) => void,
  onClickRemarksButton: (AttRecordModel, DailyRequestConditionsModel) => void,
  onClickAttentionsButton: (Array<string>) => void,
  onClickDailySummaryButton: (targetDate: string) => void,
  onDragChartStart: () => void,
  contractedDetail?: DailyContractedDetailModel,
  requestedWorkingHours?: TimeRange,
  actualWorkingPeriodList?: Array<DailyActualWorkingTimePeriodModel>,
  isManHoursGraphOpened?: boolean,
  chartPositionLeft?: number,
  attentionMessages?: Array<string>,
  userSetting: UserSetting,
};

export default class DailyRow extends React.Component<Props> {
  renderStatusIcon: () => string;

  static defaultProps = {
    contractedDetail: null,
    requestedWorkingHours: null,
    actualWorkingPeriodList: null,
    isManHoursGraphOpened: false,
    chartPositionLeft: 0,
    dailyTimeTrack: {
      workTime: null,
      totalTaskTime: null,
    },
  };

  constructor(props: Props) {
    super(props);
    this.renderStatusIcon = this.renderStatusIcon.bind(this);
  }

  renderStatusIcon() {
    const {
      attentionMessages,
      onClickAttentionsButton,
      attRecord,
    } = this.props;

    if (!attentionMessages || !attentionMessages.length) {
      return '';
    }

    const tipMsg =
      attentionMessages.length === 1
        ? attentionMessages[0]
        : msg().Att_Msg_MultipulAttentionMessage;

    return (
      <div>
        <Tooltip
          id={`${attRecord.recordDate}-attentions`}
          content={tipMsg}
          align="right"
        >
          <div
            className={`${ROOT}__attentions-button`}
            data-for={`${attRecord.recordDate}-attentions`}
            aria-label={tipMsg}
            onClick={() => onClickAttentionsButton(attentionMessages)}
          >
            <img src={iconStatusAttention} alt={tipMsg} />
          </div>
        </Tooltip>
      </div>
    );
  }

  render() {
    const {
      attRecord,
      contractedDetail,
      requestedWorkingHours,
      actualWorkingPeriodList,
      requestConditions,
      isManHoursGraphOpened,
      chartPositionLeft,
      onClickRequestButton,
      onClickTimeButton,
      onClickRemarksButton,
      onDragChartStart,
    } = this.props;

    const { effectualAllDayLeaveType } = requestConditions || {};

    const className = classNames(ROOT, {
      [`${ROOT}--man-hours-graph-opened`]: isManHoursGraphOpened,

      [`${ROOT}--day-type-workday`]:
        attRecord.dayType === AttRecordModel.DAY_TYPE.WORKDAY,
      [`${ROOT}--day-type-holiday`]:
        attRecord.dayType === AttRecordModel.DAY_TYPE.HOLIDAY,
      [`${ROOT}--day-type-legal-holiday`]:
        attRecord.dayType === AttRecordModel.DAY_TYPE.LEGAL_HOLIDAY,

      [`${ROOT}--leave-type-${(
        effectualAllDayLeaveType || ''
      ).toLowerCase()}`]: effectualAllDayLeaveType,

      [`${ROOT}--leave-of-absence`]: attRecord.isLeaveOfAbsence,

      [`${ROOT}--absence`]: requestConditions.isApprovedAbsence,
    });

    return (
      <tr className={className}>
        <td className={`${ROOT}__col-status`}>{this.renderStatusIcon()}</td>
        <td className={`${ROOT}__col-application`}>
          <RequestButtonWithStatus
            requestConditions={requestConditions}
            onClick={onClickRequestButton}
          />
        </td>
        <td className={`${ROOT}__col-date`}>
          <em>{attRecord.displayDate}</em>
          {attRecord.displayDay}
        </td>
        <td className={`${ROOT}__col-start-time`}>
          {requestConditions.isAvailableToOperateAttTime ? (
            <Button
              type="default"
              className={`${ROOT}__input-time`}
              onClick={() => onClickTimeButton(attRecord)}
            >
              {attRecord.displayStartTime}
            </Button>
          ) : (
            attRecord.displayStartTime
          )}
        </td>
        <td className={`${ROOT}__col-end-time`}>
          {requestConditions.isAvailableToOperateAttTime ? (
            <Button
              type="default"
              className={`${ROOT}__input-time`}
              onClick={() => onClickTimeButton(attRecord)}
            >
              {attRecord.displayEndTime}
            </Button>
          ) : (
            attRecord.displayEndTime
          )}
        </td>
        <AccessControl
          allowIfByEmployee
          requireIfByDelegate={['viewTimeTrackByDelegate']}
        >
          {this.props.userSetting.useWorkTime && (
            <td className={`${ROOT}__col-daily-summary-container`}>
              <div className={`${ROOT}__col-daily-summary`}>
                <DailySummaryButton
                  {...this.props.dailyTimeTrack}
                  onClick={() =>
                    this.props.onClickDailySummaryButton(
                      this.props.requestConditions.recordDate
                    )
                  }
                />
              </div>
            </td>
          )}
        </AccessControl>
        <td className={`${ROOT}__col-chart`}>
          <div
            className={`${ROOT}__chart-container`}
            onMouseDown={onDragChartStart}
            style={{ left: chartPositionLeft }}
          >
            {/* $FlowFixMe v0.89 */}
            <AttChart
              dayType={attRecord.dayType}
              startTime={attRecord.startTime}
              endTime={attRecord.endTime}
              scheduledWorkingHours={contractedDetail || requestedWorkingHours}
              actualWorkingPeriodList={actualWorkingPeriodList}
              effectualLeaveRequestList={
                requestConditions.effectualLeaveRequests || []
              }
              isLeaveOfAbsence={attRecord.isLeaveOfAbsence}
              isApprovedAbsence={requestConditions.isApprovedAbsence}
            />
          </div>
        </td>
        <td className={`${ROOT}__col-remarks`}>
          <button
            className={`${ROOT}__input-remarks`}
            onClick={() => onClickRemarksButton(attRecord, requestConditions)}
          >
            {attRecord.remarks || ''}
          </button>
        </td>
      </tr>
    );
  }
}
