// @flow

import React from 'react';

import msg from '../../../commons/languages';
import AccessControl from '../../../commons/containers/AccessControlContainer';
import Button from '../../../commons/components/buttons/Button';
import AttSummaryModel from '../../models/AttSummary';

import './Request.scss';

const ROOT = 'timesheet-pc-request';

type Props = {
  attSummary: ?AttSummaryModel,
  onClickRequestButton: () => void,
  onClickOpenRequestHistoryButton: () => void,
};

// FIXME: 適した名前を検討して適用する
export default class Request extends React.Component<Props> {
  renderStatusValue() {
    const { STATUS } = AttSummaryModel;
    const { attSummary } = this.props;
    const { status } = attSummary || {};

    const label =
      {
        [STATUS.NotRequested]: msg().Att_Lbl_ReqStatNotRequested,
        [STATUS.Pending]: msg().Att_Lbl_ReqStatPending,
        [STATUS.Approved]: msg().Att_Lbl_ReqStatApproved,
        [STATUS.Rejected]: msg().Att_Lbl_ReqStatRejected,
        [STATUS.Removed]: msg().Att_Lbl_ReqStatRemoved,
        [STATUS.Canceled]: msg().Att_Lbl_ReqStatCanceled,
      }[status] || '\u00A0';

    return (
      <span className={`${ROOT}__status__value`}>
        {!status || status === AttSummaryModel.STATUS.NotRequested ? (
          label
        ) : (
          <Button
            type="text"
            onClick={this.props.onClickOpenRequestHistoryButton}
          >
            {label}
          </Button>
        )}
      </span>
    );
  }

  renderRequestButton() {
    const { ACTIONS_FOR_FIX } = AttSummaryModel;
    const { attSummary } = this.props;
    const { performableActionForFix } = attSummary || {};

    if (!performableActionForFix) {
      return null;
    }

    const label = {
      [ACTIONS_FOR_FIX.Submit]: msg().Com_Btn_Request,
      [ACTIONS_FOR_FIX.CancelRequest]: msg().Com_Btn_RequestCancel,
      [ACTIONS_FOR_FIX.CancelApproval]: msg().Com_Btn_ApprovalCancel,
      [ACTIONS_FOR_FIX.None]: msg().Com_Btn_Request,
    }[performableActionForFix];

    const actionType = [ACTIONS_FOR_FIX.Submit, ACTIONS_FOR_FIX.None].includes(
      performableActionForFix
    )
      ? 'primary'
      : 'destructive';

    const permissionTestConditions = {
      [ACTIONS_FOR_FIX.Submit]: {
        allowIfByEmployee: true,
        requireIfByDelegate: ['submitAttRequestByDelegate'],
      },
      [ACTIONS_FOR_FIX.CancelRequest]: {
        allowIfByEmployee: true,
        requireIfByDelegate: ['cancelAttRequestByDelegate'],
      },
      [ACTIONS_FOR_FIX.CancelApproval]: {
        requireIfByEmployee: ['cancelAttApprovalByEmployee'],
        requireIfByDelegate: ['cancelAttApprovalByDelegate'],
      },
      [ACTIONS_FOR_FIX.None]: {
        allowIfByEmployee: true,
      },
    }[performableActionForFix];

    return (
      <AccessControl conditions={permissionTestConditions}>
        <Button
          type={actionType}
          onClick={this.props.onClickRequestButton}
          disabled={performableActionForFix === ACTIONS_FOR_FIX.None}
        >
          {label}
        </Button>
      </AccessControl>
    );
  }

  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__status`}>
          <span className={`${ROOT}__status__title`}>
            {msg().Att_Lbl_Status}
          </span>

          {this.renderStatusValue()}
        </div>

        <div className={`${ROOT}__action`}>{this.renderRequestButton()}</div>
      </div>
    );
  }
}
