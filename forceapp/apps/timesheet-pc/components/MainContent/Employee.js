import React from 'react';
import PropTypes from 'prop-types';

import msg from '../../../commons/languages';
import EmployeeModel from '../../models/Employee';
import './Employee.scss';

const ROOT = 'timesheet-pc-employee';

export default class Employee extends React.Component {
  static propTypes = {
    employee: PropTypes.instanceOf(EmployeeModel).isRequired,
  };

  render() {
    const { employee } = this.props;

    const departmentNameAndSeparator = employee.departmentName
      ? [
          <strong key="1" className={`${ROOT}__department-name`}>
            {employee.departmentName}
          </strong>,
          '/',
        ]
      : null;

    return (
      <div className={ROOT}>
        {departmentNameAndSeparator}
        <strong className={`${ROOT}__employee-name`}>
          {employee.employeeName}
        </strong>

        <dl className={`${ROOT}__working-type-name`}>
          <dt>{msg().Att_Lbl_WorkingType}</dt>
          <dd>
            <strong>{employee.workingTypeName}</strong>
          </dd>
        </dl>
      </div>
    );
  }
}
