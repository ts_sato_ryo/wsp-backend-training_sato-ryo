// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../commons/utils/TypeUtil';

import common from '../../commons/reducers';
import entities from './entities';
import client from './client';
import ui from './ui';
import widgets from './widgets';

const rootReducer = {
  common,
  entities,
  client,
  ui,
  widgets,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
