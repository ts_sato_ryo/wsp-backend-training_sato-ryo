import { combineReducers } from 'redux';

import PersonalMenuPopover from '../../commons/modules/widgets/PersonalMenuPopover';
import ProxyEmployeeSelectDialog from '../../../widgets/dialogs/ProxyEmployeeSelectDialog/modules';
import ApprovalHistoryDialog from '../../../widgets/dialogs/ApprovalHistoryDialog/modules';

export default combineReducers({
  PersonalMenuPopover,
  ApprovalHistoryDialog,
  ProxyEmployeeSelectDialog,
});
