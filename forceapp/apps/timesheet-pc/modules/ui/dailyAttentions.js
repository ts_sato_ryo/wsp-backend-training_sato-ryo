/* @flow */

type State = {
  messages: Array<string>,
};

type Action = {
  type: string,
  payload?: $PropertyType<State, 'messages'>,
};

const ACTIONS = {
  SET: 'TIMESHEET-PC/UI/DAILY_WARNING_TIME/SET',
  UNSET: 'TIMESHEET-PC/UI/DAILY_WARNING_TIME/UNSET',
};

export const actions = {
  set: (messages: $PropertyType<State, 'messages'>) => ({
    type: ACTIONS.SET,
    payload: messages,
  }),
  unset: () => ({
    type: ACTIONS.UNSET,
  }),
};

const initialState: State = {
  messages: [],
};

export default function reducer(state: State = initialState, action: Action) {
  switch (action.type) {
    case ACTIONS.SET:
      return {
        messages: action.payload,
      };
    case ACTIONS.UNSET:
      return {
        messages: [],
      };
    default:
      return state;
  }
}
