/** @type {DailyAttTime|null} */
const initialState = null;

const ACTIONS = {
  SET: 'TIMESHEET-PC/UI/EDITING_DAILY_ATT_TIME/SET',
  UNSET: 'TIMESHEET-PC/UI/EDITING_DAILY_ATT_TIME/UNSET',
  UPDATE: 'TIMESHEET-PC/UI/EDITING_DAILY_ATT_TIME/UPDATE',
  UPDATE_REST_TIME: 'TIMESHEET-PC/UI/EDITING_DAILY_ATT_TIME/UPDATE_REST_TIME',
  ADD_REST_TIME: 'TIMESHEET-PC/UI/EDITING_DAILY_ATT_TIME/ADD_REST_TIME',
  DELETE_REST_TIME: 'TIMESHEET-PC/UI/EDITING_DAILY_ATT_TIME/DELETE_REST_TIME',
};

export const actions = {
  set: (dailyAttTime) => ({
    type: ACTIONS.SET,
    payload: dailyAttTime,
  }),

  unset: () => ({
    type: ACTIONS.UNSET,
  }),

  update: (key, value) => ({
    type: ACTIONS.UPDATE,
    payload: { key, value },
  }),

  updateRestTime: (index, key, value) => ({
    type: ACTIONS.UPDATE_REST_TIME,
    payload: { index, key, value },
  }),

  addRestTime: () => ({
    type: ACTIONS.ADD_REST_TIME,
  }),

  deleteRestTime: (index) => ({
    type: ACTIONS.DELETE_REST_TIME,
    payload: index,
  }),
};

/**
 * @param {DailyAttTime} state
 * @param {Object} action
 * @returns {DailyAttTime}
 */
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;

    case ACTIONS.UNSET:
      return null;

    case ACTIONS.UPDATE:
      return state.update(action.payload.key, action.payload.value);

    case ACTIONS.UPDATE_REST_TIME: {
      const restTimes = state.restTimes.map((restTime, index) => {
        return index === action.payload.index
          ? { ...restTime, [action.payload.key]: action.payload.value }
          : restTime;
      });
      return state.update('restTimes', restTimes);
    }

    case ACTIONS.ADD_REST_TIME: {
      const restTimes = [...state.restTimes, { start: '', end: '' }];
      return state.update('restTimes', restTimes);
    }

    case ACTIONS.DELETE_REST_TIME: {
      const restTimes = state.restTimes.filter(
        (item, index) => index !== action.payload
      );
      return state.update('restTimes', restTimes);
    }

    default:
      return state;
  }
}
