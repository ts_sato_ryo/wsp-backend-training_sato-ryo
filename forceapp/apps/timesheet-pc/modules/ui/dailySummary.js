// @flow

// State

import type { DailySummary } from '../../../domain/models/time-management/DailySummary';
import { calculateEachRatioAndTaskTimeOfNonDirectInputTasks } from '../../../domain/models/time-management/DailySummary';
import type { Job } from '../../../domain/models/time-tracking/Job';
import {
  type DailySummaryTask,
  create as createDailySummaryTask,
} from '../../../domain/models/time-management/DailySummaryTask';
import {
  type WorkCategory,
  createWorkCategoryMap,
} from '../../../domain/models/time-tracking/WorkCategory';

export type State = {|
  isOpened: boolean,
  targetDate: string,
  note: string,
  taskList: DailySummaryTask[],
  timestampComment: string,
  isEnableEndStamp: boolean,
  realWorkTime: number,
  isTemporaryWorkTime: boolean,
  defaultJobId: string,
  isJobSelectDialogOpened: boolean,
  workCategoryMap: { [jobId: string]: WorkCategory[] },
|};

const initialState: State = {
  isOpened: false,
  targetDate: '',
  note: '',
  timestampComment: '',
  isEnableEndStamp: false,
  taskList: [],
  realWorkTime: 0,
  isTemporaryWorkTime: false,
  defaultJobId: '',
  isJobSelectDialogOpened: false,
  workCategoryMap: {},
};

// Actions

type AddJobToTaskList = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/ADD_JOB_TO_TASK_LIST',
  payload: {|
    job: Job,
    workCategories: WorkCategory[],
  |},
|};

type Open = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/OPEN',
  payload: {|
    targetDate: string,
  |},
|};

type Close = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/CLOSE',
|};

type DeleteTask = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/DELETE_TASK',
  payload: {|
    index: number,
  |},
|};

type OpenJobSelectDialog = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/OPEN_JOB_SELECT_DIALOG',
|};

type CloseJobSelectDialog = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/CLOSE_JOB_SELECT_DIALOG',
|};

type Update = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/UPDATE',
  payload: {|
    key: $Keys<State>,
    value: $Values<State>,
  |},
|};

type UpdateTask = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/UPDATE_TASK',
  payload: {|
    index: number,
    prop: string,
    value: mixed,
  |},
|};

type ToggleDirectInput = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/TOGGLE_DIRECT_INPUT',
  payload: {|
    index: number,
  |},
|};

type FetchSuccess = {|
  type: 'TIMESHEET-PC/UI/DAILY_SUMMARY/FETCH_SUCCESS',
  payload: {|
    dailySummary: DailySummary,
  |},
|};

const ADD_JOB_TO_TASK_LIST =
  'TIMESHEET-PC/UI/DAILY_SUMMARY/ADD_JOB_TO_TASK_LIST';
const OPEN = 'TIMESHEET-PC/UI/DAILY_SUMMARY/OPEN';
const CLOSE = 'TIMESHEET-PC/UI/DAILY_SUMMARY/CLOSE';
const DELETE_TASK = 'TIMESHEET-PC/UI/DAILY_SUMMARY/DELETE_TASK';
const OPEN_JOB_SELECT_DIALOG =
  'TIMESHEET-PC/UI/DAILY_SUMMARY/OPEN_JOB_SELECT_DIALOG';
const CLOSE_JOB_SELECT_DIALOG =
  'TIMESHEET-PC/UI/DAILY_SUMMARY/CLOSE_JOB_SELECT_DIALOG';
const UPDATE = 'TIMESHEET-PC/UI/DAILY_SUMMARY/UPDATE';
const UPDATE_TASK = 'TIMESHEET-PC/UI/DAILY_SUMMARY/UPDATE_TASK';
const TOGGLE_DIRECT_INPUT = 'TIMESHEET-PC/UI/DAILY_SUMMARY/TOGGLE_DIRECT_INPUT';
const FETCH_SUCCESS = 'TIMESHEET-PC/UI/DAILY_SUMMARY/FETCH_SUCCESS';

type Action =
  | AddJobToTaskList
  | Open
  | Close
  | DeleteTask
  | OpenJobSelectDialog
  | CloseJobSelectDialog
  | Update
  | UpdateTask
  | ToggleDirectInput
  | FetchSuccess;

export const actions = {
  addJobToTaskList: (
    job: Job,
    workCategories: WorkCategory[]
  ): AddJobToTaskList => ({
    type: ADD_JOB_TO_TASK_LIST,
    payload: {
      job,
      workCategories,
    },
  }),
  open: (targetDate: string): Open => ({
    type: OPEN,
    payload: {
      targetDate,
    },
  }),
  close: (): Close => ({
    type: CLOSE,
  }),
  deleteTask: (index: number): DeleteTask => ({
    type: DELETE_TASK,
    payload: {
      index,
    },
  }),
  openJobSelectDialog: (): OpenJobSelectDialog => ({
    type: OPEN_JOB_SELECT_DIALOG,
  }),
  closeJobSelectDialog: (): CloseJobSelectDialog => ({
    type: CLOSE_JOB_SELECT_DIALOG,
  }),
  update: (key: $Keys<State>, value: $Values<State>): Update => ({
    type: UPDATE,
    payload: {
      key,
      value,
    },
  }),
  updateTask: (index: number, prop: string, value: mixed): UpdateTask => ({
    type: UPDATE_TASK,
    payload: {
      index,
      prop,
      value,
    },
  }),
  toggleDirectInput: (index: number): ToggleDirectInput => ({
    type: TOGGLE_DIRECT_INPUT,
    payload: { index },
  }),
  fetchSuccess: (dailySummary: DailySummary): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: {
      dailySummary,
    },
  }),
};

// Reducer

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case ADD_JOB_TO_TASK_LIST: {
      const task = createDailySummaryTask(
        state.taskList.length,
        action.payload.job
      );
      const taskList = [...state.taskList, task];
      return {
        ...state,
        taskList,
        workCategoryMap: {
          ...state.workCategoryMap,
          [action.payload.job.id]: action.payload.workCategories,
        },
      };
    }

    case CLOSE: {
      return { ...state, isOpened: false, targetDate: '' };
    }

    case OPEN: {
      const { targetDate } = (action: Open).payload;
      return { ...state, isOpened: true, targetDate };
    }

    case DELETE_TASK: {
      const { index } = (action: DeleteTask).payload;
      return {
        ...state,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          [
            ...state.taskList.slice(0, index),
            ...state.taskList.slice(index + 1),
          ]
        ),
      };
    }

    case OPEN_JOB_SELECT_DIALOG: {
      return {
        ...state,
        isJobSelectDialogOpened: true,
      };
    }

    case CLOSE_JOB_SELECT_DIALOG: {
      return {
        ...state,
        isJobSelectDialogOpened: false,
      };
    }

    case UPDATE: {
      const { key, value } = (action: Update).payload;
      return { ...state, [key]: value };
    }

    case UPDATE_TASK: {
      const { index, prop, value } = (action: UpdateTask).payload;
      const taskList = [...state.taskList];
      taskList[index][prop] = value;
      return {
        ...state,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          taskList
        ),
      };
    }

    case TOGGLE_DIRECT_INPUT: {
      const { index } = (action: ToggleDirectInput).payload;

      const task = state.taskList[index];
      task.isDirectInput = !task.isDirectInput;

      if (task.isDirectInput) {
        task.taskTime = task.volume;
        task.volume = null;
        task.ratio = null;
      } else {
        task.volume = task.taskTime;
        task.taskTime = null;
      }

      return {
        ...state,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          [
            ...state.taskList.slice(0, index),
            task,
            ...state.taskList.slice(index + 1),
          ]
        ),
      };
    }

    case FETCH_SUCCESS: {
      const { dailySummary } = (action: FetchSuccess).payload;
      return {
        ...state,
        note: dailySummary.note,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          dailySummary.realWorkTime,
          dailySummary.taskList
        ),
        realWorkTime: dailySummary.realWorkTime,
        isTemporaryWorkTime: dailySummary.isTemporaryWorkTime,
        workCategoryMap: createWorkCategoryMap(dailySummary.taskList),
      };
    }

    default:
      return state;
  }
};
