import Api from '../../../../commons/api';
import { loadingStart, loadingEnd, catchApiError } from '../../../../commons/actions/app';
import { actions as historyListActions } from '../../../../../widgets/dialogs/ApprovalHistoryDialog/modules/entities/historyList';

import * as constants from './constants';

const open = (requestId) => (dispatch) => {
  dispatch({ type: constants.OPEN });

  const req = {
    path: '/approval/request/history/get',
    param: {
      requestId,
    },
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then((res) => {
      dispatch(historyListActions.set(res.historyList));
    })
    .catch((err) => dispatch(catchApiError(err)))
    .then(() => {
      dispatch(loadingEnd());
    });
};

const close = () => (dispatch) => {
  dispatch(historyListActions.unset());
  dispatch({ type: constants.CLOSE });
};

export { open, close };
