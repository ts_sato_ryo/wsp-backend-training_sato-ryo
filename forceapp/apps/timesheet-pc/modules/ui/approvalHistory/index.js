import * as constants from './constants';
import * as actions from './actions';

const initialState = {
  isOpen: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.OPEN:
      return Object.assign({},
        state,
        { isOpen: true },
      );
    case constants.CLOSE:
      return Object.assign({},
        state,
        { isOpen: false },
      );
    default:
      return state;
  }
};

export {
  constants,
  actions,
};

