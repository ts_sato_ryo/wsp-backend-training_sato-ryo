/** @type {DailyRemarks|null} */
const initialState = null;

const ACTIONS = {
  SET: 'TIMESHEET-PC/UI/EDITING_DAILY_REMARKS/SET',
  UNSET: 'TIMESHEET-PC/UI/EDITING_DAILY_REMARKS/UNSET',
  UPDATE: 'TIMESHEET-PC/UI/EDITING_DAILY_REMARKS/UPDATE',
};

export const actions = {
  set: (dailyRemarks) => ({
    type: ACTIONS.SET,
    payload: dailyRemarks,
  }),

  unset: () => ({
    type: ACTIONS.UNSET,
  }),

  update: (key, value) => ({
    type: ACTIONS.UPDATE,
    payload: { key, value },
  }),
};

/**
 * @param {DailyRemarks} state
 * @param {Object} action
 * @returns {DailyRemarks}
 */
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;

    case ACTIONS.UNSET:
      return null;

    case ACTIONS.UPDATE:
      return state.update(action.payload.key, action.payload.value);

    default:
      return state;
  }
}
