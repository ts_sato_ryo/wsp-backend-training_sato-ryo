/** @type {AttFixSummaryRequest|null} */
const initialState = null;

const ACTIONS = {
  SET: 'TIMESHEET-PC/UI/EDITING_FIX_SUMMARY_REQUEST/SET',
  UNSET: 'TIMESHEET-PC/UI/EDITING_FIX_SUMMARY_REQUEST/UNSET',
  UPDATE: 'TIMESHEET-PC/UI/EDITING_FIX_SUMMARY_REQUEST/UPDATE',
};

export const actions = {
  set: (attFixRequest) => ({
    type: ACTIONS.SET,
    payload: attFixRequest,
  }),

  unset: () => ({
    type: ACTIONS.UNSET,
  }),

  update: (key, value) => ({
    type: ACTIONS.UPDATE,
    payload: { key, value },
  }),
};

/**
 * @param {AttFixSummaryRequest} state
 * @param {Object} action
 * @returns {AttFixSummaryRequest}
 */
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;

    case ACTIONS.UNSET:
      return null;

    case ACTIONS.UPDATE:
      return state.update(action.payload.key, action.payload.value);

    default:
      return state;
  }
}
