const initialState = {
  /** @type {Boolean} */
  isOpened: true, // NOTE: 展開されている状態がデフォルト
};

const ACTIONS = {
  TOGGLE: 'TIMESHEET-PC/STAMP_WIDGET/TOGGLE',
};

export const actions = {
  toggle: () => ({ type: ACTIONS.TOGGLE }),
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.TOGGLE:
      return {
        ...state,
        isOpened: !state.isOpened,
      };

    default:
      return state;
  }
}
