// @flow
import { combineReducers } from 'redux';

import targetDate from './targetDate';
import editing from './editing';
import requests from './requests';

import { type $State } from '../../../../commons/utils/TypeUtil';

const reducer = {
  targetDate,
  editing,
  requests,
};

export type State = $State<typeof reducer>;

export default combineReducers<Object, Object>(reducer);
