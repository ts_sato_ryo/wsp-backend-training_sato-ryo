// @flow

import { createSelector } from 'reselect';

import { CODE } from '../../../../domain/models/attendance/AttDailyRequestType';

import { type State } from './index';

// $FlowFixMe
export const targetState = createSelector(
  (s: State) => s.editing.requestTypeCode,
  (s: State) => s.requests,
  (requestTypeCode, requests) => {
    switch (requestTypeCode) {
      case CODE.Absence:
        return requests.absenceRequest;
      case CODE.Direct:
        return requests.directRequest;
      case CODE.EarlyStartWork:
        return requests.earlyStartWorkRequest;
      case CODE.HolidayWork:
        return requests.holidayWorkRequest;
      case CODE.Leave:
        return requests.leaveRequest;
      case CODE.OvertimeWork:
        return requests.overtimeWorkRequest;
      case CODE.Pattern:
        return requests.patternRequest;
      default:
        return null;
    }
  }
);

// $FlowFixMe
export const targetRequest = createSelector(
  targetState,
  (state) => {
    return state ? state.request : null;
  }
);
