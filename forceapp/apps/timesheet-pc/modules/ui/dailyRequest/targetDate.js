/** @type {String|null} */
const initialState = null;

export const ACTIONS = {
  SET: 'TIMESHEET-PC/UI/DAILY_REQUEST/TARGET_DATE/SET',
  UNSET: 'TIMESHEET-PC/UI/DAILY_REQUEST/TARGET_DATE/UNSET',
};

export const actions = {
  set: (conditions) => ({
    type: ACTIONS.SET,
    payload: conditions,
  }),

  unset: () => ({
    type: ACTIONS.UNSET,
  }),
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;

    case ACTIONS.UNSET:
      return null;

    default:
      return state;
  }
};
