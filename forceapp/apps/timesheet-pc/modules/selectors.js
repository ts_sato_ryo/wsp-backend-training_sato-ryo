// @flow

import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';

import {
  isPermissionSatisfied,
  type Permission,
} from '../../domain/models/access-control/Permission';
import type { AttDailyRequestTypeMap } from '../../domain/models/attendance/AttDailyRequestType';
import type { AttSummary } from '../../domain/models/team/AttSummary';
import DailyActualWorkingTimePeriod from '../models/DailyActualWorkingTimePeriod';
import DailyContractedDetail from '../models/DailyContractedDetail';
import DailyRequestConditions from '../models/DailyRequestConditions';
import * as AttDailyRequest from '../../domain/models/attendance/AttDailyRequest';
import * as AttDailyRequestType from '../../domain/models/attendance/AttDailyRequestType';
import * as DailyAttentionMessages from '../models/DailyAttentionMessages';
import * as TimeRange from '../../domain/models/attendance/TimeRange';

import { type State } from './index';
import { type State as TimesheetState } from './entities/timesheet';
import AttRecord from '../models/AttRecord';

const hasPermissionForEdit = ({
  isByDelegate,
  userPermission,
}: {
  isByDelegate: boolean,
  userPermission: Permission,
}): boolean =>
  isPermissionSatisfied({
    isByDelegate,
    userPermission,
    allowIfByEmployee: true,
    requireIfByDelegate: ['editAttTimeSheetByDelegate'],
  });

const selectIsSummaryLocked = (state: State): boolean =>
  !isNil(state.entities.timesheet.attSummary) &&
  state.entities.timesheet.attSummary.isLocked;

const selectIsByDelegate = (state: State): boolean =>
  state.common.proxyEmployeeInfo.isProxyMode;

const selectUserPermission = (state: State): Permission =>
  state.common.accessControl.permission;

export const isTimesheetReadOnly: (State) => boolean = createSelector(
  selectIsSummaryLocked,
  selectIsByDelegate,
  selectUserPermission,
  (isSummaryLocked, isByDelegate, userPermission) =>
    isSummaryLocked || !hasPermissionForEdit({ isByDelegate, userPermission })
);

const selectAttSummary = (state: State): null | AttSummary =>
  (state.entities.timesheet: TimesheetState).attSummary;
const selectAttRecordList = (state: State) =>
  (state.entities.timesheet: TimesheetState).attRecordList;
const selectAttDailyRequestMap = (state: State) =>
  (state.entities.timesheet: TimesheetState).attDailyRequestMap;
const selectAttDailyRequestTypeMap = (state: State) =>
  (state.entities.timesheet: TimesheetState).attDailyRequestTypeMap;

// 実労働時間をモデルのインスタンスとして構築して、日付をキーにしたマップとして返却する
// TODO: 処理タイミングを再考したい。
// - store格納時：state上では正規化して配置されるべき？
// - AttRecordのコンストラクタ：データの更新と必ず同期するが、無駄な処理が増える懸念がある
// - AttRecordのcreateFromParamメソッド：前項に比べて処理負荷は軽減されるが、同期漏れが発生しうる
export const buildDailyActualWorkingPeriodListMap: (State) => {
  [string]: DailyActualWorkingTimePeriod,
} = createSelector(
  // $FlowFixMe v0.85
  selectAttRecordList,
  (attRecordList: AttRecord[]) => {
    if (!attRecordList) {
      return {};
    }

    return attRecordList.reduce((hash, attRecord) => {
      hash[
        attRecord.recordDate
      ] = DailyActualWorkingTimePeriod.buildDailyActualWorkingPeriodListFromAttRecord(
        attRecord
      );
      return hash;
    }, {});
  }
);

// 所定勤務時間をモデルのインスタンスとして構築して、日付をキーにしたマップとして返却する
// TODO: 処理タイミングを再考したい。※詳細は同上
export const buildDailyContractedDetailMap: (State) => {
  [string]: DailyContractedDetail,
} = createSelector(
  // $FlowFixMe v0.85
  selectAttRecordList,
  (attRecordList: AttRecord[]) => {
    if (!attRecordList) {
      return {};
    }

    return attRecordList.reduce((hash, attRecord) => {
      hash[attRecord.recordDate] = attRecord.contractedDetail
        ? DailyContractedDetail.createFromParam(attRecord.contractedDetail)
        : null;
      return hash;
    }, {});
  }
);

// 予定されている勤務時刻をモデルのインスタンスとして構築して、日付をキーにしたマップとして返却する
// TODO: 処理タイミングを再考したい。※詳細は同上
export const buildDailyRequestedWorkingHoursMap: (State) => {
  [string]: TimeRange.TimeRange,
} = createSelector(
  selectAttDailyRequestMap,
  (attDailyRequestMap: Object) => {
    const dailyRequestedWorkingHoursMap = Object.keys(attDailyRequestMap || {})
      .map((requestId) => attDailyRequestMap[requestId])
      .filter((request) =>
        AttDailyRequest.isEffectual(
          request,
          AttDailyRequestType.CODE.HolidayWork
        )
      )
      .reduce((hash, request) => {
        hash[request.startDate] = TimeRange.create(request);
        return hash;
      }, {});

    return dailyRequestedWorkingHoursMap;
  }
);

// 日ごとの申請の状態をモデルのインスタンスとして構築して、日付をキーにしたマップとして返却する
export const buildDailyRequestConditionMap: (State) => {
  [string]: DailyRequestConditions,
  // $FlowFixMe
} = createSelector(
  selectAttSummary,
  selectAttRecordList,
  selectAttDailyRequestMap,
  selectAttDailyRequestTypeMap,
  selectIsByDelegate,
  selectUserPermission,
  (
    attSummary: AttSummary,
    attRecordList: AttRecord[],
    attDailyRequestMap: Object,
    attDailyRequestTypeMap: AttDailyRequestTypeMap,
    isByDelegate: boolean,
    userPermission: Permission
  ) => {
    if (!attRecordList) {
      return {};
    }

    return attRecordList.reduce((hash, attRecord) => {
      hash[attRecord.recordDate] = DailyRequestConditions.createFromParams(
        attRecord,
        attDailyRequestMap,
        attDailyRequestTypeMap,
        {
          isSummaryLocked: isNil(attSummary) ? false : attSummary.isLocked,
          isByDelegate,
          userPermission,
        }
      );
      return hash;
    }, {});
  }
);

// 日ごとの値を確認して、必要なメッセージの配列を返す。
export const buildDailyAttentionMessagesMap: (State) => {
  [string]: DailyAttentionMessages.DailyAttentionMessages,
} = createSelector(
  // $FlowFixMe v0.85
  selectAttRecordList,
  (attRecordList: AttRecord[]) => {
    if (!attRecordList) {
      return {};
    }
    return attRecordList.reduce((hash, attRecord) => {
      hash[attRecord.recordDate] = DailyAttentionMessages.create(attRecord);
      return hash;
    }, {});
  }
);

const selectTargetDate = (state: State) => state.ui.dailyRequest.targetDate;

export const selectSelectedRequestConditions: (State) => DailyRequestConditions = createSelector(
  buildDailyRequestConditionMap,
  selectTargetDate,
  (
    dailyRequestConditionsMap: { [string]: DailyRequestConditions },
    targetDate: string
  ) => dailyRequestConditionsMap[targetDate]
);
