// @flow

import moment from 'moment';

import type { Event } from '../../../domain/models/time-management/Event';
import type { BaseEvent } from '../../../commons/models/DailySummary/BaseEvent';
import { toViewModel } from '../../../commons/models/DailySummary/Converter';

// State

type State = BaseEvent[];

const initialState: State = [];

// Actions

type FetchSuccess = {
  type: 'TIMESHEET-PC/ENTITIES/EVENT/FETCH_SUCCESS',
  payload: {
    events: Event[],
    targetDate: string,
  },
};

const FETCH_SUCCESS = 'TIMESHEET-PC/ENTITIES/EVENT/FETCH_SUCCESS';

type Action = FetchSuccess;

export const actions = {
  fetchSuccess: (events: Event[], targetDate: string): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: { events, targetDate },
  }),
};

// Reducer

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case FETCH_SUCCESS: {
      const { events, targetDate } = (action: FetchSuccess).payload;
      return toViewModel(events, moment(targetDate));
    }

    default:
      return state;
  }
};
