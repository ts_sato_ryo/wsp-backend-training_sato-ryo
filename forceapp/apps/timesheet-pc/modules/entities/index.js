// @flow

import { combineReducers } from 'redux';

import events from './events';
import dailyTimeTrack from './dailyTimeTrack';
import timesheet from './timesheet';
import jobs from './jobs';
import workCategories from './workCategories';

import { type $State } from '../../../commons/utils/TypeUtil';

const rootReducer = {
  events,
  dailyTimeTrack,
  timesheet,
  jobs,
  workCategories,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
