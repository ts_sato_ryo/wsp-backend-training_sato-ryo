// @flow

import { type Period } from '../../../domain/models/attendance/Timesheet';
import * as WorkingType from '../../../domain/models/attendance/WorkingType';
import * as AttDailyRequest from '../../../domain/models/attendance/AttDailyRequest';
import * as AttDailyRequestType from '../../../domain/models/attendance/AttDailyRequestType';

import Employee from '../../models/Employee';

import AttRecord from '../../models/AttRecord';
import AttSummary from '../../models/AttSummary';

export type State = {
  employee: Employee | null,
  attWorkingType: WorkingType.WorkingType | null,
  summaryPeriodList: Period[] | null,
  attDailyRequestTypeMap: AttDailyRequestType.AttDailyRequestTypeMap | null,
  attDailyRequestMap: { [string]: AttDailyRequest.AttDailyRequest } | null,
  attRecordList: Array<AttRecord> | null,
  attSummary: AttSummary | null,
};

const initialState: State = {
  employee: null,
  attWorkingType: null,
  summaryPeriodList: null,
  attDailyRequestTypeMap: null,
  attDailyRequestMap: null,
  attRecordList: null,
  attSummary: null,
};

type Actions = {|
  type: 'TIMESHEET-PC/ENTITIES/TIMESHEET/SET_TIMESHEET_ITEMS',
  payload: {|
    entities: Object,
    targetEmployeeId: string | null,
  |},
|};

export const ACTIONS: { [string]: $PropertyType<Actions, 'type'> } = {
  SET_TIMESHEET_ITEMS: 'TIMESHEET-PC/ENTITIES/TIMESHEET/SET_TIMESHEET_ITEMS',
};

export const actions = {
  setTimesheetItems: (
    entities: Object,
    targetEmployeeId: string | null = null
  ): Actions => ({
    type: ACTIONS.SET_TIMESHEET_ITEMS,
    payload: {
      entities,
      targetEmployeeId,
    },
  }),
};

export default function reducer(
  state: State = initialState,
  action: Actions
): State {
  switch (action.type) {
    case ACTIONS.SET_TIMESHEET_ITEMS:
      const { entities } = action.payload;

      const employee = Employee.createFromParam({
        employeeName: entities.employeeName,
        departmentName: entities.departmentName,
        workingTypeName: entities.workingTypeName,
      });

      const attWorkingType = WorkingType.createFromRemote(entities.workingType);

      const summaryPeriodList = entities.periods;

      const attDailyRequestTypeMap = entities.requestTypes;

      const attDailyRequestMap = Object.keys(entities.requests || {}).reduce(
        (hash, key) => {
          const request = entities.requests[key];
          hash[key] = AttDailyRequest.createFromRemote(
            attDailyRequestTypeMap,
            request
          );
          return hash;
        },
        {}
      );

      const attSummary = AttSummary.createFromParam(entities);

      const attRecordList = entities.records.map((record) =>
        AttRecord.createFromParam(record, attSummary.isLocked)
      );

      return {
        ...state,
        employee,
        attWorkingType,
        summaryPeriodList,
        attDailyRequestTypeMap,
        attDailyRequestMap,
        attRecordList,
        attSummary,
      };

    default:
      return state;
  }
}
