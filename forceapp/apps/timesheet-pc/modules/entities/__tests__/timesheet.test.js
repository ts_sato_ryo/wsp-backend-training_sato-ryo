import reducer, { ACTIONS } from '../timesheet';
import dummyAPIResponse from '../../../../repositories/__tests__/mocks/response/timesheet-get--fixed-time';

describe('timesheet-pc/modules/entities/reducer', () => {
  describe('case ACTIONS.SET_TIMESHEET_ITEMS', () => {
    describe('勤務表データ取得APIから取得した構造化されたデータが、変換されてストアに投入される', () => {
      const nextState = reducer(
        {},
        {
          type: ACTIONS.SET_TIMESHEET_ITEMS,
          payload: {
            entities: dummyAPIResponse,
            targetEmployeeId: null,
          },
        }
      );

      describe('employee - 社員情報', () => {
        test('上述のフィールド名であること', () => {
          expect(nextState).toHaveProperty('employee');
        });
      });

      describe('attWorkingType - 勤務体系', () => {
        test('上述のフィールド名であること', () => {
          expect(nextState).toHaveProperty('attWorkingType');
        });
      });

      describe('summaryPeriodList - 集計期間の一覧', () => {
        test('上述のフィールド名であること', () => {
          expect(nextState).toHaveProperty('summaryPeriodList');
        });
      });

      describe('attDailyRequestTypeMap - 申請可能な勤怠申請タイプリスト', () => {
        test('上述のフィールド名であること', () => {
          expect(nextState).toHaveProperty('attDailyRequestTypeMap');
        });
      });

      describe('attDailyRequestMap - 申請済みの勤怠申請リスト', () => {
        test('上述のフィールド名であること', () => {
          expect(nextState).toHaveProperty('attDailyRequestMap');
        });

        test('休暇申請が、LeaveRequestとして格納されること', () => {
          expect(nextState.attDailyRequestMap.a077F000000UyG2QAK.type).toEqual(
            'Leave'
          );
        });
      });

      describe('attSummary - 勤怠サマリー', () => {
        test('上述のフィールド名であること', () => {
          expect(nextState).toHaveProperty('attSummary');
        });
      });

      describe('attRecordList - 集計期間内の勤怠日次明細のリスト', () => {
        test('上述のフィールド名であること', () => {
          expect(nextState).toHaveProperty('attRecordList');
        });
      });

      test('上記以外の内容は投入されない', () => {
        expect(Object.keys(nextState).sort()).toEqual(
          [
            'employee',
            'summaryPeriodList',
            'attWorkingType',
            'attDailyRequestTypeMap',
            'attDailyRequestMap',
            'attSummary',
            'attRecordList',
          ].sort()
        );
      });
    });
  });
});
