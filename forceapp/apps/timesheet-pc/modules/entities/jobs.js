// @flow

import type { Job } from '../../../domain/models/time-tracking/Job';

// State

export type State = Job[][];

const initialState = [];

// Action

type Clear = {|
  type: 'TIMESHEET-PC/ENTITIES/TIMESHEET/CLEAR',
|};

type FetchSuccess = {|
  type: 'TIMESHEET-PC/ENTITIES/TIMESHEET/FETCH_SUCCESS',
  payload: {|
    jobs: Job[][],
  |},
|};

const CLEAR = 'TIMESHEET-PC/ENTITIES/TIMESHEET/CLEAR';
const FETCH_SUCCESS = 'TIMESHEET-PC/ENTITIES/TIMESHEET/FETCH_SUCCESS';

type Action = Clear | FetchSuccess;

export const actions = {
  clear: (): Clear => ({
    type: CLEAR,
  }),

  fetchSuccess: (jobs: Job[][]): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: {
      jobs,
    },
  }),
};

// Reducer

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case CLEAR: {
      return [];
    }

    case FETCH_SUCCESS: {
      return action.payload.jobs;
    }

    default:
      return state;
  }
};
