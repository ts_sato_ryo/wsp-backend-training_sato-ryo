// @flow

import _ from 'lodash';

import type { DailyRecord } from '../../../domain/models/time-tracking/DailyRecord';
import type { TimesheetFromRemote } from '../../../domain/models/attendance/Timesheet';

// State

type RecordDate = string;

export type DailyTimeTrackRecord = {
  ...DailyRecord,
  realWorkTime: ?number,
  totalTaskTime: ?number,
};

export type State = { [RecordDate]: DailyTimeTrackRecord };

const initialState = {};

// Actions

type FetchSuccess = {|
  type: 'TIMESHEET-PC/ENTITIES/DAILYTIMETRACK/FETCH_SUCCESS',
  payload: {
    dailyTimeTrackList: DailyRecord[],
    timesheet: TimesheetFromRemote,
  },
|};

type UpdateRecords = {|
  type: 'TIMESHEET-PC/ENTITIES/DAILYTIMETRACK/UPDATE_RECORDS',
  payload: {
    dailyTimeTrackList: DailyRecord[],
    timesheet: TimesheetFromRemote,
  },
|};

const FETCH_SUCCESS = 'TIMESHEET-PC/ENTITIES/DAILYTIMETRACK/FETCH_SUCCESS';
const UPDATE_RECORDS = 'TIMESHEET-PC/ENTITIES/DAILYTIMETRACK/UPDATE_RECORDS';

export type Action = FetchSuccess;

export const actions = {
  fetchSuccess: (
    dailyTimeTrackList: DailyRecord[],
    timesheet: TimesheetFromRemote
  ): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: {
      dailyTimeTrackList,
      timesheet,
    },
  }),
  updateRecords: (
    dailyTimeTrackList: DailyRecord[],
    timesheet: TimesheetFromRemote
  ): UpdateRecords => ({
    type: UPDATE_RECORDS,
    payload: {
      dailyTimeTrackList,
      timesheet,
    },
  }),
};

// Reducer

const createTimeTrackMap = (
  dailyTimeTrackList: DailyRecord[],
  timesheet: TimesheetFromRemote
): { [RecordDate]: DailyTimeTrackRecord } => {
  const dailyAttRecordMap = _(timesheet.records)
    .keyBy((record) => record.recordDate)
    .value();
  return _(dailyTimeTrackList)
    .keyBy((track: DailyRecord) => track.targetDate)
    .mapValues((track) => ({
      ...track,
      realWorkTime: dailyAttRecordMap[track.targetDate].realWorkTime,
      totalTaskTime: track.time,
    }))
    .value();
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case FETCH_SUCCESS: {
      const { dailyTimeTrackList, timesheet } = (action: FetchSuccess).payload;
      return createTimeTrackMap(dailyTimeTrackList, timesheet);
    }

    case UPDATE_RECORDS: {
      const { dailyTimeTrackList, timesheet } = action.payload;
      const updatingRecords = createTimeTrackMap(dailyTimeTrackList, timesheet);
      return {
        ...state,
        ...updatingRecords,
      };
    }

    default:
      return state;
  }
};
