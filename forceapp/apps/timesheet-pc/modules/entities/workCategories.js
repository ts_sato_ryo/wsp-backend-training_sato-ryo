// @flow

// State

import type { WorkCategory } from '../../../domain/models/time-tracking/WorkCategory';

export type State = WorkCategory[];

const initialState = [];

// Action

type Clear = {|
  type: 'TIMESHEET-PC/ENTITIES/WORK_CATEGORY/CLEAR',
|};

type FetchSuccess = {|
  type: 'TIMESHEET-PC/ENTITIES/WORK_CATEGORY/FETCH_SUCCESS',
  payload: {|
    workCategories: WorkCategory[],
  |},
|};

const CLEAR = 'TIMESHEET-PC/ENTITIES/WORK_CATEGORY/CLEAR';
const FETCH_SUCCESS = 'TIMESHEET-PC/ENTITIES/WORK_CATEGORY/FETCH_SUCCESS';

type Action = Clear | FetchSuccess;

export const actions = {
  clear: (): Clear => ({
    type: CLEAR,
  }),

  fetchSuccess: (workCategories: WorkCategory[]): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: {
      workCategories,
    },
  }),
};

// Reducer

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case CLEAR: {
      return [];
    }

    case FETCH_SUCCESS: {
      return action.payload.workCategories;
    }

    default:
      return state;
  }
};
