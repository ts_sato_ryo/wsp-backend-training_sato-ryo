/**
 * @type {String|null} YYYY-MM-DD形式の文字列
 */
const initialState = null;

const ACTIONS = {
  SET: 'TIMESHEET-PC/CLIENT/SELECTED_PERIOD_START_DATE/SET',
};

export const actions = {
  set: (period) => ({ type: ACTIONS.SET, payload: period }),
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;

    default:
      return state;
  }
}
