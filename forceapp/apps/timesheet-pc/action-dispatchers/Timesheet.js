// @flow

import { type Dispatch } from 'redux';

import type { TimesheetFromRemote } from '../../domain/models/attendance/Timesheet';
import type { UserSetting } from '../../domain/models/UserSetting';
import {
  isAvailableTimeTrack,
  type Permission,
} from '../../domain/models/access-control/Permission';

import Api from '../../commons/api';

import UrlUtil from '../../commons/utils/UrlUtil';

import { withLoading, catchBusinessError } from '../../commons/actions/app';
import * as stampWidgetActions from '../../commons/action-dispatchers/StampWidget';
import { actions as selectedPeriodUiActions } from '../modules/client/selectedPeriodStartDate';
import { actions as entitiesActions } from '../modules/entities/timesheet';

import msg from '../../commons/languages';

import { loadDailyTimeTrackRecords } from './DailyTimeTrack';

/**
 * 指定された集計期間の勤務表を表示する
 * @param {String} [targetDate=null] YYYY-MM-DD 対象の集計期間に含まれる日付
 * @param {String} [targetEmployeeId=null] The ID of target employee
 */
export const switchSummaryPeriod = (
  targetDate: null | string = null,
  targetEmployeeId: null | string = null
) => (dispatch: Dispatch<Object>) => {
  return dispatch(
    withLoading(async (): Promise<TimesheetFromRemote | null> => {
      if (targetDate) {
        dispatch(selectedPeriodUiActions.set(targetDate));
      }

      const req = {
        path: '/att/timesheet/get',
        param: {
          targetDate,
          empId: targetEmployeeId,
        },
      };

      // 打刻ウィジェットの同期
      // NOTE: 確定申請の後処理で打刻を無効化するために追加。
      // FIXME: 処理が不明瞭になるので targetEmployeeId をフラグとして扱いたくない。
      // 打刻ウィジェットを初期化する瞬間は決まっているはずなので、ここで処理するのを止めたい。
      if (targetEmployeeId === null) {
        dispatch(stampWidgetActions.initDailyStampTime());
      }

      try {
        const result = await Api.invoke(req);

        dispatch(selectedPeriodUiActions.set(result.startDate));
        dispatch(entitiesActions.setTimesheetItems(result, targetEmployeeId));

        return result;
      } catch {
        dispatch(
          catchBusinessError(
            msg().Com_Err_ErrorTitle,
            msg().Com_Err_ProxyPermissionErrorBody,
            msg().Com_Err_ProxyPermissionErrorSolution,
            { isContinuable: false }
          )
        );
        return null;
      }
    })
  );
};

/**
 * @param {String} periodStartDate The start date of target month
 * @param {String} [targetEmployeeId=null] The ID of target employee
 */
export const openLeaveWindow = (
  periodStartDate: string,
  targetEmployeeId: null | string = null
) => () => {
  const param = targetEmployeeId
    ? {
        targetDate: periodStartDate,
        targetEmployeeId,
      }
    : {
        targetDate: periodStartDate,
      };
  UrlUtil.openApp('timesheet-pc-leave', param);
};

/**
 * @param {String} periodStartDate The start date of target month
 * @param {String} [targetEmployeeId=null] The ID of target employee
 */
export const openSummaryWindow = (
  periodStartDate: string,
  targetEmployeeId: null | string = null
) => () => {
  const param = targetEmployeeId
    ? {
        targetDate: periodStartDate,
        targetEmployeeId,
      }
    : {
        targetDate: periodStartDate,
      };
  UrlUtil.openApp('timesheet-pc-summary', param);
};

/**
 * Load all resources needed displaying timesheet.
 */
export const loadTimesheetOnPeriod = (
  targetDate: null | string = null,
  targetEmployeeId: null | string = null,
  userPermission: Permission,
  userSetting: UserSetting,
  isDelegated: boolean = false
) => async (dispatch: Dispatch<Object>) => {
  const timesheet: TimesheetFromRemote = await dispatch(
    switchSummaryPeriod(targetDate, targetEmployeeId)
  );
  if (isAvailableTimeTrack(userPermission, userSetting, isDelegated)) {
    await dispatch(
      loadDailyTimeTrackRecords(timesheet, targetEmployeeId || undefined)
    );
  }
};
