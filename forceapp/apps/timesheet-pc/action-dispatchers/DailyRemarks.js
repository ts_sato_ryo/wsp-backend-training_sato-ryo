import Api from '../../commons/api';
import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';

import * as TimesheetActions from './Timesheet';
import { actions as editingDailyRemarksActions } from '../modules/ui/editingDailyRemarks';
import DailyRemarksModel from '../models/DailyRemarks';
import { collectRemarksFromRequests } from '../models/DailyRequestConditions';

/**
 * 日次の備考ダイアログを表示する
 * @param {AttRecord} attRecord
 * @param {DailyRequestConditions} rerequestConditions
 */
export const showDialog = (attRecord, requestConditions) => (dispatch) => {
  const otherRemarksFromRequest =
    requestConditions && collectRemarksFromRequests(requestConditions);

  const dailyRemarks = DailyRemarksModel.createFromParam(
    attRecord,
    otherRemarksFromRequest
  );

  dispatch(editingDailyRemarksActions.set(dailyRemarks));
};

/**
 * 日次の備考ダイアログを閉じる
 */
export const hideDialog = editingDailyRemarksActions.unset;

/**
 * 日次の備考を保存する
 * @param {DailyRemarks} dailyRemarks
 * @param {String} resultTargetPeriodStartDate 成功時に再取得・表示する対象となる集計期間の起算日
 * @param {?String} [targetEmployeeId=null] The ID of target employee
 */
export const save = (
  dailyRemarks,
  resultTargetPeriodStartDate,
  targetEmployeeId = null
) => (dispatch) => {
  const req = {
    path: '/att/daily-remarks/save',
    param: dailyRemarks.convertToPostRequestParam(),
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then(() => {
      dispatch(editingDailyRemarksActions.unset());
      return dispatch(
        TimesheetActions.switchSummaryPeriod(
          resultTargetPeriodStartDate,
          targetEmployeeId
        )
      );
    })
    .catch((err) => dispatch(catchApiError(err)))
    .then(() => {
      dispatch(loadingEnd());
    });
};
