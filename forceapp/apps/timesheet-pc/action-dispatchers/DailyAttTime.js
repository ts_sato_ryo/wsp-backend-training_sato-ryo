// @flow

import { type Dispatch, bindActionCreators } from 'redux';
import isNil from 'lodash/isNil';

import Api from '../../commons/api';
import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';

import * as TimesheetActions from './Timesheet';
import * as DailyAttTimeResultActions from './DailyAttTimeResult';
import { actions as editingDailyAttTimeActions } from '../modules/ui/editingDailyAttTime';
import DailyAttTimeModel from '../models/DailyAttTime';
import AttRecord from '../models/AttRecord';
import type { TimesheetFromRemote } from '../../domain/models/attendance/Timesheet';
import type { UserSetting } from '../../domain/models/UserSetting';
import type { Permission } from '../../domain/models/access-control/Permission';
import { loadDailyTimeTrackRecords } from './DailyTimeTrack';
import { isAvailableTimeTrack } from '../../domain/models/access-control/Permission';

/**
 * 日次の勤務時刻ダイアログを表示する
 * @param {AttRecord} attRecord
 */
export const showDialog = (attRecord: AttRecord) => (
  dispatch: Dispatch<Object>
) => {
  const dailyAttTime = DailyAttTimeModel.createFromParam(attRecord);
  dispatch(editingDailyAttTimeActions.set(dailyAttTime));
};

/**
 * 日次の勤務時刻ダイアログを閉じる
 */
export const hideDialog = editingDailyAttTimeActions.unset;

/**
 * 日次の勤務時刻を保存する
 * @param {DailyAttTime} dailyAttTime
 * @param {String} resultTargetPeriodStartDate 成功時に再取得・表示する対象となる集計期間の起算日
 * @param {?String} [targetEmployeeId=null] The ID of target employee
 */
export const save = (
  dailyAttTime: DailyAttTimeModel,
  resultTargetPeriodStartDate: string,
  targetEmployeeId: null | string = null,
  userPermission: Permission,
  userSetting: UserSetting
) => async (dispatch: Dispatch<Object>) => {
  const AppService = bindActionCreators(
    { loadingStart, loadingEnd, catchApiError },
    dispatch
  );

  const req = {
    path: '/att/daily-time/save',
    param: dailyAttTime.convertToPostRequestParam(targetEmployeeId),
  };

  try {
    AppService.loadingStart();
    const result = await Api.invoke(req);
    AppService.loadingEnd();

    await dispatch(
      DailyAttTimeResultActions.doAddProcess(
        dailyAttTime.recordDate,
        targetEmployeeId,
        result
      )
    );

    AppService.loadingStart();
    dispatch(editingDailyAttTimeActions.unset());
    const timesheet: TimesheetFromRemote = await dispatch(
      TimesheetActions.switchSummaryPeriod(
        resultTargetPeriodStartDate,
        targetEmployeeId
      )
    );
    if (
      isAvailableTimeTrack(
        userPermission,
        userSetting,
        !isNil(targetEmployeeId)
      )
    ) {
      await dispatch(
        loadDailyTimeTrackRecords(timesheet, targetEmployeeId || undefined)
      );
    }
    AppService.loadingEnd();
  } catch (err) {
    dispatch(catchApiError(err));
  } finally {
    AppService.loadingEnd();
  }
};
