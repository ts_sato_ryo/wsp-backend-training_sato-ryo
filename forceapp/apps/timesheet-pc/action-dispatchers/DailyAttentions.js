/* @flow */
import { actions as DailyAttentionsActions } from '../modules/ui/dailyAttentions';

/**
 * 警告ダイアログを開きます。
 * @param {Array<string>} messages 表示したいメッセージ一覧
 */
export const showDailyAttentionsDialog = (messages: Array<string>) => (
  dispatch: Dispatch<any>
) => {
  dispatch(DailyAttentionsActions.set(messages));
};

/**
 * 警告ダイアログを閉じます。
 */
export const hideDailyAttentionsDialog = () => (dispatch: Dispatch<any>) => {
  dispatch(DailyAttentionsActions.unset());
};
