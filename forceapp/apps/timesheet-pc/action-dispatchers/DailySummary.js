// @flow

import { bindActionCreators, type Dispatch } from 'redux';
import moment from 'moment';
import head from 'lodash/head';
import isNil from 'lodash/isNil';

import Api from '../../commons/api';

import type { DailySummaryTask } from '../../domain/models/time-management/DailySummaryTask';
import type { PersonalSetting } from '../../commons/modules/personalSetting';
import type { Job } from '../../domain/models/time-tracking/Job';
import DateUtil from '../../commons/utils/DateUtil';
import {
  catchApiError,
  loadingStart,
  loadingEnd,
  withLoading,
} from '../../commons/actions/app';
import { actions as EventsActions } from '../modules/entities/events';
import { actions as JobsActions } from '../modules/entities/jobs';
import { actions as DailySummaryActions } from '../modules/ui/dailySummary';
import PlannerEventRepository from '../../repositories/PlannerEventRepository';
import { postDailySummary } from '../../domain/models/time-management/DailySummary';
import * as DailyStampTime from '../../domain/models/attendance/DailyStampTime';
import { doAddProcess } from '../../commons/action-dispatchers/DailyStampTimeResult';
import DailySummaryRepository from '../../repositories/DailySummaryRepository';
import JobRepository from '../../repositories/JobRepository';
import { isDefaultJobAvailable } from '../../domain/models/time-tracking/DailyTask';
import { loadDailyTimeTrackRecordsBetweenPeriods } from './DailyTimeTrack';
import type { TimesheetFromRemote } from '../../domain/models/attendance/Timesheet';
import { switchSummaryPeriod } from './Timesheet';
import TimeTrackJobRepository from '../../repositories/TimeTrackJobRepository';
import type { WorkCategory } from '../../domain/models/time-tracking/WorkCategory';

const fetchDefaultJob = async (
  targetDate: string,
  personalSetting: PersonalSetting
): Promise<?Job> => {
  if (
    isDefaultJobAvailable(targetDate, personalSetting.defaultJob) &&
    !isNil(personalSetting.defaultJob)
  ) {
    const { records } = await JobRepository.search({
      id: personalSetting.defaultJob.id,
    });
    return head(records);
  } else {
    return null;
  }
};

export const openDailySummary = (
  targetDate: string,
  personalSetting: PersonalSetting,
  empId?: string
) => async (dispatch: Dispatch<Object>) => {
  const EventsService = bindActionCreators(EventsActions, dispatch);
  const DailySummaryService = bindActionCreators(DailySummaryActions, dispatch);
  const AppService = bindActionCreators(
    { catchApiError, loadingStart, loadingEnd },
    dispatch
  );

  AppService.loadingStart();
  try {
    const defaultJob = await fetchDefaultJob(targetDate, personalSetting);
    const [dailySummary, { events }] = await Promise.all([
      DailySummaryRepository.fetch(
        { targetDate, empId },
        isNil(defaultJob) ? undefined : { defaultJob }
      ),
      PlannerEventRepository.fetch({
        startDate: targetDate,
        endDate: targetDate,
        empId,
      }),
    ]);
    DailySummaryService.fetchSuccess(dailySummary);
    EventsService.fetchSuccess(events, targetDate);

    if (!isNil(defaultJob) && !isNil(defaultJob.id)) {
      DailySummaryService.update('defaultJobId', defaultJob.id);
    } else {
      DailySummaryService.update('defaultJobId', '');
    }

    if (DateUtil.isToday(targetDate)) {
      const stampTime = await DailyStampTime.fetchDailyStampTime();
      DailySummaryService.update(
        'isEnableEndStamp',
        stampTime.isEnableEndStamp
      );
    } else {
      DailySummaryService.update('isEnableEndStamp', false);
    }

    DailySummaryService.update('targetDate', targetDate);
    DailySummaryService.open(targetDate);
  } catch (e) {
    AppService.catchApiError(e);
  } finally {
    AppService.loadingEnd();
  }
};

export const closeDailySummary = () => (dispatch: Dispatch<Object>) => {
  const DailySummaryService = bindActionCreators(DailySummaryActions, dispatch);

  DailySummaryService.close();
};

export const editNote = (value: string) => (dispatch: Dispatch<Object>) => {
  dispatch(DailySummaryActions.update('note', value));
};

export const editTimestampComment = (value: string) => (
  dispatch: Dispatch<Object>
) => {
  dispatch(DailySummaryActions.update('timestampComment', value));
};

const refreshTimesheet = (
  targetDate: string,
  targetEmployeeId?: string
) => async (dispatch: Dispatch<Object>) => {
  const timesheet: TimesheetFromRemote = await dispatch(
    switchSummaryPeriod(targetDate, targetEmployeeId)
  );
  await dispatch(
    loadDailyTimeTrackRecordsBetweenPeriods(
      { startDate: targetDate, endDate: targetDate },
      timesheet,
      targetEmployeeId || undefined
    )
  );
};

export const saveDailySummary = (
  taskList: DailySummaryTask[],
  note: string,
  output?: string,
  selectedDay: moment,
  targetEmployeeId?: string
) => async (dispatch: Dispatch<Object>) => {
  const AppService = bindActionCreators(
    { catchApiError, loadingStart, loadingEnd },
    dispatch
  );

  AppService.loadingStart();
  try {
    await postDailySummary({
      editing: {
        taskList,
        note,
        output: output || '',
      },
      selectedDay,
    });
    await dispatch(closeDailySummary());
    await dispatch(
      refreshTimesheet(selectedDay.format('YYYY-MM-DD'), targetEmployeeId)
    );
  } catch (e) {
    AppService.catchApiError(e);
  } finally {
    AppService.loadingEnd();
  }
};

export const saveDailySummaryAndLeaveWork = (
  taskList: DailySummaryTask[],
  note: string,
  comment: string,
  output?: string,
  selectedDay: moment
) => async (dispatch: Dispatch<Object>) => {
  const AppService = bindActionCreators(
    { catchApiError, loadingStart, loadingEnd },
    dispatch
  );

  try {
    AppService.loadingStart();
    await postDailySummary({
      editing: {
        taskList,
        note,
        output: output || '',
      },
      selectedDay,
    });
    await dispatch(closeDailySummary());
    const result = await DailyStampTime.postStamp(
      {
        mode: DailyStampTime.CLOCK_TYPE.CLOCK_OUT,
        message: comment,
      },
      DailyStampTime.STAMP_SOURCE.WEB
    );
    AppService.loadingEnd();

    await dispatch(doAddProcess(result));

    AppService.loadingStart();
    await dispatch(refreshTimesheet(selectedDay.format('YYYY-MM-DD')));
    AppService.loadingEnd();
  } catch (e) {
    AppService.catchApiError(e);
  } finally {
    AppService.loadingEnd();
  }
};

export const editTask = (index: number, prop: string, value: mixed) => (
  dispatch: Dispatch<Object>
) => {
  dispatch(DailySummaryActions.updateTask(index, prop, value));
};

export const deleteTask = (index: number) => (dispatch: Dispatch<Object>) => {
  dispatch(DailySummaryActions.deleteTask(index));
};

export const toggleTask = (index: number) => (dispatch: Dispatch<Object>) => {
  dispatch(DailySummaryActions.toggleDirectInput(index));
};

export const reorderTask = (reorderedTasks: DailySummaryTask[]) => (
  dispatch: Dispatch<Object>
) => {
  dispatch(DailySummaryActions.update('taskList', reorderedTasks));
};

export const fetchJobTree = (
  targetDate: string,
  parentItem: null | Job = null,
  parentLevelItems: Job[][] = []
) => async (dispatch: Dispatch<Object>) => {
  const JobsService = bindActionCreators(JobsActions, dispatch);
  const AppService = bindActionCreators(
    { catchApiError, loadingStart, loadingEnd },
    dispatch
  );

  AppService.loadingStart();

  try {
    const jobTree = await TimeTrackJobRepository.search({
      targetDate,
      parentItem,
      parentLevelItems,
    });
    JobsService.fetchSuccess(jobTree);
  } catch (err) {
    AppService.catchApiError(err, { isContinuable: true });
  } finally {
    AppService.loadingEnd();
  }
};

export const openJobSelectDialog = (targetDate: string) => async (
  dispatch: Dispatch<Object>
) => {
  dispatch(JobsActions.clear());
  await dispatch(fetchJobTree(targetDate));
  dispatch(DailySummaryActions.openJobSelectDialog());
};

export const closeJobSelectDialog = () => (dispatch: Dispatch<Object>) => {
  dispatch(JobsActions.clear());
  dispatch(DailySummaryActions.closeJobSelectDialog());
};

export const selectJobOrFetchJobTree = (
  targetDate: string,
  selectedItem: Job,
  parentLevelItems: Job[][]
) => async (dispatch: Dispatch<Object>) => {
  const AppService = bindActionCreators(
    { catchApiError, withLoading },
    dispatch
  );

  if (selectedItem.hasChildren && parentLevelItems !== undefined) {
    dispatch(fetchJobTree(targetDate, selectedItem, parentLevelItems));
  } else {
    try {
      await AppService.withLoading(async () => {
        const {
          workCategoryList,
        }: { workCategoryList: WorkCategory[] } = await Api.invoke({
          path: '/time/work-category/get',
          param: {
            jobId: selectedItem.id,
            targetDate,
          },
        });
        dispatch(
          DailySummaryActions.addJobToTaskList(selectedItem, workCategoryList)
        );
      });
    } catch (e) {
      AppService.catchApiError(e);
    }

    dispatch(JobsActions.clear());
    dispatch(closeJobSelectDialog());
  }
};
