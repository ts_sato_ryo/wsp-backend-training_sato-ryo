// @flow

import head from 'lodash/head';
import isNil from 'lodash/isNil';
import { type Dispatch, bindActionCreators } from 'redux';

import { catchApiError, confirm, withLoading } from '../../commons/actions/app';
import msg from '../../commons/languages';

import {
  type AttDailyRequest,
  type EditAction,
  type DisableAction,
  DISABLE_ACTION,
  EDIT_ACTION,
  isForReapply,
  createFromRemote,
  createFromDefaultValue,
} from '../../domain/models/attendance/AttDailyRequest';
import * as AbsenceRequest from '../../domain/models/attendance/AttDailyRequest/AbsenceRequest';
import * as LeaveRequest from '../../domain/models/attendance/AttDailyRequest/LeaveRequest';
import * as HolidayWorkRequest from '../../domain/models/attendance/AttDailyRequest/HolidayWorkRequest';
import * as EarlyStartWorkRequest from '../../domain/models/attendance/AttDailyRequest/EarlyStartWorkRequest';
import * as OvertimeWorkRequest from '../../domain/models/attendance/AttDailyRequest/OvertimeWorkRequest';
import * as DirectRequest from '../../domain/models/attendance/AttDailyRequest/DirectRequest';
import * as PatternRequest from '../../domain/models/attendance/AttDailyRequest/PatternRequest';
import {
  type AttDailyRequestTypeMap,
  type Code,
  CODE,
} from '../../domain/models/attendance/AttDailyRequestType';
import { type WorkingType } from '../../domain/models/attendance/WorkingType';
import * as SubstituteLeaveType from '../../domain/models/attendance/SubstituteLeaveType';
import { doesAttLeaveExist } from '../../domain/models/attendance/LeaveType';
import { isAvailableTimeTrack } from '../../domain/models/access-control/Permission';

import type DailyRequestConditions from '../models/DailyRequestConditions';

import AttDailyRequestRepository from '../../repositories/AttDailyRequestRepository';
import AttDailyLeaveRepository from '../../repositories/AttDailyLeaveRepository';
import AttDailyPatternRepository from '../../repositories/AttDailyPatternRepository';

import { actions as editingActions } from '../modules/ui/dailyRequest/editing';
import { actions as absenceRequestActions } from '../modules/ui/dailyRequest/requests/absenceRequest';
import { actions as directRequestActions } from '../modules/ui/dailyRequest/requests/directRequest';
import { actions as earlyStartWorkRequestActions } from '../modules/ui/dailyRequest/requests/earlyStartWorkRequest';
import { actions as holidayWorkRequestActions } from '../modules/ui/dailyRequest/requests/holidayWorkRequest';
import { actions as leaveRequestActions } from '../modules/ui/dailyRequest/requests/leaveRequest';
import { actions as overtimeWorkRequestActions } from '../modules/ui/dailyRequest/requests/overtimeWorkRequest';
import { actions as patternRequestActions } from '../modules/ui/dailyRequest/requests/patternRequest';

import { actions as targetDateActions } from '../modules/ui/dailyRequest/targetDate';
import * as TimesheetActions from './Timesheet';
import { loadDailyTimeTrackRecords } from './DailyTimeTrack';
import type { TimesheetFromRemote } from '../../domain/models/attendance/Timesheet';
import type { UserSetting } from '../../domain/models/UserSetting';
import type { Permission } from '../../domain/models/access-control/Permission';

/**
 * 各種勤怠申請毎にモジュールを結びつけます。
 * @param request 各種日時申請
 */
const initializeRequest = (target: AttDailyRequest) => (
  dispatch: Dispatch<any>
) => {
  switch (target.type) {
    case CODE.Absence:
      dispatch(absenceRequestActions.initialize(target));
      break;
    case CODE.Direct:
      dispatch(directRequestActions.initialize(target));
      break;
    case CODE.EarlyStartWork:
      dispatch(earlyStartWorkRequestActions.initialize(target));
      break;
    case CODE.HolidayWork:
      dispatch(holidayWorkRequestActions.initialize(target));
      break;
    case CODE.Leave:
      dispatch(leaveRequestActions.initialize(target));
      break;
    case CODE.OvertimeWork:
      dispatch(overtimeWorkRequestActions.initialize(target));
      break;
    case CODE.Pattern:
      dispatch(patternRequestActions.initialize(target));
      break;
    default:
      break;
  }
};

/**
 * 申請済みの勤怠日次申請を詳細ペインに表示する
 * @param request 各種日時申請
 */
export const showRequestDetailPane = (target: AttDailyRequest) => (
  dispatch: Dispatch<any>
) => {
  dispatch(initializeRequest(target));
  dispatch(editingActions.initialize(target));
};

/**
 * 勤怠日次申請ダイアログを表示する
 */
export const showManagementDialog = (
  dailyRequestConditions: DailyRequestConditions
) => (dispatch: Dispatch<any>) => {
  const { recordDate, latestRequests } = dailyRequestConditions;
  const editingActionsService = bindActionCreators(editingActions, dispatch);
  const targetDateActionsService = bindActionCreators(
    targetDateActions,
    dispatch
  );

  targetDateActionsService.set(recordDate);

  const target = head(latestRequests);

  if (target) {
    dispatch(initializeRequest(target));
    dispatch(editingActions.initialize(target));
  } else {
    editingActionsService.clear();
  }
};

/**
 * 勤怠日次申請ダイアログを閉じる
 */
export const hideManagementDialog = targetDateActions.unset;

/**
 * 指定された種別の勤怠日次申請について、新規申請の編集画面を表示する
 * @param {AttRequestTypeCode} requestType
 * @param {String} targetDate
 * @param {AttWorkingType} attWorkingType
 * @param {?String} [targetEmployeeId=null] The ID of target employee
 */
export const showEntryRequestPane = (
  targetDate: string,
  requestTypeCode: Code,
  attRequestTypeMap: AttDailyRequestTypeMap,
  workingType: WorkingType,
  targetEmployeeId: ?string = null
) => (dispatch: Dispatch<any>) => {
  const service = bindActionCreators(
    {
      withLoading,
      catchApiError,
    },
    dispatch
  );
  const editingActionsService = bindActionCreators(editingActions, dispatch);
  const target = createFromDefaultValue(attRequestTypeMap, requestTypeCode);

  switch (requestTypeCode) {
    // 休暇申請
    case CODE.Leave: {
      service.withLoading(() => {
        const req = {
          targetDate,
          empId: targetEmployeeId || '',
        };
        return AttDailyLeaveRepository.search(req)
          .then((attLeaveList) => {
            const leave = LeaveRequest.create(target, attLeaveList, targetDate);
            dispatch(leaveRequestActions.initialize(leave, attLeaveList));
            editingActionsService.initialize(target);
          })
          .catch((err) =>
            dispatch(catchApiError(err, { isContinuable: true }))
          );
      });
      break;
    }

    // 休日出勤申請
    case CODE.HolidayWork: {
      const holidayWorkRequest = HolidayWorkRequest.create(
        target,
        workingType,
        targetDate
      );
      const substituteLeaveType = SubstituteLeaveType.create(
        holidayWorkRequest,
        workingType
      );
      dispatch(
        holidayWorkRequestActions.initialize(
          holidayWorkRequest,
          substituteLeaveType
        )
      );
      editingActionsService.initialize(target);
      break;
    }

    // 早朝勤務申請
    case CODE.EarlyStartWork: {
      const earlyStartWorkRequest = EarlyStartWorkRequest.create(
        target,
        workingType,
        targetDate
      );
      dispatch(earlyStartWorkRequestActions.initialize(earlyStartWorkRequest));
      editingActionsService.initialize(target);
      break;
    }

    // 残業申請
    case CODE.OvertimeWork: {
      const overtimeWorkRequest = OvertimeWorkRequest.create(
        target,
        workingType,
        targetDate
      );
      dispatch(overtimeWorkRequestActions.initialize(overtimeWorkRequest));
      editingActionsService.initialize(target);
      break;
    }

    // 欠勤申請
    case CODE.Absence: {
      const absenceRequest = AbsenceRequest.create(target, targetDate);
      dispatch(absenceRequestActions.initialize(absenceRequest));
      editingActionsService.initialize(target);
      break;
    }

    // 直行直帰申請
    case CODE.Direct: {
      const directRequest = DirectRequest.create(
        target,
        workingType,
        targetDate
      );
      dispatch(directRequestActions.initialize(directRequest));
      editingActionsService.initialize(target);
      break;
    }

    // 勤務時間変更申請
    case CODE.Pattern: {
      service.withLoading(() =>
        AttDailyPatternRepository.search({
          targetDate,
          empId: targetEmployeeId || '',
        })
          .then((attPatternList) => {
            const patternRequest = PatternRequest.create(
              target,
              attPatternList,
              targetDate
            );
            dispatch(
              patternRequestActions.initialize(patternRequest, attPatternList)
            );
            editingActionsService.initialize(target);
          })
          .catch((err) => service.catchApiError(err, { isContinuable: true }))
      );
      break;
    }
    default:
  }
};

/**
 * 申請内容の変更／修正を開始する
 * @param target 変更・修正の対象の日次申請
 */
export const startEditing = (
  target: AttDailyRequest,
  workingType: WorkingType,
  proxyEmployeeId: string
) => (dispatch: Dispatch<any>) => {
  const service = bindActionCreators(
    {
      withLoading,
      catchApiError,
    },
    dispatch
  );
  const editingActionsService = bindActionCreators(editingActions, dispatch);

  switch (target.requestTypeCode) {
    // 休暇申請
    case CODE.Leave:
      dispatch(
        withLoading(() => {
          const req = {
            targetDate: target.startDate,
            empId: proxyEmployeeId || '',
            ignoredId: target.id,
          };

          return AttDailyLeaveRepository.search(req)
            .then((attLeaveList) => {
              if (isForReapply(target)) {
                if (doesAttLeaveExist(attLeaveList, target)) {
                  const leave = LeaveRequest.create(target, attLeaveList);
                  dispatch(leaveRequestActions.initialize(leave, attLeaveList));
                } else {
                  const leave = LeaveRequest.create(target);
                  dispatch(leaveRequestActions.initialize(leave));
                }
              } else {
                const leave = LeaveRequest.create(target);
                dispatch(leaveRequestActions.initialize(leave, attLeaveList));
              }
              editingActionsService.initialize(target, true);
            })
            .catch((err) =>
              dispatch(catchApiError(err, { isContinuable: true }))
            );
        })
      );

      break;

    // 休日出勤申請
    case CODE.HolidayWork:
      const holidayWorkRequest = HolidayWorkRequest.create(target);
      const substituteLeaveType = SubstituteLeaveType.create(
        holidayWorkRequest,
        workingType
      );
      dispatch(
        holidayWorkRequestActions.initialize(
          holidayWorkRequest,
          substituteLeaveType
        )
      );
      editingActionsService.initialize(target, true);
      break;

    // 勤務時間変更申請
    case CODE.Pattern:
      service.withLoading(() =>
        AttDailyPatternRepository.search({
          targetDate: target.startDate,
          empId: proxyEmployeeId || '',
          ignoredId: target.id,
        })
          .then((patterns) => {
            const patternRequest = PatternRequest.create(target, patterns);
            dispatch(
              patternRequestActions.initialize(patternRequest, patterns)
            );
            editingActionsService.initialize(target, true);
          })
          .catch((err) => service.catchApiError(err, { isContinuable: true }))
      );
      break;

    default:
      dispatch(initializeRequest(target));
      editingActionsService.initialize(target, true);
  }
};

/**
 * 申請内容の変更／修正を取り消す（中断する）
 */
export const cancelEditing = (request: AttDailyRequest) => (
  dispatch: Dispatch<any>
) => {
  dispatch(initializeRequest(request));
  dispatch(editingActions.initialize(request));
};

/**
 * 申請を提出する
 * @param editingRequest 編集中の申請
 * @param resultTargetPeriodStartDate 成功時に再取得・表示する対象となる集計期間の起算日
 * @param targetEmployeeId The ID of target employee
 */
export const submit = (
  editingRequest: AttDailyRequest,
  editAction: EditAction,
  resultTargetPeriodStartDate: string,
  targetEmployeeId: string | null = null,
  userSetting: UserSetting,
  userPermission: Permission
) => (dispatch: Dispatch<any>) => {
  return dispatch(
    withLoading(async (): Promise<TimesheetFromRemote | void> => {
      try {
        if (editAction === EDIT_ACTION.Create) {
          await AttDailyRequestRepository.create(
            editingRequest,
            targetEmployeeId
          );
        } else {
          await AttDailyRequestRepository.update(
            editingRequest,
            editAction,
            targetEmployeeId
          );
        }
        dispatch(targetDateActions.unset());
        dispatch(editingActions.clear());

        const timesheet = await dispatch(
          TimesheetActions.switchSummaryPeriod(
            resultTargetPeriodStartDate,
            targetEmployeeId
          )
        );
        if (
          isAvailableTimeTrack(
            userPermission,
            userSetting,
            !isNil(targetEmployeeId)
          )
        ) {
          await dispatch(
            loadDailyTimeTrackRecords(timesheet, targetEmployeeId || undefined)
          );
        }
        return timesheet;
      } catch (err) {
        dispatch(catchApiError(err, { isContinuable: true }));
        return undefined;
      }
    })
  );
};

/**
 * 取消系操作の共通の入り口＜申請取消・承認取消・申請取下＞
 * 申請を無効化する（共通：申請取消, 承認取消）
 * @param targetRequest
 * @param disableAction
 * @param resultTargetPeriodStartDate 成功時に再取得・表示する対象となる集計期間の起算日
 * @param targetEmployeeId The ID of target employee
 */
export const disable = (
  targetRequest: AttDailyRequest,
  disableAction: DisableAction,
  resultTargetPeriodStartDate: string,
  targetEmployeeId: string | null = null,
  userSetting: UserSetting,
  userPermission: Permission
) => (dispatch: Dispatch<Object>): Promise<*> => {
  const request = async () => {
    try {
      await dispatch(
        withLoading(() =>
          AttDailyRequestRepository.delete(targetRequest.id, disableAction)
        )
      );

      const timesheet = await dispatch(
        // NOTE: ダイアログは閉じない
        TimesheetActions.switchSummaryPeriod(
          resultTargetPeriodStartDate,
          targetEmployeeId
        )
      );

      const requestId = targetRequest.id;
      const { requestTypes, requests } = timesheet;
      if (requests[requestId]) {
        const $request = createFromRemote(requestTypes, requests[requestId]);
        dispatch(showRequestDetailPane($request));
      } else {
        dispatch(editingActions.clear());
      }

      if (
        isAvailableTimeTrack(
          userPermission,
          userSetting,
          !isNil(targetEmployeeId)
        )
      ) {
        await dispatch(
          loadDailyTimeTrackRecords(timesheet, targetEmployeeId || undefined)
        );
      }

      return timesheet;
    } catch (err) {
      dispatch(catchApiError(err, { isContinuable: true }));
      return undefined;
    }
  };

  switch (disableAction) {
    case DISABLE_ACTION.CancelRequest:
    case DISABLE_ACTION.CancelApproval:
      return request();

    case DISABLE_ACTION.Remove: {
      const message = msg().Att_Msg_DailyReqConfirmRemove;
      return dispatch(
        confirm(message, (isAllowed: boolean): void => {
          if (isAllowed) {
            request();
          }
        })
      );
    }

    default:
      return Promise.reject('Unknown disable action');
  }
};
