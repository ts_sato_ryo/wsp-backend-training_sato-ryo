// @flow

import isNil from 'lodash/isNil';
import { type Dispatch } from 'redux';

import { catchBusinessError, withLoading } from '../../commons/actions/app';
import { getUserSetting } from '../../commons/actions/userSetting';
import { setUserPermission } from '../../commons/modules/accessControl/permission';
import { actions as proxyEmployeeInfoActions } from '../../commons/modules/proxyEmployeeInfo';
import { actions as standaloneModeActions } from '../../commons/modules/standaloneMode';
import { fetch as fetchPersonalSetting } from '../../commons/modules/personalSetting';
import { switchSummaryPeriod } from './Timesheet';
import UrlUtil from '../../commons/utils/UrlUtil';
import msg from '../../commons/languages';

import EmployeeRepository from '../../repositories/EmployeeRepository';
import type { UserSetting } from '../../domain/models/UserSetting';
import type { TimesheetFromRemote } from '../../domain/models/attendance/Timesheet';
import { loadDailyTimeTrackRecords } from './DailyTimeTrack';
import {
  type Permission,
  isAvailableTimeTrack,
} from '../../domain/models/access-control/Permission';

/**
 * 勤務表画面を初期化する
 */
export const initialize = (param: { userPermission: Permission }) => async (
  dispatch: Dispatch<Object>
): Promise<void> => {
  const urlQuery = UrlUtil.getUrlQuery();
  const empId = urlQuery && urlQuery.empId;
  const targetDate = urlQuery && urlQuery.targetDate;
  const standalone = (urlQuery && urlQuery.standalone) === '1';
  const isProxy = !isNil(empId);
  if (isProxy && isNil(targetDate)) {
    dispatch(
      catchBusinessError(
        msg().Com_Err_ErrorTitle,
        msg().Com_Err_ProxyPermissionErrorBody,
        msg().Com_Err_ProxyPermissionErrorSolution,
        { isContinuable: false }
      )
    );
  }
  if (standalone) {
    dispatch(standaloneModeActions.enable());
  }

  if (param) {
    dispatch(setUserPermission(param.userPermission));
  }

  dispatch(
    withLoading(async (): Promise<void> => {
      const [userSetting, result]: [
        UserSetting,
        TimesheetFromRemote
      ] = await Promise.all([
        dispatch(getUserSetting()),
        dispatch(switchSummaryPeriod(targetDate, empId)),
      ]);

      const isSuccess = !isNil(result);

      if (!isSuccess) {
        return;
      }

      const isCurrentEmployee =
        isNil(empId) || userSetting.employeeId === empId;

      if (!isCurrentEmployee) {
        const [targetEmployee] = await EmployeeRepository.search({
          id: empId || undefined,
          targetDate: targetDate || undefined,
        });
        dispatch(
          proxyEmployeeInfoActions.set({
            id: targetEmployee.id,
            employeeCode: targetEmployee.code,
            employeeName: targetEmployee.name,
            employeePhotoUrl: targetEmployee.user.photoUrl,
            departmentCode: '', // Not used?
            departmentName: targetEmployee.department
              ? targetEmployee.department.name
              : '',
            title: targetEmployee.title,
            managerName: targetEmployee.manager
              ? targetEmployee.manager.name
              : '',
          })
        );
      }

      if (
        isAvailableTimeTrack(
          param.userPermission,
          userSetting,
          !isCurrentEmployee
        )
      ) {
        // PersonalSetting is used by DailySummary
        await dispatch(fetchPersonalSetting(empId || undefined));
        await dispatch(loadDailyTimeTrackRecords(result, empId || undefined));
      }
    })
  );
};

/**
 * @param {String} targetDate the target date
 * @param {Object} targetEmployee the target employee
 */
export const switchProxyEmployee = (
  targetDate: string,
  targetEmployee: { id: string },
  userPermission: Permission,
  userSetting: UserSetting
) => async (dispatch: Dispatch<Object>): Promise<boolean> => {
  const timesheet: TimesheetFromRemote = await dispatch(
    switchSummaryPeriod(targetDate, targetEmployee.id)
  );

  const isSuccess = !isNil(timesheet);
  if (isSuccess) {
    if (isAvailableTimeTrack(userPermission, userSetting, true)) {
      await Promise.all([
        dispatch(fetchPersonalSetting(targetEmployee.id)),
        dispatch(loadDailyTimeTrackRecords(timesheet, targetEmployee.id)),
      ]);
    }
    dispatch(proxyEmployeeInfoActions.set(targetEmployee));
  } else {
    dispatch(
      catchBusinessError(
        msg().Com_Lbl_Error,
        msg().Com_Err_ProxyPermissionErrorBody,
        msg().Com_Err_ProxyPermissionErrorSolution,
        { isContinuable: true }
      )
    );
  }
  return isSuccess;
};

export const changeApproverEmployee = (targetDate: ?string = null) => async (
  dispatch: Dispatch<Object>
) => dispatch(switchSummaryPeriod(targetDate));

export default initialize;
