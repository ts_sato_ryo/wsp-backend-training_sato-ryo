import Api from '../../../../__tests__/mocks/ApiMock';
import DispatcherMock from '../../../../__tests__/mocks/DispatcherMock';

import * as TimesheetActions from '../Timesheet';
import dummyResponseResult from '../../../repositories/__tests__/mocks/response/timesheet-get--fixed-time';

describe('timesheet-pc/action-dispatchers/Timesheet', () => {
  describe('switchSummaryPeriod()', () => {
    const dispatchMock = new DispatcherMock();

    Api.setDummyResponse(
      '/att/timesheet/get',
      {
        targetDate: '2017-07-01',
        empId: null,
      },
      dummyResponseResult
    );
    Api.setDummyResponse(
      '/att/daily-time/get',
      {
        empId: null,
      },
      {}
    );

    let lastIndex = 0;

    const findIndex = (arr, type, offset = 0) => {
      if (arr.length <= offset) {
        return -1;
      }
      for (let i = offset; i < arr.length; i++) {
        const obj = arr[i];
        if (obj && obj.type && obj.type === type) {
          return i;
        }
      }
      return -1;
    };

    let logged;

    const promise = TimesheetActions.switchSummaryPeriod('2017-07-01')(
      dispatchMock.dispatch
    ).then(() => {
      logged = dispatchMock.logged;
    });

    it('打刻ウィジェットにAPI接続のレスポンス値が適用される', async () => {
      await promise;
      expect(
        logged.some(
          (o) =>
            o &&
            o.type &&
            o.type === 'COMMONS/STAMP_WIDGET/APPLY_DAILY_STAMP_TIME'
        )
      ).toBe(true);
    });

    it('ローディング表示の開始', async () => {
      expect.assertions(1);
      await promise;
      const a = findIndex(logged, 'LOADING_START', lastIndex);
      const b = findIndex(
        logged,
        'TIMESHEET-PC/CLIENT/SELECTED_PERIOD_START_DATE/SET',
        a
      );
      lastIndex = b;
      expect(a).toBeLessThan(b);
    });

    it('集計期間のセット1：UIインタラクション用', async () => {
      expect.assertions(1);
      await promise;
      const a = findIndex(
        logged,
        'TIMESHEET-PC/CLIENT/SELECTED_PERIOD_START_DATE/SET',
        lastIndex
      );
      const b = findIndex(
        logged,
        'TIMESHEET-PC/CLIENT/SELECTED_PERIOD_START_DATE/SET',
        a + 1
      );
      lastIndex = b;
      expect(a).toBeLessThan(b);
    });

    it('集計期間のセット2：API取得値にもとづく実際の期間', async () => {
      expect.assertions(1);
      await promise;
      const a = findIndex(
        logged,
        'TIMESHEET-PC/CLIENT/SELECTED_PERIOD_START_DATE/SET',
        lastIndex
      );
      const b = findIndex(
        logged,
        'TIMESHEET-PC/ENTITIES/TIMESHEET/SET_TIMESHEET_ITEMS',
        a
      );
      lastIndex = b;
      expect(a).toBeLessThan(b);
    });

    it('社員情報・日次レコードの一覧・集計期間の一覧のセット', async () => {
      expect.assertions(1);
      await promise;
      const a = findIndex(
        logged,
        'TIMESHEET-PC/ENTITIES/SET_TIMESHEET_ITEMS',
        lastIndex
      );
      const b = findIndex(logged, 'LOADING_END', a);
      lastIndex = b;
      expect(a).toBeLessThan(b);
    });

    it('ローディング表示の終了', async () => {
      expect.assertions(1);
      await promise;
      const a = findIndex(logged, 'LOADING_END', lastIndex);
      const b = findIndex(logged, 'LOADING_END', a + 1);
      expect(a).toBeGreaterThan(b);
    });
  });
});
