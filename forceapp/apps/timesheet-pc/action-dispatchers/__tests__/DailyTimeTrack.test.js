// @flow

import _ from 'lodash';
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

import * as DailyTimeTrack from '../DailyTimeTrack';
import expected from './DailyTimeTrack.expected';
import timesheet from '../../__tests__/mocks/timesheet';
import reducer from '../../modules';

import stub from '../../../repositories/__mocks__/DailyRecordRepository.data';
import DailyRecordRepository from '../../../repositories/DailyRecordRepository';

jest.mock('../../../repositories/DailyRecordRepository');

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('timesheet-pc/action-dispatchers/DailyTimeTrack', () => {
  describe('loadTimeTrackRecords()', () => {
    const mockInitialState = {
      common: {
        app: {
          loadingDepth: 0,
        },
      },
    };
    let store = {};
    beforeEach(() => {
      // Arrange
      store = mockStore(mockInitialState);
      DailyRecordRepository.mockClear();
      DailyRecordRepository.search = jest.fn(() => stub);
    });

    test('it should start loading', async () => {
      // Run
      await store.dispatch(DailyTimeTrack.loadDailyTimeTrackRecords(timesheet));

      const firstAction = _.first(store.getActions());
      const state = reducer(mockInitialState, firstAction);

      // Assert
      expect(state.common.app.loadingDepth).toBe(1);
    });
    test('it should update state of dailyTimeTrack', async () => {
      // Run
      await store.dispatch(DailyTimeTrack.loadDailyTimeTrackRecords(timesheet));
      const state = store.getActions().reduce(reducer, mockInitialState);

      // Assert
      expect(state.entities.dailyTimeTrack).toEqual(expected);
    });
    test('it should end loading', async () => {
      // Run
      await store.dispatch(DailyTimeTrack.loadDailyTimeTrackRecords(timesheet));
      const state = store.getActions().reduce(reducer, mockInitialState);

      // Assert
      expect(state.common.app.loadingDepth).toBe(0);
    });
    test('it should handle catchApiError', async () => {
      // Arrange
      DailyRecordRepository.search = jest.fn(() => {
        throw Error('TEST');
      });

      // Run
      await store.dispatch(DailyTimeTrack.loadDailyTimeTrackRecords(timesheet));
      const state = store.getActions().reduce(reducer, mockInitialState);

      // Assert
      expect(state.common.app.unexpectedError.message).toEqual('TEST');
    });
  });
});
