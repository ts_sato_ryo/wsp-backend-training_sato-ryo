/* @flow */
import {
  type DailyAttAddProcess,
  PROCESS_TYPE,
  createDailyAttAddProcess,
} from '../../commons/models/DailyAttAddProcess';
import * as DailyAttAddProcessActions from '../../commons/action-dispatchers/DailyAttAddProcess';
import { type UpdateResult as AttDailyTimeUpdateResult } from '../../repositories/AttDailyTimeRepository';

const createDailyAttTimeResultProcess = (
  targetDate: ?string,
  employeeId: ?string,
  result: AttDailyTimeUpdateResult
): ?DailyAttAddProcess => {
  if (!result) {
    return null;
  }

  if (Number(result.insufficientRestTime) > 0) {
    return createDailyAttAddProcess(PROCESS_TYPE.INSUFFICIENT_REST_TIME, {
      targetDate,
      employeeId,
      insufficientRestTime: Number(result.insufficientRestTime),
    });
  }

  return null;
};

/**
 * 休憩時間が足りていない場合にダイアログを表示する
 */
// eslint-disable-next-line import/prefer-default-export
export const doAddProcess = (
  targetDate: ?string,
  targetEmployeeId: ?string,
  result: AttDailyTimeUpdateResult
) => (dispatch: Dispatch<any>) => {
  const process = createDailyAttTimeResultProcess(
    targetDate,
    targetEmployeeId,
    result
  );
  return dispatch(DailyAttAddProcessActions.doAddProcess(process));
};
