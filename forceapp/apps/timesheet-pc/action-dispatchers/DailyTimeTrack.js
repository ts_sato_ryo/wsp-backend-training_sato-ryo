// @flow

import { type Dispatch } from 'redux';
import type { TimesheetFromRemote } from '../../domain/models/attendance/Timesheet';
import Repository from '../../repositories/DailyRecordRepository';
import { catchApiError, withLoading } from '../../commons/actions/app';
import { actions as dailyTimeTrackActions } from '../modules/entities/dailyTimeTrack';

export const loadDailyTimeTrackRecords = (
  timesheet: TimesheetFromRemote,
  empId?: string
) => async (dispatch: Dispatch<Object>) => {
  try {
    await dispatch(
      withLoading(async () => {
        const records = await Repository.search({
          empId,
          startDate: timesheet.startDate,
          endDate: timesheet.endDate,
        });
        dispatch(dailyTimeTrackActions.fetchSuccess(records, timesheet));
      })
    );
  } catch (e) {
    dispatch(catchApiError(e));
  }
};

export const loadDailyTimeTrackRecordsBetweenPeriods = (
  period: { startDate: string, endDate: string },
  timesheet: TimesheetFromRemote,
  empId?: string
) => async (dispatch: Dispatch<Object>) => {
  try {
    await dispatch(
      withLoading(async () => {
        const records = await Repository.search({
          empId,
          ...period,
        });
        dispatch(dailyTimeTrackActions.updateRecords(records, timesheet));
      })
    );
  } catch (e) {
    dispatch(catchApiError(e));
  }
};
