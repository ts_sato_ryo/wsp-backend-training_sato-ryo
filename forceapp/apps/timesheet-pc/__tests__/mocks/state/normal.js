import {
  requestTypes,
  generateAttDailyRequestObjectMap,
  generateAttSummaryObject,
  generateAttRecordObjectList,
} from '../../../../repositories/__tests__/mocks/helpers/timesheet';
import * as AttDailyRequest from '../../../../domain/models/attendance/AttDailyRequest';
import AttSummary from '../../../models/AttSummary';
import AttRecord from '../../../models/AttRecord';

const requests = generateAttDailyRequestObjectMap();
const attDailyRequestTypeMap = Object.keys(requestTypes).reduce((hash, key) => {
  hash[key] = requestTypes[key];
  return hash;
}, {});

export default {
  common: {
    accessControl: {
      permission: {},
    },
    proxyEmployeeInfo: {
      isProxyMode: false,
    },
  },
  entities: {
    timesheet: {
      attSummary: new AttSummary(generateAttSummaryObject()),
      attRecordList: generateAttRecordObjectList().map(
        (attRecord) => new AttRecord(attRecord)
      ),
      attDailyRequestTypeMap,
      attDailyRequestMap: Object.keys(requests).reduce((hash, key) => {
        const request = requests[key];
        hash[key] = AttDailyRequest.createFromRemote(
          attDailyRequestTypeMap,
          request
        );
        return hash;
      }, {}),
    },
  },
};
