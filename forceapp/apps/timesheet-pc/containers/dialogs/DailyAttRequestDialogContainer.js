// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { createSelector } from 'reselect';

import DailyAttRequestDialog from '../../components/dialogs/DailyAttRequestDialog';

import * as targetDateSelectors from '../../modules/selectors';
import { actions as approvalHistoryActions } from '../../modules/ui/approvalHistory';
import * as dailyRequestSelectors from '../../modules/ui/dailyRequest/selector';
import { DIALOG_TYPE } from '../../../commons/modules/approverEmployeeSearch/ui/dialog';

import * as DailyRequestActions from '../../action-dispatchers/DailyRequest';
import * as ApproverEmployeeSettingActions from '../../../commons/action-dispatchers/ApproverEmployeeSetting';

import { type State } from '../../modules';
import { type State as TimesheetState } from '../../modules/entities/timesheet';

const selectUserSetting = (state: State) => state.common.userSetting;
const selectProxyEmployeeInfo = (state: State) =>
  state.common.proxyEmployeeInfo;
const selectRequestTargetDate = (state: State) =>
  state.ui.dailyRequest.targetDate;
const selectEditing = (state) => state.ui.dailyRequest.editing;
const selectTargetRequest = (state: State) =>
  dailyRequestSelectors.targetRequest(state.ui.dailyRequest);
const selectAttRecordList = (state: State) =>
  (state.entities.timesheet: TimesheetState).attRecordList;

const selectAttRecord = createSelector(
  selectRequestTargetDate,
  selectAttRecordList,
  (targetDate, attRecordList) => {
    return targetDate && attRecordList
      ? attRecordList.find((attRecord) => attRecord.recordDate === targetDate)
      : null;
  }
);

const selectDailyAttRequestApproverEmployee = createSelector(
  selectEditing,
  selectTargetRequest,
  selectAttRecord,
  (editing, request, attRecord) => {
    if (!editing.isEditing && editing.id && request) {
      return {
        id: '',
        employeeName: request.approver01Name,
      };
    } else if (attRecord) {
      return {
        id: '',
        employeeName: attRecord.approver01Name,
      };
    } else {
      return null;
    }
  }
);

const selectCanEditApproverEmployee = createSelector(
  selectUserSetting,
  selectProxyEmployeeInfo,
  selectEditing,
  (userSetting, proxyEmployeeInfo, editing) => {
    return (
      userSetting.allowToChangeApproverSelf &&
      !proxyEmployeeInfo.isProxyMode &&
      editing.isEditing
    );
  }
);

type OwnProps = {};

const mapStateToProps = (state: State) => {
  const requestConditions = targetDateSelectors.selectSelectedRequestConditions(
    state
  );

  return {
    editing: selectEditing(state),
    canEditApproverEmployee: selectCanEditApproverEmployee(state),
    approverEmployee: selectDailyAttRequestApproverEmployee(state),
    requestConditions,
    targetDate: selectRequestTargetDate(state),
    targetRequest: selectTargetRequest(state),
    attDailyRequestTypeMap: (state.entities.timesheet: TimesheetState)
      .attDailyRequestTypeMap,
    attDailyRequestMap: (state.entities.timesheet: TimesheetState)
      .attDailyRequestMap,
    attWorkingType: (state.entities.timesheet: TimesheetState).attWorkingType,
    selectedPeriodStartDate: state.client.selectedPeriodStartDate,
    isProxyMode: state.common.proxyEmployeeInfo.isProxyMode,
    proxyEmployeeId: state.common.proxyEmployeeInfo.id,
    userSetting: state.common.userSetting,
    userPermission: state.common.accessControl.permission,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<Object>) =>
  bindActionCreators(
    {
      onClickRequestDetailButton: DailyRequestActions.showRequestDetailPane,
      onClickRequestEntryButton: DailyRequestActions.showEntryRequestPane,
      onStartEditing: DailyRequestActions.startEditing,
      onCancelEditing: DailyRequestActions.cancelEditing,
      onSubmitRequest: DailyRequestActions.submit,
      onDisableRequest: DailyRequestActions.disable,
      onCancel: DailyRequestActions.hideManagementDialog,
      onClickOpenApprovalHistoryDialog: approvalHistoryActions.open,
      onClickOpenApproverEmployeeSettingDialog:
        ApproverEmployeeSettingActions.showDialog,
    },
    dispatch
  );

const mergeProps = (stateProps: Object, dispatchProps: Object) => ({
  ...stateProps,
  ...dispatchProps,
  onClickRequestEntryButton: (requestType) => {
    dispatchProps.onClickRequestEntryButton(
      stateProps.targetDate,
      requestType,
      stateProps.attDailyRequestTypeMap,
      stateProps.attWorkingType,
      (stateProps.isProxyMode && stateProps.proxyEmployeeId) || null
    );
  },
  onStartEditing: () => {
    dispatchProps.onStartEditing(
      stateProps.targetRequest,
      stateProps.attWorkingType,
      stateProps.proxyEmployeeId
    );
  },
  onCancelEditing: () => {
    const { id } = stateProps.editing;
    const { attDailyRequestMap } = stateProps;

    if (!id || !attDailyRequestMap[id]) {
      return;
    }

    dispatchProps.onCancelEditing(attDailyRequestMap[id]);
  },
  onSubmitRequest: () => {
    dispatchProps.onSubmitRequest(
      stateProps.targetRequest,
      stateProps.editing.editAction,
      stateProps.selectedPeriodStartDate,
      stateProps.proxyEmployeeId,
      stateProps.userSetting,
      stateProps.userPermission
    );
  },
  onDisableRequest: () => {
    dispatchProps.onDisableRequest(
      stateProps.targetRequest,
      stateProps.editing.disableAction,
      stateProps.selectedPeriodStartDate,
      stateProps.proxyEmployeeId,
      stateProps.userSetting,
      stateProps.userPermission
    );
  },
  onClickOpenApprovalHistoryDialog: () => {
    dispatchProps.onClickOpenApprovalHistoryDialog(stateProps.editing.id);
  },
  onClickOpenApproverEmployeeSettingDialog: () => {
    dispatchProps.onClickOpenApproverEmployeeSettingDialog(
      stateProps.approverEmployee,
      stateProps.targetDate,
      !stateProps.canEditApproverEmployee,
      DIALOG_TYPE.AttDailyRequest
    );
  },
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(
  DailyAttRequestDialog
): React.ComponentType<OwnProps>): React.ComponentType<Object>);
