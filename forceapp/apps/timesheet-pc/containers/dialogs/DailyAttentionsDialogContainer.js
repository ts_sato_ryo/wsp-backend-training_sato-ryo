/* @flow */
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import DailyAttentionsDialog from '../../components/dialogs/DailyAttentionsDialog';
import * as DailyRemarksActions from '../../action-dispatchers/DailyAttentions';

const mapStateToProps = (state: any) => ({
  isHide: !state.ui.dailyAttentions.messages.length,
  messages: state.ui.dailyAttentions.messages,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) =>
  bindActionCreators(
    {
      onHide: DailyRemarksActions.hideDailyAttentionsDialog,
    },
    dispatch
  );

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(DailyAttentionsDialog): React.ComponentType<Object>);
