// @flow
import { compose } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../../modules/ui/dailyRequest/requests/earlyStartWorkRequest';

import Component from '../../../components/dialogs/DailyAttRequestDialog/FormForEarlyStartWork';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    targetRequest: state.ui.dailyRequest.requests.earlyStartWorkRequest.request,
  };
};

const mapDispatchToProps = {
  onUpdateValue: actions.update,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component): React.ComponentType<Object>);
