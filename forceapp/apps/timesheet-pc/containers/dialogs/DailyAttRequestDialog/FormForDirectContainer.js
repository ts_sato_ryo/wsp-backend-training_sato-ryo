// @flow
import { compose } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../../modules/ui/dailyRequest/requests/directRequest';

import Component from '../../../components/dialogs/DailyAttRequestDialog/FormForDirect';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    targetRequest: state.ui.dailyRequest.requests.directRequest.request,
    hasRange: state.ui.dailyRequest.requests.directRequest.hasRange,
  };
};

const mapDispatchToProps = {
  onUpdateValue: actions.update,
  onUpdateHasRange: actions.updateHasRange,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component): React.ComponentType<Object>);
