// @flow
import { compose } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../../modules/ui/dailyRequest/requests/holidayWorkRequest';

import Component from '../../../components/dialogs/DailyAttRequestDialog/FormForHolidayWork';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    targetRequest: state.ui.dailyRequest.requests.holidayWorkRequest.request,
    substituteLeaveTypeList:
      state.ui.dailyRequest.requests.holidayWorkRequest.substituteLeaveTypeList,
  };
};

const mapDispatchToProps = {
  onUpdateValue: actions.update,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component): React.ComponentType<Object>);
