// @flow
import { compose } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../../modules/ui/dailyRequest/requests/leaveRequest';

import Component from '../../../components/dialogs/DailyAttRequestDialog/FormForLeave';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    targetRequest: state.ui.dailyRequest.requests.leaveRequest.request,
    attLeaveList: state.ui.dailyRequest.requests.leaveRequest.attLeaveList,
    selectedAttLeave:
      state.ui.dailyRequest.requests.leaveRequest.selectedAttLeave,
    hasRange: state.ui.dailyRequest.requests.leaveRequest.hasRange,
  };
};

const mapDispatchToProps = {
  onUpdateValue: actions.update,
  onUpdateHasRange: actions.updateHasRange,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component): React.ComponentType<Object>);
