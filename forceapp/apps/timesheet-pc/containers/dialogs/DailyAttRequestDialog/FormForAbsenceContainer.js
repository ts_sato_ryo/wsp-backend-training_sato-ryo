// @flow
import { compose } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../../modules/ui/dailyRequest/requests/absenceRequest';

import Component from '../../../components/dialogs/DailyAttRequestDialog/FormForAbsence';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    targetRequest: state.ui.dailyRequest.requests.absenceRequest.request,
    hasRange: state.ui.dailyRequest.requests.absenceRequest.hasRange,
  };
};

const mapDispatchToProps = {
  onUpdateValue: actions.update,
  onUpdateHasRange: actions.updateHasRange,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component): React.ComponentType<Object>);
