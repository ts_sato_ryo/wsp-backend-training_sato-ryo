// @flow
import { compose } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../../modules/ui/dailyRequest/requests/patternRequest';

import Component from '../../../components/dialogs/DailyAttRequestDialog/FormForPattern';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    targetRequest: state.ui.dailyRequest.requests.patternRequest.request,
    attPatternList:
      state.ui.dailyRequest.requests.patternRequest.attPatternList,
    selectedAttPattern:
      state.ui.dailyRequest.requests.patternRequest.selectedAttPattern,
    hasRange: state.ui.dailyRequest.requests.patternRequest.hasRange,
  };
};

const mapDispatchToProps = {
  onUpdateValue: actions.update,
  onUpdateHasRange: actions.updateHasRange,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component): React.ComponentType<Object>);
