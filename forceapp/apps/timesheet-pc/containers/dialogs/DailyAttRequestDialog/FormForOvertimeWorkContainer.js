// @flow
import { compose } from 'redux';
import { connect } from 'react-redux';

import { actions } from '../../../modules/ui/dailyRequest/requests/overtimeWorkRequest';

import Component from '../../../components/dialogs/DailyAttRequestDialog/FormForOvertimeWork';

const mapStateToProps = (state, ownProps) => {
  return {
    ...ownProps,
    targetRequest: state.ui.dailyRequest.requests.overtimeWorkRequest.request,
  };
};

const mapDispatchToProps = {
  onUpdateValue: actions.update,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component): React.ComponentType<Object>);
