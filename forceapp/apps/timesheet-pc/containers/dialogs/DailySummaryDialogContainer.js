// @flow

import * as React from 'react';
import { bindActionCreators, type Dispatch } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import DailySummaryDialog from '../../../commons/components/dialogs/DailySummaryDialog';
import type { DailySummaryTask } from '../../../domain/models/time-management/DailySummaryTask';
import type { BaseEvent } from '../../../commons/models/DailySummary/BaseEvent';
import type { PersonalSetting } from '../../../commons/modules/personalSetting';
import type { Job } from '../../../domain/models/time-tracking/Job';
import type { State } from '../../modules';
import type { State as Entities } from '../../modules/entities';
import type { State as DailySummaryState } from '../../modules/ui/dailySummary';
import * as DailySummaryActions from '../../action-dispatchers/DailySummary';

type OwnProps = $ReadOnly<{||}>;

const mapStateToProps = (state: State) => ({
  userPermission: state.common.accessControl.permission,
  isProxyMode: state.common.proxyEmployeeInfo.isProxyMode,
  proxyEmployeeId: state.common.proxyEmployeeInfo.id,

  selectedDay: moment((state.ui.dailySummary: DailySummaryState).targetDate),
  events: (state.entities: Entities).events,
  taskList: (state.ui.dailySummary: DailySummaryState).taskList,
  /*
    eventListPopupIsOpen: state.ui.dailySummary.eventListPopup.isOpen,
    eventListPopupDay: state.ui.dailySummary.eventListPopup.day,
    eventListPopupEvents: state.ui.dailySummary.eventListPopup.events,
    eventListPopupStyle: state.ui.dailySummary.eventListPopup.style,
    */
  note: (state.ui.dailySummary: DailySummaryState).note,
  timestampComment: (state.ui.dailySummary: DailySummaryState).timestampComment,
  workCategoryLists: (state.ui.dailySummary: DailySummaryState).workCategoryMap,
  defaultJobId: (state.ui.dailySummary: DailySummaryState).defaultJobId,
  realWorkTime: (state.ui.dailySummary: DailySummaryState).realWorkTime,
  isTemporaryWorkTime: (state.ui.dailySummary: DailySummaryState)
    .isTemporaryWorkTime,
  /* TODO 工数確定が実装されたら対応する */
  isLocked: state.common.proxyEmployeeInfo.isProxyMode,
  isEnableEndStamp: (state.ui.dailySummary: DailySummaryState).isEnableEndStamp,
  plannerDefaultView: '',
  opening: (state.ui.dailySummary: DailySummaryState).isOpened,
  isOpeningJobSelectDialog: (state.ui.dailySummary: DailySummaryState)
    .isJobSelectDialogOpened,
  jobTree: state.entities.jobs,
  personalSetting: state.common.personalSetting,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) =>
  bindActionCreators(
    {
      openEventListPopup: (
        _e: SyntheticEvent<HTMLButtonElement>,
        _day: moment,
        _events: BaseEvent[]
      ) => {},
      moveDay: (
        date: moment,
        personalSetting: PersonalSetting,
        empId?: string
      ) =>
        DailySummaryActions.openDailySummary(
          date.format('YYYY-MM-DD'),
          personalSetting,
          empId
        ),
      saveDailySummary: (
        taskList: DailySummaryTask[],
        note: string,
        output: string,
        selectedDay: moment
      ) =>
        DailySummaryActions.saveDailySummary(
          taskList,
          note,
          output,
          selectedDay
        ),
      saveDailySummaryAndRecordEndTimestamp: (
        taskList: DailySummaryTask[],
        note: string,
        comment: string,
        output: string,
        selectedDay: moment
      ) =>
        DailySummaryActions.saveDailySummaryAndLeaveWork(
          taskList,
          note,
          comment,
          output,
          selectedDay
        ),
      hideDialog: DailySummaryActions.closeDailySummary,
      deleteTask: DailySummaryActions.deleteTask,
      editTask: DailySummaryActions.editTask,
      editTaskTime: DailySummaryActions.editTask,
      editNote: DailySummaryActions.editNote,
      editTimestampComment: DailySummaryActions.editTimestampComment,
      toggleDirectInput: DailySummaryActions.toggleTask,
      reorderTaskList: DailySummaryActions.reorderTask,
      fetchJobTree: DailySummaryActions.selectJobOrFetchJobTree,
      showJobSelectDialog: DailySummaryActions.openJobSelectDialog,
      closeJobSelectDialog: DailySummaryActions.closeJobSelectDialog,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps: OwnProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  moveDay: (date: moment) =>
    dispatchProps.moveDay(
      date,
      stateProps.personalSetting,
      stateProps.proxyEmployeeId
    ),
  saveDailySummary: () =>
    dispatchProps.saveDailySummary(
      stateProps.taskList,
      stateProps.note,
      '',
      stateProps.selectedDay
    ),
  saveDailySummaryAndRecordEndTimestamp: () =>
    dispatchProps.saveDailySummaryAndRecordEndTimestamp(
      stateProps.taskList,
      stateProps.note,
      stateProps.timestampComment,
      '',
      stateProps.selectedDay
    ),
  fetchJobTree: (parent: Job, parentLevelItems: Job[][]) =>
    dispatchProps.fetchJobTree(
      stateProps.selectedDay.format('YYYY-MM-DD'),
      parent,
      parentLevelItems
    ),
  showJobSelectDialog: () =>
    dispatchProps.showJobSelectDialog(
      stateProps.selectedDay.format('YYYY-MM-DD')
    ),
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(
  DailySummaryDialog
): React.ComponentType<OwnProps>): React.ComponentType<Object>);
