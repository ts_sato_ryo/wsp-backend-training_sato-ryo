import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import DailyRemarksDialog from '../../components/dialogs/DailyRemarksDialog';

import * as DailyRemarksActions from '../../action-dispatchers/DailyRemarks';
import { actions as editingDailyRemarksActions } from '../../modules/ui/editingDailyRemarks';
import { isTimesheetReadOnly } from '../../modules/selectors';

const mapStateToProps = (state) => ({
  isProxyMode: state.common.proxyEmployeeInfo.isProxyMode,
  proxyEmployeeId: state.common.proxyEmployeeInfo.id,
  selectedPeriodStartDate: state.client.selectedPeriodStartDate,
  dailyRemarks: state.ui.editingDailyRemarks,
  isReadOnly: isTimesheetReadOnly(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onUpdateValue: editingDailyRemarksActions.update,
      onSubmit: DailyRemarksActions.save,
      onCancel: DailyRemarksActions.hideDialog,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onSubmit: () => {
    dispatchProps.onSubmit(
      stateProps.dailyRemarks,
      stateProps.selectedPeriodStartDate,
      stateProps.isProxyMode ? stateProps.proxyEmployeeId : null
    );
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DailyRemarksDialog);
