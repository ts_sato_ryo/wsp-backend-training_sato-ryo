// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import isNil from 'lodash/isNil';

import { actions as proxyEmployeeInfoActions } from '../../commons/modules/proxyEmployeeInfo';
import * as AppActions from '../action-dispatchers/App';
import * as TimesheetActions from '../action-dispatchers/Timesheet';
import * as DailyAttTime from '../action-dispatchers/DailyAttTime';
import * as DailyRequest from '../action-dispatchers/DailyRequest';
import * as FixSummaryRequestActions from '../action-dispatchers/FixSummaryRequest';
import * as DailyRemarksActions from '../action-dispatchers/DailyRemarks';
import * as DailyAttentionsActions from '../action-dispatchers/DailyAttentions';
import * as DailySummaryActions from '../action-dispatchers/DailySummary';
import * as entitiesSelectors from '../modules/selectors';
import { actions as stampWidgetActions } from '../modules/ui/stampWidget';
// import { actions as timesheetActions } from '../modules/ui/timesheet';
import { actions as approvalHistoryActions } from '../modules/ui/approvalHistory';

import { type State } from '../modules';
import { type State as TimesheetState } from '../modules/entities/timesheet';
import { type State as DailyTimeTrackState } from '../modules/entities/dailyTimeTrack';

import MainContent from '../components/MainContent';

type OwnProps = {};

const mapStateToProps = (state: State) => ({
  language: state.common.userSetting.language,
  isProxyMode: state.common.proxyEmployeeInfo.isProxyMode,
  proxyEmployeeId: state.common.proxyEmployeeInfo.id,
  selectedPeriodStartDate: state.client.selectedPeriodStartDate,
  employee: (state.entities.timesheet: TimesheetState).employee,
  summaryPeriodList: (state.entities.timesheet: TimesheetState)
    .summaryPeriodList,
  dailyContractedDetailMap: entitiesSelectors.buildDailyContractedDetailMap(
    state
  ),
  dailyRequestedWorkingHoursMap: entitiesSelectors.buildDailyRequestedWorkingHoursMap(
    state
  ),
  dailyActualWorkingPeriodListMap: entitiesSelectors.buildDailyActualWorkingPeriodListMap(
    state
  ),
  dailyRequestConditionsMap: entitiesSelectors.buildDailyRequestConditionMap(
    state
  ),
  dailyAttentionMessagesMap: entitiesSelectors.buildDailyAttentionMessagesMap(
    state
  ),
  dailyTimeTrackMap: (state.entities.dailyTimeTrack: DailyTimeTrackState),
  attRecordList: (state.entities.timesheet: TimesheetState).attRecordList,
  attSummary: (state.entities.timesheet: TimesheetState).attSummary,
  isStampWidgetOpened: state.ui.stampWidget.isOpened,
  isManHoursGraphOpened: state.ui.timesheet.isManHoursGraphOpened,
  userPermission: state.common.accessControl.permission,
  userSetting: state.common.userSetting,
  personalSetting: state.common.personalSetting,
  standalone: state.common.standaloneMode.enabled,
});

const mapDispatchToProps = (dispatch: Dispatch<Object>) =>
  bindActionCreators(
    {
      onStampSuccess: AppActions.initialize,
      onClickStampWidgetToggle: stampWidgetActions.toggle,
      onPeriodSelected: TimesheetActions.loadTimesheetOnPeriod,
      // onClickToggleManHoursGraph: timesheetActions.toggleManHoursGraph,
      onClickTimesheetRequestButton: DailyRequest.showManagementDialog,
      onClickTimesheetTimeButton: DailyAttTime.showDialog,
      onClickTimesheetRemarksButton: DailyRemarksActions.showDialog,
      onClickFixSummaryRequestButton:
        FixSummaryRequestActions.manipulateFixRequestAccordingToAttSummary,
      onClickOpenAttRequestHistoryButton: approvalHistoryActions.open,
      onClickTimesheetAttentionsButton:
        DailyAttentionsActions.showDailyAttentionsDialog,
      onClickOpenDailySummaryButton: DailySummaryActions.openDailySummary,
      onExitProxyMode: (selectedPeriodStartDate) => () => {
        dispatch(proxyEmployeeInfoActions.unset());
        dispatch(TimesheetActions.switchSummaryPeriod(selectedPeriodStartDate));
      },
      onRequestOpenSummaryWindow: TimesheetActions.openSummaryWindow,
      onRequestOpenLeaveWindow: TimesheetActions.openLeaveWindow,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onStampSuccess: () => {
    dispatchProps.onStampSuccess({ userPermission: stateProps.userPermission });
  },
  onPeriodSelected: (periodStartDate) => {
    dispatchProps.onPeriodSelected(
      periodStartDate,
      stateProps.isProxyMode ? stateProps.proxyEmployeeId : null,
      stateProps.userPermission,
      stateProps.userSetting,
      stateProps.isProxyMode
    );
  },
  onClickFixSummaryRequestButton: () => {
    dispatchProps.onClickFixSummaryRequestButton(
      stateProps.attSummary,
      stateProps.dailyRequestConditionsMap
    );
  },
  onClickOpenAttRequestHistoryButton: () => {
    dispatchProps.onClickOpenAttRequestHistoryButton(
      isNil(stateProps.attSummary) ? '' : stateProps.attSummary.requestId
    );
  },
  onExitProxyMode: () => {
    dispatchProps.onExitProxyMode(stateProps.selectedPeriodStartDate);
  },
  onRequestOpenSummaryWindow: () => {
    dispatchProps.onRequestOpenSummaryWindow(
      stateProps.selectedPeriodStartDate,
      stateProps.isProxyMode ? stateProps.proxyEmployeeId : null
    );
  },
  onRequestOpenLeaveWindow: () => {
    dispatchProps.onRequestOpenLeaveWindow(
      stateProps.selectedPeriodStartDate,
      stateProps.isProxyMode ? stateProps.proxyEmployeeId : null
    );
  },
  onClickOpenDailySummaryButton: (targetDate: string) =>
    dispatchProps.onClickOpenDailySummaryButton(
      targetDate,
      stateProps.personalSetting,
      stateProps.proxyEmployeeId
    ),
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(MainContent): React.ComponentType<OwnProps>): React.ComponentType<Object>);
