// @flow

import { bindActionCreators, type Dispatch } from 'redux';
import { connect } from 'react-redux';

import { type $ExtractReturn } from '../../commons/utils/TypeUtil';
import { type State } from '../modules';

import {
  switchProxyEmployee,
  changeApproverEmployee,
} from '../action-dispatchers/App';

import App from '../components/App';

const mapStateToProps = (state: State) => ({
  isProxyMode: state.common.proxyEmployeeInfo.isProxyMode,
  userPermission: state.common.accessControl.permission,

  // Note: These props are used in props merge
  selectedPeriodStartDate: state.client.selectedPeriodStartDate,
  proxyEmployee: state.common.proxyEmployeeInfo,
  userSetting: state.common.userSetting,
});

const mapDispatchToProps = (dispatch: Dispatch<Object>) =>
  bindActionCreators(
    {
      onDecideProxyEmployee: switchProxyEmployee,
      onChangeApproverEmployee: changeApproverEmployee,
    },
    dispatch
  );

const mergeProps = (
  stateProps: $ExtractReturn<typeof mapStateToProps>,
  dispatchProps
) => ({
  ...stateProps,
  ...dispatchProps,
  onDecideProxyEmployee: (targetEmployee) =>
    dispatchProps.onDecideProxyEmployee(
      stateProps.selectedPeriodStartDate,
      targetEmployee,
      stateProps.userPermission,
      stateProps.userSetting
    ),
  onChangeApproverEmployee: () => {
    if (stateProps.proxyEmployee && stateProps.proxyEmployee.id) {
      return;
    }
    dispatchProps.onChangeApproverEmployee(stateProps.selectedPeriodStartDate);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(App): React.ComponentType<Object>);
