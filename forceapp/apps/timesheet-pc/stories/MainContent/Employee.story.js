import React from 'react';
import { storiesOf } from '@storybook/react';

import Employee from '../../components/MainContent/Employee';

import employee from '../mock-data/employee';

storiesOf('timesheet-pc', module).add(
  'Employee',
  () => <Employee employee={employee} />,
  { info: { propTables: [Employee], inline: true, source: true } }
);
