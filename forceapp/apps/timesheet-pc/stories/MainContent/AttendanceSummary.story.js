import React from 'react';
import { storiesOf } from '@storybook/react';

import AttendanceSummary from '../../components/MainContent/AttendanceSummary';

storiesOf('timesheet-pc', module).add(
  'AttendanceSummary',
  () => <AttendanceSummary />,
  { info: { propTables: [AttendanceSummary], inline: true, source: true } }
);
