import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import SummaryPeriodNav from '../../components/MainContent/SummaryPeriodNav';
import periodList from '../mock-data/summaryPeriodList';

storiesOf('timesheet-pc', module)
  .add(
    'SummaryPeriodNav',
    () => (
      <SummaryPeriodNav
        summaryPeriodList={periodList}
        selectedPeriodStartDate="2017-08-01"
        onPeriodSelected={action('集計期間選択')}
      />
    ),
    { info: { propTables: [SummaryPeriodNav], inline: true, source: true } }
  )
  .add(
    'SummaryPeriodNav - 最も古い期間が選択されている場合（入社月）',
    () => (
      <SummaryPeriodNav
        summaryPeriodList={periodList}
        selectedPeriodStartDate="2016-08-01"
        onPeriodSelected={action('集計期間選択')}
      />
    ),
    { info: { propTables: [SummaryPeriodNav], inline: true, source: true } }
  )
  .add(
    'SummaryPeriodNav - 最も新しい期間が選択されている場合（退職月）',
    () => (
      <SummaryPeriodNav
        summaryPeriodList={periodList}
        selectedPeriodStartDate="2017-10-01"
        onPeriodSelected={action('集計期間選択')}
      />
    ),
    { info: { propTables: [SummaryPeriodNav], inline: true, source: true } }
  );
