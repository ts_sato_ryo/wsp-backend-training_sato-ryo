// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, number } from '@storybook/addon-knobs';
import DailySummaryButton from '../../../components/MainContent/Timesheet/DailySummaryButton';

storiesOf('timesheet-pc', module)
  .addDecorator((story) => <div style={{ padding: '32px' }}>{story()}</div>)
  .addDecorator(withKnobs)
  .add(
    'Timesheet/DailySummaryButton',
    () => (
      <DailySummaryButton
        workTime={number('workTime', null)}
        totalTaskTime={number('totalTaskTime', null)}
        onClick={action('onClick')}
      />
    ),
    {
      info: { propTables: [DailySummaryButton], inline: true, source: true },
    }
  );
