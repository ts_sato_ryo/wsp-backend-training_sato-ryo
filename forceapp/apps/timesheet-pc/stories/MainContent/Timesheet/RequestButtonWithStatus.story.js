import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import RequestButtonWithStatus from '../../../components/MainContent/Timesheet/RequestButtonWithStatus';

import {
  dailyRequestConditionsHasRejected,
  dailyRequestConditionsHasApprovalIn,
  dailyRequestConditionsHasApproved,
  dailyRequestConditionsHasNoRequests,
} from '../../mock-data/dailyRequestConditions';

storiesOf('timesheet-pc', module)
  .add(
    'Timesheet/RequestButtonWithStatus',
    () => (
      <RequestButtonWithStatus
        requestConditions={dailyRequestConditionsHasNoRequests}
        onClick={action('Clicked')}
      />
    ),
    {
      info: {
        propTables: [RequestButtonWithStatus],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Timesheet/RequestButtonWithStatus - ステータス表示の優先度 1: 却下',
    () => (
      <RequestButtonWithStatus
        requestConditions={dailyRequestConditionsHasRejected}
        onClick={action('Clicked')}
      />
    ),
    {
      info: {
        propTables: [RequestButtonWithStatus],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Timesheet/RequestButtonWithStatus - ステータス表示の優先度 2: 承認待ち',
    () => (
      <RequestButtonWithStatus
        requestConditions={dailyRequestConditionsHasApprovalIn}
        onClick={action('Clicked')}
      />
    ),
    {
      info: {
        propTables: [RequestButtonWithStatus],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Timesheet/RequestButtonWithStatus - ステータス表示の優先度 3: 承認済み',
    () => (
      <RequestButtonWithStatus
        requestConditions={dailyRequestConditionsHasApproved}
        onClick={action('Clicked')}
      />
    ),
    {
      info: {
        propTables: [RequestButtonWithStatus],
        inline: true,
        source: true,
      },
    }
  );
