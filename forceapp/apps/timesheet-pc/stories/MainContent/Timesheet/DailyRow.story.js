import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DailyRow from '../../../components/MainContent/Timesheet/DailyRow';

import attRecordList from '../../mock-data/attRecordList';
import dailyTimeTrackList from '../../mock-data/dailyTimeTrackList';
import { dailyRequestConditionsMap } from '../../mock-data/dailyRequestConditions';

import AttRecord from '../../../models/AttRecord';
import STATUS from '../../../../domain/models/approval/request/Status';
import { create as createLeaveRequest } from '../../../../domain/models/attendance/AttDailyRequest/LeaveRequest';
import DailyRequestConditions from '../../../models/DailyRequestConditions';

const DummyTable = (props) => (
  <table
    style={{
      boxSizing: 'border-box',
      position: 'relative',
      width: '100%',
      minWidth: '1024px',
      borderCollapse: 'separate',
      transition: 'height 0.3s ease-out',
    }}
  >
    <tbody>{props.children}</tbody>
  </table>
);

DummyTable.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

storiesOf('timesheet-pc', module)
  .add(
    'Timesheet/DailyRow',
    () => (
      <DummyTable>
        {attRecordList.map((attRecord, index) => (
          <DailyRow
            key={index}
            attRecord={attRecord}
            dailyTimeTrack={dailyTimeTrackList[attRecord.recordDate]}
            requestConditions={dailyRequestConditionsMap[attRecord.recordDate]}
            onClickRequestButton={action('Click Request button')}
            onClickAttentionsButton={action('Click Attentions button')}
            attentionMessages={(() => {
              if (index === 2) {
                return ['Attention message text here.'];
              } else if (index === 5) {
                return [
                  'Attention message text 1 here.',
                  'Attention message text 2 here.',
                ];
              }
              return null;
            })()}
          />
        ))}
      </DummyTable>
    ),
    { info: { propTables: [DailyRow], inline: true, source: true } }
  )
  .add(
    'Timesheet/DailyRow/休暇タイプ - 午前：なし, 午後：なし',
    () => {
      const attRecord = new AttRecord({
        recordDate: '2017-07-03',
        dayType: AttRecord.DAY_TYPE.WORKDAY,
      });
      return (
        <DummyTable>
          <DailyRow
            attRecord={attRecord}
            requestConditions={DailyRequestConditions.createFromParams(
              attRecord,
              {},
              {},
              {}
            )}
          />
        </DummyTable>
      );
    },
    { info: { propTables: [DailyRow], inline: true, source: true } }
  )
  .add(
    'Timesheet/DailyRow/休暇タイプ - 午前：有給, 午後：なし',
    () => {
      const attRecord = new AttRecord({
        recordDate: '2017-07-03',
        dayType: AttRecord.DAY_TYPE.WORKDAY,
        requestIds: ['dummy1'],
      });
      return (
        <DummyTable>
          <DailyRow
            attRecord={attRecord}
            requestConditions={DailyRequestConditions.createFromParams(
              attRecord,
              {
                dummy1: createLeaveRequest({
                  requestTypeCode: 'Leave',
                  status: STATUS.Approved,
                  range: 'AM',
                  startTime: '10:00',
                  endTime: '14:00',
                  leaveType: 'Paid',
                }),
              },
              {},
              {}
            )}
          />
        </DummyTable>
      );
    },
    { info: { propTables: [DailyRow], inline: true, source: true } }
  )
  .add(
    'Timesheet/DailyRow/休暇タイプ - 午前：無給, 午後：なし',
    () => {
      const attRecord = new AttRecord({
        recordDate: '2017-07-03',
        dayType: AttRecord.DAY_TYPE.WORKDAY,
        requestIds: ['dummy1'],
      });
      return (
        <DummyTable>
          <DailyRow
            attRecord={attRecord}
            requestConditions={DailyRequestConditions.createFromParams(
              attRecord,
              {
                dummy1: createLeaveRequest({
                  requestTypeCode: 'Leave',
                  status: STATUS.Approved,
                  range: 'AM',
                  startTime: '10:00',
                  endTime: '14:00',
                  leaveType: 'Unpaid',
                }),
              },
              {},
              {}
            )}
          />
        </DummyTable>
      );
    },
    { info: { propTables: [DailyRow], inline: true, source: true } }
  )
  .add(
    'Timesheet/DailyRow/休暇タイプ - 午前：なし, 午後：有給',
    () => {
      const attRecord = new AttRecord({
        recordDate: '2017-07-03',
        dayType: AttRecord.DAY_TYPE.WORKDAY,
        requestIds: ['dummy1'],
      });
      return (
        <DummyTable>
          <DailyRow
            attRecord={attRecord}
            requestConditions={DailyRequestConditions.createFromParams(
              attRecord,
              {
                dummy1: createLeaveRequest({
                  requestTypeCode: 'Leave',
                  status: STATUS.Approved,
                  range: 'PM',
                  startTime: '14:00',
                  endTime: '18:00',
                  leaveType: 'Paid',
                }),
              },
              {},
              {}
            )}
          />
        </DummyTable>
      );
    },
    { info: { propTables: [DailyRow], inline: true, source: true } }
  )
  .add(
    'Timesheet/DailyRow/休暇タイプ - 午前：なし, 午後：無給',
    () => {
      const attRecord = new AttRecord({
        recordDate: '2017-07-03',
        dayType: AttRecord.DAY_TYPE.WORKDAY,
        requestIds: ['dummy1'],
      });
      return (
        <DummyTable>
          <DailyRow
            attRecord={attRecord}
            requestConditions={DailyRequestConditions.createFromParams(
              attRecord,
              {
                dummy1: createLeaveRequest({
                  requestTypeCode: 'Leave',
                  status: STATUS.Approved,
                  range: 'PM',
                  startTime: '14:00',
                  endTime: '18:00',
                  leaveType: 'Unpaid',
                }),
              },
              {},
              {}
            )}
          />
        </DummyTable>
      );
    },
    { info: { propTables: [DailyRow], inline: true, source: true } }
  )
  .add(
    'Timesheet/DailyRow/休職・休業',
    () => {
      const attRecord = new AttRecord({
        recordDate: '2017-07-03',
        dayType: AttRecord.DAY_TYPE.WORKDAY,
        isLeaveOfAbsence: true,
      });
      return (
        <DummyTable>
          <DailyRow
            attRecord={attRecord}
            requestConditions={DailyRequestConditions.createFromParams(
              attRecord,
              {},
              {},
              {}
            )}
          />
        </DummyTable>
      );
    },
    { info: { propTables: [DailyRow], inline: true, source: true } }
  );
