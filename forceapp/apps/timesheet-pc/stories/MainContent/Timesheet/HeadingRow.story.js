import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import HeadingRow from '../../../components/MainContent/Timesheet/HeadingRow';

const tableStyle = {
  boxSizing: 'border-box',
  position: 'relative',
  width: '100%',
  minWidth: '1024px',
  borderCollapse: 'separate',
  transition: 'height 0.3s ease-out',
};

storiesOf('timesheet-pc', module).add(
  'Timesheet/HeadingRow',
  () => (
    <div>
      <p>in thead</p>
      <table style={tableStyle}>
        <thead>
          <HeadingRow
            onClickToggleManHoursGraph={action('Toggle MonHoursColumn')}
            onDragChartStart={action('Drag Started')}
          />
        </thead>
      </table>

      <p style={{ marginTop: 20 }}>in tfoot</p>
      <table style={tableStyle}>
        <tfoot>
          <HeadingRow
            onClickToggleManHoursGraph={action('Toggle MonHoursColumn')}
            onDragChartStart={action('Drag Started')}
          />
        </tfoot>
      </table>
    </div>
  ),
  { info: { propTables: [HeadingRow], inline: true, source: true } }
);
