import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';

import AttRecordModel from '../../../models/AttRecord';
import AttChart from '../../../components/MainContent/Timesheet/AttChart';

import { actualWorkingPeriods } from '../../mock-data/graphParams';

const AddGauge = (props) => {
  const units = [];
  for (let i = 0; i <= 24 * 2; i += 2) {
    units.push(
      <span style={{ flex: '0 0 40px', textAlign: 'center' }}>{i % 24}</span>
    );
  }
  return (
    <div>
      <div style={{ display: 'flex' }}>{units}</div>
      {props.children}
    </div>
  );
};
AddGauge.propTypes = { children: PropTypes.node.isRequired };

storiesOf('timesheet-pc', module).add(
  'Timesheet/AttChart',
  () => (
    <AddGauge>
      <AttChart
        dayType={AttRecordModel.DAY_TYPE.WORKDAY}
        actualWorkingPeriods={actualWorkingPeriods}
      />
      <AttChart
        dayType={AttRecordModel.DAY_TYPE.HOLIDAY}
        actualWorkingPeriods={actualWorkingPeriods}
      />
      <AttChart dayType={AttRecordModel.DAY_TYPE.LEGAL_HOLIDAY} />
    </AddGauge>
  ),
  { info: { propTables: [AttChart], inline: true, source: true } }
);
