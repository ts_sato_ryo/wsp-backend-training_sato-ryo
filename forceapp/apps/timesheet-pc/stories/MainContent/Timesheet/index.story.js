import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Timesheet from '../../../components/MainContent/Timesheet';

import attRecordList from '../../mock-data/attRecordList';
import { dailyRequestConditionsMap } from '../../mock-data/dailyRequestConditions';

storiesOf('timesheet-pc', module).add(
  'Timesheet',
  () => (
    <Timesheet
      onClickToggleManHoursGraph={action('Click to Toggle Man-Hours-Graph')}
      attRecordList={attRecordList}
      dailyRequestConditionsMap={dailyRequestConditionsMap}
    />
  ),
  { info: { propTables: [Timesheet], inline: true, source: true } }
);
