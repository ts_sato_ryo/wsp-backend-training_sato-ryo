import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Request from '../../components/MainContent/Request';

import AttSummary from '../../models/AttSummary';

storiesOf('timesheet-pc', module)
  .add(
    'Request - 未申請',
    () => (
      <Request
        attSummary={new AttSummary({ status: AttSummary.STATUS.NotRequested })}
        onClickRequestButton={action('Clicked')}
      />
    ),
    { info: { propTables: [Request], inline: true, source: true } }
  )
  .add(
    'Request - 申請中',
    () => (
      <Request
        attSummary={new AttSummary({ status: AttSummary.STATUS.Pending })}
        onClickRequestButton={action('Clicked')}
      />
    ),
    { info: { propTables: [Request], inline: true, source: true } }
  )
  .add(
    'Request - 申請取消',
    () => (
      <Request
        attSummary={new AttSummary({ status: AttSummary.STATUS.Removed })}
        onClickRequestButton={action('Clicked')}
      />
    ),
    { info: { propTables: [Request], inline: true, source: true } }
  )
  .add(
    'Request - 却下',
    () => (
      <Request
        attSummary={new AttSummary({ status: AttSummary.STATUS.Rejected })}
        onClickRequestButton={action('Clicked')}
      />
    ),
    { info: { propTables: [Request], inline: true, source: true } }
  )
  .add(
    'Request - 承認済み',
    () => (
      <Request
        attSummary={new AttSummary({ status: AttSummary.STATUS.Approved })}
        onClickRequestButton={action('Clicked')}
      />
    ),
    { info: { propTables: [Request], inline: true, source: true } }
  )
  .add(
    'Request - 承認取消',
    () => (
      <Request
        attSummary={new AttSummary({ status: AttSummary.STATUS.Canceled })}
        onClickRequestButton={action('Clicked')}
      />
    ),
    { info: { propTables: [Request], inline: true, source: true } }
  )
  .add(
    'null - 申請ステータスがない(休職休業)',
    () => (
      <Request
        attSummary={new AttSummary({ status: null })}
        onClickRequestButton={action('Clicked')}
      />
    ),
    { info: { propTables: [Request], inline: true, source: true } }
  );
