import React from 'react';
import { storiesOf } from '@storybook/react';

import Utilities from '../../components/MainContent/Utilities';

storiesOf('timesheet-pc', module).add('Utilities', () => <Utilities />, {
  info: { propTables: [Utilities], inline: true, source: true },
});
