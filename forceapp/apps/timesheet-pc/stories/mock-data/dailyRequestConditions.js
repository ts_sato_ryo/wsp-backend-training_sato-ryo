import AttRecord from '../../models/AttRecord';
import DailyRequestConditions from '../../models/DailyRequestConditions';
import STATUS from '../../../domain/models/approval/request/Status';

import dummyRequestsMap from './attDailyRequestMap';

const availableRequestTypes = {
  Leave: { code: 'Leave', name: '休暇申請' },
  HolidayWork: {
    code: 'HolidayWork',
    name: '休日出勤申請',
  },
  OvertimeWork: {
    code: 'OvertimeWork',
    name: '残業申請',
  },
  EarlyStartWork: {
    code: 'EarlyStartWork',
    name: '早朝勤務申請',
  },
  Absence: {
    code: 'Absence',
    name: '欠勤申請',
  },
  Pattern: {
    code: 'Pattern',
    name: '勤務時間変更申請',
  },
};

const commonAttRecordParam = {
  recordDate: '2017-08-01',
  dayType: AttRecord.DAY_TYPE.WORKDAY,
  requestTypeCodes: Object.entries(availableRequestTypes).map(
    (reqType) => reqType[1].code
  ),
};

const commonRequestMapParam = {
  Rejected: dummyRequestsMap[STATUS.Rejected],
  ApprovalIn: dummyRequestsMap[STATUS.ApprovalIn],
  Approved: dummyRequestsMap[STATUS.Approved],
  Removed: dummyRequestsMap[STATUS.Removed],
};

export default DailyRequestConditions.createFromParams(
  new AttRecord({
    ...commonAttRecordParam,
    requestIds: ['Rejected', 'ApprovalIn', 'Approved', 'Removed'],
  }),
  commonRequestMapParam,
  availableRequestTypes,
  {}
);

export const dailyRequestConditionsHasRejected = DailyRequestConditions.createFromParams(
  new AttRecord({
    ...commonAttRecordParam,
    requestIds: ['Rejected', 'ApprovalIn', 'Approved', 'Removed'],
  }),
  commonRequestMapParam,
  availableRequestTypes,
  {}
);

export const dailyRequestConditionsHasApprovalIn = DailyRequestConditions.createFromParams(
  new AttRecord({
    ...commonAttRecordParam,
    requestIds: ['ApprovalIn', 'Approved', 'Removed'],
  }),
  commonRequestMapParam,
  availableRequestTypes,
  {}
);

export const dailyRequestConditionsHasApproved = DailyRequestConditions.createFromParams(
  new AttRecord({
    ...commonAttRecordParam,
    requestIds: ['Approved', 'Removed'],
  }),
  commonRequestMapParam,
  availableRequestTypes,
  {}
);

export const dailyRequestConditionsHasNoRequests = DailyRequestConditions.createFromParams(
  new AttRecord({
    ...commonAttRecordParam,
    requestIds: [],
  }),
  commonRequestMapParam,
  availableRequestTypes,
  {}
);

export const dailyRequestConditionsMap = {};

for (let date = 1; date <= 31; date++) {
  console.log(
    '****',
    dummyRequestsMap[Object.keys(dummyRequestsMap)[date % 6]]
  );

  const recordDate = `2017-07-${`0${date}`.slice(-2)}`;
  dailyRequestConditionsMap[
    recordDate
  ] = DailyRequestConditions.createFromParams(
    new AttRecord({
      ...commonAttRecordParam,
      recordDate,
      requestIds: [Object.keys(dummyRequestsMap)[date % 6]],
    }),
    dummyRequestsMap,
    availableRequestTypes,
    {}
  );
}
