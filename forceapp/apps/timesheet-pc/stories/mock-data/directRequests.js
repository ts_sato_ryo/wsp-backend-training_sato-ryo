// @flow

import { defaultValue } from '../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';
import {
  type DirectRequest,
  create,
} from '../../../domain/models/attendance/AttDailyRequest/DirectRequest';
import { CODE } from '../../../domain/models/attendance/AttDailyRequestType';

export const OneDay: DirectRequest = create({
  ...defaultValue,
  requestTypeCode: CODE.Direct,
  startDate: '2018-06-18',
  endDate: '2018-06-18',
  startTime: 60 * 9,
  endTime: 60 * 18,
  restTimes: [
    {
      startTime: 60 * 12,
      endTime: 60 * 13,
    },
    {
      startTime: 60 * 18,
      endTime: 60 * 19,
    },
    {
      startTime: '',
      endTime: '',
    },
  ],
  remarks: 'remarks',
  approver01Name: 'Approver',
  isForReapply: false,
});

export const Range: DirectRequest = create({
  ...defaultValue,
  startDate: '2018-06-18',
  endDate: '2018-07-31',
  startTime: 60 * 9,
  endTime: 60 * 18,
  restTimes: [
    {
      startTime: 60 * 12,
      endTime: 60 * 13,
    },
    {
      startTime: 60 * 18,
      endTime: 60 * 19,
    },
    {
      startTime: '',
      endTime: '',
    },
  ],
  remarks: 'remarks',
  approver01Name: 'Approver',
  isForReapply: false,
});
