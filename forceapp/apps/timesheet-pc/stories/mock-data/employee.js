import Employee from '../../models/Employee';

const dummyEmployee = new Employee({
  employeeName: '社員太郎',
  departmentName: 'マーケティングチーム',
  workingTypeName: '管理監督者',
});

export default dummyEmployee;
