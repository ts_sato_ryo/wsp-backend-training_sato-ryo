// @flow

import { defaultValue } from '../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';
import {
  type HolidayWorkRequest,
  create,
} from '../../../domain/models/attendance/AttDailyRequest/HolidayWorkRequest';
import { CODE } from '../../../domain/models/attendance/AttDailyRequestType';

export const requestWithoutSubstitute: HolidayWorkRequest = create({
  ...defaultValue,
  requestTypeCode: CODE.HolidayWork,
  targetDate: '2018-09-02',
  startTime: 9 * 60,
  endTime: 22 * 60,
  remarks: 'remarks',
  substituteLeaveType: 'None',
  availableSubstituteLeaveTypeList: ['None', 'Substitute'],
});

export const requestWithSubstitute: HolidayWorkRequest = create({
  ...defaultValue,
  requestTypeCode: CODE.HolidayWork,
  targetDate: '2018-09-02',
  substituteDate: '2019-10-10',
  substituteLeaveType: 'Substitute',
  startTime: 9 * 60,
  endTime: 22 * 60,
  remarks: 'remarks',
  availableSubstituteLeaveTypeList: ['None', 'Substitute'],
});
