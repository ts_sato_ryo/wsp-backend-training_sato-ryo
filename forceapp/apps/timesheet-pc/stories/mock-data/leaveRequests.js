import { create as createLeaveRequest } from '../../../domain/models/attendance/AttDailyRequest/LeaveRequest';
import { createFromParam as createAttLeave } from '../../../domain/models/attendance/AttLeave';

export const availableLeaveTypes = [
  createAttLeave({
    name: '年次有給休暇',
    code: 'AnnualPaidLeave',
    type: 'Paid',
    ranges: ['Day', 'AM', 'PM', 'Half', 'Time'],
    daysLeft: 1,
  }),
  createAttLeave({
    name: '日数管理休暇 - 残日数ゼロ',
    code: 'DaysManagedLeave',
    type: 'Paid',
    ranges: ['Day', 'AM', 'PM', 'Half', 'Time'],
    daysLeft: 0,
  }),
  createAttLeave({
    name: '欠勤 ※日数管理でない',
    code: 'Absence',
    type: 'Unpaid',
    ranges: ['Day'],
  }),
];

export const rangeDay = createLeaveRequest({
  requestTypeCode: 'Leave',
  availableLeaveTypes,
  leaveCode: 'AnnualPaidLeave',
  leaveRange: 'Day',
});

export const rangeAM = createLeaveRequest({
  requestTypeCode: 'Leave',
  availableLeaveTypes,
  leaveCode: 'AnnualPaidLeave',
  leaveRange: 'AM',
});

export const rangeHalf = createLeaveRequest({
  requestTypeCode: 'Leave',
  availableLeaveTypes,
  leaveCode: 'AnnualPaidLeave',
  leaveRange: 'Half',
});

export const rangeTime = createLeaveRequest({
  requestTypeCode: 'Leave',
  availableLeaveTypes,
  leaveCode: 'AnnualPaidLeave',
  leaveRange: 'Time',
});

export const daysManagedWith1 = createLeaveRequest({
  requestTypeCode: 'Leave',
  availableLeaveTypes,
  leaveCode: 'DaysManagedLeave',
  leaveRange: 'Time',
});

export const daysManagedWith0 = createLeaveRequest({
  requestTypeCode: 'Leave',
  availableLeaveTypes,
  leaveCode: 'DaysManagedLeave',
  leaveRange: 'Days',
});

export const notDaysManaged = createLeaveRequest({
  requestTypeCode: 'Leave',
  availableLeaveTypes,
  leaveCode: 'Absence',
  leaveRange: 'Days',
});
