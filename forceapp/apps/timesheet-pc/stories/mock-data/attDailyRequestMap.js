import { createFromDefaultValue } from '../../../domain/models/attendance/AttDailyRequest';
import { CODE } from '../../../domain/models/attendance/AttDailyRequestType';
import STATUS from '../../../domain/models/approval/request/Status';

const dummyRequestTypeMap = {
  [CODE.Leave]: { name: 'Leave' },
};

const dummyRequestsMap = {
  [STATUS.Rejected]: createFromDefaultValue(dummyRequestTypeMap, CODE.Leave, {
    id: '000001',
    status: STATUS.Rejected,
    requestTypeCode: CODE.Leave,
    requestTypeName: '休暇申請',
    leaveType: 'Paid',
  }),
  [STATUS.ApprovalIn]: createFromDefaultValue(dummyRequestTypeMap, CODE.Leave, {
    id: '000002',
    status: STATUS.ApprovalIn,
    requestTypeCode: CODE.Leave,
    requestTypeName: '休暇申請',
    leaveType: 'Paid',
  }),
  [STATUS.Approved]: createFromDefaultValue(dummyRequestTypeMap, CODE.Leave, {
    id: '000003',
    status: STATUS.Approved,
    requestTypeCode: CODE.Leave,
    requestTypeName: '休暇申請',
    leaveType: 'Paid',
  }),
  [STATUS.Removed]: createFromDefaultValue(dummyRequestTypeMap, CODE.Leave, {
    id: '000004',
    status: STATUS.Removed,
    requestTypeCode: CODE.Leave,
    requestTypeName: '休暇申請',
    leaveType: 'Paid',
  }),
  [`${STATUS.Approved}-Paid`]: createFromDefaultValue(
    dummyRequestTypeMap,
    CODE.Leave,
    {
      id: '000005',
      status: STATUS.Approved,
      requestTypeCode: CODE.Leave,
      requestTypeName: '休暇申請',
      leaveName: '有給休暇',
      leaveType: 'Paid',
      leaveRange: 'Day',
    }
  ),
  [`${STATUS.Approved}-Unpaid`]: createFromDefaultValue(
    dummyRequestTypeMap,
    CODE.Leave,
    {
      id: '000006',
      status: STATUS.Approved,
      requestTypeCode: CODE.Leave,
      requestTypeName: '休暇申請',
      leaveName: '無給休暇',
      leaveType: 'Unpaid',
      leaveRange: 'Day',
    }
  ),
};

export default dummyRequestsMap;
