import AttRecord from '../../models/AttRecord';
import '../../../commons/config/moment';

const attRecords = [];

for (let i = 1; i <= 31; i++) {
  const date = new Date(2017, 6, i);
  const dayTypeDummyMap = {
    0: AttRecord.DAY_TYPE.LEGAL_HOLIDAY,
    6: AttRecord.DAY_TYPE.HOLIDAY,
  };
  const dayType = dayTypeDummyMap[date.getDay()] || AttRecord.DAY_TYPE.WORKDAY;

  attRecords.push(new AttRecord({
    recordDate: [
      date.getFullYear(),
      `0${date.getMonth() + 1}`.slice(-2),
      `0${date.getDate()}`.slice(-2),
    ].join('-'),
    dayType,
  }));
}

export default attRecords;
