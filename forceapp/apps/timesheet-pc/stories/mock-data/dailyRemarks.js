import DailyRemarks from '../../models/DailyRemarks';

export default new DailyRemarks({
  recordId: 'dummy-id',
  recordDate: '2017-10-10',
  remarks: 'よろしくお願いします',
});
