import DailyAttTime from '../../models/DailyAttTime';
import AttRecord from '../../models/AttRecord';

export default DailyAttTime.createFromParam(new AttRecord({
  recordDate: '2017-08-01',
  dayType: DailyAttTime.DAY_TYPE.WORKDAY,
  startTime: 540,
  endTime: 1080,
  startStampTime: 520,
  endStampTime: 1090,
  rest1StartTime: 781,
  rest1EndTime: 841,
  rest2StartTime: 782,
  rest2EndTime: 842,
  rest3StartTime: 783,
  rest3EndTime: 843,
  rest4StartTime: 784,
  rest4EndTime: 844,
  // rest5StartTime: 785,
  // rest5EndTime: 845,
}));
