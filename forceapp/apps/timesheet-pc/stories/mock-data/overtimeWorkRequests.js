// @flow

import { defaultValue } from '../../../domain/models/attendance/AttDailyRequest/BaseAttDailyRequest';
import {
  type OvertimeWorkRequest,
  create,
} from '../../../domain/models/attendance/AttDailyRequest/OvertimeWorkRequest';
import { CODE } from '../../../domain/models/attendance/AttDailyRequestType';

export const defaultValues: OvertimeWorkRequest = create({
  ...defaultValue,
  requestTypeCode: CODE.OvertimeWork,
  startDate: '2018-06-18',
  startTime: 9 * 60,
  endTime: 22 * 60,
  remarks: 'remarks',
  approver01Name: 'Approver',
  isForReapply: false,
});

export default defaultValues;
