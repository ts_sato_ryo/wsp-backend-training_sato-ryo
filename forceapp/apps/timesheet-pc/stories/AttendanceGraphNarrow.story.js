import React from 'react';
import { storiesOf } from '@storybook/react';

import AttendanceGraphNarrow from '../components/graphs/AttendanceGraphNarrow';

storiesOf('timesheet-pc', module).add(
  'AttendanceGraphNarrow',
  () => <AttendanceGraphNarrow />,
  { info: { propTables: [AttendanceGraphNarrow], inline: true, source: true } }
);
