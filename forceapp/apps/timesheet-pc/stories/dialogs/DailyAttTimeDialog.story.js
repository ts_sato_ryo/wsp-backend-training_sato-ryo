import React from 'react';
import { storiesOf } from '@storybook/react';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import DailyAttTimeDialog from '../../components/dialogs/DailyAttTimeDialog';
import dummyDailyAttTime from '../mock-data/dailyAttTime';

storiesOf('timesheet-pc', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'dialogs/DailyAttTimeDialog',
    () => {
      return <DailyAttTimeDialog dailyAttTime={dummyDailyAttTime} />;
    },
    { info: { propTables: [DailyAttTimeDialog], inline: false, source: true } }
  );
