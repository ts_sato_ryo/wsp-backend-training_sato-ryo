import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import DialogDecorator from '../../../../../.storybook/decorator/Dialog';

import DailyAttRequestDialog from '../../../components/dialogs/DailyAttRequestDialog';
import * as LeaveRequest from '../../../../domain/models/attendance/AttDailyRequest/LeaveRequest';

import dummyDailyRequestConditions from '../../mock-data/dailyRequestConditions';

storiesOf('timesheet-pc', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'dialogs/DailyAttRequestDialog',
    () => {
      return (
        <DailyAttRequestDialog
          attWorkingType={{}}
          requestConditions={dummyDailyRequestConditions}
          onClickRequestDetailButton={action('提出済み申請クリック')}
          onClickRequestEntryButton={action('新規申請ボタンクリック')}
          targetRequest={LeaveRequest.create({
            ...dummyDailyRequestConditions.latestRequests[0],
            availableLeaveTypes: [
              {
                ranges: ['AM'],
              },
            ],
          })}
        />
      );
    },
    {
      info: {
        propTables: [DailyAttRequestDialog],
        inline: false,
        source: true,
      },
    }
  );
