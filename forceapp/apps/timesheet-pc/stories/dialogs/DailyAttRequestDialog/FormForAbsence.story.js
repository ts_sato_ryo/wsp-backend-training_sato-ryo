// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import FormForAbsence from '../../../components/dialogs/DailyAttRequestDialog/FormForAbsence';
import { OneDay, Range } from '../../mock-data/absenceRequests';

storiesOf('timesheet-pc', module)
  .add(
    'dialogs/DailyAttRequestDialog FormForAbsence - １日指定',
    () => {
      return (
        <FormForAbsence
          isReadOnly={false}
          hasRange={false}
          targetRequest={OneDay}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForAbsence], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForAbsence - 1日指定（Read Only）',
    () => {
      return (
        <FormForAbsence
          isReadOnly
          targetRequest={OneDay}
          hasRange={false}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForAbsence], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForAbsence - 範囲指定',
    () => {
      return (
        <FormForAbsence
          isReadOnly={false}
          hasRange
          targetRequest={Range}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForAbsence], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForAbsence - 範囲指定（Read Only）',
    () => {
      return (
        <FormForAbsence
          isReadOnly
          hasRange
          targetRequest={Range}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForAbsence], inline: true, source: true } }
  );
