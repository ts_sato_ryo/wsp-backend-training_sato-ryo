import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import FormForEarlyStartWork from '../../../components/dialogs/DailyAttRequestDialog/FormForEarlyStartWork';
import * as earlyStartWorkRequests from '../../mock-data/earlyStartWorkRequests';

storiesOf('timesheet-pc', module)
  .add(
    'dialogs/DailyAttRequestDialog FormForEarlyStartWork',
    () => {
      return (
        <FormForEarlyStartWork
          targetRequest={earlyStartWorkRequests.defaultValues}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    {
      info: { propTables: [FormForEarlyStartWork], inline: true, source: true },
    }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForEarlyStartWork (Read Only）',
    () => {
      return (
        <FormForEarlyStartWork
          isReadOnly={false}
          targetRequest={earlyStartWorkRequests.defaultValues}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    {
      info: { propTables: [FormForEarlyStartWork], inline: true, source: true },
    }
  );
