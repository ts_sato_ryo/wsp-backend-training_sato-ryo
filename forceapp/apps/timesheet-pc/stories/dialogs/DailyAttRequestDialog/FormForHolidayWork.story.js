import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { EDIT_ACTION } from '../../../../domain/models/attendance/AttDailyRequest';
import FormForHolidayWork from '../../../components/dialogs/DailyAttRequestDialog/FormForHolidayWork';
import {
  requestWithSubstitute,
  requestWithoutSubstitute,
} from '../../mock-data/holidayWorkRequests';

storiesOf('timesheet-pc', module)
  .add(
    'dialogs/DailyAttRequestDialog FormForHolidayWork 振替休日無し',
    () => {
      return (
        <FormForHolidayWork
          isReadOnly={false}
          targetRequest={requestWithoutSubstitute}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForHolidayWork], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForHolidayWork 振替休日無し（Read Only）',
    () => {
      return (
        <FormForHolidayWork
          isReadOnly={false}
          targetRequest={requestWithoutSubstitute}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForHolidayWork], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForHolidayWork 振替休日無し 承認内容変更申請',
    () => {
      return (
        <FormForHolidayWork
          isReadOnly={false}
          editAction={EDIT_ACTION.Reapply}
          targetRequest={requestWithoutSubstitute}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForHolidayWork], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForHolidayWork 振替休日有り',
    () => {
      return (
        <FormForHolidayWork
          isReadOnly={false}
          targetRequest={requestWithSubstitute}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForHolidayWork], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForHolidayWork 振替休日有り (Read Only)',
    () => {
      return (
        <FormForHolidayWork
          isReadOnly={false}
          targetRequest={requestWithSubstitute.copyAsReadOnly()}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForHolidayWork], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForHolidayWork 振替休日有り 承認内容変更申請',
    () => {
      return (
        <FormForHolidayWork
          isReadOnly={false}
          editAction={EDIT_ACTION.Reapply}
          targetRequest={requestWithSubstitute}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForHolidayWork], inline: true, source: true } }
  );
