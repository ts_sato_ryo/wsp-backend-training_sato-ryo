import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import FormForOvertimeWork from '../../../components/dialogs/DailyAttRequestDialog/FormForOvertimeWork';
import * as overtimeWorkRequests from '../../mock-data/overtimeWorkRequests';

storiesOf('timesheet-pc', module)
  .add(
    'dialogs/DailyAttRequestDialog FormForOvertimeWork',
    () => {
      return (
        <FormForOvertimeWork
          isReadOnly={false}
          targetRequest={overtimeWorkRequests.defaultValues}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForOvertimeWork], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForWorkRequest (Read Only）',
    () => {
      return (
        <FormForOvertimeWork
          isReadOnly
          targetRequest={overtimeWorkRequests.defaultValues}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForOvertimeWork], inline: true, source: true } }
  );
