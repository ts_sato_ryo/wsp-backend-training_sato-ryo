import React from 'react';
import { storiesOf } from '@storybook/react';

import FormFrame from '../../../components/dialogs/DailyAttRequestDialog/FormFrame';
import AttDailyRequest from '../../../../domain/models/attendance/AttDailyRequest';

storiesOf('timesheet-pc', module)
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 新規申請［編集モード］',
    () => {
      return (
        <FormFrame
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              isReadOnly: false,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 承認待ち［表示モード］',
    () => {
      return (
        <FormFrame
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.ApprovalIn,
              isReadOnly: true,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 却下（申請取消・承認取消）［表示モード］',
    () => {
      return (
        <FormFrame
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.Rejected,
              isReadOnly: true,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 却下（申請取消・承認取消）［編集モード］',
    () => {
      return (
        <FormFrame
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.Rejected,
              isReadOnly: false,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 承認済み［表示モード］',
    () => {
      return (
        <FormFrame
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.Approved,
              isReadOnly: true,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 承認済み［編集モード］',
    () => {
      return (
        <FormFrame
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.Approved,
              isReadOnly: false,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 承認済み・代理ログイン［表示モード］',
    () => {
      return (
        <FormFrame
          isProxyMode
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.Approved,
              isReadOnly: true,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 承認済み・代理ログイン［編集モード］',
    () => {
      return (
        <FormFrame
          isProxyMode
          isAvailableToModify
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.Approved,
              isReadOnly: false,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog/FormFrame - 承認済み・確定済み［表示モード］',
    () => {
      return (
        <FormFrame
          targetRequest={
            new AttDailyRequest({
              id: 'dummy',
              status: AttDailyRequest.STATUS.Approved,
              isReadOnly: true,
            })
          }
        />
      );
    },
    { info: { propTables: [FormFrame], inline: true, source: true } }
  );
