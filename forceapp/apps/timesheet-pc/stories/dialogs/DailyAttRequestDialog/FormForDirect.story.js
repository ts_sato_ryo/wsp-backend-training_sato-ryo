// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import FormForDirect from '../../../components/dialogs/DailyAttRequestDialog/FormForDirect';
import { OneDay, Range } from '../../mock-data/directRequests';

storiesOf('timesheet-pc', module)
  .add(
    'dialogs/DailyAttRequestDialog FormForDirect - １日指定',
    () => {
      return (
        <FormForDirect
          isReadOnly={false}
          hasRange={false}
          targetRequest={OneDay}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForDirect], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForDirect - 1日指定（Read Only）',
    () => {
      return (
        <FormForDirect
          isReadOnly={false}
          hasRange={false}
          targetRequest={OneDay}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForDirect], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForDirect - 範囲指定',
    () => {
      return (
        <FormForDirect
          isReadOnly={false}
          hasRange
          targetRequest={Range}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForDirect], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForDirect - 範囲指定（Read Only）',
    () => {
      return (
        <FormForDirect
          isReadOnly
          hasRange
          targetRequest={Range}
          onUpdateHasRange={action('UpdatedHasRange')}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForDirect], inline: true, source: true } }
  );
