import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import FormForLeave from '../../../components/dialogs/DailyAttRequestDialog/FormForLeave';
import * as dummyLeaveRequests from '../../mock-data/leaveRequests';

storiesOf('timesheet-pc', module)
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 全日',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.rangeDay}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 全日（Read Only）',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.rangeDay.copyAsReadOnly()}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 午前半休',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.rangeAM}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 午前半休（Read Only）',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.rangeAM.copyAsReadOnly()}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 時間指定半休',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.rangeHalf}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 時間指定半休（Read Only）',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.rangeHalf.copyAsReadOnly()}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 日数管理・残日数ゼロ',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.daysManagedWith0}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  )
  .add(
    'dialogs/DailyAttRequestDialog FormForLeave - 日数管理でない休暇',
    () => {
      return (
        <FormForLeave
          targetRequest={dummyLeaveRequests.notDaysManaged}
          onUpdateValue={action('Updated')}
          onSubmit={action('Submit')}
        />
      );
    },
    { info: { propTables: [FormForLeave], inline: true, source: true } }
  );
