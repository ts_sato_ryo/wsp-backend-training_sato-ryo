import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import DailyAttentionsDialog from '../../components/dialogs/DailyAttentionsDialog';

import TextUtil from '../../../commons/utils/TextUtil';
import msg from '../../../commons/languages';

const messages = [
  TextUtil.template(msg().Att_Msg_NotIncludeWorkingTime, '7:00', '8:00'),
  TextUtil.template(msg().Att_Msg_NotIncludeWorkingTime, '18:00', '20:00'),
  TextUtil.template(msg().Att_Msg_InsufficientRestTime, '15'),
];

storiesOf('timesheet-pc', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'dialogs/DailyAttentionsDialog - 単一メッセージ',
    '其日の警告を表示します。',
    () => {
      return (
        <DailyAttentionsDialog
          messages={[messages[0]]}
          isHide={false}
          onHide={action('Hide Dialog')}
        />
      );
    },
    {
      info: {
        propTables: [DailyAttentionsDialog],
        inline: false,
        source: true,
      },
    }
  );

storiesOf('timesheet-pc', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'dialogs/DailyAttentionsDialog - 複数メッセージ',
    '其日の警告を表示します。',
    () => {
      return (
        <DailyAttentionsDialog
          messages={messages}
          isHide={false}
          onHide={action('Hide Dialog')}
        />
      );
    },
    {
      info: {
        propTables: [DailyAttentionsDialog],
        inline: false,
        source: true,
      },
    }
  );
