import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import DailyRemarksDialog from '../../components/dialogs/DailyRemarksDialog';
import dummyDailyRemarks from '../mock-data/dailyRemarks';

storiesOf('timesheet-pc', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'dialogs/DailyRemarksDialog',
    () => {
      return (
        <DailyRemarksDialog
          dailyRemarks={dummyDailyRemarks}
          onUpdateValue={action('Update')}
          onSubmit={action('Submit')}
          hideDialog={action('Hide Dialog')}
        />
      );
    },
    { info: { propTables: [DailyRemarksDialog], inline: false, source: true } }
  );
