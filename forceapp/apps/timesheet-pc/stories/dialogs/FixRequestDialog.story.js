import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import FixRequestDialog from '../../components/dialogs/FixRequestDialog';

storiesOf('timesheet-pc', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'dialogs/FixRequestDialog',
    () => {
      return (
        <FixRequestDialog
          comment="コメント"
          cancel={action('cancel')}
          editComment={action('editComment')}
          submit={action('submit')}
        />
      );
    },
    { info: { propTables: [FixRequestDialog], inline: false, source: true } }
  );
