// @flow

import type { Dispatch } from 'redux';

import { getUserSetting } from '../../commons/actions/userSetting';
import UrlUtil from '../../commons/utils/UrlUtil';

import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';

import { actions as reportActions } from '../../domain/modules/exp/report';

export const openPrintDialog = () => {
  window.print();
};

export const initialize = () => (dispatch: Dispatch<any>) => {
  const params = UrlUtil.getUrlQuery();
  const reportId = params && params.reportId;
  dispatch(loadingStart());

  return Promise.all([
    dispatch(
      getUserSetting({ detailSelectors: ['DEPARTMENT'] })
    ) /* Only Retreive the basic information and Department details. */,
    dispatch(reportActions.get(reportId)),
  ])
    .catch((err) => dispatch(catchApiError(err, { isContinuable: false })))
    .then(() => dispatch(loadingEnd()));
};
