// @flow

import React from 'react';

import Header from './Header';
import Applicant from './Applicant';
import Content from './Content';
import Records from './Records';

import { type UserSetting } from '../../../domain/models/UserSetting';
import { type Report } from '../../../domain/models/exp/Report';

import './index.scss';

export type Props = {|
  report: Report,
  userSetting: UserSetting,
|};

const ROOT = 'expenses-pc-print-print-report';

const PrintReport = ({ report, userSetting }: Props) => (
  <div className={ROOT}>
    <Header report={report} />
    <hr />
    <Applicant userSetting={userSetting} />
    <hr />
    <Content report={report} userSetting={userSetting} />
    <hr />
    <Records report={report} userSetting={userSetting} />
  </div>
);

export default PrintReport;
