// @flow

import React from 'react';

import msg from '../../../../commons/languages';

import { type Report } from '../../../../domain/models/exp/Report';

import './index.scss';

const ROOT = 'expenses-pc-print-print-report-header';

export type Props = {|
  report: Report,
|};

const Header = ({ report }: Props) => {
  const items = [
    { label: msg().Exp_Clbl_ReportTitle, value: report.subject },
    { label: msg().Exp_Lbl_ReportNo, value: report.reportNo },
  ];
  return (
    <div className={ROOT}>
      <div className={`${ROOT}__title`}>{msg().Exp_Lbl_ExpenseReport}</div>
      {items.map(({ label, value }) => (
        <div className={`${ROOT}__element`}>
          <div className={`${ROOT}__label`}>{label} :</div>
          <div className={`${ROOT}__value`}>{value}</div>
        </div>
      ))}
    </div>
  );
};

export default Header;
/*
        <MultiColumnsGrid
          className={`${ROOT}__element`}
          sizeList={[1, 3, 7, 1]}
          alignments={['middle', 'top', 'top', 'middle']}
        >
          <div />
          <div className={`${ROOT}__label`}>{label} :</div>
          <div className={`${ROOT}__value`}>{value}</div>
          <div />
        </MultiColumnsGrid>
        */
