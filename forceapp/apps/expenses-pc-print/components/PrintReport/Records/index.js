// @flow

import React from 'react';

import msg from '../../../../commons/languages';

import MultiColumnsGrid from '../../../../commons/components/MultiColumnsGrid';
import FormatUtil from '../../../../commons/utils/FormatUtil';
import DateUtil from '../../../../commons/utils/DateUtil';

import './index.scss';

import { type UserSetting } from '../../../../domain/models/UserSetting';
import { type Report } from '../../../../domain/models/exp/Report';
import { type RouteInfo } from '../../../../domain/models/exp/Record';

export type Props = {|
  report: Report,
  userSetting: UserSetting,
|};

const ROOT = 'expenses-pc-print-print-report-records';

export default class Records extends React.Component<Props> {
  renderIndex = (index: string | number) => (
    <div className={`${ROOT}__record-content-number`}>{index}</div>
  );

  renderDate = (recordDate: string) => (
    <div className={`${ROOT}__record-content-date`}>
      {DateUtil.format(recordDate)}
    </div>
  );

  renderExpType = (expTypeName: string) => (
    <div className={`${ROOT}__record-content-expense-type`}>{expTypeName}</div>
  );

  renderAmount = (amount: number) => (
    <div className={`${ROOT}__record-content-amount`}>
      {this.props.userSetting.currencySymbol}
      {FormatUtil.formatNumber(
        amount,
        this.props.userSetting.currencyDecimalPlaces
      )}
    </div>
  );

  renderDetail = (routeInfo?: ?RouteInfo, remarks?: string) => (
    /* change to recordItem for routeInfo after itemization for record type Transportation */
    <div className={`${ROOT}__record-content-detail`}>
      {routeInfo && routeInfo.origin && routeInfo.arrival && (
        <div>
          {routeInfo.origin.name}
          {' - '}
          {routeInfo.arrival.name}
        </div>
      )}
      <div>{remarks}</div>
    </div>
  );

  renderReport = (report: Report) => (
    <div>
      {report.records.map((record, recordIdx) => {
        return record.items.map((item, itemIdx) => {
          const index = itemIdx === 0 ? recordIdx + 1 : '-';
          const recordDate =
            itemIdx === 0 ? record.recordDate : item.recordDate;
          return (
            <div className={`${ROOT}__record-content-row`}>
              {this.renderIndex(index)}
              {this.renderDate(recordDate)}
              {this.renderExpType(item.expTypeName)}
              {this.renderAmount(item.amount)}
              {this.renderDetail(record.routeInfo, item.remarks)}
            </div>
          );
        });
      })}
    </div>
  );

  render() {
    return (
      <div className={ROOT}>
        <MultiColumnsGrid sizeList={[1, 11]}>
          <div />
          <div className={`${ROOT}__record-name`}>{msg().Exp_Lbl_Records}</div>
        </MultiColumnsGrid>
        <div className={`${ROOT}__record-header`}>
          <div className={`${ROOT}__record-header-number`}>#</div>
          <div className={`${ROOT}__record-header-date`}>
            {msg().Exp_Clbl_Date}
          </div>
          <div className={`${ROOT}__record-header-expense-type`}>
            {msg().Exp_Clbl_ExpenseType}
          </div>
          <div className={`${ROOT}__record-header-amount`}>
            {msg().Exp_Clbl_Amount}
          </div>
          <div className={`${ROOT}__record-header-detail`}>
            {msg().Exp_Lbl_Detail}
          </div>
        </div>
        <div className={`${ROOT}__record-content`}>
          {this.renderReport(this.props.report)}
        </div>
      </div>
    );
  }
}
