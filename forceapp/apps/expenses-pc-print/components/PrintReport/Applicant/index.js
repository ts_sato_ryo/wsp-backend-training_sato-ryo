// @flow

import React from 'react';

import msg from '../../../../commons/languages';

import { type UserSetting } from '../../../../domain/models/UserSetting';

import './index.scss';

export type Props = {|
  userSetting: UserSetting,
|};

const ROOT = 'expenses-pc-print-print-report-applicant';

const Applicant = ({ userSetting }: Props) => {
  const items = [
    {
      label: msg().Exp_Lbl_EmployeeName,
      value: userSetting.employeeName,
    },
    {
      label: msg().Exp_Lbl_EmployeeCodeFull,
      value: userSetting.employeeCode,
    },
    {
      label: msg().Exp_Lbl_DepartmentName,
      value: userSetting.departmentName,
    },
    {
      label: msg().Exp_Lbl_DepartmentCode,
      value: userSetting.departmentCode,
    },
  ];

  return (
    <div className={ROOT}>
      <div className={`${ROOT}__title`}>{msg().Exp_Lbl_ActorName}</div>
      {items.map(({ label, value }) => (
        <div className={`${ROOT}__element`}>
          <div className={`${ROOT}__label`}>{label} :</div>
          <div className={`${ROOT}__value`}>{value}</div>
        </div>
      ))}
    </div>
  );
};

export default Applicant;
