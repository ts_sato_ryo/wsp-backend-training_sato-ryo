// @flow

import React from 'react';

import msg from '../../../../commons/languages';

import './index.scss';

import FormatUtil from '../../../../commons/utils/FormatUtil';
import DateUtil from '../../../../commons/utils/DateUtil';

import { type UserSetting } from '../../../../domain/models/UserSetting';
import { type Report } from '../../../../domain/models/exp/Report';

import { getLabelValueFromEIs } from '../../../../domain/models/exp/ExtendedItem';

export type Props = {|
  report: Report,
  userSetting: UserSetting,
|};

const ROOT = 'expenses-pc-print-print-report-content';

const Content = ({ report, userSetting }: Props) => {
  const formattedAmount =
    userSetting.currencySymbol +
    FormatUtil.formatNumber(
      report.totalAmount,
      userSetting.currencyDecimalPlaces
    );

  const items = [
    {
      label: msg().Exp_Clbl_Amount,
      value: formattedAmount,
    },
    {
      label: msg().Appr_Lbl_RequestDate,
      value: report.requestDate ? DateUtil.format(report.requestDate) : '',
    },
    report.expReportTypeName
      ? {
          label: msg().Exp_Clbl_ReportType,
          value: report.expReportTypeName,
        }
      : {},
    {
      label: msg().Exp_Clbl_RecordDate,
      value: DateUtil.format(report.accountingDate),
    },
    ...getLabelValueFromEIs(report),
    report.vendorId
      ? {
          label: msg().Exp_Lbl_Vendor,
          value: `${report.vendorCode} - ${report.vendorName}`,
        }
      : {},
    {
      label: msg().Exp_Lbl_Job,
      value: report.jobId ? `${report.jobCode} - ${report.jobName}` : '',
    },
    {
      label: msg().Exp_Clbl_CostCenter,
      value: report.costCenterHistoryId
        ? `${report.costCenterCode} - ${report.costCenterName || ''}`
        : '',
    },
    {
      label: msg().Exp_Clbl_ReportRemarks,
      value: report.remarks,
    },
  ];
  const filteredItems = items.filter((i) => i.label);
  return (
    <div className={ROOT}>
      <div className={`${ROOT}__title`}>{msg().Exp_Lbl_RequestContents}</div>
      {filteredItems
        .filter(
          (item) =>
            !(
              item.label === msg().Exp_Lbl_Job ||
              item.label === msg().Exp_Clbl_CostCenter
            ) || item.value
        )
        .map(({ label, value }) => (
          <div className={`${ROOT}__element`}>
            <div className={`${ROOT}__label`}>{label} :</div>
            <div className={`${ROOT}__value`}>{value}</div>
          </div>
        ))}
    </div>
  );
};

export default Content;
