// @flow

import React from 'react';

import GlobalContainer from '../../commons/containers/GlobalContainer';
import Button from '../../commons/components/buttons/Button';

import PrintReport from './PrintReport';

import { type UserSetting } from '../../domain/models/UserSetting';
import { type Report } from '../../domain/models/exp/Report';

import msg from '../../commons/languages';

import './index.scss';

export type Props = {|
  onClickPrintButton: () => void,
  report: Report,
  userSetting: UserSetting,
|};

const App = ({ onClickPrintButton, report, userSetting }: Props) => (
  <GlobalContainer>
    <Button type="default" onClick={onClickPrintButton}>
      {msg().Com_Btn_PrintDo}
    </Button>
    <PrintReport report={report} userSetting={userSetting} />
  </GlobalContainer>
);

export default App;
