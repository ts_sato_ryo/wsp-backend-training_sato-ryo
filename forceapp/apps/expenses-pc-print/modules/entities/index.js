// @flow

import { combineReducers } from 'redux';

import report from './report';

const reducers = {
  report,
};

export default combineReducers<Object, Object>(reducers);
