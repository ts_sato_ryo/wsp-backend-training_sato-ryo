// @flow

import { type Dispatch } from 'redux';
import { connect } from 'react-redux';
import App from '../components';
import * as appActions from '../action-dispatchers/App';
import { type State } from '../modules';
import { compose } from '../../commons/utils/FnUtil';
import lifecycle from '../../mobile-app/concerns/lifecycle';

const mapStateToProps = (state: State, _ownProps) => ({
  report: state.entities.report,
  userSetting: state.userSetting,
});

const mapDispatchToProps = {
  onClickPrintButton: appActions.openPrintDialog,
};

export default (compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  lifecycle({
    componentDidMount: (dispatch: Dispatch<any>, _props) => {
      dispatch(appActions.initialize());
    },
  })
)(App): React.ComponentType<Object>);
