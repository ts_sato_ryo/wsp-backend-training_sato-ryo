// @flow

import Api from '../commons/api';
import adapter from './adapters';

import { type AttDailyTimeForRemoteUpdate } from '../domain/models/attendance/AttDailyTime';

export type UpdateResult = {
  insufficientRestTime: ?number,
};

export default {
  /**
   * Excute to create an AttDailyTime
   */
  update: (
    targetDate: string,
    entity: AttDailyTimeForRemoteUpdate
  ): Promise<UpdateResult> => {
    return Api.invoke({
      path: '/att/daily-time/save',
      param: adapter.toRemote({
        targetDate,
        ...entity,
      }),
    });
  },
};
