// @flow

import Api from '../commons/api';
import adapter from './adapters';

import {
  type AttPatternFromRemote,
  type AttPattern,
  createFromRemote,
} from '../domain/models/attendance/AttPattern';

type Result = $ReadOnly<{| patterns: AttPatternFromRemote[] |}>;

export default {
  /**
   * Excute to search an AttDailyPattern
   */
  search: ({
    targetDate,
    ignoredId,
    empId,
  }: {|
    targetDate: string,
    ignoredId?: string,
    empId?: string,
  |}): Promise<AttPattern[]> => {
    return Api.invoke({
      path: '/att/daily-pattern/list',
      param: adapter.toRemote({
        targetDate,
        ignoredId: ignoredId || '',
        empId: empId || '',
      }),
    }).then((result: Result) =>
      result.patterns.map((r) =>
        adapter.fromRemote({ ...r }, [createFromRemote])
      )
    );
  },
};
