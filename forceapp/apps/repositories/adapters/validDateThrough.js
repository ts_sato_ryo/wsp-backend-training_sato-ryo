// @flow

import omit from 'lodash/omit';

import DateUtil from '../../commons/utils/DateUtil';

export const fromRemote = (record: Object): { validDateThrough: string } =>
  'validDateTo' in record
    ? omit(
        {
          ...record,
          validDateThrough: DateUtil.addDays(record.validDateTo, -1),
        },
        'validDateTo'
      )
    : record;

export const toRemote = <TEntity: { validDateThrough: string }>(
  record: TEntity
): Object =>
  'validDateThrough' in record
    ? omit(
        {
          ...record,
          validDateTo: DateUtil.addDays(record.validDateThrough, 1) || null,
        },
        'validDateThrough'
      )
    : record;
