// @flow

import Api from '../commons/api';
import adapter from './adapters';

import {
  type AttSummaryFromRemote,
  type AttSummary,
  createFromRemote,
} from '../domain/models/team/AttSummary';

type SearchQuery = $ReadOnly<{|
  targetYear: string,
  targetMonthly: string,
  departmentId: ?string,
|}>;

const queryToParam = (query: SearchQuery) => ({
  ...query,
  departmentId: query.departmentId || null,
});

export default {
  /**
   * Execute to get an timesheet
   *
   * @param targetYear - 対象年
   * @param targetMonthly - 対象月度
   * @param departmentId - 部署ID
   * {@link https://teamspiritdev.atlassian.net/wiki/spaces/GENIE/pages/894239334/team+att+summary+search}
   */
  search: (query: SearchQuery): Promise<AttSummary> => {
    return Api.invoke({
      path: '/team/att/summary/search',
      param: adapter.toRemote(query, [queryToParam]),
    }).then((result: AttSummaryFromRemote) => {
      return adapter.fromRemote({ ...result }, [createFromRemote]);
    });
  },
};
