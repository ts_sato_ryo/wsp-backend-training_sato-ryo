// @flow

import Api from '../commons/api';
import adapter from './adapters';
import type { Employee } from '../domain/models/organization/Employee';

export type SearchQuery = $Shape<{|
  id?: string,
  companyId?: string,
  targetDate?: string,
  code?: string,
  name?: string,
  departmentId?: string,
  departmentCode?: string,
  departmentName?: string,
  title?: string,
  managerName?: string,
  workingTypeName?: string,
  approvalAuthority01?: boolean,
|}>;

const queryToParam = (query: SearchQuery) => {
  return { ...query };
};

// NOTE
// Api.invoke should be also abstracted for testability?
export default {
  /**
   * Execture search for emloyees with a given query
   */
  search: (query: SearchQuery): Promise<Employee[]> => {
    return Api.invoke({
      path: '/employee/search',
      param: queryToParam(query),
    }).then((result: { records: Object[] }) => {
      return (result.records || []).map((record) => adapter.fromRemote(record));
    });
  },

  /**
   * Exectue to get an employee
   */
  /* Not required for Employee
  fetch: (id: string): Promise<Employee> => {
    return Api.invoke({
      path: '/employee/get',
      param: { id },
    }).then(adapter.fromRemote);
  },
  */

  /**
   * Exectue to update an employee
   */
  update: (entity: Employee): Promise<void> => {
    return Api.invoke({
      path: '/employee/update',
      param: adapter.toRemote(entity),
    });
  },

  /**
   * Exectue to create a new employee
   */
  create: (entity: Employee): Promise<void> => {
    return Api.invoke({
      path: '/employee/create',
      param: adapter.toRemote(entity),
    });
  },

  /**
   * Exectue to delete an employee
   */
  delete: (id: string): Promise<void> => {
    return Api.invoke({
      path: '/employee/delete',
      param: { id },
    });
  },
};
