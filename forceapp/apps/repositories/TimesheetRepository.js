// @flow

import Api from '../commons/api';
import adapter from './adapters';

import {
  type TimesheetFromRemote,
  type Timesheet,
  createFromRemote,
} from '../domain/models/attendance/Timesheet';

export default {
  /**
   * Execute to get an timesheet
   *
   * @param targetDate - 勤務表の取得対象日。YYYY-MM-DD形式の文字列。省略した場合は今日の日付。
   * @param empId - 社員Id。指定しない場合はログインユーザの社員Id。
   * {@link https://teamspiritdev.atlassian.net/wiki/spaces/GENIE/pages/11509595/att+timesheet+get}
   */
  fetch: (targetDate: ?string, empId: ?string): Promise<Timesheet> => {
    return Api.invoke({
      path: '/att/timesheet/get',
      param: adapter.toRemote({
        targetDate,
        empId,
      }),
    }).then((result: TimesheetFromRemote) => {
      return adapter.fromRemote({ ...result }, [createFromRemote]);
    });
  },
};
