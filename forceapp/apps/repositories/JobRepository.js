// @flow

import Api from '../commons/api';
import adapter from './adapters';

export default {
  /**
   * Execture search for entity with a given query
   *
   * TODO:
   * Add a model representing Job
   */

  search: (param: Object): Promise<Object> => {
    return Api.invoke({
      path: '/job/search',
      param,
    }).then((result) => adapter.fromRemote(result));
  },

  /**
   * Exectue to get an entity
   */
  /*
  fetch: (): Promise<PersonalSetting> => {
  },
  */
  /**
   * Exectue to update an entity
   */
  /*
  update: (entity: *): Promise<void> => {
  },
  */
  /**
   * Exectue to create a new entity
   */
  /*
  create: (entity: {||}): Promise<void> => {},
  */
  /**
   * Exectue to delete an employee
   */
  /*
  delete: (id: string): Promise<void> => {
  },
  */
};
