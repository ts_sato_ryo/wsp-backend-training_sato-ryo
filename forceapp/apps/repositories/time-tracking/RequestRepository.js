// @flow

import Api from '../../commons/api';
import adapter from '../adapters';

import { type RequestSummary } from '../../domain/models/time-tracking/RequestSummary';

export default {
  fetchSummary: async (param: {
    requestId: string,
  }): Promise<RequestSummary> => {
    const result = await Api.invoke({
      path: '/time-track/request/summary/get',
      param,
    });
    const entity = adapter.fromRemote(result);
    return entity;
  },
  submit: async (param: {|
    comment: string,
    targetDate: string,
  |}): Promise<void> => {
    await Api.invoke({ path: '/time-track/monthly/apply', param });
  },

  recall: async (param: {|
    requestId: string,
    comment: string,
  |}): Promise<void> => {
    await Api.invoke({ path: '/time/request/cancel', param });
  },
};
