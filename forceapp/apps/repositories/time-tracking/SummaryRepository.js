// @flow
import Api from '../../commons/api';
import adapter from '../adapters';

import type {
  Summary,
  SummaryFromRemoteWithRequest,
} from '../../domain/models/time-tracking/Summary';
import defaultRequest from '../../domain/models/time-tracking/Request';

const toSummaryModel = (entity: SummaryFromRemoteWithRequest): Summary => {
  const request = entity.request || defaultRequest;
  return {
    request,
    useRequest: entity.useRequest,
    ...entity.summary,
  };
};

export default {
  /**
   * Execture search for entity with a given query
   */
  /*
  search: (query: *): Promise<*[]> => {
  },
  */

  /**
   * Exectue to get an entity
   */
  fetchSummary: async (param: {|
    empId?: string,
    targetDate: string,
  |}): Promise<Summary> => {
    const result = await Api.invoke({
      path: '/time-track/summary/get',
      param,
    });
    const entity = adapter.fromRemote(result);
    return toSummaryModel(entity);
  },

  /**
   * Exectue to update an entity
   */
  /*
  update: (entity: *): Promise<void> => {
  },
  */
  /**
   * Exectue to create a new entity
   */
  /*
  create: (entity: {||}): Promise<void> => {},
  */
  /**
   * Exectue to delete an employee
   */
  /*
  delete: (id: string): Promise<void> => {
  },
  */
};
