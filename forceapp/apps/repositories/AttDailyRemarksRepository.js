// @flow

import Api from '../commons/api';
import adapter from './adapters';

export default {
  /**
   * Excute to create an AttDailyTime
   */
  update: (recordId: string, remarks: string): Promise<any> => {
    return Api.invoke({
      path: '/att/daily-remarks/save',
      param: adapter.toRemote({
        recordId,
        remarks,
      }),
    });
  },
};
