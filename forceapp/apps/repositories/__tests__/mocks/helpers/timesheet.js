import AttSummary from '../../../../timesheet-pc/models/AttSummary';
import AttRecord from '../../../../timesheet-pc/models/AttRecord';

function formatDate(date) {
  return [
    date.getFullYear(),
    `0${date.getMonth() + 1}`.slice(-2),
    `0${date.getDate()}`.slice(-2),
  ].join('-');
}

export const requestTypes = {
  Leave: { code: 'Leave', name: '休暇申請' },
  HolidayWork: { code: 'HolidayWork', name: '休日出勤申請' },
  Overtime: { code: 'Overtime', name: '残業申請' },
};

export const requestIds = [
  'a077F000000UyG2QAK',
  'a077F000000UyG2QAN',
  'a077F000000V3wpQAC',
];

export const generateAttWorkingType = () => {
  return {
    startTime: 540,
    endTime: 1080,
  };
};

export const generatePeriodObjectList = () => {
  const periods = [];

  for (let i = 0; i <= 11; i++) {
    const startDate = new Date(2017, i, 1);
    const endDate = new Date(2017, i + 1, 0);

    periods.push({
      name: `${startDate.getFullYear()}年${startDate.getMonth() + 1}月`,
      startDate: formatDate(startDate),
      endDate: formatDate(endDate),
    });
  }

  return periods;
};

export const generateAttDailyRequestObjectMap = () => {
  return {
    [requestIds[0]]: {
      id: requestIds[0],
      requestTypeCode: 'Leave',
      leaveType: 'Paid',
      leaveRange: 'AM',
      status: 'Approved',
    },
    [requestIds[1]]: {
      id: requestIds[1],
      requestTypeCode: 'Leave',
      leaveType: 'Paid',
      leaveRange: 'AM',
      status: 'Approval In',
    },
    [requestIds[2]]: {
      id: requestIds[2],
      requestTypeCode: 'Leave',
      leaveType: 'Unpaid',
      leaveRange: 'AM',
      status: 'Rejected',
    },
  };
};

export const generateAttSummaryObject = () => {
  return {
    id: 'dummy-id',
    status: AttSummary.STATUS.NotRequested,
    isLocked: false,
  };
};

export const generateAttRecordObjectList = () => {
  const attRecords = [];

  for (let i = 1; i <= 31; i++) {
    const date = new Date(2017, 6, i);

    // 日タイプ
    const dayTypeDummyMap = {
      0: AttRecord.DAY_TYPE.PREFERRED_LEGAL_HOLIDAY,
      6: AttRecord.DAY_TYPE.LEGAL_HOLIDAY,
    };
    let dayType =
      dayTypeDummyMap[date.getDate() + 1] || AttRecord.DAY_TYPE.WORKDAY;
    if ([17].includes(date.getDate() + 1)) {
      dayType = AttRecord.DAY_TYPE.PREFERRED_LEGAL_HOLIDAY;
    }
    if ([12].includes(date.getDate() + 1)) {
      dayType = AttRecord.DAY_TYPE.HOLIDAY;
    }

    // 申請可能な勤怠申請タイプコード一覧
    let dailyRequestTypeCodes;
    switch (date.getDay()) {
      case 0:
      case 6:
        dailyRequestTypeCodes = [
          requestTypes.Leave.code,
          requestTypes.HolidayWork.code,
        ];
        break;

      default:
        dailyRequestTypeCodes = [];
    }

    // 提出済み申請リスト
    let dailyRequestIds;
    switch (date.getDay()) {
      case 1:
        dailyRequestIds = [requestIds[0], requestIds[1]];
        break;

      case 2:
        dailyRequestIds = [requestIds[1], requestIds[2]];
        break;

      case 3:
        dailyRequestIds = [requestIds[0], requestIds[2]];
        break;

      case 4:
        dailyRequestIds = [requestIds[0], requestIds[1], requestIds[2]];
        break;

      default:
        dailyRequestIds = [];
    }

    attRecords.push({
      recordDate: formatDate(date),
      dayType,
      startTime: 600,
      endTime: 1140,
      startStampTime: 599,
      endStampTime: 1141,
      requestTypeCodes: dailyRequestTypeCodes,
      requestIds: dailyRequestIds,
    });
  }

  return attRecords;
};
