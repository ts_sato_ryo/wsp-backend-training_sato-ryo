import {
  requestTypes,
  generateAttWorkingType,
  generateAttDailyRequestObjectMap,
  generateAttRecordObjectList,
  generatePeriodObjectList,
} from '../helpers/timesheet';

const response = {
  requestTypes,
  requests: generateAttDailyRequestObjectMap(),
  records: generateAttRecordObjectList(),
  periods: generatePeriodObjectList(),
  workingTypeName: '固定時間制',
  workingType: generateAttWorkingType(),
  startDate: '2017-07-01',
  endDate: '2017-07-31',
};

export default response;
