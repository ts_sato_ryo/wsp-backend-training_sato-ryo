// @flow

import Api from '../commons/api';
import type { DailySummary } from '../domain/models/time-management/DailySummary';
import { convertFromRemote } from '../domain/models/time-management/DailySummary';
import type { Job } from '../domain/models/time-tracking/Job';

export default {
  /**
   * Execture search for entity with a given query
   */
  /*
  search: (query: *): Promise<*[]> => {
  },
  */
  /**
   * Exectue to get an entity
   */
  fetch: async (
    param: {| targetDate: string, empId?: string |},
    options: {
      defaultJob?: Job,
    } = {}
  ): Promise<DailySummary> => {
    const response = await Api.invoke({
      path: '/daily-summary/get',
      param: {
        targetDate: param.targetDate,
        empId: param.empId,
      },
    });
    return convertFromRemote(response, options.defaultJob);
  },
  /**
   * Exectue to update an entity
   */
  update: async (
    param: DailySummary
  ): Promise<$ReadOnly<{| isSuccess: true, result: null |}>> => {
    return Api.invoke({
      path: '/daily-summary/save',
      param,
    });
  },
  /**
   * Exectue to create a new entity
   */
  /*
  create: (entity: {||}): Promise<void> => {},
  */
  /**
   * Exectue to delete an employee
   */
  /*
  delete: (id: string): Promise<void> => {
  },
  */
};
