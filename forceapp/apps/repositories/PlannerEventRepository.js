// @flow

import Api from '../commons/api';
import adapter from './adapters';
import type { Event } from '../domain/models/time-management/Event';
import type { EventMessage } from '../domain/models/time-management/EventMessage';

export default {
  /**
   * Execute search for entity with a given query
   */
  /*
  search: (query: *): Promise<*[]> => {
  },
  */

  /**
   * Execute to get an entity
   */
  fetch: async (param: {|
    startDate: string,
    endDate: string,
    empId?: string,
  |}): Promise<{| events: Event[], messages: EventMessage[] |}> => {
    const { eventList, messageList } = await Api.invoke({
      path: '/planner/event/get',
      param: {
        startDate: param.startDate,
        endDate: param.endDate,
        empId: param.empId,
      },
    });
    return {
      events: (eventList || []).map((event) => adapter.fromRemote(event)),
      messages: (messageList || []).map((message) =>
        adapter.fromRemote(message)
      ),
    };
  },

  /**
   * Execute to update an entity
   */
  /**
  update: async (
  ): Promise<$ReadOnly<{| isSuccess: true, result: null |}>> => {
  },
   */
  /**
   * Execute to create a new entity
   */
  /*
  create: (entity: {||}): Promise<void> => {},
  */
  /**
   * Execute to delete an employee
   */
  /*
  delete: (id: string): Promise<void> => {
  },
  */
};
