// @flow

import Api from '../commons/api';
import adapter from './adapters';

import type { Job } from '../domain/models/time-tracking/Job';

export default {
  /**
   * Execute search for entity with a given query
   */
  search: async (param: {|
    targetDate: string,
    parentItem?: ?Job,
    parentLevelItems?: Job[][],
  |}): Promise<Job[][]> => {
    const { jobList }: { jobList: Job[] } = await Api.invoke({
      path: '/time/job/get',
      param: {
        targetDate: param.targetDate,
        parentId: param.parentItem ? param.parentItem.id : undefined,
      },
    });
    const parentLevelItems: Job[][] = [...(param.parentLevelItems || [])];
    const childLevelItems: Job[] = [
      ...jobList.map((job) => adapter.fromRemote(job)),
    ];
    return [...parentLevelItems, childLevelItems];
  },
  /**
   * Execute to get an entity
   */
  /*
  fetch: (): Promise<PersonalSetting> => {
  },
  */
  /**
   * Execute to update an entity
   */
  /*
  update: (entity: *): Promise<void> => {
  },
  */
  /**
   * Execute to create a new entity
   */
  /*
  create: (entity: {||}): Promise<void> => {},
  */
  /**
   * Execute to delete an employee
   */
  /*
  delete: (id: string): Promise<void> => {
  },
  */
};
