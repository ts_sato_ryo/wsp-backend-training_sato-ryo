// @flow
import type { Dispatch } from 'redux';

import ApprovalType, {
  type ApprovalTypeValue,
} from '../../domain/models/approval/ApprovalType';
import { actions as uiApprovalTypeActions } from '../modules/ui/approvalType';
import { actions as uiFilterTermActions } from '../modules/ui/attMonthly/list/filterTerms';
import { actions as entitiesListActions } from '../modules/entities/attMonthly/list';

export const initialize = () => (dispatch: Dispatch<any>) => {
  dispatch(uiFilterTermActions.clear());
  dispatch(entitiesListActions.browse(ApprovalType.ByEmployee));
};

export const switchApprovalType = (type: ApprovalTypeValue) => (
  dispatch: Dispatch<any>
) => {
  dispatch(uiApprovalTypeActions.switch(type));
  dispatch(uiFilterTermActions.clear());
  dispatch(entitiesListActions.clear());
  return dispatch(entitiesListActions.browse(type));
};

export default { switchApprovalType };
