// @flow
import type { Dispatch } from 'redux';
import { withLoading, catchApiError } from '../../commons/actions/app';
import { fetchSuccess } from '../modules/entities/timeTrack/detail';
import { fetch } from '../../domain/models/time-tracking/TrackRequest';
import History from './History';

export default (dispatch: Dispatch<Object>) => {
  return {
    fetch: (requestId: string) => {
      dispatch(
        withLoading(async () => {
          const history = History(dispatch);
          const [records] = await Promise.all([
            fetch(requestId),
            history.fetch(requestId),
          ]).catch((err) => catchApiError(err, { isContinuable: false }));
          dispatch(fetchSuccess(records));
        })
      );
    },
  };
};
