// @flow
import type { Dispatch } from 'redux';
import { withLoading } from '../../commons/actions/app';
import { getRequestApprovalHistory } from '../../domain/models/approval/request/History';
import { fetchSuccess } from '../modules/entities/histories';

export default (dispatch: Dispatch<Object>) => {
  return {
    fetch: async (requestId: string): Promise<void> => {
      dispatch(
        withLoading(async () => {
          const history = await getRequestApprovalHistory(requestId);
          dispatch(fetchSuccess(history.historyList));
        })
      );
    },
  };
};
