// @flow
import _ from 'lodash';

import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';

import { actions as expRequestActions } from '../../domain/modules/exp/request/report';
import { actions as expPreApprovalActions } from '../../domain/modules/exp/request/pre-request';
import { actions as approvalActions } from '../../domain/modules/approval/request/approve';
import { actions as rejectActions } from '../../domain/modules/approval/request/reject';
import { actions as commentActions } from '../modules/ui/expenses/detail/comment';
import { actions as activeDialogActions } from '../modules/ui/expenses/dialog/activeDialog';
import { actions as receiptLibraryAction } from '../../domain/modules/exp/receiptLibrary/list';
import {
  actions as pageActions,
  PAGE_SIZE,
} from '../modules/ui/expenses/list/page';
import {
  type SearchConditions,
  type ExpRequestList,
} from '../../domain/models/exp/request/Report';

import FileUtil from '../../commons/utils/FileUtil';

const getActions = (isPreApproval: boolean) => {
  return isPreApproval ? expPreApprovalActions : expRequestActions;
};

const browseDetail = (requestId: string, isPreApproval: boolean) => {
  const expActions = getActions(isPreApproval);
  return (dispatch: Dispatch<any>) => {
    dispatch(loadingStart());
    dispatch(expActions.get(requestId))
      .then((res) => {
        dispatch(commentActions.clear());
        const fileVerId = _.get(res, 'payload.attachedFileVerId');
        if (fileVerId) {
          dispatch(receiptLibraryAction.get(fileVerId)).then((fileInfo) => {
            dispatch(loadingEnd());
            const fileBody = fileInfo.payload.fileBody;
            const dataType = FileUtil.getMIMEType(fileInfo.payload.fileType);
            const url = `data:${dataType};base64,${fileBody}`;
            const fileName = FileUtil.getOriginalFileNameWithoutPrefix(
              fileInfo.payload.title
            );
            dispatch(
              expActions.setImageData(requestId, url, fileName, dataType)
            );
          });
        }
      })
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
      .then(() => {
        dispatch(loadingEnd());
      });
  };
};

export const browseDetailForExpApproval = (requestId: string) =>
  browseDetail(requestId, false);

export const browseDetailForPreApproval = (requestId: string) =>
  browseDetail(requestId, true);

const fetchExpList = (
  isPreApproval: boolean,
  requestIdList: string[],
  pageNum: number,
  empId?: string,
  isEmpty?: boolean
) => {
  return (dispatch: Dispatch<any>) => {
    const expActions = getActions(isPreApproval);
    const idsCurrentPage = requestIdList.slice(
      PAGE_SIZE * (pageNum - 1),
      PAGE_SIZE * pageNum
    );
    dispatch(loadingStart());
    dispatch(expActions.list(idsCurrentPage, empId, isEmpty))
      .then((res) => {
        dispatch(pageActions.set(pageNum));
        const firstId = res.payload[0].requestId;
        browseDetail(firstId, isPreApproval)(dispatch);
      })
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
      .finally(() => dispatch(loadingEnd()));
  };
};

export const fetchExpListForExpApproval = (
  requestIdList: string[],
  pageNum: number,
  empId?: string,
  isEmpty?: boolean
) => fetchExpList(false, requestIdList, pageNum, empId, isEmpty);

export const fetchExpListForPreApproval = (
  requestIdList: string[],
  pageNum: number,
  empId?: string,
  isEmpty?: boolean
) => fetchExpList(true, requestIdList, pageNum, empId, isEmpty);

const fetchAllIds = (
  isPreApproval: boolean,
  searchCondition: SearchConditions,
  empId?: string,
  isEmpty?: boolean
) => {
  const expActions = getActions(isPreApproval);
  return (dispatch: Dispatch<any>) => {
    dispatch(loadingStart());
    dispatch(expActions.listIds(searchCondition, empId, isEmpty))
      .then((ret) => {
        const ids = ret.payload.requestIdList;
        if (ids.length > 0) {
          fetchExpList(isPreApproval, ids, 1, empId, isEmpty)(dispatch);
        } else if (ids.length === 0) {
          dispatch(expActions.clear());
        }
      })
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
      .finally(() => dispatch(loadingEnd()));
  };
};
export const fetchAllIdsForExpApproval = (
  searchCondition: SearchConditions,
  empId?: string,
  isEmpty?: boolean
) => fetchAllIds(false, searchCondition, empId, isEmpty);

export const fetchAllIdsForPreApproval = (
  searchCondition: SearchConditions,
  empId?: string,
  isEmpty?: boolean
) => fetchAllIds(true, searchCondition, empId, isEmpty);

const approve = (
  requestIdList: Array<string>,
  comment: string,
  isPreApproval: boolean,
  allIds: Array<string>,
  pageNum: number,
  requestList: ExpRequestList,
  proxyEmployeeId?: string
) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(loadingStart());
    dispatch(approvalActions.approve(requestIdList, comment))
      .then(() => {
        dispatch(commentActions.clear());
        const currentId = requestIdList[0];
        const index = _.indexOf(allIds, currentId);
        const isLastOne = index >= allIds.length - 1;
        const isLastOneInCurrentPage = index === pageNum * PAGE_SIZE - 1;
        const expActions = getActions(isPreApproval);
        dispatch(expActions.setStatus(requestList, currentId, 'Approved'));
        if (isLastOne) {
          browseDetail(allIds[index], isPreApproval)(dispatch);
        } else if (isLastOneInCurrentPage) {
          fetchExpList(isPreApproval, allIds, pageNum + 1, proxyEmployeeId)(
            dispatch
          );
        } else {
          browseDetail(allIds[index + 1], isPreApproval)(dispatch);
        }
      })
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
      .then(() => {
        dispatch(loadingEnd());
      });
  };
};

export const approvalForExpApproval = (
  requestIdList: Array<string>,
  comment: string,
  allIds: Array<string>,
  pageNum: number,
  requestList: ExpRequestList,
  proxyEmployeeId?: string
) =>
  approve(
    requestIdList,
    comment,
    false,
    allIds,
    pageNum,
    requestList,
    proxyEmployeeId
  );

export const approvalForPreApproval = (
  requestIdList: Array<string>,
  comment: string,
  allIds: Array<string>,
  pageNum: number,
  requestList: ExpRequestList,
  proxyEmployeeId?: string
) =>
  approve(
    requestIdList,
    comment,
    true,
    allIds,
    pageNum,
    requestList,
    proxyEmployeeId
  );

const reject = (
  requestIdList: Array<string>,
  comment: string,
  isPreApproval: boolean,
  allIds: Array<string>,
  pageNum: number,
  requestList: ExpRequestList,
  proxyEmployeeId?: string
) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(loadingStart());
    dispatch(rejectActions.reject(requestIdList, comment))
      .then(() => {
        dispatch(commentActions.clear());
        const currentId = requestIdList[0];
        const index = _.indexOf(allIds, currentId);
        const isLastOne = index >= allIds.length - 1;
        const isLastOneInCurrentPage = index === pageNum * PAGE_SIZE - 1;
        const expActions = getActions(isPreApproval);
        dispatch(expActions.setStatus(requestList, currentId, 'Rejected'));
        if (isLastOne) {
          browseDetail(allIds[index], isPreApproval)(dispatch);
        } else if (isLastOneInCurrentPage) {
          fetchExpList(isPreApproval, allIds, pageNum + 1, proxyEmployeeId)(
            dispatch
          );
        } else {
          browseDetail(allIds[index + 1], isPreApproval)(dispatch);
        }
      })
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
      .then(() => {
        dispatch(loadingEnd());
      });
  };
};

export const rejectForExpApproval = (
  requestIdList: Array<string>,
  comment: string,
  allIds: Array<string>,
  pageNum: number,
  requestList: ExpRequestList,
  proxyEmployeeId?: string
) =>
  reject(
    requestIdList,
    comment,
    false,
    allIds,
    pageNum,
    requestList,
    proxyEmployeeId
  );

export const rejectForPreApproval = (
  requestIdList: Array<string>,
  comment: string,
  allIds: Array<string>,
  pageNum: number,
  requestList: ExpRequestList,
  proxyEmployeeId?: string
) =>
  reject(
    requestIdList,
    comment,
    true,
    allIds,
    pageNum,
    requestList,
    proxyEmployeeId
  );

export const getFilePreview = (receiptFileId: string) => {
  return (dispatch: Dispatch<any>) => {
    dispatch(loadingStart());
    return dispatch(receiptLibraryAction.get(receiptFileId))
      .then((res) => {
        return res;
      })
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
      .finally(dispatch(loadingEnd()));
  };
};

export const openRecordItemsConfirmDialog = () => (dispatch: Dispatch<any>) => {
  dispatch(activeDialogActions.recordItemsConfirm());
};
