// @flow
import type { Dispatch } from 'redux';

import type { Permission } from '../../domain/models/access-control/Permission';
import { getUserSetting } from '../../commons/actions/userSetting';
import { setUserPermission } from '../../commons/modules/accessControl/permission';
import { selectTab } from '../modules/ui/tabs';
import { actions as approvalTypeUIActions } from '../modules/ui/approvalType';

export const initialize = (param: { userPermission: Permission }) => (
  dispatch: Dispatch<any>
) => {
  if (param) {
    dispatch(setUserPermission(param.userPermission));
  }

  dispatch(getUserSetting());
};

export const selectRequestType = (
  selectedType: string,
  currentType?: string
) => (dispatch: Dispatch<any>) => {
  if (selectedType !== currentType) {
    dispatch(selectTab(selectedType));
    dispatch(approvalTypeUIActions.reset());
  }
};

export default initialize;
