// @flow
export type ComplexMatchersType = {
  [string]: ({ [string]: string }, { [string]: * }) => boolean,
};

export const extractUniqueValues = <T: Object>(
  recordList: T[],
  key: $Keys<T>
): string[] => {
  const values = new Set();
  recordList.forEach((record) => values.add(record[key]));
  return Array.from(values.values());
};

const getTimeFromISODateString = (ISODateString: string): number => {
  const [year, month, date] = ISODateString.split('-');
  return new Date(Number(year), Number(month) - 1, Number(date)).getTime();
};

export const dateInPeriodMatcher = (
  startDate: string,
  endDate: string,
  targetDate: string
): boolean => {
  if (targetDate === '') {
    return true;
  }

  const targetTime = getTimeFromISODateString(targetDate);
  return (
    getTimeFromISODateString(startDate) <= targetTime &&
    targetTime <= getTimeFromISODateString(endDate)
  );
};

const defaultMatcher = (filterTerm: string, value: string): boolean =>
  filterTerm === '' || (value || '').indexOf(filterTerm) >= 0;

// NOTE: (extraMatchers) => (filterTerms) => (record) => boolean
export const buildRecordFilter = <
  T: { [key: string]: string },
  R: { [key: string]: * }
>(extraMatchers: {
  [$Keys<T>]: (T, R) => boolean,
}) => (filterTerms: T) => (record: R): boolean =>
  !Object.keys(filterTerms).some((key) =>
    extraMatchers[key]
      ? !extraMatchers[key](filterTerms, record)
      : !defaultMatcher(filterTerms[key], record[key])
  );

export default buildRecordFilter;
