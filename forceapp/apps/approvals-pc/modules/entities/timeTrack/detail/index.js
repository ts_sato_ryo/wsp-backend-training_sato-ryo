import { constants as requestConstants } from '../../../ui/timeTrack/request';

// CONSTANTS
const FETCH_SUCCESS = 'MODULES/ENTITIES/TIME_TRACK/DETAIL/FETCH_SUCCESS';

export const constants = {
  FETCH_SUCCESS,
};

// ACTIONS
export const fetchSuccess = (payload) => {
  return {
    type: FETCH_SUCCESS,
    payload,
  };
};

// REDUCER
const initialState = {
  employeeName: '',
  employeePhotoUrl: '',
  status: '',
  comment: '',
  startDate: '',
  endDate: '',
  dailyTrackList: {},
  taskList: {
    allIds: [],
    byId: {},
  },
  taskIdList: [],
  requestId: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUCCESS:
      return {
        employeeName: action.payload.employeeName,
        employeePhotoUrl: action.payload.employeePhotoUrl,
        status: action.payload.status,
        comment: action.payload.comment,
        startDate: action.payload.startDate,
        endDate: action.payload.endDate,
        dailyTrackList: action.payload.dailyList,
        taskList: {
          byId: action.payload.taskList,
          allIds: action.payload.summaryTaskAllIds,
        },
        requestId: action.payload.requestId,
      };
    case requestConstants.APPROVE_SUCCESS:
    case requestConstants.REJECT_SUCCESS:
      return initialState;
    default:
      return state;
  }
};
