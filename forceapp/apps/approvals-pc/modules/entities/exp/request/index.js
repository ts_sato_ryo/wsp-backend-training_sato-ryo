import { combineReducers } from 'redux';

import report from '../../../../../domain/modules/exp/request/report';
import preRequest from '../../../../../domain/modules/exp/request/pre-request';

export default combineReducers({
  report,
  preRequest,
});
