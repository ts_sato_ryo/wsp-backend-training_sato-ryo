// @flow
import { type Reducer, type Dispatch } from 'redux';
import {
  type EmployeeList,
  getEmployeeList,
} from '../../../../../domain/models/exp/Employee';
import { type EmployeeOption } from '../../../../../domain/models/exp/request/Report';

const ACTIONS = {
  LIST_SUCCESS:
    'MODULES/ENTITIES/EXPENSES/ADV_SEARCH/EMPLOYEE_LIST/LIST_SUCCESS',
};

const listSuccess = (employeeList: EmployeeList) => ({
  type: ACTIONS.LIST_SUCCESS,
  payload: employeeList,
});

export const actions = {
  list: (companyId?: ?string, targetDate?: ?string) => (
    dispatch: Dispatch<any>
  ): Promise<EmployeeList> => {
    return getEmployeeList(companyId, targetDate).then((res: EmployeeList) =>
      dispatch(listSuccess(res))
    );
  },
};

const convertStyle = (employeeList: EmployeeList) => {
  const options = employeeList.map((employee) => {
    const employeeOption: EmployeeOption = {
      label: employee.name,
      value: employee.id,
    };
    return employeeOption;
  });
  return options;
};

const initialState: Array<EmployeeOption> = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.LIST_SUCCESS:
      return convertStyle(action.payload);
    default:
      return state;
  }
}: Reducer<Array<EmployeeOption>, any>);
