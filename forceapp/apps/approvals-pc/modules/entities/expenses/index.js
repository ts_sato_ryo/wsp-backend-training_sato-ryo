import { combineReducers } from 'redux';

import detail from './detail';
import advSearch from './advSearch';

export default combineReducers({
  detail,
  advSearch,
});
