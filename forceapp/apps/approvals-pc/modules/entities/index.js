import { combineReducers } from 'redux';

import att from './att';
import attMonthly from './attMonthly';
import timeTrack from './timeTrack';
import expenses from './expenses';
import exp from './exp';
import delegateApprover from './delegateApprover';
import histories from './histories';

export default combineReducers({
  att,
  attMonthly,
  timeTrack,
  expenses,
  exp,
  delegateApprover,
  histories,
});
