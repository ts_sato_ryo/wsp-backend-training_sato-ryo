import { createSelector } from 'reselect';

import Api from '../../../../../commons/api';

import {
  CODE as ATT_DAILY_ATTENTION_CODE,
  createAttDailyAttentions,
} from '../../../../../domain/models/attendance/AttDailyAttention';

import * as appActions from '../../../../../commons/actions/app';

/** Define constants */

const FETCH_SUCCESS = 'MODULES/ENTITIES/ATT_MONTHLY/DETAIL/FETCH_SUCCESS';
// Used when there're no requests to show
const CLEAR = 'MODULES/ENTITIES/ATT_MONTHLY/DETAIL/CLEAR';

export const constants = { FETCH_SUCCESS, CLEAR };

/** Define converters */

const addAttributesToItem = (item) => {
  switch (item.name) {
    case 'AnnualPaidLeaveDaysLeft':
      return {
        ...item,
        isAsAtClosingDate: true,
      };

    case 'GeneralPaidLeaveDays':
    case 'UnpaidLeaveDays':
      return {
        ...item,
        hasTranslatedNameInItems: true,
      };

    default:
      return item;
  }
};

const convertRecord = (record) => ({
  recordDate: record.recordDate,
  dayType: record.dayType,
  event: record.event,
  shift: record.shift,
  startTime: record.startTime,
  startTimeModified:
    record.startTime !== null && record.startTime !== record.startStampTime,
  endTime: record.endTime,
  endTimeModified:
    record.endTime !== null && record.endTime !== record.endStampTime,
  restTime: record.restTime,
  realWorkTime: record.realWorkTime,
  overTime: record.overTime,
  nightTime: record.nightTime,
  lostTime: record.lostTime,
  virtualWorkTime: record.virtualWorkTime,
  holidayWorkTime: record.holidayWorkTime,
  insufficientRestTime: record.insufficientRestTime,
  remarks: record.remarks,
  outStartTime: record.outStartTime,
  outEndTime: record.outEndTime,
});

const convertRecords = (records) => ({
  allIds: records.map((record) => record.recordDate),
  byId: Object.assign(
    {},
    ...records.map((record) => ({
      [record.recordDate]: convertRecord(record),
    }))
  ),
});

const convertSummaryItems = (items) => ({
  allIds: items.map((item) => item.name),
  byId: Object.assign(
    {},
    ...items.map((item) => ({ [item.name]: addAttributesToItem(item) }))
  ),
});

const convertSummaries = (summaries) => ({
  allIds: summaries.map((summary) => summary.name),
  byId: Object.assign(
    {},
    ...summaries.map((summary) => ({
      [summary.name]: {
        name: summary.name,
        items: convertSummaryItems(summary.items),
      },
    }))
  ),
});

const convertHistoryList = (historyList) => ({
  allIds: historyList.map((history) => history.id),
  byId: Object.assign(
    {},
    ...historyList.map((history) => ({ [history.id]: history }))
  ),
});

const convertAttentions = (records) => {
  const allIds = records.map(({ recordDate }) => recordDate);
  const byId = records.reduce((obj, record) => {
    obj[record.recordDate] = createAttDailyAttentions(record);
    return obj;
  }, {});

  const counts = {};

  counts.ineffectiveWorkingTime = allIds.filter((id) =>
    byId[id].some(
      ({ code }) => code === ATT_DAILY_ATTENTION_CODE.IneffectiveWorkingTime
    )
  ).length;
  counts.insufficientRestTime = allIds.filter((id) =>
    byId[id].some(
      ({ code }) => code === ATT_DAILY_ATTENTION_CODE.InsufficientRestTime
    )
  ).length;

  return {
    allIds,
    byId,
    counts,
  };
};

const convertDetailEntity = (body) => {
  const detailEntity = {
    id: body.id,
    status: body.status,
    employeeName: body.employeeName,
    employeePhotoUrl: body.employeePhotoUrl,
    delegatedEmployeeName: body.delegatedEmployeeName,
    comment: body.comment,
    records: convertRecords(body.records),
    historyList: convertHistoryList(body.historyList),
    summaries: convertSummaries(body.summaries),
    attentions: convertAttentions(body.records),
  };

  return detailEntity;
};

export const converters = { convertDetailEntity };

/** Define actions */

const fetchSuccess = (result) => (dispatch) => {
  dispatch({
    type: FETCH_SUCCESS,
    payload: convertDetailEntity(result),
  });
};

const browse = (requestId) => (dispatch) => {
  dispatch(appActions.loadingStart());

  const req = {
    path: '/att/request/monthly/get',
    param: {
      requestId,
    },
  };

  return Api.invoke(req)
    .then((result) => dispatch(fetchSuccess(result)))
    .catch((err) =>
      dispatch(appActions.catchApiError(err, { isContinuable: true }))
    )
    .then(() => dispatch(appActions.loadingEnd()));
};

const clear = () => ({
  type: CLEAR,
});

export const actions = { browse, clear };

/** Define selectors */

const getRecords = (state) => state.entities.attMonthly.detail.records;

const getHistoryList = (state) => state.entities.attMonthly.detail.historyList;

// $FlowFixMe
const recordsSelector = createSelector(
  getRecords,
  (records) => records.allIds.map((id) => records.byId[id])
);

// $FlowFixMe
const closingDateSelector = createSelector(
  getRecords,
  (records) => {
    const dateStrList = records.allIds || [];
    return dateStrList[dateStrList.length - 1];
  }
);

const totalTimeSelectorCreator = (fieldGetter) =>
  // $FlowFixMe
  createSelector(
    getRecords,
    (records) =>
      records.allIds
        .map((id) => records.byId[id])
        .map(fieldGetter)
        .filter((field) => !!field)
        .reduce((total, fieldValue) => total + fieldValue, 0)
  );

const restTimeTotalSelector = totalTimeSelectorCreator(
  (record) => record.restTime
);
const realWorkTimeTotalSelector = totalTimeSelectorCreator(
  (record) => record.realWorkTime
);
const overTimeTotalSelector = totalTimeSelectorCreator(
  (record) => record.overTime
);
const nightTimeTotalSelector = totalTimeSelectorCreator(
  (record) => record.nightTime
);
const lostTimeTotalSelector = totalTimeSelectorCreator(
  (record) => record.lostTime
);
const virtualWorkTimeTotalSelector = totalTimeSelectorCreator(
  (record) => record.virtualWorkTime
);
const holidayWorkTimeTotalSelector = totalTimeSelectorCreator(
  (record) => record.holidayWorkTime
);

const getSummaries = (state) => state.entities.attMonthly.detail.summaries;

// Aggregate summary selectors to one selector to achieve better performance.
// $FlowFixMe
const summariesSelector = createSelector(
  getSummaries,
  (summaries) =>
    summaries.allIds
      .map((id) => summaries.byId[id])
      .map((summary) => ({
        name: summary.name,
        items: summary.items.allIds.map((id) => summary.items.byId[id]),
      }))
);

// $FlowFixMe
const historyListSelector = createSelector(
  getHistoryList,
  (historyList) => historyList.allIds.map((id) => historyList.byId[id])
);

export const selectors = {
  historyListSelector,
  recordsSelector,
  closingDateSelector,
  restTimeTotalSelector,
  realWorkTimeTotalSelector,
  overTimeTotalSelector,
  nightTimeTotalSelector,
  lostTimeTotalSelector,
  virtualWorkTimeTotalSelector,
  holidayWorkTimeTotalSelector,
  summariesSelector,
};

/** Define reducer */

const initialState = {
  id: '',
  status: '',
  employeeName: '',
  employeePhotoUrl: '',
  delegatedEmployeeName: '',
  comment: '',
  historyList: {
    allIds: [],
    byId: {},
  },
  records: {
    allIds: [],
    byId: {},
  },
  summaries: {
    allIds: [],
    byId: {},
  },
  attentions: {
    allIds: [],
    byId: {},
    counts: {},
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case CLEAR:
      return initialState;
    default:
      return state;
  }
};
