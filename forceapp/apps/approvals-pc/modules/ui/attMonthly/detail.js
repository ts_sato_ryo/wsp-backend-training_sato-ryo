// @flow
import { type Dispatch } from 'redux';

import Api from '../../../../commons/api';

import * as appActions from '../../../../commons/actions/app';
import { actions as listActions } from '../../entities/attMonthly/list';
import type { ApprovalTypeValue } from '../../../../domain/models/approval/ApprovalType';
import { getNextId } from '../../../utils/RequestListUtils';

type State = {
  comment: string,
};

/** Define constants */

const EDIT_COMMENT = 'MODULES/UI/ATT_MONTHLY/REQUEST/EDIT_COMMENT';
const PROCESS_SUCCESS = 'MODULES/UI/ATT_MONTHLY/REQUEST/PROCEED_SUCCESS';

export const constants = { PROCESS_SUCCESS, EDIT_COMMENT };

/** Define actions */

const processSuccess = () => ({
  type: PROCESS_SUCCESS,
});

const PROCESS_TYPE_APPROVE = 'approve';
const PROCESS_TYPE_REJECT = 'reject';

const buildProcessRequest = (processType, requestIdList, comment) => {
  switch (processType) {
    case PROCESS_TYPE_APPROVE:
      return {
        path: '/approval/request/approve',
        param: {
          requestIdList,
          comment,
        },
      };
    case PROCESS_TYPE_REJECT:
      return {
        path: '/approval/request/reject',
        param: {
          requestIdList,
          comment,
        },
      };
    default:
      // This line should not be evaluated
      return {};
  }
};

const editComment = (comment: string) => ({
  type: EDIT_COMMENT,
  payload: {
    comment,
  },
});

const proceed = (
  processType,
  requestIdList: string[],
  comment: string = '',
  approvalType: ApprovalTypeValue,
  allIds: string[]
) => (dispatch: Dispatch<any>) => {
  const nextId = getNextId(requestIdList, allIds);

  const req = buildProcessRequest(processType, requestIdList, comment);

  dispatch(appActions.loadingStart());
  return Api.invoke(req)
    .then(() => {
      dispatch(processSuccess());
      dispatch(listActions.browse(approvalType, nextId));
    })
    .catch((err) =>
      dispatch(appActions.catchApiError(err, { isContinuable: true }))
    )
    .then(() => dispatch(appActions.loadingEnd()));
};

/**
 * Approve the approvals.
 * @param {array} requestIdList
 * @param {string} comment
 * @param approvalType
 * @param allIds
 * @return {function(Dispatch<*>): Promise<T | never>}
 */
const approve = (
  requestIdList: string[],
  comment: string = '',
  approvalType: ApprovalTypeValue,
  allIds: string[]
) =>
  proceed(PROCESS_TYPE_APPROVE, requestIdList, comment, approvalType, allIds);

/**
 * Reject the approvals.
 * @param {array} requestIdList
 * @param {string} comment
 * @param approvalType
 * @param allIds
 * @return {function(Dispatch<*>): Promise<T | never>}
 */
const reject = (
  requestIdList: string[],
  comment: string = '',
  approvalType: ApprovalTypeValue,
  allIds: string[]
) => proceed(PROCESS_TYPE_REJECT, requestIdList, comment, approvalType, allIds);

export const actions = { editComment, approve, reject };

/** Define reducer */

const initialState: State = {
  comment: '',
};

export default (state: State = initialState, action: *) => {
  switch (action.type) {
    case EDIT_COMMENT:
      return {
        ...state,
        ...action.payload,
      };
    case PROCESS_SUCCESS:
      return initialState;
    default:
      return state;
  }
};
