import { combineReducers } from 'redux';

import detail from './detail';
import list from './list';
import isExpanded from './isExpanded';

export default combineReducers({
  detail,
  list,
  isExpanded,
});
