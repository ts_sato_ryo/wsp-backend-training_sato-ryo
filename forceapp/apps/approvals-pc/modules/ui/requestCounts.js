import Api from '../../../commons/api';
import * as appActions from '../../../commons/actions/app';
import { constants as timeTrackRequestConstants } from './timeTrack/request';
import * as attRequestConstants from './att/request/constants';
import { constants as attMonthlyDetailConstants } from './attMonthly/detail';
import { ACTIONS as APPROVE_ACTIONS } from '../../../domain/modules/approval/request/approve';
import { ACTIONS as REJECT_ACTIONS } from '../../../domain/modules/approval/request/reject';
import { constants as PROXY_CONSTANTS } from '../../../commons/modules/proxyEmployeeInfo';
import { ACTIONS as SWITCH_APPROVER_ACTIONS } from '../../../../widgets/dialogs/SwitchApporverDialog/modules/ui';
import { constants as tabsConstants } from './tabs';

//
// constants
//
const FETCH_SUUCESS = 'MODULES/UI/REQUEST_COUNTS/FETCH_SUUCESS';

//
// actions
//
const convertCount = (res) => {
  return {
    attDaily: res.attDailyRequestCount,
    attMonthly: res.attRequestCount,
    timeRequest: res.timeRequestCount,
    expenses: res.expReportRequestCount,
    expPreApproval: res.expPreApprovalRequestCount,
  };
};

const update = (empId, isDelegatedApprover) => (dispatch) => {
  const req = {
    path: '/approval/request-count/get',
    param: { empId, isDelegatedApprover },
  };

  Api.invoke(req)
    .catch((err) =>
      dispatch(appActions.catchApiError(err, { isContinuable: true }))
    )
    .then((res) => {
      dispatch({
        type: FETCH_SUUCESS,
        payload: convertCount(res),
      });
    });
};

const updateCountsAction = [
  timeTrackRequestConstants.APPROVE_SUCCESS,
  timeTrackRequestConstants.REJECT_SUCCESS,
  attRequestConstants.APPROVE_SUCCESS,
  attRequestConstants.REJECT_SUCCESS,
  attMonthlyDetailConstants.PROCESS_SUCCESS,
  tabsConstants.SELECT,
  APPROVE_ACTIONS.APPROVE,
  REJECT_ACTIONS.REJECT,
  SWITCH_APPROVER_ACTIONS.SELECT,
  PROXY_CONSTANTS.UNSET,
];

//
// middlewares
//
const updateMiddleware = ({ dispatch, getState }) => (next) => (action) => {
  if (updateCountsAction.includes(action.type)) {
    let empId;
    let isDelegatedApprover = false;
    const currentState = getState();
    const { isProxyMode } = currentState.common.proxyEmployeeInfo;
    // Switch to proxy employee
    if (isProxyMode || action.type === SWITCH_APPROVER_ACTIONS.SELECT) {
      empId = currentState.common.proxyEmployeeInfo.id;
      isDelegatedApprover = true;
    }
    // Switch back to original user
    if (!isProxyMode || action.type === PROXY_CONSTANTS.UNSET) {
      empId = currentState.userSetting.employeeId;
      isDelegatedApprover = false;
    }
    dispatch(update(empId, isDelegatedApprover));
  }

  return next(action);
};

// まとめてpushするだけなので配列としてもつ
export const middlewares = [updateMiddleware];

export const actions = {
  update,
};

//
// Reducer
//
const initialState = {
  attDaily: null,
  attMonthly: null,
  timeRequest: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUUCESS:
      return Object.assign({}, action.payload);
    default:
      return state;
  }
};
