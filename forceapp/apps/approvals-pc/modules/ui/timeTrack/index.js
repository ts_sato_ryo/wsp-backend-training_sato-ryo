import { combineReducers } from 'redux';

import request from './request';
import isExpanded from './isExpanded';

export default combineReducers({
  request,
  isExpanded,
});
