// @flow

import { combineReducers } from 'redux';

import tabs from './tabs';
import requestCounts from './requestCounts';
import approvalType from './approvalType';
import att from './att';
import attMonthly from './attMonthly';
import timeTrack from './timeTrack';
import expenses from './expenses';
import delegateApprover from './delegateApprover';

export default combineReducers<Object, Object>({
  tabs,
  requestCounts,
  approvalType,
  att,
  attMonthly,
  timeTrack,
  expenses,
  delegateApprover,
});
