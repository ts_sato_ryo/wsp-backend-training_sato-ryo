import { combineReducers } from 'redux';

import request from './request';
import list from './list';
import isExpanded from './isExpanded';

export default combineReducers({
  list,
  request,
  isExpanded,
});
