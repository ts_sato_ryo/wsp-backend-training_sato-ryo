// @flow
import { createSelector } from 'reselect';

import { requestListSelector } from '../../../entities/att/list';
import {
  buildRecordFilter,
  dateInPeriodMatcher,
  extractUniqueValues,
  type ComplexMatchersType,
} from '../../../../utils/FilterUtils';

export type State = {
  employeeName: string,
  departmentName: string,
  approverName: string,
  approverDepartmentName: string,
  requestPeriod: string,
  type: string,
  requestDate: string,
};

const complexMatchers: ComplexMatchersType = {
  requestPeriod: (filterTerms, record) =>
    dateInPeriodMatcher(
      record.startDate,
      record.endDate,
      filterTerms.requestPeriod
    ),
};
type RecordFilter = (filterTerms: Object) => (a: Object) => boolean;
const recordFilterByTerm: RecordFilter = buildRecordFilter(complexMatchers);

// $FlowFixMe
const extractRecordsByFilter = createSelector(
  (state) => state.ui.att.list.filterTerms,
  requestListSelector,
  (filterTerms, recordList) => {
    const filter = recordFilterByTerm(filterTerms);
    return recordList.filter(filter);
  }
);

// $FlowFixMe
const extractIdsByFilter = createSelector(
  extractRecordsByFilter,
  (records) => records.map((record) => record.id)
);

// $FlowFixMe
const buildRequestTypesOptions = createSelector(
  requestListSelector,
  (state) => state.ui.att.list.filterTerms.type,
  (recordList, typeFilterTerms) => {
    const types = extractUniqueValues(recordList, 'type');
    if (typeFilterTerms !== '' && !types.includes(typeFilterTerms)) {
      types.push(typeFilterTerms);
    }
    return types;
  }
);

export const selectors = {
  extractRecordsByFilter,
  extractIdsByFilter,
  buildRequestTypesOptions,
};

const ACTIONS = {
  UPDATE: 'MODULES/UI/ATT/LIST/FILTER_TERMS/UPDATE',
  CLEAR: 'MODULES/UI/ATT/LIST/FILTER_TERMS/CLEAR',
};

type UpdateAction = {
  type: 'MODULES/UI/ATT/LIST/FILTER_TERMS/UPDATE',
  payload: { key: $Keys<State>, value: string },
};

type ClearAction = {
  type: 'MODULES/UI/ATT/LIST/FILTER_TERMS/CLEAR',
};

export const actions = {
  update: (key: $Keys<State>, value: string): UpdateAction => ({
    type: ACTIONS.UPDATE,
    payload: { key, value },
  }),

  clear(): ClearAction {
    return { type: ACTIONS.CLEAR };
  },
};

type Action = UpdateAction | ClearAction;

const initialState: State = {
  employeeName: '',
  departmentName: '',
  approverName: '',
  approverDepartmentName: '',
  requestPeriod: '',
  type: '',
  requestDate: '',
};

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case ACTIONS.UPDATE: {
      const { payload } = (action: UpdateAction);
      return {
        ...state,
        [payload.key]: payload.value,
      };
    }

    case ACTIONS.CLEAR:
      return initialState;

    default:
      return state;
  }
};
