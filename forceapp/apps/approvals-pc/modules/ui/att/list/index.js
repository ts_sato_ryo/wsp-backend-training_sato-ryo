import { combineReducers } from 'redux';

import selectedIds from './selectedIds';
import filterTerms from './filterTerms';

export default combineReducers({
  selectedIds,
  filterTerms,
});
