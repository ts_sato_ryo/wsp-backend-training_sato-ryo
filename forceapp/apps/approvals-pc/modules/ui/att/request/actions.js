// @flow
import { type Dispatch } from 'redux';

import Api from '../../../../../commons/api';
import * as appActions from '../../../../../commons/actions/app';

import { type ApprovalTypeValue } from '../../../../../domain/models/approval/ApprovalType';
import { actions as requestCountsActions } from '../../requestCounts';
import * as listActions from '../../../entities/att/list/actions';
import * as constants from './constants';
import { getNextId } from '../../../../utils/RequestListUtils';

/*
 * Actions
 */
const approveSuccess = () => {
  return {
    type: constants.APPROVE_SUCCESS,
  };
};

const rejectSuccess = () => {
  return {
    type: constants.REJECT_SUCCESS,
  };
};

/**
 * 承認処理
 * @param {array} requestIdList
 * @param {string} comment
 * @param approvalType
 * @param allIds
 * @return {function(Dispatch<*>): Promise<T | never>}
 */
export const approve = (
  requestIdList: string[],
  comment: string = '',
  approvalType: ApprovalTypeValue,
  allIds: string[]
) => (dispatch: Dispatch<any>) => {
  const nextId = getNextId(requestIdList, allIds);

  const req = {
    path: '/approval/request/approve',
    param: {
      requestIdList,
      comment,
    },
  };

  dispatch(appActions.loadingStart());
  return Api.invoke(req)
    .then(() => {
      dispatch(approveSuccess());
      dispatch(listActions.browse(approvalType, nextId));
      dispatch(requestCountsActions.update());
    })
    .catch((err) =>
      dispatch(appActions.catchApiError(err, { isContinuable: true }))
    )
    .then(() => dispatch(appActions.loadingEnd()));
};

/**
 * 申請却下
 * @param {array} requestIdList
 * @param {string} comment
 * @param approvalType
 * @param allIds
 * @return {function(Dispatch<*>): Promise<T | never>}
 */
export const reject = (
  requestIdList: string[],
  comment: string = '',
  approvalType: ApprovalTypeValue,
  allIds: string[]
) => (dispatch: Dispatch<any>) => {
  const nextId = getNextId(requestIdList, allIds);

  const req = {
    path: '/approval/request/reject',
    param: {
      requestIdList,
      comment,
    },
  };

  dispatch(appActions.loadingStart());
  return Api.invoke(req)
    .then(() => {
      dispatch(rejectSuccess());
      dispatch(listActions.browse(approvalType, nextId));
      dispatch(requestCountsActions.update());
    })
    .catch((err) =>
      dispatch(appActions.catchApiError(err, { isContinuable: true }))
    )
    .then(() => dispatch(appActions.loadingEnd()));
};

/**
 * 申請コメント編集
 * @param {string} comment
 */
export const editComment = (comment: string) => {
  return {
    type: constants.EDIT_COMMENT,
    payload: comment,
  };
};
