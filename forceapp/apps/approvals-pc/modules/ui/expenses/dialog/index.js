// @flow

import { combineReducers } from 'redux';

import activeDialog from './activeDialog';

export default combineReducers<Object, Object>({
  activeDialog,
});
