// @flow
import type { Reducer } from 'redux';
import { cloneDeep, includes, pull } from 'lodash';
import { initialSearchCondition } from '../../../../../../domain/models/exp/request/Report';

export const ACTIONS = {
  SET: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/STATUSLIST/SET',
  CLEAR: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/STATUSLIST/CLEAR',
  REPLACE: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/STATUSLIST/REPLACE',
};

type Statuses = Array<string>;

// now only set is used, replace and clear will be needed after 'save/delete condition'
export const actions = {
  set: (status: string) => ({
    type: ACTIONS.SET,
    payload: status,
  }),
  replace: (status: Array<string>) => ({
    type: ACTIONS.REPLACE,
    payload: status,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

// reducer
const initialStatuses = initialSearchCondition.statusList;
export default ((state: Statuses = initialStatuses, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const newStatusList = cloneDeep(state);
      const newItem = action.payload;
      if (!includes(state, newItem)) {
        newStatusList.push(newItem);
      } else {
        pull(newStatusList, newItem);
      }
      return newStatusList;
    case ACTIONS.CLEAR:
      return initialStatuses;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<Statuses, any>);
