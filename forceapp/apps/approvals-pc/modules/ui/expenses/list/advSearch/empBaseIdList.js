// @flow
import type { Reducer } from 'redux';
import { cloneDeep, includes, pull } from 'lodash';

export const ACTIONS = {
  SET: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/EMPBASEIDLIST/SET',
  CLEAR: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/EMPBASEIDLIST/CLEAR',
  REPLACE: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/EMPBASEIDLIST/REPLACE',
};

type EmployeeHistoryIds = Array<string>;

// now only set is used, replace and clear will be needed after 'save/delete condition'
export const actions = {
  set: (employeeBaseId: string) => ({
    type: ACTIONS.SET,
    payload: employeeBaseId,
  }),
  replace: (employeeBaseIdList: Array<string>) => ({
    type: ACTIONS.REPLACE,
    payload: employeeBaseIdList,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

// Reducer
const initialState = [];

export default ((state: EmployeeHistoryIds = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const newEmpList = cloneDeep(state);
      const newItem = action.payload;
      if (!includes(state, newItem)) {
        newEmpList.push(newItem);
      } else {
        pull(newEmpList, newItem);
      }
      return newEmpList;
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<EmployeeHistoryIds, any>);
