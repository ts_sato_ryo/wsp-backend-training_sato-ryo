// @flow
import type { Reducer } from 'redux';
import _ from 'lodash';

import {
  type DateRangeOption,
  initialSearchCondition,
} from '../../../../../../domain/models/exp/request/Report';

export const ACTIONS = {
  SET: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/SUBMIT_DATE_RANGE/SET',
  REPLACE: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/SUBMIT_DATE_RANGE/REPLACE',
  CLEAR: 'MODULES/UI/EXPENSES/LIST/ADVSEARCH/SUBMIT_DATE_RANGE/CLEAR',
};

// now only set is used, replace and clear will be needed after 'save/delete condition'
export const actions = {
  set: (requestDate: DateRangeOption) => ({
    type: ACTIONS.SET,
    payload: requestDate,
  }),
  replace: (requestDate: DateRangeOption) => ({
    type: ACTIONS.REPLACE,
    payload: requestDate,
  }),
  clear: () => ({
    type: ACTIONS.CLEAR,
  }),
};

// Reducer
const initialState = initialSearchCondition.requestDateRange;

export default ((state: DateRangeOption = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      const { startDate, endDate } = action.payload;
      return {
        startDate: startDate || null,
        endDate: endDate || null,
      };
    case ACTIONS.CLEAR:
      return initialState;
    case ACTIONS.REPLACE:
      return action.payload;
    default:
      return state;
  }
}: Reducer<DateRangeOption, any>);
