/* @flow */
import { combineReducers } from 'redux';

import statusList from './statusList';
import empBaseIdList from './empBaseIdList';
import requestDateRange from './requestDateRange';

export default combineReducers<Object, Object>({
  statusList,
  empBaseIdList,
  requestDateRange,
});
