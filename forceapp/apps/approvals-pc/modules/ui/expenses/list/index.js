import { combineReducers } from 'redux';

import selectedIds from './selectedIds';
import page from './page';
import advSearch from './advSearch';

export default combineReducers({
  page,
  selectedIds,
  advSearch,
});
