import { combineReducers } from 'redux';

import detail from './detail';
import list from './list';
import dialog from './dialog';

export default combineReducers({
  detail,
  list,
  dialog,
});
