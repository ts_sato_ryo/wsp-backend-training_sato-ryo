// @flow

import { combineReducers } from 'redux';

import common from '../../commons/reducers';
import userSetting from '../../commons/reducers/userSetting';
import entities from './entities';
import ui from './ui';
import { type $State } from '../../commons/utils/TypeUtil';
import widgets from './widgets';

const reducers = {
  common,
  userSetting,
  entities,
  ui,
  widgets,
};

export type State = $State<typeof reducers>;

export default combineReducers<Object, Object>(reducers);
