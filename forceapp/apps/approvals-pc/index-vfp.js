import '../commons/config/public-path';

/* eslint-disable import/imports-first */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/* eslint-enable */

import configureStore from './store/configureStore';

import AppContainer from './containers/AppContainer';
import * as appActions from './action-dispatchers/App';

import '../commons/styles/base.scss';
import '../commons/config/moment';

function renderApp(store, Component) {
  ReactDOM.render(
    <Provider store={store}>
      <Component />
    </Provider>,
    document.getElementById('container')
  );
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = (param) => {
  const configuredStore = configureStore();
  renderApp(configuredStore, AppContainer);
  configuredStore.dispatch(appActions.initialize(param));
};
