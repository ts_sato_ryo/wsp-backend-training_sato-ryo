import React from 'react';
import PropTypes from 'prop-types';

import CommentNarrowField from '../../../commons/components/fields/CommentNarrowField';
import Button from '../../../commons/components/buttons/Button';
import msg from '../../../commons/languages';

import './ApproveForm.scss';

const ROOT = 'approvals-pc-tracking-detail-parts-approve-form';

export default class ApproveForm extends React.Component {
  static propTypes = {
    onClickApproveButton: PropTypes.func.isRequired,
    onClickRejectButton: PropTypes.func.isRequired,
    comment: PropTypes.string.isRequired,
    onChangeApproveComment: PropTypes.func.isRequired,
    userPhotoUrl: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.onChangeApproveComment = this.onChangeApproveComment.bind(this);
  }

  onChangeApproveComment(e) {
    this.props.onChangeApproveComment(e.target.value, e);
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__field`}>
          <CommentNarrowField
            icon={this.props.userPhotoUrl}
            value={this.props.comment}
            onChange={this.props.onChangeApproveComment}
            maxLength={1000}
          />
        </div>

        <div className={`${ROOT}__buttons`}>
          <Button
            className={`${ROOT}__button`}
            type="destructive"
            onClick={this.props.onClickRejectButton}
          >
            {msg().Appr_Btn_Reject}
          </Button>
          <Button
            className={`${ROOT}__button`}
            type="primary"
            onClick={this.props.onClickApproveButton}
          >
            {msg().Appr_Btn_Approve}
          </Button>
        </div>
      </div>
    );
  }
}
