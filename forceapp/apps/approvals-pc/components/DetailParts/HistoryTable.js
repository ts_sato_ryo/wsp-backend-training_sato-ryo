/* @flow */
import React from 'react';

import type { ApprovalHistoryList } from '../../../domain/models/approval/request/History';

import Collapse from '../Collapse';
import HistoryTableInner from '../../../commons/components/tables/HistoryTable';
import msg from '../../../commons/languages';

import './HistoryTable.scss';

const ROOT = 'approvals-pc-tracking-detail-parts-history-table';

type Props = ApprovalHistoryList & {
  isEllipsis?: boolean,
};

export default class HistoryTable extends React.Component<Props> {
  static defaultProps = {
    isEllipsis: false,
  };

  render() {
    return (
      <div className={`${ROOT}`}>
        <Collapse title={msg().Appr_Lbl_HistoryList}>
          <HistoryTableInner
            historyList={this.props.historyList}
            isEllipsis={this.props.isEllipsis}
          />
        </Collapse>
      </div>
    );
  }
}
