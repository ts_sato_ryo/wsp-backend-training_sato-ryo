import React from 'react';
import PropTypes from 'prop-types';

import './Comment.scss';

const ROOT = 'approvals-pc-tracking-detail-parts-comment';

export default class Comment extends React.Component {
  static propTypes = {
    employeePhotoUrl: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div className={`${ROOT}`}>
        <img
          className={`${ROOT}__icon`}
          src={this.props.employeePhotoUrl}
          alt=""
        />
        <p className={`${ROOT}__body`}>{this.props.value}</p>
      </div>
    );
  }
}
