// @flow
import React from 'react';

import msg from '../../../commons/languages';
import Button from '../../../commons/components/buttons/Button';
import Lightbox from '../../../commons/components/Lightbox';
import FileUtil from '../../../commons/utils/FileUtil';

import pdfIcon from '../../../commons/images/pdfIcon.png';

import './ReceiptPreview.scss';

type Props = {
  fileUrl: ?string,
  fileId: ?string,
  fileType: ?string,
  fileName: string,
};

const ROOT = 'approvals-pc-detail-parts-receipt-preview';

const ReceiptPreview = (props: Props) => {
  const { fileUrl, fileId, fileName, fileType } = props;

  let attachmentContainer = null;

  if (fileUrl) {
    const isPDF = fileType === 'application/pdf';
    attachmentContainer = (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}-label`}>{msg().Exp_Lbl_AttachedFile}</div>
        <div className={`${ROOT}-body`}>
          {isPDF ? (
            <div className={`${ROOT}__pdf-download-btn`}>
              <Button
                onClick={() =>
                  FileUtil.downloadFile(
                    fileUrl,
                    fileId,
                    `${fileName}.pdf`,
                    true
                  )
                }
                className={`${ROOT}__pdf-download-link`}
              >
                <img
                  alt="pdf-icon"
                  src={pdfIcon}
                  className={`${ROOT}__pdf-icon`}
                />
              </Button>
            </div>
          ) : (
            <Lightbox>
              <img
                alt={msg().Exp_Lbl_Receipt}
                className="lightbox-img-thumbnail"
                src={fileUrl}
              />
            </Lightbox>
          )}
        </div>
      </div>
    );
  }
  return attachmentContainer;
};

export default ReceiptPreview;
