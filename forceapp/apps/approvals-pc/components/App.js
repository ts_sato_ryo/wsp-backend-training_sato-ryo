// @flow

import React from 'react';
import _ from 'lodash';
import GlobalContainer from '../../commons/containers/GlobalContainer';
import GlobalHeader from '../../commons/components/GlobalHeader';
import Tab from '../../commons/components/Tab';
import Unavailable from '../../commons/components/Unavailable';
import msg from '../../commons/languages';
import TrackingProcessListPaneContainer from '../containers/TrackingProcessListPane';
import PersonalMenuPopoverContainer from '../../commons/containers/PersonalMenuPopoverContainer';
import AttDailyProcessListPane from '../containers/AttDailyProcessListPane';
import AttMonthlyProcessListPane from '../containers/AttMonthlyProcessListPane';
import DelegateApproverContainer from '../containers/DelegateApprover/DelegateApproverContainer';
import SwitchApproverContainer from '../../../widgets/dialogs/SwitchApporverDialog/containers/SwitchApproverContainer';
import RecordItemsDialogContainer from '../containers/ExpensesCommon/RecordItemsDialogContainer';
import ExpensesRequestListPane from './ExpensesRequestListPane';
import ExpensesPreApprovalListPane from './ExpensesPreApprovalListPane';
import { tabType } from '../modules/ui/tabs';

import './App.scss';

import imgHeaderIcon from '../images/Approval.svg';
import imgIconTimesheet from '../../commons/images/iconTimesheet.png';
import imgIconTracking from '../../commons/images/iconTracking.png';
import imgIconExpenses from '../../expenses-pc/images/menuIconExpenseRequestBigger.png';
import type { UserSetting } from '../../domain/models/UserSetting';

type Props = $ReadOnly<{
  requestCounts: {
    attDaily: number,
    attMonthly: number,
    expenses: number,
    expPreApproval: number,
    timeRequest: number,
  },
  selectTab: ($Values<typeof tabType>) => void,
  selectedTab: $Values<typeof tabType>,
  language: string,
  userSetting: UserSetting,

  // Delegate Approver
  isShowDADialog: boolean,
  listDA: (string) => void,

  // Switch Employee
  isProxyMode: boolean,
  isShowSwitchApproverDialog: boolean,
  listSwitchEmployee: (string) => void,
  onExitProxyMode: () => void,
}>;

export default class ApprovalsContainer extends React.Component<Props> {
  static get defaultProps() {
    return {
      language: '',
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    if (
      this.props.userSetting.employeeId !== nextProps.userSetting.employeeId
    ) {
      this.props.listDA(nextProps.userSetting.employeeId);
      this.props.listSwitchEmployee(nextProps.userSetting.employeeId);
    }
  }

  componentDidUpdate(prevProps: Props) {
    // Select a tab at the time of the first display of the page
    if (
      !prevProps.selectedTab ||
      prevProps.userSetting !== this.props.userSetting // checking shallow equality is valid.
    ) {
      const tabGroups = [
        {
          available: this.props.userSetting.useAttendance,
          tabs: [tabType.ATT_DAILY, tabType.ATT_MONTHLY],
        },
        {
          available: this.props.userSetting.useWorkTime,
          tabs: [tabType.TRACKING],
        },
        {
          available: this.props.userSetting.useExpense,
          tabs: [tabType.EXPENSES],
        },
        {
          available: this.props.userSetting.useExpenseRequest,
          tabs: [tabType.EXP_PRE_APPROVAL],
        },
        {
          // Default
          available: true,
          tabs: [tabType.UNAVAILABLE],
        },
      ];
      const selectedTab = _(tabGroups)
        .filter((tg) => tg.available)
        .flatMap((tg) => tg.tabs)
        .head();
      this.props.selectTab(selectedTab);
    }
  }

  isActiveMenu(i: $Values<typeof tabType>) {
    return this.props.selectedTab === i;
  }

  changeMenu(i: $Values<typeof tabType>) {
    this.props.selectTab(i);
  }

  renderApprovalHeaderItem(
    menuNum: $Values<typeof tabType>,
    available: boolean,
    iconSrc: string,
    iconWidth: number,
    label: string,
    notificationCount: number
  ) {
    if (!available) {
      return null;
    }

    return (
      <Tab
        label={label}
        icon={iconSrc}
        iconWidth={iconWidth.toString()}
        selected={this.isActiveMenu(menuNum)}
        onSelect={() => this.changeMenu(menuNum)}
        notificationCount={notificationCount}
      />
    );
  }

  renderHeaderContent() {
    return (
      <div className="ts-approval__header">
        <div className="ts-approval__header__tab" role="tablist">
          {!this.props.isProxyMode &&
            this.renderApprovalHeaderItem(
              tabType.ATT_DAILY,
              this.props.userSetting.useAttendance,
              imgIconTimesheet,
              20,
              msg().Appr_Lbl_AttendanceRequest,
              this.props.requestCounts.attDaily
            )}
          {!this.props.isProxyMode &&
            this.renderApprovalHeaderItem(
              tabType.ATT_MONTHLY,
              this.props.userSetting.useAttendance,
              imgIconTimesheet,
              20,
              msg().Appr_Lbl_MonthlyAttendanceRequest,
              this.props.requestCounts.attMonthly
            )}

          {!this.props.isProxyMode &&
            this.renderApprovalHeaderItem(
              tabType.TRACKING,
              this.props.userSetting.useWorkTime,
              imgIconTracking,
              16,
              msg().Appr_Btn_TimeTrackRequest,
              this.props.requestCounts.timeRequest
            )}

          {this.renderApprovalHeaderItem(
            tabType.EXPENSES,
            this.props.userSetting.useExpense,
            imgIconExpenses,
            20,
            msg().Appr_Btn_ExpensesRequest,
            this.props.requestCounts.expenses
          )}
          {this.renderApprovalHeaderItem(
            tabType.EXP_PRE_APPROVAL,
            this.props.userSetting.useExpenseRequest,
            imgIconExpenses,
            20,
            msg().Appr_Btn_ExpensesPreApproval,
            this.props.requestCounts.expPreApproval
          )}
        </div>
      </div>
    );
  }

  renderContent() {
    switch (this.props.selectedTab) {
      case tabType.TRACKING:
        return <TrackingProcessListPaneContainer />;
      case tabType.ATT_DAILY:
        return <AttDailyProcessListPane />;
      case tabType.ATT_MONTHLY:
        return <AttMonthlyProcessListPane />;
      case tabType.UNAVAILABLE:
        return <Unavailable />;
      case tabType.EXPENSES:
        return <ExpensesRequestListPane />;
      case tabType.EXP_PRE_APPROVAL:
        return <ExpensesPreApprovalListPane />;
      default:
        return null;
    }
  }

  renderDADialog = () => {
    return this.props.isShowDADialog && <DelegateApproverContainer />;
  };

  renderSwitchApproverDialog = () => {
    return this.props.isShowSwitchApproverDialog && <SwitchApproverContainer />;
  };

  renderExpDialog = () => {
    const isPreApproval = this.props.selectedTab === tabType.EXP_PRE_APPROVAL;
    const isExpApproval =
      isPreApproval || this.props.selectedTab === tabType.EXPENSES;
    if (isExpApproval) {
      return <RecordItemsDialogContainer isPreApproval={isPreApproval} />;
    }
    return null;
  };

  render() {
    const isShowPersonalMenu =
      !this.props.isProxyMode &&
      (this.props.selectedTab === tabType.EXPENSES ||
        this.props.selectedTab === tabType.EXP_PRE_APPROVAL);
    return (
      <GlobalContainer>
        <main className="ts-approval__contents slds">
          <PersonalMenuPopoverContainer showProxyEmployeeSelectButton={false} />
          <GlobalHeader
            iconSrc={imgHeaderIcon}
            iconSrcType="svg"
            iconAssistiveText={msg().Appr_Lbl_Approval}
            content={this.renderHeaderContent()}
            showPersonalMenuPopoverButton={isShowPersonalMenu}
            showProxyIndicator={this.props.isProxyMode}
            onClickProxyExitButton={this.props.onExitProxyMode}
          />
          {this.renderContent()}
          {this.renderDADialog()}
          {this.renderSwitchApproverDialog()}
          {this.renderExpDialog()}
        </main>
      </GlobalContainer>
    );
  }
}
