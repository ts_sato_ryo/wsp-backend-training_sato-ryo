import React from 'react';
import PropTypes from 'prop-types';

import msg from '../../../../commons/languages';

import Table from './Table';

import './index.scss';

const ROOT = 'approvals-pc-tracking-process-list-pane-list';

export default class List extends React.Component {
  static propTypes = {
    requestList: PropTypes.array.isRequired,
    browseDetail: PropTypes.func.isRequired,
    selectedRequestId: PropTypes.string.isRequired,
  };

  render() {
    return (
      <section className={`${ROOT}`}>
        <header className={`${ROOT}__header`}>
          <h1 className={`${ROOT}__header-body`}>
            {msg().Appr_Lbl_ApprovalList}
          </h1>
          <div className={`${ROOT}__toolbar`}>
            <span className={`${ROOT}__toolbar-count`}>
              {this.props.requestList.length} {msg().Appr_Lbl_RecordCount}
            </span>
          </div>
        </header>

        <div className={`${ROOT}__table`}>
          <Table
            requestList={this.props.requestList}
            browseDetail={this.props.browseDetail}
            selectedRequestId={this.props.selectedRequestId}
          />
        </div>
      </section>
    );
  }
}
