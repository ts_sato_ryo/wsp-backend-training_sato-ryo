import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Table from './Table';
import Collapse from '../../Collapse';
import Approve from './Approve';
import HistoryTable from './HistoryTable';
import Button from '../../../../commons/components/buttons/Button';
import msg from '../../../../commons/languages';
import { labelMapping as statusLabelMapping } from '../../../../commons/constants/requestStatus';
import HeaderBar from '../../DetailParts/HeaderBar';
import TrackSummary from '../../../../time-tracking/TrackSummary';

import './index.scss';

import imgIconArrowLongRight from '../../../../commons/images/iconArrowLongRight.png';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail';

export default class Detail extends React.Component {
  static propTypes = {
    employeeName: PropTypes.string.isRequired,
    employeePhotoUrl: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired,
    isExpand: PropTypes.bool.isRequired,
    togglePane: PropTypes.func.isRequired,
    dailyTrackList: PropTypes.object.isRequired,
    taskList: PropTypes.object.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string.isRequired,
    selectedId: PropTypes.string.isRequired,
    historyList: PropTypes.array.isRequired,
    userPhotoUrl: PropTypes.string.isRequired,
    requestId: PropTypes.string.isRequired,
    requestComment: PropTypes.string.isRequired,
    approve: PropTypes.func.isRequired,
    reject: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
  };

  static defaultProps = {};

  render() {
    const btnExpandImageCssClass = classNames(
      `${ROOT}__header-btn-expand-image`,
      {
        [`${ROOT}__header-btn-expand-image--is-expand`]: this.props.isExpand,
      }
    );

    const btnExpandImageAlt = this.props.isExpand
      ? msg().Com_Btn_Contract
      : msg().Com_Btn_Expand;

    if (!this.props.selectedId) {
      return (
        <section className={ROOT}>
          <header className={`${ROOT}__header`}>
            <Button
              type="outline-default"
              className={`${ROOT}__header-btn-expand`}
              onClick={this.props.togglePane}
            >
              <img
                src={imgIconArrowLongRight}
                className={`${btnExpandImageCssClass}`}
                alt={btnExpandImageAlt}
              />
            </Button>
            <h2 className={`${ROOT}__header-body`}>{msg().Appr_Lbl_Detail}</h2>
          </header>
          <div className={`${ROOT}__scrollable`} />
        </section>
      );
    }

    const statusLabel = msg()[statusLabelMapping[this.props.status]];

    return (
      <section className={ROOT}>
        <HeaderBar
          title={msg().Appr_Lbl_Detail}
          meta={[
            {
              label: msg().Appr_Lbl_ApplicantName,
              value: this.props.employeeName,
              show: true,
            },
            /*
             * TODO 代理申請者名の表示に対応する
            {
              label: msg().Appr_Lbl_DelegatedApplicantName,
              value: this.props.delegatedEmployeeName || '',
              show:
                !isNil(this.props.delegatedEmployeeName) &&
                this.props.delegatedEmployeeName !== '',
            },
            */
            {
              label: msg().Appr_Lbl_Status,
              value: statusLabel,
              show: true,
            },
          ]}
          onTogglePane={this.props.togglePane}
          isExpanded={this.props.isExpand}
        />

        <div className={`${ROOT}__scrollable`}>
          <div className={`${ROOT}__info-area`}>
            <div className={`${ROOT}__comment`}>
              <img
                className={`${ROOT}__comment-icon`}
                src={this.props.employeePhotoUrl}
                alt=""
              />
              <p className={`${ROOT}__comment-body`}>{this.props.comment}</p>
            </div>
          </div>

          <div className={`${ROOT}__summary`}>
            <TrackSummary.Approval id={this.props.requestId} />
          </div>

          <div className={`${ROOT}__table`}>
            <Collapse title={msg().Appr_Lbl_DayDetail}>
              <Table
                dailyTrackList={this.props.dailyTrackList}
                taskList={this.props.taskList}
                startDate={this.props.startDate}
                endDate={this.props.endDate}
                isExpand={this.props.isExpand}
              />
            </Collapse>
          </div>

          <div className={`${ROOT}__history-table`}>
            <Collapse title={msg().Appr_Lbl_HistoryList}>
              <HistoryTable
                historyList={this.props.historyList}
                isEllipsis={!this.props.isExpand}
              />
            </Collapse>
          </div>

          <div className={`${ROOT}__approve`}>
            <Approve
              requestId={this.props.requestId}
              comment={this.props.requestComment}
              userPhotoUrl={this.props.userPhotoUrl}
              approve={this.props.approve}
              reject={this.props.reject}
              editComment={this.props.editComment}
            />
          </div>
        </div>
      </section>
    );
  }
}
