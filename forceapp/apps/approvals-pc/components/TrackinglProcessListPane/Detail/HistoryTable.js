// @flow

import React from 'react';

import type { ApprovalHistory } from '../../../../domain/models/approval/request/History';
import HistoryTable from '../../../../commons/components/tables/HistoryTable';

type Props = {
  historyList: ApprovalHistory[],
  isEllipsis: boolean,
};

export default (props: Props) => <HistoryTable {...props} />;
