import React from 'react';
import PropTypes from 'prop-types';

import CommentNarrowField from '../../../../commons/components/fields/CommentNarrowField';
import Button from '../../../../commons/components/buttons/Button';
import msg from '../../../../commons/languages';

import './Approve.scss';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail-approve';

export default class Approve extends React.Component {
  static propTypes = {
    requestId: PropTypes.string.isRequired,
    comment: PropTypes.string.isRequired,
    userPhotoUrl: PropTypes.string.isRequired,
    approve: PropTypes.func.isRequired,
    reject: PropTypes.func.isRequired,
    editComment: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.onClickApproveButton = this.onClickApproveButton.bind(this);
    this.onClickRejectButton = this.onClickRejectButton.bind(this);
  }

  onChangeComment(value) {
    this.props.editComment(value);
  }

  onClickApproveButton() {
    this.props.approve([this.props.requestId], this.props.comment);
  }

  onClickRejectButton() {
    this.props.reject([this.props.requestId], this.props.comment);
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__field`}>
          <CommentNarrowField
            icon={this.props.userPhotoUrl}
            value={this.props.comment}
            onChange={this.props.editComment}
            maxLength={1000}
          />
        </div>

        <div className={`${ROOT}__buttons`}>
          <Button
            className={`${ROOT}__button`}
            type="destructive"
            onClick={this.onClickRejectButton}
          >
            {msg().Appr_Btn_Reject}
          </Button>
          <Button
            className={`${ROOT}__button`}
            type="primary"
            onClick={this.onClickApproveButton}
          >
            {msg().Appr_Btn_Approve}
          </Button>
        </div>
      </div>
    );
  }
}
