import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Scrollable from './Scrollable';
import DateUtil from '../../../../../commons/utils/DateUtil';

import './index.scss';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail-table';

export default class Table extends React.Component {
  static propTypes = {
    dailyTrackList: PropTypes.object.isRequired,
    taskList: PropTypes.object.isRequired,
    startDate: PropTypes.string.isRequired,
    endDate: PropTypes.string.isRequired,
    isExpand: PropTypes.bool.isRequired,
  };

  static defaultProps = {};

  /**
   * 内部に含むテーブルの値を省略するか
   */
  isEllipsis() {
    return !this.props.isExpand;
  }

  createList() {
    if (this.props.startDate === '' || this.props.endDate === '') {
      return null;
    }

    const startDate = moment(this.props.startDate).startOf('day');
    const endDate = moment(this.props.endDate).endOf('day');

    const trackList = [];
    let monthCheck = null;
    while (startDate.isBefore(endDate)) {
      const nowMonth = startDate.month();
      let dateStr;
      if (nowMonth !== monthCheck) {
        monthCheck = nowMonth;
        dateStr = DateUtil.formatMDW(startDate.valueOf());
      } else {
        dateStr = DateUtil.format(startDate.valueOf(), 'D ddd');
      }

      const keyDateStr = DateUtil.formatISO8601Date(startDate.valueOf());
      const dailyTrack = this.props.dailyTrackList[keyDateStr]
        ? this.props.dailyTrackList[keyDateStr]
        : { recordItemList: [] };

      if (dailyTrack && dailyTrack.recordItemList.length > 0) {
        trackList.push({
          dateStr,
          dailyTrack,
          taskList: this.props.taskList,
        });
      } else {
        trackList.push({
          dateStr,
        });
      }

      startDate.add(1, 'days');
    }

    return trackList;
  }

  render() {
    const trackList = this.createList();

    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__scrollable`}>
          <Scrollable
            isEllipsis={this.isEllipsis()}
            trackList={trackList}
            taskList={this.props.taskList}
          />
        </div>
      </div>
    );
  }
}
