import React from 'react';
import PropTypes from 'prop-types';

import Header from './Header';
import Row from '../Row';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail-table-scrollable';

export default class Scrollabele extends React.Component {
  static propTypes = {
    trackList: PropTypes.array.isRequired,
    isEllipsis: PropTypes.bool.isRequired,
    taskList: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.renderRow = this.renderRow.bind(this);
  }

  renderRow(track) {
    return (
      <Row
        isEllipsis={this.props.isEllipsis}
        key={track.dateStr}
        dateStr={track.dateStr}
        dailyTrack={track.dailyTrack}
        taskList={this.props.taskList}
      />
    );
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        <Header />
        {this.props.trackList.map(this.renderRow)}
      </div>
    );
  }
}
