import React from 'react';

import msg from '../../../../../../commons/languages';

import './Header.scss';
import '../column.scss';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail-table-header';
const COLUMN_ROOT =
  'approvals-pc-tracking-process-list-pane-detail-table-column';

export default class Header extends React.Component {
  render() {
    return (
      <div className={`${ROOT}`} role="row">
        <div
          className={`${ROOT}__cell ${COLUMN_ROOT}--date`}
          role="columnheader"
        >
          {msg().Appr_Lbl_Date}
        </div>
        <div
          className={`${ROOT}__cell ${COLUMN_ROOT}--name`}
          role="columnheader"
        >
          {msg().Appr_Lbl_WorkCategory} / {msg().Appr_Lbl_Job}
        </div>
        <div
          className={`${ROOT}__cell ${COLUMN_ROOT}--work-time`}
          role="columnheader"
        >
          {msg().Appr_Lbl_Work}
        </div>
      </div>
    );
  }
}
