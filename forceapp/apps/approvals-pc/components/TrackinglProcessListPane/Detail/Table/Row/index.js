import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Summary from './Summary';
import Task from './Task';

import '../column.scss';
import './index.scss';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail-table-row';
const COLUMN_ROOT =
  'approvals-pc-tracking-process-list-pane-detail-table-column';

export default class Item extends React.Component {
  static propTypes = {
    dailyTrack: PropTypes.object.isRequired,
    taskList: PropTypes.object.isRequired,
    dateStr: PropTypes.string.isRequired,
    isEllipsis: PropTypes.bool.isRequired,
    inFixed: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.renderTask = this.renderTask.bind(this);
  }

  renderTask(task, i) {
    return (
      <Task
        isEllipsis={this.props.isEllipsis}
        key={i}
        index={i}
        task={task}
        taskList={this.props.taskList}
        inFixed={this.props.inFixed}
      />
    );
  }

  render() {
    if (_.isEmpty(this.props.dailyTrack)) {
      return (
        <section className={`${ROOT}`}>
          <header className={`${ROOT}__item ${COLUMN_ROOT}--date`}>
            <div className={`${ROOT}__item-date-inner`}>
              {this.props.dateStr}
            </div>
          </header>
          <div className={`${ROOT}__content ${ROOT}__empty-content`}>
            <div className={`${ROOT}__item ${COLUMN_ROOT}--name`} />
            <div className={`${ROOT}__item ${COLUMN_ROOT}--work-time`} />
          </div>
        </section>
      );
    } else {
      return (
        <section className={`${ROOT}`}>
          <header className={`${ROOT}__item ${COLUMN_ROOT}--date`}>
            <div className={`${ROOT}__item-date-inner`}>
              {this.props.dateStr}
            </div>
          </header>
          <div className={`${ROOT}__content`}>
            <div className={`${ROOT}__task-wrapper`}>
              {this.props.dailyTrack.recordItemList.map(this.renderTask)}
            </div>
            <Summary
              isEllipsis={this.props.isEllipsis}
              sumTaskTime={this.props.dailyTrack.sumTaskTime}
              note={this.props.dailyTrack.note}
              inFixed={this.props.inFixed}
            />
          </div>
        </section>
      );
    }
  }
}
