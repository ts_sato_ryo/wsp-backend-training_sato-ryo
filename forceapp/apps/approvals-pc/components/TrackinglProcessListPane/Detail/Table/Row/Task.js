import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import TimeUtil from '../../../../../../commons/utils/TimeUtil';
import TaskUtil from '../../../../../../time-tracking/tracking-pc/utils/TaskUtil';

import '../column.scss';
import './Task.scss';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail-table-row-task';
const COLUMN_ROOT =
  'approvals-pc-tracking-process-list-pane-detail-table-column';

export default class Task extends React.Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    task: PropTypes.object.isRequired,
    inFixed: PropTypes.bool.isRequired,
    isEllipsis: PropTypes.bool.isRequired,
  };

  renderComment() {
    if (this.props.inFixed) {
      return null;
    } else {
      return (
        <div className={`${ROOT}__item ${COLUMN_ROOT}--comment`}>
          <div className={`${ROOT}__position-adjust`} />
        </div>
      );
    }
  }

  render() {
    const cssClass = classNames(ROOT, {
      [`${ROOT}--is-ellipsis`]: this.props.isEllipsis,
    });

    return (
      <div className={cssClass}>
        <div
          className={`${ROOT}__item ${ROOT}__item--name ${COLUMN_ROOT}--name`}
        >
          <div className={`${ROOT}__task-name`}>
            <div className={`${ROOT}__task-index`}>{this.props.index + 1}</div>

            {TaskUtil.createTaskName(this.props.task)}
          </div>
        </div>

        <div
          className={`${ROOT}__item ${ROOT}__item--work-time ${COLUMN_ROOT}--work-time`}
        >
          <div className={`${ROOT}__work-time`}>
            {TimeUtil.toHHmm(this.props.task.taskTime)}
          </div>
        </div>
      </div>
    );
  }
}
