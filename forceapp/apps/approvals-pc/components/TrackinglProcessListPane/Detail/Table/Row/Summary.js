import React from 'react';
import PropTypes from 'prop-types';

import msg from '../../../../../../commons/languages';
import TimeUtil from '../../../../../../commons/utils/TimeUtil';

import '../column.scss';
import './Summary.scss';

const ROOT = 'approvals-pc-tracking-process-list-pane-detail-table-row-summary';
const COLUMN_ROOT =
  'approvals-pc-tracking-process-list-pane-detail-table-column';

export default class Summary extends React.Component {
  static propTypes = {
    sumTaskTime: PropTypes.number.isRequired,
  };

  render() {
    return (
      <div className={`${ROOT}`}>
        <div
          className={`${ROOT}__item ${ROOT}__item--label-summary ${COLUMN_ROOT}--label-summary`}
        >
          {msg().Trac_Lbl_Total}
        </div>
        <div
          className={`${ROOT}__item ${ROOT}__item--work-time ${COLUMN_ROOT}--work-time`}
        >
          {TimeUtil.toHHmm(this.props.sumTaskTime)}
        </div>
      </div>
    );
  }
}
