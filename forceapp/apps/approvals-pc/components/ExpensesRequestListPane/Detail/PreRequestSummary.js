// @flow
import React from 'react';

import FormatUtil from '../../../../commons/utils/FormatUtil';
import DateUtil from '../../../../commons/utils/DateUtil';

import { type ExpRequest } from '../../../../domain/models/exp/request/Report';

import HorizontalLayout from '../../../../commons/components/fields/layouts/HorizontalLayout';
import Collapse from '../../Collapse';

import msg from '../../../../commons/languages';

import './PreRequestSummary.scss';

const ROOT = 'approvals-pc-expenses-request-pane-detail_pre_request_summary';

type Props = {
  expRequest: ExpRequest,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
};

export default class PreRequestSummary extends React.Component<Props> {
  render() {
    const { expRequest } = this.props;
    const renderItems = [
      {
        label: {
          content: msg().Exp_Lbl_RequestNo,
          col: 3,
        },
        value: {
          content: expRequest.requestNo,
          col: 9,
        },
      },
      {
        label: {
          content: msg().Exp_Clbl_RequestTitle,
          col: 3,
        },
        value: {
          content: expRequest.subject,
          col: 9,
        },
      },
      {
        label: {
          content: msg().Exp_Lbl_TotalAmount,
          col: 3,
        },
        value: {
          content: `${this.props.baseCurrencySymbol} ${FormatUtil.formatNumber(
            expRequest.totalAmount,
            this.props.baseCurrencyDecimal
          )}`,
          col: 3,
        },
      },
      {
        label: {
          content: msg().Exp_Clbl_ScheduledDate,
          col: 3,
        },
        value: {
          content: DateUtil.formatYMD(expRequest.scheduledDate),
          col: 2,
        },
      },
      {
        label: {
          content: msg().Exp_Clbl_Purpose,
          col: 3,
        },
        value: {
          content: expRequest.purpose,
          col: 9,
        },
      },
    ];

    return (
      <ul className={`${ROOT}__list-detail`}>
        <Collapse title={msg().Appr_Btn_ExpensesPreApproval}>
          {renderItems.map((item, i) => (
            <li key={i} className={`${ROOT}__list-detail-item`}>
              <HorizontalLayout>
                <HorizontalLayout.Label cols={item.label.col}>
                  {item.label.content}
                </HorizontalLayout.Label>

                <HorizontalLayout.Body cols={item.value.col}>
                  {item.value.content}
                </HorizontalLayout.Body>
              </HorizontalLayout>
            </li>
          ))}
        </Collapse>
      </ul>
    );
  }
}
