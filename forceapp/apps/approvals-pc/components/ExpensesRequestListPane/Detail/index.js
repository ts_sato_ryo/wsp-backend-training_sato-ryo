// @flow
import React from 'react';

import msg from '../../../../commons/languages';
import VendorDetailWrapper from '../../../../commons/components/exp/VendorDetail/DetailWrapper';

import type { ExpRequest } from '../../../../domain/models/exp/request/Report';
import type { VendorItemList } from '../../../../domain/models/exp/Vendor';
import type { ExpenseTaxTypeList } from '../../../../domain/models/exp/TaxType';
import { status } from '../../../../domain/modules/exp/report';

import HeaderBar from '../../DetailParts/HeaderBar';
import Comment from '../../DetailParts/Comment';
import ApproveForm from '../../DetailParts/ApproveForm';
import HistoryTable from '../../DetailParts/HistoryTable';
import ReceiptPreview from '../../DetailParts/ReceiptPreview';
import Summary from './Summary';
import PreRequestSummary from './PreRequestSummary';
import RecordsArea from './RecordsArea';

import './index.scss';

type Props = {
  // ui states
  baseCurrencyCode: string,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  comment: string,
  expRequest: ExpRequest,
  expTaxTypeList: ExpenseTaxTypeList,
  userPhotoUrl: string,
  // event handlers
  onChangeComment: (string) => void,
  onClickApproveButton: () => void,
  onClickRejectButton: () => void,
  onClickAllOpenRecordButton: () => void,
  onClickAllCloseRecordButton: () => void,
  onClickRecordOpenButton: (string) => void,
  getFilePreview: (receiptFileId: string) => Promise<any>,
  openNowList: { [string]: boolean },
  openNowListInitialize: ({ [string]: boolean }) => void,
  openRecordItemsConfirmDialog: () => void,
  setSelectedRecord: (number) => void,
  searchVendorDetail: (vendorId: ?string) => Promise<VendorItemList>,
};

type State = {
  isVendorDetailOpen: boolean,
};

const ROOT = 'approvals-pc-expenses-request-pane-detail';

export default class Detail extends React.Component<Props, State> {
  scrollable: ?HTMLDivElement;

  state = {
    isVendorDetailOpen: false,
  };

  componentWillUpdate(nextProps: Props) {
    if (nextProps.expRequest.requestId !== this.props.expRequest.requestId) {
      const openNowList = nextProps.expRequest.records.reduce((ret, record) => {
        ret[record.recordId] = false;
        return ret;
      }, {});
      this.props.openNowListInitialize(openNowList);
      this.toggleVendorDetail(false);
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (
      this.scrollable &&
      prevProps.expRequest.requestId !== this.props.expRequest.requestId
    ) {
      this.scrollable.scrollTop = 0;
    }
  }

  toggleVendorDetail = (toOpen: boolean) => {
    this.setState({ isVendorDetailOpen: toOpen });
  };

  renderStatus() {
    let message;
    switch (this.props.expRequest.status) {
      case status.PENDING:
        message = msg().Com_Lbl_Pending;
        break;
      case status.REJECTED:
        message = msg().Com_Lbl_Rejected;
        break;
      case status.APPROVED:
        message = msg().Com_Lbl_Approved;
        break;
      default:
        message = '';
    }
    return message;
  }

  renderPreRequestSummary() {
    if (this.props.expRequest.expPreRequest) {
      return (
        <PreRequestSummary
          expRequest={this.props.expRequest.expPreRequest}
          baseCurrencySymbol={this.props.baseCurrencySymbol}
          baseCurrencyDecimal={this.props.baseCurrencyDecimal}
        />
      );
    }
    return '';
  }

  render() {
    const { expRequest } = this.props;
    // If no approval item were selected
    if (expRequest.requestId === '') {
      return (
        <div className={ROOT}>
          <div className={`${ROOT}__header`}>
            <HeaderBar title={msg().Appr_Lbl_Detail} />
          </div>
        </div>
      );
    }

    return (
      <div className={`${ROOT}`}>
        <HeaderBar
          title={msg().Appr_Lbl_Request}
          meta={[
            {
              label: msg().Appr_Lbl_ApplicantName,
              value: this.props.expRequest.employeeName,
            },
            {
              label: msg().Appr_Lbl_Status,
              value: this.renderStatus(),
            },
          ]}
        />

        <div
          className={`${ROOT}__scrollable`}
          ref={(scrollable) => {
            this.scrollable = scrollable;
          }}
        >
          <Comment
            value={expRequest.comment}
            employeePhotoUrl={expRequest.employeePhotoUrl}
          />

          <Summary
            expRequest={this.props.expRequest}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            openVendorDetail={() => this.toggleVendorDetail(true)}
          />

          <ReceiptPreview
            fileUrl={this.props.expRequest.attachedFileData}
            fileId={this.props.expRequest.attachedFileId}
            fileName={this.props.expRequest.fileName}
            fileType={this.props.expRequest.dataType}
          />

          {this.renderPreRequestSummary()}

          <RecordsArea
            expRequest={expRequest}
            baseCurrencyCode={this.props.baseCurrencyCode}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            openNowList={this.props.openNowList}
            onClickRecordOpenButton={this.props.onClickRecordOpenButton}
            onClickAllOpenRecordButton={this.props.onClickAllOpenRecordButton}
            onClickAllCloseRecordButton={this.props.onClickAllCloseRecordButton}
            expTaxTypeList={this.props.expTaxTypeList}
            getFilePreview={this.props.getFilePreview}
            setSelectedRecord={this.props.setSelectedRecord}
            openRecordItemsConfirmDialog={
              this.props.openRecordItemsConfirmDialog
            }
          />

          <HistoryTable historyList={this.props.expRequest.historyList} />

          {this.props.expRequest.status === status.PENDING && (
            <ApproveForm
              comment={this.props.comment}
              onChangeApproveComment={this.props.onChangeComment}
              onClickRejectButton={this.props.onClickRejectButton}
              onClickApproveButton={this.props.onClickApproveButton}
              userPhotoUrl={this.props.userPhotoUrl}
            />
          )}
        </div>

        {this.state.isVendorDetailOpen && (
          <VendorDetailWrapper
            vendorId={expRequest.vendorId}
            searchVendorDetail={this.props.searchVendorDetail}
            closePanel={() => this.toggleVendorDetail(false)}
            isApproval
          />
        )}
      </div>
    );
  }
}
