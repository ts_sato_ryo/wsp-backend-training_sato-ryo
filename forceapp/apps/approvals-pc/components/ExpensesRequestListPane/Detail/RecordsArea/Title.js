// @flow
import React from 'react';
import msg from '../../../../../commons/languages';

import './Title.scss';

const ROOT = 'approvals-pc-expenses-request-pane-detail__records-area__title';

type Props = {};

export default class Title extends React.Component<Props> {
  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}-date`}>{msg().Exp_Clbl_Date}</div>
        <div className={`${ROOT}-exp-type`}>{msg().Exp_Clbl_ExpenseType}</div>
        {/* <div className={`${ROOT}-record-type`}>
          {msg().Exp_Lbl_RecordType}
        </div> */}
        <div className={`${ROOT}-amount`}>{msg().Exp_Lbl_Amount}</div>
      </div>
    );
  }
}
