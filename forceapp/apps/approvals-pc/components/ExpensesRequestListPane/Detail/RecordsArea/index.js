// @flow
import React from 'react';
import AnimateHeight from 'react-animate-height';

import msg from '../../../../../commons/languages';

import Button from '../../../../../commons/components/buttons/Button';

import type { ExpRequest } from '../../../../../domain/models/exp/request/Report';
import type { ExpenseTaxTypeList } from '../../../../../domain/models/exp/TaxType';

import Title from './Title';
import RecordHeader from './RecordHeader';
import RecordBody from './RecordBody';

import './index.scss';

type Props = {
  expRequest: ExpRequest,
  onClickAllOpenRecordButton: () => void,
  onClickAllCloseRecordButton: () => void,
  onClickRecordOpenButton: (string) => void,
  getFilePreview: (receiptFileId: string) => Promise<any>,
  openNowList: { [string]: boolean },
  expTaxTypeList: ExpenseTaxTypeList,
  baseCurrencyCode: string,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  openRecordItemsConfirmDialog: () => void,
  setSelectedRecord: (number) => void,
};

const ROOT = 'approvals-pc-expenses-request-pane-detail__records-area';

export default class RecordsArea extends React.Component<Props> {
  render() {
    const { expRequest, openNowList } = this.props;

    if (expRequest.records.length === 0) {
      return null;
    }

    return (
      <div className={ROOT}>
        <hr className={`${ROOT}__hr ${ROOT}__hr--top`} />
        <div className={`${ROOT}-button-area`}>
          <Button onClick={this.props.onClickAllOpenRecordButton}>
            {msg().Appr_Btn_Open}
          </Button>
          <Button onClick={this.props.onClickAllCloseRecordButton}>
            {msg().Appr_Btn_Close}
          </Button>
        </div>
        <Title />
        {expRequest.records.map((record, index) => {
          const isOpen = openNowList[record.recordId];
          const height = isOpen ? 'auto' : 0;
          return (
            <section key={record.recordId} className={`${ROOT}-record`}>
              <RecordHeader
                record={record}
                isOpen={isOpen}
                baseCurrencyCode={this.props.baseCurrencyCode}
                baseCurrencySymbol={this.props.baseCurrencySymbol}
                baseCurrencyDecimal={this.props.baseCurrencyDecimal}
                onClickRecordOpenButton={this.props.onClickRecordOpenButton}
              />
              <AnimateHeight height={height}>
                <div className={`${ROOT}-record-contents-hr`}>
                  <hr />
                </div>
                <RecordBody
                  baseCurrencyCode={this.props.baseCurrencyCode}
                  baseCurrencySymbol={this.props.baseCurrencySymbol}
                  baseCurrencyDecimal={this.props.baseCurrencyDecimal}
                  report={expRequest}
                  record={record}
                  expTaxTypeList={this.props.expTaxTypeList}
                  getFilePreview={this.props.getFilePreview}
                  recordIdx={index}
                  setSelectedRecord={this.props.setSelectedRecord}
                  openRecordItemsConfirmDialog={
                    this.props.openRecordItemsConfirmDialog
                  }
                />
              </AnimateHeight>
            </section>
          );
        })}
        <hr className={`${ROOT}__hr ${ROOT}__hr--bottom`} />
      </div>
    );
  }
}
