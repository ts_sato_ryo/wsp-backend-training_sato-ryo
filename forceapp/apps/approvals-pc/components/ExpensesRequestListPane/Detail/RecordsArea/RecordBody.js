// @flow
import { isNil } from 'lodash';
import React from 'react';

import msg from '../../../../../commons/languages';
import FormatUtil from '../../../../../commons/utils/FormatUtil';
import FileUtil from '../../../../../commons/utils/FileUtil';

import Lightbox from '../../../../../commons/components/Lightbox';
import Button from '../../../../../commons/components/buttons/Button';
import TextField from '../../../../../commons/components/fields/TextField';
import MultiColumnsGrid from '../../../../../commons/components/MultiColumnsGrid';
import RouteMap from '../../../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteMap';

import ImgEditOn from '../../../../../commons/images/btnEditOn.png';
import pdfIcon from '../../../../../commons/images/pdfIcon.png';

import './RecordBody.scss';

import type {
  ExpRequestRecord,
  ExpRequest,
} from '../../../../../domain/models/exp/request/Report';
import { isRecordItemized } from '../../../../../domain/models/exp/Record';

import { getLabelValueFromEIs } from '../../../../../domain/models/exp/ExtendedItem';

type Props = {
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  record: ExpRequestRecord,
  report: ExpRequest,
  getFilePreview: (receiptFileId: string) => Promise<any>,
  openRecordItemsConfirmDialog: () => void,
  setSelectedRecord: (number) => void,
  recordIdx: number,
};

const ROOT =
  'approvals-pc-expenses-request-pane-detail__records-area-record__body';

type State = {
  fileUrl: string | null,
  fileName: string,
  dataType: string | null,
};
export default class RecordContentBody extends React.Component<Props, State> {
  state = {
    fileUrl: null,
    fileName: 'receipt',
    dataType: null,
  };

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.record.receiptFileId && !nextProps.record.receiptData) {
      nextProps.getFilePreview(nextProps.record.receiptFileId).then((res) => {
        const fileBody = res.payload.fileBody;
        const dataType = FileUtil.getMIMEType(res.payload.fileType);
        const fileUrl = `data:${dataType};base64,${fileBody}`;
        const fileName = FileUtil.getOriginalFileNameWithoutPrefix(
          res.payload.title
        );
        this.setState({ fileUrl, fileName, dataType });
      });
    }
  }

  renderBaseCurrency() {
    const editImage = this.props.record.items[0].taxManual ? (
      <img src={ImgEditOn} alt="ImgEditOn" />
    ) : null;
    const amountWithoutGST = FormatUtil.formatNumber(
      this.props.record.items[0].withoutTax,
      this.props.baseCurrencyDecimal
    );
    const gstVat = FormatUtil.formatNumber(
      this.props.record.items[0].gstVat,
      this.props.baseCurrencyDecimal
    );
    let taxRate = this.props.record.items[0].taxRate;
    taxRate = isNil(taxRate) ? '-' : taxRate;

    return (
      <div className={`${ROOT}-amount-area`}>
        <MultiColumnsGrid sizeList={[2, 3, 1, 2, 3, 1]}>
          <div className={`${ROOT}-amount-area-label`}>
            {msg().Exp_Clbl_Gst}
          </div>
          <TextField value={this.props.record.items[0].taxTypeName} readOnly />
          <div />
          <div className={`${ROOT}-amount-area-label`}>
            {msg().Exp_Clbl_GstAmount}
          </div>
          <TextField
            value={`${this.props.baseCurrencySymbol} ${gstVat}`}
            readOnly
          />
          {editImage}
        </MultiColumnsGrid>

        <MultiColumnsGrid sizeList={[2, 3, 1, 2, 3, 1]}>
          <div className={`${ROOT}-amount-area-label`}>
            {msg().Exp_Clbl_GstRate}
          </div>
          <TextField value={`${taxRate} %`} readOnly />
          <div />
          <div className={`${ROOT}-amount-area-label`}>
            {msg().Exp_Clbl_WithoutTax}
          </div>
          <TextField
            value={`${this.props.baseCurrencySymbol} ${amountWithoutGST}`}
            readOnly
          />
        </MultiColumnsGrid>
      </div>
    );
  }

  renderForeignCurrency() {
    const { record } = this.props;
    const localAmount = FormatUtil.formatNumber(
      record.items[0].localAmount,
      record.items[0].currencyInfo.decimalPlaces
    );
    const isEditable = record.items[0].exchangeRateManual;
    const editImage =
      isEditable && !isRecordItemized(record.recordType) ? (
        <img src={ImgEditOn} alt="ImgEditOn" />
      ) : null;

    const exchangeRate = isRecordItemized(record.recordType)
      ? '—'
      : record.items[0].exchangeRate;

    return (
      <div className={`${ROOT}-amount-area`}>
        <MultiColumnsGrid sizeList={[2, 3, 1, 2, 3, 1]}>
          <div className={`${ROOT}-amount-area-label`}>
            {msg().Exp_Clbl_Currency}
          </div>
          <TextField value={record.items[0].currencyInfo.code} readOnly />
          <div />

          <div className={`${ROOT}-amount-area-label`}>
            {msg().Exp_Clbl_ExchangeRate}
          </div>
          <TextField value={exchangeRate} readOnly />
          {editImage}
        </MultiColumnsGrid>

        <MultiColumnsGrid sizeList={[6, 2, 3, 1]}>
          <div />
          <div className={`${ROOT}-amount-area-label`}>
            {msg().Exp_Clbl_LocalAmount}
          </div>
          <TextField
            value={`${record.items[0].currencyInfo.symbol ||
              ''} ${localAmount}`}
            readOnly
          />
          <div />
        </MultiColumnsGrid>
      </div>
    );
  }

  renderJorudan() {
    return <RouteMap routeInfo={this.props.record.routeInfo} />;
  }

  renderAttachment() {
    let attachmentContainer = null;
    if (this.state.fileUrl) {
      const url = this.state.fileUrl;
      const isPDF = this.state.dataType === 'application/pdf';
      const fileName = this.state.fileName;

      attachmentContainer = (
        <div className={`${ROOT}-item`}>
          <div className={`${ROOT}-item-title`}>{msg().Exp_Lbl_Receipt}</div>
          <div className={`${ROOT}-item-separater`}>:</div>
          <div className={`${ROOT}-item-body`}>
            {isPDF ? (
              <div className={`${ROOT}__pdf-download-btn`}>
                <Button
                  onClick={() =>
                    FileUtil.downloadFile(
                      url,
                      this.props.record.receiptId,
                      `${fileName}.pdf`,
                      true
                    )
                  }
                  className={`${ROOT}__pdf-download-link`}
                >
                  <img
                    alt="pdf-icon"
                    src={pdfIcon}
                    className={`${ROOT}__pdf-icon`}
                  />
                </Button>
              </div>
            ) : (
              <Lightbox>
                <img
                  alt={msg().Exp_Lbl_Receipt}
                  className="lightbox-img-thumbnail"
                  src={url}
                />
              </Lightbox>
            )}
          </div>
        </div>
      );
    }
    return attachmentContainer;
  }

  renderRecordItemsBtn() {
    return (
      <div className={`${ROOT}__record-items-check-btn-container`}>
        <Button
          onClick={() => {
            this.props.setSelectedRecord(this.props.recordIdx);
            this.props.openRecordItemsConfirmDialog();
          }}
          className={`${ROOT}__record-items-check-btn`}
        >
          {msg().Exp_Btn_RecordItemsCheck}
        </Button>
      </div>
    );
  }

  renderJob() {
    const { report, record } = this.props;
    let jobCode = report.jobCode;
    let jobName = report.jobName;
    const recordItem = record.items[0];
    if (recordItem.jobId) {
      jobCode = recordItem.jobCode;
      jobName = recordItem.jobName;
    }

    return (
      jobCode &&
      jobName && (
        <div className={`${ROOT}-item`}>
          <div className={`${ROOT}-item-title`}>{msg().Exp_Lbl_Job}</div>
          <div className={`${ROOT}-item-separater`}>:</div>
          <div className={`${ROOT}-item-body`}>{`${jobCode} - ${jobName}`}</div>
        </div>
      )
    );
  }

  renderCostCenter() {
    const { report, record } = this.props;
    let costCenterCode = report.costCenterCode;
    let costCenterName = report.costCenterName;
    const recordItem = record.items[0];
    if (recordItem.costCenterHistoryId) {
      costCenterCode = recordItem.costCenterCode;
      costCenterName = recordItem.costCenterName;
    }

    return (
      costCenterCode &&
      costCenterName && (
        <div className={`${ROOT}-item`}>
          <div className={`${ROOT}-item-title`}>{msg().Exp_Lbl_CostCenter}</div>
          <div className={`${ROOT}-item-separater`}>:</div>
          <div
            className={`${ROOT}-item-body`}
          >{`${costCenterCode} - ${costCenterName}`}</div>
        </div>
      )
    );
  }

  render() {
    let renderArea;
    switch (this.props.record.recordType) {
      case 'General':
      case 'FixedAllowanceSingle':
      case 'FixedAllowanceMulti':
        renderArea = this.props.record.items[0].useForeignCurrency
          ? this.renderForeignCurrency()
          : this.renderBaseCurrency();
        break;
      case 'TransitJorudanJP':
        renderArea = this.renderJorudan();
        break;
      case 'HotelFee':
        const currencyArea = this.props.record.items[0].useForeignCurrency
          ? this.renderForeignCurrency()
          : this.renderBaseCurrency();
        renderArea = (
          <React.Fragment>
            {currencyArea} {this.renderRecordItemsBtn()}
          </React.Fragment>
        );
        break;

      default:
        break;
    }

    const extendedItems = isRecordItemized(this.props.record.recordType)
      ? []
      : getLabelValueFromEIs(this.props.record.items[0]);

    return (
      <div className={ROOT}>
        <div className={`${ROOT}-hr`} />
        {this.renderAttachment()}
        {renderArea}
        {this.renderJob()}
        {this.renderCostCenter()}

        {extendedItems.map((pair) => (
          <div className={`${ROOT}-item`} key={pair.label}>
            <div className={`${ROOT}-item-title`}>{pair.label}</div>
            <div className={`${ROOT}-item-separater`}>:</div>
            <div className={`${ROOT}-item-body`}>{pair.value}</div>
          </div>
        ))}

        <div className={`${ROOT}-item`}>
          <div className={`${ROOT}-item-title`}>{msg().Exp_Clbl_Summary}</div>
          <div className={`${ROOT}-item-separater`}>:</div>
          <div className={`${ROOT}-item-body`}>
            {this.props.record.items[0].remarks}
          </div>
        </div>
      </div>
    );
  }
}
