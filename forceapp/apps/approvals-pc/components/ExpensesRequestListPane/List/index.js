// @flow
import React from 'react';

import msg from '../../../../commons/languages';

import Grid from '../../../../commons/components/Grid';

import Employee from '../../../../commons/components/Grid/Formatters/Employee';
import DateYMD from '../../../../commons/components/Grid/Formatters/DateYMD';
import Request from '../../../../commons/components/Grid/Formatters/Request';
import Currency from '../../../../commons/components/Grid/Formatters/Currency';
import Icon from '../../../../mobile-app/components/atoms/Icon';

import SearchArea from '../../ExpensesPreApprovalListPane/List/SearchArea';
import PagerInfo, {
  type Props as PagerInfoProps,
} from '../../../../commons/components/PagerInfo';
import Pagination, {
  type Props as PagerProps,
} from '../../../../commons/components/Pagination';
import {
  PAGE_SIZE,
  MAX_SEARCH_RESULT_NUM,
  MAX_PAGE_NUM,
} from '../../../modules/ui/expenses/list/page';
import { type UserSetting } from '../../../../domain/models/UserSetting';
import {
  type ExpRequestIdsInfo,
  type ExpRequestList,
  type DateRangeOption,
  type SearchConditions,
  type EmployeeOption,
  initialSearchCondition,
} from '../../../../domain/models/exp/request/Report';
import { type ProxyEmployeeInfo } from '../../../models/ProxyEmployee';
import { getStatusText } from '../../../../domain/modules/exp/report';

import './index.scss';

const ROOT = 'approvals-pc-expenses-request-list-pane-list';

type Props = {
  requestList: ExpRequestList,
  selectedIds: Array<string>,
  browseId: string,
  userSetting: UserSetting,
  expIdsInfo: ExpRequestIdsInfo,
  employeeOptions: Array<EmployeeOption>,
  advSearchCondition: SearchConditions,
  proxyEmployeeInfo: ProxyEmployeeInfo,
  browseDetail: () => void,
  fetchAllIdsForExpApproval: (
    searchCondition: SearchConditions,
    empId?: string,
    isEmpty?: boolean
  ) => void,
  onClickRefreshButton: (searchCondition: SearchConditions) => void,
  onClickInputValueStatus: () => void,
  onClickInputValueEmployee: () => void,
  fetchInitialEmployeeList: () => void,
  onClickInputValueSubmitDate: (
    dateRangeOption: DateRangeOption,
    needUpdate: boolean
  ) => void,
  onClickAdvSearchButton: () => void,
  clearSearchCondition: () => void,
} & PagerInfoProps &
  PagerProps;

export default class List extends React.Component<Props> {
  componentDidMount() {
    const {
      proxyEmployeeInfo: { id, isProxyMode, expReportRequestCount },
    } = this.props;
    const isEmpty = isProxyMode && expReportRequestCount === 0;
    const empId = isProxyMode ? id : undefined;
    this.props.fetchAllIdsForExpApproval(
      initialSearchCondition,
      empId,
      isEmpty
    );
  }

  componentDidUpdate(prevProps: Props) {
    const {
      proxyEmployeeInfo: { id, isProxyMode, expReportRequestCount },
    } = this.props;
    const isEmpty = isProxyMode && expReportRequestCount === 0;
    const empId = isProxyMode ? id : undefined;

    if (isProxyMode !== prevProps.proxyEmployeeInfo.isProxyMode) {
      this.props.fetchAllIdsForExpApproval(
        initialSearchCondition,
        empId,
        isEmpty
      );
    }
  }

  statusFormatter(props: { value: string }) {
    return getStatusText(props.value);
  }

  render() {
    const baseCurrencySymbol = this.props.userSetting.currencySymbol;
    const baseCurrencyDecimal = this.props.userSetting.currencyDecimalPlaces;

    return (
      <section className={`${ROOT}`}>
        <header className={`${ROOT}__header`}>
          <h1 className={`${ROOT}__header-body`}>
            {msg().Appr_Lbl_ApprovalList}
          </h1>
        </header>

        <SearchArea
          isProxyMode={this.props.proxyEmployeeInfo.isProxyMode}
          fetchInitialEmployeeList={this.props.fetchInitialEmployeeList}
          advSearchCondition={this.props.advSearchCondition}
          onClickInputValueStatus={this.props.onClickInputValueStatus}
          employeeOptions={this.props.employeeOptions}
          onClickInputValueEmployee={this.props.onClickInputValueEmployee}
          onClickInputValueSubmitDate={this.props.onClickInputValueSubmitDate}
          onClickAdvSearchButton={this.props.onClickAdvSearchButton}
          clearSearchCondition={this.props.clearSearchCondition}
        />

        {this.props.expIdsInfo.totalSize > 0 && (
          <div className={`${ROOT}__header-page`}>
            <PagerInfo
              className={`${ROOT}__page-info`}
              currentPage={this.props.currentPage}
              totalNum={this.props.expIdsInfo.totalSize}
              pageSize={PAGE_SIZE}
            />
            <button
              className={`${ROOT}__refresh-btn`}
              onClick={(searchCondition) =>
                this.props.onClickRefreshButton(searchCondition)
              }
            >
              <Icon type="refresh-copy" size="small" />
            </button>
          </div>
        )}

        <div className={`${ROOT}__table`}>
          <Grid
            data={this.props.requestList}
            idKey="requestId"
            columns={[
              {
                name: `${msg().Exp_Lbl_Status}`,
                key: 'status',
                width: 90,
                shrink: false,
                grow: false,
                formatter: this.statusFormatter,
              },
              {
                name: `${msg().Appr_Lbl_EmployeeName} / ${
                  msg().Appr_Lbl_DepartmentName
                }`,
                key: ['photoUrl', 'employeeName', 'departmentName'],
                shrink: true,
                grow: true,
                formatter: Employee,
              },
              {
                name: `${msg().Exp_Clbl_ReportTitle} / ${
                  msg().Exp_Lbl_ReportNo
                }`,
                key: ['subject', 'reportNo'],
                width: 180,
                shrink: false,
                grow: false,
                formatter: Request,
              },
              {
                name: msg().Appr_Lbl_RequestDate,
                key: 'requestDate',
                width: 90,
                shrink: false,
                grow: false,
                formatter: DateYMD,
              },
              {
                name: `${msg().Exp_Clbl_Amount}`,
                key: 'totalAmount',
                extraProps: { baseCurrencySymbol, baseCurrencyDecimal },
                width: 110,
                shrink: false,
                grow: false,
                formatter: Currency,
              },
            ]}
            selected={this.props.selectedIds}
            browseId={this.props.browseId}
            onClickRow={this.props.browseDetail}
            onChangeRowSelection={() => {}}
            emptyMessage={msg().Appr_Msg_EmptyRequestList}
          />
        </div>

        {this.props.currentPage === MAX_PAGE_NUM &&
          this.props.expIdsInfo.totalSize > MAX_SEARCH_RESULT_NUM && (
            <div className={`${ROOT}__too-many-results`}>
              {msg().Com_Lbl_TooManySearchResults}
            </div>
          )}

        {this.props.expIdsInfo.totalSize > 0 && (
          <div className={`${ROOT}__footer`}>
            <PagerInfo
              className={`${ROOT}__page-info`}
              currentPage={this.props.currentPage}
              totalNum={this.props.expIdsInfo.totalSize}
              pageSize={PAGE_SIZE}
            />
            <Pagination
              className={`${ROOT}__pager`}
              currentPage={this.props.currentPage}
              totalNum={this.props.expIdsInfo.totalSize}
              displayNum={5}
              pageSize={PAGE_SIZE}
              onClickPagerLink={(num) => this.props.onClickPagerLink(num)}
              maxPageNum={MAX_PAGE_NUM}
            />
          </div>
        )}
      </section>
    );
  }
}
