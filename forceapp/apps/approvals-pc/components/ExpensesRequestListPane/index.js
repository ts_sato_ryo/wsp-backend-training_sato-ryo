import React from 'react';

import PaneWrapper from '../PaneWrapper';
import ListContainer from '../../containers/ExpensesRequestListPane/ListContainer';
import DetailContainer from '../../containers/ExpensesRequestListPane/DetailContainer';

export default class AttMonthlyProcessListPane extends React.Component {
  render() {
    return (
      <PaneWrapper list={<ListContainer />} detail={<DetailContainer />} />
    );
  }
}
