import React from 'react';
import PropTypes from 'prop-types';
import AnimateHeight from 'react-animate-height';
import uuid from 'uuid/v1';

import classNames from 'classnames';
import Button from '../../commons/components/buttons/Button';

import './Collapse.scss';

import imgIconArrowDown from '../../commons/images/iconArrowDown.png';

const ROOT = 'approvals-pc-collapse';

export default class Collapse extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
      .isRequired,
    isOpen: PropTypes.bool,
    headingLevel: PropTypes.number,
    header: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    title: PropTypes.string,
  };

  static defaultProps = {
    isOpen: false,
    headingLevel: 3,
    header: null,
    title: '',
  };

  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.isOpen,
      collapseId: `${ROOT}-${uuid()}`,
    };

    this.onClickToggle = this.onClickToggle.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isOpen !== nextProps.isOpen) {
      this.state = {
        isOpen: nextProps.isOpen,
      };
    }
  }

  onClickToggle() {
    this.setState((prevState) => ({
      isOpen: !prevState.isOpen,
    }));
  }

  render() {
    const height = this.state.isOpen ? 'auto' : 0;

    const Heading = `h${this.props.headingLevel}`;

    const buttonClass = classNames(`${ROOT}__toggle`, {
      [`${ROOT}__toggle--is-open`]: this.state.isOpen,
    });
    return (
      <section className={`${ROOT}`}>
        <header className={`${ROOT}__header`}>
          <Button
            type="default"
            className={buttonClass}
            onClick={this.onClickToggle}
            aria-expanded={this.state.isOpen}
            aria-controls={this.state.collapseId}
          >
            <img src={imgIconArrowDown} alt="toggle" />
          </Button>

          <Heading>{this.props.title}</Heading>

          {this.props.header}
        </header>
        <AnimateHeight height={height}>
          <div id={this.state.collapseId} className={`${ROOT}__panel`}>
            {this.props.children}
          </div>
        </AnimateHeight>
      </section>
    );
  }
}
