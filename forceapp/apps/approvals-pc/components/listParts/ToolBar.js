// @flow
import * as React from 'react';

import ApprovalTypeSwitch from './ApprovalTypeSwitch';
import type { Permission } from '../../../domain/models/access-control/Permission';
import type { ApprovalTypeValue } from '../../../domain/models/approval/ApprovalType';
import AccessControl from '../../../commons/containers/AccessControlContainer';
import msg from '../../../commons/languages';

import './ToolBar.scss';

const ROOT = 'approvals-pc-list-parts-tool-bar';

type ApprovalTypeSwitchProps =
  | {|
      useApprovalTypeSwitch: true,
      requiredPermissionForDelegate: $Keys<Permission>[],
      approvalType: ApprovalTypeValue,
      onSwitchApprovalType: (ApprovalTypeValue) => void,
    |}
  | {|
      useApprovalTypeSwitch: false,
    |};

type Props = {|
  totalCount: number,
  isFilterUsing: boolean,
  filterMatchedCount: number,
  ...ApprovalTypeSwitchProps,
|};

export default class ToolBar extends React.Component<Props> {
  renderApprovalTypeSwitch() {
    if (this.props.useApprovalTypeSwitch !== true) {
      return null;
    }

    return (
      <AccessControl
        requireIfByEmployee={this.props.requiredPermissionForDelegate}
      >
        <ApprovalTypeSwitch
          approvalType={this.props.approvalType}
          onSwitch={this.props.onSwitchApprovalType}
        />
      </AccessControl>
    );
  }

  renderCount() {
    const filterMatchedCountStr = this.props.isFilterUsing
      ? `${this.props.filterMatchedCount} ${msg().Appr_Lbl_RecordCount} / `
      : null;

    return (
      <span className={`${ROOT}__count`}>
        {filterMatchedCountStr}
        {`${this.props.totalCount} ${msg().Appr_Lbl_RecordCount}`}
      </span>
    );
  }

  render() {
    return (
      <div className={ROOT}>
        {this.renderApprovalTypeSwitch()}
        {this.renderCount()}
      </div>
    );
  }
}
