import React from 'react';
import PropTypes from 'prop-types';

import DateUtil from '../../../../commons/utils/DateUtil';
import TimeUtil from '../../../../commons/utils/TimeUtil';
import TextUtil from '../../../../commons/utils/TextUtil';
import msg from '../../../../commons/languages';

// Use DayType from timesheet-pc-summary
// This is temporary fix. I don't think it's good idea to share this component.
import DayType from '../../../../timesheet-pc-summary/models/DayType';
import { CODE as ATT_DAILY_ATTENTION_CODE } from '../../../../domain/models/attendance/AttDailyAttention';

import iconAttentions from '../../../../commons/images/iconAttention.png';

import './RecordTable.scss';

const ROOT = 'approvals-pc-att-monthly-process-list-pane-detail-record-table';

const signTranslations = {
  [DayType.WORKDAY]: '',
  [DayType.HOLIDAY]: '◯',
  [DayType.LEGAL_HOLIDAY]: '◎',
};

const formatDate = (dateString) =>
  DateUtil.getDate(dateString) === 1
    ? DateUtil.formatMDW(dateString)
    : DateUtil.formatDW(dateString);

const formatDayType = (dayType) => {
  return dayType ? signTranslations[dayType] : '';
};

const formatDuration = (durationInMinutes) => {
  return durationInMinutes !== null ? TimeUtil.toHHmm(durationInMinutes) : '';
};

const formatTime = (timeInMinutes) => {
  return timeInMinutes !== null ? TimeUtil.toHHmm(timeInMinutes) : '';
};

const formatDurationTotal = (totalInMinutes) => {
  return TimeUtil.toHHmm(totalInMinutes || 0);
};

export default class RecordTable extends React.Component {
  static get propTypes() {
    return {
      records: PropTypes.array.isRequired,
      attentions: PropTypes.object.isRequired,
      restTimeTotal: PropTypes.number.isRequired,
      realWorkTimeTotal: PropTypes.number.isRequired,
      overTimeTotal: PropTypes.number.isRequired,
      nightTimeTotal: PropTypes.number.isRequired,
      lostTimeTotal: PropTypes.number.isRequired,
      virtualWorkTimeTotal: PropTypes.number.isRequired,
      holidayWorkTimeTotal: PropTypes.number.isRequired,
    };
  }

  renderAttentions(attentions) {
    if (!attentions.length) {
      return '';
    }
    return (
      <div className={`${ROOT}__system-remarks`}>
        {attentions.map((attention, idx) => {
          const { code, value } = attention;
          switch (code) {
            case ATT_DAILY_ATTENTION_CODE.IneffectiveWorkingTime:
              return (
                <p key={idx}>
                  {TextUtil.template(
                    msg().Att_Msg_SummaryCommentIneffectiveWorkingTime,
                    TimeUtil.toHHmm(value.fromTime),
                    TimeUtil.toHHmm(value.toTime)
                  )}
                </p>
              );
            case ATT_DAILY_ATTENTION_CODE.InsufficientRestTime:
              return (
                <p key={idx}>
                  {TextUtil.template(
                    msg().Att_Msg_SummaryCommentInsufficientRestTime,
                    value
                  )}
                </p>
              );
            default:
              return null;
          }
        })}
      </div>
    );
  }

  renderRecords() {
    return this.props.records.map((record) => {
      const attentions = this.props.attentions[record.recordDate];
      return (
        <tr key={record.recordDate}>
          <td className={`${ROOT}__col-date`}>
            <div className={`${ROOT}__col-date-container`}>
              <div className={`${ROOT}__col-date-icon`}>
                {!!attentions.length && (
                  <img
                    src={iconAttentions}
                    title={msg().Appr_Msg_DailyAttentionTitle}
                  />
                )}
              </div>
              <div className={`${ROOT}__col-date-date`}>
                {formatDate(record.recordDate)}
              </div>
            </div>
          </td>
          <td className={`${ROOT}__col-date-sign`}>
            {formatDayType(record.dayType)}
          </td>
          <td>{record.event}</td>
          <td>{record.shift}</td>
          <td
            className={`${ROOT}__col-time${
              record.startTimeModified ? '--modified' : ''
            }`}
          >
            {formatTime(record.startTime)}
          </td>
          <td
            className={`${ROOT}__col-time${
              record.endTimeModified ? '--modified' : ''
            }`}
          >
            {formatTime(record.endTime)}
          </td>
          <td className={`${ROOT}__col-duration`}>
            {formatDuration(record.restTime)}
          </td>
          <td className={`${ROOT}__col-duration`}>
            {formatDuration(record.realWorkTime)}
          </td>
          <td className={`${ROOT}__col-duration`}>
            {formatDuration(record.overTime)}
          </td>
          <td className={`${ROOT}__col-duration`}>
            {formatDuration(record.nightTime)}
          </td>
          <td className={`${ROOT}__col-duration`}>
            {formatDuration(record.virtualWorkTime)}
          </td>
          <td className={`${ROOT}__col-duration`}>
            {formatDuration(record.holidayWorkTime)}
          </td>
          <td className={`${ROOT}__col-duration`}>
            {formatDuration(record.lostTime)}
          </td>
          <td>
            {record.remarks}
            {this.renderAttentions(attentions)}
          </td>
        </tr>
      );
    });
  }

  renderRecordTotalList() {
    return (
      <tr>
        <td colSpan="6" className={`${ROOT}__col-label`}>
          {msg().Att_Lbl_Total}
        </td>
        <td className={`${ROOT}__col-duration`}>
          {formatDurationTotal(this.props.restTimeTotal)}
        </td>
        <td className={`${ROOT}__col-duration`}>
          {formatDurationTotal(this.props.realWorkTimeTotal)}
        </td>
        <td className={`${ROOT}__col-duration`}>
          {formatDurationTotal(this.props.overTimeTotal)}
        </td>
        <td className={`${ROOT}__col-duration`}>
          {formatDurationTotal(this.props.nightTimeTotal)}
        </td>
        <td className={`${ROOT}__col-duration`}>
          {formatDurationTotal(this.props.virtualWorkTimeTotal)}
        </td>
        <td className={`${ROOT}__col-duration`}>
          {formatDurationTotal(this.props.holidayWorkTimeTotal)}
        </td>
        <td className={`${ROOT}__col-duration`}>
          {formatDurationTotal(this.props.lostTimeTotal)}
        </td>
        <td />
      </tr>
    );
  }

  renderTable() {
    return (
      <table className={`${ROOT}__table`}>
        <thead>
          <tr>
            <th colSpan="2">{msg().Com_Lbl_Date}</th>
            <th>{msg().Att_Lbl_RequestAndEvent}</th>
            <th>{msg().Att_Lbl_ShiftAndShortTimeWork}</th>
            <th>{msg().Att_Lbl_TimeIn}</th>
            <th>{msg().Att_Lbl_TimeOut}</th>
            <th>{msg().Att_Lbl_Rest}</th>
            <th>{msg().Att_Lbl_ActualWork}</th>
            <th>{msg().Att_Lbl_Overtime}</th>
            <th>{msg().Att_Lbl_LateNight}</th>
            <th>{msg().Att_Lbl_DailyVirtualWorkTime}</th>
            <th>{msg().Att_Lbl_HolidayWorkTime}</th>
            <th>{msg().Att_Lbl_Deducted}</th>
            <th>{msg().Att_Lbl_Remarks}</th>
          </tr>
        </thead>
        <tbody>{this.renderRecords()}</tbody>
        <tfoot>
          <tr className={`${ROOT}__divider`} />
          {this.renderRecordTotalList()}
        </tfoot>
      </table>
    );
  }

  render() {
    return (
      <div className={ROOT}>
        {this.renderTable()}
        <div className={`${ROOT}__footer`}>
          {formatDayType(DayType.HOLIDAY)} = {msg().Att_Lbl_StatutoryHoliday},{' '}
          {formatDayType(DayType.LEGAL_HOLIDAY)} = {msg().Att_Lbl_LegalHoliday}
        </div>
      </div>
    );
  }
}
