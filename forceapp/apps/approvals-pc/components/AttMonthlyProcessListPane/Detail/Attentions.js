/* @flow */
import React from 'react';

import msg from '../../../../commons/languages';
import TextUtil from '../../../../commons/utils/TextUtil';

import iconAttentions from '../../../../commons/images/iconAttention.png';

import './Attentions.scss';

const ROOT = 'approvals-pc-att-monthly-process-list-pane-detail-attentions';

type Props = $ReadOnly<{
  ineffectiveWorkingTime: number,
  insufficientRestTime: number,
}>;

export default class Attentions extends React.Component<Props> {
  render() {
    const { ineffectiveWorkingTime, insufficientRestTime } = this.props;
    if (!ineffectiveWorkingTime && !insufficientRestTime) {
      return '';
    }
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__icon`}>
          <img src={iconAttentions} alt="" />
        </div>
        <div className={`${ROOT}__messages`}>
          <ul className={`${ROOT}__list`}>
            {insufficientRestTime > 0 ? (
              <li className={`${ROOT}__list-item`}>
                {TextUtil.template(
                  msg().Appr_Msg_FixSummaryConfirmInsufficientRestTime,
                  insufficientRestTime
                )}
              </li>
            ) : null}
            {ineffectiveWorkingTime > 0 ? (
              <li className={`${ROOT}__list-item`}>
                {TextUtil.template(
                  msg().Appr_Msg_FixSummaryConfirmIneffectiveWorkingTime,
                  ineffectiveWorkingTime
                )}
              </li>
            ) : null}
          </ul>
        </div>
      </div>
    );
  }
}
