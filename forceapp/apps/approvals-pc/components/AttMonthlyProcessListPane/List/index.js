// @flow
import React from 'react';

import msg from '../../../../commons/languages';
import ToolBar from '../../listParts/ToolBar';
import Grid from '../../../../commons/components/Grid';
import DoubleTextFilter from '../../../../commons/components/Grid/filters/DoubleTextFilter';
import DateFilter from '../../../../commons/components/Grid/filters/DateFilter';
import SelectFilter from '../../../../commons/components/Grid/filters/SelectFilter';
import Employee from '../../../../commons/components/Grid/Formatters/Employee';
import DateYMD from '../../../../commons/components/Grid/Formatters/DateYMD';
import DateYM from '../../../../commons/components/Grid/Formatters/DateYM';
import ApprovalType, {
  type ApprovalTypeValue,
} from '../../../../domain/models/approval/ApprovalType';
import type { State as FilterTermsType } from '../../../modules/ui/attMonthly/list/filterTerms';

import './index.scss';

const ROOT = 'approvals-pc-att-monthly-process-list-pane-list';

type Props = {
  totalCount: number,
  requestList: Array<*>,
  filterTerms: FilterTermsType,
  existingMonths: Array<string | { text: string, value: any }>,
  selectedIds: Array<string>,
  browseId: string,
  approvalType: ApprovalTypeValue,
  onClickRow: (string) => void,
  onChangeRowSelection: (string) => void,
  onSwitchApprovalType: (ApprovalTypeValue) => void,
  onUpdateFilterTerm: ($Keys<FilterTermsType>, string) => void,
};

export default class List extends React.Component<Props> {
  buildColumnsSetting() {
    const { filterTerms, onUpdateFilterTerm } = this.props;
    const bindOnUpdateFilterTermByKey = (key: $Keys<FilterTermsType>) => (
      value: string
    ) => onUpdateFilterTerm(key, value);

    const employee = {
      name: `${msg().Appr_Lbl_ApplicantName} / ${
        msg().Appr_Lbl_DepartmentName
      }`,
      key: ['photoUrl', 'employeeName', 'departmentName'],
      shrink: true,
      grow: true,
      width: 87, // NOTE: ブラウザ幅1024px時の表示から算定した基準値
      formatter: Employee,
      renderFilter: () => (
        <DoubleTextFilter
          firstValue={filterTerms.employeeName}
          secondValue={filterTerms.departmentName}
          onChangeFirstValue={bindOnUpdateFilterTermByKey('employeeName')}
          onChangeSecondValue={bindOnUpdateFilterTermByKey('departmentName')}
        />
      ),
    };

    const approver = {
      name: `${msg().Appr_Lbl_ApproverName} / ${msg().Appr_Lbl_DepartmentName}`,
      key: ['approverPhotoUrl', 'approverName', 'approverDepartmentName'],
      shrink: true,
      grow: true,
      width: 87, // NOTE: ブラウザ幅1024px時の表示から算定した基準値
      formatter: Employee,
      extraProps: {
        keyMap: {
          photoUrl: 'approverPhotoUrl',
          employeeName: 'approverName',
          departmentName: 'approverDepartmentName',
        },
      },
      renderFilter: () => (
        <DoubleTextFilter
          firstValue={filterTerms.approverName}
          secondValue={filterTerms.approverDepartmentName}
          onChangeFirstValue={bindOnUpdateFilterTermByKey('approverName')}
          onChangeSecondValue={bindOnUpdateFilterTermByKey(
            'approverDepartmentName'
          )}
        />
      ),
    };

    const targetMonth = {
      name: msg().Appr_Lbl_Period,
      key: 'targetMonth',
      width: 100,
      shrink: false,
      grow: false,
      formatter: DateYM,
      renderFilter: () => (
        <SelectFilter
          options={this.props.existingMonths}
          value={filterTerms.targetMonth}
          onChange={bindOnUpdateFilterTermByKey('targetMonth')}
        />
      ),
    };

    const requestDate = {
      name: msg().Appr_Lbl_RequestDate,
      key: 'requestDate',
      width: 180,
      shrink: false,
      grow: false,
      formatter: DateYMD,
      renderFilter: () => (
        <DateFilter
          value={filterTerms.requestDate}
          onChange={bindOnUpdateFilterTermByKey('requestDate')}
        />
      ),
    };

    return this.props.approvalType === ApprovalType.ByEmployee
      ? [employee, targetMonth, requestDate]
      : [employee, approver, targetMonth, requestDate];
  }

  render() {
    const isFilterUsing = Object.values(this.props.filterTerms).some(
      (value) => value !== ''
    );

    return (
      <section className={`${ROOT}`}>
        <header className={`${ROOT}__header`}>
          <h1 className={`${ROOT}__header-body`}>
            {msg().Appr_Lbl_ApprovalList}
          </h1>
        </header>

        <ToolBar
          useApprovalTypeSwitch
          requiredPermissionForDelegate={['approveAttRequestByDelegate']}
          approvalType={this.props.approvalType}
          onSwitchApprovalType={this.props.onSwitchApprovalType}
          totalCount={this.props.totalCount}
          isFilterUsing={isFilterUsing}
          filterMatchedCount={this.props.requestList.length}
        />

        <div className={`${ROOT}__table`}>
          <Grid
            data={this.props.requestList}
            idKey="id"
            columns={this.buildColumnsSetting()}
            selected={this.props.selectedIds}
            browseId={this.props.browseId}
            onClickRow={this.props.onClickRow}
            onChangeRowSelection={this.props.onChangeRowSelection}
            useFilter
            emptyMessage={msg().Appr_Msg_EmptyRequestList}
          />
        </div>
      </section>
    );
  }
}
