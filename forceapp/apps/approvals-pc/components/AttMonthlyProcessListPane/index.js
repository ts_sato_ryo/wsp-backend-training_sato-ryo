// @flow

import React from 'react';

import PaneExpandWrapper from '../PaneExpandWrapper';
import ListContainer from '../../containers/AttMonthlyProcessListPane/ListContainer';
import DetailContainer from '../../containers/AttMonthlyProcessListPane/DetailContainer';

type Props = {|
  isExpanded: boolean,
|};

export default class AttMonthlyProcessListPane extends React.Component<Props> {
  render() {
    return (
      <PaneExpandWrapper
        list={<ListContainer />}
        detail={<DetailContainer />}
        detailExpanded={this.props.isExpanded}
      />
    );
  }
}
