// @flow
import * as React from 'react';

import msg from '../../../../commons/languages';
import displayName from '../../../../commons/concerns/displayName';
import { compose } from '../../../../commons/utils/FnUtil';
import Form from '../../../../commons/components/Form';
import TextField from '../../../../commons/components/fields/TextField';
import IconButton from '../../../../commons/components/buttons/IconButton';
import btnSearch from '../../../../commons/images/icon_search.png';

import './EmployeeSearchForm.scss';

const ROOT = 'admin-pc-employee-delegate-apporval-employee-search-form';

export type Props = $ReadOnly<{|
  search: (
    $ReadOnly<{|
      name?: string,
      code?: string,
      departmentName?: string,
      departmentCode?: string,
      title?: string,
    |}>
  ) => void,
|}>;

type InternalProps = $ReadOnly<{|
  name?: string,
  code?: string,
  departmentName?: string,
  departmentCode?: string,
  title?: string,
  updateValue: (key: string) => (value: string) => void,
  search: () => void,
|}>;

const shouldComponentUpdate = (condition: (props: *) => boolean) => (
  WrappedComponent: React.ComponentType<*>
) => {
  return class extends React.Component<Props> {
    shouldComponentUpdate() {
      return condition(this.props);
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
};

const withSearchable = (WrappedComponent: React.ComponentType<*>) => {
  type State = {|
    name?: string,
    code?: string,
    departmentName?: string,
    departmentCode?: string,
    title?: string,
  |};

  return class extends React.PureComponent<Props, State> {
    updateValue: Function;
    search: () => void;

    constructor(_props: Props) {
      super();

      this.state = {
        name: '',
        code: '',
        departmentName: '',
        departmentCode: '',
      };

      this.updateValue = this.updateValue.bind(this);
      this.search = this.search.bind(this);
    }

    updateValue(key: string) {
      return (value: string) =>
        this.setState({
          [key]: value,
        });
    }

    search() {
      this.props.search({
        name: this.state.name,
        code: this.state.code,
        departmentName: this.state.departmentName,
        departmentCode: this.state.departmentCode,
        title: this.state.title,
      });
    }

    render() {
      return (
        <WrappedComponent
          {...(this.props: Object)}
          updateValue={this.updateValue}
          search={this.search}
        />
      );
    }
  };
};

class Presentation extends React.PureComponent<InternalProps> {
  render() {
    const Control = ({ onChange, label, value, autoFocus }) => {
      return (
        <div className={`${ROOT}__control`}>
          <div className={`${ROOT}__label`}>{label}</div>
          <div className={`${ROOT}__input`}>
            <TextField
              onChange={(_e, v: string) => onChange(v)}
              value={value}
              autoFocus={autoFocus}
              placeholder={msg().Com_Lbl_Search}
            />
          </div>
        </div>
      );
    };

    return (
      <div className={ROOT}>
        <Form onSubmit={() => this.props.search()}>
          <div className={`${ROOT}__form`}>
            <Control
              onChange={this.props.updateValue('code')}
              value={this.props.code}
              label={msg().Com_Lbl_EmployeeCode}
              autoFocus={false}
            />
            <Control
              onChange={this.props.updateValue('name')}
              value={this.props.name}
              label={msg().Com_Lbl_EmployeeName}
              autoFocus
            />
            <Control
              onChange={this.props.updateValue('departmentCode')}
              value={this.props.departmentCode}
              label={msg().Com_Lbl_DepartmentCode}
              autoFocus={false}
            />
            <Control
              onChange={this.props.updateValue('departmentName')}
              value={this.props.departmentName}
              label={msg().Com_Lbl_DepartmentName}
              autoFocus={false}
            />
            <Control
              onChange={this.props.updateValue('title')}
              value={this.props.title}
              label={msg().Com_Lbl_Title}
              autoFocus={false}
            />
            <IconButton
              src={btnSearch}
              className={`${ROOT}__search-button`}
              onClick={() => this.props.search()}
            />
          </div>
        </Form>
      </div>
    );
  }
}

export default (compose(
  displayName('EmployeeSelectionSearchForm'),
  shouldComponentUpdate(() => false),
  withSearchable
)(Presentation): React.ComponentType<Object>);
