// @flow
import React from 'react';

import CustomDropdown from '../../../../../commons/components/fields/CustomDropdown';
import FilterableCheckbox from '../../../../../commons/components/fields/FilterableCheckbox';
import DropdownDateRange from '../../../../../commons/components/fields/DropdownDateRange';
import Button from '../../../../../commons/components/buttons/Button';
import msg from '../../../../../commons/languages';

import {
  type DateRangeOption,
  type EmployeeOption,
} from '../../../../../domain/models/exp/request/Report';

import './index.scss';

const ROOT = 'approvals-pc-expenses__search-area';

export type SearchConditions = {
  name?: string,
  statusList: Array<string>,
  empBaseIdList: Array<string>,
  requestDateRange: DateRangeOption,
};

type Props = {
  isProxyMode: boolean,
  onClickAdvSearchButton: () => void,
  employeeOptions: Array<EmployeeOption>,
  advSearchCondition: SearchConditions,
  onClickInputValueStatus: () => void,
  onClickInputValueEmployee: () => void,
  onClickInputValueSubmitDate: (
    dateRangeOption: DateRangeOption,
    needUpdate: boolean
  ) => void,
  clearSearchCondition: () => void,
  fetchInitialEmployeeList: () => void,
};

class SearchArea extends React.Component<Props> {
  componentDidMount() {
    this.props.fetchInitialEmployeeList();
    this.props.clearSearchCondition();
  }

  render() {
    const statusOptions = [
      { label: msg().Com_Status_Approved, value: 'Approved' },
      {
        label: msg().Com_Status_Pending,
        value: 'Pending',
      },
      {
        label: msg().Com_Status_Rejected,
        value: 'Rejected',
      },
    ];

    return (
      <div className={ROOT}>
        {this.props.isProxyMode ? (
          <div className={`${ROOT}-proxy-status`}>
            <span className={`${ROOT}-proxy-status-label`}>
              {msg().Com_Lbl_Status}:
            </span>
            <span className={`${ROOT}-proxy-status-value`}>
              {msg().Com_Status_Pending}
            </span>
          </div>
        ) : (
          <CustomDropdown
            valuePlaceholder={msg().Exp_Btn_SearchConditionStatus}
            selectedStringValues={this.props.advSearchCondition.statusList}
            data={statusOptions}
          >
            {(values, updateParentState) => (
              <FilterableCheckbox
                data={statusOptions}
                selectedStringValues={this.props.advSearchCondition.statusList}
                values={values}
                searchPlaceholder={
                  msg().Exp_Lbl_SearchConditionPlaceholderStatus
                }
                updateParentState={updateParentState}
                onSelectInput={this.props.onClickInputValueStatus}
                hasDisplayValue
              />
            )}
          </CustomDropdown>
        )}

        <CustomDropdown
          valuePlaceholder={msg().Exp_Btn_SearchConditionEmployee}
          selectedStringValues={this.props.advSearchCondition.empBaseIdList}
          data={this.props.employeeOptions}
        >
          {(values, updateParentState) => (
            <FilterableCheckbox
              data={this.props.employeeOptions}
              selectedStringValues={this.props.advSearchCondition.empBaseIdList}
              values={values}
              searchPlaceholder={
                msg().Exp_Lbl_SearchConditionPlaceholderEmployee
              }
              updateParentState={updateParentState}
              onSelectInput={this.props.onClickInputValueEmployee}
              hasDisplayValue
              maxLength={8}
            />
          )}
        </CustomDropdown>

        <CustomDropdown
          valuePlaceholder={msg().Exp_Btn_SearchConditionRequestDate}
          selectedDateRangeValues={
            this.props.advSearchCondition.requestDateRange
          }
        >
          {(values, updateParentState) => (
            <DropdownDateRange
              dateRange={this.props.advSearchCondition.requestDateRange}
              onClickUpdateDate={this.props.onClickInputValueSubmitDate}
              updateParentState={updateParentState}
              values={values}
              needStartDate
            />
          )}
        </CustomDropdown>

        <Button
          onClick={this.props.onClickAdvSearchButton}
          className={`${ROOT}-search-button`}
        >
          {msg().Exp_Btn_Search}
        </Button>
      </div>
    );
  }
}

export default SearchArea;
