import React from 'react';

import PaneWrapper from '../PaneWrapper';
import ListContainer from '../../containers/ExpensesPreApprovalListPane/ListContainer';
import DetailContainer from '../../containers/ExpensesPreApprovalListPane/DetailContainer';

export default class ExpensesPreApprovalListPane extends React.Component {
  render() {
    return (
      <PaneWrapper list={<ListContainer />} detail={<DetailContainer />} />
    );
  }
}
