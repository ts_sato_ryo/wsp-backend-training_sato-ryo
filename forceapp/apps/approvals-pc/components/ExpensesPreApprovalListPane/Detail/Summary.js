// @flow
import React from 'react';
import _ from 'lodash';

import msg from '../../../../commons/languages';
import FormatUtil from '../../../../commons/utils/FormatUtil';
import DateUtil from '../../../../commons/utils/DateUtil';

import Button from '../../../../commons/components/buttons/Button';
import HorizontalLayout from '../../../../commons/components/fields/layouts/HorizontalLayout';

import { type ExpRequest } from '../../../../domain/models/exp/request/Report';
import { getLabelValueFromEIs } from '../../../../domain/models/exp/ExtendedItem';
import { VENDOR_PAYMENT_DUE_DATE_USAGE } from '../../../../domain/models/exp/Vendor';

const ROOT = 'approvals-pc-expenses-pre-approval-pane-detail';

type Props = {
  expRequest: ExpRequest,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  openVendorDetail: () => void,
};

export default class RequestSummary extends React.Component<Props> {
  getVendorInfo = (expRequest: ExpRequest) => {
    const {
      vendorCode,
      vendorName,
      paymentDueDate,
      paymentDueDateUsage,
    } = expRequest;
    let vendorInfo = [];

    if (!vendorCode) {
      return vendorInfo;
    }

    vendorInfo = [
      {
        label: msg().Exp_Lbl_Vendor,
        value: `${vendorCode} - ${vendorName}`,
        extra: (
          <Button
            className={`${ROOT}__vendor-detail`}
            onClick={this.props.openVendorDetail}
          >
            {msg().Exp_Btn_VendorDetail}
          </Button>
        ),
      },
    ];

    if (paymentDueDateUsage !== VENDOR_PAYMENT_DUE_DATE_USAGE.NotUsed) {
      const paymentDateInfo = {
        label: msg().Exp_Lbl_PaymentDate,
        value: DateUtil.formatYMD(paymentDueDate) || '',
      };
      vendorInfo.push(paymentDateInfo);
    }

    return vendorInfo;
  };

  render() {
    const { expRequest } = this.props;
    const JOB_LABEL = msg().Exp_Lbl_Job;
    const CC_LABEL = msg().Exp_Clbl_CostCenter;
    const hasValueOrNotJobOrCC = ({ label, value }) =>
      !(label === JOB_LABEL || label === CC_LABEL) || value;

    const renderItems = [
      {
        label: msg().Exp_Clbl_RequestTitle,
        value: expRequest.subject,
      },
      {
        label: msg().Exp_Clbl_ScheduledDate,
        value: DateUtil.formatYMD(expRequest.scheduledDate),
      },
      {
        label: `${msg().Exp_Lbl_TotalAmount}`,
        value: `${this.props.baseCurrencySymbol} ${FormatUtil.formatNumber(
          expRequest.totalAmount,
          this.props.baseCurrencyDecimal
        )}`,
      },
      {
        label: msg().Exp_Clbl_Purpose,
        value: expRequest.purpose,
      },
      {
        label: msg().Exp_Clbl_ReportType,
        value: expRequest.expReportTypeName || '',
      },
      ...getLabelValueFromEIs(expRequest),
      ...this.getVendorInfo(expRequest),
      {
        label: JOB_LABEL,
        value: this.props.expRequest.jobName
          ? `${this.props.expRequest.jobCode} - ${this.props.expRequest.jobName}`
          : '',
      },
      {
        label: CC_LABEL,
        value: this.props.expRequest.costCenterName
          ? `${this.props.expRequest.costCenterCode} - ${this.props.expRequest.costCenterName}`
          : '',
      },
    ];

    return (
      <div className={`${ROOT}__container`}>
        <ul className={`${ROOT}__list-detail`}>
          {renderItems.filter(hasValueOrNotJobOrCC).map((addon) => {
            return (
              <li key={addon.label} className={`${ROOT}__list-detail-item`}>
                <HorizontalLayout>
                  <HorizontalLayout.Label cols={3}>
                    {addon.label}
                  </HorizontalLayout.Label>

                  <HorizontalLayout.Body cols={9}>
                    {addon.value}
                    {addon.extra ? addon.extra : null}
                  </HorizontalLayout.Body>
                </HorizontalLayout>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
