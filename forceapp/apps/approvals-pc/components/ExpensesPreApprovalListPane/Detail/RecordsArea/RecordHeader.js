// @flow
import React from 'react';
import classNames from 'classnames';
import { get } from 'lodash';

import FormatUtil from '../../../../../commons/utils/FormatUtil';
import DateUtil from '../../../../../commons/utils/DateUtil';

import Button from '../../../../../commons/components/buttons/Button';

import { type ExpRequestRecord } from '../../../../../domain/models/exp/request/Report';
import { isFixedAllowanceMulti } from '../../../../../domain/models/exp/Record';

import imgIconArrowDown from '../../../../../commons/images/iconArrowDown.png';

import './RecordHeader.scss';

type Props = {
  isOpen: boolean,
  record: ExpRequestRecord,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  onClickRecordOpenButton: (string) => void,
};

const ROOT =
  'approvals-pc-expenses-pre-approval-pane-detail__records-area-record__header';

export default class RecordHeader extends React.Component<Props> {
  render() {
    const { record } = this.props;
    const headerClass = classNames(`${ROOT}`, {
      [`${ROOT}--is-open`]: this.props.isOpen,
    });

    const buttonClass = classNames(`${ROOT}__toggle`, {
      [`${ROOT}__toggle--is-open`]: this.props.isOpen,
    });

    const fixedAmountLabel =
      (isFixedAllowanceMulti(record.recordType) &&
        get(record, 'items[0].fixedAllowanceOptionLabel')) ||
      '';

    const expTypeAmountLabel = fixedAmountLabel && ` : ${fixedAmountLabel}`;
    return (
      <header className={headerClass}>
        <Button
          type="default"
          className={buttonClass}
          onClick={() => this.props.onClickRecordOpenButton(record.recordId)}
          aria-expanded={this.props.isOpen}
          aria-controls={record.recordId}
        >
          <img src={imgIconArrowDown} alt="toggle" />
        </Button>
        <div className={`${ROOT}-date`}>
          {DateUtil.formatYMD(record.recordDate)}
        </div>
        <div
          className={`${ROOT}-exp-type`}
        >{`${record.items[0].expTypeName}${expTypeAmountLabel}`}</div>
        <div className={`${ROOT}-amount`}>
          {`${this.props.baseCurrencySymbol} ${FormatUtil.formatNumber(
            record.amount,
            this.props.baseCurrencyDecimal
          )}`}
        </div>
      </header>
    );
  }
}
