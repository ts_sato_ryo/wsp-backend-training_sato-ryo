/* @flow */

import React from 'react';

import PaneExpandWrapper from '../PaneExpandWrapper';
import ListContainer from '../../containers/AttDailyProcessListPane/ListContainer';
import DetailContainer from '../../containers/AttDailyProcessListPane/DetailContainer';

type Props = {|
  isExpanded: boolean,
|};

export default class AttDailyProcessListPane extends React.Component<Props> {
  render() {
    return (
      <PaneExpandWrapper
        list={<ListContainer />}
        detail={<DetailContainer />}
        detailExpanded={this.props.isExpanded}
      />
    );
  }
}
