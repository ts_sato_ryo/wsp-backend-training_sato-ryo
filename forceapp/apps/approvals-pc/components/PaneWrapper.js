import React from 'react';
import PropTypes from 'prop-types';

import './PaneWrapper.scss';

const ROOT = 'approvals-pc-pane-wrapper';

export default class PaneWrapper extends React.Component {
  static propTypes = {
    list: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    detail: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  };

  render() {
    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__list`}>{this.props.list}</div>
        <div className={`${ROOT}__detail`}>{this.props.detail}</div>
      </div>
    );
  }
}
