import React from 'react';
import { storiesOf } from '@storybook/react';

import Collapse from '../components/Collapse';

storiesOf('approvals-pc', module).add(
  'Collapse',
  () => (
    <Collapse title="一覧">
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
      <br />
      コンテンツ
    </Collapse>
  ),
  { info: { propTables: [Collapse], inline: true, source: true } }
);
