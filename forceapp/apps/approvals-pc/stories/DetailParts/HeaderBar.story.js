import React from 'react';
import { storiesOf } from '@storybook/react';

import HeaderBar from '../../components/DetailParts/HeaderBar';

storiesOf('approvals-pc/DetailParts', module)
  .add(
    'HeaderBar',
    () => (
      <HeaderBar
        title="title"
        meta={[
          { label: '項目1', value: '値1' },
          { label: '項目2', value: '値2' },
        ]}
      />
    ),
    { info: { propTables: [HeaderBar], inline: true, source: true } }
  )
  .add(
    'HeaderBar - Change heding level',
    () => (
      <HeaderBar
        title="見出しレベルを変更"
        headingLevel={3}
        meta={[
          { label: '項目1', value: '値1' },
          { label: '項目2', value: '値2' },
        ]}
      />
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'HeaderBar - Expandable pane',
    () => (
      <HeaderBar
        title="見出しレベルを変更"
        headingLevel={3}
        meta={[
          { label: '項目1', value: '値1' },
          { label: '項目2', value: '値2' },
        ]}
        isExpand
        onTogglePane={() => {}}
      />
    ),
    { info: { propTables: false, inline: true, source: true } }
  );
