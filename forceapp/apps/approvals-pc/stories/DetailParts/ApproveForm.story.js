import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ApproveForm from '../../components/DetailParts/ApproveForm';

storiesOf('approvals-pc/DetailParts', module).add(
  'ApproveForm',
  () => (
    <ApproveForm
      onClickApproveButton={action('apporve')}
      onClickRejectButton={action('reject')}
      onChangeComment={action('change comment')}
    />
  ),
  { info: { propTables: [ApproveForm], inline: true, source: true } }
);
