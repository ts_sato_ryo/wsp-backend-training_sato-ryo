import React from 'react';
import { storiesOf } from '@storybook/react';

import HistoryTable from '../../components/DetailParts/HistoryTable';

import imgPhoto from '../../../commons/images/Sample_photo001.png';

const dummyHistoryList = [
  {
    id: 'id1',
    status: 'Pending',
    stepName: 'テスト1',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id2',
    status: 'Pending',
    stepName: 'テスト2',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id3',
    status: 'Pending',
    stepName: 'テスト3',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
];

storiesOf('approvals-pc/DetailParts', module)
  .add('HistoryTable', () => <HistoryTable historyList={dummyHistoryList} />, {
    info: { propTables: [HistoryTable], inline: true, source: true },
  })
  .add(
    'HistoryTable - ellipsis',
    () => <HistoryTable historyList={dummyHistoryList} isEllipsis />,
    { info: { propTables: [false], inline: true, source: true } }
  );
