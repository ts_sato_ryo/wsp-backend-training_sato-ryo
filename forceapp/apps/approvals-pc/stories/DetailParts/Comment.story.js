import React from 'react';
import { storiesOf } from '@storybook/react';

import Comment from '../../components/DetailParts/Comment';

storiesOf('approvals-pc/DetailParts', module).add(
  'Comment',
  () => (
    <Comment
      employeePhotoUrl=""
      comment="サンプルコメント　サンプルコメント　サンプルコメント　サンプルコメント　サンプルコメント　サンプルコメント"
    />
  ),
  { info: { propTables: [Comment], inline: true, source: true } }
);
