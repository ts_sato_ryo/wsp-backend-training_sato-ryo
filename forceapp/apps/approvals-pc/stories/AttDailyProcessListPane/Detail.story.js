import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Detail from '../../components/AttDailyProcessListPane/Detail';
import TextUtil from '../../../commons/utils/TextUtil';

import imgPhoto from '../../../commons/images/Sample_photo001.png';

const longText = `this
is
a
long long long long long long long`;

const dummyDetailList = [
  { label: '申請種別', value: '休暇申請' },
  { label: '休暇情報', value: '特別休暇' },
  { label: '範囲', value: '期間指定休暇' },
  { label: '期間', value: '2016/23/23 11:11:11 - 2016/23/23 11:11:11' },
  { label: '備考', value: TextUtil.nl2br(longText) },
];

const dummyHistoryList = [
  {
    id: 'id1',
    status: 'Pending',
    stepName: 'テスト1',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id2',
    status: 'Pending',
    stepName: 'テスト2',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id3',
    status: 'Pending',
    stepName: 'テスト3',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
];

storiesOf('approvals-pc/AttDailyProcessListPane/Detail', module)
  .add(
    'Detail',
    () => (
      <Detail
        id="id"
        employeeName="承認太郎"
        status="承認待ち"
        requestComment={longText}
        employeePhotoUrl={imgPhoto}
        detailList={dummyDetailList}
        historyList={dummyHistoryList}
        onChangeApproveComment={action('change comment')}
        onClickRejectButton={action('reject')}
        onClickApproveButton={action('approve')}
        editComment={() => {}}
        approveComment={longText}
        userPhotoUrl={imgPhoto}
      />
    ),
    { info: { propTables: [Detail], inline: true, source: true } }
  )
  .add(
    'Detail 承認内容変更申請',
    () => (
      <Detail
        id="id"
        employeeName="承認太郎"
        status="承認待ち"
        requestComment={longText}
        employeePhotoUrl={imgPhoto}
        detailList={dummyDetailList}
        historyList={dummyHistoryList}
        onChangeApproveComment={action('change comment')}
        onClickRejectButton={action('reject')}
        onClickApproveButton={action('approve')}
        editComment={() => {}}
        approveComment={longText}
        userPhotoUrl={imgPhoto}
        isForReapply
      />
    ),
    { info: { propTables: [Detail], inline: true, source: true } }
  );
