// @flow
import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import List from '../../components/AttMonthlyProcessListPane/List';
import { actions as detailActions } from '../../modules/entities/attMonthly/detail';
import * as AttMonthlyActions from '../../action-dispatchers/AttMonthly';
import {
  selectors as filterTermsSelector,
  actions as filterTermsUIActions,
  type State as FilterTermsType,
} from '../../modules/ui/attMonthly/list/filterTerms';

import type { ApprovalTypeValue } from '../../../domain/models/approval/ApprovalType';

type Props = {
  totalCount: number,
  requestList: Array<*>,
  filterTerms: FilterTermsType,
  existingMonths: Array<string | { text: string, value: any }>,
  selectedIds: Array<string>,
  approvalType: ApprovalTypeValue,
  browseId: string,
  initialize: () => void,
  browseDetail: (string) => void,
  updateFilterTerm: ($Keys<FilterTermsType>, string) => void,
  switchApprovalType: (ApprovalTypeValue) => void,
};

class ListContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    // TODO Implement selection change event
    return (
      <List
        totalCount={this.props.totalCount}
        requestList={this.props.requestList}
        filterTerms={this.props.filterTerms}
        existingMonths={this.props.existingMonths}
        selectedIds={this.props.selectedIds}
        approvalType={this.props.approvalType}
        browseId={this.props.browseId}
        onClickRow={this.props.browseDetail}
        onChangeRowSelection={() => {}}
        onSwitchApprovalType={this.props.switchApprovalType}
        onUpdateFilterTerm={this.props.updateFilterTerm}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  totalCount: state.entities.attMonthly.list.allIds.length,
  requestList: filterTermsSelector.extractRecordsByFilter(state),
  filterTerms: state.ui.attMonthly.list.filterTerms,
  existingMonths: filterTermsSelector.buildTargetMonthOptions(state),
  selectedIds: state.ui.attMonthly.list.selectedIds,
  browseId: state.entities.attMonthly.detail.id,
  approvalType: state.ui.approvalType,
});

const mapDispatchToProps = (dispatch: Dispatch<*>) =>
  bindActionCreators(
    {
      initialize: AttMonthlyActions.initialize,
      switchApprovalType: AttMonthlyActions.switchApprovalType,
      updateFilterTerm: filterTermsUIActions.update,
      browseDetail: detailActions.browse,
    },
    dispatch
  );

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(ListContainer): React.ComponentType<Object>);
