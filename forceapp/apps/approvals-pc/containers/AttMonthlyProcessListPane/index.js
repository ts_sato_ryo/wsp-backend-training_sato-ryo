// @flow

import { connect } from 'react-redux';

import AttMonthlyProcessListPane from '../../components/AttMonthlyProcessListPane';
import type { State } from '../../modules';

type Props = $PropertyType<AttMonthlyProcessListPane, 'props'>;

const mapStateToProps = (state: State): Props => {
  return {
    isExpanded: state.ui.attMonthly.isExpanded,
  };
};

export default (connect(mapStateToProps)(
  AttMonthlyProcessListPane
): React.ComponentType<Object>);
