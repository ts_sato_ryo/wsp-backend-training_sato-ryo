// @flow

import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import type { AttDailyRequest } from '../../modules/entities/att/list/actions';

import List from '../../components/AttDailyProcessListPane/List';
import * as detailActions from '../../modules/entities/att/detail/actions';
import * as AttDailyActions from '../../action-dispatchers/AttDaily';
import {
  selectors as filterTermsSelector,
  actions as filterTermsUIActions,
  type State as FilterTermsType,
} from '../../modules/ui/att/list/filterTerms';

import { type ApprovalTypeValue } from '../../../domain/models/approval/ApprovalType';

type Props = {
  totalCount: number,
  requestList: Array<AttDailyRequest>,
  filterTerms: FilterTermsType,
  existingRequestTypes: Array<string | { text: string, value: any }>,
  selectedIds: Array<string>,
  approvalType: ApprovalTypeValue,
  browseId: string,
  initialize: () => void,
  browseDetail: (string) => void,
  updateFilterTerm: ($Keys<FilterTermsType>, string) => void,
  switchApprovalType: (ApprovalTypeValue) => void,
};

class ListContainer extends React.Component<Props> {
  componentDidMount() {
    this.props.initialize();
  }

  render() {
    // TODO Implement selection change event
    return (
      <List
        totalCount={this.props.totalCount}
        requestList={this.props.requestList}
        filterTerms={this.props.filterTerms}
        existingRequestTypes={this.props.existingRequestTypes}
        selectedIds={this.props.selectedIds}
        approvalType={this.props.approvalType}
        browseId={this.props.browseId}
        onClickRow={this.props.browseDetail}
        onChangeRowSelection={() => {}}
        onSwitchApprovalType={this.props.switchApprovalType}
        onUpdateFilterTerm={this.props.updateFilterTerm}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  totalCount: state.entities.att.list.allIds.length,
  requestList: filterTermsSelector.extractRecordsByFilter(state),
  filterTerms: state.ui.att.list.filterTerms,
  existingRequestTypes: filterTermsSelector.buildRequestTypesOptions(state),
  selectedIds: state.ui.att.list.selectedIds,
  browseId: state.entities.att.detail.request.id,
  approvalType: state.ui.approvalType,
});

const mapDispatchToProps = (dispatch: Dispatch<*>) =>
  bindActionCreators(
    {
      initialize: AttDailyActions.initialize,
      switchApprovalType: AttDailyActions.switchApprovalType,
      updateFilterTerm: filterTermsUIActions.update,
      browseDetail: detailActions.browse,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  browseDetail: (id: string) => dispatchProps.browseDetail(id),
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ListContainer): React.ComponentType<Object>);
