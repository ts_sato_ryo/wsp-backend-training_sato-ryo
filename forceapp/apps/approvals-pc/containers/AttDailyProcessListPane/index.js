// @flow

import { connect } from 'react-redux';

import AttDailyProcessListPane from '../../components/AttDailyProcessListPane';
import type { State } from '../../modules';

type Props = $PropertyType<AttDailyProcessListPane, 'props'>;

const mapStateToProps = (state: State): Props => {
  return {
    isExpanded: state.ui.att.isExpanded,
  };
};

export default (connect(mapStateToProps)(
  AttDailyProcessListPane
): React.ComponentType<Object>);
