// @flow

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';

import {
  fetchAllIdsForExpApproval,
  fetchExpListForExpApproval,
  browseDetailForExpApproval,
} from '../../action-dispatchers/Expenses';
// import { actions as detailActions } from '../../modules/entities/expenses/detail';
//
import { actions as taxTypeActions } from '../../../domain/modules/exp/taxType';
import { actions as statusActions } from '../../modules/ui/expenses/list/advSearch/statusList';
import { actions as employeeActions } from '../../modules/ui/expenses/list/advSearch/empBaseIdList';
import { actions as requestDateActions } from '../../modules/ui/expenses/list/advSearch/requestDateRange';
import { actions as employeeListActions } from '../../modules/entities/expenses/advSearch/employeeList';

import {
  type DateRangeOption,
  submitDateInitVal,
} from '../../../domain/models/exp/request/Report';
import List from '../../components/ExpensesRequestListPane/List';

const mapStateToProps = (state) => ({
  selectedIds: state.ui.expenses.list.selectedIds,
  currentPage: state.ui.expenses.list.page,
  expIdsInfo: state.entities.exp.request.report.expIdsInfo || {},
  browseId: state.entities.exp.request.report.expRequest.requestId || '',
  requestList: state.entities.exp.request.report.expRequestList,
  userSetting: state.userSetting,
  advSearchCondition: state.ui.expenses.list.advSearch,
  employeeOptions: state.entities.expenses.advSearch.employeeList,
  proxyEmployeeInfo: state.common.proxyEmployeeInfo,
});

const mapDispatchToProps = (dispatch: Dispatch<*>) =>
  bindActionCreators(
    {
      fetchAllIdsForExpApproval,
      fetchExpListForExpApproval,
      browseDetailForExpApproval,
      taxList: taxTypeActions.search,
      onClickInputValueStatus: statusActions.set,
      onClickInputValueEmployee: employeeActions.set,
      fetchEmployeeList: employeeListActions.list,
      setSubmitDate: requestDateActions.set,
      clearStatus: statusActions.clear,
      clearEmployee: employeeActions.clear,
      clearSubmitDate: requestDateActions.clear,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickPagerLink: (pageNum: number) => {
    dispatchProps.fetchExpListForExpApproval(
      stateProps.expIdsInfo.requestIdList,
      pageNum
    );
  },
  onClickRefreshButton: () => {
    const {
      proxyEmployeeInfo: { id, isProxyMode, expReportRequestCount },
    } = stateProps;
    const empId = isProxyMode ? id : undefined;
    const isEmpty = isProxyMode && expReportRequestCount === 0;

    // To search using same condition for last search
    dispatchProps.fetchAllIdsForExpApproval(
      stateProps.advSearchCondition,
      empId,
      isEmpty
    );
    dispatchProps.taxList(moment().format('YYYY-MM-DD'));
  },
  onClickAdvSearchButton: () => {
    const {
      proxyEmployeeInfo: { id, isProxyMode, expReportRequestCount },
    } = stateProps;
    const empId = isProxyMode ? id : undefined;
    const isEmpty = isProxyMode && expReportRequestCount === 0;
    dispatchProps.fetchAllIdsForExpApproval(
      stateProps.advSearchCondition,
      empId,
      isEmpty
    );
  },
  browseDetail: (reportId: string) => {
    dispatchProps.browseDetailForExpApproval(reportId);
    dispatchProps.taxList(moment().format('YYYY-MM-DD'));
  },
  onClickInputValueSubmitDate: (
    date: DateRangeOption,
    needUpdateList: boolean
  ) => {
    dispatchProps.setSubmitDate(date);
    if (needUpdateList) {
      dispatchProps.fetchEmployeeList(
        stateProps.userSetting.companyId,
        date.startDate
      );
    }
  },
  clearSearchCondition: () => {
    dispatchProps.clearStatus();
    dispatchProps.clearEmployee();
    dispatchProps.clearSubmitDate();
  },
  fetchInitialEmployeeList: () => {
    const dateRange = submitDateInitVal();
    dispatchProps.fetchEmployeeList(
      stateProps.userSetting.companyId,
      dateRange.startDate
    );
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(List): React.ComponentType<Object>);
