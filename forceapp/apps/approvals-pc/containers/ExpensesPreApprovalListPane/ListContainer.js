// @flow

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import {
  browseDetailForPreApproval,
  fetchAllIdsForPreApproval,
  fetchExpListForPreApproval,
} from '../../action-dispatchers/Expenses';
import { actions as taxTypeActions } from '../../../domain/modules/exp/taxType';
import { actions as statusActions } from '../../modules/ui/expenses/list/advSearch/statusList';
import { actions as employeeActions } from '../../modules/ui/expenses/list/advSearch/empBaseIdList';
import { actions as requestDateActions } from '../../modules/ui/expenses/list/advSearch/requestDateRange';
import { actions as employeeListActions } from '../../modules/entities/expenses/advSearch/employeeList';

import {
  type DateRangeOption,
  submitDateInitVal,
} from '../../../domain/models/exp/request/Report';
import List from '../../components/ExpensesPreApprovalListPane/List';

const mapStateToProps = (state) => ({
  selectedIds: state.ui.expenses.list.selectedIds,
  currentPage: state.ui.expenses.list.page,
  expIdsInfo: state.entities.exp.request.preRequest.expIdsInfo || {},
  browseId: state.entities.exp.request.preRequest.expRequest.requestId || '',
  requestList: state.entities.exp.request.preRequest.expRequestList,
  userSetting: state.userSetting,
  advSearchCondition: state.ui.expenses.list.advSearch,
  employeeOptions: state.entities.expenses.advSearch.employeeList,
  proxyEmployeeInfo: state.common.proxyEmployeeInfo,
});

const mapDispatchToProps = (dispatch: Dispatch<*>) =>
  bindActionCreators(
    {
      fetchAllIdsForPreApproval,
      fetchExpListForPreApproval,
      browseDetailForPreApproval,
      taxList: taxTypeActions.search,
      onClickInputValueStatus: statusActions.set,
      onClickInputValueEmployee: employeeActions.set,
      fetchEmployeeList: employeeListActions.list,
      setSubmitDate: requestDateActions.set,
      clearStatus: statusActions.clear,
      clearEmployee: employeeActions.clear,
      clearSubmitDate: requestDateActions.clear,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickPagerLink: (pageNum: number) => {
    dispatchProps.fetchExpListForPreApproval(
      stateProps.expIdsInfo.requestIdList,
      pageNum
    );
  },
  onClickAdvSearchButton: () => {
    const {
      proxyEmployeeInfo: { id, isProxyMode, expPreApprovalRequestCount },
    } = stateProps;
    const empId = isProxyMode ? id : undefined;
    const isEmpty = isProxyMode && expPreApprovalRequestCount === 0;
    dispatchProps.fetchAllIdsForPreApproval(
      stateProps.advSearchCondition,
      empId,
      isEmpty
    );
  },
  onClickRefreshButton: () => {
    const {
      proxyEmployeeInfo: { id, isProxyMode, expPreApprovalRequestCount },
    } = stateProps;
    const empId = isProxyMode ? id : undefined;
    const isEmpty = isProxyMode && expPreApprovalRequestCount === 0;

    // To search using same condition for last search
    dispatchProps.fetchAllIdsForPreApproval(
      stateProps.advSearchCondition,
      empId,
      isEmpty
    );
    dispatchProps.taxList(moment().format('YYYY-MM-DD'));
  },
  onClickInputValueSubmitDate: (
    date: DateRangeOption,
    needUpdateList: boolean
  ) => {
    dispatchProps.setSubmitDate(date);
    if (needUpdateList) {
      dispatchProps.fetchEmployeeList(
        stateProps.userSetting.companyId,
        date.startDate
      );
    }
  },
  browseDetail: (reportId: string) => {
    dispatchProps.browseDetailForPreApproval(reportId);
    dispatchProps.taxList(moment().format('YYYY-MM-DD'));
  },
  clearSearchCondition: () => {
    dispatchProps.clearStatus();
    dispatchProps.clearEmployee();
    dispatchProps.clearSubmitDate();
  },
  fetchInitialEmployeeList: () => {
    const dateRange = submitDateInitVal();
    dispatchProps.fetchEmployeeList(
      stateProps.userSetting.companyId,
      dateRange.startDate
    );
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(List): React.ComponentType<Object>);
