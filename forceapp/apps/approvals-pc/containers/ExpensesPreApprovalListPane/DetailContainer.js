// @flow
import { connect } from 'react-redux';

import { actions as commentActions } from '../../modules/ui/expenses/detail/comment';
import { actions as openNowListActions } from '../../modules/ui/expenses/detail/recordsArea/openNowList';
import { actions as selectedRecordActions } from '../../modules/ui/expenses/detail/recordsArea/selectedRecord';
import {
  approvalForPreApproval,
  rejectForPreApproval,
  getFilePreview,
  openRecordItemsConfirmDialog,
} from '../../action-dispatchers/Expenses';

import { searchVendorDetail } from '../../../expenses-pc/action-dispatchers/Expenses';

import Detail from '../../components/ExpensesPreApprovalListPane/Detail/index';

const mapStateToProps = (state) => ({
  expRequest: state.entities.exp.request.preRequest.expRequest,
  baseCurrencyCode: state.userSetting.currencyCode,
  baseCurrencySymbol: state.userSetting.currencySymbol,
  baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  userPhotoUrl: state.userSetting.photoUrl,
  comment: state.ui.expenses.detail.comment,
  openNowList: state.ui.expenses.detail.recordsArea.openNowList,
  expTaxTypeList: state.common.expTaxTypeList.list,
  requestList: state.entities.exp.request.preRequest.expRequestList,
  pageNum: state.ui.expenses.list.page,
  expIdsInfo: state.entities.exp.request.preRequest.expIdsInfo,
  proxyEmployeeId: state.common.proxyEmployeeInfo.id,
});

const mapDispatchToProps = {
  onChangeComment: commentActions.set,
  openNowListInitialize: (toggleList: { [string]: boolean }) =>
    openNowListActions.initialize(toggleList),
  onClickRecordOpenButton: (item: { [string]: boolean }) =>
    openNowListActions.set(item),
  openNowListSet: (item: { [string]: boolean }) => openNowListActions.set(item),
  approvalForPreApproval,
  rejectForPreApproval,
  getFilePreview,
  searchVendorDetail,
  openRecordItemsConfirmDialog,
  setSelectedRecord: (recordIdx) => selectedRecordActions.set(recordIdx),
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickApproveButton: () =>
    dispatchProps.approvalForPreApproval(
      [stateProps.expRequest.requestId],
      stateProps.comment,
      stateProps.expIdsInfo.requestIdList,
      stateProps.pageNum,
      stateProps.requestList,
      stateProps.proxyEmployeeId
    ),
  onClickRejectButton: () =>
    dispatchProps.rejectForPreApproval(
      [stateProps.expRequest.requestId],
      stateProps.comment,
      stateProps.expIdsInfo.requestIdList,
      stateProps.pageNum,
      stateProps.requestList,
      stateProps.proxyEmployeeId
    ),
  openNowListInitialize: () => {
    const openNowList = stateProps.expRequest.records.reduce((ret, record) => {
      ret[record.recordId] = false;
      return ret;
    }, {});
    dispatchProps.openNowListInitialize(openNowList);
  },
  onClickAllOpenRecordButton: () => {
    const openNowList = stateProps.expRequest.records.reduce((ret, record) => {
      ret[record.recordId] = true;
      return ret;
    }, {});
    dispatchProps.openNowListInitialize(openNowList);
  },
  onClickAllCloseRecordButton: () => {
    const openNowList = stateProps.expRequest.records.reduce((ret, record) => {
      ret[record.recordId] = false;
      return ret;
    }, {});
    dispatchProps.openNowListInitialize(openNowList);
  },
  onClickRecordOpenButton: (recordId: string) =>
    dispatchProps.openNowListSet({
      [recordId]: !stateProps.openNowList[recordId],
    }),
  getFilePreview: (receiptId: string) => {
    return dispatchProps.getFilePreview(receiptId);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Detail): React.ComponentType<Object>);
