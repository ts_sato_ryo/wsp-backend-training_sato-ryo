// @flow
import { connect } from 'react-redux';
import last from 'lodash/last';
import { openRecordItemsConfirmDialog } from '../../action-dispatchers/Expenses';
import { actions as activeDialogActions } from '../../modules/ui/expenses/dialog/activeDialog';
import RecordItems from '../../../commons/components/exp/Form/Dialog/RecordItems';

const mapStateToProps = (state, ownProps) => {
  const activeDialog = state.ui.expenses.dialog.activeDialog;
  const currentDialog = last(activeDialog);

  return {
    currentDialog,
    expReport: ownProps.isPreApproval
      ? state.entities.exp.request.preRequest.expRequest
      : state.entities.exp.request.report.expRequest,
    recordIdx: state.ui.expenses.detail.recordsArea.selectedRecord,
    baseCurrencyCode: state.userSetting.currencyCode,
    baseCurrencySymbol: state.userSetting.currencySymbol,
    baseCurrencyDecimal: state.userSetting.currencyDecimalPlaces,
  };
};

const mapDispatchToProps = {
  hideDialog: activeDialogActions.hide,
  hideAllDialog: activeDialogActions.hideAll,
  openRecordItemsConfirmDialog,
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  isApproval: true,
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(RecordItems): React.ComponentType<*>): React.ComponentType<Object>);
