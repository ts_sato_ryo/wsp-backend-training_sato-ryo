import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import PaneExpandWrapper from '../../components/PaneExpandWrapper';
import DetailContainer from './DetailContainer';
import ListContainer from './ListContainer';

class TrackingProcessListPaneContainer extends React.Component {
  static propTypes = {
    isExpanded: PropTypes.bool.isRequired,
  };

  render() {
    return (
      <PaneExpandWrapper
        detailExpanded={this.props.isExpanded}
        list={<ListContainer />}
        detail={<DetailContainer />}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isExpanded: state.ui.timeTrack.isExpanded,
  };
};

export default connect(mapStateToProps)(TrackingProcessListPaneContainer);
