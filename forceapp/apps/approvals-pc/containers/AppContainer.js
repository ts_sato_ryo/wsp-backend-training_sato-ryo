import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import App from '../components/App';
import { selectRequestType } from '../action-dispatchers/App';
import { actions as delegateApproverActions } from '../modules/entities/delegateApprover/assignment';
import { actions as switchApproverActions } from '../../../widgets/dialogs/SwitchApporverDialog/modules/entities';
import { actions as proxyEmployeeInfoActions } from '../../commons/modules/proxyEmployeeInfo';
import { actions as uiActionsPopoverMenu } from '../../commons/modules/widgets/PersonalMenuPopover/ui';

const refreshNotification = (stateProps, listSwitchEmployee) => {
  if (['EXPENSES', 'EXP_PRE_APPROVAL'].includes(stateProps.selectedTab)) {
    listSwitchEmployee(stateProps.userSetting.employeeId);
  }
};

const mapStateToProps = (state) => ({
  empInfo: state.common.empInfo,
  requestCounts: state.ui.requestCounts,
  selectedTab: state.ui.tabs.selected,
  language: state.common.userSetting.language,
  userSetting: state.userSetting,
  isShowDADialog: state.common.delegateApprovalDialog.isNewAssignment,
  isProxyMode: state.common.proxyEmployeeInfo.isProxyMode,
  isShowSwitchApproverDialog: state.widgets.SwitchApproverDialog.ui.isVisible,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      selectTab: selectRequestType,
      listDA: delegateApproverActions.list,
      listSwitchEmployee: switchApproverActions.list,
      unsetProxy: proxyEmployeeInfoActions.unset,
      hidePopover: uiActionsPopoverMenu.hide,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  selectTab: (newSelectedTab) => {
    dispatchProps.selectTab(newSelectedTab, stateProps.selectedTab);
    if (!stateProps.isProxyMode) {
      refreshNotification(stateProps, dispatchProps.listSwitchEmployee);
    }
    if (!['EXPENSES', 'EXP_PRE_APPROVAL'].includes(newSelectedTab)) {
      dispatchProps.hidePopover();
    }
  },
  onExitProxyMode: () => {
    dispatchProps.unsetProxy();
    refreshNotification(stateProps, dispatchProps.listSwitchEmployee);
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(App);
