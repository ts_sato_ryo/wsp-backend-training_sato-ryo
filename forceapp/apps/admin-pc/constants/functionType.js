// @flow

export type FunctionType = $ReadOnly<{|
  ATTENDANCE: 'useAttendance',
  PLANNER: 'usePlanner',
  WORK_TIME: 'useWorkTime',
  EXPENSE: 'useExpense',
|}>;

export type FunctionTypeList = { [$Values<FunctionType>]: boolean };

const functionType: FunctionType = {
  ATTENDANCE: 'useAttendance',
  PLANNER: 'usePlanner',
  WORK_TIME: 'useWorkTime',
  EXPENSE: 'useExpense',
};

export default functionType;
