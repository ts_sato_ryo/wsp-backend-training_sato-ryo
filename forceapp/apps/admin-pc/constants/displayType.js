// @flow

export type DisplayType = $ReadOnly<{|
  DISPLAY_BOTH: 1,
  DISPLAY_LIST: 2,
  DISPLAY_DETAIL: 3,
|}>;

const displayType: DisplayType = {
  DISPLAY_BOTH: 1,
  DISPLAY_LIST: 2,
  DISPLAY_DETAIL: 3,
};

export default displayType;
