const fieldValues = {
  bankAccountType: [
    {
      label: 'Exp_Sel_BankChecking',
      value: 'Checking',
    },
    {
      label: 'Exp_Sel_BankSavings',
      value: 'Savings',
    },
  ],
};

export default fieldValues;
