// @flow

import { type ConfigList, type ConfigListMap } from '../../utils/ConfigUtil';

import fieldType from '../fieldType';
import displayType from '../displayType';

const {
  FIELD_HIDDEN,
  FIELD_TEXT,
  FIELD_AUTOSUGGEST_TEXT,
  FIELD_SELECT_WITH_PLACEHOLDER,
  FIELD_CHECKBOX,
  FIELD_VALID_DATE,
  FIELD_RADIO,
} = fieldType;
const { DISPLAY_LIST, DISPLAY_DETAIL } = displayType;

const base: ConfigList = [
  { key: 'id', type: FIELD_HIDDEN },
  { key: 'code', msgkey: 'Admin_Lbl_Code', type: FIELD_TEXT, isRequired: true },
  {
    key: 'name',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_LIST,
  },
  {
    key: 'name_L0',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
    isRequired: true,
  },
  {
    key: 'name_L1',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'name_L2',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  { key: 'companyId', type: FIELD_HIDDEN },
  {
    key: 'jobTypeId',
    msgkey: 'Admin_Lbl_JobType',
    props: 'jobTypeId',
    dependent: 'jobType',
    type: FIELD_SELECT_WITH_PLACEHOLDER,
    isRequired: true,
  },
  {
    key: 'departmentId',
    msgkey: 'Admin_Lbl_DepartmentName',
    type: FIELD_AUTOSUGGEST_TEXT,
    props: 'departmentId',
    dependent: 'department',
    autoSuggest: {
      value: 'id',
      label: 'name',
      buildLabel: (item) => `${item.code} - ${item.name}`,
      suggestionKey: ['id', 'code', 'name'],
    },
    help: 'Admin_Help_AutoSuggest',
  },
  {
    key: 'parentId',
    msgkey: 'Admin_Lbl_ParentJobName',
    type: FIELD_AUTOSUGGEST_TEXT,
    dependent: 'parent',
    props: 'jobId',
    autoSuggest: {
      value: 'id',
      label: 'name',
      buildLabel: (item) => `${item.code} - ${item.name}`,
      suggestionKey: ['id', 'code', 'name'],
    },
    help: 'Admin_Help_AutoSuggest',
  },
  {
    key: 'isDirectCharged',
    msgkey: 'Admin_Lbl_DirectCharged',
    label: 'Admin_Msg_DirectCharged',
    type: FIELD_CHECKBOX,
    display: DISPLAY_DETAIL,
    defaultValue: true,
  },
  {
    key: 'jobOwnerId',
    msgkey: 'Admin_Lbl_JobOwnerName',
    type: FIELD_AUTOSUGGEST_TEXT,
    dependent: 'jobOwner',
    props: 'employeeId',
    autoSuggest: {
      value: 'id',
      label: 'name',
      buildLabel: (item) => `${item.code} - ${item.name}`,
      suggestionKey: ['id', 'code', 'name'],
    },
    help: 'Admin_Help_AutoSuggest',
    display: DISPLAY_DETAIL,
  },
  {
    key: 'isSelectableTimeTrack',
    msgkey: 'Admin_Lbl_IsSelectableTimeTrack',
    type: FIELD_CHECKBOX,
    display: DISPLAY_DETAIL,
    defaultValue: true,
    useFunction: 'useExpense',
  },
  {
    key: 'isSelectableExpense',
    msgkey: 'Admin_Lbl_IsSelectableExpense',
    type: FIELD_CHECKBOX,
    display: DISPLAY_DETAIL,
    defaultValue: true,
    useFunction: 'useExpense',
  },
  {
    key: 'isScopedAssignment',
    msgkey: 'Admin_Lbl_ScopedAssignment',
    type: FIELD_RADIO,
    props: 'scopedAssignment',
    display: DISPLAY_DETAIL,
    isRequired: true,
    defaultValue: 'true',
  },
  {
    key: 'validDateFrom',
    msgkey: 'Admin_Lbl_ValidDate',
    type: FIELD_VALID_DATE,
    display: DISPLAY_DETAIL,
  },
  { key: 'validDateTo', type: FIELD_HIDDEN },
];

const configList: ConfigListMap = { base };

export default configList;
