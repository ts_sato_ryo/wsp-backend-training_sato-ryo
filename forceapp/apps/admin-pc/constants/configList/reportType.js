// @flow

import { type ConfigList, type ConfigListMap } from '../../utils/ConfigUtil';

import fieldType from '../fieldType';
import displayType from '../displayType';
import { getExtendedItemList } from './extendedItemSetting';
import ReportLinkExpGridComponent from '../../components/ExpenseTypeLinkConfig/ReportLinkExpGridComponent';
import ExpTypeLinkConfigComponent from '../../components/ExpenseTypeLinkConfig/ExpTypeLinkConfigComponent';

const {
  FIELD_HIDDEN,
  FIELD_TEXT,
  FIELD_CHECKBOX,
  FIELD_SELECT,
  FIELD_CUSTOM,
} = fieldType;
const { DISPLAY_DETAIL } = displayType;

const base: ConfigList = [
  { key: 'id', type: FIELD_HIDDEN },
  { key: 'companyId', type: FIELD_HIDDEN, isRequired: true },
  {
    key: 'code',
    msgkey: 'Admin_Lbl_Code',
    type: FIELD_TEXT,
    isRequired: true,
  },
  {
    key: 'name_L0',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    isRequired: true,
  },
  {
    key: 'name_L1',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'description_L0',
    msgkey: 'Admin_Lbl_Description',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'description_L1',
    msgkey: 'Admin_Lbl_Description',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'active',
    msgkey: 'Admin_Lbl_Active',
    type: FIELD_CHECKBOX,
    display: DISPLAY_DETAIL,
  },
  {
    key: `costCenterUsedIn`,
    msgkey: `Admin_Lbl_ExpCostCenterUsedIn`,
    props: 'costCenterUsedOption',
    type: FIELD_SELECT,
    display: DISPLAY_DETAIL,
    multiLanguageValue: true,
  },
  {
    key: `costCenterRequiredFor`,
    msgkey: 'Admin_Lbl_ExpCostCenterRequiredFor',
    props: 'costCenterUsedOption',
    type: FIELD_SELECT,
    display: DISPLAY_DETAIL,
    multiLanguageValue: true,
  },
  {
    key: `jobUsedIn`,
    msgkey: `Admin_Lbl_JobUsedIn`,
    props: 'jobUsedOption',
    type: FIELD_SELECT,
    display: DISPLAY_DETAIL,
    multiLanguageValue: true,
  },
  {
    key: `jobRequiredFor`,
    msgkey: 'Admin_Lbl_JobRequiredFor',
    props: 'jobUsedOption',
    type: FIELD_SELECT,
    display: DISPLAY_DETAIL,
    multiLanguageValue: true,
  },
  {
    key: `vendorUsedIn`,
    msgkey: `Admin_Lbl_ExpVendorUsedIn`,
    props: 'vendorUsedOption',
    type: FIELD_SELECT,
    display: DISPLAY_DETAIL,
    multiLanguageValue: true,
  },
  {
    key: `vendorRequiredFor`,
    msgkey: 'Admin_Lbl_ExpVendorRequiredFor',
    props: 'vendorUsedOption',
    type: FIELD_SELECT,
    display: DISPLAY_DETAIL,
    multiLanguageValue: true,
  },
  {
    key: 'useFileAttachment',
    msgkey: 'Admin_Lbl_UseFileAttachment',
    type: FIELD_CHECKBOX,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'expenseConfig',
    class: 'admin-pc-contents-detail-pane__body__item-list__exp_config',
    msgkey: 'Admin_Lbl_ExpenseTypeConfig',
    type: FIELD_CUSTOM,
    useFunction: 'useExpense',
    Component: ExpTypeLinkConfigComponent,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'expTypeIds',
    class:
      'admin-pc-contents-detail-pane__body__item-list__base-item_expense_type_grid',
    msgkey: 'Admin_Lbl_ExpenseTypeConfig',
    type: FIELD_CUSTOM,
    useFunction: 'useExpense',
    Component: ReportLinkExpGridComponent,
    display: DISPLAY_DETAIL,
  },
  getExtendedItemList('Text'),
  getExtendedItemList('Picklist'),
  getExtendedItemList('Date'),
  getExtendedItemList('Lookup'),
];

const configList: ConfigListMap = { base };

export default configList;
