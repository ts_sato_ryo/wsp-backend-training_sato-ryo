// @flow

import { type ConfigList, type ConfigListMap } from '../../utils/ConfigUtil';

import fieldType from '../fieldType';
import fieldSize from '../fieldSize';
import displayType from '../displayType';
import { getExtendedItemList } from './extendedItemSetting';
import AmountOptionListComponent from '../../components/AmountList/AmountOptionListComponent';
import ExpenseTypeGridComponent from '../../components/ExpenseTypeLinkConfig/ExpenseTypeGridComponent';
import ExpTypeLinkConfigComponent from '../../components/ExpenseTypeLinkConfig/ExpTypeLinkConfigComponent';

import { RECEIPT_TYPE } from '../../../domain/models/exp/Record';

const {
  FIELD_HIDDEN,
  FIELD_TEXT,
  FIELD_VALID_DATE,
  FIELD_SELECT,
  FIELD_CUSTOM,
  FIELD_AUTOSUGGEST_TEXT,
} = fieldType;
const { SIZE_SMALL } = fieldSize;
const { DISPLAY_LIST, DISPLAY_DETAIL } = displayType;

const receiptSettingsOptions = [
  {
    msgkey: 'Exp_Sel_ReceiptType_Optional',
    value: RECEIPT_TYPE.Optional,
  },
  {
    msgkey: 'Exp_Sel_ReceiptType_Required',
    value: RECEIPT_TYPE.Required,
  },
  {
    msgkey: 'Exp_Sel_ReceiptType_NotUsed',
    value: RECEIPT_TYPE.NotUsed,
  },
];

const canUseForeignCurrency = (baseValueGetter) =>
  baseValueGetter('recordType') !== 'TransitJorudanJP';

const base: ConfigList = [
  { key: 'id', type: FIELD_HIDDEN },
  { key: 'code', msgkey: 'Admin_Lbl_Code', type: FIELD_TEXT, isRequired: true },
  {
    key: 'name',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_LIST,
  },
  {
    key: 'name_L0',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
    isRequired: true,
  },
  {
    key: 'name_L1',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'name_L2',
    msgkey: 'Admin_Lbl_Name',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'validDateFrom',
    msgkey: 'Admin_Lbl_ValidDate',
    type: FIELD_VALID_DATE,
    display: DISPLAY_DETAIL,
  },
  { key: 'validDateTo', type: FIELD_HIDDEN },
  {
    key: 'description',
    msgkey: 'Admin_Lbl_Description',
    type: FIELD_TEXT,
    display: DISPLAY_LIST,
  },
  {
    key: 'description_L0',
    msgkey: 'Admin_Lbl_Description',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
    help: 'Admin_Help_ExpTypeDescription',
  },
  {
    key: 'description_L1',
    msgkey: 'Admin_Lbl_Description',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'description_L2',
    msgkey: 'Admin_Lbl_Description',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'parentGroupId',
    msgkey: 'Admin_Lbl_ParentExpTypeGroup',
    props: 'expTypeGroupId',
    dependent: 'parentGroup',
    type: FIELD_SELECT,
    action: 'searchparentExpTypeGroup',
    help: 'Admin_Help_ExpTypeParent',
  },
  {
    key: 'order',
    msgkey: 'Admin_Lbl_Order',
    type: FIELD_TEXT,
    size: SIZE_SMALL,
    charType: 'numeric',
    help: 'Admin_Help_ExpTypeOrder',
  },
  {
    key: 'recordType',
    msgkey: 'Exp_Lbl_RecordType',
    props: 'recordType',
    type: FIELD_SELECT,
    multiLanguageValue: true,
    isRequired: true,
    help: 'Admin_Help_ExpTypeRecordType',
  },
  {
    key: 'expenseToExpenseConfig',
    class: 'admin-pc-contents-detail-pane__body__item-list__exp_config',
    msgkey: 'Admin_Lbl_ExpenseTypeConfig',
    type: FIELD_CUSTOM,
    useFunction: 'useExpense',
    Component: ExpTypeLinkConfigComponent,
    condition: (baseValueGetter) =>
      baseValueGetter('recordType') === 'HotelFee',
    display: DISPLAY_DETAIL,
  },
  {
    key: 'expTypeChildIds',
    class:
      'admin-pc-contents-detail-pane__body__item-list__base-item_expense_type_grid',
    msgkey: 'Admin_Lbl_ExpenseTypeConfig',
    type: FIELD_CUSTOM,
    useFunction: 'useExpense',
    Component: ExpenseTypeGridComponent,
    condition: (baseValueGetter) =>
      baseValueGetter('recordType') === 'HotelFee',
    display: DISPLAY_DETAIL,
  },
  {
    key: 'foreignCurrencyUsage',
    msgkey: 'Admin_Lbl_UseForeignCurrency',
    props: 'foreignCurrencyUsage',
    multiLanguageValue: true,
    type: FIELD_SELECT,
    isRequired: true,
    condition: (baseValueGetter) => canUseForeignCurrency(baseValueGetter),
  },
  {
    key: 'fixedForeignCurrencyId',
    msgkey: 'Admin_Lbl_CurrencyCode',
    props: 'currencyId',
    isRequired: true,
    display: DISPLAY_DETAIL,
    type: FIELD_AUTOSUGGEST_TEXT,
    action: 'searchCurrency',
    autoSuggest: {
      value: 'id',
      label: 'name',
      buildLabel: (item) => `${item.isoCurrencyCode} - ${item.name}`,
      suggestionKey: ['id', 'name', 'isoCurrencyCode'],
    },
    help: 'Admin_Help_AutoSuggest',
    condition: (baseValueGetter) =>
      baseValueGetter('foreignCurrencyUsage') === 'FIXED' &&
      canUseForeignCurrency(baseValueGetter),
  },
  {
    key: 'fixedAllowanceSingleAmount',
    msgkey: 'Admin_Lbl_Amount',
    type: FIELD_TEXT,
    charType: 'numeric',
    isRequired: true,
    help: 'Admin_Help_FixedAmountSingle',
    condition: (baseValueGetter) =>
      baseValueGetter('recordType') === 'FixedAllowanceSingle',
  },
  {
    key: 'fixedAllowanceOptionList',
    msgkey: 'Admin_Lbl_AmountList',
    class:
      'admin-pc-contents-detail-pane__body__item-list__base-item_fixed_allowance_option_list',
    type: FIELD_CUSTOM,
    Component: AmountOptionListComponent,
    display: DISPLAY_DETAIL,
    isRequired: true,
    help: 'Admin_Help_FixedAmountMultiple',
    condition: (baseValueGetter) =>
      baseValueGetter('recordType') === 'FixedAllowanceMulti',
  },
  {
    key: 'taxTypeOptional1Id',
    msgkey: 'Admin_Lbl_ExpTaxType1',
    props: 'taxTypeId',
    dependent: 'taxTypeOptional1',
    type: FIELD_SELECT,
    display: DISPLAY_DETAIL,
    condition: (baseValueGetter) =>
      baseValueGetter('useCompanyTaxMaster') !== true,
  },
  {
    key: 'taxType1Id',
    msgkey: 'Admin_Lbl_ExpTaxType1',
    props: 'taxTypeId',
    dependent: 'taxType1',
    type: FIELD_SELECT,
    help: 'Admin_Help_ExpTypeTaxType',
    isRequired: true,
    condition: (baseValueGetter) => {
      return (
        baseValueGetter('foreignCurrencyUsage') === 'NOT_USED' ||
        baseValueGetter('recordType') === 'TransitJorudanJP'
      );
    },
  },
  {
    key: 'taxType2Id',
    msgkey: 'Admin_Lbl_ExpTaxType2',
    props: 'taxTypeId',
    dependent: 'taxType2',
    type: FIELD_SELECT,
    condition: (baseValueGetter) => {
      const recordType = baseValueGetter('recordType');
      const useForeignCurrency =
        baseValueGetter('foreignCurrencyUsage') !== 'NOT_USED';
      return !useForeignCurrency && recordType !== 'TransitJorudanJP';
    },
  },
  {
    key: 'taxType3Id',
    msgkey: 'Admin_Lbl_ExpTaxType3',
    props: 'taxTypeId',
    dependent: 'taxType3',
    type: FIELD_SELECT,
    condition: (baseValueGetter) => {
      const recordType = baseValueGetter('recordType');
      const useForeignCurrency =
        baseValueGetter('foreignCurrencyUsage') !== 'NOT_USED';
      return !useForeignCurrency && recordType !== 'TransitJorudanJP';
    },
  },
  {
    key: 'fileAttachment',
    msgkey: 'Exp_Lbl_ReceiptSetting',
    options: receiptSettingsOptions,
    type: FIELD_SELECT,
    multiLanguageValue: true,
    isRequired: true,
    condition: (baseValueGetter) =>
      baseValueGetter('recordType') !== 'TransitJorudanJP',
  },
  {
    key: 'debitAccountName',
    msgkey: 'Admin_Lbl_DebitAccountName',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'debitAccountCode',
    msgkey: 'Admin_Lbl_DebitAccountCode',
    type: FIELD_TEXT,
    size: SIZE_SMALL,
    display: DISPLAY_DETAIL,
    isRequired: true,
  },
  {
    key: 'debitSubAccountName',
    msgkey: 'Admin_Lbl_DebitSubAccountName',
    type: FIELD_TEXT,
    display: DISPLAY_DETAIL,
  },
  {
    key: 'debitSubAccountCode',
    msgkey: 'Admin_Lbl_DebitSubAccountCode',
    type: FIELD_TEXT,
    size: SIZE_SMALL,
    display: DISPLAY_DETAIL,
  },
  getExtendedItemList('Text'),
  getExtendedItemList('Picklist'),
  getExtendedItemList('Date'),
  getExtendedItemList('Lookup'),
  { key: 'companyId', type: FIELD_HIDDEN },
  { key: 'useCompanyTaxMaster', type: FIELD_HIDDEN },

  {
    key: 'usage',
    type: FIELD_HIDDEN,
    defaultValue: 'Normal',
  },
];

const configList: ConfigListMap = { base };

export default configList;
