// @flow

import { type ConfigList, type ConfigListMap } from '../../utils/ConfigUtil';

import fieldType from '../fieldType';
import fieldSize from '../fieldSize';

const {
  FIELD_HIDDEN,
  FIELD_SELECT,
  FIELD_RADIO,
  FIELD_TEXT,
  FIELD_CHECKBOX,
} = fieldType;

const { SIZE_SMALL } = fieldSize;

const base: ConfigList = [
  { key: 'id', type: FIELD_HIDDEN },
  {
    section: 'MasterCurrencySetting',
    msgkey: 'Admin_Lbl_MasterCurrencySetting',
    isExpandable: true,
    configList: [
      {
        key: 'currencyId',
        msgkey: 'Admin_Lbl_CurrencyCode',
        type: FIELD_SELECT,
        props: 'currencyId',
        size: SIZE_SMALL,
      },
    ],
  },
  {
    section: 'TaxSetting',
    msgkey: 'Admin_Lbl_TaxSetting',
    isExpandable: true,
    configList: [
      {
        key: 'allowTaxAmountChange',
        msgkey: 'Admin_Lbl_AllowTaxAmountChange',
        type: FIELD_CHECKBOX,
        help: 'Admin_Msg_AllowTaxAmountChange',
      },
    ],
  },
  // {
  //   section: 'MasterTaxTypeSetting',
  //   msgkey: 'Admin_Lbl_MasterTaxTypeSetting',
  //   isExpandable: true,
  //   configList: [
  //     {
  //       key: 'useCompanyTaxMaster',
  //       msgkey: 'Admin_Lbl_UseCompanyTaxMaster',
  //       type: FIELD_CHECKBOX,
  //       props: 'useCompanyTaxMaster',
  //     },
  //   ],
  // },
  {
    key: 'useCompanyTaxMaster',
    msgkey: 'Admin_Lbl_UseCompanyTaxMaster',
    type: FIELD_HIDDEN,
    defaultValue: true,
    props: 'useCompanyTaxMaster',
  },
  {
    section: 'JorudanSearchOption',
    msgkey: 'Admin_Lbl_JorudanSearchOption',
    isExpandable: true,
    configList: [
      {
        key: 'jorudanFareType',
        msgkey: 'Admin_Lbl_JorudanFareType',
        type: FIELD_RADIO,
        props: 'jorudanFareType',
      },
      {
        key: 'jorudanAreaPreference',
        msgkey: 'Admin_Lbl_JorudanAreaPreference',
        type: FIELD_SELECT,
        props: 'jorudanAreaPreference',
        multiLanguageValue: true,
        size: SIZE_SMALL,
      },
      {
        key: 'jorudanUseChargedExpress',
        msgkey: 'Admin_Lbl_JorudanUseChargedExpress',
        type: FIELD_RADIO,
        props: 'jorudanUseChargedExpress',
      },
      {
        key: 'jorudanChargedExpressDistance',
        msgkey: 'Admin_Lbl_JorudanChargedExpressDistance',
        type: FIELD_TEXT,
        charType: 'numeric',
        size: SIZE_SMALL,
      },
      {
        key: 'jorudanSeatPreference',
        msgkey: 'Admin_Lbl_JorudanSeatPreference',
        type: FIELD_RADIO,
        props: 'jorudanSeatPreference',
      },
      {
        key: 'jorudanRouteSort',
        msgkey: 'Admin_Lbl_JorudanRouteSort',
        type: FIELD_RADIO,
        props: 'jorudanRouteSort',
      },
      {
        key: 'jorudanHighwayBus',
        msgkey: 'Admin_Lbl_JorudanHighwayBus',
        type: FIELD_RADIO,
        props: 'jorudanHighwayBus',
      },
    ],
  },
];

const configList: ConfigListMap = { base };

export default configList;
