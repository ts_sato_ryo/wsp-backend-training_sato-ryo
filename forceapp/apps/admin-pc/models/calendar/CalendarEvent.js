/* @flow */
export const DAY_TYPE = {
  Workday: 'Workday',
  Holiday: 'Holiday',
  LegalHoliday: 'LegalHoliday',
};

export type DayType = $Keys<typeof DAY_TYPE>;

export type CalendarEvent = {
  id?: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
  recordDate: string,
  calendarId: string,
  dayType: DayType,
  remarks: string,
};

export const create = (calendarId: string): CalendarEvent => ({
  calendarId,
  name: '',
  name_L0: '',
  name_L1: '',
  name_L2: '',
  recordDate: '',
  dayType: DAY_TYPE.Holiday,
  remarks: '',
});
