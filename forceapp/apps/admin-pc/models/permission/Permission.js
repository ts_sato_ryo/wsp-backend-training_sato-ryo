/* @flow */

import { type Permission as DomainPermissionSet } from '../../../domain/models/access-control/Permission';

export type Permission = {
  id: string,
  code: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
  companyId: string,

  ...DomainPermissionSet,
};
