/* @flow */
export type ShortTimeWorkReason = {
  id: string,
  code: string,
  tagType: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
  companyId: string,
};
