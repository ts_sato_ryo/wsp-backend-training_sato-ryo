// @flow
export type LeaveType = {
  id: string,
  name: string,
};
