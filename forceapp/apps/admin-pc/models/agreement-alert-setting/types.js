// @flow
export type AgreementAlertSetting = {
  id: string,
  code: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
  companyId: string,
  validDateFrom: string,
  validDateTo: string,
  monthlyAgreementHourWarning1: number,
  monthlyAgreementHourWarning2: number,
  monthlyAgreementHourLimit: number,
  monthlyAgreementHourWarningSpecial1: number,
  monthlyAgreementHourWarningSpecial2: number,
  monthlyAgreementHourLimitSpecial: number,
};
