/* @flow */
export type GrantHistoryRecord = {
  id: string,
  validDateFrom: ?string,
  validDateTo: ?string,
  daysGranted: number,
  daysLeft: number,
  hoursLeft: number,
  comment: ?string,
};
