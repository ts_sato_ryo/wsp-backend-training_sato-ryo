/* @flow */
export type Job = {
  id: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
  code: string,
  companyId: string,
  jobTypeId?: string,
  jobType?: {
    name: string,
  },
  departmentId: string,
  department: {
    name: string,
  },
  parentId: string,
  parent: {
    name: string,
  },
  jobOwnerId: string,
  jobOwner: {
    name: string,
  },
  validDateFrom: string,
  validDateTo: string,
  isDirectCharged: boolean,
  isScopedAssignment: string, // UIの都合上 文字列で扱うほうがよかったので文字列にしている。 (APIとしてはBooleanが正しい。)
};
