/* @flow */
export type JobType = {
  id: string,
  name: string,
  name_L0: string,
  name_L1: string,
  name_L2: string,
  code: string,
  companyId: string,
  workCategoryIdList: string[],
};
