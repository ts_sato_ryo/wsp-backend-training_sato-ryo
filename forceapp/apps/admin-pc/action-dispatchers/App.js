// @flow
import type { Permission } from '../../domain/models/access-control/Permission';
import { setUserPermission } from '../../commons/modules/accessControl/permission';

export const initialize = (param: { userPermission: Permission }) => (
  dispatch: Dispatch<any>
) => {
  dispatch(setUserPermission(param.userPermission));
};

export default initialize;
