import '../commons/config/public-path';

/* eslint-disable import/imports-first */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/* eslint-enable */

import configureStore from './store/configureStore';

import Api from './api/vfp';

import * as AppActions from './action-dispatchers/App';
import AdminContainer from './containers/AdminContainer';

import '../commons/styles/base.scss';
import '../commons/config/moment';

const renderApp = (store, Component) => {
  ReactDOM.render(
    <Provider store={store}>
      <Component />
    </Provider>,
    document.getElementById('container')
  );
};

// eslint-disable-next-line import/prefer-default-export
export const startApp = (params) => {
  const configuredStore = configureStore({
    env: {
      api: new Api({}),
    },
  });
  renderApp(configuredStore, AdminContainer);
  configuredStore.dispatch(AppActions.initialize(params));
};
