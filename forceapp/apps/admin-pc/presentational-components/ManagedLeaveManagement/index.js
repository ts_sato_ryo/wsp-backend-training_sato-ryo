/* @flow */
import React from 'react';
import { CSSTransition as ReactCSSTransitionGroup } from 'react-transition-group';

import ListPaneContainer from '../../containers/ManagedLeaveManagementContainer/ListPaneContainer';
import DetailPaneContainer from '../../containers/ManagedLeaveManagementContainer/DetailPaneContainer';
import UpdateDialogContainer from '../../containers/ManagedLeaveManagementContainer/UpdateDialogContainer';

import '../../../commons/styles/modal-transition-slideleft.css';
import './index.scss';

const ROOT = 'admin-pc-managed-leave-management';

export type Props = {
  isDetailVisible: boolean,
};

export default class ManagedLeaveManagement extends React.Component<Props> {
  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__list-pane`}>
          <ListPaneContainer />
        </div>

        <ReactCSSTransitionGroup
          classNames="ts-modal-transition-slideleft"
          timeout={{ enter: 200, exit: 200 }}
        >
          <div>
            {this.props.isDetailVisible ? (
              <div className={`${ROOT}__detail-pane`}>
                <DetailPaneContainer />
              </div>
            ) : null}
          </div>
        </ReactCSSTransitionGroup>

        <UpdateDialogContainer />
      </div>
    );
  }
}
