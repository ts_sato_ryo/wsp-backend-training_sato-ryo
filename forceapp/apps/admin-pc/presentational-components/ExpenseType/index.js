import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configListTemplate from '../../constants/configList/expenseType';

import MainContents from '../../components/MainContents';

export default class ExpenseType extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchExpenseType: PropTypes.array.isRequired,
      companyId: PropTypes.string.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.getConstantsExpenseType();
    this.props.actions.searchExpenseType(param);
    this.props.actions.searchExpTypeGroup(param);
    this.props.actions.searchTaxType(param);
    this.props.actions.searchExtendedItem(param);
    this.props.actions.searchCurrency(param);
  }

  componentWillUpdate(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchExpenseType(param);
      this.props.actions.searchExpTypeGroup(param);
      this.props.actions.searchTaxType(param);
      this.props.actions.searchExtendedItem(param);
    } else if (
      !_.isEmpty(nextProps.tmpEditRecord) &&
      nextProps.tmpEditRecord.validDateFrom !== undefined &&
      this.props.tmpEditRecord.validDateFrom !==
        nextProps.tmpEditRecord.validDateFrom
    ) {
      const param = { companyId: nextProps.companyId };
      if (nextProps.tmpEditRecord.validDateFrom !== '') {
        param.targetDate = nextProps.tmpEditRecord.validDateFrom;
      }
      this.props.actions.searchTaxType(param);
    }
  }

  componentDidUpdate(prevProps) {
    for (let i = 1; i <= 3; i++) {
      const taxTypeId = prevProps.tmpEditRecord[`taxType${i}Id`];

      if (
        taxTypeId &&
        !this.props.sfObjFieldValues.taxTypeId
          .map((e) => e.id)
          .includes(taxTypeId)
      ) {
        // If the tax type list retrieved doesn't have the selected tax type,
        // set first one as well as the screen dose.
        if (this.props.sfObjFieldValues.taxTypeId.length > 0) {
          this.props.commonActions.setTmpEditRecordByKeyValue(
            `taxType${i}Id`,
            this.props.sfObjFieldValues.taxTypeId[0].id
          );
        }
      }
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    const {
      useCompanyTaxMaster,
      currencyField: { code },
    } = this.props.useFunction;
    configList.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      } else if (config.key === 'useCompanyTaxMaster') {
        config.defaultValue = useCompanyTaxMaster;
      } else if (config.key === 'recordType' && code !== 'JPY') {
        config.props = 'recordTypeWithoutJorudan';
      }
    });

    return (
      <MainContents
        componentKey="expenseType"
        configList={configList}
        itemList={this.props.searchExpenseType.map((e) => {
          return {
            ...e,
            useCompanyTaxMaster: this.props.useFunction.useCompanyTaxMaster,
            taxTypeOptional1Id: e.taxType1Id,
            taxTypeOptional1: e.taxType1,
          };
        })}
        {...this.props}
      />
    );
  }
}
