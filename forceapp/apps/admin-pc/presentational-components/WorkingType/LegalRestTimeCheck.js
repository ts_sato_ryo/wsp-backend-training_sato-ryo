/* @flow */
import React from 'react';
import msg from '../../../commons/languages';
import TimeUtil from '../../../commons/utils/TimeUtil';
import PlaceInTemplate from '../../components/PlaceInTemplate';
import TextField from '../../../commons/components/fields/TextField';
import AttTimeField from '../../../commons/components/fields/AttTimeField';
import { parseIntOrStringNull } from '../../../commons/utils/NumberUtil';
import './LegalRestTimeCheck.scss';

const ROOT = 'admin-pc-working-type-legal-rest-time-check';

const parseMinute = (value) => {
  if (value === 0 || value === '0') {
    return 0;
  } else {
    return parseInt(value) || '';
  }
};

type Props = {
  config: {
    key: string,
  },
  childrenKeys: { [string]: string },
  disabled: boolean,
  tmpEditRecord: Object,
  onChangeDetailItem: (string, *) => void,
  historyValueGetter: (string) => boolean,
};

export default class LegalRestTimeCheck extends React.Component<Props> {
  renderCheckbox: () => React$Node;
  renderWorkTime: () => React$Node;
  renderRestTime: () => React$Node;

  renderCheckbox(): React$Node {
    const { config, disabled, tmpEditRecord, onChangeDetailItem } = this.props;
    const { key } = config;

    return (
      <label>
        <input
          type="checkbox"
          disabled={disabled}
          onChange={(e) => onChangeDetailItem(key, !!e.target.checked)}
          checked={tmpEditRecord[key] || false}
        />
        <span className="admin-pc-contents-detail-pane__body__item-list__item__checkbox-message">
          {msg().Admin_Lbl_Use}
        </span>
      </label>
    );
  }

  renderWorkTime(): React$Node {
    const {
      childrenKeys,
      disabled,
      tmpEditRecord,
      onChangeDetailItem,
    } = this.props;
    const key = childrenKeys.workTime;

    return (
      <AttTimeField
        key={key}
        disabled={disabled}
        onBlur={(value) => {
          onChangeDetailItem(key, String(TimeUtil.toMinutes(value)));
        }}
        value={TimeUtil.toHHmm(parseMinute(tmpEditRecord[key]))}
        required
      />
    );
  }

  renderRestTime(): React$Node {
    const {
      childrenKeys,
      disabled,
      tmpEditRecord,
      onChangeDetailItem,
    } = this.props;
    const key = childrenKeys.restTime;

    return (
      <TextField
        key={key}
        className={`${ROOT}__rest-time`}
        type="number"
        min={1}
        max={2880}
        step={1}
        value={parseIntOrStringNull(tmpEditRecord[key])}
        disabled={disabled}
        onChange={(e) =>
          onChangeDetailItem(
            childrenKeys.restTime,
            String(parseIntOrStringNull(e.target.value))
          )
        }
        required
      />
    );
  }

  render() {
    return (
      <div className={`${ROOT}__body`}>
        <div className={`${ROOT}__item-label`}>{this.renderCheckbox()}</div>
        {this.props.historyValueGetter(this.props.config.key) && (
          <div className={`${ROOT}__item-value`}>
            <PlaceInTemplate
              template={msg().Admin_Lbl_IfWorkingForXThenLegalRestTimeIsX}
            >
              {(this.renderWorkTime(): React$Node)}
              {(this.renderRestTime(): React$Node)}
            </PlaceInTemplate>
          </div>
        )}
      </div>
    );
  }
}
