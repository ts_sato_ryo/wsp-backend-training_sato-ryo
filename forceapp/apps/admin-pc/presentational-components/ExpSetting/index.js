import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/expSetting';

import MainContents from '../../components/MainContents';

export default class ExpSetting extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      commonActions: PropTypes.object.isRequired,
      searchExpSetting: PropTypes.array.isRequired,
      companyId: PropTypes.string.isRequired,
    };
  }

  componentDidMount() {
    const idx = _.findIndex(this.props.searchExpSetting, [
      'id',
      this.props.companyId,
    ]);
    this.props.actions.searchCurrency();
    this.props.commonActions.setEditRecord(this.props.searchExpSetting[idx]);
    this.props.actions.getConstantsExpSetting();
  }

  componentWillReceiveProps(nextProps) {
    const idx = _.findIndex(nextProps.searchExpSetting, [
      'id',
      nextProps.companyId,
    ]);

    if (
      !_.isEqual(this.props.companyId, nextProps.companyId) ||
      !_.isEqual(this.props.searchExpSetting, nextProps.searchExpSetting)
    ) {
      nextProps.commonActions.setEditRecord(nextProps.searchExpSetting[idx]);
      this.props.actions.getConstantsExpSetting();
    }
  }

  render() {
    return (
      <MainContents
        mode={this.props.mode}
        componentKey="expSetting"
        configList={configList}
        {...this.props}
        isSinglePane
      />
    );
  }
}
