/* @flow */
import React from 'react';

import ListPaneFrame from '../../components/Common/ListPaneFrame';
import EmployeeSearchForm, {
  type Props as EmployeeSearchFormProps,
} from '../../components/Common/EmployeeSearchForm';
import EmployeeList, {
  type Props as EmployeeListProps,
} from '../../components/Common/EmployeeList';
import msg from '../../../commons/languages';

export type Props = EmployeeSearchFormProps &
  EmployeeListProps & {
    isSearchExecuted: boolean,
  };

export default class ListPane extends React.Component<Props> {
  render() {
    return (
      <ListPaneFrame title={msg().Admin_Lbl_ShortTimeWorkPeriodStatus}>
        <EmployeeSearchForm
          isSearchExecuted={this.props.isSearchExecuted}
          employeeCodeQuery={this.props.employeeCodeQuery}
          employeeNameQuery={this.props.employeeNameQuery}
          departmentNameQuery={this.props.departmentNameQuery}
          workingTypeNameQuery={this.props.workingTypeNameQuery}
          onChangeEmployeeCodeQuery={this.props.onChangeEmployeeCodeQuery}
          onChangeEmployeeNameQuery={this.props.onChangeEmployeeNameQuery}
          onChangeDepartmentNameQuery={this.props.onChangeDepartmentNameQuery}
          onChangeWorkingTypeNameQuery={this.props.onChangeWorkingTypeNameQuery}
          onSubmitSearchForm={this.props.onSubmitSearchForm}
        />
        <EmployeeList
          employees={this.props.employees}
          isSearchExecuted={this.props.isSearchExecuted}
          selectedEmployeeId={this.props.selectedEmployeeId}
          onClickEmployee={this.props.onClickEmployee}
        />
      </ListPaneFrame>
    );
  }
}
