/* @flow */
import React from 'react';

import { type EmployeePersonalInfo } from '../../../models/common/EmployeePersonalInfo';
import {
  type ShortTimeWorkPeriodStatus,
  type EditingShortTimeWorkPeriodStatus,
} from '../../../models/short-time-work/ShortTimeWorkPeriodStatus';
import Button from '../../../../commons/components/buttons/Button';
import DetailPaneFrame from '../../../components/Common/DetailPaneFrame';
import PeriodStatusForm from '../common/PeriodStatusFieldSet';
import HistoryList from './HistoryList';
import msg from '../../../../commons/languages';

import './index.scss';

const ROOT = 'admin-pc-short-time-work-period-status-detail-pane';

export type Props = {
  targetEmployee: ?EmployeePersonalInfo,
  shortTimeWorkPeriodStatusList: ShortTimeWorkPeriodStatus[],
  editingShortTimeWorkPeriodStatus: EditingShortTimeWorkPeriodStatus,
  onUpdateEntryFormValue: (key: string, value: string) => void,
  onSubmitEntryForm: () => void,
  onClickCloseButton: () => void,
  onClickEditHistoryButton: (ShortTimeWorkPeriodStatus) => void,
};

export default class DetailPane extends React.Component<Props> {
  onSubmitEntryForm: (SyntheticEvent<HTMLFormElement>) => void;

  constructor() {
    super();
    this.onSubmitEntryForm = this.onSubmitEntryForm.bind(this);
  }

  onSubmitEntryForm(e: SyntheticEvent<HTMLFormElement>) {
    e.preventDefault();
    this.props.onSubmitEntryForm();
  }

  render() {
    const headerSubTitle = [
      `${msg().Com_Lbl_EmployeeName}: `,
      <span key="employeeName" className={`${ROOT}__employee-name`}>
        {(this.props.targetEmployee || {}).name}
      </span>,
    ];

    return (
      <DetailPaneFrame
        className={ROOT}
        title={msg().Admin_Lbl_Apply}
        subTitle={headerSubTitle}
        headerButtons={
          <Button
            className={`${ROOT}__close-button`}
            onClick={this.props.onClickCloseButton}
          >
            {msg().Com_Btn_Close}
          </Button>
        }
      >
        <form onSubmit={this.onSubmitEntryForm} action="/#">
          <PeriodStatusForm
            parentViewType="Pane"
            editingShortTimeWorkPeriodStatus={
              this.props.editingShortTimeWorkPeriodStatus
            }
            onUpdateValue={this.props.onUpdateEntryFormValue}
          />
          <div className={`${ROOT}__entry-form-footer`}>
            <Button submit type="primary" className={`${ROOT}__entry-button`}>
              {msg().Com_Btn_Execute}
            </Button>
          </div>
        </form>

        <HistoryList
          historyList={this.props.shortTimeWorkPeriodStatusList}
          onClickEditButton={this.props.onClickEditHistoryButton}
        />
      </DetailPaneFrame>
    );
  }
}
