/* @flow */
import React from 'react';

import MainContentFrame from '../../components/Common/MainContentFrame';
import ListPaneContainer from '../../containers/ShortTimeWorkPeriodStatusContainer/ListPaneContainer';
import DetailPaneContainer from '../../containers/ShortTimeWorkPeriodStatusContainer/DetailPaneContainer';
import UpdatePeriodStatusDialogContainer from '../../containers/ShortTimeWorkPeriodStatusContainer/UpdatePeriodStatusDialogContainer';

export type Props = {
  isDetailVisible: boolean,
};

export default class ShortTimeWorkPeriodStatus extends React.Component<Props> {
  render() {
    return (
      <MainContentFrame
        ListPane={<ListPaneContainer />}
        DetailPane={<DetailPaneContainer />}
        Dialogs={[<UpdatePeriodStatusDialogContainer key="updateDialog" />]}
        isDetailVisible={this.props.isDetailVisible}
      />
    );
  }
}
