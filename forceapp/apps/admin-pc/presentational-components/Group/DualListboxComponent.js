import React from 'react';
import DualListBoxContainer from '../../containers/DualListboxContainer';

const DualListbox = (props) => <DualListBoxContainer {...props} />;

export default DualListbox;
