/* @flow */
import React from 'react';
import DualListBox from 'react-dual-listbox';
import msg from '../../../commons/languages';

import btnArrowRight from '../../../commons/images/btnArrowRight.png';
import btnArrowLeft from '../../../commons/images/btnArrowLeft.png';
import btnArrowUp from '../../../commons/images/btnArrowUp.png';
import btnArrowDown from '../../../commons/images/btnArrowDown.png';

import 'react-dual-listbox/lib/react-dual-listbox.css';
import './DualListbox.scss';

const ROOT = 'react-dual-listbox';
export type Props = {
  disabled: boolean,
  groupReportType: {
    options: Array<Object>,
    selected: Array<Object>,
  },
  tmpEditRecord: Object,
  initialiseOptions: () => void,
  setOptions: (selected: Array<string>) => void,
  // admin method
  onChangeDetailItem: (string, *) => void,
};

export default class DualListbox extends React.Component<Props> {
  componentWillMount() {
    this.props.initialiseOptions();
  }

  componentDidUpdate(prevProps: Props) {
    const { tmpEditRecord, disabled } = this.props;
    if (prevProps.tmpEditRecord.id !== tmpEditRecord.id) {
      this.props.initialiseOptions();
    }
    // when cancel current operation, reset to original data
    const isDisabledChanged = prevProps.disabled !== disabled;
    if (disabled && isDisabledChanged) {
      this.props.initialiseOptions();
    }
  }

  handleOptionChange = (selected: Array<string>) => {
    this.props.setOptions(selected);
    this.props.onChangeDetailItem('reportTypeIdList', selected);
  };

  render() {
    const { disabled, groupReportType } = this.props;
    return (
      <React.Fragment>
        <div className={`${ROOT}__dual_listbox_title`}>
          <span className={`${ROOT}__dual_listbox_title_left`}>
            {msg().Admin_Lbl_ReportTypeNotUsed}
          </span>
          <span>{msg().Admin_Lbl_ReportTypeUsed}</span>
        </div>
        <DualListBox
          key={this.props.tmpEditRecord.id + this.props.disabled}
          options={groupReportType.options}
          selected={groupReportType.selected}
          preserveSelectOrder
          showOrderButtons
          disabled={disabled}
          onChange={this.handleOptionChange}
          icons={{
            moveLeft: (
              <span className="fa fa-chevron-left">
                <img src={btnArrowLeft} />
              </span>
            ),
            moveRight: (
              <span className="fa fa-chevron-right">
                <img src={btnArrowRight} />
              </span>
            ),
            moveDown: (
              <span className="fa fa-chevron-down">
                <img src={btnArrowDown} />
              </span>
            ),
            moveUp: (
              <span className="fa fa-chevron-up">
                <img src={btnArrowUp} />
              </span>
            ),
          }}
        />
        {groupReportType.selected.length > 0 && (
          <div className={`${ROOT}__option_appended_text`}>
            {msg().Admin_Lbl_Default}
          </div>
        )}
      </React.Fragment>
    );
  }
}
