import PropTypes from 'prop-types';
import React from 'react';
import { cloneDeep } from 'lodash';

import configList from '../../constants/configList/group';
import MainContents from '../../components/MainContents';

export default class Group extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchGroup: PropTypes.array.isRequired,
    };
  }

  componentDidMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchReportType({ ...param, active: true });
    this.props.actions.search(param);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.search(param);
      this.props.actions.searchReportType({ ...param, active: true });
    }
  }

  render() {
    const configListGroup = cloneDeep(configList);
    configListGroup.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="Group"
        configList={configListGroup}
        itemList={this.props.searchGroup}
        {...this.props}
      />
    );
  }
}
