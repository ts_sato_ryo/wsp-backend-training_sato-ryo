/* @flow */
import React from 'react';

import DataGrid from '../../components/DataGrid';
import isDateIncludedInValidDate from '../../components/DataGrid/filter-logics/isDateIncludedInValidDate';
import Button from '../../../commons/components/buttons/Button';
import DateUtil from '../../../commons/utils/DateUtil';
import msg from '../../../commons/languages';

import './JobAssignment.scss';

const ROOT = 'admin-pc-job-job-assignment';

type Row = { id: string };
type RowSelection = { row: Row };

const extractJobAssignmentIds = (rowSelectionList: RowSelection[]): string[] =>
  rowSelectionList.map((rowSelection) => rowSelection.row.id);

const formatPeriod = ({
  value,
}: {
  value: { from: string, through: string },
}) => {
  return [
    DateUtil.formatYMD(value.from),
    '-',
    DateUtil.formatYMD(value.through),
  ].join('');
};

export type JobAssignmentAsListItem = {
  id: string,
  employeeCode: string,
  employeeName: string,
  departmentCode: string,
  departmentName: string,
  validDate: {
    from: string,
    through: string,
  },
  isSelected: boolean,
};

type Props = {|
  jobAssignmentList: JobAssignmentAsListItem[],
  canOperateAssignment: boolean,
  canExecModifyAssignment: boolean,
  actions: {
    openNewAssignment: () => void,
    openChangeValidPeriod: () => void,
    deleteAssignment: () => void,
    onAssignmentListRowsSelected: (string[]) => void,
    onAssignmentListRowsDeselected: (string[]) => void,
    onAssignmentListRowClick: (string) => void,
    onAssignmentListFilterChange: () => void,
  },
|};

export default class JobAssignment extends React.Component<Props> {
  onRowsSelected: $PropertyType<JobAssignment, 'onRowsSelected'>;
  onRowsDeselected: $PropertyType<JobAssignment, 'onRowsDeselected'>;
  onRowClick: $PropertyType<JobAssignment, 'onRowClick'>;
  onFilterChange: $PropertyType<JobAssignment, 'onFilterChange'>;

  constructor(props: Props) {
    super(props);
    this.onRowsSelected = this.onRowsSelected.bind(this);
    this.onRowsDeselected = this.onRowsDeselected.bind(this);
    this.onRowClick = this.onRowClick.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
  }

  onRowsSelected(rowSelectionList: RowSelection[]) {
    this.props.actions.onAssignmentListRowsSelected(
      extractJobAssignmentIds(rowSelectionList)
    );
  }

  onRowsDeselected(rowSelectionList: RowSelection[]) {
    this.props.actions.onAssignmentListRowsDeselected(
      extractJobAssignmentIds(rowSelectionList)
    );
  }

  onRowClick(rowIndex: number, row: Row) {
    this.props.actions.onAssignmentListRowClick(row.id);
  }

  onFilterChange() {
    this.props.actions.onAssignmentListFilterChange();
  }

  renderActionButtons() {
    return (
      <div className={`${ROOT}__action-buttons`}>
        <Button
          type="secondary"
          disabled={!this.props.canOperateAssignment}
          onClick={this.props.actions.openNewAssignment}
        >
          {msg().Com_Btn_New}
        </Button>

        <div className={`${ROOT}__modify-buttons`}>
          <Button
            type="destructive"
            disabled={
              !this.props.canOperateAssignment ||
              !this.props.canExecModifyAssignment
            }
            onClick={this.props.actions.deleteAssignment}
          >
            {msg().Com_Btn_Delete}
          </Button>

          <Button
            type="default"
            disabled={
              !this.props.canOperateAssignment ||
              !this.props.canExecModifyAssignment
            }
            onClick={this.props.actions.openChangeValidPeriod}
          >
            {msg().Admin_Lbl_ChangePeriod}
          </Button>
        </div>
      </div>
    );
  }

  render() {
    const filterable = this.props.canOperateAssignment;

    return (
      <div className={ROOT}>
        <div className={`${ROOT}__heading`}>
          {msg().Admin_Lbl_ScopedAssignment}
        </div>

        {this.renderActionButtons()}

        <div className={`${ROOT}__list`}>
          <DataGrid
            columns={[
              {
                key: 'employeeName',
                name: msg().Com_Lbl_EmployeeName,
                resizable: true,
                filterable,
              },
              {
                key: 'employeeCode',
                name: msg().Com_Lbl_EmployeeCode,
                resizable: true,
                filterable,
              },
              {
                key: 'departmentName',
                name: msg().Com_Lbl_DepartmentName,
                resizable: true,
                filterable,
              },
              {
                key: 'departmentCode',
                name: msg().Com_Lbl_DepartmentCode,
                resizable: true,
                filterable,
              },
              {
                key: 'validDate',
                name: msg().Admin_Lbl_Period,
                formatter: formatPeriod,
                filterable,
                filterValues: isDateIncludedInValidDate,
                width: 190,
              },
            ]}
            rows={this.props.jobAssignmentList}
            showCheckbox={this.props.canOperateAssignment}
            onRowsSelected={this.onRowsSelected}
            onRowsDeselected={this.onRowsDeselected}
            onRowClick={this.onRowClick}
            onFilterChange={this.onFilterChange}
          />
        </div>
      </div>
    );
  }
}
