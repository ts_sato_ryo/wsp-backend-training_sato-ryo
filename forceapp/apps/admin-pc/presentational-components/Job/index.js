/* @flow */
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/job';
import * as RecordUtil from '../../utils/RecordUtil';
import MainContents from '../../components/MainContents';

import JobAssignment, { type JobAssignmentAsListItem } from './JobAssignment';
import JobAssignmentDialog from '../../containers/JobContainer/JobAssignment/JobAssignmentDialogContainer';
import JobAssignmentTargetEmployeesSearchDialog from '../../containers/JobContainer/JobAssignment/TargetEmployeesSearchDialogContainer';
import ChangePeriodDialog from '../../containers/JobContainer/JobAssignment/ChangePeriodDialogContainer';

import { type Job as JobModel } from '../../models/job/Job';
import {
  type ThunkAction,
  type QueryAction,
  type CommandAction,
  type BaseMasterCRUDActions,
  type AdditionalSearchAction,
} from '../Type';

import './index.scss';

const ROOT = 'admin-pc-job';

type Props = {
  actions: BaseMasterCRUDActions & {
    searchJob: QueryAction,
    create: CommandAction,
    update: CommandAction,

    searchDepartment: AdditionalSearchAction,
    searchJobType: AdditionalSearchAction,

    searchJobAssignmentByJob: (string) => void,
    openNewAssignment: () => void,
    openChangeValidPeriod: () => void,
    deleteAssignment: () => void,
    onAssignmentListRowsSelected: (string[]) => void,
    onAssignmentListRowsDeselected: (string[]) => void,
    onAssignmentListRowClick: (string) => void,
    onAssignmentListFilterChange: () => void,
    getConstantsScopedAssignment: () => void,
  },
  showAssignmentArea: boolean,
  canOperateAssignment: boolean,
  canExecModifyAssignment: boolean,
  isOpeningNewAssignment: boolean,
  isOpeningEmployeeSelection: boolean,
  isOpeningChangePeriod: boolean,
  companyId: string,
  editRecord: JobModel | {},
  searchJob: JobModel[],
  tmpEditRecord: JobModel | {},
  searchCompany: {},
  jobAssignmentList: JobAssignmentAsListItem[],
};

export default class Job extends React.Component<Props> {
  constructor(props: Props) {
    super(props);

    this.onClickCreateButton = this.onClickCreateButton.bind(this);
    this.onClickCreateNewButton = this.onClickCreateNewButton.bind(this);
    this.onClickCancelEditButton = this.onClickCancelEditButton.bind(this);
    this.onClickEditButton = this.onClickEditButton.bind(this);
    this.onClickUpdateButton = this.onClickUpdateButton.bind(this);

    this.renderDetailExtraArea = this.renderDetailExtraArea.bind(this);
  }

  componentDidMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.getConstantsScopedAssignment();
    this.props.actions.searchJob(param);
    this.props.actions.searchJobType(param);
  }

  componentWillReceiveProps(nextProps: Props) {
    const param = {};
    if (this.props.companyId !== nextProps.companyId) {
      param.companyId = nextProps.companyId;
      this.props.actions.searchJob(param);
      this.props.actions.searchJobType(param);
    }

    if (
      !_.isEmpty(nextProps.tmpEditRecord) &&
      nextProps.tmpEditRecord.validDateFrom !== undefined &&
      this.props.tmpEditRecord.validDateFrom !==
        nextProps.tmpEditRecord.validDateFrom
    ) {
      param.companyId = this.props.companyId;
      if (nextProps.tmpEditRecord.validDateFrom !== '') {
        param.targetDate = nextProps.tmpEditRecord.validDateFrom;
      }
      this.props.actions.searchDepartment(param);
    }

    if (
      !_.isEmpty(nextProps.tmpEditRecord) &&
      typeof nextProps.tmpEditRecord.id === 'string'
    ) {
      const nextRecordId = nextProps.tmpEditRecord.id;
      if (nextRecordId !== '' && nextRecordId !== this.props.tmpEditRecord.id) {
        this.props.actions.searchJobAssignmentByJob(nextRecordId);
      }
    }
  }

  onClickCreateButton = (): ThunkAction => {
    const useFunction =
      _.find(this.props.searchCompany, { id: this.props.companyId }) || {};
    return this.props.actions.create(
      RecordUtil.makeForRemote(
        configList.base,
        this.props.editRecord,
        this.props.tmpEditRecord,
        useFunction,
        RecordUtil.getter(this.props.tmpEditRecord),
        RecordUtil.getter({})
      )
    );
  };

  onClickCancelEditButton = (): void => {};

  onClickCreateNewButton = (): void => {
    this.props.actions.searchDepartment({ companyId: this.props.companyId });
    this.props.actions.searchJobType({ companyId: this.props.companyId });
  };

  onClickEditButton = (editRecord: JobModel): void => {
    const param = {};
    param.companyId = this.props.companyId;
    if (editRecord.validDateFrom) {
      param.targetDate = editRecord.validDateFrom;
    }
    this.props.actions.searchDepartment(param);
  };

  onClickUpdateButton = (): ThunkAction => {
    const useFunction =
      _.find(this.props.searchCompany, { id: this.props.companyId }) || {};
    return this.props.actions.update(
      RecordUtil.makeForRemote(
        configList.base,
        this.props.editRecord,
        this.props.tmpEditRecord,
        useFunction,
        RecordUtil.getter(this.props.tmpEditRecord),
        RecordUtil.getter({})
      )
    );
  };

  renderDialog = () => {
    if (this.props.isOpeningEmployeeSelection) {
      return <JobAssignmentTargetEmployeesSearchDialog />;
    } else if (this.props.isOpeningNewAssignment) {
      return <JobAssignmentDialog />;
    } else if (this.props.isOpeningChangePeriod) {
      return <ChangePeriodDialog />;
    } else {
      return null;
    }
  };

  renderDetailExtraArea = () => {
    return (
      <div className={`${ROOT}__extra-area`}>
        {this.props.showAssignmentArea ? (
          <JobAssignment
            actions={this.props.actions}
            jobAssignmentList={this.props.jobAssignmentList}
            canOperateAssignment={this.props.canOperateAssignment}
            canExecModifyAssignment={this.props.canExecModifyAssignment}
          />
        ) : null}
      </div>
    );
  };

  render() {
    const configListJob = _.cloneDeep(configList);
    configListJob.base.forEach((config) => {
      if (config.key && config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <div className="admin-pc-contents__wrapper">
        <MainContents
          componentKey="job"
          itemList={this.props.searchJob}
          configList={configListJob}
          onClickUpdateButton={this.onClickUpdateButton}
          onClickCreateButton={this.onClickCreateButton}
          onClickCancelEditButton={this.onClickCancelEditButton}
          onClickCreateNewButton={this.onClickCreateNewButton}
          onClickEditButton={this.onClickEditButton}
          renderDetailExtraArea={this.renderDetailExtraArea}
          {...this.props}
        />
        {this.renderDialog()}
      </div>
    );
  }
}
