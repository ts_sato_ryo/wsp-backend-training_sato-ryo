/* @flow */
import React from 'react';

import CalendarList from './CalendarList';
import ListPaneHeader from '../../../components/MainContents/ListPaneHeader';
import msg from '../../../../commons/languages';

import { type Calendar } from '../../../models/calendar/Calendar';

import './index.scss';

const ROOT = 'admin-pc-calendar-list-pane';

type Props = {
  calendarList: Calendar[],
  selectedCalendarId: ?string,
  onSelectCalendar: (any) => void,
  onClickCreateNewButton: () => void,
};

export default class ListPane extends React.Component<Props> {
  render() {
    return (
      <div className={ROOT}>
        <ListPaneHeader
          title={msg().Admin_Lbl_Calendar}
          onClickCreateNewButton={this.props.onClickCreateNewButton}
        />

        <div className={`${ROOT}__content`}>
          {this.props.calendarList.length > 0 && (
            <div className={`${ROOT}__company-calendar`}>
              <CalendarList
                calendarList={this.props.calendarList}
                selectedCalendarId={this.props.selectedCalendarId}
                onClickCalendarRow={this.props.onSelectCalendar}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}
