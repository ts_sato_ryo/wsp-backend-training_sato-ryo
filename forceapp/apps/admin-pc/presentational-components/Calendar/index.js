/* @flow */
import React from 'react';

import MainContentFrame from '../../components/Common/MainContentFrame';
import ListPaneContainer from '../../containers/CalendarContainer/ListPaneContainer';
import DetailPaneContainer from '../../containers/CalendarContainer/DetailPaneContainer';
import EventDialogContainer from '../../containers/CalendarContainer/EventDialogContainer';

export type Props = {
  companyId: string,
  onInitialize: () => void,
  isDetailVisible: boolean,
};

export default class Calendar extends React.Component<Props> {
  componentWillMount() {
    this.props.onInitialize();
  }

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.companyId !== nextProps.companyId) {
      nextProps.onInitialize();
    }
  }

  render() {
    return (
      <MainContentFrame
        ListPane={<ListPaneContainer />}
        DetailPane={<DetailPaneContainer />}
        Dialogs={[<EventDialogContainer />]}
        isDetailVisible={this.props.isDetailVisible}
      />
    );
  }
}
