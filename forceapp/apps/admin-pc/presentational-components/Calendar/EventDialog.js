/* @flow */
import React from 'react';

import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import Button from '../../../commons/components/buttons/Button';
import DetailItem from '../../components/MainContents/DetailPane/DetailItem';
import msg from '../../../commons/languages';

import { type CalendarEvent } from '../../models/calendar/CalendarEvent';
import configList from '../../constants/configList/calendarRecord';

import './EventDialog.scss';

const ROOT = 'admin-pc-calendar-event-dialog';

export type Props = {
  event: ?CalendarEvent,
  sfObjFieldValues: {},
  getOrganizationSetting: {},
  onChangeDetailItem: (any) => void,
  onSubmit: (any) => void,
  onCancel: (any) => void,
};

export default class EventDialog extends React.Component<Props> {
  onSubmit: () => void;

  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e: Event) {
    e.preventDefault();
    this.props.onSubmit();
  }

  renderFooter() {
    return (
      <DialogFrame.Footer>
        <Button key="cancel" onClick={this.props.onCancel}>
          {msg().Com_Btn_Cancel}
        </Button>
        <Button key="submit" type="primary" submit>
          {msg().Com_Btn_Save}
        </Button>
      </DialogFrame.Footer>
    );
  }

  render() {
    if (!this.props.event) {
      return null;
    }

    return (
      <form onSubmit={this.onSubmit} action="/#">
        <DialogFrame
          className={ROOT}
          title={msg().Att_Lbl_Event}
          footer={this.renderFooter()}
          hide={this.props.onCancel}
        >
          <div className={`${ROOT}__body`}>
            <ul className={`${ROOT}__item-list`}>
              {configList.base.map((config) =>
                config.key ? (
                  <DetailItem
                    key={config.key}
                    config={config}
                    tmpEditRecord={this.props.event}
                    sfObjFieldValues={this.props.sfObjFieldValues}
                    getOrganizationSetting={this.props.getOrganizationSetting}
                    onChangeDetailItem={this.props.onChangeDetailItem}
                  />
                ) : null
              )}
            </ul>
          </div>
        </DialogFrame>
      </form>
    );
  }
}
