import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/timeSetting';

import MainContents from '../../components/MainContents';

export default class Department extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      searchTimeSetting: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchTimeSetting(param);
    this.props.actions.getConstantsTimeSetting();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchTimeSetting(param);
    }
  }

  render() {
    const configListTimeSetting = _.cloneDeep(configList);
    configListTimeSetting.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="TimeSetting"
        configList={configListTimeSetting}
        itemList={this.props.searchTimeSetting}
        {...this.props}
      />
    );
  }
}
