import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configListTemplate from '../../constants/configList/leave';

import MainContents from '../../components/MainContents';

export default class Leave extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchLeave: PropTypes.array.isRequired,
      companyId: PropTypes.string.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.getConstantsLeave();
    this.props.actions.searchLeave(param);
  }

  componentWillUpdate(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchLeave(param);
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="leave"
        configList={configList}
        itemList={this.props.searchLeave}
        {...this.props}
      />
    );
  }
}
