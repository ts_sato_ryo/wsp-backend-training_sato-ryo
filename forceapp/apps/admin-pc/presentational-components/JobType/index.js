/* @flow */
import React from 'react';

import _ from 'lodash';

import configListTemplate from '../../constants/configList/jobType';

import MainContents from '../../components/MainContents';

import {
  type BaseMasterCRUDActions,
  type AdditionalSearchAction,
} from '../Type';
import { type JobType as JobTypeModel } from '../../models/job-type/JobType';

type Props = {
  actions: BaseMasterCRUDActions & {
    searchSortedWorkCategory: AdditionalSearchAction,
  },
  itemList: Array<JobTypeModel>,
  companyId: string,
};

export default class JobType extends React.Component<Props> {
  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.search(param);
    this.props.actions.searchSortedWorkCategory(param);
  }

  componentWillUpdate(nextProps: Props) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.search(param);
      this.props.actions.searchSortedWorkCategory(param);
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key && config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });
    return (
      <MainContents
        componentKey="JobType"
        configList={configList}
        itemList={this.props.itemList}
        {...this.props}
      />
    );
  }
}
