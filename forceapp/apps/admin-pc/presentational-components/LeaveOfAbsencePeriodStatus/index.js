/* @flow */
import React from 'react';

import MainContentFrame from '../../components/Common/MainContentFrame';
import ListPaneContainer from '../../containers/LeaveOfAbsencePeriodStatusContainer/ListPaneContainer';
import DetailPaneContainer from '../../containers/LeaveOfAbsencePeriodStatusContainer/DetailPaneContainer';
import UpdatePeriodStatusDialogContainer from '../../containers/LeaveOfAbsencePeriodStatusContainer/UpdatePeriodStatusDialogContainer';

export type Props = {
  isDetailVisible: boolean,
};

export default class LeaveOfAbsencePeriodStatus extends React.Component<Props> {
  render() {
    return (
      <MainContentFrame
        ListPane={<ListPaneContainer />}
        DetailPane={<DetailPaneContainer />}
        Dialogs={[
          <UpdatePeriodStatusDialogContainer key="UpdatePeriodStatusDialogContainer" />,
        ]}
        isDetailVisible={this.props.isDetailVisible}
      />
    );
  }
}
