import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/useFunction';

import MainContents from '../../components/MainContents';

export default class UseFunction extends React.Component {
  static get propTypes() {
    return {
      commonActions: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      searchCompany: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    const idx = _.findIndex(this.props.searchCompany, [
      'id',
      this.props.companyId,
    ]);
    this.props.commonActions.setEditRecord(this.props.searchCompany[idx]);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const idx = _.findIndex(this.props.searchCompany, [
        'id',
        nextProps.companyId,
      ]);
      this.props.commonActions.setEditRecord(this.props.searchCompany[idx]);
    }
  }

  render() {
    return (
      <MainContents
        componentKey="useFunction"
        configList={configList}
        isSinglePane
        {...this.props}
      />
    );
  }
}
