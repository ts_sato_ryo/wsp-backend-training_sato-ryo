import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/workCategory';

import MainContents from '../../components/MainContents';

export default class WorkCategory extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      searchWorkCategory: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchWorkCategory(param);
    this.props.actions.searchEmployee(param);
  }

  componentWillUpdate(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchWorkCategory(param);
      this.props.actions.searchEmployee(param);
    }
  }

  render() {
    const configListWorkCategory = _.cloneDeep(configList);
    configListWorkCategory.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="workCategory"
        configList={configListWorkCategory}
        itemList={this.props.searchWorkCategory}
        {...this.props}
      />
    );
  }
}
