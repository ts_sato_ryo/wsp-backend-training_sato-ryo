import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/organization';

import MainContents from '../../components/MainContents';

export default class Organization extends React.Component {
  static get propTypes() {
    return {
      commonActions: PropTypes.object.isRequired,
      getOrganizationSetting: PropTypes.object.isRequired,
      tmpEditRecord: PropTypes.object,
    };
  }

  componentWillMount() {
    this.props.commonActions.setEditRecord(this.props.getOrganizationSetting);
  }

  componentWillReceiveProps(nextProps) {
    if (
      !_.isEqual(
        this.props.getOrganizationSetting,
        nextProps.getOrganizationSetting
      )
    ) {
      this.props.commonActions.setEditRecord(nextProps.getOrganizationSetting);
    }
  }

  render() {
    return (
      <MainContents
        componentKey="organization"
        configList={configList}
        isSinglePane
        {...this.props}
      />
    );
  }
}
