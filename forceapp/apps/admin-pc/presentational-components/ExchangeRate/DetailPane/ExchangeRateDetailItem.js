/* @flow */
import React from 'react';
import uuidV4 from 'uuid/v4';
import _ from 'lodash';

import DetailItem from '../../../components/MainContents/DetailPane/DetailItem';

import { type ExchangeRate } from '../../../models/exchange-rate/ExchangeRate';
import configList from '../../../constants/configList/exchangeRate';
import fieldSize from '../../../constants/fieldSize';
import msg from '../../../../commons/languages';

import Label from '../../../../commons/components/fields/Label';
import TextField from '../../../../commons/components/fields/TextField';

const ROOT = 'admin-pc-contents-detail-pane';
const ITEM_LIST = 'admin-pc-contents-detail-pane__body__item-list';

export type Props = {
  editingRecord: ExchangeRate,
  mode: '' | 'edit' | 'new',
  sfObjFieldValues: {},
  onUpdateDetailItemValue: (any) => void,
  clearCurrencyInfo: (ExchangeRate) => void,
  setCurrencyInfo: (ExchangeRate) => void,
  setCurrencyPair: (ExchangeRate) => boolean,
};

export default class ExchangeRateDetailItem extends React.Component<Props> {
  componentWillReceiveProps(nextProps: Props) {
    const record = nextProps.editingRecord;

    if (record.currencyId === this.props.editingRecord.currencyId) {
      return;
    }

    if (_.isEmpty(record.currencyId)) {
      this.props.clearCurrencyInfo(record);
      return;
    }

    this.props.setCurrencyInfo(record);
    this.props.setCurrencyPair(record);
  }

  renderDetailItem(config: *, isDisable: boolean) {
    const uuid = uuidV4();
    if (config.key === 'currencyPairLabel') {
      return (
        <li key={uuid} className={`${ITEM_LIST}__item`}>
          <Label
            text={msg()[config.msgkey]}
            childCols={config.size || fieldSize.SIZE_MEDIUM}
            labelCols={config.labelSize}
            helpMsg={config.help ? msg()[config.help] : ''}
          >
            <TextField
              key={`${uuid}${config.key}`}
              readOnly
              value={this.props.editingRecord[config.key]}
              required={false}
            />
          </Label>
        </li>
      );
    } else if (config.key === 'baseCurrencyName') {
      return (
        <li key={uuid} className={`${ITEM_LIST}__item`}>
          <Label
            text={msg()[config.msgkey]}
            childCols={config.size || fieldSize.SIZE_MEDIUM}
            labelCols={config.labelSize}
            helpMsg={config.help ? msg()[config.help] : ''}
          >
            <TextField
              key={`${uuid}${config.key}`}
              readOnly
              value={`${this.props.editingRecord.baseCurrencyCode} - ${
                this.props.editingRecord.baseCurrencyName
              }`}
              required={false}
            />
          </Label>
        </li>
      );
    } else {
      return (
        <DetailItem
          key={`${config.key}`}
          config={config}
          tmpEditRecord={this.props.editingRecord}
          sfObjFieldValues={this.props.sfObjFieldValues}
          onChangeDetailItem={this.props.onUpdateDetailItemValue}
          disabled={isDisable}
          baseValueGetter={() => {}}
          historyValueGetter={() => {}}
          useFunction={{}}
        />
      );
    }
  }

  render() {
    const isDetailItemDisabled =
      this.props.mode !== 'edit' && this.props.mode !== 'new';

    return (
      <div className={`${ROOT}__body`}>
        <ul className={`${ROOT}__item-list`}>
          {configList.base.map((config) =>
            this.renderDetailItem(config, isDetailItemDisabled)
          )}
        </ul>
      </div>
    );
  }
}
