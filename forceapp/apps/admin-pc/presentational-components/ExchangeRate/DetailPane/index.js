/* @flow */
import React from 'react';

import { DetailPaneButtonsHeader } from '../../../components/MainContents/DetailPane/DetailPaneHeader';
import ExchangeRateDetailItem from './ExchangeRateDetailItem';
import msg from '../../../../commons/languages';

import { type ExchangeRate } from '../../../models/exchange-rate/ExchangeRate';

const ROOT = 'admin-pc-contents-detail-pane';

export type Props = {
  editingRecord: ExchangeRate,
  mode: '' | 'edit' | 'new',
  sfObjFieldValues: {},
  onClickSaveButton: (any) => void,
  onClickClosePane: (any) => void,
  onClickEditDetailButton: (any) => void,
  onClickCancelEditButton: (any) => void,
  onClickUpdateButton: (any) => void,
  onClickDeleteButton: (SyntheticEvent<any>) => void,
  onUpdateDetailItemValue: (any) => void,
  clearCurrencyInfo: (ExchangeRate) => void,
  setCurrencyInfo: (ExchangeRate) => void,
  setCurrencyPair: (ExchangeRate) => boolean,
};

export default class DetailPane extends React.Component<Props> {
  render() {
    const editingRecord = this.props.editingRecord || {};

    const title = editingRecord.id ? msg().Com_Btn_Edit : msg().Com_Btn_New;

    return (
      <div className={ROOT}>
        <DetailPaneButtonsHeader
          isSinglePane={false}
          isDeleteDisabled={this.props.mode === 'new'}
          type="top"
          id={this.props.mode === 'new' ? '' : editingRecord.id} // note idに空文字列を渡さないと新規作成時の登録ボタンが表示されない。nullでは登録ボタンは表示されない。
          title={title}
          mode={this.props.mode}
          onClickSaveButton={this.props.onClickSaveButton}
          onClickCancelButton={this.props.onClickCancelEditButton}
          onClickEditButton={this.props.onClickEditDetailButton}
          onClickCloseButton={this.props.onClickClosePane}
          onClickUpdateButton={this.props.onClickUpdateButton}
          onClickDeleteButton={this.props.onClickDeleteButton}
        />

        <ExchangeRateDetailItem
          editingRecord={editingRecord}
          sfObjFieldValues={this.props.sfObjFieldValues}
          mode={this.props.mode}
          onUpdateDetailItemValue={this.props.onUpdateDetailItemValue}
          clearCurrencyInfo={this.props.clearCurrencyInfo}
          setCurrencyInfo={this.props.setCurrencyInfo}
          setCurrencyPair={this.props.setCurrencyPair}
        />
      </div>
    );
  }
}
