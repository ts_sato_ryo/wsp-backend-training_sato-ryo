/* @flow */
import React from 'react';

import MainContentFrame from '../../components/Common/MainContentFrame';
import ListPaneContainer from '../../containers/ExchangeRateContainer/ListPaneContainer';
import DetailPaneContainer from '../../containers/ExchangeRateContainer/DetailPaneContainer';

export type Props = {
  company: {},
  companyId: string,
  onInitialize: () => void,
  isDetailVisible: boolean,
  getCurrencyPair: () => void,
};

export default class ExchangeRate extends React.Component<Props> {
  componentWillMount() {
    this.props.onInitialize();
    this.props.getCurrencyPair();
  }

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.companyId !== nextProps.companyId) {
      nextProps.onInitialize();
    }
  }

  render() {
    return (
      <MainContentFrame
        ListPane={<ListPaneContainer company={this.props.company} />}
        DetailPane={<DetailPaneContainer />}
        Dialogs={[]}
        isDetailVisible={this.props.isDetailVisible}
      />
    );
  }
}
