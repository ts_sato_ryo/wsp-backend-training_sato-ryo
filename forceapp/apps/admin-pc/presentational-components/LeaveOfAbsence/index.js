import PropTypes from 'prop-types';
import React from 'react';

import _ from 'lodash';

import configListTemplate from '../../constants/configList/leaveOfAbsence';

import MainContents from '../../components/MainContents';

export default class LeaveOfAbsence extends React.Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    itemList: PropTypes.array.isRequired,
    companyId: PropTypes.string.isRequired,
  };

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.search(param);
  }

  componentWillUpdate(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.search(param);
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });
    return (
      <MainContents
        componentKey="LeaveOfAbsence"
        configList={configList}
        itemList={this.props.itemList}
        {...this.props}
      />
    );
  }
}
