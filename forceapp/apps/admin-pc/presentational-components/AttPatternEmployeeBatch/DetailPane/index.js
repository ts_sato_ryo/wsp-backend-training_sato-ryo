// @flow

import * as React from 'react';

import msg from '../../../../commons/languages';
import Button from '../../../../commons/components/buttons/Button';
import DetailPaneFrame from '../../../components/Common/DetailPaneFrame';

import NewContainer from '../../../containers/AttPatternEmployeeBatchContainer/NewContainer';
import DownloadContainer from '../../../containers/AttPatternEmployeeBatchContainer/DownloadContainer';

const ROOT = 'admin-pc-att-pattern-employee-batch-detail-pane';

export type Props = $ReadOnly<{
  isNew: boolean,
  onClickClose: () => void,
}>;

export default class extends React.Component<Props> {
  render() {
    return (
      <DetailPaneFrame
        className={ROOT}
        title={msg().Admin_Lbl_Apply}
        subTitle=""
        headerButtons={
          <Button
            className={`${ROOT}__close-button`}
            onClick={this.props.onClickClose}
          >
            {msg().Com_Btn_Close}
          </Button>
        }
      >
        {this.props.isNew ? <NewContainer /> : <DownloadContainer />}
      </DetailPaneFrame>
    );
  }
}
