// @flow

import * as React from 'react';

import msg from '../../../../commons/languages';
import DateUtil from '../../../../commons/utils/DateUtil';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';
import HorizontalLayout from '../../../../commons/components/fields/layouts/HorizontalLayout';
import Button from '../../../../commons/components/buttons/Button';

import './Download.scss';

const ROOT = 'admin-pc-att-pattern-employee-batch-detail-pane-download';

export type Props = $ReadOnly<{
  actedAt: string,
  actor: string,
  departmentName: string,
  comment: string,
  status: string,
  count: number,
  successCount: number,
  failureCount: number,
  isShowLog: boolean,
  onClickDownload: () => void,
}>;

export default class Download extends React.Component<Props> {
  render() {
    return (
      <div className={ROOT}>
        <HorizontalLayout>
          <HorizontalLayout.Label>
            {msg().Admin_Lbl_ExecutedAt}
          </HorizontalLayout.Label>
          <HorizontalLayout.Body>
            {this.props.actedAt
              ? DateUtil.formatYMDhhmm(this.props.actedAt)
              : ''}
          </HorizontalLayout.Body>
        </HorizontalLayout>
        <HorizontalLayout>
          <HorizontalLayout.Label>
            {msg().Admin_Lbl_Status}
          </HorizontalLayout.Label>
          <HorizontalLayout.Body>
            {msg()[`Batch_Lbl_${this.props.status}`]}
          </HorizontalLayout.Body>
        </HorizontalLayout>
        <HorizontalLayout>
          <HorizontalLayout.Label>
            {msg().Admin_Lbl_Actor}
          </HorizontalLayout.Label>
          <HorizontalLayout.Body>{this.props.actor}</HorizontalLayout.Body>
        </HorizontalLayout>
        <HorizontalLayout>
          <HorizontalLayout.Label>
            {msg().Admin_Lbl_DepartmentName}
          </HorizontalLayout.Label>
          <HorizontalLayout.Body>
            {this.props.departmentName}
          </HorizontalLayout.Body>
        </HorizontalLayout>
        <HorizontalLayout>
          <HorizontalLayout.Label>
            {msg().Admin_Lbl_Comment}
          </HorizontalLayout.Label>
          <HorizontalLayout.Body>
            <TextAreaField value={this.props.comment} disabled autosize />
          </HorizontalLayout.Body>
        </HorizontalLayout>
        {this.props.isShowLog && (
          <HorizontalLayout className={`${ROOT}__log-layout`}>
            <HorizontalLayout.Label>
              {msg().Admin_Lbl_Log}
            </HorizontalLayout.Label>
            <HorizontalLayout.Body>
              <div>
                <Button onClick={this.props.onClickDownload}>
                  {msg().Admin_Lbl_Download}
                </Button>
              </div>
              <HorizontalLayout className={`${ROOT}__log-result`}>
                <HorizontalLayout.Label>
                  {msg().Admin_Lbl_CountAppliedNumber}
                </HorizontalLayout.Label>
                <HorizontalLayout.Body>
                  {this.props.count}
                </HorizontalLayout.Body>
              </HorizontalLayout>
              <HorizontalLayout>
                <HorizontalLayout.Label>
                  {msg().Admin_Lbl_CountAppliedSuccessNumber}
                </HorizontalLayout.Label>
                <HorizontalLayout.Body>
                  {this.props.successCount}
                </HorizontalLayout.Body>
              </HorizontalLayout>
              <HorizontalLayout>
                <HorizontalLayout.Label>
                  {msg().Admin_Lbl_CountAppliedErrorNumber}
                </HorizontalLayout.Label>
                <HorizontalLayout.Body>
                  {this.props.failureCount}
                </HorizontalLayout.Body>
              </HorizontalLayout>
            </HorizontalLayout.Body>
          </HorizontalLayout>
        )}
      </div>
    );
  }
}
