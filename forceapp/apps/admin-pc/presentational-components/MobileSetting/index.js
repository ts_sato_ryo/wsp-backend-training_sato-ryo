// @flow
import React from 'react';
import _ from 'lodash';

import DetailPaneContainer from '../../containers/MobileSettingContainer/DetailPaneContainer';

import './index.scss';

const ROOT = 'admin-pc-mobile-setting';

type Props = {
  companyId: string,
  onInitialize: () => void,
};

export default class MobileSetting extends React.Component<Props> {
  componentWillMount() {
    this.props.onInitialize();
  }

  componentWillReceiveProps(nextProps: Props) {
    if (this.props.companyId !== nextProps.companyId) {
      nextProps.onInitialize();
    }
  }

  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__detail-pane`}>
          <DetailPaneContainer />
        </div>
      </div>
    );
  }
}
