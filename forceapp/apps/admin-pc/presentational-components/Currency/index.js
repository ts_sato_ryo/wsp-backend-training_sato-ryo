import PropTypes from 'prop-types';
import React from 'react';

import configList from '../../constants/configList/currency';

import MainContents from '../../components/MainContents';

export default class Currency extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchCurrency: PropTypes.array.isRequired,
      searchIsoCurrencyCode: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    this.props.actions.searchCurrency();
    this.props.actions.searchIsoCurrencyCode();
  }

  render() {
    return (
      <MainContents
        componentKey="currency"
        configList={configList}
        itemList={this.props.searchCurrency}
        {...this.props}
      />
    );
  }
}
