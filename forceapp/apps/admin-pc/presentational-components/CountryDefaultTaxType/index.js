import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/countryDefaultTaxType';

import MainContents from '../../components/MainContents';

export default class CountryDefaultTaxType extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchTaxType: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    this.props.actions.searchTaxType();
    this.props.actions.searchCountry();
  }

  render() {
    return (
      <MainContents
        componentKey="countryDefaultTaxType"
        configList={configList}
        itemList={this.props.searchTaxType}
        {...this.props}
      />
    );
  }
}
