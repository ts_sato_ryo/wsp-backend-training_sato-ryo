import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/reportType';

import MainContents from '../../components/MainContents';

export default class ReportType extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchReportType: PropTypes.array.isRequired,
      companyId: PropTypes.string.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.search(param);
    this.props.actions.searchExtendedItem(param);
    this.props.actions.searchExpenseType(param);
    this.props.actions.getConstantsVendorUsed();
    this.props.actions.getConstantsCostCenterUsed();
    this.props.actions.getConstantsJobUsed();
  }

  componentWillUpdate(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.search(param);
      this.props.actions.searchExtendedItem(param);
    }
  }

  render() {
    const configListReportType = _.cloneDeep(configList);
    configListReportType.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="ReportType"
        configList={configListReportType}
        itemList={this.props.searchReportType}
        {...this.props}
      />
    );
  }
}
