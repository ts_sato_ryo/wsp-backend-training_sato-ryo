import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configListTemplate from '../../constants/configList/accountingPeriod';
import MainContents from '../../components/MainContents';

export default class AccountingPeriod extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchAccountingPeriod: PropTypes.array.isRequired,
      companyId: PropTypes.string.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchAccountingPeriod(param);
  }

  componentWillUpdate(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchAccountingPeriod(param);
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
      if (config.key === 'active') {
        config.defaultValue = false;
      }
    });

    return (
      <MainContents
        componentKey="accountingPeriod"
        configList={configList}
        itemList={this.props.searchAccountingPeriod}
        {...this.props}
      />
    );
  }
}
