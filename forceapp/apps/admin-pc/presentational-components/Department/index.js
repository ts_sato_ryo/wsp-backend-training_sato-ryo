import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/department';

import MainContents from '../../components/MainContents';

export default class Department extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      searchDepartment: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchDepartment(param);
    this.props.actions.searchEmployee(param);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchDepartment(param);
      this.props.actions.searchEmployee(param);
    }
  }

  render() {
    const configListDepartment = _.cloneDeep(configList);
    configListDepartment.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="department"
        configList={configListDepartment}
        itemList={this.props.searchDepartment}
        {...this.props}
      />
    );
  }
}
