// @flow
import * as React from 'react';

type Props = {
  config: {
    key: string,
    isRequired: boolean,
  },
  disabled: boolean,
  tmpEditRecord: Object,
  // false positive
  // eslint-disable-next-line react/no-unused-prop-types
  tmpEditRecordBase: Object,
  onChangeDetailItem: (string, *) => void,
};

type Methods<T> = {
  checkReceiveProps: (T, T) => boolean,
  getDefaultValue: (T) => *,
};

export default <InputProps: Props>(methods: Methods<InputProps>) => (
  Component: React.ComponentType<*>
) => {
  return class extends React.Component<InputProps> {
    checkReceiveProps: $PropertyType<Methods<InputProps>, 'checkReceiveProps'>;
    getDefaultValue: $PropertyType<Methods<InputProps>, 'getDefaultValue'>;

    constructor(props: InputProps) {
      super(props);
      this.checkReceiveProps = methods.checkReceiveProps.bind(this);
      this.getDefaultValue = methods.getDefaultValue.bind(this);
    }

    componentDidMount() {
      this.setDefault(this.props);
    }

    componentWillReceiveProps(nextProps: InputProps) {
      if (this.checkReceiveProps(this.props, nextProps)) {
        this.setDefault(nextProps);
      }
    }

    setDefault(props: InputProps) {
      this.props.onChangeDetailItem(
        props.config.key,
        this.getDefaultValue(props)
      );
    }

    render() {
      return <Component {...this.props} />;
    }
  };
};
