import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configListTemplate from '../../constants/configList/taxType';

import MainContents from '../../components/MainContents';

export default class TaxType extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchTaxType: PropTypes.array.isRequired,
      companyId: PropTypes.string.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchTaxType(param);
  }

  componentWillUpdate(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchTaxType(param);
      this.props.actions.searchExpTypeGroup(param);
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="taxType"
        configList={configList}
        itemList={this.props.searchTaxType}
        {...this.props}
      />
    );
  }
}
