// @flow

import * as React from 'react';
import _ from 'lodash';

import { type Permission } from '../../models/permission/Permission';

import configListTemplate from '../../constants/configList/permission';

import MainContents from '../../components/MainContents';

type Props = $ReadOnly<{|
  companyId: string,
  actions: {
    searchPermission: ({ companyId: string }) => void,
    create: (Permission) => void,
    update: (Permission) => void,
    delete: ({ id: string }) => void,
  },
  searchPermission: Permission[],
|}>;

export default class Employee extends React.Component<Props> {
  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchPermission(param);
  }

  componentWillUpdate(nextProps: Props) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchPermission(param);
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key && config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="permission"
        configList={configList}
        itemList={this.props.searchPermission}
        {...this.props}
      />
    );
  }
}
