import PropTypes from 'prop-types';
import React from 'react';
import isEqual from 'lodash/isEqual';
import configList from '../../constants/configList/customHint';
import MainContents from '../../components/MainContents';

export default class ExpCustomHint extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      commonActions: PropTypes.object.isRequired,
      searchCustomHint: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      tmpEditRecord: PropTypes.object,
    };
  }

  componentDidMount() {
    const param = { companyId: this.props.companyId, moduleType: 'Expense' };
    this.props.actions.searchCustomHint(param);
    this.props.commonActions.setEditRecord({
      ...this.props.searchCustomHint,
      ...param,
    });
  }

  componentWillReceiveProps(nextProps) {
    const { searchCustomHint, tmpEditRecord, commonActions } = this.props;
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId, moduleType: 'Expense' };
      this.props.actions.searchCustomHint(param);
    }
    if (!isEqual(searchCustomHint, nextProps.searchCustomHint)) {
      commonActions.setEditRecord(nextProps.searchCustomHint);
    } else if (tmpEditRecord.companyId !== nextProps.tmpEditRecord.companyId) {
      commonActions.setTmpEditRecord(nextProps.searchCustomHint);
    }
  }

  render() {
    return (
      <MainContents
        mode={this.props.mode}
        componentKey="customHint"
        configList={configList}
        moduleType="Expense"
        {...this.props}
        isSinglePane
      />
    );
  }
}
