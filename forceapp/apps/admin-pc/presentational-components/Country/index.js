import PropTypes from 'prop-types';
import React from 'react';

import configList from '../../constants/configList/country';

import MainContents from '../../components/MainContents';

export default class Country extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchCountry: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    this.props.actions.searchCountry();
  }

  render() {
    return (
      <MainContents
        componentKey="country"
        configList={configList}
        itemList={this.props.searchCountry}
        {...this.props}
      />
    );
  }
}
