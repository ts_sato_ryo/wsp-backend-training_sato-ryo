import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
import msg from '../../../commons/languages';
import configListTemplate from '../../constants/configList/employee';

import MainContents from '../../components/MainContents';
import DelegateApproverContainer from '../../containers/DelegateApprover/DelegateApproverContainer';
import './index.scss';

const ROOT = 'admin-pc-employee';
export default class Employee extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      searchEmployee: PropTypes.array.isRequired,
      companyId: PropTypes.string.isRequired,
      openNewAssignment: PropTypes.func,
      delegateSettings: PropTypes.array,
      isEditMode: PropTypes.bool,
      isNew: PropTypes.bool,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchUser();
    this.props.actions.searchEmployee(param);
    this.props.actions.searchCalendar(param);
    this.props.actions.searchPermission(param);
    this.props.actions.searchEmployeeGroup(param);
  }

  componentWillUpdate(nextProps) {
    if (
      nextProps.editRecord.id &&
      this.props.editRecord.id !== nextProps.editRecord.id
    ) {
      this.props.actions.listDelegatedApprovers(nextProps.editRecord.id);
    }
    if (this.props.companyId !== nextProps.companyId) {
      const param = {
        companyId: nextProps.companyId,
      };
      this.props.actions.searchEmployee(param);
      this.props.actions.searchCalendar(param);
      this.props.actions.searchPermission(param);
      this.props.actions.searchEmployeeGroup(param);
    }
  }

  renderDialog = () => {
    if (this.props.isNewAssignment) {
      return <DelegateApproverContainer />;
    } else {
      return null;
    }
  };

  renderDetailExtraArea = () => {
    let additionalClass = null;
    if (this.props.isNew) {
      additionalClass = `${ROOT}__extra-area-new`;
    } else if (this.props.isEditMode) {
      additionalClass = `${ROOT}__extra-area-edit`;
    }

    return (
      <div className={`${ROOT}__extra-area ${additionalClass}`}>
        <div onClick={this.props.actions.openNewAssignment}>
          <b>
            {msg().Com_Lbl_DelegateApprover}(
            {this.props.delegateSettings.length})
          </b>
        </div>
      </div>
    );
  };

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <div className="admin-pc-contents__wrapper">
        <MainContents
          componentKey="employee"
          configList={configList}
          itemList={this.props.searchEmployee}
          renderDetailExtraArea={this.renderDetailExtraArea}
          {...this.props}
        />
        {this.renderDialog()}
      </div>
    );
  }
}
