import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/vendor';

import MainContents from '../../components/MainContents';

export default class Vendor extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      searchVendor: PropTypes.array.isRequired,
      searchIsoCurrencyCode: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchVendor(param);
    this.props.actions.searchIsoCurrencyCode();
    this.props.actions.getConstantsBankAccountType();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchVendor(param);
    }
  }

  render() {
    const configListVendor = _.cloneDeep(configList);
    configListVendor.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="vendor"
        configList={configListVendor}
        itemList={this.props.searchVendor}
        {...this.props}
      />
    );
  }
}
