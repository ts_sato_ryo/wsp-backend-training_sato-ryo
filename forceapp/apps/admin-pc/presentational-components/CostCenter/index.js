import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configList from '../../constants/configList/costCenter';

import MainContents from '../../components/MainContents';

export default class CostCenter extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      searchCostCenter: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchCostCenter(param);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchCostCenter(param);
    }
  }

  render() {
    const configListCostCenter = _.cloneDeep(configList);
    configListCostCenter.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });

    return (
      <MainContents
        componentKey="costCenter"
        configList={configListCostCenter}
        itemList={this.props.searchCostCenter}
        {...this.props}
      />
    );
  }
}
