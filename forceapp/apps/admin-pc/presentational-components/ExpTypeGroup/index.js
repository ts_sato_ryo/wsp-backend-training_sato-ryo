import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';

import configListTemplate from '../../constants/configList/expTypeGroup';

import MainContents from '../../components/MainContents';

export default class ExpTypeGroup extends React.Component {
  static get propTypes() {
    return {
      actions: PropTypes.object.isRequired,
      companyId: PropTypes.string.isRequired,
      searchExpTypeGroup: PropTypes.array.isRequired,
      searchParentExpTypeGroup: PropTypes.array.isRequired,
    };
  }

  componentWillMount() {
    const param = { companyId: this.props.companyId };
    this.props.actions.searchExpTypeGroup(param);
    this.props.actions.searchParentExpTypeGroup(param);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.companyId !== nextProps.companyId) {
      const param = { companyId: nextProps.companyId };
      this.props.actions.searchExpTypeGroup(param);
      this.props.actions.searchParentExpTypeGroup(param);
    }
  }

  render() {
    const configList = _.cloneDeep(configListTemplate);
    configList.base.forEach((config) => {
      if (config.key === 'companyId') {
        config.defaultValue = this.props.companyId;
      }
    });
    return (
      <MainContents
        componentKey="expTypeGroup"
        configList={configList}
        itemList={this.props.searchExpTypeGroup}
        {...this.props}
      />
    );
  }
}
