// @flow

import React, { useEffect } from 'react';
import _ from 'lodash';

import { type ShortTimeWorkReason as ShortTimeWorkReasonModel } from '../../models/short-time-work-reason/ShortTimeWorkReason';

import configListTemplate from '../../constants/configList/shortTimeWorkReason';

import MainContents from '../../components/MainContents';

type Props = $ReadOnly<{|
  companyId: string,
  actions: {
    search: ({ companyId: string }) => void,
    create: (ShortTimeWorkReasonModel) => void,
    update: (ShortTimeWorkReasonModel) => void,
    delete: ({ id: string }) => void,
  },
  searchShortTimeWorkReason: ShortTimeWorkReasonModel[],
|}>;

const ShortTimeWorkReason = (props: Props) => {
  const { companyId, actions, searchShortTimeWorkReason } = props;

  const configList = _.cloneDeep(configListTemplate);
  configList.base.forEach((config) => {
    if (config.key && config.key === 'companyId') {
      config.defaultValue = companyId;
    }
  });

  useEffect(() => {
    actions.search({ companyId });
  }, [companyId]);

  return (
    <MainContents
      componentKey="ShortTimeWorkReason"
      configList={configList}
      itemList={searchShortTimeWorkReason}
      {...props}
    />
  );
};

export default ShortTimeWorkReason;
