import { combineReducers } from 'redux';

import listPane from './list-pane';
import detailPane from './detail-pane';
import updateDialog from './update-dialog';

export default combineReducers({
  listPane,
  detailPane,
  updateDialog,
});
