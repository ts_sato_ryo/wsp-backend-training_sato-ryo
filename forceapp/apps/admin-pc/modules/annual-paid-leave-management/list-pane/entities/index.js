import { combineReducers } from 'redux';

import employeeList from './employee-list';

export default combineReducers({
  employeeList,
});
