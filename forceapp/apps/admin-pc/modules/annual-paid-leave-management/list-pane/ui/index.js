import { combineReducers } from 'redux';

import searchForm from './search-form';
import employeeList from './employee-list';

export default combineReducers({
  searchForm,
  employeeList,
});
