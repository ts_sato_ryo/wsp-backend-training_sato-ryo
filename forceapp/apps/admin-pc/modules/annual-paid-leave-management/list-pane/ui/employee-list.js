/* @flow */
import { type Reducer, type Dispatch } from 'redux';
import { SELECT_TAB } from '../../../../../commons/actions/tab';
import { CHANGE_COMPANY, SELECT_MENU_ITEM } from '../../../base/menu-pane/ui';
import { fetch as fetchGrantHistoryList } from '../../detail-pane/entities/grant-history-list';
import { CLEAR, SEARCH_EMPLOYEE_SUCCESS } from '../entities/employee-list';

type State = {
  +isSearchExecuted: boolean,
  +selectedEmployeeId: ?string,
};

export const SELECT_EMPLOYEE =
  'MODULES/ANNUAL_PAID_LEAVE_MANAGEMENT/UI/EMPLOYEE_LIST/SELECT_EMPLOYEE';
export const DESELECT_EMPLOYEE =
  'MODULES/ANNUAL_PAID_LEAVE_MANAGEMENT/UI/EMPLOYEE_LIST/DESELECT_EMPLOYEE';

export const selectEmployee = (targetEmployeeId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(fetchGrantHistoryList(targetEmployeeId));
  dispatch({
    type: SELECT_EMPLOYEE,
    payload: targetEmployeeId,
  });
};

export const deselectEmployee = () => ({
  type: DESELECT_EMPLOYEE,
});

export const initialState: State = {
  isSearchExecuted: false,
  selectedEmployeeId: null,
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case SELECT_EMPLOYEE:
      return {
        ...state,
        selectedEmployeeId: (action.payload: string),
      };
    case DESELECT_EMPLOYEE:
      return {
        ...state,
        selectedEmployeeId: null,
      };

    case SEARCH_EMPLOYEE_SUCCESS:
      return {
        ...state,
        isSearchExecuted: true,
        selectedEmployeeId: null,
      };

    case CLEAR:
    case SELECT_MENU_ITEM:
    case CHANGE_COMPANY:
    case SELECT_TAB:
      return initialState;

    default:
      return state;
  }
}: Reducer<State, any>);
