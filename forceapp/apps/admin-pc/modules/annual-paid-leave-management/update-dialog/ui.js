/* @flow */
import { type Dispatch } from 'redux';

import Api from '../../../../commons/api';
import {
  loadingStart,
  catchApiError,
  loadingEnd,
} from '../../../../commons/actions/app';
import { fetch as fetchGrantHistoryList } from '../detail-pane/entities/grant-history-list';

type State = {
  +isVisible: boolean,
  +targetGrantHistoryRecordId: ?string,
  +targetGrantHistoryRecordValidDateFrom: ?string,
  +targetGrantHistoryRecordValidDateTo: ?string,
  +targetGrantHistoryRecordDaysGranted: ?number,
  +newDaysGranted: ?string,
};

export const SHOW =
  'MODULES/ANNUAL_PAID_LEAVE_MANAGEMENT/UPDATE_DIALOG/UI/SHOW';
export const HIDE =
  'MODULES/ANNUAL_PAID_LEAVE_MANAGEMENT/UPDATE_DIALOG/UI/HIDE';

export const UPDATE_NEW_DAYS_GRANTED =
  'MODULES/ANNUAL_PAID_LEAVE_MANAGEMENT/UPDATE_DIALOG/UI/UPDATE_NEW_DAYS_GRANTED';

export const EXECUTE_SUCCESS =
  'MODULES/ANNUAL_PAID_LEAVE_MANAGEMENT/UPDATE_DIALOG/UI/EXECUTE_SUCCESS';

export const show = (
  targetGrantHistoryRecordId: string,
  targetGrantHistoryRecordValidDateFrom: string,
  targetGrantHistoryRecordValidDateTo: string,
  targetGrantHistoryRecordDaysGranted: string
) => ({
  type: SHOW,
  payload: {
    targetGrantHistoryRecordId,
    targetGrantHistoryRecordValidDateFrom,
    targetGrantHistoryRecordValidDateTo,
    targetGrantHistoryRecordDaysGranted,
  },
});

export const hide = () => ({
  type: HIDE,
});

export const updateNewDaysGranted = (newDaysGranted: string) => ({
  type: UPDATE_NEW_DAYS_GRANTED,
  payload: newDaysGranted,
});

const executeSuccess = (targetEmployeeId: string) => (
  dispatch: Dispatch<any>
) => {
  dispatch(fetchGrantHistoryList(targetEmployeeId));
  dispatch({
    type: EXECUTE_SUCCESS,
  });
};

export const execute = (
  targetEmployeeId: string,
  targetGrantHistoryRecordId: string,
  newDaysGranted: string
) => (dispatch: Dispatch<any>) => {
  dispatch(loadingStart());

  return Api.invoke({
    path: '/att/annual-leave/grant/update',
    param: {
      grantId: targetGrantHistoryRecordId,
      daysGranted: newDaysGranted,
    },
  })
    .then(() => dispatch(executeSuccess(targetEmployeeId)))
    .catch(err => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export const initialState: State = {
  isVisible: false,
  targetGrantHistoryRecordId: null,
  targetGrantHistoryRecordValidDateFrom: null,
  targetGrantHistoryRecordValidDateTo: null,
  targetGrantHistoryRecordDaysGranted: null,
  newDaysGranted: null,
};

export default (state: State = initialState, action: any) => {
  switch (action.type) {
    case SHOW:
      return {
        ...state,
        isVisible: true,
        targetGrantHistoryRecordId: (action.payload
          .targetGrantHistoryRecordId: string),
        targetGrantHistoryRecordValidDateFrom: (action.payload
          .targetGrantHistoryRecordValidDateFrom: string),
        targetGrantHistoryRecordValidDateTo: (action.payload
          .targetGrantHistoryRecordValidDateTo: string),
        targetGrantHistoryRecordDaysGranted: (action.payload
          .targetGrantHistoryRecordDaysGranted: number),
        newDaysGranted: String(
          (action.payload.targetGrantHistoryRecordDaysGranted: number)
        ),
      };

    case UPDATE_NEW_DAYS_GRANTED:
      return {
        ...state,
        newDaysGranted: (action.payload: string),
      };

    case HIDE:
    case EXECUTE_SUCCESS:
      return initialState;

    default:
      return state;
  }
};
