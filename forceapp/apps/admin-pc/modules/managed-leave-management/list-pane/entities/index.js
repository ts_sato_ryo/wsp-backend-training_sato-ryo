import { combineReducers } from 'redux';

import leaveTypes from './leave-types';
import employeeList from './employee-list';

export default combineReducers({
  leaveTypes,
  employeeList,
});
