import { createSelector } from 'reselect';

import {
  loadingStart,
  catchApiError,
  loadingEnd,
} from '../../../../../commons/actions/app';
import Api from '../../../../../commons/api';

import { SELECT_TAB } from '../../../../../commons/actions/tab';
import { CHANGE_COMPANY, SELECT_MENU_ITEM } from '../../../base/menu-pane/ui';
import { CHANGE_LEAVE_TYPE } from '../ui/leave-type';

export const SEARCH_EMPLOYEE_SUCCESS =
  'MODULES/MANAGED_LEAVE_MANAGEMENT/ENTITIES/EMPLOYEE_LIST/SEARCH_EMPLOYEE_SUCCESS';
export const CLEAR =
  'MODULES/MANAGED_LEAVE_MANAGEMENT/ENTITIES/EMPLOYEE_LIST/CLEAR';

const convertEmployees = (records) => ({
  allIds: records.map((record) => record.id),
  byId: Object.assign(
    {},
    ...records.map((record) => ({ [record.id]: record }))
  ),
});

export const searchEmployeeSuccess = (body) => ({
  type: SEARCH_EMPLOYEE_SUCCESS,
  payload: convertEmployees(body.records),
});

/**
 * Search employees which match to the specified queries
 *
 * @param {String} companyId the ID of company
 * @param {String} leaveTypeId the ID of leave type
 * @param {String} employeeCodeQuery search query for employee's code
 * @param {String} employeeNameQuery search query for employee's name
 * @param {String} departmentNameQuery search query for the department name of employee's
 * @param {String} workingTypeNameQuery search query for the working type
 * @returns {Promise} Promise object to handle API request
 */
export const searchEmployees = (
  companyId,
  leaveTypeId,
  employeeCodeQuery,
  employeeNameQuery,
  departmentNameQuery,
  workingTypeNameQuery
) => (dispatch) => {
  dispatch(loadingStart());

  return Api.invoke({
    path: '/att/managed-leave/employee/search',
    param: {
      companyId,
      leaveId: leaveTypeId,
      empCode: employeeCodeQuery,
      empName: employeeNameQuery,
      deptName: departmentNameQuery,
      workingTypeName: workingTypeNameQuery,
    },
  })
    .then((res) => dispatch(searchEmployeeSuccess(res)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

/**
 * Clear the state
 */
export const clear = () => ({
  type: CLEAR,
});

// $FlowFixMe
export const employeesSelector = createSelector(
  (state) => state.managedLeaveManagement.listPane.entities.employeeList.allIds,
  (state) => state.managedLeaveManagement.listPane.entities.employeeList.byId,
  (allIds, byId) => allIds.map((id) => byId[id])
);

// $FlowFixMe
export const selectedEmployeeSelector = createSelector(
  (state) =>
    state.managedLeaveManagement.listPane.ui.employeeList.selectedEmployeeId,
  (state) => state.managedLeaveManagement.listPane.entities.employeeList.byId,
  (selectedEmployeeId, byId) => byId[selectedEmployeeId] || null
);

export const initialState = {
  allIds: [],
  byId: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_EMPLOYEE_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };

    case CLEAR:
    case CHANGE_LEAVE_TYPE:
    case SELECT_MENU_ITEM:
    case CHANGE_COMPANY:
    case SELECT_TAB:
      return initialState;

    default:
      return state;
  }
};
