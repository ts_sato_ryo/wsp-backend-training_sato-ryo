import {
  LOADING_END,
  LOADING_START,
} from '../../../../../../commons/actions/app';
import ApiMock from '../../../../../../../__tests__/mocks/ApiMock';
import DispatcherMock from '../../../../../../../__tests__/mocks/DispatcherMock';
import tabType from '../../../../../../commons/constants/tabType';
import { SELECT_TAB } from '../../../../../../commons/actions/tab';
import {
  CHANGE_COMPANY,
  SELECT_MENU_ITEM,
} from '../../../../base/menu-pane/ui';
import { CHANGE_LEAVE_TYPE } from '../../ui/leave-type';
import reducer, {
  CLEAR,
  SEARCH_EMPLOYEE_SUCCESS,
  initialState,
  searchEmployeeSuccess,
  searchEmployees,
} from '../employee-list';

import dummyResponse from '../../../../../../repositories/__tests__/mocks/response/employee-search';

describe('admin-pc/managed-leave-management/list-pane/modules/entities/employee-list', () => {
  describe('action', () => {
    describe('searchEmployees ~ searchEmployeeSuccess', () => {
      ApiMock.setDummyResponse(
        '/att/managed-leave/employee/search',
        {
          companyId: 'a0F6F00000qn3QGUAY',
          leaveId: 'xxxx',
          empCode: '000002',
          empName: 'Suzuki Niko',
          deptName: 'テスト部署',
          workingTypeName: 'TestFix',
        },
        {
          records: [
            {
              workingTypeName: 'Test Fix',
              photoUrl:
                'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T',
              name: 'Suzuki Niko',
              id: 'a0I7F000000OwqTUAS',
              deptName: 'テスト部署',
              code: '000002',
            },
          ],
        }
      );

      const dispatcherMock = new DispatcherMock();

      searchEmployees(
        'a0F6F00000qn3QGUAY',
        'xxxx',
        '000002',
        'Suzuki Niko',
        'テスト部署',
        'TestFix'
      )(dispatcherMock.dispatch);

      test('should show loading spinner', () => {
        expect(dispatcherMock.logged[0]).toEqual({
          type: LOADING_START,
        });
      });

      test('should dispatch action', () => {
        expect(dispatcherMock.logged[1]).toEqual({
          type: SEARCH_EMPLOYEE_SUCCESS,
          payload: {
            allIds: ['a0I7F000000OwqTUAS'],
            byId: {
              a0I7F000000OwqTUAS: {
                workingTypeName: 'Test Fix',
                photoUrl:
                  'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T',
                name: 'Suzuki Niko',
                id: 'a0I7F000000OwqTUAS',
                deptName: 'テスト部署',
                code: '000002',
              },
            },
          },
        });
      });

      test('should hide loading spinner', () => {
        expect(dispatcherMock.logged[2]).toEqual({
          type: LOADING_END,
        });
      });
    });
  });

  describe('reducer', () => {
    describe('SEARCH_EMPLOYEE_SUCCESS', () => {
      const nextState = reducer(
        initialState,
        searchEmployeeSuccess(dummyResponse)
      );

      test('should have employees in next state when some records given', () => {
        expect(nextState.allIds).toEqual([
          'a0I7F000000OwqTUAS',
          'a0I7F000000P6heUAC',
        ]);
        expect(Object.keys(nextState.byId)).toEqual([
          'a0I7F000000OwqTUAS',
          'a0I7F000000P6heUAC',
        ]);
      });

      // Do assertion only to the first employee
      const anEmployee = nextState.byId.a0I7F000000OwqTUAS;

      test('should set the record ID of first employee', () => {
        expect(anEmployee.id).toBe('a0I7F000000OwqTUAS');
      });
      test("should set the first employee's code", () => {
        expect(anEmployee.code).toBe('000002');
      });
      test("should set the first employee's name", () => {
        expect(anEmployee.name).toBe('Suzuki Niko');
      });
      test("should set the URL of first employee's avatar", () => {
        expect(anEmployee.photoUrl).toBe(
          'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T'
        );
      });
      test("should set the name of first employee's department", () => {
        expect(anEmployee.deptName).toBe('テスト部署');
      });
      test('should set the name of work scheme of first employee', () => {
        expect(anEmployee.workingTypeName).toBe('Test Fix');
      });
    });

    describe('CLEAR', () => {
      test('should clear state', () => {
        const nextState = reducer(
          {
            allIds: ['a0I7F000000OwqTUAS'],
            byId: {
              a0I7F000000OwqTUAS: {
                workingTypeName: 'Test Fix',
                photoUrl:
                  'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T',
                name: 'Suzuki Niko',
                id: 'a0I7F000000OwqTUAS',
                deptName: 'テスト部署',
                code: '000002',
              },
            },
          },
          { type: CLEAR }
        );

        expect(nextState).toEqual(initialState);
      });
    });
    describe('CHANGE_LEAVE_TYPE', () => {
      test('should clear state', () => {
        const nextState = reducer(
          {
            allIds: ['a0I7F000000OwqTUAS'],
            byId: {
              a0I7F000000OwqTUAS: {
                workingTypeName: 'Test Fix',
                photoUrl:
                  'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T',
                name: 'Suzuki Niko',
                id: 'a0I7F000000OwqTUAS',
                deptName: 'テスト部署',
                code: '000002',
              },
            },
          },
          {
            type: CHANGE_LEAVE_TYPE,
            payload: 'xxxx',
          }
        );

        expect(nextState).toEqual(initialState);
      });
    });
    describe('SELECT_MENU_ITEM', () => {
      test('should clear state', () => {
        const nextState = reducer(
          {
            allIds: ['a0I7F000000OwqTUAS'],
            byId: {
              a0I7F000000OwqTUAS: {
                workingTypeName: 'Test Fix',
                photoUrl:
                  'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T',
                name: 'Suzuki Niko',
                id: 'a0I7F000000OwqTUAS',
                deptName: 'テスト部署',
                code: '000002',
              },
            },
          },
          {
            type: SELECT_MENU_ITEM,
            payload: 'Organization',
          }
        );

        expect(nextState).toEqual(initialState);
      });
    });
    describe('CHANGE_COMPANY', () => {
      test('should clear state', () => {
        const nextState = reducer(
          {
            allIds: ['a0I7F000000OwqTUAS'],
            byId: {
              a0I7F000000OwqTUAS: {
                workingTypeName: 'Test Fix',
                photoUrl:
                  'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T',
                name: 'Suzuki Niko',
                id: 'a0I7F000000OwqTUAS',
                deptName: 'テスト部署',
                code: '000002',
              },
            },
          },
          {
            type: CHANGE_COMPANY,
            payload: 'a0F6F00000qn3QGUAY',
          }
        );

        expect(nextState).toEqual(initialState);
      });
    });
    describe('SELECT_TAB', () => {
      test('should clear state', () => {
        const nextState = reducer(
          {
            allIds: ['a0I7F000000OwqTUAS'],
            byId: {
              a0I7F000000OwqTUAS: {
                workingTypeName: 'Test Fix',
                photoUrl:
                  'https://c.ap5.content.force.com/profilephoto/7297F000000kJst/T',
                name: 'Suzuki Niko',
                id: 'a0I7F000000OwqTUAS',
                deptName: 'テスト部署',
                code: '000002',
              },
            },
          },
          {
            type: SELECT_TAB,
            payload: tabType.ADMIN_ORGANIZATION_REQUEST,
          }
        );

        expect(nextState).toEqual(initialState);
      });
    });
  });
});
