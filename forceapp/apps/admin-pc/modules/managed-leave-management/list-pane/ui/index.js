import { combineReducers } from 'redux';

import leaveType from './leave-type';
import searchForm from './search-form';
import employeeList from './employee-list';

export default combineReducers({
  leaveType,
  searchForm,
  employeeList,
});
