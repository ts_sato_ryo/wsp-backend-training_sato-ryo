/* @flow */
import { combineReducers } from 'redux';

import grantHistoryList from './grant-history-list';

export default combineReducers<Object, Object>({
  grantHistoryList,
});
