/* @flow */
import { type Reducer, type Dispatch } from 'redux';

import {
  type MobileSetting,
  fetch,
} from '../../models/mobile-setting/MobileSetting';

import { catchApiError, withLoading } from '../../../commons/actions/app';

import { SELECT_TAB } from '../../../commons/actions/tab';
import { CHANGE_COMPANY, SELECT_MENU_ITEM } from '../base/menu-pane/ui';

type State = MobileSetting;

const ACTIONS = {
  SET: 'ADMIN/MOBILE_SETTING/ENTITIES/SET',
  UNSET: 'ADMIN/MOBILE_SETTING/ENTITIES/UNSET',
};

const setMobileSetting = (mobileSetting: MobileSetting) => ({
  type: ACTIONS.SET,
  payload: mobileSetting,
});

export const actions = {
  clear: () => ({
    type: ACTIONS.UNSET,
  }),

  fetch: (companyId: string) => (dispatch: Dispatch<any>) =>
    dispatch(
      withLoading(
        () => fetch({ companyId }),
        (result: MobileSetting) => dispatch(setMobileSetting(result))
      )
    ).catch((err) => dispatch(catchApiError(err, { isContinuable: true }))),
};

const initialState = {
  requireLocationAtMobileStamp: false,
};

export default ((state: State = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;

    case SELECT_MENU_ITEM:
    case CHANGE_COMPANY:
    case SELECT_TAB:
    case ACTIONS.UNSET:
      return initialState;

    default:
      return state;
  }
}: Reducer<State, any>);
