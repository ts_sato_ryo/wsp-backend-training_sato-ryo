// @flow

import { type Dispatch } from 'redux';

import {
  catchApiError,
  withLoading,
  confirm,
} from '../../../../commons/actions/app';
import {
  CHANGE_COMPANY,
  SELECT_MENU_ITEM,
  type ChangeCompanyAction,
  type SelectMenuItemAction,
} from '../../base/menu-pane/ui';
import {
  SELECT_TAB,
  type SelectTabAction,
} from '../../../../commons/actions/tab';
import { searchByJob as searchJobAssignmentByJob } from '../entities/assignmentList';
import JobAssignRepository from '../../../../repositories/JobAssignRepository';
import msg from '../../../../commons/languages';

// State
type State = {|
  selectedIds: string[],
  isOpeningChangePeriod: boolean,
  editingValidPeriod: {|
    from: string,
    through: string,
  |},
|};

// Action

const ACTIONS = {
  SELECT: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/SELECT',
  DESELECT: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/DESELECT',
  TOGGLE: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/TOGGLE',
  CLEAR: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/CLEAR',
  START_VALID_PERIOD_UPDATING:
    'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/START_VALID_PERIOD_UPDATING',
  END_VALID_PERIOD_UPDATING:
    'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/END_VALID_PERIOD_UPDATING',
  UPDATE_VALID_PERIOD: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/UPDATE_VALID_PERIOD',
};

type SelectJobAssignmentsAction = {|
  type: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/SELECT',
  payload: string[],
|};

type DeselectJobAssignmentsAction = {|
  type: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/DESELECT',
  payload: string[],
|};

type ToggleJobAssignmentsAction = {|
  type: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/TOGGLE',
  payload: string,
|};

type ClearSelectedJobAssignmentsAction = {|
  type: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/CLEAR',
|};

type StartValidPeriodUpdatingAction = {|
  type: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/START_VALID_PERIOD_UPDATING',
|};

type EndValidPeriodUpdatingAction = {|
  type: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/END_VALID_PERIOD_UPDATING',
|};

type UpdateValidPeriodOfSelectedJobAssignmentsAction = {|
  type: 'ADMIN/JOB/UI/JOB_ASSIGNMENT_LIST/UPDATE_VALID_PERIOD',
  payload: {|
    key: 'from' | 'through',
    value: string,
  |},
|};

type Action =
  | SelectJobAssignmentsAction
  | DeselectJobAssignmentsAction
  | ToggleJobAssignmentsAction
  | ClearSelectedJobAssignmentsAction
  | StartValidPeriodUpdatingAction
  | EndValidPeriodUpdatingAction
  | UpdateValidPeriodOfSelectedJobAssignmentsAction
  | ChangeCompanyAction
  | SelectMenuItemAction
  | SelectTabAction;

export const actions = {
  select: (selectedIds: string[]): SelectJobAssignmentsAction => ({
    type: ACTIONS.SELECT,
    payload: selectedIds,
  }),

  deselect: (deselectedIds: string[]): DeselectJobAssignmentsAction => ({
    type: ACTIONS.DESELECT,
    payload: deselectedIds,
  }),

  toggle: (id: string): ToggleJobAssignmentsAction => ({
    type: ACTIONS.TOGGLE,
    payload: id,
  }),

  clear: (): ClearSelectedJobAssignmentsAction => ({
    type: ACTIONS.CLEAR,
  }),

  startValidPeriodUpdating: (): StartValidPeriodUpdatingAction => ({
    type: ACTIONS.START_VALID_PERIOD_UPDATING,
  }),

  endValidPeriodUpdating: (): EndValidPeriodUpdatingAction => ({
    type: ACTIONS.END_VALID_PERIOD_UPDATING,
  }),

  updateValidPeriodBoundKey: (key: 'from' | 'through') => (
    value: string
  ): UpdateValidPeriodOfSelectedJobAssignmentsAction => ({
    type: ACTIONS.UPDATE_VALID_PERIOD,
    payload: { key, value },
  }),

  bulkUpdateJobAssignments: (
    param: {
      ids: string[],
      validDateFrom: string,
      validDateThrough: string,
    },
    jobId: string
  ) => (dispatch: Dispatch<any>) => {
    return dispatch(
      withLoading(() =>
        JobAssignRepository.bulkUpdate(param).then(() => {
          dispatch(actions.endValidPeriodUpdating());
          dispatch(actions.clear());
        })
      )
    )
      .then(() => dispatch(searchJobAssignmentByJob(jobId)))
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
  },

  bulkDeleteJobAssignments: (param: { ids: string[] }, jobId: string) => (
    dispatch: Dispatch<any>
  ) => {
    dispatch(confirm(msg().Exp_Msg_ConfirmDelete)).then((yes) => {
      if (!yes) {
        return undefined;
      }

      return dispatch(
        withLoading(() =>
          JobAssignRepository.bulkDelete(param).then(() => {
            dispatch(actions.clear());
          })
        )
      )
        .then(() => dispatch(searchJobAssignmentByJob(jobId)))
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
    });
  },
};

// Reducer
const initialState: State = {
  selectedIds: [],
  isOpeningChangePeriod: false,
  editingValidPeriod: {
    from: '',
    through: '',
  },
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case ACTIONS.SELECT: {
      const { payload } = (action: SelectJobAssignmentsAction);
      return {
        ...state,
        selectedIds: [
          ...state.selectedIds,
          ...payload.filter((newId) => !state.selectedIds.includes(newId)),
        ],
      };
    }

    case ACTIONS.DESELECT: {
      const { payload } = (action: DeselectJobAssignmentsAction);
      return {
        ...state,
        selectedIds: state.selectedIds.filter((id) => !payload.includes(id)),
      };
    }

    case ACTIONS.TOGGLE: {
      const { payload } = (action: ToggleJobAssignmentsAction);
      const { selectedIds } = state;
      return {
        ...state,
        selectedIds: selectedIds.includes(payload)
          ? selectedIds.filter((id) => id !== payload)
          : selectedIds.concat(payload),
      };
    }

    case ACTIONS.START_VALID_PERIOD_UPDATING:
      return {
        ...state,
        isOpeningChangePeriod: true,
        editingValidPeriod: initialState.editingValidPeriod,
      };

    case ACTIONS.END_VALID_PERIOD_UPDATING:
      return {
        ...state,
        isOpeningChangePeriod: false,
        editingValidPeriod: initialState.editingValidPeriod,
      };

    case ACTIONS.UPDATE_VALID_PERIOD: {
      const {
        payload,
      } = (action: UpdateValidPeriodOfSelectedJobAssignmentsAction);
      return {
        ...state,
        editingValidPeriod: {
          ...state.editingValidPeriod,
          [payload.key]: payload.value,
        },
      };
    }

    case SELECT_MENU_ITEM:
    case CHANGE_COMPANY:
    case SELECT_TAB:
    case ACTIONS.CLEAR:
      return initialState;

    default:
      return state;
  }
};
