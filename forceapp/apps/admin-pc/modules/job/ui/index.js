// @flow

import { combineReducers } from 'redux';

import assignment from './assignment';
import assignmentList from './assignmentList';

export default combineReducers<Object, Object>({
  assignment,
  assignmentList,
});
