// @flow

import { type Dispatch } from 'redux';
import differenceBy from 'lodash/differenceBy';

import { withLoading, catchApiError } from '../../../../commons/actions/app';
import { type Employee as DomainEmployee } from '../../../../domain/models/organization/Employee';
import EmployeeRepository, {
  type SearchQuery,
} from '../../../../repositories/EmployeeRepository';
import JobAssignRepository from '../../../../repositories/JobAssignRepository';
import { searchByJob as searchJobAssignmentByJob } from '../entities/assignmentList';

// State

export type Employee = {|
  ...DomainEmployee,
  isSelected: boolean,
|};

type State = {|
  /**
   * 社員検索の結果
   */
  foundEmployees: Employee[],

  /**
   * 未決定のジョブアサインの社員リスト
   */
  candidates: Employee[],

  /**
   * 決定されたジョブアサインの社員リスト
   * アサインが最終的に確定するとサーバーに送信される
   */
  stagedEmployees: Employee[],

  /**
   * 有効開始日
   */
  validDateFrom: string,

  /**
   * 有効終了日
   */
  validDateThrough: string,

  /**
   * ジョブアサインの新規追加が開いてるかどうか
   */
  isOpeningNewAssignment: boolean,

  /**
   * 社員選択が開いてるかどうか
   */
  isOpeningEmployeeSelection: boolean,
|};

// Action

/* foundEmployees */
type AddToFoundEmployees = {|
  type: 'ADD_TO_FOUND_EMPLOYEES',
  payload: Employee[],
|};

type AppendToFoundEmployees = {|
  type: 'APPEND_TO_FOUND_EMPLOYEES',
  payload: Employee[],
|};

type RemoveFromFoundEmployees = {|
  type: 'REMOVE_FROM_FOUND_EMPLOYEES',
  payload: Employee[],
|};

type ClearFoundEmployees = {|
  type: 'CLEAR_FOUND_EMPLOYEES',
|};

type ClearSelection = {|
  type: 'CLEAR_SELECTION',
|};

type ToggleSlection = {|
  type: 'TOGGLE_SELECTION',
  payload: Employee,
|};

/* candidates */
type AddToCandidates = {|
  type: 'ADD_TO_CANDIDATES',
  payload: Employee[],
|};

type RemoveFromCandidates = {|
  type: 'REMOVE_FROM_CANDADATES',
  payload: Employee[],
|};

type ClearCandidates = {|
  type: 'CLEAR_CANDADATES',
|};

/* stargedEmployees */
type AddToStagedEmployees = {|
  type: 'ADD_TO_STAGED_EMPLOYEES',
  payload: Employee[],
|};

type ClearStagedEmployees = {|
  type: 'CLEAR_STAGED_EMPLOYEES',
|};

/* generize state updater */
type Update = {|
  type: 'UPDATE',
  payload: {
    key: string,
    value: any,
  },
|};

type Action =
  | AddToFoundEmployees
  | AppendToFoundEmployees
  | RemoveFromFoundEmployees
  | ClearFoundEmployees
  | ClearSelection
  | ToggleSlection
  | AddToCandidates
  | RemoveFromCandidates
  | ClearCandidates
  | AddToStagedEmployees
  | ClearStagedEmployees
  | Update;

export const addToFoundEmployees = (
  employees: Employee[]
): AddToFoundEmployees => ({
  type: 'ADD_TO_FOUND_EMPLOYEES',
  payload: employees,
});

export const appendToFoundEmployees = (
  employees: Employee[]
): AppendToFoundEmployees => ({
  type: 'APPEND_TO_FOUND_EMPLOYEES',
  payload: employees,
});

export const removeFromFoundEmployees = (
  employees: Employee[]
): RemoveFromFoundEmployees => ({
  type: 'REMOVE_FROM_FOUND_EMPLOYEES',
  payload: employees,
});

export const clearFoundEmployees = (): ClearFoundEmployees => ({
  type: 'CLEAR_FOUND_EMPLOYEES',
});

export const clearSelection = (): ClearSelection => ({
  type: 'CLEAR_SELECTION',
});

export const toggleSelection = (target: Employee): ToggleSlection => ({
  type: 'TOGGLE_SELECTION',
  payload: target,
});

export const addToCandidates = (employees: Employee[]): AddToCandidates => ({
  type: 'ADD_TO_CANDIDATES',
  payload: employees,
});

export const removeFromCandidates = (
  employees: Employee[]
): RemoveFromCandidates => ({
  type: 'REMOVE_FROM_CANDADATES',
  payload: employees,
});

export const clearCandidates = (): ClearCandidates => ({
  type: 'CLEAR_CANDADATES',
});

export const addToStagedEmployees = (
  employees: Employee[]
): AddToStagedEmployees => ({
  type: 'ADD_TO_STAGED_EMPLOYEES',
  payload: employees,
});

export const clearStagedEmployees = (): ClearStagedEmployees => ({
  type: 'CLEAR_STAGED_EMPLOYEES',
});

export const update = (key: string, value: any) => ({
  type: 'UPDATE',
  payload: {
    key,
    value,
  },
});

//

export const clear = () => (dispatch: Dispatch<any>) => {
  dispatch(clearSelection());
  dispatch(clearCandidates());
  dispatch(clearFoundEmployees());
  dispatch(clearStagedEmployees());
  dispatch(update('validDateThrough', ''));
  dispatch(update('validDateFrom', ''));
};

export const assignJobToEmployees = (param: {
  jobId: string,
  validDateFrom: string,
  validDateThrough: string,
  employees: Employee[],
}) => (dispatch: Dispatch<any>) => {
  return dispatch(
    withLoading(() =>
      JobAssignRepository.create({
        jobId: param.jobId,
        employeeIds: param.employees.map((employee) => employee.id),
        validDateFrom: param.validDateFrom,
        validDateThrough: param.validDateThrough,
      }).then(() => {
        dispatch(clear());
        dispatch(update('isOpeningNewAssignment', false));
      })
    )
  )
    .then(() => dispatch(searchJobAssignmentByJob(param.jobId)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
};

export const searchEmployees = (query: SearchQuery) => (
  dispatch: Dispatch<any>
): Promise<any> => {
  return dispatch(
    withLoading(() =>
      EmployeeRepository.search({
        ...query,
      })
        .then((foundEmployees) =>
          foundEmployees.map((employee) => ({
            ...employee,
            isSelected: false,
          }))
        )
        .then((foundEmployees) => {
          dispatch(clearFoundEmployees());
          dispatch(addToFoundEmployees(foundEmployees));
        })
        .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    )
  );
};

export const selectCandidates = (employees: Employee[]) => (
  dispatch: Dispatch<any>
) => {
  const candidates = employees.filter((e) => e.isSelected);

  dispatch(clearSelection());
  dispatch(removeFromFoundEmployees(candidates));
  dispatch(addToCandidates(candidates));
};

export const deleteACandidate = (candidate: Employee) => (
  dispatch: Dispatch<any>
) => {
  dispatch(removeFromCandidates([candidate]));
  dispatch(appendToFoundEmployees([candidate]));
};

export const decideCandidates = (candidates: Employee[]) => (
  dispatch: Dispatch<any>
) => {
  dispatch(addToStagedEmployees(candidates));
  dispatch(clearCandidates());
  dispatch(update('isOpeningEmployeeSelection', false));
};

export const openEmployeeSelection = (stagedEmployees: Employee[]) => (
  dispatch: Dispatch<any>
) => {
  dispatch(clearSelection());
  dispatch(clearCandidates());
  dispatch(clearFoundEmployees());
  dispatch(addToCandidates(stagedEmployees));
  dispatch(update('isOpeningEmployeeSelection', true));
};

export const cancelEmployeeSelection = () => (dispatch: Dispatch<any>) => {
  dispatch(clearSelection());
  dispatch(clearCandidates());
  dispatch(clearFoundEmployees());
  dispatch(update('isOpeningEmployeeSelection', false));
};

export const openNewAssignment = () => (dispatch: Dispatch<any>) => {
  dispatch(update('isOpeningNewAssignment', true));
};

export const cancelNewAssignment = () => (dispatch: Dispatch<any>) => {
  dispatch(cancelEmployeeSelection());
  dispatch(clearStagedEmployees());
  dispatch(update('validDateThrough', ''));
  dispatch(update('validDateFrom', ''));
  dispatch(update('isOpeningNewAssignment', false));
};

// Reducer

const initialState: State = {
  foundEmployees: [],
  candidates: [],
  stagedEmployees: [],
  validDateFrom: '',
  validDateThrough: '',
  isOpeningNewAssignment: false,
  isOpeningEmployeeSelection: false,
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case 'ADD_TO_FOUND_EMPLOYEES': {
      const payload = (action: AddToFoundEmployees).payload;

      return {
        ...state,
        foundEmployees: [...differenceBy(payload, state.candidates, 'id')],
      };
    }
    case 'APPEND_TO_FOUND_EMPLOYEES': {
      const payload = (action: AppendToFoundEmployees).payload;

      return {
        ...state,
        foundEmployees: [...state.foundEmployees, ...payload],
      };
    }
    case 'REMOVE_FROM_FOUND_EMPLOYEES': {
      const payload = (action: RemoveFromFoundEmployees).payload;

      return {
        ...state,
        foundEmployees: [...differenceBy(state.foundEmployees, payload, 'id')],
      };
    }
    case 'CLEAR_FOUND_EMPLOYEES': {
      return {
        ...state,
        foundEmployees: [],
      };
    }
    case 'TOGGLE_SELECTION': {
      const payload = (action: ToggleSlection).payload;
      const clone = [...state.foundEmployees];

      const index = state.foundEmployees.findIndex(
        (employee) => employee.id === payload.id
      );
      clone[index] = {
        ...payload,
        isSelected: !payload.isSelected,
      };

      return {
        ...state,
        foundEmployees: [...clone],
      };
    }
    case 'CLEAR_SELECTION': {
      return {
        ...state,
        foundEmployees: state.foundEmployees.map((e) => ({
          ...e,
          isSelected: false,
        })),
      };
    }
    case 'ADD_TO_CANDIDATES': {
      const payload = (action: AddToCandidates).payload.map((value) => ({
        ...value,
        isSelected: false,
      }));

      return {
        ...state,
        candidates: [...state.candidates, ...payload],
      };
    }
    case 'REMOVE_FROM_CANDADATES': {
      const payload = (action: RemoveFromCandidates).payload;

      return {
        ...state,
        candidates: [...differenceBy(state.candidates, payload, 'id')],
      };
    }
    case 'CLEAR_CANDADATES': {
      return {
        ...state,
        candidates: [],
      };
    }
    case 'ADD_TO_STAGED_EMPLOYEES': {
      const payload = (action: AddToStagedEmployees).payload;

      return {
        ...state,
        stagedEmployees: [...payload],
      };
    }
    case 'CLEAR_STAGED_EMPLOYEES': {
      return {
        ...state,
        stagedEmployees: [],
      };
    }
    case 'UPDATE': {
      const payload = (action: Update).payload;
      return {
        ...state,
        [payload.key]: payload.value,
      };
    }
    default:
      return state;
  }
};
