// @flow

import { combineReducers } from 'redux';

import assignmentList from './assignmentList';

export default combineReducers<Object, Object>({
  assignmentList,
});
