// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';
import ui from './ui';

const reducers = {
  ui,
};

export type State = $State<typeof reducers>;

export default combineReducers<Object, Object>(reducers);
