import { combineReducers } from 'redux';

import shortTimeWorkPeriodStatusList from './shortTimeWorkPeriodStatusList';

export default combineReducers({
  shortTimeWorkPeriodStatusList,
});
