import { combineReducers } from 'redux';

import leaveOfAbsenceList from './leaveOfAbsenceList';
import leaveOfAbsencePeriodStatusList from './leaveOfAbsencePeriodStatusList';

export default combineReducers({
  leaveOfAbsenceList,
  leaveOfAbsencePeriodStatusList,
});
