// @flow
import type { Reducer } from 'redux';
import { cloneDeep } from 'lodash';
import type { AmountOption } from '../../../domain/models/exp/ExpenseType';

type State = Array<Object>;

const initialState = [];

const ACTIONS = {
  ADD_ROW: 'FIXED_ALLOWANCE_LIST/ADD_ROW',
  REMOVE_ROW: 'FIXED_ALLOWANCE_LIST/REMOVE_ROW',
  EDIT_ROW: 'FIXED_ALLOWANCE_LIST/EDIT_ROW',
  RESET: 'FIXED_ALLOWANCE_LIST/RESET',
  VALIDATE: 'FIXED_ALLOWANCE_LIST/VALIDATE',
};

export const actions = {
  add: (rows: Array<AmountOption>) => ({
    type: ACTIONS.ADD_ROW,
    payload: rows,
  }),
  remove: (idx: number, rows: Array<AmountOption>) => ({
    type: ACTIONS.REMOVE_ROW,
    payload: { idx, rows },
  }),
  update: (
    idx: number,
    key: string,
    value: string | number,
    rows: Array<AmountOption>
  ) => ({
    type: ACTIONS.EDIT_ROW,
    payload: { idx, key, value, rows },
  }),
  reset: (rows: Array<AmountOption>) => ({
    type: ACTIONS.RESET,
    payload: rows,
  }),
  validate: (rows: Array<AmountOption>) => ({
    type: ACTIONS.VALIDATE,
    payload: rows,
  }),
};

export default ((state: State = initialState, action: any) => {
  switch (action.type) {
    case ACTIONS.ADD_ROW:
      return [
        ...action.payload,
        {
          id: null,
          label: '',
          label_L0: '',
          label_L1: '',
          label_L2: '',
          allowanceAmount: 0,
          label_L0_error: false,
          allowanceAmount_error: false,
        },
      ];
    case ACTIONS.REMOVE_ROW:
      const rows = [...action.payload.rows];
      const remains = rows.filter(
        (items, index) => index !== action.payload.idx
      );
      return remains;
    case ACTIONS.EDIT_ROW:
      const prevRows = [...action.payload.rows];
      const targetRow = prevRows.find(
        (item, index) => index === action.payload.idx
      );
      targetRow[action.payload.key] = action.payload.value;
      if (
        action.payload.key === 'allowanceAmount' &&
        action.payload.value !== 0
      ) {
        targetRow.allowanceAmount_error = false;
      }
      if (action.payload.key === 'label_L0' && action.payload.length !== 0) {
        targetRow.label_L0_error = false;
      }
      return prevRows;
    case ACTIONS.RESET:
      return cloneDeep(action.payload);
    case ACTIONS.VALIDATE:
      const copyCheck = cloneDeep(action.payload);
      const validateRows = copyCheck.map((item) => {
        if (item.label_L0.length === 0) {
          item.label_L0_error = true;
        }
        if (item.allowanceAmount === 0) {
          item.allowanceAmount_error = true;
        }
        return item;
      });
      return validateRows;
    default:
      return state;
  }
}: Reducer<State, any>);
