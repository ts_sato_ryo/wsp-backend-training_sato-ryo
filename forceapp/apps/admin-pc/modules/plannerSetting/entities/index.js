import { combineReducers } from 'redux';

import plannerSetting from './plannerSetting';

export default combineReducers({
  plannerSetting,
});
