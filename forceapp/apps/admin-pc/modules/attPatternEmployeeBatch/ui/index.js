// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../../../commons/utils/TypeUtil';

import detailPane from './detailPane';

const rootReducer = {
  detailPane,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
