// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';

import entites from './entities';
import ui from './ui';

const rootReducer = {
  entites,
  ui,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
