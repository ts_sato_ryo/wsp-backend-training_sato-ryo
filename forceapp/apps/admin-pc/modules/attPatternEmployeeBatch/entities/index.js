// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../../../commons/utils/TypeUtil';

import batchResultList from './batchResultList';
import employeePatternList from './employeePatternList';

const rootReducer = {
  batchResultList,
  employeePatternList,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
