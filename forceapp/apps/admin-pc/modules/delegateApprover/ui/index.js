// @flow
import { combineReducers } from 'redux';
import assignment from './assignment';

export default combineReducers<Object, Object>({
  assignment,
});
