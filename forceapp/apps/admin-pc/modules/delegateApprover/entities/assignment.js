// @flow

import { type Dispatch } from 'redux';

import { withLoading, catchApiError } from '../../../../commons/actions/app';
import {
  type DelegatedApprover,
  getDelegatedApproverList,
  saveDelegatedApprovers,
} from '../../../models/DelegatedApprover';
import { actions as uiActions } from '../ui/assignment';

// State
type State = DelegatedApprover[];

// Action
const ACTIONS = {
  LIST: 'ADMIN/MODULES/DA/ENTITIES/ASSIGNMENT/LIST',
  SAVE: 'ADMIN/MODULES/DA/ENTITIES/ASSIGNMENT/SAVE',
};

type ListDelegatedApprovers = {|
  type: 'ADMIN/MODULES/DA/ENTITIES/ASSIGNMENT/LIST',
  payload: DelegatedApprover[],
|};

type SaveDelegatedApprovers = {|
  type: 'ADMIN/MODULES/DA/ENTITIES/ASSIGNMENT/SAVE',
|};

type Action = ListDelegatedApprovers | SaveDelegatedApprovers;

const listSuccess = (
  settingList: Array<DelegatedApprover>
): ListDelegatedApprovers => ({
  type: ACTIONS.LIST,
  payload: settingList,
});

const saveSuccess = (): SaveDelegatedApprovers => ({
  type: ACTIONS.SAVE,
});

export const actions = {
  list: (empId: string) => (dispatch: Dispatch<any>) =>
    dispatch(
      withLoading(() =>
        getDelegatedApproverList(empId).then((settingList) => {
          dispatch(listSuccess(settingList));
          dispatch(uiActions.clearExcludedEmployees());
          dispatch(
            uiActions.initializeExcludedEmployees(
              settingList.map((x) => x.delegatedApproverId)
            )
          );
        })
      )
    ).catch((err) => dispatch(catchApiError(err, { isContinuable: true }))),

  save: (empId: string, settings: Array<DelegatedApprover>) => (
    dispatch: Dispatch<any>
  ) =>
    dispatch(
      withLoading(() =>
        saveDelegatedApprovers(empId, settings).then(() => {
          getDelegatedApproverList(empId).then((settingList) => {
            dispatch(listSuccess(settingList));
            dispatch(uiActions.clearExcludedEmployees());
            dispatch(
              uiActions.initializeExcludedEmployees(
                settingList.map((x) => x.delegatedApproverId)
              )
            );
          });
          dispatch(saveSuccess());
        })
      )
    ).catch((err) => dispatch(catchApiError(err, { isContinuable: true }))),
};
// Reducer
const initialState: State = [];

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case ACTIONS.LIST: {
      return action.payload;
    }
    case ACTIONS.SAVE:
    default:
      return state;
  }
};
