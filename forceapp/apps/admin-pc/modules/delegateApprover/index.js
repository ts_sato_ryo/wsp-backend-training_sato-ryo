// @flow

import { combineReducers } from 'redux';

import { type $State } from '../../../commons/utils/TypeUtil';
import ui from './ui';
import entities from './entities';

const reducers = {
  ui,
  entities,
};

export type State = $State<typeof reducers>;

export default combineReducers<Object, Object>(reducers);
