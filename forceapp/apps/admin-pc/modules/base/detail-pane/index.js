// @flow
import { combineReducers } from 'redux';

import ui from './ui';

export default combineReducers<Object, Object>({
  ui,
});
