// @flow
import type { Reducer } from 'redux';

export const MODE = {
  VIEW: '',
  NEW: 'new',
  EDIT: 'edit',
  REVISION: 'revision',
};

export const INITIALIZE = 'MODULES/BASE/DETAIL_PANE/UI/INITIALIZE';
export const SHOW_DETAIL_PANE = 'MODULES/BASE/DETAIL_PANE/UI/SHOW_DETAIL_PANE';
export const SET_MODE_BASE = 'MODULES/BASE/DETAIL_PANE/UI/SET_MODE_BASE';
export const SET_MODE_HISTORY =
  'MODULES/HISTORY/DETAIL_PANE/UI/SET_MODE_HISTORY';

export type State = {
  +isShowDetail: ?boolean,
  +modeBase: ?string,
  +modeHistory: ?string,
};

export const initializeDetailPane = () => ({
  type: INITIALIZE,
});

export const showDetailPane = (show: boolean) => ({
  type: SHOW_DETAIL_PANE,
  payload: show,
});

export const setModeBase = (mode: string) => ({
  type: SET_MODE_BASE,
  payload: mode,
});

export const setModeHistory = (mode: string) => ({
  type: SET_MODE_HISTORY,
  payload: mode,
});

export const initialState: State = {
  isShowDetail: false,
  modeBase: '',
  modeHistory: '',
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case INITIALIZE:
      return initialState;
    case SHOW_DETAIL_PANE:
      return {
        ...state,
        isShowDetail: action.payload,
      };
    case SET_MODE_BASE:
      return {
        ...state,
        modeBase: action.payload,
      };
    case SET_MODE_HISTORY:
      return {
        ...state,
        modeHistory: action.payload,
      };
    default:
      return state;
  }
}: Reducer<State, any>);
