// @flow
import { combineReducers } from 'redux';

import detailPane from './detail-pane';
import menuPane from './menu-pane';

export default combineReducers<Object, Object>({
  detailPane,
  menuPane,
});
