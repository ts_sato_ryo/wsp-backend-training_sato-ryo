/* @flow */
import { type Reducer, type Dispatch } from 'redux';

import Api from '../../../../commons/api';
import {
  catchApiError,
  loadingEnd,
  loadingStart,
} from '../../../../commons/actions/app';
import { SELECT_TAB } from '../../../../commons/actions/tab';
import { CHANGE_COMPANY, SELECT_MENU_ITEM } from '../../base/menu-pane/ui';

import {
  type CalendarEvent,
  create as createCalendarEvent,
} from '../../../models/calendar/CalendarEvent';

type State = ?CalendarEvent;

const initialState: State = null;

const ACTIONS = {
  SET: 'ADMIN/CALENDAR/UI/CALENDAR_EVENT/SET',
  UNSET: 'ADMIN/CALENDAR/UI/CALENDAR_EVENT/UNSET',
  UPDATE: 'ADMIN/CALENDAR/UI/CALENDAR_EVENT/UPDATE',
};

export const actions = {
  select: (calendarEvent: CalendarEvent) => ({
    type: ACTIONS.SET,
    payload: calendarEvent,
  }),

  create: (calendarId: string) => ({
    type: ACTIONS.SET,
    payload: createCalendarEvent(calendarId),
  }),

  unset: () => ({
    type: ACTIONS.UNSET,
  }),

  update: (key: string, value: any) => ({
    type: ACTIONS.UPDATE,
    payload: {
      key,
      value,
    },
  }),

  save: (
    editingCalendarEvent: CalendarEvent,
    onSuccess: () => Promise<any>
  ) => (dispatch: Dispatch<any>) => {
    dispatch(loadingStart());

    const path =
      editingCalendarEvent.id === null || editingCalendarEvent.id === undefined
        ? '/calendar/record/create'
        : '/calendar/record/update';

    return Api.invoke({
      path,
      param: editingCalendarEvent,
    })
      .then(onSuccess)
      .then(() => dispatch(actions.unset()))
      .catch(err => dispatch(catchApiError(err, { isContinuable: true })))
      .then(() => dispatch(loadingEnd()));
  },
};

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return { ...action.payload };

    case ACTIONS.UPDATE:
      return {
        ...state,
        [action.payload.key]: action.payload.value,
      };

    case SELECT_MENU_ITEM:
    case CHANGE_COMPANY:
    case SELECT_TAB:
    case ACTIONS.UNSET:
      return initialState;

    default:
      return state;
  }
}: Reducer<State, any>);
