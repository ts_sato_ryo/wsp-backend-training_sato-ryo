import { combineReducers } from 'redux';

import calendar from './calendar';
import calendarEvent from './calendarEvent';

export default combineReducers({
  calendar,
  calendarEvent,
});
