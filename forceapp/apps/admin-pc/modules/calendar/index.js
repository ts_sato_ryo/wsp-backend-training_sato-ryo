import { combineReducers } from 'redux';

import ui from './ui';
import entities from './entities';

export const GET_CONSTANTS_CALENDAR = '';

export const actions = {
  getConstantsCalendar: () => ({
    type: GET_CONSTANTS_CALENDAR,
  }),
};

export default combineReducers({
  ui,
  entities,
});
