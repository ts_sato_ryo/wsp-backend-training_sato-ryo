import { combineReducers } from 'redux';

import calendarList from './calendarList';
import calendarEventList from './calendarEventList';

export default combineReducers({
  calendarList,
  calendarEventList,
});
