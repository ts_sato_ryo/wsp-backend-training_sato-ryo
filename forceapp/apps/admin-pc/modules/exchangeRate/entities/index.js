import { combineReducers } from 'redux';

import exchangeRateList from './exchangeRateList';

export default combineReducers({
  exchangeRateList,
});
