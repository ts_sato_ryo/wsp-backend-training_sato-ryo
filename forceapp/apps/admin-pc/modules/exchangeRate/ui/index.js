import { combineReducers } from 'redux';

import editingExchangeRate from './editingExchangeRate';

export default combineReducers({
  editingExchangeRate,
});
