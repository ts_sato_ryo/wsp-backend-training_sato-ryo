// @flow
import type { Reducer } from 'redux';

type State = {
  options: Array<string>,
  selected: Array<string>,
};
const initialState = { options: [], selected: [] };

const ACTION = {
  INITIALISE_OPTIONS: 'INITIALISE_OPTIONS',
  SET_OPTIONS: 'SET_OPTIONS',
};

export const initialiseOptions = (
  selectedIds: Array<string>,
  reportType: Array<string>
) => ({
  type: ACTION.INITIALISE_OPTIONS,
  payload: { selectedIds, reportType },
});

export const setOptions = (selectedIds: Array<string>) => ({
  type: ACTION.SET_OPTIONS,
  payload: selectedIds,
});

const orderbyCode = (optionA, optionB) => {
  const labelA = optionA.label.toUpperCase();
  const labelB = optionB.label.toUpperCase();

  let comparison = 0;
  if (labelA > labelB) {
    comparison = 1;
  } else if (labelA < labelB) {
    comparison = -1;
  }
  return comparison;
};

const convertToOptionFormat = (reportType) => {
  return reportType
    .map(({ id, code, name }) => ({
      value: id,
      label: `${code} - ${name}`,
    }))
    .sort(orderbyCode);
};

// keep selected options' order and handle the case selected report type be deleted
const getSelected = (ids, reportType) =>
  ids.filter((id) => reportType.find((rt) => rt.id === id));

export default ((state: State = initialState, action: any) => {
  switch (action.type) {
    case ACTION.INITIALISE_OPTIONS:
      return {
        ...state,
        options: convertToOptionFormat(action.payload.reportType),
        selected: getSelected(
          action.payload.selectedIds,
          action.payload.reportType
        ),
      };
    case ACTION.SET_OPTIONS:
      return {
        ...state,
        selected: action.payload,
      };
    default:
      return state;
  }
}: Reducer<State, any>);
