/* @flow */
import type { Reducer, Dispatch } from 'redux';

import {
  fetch as fetchEmployees,
  type EmployeePersonalInfo,
  type FetchQuery,
} from '../../../../models/common/EmployeePersonalInfo';
import {
  catchApiError,
  loadingEnd,
  loadingStart,
} from '../../../../../commons/actions/app';
import { SELECT_TAB } from '../../../../../commons/actions/tab';
import { CHANGE_COMPANY, SELECT_MENU_ITEM } from '../../../base/menu-pane/ui';

type State = EmployeePersonalInfo[];

const KEY = 'MODULES/ADMIN_COMMON/EMPLOYEE_SELECTION/UI/SEARCH_QUERY';
const ACTIONS = {
  SET: `${KEY}/SET`,
  UNSET: `${KEY}/UNSET`,
};

const setEmployeeList = (employeeList: EmployeePersonalInfo[]) => ({
  type: ACTIONS.SET,
  payload: employeeList,
});

export const actions = {
  clear: () => ({
    type: ACTIONS.UNSET,
  }),

  fetch: (param: FetchQuery, onSuccess: () => void = () => {}) => (
    dispatch: Dispatch<any>
  ) => {
    dispatch(loadingStart());
    return fetchEmployees(param)
      .then((employeesList) => dispatch(setEmployeeList(employeesList)))
      .then(onSuccess)
      .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
      .then(() => dispatch(loadingEnd()));
  },
};

const initialState: State = [];

export default ((state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET:
      return action.payload;

    case SELECT_TAB:
    case CHANGE_COMPANY:
    case SELECT_MENU_ITEM:
    case ACTIONS.UNSET:
      return initialState;

    default:
      return state;
  }
}: Reducer<State, any>);
