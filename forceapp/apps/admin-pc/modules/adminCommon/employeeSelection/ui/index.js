import { combineReducers } from 'redux';

import selection from './selection';
import searchCondition from './searchCondition';

export default combineReducers({
  selection,
  searchCondition,
});
