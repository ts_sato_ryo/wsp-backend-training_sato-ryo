import Api from '../../commons/api';
import {
  loadingStart,
  loadingEnd,
  catchApiError,
} from '../../commons/actions/app';
import * as base from './base';

const FUNC_NAME = 'job';
export const CREATE_JOB = 'CREATE_JOB';
export const DELETE_JOB = 'DELETE_JOB';
export const SEARCH_JOB = 'SEARCH_JOB';
export const UPDATE_JOB = 'UPDATE_JOB';
export const CREATE_JOB_ERROR = 'CREATE_JOB_ERROR';
export const DELETE_JOB_ERROR = 'DELETE_JOB_ERROR';
export const SEARCH_JOB_ERROR = 'SEARCH_JOB_ERROR';
export const UPDATE_JOB_ERROR = 'UPDATE_JOB_ERROR';
export const GET_CONSTANTS_SCOPED_ASSIGNMENT =
  'GET_CONSTANTS_SCOPED_ASSIGNMENT';

const searchJobSuccess = (records) => {
  return {
    type: SEARCH_JOB,
    payload: records,
  };
};

function createJobSearchError() {
  return {
    type: SEARCH_JOB_ERROR,
  };
}

function createJobSuccess() {
  return {
    type: CREATE_JOB,
  };
}

function updateJobSuccess() {
  return {
    type: UPDATE_JOB,
  };
}

function createJobError() {
  return {
    type: CREATE_JOB_ERROR,
  };
}

function updateJobError() {
  return {
    type: UPDATE_JOB_ERROR,
  };
}

function fromRemoteFormat(param) {
  return {
    ...param,
    records: param.records.map((record) => ({
      ...record,
      isScopedAssignment: String(record.isScopedAssignment),
    })),
  };
}

function toRemoteFormat(param) {
  return base.convertToRemoteFormat({
    ...param,
    isScopedAssignment: String(param.isScopedAssignment) === 'true',
  });
}

export const getConstantsScopedAssignment = () => ({
  type: GET_CONSTANTS_SCOPED_ASSIGNMENT,
});

export const searchJob = (param = {}) => (dispatch) => {
  dispatch(loadingStart());
  const req = { path: `/job/search`, param };
  return Api.invoke(req)
    .then((result) => {
      dispatch(loadingEnd());
      const convertedResult = fromRemoteFormat(result);
      const records = (convertedResult.records || []).map(
        base.convertFromRemoteFormat
      );
      dispatch(searchJobSuccess(records));
    })
    .catch((err) => {
      dispatch(loadingEnd());
      dispatch(createJobSearchError());
      dispatch(catchApiError(err, { isContinuable: true }));
      throw err; // 後続（component/MainContent）のthenをスキップさせる
    });
};

export const createJob = (param) => (dispatch) => {
  const req = {
    path: '/job/create',
    param: toRemoteFormat(param),
  };
  dispatch(loadingStart());
  return Api.invoke(req)
    .then((_result) => {
      dispatch(loadingEnd());
      dispatch(createJobSuccess());
    })
    .catch((err) => {
      dispatch(loadingEnd());
      dispatch(createJobError());
      dispatch(catchApiError(err, { isContinuable: true }));
      throw err;
    });
};

export const deleteJob = (param) => {
  return base.del(FUNC_NAME, param, DELETE_JOB, DELETE_JOB_ERROR);
};

export const updateJob = (param) => (dispatch) => {
  dispatch(loadingStart());
  const req = {
    path: '/job/update',
    param: toRemoteFormat(param),
  };
  return Api.invoke(req)
    .then(() => {
      dispatch(loadingEnd());
      dispatch(updateJobSuccess());
    })
    .catch((err) => {
      dispatch(loadingEnd());
      dispatch(updateJobError());
      dispatch(catchApiError(err, { isContinuable: true }));
      throw err;
    });
};
