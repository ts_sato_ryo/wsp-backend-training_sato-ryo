import * as base from './base';

const FUNC_NAME = 'exp/expense-type';
export const SEARCH_EXPENSE_TYPE = 'SEARCH_EXPENSE_TYPE';
export const CREATE_EXPENSE_TYPE = 'CREATE_EXPENSE_TYPE';
export const DELETE_EXPENSE_TYPE = 'DELETE_EXPENSE_TYPE';
export const UPDATE_EXPENSE_TYPE = 'UPDATE_EXPENSE_TYPE';
export const SEARCH_EXPENSE_TYPE_ERROR = 'SEARCH_EXPENSE_TYPE_ERROR';
export const CREATE_EXPENSE_TYPE_ERROR = 'CREATE_EXPENSE_TYPE_ERROR';
export const DELETE_EXPENSE_TYPE_ERROR = 'DELETE_EXPENSE_TYPE_ERROR';
export const UPDATE_EXPENSE_TYPE_ERROR = 'UPDATE_EXPENSE_TYPE_ERROR';
export const GET_CONSTANTS_EXPENSE_TYPE = 'GET_CONSTANTS_EXPENSE_TYPE';

export const searchExpenseType = (param = {}) => {
  return base.search(
    FUNC_NAME,
    param,
    SEARCH_EXPENSE_TYPE,
    SEARCH_EXPENSE_TYPE_ERROR
  );
};

export const createExpenseType = (param) => {
  return base.create(
    FUNC_NAME,
    {
      ...param,
      taxType1Id: param.useCompanyTaxMaster
        ? param.taxType1Id
        : param.taxTypeOptional1Id,
    },
    CREATE_EXPENSE_TYPE,
    CREATE_EXPENSE_TYPE_ERROR
  );
};

export const deleteExpenseType = (param) => {
  return base.del(
    FUNC_NAME,
    param,
    DELETE_EXPENSE_TYPE,
    DELETE_EXPENSE_TYPE_ERROR
  );
};

export const updateExpenseType = (param) => {
  return base.update(
    FUNC_NAME,
    {
      ...param,
      taxType1Id: param.useCompanyTaxMaster
        ? param.taxType1Id
        : param.taxTypeOptional1Id,
    },
    UPDATE_EXPENSE_TYPE,
    UPDATE_EXPENSE_TYPE_ERROR
  );
};

export const getConstantsExpenseType = () => ({
  type: GET_CONSTANTS_EXPENSE_TYPE,
});
