import * as base from './base';
import * as history from './history';

const FUNC_NAME = 'employee';

export const SEARCH_EMPLOYEE = 'SEARCH_EMPLOYEE';
export const SEARCH_MANAGER = 'SEARCH_MANAGER';
export const CREATE_EMPLOYEE = 'CREATE_EMPLOYEE';
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE';
export const CREATE_HISTORY_EMPLOYEE = 'CREATE_HISTORY_EMPLOYEE';
export const SEARCH_HISTORY_EMPLOYEE = 'SEARCH_HISTORY_EMPLOYEE';
export const UPDATE_HISTORY_EMPLOYEE = 'UPDATE_HISTORY_EMPLOYEE';
export const DELETE_HISTORY_EMPLOYEE = 'DELETE_HISTORY_EMPLOYEE';
export const SEARCH_EMPLOYEE_ERROR = 'SEARCH_EMPLOYEE_ERROR';
export const SEARCH_MANAGER_ERROR = 'SEARCH_MANAGER_ERROR';
export const CREATE_EMPLOYEE_ERROR = 'CREATE_EMPLOYEE_ERROR';
export const DELETE_EMPLOYEE_ERROR = 'DELETE_EMPLOYEE_ERROR';
export const UPDATE_EMPLOYEE_ERROR = 'UPDATE_EMPLOYEE_ERROR';
export const CREATE_HISTORY_EMPLOYEE_ERROR = 'CREATE_HISTORY_EMPLOYEE_ERROR';
export const SEARCH_HISTORY_EMPLOYEE_ERROR = 'SEARCH_HISTORY_EMPLOYEE_ERROR';
export const UPDATE_HISTORY_EMPLOYEE_ERROR = 'UPDATE_HISTORY_EMPLOYEE_ERROR';
export const DELETE_HISTORY_EMPLOYEE_ERROR = 'DELETE_HISTORY_EMPLOYEE_ERROR';
export const SEARCH_USER = 'SEARCH_USER';
export const SEARCH_USER_ERROR = 'SEARCH_USER_ERROR';

export const searchEmployee = (param = {}) => {
  return base.search(FUNC_NAME, param, SEARCH_EMPLOYEE, SEARCH_EMPLOYEE_ERROR);
};

export const searchManager = (param = {}) => {
  return base.search(FUNC_NAME, param, SEARCH_MANAGER, SEARCH_MANAGER_ERROR);
};

export const createEmployee = (param) => {
  return base.create(FUNC_NAME, param, CREATE_EMPLOYEE, CREATE_EMPLOYEE_ERROR);
};

export const deleteEmployee = (param) => {
  return base.del(FUNC_NAME, param, DELETE_EMPLOYEE, DELETE_EMPLOYEE_ERROR);
};

export const updateEmployee = (param) => {
  return base.update(FUNC_NAME, param, UPDATE_EMPLOYEE, UPDATE_EMPLOYEE_ERROR);
};

export const searchHistoryEmployee = (param = {}) => {
  return history.searchHistory(FUNC_NAME, param, SEARCH_HISTORY_EMPLOYEE, SEARCH_HISTORY_EMPLOYEE_ERROR);
};

export const createHistoryEmployee = (param) => {
  return history.createHistory(FUNC_NAME, param, CREATE_HISTORY_EMPLOYEE, CREATE_HISTORY_EMPLOYEE_ERROR);
};

export const updateHistoryEmployee = (param) => {
  return history.updateHistory(FUNC_NAME, param, UPDATE_HISTORY_EMPLOYEE, UPDATE_HISTORY_EMPLOYEE_ERROR);
};

export const deleteHistoryEmployee = (param) => {
  return history.deleteHistory(FUNC_NAME, param, DELETE_HISTORY_EMPLOYEE, DELETE_HISTORY_EMPLOYEE_ERROR);
};

export const searchUser = (param = {}) => {
  return base.search('user', param, SEARCH_USER, SEARCH_USER_ERROR);
};
