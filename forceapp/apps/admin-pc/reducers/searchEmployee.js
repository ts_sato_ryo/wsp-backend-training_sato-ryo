import { SEARCH_EMPLOYEE } from '../actions/employee';

const initialState = [];

export default function searchEmployeeReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_EMPLOYEE:
      return action.payload;
    default:
      return state;
  }
}
