import { SEARCH_JOB } from '../actions/job';

const initialState = [];

export default function searchJobReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_JOB:
      return action.payload;
    default:
      return state;
  }
}
