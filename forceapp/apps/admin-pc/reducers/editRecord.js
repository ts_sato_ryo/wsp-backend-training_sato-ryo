import { convertForView } from '../utils/RecordUtil';
import { INITIALIZE, SET_EDIT_RECORD } from '../actions/editRecord';

const initialState = {};

export default function editRecordReducer(state = initialState, action) {
  switch (action.type) {
    case INITIALIZE:
      return initialState;
    case SET_EDIT_RECORD:
      return convertForView(action.payload);
    default:
      return state;
  }
}
