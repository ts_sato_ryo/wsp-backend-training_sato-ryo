import { SEARCH_LEAVE } from '../actions/leave';

const initialState = [];

export default function searchLeaveReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_LEAVE:
      return action.payload;
    default:
      return state;
  }
}
