// @flow

import { combineReducers } from 'redux';

import attPatternEmployeeBatch from '../modules/attPatternEmployeeBatch';
import common from '../../commons/reducers';
import adminCommon from '../modules/adminCommon';
import base from '../modules/base';
import delegateApprover from '../modules/delegateApprover';
import job from '../modules/job';
import editRecord from './editRecord';
import editRecordHistory from './editRecordHistory';
import searchExpSetting from './searchExpSetting';
import searchCustomHint from './searchCustomHint';
import getOrganizationSetting from './getOrganizationSetting';
import searchAccountingPeriod from './searchAccountingPeriod';
import searchCompany from './searchCompany';
import searchCostCenter from './searchCostCenter';
import searchCountry from './searchCountry';
import searchCurrency from './searchCurrency';
import searchIsoCurrencyCode from './searchIsoCurrencyCode';
import getCurrencyPair from './getCurrencyPair';
import searchDepartment from './searchDepartment';
import searchEmployee from './searchEmployee';
import searchReportType from './searchReportType';
import searchExpenseType from './searchExpenseType';
import searchExpTypeGroup from './searchExpTypeGroup';
import searchExtendedItem from './searchExtendedItem';
import searchExtendedItemCustom from './searchExtendedItemCustom';
import searchTaxType from './searchTaxType';
import searchHistory from './searchHistory';
import searchJob from './searchJob';
import searchJobType from './searchJobType';
import searchLeave from './searchLeave';
import searchLeaveOfAbsence from './searchLeaveOfAbsence';
import searchShortTimeWorkSetting from './searchShortTimeWorkSetting';
import searchShortTimeWorkReason from './searchShortTimeWorkReason';
import searchTimeSetting from './searchTimeSetting';
import searchPermission from './searchPermission';
import agreementAlertSetting from '../modules/agreement-alert-setting';
import calendar from '../modules/calendar';
import shortTimeWorkPeriodStatus from '../modules/shortTimeWorkPeriodStatus';
import leaveOfAbsencePeriodStatus from '../modules/leaveOfAbsencePeriodStatus';
import annualPaidLeaveManagement from '../modules/annual-paid-leave-management';
import managedLeaveManagement from '../modules/managed-leave-management';
import plannerSetting from '../modules/plannerSetting';
import searchUser from './searchUser';
import searchWorkCategory from './searchWorkCategory';
import searchWorkingType from './searchWorkingType';
import searchAttPattern from './searchAttPattern';
import searchVendor from './searchVendor';
import searchEmployeeGroup from './searchEmployeeGroup';
import sfObjFieldValues from './sfObjFieldValues';
import tmpEditRecord from './tmpEditRecord';
import tmpEditRecordHistory from './tmpEditRecordHistory';
import value2msgkey from './value2msgkey';
import detailPane from '../modules/base/detail-pane';
import exchangeRate from '../modules/exchangeRate';
import commuterRoute from '../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm';
import routeList from '../../domain/modules/exp/jorudan/route';
import mobileSetting from '../modules/mobileSetting';
import groupReportType from '../modules/groupReportType';
import expTypeLinkConfig from '../modules/expTypeLinkConfig';
import fixedAllowanceList from '../modules/fixedAllowanceList';

import { type $State } from '../../commons/utils/TypeUtil';

// eslint-disable-next-line no-unused-vars
function env(state: Object = {}, action: Object) {
  return state;
}

const rootReducer = {
  attPatternEmployeeBatch,
  env,
  common,
  adminCommon,
  base,
  delegateApprover,
  job,
  agreementAlertSetting,
  editRecord,
  editRecordHistory,
  getOrganizationSetting,
  searchCustomHint,
  searchExpSetting,
  searchAccountingPeriod,
  searchCompany,
  searchCostCenter,
  searchCountry,
  searchCurrency,
  searchIsoCurrencyCode,
  getCurrencyPair,
  searchDepartment,
  searchEmployee,
  searchExpenseType,
  searchExpTypeGroup,
  searchExtendedItem,
  searchExtendedItemCustom,
  searchTaxType,
  searchHistory,
  searchJob,
  searchJobType,
  mobileSetting,
  searchLeave,
  searchLeaveOfAbsence,
  searchShortTimeWorkSetting,
  searchShortTimeWorkReason,
  searchTimeSetting,
  calendar,
  shortTimeWorkPeriodStatus,
  leaveOfAbsencePeriodStatus,
  annualPaidLeaveManagement,
  managedLeaveManagement,
  plannerSetting,
  searchUser,
  searchWorkCategory,
  searchWorkingType,
  searchAttPattern,
  sfObjFieldValues,
  tmpEditRecord,
  tmpEditRecordHistory,
  value2msgkey,
  detailPane,
  exchangeRate,
  commuterRoute,
  routeList,
  searchReportType,
  searchPermission,
  searchVendor,
  searchEmployeeGroup,
  groupReportType,
  expTypeLinkConfig,
  fixedAllowanceList,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
