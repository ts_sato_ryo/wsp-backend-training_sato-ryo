import { SEARCH_USER } from '../actions/employee';

const initialState = [];

export default function searchUserReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_USER:
      return action.payload;
    default:
      return state;
  }
}
