// @flow

import * as React from 'react';

type ColumnKey = string;
type Row = { [ColumnKey]: * };

export type Column = {
  key: string,
  name?: string,
  width?: number,
  filterable?: boolean,
  sortable?: boolean,
  resizable?: boolean,
  filterRenderer?: React.ComponentType<*>,
  filterValues?: (
    Row,
    {
      filterTerm: string,
      column: Column,
      filterValues?: $PropertyType<Column, 'filterValues'>,
    },
    ColumnKey
  ) => boolean,
};
