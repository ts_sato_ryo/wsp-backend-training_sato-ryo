// @flow

import * as React from 'react';

import msg from '../../../../commons/languages/index';
import TextField from '../../../../commons/components/fields/TextField';
import type { Column } from '../DataGridColumn';

import './TextFieldFilter.scss';

const ROOT = 'admin-pc-data-grid-text-field-filter';

type Props = {
  onChange: ({ filterTerm: string, column: Column }) => void,
  column: Column,
};

type State = {
  value: string,
};

type PresentationProps = {
  onTextFieldChange: (event: SyntheticInputEvent<*>) => void,
} & State &
  Props;

const withState = (WrappedComponent: React.ComponentType<PresentationProps>) =>
  class TextFieldFilterContainer extends React.Component<Props, State> {
    handleOnChange: $PropertyType<PresentationProps, 'onTextFieldChange'>;

    constructor(_props: Props) {
      super();

      this.handleOnChange = this.handleOnChange.bind(this);

      this.state = {
        value: '',
      };
    }

    handleOnChange(event: SyntheticInputEvent<*>) {
      const value = event.target.value;
      this.setState({ value: event.target.value });
      this.props.onChange({ filterTerm: value, column: this.props.column });
    }

    render() {
      const inputKey = `header-filter-${this.props.column.key}`;
      return (
        <WrappedComponent
          key={inputKey}
          {...this.props}
          {...this.state}
          onTextFieldChange={this.handleOnChange}
        />
      );
    }
  };

const TextFieldFilterPresentation = ({
  value,
  onTextFieldChange,
  ..._props
}: PresentationProps) => (
  <div id={ROOT} className={ROOT}>
    <TextField
      onChange={onTextFieldChange}
      value={value}
      placeholder={msg().Admin_Lbl_Search}
    />
  </div>
);

const Component = withState(TextFieldFilterPresentation);
Component.displayName = 'TextFieldFilter';

export default Component;
