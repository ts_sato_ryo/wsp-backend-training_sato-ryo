// @flow
type NestedValue = { [string]: NestedValue };

const getValueBoundNestedKey = (key: string) => ({
  value,
}: {
  value: { [string]: NestedValue },
}): * => {
  const keys = key.split('.');
  const result = keys.reduce((obj, aKey) => obj && obj[aKey], value);
  return result === 0 ? 0 : result || '';
};

export default getValueBoundNestedKey;
