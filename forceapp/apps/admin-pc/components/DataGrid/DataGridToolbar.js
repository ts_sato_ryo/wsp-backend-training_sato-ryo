// @flow

import * as React from 'react';
import { Toolbar } from 'react-data-grid-addons';

const ROOT = 'admin-pc-data-grid-data-grid-toolbar';

type Props = {
  onToggleFilter: () => void,
  enableFilter: boolean,
};

export default class DataGridToolbar extends Toolbar {
  componentDidMount() {
    const props = (this.props: Props);

    // 検索フィルターは初期表示する
    if (props.enableFilter) {
      props.onToggleFilter();
    }
  }

  render() {
    return <div className={ROOT} />;
  }
}
