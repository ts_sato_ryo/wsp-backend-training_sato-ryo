import React from 'react';
import { Toolbar } from 'react-data-grid-addons';

import './CustomToolbar.scss';

import btnSearch from '../../images/btnSearch.png';

export default class CustomToolbar extends Toolbar {

  constructor(props) {
    super(props);
    this.onToggleFilter = this.onToggleFilter.bind(this);
  }

  onToggleFilter() {
    if (this.props.onClickToggleSearchFilterButton
      && this.props.onClickToggleSearchFilterButton instanceof Function) {
      this.props.onClickToggleSearchFilterButton();
    }
    this.props.onToggleFilter();
  }

  customRenderToggleFilterButton() { // NOTE: overrideできないので、別のメソッドとして定義
    if (this.props.enableFilter) {
      return (
        <button type="button" className="btn" onClick={this.onToggleFilter}>
          <img src={btnSearch} />
        </button>
      );
    }
    return null;
  }

  render() {
    return (
      <div className="react-grid-Toolbar">
        <div className="tools">
          {this.renderAddRowButton()}
          {this.customRenderToggleFilterButton()}
          {this.props.children}
        </div>
      </div>
    );
  }
}
