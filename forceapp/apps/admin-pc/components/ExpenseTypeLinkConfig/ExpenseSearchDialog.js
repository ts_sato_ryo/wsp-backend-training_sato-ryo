// @flow
import * as React from 'react';

import msg from '../../../commons/languages';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';
import Button from '../../../commons/components/buttons/Button';
import DataGrid from '../DataGrid';
import { type Expense } from '../../modules/expTypeLinkConfig/ui';

import ExpenseSearchForm, {
  type Props as SearchFormProps,
} from './ExpenseSearchForm';

import './ExpenseSearchDialog.scss';

const ROOT = 'admin-pc-expense-type-search-dialog';

export type Props = $ReadOnly<{|
  foundExpense: Array<Expense>,
  isAddButtonDisabled: boolean,
  cancelSelection: () => void,
  addSelectedExp: () => void,
  toggleSelection: (Expense) => void,
  ...SearchFormProps,
|}>;

export default class ExpenseSearchDialog extends React.Component<Props> {
  handleRowClick: Function;
  handleRowsToggle: Function;
  handleRowClick = (rowIdx: number, row: Expense) =>
    this.props.toggleSelection(row);

  handleRowsToggle = (rows: Array<Object>) => {
    return rows.forEach(({ row }) => this.props.toggleSelection(row));
  };

  render() {
    return (
      <DialogFrame
        className={`${ROOT}`}
        title={msg().Admin_Lbl_ExpenseSearch}
        hide={this.props.cancelSelection}
        footer={
          <DialogFrame.Footer>
            <Button
              className={`${ROOT}__button`}
              onClick={this.props.cancelSelection}
            >
              {msg().Com_Btn_Cancel}
            </Button>
            <Button
              className={`${ROOT}__button ${ROOT}__add-button`}
              disabled={this.props.isAddButtonDisabled}
              onClick={this.props.addSelectedExp}
            >
              {msg().Com_Btn_Add}
            </Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__body ${ROOT}__list--search-result`}>
          <ExpenseSearchForm search={this.props.search} />
          <DataGrid
            numberOfRowsVisibleWithoutScrolling={5}
            columns={[
              {
                key: 'expenseTypeCode',
                name: msg().Admin_Lbl_ExpenseTypeCode,
              },
              {
                key: 'expenseTypeName',
                name: msg().Admin_Lbl_ExpenseTypeName,
              },
              {
                key: 'expenseGroupCode',
                name: msg().Admin_Lbl_ExpenseTypeGroupCode,
              },
              {
                key: 'expenseGroupName',
                name: msg().Admin_Lbl_ExpenseTypeGroupName,
              },
            ]}
            showCheckbox
            rows={this.props.foundExpense}
            onRowClick={this.handleRowClick}
            onRowsSelected={this.handleRowsToggle}
            onRowsDeselected={this.handleRowsToggle}
          />
        </div>
      </DialogFrame>
    );
  }
}
