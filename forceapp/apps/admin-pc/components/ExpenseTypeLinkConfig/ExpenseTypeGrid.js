// @flow
import React from 'react';
import Button from '../../../commons/components/buttons/Button';
import msg from '../../../commons/languages';
import DataGrid from '../DataGrid';
import { type Expense } from '../../modules/expTypeLinkConfig/ui';

import './ExpenseTypeGrid.scss';

const ROOT = 'admin-pc-detail-pane-expense-type-grid';

type Props = $ReadOnly<{|
  selectedId: string,
  selectedExpense: Array<Expense>,
  remove: () => void,
  toggleSelectedExp: (Expense) => void,
  setSelectedExp: (selectedId: string) => void,
  cleanSelectedExpense: () => void,
  disabled: boolean,
  config: Object,
  onChangeDetailItem: (key: string, value: Array<string>) => void,
  isRemoveButtonDisabled: boolean,
|}>;

export default class ExpenseTypeGrid extends React.Component<Props> {
  handleRowClick: Function;
  handleRowsToggle: Function;

  componentDidMount() {
    const { selectedId, setSelectedExp } = this.props;
    setSelectedExp(selectedId);
  }

  componentDidUpdate(preProps: Props) {
    const {
      selectedId,
      disabled,
      selectedExpense,
      onChangeDetailItem,
      config,
      setSelectedExp,
    } = this.props;

    const ids = selectedExpense.map((exp) => exp.id);
    const isSelectedIdChanged = preProps.selectedId !== selectedId;
    const isSelectedExpenseChanged =
      preProps.selectedExpense.length !== selectedExpense.length;
    const isDisabledChanged = preProps.disabled !== disabled;

    if (isSelectedIdChanged) {
      setSelectedExp(selectedId);
    }
    if (isSelectedIdChanged || isSelectedExpenseChanged) {
      onChangeDetailItem(config.key, ids);
    }
    // when cancel current operation, reset to original data
    if (disabled && isDisabledChanged) {
      setSelectedExp(selectedId);
    }
  }

  componentWillUnmount() {
    const { cleanSelectedExpense, onChangeDetailItem, config } = this.props;
    cleanSelectedExpense();
    onChangeDetailItem(config.key, []);
  }

  handleRowClick = (rowIdx: number, row: Expense) =>
    this.props.toggleSelectedExp(row);

  handleRowsToggle = (rows: Array<Object>) => {
    return rows.forEach(({ row }) => this.props.toggleSelectedExp(row));
  };

  render() {
    let rootClassName = ROOT;
    if (this.props.disabled) {
      rootClassName += ` ${ROOT}__dataGrid-disabled`;
    }

    return (
      <div className={rootClassName}>
        <DataGrid
          key={this.props.selectedExpense.length}
          numberOfRowsVisibleWithoutScrolling={5}
          columns={[
            {
              key: 'expenseTypeCode',
              name: msg().Admin_Lbl_ExpenseTypeCode,
              filterable: true,
            },
            {
              key: 'expenseTypeName',
              name: msg().Admin_Lbl_ExpenseTypeName,
              filterable: true,
            },
            {
              key: 'expenseGroupCode',
              name: msg().Admin_Lbl_ExpenseTypeGroupCode,
              filterable: true,
            },
            {
              key: 'expenseGroupName',
              name: msg().Admin_Lbl_ExpenseTypeGroupName,
              filterable: true,
            },
          ]}
          showCheckbox
          rows={this.props.selectedExpense}
          onRowClick={this.handleRowClick}
          onRowsSelected={this.handleRowsToggle}
          onRowsDeselected={this.handleRowsToggle}
          disabled={this.props.disabled}
        />
        {this.props.selectedExpense.length !== 0 && (
          <Button
            type="destructive"
            className={`${ROOT}__editAction ${ROOT}__removeButton`}
            onClick={this.props.remove}
            disabled={this.props.isRemoveButtonDisabled}
          >
            {msg().Com_Btn_Remove}
          </Button>
        )}
      </div>
    );
  }
}
