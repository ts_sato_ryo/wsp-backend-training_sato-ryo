// @flow

import * as React from 'react';

import msg from '../../../commons/languages';
import displayName from '../../../commons/concerns/displayName';
import { compose } from '../../../commons/utils/FnUtil';
import Form from '../../../commons/components/Form';
import TextField from '../../../commons/components/fields/TextField';
import IconButton from '../../../commons/components/buttons/IconButton';
import btnSearch from '../../../commons/images/btnSearchVia.png';

const ROOT = 'admin-pc-expense-type-search-form';

export type Props = $ReadOnly<{|
  search: (
    $ReadOnly<{|
      name?: string,
      code?: string,
      expGroupName?: string,
      expGroupCode?: string,
    |}>
  ) => void,
|}>;

type InternalProps = $ReadOnly<{|
  expName?: string,
  expCode?: string,
  expGroupName?: string,
  expGroupCode?: string,
  updateValue: (key: string) => (value: string) => void,
  search: () => void,
|}>;

const shouldComponentUpdate = (condition: (props: *) => boolean) => (
  WrappedComponent: React.ComponentType<*>
) => {
  return class extends React.Component<Props> {
    shouldComponentUpdate() {
      return condition(this.props);
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
};

const withSearchable = (WrappedComponent: React.ComponentType<*>) => {
  type State = {
    expName?: string,
    expCode?: string,
    expGroupName?: string,
    expGroupCode?: string,
  };

  return class extends React.PureComponent<Props, State> {
    updateValue: Function;
    search: () => void;

    state = {
      expName: '',
      expCode: '',
      expGroupName: '',
      expGroupCode: '',
    };

    updateValue = (key: string) => (value: string) =>
      this.setState({
        [key]: value,
      });

    search = () =>
      this.props.search({
        name: this.state.expName,
        code: this.state.expCode,
        expGroupName: this.state.expGroupName,
        expGroupCode: this.state.expGroupCode,
      });

    render() {
      return (
        <WrappedComponent
          {...(this.props: Object)}
          updateValue={this.updateValue}
          search={this.search}
        />
      );
    }
  };
};

class ExpSearchHeader extends React.PureComponent<InternalProps> {
  render() {
    type controlState = {
      onChange: Function,
      onKeyDown: Function,
      label: string,
      value?: string,
      autoFocus?: boolean,
    };
    const Control = ({
      onChange,
      onKeyDown,
      label,
      value,
      autoFocus = false,
    }: controlState) => {
      return (
        <div className={`${ROOT}__control`}>
          <div className={`${ROOT}__label`}>{label}</div>
          <div className={`${ROOT}__input`}>
            <TextField
              onChange={(_e, test: string) => onChange(test)}
              onKeyDown={(e, text: string) => {
                if (e.key === 'Enter') {
                  onKeyDown(text);
                }
              }}
              value={value}
              autoFocus={autoFocus}
              placeholder={msg().Admin_Lbl_Search}
            />
          </div>
        </div>
      );
    };

    return (
      <Form className={ROOT} onSubmit={() => this.props.search()}>
        <Control
          onChange={this.props.updateValue('expCode')}
          onKeyDown={() => this.props.search()}
          value={this.props.expCode}
          label={msg().Admin_Lbl_ExpenseTypeCode}
          autoFocus
        />
        <Control
          onChange={this.props.updateValue('expName')}
          onKeyDown={() => this.props.search()}
          value={this.props.expName}
          label={msg().Admin_Lbl_ExpenseTypeName}
        />
        <Control
          onChange={this.props.updateValue('expGroupCode')}
          onKeyDown={() => this.props.search()}
          value={this.props.expGroupCode}
          label={msg().Admin_Lbl_ExpenseTypeGroupCode}
        />
        <Control
          onChange={this.props.updateValue('expGroupName')}
          onKeyDown={() => this.props.search()}
          value={this.props.expGroupName}
          label={msg().Admin_Lbl_ExpenseTypeGroupName}
        />
        <IconButton
          src={btnSearch}
          className={`${ROOT}__search-button`}
          onClick={() => this.props.search()}
        />
      </Form>
    );
  }
}

export default (compose(
  displayName('ExpSearchFormView'),
  shouldComponentUpdate(() => false),
  withSearchable
)(ExpSearchHeader): React.ComponentType<Object>);
