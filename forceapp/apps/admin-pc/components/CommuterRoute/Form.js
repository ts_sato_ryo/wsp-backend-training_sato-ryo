// @flow
import React from 'react';
import moment from 'moment';

// common components
import msg from '../../../commons/languages';
import Button from '../../../commons/components/buttons/Button';

// reusable components
import Condition from '../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteForm/Condition';
import Via from '../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteForm/Via';
import AddViaButton from '../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteForm/AddViaButton';
import RouteOption from '../../../commons/components/exp/Form/RecordItem/TransitJorudanJP/RouteForm/RouteOption';
import MultiColumnsGrid from '../../../commons/components/MultiColumnsGrid';
import SuggestContainer from '../../../expenses-pc/containers/Expenses/SuggestContainer';

// prop types
import type { StationInfo } from '../../../domain/models/exp/jorudan/Station';

type Props = {
  // states
  tmpOrigin: string,
  errorOrigin: string,
  tmpViaList: Array<string>,
  errorViaList: Array<string>,
  tmpArrival: string,
  errorArrival: string,
  useChargedExpress: string,
  // event handlers
  onDeleteVia: (number) => void,
  onClickSearchRouteButton: () => void,
  onChangeUseChargedExpress: (string) => void,
  onClickAddViaButton: () => void,
  onChangeOrigin: (StationInfo) => void,
  onChangeTmpOrigin: (string, boolean) => void,
  onChangeArrival: (StationInfo) => void,
  onChangeTmpArrival: (string, boolean) => void,
  onChangeViaList: (StationInfo, number) => void,
  onChangeTmpViaList: (string, number, boolean) => void,
};

const ROOT = 'commuter-route__form';

const CommuterRouteForm = (props: Props) => {
  const targetDate = moment().format('YYYY-MM-DD');
  const useChargedExpress = [
    { key: '0', value: msg().Exp_Lbl_RouteOptionUseChargedExpress_Use },
    { key: '1', value: msg().Exp_Lbl_RouteOptionUseChargedExpress_DoNotUse },
  ];

  return (
    <div className={ROOT}>
      <MultiColumnsGrid alignments={['top', 'top', 'top']} sizeList={[4, 4, 4]}>
        <Condition
          title={msg().Exp_Lbl_DepartFrom}
          inputType="origin"
          placeholder={msg().Exp_Lbl_RoutePlaceholder}
          onChange={props.onChangeOrigin}
          onChangeTmp={props.onChangeTmpOrigin}
          readOnly={false}
          error={props.errorOrigin}
          value={props.tmpOrigin}
          targetDate={targetDate}
          suggest={SuggestContainer}
        />
        <div className={`${ROOT}__middle-component`}>
          <Via
            onChange={props.onChangeViaList}
            onChangeTmp={props.onChangeTmpViaList}
            readOnly={false}
            error={props.errorViaList}
            tmpViaList={props.tmpViaList}
            targetDate={targetDate}
            suggest={SuggestContainer}
            withDelete
            onDeleteVia={props.onDeleteVia}
          />
          <AddViaButton
            onClickAddViaButton={props.onClickAddViaButton}
            readOnly={false}
            tmpViaList={props.tmpViaList}
          />
          <Button
            className={`${ROOT}__search-button`}
            type="primary"
            onClick={props.onClickSearchRouteButton}
          >
            {msg().Exp_Btn_RouteSearch}
          </Button>
          <RouteOption
            title={msg().Exp_Lbl_RouteOptionUseChargedExpress}
            name="useChargedExpress"
            items={useChargedExpress}
            checked={props.useChargedExpress}
            onChange={props.onChangeUseChargedExpress}
            readOnly={false}
          />
        </div>
        <Condition
          title={msg().Exp_Lbl_Destination}
          inputType="arrival"
          placeholder={msg().Exp_Lbl_RoutePlaceholder}
          onChange={props.onChangeArrival}
          onChangeTmp={props.onChangeTmpArrival}
          readOnly={false}
          error={props.errorArrival}
          value={props.tmpArrival}
          targetDate={targetDate}
          suggest={SuggestContainer}
        />
      </MultiColumnsGrid>
    </div>
  );
};

export default CommuterRouteForm;
