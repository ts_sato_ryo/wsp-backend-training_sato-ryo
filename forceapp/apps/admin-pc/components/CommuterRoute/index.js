// @flow
import React from 'react';

// common components
import msg from '../../../commons/languages';
import Button from '../../../commons/components/buttons/Button';
import DialogFrame from '../../../commons/components/dialogs/DialogFrame';

// custom components
import Form from './Form';
import Content from './Content';
import RecordHeader from './RecordHeader';
import RecordBody from './RecordBody';

// prop types
import type { StationInfo } from '../../../domain/models/exp/jorudan/Station';
import type { Route } from '../../../domain/models/exp/jorudan/Route';

// styles
import './index.scss';
import '../../../commons/components/exp/Form/Dialog/RouteSelect/RecordHeader/index.scss';
import '../../../commons/components/exp/Form/Dialog/RouteSelect/index.scss';
import '../../../commons/components/exp/Form/Dialog/RouteSelect/ContentsHeader/index.scss';

const ROOT = 'ts-expenses-modal-route';
const SELECTED_ROUTE_KEY = 'jorudanRoute';

type Props = {
  // states
  disabled: boolean,
  errorArrival: string,
  errorOrigin: string,
  errorViaList: Array<string>,
  route: Route,
  tmpArrival: string,
  tmpOrigin: string,
  tmpViaList: Array<string>,
  useChargedExpress: string,
  // event handlers
  onClickDeleteViaButton: (number) => void,
  onClickAddViaButton: () => void,
  onClickSearchRouteButton: () => void,
  onChangeUseChargedExpress: (string) => void,
  onChangeOrigin: (StationInfo) => void,
  onChangeTmpOrigin: (string, boolean) => void,
  onChangeArrival: (StationInfo) => void,
  onChangeTmpArrival: (string, boolean) => void,
  onChangeViaList: (StationInfo, number) => void,
  onChangeTmpViaList: (string, number, boolean) => void,
  onClickResetRouteButton: () => void,
  // admin methods
  tmpEditRecord: Object,
  onChangeDetailItem: (string, *) => void,
};

type State = {
  visible: boolean,
  selectedRoute: any,
};

export default class CommuterRoute extends React.Component<Props, State> {
  state = {
    visible: false,
    selectedRoute: this.props.tmpEditRecord.jorudanRoute || null,
  };

  componentWillReceiveProps(nextProps: Props) {
    const isSelectedRouteChanged =
      this.props.tmpEditRecord !== nextProps.tmpEditRecord;

    if (isSelectedRouteChanged) {
      this.setState({
        selectedRoute: nextProps.tmpEditRecord.jorudanRoute || null,
      });
    }
  }

  componentWillUnmount() {
    this.resetRouteInfo();
  }

  toggleDialogVisibility = () => {
    this.setState((prevState) => ({
      visible: !prevState.visible,
    }));
  };

  resetRouteInfo = () => {
    this.props.onClickResetRouteButton();
    this.props.onChangeDetailItem(SELECTED_ROUTE_KEY, null);
    this.setState({
      selectedRoute: null,
    });
  };

  handleRouteSelectClick = (item: any) => {
    const selectedRoute = item;

    this.props.onChangeDetailItem(SELECTED_ROUTE_KEY, item);
    // set selected state and hide dialog
    this.setState({
      selectedRoute,
      visible: false,
    });
  };

  renderDialog() {
    if (!this.state.visible) {
      return null;
    } else {
      return (
        <DialogFrame
          title={msg().Exp_Lbl_CommuterPassSearch}
          className={ROOT}
          hide={this.toggleDialogVisibility}
          footer={
            <div className={`${ROOT}-btn-close`}>
              <Button onClick={this.toggleDialogVisibility}>
                {msg().Com_Btn_Close}
              </Button>
            </div>
          }
        >
          <div className={`${ROOT}-contents`}>
            <Form
              useChargedExpress={this.props.useChargedExpress}
              tmpOrigin={this.props.tmpOrigin}
              errorOrigin={this.props.errorOrigin}
              onChangeOrigin={this.props.onChangeOrigin}
              onChangeTmpOrigin={this.props.onChangeTmpOrigin}
              tmpViaList={this.props.tmpViaList}
              errorViaList={this.props.errorViaList}
              onChangeViaList={this.props.onChangeViaList}
              onChangeTmpViaList={this.props.onChangeTmpViaList}
              onClickAddViaButton={this.props.onClickAddViaButton}
              tmpArrival={this.props.tmpArrival}
              errorArrival={this.props.errorArrival}
              onChangeArrival={this.props.onChangeArrival}
              onChangeTmpArrival={this.props.onChangeTmpArrival}
              onChangeUseChargedExpress={this.props.onChangeUseChargedExpress}
              onClickSearchRouteButton={this.props.onClickSearchRouteButton}
              onDeleteVia={this.props.onClickDeleteViaButton}
            />
            <Content
              route={this.props.route}
              onClickRouteSelectListItem={this.handleRouteSelectClick}
            />
          </div>
        </DialogFrame>
      );
    }
  }

  renderSelectedRoute() {
    if (this.state.selectedRoute) {
      return (
        <div className="commuter-route__selected">
          <RecordBody item={this.state.selectedRoute} />
          <RecordHeader item={this.state.selectedRoute} />
          <Button disabled={this.props.disabled} onClick={this.resetRouteInfo}>
            {msg().Admin_Lbl_CommuterResetButton}
          </Button>
        </div>
      );
    } else {
      return (
        <React.Fragment>
          <Button
            disabled={this.props.disabled}
            type="secondary"
            onClick={this.toggleDialogVisibility}
          >
            {msg().Admin_Lbl_SearchRoute}
          </Button>
          {this.renderDialog()}
        </React.Fragment>
      );
    }
  }

  render() {
    return <div className="commuter-route">{this.renderSelectedRoute()}</div>;
  }
}
