/* @flow */
import React from 'react';

import OrganizationContainer from '../../containers/OrganizationContainer';
import CompanyContainer from '../../containers/CompanyContainer';
import CountryContainer from '../../containers/CountryContainer';
import CountryDefaultTaxTypeContainer from '../../containers/CountryDefaultTaxTypeContainer';
import UseFunctionContainer from '../../containers/UseFunctionContainer';
import CostCenterContainer from '../../containers/CostCenterContainer';
import DepartmentContainer from '../../containers/DepartmentContainer';
import EmployeeContainer from '../../containers/EmployeeContainer';
import ExpTypeGroupContainer from '../../containers/ExpTypeGroupContainer';
import ExtendedItemContainer from '../../containers/ExtendedItemContainer';
import CalendarContainer from '../../containers/CalendarContainer';
import JobTypeContainer from '../../containers/JobTypeContainer';
import JobContainer from '../../containers/JobContainer';
import MobileSettingContainer from '../../containers/MobileSettingContainer';
import LeaveContainer from '../../containers/LeaveContainer';
import LeaveOfAbsenceContainer from '../../containers/LeaveOfAbsenceContainer';
import ShortTimeWorkSettingContainer from '../../containers/ShortTimeWorkSettingContainer';
import ShortTimeWorkReasonContainer from '../../containers/ShortTimeWorkReasonContainer';
import WorkingTypeContainer from '../../containers/WorkingTypeContainer';
import AttPatternContainer from '../../containers/AttPatternContainer';
import AgreementAlertSettingContainer from '../../containers/AgreementAlertSettingContainer';
import AnnualPaidLeaveManagementContainer from '../../containers/AnnualPaidLeaveManagementContainer';
import CustomHintContainer from '../../containers/CustomHintContainer';
import ManagedLeaveManagementContainer from '../../containers/ManagedLeaveManagementContainer';
import ShortTimeWorkPeriodStatusContainer from '../../containers/ShortTimeWorkPeriodStatusContainer';
import LeaveOfAbsencePeriodStatusContainer from '../../containers/LeaveOfAbsencePeriodStatusContainer';
import AttPatternEmployeeBatchContainer from '../../containers/AttPatternEmployeeBatchContainer';
import TimeSettingContainer from '../../containers/TimeSettingContainer';
import WorkCategoryContainer from '../../containers/WorkCategoryContainer';
import PlannerSettingContainer from '../../containers/PlannerSettingContainer';
import ExpenseTypeContainer from '../../containers/ExpenseTypeContainer';
import TaxTypeContainer from '../../containers/TaxTypeContainer';
import CurrencyContainer from '../../containers/CurrencyContainer';
import ExpSettingContainer from '../../containers/ExpSettingContainer';
import ExchangeRateContainer from '../../containers/ExchangeRateContainer';
import AccountingPeriodContainer from '../../containers/AccountingPeriodContainer';
import ReportTypeContainer from '../../containers/ReportTypeContainer';
import PermissionContainer from '../../containers/PermissionContainer';
import VendorContainer from '../../containers/VendorContainer';
import GroupContainer from '../../containers/GroupContainer';

/**
 * メニューで選択された管理項目のコンテナーを返す
 * */

type Props = $Shape<{
  selectedKey: string,
}>;

export default class ContentsContainer extends React.Component<Props> {
  render() {
    switch (this.props.selectedKey) {
      // 組織設定
      case 'Organization':
        return <OrganizationContainer {...this.props} />;
      case 'Country':
        return <CountryContainer {...this.props} />;
      case 'Company':
        return <CompanyContainer {...this.props} />;
      case 'CountryDefaultTaxType':
        return <CountryDefaultTaxTypeContainer {...this.props} />;

      // 会社別設定
      case 'Use':
        return <UseFunctionContainer {...this.props} />;
      case 'CostCenter':
        return <CostCenterContainer {...this.props} />;
      case 'Department':
        return <DepartmentContainer {...this.props} />;
      case 'Emp':
        return <EmployeeContainer {...this.props} />;
      case 'ExpTypeGroup':
        return <ExpTypeGroupContainer {...this.props} />;
      case 'Calendar':
        return <CalendarContainer {...this.props} />;
      case 'JobType':
        return <JobTypeContainer {...this.props} />;
      case 'Job':
        return <JobContainer {...this.props} />;
      case 'Permission':
        return <PermissionContainer {...this.props} />;
      case 'MobileSetting':
        return <MobileSettingContainer {...this.props} />;
      case 'PlannerSetting':
        return <PlannerSettingContainer {...this.props} />;
      case 'Leave':
        return <LeaveContainer {...this.props} />;
      case 'LeaveOfAbsence':
        return <LeaveOfAbsenceContainer {...this.props} />;
      case 'ShortTimeWorkSetting':
        return <ShortTimeWorkSettingContainer {...this.props} />;
      case 'ShortTimeWorkReason':
        return <ShortTimeWorkReasonContainer {...this.props} />;
      case 'WorkScheme':
        return <WorkingTypeContainer {...this.props} />;
      // Master > Agreement Alert Setting (マスタ > 残業警告設定)
      case 'AttPattern':
        return <AttPatternContainer {...this.props} />;
      case 'ExtendedItem':
        return <ExtendedItemContainer {...this.props} />;
      case 'AgreementAlertSetting':
        return <AgreementAlertSettingContainer {...this.props} />;
      // Leave Management > Annual Paid Leave (休暇管理 > 年次有給休暇)
      case 'AnnualPaidLeaveManagement':
        return <AnnualPaidLeaveManagementContainer {...this.props} />;
      // Leave Management > Managed Leave (休暇管理 > 日数管理休暇)
      case 'ManagedLeaveManagement':
        return <ManagedLeaveManagementContainer {...this.props} />;
      case 'ShortTimeWorkPeriodStatus':
        return <ShortTimeWorkPeriodStatusContainer {...this.props} />;
      case 'LeaveOfAbsencePeriodStatus':
        return <LeaveOfAbsencePeriodStatusContainer {...this.props} />;
      case 'AttPatternEmployeeBatch':
        return <AttPatternEmployeeBatchContainer {...this.props} />;
      case 'TimeSetting':
        return <TimeSettingContainer {...this.props} />;
      case 'WorkCategory':
        return <WorkCategoryContainer {...this.props} />;
      case 'CustomHint':
        return <CustomHintContainer {...this.props} />;
      case 'ExpenseType':
        return <ExpenseTypeContainer {...this.props} />;
      case 'TaxType':
        return <TaxTypeContainer {...this.props} />;
      case 'Currency':
        return <CurrencyContainer {...this.props} />;
      case 'ExpSetting':
        return <ExpSettingContainer {...this.props} />;
      case 'ExchangeRate':
        return <ExchangeRateContainer {...this.props} />;
      case 'AccountingPeriod':
        return <AccountingPeriodContainer {...this.props} />;
      case 'ReportType':
        return <ReportTypeContainer {...this.props} />;
      case 'Vendor':
        return <VendorContainer {...this.props} />;
      case 'Group':
        return <GroupContainer {...this.props} />;
      default:
        return null;
    }
  }
}
