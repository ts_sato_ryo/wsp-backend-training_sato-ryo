// @flow

import React from 'react';
import _ from 'lodash';

import msg from '../../../commons/languages';
import tabType, { type TabType } from '../../../commons/constants/tabType';

import GlobalContainer from '../../../commons/containers/GlobalContainer';
import GlobalHeader from '../../../commons/components/GlobalHeader';
import Tab from '../../../commons/components/Tab';

import MenuPane from './MenuPane';
import ContentsSelectorContainer from '../../containers/ContentsSelectorContainer';
// import AdminUtil from '../../../commons/utils/AdminUtil';

import type { Permission } from '../../../domain/models/access-control/Permission';
import type { MenuItem, MenuSetting } from '../../constants/Setting/types';

import '../../../commons/styles/modal-transition-slideleft.css';
import './index.scss';

import ImgIconHeaderAdmin from '../../images/Admin.svg';
import ImgMenuIconOrganizationRequest from '../../images/menuIconOrganizationRequest.png';
import ImgMenuIconCompanyRequest from '../../images/menuIconCompanyRequest.png';

import orgSettings from '../../constants/Setting/orgSettings';
import companySettings from '../../constants/Setting/companySettings';

const ROOT = 'admin-pc';

type Props = {
  actions: { [string]: Function },
  userSetting: {
    companyId: string,
  },
  searchCompany: Array<{ [string]: * }>,
  selectedTab: TabType,
  hasPermission: ($Keys<Permission>[]) => boolean,
  onSelectMenuItem: (string) => void,
  onChangeCompany: (string) => void,
};

type State = {
  selectedKey: string,
  selectedCompanyId: string,
  title: string,
  selectedMenuFilter: string[],
};

/**
 * [Helper] メニュー項目を受け取り、再帰的に有効な項目のみ抽出して、フラットな配列として返却する
 * returns flatten valid menu items.
 */
const extractValidMenuItemsRecursively = (
  menuItem: MenuItem,
  hasPermission: ($Keys<Permission>[]) => boolean,
  useFunction: { [string]: * }
): MenuItem[] => {
  if (menuItem.objectName && !useFunction[menuItem.objectName]) {
    return [];
  }

  if (
    menuItem.requiredPermission &&
    !hasPermission(menuItem.requiredPermission)
  ) {
    return [];
  }

  return menuItem.childMenuList === undefined
    ? [menuItem]
    : menuItem.childMenuList.reduce(
        (acc, childMenuItem) =>
          acc.concat(
            extractValidMenuItemsRecursively(
              childMenuItem,
              hasPermission,
              useFunction
            )
          ),
        []
      );
};

/**
 * [Helper] メニュー設定を受け取って、有効な項目のみ抽出して返却する
 * returns flatten valid menu items.
 */
const extractValidMenuItemsFromMenuMenuSetting = (
  menuSetting: MenuSetting,
  hasPermission: ($Keys<Permission>[]) => boolean,
  useFunction: { [string]: * }
): MenuItem[] => {
  const menuItems: MenuItem[] = [];
  menuSetting.forEach((menuGroup) => {
    if (menuGroup.objectName && !useFunction[menuGroup.objectName]) {
      return;
    }

    if (
      menuGroup.requiredPermission &&
      !hasPermission(menuGroup.requiredPermission)
    ) {
      return;
    }

    menuGroup.menuList.forEach((menuItem) => {
      menuItems.push(
        ...extractValidMenuItemsRecursively(
          menuItem,
          hasPermission,
          useFunction
        )
      );
    });
  });
  return menuItems;
};

/**
 * 管理画面コンテンツ部
 * 管理画面の左ペインで選択された内容を元に各機能のコンテナーを作成して右ペインに描画する
 */
export default class Admin extends React.Component<Props, State> {
  onClickMenuItem: (
    event: ?SyntheticEvent<HTMLElement>,
    key: string,
    title: string,
    type: ?string
  ) => void;

  onChangeSelectedCompany: (SyntheticEvent<HTMLSelectElement>) => void;

  constructor(props: Props) {
    super(props);
    this.state = {
      selectedKey: '',
      selectedCompanyId: '',
      title: '',
      selectedMenuFilter: [],
    };

    this.onClickMenuItem = this.onClickMenuItem.bind(this);
    this.onChangeSelectedCompany = this.onChangeSelectedCompany.bind(this);
  }

  componentDidMount() {
    Promise.all([
      this.props.actions.getLanguagePickList(),
      this.props.actions.getUserSetting(),
      this.props.actions.getOrganizationSetting(),
      this.props.actions.searchCompany(),
    ]).then(() => {
      // FIXME: 先行する非同期処理の結果を、then内でthis.props経由に参照するのは適切でないはず
      if (
        this.props.userSetting.companyId &&
        this.detectDefaultMenuItem(companySettings) !== null
      ) {
        this.selectTab(tabType.ADMIN_COMPANY_REQUEST);
      } else {
        this.selectTab(tabType.ADMIN_ORGANIZATION_REQUEST);
      }
    });
  }

  onClickMenuItem(
    event: SyntheticEvent<HTMLElement>,
    key: string,
    title: string,
    type: ?string = null
  ) {
    if (this.state.selectedKey === key) {
      return;
    }
    // FIXME: モック用として先頭"a"から始まるものは標準画面に遷移する
    if ((key || '').substring(0, 1) === 'a') {
      if (window.sforce) {
        window.sforce.one.navigateToURL(`/${key}`);
      } else {
        window.location = `/${key}`;
      }
    }
    const currentTarget = event !== null ? event.currentTarget : undefined;
    const filter =
      currentTarget !== undefined ? currentTarget.dataset.filter : '';
    // FIXME Set both local state and redux state. This should be fixed.
    this.setState({
      selectedKey: key,
      title,
      selectedMenuFilter: filter.split(','),
    });
    this.props.onSelectMenuItem(key);

    if (type) {
      this.props.actions.selectTab(type);
    }
    this.props.actions.initializeEditRecord();
    this.props.actions.initializeDetailPane();
  }

  onChangeSelectedCompany(event: SyntheticEvent<HTMLSelectElement>) {
    let newCompanyId;
    if (event && event.currentTarget) {
      // FIXME Set both local state and redux state. This should be fixed.
      newCompanyId = event.currentTarget.value;
      this.setState({ selectedCompanyId: newCompanyId });
      this.props.onChangeCompany(event.currentTarget.value);
    }
    const useFunction =
      _.find(this.props.searchCompany, {
        id: newCompanyId,
      }) || {};
    this.state.selectedMenuFilter.map((item) => {
      if (useFunction[item] === false) {
        const defaultMenu = this.detectDefaultMenuItem(
          companySettings,
          useFunction
        );
        if (defaultMenu) {
          this.onClickMenuItem(
            null,
            defaultMenu.key,
            msg()[defaultMenu.name],
            tabType.ADMIN_COMPANY_REQUEST
          );
        }
      }
      return item;
    });
  }

  detectDefaultMenuItem(
    menuSetting: MenuSetting,
    nextUseFunction?: {
      [string]: *,
    }
  ): MenuItem | null {
    const validUseFunction =
      nextUseFunction ||
      this.props.searchCompany.find(
        (company) => company.id === this.state.selectedCompanyId
      ) ||
      {};

    const validMenuItems = extractValidMenuItemsFromMenuMenuSetting(
      menuSetting,
      this.props.hasPermission,
      validUseFunction
    );

    return (
      validMenuItems.find((menuItem) => menuItem.key === 'Emp') ||
      validMenuItems[0] ||
      null
    );
  }

  selectTab(type: ?TabType) {
    if (type === tabType.ADMIN_ORGANIZATION_REQUEST) {
      const defaultMenu = this.detectDefaultMenuItem(orgSettings);
      if (defaultMenu) {
        this.onClickMenuItem(
          null,
          defaultMenu.key,
          msg()[defaultMenu.name],
          type
        );
      }
    } else if (
      !this.state.selectedCompanyId ||
      !_.find(this.props.searchCompany, { id: this.state.selectedCompanyId })
    ) {
      let selectedCompanyId;
      if (
        this.props.userSetting &&
        this.props.userSetting.companyId &&
        _.find(this.props.searchCompany, {
          id: this.props.userSetting.companyId,
        })
      ) {
        selectedCompanyId = this.props.userSetting.companyId;
      } else {
        selectedCompanyId = this.props.searchCompany[0].id;
      }

      this.props.onChangeCompany(selectedCompanyId);

      const defaultMenu = this.detectDefaultMenuItem(companySettings);
      this.setState({ selectedCompanyId }, () => {
        if (defaultMenu) {
          this.onClickMenuItem(
            null,
            defaultMenu.key,
            msg()[defaultMenu.name],
            type
          );
        }
      });
    } else {
      const defaultMenu = this.detectDefaultMenuItem(companySettings);
      if (defaultMenu) {
        this.onClickMenuItem(
          null,
          defaultMenu.key,
          msg()[defaultMenu.name],
          type
        );
      }
    }
  }

  renderHeaderContent() {
    if (_.isEmpty(this.props.searchCompany)) {
      return null;
    }

    const tabItemList = [];

    if (this.props.hasPermission(['manageOverallSetting'])) {
      tabItemList.push({
        icon: ImgMenuIconOrganizationRequest,
        label: msg().Admin_Lbl_Organization,
        onSelect: () => this.selectTab(tabType.ADMIN_ORGANIZATION_REQUEST),
        type: tabType.ADMIN_ORGANIZATION_REQUEST,
      });
    }

    tabItemList.push({
      icon: ImgMenuIconCompanyRequest,
      label: msg().Admin_Lbl_Tab_Company,
      onSelect: () => this.selectTab(tabType.ADMIN_COMPANY_REQUEST),
      type: tabType.ADMIN_COMPANY_REQUEST,
    });

    return (
      <div className={`${ROOT}__header slds`}>
        <div className="slds-align-middle">
          {tabItemList.map((item, i) => {
            return (
              <Tab
                icon={item.icon}
                key={i}
                label={item.label}
                onSelect={item.onSelect}
                selected={this.props.selectedTab === item.type}
              />
            );
          })}
        </div>
      </div>
    );
  }

  renderMainContents(selectedCompanyId: string) {
    if (_.isEmpty(this.props.userSetting)) {
      return null;
    }
    const useFunction =
      _.find(this.props.searchCompany, { id: this.state.selectedCompanyId }) ||
      {};

    return (
      <ContentsSelectorContainer
        key="main"
        companyId={selectedCompanyId}
        selectedKey={this.state.selectedKey}
        title={this.state.title}
        useFunction={useFunction}
      />
    );
  }

  render() {
    let menuGroupList = {};
    let selectedCompanyId = '';
    if (this.props.selectedTab === tabType.ADMIN_COMPANY_REQUEST) {
      menuGroupList = companySettings;
      selectedCompanyId = this.state.selectedCompanyId;
    } else {
      menuGroupList = orgSettings;
    }

    return (
      <GlobalContainer>
        <GlobalHeader
          content={this.renderHeaderContent()}
          iconAssistiveText={msg().Admin_Lbl_ManagementScreen}
          iconSrc={ImgIconHeaderAdmin}
          iconSrcType="svg"
          showPersonalMenuPopoverButton={false}
          showProxyIndicator={false}
        />
        <div className={`${ROOT}`}>
          {this.props.selectedTab !== tabType.NONE
            ? [
                <MenuPane
                  key="menu"
                  searchCompany={this.props.searchCompany}
                  selectedCompanyId={selectedCompanyId}
                  selectedKey={this.state.selectedKey}
                  isCompany={
                    this.props.selectedTab === tabType.ADMIN_COMPANY_REQUEST
                  }
                  menuGroupList={menuGroupList}
                  hasPermission={this.props.hasPermission}
                  onChangeSelectedCompany={this.onChangeSelectedCompany}
                  onClickMenuItem={this.onClickMenuItem}
                />,
                this.renderMainContents(selectedCompanyId),
              ]
            : null}
        </div>
      </GlobalContainer>
    );
  }
}
