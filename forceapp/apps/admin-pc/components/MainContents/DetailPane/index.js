import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
import DateUtil from '../../../../commons/utils/DateUtil';

import msg from '../../../../commons/languages';
import fieldType from '../../../constants/fieldType';

import * as RecordUtil from '../../../utils/RecordUtil';

import DetailPaneHeader, { DetailPaneButtonsHeader } from './DetailPaneHeader';
import DetailPaneBody from './DetailPaneBody';
import DetailDialog from './DetailDialog';

import Button from '../../../../commons/components/buttons/Button';
import SelectField from '../../../../commons/components/fields/SelectField';

import './index.scss';

const ROOT = 'admin-pc-contents-detail-pane';

const DetailPaneHeaderCtrl = (props) => {
  const {
    modeBase,
    modeHistory,
    onClickUpdateBaseButton,
    onClickUpdateHistoryButton,
    onClickCreateHistoryButton,
    ...newProps
  } = props;
  const mode = modeBase || modeHistory;
  const title = mode ? msg().Admin_Lbl_Editing : msg().Admin_Lbl_View;
  let onClickUpdateButton = null;

  if (modeBase === 'edit') {
    onClickUpdateButton = onClickUpdateBaseButton;
  } else if (modeHistory === 'edit') {
    onClickUpdateButton = onClickUpdateHistoryButton;
  } else if (modeHistory === 'revision') {
    onClickUpdateButton = onClickCreateHistoryButton;
  }

  return (
    <DetailPaneButtonsHeader
      {...newProps}
      mode={mode}
      title={title}
      isDisplayUpdateButton
      onClickUpdateButton={onClickUpdateButton}
    />
  );
};

const createDetailPaneHeaderSub = (Component) =>
  class DetailPaneHeaderSub extends React.Component {
    isHideButtons() {
      return this.props.modeBase || this.props.modeHistory;
    }

    render() {
      return (
        <DetailPaneHeader
          title={this.props.title}
          className={this.props.className}
        >
          {!this.isHideButtons() && <Component {...this.props} />}
        </DetailPaneHeader>
      );
    }
  };

const DetailPaneHeaderSubBase = createDetailPaneHeaderSub((props) => {
  return (
    <div>
      {!props.isSinglePane && (
        <Button type="destructive" onClick={props.onClickDeleteButton}>
          {msg().Com_Btn_Delete}
        </Button>
      )}
      <Button onClick={props.onClickEditButton}>{msg().Admin_Lbl_Edit}</Button>
    </div>
  );
});

const DetailPaneHeaderSubHistory = createDetailPaneHeaderSub((props) => {
  let selectedNewestHistory = false;
  let disableDeleteButton = false;
  if (props.searchHistory.length > 0) {
    selectedNewestHistory = props.currentHistory === props.searchHistory[0].id;
    disableDeleteButton =
      !selectedNewestHistory || props.searchHistory.length === 1;
  } else {
    disableDeleteButton = true;
  }

  let historyList = null;

  if (props.id !== '') {
    const options =
      props.modeHistory === 'revision'
        ? []
        : props.searchHistory.map((history) => ({
            text: DateUtil.customFormat(history.validDateFrom, 'L') || '',
            value: history.id,
          }));

    const disabled = props.modeHistory !== '';

    historyList = (
      <div className={`${ROOT}__history-select_wrapper`}>
        <div
          className={`${ROOT}__history-select_title`}
          style={{ display: 'inline-block' }}
        >
          {msg().Admin_Lbl_Selected}：
        </div>
        <SelectField
          className={`${ROOT}__history-select`}
          onChange={(e) => {
            props.onChangeHistory(e.target.value);
          }}
          options={options}
          value={props.currentHistory}
          disabled={disabled}
        />
      </div>
    );
  }

  return (
    <div>
      {historyList}
      <Button
        type="destructive"
        disabled={disableDeleteButton}
        onClick={props.onClickDeleteHistoryButton}
      >
        {msg().Com_Btn_Delete}
      </Button>
      <Button onClick={props.onClickRevisionButton}>
        {msg().Admin_Lbl_Revision}
      </Button>
    </div>
  );
});

export default class DetailPane extends React.Component {
  static propTypes = {
    configList: PropTypes.object.isRequired,
    currentHistory: PropTypes.string.isRequired,
    editRecord: PropTypes.object.isRequired,
    editRecordHistory: PropTypes.object.isRequired,
    getOrganizationSetting: PropTypes.object.isRequired,
    isSinglePane: PropTypes.bool.isRequired,
    isShowDialog: PropTypes.bool.isRequired,
    onChangeDetailItem: PropTypes.func.isRequired,
    onChangeDetailItemHistory: PropTypes.func.isRequired,
    onChangeHistory: PropTypes.func.isRequired,
    onClickCancelButton: PropTypes.func.isRequired,
    onClickCancelEditButton: PropTypes.func.isRequired,
    onClickEditDetailButton: PropTypes.func.isRequired,
    onClickRevisionButton: PropTypes.func.isRequired,
    onClickDeleteButton: PropTypes.func.isRequired,
    onClickDeleteHistoryButton: PropTypes.func.isRequired,
    onClickSaveButton: PropTypes.func.isRequired,
    onClickCreateHistoryButton: PropTypes.func.isRequired,
    onClickUpdateButton: PropTypes.func.isRequired,
    onClickUpdateHistoryButton: PropTypes.func.isRequired,
    onClickRevisionStartButton: PropTypes.func.isRequired,
    searchHistory: PropTypes.array.isRequired,
    sfObjFieldValues: PropTypes.object.isRequired,
    tmpEditRecord: PropTypes.object.isRequired,
    tmpEditRecordHistory: PropTypes.object.isRequired,
    modeBase: PropTypes.string.isRequired,
    modeHistory: PropTypes.string.isRequired,
    renderDetailExtraArea: PropTypes.func,
    title: PropTypes.string,
    useFunction: PropTypes.object.isRequired,
  };

  static defaultProps = {
    title: '',
    renderDetailExtraArea: null,
  };

  constructor(props) {
    super(props);
    this.state = { checkboxes: {} };
    this.baseValueGetter = this.getBaseValue.bind(this);
    this.historyValueGetter = this.getHistoryValue.bind(this);
    this.onChangeCheckBox = this.onChangeCheckBox.bind(this);
    this.onChangeCheckBoxHistory = this.onChangeCheckBoxHistory.bind(this);
    this.onClickCancelEditButton = this.onClickCancelEditButton.bind(this);
  }

  componentWillMount() {
    this.setCheckBoxes(this.props.editRecord, this.props.configList.base);
    this.setCheckBoxes(
      this.props.editRecordHistory,
      this.props.configList.history
    );
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.editRecord.id !== nextProps.editRecord.id) {
      this.setCheckBoxes(nextProps.editRecord, nextProps.configList.base);
    }
    if (this.props.editRecordHistory.id !== nextProps.editRecordHistory.id) {
      this.setCheckBoxes(
        nextProps.editRecordHistory,
        nextProps.configList.history
      );
    }
  }

  onChangeCheckBox(e, key) {
    const isChecked = e.target.checked;
    this.setState((prevState) => {
      const checkboxes = prevState.checkboxes;
      checkboxes[key] = isChecked;
      return { checkboxes };
    });
    this.props.onChangeDetailItem(key, e.target.checked);
  }

  onChangeCheckBoxHistory(e, key) {
    const isChecked = e.target.checked;
    this.setState((prevState) => {
      const checkboxes = prevState.checkboxes;
      checkboxes[key] = isChecked;
      return { checkboxes };
    });
    this.props.onChangeDetailItemHistory(key, e.target.checked);
  }

  onClickCancelEditButton() {
    this.setCheckBoxes(this.props.editRecord, this.props.configList.base);
    this.setCheckBoxes(
      this.props.editRecordHistory,
      this.props.configList.history
    );
    this.props.onClickCancelEditButton();
  }

  getBaseValue(key) {
    return RecordUtil.getter(this.props.tmpEditRecord)(key);
  }

  getHistoryValue(key) {
    return RecordUtil.getter(this.props.tmpEditRecordHistory)(key);
  }

  setCheckBoxes(editRecord, configList) {
    if (configList) {
      this.setState((prevState) => {
        const checkboxes = _.cloneDeep(prevState.checkboxes);

        const evalConfigTree = (targetConfigList) => {
          targetConfigList.forEach((config) => {
            if (config.section) {
              evalConfigTree(config.configList);
            } else if (
              config.type === fieldType.FIELD_CHECKBOX ||
              config.type === fieldType.FIELD_REPLACEMENT_LEAVE_OF_HOLIDAY_WORK
            ) {
              checkboxes[config.key] = editRecord[config.key] || false;
            }
          });
        };
        evalConfigTree(configList);

        return { checkboxes };
      });
    }
  }

  renderHr() {
    if (this.props.searchHistory.length < 1) {
      return null;
    }
    return <hr className={`${ROOT}__hr`} />;
  }

  renderRevisionList() {
    if (this.props.searchHistory.length < 1) {
      return null;
    }

    return (
      <ul className={`${ROOT}__revision`}>
        <li className={`${ROOT}__revision-title`}>
          <div className={`${ROOT}__revision-title__date`}>
            {msg().Admin_Lbl_RevisionDate}
          </div>
          <div className={`${ROOT}__revision-title__comment`}>
            {msg().Admin_Lbl_ReasonForRevision}
          </div>
        </li>
        {this.props.searchHistory.map((history) => {
          return (
            <li key={history.id} className={`${ROOT}__revision-item`}>
              <div className={`${ROOT}__revision-item__date`}>
                {DateUtil.customFormat(history.validDateFrom, 'L')}
              </div>
              <div className={`${ROOT}__revision-item__comment`}>
                {history.comment}
              </div>
            </li>
          );
        })}
      </ul>
    );
  }

  renderDetailDialog() {
    if (!this.props.isShowDialog) {
      return null;
    }

    return (
      <DetailDialog
        onChangeDetailItem={this.props.onChangeDetailItemHistory}
        onClickCancelEditButton={this.props.onClickCancelEditButton}
        onClickRevisionStartButton={this.props.onClickRevisionStartButton}
        title={msg().Admin_Lbl_Revision}
        tmpEditRecord={this.props.tmpEditRecordHistory}
      />
    );
  }

  renderHistoryArea() {
    if (!this.props.configList.history) {
      return null;
    }

    return (
      <div>
        <DetailPaneHeaderSubHistory
          id={this.props.editRecord.id}
          modeBase={this.props.modeBase}
          modeHistory={this.props.modeHistory}
          onClickDeleteHistoryButton={this.props.onClickDeleteHistoryButton}
          onClickRevisionButton={this.props.onClickRevisionButton}
          onChangeHistory={this.props.onChangeHistory}
          currentHistory={this.props.currentHistory}
          searchHistory={this.props.searchHistory}
          title={msg().Admin_Lbl_HistoryProperties}
          className={[`${ROOT}__header-sub`]}
        />
        <DetailPaneBody
          checkboxes={this.state.checkboxes}
          configList={this.props.configList.history}
          baseValueGetter={this.baseValueGetter}
          historyValueGetter={this.historyValueGetter}
          disabled={this.props.modeHistory === ''}
          mode={this.props.modeHistory}
          getOrganizationSetting={this.props.getOrganizationSetting}
          onChangeCheckBox={this.onChangeCheckBoxHistory}
          onChangeDetailItem={this.props.onChangeDetailItemHistory}
          sfObjFieldValues={this.props.sfObjFieldValues}
          tmpEditRecord={this.props.tmpEditRecordHistory}
          tmpEditRecordBase={this.props.tmpEditRecord}
          useFunction={this.props.useFunction}
        />
        {this.renderHr()}
        {this.renderRevisionList()}
      </div>
    );
  }

  render() {
    const title = this.props.configList.history ? msg().Admin_Lbl_Base : '';
    return (
      <div className={ROOT}>
        {this.props.configList.history ? (
          /*
           * このヘッダーバーのボタンの左端マージンが
           * 他のヘッダーバーのボタンと同一でないという既知の問題があります。
           * しかし、スクロールバーの幅によるズレなので、
           * OS、ブラウザ、スクロールバーが無い場合で変化してしまいます。
           * 対応が難しいので対応不要ということにしております。
           */
          <DetailPaneHeaderCtrl
            isSinglePane={this.props.isSinglePane}
            modeBase={this.props.modeBase}
            modeHistory={this.props.modeHistory}
            onClickCloseButton={this.props.onClickCancelButton}
            onClickCancelButton={this.onClickCancelEditButton}
            onClickSaveButton={this.props.onClickSaveButton}
            onClickUpdateBaseButton={this.props.onClickUpdateButton}
            onClickCreateHistoryButton={this.props.onClickCreateHistoryButton}
            onClickUpdateHistoryButton={this.props.onClickUpdateHistoryButton}
          />
        ) : (
          <DetailPaneButtonsHeader
            isSinglePane={this.props.isSinglePane}
            mode={this.props.modeBase}
            title={this.props.title || title}
            onClickDeleteButton={this.props.onClickDeleteButton}
            onClickEditButton={this.props.onClickEditDetailButton}
            onClickCloseButton={this.props.onClickCancelButton}
            onClickCancelButton={this.onClickCancelEditButton}
            onClickSaveButton={this.props.onClickSaveButton}
            onClickUpdateButton={this.props.onClickUpdateButton}
          />
        )}
        <div className={`${ROOT}__scrollable`}>
          {this.props.configList.history && (
            <DetailPaneHeaderSubBase
              isSinglePane={this.props.isSinglePane}
              modeBase={this.props.modeBase}
              modeHistory={this.props.modeHistory}
              onClickDeleteButton={this.props.onClickDeleteButton}
              onClickEditButton={this.props.onClickEditDetailButton}
              className={[`${ROOT}__header-sub`]}
              title={msg().Admin_Lbl_BaseInfo}
            />
          )}
          <DetailPaneBody
            mode={this.props.modeBase}
            checkboxes={this.state.checkboxes}
            configList={this.props.configList.base}
            baseValueGetter={this.baseValueGetter}
            historyValueGetter={this.historyValueGetter}
            disabled={!this.props.modeBase}
            getOrganizationSetting={this.props.getOrganizationSetting}
            onChangeCheckBox={this.onChangeCheckBox}
            onChangeDetailItem={this.props.onChangeDetailItem}
            sfObjFieldValues={this.props.sfObjFieldValues}
            tmpEditRecord={this.props.tmpEditRecord}
            renderDetailExtraArea={this.props.renderDetailExtraArea}
            useFunction={this.props.useFunction}
          />
          {this.renderHistoryArea()}
          {this.renderDetailDialog()}
        </div>
      </div>
    );
  }
}
