// @flow
import * as React from 'react';
import classNames from 'classnames';
import './DetailPaneHeader.scss';
import msg from '../../../../commons/languages';
import Button from '../../../../commons/components/buttons/Button';
import { compose } from '../../../../commons/utils/FnUtil';

const ROOT = 'admin-pc-contents-detail-pane__header';

type DetailPaneHeaderProps = {
  title?: string,
  className?: string,
  children?: React.Node,
};

export default function DetailPaneHeader(props: DetailPaneHeaderProps) {
  return (
    <div className={classNames(ROOT, props.className)}>
      <div className={`${ROOT}-content slds-align-middle`}>
        <div className={`${ROOT}-content__title slds-align-middle`}>
          {props.title || ''}
        </div>
        <div className={`${ROOT}-content__button-area slds-align-middle`}>
          {props.children}
        </div>
      </div>
    </div>
  );
}

type ButtonEventProps = {
  onClickCancelButton?: ?(*) => *,
  onClickCloseButton?: ?(*) => *,
  onClickDeleteButton?: ?(*) => *,
  onClickEditButton?: ?(*) => *,
  onClickSaveButton?: ?(*) => *,
  onClickUpdateButton?: ?(*) => *,
};

type ButtonsProps = {
  isDeleteButtonDisabled?: boolean,
} & ButtonEventProps;

const Buttons: React.StatelessFunctionalComponent<ButtonsProps> = (props) => {
  return (
    <React.Fragment>
      {props.onClickDeleteButton && (
        <Button
          type="destructive"
          disabled={props.isDeleteButtonDisabled}
          onClick={props.onClickDeleteButton}
        >
          {msg().Com_Btn_Delete}
        </Button>
      )}
      {props.onClickEditButton && (
        <Button onClick={props.onClickEditButton}>
          {msg().Admin_Lbl_Edit}
        </Button>
      )}
      {props.onClickCloseButton && (
        <Button onClick={props.onClickCloseButton}>
          {msg().Com_Btn_Close}
        </Button>
      )}
      {props.onClickCancelButton && (
        <Button onClick={props.onClickCancelButton}>
          {msg().Com_Btn_Cancel}
        </Button>
      )}
      {props.onClickSaveButton && (
        <Button type="primary" onClick={props.onClickSaveButton}>
          {msg().Com_Btn_Submit}
        </Button>
      )}
      {props.onClickUpdateButton && (
        <Button type="primary" onClick={props.onClickUpdateButton}>
          {msg().Com_Btn_Save}
        </Button>
      )}
    </React.Fragment>
  );
};

type ButtonStatusProps = {
  mode: string,
  isSinglePane?: boolean,
  isDisplayCancelButton?: boolean,
  isDisplayCloseButton?: boolean,
  isDisplayDeleteButton?: boolean,
  isDisplayEditButton?: boolean,
  isDisplaySaveButton?: boolean,
  isDisplayUpdateButton?: boolean,
};

const bindButtonEvents = (WrappedComponent: React.ComponentType<*>) => {
  return class BindButtonEvents extends React.Component<
    ButtonStatusProps & ButtonEventProps
  > {
    render() {
      const {
        isDisplayCancelButton,
        isDisplayCloseButton,
        isDisplayDeleteButton,
        isDisplayEditButton,
        isDisplaySaveButton,
        isDisplayUpdateButton,
        onClickCancelButton,
        onClickCloseButton,
        onClickDeleteButton,
        onClickEditButton,
        onClickSaveButton,
        onClickUpdateButton,
        ...newProps
      } = this.props;

      return (
        <WrappedComponent
          {...newProps}
          onClickCancelButton={
            isDisplayCancelButton ? onClickCancelButton : null
          }
          onClickCloseButton={isDisplayCloseButton ? onClickCloseButton : null}
          onClickDeleteButton={
            isDisplayDeleteButton ? onClickDeleteButton : null
          }
          onClickEditButton={isDisplayEditButton ? onClickEditButton : null}
          onClickSaveButton={isDisplaySaveButton ? onClickSaveButton : null}
          onClickUpdateButton={
            isDisplayUpdateButton ? onClickUpdateButton : null
          }
        />
      );
    }
  };
};

const withDefaultButtonStatus = (WrappedComponent: React.ComponentType<*>) => {
  return class WithDefaultButtonStatus extends React.Component<ButtonStatusProps> {
    render() {
      const { mode, isSinglePane } = this.props;
      if (isSinglePane) {
        return (
          <WrappedComponent
            isDisplayCancelButton={mode === 'edit'}
            isDisplayCloseButton={false}
            isDisplayDeleteButton={false}
            isDisplayEditButton={!mode}
            isDisplaySaveButton={false}
            isDisplayUpdateButton={mode === 'edit'}
            {...this.props} // 既存の値でデフォルト値を上書きします。上記の値はあくまでもデフォルト値です。
          />
        );
      } else {
        return (
          <WrappedComponent
            isDisplayCancelButton={!!mode && mode !== 'new'}
            isDisplayCloseButton={!mode || mode === 'new'}
            isDisplayDeleteButton={!mode}
            isDisplayEditButton={!mode}
            isDisplaySaveButton={mode === 'new'}
            isDisplayUpdateButton={mode === 'edit'}
            {...this.props} // 既存の値でデフォルト値を上書きします。上記の値はあくまでもデフォルト値です。
          />
        );
      }
    }
  };
};

const ButtonHeader = (
  props: DetailPaneHeaderProps & ButtonsProps & ButtonStatusProps
) => {
  const { title, className, ...newProps } = props;
  return (
    <DetailPaneHeader title={title} className={className}>
      <Buttons {...newProps} />
    </DetailPaneHeader>
  );
};

export const DetailPaneButtonsHeader = (compose(
  withDefaultButtonStatus,
  bindButtonEvents
)(ButtonHeader): React.ComponentType<Object>);
