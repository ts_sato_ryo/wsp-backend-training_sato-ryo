import React from 'react';
import PropTypes from 'prop-types';

import IconButton from '../../../../commons/components/buttons/IconButton';
import btnOpen from '../../../images/btnOpen.png';
import btnClose from '../../../images/btnClose.png';

import './DetailSection.scss';

const ROOT = 'admin-pc-main-contents-detail-pane-detail-section';

export default class DetailSection extends React.Component {
  static propTypes = {
    sectionKey: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    isClosed: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
    isExpandable: PropTypes.bool,
    description: PropTypes.string,
    onClickToggleButton: PropTypes.func,
  };

  static defaultProps = {
    isExpandable: false,
    description: null,
    onClickToggleButton: () => {},
  };

  render() {
    const description = this.props.description ? (
      <div key="description" className={`${ROOT}__description`}>
        {this.props.description}
      </div>
    ) : null;

    return (
      <li key={this.props.sectionKey} className={ROOT}>
        <div className={`${ROOT}__header`}>
          {this.props.isExpandable ? (
            <div className={`${ROOT}__toggle-button`}>
              <IconButton
                src={this.props.isClosed ? btnClose : btnOpen}
                onClick={this.props.onClickToggleButton}
              />
            </div>
          ) : null}
          <div className={`${ROOT}__title`}>{this.props.title}</div>
        </div>
        {this.props.isExpandable && this.props.isClosed
          ? null
          : [description, this.props.children]}
      </li>
    );
  }
}
