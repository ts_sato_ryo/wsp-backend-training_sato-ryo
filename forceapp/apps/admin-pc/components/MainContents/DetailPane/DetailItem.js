import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
import uuidV4 from 'uuid/v4';

import msg from '../../../../commons/languages';

import * as ConfigUtil from '../../../utils/ConfigUtil';

import displayType from '../../../constants/displayType';
import fieldSize from '../../../constants/fieldSize';
import fieldType from '../../../constants/fieldType';

import DateField from '../../../../commons/components/fields/DateField';
import Label from '../../../../commons/components/fields/Label';
import SelectField from '../../../../commons/components/fields/SelectField';
import RadioGroupField, {
  LAYOUT_TYPE as RADIO_GROUP_FIELD_LAYOUT_TYPE,
} from '../../../../commons/components/fields/RadioGroupField';
import TextAreaField from '../../../../commons/components/fields/TextAreaField';
import TextField from '../../../../commons/components/fields/TextField';
import AttTimeField from '../../../../commons/components/fields/AttTimeField';
import AttTimeRangeField from '../../../../commons/components/fields/AttTimeRangeField';

import AutoSuggestTextField from '../../AutoSuggestTextField';

import TimeUtil from '../../../../commons/utils/TimeUtil';

import './DetailItem.scss';

const ROOT = 'admin-pc-contents-detail-pane__body__item-list';

export default class DetailItem extends React.Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    baseValueGetter: PropTypes.func.isRequired,
    historyValueGetter: PropTypes.func.isRequired,
    getOrganizationSetting: PropTypes.object.isRequired,
    onChangeCheckBox: PropTypes.func,
    onChangeDetailItem: PropTypes.func.isRequired,
    sfObjFieldValues: PropTypes.object,
    tmpEditRecord: PropTypes.object.isRequired,
    useFunction: PropTypes.object.isRequired,
    disabled: PropTypes.bool,
    disabledValidDateTo: PropTypes.bool,
  };

  static defaultProps = {
    disabled: false,
    getOrganizationSetting: {},
    onChangeCheckBox: () => {},
    sfObjFieldValues: {},
    disabledValidDateTo: false,
  };

  static makeOptions(props) {
    const { config, tmpEditRecord, sfObjFieldValues } = props;
    let options = {};

    if (config.options) {
      options = config.options.map((option) => ({
        text: msg()[option.msgkey],
        value: option.value,
      }));
    } else {
      options = [...(sfObjFieldValues[config.props] || [])]
        .filter((settingItem) => {
          return (
            !config.ignoreItself ||
            (settingItem.value !== tmpEditRecord.id &&
              settingItem.value !== tmpEditRecord.baseId)
          );
        })
        .map((settingItem) => {
          if (config.multiLanguageValue) {
            return {
              text: msg()[settingItem.label],
              value: settingItem.value,
            };
          } else {
            return { text: settingItem.label, value: settingItem.value };
          }
        });
    }

    // 必須でない場合、セレクトボックスの先頭の要素は空白とする
    if (
      config.type === fieldType.FIELD_SELECT_WITH_PLACEHOLDER ||
      (!config.isRequired && !config.multiple)
    ) {
      const firstOption = _.first(options);
      const selectLabel = msg().Admin_Lbl_PleaseSelect;
      if (firstOption && firstOption.text !== selectLabel) {
        options.unshift({ text: selectLabel, value: '' });
      }
    }

    return options;
  }

  static getDerivedStateFromProps(nextProps, state) {
    return {
      uuid: state.uuidV4,
      options: DetailItem.makeOptions(nextProps),
    };
  }

  constructor(props) {
    super(props);
    this.state = DetailItem.getDerivedStateFromProps(props, { uuid: uuidV4() });
  }

  // FIXME: We can remove this method by fixed GENIE-6044
  // However, Admin-pc is affected all around it.
  // This big change needs carefully fix.
  // ---
  // このメソッドは GENIE-6044 の修正により削除できます。
  // しかし、管理画面の全てに影響します。
  // 変更を行う場合は慎重にしてください。
  componentDidUpdate() {
    const { config, tmpEditRecord, onChangeDetailItem } = this.props;

    switch (config.type) {
      case fieldType.FIELD_SELECT:
      case fieldType.FIELD_SELECT_WITH_PLACEHOLDER: {
        // 上流のステートを更新する
        // getDerivedStateFromProps/UNSAFE_componentWillReceiveProps は上流のステートを使ってローカルステートを更新するようなので
        // 上流のステートの更新には使えない。
        // 仕方ないので、renderが呼ばれた後に対処する。
        //
        // !!onChangeDetailItemに同じ値を毎回セットすると無限ループになるため注意すること!!
        if (
          config.isRequired &&
          _.isEmpty(tmpEditRecord[config.key]) &&
          this.state.options.length > 0 &&
          this.state.options[0].value !== tmpEditRecord[config.key] &&
          !config.multiple
        ) {
          // 勤務体系の曜日ごとの日タイプ選択でしか使われていない？
          onChangeDetailItem(config.key, this.state.options[0].value);
        }
        break;
      }
      default:
        break;
    }
  }

  parseMinute(value) {
    if (value === 0 || value === '0') {
      return 0;
    } else {
      return parseInt(value) || '';
    }
  }

  renderCheckboxMessage(config) {
    if (_.isEmpty(config.label)) {
      return null;
    }

    return (
      <span className={`${ROOT}__item__checkbox-message`}>
        {msg()[config.label]}
      </span>
    );
  }

  renderAttTimeField(key) {
    const { config, onChangeDetailItem, tmpEditRecord, disabled } = this.props;
    const timeValue = this.parseMinute(tmpEditRecord[key]);

    return (
      <AttTimeField
        disabled={disabled}
        onBlur={(value) => {
          onChangeDetailItem(key, String(TimeUtil.toMinutes(value)));
        }}
        value={TimeUtil.toHHmm(timeValue)}
        required={config.isRequired}
      />
    );
  }

  renderAttTimeRangeField(key, subkey) {
    const { uuid } = this.state;
    const { config, onChangeDetailItem, tmpEditRecord, disabled } = this.props;
    const startTimeValue = this.parseMinute(tmpEditRecord[key]);
    const endTimeValue = this.parseMinute(tmpEditRecord[subkey]);

    return (
      <AttTimeRangeField
        key={`${uuid}${key}`}
        onBlurAtStart={(value) => {
          onChangeDetailItem(key, String(TimeUtil.toMinutes(value)));
        }}
        onBlurAtEnd={(value) => {
          onChangeDetailItem(subkey, String(TimeUtil.toMinutes(value)));
        }}
        startTime={TimeUtil.toHHmm(startTimeValue)}
        endTime={TimeUtil.toHHmm(endTimeValue)}
        required={config.isRequired}
        disabled={disabled}
      />
    );
  }

  renderCheckbox(key, disabled, label) {
    return (
      <label>
        <input
          type="checkbox"
          disabled={disabled}
          onChange={(event) => {
            event.persist();
            return this.props.onChangeCheckBox(event, key);
          }}
          checked={this.props.tmpEditRecord[key] || false}
        />
        {this.renderCheckboxMessage({ label })}
      </label>
    );
  }

  renderReplacementLeaveOfHolidayWork() {
    return (
      <div>
        {this.renderCheckbox(
          'useSubstituteLeave',
          this.props.disabled,
          'Att_Lbl_Substitute'
        )}
      </div>
    );
  }

  renderDetailItemField() {
    const { uuid } = this.state;
    const {
      config,
      tmpEditRecord,
      onChangeDetailItem,
      sfObjFieldValues,
    } = this.props;
    switch (config.type) {
      case fieldType.FIELD_AUTOSUGGEST_TEXT:
        const suggestList = (sfObjFieldValues[config.props] || []).map(
          (settingItem) => {
            const suggest = {};
            for (let i = 0; i < config.autoSuggest.suggestionKey.length; i++) {
              const key = config.autoSuggest.suggestionKey[i];
              suggest[key] = settingItem[key];
            }
            return suggest;
          }
        );
        return (
          <AutoSuggestTextField
            key={`${uuid}${config.key}`}
            onBlur={(e, value) => {
              onChangeDetailItem(config.key, value);
            }}
            disabled={this.props.disabled}
            value={tmpEditRecord[config.key]}
            configKey={config.key}
            required={config.isRequired}
            suggestList={suggestList}
            suggestConfig={config.autoSuggest}
          />
        );
      case fieldType.FIELD_VALID_DATE:
        return (
          <div required={config.isRequired}>
            <DateField
              key={`${uuid}${config.key}1`}
              className={`${ROOT}__item-date ts-text-field slds-input`}
              disabled={this.props.disabled}
              onChange={(value) => {
                onChangeDetailItem('validDateFrom', value);
              }}
              value={tmpEditRecord.validDateFrom}
              required={config.isRequired}
            />
            {` – `}
            <DateField
              key={`${uuid}${config.key}2`}
              className={`${ROOT}__item-date ts-text-field slds-input`}
              disabled={this.props.disabled || this.props.disabledValidDateTo}
              onChange={(value) => {
                onChangeDetailItem('validDateTo', value);
              }}
              value={tmpEditRecord.validDateTo}
            />
          </div>
        );
      case fieldType.FIELD_USER_NAME:
        return (
          <div required={config.isRequired}>
            <TextField
              key={`${uuid}${config.key}1`}
              className={`${ROOT}__item-name`}
              disabled={this.props.disabled}
              onChange={(e) => {
                onChangeDetailItem(
                  `lastName_${config.ltype}`,
                  e.target.value,
                  config.charType
                );
              }}
              placeholder={msg().Admin_Lbl_LastName}
              required={config.isRequired}
              value={tmpEditRecord[`lastName_${config.ltype}`]}
            />
            <TextField
              key={`${uuid}${config.key}2`}
              className={`${ROOT}__item-name`}
              disabled={this.props.disabled}
              onChange={(e) => {
                onChangeDetailItem(
                  `firstName_${config.ltype}`,
                  e.target.value,
                  config.charType
                );
              }}
              placeholder={msg().Admin_Lbl_FirstName}
              value={tmpEditRecord[`firstName_${config.ltype}`]}
            />
            <TextField
              key={`${uuid}${config.key}3`}
              className={`${ROOT}__item-name`}
              disabled={this.props.disabled}
              onChange={(e) => {
                onChangeDetailItem(
                  `middleName_${config.ltype}`,
                  e.target.value,
                  config.charType
                );
              }}
              placeholder={msg().Admin_Lbl_MiddleName}
              value={tmpEditRecord[`middleName_${config.ltype}`]}
              required={config.isRequired}
            />
          </div>
        );
      case fieldType.FIELD_TEXT:
        return (
          <TextField
            key={`${uuid}${config.key}`}
            onChange={(e) => {
              onChangeDetailItem(config.key, e.target.value, config.charType);
            }}
            disabled={this.props.disabled}
            value={tmpEditRecord[config.key]}
            required={config.isRequired}
          />
        );
      case fieldType.FIELD_TEXTAREA:
        return (
          <TextAreaField
            key={`${uuid}${config.key}`}
            onChange={(e) => {
              onChangeDetailItem(config.key, e.target.value);
            }}
            disabled={this.props.disabled}
            value={tmpEditRecord[config.key] || ''}
            required={config.isRequired}
          />
        );
      case fieldType.FIELD_SELECT:
      case fieldType.FIELD_SELECT_WITH_PLACEHOLDER: {
        const val = _.isInteger(tmpEditRecord[config.key])
          ? tmpEditRecord[config.key].toString()
          : tmpEditRecord[config.key];

        return (
          <SelectField
            key={`${uuid}${config.key}`}
            onChange={(e) => {
              if (config.multiple) {
                const values = [].filter
                  .call(e.target.options, (o) => {
                    return o.selected;
                  })
                  .map((o) => {
                    return o.value;
                  });
                onChangeDetailItem(config.key, values);
              } else {
                onChangeDetailItem(config.key, e.target.value);
              }
            }}
            disabled={this.props.disabled}
            options={this.state.options}
            value={val || (config.multiple ? [] : '')}
            required={config.isRequired}
            multiple={config.multiple || false}
            size={config.multiple ? this.state.options.length : null}
          />
        );
      }
      case fieldType.FIELD_RADIO: {
        const options = (sfObjFieldValues[config.props] || []).map(
          (settingItem) => ({
            text: msg()[settingItem.label],
            value: settingItem.value,
          })
        );
        return (
          <RadioGroupField
            disabled={this.props.disabled}
            key={`${uuid}${config.key}`}
            layout={RADIO_GROUP_FIELD_LAYOUT_TYPE.vertical}
            options={options}
            value={tmpEditRecord[config.key]}
            onChange={(value) => onChangeDetailItem(config.key, value)}
            showSelectedTextOnly={false}
            name={`${uuid}${config.key}`}
          />
        );
      }
      case fieldType.FIELD_DATE:
        return (
          <DateField
            key={`${uuid}${config.key}`}
            className={`${ROOT}__item-date ts-text-field slds-input`}
            disabled={this.props.disabled}
            onChange={(value) => {
              onChangeDetailItem(config.key, value);
            }}
            value={tmpEditRecord[config.key] || ''}
            required={config.isRequired}
          />
        );
      case fieldType.FIELD_TIME:
        return this.renderAttTimeField(config.key);
      case fieldType.FIELD_TIME_START_END:
        return this.renderAttTimeRangeField(config.key, config.subkey);
      case fieldType.FIELD_CHECKBOX:
        return this.renderCheckbox(
          config.key,
          this.props.disabled,
          config.label
        );
      case fieldType.FIELD_REPLACEMENT_LEAVE_OF_HOLIDAY_WORK:
        return this.renderReplacementLeaveOfHolidayWork();
      case fieldType.FIELD_NUMBER:
        return (
          <TextField
            key={`${uuid}${config.key}`}
            type="number"
            min={config.min}
            max={config.max}
            step={config.step}
            value={tmpEditRecord[config.key]}
            disabled={this.props.disabled}
            required={config.isRequired}
            onChange={(e) =>
              onChangeDetailItem(config.key, e.target.value, 'numeric')
            }
          />
        );
      case fieldType.FIELD_CUSTOM:
        return (
          <config.Component
            key={`${uuid}${config.key}`}
            required={this.props.config.isRequired} // Labelコンポーネントから参照される
            {...this.props}
          />
        );
      default:
        return null;
    }
  }

  render() {
    // FIXME リファクタリング対象
    const { config } = this.props;

    if (
      !ConfigUtil.isSatisfiedCondition(
        config,
        this.props.baseValueGetter,
        this.props.historyValueGetter
      )
    ) {
      return null;
    }

    if (
      config.type === fieldType.FIELD_HIDDEN ||
      config.display === displayType.DISPLAY_LIST ||
      !ConfigUtil.isAllowedFunction(config, this.props.useFunction)
    ) {
      return null;
    }
    let name = msg()[config.msgkey] || '';
    if (config.key.match(/_L0$/)) {
      if (!this.props.getOrganizationSetting.language0) {
        return null;
      }
      const lang0 = _.find(this.props.sfObjFieldValues.language, (o) => {
        return o.value === this.props.getOrganizationSetting.language0;
      });
      name += `(${lang0.label})`;
    } else if (config.key.match(/_L1$/)) {
      const lang1 = _.find(this.props.sfObjFieldValues.language, (o) => {
        return o.value === this.props.getOrganizationSetting.language1;
      });
      if (!this.props.getOrganizationSetting.language1) {
        return null;
      }
      name += `(${lang1.label})`;
    } else if (config.key.match(/_L2$/)) {
      if (!this.props.getOrganizationSetting.language2) {
        return null;
      }
      const lang2 = _.find(this.props.sfObjFieldValues.language, (o) => {
        return o.value === this.props.getOrganizationSetting.language2;
      });
      name += `(${lang2.label})`;
    }

    let className = `${ROOT}__item`;

    const helpMsg = config.help ? msg()[config.help] : '';

    if (config.title) {
      className += ` ${ROOT}__item--has-title`;
    }

    if (config.class) {
      className += ` ${config.class}`;
    }

    return (
      <li key={this.state.uuid} className={className}>
        <Label
          text={name}
          childCols={config.size || fieldSize.SIZE_MEDIUM}
          labelCols={config.labelSize}
          helpMsg={helpMsg}
        >
          {this.renderDetailItemField()}
        </Label>
      </li>
    );
  }
}
