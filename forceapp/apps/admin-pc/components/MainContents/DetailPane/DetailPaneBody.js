import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../../../commons/languages';

import * as ConfigUtil from '../../../utils/ConfigUtil';

import DetailSection from './DetailSection';
import DetailItem from './DetailItem';

import './DetailPaneBody.scss';

const ROOT = 'admin-pc-contents-detail-pane__body';

export default class DetailPaneBody extends React.Component {
  static propTypes = {
    checkboxes: PropTypes.object.isRequired,
    configList: PropTypes.array.isRequired,
    baseValueGetter: PropTypes.func.isRequired,
    historyValueGetter: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    getOrganizationSetting: PropTypes.object.isRequired,
    onChangeCheckBox: PropTypes.func.isRequired,
    onChangeDetailItem: PropTypes.func.isRequired,
    sfObjFieldValues: PropTypes.object.isRequired,
    tmpEditRecord: PropTypes.object.isRequired,
    tmpEditRecordBase: PropTypes.object.isRequired,
    renderDetailExtraArea: PropTypes.func,
    disabledValidDateTo: PropTypes.bool,
    useFunction: PropTypes.object.isRequired,
  };

  static defaultProps = {
    renderDetailExtraArea: null,
    disabledValidDateTo: false,
  };

  constructor(props) {
    super(props);
    this.state = { sections: {} };
  }

  componentWillMount() {
    const sections = {};
    this.getSectionKey(this.props.configList, sections);
    this.setState({ sections });
  }

  onClickSectionTitle(section) {
    this.setState((prevState) => {
      const sections = prevState.sections;
      sections[section] = !sections[section];
      return { sections };
    });
  }

  getSectionKey(configList, sections) {
    if (!configList) {
      return;
    }
    configList.forEach((config) => {
      if (config.section) {
        this.getSectionKey(config.configList, sections);
        sections[config.section] = config.defaultOpen || false;
      }
    });
  }

  renderSection(config) {
    if (
      !ConfigUtil.isEffective(
        config,
        this.props.useFunction,
        this.props.baseValueGetter,
        this.props.historyValueGetter
      )
    ) {
      return null;
    }

    const isClosed = this.state.sections[config.section];
    return (
      <DetailSection
        key={config.section}
        sectionKey={config.section}
        title={msg()[config.msgkey]}
        description={
          config.descriptionKey ? msg()[config.descriptionKey] : null
        }
        isExpandable={config.isExpandable}
        isClosed={isClosed}
        onClickToggleButton={() => this.onClickSectionTitle(config.section)}
      >
        {this.renderItemList(config.configList, config.section)}
      </DetailSection>
    );
  }

  renderItem(config, key) {
    let disabled = this.props.disabled;
    if (
      config.enableMode &&
      !disabled &&
      config.enableMode !== this.props.mode
    ) {
      disabled = true;
    }
    return (
      <DetailItem
        key={key}
        checkboxes={this.props.checkboxes}
        config={config}
        baseValueGetter={this.props.baseValueGetter}
        historyValueGetter={this.props.historyValueGetter}
        disabled={disabled}
        getOrganizationSetting={this.props.getOrganizationSetting}
        onChangeCheckBox={this.props.onChangeCheckBox}
        onChangeDetailItem={this.props.onChangeDetailItem}
        sfObjFieldValues={this.props.sfObjFieldValues}
        tmpEditRecord={this.props.tmpEditRecord}
        tmpEditRecordBase={this.props.tmpEditRecordBase}
        disabledValidDateTo={this.props.disabledValidDateTo}
        useFunction={this.props.useFunction}
      />
    );
  }

  renderItemList(configList, key = '') {
    return (
      <ul className={`${ROOT}__item-list`}>
        {configList.map((config, idx) => {
          return config.section
            ? this.renderSection(config)
            : this.renderItem(config, `${key}${idx}`);
        })}
      </ul>
    );
  }

  renderDetailExtraArea() {
    if (this.props.renderDetailExtraArea) {
      return this.props.renderDetailExtraArea(this.props.disabled);
    }
    return null;
  }

  render() {
    return (
      <div className={ROOT}>
        {this.renderItemList(this.props.configList)}
        {this.renderDetailExtraArea()}
      </div>
    );
  }
}
