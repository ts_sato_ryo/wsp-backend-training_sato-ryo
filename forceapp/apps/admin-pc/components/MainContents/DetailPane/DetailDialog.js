import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../../../commons/languages';
import fieldSize from '../../../constants/fieldSize';

import Button from '../../../../commons/components/buttons/Button';
import DateField from '../../../../commons/components/fields/DateField';
import DialogFrame from '../../../../commons/components/dialogs/DialogFrame';
import Label from '../../../../commons/components/fields/Label';
import TextField from '../../../../commons/components/fields/TextField';

import './DetailDialog.scss';

const ROOT = 'admin-pc-contents-detail-pane__dialog';

export default class HistoryDialog extends React.Component {
  static propTypes = {
    onChangeDetailItem: PropTypes.func.isRequired,
    onClickCancelEditButton: PropTypes.func.isRequired,
    onClickRevisionStartButton: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    tmpEditRecord: PropTypes.object.isRequired,
  };

  renderSaveHistoryArea() {
    return (
      <div className={`${ROOT}__body`}>
        <Label
          text={msg().Admin_Lbl_RevisionDate}
          childCols={fieldSize.SIZE_MEDIUM}
        >
          <DateField
            className="ts-text-field slds-input"
            onChange={(value) => {
              this.props.onChangeDetailItem('validDateFrom', value);
            }}
            value={this.props.tmpEditRecord.validDateFrom || ''}
          />
        </Label>
        <Label
          text={msg().Admin_Lbl_ReasonForRevision}
          childCols={fieldSize.SIZE_MEDIUM}
        >
          <TextField
            className={`${ROOT}__item-name`}
            onChange={(e) => {
              this.props.onChangeDetailItem('comment', e.target.value);
            }}
            value={this.props.tmpEditRecord.comment}
          />
        </Label>
      </div>
    );
  }

  render() {
    let disabled = false;
    if (!this.props.tmpEditRecord.validDateFrom) {
      disabled = true;
    }

    return (
      <DialogFrame
        className={`${ROOT}`}
        title={this.props.title}
        hide={this.props.onClickCancelEditButton}
        footer={
          <DialogFrame.Footer>
            <Button type="default" onClick={this.props.onClickCancelEditButton}>
              {msg().Com_Btn_Cancel}
            </Button>
            <Button
              type="primary"
              disabled={disabled}
              onClick={this.props.onClickRevisionStartButton}
            >
              {msg().Com_Btn_Save}
            </Button>
          </DialogFrame.Footer>
        }
      >
        {this.renderSaveHistoryArea()}
      </DialogFrame>
    );
  }
}
