/* @flow */
import React from 'react';

import ExpandableSection from '../../../commons/components/ExpandableSection';
import Label from '../../../commons/components/fields/Label';
import TextField from '../../../commons/components/fields/TextField';
import Button from '../../../commons/components/buttons/Button';

import msg from '../../../commons/languages';

import './EmployeeSearchForm.scss';

const ROOT = 'admin-pc-common-employee-search-form';

const QueryField = (props: {
  label: string,
  value: string,
  onChange: (string) => void,
}) => (
  <Label
    labelCols={2}
    childCols={4}
    text={props.label}
    className={`${ROOT}__label`}
  >
    <TextField
      type="search"
      value={props.value}
      onChange={(event) => props.onChange(event.target.value)}
    />
  </Label>
);

export type Props = {
  isSearchExecuted: boolean,
  employeeCodeQuery: string,
  employeeNameQuery: string,
  departmentNameQuery: string,
  workingTypeNameQuery: string,
  onChangeEmployeeCodeQuery: (string) => void,
  onChangeEmployeeNameQuery: (string) => void,
  onChangeDepartmentNameQuery: (string) => void,
  onChangeWorkingTypeNameQuery: (string) => void,
  onSubmitSearchForm: () => void,
};

export default class SearchForm extends React.Component<Props> {
  onSubmitSearchForm: (SyntheticEvent<HTMLFormElement>) => void;

  constructor() {
    super();
    this.onSubmitSearchForm = this.onSubmitSearchForm.bind(this);
  }

  onSubmitSearchForm(event: SyntheticEvent<HTMLFormElement>) {
    event.preventDefault();
    this.props.onSubmitSearchForm();
  }

  renderTitleAndConditionsSummary(isExpanded: boolean) {
    return (
      <span>
        <span>{msg().Com_Lbl_EmployeeSearch}</span>
        {this.props.isSearchExecuted && !isExpanded ? (
          <span
            className={`slds-text-color--weak ${ROOT}__search-summary-conditions`}
          >
            {[
              `${msg().Com_Lbl_SearchConditions}:`,
              this.props.employeeCodeQuery,
              this.props.employeeNameQuery,
              this.props.departmentNameQuery,
              this.props.workingTypeNameQuery,
            ].join(' ')}
          </span>
        ) : null}
      </span>
    );
  }

  render() {
    return (
      <ExpandableSection
        className={ROOT}
        expanded={!this.props.isSearchExecuted}
        summary={(isExpanded) =>
          this.renderTitleAndConditionsSummary(isExpanded)
        }
      >
        <form onSubmit={this.onSubmitSearchForm}>
          <QueryField
            label={msg().Com_Lbl_EmployeeName}
            value={this.props.employeeNameQuery}
            onChange={this.props.onChangeEmployeeNameQuery}
          />
          <QueryField
            label={msg().Com_Lbl_EmployeeCode}
            value={this.props.employeeCodeQuery}
            onChange={this.props.onChangeEmployeeCodeQuery}
          />
          <QueryField
            label={msg().Com_Lbl_DepartmentName}
            value={this.props.departmentNameQuery}
            onChange={this.props.onChangeDepartmentNameQuery}
          />
          <QueryField
            label={msg().Admin_Lbl_WorkScheme}
            value={this.props.workingTypeNameQuery}
            onChange={this.props.onChangeWorkingTypeNameQuery}
          />
          <div className="slds-grid">
            <div className="slds-size--2-of-12 slds-grow-none slds-shrink-none" />
            <div
              className={`slds-size--4-of-12 slds-grow-none slds-shrink-none ${ROOT}__button-container`}
            >
              <Button type="default" submit className={`${ROOT}__button`}>
                {msg().Com_Btn_Search}
              </Button>
            </div>
          </div>
        </form>
      </ExpandableSection>
    );
  }
}
