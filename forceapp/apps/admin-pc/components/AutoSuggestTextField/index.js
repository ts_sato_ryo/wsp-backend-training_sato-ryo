import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import Autosuggest from 'react-autosuggest';

import '../../../commons/components/fields/TextField.scss';
import './index.scss';

/**
 * インクリメンタル検索対応テキスト項目 - 共通コンポーネント
 */
export default class AutoSuggestTextField extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    configKey: PropTypes.string,
    disabled: PropTypes.bool,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onKeyDown: PropTypes.func,
    onChange: PropTypes.func,
    readOnly: PropTypes.bool,
    suggestConfig: PropTypes.object,
    suggestList: PropTypes.array,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  };

  static defaultProps = {
    suggestConfig: {},
    disabled: false,
    readOnly: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: this.convertValueToLabel(this.props),
      suggestions: [],
    };

    this.buildLabel = this.buildLabel.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onChange = this.onChange.bind(this);
    this.getSuggestions = this.getSuggestions.bind(this);
    this.getSuggestionValue = this.getSuggestionValue.bind(this);
    this.renderInputComponent = this.renderInputComponent.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(
      this
    );
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(
      this
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      value: this.convertValueToLabel(nextProps),
    });
  }

  onFocus(e) {
    if (this.props.onFocus) {
      this.props.onFocus(e, e.target.value);
    }
  }

  onBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e, this.convertLabelToValue(e.target.value));
    }
  }

  onKeyDown(e) {
    if (this.props.onKeyDown) {
      this.props.onKeyDown(e, e.target.value);
    }
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
    });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  buildLabel(suggestItem) {
    const { suggestConfig } = this.props;
    return suggestConfig.buildLabel
      ? suggestConfig.buildLabel(suggestItem)
      : suggestItem[suggestConfig.label];
  }

  convertValueToLabel({ value, suggestList, suggestConfig }) {
    for (let i = 0; i < suggestList.length; i++) {
      const suggest = suggestList[i];
      if (suggest[suggestConfig.value] === value) {
        return this.buildLabel(suggest);
      }
    }
    return '';
  }

  convertLabelToValue(label) {
    for (let i = 0; i < this.props.suggestList.length; i++) {
      const suggest = this.props.suggestList[i];
      if (this.buildLabel(suggest) === label) {
        return suggest[this.props.suggestConfig.value];
      }
    }
    return '';
  }

  getSuggestions(value) {
    const inputValue = value.trim().toLowerCase();

    if (inputValue === '') {
      return [];
    }

    return this.props.suggestList.filter((suggest) =>
      Object.keys(suggest).some(
        (key) =>
          key !== 'id' &&
          suggest[key] &&
          suggest[key].toLowerCase().match(inputValue)
      )
    );
  }

  getSuggestionValue(suggestion) {
    return this.buildLabel(suggestion);
  }

  renderSuggestion(suggestion) {
    return <span>{this.buildLabel(suggestion)}</span>;
  }

  renderInputComponent = (inputProps) => (
    <div>
      <input {...inputProps} />
    </div>
  );

  render() {
    const { className, disabled, readOnly, ...props } = this.props;

    const textFieldClass = classNames('ts-text-field', 'slds-input', className);

    // 重複回避
    delete props.onFocus;
    delete props.onBlur;
    delete props.onKeyDown;
    delete props.onChange;

    const { value, suggestions } = this.state;
    const inputProps = {
      className: textFieldClass,
      disabled,
      readOnly,
      value,
      onChange: this.onChange,
      onBlur: this.onBlur,
    };

    if (readOnly) {
      return (
        <div className="ts-text-field ts-text-field--readonly" title={value}>
          {value}
        </div>
      );
    } else {
      return (
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={this.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          inputProps={inputProps}
          renderInputComponent={this.renderInputComponent}
        />
      );
    }
  }
}
