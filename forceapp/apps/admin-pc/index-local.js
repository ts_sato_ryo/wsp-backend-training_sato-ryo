// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import Api from './api/local';

import * as AppActions from './action-dispatchers/App';
import AdminContainer from './containers/AdminContainer';

import onDevelopment from '../commons/config/development';

import '../commons/styles/base.scss';
import '../commons/config/moment';

const renderApp = (store, Component) => {
  const container = document.getElementById('container');
  if (container) {
    onDevelopment(() => {
      ReactDOM.render(
        <Provider store={store}>
          <Component />
        </Provider>,
        container
      );
    });
  }
};

// eslint-disable-next-line import/prefer-default-export
export const startApp = (params: Object) => {
  const configuredStore = configureStore({
    env: {
      api: new Api(),
    },
  });
  renderApp(configuredStore, AdminContainer);
  configuredStore.dispatch(AppActions.initialize(params));
};
