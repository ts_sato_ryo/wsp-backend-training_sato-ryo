// @flow

import isFunction from 'lodash/isFunction';
import isFinite from 'lodash/isFinite';
import get from 'lodash/get';
import * as React from 'react';

import FIELD_TYPE, { type FieldType } from '../constants/fieldType';
import { type FieldSize } from '../constants/fieldSize';
import DISPLAY_TYPE, { type DisplayType } from '../constants/displayType';
import {
  type FunctionType,
  type FunctionTypeList,
} from '../constants/functionType';

type $Config = {|
  // Field key.
  key: string,

  // Label message key in en_US.csv.
  msgkey?: string,
  // Help message key in en_US.csv.
  help?: string,
  // Title(?) message key in en_US.csv.
  // FIXMI: I did not find an implementation that uses this property.
  title?: string,
  // Specify element class name of row.
  class?: string,

  // Specify field value type.
  charType?: string,
  // On required value.
  isRequired?: boolean,
  // Specify default value
  defaultValue?: mixed,

  // Path to get values from API when initilaizing.
  // Response values is saved to sfObjFieldValues specified `props` property.
  // (Need to set `props`.)
  path?: string,
  // Option values from sfObjFieldValues.
  props?: string,

  // Redux's action name for initilazing sfObjFieldValue
  // if validDateFrom of history record is changed.
  action?: string,

  // Field is displayed by this value.
  enableMode?: '' | 'new' | 'edit',

  // Specify display view, only list, detail or both.
  display?: $Values<DisplayType>,

  // Specify view value in list instead.
  dependent?: string,

  // Specify dispaly by another value.
  condition?: ((string) => mixed, (string) => mixed) => boolean,

  // Specify to use function.
  useFunction?: $Values<FunctionType>,

  // Column size.
  // FIXME: Some configs use out of FieldSize.
  size?: $Values<FieldSize> | 7,
  // Label size.
  labelSize?: $Values<FieldSize>,
|};

type FieldTypeNone = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_NONE'>,
|};

type FieldTypeHidden = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_HIDDEN'>,
|};

type FieldTypeText = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_TEXT'>,
|};

type FieldTypeTextarea = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_TEXTAREA'>,
|};

type FieldTypeSelect = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_SELECT'>,
  // On multiple value.
  multiple?: boolean,
  // Create location label in option.
  multiLanguageValue?: boolean,
  // Specify Options
  options?: {
    msgkey: string,
    value: mixed,
  }[],
  // On to ignore it self.
  ignoreItself?: boolean,
|};

type FieldTypeDate = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_DATE'>,
|};

type FieldTypeCheckbox = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_CHECKBOX'>,
  // Specify label if type is FIELD_CHECKBOX.
  label?: string,
|};

type FieldTypeUserName = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_USER_NAME'>,
  // Specify Suffix of name value.
  ltype?: string,
|};

type FieldTypeValidDate = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_VALID_DATE'>,
|};

type FieldTypeTime = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_TIME'>,
  // Specify label
  label?: string,
|};

type FieldTypeTimeStartEnd = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_TIME_START_END'>,
  subkey: string,
|};

type FieldTypeSelectWithPlaceholder = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_SELECT_WITH_PLACEHOLDER'>,
  // On multiple value.
  multiple?: boolean,
  // Create location label in option.
  multiLanguageValue?: boolean,
  // Specify Options
  options?: {
    msgkey: string,
    value: mixed,
  }[],
  // On to ignore it self.
  ignoreItself?: boolean,
|};

type FieldTypeReplacementLeaveOfHolidayWork = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_REPLACEMENT_LEAVE_OF_HOLIDAY_WORK'>,
|};

type FieldTypeNumber = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_NUMBER'>,
  max?: number,
  min?: number,
  step?: number,
|};

type FieldTypeRadio = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_RADIO'>,
|};

type FieldTypeCustom = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_CUSTOM'>,
  Component: React.ComponentType<any>,
|};

type FieldTypeAutosuggestText = {|
  ...$Config,
  type: $PropertyType<FieldType, 'FIELD_AUTOSUGGEST_TEXT'>,
  // Specify auto suggest setting
  autoSuggest: {
    value: string,
    label: string,
    buildLabel: (Object) => string,
    suggestionKey: string[],
  },
|};

export type Config =
  | FieldTypeNone
  | FieldTypeHidden
  | FieldTypeText
  | FieldTypeTextarea
  | FieldTypeSelect
  | FieldTypeDate
  | FieldTypeCheckbox
  | FieldTypeUserName
  | FieldTypeValidDate
  | FieldTypeTime
  | FieldTypeTimeStartEnd
  | FieldTypeSelectWithPlaceholder
  | FieldTypeReplacementLeaveOfHolidayWork
  | FieldTypeNumber
  | FieldTypeRadio
  | FieldTypeCustom
  | FieldTypeAutosuggestText;

export type Section = {|
  // Section key.
  section: string,
  // Display expandable button.
  isExpandable?: boolean,
  // Label message key in en_US.csv.
  msgkey?: $PropertyType<Config, 'msgkey'>,
  // Description key in en_US.csv.
  descriptionKey?: string,
  // Specify dispaly by another value.
  condition?: $PropertyType<Config, 'condition'>,
  // Specify to use function.
  useFunction?: $PropertyType<Config, 'useFunction'>,
  // Child configs
  configList?: Array<Config | Section>,
|};

export type ConfigListItem = Config | Section;

export type ConfigList = ConfigListItem[];

export type ConfigListMap = {|
  base: ConfigList,
  history?: ConfigList,
|};

/**
 * Flattens config array.
 */
export const flatten = (...configLists: ConfigList[]): Config[] => {
  return configLists.reduce((list, configList) => {
    if (!configList) {
      return list;
    } else {
      return configList.reduce(($list, config) => {
        if (config.section && config.configList) {
          return $list.concat(flatten(config.configList));
        } else if (config.key && config.type) {
          return $list.concat(config);
        } else {
          return $list;
        }
      }, list);
    }
  }, []);
};

/**
 * Invoke condition
 */
export const isSatisfiedCondition = (
  config: {
    +condition?: $PropertyType<Config, 'condition'>,
  },
  baseValueGetter: (string) => mixed,
  historyValueGetter: (string) => mixed
): boolean => {
  const { condition } = config;
  if (!isFunction(condition)) {
    return true;
  }
  return condition(baseValueGetter, historyValueGetter);
};

/**
 * Check has function
 */
export const isAllowedFunction = (
  config: {
    +useFunction?: $PropertyType<Config, 'useFunction'>,
  },
  functionTypeList: FunctionTypeList
): boolean => !config.useFunction || !!functionTypeList[config.useFunction];

/**
 * is a effectable config
 */
export const isEffective = (
  config: {
    +condition?: $PropertyType<Config, 'condition'>,
    +useFunction?: $PropertyType<Config, 'useFunction'>,
  },
  functionTypeList: FunctionTypeList,
  baseValueGetter: (string) => mixed,
  historyValueGetter: (string) => mixed
): boolean =>
  isAllowedFunction(config, functionTypeList) &&
  isSatisfiedCondition(config, baseValueGetter, historyValueGetter);

/**
 * is a value of detail view
 */
export const isDisplayDetail = (config: Config) =>
  !config.display ||
  config.display === DISPLAY_TYPE.DISPLAY_DETAIL ||
  config.display === DISPLAY_TYPE.DISPLAY_BOTH;

/**
 * is a value of detail view
 */
export const isDisplayList = (config: Config) =>
  config.display === DISPLAY_TYPE.DISPLAY_LIST ||
  config.display === DISPLAY_TYPE.DISPLAY_BOTH;

/**
 * check charType
 */
export const checkCharType = (config: Config, value: any): boolean => {
  switch (config.charType) {
    case 'numeric':
      return isFinite(value);
    default:
      return true;
  }
};

/**
 * Get default value from config
 */
export const getDefaultValue = (
  config: Config,
  sfObjFieldValues: { [string]: any }
) => {
  const { type } = config;
  if (config.defaultValue !== undefined) {
    return config.defaultValue;
  } else if (type === FIELD_TYPE.FIELD_SELECT) {
    if (config.multiple) {
      return [];
    } else if (config.isRequired) {
      return config.props
        ? get(sfObjFieldValues, `${config.props}[0].value`, '')
        : '';
    } else {
      return '';
    }
  } else {
    return '';
  }
};
