// @flow

import * as ConfigUtil from '../ConfigUtil';

import FIELD_TYPE from '../../constants/fieldType';
import DISPLAY_TYPE, { type DisplayType } from '../../constants/displayType';

// Arrange
const valueGatter = (key: string) => key === 'true';

describe('admin-pc/utils/ConfigUtil', () => {
  describe('flattern', () => {
    test('single', () => {
      // Arrange
      const configList: ConfigUtil.Config = [
        {
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          section: 'section1',
          configList: [
            {
              key: 'key2',
              type: FIELD_TYPE.FIELD_HIDDEN,
            },
            {
              key: 'key3',
              type: FIELD_TYPE.FIELD_HIDDEN,
            },
            {
              section: 'section2',
              configList: [
                {
                  key: 'key4',
                  type: FIELD_TYPE.FIELD_HIDDEN,
                },
              ],
            },
          ],
        },
        {
          section: 'section3',
        },
        {
          key: 'key5',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
      ];

      const expected = [
        {
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          key: 'key2',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          key: 'key3',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          key: 'key4',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          key: 'key5',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
      ];

      // Run
      const result = ConfigUtil.flatten(configList);

      // Assert
      expect(result).toEqual(expected);
    });

    test('multiple', () => {
      // Arrange
      const configList1: ConfigUtil.Config = [
        {
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          section: 'section1',
          configList: [
            {
              key: 'key2',
              type: FIELD_TYPE.FIELD_HIDDEN,
            },
          ],
        },
      ];
      const configList2: ConfigUtil.Config = [
        {
          key: 'key3',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          section: 'section2',
          configList: [
            {
              key: 'key4',
              type: FIELD_TYPE.FIELD_HIDDEN,
            },
          ],
        },
      ];
      const expected = [
        {
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          key: 'key2',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          key: 'key3',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
        {
          key: 'key4',
          type: FIELD_TYPE.FIELD_HIDDEN,
        },
      ];

      // Run
      const result = ConfigUtil.flatten(configList1, configList2);

      // Assert
      expect(result).toEqual(expected);
    });
  });

  describe('isSatisfiedCondition', () => {
    test('had condition and return true', () => {
      // Arrange
      const config: ConfigUtil = {
        key: 'key1',
        type: FIELD_TYPE.FIELD_HIDDEN,
        condition: (baseValueGatter, historyValueGetter) =>
          baseValueGatter('true') && historyValueGetter('true'),
      };

      // Run
      const result = ConfigUtil.isSatisfiedCondition(
        config,
        valueGatter,
        valueGatter
      );

      // Assert
      expect(result).toEqual(true);
    });

    test('had condition and return false', () => {
      // Arrange
      const config: ConfigUtil = {
        key: 'key1',
        type: FIELD_TYPE.FIELD_HIDDEN,
        condition: (baseValueGatter, historyValueGetter) =>
          baseValueGatter('false') && historyValueGetter('false'),
      };

      // Run
      const result = ConfigUtil.isSatisfiedCondition(
        config,
        valueGatter,
        valueGatter
      );

      // Assert
      expect(result).toEqual(false);
    });

    test("don't had condition", () => {
      // Arrange
      const config: ConfigUtil = {
        key: 'key1',
        type: FIELD_TYPE.FIELD_HIDDEN,
      };

      // Run
      const result = ConfigUtil.isSatisfiedCondition(
        config,
        valueGatter,
        valueGatter
      );

      // Assert
      expect(result).toEqual(true);
    });
  });

  describe('isAllowedFunction', () => {
    test('empty', () => {
      expect(
        ConfigUtil.isAllowedFunction(
          {
            key: 'key1',
            type: FIELD_TYPE.FIELD_HIDDEN,
          },
          {
            useAttendance: true,
          }
        )
      ).toEqual(true);
    });

    test('has use function and has permisson', () => {
      expect(
        ConfigUtil.isAllowedFunction(
          {
            key: 'key1',
            type: FIELD_TYPE.FIELD_HIDDEN,
            useFunction: 'useAttendance',
          },
          {
            useAttendance: true,
          }
        )
      ).toEqual(true);
    });

    test("has use function and don't has permission", () => {
      expect(
        ConfigUtil.isAllowedFunction(
          {
            key: 'key1',
            type: FIELD_TYPE.FIELD_HIDDEN,
            useFunction: 'useAttendance',
          },
          {
            useAttendance: false,
          }
        )
      ).toEqual(false);
    });

    test('has use function and empty permission', () => {
      expect(
        ConfigUtil.isAllowedFunction(
          {
            key: 'key1',
            type: FIELD_TYPE.FIELD_HIDDEN,
            useFunction: 'useAttendance',
          },
          {}
        )
      ).toEqual(false);
    });
  });

  describe('isDisplayDetail', () => {
    test("don't has displayType", () => {
      expect(
        ConfigUtil.isDisplayDetail({
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
        })
      ).toEqual(true);
    });
    test.each(
      ([
        ['DISPLAY_DETAIL', true],
        ['DISPLAY_LIST', false],
        ['DISPLAY_BOTH', true],
      ]: [$Keys<DisplayType>, boolean][])
    )(`%s: %p`, (key: $Keys<DisplayType>, result: boolean) => {
      expect(
        ConfigUtil.isDisplayDetail({
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
          display: DISPLAY_TYPE[key],
        })
      ).toEqual(result);
    });
  });

  describe('isDisplayList', () => {
    test("don't has displayType", () => {
      expect(
        ConfigUtil.isDisplayList({
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
        })
      ).toEqual(false);
    });
    test.each(
      ([
        ['DISPLAY_DETAIL', false],
        ['DISPLAY_LIST', true],
        ['DISPLAY_BOTH', true],
      ]: [$Keys<DisplayType>, boolean][])
    )(`%s: %p`, (key: $Keys<DisplayType>, result: boolean) => {
      expect(
        ConfigUtil.isDisplayList({
          key: 'key1',
          type: FIELD_TYPE.FIELD_HIDDEN,
          display: DISPLAY_TYPE[key],
        })
      ).toEqual(result);
    });
  });

  describe('checkCharType', () => {
    describe('has not charType', () => {
      test.each([undefined, false, null, '0', 0, '', 'abc'])('%p', (value) => {
        expect(
          ConfigUtil.checkCharType(
            {
              key: 'key1',
              type: FIELD_TYPE.FIELD_HIDDEN,
            },
            value
          )
        ).toEqual(true);
      });
    });

    describe.each([
      [
        'numeric',
        {
          false: [undefined, false, null, '', 'abc', '0', '123'],
          true: [0, 123],
        },
      ],
    ])('charType is %s', (charType, testTable) => {
      describe('return false', () => {
        test.each(testTable.false)('%p', (value) => {
          expect(
            ConfigUtil.checkCharType(
              {
                key: 'key1',
                type: FIELD_TYPE.FIELD_HIDDEN,
                charType,
              },
              value
            )
          ).toEqual(false);
        });
      });
      describe('return true', () => {
        test.each(testTable.true)('%p', (value) => {
          expect(
            ConfigUtil.checkCharType(
              {
                key: 'key1',
                type: FIELD_TYPE.FIELD_HIDDEN,
                charType,
              },
              value
            )
          ).toEqual(true);
        });
      });
    });
  });
});
