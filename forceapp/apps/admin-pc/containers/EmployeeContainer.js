import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as employee from '../actions/employee';
import { searchCostCenter } from '../actions/costCenter';
import { searchDepartment } from '../actions/department';
import { searchWorkingType } from '../actions/workingType';
import { searchAgreementAlertSetting } from '../modules/agreement-alert-setting/entities';
import { actions as uiActionsDA } from '../modules/delegateApprover/ui/assignment';
import { actions as actionsDA } from '../modules/delegateApprover/entities/assignment';
import { searchTimeSetting } from '../actions/timeSetting';
import { searchCalendar } from '../actions/calendar';
import { searchPermission } from '../actions/permission';
import { searchEmployeeGroup } from '../actions/employeeGroup';

import Employee from '../presentational-components/Employee';

const mapStateToProps = (state) => {
  return {
    searchEmployee: state.searchEmployee,
    searchHistory: state.searchHistory,
    searchUser: state.searchUser,
    searchWorkingType: state.searchWorkingType,
    searchEmployeeGroup: state.searchEmployeeGroup,
    isNewAssignment: state.delegateApprover.ui.assignment.isNewAssignment,
    delegateSettings: state.delegateApprover.entities.assignment,
    isEditMode:
      state.base.detailPane.ui.modeBase ||
      state.base.detailPane.ui.modeHistory === 'revision',
    isNew: state.base.detailPane.ui.modeHistory === 'new',
  };
};

const mapDispatchToProps = (dispatch) => {
  const alias = {
    search: employee.searchEmployee,
    create: employee.createEmployee,
    update: employee.updateEmployee,
    delete: employee.deleteEmployee,
    createHistory: employee.createHistoryEmployee,
    searchHistory: employee.searchHistoryEmployee,
    updateHistory: employee.updateHistoryEmployee,
    deleteHistory: employee.deleteHistoryEmployee,
  };

  const actions = bindActionCreators(
    _.pickBy(
      Object.assign({}, alias, employee, {
        searchCostCenter,
        searchDepartment,
        searchWorkingType,
        searchAgreementAlertSetting,
        searchTimeSetting,
        searchCalendar,
        searchPermission,
        searchEmployeeGroup,
        listDelegatedApprovers: actionsDA.list,
        openNewAssignment: uiActionsDA.openNewAssignment,
      }),
      _.isFunction
    ),
    dispatch
  );
  return { actions };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Employee);
