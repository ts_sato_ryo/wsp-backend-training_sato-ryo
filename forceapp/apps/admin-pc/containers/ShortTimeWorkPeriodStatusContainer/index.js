import { connect } from 'react-redux';

import ShortTimeWorkPeriodStatus from '../../presentational-components/ShortTimeWorkPeriodStatus';
import { selectors as selectionUISelectors } from '../../modules/adminCommon/employeeSelection/ui/selection';

const mapStateToProps = (state) => ({
  isDetailVisible: selectionUISelectors.selectSelectedEmployee(state),
});

export default connect(mapStateToProps)(ShortTimeWorkPeriodStatus);
