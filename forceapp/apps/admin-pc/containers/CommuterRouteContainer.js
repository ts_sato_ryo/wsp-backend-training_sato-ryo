import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actions as useChargedExpressActions } from '../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm/option/useChargedExpress';
import { actions as useExReservationActions } from '../../expenses-pc/modules/ui/expenses/recordItemPane/routeForm/option/useExReservation';
import { changeRecordHistoryValue } from '../action-dispatchers/Edit';

import {
  searchRoute,
  onClickAddViaButton,
  onChangeOrigin,
  onChangeViaList,
  onChangeArrival,
  onChangeTmpOrigin,
  onChangeTmpViaList,
  onChangeTmpArrival,
  onClickDeleteViaButton,
  resetRouteForm,
  resetRouteSearchResult,
} from '../../expenses-pc/action-dispatchers/Route';

// presentational component
import CommuterRoute from '../components/CommuterRoute';

const mapStateToProps = (state, ownProps) => ({
  origin: state.commuterRoute.origin,
  viaList: state.commuterRoute.viaList,
  arrival: state.commuterRoute.arrival,
  useChargedExpress: state.commuterRoute.option.useChargedExpress,
  useExReservation: state.commuterRoute.option.useExReservation,
  fareType: state.commuterRoute.option.fareType,
  errorOrigin: state.commuterRoute.errors.origin,
  errorViaList: state.commuterRoute.errors.viaList,
  errorArrival: state.commuterRoute.errors.arrival,
  tmpOrigin: state.commuterRoute.edits.origin,
  tmpViaList: state.commuterRoute.edits.viaList,
  tmpArrival: state.commuterRoute.edits.arrival,
  route: state.routeList,
  tmpEditRecord: state.tmpEditRecord,
  disabled: ownProps.disabled,
});

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators(
    {
      onClickAddViaButton,
      onChangeOrigin,
      onChangeViaList,
      onChangeArrival,
      onChangeTmpOrigin,
      onChangeTmpViaList,
      onChangeUseChargedExpress: useChargedExpressActions.set,
      onChangeUseExReservation: useExReservationActions.set,
      onChangeTmpArrival,
      searchRoute,
      resetRouteForm,
      resetRouteSearchResult,
      onClickDeleteViaButton,
      changeRecordHistoryValue,
    },
    dispatch
  ),
});

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickSearchRouteButton: () => {
    const param = {
      targetDate: moment().format('YYYY-MM-DD'),
      origin: stateProps.origin,
      arrival: stateProps.arrival,
      viaList: stateProps.viaList,
      option: {
        useChargedExpress: stateProps.useChargedExpress,
        seatPreference: 1,
        excludeCommuterRoute: false,
        useExReservationActions: stateProps.useExReservation,
      },
    };

    dispatchProps.searchRoute(
      param,
      stateProps.tmpOrigin,
      stateProps.tmpArrival,
      stateProps.tmpViaList
    );
  },
  onClickResetRouteButton: () => {
    dispatchProps.resetRouteSearchResult();
    dispatchProps.resetRouteForm(null, true);
  },
  onChangeDetailItem: (key, value, charType) =>
    dispatchProps.changeRecordHistoryValue(key, value, charType),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(CommuterRoute);
