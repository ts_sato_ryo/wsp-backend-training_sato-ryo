import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as workingType from '../actions/workingType';
import { searchLeave } from '../actions/leave';
import { searchAttPattern } from '../actions/attPattern';

import WorkingType from '../presentational-components/WorkingType';

const mapStateToProps = (state) => {
  return {
    searchWorkingType: state.searchWorkingType,
    value2msgkey: state.value2msgkey,
  };
};

const mapDispatchToProps = (dispatch) => {
  const alias = {
    search: workingType.searchWorkingType,
    create: workingType.createWorkingType,
    update: workingType.updateWorkingType,
    delete: workingType.deleteWorkingType,
    createHistory: workingType.createHistoryWorkingType,
    searchHistory: workingType.searchHistoryWorkingType,
    updateHistory: workingType.updateHistoryWorkingType,
    deleteHistory: workingType.deleteHistoryWorkingType,
  };

  const actions = bindActionCreators(
    _.pickBy(
      Object.assign({}, alias, workingType, {
        searchLeave,
        searchAttPattern,
      }),
      _.isFunction
    ),
    dispatch
  );
  return { actions };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkingType);
