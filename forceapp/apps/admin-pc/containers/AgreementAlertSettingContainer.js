import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { searchAgreementAlertSetting } from '../modules/agreement-alert-setting/entities';
import {
  createAgreementAlertSetting,
  deleteAgreementAlertSetting,
  updateAgreementAlertSetting,
} from '../modules/agreement-alert-setting/ui';

import AgreementAlertSetting from '../presentational-components/AgreementAlertSetting';

const mapStateToProps = (state) => ({
  itemList: state.agreementAlertSetting.entities,
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(
    {
      search: searchAgreementAlertSetting,
      create: createAgreementAlertSetting,
      update: updateAgreementAlertSetting,
      delete: deleteAgreementAlertSetting,
    },
    dispatch
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  AgreementAlertSetting
);
