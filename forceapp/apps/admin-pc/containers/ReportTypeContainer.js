import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as reportType from '../actions/reportType';

import ReportType from '../presentational-components/ReportType';
import { searchExtendedItem } from '../actions/extendedItem';
import { searchExpenseType } from '../actions/expenseType';

const mapStateToProps = (state) => ({
  searchReportType: state.searchReportType,
  searchExtendedItem: state.searchExtendedItem,
  value2msgkey: state.value2msgkey,
});

const mapDispatchToProps = (dispatch) => {
  const alias = {
    create: reportType.createReportType,
    update: reportType.updateReportType,
    delete: reportType.deleteReportType,
    search: reportType.searchReportType,
  };

  const actions = bindActionCreators(
    _.pickBy(
      Object.assign({}, alias, reportType, {
        searchExtendedItem,
        searchExpenseType,
      }),
      _.isFunction
    ),
    dispatch
  );
  return { actions };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportType);
