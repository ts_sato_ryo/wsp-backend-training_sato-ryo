// @flow

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as shortTimeWorkReason from '../actions/shortTimeWorkReason';

import ShortTimeWorkReason from '../presentational-components/ShortTimeWorkReason';

const mapStateToProps = (state) => ({
  searchShortTimeWorkReason: state.searchShortTimeWorkReason,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => ({
  actions: bindActionCreators(
    {
      search: shortTimeWorkReason.searchShortTimeWorkReason,
      create: shortTimeWorkReason.createShortTimeWorkReason,
      update: shortTimeWorkReason.updateShortTimeWorkReason,
      delete: shortTimeWorkReason.deleteShortTimeWorkReason,
    },
    dispatch
  ),
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(ShortTimeWorkReason): React.ComponentType<Object>);
