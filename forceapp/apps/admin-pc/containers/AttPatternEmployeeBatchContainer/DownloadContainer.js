// @flow

import { connect } from 'react-redux';

import { BATCH_RESULT_STATUS } from '../../../repositories/AttPatternEmployeeBatchRepository';

import Download from '../../presentational-components/AttPatternEmployeeBatch/DetailPane/Download';
import { downloadFile } from '../../action-dispatchers/AttPatternEmployeeBatch';

import { type State } from '../../reducers';

export default (connect(
  (state: State) => ({
    id: state.attPatternEmployeeBatch.ui.detailPane.id,
    actedAt: state.attPatternEmployeeBatch.ui.detailPane.actedAt,
    actor: state.attPatternEmployeeBatch.ui.detailPane.actor,
    departmentName: state.attPatternEmployeeBatch.ui.detailPane.departmentName,
    comment: state.attPatternEmployeeBatch.ui.detailPane.comment,
    status: state.attPatternEmployeeBatch.ui.detailPane.status,
    count: state.attPatternEmployeeBatch.ui.detailPane.count,
    successCount: state.attPatternEmployeeBatch.ui.detailPane.successCount,
    failureCount: state.attPatternEmployeeBatch.ui.detailPane.failureCount,
    isShowLog:
      state.attPatternEmployeeBatch.ui.detailPane.status ===
      BATCH_RESULT_STATUS.Completed,
  }),
  {
    onClickDownload: downloadFile,
  },
  (stateProps, dispatchProps) => ({
    ...stateProps,
    ...dispatchProps,
    onClickDownload: () => dispatchProps.onClickDownload(stateProps.id),
  })
)(Download): React.ComponentType<Object>);
