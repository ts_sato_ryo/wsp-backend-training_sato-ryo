// @flow

import { connect } from 'react-redux';

import AttPatternEmployeeBatch from '../../presentational-components/AttPatternEmployeeBatch';
import {
  openNewDetailPane,
  openDownloadDetailPane,
  listBatchResults,
} from '../../action-dispatchers/AttPatternEmployeeBatch';

import { type State } from '../../reducers';

export default (connect(
  (state: State, ownProps) => ({
    ...ownProps,
    items: state.attPatternEmployeeBatch.entites.batchResultList,
    companyId: state.base.menuPane.ui.targetCompanyId,
  }),
  {
    onClickCreate: openNewDetailPane,
    onClickEdit: openDownloadDetailPane,
    listBatchResults,
  },
  (stateProps, dispatchProps, ownProps) => ({
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    initialize: (companyId: string, showLoading: boolean = true) => {
      dispatchProps.listBatchResults(companyId, showLoading);
    },
  })
)(AttPatternEmployeeBatch): React.ComponentType<Object>);
