// @flow

import { connect } from 'react-redux';

import DetailPane from '../../presentational-components/AttPatternEmployeeBatch/DetailPane';
import * as baseActions from '../../modules/base/detail-pane/ui';

import { type State } from '../../reducers';

export default (connect(
  (state: State, ownProps) => ({
    ...ownProps,
    isNew: state.attPatternEmployeeBatch.ui.detailPane.isNew,
  }),
  {
    onClickClose: () => baseActions.showDetailPane(false),
  }
)(DetailPane): React.ComponentType<Object>);
