import { connect } from 'react-redux';

import PlannerSetting from '../../presentational-components/PlannerSetting';
import { actions as entityActions } from '../../modules/plannerSetting/entities/plannerSetting';

const mapStateToProps = (state) => ({
  companyId: state.base.menuPane.ui.targetCompanyId,
});

const mapDispatchToProps = (dispatch) => ({
  onInitialize: (stateProps) => {
    dispatch(entityActions.clear());
    dispatch(entityActions.fetch(stateProps.companyId));
  },
});

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onInitialize: () => dispatchProps.onInitialize(stateProps),
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  PlannerSetting
);
