import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as jobType from '../actions/jobType';
import { searchSortedWorkCategory } from '../actions/workCategory';

import JobType from '../presentational-components/JobType';

const mapStateToProps = (state) => ({
  itemList: state.searchJobType,
  searchWorkCategory: state.searchWorkCategory,
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(
    {
      search: jobType.searchJobType,
      create: jobType.createJobType,
      update: jobType.updateJobType,
      delete: jobType.deleteJobType,
      searchSortedWorkCategory,
    },
    dispatch
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobType);
