// @flow

import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';

import {
  assignJobToEmployees,
  openEmployeeSelection,
  cancelNewAssignment,
  update,
} from '../../../modules/job/ui/assignment';

import JobAssignmentDialog from '../../../presentational-components/Job/jobAssignmentDialogs/JobAssignmentDialog';

import type { State } from '../../../reducers';

const mapStateToProps = (state: State) => {
  return {
    employees: state.job.ui.assignment.stagedEmployees,
    validDateThrough: state.job.ui.assignment.validDateThrough,
    validDateFrom: state.job.ui.assignment.validDateFrom,
    minValidDateFrom: state.editRecord.validDateFrom,
    canOpenEmployeeSelection: state.job.ui.assignment.validDateFrom !== '',
    canAssign:
      state.job.ui.assignment.validDateFrom !== '' &&
      state.job.ui.assignment.stagedEmployees &&
      state.job.ui.assignment.stagedEmployees.length > 0,
    hasEmployees: state.job.ui.assignment.stagedEmployees.length > 0,
    jobId: state.editRecord.id,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<any>) =>
  bindActionCreators(
    {
      open: openEmployeeSelection,
      cancel: cancelNewAssignment,
      updateValidDateThrough: (value) => update('validDateThrough', value),
      updateValidDateFrom: (value) => update('validDateFrom', value),
      assign: assignJobToEmployees,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  assign: () => {
    dispatchProps.assign({
      jobId: stateProps.jobId,
      validDateThrough: stateProps.validDateThrough,
      validDateFrom: stateProps.validDateFrom,
      employees: stateProps.employees,
    });
  },
  open: () => {
    dispatchProps.open(stateProps.employees);
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(JobAssignmentDialog): React.ComponentType<Object>);
