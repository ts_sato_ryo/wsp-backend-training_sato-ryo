// @flow

import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';

import { actions as assignmentListUIActions } from '../../../modules/job/ui/assignmentList';

import ChangePeriodDialog from '../../../presentational-components/Job/jobAssignmentDialogs/ChangePeriodDialog';

import type { State } from '../../../reducers';

const mapStateToProps = (state: State) => {
  return {
    selectedIds: state.job.ui.assignmentList.selectedIds,
    validDateFrom: state.job.ui.assignmentList.editingValidPeriod.from,
    validDateThrough: state.job.ui.assignmentList.editingValidPeriod.through,
    minValidDateFrom: state.editRecord.validDateFrom,
    canSubmit: state.job.ui.assignmentList.editingValidPeriod.from !== '',
    jobId: state.editRecord.id,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<any>) =>
  bindActionCreators(
    {
      cancel: assignmentListUIActions.endValidPeriodUpdating,
      updateValidDateFrom: assignmentListUIActions.updateValidPeriodBoundKey(
        'from'
      ),
      updateValidDateThrough: assignmentListUIActions.updateValidPeriodBoundKey(
        'through'
      ),
      submit: assignmentListUIActions.bulkUpdateJobAssignments,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  submit: () => {
    dispatchProps.submit(
      {
        ids: stateProps.selectedIds,
        validDateFrom: stateProps.validDateFrom,
        validDateThrough: stateProps.validDateThrough,
      },
      stateProps.jobId
    );
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ChangePeriodDialog): React.ComponentType<Object>);
