// @flow

import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';

import {
  cancelEmployeeSelection,
  selectCandidates,
  deleteACandidate,
  decideCandidates,
  searchEmployees,
  toggleSelection,
} from '../../../modules/job/ui/assignment';

import TargetEmployeesSearchDialog from '../../../presentational-components/Job/jobAssignmentDialogs/TargetEmployeesSearchDialog/index';

import type { State } from '../../../reducers';

const mapStateToProps = (state: State) => {
  return {
    foundEmployees: state.job.ui.assignment.foundEmployees,
    candidates: state.job.ui.assignment.candidates,
    validDateFrom: state.job.ui.assignment.validDateFrom,
    companyId: state.base.menuPane.ui.targetCompanyId,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<any>) =>
  bindActionCreators(
    {
      cancel: cancelEmployeeSelection,
      toggleSelection,
      selectCandidates,
      deleteACandidate,
      decideCandidates,
      search: searchEmployees,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  selectCandidates: () =>
    dispatchProps.selectCandidates(stateProps.foundEmployees),
  decideCandidates: () => dispatchProps.decideCandidates(stateProps.candidates),
  search: (query) =>
    dispatchProps.search({
      ...query,
      targetDate: stateProps.validDateFrom,
      companyId: stateProps.companyId,
    }),
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(TargetEmployeesSearchDialog): React.ComponentType<Object>);
