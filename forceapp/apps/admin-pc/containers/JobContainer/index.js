// @flow

import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { createSelector } from 'reselect';

import { showDialog, hideDialog } from '../../../commons/actions/dialog';
import * as job from '../../actions/job';
import { searchDepartment } from '../../actions/department';
import { searchJobType } from '../../actions/jobType';
import { openNewAssignment } from '../../modules/job/ui/assignment';
import { actions as assignmentListUIActions } from '../../modules/job/ui/assignmentList';
import { searchByJob as searchJobAssignmentByJob } from '../../modules/job/entities/assignmentList';

import Job from '../../presentational-components/Job';

import type { State } from '../../reducers';

const getJobAssignmentList = (state: State) =>
  state.job.entities.assignmentList;

const getSelectedJobAssignmentIds = (state: State) =>
  state.job.ui.assignmentList.selectedIds;

const getConvertedJobAssignmentListAsListItem = createSelector(
  getJobAssignmentList,
  getSelectedJobAssignmentIds,
  (jobAssignmentList, selectedIds) =>
    (jobAssignmentList || []).map((item) => ({
      ...item,
      validDate: { from: item.validDateFrom, through: item.validDateThrough },
      isSelected: selectedIds.includes(item.id),
    }))
);

const mapStateToProps = (state: State) => {
  return {
    searchJob: state.searchJob,
    jobAssignmentList: getConvertedJobAssignmentListAsListItem(state),
    isOpeningNewAssignment: state.job.ui.assignment.isOpeningNewAssignment,
    isOpeningChangePeriod: state.job.ui.assignmentList.isOpeningChangePeriod,
    isOpeningEmployeeSelection:
      state.job.ui.assignment.isOpeningEmployeeSelection,
    showAssignmentArea:
      state.editRecord.id &&
      String(state.tmpEditRecord.isScopedAssignment) === 'true',
    canOperateAssignment:
      state.editRecord.id &&
      String(state.editRecord.isScopedAssignment) === 'true' &&
      state.base.detailPane.ui.modeBase !== 'edit',
    searchCompany: state.searchCompany,
    canExecModifyAssignment:
      state.editRecord.id &&
      String(state.editRecord.isScopedAssignment) === 'true' &&
      state.base.detailPane.ui.modeBase !== 'edit' &&
      state.job.ui.assignmentList.selectedIds.length > 0,
    selectedJobId: state.editRecord.id,
    selectedJobAssignmentIds: state.job.ui.assignmentList.selectedIds,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<any>) => {
  const alias = {
    create: job.createJob,
    update: job.updateJob,
    delete: job.deleteJob,
    search: job.searchJob,
  };

  const actions = bindActionCreators(
    {
      ...alias,
      ...job,
      showDialog,
      hideDialog,
      searchDepartment,
      searchJobType,
      searchJobAssignmentByJob,
      openNewAssignment,
      openChangeValidPeriod: assignmentListUIActions.startValidPeriodUpdating,
      deleteAssignment: assignmentListUIActions.bulkDeleteJobAssignments,
      onAssignmentListRowsSelected: assignmentListUIActions.select,
      onAssignmentListRowsDeselected: assignmentListUIActions.deselect,
      onAssignmentListRowClick: assignmentListUIActions.toggle,
      onAssignmentListFilterChange: assignmentListUIActions.clear,
      getConstantsScopedAssignment: job.getConstantsScopedAssignment,
    },
    dispatch
  );
  return { actions };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  actions: {
    ...dispatchProps.actions,
    deleteAssignment: () =>
      dispatchProps.actions.deleteAssignment(
        {
          ids: stateProps.selectedJobAssignmentIds,
        },
        stateProps.selectedJobId
      ),
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Job): React.ComponentType<Object>);
