import { connect } from 'react-redux';

import LeaveOfAbsencePeriodStatus from '../../presentational-components/LeaveOfAbsencePeriodStatus';
import { selectors as selectionUISelectors } from '../../modules/adminCommon/employeeSelection/ui/selection';

const mapStateToProps = (state) => ({
  isDetailVisible: selectionUISelectors.selectSelectedEmployee(state),
});

export default connect(mapStateToProps)(LeaveOfAbsencePeriodStatus);
