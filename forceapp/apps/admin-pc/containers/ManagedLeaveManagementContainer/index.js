import { connect } from 'react-redux';

import ManagedLeaveManagement from '../../presentational-components/ManagedLeaveManagement';

const mapStateToProps = state => ({
  isDetailVisible:
    state.managedLeaveManagement.listPane.ui.employeeList
      .selectedEmployeeId !== null,
});

export default connect(mapStateToProps)(ManagedLeaveManagement);
