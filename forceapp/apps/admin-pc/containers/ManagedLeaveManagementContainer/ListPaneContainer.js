import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  searchEmployees,
  employeesSelector,
} from '../../modules/managed-leave-management/list-pane/entities/employee-list';
import { leaveTypesSelector } from '../../modules/managed-leave-management/list-pane/entities/leave-types';
import { changeLeaveType } from '../../modules/managed-leave-management/list-pane/ui/leave-type';
import {
  updateEmployeeCodeQuery,
  updateEmployeeNameQuery,
  updateDepartmentNameQuery,
  updateWorkingTypeNameQuery,
} from '../../modules/managed-leave-management/list-pane/ui/search-form';
import { selectEmployee } from '../../modules/managed-leave-management/list-pane/ui/employee-list';
import ListPane from '../../presentational-components/ManagedLeaveManagement/ListPane';

const mapStateToProps = state => ({
  targetCompanyId: state.base.menuPane.ui.targetCompanyId,
  selectedLeaveTypeId:
    state.managedLeaveManagement.listPane.ui.leaveType.selectedLeaveTypeId,
  leaveTypes: leaveTypesSelector(state),
  employeeCodeQuery:
    state.managedLeaveManagement.listPane.ui.searchForm.employeeCodeQuery,
  employeeNameQuery:
    state.managedLeaveManagement.listPane.ui.searchForm.employeeNameQuery,
  departmentNameQuery:
    state.managedLeaveManagement.listPane.ui.searchForm.departmentNameQuery,
  workingTypeNameQuery:
    state.managedLeaveManagement.listPane.ui.searchForm.workingTypeNameQuery,
  employees: employeesSelector(state),
  isSearchExecuted:
    state.managedLeaveManagement.listPane.ui.employeeList.isSearchExecuted,
  selectedEmployeeId:
    state.managedLeaveManagement.listPane.ui.employeeList.selectedEmployeeId,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  ...bindActionCreators(
    {
      onChangeLeaveType: changeLeaveType,
      onChangeEmployeeCodeQuery: updateEmployeeCodeQuery,
      onChangeEmployeeNameQuery: updateEmployeeNameQuery,
      onChangeDepartmentNameQuery: updateDepartmentNameQuery,
      onChangeWorkingTypeNameQuery: updateWorkingTypeNameQuery,
      onSubmitSearchForm: searchEmployees,
      onClickEmployee: selectEmployee,
    },
    dispatch
  ),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onSubmitSearchForm: () =>
    dispatchProps.onSubmitSearchForm(
      stateProps.targetCompanyId,
      stateProps.selectedLeaveTypeId,
      stateProps.employeeCodeQuery,
      stateProps.employeeNameQuery,
      stateProps.departmentNameQuery,
      stateProps.workingTypeNameQuery
    ),
  onClickEmployee: (selectedEmployeeId) => {
    dispatchProps.onClickEmployee(
      selectedEmployeeId,
      stateProps.selectedLeaveTypeId
    );
  },
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  ListPane
);
