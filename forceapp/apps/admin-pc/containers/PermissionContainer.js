// @flow

import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import _ from 'lodash';

import {
  searchPermission,
  createPermission,
  updatePermission,
  deletePermission,
} from '../actions/permission';

import Permission from '../presentational-components/Permission';

import { type State } from '../reducers';

const mapStateToProps = (state: State) => ({
  searchPermission: state.searchPermission,
});

const mapDispatchToProps = (dispatch: Dispatch<any>) => {
  const alias = {
    create: createPermission,
    update: updatePermission,
    delete: deletePermission,
    search: searchPermission,
  };

  const actions = bindActionCreators(
    _.pickBy(Object.assign({}, alias, { searchPermission }), _.isFunction),
    dispatch
  );
  return { actions };
};

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(Permission): React.ComponentType<Object>);
