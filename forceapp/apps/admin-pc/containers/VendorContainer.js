import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as vendor from '../actions/vendor';
import { searchIsoCurrencyCode } from '../actions/currency';

import Vendor from '../presentational-components/Vendor';

const mapStateToProps = (state) => {
  return {
    searchVendor: state.searchVendor,
    searchIsoCurrencyCode: state.searchIsoCurrencyCode,
    value2msgkey: state.value2msgkey,
  };
};

const mapDispatchToProps = (dispatch) => {
  const alias = {
    search: vendor.searchVendor,
    create: vendor.createVendor,
    update: vendor.updateVendor,
    delete: vendor.deleteVendor,
  };

  const actions = bindActionCreators(
    _.pickBy(
      Object.assign({}, alias, vendor, { searchIsoCurrencyCode }),
      _.isFunction
    ),
    dispatch
  );
  return { actions };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Vendor);
