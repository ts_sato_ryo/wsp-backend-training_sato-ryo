import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { selectedEmployeeSelector } from '../../modules/annual-paid-leave-management/list-pane/entities/employee-list';
import { deleteGrantedPaidHoliday } from '../../modules/annual-paid-leave-management/detail-pane/entities/grant-history-list';
import {
  updateNewDaysGranted,
  execute,
  hide,
} from '../../modules/annual-paid-leave-management/update-dialog/ui';

import UpdateDialog from '../../presentational-components/AnnualPaidLeaveManagement/UpdateDialog';

const mapStateToProps = (state) => {
  const targetEmployee = selectedEmployeeSelector(state);

  return {
    targetEmployeeId:
      targetEmployee !== null && targetEmployee !== undefined
        ? targetEmployee.id
        : null,
    isVisible: state.annualPaidLeaveManagement.updateDialog.ui.isVisible,
    targetGrantHistoryRecordId:
      state.annualPaidLeaveManagement.updateDialog.ui
        .targetGrantHistoryRecordId,
    targetGrantHistoryRecordValidDateFrom:
      state.annualPaidLeaveManagement.updateDialog.ui
        .targetGrantHistoryRecordValidDateFrom,
    targetGrantHistoryRecordValidDateTo:
      state.annualPaidLeaveManagement.updateDialog.ui
        .targetGrantHistoryRecordValidDateTo,
    targetGrantHistoryRecordDaysGranted:
      state.annualPaidLeaveManagement.updateDialog.ui
        .targetGrantHistoryRecordDaysGranted,
    newDaysGranted:
      state.annualPaidLeaveManagement.updateDialog.ui.newDaysGranted,
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      onChangeNewDaysGranted: updateNewDaysGranted,
      onClickExecuteButton: execute,
      onClickCancelButton: hide,
      onClickDeleteButton: deleteGrantedPaidHoliday,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickExecuteButton: () =>
    dispatchProps.onClickExecuteButton(
      stateProps.targetEmployeeId,
      stateProps.targetGrantHistoryRecordId,
      stateProps.newDaysGranted
    ),
  onClickDeleteButton: () =>
    dispatchProps.onClickDeleteButton(
      stateProps.targetGrantHistoryRecordId,
      stateProps.targetEmployeeId
    ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(UpdateDialog);
