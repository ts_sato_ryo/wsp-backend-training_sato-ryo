import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  searchEmployees,
  employeesSelector,
} from '../../modules/annual-paid-leave-management/list-pane/entities/employee-list';
import {
  updateEmployeeCodeQuery,
  updateEmployeeNameQuery,
  updateDepartmentNameQuery,
  updateWorkingTypeNameQuery,
} from '../../modules/annual-paid-leave-management/list-pane/ui/search-form';
import { selectEmployee } from '../../modules/annual-paid-leave-management/list-pane/ui/employee-list';

import ListPane from '../../presentational-components/AnnualPaidLeaveManagement/ListPane';

const mapStateToProps = state => ({
  targetCompanyId: state.base.menuPane.ui.targetCompanyId,
  employeeCodeQuery:
    state.annualPaidLeaveManagement.listPane.ui.searchForm.employeeCodeQuery,
  employeeNameQuery:
    state.annualPaidLeaveManagement.listPane.ui.searchForm.employeeNameQuery,
  departmentNameQuery:
    state.annualPaidLeaveManagement.listPane.ui.searchForm.departmentNameQuery,
  workingTypeNameQuery:
    state.annualPaidLeaveManagement.listPane.ui.searchForm.workingTypeNameQuery,
  employees: employeesSelector(state),
  isSearchExecuted:
    state.annualPaidLeaveManagement.listPane.ui.employeeList.isSearchExecuted,
  selectedEmployeeId:
    state.annualPaidLeaveManagement.listPane.ui.employeeList.selectedEmployeeId,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  ...bindActionCreators(
    {
      onChangeEmployeeCodeQuery: updateEmployeeCodeQuery,
      onChangeEmployeeNameQuery: updateEmployeeNameQuery,
      onChangeDepartmentNameQuery: updateDepartmentNameQuery,
      onChangeWorkingTypeNameQuery: updateWorkingTypeNameQuery,
      onSubmitSearchForm: searchEmployees,
      onClickEmployee: selectEmployee,
    },
    dispatch
  ),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onSubmitSearchForm: () =>
    dispatchProps.onSubmitSearchForm(
      stateProps.targetCompanyId,
      stateProps.employeeCodeQuery,
      stateProps.employeeNameQuery,
      stateProps.departmentNameQuery,
      stateProps.workingTypeNameQuery
    ),
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(
  ListPane
);
