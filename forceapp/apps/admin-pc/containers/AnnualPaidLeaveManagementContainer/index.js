import { connect } from 'react-redux';

import AnnualPaidLeaveManagement from '../../presentational-components/AnnualPaidLeaveManagement';

const mapStateToProps = state => ({
  isDetailVisible:
    state.annualPaidLeaveManagement.listPane.ui.employeeList
      .selectedEmployeeId !== null,
});

export default connect(mapStateToProps)(AnnualPaidLeaveManagement);
