import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { catchBusinessError } from '../../commons/actions/app';
import { getUserSetting } from '../../commons/actions/userSetting';
import { selectTab } from '../../commons/actions/tab';
import { buildPermissionChecker } from '../../commons/modules/accessControl/permission';
import { getLanguagePickList } from '../actions/language';
import { searchCompany } from '../actions/company';
import { getOrganizationSetting } from '../actions/organization';
import { changeCompany, selectMenuItem } from '../modules/base/menu-pane/ui';
import { initializeEditRecord, setEditRecord } from '../actions/editRecord';
import {
  initializeDetailPane,
  showDetailPane,
} from '../modules/base/detail-pane/ui';

import Admin from '../components/Admin';

const mapStateToProps = (state) => {
  return {
    userSetting: state.common.userSetting,
    searchCompany: state.searchCompany,
    selectedTab: state.common.selectedTab,
    editRecord: state.editRecord,
    tmpEditRecord: state.tmpEditRecord,
    hasPermission: buildPermissionChecker(state),
  };
};

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        onSelectMenuItem: selectMenuItem,
        onChangeCompany: (param) => (thunkDispatch) => {
          thunkDispatch(changeCompany(param));
          thunkDispatch(showDetailPane(false));
        },
      },
      dispatch
    ),
    actions: bindActionCreators(
      {
        catchBusinessError,
        getOrganizationSetting,
        getUserSetting,
        getLanguagePickList,
        initializeDetailPane,
        initializeEditRecord,
        setEditRecord,
        searchCompany,
        selectTab,
      },
      dispatch
    ),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Admin);
