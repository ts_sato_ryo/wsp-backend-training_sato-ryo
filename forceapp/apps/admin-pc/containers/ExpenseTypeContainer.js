import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as expenseType from '../actions/expenseType';
import { searchExpTypeGroup } from '../actions/expTypeGroup';
import { searchTaxType } from '../actions/taxType';
import { searchExtendedItem } from '../actions/extendedItem';
import { searchCurrency } from '../actions/currency';

import ExpenseType from '../presentational-components/ExpenseType';

const mapStateToProps = (state) => {
  return {
    editRecord: state.editRecord,
    searchExpTypeGroup: state.searchExpTypeGroup,
    searchTaxType: state.searchTaxType,
    searchExtendedItem: state.searchExtendedItem,
    searchExpenseType: state.searchExpenseType,
    value2msgkey: state.value2msgkey,
  };
};

const mapDispatchToProps = (dispatch) => {
  const alias = {
    create: expenseType.createExpenseType,
    update: expenseType.updateExpenseType,
    delete: expenseType.deleteExpenseType,
    search: expenseType.searchExpenseType,
  };

  const actions = bindActionCreators(
    _.pickBy(
      Object.assign({}, alias, expenseType, {
        searchExpTypeGroup,
        searchTaxType,
        searchExtendedItem,
        searchCurrency,
      }),
      _.isFunction
    ),
    dispatch
  );
  return { actions };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpenseType);
