import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { catchBusinessError, confirm } from '../../commons/actions/app';
import * as editRecord from '../actions/editRecord';
import * as getSFObjFieldValues from '../actions/sfObjFieldValues';
import {
  initialize,
  create,
  appendHistory,
  updateBase,
  updateHistory,
  removeBase,
  removeHistory,
  showDetail,
  hideDetail,
  showRevisionDialog,
  startEditingNewRecord,
  startEditingBase,
  startEditingHistory,
  cancelEditing,
  changeHistory,
  changeRecordValue,
  changeRecordHistoryValue,
} from '../action-dispatchers/Edit';

import ContentsSelector from '../components/Admin/ContentsSelector';
import {
  showDetailPane,
  setModeBase,
  setModeHistory,
} from '../modules/base/detail-pane/ui';

const mapStateToProps = (state) => {
  return {
    editRecord: state.editRecord,
    editRecordHistory: state.editRecordHistory,
    getOrganizationSetting: state.getOrganizationSetting,
    searchHistory: state.searchHistory,
    sfObjFieldValues: state.sfObjFieldValues,
    tmpEditRecord: state.tmpEditRecord,
    tmpEditRecordHistory: state.tmpEditRecordHistory,
    isShowDetail: state.detailPane.ui.isShowDetail,
    modeBase: state.detailPane.ui.modeBase,
    modeHistory: state.detailPane.ui.modeHistory,
  };
};

const mapDispatchToProps = (dispatch) => {
  const commonActions = bindActionCreators(
    _.pickBy(
      Object.assign(
        {},
        { catchBusinessError, confirm },
        editRecord,
        { showDetailPane, setModeBase, setModeHistory },
        getSFObjFieldValues,
        {
          initialize,
          create,
          appendHistory,
          showDetail,
          hideDetail,
          showRevisionDialog,
          updateBase,
          updateHistory,
          removeBase,
          removeHistory,
          startEditingNewRecord,
          startEditingBase,
          startEditingHistory,
          cancelEditing,
          changeHistory,
          changeRecordValue,
          changeRecordHistoryValue,
        }
      ),
      _.isFunction
    ),
    dispatch
  );
  return { commonActions };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContentsSelector);
// export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ContentsSelector);
