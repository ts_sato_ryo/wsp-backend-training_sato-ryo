/* flow */
import { connect } from 'react-redux';
import { get } from 'lodash';
import { initialiseOptions, setOptions } from '../modules/groupReportType';
import DualListbox from '../presentational-components/Group/DualListbox';
import { changeRecordValue } from '../action-dispatchers/Edit';

const mapStateToProps = (state, ownProps) => ({
  disabled: ownProps.disabled,
  groups: state.searchEmployeeGroup,
  groupReportType: state.groupReportType,
  reportTypes: state.searchReportType,
  tmpEditRecord: state.tmpEditRecord,
});

const mapDispatchToProps = (dispatch) => ({
  initialiseOptions: (ids, reportTypes) =>
    dispatch(initialiseOptions(ids, reportTypes)),
  onChangeDetailItem: (key, value, charType) =>
    dispatch(changeRecordValue(key, value, charType)),
  setOptions: (ids) => {
    dispatch(setOptions(ids));
  },
});

const mergeProps = (stateProps, dispatchProps) => {
  return {
    ...stateProps,
    ...dispatchProps,
    initialiseOptions: () => {
      const selectedGroup = stateProps.groups.find(
        (group) => group.id === stateProps.tmpEditRecord.id
      );
      const selectedReportTypeIds =
        get(selectedGroup, 'reportTypeIdList', []) === null
          ? []
          : get(selectedGroup, 'reportTypeIdList', []);
      return dispatchProps.initialiseOptions(
        selectedReportTypeIds,
        stateProps.reportTypes
      );
    },
  };
};

const DualListboxContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DualListbox);

export default DualListboxContainer;
