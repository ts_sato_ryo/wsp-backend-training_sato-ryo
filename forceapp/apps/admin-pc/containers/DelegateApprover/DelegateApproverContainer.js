import { connect } from 'react-redux';

import { actions as uiActionsDA } from '../../modules/delegateApprover/ui/assignment';
import { actions as actionsDA } from '../../modules/delegateApprover/entities/assignment';

import DelegateAssignemnt from '../../presentational-components/Employee/DelegateApprover/DelegateAssignment';

const mapStateToProps = (state) => ({
  isEmployeeSelection: state.delegateApprover.ui.assignment.isEmployeeSelection,
  isNewAssignment: state.delegateApprover.ui.assignment.isNewAssignment,
  delegateAssignments: state.delegateApprover.entities.assignment,
  selectedEmployees: state.delegateApprover.ui.assignment.selectedEmployees,
  employeeId: state.editRecord.id,
});

const mapDispatchToProps = {
  save: actionsDA.save,
  list: actionsDA.list,
  openEmployeeSelection: uiActionsDA.openEmployeeSelection,
  cancelDA: uiActionsDA.cancelNewAssignment,
  removeDA: uiActionsDA.removeFromSelectedEmployees,
  removeFromExcludedEmployees: uiActionsDA.removeFromExcludedEmployees,
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  openEmployeeSearchDialog: () => {
    dispatchProps.openEmployeeSelection(stateProps.openEmployeeSelection);
  },
  listDA: () => dispatchProps.list(stateProps.employeeId),
  saveDA: (delegateAssignments) => {
    dispatchProps
      .save(stateProps.employeeId, delegateAssignments)
      .then(() => dispatchProps.cancelDA());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(DelegateAssignemnt);
