// @flow
import { connect } from 'react-redux';

import { actions as uiActionsDA } from '../../modules/delegateApprover/ui/assignment';

import EmployeesSearchDialog from '../../presentational-components/Employee/DelegateApprover/EmployeesSearchDialog';

import type { State } from '../../reducers';

const mapStateToProps = (state: State) => ({
  foundEmployees: state.delegateApprover.ui.assignment.foundEmployees,
  selectedEmployees: state.delegateApprover.ui.assignment.selectedEmployees,
  excludedEmployees: state.delegateApprover.ui.assignment.excludedEmployees,
  delegateAssignments: state.delegateApprover.entities.assignment,
  currentEmpId: state.editRecord.id,
  companyId: state.base.menuPane.ui.targetCompanyId,
});

const mapDispatchToProps = {
  cancel: uiActionsDA.cancelEmployeeSelection,
  search: uiActionsDA.searchEmployees,
  selectEmployees: uiActionsDA.selectEmployees,
};
const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  select: (employees) => dispatchProps.selectEmployees(employees),
  search: (query) => {
    const excludedEmployees = stateProps.excludedEmployees;
    const excludedIds = [stateProps.currentEmpId].concat(excludedEmployees);
    dispatchProps.search(
      {
        ...query,
        targetDate: new Date().toISOString().slice(0, 10),
        companyId: stateProps.companyId,
      },
      excludedIds
    );
  },
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(EmployeesSearchDialog): React.ComponentType<Object>);
