import { connect } from 'react-redux';

import Calendar from '../../presentational-components/Calendar';
import { actions as calendarActions } from '../../modules/calendar';
import { actions as calendarListEntityActions } from '../../modules/calendar/entities/calendarList';

const mapStateToProps = (state) => ({
  companyId: state.base.menuPane.ui.targetCompanyId,
  isDetailVisible: state.base.detailPane.ui.isShowDetail,
});

const mapDispatchToProps = (dispatch) => ({
  onInitialize: (stateProps) => {
    dispatch(calendarListEntityActions.clear());
    dispatch(calendarActions.getConstantsCalendar());
    dispatch(calendarListEntityActions.fetch(stateProps.companyId));
  },
});

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onInitialize: () => dispatchProps.onInitialize(stateProps),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Calendar);
