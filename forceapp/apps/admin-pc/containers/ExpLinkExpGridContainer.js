import { connect } from 'react-redux';

import ExpenseTypeGrid from '../components/ExpenseTypeLinkConfig/ExpenseTypeGrid';
import {
  actions as expTypeLinkConfigActions,
  setSelectedExp,
} from '../modules/expTypeLinkConfig/ui';
import { changeRecordValue } from '../action-dispatchers/Edit';

const mapStateToProps = (state, ownProps) => ({
  selectedExpense: state.expTypeLinkConfig.ui.selectedExpense,
  disabled: ownProps.disabled,
  config: ownProps.config,
  selectedId: state.tmpEditRecord.id,
  expenseTypes: state.searchExpenseType,
});

const mapDispatchToProps = {
  toggleSelectedExp: expTypeLinkConfigActions.toggleSelectedExp,
  removeFromSelectedExpense: expTypeLinkConfigActions.removeFromSelectedExpense,
  setSelectedExp,
  cleanSelectedExpense: expTypeLinkConfigActions.cleanSelectedExpense,
  changeRecordValue,
};

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onChangeDetailItem: (key, value, charType) =>
    dispatchProps.changeRecordValue(key, value, charType),

  remove: () =>
    dispatchProps.removeFromSelectedExpense(stateProps.selectedExpense),
  setSelectedExp: (selectedId) => {
    const expenseType = stateProps.expenseTypes;
    let selectedExpId = [];
    if (selectedId) {
      selectedExpId =
        expenseType.find((exp) => exp.id === selectedId).expTypeChildIds || [];
    }
    dispatchProps.setSelectedExp(selectedExpId, expenseType);
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(ExpenseTypeGrid);
