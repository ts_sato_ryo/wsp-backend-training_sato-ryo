import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as workCategory from '../actions/workCategory';
import { searchEmployee } from '../actions/employee';

import WorkCategory from '../presentational-components/WorkCategory';

const mapStateToProps = (state) => {
  return {
    searchWorkCategory: state.searchWorkCategory,
  };
};

const mapDispatchToProps = (dispatch) => {
  const alias = {
    create: workCategory.createWorkCategory,
    update: workCategory.updateWorkCategory,
    delete: workCategory.deleteWorkCategory,
    search: workCategory.searchWorkCategory,
  };

  const actions = bindActionCreators(
    _.pickBy(
      Object.assign(
        {},
        alias,
        workCategory,
        { searchEmployee },
      ), _.isFunction
    ), dispatch
  );
  return { actions };
};

export default connect(mapStateToProps, mapDispatchToProps)(WorkCategory);
