import jsforce from 'jsforce';
import SALESFORCE_API_VERSION from '../config/salesforceApiVersion';

function getRemoting() {
  // eslint-disable-next-line no-undef, camelcase
  return __SF_NAMESPACE__RemoteApiController;
}

let connection;

function getConnection() {
  if (!connection) {
    connection = new jsforce.Connection({
      accessToken: window.sfSessionId,
      version: SALESFORCE_API_VERSION,
    });
  }
  return connection;
}

export default class ApiForVfp {
  /**
   * API リクエスト
   * @param {object} req { path, param } から成るオブジェクト
   */
  static invoke(req) {
    console.log('API接続環境: vfp');
    return new Promise((resolve, reject) => {
      getRemoting().invoke(
        JSON.stringify(req),
        (responseStr, event) => {
          if (event.status) {
            // RemoteAction成功
            // レスポンスはJSON形式のString型で返ってくるので、必ずparseする
            const response = JSON.parse(responseStr);

            // 実行結果でPromiseをresolve/rejectする
            if (response.isSuccess) {
              resolve(response.result);
            } else {
              reject(response.error);
            }
          } else {
            console.error(event.message, event.where);
            reject({ type: 'WSP_PRODUCTION_APEX_ERROR', event });
          }
        },
        { escape: false, buffer: false }
      );
    });
  }

  /**
   * SFAPI Request
   * @param {string} reqestPath
   * @param {object} param
   */
  static requestSFApi = (requestPath, param) =>
    new Promise((resolve, reject) => {
      const conn = getConnection();
      conn
        .requestPost(requestPath, param)
        // TODO: add error logic.
        .then((response) => resolve(response))
        .catch((err) => reject(err));
    });
}
