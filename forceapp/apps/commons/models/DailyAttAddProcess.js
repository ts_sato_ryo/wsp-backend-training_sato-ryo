/* @flow */
/**
 * 日次勤怠の編集を行った後の追加処理の情報を発行します。
 *
 * 戻り値を変換するモデルは別に定義してあります。
 * 追加処理自体を共通化させるためのモデルです。
 */
export type ProcessType = 'INSUFFICIENT_REST_TIME';

export const PROCESS_TYPE = {
  INSUFFICIENT_REST_TIME: 'INSUFFICIENT_REST_TIME',
};

type InsufficientRestTimeProcess = {
  type: 'INSUFFICIENT_REST_TIME',
  payload: {
    employeeId?: ?string,
    targetDate?: ?string,
    insufficientRestTime: number,
  },
};

export type DailyAttAddProcess = InsufficientRestTimeProcess;

type Payload = $PropertyType<InsufficientRestTimeProcess, 'payload'>;

export const createDailyAttAddProcess = (
  type: ProcessType,
  payload: Payload
): ?DailyAttAddProcess => {
  return { type, payload };
};
