// @flow
// TODO 後々 Domain の repository になる。
import Api from '../api';

export type SearchParamType = {|
  id?: string,
  companyId?: string,
  targetDate?: string,
  code?: string,
  name?: string,
  departmentId?: string,
  departmentCode?: string,
  departmentName?: string,
  title?: string,
  managerName?: string,
  workingTypeName?: string,
  approvalAuthority01?: boolean,
|};

const search = (param: SearchParamType) =>
  Api.invoke({
    path: '/employee/search',
    param: {
      ...param,
      targetDate: param.targetDate || null,
    },
  });

type SearchApproverEmployeeParamType = $Diff<
  SearchParamType,
  { approvalAuthority01?: boolean }
>;

// eslint-disable-next-line import/prefer-default-export
export const searchApproverEmployee = (
  param: SearchApproverEmployeeParamType
) => search({ ...param, approvalAuthority01: true });
