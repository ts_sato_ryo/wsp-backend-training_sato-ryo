import moment from 'moment-timezone';
import { eventsFromRemote } from './mocks/eventsFromRemote';
import { toViewModel } from '../Converter';

jest.mock('moment-timezone', () => {
  const mockMoment = jest.requireActual('moment-timezone');
  mockMoment.tz.setDefault('UTC');
  return mockMoment;
});

describe('DailySummaryEvent', () => {
  describe('toViewModel', () => {
    const convertedEvents = toViewModel(eventsFromRemote, moment('2018-05-11'));

    test('イベントの並び順は、[開始日時が早い順] > [期間が長い順]であること', () => {
      // 前日からの予定 > 終日イベント > 特定時刻のイベント の順
      expect(convertedEvents.length).toBe(8);
      expect(convertedEvents[0].title).toBe('終日イベント 05/10-05/11');
      expect(convertedEvents[1].title).toBe(
        '日跨ぎイベント 05/10 22:00-05/11 10:00'
      );
      expect(convertedEvents[2].title).toBe('終日イベント 05/11-05/12');
      expect(convertedEvents[3].title).toBe('終日イベント 05/11');
      expect(convertedEvents[4].title).toBe('イベント 09:00-12:00');
      expect(convertedEvents[5].title).toBe('イベント 09:00-10:00');
      expect(convertedEvents[6].title).toBe('イベント 11:00-12:00');
      expect(convertedEvents[7].title).toBe(
        '日跨ぎイベント 05/11 22:00-05/12 10:00'
      );
    });

    test('各値が正しく格納されていること', () => {
      const targetEvent = convertedEvents[4];
      expect(targetEvent.title).toBe('イベント 09:00-12:00');
      expect(targetEvent.start.format()).toBe('2018-05-11T09:00:00Z');
      expect(targetEvent.end.format()).toBe('2018-05-11T12:00:00Z');
      expect(targetEvent.isAllDay).toBe(false);
      expect(targetEvent.isOrganizer).toBe(eventsFromRemote[0].isOrganizer);
      expect(targetEvent.location).toBe(eventsFromRemote[0].location);
      expect(targetEvent.isOuting).toBe(eventsFromRemote[0].isOuting);
      expect(targetEvent.createdServiceBy).toBe(
        eventsFromRemote[0].createdServiceBy
      );
      expect(targetEvent.job.id).toBe(eventsFromRemote[0].jobId);
      expect(targetEvent.job.code).toBe(eventsFromRemote[0].jobCode);
      expect(targetEvent.job.name).toBe(eventsFromRemote[0].jobName);
      expect(targetEvent.workCategoryId).toBe(
        eventsFromRemote[0].workCategoryId
      );
      expect(targetEvent.workCategoryName).toBe(
        eventsFromRemote[0].workCategoryName
      );
    });

    test('終日予定: レイアウト情報が正しく格納されること', () => {
      const targetEvent = convertedEvents[0];
      expect(targetEvent.layout.startMinutesOfDay).toBe(0);
      expect(targetEvent.layout.endMinutesOfDay).toBe(0);
      expect(targetEvent.layout.containsAllDay).toBe(true);
    });

    test('非終日予定: レイアウト情報が正しく格納されること', () => {
      const targetEvent = convertedEvents[4];
      expect(targetEvent.layout.startMinutesOfDay).toBe((9 * 60 * 2) / 3); // カレンダー表示の3分の2
      expect(targetEvent.layout.endMinutesOfDay).toBe((12 * 60 * 2) / 3);
      expect(targetEvent.layout.containsAllDay).toBe(false);
    });

    test('日跨ぎ予定: レイアウト情報が正しく格納されること', () => {
      const targetEvent = convertedEvents[1];
      expect(targetEvent.layout.startMinutesOfDay).toBe(0);
      expect(targetEvent.layout.endMinutesOfDay).toBe((10 * 60 * 2) / 3);
      expect(targetEvent.layout.containsAllDay).toBe(false);
    });
  });
});
