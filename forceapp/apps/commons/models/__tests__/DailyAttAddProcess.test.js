import { createDailyAttAddProcess, PROCESS_TYPE } from '../DailyAttAddProcess';

describe('createDailyAttAddProcess', () => {
  test('type INSUFFICIENCY_REST_TIME', () => {
    const process = createDailyAttAddProcess(
      PROCESS_TYPE.INSUFFICIENT_REST_TIME,
      {
        insufficientRestTime: 15,
      }
    );

    expect(process.type).toBe(PROCESS_TYPE.INSUFFICIENT_REST_TIME);
    expect(process.payload.insufficientRestTime).toBe(15);
  });
});
