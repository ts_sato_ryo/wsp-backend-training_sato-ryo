import React from 'react';
import { storiesOf } from '@storybook/react';
import PopupWindowPage from '../components/PopupWindowPage';
import PopupWindowNavbar from '../components/PopupWindowNavbar';

storiesOf('commons', module).add(
  'PopupWindowPage',
  () => (
    <div>
      <PopupWindowNavbar title="Page Title" />
      <PopupWindowPage>Page Content</PopupWindowPage>
    </div>
  ),
  {
    info: {
      text: `
Container component for contents in popup windows. See also PopupWindowNavbar.
    `,
      propTables: [PopupWindowPage],
      inline: true,
      source: true,
    },
  }
);
