// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import ConfirmDialog from '../../components/dialogs/ConfirmDialog';

storiesOf('commons/dialogs', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'ConfirmDialog',
    () => (
      <ConfirmDialog
        onClickCancel={action('キャンセルする操作')}
        onClickOk={action('OKする操作')}
      >
        {'人生をやり直しすチャンスです!'}
      </ConfirmDialog>
    ),
    {
      info: {
        text: '人生を異世界でやり直しますか?',
        propTables: [ConfirmDialog],
        inline: false,
        source: true,
      },
    }
  );

storiesOf('commons/dialogs', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'ConfirmDialog - 複数行',
    () => (
      <ConfirmDialog
        onClickCancel={action('キャンセルする操作')}
        onClickOk={action('OKする操作')}
      >
        {'人生をやり直しすチャンスです!\n異世界転生しますか?'}
      </ConfirmDialog>
    ),
    {
      info: {
        text: '人生を異世界でやり直しますか?',
        propTables: [ConfirmDialog],
        inline: false,
        source: true,
      },
    }
  );
