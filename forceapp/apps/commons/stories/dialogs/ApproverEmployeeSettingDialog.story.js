// @flow
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import ApproverEmployeeSettingDialog from '../../components/dialogs/ApproverEmployeeSettingDialog';

storiesOf('commons/dialogs', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'dialogs/ApproverEmployeeSettingDialog - 書き換え可能',
    () => {
      return (
        <ApproverEmployeeSettingDialog
          isHide={false}
          isReadOnly={false}
          isEdited
          approverEmployeeName="１人目の承認者"
          handleCancel={action('Hide Dialog')}
          handleSave={action('Save')}
          handleChangeEmployee={action('Change Employee')}
        />
      );
    },
    {
      info: {
        text: '現在の承認ステップと承認者を表示します。',
        propTables: [ApproverEmployeeSettingDialog],
        inline: false,
        source: true,
      },
    }
  )
  .add(
    'dialogs/ApproverEmployeeSettingDialog - 未変更による書き換え不可',
    () => {
      return (
        <ApproverEmployeeSettingDialog
          isHide={false}
          isReadOnly={false}
          isEdited={false}
          approverEmployeeName="１人目の承認者"
          handleCancel={action('Hide Dialog')}
          handleSave={action('Save')}
          handleChangeEmployee={action('Change Employee')}
        />
      );
    },
    {
      info: '現在の承認ステップと承認者を表示します。',
    }
  )
  .add(
    'dialogs/ApproverEmployeeSettingDialog - 書き換え不可',
    () => {
      return (
        <ApproverEmployeeSettingDialog
          isHide={false}
          isReadOnly
          isEdited={false}
          approverEmployeeName="１人目の承認者"
          handleCancel={action('Hide Dialog')}
        />
      );
    },
    {
      info: {
        text: '現在の承認ステップと承認者を表示します。',
        propTables: [ApproverEmployeeSettingDialog],
        inline: false,
        source: true,
      },
    }
  );
