import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DialogDecorator from '../../../../.storybook/decorator/Dialog';

import ErrorDialog from '../../components/dialogs/ErrorDialog';
import BaseWSPError from '../../errors/BaseWSPError';

storiesOf('commons/dialogs', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'ErrorDialog',
    () => (
      <ErrorDialog
        error={
          new BaseWSPError(
            'データ不正エラー',
            '予期せぬエラーです',
            'システム管理者にお問い合わせください'
          )
        }
        handleClose={action('閉じる操作')}
      />
    ),
    {
      info: {
        text: '回復方法あり',
        propTables: [ErrorDialog],
        inline: false,
        source: true,
      },
    }
  );

storiesOf('commons/dialogs', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'ErrorDialog - 複数行',
    () => (
      <ErrorDialog
        error={
          new BaseWSPError(
            'データ不正エラー',
            '勤務体系が設定されていないため、勤務表を表示できませんでした',
            'システム管理者にお問い合わせください'
          )
        }
        handleClose={action('閉じる操作')}
      />
    ),
    {
      info: {
        text: '回復方法あり',
        propTables: [ErrorDialog],
        inline: false,
        source: true,
      },
    }
  );

storiesOf('commons/dialogs', module)
  .addDecorator((story) => <DialogDecorator>{story()}</DialogDecorator>)
  .add(
    'ErrorDialog - 回復方法無し',
    () => (
      <ErrorDialog
        error={new BaseWSPError('データ不正エラー', '予期せぬエラーです')}
        handleClose={action('閉じる操作')}
      />
    ),
    {
      info: {
        text: '回復方法無し',
        propTables: [ErrorDialog],
        inline: false,
        source: true,
      },
    }
  );
