import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import uuid from 'uuid/v1';

import DialogFrame from '../../components/dialogs/DialogFrame';
import Button from '../../components/buttons/Button';

import './DialogFrame.story.scss';

const dummyIconImage = require('../../images/HeaderIconInfo.png');

const focusId = uuid();

class DialogContainer extends React.Component {
  constructor() {
    super();

    this.state = {
      open: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState((prevState) => ({
      open: !prevState.open,
    }));
  }

  render() {
    return (
      <div>
        <button onClick={this.toggle}>open dialog</button>

        {this.state.open ? (
          <DialogFrame
            className="commons-dialog-frame-story"
            titleIcon={dummyIconImage}
            title="ダイアログタイトル"
            hide={this.toggle}
            initialFocus={focusId}
            headerSub={
              <div className="commons-dialog-frame-story__header-sub">
                <Button>ツール</Button>
              </div>
            }
            footer={
              <DialogFrame.Footer
                sub={<Button type="destructive">削除</Button>}
              >
                <Button type="default">キャンセル</Button>
                <Button type="primary" id={focusId}>
                  保存
                </Button>
              </DialogFrame.Footer>
            }
          >
            <div>
              初期フォーカスが保存ボタンになっています。
              <br />
              上部の×ボタンをおして閉じると、元のボタンにフォーカスが戻ります。
            </div>
          </DialogFrame>
        ) : null}
      </div>
    );
  }
}

storiesOf('commons/dialogs', module)
  .add(
    'DialogFrame',
    () => {
      return (
        <DialogFrame
          className="commons-dialog-frame-story"
          titleIcon={dummyIconImage}
          title="ダイアログタイトル"
          hide={action('閉じるボタンクリック')}
          headerSub={
            <div className="commons-dialog-frame-story__header-sub">
              <Button>ツール</Button>
            </div>
          }
          footer={
            <DialogFrame.Footer sub={<Button type="destructive">削除</Button>}>
              <Button type="default">キャンセル</Button>
              <Button type="primary">保存</Button>
            </DialogFrame.Footer>
          }
        >
          <div>&lt;SomeComponent&gt;</div>
        </DialogFrame>
      );
    },
    {
      info: {
        text: 'タイトル文言／アイコン／適当な内容',
        propTables: [DialogFrame],
        inline: false,
        source: true,
      },
    }
  )
  .add(
    'DialogFrame - focus control',
    () => {
      return <DialogContainer />;
    },
    { info: { propTables: false, inline: false, source: true } }
  );
