// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { boolean, withKnobs } from '@storybook/addon-knobs';

import JobSelectDialog from '../../../components/dialogs/DailySummaryDialog/JobSelectDialog';
import jobList from '../../mocks/jobList';

storiesOf('commons/dialogs/DailySummaryDialog', module)
  .addDecorator(withKnobs)
  .add(
    'JobSelectDialog',
    () => (
      <JobSelectDialog
        jobTree={jobList}
        opening={boolean('opening', true)}
        onClickItem={action('onClickItem')}
        onClickClose={action('onClickClose')}
      />
    ),
    {
      info: {
        propTables: [JobSelectDialog],
        inline: false,
        source: true,
      },
    }
  );
