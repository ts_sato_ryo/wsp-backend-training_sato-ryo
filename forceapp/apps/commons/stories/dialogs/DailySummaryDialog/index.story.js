// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import moment from 'moment';

import DailySummaryDialog from '../../../components/dialogs/DailySummaryDialog';
import taskList from '../../mocks/taskList';
import eventList from '../../mocks/eventList';
import jobList from '../../mocks/jobList';

storiesOf('commons/dialogs', module).add(
  'DailySummaryDialog',
  () => (
    <DailySummaryDialog
      note="NOTE"
      timestampComment="COMMENT"
      taskList={taskList}
      events={eventList}
      workCategoryLists={{}}
      defaultJobId=""
      isLocked={false}
      isTemporaryWorkTime={false}
      realWorkTime={500}
      selectedDay={moment()}
      isOpeningJobSelectDialog={false}
      // isEnableEndStamp={null}
      moveDay={action('moveDay')}
      showJobSelectDialog={action('showJobSelectDialog')}
      saveDailySummary={action('saveDailySummary')}
      saveDailySummaryAndRecordEndTimestamp={action(
        'saveDailySummaryAndRecordEndTimestamp'
      )}
      hideDialog={action('hideDialog')}
      deleteTask={action('deleteTask')}
      editTask={action('editTask')}
      editTaskTime={action('editTaskTime')}
      toggleDirectInput={action('toggleDirectInput')}
      editNote={action('editNote')}
      editTimestampComment={action('editTimestampComment')}
      reorderTaskList={action('reorderTaskList')}
      openEventListPopup={action('openEventListPopup')}
      closeJobSelectDialog={action('closeJobSelectDialog')}
      fetchJobTree={action('fetchJobTree')}
      jobTree={jobList}
    />
  ),
  {
    info: {
      text: '人生を異世界でやり直しますか?',
      propTables: [DailySummaryDialog],
      inline: false,
      source: true,
    },
  }
);
