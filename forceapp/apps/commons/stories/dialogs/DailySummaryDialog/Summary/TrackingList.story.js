import React from 'react';
import { storiesOf } from '@storybook/react';

import TrackingList from '../../../../components/dialogs/DailySummaryDialog/Summary/TrackingList';

storiesOf('commons/dialogs/DailySummaryDialog', module).add(
  'TrackingList',
  () => <TrackingList />,
  { info: { propTables: [TrackingList], inline: true, source: true } }
);
