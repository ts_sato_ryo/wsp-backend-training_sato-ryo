import React from 'react';
import { storiesOf } from '@storybook/react';

import Collapse from '../../../../components/dialogs/DailySummaryDialog/Summary/Collapse';

import './Collapse.story.scss';

const ROOT = 'commons-story-daily-summary-dailg-summary-collapse';

storiesOf('commons', module)
  .add(
    'Collapse',
    () => (
      <Collapse header="Collpase Sample">
        <div className={`${ROOT}__children`}>コンテンツ</div>
      </Collapse>
    ),
    { info: { propTables: [Collapse], inline: true, source: true } }
  )
  .add(
    'Collapse - with Summary',
    () => (
      <Collapse
        header="Collpase Sample"
        summary={<span className={`${ROOT}__summary-sample`}>Sample</span>}
      >
        <div className={`${ROOT}__children`}>コンテンツ</div>
      </Collapse>
    ),
    { info: { propTables: false, inline: true, source: true } }
  );
