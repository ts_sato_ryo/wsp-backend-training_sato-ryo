import React from 'react';
import { storiesOf } from '@storybook/react';

import ButtonGroups from '../../components/buttons/ButtonGroups';
import Button from '../../components/buttons/Button';

storiesOf('commons/buttons', module).add(
  'ButtonGroups',
  () => (
    <ButtonGroups>
      <Button>ボタン1</Button>
      <Button>ボタン2</Button>
    </ButtonGroups>
  ),
  { info: { propTables: [ButtonGroups], inline: true, source: true } }
);
