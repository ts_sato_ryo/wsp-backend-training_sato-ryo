import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from '../../components/buttons/Button';

import ImgIconDummy from '../../images/iconClock.png';

storiesOf('commons/buttons', module)
  .add(
    'Button',
    () => <Button onClick={action('ボタンクリック')}>Default</Button>,
    {
      info: {
        text: `
    デフォルト表示（type指定なし）
    `,
        propTables: [Button],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Button - type:outline-default',
    () => (
      <div style={{ padding: 20, backgroundColor: '#f2f2f2' }}>
        <Button type="outline-default" onClick={action('ボタンクリック')}>
          Primary
        </Button>
      </div>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - type:primary',
    () => (
      <Button type="primary" onClick={action('ボタンクリック')}>
        Primary
      </Button>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - type:secondary',
    () => (
      <Button type="secondary" onClick={action('ボタンクリック')}>
        Secondary
      </Button>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - type:destructive',
    () => (
      <Button type="destructive" onClick={action('ボタンクリック')}>
        Destructive
      </Button>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - type:text',
    () => (
      <Button type="text" onClick={action('ボタンクリック')}>
        Destructive
      </Button>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - disabled',
    () => (
      <div>
        <Button type="default" onClick={action('Defaultクリック')} disabled>
          Default
        </Button>
        <Button
          type="outline-default"
          onClick={action('Outline Defultクリック')}
          disabled
        >
          Outline
        </Button>
        <Button type="primary" onClick={action('Primaryクリック')} disabled>
          Primary
        </Button>
        <Button type="secondary" onClick={action('Secondaryクリック')} disabled>
          Secondary
        </Button>
        <Button
          type="destructive"
          onClick={action('Destructiveクリック')}
          disabled
        >
          Destructive
        </Button>
        <Button type="text" onClick={action('textクリック')} disabled>
          Destructive
        </Button>
      </div>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - submit',
    () => (
      <form onSubmit={action('submit')}>
        <input type="text" />
        <Button type="primary" submit>
          送信
        </Button>
      </form>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - with icon',
    () => (
      <Button
        onClick={action('ボタンクリック')}
        iconSrc={ImgIconDummy}
        iconAlt="image"
      >
        with
      </Button>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - with icon (align:right)',
    () => (
      <Button
        onClick={action('ボタンクリック')}
        iconSrc={ImgIconDummy}
        iconAlt="image"
        iconAlign="right"
      >
        with
      </Button>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Button - with css class',
    () => (
      <div className="slds-clearfix">
        <Button
          className="slds-float--right"
          type="primary"
          onClick={action('Primaryクリック')}
        >
          Default
        </Button>
      </div>
    ),
    { info: { propTables: false, inline: true, source: true } }
  );
