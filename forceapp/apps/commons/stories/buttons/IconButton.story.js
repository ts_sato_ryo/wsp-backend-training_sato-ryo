import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import IconButton from '../../components/buttons/IconButton';

import ImgBtnAdd from '../../images/btn_add.png';

storiesOf('commons/buttons', module)
  .add(
    'IconButton',
    () => (
      <IconButton
        src={ImgBtnAdd}
        alt="追加ボタン"
        onClick={action('ボタンクリック')}
      />
    ),
    { info: { propTables: [IconButton], inline: true, source: true } }
  )
  .add(
    'IconButton - no alt',
    () => <IconButton src={ImgBtnAdd} onClick={action('ボタンクリック')} />,
    {
      info: {
        text: `
    altを指定しない場合、空文字がaltに付与されます
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'IconButton - disabled',
    () => (
      <IconButton
        disabled
        src={ImgBtnAdd}
        alt="追加ボタン"
        onClick={action('ボタンクリック')}
      />
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'IconButton - with css class',
    () => (
      <div className="slds-clearfix">
        <IconButton
          className="slds-float--right"
          src={ImgBtnAdd}
          alt="追加ボタン"
          onClick={action('ボタンクリック')}
        />
      </div>
    ),
    { info: { propTables: false, inline: true, source: true } }
  );
