import React from 'react';
import { storiesOf } from '@storybook/react';

import SingleToggleButton from '../../components/buttons/SingleToggleButton';

import ImgIconPencilOff from '../../images/iconPencilOff.png';
import ImgIconPencilOn from '../../images/iconPencilOn.png';

storiesOf('commons/buttons', module).add(
  'SingleToggleButton',
  () => (
    <SingleToggleButton
      iconSrcActive={ImgIconPencilOn}
      iconSrcDisabled={ImgIconPencilOff}
    />
  ),
  { info: { propTables: [SingleToggleButton], inline: true, source: true } }
);
