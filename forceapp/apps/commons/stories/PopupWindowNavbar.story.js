import React from 'react';
import { storiesOf } from '@storybook/react';

import PopupWindowNavbar from '../components/PopupWindowNavbar';
import Button from '../components/buttons/Button';

storiesOf('commons', module)
  .add(
    'PopupWindowNavbar',
    () => <PopupWindowNavbar title="Popup Window Title" />,
    {
      info: {
        text: `
Navigation bar component for popup windows. See also PopupWindowPage.
    `,
        propTables: [PopupWindowNavbar],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'PopupWindowNavbar - with buttons',
    () => (
      <PopupWindowNavbar title="Popup Window Title">
        <Button>Page Action</Button>
      </PopupWindowNavbar>
    ),
    {
      info: {
        text: `
You can place your components on right side of the title.
    `,
        propTables: [PopupWindowNavbar],
        inline: true,
        source: true,
      },
    }
  );
