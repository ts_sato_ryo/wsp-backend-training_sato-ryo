import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import LookupField from '../../components/fields/LookupField';

const dummyData = [
  { value: 'value1', text: '選択肢1' },
  { value: 'value2', text: '選択肢2' },
  { value: 'value3', text: '選択肢3' },
  { value: 'value4', text: '選択肢4' },
];

storiesOf('commons/fields', module).add(
  'LookupField',
  () => (
    <LookupField
      id="some-id"
      options={dummyData}
      onClickSearchButton={action('検索ボタンクリック')}
    />
  ),
  { info: { propTables: [LookupField], inline: true, source: true } }
);
