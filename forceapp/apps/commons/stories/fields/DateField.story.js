import React from 'react';
import moment from 'moment';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DateField from '../../components/fields/DateField';

class LocaleSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = { value: 'en' };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    moment.locale(e.target.value);
    this.setState({ value: e.target.value });
  }

  render() {
    return (
      <select value={this.state.value} onChange={this.handleChange}>
        <option>en</option>
        <option>ja</option>
      </select>
    );
  }
}

class DateFieldContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '2017-03-01',
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    action('onChange')(value);
    this.setState({
      value,
    });
  }

  render() {
    return <DateField onChange={this.onChange} value={this.state.value} />;
  }
}

storiesOf('commons/fields', module)
  .add(
    'DateField - 初期値あり',
    () => {
      const defaultValue = '2017-03-01';
      return (
        <div>
          <DateField value={defaultValue} onChange={action('onChange')} />
          <LocaleSelect />
        </div>
      );
    },
    { info: { propTables: [DateField], inline: true, source: true } }
  )
  .add(
    'DateField - 初期値なし',
    () => {
      return (
        <div>
          <DateField onChange={action('onChange')} />
          <LocaleSelect />
        </div>
      );
    },
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'DateField - 選択可能な下限の指定あり （日付：YYYY-MM-DD形式）',
    () => {
      return (
        <div>
          <DateField minDate="2017-09-15" onChange={action('onChange')} />
        </div>
      );
    },
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'DateField - 選択可能な下限の指定あり （日数）',
    () => {
      return (
        <div>
          <DateField minDays={-1} onChange={action('onChange')} />
        </div>
      );
    },
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'DateField - 選択可能な上限の指定あり （日付：YYYY-MM-DD形式）',
    () => {
      return (
        <div>
          <DateField maxDate="2017-09-15" onChange={action('onChange')} />
        </div>
      );
    },
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'DateField - 選択可能な上限の指定あり （日数）',
    () => {
      return (
        <div>
          <DateField maxDays={7} onChange={action('onChange')} />
        </div>
      );
    },
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'DateField - disabled',
    () => {
      return (
        <div>
          <DateField disabled onChange={action('onChange')} />
        </div>
      );
    },
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'DateField - with Container',
    () => {
      return <DateFieldContainer />;
    },
    {
      info: {
        text: `
    Containerでvalueを管理した際のサンプル
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  );
