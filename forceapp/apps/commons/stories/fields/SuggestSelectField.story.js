import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import SuggestSelectField from '../../components/fields/SuggestSelectField';

const languages = [
  {
    name: 'Clang',
    year: 1972,
  },
  {
    name: 'Elm',
    year: 2012,
  },
  {
    name: 'JavaScript',
    year: 2013,
  },
  {
    name: 'Java',
    year: 2014,
  },

  {
    name: 'Haskell',
    year: 2015,
  },
  {
    name: 'Ruby',
    year: 2016,
  },
];

const getSuggestionValue = (suggestion) => {
  return suggestion.name;
};

class SuggestContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      suggestions: [],
    };

    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(
      this
    );
  }

  onSuggestionsFetchRequested(value) {
    setTimeout(() => {
      const inputValue = value.trim().toLowerCase();
      const inputLength = inputValue.length;

      const suggestions =
        inputLength === 0
          ? []
          : languages.filter(
              (lang) =>
                lang.name.toLowerCase().slice(0, inputLength) === inputValue
            );

      this.setState({ suggestions });
    }, 500);
  }

  render() {
    return (
      <SuggestSelectField
        getSuggestionValue={getSuggestionValue}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        suggestions={this.state.suggestions}
        onSelected={action()}
        inputProps={{
          placeholder: 'Hit j',
        }}
      />
    );
  }
}

storiesOf('commons/fields', module)
  .addDecorator((story) => <div style={{ padding: 10 }}>{story()}</div>)
  .add(
    'SuggestSelectField',
    `
    フィールド内でエンターキー押下時に検索がかかる。その他のタイミングで検索させたい場合、  
    refのインスタンスからonSuggestionsFetchRequestedを直接叩かActionを直接投げる  
    サジェスト内容はキーボード操作可能。  
    カスタマイズ方法に関してはソースコメントを参照
    `,
    () => <SuggestContainer />,
    { info: { propTables: [SuggestSelectField], inline: true, source: true } }
  );
