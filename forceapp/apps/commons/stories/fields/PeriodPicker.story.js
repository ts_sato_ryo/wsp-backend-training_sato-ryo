import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import PeriodPicker from '../../components/fields/PeriodPicker';

const selectOptions = [
  { text: 'item1', value: 'item1' },
  { text: 'item2', value: 'item2' },
  { text: 'item3', value: 'item3' },
];

storiesOf('commons/fields', module).add(
  'PeriodPicker',
  () => (
    <PeriodPicker
      selectOptions={selectOptions}
      currentButtonLabel="今日"
      selectValue="item2"
      onClickCurrentButton={action('click current')}
      onClickNextButton={action('click next')}
      onClickPrevButton={action('click previous')}
      onChangeSelect={action('change select')}
    />
  ),
  { info: { propTables: [PeriodPicker], inline: true, source: true } }
);
