import React from 'react';
import { storiesOf } from '@storybook/react';

import CommentFieldIconOutside from '../../components/fields/CommentFieldIconOutside';

// ダミーアイコン
import ImgSamplePhoto001 from '../../images/Sample_photo001.png';

const style = {
  position: 'relatve',
  paddingLeft: 100,
};

storiesOf('commons/fields', module).add(
  'CommentFieldIconOutside',
  () => (
    <div style={style}>
      <CommentFieldIconOutside icon={ImgSamplePhoto001} />
    </div>
  ),
  {
    info: {
      text: `
    アイコンは基点からマイナスマージンでポジショニングされて表示される
    `,
      propTables: [CommentFieldIconOutside],
      inline: true,
      source: true,
    },
  }
);
