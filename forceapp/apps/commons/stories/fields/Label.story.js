import React from 'react';
import { storiesOf } from '@storybook/react';

import TextField from '../../components/fields/TextField';
import Label from '../../components/fields/Label';

import './Label.story.scss';

storiesOf('commons/fields', module)
  .add(
    'Label - with TextField',
    () => (
      <Label text="入力項目">
        <TextField id="some-text-filed" name="some-text-filed" />
      </Label>
    ),
    {
      info: {
        text: `
    サイズ, ラベル:テキストフィールド 3:9のレイアウトです。  
    LabelとTextFieldの紐付けは自動的に行われます。
    `,
        propTables: [Label],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Label - with input element',
    () => (
      <Label text="入力項目">
        <input id="some-input" type="text" className="slds-input" />
      </Label>
    ),
    {
      info: {
        text: `
    id属性をもつプリミティブなelementにも対応しています。
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Label - with long text',
    () => (
      <Label text="Lorem ipsum dolor sit amet, vix an viderer scripserit.">
        <TextField id="some-text-filed" name="some-text-filed" />
      </Label>
    ),
    {
      info: {
        text: `
    長いLabel.textは省略されますが、title属性にも同じStringが設定されるため、マウスオーバーで表示はされます。
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Label - with css class',
    () => (
      <Label text="入力項目" className="story-ts-label-background-blue">
        <TextField id="some-text-filed" name="some-text-filed" />
      </Label>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Label - change layout ratio',
    () => (
      <Label text="入力項目" labelCols={8} childCols={4}>
        <TextField id="some-text-filed" name="some-text-filed" />
      </Label>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Label - has required input',
    () => (
      <Label text="入力項目">
        <TextField id="some-text-filed" name="some-text-filed" required />
      </Label>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'Label - multi line layout',
    () => (
      <div>
        <Label text="カテゴリ">
          <TextField id="some-text-filed" name="some-text-filed" required />
        </Label>
        <Label text="分類">
          <TextField id="some-text-filed2" name="some-text-filed" required />
        </Label>
        <Label text="備考">
          <TextField id="some-text-filed3" name="some-text-filed" />
        </Label>
      </div>
    ),
    { info: { propTables: false, inline: true, source: true } }
  );
