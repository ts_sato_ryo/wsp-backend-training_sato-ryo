import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import TimeDurationField from '../../components/fields/TimeDurationField';

storiesOf('commons/fields', module).add(
  'TimeDurationField',
  () => <TimeDurationField onBlur={action('onBlur')} value={120} />,
  { info: { propTables: [TimeDurationField], inline: true, source: true } }
);
