import React from 'react';
import { storiesOf } from '@storybook/react';

import TimeField from '../../components/fields/TimeField';

storiesOf('commons/fields', module).add(
  'TimeField',
  () => <TimeField onBlur={() => {}} value="" />,
  {
    info: {
      text: `
    時刻入力フィールド

    テキスト入力と15分刻みのプルダウン入力に対応している

    blurされるたびにmoment.jsによる変換処理を走らせているため、ある程度の記載揺れは修正される。  
    全角と半角の自動変換にも対応している
    `,
      propTables: [TimeField],
      inline: true,
      source: true,
    },
  }
);
