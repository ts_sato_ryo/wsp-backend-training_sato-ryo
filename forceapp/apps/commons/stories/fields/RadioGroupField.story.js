import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import RadioGroupField, {
  LAYOUT_TYPE,
} from '../../components/fields/RadioGroupField';

const dummyOptions = [
  { text: 'sample1', value: 'sample1-value' },
  { text: 'sample2', value: 'sample2-value' },
];

storiesOf('commons/fields', module)
  .add(
    'radioGroupField',
    () => (
      <RadioGroupField
        options={dummyOptions}
        value="sample2-value"
        onChange={action()}
        name="name"
      />
    ),
    { info: { propTables: [RadioGroupField], inline: true, source: true } }
  )
  .add(
    'radioGroupField - vertical layout',
    () => (
      <RadioGroupField
        options={dummyOptions}
        value="sample2-value"
        onChange={action()}
        name="name"
        layout={LAYOUT_TYPE.vertical}
      />
    ),
    { info: { propTables: [RadioGroupField], inline: true, source: true } }
  );
