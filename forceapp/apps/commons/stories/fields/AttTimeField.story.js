import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import AttTimeField from '../../components/fields/AttTimeField';

storiesOf('commons/fields', module)
  .add(
    'AttTimeField',
    () => <AttTimeField onBlur={action('blurred')} value="" />,
    {
      info: {
        text: `
      勤怠時刻入力フィールド
      TimeField と以下の点で異なる

        * 選択リストは表示されず、テキスト入力のみ可能
        * 24:00-48:00の時刻も入力可能の時刻も入力可能
      `,
        propTables: [AttTimeField],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'AttTimeField (disabled)',
    () => <AttTimeField onBlur={action('blurred')} value="" disabled />,
    { info: { propTables: [AttTimeField], inline: true, source: true } }
  );
