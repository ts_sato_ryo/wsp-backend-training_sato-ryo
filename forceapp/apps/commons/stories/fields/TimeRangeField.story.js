import React from 'react';
import { storiesOf } from '@storybook/react';

import TimeRangeField from '../../components/fields/TimeRangeField';

storiesOf('commons/fields', module).add(
  'TimeRangeField',
  () => <TimeRangeField />,
  { info: { propTables: [TimeRangeField], inline: true, source: true } }
);
