import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import TextField from '../../components/fields/TextField';

storiesOf('commons/fields', module)
  .add(
    'TextField',
    () => <TextField id="some-text-filed" name="some-text-filed" />,
    { info: { propTables: [TextField], inline: true, source: true } }
  )
  .add(
    'TextField - type',
    () => <TextField type="password" value="sercret" />,
    {
      info: {
        text: `
      typeはデフォルトでtextが設定されます。
      sampleはpasswordです。
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  )
  .add('TextField - disabled', () => <TextField disabled />, {
    propTables: false,
    inline: true,
    source: true,
  })
  .add(
    'TextField - readOnly',
    () => (
      <TextField
        readOnly
        value="read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, read only, "
      />
    ),
    {
      info: {
        text: `
    readOnly, Oが大文字なので注意
    単純なinline-blockを表示します。
    paddingなどはreadOnlyでない時に合わせて統一しています。
    title属性にvalueを設定しているので、文字が長くて省略された場合も、
    マウスホバーによってツールチップを表示することで内容を確認できます。
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'TextField - placeholder',
    () => <TextField value="text" placeholder="サンプルプレースホルダー" />,
    {
      info: {
        text: `
      placeholderが設定されます。
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'TextField - onEvent',
    () => (
      <TextField
        onFocus={action('Focus')}
        onBlur={action('Blur')}
        onKeyDown={action('KeyDown')}
        onChange={action('change')}
      />
    ),
    {
      info: {
        text: `
    onFocus: フォーカスが当たった時
    onBlur: フォーカスが外れた時
    onKeyDown: フォーカス中にキーボードが押された時
    onChange: 値が変更されたとき
    `,
        propTables: false,
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'TextField - with css class',
    () => <TextField className="slds-theme--success" value="default value" />,
    { info: { propTables: false, inline: true, source: true } }
  );
