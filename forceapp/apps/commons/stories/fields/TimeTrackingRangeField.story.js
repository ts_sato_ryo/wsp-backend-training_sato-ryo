import React from 'react';
import { storiesOf } from '@storybook/react';

import TimeTrackingRangeField from '../../components/fields/TimeTrackingRangeField';

class Container extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ratio: 40,
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    this.setState({
      ratio: value,
    });
  }

  render() {
    return (
      <div>
        <TimeTrackingRangeField
          barColor="rgb(0, 131, 182)"
          linkBarColor="rgb(0, 101, 140)"
          ratio={this.state.ratio}
          maxRatio={600}
          linkRatio={40}
          onChange={this.onChange}
        />

        <div>{this.state.ratio}</div>
      </div>
    );
  }
}

storiesOf('commons/fields', module)
  .addDecorator((story) => <div style={{ paddingLeft: 20 }}>{story()}</div>)
  .add('TimeTrackingRangeField', () => <Container />, {
    info: { propTables: [TimeTrackingRangeField], inline: true, source: true },
  });
