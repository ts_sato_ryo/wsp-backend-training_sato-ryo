import React from 'react';
import { storiesOf } from '@storybook/react';

import CommentField from '../../components/fields/CommentField';

// ダミーアイコン
import ImgSamplePhoto001 from '../../images/Sample_photo001.png';

storiesOf('commons/fields', module).add(
  'CommentField',
  () => <CommentField icon={ImgSamplePhoto001} />,
  { info: { propTables: [CommentField], inline: true, source: true } }
);
