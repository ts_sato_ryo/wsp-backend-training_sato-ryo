import React from 'react';
import { storiesOf } from '@storybook/react';

import HorizontalLayout from '../../../components/fields/layouts/HorizontalLayout';

storiesOf('commons/fields', module)
  .add(
    'HorizontalLayout',
    () => (
      <HorizontalLayout>
        <HorizontalLayout.Label>項目</HorizontalLayout.Label>
        <HorizontalLayout.Body>テキスト</HorizontalLayout.Body>
      </HorizontalLayout>
    ),
    {
      info: {
        text: `
    Labelコンポーネントと基本は同じ
    `,
        propTables: [HorizontalLayout],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'HorizontalLayout - layout ratio',
    () => (
      <HorizontalLayout>
        <HorizontalLayout.Label cols={8}>項目</HorizontalLayout.Label>
        <HorizontalLayout.Body cols={4}>
          <input type="text" className="slds-input" />
        </HorizontalLayout.Body>
      </HorizontalLayout>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'HorizontalLayout - required Label',
    () => (
      <HorizontalLayout>
        <HorizontalLayout.Label required>必須項目</HorizontalLayout.Label>
        <HorizontalLayout.Body>テキスト</HorizontalLayout.Body>
      </HorizontalLayout>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'HorizontalLayout - with css class',
    () => (
      <HorizontalLayout className="slds-border--bottom">
        <HorizontalLayout.Label className="slds-theme--shade">
          必須項目
        </HorizontalLayout.Label>
        <HorizontalLayout.Body className="slds-text-align--right">
          テキスト
        </HorizontalLayout.Body>
      </HorizontalLayout>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'HorizontalLayout - with long text',
    () => (
      <HorizontalLayout>
        <HorizontalLayout.Label>
          項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目項目
        </HorizontalLayout.Label>
        <HorizontalLayout.Body>テキスト</HorizontalLayout.Body>
      </HorizontalLayout>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'HorizontalLayout - multi line layout',
    () => (
      <div>
        <HorizontalLayout>
          <HorizontalLayout.Label required>項目1</HorizontalLayout.Label>
          <HorizontalLayout.Body>テキスト</HorizontalLayout.Body>
        </HorizontalLayout>

        <HorizontalLayout>
          <HorizontalLayout.Label required>項目2</HorizontalLayout.Label>
          <HorizontalLayout.Body>テキスト</HorizontalLayout.Body>
        </HorizontalLayout>

        <HorizontalLayout>
          <HorizontalLayout.Label>項目3</HorizontalLayout.Label>
          <HorizontalLayout.Body>テキスト</HorizontalLayout.Body>
        </HorizontalLayout>
      </div>
    ),
    { info: { propTables: false, inline: true, source: true } }
  )
  .add(
    'HorizontalLayout - with help message',
    () => (
      <HorizontalLayout>
        <HorizontalLayout.Label helpMsg="this is the help text.">
          項目
        </HorizontalLayout.Label>
        <HorizontalLayout.Body>テキスト</HorizontalLayout.Body>
      </HorizontalLayout>
    ),
    { info: { propTables: false, inline: true, source: true } }
  );
