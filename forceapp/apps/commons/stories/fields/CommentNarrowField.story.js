import React from 'react';
import { storiesOf } from '@storybook/react';

import CommentNarrowField from '../../components/fields/CommentNarrowField';

// ダミーアイコン
import ImgSamplePhoto001 from '../../images/Sample_photo001.png';

storiesOf('commons/fields', module).add(
  'CommentNarrowField',
  () => <CommentNarrowField icon={ImgSamplePhoto001} />,
  { info: { propTables: [CommentNarrowField], inline: true, source: true } }
);
