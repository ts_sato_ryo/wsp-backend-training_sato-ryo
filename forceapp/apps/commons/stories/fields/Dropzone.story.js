// @flow

import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';

import Component from '../../components/fields/Dropzone';

const withState = (WrappedComponent: React.ComponentType<any>) =>
  class extends React.Component<any, any> {
    state = {
      files: [],
    };

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          onDropAccepted={(files, event) => {
            this.props.onDropAccepted(files, event);
            this.setState({ files });
          }}
          onClickDelete={() => {
            this.props.onClickDelete();
            this.setState({ files: [] });
          }}
        />
      );
    }
  };
const Dropzone = withState(Component);
Dropzone.displayName = 'Dropzone';

storiesOf('commons/fields', module).add(
  'Dropzone',
  withInfo({
    inline: true,
    propTables: [Dropzone],
    text: `
        # Description

        Dropzone for file upload.
      `,
  })(() => (
    <Dropzone
      files={[]}
      onDropAccepted={action('onDropAccepted')}
      onDropRejected={action('onDropRejected')}
      onClickDelete={action('onClickDelete')}
    />
  ))
);
