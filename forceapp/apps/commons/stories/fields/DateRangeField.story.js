import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DateRangeField from '../../components/fields/DateRangeField';

storiesOf('commons/fields', module).add(
  'DateRangeField',
  () => (
    <DateRangeField
      startDateFieldProps={{ onChange: action('on StartDateField change') }}
      endDateFieldProps={{ onChange: action('on EndDateField change') }}
    />
  ),
  { info: { propTables: [DateRangeField], inline: true, source: true } }
);
