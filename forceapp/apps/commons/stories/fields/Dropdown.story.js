// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Dropdown from '../../components/fields/Dropdown';

const dummyData = [
  { type: 'item', label: '選択肢1', value: 1 },
  { type: 'item', label: '選択肢2', value: 2 },
  { type: 'item', label: '選択肢3', value: 3 },
  { type: 'divider' },
  {
    type: 'item',
    label:
      'とーっても長い選択肢です。幅が足りない場合でも、折り返して表示します。ながーいマスタデータ等が作られた時でも、選択肢が途中で隠れなくて便利ですね!',
    value: 5,
  },
  { type: 'item', label: '選択肢4', value: 4 },
];

storiesOf('commons/fields', module).add(
  'Dropdown',
  () => <Dropdown label="ドロップダウン" options={dummyData} />,
  { info: { propTables: [Dropdown], inline: true, source: true } }
);

storiesOf('commons/fields', module).add(
  'Dropdown without label',
  () => <Dropdown options={dummyData} />,
  { info: { propTables: [Dropdown], inline: true, source: true } }
);

storiesOf('commons/fields', module).add(
  'Dropdown onSelect',
  () => (
    <Dropdown
      label="ドロップダウン"
      options={dummyData}
      onSelect={action('onSelect')}
    />
  ),
  { info: { propTables: [Dropdown], inline: true, source: true } }
);

storiesOf('commons/fields', module).add(
  'Dropdown default value',
  () => (
    <Dropdown
      label="ドロップダウン"
      options={dummyData}
      value={3}
      onSelect={action('onSelect')}
    />
  ),
  { info: { propTables: [Dropdown], inline: true, source: true } }
);

storiesOf('commons/fields', module).add(
  'Dropdown disabled',
  () => (
    <Dropdown
      label="ドロップダウン"
      options={dummyData}
      value={3}
      disabled
      onSelect={action('onSelect')}
    />
  ),
  { info: { propTables: [Dropdown], inline: true, source: true } }
);

const dummyData2 = [
  {
    className: 'custom-li-class',
    divider: 'bottom',
    label: 'A Header',
    type: 'header',
  },
  {
    href: 'http://sfdc.co/',
    id: 'custom-li-id',
    label: 'Has a value',
    leftIcon: {
      name: 'settings',
      category: 'utility',
    },
    rightIcon: {
      name: 'settings',
      category: 'utility',
    },
    type: 'item',
    value: 'B0',
  },
  {
    type: 'divider',
  },
];

storiesOf('commons/fields', module).add(
  'Dropdown rich menu',
  () => (
    <Dropdown
      label="ドロップダウン"
      options={dummyData2}
      onSelect={action('onSelect')}
    />
  ),
  { info: { propTables: [Dropdown], inline: true, source: true } }
);
