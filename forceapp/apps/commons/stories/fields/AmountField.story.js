import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import AmountField from '../../components/fields/AmountField';

storiesOf('commons/fields', module).add(
  'AmountField',
  () => (
    <AmountField
      value={1000.345}
      fractionDigits={2}
      onBlur={action('onBlur')}
      onFocus={action('onFocus')}
    />
  ),
  { info: { propTables: [AmountField], inline: true, source: true } }
);
