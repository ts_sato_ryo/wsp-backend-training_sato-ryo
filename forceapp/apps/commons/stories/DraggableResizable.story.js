import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number, text, object } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';
import DraggableResizable from '../components/DraggableResizable';

storiesOf('commons', module)
  .addDecorator(withKnobs)
  .add(
    'DraggableResizable',
    withInfo(
      `
        # Description
        Drag and resize
        More detailed props refer to react-rnd

        # Propsについて
        bounds: boundary for drag area
        maxHeight: max height for resize
        minHeight: min height for resize
        default: default size and position
      `
    )(() => (
      <DraggableResizable
        bounds={text('bounds', 'window')}
        maxHeight={number('maxHeight', 700)}
        maxWidth={number('maxWidth', 1000)}
        minHeight={number('minHeight', 200)}
        minWidth={number('minWidth', 200)}
        default={object('defaultProperty', {
          width: 300,
          height: 300,
          x: 0,
          y: 0,
        })}
      >
        Drag and resize
      </DraggableResizable>
    ))
  );
