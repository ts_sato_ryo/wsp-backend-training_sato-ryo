import React from 'react';
import { storiesOf } from '@storybook/react';

import HorizontalBarGraph from '../../components/graphs/HorizontalBarGraph';

import './HorizontalBarGraph.story.scss';

storiesOf('commons/graphs', module).add(
  'HorizontalBarGraph',
  () => (
    <HorizontalBarGraph
      data={[
        {
          value: 30,
          color: 'red',
        },
        {
          value: 50,
          color: 'blue',
        },
      ]}
      className="ts-horizontal-bar-graph-story"
    />
  ),
  { info: { propTables: [HorizontalBarGraph], inline: true, source: true } }
);
