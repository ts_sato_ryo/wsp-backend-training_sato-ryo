import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import CurrencyFieldSet from '../../components/fieldsets/CurrencyFieldSet';

const CURRENCY_LIST = [
  { code: 'USD', digits: 2, id: 'dummyId1' },
  { code: 'SGD', digits: 2, id: 'dummyId2' },
];

const getCurrencyRateInfo = () => {
  return {
    currencyCode: 'SGD',
    currencyRate: '120.20',
    useInverseRate: false,
  };
};

// TODO: コンポーネントの外側の <div> はスタイルの分離ができたら削除
storiesOf('commons/fieldsets', module)
  .add('CurrencyFieldSet: baseCurrency', () => {
    return (
      <div className="ts-expenses-requests slds">
        <div className="ts-expenses-requests__contents">
          <div className="ts-expenses-requests__contents__wrap">
            <div className="ts-expenses-requests__contents__form">
              <div className="ts-expenses-requests__contents__amount">
                <CurrencyFieldSet
                  caller="expenses"
                  baseCurrencyCode="JPY"
                  baseCurrencyFractionDigits={0}
                  totalAmount={1000}
                  onChangeTotalAmount={action('onChangeTotalAmount')}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  })
  .add('CurrencyFieldSet: foreignCurrency', () => {
    return (
      <div className="ts-expenses-requests slds">
        <div className="ts-expenses-requests__contents">
          <div className="ts-expenses-requests__contents__wrap">
            <div className="ts-expenses-requests__contents__form">
              <div className="ts-expenses-requests__contents__amount">
                <CurrencyFieldSet
                  useForeignCurrency
                  caller="expenses"
                  baseCurrencyCode="JPY"
                  baseCurrencyFractionDigits={2}
                  currencyList={CURRENCY_LIST}
                  currencyId={CURRENCY_LIST[0].id}
                  localAmount={1000}
                  totalAmount={3000}
                  getCurrencyRateInfo={getCurrencyRateInfo}
                  onChangeCurrencyRateEditMode={action(
                    'onChangeCurrencyRateEditMode'
                  )}
                  onChangeFormParam={action('onChangeFormParam')}
                  onChangeLocalAmount={action('onChangeLocalAmount')}
                  isEditingCurrencyRate
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
    // TODO: rate が編集不可の場合などのストーリーを追加
  });
