import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { withInfo } from '@storybook/addon-info';
import Pagination from '../components/Pagination';

storiesOf('commons', module)
  .addDecorator(withKnobs)
  .add(
    'Pagenation',
    withInfo(
      `
        # Description

        ページャーコンポーネント
        自動的にページャーを作成します

        # Propsについて

        currentPage:現在のページ 
        pageSize: １ページの件数 
        displayNum: 表示するページ数
        totalNum: 件数
      `
    )(() => (
      <Pagination
        currentPage={number('current', 18)}
        pageSize={number('pageSize', 30)}
        displayNum={number('displayNum', 5)}
        totalNum={number('totalNum', 524)}
        onClick={action('pageClick')}
      />
    ))
  );
