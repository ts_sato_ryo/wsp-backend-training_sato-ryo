import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import transportationType from '../constants/transportationType';
import Route from '../components/Route';

const routeList = [
  {
    place: '東京',
    transportationType: transportationType.BULLET_TRAIN,
    transportationName: 'のぞみ49号(自由席)[博多行き]',
    price: 10000,
  },
  {
    place: '新大阪',
    transportationType: transportationType.TRAIN,
    transportationName: '東海道本線(普通)[西有明行き]',
    price: 3600,
  },
  {
    place: '梅田',
    transportationType: transportationType.WALK,
    transportationName: '徒歩',
    price: 0,
  },
  {
    place: '大阪',
  },
];

const attentionIcon = {
  yasui: true,
  hayai: true,
  raku: true,
};

const routeInfo = {
  from: '東京',
  to: '大阪',
  roundTrip: false,
  attentionIcon,
  routeList,
};

const routeInfo2 = {
  from: '東京',
  to: '大阪',
  roundTrip: true,
  attentionIcon,
  routeList,
};

storiesOf('commons/Route', module)
  .add(
    'Route - 片道',
    () => (
      <Route
        routeInfo={routeInfo}
        onClickRouteFinderButton={action('click onClickRouteFinderButton')}
        onClickToggleWayButton={action('click onClickToggleWayButton')}
      />
    ),
    { info: { propTables: [Route], inline: true, source: true } }
  )
  .add(
    'Route - 往復',
    () => (
      <Route
        routeInfo={routeInfo2}
        onClickRouteFinderButton={action('click onClickRouteFinderButton')}
        onClickToggleWayButton={action('click onClickToggleWayButton')}
      />
    ),
    { info: { propTables: false, inline: true, source: true } }
  );
