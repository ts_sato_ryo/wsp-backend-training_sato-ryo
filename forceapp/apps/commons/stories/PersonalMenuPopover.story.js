import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { action } from '@storybook/addon-actions';

import PersonalMenuPopover from '../components/PersonalMenuPopover';

storiesOf('commons', module).add(
  'PersonalMenuPopover',
  withInfo({
    info: { propTables: [PersonalMenuPopover], inline: true, source: true },
  })(() => (
    <div
      style={{
        height: 240,
        position: 'relative',
        width: 480,
      }}
    >
      <PersonalMenuPopover
        employeeName="田中 太郎"
        departmentName="テスト部署"
        showProxyEmployeeSelectButton
        showChangeApproverButton
        onClickCloseButton={action('clickCloseButton')}
        onClickOpenProxyEmployeeSelectButton={action(
          'clickOpenProxyEmployeeSelectButton'
        )}
        onClickOpenLeaveWindowButton={action('clickOpenLeaveWindowButton')}
        onClickOpenChangeApproverButton={action(
          'clickOpenChangeApproverButton'
        )}
      />
    </div>
  ))
);
