import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import BlowingPopup from '../components/BlowingPopup';

storiesOf('commons', module).add(
  'BlowingPopup',
  () => (
    <div style={{ height: 250 }}>
      <BlowingPopup
        showPopup
        popupHeaderTitle="サンプル"
        popupShowItemList={[
          {
            type: 'type1',
            label: 'item1',
          },
          {
            type: 'type2',
            label: 'item2',
          },
          {
            type: 'type3',
            label: 'item3',
          },
          {
            type: 'type4',
            label: 'item4',
          },
        ]}
        addRecord={action('add record')}
        closeBlowingPopup={action('close')}
      />
    </div>
  ),
  { info: { propTables: [BlowingPopup], inline: true, source: true } }
);
