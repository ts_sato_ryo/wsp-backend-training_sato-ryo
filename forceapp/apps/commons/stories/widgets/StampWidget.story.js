import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import StampWidget from '../../components/widgets/StampWidget';

storiesOf('commons/widgets', module)
  .add(
    'StampWidget',
    () => {
      return (
        <StampWidget
          onClickModeButton={action('出退勤の切り替え')}
          onClickStampButton={action('打刻')}
          onChangeMessage={action('メッセージ更新')}
        />
      );
    },
    { info: { propTables: [StampWidget], inline: true, source: true } }
  )
  .add(
    'StampWidget - 出退勤両方不可',
    () => {
      return (
        <StampWidget
          onClickModeButton={action('出退勤の切り替え')}
          onClickStampButton={action('打刻')}
          onChangeMessage={action('メッセージ更新')}
          isClockInDisabled
          isClockOutDisabled
        />
      );
    },
    { info: { propTables: [StampWidget], inline: true, source: true } }
  );
