import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Tab from '../components/Tab';

import ImgIconDummy from '../images/iconHeaderExp.png';

storiesOf('commons', module)
  .add(
    'Tab',
    () => (
      <div role="tablist">
        <Tab
          icon={ImgIconDummy}
          label="Label 1"
          selected
          onSelect={action('onSelect')}
        />
        <Tab
          icon={ImgIconDummy}
          label="Label 2"
          selected={false}
          onSelect={action('onSelect')}
        />
      </div>
    ),
    {
      info: {
        text: `
  選択状態を指定するにはselected(bool)を渡す

  `,
        propTables: [Tab],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'Tab - 通知あり',
    () => (
      <div role="tablist">
        <Tab
          icon={ImgIconDummy}
          label="Label 1"
          selected
          onSelect={action('onSelect')}
          notificationCount={3}
        />
        <Tab
          icon={ImgIconDummy}
          label="Label 2"
          selected={false}
          onSelect={action('onSelect')}
          notificationCount={100}
        />
      </div>
    ),
    {
      info: {
        text: `
  notificationCountが1以上のときに、通知バッチを表示する  
  100件以上の場合は「99+」に省略される
  `,
        propTables: [Tab],
        inline: true,
        source: true,
      },
    }
  );
