// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';

import UnexpectedErrorPage from '../../components/errors/UnexpectedErrorPage';
import fatalError from '../mock-data/dummyFatalError';
import apexError from '../mock-data/dummyApexError';

storiesOf('commons/errors', module)
  .add(
    'UnexpectedErrorPage with FatalError',
    () => <UnexpectedErrorPage error={fatalError} />,
    {
      info: {
        text: 'FatalError版',
        propTables: [UnexpectedErrorPage],
        inline: false,
        source: true,
      },
    }
  )
  .add(
    'UnexpectedErrorPage with ApexError',
    () => <UnexpectedErrorPage error={apexError} />,
    {
      info: {
        text: 'ApexError版',
        propTables: [UnexpectedErrorPage],
        inline: false,
        source: true,
      },
    }
  );
