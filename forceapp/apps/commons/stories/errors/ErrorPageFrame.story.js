// @flow

import React from 'react';
import { storiesOf } from '@storybook/react';

import ErrorPageFrame from '../../components/errors/ErrorPageFrame';

import iconErrorLarge from '../../images/iconErrorLarge.png';

storiesOf('commons/errors', module).add(
  'ErrorPageFrame',
  () => (
    <ErrorPageFrame
      title="SOMETHING WENT WRONG"
      icon={iconErrorLarge}
      solution="ブラウザをリロードして再度お試しください。"
    />
  ),
  {
    info: {
      text: 'FatalError版',
      propTables: [ErrorPageFrame],
      inline: false,
      source: true,
    },
  }
);
