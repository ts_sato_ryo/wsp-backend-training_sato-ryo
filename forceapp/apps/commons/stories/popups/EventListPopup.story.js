import React from 'react';
import moment from 'moment';

import { action } from '@storybook/addon-actions';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import EventListPopup from '../../components/popups/EventListPopup';

const events = [
  {
    id: 'a0Q7F000008HfBQUA0',
    title: 'All Day Event',
    start: moment([2018, 6, 4]),
    end: moment([2018, 6, 4]).endOf('day'),
    isAllDay: true,
    isOrganizer: false,
    location: '',
    remarks: '',
    isOuting: false,
    createdServiceBy: 'teamspirit',
    job: {
      id: '',
      code: '',
      name: '',
    },
    workCategoryId: '',
    workCategoryName: '',
    layout: {
      containsAllDay: true,
      startMinutesOfDay: 0,
      endMinutesOfDay: 0,
    },
  },
  {
    id: 'a0Q7F000008HfBQUA0',
    title: 'All Day Event from Office365',
    start: moment([2018, 6, 4]),
    end: moment([2018, 6, 4]).endOf('day'),
    isAllDay: true,
    isOrganizer: false,
    location: 'Seminar Room',
    remarks: '',
    isOuting: false,
    createdServiceBy: 'office365',
    job: {
      id: 'ABC',
      code: '',
      name: '',
    },
    workCategoryId: '',
    workCategoryName: '',
    layout: {
      containsAllDay: true,
      startMinutesOfDay: 0,
      endMinutesOfDay: 0,
    },
  },
  {
    id: 'a0Q7F000008HfBQUA0',
    title: 'All Day Event from Office365',
    start: moment([2018, 6, 4]),
    end: moment([2018, 6, 4]).endOf('day'),
    isAllDay: true,
    isOrganizer: false,
    location: 'Seminar Room',
    remarks: '',
    isOuting: true,
    createdServiceBy: 'office365',
    job: {
      id: 'ABC',
      code: '',
      name: '',
    },
    workCategoryId: '',
    workCategoryName: '',
    layout: {
      containsAllDay: true,
      startMinutesOfDay: 0,
      endMinutesOfDay: 0,
    },
  },
];

storiesOf('commons/popups', module)
  .addDecorator((story) => (
    <div
      style={{
        position: 'relative',
        width: '100%',
        height: '500px',
      }}
    >
      {story()}
    </div>
  ))
  .addDecorator(withKnobs)
  .add(
    'EventListPopup',
    withInfo({
      propTables: [EventListPopup],
      inline: true,
      source: true,
    })(() => (
      <EventListPopup
        targetDate={moment([2018, 6, 4])}
        events={events}
        isOpened={boolean('isOpened', true)}
        onClickClose={action('onClickClose')}
        onClickEvent={action('onClickEvent')}
      />
    ))
  );
