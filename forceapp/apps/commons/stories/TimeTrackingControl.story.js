import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import '../components/dialogs/Dialog.scss';
import TimeTrackingControlDialog from '../components/dialogs/TimeTrackingControl';

storiesOf('commons/dialogs', module).add(
  'TimeTrackingControlDialog',
  () => (
    <div className="dialog">
      <TimeTrackingControlDialog hideDialog={action('close Dialog')} />
    </div>
  ),
  { info: { propTables: false, inline: false, source: true } }
);
