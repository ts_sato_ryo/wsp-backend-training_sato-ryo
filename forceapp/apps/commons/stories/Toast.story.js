// @flow

import React from 'react';

/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/react';

/* eslint-disable import/no-extraneous-dependencies */

import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import Toast from '../components/Toast';

storiesOf('commons/Toast', module)
  .addDecorator(withKnobs)
  .add(
    'Success',
    () => {
      return (
        <Toast
          message={text('message', '個人設定を保存しました。')}
          isShow={boolean('isShow', true)}
          onClick={action('ボタンクリック')}
          onExit={action('onExit')}
        />
      );
    },
    {
      info: `
        # Description

        申請完了や、保存完了時に表示するトーストです
      `,
    }
  )
  .add('Warning', () => {
    return (
      <Toast
        variant="warning"
        message={text('message', '個人設定を保存しました。')}
        isShow={boolean('isShow', true)}
        onClick={action('ボタンクリック')}
        onExit={action('onExit')}
      />
    );
  })
  .add('Error', () => {
    return (
      <Toast
        variant="error"
        message={text('message', '個人設定を保存しました。')}
        isShow={boolean('isShow', true)}
        onClick={action('ボタンクリック')}
        onExit={action('onExit')}
      />
    );
  })
  .add('Long message', () => {
    return (
      <Toast
        variant="success"
        message={text(
          'message',
          'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,'
        )}
        isShow={boolean('isShow', true)}
        onClick={action('ボタンクリック')}
        onExit={action('onExit')}
      />
    );
  });
