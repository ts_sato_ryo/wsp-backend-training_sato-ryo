import React from 'react';
import { storiesOf } from '@storybook/react';

import ErrorPage from '../components/ErrorPage';
import dummyError from './mock-data/dummyError';

storiesOf('commons', module).add(
  'ErrorPage',
  () => <ErrorPage error={dummyError} />,
  {
    info: {
      text: '回復方法あり',
      propTables: [ErrorPage],
      inline: false,
      source: true,
    },
  }
);
