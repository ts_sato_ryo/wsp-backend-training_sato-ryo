/* @flow */
import React from 'react';

import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import ProxyIndicator from '../components/ProxyIndicator';
import imgPhotoBlank from '../images/photo_Blank.png';

storiesOf('commons', module).add(
  'ProxyIndicator',
  withInfo({
    propTables: [ProxyIndicator],
    inline: true,
    source: true,
  })(() => (
    <ProxyIndicator
      employeePhotoUrl={imgPhotoBlank}
      employeeName="田中 太郎"
      onClickExitButton={action('exit')}
      standalone={false}
    />
  ))
);
