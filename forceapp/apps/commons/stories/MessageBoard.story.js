import React from 'react';
import { storiesOf } from '@storybook/react';

import MessageBoard from '../components/MessageBoard';

import imgIconDoneCircle from '../images/iconDoneCircle.png';

const style = {
  width: 700,
  height: 500,
  backgroundColor: '#f5f5f5',
};

storiesOf('commons', module)
  .add(
    'MessageBoard',
    () => (
      <div style={style}>
        <MessageBoard
          message="This is a sample meesgae."
          iconSrc={imgIconDoneCircle}
        />
      </div>
    ),
    {
      info: {
        text: `
    MessageBoardを配置した親要素に対してレイアウトされる
    背景色は透明
    `,
        propTables: [MessageBoard],
        inline: true,
        source: true,
      },
    }
  )
  .add(
    'MessageBoard - with descirption',
    () => (
      <div style={style}>
        <MessageBoard
          message="This is a sample meesgae."
          iconSrc={imgIconDoneCircle}
          description="here is description area."
        />
      </div>
    ),
    {
      info: {
        text: `
    MessageBoardを配置した親要素に対してレイアウトされる
    背景色は透明
    `,
        propTables: [MessageBoard],
        inline: true,
        source: true,
      },
    }
  );
