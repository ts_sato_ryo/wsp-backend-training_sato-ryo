import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { action } from '@storybook/addon-actions';

import ExpandableSection from '../components/ExpandableSection';

storiesOf('commons', module).add(
  'ExpandableSection',
  withInfo({
    propTables: [ExpandableSection],
    inline: true,
    source: true,
  })(() => (
    <ExpandableSection
      expanded
      summary="Lorem-ipsum"
      onToggle={action('toggle')}
    >
      Pellentesque dapibus suscipit ligula. Donec posuere augue in quam. Etiam
      vel tortor sodales tellus ultricies commodo. Suspendisse potenti. Aenean
      in sem ac leo mollis blandit. Donec neque quam, dignissim in, mollis nec,
      sagittis eu, wisi. Phasellus lacus. Etiam laoreet quam sed arcu. Phasellus
      at dui in ligula mollis ultricies. Integer placerat tristique nisl.
      Praesent augue. Fusce commodo.
    </ExpandableSection>
  ))
);
