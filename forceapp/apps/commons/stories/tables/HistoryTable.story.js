import React from 'react';
import { storiesOf } from '@storybook/react';

import HistoryTable from '../../components/tables/HistoryTable';

import imgPhoto from '../../images/Sample_photo001.png';

const dummyHistoryList = [
  {
    id: 'id1',
    status: 'Pending',
    stepName: 'テスト1',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id2',
    status: 'Pending',
    stepName: 'テスト2',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id3',
    status: 'Pending',
    stepName: 'テスト3',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id4',
    status: 'Pending',
    stepName: 'テスト4',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id5',
    status: 'Pending',
    stepName: 'テスト5',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
  {
    id: 'id6',
    status: 'Pending',
    stepName: 'テスト6',
    approveTime: '2004-04-01T12:00Z',
    approverName: '承認者名',
    actorPhotoUrl: imgPhoto,
    actorName: '申請者名',
    comment: 'コメント コメント コメント コメント コメント コメント コメント',
  },
];

storiesOf('commons/tables', module).add(
  'HistoryTable',
  () => <HistoryTable historyList={dummyHistoryList} />,
  { info: { propTables: [HistoryTable], inline: true, source: true } }
);
