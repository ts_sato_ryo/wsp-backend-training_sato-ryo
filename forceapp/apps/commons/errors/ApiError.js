// @flow

import BaseWSPError from './BaseWSPError';
import msg from '../languages';

export default class ApiError extends BaseWSPError {
  /**
   * @param {Object} apiErr
   * @param {String} apiErr.errorCode
   * @param {String} apiErr.message
   * @param {String} apiErr.stackTrace
   * @param {Object} [options]
   * @param {Boolean} [options.isContinuable=true] 操作継続の可否：true -> ダイアログ／ false -> ページ全体でエラーを表示する
   */
  constructor(apiErr: *, options: *) {
    const msgKey = apiErr.errorCode;

    // FIXME: 規定のメッセージがない場合、API由来のメッセージをそのまま使っているが、あまりよろしくない。
    const msgStr = msg()[msgKey] || apiErr.message;

    const isFunctionCantUseError = apiErr.errorCode
      ? apiErr.errorCode.match(/_CANNOT_USE$/)
      : false;

    // TODO: 内容の組み立ては、後続のストーリー（GENIE-3629）で実装予定
    super('APIエラー', msgStr, null, options, isFunctionCantUseError);
  }
}
