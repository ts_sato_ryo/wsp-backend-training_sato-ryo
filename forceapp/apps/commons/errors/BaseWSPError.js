export default class BaseWSPError extends Error {
  /**
   * @param {String} type
   * @param {String} problem
   * @param {?String} [solution]
   * @param {Object} [options]
   * @param {Boolean} [options.isContinuable]
   * @param {Boolean} [isFunctionCantUseError]
   */
  constructor(type, problem, solution, options = {}, isFunctionCantUseError) {
    super(`${type} - ${problem}`);

    /** @type {String} エラーのタイプ：ダイアログのヘッダーに表示される */
    this.type = type;

    /** @type {String} （原因＋）起こった問題：e.g.「〜〜のため、〜〜できませんでした」 */
    this.problem = problem;

    /** @type {?String} 回復方法：e.g.「〜〜してください」 */
    this.solution = solution;

    /** @type {Boolean} 操作継続の可否 */
    this.isContinuable =
      typeof options.isContinuable !== 'undefined'
        ? options.isContinuable
        : true;

    this.isFunctionCantUseError = isFunctionCantUseError;
  }
}
