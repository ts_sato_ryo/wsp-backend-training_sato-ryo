// @flow

import * as React from 'react';

type ClickEventHandler = (event: SyntheticEvent<Element>) => void;

export type ClickableProps = $ReadOnly<{
  onClick?: ClickEventHandler,
}>;

export default <T: $Shape<ClickableProps>>(
  WrappedComponent: React.ComponentType<T>
) => {
  return class extends React.PureComponent<T> {
    handleClick: ClickEventHandler;

    constructor() {
      super();

      this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event: SyntheticEvent<Element>): void {
      event.preventDefault();
      event.stopPropagation();

      if (this.props.onClick) {
        this.props.onClick(event);
      }
    }

    render() {
      return <WrappedComponent {...this.props} onClick={this.handleClick} />;
    }
  };
};
