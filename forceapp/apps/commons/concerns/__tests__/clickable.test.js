// @flow
import * as React from 'react';
import { shallow } from 'enzyme';

import clickable from '../clickable';

const syntheticEvent = <A>(a: A): SyntheticEvent<A> => {
  return ({
    target: a,
    preventDefault: jest.fn(),
    stopPropagation: jest.fn(),
  }: any);
};

describe('clickable()', () => {
  test('onClick handler should be called', () => {
    const mock = jest.fn(() => <div />);
    const MockedComponent = clickable(mock);
    const handler = jest.fn(() => {});
    const wrapper = shallow(<MockedComponent onClick={handler} />);

    const mockSythenicEvent = syntheticEvent();
    wrapper.instance().handleClick(mockSythenicEvent);

    expect(handler).toHaveBeenCalled();
  });
});
