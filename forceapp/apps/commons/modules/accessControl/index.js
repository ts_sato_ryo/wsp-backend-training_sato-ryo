// @flow

import { combineReducers } from 'redux';

import permission from './permission';
import type { $State } from '../../utils/TypeUtil';

const reducers = {
  permission,
};

export type State = $State<typeof reducers>;

export default combineReducers<Object, Object>(reducers);
