// @flow

import type { Dispatch } from 'redux';

type Variant = 'success' | 'warning' | 'error';

export type State = {|
  isShow: boolean,
  message: string,
  variant?: Variant,
|};

const initialState: State = {
  isShow: false,
  message: '',
};

type Show = {|
  type: 'COMMONS/MODULES/TOAST/SHOW',
  payload: {
    message: string,
    variant: Variant,
  },
|};

type Hide = {|
  type: 'COMMONS/MODULES/TOAST/HIDE',
|};

type Reset = {|
  type: 'COMMONS/MODULES/TOAST/RESET',
|};

export type Action = Show | Hide | Reset;

const SHOW: $PropertyType<Show, 'type'> = 'COMMONS/MODULES/TOAST/SHOW';

const HIDE: $PropertyType<Hide, 'type'> = 'COMMONS/MODULES/TOAST/HIDE';

const RESET: $PropertyType<Reset, 'type'> = 'COMMONS/MODULES/TOAST/RESET';

export const actions = {
  show: (message: string, variant: Variant = 'success'): Show => ({
    type: SHOW,
    payload: {
      message,
      variant,
    },
  }),
  hide: (): Hide => ({
    type: HIDE,
  }),
  reset: (): Reset => ({
    type: RESET,
  }),
};

const defaultDurationTime: number = 4000;

/**
 *
 * @param message A message will be displayed in toast.
 * @param duration The duration while toast is displayed (4000ms as default).
 *
 * Display a toast.
 * The toast will be hidden after a given `duration` is elapsed.
 * Note that the continued process will be blocked until toast is hidden
 * if you use it with `await`.
 *
 * NOTE
 * According to our best practice, the method running `dispatch` should not be implemented
 * in `module`, but utilities in commons is an exception.
 *
 *
 * トーストを表示し、設定時間経過後非表示にするメソッド
 * awaitをつけてしまうとトーストが出てから消えるまで次の処理を行わなくなるためawaitをつける必要はありません
 *
 * NOTE
 * 本来moduleの中でdispatchは行わないのだが、commonsに限り例外的に良しとしている
 */
export const showToast = (
  message: string,
  duration: number = defaultDurationTime
) => async (dispatch: Dispatch<any>) => {
  await dispatch(actions.show(message));

  return new Promise((resolve) => {
    setTimeout(() => {
      dispatch(actions.hide());
      resolve();
    }, duration);
  });
};

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case SHOW:
      const { message, variant } = action.payload;
      return {
        ...state,
        isShow: true,
        message,
        variant,
      };
    case HIDE:
      return {
        ...state,
        isShow: false,
      };

    case RESET: {
      return initialState;
    }
    default:
      return state;
  }
};
