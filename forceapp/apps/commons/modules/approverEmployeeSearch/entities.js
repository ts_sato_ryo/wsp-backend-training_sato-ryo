// @flow
import { createSelector } from 'reselect';
import { DIALOG_TYPE } from './ui/dialog';
import Employee from '../../../../widgets/dialogs/ProxyEmployeeSelectDialog/models/Employee';

type State = {
  allIds: Array<string>,
  byId: {
    [string]: Employee,
  },
};

const convertEmployee = (record) =>
  new Employee({
    id: record.id,
    employeeCode: record.code || '',
    employeeName: record.name || '',
    employeePhotoUrl: record.user.photoUrl,
    departmentCode: (record.department && record.department.code) || '',
    departmentName: (record.department && record.department.name) || '',
    title: record.title || '',
  });

/**
 * Convert an array of records into normalized list of employees
 * @param {Object[]} records
 * @return
 */
const convertEmployees = (records) => ({
  allIds: records.map((item) => item.id),
  byId: Object.assign(
    {},
    ...records.map((item) => ({ [item.id]: convertEmployee(item) }))
  ),
});

const ACTIONS = {
  SET_BY_REPOSITORY_RESPONSE_DATA:
    'COMMON/APPROVER_EMPLOYEE_SEARCH/LIST/SET_BY_REPOSITORY_RESPONSE_DATA',
  CLEAR: 'COMMON/APPROVER_EMPLOYEE_SEARCH/LIST/CLEAR',
};

export const actions = {
  setByRepositoryResponseData: (result: Array<*>) => ({
    type: ACTIONS.SET_BY_REPOSITORY_RESPONSE_DATA,
    payload: result,
  }),
  clear: () => ({ type: ACTIONS.CLEAR }),
};

/**
 * Select employees but the user's
 */
// $FlowFixMe
const employeesButUsersSelector = createSelector(
  (state) => state.common.userSetting.employeeId,
  (state) => state.common.accessControl.permission,
  (state) => state.common.approverEmployeeSearch.entities.allIds,
  (state) => state.common.approverEmployeeSearch.entities.byId,
  (state) => state.common.approverEmployeeSearch.ui.dialog.type,
  (userEmployeeId, permission, allIds, byId, dialogType) =>
    allIds
      .filter((id) => {
        if (id === userEmployeeId) {
          const {
            approveSelfAttDailyRequestByEmployee,
            approveSelfAttRequestByEmployee,
          } = permission;
          switch (dialogType) {
            case DIALOG_TYPE.AttDailyRequest:
              return approveSelfAttDailyRequestByEmployee;
            case DIALOG_TYPE.AttRequest:
              return approveSelfAttRequestByEmployee;
            default:
              return (
                approveSelfAttDailyRequestByEmployee ||
                approveSelfAttRequestByEmployee
              );
          }
        }
        return true;
      })
      .map((id) => byId[id])
);

/**
 * Select full data of the selected employee
 */
// $FlowFixMe
const selectedEmployeeIdSelector = createSelector(
  (state) =>
    state.common.approverEmployeeSearch.ui.operation.selectedEmployeeId,
  (state) => state.common.approverEmployeeSearch.entities.byId,
  (selectedEmployeeId, byId) => {
    return selectedEmployeeId in byId ? selectedEmployeeId : null;
  }
);

/**
 * Select full data of the selected employee
 */
// $FlowFixMe
const selectedEmployeeSelector = createSelector(
  (state) =>
    state.common.approverEmployeeSearch.ui.operation.selectedEmployeeId,
  (state) => state.common.approverEmployeeSearch.entities.byId,
  (selectedEmployeeId, byId) => {
    return selectedEmployeeId && selectedEmployeeId in byId
      ? byId[selectedEmployeeId]
      : null;
  }
);

export const selectors = {
  employeesButUsersSelector,
  selectedEmployeeIdSelector,
  selectedEmployeeSelector,
};

const initialState: State = {
  allIds: [],
  byId: {},
};

export default (state: State = initialState, action: *) => {
  const { type, payload } = action;
  switch (type) {
    case ACTIONS.SET_BY_REPOSITORY_RESPONSE_DATA:
      return convertEmployees(payload);
    case ACTIONS.CLEAR:
      return initialState;
    default:
      return state;
  }
};
