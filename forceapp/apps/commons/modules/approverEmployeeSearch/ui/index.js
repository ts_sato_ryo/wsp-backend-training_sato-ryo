// @flow
import { combineReducers } from 'redux';

import dialog from './dialog';
import operation from './operation';

export default combineReducers<Object, Object>({
  dialog,
  operation,
});
