/* @flow */
import { loadingStart, catchApiError, loadingEnd } from '../actions/app';
import Api from '../api';

export type PersonalSetting = {
  defaultJob: ?{
    id: string,
    code: string,
    name: string,
    validDateFrom: null | string,
    validDateTo: null | string,
  },
  plannerDefaultView: 'Daily' | 'Weekly' | 'Monthly',
};

type ResponseBody = PersonalSetting;

type FetchSuccessAction = {
  type: 'FETCH_PERSONAL_SETTING_SUCCESS',
  payload: PersonalSetting,
};

/**
 * Creates an action to update the Personal Setting
 * @param {PersonalSetting} result The response from backend API
 */
export const fetchSuccess = (result: ResponseBody): FetchSuccessAction => ({
  type: 'FETCH_PERSONAL_SETTING_SUCCESS',
  payload: result,
});

/**
 * Fetches the Personal Setting.
 * @see https://teamspiritdev.atlassian.net/wiki/spaces/GENIE/pages/518291840/personal-setting+get
 * @see {@link fetchSuccess}
 */
export const fetch = (empId?: string) => (
  dispatch: Dispatch<FetchSuccessAction>
): Promise<PersonalSetting> => {
  dispatch(loadingStart());

  return Api.invoke({
    path: '/personal-setting/get',
    param: { empId },
  })
    .then((result: ResponseBody) => {
      dispatch(fetchSuccess(result));
      return result;
    })
    .catch((error: Error) => catchApiError(error, { isContinuable: false }))
    .then((result) => {
      dispatch(loadingEnd());
      return result;
    });
};

type State = PersonalSetting;

const initialState: State = {
  plannerDefaultView: 'Weekly',
  defaultJob: null,
};

type Action = FetchSuccessAction;

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case 'FETCH_PERSONAL_SETTING_SUCCESS':
      return (action: FetchSuccessAction).payload;

    default:
      return state;
  }
};
