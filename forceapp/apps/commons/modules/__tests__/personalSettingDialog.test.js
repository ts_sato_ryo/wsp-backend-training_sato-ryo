import { LOADING_END, LOADING_START } from '../../actions/app';
import ApiMock from '../../../../__tests__/mocks/ApiMock';
import DispatcherMock from '../../../../__tests__/mocks/DispatcherMock';
import reducer, { save } from '../personalSettingDialog';

describe('action dispatchers', () => {
  describe('save()', () => {
    ApiMock.setDummyResponse(
      '/personal-setting/update',
      {
        defaultJobId: 'xxxxxxxx',
      },
      {}
    );
    ApiMock.setDummyResponse(
      '/personal-setting/get',
      {},
      {
        defaultJob: {
          id: 'xxxxxxxx',
          name: '共通ジョブ',
          validDateFrom: '2018-01-01',
          validDateTo: '2100-12-31',
        },
      }
    );

    const dispatcherMock = new DispatcherMock();

    // Call the action dispatcher of save
    save({
      defaultJob: {
        id: 'xxxxxxxx',
        name: '共通ジョブ',
      },
    })(dispatcherMock.dispatch);

    test('should show the Loading Spinner', () => {
      expect(dispatcherMock.logged[0]).toEqual({
        type: LOADING_START,
      });
    });

    test('should update the Personal Setting in the page after save', () => {
      expect(dispatcherMock.logged[3]).toEqual({
        type: 'FETCH_PERSONAL_SETTING_SUCCESS',
        payload: {
          defaultJob: {
            id: 'xxxxxxxx',
            name: '共通ジョブ',
            validDateFrom: '2018-01-01',
            validDateTo: '2100-12-31',
          },
        },
      });
    });

    // TODO Add a test case that the Default Job is null (not specified)

    test('should hide the Loading Spinner', () => {
      expect(dispatcherMock.logged[5]).toEqual({
        type: LOADING_END,
      });
    });
  });
});

describe('reducer', () => {
  describe('SHOW_PERSONAL_SETTING_DIALOG', () => {
    const prevState = {
      isVisible: false,
      newPersonalSetting: { defaultJob: null },
    };
    const nextState = reducer(prevState, {
      type: 'SHOW_PERSONAL_SETTING_DIALOG',
      payload: {
        defaultJob: {
          id: 'xxxxxxxx',
          name: '共通ジョブ',
        },
      },
    });

    test('should set the visibility of the Personal Setting Dialog to truthy', () => {
      expect(nextState.isVisible).toBeTruthy();
    });

    test('should set the editing Personal Setting to the current one', () => {
      expect(nextState.newPersonalSetting.defaultJob).toEqual({
        id: 'xxxxxxxx',
        name: '共通ジョブ',
      });
    });
  });

  describe('HIDE_PERSONAL_SETTING_DIALOG', () => {
    const prevState = {
      isVisible: true,
      newPersonalSetting: {
        defaultJob: {
          id: 'xxxxxxxx',
          name: '共通ジョブ',
        },
      },
    };
    const nextState = reducer(prevState, {
      type: 'HIDE_PERSONAL_SETTING_DIALOG',
    });

    test('should set the visibility of the Personal Setting Dialog to falsy', () => {
      expect(nextState.isVisible).toBeFalsy();
    });

    test('should clear the editing Personal Setting', () => {
      expect(nextState.newPersonalSetting.defaultJob).toBeNull();
    });
  });

  // TODO Add a test case for CHANGE_PERSONAL_SETTING_GROUP

  describe('SELECT_PERSONAL_SETTING_DIALOG_JOB_ITEM', () => {
    const prevState = {
      isVisible: true,
      newPersonalSetting: { defaultJob: null },
    };
    const nextState = reducer(prevState, {
      type: 'SELECT_PERSONAL_SETTING_DEFAULT_JOB_ITEM',
      payload: {
        id: 'xxxxxxxx',
        name: '共通ジョブ',
      },
    });

    test('should set the Default Job in the editing Personal Setting', () => {
      expect(nextState.newPersonalSetting.defaultJob).toEqual({
        id: 'xxxxxxxx',
        name: '共通ジョブ',
      });
    });
  });
});
