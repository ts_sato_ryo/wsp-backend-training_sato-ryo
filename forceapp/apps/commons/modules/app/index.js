// @flow

import { createSelector } from 'reselect';
import {
  type LoadingStartAction,
  type LoadingEndAction,
  type CatchApiErrorAction,
  type CatchUnexpectedErrorAction,
  type CatchBusinessErrorAction,
  type ClearErrorAction,
  type ConfirmDialogAction,
  type ConfirmDialogOpenAction,
  ConfirmDialogActionType,
  LOADING_START,
  LOADING_END,
  CATCH_API_ERROR,
  CATCH_UNEXPECTED_ERROR,
  CATCH_BUSINESS_ERROR,
  CLEAR_ERROR,
  loadingStart,
  loadingEnd,
  catchApiError,
  clearError,
  confirm,
} from '../../actions/app';

import type ApiError from '../../errors/ApiError';
import type BusinessError from '../../errors/BusinessError';

/** Define constants */

export const constants = {
  LOADING_START,
  LOADING_END,
  CATCH_API_ERROR,
  CLEAR_ERROR,
};

/** Define actions */

/**
 * アクション
 */
type Action =
  | LoadingStartAction
  | LoadingEndAction
  | CatchApiErrorAction
  | CatchUnexpectedErrorAction
  | CatchBusinessErrorAction
  | ClearErrorAction
  | ConfirmDialogAction;

export const actions = {
  loadingStart,
  loadingEnd,
  catchApiError,
  clearError,
  confirm,
};

/** Define selectors */

const getLoadingDepth = (state) => state.common.app.loadingDepth;

// $FlowFixMe
const loadingSelector = createSelector(
  getLoadingDepth,
  (loadingDepth) => loadingDepth > 0
);

export const selectors = { loadingSelector };

/** Define reducer */

type State = {
  loadingDepth: number,
  error: ApiError | BusinessError | null,
  unexpectedError: Error | null,
  confirmDialog: ?{
    message: string,
    callback: (boolean) => void,
  },
};

const initialState: State = {
  loadingDepth: 0,
  error: null,
  unexpectedError: null,
  confirmDialog: null,
};

/**
 * TODO Fix this
 * Loading spinner currently has loadingDepth as atomic state.
 */
export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case LOADING_START:
      return {
        ...state,
        loadingDepth: state.loadingDepth + 1,
      };

    case LOADING_END:
      // Prevent negative loadingDepth in case someone accidentally call loadingStart without loadingEnd
      const result = state.loadingDepth - 1;
      const loadingDepth = result < 0 ? 0 : result;
      return {
        ...state,
        loadingDepth,
      };

    /** TODO: 表示形式（ダイアログ or ページ全体）が異なる場合も、同じ格納場所で良いか別途検討 */
    /** TODO：APIエラー以外のエラーも同等？ APIエラー以外の想定ができていないので方針保留 */
    case CATCH_API_ERROR:
      if (state.error && state.error.isContinuable === false) {
        return state;
      } else {
        return {
          ...state,
          error: (action: CatchApiErrorAction).payload,
        };
      }
    case CATCH_UNEXPECTED_ERROR:
      return {
        ...state,
        unexpectedError: (action: CatchUnexpectedErrorAction).payload,
      };
    case CATCH_BUSINESS_ERROR:
      if (state.error && state.error.isContinuable === false) {
        return state;
      } else {
        return {
          ...state,
          error: (action: CatchBusinessErrorAction).payload,
        };
      }

    case CLEAR_ERROR:
      return {
        ...state,
        error: null,
      };

    case ConfirmDialogActionType.OPEN:
      return {
        ...state,
        confirmDialog: { ...(action: ConfirmDialogOpenAction).payload },
      };

    case ConfirmDialogActionType.CLOSE:
      return {
        ...state,
        confirmDialog: null,
      };

    default:
      return state;
  }
};
