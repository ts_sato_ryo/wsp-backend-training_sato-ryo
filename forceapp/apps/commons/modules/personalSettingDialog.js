/* @flow */
import { type PersonalSetting, fetch } from './personalSetting';
import { loadingStart, catchApiError, loadingEnd } from '../actions/app';
import { showToast } from './toast';
import Api from '../api';
import { type PersonalSettingGroup } from '../constants/personalSettingGroup';

import msg from '../languages';
import dispatchEvent from '../utils/EventUtil';
import { DEFAULT_JOB_UPDATED } from '../constants/customEventName';

type DefaultJob = ?$PropertyType<PersonalSetting, 'defaultJob'>;

type ShowDialogAction = {
  type: 'SHOW_PERSONAL_SETTING_DIALOG',
  payload: PersonalSetting,
};

/**
 * Creates an action to show the dialog of Personal Setting.
 * @return {ShowDialogAction}
 */
export const show = (
  originalPersonalSetting: PersonalSetting
): ShowDialogAction => ({
  type: 'SHOW_PERSONAL_SETTING_DIALOG',
  payload: originalPersonalSetting,
});

type HideDialogAction = {
  type: 'HIDE_PERSONAL_SETTING_DIALOG',
};

/**
 * Creates an action to hide the dialog of Personal Setting.
 * @return {HideDialogAction}
 */
export const hide = (): HideDialogAction => ({
  type: 'HIDE_PERSONAL_SETTING_DIALOG',
});

type ChangeGroupAction = {
  type: 'CHANGE_PERSONAL_SETTING_GROUP',
  payload: {
    newGroup: PersonalSettingGroup,
    originalPersonalSetting: PersonalSetting,
  },
};

/**
 * Creates an action to switch between the groups in dialog.
 */
export const changeGroup = (
  newGroup: PersonalSettingGroup,
  originalPersonalSetting: PersonalSetting
): ChangeGroupAction => ({
  type: 'CHANGE_PERSONAL_SETTING_GROUP',
  payload: {
    newGroup,
    originalPersonalSetting,
  },
});

type SelectDefaultJobItemAction = {
  type: 'SELECT_PERSONAL_SETTING_DEFAULT_JOB_ITEM',
  payload: ?DefaultJob,
};

export const selectDefaultJobItem = (
  newDefaultJob: any
): SelectDefaultJobItemAction => ({
  type: 'SELECT_PERSONAL_SETTING_DEFAULT_JOB_ITEM',
  payload: newDefaultJob,
});

/**
 * Save the Personal Setting.
 * @see https://teamspiritdev.atlassian.net/wiki/spaces/GENIE/pages/518291840/personal-setting+save
 */
export const save = (newPersonalSetting: PersonalSetting) => (
  dispatch: Dispatch<any>
) => {
  dispatch(loadingStart());

  return Api.invoke({
    path: '/personal-setting/update',
    param: {
      defaultJobId:
        newPersonalSetting.defaultJob && newPersonalSetting.defaultJob.id,
    },
  })
    .then(() => dispatch(fetch()))
    .then(() => dispatchEvent(DEFAULT_JOB_UPDATED))
    .catch((error: Error) => catchApiError(error, { isContinuable: false }))
    .then(() => {
      dispatch(loadingEnd());
      dispatch(hide());
      dispatch(showToast(msg().Com_Msg_SavingPersonalSetting));
    });
};

type State = {
  isVisible: boolean,
  selectedGroup: PersonalSettingGroup,
  newPersonalSetting: PersonalSetting,
};

const initialState: State = {
  isVisible: false,
  selectedGroup: 'TIME_TRACKING',
  newPersonalSetting: {
    plannerDefaultView: 'Weekly',
    defaultJob: null,
  },
};

type Action =
  | ShowDialogAction
  | HideDialogAction
  | ChangeGroupAction
  | SelectDefaultJobItemAction;

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case 'SHOW_PERSONAL_SETTING_DIALOG':
      return {
        ...state,
        isVisible: true,
        newPersonalSetting: (action: ShowDialogAction).payload,
      };
    case 'HIDE_PERSONAL_SETTING_DIALOG':
      return initialState;
    case 'CHANGE_PERSONAL_SETTING_GROUP':
      const {
        newGroup,
        originalPersonalSetting,
      } = (action: ChangeGroupAction).payload;
      return {
        ...state,
        selectedGroup: newGroup,
        newPersonalSetting: originalPersonalSetting,
      };
    case 'SELECT_PERSONAL_SETTING_DEFAULT_JOB_ITEM':
      return {
        ...state,
        newPersonalSetting: {
          ...state.newPersonalSetting,
          defaultJob: (action: SelectDefaultJobItemAction).payload,
        },
      };

    default:
      return state;
  }
};
