// @flow

import { combineReducers } from 'redux';

import app from './app';
import accessControl from './accessControl';
import standaloneMode from './standaloneMode';
import proxyEmployeeInfo from './proxyEmployeeInfo';
import delegateApprovalDialog from './delegateApprovalDialog';
import toast from './toast';
import type { $State } from '../utils/TypeUtil';

const reducers = {
  app,
  accessControl,
  standaloneMode,
  proxyEmployeeInfo,
  delegateApprovalDialog,
  toast,
};

export type State = $State<typeof reducers>;

export default combineReducers<Object, Object>(reducers);
