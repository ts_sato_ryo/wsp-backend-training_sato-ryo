// @flow

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import * as appActions from '../app';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('confirm(message, callback)', () => {
  test(`${appActions.ConfirmDialogActionType.OPEN} が作成される`, () => {
    // arrange
    const message = 'Hello World!';
    const callback = jest.fn();
    const expected = {
      type: 'CONFIRM_DIALOG_OPEN',
      payload: {
        message,
        callback,
      },
    };

    const store = mockStore({ confirmDialog: null });

    // run
    store.dispatch(appActions.confirm(message, callback));
    const action = store.getActions()[0];

    // assert
    expect(action.type).toEqual(expected.type);
    expect(action.payload.message).toEqual(expected.payload.message);
    expect(typeof action.payload.callback).toBe('function');
  });

  describe('confirm(message, callback).callback()', () => {
    const callback = jest.fn();
    const store = mockStore({ confirmDialog: null });

    beforeEach(() => {});

    test(`${appActions.ConfirmDialogActionType.CLOSE} が作成される`, () => {
      // arrange
      const expected = {
        type: 'CONFIRM_DIALOG_CLOSE',
      };
      store.dispatch(appActions.confirm('hello world!', callback));

      // run
      const openAction = store.getActions()[0];
      openAction.payload.callback(true);

      // assert
      const closeAction = store.getActions()[1];
      expect(closeAction).toEqual(expected);
    });

    test(`コールバック処理が実行される`, () => {
      // run
      const action = store.dispatch(
        appActions.confirm('hello world!', callback)
      );

      // run
      action.payload.callback(true);

      // assert
      expect(callback).toHaveBeenCalledWith(true);

      // run
      action.payload.callback(false);

      // assert
      expect(callback).toHaveBeenCalledWith(false);
    });
  });
});
