// @flow
import moment from 'moment';

import LangUtil from '../utils/LangUtil';
import { type UserSetting } from '../../domain/models/UserSetting';

import Api from '../api';

export const GET_USER_SETTING = 'GET_USER_SETTING';
export const GET_USER_SETTING_ERROR = 'GET_USER_SETTING_ERROR';

export type GetUserSettingRequestParameter = {
  detailSelectors?: ?Array<string>,
};
export type GetUserSettingSuccessAction = {|
  type: 'GET_USER_SETTING',
  payload: UserSetting,
|};

export type GetUserSettingErrorAction = {|
  type: 'GET_USER_SETTING_ERROR',
|};

function getUserSettingSuccess(
  result: UserSetting
): GetUserSettingSuccessAction {
  return {
    type: GET_USER_SETTING,
    payload: result,
  };
}

function getUserSettingError(): GetUserSettingErrorAction {
  return {
    type: GET_USER_SETTING_ERROR,
  };
}

export type Action = GetUserSettingSuccessAction | GetUserSettingErrorAction;

export function fetchUserSetting(
  param: GetUserSettingRequestParameter = {}
): Promise<UserSetting> {
  return Api.invoke({
    path: '/user-setting/get',
    param,
  });
}

export function getUserSetting(param: GetUserSettingRequestParameter = {}) {
  return (dispatch: Dispatch<Action>) => {
    return fetchUserSetting(param)
      .then((result) => {
        const language = LangUtil.convertSfLang(result.language);
        result.currencySymbol = result.currencySymbol || '';

        if (
          document.documentElement !== null &&
          document.documentElement !== undefined
        ) {
          document.documentElement.lang = language;
        }

        moment.locale(language);
        window.empInfo = { language }; // FIXME: for msg()

        dispatch(getUserSettingSuccess(result));

        return result;
      })
      .catch(() => {
        dispatch(getUserSettingError());
      });
  };
}
