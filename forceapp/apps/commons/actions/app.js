// @flow
import type ApiError from '../errors/ApiError';
import FatalError from '../errors/FatalError';
import ApexError from '../errors/ApexError';

import ApiUtil, { type BuildCustomErrorOptions } from '../utils/ApiUtil';
import BusinessError from '../errors/BusinessError';

import { type CustomConfirmDialogComponent } from '../components/dialogs/ConfirmDialog';

export const LOADING_START = 'LOADING_START';
export const LOADING_END = 'LOADING_END';
export const CATCH_API_ERROR = 'CATCH_API_ERROR';
export const CATCH_UNEXPECTED_ERROR = 'CATCH_UNEXPECTED_ERROR';
export const CATCH_BUSINESS_ERROR = 'CATCH_BUSINESS_ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';

export type LoadingStartAction = {
  type: 'LOADING_START',
};

export function loadingStart(): LoadingStartAction {
  return {
    type: LOADING_START,
  };
}

export type LoadingEndAction = {
  type: 'LOADING_END',
};

export function loadingEnd(): LoadingEndAction {
  return {
    type: LOADING_END,
  };
}

export const withLoading = <T>(
  ...processes: Array<(*) => Promise<T | *> | *>
) => (dispatch: Dispatch<*>): Promise<T | *> => {
  dispatch(loadingStart());
  return processes
    .reduce((acc, process) => acc.then(process), Promise.resolve())
    .then((result) => {
      dispatch(loadingEnd());
      return result;
    })
    .catch((err) => {
      dispatch(loadingEnd());
      throw err;
    });
};

/**
 * Confirm Dialog
 */

/**
 * Confirm Dialog を開くアクション
 */
export type ConfirmDialogOpenAction = {|
  type: 'CONFIRM_DIALOG_OPEN',
  payload: {|
    Component: CustomConfirmDialogComponent<*>,
    params: { [string]: * },
    callback: (boolean) => void,
  |},
|};

/**
 * Confirm Dialog を閉じるアクション
 */
export type ConfirmDialogCloseAction = {|
  type: 'CONFIRM_DIALOG_CLOSE',
|};

/**
 * Confirm Dialogのアクション型
 * @type {[type]}
 */
export type ConfirmDialogAction =
  | ConfirmDialogOpenAction
  | ConfirmDialogCloseAction;

type $ConfirmDialogActionType = {
  OPEN: $PropertyType<ConfirmDialogOpenAction, 'type'>,
  CLOSE: $PropertyType<ConfirmDialogCloseAction, 'type'>,
};

/**
 * Confirm Dialog に送れる型
 */
export type ConfirmDialogProps<T> =
  | string
  | string[]
  | {
      Component: CustomConfirmDialogComponent<T>,
      params: T,
    };

/**
 * Confirm Dialog のアクション
 */
export const ConfirmDialogActionType: $ConfirmDialogActionType = {
  OPEN: 'CONFIRM_DIALOG_OPEN',
  CLOSE: 'CONFIRM_DIALOG_CLOSE',
};

/**
 * Confirm Dialogを開きます。
 *
 * callback は非推奨になります。
 * 今後は confirm ダイアログから返される Promise の中で処理してください。
 */
export const confirm = <T>(
  props: ConfirmDialogProps<T>,
  callback?: (boolean) => void
) => (dispatch: Dispatch<*>) => {
  let params;

  if (
    props !== null &&
    props !== undefined &&
    typeof props === 'object' &&
    !Array.isArray(props)
  ) {
    params = { ...props };
  } else {
    params = {
      message: props,
    };
  }

  const createAction = (args: { [string]: * }, next: (boolean) => void) => {
    return dispatch({
      type: ConfirmDialogActionType.OPEN,
      payload: {
        ...args,
        callback: (yes: boolean) => {
          dispatch({ type: ConfirmDialogActionType.CLOSE });
          next(yes);
        },
      },
    });
  };

  if (callback) {
    return createAction(params, callback);
  } else {
    return (new Promise((resolve) =>
      createAction(params, resolve)
    ): Promise<boolean>);
  }
};

/**
 * [END] Confirm Dialog
 */

export type CatchBusinessErrorAction = {|
  type: 'CATCH_BUSINESS_ERROR',
  payload: BusinessError,
|};

/**
 * Action to catch and store a business error.
 */
export function catchBusinessError(
  type: string,
  problem: string,
  solution: null | string,
  options: BuildCustomErrorOptions = {},
  isFunctionCantUseError: boolean = false
): CatchBusinessErrorAction {
  return {
    type: CATCH_BUSINESS_ERROR,
    payload: new BusinessError(
      type,
      problem,
      solution,
      options,
      isFunctionCantUseError
    ),
  };
}

export type CatchApiErrorAction = {|
  type: 'CATCH_API_ERROR',
  payload: ApiError,
|};

export type CatchUnexpectedErrorAction = {|
  type: 'CATCH_UNEXPECTED_ERROR',
  payload: FatalError | ApexError,
|};

export type CatchError = CatchApiErrorAction | CatchUnexpectedErrorAction;

export function catchApiError(
  apiErr: any,
  options?: BuildCustomErrorOptions
): CatchError {
  if (apiErr instanceof Error) {
    return {
      type: CATCH_UNEXPECTED_ERROR,
      payload: new FatalError(apiErr),
    };
  } else if (apiErr.type === 'WSP_PRODUCTION_APEX_ERROR') {
    return {
      type: CATCH_UNEXPECTED_ERROR,
      payload: new ApexError(apiErr.event),
    };
  } else {
    return {
      type: CATCH_API_ERROR,
      payload: ApiUtil.buildCustomError((apiErr: Error), options),
    };
  }
}

export type ClearErrorAction = {
  type: 'CLEAR_ERROR',
};

export function clearError(): ClearErrorAction {
  return {
    type: CLEAR_ERROR,
  };
}
