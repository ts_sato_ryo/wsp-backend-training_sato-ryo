// @flow

export default ((): string => {
  /* eslint-disable global-require */
  if (process.env.SF_ENV === 'vfp') {
    return require('./vfp').default;
  } else {
    // storybook で表示できなくなってしまうので
    // 「本番」と「その他」という扱いにしています。
    return require('./local').default;
  }
  /* eslint-enable global-require */
})();
