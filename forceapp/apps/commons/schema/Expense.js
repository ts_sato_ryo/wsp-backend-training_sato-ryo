import * as Yup from 'yup';
import { get } from 'lodash';
import {
  totalNumTextExtendedItems,
  totalNumPicklistExtendedItems,
  totalNumLookupExtendedItems,
  totalNumDateExtendedItems,
} from '../../domain/models/exp/ExtendedItem';

import { RECEIPT_TYPE } from '../../domain/models/exp/Record';
import { VENDOR_PAYMENT_DUE_DATE_USAGE } from '../../domain/models/exp/Vendor';

// /////////////////////////////////////////////////////////////////////
//
// Report
//
// /////////////////////////////////////////////////////////////////////
export const subjectSchema = () =>
  Yup.string()
    .required('Common_Err_Required')
    .max(80, 'Common_Err_Max')
    .trim('Common_Err_Required');

export const accountingDateSchema = () =>
  Yup.string().required('Common_Err_Required');

export const expReportTypeIdSchema = (values) =>
  Yup.string()
    .test('expReportTypeId', 'Common_Err_Required', (value) => {
      const status = values.report.status;
      if (status === 'Removed' || status === 'NotRequested') {
        return value !== undefined;
      }
      return true;
    })
    .nullable();

export const vendorIdSchema = (values) =>
  Yup.string()
    .nullable()
    .test('vendorId', 'Common_Err_Required', (value) => {
      if (values.ui.isVendorRequired) {
        return !!value;
      }
      return true;
    });

export const costCenterNameSchema = (values) =>
  Yup.string()
    .nullable()
    .test('costCenterName', 'Common_Err_Required', (value) => {
      if (values.ui.isCostCenterRequired) {
        return !!value;
      }
      return true;
    });

export const jobIdSchema = (values) =>
  Yup.string()
    .nullable()
    .test('jobId', 'Common_Err_Required', (value) => {
      if (values.ui.isJobRequired) {
        return !!value;
      }
      return true;
    });

export const paymentDueDateSchema = (values, isRequest) =>
  Yup.string()
    .nullable()
    .test('Payment Due Date is Not Empty', 'Common_Err_Required', (value) => {
      if (
        values.report.vendorId &&
        values.report.paymentDueDateUsage ===
          VENDOR_PAYMENT_DUE_DATE_USAGE.Required
      ) {
        return !!value;
      }
      return true;
    })
    .test(
      'Payment Due Date is valid date',
      'Exp_Err_PaymentDateAfterRecordDate',
      (value) => {
        const paymentDueDate = new Date(value);
        const headerDate = isRequest
          ? new Date(values.report.scheduledDate)
          : new Date(values.report.accountingDate);

        if (paymentDueDate <= headerDate && value) {
          return false;
        }
        return true;
      }
    );

export const scheduledDateSchema = () =>
  Yup.string().required('Common_Err_Required');

export const purposeSchema = () =>
  Yup.string()
    .nullable()
    .required('Common_Err_Required');

// /////////////////////////////////////////////////////////////////////
//
// Record
//
// /////////////////////////////////////////////////////////////////////

// Error in the following cases.
// 1. recordType is General, attachmentBody is empty
// 2. recordType is GeneralOption, attachmentBody and remarks empty
export const receiptIdSchema = (values) =>
  Yup.string()
    .nullable()
    .test('General', 'Exp_Msg_NoRecipt', function validateNoReceipt(value) {
      const receiptType = this.parent.fileAttachment;
      const isReceiptNotUsed = receiptType === RECEIPT_TYPE.NotUsed;
      const isReceiptOptional = receiptType === RECEIPT_TYPE.Optional;
      const isGeneralRecord = this.parent.recordType === 'General';

      if (isReceiptNotUsed || isReceiptOptional) {
        return true;
      }

      if (!values.ui.saveMode && isGeneralRecord && !value) {
        return false;
      }

      return true;
    })
    .test(
      'GeneralReceiptOptional',
      'Exp_Err_NoReceiptAndReceipt',
      function validateAttachmentBody(value) {
        const receiptType = this.parent.fileAttachment;
        const isReceiptNotUsed = receiptType === RECEIPT_TYPE.NotUsed;
        const isReceiptOptional = receiptType === RECEIPT_TYPE.Optional;

        if (isReceiptNotUsed) {
          return true;
        }

        if (
          !values.ui.saveMode &&
          isReceiptOptional &&
          !value &&
          (!this.parent.items[0].remarks ||
            !this.parent.items[0].remarks.length)
        ) {
          return false;
        }
        return true;
      }
    );

// Error in the following cases.
// 1. recordType is TransitJorudanJP, selectedRoute is empty
export const routeInfoSchema = (values) =>
  Yup.object()
    .nullable()
    .shape({
      selectedRoute: Yup.object()
        .nullable()
        .test(
          'selectedRoute',
          'Exp_Err_NoSelectedRoute',
          function validateSelectedRoute(value) {
            const recordPath = this.path.match(/([^.]*\.[^.]*)./)[1];
            const recordType = get(values, `${recordPath}.recordType`);
            if (recordType === 'TransitJorudanJP' && !value) {
              return false;
            }
            return true;
          }
        ),
    });

// Error in the following cases.
// 1. recordDate and accountingPeriod do not match
export const recordDateSchema = (values) =>
  Yup.string()
    .test('is-date-in-range', 'Exp_Err_DateNotInRange', (value) => {
      const accountingPeriod = values.ui.selectedAccountingPeriod;
      const currentSelectedRecord = values.report.records[values.ui.recordIdx];
      const currentSelectedRecordDate =
        currentSelectedRecord && currentSelectedRecord.recordDate;
      let isCurrentSelectedRecordDateValid = true;

      if (accountingPeriod && currentSelectedRecordDate) {
        const currentRecordDate = new Date(currentSelectedRecordDate);
        const startDate = new Date(accountingPeriod.validDateFrom);
        const endDate = new Date(accountingPeriod.validDateTo);

        if (!(currentRecordDate >= startDate && currentRecordDate <= endDate)) {
          isCurrentSelectedRecordDateValid = false;
        }

        // if currentSelectedRecordDate is invalid, continue checking other record items date
        if (!isCurrentSelectedRecordDateValid) {
          const recordItemDate = new Date(value);

          if (!(recordItemDate >= startDate && recordItemDate <= endDate)) {
            return false;
          }
        }

        return true;
      }
      return true;
    })
    .required('Common_Err_Required');

// local function for recordDate.
const checkWorkingDayFlag = (recordDate, target, workingDays) =>
  !get(workingDays, `${recordDate}.${target}`, false);

// Error in the following cases.
// 1. recordDate and accountingPeriod do not match
// 2. recordDate is not working day.
export const recordDateWithCheckWorkingDaysSchema = (values) => {
  const { workingDays } = values.ui;
  return Yup.string()
    .test('is-date-leave', 'Exp_Warn_DateIsLeave', (value) =>
      checkWorkingDayFlag(value, 'isLeave', workingDays)
    )
    .test('is-date-holiday', 'Exp_Warn_DateIsHoliday', (value) =>
      checkWorkingDayFlag(value, 'isHoliday', workingDays)
    )
    .test('is-date-legal-holiday', 'Exp_Warn_DateIsLegalHoliday', (value) =>
      checkWorkingDayFlag(value, 'isLegalHoliday', workingDays)
    )
    .test(
      'is-date-leave-of-absence',
      'Exp_Warn_DateIsLeaveOfAbsence',
      (value) => checkWorkingDayFlag(value, 'isLeaveOfAbsence', workingDays)
    )
    .test('is-date-absence', 'Exp_Warn_DateIsAbsence', (value) =>
      checkWorkingDayFlag(value, 'isAbsence', workingDays)
    )
    .test(
      'is-date-att-record-not-found',
      'Exp_Warn_AttRecordNotFound',
      (value) => checkWorkingDayFlag(value, 'isAttRecordNotFound', workingDays)
    )
    .test(
      'is-date-illegal-att-calculation',
      'Exp_Warn_IllegalAttCalculation',
      (value) =>
        checkWorkingDayFlag(value, 'isIllegalAttCalculation', workingDays)
    )
    .test('is-date-in-range', 'Exp_Err_DateNotInRange', (value) => {
      const accountingPeriod = values.ui.selectedAccountingPeriod;

      if (accountingPeriod) {
        const currentDate = new Date(value);
        const startDate = new Date(accountingPeriod.validDateFrom);
        const endDate = new Date(accountingPeriod.validDateTo);

        if (!(currentDate >= startDate && currentDate <= endDate)) {
          return false;
        }
      }
      return true;
    })
    .required('Common_Err_Required');
};

// Error in the the following case.
// record's expense type doesn't match with report type
export const expTypeIdSchema = (values) =>
  Yup.string().test(
    'RecordExpenseTypeValidforReportType',
    'Exp_Err_InvalidRecordExpenseTypeForReportType',
    function validateRecordExpType(value) {
      // always return true if it's child record items
      if (!this.path.includes('items[0]')) {
        return true;
      }
      const availableExpType = values.ui.availableExpType || [];
      if (!availableExpType.includes(value)) {
        return false;
      }
      return true;
    }
  );

// Error in the the following case.
// record in base currency does not have taxTypeHistoryId (no tax type selected/available)
export const taxTypeHistoryIdSchema = () =>
  Yup.string()
    .nullable()
    .when('useForeignCurrency', {
      is: true,
      then: Yup.string(),
      otherwise: Yup.string().required('Exp_Err_NoTaxAvailable'),
    });

// Error in the following cases.
// recordType is FixedAllowanceMulti, amountSelection is empty
export const fixedAmountSelectionSchema = () =>
  Yup.string().required('Common_Err_Required');

// /////////////////////////////////////////////////////////////////////
//
// Items
//
// /////////////////////////////////////////////////////////////////////
export const amountSchema = () =>
  Yup.number()
    .required('Common_Err_Required')
    .max(999999999999, 'Common_Err_Max');

// Error in the following cases: items[n].recordDate is empty or out of range (skip items[0] which is parent record)
// use function(){} to access this.path in this scope
export const recordItemDateSchema = (values) =>
  Yup.string()
    .test('is-date-in-range', 'Exp_Err_DateNotInRange', function checkInRange(
      value
    ) {
      // always return true if it's parent record (items[0])
      if (this.path.includes('items[0]')) {
        return true;
      }
      const accountingPeriod = values.ui.selectedAccountingPeriod;
      if (accountingPeriod) {
        const currentDate = new Date(value);
        const startDate = new Date(accountingPeriod.validDateFrom);
        const endDate = new Date(accountingPeriod.validDateTo);

        if (!(currentDate >= startDate && currentDate <= endDate)) {
          return false;
        }
      }
      return true;
    })
    .test('is-required', 'Common_Err_Required', function checkRequired(value) {
      if (this.path.includes('items[0]')) {
        return true;
      }
      return value;
    });

// Error in the following cases: items[n].expTypeId is empty (skip items[0] which is parent record)
// use function(){} to access this.path in this scope
export const recordItemExpTypeIdSchema = () =>
  Yup.string().test(
    'is-required',
    'Common_Err_Required',
    function checkRequired(value) {
      if (this.path.includes('items[0]')) {
        return true;
      }
      return value;
    }
  );

// Error in the following cases: items[n].amount is empty or out of range (skip items[0] which is parent record)
// use function(){} to access this.path in this scope
export const recordItemAmountSchema = () =>
  Yup.number()
    .test('is-required', 'Common_Err_Required', function checkRequired(value) {
      if (this.path.includes('items[0]')) {
        return true;
      }
      return typeof value === 'number';
    })
    .test('too-large', 'Common_Err_Max', function checkMax(value) {
      if (this.path.includes('items[0]')) {
        return true;
      }
      return value <= 999999999999;
    });

// /////////////////////////////////////////////////////////////////////
//
// ExtendedItem
//
// /////////////////////////////////////////////////////////////////////
function validateExtendedItem(value) {
  const index = this.path.substr(-7, 2);
  const info = get(this.parent, `extendedItemText${index}Info`);
  const limitLength = info && info.limitLength;
  return value && limitLength ? !(value.length > limitLength) : true;
}

function validateEIIsRequired(isRequired, schema) {
  if (isRequired === 'true') {
    return schema.required('Common_Err_Required');
  }
  return schema;
}

export const getExtendedItemsValidators = () => {
  const eIValidators = {};

  [...Array(totalNumTextExtendedItems).keys()].forEach((i) => {
    const index = `0${i + 1}`.slice(-2);
    eIValidators[`extendedItemText${index}Value`] = Yup.string()
      .when(`extendedItemText${index}Info.isRequired`, validateEIIsRequired)
      .test(`ExtendedItem${index}`, 'Common_Err_Max', validateExtendedItem)
      .nullable();
    eIValidators[`extendedItemText${index}Info`] = Yup.object()
      .shape({
        isRequired: Yup.string(),
      })
      .nullable();
  });

  [...Array(totalNumPicklistExtendedItems).keys()].forEach((i) => {
    const index = `0${i + 1}`.slice(-2);
    eIValidators[`extendedItemPicklist${index}Value`] = Yup.string()
      .when(`extendedItemPicklist${index}Info.isRequired`, validateEIIsRequired)
      .nullable();
    eIValidators[`extendedItemPicklist${index}Info`] = Yup.object()
      .shape({
        isRequired: Yup.string(),
      })
      .nullable();
  });

  [...Array(totalNumLookupExtendedItems).keys()].forEach((i) => {
    const index = `0${i + 1}`.slice(-2);
    eIValidators[`extendedItemLookup${index}Value`] = Yup.string()
      .when(`extendedItemLookup${index}Info.isRequired`, validateEIIsRequired)
      .nullable();
    eIValidators[`extendedItemLookup${index}Info`] = Yup.object()
      .shape({
        isRequired: Yup.string(),
      })
      .nullable();
  });

  [...Array(totalNumDateExtendedItems).keys()].forEach((i) => {
    const index = `0${i + 1}`.slice(-2);
    eIValidators[`extendedItemDate${index}Value`] = Yup.string()
      .when(`extendedItemDate${index}Info.isRequired`, validateEIIsRequired)
      .nullable();
    eIValidators[`extendedItemDate${index}Info`] = Yup.object()
      .shape({
        isRequired: Yup.string(),
      })
      .nullable();
  });

  return eIValidators;
};

// /////////////////////////////////////////////////////////////////////
//
// Record Items Create/Edit
//
// /////////////////////////////////////////////////////////////////////

export const recordItemsEditSchema = (values) => {
  return Yup.object().shape({
    report: Yup.object().shape({
      records: Yup.array().of(
        Yup.object().shape({
          items: Yup.array().of(
            Yup.object().shape({
              amount: recordItemAmountSchema(),
              recordDate: recordItemDateSchema(values),
              expTypeId: recordItemExpTypeIdSchema(),
              taxTypeHistoryId: taxTypeHistoryIdSchema(),
              ...getExtendedItemsValidators(),
            })
          ),
        })
      ),
    }),
  });
};

export const recordItemsCreateSchema = (values) => {
  return Yup.object().shape({
    report: Yup.object().shape({
      records: Yup.array().of(
        Yup.object().shape({
          items: Yup.array().of(
            Yup.object().shape({
              amount: recordItemAmountSchema(),
              recordDate: recordItemDateSchema(values),
              expTypeId: recordItemExpTypeIdSchema(),
            })
          ),
        })
      ),
    }),
  });
};
