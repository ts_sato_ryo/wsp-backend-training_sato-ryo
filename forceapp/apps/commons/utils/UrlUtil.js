/* @flow */
import querystring from 'querystring';

const LOCATION_MAP = {
  'accounting-csv-pc': {
    // NOTE: このパスは計上仕訳の CSV 出力で使用するが local 環境では動かせないので null をセットする
    //       ref. GENIE-2026
    local: null,
    vfp: 'AccountingCsv',
  },
  'accounting-fb-pc': {
    // NOTE: このパスはFBデータ出力で使用するが local 環境では動かせないので null をセットする
    //       ref. GENIE-2068
    local: null,
    vfp: 'AccountingFBData',
  },
  'expenses-pc': {
    local: 'expenses-pc',
    vfp: 'ExpenseReport',
  },
  'expenses-pc-print': {
    local: 'expenses-pc-print',
    vfp: 'ExpenseReportPrint',
  },
  'requests-pc': {
    local: 'requests-pc',
    vfp: 'Requests',
  },
  'timesheet-pc': {
    local: 'timesheet-pc',
    vfp: 'TimeAttendance',
  },
  'timesheet-pc-summary': {
    local: 'timesheet-pc-summary',
    vfp: 'TimesheetSummary',
  },
  'timesheet-pc-leave': {
    local: 'timesheet-pc-leave',
    vfp: 'TimesheetLeave',
  },
  'test-api': {
    local: 'test-api',
  },
};

type QueryMap = { [key: string]: string };

export default class UrlUtil {
  // URLクエリを分割して格納したObjectを返す
  static getUrlQuery(): QueryMap | null {
    // TODO Return empty hash if no queries given
    let obj = null;
    const queryString = window.location.search.substring(1);
    if (queryString && queryString.length > 0) {
      obj = {};
      const pairs = queryString.split('&');
      for (const pair of pairs) {
        const elements = pair.split('=');
        const paramName = decodeURIComponent(elements[0]);
        const paramValue = decodeURIComponent(elements[1]);
        obj[paramName] = paramValue;
      }
    }

    return obj;
  }

  /**
   * Return relative url to the app whose ID is the specified one.
   * @param {string} appId Identifier of the app.
   * @param {Object} [param] Query parameters. Must be a hash.
   * @returns {?string} Relative url to the app.
   *     Return null if the app can't be found, or can't be opened current environment.
   */
  static appUrl(appId: string, param: QueryMap = {}): string | null {
    const queryString = querystring.stringify(param);

    // ローカル・LEX・Classic それぞれの環境で遷移方法が異なる
    if (UrlUtil.isLocal()) {
      if (!LOCATION_MAP[appId].local) {
        return null;
      }
      return `/${LOCATION_MAP[appId].local}/index.html?${queryString}`;
    }
    if (UrlUtil.isLex()) {
      return `/apex/${LOCATION_MAP[appId].vfp}?${queryString}`;
    }
    // Classic 環境
    return `./${LOCATION_MAP[appId].vfp}?${queryString}`;
  }

  /**
   * Go the app whose ID is the specified one if it avails.
   * @param {string} appId Identifier of the app.
   * @param {Object} [param] Query parameters. Must be a hash.
   */
  static navigateTo(appId: string, param: QueryMap = {}): void {
    const appUrl = UrlUtil.appUrl(appId, param);

    if (!appUrl) {
      // ローカル環境で動かない場合は処理を中断
      return;
    }
    if (UrlUtil.isLex()) {
      // Salesforce1 navigation
      window.sforce.one.navigateToURL(appUrl);
    }

    location.href = appUrl;
  }

  /**
   * Open a new window with URL parameters (optional).
   * @param {string} appId Identifier of the app.
   * @param {?Object} [param] Query parameters. Must be a hash.
   * @returns {?Window} Window object if the app were opened. Otherwise returns null.
   */
  static openApp(appId: string, param: QueryMap = {}): window | null {
    const appUrl = UrlUtil.appUrl(appId, param);

    if (appUrl) {
      return window.open(appUrl);
    }

    return null;
  }

  static isLex(): boolean {
    // ref. http://salesforce.stackexchange.com/questions/44148/salesforce1-navigatetourl-method
    return typeof window.sforce !== 'undefined' && window.sforce !== null;
  }

  /**
   * ローカル環境かどうかの判定
   * 現在は「URL に `.html` が含まれるかどうか」という簡易的な判定になっている
   */
  static isLocal(): boolean {
    return window.location.href.split('.')[1] === 'html';
  }
}
