// @flow

/**
 * Identity function
 */
export const identity = <T>(x: T): T => x;

type Fn<X, Y> = (X) => Y;

/**
 * Internal implementation for compose
 */
const compose_ = (...fns: Function[]): Function =>
  fns.reduceRight(
    (g: Function, f: Function) => (...args) => f(g(...args)),
    (x) => x
  );

/**
 * Compose functions
 * @param fns Functions
 * @return Composed function
 */
export const compose: (<X0>(void) => X0) &
  (<X0, X1>(Fn<X0, X1>, void) => Fn<X0, X1>) &
  (<X0, X1, X2>(Fn<X1, X2>, Fn<X0, X1>, void) => Fn<X0, X2>) &
  (<X0, X1, X2, X3>(Fn<X2, X3>, Fn<X1, X2>, Fn<X0, X1>, void) => Fn<X0, X3>) &
  (<X0, X1, X2, X3, X4>(
    Fn<X3, X4>,
    Fn<X2, X3>,
    Fn<X1, X2>,
    Fn<X0, X1>,
    void
  ) => Fn<X0, X4>) &
  (<X0, X1, X2, X3, X4, X5>(
    Fn<X4, X5>,
    Fn<X3, X4>,
    Fn<X2, X3>,
    Fn<X1, X2>,
    Fn<X0, X1>,
    void
  ) => Fn<X0, X5>) &
  (<X0, X1, X2, X3, X4, X5, X6>(
    Fn<X5, X6>,
    Fn<X4, X5>,
    Fn<X3, X4>,
    Fn<X2, X3>,
    Fn<X1, X2>,
    Fn<X0, X1>,
    void
  ) => Fn<X0, X6>) &
  (<X0, X1, X2, X3, X4, X5, X6, X7>(
    Fn<X6, X7>,
    Fn<X5, X6>,
    Fn<X4, X5>,
    Fn<X3, X4>,
    Fn<X2, X3>,
    Fn<X1, X2>,
    Fn<X0, X1>,
    void
  ) => Fn<X0, X7>) &
  (<X0, X1, X2, X3, X4, X5, X6, X7, X8>(
    Fn<X7, X8>,
    Fn<X6, X7>,
    Fn<X5, X6>,
    Fn<X4, X5>,
    Fn<X3, X4>,
    Fn<X2, X3>,
    Fn<X1, X2>,
    Fn<X0, X1>,
    void
  ) => Fn<X0, X8>) &
  (<X0, X1, X2, X3, X4, X5, X6, X7, X8, X9>(
    Fn<X8, X9>,
    Fn<X7, X8>,
    Fn<X6, X7>,
    Fn<X5, X6>,
    Fn<X4, X5>,
    Fn<X3, X4>,
    Fn<X2, X3>,
    Fn<X1, X2>,
    Fn<X0, X1>,
    void
  ) => Fn<X0, X9>) &
  (<T, R>(Array<Fn<T, R>>) => Fn<T, R>) = (compose_: any); // Hack

/**
 * Internal implementation for compose
 */
const pipe_ = (...fns: Function[]): Function =>
  fns.reduceRight(
    (f: Function, g: Function) => (...args) => f(g(...args)),
    (x) => x
  );

/**
 * Pipe
 * @param fns Functions
 * @return Composed function
 */
export const pipe: (<X0>(void) => X0) &
  (<X0, X1>(Fn<X0, X1>, void) => Fn<X0, X1>) &
  (<X0, X1, X2>(Fn<X0, X1>, Fn<X1, X2>, void) => Fn<X0, X2>) &
  (<X0, X1, X2, X3>(Fn<X0, X1>, Fn<X1, X2>, Fn<X2, X3>, void) => Fn<X0, X3>) &
  (<X0, X1, X2, X3, X4>(
    Fn<X0, X1>,
    Fn<X1, X2>,
    Fn<X2, X3>,
    Fn<X3, X4>,
    void
  ) => Fn<X0, X4>) &
  (<X0, X1, X2, X3, X4, X5>(
    Fn<X0, X1>,
    Fn<X1, X2>,
    Fn<X2, X3>,
    Fn<X3, X4>,
    Fn<X4, X5>,
    void
  ) => Fn<X0, X5>) &
  (<X0, X1, X2, X3, X4, X5, X6>(
    Fn<X0, X1>,
    Fn<X1, X2>,
    Fn<X2, X3>,
    Fn<X3, X4>,
    Fn<X4, X5>,
    Fn<X5, X6>,
    void
  ) => Fn<X0, X6>) &
  (<X0, X1, X2, X3, X4, X5, X6, X7>(
    Fn<X0, X1>,
    Fn<X1, X2>,
    Fn<X2, X3>,
    Fn<X3, X4>,
    Fn<X4, X5>,
    Fn<X5, X6>,
    Fn<X6, X7>,
    void
  ) => Fn<X0, X7>) &
  (<X0, X1, X2, X3, X4, X5, X6, X7, X8>(
    Fn<X0, X1>,
    Fn<X1, X2>,
    Fn<X2, X3>,
    Fn<X3, X4>,
    Fn<X4, X5>,
    Fn<X5, X6>,
    Fn<X6, X7>,
    Fn<X7, X8>,
    void
  ) => Fn<X0, X8>) &
  (<X0, X1, X2, X3, X4, X5, X6, X7, X8, X9>(
    Fn<X0, X1>,
    Fn<X1, X2>,
    Fn<X2, X3>,
    Fn<X3, X4>,
    Fn<X4, X5>,
    Fn<X5, X6>,
    Fn<X6, X7>,
    Fn<X7, X8>,
    Fn<X8, X9>,
    void
  ) => Fn<X0, X9>) &
  (<T, R>(Array<Fn<T, R>>) => Fn<T, R>) = (pipe_: any); // Hack
