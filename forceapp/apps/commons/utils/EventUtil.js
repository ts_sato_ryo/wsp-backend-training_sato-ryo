// @flow

export default function dispatchEvent(name: string, properties?: Object = {}) {
  const event = new CustomEvent(name, properties);
  window.dispatchEvent(event);
}
