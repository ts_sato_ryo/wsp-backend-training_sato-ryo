// @flow
import moment from 'moment';
import {
  startOfMonth,
  endOfMonth,
  startOfWeek,
  endOfWeek,
  eachDay,
} from 'date-fns';

type Day = {
  Sunday: 0,
  Monday: 1,
  Tuesday: 2,
  Wednesday: 3,
  Thursday: 4,
  Friday: 5,
  Saturday: 6,
};

const defaultLocale = (): string => {
  if (document.documentElement && document.documentElement.lang) {
    return document.documentElement.lang;
  } else if (window.navigator.userLanguage || window.navigator.language) {
    return window.navigator.userLanguage || window.navigator.language;
  } else {
    throw new Error("Cannot read property 'lang' of undefined or null");
  }
};

export default class CalendarUtil {
  /**
   * カレンダーの開始曜日をもとに対象月の月カレンダーの開始日を返す
   * @param month {Moment} 対象月のMomentオブジェクト
   * @param startDayOfTheWeek {int} 週の開始曜日（ Sunday(0) to Satureday(6) ）
   * @return startDate {Moment} 月カレンダーの開始日
   */
  static getStartDateOfMonthView(
    month: moment,
    startDayOfTheWeek: number
  ): moment {
    const startDateOfTheMonth = month.clone().startOf('month');
    let startDate = startDateOfTheMonth.clone().day(startDayOfTheWeek);
    if (startDate.isAfter(startDateOfTheMonth)) {
      startDate = startDateOfTheMonth
        .clone()
        .add(-1, 'w')
        .day(startDayOfTheWeek);
    }
    return startDate;
  }

  /**
   * Return calendar dates as of a given `targetDate`
   */
  static getCalendarAsOf(
    targetDate: Date,
    option: { weekStartsOn: $Values<Day> } = { weekStartsOn: 0 }
  ): $ReadOnlyArray<Date> {
    const start = startOfWeek(startOfMonth(targetDate), option);
    const end = endOfWeek(endOfMonth(targetDate), option);
    return eachDay(start, end);
  }

  /**
   * format day for calendar
   */
  static format(
    date: Date,
    options: Intl$DateTimeFormatOptions,
    locale: string = defaultLocale()
  ) {
    return new Intl.DateTimeFormat(locale, options).format(date);
  }
}
