// @flow
import { cloneDeep, set } from 'lodash';

/* eslint-disable import/prefer-default-export */
export const updateValues = <T: Object, U: Object, O: Object>(
  values: T,
  touched: U,
  updateObj: O
): { values: T, touched: U } => {
  const tmpValues = cloneDeep(values);
  const tmpTouched = cloneDeep(touched);

  Object.keys(updateObj).forEach((key) => {
    set(tmpValues, key, updateObj[key]);
    set(tmpTouched, key, true);
  });
  return { values: tmpValues, touched: tmpTouched };
};
