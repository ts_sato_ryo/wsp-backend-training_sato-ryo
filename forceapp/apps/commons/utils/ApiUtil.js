// @flow
import ApiError from '../errors/ApiError';

export type BuildCustomErrorOptions = {
  errorCode?: string,
  message?: string,
  stackTrace?: string,
  isContinuable?: boolean,
};

export default class ApiUtil {
  /**
   * @param {Error} apiErr
   * @param {String} apiErr.errorCode
   * @param {String} apiErr.message
   * @param {String} apiErr.stackTrace
   * @param {Object} [options]
   * @param {Boolean} [options.isContinuable]
   */
  static buildCustomError(
    apiErr: Error,
    options?: BuildCustomErrorOptions
  ): ApiError {
    return new ApiError(apiErr, options);
  }
}
