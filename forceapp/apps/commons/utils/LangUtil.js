export default class LangUtil {
  /**
   * Salesforceから取得したLangをRFCに合わせて変換する
   */
  static convertSfLang(lang) {
    return lang.replace('_', '-');
  }
}
