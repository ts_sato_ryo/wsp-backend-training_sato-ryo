import _ from 'lodash';
import FormatUtil from './FormatUtil';
import TAX_TYPE from '../constants/taxType';

export default class TaxUtil {
  // 合計金額と税率から税額を計算
  static calcGstVat(totalAmount, taxRate, rounding, scale) {
    const unformatTotalAmount = FormatUtil.unformat(totalAmount);
    const unformatTaxRate = FormatUtil.unformat(taxRate) / 100;
    return FormatUtil.roundValue(
      unformatTotalAmount * (unformatTaxRate / (1 + unformatTaxRate)),
      rounding,
      scale
    );
  }

  // 合計金額から税額を引き税抜き金額を算出する
  static calcAmountWithoutTax(totalAmount, gstVat, rounding, scale) {
    const unformatTotalAmount = FormatUtil.unformat(totalAmount);
    const unformatGstVat = FormatUtil.unformat(gstVat);
    return FormatUtil.roundValue(
      unformatTotalAmount - unformatGstVat,
      rounding,
      scale
    );
  }

  /**
   * 税種別が「総額」「内訳毎」の場合に経費明細の合計金額、税抜金額を計算する際に呼ばれる関数
   * 税種別が「総額」の場合、明細の税抜金額 + 税額1 + 税額2の値を返す
   * 税種別が「内訳毎」の場合、明細の合計金額 - 税額1 - 税額2の値を返す
   * @param  {String} amount           経費明細の税抜金額、もしくは合計金額
   * @param  {String} gstVat1          経費明細の税額1
   * @param  {String} gstVat2          経費明細の税額2
   * @param  {String} type             税種別
   */
  static calcRecordAmountByTaxType(amount, gstVat1, gstVat2, type) {
    const unformattedAmount = FormatUtil.unformat(amount);
    const unformattedGstVat1 = FormatUtil.unformat(gstVat1);
    const unformattedGstVat2 = FormatUtil.unformat(gstVat2);

    if (this.isTaxTypeTotal(type)) {
      return FormatUtil.convertToIntegerString(
        unformattedAmount + unformattedGstVat1 + unformattedGstVat2
      );
    } else {
      return FormatUtil.convertToIntegerString(
        unformattedAmount - unformattedGstVat1 - unformattedGstVat2
      );
    }
  }

  /**
   * 内訳レコードにある同じ税率の金額を集計する関数
   * @param  {Array}  recordItemList  内訳レコードList
   * @param  {Number} taxRate         税率
   * @return {String} Formatした集計金額
   */
  static calcRecordItemGstVatBySameRate(recordItemList, taxRate) {
    const totalAmount = recordItemList.reduce((previous, current) => {
      const selectedExpTax = this.findSelectedExpTax(
        current.expTaxTypeList,
        current.taxTypeId
      );
      if (selectedExpTax !== null && selectedExpTax.rate === taxRate) {
        return previous + FormatUtil.unformat(current.amount);
      }
      return previous;
    }, 0);
    const totalGstVat = this.calcGstVat(totalAmount, taxRate);
    return FormatUtil.convertToIntegerString(totalGstVat);
  }

  /**
   * TaxTypeがTotal(総額)かどうかを判定する関数
   * 内訳無しの場合は税額が手入力できる場合もTaxTypeはTotalとなるため同じ関数を使用する
   * @param  {String}  type 税種別
   * @return {Boolean}      TaxTypeがTotalの場合、True。それ以外はFalse
   */
  static isTaxTypeTotal(type) {
    return type === TAX_TYPE.TOTAL;
  }

  /**
   * TaxTypeがPerRecordItem(内訳毎)かどうかを判定する関数
   * 内訳無しの場合は税額が自動計算となる場合もTaxTypeはPerRecordItemとなるため同じ関数を使用する
   * @param  {String}  type 税種別
   * @return {Boolean}      TaxTypeがPerRecordItemの場合、True。それ以外はFalse
   */
  static isTaxTypePerRecordItem(type) {
    return type === TAX_TYPE.PER_RECORD_ITEM;
  }

  /**
   * TaxTypeがNontaxable(課税なし)かどうかを判定する関数
   * @param  {String}  type 税種別
   * @return {Boolean}      TaxTypeがNontaxableの場合、True。それ以外はFalse
   */
  static isTaxTypeNonTaxable(type) {
    return type === TAX_TYPE.NONTAXABLE;
  }

  /**
   * TaxTypeごとの税額を取得する関数
   * 税額の自動計算・手動入力を切り替えた際に税額を計算する
   * taxType = "PerRecordItem" の場合、合計金額と税率から計算、taxType = "Total"の場合、手入力の値を返す
   * @param  {number} amount          合計金額
   * @param  {number} gstVat          税額
   * @param  {number} taxRate         税率
   * @param  {String} type            税種別。Total・・手入力、PerRecordItem・・自動入力
   * @return {number} 税額の値
   */
  static getGstVatByTaxType(amount, gstVat, taxRate, type) {
    return TaxUtil.isTaxTypePerRecordItem(type)
      ? TaxUtil.calcGstVat(amount, taxRate)
      : gstVat;
  }

  /**
   * 取得できたexpTaxTypeListから税率の選択リストに表示するためのラベル名と初期選択する税率をセットする関数
   * @param  {Array} expTaxTypeList 税率の選択リスト
   * @param  {String} type 税種別
   */
  static setInitialExpTaxLabel(expTaxTypeList) {
    const editingExpTaxTypeList = _.cloneDeep(expTaxTypeList);
    editingExpTaxTypeList.map((expTax) => {
      expTax.label =
        expTax.rate > 0
          ? FormatUtil.convertToDisplayingPercent(expTax.rate)
          : expTax.name;
      return expTax;
    });
    return editingExpTaxTypeList;
  }

  /**
   * 税率選択リストのうち、選択したitemを取得する
   * @param  {array} expTaxTypeList 税率の選択リスト
   * @param  {String} selectedTaxTypeId 選択した税区分レコードID
   * @return {Object}                   選択した税率のexpTax(Object)の値
   */
  static findSelectedExpTax(expTaxTypeList, selectedTaxTypeId = null) {
    if (_.isEmpty(expTaxTypeList) || selectedTaxTypeId === null) {
      return null;
    }
    return expTaxTypeList.find((item) => {
      return item.id === selectedTaxTypeId;
    });
  }

  /**
   * 選択できる税率のうち、課税のレコードが2件以上あるかを判定する関数
   * @param  {Array}  expTaxTypeList
   * @return {Boolean}  選択できる税率のうち、課税のレコードが2件以上ある場合True,それ以外False
   */
  static hasMultipleTaxRecord(expTaxTypeList) {
    const filteredExpTaxTypeList = expTaxTypeList.filter((expTax) => {
      return expTax.rate > 0;
    });
    return filteredExpTaxTypeList.length >= 2;
  }
}
