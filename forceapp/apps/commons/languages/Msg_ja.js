/* @flow */

const ja = {
  /**
   * @description [ja] 直行直帰
   */
  $Att_Lbl_RequestTypeDirect: '直行直帰',

  /**
   * @description [ja] 申請期間
   */
  $Exp_Clbl_AccountingPeriod: '申請期間',

  /**
   * @description [ja] 金額
   */
  $Exp_Clbl_Amount: '金額',

  /**
   * @description [ja] コストセンター
   */
  $Exp_Clbl_CostCenter: 'コストセンター',

  /**
   * @description [ja] 通貨
   */
  $Exp_Clbl_Currency: '通貨',

  /**
   * @description [ja] 日付
   */
  $Exp_Clbl_Date: '日付',

  /**
   * @description [ja] 為替レート
   */
  $Exp_Clbl_ExchangeRate: '為替レート',

  /**
   * @description [ja] 費目
   */
  $Exp_Clbl_ExpenseType: '費目',

  /**
   * @description [ja] 税区分
   */
  $Exp_Clbl_Gst: '税区分',

  /**
   * @description [ja] 税額
   */
  $Exp_Clbl_GstAmount: '税額',

  /**
   * @description [ja] 消費税率
   */
  $Exp_Clbl_GstRate: '消費税率',

  /**
   * @description [ja] 税込金額
   */
  $Exp_Clbl_IncludeTax: '税込金額',

  /**
   * @description [ja] 現地金額
   */
  $Exp_Clbl_LocalAmount: '現地金額',

  /**
   * @description [ja] 申請から作成
   */
  $Exp_Clbl_NewReport: '申請から作成',

  /**
   * @description [ja] 目的
   */
  $Exp_Clbl_Purpose: '目的',

  /**
   * @description [ja] 計上日
   */
  $Exp_Clbl_RecordDate: '計上日',

  /**
   * @description [ja] 備考
   */
  $Exp_Clbl_ReportRemarks: '備考',

  /**
   * @description [ja] 件名
   */
  $Exp_Clbl_ReportTitle: '件名',

  /**
   * @description [ja] 申請種別
   */
  $Exp_Clbl_ReportType: '申請種別',

  /**
   * @description [ja] 件名
   */
  $Exp_Clbl_RequestTitle: '件名',

  /**
   * @description [ja] 購入予定日
   */
  $Exp_Clbl_ScheduledDate: '購入予定日',

  /**
   * @description [ja] 摘要
   */
  $Exp_Clbl_Summary: '摘要',

  /**
   * @description [ja] 種別
   */
  $Exp_Clbl_Type: '種別',

  /**
   * @description [ja] 税抜金額
   */
  $Exp_Clbl_WithoutTax: '税抜金額',

  /**
   * @description [ja] 各種勤怠申請、勤務確定申請の承認者を、「承認権限フラグ01」が有効な他の社員に変更できるようにします
   */
  Admin_Help_AllowToChangeApproverHelp: '各種勤怠申請、勤務確定申請の承認者を、「承認権限フラグ01」が有効な他の社員に変更できるようにします',

  /**
   * @description [ja] 勤務体系の「申請者本人による承認者の選択を認める」が有効のとき、このフラグが有効な社員を承認者として選択できます
   */
  Admin_Help_ApprovalAuthority01: '勤務体系の「申請者本人による承認者の選択を認める」が有効のとき、このフラグが有効な社員を承認者として選択できます',

  /**
   * @description [ja] 名前かコードの一部を入力することで検索を行います
   */
  Admin_Help_AutoSuggest: '名前かコードの一部を入力することで検索を行います',

  /**
   * @description [ja] 名前かユーザ名の一部を入力することで検索を行います
   */
  Admin_Help_AutoSuggestSFUser: '名前かユーザ名の一部を入力することで検索を行います',

  /**
   * @description [ja] 0:00から境界時刻以前までの間に退勤打刻した場合、当日の退勤ではなく、前日の24:00以降に退勤打刻したものと見なします。何も設定しない場合、05:00 として扱います。
   */
  Admin_Help_BoundaryOfEndTime: '0:00から境界時刻以前までの間に退勤打刻した場合、当日の退勤ではなく、前日の24:00以降に退勤打刻したものと見なします。何も設定しない場合、05:00 として扱います。',

  /**
   * @description [ja] 0:00から境界時刻までの間に出勤打刻した場合、当日の出勤ではなく、前日の24:00以降に出勤打刻したものと見なします。何も設定しない場合、05:00 として扱います。
   */
  Admin_Help_BoundaryOfStartTime: '0:00から境界時刻までの間に出勤打刻した場合、当日の出勤ではなく、前日の24:00以降に出勤打刻したものと見なします。何も設定しない場合、05:00 として扱います。',

  /**
   * @description [ja] 外部システム連携用のコードを入力します
   */
  Admin_Help_CostCenterCodeDescription: '外部システム連携用のコードを入力します',

  /**
   * @description [ja] {$Att_Lbl_RequestTypeDirect}申請を行う際にデフォルトで表示される出退勤時刻を設定します
   */
  Admin_Help_DirectRequestDefaultAttendanceTime: '{$Att_Lbl_RequestTypeDirect}申請を行う際にデフォルトで表示される出退勤時刻を設定します',

  /**
   * @description [ja] 終業時刻やフレックス時間帯の直後に、申請なしでも勤務時間と認める時間帯を設ける場合に設定します
   */
  Admin_Help_EndTimeOfAcceptAsWorkingHour: '終業時刻やフレックス時間帯の直後に、申請なしでも勤務時間と認める時間帯を設ける場合に設定します',

  /**
   * @description [ja] 入力内容は経費申請と事前申請入力フォームで表示されます。
   */
  Admin_Help_ExpExtendedItem: '入力内容は経費申請と事前申請入力フォームで表示されます。',

  /**
   * @description [ja] 入力内容は費目選択ダイアログで表示されます。
   */
  Admin_Help_ExpTypeDescription: '入力内容は費目選択ダイアログで表示されます。',

  /**
   * @description [ja] 設定した順番で費目選択ダイアログに昇順で表示されます。
   */
  Admin_Help_ExpTypeOrder: '設定した順番で費目選択ダイアログに昇順で表示されます。',

  /**
   * @description [ja] 費目選択ダイアログで費目および費目グループを階層構造にまとめるための親となるグループを設定します。
   */
  Admin_Help_ExpTypeParent: '費目選択ダイアログで費目および費目グループを階層構造にまとめるための親となるグループを設定します。',

  /**
   * @description [ja] 選択されたタイプに応じて経費明細の入力項目が変わります。
   */
  Admin_Help_ExpTypeRecordType: '選択されたタイプに応じて経費明細の入力項目が変わります。',

  /**
   * @description [ja] 費目に設定する税率を設定します。
   */
  Admin_Help_ExpTypeTaxType: '費目に設定する税率を設定します。',

  /**
   * @description [ja] この金額リストは経費申請、事前申請に表示されます。
   */
  Admin_Help_FixedAmountMultiple: 'この金額リストは経費申請、事前申請に表示されます。',

  /**
   * @description [ja] 明細にこの金額が設定され、ユーザーは変更することができません
   */
  Admin_Help_FixedAmountSingle: '明細にこの金額が設定され、ユーザーは変更することができません',

  /**
   * @description [ja] Shift、又はCtrl/Commandキー押下することで複数の項目を選択できます
   */
  Admin_Help_LeaveCodeList: 'Shift、又はCtrl/Commandキー押下することで複数の項目を選択できます',

  /**
   * @description [ja] 休暇申請時に、「備考」欄の代わりに「理由」欄（必須入力）が表示されるようになります。
   */
  Admin_Help_LeaveRequireReason: '休暇申請時に、「備考」欄の代わりに「理由」欄（必須入力）が表示されるようになります。',

  /**
   * @description [ja] ヘルプメッセージ
   */
  Admin_Help_Name: 'ヘルプメッセージ',

  /**
   * @description [ja] このヒントは経費申請のみに表示されます
   */
  Admin_Help_OnlyExpense: 'このヒントは経費申請のみに表示されます',

  /**
   * @description [ja] このヒントは事前申請のみに表示されます
   */
  Admin_Help_OnlyRequest: 'このヒントは事前申請のみに表示されます',

  /**
   * @description [ja] 有効にすると、打刻時に現在地を送信できるようになります（社員が現在地を送信しなかった場合や、現在地取得に失敗した場合は、メッセージの入力が必須となります）。
   */
  Admin_Help_RequireLocationAtMobileStamp: '有効にすると、打刻時に現在地を送信できるようになります（社員が現在地を送信しなかった場合や、現在地取得に失敗した場合は、メッセージの入力が必須となります）。',

  /**
   * @description [ja] 始業時刻やフレックス時間帯の直前に、申請なしでも勤務時間と認める時間帯を設ける場合に設定します
   */
  Admin_Help_StartTimeOfAcceptAsWorkingHour: '始業時刻やフレックス時間帯の直前に、申請なしでも勤務時間と認める時間帯を設ける場合に設定します',

  /**
   * @description [ja] Shift、又はCtrl/Commandキー押下することで複数の項目を選択できます
   */
  Admin_Help_WorkPatternCodeList: 'Shift、又はCtrl/Commandキー押下することで複数の項目を選択できます',

  /**
   * @description [ja] 36協定
   */
  Admin_Lbl_36kyotei: '36協定',

  /**
   * @description [ja] 午前半日休
   */
  Admin_Lbl_AMHalfDayLeave: '午前半日休',

  /**
   * @description [ja] 欠勤申請
   */
  Admin_Lbl_AbsenceRequest: '欠勤申請',

  /**
   * @description [ja] アクセス権限
   */
  Admin_Lbl_AccessPermission: 'アクセス権限',

  /**
   * @description [ja] アクセス権限
   */
  Admin_Lbl_AccessPermissionManagement: 'アクセス権限',

  /**
   * @description [ja] 有効
   */
  Admin_Lbl_Active: '有効',

  /**
   * @description [ja] 実行者
   */
  Admin_Lbl_Actor: '実行者',

  /**
   * @description [ja] 費目を追加
   */
  Admin_Lbl_AddExpenseType: '費目を追加',

  /**
   * @description [ja] 管理者
   */
  Admin_Lbl_Administrator: '管理者',

  /**
   * @description [ja] 認める
   */
  Admin_Lbl_Admit: '認める',

  /**
   * @description [ja] 残業警告設定
   */
  Admin_Lbl_AgreementAlertSetting: '残業警告設定',

  /**
   * @description [ja] 有効開始日
   */
  Admin_Lbl_AgreementAlertSettingValidDateFrom: '有効開始日',

  /**
   * @description [ja] 失効日
   */
  Admin_Lbl_AgreementAlertSettingValidDateTo: '失効日',

  /**
   * @description [ja] 申請者本人による承認者の選択を認める
   */
  Admin_Lbl_AllowRequesterToChangeApprover: '申請者本人による承認者の選択を認める',

  /**
   * @description [ja] 税額変更を許可する
   */
  Admin_Lbl_AllowTaxAmountChange: '税額変更を許可する',

  /**
   * @description [ja] 承認者選択
   */
  Admin_Lbl_AllowToChangeApprover: '承認者選択',

  /**
   * @description [ja] 半日休暇時間帯の勤務を認める
   */
  Admin_Lbl_AllowToWorkOnHalfDayLeaveTime: '半日休暇時間帯の勤務を認める',

  /**
   * @description [ja] 短時間勤務を許可する時間
   */
  Admin_Lbl_AllowableTimeOfShortTimeWork: '短時間勤務を許可する時間',

  /**
   * @description [ja] 短時間勤務を許可する時間（所定労働時間対象）
   */
  Admin_Lbl_AllowableTimeOfShortTimeWorkForFlex: '短時間勤務を許可する時間（所定労働時間対象）',

  /**
   * @description [ja] 短時間勤務を許可する時間（コアタイム対象）
   */
  Admin_Lbl_AllowableTimeOfShortTimeWorkInCoreTime: '短時間勤務を許可する時間（コアタイム対象）',

  /**
   * @description [ja] 金額
   */
  Admin_Lbl_Amount: '金額',

  /**
   * @description [ja] 金額リスト
   */
  Admin_Lbl_AmountList: '金額リスト',

  /**
   * @description [ja] 付与された年次有給休暇
   */
  Admin_Lbl_AnnualPaidLeaveGrantHistoryOfValid: '付与された年次有給休暇',

  /**
   * @description [ja] 申請オプション
   */
  Admin_Lbl_AppOptions: '申請オプション',

  /**
   * @description [ja] 適用
   */
  Admin_Lbl_Apply: '適用',

  /**
   * @description [ja] 承認権限フラグ01
   */
  Admin_Lbl_ApprovalAuthority01: '承認権限フラグ01',

  /**
   * @description [ja] 承認プロセス
   */
  Admin_Lbl_ApprovalProcess: '承認プロセス',

  /**
   * @description [ja] 承認設定
   */
  Admin_Lbl_ApprovalSetting: '承認設定',

  /**
   * @description [ja] 承認者01
   */
  Admin_Lbl_Approver01Name: '承認者01',

  /**
   * @description [ja] 勤務パターン
   */
  Admin_Lbl_AttPattern: '勤務パターン',

  /**
   * @description [ja] 勤務時間変更申請
   */
  Admin_Lbl_AttPatternApplyRequest: '勤務時間変更申請',

  /**
   * @description [ja] 退勤打刻境界時刻
   */
  Admin_Lbl_AttPatternBoundaryEndTimeOfDay: '退勤打刻境界時刻',

  /**
   * @description [ja] 出勤打刻境界時刻
   */
  Admin_Lbl_AttPatternBoundaryStartTimeOfDay: '出勤打刻境界時刻',

  /**
   * @description [ja] 適用可能な勤務パターン
   */
  Admin_Lbl_AttPatternCodeList: '適用可能な勤務パターン',

  /**
   * @description [ja] 勤務パターン適用
   */
  Admin_Lbl_AttPatternEmployeeBatch: '勤務パターン適用',

  /**
   * @description [ja] 勤務パターンCSVファイル
   */
  Admin_Lbl_AttPatternEmployeeBatchCsvFile: '勤務パターンCSVファイル',

  /**
   * @description [ja] CSVファイルをドラッグ＆ドロップ、もしくは
   */
  Admin_Lbl_AttPatternEmployeeBatchDragAndDrop: 'CSVファイルをドラッグ＆ドロップ、もしくは',

  /**
   * @description [ja] 日タイプ
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderDayType: '日タイプ',

  /**
   * @description [ja] 社員コード
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderEmployeeCode: '社員コード',

  /**
   * @description [ja] エラー詳細
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderErrorDetail: 'エラー詳細',

  /**
   * @description [ja] 勤務パターンコード
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderPatternCode: '勤務パターンコード',

  /**
   * @description [ja] ステータス
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderStatus: 'ステータス',

  /**
   * @description [ja] 日付
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderTargetDate: '日付',

  /**
   * @description [ja] 勤怠時間系オプション
   */
  Admin_Lbl_AttPatternOptions: '勤怠時間系オプション',

  /**
   * @description [ja] 勤怠管理
   */
  Admin_Lbl_AttendanceManagement: '勤怠管理',

  /**
   * @description [ja] 基本情報
   */
  Admin_Lbl_Base: '基本情報',

  /**
   * @description [ja] 基本情報
   */
  Admin_Lbl_BaseInfo: '基本情報',

  /**
   * @description [ja] 開始日に合わせる
   */
  Admin_Lbl_BeginBaseDay: '開始日に合わせる',

  /**
   * @description [ja] 開始月に合わせる
   */
  Admin_Lbl_BeginBaseMonth: '開始月に合わせる',

  /**
   * @description [ja] 両方の祝日
   */
  Admin_Lbl_BothHoliday: '両方の祝日',

  /**
   * @description [ja] カレンダー
   */
  Admin_Lbl_Calendar: 'カレンダー',

  /**
   * @description [ja] 外部カレンダー連携
   */
  Admin_Lbl_CalendarAccess: '外部カレンダー連携',

  /**
   * @description [ja] 認証
   */
  Admin_Lbl_CalendarAccessAuthButton: '認証',

  /**
   * @description [ja] 再認証
   */
  Admin_Lbl_CalendarAccessAuthButtonReauthorize: '再認証',

  /**
   * @description [ja] 設定を確認
   */
  Admin_Lbl_CalendarAccessAuthOpenRemoteSiteSetting: '設定を確認',

  /**
   * @description [ja] 外部カレンダーへの接続に失敗しました。
   */
  Admin_Lbl_CalendarAccessAuthStatusApiConnectionFailed: '外部カレンダーへの接続に失敗しました。',

  /**
   * @description [ja] 認証されています。
   */
  Admin_Lbl_CalendarAccessAuthStatusAuthorized: '認証されています。',

  /**
   * @description [ja] リモートサイト設定が有効化されていません。
   */
  Admin_Lbl_CalendarAccessAuthStatusRemoteSiteInactive: 'リモートサイト設定が有効化されていません。',

  /**
   * @description [ja] 認証されていません。
   */
  Admin_Lbl_CalendarAccessAuthStatusUnauthorized: '認証されていません。',

  /**
   * @description [ja] （保存後、認証ボタンが表示されます）
   */
  Admin_Lbl_CalendarAccessAuthStatusUnsaved: '（保存後、認証ボタンが表示されます）',

  /**
   * @description [ja] 認証
   */
  Admin_Lbl_CalendarAccessAuthorization: '認証',

  /**
   * @description [ja] 有効にする
   */
  Admin_Lbl_CalendarAccessEnable: '有効にする',

  /**
   * @description [ja] 機能
   */
  Admin_Lbl_CalendarAccessFunction: '機能',

  /**
   * @description [ja] 候補検索
   */
  Admin_Lbl_CandidateSearch: '候補検索',

  /**
   * @description [ja] 期間変更
   */
  Admin_Lbl_ChangePeriod: '期間変更',

  /**
   * @description [ja] コード
   */
  Admin_Lbl_Code: 'コード',

  /**
   * @description [ja] コストセンター必須
   */
  Admin_Lbl_ComCostCenterRequiredFor: 'コストセンター必須',

  /**
   * @description [ja] コストセンター使用
   */
  Admin_Lbl_ComCostCenterUsedIn: 'コストセンター使用',

  /**
   * @description [ja] ジョブ必須
   */
  Admin_Lbl_ComJobRequiredFor: 'ジョブ必須',

  /**
   * @description [ja] ジョブ使用
   */
  Admin_Lbl_ComJobUsedIn: 'ジョブ使用',

  /**
   * @description [ja] コメント
   */
  Admin_Lbl_Comment: 'コメント',

  /**
   * @description [ja] 共通マスタ
   */
  Admin_Lbl_CommonMaster: '共通マスタ',

  /**
   * @description [ja] 共通設定
   */
  Admin_Lbl_CommonSettings: '共通設定',

  /**
   * @description [ja] この区間は定期に対応していません
   */
  Admin_Lbl_CommuterNoSearchResult: 'この区間は定期に対応していません',

  /**
   * @description [ja] 定期区間設定
   */
  Admin_Lbl_CommuterPassSetting: '定期区間設定',

  /**
   * @description [ja] 削除
   */
  Admin_Lbl_CommuterResetButton: '削除',

  /**
   * @description [ja] 会社
   */
  Admin_Lbl_Company: '会社',

  /**
   * @description [ja] 全社共通カレンダー
   */
  Admin_Lbl_CompanyCalendar: '全社共通カレンダー',

  /**
   * @description [ja] 全社共通
   */
  Admin_Lbl_CompanyCalendar_Abbr: '全社共通',

  /**
   * @description [ja] 会社コード
   */
  Admin_Lbl_CompanyCode: '会社コード',

  /**
   * @description [ja] 会社名
   */
  Admin_Lbl_CompanyName: '会社名',

  /**
   * @description [ja] 一般設定
   */
  Admin_Lbl_Company_General: '一般設定',

  /**
   * @description [ja] コアタイム
   */
  Admin_Lbl_CoreTime: 'コアタイム',

  /**
   * @description [ja] コストセンター
   */
  Admin_Lbl_CostCenter: 'コストセンター',

  /**
   * @description [ja] 外部システム連携コード
   */
  Admin_Lbl_CostCenterLinkageCode: '外部システム連携コード',

  /**
   * @description [ja] コストセンター名
   */
  Admin_Lbl_CostCenterName: 'コストセンター名',

  /**
   * @description [ja] 全件数
   */
  Admin_Lbl_Count: '全件数',

  /**
   * @description [ja] エラー件数
   */
  Admin_Lbl_CountAppliedErrorNumber: 'エラー件数',

  /**
   * @description [ja] 全件数
   */
  Admin_Lbl_CountAppliedNumber: '全件数',

  /**
   * @description [ja] 成功件数
   */
  Admin_Lbl_CountAppliedSuccessNumber: '成功件数',

  /**
   * @description [ja] ファイル件数
   */
  Admin_Lbl_CountFileNmber: 'ファイル件数',

  /**
   * @description [ja] 国
   */
  Admin_Lbl_Country: '国',

  /**
   * @description [ja] アップロードされたCSVファイルに行が含まれていません。
   */
  Admin_Lbl_CsvEmptyError: 'アップロードされたCSVファイルに行が含まれていません。',

  /**
   * @description [ja] １行以上の勤務パターンを追加してください。
   */
  Admin_Lbl_CsvEmptyErrorSolution: '１行以上の勤務パターンを追加してください。',

  /**
   * @description [ja] アップロードされたCSVファイルの列の数が間違っています。
   */
  Admin_Lbl_CsvFormatError: 'アップロードされたCSVファイルの列の数が間違っています。',

  /**
   * @description [ja] 修正して再アップロードしてください。
   */
  Admin_Lbl_CsvFormatErrorSolution: '修正して再アップロードしてください。',

  /**
   * @description [ja] アップロードされたCSVファイルの行数が上限を超えました。
   */
  Admin_Lbl_CsvRowsLimitExceeded: 'アップロードされたCSVファイルの行数が上限を超えました。',

  /**
   * @description [ja] 件数を5000件以下にしてアップロードして下さい。
   */
  Admin_Lbl_CsvRowsLimitExceededSolution: '件数を5000件以下にしてアップロードして下さい。',

  /**
   * @description [ja] 通貨
   */
  Admin_Lbl_Currency: '通貨',

  /**
   * @description [ja] 通貨コード
   */
  Admin_Lbl_CurrencyCode: '通貨コード',

  /**
   * @description [ja] 通貨名
   */
  Admin_Lbl_CurrencyName: '通貨名',

  /**
   * @description [ja] 通貨ペア
   */
  Admin_Lbl_CurrencyPair: '通貨ペア',

  /**
   * @description [ja] 通貨記号
   */
  Admin_Lbl_CurrencySymbol: '通貨記号',

  /**
   * @description [ja] カスタム拡張項目
   */
  Admin_Lbl_CustomExtendedItem: 'カスタム拡張項目',

  /**
   * @description [ja] 日種別
   */
  Admin_Lbl_DayType: '日種別',

  /**
   * @description [ja] 付与日数
   */
  Admin_Lbl_DaysGranted: '付与日数',

  /**
   * @description [ja] 日数管理
   */
  Admin_Lbl_DaysManaged: '日数管理',

  /**
   * @description [ja] 借方勘定科目コード
   */
  Admin_Lbl_DebitAccountCode: '借方勘定科目コード',

  /**
   * @description [ja] 借方勘定科目名
   */
  Admin_Lbl_DebitAccountName: '借方勘定科目名',

  /**
   * @description [ja] 借方補助科目コード
   */
  Admin_Lbl_DebitSubAccountCode: '借方補助科目コード',

  /**
   * @description [ja] 借方補助科目名
   */
  Admin_Lbl_DebitSubAccountName: '借方補助科目名',

  /**
   * @description [ja] 小数点以下の桁数
   */
  Admin_Lbl_DecimalPlaces: '小数点以下の桁数',

  /**
   * @description [ja] うち残業時間
   */
  Admin_Lbl_DeemedOvertimeHours: 'うち残業時間',

  /**
   * @description [ja] みなし労働時間
   */
  Admin_Lbl_DeemedWorkHours: 'みなし労働時間',

  /**
   * @description [ja] デフォルト
   */
  Admin_Lbl_Default: 'デフォルト',

  /**
   * @description [ja] デフォルト言語
   */
  Admin_Lbl_DefaultLanguage: 'デフォルト言語',

  /**
   * @description [ja] 初期値
   */
  Admin_Lbl_DefaultValue: '初期値',

  /**
   * @description [ja] 管理者
   */
  Admin_Lbl_DeparmentManagerName: '管理者',

  /**
   * @description [ja] 部署
   */
  Admin_Lbl_Department: '部署',

  /**
   * @description [ja] 部署コード
   */
  Admin_Lbl_DepartmentCode: '部署コード',

  /**
   * @description [ja] 部署名
   */
  Admin_Lbl_DepartmentName: '部署名',

  /**
   * @description [ja] 説明
   */
  Admin_Lbl_Description: '説明',

  /**
   * @description [ja] {$Att_Lbl_RequestTypeDirect}申請
   */
  Admin_Lbl_DirectApplyRequest: '{$Att_Lbl_RequestTypeDirect}申請',

  /**
   * @description [ja] 休憩１(デフォルト)
   */
  Admin_Lbl_DirectApplyRestTime1: '休憩１(デフォルト)',

  /**
   * @description [ja] 休憩２(デフォルト)
   */
  Admin_Lbl_DirectApplyRestTime2: '休憩２(デフォルト)',

  /**
   * @description [ja] 休憩３(デフォルト)
   */
  Admin_Lbl_DirectApplyRestTime3: '休憩３(デフォルト)',

  /**
   * @description [ja] 休憩４(デフォルト)
   */
  Admin_Lbl_DirectApplyRestTime4: '休憩４(デフォルト)',

  /**
   * @description [ja] 休憩５(デフォルト)
   */
  Admin_Lbl_DirectApplyRestTime5: '休憩５(デフォルト)',

  /**
   * @description [ja] 出退勤時刻(デフォルト)
   */
  Admin_Lbl_DirectApplyTime: '出退勤時刻(デフォルト)',

  /**
   * @description [ja] 直課
   */
  Admin_Lbl_DirectCharged: '直課',

  /**
   * @description [ja] ダウンロード
   */
  Admin_Lbl_Download: 'ダウンロード',

  /**
   * @description [ja] 早朝勤務申請
   */
  Admin_Lbl_EarlyStartWorkRequest: '早朝勤務申請',

  /**
   * @description [ja] 編集
   */
  Admin_Lbl_Edit: '編集',

  /**
   * @description [ja] 編集中
   */
  Admin_Lbl_Editing: '編集中',

  /**
   * @description [ja] 社員
   */
  Admin_Lbl_Employee: '社員',

  /**
   * @description [ja] 担当社員割り当て
   */
  Admin_Lbl_EmployeeAssignment: '担当社員割り当て',

  /**
   * @description [ja] グレード
   */
  Admin_Lbl_EmployeeGrade: 'グレード',

  /**
   * @description [ja] 社員グループ
   */
  Admin_Lbl_EmployeeGroup: '社員グループ',

  /**
   * @description [ja] 終了
   */
  Admin_Lbl_End: '終了',

  /**
   * @description [ja] 終了日に合わせる
   */
  Admin_Lbl_EndBaseDay: '終了日に合わせる',

  /**
   * @description [ja] 終了月に合わせる
   */
  Admin_Lbl_EndBaseMonth: '終了月に合わせる',

  /**
   * @description [ja] 終了日
   */
  Admin_Lbl_EndDate: '終了日',

  /**
   * @description [ja] 申請なしでも認める残業の終了時刻
   */
  Admin_Lbl_EndTimeOfAcceptAsWorkingHour: '申請なしでも認める残業の終了時刻',

  /**
   * @description [ja] 入社日
   */
  Admin_Lbl_EnteringDate: '入社日',

  /**
   * @description [ja] イベント
   */
  Admin_Lbl_Event: 'イベント',

  /**
   * @description [ja] 実行日時
   */
  Admin_Lbl_ExecutedAt: '実行日時',

  /**
   * @description [ja] コストセンター必須
   */
  Admin_Lbl_ExpCostCenterRequiredFor: 'コストセンター必須',

  /**
   * @description [ja] コストセンター使用
   */
  Admin_Lbl_ExpCostCenterUsedIn: 'コストセンター使用',

  /**
   * @description [ja] カスタムヒント
   */
  Admin_Lbl_ExpCustomHint: 'カスタムヒント',

  /**
   * @description [ja] 経費明細
   */
  Admin_Lbl_ExpRecord: '経費明細',

  /**
   * @description [ja] 経費申請ヘッダー
   */
  Admin_Lbl_ExpReportHeader: '経費申請ヘッダー',

  /**
   * @description [ja] 設定
   */
  Admin_Lbl_ExpSetting: '設定',

  /**
   * @description [ja] 税区分
   */
  Admin_Lbl_ExpTaxType: '税区分',

  /**
   * @description [ja] 税区分1
   */
  Admin_Lbl_ExpTaxType1: '税区分1',

  /**
   * @description [ja] 税区分2
   */
  Admin_Lbl_ExpTaxType2: '税区分2',

  /**
   * @description [ja] 税区分3
   */
  Admin_Lbl_ExpTaxType3: '税区分3',

  /**
   * @description [ja] 費目
   */
  Admin_Lbl_ExpType: '費目',

  /**
   * @description [ja] 費目グループ
   */
  Admin_Lbl_ExpTypeGroup: '費目グループ',

  /**
   * @description [ja] 支払先必須
   */
  Admin_Lbl_ExpVendorRequiredFor: '支払先必須',

  /**
   * @description [ja] 支払先使用
   */
  Admin_Lbl_ExpVendorUsedIn: '支払先使用',

  /**
   * @description [ja] 設定されている費目はありません
   */
  Admin_Lbl_ExpenseNoItemInTheSet: '設定されている費目はありません',

  /**
   * @description [ja] 経費精算のみ
   */
  Admin_Lbl_ExpenseReporextendedItemUsedInOptionst: '経費精算のみ',

  /**
   * @description [ja] 経費精算のみ
   */
  Admin_Lbl_ExpenseReport: '経費精算のみ',

  /**
   * @description [ja] 経費精算
   */
  Admin_Lbl_ExpenseRequest: '経費精算',

  /**
   * @description [ja] 事前申請と経費精算
   */
  Admin_Lbl_ExpenseRequestAndExpenseReport: '事前申請と経費精算',

  /**
   * @description [ja] 費目を検索
   */
  Admin_Lbl_ExpenseSearch: '費目を検索',

  /**
   * @description [ja] コード
   */
  Admin_Lbl_ExpenseTypeCode: 'コード',

  /**
   * @description [ja] 設定されている費目
   */
  Admin_Lbl_ExpenseTypeConfig: '設定されている費目',

  /**
   * @description [ja] グループコード
   */
  Admin_Lbl_ExpenseTypeGroupCode: 'グループコード',

  /**
   * @description [ja] グループ名
   */
  Admin_Lbl_ExpenseTypeGroupName: 'グループ名',

  /**
   * @description [ja] 名前
   */
  Admin_Lbl_ExpenseTypeName: '名前',

  /**
   * @description [ja] 拡張項目
   */
  Admin_Lbl_ExtendedItem: '拡張項目',

  /**
   * @description [ja] 拡張項目日付
   */
  Admin_Lbl_ExtendedItemDate: '拡張項目日付',

  /**
   * @description [ja] 拡張項目日付01
   */
  Admin_Lbl_ExtendedItemDate01: '拡張項目日付01',

  /**
   * @description [ja] 拡張項目日付02
   */
  Admin_Lbl_ExtendedItemDate02: '拡張項目日付02',

  /**
   * @description [ja] 拡張項目日付03
   */
  Admin_Lbl_ExtendedItemDate03: '拡張項目日付03',

  /**
   * @description [ja] 拡張項目日付04
   */
  Admin_Lbl_ExtendedItemDate04: '拡張項目日付04',

  /**
   * @description [ja] 拡張項目日付05
   */
  Admin_Lbl_ExtendedItemDate05: '拡張項目日付05',

  /**
   * @description [ja] 拡張項目日付06
   */
  Admin_Lbl_ExtendedItemDate06: '拡張項目日付06',

  /**
   * @description [ja] 拡張項目日付07
   */
  Admin_Lbl_ExtendedItemDate07: '拡張項目日付07',

  /**
   * @description [ja] 拡張項目日付08
   */
  Admin_Lbl_ExtendedItemDate08: '拡張項目日付08',

  /**
   * @description [ja] 拡張項目日付09
   */
  Admin_Lbl_ExtendedItemDate09: '拡張項目日付09',

  /**
   * @description [ja] 拡張項目日付10
   */
  Admin_Lbl_ExtendedItemDate10: '拡張項目日付10',

  /**
   * @description [ja] 拡張項目参照
   */
  Admin_Lbl_ExtendedItemLookup: '拡張項目参照',

  /**
   * @description [ja] 拡張項目参照01
   */
  Admin_Lbl_ExtendedItemLookup01: '拡張項目参照01',

  /**
   * @description [ja] 拡張項目参照02
   */
  Admin_Lbl_ExtendedItemLookup02: '拡張項目参照02',

  /**
   * @description [ja] 拡張項目参照03
   */
  Admin_Lbl_ExtendedItemLookup03: '拡張項目参照03',

  /**
   * @description [ja] 拡張項目参照04
   */
  Admin_Lbl_ExtendedItemLookup04: '拡張項目参照04',

  /**
   * @description [ja] 拡張項目参照05
   */
  Admin_Lbl_ExtendedItemLookup05: '拡張項目参照05',

  /**
   * @description [ja] 拡張項目参照06
   */
  Admin_Lbl_ExtendedItemLookup06: '拡張項目参照06',

  /**
   * @description [ja] 拡張項目参照07
   */
  Admin_Lbl_ExtendedItemLookup07: '拡張項目参照07',

  /**
   * @description [ja] 拡張項目参照08
   */
  Admin_Lbl_ExtendedItemLookup08: '拡張項目参照08',

  /**
   * @description [ja] 拡張項目参照09
   */
  Admin_Lbl_ExtendedItemLookup09: '拡張項目参照09',

  /**
   * @description [ja] 拡張項目参照10
   */
  Admin_Lbl_ExtendedItemLookup10: '拡張項目参照10',

  /**
   * @description [ja] 拡張項目選択リスト
   */
  Admin_Lbl_ExtendedItemPicklist: '拡張項目選択リスト',

  /**
   * @description [ja] 拡張項目選択リスト01
   */
  Admin_Lbl_ExtendedItemPicklist01: '拡張項目選択リスト01',

  /**
   * @description [ja] 拡張項目選択リスト02
   */
  Admin_Lbl_ExtendedItemPicklist02: '拡張項目選択リスト02',

  /**
   * @description [ja] 拡張項目選択リスト03
   */
  Admin_Lbl_ExtendedItemPicklist03: '拡張項目選択リスト03',

  /**
   * @description [ja] 拡張項目選択リスト04
   */
  Admin_Lbl_ExtendedItemPicklist04: '拡張項目選択リスト04',

  /**
   * @description [ja] 拡張項目選択リスト05
   */
  Admin_Lbl_ExtendedItemPicklist05: '拡張項目選択リスト05',

  /**
   * @description [ja] 拡張項目選択リスト06
   */
  Admin_Lbl_ExtendedItemPicklist06: '拡張項目選択リスト06',

  /**
   * @description [ja] 拡張項目選択リスト07
   */
  Admin_Lbl_ExtendedItemPicklist07: '拡張項目選択リスト07',

  /**
   * @description [ja] 拡張項目選択リスト08
   */
  Admin_Lbl_ExtendedItemPicklist08: '拡張項目選択リスト08',

  /**
   * @description [ja] 拡張項目選択リスト09
   */
  Admin_Lbl_ExtendedItemPicklist09: '拡張項目選択リスト09',

  /**
   * @description [ja] 拡張項目選択リスト10
   */
  Admin_Lbl_ExtendedItemPicklist10: '拡張項目選択リスト10',

  /**
   * @description [ja] 必須
   */
  Admin_Lbl_ExtendedItemRequiredFor: '必須',

  /**
   * @description [ja] 拡張項目テキスト
   */
  Admin_Lbl_ExtendedItemText: '拡張項目テキスト',

  /**
   * @description [ja] 拡張項目テキスト01
   */
  Admin_Lbl_ExtendedItemText01: '拡張項目テキスト01',

  /**
   * @description [ja] 拡張項目テキスト02
   */
  Admin_Lbl_ExtendedItemText02: '拡張項目テキスト02',

  /**
   * @description [ja] 拡張項目テキスト03
   */
  Admin_Lbl_ExtendedItemText03: '拡張項目テキスト03',

  /**
   * @description [ja] 拡張項目テキスト04
   */
  Admin_Lbl_ExtendedItemText04: '拡張項目テキスト04',

  /**
   * @description [ja] 拡張項目テキスト05
   */
  Admin_Lbl_ExtendedItemText05: '拡張項目テキスト05',

  /**
   * @description [ja] 拡張項目テキスト06
   */
  Admin_Lbl_ExtendedItemText06: '拡張項目テキスト06',

  /**
   * @description [ja] 拡張項目テキスト07
   */
  Admin_Lbl_ExtendedItemText07: '拡張項目テキスト07',

  /**
   * @description [ja] 拡張項目テキスト08
   */
  Admin_Lbl_ExtendedItemText08: '拡張項目テキスト08',

  /**
   * @description [ja] 拡張項目テキスト09
   */
  Admin_Lbl_ExtendedItemText09: '拡張項目テキスト09',

  /**
   * @description [ja] 拡張項目テキスト10
   */
  Admin_Lbl_ExtendedItemText10: '拡張項目テキスト10',

  /**
   * @description [ja] 使用
   */
  Admin_Lbl_ExtendedItemUsedIn: '使用',

  /**
   * @description [ja] 拡張項目
   */
  Admin_Lbl_ExtraField: '拡張項目',

  /**
   * @description [ja] エラー
   */
  Admin_Lbl_FailureCount: 'エラー',

  /**
   * @description [ja] 名
   */
  Admin_Lbl_FirstName: '名',

  /**
   * @description [ja] フレックス時間帯
   */
  Admin_Lbl_FlexHours: 'フレックス時間帯',

  /**
   * @description [ja] 機能設定
   */
  Admin_Lbl_FunctionSettings: '機能設定',

  /**
   * @description [ja] グレード
   */
  Admin_Lbl_Grade: 'グレード',

  /**
   * @description [ja] 社員グループ
   */
  Admin_Lbl_Group: '社員グループ',

  /**
   * @description [ja] 半日休
   */
  Admin_Lbl_HalfDayLeave: '半日休',

  /**
   * @description [ja] 半日休の休暇時間
   */
  Admin_Lbl_HalfDayLeaveHours: '半日休の休暇時間',

  /**
   * @description [ja] 履歴
   */
  Admin_Lbl_History: '履歴',

  /**
   * @description [ja] 履歴管理
   */
  Admin_Lbl_HistoryProperties: '履歴管理',

  /**
   * @description [ja] 休日出勤申請
   */
  Admin_Lbl_HolidayWorkRequest: '休日出勤申請',

  /**
   * @description [ja] [%1]を超える勤務の場合、休憩時間を[%2]分以上取ること
   */
  Admin_Lbl_IfWorkingForXThenLegalRestTimeIsX: '[%1]を超える勤務の場合、休憩時間を[%2]分以上取ること',

  /**
   * @description [ja] 含める
   */
  Admin_Lbl_Include: '含める',

  /**
   * @description [ja] 所定休日の労働時間をみなし労働時間に含める
   */
  Admin_Lbl_IncludeHolidayWorkInDeemedTime: '所定休日の労働時間をみなし労働時間に含める',

  /**
   * @description [ja] 所定休日の労働時間をフレックス対象労働時間に含める
   */
  Admin_Lbl_IncludeHolidayWorkInPlainTime: '所定休日の労働時間をフレックス対象労働時間に含める',

  /**
   * @description [ja] 項目種類
   */
  Admin_Lbl_InputType: '項目種類',

  /**
   * @description [ja] 経費で使用する
   */
  Admin_Lbl_IsSelectableExpense: '経費で使用する',

  /**
   * @description [ja] 工数で使用する
   */
  Admin_Lbl_IsSelectableTimeTrack: '工数で使用する',

  /**
   * @description [ja] 裁量労働制
   */
  Admin_Lbl_JP_Discretion: '裁量労働制',

  /**
   * @description [ja] 固定労働時間制
   */
  Admin_Lbl_JP_Fix: '固定労働時間制',

  /**
   * @description [ja] フレックスタイム制
   */
  Admin_Lbl_JP_Flex: 'フレックスタイム制',

  /**
   * @description [ja] 管理監督者
   */
  Admin_Lbl_JP_Manager: '管理監督者',

  /**
   * @description [ja] 変形労働時間制
   */
  Admin_Lbl_JP_Modified: '変形労働時間制',

  /**
   * @description [ja] ジョブ
   */
  Admin_Lbl_Job: 'ジョブ',

  /**
   * @description [ja] ジョブアサイン
   */
  Admin_Lbl_JobAssignment: 'ジョブアサイン',

  /**
   * @description [ja] ジョブ入力タイプ
   */
  Admin_Lbl_JobInputType: 'ジョブ入力タイプ',

  /**
   * @description [ja] ジョブ名
   */
  Admin_Lbl_JobName: 'ジョブ名',

  /**
   * @description [ja] ジョブオーナー
   */
  Admin_Lbl_JobOwnerName: 'ジョブオーナー',

  /**
   * @description [ja] ジョブ必須
   */
  Admin_Lbl_JobRequiredFor: 'ジョブ必須',

  /**
   * @description [ja] ジョブタイプ
   */
  Admin_Lbl_JobType: 'ジョブタイプ',

  /**
   * @description [ja] ジョブ使用
   */
  Admin_Lbl_JobUsedIn: 'ジョブ使用',

  /**
   * @description [ja] 検索優先エリア
   */
  Admin_Lbl_JorudanAreaPreference: '検索優先エリア',

  /**
   * @description [ja] 有料特急の規定距離(km)
   */
  Admin_Lbl_JorudanChargedExpressDistance: '有料特急の規定距離(km)',

  /**
   * @description [ja] 定期区間の取り扱い
   */
  Admin_Lbl_JorudanCommuterPass: '定期区間の取り扱い',

  /**
   * @description [ja] 運賃
   */
  Admin_Lbl_JorudanFareType: '運賃',

  /**
   * @description [ja] 高速バス（デフォルト設定）
   */
  Admin_Lbl_JorudanHighwayBus: '高速バス（デフォルト設定）',

  /**
   * @description [ja] 検索結果の表示順（デフォルト設定）
   */
  Admin_Lbl_JorudanRouteSort: '検索結果の表示順（デフォルト設定）',

  /**
   * @description [ja] ジョルダン路線検索オプション
   */
  Admin_Lbl_JorudanSearchOption: 'ジョルダン路線検索オプション',

  /**
   * @description [ja] 優先座席（デフォルト設定）
   */
  Admin_Lbl_JorudanSeatPreference: '優先座席（デフォルト設定）',

  /**
   * @description [ja] 有料特急の利用
   */
  Admin_Lbl_JorudanUseChargedExpress: '有料特急の利用',

  /**
   * @description [ja] 名前
   */
  Admin_Lbl_Label: '名前',

  /**
   * @description [ja] 言語
   */
  Admin_Lbl_Language: '言語',

  /**
   * @description [ja] 主言語
   */
  Admin_Lbl_Language0: '主言語',

  /**
   * @description [ja] 副言語1
   */
  Admin_Lbl_Language1: '副言語1',

  /**
   * @description [ja] 副言語2
   */
  Admin_Lbl_Language2: '副言語2',

  /**
   * @description [ja] 姓
   */
  Admin_Lbl_LastName: '姓',

  /**
   * @description [ja] 休暇
   */
  Admin_Lbl_Leave: '休暇',

  /**
   * @description [ja] 取得可能な休暇
   */
  Admin_Lbl_LeaveCodeList: '取得可能な休暇',

  /**
   * @description [ja] 付与日数
   */
  Admin_Lbl_LeaveDaysGranted: '付与日数',

  /**
   * @description [ja] 残日数
   */
  Admin_Lbl_LeaveDaysLeft: '残日数',

  /**
   * @description [ja] 付与日
   */
  Admin_Lbl_LeaveGrantValidDateFrom: '付与日',

  /**
   * @description [ja] 失効日
   */
  Admin_Lbl_LeaveGrantValidDateTo: '失効日',

  /**
   * @description [ja] 休暇管理
   */
  Admin_Lbl_LeaveManagement: '休暇管理',

  /**
   * @description [ja] 休職・休業
   */
  Admin_Lbl_LeaveOfAbsence: '休職・休業',

  /**
   * @description [ja] 休職・休業適用
   */
  Admin_Lbl_LeaveOfAbsencePeriodStatus: '休職・休業適用',

  /**
   * @description [ja] 法律上の祝日のみ
   */
  Admin_Lbl_LegalHolidayOnly: '法律上の祝日のみ',

  /**
   * @description [ja] 休憩時間チェック1
   */
  Admin_Lbl_LegalRestTimeCheck1: '休憩時間チェック1',

  /**
   * @description [ja] 休憩時間チェック2
   */
  Admin_Lbl_LegalRestTimeCheck2: '休憩時間チェック2',

  /**
   * @description [ja] 最大文字数
   */
  Admin_Lbl_LimitLength: '最大文字数',

  /**
   * @description [ja] ログ
   */
  Admin_Lbl_Log: 'ログ',

  /**
   * @description [ja] 付与された日数管理休暇
   */
  Admin_Lbl_ManagedLeaveGrantHistoryOfValid: '付与された日数管理休暇',

  /**
   * @description [ja] 管理画面
   */
  Admin_Lbl_ManagementScreen: '管理画面',

  /**
   * @description [ja] 上長
   */
  Admin_Lbl_ManagerName: '上長',

  /**
   * @description [ja] マスタ
   */
  Admin_Lbl_Master: 'マスタ',

  /**
   * @description [ja] マスタ通貨設定
   */
  Admin_Lbl_MasterCurrencySetting: 'マスタ通貨設定',

  /**
   * @description [ja] 税区分設定
   */
  Admin_Lbl_MasterTaxTypeSetting: '税区分設定',

  /**
   * @description [ja] 中間名
   */
  Admin_Lbl_MiddleName: '中間名',

  /**
   * @description [ja] モバイル
   */
  Admin_Lbl_Mobile: 'モバイル',

  /**
   * @description [ja] 月度表記
   */
  Admin_Lbl_MonthMark: '月度表記',

  /**
   * @description [ja] 延長 1ヶ月限度
   */
  Admin_Lbl_MonthlyAgreementHourLimit: '延長 1ヶ月限度',

  /**
   * @description [ja] 特別条項 1ヶ月限度
   */
  Admin_Lbl_MonthlyAgreementHourLimitSpecial: '特別条項 1ヶ月限度',

  /**
   * @description [ja] 延長 1ヶ月警告1
   */
  Admin_Lbl_MonthlyAgreementHourWarning1: '延長 1ヶ月警告1',

  /**
   * @description [ja] 延長 1ヶ月警告2
   */
  Admin_Lbl_MonthlyAgreementHourWarning2: '延長 1ヶ月警告2',

  /**
   * @description [ja] 特別条項 1ヶ月警告1
   */
  Admin_Lbl_MonthlyAgreementHourWarningSpecial1: '特別条項 1ヶ月警告1',

  /**
   * @description [ja] 特別条項 1ヶ月警告2
   */
  Admin_Lbl_MonthlyAgreementHourWarningSpecial2: '特別条項 1ヶ月警告2',

  /**
   * @description [ja] 名前
   */
  Admin_Lbl_Name: '名前',

  /**
   * @description [ja] 必ず理由を求める
   */
  Admin_Lbl_NeedRequireReason: '必ず理由を求める',

  /**
   * @description [ja] 番号体系
   */
  Admin_Lbl_NumberScheme: '番号体系',

  /**
   * @description [ja] 表示順
   */
  Admin_Lbl_Order: '表示順',

  /**
   * @description [ja] 全体設定
   */
  Admin_Lbl_Organization: '全体設定',

  /**
   * @description [ja] 36協定/時間外労働
   */
  Admin_Lbl_OvertimeWorkHoursAndSpecialProvisions: '36協定/時間外労働',

  /**
   * @description [ja] 残業申請
   */
  Admin_Lbl_OvertimeWorkRequest: '残業申請',

  /**
   * @description [ja] 午後半日休
   */
  Admin_Lbl_PMHalfDayLeave: '午後半日休',

  /**
   * @description [ja] 親コストセンター
   */
  Admin_Lbl_ParentCostCenterName: '親コストセンター',

  /**
   * @description [ja] 親部署
   */
  Admin_Lbl_ParentDepartName: '親部署',

  /**
   * @description [ja] 親費目グループ
   */
  Admin_Lbl_ParentExpTypeGroup: '親費目グループ',

  /**
   * @description [ja] 親ジョブ名
   */
  Admin_Lbl_ParentJobName: '親ジョブ名',

  /**
   * @description [ja] １日あたり
   */
  Admin_Lbl_PerDay: '１日あたり',

  /**
   * @description [ja] 期間
   */
  Admin_Lbl_Period: '期間',

  /**
   * @description [ja] 代理で承認・却下できる
   */
  Admin_Lbl_PermissionApproveAttRequestByDelegate: '代理で承認・却下できる',

  /**
   * @description [ja] 自身を承認者として申請できる
   */
  Admin_Lbl_PermissionApproveSelfAttRequestByEmployee: '自身を承認者として申請できる',

  /**
   * @description [ja] 各種勤怠申請
   */
  Admin_Lbl_PermissionAttDailyRequest: '各種勤怠申請',

  /**
   * @description [ja] 勤務確定申請
   */
  Admin_Lbl_PermissionAttMonthlyRequest: '勤務確定申請',

  /**
   * @description [ja] 他社員の申請
   */
  Admin_Lbl_PermissionAttRequestByDelegate: '他社員の申請',

  /**
   * @description [ja] 社員自身の申請
   */
  Admin_Lbl_PermissionAttRequestByEmployee: '社員自身の申請',

  /**
   * @description [ja] 可能
   */
  Admin_Lbl_PermissionAvailable: '可能',

  /**
   * @description [ja] 代理で承認取消できる
   */
  Admin_Lbl_PermissionCancelAttApprovalByDelegate: '代理で承認取消できる',

  /**
   * @description [ja] 自身で承認取消できる
   */
  Admin_Lbl_PermissionCancelAttApprovalByEmployee: '自身で承認取消できる',

  /**
   * @description [ja] 代理で申請取消できる
   */
  Admin_Lbl_PermissionCancelAttRequestByDelegate: '代理で申請取消できる',

  /**
   * @description [ja] 編集できる
   */
  Admin_Lbl_PermissionEdit: '編集できる',

  /**
   * @description [ja] 残業警告設定の管理
   */
  Admin_Lbl_PermissionManageAttAgreementAlertSetting: '残業警告設定の管理',

  /**
   * @description [ja] 休暇の管理
   */
  Admin_Lbl_PermissionManageAttLeave: '休暇の管理',

  /**
   * @description [ja] 休暇管理の管理
   */
  Admin_Lbl_PermissionManageAttLeaveGrant: '休暇管理の管理',

  /**
   * @description [ja] 休職・休業の管理
   */
  Admin_Lbl_PermissionManageAttLeaveOfAbsence: '休職・休業の管理',

  /**
   * @description [ja] 休職・休業適用の管理
   */
  Admin_Lbl_PermissionManageAttLeaveOfAbsenceApply: '休職・休業適用の管理',

  /**
   * @description [ja] 勤務パターンの管理
   */
  Admin_Lbl_PermissionManageAttPattern: '勤務パターンの管理',

  /**
   * @description [ja] 勤務パターン適用の管理
   */
  Admin_Lbl_PermissionManageAttPatternApply: '勤務パターン適用の管理',

  /**
   * @description [ja] 短時間勤務設定の管理
   */
  Admin_Lbl_PermissionManageAttShortTimeWorkSetting: '短時間勤務設定の管理',

  /**
   * @description [ja] 短時間勤務設定適用の管理
   */
  Admin_Lbl_PermissionManageAttShortTimeWorkSettingApply: '短時間勤務設定適用の管理',

  /**
   * @description [ja] 勤務体系の管理
   */
  Admin_Lbl_PermissionManageAttWorkingType: '勤務体系の管理',

  /**
   * @description [ja] カレンダーの管理
   */
  Admin_Lbl_PermissionManageCalendar: 'カレンダーの管理',

  /**
   * @description [ja] 部署の管理
   */
  Admin_Lbl_PermissionManageDepartment: '部署の管理',

  /**
   * @description [ja] 社員の管理
   */
  Admin_Lbl_PermissionManageEmployee: '社員の管理',

  /**
   * @description [ja] ジョブの管理
   */
  Admin_Lbl_PermissionManageJob: 'ジョブの管理',

  /**
   * @description [ja] ジョブタイプの管理
   */
  Admin_Lbl_PermissionManageJobType: 'ジョブタイプの管理',

  /**
   * @description [ja] モバイル機能設定の管理
   */
  Admin_Lbl_PermissionManageMobileSetting: 'モバイル機能設定の管理',

  /**
   * @description [ja] 全体設定の管理
   */
  Admin_Lbl_PermissionManageOverallSetting: '全体設定の管理',

  /**
   * @description [ja] アクセス権限の管理
   */
  Admin_Lbl_PermissionManagePermission: 'アクセス権限の管理',

  /**
   * @description [ja] プランナー機能設定の管理
   */
  Admin_Lbl_PermissionManagePlannerSetting: 'プランナー機能設定の管理',

  /**
   * @description [ja] 工数設定の管理
   */
  Admin_Lbl_PermissionManageTimeSetting: '工数設定の管理',

  /**
   * @description [ja] 作業分類の管理
   */
  Admin_Lbl_PermissionManageTimeWorkCategory: '作業分類の管理',

  /**
   * @description [ja] 管理
   */
  Admin_Lbl_PermissionManagement: '管理',

  /**
   * @description [ja] 自身を承認者として申請できる
   */
  Admin_Lbl_PermissionSelfAttRequestByEmployee: '自身を承認者として申請できる',

  /**
   * @description [ja] 権限設定
   */
  Admin_Lbl_PermissionSettings: '権限設定',

  /**
   * @description [ja] 代理で申請できる
   */
  Admin_Lbl_PermissionSubmitAttRequestByDelegate: '代理で申請できる',

  /**
   * @description [ja] 会社の切り替え
   */
  Admin_Lbl_PermissionSwitchCompany: '会社の切り替え',

  /**
   * @description [ja] 工数実績
   */
  Admin_Lbl_PermissionTimeTrack: '工数実績',

  /**
   * @description [ja] 他社員の工数実績
   */
  Admin_Lbl_PermissionTimeTrackByDelegate: '他社員の工数実績',

  /**
   * @description [ja] 勤務表
   */
  Admin_Lbl_PermissionTimesheet: '勤務表',

  /**
   * @description [ja] 他社員の勤務表
   */
  Admin_Lbl_PermissionTimesheetByDelegate: '他社員の勤務表',

  /**
   * @description [ja] 表示できる
   */
  Admin_Lbl_PermissionView: '表示できる',

  /**
   * @description [ja] 個人設定
   */
  Admin_Lbl_PersonalSettings: '個人設定',

  /**
   * @description [ja] 選択リスト名
   */
  Admin_Lbl_PickListLabel: '選択リスト名',

  /**
   * @description [ja] 選択リスト値
   */
  Admin_Lbl_PickListValue: '選択リスト値',

  /**
   * @description [ja] プランナー
   */
  Admin_Lbl_Planner: 'プランナー',

  /**
   * @description [ja] 初期表示をデイリーサマリーとする
   */
  Admin_Lbl_PlannerDefaultView: '初期表示をデイリーサマリーとする',

  /**
   * @description [ja] プランナー設定
   */
  Admin_Lbl_PlannerSetting: 'プランナー設定',

  /**
   * @description [ja] 選択してください
   */
  Admin_Lbl_PleaseSelect: '選択してください',

  /**
   * @description [ja] 役職
   */
  Admin_Lbl_Position: '役職',

  /**
   * @description [ja] 理由
   */
  Admin_Lbl_Reason: '理由',

  /**
   * @description [ja] 改定理由
   */
  Admin_Lbl_ReasonForRevision: '改定理由',

  /**
   * @description [ja] 計上日
   */
  Admin_Lbl_RecordingDate: '計上日',

  /**
   * @description [ja] 備考
   */
  Admin_Lbl_Remarks: '備考',

  /**
   * @description [ja] 休日出勤の代わりに許可される休日
   */
  Admin_Lbl_ReplacementLeaveOfHolidayWork: '休日出勤の代わりに許可される休日',

  /**
   * @description [ja] 申請種別
   */
  Admin_Lbl_ReportType: '申請種別',

  /**
   * @description [ja] 設定されていない申請種別
   */
  Admin_Lbl_ReportTypeNotUsed: '設定されていない申請種別',

  /**
   * @description [ja] 設定されている申請種別
   */
  Admin_Lbl_ReportTypeUsed: '設定されている申請種別',

  /**
   * @description [ja] 申請関連オプション
   */
  Admin_Lbl_RequestOptions: '申請関連オプション',

  /**
   * @description [ja] 打刻時に位置情報を送信する
   */
  Admin_Lbl_RequireLocationAtMobileStamp: '打刻時に位置情報を送信する',

  /**
   * @description [ja] 理由を求める
   */
  Admin_Lbl_RequireReason: '理由を求める',

  /**
   * @description [ja] 申請した場合のみ勤務として認める
   */
  Admin_Lbl_RequireRequestForAcceptAsWorkHours: '申請した場合のみ勤務として認める',

  /**
   * @description [ja] 退社日
   */
  Admin_Lbl_ResignationDate: '退社日',

  /**
   * @description [ja] 改定
   */
  Admin_Lbl_Revision: '改定',

  /**
   * @description [ja] 改定日
   */
  Admin_Lbl_RevisionDate: '改定日',

  /**
   * @description [ja] 公開範囲
   */
  Admin_Lbl_ScopedAssignment: '公開範囲',

  /**
   * @description [ja] 全社共通
   */
  Admin_Lbl_ScopedAssignmentCompany: '全社共通',

  /**
   * @description [ja] 個別に設定する
   */
  Admin_Lbl_ScopedAssignmentPerson: '個別に設定する',

  /**
   * @description [ja] 検索
   */
  Admin_Lbl_Search: '検索',

  /**
   * @description [ja] 経路を検索して定期区間を設定する
   */
  Admin_Lbl_SearchRoute: '経路を検索して定期区間を設定する',

  /**
   * @description [ja] セクション
   */
  Admin_Lbl_Section: 'セクション',

  /**
   * @description [ja] 社員選択
   */
  Admin_Lbl_SelectEmployee: '社員選択',

  /**
   * @description [ja] 利用機能選択
   */
  Admin_Lbl_SelectFunction: '利用機能選択',

  /**
   * @description [ja] 選択中
   */
  Admin_Lbl_Selected: '選択中',

  /**
   * @description [ja] 短時間勤務設定適用
   */
  Admin_Lbl_ShortTimeWorkPeriodStatus: '短時間勤務設定適用',

  /**
   * @description [ja] 短時間勤務理由
   */
  Admin_Lbl_ShortTimeWorkReason: '短時間勤務理由',

  /**
   * @description [ja] 短時間勤務設定
   */
  Admin_Lbl_ShortTimeWorkSetting: '短時間勤務設定',

  /**
   * @description [ja] 開始
   */
  Admin_Lbl_Start: '開始',

  /**
   * @description [ja] 開始日
   */
  Admin_Lbl_StartDate: '開始日',

  /**
   * @description [ja] 月度起算日
   */
  Admin_Lbl_StartDateOfMonth: '月度起算日',

  /**
   * @description [ja] 週度起算曜日
   */
  Admin_Lbl_StartDayOfWeek: '週度起算曜日',

  /**
   * @description [ja] 申請なしでも認める早朝勤務の開始時刻
   */
  Admin_Lbl_StartTimeOfAcceptAsWorkingHour: '申請なしでも認める早朝勤務の開始時刻',

  /**
   * @description [ja] ステータス
   */
  Admin_Lbl_Status: 'ステータス',

  /**
   * @description [ja] 振替休日となる休暇種類
   */
  Admin_Lbl_SubstituteLeaveToUse: '振替休日となる休暇種類',

  /**
   * @description [ja] 成功
   */
  Admin_Lbl_SuccessCount: '成功',

  /**
   * @description [ja] サマリー期間
   */
  Admin_Lbl_SummaryPeriod: 'サマリー期間',

  /**
   * @description [ja] 会社設定
   */
  Admin_Lbl_Tab_Company: '会社設定',

  /**
   * @description [ja] 日付指定
   */
  Admin_Lbl_TargetDate: '日付指定',

  /**
   * @description [ja] 対象社員
   */
  Admin_Lbl_TargetEmployee: '対象社員',

  /**
   * @description [ja] 税率(%)
   */
  Admin_Lbl_TaxRate: '税率(%)',

  /**
   * @description [ja] 税設定
   */
  Admin_Lbl_TaxSetting: '税設定',

  /**
   * @description [ja] 工数設定
   */
  Admin_Lbl_TimeSetting: '工数設定',

  /**
   * @description [ja] 使用する
   */
  Admin_Lbl_Use: '使用する',

  /**
   * @description [ja] 勤怠管理
   */
  Admin_Lbl_UseAttendance: '勤怠管理',

  /**
   * @description [ja] 定期区間を設定する
   */
  Admin_Lbl_UseCommuterPass: '定期区間を設定する',

  /**
   * @description [ja] 会社の税区分マスタを使用する
   */
  Admin_Lbl_UseCompanyTaxMaster: '会社の税区分マスタを使用する',

  /**
   * @description [ja] 経費精算
   */
  Admin_Lbl_UseExpense: '経費精算',

  /**
   * @description [ja] 添付ファイル使用
   */
  Admin_Lbl_UseFileAttachment: '添付ファイル使用',

  /**
   * @description [ja] 外貨
   */
  Admin_Lbl_UseForeignCurrency: '外貨',

  /**
   * @description [ja] プランナー
   */
  Admin_Lbl_UsePlanner: 'プランナー',

  /**
   * @description [ja] 確定申請
   */
  Admin_Lbl_UseTimeTrackRequest: '確定申請',

  /**
   * @description [ja] 工数管理
   */
  Admin_Lbl_UseWorkTime: '工数管理',

  /**
   * @description [ja] Salesforceユーザ
   */
  Admin_Lbl_User: 'Salesforceユーザ',

  /**
   * @description [ja] 有効期間
   */
  Admin_Lbl_ValidDate: '有効期間',

  /**
   * @description [ja] 有効期間開始日
   */
  Admin_Lbl_ValidDateFrom: '有効期間開始日',

  /**
   * @description [ja] 有効期間終了日
   */
  Admin_Lbl_ValidDateTo: '有効期間終了日',

  /**
   * @description [ja] 付与期間
   */
  Admin_Lbl_ValidPeriod: '付与期間',

  /**
   * @description [ja] 項目チェック
   */
  Admin_Lbl_ValidationCheck: '項目チェック',

  /**
   * @description [ja] 支払先
   */
  Admin_Lbl_Vendor: '支払先',

  /**
   * @description [ja] 不要
   */
  Admin_Lbl_VendorPaymentDueDate_NotUsed: '不要',

  /**
   * @description [ja] 任意
   */
  Admin_Lbl_VendorPaymentDueDate_Optional: '任意',

  /**
   * @description [ja] 必須
   */
  Admin_Lbl_VendorPaymentDueDate_Required: '必須',

  /**
   * @description [ja] 表示
   */
  Admin_Lbl_View: '表示',

  /**
   * @description [ja] コアタイムなし
   */
  Admin_Lbl_WithoutCoreTime: 'コアタイムなし',

  /**
   * @description [ja] 作業分類
   */
  Admin_Lbl_WorkCategory: '作業分類',

  /**
   * @description [ja] 作業分類コード
   */
  Admin_Lbl_WorkCategoryCode: '作業分類コード',

  /**
   * @description [ja] 作業分類名
   */
  Admin_Lbl_WorkCategoryName: '作業分類名',

  /**
   * @description [ja] 勤務体系
   */
  Admin_Lbl_WorkScheme: '勤務体系',

  /**
   * @description [ja] 工数管理
   */
  Admin_Lbl_WorkTimeManagement: '工数管理',

  /**
   * @description [ja] 始業終業時刻
   */
  Admin_Lbl_WorkingHours: '始業終業時刻',

  /**
   * @description [ja] 始業終業時刻の目安
   */
  Admin_Lbl_WorkingHoursCriterion: '始業終業時刻の目安',

  /**
   * @description [ja] 補填時間は勤務時間に加える
   */
  Admin_Lbl_WorkingTypeAddCompensationTimeToWorkHours: '補填時間は勤務時間に加える',

  /**
   * @description [ja] 午前半日休始業終業時刻
   */
  Admin_Lbl_WorkingTypeAm: '午前半日休始業終業時刻',

  /**
   * @description [ja] 午前半日休所定労働時間
   */
  Admin_Lbl_WorkingTypeAmContractedWorkHours: '午前半日休所定労働時間',

  /**
   * @description [ja] 午前半日休始業終業時刻の目安
   */
  Admin_Lbl_WorkingTypeAmCriterion: '午前半日休始業終業時刻の目安',

  /**
   * @description [ja] 午前半日休所定休憩１
   */
  Admin_Lbl_WorkingTypeAmRest1: '午前半日休所定休憩１',

  /**
   * @description [ja] 午前半日休所定休憩２
   */
  Admin_Lbl_WorkingTypeAmRest2: '午前半日休所定休憩２',

  /**
   * @description [ja] 午前半日休所定休憩３
   */
  Admin_Lbl_WorkingTypeAmRest3: '午前半日休所定休憩３',

  /**
   * @description [ja] 午前半日休所定休憩４
   */
  Admin_Lbl_WorkingTypeAmRest4: '午前半日休所定休憩４',

  /**
   * @description [ja] 午前半日休所定休憩５
   */
  Admin_Lbl_WorkingTypeAmRest5: '午前半日休所定休憩５',

  /**
   * @description [ja] 午前半日休休憩時刻の目安１
   */
  Admin_Lbl_WorkingTypeAmRestCriterion1: '午前半日休休憩時刻の目安１',

  /**
   * @description [ja] 午前半日休休憩時刻の目安２
   */
  Admin_Lbl_WorkingTypeAmRestCriterion2: '午前半日休休憩時刻の目安２',

  /**
   * @description [ja] 午前半日休休憩時刻の目安３
   */
  Admin_Lbl_WorkingTypeAmRestCriterion3: '午前半日休休憩時刻の目安３',

  /**
   * @description [ja] 午前半日休休憩時刻の目安４
   */
  Admin_Lbl_WorkingTypeAmRestCriterion4: '午前半日休休憩時刻の目安４',

  /**
   * @description [ja] 午前半日休休憩時刻の目安５
   */
  Admin_Lbl_WorkingTypeAmRestCriterion5: '午前半日休休憩時刻の目安５',

  /**
   * @description [ja] 午前半日休所定労働時間の目安
   */
  Admin_Lbl_WorkingTypeAmWorkHoursCriterion: '午前半日休所定労働時間の目安',

  /**
   * @description [ja] 法定休日を自動的に割り当て
   */
  Admin_Lbl_WorkingTypeAutomaticLegalHolidayAssign: '法定休日を自動的に割り当て',

  /**
   * @description [ja] 退勤打刻境界時刻
   */
  Admin_Lbl_WorkingTypeBoundaryEndTimeOfDay: '退勤打刻境界時刻',

  /**
   * @description [ja] 出勤打刻境界時刻
   */
  Admin_Lbl_WorkingTypeBoundaryStartTimeOfDay: '出勤打刻境界時刻',

  /**
   * @description [ja] 勤務日の境界時刻
   */
  Admin_Lbl_WorkingTypeBoundaryTimeOfDay: '勤務日の境界時刻',

  /**
   * @description [ja] ２歴日をまたぐ勤怠計算方法
   */
  Admin_Lbl_WorkingTypeClassificationNextDayWork: '２歴日をまたぐ勤怠計算方法',

  /**
   * @description [ja] 履歴コメント
   */
  Admin_Lbl_WorkingTypeComment: '履歴コメント',

  /**
   * @description [ja] 所定労働時間
   */
  Admin_Lbl_WorkingTypeContractedWorkHours: '所定労働時間',

  /**
   * @description [ja] 深夜勤務終了時刻
   */
  Admin_Lbl_WorkingTypeEndOfNightWork: '深夜勤務終了時刻',

  /**
   * @description [ja] 終業時刻
   */
  Admin_Lbl_WorkingTypeEndTime: '終業時刻',

  /**
   * @description [ja] 午前半日休コアタイム
   */
  Admin_Lbl_WorkingTypeFlexAm: '午前半日休コアタイム',

  /**
   * @description [ja] 午後半日休コアタイム
   */
  Admin_Lbl_WorkingTypeFlexPm: '午後半日休コアタイム',

  /**
   * @description [ja] 法定休日の日数
   */
  Admin_Lbl_WorkingTypeLegalHolidayAssignmentDays: '法定休日の日数',

  /**
   * @description [ja] 法定休日の期間
   */
  Admin_Lbl_WorkingTypeLegalHolidayAssignmentPeriod: '法定休日の期間',

  /**
   * @description [ja] １日の法定労働時間
   */
  Admin_Lbl_WorkingTypeLegalWorkTimeADay: '１日の法定労働時間',

  /**
   * @description [ja] １週の法定労働時間
   */
  Admin_Lbl_WorkingTypeLegalWorkTimeAWeek: '１週の法定労働時間',

  /**
   * @description [ja] 月度の表記
   */
  Admin_Lbl_WorkingTypeMonthMark: '月度の表記',

  /**
   * @description [ja] 勤務体系名
   */
  Admin_Lbl_WorkingTypeName: '勤務体系名',

  /**
   * @description [ja] 相殺する
   */
  Admin_Lbl_WorkingTypeOffset: '相殺する',

  /**
   * @description [ja] 勤務体系オプション
   */
  Admin_Lbl_WorkingTypeOptions: '勤務体系オプション',

  /**
   * @description [ja] 控除と残業
   */
  Admin_Lbl_WorkingTypeOvertimeAndDeductionTime: '控除と残業',

  /**
   * @description [ja] 清算期間
   */
  Admin_Lbl_WorkingTypePayrollPeriod: '清算期間',

  /**
   * @description [ja] 所定時間まで申請なしで残業を許可
   */
  Admin_Lbl_WorkingTypePermitOvertimeWorkUntilContractedHours: '所定時間まで申請なしで残業を許可',

  /**
   * @description [ja] 午後半日休始業終業時刻
   */
  Admin_Lbl_WorkingTypePm: '午後半日休始業終業時刻',

  /**
   * @description [ja] 午後半日休所定労働時間
   */
  Admin_Lbl_WorkingTypePmContractedWorkHours: '午後半日休所定労働時間',

  /**
   * @description [ja] 午後半日休始業終業時刻の目安
   */
  Admin_Lbl_WorkingTypePmCriterion: '午後半日休始業終業時刻の目安',

  /**
   * @description [ja] 午後半日休所定休憩１
   */
  Admin_Lbl_WorkingTypePmRest1: '午後半日休所定休憩１',

  /**
   * @description [ja] 午後半日休所定休憩１終了時刻
   */
  Admin_Lbl_WorkingTypePmRest1EndTime: '午後半日休所定休憩１終了時刻',

  /**
   * @description [ja] 午後半日休所定休憩２
   */
  Admin_Lbl_WorkingTypePmRest2: '午後半日休所定休憩２',

  /**
   * @description [ja] 午後半日休所定休憩３
   */
  Admin_Lbl_WorkingTypePmRest3: '午後半日休所定休憩３',

  /**
   * @description [ja] 午後半日休所定休憩４
   */
  Admin_Lbl_WorkingTypePmRest4: '午後半日休所定休憩４',

  /**
   * @description [ja] 午後半日休所定休憩５
   */
  Admin_Lbl_WorkingTypePmRest5: '午後半日休所定休憩５',

  /**
   * @description [ja] 午後半日休休憩時刻の目安１
   */
  Admin_Lbl_WorkingTypePmRestCriterion1: '午後半日休休憩時刻の目安１',

  /**
   * @description [ja] 午後半日休休憩時刻の目安２
   */
  Admin_Lbl_WorkingTypePmRestCriterion2: '午後半日休休憩時刻の目安２',

  /**
   * @description [ja] 午後半日休休憩時刻の目安３
   */
  Admin_Lbl_WorkingTypePmRestCriterion3: '午後半日休休憩時刻の目安３',

  /**
   * @description [ja] 午後半日休休憩時刻の目安４
   */
  Admin_Lbl_WorkingTypePmRestCriterion4: '午後半日休休憩時刻の目安４',

  /**
   * @description [ja] 午後半日休休憩時刻の目安５
   */
  Admin_Lbl_WorkingTypePmRestCriterion5: '午後半日休休憩時刻の目安５',

  /**
   * @description [ja] 午後半日休所定労働時間の目安
   */
  Admin_Lbl_WorkingTypePmWorkHoursCriterion: '午後半日休所定労働時間の目安',

  /**
   * @description [ja] 所定休憩
   */
  Admin_Lbl_WorkingTypeRest: '所定休憩',

  /**
   * @description [ja] 所定休憩１
   */
  Admin_Lbl_WorkingTypeRest1: '所定休憩１',

  /**
   * @description [ja] 所定休憩２
   */
  Admin_Lbl_WorkingTypeRest2: '所定休憩２',

  /**
   * @description [ja] 所定休憩３
   */
  Admin_Lbl_WorkingTypeRest3: '所定休憩３',

  /**
   * @description [ja] 所定休憩４
   */
  Admin_Lbl_WorkingTypeRest4: '所定休憩４',

  /**
   * @description [ja] 所定休憩５
   */
  Admin_Lbl_WorkingTypeRest5: '所定休憩５',

  /**
   * @description [ja] 所定休憩５開始時刻
   */
  Admin_Lbl_WorkingTypeRest5StartTime: '所定休憩５開始時刻',

  /**
   * @description [ja] 休憩時刻の目安１
   */
  Admin_Lbl_WorkingTypeRestCriterion1: '休憩時刻の目安１',

  /**
   * @description [ja] 休憩時刻の目安２
   */
  Admin_Lbl_WorkingTypeRestCriterion2: '休憩時刻の目安２',

  /**
   * @description [ja] 休憩時刻の目安３
   */
  Admin_Lbl_WorkingTypeRestCriterion3: '休憩時刻の目安３',

  /**
   * @description [ja] 休憩時刻の目安４
   */
  Admin_Lbl_WorkingTypeRestCriterion4: '休憩時刻の目安４',

  /**
   * @description [ja] 休憩時刻の目安５
   */
  Admin_Lbl_WorkingTypeRestCriterion5: '休憩時刻の目安５',

  /**
   * @description [ja] 月度の開始日
   */
  Admin_Lbl_WorkingTypeStartDayOfMonth: '月度の開始日',

  /**
   * @description [ja] 週の開始曜日
   */
  Admin_Lbl_WorkingTypeStartDayOfWeek: '週の開始曜日',

  /**
   * @description [ja] 年度の開始月
   */
  Admin_Lbl_WorkingTypeStartMonthOfYear: '年度の開始月',

  /**
   * @description [ja] 深夜勤務開始時刻
   */
  Admin_Lbl_WorkingTypeStartOfNightWork: '深夜勤務開始時刻',

  /**
   * @description [ja] 始業時刻
   */
  Admin_Lbl_WorkingTypeStartTime: '始業時刻',

  /**
   * @description [ja] 勤務体系時間
   */
  Admin_Lbl_WorkingTypeTime: '勤務体系時間',

  /**
   * @description [ja] 有効開始日
   */
  Admin_Lbl_WorkingTypeValidDateFrom: '有効開始日',

  /**
   * @description [ja] 失効日
   */
  Admin_Lbl_WorkingTypeValidDateTo: '失効日',

  /**
   * @description [ja] 曜日種別・金
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeFRI: '曜日種別・金',

  /**
   * @description [ja] 曜日種別・月
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeMON: '曜日種別・月',

  /**
   * @description [ja] 曜日種別・土
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeSAT: '曜日種別・土',

  /**
   * @description [ja] 曜日種別・日
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeSUN: '曜日種別・日',

  /**
   * @description [ja] 曜日種別・木
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeTHU: '曜日種別・木',

  /**
   * @description [ja] 曜日種別・火
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeTUE: '曜日種別・火',

  /**
   * @description [ja] 曜日種別・水
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeWED: '曜日種別・水',

  /**
   * @description [ja] 労働時間の目安
   */
  Admin_Lbl_WorkingTypeWorkHoursCriterion: '労働時間の目安',

  /**
   * @description [ja] 労働時間制
   */
  Admin_Lbl_WorkingTypeWorkSystem: '労働時間制',

  /**
   * @description [ja] 年度の表記
   */
  Admin_Lbl_WorkingTypeYearMark: '年度の表記',

  /**
   * @description [ja] 有効
   */
  Admin_Msg_Active: '有効',

  /**
   * @description [ja] チェックを付けた場合、ユーザーが税額を手修正できるようになります。
   */
  Admin_Msg_AllowTaxAmountChange: 'チェックを付けた場合、ユーザーが税額を手修正できるようになります。',

  /**
   * @description [ja] 付与された年次有給休暇が表示されます。
   */
  Admin_Msg_AnnualPaidLeaveGrantHistoryListEmptyRowsViewMessage: '付与された年次有給休暇が表示されます。',

  /**
   * @description [ja] プランナーとOffice 365の連携を設定します。
   */
  Admin_Msg_CalendarAccessOffice365: 'プランナーとOffice 365の連携を設定します。',

  /**
   * @description [ja] 削除します。よろしいですか？
   */
  Admin_Msg_ConfirmDelete: '削除します。よろしいですか？',

  /**
   * @description [ja] 設定した時間は控除から免除されます。
   */
  Admin_Msg_DescAllowableShortenTime: '設定した時間は控除から免除されます。',

  /**
   * @description [ja] 設定した時間分コアタイムを短縮します。
   */
  Admin_Msg_DescAllowableShortenTimeForFlex: '設定した時間分コアタイムを短縮します。',

  /**
   * @description [ja] 直課とする
   */
  Admin_Msg_DirectCharged: '直課とする',

  /**
   * @description [ja] [%1] 件の社員が見つかりました。
   */
  Admin_Msg_EmployeesFound: '[%1] 件の社員が見つかりました。',

  /**
   * @description [ja] 設定値が空白です
   */
  Admin_Msg_EmptyItem: '設定値が空白です',

  /**
   * @description [ja] 付与された日数管理休暇が表示されます。
   */
  Admin_Msg_ManagedLeaveGrantHistoryListEmptyRowsViewMessage: '付与された日数管理休暇が表示されます。',

  /**
   * @description [ja] 勤怠管理を利用する
   */
  Admin_Msg_UseAttendance: '勤怠管理を利用する',

  /**
   * @description [ja] 経費精算を利用する
   */
  Admin_Msg_UseExpense: '経費精算を利用する',

  /**
   * @description [ja] プランナーを利用する
   */
  Admin_Msg_UsePlanner: 'プランナーを利用する',

  /**
   * @description [ja] 工数管理を利用する
   */
  Admin_Msg_UseWorkTime: '工数管理を利用する',

  /**
   * @description [ja] 日付
   */
  Admin_Sel_InputTypeDate: '日付',

  /**
   * @description [ja] 参照
   */
  Admin_Sel_InputTypeLookup: '参照',

  /**
   * @description [ja] プルダウン
   */
  Admin_Sel_InputTypePickUp: 'プルダウン',

  /**
   * @description [ja] テキスト
   */
  Admin_Sel_InputTypeText: 'テキスト',

  /**
   * @description [ja] 承 認
   */
  Appr_Btn_Approval: '承 認',

  /**
   * @description [ja] 一括承認 / 却下
   */
  Appr_Btn_ApprovalOrReject: '一括承認 / 却下',

  /**
   * @description [ja] 承認
   */
  Appr_Btn_Approve: '承認',

  /**
   * @description [ja] 全て閉じる
   */
  Appr_Btn_Close: '全て閉じる',

  /**
   * @description [ja] 編集
   */
  Appr_Btn_Edit: '編集',

  /**
   * @description [ja] 事前申請
   */
  Appr_Btn_ExpensesPreApproval: '事前申請',

  /**
   * @description [ja] 経費申請
   */
  Appr_Btn_ExpensesRequest: '経費申請',

  /**
   * @description [ja] 全て開く
   */
  Appr_Btn_Open: '全て開く',

  /**
   * @description [ja] もっと見る
   */
  Appr_Btn_ReadMore: 'もっと見る',

  /**
   * @description [ja] 却下
   */
  Appr_Btn_Reject: '却下',

  /**
   * @description [ja] 工数確定
   */
  Appr_Btn_TimeTrackRequest: '工数確定',

  /**
   * @description [ja] 実行者
   */
  Appr_Lbl_Actor: '実行者',

  /**
   * @description [ja] 申請者名
   */
  Appr_Lbl_ActorName: '申請者名',

  /**
   * @description [ja] 金額
   */
  Appr_Lbl_Amount: '金額',

  /**
   * @description [ja] 税額
   */
  Appr_Lbl_AmountOfTax: '税額',

  /**
   * @description [ja] 申請者名
   */
  Appr_Lbl_ApplicantName: '申請者名',

  /**
   * @description [ja] 承認
   */
  Appr_Lbl_Approval: '承認',

  /**
   * @description [ja] 承認詳細
   */
  Appr_Lbl_ApprovalDetail: '承認詳細',

  /**
   * @description [ja] 承認履歴
   */
  Appr_Lbl_ApprovalHistory: '承認履歴',

  /**
   * @description [ja] 承認申請一覧
   */
  Appr_Lbl_ApprovalList: '承認申請一覧',

  /**
   * @description [ja] 承認コメント
   */
  Appr_Lbl_ApproveComment: '承認コメント',

  /**
   * @description [ja] 承認しました
   */
  Appr_Lbl_Approved: '承認しました',

  /**
   * @description [ja] 承認者名
   */
  Appr_Lbl_ApproverName: '承認者名',

  /**
   * @description [ja] 添付ファイル
   */
  Appr_Lbl_AttachedFiles: '添付ファイル',

  /**
   * @description [ja] 勤怠申請
   */
  Appr_Lbl_AttendanceRequest: '勤怠申請',

  /**
   * @description [ja] 参加者
   */
  Appr_Lbl_Attendee: '参加者',

  /**
   * @description [ja] 変更箇所
   */
  Appr_Lbl_Changes: '変更箇所',

  /**
   * @description [ja] コメント
   */
  Appr_Lbl_Comments: 'コメント',

  /**
   * @description [ja] なし
   */
  Appr_Lbl_ComparisonNone: 'なし',

  /**
   * @description [ja] 続けて登録する
   */
  Appr_Lbl_ContinueToRegister: '続けて登録する',

  /**
   * @description [ja] 費用
   */
  Appr_Lbl_Cost: '費用',

  /**
   * @description [ja] コストセンター
   */
  Appr_Lbl_CostCenter: 'コストセンター',

  /**
   * @description [ja] 費用(一人当たり)
   */
  Appr_Lbl_CostPerHead: '費用(一人当たり)',

  /**
   * @description [ja] 通貨
   */
  Appr_Lbl_Currency: '通貨',

  /**
   * @description [ja] 日付
   */
  Appr_Lbl_Date: '日付',

  /**
   * @description [ja] 申請日
   */
  Appr_Lbl_DateSubmitted: '申請日',

  /**
   * @description [ja] 日別詳細
   */
  Appr_Lbl_DayDetail: '日別詳細',

  /**
   * @description [ja] 代理
   */
  Appr_Lbl_Delegate: '代理',

  /**
   * @description [ja] 代理申請者名
   */
  Appr_Lbl_DelegatedApplicantName: '代理申請者名',

  /**
   * @description [ja] 部署名
   */
  Appr_Lbl_DepartmentName: '部署名',

  /**
   * @description [ja] 詳細
   */
  Appr_Lbl_Detail: '詳細',

  /**
   * @description [ja] 差額
   */
  Appr_Lbl_Difference: '差額',

  /**
   * @description [ja] 社員名 / 社員コード
   */
  Appr_Lbl_EmployeeAndID: '社員名 / 社員コード',

  /**
   * @description [ja] 社員名
   */
  Appr_Lbl_EmployeeName: '社員名',

  /**
   * @description [ja] 税抜
   */
  Appr_Lbl_ExclTax: '税抜',

  /**
   * @description [ja] 会計処理日
   */
  Appr_Lbl_ExpAccountingDate: '会計処理日',

  /**
   * @description [ja] 経費精算
   */
  Appr_Lbl_Expense: '経費精算',

  /**
   * @description [ja] 費目
   */
  Appr_Lbl_ExpenseType: '費目',

  /**
   * @description [ja] 消費税
   */
  Appr_Lbl_GST: '消費税',

  /**
   * @description [ja] 履歴一覧
   */
  Appr_Lbl_HistoryList: '履歴一覧',

  /**
   * @description [ja] 画像
   */
  Appr_Lbl_Image: '画像',

  /**
   * @description [ja] 取り込み
   */
  Appr_Lbl_ImportFrom: '取り込み',

  /**
   * @description [ja] ジョブ
   */
  Appr_Lbl_Job: 'ジョブ',

  /**
   * @description [ja] 直前のステータス
   */
  Appr_Lbl_JustBeforeStatus: '直前のステータス',

  /**
   * @description [ja] 品書き
   */
  Appr_Lbl_Menu: '品書き',

  /**
   * @description [ja] 勤務確定
   */
  Appr_Lbl_MonthlyAttendanceRequest: '勤務確定',

  /**
   * @description [ja] 領収書なし
   */
  Appr_Lbl_NoReceipt: '領収書なし',

  /**
   * @description [ja] 過去に承認した内容が変更されています。変更箇所をご確認ください。
   */
  Appr_Lbl_NotificationReapply: '過去に承認した内容が変更されています。変更箇所をご確認ください。',

  /**
   * @description [ja] 割り当て先
   */
  Appr_Lbl_OriginalActor: '割り当て先',

  /**
   * @description [ja] 支払い期間
   */
  Appr_Lbl_PaymentPeriod: '支払い期間',

  /**
   * @description [ja] 支払種別
   */
  Appr_Lbl_PaymentType: '支払種別',

  /**
   * @description [ja] 対象期間
   */
  Appr_Lbl_Period: '対象期間',

  /**
   * @description [ja] 人
   */
  Appr_Lbl_Person: '人',

  /**
   * @description [ja] 処理日
   */
  Appr_Lbl_PostingDate: '処理日',

  /**
   * @description [ja] 前回申請時
   */
  Appr_Lbl_PreviousApplication: '前回申請時',

  /**
   * @description [ja] 領収書
   */
  Appr_Lbl_Receipt: '領収書',

  /**
   * @description [ja] 件
   */
  Appr_Lbl_RecordCount: '件',

  /**
   * @description [ja] 却下コメント
   */
  Appr_Lbl_RejectComment: '却下コメント',

  /**
   * @description [ja] 却下しました
   */
  Appr_Lbl_Rejected: '却下しました',

  /**
   * @description [ja] 備考
   */
  Appr_Lbl_Remarks: '備考',

  /**
   * @description [ja] 申請
   */
  Appr_Lbl_Request: '申請',

  /**
   * @description [ja] 申請日付
   */
  Appr_Lbl_RequestDate: '申請日付',

  /**
   * @description [ja] 申請番号
   */
  Appr_Lbl_RequestNumber: '申請番号',

  /**
   * @description [ja] 件名 / 申請番号
   */
  Appr_Lbl_RequestTitleAndNumber: '件名 / 申請番号',

  /**
   * @description [ja] 申請種別
   */
  Appr_Lbl_RequestType: '申請種別',

  /**
   * @description [ja] 店名
   */
  Appr_Lbl_Shop: '店名',

  /**
   * @description [ja] 状況
   */
  Appr_Lbl_Situation: '状況',

  /**
   * @description [ja] ステータス
   */
  Appr_Lbl_Status: 'ステータス',

  /**
   * @description [ja] ステップ
   */
  Appr_Lbl_Step: 'ステップ',

  /**
   * @description [ja] 申請する
   */
  Appr_Lbl_Submit: '申請する',

  /**
   * @description [ja] 申請コメント
   */
  Appr_Lbl_SubmitComment: '申請コメント',

  /**
   * @description [ja] 摘要
   */
  Appr_Lbl_Summary: '摘要',

  /**
   * @description [ja] 経費事前申請
   */
  Appr_Lbl_TAndERequest: '経費事前申請',

  /**
   * @description [ja] 事前申請情報
   */
  Appr_Lbl_TAndERequestInfo: '事前申請情報',

  /**
   * @description [ja] 事前申請番号
   */
  Appr_Lbl_TAndERequestNumber: '事前申請番号',

  /**
   * @description [ja] 税率
   */
  Appr_Lbl_TaxRate: '税率',

  /**
   * @description [ja] 日時
   */
  Appr_Lbl_Time: '日時',

  /**
   * @description [ja] 工数実績
   */
  Appr_Lbl_TimeTrack: '工数実績',

  /**
   * @description [ja] 勤務表
   */
  Appr_Lbl_Timesheet: '勤務表',

  /**
   * @description [ja] 件名
   */
  Appr_Lbl_Title: '件名',

  /**
   * @description [ja] 合計
   */
  Appr_Lbl_Total: '合計',

  /**
   * @description [ja] 合計金額
   */
  Appr_Lbl_TotalAmount: '合計金額',

  /**
   * @description [ja] 種別
   */
  Appr_Lbl_Type: '種別',

  /**
   * @description [ja] 拡張項目1
   */
  Appr_Lbl_UserDefinedField1: '拡張項目1',

  /**
   * @description [ja] 拡張項目2
   */
  Appr_Lbl_UserDefinedField2: '拡張項目2',

  /**
   * @description [ja] 支払先
   */
  Appr_Lbl_Vendor: '支払先',

  /**
   * @description [ja] 税抜金額
   */
  Appr_Lbl_WithoutTax: '税抜金額',

  /**
   * @description [ja] 作業
   */
  Appr_Lbl_Work: '作業',

  /**
   * @description [ja] 作業分類
   */
  Appr_Lbl_WorkCategory: '作業分類',

  /**
   * @description [ja] 注意（備考欄をご確認ください）
   */
  Appr_Msg_DailyAttentionTitle: '注意（備考欄をご確認ください）',

  /**
   * @description [ja] 承認対象の申請はありません。
   */
  Appr_Msg_EmptyRequestList: '承認対象の申請はありません。',

  /**
   * @description [ja] 勤務とみなされていない時間帯を含む日が[%1]日あります。
   */
  Appr_Msg_FixSummaryConfirmIneffectiveWorkingTime: '勤務とみなされていない時間帯を含む日が[%1]日あります。',

  /**
   * @description [ja] 休憩が不足している日が[%1]日あります。
   */
  Appr_Msg_FixSummaryConfirmInsufficientRestTime: '休憩が不足している日が[%1]日あります。',

  /**
   * @description [ja] 項目追加
   */
  Att_Btn_AddItem: '項目追加',

  /**
   * @description [ja] 代理承認
   */
  Att_Btn_ApproveOnBehalf: '代理承認',

  /**
   * @description [ja] 取消し
   */
  Att_Btn_CancelApproval: '取消し',

  /**
   * @description [ja] 取消し
   */
  Att_Btn_CancelRequest: '取消し',

  /**
   * @description [ja] 変更
   */
  Att_Btn_ChangeRequest: '変更',

  /**
   * @description [ja] 承認ステップ表示
   */
  Att_Btn_DisplayApprovalSteps: '承認ステップ表示',

  /**
   * @description [ja] 修正
   */
  Att_Btn_ModifyRequest: '修正',

  /**
   * @description [ja] 変更申請
   */
  Att_Btn_Reapply: '変更申請',

  /**
   * @description [ja] 項目削除
   */
  Att_Btn_RemoveItem: '項目削除',

  /**
   * @description [ja] 削除
   */
  Att_Btn_RemoveRequest: '削除',

  /**
   * @description [ja] 申請
   */
  Att_Btn_Request: '申請',

  /**
   * @description [ja] 再申請
   */
  Att_Btn_RequestAgain: '再申請',

  /**
   * @description [ja] 今月
   */
  Att_Btn_ThisMonth: '今月',

  /**
   * @description [ja] 本人宛
   */
  Att_Btn_ToYourself: '本人宛',

  /**
   * @description [ja] 期間内に却下または取消しされた申請があります。
   */
  Att_Err_InvalidRequestExist: '期間内に却下または取消しされた申請があります。',

  /**
   * @description [ja] 期間内に承認待ちの申請があります。
   */
  Att_Err_RequestingRequestExist: '期間内に承認待ちの申請があります。',

  /**
   * @description [ja] 位置情報を送信しない場合はメッセージを入力してください
   */
  Att_Err_RequireCommentWithoutLocation: '位置情報を送信しない場合はメッセージを入力してください',

  /**
   * @description [ja] 午前半日休
   */
  Att_Lbl_AMLeave: '午前半日休',

  /**
   * @description [ja] 欠勤
   */
  Att_Lbl_Absence: '欠勤',

  /**
   * @description [ja] 実労働
   */
  Att_Lbl_ActualWork: '実労働',

  /**
   * @description [ja] 休憩を追加
   */
  Att_Lbl_AddRestTime: '休憩を追加',

  /**
   * @description [ja] 年次有給休暇
   */
  Att_Lbl_AnnualPaidLeave: '年次有給休暇',

  /**
   * @description [ja] 年次有給休暇取得日数
   */
  Att_Lbl_AnnualPaidLeaveDays: '年次有給休暇取得日数',

  /**
   * @description [ja] 年次有給休暇残日数
   */
  Att_Lbl_AnnualPaidLeaveDaysLeft: '年次有給休暇残日数',

  /**
   * @description [ja] 承認ステップ
   */
  Att_Lbl_ApprovalSteps: '承認ステップ',

  /**
   * @description [ja] 申請済み
   */
  Att_Lbl_ApprovelIn: '申請済み',

  /**
   * @description [ja] 勤務パターン
   */
  Att_Lbl_AttPattern: '勤務パターン',

  /**
   * @description [ja] 出勤
   */
  Att_Lbl_Attendance: '出勤',

  /**
   * @description [ja] 退勤時刻
   */
  Att_Lbl_AttendanceEndTime: '退勤時刻',

  /**
   * @description [ja] 勤怠申請一覧
   */
  Att_Lbl_AttendanceRequestList: '勤怠申請一覧',

  /**
   * @description [ja] 出勤時刻
   */
  Att_Lbl_AttendanceStartTime: '出勤時刻',

  /**
   * @description [ja] 私用外出
   */
  Att_Lbl_BreakLost: '私用外出',

  /**
   * @description [ja] 私用外出回数
   */
  Att_Lbl_BreakLostCount: '私用外出回数',

  /**
   * @description [ja] 私用外出 (控除対象)
   */
  Att_Lbl_BreakLostTime: '私用外出 (控除対象)',

  /**
   * @description [ja] 私用外出時間
   */
  Att_Lbl_BreakTime: '私用外出時間',

  /**
   * @description [ja] 取消しました
   */
  Att_Lbl_CancelRequest: '取消しました',

  /**
   * @description [ja] 締め日
   */
  Att_Lbl_ClosingDate: '締め日',

  /**
   * @description [ja] 代替
   */
  Att_Lbl_Compensatory: '代替',

  /**
   * @description [ja] 所定労働時間
   */
  Att_Lbl_ContractedWorkHours: '所定労働時間',

  /**
   * @description [ja] 所定勤務日数
   */
  Att_Lbl_ContractualWorkDays: '所定勤務日数',

  /**
   * @description [ja] 勤務カウント種別
   */
  Att_Lbl_CountType: '勤務カウント種別',

  /**
   * @description [ja] 勤務相当
   */
  Att_Lbl_DailyVirtualWorkTime: '勤務相当',

  /**
   * @description [ja] 日付
   */
  Att_Lbl_Date: '日付',

  /**
   * @description [ja] 全日休
   */
  Att_Lbl_DayLeave: '全日休',

  /**
   * @description [ja] 残日数
   */
  Att_Lbl_DaysLeft: '残日数',

  /**
   * @description [ja] 付与日数
   */
  Att_Lbl_DaysManagedDaysGranted: '付与日数',

  /**
   * @description [ja] 残日数
   */
  Att_Lbl_DaysManagedDaysLeft: '残日数',

  /**
   * @description [ja] 取得日数
   */
  Att_Lbl_DaysManagedDaysTaken: '取得日数',

  /**
   * @description [ja] 付与日
   */
  Att_Lbl_DaysManagedValidDateFrom: '付与日',

  /**
   * @description [ja] 失効日
   */
  Att_Lbl_DaysManagedValidDateTo: '失効日',

  /**
   * @description [ja] 控除
   */
  Att_Lbl_Deducted: '控除',

  /**
   * @description [ja] 過不足時間
   */
  Att_Lbl_DifferenceTime: '過不足時間',

  /**
   * @description [ja] 利用しない
   */
  Att_Lbl_DoNotUseReplacementDayOff: '利用しない',

  /**
   * @description [ja] 時間
   */
  Att_Lbl_Duration: '時間',

  /**
   * @description [ja] 早退
   */
  Att_Lbl_EarlyLeave: '早退',

  /**
   * @description [ja] 早退回数
   */
  Att_Lbl_EarlyLeaveCount: '早退回数',

  /**
   * @description [ja] 早退 (控除対象)
   */
  Att_Lbl_EarlyLeaveLostTime: '早退 (控除対象)',

  /**
   * @description [ja] 早退時間
   */
  Att_Lbl_EarlyLeaveTime: '早退時間',

  /**
   * @description [ja] 終了時刻
   */
  Att_Lbl_EndTime: '終了時刻',

  /**
   * @description [ja] イベント
   */
  Att_Lbl_Event: 'イベント',

  /**
   * @description [ja] 誤差
   */
  Att_Lbl_FetchLocationAccuracy: '誤差',

  /**
   * @description [ja] 位置情報の取得に失敗しました
   */
  Att_Lbl_FetchLocationFail: '位置情報の取得に失敗しました',

  /**
   * @description [ja] お使いの端末の設定で、位置情報の取得が許可されているかご確認ください
   */
  Att_Lbl_FetchLocationFailRemarks: 'お使いの端末の設定で、位置情報の取得が許可されているかご確認ください',

  /**
   * @description [ja] 取得時刻
   */
  Att_Lbl_FetchLocationFetchTime: '取得時刻',

  /**
   * @description [ja] 位置情報を取得中です...
   */
  Att_Lbl_FetchLocationFetching: '位置情報を取得中です...',

  /**
   * @description [ja] 位置情報の取得に成功しました
   */
  Att_Lbl_FetchLocationSuccess: '位置情報の取得に成功しました',

  /**
   * @description [ja] 午前半日休
   */
  Att_Lbl_FirstHalfOfDayLeave: '午前半日休',

  /**
   * @description [ja] 全日休
   */
  Att_Lbl_FullDayLeave: '全日休',

  /**
   * @description [ja] 有給休暇取得日数
   */
  Att_Lbl_GeneralPaidLeaveDays: '有給休暇取得日数',

  /**
   * @description [ja] 半日休
   */
  Att_Lbl_HalfDayLeave: '半日休',

  /**
   * @description [ja] 所定休日出勤日数
   */
  Att_Lbl_HolidayWorkCount: '所定休日出勤日数',

  /**
   * @description [ja] 休日出勤日
   */
  Att_Lbl_HolidayWorkDate: '休日出勤日',

  /**
   * @description [ja] 休日勤務
   */
  Att_Lbl_HolidayWorkTime: '休日勤務',

  /**
   * @description [ja] 時間単位休
   */
  Att_Lbl_HourlyDesignatedLeave: '時間単位休',

  /**
   * @description [ja] 時間単位休
   */
  Att_Lbl_HourlyLeave: '時間単位休',

  /**
   * @description [ja] 勤怠打刻
   */
  Att_Lbl_InputAttendance: '勤怠打刻',

  /**
   * @description [ja] 不足休憩時間
   */
  Att_Lbl_InsufficientRestTime: '不足休憩時間',

  /**
   * @description [ja] 遅刻
   */
  Att_Lbl_LateArrival: '遅刻',

  /**
   * @description [ja] 遅刻回数
   */
  Att_Lbl_LateArriveCount: '遅刻回数',

  /**
   * @description [ja] 遅刻 (控除対象)
   */
  Att_Lbl_LateArriveLostTime: '遅刻 (控除対象)',

  /**
   * @description [ja] 遅刻時間
   */
  Att_Lbl_LateArriveTime: '遅刻時間',

  /**
   * @description [ja] 深夜
   */
  Att_Lbl_LateNight: '深夜',

  /**
   * @description [ja] 取得日数
   */
  Att_Lbl_LeaveDays: '取得日数',

  /**
   * @description [ja] 休暇情報
   */
  Att_Lbl_LeaveDetails: '休暇情報',

  /**
   * @description [ja] 休暇 (控除対象)
   */
  Att_Lbl_LeaveLostTime: '休暇 (控除対象)',

  /**
   * @description [ja] 休暇取得明細
   */
  Att_Lbl_LeaveTakenDetails: '休暇取得明細',

  /**
   * @description [ja] 休暇種類
   */
  Att_Lbl_LeaveType: '休暇種類',

  /**
   * @description [ja] 法定休日
   */
  Att_Lbl_LegalHoliday: '法定休日',

  /**
   * @description [ja] 法定休日出勤日数
   */
  Att_Lbl_LegalHolidayWorkCount: '法定休日出勤日数',

  /**
   * @description [ja] 当月度の超過時間
   */
  Att_Lbl_LegalOverTime: '当月度の超過時間',

  /**
   * @description [ja] 期間の法定労働時間
   */
  Att_Lbl_LegalWorkHours: '期間の法定労働時間',

  /**
   * @description [ja] 日数管理休暇
   */
  Att_Lbl_ManagedLeave: '日数管理休暇',

  /**
   * @description [ja] 勤怠サマリー
   */
  Att_Lbl_MonthlySummary: '勤怠サマリー',

  /**
   * @description [ja] 勤務確定状況
   */
  Att_Lbl_MonthlySummaryStatus: '勤務確定状況',

  /**
   * @description [ja] 新規申請
   */
  Att_Lbl_NewRequest: '新規申請',

  /**
   * @description [ja] 次の承認者
   */
  Att_Lbl_NextApproverEmployee: '次の承認者',

  /**
   * @description [ja] 翌月
   */
  Att_Lbl_NextMonth: '翌月',

  /**
   * @description [ja] 勤務日として扱わない
   */
  Att_Lbl_NotWorkDay: '勤務日として扱わない',

  /**
   * @description [ja] その他の休憩時間
   */
  Att_Lbl_OtherRestTime: 'その他の休憩時間',

  /**
   * @description [ja] その他の休憩時間（分）
   */
  Att_Lbl_OtherRestTimeWithMinutes: 'その他の休憩時間（分）',

  /**
   * @description [ja] 法定内残業時間
   */
  Att_Lbl_OutWorkTime01: '法定内残業時間',

  /**
   * @description [ja] 法定外残業時間
   */
  Att_Lbl_OutWorkTime02: '法定外残業時間',

  /**
   * @description [ja] 法定休日労働時間
   */
  Att_Lbl_OutWorkTime03: '法定休日労働時間',

  /**
   * @description [ja] 法定時間外割増
   */
  Att_Lbl_OutWorkTime04: '法定時間外割増',

  /**
   * @description [ja] 法定休日割増時間
   */
  Att_Lbl_OutWorkTime05: '法定休日割増時間',

  /**
   * @description [ja] 深夜労働時間
   */
  Att_Lbl_OutWorkTime06: '深夜労働時間',

  /**
   * @description [ja] 代休控除時間
   */
  Att_Lbl_OutWorkTime07: '代休控除時間',

  /**
   * @description [ja] 残業
   */
  Att_Lbl_Overtime: '残業',

  /**
   * @description [ja] 午後半日休
   */
  Att_Lbl_PMLeave: '午後半日休',

  /**
   * @description [ja] 有給休暇
   */
  Att_Lbl_Paid: '有給休暇',

  /**
   * @description [ja] 有給計画付与日
   */
  Att_Lbl_PaidDayOff: '有給計画付与日',

  /**
   * @description [ja] 期間
   */
  Att_Lbl_Period: '期間',

  /**
   * @description [ja] フレックス対象労働時間
   */
  Att_Lbl_PlainTime: 'フレックス対象労働時間',

  /**
   * @description [ja] 前月
   */
  Att_Lbl_PrevMonth: '前月',

  /**
   * @description [ja] 当四半期の超過時間
   */
  Att_Lbl_QuarterLegalOverTime: '当四半期の超過時間',

  /**
   * @description [ja] 範囲
   */
  Att_Lbl_Range: '範囲',

  /**
   * @description [ja] 実出勤日数
   */
  Att_Lbl_RealWorkDays: '実出勤日数',

  /**
   * @description [ja] 実労働時間
   */
  Att_Lbl_RealWorkTime: '実労働時間',

  /**
   * @description [ja] 理由
   */
  Att_Lbl_Reason: '理由',

  /**
   * @description [ja] 備考
   */
  Att_Lbl_Remarks: '備考',

  /**
   * @description [ja] 削除しました
   */
  Att_Lbl_RemoveRequest: '削除しました',

  /**
   * @description [ja] 代わりの休日
   */
  Att_Lbl_ReplacementDayOff: '代わりの休日',

  /**
   * @description [ja] 承認済み
   */
  Att_Lbl_ReqStatApproved: '承認済み',

  /**
   * @description [ja] 承認取消
   */
  Att_Lbl_ReqStatCanceled: '承認取消',

  /**
   * @description [ja] 未申請
   */
  Att_Lbl_ReqStatNotRequested: '未申請',

  /**
   * @description [ja] 承認待ち
   */
  Att_Lbl_ReqStatPending: '承認待ち',

  /**
   * @description [ja] 却下
   */
  Att_Lbl_ReqStatRejected: '却下',

  /**
   * @description [ja] 申請取消
   */
  Att_Lbl_ReqStatRemoved: '申請取消',

  /**
   * @description [ja] 申請
   */
  Att_Lbl_Request: '申請',

  /**
   * @description [ja] 申請/イベント
   */
  Att_Lbl_RequestAndEvent: '申請/イベント',

  /**
   * @description [ja] 承認申請
   */
  Att_Lbl_RequestForApproval: '承認申請',

  /**
   * @description [ja] 申請一覧
   */
  Att_Lbl_RequestList: '申請一覧',

  /**
   * @description [ja] 申請しました
   */
  Att_Lbl_Requested: '申請しました',

  /**
   * @description [ja] 休憩
   */
  Att_Lbl_Rest: '休憩',

  /**
   * @description [ja] 休憩時間
   */
  Att_Lbl_RestTime: '休憩時間',

  /**
   * @description [ja] 安全配慮上の超過時間
   */
  Att_Lbl_SafetyObligationalExcessTime: '安全配慮上の超過時間',

  /**
   * @description [ja] 保存しました
   */
  Att_Lbl_SaveWorkTime: '保存しました',

  /**
   * @description [ja] 振替休日取得予定日
   */
  Att_Lbl_ScheduledDateOfSubstitute: '振替休日取得予定日',

  /**
   * @description [ja] 午後半日休
   */
  Att_Lbl_SecondHalfOfDayLeave: '午後半日休',

  /**
   * @description [ja] 位置情報を送信する
   */
  Att_Lbl_SendLocation: '位置情報を送信する',

  /**
   * @description [ja] シフト/短時間勤務
   */
  Att_Lbl_ShiftAndShortTimeWork: 'シフト/短時間勤務',

  /**
   * @description [ja] 短縮時間 (短時間勤務)
   */
  Att_Lbl_ShortenedWorkTime: '短縮時間 (短時間勤務)',

  /**
   * @description [ja] メッセージ
   */
  Att_Lbl_StampMessage: 'メッセージ',

  /**
   * @description [ja] 開始時刻
   */
  Att_Lbl_StartTime: '開始時刻',

  /**
   * @description [ja] ステータス
   */
  Att_Lbl_Status: 'ステータス',

  /**
   * @description [ja] 所定休日
   */
  Att_Lbl_StatutoryHoliday: '所定休日',

  /**
   * @description [ja] 申請済み
   */
  Att_Lbl_Submitted: '申請済み',

  /**
   * @description [ja] 振替休日
   */
  Att_Lbl_Substitute: '振替休日',

  /**
   * @description [ja] 集計No
   */
  Att_Lbl_SummaryItemNo: '集計No',

  /**
   * @description [ja] 取得
   */
  Att_Lbl_Take: '取得',

  /**
   * @description [ja] 年月
   */
  Att_Lbl_TargetMonth: '年月',

  /**
   * @description [ja] 勤務表
   */
  Att_Lbl_TimeAttendance: '勤務表',

  /**
   * @description [ja] 出勤
   */
  Att_Lbl_TimeIn: '出勤',

  /**
   * @description [ja] 時間単位休
   */
  Att_Lbl_TimeLeave: '時間単位休',

  /**
   * @description [ja] 退勤
   */
  Att_Lbl_TimeOut: '退勤',

  /**
   * @description [ja] 工数
   */
  Att_Lbl_TimeTrack: '工数',

  /**
   * @description [ja] 合計
   */
  Att_Lbl_Total: '合計',

  /**
   * @description [ja] 無給休暇
   */
  Att_Lbl_Unpaid: '無給休暇',

  /**
   * @description [ja] 無給休暇取得日数
   */
  Att_Lbl_UnpaidLeaveDays: '無給休暇取得日数',

  /**
   * @description [ja] 期間(複数日)で申請する
   */
  Att_Lbl_UsePeriod: '期間(複数日)で申請する',

  /**
   * @description [ja] 勤務相当時間
   */
  Att_Lbl_VirtualWorkTime: '勤務相当時間',

  /**
   * @description [ja] 36協定対象労働時間
   */
  Att_Lbl_WholeLegalWorkTime: '36協定対象労働時間',

  /**
   * @description [ja] 欠勤日数
   */
  Att_Lbl_WorkAbsenceDays: '欠勤日数',

  /**
   * @description [ja] 勤務時間
   */
  Att_Lbl_WorkHours: '勤務時間',

  /**
   * @description [ja] 勤務体系
   */
  Att_Lbl_WorkScheme: '勤務体系',

  /**
   * @description [ja] 出退勤時刻
   */
  Att_Lbl_WorkTime: '出退勤時刻',

  /**
   * @description [ja] 勤務時間入力
   */
  Att_Lbl_WorkTimeInput: '勤務時間入力',

  /**
   * @description [ja] 勤務体系
   */
  Att_Lbl_WorkingType: '勤務体系',

  /**
   * @description [ja] 当年度の超過回数
   */
  Att_Lbl_YearLegalOverCount: '当年度の超過回数',

  /**
   * @description [ja] 当年度の超過時間
   */
  Att_Lbl_YearLegalOverTime: '当年度の超過時間',

  /**
   * @description [ja] 注意
   */
  Att_Msg_DailyAttention: '注意',

  /**
   * @description [ja] 申請を削除してもよろしいですか？この操作は取り消せません。
   */
  Att_Msg_DailyReqConfirmRemove: '申請を削除してもよろしいですか？この操作は取り消せません。',

  /**
   * @description [ja] 以下の注意事項があります。このまま勤務確定を行ってもよろしいですか？
   */
  Att_Msg_FixSummaryConfirm: '以下の注意事項があります。このまま勤務確定を行ってもよろしいですか？',

  /**
   * @description [ja] 休憩時間が [%1] 分不足しています。
   */
  Att_Msg_InsufficientRestTime: '休憩時間が [%1] 分不足しています。',

  /**
   * @description [ja] 複数の注意があります。クリックして内容をご確認ください。
   */
  Att_Msg_MultipulAttentionMessage: '複数の注意があります。クリックして内容をご確認ください。',

  /**
   * @description [ja] 新規申請することはできません
   */
  Att_Msg_NoRequestAvailable: '新規申請することはできません',

  /**
   * @description [ja] 申請済みはありません
   */
  Att_Msg_NoRequestSubmitted: '申請済みはありません',

  /**
   * @description [ja] [%1] から [%2] までは勤務時間に含まれません。
   */
  Att_Msg_NotIncludeWorkingTime: '[%1] から [%2] までは勤務時間に含まれません。',

  /**
   * @description [ja] 分単位の取得時間は切り上げられます。例：9:30–11:00 → 2時間
   */
  Att_Msg_RoundingUpNotice: '分単位の取得時間は切り上げられます。例：9:30–11:00 → 2時間',

  /**
   * @description [ja] 申請する項目を左のリストから選択してください。
   */
  Att_Msg_SelectRequestTypeFromList: '申請する項目を左のリストから選択してください。',

  /**
   * @description [ja] 【勤務としてみなされていない時間帯: [%1] ～ [%2]】
   */
  Att_Msg_SummaryCommentIneffectiveWorkingTime: '【勤務としてみなされていない時間帯: [%1] ～ [%2]】',

  /**
   * @description [ja] 【休憩不足: [%1] 分】
   */
  Att_Msg_SummaryCommentInsufficientRestTime: '【休憩不足: [%1] 分】',

  /**
   * @description [ja] 出勤打刻が完了しました
   */
  Att_Msg_TimeStampDoneIn: '出勤打刻が完了しました',

  /**
   * @description [ja] 退勤打刻が完了しました
   */
  Att_Msg_TimeStampDoneOut: '退勤打刻が完了しました',

  /**
   * @description [ja] 再出勤打刻が完了しました
   */
  Att_Msg_TimeStampDoneRein: '再出勤打刻が完了しました',

  /**
   * @description [ja] 該当の申請を修正して再申請するか、申請を削除してから勤務表の承認申請を実行してください。
   */
  Att_Slt_InvalidRequestExist: '該当の申請を修正して再申請するか、申請を削除してから勤務表の承認申請を実行してください。',

  /**
   * @description [ja] 申請が承認された後で勤務表の承認申請を実行してください。
   */
  Att_Slt_RequestingRequestExist: '申請が承認された後で勤務表の承認申請を実行してください。',

  /**
   * @description [ja] 実労働時間と工数入力時間に差異があります
   */
  Att_TimeTracking_Hint: '実労働時間と工数入力時間に差異があります',

  /**
   * @description [ja] 勤怠打刻
   */
  Att_Title_TimeStamp: '勤怠打刻',

  /**
   * @description [ja] 完了
   */
  Batch_Lbl_Completed: '完了',

  /**
   * @description [ja] 失敗
   */
  Batch_Lbl_Failed: '失敗',

  /**
   * @description [ja] 実行中
   */
  Batch_Lbl_Processing: '実行中',

  /**
   * @description [ja] 未処理
   */
  Batch_Lbl_Waiting: '未処理',

  /**
   * @description [ja] Cancel
   */
  Cal_Btn_Cancel: 'Cancel',

  /**
   * @description [ja] 日次確定
   */
  Cal_Btn_Daily: '日次確定',

  /**
   * @description [ja] 日
   */
  Cal_Btn_Day: '日',

  /**
   * @description [ja] 削除
   */
  Cal_Btn_Delete: '削除',

  /**
   * @description [ja] 振り替え申請
   */
  Cal_Btn_Exchange: '振り替え申請',

  /**
   * @description [ja] 休日出勤申請
   */
  Cal_Btn_HolidayWork: '休日出勤申請',

  /**
   * @description [ja] 休暇申請
   */
  Cal_Btn_Leave: '休暇申請',

  /**
   * @description [ja] メニュー
   */
  Cal_Btn_Menu: 'メニュー',

  /**
   * @description [ja] 保存
   */
  Cal_Btn_Save: '保存',

  /**
   * @description [ja] 保存して退勤
   */
  Cal_Btn_SaveAndRecordEndTimestamp: '保存して退勤',

  /**
   * @description [ja] 精算する
   */
  Cal_Btn_Settle: '精算する',

  /**
   * @description [ja] 経費事前申請
   */
  Cal_Btn_TERequest: '経費事前申請',

  /**
   * @description [ja] 取引先
   */
  Cal_Lbl_Account: '取引先',

  /**
   * @description [ja] 実労働時間
   */
  Cal_Lbl_ActualWorkingHours: '実労働時間',

  /**
   * @description [ja] 住所
   */
  Cal_Lbl_Address: '住所',

  /**
   * @description [ja] 終日
   */
  Cal_Lbl_AllDay: '終日',

  /**
   * @description [ja] 金額
   */
  Cal_Lbl_Amount: '金額',

  /**
   * @description [ja] 添付ファイル
   */
  Cal_Lbl_AttachedFiles: '添付ファイル',

  /**
   * @description [ja] すべての予定を取得できません
   */
  Cal_Lbl_CalendarEventSyncErrorTitle: 'すべての予定を取得できません',

  /**
   * @description [ja] 負担部署
   */
  Cal_Lbl_CostCenter: '負担部署',

  /**
   * @description [ja] 現在時刻
   */
  Cal_Lbl_CurrentTime: '現在時刻',

  /**
   * @description [ja] お客さま
   */
  Cal_Lbl_Customer: 'お客さま',

  /**
   * @description [ja] デイリーサマリー
   */
  Cal_Lbl_DailySummary: 'デイリーサマリー',

  /**
   * @description [ja] 日時
   */
  Cal_Lbl_Date: '日時',

  /**
   * @description [ja] 説明
   */
  Cal_Lbl_Description: '説明',

  /**
   * @description [ja] 内容
   */
  Cal_Lbl_Details: '内容',

  /**
   * @description [ja] 直接
   */
  Cal_Lbl_Direct: '直接',

  /**
   * @description [ja] 直課
   */
  Cal_Lbl_DirectCharge: '直課',

  /**
   * @description [ja] 退勤時コメント
   */
  Cal_Lbl_EndTimestampComment: '退勤時コメント',

  /**
   * @description [ja] 予定
   */
  Cal_Lbl_Event: '予定',

  /**
   * @description [ja] 経費精算
   */
  Cal_Lbl_Expense: '経費精算',

  /**
   * @description [ja] 経費関連
   */
  Cal_Lbl_ExpenseGroup: '経費関連',

  /**
   * @description [ja] 費目
   */
  Cal_Lbl_ExpenseType: '費目',

  /**
   * @description [ja] 金
   */
  Cal_Lbl_Fri: '金',

  /**
   * @description [ja] 重要
   */
  Cal_Lbl_Important: '重要',

  /**
   * @description [ja] 間接
   */
  Cal_Lbl_Indirect: '間接',

  /**
   * @description [ja] 勤怠打刻
   */
  Cal_Lbl_InputAttendance: '勤怠打刻',

  /**
   * @description [ja] 予定入力
   */
  Cal_Lbl_InputEvent: '予定入力',

  /**
   * @description [ja] ジョブ
   */
  Cal_Lbl_Job: 'ジョブ',

  /**
   * @description [ja] 退社打刻時間
   */
  Cal_Lbl_LeavingWorkTime: '退社打刻時間',

  /**
   * @description [ja] 場所
   */
  Cal_Lbl_Location: '場所',

  /**
   * @description [ja] 月
   */
  Cal_Lbl_Mon: '月',

  /**
   * @description [ja] 月
   */
  Cal_Lbl_Month: '月',

  /**
   * @description [ja] その他
   */
  Cal_Lbl_MoreEvents1: 'その他',

  /**
   * @description [ja] 件...
   */
  Cal_Lbl_MoreEvents2: '件...',

  /**
   * @description [ja] （タイトルなし）
   */
  Cal_Lbl_NoTitle: '（タイトルなし）',

  /**
   * @description [ja] 重要ではない
   */
  Cal_Lbl_NotImportant: '重要ではない',

  /**
   * @description [ja] 急ぎではない
   */
  Cal_Lbl_NotUrgent: '急ぎではない',

  /**
   * @description [ja] Office 365 に登録された予定
   */
  Cal_Lbl_O365CalendarEvent: 'Office 365 に登録された予定',

  /**
   * @description [ja] 外出
   */
  Cal_Lbl_OutOf: '外出',

  /**
   * @description [ja] アウトプット
   */
  Cal_Lbl_Output: 'アウトプット',

  /**
   * @description [ja] 登録件数
   */
  Cal_Lbl_OutputNumber: '登録件数',

  /**
   * @description [ja] 件
   */
  Cal_Lbl_OutputUnit: '件',

  /**
   * @description [ja] 残業
   */
  Cal_Lbl_OvertimeWork: '残業',

  /**
   * @description [ja] プランナー
   */
  Cal_Lbl_Planner: 'プランナー',

  /**
   * @description [ja] プランナー
   */
  Cal_Lbl_Planning: 'プランナー',

  /**
   * @description [ja] 4象限
   */
  Cal_Lbl_Quadrants: '4象限',

  /**
   * @description [ja] 関連目標
   */
  Cal_Lbl_RelatedTarget: '関連目標',

  /**
   * @description [ja] 備考
   */
  Cal_Lbl_Remarks: '備考',

  /**
   * @description [ja] 申請可能項目
   */
  Cal_Lbl_RequestItems: '申請可能項目',

  /**
   * @description [ja] 早退申請
   */
  Cal_Lbl_RequestsLeaveEarly: '早退申請',

  /**
   * @description [ja] 残業申請
   */
  Cal_Lbl_RequestsOvertime: '残業申請',

  /**
   * @description [ja] 土
   */
  Cal_Lbl_Sat: '土',

  /**
   * @description [ja] 経費精算する
   */
  Cal_Lbl_SettleExpenses: '経費精算する',

  /**
   * @description [ja] 日
   */
  Cal_Lbl_Sun: '日',

  /**
   * @description [ja] タスク
   */
  Cal_Lbl_Task: 'タスク',

  /**
   * @description [ja] 暫定労働時間
   */
  Cal_Lbl_TemporaryWorkHours: '暫定労働時間',

  /**
   * @description [ja] 木
   */
  Cal_Lbl_Thu: '木',

  /**
   * @description [ja] 時間
   */
  Cal_Lbl_Time: '時間',

  /**
   * @description [ja] 工数実績
   */
  Cal_Lbl_TimeTrack: '工数実績',

  /**
   * @description [ja] 件名
   */
  Cal_Lbl_Title: '件名',

  /**
   * @description [ja] 今日
   */
  Cal_Lbl_Today: '今日',

  /**
   * @description [ja] 総労働時間
   */
  Cal_Lbl_TotalWorkingHours: '総労働時間',

  /**
   * @description [ja] 交通費
   */
  Cal_Lbl_TransportationExpense: '交通費',

  /**
   * @description [ja] 火
   */
  Cal_Lbl_Tue: '火',

  /**
   * @description [ja] 急ぎ
   */
  Cal_Lbl_Urgent: '急ぎ',

  /**
   * @description [ja] 顧客
   */
  Cal_Lbl_VisitDestination: '顧客',

  /**
   * @description [ja] 水
   */
  Cal_Lbl_Wed: '水',

  /**
   * @description [ja] 週
   */
  Cal_Lbl_Week: '週',

  /**
   * @description [ja] 勤務
   */
  Cal_Lbl_Work: '勤務',

  /**
   * @description [ja] 勤怠関連
   */
  Cal_Lbl_WorkArrangements: '勤怠関連',

  /**
   * @description [ja] 作業分類
   */
  Cal_Lbl_WorkCategory: '作業分類',

  /**
   * @description [ja] 作業報告
   */
  Cal_Lbl_WorkReport: '作業報告',

  /**
   * @description [ja] 工数入力
   */
  Cal_Lbl_WorkTimeInput: '工数入力',

  /**
   * @description [ja] 経費申請
   */
  Cal_Mnu_ExpenseRequest: '経費申請',

  /**
   * @description [ja] その他申請
   */
  Cal_Mnu_OtherRequest: 'その他申請',

  /**
   * @description [ja] 招待された予定は編集できません。
   */
  Cal_Msg_CannotEditInvitedEvent: '招待された予定は編集できません。',

  /**
   * @description [ja] 退勤時にあまった工数時間を割り当てるジョブです
   */
  Cal_Msg_DefaultJobDescription: '退勤時にあまった工数時間を割り当てるジョブです',

  /**
   * @description [ja] 予定を削除してよろしいですか？
   */
  Cal_Msg_DeleteEvent: '予定を削除してよろしいですか？',

  /**
   * @description [ja] 作成する予定の終了日を開始日の前に指定することはできません。
   */
  Cal_Msg_InvalidStartEndTime: '作成する予定の終了日を開始日の前に指定することはできません。',

  /**
   * @description [ja] 早退をした(する)場合、申請します。
   */
  Cal_Txt_RequestsLeaveEarly: '早退をした(する)場合、申請します。',

  /**
   * @description [ja] 残業を申請します。
   */
  Cal_Txt_RequestsOvertime: '残業を申請します。',

  /**
   * @description [ja] [%3]件のうち[%1]件目 - [%2]件目までを表示しています
   */
  Cmn_Lbl_PagerInfo: '[%3]件のうち[%1]件目 - [%2]件目までを表示しています',

  /**
   * @description [ja] 検索結果はありませんでした。
   */
  Cmn_Lbl_SuggestNoResult: '検索結果はありませんでした。',

  /**
   * @description [ja] 登録
   */
  Com_Btn_Add: '登録',

  /**
   * @description [ja] 新規登録
   */
  Com_Btn_AddNew: '新規登録',

  /**
   * @description [ja] 承認取消
   */
  Com_Btn_ApprovalCancel: '承認取消',

  /**
   * @description [ja] 承認履歴
   */
  Com_Btn_ApprovalHistory: '承認履歴',

  /**
   * @description [ja] キャンセル
   */
  Com_Btn_Cancel: 'キャンセル',

  /**
   * @description [ja] 変更
   */
  Com_Btn_Change: '変更',

  /**
   * @description [ja] 承認者選択
   */
  Com_Btn_ChangeApprover: '承認者選択',

  /**
   * @description [ja] 出勤
   */
  Com_Btn_ClockIn: '出勤',

  /**
   * @description [ja] 退勤
   */
  Com_Btn_ClockOut: '退勤',

  /**
   * @description [ja] 再出勤
   */
  Com_Btn_ClockRein: '再出勤',

  /**
   * @description [ja] 閉じる
   */
  Com_Btn_Close: '閉じる',

  /**
   * @description [ja] 確定
   */
  Com_Btn_Confirm: '確定',

  /**
   * @description [ja] 縮める
   */
  Com_Btn_Contract: '縮める',

  /**
   * @description [ja] コピー
   */
  Com_Btn_Copy: 'コピー',

  /**
   * @description [ja] 決定
   */
  Com_Btn_Decide: '決定',

  /**
   * @description [ja] 削除
   */
  Com_Btn_Delete: '削除',

  /**
   * @description [ja] 破棄
   */
  Com_Btn_Discard: '破棄',

  /**
   * @description [ja] 編集
   */
  Com_Btn_Edit: '編集',

  /**
   * @description [ja] 実行
   */
  Com_Btn_Execute: '実行',

  /**
   * @description [ja] 解除
   */
  Com_Btn_Exit: '解除',

  /**
   * @description [ja] 広げる
   */
  Com_Btn_Expand: '広げる',

  /**
   * @description [ja] 一括編集
   */
  Com_Btn_MultiEdit: '一括編集',

  /**
   * @description [ja] 新規追加
   */
  Com_Btn_New: '新規追加',

  /**
   * @description [ja] 新規申請
   */
  Com_Btn_NewRequest: '新規申請',

  /**
   * @description [ja] OK
   */
  Com_Btn_Ok: 'OK',

  /**
   * @description [ja] 開く
   */
  Com_Btn_Open: '開く',

  /**
   * @description [ja] 個人メニューを開く
   */
  Com_Btn_OpenPersonalMenu: '個人メニューを開く',

  /**
   * @description [ja] 個人設定
   */
  Com_Btn_PersonalSetting: '個人設定',

  /**
   * @description [ja] 印刷
   */
  Com_Btn_Print: '印刷',

  /**
   * @description [ja] 印刷する
   */
  Com_Btn_PrintDo: '印刷する',

  /**
   * @description [ja] 印刷画面
   */
  Com_Btn_PrintPage: '印刷画面',

  /**
   * @description [ja] 削除
   */
  Com_Btn_Remove: '削除',

  /**
   * @description [ja] 承認申請
   */
  Com_Btn_Request: '承認申請',

  /**
   * @description [ja] 申請取消
   */
  Com_Btn_RequestCancel: '申請取消',

  /**
   * @description [ja] 取り消し
   */
  Com_Btn_Reset: '取り消し',

  /**
   * @description [ja] 保存
   */
  Com_Btn_Save: '保存',

  /**
   * @description [ja] 新規保存
   */
  Com_Btn_Save_New: '新規保存',

  /**
   * @description [ja] 上書き保存
   */
  Com_Btn_Save_Overwrite: '上書き保存',

  /**
   * @description [ja] 検索
   */
  Com_Btn_Search: '検索',

  /**
   * @description [ja] 選択
   */
  Com_Btn_Select: '選択',

  /**
   * @description [ja] 打刻
   */
  Com_Btn_Stamp: '打刻',

  /**
   * @description [ja] 登録
   */
  Com_Btn_Submit: '登録',

  /**
   * @description [ja] 登録＆追加
   */
  Com_Btn_SubmitAndAdd: '登録＆追加',

  /**
   * @description [ja] 社員切替
   */
  Com_Btn_SwitchEmployee: '社員切替',

  /**
   * @description [ja] 工数
   */
  Com_Btn_TimeTracking: '工数',

  /**
   * @description [ja] 更新
   */
  Com_Btn_Update: '更新',

  /**
   * @description [ja] 休日
   */
  Com_DayType_Holiday: '休日',

  /**
   * @description [ja] 法定休日
   */
  Com_DayType_LegalHoliday: '法定休日',

  /**
   * @description [ja] 優先法定休日
   */
  Com_DayType_PreferredLegalHoliday: '優先法定休日',

  /**
   * @description [ja] 勤務日
   */
  Com_DayType_Workday: '勤務日',

  /**
   * @description [ja] エラー
   */
  Com_Err_ErrorTitle: 'エラー',

  /**
   * @description [ja] 不正な金額です。
   */
  Com_Err_InvalidAmount: '不正な金額です。',

  /**
   * @description [ja] 不正な日付です。
   */
  Com_Err_InvalidDate: '不正な日付です。',

  /**
   * @description [ja] 小数は[%1]桁以内で入力してください。
   */
  Com_Err_InvalidScale: '小数は[%1]桁以内で入力してください。',

  /**
   * @description [ja] 該当する社員が見つかりません
   */
  Com_Err_NoEmployeesFound: '該当する社員が見つかりません',

  /**
   * @description [ja] が入力されていません。
   */
  Com_Err_NotEntered: 'が入力されていません。',

  /**
   * @description [ja] 該当する[%1]が見つかりません。
   */
  Com_Err_NotFound: '該当する[%1]が見つかりません。',

  /**
   * @description [ja] 勤怠表が取得できませんでした。この月度の開始日に対象社員が存在しないか、または対象社員に社員切替するために必要な権限が不足している可能性があります。
   */
  Com_Err_ProxyPermissionErrorBody: '勤怠表が取得できませんでした。この月度の開始日に対象社員が存在しないか、または対象社員に社員切替するために必要な権限が不足している可能性があります。',

  /**
   * @description [ja] 社員の入社日、およびSalesforceユーザのロール設定を確認してください。
   */
  Com_Err_ProxyPermissionErrorSolution: '社員の入社日、およびSalesforceユーザのロール設定を確認してください。',

  /**
   * @description [ja] 承認履歴
   */
  Com_Lbl_ApprovalHistory: '承認履歴',

  /**
   * @description [ja] 承認取消
   */
  Com_Lbl_ApproveCancel: '承認取消',

  /**
   * @description [ja] 承認済み
   */
  Com_Lbl_Approved: '承認済み',

  /**
   * @description [ja] 調整
   */
  Com_Lbl_Arrangement: '調整',

  /**
   * @description [ja] [%1] 時点
   */
  Com_Lbl_AsAt: '[%1] 時点',

  /**
   * @description [ja] 添付ファイル
   */
  Com_Lbl_Attachments: '添付ファイル',

  /**
   * @description [ja] 戻る
   */
  Com_Lbl_Back: '戻る',

  /**
   * @description [ja] 一覧に戻る
   */
  Com_Lbl_BackToList: '一覧に戻る',

  /**
   * @description [ja] キャンセル
   */
  Com_Lbl_Canceled: 'キャンセル',

  /**
   * @description [ja] ファイルを選択する
   */
  Com_Lbl_ChooseFile: 'ファイルを選択する',

  /**
   * @description [ja] 確認
   */
  Com_Lbl_Confirm: '確認',

  /**
   * @description [ja] 日付
   */
  Com_Lbl_Date: '日付',

  /**
   * @description [ja] 日
   */
  Com_Lbl_Day: '日',

  /**
   * @description [ja] 日
   */
  Com_Lbl_Day_s: '日',

  /**
   * @description [ja] 日
   */
  Com_Lbl_Days: '日',

  /**
   * @description [ja] 残工数割当ジョブ
   */
  Com_Lbl_DefaultJob: '残工数割当ジョブ',

  /**
   * @description [ja] 未割り当て
   */
  Com_Lbl_DefaultJobNotSelected: '未割り当て',

  /**
   * @description [ja] 残工数ジョブ
   */
  Com_Lbl_DefaultJobShort: '残工数ジョブ',

  /**
   * @description [ja] 代理承認者
   */
  Com_Lbl_DelegateApprover: '代理承認者',

  /**
   * @description [ja] 削除
   */
  Com_Lbl_DeleteFile: '削除',

  /**
   * @description [ja] 部署コード
   */
  Com_Lbl_DepartmentCode: '部署コード',

  /**
   * @description [ja] 部署名
   */
  Com_Lbl_DepartmentName: '部署名',

  /**
   * @description [ja] 閉じる
   */
  Com_Lbl_Dismiss: '閉じる',

  /**
   * @description [ja] 完了
   */
  Com_Lbl_Done: '完了',

  /**
   * @description [ja] ファイルをドラッグ＆ドロップ、もしくは
   */
  Com_Lbl_DragAndDrop: 'ファイルをドラッグ＆ドロップ、もしくは',

  /**
   * @description [ja] 社員
   */
  Com_Lbl_Employee: '社員',

  /**
   * @description [ja] 社員コード
   */
  Com_Lbl_EmployeeCode: '社員コード',

  /**
   * @description [ja] 社員名
   */
  Com_Lbl_EmployeeName: '社員名',

  /**
   * @description [ja] 社員検索
   */
  Com_Lbl_EmployeeSearch: '社員検索',

  /**
   * @description [ja] エラー
   */
  Com_Lbl_Error: 'エラー',

  /**
   * @description [ja] 経費申請承認
   */
  Com_Lbl_ExpenseApproval: '経費申請承認',

  /**
   * @description [ja] 金
   */
  Com_Lbl_FRIDAY: '金',

  /**
   * @description [ja] 経理承認
   */
  Com_Lbl_FinanceApproval: '経理承認',

  /**
   * @description [ja] 金曜日
   */
  Com_Lbl_Friday: '金曜日',

  /**
   * @description [ja] 付与
   */
  Com_Lbl_Grant: '付与',

  /**
   * @description [ja] 時間
   */
  Com_Lbl_Hour: '時間',

  /**
   * @description [ja] 時間
   */
  Com_Lbl_Hour_s: '時間',

  /**
   * @description [ja] 時間
   */
  Com_Lbl_Hours: '時間',

  /**
   * @description [ja] 情報
   */
  Com_Lbl_Information: '情報',

  /**
   * @description [ja] ジョブ
   */
  Com_Lbl_Job: 'ジョブ',

  /**
   * @description [ja] 月
   */
  Com_Lbl_MONDAY: '月',

  /**
   * @description [ja] ※最大10人まで
   */
  Com_Lbl_MaxDelegateApproverInfo: '※最大10人まで',

  /**
   * @description [ja] 代理承認者に設定できる人数は最大10人です。新規追加する場合は選択済みの代理承認者を解除してください。
   */
  Com_Lbl_MaxDelegateApproverWarning: '代理承認者に設定できる人数は最大10人です。新規追加する場合は選択済みの代理承認者を解除してください。',

  /**
   * @description [ja] 分
   */
  Com_Lbl_Mins: '分',

  /**
   * @description [ja] タップして日付を選択してください
   */
  Com_Lbl_MobilePleaseTapAndSelectDate: 'タップして日付を選択してください',

  /**
   * @description [ja] 月曜日
   */
  Com_Lbl_Monday: '月曜日',

  /**
   * @description [ja] 月
   */
  Com_Lbl_Month: '月',

  /**
   * @description [ja] カ月
   */
  Com_Lbl_Months: 'カ月',

  /**
   * @description [ja] 次へ
   */
  Com_Lbl_NextButton: '次へ',

  /**
   * @description [ja] 領収書はありません
   */
  Com_Lbl_NoReceipt: '領収書はありません',

  /**
   * @description [ja] 設定する項目がありません
   */
  Com_Lbl_NoSetting: '設定する項目がありません',

  /**
   * @description [ja] なし
   */
  Com_Lbl_None: 'なし',

  /**
   * @description [ja] 未申請
   */
  Com_Lbl_NotRequested: '未申請',

  /**
   * @description [ja] 承認待ち
   */
  Com_Lbl_Pending: '承認待ち',

  /**
   * @description [ja] 個人設定
   */
  Com_Lbl_PersonalSetting: '個人設定',

  /**
   * @description [ja] 前へ
   */
  Com_Lbl_PrevButton: '前へ',

  /**
   * @description [ja] 却下
   */
  Com_Lbl_Rejected: '却下',

  /**
   * @description [ja] 備考
   */
  Com_Lbl_Remarks: '備考',

  /**
   * @description [ja] 項目削除
   */
  Com_Lbl_Remove: '項目削除',

  /**
   * @description [ja] 選択解除
   */
  Com_Lbl_RemoveDelegate: '選択解除',

  /**
   * @description [ja] 申請取消
   */
  Com_Lbl_Removed: '申請取消',

  /**
   * @description [ja] 事前申請承認
   */
  Com_Lbl_RequestApproval: '事前申請承認',

  /**
   * @description [ja] 必須
   */
  Com_Lbl_Required: '必須',

  /**
   * @description [ja] 経路
   */
  Com_Lbl_Route: '経路',

  /**
   * @description [ja] 土
   */
  Com_Lbl_SATURDAY: '土',

  /**
   * @description [ja] 日
   */
  Com_Lbl_SUNDAY: '日',

  /**
   * @description [ja] 土曜日
   */
  Com_Lbl_Saturday: '土曜日',

  /**
   * @description [ja] 登録
   */
  Com_Lbl_SaveRestHours: '登録',

  /**
   * @description [ja] 検索
   */
  Com_Lbl_Search: '検索',

  /**
   * @description [ja] 承認者検索
   */
  Com_Lbl_SearchApproverEmployee: '承認者検索',

  /**
   * @description [ja] 検索条件
   */
  Com_Lbl_SearchConditions: '検索条件',

  /**
   * @description [ja] 社員をさがす
   */
  Com_Lbl_SearchEmployee: '社員をさがす',

  /**
   * @description [ja] 選択する
   */
  Com_Lbl_Select: '選択する',

  /**
   * @description [ja] 所属部署の社員を表示
   */
  Com_Lbl_ShowEmployeesInSameDepartment: '所属部署の社員を表示',

  /**
   * @description [ja] ステータス
   */
  Com_Lbl_Status: 'ステータス',

  /**
   * @description [ja] 日曜日
   */
  Com_Lbl_Sunday: '日曜日',

  /**
   * @description [ja] 社員切替
   */
  Com_Lbl_SwitchEmployee: '社員切替',

  /**
   * @description [ja] 木
   */
  Com_Lbl_THURSDAY: '木',

  /**
   * @description [ja] 火
   */
  Com_Lbl_TUESDAY: '火',

  /**
   * @description [ja] 木曜日
   */
  Com_Lbl_Thursday: '木曜日',

  /**
   * @description [ja] 回
   */
  Com_Lbl_Times: '回',

  /**
   * @description [ja] 役職
   */
  Com_Lbl_Title: '役職',

  /**
   * @description [ja] 表示する検索結果が多すぎます。検索条件を絞り込んでもう一度やり直してください。
   */
  Com_Lbl_TooManySearchResults: '表示する検索結果が多すぎます。検索条件を絞り込んでもう一度やり直してください。',

  /**
   * @description [ja] 火曜日
   */
  Com_Lbl_Tuesday: '火曜日',

  /**
   * @description [ja] 種別
   */
  Com_Lbl_Type: '種別',

  /**
   * @description [ja] 未申請
   */
  Com_Lbl_UnApproved: '未申請',

  /**
   * @description [ja] 現在、この画面は使用できません
   */
  Com_Lbl_Unavailable: '現在、この画面は使用できません',

  /**
   * @description [ja] 何度もエラーが発生する場合には...。
   */
  Com_Lbl_UnexpectedErrorPageHint: '何度もエラーが発生する場合には...。',

  /**
   * @description [ja] 以下の内容をシステム管理者に問い合わせてください。
   */
  Com_Lbl_UnexpectedErrorPageHintDescription: '以下の内容をシステム管理者に問い合わせてください。',

  /**
   * @description [ja] ブラウザをリロードして再度お試しください。
   */
  Com_Lbl_UnexpectedErrorPageSolution: 'ブラウザをリロードして再度お試しください。',

  /**
   * @description [ja] 処理が完了しませんでした。
   */
  Com_Lbl_UnexpectedErrorPageTitle: '処理が完了しませんでした。',

  /**
   * @description [ja] 時
   */
  Com_Lbl_UnitHours: '時',

  /**
   * @description [ja] 分
   */
  Com_Lbl_UnitMins: '分',

  /**
   * @description [ja] 時間
   */
  Com_Lbl_UnitTimeHours: '時間',

  /**
   * @description [ja] 未設定
   */
  Com_Lbl_Unspecified: '未設定',

  /**
   * @description [ja] 水
   */
  Com_Lbl_WEDNESDAY: '水',

  /**
   * @description [ja] 水曜日
   */
  Com_Lbl_Wednesday: '水曜日',

  /**
   * @description [ja] 週
   */
  Com_Lbl_Week: '週',

  /**
   * @description [ja] 検索結果は0件です
   */
  Com_Lbl_ZeroSearchResult: '検索結果は0件です',

  /**
   * @description [ja] 休憩時間の入力が [%1] 分不足しています。必要な分を自動登録しますか？\n自動登録しない場合はキャンセルして、勤務表の勤務時間入力から行ってください。
   */
  Com_Msg_InsufficientRestTime: '休憩時間の入力が [%1] 分不足しています。必要な分を自動登録しますか？\n自動登録しない場合はキャンセルして、勤務表の勤務時間入力から行ってください。',

  /**
   * @description [ja] 読み込み中
   */
  Com_Msg_LoadingPage: '読み込み中',

  /**
   * @description [ja] [%1] として代理操作中
   */
  Com_Msg_OperatingAs: '[%1] として代理操作中',

  /**
   * @description [ja] 申請中
   */
  Com_Msg_RequestingPage: '申請中',

  /**
   * @description [ja] 個人設定を保存しました。
   */
  Com_Msg_SavingPersonalSetting: '個人設定を保存しました。',

  /**
   * @description [ja] 検索条件を入れて [検索] を押してください
   */
  Com_Msg_SearchGuideMessage: '検索条件を入れて [検索] を押してください',

  /**
   * @description [ja] すべて
   */
  Com_Sel_All: 'すべて',

  /**
   * @description [ja] 仕訳済み
   */
  Com_Sel_JournalStatusDone: '仕訳済み',

  /**
   * @description [ja] 仕訳待ち
   */
  Com_Sel_JournalStatusWaiting: '仕訳待ち',

  /**
   * @description [ja] する
   */
  Com_Sel_Need: 'する',

  /**
   * @description [ja] しない
   */
  Com_Sel_NotNeed: 'しない',

  /**
   * @description [ja] 選択した明細のみ
   */
  Com_Sel_SelectedOnly: '選択した明細のみ',

  /**
   * @description [ja] 承認済み
   */
  Com_Sel_StatusApproved: '承認済み',

  /**
   * @description [ja] 申請済み
   */
  Com_Sel_StatusApprovelIn: '申請済み',

  /**
   * @description [ja] 精算済み
   */
  Com_Sel_StatusProcessed: '精算済み',

  /**
   * @description [ja] 却下
   */
  Com_Sel_StatusReject: '却下',

  /**
   * @description [ja] 承認待ち
   */
  Com_Status_ApprovalIn: '承認待ち',

  /**
   * @description [ja] 承認済み
   */
  Com_Status_Approved: '承認済み',

  /**
   * @description [ja] 承認取消
   */
  Com_Status_Canceled: '承認取消',

  /**
   * @description [ja] 未申請
   */
  Com_Status_NotRequested: '未申請',

  /**
   * @description [ja] 申請中
   */
  Com_Status_Pending: '申請中',

  /**
   * @description [ja] 承認内容変更承認待ち
   */
  Com_Status_Reapplying: '承認内容変更承認待ち',

  /**
   * @description [ja] 却下
   */
  Com_Status_Rejected: '却下',

  /**
   * @description [ja] 申請取消
   */
  Com_Status_Removed: '申請取消',

  /**
   * @description [ja] （[%1]）
   */
  Com_Str_Parenthesis: '（[%1]）',

  /**
   * @description [ja] 編集中の内容を破棄しますか？
   */
  Common_Confirm_DiscardEdits: '編集中の内容を破棄しますか？',

  /**
   * @description [ja] このファイルフォーマットはサポートされていません。
   */
  Common_Err_InvalidType: 'このファイルフォーマットはサポートされていません。',

  /**
   * @description [ja] 最大文字数を超えています。
   */
  Common_Err_Max: '最大文字数を超えています。',

  /**
   * @description [ja] ファイルが最大サイズ(5MB)を超えています。
   */
  Common_Err_MaxFileSize: 'ファイルが最大サイズ(5MB)を超えています。',

  /**
   * @description [ja] 入力は必須です。
   */
  Common_Err_Required: '入力は必須です。',

  /**
   * @description [ja] 数字である必要があります。
   */
  Common_Err_TypeNumber: '数字である必要があります。',

  /**
   * @description [ja] 計上仕訳
   */
  Exp_Btn_AccrualJournalize: '計上仕訳',

  /**
   * @description [ja] 追加する
   */
  Exp_Btn_AddNewItem: '追加する',

  /**
   * @description [ja] 領収書を追加
   */
  Exp_Btn_AddReceipt: '領収書を追加',

  /**
   * @description [ja] クリア
   */
  Exp_Btn_Clear: 'クリア',

  /**
   * @description [ja] 検索条件を削除
   */
  Exp_Btn_DeleteSearchCondition: '検索条件を削除',

  /**
   * @description [ja] FBデータ出力
   */
  Exp_Btn_FBOutput: 'FBデータ出力',

  /**
   * @description [ja] 費目一覧から
   */
  Exp_Btn_FromExpenseTypeList: '費目一覧から',

  /**
   * @description [ja] お気に入りから
   */
  Exp_Btn_FromFavorite: 'お気に入りから',

  /**
   * @description [ja] 履歴から
   */
  Exp_Btn_FromHistory: '履歴から',

  /**
   * @description [ja] 一覧から
   */
  Exp_Btn_FromList: '一覧から',

  /**
   * @description [ja] 履歴
   */
  Exp_Btn_History: '履歴',

  /**
   * @description [ja] 移動
   */
  Exp_Btn_Move: '移動',

  /**
   * @description [ja] 別の申請へ移動
   */
  Exp_Btn_MoveTo: '別の申請へ移動',

  /**
   * @description [ja] 新規明細
   */
  Exp_Btn_NewRecord: '新規明細',

  /**
   * @description [ja] 新規申請
   */
  Exp_Btn_NewReport: '新規申請',

  /**
   * @description [ja] 新規申請作成
   */
  Exp_Btn_NewRequest: '新規申請作成',

  /**
   * @description [ja] 支払方法設定
   */
  Exp_Btn_PayMethodSetting: '支払方法設定',

  /**
   * @description [ja] 支払日設定
   */
  Exp_Btn_PaymentDateSetting: '支払日設定',

  /**
   * @description [ja] 支払仕訳
   */
  Exp_Btn_PaymentJournal: '支払仕訳',

  /**
   * @description [ja] プレビュー
   */
  Exp_Btn_Preview: 'プレビュー',

  /**
   * @description [ja] 内訳確認
   */
  Exp_Btn_RecordItemsCheck: '内訳確認',

  /**
   * @description [ja] 内訳作成
   */
  Exp_Btn_RecordItemsCreate: '内訳作成',

  /**
   * @description [ja] 内訳削除
   */
  Exp_Btn_RecordItemsDelete: '内訳削除',

  /**
   * @description [ja] 内訳編集
   */
  Exp_Btn_RecordItemsEdit: '内訳編集',

  /**
   * @description [ja] 申請一覧
   */
  Exp_Btn_ReportList: '申請一覧',

  /**
   * @description [ja] 申請
   */
  Exp_Btn_Request: '申請',

  /**
   * @description [ja] 経路を検索
   */
  Exp_Btn_RouteSearch: '経路を検索',

  /**
   * @description [ja] 検索条件を保存
   */
  Exp_Btn_SaveSearchCondition: '検索条件を保存',

  /**
   * @description [ja] 検索
   */
  Exp_Btn_Search: '検索',

  /**
   * @description [ja] 計上日
   */
  Exp_Btn_SearchConditionAccountingDate: '計上日',

  /**
   * @description [ja] 金額
   */
  Exp_Btn_SearchConditionAmount: '金額',

  /**
   * @description [ja] 部署
   */
  Exp_Btn_SearchConditionDepartment: '部署',

  /**
   * @description [ja] 詳細
   */
  Exp_Btn_SearchConditionDetail: '詳細',

  /**
   * @description [ja] 社員
   */
  Exp_Btn_SearchConditionEmployee: '社員',

  /**
   * @description [ja] 申請番号
   */
  Exp_Btn_SearchConditionReportNo: '申請番号',

  /**
   * @description [ja] 申請日
   */
  Exp_Btn_SearchConditionRequestDate: '申請日',

  /**
   * @description [ja] ステータス
   */
  Exp_Btn_SearchConditionStatus: 'ステータス',

  /**
   * @description [ja] 検索条件
   */
  Exp_Btn_SearchSetting: '検索条件',

  /**
   * @description [ja] 申請にまとめる
   */
  Exp_Btn_SelectRecord: '申請にまとめる',

  /**
   * @description [ja] 精算確定
   */
  Exp_Btn_Settle: '精算確定',

  /**
   * @description [ja] 申請
   */
  Exp_Btn_ShowRequests: '申請',

  /**
   * @description [ja] 経理承認済み
   */
  Exp_Btn_StatusOptionAccountingAuthorized: '経理承認済み',

  /**
   * @description [ja] 経理却下
   */
  Exp_Btn_StatusOptionAccountingRejected: '経理却下',

  /**
   * @description [ja] 仕訳データ作成済み
   */
  Exp_Btn_StatusOptionJournalCreated: '仕訳データ作成済み',

  /**
   * @description [ja] 支払い済み
   */
  Exp_Btn_StatusOptionPaid: '支払い済み',

  /**
   * @description [ja] 承認済み
   */
  Exp_Btn_StatusOptionStatusApproved: '承認済み',

  /**
   * @description [ja] 申請
   */
  Exp_Btn_Submit: '申請',

  /**
   * @description [ja] 送信
   */
  Exp_Btn_SubmitReceipt: '送信',

  /**
   * @description [ja] テンプレート
   */
  Exp_Btn_Template: 'テンプレート',

  /**
   * @description [ja] 路線検索
   */
  Exp_Btn_Transit: '路線検索',

  /**
   * @description [ja] 支払仕訳取消
   */
  Exp_Btn_UndoPaymentJournal: '支払仕訳取消',

  /**
   * @description [ja] 確定取消
   */
  Exp_Btn_UndoSettle: '確定取消',

  /**
   * @description [ja] ファイルを添付
   */
  Exp_Btn_UploadFile: 'ファイルを添付',

  /**
   * @description [ja] 支払詳細を確認する
   */
  Exp_Btn_VendorDetail: '支払詳細を確認する',

  /**
   * @description [ja] {$Exp_Clbl_AccountingPeriod}
   */
  Exp_Clbl_AccountingPeriod: '{$Exp_Clbl_AccountingPeriod}',

  /**
   * @description [ja] {$Exp_Clbl_Amount}
   */
  Exp_Clbl_Amount: '{$Exp_Clbl_Amount}',

  /**
   * @description [ja] {$Exp_Clbl_CostCenter}
   */
  Exp_Clbl_CostCenter: '{$Exp_Clbl_CostCenter}',

  /**
   * @description [ja] {$Exp_Clbl_Currency}
   */
  Exp_Clbl_Currency: '{$Exp_Clbl_Currency}',

  /**
   * @description [ja] {$Exp_Clbl_Date}
   */
  Exp_Clbl_Date: '{$Exp_Clbl_Date}',

  /**
   * @description [ja] {$Exp_Clbl_ExchangeRate}
   */
  Exp_Clbl_ExchangeRate: '{$Exp_Clbl_ExchangeRate}',

  /**
   * @description [ja] {$Exp_Clbl_ExpenseType}
   */
  Exp_Clbl_ExpenseType: '{$Exp_Clbl_ExpenseType}',

  /**
   * @description [ja] {$Exp_Clbl_Gst}
   */
  Exp_Clbl_Gst: '{$Exp_Clbl_Gst}',

  /**
   * @description [ja] {$Exp_Clbl_GstAmount}
   */
  Exp_Clbl_GstAmount: '{$Exp_Clbl_GstAmount}',

  /**
   * @description [ja] {$Exp_Clbl_GstRate}
   */
  Exp_Clbl_GstRate: '{$Exp_Clbl_GstRate}',

  /**
   * @description [ja] {$Exp_Clbl_IncludeTax}
   */
  Exp_Clbl_IncludeTax: '{$Exp_Clbl_IncludeTax}',

  /**
   * @description [ja] {$Exp_Clbl_LocalAmount}
   */
  Exp_Clbl_LocalAmount: '{$Exp_Clbl_LocalAmount}',

  /**
   * @description [ja] {$Exp_Clbl_NewReport}
   */
  Exp_Clbl_NewReport: '{$Exp_Clbl_NewReport}',

  /**
   * @description [ja] {$Exp_Clbl_Purpose}
   */
  Exp_Clbl_Purpose: '{$Exp_Clbl_Purpose}',

  /**
   * @description [ja] {$Exp_Clbl_RecordDate}
   */
  Exp_Clbl_RecordDate: '{$Exp_Clbl_RecordDate}',

  /**
   * @description [ja] {$Exp_Clbl_ReportRemarks}
   */
  Exp_Clbl_ReportRemarks: '{$Exp_Clbl_ReportRemarks}',

  /**
   * @description [ja] {$Exp_Clbl_ReportTitle}
   */
  Exp_Clbl_ReportTitle: '{$Exp_Clbl_ReportTitle}',

  /**
   * @description [ja] {$Exp_Clbl_ReportType}
   */
  Exp_Clbl_ReportType: '{$Exp_Clbl_ReportType}',

  /**
   * @description [ja] {$Exp_Clbl_RequestTitle}
   */
  Exp_Clbl_RequestTitle: '{$Exp_Clbl_RequestTitle}',

  /**
   * @description [ja] {$Exp_Clbl_ScheduledDate}
   */
  Exp_Clbl_ScheduledDate: '{$Exp_Clbl_ScheduledDate}',

  /**
   * @description [ja] {$Exp_Clbl_Summary}
   */
  Exp_Clbl_Summary: '{$Exp_Clbl_Summary}',

  /**
   * @description [ja] {$Exp_Clbl_Type}
   */
  Exp_Clbl_Type: '{$Exp_Clbl_Type}',

  /**
   * @description [ja] {$Exp_Clbl_WithoutTax}
   */
  Exp_Clbl_WithoutTax: '{$Exp_Clbl_WithoutTax}',

  /**
   * @description [ja] 左の値は右の値以下にしてください
   */
  Exp_Err_AmountLeftLessThanRight: '左の値は右の値以下にしてください',

  /**
   * @description [ja] [%1]よりも小さい値を入力してください。
   */
  Exp_Err_AmountMaxNumber: '[%1]よりも小さい値を入力してください。',

  /**
   * @description [ja] 金額を選択してください。
   */
  Exp_Err_AmountSelection: '金額を選択してください。',

  /**
   * @description [ja] 到着地が設定されていません
   */
  Exp_Err_ArrivalNotDecided: '到着地が設定されていません',

  /**
   * @description [ja] 明細の日付が申請可能期間外です。
   */
  Exp_Err_DateNotInRange: '明細の日付が申請可能期間外です。',

  /**
   * @description [ja] この費目は選択されている申請種別では使用できません。
   */
  Exp_Err_InvalidRecordExpenseTypeForReportType: 'この費目は選択されている申請種別では使用できません。',

  /**
   * @description [ja] 次の費目は選択されている申請種別では使用できません。:
   */
  Exp_Err_InvalidRecordExpenseTypeForReportTypeMobile: '次の費目は選択されている申請種別では使用できません。:',

  /**
   * @description [ja] 領収書か摘要の記入が必要です。
   */
  Exp_Err_NoReceiptAndReceipt: '領収書か摘要の記入が必要です。',

  /**
   * @description [ja] 経路が選択されていません。
   */
  Exp_Err_NoSelectedRoute: '経路が選択されていません。',

  /**
   * @description [ja] 有効な税区分がありません。
   */
  Exp_Err_NoTaxAvailable: '有効な税区分がありません。',

  /**
   * @description [ja] 出発地が設定されていません
   */
  Exp_Err_OriginNotDecided: '出発地が設定されていません',

  /**
   * @description [ja] 支払日の日付は計上日より後ろにしてください。
   */
  Exp_Err_PaymentDateAfterRecordDate: '支払日の日付は計上日より後ろにしてください。',

  /**
   * @description [ja] には内訳作成が必須です。内訳を設定してください。
   */
  Exp_Err_RecordItemsMandatory: 'には内訳作成が必須です。内訳を設定してください。',

  /**
   * @description [ja] 申請時には明細が１件以上必要です。
   */
  Exp_Err_SubmitReportNoRecords: '申請時には明細が１件以上必要です。',

  /**
   * @description [ja] 金額は費目によって固定されています。
   */
  Exp_Hint_FixedAllowanceMulti: '金額は費目によって固定されています。',

  /**
   * @description [ja] 金額は費目によって固定されています。
   */
  Exp_Hint_FixedAllownceSingle: '金額は費目によって固定されています。',

  /**
   * @description [ja] この通貨は費目によって固定されています。
   */
  Exp_Hint_FixedCurrency: 'この通貨は費目によって固定されています。',

  /**
   * @description [ja] この通貨は明細の通貨によって固定されています。
   */
  Exp_Hint_FixedCurrencyChild: 'この通貨は明細の通貨によって固定されています。',

  /**
   * @description [ja] 取引先名
   */
  Exp_Lbl_Account: '取引先名',

  /**
   * @description [ja] 処理日
   */
  Exp_Lbl_AccountingDate: '処理日',

  /**
   * @description [ja] 申請期間
   */
  Exp_Lbl_AccountingPeriod: '申請期間',

  /**
   * @description [ja] 利用中の申請
   */
  Exp_Lbl_ActiveReport: '利用中の申請',

  /**
   * @description [ja] 申請者名
   */
  Exp_Lbl_ActorName: '申請者名',

  /**
   * @description [ja] 源泉所得税追加
   */
  Exp_Lbl_AddIncomeTax: '源泉所得税追加',

  /**
   * @description [ja] 領収書を登録
   */
  Exp_Lbl_AddReceipt: '領収書を登録',

  /**
   * @description [ja] +経由を追加
   */
  Exp_Lbl_AddViaButton: '+経由を追加',

  /**
   * @description [ja] 住所
   */
  Exp_Lbl_Address: '住所',

  /**
   * @description [ja] 支払希望日
   */
  Exp_Lbl_AdvanceRequestDate: '支払希望日',

  /**
   * @description [ja] 本人立替
   */
  Exp_Lbl_AdvancesPaid: '本人立替',

  /**
   * @description [ja] 航空券
   */
  Exp_Lbl_AirTravelPolicy: '航空券',

  /**
   * @description [ja] 金額
   */
  Exp_Lbl_Amount: '金額',

  /**
   * @description [ja] 金額選択
   */
  Exp_Lbl_AmountSelection: '金額選択',

  /**
   * @description [ja] 利用可能な費目
   */
  Exp_Lbl_ApplicableExpenseType: '利用可能な費目',

  /**
   * @description [ja] 承認日
   */
  Exp_Lbl_ApprovalDate: '承認日',

  /**
   * @description [ja] 確認済み（オプション）
   */
  Exp_Lbl_ApprovedOption: '確認済み（オプション）',

  /**
   * @description [ja] 過去の申請
   */
  Exp_Lbl_ApprovedReport: '過去の申請',

  /**
   * @description [ja] 到着日時
   */
  Exp_Lbl_ArrivedDateTime: '到着日時',

  /**
   * @description [ja] 確定
   */
  Exp_Lbl_Attach: '確定',

  /**
   * @description [ja] 添付ファイル
   */
  Exp_Lbl_AttachedFile: '添付ファイル',

  /**
   * @description [ja] 明細選択
   */
  Exp_Lbl_BackToRecordSelection: '明細選択',

  /**
   * @description [ja] 振込先口座番号
   */
  Exp_Lbl_BankAccountNumber: '振込先口座番号',

  /**
   * @description [ja] 振込先口座種別
   */
  Exp_Lbl_BankAccountType: '振込先口座種別',

  /**
   * @description [ja] 振込先銀行コード
   */
  Exp_Lbl_BankCode: '振込先銀行コード',

  /**
   * @description [ja] 振込通貨
   */
  Exp_Lbl_BankCurrency: '振込通貨',

  /**
   * @description [ja] 振込先銀行名
   */
  Exp_Lbl_BankName: '振込先銀行名',

  /**
   * @description [ja] 基準通貨
   */
  Exp_Lbl_BaseCurrency: '基準通貨',

  /**
   * @description [ja] 予約基準
   */
  Exp_Lbl_BaseOn: '予約基準',

  /**
   * @description [ja] 請求書
   */
  Exp_Lbl_Bill: '請求書',

  /**
   * @description [ja] 請求金額
   */
  Exp_Lbl_BillAmount: '請求金額',

  /**
   * @description [ja] 手配
   */
  Exp_Lbl_Booking: '手配',

  /**
   * @description [ja] 振込先支店住所
   */
  Exp_Lbl_BranchAddress: '振込先支店住所',

  /**
   * @description [ja] 振込先支店コード
   */
  Exp_Lbl_BranchCode: '振込先支店コード',

  /**
   * @description [ja] 振込先支店名
   */
  Exp_Lbl_BranchName: '振込先支店名',

  /**
   * @description [ja] 朝
   */
  Exp_Lbl_Breakfast: '朝',

  /**
   * @description [ja] カメラ画像
   */
  Exp_Lbl_Camera: 'カメラ画像',

  /**
   * @description [ja] 車種
   */
  Exp_Lbl_CarType: '車種',

  /**
   * @description [ja] チェックイン
   */
  Exp_Lbl_CheckInDate: 'チェックイン',

  /**
   * @description [ja] チェックアウト
   */
  Exp_Lbl_CheckOutDate: 'チェックアウト',

  /**
   * @description [ja] ファイルを選択する
   */
  Exp_Lbl_ChooseFile: 'ファイルを選択する',

  /**
   * @description [ja] 都市
   */
  Exp_Lbl_City: '都市',

  /**
   * @description [ja] 拡大する
   */
  Exp_Lbl_ClickToZoom: '拡大する',

  /**
   * @description [ja] コピー
   */
  Exp_Lbl_Clone: 'コピー',

  /**
   * @description [ja] コード
   */
  Exp_Lbl_Code: 'コード',

  /**
   * @description [ja] コメント
   */
  Exp_Lbl_Comment: 'コメント',

  /**
   * @description [ja] 通勤定期区間検索
   */
  Exp_Lbl_CommuterPassSearch: '通勤定期区間検索',

  /**
   * @description [ja] 産業医への確認
   */
  Exp_Lbl_ConsultDoctor: '産業医への確認',

  /**
   * @description [ja] 法人カード
   */
  Exp_Lbl_CorporateCard: '法人カード',

  /**
   * @description [ja] 経由銀行 - 銀行住所
   */
  Exp_Lbl_CorrespondentBankAddress: '経由銀行 - 銀行住所',

  /**
   * @description [ja] 経由銀行 - 銀行名
   */
  Exp_Lbl_CorrespondentBankBankName: '経由銀行 - 銀行名',

  /**
   * @description [ja] 経由銀行 - 銀行支店名
   */
  Exp_Lbl_CorrespondentBankBranchName: '経由銀行 - 銀行支店名',

  /**
   * @description [ja] 経由銀行 - SWIFTコード
   */
  Exp_Lbl_CorrespondentBankSwiftCode: '経由銀行 - SWIFTコード',

  /**
   * @description [ja] コストセンター
   */
  Exp_Lbl_CostCenter: 'コストセンター',

  /**
   * @description [ja] 費用(一人当たり)
   */
  Exp_Lbl_CostPerHead: '費用(一人当たり)',

  /**
   * @description [ja] 一人あたり
   */
  Exp_Lbl_CostPerPerson: '一人あたり',

  /**
   * @description [ja] 国コード
   */
  Exp_Lbl_CountryCode: '国コード',

  /**
   * @description [ja] 領収書から作成
   */
  Exp_Lbl_CreateRecordFromReceipt: '領収書から作成',

  /**
   * @description [ja] 申請作成
   */
  Exp_Lbl_CreateReport: '申請作成',

  /**
   * @description [ja] 通貨
   */
  Exp_Lbl_Currency: '通貨',

  /**
   * @description [ja] 日付
   */
  Exp_Lbl_Date: '日付',

  /**
   * @description [ja] 支払日
   */
  Exp_Lbl_DateOfPayment: '支払日',

  /**
   * @description [ja] 利用日
   */
  Exp_Lbl_DateOfUse: '利用日',

  /**
   * @description [ja] 申請日付
   */
  Exp_Lbl_DateSubmitted: '申請日付',

  /**
   * @description [ja] 削除
   */
  Exp_Lbl_DeleteFile: '削除',

  /**
   * @description [ja] 領収書を削除
   */
  Exp_Lbl_DeleteReceipt: '領収書を削除',

  /**
   * @description [ja] 検索条件を削除
   */
  Exp_Lbl_DeleteSearchCondition: '検索条件を削除',

  /**
   * @description [ja] 出発
   */
  Exp_Lbl_DepartFrom: '出発',

  /**
   * @description [ja] 所属部署
   */
  Exp_Lbl_Department: '所属部署',

  /**
   * @description [ja] 部署コード
   */
  Exp_Lbl_DepartmentCode: '部署コード',

  /**
   * @description [ja] 部署名
   */
  Exp_Lbl_DepartmentName: '部署名',

  /**
   * @description [ja] 往路
   */
  Exp_Lbl_Departure: '往路',

  /**
   * @description [ja] 到着
   */
  Exp_Lbl_Destination: '到着',

  /**
   * @description [ja] 詳細
   */
  Exp_Lbl_Detail: '詳細',

  /**
   * @description [ja] 差額
   */
  Exp_Lbl_DiffAmount: '差額',

  /**
   * @description [ja] 夜
   */
  Exp_Lbl_Dinner: '夜',

  /**
   * @description [ja] 画像をドラッグ＆ドロップ、もしくは
   */
  Exp_Lbl_DragAndDrop: '画像をドラッグ＆ドロップ、もしくは',

  /**
   * @description [ja] 画像をドラッグ&ドロップ、もしくはファイルを選択
   */
  Exp_Lbl_DragAndDropOCR: '画像をドラッグ&ドロップ、もしくはファイルを選択',

  /**
   * @description [ja] 飲酒あり
   */
  Exp_Lbl_Drinking: '飲酒あり',

  /**
   * @description [ja] 変更履歴
   */
  Exp_Lbl_EditHistory: '変更履歴',

  /**
   * @description [ja] コード
   */
  Exp_Lbl_EmployeeCode: 'コード',

  /**
   * @description [ja] 社員コード
   */
  Exp_Lbl_EmployeeCodeFull: '社員コード',

  /**
   * @description [ja] 社員名
   */
  Exp_Lbl_EmployeeName: '社員名',

  /**
   * @description [ja] 見積・注文No
   */
  Exp_Lbl_EstimationNo: '見積・注文No',

  /**
   * @description [ja] 証憑
   */
  Exp_Lbl_Evidence: '証憑',

  /**
   * @description [ja] 為替レート
   */
  Exp_Lbl_ExchangeRate: '為替レート',

  /**
   * @description [ja] 為替レートコード
   */
  Exp_Lbl_ExchangeRateCode: '為替レートコード',

  /**
   * @description [ja] 同じ名前が存在します
   */
  Exp_Lbl_ExistsSameName: '同じ名前が存在します',

  /**
   * @description [ja] エリアを拡大
   */
  Exp_Lbl_ExpandArea: 'エリアを拡大',

  /**
   * @description [ja] 実行者
   */
  Exp_Lbl_ExpenseJournalEmp: '実行者',

  /**
   * @description [ja] 仕訳日時
   */
  Exp_Lbl_ExpenseJournalTime: '仕訳日時',

  /**
   * @description [ja] 経費精算レポート
   */
  Exp_Lbl_ExpenseReport: '経費精算レポート',

  /**
   * @description [ja] 経費申請検索
   */
  Exp_Lbl_ExpenseReportSearch: '経費申請検索',

  /**
   * @description [ja] 精算確定
   */
  Exp_Lbl_ExpenseSettlement: '精算確定',

  /**
   * @description [ja] 費目
   */
  Exp_Lbl_ExpenseType: '費目',

  /**
   * @description [ja] 費目選択
   */
  Exp_Lbl_ExpenseTypeSelect: '費目選択',

  /**
   * @description [ja] 拡張項目
   */
  Exp_Lbl_ExtendedItem: '拡張項目',

  /**
   * @description [ja] 社外
   */
  Exp_Lbl_External: '社外',

  /**
   * @description [ja] 社外参加者
   */
  Exp_Lbl_ExternalParticipants: '社外参加者',

  /**
   * @description [ja] 実行者
   */
  Exp_Lbl_FbOutputEmp: '実行者',

  /**
   * @description [ja] FBデータ出力日時
   */
  Exp_Lbl_FbOutputTime: 'FBデータ出力日時',

  /**
   * @description [ja] フィールド
   */
  Exp_Lbl_FieldName: 'フィールド',

  /**
   * @description [ja] 精算済み
   */
  Exp_Lbl_Finalized: '精算済み',

  /**
   * @description [ja] 会社から
   */
  Exp_Lbl_FromCompany: '会社から',

  /**
   * @description [ja] 直行
   */
  Exp_Lbl_FromHome: '直行',

  /**
   * @description [ja] 申請作成
   */
  Exp_Lbl_GeneralExpense: '申請作成',

  /**
   * @description [ja] 税区分
   */
  Exp_Lbl_Gst: '税区分',

  /**
   * @description [ja] 税額
   */
  Exp_Lbl_GstAmount: '税額',

  /**
   * @description [ja] 消費税率
   */
  Exp_Lbl_GstRate: '消費税率',

  /**
   * @description [ja] 健康診断
   */
  Exp_Lbl_HealthCheck: '健康診断',

  /**
   * @description [ja] ヒント
   */
  Exp_Lbl_Hint: 'ヒント',

  /**
   * @description [ja] ホテル名
   */
  Exp_Lbl_Hotel: 'ホテル名',

  /**
   * @description [ja] 取込み
   */
  Exp_Lbl_Import: '取込み',

  /**
   * @description [ja] カード明細関連付け
   */
  Exp_Lbl_ImportFromCard: 'カード明細関連付け',

  /**
   * @description [ja] 非アクティブ
   */
  Exp_Lbl_Inactive: '非アクティブ',

  /**
   * @description [ja] 税込金額
   */
  Exp_Lbl_IncludeTax: '税込金額',

  /**
   * @description [ja] 社員の初期設定が完了していません
   */
  Exp_Lbl_InitEmpSettingNotCompleted: '社員の初期設定が完了していません',

  /**
   * @description [ja] 未入力
   */
  Exp_Lbl_InputEmpty: '未入力',

  /**
   * @description [ja] 保険申請
   */
  Exp_Lbl_InsuranceRequest: '保険申請',

  /**
   * @description [ja] 社内
   */
  Exp_Lbl_Internal: '社内',

  /**
   * @description [ja] 社内参加者
   */
  Exp_Lbl_InternalParticipant: '社内参加者',

  /**
   * @description [ja] 明細を保存しました
   */
  Exp_Lbl_ItemIsSaved: '明細を保存しました',

  /**
   * @description [ja] ジョブ
   */
  Exp_Lbl_Job: 'ジョブ',

  /**
   * @description [ja] プレビューロード中…
   */
  Exp_Lbl_LoadingPreview: 'プレビューロード中…',

  /**
   * @description [ja] 現地金額
   */
  Exp_Lbl_LocalAmount: '現地金額',

  /**
   * @description [ja] 責任者名
   */
  Exp_Lbl_LocalContact: '責任者名',

  /**
   * @description [ja] 宿泊費
   */
  Exp_Lbl_LodgingPolicy: '宿泊費',

  /**
   * @description [ja] 昼
   */
  Exp_Lbl_Lunch: '昼',

  /**
   * @description [ja] 手動作成
   */
  Exp_Lbl_Manual: '手動作成',

  /**
   * @description [ja] ※ファイル容量5MBまで
   */
  Exp_Lbl_MaxFileSize: '※ファイル容量5MBまで',

  /**
   * @description [ja] 食事手当
   */
  Exp_Lbl_MealAllowance: '食事手当',

  /**
   * @description [ja] 入力した内容に不備があります。内容を確認してください。
   */
  Exp_Lbl_MessageAreaTitle: '入力した内容に不備があります。内容を確認してください。',

  /**
   * @description [ja] 現在モバイルでは下記の機能がサポートされていません。\n - [%1]\n上記を持つ申請はリストに表示されません。
   */
  Exp_Lbl_MobileNotSupportedInfo: '現在モバイルでは下記の機能がサポートされていません。\n - [%1]\n上記を持つ申請はリストに表示されません。',

  /**
   * @description [ja] 変更者
   */
  Exp_Lbl_ModifiedBy: '変更者',

  /**
   * @description [ja] 名前
   */
  Exp_Lbl_Name: '名前',

  /**
   * @description [ja] 続けて登録
   */
  Exp_Lbl_New: '続けて登録',

  /**
   * @description [ja] 新しい値
   */
  Exp_Lbl_NewFieldValue: '新しい値',

  /**
   * @description [ja] 明細から作成
   */
  Exp_Lbl_NewRecord: '明細から作成',

  /**
   * @description [ja] 申請から作成
   */
  Exp_Lbl_NewReport: '申請から作成',

  /**
   * @description [ja] 新しい経費申請
   */
  Exp_Lbl_NewReportCreateExp: '新しい経費申請',

  /**
   * @description [ja] 新しい事前申請
   */
  Exp_Lbl_NewReportCreateReq: '新しい事前申請',

  /**
   * @description [ja] 新規申請
   */
  Exp_Lbl_NewRequest: '新規申請',

  /**
   * @description [ja] 泊
   */
  Exp_Lbl_Night: '泊',

  /**
   * @description [ja] 宿泊数
   */
  Exp_Lbl_Nights: '宿泊数',

  /**
   * @description [ja] いいえ
   */
  Exp_Lbl_No: 'いいえ',

  /**
   * @description [ja] [%2]件中[%1]件目
   */
  Exp_Lbl_NumberDisplay: '[%2]件中[%1]件目',

  /**
   * @description [ja] 参加人数
   */
  Exp_Lbl_NumberOfParticipants: '参加人数',

  /**
   * @description [ja] 合計人数
   */
  Exp_Lbl_NumberOfPeople: '合計人数',

  /**
   * @description [ja] 元の値
   */
  Exp_Lbl_OriginalFieldValue: '元の値',

  /**
   * @description [ja] 他
   */
  Exp_Lbl_Other: '他',

  /**
   * @description [ja] その他手当
   */
  Exp_Lbl_OtherAllowance: 'その他手当',

  /**
   * @description [ja] その他の費目
   */
  Exp_Lbl_OtherExpenses: 'その他の費目',

  /**
   * @description [ja] 参加者
   */
  Exp_Lbl_Participant: '参加者',

  /**
   * @description [ja] パスポート申請
   */
  Exp_Lbl_PassportRequest: 'パスポート申請',

  /**
   * @description [ja] 支払金額
   */
  Exp_Lbl_PayAmount: '支払金額',

  /**
   * @description [ja] 振込受取人名
   */
  Exp_Lbl_PayeeName: '振込受取人名',

  /**
   * @description [ja] 支払額
   */
  Exp_Lbl_PaymentAmount: '支払額',

  /**
   * @description [ja] 支払準備
   */
  Exp_Lbl_PaymentArrangement: '支払準備',

  /**
   * @description [ja] 支払確定
   */
  Exp_Lbl_PaymentConfirmation: '支払確定',

  /**
   * @description [ja] 支払日
   */
  Exp_Lbl_PaymentDate: '支払日',

  /**
   * @description [ja] 支払方法
   */
  Exp_Lbl_PaymentMethod: '支払方法',

  /**
   * @description [ja] 支払い期間
   */
  Exp_Lbl_PaymentPeriod: '支払い期間',

  /**
   * @description [ja] 支払依頼
   */
  Exp_Lbl_PaymentRequest: '支払依頼',

  /**
   * @description [ja] 支払い条件
   */
  Exp_Lbl_PaymentTerm: '支払い条件',

  /**
   * @description [ja] 支払い条件コード
   */
  Exp_Lbl_PaymentTermCode: '支払い条件コード',

  /**
   * @description [ja] 支払種別
   */
  Exp_Lbl_PaymentType: '支払種別',

  /**
   * @description [ja] 日当
   */
  Exp_Lbl_PerDiem: '日当',

  /**
   * @description [ja] 名
   */
  Exp_Lbl_Person: '名',

  /**
   * @description [ja] 本人立替金額
   */
  Exp_Lbl_PersonalAmountTransferred: '本人立替金額',

  /**
   * @description [ja] 画像
   */
  Exp_Lbl_Photo: '画像',

  /**
   * @description [ja] 写真登録日
   */
  Exp_Lbl_PhotpAddedDate: '写真登録日',

  /**
   * @description [ja] 選択してください
   */
  Exp_Lbl_PleaseSelect: '選択してください',

  /**
   * @description [ja] 役職
   */
  Exp_Lbl_Position: '役職',

  /**
   * @description [ja] 事前申請件名
   */
  Exp_Lbl_PreapprovalSubject: '事前申請件名',

  /**
   * @description [ja] 準備確定
   */
  Exp_Lbl_Prepare: '準備確定',

  /**
   * @description [ja] 準備確定者
   */
  Exp_Lbl_PreparedEmp: '準備確定者',

  /**
   * @description [ja] 準備確定日時
   */
  Exp_Lbl_PreparedTime: '準備確定日時',

  /**
   * @description [ja] 私的利用
   */
  Exp_Lbl_PrivateUse: '私的利用',

  /**
   * @description [ja] 処理日
   */
  Exp_Lbl_ProcessedDate: '処理日',

  /**
   * @description [ja] 確定者
   */
  Exp_Lbl_ProcessedEmp: '確定者',

  /**
   * @description [ja] 確定日時
   */
  Exp_Lbl_ProcessedTime: '確定日時',

  /**
   * @description [ja] 規定
   */
  Exp_Lbl_Provision: '規定',

  /**
   * @description [ja] 仮払金額
   */
  Exp_Lbl_ProvisionalPaymentAmount: '仮払金額',

  /**
   * @description [ja] 目的
   */
  Exp_Lbl_Purpose: '目的',

  /**
   * @description [ja] 送金目的コード
   */
  Exp_Lbl_PurposeCode: '送金目的コード',

  /**
   * @description [ja] 見積書
   */
  Exp_Lbl_Quotation: '見積書',

  /**
   * @description [ja] 換算レート(%)
   */
  Exp_Lbl_Rate: '換算レート(%)',

  /**
   * @description [ja] 宿泊費超過理由
   */
  Exp_Lbl_ReasonOfExcess: '宿泊費超過理由',

  /**
   * @description [ja] 申請取消
   */
  Exp_Lbl_Recall: '申請取消',

  /**
   * @description [ja] 領収書
   */
  Exp_Lbl_Receipt: '領収書',

  /**
   * @description [ja] 領収書ライブラリ
   */
  Exp_Lbl_ReceiptLibrary: '領収書ライブラリ',

  /**
   * @description [ja] 領収書が必要です
   */
  Exp_Lbl_ReceiptNeeded: '領収書が必要です',

  /**
   * @description [ja] 領収書選択
   */
  Exp_Lbl_ReceiptSelection: '領収書選択',

  /**
   * @description [ja] 領収書設定
   */
  Exp_Lbl_ReceiptSetting: '領収書設定',

  /**
   * @description [ja] 領収書タイプ
   */
  Exp_Lbl_ReceiptType: '領収書タイプ',

  /**
   * @description [ja] アップロードに失敗しました
   */
  Exp_Lbl_ReceiptUploadFailed: 'アップロードに失敗しました',

  /**
   * @description [ja] アップロードに成功しました
   */
  Exp_Lbl_ReceiptUploadSuccess: 'アップロードに成功しました',

  /**
   * @description [ja] 最近使った項目
   */
  Exp_Lbl_RecentlyUsed: '最近使った項目',

  /**
   * @description [ja] 最近使用した項目
   */
  Exp_Lbl_RecentlyUsedItems: '最近使用した項目',

  /**
   * @description [ja] 種別
   */
  Exp_Lbl_RecordClass: '種別',

  /**
   * @description [ja] 明細コピー 
   */
  Exp_Lbl_RecordClone: '明細コピー ',

  /**
   * @description [ja] 明細をコピーする日付を選択してください。
   */
  Exp_Lbl_RecordCloneDateSelect: '明細をコピーする日付を選択してください。',

  /**
   * @description [ja] 日
   */
  Exp_Lbl_RecordCloneDay: '日',

  /**
   * @description [ja] Shiftキーを押しながら選択すると、範囲を選択できます。
   */
  Exp_Lbl_RecordCloneSelectRange: 'Shiftキーを押しながら選択すると、範囲を選択できます。',

  /**
   * @description [ja] 件
   */
  Exp_Lbl_RecordCount: '件',

  /**
   * @description [ja] 計上日
   */
  Exp_Lbl_RecordDate: '計上日',

  /**
   * @description [ja] 明細詳細
   */
  Exp_Lbl_RecordDetails: '明細詳細',

  /**
   * @description [ja] 内訳
   */
  Exp_Lbl_RecordItems: '内訳',

  /**
   * @description [ja] 内訳詳細
   */
  Exp_Lbl_RecordItemsDetail: '内訳詳細',

  /**
   * @description [ja] 明細一覧
   */
  Exp_Lbl_RecordList: '明細一覧',

  /**
   * @description [ja] 明細種別
   */
  Exp_Lbl_RecordType: '明細種別',

  /**
   * @description [ja] 明細
   */
  Exp_Lbl_Records: '明細',

  /**
   * @description [ja] 明細一覧
   */
  Exp_Lbl_RecordsList: '明細一覧',

  /**
   * @description [ja] 明細選択
   */
  Exp_Lbl_RecordsListSelect: '明細選択',

  /**
   * @description [ja] 元に戻す
   */
  Exp_Lbl_ReduceArea: '元に戻す',

  /**
   * @description [ja] 却下
   */
  Exp_Lbl_Reject: '却下',

  /**
   * @description [ja] 申請
   */
  Exp_Lbl_Report: '申請',

  /**
   * @description [ja] 申請金額
   */
  Exp_Lbl_ReportAmount: '申請金額',

  /**
   * @description [ja] 申請日
   */
  Exp_Lbl_ReportDate: '申請日',

  /**
   * @description [ja] 申請内容
   */
  Exp_Lbl_ReportDetail: '申請内容',

  /**
   * @description [ja] 申請編集
   */
  Exp_Lbl_ReportEdit: '申請編集',

  /**
   * @description [ja] 申請一覧
   */
  Exp_Lbl_ReportList: '申請一覧',

  /**
   * @description [ja] 申請番号
   */
  Exp_Lbl_ReportNo: '申請番号',

  /**
   * @description [ja] 申請名
   */
  Exp_Lbl_ReportTitle: '申請名',

  /**
   * @description [ja] 申請種別
   */
  Exp_Lbl_ReportType: '申請種別',

  /**
   * @description [ja] 申請種別選択
   */
  Exp_Lbl_ReportTypeSelection: '申請種別選択',

  /**
   * @description [ja] 経費申請一覧
   */
  Exp_Lbl_Reports: '経費申請一覧',

  /**
   * @description [ja] 事前申請一覧
   */
  Exp_Lbl_ReportsRequest: '事前申請一覧',

  /**
   * @description [ja] 承認申請
   */
  Exp_Lbl_Request: '承認申請',

  /**
   * @description [ja] 申請内容
   */
  Exp_Lbl_RequestContents: '申請内容',

  /**
   * @description [ja] 申請番号
   */
  Exp_Lbl_RequestNo: '申請番号',

  /**
   * @description [ja] 申請対象
   */
  Exp_Lbl_RequestTarget: '申請対象',

  /**
   * @description [ja] 復路
   */
  Exp_Lbl_Return: '復路',

  /**
   * @description [ja] 帰着区分
   */
  Exp_Lbl_ReturnAt: '帰着区分',

  /**
   * @description [ja] 帰着予定日
   */
  Exp_Lbl_ReturnDate: '帰着予定日',

  /**
   * @description [ja] 返却日時
   */
  Exp_Lbl_ReturnDateTime: '返却日時',

  /**
   * @description [ja] 返却場所
   */
  Exp_Lbl_ReturnPlace: '返却場所',

  /**
   * @description [ja] 帰着予定時刻
   */
  Exp_Lbl_ReturnTime: '帰着予定時刻',

  /**
   * @description [ja] 往復
   */
  Exp_Lbl_RoundTrip: '往復',

  /**
   * @description [ja] 総移動距離[%1]km
   */
  Exp_Lbl_RouteDistance: '総移動距離[%1]km',

  /**
   * @description [ja] EX予約 グリーン車
   */
  Exp_Lbl_RouteExGreen: 'EX予約 グリーン車',

  /**
   * @description [ja] EX予約 自由席
   */
  Exp_Lbl_RouteExJiyuuseki: 'EX予約 自由席',

  /**
   * @description [ja] EX予約 指定席
   */
  Exp_Lbl_RouteExShiteiseki: 'EX予約 指定席',

  /**
   * @description [ja] EX予約利用
   */
  Exp_Lbl_RouteExUsed: 'EX予約利用',

  /**
   * @description [ja] 安
   */
  Exp_Lbl_RouteIconCheap: '安',

  /**
   * @description [ja] EX
   */
  Exp_Lbl_RouteIconEX: 'EX',

  /**
   * @description [ja] 楽
   */
  Exp_Lbl_RouteIconEasy: '楽',

  /**
   * @description [ja] 早
   */
  Exp_Lbl_RouteIconFast: '早',

  /**
   * @description [ja] ICカード利用
   */
  Exp_Lbl_RouteIconIcCard: 'ICカード利用',

  /**
   * @description [ja] 往復
   */
  Exp_Lbl_RouteIconRoundTrip: '往復',

  /**
   * @description [ja] 条件を指定して再検索
   */
  Exp_Lbl_RouteInfoResetButton: '条件を指定して再検索',

  /**
   * @description [ja] 経路を選択してください
   */
  Exp_Lbl_RouteNeeded: '経路を選択してください',

  /**
   * @description [ja] 経路を選択するとこちらに表示されます。
   */
  Exp_Lbl_RouteNoSelect: '経路を選択するとこちらに表示されます。',

  /**
   * @description [ja] 運賃種別
   */
  Exp_Lbl_RouteOptionFareType: '運賃種別',

  /**
   * @description [ja] ICカード
   */
  Exp_Lbl_RouteOptionFareType_IC: 'ICカード',

  /**
   * @description [ja] 切符
   */
  Exp_Lbl_RouteOptionFareType_Ticket: '切符',

  /**
   * @description [ja] 高速バス
   */
  Exp_Lbl_RouteOptionHighWayBus: '高速バス',

  /**
   * @description [ja] 使わない
   */
  Exp_Lbl_RouteOptionHighWayBusNotUse: '使わない',

  /**
   * @description [ja] おまかせ
   */
  Exp_Lbl_RouteOptionHighWayBusUse: 'おまかせ',

  /**
   * @description [ja] 片道
   */
  Exp_Lbl_RouteOptionOneWay: '片道',

  /**
   * @description [ja] 往復
   */
  Exp_Lbl_RouteOptionRoundTrip: '往復',

  /**
   * @description [ja] 表示順
   */
  Exp_Lbl_RouteOptionRouteSort: '表示順',

  /**
   * @description [ja] 安い順
   */
  Exp_Lbl_RouteOptionRouteSort_Cheap: '安い順',

  /**
   * @description [ja] 乗換回数順
   */
  Exp_Lbl_RouteOptionRouteSort_NumberOfTransfers: '乗換回数順',

  /**
   * @description [ja] 早い順
   */
  Exp_Lbl_RouteOptionRouteSort_TimeRequired: '早い順',

  /**
   * @description [ja] 優先席順
   */
  Exp_Lbl_RouteOptionSeatPreference: '優先席順',

  /**
   * @description [ja] 自由席
   */
  Exp_Lbl_RouteOptionSeatPreference_FreeSeat: '自由席',

  /**
   * @description [ja] グリーン車
   */
  Exp_Lbl_RouteOptionSeatPreference_GreenSeat: 'グリーン車',

  /**
   * @description [ja] 指定席
   */
  Exp_Lbl_RouteOptionSeatPreference_ReservedSeat: '指定席',

  /**
   * @description [ja] 有料特急
   */
  Exp_Lbl_RouteOptionUseChargedExpress: '有料特急',

  /**
   * @description [ja] 利用しない
   */
  Exp_Lbl_RouteOptionUseChargedExpress_DoNotUse: '利用しない',

  /**
   * @description [ja] 13km以上の場合利用する
   */
  Exp_Lbl_RouteOptionUseChargedExpress_Over13km: '13km以上の場合利用する',

  /**
   * @description [ja] 利用する
   */
  Exp_Lbl_RouteOptionUseChargedExpress_Use: '利用する',

  /**
   * @description [ja] EX予約
   */
  Exp_Lbl_RouteOptionUseExReservation: 'EX予約',

  /**
   * @description [ja] 利用しない
   */
  Exp_Lbl_RouteOptionUseExReservation_NotUse: '利用しない',

  /**
   * @description [ja] 利用する
   */
  Exp_Lbl_RouteOptionUseExReservation_Use: '利用する',

  /**
   * @description [ja] (駅・スポット・バス停)
   */
  Exp_Lbl_RoutePlaceholder: '(駅・スポット・バス停)',

  /**
   * @description [ja] 経路検索
   */
  Exp_Lbl_RouteSearch: '経路検索',

  /**
   * @description [ja] 経路検索条件
   */
  Exp_Lbl_RouteSearchCondition: '経路検索条件',

  /**
   * @description [ja] 同じ場所に返却
   */
  Exp_Lbl_SamePlace: '同じ場所に返却',

  /**
   * @description [ja] 領収書を読み取り
   */
  Exp_Lbl_ScanReceipt: '領収書を読み取り',

  /**
   * @description [ja] 購入予定日
   */
  Exp_Lbl_ScheduledDate: '購入予定日',

  /**
   * @description [ja] コードまたは名前を入力してください
   */
  Exp_Lbl_SearchCodeOrName: 'コードまたは名前を入力してください',

  /**
   * @description [ja] 承認済み申請一覧
   */
  Exp_Lbl_SearchConditionApprovelreRuestList: '承認済み申請一覧',

  /**
   * @description [ja] 検索条件名
   */
  Exp_Lbl_SearchConditionName: '検索条件名',

  /**
   * @description [ja] 部署を検索
   */
  Exp_Lbl_SearchConditionPlaceholderDepartment: '部署を検索',

  /**
   * @description [ja] 詳細を検索
   */
  Exp_Lbl_SearchConditionPlaceholderDetail: '詳細を検索',

  /**
   * @description [ja] 社員を検索
   */
  Exp_Lbl_SearchConditionPlaceholderEmployee: '社員を検索',

  /**
   * @description [ja] ステータスを検索
   */
  Exp_Lbl_SearchConditionPlaceholderStatus: 'ステータスを検索',

  /**
   * @description [ja] 検索結果
   */
  Exp_Lbl_SearchResult: '検索結果',

  /**
   * @description [ja] 検索結果から選択
   */
  Exp_Lbl_SearchSelect: '検索結果から選択',

  /**
   * @description [ja] 座席クラス
   */
  Exp_Lbl_SeatClass: '座席クラス',

  /**
   * @description [ja] 選択
   */
  Exp_Lbl_Select: '選択',

  /**
   * @description [ja] カテゴリーから検索
   */
  Exp_Lbl_SelectFromCategory: 'カテゴリーから検索',

  /**
   * @description [ja] 領収書ライブラリから選択する
   */
  Exp_Lbl_SelectReceiptLibrary: '領収書ライブラリから選択する',

  /**
   * @description [ja] 経路選択
   */
  Exp_Lbl_SelectRoute: '経路選択',

  /**
   * @description [ja] 名前を入力してください
   */
  Exp_Lbl_SetName: '名前を入力してください',

  /**
   * @description [ja] 支払確定者
   */
  Exp_Lbl_SettledEmp: '支払確定者',

  /**
   * @description [ja] 支払確定日時
   */
  Exp_Lbl_SettledTime: '支払確定日時',

  /**
   * @description [ja] 店名
   */
  Exp_Lbl_ShopName: '店名',

  /**
   * @description [ja] 但し書き
   */
  Exp_Lbl_ShopNote: '但し書き',

  /**
   * @description [ja] 禁煙/喫煙
   */
  Exp_Lbl_SmokingType: '禁煙/喫煙',

  /**
   * @description [ja] 支払元口座
   */
  Exp_Lbl_SourceAccountOfPayment: '支払元口座',

  /**
   * @description [ja] 希望条件
   */
  Exp_Lbl_SpecialRequests: '希望条件',

  /**
   * @description [ja] 経費精算申請を作成する
   */
  Exp_Lbl_StartClaim: '経費精算申請を作成する',

  /**
   * @description [ja] 出発予定日
   */
  Exp_Lbl_StartDate: '出発予定日',

  /**
   * @description [ja] 出発日時
   */
  Exp_Lbl_StartDateTime: '出発日時',

  /**
   * @description [ja] 出発区分
   */
  Exp_Lbl_StartFrom: '出発区分',

  /**
   * @description [ja] 出発予定時刻
   */
  Exp_Lbl_StartTime: '出発予定時刻',

  /**
   * @description [ja] 利用開始日時
   */
  Exp_Lbl_StartUseDatetime: '利用開始日時',

  /**
   * @description [ja] 利用開始場所
   */
  Exp_Lbl_StartUsePlace: '利用開始場所',

  /**
   * @description [ja] ステータス
   */
  Exp_Lbl_Status: 'ステータス',

  /**
   * @description [ja] 未読み取り
   */
  Exp_Lbl_StatusNotScanned: '未読み取り',

  /**
   * @description [ja] 処理中
   */
  Exp_Lbl_StatusProcessing: '処理中',

  /**
   * @description [ja] 読み取り済
   */
  Exp_Lbl_StatusScanned: '読み取り済',

  /**
   * @description [ja] 申請する
   */
  Exp_Lbl_Submit: '申請する',

  /**
   * @description [ja] 摘要
   */
  Exp_Lbl_Summary: '摘要',

  /**
   * @description [ja] SWIFTコード
   */
  Exp_Lbl_SwiftCode: 'SWIFTコード',

  /**
   * @description [ja] 事前申請情報
   */
  Exp_Lbl_TERequest: '事前申請情報',

  /**
   * @description [ja] 事前申請一覧
   */
  Exp_Lbl_TERequests: '事前申請一覧',

  /**
   * @description [ja] 対象
   */
  Exp_Lbl_Target: '対象',

  /**
   * @description [ja] 税率
   */
  Exp_Lbl_TaxRate: '税率',

  /**
   * @description [ja] 日時
   */
  Exp_Lbl_Time: '日時',

  /**
   * @description [ja] 件名
   */
  Exp_Lbl_Title: '件名',

  /**
   * @description [ja] 会社へ
   */
  Exp_Lbl_ToCompany: '会社へ',

  /**
   * @description [ja] 直帰
   */
  Exp_Lbl_ToHome: '直帰',

  /**
   * @description [ja] 計
   */
  Exp_Lbl_Total: '計',

  /**
   * @description [ja] 合計金額
   */
  Exp_Lbl_TotalAmount: '合計金額',

  /**
   * @description [ja] 合計
   */
  Exp_Lbl_TotalLong: '合計',

  /**
   * @description [ja] 飛行機
   */
  Exp_Lbl_TransTypeNameAirline: '飛行機',

  /**
   * @description [ja] 空港連絡バス
   */
  Exp_Lbl_TransTypeNameAirportBus: '空港連絡バス',

  /**
   * @description [ja] バス
   */
  Exp_Lbl_TransTypeNameBus: 'バス',

  /**
   * @description [ja] 自動車
   */
  Exp_Lbl_TransTypeNameCar: '自動車',

  /**
   * @description [ja] 有料急行列車
   */
  Exp_Lbl_TransTypeNameExpressTrain: '有料急行列車',

  /**
   * @description [ja] 船
   */
  Exp_Lbl_TransTypeNameFerry: '船',

  /**
   * @description [ja] 高速バス
   */
  Exp_Lbl_TransTypeNameHighwayBus: '高速バス',

  /**
   * @description [ja] JR在来線
   */
  Exp_Lbl_TransTypeNameJROrdinaryLine: 'JR在来線',

  /**
   * @description [ja] 有料特急列車
   */
  Exp_Lbl_TransTypeNamePremierExpressTrain: '有料特急列車',

  /**
   * @description [ja] 私鉄在来線
   */
  Exp_Lbl_TransTypeNamePrivateOrdinaryLine: '私鉄在来線',

  /**
   * @description [ja] 新幹線
   */
  Exp_Lbl_TransTypeNameShinkansen: '新幹線',

  /**
   * @description [ja] 寝台列車
   */
  Exp_Lbl_TransTypeNameSleeperTrain: '寝台列車',

  /**
   * @description [ja] 地下鉄
   */
  Exp_Lbl_TransTypeNameSubway: '地下鉄',

  /**
   * @description [ja] 路面電車
   */
  Exp_Lbl_TransTypeNameTram: '路面電車',

  /**
   * @description [ja] 徒歩
   */
  Exp_Lbl_TransTypeNameWalk: '徒歩',

  /**
   * @description [ja] 路線検索
   */
  Exp_Lbl_Transit: '路線検索',

  /**
   * @description [ja] 交通カード
   */
  Exp_Lbl_TransportCard: '交通カード',

  /**
   * @description [ja] トラベル
   */
  Exp_Lbl_Travel: 'トラベル',

  /**
   * @description [ja] 渡航準備金申請
   */
  Exp_Lbl_TravelBudgetRequest: '渡航準備金申請',

  /**
   * @description [ja] 未申請明細一覧
   */
  Exp_Lbl_UnreportedExpenses: '未申請明細一覧',

  /**
   * @description [ja] 撮影した領収書をアップロードしてください
   */
  Exp_Lbl_UploadReceiptImage: '撮影した領収書をアップロードしてください',

  /**
   * @description [ja] 拡張項目1
   */
  Exp_Lbl_UserDefinedField1: '拡張項目1',

  /**
   * @description [ja] 拡張項目２
   */
  Exp_Lbl_UserDefinedField2: '拡張項目２',

  /**
   * @description [ja] 支払先
   */
  Exp_Lbl_Vendor: '支払先',

  /**
   * @description [ja] 支払先詳細
   */
  Exp_Lbl_VendorDetail: '支払先詳細',

  /**
   * @description [ja] 経由
   */
  Exp_Lbl_Via: '経由',

  /**
   * @description [ja] ビザ申請
   */
  Exp_Lbl_VisaRequest: 'ビザ申請',

  /**
   * @description [ja] 出張先住所
   */
  Exp_Lbl_VisitingAddress: '出張先住所',

  /**
   * @description [ja] 現地受入部署
   */
  Exp_Lbl_VisitingDept: '現地受入部署',

  /**
   * @description [ja] 出張先
   */
  Exp_Lbl_VisitingFor: '出張先',

  /**
   * @description [ja] 食事付き
   */
  Exp_Lbl_WithMeals: '食事付き',

  /**
   * @description [ja] 源泉徴収
   */
  Exp_Lbl_WithholdingTax: '源泉徴収',

  /**
   * @description [ja] 税抜
   */
  Exp_Lbl_WithoutGst: '税抜',

  /**
   * @description [ja] 税抜金額
   */
  Exp_Lbl_WithoutTax: '税抜金額',

  /**
   * @description [ja] はい
   */
  Exp_Lbl_Yes: 'はい',

  /**
   * @description [ja] 郵便番号
   */
  Exp_Lbl_ZipCode: '郵便番号',

  /**
   * @description [ja] アジアパシフィック向けのフライトの場合は、全社員ビジネスクラスの利用が可能
   */
  Exp_Msg_AirTravelPolicy2: 'アジアパシフィック向けのフライトの場合は、全社員ビジネスクラスの利用が可能',

  /**
   * @description [ja] ヨーロッパ向けフライトの場合は、マネージャ以上はビジネスクラス利用可能
   */
  Exp_Msg_AirTravelPolicy3: 'ヨーロッパ向けフライトの場合は、マネージャ以上はビジネスクラス利用可能',

  /**
   * @description [ja] 項目が選択されていません
   */
  Exp_Msg_AlertNoSelect: '項目が選択されていません',

  /**
   * @description [ja] 明細がコピーされました。
   */
  Exp_Msg_CloneRecords: '明細がコピーされました。',

  /**
   * @description [ja] 一度にコピー作成できるのは最大10明細までです。
   */
  Exp_Msg_CloneRecordsNumberError: '一度にコピー作成できるのは最大10明細までです。',

  /**
   * @description [ja] 金額が明細日付の[%1]によって再計算されました。
   */
  Exp_Msg_CloneRecordsRecalcution: '金額が明細日付の[%1]によって再計算されました。',

  /**
   * @description [ja] 為替レート
   */
  Exp_Msg_CloneRecordsRecalcutionExchangeRate: '為替レート',

  /**
   * @description [ja] 税率
   */
  Exp_Msg_CloneRecordsRecalcutionTaxRate: '税率',

  /**
   * @description [ja] 削除します。よろしいですか？
   */
  Exp_Msg_ConfirmDelete: '削除します。よろしいですか？',

  /**
   * @description [ja] 明細を削除してもよろしいですか？この操作は取り消せません。
   */
  Exp_Msg_ConfirmDeleteSelectedRecords: '明細を削除してもよろしいですか？この操作は取り消せません。',

  /**
   * @description [ja] [新規明細]ボタンをクリックして明細を作成してください。
   */
  Exp_Msg_CreateNewRecord: '[新規明細]ボタンをクリックして明細を作成してください。',

  /**
   * @description [ja] [申請から作成]ボタンをクリックして新しい申請を作成してください。
   */
  Exp_Msg_CreateNewReport: '[申請から作成]ボタンをクリックして新しい申請を作成してください。',

  /**
   * @description [ja] 金額が宿泊費の上限を超えています。
   */
  Exp_Msg_HotelExcess: '金額が宿泊費の上限を超えています。',

  /**
   * @description [ja] 主任の宿泊費上限は、
   */
  Exp_Msg_HotelLimitsOfDirector1: '主任の宿泊費上限は、',

  /**
   * @description [ja] となります。
   */
  Exp_Msg_HotelLimitsOfDirector2: 'となります。',

  /**
   * @description [ja] お困りでしたらシステム管理者へ問い合わせてください。
   */
  Exp_Msg_Inquire: 'お困りでしたらシステム管理者へ問い合わせてください。',

  /**
   * @description [ja] 「接待費」上限は、一人あたり
   */
  Exp_Msg_LimitsOfReception1: '「接待費」上限は、一人あたり',

  /**
   * @description [ja] となります。
   */
  Exp_Msg_LimitsOfReception2: 'となります。',

  /**
   * @description [ja] 現地金額と内訳が一致していません。
   */
  Exp_Msg_LocalAmountMismatchItems: '現地金額と内訳が一致していません。',

  /**
   * @description [ja] マネージャの１泊あたりの宿泊費上限（ローカルの税金、サービス料等含む）
   */
  Exp_Msg_LodgingPolicy1: 'マネージャの１泊あたりの宿泊費上限（ローカルの税金、サービス料等含む）',

  /**
   * @description [ja] ・シンガポール：USD200
   */
  Exp_Msg_LodgingPolicy2: '・シンガポール：USD200',

  /**
   * @description [ja] ・東京：USD180
   */
  Exp_Msg_LodgingPolicy3: '・東京：USD180',

  /**
   * @description [ja] 基準通貨が存在しないため
   */
  Exp_Msg_NoBaseCurrencyForExpense: '基準通貨が存在しないため',

  /**
   * @description [ja] 経費申請は利用できません
   */
  Exp_Msg_NoBaseCurrencyForExpenseSolution: '経費申請は利用できません',

  /**
   * @description [ja] 過去の申請はありません
   */
  Exp_Msg_NoPassedReportFound: '過去の申請はありません',

  /**
   * @description [ja] 経費精算を利用する権限が与えられていないようです
   */
  Exp_Msg_NoPermissionForExpense: '経費精算を利用する権限が与えられていないようです',

  /**
   * @description [ja] 領収書画像がありません。
   */
  Exp_Msg_NoRecipt: '領収書画像がありません。',

  /**
   * @description [ja] 明細は0件です
   */
  Exp_Msg_NoRecordItem: '明細は0件です',

  /**
   * @description [ja] 利用中の申請はありません
   */
  Exp_Msg_NoReportFound: '利用中の申請はありません',

  /**
   * @description [ja] 本人立替経費の支払日を指定してください。
   */
  Exp_Msg_PaymentDateOwnExpense: '本人立替経費の支払日を指定してください。',

  /**
   * @description [ja] １泊あたり
   */
  Exp_Msg_PerNight: '１泊あたり',

  /**
   * @description [ja] 費目：[%1]には内訳作成が必須です。内訳を設定してください。
   */
  Exp_Msg_RecordItemsMandatory: '費目：[%1]には内訳作成が必須です。内訳を設定してください。',

  /**
   * @description [ja] 事前申請から作成された経費申請のため、申請種別は変更できません。
   */
  Exp_Msg_ReportTypeInfo: '事前申請から作成された経費申請のため、申請種別は変更できません。',

  /**
   * @description [ja] 事前申請から作成された経費申請のため、申請種別は変更できません。
   */
  Exp_Msg_ReportTypeInfoForExpense: '事前申請から作成された経費申請のため、申請種別は変更できません。',

  /**
   * @description [ja] レポートタイプは社員のみが変更できます
   */
  Exp_Msg_ReportTypeInfoForFinanceApproval: 'レポートタイプは社員のみが変更できます',

  /**
   * @description [ja] 申請時の部署で検索
   */
  Exp_Msg_SearchByDepartment: '申請時の部署で検索',

  /**
   * @description [ja] 合計金額が一致しません。
   */
  Exp_Msg_TotalAmountMismatch: '合計金額が一致しません。',

  /**
   * @description [ja] 税込金額と内訳が一致していません。
   */
  Exp_Msg_TotalAmountMismatchItems: '税込金額と内訳が一致していません。',

  /**
   * @description [ja] 税抜金額と内訳が一致していません。
   */
  Exp_Msg_WithoutTaxAmountMismatchItems: '税抜金額と内訳が一致していません。',

  /**
   * @description [ja] 手当
   */
  Exp_Sel_Allowance: '手当',

  /**
   * @description [ja] 費目から自動設定
   */
  Exp_Sel_Auto_Select_From_Exp_Type: '費目から自動設定',

  /**
   * @description [ja] 当座預金口座
   */
  Exp_Sel_BankChecking: '当座預金口座',

  /**
   * @description [ja] 普通預金口座
   */
  Exp_Sel_BankSavings: '普通預金口座',

  /**
   * @description [ja] 出発日時基準
   */
  Exp_Sel_BaseDeparture: '出発日時基準',

  /**
   * @description [ja] 到着日時基準
   */
  Exp_Sel_BaseReturn: '到着日時基準',

  /**
   * @description [ja] 朝食
   */
  Exp_Sel_Breakfast: '朝食',

  /**
   * @description [ja] ビジネス
   */
  Exp_Sel_Business: 'ビジネス',

  /**
   * @description [ja] 現金
   */
  Exp_Sel_Cash: '現金',

  /**
   * @description [ja] 仮払い
   */
  Exp_Sel_CashAdvance: '仮払い',

  /**
   * @description [ja] 会議費
   */
  Exp_Sel_Conference: '会議費',

  /**
   * @description [ja] 夕食
   */
  Exp_Sel_Dinner: '夕食',

  /**
   * @description [ja] 国内出張手当
   */
  Exp_Sel_DomesticPerDiem: '国内出張手当',

  /**
   * @description [ja] 国内出張
   */
  Exp_Sel_DomesticTravel: '国内出張',

  /**
   * @description [ja] 内訳毎
   */
  Exp_Sel_EachBreakdown: '内訳毎',

  /**
   * @description [ja] エコノミー
   */
  Exp_Sel_Economy: 'エコノミー',

  /**
   * @description [ja] 駅探
   */
  Exp_Sel_Ekitan: '駅探',

  /**
   * @description [ja] 会議・交際費
   */
  Exp_Sel_Entertainment: '会議・交際費',

  /**
   * @description [ja] 経費申請
   */
  Exp_Sel_Expense: '経費申請',

  /**
   * @description [ja] ファースト
   */
  Exp_Sel_First: 'ファースト',

  /**
   * @description [ja] 金額固定
   */
  Exp_Sel_FixedAmount: '金額固定',

  /**
   * @description [ja] 金額固定（選択式)
   */
  Exp_Sel_FixedAmount_Mul: '金額固定（選択式)',

  /**
   * @description [ja] 航空券
   */
  Exp_Sel_Flight: '航空券',

  /**
   * @description [ja] 外貨固定
   */
  Exp_Sel_ForeginCurrency_Fixed: '外貨固定',

  /**
   * @description [ja] 外貨任意
   */
  Exp_Sel_ForeginCurrency_Flexible: '外貨任意',

  /**
   * @description [ja] 使用しない
   */
  Exp_Sel_ForeginCurrency_NotUsed: '使用しない',

  /**
   * @description [ja] 一般経費
   */
  Exp_Sel_GeneralExpense: '一般経費',

  /**
   * @description [ja] 一般経費(領収書不要)
   */
  Exp_Sel_GeneralExpenseHidden: '一般経費(領収書不要)',

  /**
   * @description [ja] 一般経費(領収書任意)
   */
  Exp_Sel_GeneralExpenseOptional: '一般経費(領収書任意)',

  /**
   * @description [ja] 宿泊費
   */
  Exp_Sel_Hotel: '宿泊費',

  /**
   * @description [ja] 宿泊費
   */
  Exp_Sel_HotelFee: '宿泊費',

  /**
   * @description [ja] 全国
   */
  Exp_Sel_JorudanAreaPreferenceOption00: '全国',

  /**
   * @description [ja] 関東
   */
  Exp_Sel_JorudanAreaPreferenceOption01: '関東',

  /**
   * @description [ja] 近畿
   */
  Exp_Sel_JorudanAreaPreferenceOption02: '近畿',

  /**
   * @description [ja] 北海道
   */
  Exp_Sel_JorudanAreaPreferenceOption03: '北海道',

  /**
   * @description [ja] 東北
   */
  Exp_Sel_JorudanAreaPreferenceOption04: '東北',

  /**
   * @description [ja] 東海
   */
  Exp_Sel_JorudanAreaPreferenceOption05: '東海',

  /**
   * @description [ja] 北陸
   */
  Exp_Sel_JorudanAreaPreferenceOption06: '北陸',

  /**
   * @description [ja] 中国
   */
  Exp_Sel_JorudanAreaPreferenceOption07: '中国',

  /**
   * @description [ja] 四国
   */
  Exp_Sel_JorudanAreaPreferenceOption08: '四国',

  /**
   * @description [ja] 九州
   */
  Exp_Sel_JorudanAreaPreferenceOption09: '九州',

  /**
   * @description [ja] のぞいた交通費を計算
   */
  Exp_Sel_JorudanCommuterPassOption0: 'のぞいた交通費を計算',

  /**
   * @description [ja] 考慮しない
   */
  Exp_Sel_JorudanCommuterPassOption1: '考慮しない',

  /**
   * @description [ja] 切符運賃
   */
  Exp_Sel_JorudanFareTypeOption0: '切符運賃',

  /**
   * @description [ja] IC運賃
   */
  Exp_Sel_JorudanFareTypeOption1: 'IC運賃',

  /**
   * @description [ja] おまかせ
   */
  Exp_Sel_JorudanHighwayBusOption0: 'おまかせ',

  /**
   * @description [ja] 使わない
   */
  Exp_Sel_JorudanHighwayBusOption1: '使わない',

  /**
   * @description [ja] 早い順
   */
  Exp_Sel_JorudanRouteSortOption0: '早い順',

  /**
   * @description [ja] 安い順
   */
  Exp_Sel_JorudanRouteSortOption1: '安い順',

  /**
   * @description [ja] 乗換回数順
   */
  Exp_Sel_JorudanRouteSortOption2: '乗換回数順',

  /**
   * @description [ja] 指定席
   */
  Exp_Sel_JorudanSeatPreferenceOption0: '指定席',

  /**
   * @description [ja] 自由席
   */
  Exp_Sel_JorudanSeatPreferenceOption1: '自由席',

  /**
   * @description [ja] グリーン席
   */
  Exp_Sel_JorudanSeatPreferenceOption2: 'グリーン席',

  /**
   * @description [ja] 利用する
   */
  Exp_Sel_JorudanUseChargedExpressOption0: '利用する',

  /**
   * @description [ja] 利用しない
   */
  Exp_Sel_JorudanUseChargedExpressOption1: '利用しない',

  /**
   * @description [ja] 一定距離以上の場合のみ利用
   */
  Exp_Sel_JorudanUseChargedExpressOption2: '一定距離以上の場合のみ利用',

  /**
   * @description [ja] 手配なし
   */
  Exp_Sel_NoBooking: '手配なし',

  /**
   * @description [ja] 領収書なし
   */
  Exp_Sel_NoRecipt: '領収書なし',

  /**
   * @description [ja] 不要
   */
  Exp_Sel_NoUsed: '不要',

  /**
   * @description [ja] 禁煙
   */
  Exp_Sel_NonSmoking: '禁煙',

  /**
   * @description [ja] 課税なし
   */
  Exp_Sel_Nontaxable: '課税なし',

  /**
   * @description [ja] 未登録
   */
  Exp_Sel_NotSelected: '未登録',

  /**
   * @description [ja] オペレータ手配
   */
  Exp_Sel_OperatorBooking: 'オペレータ手配',

  /**
   * @description [ja] 任意
   */
  Exp_Sel_Optional: '任意',

  /**
   * @description [ja] 大阪
   */
  Exp_Sel_Osaka: '大阪',

  /**
   * @description [ja] その他
   */
  Exp_Sel_Other: 'その他',

  /**
   * @description [ja] 海外出張手当
   */
  Exp_Sel_OverseasPerDiem: '海外出張手当',

  /**
   * @description [ja] 海外出張
   */
  Exp_Sel_OverseasTravel: '海外出張',

  /**
   * @description [ja] 先行手配
   */
  Exp_Sel_PreBooking: '先行手配',

  /**
   * @description [ja] 不要
   */
  Exp_Sel_ReceiptType_Hidden: '不要',

  /**
   * @description [ja] 不要
   */
  Exp_Sel_ReceiptType_NotUsed: '不要',

  /**
   * @description [ja] 任意
   */
  Exp_Sel_ReceiptType_Optional: '任意',

  /**
   * @description [ja] 必須
   */
  Exp_Sel_ReceiptType_Required: '必須',

  /**
   * @description [ja] 接待費
   */
  Exp_Sel_Reception: '接待費',

  /**
   * @description [ja] 交際費
   */
  Exp_Sel_RecordEntertainment: '交際費',

  /**
   * @description [ja] レンタカー
   */
  Exp_Sel_RentCar: 'レンタカー',

  /**
   * @description [ja] 必須
   */
  Exp_Sel_Required: '必須',

  /**
   * @description [ja] 指定席
   */
  Exp_Sel_ReservedSeat: '指定席',

  /**
   * @description [ja] シンガポール
   */
  Exp_Sel_Singapore: 'シンガポール',

  /**
   * @description [ja] 喫煙
   */
  Exp_Sel_Smoking: '喫煙',

  /**
   * @description [ja] 課税
   */
  Exp_Sel_Tax: '課税',

  /**
   * @description [ja] 東京
   */
  Exp_Sel_Tokyo: '東京',

  /**
   * @description [ja] 総額
   */
  Exp_Sel_TotalAmount: '総額',

  /**
   * @description [ja] 鉄道
   */
  Exp_Sel_Train: '鉄道',

  /**
   * @description [ja] 交通費(ジョルダン日本)
   */
  Exp_Sel_TransitJorudanJP: '交通費(ジョルダン日本)',

  /**
   * @description [ja] 交通費
   */
  Exp_Sel_Transportation: '交通費',

  /**
   * @description [ja] 経理承認済み
   */
  Exp_Status_AccountingAuthorized: '経理承認済み',

  /**
   * @description [ja] 経理却下
   */
  Exp_Status_AccountingRejected: '経理却下',

  /**
   * @description [ja] 承認済み事前申請
   */
  Exp_Status_ApprovedPreRequest: '承認済み事前申請',

  /**
   * @description [ja] 経費申請済み
   */
  Exp_Status_Claimed: '経費申請済み',

  /**
   * @description [ja] 支払い済み
   */
  Exp_Status_FullyPaid: '支払い済み',

  /**
   * @description [ja] 仕訳データ作成済み
   */
  Exp_Status_JournalCreated: '仕訳データ作成済み',

  /**
   * @description [ja] この日の勤怠情報がありません。
   */
  Exp_Warn_AttRecordNotFound: 'この日の勤怠情報がありません。',

  /**
   * @description [ja] この日は欠勤です。
   */
  Exp_Warn_DateIsAbsence: 'この日は欠勤です。',

  /**
   * @description [ja] この日は休日です。
   */
  Exp_Warn_DateIsHoliday: 'この日は休日です。',

  /**
   * @description [ja] この日は休暇を取得しています。
   */
  Exp_Warn_DateIsLeave: 'この日は休暇を取得しています。',

  /**
   * @description [ja] この日は休職・休業期間内です。
   */
  Exp_Warn_DateIsLeaveOfAbsence: 'この日は休職・休業期間内です。',

  /**
   * @description [ja] この日は法定休日です。
   */
  Exp_Warn_DateIsLegalHoliday: 'この日は法定休日です。',

  /**
   * @description [ja] この日の勤怠情報が正しくない可能性があります
   */
  Exp_Warn_IllegalAttCalculation: 'この日の勤怠情報が正しくない可能性があります',

  /**
   * @description [ja] 次の費目は選択されている申請種別では使用できません。:
   */
  Exp_Warn_InvalidRecordExpenseTypeForReportTypeMobile: '次の費目は選択されている申請種別では使用できません。:',

  /**
   * @description [ja] 内訳追加
   */
  Exp_btn_AddRecordItem: '内訳追加',

  /**
   * @description [ja] エラーが発生しました。通信状態のよい場所で再度やり直してください。
   */
  Mobile_Err_ConnectionError: 'エラーが発生しました。通信状態のよい場所で再度やり直してください。',

  /**
   * @description [ja] 認証エラー
   */
  Oauth_Err_CalendarAccessAuthError: '認証エラー',

  /**
   * @description [ja] 認証情報の取得に失敗しました。認証がキャンセルされたか、このページに直接遷移した可能性があります。
   */
  Oauth_Msg_CalendarAccessAuthErrorNoParam: '認証情報の取得に失敗しました。認証がキャンセルされたか、このページに直接遷移した可能性があります。',

  /**
   * @description [ja] このページを閉じ、認証を再度やり直してください。
   */
  Oauth_Msg_CalendarAccessAuthErrorNoParamSolution: 'このページを閉じ、認証を再度やり直してください。',

  /**
   * @description [ja] 外部カレンダー連携の認証が成功しました。
   */
  Oauth_Msg_CalendarAccessAuthSuccess: '外部カレンダー連携の認証が成功しました。',

  /**
   * @description [ja] このページは閉じてください。
   */
  Oauth_Msg_CalendarAccessAuthSuccessRemarks: 'このページは閉じてください。',

  /**
   * @description [ja] 部署
   */
  Team_Lbl_Department: '部署',

  /**
   * @description [ja] 社員コード
   */
  Team_Lbl_EmployeeCode: '社員コード',

  /**
   * @description [ja] 社員名
   */
  Team_Lbl_EmployeeName: '社員名',

  /**
   * @description [ja] 所属部署なし
   */
  Team_Lbl_EmptyDepartment: '所属部署なし',

  /**
   * @description [ja] 勤務表を開く
   */
  Team_Lbl_OpenTimesheet: '勤務表を開く',

  /**
   * @description [ja] 指定した条件での社員が見つかりません。
   */
  Team_Msg_EmptyEmployeeList: '指定した条件での社員が見つかりません。',

  /**
   * @description [ja] 承認
   */
  Time_Action_Approve: '承認',

  /**
   * @description [ja] 承認取消
   */
  Time_Action_CancelApproval: '承認取消',

  /**
   * @description [ja] 申請取消
   */
  Time_Action_Recall: '申請取消',

  /**
   * @description [ja] 却下
   */
  Time_Action_Reject: '却下',

  /**
   * @description [ja] 承認申請
   */
  Time_Action_Submit: '承認申請',

  /**
   * @description [ja] 現在の工数期間
   */
  Time_Btn_CurrentPeriod: '現在の工数期間',

  /**
   * @description [ja] 承認済み工数確定を取消しました
   */
  Time_Lbl_CancelApprovalSucceeded: '承認済み工数確定を取消しました',

  /**
   * @description [ja] ジョブ/作業分類
   */
  Time_Lbl_JobName: 'ジョブ/作業分類',

  /**
   * @description [ja] 工数確定を取消しました
   */
  Time_Lbl_RecallSucceeded: '工数確定を取消しました',

  /**
   * @description [ja] 工数確定を承認申請しました
   */
  Time_Lbl_SubmitSucceeded: '工数確定を承認申請しました',

  /**
   * @description [ja] 工数実績サマリー
   */
  Time_Lbl_TrackSummaryTitle: '工数実績サマリー',

  /**
   * @description [ja] 作業時間
   */
  Time_Lbl_WorkingHours: '作業時間',

  /**
   * @description [ja] 作業時間割合
   */
  Time_Lbl_WorkingPercentage: '作業時間割合',

  /**
   * @description [ja] 承認済み
   */
  Time_Status_Approved: '承認済み',

  /**
   * @description [ja] 承認取消
   */
  Time_Status_Canceled: '承認取消',

  /**
   * @description [ja] 未申請
   */
  Time_Status_NotRequested: '未申請',

  /**
   * @description [ja] 承認待ち
   */
  Time_Status_Pending: '承認待ち',

  /**
   * @description [ja] 申請取消
   */
  Time_Status_Recalled: '申請取消',

  /**
   * @description [ja] 却下
   */
  Time_Status_Rejected: '却下',

  /**
   * @description [ja] 申請取消
   */
  Time_Status_Removed: '申請取消',

  /**
   * @description [ja] ジョブを追加
   */
  Trac_Lbl_AddJob: 'ジョブを追加',

  /**
   * @description [ja] 申請取消
   */
  Trac_Lbl_Cancel: '申請取消',

  /**
   * @description [ja] コメント
   */
  Trac_Lbl_Comments: 'コメント',

  /**
   * @description [ja] 工数入力
   */
  Trac_Lbl_DailyTimeTrack: '工数入力',

  /**
   * @description [ja] 日付
   */
  Trac_Lbl_Date: '日付',

  /**
   * @description [ja] ジョブ
   */
  Trac_Lbl_Job: 'ジョブ',

  /**
   * @description [ja] 次の承認者
   */
  Trac_Lbl_NextApproverEmployee: '次の承認者',

  /**
   * @description [ja] 実労働
   */
  Trac_Lbl_RealWorkTime: '実労働',

  /**
   * @description [ja] 承認申請
   */
  Trac_Lbl_Request: '承認申請',

  /**
   * @description [ja] 工数実績
   */
  Trac_Lbl_TimeTrack: '工数実績',

  /**
   * @description [ja] 合計
   */
  Trac_Lbl_Total: '合計',

  /**
   * @description [ja] 記録済
   */
  Trac_Lbl_Tracked: '記録済',

  /**
   * @description [ja] 作業
   */
  Trac_Lbl_Work: '作業',

  /**
   * @description [ja] 作業分類
   */
  Trac_Lbl_WorkCategory: '作業分類',

  /**
   * @description [ja] 工数時間
   */
  Trac_Lbl_WorkTime: '工数時間',

  /**
   * @description [ja] 工数グラフ
   */
  Trac_Lbl_WorkTimeGraph: '工数グラフ',
  ['']: '',
};

export default ja;
