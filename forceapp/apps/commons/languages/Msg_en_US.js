/* @flow */

const en_US = {
  /**
   * @description [en_US] Direct Attendance
   */
  $Att_Lbl_RequestTypeDirect: 'Direct Attendance',

  /**
   * @description [en_US] Accounting Period
   */
  $Exp_Clbl_AccountingPeriod: 'Accounting Period',

  /**
   * @description [en_US] Amount
   */
  $Exp_Clbl_Amount: 'Amount',

  /**
   * @description [en_US] Cost Center
   */
  $Exp_Clbl_CostCenter: 'Cost Center',

  /**
   * @description [en_US] Currency
   */
  $Exp_Clbl_Currency: 'Currency',

  /**
   * @description [en_US] Date
   */
  $Exp_Clbl_Date: 'Date',

  /**
   * @description [en_US] Exchange Rate
   */
  $Exp_Clbl_ExchangeRate: 'Exchange Rate',

  /**
   * @description [en_US] Expense Type
   */
  $Exp_Clbl_ExpenseType: 'Expense Type',

  /**
   * @description [en_US] Tax
   */
  $Exp_Clbl_Gst: 'Tax',

  /**
   * @description [en_US] Tax Amount
   */
  $Exp_Clbl_GstAmount: 'Tax Amount',

  /**
   * @description [en_US] Tax Rate
   */
  $Exp_Clbl_GstRate: 'Tax Rate',

  /**
   * @description [en_US] Amount (incl. Tax)
   */
  $Exp_Clbl_IncludeTax: 'Amount (incl. Tax)',

  /**
   * @description [en_US] Local Amount
   */
  $Exp_Clbl_LocalAmount: 'Local Amount',

  /**
   * @description [en_US] New Report
   */
  $Exp_Clbl_NewReport: 'New Report',

  /**
   * @description [en_US] Purpose
   */
  $Exp_Clbl_Purpose: 'Purpose',

  /**
   * @description [en_US] Record Date
   */
  $Exp_Clbl_RecordDate: 'Record Date',

  /**
   * @description [en_US] Remarks
   */
  $Exp_Clbl_ReportRemarks: 'Remarks',

  /**
   * @description [en_US] Title
   */
  $Exp_Clbl_ReportTitle: 'Title',

  /**
   * @description [en_US] Report Type
   */
  $Exp_Clbl_ReportType: 'Report Type',

  /**
   * @description [en_US] Title
   */
  $Exp_Clbl_RequestTitle: 'Title',

  /**
   * @description [en_US] Scheduled Date
   */
  $Exp_Clbl_ScheduledDate: 'Scheduled Date',

  /**
   * @description [en_US] Summary
   */
  $Exp_Clbl_Summary: 'Summary',

  /**
   * @description [en_US] Type
   */
  $Exp_Clbl_Type: 'Type',

  /**
   * @description [en_US] Amount (excl. Tax)
   */
  $Exp_Clbl_WithoutTax: 'Amount (excl. Tax)',

  /**
   * @description [en_US] Allow to change approver of work arrangement request / monthly request to other employees who have Approval Authority 01.
   */
  Admin_Help_AllowToChangeApproverHelp: 'Allow to change approver of work arrangement request / monthly request to other employees who have Approval Authority 01.',

  /**
   * @description [en_US] If the work scheme allows requester to change approver, the employees who have this authority can be approvers.
   */
  Admin_Help_ApprovalAuthority01: 'If the work scheme allows requester to change approver, the employees who have this authority can be approvers.',

  /**
   * @description [en_US] Enter name or code
   */
  Admin_Help_AutoSuggest: 'Enter name or code',

  /**
   * @description [en_US] Enter name or user name
   */
  Admin_Help_AutoSuggestSFUser: 'Enter name or user name',

  /**
   * @description [en_US] If the attendance end time is stamped from 0:00 through the boundary time, the stamp is considered as of yesterday after 24:00, instead of today. If no value is set, it will be treated as 05:00.
   */
  Admin_Help_BoundaryOfEndTime: 'If the attendance end time is stamped from 0:00 through the boundary time, the stamp is considered as of yesterday after 24:00, instead of today. If no value is set, it will be treated as 05:00.',

  /**
   * @description [en_US] If the attendance start time is stamped from 0:00 to the boundary time, the stamp is considered as of yesterday after 24:00, instead of today. If no value is set, it will be treated as 05:00.
   */
  Admin_Help_BoundaryOfStartTime: 'If the attendance start time is stamped from 0:00 to the boundary time, the stamp is considered as of yesterday after 24:00, instead of today. If no value is set, it will be treated as 05:00.',

  /**
   * @description [en_US] Enter code for external system linkage
   */
  Admin_Help_CostCenterCodeDescription: 'Enter code for external system linkage',

  /**
   * @description [en_US] Set the default attendance time for {$Att_Lbl_RequestTypeDirect} request
   */
  Admin_Help_DirectRequestDefaultAttendanceTime: 'Set the default attendance time for {$Att_Lbl_RequestTypeDirect} request',

  /**
   * @description [en_US] Set when you set the time slot accepted as working hours just after end time or flex hours without request.
   */
  Admin_Help_EndTimeOfAcceptAsWorkingHour: 'Set when you set the time slot accepted as working hours just after end time or flex hours without request.',

  /**
   * @description [en_US] input will be displayed in Expenses and Requests from
   */
  Admin_Help_ExpExtendedItem: 'input will be displayed in Expenses and Requests from',

  /**
   * @description [en_US] input will be displayed in Expense Selection Dialog
   */
  Admin_Help_ExpTypeDescription: 'input will be displayed in Expense Selection Dialog',

  /**
   * @description [en_US] In accending order, expense type will be displayed in Expense Selection Dialog
   */
  Admin_Help_ExpTypeOrder: 'In accending order, expense type will be displayed in Expense Selection Dialog',

  /**
   * @description [en_US] Set parent group which organize expense type and group in a hierarchial manner in expense Selection Dialog
   */
  Admin_Help_ExpTypeParent: 'Set parent group which organize expense type and group in a hierarchial manner in expense Selection Dialog',

  /**
   * @description [en_US] input can be changed according to selected record type
   */
  Admin_Help_ExpTypeRecordType: 'input can be changed according to selected record type',

  /**
   * @description [en_US] Set tax type of expense type
   */
  Admin_Help_ExpTypeTaxType: 'Set tax type of expense type',

  /**
   * @description [en_US] This amount list will be displayed on Expenses and Requests form.
   */
  Admin_Help_FixedAmountMultiple: 'This amount list will be displayed on Expenses and Requests form.',

  /**
   * @description [en_US] This amount will be set in the amount field. Users can’t change this amount
   */
  Admin_Help_FixedAmountSingle: 'This amount will be set in the amount field. Users can’t change this amount',

  /**
   * @description [en_US] Multiple selection can be made by pressing Shift or command / ctrl key.
   */
  Admin_Help_LeaveCodeList: 'Multiple selection can be made by pressing Shift or command / ctrl key.',

  /**
   * @description [en_US] The \'Reason\' field will be shown instead of the \'Remarks\' field and be required in the leave request.
   */
  Admin_Help_LeaveRequireReason: 'The \'Reason\' field will be shown instead of the \'Remarks\' field and be required in the leave request.',

  /**
   * @description [en_US] Help Message.
   */
  Admin_Help_Name: 'Help Message.',

  /**
   * @description [en_US] This hint is only displayed on Expenses
   */
  Admin_Help_OnlyExpense: 'This hint is only displayed on Expenses',

  /**
   * @description [en_US] This hint is only displayed on Request
   */
  Admin_Help_OnlyRequest: 'This hint is only displayed on Request',

  /**
   * @description [en_US] If enabled, employees can send their locations on the mobile time-stamp page. (The message is required when they cannot or do not send their locations)
   */
  Admin_Help_RequireLocationAtMobileStamp: 'If enabled, employees can send their locations on the mobile time-stamp page. (The message is required when they cannot or do not send their locations)',

  /**
   * @description [en_US] Set when you set the time slot accepted as working hours just before start time or flex hours without request.
   */
  Admin_Help_StartTimeOfAcceptAsWorkingHour: 'Set when you set the time slot accepted as working hours just before start time or flex hours without request.',

  /**
   * @description [en_US] Multiple selection can be made by pressing Shift or command / ctrl key.
   */
  Admin_Help_WorkPatternCodeList: 'Multiple selection can be made by pressing Shift or command / ctrl key.',

  /**
   * @description [en_US] 36 kyotei
   */
  Admin_Lbl_36kyotei: '36 kyotei',

  /**
   * @description [en_US] Am half day leave
   */
  Admin_Lbl_AMHalfDayLeave: 'Am half day leave',

  /**
   * @description [en_US] Absence Request
   */
  Admin_Lbl_AbsenceRequest: 'Absence Request',

  /**
   * @description [en_US] Access Permission
   */
  Admin_Lbl_AccessPermission: 'Access Permission',

  /**
   * @description [en_US] Access Permission Management
   */
  Admin_Lbl_AccessPermissionManagement: 'Access Permission Management',

  /**
   * @description [en_US] Active
   */
  Admin_Lbl_Active: 'Active',

  /**
   * @description [en_US] Actor
   */
  Admin_Lbl_Actor: 'Actor',

  /**
   * @description [en_US] Add Expense Type
   */
  Admin_Lbl_AddExpenseType: 'Add Expense Type',

  /**
   * @description [en_US] Administrator
   */
  Admin_Lbl_Administrator: 'Administrator',

  /**
   * @description [en_US] Allow
   */
  Admin_Lbl_Admit: 'Allow',

  /**
   * @description [en_US] Agreement Alert Settings
   */
  Admin_Lbl_AgreementAlertSetting: 'Agreement Alert Settings',

  /**
   * @description [en_US] Valid From
   */
  Admin_Lbl_AgreementAlertSettingValidDateFrom: 'Valid From',

  /**
   * @description [en_US] Valid To
   */
  Admin_Lbl_AgreementAlertSettingValidDateTo: 'Valid To',

  /**
   * @description [en_US] Allow requester to change approver of their request
   */
  Admin_Lbl_AllowRequesterToChangeApprover: 'Allow requester to change approver of their request',

  /**
   * @description [en_US] Allow to Modify Tax Amount
   */
  Admin_Lbl_AllowTaxAmountChange: 'Allow to Modify Tax Amount',

  /**
   * @description [en_US] Changing Approver
   */
  Admin_Lbl_AllowToChangeApprover: 'Changing Approver',

  /**
   * @description [en_US] Allow to work on time of half day leave
   */
  Admin_Lbl_AllowToWorkOnHalfDayLeaveTime: 'Allow to work on time of half day leave',

  /**
   * @description [en_US] Allowable Time of Short Time Work
   */
  Admin_Lbl_AllowableTimeOfShortTimeWork: 'Allowable Time of Short Time Work',

  /**
   * @description [en_US] Allowable Time of Short Time Work(Contracted Work Hours)
   */
  Admin_Lbl_AllowableTimeOfShortTimeWorkForFlex: 'Allowable Time of Short Time Work(Contracted Work Hours)',

  /**
   * @description [en_US] Allowable Time of Short Time Work(Core Time)
   */
  Admin_Lbl_AllowableTimeOfShortTimeWorkInCoreTime: 'Allowable Time of Short Time Work(Core Time)',

  /**
   * @description [en_US] Amount
   */
  Admin_Lbl_Amount: 'Amount',

  /**
   * @description [en_US] Amount List
   */
  Admin_Lbl_AmountList: 'Amount List',

  /**
   * @description [en_US] Granted Annual Paid Leave(s)
   */
  Admin_Lbl_AnnualPaidLeaveGrantHistoryOfValid: 'Granted Annual Paid Leave(s)',

  /**
   * @description [en_US] Application Options
   */
  Admin_Lbl_AppOptions: 'Application Options',

  /**
   * @description [en_US] Apply
   */
  Admin_Lbl_Apply: 'Apply',

  /**
   * @description [en_US] Approval Authority 01
   */
  Admin_Lbl_ApprovalAuthority01: 'Approval Authority 01',

  /**
   * @description [en_US] Approval Process
   */
  Admin_Lbl_ApprovalProcess: 'Approval Process',

  /**
   * @description [en_US] Approval Setting
   */
  Admin_Lbl_ApprovalSetting: 'Approval Setting',

  /**
   * @description [en_US] Approver 01
   */
  Admin_Lbl_Approver01Name: 'Approver 01',

  /**
   * @description [en_US] Work Pattern
   */
  Admin_Lbl_AttPattern: 'Work Pattern',

  /**
   * @description [en_US] Working time change request
   */
  Admin_Lbl_AttPatternApplyRequest: 'Working time change request',

  /**
   * @description [en_US] Boundary end time of day
   */
  Admin_Lbl_AttPatternBoundaryEndTimeOfDay: 'Boundary end time of day',

  /**
   * @description [en_US] Boundary start time of day
   */
  Admin_Lbl_AttPatternBoundaryStartTimeOfDay: 'Boundary start time of day',

  /**
   * @description [en_US] Work Pattern
   */
  Admin_Lbl_AttPatternCodeList: 'Work Pattern',

  /**
   * @description [en_US] Apply Work Pattern
   */
  Admin_Lbl_AttPatternEmployeeBatch: 'Apply Work Pattern',

  /**
   * @description [en_US] CSV File
   */
  Admin_Lbl_AttPatternEmployeeBatchCsvFile: 'CSV File',

  /**
   * @description [en_US] Drag and drop CSV files or select
   */
  Admin_Lbl_AttPatternEmployeeBatchDragAndDrop: 'Drag and drop CSV files or select',

  /**
   * @description [en_US] Day type
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderDayType: 'Day type',

  /**
   * @description [en_US] Employee code
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderEmployeeCode: 'Employee code',

  /**
   * @description [en_US] ErrorDetail
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderErrorDetail: 'ErrorDetail',

  /**
   * @description [en_US] Pattern code
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderPatternCode: 'Pattern code',

  /**
   * @description [en_US] Status
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderStatus: 'Status',

  /**
   * @description [en_US] Target date
   */
  Admin_Lbl_AttPatternEmployeeBatchFetchResultHeaderTargetDate: 'Target date',

  /**
   * @description [en_US] Working pattern options
   */
  Admin_Lbl_AttPatternOptions: 'Working pattern options',

  /**
   * @description [en_US] Attendance Management
   */
  Admin_Lbl_AttendanceManagement: 'Attendance Management',

  /**
   * @description [en_US] Base Propeties
   */
  Admin_Lbl_Base: 'Base Propeties',

  /**
   * @description [en_US] Base Info
   */
  Admin_Lbl_BaseInfo: 'Base Info',

  /**
   * @description [en_US] Begin Base Day
   */
  Admin_Lbl_BeginBaseDay: 'Begin Base Day',

  /**
   * @description [en_US] Begin Base Month
   */
  Admin_Lbl_BeginBaseMonth: 'Begin Base Month',

  /**
   * @description [en_US] BothHoliday
   */
  Admin_Lbl_BothHoliday: 'BothHoliday',

  /**
   * @description [en_US] Calendar
   */
  Admin_Lbl_Calendar: 'Calendar',

  /**
   * @description [en_US] Connect with External Calendar
   */
  Admin_Lbl_CalendarAccess: 'Connect with External Calendar',

  /**
   * @description [en_US] Authorize
   */
  Admin_Lbl_CalendarAccessAuthButton: 'Authorize',

  /**
   * @description [en_US] Reauthorize
   */
  Admin_Lbl_CalendarAccessAuthButtonReauthorize: 'Reauthorize',

  /**
   * @description [en_US] Show Settings
   */
  Admin_Lbl_CalendarAccessAuthOpenRemoteSiteSetting: 'Show Settings',

  /**
   * @description [en_US] Failed to access the external calendar service.
   */
  Admin_Lbl_CalendarAccessAuthStatusApiConnectionFailed: 'Failed to access the external calendar service.',

  /**
   * @description [en_US] Authorized
   */
  Admin_Lbl_CalendarAccessAuthStatusAuthorized: 'Authorized',

  /**
   * @description [en_US] Remote Site Settings are inactive.
   */
  Admin_Lbl_CalendarAccessAuthStatusRemoteSiteInactive: 'Remote Site Settings are inactive.',

  /**
   * @description [en_US] Unauthorized
   */
  Admin_Lbl_CalendarAccessAuthStatusUnauthorized: 'Unauthorized',

  /**
   * @description [en_US] Unauthorized (Save settings to start authorization)
   */
  Admin_Lbl_CalendarAccessAuthStatusUnsaved: 'Unauthorized (Save settings to start authorization)',

  /**
   * @description [en_US] Authorization
   */
  Admin_Lbl_CalendarAccessAuthorization: 'Authorization',

  /**
   * @description [en_US] Enable
   */
  Admin_Lbl_CalendarAccessEnable: 'Enable',

  /**
   * @description [en_US] Connection
   */
  Admin_Lbl_CalendarAccessFunction: 'Connection',

  /**
   * @description [en_US] Search Candidates
   */
  Admin_Lbl_CandidateSearch: 'Search Candidates',

  /**
   * @description [en_US] Change Period
   */
  Admin_Lbl_ChangePeriod: 'Change Period',

  /**
   * @description [en_US] Code
   */
  Admin_Lbl_Code: 'Code',

  /**
   * @description [en_US] Cost Center Required For
   */
  Admin_Lbl_ComCostCenterRequiredFor: 'Cost Center Required For',

  /**
   * @description [en_US] Cost Center Used In
   */
  Admin_Lbl_ComCostCenterUsedIn: 'Cost Center Used In',

  /**
   * @description [en_US] Job Required For
   */
  Admin_Lbl_ComJobRequiredFor: 'Job Required For',

  /**
   * @description [en_US] Job Used In
   */
  Admin_Lbl_ComJobUsedIn: 'Job Used In',

  /**
   * @description [en_US] Comment
   */
  Admin_Lbl_Comment: 'Comment',

  /**
   * @description [en_US] Common Master
   */
  Admin_Lbl_CommonMaster: 'Common Master',

  /**
   * @description [en_US] Common Settings
   */
  Admin_Lbl_CommonSettings: 'Common Settings',

  /**
   * @description [en_US] This route does not have corresponding commuter route. Please try other route.
   */
  Admin_Lbl_CommuterNoSearchResult: 'This route does not have corresponding commuter route. Please try other route.',

  /**
   * @description [en_US] Commuter Pass Settings
   */
  Admin_Lbl_CommuterPassSetting: 'Commuter Pass Settings',

  /**
   * @description [en_US] Reset Search
   */
  Admin_Lbl_CommuterResetButton: 'Reset Search',

  /**
   * @description [en_US] Company
   */
  Admin_Lbl_Company: 'Company',

  /**
   * @description [en_US] Company Calendar
   */
  Admin_Lbl_CompanyCalendar: 'Company Calendar',

  /**
   * @description [en_US] Company
   */
  Admin_Lbl_CompanyCalendar_Abbr: 'Company',

  /**
   * @description [en_US] Company Code
   */
  Admin_Lbl_CompanyCode: 'Company Code',

  /**
   * @description [en_US] Company Name
   */
  Admin_Lbl_CompanyName: 'Company Name',

  /**
   * @description [en_US] General
   */
  Admin_Lbl_Company_General: 'General',

  /**
   * @description [en_US] Core Period
   */
  Admin_Lbl_CoreTime: 'Core Period',

  /**
   * @description [en_US] Cost Center
   */
  Admin_Lbl_CostCenter: 'Cost Center',

  /**
   * @description [en_US] External System Linkage Code
   */
  Admin_Lbl_CostCenterLinkageCode: 'External System Linkage Code',

  /**
   * @description [en_US] Cost Center Name
   */
  Admin_Lbl_CostCenterName: 'Cost Center Name',

  /**
   * @description [en_US] Count
   */
  Admin_Lbl_Count: 'Count',

  /**
   * @description [en_US] Count Applied Error Number
   */
  Admin_Lbl_CountAppliedErrorNumber: 'Count Applied Error Number',

  /**
   * @description [en_US] Count Number
   */
  Admin_Lbl_CountAppliedNumber: 'Count Number',

  /**
   * @description [en_US] Count Applied Success Number
   */
  Admin_Lbl_CountAppliedSuccessNumber: 'Count Applied Success Number',

  /**
   * @description [en_US] Count File Number
   */
  Admin_Lbl_CountFileNmber: 'Count File Number',

  /**
   * @description [en_US] Country
   */
  Admin_Lbl_Country: 'Country',

  /**
   * @description [en_US] Any rows are not found in the uploaded CSV file.
   */
  Admin_Lbl_CsvEmptyError: 'Any rows are not found in the uploaded CSV file.',

  /**
   * @description [en_US] Add one or more work pattern into the CSV file.
   */
  Admin_Lbl_CsvEmptyErrorSolution: 'Add one or more work pattern into the CSV file.',

  /**
   * @description [en_US] The column length in the uploaded CSV file is incorrect.
   */
  Admin_Lbl_CsvFormatError: 'The column length in the uploaded CSV file is incorrect.',

  /**
   * @description [en_US] Fix the csv file, and upload it again.
   */
  Admin_Lbl_CsvFormatErrorSolution: 'Fix the csv file, and upload it again.',

  /**
   * @description [en_US] The uploaded CSV file exceeded limits of row count.
   */
  Admin_Lbl_CsvRowsLimitExceeded: 'The uploaded CSV file exceeded limits of row count.',

  /**
   * @description [en_US] Please upload CSV file whose the number of data to 5000 or less..
   */
  Admin_Lbl_CsvRowsLimitExceededSolution: 'Please upload CSV file whose the number of data to 5000 or less..',

  /**
   * @description [en_US] Currency
   */
  Admin_Lbl_Currency: 'Currency',

  /**
   * @description [en_US] Currency Code
   */
  Admin_Lbl_CurrencyCode: 'Currency Code',

  /**
   * @description [en_US] Currency Name
   */
  Admin_Lbl_CurrencyName: 'Currency Name',

  /**
   * @description [en_US] Currency Pair
   */
  Admin_Lbl_CurrencyPair: 'Currency Pair',

  /**
   * @description [en_US] Currency Symbol
   */
  Admin_Lbl_CurrencySymbol: 'Currency Symbol',

  /**
   * @description [en_US] Custom Extended Item
   */
  Admin_Lbl_CustomExtendedItem: 'Custom Extended Item',

  /**
   * @description [en_US] Day Type
   */
  Admin_Lbl_DayType: 'Day Type',

  /**
   * @description [en_US] Days to Grant
   */
  Admin_Lbl_DaysGranted: 'Days to Grant',

  /**
   * @description [en_US] Days to Managed.
   */
  Admin_Lbl_DaysManaged: 'Days to Managed.',

  /**
   * @description [en_US] Debit Account Code
   */
  Admin_Lbl_DebitAccountCode: 'Debit Account Code',

  /**
   * @description [en_US] Debit Account Name
   */
  Admin_Lbl_DebitAccountName: 'Debit Account Name',

  /**
   * @description [en_US] Debit Sub Account Code
   */
  Admin_Lbl_DebitSubAccountCode: 'Debit Sub Account Code',

  /**
   * @description [en_US] Debit Sub Account Name
   */
  Admin_Lbl_DebitSubAccountName: 'Debit Sub Account Name',

  /**
   * @description [en_US] Decimal Places
   */
  Admin_Lbl_DecimalPlaces: 'Decimal Places',

  /**
   * @description [en_US] Deemed over time hours
   */
  Admin_Lbl_DeemedOvertimeHours: 'Deemed over time hours',

  /**
   * @description [en_US] Deemed work hours
   */
  Admin_Lbl_DeemedWorkHours: 'Deemed work hours',

  /**
   * @description [en_US] Default
   */
  Admin_Lbl_Default: 'Default',

  /**
   * @description [en_US] Default Language
   */
  Admin_Lbl_DefaultLanguage: 'Default Language',

  /**
   * @description [en_US] Default Value
   */
  Admin_Lbl_DefaultValue: 'Default Value',

  /**
   * @description [en_US] Manager
   */
  Admin_Lbl_DeparmentManagerName: 'Manager',

  /**
   * @description [en_US] Department
   */
  Admin_Lbl_Department: 'Department',

  /**
   * @description [en_US] Department Code
   */
  Admin_Lbl_DepartmentCode: 'Department Code',

  /**
   * @description [en_US] Department Name
   */
  Admin_Lbl_DepartmentName: 'Department Name',

  /**
   * @description [en_US] Description
   */
  Admin_Lbl_Description: 'Description',

  /**
   * @description [en_US] {$Att_Lbl_RequestTypeDirect} Request
   */
  Admin_Lbl_DirectApplyRequest: '{$Att_Lbl_RequestTypeDirect} Request',

  /**
   * @description [en_US] Rest1(default)
   */
  Admin_Lbl_DirectApplyRestTime1: 'Rest1(default)',

  /**
   * @description [en_US] Rest2(default)
   */
  Admin_Lbl_DirectApplyRestTime2: 'Rest2(default)',

  /**
   * @description [en_US] Rest3(default)
   */
  Admin_Lbl_DirectApplyRestTime3: 'Rest3(default)',

  /**
   * @description [en_US] Rest4(default)
   */
  Admin_Lbl_DirectApplyRestTime4: 'Rest4(default)',

  /**
   * @description [en_US] Rest5(default)
   */
  Admin_Lbl_DirectApplyRestTime5: 'Rest5(default)',

  /**
   * @description [en_US] Attendance time(default)
   */
  Admin_Lbl_DirectApplyTime: 'Attendance time(default)',

  /**
   * @description [en_US] Direct Charged
   */
  Admin_Lbl_DirectCharged: 'Direct Charged',

  /**
   * @description [en_US] Download
   */
  Admin_Lbl_Download: 'Download',

  /**
   * @description [en_US] Early Start Work Request
   */
  Admin_Lbl_EarlyStartWorkRequest: 'Early Start Work Request',

  /**
   * @description [en_US] Edit
   */
  Admin_Lbl_Edit: 'Edit',

  /**
   * @description [en_US] Editing
   */
  Admin_Lbl_Editing: 'Editing',

  /**
   * @description [en_US] Employee
   */
  Admin_Lbl_Employee: 'Employee',

  /**
   * @description [en_US] Employee Assignment
   */
  Admin_Lbl_EmployeeAssignment: 'Employee Assignment',

  /**
   * @description [en_US] Employee Grade
   */
  Admin_Lbl_EmployeeGrade: 'Employee Grade',

  /**
   * @description [en_US] Employee Group
   */
  Admin_Lbl_EmployeeGroup: 'Employee Group',

  /**
   * @description [en_US] End
   */
  Admin_Lbl_End: 'End',

  /**
   * @description [en_US] End Base Day
   */
  Admin_Lbl_EndBaseDay: 'End Base Day',

  /**
   * @description [en_US] End Base Month
   */
  Admin_Lbl_EndBaseMonth: 'End Base Month',

  /**
   * @description [en_US] End Date
   */
  Admin_Lbl_EndDate: 'End Date',

  /**
   * @description [en_US] Accept As Working Hour Before The Designated Time Without Request
   */
  Admin_Lbl_EndTimeOfAcceptAsWorkingHour: 'Accept As Working Hour Before The Designated Time Without Request',

  /**
   * @description [en_US] Entering Date
   */
  Admin_Lbl_EnteringDate: 'Entering Date',

  /**
   * @description [en_US] Event
   */
  Admin_Lbl_Event: 'Event',

  /**
   * @description [en_US] Executed At
   */
  Admin_Lbl_ExecutedAt: 'Executed At',

  /**
   * @description [en_US] Cost Center Required For
   */
  Admin_Lbl_ExpCostCenterRequiredFor: 'Cost Center Required For',

  /**
   * @description [en_US] Cost Center Used In
   */
  Admin_Lbl_ExpCostCenterUsedIn: 'Cost Center Used In',

  /**
   * @description [en_US] Custom Hint
   */
  Admin_Lbl_ExpCustomHint: 'Custom Hint',

  /**
   * @description [en_US] Expense Record
   */
  Admin_Lbl_ExpRecord: 'Expense Record',

  /**
   * @description [en_US] Expense Report Header
   */
  Admin_Lbl_ExpReportHeader: 'Expense Report Header',

  /**
   * @description [en_US] Setting
   */
  Admin_Lbl_ExpSetting: 'Setting',

  /**
   * @description [en_US] Tax Type
   */
  Admin_Lbl_ExpTaxType: 'Tax Type',

  /**
   * @description [en_US] Tax Type1
   */
  Admin_Lbl_ExpTaxType1: 'Tax Type1',

  /**
   * @description [en_US] Tax Type2
   */
  Admin_Lbl_ExpTaxType2: 'Tax Type2',

  /**
   * @description [en_US] Tax Type3
   */
  Admin_Lbl_ExpTaxType3: 'Tax Type3',

  /**
   * @description [en_US] Expense Type
   */
  Admin_Lbl_ExpType: 'Expense Type',

  /**
   * @description [en_US] Expense Type Group
   */
  Admin_Lbl_ExpTypeGroup: 'Expense Type Group',

  /**
   * @description [en_US] Vendor Required For
   */
  Admin_Lbl_ExpVendorRequiredFor: 'Vendor Required For',

  /**
   * @description [en_US] Vendor Used In
   */
  Admin_Lbl_ExpVendorUsedIn: 'Vendor Used In',

  /**
   * @description [en_US] There is no configured expense type
   */
  Admin_Lbl_ExpenseNoItemInTheSet: 'There is no configured expense type',

  /**
   * @description [en_US] Expense Report Only
   */
  Admin_Lbl_ExpenseReporextendedItemUsedInOptionst: 'Expense Report Only',

  /**
   * @description [en_US] Expense Report Only
   */
  Admin_Lbl_ExpenseReport: 'Expense Report Only',

  /**
   * @description [en_US] Expense
   */
  Admin_Lbl_ExpenseRequest: 'Expense',

  /**
   * @description [en_US] Expense Request and Expense Report
   */
  Admin_Lbl_ExpenseRequestAndExpenseReport: 'Expense Request and Expense Report',

  /**
   * @description [en_US] Search Expense Type
   */
  Admin_Lbl_ExpenseSearch: 'Search Expense Type',

  /**
   * @description [en_US] Code
   */
  Admin_Lbl_ExpenseTypeCode: 'Code',

  /**
   * @description [en_US] Configured expense type
   */
  Admin_Lbl_ExpenseTypeConfig: 'Configured expense type',

  /**
   * @description [en_US] Group Code
   */
  Admin_Lbl_ExpenseTypeGroupCode: 'Group Code',

  /**
   * @description [en_US] Group Name
   */
  Admin_Lbl_ExpenseTypeGroupName: 'Group Name',

  /**
   * @description [en_US] Name
   */
  Admin_Lbl_ExpenseTypeName: 'Name',

  /**
   * @description [en_US] Extended Item
   */
  Admin_Lbl_ExtendedItem: 'Extended Item',

  /**
   * @description [en_US] Extended Item Date
   */
  Admin_Lbl_ExtendedItemDate: 'Extended Item Date',

  /**
   * @description [en_US] Extended Item Date 01
   */
  Admin_Lbl_ExtendedItemDate01: 'Extended Item Date 01',

  /**
   * @description [en_US] Extended Item Date 02
   */
  Admin_Lbl_ExtendedItemDate02: 'Extended Item Date 02',

  /**
   * @description [en_US] Extended Item Date 03
   */
  Admin_Lbl_ExtendedItemDate03: 'Extended Item Date 03',

  /**
   * @description [en_US] Extended Item Date 04
   */
  Admin_Lbl_ExtendedItemDate04: 'Extended Item Date 04',

  /**
   * @description [en_US] Extended Item Date 05
   */
  Admin_Lbl_ExtendedItemDate05: 'Extended Item Date 05',

  /**
   * @description [en_US] Extended Item Date 06
   */
  Admin_Lbl_ExtendedItemDate06: 'Extended Item Date 06',

  /**
   * @description [en_US] Extended Item Date 07
   */
  Admin_Lbl_ExtendedItemDate07: 'Extended Item Date 07',

  /**
   * @description [en_US] Extended Item Date 08
   */
  Admin_Lbl_ExtendedItemDate08: 'Extended Item Date 08',

  /**
   * @description [en_US] Extended Item Date 09
   */
  Admin_Lbl_ExtendedItemDate09: 'Extended Item Date 09',

  /**
   * @description [en_US] Extended Item Date 10
   */
  Admin_Lbl_ExtendedItemDate10: 'Extended Item Date 10',

  /**
   * @description [en_US] Extended Item Lookup
   */
  Admin_Lbl_ExtendedItemLookup: 'Extended Item Lookup',

  /**
   * @description [en_US] Extended Item Lookup 01
   */
  Admin_Lbl_ExtendedItemLookup01: 'Extended Item Lookup 01',

  /**
   * @description [en_US] Extended Item Lookup 02
   */
  Admin_Lbl_ExtendedItemLookup02: 'Extended Item Lookup 02',

  /**
   * @description [en_US] Extended Item Lookup 03
   */
  Admin_Lbl_ExtendedItemLookup03: 'Extended Item Lookup 03',

  /**
   * @description [en_US] Extended Item Lookup 04
   */
  Admin_Lbl_ExtendedItemLookup04: 'Extended Item Lookup 04',

  /**
   * @description [en_US] Extended Item Lookup 05
   */
  Admin_Lbl_ExtendedItemLookup05: 'Extended Item Lookup 05',

  /**
   * @description [en_US] Extended Item Lookup 06
   */
  Admin_Lbl_ExtendedItemLookup06: 'Extended Item Lookup 06',

  /**
   * @description [en_US] Extended Item Lookup 07
   */
  Admin_Lbl_ExtendedItemLookup07: 'Extended Item Lookup 07',

  /**
   * @description [en_US] Extended Item Lookup 08
   */
  Admin_Lbl_ExtendedItemLookup08: 'Extended Item Lookup 08',

  /**
   * @description [en_US] Extended Item Lookup 09
   */
  Admin_Lbl_ExtendedItemLookup09: 'Extended Item Lookup 09',

  /**
   * @description [en_US] Extended Item Lookup 10
   */
  Admin_Lbl_ExtendedItemLookup10: 'Extended Item Lookup 10',

  /**
   * @description [en_US] Extended Item Select List
   */
  Admin_Lbl_ExtendedItemPicklist: 'Extended Item Select List',

  /**
   * @description [en_US] Extended Item Select List 01
   */
  Admin_Lbl_ExtendedItemPicklist01: 'Extended Item Select List 01',

  /**
   * @description [en_US] Extended Item Select List 02
   */
  Admin_Lbl_ExtendedItemPicklist02: 'Extended Item Select List 02',

  /**
   * @description [en_US] Extended Item Select List 03
   */
  Admin_Lbl_ExtendedItemPicklist03: 'Extended Item Select List 03',

  /**
   * @description [en_US] Extended Item Select List 04
   */
  Admin_Lbl_ExtendedItemPicklist04: 'Extended Item Select List 04',

  /**
   * @description [en_US] Extended Item Select List 05
   */
  Admin_Lbl_ExtendedItemPicklist05: 'Extended Item Select List 05',

  /**
   * @description [en_US] Extended Item Select List 06
   */
  Admin_Lbl_ExtendedItemPicklist06: 'Extended Item Select List 06',

  /**
   * @description [en_US] Extended Item Select List 07
   */
  Admin_Lbl_ExtendedItemPicklist07: 'Extended Item Select List 07',

  /**
   * @description [en_US] Extended Item Select List 08
   */
  Admin_Lbl_ExtendedItemPicklist08: 'Extended Item Select List 08',

  /**
   * @description [en_US] Extended Item Select List 09
   */
  Admin_Lbl_ExtendedItemPicklist09: 'Extended Item Select List 09',

  /**
   * @description [en_US] Extended Item Select List 10
   */
  Admin_Lbl_ExtendedItemPicklist10: 'Extended Item Select List 10',

  /**
   * @description [en_US] Required For
   */
  Admin_Lbl_ExtendedItemRequiredFor: 'Required For',

  /**
   * @description [en_US] Extended Item Text
   */
  Admin_Lbl_ExtendedItemText: 'Extended Item Text',

  /**
   * @description [en_US] Extended Item Text 01
   */
  Admin_Lbl_ExtendedItemText01: 'Extended Item Text 01',

  /**
   * @description [en_US] Extended Item Text 02
   */
  Admin_Lbl_ExtendedItemText02: 'Extended Item Text 02',

  /**
   * @description [en_US] Extended Item Text 03
   */
  Admin_Lbl_ExtendedItemText03: 'Extended Item Text 03',

  /**
   * @description [en_US] Extended Item Text 04
   */
  Admin_Lbl_ExtendedItemText04: 'Extended Item Text 04',

  /**
   * @description [en_US] Extended Item Text 05
   */
  Admin_Lbl_ExtendedItemText05: 'Extended Item Text 05',

  /**
   * @description [en_US] Extended Item Text 06
   */
  Admin_Lbl_ExtendedItemText06: 'Extended Item Text 06',

  /**
   * @description [en_US] Extended Item Text 07
   */
  Admin_Lbl_ExtendedItemText07: 'Extended Item Text 07',

  /**
   * @description [en_US] Extended Item Text 08
   */
  Admin_Lbl_ExtendedItemText08: 'Extended Item Text 08',

  /**
   * @description [en_US] Extended Item Text 09
   */
  Admin_Lbl_ExtendedItemText09: 'Extended Item Text 09',

  /**
   * @description [en_US] Extended Item Text 10
   */
  Admin_Lbl_ExtendedItemText10: 'Extended Item Text 10',

  /**
   * @description [en_US] Used In
   */
  Admin_Lbl_ExtendedItemUsedIn: 'Used In',

  /**
   * @description [en_US] Extra Field 1
   */
  Admin_Lbl_ExtraField: 'Extra Field 1',

  /**
   * @description [en_US] Failure Count
   */
  Admin_Lbl_FailureCount: 'Failure Count',

  /**
   * @description [en_US] First Name
   */
  Admin_Lbl_FirstName: 'First Name',

  /**
   * @description [en_US] Flex Hours
   */
  Admin_Lbl_FlexHours: 'Flex Hours',

  /**
   * @description [en_US] Settings
   */
  Admin_Lbl_FunctionSettings: 'Settings',

  /**
   * @description [en_US] Grade
   */
  Admin_Lbl_Grade: 'Grade',

  /**
   * @description [en_US] Employee Group
   */
  Admin_Lbl_Group: 'Employee Group',

  /**
   * @description [en_US] Half day leave
   */
  Admin_Lbl_HalfDayLeave: 'Half day leave',

  /**
   * @description [en_US] Half day leave hours
   */
  Admin_Lbl_HalfDayLeaveHours: 'Half day leave hours',

  /**
   * @description [en_US] History
   */
  Admin_Lbl_History: 'History',

  /**
   * @description [en_US] History Properties
   */
  Admin_Lbl_HistoryProperties: 'History Properties',

  /**
   * @description [en_US] Holiday Work Request
   */
  Admin_Lbl_HolidayWorkRequest: 'Holiday Work Request',

  /**
   * @description [en_US] If working hours is more than [%1] hours, rest time should be more than or equal to [%2] mins
   */
  Admin_Lbl_IfWorkingForXThenLegalRestTimeIsX: 'If working hours is more than [%1] hours, rest time should be more than or equal to [%2] mins',

  /**
   * @description [en_US] Include
   */
  Admin_Lbl_Include: 'Include',

  /**
   * @description [en_US] Include Holiday Work In Plain Time
   */
  Admin_Lbl_IncludeHolidayWorkInDeemedTime: 'Include Holiday Work In Plain Time',

  /**
   * @description [en_US] Include Holiday Work In Plain Time
   */
  Admin_Lbl_IncludeHolidayWorkInPlainTime: 'Include Holiday Work In Plain Time',

  /**
   * @description [en_US] Input Type
   */
  Admin_Lbl_InputType: 'Input Type',

  /**
   * @description [en_US] use in Expense
   */
  Admin_Lbl_IsSelectableExpense: 'use in Expense',

  /**
   * @description [en_US] Use in Time Tracking
   */
  Admin_Lbl_IsSelectableTimeTrack: 'Use in Time Tracking',

  /**
   * @description [en_US] Discretion
   */
  Admin_Lbl_JP_Discretion: 'Discretion',

  /**
   * @description [en_US] Fix
   */
  Admin_Lbl_JP_Fix: 'Fix',

  /**
   * @description [en_US] Flex
   */
  Admin_Lbl_JP_Flex: 'Flex',

  /**
   * @description [en_US] Manager
   */
  Admin_Lbl_JP_Manager: 'Manager',

  /**
   * @description [en_US] Modified
   */
  Admin_Lbl_JP_Modified: 'Modified',

  /**
   * @description [en_US] Job
   */
  Admin_Lbl_Job: 'Job',

  /**
   * @description [en_US] Job Assignment
   */
  Admin_Lbl_JobAssignment: 'Job Assignment',

  /**
   * @description [en_US] Job Input Type
   */
  Admin_Lbl_JobInputType: 'Job Input Type',

  /**
   * @description [en_US] Job Name
   */
  Admin_Lbl_JobName: 'Job Name',

  /**
   * @description [en_US] Job Owner
   */
  Admin_Lbl_JobOwnerName: 'Job Owner',

  /**
   * @description [en_US] Job Required For
   */
  Admin_Lbl_JobRequiredFor: 'Job Required For',

  /**
   * @description [en_US] Job Type
   */
  Admin_Lbl_JobType: 'Job Type',

  /**
   * @description [en_US] Job Used In
   */
  Admin_Lbl_JobUsedIn: 'Job Used In',

  /**
   * @description [en_US] Search Area Preference
   */
  Admin_Lbl_JorudanAreaPreference: 'Search Area Preference',

  /**
   * @description [en_US] provided distance of charged express (km)
   */
  Admin_Lbl_JorudanChargedExpressDistance: 'provided distance of charged express (km)',

  /**
   * @description [en_US] displaying commuter pass
   */
  Admin_Lbl_JorudanCommuterPass: 'displaying commuter pass',

  /**
   * @description [en_US] Fare Type
   */
  Admin_Lbl_JorudanFareType: 'Fare Type',

  /**
   * @description [en_US] Highway Bus (Default)
   */
  Admin_Lbl_JorudanHighwayBus: 'Highway Bus (Default)',

  /**
   * @description [en_US] Display Order of Search Result (Default)
   */
  Admin_Lbl_JorudanRouteSort: 'Display Order of Search Result (Default)',

  /**
   * @description [en_US] Jorudan Route Search Option
   */
  Admin_Lbl_JorudanSearchOption: 'Jorudan Route Search Option',

  /**
   * @description [en_US] Seat Preference (Default)
   */
  Admin_Lbl_JorudanSeatPreference: 'Seat Preference (Default)',

  /**
   * @description [en_US] Use Charged Express
   */
  Admin_Lbl_JorudanUseChargedExpress: 'Use Charged Express',

  /**
   * @description [en_US] Label
   */
  Admin_Lbl_Label: 'Label',

  /**
   * @description [en_US] Language
   */
  Admin_Lbl_Language: 'Language',

  /**
   * @description [en_US] Main Language
   */
  Admin_Lbl_Language0: 'Main Language',

  /**
   * @description [en_US] Sub Language1
   */
  Admin_Lbl_Language1: 'Sub Language1',

  /**
   * @description [en_US] Sub Language2
   */
  Admin_Lbl_Language2: 'Sub Language2',

  /**
   * @description [en_US] Last Name
   */
  Admin_Lbl_LastName: 'Last Name',

  /**
   * @description [en_US] Leave
   */
  Admin_Lbl_Leave: 'Leave',

  /**
   * @description [en_US] Leave
   */
  Admin_Lbl_LeaveCodeList: 'Leave',

  /**
   * @description [en_US] Granted Days
   */
  Admin_Lbl_LeaveDaysGranted: 'Granted Days',

  /**
   * @description [en_US] Left Days
   */
  Admin_Lbl_LeaveDaysLeft: 'Left Days',

  /**
   * @description [en_US] Valid From
   */
  Admin_Lbl_LeaveGrantValidDateFrom: 'Valid From',

  /**
   * @description [en_US] Valid To
   */
  Admin_Lbl_LeaveGrantValidDateTo: 'Valid To',

  /**
   * @description [en_US] Leave Management
   */
  Admin_Lbl_LeaveManagement: 'Leave Management',

  /**
   * @description [en_US] Leave of Absence
   */
  Admin_Lbl_LeaveOfAbsence: 'Leave of Absence',

  /**
   * @description [en_US] Apply Leave Of Absence
   */
  Admin_Lbl_LeaveOfAbsencePeriodStatus: 'Apply Leave Of Absence',

  /**
   * @description [en_US] Legal Holiday Only
   */
  Admin_Lbl_LegalHolidayOnly: 'Legal Holiday Only',

  /**
   * @description [en_US] Legal rest time check 1
   */
  Admin_Lbl_LegalRestTimeCheck1: 'Legal rest time check 1',

  /**
   * @description [en_US] Legal rest time check 2
   */
  Admin_Lbl_LegalRestTimeCheck2: 'Legal rest time check 2',

  /**
   * @description [en_US] Limit Length
   */
  Admin_Lbl_LimitLength: 'Limit Length',

  /**
   * @description [en_US] Logs
   */
  Admin_Lbl_Log: 'Logs',

  /**
   * @description [en_US] Granted Managed Leave(s)
   */
  Admin_Lbl_ManagedLeaveGrantHistoryOfValid: 'Granted Managed Leave(s)',

  /**
   * @description [en_US] Management
   */
  Admin_Lbl_ManagementScreen: 'Management',

  /**
   * @description [en_US] Manager Name
   */
  Admin_Lbl_ManagerName: 'Manager Name',

  /**
   * @description [en_US] Master
   */
  Admin_Lbl_Master: 'Master',

  /**
   * @description [en_US] Base Currency Setting
   */
  Admin_Lbl_MasterCurrencySetting: 'Base Currency Setting',

  /**
   * @description [en_US] Tax Type Setting
   */
  Admin_Lbl_MasterTaxTypeSetting: 'Tax Type Setting',

  /**
   * @description [en_US] Middle Name
   */
  Admin_Lbl_MiddleName: 'Middle Name',

  /**
   * @description [en_US] Mobile
   */
  Admin_Lbl_Mobile: 'Mobile',

  /**
   * @description [en_US] Month Mark
   */
  Admin_Lbl_MonthMark: 'Month Mark',

  /**
   * @description [en_US] Monthly Overtime Work Hours Limit
   */
  Admin_Lbl_MonthlyAgreementHourLimit: 'Monthly Overtime Work Hours Limit',

  /**
   * @description [en_US] Monthly Overtime Work Hours Limit (Special Provisions)
   */
  Admin_Lbl_MonthlyAgreementHourLimitSpecial: 'Monthly Overtime Work Hours Limit (Special Provisions)',

  /**
   * @description [en_US] Monthly Overtime Work Hours Warning Threshold 1
   */
  Admin_Lbl_MonthlyAgreementHourWarning1: 'Monthly Overtime Work Hours Warning Threshold 1',

  /**
   * @description [en_US] Monthly Overtime Work Hours Warning Threshold 2
   */
  Admin_Lbl_MonthlyAgreementHourWarning2: 'Monthly Overtime Work Hours Warning Threshold 2',

  /**
   * @description [en_US] Monthly Overtime Work Hours Warning Threshold (Special Provisions) 1
   */
  Admin_Lbl_MonthlyAgreementHourWarningSpecial1: 'Monthly Overtime Work Hours Warning Threshold (Special Provisions) 1',

  /**
   * @description [en_US] Monthly Overtime Work Hours Warning Threshold (Special Provisions) 2
   */
  Admin_Lbl_MonthlyAgreementHourWarningSpecial2: 'Monthly Overtime Work Hours Warning Threshold (Special Provisions) 2',

  /**
   * @description [en_US] Name
   */
  Admin_Lbl_Name: 'Name',

  /**
   * @description [en_US] Require reason
   */
  Admin_Lbl_NeedRequireReason: 'Require reason',

  /**
   * @description [en_US] Number Scheme
   */
  Admin_Lbl_NumberScheme: 'Number Scheme',

  /**
   * @description [en_US] Order
   */
  Admin_Lbl_Order: 'Order',

  /**
   * @description [en_US] Overall Settings
   */
  Admin_Lbl_Organization: 'Overall Settings',

  /**
   * @description [en_US] 36-Agreement Special Provisions/Overtime Work Hours
   */
  Admin_Lbl_OvertimeWorkHoursAndSpecialProvisions: '36-Agreement Special Provisions/Overtime Work Hours',

  /**
   * @description [en_US] Overtime Work Request
   */
  Admin_Lbl_OvertimeWorkRequest: 'Overtime Work Request',

  /**
   * @description [en_US] Pm half day leave
   */
  Admin_Lbl_PMHalfDayLeave: 'Pm half day leave',

  /**
   * @description [en_US] Parent Cost Center
   */
  Admin_Lbl_ParentCostCenterName: 'Parent Cost Center',

  /**
   * @description [en_US] Parent Department
   */
  Admin_Lbl_ParentDepartName: 'Parent Department',

  /**
   * @description [en_US] Parent Expense Type Group
   */
  Admin_Lbl_ParentExpTypeGroup: 'Parent Expense Type Group',

  /**
   * @description [en_US] Parent Job Name
   */
  Admin_Lbl_ParentJobName: 'Parent Job Name',

  /**
   * @description [en_US] Per Day
   */
  Admin_Lbl_PerDay: 'Per Day',

  /**
   * @description [en_US] Period
   */
  Admin_Lbl_Period: 'Period',

  /**
   * @description [en_US] Delegated users can approve and reject requests
   */
  Admin_Lbl_PermissionApproveAttRequestByDelegate: 'Delegated users can approve and reject requests',

  /**
   * @description [en_US] Employees can be the approvers of their own request
   */
  Admin_Lbl_PermissionApproveSelfAttRequestByEmployee: 'Employees can be the approvers of their own request',

  /**
   * @description [en_US] Attendance Daily Request
   */
  Admin_Lbl_PermissionAttDailyRequest: 'Attendance Daily Request',

  /**
   * @description [en_US] Attendance Monthly Request
   */
  Admin_Lbl_PermissionAttMonthlyRequest: 'Attendance Monthly Request',

  /**
   * @description [en_US] Operation by delegated users
   */
  Admin_Lbl_PermissionAttRequestByDelegate: 'Operation by delegated users',

  /**
   * @description [en_US] Operation by employees
   */
  Admin_Lbl_PermissionAttRequestByEmployee: 'Operation by employees',

  /**
   * @description [en_US] available
   */
  Admin_Lbl_PermissionAvailable: 'available',

  /**
   * @description [en_US] Delegated users can cancel approval of requests
   */
  Admin_Lbl_PermissionCancelAttApprovalByDelegate: 'Delegated users can cancel approval of requests',

  /**
   * @description [en_US] Employees can cancel requests
   */
  Admin_Lbl_PermissionCancelAttApprovalByEmployee: 'Employees can cancel requests',

  /**
   * @description [en_US] Delegated users can cancel requests
   */
  Admin_Lbl_PermissionCancelAttRequestByDelegate: 'Delegated users can cancel requests',

  /**
   * @description [en_US] edit
   */
  Admin_Lbl_PermissionEdit: 'edit',

  /**
   * @description [en_US] Agreement Alert Settings
   */
  Admin_Lbl_PermissionManageAttAgreementAlertSetting: 'Agreement Alert Settings',

  /**
   * @description [en_US] Leave
   */
  Admin_Lbl_PermissionManageAttLeave: 'Leave',

  /**
   * @description [en_US] Leave Management
   */
  Admin_Lbl_PermissionManageAttLeaveGrant: 'Leave Management',

  /**
   * @description [en_US] Leave of Absence
   */
  Admin_Lbl_PermissionManageAttLeaveOfAbsence: 'Leave of Absence',

  /**
   * @description [en_US] Apply Leave Of Absence
   */
  Admin_Lbl_PermissionManageAttLeaveOfAbsenceApply: 'Apply Leave Of Absence',

  /**
   * @description [en_US] Work Pattern
   */
  Admin_Lbl_PermissionManageAttPattern: 'Work Pattern',

  /**
   * @description [en_US] Apply Work Pattern
   */
  Admin_Lbl_PermissionManageAttPatternApply: 'Apply Work Pattern',

  /**
   * @description [en_US] Short Time Work Setting
   */
  Admin_Lbl_PermissionManageAttShortTimeWorkSetting: 'Short Time Work Setting',

  /**
   * @description [en_US] Apply Short Time Work Setting
   */
  Admin_Lbl_PermissionManageAttShortTimeWorkSettingApply: 'Apply Short Time Work Setting',

  /**
   * @description [en_US] Work Schema
   */
  Admin_Lbl_PermissionManageAttWorkingType: 'Work Schema',

  /**
   * @description [en_US] Calendar
   */
  Admin_Lbl_PermissionManageCalendar: 'Calendar',

  /**
   * @description [en_US] Department
   */
  Admin_Lbl_PermissionManageDepartment: 'Department',

  /**
   * @description [en_US] Employee
   */
  Admin_Lbl_PermissionManageEmployee: 'Employee',

  /**
   * @description [en_US] Job
   */
  Admin_Lbl_PermissionManageJob: 'Job',

  /**
   * @description [en_US] Job Type
   */
  Admin_Lbl_PermissionManageJobType: 'Job Type',

  /**
   * @description [en_US] Mobile Settings
   */
  Admin_Lbl_PermissionManageMobileSetting: 'Mobile Settings',

  /**
   * @description [en_US] Overall Settings
   */
  Admin_Lbl_PermissionManageOverallSetting: 'Overall Settings',

  /**
   * @description [en_US] Access Permission Management
   */
  Admin_Lbl_PermissionManagePermission: 'Access Permission Management',

  /**
   * @description [en_US] Planner Settings
   */
  Admin_Lbl_PermissionManagePlannerSetting: 'Planner Settings',

  /**
   * @description [en_US] Time Tracking Setting
   */
  Admin_Lbl_PermissionManageTimeSetting: 'Time Tracking Setting',

  /**
   * @description [en_US] Work Category
   */
  Admin_Lbl_PermissionManageTimeWorkCategory: 'Work Category',

  /**
   * @description [en_US] Management
   */
  Admin_Lbl_PermissionManagement: 'Management',

  /**
   * @description [en_US] Employees can be approver of their requests
   */
  Admin_Lbl_PermissionSelfAttRequestByEmployee: 'Employees can be approver of their requests',

  /**
   * @description [en_US] Permission Settings
   */
  Admin_Lbl_PermissionSettings: 'Permission Settings',

  /**
   * @description [en_US] Delegated users can submit requests
   */
  Admin_Lbl_PermissionSubmitAttRequestByDelegate: 'Delegated users can submit requests',

  /**
   * @description [en_US] Switching of Company
   */
  Admin_Lbl_PermissionSwitchCompany: 'Switching of Company',

  /**
   * @description [en_US] Time Tracking
   */
  Admin_Lbl_PermissionTimeTrack: 'Time Tracking',

  /**
   * @description [en_US] Time Report of Other Employees
   */
  Admin_Lbl_PermissionTimeTrackByDelegate: 'Time Report of Other Employees',

  /**
   * @description [en_US] Timesheet
   */
  Admin_Lbl_PermissionTimesheet: 'Timesheet',

  /**
   * @description [en_US] Timesheet of other employees
   */
  Admin_Lbl_PermissionTimesheetByDelegate: 'Timesheet of other employees',

  /**
   * @description [en_US] view
   */
  Admin_Lbl_PermissionView: 'view',

  /**
   * @description [en_US] Personal Settings
   */
  Admin_Lbl_PersonalSettings: 'Personal Settings',

  /**
   * @description [en_US] Select List Label
   */
  Admin_Lbl_PickListLabel: 'Select List Label',

  /**
   * @description [en_US] Select List Value
   */
  Admin_Lbl_PickListValue: 'Select List Value',

  /**
   * @description [en_US] Planner
   */
  Admin_Lbl_Planner: 'Planner',

  /**
   * @description [en_US] Open Daily Summary as default
   */
  Admin_Lbl_PlannerDefaultView: 'Open Daily Summary as default',

  /**
   * @description [en_US] Planner Setting
   */
  Admin_Lbl_PlannerSetting: 'Planner Setting',

  /**
   * @description [en_US] Select ...
   */
  Admin_Lbl_PleaseSelect: 'Select ...',

  /**
   * @description [en_US] Position
   */
  Admin_Lbl_Position: 'Position',

  /**
   * @description [en_US] Reason
   */
  Admin_Lbl_Reason: 'Reason',

  /**
   * @description [en_US] Reason for Revision
   */
  Admin_Lbl_ReasonForRevision: 'Reason for Revision',

  /**
   * @description [en_US] Record Date
   */
  Admin_Lbl_RecordingDate: 'Record Date',

  /**
   * @description [en_US] Remarks
   */
  Admin_Lbl_Remarks: 'Remarks',

  /**
   * @description [en_US] Replacement Leave of Holiday Work
   */
  Admin_Lbl_ReplacementLeaveOfHolidayWork: 'Replacement Leave of Holiday Work',

  /**
   * @description [en_US] Report type
   */
  Admin_Lbl_ReportType: 'Report type',

  /**
   * @description [en_US] Report type not used
   */
  Admin_Lbl_ReportTypeNotUsed: 'Report type not used',

  /**
   * @description [en_US] Report type used
   */
  Admin_Lbl_ReportTypeUsed: 'Report type used',

  /**
   * @description [en_US] Request Options
   */
  Admin_Lbl_RequestOptions: 'Request Options',

  /**
   * @description [en_US] Send the current locations of employees on time-stamping
   */
  Admin_Lbl_RequireLocationAtMobileStamp: 'Send the current locations of employees on time-stamping',

  /**
   * @description [en_US] Require reason
   */
  Admin_Lbl_RequireReason: 'Require reason',

  /**
   * @description [en_US] RequireRequestForAcceptAsWorkHours
   */
  Admin_Lbl_RequireRequestForAcceptAsWorkHours: 'RequireRequestForAcceptAsWorkHours',

  /**
   * @description [en_US] Resignation Date
   */
  Admin_Lbl_ResignationDate: 'Resignation Date',

  /**
   * @description [en_US] Revision
   */
  Admin_Lbl_Revision: 'Revision',

  /**
   * @description [en_US] Revision Date
   */
  Admin_Lbl_RevisionDate: 'Revision Date',

  /**
   * @description [en_US] Scope
   */
  Admin_Lbl_ScopedAssignment: 'Scope',

  /**
   * @description [en_US] Company
   */
  Admin_Lbl_ScopedAssignmentCompany: 'Company',

  /**
   * @description [en_US] Person
   */
  Admin_Lbl_ScopedAssignmentPerson: 'Person',

  /**
   * @description [en_US] Search
   */
  Admin_Lbl_Search: 'Search',

  /**
   * @description [en_US] Search commuter pass route
   */
  Admin_Lbl_SearchRoute: 'Search commuter pass route',

  /**
   * @description [en_US] Section
   */
  Admin_Lbl_Section: 'Section',

  /**
   * @description [en_US] Select Employee
   */
  Admin_Lbl_SelectEmployee: 'Select Employee',

  /**
   * @description [en_US] Select Function
   */
  Admin_Lbl_SelectFunction: 'Select Function',

  /**
   * @description [en_US] Selected
   */
  Admin_Lbl_Selected: 'Selected',

  /**
   * @description [en_US] Apply Short Time Work Setting
   */
  Admin_Lbl_ShortTimeWorkPeriodStatus: 'Apply Short Time Work Setting',

  /**
   * @description [en_US] Short Time Work Reason
   */
  Admin_Lbl_ShortTimeWorkReason: 'Short Time Work Reason',

  /**
   * @description [en_US] Short Time Work Setting
   */
  Admin_Lbl_ShortTimeWorkSetting: 'Short Time Work Setting',

  /**
   * @description [en_US] Start
   */
  Admin_Lbl_Start: 'Start',

  /**
   * @description [en_US] Start Date
   */
  Admin_Lbl_StartDate: 'Start Date',

  /**
   * @description [en_US] Start Date Of Month
   */
  Admin_Lbl_StartDateOfMonth: 'Start Date Of Month',

  /**
   * @description [en_US] Start Day Of Week
   */
  Admin_Lbl_StartDayOfWeek: 'Start Day Of Week',

  /**
   * @description [en_US] Accept As Working Hour After The Designated Time Without Request
   */
  Admin_Lbl_StartTimeOfAcceptAsWorkingHour: 'Accept As Working Hour After The Designated Time Without Request',

  /**
   * @description [en_US] Status
   */
  Admin_Lbl_Status: 'Status',

  /**
   * @description [en_US] Substitute Leave to Use
   */
  Admin_Lbl_SubstituteLeaveToUse: 'Substitute Leave to Use',

  /**
   * @description [en_US] Success Count
   */
  Admin_Lbl_SuccessCount: 'Success Count',

  /**
   * @description [en_US] Summary Period
   */
  Admin_Lbl_SummaryPeriod: 'Summary Period',

  /**
   * @description [en_US] Company Settings
   */
  Admin_Lbl_Tab_Company: 'Company Settings',

  /**
   * @description [en_US] specify date
   */
  Admin_Lbl_TargetDate: 'specify date',

  /**
   * @description [en_US] Target Employees
   */
  Admin_Lbl_TargetEmployee: 'Target Employees',

  /**
   * @description [en_US] Tax Rate(%)
   */
  Admin_Lbl_TaxRate: 'Tax Rate(%)',

  /**
   * @description [en_US] Tax Setting
   */
  Admin_Lbl_TaxSetting: 'Tax Setting',

  /**
   * @description [en_US] Time Tracking Setting
   */
  Admin_Lbl_TimeSetting: 'Time Tracking Setting',

  /**
   * @description [en_US] Use
   */
  Admin_Lbl_Use: 'Use',

  /**
   * @description [en_US] use Attendance
   */
  Admin_Lbl_UseAttendance: 'use Attendance',

  /**
   * @description [en_US] Use Commuter Pass
   */
  Admin_Lbl_UseCommuterPass: 'Use Commuter Pass',

  /**
   * @description [en_US] Use Company Tax Master
   */
  Admin_Lbl_UseCompanyTaxMaster: 'Use Company Tax Master',

  /**
   * @description [en_US] use Expense
   */
  Admin_Lbl_UseExpense: 'use Expense',

  /**
   * @description [en_US] Use File Attachment
   */
  Admin_Lbl_UseFileAttachment: 'Use File Attachment',

  /**
   * @description [en_US] Use Foreign Currency
   */
  Admin_Lbl_UseForeignCurrency: 'Use Foreign Currency',

  /**
   * @description [en_US] use Planner
   */
  Admin_Lbl_UsePlanner: 'use Planner',

  /**
   * @description [en_US] Request
   */
  Admin_Lbl_UseTimeTrackRequest: 'Request',

  /**
   * @description [en_US] use Work Time
   */
  Admin_Lbl_UseWorkTime: 'use Work Time',

  /**
   * @description [en_US] Salesforce user
   */
  Admin_Lbl_User: 'Salesforce user',

  /**
   * @description [en_US] Valid Date
   */
  Admin_Lbl_ValidDate: 'Valid Date',

  /**
   * @description [en_US] Valid Date From
   */
  Admin_Lbl_ValidDateFrom: 'Valid Date From',

  /**
   * @description [en_US] Valid Date To
   */
  Admin_Lbl_ValidDateTo: 'Valid Date To',

  /**
   * @description [en_US] Valid Period
   */
  Admin_Lbl_ValidPeriod: 'Valid Period',

  /**
   * @description [en_US] validation check
   */
  Admin_Lbl_ValidationCheck: 'validation check',

  /**
   * @description [en_US] Vendor
   */
  Admin_Lbl_Vendor: 'Vendor',

  /**
   * @description [en_US] Not Used
   */
  Admin_Lbl_VendorPaymentDueDate_NotUsed: 'Not Used',

  /**
   * @description [en_US] Optional
   */
  Admin_Lbl_VendorPaymentDueDate_Optional: 'Optional',

  /**
   * @description [en_US] Required
   */
  Admin_Lbl_VendorPaymentDueDate_Required: 'Required',

  /**
   * @description [en_US] View
   */
  Admin_Lbl_View: 'View',

  /**
   * @description [en_US] Flex without core time
   */
  Admin_Lbl_WithoutCoreTime: 'Flex without core time',

  /**
   * @description [en_US] Work Category
   */
  Admin_Lbl_WorkCategory: 'Work Category',

  /**
   * @description [en_US] Work Category Code
   */
  Admin_Lbl_WorkCategoryCode: 'Work Category Code',

  /**
   * @description [en_US] Work Category Name
   */
  Admin_Lbl_WorkCategoryName: 'Work Category Name',

  /**
   * @description [en_US] Work Scheme
   */
  Admin_Lbl_WorkScheme: 'Work Scheme',

  /**
   * @description [en_US] Time Tracking Management
   */
  Admin_Lbl_WorkTimeManagement: 'Time Tracking Management',

  /**
   * @description [en_US] Working time
   */
  Admin_Lbl_WorkingHours: 'Working time',

  /**
   * @description [en_US] Working Criterion time
   */
  Admin_Lbl_WorkingHoursCriterion: 'Working Criterion time',

  /**
   * @description [en_US] Add compensation time to work hours
   */
  Admin_Lbl_WorkingTypeAddCompensationTimeToWorkHours: 'Add compensation time to work hours',

  /**
   * @description [en_US] Am Working time
   */
  Admin_Lbl_WorkingTypeAm: 'Am Working time',

  /**
   * @description [en_US] Am contracted work hours
   */
  Admin_Lbl_WorkingTypeAmContractedWorkHours: 'Am contracted work hours',

  /**
   * @description [en_US] Am Working Criterion time
   */
  Admin_Lbl_WorkingTypeAmCriterion: 'Am Working Criterion time',

  /**
   * @description [en_US] Am rest1
   */
  Admin_Lbl_WorkingTypeAmRest1: 'Am rest1',

  /**
   * @description [en_US] Am rest2
   */
  Admin_Lbl_WorkingTypeAmRest2: 'Am rest2',

  /**
   * @description [en_US] Am rest3
   */
  Admin_Lbl_WorkingTypeAmRest3: 'Am rest3',

  /**
   * @description [en_US] Am rest4
   */
  Admin_Lbl_WorkingTypeAmRest4: 'Am rest4',

  /**
   * @description [en_US] Am rest5
   */
  Admin_Lbl_WorkingTypeAmRest5: 'Am rest5',

  /**
   * @description [en_US] AM Rest Time Criterion 1
   */
  Admin_Lbl_WorkingTypeAmRestCriterion1: 'AM Rest Time Criterion 1',

  /**
   * @description [en_US] AM Rest Time Criterion 2
   */
  Admin_Lbl_WorkingTypeAmRestCriterion2: 'AM Rest Time Criterion 2',

  /**
   * @description [en_US] AM Rest Time Criterion 3
   */
  Admin_Lbl_WorkingTypeAmRestCriterion3: 'AM Rest Time Criterion 3',

  /**
   * @description [en_US] AM Rest Time Criterion 4
   */
  Admin_Lbl_WorkingTypeAmRestCriterion4: 'AM Rest Time Criterion 4',

  /**
   * @description [en_US] AM Rest Time Criterion 5
   */
  Admin_Lbl_WorkingTypeAmRestCriterion5: 'AM Rest Time Criterion 5',

  /**
   * @description [en_US] Am work hours Criterion
   */
  Admin_Lbl_WorkingTypeAmWorkHoursCriterion: 'Am work hours Criterion',

  /**
   * @description [en_US] Automatic legal holiday assign
   */
  Admin_Lbl_WorkingTypeAutomaticLegalHolidayAssign: 'Automatic legal holiday assign',

  /**
   * @description [en_US] Boundary end time of day
   */
  Admin_Lbl_WorkingTypeBoundaryEndTimeOfDay: 'Boundary end time of day',

  /**
   * @description [en_US] Boundary start time of day
   */
  Admin_Lbl_WorkingTypeBoundaryStartTimeOfDay: 'Boundary start time of day',

  /**
   * @description [en_US] Boundary time of day
   */
  Admin_Lbl_WorkingTypeBoundaryTimeOfDay: 'Boundary time of day',

  /**
   * @description [en_US] Classification next day work
   */
  Admin_Lbl_WorkingTypeClassificationNextDayWork: 'Classification next day work',

  /**
   * @description [en_US] Comment
   */
  Admin_Lbl_WorkingTypeComment: 'Comment',

  /**
   * @description [en_US] Contracted work hours
   */
  Admin_Lbl_WorkingTypeContractedWorkHours: 'Contracted work hours',

  /**
   * @description [en_US] End of night work
   */
  Admin_Lbl_WorkingTypeEndOfNightWork: 'End of night work',

  /**
   * @description [en_US] End time
   */
  Admin_Lbl_WorkingTypeEndTime: 'End time',

  /**
   * @description [en_US] Am core time
   */
  Admin_Lbl_WorkingTypeFlexAm: 'Am core time',

  /**
   * @description [en_US] Pm core time
   */
  Admin_Lbl_WorkingTypeFlexPm: 'Pm core time',

  /**
   * @description [en_US] Legal holiday assignment days
   */
  Admin_Lbl_WorkingTypeLegalHolidayAssignmentDays: 'Legal holiday assignment days',

  /**
   * @description [en_US] Legal holiday assignment period
   */
  Admin_Lbl_WorkingTypeLegalHolidayAssignmentPeriod: 'Legal holiday assignment period',

  /**
   * @description [en_US] Legal work time a day
   */
  Admin_Lbl_WorkingTypeLegalWorkTimeADay: 'Legal work time a day',

  /**
   * @description [en_US] Legal work time a week
   */
  Admin_Lbl_WorkingTypeLegalWorkTimeAWeek: 'Legal work time a week',

  /**
   * @description [en_US] Month mark
   */
  Admin_Lbl_WorkingTypeMonthMark: 'Month mark',

  /**
   * @description [en_US] Working Type Name
   */
  Admin_Lbl_WorkingTypeName: 'Working Type Name',

  /**
   * @description [en_US] Offset
   */
  Admin_Lbl_WorkingTypeOffset: 'Offset',

  /**
   * @description [en_US] Working type options
   */
  Admin_Lbl_WorkingTypeOptions: 'Working type options',

  /**
   * @description [en_US] Overtime and deduction time
   */
  Admin_Lbl_WorkingTypeOvertimeAndDeductionTime: 'Overtime and deduction time',

  /**
   * @description [en_US] Payroll period
   */
  Admin_Lbl_WorkingTypePayrollPeriod: 'Payroll period',

  /**
   * @description [en_US] Permit overtime work until contracted hours
   */
  Admin_Lbl_WorkingTypePermitOvertimeWorkUntilContractedHours: 'Permit overtime work until contracted hours',

  /**
   * @description [en_US] Pm working time
   */
  Admin_Lbl_WorkingTypePm: 'Pm working time',

  /**
   * @description [en_US] Pm contracted work hours
   */
  Admin_Lbl_WorkingTypePmContractedWorkHours: 'Pm contracted work hours',

  /**
   * @description [en_US] Pm working criterion time
   */
  Admin_Lbl_WorkingTypePmCriterion: 'Pm working criterion time',

  /**
   * @description [en_US] Pm rest1
   */
  Admin_Lbl_WorkingTypePmRest1: 'Pm rest1',

  /**
   * @description [en_US] Pm rest1 end time
   */
  Admin_Lbl_WorkingTypePmRest1EndTime: 'Pm rest1 end time',

  /**
   * @description [en_US] Pm rest2
   */
  Admin_Lbl_WorkingTypePmRest2: 'Pm rest2',

  /**
   * @description [en_US] Pm rest3
   */
  Admin_Lbl_WorkingTypePmRest3: 'Pm rest3',

  /**
   * @description [en_US] Pm rest4
   */
  Admin_Lbl_WorkingTypePmRest4: 'Pm rest4',

  /**
   * @description [en_US] Pm rest5 time
   */
  Admin_Lbl_WorkingTypePmRest5: 'Pm rest5 time',

  /**
   * @description [en_US] PM Rest Time Criterion 1
   */
  Admin_Lbl_WorkingTypePmRestCriterion1: 'PM Rest Time Criterion 1',

  /**
   * @description [en_US] PM Rest Time Criterion 2
   */
  Admin_Lbl_WorkingTypePmRestCriterion2: 'PM Rest Time Criterion 2',

  /**
   * @description [en_US] PM Rest Time Criterion 3
   */
  Admin_Lbl_WorkingTypePmRestCriterion3: 'PM Rest Time Criterion 3',

  /**
   * @description [en_US] PM Rest Time Criterion 4
   */
  Admin_Lbl_WorkingTypePmRestCriterion4: 'PM Rest Time Criterion 4',

  /**
   * @description [en_US] PM Rest Time Criterion 5
   */
  Admin_Lbl_WorkingTypePmRestCriterion5: 'PM Rest Time Criterion 5',

  /**
   * @description [en_US] Pm work hours Criterion
   */
  Admin_Lbl_WorkingTypePmWorkHoursCriterion: 'Pm work hours Criterion',

  /**
   * @description [en_US] Rest
   */
  Admin_Lbl_WorkingTypeRest: 'Rest',

  /**
   * @description [en_US] Rest1
   */
  Admin_Lbl_WorkingTypeRest1: 'Rest1',

  /**
   * @description [en_US] Rest2
   */
  Admin_Lbl_WorkingTypeRest2: 'Rest2',

  /**
   * @description [en_US] Rest3
   */
  Admin_Lbl_WorkingTypeRest3: 'Rest3',

  /**
   * @description [en_US] Rest4
   */
  Admin_Lbl_WorkingTypeRest4: 'Rest4',

  /**
   * @description [en_US] Rest5
   */
  Admin_Lbl_WorkingTypeRest5: 'Rest5',

  /**
   * @description [en_US] Rest5 start time
   */
  Admin_Lbl_WorkingTypeRest5StartTime: 'Rest5 start time',

  /**
   * @description [en_US] Criterion of Rest 1
   */
  Admin_Lbl_WorkingTypeRestCriterion1: 'Criterion of Rest 1',

  /**
   * @description [en_US] Criterion of Rest 2
   */
  Admin_Lbl_WorkingTypeRestCriterion2: 'Criterion of Rest 2',

  /**
   * @description [en_US] Criterion of Rest 3
   */
  Admin_Lbl_WorkingTypeRestCriterion3: 'Criterion of Rest 3',

  /**
   * @description [en_US] Criterion of Rest 4
   */
  Admin_Lbl_WorkingTypeRestCriterion4: 'Criterion of Rest 4',

  /**
   * @description [en_US] Criterion of Rest 5
   */
  Admin_Lbl_WorkingTypeRestCriterion5: 'Criterion of Rest 5',

  /**
   * @description [en_US] Start day of month
   */
  Admin_Lbl_WorkingTypeStartDayOfMonth: 'Start day of month',

  /**
   * @description [en_US] Start day of week
   */
  Admin_Lbl_WorkingTypeStartDayOfWeek: 'Start day of week',

  /**
   * @description [en_US] Start month of year
   */
  Admin_Lbl_WorkingTypeStartMonthOfYear: 'Start month of year',

  /**
   * @description [en_US] Start of night work
   */
  Admin_Lbl_WorkingTypeStartOfNightWork: 'Start of night work',

  /**
   * @description [en_US] Start time
   */
  Admin_Lbl_WorkingTypeStartTime: 'Start time',

  /**
   * @description [en_US] Working type time
   */
  Admin_Lbl_WorkingTypeTime: 'Working type time',

  /**
   * @description [en_US] Valid date from
   */
  Admin_Lbl_WorkingTypeValidDateFrom: 'Valid date from',

  /**
   * @description [en_US] Valid date to
   */
  Admin_Lbl_WorkingTypeValidDateTo: 'Valid date to',

  /**
   * @description [en_US] Weekly day type FRI
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeFRI: 'Weekly day type FRI',

  /**
   * @description [en_US] Weekly day type MON
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeMON: 'Weekly day type MON',

  /**
   * @description [en_US] Weekly day type SAT
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeSAT: 'Weekly day type SAT',

  /**
   * @description [en_US] Weekly day type SUN
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeSUN: 'Weekly day type SUN',

  /**
   * @description [en_US] Weekly day type THU
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeTHU: 'Weekly day type THU',

  /**
   * @description [en_US] Weekly day type TUE
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeTUE: 'Weekly day type TUE',

  /**
   * @description [en_US] Weekly day type WED
   */
  Admin_Lbl_WorkingTypeWeeklyDayTypeWED: 'Weekly day type WED',

  /**
   * @description [en_US] Criterion of work hours
   */
  Admin_Lbl_WorkingTypeWorkHoursCriterion: 'Criterion of work hours',

  /**
   * @description [en_US] Work system
   */
  Admin_Lbl_WorkingTypeWorkSystem: 'Work system',

  /**
   * @description [en_US] Year mark
   */
  Admin_Lbl_WorkingTypeYearMark: 'Year mark',

  /**
   * @description [en_US] Active
   */
  Admin_Msg_Active: 'Active',

  /**
   * @description [en_US] If this is checked, the users are able to modify tax amounts manually.
   */
  Admin_Msg_AllowTaxAmountChange: 'If this is checked, the users are able to modify tax amounts manually.',

  /**
   * @description [en_US] Grant history of annual paid leave(s) will be shown here.
   */
  Admin_Msg_AnnualPaidLeaveGrantHistoryListEmptyRowsViewMessage: 'Grant history of annual paid leave(s) will be shown here.',

  /**
   * @description [en_US] Connect Planner with Office 365.
   */
  Admin_Msg_CalendarAccessOffice365: 'Connect Planner with Office 365.',

  /**
   * @description [en_US] Are you sure to delete?
   */
  Admin_Msg_ConfirmDelete: 'Are you sure to delete?',

  /**
   * @description [en_US] Time to exempt deduction.
   */
  Admin_Msg_DescAllowableShortenTime: 'Time to exempt deduction.',

  /**
   * @description [en_US] Time to redice core time.
   */
  Admin_Msg_DescAllowableShortenTimeForFlex: 'Time to redice core time.',

  /**
   * @description [en_US] to Direct Charged
   */
  Admin_Msg_DirectCharged: 'to Direct Charged',

  /**
   * @description [en_US] [%1] employees were found.
   */
  Admin_Msg_EmployeesFound: '[%1] employees were found.',

  /**
   * @description [en_US] value is empty
   */
  Admin_Msg_EmptyItem: 'value is empty',

  /**
   * @description [en_US] Grant history of managed leave(s) will be shown here.
   */
  Admin_Msg_ManagedLeaveGrantHistoryListEmptyRowsViewMessage: 'Grant history of managed leave(s) will be shown here.',

  /**
   * @description [en_US] use Attendance
   */
  Admin_Msg_UseAttendance: 'use Attendance',

  /**
   * @description [en_US] Use Expenses
   */
  Admin_Msg_UseExpense: 'Use Expenses',

  /**
   * @description [en_US] use Planner
   */
  Admin_Msg_UsePlanner: 'use Planner',

  /**
   * @description [en_US] Use Time Tracking
   */
  Admin_Msg_UseWorkTime: 'Use Time Tracking',

  /**
   * @description [en_US] Date
   */
  Admin_Sel_InputTypeDate: 'Date',

  /**
   * @description [en_US] Lookup
   */
  Admin_Sel_InputTypeLookup: 'Lookup',

  /**
   * @description [en_US] Pull Down
   */
  Admin_Sel_InputTypePickUp: 'Pull Down',

  /**
   * @description [en_US] Text
   */
  Admin_Sel_InputTypeText: 'Text',

  /**
   * @description [en_US] Approve
   */
  Appr_Btn_Approval: 'Approve',

  /**
   * @description [en_US] Multiple Approval/Reject
   */
  Appr_Btn_ApprovalOrReject: 'Multiple Approval/Reject',

  /**
   * @description [en_US] Approve
   */
  Appr_Btn_Approve: 'Approve',

  /**
   * @description [en_US] Close all
   */
  Appr_Btn_Close: 'Close all',

  /**
   * @description [en_US] Edit
   */
  Appr_Btn_Edit: 'Edit',

  /**
   * @description [en_US] Requests
   */
  Appr_Btn_ExpensesPreApproval: 'Requests',

  /**
   * @description [en_US] Expenses
   */
  Appr_Btn_ExpensesRequest: 'Expenses',

  /**
   * @description [en_US] Open all
   */
  Appr_Btn_Open: 'Open all',

  /**
   * @description [en_US] Read More
   */
  Appr_Btn_ReadMore: 'Read More',

  /**
   * @description [en_US] Reject
   */
  Appr_Btn_Reject: 'Reject',

  /**
   * @description [en_US] Time Report
   */
  Appr_Btn_TimeTrackRequest: 'Time Report',

  /**
   * @description [en_US] Actor
   */
  Appr_Lbl_Actor: 'Actor',

  /**
   * @description [en_US] Actor Name
   */
  Appr_Lbl_ActorName: 'Actor Name',

  /**
   * @description [en_US] Amount
   */
  Appr_Lbl_Amount: 'Amount',

  /**
   * @description [en_US] Tax Amount
   */
  Appr_Lbl_AmountOfTax: 'Tax Amount',

  /**
   * @description [en_US] Applicant Name
   */
  Appr_Lbl_ApplicantName: 'Applicant Name',

  /**
   * @description [en_US] Approval
   */
  Appr_Lbl_Approval: 'Approval',

  /**
   * @description [en_US] Approval Detail
   */
  Appr_Lbl_ApprovalDetail: 'Approval Detail',

  /**
   * @description [en_US] Approval History
   */
  Appr_Lbl_ApprovalHistory: 'Approval History',

  /**
   * @description [en_US] Approval Requests
   */
  Appr_Lbl_ApprovalList: 'Approval Requests',

  /**
   * @description [en_US] Approve Comment
   */
  Appr_Lbl_ApproveComment: 'Approve Comment',

  /**
   * @description [en_US] Approved
   */
  Appr_Lbl_Approved: 'Approved',

  /**
   * @description [en_US] Approver Name
   */
  Appr_Lbl_ApproverName: 'Approver Name',

  /**
   * @description [en_US] Attached files
   */
  Appr_Lbl_AttachedFiles: 'Attached files',

  /**
   * @description [en_US] Work Arrangement Request
   */
  Appr_Lbl_AttendanceRequest: 'Work Arrangement Request',

  /**
   * @description [en_US] Attendee(s)
   */
  Appr_Lbl_Attendee: 'Attendee(s)',

  /**
   * @description [en_US] Changes
   */
  Appr_Lbl_Changes: 'Changes',

  /**
   * @description [en_US] Comments
   */
  Appr_Lbl_Comments: 'Comments',

  /**
   * @description [en_US] -
   */
  Appr_Lbl_ComparisonNone: '-',

  /**
   * @description [en_US] Continue to register
   */
  Appr_Lbl_ContinueToRegister: 'Continue to register',

  /**
   * @description [en_US] Amount
   */
  Appr_Lbl_Cost: 'Amount',

  /**
   * @description [en_US] Cost Center
   */
  Appr_Lbl_CostCenter: 'Cost Center',

  /**
   * @description [en_US] Amount per head
   */
  Appr_Lbl_CostPerHead: 'Amount per head',

  /**
   * @description [en_US] Currency
   */
  Appr_Lbl_Currency: 'Currency',

  /**
   * @description [en_US] Date
   */
  Appr_Lbl_Date: 'Date',

  /**
   * @description [en_US] Date Submitted
   */
  Appr_Lbl_DateSubmitted: 'Date Submitted',

  /**
   * @description [en_US] Details
   */
  Appr_Lbl_DayDetail: 'Details',

  /**
   * @description [en_US] Delegate
   */
  Appr_Lbl_Delegate: 'Delegate',

  /**
   * @description [en_US] Delegate
   */
  Appr_Lbl_DelegatedApplicantName: 'Delegate',

  /**
   * @description [en_US] Department
   */
  Appr_Lbl_DepartmentName: 'Department',

  /**
   * @description [en_US] Details
   */
  Appr_Lbl_Detail: 'Details',

  /**
   * @description [en_US] Difference
   */
  Appr_Lbl_Difference: 'Difference',

  /**
   * @description [en_US] Employee Name / Code
   */
  Appr_Lbl_EmployeeAndID: 'Employee Name / Code',

  /**
   * @description [en_US] Employee
   */
  Appr_Lbl_EmployeeName: 'Employee',

  /**
   * @description [en_US] Excl. Tax
   */
  Appr_Lbl_ExclTax: 'Excl. Tax',

  /**
   * @description [en_US] Date
   */
  Appr_Lbl_ExpAccountingDate: 'Date',

  /**
   * @description [en_US] Expense
   */
  Appr_Lbl_Expense: 'Expense',

  /**
   * @description [en_US] Expense Type
   */
  Appr_Lbl_ExpenseType: 'Expense Type',

  /**
   * @description [en_US] Tax
   */
  Appr_Lbl_GST: 'Tax',

  /**
   * @description [en_US] History List
   */
  Appr_Lbl_HistoryList: 'History List',

  /**
   * @description [en_US] Image
   */
  Appr_Lbl_Image: 'Image',

  /**
   * @description [en_US] Import from
   */
  Appr_Lbl_ImportFrom: 'Import from',

  /**
   * @description [en_US] Job
   */
  Appr_Lbl_Job: 'Job',

  /**
   * @description [en_US] Previous Status
   */
  Appr_Lbl_JustBeforeStatus: 'Previous Status',

  /**
   * @description [en_US] Menu
   */
  Appr_Lbl_Menu: 'Menu',

  /**
   * @description [en_US] Monthly Attendance Request
   */
  Appr_Lbl_MonthlyAttendanceRequest: 'Monthly Attendance Request',

  /**
   * @description [en_US] No receipt
   */
  Appr_Lbl_NoReceipt: 'No receipt',

  /**
   * @description [en_US] The approved request has been modified. Please confirm changes.
   */
  Appr_Lbl_NotificationReapply: 'The approved request has been modified. Please confirm changes.',

  /**
   * @description [en_US] Assign to
   */
  Appr_Lbl_OriginalActor: 'Assign to',

  /**
   * @description [en_US] Payment Term
   */
  Appr_Lbl_PaymentPeriod: 'Payment Term',

  /**
   * @description [en_US] Payment Type
   */
  Appr_Lbl_PaymentType: 'Payment Type',

  /**
   * @description [en_US] Period
   */
  Appr_Lbl_Period: 'Period',

  /**
   * @description [en_US] Persion
   */
  Appr_Lbl_Person: 'Persion',

  /**
   * @description [en_US] Posting Date
   */
  Appr_Lbl_PostingDate: 'Posting Date',

  /**
   * @description [en_US] Previous application
   */
  Appr_Lbl_PreviousApplication: 'Previous application',

  /**
   * @description [en_US] Receipt
   */
  Appr_Lbl_Receipt: 'Receipt',

  /**
   * @description [en_US] record(s)
   */
  Appr_Lbl_RecordCount: 'record(s)',

  /**
   * @description [en_US] Reject Comment
   */
  Appr_Lbl_RejectComment: 'Reject Comment',

  /**
   * @description [en_US] Rejected
   */
  Appr_Lbl_Rejected: 'Rejected',

  /**
   * @description [en_US] Remarks
   */
  Appr_Lbl_Remarks: 'Remarks',

  /**
   * @description [en_US] Request
   */
  Appr_Lbl_Request: 'Request',

  /**
   * @description [en_US] Request Date
   */
  Appr_Lbl_RequestDate: 'Request Date',

  /**
   * @description [en_US] Request Number
   */
  Appr_Lbl_RequestNumber: 'Request Number',

  /**
   * @description [en_US] Title / Request Number
   */
  Appr_Lbl_RequestTitleAndNumber: 'Title / Request Number',

  /**
   * @description [en_US] Request Type
   */
  Appr_Lbl_RequestType: 'Request Type',

  /**
   * @description [en_US] Shop
   */
  Appr_Lbl_Shop: 'Shop',

  /**
   * @description [en_US] Status
   */
  Appr_Lbl_Situation: 'Status',

  /**
   * @description [en_US] Status
   */
  Appr_Lbl_Status: 'Status',

  /**
   * @description [en_US] Step
   */
  Appr_Lbl_Step: 'Step',

  /**
   * @description [en_US] Submit
   */
  Appr_Lbl_Submit: 'Submit',

  /**
   * @description [en_US] Comment
   */
  Appr_Lbl_SubmitComment: 'Comment',

  /**
   * @description [en_US] Summary
   */
  Appr_Lbl_Summary: 'Summary',

  /**
   * @description [en_US] Request
   */
  Appr_Lbl_TAndERequest: 'Request',

  /**
   * @description [en_US] Request
   */
  Appr_Lbl_TAndERequestInfo: 'Request',

  /**
   * @description [en_US] Request Number
   */
  Appr_Lbl_TAndERequestNumber: 'Request Number',

  /**
   * @description [en_US] Tax Rate
   */
  Appr_Lbl_TaxRate: 'Tax Rate',

  /**
   * @description [en_US] Time
   */
  Appr_Lbl_Time: 'Time',

  /**
   * @description [en_US] Time Track
   */
  Appr_Lbl_TimeTrack: 'Time Track',

  /**
   * @description [en_US] Timesheet
   */
  Appr_Lbl_Timesheet: 'Timesheet',

  /**
   * @description [en_US] Title
   */
  Appr_Lbl_Title: 'Title',

  /**
   * @description [en_US] Total
   */
  Appr_Lbl_Total: 'Total',

  /**
   * @description [en_US] Amount
   */
  Appr_Lbl_TotalAmount: 'Amount',

  /**
   * @description [en_US] Type
   */
  Appr_Lbl_Type: 'Type',

  /**
   * @description [en_US] User Defined Field 1
   */
  Appr_Lbl_UserDefinedField1: 'User Defined Field 1',

  /**
   * @description [en_US] User Defined Field 2
   */
  Appr_Lbl_UserDefinedField2: 'User Defined Field 2',

  /**
   * @description [en_US] Vendor
   */
  Appr_Lbl_Vendor: 'Vendor',

  /**
   * @description [en_US] Amount excl. Tax
   */
  Appr_Lbl_WithoutTax: 'Amount excl. Tax',

  /**
   * @description [en_US] Work
   */
  Appr_Lbl_Work: 'Work',

  /**
   * @description [en_US] Work Category
   */
  Appr_Lbl_WorkCategory: 'Work Category',

  /**
   * @description [en_US] Attention(See remark line)
   */
  Appr_Msg_DailyAttentionTitle: 'Attention(See remark line)',

  /**
   * @description [en_US] There is no request for approval.
   */
  Appr_Msg_EmptyRequestList: 'There is no request for approval.',

  /**
   * @description [en_US] There are [%1] days when woringtime time range is ineffective.
   */
  Appr_Msg_FixSummaryConfirmIneffectiveWorkingTime: 'There are [%1] days when woringtime time range is ineffective.',

  /**
   * @description [en_US] There are [%1] days when rest time is insufficient.
   */
  Appr_Msg_FixSummaryConfirmInsufficientRestTime: 'There are [%1] days when rest time is insufficient.',

  /**
   * @description [en_US] Add
   */
  Att_Btn_AddItem: 'Add',

  /**
   * @description [en_US] Approve On Behalf
   */
  Att_Btn_ApproveOnBehalf: 'Approve On Behalf',

  /**
   * @description [en_US] Cancel approval
   */
  Att_Btn_CancelApproval: 'Cancel approval',

  /**
   * @description [en_US] Cancel Request
   */
  Att_Btn_CancelRequest: 'Cancel Request',

  /**
   * @description [en_US] Change
   */
  Att_Btn_ChangeRequest: 'Change',

  /**
   * @description [en_US] Display steps for approval
   */
  Att_Btn_DisplayApprovalSteps: 'Display steps for approval',

  /**
   * @description [en_US] Modify
   */
  Att_Btn_ModifyRequest: 'Modify',

  /**
   * @description [en_US] Reapply
   */
  Att_Btn_Reapply: 'Reapply',

  /**
   * @description [en_US] Remove
   */
  Att_Btn_RemoveItem: 'Remove',

  /**
   * @description [en_US] Delete
   */
  Att_Btn_RemoveRequest: 'Delete',

  /**
   * @description [en_US] Request
   */
  Att_Btn_Request: 'Request',

  /**
   * @description [en_US] Request Again
   */
  Att_Btn_RequestAgain: 'Request Again',

  /**
   * @description [en_US] This Month
   */
  Att_Btn_ThisMonth: 'This Month',

  /**
   * @description [en_US] To Yourself
   */
  Att_Btn_ToYourself: 'To Yourself',

  /**
   * @description [en_US] There are requests that were rejected or canceled during the period.
   */
  Att_Err_InvalidRequestExist: 'There are requests that were rejected or canceled during the period.',

  /**
   * @description [en_US] There is a request waiting for approval during the period.
   */
  Att_Err_RequestingRequestExist: 'There is a request waiting for approval during the period.',

  /**
   * @description [en_US] Message is required if you don\'t send the current location.
   */
  Att_Err_RequireCommentWithoutLocation: 'Message is required if you don\'t send the current location.',

  /**
   * @description [en_US] AM leave
   */
  Att_Lbl_AMLeave: 'AM leave',

  /**
   * @description [en_US] Absence
   */
  Att_Lbl_Absence: 'Absence',

  /**
   * @description [en_US] Actual Work
   */
  Att_Lbl_ActualWork: 'Actual Work',

  /**
   * @description [en_US] Add rest time
   */
  Att_Lbl_AddRestTime: 'Add rest time',

  /**
   * @description [en_US] Annual Paid Leave
   */
  Att_Lbl_AnnualPaidLeave: 'Annual Paid Leave',

  /**
   * @description [en_US] Annual Paid Leave days
   */
  Att_Lbl_AnnualPaidLeaveDays: 'Annual Paid Leave days',

  /**
   * @description [en_US] Annual Paid Leave days left
   */
  Att_Lbl_AnnualPaidLeaveDaysLeft: 'Annual Paid Leave days left',

  /**
   * @description [en_US] Steps for approval
   */
  Att_Lbl_ApprovalSteps: 'Steps for approval',

  /**
   * @description [en_US] Approval In
   */
  Att_Lbl_ApprovelIn: 'Approval In',

  /**
   * @description [en_US] Work Pattern
   */
  Att_Lbl_AttPattern: 'Work Pattern',

  /**
   * @description [en_US] Attendance
   */
  Att_Lbl_Attendance: 'Attendance',

  /**
   * @description [en_US] End time
   */
  Att_Lbl_AttendanceEndTime: 'End time',

  /**
   * @description [en_US] Work Arrangement Request List
   */
  Att_Lbl_AttendanceRequestList: 'Work Arrangement Request List',

  /**
   * @description [en_US] Start time
   */
  Att_Lbl_AttendanceStartTime: 'Start time',

  /**
   * @description [en_US] Off-office
   */
  Att_Lbl_BreakLost: 'Off-office',

  /**
   * @description [en_US] Off-office Count
   */
  Att_Lbl_BreakLostCount: 'Off-office Count',

  /**
   * @description [en_US] Off-office time (deducted)
   */
  Att_Lbl_BreakLostTime: 'Off-office time (deducted)',

  /**
   * @description [en_US] Off-office time
   */
  Att_Lbl_BreakTime: 'Off-office time',

  /**
   * @description [en_US] Canceled
   */
  Att_Lbl_CancelRequest: 'Canceled',

  /**
   * @description [en_US] Closing Date
   */
  Att_Lbl_ClosingDate: 'Closing Date',

  /**
   * @description [en_US] Compensatory
   */
  Att_Lbl_Compensatory: 'Compensatory',

  /**
   * @description [en_US] Contracted Work Hours
   */
  Att_Lbl_ContractedWorkHours: 'Contracted Work Hours',

  /**
   * @description [en_US] Contracted Work Days
   */
  Att_Lbl_ContractualWorkDays: 'Contracted Work Days',

  /**
   * @description [en_US] Count Type
   */
  Att_Lbl_CountType: 'Count Type',

  /**
   * @description [en_US] Virtual Work Time
   */
  Att_Lbl_DailyVirtualWorkTime: 'Virtual Work Time',

  /**
   * @description [en_US] Date
   */
  Att_Lbl_Date: 'Date',

  /**
   * @description [en_US] Day leave
   */
  Att_Lbl_DayLeave: 'Day leave',

  /**
   * @description [en_US] Days Left
   */
  Att_Lbl_DaysLeft: 'Days Left',

  /**
   * @description [en_US] Days Granted
   */
  Att_Lbl_DaysManagedDaysGranted: 'Days Granted',

  /**
   * @description [en_US] Days Left
   */
  Att_Lbl_DaysManagedDaysLeft: 'Days Left',

  /**
   * @description [en_US] Days Taken
   */
  Att_Lbl_DaysManagedDaysTaken: 'Days Taken',

  /**
   * @description [en_US] Valid From
   */
  Att_Lbl_DaysManagedValidDateFrom: 'Valid From',

  /**
   * @description [en_US] Valid To
   */
  Att_Lbl_DaysManagedValidDateTo: 'Valid To',

  /**
   * @description [en_US] Deducted
   */
  Att_Lbl_Deducted: 'Deducted',

  /**
   * @description [en_US] Work Hours Difference
   */
  Att_Lbl_DifferenceTime: 'Work Hours Difference',

  /**
   * @description [en_US] Do not use
   */
  Att_Lbl_DoNotUseReplacementDayOff: 'Do not use',

  /**
   * @description [en_US] Duration
   */
  Att_Lbl_Duration: 'Duration',

  /**
   * @description [en_US] Early Leave
   */
  Att_Lbl_EarlyLeave: 'Early Leave',

  /**
   * @description [en_US] Early Leave Count
   */
  Att_Lbl_EarlyLeaveCount: 'Early Leave Count',

  /**
   * @description [en_US] Early Leave Time (deducted)
   */
  Att_Lbl_EarlyLeaveLostTime: 'Early Leave Time (deducted)',

  /**
   * @description [en_US] Early Leave Time
   */
  Att_Lbl_EarlyLeaveTime: 'Early Leave Time',

  /**
   * @description [en_US] End time
   */
  Att_Lbl_EndTime: 'End time',

  /**
   * @description [en_US] Event
   */
  Att_Lbl_Event: 'Event',

  /**
   * @description [en_US] Accuracy
   */
  Att_Lbl_FetchLocationAccuracy: 'Accuracy',

  /**
   * @description [en_US] Failed to fetch your current location
   */
  Att_Lbl_FetchLocationFail: 'Failed to fetch your current location',

  /**
   * @description [en_US] Ensure that the device allows to fetch the current location.
   */
  Att_Lbl_FetchLocationFailRemarks: 'Ensure that the device allows to fetch the current location.',

  /**
   * @description [en_US] Fetched at
   */
  Att_Lbl_FetchLocationFetchTime: 'Fetched at',

  /**
   * @description [en_US] Fetching your current location...
   */
  Att_Lbl_FetchLocationFetching: 'Fetching your current location...',

  /**
   * @description [en_US] Successfully fetched your current location
   */
  Att_Lbl_FetchLocationSuccess: 'Successfully fetched your current location',

  /**
   * @description [en_US] First Half of Day Leave
   */
  Att_Lbl_FirstHalfOfDayLeave: 'First Half of Day Leave',

  /**
   * @description [en_US] Full Day Leave
   */
  Att_Lbl_FullDayLeave: 'Full Day Leave',

  /**
   * @description [en_US] Paid Leave Days
   */
  Att_Lbl_GeneralPaidLeaveDays: 'Paid Leave Days',

  /**
   * @description [en_US] Half Day Leave
   */
  Att_Lbl_HalfDayLeave: 'Half Day Leave',

  /**
   * @description [en_US] Work on Contracted Holidays
   */
  Att_Lbl_HolidayWorkCount: 'Work on Contracted Holidays',

  /**
   * @description [en_US] Holiday Work Date
   */
  Att_Lbl_HolidayWorkDate: 'Holiday Work Date',

  /**
   * @description [en_US] Holiday Work Time
   */
  Att_Lbl_HolidayWorkTime: 'Holiday Work Time',

  /**
   * @description [en_US] Hourly Designated Leave
   */
  Att_Lbl_HourlyDesignatedLeave: 'Hourly Designated Leave',

  /**
   * @description [en_US] Hourly Leave
   */
  Att_Lbl_HourlyLeave: 'Hourly Leave',

  /**
   * @description [en_US] Input Attendance
   */
  Att_Lbl_InputAttendance: 'Input Attendance',

  /**
   * @description [en_US] Insufficient Rest Time
   */
  Att_Lbl_InsufficientRestTime: 'Insufficient Rest Time',

  /**
   * @description [en_US] Late Arrival
   */
  Att_Lbl_LateArrival: 'Late Arrival',

  /**
   * @description [en_US] Late Count
   */
  Att_Lbl_LateArriveCount: 'Late Count',

  /**
   * @description [en_US] Late Time (deducted)
   */
  Att_Lbl_LateArriveLostTime: 'Late Time (deducted)',

  /**
   * @description [en_US] Late Time
   */
  Att_Lbl_LateArriveTime: 'Late Time',

  /**
   * @description [en_US] Late-night
   */
  Att_Lbl_LateNight: 'Late-night',

  /**
   * @description [en_US] Days
   */
  Att_Lbl_LeaveDays: 'Days',

  /**
   * @description [en_US] Leave Details
   */
  Att_Lbl_LeaveDetails: 'Leave Details',

  /**
   * @description [en_US] Leave time (deducted)
   */
  Att_Lbl_LeaveLostTime: 'Leave time (deducted)',

  /**
   * @description [en_US] Leave Taken Details
   */
  Att_Lbl_LeaveTakenDetails: 'Leave Taken Details',

  /**
   * @description [en_US] Leave Type
   */
  Att_Lbl_LeaveType: 'Leave Type',

  /**
   * @description [en_US] Legal Holiday
   */
  Att_Lbl_LegalHoliday: 'Legal Holiday',

  /**
   * @description [en_US] Work on Legal Holidays
   */
  Att_Lbl_LegalHolidayWorkCount: 'Work on Legal Holidays',

  /**
   * @description [en_US] Excess Legal Overtime Hours (in month)
   */
  Att_Lbl_LegalOverTime: 'Excess Legal Overtime Hours (in month)',

  /**
   * @description [en_US] Legal Work Hours
   */
  Att_Lbl_LegalWorkHours: 'Legal Work Hours',

  /**
   * @description [en_US] Managed Leave
   */
  Att_Lbl_ManagedLeave: 'Managed Leave',

  /**
   * @description [en_US] Attendance Report
   */
  Att_Lbl_MonthlySummary: 'Attendance Report',

  /**
   * @description [en_US] Monthly Summary Status
   */
  Att_Lbl_MonthlySummaryStatus: 'Monthly Summary Status',

  /**
   * @description [en_US] New Request
   */
  Att_Lbl_NewRequest: 'New Request',

  /**
   * @description [en_US] Next approval user
   */
  Att_Lbl_NextApproverEmployee: 'Next approval user',

  /**
   * @description [en_US] Next Month
   */
  Att_Lbl_NextMonth: 'Next Month',

  /**
   * @description [en_US] NotWorkDay
   */
  Att_Lbl_NotWorkDay: 'NotWorkDay',

  /**
   * @description [en_US] Other rest time
   */
  Att_Lbl_OtherRestTime: 'Other rest time',

  /**
   * @description [en_US] Other rest time (minutes)
   */
  Att_Lbl_OtherRestTimeWithMinutes: 'Other rest time (minutes)',

  /**
   * @description [en_US] Legal Overtime Hours
   */
  Att_Lbl_OutWorkTime01: 'Legal Overtime Hours',

  /**
   * @description [en_US] Excess Legal Overtime Hours
   */
  Att_Lbl_OutWorkTime02: 'Excess Legal Overtime Hours',

  /**
   * @description [en_US] Work Hours on Legal Holidays
   */
  Att_Lbl_OutWorkTime03: 'Work Hours on Legal Holidays',

  /**
   * @description [en_US] Non-legal Work Hours
   */
  Att_Lbl_OutWorkTime04: 'Non-legal Work Hours',

  /**
   * @description [en_US] Statutory Holidays Work Hours
   */
  Att_Lbl_OutWorkTime05: 'Statutory Holidays Work Hours',

  /**
   * @description [en_US] Late-night Work Hours
   */
  Att_Lbl_OutWorkTime06: 'Late-night Work Hours',

  /**
   * @description [en_US] Compensatory Leave Hours (deducted)
   */
  Att_Lbl_OutWorkTime07: 'Compensatory Leave Hours (deducted)',

  /**
   * @description [en_US] Overtime
   */
  Att_Lbl_Overtime: 'Overtime',

  /**
   * @description [en_US] PM leave
   */
  Att_Lbl_PMLeave: 'PM leave',

  /**
   * @description [en_US] Paid
   */
  Att_Lbl_Paid: 'Paid',

  /**
   * @description [en_US] Paid Day-off
   */
  Att_Lbl_PaidDayOff: 'Paid Day-off',

  /**
   * @description [en_US] Period
   */
  Att_Lbl_Period: 'Period',

  /**
   * @description [en_US] Flex-time Work Hours
   */
  Att_Lbl_PlainTime: 'Flex-time Work Hours',

  /**
   * @description [en_US] Prev Month
   */
  Att_Lbl_PrevMonth: 'Prev Month',

  /**
   * @description [en_US] Excess Legal Overtime Hours (in quarter)
   */
  Att_Lbl_QuarterLegalOverTime: 'Excess Legal Overtime Hours (in quarter)',

  /**
   * @description [en_US] Range
   */
  Att_Lbl_Range: 'Range',

  /**
   * @description [en_US] Actual Work Days
   */
  Att_Lbl_RealWorkDays: 'Actual Work Days',

  /**
   * @description [en_US] Actual Work Hours
   */
  Att_Lbl_RealWorkTime: 'Actual Work Hours',

  /**
   * @description [en_US] Reason
   */
  Att_Lbl_Reason: 'Reason',

  /**
   * @description [en_US] Remarks
   */
  Att_Lbl_Remarks: 'Remarks',

  /**
   * @description [en_US] Removed
   */
  Att_Lbl_RemoveRequest: 'Removed',

  /**
   * @description [en_US] Replacement Day Off
   */
  Att_Lbl_ReplacementDayOff: 'Replacement Day Off',

  /**
   * @description [en_US] Approved
   */
  Att_Lbl_ReqStatApproved: 'Approved',

  /**
   * @description [en_US] Canceled
   */
  Att_Lbl_ReqStatCanceled: 'Canceled',

  /**
   * @description [en_US] Not Requested
   */
  Att_Lbl_ReqStatNotRequested: 'Not Requested',

  /**
   * @description [en_US] Pending
   */
  Att_Lbl_ReqStatPending: 'Pending',

  /**
   * @description [en_US] Rejected
   */
  Att_Lbl_ReqStatRejected: 'Rejected',

  /**
   * @description [en_US] Removed
   */
  Att_Lbl_ReqStatRemoved: 'Removed',

  /**
   * @description [en_US] Request
   */
  Att_Lbl_Request: 'Request',

  /**
   * @description [en_US] Request / Event
   */
  Att_Lbl_RequestAndEvent: 'Request / Event',

  /**
   * @description [en_US] Request for Approval
   */
  Att_Lbl_RequestForApproval: 'Request for Approval',

  /**
   * @description [en_US] Requests
   */
  Att_Lbl_RequestList: 'Requests',

  /**
   * @description [en_US] Requested
   */
  Att_Lbl_Requested: 'Requested',

  /**
   * @description [en_US] Rest
   */
  Att_Lbl_Rest: 'Rest',

  /**
   * @description [en_US] Rest time
   */
  Att_Lbl_RestTime: 'Rest time',

  /**
   * @description [en_US] Excess Time on Safety Obligation
   */
  Att_Lbl_SafetyObligationalExcessTime: 'Excess Time on Safety Obligation',

  /**
   * @description [en_US] Saved
   */
  Att_Lbl_SaveWorkTime: 'Saved',

  /**
   * @description [en_US] Scheduled Date Of Substitute
   */
  Att_Lbl_ScheduledDateOfSubstitute: 'Scheduled Date Of Substitute',

  /**
   * @description [en_US] Second Half of Day Leave
   */
  Att_Lbl_SecondHalfOfDayLeave: 'Second Half of Day Leave',

  /**
   * @description [en_US] Send the current location
   */
  Att_Lbl_SendLocation: 'Send the current location',

  /**
   * @description [en_US] Shift / Short Time Work
   */
  Att_Lbl_ShiftAndShortTimeWork: 'Shift / Short Time Work',

  /**
   * @description [en_US] Shortened Work Time (Short Time Work)
   */
  Att_Lbl_ShortenedWorkTime: 'Shortened Work Time (Short Time Work)',

  /**
   * @description [en_US] Message
   */
  Att_Lbl_StampMessage: 'Message',

  /**
   * @description [en_US] Start time
   */
  Att_Lbl_StartTime: 'Start time',

  /**
   * @description [en_US] Status
   */
  Att_Lbl_Status: 'Status',

  /**
   * @description [en_US] Statutory Holiday
   */
  Att_Lbl_StatutoryHoliday: 'Statutory Holiday',

  /**
   * @description [en_US] Submitted Requests
   */
  Att_Lbl_Submitted: 'Submitted Requests',

  /**
   * @description [en_US] Substitute
   */
  Att_Lbl_Substitute: 'Substitute',

  /**
   * @description [en_US] Summary Item No
   */
  Att_Lbl_SummaryItemNo: 'Summary Item No',

  /**
   * @description [en_US] Take
   */
  Att_Lbl_Take: 'Take',

  /**
   * @description [en_US] Target Month
   */
  Att_Lbl_TargetMonth: 'Target Month',

  /**
   * @description [en_US] Time & Attendance
   */
  Att_Lbl_TimeAttendance: 'Time & Attendance',

  /**
   * @description [en_US] In
   */
  Att_Lbl_TimeIn: 'In',

  /**
   * @description [en_US] Time leave
   */
  Att_Lbl_TimeLeave: 'Time leave',

  /**
   * @description [en_US] Out
   */
  Att_Lbl_TimeOut: 'Out',

  /**
   * @description [en_US] Time Tracking
   */
  Att_Lbl_TimeTrack: 'Time Tracking',

  /**
   * @description [en_US] Total
   */
  Att_Lbl_Total: 'Total',

  /**
   * @description [en_US] Unpaid
   */
  Att_Lbl_Unpaid: 'Unpaid',

  /**
   * @description [en_US] Unpaid Leave Days
   */
  Att_Lbl_UnpaidLeaveDays: 'Unpaid Leave Days',

  /**
   * @description [en_US] Use period
   */
  Att_Lbl_UsePeriod: 'Use period',

  /**
   * @description [en_US] On-office Hours
   */
  Att_Lbl_VirtualWorkTime: 'On-office Hours',

  /**
   * @description [en_US] Work Hours included Article 36
   */
  Att_Lbl_WholeLegalWorkTime: 'Work Hours included Article 36',

  /**
   * @description [en_US] Absence Days
   */
  Att_Lbl_WorkAbsenceDays: 'Absence Days',

  /**
   * @description [en_US] Work Time
   */
  Att_Lbl_WorkHours: 'Work Time',

  /**
   * @description [en_US] Work Scheme
   */
  Att_Lbl_WorkScheme: 'Work Scheme',

  /**
   * @description [en_US] Work time
   */
  Att_Lbl_WorkTime: 'Work time',

  /**
   * @description [en_US] Work Time Input
   */
  Att_Lbl_WorkTimeInput: 'Work Time Input',

  /**
   * @description [en_US] Working Type
   */
  Att_Lbl_WorkingType: 'Working Type',

  /**
   * @description [en_US] Excess Count on Legal Work Hours
   */
  Att_Lbl_YearLegalOverCount: 'Excess Count on Legal Work Hours',

  /**
   * @description [en_US] Excess Time on Legal Work Hours
   */
  Att_Lbl_YearLegalOverTime: 'Excess Time on Legal Work Hours',

  /**
   * @description [en_US] Attention
   */
  Att_Msg_DailyAttention: 'Attention',

  /**
   * @description [en_US] Are you sure to delete this request? This operation can not be undone.
   */
  Att_Msg_DailyReqConfirmRemove: 'Are you sure to delete this request? This operation can not be undone.',

  /**
   * @description [en_US] There are the following notes. Are you sure to submit Attendance Request?
   */
  Att_Msg_FixSummaryConfirm: 'There are the following notes. Are you sure to submit Attendance Request?',

  /**
   * @description [en_US] Rest time is insufficient by [%1] min.
   */
  Att_Msg_InsufficientRestTime: 'Rest time is insufficient by [%1] min.',

  /**
   * @description [en_US] Multiple attention message on day. click here and checking
   */
  Att_Msg_MultipulAttentionMessage: 'Multiple attention message on day. click here and checking',

  /**
   * @description [en_US] No requests available
   */
  Att_Msg_NoRequestAvailable: 'No requests available',

  /**
   * @description [en_US] No requests submitted
   */
  Att_Msg_NoRequestSubmitted: 'No requests submitted',

  /**
   * @description [en_US] The times from [%1] to [%2] is not included in working hours.
   */
  Att_Msg_NotIncludeWorkingTime: 'The times from [%1] to [%2] is not included in working hours.',

  /**
   * @description [en_US] The acquisition time in minutes is rounded up. Example: 9:30-11:00 -> 2 hours
   */
  Att_Msg_RoundingUpNotice: 'The acquisition time in minutes is rounded up. Example: 9:30-11:00 -> 2 hours',

  /**
   * @description [en_US] Select request type from list at left side.
   */
  Att_Msg_SelectRequestTypeFromList: 'Select request type from list at left side.',

  /**
   * @description [en_US] [Ineffective Working Time range: [%1] ~ [%2]]
   */
  Att_Msg_SummaryCommentIneffectiveWorkingTime: '[Ineffective Working Time range: [%1] ~ [%2]]',

  /**
   * @description [en_US] [Insufficient Rest Time: [%1] min]
   */
  Att_Msg_SummaryCommentInsufficientRestTime: '[Insufficient Rest Time: [%1] min]',

  /**
   * @description [en_US] Start time of attendance has been stamped
   */
  Att_Msg_TimeStampDoneIn: 'Start time of attendance has been stamped',

  /**
   * @description [en_US] End time of attendance has been stamped
   */
  Att_Msg_TimeStampDoneOut: 'End time of attendance has been stamped',

  /**
   * @description [en_US] Restart time of attendance has been stamped
   */
  Att_Msg_TimeStampDoneRein: 'Restart time of attendance has been stamped',

  /**
   * @description [en_US] Please revise the request or delete it.
   */
  Att_Slt_InvalidRequestExist: 'Please revise the request or delete it.',

  /**
   * @description [en_US] Please wait for approval of the request.
   */
  Att_Slt_RequestingRequestExist: 'Please wait for approval of the request.',

  /**
   * @description [en_US] The work hours you entered are different from actual work hours.
   */
  Att_TimeTracking_Hint: 'The work hours you entered are different from actual work hours.',

  /**
   * @description [en_US] Time Stamp
   */
  Att_Title_TimeStamp: 'Time Stamp',

  /**
   * @description [en_US] Completed
   */
  Batch_Lbl_Completed: 'Completed',

  /**
   * @description [en_US] Failed
   */
  Batch_Lbl_Failed: 'Failed',

  /**
   * @description [en_US] Processing
   */
  Batch_Lbl_Processing: 'Processing',

  /**
   * @description [en_US] Waiting
   */
  Batch_Lbl_Waiting: 'Waiting',

  /**
   * @description [en_US] Cancel
   */
  Cal_Btn_Cancel: 'Cancel',

  /**
   * @description [en_US] Daily Request
   */
  Cal_Btn_Daily: 'Daily Request',

  /**
   * @description [en_US] D
   */
  Cal_Btn_Day: 'D',

  /**
   * @description [en_US] Delete
   */
  Cal_Btn_Delete: 'Delete',

  /**
   * @description [en_US] Exchange Request
   */
  Cal_Btn_Exchange: 'Exchange Request',

  /**
   * @description [en_US] Holiday Work Request
   */
  Cal_Btn_HolidayWork: 'Holiday Work Request',

  /**
   * @description [en_US] Leave Request
   */
  Cal_Btn_Leave: 'Leave Request',

  /**
   * @description [en_US] Menu
   */
  Cal_Btn_Menu: 'Menu',

  /**
   * @description [en_US] Save
   */
  Cal_Btn_Save: 'Save',

  /**
   * @description [en_US] Save and Record End Timestamp
   */
  Cal_Btn_SaveAndRecordEndTimestamp: 'Save and Record End Timestamp',

  /**
   * @description [en_US] Settle
   */
  Cal_Btn_Settle: 'Settle',

  /**
   * @description [en_US] T&E Request
   */
  Cal_Btn_TERequest: 'T&E Request',

  /**
   * @description [en_US] Account
   */
  Cal_Lbl_Account: 'Account',

  /**
   * @description [en_US] Actual Work Hours
   */
  Cal_Lbl_ActualWorkingHours: 'Actual Work Hours',

  /**
   * @description [en_US] Address
   */
  Cal_Lbl_Address: 'Address',

  /**
   * @description [en_US] All Day
   */
  Cal_Lbl_AllDay: 'All Day',

  /**
   * @description [en_US] Amount
   */
  Cal_Lbl_Amount: 'Amount',

  /**
   * @description [en_US] Attached files
   */
  Cal_Lbl_AttachedFiles: 'Attached files',

  /**
   * @description [en_US] Couldn\'t get Calendar Events completely
   */
  Cal_Lbl_CalendarEventSyncErrorTitle: 'Couldn\'t get Calendar Events completely',

  /**
   * @description [en_US] Cost Center
   */
  Cal_Lbl_CostCenter: 'Cost Center',

  /**
   * @description [en_US] Current Time
   */
  Cal_Lbl_CurrentTime: 'Current Time',

  /**
   * @description [en_US] Customer
   */
  Cal_Lbl_Customer: 'Customer',

  /**
   * @description [en_US] Daily Summary
   */
  Cal_Lbl_DailySummary: 'Daily Summary',

  /**
   * @description [en_US] Date
   */
  Cal_Lbl_Date: 'Date',

  /**
   * @description [en_US] Description
   */
  Cal_Lbl_Description: 'Description',

  /**
   * @description [en_US] Details
   */
  Cal_Lbl_Details: 'Details',

  /**
   * @description [en_US] Direct
   */
  Cal_Lbl_Direct: 'Direct',

  /**
   * @description [en_US] Direct Cost
   */
  Cal_Lbl_DirectCharge: 'Direct Cost',

  /**
   * @description [en_US] Comment
   */
  Cal_Lbl_EndTimestampComment: 'Comment',

  /**
   * @description [en_US] Event
   */
  Cal_Lbl_Event: 'Event',

  /**
   * @description [en_US] Expense
   */
  Cal_Lbl_Expense: 'Expense',

  /**
   * @description [en_US] Expense
   */
  Cal_Lbl_ExpenseGroup: 'Expense',

  /**
   * @description [en_US] Expense Type
   */
  Cal_Lbl_ExpenseType: 'Expense Type',

  /**
   * @description [en_US] Fri
   */
  Cal_Lbl_Fri: 'Fri',

  /**
   * @description [en_US] IMPORTANT
   */
  Cal_Lbl_Important: 'IMPORTANT',

  /**
   * @description [en_US] Indirect
   */
  Cal_Lbl_Indirect: 'Indirect',

  /**
   * @description [en_US] Input Attendance
   */
  Cal_Lbl_InputAttendance: 'Input Attendance',

  /**
   * @description [en_US] Input Event
   */
  Cal_Lbl_InputEvent: 'Input Event',

  /**
   * @description [en_US] Job
   */
  Cal_Lbl_Job: 'Job',

  /**
   * @description [en_US] Time Out At
   */
  Cal_Lbl_LeavingWorkTime: 'Time Out At',

  /**
   * @description [en_US] Location
   */
  Cal_Lbl_Location: 'Location',

  /**
   * @description [en_US] Mon
   */
  Cal_Lbl_Mon: 'Mon',

  /**
   * @description [en_US] Month
   */
  Cal_Lbl_Month: 'Month',

  /**
   * @description [en_US] + 
   */
  Cal_Lbl_MoreEvents1: '+ ',

  /**
   * @description [en_US]  more
   */
  Cal_Lbl_MoreEvents2: ' more',

  /**
   * @description [en_US] (No Title)
   */
  Cal_Lbl_NoTitle: '(No Title)',

  /**
   * @description [en_US] NOT IMPORTANT
   */
  Cal_Lbl_NotImportant: 'NOT IMPORTANT',

  /**
   * @description [en_US] NOT URGENT
   */
  Cal_Lbl_NotUrgent: 'NOT URGENT',

  /**
   * @description [en_US] Calendar Event on Office 365
   */
  Cal_Lbl_O365CalendarEvent: 'Calendar Event on Office 365',

  /**
   * @description [en_US] Go Out
   */
  Cal_Lbl_OutOf: 'Go Out',

  /**
   * @description [en_US] Deliverables
   */
  Cal_Lbl_Output: 'Deliverables',

  /**
   * @description [en_US] Number
   */
  Cal_Lbl_OutputNumber: 'Number',

  /**
   * @description [en_US] Count
   */
  Cal_Lbl_OutputUnit: 'Count',

  /**
   * @description [en_US] Overtime
   */
  Cal_Lbl_OvertimeWork: 'Overtime',

  /**
   * @description [en_US] Planner
   */
  Cal_Lbl_Planner: 'Planner',

  /**
   * @description [en_US] Planning
   */
  Cal_Lbl_Planning: 'Planning',

  /**
   * @description [en_US] Quadrants
   */
  Cal_Lbl_Quadrants: 'Quadrants',

  /**
   * @description [en_US] Objectives
   */
  Cal_Lbl_RelatedTarget: 'Objectives',

  /**
   * @description [en_US] Remarks
   */
  Cal_Lbl_Remarks: 'Remarks',

  /**
   * @description [en_US] Request items
   */
  Cal_Lbl_RequestItems: 'Request items',

  /**
   * @description [en_US] Early Leave Request
   */
  Cal_Lbl_RequestsLeaveEarly: 'Early Leave Request',

  /**
   * @description [en_US] Overtime Request
   */
  Cal_Lbl_RequestsOvertime: 'Overtime Request',

  /**
   * @description [en_US] Sat
   */
  Cal_Lbl_Sat: 'Sat',

  /**
   * @description [en_US] Report expenses
   */
  Cal_Lbl_SettleExpenses: 'Report expenses',

  /**
   * @description [en_US] Sun
   */
  Cal_Lbl_Sun: 'Sun',

  /**
   * @description [en_US] Task
   */
  Cal_Lbl_Task: 'Task',

  /**
   * @description [en_US] Temporary Work Hours
   */
  Cal_Lbl_TemporaryWorkHours: 'Temporary Work Hours',

  /**
   * @description [en_US] Thu
   */
  Cal_Lbl_Thu: 'Thu',

  /**
   * @description [en_US] Hours
   */
  Cal_Lbl_Time: 'Hours',

  /**
   * @description [en_US] Time Track
   */
  Cal_Lbl_TimeTrack: 'Time Track',

  /**
   * @description [en_US] Title
   */
  Cal_Lbl_Title: 'Title',

  /**
   * @description [en_US] Today
   */
  Cal_Lbl_Today: 'Today',

  /**
   * @description [en_US] Total Work Hours
   */
  Cal_Lbl_TotalWorkingHours: 'Total Work Hours',

  /**
   * @description [en_US] Transportation Expense
   */
  Cal_Lbl_TransportationExpense: 'Transportation Expense',

  /**
   * @description [en_US] Tue
   */
  Cal_Lbl_Tue: 'Tue',

  /**
   * @description [en_US] URGENT
   */
  Cal_Lbl_Urgent: 'URGENT',

  /**
   * @description [en_US] Customer
   */
  Cal_Lbl_VisitDestination: 'Customer',

  /**
   * @description [en_US] Wed
   */
  Cal_Lbl_Wed: 'Wed',

  /**
   * @description [en_US] Week
   */
  Cal_Lbl_Week: 'Week',

  /**
   * @description [en_US] Work
   */
  Cal_Lbl_Work: 'Work',

  /**
   * @description [en_US] Work Arrangements
   */
  Cal_Lbl_WorkArrangements: 'Work Arrangements',

  /**
   * @description [en_US] Job Detail
   */
  Cal_Lbl_WorkCategory: 'Job Detail',

  /**
   * @description [en_US] Job Report
   */
  Cal_Lbl_WorkReport: 'Job Report',

  /**
   * @description [en_US] Time Tracking
   */
  Cal_Lbl_WorkTimeInput: 'Time Tracking',

  /**
   * @description [en_US] Expense Request
   */
  Cal_Mnu_ExpenseRequest: 'Expense Request',

  /**
   * @description [en_US] Other Request
   */
  Cal_Mnu_OtherRequest: 'Other Request',

  /**
   * @description [en_US] Cannot edit the event created by others.
   */
  Cal_Msg_CannotEditInvitedEvent: 'Cannot edit the event created by others.',

  /**
   * @description [en_US] The rest of work hours will be attached to this job
   */
  Cal_Msg_DefaultJobDescription: 'The rest of work hours will be attached to this job',

  /**
   * @description [en_US] Are you sure you want to delete this event?
   */
  Cal_Msg_DeleteEvent: 'Are you sure you want to delete this event?',

  /**
   * @description [en_US] You cannot create an event that ends before it starts.
   */
  Cal_Msg_InvalidStartEndTime: 'You cannot create an event that ends before it starts.',

  /**
   * @description [en_US] Submit when you leave (left) early.
   */
  Cal_Txt_RequestsLeaveEarly: 'Submit when you leave (left) early.',

  /**
   * @description [en_US] Submit overtime request
   */
  Cal_Txt_RequestsOvertime: 'Submit overtime request',

  /**
   * @description [en_US] Displaying [%1] to [%2] of [%3] results
   */
  Cmn_Lbl_PagerInfo: 'Displaying [%1] to [%2] of [%3] results',

  /**
   * @description [en_US] no search result.
   */
  Cmn_Lbl_SuggestNoResult: 'no search result.',

  /**
   * @description [en_US] Add
   */
  Com_Btn_Add: 'Add',

  /**
   * @description [en_US] Add New
   */
  Com_Btn_AddNew: 'Add New',

  /**
   * @description [en_US] Cancel
   */
  Com_Btn_ApprovalCancel: 'Cancel',

  /**
   * @description [en_US] Approval History
   */
  Com_Btn_ApprovalHistory: 'Approval History',

  /**
   * @description [en_US] Cancel
   */
  Com_Btn_Cancel: 'Cancel',

  /**
   * @description [en_US] Change
   */
  Com_Btn_Change: 'Change',

  /**
   * @description [en_US] Change Approver
   */
  Com_Btn_ChangeApprover: 'Change Approver',

  /**
   * @description [en_US] In
   */
  Com_Btn_ClockIn: 'In',

  /**
   * @description [en_US] Out
   */
  Com_Btn_ClockOut: 'Out',

  /**
   * @description [en_US] Re-in
   */
  Com_Btn_ClockRein: 'Re-in',

  /**
   * @description [en_US] Close
   */
  Com_Btn_Close: 'Close',

  /**
   * @description [en_US] Confirm
   */
  Com_Btn_Confirm: 'Confirm',

  /**
   * @description [en_US] Contract
   */
  Com_Btn_Contract: 'Contract',

  /**
   * @description [en_US] Copy
   */
  Com_Btn_Copy: 'Copy',

  /**
   * @description [en_US] Select
   */
  Com_Btn_Decide: 'Select',

  /**
   * @description [en_US] Delete
   */
  Com_Btn_Delete: 'Delete',

  /**
   * @description [en_US] Discard
   */
  Com_Btn_Discard: 'Discard',

  /**
   * @description [en_US] Edit
   */
  Com_Btn_Edit: 'Edit',

  /**
   * @description [en_US] Execute
   */
  Com_Btn_Execute: 'Execute',

  /**
   * @description [en_US] Exit
   */
  Com_Btn_Exit: 'Exit',

  /**
   * @description [en_US] Expand
   */
  Com_Btn_Expand: 'Expand',

  /**
   * @description [en_US] Multi Edit
   */
  Com_Btn_MultiEdit: 'Multi Edit',

  /**
   * @description [en_US] New
   */
  Com_Btn_New: 'New',

  /**
   * @description [en_US] New Request
   */
  Com_Btn_NewRequest: 'New Request',

  /**
   * @description [en_US] OK
   */
  Com_Btn_Ok: 'OK',

  /**
   * @description [en_US] Open
   */
  Com_Btn_Open: 'Open',

  /**
   * @description [en_US] Open Personal Menu
   */
  Com_Btn_OpenPersonalMenu: 'Open Personal Menu',

  /**
   * @description [en_US] Personal Setting
   */
  Com_Btn_PersonalSetting: 'Personal Setting',

  /**
   * @description [en_US] Print
   */
  Com_Btn_Print: 'Print',

  /**
   * @description [en_US] Print
   */
  Com_Btn_PrintDo: 'Print',

  /**
   * @description [en_US] Print Page
   */
  Com_Btn_PrintPage: 'Print Page',

  /**
   * @description [en_US] Remove
   */
  Com_Btn_Remove: 'Remove',

  /**
   * @description [en_US] Request
   */
  Com_Btn_Request: 'Request',

  /**
   * @description [en_US] Cancel
   */
  Com_Btn_RequestCancel: 'Cancel',

  /**
   * @description [en_US] Reset
   */
  Com_Btn_Reset: 'Reset',

  /**
   * @description [en_US] Save
   */
  Com_Btn_Save: 'Save',

  /**
   * @description [en_US] Save as New
   */
  Com_Btn_Save_New: 'Save as New',

  /**
   * @description [en_US] Save Overwrite
   */
  Com_Btn_Save_Overwrite: 'Save Overwrite',

  /**
   * @description [en_US] Search
   */
  Com_Btn_Search: 'Search',

  /**
   * @description [en_US] Select
   */
  Com_Btn_Select: 'Select',

  /**
   * @description [en_US] Stamp
   */
  Com_Btn_Stamp: 'Stamp',

  /**
   * @description [en_US] Submit
   */
  Com_Btn_Submit: 'Submit',

  /**
   * @description [en_US] Submit&Add
   */
  Com_Btn_SubmitAndAdd: 'Submit&Add',

  /**
   * @description [en_US] Switch Employee
   */
  Com_Btn_SwitchEmployee: 'Switch Employee',

  /**
   * @description [en_US] Time Tracking
   */
  Com_Btn_TimeTracking: 'Time Tracking',

  /**
   * @description [en_US] Update
   */
  Com_Btn_Update: 'Update',

  /**
   * @description [en_US] Holiday
   */
  Com_DayType_Holiday: 'Holiday',

  /**
   * @description [en_US] Legal holiday
   */
  Com_DayType_LegalHoliday: 'Legal holiday',

  /**
   * @description [en_US] PreferredLegal holiday
   */
  Com_DayType_PreferredLegalHoliday: 'PreferredLegal holiday',

  /**
   * @description [en_US] Workday
   */
  Com_DayType_Workday: 'Workday',

  /**
   * @description [en_US] Error
   */
  Com_Err_ErrorTitle: 'Error',

  /**
   * @description [en_US] Amount is invalid.
   */
  Com_Err_InvalidAmount: 'Amount is invalid.',

  /**
   * @description [en_US] Date is invalid.
   */
  Com_Err_InvalidDate: 'Date is invalid.',

  /**
   * @description [en_US] Decimal number must be within [%1] digits.
   */
  Com_Err_InvalidScale: 'Decimal number must be within [%1] digits.',

  /**
   * @description [en_US] No employees were matched
   */
  Com_Err_NoEmployeesFound: 'No employees were matched',

  /**
   * @description [en_US]  is not entered
   */
  Com_Err_NotEntered: ' is not entered',

  /**
   * @description [en_US] [%1] cannot be found.
   */
  Com_Err_NotFound: '[%1] cannot be found.',

  /**
   * @description [en_US] The timetable could not be obtained. There is a possibility that the target employee does not exist on the start date of this month, or the authority necessary to switch to the target employee is insufficient.
   */
  Com_Err_ProxyPermissionErrorBody: 'The timetable could not be obtained. There is a possibility that the target employee does not exist on the start date of this month, or the authority necessary to switch to the target employee is insufficient.',

  /**
   * @description [en_US] Check the employee\'s hire date and Salesforce user role settings.
   */
  Com_Err_ProxyPermissionErrorSolution: 'Check the employee\'s hire date and Salesforce user role settings.',

  /**
   * @description [en_US] Approval History
   */
  Com_Lbl_ApprovalHistory: 'Approval History',

  /**
   * @description [en_US] Canceled
   */
  Com_Lbl_ApproveCancel: 'Canceled',

  /**
   * @description [en_US] Approved
   */
  Com_Lbl_Approved: 'Approved',

  /**
   * @description [en_US] Arrangement
   */
  Com_Lbl_Arrangement: 'Arrangement',

  /**
   * @description [en_US] as at [%1]
   */
  Com_Lbl_AsAt: 'as at [%1]',

  /**
   * @description [en_US] Attached file(s)
   */
  Com_Lbl_Attachments: 'Attached file(s)',

  /**
   * @description [en_US] Back
   */
  Com_Lbl_Back: 'Back',

  /**
   * @description [en_US] Back to list
   */
  Com_Lbl_BackToList: 'Back to list',

  /**
   * @description [en_US] Canceled
   */
  Com_Lbl_Canceled: 'Canceled',

  /**
   * @description [en_US] Select a file
   */
  Com_Lbl_ChooseFile: 'Select a file',

  /**
   * @description [en_US] Confirm
   */
  Com_Lbl_Confirm: 'Confirm',

  /**
   * @description [en_US] Date
   */
  Com_Lbl_Date: 'Date',

  /**
   * @description [en_US] day
   */
  Com_Lbl_Day: 'day',

  /**
   * @description [en_US] day(s)
   */
  Com_Lbl_Day_s: 'day(s)',

  /**
   * @description [en_US] days
   */
  Com_Lbl_Days: 'days',

  /**
   * @description [en_US] Default Job
   */
  Com_Lbl_DefaultJob: 'Default Job',

  /**
   * @description [en_US] Not Selected
   */
  Com_Lbl_DefaultJobNotSelected: 'Not Selected',

  /**
   * @description [en_US] Default Job
   */
  Com_Lbl_DefaultJobShort: 'Default Job',

  /**
   * @description [en_US] Delegate Approver
   */
  Com_Lbl_DelegateApprover: 'Delegate Approver',

  /**
   * @description [en_US] Delete
   */
  Com_Lbl_DeleteFile: 'Delete',

  /**
   * @description [en_US] Department Code
   */
  Com_Lbl_DepartmentCode: 'Department Code',

  /**
   * @description [en_US] Department Name
   */
  Com_Lbl_DepartmentName: 'Department Name',

  /**
   * @description [en_US] Dismiss
   */
  Com_Lbl_Dismiss: 'Dismiss',

  /**
   * @description [en_US] Done
   */
  Com_Lbl_Done: 'Done',

  /**
   * @description [en_US] Drag and drop files or select
   */
  Com_Lbl_DragAndDrop: 'Drag and drop files or select',

  /**
   * @description [en_US] Employee
   */
  Com_Lbl_Employee: 'Employee',

  /**
   * @description [en_US] Employee Code
   */
  Com_Lbl_EmployeeCode: 'Employee Code',

  /**
   * @description [en_US] Employee Name
   */
  Com_Lbl_EmployeeName: 'Employee Name',

  /**
   * @description [en_US] Employee Search
   */
  Com_Lbl_EmployeeSearch: 'Employee Search',

  /**
   * @description [en_US] Error
   */
  Com_Lbl_Error: 'Error',

  /**
   * @description [en_US] Expense Approval
   */
  Com_Lbl_ExpenseApproval: 'Expense Approval',

  /**
   * @description [en_US] Friday
   */
  Com_Lbl_FRIDAY: 'Friday',

  /**
   * @description [en_US] Finance Approval
   */
  Com_Lbl_FinanceApproval: 'Finance Approval',

  /**
   * @description [en_US] Friday
   */
  Com_Lbl_Friday: 'Friday',

  /**
   * @description [en_US] Grant
   */
  Com_Lbl_Grant: 'Grant',

  /**
   * @description [en_US] hour
   */
  Com_Lbl_Hour: 'hour',

  /**
   * @description [en_US] hour(s)
   */
  Com_Lbl_Hour_s: 'hour(s)',

  /**
   * @description [en_US] hours
   */
  Com_Lbl_Hours: 'hours',

  /**
   * @description [en_US] Information
   */
  Com_Lbl_Information: 'Information',

  /**
   * @description [en_US] Job
   */
  Com_Lbl_Job: 'Job',

  /**
   * @description [en_US] Monday
   */
  Com_Lbl_MONDAY: 'Monday',

  /**
   * @description [en_US] *Up to 10 people
   */
  Com_Lbl_MaxDelegateApproverInfo: '*Up to 10 people',

  /**
   * @description [en_US] Number of delegated approvers can not exceed 10. Please remove some of approvers
   */
  Com_Lbl_MaxDelegateApproverWarning: 'Number of delegated approvers can not exceed 10. Please remove some of approvers',

  /**
   * @description [en_US] min.
   */
  Com_Lbl_Mins: 'min.',

  /**
   * @description [en_US] Please tap and select date
   */
  Com_Lbl_MobilePleaseTapAndSelectDate: 'Please tap and select date',

  /**
   * @description [en_US] Monday
   */
  Com_Lbl_Monday: 'Monday',

  /**
   * @description [en_US] Month
   */
  Com_Lbl_Month: 'Month',

  /**
   * @description [en_US] Month
   */
  Com_Lbl_Months: 'Month',

  /**
   * @description [en_US] Next
   */
  Com_Lbl_NextButton: 'Next',

  /**
   * @description [en_US] No Receipt
   */
  Com_Lbl_NoReceipt: 'No Receipt',

  /**
   * @description [en_US] There is no setting item available
   */
  Com_Lbl_NoSetting: 'There is no setting item available',

  /**
   * @description [en_US] None
   */
  Com_Lbl_None: 'None',

  /**
   * @description [en_US] Not Requested
   */
  Com_Lbl_NotRequested: 'Not Requested',

  /**
   * @description [en_US] Pending
   */
  Com_Lbl_Pending: 'Pending',

  /**
   * @description [en_US] Personal Setting
   */
  Com_Lbl_PersonalSetting: 'Personal Setting',

  /**
   * @description [en_US] Previous
   */
  Com_Lbl_PrevButton: 'Previous',

  /**
   * @description [en_US] Rejected
   */
  Com_Lbl_Rejected: 'Rejected',

  /**
   * @description [en_US] Remarks
   */
  Com_Lbl_Remarks: 'Remarks',

  /**
   * @description [en_US] Remove
   */
  Com_Lbl_Remove: 'Remove',

  /**
   * @description [en_US] Remove
   */
  Com_Lbl_RemoveDelegate: 'Remove',

  /**
   * @description [en_US] Removed
   */
  Com_Lbl_Removed: 'Removed',

  /**
   * @description [en_US] Request Approval
   */
  Com_Lbl_RequestApproval: 'Request Approval',

  /**
   * @description [en_US] Required
   */
  Com_Lbl_Required: 'Required',

  /**
   * @description [en_US] Route
   */
  Com_Lbl_Route: 'Route',

  /**
   * @description [en_US] Saturday
   */
  Com_Lbl_SATURDAY: 'Saturday',

  /**
   * @description [en_US] Sunday
   */
  Com_Lbl_SUNDAY: 'Sunday',

  /**
   * @description [en_US] Saturday
   */
  Com_Lbl_Saturday: 'Saturday',

  /**
   * @description [en_US] Fill
   */
  Com_Lbl_SaveRestHours: 'Fill',

  /**
   * @description [en_US] Search
   */
  Com_Lbl_Search: 'Search',

  /**
   * @description [en_US] Search approver employees
   */
  Com_Lbl_SearchApproverEmployee: 'Search approver employees',

  /**
   * @description [en_US] Search Condititons
   */
  Com_Lbl_SearchConditions: 'Search Condititons',

  /**
   * @description [en_US] Search Employee
   */
  Com_Lbl_SearchEmployee: 'Search Employee',

  /**
   * @description [en_US] Select
   */
  Com_Lbl_Select: 'Select',

  /**
   * @description [en_US] Show Employees in Same Department
   */
  Com_Lbl_ShowEmployeesInSameDepartment: 'Show Employees in Same Department',

  /**
   * @description [en_US] Status
   */
  Com_Lbl_Status: 'Status',

  /**
   * @description [en_US] Sunday
   */
  Com_Lbl_Sunday: 'Sunday',

  /**
   * @description [en_US] Switch Employee
   */
  Com_Lbl_SwitchEmployee: 'Switch Employee',

  /**
   * @description [en_US] Thursday
   */
  Com_Lbl_THURSDAY: 'Thursday',

  /**
   * @description [en_US] Tuesday
   */
  Com_Lbl_TUESDAY: 'Tuesday',

  /**
   * @description [en_US] Thursday
   */
  Com_Lbl_Thursday: 'Thursday',

  /**
   * @description [en_US] times
   */
  Com_Lbl_Times: 'times',

  /**
   * @description [en_US] Title
   */
  Com_Lbl_Title: 'Title',

  /**
   * @description [en_US] Too many search results. Please refine your search criteria and try again.
   */
  Com_Lbl_TooManySearchResults: 'Too many search results. Please refine your search criteria and try again.',

  /**
   * @description [en_US] Tuesday
   */
  Com_Lbl_Tuesday: 'Tuesday',

  /**
   * @description [en_US] Type
   */
  Com_Lbl_Type: 'Type',

  /**
   * @description [en_US] Un Approved
   */
  Com_Lbl_UnApproved: 'Un Approved',

  /**
   * @description [en_US] This page is currently not available.
   */
  Com_Lbl_Unavailable: 'This page is currently not available.',

  /**
   * @description [en_US] Still wrong...
   */
  Com_Lbl_UnexpectedErrorPageHint: 'Still wrong...',

  /**
   * @description [en_US] Please ask the system admin about the below log.
   */
  Com_Lbl_UnexpectedErrorPageHintDescription: 'Please ask the system admin about the below log.',

  /**
   * @description [en_US] Please reload the browser and try again.
   */
  Com_Lbl_UnexpectedErrorPageSolution: 'Please reload the browser and try again.',

  /**
   * @description [en_US] Something went wrong.
   */
  Com_Lbl_UnexpectedErrorPageTitle: 'Something went wrong.',

  /**
   * @description [en_US]  
   */
  Com_Lbl_UnitHours: ' ',

  /**
   * @description [en_US]  
   */
  Com_Lbl_UnitMins: ' ',

  /**
   * @description [en_US]  
   */
  Com_Lbl_UnitTimeHours: ' ',

  /**
   * @description [en_US] Unspecified
   */
  Com_Lbl_Unspecified: 'Unspecified',

  /**
   * @description [en_US] Wednesday
   */
  Com_Lbl_WEDNESDAY: 'Wednesday',

  /**
   * @description [en_US] Wednesday
   */
  Com_Lbl_Wednesday: 'Wednesday',

  /**
   * @description [en_US] Week
   */
  Com_Lbl_Week: 'Week',

  /**
   * @description [en_US] 0 Search Result
   */
  Com_Lbl_ZeroSearchResult: '0 Search Result',

  /**
   * @description [en_US] Rest time is [%1] minute(s) insufficient: Fill enough rest time automatically? \nOr cancel to put actual rest time on the timesheet manually.
   */
  Com_Msg_InsufficientRestTime: 'Rest time is [%1] minute(s) insufficient: Fill enough rest time automatically? \nOr cancel to put actual rest time on the timesheet manually.',

  /**
   * @description [en_US] Loading...
   */
  Com_Msg_LoadingPage: 'Loading...',

  /**
   * @description [en_US] Operating as [%1]
   */
  Com_Msg_OperatingAs: 'Operating as [%1]',

  /**
   * @description [en_US] Requesting...
   */
  Com_Msg_RequestingPage: 'Requesting...',

  /**
   * @description [en_US] Your personal settings have been saved.
   */
  Com_Msg_SavingPersonalSetting: 'Your personal settings have been saved.',

  /**
   * @description [en_US] Enter search criteria and press [Search].
   */
  Com_Msg_SearchGuideMessage: 'Enter search criteria and press [Search].',

  /**
   * @description [en_US] All
   */
  Com_Sel_All: 'All',

  /**
   * @description [en_US] Journalized
   */
  Com_Sel_JournalStatusDone: 'Journalized',

  /**
   * @description [en_US] Pending Journal
   */
  Com_Sel_JournalStatusWaiting: 'Pending Journal',

  /**
   * @description [en_US] Yes
   */
  Com_Sel_Need: 'Yes',

  /**
   * @description [en_US] No
   */
  Com_Sel_NotNeed: 'No',

  /**
   * @description [en_US] Only selected items
   */
  Com_Sel_SelectedOnly: 'Only selected items',

  /**
   * @description [en_US] Approved
   */
  Com_Sel_StatusApproved: 'Approved',

  /**
   * @description [en_US] Approval In
   */
  Com_Sel_StatusApprovelIn: 'Approval In',

  /**
   * @description [en_US] Processed
   */
  Com_Sel_StatusProcessed: 'Processed',

  /**
   * @description [en_US] Rejected
   */
  Com_Sel_StatusReject: 'Rejected',

  /**
   * @description [en_US] Approval In
   */
  Com_Status_ApprovalIn: 'Approval In',

  /**
   * @description [en_US] Approved
   */
  Com_Status_Approved: 'Approved',

  /**
   * @description [en_US] Canceled
   */
  Com_Status_Canceled: 'Canceled',

  /**
   * @description [en_US] Not Requested
   */
  Com_Status_NotRequested: 'Not Requested',

  /**
   * @description [en_US] Pending
   */
  Com_Status_Pending: 'Pending',

  /**
   * @description [en_US] Reapplying
   */
  Com_Status_Reapplying: 'Reapplying',

  /**
   * @description [en_US] Rejected
   */
  Com_Status_Rejected: 'Rejected',

  /**
   * @description [en_US] Removed
   */
  Com_Status_Removed: 'Removed',

  /**
   * @description [en_US] ([%1])
   */
  Com_Str_Parenthesis: '([%1])',

  /**
   * @description [en_US] Discard Edits?
   */
  Common_Confirm_DiscardEdits: 'Discard Edits?',

  /**
   * @description [en_US] File type not supported
   */
  Common_Err_InvalidType: 'File type not supported',

  /**
   * @description [en_US] Over Characters
   */
  Common_Err_Max: 'Over Characters',

  /**
   * @description [en_US] File exceeds maximum file size (5MB)
   */
  Common_Err_MaxFileSize: 'File exceeds maximum file size (5MB)',

  /**
   * @description [en_US]  Required
   */
  Common_Err_Required: ' Required',

  /**
   * @description [en_US]  Field value must be a number
   */
  Common_Err_TypeNumber: ' Field value must be a number',

  /**
   * @description [en_US] Journalize
   */
  Exp_Btn_AccrualJournalize: 'Journalize',

  /**
   * @description [en_US] Add New Item
   */
  Exp_Btn_AddNewItem: 'Add New Item',

  /**
   * @description [en_US] Add Receipt
   */
  Exp_Btn_AddReceipt: 'Add Receipt',

  /**
   * @description [en_US] Clear
   */
  Exp_Btn_Clear: 'Clear',

  /**
   * @description [en_US] Delete Condition
   */
  Exp_Btn_DeleteSearchCondition: 'Delete Condition',

  /**
   * @description [en_US] Output FB Data
   */
  Exp_Btn_FBOutput: 'Output FB Data',

  /**
   * @description [en_US] Expense Type List
   */
  Exp_Btn_FromExpenseTypeList: 'Expense Type List',

  /**
   * @description [en_US] Favorites
   */
  Exp_Btn_FromFavorite: 'Favorites',

  /**
   * @description [en_US] History
   */
  Exp_Btn_FromHistory: 'History',

  /**
   * @description [en_US] List
   */
  Exp_Btn_FromList: 'List',

  /**
   * @description [en_US] History
   */
  Exp_Btn_History: 'History',

  /**
   * @description [en_US] Move
   */
  Exp_Btn_Move: 'Move',

  /**
   * @description [en_US] Move To
   */
  Exp_Btn_MoveTo: 'Move To',

  /**
   * @description [en_US] New Record
   */
  Exp_Btn_NewRecord: 'New Record',

  /**
   * @description [en_US] New Report
   */
  Exp_Btn_NewReport: 'New Report',

  /**
   * @description [en_US] New Request
   */
  Exp_Btn_NewRequest: 'New Request',

  /**
   * @description [en_US] Pay Method Setting
   */
  Exp_Btn_PayMethodSetting: 'Pay Method Setting',

  /**
   * @description [en_US] Payment Date Setting
   */
  Exp_Btn_PaymentDateSetting: 'Payment Date Setting',

  /**
   * @description [en_US] Payment Journal
   */
  Exp_Btn_PaymentJournal: 'Payment Journal',

  /**
   * @description [en_US] Preview
   */
  Exp_Btn_Preview: 'Preview',

  /**
   * @description [en_US] Check Record Items
   */
  Exp_Btn_RecordItemsCheck: 'Check Record Items',

  /**
   * @description [en_US] Create Record Items
   */
  Exp_Btn_RecordItemsCreate: 'Create Record Items',

  /**
   * @description [en_US] Delete Record Items
   */
  Exp_Btn_RecordItemsDelete: 'Delete Record Items',

  /**
   * @description [en_US] Edit Record Items
   */
  Exp_Btn_RecordItemsEdit: 'Edit Record Items',

  /**
   * @description [en_US] Reports
   */
  Exp_Btn_ReportList: 'Reports',

  /**
   * @description [en_US] Request
   */
  Exp_Btn_Request: 'Request',

  /**
   * @description [en_US] Search
   */
  Exp_Btn_RouteSearch: 'Search',

  /**
   * @description [en_US] Save Condition
   */
  Exp_Btn_SaveSearchCondition: 'Save Condition',

  /**
   * @description [en_US] Search
   */
  Exp_Btn_Search: 'Search',

  /**
   * @description [en_US] Accounting Date
   */
  Exp_Btn_SearchConditionAccountingDate: 'Accounting Date',

  /**
   * @description [en_US] Amount
   */
  Exp_Btn_SearchConditionAmount: 'Amount',

  /**
   * @description [en_US] Department
   */
  Exp_Btn_SearchConditionDepartment: 'Department',

  /**
   * @description [en_US] Detail
   */
  Exp_Btn_SearchConditionDetail: 'Detail',

  /**
   * @description [en_US] Employee
   */
  Exp_Btn_SearchConditionEmployee: 'Employee',

  /**
   * @description [en_US] Report No
   */
  Exp_Btn_SearchConditionReportNo: 'Report No',

  /**
   * @description [en_US] Submit Date
   */
  Exp_Btn_SearchConditionRequestDate: 'Submit Date',

  /**
   * @description [en_US] Status
   */
  Exp_Btn_SearchConditionStatus: 'Status',

  /**
   * @description [en_US] Search Setting
   */
  Exp_Btn_SearchSetting: 'Search Setting',

  /**
   * @description [en_US] Create report
   */
  Exp_Btn_SelectRecord: 'Create report',

  /**
   * @description [en_US] Settle
   */
  Exp_Btn_Settle: 'Settle',

  /**
   * @description [en_US] Requests
   */
  Exp_Btn_ShowRequests: 'Requests',

  /**
   * @description [en_US] Accounting Authorized
   */
  Exp_Btn_StatusOptionAccountingAuthorized: 'Accounting Authorized',

  /**
   * @description [en_US] Accounting Rejected
   */
  Exp_Btn_StatusOptionAccountingRejected: 'Accounting Rejected',

  /**
   * @description [en_US] Journal Data Created
   */
  Exp_Btn_StatusOptionJournalCreated: 'Journal Data Created',

  /**
   * @description [en_US] Paid
   */
  Exp_Btn_StatusOptionPaid: 'Paid',

  /**
   * @description [en_US] Approved
   */
  Exp_Btn_StatusOptionStatusApproved: 'Approved',

  /**
   * @description [en_US] Submit
   */
  Exp_Btn_Submit: 'Submit',

  /**
   * @description [en_US] Submit
   */
  Exp_Btn_SubmitReceipt: 'Submit',

  /**
   * @description [en_US] Template
   */
  Exp_Btn_Template: 'Template',

  /**
   * @description [en_US] Route Search
   */
  Exp_Btn_Transit: 'Route Search',

  /**
   * @description [en_US] Undo Payment Journal
   */
  Exp_Btn_UndoPaymentJournal: 'Undo Payment Journal',

  /**
   * @description [en_US] Undo Settle
   */
  Exp_Btn_UndoSettle: 'Undo Settle',

  /**
   * @description [en_US] Upload File
   */
  Exp_Btn_UploadFile: 'Upload File',

  /**
   * @description [en_US] View Vendor Detail
   */
  Exp_Btn_VendorDetail: 'View Vendor Detail',

  /**
   * @description [en_US] {$Exp_Clbl_AccountingPeriod}
   */
  Exp_Clbl_AccountingPeriod: '{$Exp_Clbl_AccountingPeriod}',

  /**
   * @description [en_US] {$Exp_Clbl_Amount}
   */
  Exp_Clbl_Amount: '{$Exp_Clbl_Amount}',

  /**
   * @description [en_US] {$Exp_Clbl_CostCenter}
   */
  Exp_Clbl_CostCenter: '{$Exp_Clbl_CostCenter}',

  /**
   * @description [en_US] {$Exp_Clbl_Currency}
   */
  Exp_Clbl_Currency: '{$Exp_Clbl_Currency}',

  /**
   * @description [en_US] {$Exp_Clbl_Date}
   */
  Exp_Clbl_Date: '{$Exp_Clbl_Date}',

  /**
   * @description [en_US] {$Exp_Clbl_ExchangeRate}
   */
  Exp_Clbl_ExchangeRate: '{$Exp_Clbl_ExchangeRate}',

  /**
   * @description [en_US] {$Exp_Clbl_ExpenseType}
   */
  Exp_Clbl_ExpenseType: '{$Exp_Clbl_ExpenseType}',

  /**
   * @description [en_US] {$Exp_Clbl_Gst}
   */
  Exp_Clbl_Gst: '{$Exp_Clbl_Gst}',

  /**
   * @description [en_US] {$Exp_Clbl_GstAmount}
   */
  Exp_Clbl_GstAmount: '{$Exp_Clbl_GstAmount}',

  /**
   * @description [en_US] {$Exp_Clbl_GstRate}
   */
  Exp_Clbl_GstRate: '{$Exp_Clbl_GstRate}',

  /**
   * @description [en_US] {$Exp_Clbl_IncludeTax}
   */
  Exp_Clbl_IncludeTax: '{$Exp_Clbl_IncludeTax}',

  /**
   * @description [en_US] {$Exp_Clbl_LocalAmount}
   */
  Exp_Clbl_LocalAmount: '{$Exp_Clbl_LocalAmount}',

  /**
   * @description [en_US] {$Exp_Clbl_NewReport}
   */
  Exp_Clbl_NewReport: '{$Exp_Clbl_NewReport}',

  /**
   * @description [en_US] {$Exp_Clbl_Purpose}
   */
  Exp_Clbl_Purpose: '{$Exp_Clbl_Purpose}',

  /**
   * @description [en_US] {$Exp_Clbl_RecordDate}
   */
  Exp_Clbl_RecordDate: '{$Exp_Clbl_RecordDate}',

  /**
   * @description [en_US] {$Exp_Clbl_ReportRemarks}
   */
  Exp_Clbl_ReportRemarks: '{$Exp_Clbl_ReportRemarks}',

  /**
   * @description [en_US] {$Exp_Clbl_ReportTitle}
   */
  Exp_Clbl_ReportTitle: '{$Exp_Clbl_ReportTitle}',

  /**
   * @description [en_US] {$Exp_Clbl_ReportType}
   */
  Exp_Clbl_ReportType: '{$Exp_Clbl_ReportType}',

  /**
   * @description [en_US] {$Exp_Clbl_RequestTitle}
   */
  Exp_Clbl_RequestTitle: '{$Exp_Clbl_RequestTitle}',

  /**
   * @description [en_US] {$Exp_Clbl_ScheduledDate}
   */
  Exp_Clbl_ScheduledDate: '{$Exp_Clbl_ScheduledDate}',

  /**
   * @description [en_US] {$Exp_Clbl_Summary}
   */
  Exp_Clbl_Summary: '{$Exp_Clbl_Summary}',

  /**
   * @description [en_US] {$Exp_Clbl_Type}
   */
  Exp_Clbl_Type: '{$Exp_Clbl_Type}',

  /**
   * @description [en_US] {$Exp_Clbl_WithoutTax}
   */
  Exp_Clbl_WithoutTax: '{$Exp_Clbl_WithoutTax}',

  /**
   * @description [en_US] Left value needs to be less or equal than right
   */
  Exp_Err_AmountLeftLessThanRight: 'Left value needs to be less or equal than right',

  /**
   * @description [en_US] Enter less than [%1]
   */
  Exp_Err_AmountMaxNumber: 'Enter less than [%1]',

  /**
   * @description [en_US] Please select an amount.
   */
  Exp_Err_AmountSelection: 'Please select an amount.',

  /**
   * @description [en_US] Arrival is not decided
   */
  Exp_Err_ArrivalNotDecided: 'Arrival is not decided',

  /**
   * @description [en_US] The date of the record is outside the range.
   */
  Exp_Err_DateNotInRange: 'The date of the record is outside the range.',

  /**
   * @description [en_US] This expense type cannot be used with the selected report type.
   */
  Exp_Err_InvalidRecordExpenseTypeForReportType: 'This expense type cannot be used with the selected report type.',

  /**
   * @description [en_US] Following expense type cannot be used with the selected report type:
   */
  Exp_Err_InvalidRecordExpenseTypeForReportTypeMobile: 'Following expense type cannot be used with the selected report type:',

  /**
   * @description [en_US] It\'s necessary to enter a receipt or summary.
   */
  Exp_Err_NoReceiptAndReceipt: 'It\'s necessary to enter a receipt or summary.',

  /**
   * @description [en_US] Route not selected
   */
  Exp_Err_NoSelectedRoute: 'Route not selected',

  /**
   * @description [en_US] There is no tax type available.
   */
  Exp_Err_NoTaxAvailable: 'There is no tax type available.',

  /**
   * @description [en_US] Departure is not decided
   */
  Exp_Err_OriginNotDecided: 'Departure is not decided',

  /**
   * @description [en_US] Payment Due Date must be later than Record Date.
   */
  Exp_Err_PaymentDateAfterRecordDate: 'Payment Due Date must be later than Record Date.',

  /**
   * @description [en_US] Record items is mandatory. Please create record items.
   */
  Exp_Err_RecordItemsMandatory: 'Record items is mandatory. Please create record items.',

  /**
   * @description [en_US] Record(s) are necessary when submitting.
   */
  Exp_Err_SubmitReportNoRecords: 'Record(s) are necessary when submitting.',

  /**
   * @description [en_US] This amount cannot be entered directly.
   */
  Exp_Hint_FixedAllowanceMulti: 'This amount cannot be entered directly.',

  /**
   * @description [en_US] This Amount is fixed with the expense type.
   */
  Exp_Hint_FixedAllownceSingle: 'This Amount is fixed with the expense type.',

  /**
   * @description [en_US] Currency is decided by the expense type.
   */
  Exp_Hint_FixedCurrency: 'Currency is decided by the expense type.',

  /**
   * @description [en_US] Currency is decided by the parent expense type.
   */
  Exp_Hint_FixedCurrencyChild: 'Currency is decided by the parent expense type.',

  /**
   * @description [en_US] Account
   */
  Exp_Lbl_Account: 'Account',

  /**
   * @description [en_US] Date
   */
  Exp_Lbl_AccountingDate: 'Date',

  /**
   * @description [en_US] Accounting Period
   */
  Exp_Lbl_AccountingPeriod: 'Accounting Period',

  /**
   * @description [en_US] Active Report
   */
  Exp_Lbl_ActiveReport: 'Active Report',

  /**
   * @description [en_US] Actor Name
   */
  Exp_Lbl_ActorName: 'Actor Name',

  /**
   * @description [en_US] Add income tax
   */
  Exp_Lbl_AddIncomeTax: 'Add income tax',

  /**
   * @description [en_US] Add Receipt
   */
  Exp_Lbl_AddReceipt: 'Add Receipt',

  /**
   * @description [en_US] + Via point
   */
  Exp_Lbl_AddViaButton: '+ Via point',

  /**
   * @description [en_US] Address
   */
  Exp_Lbl_Address: 'Address',

  /**
   * @description [en_US] Advance Request Date
   */
  Exp_Lbl_AdvanceRequestDate: 'Advance Request Date',

  /**
   * @description [en_US] Out of pocket
   */
  Exp_Lbl_AdvancesPaid: 'Out of pocket',

  /**
   * @description [en_US] Air Ticket
   */
  Exp_Lbl_AirTravelPolicy: 'Air Ticket',

  /**
   * @description [en_US] Amount
   */
  Exp_Lbl_Amount: 'Amount',

  /**
   * @description [en_US] Amount Selection
   */
  Exp_Lbl_AmountSelection: 'Amount Selection',

  /**
   * @description [en_US] Applicable Expense Type
   */
  Exp_Lbl_ApplicableExpenseType: 'Applicable Expense Type',

  /**
   * @description [en_US] Approval Date
   */
  Exp_Lbl_ApprovalDate: 'Approval Date',

  /**
   * @description [en_US] Approved (option)
   */
  Exp_Lbl_ApprovedOption: 'Approved (option)',

  /**
   * @description [en_US] Approved Report
   */
  Exp_Lbl_ApprovedReport: 'Approved Report',

  /**
   * @description [en_US] Arrive On
   */
  Exp_Lbl_ArrivedDateTime: 'Arrive On',

  /**
   * @description [en_US] Attach
   */
  Exp_Lbl_Attach: 'Attach',

  /**
   * @description [en_US] Attached File
   */
  Exp_Lbl_AttachedFile: 'Attached File',

  /**
   * @description [en_US] SelectRecord
   */
  Exp_Lbl_BackToRecordSelection: 'SelectRecord',

  /**
   * @description [en_US] Bank Account Number
   */
  Exp_Lbl_BankAccountNumber: 'Bank Account Number',

  /**
   * @description [en_US] Bank Account Type
   */
  Exp_Lbl_BankAccountType: 'Bank Account Type',

  /**
   * @description [en_US] Bank Code
   */
  Exp_Lbl_BankCode: 'Bank Code',

  /**
   * @description [en_US] Currency
   */
  Exp_Lbl_BankCurrency: 'Currency',

  /**
   * @description [en_US] Bank Name
   */
  Exp_Lbl_BankName: 'Bank Name',

  /**
   * @description [en_US] Base Currency
   */
  Exp_Lbl_BaseCurrency: 'Base Currency',

  /**
   * @description [en_US] Booking Basis
   */
  Exp_Lbl_BaseOn: 'Booking Basis',

  /**
   * @description [en_US] Bill
   */
  Exp_Lbl_Bill: 'Bill',

  /**
   * @description [en_US] Billing Amount
   */
  Exp_Lbl_BillAmount: 'Billing Amount',

  /**
   * @description [en_US] Booking
   */
  Exp_Lbl_Booking: 'Booking',

  /**
   * @description [en_US] Branch Address
   */
  Exp_Lbl_BranchAddress: 'Branch Address',

  /**
   * @description [en_US] Branch Code
   */
  Exp_Lbl_BranchCode: 'Branch Code',

  /**
   * @description [en_US] Branch Name
   */
  Exp_Lbl_BranchName: 'Branch Name',

  /**
   * @description [en_US] Breakfast
   */
  Exp_Lbl_Breakfast: 'Breakfast',

  /**
   * @description [en_US] Camera Image
   */
  Exp_Lbl_Camera: 'Camera Image',

  /**
   * @description [en_US] Model
   */
  Exp_Lbl_CarType: 'Model',

  /**
   * @description [en_US] Check In
   */
  Exp_Lbl_CheckInDate: 'Check In',

  /**
   * @description [en_US] Check Out
   */
  Exp_Lbl_CheckOutDate: 'Check Out',

  /**
   * @description [en_US] Select a file
   */
  Exp_Lbl_ChooseFile: 'Select a file',

  /**
   * @description [en_US] City
   */
  Exp_Lbl_City: 'City',

  /**
   * @description [en_US] Click to zoom
   */
  Exp_Lbl_ClickToZoom: 'Click to zoom',

  /**
   * @description [en_US] Clone
   */
  Exp_Lbl_Clone: 'Clone',

  /**
   * @description [en_US] Code
   */
  Exp_Lbl_Code: 'Code',

  /**
   * @description [en_US] Comment
   */
  Exp_Lbl_Comment: 'Comment',

  /**
   * @description [en_US] Commuter Pass Route Search
   */
  Exp_Lbl_CommuterPassSearch: 'Commuter Pass Route Search',

  /**
   * @description [en_US] Consult Doctor
   */
  Exp_Lbl_ConsultDoctor: 'Consult Doctor',

  /**
   * @description [en_US] Corporate Card
   */
  Exp_Lbl_CorporateCard: 'Corporate Card',

  /**
   * @description [en_US] Correspondent Bank Address
   */
  Exp_Lbl_CorrespondentBankAddress: 'Correspondent Bank Address',

  /**
   * @description [en_US] Correspondent Bank Bank Name
   */
  Exp_Lbl_CorrespondentBankBankName: 'Correspondent Bank Bank Name',

  /**
   * @description [en_US] Correspondent Bank Branch Name
   */
  Exp_Lbl_CorrespondentBankBranchName: 'Correspondent Bank Branch Name',

  /**
   * @description [en_US] Correspondent Bank SWIFT Code
   */
  Exp_Lbl_CorrespondentBankSwiftCode: 'Correspondent Bank SWIFT Code',

  /**
   * @description [en_US] Cost Center
   */
  Exp_Lbl_CostCenter: 'Cost Center',

  /**
   * @description [en_US] Amount per head
   */
  Exp_Lbl_CostPerHead: 'Amount per head',

  /**
   * @description [en_US] Cost per person
   */
  Exp_Lbl_CostPerPerson: 'Cost per person',

  /**
   * @description [en_US] Country Code
   */
  Exp_Lbl_CountryCode: 'Country Code',

  /**
   * @description [en_US] Create Record From Receipt
   */
  Exp_Lbl_CreateRecordFromReceipt: 'Create Record From Receipt',

  /**
   * @description [en_US] Create Report
   */
  Exp_Lbl_CreateReport: 'Create Report',

  /**
   * @description [en_US] Currency
   */
  Exp_Lbl_Currency: 'Currency',

  /**
   * @description [en_US] Date
   */
  Exp_Lbl_Date: 'Date',

  /**
   * @description [en_US] Date of payment
   */
  Exp_Lbl_DateOfPayment: 'Date of payment',

  /**
   * @description [en_US] Date of use
   */
  Exp_Lbl_DateOfUse: 'Date of use',

  /**
   * @description [en_US] Date Submitted
   */
  Exp_Lbl_DateSubmitted: 'Date Submitted',

  /**
   * @description [en_US] Delete
   */
  Exp_Lbl_DeleteFile: 'Delete',

  /**
   * @description [en_US] Delete Receipt
   */
  Exp_Lbl_DeleteReceipt: 'Delete Receipt',

  /**
   * @description [en_US] Delete Search Condition
   */
  Exp_Lbl_DeleteSearchCondition: 'Delete Search Condition',

  /**
   * @description [en_US] From
   */
  Exp_Lbl_DepartFrom: 'From',

  /**
   * @description [en_US] Department
   */
  Exp_Lbl_Department: 'Department',

  /**
   * @description [en_US] Department Code
   */
  Exp_Lbl_DepartmentCode: 'Department Code',

  /**
   * @description [en_US] Department Name
   */
  Exp_Lbl_DepartmentName: 'Department Name',

  /**
   * @description [en_US] Departure
   */
  Exp_Lbl_Departure: 'Departure',

  /**
   * @description [en_US] To
   */
  Exp_Lbl_Destination: 'To',

  /**
   * @description [en_US] Detail
   */
  Exp_Lbl_Detail: 'Detail',

  /**
   * @description [en_US] Difference
   */
  Exp_Lbl_DiffAmount: 'Difference',

  /**
   * @description [en_US] Dinner
   */
  Exp_Lbl_Dinner: 'Dinner',

  /**
   * @description [en_US] Drag and drop a file or select
   */
  Exp_Lbl_DragAndDrop: 'Drag and drop a file or select',

  /**
   * @description [en_US] Drag and drop a file or select
   */
  Exp_Lbl_DragAndDropOCR: 'Drag and drop a file or select',

  /**
   * @description [en_US] Alcohol included
   */
  Exp_Lbl_Drinking: 'Alcohol included',

  /**
   * @description [en_US] Edit History
   */
  Exp_Lbl_EditHistory: 'Edit History',

  /**
   * @description [en_US] Employee Code
   */
  Exp_Lbl_EmployeeCode: 'Employee Code',

  /**
   * @description [en_US] Employee Code
   */
  Exp_Lbl_EmployeeCodeFull: 'Employee Code',

  /**
   * @description [en_US] Employee Name
   */
  Exp_Lbl_EmployeeName: 'Employee Name',

  /**
   * @description [en_US] Estimation No
   */
  Exp_Lbl_EstimationNo: 'Estimation No',

  /**
   * @description [en_US] Proof
   */
  Exp_Lbl_Evidence: 'Proof',

  /**
   * @description [en_US] Exchange Rate
   */
  Exp_Lbl_ExchangeRate: 'Exchange Rate',

  /**
   * @description [en_US] Exchange Rate Code
   */
  Exp_Lbl_ExchangeRateCode: 'Exchange Rate Code',

  /**
   * @description [en_US] Exists same name
   */
  Exp_Lbl_ExistsSameName: 'Exists same name',

  /**
   * @description [en_US] Expand area
   */
  Exp_Lbl_ExpandArea: 'Expand area',

  /**
   * @description [en_US] Journal By
   */
  Exp_Lbl_ExpenseJournalEmp: 'Journal By',

  /**
   * @description [en_US] Journal Time
   */
  Exp_Lbl_ExpenseJournalTime: 'Journal Time',

  /**
   * @description [en_US] Expense Report
   */
  Exp_Lbl_ExpenseReport: 'Expense Report',

  /**
   * @description [en_US] Expense Report Search
   */
  Exp_Lbl_ExpenseReportSearch: 'Expense Report Search',

  /**
   * @description [en_US] Expense Settlement
   */
  Exp_Lbl_ExpenseSettlement: 'Expense Settlement',

  /**
   * @description [en_US] Expense Type
   */
  Exp_Lbl_ExpenseType: 'Expense Type',

  /**
   * @description [en_US] Expense Type Selection
   */
  Exp_Lbl_ExpenseTypeSelect: 'Expense Type Selection',

  /**
   * @description [en_US] Extended Item
   */
  Exp_Lbl_ExtendedItem: 'Extended Item',

  /**
   * @description [en_US] External
   */
  Exp_Lbl_External: 'External',

  /**
   * @description [en_US] External Participants
   */
  Exp_Lbl_ExternalParticipants: 'External Participants',

  /**
   * @description [en_US] FB Output By
   */
  Exp_Lbl_FbOutputEmp: 'FB Output By',

  /**
   * @description [en_US] FB Output Time
   */
  Exp_Lbl_FbOutputTime: 'FB Output Time',

  /**
   * @description [en_US] Field
   */
  Exp_Lbl_FieldName: 'Field',

  /**
   * @description [en_US] Finalized
   */
  Exp_Lbl_Finalized: 'Finalized',

  /**
   * @description [en_US] From company
   */
  Exp_Lbl_FromCompany: 'From company',

  /**
   * @description [en_US] From home
   */
  Exp_Lbl_FromHome: 'From home',

  /**
   * @description [en_US] New Request
   */
  Exp_Lbl_GeneralExpense: 'New Request',

  /**
   * @description [en_US] Tax
   */
  Exp_Lbl_Gst: 'Tax',

  /**
   * @description [en_US] Tax Amount
   */
  Exp_Lbl_GstAmount: 'Tax Amount',

  /**
   * @description [en_US] Tax Rate
   */
  Exp_Lbl_GstRate: 'Tax Rate',

  /**
   * @description [en_US] Health check-up
   */
  Exp_Lbl_HealthCheck: 'Health check-up',

  /**
   * @description [en_US] Hint
   */
  Exp_Lbl_Hint: 'Hint',

  /**
   * @description [en_US] Hotel
   */
  Exp_Lbl_Hotel: 'Hotel',

  /**
   * @description [en_US] Import from
   */
  Exp_Lbl_Import: 'Import from',

  /**
   * @description [en_US] Import from Card
   */
  Exp_Lbl_ImportFromCard: 'Import from Card',

  /**
   * @description [en_US] Inactive
   */
  Exp_Lbl_Inactive: 'Inactive',

  /**
   * @description [en_US] Amount (incl. Tax)
   */
  Exp_Lbl_IncludeTax: 'Amount (incl. Tax)',

  /**
   * @description [en_US] Initial employee setting is not completed.
   */
  Exp_Lbl_InitEmpSettingNotCompleted: 'Initial employee setting is not completed.',

  /**
   * @description [en_US] Empty
   */
  Exp_Lbl_InputEmpty: 'Empty',

  /**
   * @description [en_US] Insurance Request
   */
  Exp_Lbl_InsuranceRequest: 'Insurance Request',

  /**
   * @description [en_US] Internal
   */
  Exp_Lbl_Internal: 'Internal',

  /**
   * @description [en_US] Internal Participant
   */
  Exp_Lbl_InternalParticipant: 'Internal Participant',

  /**
   * @description [en_US] Record is saved!
   */
  Exp_Lbl_ItemIsSaved: 'Record is saved!',

  /**
   * @description [en_US] Job
   */
  Exp_Lbl_Job: 'Job',

  /**
   * @description [en_US] Loading Preview...
   */
  Exp_Lbl_LoadingPreview: 'Loading Preview...',

  /**
   * @description [en_US] Local Amount
   */
  Exp_Lbl_LocalAmount: 'Local Amount',

  /**
   * @description [en_US] Local Contact
   */
  Exp_Lbl_LocalContact: 'Local Contact',

  /**
   * @description [en_US] Lodging Policy
   */
  Exp_Lbl_LodgingPolicy: 'Lodging Policy',

  /**
   * @description [en_US] L
   */
  Exp_Lbl_Lunch: 'L',

  /**
   * @description [en_US] Manually Create
   */
  Exp_Lbl_Manual: 'Manually Create',

  /**
   * @description [en_US] Max File size 5MB
   */
  Exp_Lbl_MaxFileSize: 'Max File size 5MB',

  /**
   * @description [en_US] Meal Allowance
   */
  Exp_Lbl_MealAllowance: 'Meal Allowance',

  /**
   * @description [en_US] There are error(s) in the form. Please check the content.
   */
  Exp_Lbl_MessageAreaTitle: 'There are error(s) in the form. Please check the content.',

  /**
   * @description [en_US] Mobile app currently does not support the following items:\n - [%1]\nReports having the above items are not present in the list shown.
   */
  Exp_Lbl_MobileNotSupportedInfo: 'Mobile app currently does not support the following items:\n - [%1]\nReports having the above items are not present in the list shown.',

  /**
   * @description [en_US] Modified By
   */
  Exp_Lbl_ModifiedBy: 'Modified By',

  /**
   * @description [en_US] Name
   */
  Exp_Lbl_Name: 'Name',

  /**
   * @description [en_US] New
   */
  Exp_Lbl_New: 'New',

  /**
   * @description [en_US] New Value
   */
  Exp_Lbl_NewFieldValue: 'New Value',

  /**
   * @description [en_US] New Record
   */
  Exp_Lbl_NewRecord: 'New Record',

  /**
   * @description [en_US] New Report
   */
  Exp_Lbl_NewReport: 'New Report',

  /**
   * @description [en_US] New Report
   */
  Exp_Lbl_NewReportCreateExp: 'New Report',

  /**
   * @description [en_US] New Request
   */
  Exp_Lbl_NewReportCreateReq: 'New Request',

  /**
   * @description [en_US] New Request
   */
  Exp_Lbl_NewRequest: 'New Request',

  /**
   * @description [en_US] night(s)
   */
  Exp_Lbl_Night: 'night(s)',

  /**
   * @description [en_US] Number of nights
   */
  Exp_Lbl_Nights: 'Number of nights',

  /**
   * @description [en_US] No
   */
  Exp_Lbl_No: 'No',

  /**
   * @description [en_US] Displaying [%1] of [%2]
   */
  Exp_Lbl_NumberDisplay: 'Displaying [%1] of [%2]',

  /**
   * @description [en_US] The number of participants
   */
  Exp_Lbl_NumberOfParticipants: 'The number of participants',

  /**
   * @description [en_US] Number of people
   */
  Exp_Lbl_NumberOfPeople: 'Number of people',

  /**
   * @description [en_US] Original Value
   */
  Exp_Lbl_OriginalFieldValue: 'Original Value',

  /**
   * @description [en_US] other
   */
  Exp_Lbl_Other: 'other',

  /**
   * @description [en_US] Other Allowance
   */
  Exp_Lbl_OtherAllowance: 'Other Allowance',

  /**
   * @description [en_US] Other Expenses
   */
  Exp_Lbl_OtherExpenses: 'Other Expenses',

  /**
   * @description [en_US] Participant 
   */
  Exp_Lbl_Participant: 'Participant ',

  /**
   * @description [en_US] Passport Request
   */
  Exp_Lbl_PassportRequest: 'Passport Request',

  /**
   * @description [en_US] Due Employee
   */
  Exp_Lbl_PayAmount: 'Due Employee',

  /**
   * @description [en_US] Payee Name
   */
  Exp_Lbl_PayeeName: 'Payee Name',

  /**
   * @description [en_US] Payment Amount
   */
  Exp_Lbl_PaymentAmount: 'Payment Amount',

  /**
   * @description [en_US] Payment Arrangement
   */
  Exp_Lbl_PaymentArrangement: 'Payment Arrangement',

  /**
   * @description [en_US] Payment Confirmation
   */
  Exp_Lbl_PaymentConfirmation: 'Payment Confirmation',

  /**
   * @description [en_US] Payment Due Date
   */
  Exp_Lbl_PaymentDate: 'Payment Due Date',

  /**
   * @description [en_US] Payment Method
   */
  Exp_Lbl_PaymentMethod: 'Payment Method',

  /**
   * @description [en_US] Payment Term 
   */
  Exp_Lbl_PaymentPeriod: 'Payment Term ',

  /**
   * @description [en_US] Payment Request
   */
  Exp_Lbl_PaymentRequest: 'Payment Request',

  /**
   * @description [en_US] Payment Term
   */
  Exp_Lbl_PaymentTerm: 'Payment Term',

  /**
   * @description [en_US] Payment Term Code
   */
  Exp_Lbl_PaymentTermCode: 'Payment Term Code',

  /**
   * @description [en_US] Payment Type
   */
  Exp_Lbl_PaymentType: 'Payment Type',

  /**
   * @description [en_US] Per Diem
   */
  Exp_Lbl_PerDiem: 'Per Diem',

  /**
   * @description [en_US] person
   */
  Exp_Lbl_Person: 'person',

  /**
   * @description [en_US] Personal amount to be transferred
   */
  Exp_Lbl_PersonalAmountTransferred: 'Personal amount to be transferred',

  /**
   * @description [en_US] Image
   */
  Exp_Lbl_Photo: 'Image',

  /**
   * @description [en_US] Image Added Date
   */
  Exp_Lbl_PhotpAddedDate: 'Image Added Date',

  /**
   * @description [en_US] Select ...
   */
  Exp_Lbl_PleaseSelect: 'Select ...',

  /**
   * @description [en_US] Position
   */
  Exp_Lbl_Position: 'Position',

  /**
   * @description [en_US] Request Title
   */
  Exp_Lbl_PreapprovalSubject: 'Request Title',

  /**
   * @description [en_US] Prepare
   */
  Exp_Lbl_Prepare: 'Prepare',

  /**
   * @description [en_US] Prepared By
   */
  Exp_Lbl_PreparedEmp: 'Prepared By',

  /**
   * @description [en_US] Prepared Time
   */
  Exp_Lbl_PreparedTime: 'Prepared Time',

  /**
   * @description [en_US] Private Use
   */
  Exp_Lbl_PrivateUse: 'Private Use',

  /**
   * @description [en_US] Date Processed
   */
  Exp_Lbl_ProcessedDate: 'Date Processed',

  /**
   * @description [en_US] Processed By
   */
  Exp_Lbl_ProcessedEmp: 'Processed By',

  /**
   * @description [en_US] Processed Time
   */
  Exp_Lbl_ProcessedTime: 'Processed Time',

  /**
   * @description [en_US] Provision
   */
  Exp_Lbl_Provision: 'Provision',

  /**
   * @description [en_US] Provisional Payment Amount
   */
  Exp_Lbl_ProvisionalPaymentAmount: 'Provisional Payment Amount',

  /**
   * @description [en_US] Purpose
   */
  Exp_Lbl_Purpose: 'Purpose',

  /**
   * @description [en_US] Purpose Code
   */
  Exp_Lbl_PurposeCode: 'Purpose Code',

  /**
   * @description [en_US] Quotation
   */
  Exp_Lbl_Quotation: 'Quotation',

  /**
   * @description [en_US] FX Rate(%)
   */
  Exp_Lbl_Rate: 'FX Rate(%)',

  /**
   * @description [en_US] Reason Of Excess Limits
   */
  Exp_Lbl_ReasonOfExcess: 'Reason Of Excess Limits',

  /**
   * @description [en_US] Recall
   */
  Exp_Lbl_Recall: 'Recall',

  /**
   * @description [en_US] Receipt
   */
  Exp_Lbl_Receipt: 'Receipt',

  /**
   * @description [en_US] Receipt Library
   */
  Exp_Lbl_ReceiptLibrary: 'Receipt Library',

  /**
   * @description [en_US] Receipt Needed
   */
  Exp_Lbl_ReceiptNeeded: 'Receipt Needed',

  /**
   * @description [en_US] Receipt Selection
   */
  Exp_Lbl_ReceiptSelection: 'Receipt Selection',

  /**
   * @description [en_US] Receipt Setting
   */
  Exp_Lbl_ReceiptSetting: 'Receipt Setting',

  /**
   * @description [en_US] Receipt Type
   */
  Exp_Lbl_ReceiptType: 'Receipt Type',

  /**
   * @description [en_US] Upload Failed
   */
  Exp_Lbl_ReceiptUploadFailed: 'Upload Failed',

  /**
   * @description [en_US] Upload Success
   */
  Exp_Lbl_ReceiptUploadSuccess: 'Upload Success',

  /**
   * @description [en_US] Recently Used
   */
  Exp_Lbl_RecentlyUsed: 'Recently Used',

  /**
   * @description [en_US] Recently Used Items
   */
  Exp_Lbl_RecentlyUsedItems: 'Recently Used Items',

  /**
   * @description [en_US] Record Class
   */
  Exp_Lbl_RecordClass: 'Record Class',

  /**
   * @description [en_US] Clone Record
   */
  Exp_Lbl_RecordClone: 'Clone Record',

  /**
   * @description [en_US] Select dates to clone the records on.
   */
  Exp_Lbl_RecordCloneDateSelect: 'Select dates to clone the records on.',

  /**
   * @description [en_US] Days
   */
  Exp_Lbl_RecordCloneDay: 'Days',

  /**
   * @description [en_US] You can select a range by selecting while pressing the shift key.
   */
  Exp_Lbl_RecordCloneSelectRange: 'You can select a range by selecting while pressing the shift key.',

  /**
   * @description [en_US] record(s)
   */
  Exp_Lbl_RecordCount: 'record(s)',

  /**
   * @description [en_US] Record Date
   */
  Exp_Lbl_RecordDate: 'Record Date',

  /**
   * @description [en_US] Record Details
   */
  Exp_Lbl_RecordDetails: 'Record Details',

  /**
   * @description [en_US] Record Items
   */
  Exp_Lbl_RecordItems: 'Record Items',

  /**
   * @description [en_US] Record Items
   */
  Exp_Lbl_RecordItemsDetail: 'Record Items',

  /**
   * @description [en_US] Records
   */
  Exp_Lbl_RecordList: 'Records',

  /**
   * @description [en_US] Record Type
   */
  Exp_Lbl_RecordType: 'Record Type',

  /**
   * @description [en_US] Record
   */
  Exp_Lbl_Records: 'Record',

  /**
   * @description [en_US] Records
   */
  Exp_Lbl_RecordsList: 'Records',

  /**
   * @description [en_US] Select Records
   */
  Exp_Lbl_RecordsListSelect: 'Select Records',

  /**
   * @description [en_US] Reduce area
   */
  Exp_Lbl_ReduceArea: 'Reduce area',

  /**
   * @description [en_US] Reject
   */
  Exp_Lbl_Reject: 'Reject',

  /**
   * @description [en_US] Report
   */
  Exp_Lbl_Report: 'Report',

  /**
   * @description [en_US] Report Amount
   */
  Exp_Lbl_ReportAmount: 'Report Amount',

  /**
   * @description [en_US] Report Date
   */
  Exp_Lbl_ReportDate: 'Report Date',

  /**
   * @description [en_US] Report Detail
   */
  Exp_Lbl_ReportDetail: 'Report Detail',

  /**
   * @description [en_US] Report Edit
   */
  Exp_Lbl_ReportEdit: 'Report Edit',

  /**
   * @description [en_US] Reports
   */
  Exp_Lbl_ReportList: 'Reports',

  /**
   * @description [en_US] Report Number
   */
  Exp_Lbl_ReportNo: 'Report Number',

  /**
   * @description [en_US] Report Title
   */
  Exp_Lbl_ReportTitle: 'Report Title',

  /**
   * @description [en_US] Report Type
   */
  Exp_Lbl_ReportType: 'Report Type',

  /**
   * @description [en_US] Report Type Selection
   */
  Exp_Lbl_ReportTypeSelection: 'Report Type Selection',

  /**
   * @description [en_US] Expense Reports
   */
  Exp_Lbl_Reports: 'Expense Reports',

  /**
   * @description [en_US] Request Reports
   */
  Exp_Lbl_ReportsRequest: 'Request Reports',

  /**
   * @description [en_US] Request Approval
   */
  Exp_Lbl_Request: 'Request Approval',

  /**
   * @description [en_US] Request Contents
   */
  Exp_Lbl_RequestContents: 'Request Contents',

  /**
   * @description [en_US] Request Number
   */
  Exp_Lbl_RequestNo: 'Request Number',

  /**
   * @description [en_US] Request
   */
  Exp_Lbl_RequestTarget: 'Request',

  /**
   * @description [en_US] Return
   */
  Exp_Lbl_Return: 'Return',

  /**
   * @description [en_US] Return At
   */
  Exp_Lbl_ReturnAt: 'Return At',

  /**
   * @description [en_US] Return Date
   */
  Exp_Lbl_ReturnDate: 'Return Date',

  /**
   * @description [en_US] Rental End time
   */
  Exp_Lbl_ReturnDateTime: 'Rental End time',

  /**
   * @description [en_US] Return Location
   */
  Exp_Lbl_ReturnPlace: 'Return Location',

  /**
   * @description [en_US] Return Time
   */
  Exp_Lbl_ReturnTime: 'Return Time',

  /**
   * @description [en_US] Round-Trip
   */
  Exp_Lbl_RoundTrip: 'Round-Trip',

  /**
   * @description [en_US] Total distance [%1] km.
   */
  Exp_Lbl_RouteDistance: 'Total distance [%1] km.',

  /**
   * @description [en_US] EX Res Green Seat
   */
  Exp_Lbl_RouteExGreen: 'EX Res Green Seat',

  /**
   * @description [en_US] EX Res Non Res Seat
   */
  Exp_Lbl_RouteExJiyuuseki: 'EX Res Non Res Seat',

  /**
   * @description [en_US] EX Res Res Seat
   */
  Exp_Lbl_RouteExShiteiseki: 'EX Res Res Seat',

  /**
   * @description [en_US] EX Reservation
   */
  Exp_Lbl_RouteExUsed: 'EX Reservation',

  /**
   * @description [en_US] Cheap
   */
  Exp_Lbl_RouteIconCheap: 'Cheap',

  /**
   * @description [en_US] EX
   */
  Exp_Lbl_RouteIconEX: 'EX',

  /**
   * @description [en_US] Easy
   */
  Exp_Lbl_RouteIconEasy: 'Easy',

  /**
   * @description [en_US] Fast
   */
  Exp_Lbl_RouteIconFast: 'Fast',

  /**
   * @description [en_US] Use IC Card
   */
  Exp_Lbl_RouteIconIcCard: 'Use IC Card',

  /**
   * @description [en_US] Round Trip
   */
  Exp_Lbl_RouteIconRoundTrip: 'Round Trip',

  /**
   * @description [en_US] Search again by specifying condition(s).
   */
  Exp_Lbl_RouteInfoResetButton: 'Search again by specifying condition(s).',

  /**
   * @description [en_US] select route
   */
  Exp_Lbl_RouteNeeded: 'select route',

  /**
   * @description [en_US] Selected route will be shown here.
   */
  Exp_Lbl_RouteNoSelect: 'Selected route will be shown here.',

  /**
   * @description [en_US] Fare Type
   */
  Exp_Lbl_RouteOptionFareType: 'Fare Type',

  /**
   * @description [en_US] IC Card
   */
  Exp_Lbl_RouteOptionFareType_IC: 'IC Card',

  /**
   * @description [en_US] Ticket
   */
  Exp_Lbl_RouteOptionFareType_Ticket: 'Ticket',

  /**
   * @description [en_US] Highway Bus
   */
  Exp_Lbl_RouteOptionHighWayBus: 'Highway Bus',

  /**
   * @description [en_US] Not Use
   */
  Exp_Lbl_RouteOptionHighWayBusNotUse: 'Not Use',

  /**
   * @description [en_US] Use
   */
  Exp_Lbl_RouteOptionHighWayBusUse: 'Use',

  /**
   * @description [en_US] One way
   */
  Exp_Lbl_RouteOptionOneWay: 'One way',

  /**
   * @description [en_US] Round trip
   */
  Exp_Lbl_RouteOptionRoundTrip: 'Round trip',

  /**
   * @description [en_US] Display Order
   */
  Exp_Lbl_RouteOptionRouteSort: 'Display Order',

  /**
   * @description [en_US] Cheap
   */
  Exp_Lbl_RouteOptionRouteSort_Cheap: 'Cheap',

  /**
   * @description [en_US] Easy
   */
  Exp_Lbl_RouteOptionRouteSort_NumberOfTransfers: 'Easy',

  /**
   * @description [en_US] Fast
   */
  Exp_Lbl_RouteOptionRouteSort_TimeRequired: 'Fast',

  /**
   * @description [en_US] Seat Preference
   */
  Exp_Lbl_RouteOptionSeatPreference: 'Seat Preference',

  /**
   * @description [en_US] Non-Reserved Seat
   */
  Exp_Lbl_RouteOptionSeatPreference_FreeSeat: 'Non-Reserved Seat',

  /**
   * @description [en_US] Green Car
   */
  Exp_Lbl_RouteOptionSeatPreference_GreenSeat: 'Green Car',

  /**
   * @description [en_US] Reserved Seat
   */
  Exp_Lbl_RouteOptionSeatPreference_ReservedSeat: 'Reserved Seat',

  /**
   * @description [en_US] Charged Limited Express
   */
  Exp_Lbl_RouteOptionUseChargedExpress: 'Charged Limited Express',

  /**
   * @description [en_US] Do Not Use
   */
  Exp_Lbl_RouteOptionUseChargedExpress_DoNotUse: 'Do Not Use',

  /**
   * @description [en_US] Use When Over 13 km
   */
  Exp_Lbl_RouteOptionUseChargedExpress_Over13km: 'Use When Over 13 km',

  /**
   * @description [en_US] Use
   */
  Exp_Lbl_RouteOptionUseChargedExpress_Use: 'Use',

  /**
   * @description [en_US] EX Res
   */
  Exp_Lbl_RouteOptionUseExReservation: 'EX Res',

  /**
   * @description [en_US] Not Use
   */
  Exp_Lbl_RouteOptionUseExReservation_NotUse: 'Not Use',

  /**
   * @description [en_US] Use
   */
  Exp_Lbl_RouteOptionUseExReservation_Use: 'Use',

  /**
   * @description [en_US] (Station name)
   */
  Exp_Lbl_RoutePlaceholder: '(Station name)',

  /**
   * @description [en_US] Route Search
   */
  Exp_Lbl_RouteSearch: 'Route Search',

  /**
   * @description [en_US] Route Options
   */
  Exp_Lbl_RouteSearchCondition: 'Route Options',

  /**
   * @description [en_US] Return car to the same location
   */
  Exp_Lbl_SamePlace: 'Return car to the same location',

  /**
   * @description [en_US] Scan the Receipt Information
   */
  Exp_Lbl_ScanReceipt: 'Scan the Receipt Information',

  /**
   * @description [en_US] Scheduled Date
   */
  Exp_Lbl_ScheduledDate: 'Scheduled Date',

  /**
   * @description [en_US] Search by entering code or name
   */
  Exp_Lbl_SearchCodeOrName: 'Search by entering code or name',

  /**
   * @description [en_US] Approved Report List
   */
  Exp_Lbl_SearchConditionApprovelreRuestList: 'Approved Report List',

  /**
   * @description [en_US] Search Condition Name
   */
  Exp_Lbl_SearchConditionName: 'Search Condition Name',

  /**
   * @description [en_US] Find Department
   */
  Exp_Lbl_SearchConditionPlaceholderDepartment: 'Find Department',

  /**
   * @description [en_US] Find Detail
   */
  Exp_Lbl_SearchConditionPlaceholderDetail: 'Find Detail',

  /**
   * @description [en_US] Find Employee
   */
  Exp_Lbl_SearchConditionPlaceholderEmployee: 'Find Employee',

  /**
   * @description [en_US] Find Status
   */
  Exp_Lbl_SearchConditionPlaceholderStatus: 'Find Status',

  /**
   * @description [en_US] Search Results
   */
  Exp_Lbl_SearchResult: 'Search Results',

  /**
   * @description [en_US] Search and Select
   */
  Exp_Lbl_SearchSelect: 'Search and Select',

  /**
   * @description [en_US] Class
   */
  Exp_Lbl_SeatClass: 'Class',

  /**
   * @description [en_US] Selection
   */
  Exp_Lbl_Select: 'Selection',

  /**
   * @description [en_US] Select from Category
   */
  Exp_Lbl_SelectFromCategory: 'Select from Category',

  /**
   * @description [en_US] Select From Receipt Library
   */
  Exp_Lbl_SelectReceiptLibrary: 'Select From Receipt Library',

  /**
   * @description [en_US] Select Route
   */
  Exp_Lbl_SelectRoute: 'Select Route',

  /**
   * @description [en_US] Enter Name
   */
  Exp_Lbl_SetName: 'Enter Name',

  /**
   * @description [en_US] Settled By
   */
  Exp_Lbl_SettledEmp: 'Settled By',

  /**
   * @description [en_US] Settled Time
   */
  Exp_Lbl_SettledTime: 'Settled Time',

  /**
   * @description [en_US] Shop Name
   */
  Exp_Lbl_ShopName: 'Shop Name',

  /**
   * @description [en_US] Note
   */
  Exp_Lbl_ShopNote: 'Note',

  /**
   * @description [en_US] Smoking or Non
   */
  Exp_Lbl_SmokingType: 'Smoking or Non',

  /**
   * @description [en_US] Source Account of payment
   */
  Exp_Lbl_SourceAccountOfPayment: 'Source Account of payment',

  /**
   * @description [en_US] Special Requests 
   */
  Exp_Lbl_SpecialRequests: 'Special Requests ',

  /**
   * @description [en_US] Create Report
   */
  Exp_Lbl_StartClaim: 'Create Report',

  /**
   * @description [en_US] Start Date
   */
  Exp_Lbl_StartDate: 'Start Date',

  /**
   * @description [en_US]  Depart On
   */
  Exp_Lbl_StartDateTime: ' Depart On',

  /**
   * @description [en_US] Start From
   */
  Exp_Lbl_StartFrom: 'Start From',

  /**
   * @description [en_US] Start Time
   */
  Exp_Lbl_StartTime: 'Start Time',

  /**
   * @description [en_US] Rental Start Time
   */
  Exp_Lbl_StartUseDatetime: 'Rental Start Time',

  /**
   * @description [en_US] Rental Location
   */
  Exp_Lbl_StartUsePlace: 'Rental Location',

  /**
   * @description [en_US] Status
   */
  Exp_Lbl_Status: 'Status',

  /**
   * @description [en_US] Not Scanned
   */
  Exp_Lbl_StatusNotScanned: 'Not Scanned',

  /**
   * @description [en_US] Processing
   */
  Exp_Lbl_StatusProcessing: 'Processing',

  /**
   * @description [en_US] Scanned
   */
  Exp_Lbl_StatusScanned: 'Scanned',

  /**
   * @description [en_US] Submit
   */
  Exp_Lbl_Submit: 'Submit',

  /**
   * @description [en_US] Summary
   */
  Exp_Lbl_Summary: 'Summary',

  /**
   * @description [en_US] SWIFT Code
   */
  Exp_Lbl_SwiftCode: 'SWIFT Code',

  /**
   * @description [en_US] T&E Request
   */
  Exp_Lbl_TERequest: 'T&E Request',

  /**
   * @description [en_US] T&E Requests
   */
  Exp_Lbl_TERequests: 'T&E Requests',

  /**
   * @description [en_US] Target
   */
  Exp_Lbl_Target: 'Target',

  /**
   * @description [en_US] Tax Rate
   */
  Exp_Lbl_TaxRate: 'Tax Rate',

  /**
   * @description [en_US] Date Time
   */
  Exp_Lbl_Time: 'Date Time',

  /**
   * @description [en_US] Title
   */
  Exp_Lbl_Title: 'Title',

  /**
   * @description [en_US] To company
   */
  Exp_Lbl_ToCompany: 'To company',

  /**
   * @description [en_US] To home
   */
  Exp_Lbl_ToHome: 'To home',

  /**
   * @description [en_US] Total
   */
  Exp_Lbl_Total: 'Total',

  /**
   * @description [en_US] Total Amount
   */
  Exp_Lbl_TotalAmount: 'Total Amount',

  /**
   * @description [en_US] Amount
   */
  Exp_Lbl_TotalLong: 'Amount',

  /**
   * @description [en_US] Airline
   */
  Exp_Lbl_TransTypeNameAirline: 'Airline',

  /**
   * @description [en_US] Airport Bus
   */
  Exp_Lbl_TransTypeNameAirportBus: 'Airport Bus',

  /**
   * @description [en_US] Bus
   */
  Exp_Lbl_TransTypeNameBus: 'Bus',

  /**
   * @description [en_US] Car
   */
  Exp_Lbl_TransTypeNameCar: 'Car',

  /**
   * @description [en_US] Express Train
   */
  Exp_Lbl_TransTypeNameExpressTrain: 'Express Train',

  /**
   * @description [en_US] Ferry
   */
  Exp_Lbl_TransTypeNameFerry: 'Ferry',

  /**
   * @description [en_US] Highway Bus
   */
  Exp_Lbl_TransTypeNameHighwayBus: 'Highway Bus',

  /**
   * @description [en_US] JR Ordinary Line
   */
  Exp_Lbl_TransTypeNameJROrdinaryLine: 'JR Ordinary Line',

  /**
   * @description [en_US] Premier Express Train
   */
  Exp_Lbl_TransTypeNamePremierExpressTrain: 'Premier Express Train',

  /**
   * @description [en_US] Private Ordinary Line
   */
  Exp_Lbl_TransTypeNamePrivateOrdinaryLine: 'Private Ordinary Line',

  /**
   * @description [en_US] Shinkansen
   */
  Exp_Lbl_TransTypeNameShinkansen: 'Shinkansen',

  /**
   * @description [en_US] Sleeper Train
   */
  Exp_Lbl_TransTypeNameSleeperTrain: 'Sleeper Train',

  /**
   * @description [en_US] Subway
   */
  Exp_Lbl_TransTypeNameSubway: 'Subway',

  /**
   * @description [en_US] Tram
   */
  Exp_Lbl_TransTypeNameTram: 'Tram',

  /**
   * @description [en_US] Walk
   */
  Exp_Lbl_TransTypeNameWalk: 'Walk',

  /**
   * @description [en_US] Transit
   */
  Exp_Lbl_Transit: 'Transit',

  /**
   * @description [en_US] Transport Card
   */
  Exp_Lbl_TransportCard: 'Transport Card',

  /**
   * @description [en_US] Travel
   */
  Exp_Lbl_Travel: 'Travel',

  /**
   * @description [en_US] Travel Budget Request
   */
  Exp_Lbl_TravelBudgetRequest: 'Travel Budget Request',

  /**
   * @description [en_US] Unreported Expense(s)
   */
  Exp_Lbl_UnreportedExpenses: 'Unreported Expense(s)',

  /**
   * @description [en_US] Upload Receipt Image
   */
  Exp_Lbl_UploadReceiptImage: 'Upload Receipt Image',

  /**
   * @description [en_US] User Defined Field 1
   */
  Exp_Lbl_UserDefinedField1: 'User Defined Field 1',

  /**
   * @description [en_US] User Defined Field 2
   */
  Exp_Lbl_UserDefinedField2: 'User Defined Field 2',

  /**
   * @description [en_US] Vendor
   */
  Exp_Lbl_Vendor: 'Vendor',

  /**
   * @description [en_US] Vendor Details
   */
  Exp_Lbl_VendorDetail: 'Vendor Details',

  /**
   * @description [en_US] Via
   */
  Exp_Lbl_Via: 'Via',

  /**
   * @description [en_US] Visa Request
   */
  Exp_Lbl_VisaRequest: 'Visa Request',

  /**
   * @description [en_US] Visiting Address
   */
  Exp_Lbl_VisitingAddress: 'Visiting Address',

  /**
   * @description [en_US] Visiting Department
   */
  Exp_Lbl_VisitingDept: 'Visiting Department',

  /**
   * @description [en_US] Visiting Company
   */
  Exp_Lbl_VisitingFor: 'Visiting Company',

  /**
   * @description [en_US] With Meals
   */
  Exp_Lbl_WithMeals: 'With Meals',

  /**
   * @description [en_US] Withholding Tax
   */
  Exp_Lbl_WithholdingTax: 'Withholding Tax',

  /**
   * @description [en_US] Amount excl. Tax
   */
  Exp_Lbl_WithoutGst: 'Amount excl. Tax',

  /**
   * @description [en_US] Amount (excl. Tax)
   */
  Exp_Lbl_WithoutTax: 'Amount (excl. Tax)',

  /**
   * @description [en_US] Yes
   */
  Exp_Lbl_Yes: 'Yes',

  /**
   * @description [en_US] Zip Code
   */
  Exp_Lbl_ZipCode: 'Zip Code',

  /**
   * @description [en_US] -  Any individuals traveling from US to Asia Paciif may travel in business class
   */
  Exp_Msg_AirTravelPolicy2: '-  Any individuals traveling from US to Asia Paciif may travel in business class',

  /**
   * @description [en_US] -  Any managers or above traveling from US to Europ may travel in business class
   */
  Exp_Msg_AirTravelPolicy3: '-  Any managers or above traveling from US to Europ may travel in business class',

  /**
   * @description [en_US] Item is not selected
   */
  Exp_Msg_AlertNoSelect: 'Item is not selected',

  /**
   * @description [en_US] The records are cloned.
   */
  Exp_Msg_CloneRecords: 'The records are cloned.',

  /**
   * @description [en_US] The max number of records can be created by cloning at one time is 10.
   */
  Exp_Msg_CloneRecordsNumberError: 'The max number of records can be created by cloning at one time is 10.',

  /**
   * @description [en_US] The amount has been recalculated with the [%1] of the record date.
   */
  Exp_Msg_CloneRecordsRecalcution: 'The amount has been recalculated with the [%1] of the record date.',

  /**
   * @description [en_US] exchange rate
   */
  Exp_Msg_CloneRecordsRecalcutionExchangeRate: 'exchange rate',

  /**
   * @description [en_US] tax rate
   */
  Exp_Msg_CloneRecordsRecalcutionTaxRate: 'tax rate',

  /**
   * @description [en_US] Are you sure to delete?
   */
  Exp_Msg_ConfirmDelete: 'Are you sure to delete?',

  /**
   * @description [en_US] Are you sure to delete the selected records? This operation can not be undone.
   */
  Exp_Msg_ConfirmDeleteSelectedRecords: 'Are you sure to delete the selected records? This operation can not be undone.',

  /**
   * @description [en_US] Click [New] button to create a new record.
   */
  Exp_Msg_CreateNewRecord: 'Click [New] button to create a new record.',

  /**
   * @description [en_US] Click [New Report] button to create a new request.
   */
  Exp_Msg_CreateNewReport: 'Click [New Report] button to create a new request.',

  /**
   * @description [en_US] The amount exceeded the allowable limit of accommodation expense.
   */
  Exp_Msg_HotelExcess: 'The amount exceeded the allowable limit of accommodation expense.',

  /**
   * @description [en_US] The allowable limit of accommodation expense for supervisor is 
   */
  Exp_Msg_HotelLimitsOfDirector1: 'The allowable limit of accommodation expense for supervisor is ',

  /**
   * @description [en_US] .
   */
  Exp_Msg_HotelLimitsOfDirector2: '.',

  /**
   * @description [en_US] If you need help, please contact system administrator.
   */
  Exp_Msg_Inquire: 'If you need help, please contact system administrator.',

  /**
   * @description [en_US] The allowable limit of entertainment per person is
   */
  Exp_Msg_LimitsOfReception1: 'The allowable limit of entertainment per person is',

  /**
   * @description [en_US] .
   */
  Exp_Msg_LimitsOfReception2: '.',

  /**
   * @description [en_US] Local amount does not match record items.
   */
  Exp_Msg_LocalAmountMismatchItems: 'Local amount does not match record items.',

  /**
   * @description [en_US] The allowable per night limit of accommodation expense for manager is
   */
  Exp_Msg_LodgingPolicy1: 'The allowable per night limit of accommodation expense for manager is',

  /**
   * @description [en_US] Singapore : USD 200 per night
   */
  Exp_Msg_LodgingPolicy2: 'Singapore : USD 200 per night',

  /**
   * @description [en_US] Tokyo : USD 180 per night
   */
  Exp_Msg_LodgingPolicy3: 'Tokyo : USD 180 per night',

  /**
   * @description [en_US] No Base Currency found
   */
  Exp_Msg_NoBaseCurrencyForExpense: 'No Base Currency found',

  /**
   * @description [en_US] Please set a Base Currency to enable the Expense function.
   */
  Exp_Msg_NoBaseCurrencyForExpenseSolution: 'Please set a Base Currency to enable the Expense function.',

  /**
   * @description [en_US] No Approved reports found
   */
  Exp_Msg_NoPassedReportFound: 'No Approved reports found',

  /**
   * @description [en_US] No permission to use Expenses
   */
  Exp_Msg_NoPermissionForExpense: 'No permission to use Expenses',

  /**
   * @description [en_US] No Receipt Image.
   */
  Exp_Msg_NoRecipt: 'No Receipt Image.',

  /**
   * @description [en_US] No Record Item
   */
  Exp_Msg_NoRecordItem: 'No Record Item',

  /**
   * @description [en_US] No reports found
   */
  Exp_Msg_NoReportFound: 'No reports found',

  /**
   * @description [en_US] Please specify the payment date of your own expense.
   */
  Exp_Msg_PaymentDateOwnExpense: 'Please specify the payment date of your own expense.',

  /**
   * @description [en_US] Per Night
   */
  Exp_Msg_PerNight: 'Per Night',

  /**
   * @description [en_US] Record items is mandatory for [%1]. Please create record items.
   */
  Exp_Msg_RecordItemsMandatory: 'Record items is mandatory for [%1]. Please create record items.',

  /**
   * @description [en_US] This report is created from the request. You cannot change the report type.
   */
  Exp_Msg_ReportTypeInfo: 'This report is created from the request. You cannot change the report type.',

  /**
   * @description [en_US] This report is created from the request. You cannot change the report type.
   */
  Exp_Msg_ReportTypeInfoForExpense: 'This report is created from the request. You cannot change the report type.',

  /**
   * @description [en_US] Report type can only be modified by employee.
   */
  Exp_Msg_ReportTypeInfoForFinanceApproval: 'Report type can only be modified by employee.',

  /**
   * @description [en_US] Search by your department at the time of the report
   */
  Exp_Msg_SearchByDepartment: 'Search by your department at the time of the report',

  /**
   * @description [en_US] Total amount does not match record amount.
   */
  Exp_Msg_TotalAmountMismatch: 'Total amount does not match record amount.',

  /**
   * @description [en_US] Total amount does not match record items.
   */
  Exp_Msg_TotalAmountMismatchItems: 'Total amount does not match record items.',

  /**
   * @description [en_US] Amount (excl. Tax) does not match record items.
   */
  Exp_Msg_WithoutTaxAmountMismatchItems: 'Amount (excl. Tax) does not match record items.',

  /**
   * @description [en_US] Allowance
   */
  Exp_Sel_Allowance: 'Allowance',

  /**
   * @description [en_US] Auto Select from Expense Type
   */
  Exp_Sel_Auto_Select_From_Exp_Type: 'Auto Select from Expense Type',

  /**
   * @description [en_US] Checking Account
   */
  Exp_Sel_BankChecking: 'Checking Account',

  /**
   * @description [en_US] Savings Account
   */
  Exp_Sel_BankSavings: 'Savings Account',

  /**
   * @description [en_US] Start Date
   */
  Exp_Sel_BaseDeparture: 'Start Date',

  /**
   * @description [en_US] Return Date
   */
  Exp_Sel_BaseReturn: 'Return Date',

  /**
   * @description [en_US] Breakfast
   */
  Exp_Sel_Breakfast: 'Breakfast',

  /**
   * @description [en_US] Business
   */
  Exp_Sel_Business: 'Business',

  /**
   * @description [en_US] Cash
   */
  Exp_Sel_Cash: 'Cash',

  /**
   * @description [en_US] Cash Advance
   */
  Exp_Sel_CashAdvance: 'Cash Advance',

  /**
   * @description [en_US] Meeting expense
   */
  Exp_Sel_Conference: 'Meeting expense',

  /**
   * @description [en_US] Dinner
   */
  Exp_Sel_Dinner: 'Dinner',

  /**
   * @description [en_US] Per Diem
   */
  Exp_Sel_DomesticPerDiem: 'Per Diem',

  /**
   * @description [en_US] Domestic Travel
   */
  Exp_Sel_DomesticTravel: 'Domestic Travel',

  /**
   * @description [en_US] Each Breakdown
   */
  Exp_Sel_EachBreakdown: 'Each Breakdown',

  /**
   * @description [en_US] Economy
   */
  Exp_Sel_Economy: 'Economy',

  /**
   * @description [en_US] Ekitan
   */
  Exp_Sel_Ekitan: 'Ekitan',

  /**
   * @description [en_US] Entertainment
   */
  Exp_Sel_Entertainment: 'Entertainment',

  /**
   * @description [en_US] Expense
   */
  Exp_Sel_Expense: 'Expense',

  /**
   * @description [en_US] First
   */
  Exp_Sel_First: 'First',

  /**
   * @description [en_US] Fixed Amount
   */
  Exp_Sel_FixedAmount: 'Fixed Amount',

  /**
   * @description [en_US] Fixed Amount(Multiple-choice)
   */
  Exp_Sel_FixedAmount_Mul: 'Fixed Amount(Multiple-choice)',

  /**
   * @description [en_US] Air Ticket
   */
  Exp_Sel_Flight: 'Air Ticket',

  /**
   * @description [en_US] Fixed
   */
  Exp_Sel_ForeginCurrency_Fixed: 'Fixed',

  /**
   * @description [en_US] Flexible
   */
  Exp_Sel_ForeginCurrency_Flexible: 'Flexible',

  /**
   * @description [en_US] Not Used
   */
  Exp_Sel_ForeginCurrency_NotUsed: 'Not Used',

  /**
   * @description [en_US] General Expense
   */
  Exp_Sel_GeneralExpense: 'General Expense',

  /**
   * @description [en_US] General Expense Hidden
   */
  Exp_Sel_GeneralExpenseHidden: 'General Expense Hidden',

  /**
   * @description [en_US] General Expense Optional
   */
  Exp_Sel_GeneralExpenseOptional: 'General Expense Optional',

  /**
   * @description [en_US] Accommodation
   */
  Exp_Sel_Hotel: 'Accommodation',

  /**
   * @description [en_US] Hotel Fee
   */
  Exp_Sel_HotelFee: 'Hotel Fee',

  /**
   * @description [en_US] Nation Wide
   */
  Exp_Sel_JorudanAreaPreferenceOption00: 'Nation Wide',

  /**
   * @description [en_US] Kanto
   */
  Exp_Sel_JorudanAreaPreferenceOption01: 'Kanto',

  /**
   * @description [en_US] Kinki
   */
  Exp_Sel_JorudanAreaPreferenceOption02: 'Kinki',

  /**
   * @description [en_US] Hokkaido
   */
  Exp_Sel_JorudanAreaPreferenceOption03: 'Hokkaido',

  /**
   * @description [en_US] Tohoku
   */
  Exp_Sel_JorudanAreaPreferenceOption04: 'Tohoku',

  /**
   * @description [en_US] Tokai
   */
  Exp_Sel_JorudanAreaPreferenceOption05: 'Tokai',

  /**
   * @description [en_US] Hokuriku
   */
  Exp_Sel_JorudanAreaPreferenceOption06: 'Hokuriku',

  /**
   * @description [en_US] Chugoku
   */
  Exp_Sel_JorudanAreaPreferenceOption07: 'Chugoku',

  /**
   * @description [en_US] Shikoku
   */
  Exp_Sel_JorudanAreaPreferenceOption08: 'Shikoku',

  /**
   * @description [en_US] Kyushu
   */
  Exp_Sel_JorudanAreaPreferenceOption09: 'Kyushu',

  /**
   * @description [en_US] calcurating expense without commuter pass
   */
  Exp_Sel_JorudanCommuterPassOption0: 'calcurating expense without commuter pass',

  /**
   * @description [en_US] not considering
   */
  Exp_Sel_JorudanCommuterPassOption1: 'not considering',

  /**
   * @description [en_US] Ticket
   */
  Exp_Sel_JorudanFareTypeOption0: 'Ticket',

  /**
   * @description [en_US] IC Card
   */
  Exp_Sel_JorudanFareTypeOption1: 'IC Card',

  /**
   * @description [en_US] Use
   */
  Exp_Sel_JorudanHighwayBusOption0: 'Use',

  /**
   * @description [en_US] Not Use
   */
  Exp_Sel_JorudanHighwayBusOption1: 'Not Use',

  /**
   * @description [en_US] Early
   */
  Exp_Sel_JorudanRouteSortOption0: 'Early',

  /**
   * @description [en_US] Cheap
   */
  Exp_Sel_JorudanRouteSortOption1: 'Cheap',

  /**
   * @description [en_US] Easy
   */
  Exp_Sel_JorudanRouteSortOption2: 'Easy',

  /**
   * @description [en_US] Reserve
   */
  Exp_Sel_JorudanSeatPreferenceOption0: 'Reserve',

  /**
   * @description [en_US] No Reserve
   */
  Exp_Sel_JorudanSeatPreferenceOption1: 'No Reserve',

  /**
   * @description [en_US] Green
   */
  Exp_Sel_JorudanSeatPreferenceOption2: 'Green',

  /**
   * @description [en_US] Use
   */
  Exp_Sel_JorudanUseChargedExpressOption0: 'Use',

  /**
   * @description [en_US] Not Use
   */
  Exp_Sel_JorudanUseChargedExpressOption1: 'Not Use',

  /**
   * @description [en_US] Use When over Specified Distance
   */
  Exp_Sel_JorudanUseChargedExpressOption2: 'Use When over Specified Distance',

  /**
   * @description [en_US] Not Need
   */
  Exp_Sel_NoBooking: 'Not Need',

  /**
   * @description [en_US] No Receipt
   */
  Exp_Sel_NoRecipt: 'No Receipt',

  /**
   * @description [en_US] Not Used
   */
  Exp_Sel_NoUsed: 'Not Used',

  /**
   * @description [en_US] Non-Smoking
   */
  Exp_Sel_NonSmoking: 'Non-Smoking',

  /**
   * @description [en_US] Nontaxable
   */
  Exp_Sel_Nontaxable: 'Nontaxable',

  /**
   * @description [en_US] Not Selected
   */
  Exp_Sel_NotSelected: 'Not Selected',

  /**
   * @description [en_US] Operator Booking
   */
  Exp_Sel_OperatorBooking: 'Operator Booking',

  /**
   * @description [en_US] Optional
   */
  Exp_Sel_Optional: 'Optional',

  /**
   * @description [en_US] Osaka
   */
  Exp_Sel_Osaka: 'Osaka',

  /**
   * @description [en_US] Other
   */
  Exp_Sel_Other: 'Other',

  /**
   * @description [en_US] Overseas Per Diem
   */
  Exp_Sel_OverseasPerDiem: 'Overseas Per Diem',

  /**
   * @description [en_US] Overseas Travel
   */
  Exp_Sel_OverseasTravel: 'Overseas Travel',

  /**
   * @description [en_US] Advanced Booking
   */
  Exp_Sel_PreBooking: 'Advanced Booking',

  /**
   * @description [en_US] Hidden
   */
  Exp_Sel_ReceiptType_Hidden: 'Hidden',

  /**
   * @description [en_US] Not Used
   */
  Exp_Sel_ReceiptType_NotUsed: 'Not Used',

  /**
   * @description [en_US] Optional
   */
  Exp_Sel_ReceiptType_Optional: 'Optional',

  /**
   * @description [en_US] Required
   */
  Exp_Sel_ReceiptType_Required: 'Required',

  /**
   * @description [en_US] Entertainment
   */
  Exp_Sel_Reception: 'Entertainment',

  /**
   * @description [en_US] Entertainment
   */
  Exp_Sel_RecordEntertainment: 'Entertainment',

  /**
   * @description [en_US] Rent-a-car
   */
  Exp_Sel_RentCar: 'Rent-a-car',

  /**
   * @description [en_US] Required
   */
  Exp_Sel_Required: 'Required',

  /**
   * @description [en_US] Reserved Seat
   */
  Exp_Sel_ReservedSeat: 'Reserved Seat',

  /**
   * @description [en_US] Republic of Singapore
   */
  Exp_Sel_Singapore: 'Republic of Singapore',

  /**
   * @description [en_US] Smoking
   */
  Exp_Sel_Smoking: 'Smoking',

  /**
   * @description [en_US] Taxable
   */
  Exp_Sel_Tax: 'Taxable',

  /**
   * @description [en_US] Tokyo
   */
  Exp_Sel_Tokyo: 'Tokyo',

  /**
   * @description [en_US] Amount
   */
  Exp_Sel_TotalAmount: 'Amount',

  /**
   * @description [en_US] Train
   */
  Exp_Sel_Train: 'Train',

  /**
   * @description [en_US] Transit Jorudan JP
   */
  Exp_Sel_TransitJorudanJP: 'Transit Jorudan JP',

  /**
   * @description [en_US] Transportation
   */
  Exp_Sel_Transportation: 'Transportation',

  /**
   * @description [en_US] Accounting Authorized
   */
  Exp_Status_AccountingAuthorized: 'Accounting Authorized',

  /**
   * @description [en_US] Accounting Rejected
   */
  Exp_Status_AccountingRejected: 'Accounting Rejected',

  /**
   * @description [en_US] Approved Request
   */
  Exp_Status_ApprovedPreRequest: 'Approved Request',

  /**
   * @description [en_US] Expense Claimed
   */
  Exp_Status_Claimed: 'Expense Claimed',

  /**
   * @description [en_US] Paid
   */
  Exp_Status_FullyPaid: 'Paid',

  /**
   * @description [en_US] Journal Data Created
   */
  Exp_Status_JournalCreated: 'Journal Data Created',

  /**
   * @description [en_US] There\'s no work information on the day.
   */
  Exp_Warn_AttRecordNotFound: 'There\'s no work information on the day.',

  /**
   * @description [en_US] The day was absence.
   */
  Exp_Warn_DateIsAbsence: 'The day was absence.',

  /**
   * @description [en_US] The day was holiday.
   */
  Exp_Warn_DateIsHoliday: 'The day was holiday.',

  /**
   * @description [en_US] The day was leave.
   */
  Exp_Warn_DateIsLeave: 'The day was leave.',

  /**
   * @description [en_US] The day was leave of absence.
   */
  Exp_Warn_DateIsLeaveOfAbsence: 'The day was leave of absence.',

  /**
   * @description [en_US] The day was legal holiday.
   */
  Exp_Warn_DateIsLegalHoliday: 'The day was legal holiday.',

  /**
   * @description [en_US] There is a problem with work information on the day.
   */
  Exp_Warn_IllegalAttCalculation: 'There is a problem with work information on the day.',

  /**
   * @description [en_US] Following expense type cannot be used with the selected report type:
   */
  Exp_Warn_InvalidRecordExpenseTypeForReportTypeMobile: 'Following expense type cannot be used with the selected report type:',

  /**
   * @description [en_US] New Record Item
   */
  Exp_btn_AddRecordItem: 'New Record Item',

  /**
   * @description [en_US] An error occurred. Try again at a place with stable network.
   */
  Mobile_Err_ConnectionError: 'An error occurred. Try again at a place with stable network.',

  /**
   * @description [en_US] Authorization Error
   */
  Oauth_Err_CalendarAccessAuthError: 'Authorization Error',

  /**
   * @description [en_US] Failed to get enough authorization parameter. It may be because you have canceled authorization or jumped to this page directly.
   */
  Oauth_Msg_CalendarAccessAuthErrorNoParam: 'Failed to get enough authorization parameter. It may be because you have canceled authorization or jumped to this page directly.',

  /**
   * @description [en_US] Close this page and try authorization again.
   */
  Oauth_Msg_CalendarAccessAuthErrorNoParamSolution: 'Close this page and try authorization again.',

  /**
   * @description [en_US] Succeeded authorization for the external calendar service.
   */
  Oauth_Msg_CalendarAccessAuthSuccess: 'Succeeded authorization for the external calendar service.',

  /**
   * @description [en_US] Close this window.
   */
  Oauth_Msg_CalendarAccessAuthSuccessRemarks: 'Close this window.',

  /**
   * @description [en_US] Department
   */
  Team_Lbl_Department: 'Department',

  /**
   * @description [en_US] Employee name
   */
  Team_Lbl_EmployeeCode: 'Employee name',

  /**
   * @description [en_US] Employee name
   */
  Team_Lbl_EmployeeName: 'Employee name',

  /**
   * @description [en_US] Not affliliation
   */
  Team_Lbl_EmptyDepartment: 'Not affliliation',

  /**
   * @description [en_US] Open this timesheet
   */
  Team_Lbl_OpenTimesheet: 'Open this timesheet',

  /**
   * @description [en_US] There is no employee.
   */
  Team_Msg_EmptyEmployeeList: 'There is no employee.',

  /**
   * @description [en_US] Approve
   */
  Time_Action_Approve: 'Approve',

  /**
   * @description [en_US] Cancel Approval
   */
  Time_Action_CancelApproval: 'Cancel Approval',

  /**
   * @description [en_US] Recall
   */
  Time_Action_Recall: 'Recall',

  /**
   * @description [en_US] Reject
   */
  Time_Action_Reject: 'Reject',

  /**
   * @description [en_US] Submit
   */
  Time_Action_Submit: 'Submit',

  /**
   * @description [en_US] Current Period
   */
  Time_Btn_CurrentPeriod: 'Current Period',

  /**
   * @description [en_US] Canceled Approved Time Report.
   */
  Time_Lbl_CancelApprovalSucceeded: 'Canceled Approved Time Report.',

  /**
   * @description [en_US] Job/Work Category
   */
  Time_Lbl_JobName: 'Job/Work Category',

  /**
   * @description [en_US] Recalled Time Report.
   */
  Time_Lbl_RecallSucceeded: 'Recalled Time Report.',

  /**
   * @description [en_US] Submitted Time Report.
   */
  Time_Lbl_SubmitSucceeded: 'Submitted Time Report.',

  /**
   * @description [en_US] Time Tracking Summary
   */
  Time_Lbl_TrackSummaryTitle: 'Time Tracking Summary',

  /**
   * @description [en_US] Hours Spent
   */
  Time_Lbl_WorkingHours: 'Hours Spent',

  /**
   * @description [en_US] % in Hours Sepnt
   */
  Time_Lbl_WorkingPercentage: '% in Hours Sepnt',

  /**
   * @description [en_US] Approved
   */
  Time_Status_Approved: 'Approved',

  /**
   * @description [en_US] Canceled
   */
  Time_Status_Canceled: 'Canceled',

  /**
   * @description [en_US] No Requested
   */
  Time_Status_NotRequested: 'No Requested',

  /**
   * @description [en_US] Pending
   */
  Time_Status_Pending: 'Pending',

  /**
   * @description [en_US] Recalled
   */
  Time_Status_Recalled: 'Recalled',

  /**
   * @description [en_US] Rejected
   */
  Time_Status_Rejected: 'Rejected',

  /**
   * @description [en_US] Recalled
   */
  Time_Status_Removed: 'Recalled',

  /**
   * @description [en_US] Add a Job
   */
  Trac_Lbl_AddJob: 'Add a Job',

  /**
   * @description [en_US] Request Cancel
   */
  Trac_Lbl_Cancel: 'Request Cancel',

  /**
   * @description [en_US] Comments
   */
  Trac_Lbl_Comments: 'Comments',

  /**
   * @description [en_US] Time Tracking
   */
  Trac_Lbl_DailyTimeTrack: 'Time Tracking',

  /**
   * @description [en_US] Date
   */
  Trac_Lbl_Date: 'Date',

  /**
   * @description [en_US] Job
   */
  Trac_Lbl_Job: 'Job',

  /**
   * @description [en_US] Next approval user
   */
  Trac_Lbl_NextApproverEmployee: 'Next approval user',

  /**
   * @description [en_US] Actual Work Hours
   */
  Trac_Lbl_RealWorkTime: 'Actual Work Hours',

  /**
   * @description [en_US] Request Approval
   */
  Trac_Lbl_Request: 'Request Approval',

  /**
   * @description [en_US] Time Tracking
   */
  Trac_Lbl_TimeTrack: 'Time Tracking',

  /**
   * @description [en_US] Total
   */
  Trac_Lbl_Total: 'Total',

  /**
   * @description [en_US] Tracked
   */
  Trac_Lbl_Tracked: 'Tracked',

  /**
   * @description [en_US] Hours Spent
   */
  Trac_Lbl_Work: 'Hours Spent',

  /**
   * @description [en_US] Work Category
   */
  Trac_Lbl_WorkCategory: 'Work Category',

  /**
   * @description [en_US] Hours Spent
   */
  Trac_Lbl_WorkTime: 'Hours Spent',

  /**
   * @description [en_US] Graph
   */
  Trac_Lbl_WorkTimeGraph: 'Graph',
  ['']: '',
};

export default en_US;
