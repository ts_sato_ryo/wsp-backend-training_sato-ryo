// @flow

/* eslint-disable camelcase */

import _ from 'lodash';

import { compose } from '../utils/FnUtil';

import ja from './Msg_ja';
import en_US from './Msg_en_US';

const defaultLang = en_US;

type MessageDictionary = typeof ja | typeof en_US | typeof defaultLang;

let __IS_INITIALIZED__ = false;

let languages: { [string]: MessageDictionary } = { ja, en_US, defaultLang };

const SEPARATOR: RegExp = /{(.+)}/g;

const configureTemplate = () => {
  _.templateSettings.interpolate = SEPARATOR;
};

const expandVariables = (messages: MessageDictionary): MessageDictionary => {
  const variables: Object = _(messages)
    .toPairs()
    .filter(([id, _message]) => /^\$/g.test(id))
    .reduce((acc, [id, message]) => ({ ...acc, [id]: message }), {});
  const expanded = _(messages)
    .toPairs()
    .filter(
      ([_id, message]: [string, string]) => message && /{(.+)}/g.test(message)
    )
    .reduce(
      (expandedMessages: Object, [id, message]: [string, string]) => ({
        ...expandedMessages,
        [id]: _.template(message)(variables),
      }),
      {}
    );
  return {
    ...messages,
    ...expanded,
  };
};

const mergeUserDefinedMessages = (lang: string) => (
  messages: MessageDictionary
): MessageDictionary => {
  return window.customMessageMap && window.customMessageMap[lang]
    ? {
        ...messages,
        ...window.customMessageMap[lang],
      }
    : messages;
};

const buildMessage = (
  lang: string,
  messages: MessageDictionary
): MessageDictionary =>
  compose(
    expandVariables,
    mergeUserDefinedMessages(lang)
  )(messages);

/**
 * Build translated messages for each language.
 */
export const setupMessages = (): void => {
  configureTemplate();

  languages = {
    ja: buildMessage('ja', ja),
    en_US: buildMessage('en_US', en_US),
    defaultLang: buildMessage('en_US', defaultLang),
  };
};

export default (): MessageDictionary => {
  if (!__IS_INITIALIZED__ && window.customMessageMap) {
    setupMessages();
    __IS_INITIALIZED__ = true;
  }

  // TODO: windowを使わないようにする
  if (!window.empInfo) {
    return languages.defaultLang;
  }
  switch (window.empInfo.language) {
    case 'ja':
      return languages.ja;
    default:
      return languages.defaultLang;
  }
};

/* eslint-enable */
