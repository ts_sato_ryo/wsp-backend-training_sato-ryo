// @flow

/**
 *
 * define custome event name
 *  Rule
 *   -  Value is `'EVENT'/DOMAIN/EVENTNAME`
 */

export const TIME_TRACK_UPDATED = 'EVENT/TIME_TRACKING/TIME_TRACK_UPDATED';

export const DEFAULT_JOB_UPDATED = 'EVENT/TIME_TRACKING/DEFAULT_JOB_UPDATED';
