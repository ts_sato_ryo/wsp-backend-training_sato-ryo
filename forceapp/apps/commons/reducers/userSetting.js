// @flow
import { GET_USER_SETTING, type Action } from '../actions/userSetting';
import type { UserSetting } from '../../domain/models/UserSetting';

export type State = UserSetting;

const initialState: State = {
  id: '',
  name: '',
  userName: '',
  photoUrl: '',
  language: '',
  managerName: '',
  companyId: '',
  companyName: '',
  costCenterHistoryId: '',
  costCenterCode: '',
  costCenterName: '',
  departmentId: '',
  departmentCode: '',
  departmentName: '',
  employeeId: '',
  employeeCode: '',
  employeeName: '',
  allowToChangeApproverSelf: false,
  allowTaxAmountChange: false,
  approver01Name: '',
  requireLocationAtMobileStamp: false,
  currencySymbol: '',
  currencyDecimalPlaces: 0,
  useExpense: false,
  useExpenseRequest: false,
  useAttendance: false,
  usePlanner: false,
  useWorkTime: false,
  currencyId: '',
  currencyCode: '',
};

export default function userSettingReducer(
  state: State = initialState,
  action: Action
): State {
  switch (action.type) {
    case GET_USER_SETTING:
      return action.payload;

    default:
      return state;
  }
}
