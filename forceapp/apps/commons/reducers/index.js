// @flow

import { combineReducers } from 'redux';

import app from '../modules/app';
import accessControl from '../modules/accessControl';
import approverEmployeeSetting from '../modules/approverEmployeeSetting';
import approverEmployeeSearch from '../modules/approverEmployeeSearch';
import approvalHistoryList from './approvalHistoryList';
import costCenterList from './costCenterList';
import currencyList from './currencyList';
import dialog from './dialog';
import delegateApprovalDialog from '../modules/delegateApprovalDialog';
import empInfo from './empInfo';
import expTypeList from './expTypeList';
import jobList from './jobList';
import jobSelectDialog from '../modules/jobSelectDialog';
import personalSetting from '../modules/personalSetting';
import personalSettingDialog from '../modules/personalSettingDialog';
import proxyEmployeeInfo from '../modules/proxyEmployeeInfo';
import searchedRoute from './searchedRoute';
import selectedTab from './selectedTab';
import standaloneMode from '../modules/standaloneMode';
import stampWidget from './stampWidget';
import stationHistoryList from './stationHistoryList';
import suggestInput from './suggestInput';
import targetEmpId from './targetEmpId';
import userSetting from './userSetting';
import expTaxTypeList from '../../domain/modules/exp/taxType';
import toast from '../modules/toast';

import type { $State } from '../utils/TypeUtil';

const reducers = {
  app,
  accessControl,
  dialog,
  delegateApprovalDialog,
  empInfo,
  proxyEmployeeInfo,
  targetEmpId,
  approverEmployeeSetting,
  approverEmployeeSearch,
  approvalHistoryList,
  jobList,
  expTypeList,
  expTaxTypeList,
  costCenterList,
  selectedTab,
  currencyList,
  suggestInput,
  searchedRoute,
  standaloneMode,
  stationHistoryList,
  userSetting,
  personalSetting,
  personalSettingDialog,
  jobSelectDialog,
  stampWidget,
  toast,
};

export type State = $State<typeof reducers>;

const rootReducer = combineReducers<Object, Object>(reducers);

export default rootReducer;
