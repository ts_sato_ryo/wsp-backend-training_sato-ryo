// @flow
import { SELECT_TAB, type Action, type SelectTabAction } from '../actions/tab';
import tabType, { type TabType } from '../constants/tabType';

export type State = TabType;

export default function tabReducer(
  state: State = tabType.NONE,
  action: Action
): State {
  switch (action.type) {
    case SELECT_TAB:
      return (action: SelectTabAction).payload;

    default:
      return state;
  }
}
