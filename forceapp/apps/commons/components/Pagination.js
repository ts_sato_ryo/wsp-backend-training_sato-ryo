// @flow
import React from 'react';
import classNames from 'classnames';
import msg from '../languages';
import './Pagination.scss';

const ROOT = 'ts-pager';

export type Props = {
  className?: ?string,
  currentPage: number,
  totalNum: number,
  displayNum: number,
  pageSize: number,
  maxPageNum: number,
  onClickPagerLink: (number) => void,
};

export default class Pager extends React.PureComponent<Props> {
  createPager(totalPageNum: number) {
    const { currentPage } = this.props;
    // When the total number of pages is smaller than the displayed number,
    // the total number of pages is set as the upper limit
    const finalDisplayNum =
      totalPageNum < this.props.displayNum
        ? totalPageNum
        : this.props.displayNum;

    // central index
    const centerIdx = Math.floor(finalDisplayNum / 2);

    // Since display is treated as length, predicted total value with -1
    const prediction = currentPage + (finalDisplayNum - 1);

    // list start number
    let num = currentPage - centerIdx;

    if (currentPage <= centerIdx + 1) {
      num = 1;
    } else if (prediction > totalPageNum) {
      // If it exceeds the total number of pages, minus
      num = currentPage + (totalPageNum - prediction);
    }

    const pages = [];
    for (let i = 0; i < finalDisplayNum; i++) {
      pages.push(num + i);
    }

    return pages;
  }

  renderPager(label: number | string, pageNum: number, disable: boolean) {
    return (
      <div className={`${ROOT}__page-item`} key={`${label}${pageNum}`}>
        {disable || pageNum === this.props.currentPage ? (
          label
        ) : (
          <a onClick={() => this.props.onClickPagerLink(pageNum)} href={null}>
            {label}
          </a>
        )}
      </div>
    );
  }

  render() {
    const { currentPage, pageSize, totalNum, maxPageNum } = this.props;
    const lastPageNum = Math.ceil(totalNum / pageSize);
    const finalLastPageNum =
      maxPageNum < lastPageNum ? maxPageNum : lastPageNum;

    const pager = this.createPager(finalLastPageNum);
    const isVisibleFirstPage = pager[0] !== 1;
    const isVisibleEllipsisAfterFirstPage = pager[0] > 2;
    const isVisibleLastPage = pager[pager.length - 1] !== finalLastPageNum;
    const isVisibleEllipsisBeforeLastPage =
      pager[pager.length - 1] < finalLastPageNum;

    // first - last / totalNum
    const first = (currentPage - 1) * pageSize + 1;
    let last;

    // when display last page
    if (currentPage === finalLastPageNum) {
      if (maxPageNum < lastPageNum) {
        // if limited num of api is max 1000,last is 1000
        last = maxPageNum * pageSize;
      } else {
        last = totalNum;
      }
    } else {
      last = currentPage * pageSize;
    }

    const cssName = classNames(ROOT, this.props.className);

    return (
      <div className={cssName}>
        <div className={`${ROOT}__page`}>
          {this.renderPager('<', currentPage - 1, currentPage < 2)}
          {isVisibleFirstPage && this.renderPager(1, 1, false)}
          {isVisibleEllipsisAfterFirstPage && <div>...</div>}
          {pager.map((page) => this.renderPager(page, page, false))}
          {isVisibleEllipsisBeforeLastPage && <div>...</div>}
          {isVisibleLastPage &&
            this.renderPager(finalLastPageNum, finalLastPageNum, false)}
          {this.renderPager(
            '>',
            currentPage + 1,
            currentPage === finalLastPageNum
          )}
        </div>
        <div className={`${ROOT}__count`}>
          {first} - {last} / {this.props.totalNum} {msg().Exp_Lbl_RecordCount}
        </div>
      </div>
    );
  }
}
