import React from 'react';
import PropTypes from 'prop-types';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import classNames from 'classnames';

const reorder = (list, startIndex, endIndex) => {
  const result = [...list];
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const ROOT = 'commons-reorderble-list';

export default class ReorderbleList extends React.Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    children: PropTypes.func.isRequired,
    onDragEnd: PropTypes.func.isRequired,
    isDropDisabled: PropTypes.bool,
    className: PropTypes.string,
  };

  static defaultProps = {
    isDropDisabled: false,
    className: null,
  };

  constructor() {
    super();
    this.onDragEnd = this.onDragEnd.bind(this);
  }

  onDragEnd(result) {
    if (!result.destination) {
      return;
    }

    const reorderedList = reorder(
      this.props.list,
      result.source.index,
      result.destination.index
    );

    this.props.onDragEnd(reorderedList);
  }

  render() {
    return (
      <DragDropContext
        onDragEnd={this.onDragEnd}
        isDropDisabled={this.props.isDropDisabled}
      >
        <Droppable droppableId="general">
          {(provided, snapshot) => {
            const className = classNames(ROOT, {
              [this.props.className || '']: this.props.className,
              [`${ROOT}--disabled`]: this.props.isDropDisabled,
              [`${ROOT}--dragging`]: snapshot.isDraggingOver,
            });

            return (
              <ol
                className={className}
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                {(this.props.list || []).map(this.props.children)}
                {provided.placeholder}
              </ol>
            );
          }}
        </Droppable>
      </DragDropContext>
    );
  }
}
