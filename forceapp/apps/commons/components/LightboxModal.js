import React from 'react';

import IconButton from './buttons/IconButton';
import btnCloseZoom from '../images-pre-optimized/btnCloseZoom.png';
import btnRotate from '../images-pre-optimized/btnRotate.png';

export default class LightboxModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // false positive
      // eslint-disable-next-line react/no-unused-state
      rotate: 0,
      // false positive
      // eslint-disable-next-line react/no-unused-state
      bottom: null,
      // false positive
      // eslint-disable-next-line react/no-unused-state
      top: 0,
      // false positive
      // eslint-disable-next-line react/no-unused-state
      isTallImage: false,
    };
  }

  componentDidMount() {
    document.addEventListener('click', this.handleOutsideClick, false);
    this.resetImageInitialState(this.props);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleOutsideClick, false);
  }

  resetImageInitialState = (props) => {
    const { img } = this;
    const { imgContainer } = this;
    const isTallImage =
      img.clientHeight > imgContainer.clientHeight &&
      img.clientWidth < imgContainer.clientWidth &&
      img.clientWidth < imgContainer.clientHeight;
    const newImg = new Image();

    newImg.src = props.src;
    newImg.onload = () => {
      this.setState({
        // false positive
        // eslint-disable-next-line react/no-unused-state
        rotate: 0,
        // false positive
        // eslint-disable-next-line react/no-unused-state
        isTallImage,
      });
    };
  };

  handleRotate = (angle) => {
    const { img } = this;
    this.setState((prevState) => {
      const rotate = (360 + prevState.rotate + angle) % 360;
      let top = 0;
      let bottom = null;

      if ((rotate / 90) % 2) {
        // 90 || 270 rotation
        if (prevState.isTallImage) {
          bottom = 0;
        } else {
          top = (img.clientWidth - img.clientHeight) / 2;
        }
      }

      return {
        rotate,
        // false positive
        // eslint-disable-next-line react/no-unused-state
        top,
        // false positive
        // eslint-disable-next-line react/no-unused-state
        bottom,
      };
    });
  };

  handleRotateClockwise = () => {
    this.handleRotate.call(this, 90);
  };

  handleClose = () => {
    this.props.toggleLightbox();
  };

  handleOutsideClick = (e) => {
    const { img } = this;
    const isButton = e.target.className === 'ts-icon-button__image';
    // ignore clicks on the component itself
    if (img.contains(e.target) || isButton) {
      return;
    }

    this.handleClose();
  };

  render() {
    const [props, state] = [this.props, this.state];
    const transform = `rotate(${state.rotate}deg)`;

    const styles = {
      transform,
      top: state.top,
      bottom: state.bottom,
    };

    return (
      <div className="lightbox__modal">
        <div className="lightbox__modal__image-controls">
          <IconButton
            src={btnRotate}
            onClick={this.handleRotateClockwise}
            alt="rotate"
          />
          <IconButton
            src={btnCloseZoom}
            onClick={this.handleClose}
            alt="close"
          />
        </div>
        <div
          className="lightbox__modal__image-container"
          ref={(el) => {
            this.imgContainer = el;
          }}
        >
          <img
            src={props.src}
            alt="expense-evidence"
            style={styles}
            ref={(el) => {
              this.img = el;
            }}
          />
        </div>
      </div>
    );
  }
}
