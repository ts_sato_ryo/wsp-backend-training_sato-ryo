// @flow

import * as React from 'react';

import BaseWSPError from '../errors/BaseWSPError';
import type ApexError from '../errors/ApexError';
import type FatalError from '../errors/FatalError';
import Spinner from './Spinner';
import ConfirmDialog, {
  bindHandlerToConfirmDialog,
  type CustomConfirmDialogComponent,
} from './dialogs/ConfirmDialog';
import ErrorPage from './ErrorPage';
import ErrorDialog from './dialogs/ErrorDialog';
import UnexpectedErrorPage from './errors/UnexpectedErrorPage';

export type Props = {
  children: React.Node,
  handleCloseErrorDialog: (event: SyntheticEvent<any>) => void,
  loading: boolean,
  error: ?Class<BaseWSPError>,
  unexpectedError?: FatalError | ApexError,
  confirmDialog: ?{
    message?: string,
    Component?: CustomConfirmDialogComponent<*>,
    params?: *,
    okButtonLabel?: ?string,
    cancelButtonLabel?: ?string,
    handleClickOkButton: () => void,
    handleClickCancelButton: () => void,
  },
};

/**
 * 画面全体をラップして、イベントをハンドリングするコンポーネント
 * - ローディング表示：state.commons.app.loading
 * - エラー表示：state.commons.app.error
 *     - error.isContinuable === true -> エラーダイアログ表示
 *     - error.isContinuable === false -> エラーページ表示
 * TODO: Globalという名称では役割が不明瞭なので、改善案があればリネームする
 */
export default class Global extends React.Component<Props> {
  static defaultProps = {
    error: null,
  };

  renderContent() {
    const { error, children } = this.props;
    return error && !error.isContinuable ? (
      <ErrorPage error={error} />
    ) : (
      children
    );
  }

  renderConfirmDialog() {
    const { confirmDialog } = this.props;

    if (confirmDialog === undefined || confirmDialog === null) {
      return null;
    }

    if (confirmDialog.Component) {
      const CustomConfirmDialog = confirmDialog.Component;

      const CommonConfirmBoundHandler = bindHandlerToConfirmDialog({
        onClickOk: confirmDialog.handleClickOkButton,
        onClickCancel: confirmDialog.handleClickCancelButton,
      });

      return (
        <CustomConfirmDialog
          ConfirmDialog={CommonConfirmBoundHandler}
          params={confirmDialog.params}
        />
      );
    }

    return (
      <ConfirmDialog
        okButtonLabel={confirmDialog.okButtonLabel}
        cancelButtonLabel={confirmDialog.cancelButtonLabel}
        onClickOk={confirmDialog.handleClickOkButton}
        onClickCancel={confirmDialog.handleClickCancelButton}
      >
        {confirmDialog.message}
      </ConfirmDialog>
    );
  }

  renderErrorDialog() {
    const { error, handleCloseErrorDialog } = this.props;
    return error && error.isContinuable ? (
      <ErrorDialog error={error} handleClose={handleCloseErrorDialog} />
    ) : null;
  }

  renderUnexpectedErrorPage() {
    const { unexpectedError } = this.props;
    return unexpectedError ? (
      <UnexpectedErrorPage error={unexpectedError} />
    ) : null;
  }

  render() {
    return (
      <>
        <div className="ts-container">
          {this.props.unexpectedError
            ? this.renderUnexpectedErrorPage()
            : this.renderContent()}
          {this.renderConfirmDialog()}
          {this.renderErrorDialog()}
        </div>
        <Spinner loading={this.props.loading} />
      </>
    );
  }
}
