/* @flow */
import React from 'react';

export type Props = {
  [string]: any,
  onSubmit: ?(SyntheticEvent<HTMLFormElement>) => void,
  children: ?React$Node,
};

export default class Form extends React.Component<Props> {
  onSubmit: (SyntheticEvent<HTMLFormElement>) => void;

  constructor(props: Props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event: SyntheticEvent<HTMLFormElement>) {
    event.preventDefault();

    if (this.props.onSubmit !== null && this.props.onSubmit !== undefined) {
      this.props.onSubmit(event);
    }
  }

  render() {
    const restProps = { ...this.props };
    delete restProps.onSubmit;

    return (
      <form onSubmit={this.onSubmit} {...restProps}>
        {this.props.children}
      </form>
    );
  }
}
