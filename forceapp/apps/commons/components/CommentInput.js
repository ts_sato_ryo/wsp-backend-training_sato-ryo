import PropTypes from 'prop-types';
import React from 'react';
import Textarea from 'react-textarea-autosize';
import './CommentInput.scss';

export default class CommentInput extends React.Component {
  static get propTypes() {
    return {
      comment: PropTypes.string,
      photoUrl: PropTypes.string,
      onCommentChanged: PropTypes.func,
    };
  }

  onCommentChanged(event) {
    const comment = event.target.value;

    if (this.props.onCommentChanged) {
      this.props.onCommentChanged(comment);
    }
  }

  commentInputRef() {
    return 'comment';
  }

  render() {
    return (
      <div className="ts-comment-input slds">
        <div className="slds-grid">
          <div className="slds-col slds-size--12-of-12">
            <img
              className="ts-comment-input__icon-photo"
              src={this.props.photoUrl}
            />
            <div className="ts-comment-input__balloon">
              <div className="ts-comment-input__balloon-box">
                <div className="ts-comment-input__balloon-textarea">
                  <Textarea
                    value={this.props.comment}
                    ref={(ref) => { this.commentInputRef = ref; }}
                    onChange={(event) => this.onCommentChanged(event)}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
