import React from 'react';
import PropTypes from 'prop-types';

import BaseWSPError from '../errors/BaseWSPError';
import './ErrorPage.scss';

import iconErrorLarge from '../images/iconErrorLarge.png';
import iconAuthority from '../images/icon_Authority.png';

import msg from '../languages';

const ROOT = 'commons-error-page';

export default class ErrorPage extends React.Component {
  static propTypes = {
    error: PropTypes.instanceOf(BaseWSPError).isRequired,
  };

  render() {
    const { error } = this.props;

    const solution = error.isFunctionCantUseError
      ? msg().Exp_Msg_Inquire
      : error.solution;

    const solutionArea = solution ? (
      <div className={`${ROOT}__solution`}>
        <p>{solution}</p>
      </div>
    ) : null;

    const icon = error.isFunctionCantUseError ? iconAuthority : iconErrorLarge;

    return (
      <div className={ROOT}>
        <div className={`${ROOT}__problem`}>
          <div className={`${ROOT}__icon`}>
            <img src={icon} alt="" />
          </div>
          <p>{error.problem}</p>
        </div>
        {solutionArea}
      </div>
    );
  }
}
