import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './HeaderRow.scss';

const ROOT = 'commons-fixed-header-table-header-row';

/**
 * 共通コンポーネント - ヘッダー固定テーブル - ヘッダー行
 * 必ずFixedHeaderTableとして使うこと
 */
export default class HeaderRow extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
      .isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
    children: '',
  };

  render() {
    const { className, children, ...props } = this.props;

    const cssClass = classNames(ROOT, className);

    return (
      <div className={`${cssClass}`} role="row" {...props}>
        {children}
      </div>
    );
  }
}

HeaderRow.role = 'HeaderRow';
