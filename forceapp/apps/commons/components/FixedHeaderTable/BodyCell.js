import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './BodyCell.scss';

const ROOT = 'commons-fixed-header-table-body-cell';

/**
 * 共通コンポーネント - ヘッダー固定テーブル - ボディセル
 * 必ずFixedHeaderTableとして使うこと
 */
export default class BodyCell extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
  };

  static defaultProps = {
    children: null,
    className: '',
  };

  render() {
    const { className, children, ...props } = this.props;

    const cssClass = classNames(ROOT, className);

    return (
      <div className={`${cssClass}`} role="grid" {...props}>
        {children}
      </div>
    );
  }
}
