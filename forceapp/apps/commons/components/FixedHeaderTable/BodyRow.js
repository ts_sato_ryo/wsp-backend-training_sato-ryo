import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './BodyRow.scss';

const ROOT = 'commons-fixed-header-table-body-row';

/**
 * 共通コンポーネント - ヘッダー固定テーブル - ボディ行
 * 必ずFixedHeaderTableとして使うこと
 */
export default class BodyRow extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
      .isRequired,
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  render() {
    const { className, children, ...props } = this.props;

    const cssClass = classNames(ROOT, className);

    return (
      <div className={`${cssClass}`} role="columnheader" {...props}>
        {children}
      </div>
    );
  }
}

BodyRow.role = 'BodyRow';
