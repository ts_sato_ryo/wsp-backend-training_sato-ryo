// @flow

import * as React from 'react';
import {
  IconSettings,
  Dropdown as SFDropdown,
  DropdownTrigger,
  Button,
} from '@salesforce/design-system-react';
import isEqual from 'lodash/isEqual';

import './Dropdown.scss';

const ROOT = 'commons-fields-dropdown';

/* eslint-disable react/no-unused-prop-types */
// NOTE eslint の flowtype プラグインが正しく未使用propsを追えないので、
// disableにしている

export type Option<T> = {|
  // dom attribute
  id?: string,
  className?: string,

  // option type
  type: 'header' | 'item' | 'divider' | 'html',

  // content
  label?: string,
  value?: T,

  // divider
  divider?: 'top' | 'bottom',

  // icon
  leftIcon?: {
    name: string,
    category: string,
  },
  rightIcon?: {
    name: string,
    category: string,
  },

  // misc
  href?: string,
|};

export type Props<T> = {|
  label?: string,
  value?: T,
  disabled?: boolean,
  className?: string,
  options: Option<T>[],

  onSelect?: (option: Option<T>) => void,
|};

/* eslint-enable react/no-unused-prop-types */

const withStyle = <T>(WrappedComponent: React.ComponentType<Props<T>>) => (
  props: Props<T>
): React.Element<*> => (
  <WrappedComponent {...props} className={`${ROOT} ${props.className || ''}`} />
);

const withState = <T>(
  WrappedComponent: React.ComponentType<Props<T>>
): React.ComponentType<Props<T>> => {
  type PropsT = Props<T>;
  type State = {| label?: string, value?: T |};

  type $UnwrapVoid = <Fn>({ f: void | Fn }) => Fn;

  return class DropdownContainer extends React.Component<PropsT, State> {
    onSelect: $Call<$UnwrapVoid, { f: $PropertyType<PropsT, 'onSelect'> }>;
    findSelectedOption: ({
      options: Option<T>[],
      label?: string,
      value?: T,
    }) => Option<T>;

    constructor(props: PropsT) {
      super();

      this.onSelect = this.onSelect.bind(this);
      this.findSelectedOption = this.findSelectedOption.bind(this);

      const option = this.findSelectedOption(props);
      this.state = {
        label: option.label,
        value: option.value,
      };
    }

    componentWillReceiveProps(nextProps: PropsT) {
      if (nextProps.label !== this.props.label) {
        this.setState({
          label: nextProps.label,
        });
      }

      const isValueChanged = nextProps.value !== this.props.value;
      const isOptionsChanged =
        nextProps.options !== this.props.options &&
        !isEqual(nextProps.options, this.props.value);
      if (isValueChanged || isOptionsChanged) {
        const option = this.findSelectedOption(nextProps);
        this.setState({
          label: option.label,
          value: option.value,
        });
      }
    }

    onSelect(option: Option<T>): void {
      this.setState({
        label: this.props.label === '' ? '' : option.label,
        value: option.value,
      });

      if (this.props.onSelect) {
        this.props.onSelect(option);
      }
    }

    findSelectedOption({ options, label, value }: *) {
      const os = options || [];
      return (
        os.find((o) => o.value === value) ||
        ({ type: 'item', label }: Option<T>)
      );
    }

    render() {
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          onSelect={this.onSelect}
        />
      );
    }
  };
};

export const DropdownPresentation = <T>({
  label,
  value,
  onSelect,
  options,
  className,
  disabled,
}: Props<T>): React.Element<'div'> => (
  <div className={className}>
    <IconSettings iconPath={window.__icon_path__}>
      <SFDropdown
        align="left"
        options={options}
        value={value}
        onSelect={onSelect}
        disabled={disabled}
        className={className}
        menuPosition="overflowBoundaryElement"
      >
        <DropdownTrigger>
          <Button
            iconCategory="utility"
            iconName="down"
            iconPosition="right"
            label={label}
            disabled={disabled}
            className={label ? '' : 'empty-content'}
          />
        </DropdownTrigger>
      </SFDropdown>
    </IconSettings>
  </div>
);

const compose = <T>(...fns: Function[]) => (
  name: string,
  presentation: React.ComponentType<Props<T>>
) => {
  const container = fns.reduceRight(
    (prevFn: Function, nextFn: Function) => (
      arg: React.ComponentType<Props<T>>
    ) => nextFn(prevFn(arg)),
    (value) => value
  );
  const component = container(presentation);
  component.displayName = name;

  return component;
};

export default (compose(
  withStyle,
  withState
)('Dropdown', DropdownPresentation): React.ComponentType<Object>);
