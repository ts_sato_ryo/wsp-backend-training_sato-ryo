// @flow
import React from 'react';
import {
  SLDSMenuDropdown,
  Input as SLDSInput,
} from '@salesforce/design-system-react';
import KEYS from '@salesforce/design-system-react/utilities/key-code';
import { MENU_DROPDOWN_TRIGGER } from '@salesforce/design-system-react/utilities/constants';
import TimeUtil from '../../utils/TimeUtil';

type MenuDropdownTriggerPropsType = {
  id?: string,
  className?: string,
  label?: string,
  menu?: React$Node,
  isOpen?: boolean,
  onBlur?: Function,
  onClick?: Function,
  onFocus?: Function,
  onKeyDown?: Function,
  onMouseDown?: Function,
  triggerRef?: Function,
  value?: string,
};

class MenuDropdownTrigger extends React.Component<MenuDropdownTriggerPropsType> {
  handleKeyDown: (Event) => void;
  handleBlur: (Event) => void;

  static displayName = MENU_DROPDOWN_TRIGGER;

  constructor(props) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleKeyDown(event) {
    if (this.props.onKeyDown && event.keyCode) {
      if (
        event.keyCode === KEYS.ENTER ||
        event.keyCode === KEYS.DOWN ||
        event.keyCode === KEYS.UP ||
        event.keyCode === KEYS.ESCAPE ||
        event.keyCode === KEYS.TAB
      ) {
        this.props.onKeyDown(event);
      }
    }
  }

  handleBlur(event) {
    // メニュー内を移動している時に常に onBlur が発生してしますので、
    // メニューが閉じたままの時の onBlur の時だけ入力を中止した際の動作を実行します。
    if (this.props.onBlur && !this.props.isOpen) {
      this.props.onBlur(event);
    }
  }

  render() {
    const {
      className,
      menu,
      onFocus,
      onKeyDown, // eslint-disable-line no-unused-vars
      onBlur, // eslint-disable-line no-unused-vars
      onMouseDown,
      triggerRef,
      ...props
    } = this.props;

    return (
      /* eslint-disable jsx-a11y/no-static-element-interactions */
      <div
        className={className}
        onBlur={this.handleBlur}
        onFocus={onFocus}
        onKeyDown={this.handleKeyDown}
        onMouseDown={onMouseDown}
      >
        {/* eslint-enable jsx-a11y/no-static-element-interactions */}
        <SLDSInput {...props} inputRef={triggerRef}>
          {menu}
        </SLDSInput>
      </div>
    );
  }
}

type Props = {
  id?: string,
  className?: string,
  value: string,
  onTimeChange: (string) => void,
  placeholder?: string,
  minMinutes?: string,
  maxMinutes?: string,
  stepInMinutes?: number,
  required?: boolean,
  disabled?: boolean,
};

type State = {|
  isOpen: boolean,
  strValue: string,
|};

export default class TimePicker extends React.Component<Props, State> {
  callOnTimeChange: (string, ?string) => void;
  handleInputBlur: () => void;
  handleInputChange: (SyntheticEvent<HTMLInputElement>) => void;
  handleSLDSMenuDropdownSelect: (?{ label: string, value: string }) => void;
  handleSLDSMenuDropdownClose: () => void;

  static defaultProps = {
    placeholder: '',
    minMinutes: '00:00',
    maxMinutes: '23:59',
    stepInMinutes: 30,
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      isOpen: false,
      strValue: this.props.value,
    };

    this.callOnTimeChange = this.callOnTimeChange.bind(this);
    this.handleInputBlur = this.handleInputBlur.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSLDSMenuDropdownSelect = this.handleSLDSMenuDropdownSelect.bind(
      this
    );
    this.handleSLDSMenuDropdownClose = this.handleSLDSMenuDropdownClose.bind(
      this
    );
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(props: Props) {
    this.setState({
      strValue: props.value,
    });
  }

  isStrTime(val: string) {
    if (/^\d{2}:[0-5]\d$/.test(val)) {
      const time = TimeUtil.toMinutes(val) || 0;
      const min = TimeUtil.toMinutes(this.props.minMinutes) || 0;
      const max = TimeUtil.toMinutes(this.props.maxMinutes) || 0;
      return min <= time && time <= max;
    }
    return false;
  }

  getStrValue(val: string, defaultValue: string = '') {
    if (val !== '') {
      const formattedValue = TimeUtil.supportFormat(val);
      if (this.isStrTime(formattedValue)) {
        return formattedValue;
      }
    }
    return defaultValue;
  }

  getOptions(): Array<{
    label: string,
    value: string,
  }> {
    const step = this.props.stepInMinutes;
    const min = TimeUtil.toMinutes(this.props.minMinutes) || 0;
    const max = TimeUtil.toMinutes(this.props.maxMinutes) || 0;

    if (min > max || Number(step) === 0) {
      return [];
    }

    const res = [];

    for (let t = min; t <= max; t += step) {
      const time = TimeUtil.toHHmm(t);
      res.push({
        label: time,
        value: time,
      });
    }
    return res;
  }

  callOnTimeChange(value: string, defaultValue: string = '') {
    const time = this.getStrValue(value, defaultValue);
    this.props.onTimeChange(time);
  }

  setStrValue(strValue: string) {
    this.setState({
      strValue,
    });

    if (this.isStrTime(strValue)) {
      this.callOnTimeChange(strValue);
    }
  }

  handleInputBlur() {
    if (this.props.value !== this.state.strValue) {
      this.callOnTimeChange(this.state.strValue, this.props.value);
    }
  }

  handleInputChange(event: SyntheticEvent<HTMLInputElement>) {
    const strValue = event.currentTarget.value;
    this.setStrValue(strValue);
  }

  handleSLDSMenuDropdownSelect(val: { label: string, value: string }) {
    if (val && val.value) {
      this.setStrValue(val.value);
    }
  }

  handleSLDSMenuDropdownClose() {
    this.handleInputBlur();
  }

  render() {
    return (
      <SLDSMenuDropdown
        id={this.props.id}
        checkmark={false}
        disabled={this.props.disabled}
        menuStyle={{
          maxHeight: '20em',
          overflowX: 'hidden',
          minWidth: '100%',
        }}
        menuPosition="absolute"
        onSelect={this.handleSLDSMenuDropdownSelect}
        onClose={this.handleSLDSMenuDropdownClose}
        options={this.getOptions()}
      >
        <MenuDropdownTrigger
          className={this.props.className}
          onBlur={this.handleInputBlur}
          onChange={this.handleInputChange}
          placeholder={this.props.placeholder}
          required={this.props.required}
          type="text"
          value={this.state.strValue}
        />
      </SLDSMenuDropdown>
    );
  }
}
