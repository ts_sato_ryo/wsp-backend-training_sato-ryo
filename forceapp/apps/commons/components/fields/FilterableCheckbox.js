// @flow
import React from 'react';
import _ from 'lodash';

import Icon from '../../../mobile-app/components/atoms/Icon';
import './FilterableCheckbox.scss';

const ROOT = 'commons-fields-filterable-checkbox';

type Props = {
  data: any,
  values?: any,
  onSelectInput: (value: string) => void,
  hasDisplayValue?: boolean,
  searchPlaceholder: string,
  selectedStringValues?: ?any,
  maxLength?: number,
};

type State = {
  searchString: string,
};

// Helper presentational components
const SmallIcon = (props: *) => <Icon {...props} size="small" />;
const Checkbox = ({ name, value, label, onChange, isChecked, isHidden }) => {
  return (
    <label className={`${ROOT}__input-label${isHidden}`} htmlFor={name}>
      <input
        type="checkbox"
        id={name}
        className={`${ROOT}__input-checkbox`}
        onChange={onChange}
        value={value}
        checked={isChecked}
      />
      {label}
    </label>
  );
};

class FilterableCheckbox extends React.Component<Props, State> {
  state = {
    searchString: '',
  };

  onChangeInput = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const { value, checked } = e.target;
    const values = checked
      ? [...this.props.data, value]
      : this.props.data.filter((i) => i !== value);

    const labels = values.map((v) => {
      const data = _.find(this.props.data, { value: v });
      return data ? data.label : null;
    });

    const newState: any = { values };
    if (this.props.hasDisplayValue) {
      newState.displayValue = labels.join(', ');
    }

    this.props.onSelectInput(e.target.value);
  };

  onSearch = (e: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({
      searchString: e.target.value,
    });
  };

  clearSearch = () => {
    this.setState({
      searchString: '',
    });
  };

  filterData = (data: any) => {
    return data.filter((i) =>
      i.label.toLowerCase().includes(this.state.searchString.toLowerCase())
    );
  };

  renderSearchButton = () => {
    if (this.state.searchString) {
      return (
        <button onClick={this.clearSearch} className={`${ROOT}__clear-search`}>
          <SmallIcon type="clear" className={`${ROOT}__search-icon`} />
        </button>
      );
    } else {
      return <SmallIcon type="search" className={`${ROOT}__search-icon`} />;
    }
  };

  render() {
    const { searchString } = this.state;
    const {
      data,
      values,
      searchPlaceholder,
      selectedStringValues,
    } = this.props;

    const filteredData = searchString ? this.filterData(data) : data;

    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__search-container`}>
          <input
            type="search"
            className={`${ROOT}__search-field`}
            onChange={this.onSearch}
            placeholder={searchPlaceholder}
            value={searchString}
          />
          {this.renderSearchButton()}
        </div>

        {filteredData.map((i, index) => {
          let isHidden = '';
          if (index >= this.props.maxLength) {
            isHidden = '__is-hidden';
          }

          const type = typeof selectedStringValues;
          let isChecked;
          if (selectedStringValues || selectedStringValues === '') {
            if (type === 'object') {
              isChecked = selectedStringValues.includes(i.value);
            } else {
              isChecked = selectedStringValues === i.value;
            }
          } else {
            isChecked = values ? values.includes(i.value) : false;
          }

          return (
            <Checkbox
              key={index}
              value={i.value}
              name={i.name}
              label={i.label}
              onChange={this.onChangeInput}
              isChecked={isChecked}
              isHidden={isHidden}
            />
          );
        })}
      </div>
    );
  }
}

export default FilterableCheckbox;
