import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import './CommentFieldIconOutside.scss';

/**
 * テキスト項目 - 共通コンポーネント
 */
export default class CommentField extends React.Component {
  static get propTypes() {
    return {
      icon: PropTypes.string.isRequired,
      className: PropTypes.string,
      value: PropTypes.string,
    };
  }

  renderIcon() {
    // アイコン自体は特に意味はないのでaltは空に
    return <img className="ts-comment-field-icon-outside__icon" src={this.props.icon} alt="" aria-hidden="true" />;
  }

  render() {
    const fieldClass = classNames(
      'ts-comment-field-icon-outside',
      this.props.className
    );

    return (
      <div className={fieldClass}>
        <div className="ts-comment-field-icon-outside__bubble">
          {this.renderIcon()}
          <textarea className="ts-comment-field-icon-outside__textarea slds-textarea">
            {this.props.value}
          </textarea>
        </div>
      </div>
    );
  }
}
