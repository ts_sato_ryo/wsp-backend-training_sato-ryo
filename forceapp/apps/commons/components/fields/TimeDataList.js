import PropTypes from 'prop-types';
import React from 'react';

/**
 * データリスト - 共通コンポーネント
 * 時刻項目などで利用される時刻の選択肢を生成するコンポーネント
 * FIXME デモ向け仮実装
 */
export default class TimeDataList extends React.Component {
  static get propTypes() {
    return {
      id: PropTypes.string.isRequired,
    };
  }

  static get defaultProps() {
    return {

    };
  }

  /**
   * 0埋め
   */
  zeroPadding(num) {
    return `0${num}`.substr(-2);
  }

  renderOptions() {
    const times = [];
    for (let hourI = 0; hourI < 24; hourI++) {
      for (let minutesI = 0; minutesI < 4; minutesI++) {
        const hour = this.zeroPadding(hourI);
        const minutes = this.zeroPadding(minutesI * 15);
        const time = `${hour}:${minutes}`;
        times.push(<option value={time} key={`${hourI}${minutesI}`}>{time}</option>);
      }
    }

    return times;
  }

  render() {
    return (
      <datalist id={this.props.id}>
        {this.renderOptions()}
      </datalist>
    );
  }
}
