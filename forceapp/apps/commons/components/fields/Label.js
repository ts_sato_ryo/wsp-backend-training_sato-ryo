import PropTypes from 'prop-types';
import React from 'react';

import HorizontalLayout from './layouts/HorizontalLayout';
import ObjectUtil from '../../utils/ObjectUtil';

/**
 * Label - 共通コンポーネント
 */
export default class Label extends React.Component {
  static get propTypes() {
    return {
      text: PropTypes.string.isRequired,
      children: PropTypes.node,
      className: PropTypes.string,
      // Lightning Design SystemのGridを参照
      // https://www.lightningdesignsystem.com/components/utilities/grid/
      helpMsg: PropTypes.string,
      labelCols: PropTypes.number,
      childCols: PropTypes.number,
    };
  }

  static get defaultProps() {
    return {
      labelCols: 3,
      childCols: 9,
    };
  }

  render() {
    const childProps = ObjectUtil.getOrDefault(this.props.children, 'props', {});

    return (
      <HorizontalLayout className={this.props.className}>
        <HorizontalLayout.Label cols={this.props.labelCols} required={childProps.required} helpMsg={this.props.helpMsg}>
          <label htmlFor={childProps.id} title={this.props.text}>
            {this.props.text}
          </label>
        </HorizontalLayout.Label>
        <HorizontalLayout.Body cols={this.props.childCols}>
          {this.props.children}
        </HorizontalLayout.Body>
      </HorizontalLayout>
    );
  }
}
