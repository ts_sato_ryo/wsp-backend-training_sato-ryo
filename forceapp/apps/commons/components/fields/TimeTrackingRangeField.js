import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import './TimeTrackingRangeField.scss';

const STEP_MINUTES = 5;
const ROOT = 'time-tracking-range-filed';

/**
 * 工数入力項目 - 共通コンポーネント
 * FIXME デモ向け仮実装
 */
export default class TimeTrackingRangeField extends React.Component {
  static get propTypes() {
    return {
      // 工数表示用
      barColor: PropTypes.string.isRequired,
      // 連携済み工数表示用
      linkBarColor: PropTypes.string.isRequired,
      onChange: PropTypes.func.isRequired,
      ratio: PropTypes.number,
      // ゲージの動かす最大値を決める
      // FIXME 単純にグラフの横幅pixelを指定するようになってしまっている
      // この値は、グラフの見た目ではなく、入力する項目の特性から決めるべき
      maxRatio: PropTypes.number,
      // つまみをバーの位置を同期しない場合に設定する
      barRatio: PropTypes.number,
      linkRatio: PropTypes.number,
      className: PropTypes.string,
      readOnly: PropTypes.bool,
      width: PropTypes.number,
      isDirectInputMode: PropTypes.bool,
      ratioLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    };
  }

  static get defaultProps() {
    return {
      ratio: 0,
      barRatio: null,
      linkRatio: 0,
      // 作業
      maxRatio: 600,
      readOnly: false,
      width: 300,
      isDirectInputMode: true,
      ratioLabel: '-',
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      ratio: this.props.ratio,
    };

    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onChange = this.onChange.bind(this);
    this.convertPixelToRatio = this.convertPixelToRatio.bind(this);
    this.convertRatioToPixel = this.convertRatioToPixel.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ratio: nextProps.ratio,
    });
  }

  onMouseDown() {
    window.document.addEventListener('mousemove', this.onMouseMove);
    window.document.addEventListener('mouseup', this.onMouseUp);
  }

  onMouseMove(e) {
    // 座業計算
    const elX = this.el.getBoundingClientRect().left;
    const mouseX = e.pageX - document.body.scrollLeft;
    const width = mouseX - elX;

    const ratio = this.convertPixelToRatio(width);
    let steppedRatio = this.calcSteppedRatio(ratio);

    if (steppedRatio >= this.props.maxRatio) {
      steppedRatio = this.props.maxRatio;
    }

    if (steppedRatio <= 0) {
      steppedRatio = 0;
    }

    this.setState({
      ratio: steppedRatio,
    });

    this.onChange(this.state.ratio);
  }

  onMouseUp() {
    window.document.removeEventListener('mousemove', this.onMouseMove);
    window.document.removeEventListener('mouseup', this.onMouseUp);
  }

  onDragStartPickIcon(e) {
    e.preventDefault();
  }

  onChange() {
    this.props.onChange(this.state.ratio);
  }

  isPickEdge(leftPickStyle) {
    return leftPickStyle < 10;
  }

  /**
   * Step調整されたRatioを算出する
   * @param {number} tmpWidth
   */
  calcSteppedRatio(ratio) {
    return Math.floor(ratio / STEP_MINUTES) * STEP_MINUTES;
  }

  convertRatioToPixel(ratio) {
    const pixel = ratio * (this.props.width / this.props.maxRatio);
    return pixel <= this.props.width ? pixel : this.props.width;
  }

  convertPixelToRatio(pixel) {
    return Math.floor(this.props.maxRatio * (pixel / this.props.width));
  }

  renderPickIcon(leftPickStyle) {
    const pickIconStyle = {
      left: leftPickStyle,
    };

    const pickIconCssClass = classNames(
      'time-tracking-range-filed__pick-icon',
      {
        'time-tracking-range-filed__pick-icon--is-read-only': this.props
          .readOnly,
        'time-tracking-range-filed__pick-icon--is-direct-input': this.props
          .isDirectInputMode,
        'time-tracking-range-filed__pick-icon--in-edge': this.isPickEdge(
          leftPickStyle
        ),
      }
    );
    const mouseDownHandle = this.props.readOnly ? null : this.onMouseDown;

    if (this.props.isDirectInputMode) {
      return (
        <span
          className={pickIconCssClass}
          onMouseDown={mouseDownHandle}
          style={pickIconStyle}
        />
      );
    }

    return (
      <span
        className={pickIconCssClass}
        onMouseDown={mouseDownHandle}
        style={pickIconStyle}
      >
        <span className={`${ROOT}__pick-icon-value`}>
          {this.props.ratioLabel}
        </span>
      </span>
    );
  }

  render() {
    const barRatio = this.props.barRatio
      ? this.props.barRatio
      : this.state.ratio;
    const barStyle = {
      backgroundColor: this.props.barColor,
      width: this.convertRatioToPixel(barRatio),
    };

    const linkBarStyle = {
      backgroundColor: this.props.linkBarColor,
      width: this.convertRatioToPixel(this.props.linkRatio),
    };

    const cssClassObj = [this.props.className, ROOT];

    const style = {
      width: this.props.width,
    };

    const leftPickStyle = this.convertRatioToPixel(this.state.ratio);
    cssClassObj.push({
      [`${ROOT}--pick-in-edge`]:
        this.isPickEdge(leftPickStyle) && this.props.isDirectInputMode,
      [`${ROOT}--is-read-only`]: this.props.readOnly,
    });

    return (
      <div
        className={classNames(...cssClassObj)}
        style={style}
        ref={(el) => {
          this.el = el;
        }}
      >
        <div className={`${ROOT}__bar-wrapper`}>
          <div className="time-tracking-range-filed__bar" style={barStyle} />
          <div
            className="time-tracking-range-filed__link-bar"
            style={linkBarStyle}
          />
        </div>
        {this.renderPickIcon(leftPickStyle)}
      </div>
    );
  }
}
