import PropTypes from 'prop-types';
import React from 'react';

import classNames from 'classnames';

import './SelectField.scss';

/**
 * プルダウン項目 - 共通コンポーネント
 * FIXME デモ向け仮実装
 *
 * プルダウンの選択肢は以下の構造の Array<Object> を props.options に指定する
 *
 * ```
 * [
 *   { text: '選択肢1', value: 100 },
 *   { text: '選択肢2', value: 200 },
 *   ...
 * ]
 * ```
 */
export default class SelectField extends React.Component {
  static get propTypes() {
    return {
      id: PropTypes.string,
      options: PropTypes.array,
      disabled: PropTypes.bool,
      className: PropTypes.string,
      onChange: PropTypes.func,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string),
      ]),
      multiple: PropTypes.bool,
      size: PropTypes.number,
      required: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      id: '',
      options: [],
      disabled: false,
      value: '',
      multiple: false,
      size: 0,
      required: false,
    };
  }

  renderOptions(item, index) {
    return (
      <option key={index} value={item.value}>
        {item.text}
      </option>
    );
  }

  render() {
    const cssClass = classNames(
      'slds-select',
      'ts-select',
      this.props.className
    );

    return (
      <select
        onChange={this.props.onChange}
        className={cssClass}
        id={this.props.id}
        disabled={
          this.props.options.length === 0 || this.props.disabled === true
        }
        value={this.props.value}
        multiple={this.props.multiple}
        size={this.props.size}
        required={this.props.required}
      >
        {this.props.options.map(this.renderOptions)}
      </select>
    );
  }
}
