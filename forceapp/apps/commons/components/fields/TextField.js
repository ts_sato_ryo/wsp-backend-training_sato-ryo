import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import './TextField.scss';

/**
 * テキスト項目 - 共通コンポーネント
 */
export default class TextField extends React.Component {
  static get propTypes() {
    return {
      className: PropTypes.string,
      type: PropTypes.string,
      onFocus: PropTypes.func,
      onBlur: PropTypes.func,
      onKeyDown: PropTypes.func,
      onChange: PropTypes.func,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      disabled: PropTypes.bool,
      readOnly: PropTypes.bool,
      placeholder: PropTypes.string,
      isRequired: PropTypes.bool,
      label: PropTypes.string,
    };
  }

  static get defaultProps() {
    return {
      type: 'text',
      disabled: false,
      readOnly: false,
      isRequired: false,
      placeholder: null,
    };
  }

  constructor(props) {
    super(props);

    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onFocus(e) {
    if (this.props.onFocus) {
      this.props.onFocus(e, e.target.value);
    }
  }

  onBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e, e.target.value);
    }
  }

  onKeyDown(e) {
    if (this.props.onKeyDown) {
      this.props.onKeyDown(e, e.target.value);
    }
  }

  onChange(e) {
    if (this.props.onChange) {
      this.props.onChange(e, e.target.value);
    }
  }

  render() {
    const {
      className,
      value,
      disabled,
      readOnly,
      placeholder,
      label,
      isRequired,
      ...props
    } = this.props;

    const textFieldClass = classNames('ts-text-field', 'slds-input', className);

    // 重複回避
    delete props.onFocus;
    delete props.onBlur;
    delete props.onKeyDown;
    delete props.onChange;

    if (readOnly) {
      return (
        <div className="ts-text-field ts-text-field--readonly" title={value}>
          {value}
        </div>
      );
    } else if (label) {
      const isRequiredString = isRequired ? (
        <span className="is-required">*</span>
      ) : (
        ''
      );
      return (
        <div className="ts-text-field-container">
          <p className="key">
            {isRequiredString}
            &nbsp;{label}
          </p>
          <input
            className={textFieldClass}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            onKeyDown={this.onKeyDown}
            onChange={this.onChange}
            value={value}
            disabled={disabled}
            readOnly={readOnly}
            placeholder={placeholder}
            {...props}
          />
        </div>
      );
    } else {
      return (
        <input
          className={textFieldClass}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onKeyDown={this.onKeyDown}
          onChange={this.onChange}
          value={value}
          disabled={disabled}
          readOnly={readOnly}
          placeholder={placeholder}
          {...props}
        />
      );
    }
  }
}
