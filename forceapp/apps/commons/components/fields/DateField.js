import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import classNames from 'classnames';

import './DateField.scss';

const ROOT = 'commons-fields-date-field';

/**
 * 日付項目
 * NOTE: 現在内部的に使用している react-datepicker は moment オブジェクトでやり取りするが、
 * このコンポーネントの I/F は YYYY-MM-DD 形式の文字列になっている。
 * （props に渡す値も、onChange で返される値も YYYY-MM-DD）
 * <input type="date"> と揃えたというのと、
 * react-lightning-design-system への置き換えも検討しているのが理由。
 */
export default class DateField extends React.Component {
  static get propTypes() {
    return {
      value: PropTypes.string, // YYYY-MM-DD
      selected: PropTypes.string, // YYYY-MM-DD
      minDays: PropTypes.number,
      maxDays: PropTypes.number,
      minDate: PropTypes.string, // YYYY-MM-DD
      maxDate: PropTypes.string, // YYYY-MM-DD
      placeholder: PropTypes.string,
      disabled: PropTypes.bool,
      onChange: PropTypes.func.isRequired,
      className: PropTypes.string,
      required: PropTypes.bool,
      dateFormat: PropTypes.string,
    };
  }

  static get defaultProps() {
    return {
      value: '',
      selected: null,
      minDays: null,
      maxDays: null,
      minDate: null,
      maxDate: null,
      placeholder: '',
      disabled: false,
      className: '',
      required: false,
      dateFormat: 'L',
    };
  }

  constructor(props) {
    super(props);
    this.state = { value: this.convertToFormattedStr(props.value) };
    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onChangeRaw = this.onChangeRaw.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const formattedValue = this.convertToFormattedStr(nextProps.value);
    this.setState({ value: formattedValue });
  }

  onChange(momentObj) {
    // stateのvalueはinputにセットされるため空文字を設定
    const value = momentObj ? momentObj.format('YYYY-MM-DD') : '';

    // 渡される値はYYYY-MM-DDのString
    if (this.props.onChange) {
      this.props.onChange(value);
    }
  }

  onBlur(e) {
    const value = e.target.value;
    const momentObj = this.convertToMomentObj(value);

    // onChangeに通すのはmoment
    // convertToMomentObjの変換により値が不正の場合はnullが返っている
    this.onChange(momentObj);
  }

  // 内部inputのchangeイベント
  // value propを利用するようにしたため、自前で更新する必要がでてきた
  onChangeRaw(e) {
    this.setState({ value: e.target.value });
  }

  convertToMomentObj(dateStr) {
    return moment(dateStr, ['YYYY-MM-DD', 'YYYY/M/D', 'L'], true).isValid()
      ? moment(dateStr)
      : null;
  }

  convertToFormattedStr(dateStr) {
    const momentObj = this.convertToMomentObj(dateStr);

    if (momentObj === null) {
      return '';
    } else {
      return momentObj.format(this.props.dateFormat);
    }
  }

  render() {
    const cssClass = classNames(this.props.className, 'slds-input');

    // NOTE: showYearDropdown, dateFormatCalendar は
    // locale を日本語にしたときに "7月 2017" と表示されるのを防ぐためにセットしている
    const props = {
      className: cssClass,
      selected: this.state.value
        ? this.convertToMomentObj(this.state.value)
        : this.convertToMomentObj(this.props.selected),
      value: this.state.value,
      placeholderText: this.props.placeholder,
      disabled: this.props.disabled,
      onChange: this.onChange,
      onBlur: this.onBlur,
      onChangeRaw: this.onChangeRaw,
      showYearDropdown: true,
      dateFormatCalendar: 'MMMM',
      required: this.props.required,
    };

    // minDays, maxDaysにnullを通してしまうとキーボード操作時にエラーとなる
    // (PropTypesはObject)
    // なので項目自体を必要なときのみ生成する
    if (this.props.minDate !== null) {
      props.minDate = this.convertToMomentObj(this.props.minDate);
    } else if (this.props.minDays !== null) {
      // TODO: 範囲の基準がクライアント環境の「当日」に固定されている点に、そのような用途があるか懸念がある
      props.minDate = moment().add(this.props.minDays, 'days');
    }
    if (this.props.maxDate !== null) {
      props.maxDate = this.convertToMomentObj(this.props.maxDate);
    } else if (this.props.maxDays !== null) {
      // TODO: 範囲の基準がクライアント環境の「当日」に固定されている点に、そのような用途があるか懸念がある
      props.maxDate = moment().add(this.props.maxDays, 'days');
    }

    return (
      <div className={`${ROOT}`}>
        <DatePicker {...props} />
      </div>
    );
  }
}
