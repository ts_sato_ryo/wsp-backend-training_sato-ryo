import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import TimeField from './TimeField';

import './TimeRangeField.scss';

/**
 * 時間帯選択項目 - 共通コンポーネント
 */
export default class TimeRangeField extends React.Component {
  static get propTypes() {
    return {
      onBlurAtStart: PropTypes.func.isRequired,
      onBlurAtEnd: PropTypes.func.isRequired,
      startTime: PropTypes.string,
      endTime: PropTypes.string,
      className: PropTypes.string,
      required: PropTypes.bool,
      disabled: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      startTime: '',
      endTime: '',
      required: false,
      disabled: false,
    };
  }

  render() {
    const fieldClass = classNames('ts-time-range-field', this.props.className);

    return (
      <div className={fieldClass}>
        <span className="ts-time-range-field__input">
          <TimeField
            value={this.props.startTime}
            required={this.props.required}
            onBlur={this.props.onBlurAtStart}
            disabled={this.props.disabled}
          />
        </span>

        <span className="ts-time-range-field__separation">
          <span className="ts-time-range-field__separation-inner">–</span>
        </span>

        <span className="ts-time-range-field__input">
          <TimeField
            value={this.props.endTime}
            required={this.props.required}
            onBlur={this.props.onBlurAtEnd}
            disabled={this.props.disabled}
          />
        </span>
      </div>
    );
  }
}
