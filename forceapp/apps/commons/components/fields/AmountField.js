/* @flow */
import React from 'react';
import isNil from 'lodash/isNil';
import FormatUtil from '../../utils/FormatUtil';
import TextUtil from '../../utils/TextUtil';
import msg from '../../languages';
import TextField from './TextField';
import './AmountField.scss';

/**
 * 金額項目
 *
 * TODO:
 * - 全角数字を入力されたときの処理
 * - 有効桁数を超えたときに有効桁数を超える数字は切り捨てる処理
 */

type Props = {
  value: ?number,
  fractionDigits?: ?number,
  className?: ?string,
  disabled?: ?boolean,
  readOnly?: ?boolean,
  onBlur: (number | null) => void,
  nullable?: boolean,
  isFixedAllowance?: boolean,
};

type State = {
  focussed: boolean,
  pattern: RegExp,
  value: string,
  fractionDigits: number,
};

export default class AmountField extends React.Component<Props, State> {
  onFocus: (SyntheticInputEvent<HTMLInputElement>) => void;
  onBlur: (SyntheticInputEvent<HTMLInputElement>) => void;
  onChange: (SyntheticInputEvent<HTMLInputElement>) => void;
  convertCharacter: (number | string) => void;

  static defaultProps = {
    fractionDigits: 0,
    disabled: false,
    readOnly: false,
  };

  // TODO: props.value の小数桁が fractionDigits 以上だったら
  // 最初から切り捨てる必要がある
  state = {
    focussed: false,
    value: this.props.value === null ? '0.0' : String(this.props.value),
    pattern: new RegExp(/^([1-9１-９]{1}[0-9０-９]{0,11}|0|０)?$/),
    fractionDigits: 0,
  };

  // eslint-disable-next-line camelcase,react/sort-com
  UNSAFE_componentWillReceiveProps(nextProps: Props) {
    if (
      nextProps.value !== this.props.value ||
      String(nextProps.value) === ''
    ) {
      this.setState({
        value: String(nextProps.value),
      });
      if (this.props.isFixedAllowance && nextProps.value) {
        this.props.onBlur(nextProps.value);
      }
    }
    if (nextProps.fractionDigits !== this.state.fractionDigits) {
      this.setState({
        fractionDigits: nextProps.fractionDigits || 0,
        pattern: this.makePattern(nextProps.fractionDigits),
      });
    }
  }

  onFocus = () => {
    const { value } = this.state;
    const currentValue =
      value.length > 0 && parseFloat(value) === 0 ? '' : value;

    this.setState({
      value: currentValue,
      focussed: true,
    });
  };

  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const { value } = e.target;
    if (value.length > 0 && !value.match(this.state.pattern)) {
      return;
    }
    this.setState({
      value,
    });
  };

  onBlur = () => {
    if (this.props.value !== this.state.value) {
      let formattedValue;
      if (this.props.nullable) {
        formattedValue =
          this.state.value.length > 0
            ? this.convertCharacter(this.state.value)
            : '';
        this.setState({
          focussed: false,
          value: formattedValue,
        });
        formattedValue = formattedValue !== '' ? Number(formattedValue) : null;
      } else {
        formattedValue =
          this.state.value.length > 0
            ? this.convertCharacter(this.state.value)
            : '0';
        this.setState({
          focussed: false,
          value: formattedValue,
        });
        formattedValue = Number(formattedValue);
      }
      this.props.onBlur(formattedValue);
    }
  };

  makePattern = (fractionDigits: ?number) =>
    !isNil(fractionDigits) && fractionDigits > 0
      ? new RegExp(
          `^(([1-9|１-９]{1}[0-9|０-９]{0,11})|0|０)?(\\.|(\\.[0-9０-９]{0,${fractionDigits}}))?$`
        )
      : new RegExp(/^([1-9１-９]{1}[0-9０-９]{0,11}|0|０)?$/);

  // Shift character code if Japanese capital number（全角数字を半角に変換）
  // TODO: Consider other language (not only Japanese)
  convertCharacter = (v: string) => {
    return v.replace(/[０-９]/g, (tmpStr) =>
      String.fromCharCode(tmpStr.charCodeAt(0) - 0xfee0)
    );
  };

  countDecimals = (value: string) => {
    if (Math.floor(parseFloat(value)) === parseFloat(value)) {
      return 0;
    }
    const decimal = value.split('.')[1] || '';
    return decimal.length || 0;
  };

  render() {
    const { className, disabled, readOnly, nullable } = this.props;
    let value;
    if (nullable && this.state.value === '') {
      value = '';
    } else if (
      this.state.focussed ||
      this.countDecimals(this.state.value) > this.state.fractionDigits
    ) {
      value = this.state.value;
    } else {
      value = FormatUtil.formatNumber(
        this.state.value,
        this.props.fractionDigits
      );
    }
    return (
      <>
        <TextField
          type="tel"
          className={className}
          value={value}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          disabled={disabled}
          readOnly={readOnly}
        />
        {this.countDecimals(this.state.value) > this.state.fractionDigits && (
          <div className="amount-field-input-feedback">
            {TextUtil.nl2br(
              TextUtil.template(
                msg().Com_Err_InvalidScale,
                this.state.fractionDigits
              )
            )}
          </div>
        )}
      </>
    );
  }
}
