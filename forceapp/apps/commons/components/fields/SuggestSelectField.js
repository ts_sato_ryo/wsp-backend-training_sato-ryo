import React from 'react';
import PropTypes from 'prop-types';

import Autosuggest from 'react-autosuggest';
import classNames from 'classnames';

import TextField from './TextField';

import './SuggestSelectField.scss';

const ROOT = 'commons-fields-suggest-select-field';

/**
 * AutoSuggestionのCSS Class Nameを上書きする
 */
const suggestTheme = {
  suggestionsContainer: `${ROOT}__suggestions-container`,
  suggestionsList: `${ROOT}__suggestions-list`,
  suggestion: `${ROOT}__suggestion`,
  suggestionHighlighted: `${ROOT}__suggestionHighlighted`,
};

export default class SuggestSelectField extends React.Component {
  static get propTypes() {
    return {
      // サジェスト表示の際に、表示する値を抽出する
      getSuggestionValue: PropTypes.func.isRequired,
      // サジェストデータが取得必要なタイミングに呼ばれる
      // 1. エンターキーが押下された場合 2. 外部から直接onSuggestionsFetchRequestedが呼ばれた場合
      // 第一引数としてテキストフィールドの入力値が渡される
      onSuggestionsFetchRequested: PropTypes.func.isRequired,
      // サジェストが選択された際に呼ばれる、第一引数として、選択されたサジェストデータが渡される
      onSelected: PropTypes.func.isRequired,
      // サジェストとして表示するデータ
      suggestions: PropTypes.array,
      className: PropTypes.string,
      // 内的に使用しているTextFieldに渡されるprops
      inputProps: PropTypes.object,
    };
  }

  static get defaultProps() {
    return {
      suggestions: [],
      className: '',
      inputProps: {},
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      // 内的なTextFieldで保持するvalue、onSelectのタイミングでこのstateにデータが格納されるわけではない
      value: '',
      selectedSuggestion: null,
    };

    this.onChangeTextField = this.onChangeTextField.bind(this);
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(
      this
    );
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.renderInputComponent = this.renderInputComponent.bind(this);
    this.onKeyPressEnterTextField = this.onKeyPressEnterTextField.bind(this);
    this.onBlurTextField = this.onBlurTextField.bind(this);
    this.onSelected = this.onSelected.bind(this);
  }

  // 内部で使用するSelectFieldに設定する
  onChangeTextField(event, { newValue }) {
    this.setState({
      value: newValue,
    });
  }

  onSuggestionsClearRequested() {}

  /**
   * Enter押下時にサジェスト検索を呼び出す
   */
  onKeyPressEnterTextField(e) {
    if (e.key === 'Enter') {
      this.onSuggestionsFetchRequested();
    }
  }

  /**
   * テキストフィールドからフォーカスが外れたタイミングで、
   * 選択されたサジェストの値と入力値が異なる場合クリア
   * プルダウンの挙動をエミュレートする
   */
  onBlurTextField(event) {
    // FIXME: onSelectedでsetStateした後に, onBlurが走るが、その時点でstateが更新されていない
    // そのためスケジューリングを強制的に後ろへ移動
    setTimeout(() => {
      const suggestion = this.state.selectedSuggestion;

      if (
        !suggestion ||
        this.props.getSuggestionValue(suggestion) !== this.state.value
      ) {
        this.setState({
          value: '',
          selectedSuggestion: null,
        });
        this.props.onSelected(event, null);
      }
    }, 0);
  }

  /**
   * AutoSuggestionのonSuggestionsFetchRequestedを潰して独自に定義
   */
  onSuggestionsFetchRequested() {
    this.props.onSuggestionsFetchRequested(this.state.value);
  }

  onSelected(event, { suggestion }) {
    // サジェスト項目をクリックで選択した場合、blurがselectより先に発火して
    // valueがクリアされるため、再度設定している
    this.setState({
      value: this.props.getSuggestionValue(suggestion),
      selectedSuggestion: suggestion,
    });

    this.props.onSelected(event, suggestion);
  }

  /**
   * サジェストの各項目を描画する
   */
  renderSuggestion(suggestion) {
    return (
      <div className={`${ROOT}__suggest-item`}>
        {this.props.getSuggestionValue(suggestion)}
      </div>
    );
  }

  /**
   * テキストフィールドを描画する
   */
  renderInputComponent(inputProps) {
    return <TextField {...inputProps} />;
  }

  render() {
    // SelectFieldへ渡す必須propsを定義
    const inputPropsOverride = {
      value: this.state.value,
      onChange: this.onChangeTextField,
      onBlur: this.onBlurTextField,
      onKeyPress: this.onKeyPressEnterTextField,
    };

    const inputProps = Object.assign(this.props.inputProps, inputPropsOverride);

    const cssClass = classNames(ROOT, this.props.className);

    return (
      <div className={cssClass}>
        <Autosuggest
          theme={suggestTheme}
          suggestions={this.props.suggestions}
          // リアルタイム検索を停止するため、空の関数で上書き
          onSuggestionsFetchRequested={() => {}}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={this.props.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          inputProps={inputProps}
          onSuggestionSelected={this.onSelected}
          renderInputComponent={this.renderInputComponent}
          // TODO: 本当はTrueにすべきだが、refの問題でエラーが出る
          // https://github.com/moroshko/react-autosuggest/issues/377
          focusInputOnSuggestionClick={false}
        />
      </div>
    );
  }
}
