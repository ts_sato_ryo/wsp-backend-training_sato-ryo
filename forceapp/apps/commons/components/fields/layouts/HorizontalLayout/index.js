import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import Label from './Label';
import Body from './Body';

import './index.scss';

/**
 * Horizontal Layout
 * フォーム画面で用いられるラベル+項目、横並びのレイアウトを提供する
 */
export default class HorizontalLayout extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      className: PropTypes.string,
    };
  }

  render() {
    const layoutClass = classNames(
      'ts-horizontal-layout',
      'slds-grid',
      this.props.className
    );

    return (
      <div className={layoutClass}>
        {this.props.children}
      </div>
    );
  }
}

// contain child class
HorizontalLayout.Label = Label;
HorizontalLayout.Body = Body;
