import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

/**
 * Children Layout
 * HorizontalLayoutを必ず親とする
 */
export default class Body extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      className: PropTypes.string,
      cols: PropTypes.number,
    };
  }

  static get defaultProps() {
    return {
      cols: 9,
      children: null,
    };
  }

  render() {
    const childrenClass = classNames(
      `slds-size--${this.props.cols}-of-12`,
      'ts-horizontal-layout__body',
      this.props.className
    );

    return <div className={childrenClass}>{this.props.children}</div>;
  }
}
