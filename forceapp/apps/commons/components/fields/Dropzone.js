// @flow

import * as React from 'react';
import classNames from 'classnames';
import ReactDropzone, {
  type DropzoneFile as $DropzoneFile,
  type DropzoneProps,
} from 'react-dropzone';
import isNil from 'lodash/isNil';

import msg from '../../languages';

import './Dropzone.scss';

const ROOT = 'commons-fields-dropzone';

export type DropzoneFile = $DropzoneFile;

type Props = $Diff<
  DropzoneProps,
  {
    onDrop: $PropertyType<DropzoneProps, 'onDrop'>,
  }
> & {
  hint?: string,
  files?: DropzoneFile[],
  onClickDelete?: () => void,
};

export default class Dropzone extends React.Component<Props> {
  onDropAccepted: (files: DropzoneFile[]) => mixed;

  renderDropzone() {
    return (
      <ReactDropzone {...this.props} className={`${ROOT}__dropzone`}>
        <div className={`${ROOT}__file-label`}>
          <p>{this.props.hint || msg().Com_Lbl_DragAndDrop}</p>
          <button
            type="button"
            className={`${ROOT}__button--file ts-button`}
            disabled={this.props.disabled}
          >
            {msg().Com_Lbl_ChooseFile}
          </button>
        </div>
      </ReactDropzone>
    );
  }

  renderDroppedResult() {
    return (
      <React.Fragment>
        {isNil(this.props.children) ? (
          <React.Fragment>
            {(this.props.files || []).map((file, index) => (
              <div key={index}>
                {file.name}: {file.type}
              </div>
            ))}
          </React.Fragment>
        ) : (
          this.props.children
        )}
        <button
          type="button"
          className={`${ROOT}__button--delete ts-button`}
          onClick={this.props.onClickDelete}
        >
          {msg().Com_Lbl_DeleteFile}
        </button>
      </React.Fragment>
    );
  }

  render() {
    const className = classNames(ROOT, this.props.className);
    const isDropped = (this.props.files || []).length > 0;
    return (
      <div className={className}>
        {isDropped ? this.renderDroppedResult() : this.renderDropzone()}
      </div>
    );
  }
}
