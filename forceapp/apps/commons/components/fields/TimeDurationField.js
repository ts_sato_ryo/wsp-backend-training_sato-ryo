import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v1';
import classNames from 'classnames';

import TimeUtil from '../../utils/TimeUtil';

import './TimeDurationField.scss';

const ROOT = 'commons-fields-time-duration-field';

/**
 * 共通コンポーネント - 時間入力フィールド
 * 時間（分）による期間を扱う
 */
export default class Class extends React.Component {
  static propTypes = {
    // miniutes
    value: PropTypes.number.isRequired,
    onBlur: PropTypes.func.isRequired,
    className: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    readOnly: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    required: false,
    disabled: false,
    readOnly: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: TimeUtil.toHHmm(this.props.value),
      listId: `${ROOT}-${uuid()}`,
    };

    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      value: TimeUtil.toHHmm(nextProps.value),
    });
  }

  onChange(event) {
    this.setState({ value: event.target.value });
  }

  onBlur(e) {
    const minutes = TimeUtil.toMinutes(e.target.value);
    const validValue = TimeUtil.toHHmm(minutes);

    this.setState({
      value: validValue,
    });

    this.props.onBlur(minutes);
  }

  /**
   * Form Submit時に先に入力を確定させる
   * @param {event} e
   */
  onKeyPress(e) {
    if (e.key === 'Enter') {
      this.onBlur(e);
    }
  }

  render() {
    const cssClass = classNames('slds-input', ROOT, this.props.className);

    return (
      <span>
        <input
          type="text"
          className={cssClass}
          value={this.state.value}
          list={this.state.listId}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onKeyPress={this.onKeyPress}
          required={this.props.required}
          disabled={this.props.disabled}
          readOnly={this.props.readOnly}
        />

        <datalist id={this.state.listId}>
          <option value="00:00" />
          <option value="00:15" />
          <option value="00:30" />
          <option value="00:45" />
          <option value="01:00" />
          <option value="01:15" />
          <option value="01:30" />
          <option value="01:45" />
          <option value="02:00" />
          <option value="02:15" />
          <option value="02:30" />
          <option value="02:45" />
          <option value="03:00" />
          <option value="03:15" />
          <option value="03:30" />
          <option value="03:45" />
          <option value="04:00" />
          <option value="04:15" />
          <option value="04:30" />
          <option value="04:45" />
          <option value="05:00" />
          <option value="05:15" />
          <option value="05:30" />
          <option value="05:45" />
          <option value="06:00" />
          <option value="06:15" />
          <option value="06:30" />
          <option value="06:45" />
          <option value="07:00" />
          <option value="07:15" />
          <option value="07:30" />
          <option value="07:45" />
          <option value="08:00" />
          <option value="08:15" />
          <option value="08:30" />
          <option value="08:45" />
          <option value="09:00" />
          <option value="09:15" />
          <option value="09:30" />
          <option value="09:45" />
          <option value="10:00" />
          <option value="10:15" />
          <option value="10:30" />
          <option value="10:45" />
          <option value="11:00" />
          <option value="11:15" />
          <option value="11:30" />
          <option value="11:45" />
          <option value="12:00" />
          <option value="12:15" />
          <option value="12:30" />
          <option value="12:45" />
          <option value="13:00" />
          <option value="13:15" />
          <option value="13:30" />
          <option value="13:45" />
          <option value="14:00" />
          <option value="14:15" />
          <option value="14:30" />
          <option value="14:45" />
          <option value="15:00" />
          <option value="15:15" />
          <option value="15:30" />
          <option value="15:45" />
          <option value="16:00" />
          <option value="16:15" />
          <option value="16:30" />
          <option value="16:45" />
          <option value="17:00" />
          <option value="17:15" />
          <option value="17:30" />
          <option value="17:45" />
          <option value="18:00" />
          <option value="18:15" />
          <option value="18:30" />
          <option value="18:45" />
          <option value="19:00" />
          <option value="19:15" />
          <option value="19:30" />
          <option value="19:45" />
          <option value="20:00" />
          <option value="20:15" />
          <option value="20:30" />
          <option value="20:45" />
          <option value="21:00" />
          <option value="21:15" />
          <option value="21:30" />
          <option value="21:45" />
          <option value="22:00" />
          <option value="22:15" />
          <option value="22:30" />
          <option value="22:45" />
          <option value="23:00" />
          <option value="23:15" />
          <option value="23:30" />
          <option value="23:45" />
        </datalist>
      </span>
    );
  }
}
