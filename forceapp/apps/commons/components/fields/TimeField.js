import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import _ from 'lodash';

import StringUtil from '../../utils/StringUtil';

import './TimeField.scss';

/**
 * 共通コンポーネント - 時刻入力フィールド
 * あくまで時刻を扱うフィールドであり、25:00のような表記には対応していない
 */
export default class TimeField extends React.Component {
  static get propTypes() {
    return {
      // HH:mm形式を受ける
      value: PropTypes.string.isRequired,
      onBlur: PropTypes.func.isRequired,
      required: PropTypes.bool,
      disabled: PropTypes.bool,
      className: PropTypes.string,
    };
  }

  static get defaultProps() {
    return {
      required: false,
      disabled: false,
      className: '',
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      prevValue: this.props.value,
      value: this.props.value,
    };

    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // 空白、つまり強制的にクリアされた際にリセットする
    if (nextProps.value !== this.props.value || nextProps.value === '') {
      this.setState({
        value: nextProps.value,
        prevValue: nextProps.value,
      });
    }
  }

  onChange(event) {
    this.setState({ value: event.target.value });
  }

  // FormがSubmitされる前に変換処理を通しておく意図
  onKeyPress(e) {
    if (e.key === 'Enter') {
      this.onBlur(e);
    }
  }

  /**
   * HH:mm 形式にパースできる文字列だったら値を更新しつつ prevValue も更新
   * invalid だった場合は prevValue に戻す
   */
  onBlur(event) {
    if (_.isEmpty(event.target.value)) {
      this.setState({
        prevValue: '',
        value: '',
      });
      this.props.onBlur('');

      return;
    }

    const convertedValue = StringUtil.convertToHankaku(event.target.value);

    const newMomentObj = moment(convertedValue, 'HH:mm');
    if (newMomentObj.isValid()) {
      const value = newMomentObj.format('HH:mm');
      this.setState({
        prevValue: value,
        value,
      });
      this.props.onBlur(value);
    } else {
      this.setState((prevState) => ({ value: prevState.prevValue }));
    }
  }

  /**
   * datalistとの紐付け用のIDを発行する
   */
  generateDataListId() {
    const time = new Date().getTime();
    const rand = Math.random();
    return `ts-timefield-${time}-${rand}`;
  }

  render() {
    const dataListId = this.generateDataListId();
    const cssClass = classNames(
      'slds-input',
      'ts-time-field',
      this.props.className
    );

    return (
      <span>
        <input
          type="text"
          className={cssClass}
          value={this.state.value}
          onChange={this.onChange}
          onBlur={this.onBlur}
          list={dataListId}
          required={this.props.required}
          disabled={this.props.disabled}
          onKeyPress={this.onKeyPress}
        />

        <datalist id={dataListId}>
          <option value="00:00" />
          <option value="00:15" />
          <option value="00:30" />
          <option value="00:45" />
          <option value="01:00" />
          <option value="01:15" />
          <option value="01:30" />
          <option value="01:45" />
          <option value="02:00" />
          <option value="02:15" />
          <option value="02:30" />
          <option value="02:45" />
          <option value="03:00" />
          <option value="03:15" />
          <option value="03:30" />
          <option value="03:45" />
          <option value="04:00" />
          <option value="04:15" />
          <option value="04:30" />
          <option value="04:45" />
          <option value="05:00" />
          <option value="05:15" />
          <option value="05:30" />
          <option value="05:45" />
          <option value="06:00" />
          <option value="06:15" />
          <option value="06:30" />
          <option value="06:45" />
          <option value="07:00" />
          <option value="07:15" />
          <option value="07:30" />
          <option value="07:45" />
          <option value="08:00" />
          <option value="08:15" />
          <option value="08:30" />
          <option value="08:45" />
          <option value="09:00" />
          <option value="09:15" />
          <option value="09:30" />
          <option value="09:45" />
          <option value="10:00" />
          <option value="10:15" />
          <option value="10:30" />
          <option value="10:45" />
          <option value="11:00" />
          <option value="11:15" />
          <option value="11:30" />
          <option value="11:45" />
          <option value="12:00" />
          <option value="12:15" />
          <option value="12:30" />
          <option value="12:45" />
          <option value="13:00" />
          <option value="13:15" />
          <option value="13:30" />
          <option value="13:45" />
          <option value="14:00" />
          <option value="14:15" />
          <option value="14:30" />
          <option value="14:45" />
          <option value="15:00" />
          <option value="15:15" />
          <option value="15:30" />
          <option value="15:45" />
          <option value="16:00" />
          <option value="16:15" />
          <option value="16:30" />
          <option value="16:45" />
          <option value="17:00" />
          <option value="17:15" />
          <option value="17:30" />
          <option value="17:45" />
          <option value="18:00" />
          <option value="18:15" />
          <option value="18:30" />
          <option value="18:45" />
          <option value="19:00" />
          <option value="19:15" />
          <option value="19:30" />
          <option value="19:45" />
          <option value="20:00" />
          <option value="20:15" />
          <option value="20:30" />
          <option value="20:45" />
          <option value="21:00" />
          <option value="21:15" />
          <option value="21:30" />
          <option value="21:45" />
          <option value="22:00" />
          <option value="22:15" />
          <option value="22:30" />
          <option value="22:45" />
          <option value="23:00" />
          <option value="23:15" />
          <option value="23:30" />
          <option value="23:45" />
        </datalist>
      </span>
    );
  }
}
