import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import './CommentField.scss';

const ROOT = 'ts-comment-field';

/**
 * テキスト項目 - 共通コンポーネント
 */
export default class CommentField extends React.Component {
  static get propTypes() {
    return {
      icon: PropTypes.string.isRequired,
      className: PropTypes.string,
      value: PropTypes.string,
    };
  }

  render() {
    const cssClass = classNames(ROOT, this.props.className);

    return (
      <div className={cssClass}>
        <div className={`${ROOT}__icon`}>
          <img
            className={`${ROOT}__icon-img`}
            src={this.props.icon}
            alt=""
            aria-hidden="true"
          />
        </div>

        <div className={`${ROOT}__bubble`}>
          <textarea className={`${ROOT}__textarea slds-textarea`}>
            {this.props.value}
          </textarea>
        </div>
      </div>
    );
  }
}
