import React from 'react';
import { shallow } from 'enzyme';

import TimeTrackingRangeField from '../TimeTrackingRangeField';

const dummyProp = {
  barColor: 'red',
  linkBarColor: 'yellow',
  onChange: () => {},
  width: 600,
};

/*
 * 単体テスト
 * TimeTrackingRangeField - 工数レンジフィールド
 */
describe('TimeTrackingRangeField', () => {
  let field;

  beforeEach(() => {
    field = shallow(<TimeTrackingRangeField {...dummyProp} />);
  });

  test('calcSteppedRatio()', () => {
    const steppedRatio = field.instance().calcSteppedRatio(150);
    expect(steppedRatio).toBe(150);
  });
});
