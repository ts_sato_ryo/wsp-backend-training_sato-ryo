// @flow
import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import Icon from '../../../mobile-app/components/atoms/Icon';
import IconButton from '../../../mobile-app/components/atoms/IconButton';

import DateUtil from '../../utils/DateUtil';
import FormatUtil from '../../utils/FormatUtil';

import './CustomDropdown.scss';

const ROOT = 'commons-fields-dropdown-custom';

type Props = {
  children: any,
  valuePlaceholder: string,
  isClearable?: boolean,
  displayColor?: boolean,
  onClickDeteleButton?: () => void,
  data?: Array<{ label: string, value: string }>,
  selectedStringValues?: Array<string>,
  selectedStringValue?: ?string,
  selectedDateRangeValues?: { startDate: ?string, endDate: ?string },
  selectedAmountRangeValues?: { startAmount: ?number, endAmount: ?number },
  selectedSearchConditionName?: string,
  currencyDecimalPlaces?: number,
};

type State = {
  isOpen: boolean,
  displayValue: ?string,
};

class CustomDropdown extends React.Component<Props, State> {
  component: any;

  state = {
    isOpen: false,
    displayValue: this.createDisplayValue(),
  };

  componentDidMount() {
    document.addEventListener('click', this.handleOutsideClick, false);
  }

  componentWillReceiveProps(nextProps: Props) {
    if (
      this.props.selectedSearchConditionName !==
        nextProps.selectedSearchConditionName ||
      this.props.selectedStringValues !== nextProps.selectedStringValues
    ) {
      const displayValueArray = nextProps.selectedStringValues
        ? nextProps.selectedStringValues.map((selectedValue) => {
            const findVal = _.find(nextProps.data, { value: selectedValue });
            return findVal && findVal.label ? findVal.label : null;
          })
        : [];
      const displayValue = _.compact(displayValueArray).join(', ');
      this.setState({ displayValue });
    }
    if (
      this.props.selectedSearchConditionName !==
        nextProps.selectedSearchConditionName ||
      this.props.selectedDateRangeValues !== nextProps.selectedDateRangeValues
    ) {
      const displayValue = this.formatDateRange(
        nextProps.selectedDateRangeValues
      );
      this.setState({ displayValue });
    }
    if (
      this.props.selectedSearchConditionName !==
        nextProps.selectedSearchConditionName ||
      this.props.selectedAmountRangeValues !==
        nextProps.selectedAmountRangeValues
    ) {
      const displayValue = this.formatAmountRange(
        nextProps.selectedAmountRangeValues
      );
      this.setState({ displayValue });
    }
    if (
      this.props.selectedSearchConditionName !==
        nextProps.selectedSearchConditionName ||
      this.props.selectedStringValue !== nextProps.selectedStringValue
    ) {
      const displayValue = nextProps.selectedStringValue;
      this.setState({ displayValue });
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleOutsideClick, false);
  }

  createDisplayValue() {
    let displayValue;
    if (this.props.selectedStringValues) {
      const displayValueArray = this.props.selectedStringValues.map((value) => {
        const findVal = _.find(this.props.data, { value });
        return _.get(findVal, 'label', null);
      });
      displayValue = displayValueArray.join(', ');
    } else if (this.props.selectedStringValue) {
      displayValue = this.props.selectedStringValue || '';
    } else if (this.props.selectedDateRangeValues) {
      displayValue = this.formatDateRange(this.props.selectedDateRangeValues);
    } else if (this.props.selectedAmountRangeValues) {
      displayValue = this.formatAmountRange(
        this.props.selectedAmountRangeValues
      );
    }
    return displayValue;
  }

  formatDateRange(dateRange: ?{ startDate: ?string, endDate: ?string }) {
    const startDate =
      dateRange && dateRange.startDate
        ? DateUtil.format(dateRange.startDate)
        : '';
    const endDate =
      dateRange && dateRange.endDate ? DateUtil.format(dateRange.endDate) : '';
    return startDate || endDate ? `${startDate} - ${endDate}` : '';
  }

  formatNumber(value: number) {
    return FormatUtil.formatNumber(value, this.props.currencyDecimalPlaces);
  }

  formatAmountRange(
    amountRange: ?{ startAmount: ?number, endAmount: ?number }
  ) {
    const startAmount =
      amountRange && Number.isInteger(amountRange.startAmount)
        ? this.formatNumber(parseInt(amountRange.startAmount))
        : '';
    const endAmount =
      amountRange && Number.isInteger(amountRange.endAmount)
        ? this.formatNumber(parseInt(amountRange.endAmount))
        : '';

    return (startAmount || endAmount) && `${startAmount} - ${endAmount}`;
  }

  handleOutsideClick = (e: MouseEvent) => {
    const targetNode: any = e.target;
    const currentComponent = this.component;
    const isDatepickerComponent =
      targetNode.className &&
      typeof targetNode.className.includes !== 'undefined' &&
      targetNode.className.includes('react-datepicker__');
    if (currentComponent.contains(targetNode) || isDatepickerComponent) {
      return;
    }
    if (this.state.isOpen) {
      this.toggleDropdown();
    }
  };

  clearValues = () => {
    this.setState({
      displayValue: '',
    });
    if (this.props.onClickDeteleButton) {
      this.props.onClickDeteleButton();
    }
  };

  toggleDropdown = () => {
    this.setState((prevState) => ({
      isOpen: !prevState.isOpen,
    }));
  };

  render() {
    const { isOpen, displayValue } = this.state;
    const { valuePlaceholder, displayColor } = this.props;

    let isChangeColor: boolean = false;
    if (this.props.selectedStringValues) {
      isChangeColor = !!displayValue || !!displayColor;
    } else if (this.props.selectedDateRangeValues) {
      isChangeColor =
        !!this.props.selectedDateRangeValues.startDate ||
        !!this.props.selectedDateRangeValues.endDate;
    } else if (this.props.selectedAmountRangeValues) {
      isChangeColor =
        this.props.selectedAmountRangeValues &&
        (this.props.selectedAmountRangeValues.startAmount !== null ||
          this.props.selectedAmountRangeValues.endAmount !== null);
    } else if (this.props.selectedStringValue) {
      isChangeColor = !!this.props.selectedStringValue;
    }

    const triggerClass = classNames(`${ROOT}__value`, {
      [`${ROOT}__value--is-active`]: isChangeColor,
    });

    const renderColon = displayValue ? ' : ' : '';

    return (
      <div
        className={ROOT}
        ref={(el) => {
          this.component = el;
        }}
      >
        <button className={triggerClass} onClick={this.toggleDropdown}>
          <div>{`${valuePlaceholder}${renderColon} ${displayValue || ''}`}</div>
          <Icon
            type="chevrondown"
            className={`${ROOT}__dropdown-icon`}
            size="small"
          />
        </button>
        {this.props.isClearable && (
          <IconButton
            className={`${ROOT}__dropdown-icon-btn`}
            icon="clear"
            size="small"
            onClick={this.clearValues}
          />
        )}

        {isOpen && this.props.children(this.toggleDropdown)}
      </div>
    );
  }
}

export default CustomDropdown;
