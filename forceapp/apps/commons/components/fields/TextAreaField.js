// @flow

import * as React from 'react';
import classNames from 'classnames';
import TextAreaAutoSize from 'react-textarea-autosize';

import './TextAreaField.scss';

/**
 * テキストエリア項目 - 共通コンポーネント
 */

type Props = {
  className?: string,
  onChange?: (e: SyntheticInputEvent<HTMLInputElement>, string) => void,
  onFocus?: (e: SyntheticInputEvent<HTMLInputElement>, string) => void,
  onBlur?: (e: SyntheticInputEvent<HTMLInputElement>, string) => void,
  onKeyDown?: (e: SyntheticInputEvent<HTMLInputElement>, string) => void,
  value?: string | number,
  disabled?: boolean,
  readOnly?: boolean,
  autosize?: boolean,
  rows?: number,
  label?: string,
  minRows?: number,
  maxRows?: number,
  resize?: 'none',
  isRequired?: boolean,
};

type State = { textarea: React.ComponentType<Props> };

const ROOT = 'ts-textarea-field';

export default class TextAreaField extends React.Component<Props, State> {
  static defaultProp = {
    disabled: false,
    readOnly: false,
    autosize: false,
  };

  state = {
    textarea: this.props.autosize
      ? TextAreaAutoSize
      : (ps: Props) => <textarea {...ps} />,
  };

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps: Props) {
    if (nextProps.autosize !== this.props.autosize) {
      this.setState({
        textarea: nextProps.autosize
          ? TextAreaAutoSize
          : (ps: Props) => <textarea {...ps} />,
      });
    }
  }

  onFocus = (e: SyntheticInputEvent<HTMLInputElement>) => {
    if (this.props.onFocus) {
      this.props.onFocus(e, e.target.value);
    }
  };

  onBlur = (e: SyntheticInputEvent<HTMLInputElement>) => {
    if (this.props.onBlur) {
      this.props.onBlur(e, e.target.value);
    }
  };

  onKeyDown = (e: SyntheticInputEvent<HTMLInputElement>) => {
    if (this.props.onKeyDown) {
      this.props.onKeyDown(e, e.target.value);
    }
  };

  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    if (this.props.onChange) {
      this.props.onChange(e, e.target.value);
    }
  };

  render() {
    const {
      className,
      value,
      disabled,
      readOnly,
      label,
      isRequired,
      minRows,
      maxRows,
      resize,
      ...props
    } = this.props;

    const textAreaFieldClass = classNames(ROOT, className, {
      'slds-input': true,
      [`${ROOT}--no-resize`]: resize === 'none',
    });

    const TextArea = this.state.textarea;

    // 重複回避
    delete props.onFocus;
    delete props.onBlur;
    delete props.onKeyDown;
    delete props.onChange;

    if (readOnly) {
      return <div className={`${ROOT} ${ROOT}--readonly`}>{value}</div>;
    } else {
      const body = this.props.autosize ? (
        <TextArea
          className={textAreaFieldClass}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onKeyDown={this.onKeyDown}
          onChange={this.onChange}
          value={value}
          disabled={disabled}
          readOnly={readOnly}
          minRows={minRows}
          maxRows={maxRows}
          {...props}
        />
      ) : (
        <TextArea
          className={textAreaFieldClass}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onKeyDown={this.onKeyDown}
          onChange={this.onChange}
          value={value}
          disabled={disabled}
          readOnly={readOnly}
          {...props}
        />
      );
      if (label) {
        const isRequiredString = isRequired ? (
          <span className="is-required">*</span>
        ) : (
          ''
        );
        return (
          <div className={`${ROOT}-container`}>
            <p className="key">
              {isRequiredString}
              &nbsp;{label}
            </p>
            {body}
          </div>
        );
      } else {
        return body;
      }
    }
  }
}
