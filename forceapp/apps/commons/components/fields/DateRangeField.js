import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import DateField from './DateField';

import './DateRangeField.scss';

const ROOT = 'ts-date-range-field';

/**
 * 期間選択項目 - 共通コンポーネント
 */
export default class DateRangeField extends React.Component {
  static propTypes = {
    startDateFieldProps: PropTypes.shape(DateField.propTypes).isRequired,
    endDateFieldProps: PropTypes.shape(DateField.propTypes).isRequired,
    className: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    required: false,
    disabled: false,
  };

  render() {
    const fieldClass = classNames(ROOT, this.props.className);

    const {
      value: valueOfStartDateField,
      selected: selectedOfStartDateField,
      maxDate: maxDateOfStartDateField,
      className: classNameOfStartDateField,
      required: requiredOfStartDateField,
      disabled: disabledOfStartDateField,
      ...restPropsOfStartDateField
    } = this.props.startDateFieldProps;

    const {
      value: valueOfEndDateField,
      selected: selectedOfEndDateField,
      minDate: minDateOfStartDateField,
      className: classNameOfEndDateField,
      required: requiredOfEndDateField,
      disabled: disabledOfEndDateField,
      ...restPropsOfEndDateField
    } = this.props.endDateFieldProps;

    return (
      <div className={fieldClass}>
        <span className={`${ROOT}__input`}>
          <DateField
            value={valueOfStartDateField}
            selected={selectedOfStartDateField}
            maxDate={maxDateOfStartDateField || valueOfEndDateField}
            className={classNames('slds-input', classNameOfStartDateField)}
            required={this.props.required || requiredOfStartDateField}
            disabled={this.props.disabled || disabledOfStartDateField}
            {...restPropsOfStartDateField}
          />
        </span>

        <span className={`${ROOT}__separation`}>
          <span className={`${ROOT}__separation-inner`}>–</span>
        </span>

        <span className={`${ROOT}__input`}>
          <DateField
            value={valueOfEndDateField}
            selected={selectedOfEndDateField}
            minDate={minDateOfStartDateField || valueOfStartDateField}
            className={classNames('slds-input', classNameOfEndDateField)}
            required={this.props.required || requiredOfEndDateField}
            disabled={this.props.disabled || disabledOfEndDateField}
            {...restPropsOfEndDateField}
          />
        </span>
      </div>
    );
  }
}
