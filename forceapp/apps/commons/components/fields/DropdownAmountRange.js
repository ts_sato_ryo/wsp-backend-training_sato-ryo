// @flow
import React from 'react';

import msg from '../../languages';
import Button from '../buttons/Button';
import AmountField from './AmountField';

import './DropdownAmountRange.scss';

const ROOT = 'commons-fields-dropdown-amount';

type AmountRangeOption = {
  startAmount: ?number,
  endAmount: ?number,
};

type Props = {
  amountRange: AmountRangeOption,
  onClickUpdateAmount: (amountRangeOption: AmountRangeOption) => void,
  currencyDecimalPlaces: number,
  toggleDropdown: any,
};

type State = {
  startAmount: ?number,
  endAmount: ?number,
  error: ?string,
};

class DropdownAmountRange extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      startAmount: this.props.amountRange.startAmount,
      endAmount: this.props.amountRange.endAmount,
      error: null,
    };
  }

  updateStartAmount = (value: number | null) => {
    const valueNumber = value === null ? null : Number(value);
    if (
      valueNumber !== null &&
      valueNumber !== undefined &&
      this.state.endAmount !== null &&
      this.state.endAmount !== undefined &&
      valueNumber > this.state.endAmount
    ) {
      this.setState({
        error: msg().Exp_Err_AmountLeftLessThanRight,
      });
    } else {
      this.clearError();
    }
    this.setState({
      startAmount: valueNumber,
    });
  };

  updateEndAmount = (value: number | null) => {
    const valueNumber = value === null ? null : Number(value);
    if (
      valueNumber !== null &&
      valueNumber !== undefined &&
      this.state.startAmount !== null &&
      this.state.startAmount !== undefined &&
      valueNumber < this.state.startAmount
    ) {
      this.setState({
        error: msg().Exp_Err_AmountLeftLessThanRight,
      });
    } else {
      this.clearError();
    }
    this.setState({
      endAmount: valueNumber,
    });
  };

  updateResult = () => {
    const { startAmount, endAmount } = this.state;
    this.props.onClickUpdateAmount({ startAmount, endAmount });
    this.props.toggleDropdown();
  };

  clearError = () => {
    this.setState({
      error: '',
    });
  };

  render() {
    const { error } = this.state;
    const { startAmount, endAmount } = this.props.amountRange;

    return (
      <div className={`${ROOT}`}>
        <span className={`${ROOT}__input1`}>
          <AmountField
            value={startAmount}
            onBlur={this.updateStartAmount}
            fractionDigits={this.props.currencyDecimalPlaces}
            nullable
          />
        </span>
        <span className={`${ROOT}__separation`}>
          <span className={`${ROOT}__separation-inner`}>–</span>
        </span>
        <span className={`${ROOT}__input2`}>
          <AmountField
            value={endAmount}
            onBlur={this.updateEndAmount}
            fractionDigits={this.props.currencyDecimalPlaces}
            nullable
          />
        </span>

        {error && <p className={`${ROOT}__error`}>{error}</p>}

        <div className={`${ROOT}__amount-buttons`}>
          <Button
            onClick={this.updateResult}
            disabled={this.state.error !== ''}
          >
            {msg().Com_Btn_Update}
          </Button>
        </div>
      </div>
    );
  }
}

export default DropdownAmountRange;
