import PropTypes from 'prop-types';
import React from 'react';

import SelectField from './SelectField';
import Button from '../buttons/Button';

import ImgBtnSearchInput from '../../images/btnSearchInput.png';

import './LookupField.scss';

export default class LookupField extends React.Component {
  static get propTypes() {
    return {
      options: PropTypes.array,
      id: PropTypes.string,
      disabled: PropTypes.bool,
      onClickSearchButton: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      options: [],
      id: '',
      disabled: false,
    };
  }

  render() {
    return (
      <div className="ts-lookup-field">
        <div className="ts-lookup-field__select">
          <SelectField id={this.props.id} options={this.props.options} disabled={this.props.options.length === 0 || this.props.disabled === true} />
        </div>
        <div className="ts-lookup-field__button">
          <Button onClick={this.props.onClickSearchButton}>
            <img src={ImgBtnSearchInput} alt="Search" />
          </Button>
        </div>
      </div>
    );
  }
}
