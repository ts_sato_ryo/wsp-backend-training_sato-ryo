// @flow
import React from 'react';
import classNames from 'classnames';

import msg from '../languages';
import TextUtil from '../utils/TextUtil';

// import './Pagination.scss';

const ROOT = 'ts-pager-info';

export type Props = {
  className?: ?string,
  currentPage: number,
  totalNum: number,
  pageSize: number,
};

const Pager = ({ className, currentPage, pageSize, totalNum }: Props) => {
  const cssName = classNames(ROOT, className);

  // TODO: confirm design
  if (totalNum === 0) {
    return (
      <div className={cssName}>
        {TextUtil.template(msg().Cmn_Lbl_PagerInfo, 0, 0, 0)}
      </div>
    );
  }

  const lastPageNum = Math.ceil(totalNum / pageSize);
  const first = (currentPage - 1) * pageSize + 1;
  const last = currentPage === lastPageNum ? totalNum : currentPage * pageSize;

  return (
    <div className={cssName}>
      {TextUtil.template(msg().Cmn_Lbl_PagerInfo, first, last, totalNum)}
    </div>
  );
};

export default Pager;
