// @flow
import React from 'react';

import GlobalContainer from '../containers/GlobalContainer';
import PCHeader from './PCHeader';
import Tab from './Tab';
import Unavailable from './Unavailable';

type Props = {
  // specify shape of object
  config: {
    tabs: Array<any>,
    header: {
      title: string,
      icon: string,
      color: string,
    },
  },
};

type State = {
  selectedTab: number,
};

export default class GlobalNav extends React.Component<Props, State> {
  state = {
    selectedTab: 0,
  };

  isActiveMenu(tabIndex: number) {
    return this.state.selectedTab === tabIndex;
  }

  changeMenu(tabIndex: number) {
    this.setState({
      selectedTab: tabIndex,
    });
  }

  renderTabItems(tabs: Array<any>): any {
    if (!tabs) {
      return null;
    }

    const tabItems = tabs.map((tab: any, index: number) => {
      return (
        <Tab
          key={index}
          label={tab.label}
          icon={tab.iconSrc}
          iconWidth={tab.iconWidth || '20'}
          selected={this.isActiveMenu(index)}
          onSelect={() => this.changeMenu(index)}
          notificationCount={tab.notificationCount}
        />
      );
    });
    return <div>{tabItems}</div>;
  }

  // render SelectedComponent. Defaults to Unavailable Component
  renderContent(tabs: any) {
    const SelectedComponent =
      tabs[this.state.selectedTab].component || Unavailable;
    return SelectedComponent;
  }

  render() {
    const { config } = this.props;

    return (
      <GlobalContainer>
        <PCHeader
          {...config.header}
          headerContent={this.renderTabItems(config.tabs)}
        />
        {this.renderContent(config.tabs)}
      </GlobalContainer>
    );
  }
}
