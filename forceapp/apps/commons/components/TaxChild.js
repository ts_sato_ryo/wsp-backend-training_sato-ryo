import PropTypes from 'prop-types';
import React from 'react';

import msg from '../languages';
import FormatUtil from '../utils/FormatUtil';
import TaxUtil from '../utils/TaxUtil';
import ObjectUtil from '../utils/ObjectUtil';

import './Tax.scss';

export default class TaxChild extends React.Component {
  static get propTypes() {
    return {
      readOnly: PropTypes.bool.isRequired,
      amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      amountWithoutTax: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequired,
      expTaxTypeList: PropTypes.array.isRequired,
      // FIXME: taxTypeとtaxTypeIdという項目が存在し、taxTypeという単語がかぶっているため「taxType」の単語をオブジェクトのスキーマレベルで再定義するか検討する（GENIE-1886）
      // 税額が自動計算かどうかを表す「TaxType」として定義している。「税の計算が自動計算かどうか」は明細単位で指定するので ExpRecord__c.TaxType__c という項目が存在する。
      // 税区分マスタのオブジェクトを表すものは「TaxTypeId」として定義している。「税区分」は内訳単位で指定するので ExpRecordItem__c.TaxTypeId__c という項目が存在する。
      taxTypeId: PropTypes.string,
      taxType: PropTypes.string.isRequired,
      onChangeRecordItemTaxValues: PropTypes.func.isRequired,
      recordItemIndex: PropTypes.number.isRequired,
    };
  }

  static get defaultProps() {
    return {
      taxTypeId: '',
    };
  }

  onChangeRecordItemAmount(event) {
    const amount = event.target.value;
    const selectedExpTax = TaxUtil.findSelectedExpTax(
      this.props.expTaxTypeList,
      this.props.taxTypeId
    );
    const taxRate = ObjectUtil.getOrDefault(selectedExpTax, 'rate', 0);

    // 税種別が「総額」「課税なし」の場合
    if (
      TaxUtil.isTaxTypeTotal(this.props.taxType) ||
      TaxUtil.isTaxTypeNonTaxable(this.props.taxType)
    ) {
      this.props.onChangeRecordItemTaxValues(
        'amountWithoutTax',
        amount,
        taxRate,
        this.props.recordItemIndex
      );
    } else if (TaxUtil.isTaxTypePerRecordItem(this.props.taxType)) {
      // 税種別が「内訳毎」の場合
      this.props.onChangeRecordItemTaxValues(
        'amount',
        amount,
        taxRate,
        this.props.recordItemIndex
      );
    }
  }

  onChangeTaxRate(event) {
    const taxTypeId = event.target.value;
    const selectedExpTax = TaxUtil.findSelectedExpTax(
      this.props.expTaxTypeList,
      taxTypeId
    );
    this.props.onChangeRecordItemTaxValues(
      'taxTypeId',
      selectedExpTax.id,
      selectedExpTax.rate,
      this.props.recordItemIndex
    );
  }

  renderSelectTaxRate() {
    const selectTypeList = TaxUtil.setInitialExpTaxLabel(
      this.props.expTaxTypeList
    ).map((item) => (
      <option key={item.id} value={item.id}>
        {item.label}
      </option>
    ));

    return (
      <div className="value">
        <select
          value={this.props.taxTypeId}
          onChange={(event) => this.onChangeTaxRate(event)}
          disabled={this.props.readOnly}
        >
          {selectTypeList}
        </select>
      </div>
    );
  }

  renderInputAmount() {
    // 税種別が「総額」の場合、税抜金額を表示する
    const amount = TaxUtil.isTaxTypeTotal(this.props.taxType)
      ? this.props.amountWithoutTax
      : this.props.amount;
    return (
      <div className="value">
        <input
          type="text"
          value={FormatUtil.convertToIntegerString(amount)}
          onChange={(event) => this.onChangeRecordItemAmount(event)}
          disabled={this.props.readOnly}
        />
      </div>
    );
  }

  renderAmountLabel() {
    if (TaxUtil.isTaxTypeTotal(this.props.taxType)) {
      return msg().Exp_Lbl_WithoutTax;
    } else if (TaxUtil.isTaxTypePerRecordItem(this.props.taxType)) {
      return msg().Exp_Lbl_IncludeTax;
    } else {
      return msg().Exp_Lbl_Amount;
    }
  }

  render() {
    return (
      <RenderTaxItemArea
        leftLabel={msg().Exp_Lbl_TaxRate}
        leftArea={this.renderSelectTaxRate()}
        rightLabel={this.renderAmountLabel()}
        rightArea={this.renderInputAmount()}
        taxType={this.props.taxType}
      />
    );
  }
}

class RenderTaxItemArea extends React.Component {
  static get propTypes() {
    return {
      leftLabel: PropTypes.node.isRequired,
      leftArea: PropTypes.node.isRequired,
      rightLabel: PropTypes.node.isRequired,
      rightArea: PropTypes.node.isRequired,
      taxType: PropTypes.string.isRequired,
    };
  }

  renderTaxRateLabel() {
    return (
      <div className="slds-col slds-size--3-of-12 slds-align-middle">
        <div className="key">&nbsp;{this.props.leftLabel}</div>
        <div className="separate">:</div>
      </div>
    );
  }

  renderTaxRate() {
    return (
      <div className="slds-col slds-size--3-of-12 slds-align-middle">
        {this.props.leftArea}
      </div>
    );
  }

  renderAmountLabel(column) {
    return (
      <div className={`slds-col slds-size--${column}-of-12 slds-align-middle`}>
        <div className="key">&nbsp;{this.props.rightLabel}</div>
        <div className="separate">:</div>
      </div>
    );
  }

  renderAmount() {
    return (
      <div className="slds-col slds-size--3-of-12 slds-align-middle right">
        {this.props.rightArea}
      </div>
    );
  }

  render() {
    // 税種別が「課税なし」が選択された場合は税率は非表示
    if (TaxUtil.isTaxTypeNonTaxable(this.props.taxType)) {
      return (
        <div className="slds-grid">
          {this.renderAmountLabel(3)}
          {this.renderAmount()}
          <div className="slds-col slds-size--6-of-12 slds-align-middle" />
        </div>
      );
    }
    return (
      <div className="slds-grid">
        {this.renderTaxRateLabel()}
        {this.renderTaxRate()}
        <div className="slds-col slds-size--1-of-12 slds-align-middle" />
        {this.renderAmountLabel(2)}
        {this.renderAmount()}
      </div>
    );
  }
}
