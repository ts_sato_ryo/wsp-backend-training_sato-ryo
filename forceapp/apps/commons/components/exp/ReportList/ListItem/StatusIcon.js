// @flow
import React from 'react';

import { status } from '../../../../../domain/modules/exp/report';
import { type ReportListItem } from '../../../../../domain/models/exp/Report';

// images
import ImgIconStatusApprovedNoPhoto from '../../../../images/iconStatusApprovedNoPhoto.png';
import ImgIconStatusSubmit from '../../../../images/iconStatusSubmit.png';
import ImgIconStatusReject from '../../../../images/iconStatusReject.png';
import ImgIconStatusApprovedPreRequest from '../../../../images/iconStatusApprovedPreRequest.png';

const ROOT = 'ts-expenses__reports-icon';

type Props = {
  item: ReportListItem,
};

export default class StatusIcon extends React.Component<Props> {
  render() {
    const { item } = this.props;
    let src = null;

    if (item.status === status.APPROVED) {
      src = ImgIconStatusApprovedNoPhoto;
    } else if (
      item.status === status.REJECTED ||
      item.status === status.CANCELED
    ) {
      src = ImgIconStatusReject;
    } else if (item.status === status.REMOVED) {
      src = ImgIconStatusReject;
    } else if (item.status === status.PENDING) {
      src = ImgIconStatusSubmit;
    } else if (item.status === status.APPROVED_PRE_REQUEST) {
      src = ImgIconStatusApprovedPreRequest;
    } else if (item.status === status.CLAIMED) {
      src = ImgIconStatusApprovedPreRequest;
    }
    const mainIcon = src ? (
      <img className={`${ROOT}-status`} src={src} role="presentation" />
    ) : null;

    return mainIcon;
  }
}
