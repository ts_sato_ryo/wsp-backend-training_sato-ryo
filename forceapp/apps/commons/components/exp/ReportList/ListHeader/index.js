// @flow
import React from 'react';

import msg from '../../../../languages';
import MultiColumnsGrid from '../../../MultiColumnsGrid';

import './index.scss';

type Props = {};

const ROOT = 'ts-expenses__reports-list-header';

export default class ExpensesReportListHeader extends React.Component<Props> {
  render() {
    return (
      <MultiColumnsGrid className={`${ROOT}`} sizeList={[2, 2, 4, 3, 1]}>
        <div className={`${ROOT}--status`}>{msg().Exp_Lbl_Status}</div>
        <div className={`${ROOT}--date`}>{msg().Exp_Lbl_DateSubmitted}</div>
        <div className={`${ROOT}--subject`}>{msg().Exp_Clbl_ReportTitle}</div>
        <div className={`${ROOT}--amount`}>{msg().Exp_Clbl_Amount}</div>
        <div />
      </MultiColumnsGrid>
    );
  }
}
