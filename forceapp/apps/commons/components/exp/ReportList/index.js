// @flow
import React from 'react';

import msg from '../../../languages';

import ListHeader from './ListHeader';
import ListItem from './ListItem';

import {
  type Report,
  type ReportList,
} from '../../../../domain/models/exp/Report';

// Pagination
import PagerInfo, { type Props as PagerInfoProps } from '../../PagerInfo';
import Pagenation, { type Props as PagerProps } from '../../Pagination';
import Icon from '../../../../mobile-app/components/atoms/Icon';

import './index.scss';

const ROOT = 'ts-expenses__reports';

type PaginationProps = {|
  pageSize: number,
  maxPageNo: number,
  maxSearchNo: number,
  currentPage: number,
  requestTotalNum: number,
  fetchExpReportIdList: (isApproved: boolean) => void,
  onClickRefreshButton: (isApproved: boolean) => void,
|};

type Props = {
  filter?: string,
  expReportList: ReportList,
  selectedExpReport: Report,
  onClickReportItem: () => void,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  ...PaginationProps,
} & PagerProps &
  PagerInfoProps;

export default class ExpensesReportList extends React.Component<Props> {
  componentWillMount() {
    const isApproved = this.props.filter === 'Approved';
    this.props.fetchExpReportIdList(isApproved);
  }

  componentDidUpdate(prevProps: Props) {
    const { filter } = this.props;
    const isApproved = filter === 'Approved';

    if (filter !== prevProps.filter) {
      this.props.fetchExpReportIdList(isApproved);
    }
  }

  render() {
    const {
      expReportList,
      filter,
      // pagination
      currentPage,
      pageSize,
      requestTotalNum,
      maxPageNo,
      maxSearchNo,
      onClickPagerLink,
    } = this.props;

    const isApproved = filter === 'Approved';

    return (
      <div className={`${ROOT}`}>
        {this.props.requestTotalNum > 0 && (
          <div className={`${ROOT}__request-header`}>
            <PagerInfo
              className={`${ROOT}__page-info`}
              currentPage={this.props.currentPage}
              totalNum={this.props.requestTotalNum}
              pageSize={this.props.pageSize}
            />
            <button
              className={`${ROOT}__refresh-btn`}
              onClick={() => this.props.onClickRefreshButton(isApproved)}
            >
              <Icon type="refresh-copy" size="small" />
            </button>
          </div>
        )}
        <ListHeader sortKey="" />

        {/* if any kinds of report doesnt exists / どんな種類のレポートでも存在しなければ */}
        {expReportList.length === 0 && (
          <div className={`${ROOT}-list__empty--notapproved`}>
            {msg().Exp_Msg_CreateNewReport}
          </div>
        )}
        {/* if any kinds of report exists / どんな種類のレポートでもあれば */}
        {expReportList.length !== 0 && (
          <div className={`${ROOT}-list`}>
            {expReportList.map((item) => (
              <ListItem
                item={item}
                key={item.reportId}
                baseCurrencySymbol={this.props.baseCurrencySymbol}
                baseCurrencyDecimal={this.props.baseCurrencyDecimal}
                onClickReportItem={this.props.onClickReportItem}
                selectedExpReportId={this.props.selectedExpReport.reportId}
              />
            ))}

            {currentPage === maxPageNo && requestTotalNum > maxSearchNo && (
              <div className={`${ROOT}-too-many-results`}>
                {msg().Com_Lbl_TooManySearchResults}
              </div>
            )}

            <div className={`${ROOT}-list-footer`}>
              <PagerInfo
                className={`${ROOT}-page-info`}
                currentPage={currentPage}
                totalNum={requestTotalNum}
                pageSize={pageSize}
              />
              <Pagenation
                className={`${ROOT}-list-pager`}
                currentPage={currentPage}
                totalNum={requestTotalNum}
                displayNum={5}
                pageSize={pageSize}
                onClickPagerLink={(num) => onClickPagerLink(num)}
                maxPageNum={maxPageNo}
              />
            </div>
          </div>
        )}
        {expReportList.length !== 0 && filter === 'NotRequested' && (
          <div className={`${ROOT}-list__empty--approved`}>
            {msg().Exp_Msg_NoReportFound}
          </div>
        )}
        {expReportList.length !== 0 && filter === 'Approved' && (
          <div className={`${ROOT}-list__empty--approved`}>
            {msg().Exp_Msg_NoPassedReportFound}
          </div>
        )}
      </div>
    );
  }
}
