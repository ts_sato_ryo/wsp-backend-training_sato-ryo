// @flow
import React, { useState, useEffect } from 'react';
import get from 'lodash/get';

import msg from '../../../../languages';
import Button from '../../../buttons/Button';
import {
  type VendorItemList,
  VENDOR_PAYMENT_DUE_DATE_USAGE,
} from '../../../../../domain/models/exp/Vendor';
import IconClose from '../../../../images/icons/close.svg';

import './index.scss';

const ROOT = 'ts-expenses-vendor-detail-panel';

export const DRAG_HANDLE = `${ROOT}__drag-handle`;

export type PanelProps = {|
  vendorId: ?string,
  closePanel: () => void,
  searchVendorDetail: (vendorId: string) => Promise<VendorItemList>,
|};

const renderBankAccountType = (value: string) => {
  if (value === 'Checking') {
    return msg().Exp_Sel_BankChecking;
  } else if (value === 'Savings') {
    return msg().Exp_Sel_BankSavings;
  }
  return null;
};

const renderWithholdingTax = (isWithholding) => {
  return isWithholding ? msg().Exp_Lbl_Yes : msg().Exp_Lbl_No;
};

const renderPaymentDateUsage = (usage) => {
  let text;
  switch (usage) {
    case VENDOR_PAYMENT_DUE_DATE_USAGE.NotUsed:
      text = msg().Exp_Sel_NoUsed;
      break;
    case VENDOR_PAYMENT_DUE_DATE_USAGE.Required:
      text = msg().Exp_Sel_Required;
      break;
    case VENDOR_PAYMENT_DUE_DATE_USAGE.Optional:
      text = msg().Exp_Sel_Optional;
      break;
    default:
      text = msg().Exp_Sel_Optional;
  }
  return text;
};

const renderRow = (key, value) => {
  return value ? (
    <tr>
      <th>{key}</th>
      <td>{value}</td>
    </tr>
  ) : null;
};

const DetailPanel = (props: PanelProps) => {
  const [vendorInfo, setVendorInfo] = useState(null);
  const { searchVendorDetail, closePanel, vendorId } = props;

  useEffect(() => {
    setVendorInfo(null);
    if (vendorId) {
      searchVendorDetail(vendorId).then((res: VendorItemList) => {
        const info = get(res, 'records.0', null);
        setVendorInfo(info);
      });
    }
  }, [vendorId]);

  let container = null;

  if (vendorInfo) {
    const accountTypeDisplay = renderBankAccountType(
      vendorInfo.bankAccountType
    );

    const withholdingTax = renderWithholdingTax(vendorInfo.isWithholdingTax);

    const keyValueInfo = {
      [msg().Exp_Lbl_PaymentDate]: renderPaymentDateUsage(
        vendorInfo.paymentDueDateUsage
      ),
      [msg().Exp_Lbl_PaymentTerm]: vendorInfo.paymentTerm,
      [msg().Exp_Lbl_PaymentTermCode]: vendorInfo.paymentTermCode,
      [msg().Exp_Lbl_WithholdingTax]: withholdingTax,
      [msg().Exp_Lbl_BankAccountType]: accountTypeDisplay,
      [msg().Exp_Lbl_BankAccountNumber]: vendorInfo.bankAccountNumber,
      [msg().Exp_Lbl_BankCode]: vendorInfo.bankCode,
      [msg().Exp_Lbl_BankName]: vendorInfo.bankName,
      [msg().Exp_Lbl_BranchCode]: vendorInfo.branchCode,
      [msg().Exp_Lbl_BranchName]: vendorInfo.branchName,
      [msg().Exp_Lbl_BranchAddress]: vendorInfo.branchAddress,
      [msg().Exp_Lbl_PayeeName]: vendorInfo.payeeName,
      [msg().Exp_Lbl_BankCurrency]: vendorInfo.currencyCode,
      [msg().Exp_Lbl_SwiftCode]: vendorInfo.swiftCode,

      [msg().Exp_Lbl_CorrespondentBankBankName]:
        vendorInfo.correspondentBankName,

      [msg().Exp_Lbl_CorrespondentBankBranchName]:
        vendorInfo.correspondentBranchName,

      [msg().Exp_Lbl_CorrespondentBankAddress]:
        vendorInfo.correspondentBankAddress,

      [msg().Exp_Lbl_CorrespondentBankSwiftCode]:
        vendorInfo.correspondentSwiftCode,

      [msg().Exp_Lbl_CountryCode]: vendorInfo.country,
      [msg().Exp_Lbl_ZipCode]: vendorInfo.zipCode,
      [msg().Exp_Lbl_Address]: vendorInfo.address,
    };

    const infoRows = Object.keys(keyValueInfo).map((key) =>
      renderRow(key, keyValueInfo[key])
    );

    container = (
      <div className={ROOT}>
        <div className={`${ROOT}__header`}>
          <div className={`${ROOT}__header-title ${DRAG_HANDLE}`}>
            {msg().Exp_Lbl_VendorDetail}
          </div>
          <Button type="text" className={`${ROOT}__close`} onClick={closePanel}>
            <IconClose aria-hidden="true" className={`${ROOT}__close-icon`} />
          </Button>
        </div>
        <div className={`${ROOT}__table`}>
          <table className={`${ROOT}__table-content`}>
            <tbody>{infoRows}</tbody>
          </table>
        </div>
      </div>
    );
  }
  return container;
};

export default DetailPanel;
