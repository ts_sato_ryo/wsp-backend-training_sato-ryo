// @flow
import React from 'react';

import { get } from 'lodash';
import msg from '../../../../../languages';
import Button from '../../../../buttons/Button';
import RecordDate from '../RecordDate';
import SelectField from '../../../../fields/SelectField';
import TextField from '../../../../fields/TextField';
import LabelWithHint from '../../../../fields/LabelWithHint';
import General from '../General';
import ExtendedItems from '../ExtendedItem';
import Summary from '../Summary';
import { type Record } from '../../../../../../domain/models/exp/Record';
import { type CustomHint } from '../../../../../../domain/models/exp/CustomHint';
import { type EISearchObj } from '../../../../../../domain/models/exp/ExtendedItem';

import './index.scss';

const ROOT = 'ts-expenses-child-record-item';

export type expTypeDisplay = {
  value: string,
  text: string,
};

export type ChildRecordItemProps = {
  onChangeChildDateOrTypeForBC: ({
    recordDate?: string,
    expTypeId?: string,
  }) => void,
  handleClickDeleteBtn: (number) => void,
  onChangeChildDateForFC: (string) => void,
  onChangeChildExpTypeForFC: (string) => void,
  recordItemIdx: number,
  expTypesDisplay: Array<expTypeDisplay>,
};
type Props = {
  errors: { recordDate?: string, records?: Array<*> },
  touched: { recordDate?: string, records?: Array<*> },
  recordIdx: number,
  expRecord: Record,
  customHint: CustomHint,
  baseCurrencyCode: string,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  baseCurrency: any,
  foreignCurrency: any,
  isFinanceApproval?: boolean,
  onChangeEditingExpReport: (string, any, any) => void,
  onClickLookupEISearch: (item: EISearchObj) => void,
} & ChildRecordItemProps;

export default class ChildRecordItem extends React.Component<Props> {
  render() {
    const {
      expRecord,
      errors,
      touched,
      recordIdx,
      recordItemIdx,
      expTypesDisplay,
      isFinanceApproval,
      customHint,
    } = this.props;

    const targetRecord = `records.${recordIdx}.items.${recordItemIdx}`;
    const recordErrors = get(errors, `records.${recordIdx}`);
    const expTypeError = get(errors, `${targetRecord}.expTypeId`);

    // if no item is selected, show empty only with title
    if (!recordItemIdx) {
      return (
        <section className={`${ROOT}-header`}>
          <div className={`${ROOT}-nav-title`}>
            <span>{msg().Exp_Lbl_Detail}</span>
          </div>
        </section>
      );
    }

    const targetItem = expRecord.items[recordItemIdx];
    const expenseType = get(targetItem, 'expTypeEditable') ? (
      <div className={`${ROOT}-expense-type`}>
        <LabelWithHint
          text={msg().Exp_Clbl_ExpenseType}
          hintMsg={customHint.recordExpenseType}
          isRequired
        />
        <SelectField
          className={`${ROOT}-expense-type ts-select-input`}
          onChange={(e) => {
            if (targetItem.useForeignCurrency) {
              this.props.onChangeChildExpTypeForFC(e.target.value);
            } else {
              this.props.onChangeChildDateOrTypeForBC({
                expTypeId: e.target.value,
                recordDate: targetItem.recordDate,
              });
            }
          }}
          options={expTypesDisplay}
          value={targetItem.expTypeId}
        />
        {expTypeError && (
          <div className="input-feedback">{msg()[expTypeError]}</div>
        )}
      </div>
    ) : (
      <TextField
        className={`${ROOT}-expense-type`}
        value={targetItem.expTypeName}
        label={msg().Exp_Clbl_ExpenseType}
        isRequired
        disabled
      />
    );

    return (
      <div className={`${ROOT}  slds`}>
        <section className={`${ROOT}-header`}>
          <div className={`${ROOT}-nav-title`}>
            <span>{msg().Exp_Lbl_Detail}</span>
          </div>
          {!isFinanceApproval && (
            <Button
              type="destructive"
              onClick={() => this.props.handleClickDeleteBtn(recordItemIdx)}
              disabled={expRecord.items.length <= 2}
            >
              {msg().Com_Btn_Delete}
            </Button>
          )}
        </section>

        <section className={`${ROOT}-content`}>
          <RecordDate
            recordDate={targetItem.recordDate}
            targetRecord={targetRecord}
            hintMsg={customHint.recordDate}
            onChangeRecordDate={(recordDate) => {
              if (targetItem.useForeignCurrency) {
                this.props.onChangeChildDateForFC(recordDate);
              } else {
                this.props.onChangeChildDateOrTypeForBC({
                  expTypeId: targetItem.expTypeId,
                  recordDate,
                });
              }
            }}
            readOnly={false}
            errors={errors}
            touched={touched}
          />

          {expenseType}

          <p className={`${ROOT}-amount-title key-dark`}>
            <span className="is-required">*</span>
            &nbsp;{msg().Exp_Lbl_Amount}
          </p>

          <General
            expRecord={expRecord}
            targetRecord={`records.${recordIdx}`}
            recordItemIdx={recordItemIdx}
            recordIdx={recordIdx}
            customHint={customHint}
            onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            readOnly={false}
            errors={recordErrors}
            touched={touched}
            baseCurrencyCode={this.props.baseCurrencyCode}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            baseCurrency={this.props.baseCurrency}
            foreignCurrency={this.props.foreignCurrency}
          />

          <ExtendedItems
            recordItem={targetItem}
            onClickLookupEISearch={this.props.onClickLookupEISearch}
            onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            readOnly={false}
            targetRecordItem={`records.${recordIdx}.items.${recordItemIdx}`}
            errors={errors}
            touched={touched}
          />

          <Summary
            value={targetItem.remarks}
            hintMsg={customHint.recordSummary}
            onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            readOnly={false}
            targetRecord={`records.${recordIdx}.items.${recordItemIdx}`}
            errors={this.props.errors}
            touched={this.props.touched}
          />
        </section>
      </div>
    );
  }
}
