// @flow
import React from 'react';
import { isEmpty } from 'lodash';

import msg from '../../../../../languages';
import SelectField from '../../../../fields/SelectField';

type Props = {
  onChangeAmountSelection: (string) => void,
  readOnly: ?boolean,
  value: string,
  options: Array<Object>,
  error: string,
};

export default class AmountSelection extends React.Component<Props> {
  render() {
    const {
      options,
      value,
      readOnly,
      error,
      onChangeAmountSelection,
    } = this.props;

    const optionList =
      options.map((item) => ({
        value: item.id,
        text: item.text,
      })) || [];
    return (
      <div className="record_item__contents__amount_selection">
        <p className="key">
          <span className="is-required">*</span>
          &nbsp;{msg().Exp_Lbl_AmountSelection}
        </p>

        <SelectField
          className="ts-select-input"
          onChange={onChangeAmountSelection}
          options={optionList}
          value={value}
          disabled={readOnly}
          required
        />
        {!isEmpty(error) && <div className="input-feedback">{error}</div>}
      </div>
    );
  }
}
