// @flow
import React from 'react';
import { get, drop, isEmpty } from 'lodash';

import msg from '../../../../languages';

import ActionButtons from './ActionButtons';
import type { Report } from '../../../../../domain/models/exp/Report';
import {
  newRecord,
  RECORD_TYPE,
  RECEIPT_TYPE,
  isRecordItemized,
  isFixedAllowanceSingle,
  isFixedAllowanceMulti,
  isAmountMatch,
  calcItemsTotalAmount,
  type RouteInfo,
  type RecordItem as Record,
} from '../../../../../domain/models/exp/Record';
import MultiColumnsGrid from '../../../MultiColumnsGrid';
import { type EISearchObj } from '../../../../../domain/models/exp/ExtendedItem';
import { type CustomHint } from '../../../../../domain/models/exp/CustomHint';
import { status } from '../../../../../domain/modules/exp/report';
import ChildRecordItem, { type ChildRecordItemProps } from './ChildRecordItem';
import { type AmountOption } from '../../../../../domain/models/exp/ExpenseType';

import General from './General';
import TextField from '../../../fields/TextField';
import TransitJorudanJP from './TransitJorudanJP';
import AmountSelection from './AmountSelection';
import RecordDate from './RecordDate';
import RecordReceipt from './RecordReceipt';
import Summary from './Summary';
import ExtendedItems from './ExtendedItem';
import RecordItemsArea from './RecordItemsArea';
import RecordJob from './Job';
import RecordCostCenter from './CostCenter';

import './index.scss';

const ROOT = 'ts-expenses-requests';

type Props = {
  isExpenseRequest?: boolean,
  isFinanceApproval?: boolean,
  isChildRecord?: boolean,
  recordItemIdx: number,
  // ui states
  baseCurrencyCode: string,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  errors: { recordDate?: string, records?: Array<*> },
  expReport: Report,
  customHint: CustomHint,
  mode: string,
  overlap: { report: boolean, record: boolean },
  readOnly: boolean,
  recordIdx: number,
  touched: { recordDate?: string, records?: Array<*> },
  fixedAmountOptionList: Array<AmountOption>,
  // event handlers
  onClickHideRecordButton: () => void,
  updateReport: (string, any) => void,
  onClickRecordItemsCreateButton: () => void,
  onClickRecordItemsConfirmButton: () => void,
  onClickRecordItemsDeleteButton: () => void,
  onClickResetRouteInfoButton: () => void,
  onClickSaveButton: () => void,
  onClickJobBtn: () => void,
  onClickCostCenterBtn: () => void,
  onClickLookupEISearch: (item: EISearchObj) => void,
  resetRouteForm: (?RouteInfo) => void,
  reportEdit: () => void,
  onChangeRecordDate: (value: string) => void,
  onChangeAmountSelection: (value: string) => void,
  // receipt-library
  onClickOpenLibraryButton: () => void,
  onImageDrop: (files: File) => void,
  getFilePreview: (receiptFileId: string) => Promise<any>,
  // Formik
  setFieldError: (string, any) => void,
  setFieldTouched: (string, {} | boolean, ?boolean) => void,
  // Components
  baseCurrency: any,
  foreignCurrency: any,
  routeForm: any,
  suggest: any,
  tempSavedRecordItems: Array<Record>,
} & ChildRecordItemProps;

export default class RecordItem extends React.Component<Props> {
  render() {
    const {
      recordIdx,
      isFinanceApproval,
      isExpenseRequest,
      expReport,
      tempSavedRecordItems,
      baseCurrencyDecimal,
      baseCurrencySymbol,
      isChildRecord,
      recordItemIdx = 0,
      customHint,
    } = this.props;

    let expRecord;
    let errors;
    let touched;
    const navTitle = isFinanceApproval
      ? msg().Exp_Lbl_Records
      : msg().Exp_Btn_NewRecord;
    const targetRecord = `records[${this.props.recordIdx}]`;
    if (recordIdx === -1) {
      expRecord = newRecord('', '', RECORD_TYPE.General);
      errors = {};
      touched = {};
    } else {
      expRecord = get(expReport, targetRecord);
      errors = get(this.props.errors, targetRecord) || {};
      touched = get(this.props.touched, targetRecord) || {};
    }

    if (!expRecord) {
      return null;
    }

    // Itemization
    const isItemized = isRecordItemized(expRecord.recordType, isChildRecord);
    const hasChildItems =
      tempSavedRecordItems && tempSavedRecordItems.length > 1;
    const useForeignCurrency = get(expRecord, 'items.0.useForeignCurrency');
    const tempSavedChildItems = drop(tempSavedRecordItems);
    const decimalPlaces = useForeignCurrency
      ? get(expRecord, 'items.0.currencyInfo.decimalPlaces', 0)
      : baseCurrencyDecimal;
    const key = useForeignCurrency ? 'localAmount' : 'amount';
    const childTotalAmount = calcItemsTotalAmount(
      tempSavedChildItems,
      key,
      decimalPlaces
    );
    const parentAmount = useForeignCurrency
      ? get(expRecord, 'items.0.localAmount')
      : expRecord.amount;
    const isMatch = isAmountMatch(childTotalAmount, parentAmount);
    const hasRIErrors = isItemized && (!hasChildItems || !isMatch);

    let contentsCss = recordIdx === -1 ? 'disabled' : '';
    contentsCss += isItemized && !hasChildItems ? ' to-be-itemized' : '';
    const contentsDisabled = this.props.readOnly || recordIdx === -1;

    // Receipt Logic
    const { fileAttachment } = expRecord;
    const isReceiptOptional = fileAttachment === RECEIPT_TYPE.Optional;
    const isReceiptDisabled = fileAttachment === RECEIPT_TYPE.NotUsed;

    const canRenderReceipt =
      isFinanceApproval && !isReceiptOptional
        ? !!expRecord.receiptFileId
        : !isReceiptDisabled;

    // End Receipt Logic

    const isDisabled =
      expReport.status === status.ACCOUNTING_AUTHORIZED ||
      expReport.status === status.ACCOUNTING_REJECTED ||
      expReport.status === status.JOURNAL_CREATED ||
      expReport.status === status.FULLY_PAID;

    const hasReportId = expReport.reportId;
    const hasPreRequestId = expReport.preRequestId;
    const isNewReportFromPreRequest = hasPreRequestId && !hasReportId;

    // Fixed Allownance
    const isMultiFixedAllowance = isFixedAllowanceMulti(expRecord.recordType);
    const isSingleFixedAllowance = isFixedAllowanceSingle(expRecord.recordType);
    const isFixedAllowance = isSingleFixedAllowance || isMultiFixedAllowance;
    const fixedAmountMessage = isSingleFixedAllowance
      ? msg().Exp_Hint_FixedAllownceSingle
      : msg().Exp_Hint_FixedAllowanceMulti;

    // multi-amount option
    const expTypeId = get(expRecord, 'items.0.expTypeId');
    let currencySymbol = baseCurrencySymbol;
    if (useForeignCurrency) {
      const foreignCurrencySymbol = get(
        expRecord,
        'items.0.currencyInfo.symbol'
      );
      currencySymbol = foreignCurrencySymbol || '';
    }
    const optionList = get(
      this.props.fixedAmountOptionList,
      `${expTypeId}`,
      []
    );
    const recordId = expRecord.recordId;

    const amountOptions = optionList.map((item) => ({
      id: item.id,
      text: `${item.label} ${currencySymbol}${item.allowanceAmount}`,
    }));

    if (isEmpty(recordId)) {
      amountOptions.unshift({
        id: '',
        text: msg().Exp_Lbl_PleaseSelect,
      });
    }
    // multi-amount value
    const amountValue = get(expRecord, 'items.0.fixedAllowanceOptionId', '');
    const amountError = get(
      this.props.errors,
      `records[${this.props.recordIdx}]items.0.fixedAllowanceOptionId`,
      ''
    );

    // Job
    const isShowJob = expReport.jobId;

    // Cost Center
    const isShowCostCenter = expReport.costCenterHistoryId;

    let contents = null;
    switch (expRecord.recordType) {
      case 'General':
      case 'HotelFee':
      case 'FixedAllowanceSingle':
      case 'FixedAllowanceMulti':
        contents = (
          <General
            expRecord={expRecord}
            targetRecord={targetRecord}
            recordIdx={this.props.recordIdx}
            onChangeEditingExpReport={this.props.updateReport}
            readOnly={contentsDisabled}
            errors={errors}
            touched={touched}
            baseCurrencyCode={this.props.baseCurrencyCode}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            baseCurrency={this.props.baseCurrency}
            foreignCurrency={this.props.foreignCurrency}
            isItemized={isItemized}
            isFixedAllowance={isFixedAllowance}
            fixedAmountMessage={(isFixedAllowance && fixedAmountMessage) || ''}
            hasChildItems={hasChildItems}
            recordItemIdx={recordItemIdx}
          />
        );
        break;
      case 'TransitJorudanJP':
        contents = (
          <TransitJorudanJP
            expRecord={expRecord}
            targetRecord={targetRecord}
            onChangeEditingExpReport={this.props.updateReport}
            readOnly={contentsDisabled}
            errors={errors}
            touched={touched}
            onClickResetRouteInfoButton={this.props.onClickResetRouteInfoButton}
            resetRouteForm={this.props.resetRouteForm}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            routeForm={this.props.routeForm}
            suggest={this.props.suggest}
          />
        );
        break;
      default:
        break;
    }

    if (isChildRecord) {
      return (
        <ChildRecordItem
          expRecord={expRecord}
          recordIdx={recordIdx}
          recordItemIdx={this.props.recordItemIdx}
          onChangeChildDateOrTypeForBC={this.props.onChangeChildDateOrTypeForBC}
          onChangeChildExpTypeForFC={this.props.onChangeChildExpTypeForFC}
          onChangeChildDateForFC={this.props.onChangeChildDateForFC}
          onChangeEditingExpReport={this.props.updateReport}
          errors={this.props.errors}
          touched={this.props.touched}
          baseCurrencyCode={this.props.baseCurrencyCode}
          baseCurrencySymbol={this.props.baseCurrencySymbol}
          baseCurrencyDecimal={this.props.baseCurrencyDecimal}
          baseCurrency={this.props.baseCurrency}
          foreignCurrency={this.props.foreignCurrency}
          handleClickDeleteBtn={this.props.handleClickDeleteBtn}
          expTypesDisplay={this.props.expTypesDisplay}
          onClickLookupEISearch={this.props.onClickLookupEISearch}
          isFinanceApproval={isFinanceApproval}
          customHint={customHint}
        />
      );
    }

    return (
      <div className="ts-expenses-requests slds">
        <section className={`${ROOT}-header`}>
          <div className={`${ROOT}-nav-title`}>
            <span>{navTitle}</span>
          </div>
          <ActionButtons
            mode={this.props.mode}
            isDisabled={isDisabled}
            isNewReportFromPreRequest={!!isNewReportFromPreRequest}
            isFinanceApproval={isFinanceApproval}
            onClickEditButton={this.props.reportEdit}
            onClickBackButton={this.props.onClickHideRecordButton}
            onClickSaveButton={this.props.onClickSaveButton}
            hasRIErrors={hasRIErrors}
          />
        </section>

        <div className={`${ROOT}__contents ${contentsCss}`}>
          <RecordDate
            recordDate={expRecord.recordDate}
            targetRecord={targetRecord}
            hintMsg={
              isExpenseRequest
                ? customHint.requestRecordDate
                : customHint.recordDate
            }
            isExpenseRequest={isExpenseRequest}
            onChangeRecordDate={this.props.onChangeRecordDate}
            readOnly={this.props.readOnly}
            errors={this.props.errors}
            touched={this.props.touched}
          />
          <MultiColumnsGrid sizeList={[6, 6]}>
            <TextField
              className={`${ROOT}__contents__expense-type`}
              value={expRecord.items[0].expTypeName}
              label={msg().Exp_Clbl_ExpenseType}
              isRequired
              disabled
            />
            {isMultiFixedAllowance && (
              <AmountSelection
                readOnly={this.props.readOnly}
                value={amountValue}
                onChangeAmountSelection={(e: any) =>
                  this.props.onChangeAmountSelection(e.target.value)
                }
                error={msg()[amountError]}
                options={amountOptions || []}
              />
            )}
          </MultiColumnsGrid>
          {contents}

          {canRenderReceipt && (
            <RecordReceipt
              isFinanceApproval={isFinanceApproval}
              isExpenseRequest={isExpenseRequest}
              expRecord={expRecord}
              hintMsg={
                isExpenseRequest
                  ? customHint.recordQuotation
                  : customHint.recordReceipt
              }
              recordType={expRecord.recordType}
              fileAttachment={expRecord.fileAttachment}
              targetRecord={targetRecord}
              onChangeEditingExpReport={this.props.updateReport}
              readOnly={this.props.readOnly}
              errors={errors}
              setFieldError={this.props.setFieldError}
              setFieldTouched={this.props.setFieldTouched}
              touched={touched}
              onClickOpenLibraryButton={this.props.onClickOpenLibraryButton}
              onImageDrop={this.props.onImageDrop}
              getFilePreview={this.props.getFilePreview}
            />
          )}
          {isItemized && (
            <RecordItemsArea
              expRecord={expRecord}
              baseCurrencySymbol={this.props.baseCurrencySymbol}
              baseCurrencyDecimal={this.props.baseCurrencyDecimal}
              readOnly={this.props.readOnly}
              isFinanceApproval={isFinanceApproval}
              hasChildItems={hasChildItems}
              isAmountMatch={isMatch}
              onClickRecordItemsCreateButton={
                this.props.onClickRecordItemsCreateButton
              }
              onClickRecordItemsConfirmButton={
                this.props.onClickRecordItemsConfirmButton
              }
              onClickRecordItemsDeleteButton={
                this.props.onClickRecordItemsDeleteButton
              }
            />
          )}
          {isShowJob && (
            <RecordJob
              expReport={expReport}
              expRecord={expRecord}
              readOnly={this.props.readOnly}
              hintMsg={customHint.recordJob}
              handleClickJobBtn={this.props.onClickJobBtn}
            />
          )}
          {isShowCostCenter && (
            <RecordCostCenter
              expReport={expReport}
              expRecord={expRecord}
              readOnly={this.props.readOnly}
              hintMsg={customHint.recordCostCenter}
              handleClickCostCenterBtn={this.props.onClickCostCenterBtn}
            />
          )}
          {!isItemized && (
            <ExtendedItems
              recordItem={expRecord.items[0]}
              onClickLookupEISearch={this.props.onClickLookupEISearch}
              onChangeEditingExpReport={this.props.updateReport}
              readOnly={this.props.readOnly}
              targetRecordItem={`${targetRecord}.items.0`}
              errors={this.props.errors}
              touched={this.props.touched}
            />
          )}
          <Summary
            value={expRecord.items[0].remarks}
            hintMsg={customHint.recordSummary}
            onChangeEditingExpReport={this.props.updateReport}
            readOnly={this.props.readOnly}
            targetRecord={`${targetRecord}.items.${recordItemIdx}`}
            errors={this.props.errors}
            touched={this.props.touched}
          />
        </div>
      </div>
    );
  }
}
