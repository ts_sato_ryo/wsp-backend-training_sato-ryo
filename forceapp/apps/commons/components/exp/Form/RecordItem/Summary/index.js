// @flow
import React from 'react';
import _ from 'lodash';

import msg from '../../../../../languages';

import TextAreaField from '../../../../fields/TextAreaField';
import LabelWithHint from '../../../../fields/LabelWithHint';

const ROOT = 'ts-expenses__form-record-item-summary';
type Props = {
  onChangeEditingExpReport: (string, any) => void,
  readOnly: boolean,
  value: string,
  targetRecord: string,
  hintMsg?: string,
  errors: { recordDate?: string, records?: Array<*> },
  touched: { recordDate?: string, records?: Array<*> },
};

export default class Summary extends React.Component<Props> {
  render() {
    const {
      errors,
      touched,
      value,
      onChangeEditingExpReport,
      targetRecord,
      hintMsg,
    } = this.props;

    const remarksError = _.get(errors, `${targetRecord}.remarks`);
    const isRemarksTouched = _.get(touched, `${targetRecord}.remarks`);

    return (
      <div className={`${ROOT} ts-text-field-container`}>
        <LabelWithHint
          text={msg().Exp_Clbl_Summary}
          hintMsg={(!this.props.readOnly && hintMsg) || ''}
        />
        <TextAreaField
          value={value || ''}
          onChange={(e: SyntheticInputEvent<HTMLInputElement>) =>
            onChangeEditingExpReport(`${targetRecord}.remarks`, e.target.value)
          }
          disabled={this.props.readOnly}
        />
        {remarksError && isRemarksTouched && (
          <div className="value">
            <div className="input-feedback">{msg()[remarksError]}</div>
          </div>
        )}
      </div>
    );
  }
}
