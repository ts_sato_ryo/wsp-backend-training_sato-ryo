// @flow
import React from 'react';

import ActionButtonsExpense from './Expense';
import ActionButtonsFA from './FinanceApproval';

import { modes } from '../../../../../../requests-pc/modules/ui/expenses/mode';

export type Props = {
  mode: string,
  isDisabled: boolean,
  isFinanceApproval?: boolean,
  isNewReportFromPreRequest: boolean,
  hasRIErrors: boolean,
  onClickEditButton: () => void,
  onClickBackButton: () => void,
  onClickSaveButton: () => void,
};

export default class RecordItemActionButtons extends React.Component<Props> {
  isMode(mode: string) {
    return this.props.mode === modes[mode];
  }

  render() {
    const { isFinanceApproval } = this.props;
    const isEditMode = this.isMode('REPORT_EDIT');
    const isSaveDisabled =
      this.isMode('INITIALIZE') ||
      this.isMode('REPORT_SELECT') ||
      this.props.hasRIErrors;
    const isExpenseSaveDisabled =
      isSaveDisabled || this.props.isNewReportFromPreRequest;

    return isFinanceApproval ? (
      <ActionButtonsFA
        isEditMode={isEditMode}
        isSaveDisabled={isSaveDisabled}
        isDisabled={this.props.isDisabled}
        onClickEditButton={this.props.onClickEditButton}
        onClickBackButton={this.props.onClickBackButton}
        onClickSaveButton={this.props.onClickSaveButton}
      />
    ) : (
      <ActionButtonsExpense
        isSaveDisabled={isExpenseSaveDisabled}
        onClickSaveButton={this.props.onClickSaveButton}
        onClickBackButton={this.props.onClickBackButton}
      />
    );
  }
}
