// @flow
import { cloneDeep, set } from 'lodash';
import React from 'react';

import msg from '../../../../../languages';
import FileUtil from '../../../../../utils/FileUtil';

import Lightbox from '../../../../Lightbox';
import Button from '../../../../buttons/Button';

import pdfIcon from '../../../../../images/pdfIcon.png';

import Dropzone from '../../../../fields/Dropzone';
import LabelWithHint from '../../../../fields/LabelWithHint';
import './index.scss';

import {
  RECORD_TYPE,
  RECEIPT_TYPE,
  type Record,
} from '../../../../../../domain/models/exp/Record';

type Props = {
  isExpenseRequest?: boolean,
  isFinanceApproval?: boolean,
  onChangeEditingExpReport: (string, any, any, any) => void,
  onClickOpenLibraryButton: () => void,
  expRecord: Record,
  hintMsg?: string,
  targetRecord: string,
  readOnly: boolean,
  recordType: string,
  fileAttachment: string,
  errors: { receiptId?: string },
  touched: { receiptId?: string },
  setFieldError: (string, any) => void,
  setFieldTouched: (string, {} | boolean, ?boolean) => void,
  onImageDrop: (files: File) => void,
  getFilePreview: (receiptFileId: string) => Promise<any>,
};

const ROOT = 'record-receipt';
const ALLOWED_MIME_TYPES = 'image/*, application/pdf';
const MAX_FILE_SIZE = 5242880;

export default class RecordReceipt extends React.Component<Props> {
  componentDidMount() {
    if (
      this.props.expRecord.receiptFileId &&
      (!this.props.expRecord.receiptData ||
        this.props.expRecord.fileName === 'receipt' ||
        !this.props.expRecord.dataType)
    ) {
      this.props.getFilePreview(this.props.expRecord.receiptFileId);
    }
  }

  updateRecord(updateObj: any) {
    const tmpRecord = cloneDeep(this.props.expRecord);
    const tmpTouched = cloneDeep(this.props.touched);

    Object.keys((updateObj: any)).forEach((key) => {
      set(tmpRecord, key, updateObj[key]);
      set(tmpTouched, key, true);
    });

    this.props.onChangeEditingExpReport(
      this.props.targetRecord,
      tmpRecord,
      false,
      tmpTouched
    );
  }

  handleDropAccepted = (files: File[]) => {
    this.props.onImageDrop(files[0]);
  };

  handleDropRejected = (rejectedFiles: any) => {
    const { setFieldError, setFieldTouched, targetRecord } = this.props;
    const rejectedFile = rejectedFiles[0];
    let error = 'Common_Err_Required';

    if (rejectedFile.size > MAX_FILE_SIZE) {
      error = 'Common_Err_MaxFileSize';
    }

    setFieldError(`report.${targetRecord}.receiptId`, error);
    setFieldTouched(`report.${targetRecord}.receiptId`, true, false);
  };

  handleDeleteFile = () => {
    this.updateRecord({
      receiptId: null,
      receiptFileId: null,
      receiptData: null,
    });
  };

  isRequired() {
    const { fileAttachment, isExpenseRequest } = this.props;
    const isNotRequired = fileAttachment !== RECEIPT_TYPE.Required;
    return !(isNotRequired || isExpenseRequest);
  }

  renderFileInput = (receiptFileId: ?string) => {
    const { isFinanceApproval, fileAttachment } = this.props;
    const isReceiptOptional = fileAttachment === RECEIPT_TYPE.Optional;
    let container =
      isFinanceApproval && isReceiptOptional ? msg().Com_Lbl_NoReceipt : '';

    if (!isFinanceApproval && !receiptFileId) {
      container = (
        <Dropzone
          className={`${ROOT}__file`}
          accept={ALLOWED_MIME_TYPES}
          onDropAccepted={this.handleDropAccepted}
          onDropRejected={this.handleDropRejected}
          disabled={this.props.readOnly}
          multiple={false}
          maxSize={MAX_FILE_SIZE}
        >
          <div className={`${ROOT}__file-label`}>
            <p>{msg().Exp_Lbl_DragAndDrop}</p>
            <button type="button" className={`${ROOT}__button--file ts-button`}>
              {msg().Exp_Lbl_ChooseFile}
            </button>
          </div>
        </Dropzone>
      );
    }
    if (this.props.expRecord.receiptData) {
      const url = this.props.expRecord.receiptData;
      const isPDF = this.props.expRecord.dataType === 'application/pdf';
      const fileName = this.props.expRecord.fileName;

      container = (
        <React.Fragment>
          {isPDF ? (
            <div className={`${ROOT}__pdf-download-btn`}>
              <Button
                onClick={() =>
                  FileUtil.downloadFile(
                    url,
                    this.props.expRecord.receiptId,
                    `${fileName}.pdf`,
                    true
                  )
                }
                className={`${ROOT}__pdf-download-link`}
              >
                <img
                  alt="pdf-icon"
                  src={pdfIcon}
                  className={`${ROOT}__pdf-icon`}
                />
              </Button>
            </div>
          ) : (
            <Lightbox>
              <img alt="Receipt" className="lightbox-img-thumbnail" src={url} />
            </Lightbox>
          )}
          {!this.props.readOnly && !this.props.isFinanceApproval && (
            <button
              type="button"
              className={`${ROOT}__button--delete ts-button`}
              onClick={this.handleDeleteFile}
              disabled={!this.props.expRecord.receiptFileId}
            >
              {msg().Exp_Lbl_DeleteFile}
            </button>
          )}
        </React.Fragment>
      );
    }

    return container;
  };

  render() {
    const {
      errors,
      touched,
      recordType,
      expRecord,
      fileAttachment,
      hintMsg,
    } = this.props;

    const isNotOptional = fileAttachment !== RECEIPT_TYPE.Optional;

    const isReceiptAllowedByExpType = (): boolean => {
      switch (recordType) {
        case RECORD_TYPE.General:
        case RECORD_TYPE.HotelFee:
        case RECORD_TYPE.FixedAllowanceSingle:
          return true;
        default:
          return false;
      }
    };

    if (!isReceiptAllowedByExpType && isNotOptional) {
      return null;
    }

    const attachmentError = errors.receiptId;
    const isAttachmentTouched = touched.receiptId || false;
    const attachmentLabel = this.props.isExpenseRequest
      ? msg().Exp_Lbl_Quotation
      : msg().Exp_Lbl_Receipt;
    return (
      <div className={`${ROOT}`}>
        <LabelWithHint
          text={attachmentLabel}
          hintMsg={
            (!this.props.readOnly &&
              !this.props.isFinanceApproval &&
              hintMsg) ||
            ''
          }
          isRequired={this.isRequired()}
        />
        <div className={`${ROOT}__input`}>
          {this.renderFileInput(expRecord.receiptFileId)}
          {!this.props.isExpenseRequest && !this.props.isFinanceApproval && (
            <div>
              <button
                type="button"
                className={`${ROOT}__button--file ts-button`}
                onClick={this.props.onClickOpenLibraryButton}
                disabled={this.props.readOnly}
              >
                {msg().Exp_Lbl_SelectReceiptLibrary}
              </button>
            </div>
          )}
          {attachmentError && isAttachmentTouched && (
            <div className="value">
              <div className="input-feedback">{msg()[attachmentError]}</div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
