// @flow
import React from 'react';
import _ from 'lodash';

import msg from '../../../../../languages';
import DateField from '../../../../fields/DateField';
import LabelWithHint from '../../../../fields/LabelWithHint';

type Props = {
  recordDate: string,
  hintMsg?: string,
  targetRecord: string,
  readOnly: boolean,
  errors: { records?: any },
  touched: { records?: any },
  onChangeRecordDate: (value: string) => void,
};

export default class RecordDate extends React.Component<Props> {
  render() {
    const { errors, touched, targetRecord, hintMsg } = this.props;

    const recordDateError = _.get(errors, `${targetRecord}.recordDate`);
    const isRecordDateTouched = _.get(touched, `${targetRecord}.recordDate`);
    return (
      <div className="ts-text-field-container">
        <LabelWithHint
          text={msg().Exp_Clbl_Date}
          hintMsg={(!this.props.readOnly && hintMsg) || ''}
          isRequired
        />
        <DateField
          value={this.props.recordDate}
          onChange={this.props.onChangeRecordDate}
          disabled={this.props.readOnly}
        />
        {recordDateError && isRecordDateTouched && (
          <div className="input-feedback">{msg()[recordDateError]}</div>
        )}
      </div>
    );
  }
}
