// @flow
import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import msg from '../../../../../../languages';
import Button from '../../../../../buttons/Button';
import IconButton from '../../../../../buttons/IconButton';

import type { StationInfo } from '../../../../../../../domain/models/exp/jorudan/Station';
import type { RouteInfo } from '../../../../../../../domain/models/exp/Record';

import btnClose from '../../../../../../images/btnDetailOpen.png';
import btnOpen from '../../../../../../images/btnDetailClose.png';

import Via from './Via';
import Condition from './Condition';
import AddViaButton from './AddViaButton';
import RouteOption from './RouteOption';
import Radio from './Radio';

import './index.scss';

const ROOT = 'ts-route-form';

type Props = {
  readOnly: boolean,
  roundTrip: string,
  onChangeRoundTrip: (string) => void,
  onChangeOrigin: (StationInfo) => void,
  onChangeViaList: (StationInfo, number) => void,
  onChangeTmpOrigin: (string, boolean) => void,
  onChangeTmpViaList: (string, number, boolean) => void,
  onChangeArrival: (StationInfo) => void,
  onChangeTmpArrival: (string, boolean) => void,
  useChargedExpress: string,
  useExReservation: string,
  onChangeUseChargedExpress: (string) => void,
  onChangeUseExReservation: (string) => void,
  routeSort: string,
  onChangeRouteSort: (string) => void,
  seatPreference: string,
  onChangeSeatPreference: (string) => void,
  highwayBus: string,
  jorudanUseChargedExpress: string,
  onClickSearchRouteButton: () => void,
  onClickAddViaButton: () => void,
  onChangeHighwayBus: (string) => void,
  onClickDeleteViaButton: (number) => void,
  targetDate: string,
  tmpArrival: string,
  tmpViaList: Array<string>,
  tmpOrigin: string,
  errorOrigin: string,
  errorViaList: Array<string>,
  errorArrival: string,
  routeInfo: RouteInfo,
  errors: { recordDate?: string },
  touched: { recordDate?: string },
  // components
  suggest: any,
  language: string,
};

type State = {
  isOpen: boolean,
  inputOrigin: string,
  inputViaList: Array<string>,
  inputArrival: string,
};

export default class RouteForm extends React.Component<Props, State> {
  state = {
    isOpen: false,
    inputOrigin: '',
    inputViaList: [],
    inputArrival: '',
  };

  componentDidMount() {
    this.setState({
      inputOrigin: this.props.tmpOrigin || '',
      inputArrival: this.props.tmpArrival || '',
      inputViaList: this.props.tmpViaList || [],
    });
  }

  componentWillReceiveProps(nextProps: Props) {
    if (!_.isEqual(this.props.tmpOrigin, nextProps.tmpOrigin)) {
      this.setState({ inputOrigin: nextProps.tmpOrigin });
    }
    if (!_.isEqual(this.props.tmpArrival, nextProps.tmpArrival)) {
      this.setState({ inputArrival: nextProps.tmpArrival });
    }
    if (!_.isEqual(this.props.tmpViaList, nextProps.tmpViaList)) {
      this.setState({ inputViaList: nextProps.tmpViaList });
    }
  }

  onClickToggleButton = () => {
    this.setState((prevState) => ({ isOpen: !prevState.isOpen }));
  };

  onClickAddViaButton = () => {
    this.setState((prevState) => {
      const newViaList = [...prevState.inputViaList];
      newViaList.push('');
      return {
        inputViaList: newViaList,
      };
    });
    this.props.onClickAddViaButton();
  };

  onClickDeleteViaButton = (idx: number) => {
    this.setState((prevState) => {
      const newViaList = [...prevState.inputViaList];
      newViaList.splice(idx, 1);
      return {
        inputViaList: newViaList,
      };
    });
    this.props.onClickDeleteViaButton(idx);
  };

  onChangeTmpOrigin = (value: string, isClear: boolean) => {
    this.setState({ inputOrigin: value });
    this.props.onChangeTmpOrigin(value, isClear);
  };

  onChangeTmpViaList = (value: string, idx: number, isClear: boolean) => {
    this.setState((prevState) => {
      const newViaList = prevState.inputViaList;
      newViaList[idx] = value;
      return {
        inputViaList: newViaList,
      };
    });
    this.props.onChangeTmpViaList(value, idx, isClear);
  };

  onChangeTmpArrival = (value: string, isClear: boolean) => {
    this.setState({ inputArrival: value });
    this.props.onChangeTmpArrival(value, isClear);
  };

  render() {
    const roundTrip = [
      { key: false, value: msg().Exp_Lbl_RouteOptionOneWay },
      { key: true, value: msg().Exp_Lbl_RouteOptionRoundTrip },
    ];
    const useChargedExpress = [
      { key: '0', value: msg().Exp_Lbl_RouteOptionUseChargedExpress_Use },
      { key: '1', value: msg().Exp_Lbl_RouteOptionUseChargedExpress_DoNotUse },
    ];
    const useExReservation = [
      { key: '1', value: msg().Exp_Lbl_RouteOptionUseExReservation_Use },
      { key: '0', value: msg().Exp_Lbl_RouteOptionUseExReservation_NotUse },
    ];
    const routeSort = [
      { key: '0', value: msg().Exp_Lbl_RouteOptionRouteSort_TimeRequired },
      { key: '1', value: msg().Exp_Lbl_RouteOptionRouteSort_Cheap },
      { key: '2', value: msg().Exp_Lbl_RouteOptionRouteSort_NumberOfTransfers },
    ];
    const seatPreference = [
      { key: '0', value: msg().Exp_Lbl_RouteOptionSeatPreference_ReservedSeat },
      { key: '1', value: msg().Exp_Lbl_RouteOptionSeatPreference_FreeSeat },
      { key: '2', value: msg().Exp_Lbl_RouteOptionSeatPreference_GreenSeat },
    ];
    const highwayBus = [
      { key: '0', value: msg().Exp_Lbl_RouteOptionHighWayBusUse },
      { key: '1', value: msg().Exp_Lbl_RouteOptionHighWayBusNotUse },
    ];

    const readOnly =
      this.props.readOnly || !!this.props.routeInfo.selectedRoute;
    const hrClass = classNames(`${ROOT}-hr`, {
      [`${ROOT}-hr--hidden`]: readOnly,
    });
    const searchNameLabelClass = classNames(`${ROOT}-snLabel`, {
      [`${ROOT}-snLabel--hidden`]: readOnly,
    });
    const searchClass = classNames(`${ROOT}-search`, {
      [`${ROOT}-search--hidden`]: readOnly,
    });
    const optionGridClass = classNames(`${ROOT}-option-grid`, {
      [`${ROOT}-option-grid--hidden`]: readOnly,
    });

    const { errors, touched } = this.props;
    const selectedRouteError = _.get(errors, 'routeInfo.selectedRoute');
    const isSelectedRouteTouched = _.get(touched, 'routeInfo.selectedRoute');

    return (
      <div className={ROOT}>
        <div className={`${ROOT}-round-trip`}>
          <Radio
            name="roundTrip"
            items={roundTrip}
            checked={this.props.roundTrip}
            onChange={this.props.onChangeRoundTrip}
            readOnly={this.props.readOnly}
          />
        </div>
        <Condition
          title={msg().Exp_Lbl_DepartFrom}
          inputType="origin"
          placeholder={msg().Exp_Lbl_RoutePlaceholder}
          onChange={this.props.onChangeOrigin}
          onChangeTmp={this.onChangeTmpOrigin}
          readOnly={this.props.readOnly || !!this.props.routeInfo.selectedRoute}
          error={this.props.errorOrigin}
          value={this.state.inputOrigin}
          targetDate={this.props.targetDate}
          suggest={this.props.suggest}
        />
        <Via
          onChange={this.props.onChangeViaList}
          onChangeTmp={this.onChangeTmpViaList}
          readOnly={this.props.readOnly || !!this.props.routeInfo.selectedRoute}
          error={this.props.errorViaList}
          tmpViaList={this.state.inputViaList}
          targetDate={this.props.targetDate}
          suggest={this.props.suggest}
          withDelete={
            !(this.props.readOnly || !!this.props.routeInfo.selectedRoute)
          }
          onDeleteVia={this.onClickDeleteViaButton}
        />
        <AddViaButton
          onClickAddViaButton={this.onClickAddViaButton}
          readOnly={readOnly}
          tmpViaList={this.props.tmpViaList}
        />

        <Condition
          title={msg().Exp_Lbl_Destination}
          inputType="arrival"
          placeholder={msg().Exp_Lbl_RoutePlaceholder}
          onChange={this.props.onChangeArrival}
          onChangeTmp={this.onChangeTmpArrival}
          readOnly={this.props.readOnly || !!this.props.routeInfo.selectedRoute}
          error={this.props.errorArrival}
          value={this.state.inputArrival}
          targetDate={this.props.targetDate}
          suggest={this.props.suggest}
        />
        {selectedRouteError && isSelectedRouteTouched && (
          <div className={`${ROOT}-route-selected-error`}>
            {msg()[selectedRouteError]}
          </div>
        )}

        <div className={searchClass}>
          <Button
            className={`${ROOT}-search-button`}
            type="primary"
            onClick={this.props.onClickSearchRouteButton}
            readOnly={readOnly}
          >
            {msg().Exp_Btn_RouteSearch}
          </Button>
        </div>
        <hr className={hrClass} />
        <div className={searchNameLabelClass}>
          <IconButton
            src={this.state.isOpen ? btnOpen : btnClose}
            onClick={this.onClickToggleButton}
            className={`${ROOT}-option-grid-toggle-button`}
          />
          {msg().Exp_Lbl_RouteSearchCondition}
        </div>
        {this.state.isOpen && (
          <div>
            <div className={optionGridClass}>
              <div className={`${ROOT}-option-grid-displayOrder`}>
                <RouteOption
                  title={msg().Exp_Lbl_RouteOptionRouteSort}
                  name="routeSort"
                  items={routeSort}
                  checked={this.props.routeSort}
                  onChange={this.props.onChangeRouteSort}
                  readOnly={readOnly}
                />
              </div>
              <div className={`${ROOT}-option-grid-highwayBus`}>
                <RouteOption
                  title={msg().Exp_Lbl_RouteOptionHighWayBus}
                  name="highwayBus"
                  items={highwayBus}
                  checked={this.props.highwayBus}
                  onChange={this.props.onChangeHighwayBus}
                  readOnly={readOnly}
                />
              </div>
            </div>

            <RouteOption
              title={msg().Exp_Lbl_RouteOptionSeatPreference}
              name="seatPreference"
              items={seatPreference}
              checked={this.props.seatPreference}
              onChange={this.props.onChangeSeatPreference}
              readOnly={readOnly}
            />

            {this.props.jorudanUseChargedExpress === '2' ? null : (
              <RouteOption
                title={msg().Exp_Lbl_RouteOptionUseChargedExpress}
                name="useChargedExpress"
                items={useChargedExpress}
                checked={this.props.useChargedExpress}
                onChange={this.props.onChangeUseChargedExpress}
                readOnly={readOnly}
              />
            )}
            {this.props.language === 'ja' && ( // EX Reservation is supported only in Japanese
              <RouteOption
                title={msg().Exp_Lbl_RouteOptionUseExReservation}
                name="useExReservation"
                items={useExReservation}
                checked={this.props.useExReservation}
                onChange={this.props.onChangeUseExReservation}
                readOnly={readOnly}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}
