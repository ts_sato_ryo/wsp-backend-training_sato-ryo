/* @flow */
import React from 'react';
import _ from 'lodash';

import msg from '../../../../../languages';

import type {
  Record,
  RouteInfo,
} from '../../../../../../domain/models/exp/Record';

import Button from '../../../../buttons/Button';

import Amount from './Amount';
import RouteMap from './RouteMap';

type Props = {
  // ui states
  expRecord: Record,
  targetRecord: string,
  readOnly: boolean,
  baseCurrencySymbol: string,
  // event handlers
  onChangeEditingExpReport: (string, any) => void,
  onClickResetRouteInfoButton: () => void,
  resetRouteForm: (?RouteInfo) => void,
  // Formik
  errors: { recordDate?: string },
  touched: { recordDate?: string },
  // components
  routeForm: any,
  suggest: any,
};

const ROOT = 'ts-expenses-requests__contents__form__transit';

export default class TransitJorudanJP extends React.Component<Props> {
  componentWillMount() {
    this.props.resetRouteForm(this.props.expRecord.routeInfo);
  }

  componentWillReceiveProps(nextProps: Props) {
    if (!_.isEqual(this.props.targetRecord, nextProps.targetRecord)) {
      nextProps.resetRouteForm(nextProps.expRecord.routeInfo);
    }
  }

  render() {
    const {
      expRecord,
      readOnly,
      baseCurrencySymbol,
      targetRecord,
      onChangeEditingExpReport,
    } = this.props;

    const RouteFormContainer = this.props.routeForm;
    return (
      <div className={`${ROOT} ts-text-field-container`}>
        <div className="key">{msg().Com_Lbl_Route}</div>
        <RouteFormContainer
          targetDate={expRecord.recordDate}
          targetRecord={targetRecord}
          readOnly={readOnly}
          routeInfo={expRecord.routeInfo}
          onChangeEditingExpReport={onChangeEditingExpReport}
          errors={this.props.errors}
          touched={this.props.touched}
          suggest={this.props.suggest}
          expRecord={this.props.expRecord}
        />
        {expRecord.routeInfo && expRecord.routeInfo.selectedRoute && (
          <Amount
            amount={expRecord.items[0].amount}
            baseCurrencySymbol={baseCurrencySymbol}
          />
        )}
        <RouteMap routeInfo={expRecord.routeInfo} />
        {expRecord.routeInfo && expRecord.routeInfo.selectedRoute && !readOnly && (
          <Button
            type="text"
            className={`${ROOT}-route-reset`}
            onClick={this.props.onClickResetRouteInfoButton}
          >
            {msg().Exp_Lbl_RouteInfoResetButton}
          </Button>
        )}
      </div>
    );
  }
}
