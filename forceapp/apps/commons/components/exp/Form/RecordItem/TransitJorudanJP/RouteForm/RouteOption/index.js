// @flow
import React from 'react';

import Radio from '../Radio';

import './index.scss';

type Props = {
  title: string,
  name: string,
  items: Array<{ key: string, value: string }>,
  checked: string,
  onChange: (string) => void,
  readOnly: boolean,
};

const ROOT = 'ts-route-form__options';

export default class RouteOption extends React.Component<Props> {
  render() {
    if (this.props.readOnly) {
      return null;
    }

    return (
      <div className={ROOT}>
        <div className={`${ROOT}-key`}>{this.props.title}</div>
        {/* <div className={`${ROOT}-separater`}>:</div> */}
        <div className={`${ROOT}-value`}>
          <Radio
            name={this.props.name}
            items={this.props.items}
            checked={this.props.checked}
            onChange={this.props.onChange}
            readOnly={this.props.readOnly}
          />
        </div>
      </div>
    );
  }
}
