// @flow
import React from 'react';
import { get } from 'lodash';
import { Field } from 'formik';

// common components
import msg from '../../../../../../languages';
import Tooltip from '../../../../../Tooltip';
import MultiColumnsGrid from '../../../../../MultiColumnsGrid';
import FormatUtil from '../../../../../../utils/FormatUtil';
import StringUtil from '../../../../../../utils/StringUtil';
import AmountField from '../../../../../fields/AmountField';

import IconButton from '../../../../../buttons/IconButton';
import ImgEditOff from '../../../../../../images/btnEditOff.png';
import ImgEditOn from '../../../../../../images/btnEditOn.png';

// model
import { type CurrencyList } from '../../../../../../../domain/models/exp/foreign-currency/Currency';
import { type Record } from '../../../../../../../domain/models/exp/Record';

import './index.scss';

type Props = {
  expRecord: Record,
  targetRecord: string,
  currencyRecord: CurrencyList,
  readOnly: boolean,
  isItemized: boolean,
  hasChildItems: boolean,
  // Container props
  searchCurrencyList: () => void,
  onCurrencyChange: (
    currencyId: any,
    decimalPlaces: number,
    symbol: string
  ) => void,
  calcNewRate: (exchangeRate: number) => void,
  onChangeAmountField: (localAmount: number) => void,
  updateNewRate: (exchangeRate: number) => void,
  validateRate: (value: string) => void,
  onChangeEditingExpReport: (string, any, boolean) => void,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  // Formik
  formikErrors: any,
  handleChange: (e: SyntheticEvent<Element>) => void,
  setFieldValue: (string, any) => void,
  recordItemIdx: number,
  isFixedAllowance: boolean,
  fixedAmountMessage?: string,
};

type State = {
  targetRecord: string,
};

const ROOT = 'ts-expenses-requests__contents__amount';

export default class ForeignCurrency extends React.Component<Props, State> {
  state = {
    targetRecord: 'records[-1]',
  };

  componentWillMount() {
    if (this.props.recordItemIdx > 0) {
      return;
    }
    this.props.searchCurrencyList();
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.recordItemIdx > 0) {
      return;
    }

    const isTargetRecordChanged =
      this.state.targetRecord !== prevProps.targetRecord;

    if (isTargetRecordChanged) {
      if (!this.props.expRecord.items[0].currencyId) {
        this.setCurrencyInfo();
      }
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        targetRecord: prevProps.targetRecord,
      });
    }
  }

  setCurrencyInfo() {
    const record = this.props.expRecord.items[0];
    const propRecord = this.props.currencyRecord[0];
    if (!record.currencyId && !propRecord) {
      return;
    }

    const currencyId = record.currencyId || propRecord.id;
    const decimal =
      record.currencyInfo.decimalPlaces || Number(propRecord.decimalPlaces);
    const symbol =
      (record.currencyInfo && record.currencyInfo.symbol) || propRecord.symbol;
    this.props.onCurrencyChange(currencyId, decimal, symbol);
  }

  handleCurrencySelectorChange = (e: any) => {
    const selected = e.target[e.target.selectedIndex];
    const currencyDecimal = Number(selected.getAttribute('data-decimal'));
    const currencySymbol = selected.getAttribute('data-symbol');
    this.props.onCurrencyChange(
      e.target.value,
      currencyDecimal,
      currencySymbol
    );
  };

  handleExchangeRateChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const { value } = e.target;
    if (this.props.validateRate(value) || value.length === 0) {
      this.props.handleChange(e);
    }
  };

  handleExchangeRateBlur = (e: SyntheticInputEvent<HTMLInputElement>) => {
    const value = StringUtil.convertToHankaku(
      StringUtil.removeComma(e.target.value)
    );
    if (value === '') {
      this.props.setFieldValue('exchangeRate', '0');
    } else {
      this.props.setFieldValue(
        'exchangeRate',
        StringUtil.removeLeadingZeroes(value)
      );
    }
    this.props.updateNewRate(Number(value));
    this.props.calcNewRate(Number(value));
  };

  handleEditButtonClick = () => {
    const itemIdx = this.props.recordItemIdx || 0;
    const {
      originalExchangeRate,
      exchangeRate,
      exchangeRateManual,
    } = this.props.expRecord.items[itemIdx];

    if (exchangeRateManual) {
      this.props.calcNewRate(originalExchangeRate || exchangeRate);
    }
    this.props.onChangeEditingExpReport(
      `${this.props.targetRecord}.items.${itemIdx}.exchangeRateManual`,
      !exchangeRateManual,
      false
    );
  };

  renderEditImage(isEditable: boolean, readOnly: boolean) {
    const itemIdx = this.props.recordItemIdx || 0;
    const originalExchangeRate = get(
      this.props.expRecord,
      `items.${itemIdx}.originalExchangeRate`
    );
    // when there is no exchange rate from master, user cannot exit edit mode
    const disabled = readOnly || (isEditable && !originalExchangeRate);
    const imgEdit = isEditable ? ImgEditOn : ImgEditOff;
    const imgEditAlt = isEditable ? 'ImgEditOn' : 'ImgEditOff';

    if (!isEditable && readOnly) {
      return null;
    }

    return (
      <div className="ts-tax-auto">
        <IconButton
          src={imgEdit}
          onClick={this.handleEditButtonClick}
          alt={imgEditAlt}
          disabled={disabled}
        />
      </div>
    );
  }

  renderEditImageForView() {
    return (
      <div className="ts-tax-auto">
        <IconButton src={ImgEditOn} />
      </div>
    );
  }

  renderCurrencySelectField() {
    const {
      currencyRecord,
      readOnly,
      expRecord,
      hasChildItems,
      isItemized,
      recordItemIdx = 0,
    } = this.props;
    const isFixedForeignCurrency = expRecord.items[0].useFixedForeignCurrency;
    const isChildItem = recordItemIdx > 0;
    const isParentHasChild = isItemized && hasChildItems;

    const selectField = (
      <Field
        className={`slds-input ${ROOT}__foreign-currency-input`}
        component="select"
        name="currencySelector"
        onChange={this.handleCurrencySelectorChange}
        disabled={
          readOnly || isChildItem || isParentHasChild || isFixedForeignCurrency
        }
      >
        {currencyRecord.map((record) => {
          return (
            <option
              key={record.id}
              value={record.id}
              data-symbol={record.symbol}
              data-decimal={record.decimalPlaces}
            >
              {record.isoCurrencyCode}
            </option>
          );
        })}
      </Field>
    );
    const fixedCurrencyHintMsg = isChildItem
      ? msg().Exp_Hint_FixedCurrencyChild
      : msg().Exp_Hint_FixedCurrency;
    return isFixedForeignCurrency ? (
      <Tooltip align="top" content={fixedCurrencyHintMsg}>
        <div>{selectField} </div>
      </Tooltip>
    ) : (
      selectField
    );
  }

  render() {
    const {
      expRecord,
      readOnly,
      baseCurrencySymbol,
      formikErrors,
      baseCurrencyDecimal,
      isItemized,
      isFixedAllowance,
      fixedAmountMessage,
      recordItemIdx = 0,
    } = this.props;

    const isEditable = get(
      expRecord,
      `items.${recordItemIdx}.exchangeRateManual`
    );
    const amount = FormatUtil.formatNumber(
      expRecord.items[recordItemIdx].amount,
      baseCurrencyDecimal
    );
    const amountError = get(formikErrors, `items.${recordItemIdx}.amount`);
    const modifiedClass = readOnly && isEditable ? 'ts-currency-modified' : '';

    const symbol = get(expRecord.items[0], 'currencyInfo.symbol');

    const amountField = (
      <AmountField
        className={
          readOnly
            ? 'input_right-aligned input_disabled_no-border input_disabled_no-background'
            : 'input_right-aligned'
        }
        disabled={readOnly || isFixedAllowance}
        fractionDigits={
          expRecord.items[0].currencyInfo
            ? expRecord.items[0].currencyInfo.decimalPlaces
            : 0
        }
        value={get(expRecord, `items.${recordItemIdx}.localAmount`, 0)}
        isFixedAllowance={isFixedAllowance}
        onBlur={(value: number | null) => {
          if (value !== null) {
            this.props.onChangeAmountField(value);
          }
        }}
      />
    );

    return (
      <div className={`${ROOT}`}>
        <MultiColumnsGrid sizeList={[5, 1, 5]}>
          <div className={`${ROOT}__local-amount`}>
            <div className="ts-text-field-container">
              <div className="key">
                {msg().Exp_Clbl_LocalAmount} {symbol ? `(${symbol})` : null}
              </div>
              {(isFixedAllowance && !readOnly && (
                <Tooltip align="top" content={fixedAmountMessage}>
                  <div>{amountField}</div>
                </Tooltip>
              )) ||
                amountField}
            </div>
          </div>

          <div />

          <div className={`${ROOT}__currency-selector`}>
            <div className="key">{msg().Exp_Clbl_Currency}</div>
            <div className="ts-text-field-container">
              {readOnly
                ? expRecord.items[0].currencyInfo.code
                : this.renderCurrencySelectField()}
            </div>
          </div>
        </MultiColumnsGrid>
        <MultiColumnsGrid
          sizeList={[5, 1, 5, 1]}
          alignments={['top', 'top', 'top', 'middle']}
        >
          <div className={`${ROOT}__amount`}>
            <div className="key">{msg().Exp_Clbl_Amount}</div>
            <input
              className="slds-input input_disabled_no-border input_disabled_no-background input_disabled_right-aligned"
              type="text"
              disabled
              value={`${baseCurrencySymbol} ${amount}`}
            />
            {amountError && (
              <div className="input-feedback">{msg()[amountError]}</div>
            )}
          </div>

          <div />

          {!isItemized && (
            <div className={`${ROOT}__exchange-rate`}>
              <div className="key">{msg().Exp_Clbl_ExchangeRate}</div>
              <Field
                type="tel"
                className={`${modifiedClass} slds-input input_disabled_no-border input_disabled_no-background input_disabled_right-aligned input_right-aligned`}
                name="exchangeRate"
                disabled={readOnly || !isEditable}
                onChange={this.handleExchangeRateChange}
                onBlur={this.handleExchangeRateBlur}
              />
            </div>
          )}

          {!isItemized && this.renderEditImage(isEditable, readOnly)}
        </MultiColumnsGrid>
      </div>
    );
  }
}
