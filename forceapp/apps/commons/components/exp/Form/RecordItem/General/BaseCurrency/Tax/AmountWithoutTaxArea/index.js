// @flow
import React from 'react';

import msg from '../../../../../../../../languages';
import FormatUtil from '../../../../../../../../utils/FormatUtil';

import type { RecordItem } from '../../../../../../../../../domain/models/exp/Record';

type Props = {
  expRecordItem: RecordItem,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
};

export default class AmountWithoutTaxArea extends React.Component<Props> {
  render() {
    const withoutTax = FormatUtil.formatNumber(
      this.props.expRecordItem.withoutTax,
      this.props.baseCurrencyDecimal
    );

    return (
      <div>
        <div className="key">{msg().Exp_Clbl_WithoutTax}</div>
        <input
          type="text"
          className="slds-input input_disabled_no-border input_disabled_no-background input_disabled_right-aligned amount-without-tax"
          disabled
          value={`${this.props.baseCurrencySymbol} ${withoutTax}`}
        />
      </div>
    );
  }
}
