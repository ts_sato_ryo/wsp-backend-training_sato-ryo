// @flow
import React from 'react';
import _ from 'lodash';
import msg from '../../../../../../../../languages';

import TaxUtil from '../../../../../../../../utils/TaxUtil';

import type { RecordItem } from '../../../../../../../../../domain/models/exp/Record';
import type { ExpTaxTypeList } from '../../../../../../../../../domain/models/exp/TaxType';

const ROOT = 'ts-expenses-taxRateArea';

type Props = {
  expRecordItem: RecordItem,
  readOnly: boolean,
  expenseTaxTypeList: ExpTaxTypeList,
  onChangeAmountOrTaxType: (?number, ?string, ?string) => void,
  recordItemIdx: number,
  errors: { items?: Array<Object> },
};

export default class TaxRateArea extends React.Component<Props> {
  renderTaxRateSelect() {
    const taxTypeList: ExpTaxTypeList = TaxUtil.setInitialExpTaxLabel(
      this.props.expenseTaxTypeList
    );
    if (taxTypeList && taxTypeList.length > 0) {
      const defaultTaxType = {};
      defaultTaxType.baseId = 'noIdSelected';
      defaultTaxType.historyId = '';
      defaultTaxType.rate = 0;
      defaultTaxType.name = msg().Exp_Sel_Auto_Select_From_Exp_Type;
      taxTypeList.unshift(defaultTaxType);
    }
    const selectTypeList = taxTypeList.map((item, idx) => (
      <option
        key={item.baseId}
        baseid={item.baseId}
        name={item.name}
        value={idx}
      >
        {item.name}
      </option>
    ));

    const selectedValue = this.props.expRecordItem.taxTypeBaseId
      ? _.findIndex(taxTypeList, {
          baseId: this.props.expRecordItem.taxTypeBaseId,
        })
      : 'noIdSelected';

    return (
      <select
        className="slds-select ts-select"
        value={selectedValue}
        onChange={(event) =>
          this.props.onChangeAmountOrTaxType(
            this.props.expRecordItem.amount,
            event.target.selectedOptions[0].getAttribute('baseId'),
            event.target.selectedOptions[0].getAttribute('name')
          )
        }
        disabled={this.props.readOnly}
      >
        {selectTypeList}
      </select>
    );
  }

  render() {
    const { errors, recordItemIdx = 0 } = this.props;

    const historyIdError = _.get(
      errors,
      `items.${recordItemIdx}.taxTypeHistoryId`
    );

    return (
      <div className={`${ROOT}`}>
        <div className="ts-text-field-container">
          <div className="key">{msg().Exp_Clbl_Gst}</div>
          <div className="value">
            {this.props.readOnly
              ? this.props.expRecordItem.taxTypeName
              : this.renderTaxRateSelect()}
          </div>
          {historyIdError && (
            <div className="input-feedback">{msg()[historyIdError]}</div>
          )}
        </div>
      </div>
    );
  }
}
