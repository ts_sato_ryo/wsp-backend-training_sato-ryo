// @flow
import React from 'react';

import msg from '../../../../../../../../languages';
import AmountField from '../../../../../../../fields/AmountField';
import type { RecordItem } from '../../../../../../../../../domain/models/exp/Record';

type Props = {
  readOnly: boolean,
  expRecordItem: RecordItem,
  baseCurrencyDecimal: number,
  baseCurrencySymbol: string,
  calcTaxFromGstVat: (number) => void,
};

export default class GstVatArea extends React.Component<Props> {
  handleAmountChange = (value: number | null) => {
    this.props.calcTaxFromGstVat(Number(value));
  };

  render() {
    const { readOnly, expRecordItem } = this.props;
    const isEditable = expRecordItem.taxManual;
    const modifiedClass =
      readOnly && isEditable
        ? 'ts-currency-modified input_disabled_no-border input_disabled_no-background input_disabled_right-aligned'
        : 'input_disabled_no-border input_disabled_no-background input_disabled_right-aligned input_right-aligned';

    return (
      <div>
        <div className="key">{msg().Exp_Clbl_GstAmount}</div>
        {expRecordItem.taxManual ? (
          <AmountField
            className={modifiedClass}
            value={expRecordItem.gstVat || 0}
            disabled={readOnly || !isEditable}
            fractionDigits={this.props.baseCurrencyDecimal}
            onBlur={this.handleAmountChange}
          />
        ) : (
          <input
            type="text"
            className="slds-input input_disabled_no-border input_disabled_no-background input_disabled_right-aligned gst-vat"
            disabled
            value={`${this.props.baseCurrencySymbol} ${expRecordItem.gstVat ||
              0}`}
          />
        )}
      </div>
    );
  }
}
