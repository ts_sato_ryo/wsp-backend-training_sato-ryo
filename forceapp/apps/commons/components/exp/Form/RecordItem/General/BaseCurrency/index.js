// @flow
import React from 'react';
import { get } from 'lodash';

import Tax from './Tax';
import Tooltip from '../../../../../Tooltip';
import msg from '../../../../../../languages';
import AmountField from '../../../../../fields/AmountField';
import MultiColumnsGrid from '../../../../../MultiColumnsGrid';

import { type Record } from '../../../../../../../domain/models/exp/Record';
import { type ExpTaxTypeList } from '../../../../../../../domain/models/exp/TaxType';
import TaxRateArea from './Tax/TaxRateArea';

type Props = {
  expRecord: Record,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  tax: { [string]: { [string]: ExpTaxTypeList } },
  readOnly: boolean,
  recordItemIdx: number,
  onClickEditButton: () => void,
  calcTaxFromGstVat: (number) => void,
  onChangeAmountOrTaxType: (?number, ?string, ?string) => void,
  searchTaxTypeList: () => void,
  isItemized: boolean,
  isFixedAllowance: boolean,
  allowTaxAmountChange: boolean,
  fixedAmountMessage?: string,
  errors: { items?: Array<Object> },
};

const ROOT = 'ts-expenses-requests__contents__amount';

export default class BaseCurrency extends React.Component<Props> {
  componentWillMount() {
    this.props.searchTaxTypeList();
  }

  componentWillReceiveProps(nextProps: Props) {
    if (!nextProps.readOnly) {
      const isChild = nextProps.recordItemIdx > 0;
      const isCreateNew = !nextProps.expRecord.recordId;
      if (isChild || nextProps.isItemized || isCreateNew) {
        return;
      }
      const thisItem = this.props.expRecord.items[0];
      const nextItem = nextProps.expRecord.items[0];

      const isExpTypeChanged = thisItem.expTypeId !== nextItem.expTypeId;
      const isDateChanged =
        this.props.expRecord.recordDate !== nextProps.expRecord.recordDate;
      const hasNoTaxWithDate = !get(
        nextProps,
        `tax[${nextItem.expTypeId}][${nextProps.expRecord.recordDate}]`
      );

      if (isExpTypeChanged || isDateChanged || hasNoTaxWithDate) {
        nextProps.searchTaxTypeList();
      }
    }
  }

  render() {
    const {
      readOnly,
      baseCurrencyDecimal,
      tax,
      expRecord,
      recordItemIdx = 0,
      isFixedAllowance,
      fixedAmountMessage,
    } = this.props;

    const { expTypeId } = expRecord.items[recordItemIdx];

    const recordDate =
      expRecord.items[recordItemIdx].recordDate || expRecord.recordDate;

    const expenseTaxTypeList =
      tax[expTypeId] && tax[expTypeId][recordDate]
        ? tax[expTypeId][recordDate]
        : [];

    const amountField = (
      <AmountField
        className={
          readOnly
            ? 'input_right-aligned input_disabled_no-border input_disabled_no-background'
            : 'input_right-aligned'
        }
        disabled={readOnly || isFixedAllowance}
        fractionDigits={baseCurrencyDecimal}
        value={expRecord.items[recordItemIdx].amount}
        isFixedAllowance={isFixedAllowance}
        onBlur={(value: number | null) =>
          this.props.onChangeAmountOrTaxType(value)
        }
      />
    );

    return (
      <div className={ROOT}>
        <MultiColumnsGrid sizeList={[5, 1, 5, 1]}>
          <div className={`${ROOT}__amount`}>
            <div className="ts-text-field-container">
              <div className="key">{msg().Exp_Clbl_IncludeTax}</div>
              {(isFixedAllowance && !readOnly && (
                <Tooltip align="top" content={fixedAmountMessage}>
                  <div>{amountField}</div>
                </Tooltip>
              )) ||
                amountField}
            </div>
          </div>
          <div />
          {!this.props.isItemized && (
            <TaxRateArea
              readOnly={this.props.readOnly}
              expRecordItem={expRecord.items[recordItemIdx]}
              recordItemIdx={recordItemIdx}
              expenseTaxTypeList={expenseTaxTypeList}
              onChangeAmountOrTaxType={this.props.onChangeAmountOrTaxType}
              errors={this.props.errors}
            />
          )}
          <div />
        </MultiColumnsGrid>
        <Tax
          baseCurrencySymbol={this.props.baseCurrencySymbol}
          baseCurrencyDecimal={baseCurrencyDecimal}
          expRecord={expRecord}
          readOnly={this.props.readOnly || this.props.isItemized}
          expenseTaxTypeList={expenseTaxTypeList}
          onChangeAmountOrTaxType={this.props.onChangeAmountOrTaxType}
          onClickEditButton={this.props.onClickEditButton}
          calcTaxFromGstVat={this.props.calcTaxFromGstVat}
          recordItemIdx={this.props.recordItemIdx}
          allowTaxAmountChange={this.props.allowTaxAmountChange}
        />
      </div>
    );
  }
}
