// @flow
import React from 'react';
import _ from 'lodash';

import { type Record } from '../../../../../../../../domain/models/exp/Record';
import { type ExpTaxTypeList } from '../../../../../../../../domain/models/exp/TaxType';
import IconButton from '../../../../../../buttons/IconButton';
import MultiColumnsGrid from '../../../../../../MultiColumnsGrid';

import GstVatArea from './GstVatArea';
import AmountWithoutTaxArea from './AmountWithoutTaxArea';

import ImgEditOn from '../../../../../../../images/btnEditOn.png';
import ImgEditOff from '../../../../../../../images/btnEditOff.png';

type Props = {
  expRecord: Record,
  readOnly: boolean,
  expenseTaxTypeList: ExpTaxTypeList,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  recordItemIdx: number,
  allowTaxAmountChange: boolean,
  onClickEditButton: () => void,
  calcTaxFromGstVat: (number) => void,
};

export default class Tax extends React.Component<Props> {
  renderEditImage() {
    const {
      expRecord,
      recordItemIdx,
      allowTaxAmountChange,
      readOnly,
      onClickEditButton,
    } = this.props;
    const isEditable = expRecord.items[recordItemIdx].taxManual;
    const imgEdit = isEditable ? ImgEditOn : ImgEditOff;
    const imgEditAlt = isEditable ? 'ImgEditOn' : 'ImgEditOff';

    if (!allowTaxAmountChange || (!isEditable && readOnly)) {
      return null;
    }

    return (
      <IconButton
        src={imgEdit}
        onClick={() => onClickEditButton()}
        alt={imgEditAlt}
        disabled={readOnly}
      />
    );
  }

  render() {
    const { expRecord, expenseTaxTypeList, recordItemIdx } = this.props;
    const readOnly = this.props.readOnly || expenseTaxTypeList.length === 0;
    return (
      <div>
        <MultiColumnsGrid
          sizeList={[5, 1, 5, 1]}
          alignments={['top', 'top', 'top', 'middle']}
        >
          <AmountWithoutTaxArea
            expRecordItem={expRecord.items[recordItemIdx]}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
          />

          <div />

          <GstVatArea
            readOnly={readOnly}
            expRecordItem={expRecord.items[recordItemIdx]}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            calcTaxFromGstVat={this.props.calcTaxFromGstVat}
          />
          <div className="ts-tax-auto">{this.renderEditImage()}</div>
        </MultiColumnsGrid>
      </div>
    );
  }
}
