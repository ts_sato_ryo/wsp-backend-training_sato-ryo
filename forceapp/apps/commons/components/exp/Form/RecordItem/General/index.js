/* @flow */
import React from 'react';

import { type Record } from '../../../../../../domain/models/exp/Record';
import './index.scss';

type Props = {
  expRecord: Record,
  targetRecord: string,
  recordItemIdx: number,
  onChangeEditingExpReport: (string, any) => void,
  readOnly: boolean,
  touched: { recordDate?: string, records?: Array<*> },
  baseCurrencyCode: string,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  errors: any,
  // components
  baseCurrency: any,
  foreignCurrency: any,
  isItemized?: boolean,
  hasChildItems?: boolean,
  isFixedAllowance?: boolean,
  fixedAmountMessage?: string,
};

export default class General extends React.Component<Props> {
  render() {
    const {
      expRecord,
      targetRecord,
      readOnly,
      touched,
      onChangeEditingExpReport,
      baseCurrencyCode,
      baseCurrencySymbol,
      baseCurrencyDecimal,
      errors,
      isItemized,
      isFixedAllowance,
      fixedAmountMessage,
      hasChildItems,
      recordItemIdx = 0,
    } = this.props;

    const ForeignCurrencyContainer = this.props.foreignCurrency;
    const BaseCurrencyContainer = this.props.baseCurrency;

    if (expRecord.items[0].useForeignCurrency) {
      return (
        <ForeignCurrencyContainer
          readOnly={readOnly}
          touched={touched}
          formikErrors={errors}
          expRecord={expRecord}
          targetRecord={targetRecord}
          onChangeEditingExpReport={onChangeEditingExpReport}
          baseCurrencyCode={baseCurrencyCode}
          baseCurrencySymbol={baseCurrencySymbol}
          baseCurrencyDecimal={baseCurrencyDecimal}
          isItemized={isItemized}
          isFixedAllowance={isFixedAllowance}
          fixedAmountMessage={fixedAmountMessage}
          hasChildItems={hasChildItems}
          recordItemIdx={recordItemIdx}
        />
      );
    } else {
      return (
        <BaseCurrencyContainer
          readOnly={readOnly}
          touched={touched}
          errors={errors}
          expRecord={expRecord}
          recordItemIdx={recordItemIdx}
          targetRecord={targetRecord}
          onChangeEditingExpReport={onChangeEditingExpReport}
          baseCurrencyCode={baseCurrencyCode}
          baseCurrencySymbol={baseCurrencySymbol}
          baseCurrencyDecimal={baseCurrencyDecimal}
          isItemized={isItemized}
          isFixedAllowance={isFixedAllowance}
          fixedAmountMessage={fixedAmountMessage}
        />
      );
    }
  }
}
