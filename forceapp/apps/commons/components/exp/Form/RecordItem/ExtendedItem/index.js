// @flow
import * as React from 'react';
import _ from 'lodash';

import TextField from '../../../../fields/TextField';
import SelectField from '../../../../fields/SelectField';
import DateField from '../../../../fields/DateField';
import LabelWithHint from '../../../../fields/LabelWithHint';

import Button from '../../../../buttons/Button';
import IconButton from '../../../../../../mobile-app/components/atoms/IconButton';

import msg from '../../../../../languages';

import {
  getExtendedItemArray,
  type ExtendedItem,
  type EISearchObj,
  type ExtendItemInfo,
} from '../../../../../../domain/models/exp/ExtendedItem';

import { type RecordItem } from '../../../../../../domain/models/exp/Record';
import './index.scss';

const ROOT = 'ts-expenses__form-record-item-summary';
type Props = {
  onChangeEditingExpReport: (string, any) => void,
  onClickLookupEISearch: (item: EISearchObj) => void,
  readOnly: boolean,
  recordItem: RecordItem,
  targetRecordItem: string,
  errors: { recordDate?: string, records?: Array<*> },
  touched: { recordDate?: string, records?: Array<*> },
};

export default class ExtendedItems extends React.Component<Props> {
  onChangeSelectField(
    e: SyntheticInputEvent<HTMLInputElement>,
    targetIndex: string
  ) {
    this.props.onChangeEditingExpReport(
      `${this.props.targetRecordItem}.extendedItemPicklist${targetIndex}Value`,
      e.target.value
    );
  }

  removeEILookup(targetIndex: string) {
    const field = `${this.props.targetRecordItem}.extendedItemLookup${targetIndex}Value`;
    this.props.onChangeEditingExpReport(field, null);
  }

  buildOptionList(picklist: any) {
    const optionList = picklist.map((pick) => {
      return {
        value: pick.value || '',
        text: pick.label,
      };
    });
    return [{ value: '', text: msg().Exp_Lbl_PleaseSelect }, ...optionList];
  }

  handleExtendedItemChange(
    e: SyntheticInputEvent<HTMLInputElement>,
    targetIndex: string
  ) {
    this.props.onChangeEditingExpReport(
      `${this.props.targetRecordItem}.extendedItemText${targetIndex}Value`,
      e.target.value
    );
  }

  handleExtendedItemDateChange(value: string, targetIndex: string) {
    this.props.onChangeEditingExpReport(
      `${this.props.targetRecordItem}.extendedItemDate${targetIndex}Value`,
      value
    );
  }

  renderLabel(info: ?ExtendItemInfo) {
    const text = (info && info.name) || '';
    const hintMsg = (!this.props.readOnly && info && info.description) || '';
    const isRequired = (info && info.isRequired) || false;
    return (
      <LabelWithHint text={text} hintMsg={hintMsg} isRequired={isRequired} />
    );
  }

  render() {
    const { recordItem, readOnly } = this.props;

    const extendedItems = getExtendedItemArray(recordItem);

    const extendedItemsWithID = extendedItems.filter((i) => i.id);

    if (!extendedItemsWithID.length) {
      return null;
    }

    return (
      <div className={`${ROOT}`}>
        {extendedItemsWithID.map(
          (extendedItem: ExtendedItem, index: number): React.Node => {
            const targetIndex = extendedItem.index;
            const type = extendedItem.info ? extendedItem.info.inputType : '';
            const extendedItemError = _.get(
              this.props.errors,
              `${this.props.targetRecordItem}.extendedItem${type}${targetIndex}Value`
            );
            const isExtendedItemTouched = _.get(
              this.props.touched,
              `${this.props.targetRecordItem}.extendedItem${type}${targetIndex}Value`
            );
            return (
              <div key={Number(index)} className="ts-text-field-container">
                {this.renderLabel(extendedItem.info)}
                {type === 'Text' && (
                  <TextField
                    value={extendedItem.value || ''}
                    onChange={(e) =>
                      this.handleExtendedItemChange(e, targetIndex)
                    }
                    disabled={readOnly}
                  />
                )}
                {type === 'Picklist' && (
                  <SelectField
                    className="ts-select-input"
                    onChange={(e) => this.onChangeSelectField(e, targetIndex)}
                    id={`${ROOT}-extendedItems-${extendedItem.index}`}
                    options={this.buildOptionList(
                      extendedItem.info && extendedItem.info.picklist
                    )}
                    value={extendedItem.value || ''}
                    disabled={readOnly}
                  />
                )}
                {type === 'Lookup' && (
                  <div
                    className={`${ROOT}__ei-lookup-input ts-text-field-container`}
                  >
                    {extendedItem.value ? (
                      <div className={`${ROOT}__ei-lookup-input-field`}>
                        <span>{`${extendedItem.value} - ${extendedItem.name ||
                          ''}`}</span>
                        <IconButton
                          icon="close-copy"
                          size="small"
                          className={`${ROOT}__ei-lookup-input-btn--clear`}
                          onClick={() => this.removeEILookup(targetIndex)}
                          disabled={readOnly || !recordItem}
                        />
                      </div>
                    ) : (
                      <Button
                        className={`${ROOT}__ei-lookup-btn--search`}
                        onClick={() =>
                          extendedItem.info &&
                          this.props.onClickLookupEISearch({
                            extendedItemLookupId: extendedItem.id,
                            extendedItemCustomId:
                              extendedItem.info.extendedItemCustomId,
                            name: extendedItem.info.name,
                            idx: targetIndex,
                            target: 'RECORD',
                            hintMsg: extendedItem.info.description,
                          })
                        }
                        disabled={readOnly}
                      >
                        {msg().Com_Lbl_Select}
                      </Button>
                    )}
                  </div>
                )}
                {type === 'Date' && (
                  <DateField
                    className={`${ROOT}__ei-date `}
                    disabled={readOnly}
                    onChange={(value) => {
                      this.handleExtendedItemDateChange(value, targetIndex);
                    }}
                    value={extendedItem.value || ''}
                  />
                )}
                {extendedItemError && isExtendedItemTouched && (
                  <div className="value">
                    <div className="input-feedback">
                      {msg()[extendedItemError]}
                    </div>
                  </div>
                )}
              </div>
            );
          }
        )}
      </div>
    );
  }
}
