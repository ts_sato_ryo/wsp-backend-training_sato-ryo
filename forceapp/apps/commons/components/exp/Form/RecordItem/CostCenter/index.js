// @flow
import React from 'react';

import msg from '../../../../../languages';
import Button from '../../../../buttons/Button';

import { type Report } from '../../../../../../domain/models/exp/Report';
import { type Record } from '../../../../../../domain/models/exp/Record';
import LabelWithHint from '../../../../fields/LabelWithHint';

type Props = {
  expReport: Report,
  expRecord: Record,
  readOnly: boolean,
  hintMsg?: string,
  handleClickCostCenterBtn: (string) => void,
};

const ROOT = 'ts-expenses-requests__record_cost-center';
const RecordCostCenter = (props: Props) => {
  const {
    expReport,
    expRecord,
    readOnly,
    handleClickCostCenterBtn,
    hintMsg,
  } = props;
  const recordDate = expRecord.recordDate;
  const {
    costCenterHistoryId,
    costCenterCode,
    costCenterName,
  } = expRecord.items[0];
  let ccCode = expReport.costCenterCode || '';
  let ccName = expReport.costCenterName || '';
  if (costCenterHistoryId) {
    ccCode = costCenterCode;
    ccName = costCenterName;
  }

  return (
    <>
      <div className={`${ROOT} ts-text-field-container`}>
        <LabelWithHint
          text={msg().Exp_Clbl_CostCenter}
          hintMsg={(!readOnly && hintMsg) || ''}
          isRequired
        />
        <div className={`${ROOT}-field`}>
          <span>{`${ccCode} - ${ccName}`}</span>
          {!readOnly && (
            <Button
              className={`${ROOT}-btn--change`}
              onClick={() => handleClickCostCenterBtn(recordDate)}
              disabled={readOnly}
            >
              {msg().Com_Btn_Change}
            </Button>
          )}
        </div>
      </div>
    </>
  );
};

export default RecordCostCenter;
