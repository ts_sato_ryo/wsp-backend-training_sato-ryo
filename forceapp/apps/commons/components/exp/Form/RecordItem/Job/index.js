// @flow
import React from 'react';

import msg from '../../../../../languages';
import Button from '../../../../buttons/Button';

import { type Report } from '../../../../../../domain/models/exp/Report';
import { type Record } from '../../../../../../domain/models/exp/Record';
import LabelWithHint from '../../../../fields/LabelWithHint';

type Props = {
  expReport: Report,
  expRecord: Record,
  readOnly: boolean,
  hintMsg?: string,
  handleClickJobBtn: (string) => void,
};

const ROOT = 'ts-expenses-requests__record_job';

const RecordJob = (props: Props) => {
  const { expReport, expRecord, readOnly, handleClickJobBtn, hintMsg } = props;
  const recordDate = expRecord.recordDate;
  const { jobId, jobCode, jobName } = expRecord.items[0];

  let recordJobCode = expReport.jobCode;
  let recordJobName = expReport.jobName;
  if (jobId) {
    recordJobCode = jobCode;
    recordJobName = jobName;
  }

  return (
    <>
      <div className={`${ROOT} ts-text-field-container`}>
        <LabelWithHint
          text={msg().Exp_Lbl_Job}
          hintMsg={(!readOnly && hintMsg) || ''}
          isRequired
        />
        <div className={`${ROOT}-field`}>
          <span>{`${recordJobCode} - ${recordJobName}`}</span>
          {!readOnly && (
            <Button
              className={`${ROOT}-btn--change`}
              onClick={() => handleClickJobBtn(recordDate)}
              disabled={readOnly}
            >
              {msg().Com_Btn_Change}
            </Button>
          )}
        </div>
      </div>
    </>
  );
};
export default RecordJob;
