// @flow
import React from 'react';

import './index.scss';

const ROOT = 'ts-navigation-button';

type Props = {
  imgSrc: string,
  imgAlt: string,
  title: string,
  onClick: any,
};

const NavigationButton = (props: Props) => {
  return (
    <button type="button" className={ROOT} onClick={props.onClick}>
      <img src={props.imgSrc} alt={props.imgAlt} />
      <span>{props.title}</span>
    </button>
  );
};

export default NavigationButton;
