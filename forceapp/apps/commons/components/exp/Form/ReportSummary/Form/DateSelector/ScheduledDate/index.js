// @flow
import React from 'react';

import msg from '../../../../../../../languages';
import DateField from '../../../../../../fields/DateField';
import DateUtil from '../../../../../../../utils/DateUtil';
import LabelWithHint from '../../../../../../fields/LabelWithHint';

type Props = {
  // ui states
  errors: {|
    scheduledDate: string,
    subject: string,
    purpose: string,
  |},
  touched: {|
    records?: Array<*>,
    scheduledDate: string,
    subject: string,
    purpose: string,
  |},
  // HOC props
  onChangeScheduledDate: (date: string) => void,
  expReport: any,
  hintMsg?: string,
  readOnly: boolean,
};

// this class is only used in expense request (not in expense report)
export default class ScheduledDate extends React.Component<Props> {
  render() {
    const { errors, touched, hintMsg, readOnly } = this.props;

    return (
      <React.Fragment>
        <div className="ts-text-field-container">
          <LabelWithHint
            text={msg().Exp_Clbl_ScheduledDate}
            hintMsg={(!readOnly && hintMsg) || ''}
            isRequired
          />
          <DateField
            value={DateUtil.format(
              this.props.expReport.scheduledDate,
              'YYYY-MM-DD'
            )}
            onChange={this.props.onChangeScheduledDate}
            disabled={readOnly}
          />
        </div>
        {errors.scheduledDate && touched.scheduledDate && (
          <div className="input-feedback">{msg()[errors.scheduledDate]}</div>
        )}
      </React.Fragment>
    );
  }
}
