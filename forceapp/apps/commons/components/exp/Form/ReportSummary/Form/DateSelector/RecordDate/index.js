// @flow
import React from 'react';
import msg from '../../../../../../../languages';
import MultiColumnsGrid from '../../../../../../MultiColumnsGrid';
import DateField from '../../../../../../fields/DateField';
import DateUtil from '../../../../../../../utils/DateUtil';
import LabelWithHint from '../../../../../../fields/LabelWithHint';

type Props = {
  // ui states
  errors: {|
    accountingDate: string,
    subject: string,
  |},
  touched: {|
    records?: Array<*>,
    accountingDate: string,
    subject: string,
  |},
  // HOC props
  onChangeRecordDate: (date: string) => void,
  expReport: any,
  hintMsg?: string,
  readOnly: boolean,
};

// this class is only used in expense request (not in expense report)
export default class RecordDate extends React.Component<Props> {
  render() {
    const { errors, touched, hintMsg, readOnly } = this.props;

    return (
      <React.Fragment>
        <MultiColumnsGrid alignments={['top', 'middle']} sizeList={[6, 6]}>
          <div className="ts-text-field-container">
            <LabelWithHint
              text={msg().Exp_Clbl_RecordDate}
              hintMsg={(!readOnly && hintMsg) || ''}
              isRequired
            />
            <DateField
              value={DateUtil.format(
                this.props.expReport.accountingDate,
                'YYYY-MM-DD'
              )}
              onChange={this.props.onChangeRecordDate}
              disabled={readOnly}
            />
          </div>
        </MultiColumnsGrid>
        {errors.accountingDate && touched.accountingDate && (
          <div className="input-feedback">{msg()[errors.accountingDate]}</div>
        )}
      </React.Fragment>
    );
  }
}
