// @flow
import React from 'react';
import { isEmpty } from 'lodash';

import ApAndRecordDateContainer from '../../../../../../../expenses-pc/containers/Expenses/AccountingPeriodAndrecordDateContainer';
import RecordDateContainer from '../../../../../../../expenses-pc/containers/Expenses/RecordDateContainer';
import ScheduledDateContainer from '../../../../../../../requests-pc/containers/Requests/ScheduledDateContainer';
import { type Report } from '../../../../../../../domain/models/exp/Report';
import { type AccountingPeriod } from '../../../../../../../domain/models/exp/AccountingPeriod';
import { type CustomHint } from '../../../../../../../domain/models/exp/CustomHint';

type Props = {
  // componentType: string,
  errors: any,
  touched: any,
  expReport: Report,
  customHint: CustomHint,
  onChangeEditingExpReport: any,
  isExpense?: boolean,
  isFinanceApproval?: boolean,
  apActive: AccountingPeriod,
  readOnly: boolean,
};

// this class is only used in expense request (not in expense report)
export default class DateSelector extends React.Component<Props> {
  selectComponent() {
    const isExpenseOrFinanceApproval =
      this.props.isExpense || this.props.isFinanceApproval;
    const isRequest = !this.props.isExpense;
    const existActiveAp = !isEmpty(this.props.apActive);

    if (isExpenseOrFinanceApproval && existActiveAp) {
      return (
        <ApAndRecordDateContainer
          expReport={this.props.expReport}
          customHint={this.props.customHint}
          onChangeEditingExpReport={this.props.onChangeEditingExpReport}
          readOnly={this.props.readOnly}
          isFinanceApproval={this.props.isFinanceApproval}
          errors={this.props.errors}
          touched={this.props.touched}
        />
      );
    } else if (isExpenseOrFinanceApproval && !existActiveAp) {
      // only RecordDate without accounting period
      return (
        <RecordDateContainer
          expReport={this.props.expReport}
          hintMsg={this.props.customHint.reportHeaderRecordDate}
          onChangeEditingExpReport={this.props.onChangeEditingExpReport}
          readOnly={this.props.readOnly}
          errors={this.props.errors}
          touched={this.props.touched}
        />
      );
    } else if (isRequest) {
      return (
        <ScheduledDateContainer
          expReport={this.props.expReport}
          hintMsg={this.props.customHint.reportHeaderScheduledDate}
          onChangeEditingExpReport={this.props.onChangeEditingExpReport}
          readOnly={this.props.readOnly}
          errors={this.props.errors}
          touched={this.props.touched}
        />
      );
    } else {
      return null;
    }
  }

  render() {
    return <React.Fragment>{this.selectComponent()}</React.Fragment>;
  }
}
